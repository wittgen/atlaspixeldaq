#!/usr/bin/python
import subprocess
import paramiko
import sys
from scp import SCPClient
class Ppc:
    def __init__(self,ip):
    	self.ip=ip

    def ssh_cmd(self,cmd):
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:
            ssh.connect(self.ip, username='root')
        except:
            ssh.connect(self.ip, username='root',password='root')
        stdin, stdout, stderr = ssh.exec_command(cmd)
        out=stdout.readlines()
        err=stderr.readlines()
        rc=stdout.channel.recv_exit_status()
        ssh.close()
        return (out,err, rc)

    def ssh_get(self,source,dest,recursive=False):
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:
            ssh.connect(self.ip, username='root')
        except:
            ssh.connect(self.ip, username='root',password='root')
        scp = SCPClient(ssh.get_transport())
        scp.get(source, dest,recursive)
        ssh.close()
    def ssh_put(self,source,dest,recursive=False):
          ssh = paramiko.SSHClient()
          ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
          try:
              ssh.connect(self.ip, username='root')
          except:
              ssh.connect(self.ip, username='root',password='root')
          scp = SCPClient(ssh.get_transport())
          scp.put(source, dest,recursive)
          ssh.close()

    def getfromtftp(self):
        print ("Download from tftp. It will take ~10s")
        output=self.ssh_cmd("/etc/init.d/S70pixel_daq getfromtftp")[0]
        for i in output: print(i, end = '')

    def stop(self):

        output=self.ssh_cmd("/etc/init.d/S70pixel_daq stop")[0]
        for i in output: print(i, end = '')

    def start(self):
        output=self.ssh_cmd("/etc/init.d/S70pixel_daq start")[0]
        for i in output: print(i, end = '')
        
    def status(self):        
        (output, err, rc) =self.ssh_cmd("/etc/init.d/S70pixel_daq status")
        for i in output: print(i, end = '')
        for i in err: print(i, end = '')
        print("Return code {0}".format(rc))


    def install(self):
        self.stop()
        self.ssh_put("/home/okepka/daq/build/RodDaq/IblDaq/RodMaster/Software/PPCpp/ppc_main","/root/ppc_main")
#        self.start()
        return "Done"

    def reboot(self):
        self.stop()
        output=self.ssh_cmd("reboot")

        return "Done"


def usage():
    print ("ppc_talk_py <PPC hostname> <install|start|stop|restart|status|getfromtftp>")
    print ("   install : copy ppc daq sw and restart")
    print ("   stop    : stop ppc daq service")
    print ("   start   : start ppc daq service")
    print ("   restart : restart ppc daq service")
    print ("   status  : print ppc daq status")
    exit(1)

if __name__ == "__main__":

    if(len(sys.argv)!=3): usage()

    ppc=Ppc(sys.argv[1])
    cmd=sys.argv[2]
    try:
        call=getattr(ppc,cmd);
    except:
        usage()
    call()







