#!/usr/bin/python
import sys
import threading
from ppc_talk import usage as ppc_usage, Ppc

def usage():

    print("<ppc hostname> is comma separated string with: ip, rodname, crate, SR1/PIT")

    ppc_usage()

    exit(1)



if(len(sys.argv)!=3): usage()

rodstr = sys.argv[1]
cmd = sys.argv[2]

rodlist = rodstr.rsplit(",")

for i in rodlist: print(i)

for hostname in rodlist:

    ppc=Ppc(hostname)
    try:
        call=getattr(ppc,cmd);
    except:
        usage()

    t = threading.Thread(target=call)

    t.start()
