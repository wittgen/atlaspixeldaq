#include <QApplication>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QScrollArea>
#include <QTabWidget>
#include <QLineEdit>
#include <QLabel>
#include <QComboBox>

#include "DefaultClient.h"
#include "GetFei4CfgRod.h"

#include "FeI4GlobCfgWidget.h"
#include "FeI4PixelMapWidget.h"

int main(int argc, char* argv[]){

    QApplication app(argc, argv);

    auto emptyGlobCfg = Fei4GlobalCfg();
    auto emptyPixCfg = Fei4PixelCfg();
    auto widget = new FeI4GlobCfgWidget(&emptyGlobCfg);
    auto widget2 = new FeI4PixelConfWidget(&emptyPixCfg);
    
    std::map<std::string, Fei4GlobalCfg> globalCfgs;
    std::map<std::string, Fei4PixelCfg> pixelCfgs;

    // Horizontal layout with 3 buttons
    QHBoxLayout *hLayout = new QHBoxLayout;
    auto ip_label = new QLabel("ROD IP:");
    auto ip_box = new QLineEdit("10.11.56.135");
    auto channel_label = new QLabel("Channel:");
    auto channel_box = new QLineEdit("1");
    auto name_label = new QLabel("Name:");
    auto name_box = new QLineEdit("default_name");
    auto go_button = new QPushButton("Go");

    hLayout->addWidget(ip_label);
    hLayout->addWidget(ip_box);
    hLayout->addWidget(channel_label);
    hLayout->addWidget(channel_box);
    hLayout->addWidget(name_label);
    hLayout->addWidget(name_box);
    hLayout->addWidget(go_button);

    // Vertical layout with 3 buttons
    QHBoxLayout *hLayout2 = new QHBoxLayout;
    auto select1 = new QComboBox();
    auto select2 = new QComboBox();
    auto compare_button = new QPushButton("Compare");

    QObject::connect(go_button, &QPushButton::clicked, 
		     [&](){ 
    			   DefaultClient HCPClient(ip_box->text().toUtf8());
                           GetFei4CfgRod fei4rdbck;
                           int const chan = channel_box->text().toInt();
                           fei4rdbck.m_rxMask = 1 << chan;
                           fei4rdbck.m_opMode = GetFei4CfgRod::FullFE;
                           HCPClient.run(fei4rdbck);
			   auto name = name_box->text()+"_Ch"+channel_box->text();
		           select1->addItem(name); 
		           select2->addItem(name); 
		           globalCfgs[name.toStdString()] = std::move(fei4rdbck.result.GlobalCfg[chan][0]);
		           pixelCfgs[name.toStdString()] = std::move(fei4rdbck.result.PixelCfg[chan][0]);
		          }
		     );

    hLayout2->addWidget(select1);
    hLayout2->addWidget(select2);
    hLayout2->addWidget(compare_button);

    // Outer Layer
    QVBoxLayout *mainLayout = new QVBoxLayout;
    
    // Add the previous two inner layouts
    mainLayout->addLayout(hLayout);
    mainLayout->addLayout(hLayout2);
    mainLayout->addWidget(widget);
    mainLayout->addWidget(widget2);

    // Create a widget
    QWidget *w = new QWidget();
    w->setAttribute(Qt::WA_AlwaysShowToolTips,true); 
    // Set the outer layout as a main layout 
    // of the widget
    w->setLayout(mainLayout);

    // Window title
    w->setWindowTitle("ConfMon");
    
    // Display
    w->show();
 
    QObject::connect(compare_button, &QPushButton::clicked, [&](){
		    if(widget) widget->deleteLater();
		    auto newWidget = new FeI4GlobCfgWidget(&globalCfgs[select1->currentText().toStdString()],
			         	                   &globalCfgs[select2->currentText().toStdString()]);
		    mainLayout->replaceWidget(widget, newWidget);
		    newWidget->show();
		    widget = newWidget;

		    if(widget2) widget2->deleteLater();
		    auto newWidget2 = new FeI4PixelConfWidget(&pixelCfgs[select1->currentText().toStdString()],
 			         	                      &pixelCfgs[select2->currentText().toStdString()]);
		    mainLayout->replaceWidget(widget2, newWidget2);
		    newWidget2->show();
		    widget2 = newWidget2;

		    });   
    // Event loop
    return app.exec();
}
