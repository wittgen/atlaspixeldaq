#ifndef FeI4GlobCfgWidget_h
#define FeI4GlobCfgWidget_h

#include <QWidget>
#include "Fei4GlobalCfg.h"

class FeI4GlobCfgWidget: public QWidget {
  public:
	FeI4GlobCfgWidget(Fei4GlobalCfg const * const cfg1 = nullptr, Fei4GlobalCfg const * const cfg2 = nullptr);
};


#endif
