#ifndef FeI4PixelMapWidget_h
#define FeI4PixelMapWidget_h

#include <QWidget>
#include <QVBoxLayout>
#include <QChartView>
#include <QComboBox>
#include <QCheckBox>
#include "Fei4PixelCfg.h"

class FeI4PixelTDACMapWidget: public QWidget {
  public:
	FeI4PixelTDACMapWidget(Fei4PixelCfg * const cfg1 = nullptr, Fei4PixelCfg * const cfg2 = nullptr);
};

class FeI4PixelTDACHistoWidget: public QtCharts::QChartView {
  public:
        FeI4PixelTDACHistoWidget(Fei4PixelCfg * const cfg1, Fei4PixelCfg * const cfg2, int min, int max, bool diffMode);
};

class FeI4PixelFDACMapWidget: public QWidget {
  public:
	FeI4PixelFDACMapWidget(Fei4PixelCfg * const cfg1 = nullptr, Fei4PixelCfg * const cfg2 = nullptr);
};

class FeI4PixelFDACHistoWidget: public QtCharts::QChartView {
  public:
        FeI4PixelFDACHistoWidget(Fei4PixelCfg * const cfg1, Fei4PixelCfg * const cfg2, int min, int max, bool diffMode);
};


class FeI4PixelConfWidget: public QWidget {
  public:
	FeI4PixelConfWidget(Fei4PixelCfg * const cfg1 = nullptr, Fei4PixelCfg * const cfg2 = nullptr);
   QComboBox * type = nullptr;
   Fei4PixelCfg * const mcfg1 = nullptr;
   QVBoxLayout * layout = nullptr;
   QWidget * widget = nullptr;
   Fei4PixelCfg * const mcfg2 = nullptr;
   QCheckBox * diffMode = nullptr;
};

#endif
