#include "FeI4GlobCfgWidget.h"
#include <QGridLayout>
#include <QBoxLayout>
#include <QLabel>
#include <QVariant>

FeI4GlobCfgWidget::FeI4GlobCfgWidget(Fei4GlobalCfg const * const cfg1, Fei4GlobalCfg const * const cfg2) {
  auto mainLayout = new QGridLayout(this);

  auto addLabel = [&mainLayout, &cfg1, &cfg2](std::string const & name, auto const & field, int row, int col){
    auto value_cfg1 = cfg1->getValue(field);
    auto value_cfg2 = cfg2 ? cfg2->getValue(field) : 0;
    auto label_name = new QLabel(name.c_str());
    auto label_value = new QLabel();
    QPalette pal = label_value->palette();

    if(cfg2 && (cfg1 != cfg2)) {
      if(value_cfg1 == value_cfg2) {
        pal.setColor(label_value->backgroundRole(), QColor(0,255,0,100));
        label_value->setText(QVariant(value_cfg1).toString());
      } else {
        pal.setColor(label_value->backgroundRole(), QColor(255,0,0,100));
        label_value->setText(std::string(std::to_string(value_cfg1)+"/"+std::to_string(value_cfg2)).c_str());
      }
    } else {
        pal.setColor(label_value->backgroundRole(), QColor(0,0,255,50));
        label_value->setText(QVariant(value_cfg1).toString());
    }

    label_value->setPalette(pal);
    label_value->setAutoFillBackground(true);
    
    label_name->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    label_value->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    
    mainLayout->addWidget(label_value, row, col);
    mainLayout->addWidget(label_name, row, col+1);
  };
  
  auto addTitel = [&mainLayout](std::string const & text, int row) {
    auto label = new QLabel(text.c_str());
    label->setStyleSheet("font-weight: bold; font-size: 10pt");
    mainLayout->addWidget(label, row, 0, 1, 5);
  };

  int col1 = 1;
  int col2 = 4;
  int col3 = 7;
  int col4 = 10;
  int col5 = 13;
  int col6 = 16;
  int col7 = 19;
  int col8 = 22;

  int const space = 8;
  mainLayout->setColumnMinimumWidth(col1-1, space);
  mainLayout->setColumnMinimumWidth(col2-1, space);
  mainLayout->setColumnMinimumWidth(col3-1, space);
  mainLayout->setColumnMinimumWidth(col4-1, space);
  mainLayout->setColumnMinimumWidth(col5-1, space);
  mainLayout->setColumnMinimumWidth(col6-1, space);
  mainLayout->setColumnMinimumWidth(col7-1, space);
  mainLayout->setColumnMinimumWidth(col8-1, space);

  int const textwidth = 135;
  mainLayout->setColumnMinimumWidth(col1+1, textwidth);
  mainLayout->setColumnMinimumWidth(col2+1, textwidth);
  mainLayout->setColumnMinimumWidth(col3+1, textwidth);
  mainLayout->setColumnMinimumWidth(col4+1, textwidth);
  mainLayout->setColumnMinimumWidth(col5+1, textwidth);
  mainLayout->setColumnMinimumWidth(col6+1, textwidth);
  mainLayout->setColumnMinimumWidth(col7+1, textwidth);
  mainLayout->setColumnMinimumWidth(col8+1, textwidth);

  int const valwidth = 55;
  mainLayout->setColumnMinimumWidth(col1, valwidth);
  mainLayout->setColumnMinimumWidth(col2, valwidth);
  mainLayout->setColumnMinimumWidth(col3, valwidth);
  mainLayout->setColumnMinimumWidth(col4, valwidth);
  mainLayout->setColumnMinimumWidth(col5, valwidth);
  mainLayout->setColumnMinimumWidth(col6, valwidth);
  mainLayout->setColumnMinimumWidth(col7, valwidth);
  mainLayout->setColumnMinimumWidth(col8, valwidth);


  int row = 0;
  //ROW 0
  addTitel("Initialisation of chip", row);
  
  //ROW 1
  row = 1;
  addLabel("EN_PLL", &Fei4GlobalCfg::PLL, row, col1);
  addLabel("CLK0", &Fei4GlobalCfg::clk0, row, col2);
  addLabel("CLK1", &Fei4GlobalCfg::clk1, row, col3);
  addLabel("EN_320M", &Fei4GlobalCfg::EN_320, row, col4);
  addLabel("EN_160M", &Fei4GlobalCfg::EN_160, row, col5);
  addLabel("EN_80M", &Fei4GlobalCfg::EN_80M, row, col6);
  addLabel("EN_40M", &Fei4GlobalCfg::EN_40M, row,col7);
  
  //ROW 2
  row = 2;
  addLabel("LVDSDrvEN (LVE)", &Fei4GlobalCfg::LVE, row, col1);
  addLabel("GateHitOr (HOR)", &Fei4GlobalCfg::HOR, row, col2);
  addLabel("DIGHITIN_SEL (DHS)", &Fei4GlobalCfg::DHS, row, col3);
  addLabel("PlsrPwr (PPW)", &Fei4GlobalCfg::PPW, row, col4);
  addLabel("VrefDigTune", &Fei4GlobalCfg::VrefDigTune, row, col5);
  addLabel("VrefAnTune", &Fei4GlobalCfg::VrefAnTune, row, col6);
  
  //ROW 3
  row = 3;
  addTitel("Options/Functions", row);
  
  //ROW 4
  row = 4;
  addLabel("Colpr_Mode (CP)", &Fei4GlobalCfg::CP, row, col1);
  addLabel("Colpr_Addr", &Fei4GlobalCfg::Colpr_Addr, row, col2);
  addLabel("no8b10b (N8b)", &Fei4GlobalCfg::N8b, row, col3);
  addLabel("Clk2OutCnfg (c2o)", &Fei4GlobalCfg::c2o, row, col4);
  addLabel("PxStrobes (Pixel_latch_strobe)", &Fei4GlobalCfg::Pixel_latch_strobe, row, col5);
  addLabel("S0", &Fei4GlobalCfg::S0, row, col6);
  addLabel("S1", &Fei4GlobalCfg::S1, row, col7);
  addLabel("HITLD_IN (HLD)", &Fei4GlobalCfg::HLD, row, col8);
  
  //ROW 5
  row = 5;
  addLabel("DINJ_Override (DJO)", &Fei4GlobalCfg::DJO, row, col1);
  addLabel("StopModeCnfg (STC)", &Fei4GlobalCfg::STC, row, col2);
  addLabel("ExtAnaCalSW (XAC)", &Fei4GlobalCfg::XAC, row, col3);
  addLabel("ExtDigCalSW (XDC)", &Fei4GlobalCfg::XDC, row, col4);
  addLabel("SRRead (SRR)", &Fei4GlobalCfg::SRR, row, col5);
  addLabel("GADCSel", &Fei4GlobalCfg::GADCSel, row, col6);
  addLabel("TempSensDiodeBiasSel (TM)", &Fei4GlobalCfg::TM, row, col7);
  addLabel("TempSensDisable (TMD)", &Fei4GlobalCfg::TMD, row, col8);

  row = 6;
  addLabel("MonleakRange (ILR)", &Fei4GlobalCfg::ILR, row, col1);
  
  row = 7;
  addTitel("Digital Value Settings and Masks", row);

  row = 8;
  addLabel("Trig_Count", &Fei4GlobalCfg::Trig_Count, row, col1);
  addLabel("Trig_Lat", &Fei4GlobalCfg::Trig_Lat, row, col2);
  addLabel("EventLimit", &Fei4GlobalCfg::EventLimit, row, col3);
  addLabel("CMDcnt", &Fei4GlobalCfg::CMDcnt12, row, col4); //CHECK ME
  addLabel("HitDiscCnfg (HD)", &Fei4GlobalCfg::HD, row, col5);
  addLabel("SmallHitErase (SME)", &Fei4GlobalCfg::SME, row, col6);
  addLabel("ErrorMask_0", &Fei4GlobalCfg::ErrorMask_0, row, col7);
  addLabel("ErrorMask_1", &Fei4GlobalCfg::ErrorMask_1, row, col8);

  row = 9;
  addLabel("SELB0", &Fei4GlobalCfg::SELB0, row, col1);
  addLabel("SELB1", &Fei4GlobalCfg::SELB1, row, col2);
  addLabel("SELB2", &Fei4GlobalCfg::SELB2, row, col3);
  addLabel("DisableColCnfg0", &Fei4GlobalCfg::DisableColCnfg0, row, col4);
  addLabel("DisableColCnfg1", &Fei4GlobalCfg::DisableColCnfg1, row, col5);
  addLabel("DisableColCnfg2", &Fei4GlobalCfg::DisableColCnfg2, row, col6);
  addLabel("Chip_SN (EFUSE)", &Fei4GlobalCfg::EFUSE, row, col7);
  addLabel("EmptyRecordCnfg", &Fei4GlobalCfg::EmptyRecordCnfg, row, col8);

  row = 10;
  addTitel("Control Pulser", row);

  row = 11;
  addLabel("SR_Clock (SRK)", &Fei4GlobalCfg::SRK, row, col1);
  addLabel("LatchEn (LEN)", &Fei4GlobalCfg::LEN, row, col2);
  addLabel("SRClr (SRC)", &Fei4GlobalCfg::SRC, row, col3);
  addLabel("CalEn (CAL)", &Fei4GlobalCfg::CAL, row, col4);
  addLabel("ReadErrorReq (RER)", &Fei4GlobalCfg::RER, row, col5);
  addLabel("StopClkPulse (STP)", &Fei4GlobalCfg::STP, row, col6);
  addLabel("EFUSE_Sense (EFS)", &Fei4GlobalCfg::EFS, row, col7);
  addLabel("GADCStart (ADC)", &Fei4GlobalCfg::ADC, row, col8);

  row = 12;
  addTitel("Analog Biases", row);

  row = 13;
  addLabel("PrmpVbp", &Fei4GlobalCfg::PrmpVbp, row, col1);
  addLabel("PrmpVbp_L", &Fei4GlobalCfg::PrmpVbp_L, row, col2);
  addLabel("PrmpVbp_R", &Fei4GlobalCfg::PrmpVbp_R, row, col3);
  addLabel("PrmpVbnFol", &Fei4GlobalCfg::PrmpVbnFol, row, col4);
  addLabel("PrmpVbpf", &Fei4GlobalCfg::PrmpVbpf, row, col5);
  addLabel("PrmpVbnLcc (PrmpVbnLCC)", &Fei4GlobalCfg::PrmpVbnLCC, row, col6);
  addLabel("Amp2Vbn", &Fei4GlobalCfg::Amp2Vbn, row, col7);
  addLabel("Amp2Vbp", &Fei4GlobalCfg::Amp2Vbp, row, col8);

  row = 14;
  addLabel("Amp2VbpFol", &Fei4GlobalCfg::Amp2VbpFol, row, col1);
  addLabel("Amp2Vbpff", &Fei4GlobalCfg::Amp2Vbpff, row, col2);
  addLabel("Vthin_Fine", &Fei4GlobalCfg::Vthin_Fine, row, col3);
  addLabel("Vthin_Coarse", &Fei4GlobalCfg::Vthin_Coarse, row, col4);
  addLabel("DisVbn", &Fei4GlobalCfg::DisVbn, row, col5);
  addLabel("TdacVbp (TDACVbp)", &Fei4GlobalCfg::TDACVbp, row, col6);
  addLabel("FdacVbn (FDACVbn)", &Fei4GlobalCfg::FDACVbn, row, col7);
  addLabel("GADCVref", &Fei4GlobalCfg::GADCVref, row, col8);

  row = 15;
  addLabel("GADCCompBias", &Fei4GlobalCfg::GADCCompBias, row, col1);
  addLabel("PlsrDacBias (PlsrDACbias)", &Fei4GlobalCfg::PlsrDACbias, row, col2);
  addLabel("PlsrDAC", &Fei4GlobalCfg::PlsrDAC, row, col3);
  addLabel("PlsrDelay", &Fei4GlobalCfg::PlsrDelay, row, col4);
  addLabel("PlsrldacRamp (PlsrIDACRamp)", &Fei4GlobalCfg::PlsrIDACRamp, row, col5);
  addLabel("PlsrRiseUpTau", &Fei4GlobalCfg::PlsrRiseUpTau, row, col6);
  addLabel("PlsrVgOPamp (PlsrVgOpAmp)", &Fei4GlobalCfg::PlsrVgOpAmp, row, col7);
  addLabel("BufVgOpAmp/GADCVref", &Fei4GlobalCfg::BufVgOpAmp, row, col8);

  row = 16;
  addLabel("LVDSDrvIref", &Fei4GlobalCfg::LVDSDrvIref, row, col1);
  addLabel("LVDSDrvVos", &Fei4GlobalCfg::LVDSDrvVos, row, col2);
  addLabel("LVDSDrvSet12 (LV1)", &Fei4GlobalCfg::LV1, row, col3);
  addLabel("LVDSDrvSet30 (LV3)", &Fei4GlobalCfg::LV3, row, col4);
  addLabel("LVDSDrvSet06 (LV0)", &Fei4GlobalCfg::LV0, row, col5);
  addLabel("PllIbias", &Fei4GlobalCfg::PllIbias, row, col6);
  addLabel("PllIcp", &Fei4GlobalCfg::PllIcp, row, col7);
  addLabel("TempSensIbias", &Fei4GlobalCfg::TempSensIbias, row, col8);
}
