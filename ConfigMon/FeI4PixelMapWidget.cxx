#include "FeI4PixelMapWidget.h"
#include <QGridLayout>
#include <QLabel>
#include <QVariant>
#include <random>
#include <QTableView>

#include <QChart>
#include <QSpacerItem>
#include <QChartView>
#include <QBarSet>
#include <QBarSeries>
#include <QBarCategoryAxis>
#include <QValueAxis>
#include <QComboBox>
#include <QPushButton>
#include <QLineEdit>

#include <QScrollArea>
#include <iostream>

using namespace QtCharts;

class PixFieldLabel : public QLabel {
public:
  int x = 0;
  int y = 0;
  
  static void getcolor(int p, int np, float&r, float&g, float&b) {
    int range1 = 0.25*(np+1);
    int range2 = 0.5*(np+1);
    int range3 = 0.75*(np+1);

    if(p<range1) {
      b = 1;
      r = 0;
      g = (float)p/range1;
    } else if ( p < range2 ) {
      g = 1;
      r = 0;
      b = 1 - float(p-range1)/range1; 
    } else if ( p < range3 ) {
      g = 1;
      b = 0;
      r = float(p-range2)/range1;
    } else {
      r = 1;
      b = 0;
      g = 1 - float(p-range3)/range1;
    }
    return;
 };

  PixFieldLabel(int value, int _x, int _y, int min, int max) : QLabel(QVariant(value).toString()), x(_x), y(_y) {
    setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    QPalette pal = palette();
    float r,g,b;
    getcolor(value-min, max-min, r, g, b);
    pal.setColor(backgroundRole(), QColor((int)(255*r),(int)(255*g),(int)(255*b),255));
    setPalette(pal);
    setAutoFillBackground(true);
    setToolTip(std::string(std::to_string(value)+" ("+std::to_string(x)+"/"+std::to_string(y)+")").c_str());
  };
};

FeI4PixelTDACMapWidget::FeI4PixelTDACMapWidget(Fei4PixelCfg * const cfg1, Fei4PixelCfg * const cfg2) {
  auto mainLayout = new QGridLayout(this);
  mainLayout->setSpacing(0);
  for(int col = 0; col < 80; col++) {
    mainLayout->setColumnMinimumWidth(col, 22); 
    for(int row = 0; row < 336; row++) {
      auto value = cfg1->TDAC(col/2).getPixel(row+1, col+1);
      if(cfg2 && (cfg2 != cfg1)) value -= cfg2->TDAC(col/2).getPixel(row+1, col+1);
      int min = (cfg2 && (cfg2 != cfg1)) ? -32 : 0;
      mainLayout->addWidget(new PixFieldLabel(value, row, col, min, 32), row, col);
    }
  }
}

FeI4PixelTDACHistoWidget::FeI4PixelTDACHistoWidget(Fei4PixelCfg * const cfg1, Fei4PixelCfg * const cfg2, int min, int max, bool diffMode) {
  std::vector<int> values_cfg1 (32, 0);
  std::vector<int> values_cfg2 (32, 0);
  
  if(diffMode) {
     values_cfg1.resize(64,0);
     for(int col = 0; col < 80; col++) {
       for(int row = 0; row < 336; row++) {
        auto value = cfg1->TDAC(col/2).getPixel(row+1, col+1)-cfg2->TDAC(col/2).getPixel(row+1, col+1);
        values_cfg1[value+31]++;
      }
     }   
  } else {
    for(int col = 0; col < 80; col++) {
      for(int row = 0; row < 336; row++) {
        auto value = cfg1->TDAC(col/2).getPixel(row+1, col+1);
        values_cfg1[value]++;
      }
    }
  
    if(cfg2 && (cfg2 != cfg1)) {
      for(int col = 0; col < 80; col++) {
        for(int row = 0; row < 336; row++) {
          auto value = cfg2->TDAC(col/2).getPixel(row+1, col+1);
          values_cfg2[value]++;
        }
      }
    }
  }

  auto dataSet_cfg1 = diffMode ? new QBarSet("deltaTDAC") : new QBarSet("TDAC1");
  auto dataSet_cfg2 = new QBarSet("TDAC2");
  for(auto const& i: values_cfg1) dataSet_cfg1->append(i);
  for(auto const& i: values_cfg2) dataSet_cfg2->append(i);

  auto series = new QBarSeries();
  series->append(dataSet_cfg1);
  if(!diffMode && cfg2 && (cfg2 != cfg1)) series->append(dataSet_cfg2);
  auto chart = new QChart();
  chart->addSeries(series);
  chart->setAnimationOptions(QChart::SeriesAnimations);
  auto axisX = new QBarCategoryAxis();
  chart->addAxis(axisX, Qt::AlignBottom);
  series->attachAxis(axisX);

  auto axisY = new QValueAxis();
  chart->addAxis(axisY, Qt::AlignLeft);
  series->attachAxis(axisY);
  setChart(chart);
}

FeI4PixelFDACHistoWidget::FeI4PixelFDACHistoWidget(Fei4PixelCfg * const cfg1, Fei4PixelCfg * const cfg2, int min, int max, bool diffMode) {
  std::vector<int> values_cfg1 (16, 0);
  std::vector<int> values_cfg2 (16, 0);
  
  if(diffMode) {
     values_cfg1.resize(32,0);
     for(int col = 0; col < 80; col++) {
       for(int row = 0; row < 336; row++) {
        auto value = cfg1->FDAC(col/2).getPixel(row+1, col+1)-cfg2->FDAC(col/2).getPixel(row+1, col+1);
        values_cfg1[value+15]++;
      }
     }   
  } else {
    for(int col = 0; col < 80; col++) {
      for(int row = 0; row < 336; row++) {
        auto value = cfg1->FDAC(col/2).getPixel(row+1, col+1);
        values_cfg1[value]++;
      }
    }
    if(cfg2 && (cfg2 != cfg1)) {
      for(int col = 0; col < 80; col++) {
        for(int row = 0; row < 336; row++) {
          auto value = cfg2->FDAC(col/2).getPixel(row+1, col+1);
          values_cfg2[value]++;
        }
      }
    }
  }

  auto dataSet_cfg1 = diffMode ? new QBarSet("deltaFDAC") : new QBarSet("FDAC1");
  auto dataSet_cfg2 = new QBarSet("FDAC2");
  for(auto const& i: values_cfg1) dataSet_cfg1->append(i);
  for(auto const& i: values_cfg2) dataSet_cfg2->append(i);

  auto series = new QBarSeries();
  series->append(dataSet_cfg1);
  if(!diffMode && cfg2 && (cfg2 != cfg1)) series->append(dataSet_cfg2);
  auto chart = new QChart();
  chart->addSeries(series);
  //chart->setTitle("Simple barchart example");
  chart->setAnimationOptions(QChart::SeriesAnimations);
//    QStringList categories;
//    categories << "Jan" << "Feb" << "Mar" << "Apr" << "May" << "Jun";
  auto axisX = new QBarCategoryAxis();
//    axisX->append(categories);
  chart->addAxis(axisX, Qt::AlignBottom);
  series->attachAxis(axisX);

  auto axisY = new QValueAxis();
  //axisY->setRange(0,15);
  chart->addAxis(axisY, Qt::AlignLeft);
  series->attachAxis(axisY);
  setChart(chart);
}

FeI4PixelFDACMapWidget::FeI4PixelFDACMapWidget(Fei4PixelCfg * const cfg1, Fei4PixelCfg * const cfg2) {
  auto mainLayout = new QGridLayout(this);
  mainLayout->setSpacing(0);
  //auto table = new QTableView();
  for(int col = 0; col < 80; col++) {
    mainLayout->setColumnMinimumWidth(col, 22); 
    for(int row = 0; row < 336; row++) {
      auto value = cfg1->FDAC(col/2).getPixel(row+1, col+1);
      if(cfg2 && (cfg2 != cfg1)) value -= cfg2->FDAC(col/2).getPixel(row+1, col+1);
      int min = (cfg2 && (cfg2 != cfg1)) ? -16 : 0;
      mainLayout->addWidget(new PixFieldLabel(value, row, col, min, 16), row, col);
    }
  }
}

FeI4PixelConfWidget::FeI4PixelConfWidget(Fei4PixelCfg * const cfg1, Fei4PixelCfg * const cfg2): mcfg1(cfg1), mcfg2(cfg2) {
  layout = new QVBoxLayout(this);
  auto top_bar = new QHBoxLayout();
  type = new QComboBox();
  diffMode = new QCheckBox();
  /*auto TDAC_limits_label = new QLabel("TDAC min/max");
  auto TDAC_min = new QLineEdit("0");
  auto TDAC_max = new QLineEdit("31");
  auto FDAC_limits_label = new QLabel("FDAC min/max");
  auto FDAC_min = new QLineEdit("0");
  auto FDAC_max = new QLineEdit("15");*/
  auto go_button = new QPushButton("Show");

  type->addItem("TDAC histo"); 
  type->addItem("FDAC histo");
  type->addItem("TDAC map"); 
  type->addItem("FDAC map"); 

  top_bar->addWidget(type); /*
  top_bar->addWidget(TDAC_limits_label); 
  top_bar->addWidget(TDAC_min); 
  top_bar->addWidget(TDAC_max); 
  top_bar->addWidget(FDAC_limits_label); 
  top_bar->addWidget(FDAC_min); 
  top_bar->addWidget(FDAC_max);*/ 
  top_bar->addItem(new QSpacerItem(200,1));
  top_bar->addWidget(new QLabel("Show delta in TDAC/FDAC histos: "));
  top_bar->addWidget(diffMode);
  top_bar->addItem(new QSpacerItem(700,1));
  top_bar->addWidget(go_button);

  //auto tab = new QTabWidget();
  //auto scroll_tdac = new QScrollArea();
  //auto scroll_fdac = new QScrollArea();
  widget = new FeI4PixelTDACHistoWidget(cfg1, cfg2, 0, 31, false);

  layout->addLayout(top_bar);
  layout->addWidget(widget);

  QObject::connect(go_button, &QPushButton::clicked, [&](){
	    if(widget) widget->deleteLater();
	    if(type->currentText() == "TDAC map") {
	      auto newWidget = new FeI4PixelTDACMapWidget(mcfg1, mcfg2);
	      auto scroll_area = new QScrollArea();
	      scroll_area->setWidget(newWidget);
	      layout->replaceWidget(widget, scroll_area);
	      scroll_area->show();
	      widget = scroll_area;
	    } else if (type->currentText() == "TDAC histo") {
	      auto newWidget = new FeI4PixelTDACHistoWidget(mcfg1, mcfg2, 0, 31, diffMode->isChecked());
	      layout->replaceWidget(widget, newWidget);
	      newWidget->show();
	      widget = newWidget;
	    } else if(type->currentText() == "FDAC map") {
	      auto newWidget = new FeI4PixelFDACMapWidget(mcfg1, mcfg2);
	      auto scroll_area = new QScrollArea();
	      scroll_area->setWidget(newWidget);
	      layout->replaceWidget(widget, scroll_area);
	      scroll_area->show();
	      widget = scroll_area;
	    } else if (type->currentText() == "FDAC histo") {
	      auto newWidget = new FeI4PixelFDACHistoWidget(mcfg1, mcfg2, 0, 15, diffMode->isChecked());
	      layout->replaceWidget(widget, newWidget);
	      newWidget->show();
	      widget = newWidget;
	    }

	});   
}
