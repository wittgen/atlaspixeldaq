cmake_minimum_required(VERSION 3.6.0)
project(atlaspixeldaq)
find_package(TDAQ)
include(CTest)
include(cmake/lmfit.cmake)
include(cmake/ppc.cmake)
include(cmake/mb.cmake)

link_directories  (${PROJECT_SOURCE_DIR}/installed/$ENV{CMTCONFIG}/lib)

add_compile_options(-Wshadow -Werror)

if(CLANGFIX)
  add_definitions(-D"_0RL_library_version='__attribute__((unused)) _0RL_library_version'")
endif()

if(ASAN)
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fno-omit-frame-pointer -fsanitize=address")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fno-omit-frame-pointer -fsanitize=address")
  set(CMAKE_LINKER_FLAGS "${CMAKE_LINKER_FLAGS} -fno-omit-frame-pointer -fsanitize=address -fsanitize=leak")
endif()

tdaq_work_area()


include( ./Testing/ctest.txt )


