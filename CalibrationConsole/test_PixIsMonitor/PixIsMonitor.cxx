#include "PixIsMonitor.h"
#include <ipc/core.h>

#include "IHistogramProvider.h"
#include <DataContainer/HistoList_t.h>

#include <memory>

#include <CalibrationDataManager.h>
#include <CalibrationDataStyle.h>
#include <CalibrationConsoleContextMenu.h>
#include <CategoryList.h>
#include <HistogramCache.h>

#include <IsMonitor.h>
#include <IsReceptor.h>
#include <DcsVariables.h>
#include <QIsInfoEvent.h>

#include <ScanMainPanel.h>

#include <FullDetectorLogoFactory.h>

namespace PixCon {


  /** Do nothing histogram server to make the context manu happy.
   */
  class DummyHistogramProvider : public IHistogramProvider
  {
  public:
    virtual HistoPtr_t loadHistogram(EMeasurementType /*type*/,
                                     SerialNumber_t /*serial_number*/,
                                     EConnItemType /*conn_item_type*/,
                                     const std::string &/*connectivity_name*/,
                                     const std::string &/*histo_name*/,
                                     const Index_t &/*index*/) { return NULL; }

    virtual void requestHistogramList(EMeasurementType /*type*/,
                                      SerialNumber_t /*serial_number*/,
                                      EConnItemType /*conn_item_type*/,
                                      std::vector< std::string > &histogram_name_list ) { histogram_name_list.clear();}

    virtual void requestHistogramList(EMeasurementType type,
				      SerialNumber_t serial_number,
				      EConnItemType conn_item_type,
				      std::vector< std::string > &histogram_name_list,
				      const std::string &/*rod_name*/) {
      requestHistogramList(type,serial_number, conn_item_type, histogram_name_list);
    }


    virtual void numberOfHistograms(EMeasurementType /*type*/,
                                    SerialNumber_t /*serial_number*/,
                                    EConnItemType /*conn_item_type*/,
                                    const std::string &/*connectivity_name*/,
                                    const std::string &/*histo_name*/,
                                    HistoInfo_t &info) { info.reset(); }

    virtual void reset(){};
  };


  class DcsVarNameExtractor : public IVarNameExtractor 
  {
  public:
    void extractVar(const std::string &full_is_name, ConnVar_t &conn_var) const
    {
      std::string::size_type conn_start = full_is_name.find(".");
      if (conn_start != std::string::npos) {
	std::string::size_type conn_end = full_is_name.rfind("_");
	if (conn_end != std::string::npos) {
	  conn_var.connName()=std::string(full_is_name,conn_start+1,conn_end-conn_start-1);
	  conn_var.varName()=std::string(full_is_name,conn_end+1,full_is_name.size()-conn_end-1);
	}
      }

    }

  };

  class PixLibVarNameExtractor : public IVarNameExtractor 
  {
  public:
    void extractVar(const std::string &full_is_name, ConnVar_t &conn_var) const
    {
      std::string::size_type conn_end = full_is_name.rfind("/");
      if (conn_end != std::string::npos && conn_end>0) {
	std::string::size_type conn_start = full_is_name.rfind("/",conn_end-1);
	if (conn_start != std::string::npos) {
	  conn_var.connName()=std::string(full_is_name,conn_start+1,conn_end-conn_start-1);
	  conn_var.varName()=std::string(full_is_name,conn_end+1,full_is_name.size()-conn_end-1);
	}
      }

    }

  };

  DcsVarNameExtractor s_dcsVarNameExtractor;
  PixLibVarNameExtractor s_pixLibVarNameExtractor;

  class test_MainPanel : public ScanMainPanel
  {
  public:
  
    test_MainPanel(std::shared_ptr<PixCon::UserMode>& user_mode,
		  const std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data,
                  const std::shared_ptr<PixCon::HistogramCache> &histogram_cache,
                  const std::shared_ptr<PixCon::CalibrationDataPalette> &palette,
                  const std::shared_ptr<PixCon::CategoryList> &categories,
                  const std::shared_ptr<PixCon::IContextMenu> &context_menu,
                  const std::shared_ptr<PixDet::ILogoFactory> &logo_factory,
                  const std::shared_ptr<VisualiserList> &visualiser_list,
                  QApplication &application,
                  QWidget* parent = 0)
    : ScanMainPanel(user_mode,
		    calibration_data,
		    histogram_cache,
		    palette,
		    categories,
		    context_menu,
		    std::shared_ptr<PixCon::IMetaDataUpdateHelper>(),
		    logo_factory,
		    visualiser_list,
		    application,
		    parent)
    {}

     void addMeasurement() { /*Nothing to do*/ }
  };

}


PixIsMonitor::PixIsMonitor(int qt_argc, char **qt_argv, const std::string &partition_name, const std::string &is_name)
  : QApplication(qt_argc, qt_argv),
    m_categories(new PixCon::CategoryList),
    m_calibrationData(new PixCon::CalibrationDataManager)
{
  // histogram server will be unused but the context menu wants one
  std::shared_ptr<PixCon::UserMode> user_mode(new PixCon::UserMode(PixCon::UserMode::kShifter));
  std::shared_ptr<PixCon::IHistogramProvider> dummy_hp( new PixCon::DummyHistogramProvider );
  std::shared_ptr<PixCon::HistogramCache> histogram_cache( new PixCon::HistogramCache( dummy_hp ) );

  std::shared_ptr<PixCon::CalibrationDataPalette> palette(new PixCon::CalibrationDataPalette);
  palette->setDefaultPalette();

  std::shared_ptr<PixCon::VisualiserList> visualiser_list(new PixCon::VisualiserList);
  std::shared_ptr<PixCon::IContextMenu> context_menu(  new PixCon::CalibrationConsoleContextMenu(m_calibrationData, histogram_cache, visualiser_list) );

  m_detectorView=std::unique_ptr<PixCon::ScanMainPanel>(new PixCon::test_MainPanel(user_mode, 
										   m_calibrationData,
										   histogram_cache,
										   palette,
										   m_categories,
										   context_menu,
                                           std::shared_ptr<PixDet::ILogoFactory>(new PixDet::FullDetectorLogoFactory),
										   visualiser_list,
										   *this) );
  setupIsMonitor(0,qt_argv, partition_name, is_name);

  m_detectorView->updateCategories(m_categories);
    //    detector_view_window.updateCategories(categories);
    //    detector_view_window.addCurrentSerialNumber();

  m_detectorView->show();
  connect( this, SIGNAL( lastWindowClosed() ), this, SLOT( quit() ) );
}

PixIsMonitor::~PixIsMonitor()
{}

void PixIsMonitor::setTags(const std::string &object_tag_name, const std::string &tag_name, const std::string &cfg_tag_name, const std::string &mod_cfg_tag_name)
{
  if (!object_tag_name.empty() && !tag_name.empty() && !cfg_tag_name.empty()) {
    m_detectorView->changeConnectivity(object_tag_name, tag_name,tag_name,tag_name,cfg_tag_name,mod_cfg_tag_name, 0);

    for (  std::vector<std::shared_ptr< PixCon::IsMonitorBase > >::const_iterator iter = m_isMonitorList->begin();
 	   iter != m_isMonitorList->end();
 	   iter++) {
      (*iter)->update();
    }
    
    m_detectorView->updateCategories(m_categories);
    
  }
}

void PixIsMonitor::setupIsMonitor(int ipc_argc, char **ipc_argv, const std::string &partition_name, const std::string &is_name)
{
  //  IPCCore::init(ipc_argc, ipc_argv);

  if (!partition_name.empty() && !is_name.empty()) {
    PixCon::CategoryList::Category dcs_cat=m_categories->addCategory("DCS");

    m_ipcPartition = std::unique_ptr<IPCPartition>( new IPCPartition(partition_name.c_str()) );
    m_infoReceiver = std::unique_ptr<ISInfoReceiver>( new ISInfoReceiver(*m_ipcPartition) );

    m_receptor = std::shared_ptr<PixCon::IsReceptor>(new PixCon::IsReceptor(this,
									      m_detectorView.get(),
									      is_name,
									      /* *m_infoReceiver, */
									      *m_ipcPartition,
	                                                                      m_calibrationData ));

    m_isMonitorList = std::unique_ptr< std::vector<std::shared_ptr< PixCon::IsMonitorBase > > > (new std::vector<std::shared_ptr< PixCon::IsMonitorBase > >());
    {

      std::string is_head(".*_");
      if (true) {
      for (std::vector<PixCon::DcsVariables::EVariable>::const_iterator module_var_iter = PixCon::DcsVariables::beginModuleVar();
	   module_var_iter != PixCon::DcsVariables::endModuleVar();
	   module_var_iter++) {
	m_isMonitorList->push_back( std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsValueMonitor(m_receptor,
													PixCon::kModuleValue,
													-1, 
													PixCon::s_dcsVarNameExtractor,
													is_head+PixCon::DcsVariables::varName(*module_var_iter))));
	dcs_cat.addVariable(PixCon::DcsVariables::varName(*module_var_iter));
      }
      
      {
	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createStateVarDef("AVAILABLE",PixCon::kRodValue,2);
	if (var_def.isStateWithOrWithoutHistory()) {
	  var_def.stateDef().addState(0,"Free");
	  var_def.stateDef().addState(1,"Allocated");
	}
	dcs_cat.addVariable("AVAILABLE");
      }

      m_isMonitorList->push_back( std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntStateMonitor(m_receptor,
													 PixCon::kRodValue,
													 -1,
													 PixCon::s_pixLibVarNameExtractor,
													 ".*ROD_.*/AVAILABLE")));

      }

      enum ERodStateVar {kRODBusy, kROS, kNRodStates};

      const char *rod_state_name[kNRodStates]={
	"RODBusy",
	"ROSBuffer",
       };
 
      {
	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createStateVarDef(rod_state_name[kRODBusy],PixCon::kRodValue,2);
	if (var_def.isStateWithOrWithoutHistory() ) {
	  var_def.stateDef().addState(0,"Ok");
	  var_def.stateDef().addState(1,"Busy");
	}
      }

      {
	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createStateVarDef(rod_state_name[kROS],PixCon::kRodValue,3);
	if (var_def.isStateWithOrWithoutHistory() ) {
	  var_def.stateDef().addState(0,"Ok");
	  var_def.stateDef().addState(1,"Full");
	  var_def.stateDef().addState(2,"Flooded");
	}
      }

      PixCon::CategoryList::Category daq_cat=m_categories->addCategory("DAQ");

      for (unsigned int state_i=0; state_i< kNRodStates; state_i++) {
	m_isMonitorList->push_back( std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntStateMonitor(m_receptor,
 													   PixCon::kRodValue,
													   -1,
 													   PixCon::s_dcsVarNameExtractor,
 													   is_head+rod_state_name[state_i])));
 	daq_cat.addVariable(rod_state_name[state_i]);
       }

    }
  }
}
