#include <QApplication>
//#include <QWindowsStyle>
#include "PixIsMonitor.h"
#include <ConfigWrapper/ConnectivityManager.h>
#include <ConfigWrapper/createDbServer.h>
#include <memory>
#include <memory>

int main( int argc, char ** argv )
{
    QApplication::setStyle("fusion");
    //new QWindowsStyle());

    std::string partition_name;
    std::string object_tag_name="CT";
    std::string db_server_partition_name;
    std::string db_server_name;
    std::string tag_name;
    std::string cfg_tag_name;
    std::string mod_cfg_tag_name;
    std::string is_name = "PixFakeData";
    bool error=false;

    std::vector<std::string> result_files;
    IPCCore::init(argc, argv);

    for (int arg_i=1; arg_i<argc; arg_i++) {
      if (strcmp(argv[arg_i],"-p")==0 && arg_i+1<argc) {
	partition_name = argv[++arg_i];
      }
      else if (strcmp(argv[arg_i],"-i")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	object_tag_name = argv[++arg_i];
      }
      else if (strcmp(argv[arg_i],"-t")==0 && arg_i+2<argc && argv[arg_i+2][0]!='-' && argv[arg_i+1][0]!='-') {
	tag_name = argv[++arg_i];
	cfg_tag_name = argv[++arg_i];
	if (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	  mod_cfg_tag_name = argv[++arg_i];
	}
      }
      else if ( (strcmp(argv[arg_i],"-D")==0
		 || strcmp(argv[arg_i],"--db-server-name")==0)
		&& arg_i+1<argc && argv[arg_i+1][0]!='-') {
	db_server_name = argv[++arg_i];
      }
      else if ( (strcmp(argv[arg_i],"--db-server-partition")==0
		 || strcmp(argv[arg_i],"-p")==0)
		&& arg_i+1<argc && argv[arg_i+1][0]!='-') {
	db_server_partition_name = argv[++arg_i];
      }
      else if (strcmp(argv[arg_i],"-n")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	is_name = argv[++arg_i];
      }
      else {
	std::cout << "ERROR ["<< argv[0] << ":main] unhandled argument " << arg_i << " : " << argv[arg_i]<< "." << std::endl;
	error=true;
      }
    }

    if (error ) {
      std::cout << "usage: " << argv[0] << "[-p partition]  [-n \"IS server name\"]" << std::endl;
      return -1;
    }

    std::unique_ptr<QApplication> app;

    if (db_server_partition_name.empty() && !partition_name.empty()) {
      db_server_partition_name = partition_name;
    }

    std::unique_ptr<IPCPartition> db_server_partition;
    if (!db_server_name.empty() && !db_server_partition_name.empty()) {
      db_server_partition = std::unique_ptr<IPCPartition>(new IPCPartition(db_server_partition_name.c_str()));
      if (!db_server_partition->isValid()) {
	std::cerr << "ERROR [" << argv[0] << ":main] Not a valid partition " << db_server_partition_name
		  << " for the db server." << std::endl;
	return EXIT_FAILURE;
      }

      std::shared_ptr<PixLib::PixDbServerInterface> db_server( PixA::createDbServer(*db_server_partition,
										      db_server_name ));
      if (db_server.get()) {
	PixA::ConnectivityManager::useDbServer( db_server );
      }
      else {
	std::cout << "WARNING [main:" << argv[0] << "] Failed to create the db server." << std::endl;
      }
    }


    {
      PixIsMonitor *pix_is_monitor = new PixIsMonitor(1,argv,partition_name, is_name);
      app=std::unique_ptr<QApplication>(pix_is_monitor);
      if (!object_tag_name.empty() && !tag_name.empty() && !cfg_tag_name.empty()) {
	pix_is_monitor->setTags(object_tag_name,tag_name,cfg_tag_name,mod_cfg_tag_name);
      }
    }

    int ret  = (app.get() ? app->exec() : -1);
    return ret;
}
