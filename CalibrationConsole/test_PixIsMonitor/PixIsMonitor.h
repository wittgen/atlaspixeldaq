// Dear emacs, this is -*-c++-*-
#ifndef _PixIsMonitor_H_
#define _PixIsMonitor_H_

#include <memory>
#include <vector>
#include <string>

#include <Lock.h>

#include <ipc/partition.h>
#include <is/inforeceiver.h>
#include <QApplication>

namespace PixCon {
  class ScanMainPanel;
  class CalibrationDataManager;
  class CategoryList;
  class IsReceptor;
  class IsMonitorBase;
}

class PixIsMonitor : public QApplication
{
public:
  PixIsMonitor(int qt_argc, char **qt_argv, const std::string &partition_name, const std::string &is_name);
  ~PixIsMonitor();

  void setTags(const std::string &object_tag, const std::string &tag_name, const std::string &cfg_tag_name, const std::string &mod_cfg_tag_name);

protected:

  void setupIsMonitor(int ipc_argc, char **ipc_argv, const std::string &partition_name, const std::string &is_name);

  std::unique_ptr<PixCon::ScanMainPanel>              m_detectorView;
  std::shared_ptr<PixCon::CategoryList>           m_categories;
  std::shared_ptr<PixCon::CalibrationDataManager> m_calibrationData;

  std::unique_ptr<IPCPartition>           m_ipcPartition;
  std::unique_ptr<ISInfoReceiver>         m_infoReceiver;
  std::shared_ptr<PixCon::IsReceptor> m_receptor;
  std::unique_ptr< std::vector<std::shared_ptr< PixCon::IsMonitorBase > > > m_isMonitorList;

private:

};
#endif
