# SRC = all source files
# top = contains the path name of the PixLib top level directory (needed ?)
# ALL_SRC = all source files plus automatically generated sourcefiles e.g. those generated by omniidl or rootcint
# CPPFLAGS = preprocessor flags e.g. defines include paths
# CFLAGS = compile flags e.g. debug or optimisation flags
# LFLAGS = linker flags e.g. additional libraries and search paths 
# SOLFLAGS = additional flags to link shared libraries
# OBJ = all object files which should be compiled is derived from ALL_SRC

ifeq ($(origin BOOST_VERSION_HEADERS), undefined)
	BOOST_VERSION_HEADERS="-gcc"
else
	LFLAGS += -L$(BOOSTLIB) -lboost_regex$(BOOST_VERSION_HEADERS)
endif

# avoid creating dependencies on make clean
ifneq ($(MAKECMDGOALS),clean)
  dependence = $(patsubst %.cxx,.depend/%.d,$(SRC))
endif

include $(PIX_LIB)/PixLib.mk

ifeq ($(origin CPPFLAGS), undefined)
  CPPFLAGS := $(CFLAGS)
  CFLAGS = $(CPPFLAGS)
endif

LFLAGS := $(LFLAGS) -L$(QTDIR)/lib 
SOLFLAGS = -shared
SOLFLAGS += $(shell root-config --ldflags)

INSTALL := install
INSTALL_PROGRAM = $(INSTALL)
INSTALL_DATA = $(INSTALL) -m644

OBJ = $(addsuffix .o, $(basename $(ALL_SRC)))

.SUFFIXES: .cxx .cc .h .o .so .a  .hh .d

.cxx.o:
	$(CPP) $(CFLAGS) -c $<

install_bin:
	test -n "$(PIX_INSTALL)"
	test -n "$(CMTCONFIG)"
	mkdir -p $(PIX_INSTALL)/$(CMTCONFIG)/lib
	test -z "$(INST_LIBS)" || ( mkdir -p $(PIX_INSTALL)/$(CMTCONFIG)/lib && $(INSTALL_PROGRAM) $(INST_LIBS) $(PIX_INSTALL)/$(CMTCONFIG)/lib )
	test -z "$(INST_APPS)" || ( mkdir -p $(PIX_INSTALL)/$(CMTCONFIG)/bin && $(INSTALL_PROGRAM) $(INST_APPS)  $(PIX_INSTALL)/$(CMTCONFIG)/bin )

install_data:
	test -n "$(PIX_INSTALL)"
	mkdir -p $(PIX_INSTALL)/shared/Karl
	test -z "$(INST_DATA)" || ( mkdir -p $(PIX_INSTALL)/shared/Karl && $(INSTALL_DATA) $(INST_DATA) $(PIX_INSTALL)/shared/Karl )

clean_depend:
	rm -f .depend/*.d

.depend/%.d:  %.cxx $(ALL_IDL_HH)
	[ -d .depend ] || mkdir -p .depend || [ -d .depend ]
	$(CPP) -MM -I$(CMTCONFIG) $(CPPFLAGS) $< | sed -e 's/\($*\)\.o[ :]*/\1.o $*.d : /g' > $@; \
	[ -s $@ ] || (rm -f $@; exit 1)
