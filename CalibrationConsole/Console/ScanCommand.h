#ifndef _PixCon_ScanCommand_h_
#define _PixCon_ScanCommand_h_

#include "CommandKits.h"
#include <memory>

namespace PixCon {

  class ConsoleEngine;
  
  ICommand *scanCommand(const std::shared_ptr<ConsoleEngine> &engine, const std::string &scan_type_name);

}

#endif
