#include "test_DummyHistoInput.h"
#include <TFile.h>
#include <TDirectory.h>
#include <TClass.h>
#include <TKey.h>
#include <TList.h>
#include <TH1.h>
#include <TROOT.h>

#include <iostream>
#include <sstream>

#include <ConfigWrapper/ConfigHandle.h>
#include <ConfigWrapper/ConfigRef.h>
#include <ConfigWrapper/ConfObjUtil.h>
namespace PixCon {


  class RestoreDir
  {
  public:
    RestoreDir() : m_file(gFile), m_dir(gDirectory) {}
    ~RestoreDir() { gFile=m_file; m_dir=gDirectory; }
  private:
    TFile      *m_file;
    TDirectory *m_dir;
  };

  const char *DummyHistoInput::s_modeNames[kNModes] = {"READ", "UPDATE", "CREATE"};
  const char *DummyHistoInput::s_qualityNames[kNModes] = {"Ok", "StillOk", "Failure" };

  DummyHistoInput::DummyHistoInput(const std::string &file_name, EMode mode) 
  {
    if (!file_name.empty() && mode <= kNModes) {
      m_file=std::unique_ptr<TFile>(new TFile(file_name.c_str(), s_modeNames[mode]));
      if (mode==kUpdate && !m_file->IsWritable()) {
	m_file.reset();
	m_file=std::unique_ptr<TFile>(new TFile(file_name.c_str(), s_modeNames[kCreate]));
      }
    }
  }

  DummyHistoInput::~DummyHistoInput() {
  }

  void DummyHistoInput::add(const std::string &histo_name, const PixA::ConfigHandle &scan_config_handle, EQuality quality, const PixA::Index_t &index, TH1 *histo)
  {
    if (!histo || index.empty() || !m_file.get()) return;
    TDirectory *dest_dir = dir(histo_name, scan_config_handle, quality, true);
    if (!dest_dir) return;
    std::string full_histo_name = makeName(histo_name, quality, index);

    RestoreDir restore;
    dest_dir->cd();
    histo->Write(full_histo_name.c_str());
  }

  void DummyHistoInput::numberOfHistograms(const std::string &histo_name, const PixA::ConfigHandle &scan_config_handle, EQuality quality, HistoInfo_t &histo_info)
  {
    histo_info.reset();

    if (!m_file.get()) return;

    TDirectory *the_dir = dir(histo_name, scan_config_handle, quality, false);
    if (!the_dir) return;

    TListIter iter( the_dir->GetListOfKeys() );
    TObject *obj;
    std::string last_class_name;
    bool is_th1 = false;
    std::string histo_name_head = makeName(histo_name, quality, PixA::Index_t());
    while ( (obj = iter.Next()) ) {

      // should always be a TKey
      if (!obj->InheritsFrom(TKey::Class())) continue;

      TKey *key = static_cast<TKey *>(obj);

      if (key->GetClassName() != last_class_name) {
	TObject *a_class_obj = gROOT->GetListOfClasses()->FindObject(key->GetClassName());
	is_th1=false;
	//	if (a_class_obj->InheritsFrom(TClass::Class())) {
	TClass *a_class=static_cast<TClass *>(a_class_obj);
	if (a_class->GetBaseClass(key->GetClassName()) ) {
	  is_th1 = true;
	}
	//}
	last_class_name = key->GetClassName();
      }
      if (is_th1) {
	std::string root_histo_name = key->GetName();
	if (root_histo_name.compare(0,histo_name_head.size(),histo_name_head)==0) {
	  std::string::size_type pos = histo_name_head.size();
	  char letter='A';
	  bool is_last=false;
	  PixA::Index_t  index;
	  while (pos+1 < root_histo_name.size() && root_histo_name[pos]=='_' && !is_last) {
	    pos++;
	    if (root_histo_name.c_str()[pos]==letter) {
	      pos++;
	      letter++;
	    }
	    else {
	      is_last=true;
	    }
	    if (!isdigit(root_histo_name.c_str()[pos])) {
	      --pos;
	      break;
	    }

	    index.push_back(  atoi(&(root_histo_name.c_str()[pos])) );
	    do {
	      pos++;
	    } while (isdigit(root_histo_name[pos]));
	  }
	  bool error=true;
	  if ((pos>=root_histo_name.size() || root_histo_name[pos]==';') && index.size()>0 && index.size()<=4) {
	    if (histo_info.minHistoIndex()<histo_info.maxHistoIndex()) {
	      bool new_level=false;
	      for (unsigned int i=0; i<index.size()-1; i++) {
		if (histo_info.maxIndex(i)==0) {
		  new_level=true;
		  break;
		}
	      }
	      if (!new_level && (index.size()==4 || histo_info.maxIndex(index.size()-1)==0)) {
		for (unsigned int i=0; i<index.size()-1; i++) {
		  if (histo_info.maxIndex(i)<index[i]+1) {
		    histo_info.setMaxIndex(i,index[i]+1);
		  }
		}
		if (histo_info.maxHistoIndex()<index.back()+1) {
		  histo_info.setMaxHistoIndex(index.back()+1);
		}
		if (histo_info.minHistoIndex()>index.back()) {
		  histo_info.setMinHistoIndex(index.back());
		}
		error=false;
	      }
	    }
	    else {
	      for (unsigned int i=0; i<index.size()-1; i++) {
		histo_info.setMaxIndex(i,index[i]+1);
	      }
	      histo_info.setMaxHistoIndex(index.back()+1);
	      histo_info.setMinHistoIndex(index.back());
	      error=false;
	    }
	  }
	  if (error) {
	    std::cout << "ERROR [DummyHistoInput::numberOfHistograms] Failed to parse name : " << root_histo_name << "." << std::endl;
	  }
	}
      }
    }
  }

  TH1 *DummyHistoInput::histo(const std::string &histo_name, const PixA::ConfigHandle &scan_config_handle, EQuality quality, const PixA::Index_t &index)
  {
    if (!m_file.get()) return NULL;

    TDirectory *the_dir = dir(histo_name, scan_config_handle, quality, false);
    if (!the_dir) return NULL;
    std::string full_histo_name = makeName(histo_name, quality, index);
    TObject *obj = the_dir->Get(full_histo_name.c_str());
    if (!obj || !obj->InheritsFrom(TH1::Class())) return NULL;
    TH1 *a_his = static_cast<TH1 *>(obj);
    a_his->SetDirectory(0);
    return a_his;
  }

  TDirectory *DummyHistoInput::dir(const std::string &histo_name, const PixA::ConfigHandle &scan_config_handle, EQuality quality, bool create)
  {
    if (quality>= kNQualityLevels) return NULL;
    if (!m_file.get()) return NULL;

    TObject  *his_dir_obj =m_file->Get(histo_name.c_str());
    if (his_dir_obj && !his_dir_obj->InheritsFrom(TDirectory::Class())) {
      throw AccessError("Not a directory.");
    }
    TDirectory *his_dir = static_cast<TDirectory *>(his_dir_obj);
    if (!his_dir) {
      if (!create) return NULL;
      his_dir = m_file->mkdir(histo_name.c_str());
      if (!his_dir) {
	throw WriteError("Cannot create TDirectory.");
      }
    }

    std::string config_name = makeConfigName(scan_config_handle);

    TObject  *config_dir_obj =his_dir->Get(config_name.c_str());
    if (config_dir_obj && !config_dir_obj->InheritsFrom(TDirectory::Class())) {
      throw AccessError("Not a directory.");
    }
    TDirectory *config_dir = static_cast<TDirectory *>(config_dir_obj);
    if (!config_dir) {
      if (!create) return NULL;
      config_dir = his_dir->mkdir(config_name.c_str());
      if (!config_dir) {
	throw WriteError("Cannot create TDirectory.");
      }
    }


    TObject  *quality_dir_obj =config_dir->Get(s_qualityNames[quality]);
    if (quality_dir_obj && !quality_dir_obj->InheritsFrom(TDirectory::Class())) {
      throw AccessError("Not a directory.");
    }
    TDirectory *quality_dir = static_cast<TDirectory *>(quality_dir_obj);
    if (!quality_dir) {
      if (!create) return NULL;
      quality_dir = config_dir->mkdir(s_qualityNames[quality]);
      if (!quality_dir) {
	throw WriteError("Cannot create TDirectory.");
      }
    }

    return quality_dir;
  }

  std::string DummyHistoInput::makeName(const std::string &histo_name, EQuality quality, const PixA::Index_t &index)
  {
    assert(quality < DummyHistoInput::kNQualityLevels);

    std::stringstream full_histo_name;
    full_histo_name << histo_name << "_" << DummyHistoInput::s_qualityNames[quality];
    char letter='A';
    for (PixA::Index_t::const_iterator iter = index.begin();
	 iter!=index.end();
	 iter++) {
      full_histo_name << '_';
      if (index.end()-iter > 1) {
	full_histo_name << letter;
	letter++;
      }
      full_histo_name << *iter;
    }
    return full_histo_name.str();
  }

  std::string DummyHistoInput::makeConfigName(const PixA::ConfigHandle &scan_config_handle) {
    std::stringstream config_name;
    if (scan_config_handle) {
      try {
	PixA::ConfigRef pix_scan_config = scan_config_handle.ref();
	PixA::ConfGrpRef general_config( pix_scan_config["general"] );

	unsigned int mask_stage_steps = confVal<int>(general_config["maskStageSteps"]);
	std::string total_mask_stage_steps = confVal<std::string>(general_config["maskStageTotalSteps"]);
	config_name << total_mask_stage_steps << "_m" << mask_stage_steps;

	const PixA::ConfGrpRef &loop_config(pix_scan_config["loops"]);

	for (unsigned int loop_i=3; loop_i-->0;) {
	  {
	    std::stringstream loop_idx;
	    loop_idx << loop_i;

	    if (!confVal<bool>(loop_config[std::string("activeLoop_")+loop_idx.str()])) continue;

	    std::string param_name = confVal<std::string>(loop_config[std::string("paramLoop_")+loop_idx.str()]);
	    bool really_uniform = confVal<bool>(loop_config[std::string("loopVarUniformLoop_")+loop_idx.str()]);
	    bool uniform = really_uniform;
	    unsigned int n_steps=0;
	    if (!uniform) {
	      const std::vector<float> &values ( confVal<std::vector<float> >(loop_config[std::string("loopVarValuesLoop_")+loop_idx.str()]));
	      n_steps = values.size();
	    }
	    else {
	      n_steps   =confVal<int>(loop_config[std::string("loopVarNStepsLoop_")+loop_idx.str()]);
	    }
	    config_name << "_l" <<loop_i << "_" << param_name << "_s" << n_steps;
	  }
	}
      }
      catch (PixA::ConfigException &) {
      }
    }
    else {
      config_name << "unconfigured";
    }
    return config_name.str();
  }


}
