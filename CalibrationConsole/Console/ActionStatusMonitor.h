/* Dear emacs, this is -*-c++-*- */
#ifndef _PixCon_ActionStatusMonitor_h_
#define _PixCon_ActionStatusMonitor_h_

#include <Flag.h>
#include <IStatusMonitor.h>
#include "VarDefList.h"
#include "DoNothingExtractor.h"
#include <memory>

class IPCPartition;
class ISInfoReceiver;

class IsReceptor;

namespace PixCon {

  class CalibrationDataManager;
  class StatusChangeTransceiver;

  /** Monitors the progress of pix actions.
   * The monitor is started with a list of actions which are expected to get executed in 
   * the given sequence  e.g "reset", "loadConfig" for the action "initCal". The action
   * is considered to have terminated if the status of one of these methods is "FAILED" 
   * or if the sequence of the actions was violated.
   */
  class ActionStatusMonitor : public IStatusMonitor
  {
  public:
    ActionStatusMonitor(const std::shared_ptr<IsReceptor> &receptor,
			const std::shared_ptr<CalibrationDataManager> &calibration_data,
			const std::shared_ptr<StatusChangeTransceiver> &status_change_transceiver);

    ~ActionStatusMonitor();

    /** Process the accumulated status information of a monitor.
     */
    void process() ;

    /** Called if no monitor anounced changes to the main monitoring thread within a given amount of time.
     */
    void timeout(double elapsed_seconds);


    void setStatusChangeFlag( Flag &the_flag ) { m_statusChangeFlag = &the_flag; }

    /** Activate monitoring of an action sequence which .
     * @param expected_action_sequence the expected sequence of actions.
     * The actions are expected to be executed in the given order. The action
     * sequence get the status "FAILED" If the sequece is violated or if at
     * least one action got the status "FAILED".
     */
    bool monitorAction(const std::vector<std::string> &expected_action_sequence);

    /** Abort the monitoring of the current action.
     */
    void abortMonitoring() ;

    /** Return if an action sequence is currently monitored.
     */
    bool isMonitoring() const { return m_isMonitoring;}

  protected:

    /** Reread the values from IS.
     */
    void update();

    /** Cancel IS subscription.
     */
    void unsubscribe();

    /** Stop monitoring and set status of all RODs which are not yet done to unknown.
     */
    void stopMonitoring();

    /** Update the type of the currently running action for the ROD which is given by the IS variable.
     */
    void updateAction(const std::string &is_name, const std::string &action_name);

    /** Update teh status of the currently running action for the ROD which is given by the IS variable.
     */
    void updateStatus(const std::string &is_name, const std::string &status_name);

    /** extract the ROD name from the given is variable name.
     */
    static std::string extract(const std::string &is_name);

    void statusChanged(ISCallbackInfo *info);
    void actionChanged(ISCallbackInfo *info);

    std::shared_ptr<CalibrationDataManager> &calibrationData() { return m_calibrationData; }


  private:
    std::shared_ptr<IsReceptor> m_receptor;       /**< The receptor which receives the values from IS.*/
    std::shared_ptr<CalibrationDataManager> m_calibrationData;      /**< Pointer to the calibration data manager.*/
    DoNothingExtractor  m_extractor;
    Flag               *m_statusChangeFlag;
    std::shared_ptr<StatusChangeTransceiver> m_statusChangeTransceiver;/**< Auxiliary class to notify listeners about status changes.*/

    enum ERodStatus { kUnknown, kRunning, kSuccess, kFailed, kNStates, kUnset};

    /** The state of the currently running actions for a ROD.
     */
    class RodState_t {
    public:
      RodState_t() : m_status(kUnset), m_currentAction(s_unknownCurrentAction), m_state(s_unknownCurrentAction), m_isDone(false) {}
      RodState_t(ERodStatus status, unsigned int state) : m_status(status), m_currentAction(state), m_state(state), m_isDone(false) {}

      /** Considere the status of the given state to build the combined state of the action sequence.
       * The status will degrade from success to failure. The status becomes failure
       * if the new state is not the successor of the last given state or if
       * the status of the new status is failure.
       */
      void setState( unsigned int new_state, bool success) {
	if (new_state==0 && success) {
	  m_status=kRunning;
	  m_state=0;
	}
	else if (!success) {
	  m_status=kFailed;
	}
	else if (new_state!=m_state+1) {
	  m_status=kFailed;
	}
	m_state=new_state;
      }

      /** change the status of the current state.
       */
      void updateState( bool success) {
	unsigned int new_state = m_currentAction;
	if (m_state == new_state && success) return;
	if (new_state==0 && success) {
	  m_status=kRunning;
	  m_state=0;
	}
	else if (!success) {
	  m_status=kFailed;
	}
	else if (new_state!=m_state+1) {
	  m_status=kFailed;
	}
	m_state=new_state;
      }

      /** Change the status to unknown unless it is failed.
       */
      void setUnknown()  { if (m_status != kFailed) m_status = kUnknown; }

      /** Return the combined status.
       */
      ERodStatus status() const          { return m_status; }

      /** Return the latest state that terminated.
       */
      unsigned int state() const         { return m_state; }

      /** Return the current state.
       */
      unsigned int currentAction() const { return m_currentAction; }

      /** Mark the action sequence as done.
       */
      void setDone()                     { if (m_status==kRunning) { m_status =kSuccess; } m_isDone=true; }

      /** Return true if the action sequence is terminated.
       * The action sequence is considered to be terminated if
       * one method of the sequn
       */
      bool isDone() const                { return status()==kFailed || m_isDone; }

      /** Set the current action.
       */
      void setCurrentAction( unsigned int current_action) { m_currentAction=current_action; }

      /** Set the current action to unknwon e.g. it is not part of the expected action sequence
       */
      void setCurrentActionUnknown( )                     { m_currentAction=s_unknownCurrentAction; }

      /** Return true if the current action is unknown.
       */
      bool currentActionIsUnknown( ) const                { return m_currentAction == s_unknownCurrentAction; }

    private:
      ERodStatus   m_status;
      unsigned int m_currentAction;
      unsigned int m_state;
      bool         m_isDone;
      static const unsigned int s_unknownCurrentAction = 0-1;
    };


    enum EActionStatus {kActionStatusUnknown, kActionProcessing, kActionSuccess, kActionFailed}; /**< Possible action statii. */

    Mutex                             m_rodStatusMutex;                     /**< Mutex to protect the rod status list.*/
    std::map<std::string, RodState_t> m_rodStatus;                          /**< The action satus per ROD.*/
    std::vector<std::string>          m_expectedActionSequence;

    static std::map<std::string, EActionStatus> s_actionStatusNames;        /**< Action status names in IS. */
    static const std::string                    s_rodStatusVarName;         /**< The name of the ROD status in the calibration data
									         container. */
    unsigned int m_doneCounter;
    unsigned int m_lastDoneCounter;

    bool m_isMonitoring;
    bool m_verbose;  // for debugging
  };

}
#endif
