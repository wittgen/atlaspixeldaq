#include "ExitHandler.h"

//#include <sys/types.h>
// #include <unistd.h>
#include <iostream>

namespace PixCon {

  ExitHandler *ExitHandler::s_instance=NULL;


  AutoExitHelper::~AutoExitHelper()
  {
    ExitHandler::instance()->removeHelper(this);
  }

  void ExitHandler::addHelper(IExitHelper *helper) {
    m_exitHelper.push_back( helper );
  }

  void ExitHandler::removeHelper(IExitHelper *helper) 
  {
    for (std::vector<IExitHelper *>::iterator exit_helper = m_exitHelper.begin();
	 exit_helper != m_exitHelper.end();
	 ++exit_helper) {

      if (*exit_helper == helper ) {
	//	IExitHelper *an_exit_helper = *exit_helper;
	m_exitHelper.erase(exit_helper);
	return;
      }
    }
  }

  int ExitHandler::installHandler( std::vector<int> signal_list) 
  {

    struct sigaction sa;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    sa.sa_handler = &ExitHandler::exitHandler;

    for (std::vector<int>::const_iterator signal_iter = signal_list.begin();
	 signal_iter != signal_list.end();
	 signal_iter++) {
      std::cout << "INFO [Exithandler::installHandler] Install exit handler for signal " << *signal_iter << std::endl;
      int ret = installHandler(*signal_iter, sa);
      if (ret !=0 ) {
	std::cerr << "FATAL [ConsoleEngine::installExitHandler] Failed to install the exit handler which ensures deallocation of actions." << std::endl;
	exit(-1);
      }
    }
    m_installed=true;
    return 0;
  }

  void ExitHandler::restoreHandler()
  {
    std::map<int, struct sigaction>::iterator handler_iter =  m_oldHandler.begin();
    bool reached_end = (handler_iter == m_oldHandler.end() );
    // @todo should block signals
    while (!reached_end) {

      std::map<int, struct sigaction>::iterator current_handler_iter = handler_iter++;
      reached_end = (handler_iter == m_oldHandler.end() );

      sigaction( current_handler_iter->first, &current_handler_iter->second, NULL);
      m_oldHandler.erase(current_handler_iter);

    }
  }

  int ExitHandler::installHandler( int signal, struct sigaction &sa)
  {
    struct sigaction temp;
    std::pair< std::map<int, struct sigaction>::iterator, bool>
      old_handler = m_oldHandler.insert(std::make_pair(signal, temp));

    return sigaction( signal, &sa, (old_handler.second  ? &(old_handler.first->second) : NULL));
  }

  void ExitHandler::exitHandler(int signum)
  {
    instance()->freeResources(signum);
  }


  void ExitHandler::freeResources(int signum)
  {
    std::cout << "INFO [Exithandler::freeResources] Caught signal " << signum << " in process " << getpid() << "(=?" << m_pid << ")." << std::endl;

    if(getpid() == m_pid){

      if (m_counter--==0) {
	restoreHandler();
      }

      std::cout << "INFO [Exithandler::freeResources] free resources." << std::endl;

      {
	std::vector<IExitHelper *>::iterator exit_helper = m_exitHelper.begin();
	bool reached_end = (exit_helper == m_exitHelper.end() );
	while (!reached_end) {
	  // @todo locking? 
	  std::vector<IExitHelper *>::iterator current_exit_helper = exit_helper++;
	  reached_end = (exit_helper == m_exitHelper.end() );

	  IExitHelper *an_exit_helper = *current_exit_helper;
	  m_exitHelper.erase(current_exit_helper);
	  an_exit_helper->freeResources();
	}
      }

    }
    if(signum == 0){
      exit(0);
    }else{
      exit(1);
    }
  }


}
