#include "ActionCommandBase.h"
#include <PixActions/PixActionsMulti.h>
#include "IStatusMonitorActivator.h"
#include "ConsoleEngine.h"

namespace PixCon {

  ActionCommandBase::~ActionCommandBase() { }

  PixCon::Result_t ActionCommandBase::execute(IStatusMessageListener &/*listener*/)
  {
    PixCon::Result_t ret;
    //      m_engine->initialiseRods(m_uninitialisedRodsOnly);
    //       /* ConsoleEngine::EActionStatus status = */ m_engine->waitForCurrentAction(m_abort);

    try {
      bool status = false;

      {

	// action sequence called by initCal
	std::vector<std::string> action_sequence;
	setActionSequence(action_sequence);
	for (std::vector<std::string>::const_iterator action_iter = action_sequence.begin();
	     action_iter != action_sequence.end();
	     action_iter++) {
	  std::cout << "INFO [ActionCommandBase::execute] "  << *action_iter << std::endl;
	}

	Lock action_lock(m_engine->multiActionMutex());
	if (m_engine->haveAllocatedActions()) {

	// reset the action values in IS
	// @todo should verify before that no action monitor is active. Probably should move the
	// the reset code to the action monitor itself
	m_engine->resetActionIsStatus();
	if (!m_engine->statusMonitorActivator()->activateActionMonitoring( action_sequence )) {
	  ret.setFailure();
	  return ret;
	}
	else {
	  // the engine will throw an exception if there is no multi action.
	  // Atlernatively, the engine could return a pointer ?
	  PixLib::PixActionsMulti &action = m_engine->multiAction();
	  status = callAction(action);

	  m_engine->waitForAction( m_abort );
	}
	}
	else {
	  // status will remain failed.
	}
      }

      if (m_abort) {
	ret.setAborted();
      }
      else {
	if (status) {
	  // get status from action
	  ret.setSuccess();
	}
	else {
	  ret.setFailure();
	}
      }
    }
    catch(...) {
      ret.setFailure();
    }

    return ret;
  }

  void ActionCommandBase::kill()
  {
    m_abort=true;
    m_engine->statusMonitorActivator()->abortActionMonitoring( );
    m_engine->statusChangeTransceiver()->abortSignal(kCurrent);
  }

}
