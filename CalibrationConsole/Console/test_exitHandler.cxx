#include "ExitHandler.h"
#include <iostream>
#include <qapplication.h>
#include <qmessagebox.h>
#include <qthread.h>
#include <Flag.h>

namespace PixCon {
  class App {
  public:
    class ResourceHog
    {
    public:
      ResourceHog() {
	std::cout << "INFO [ResourceHog::ctor] eat up resources." << std::endl;
      }

      ~ResourceHog() {
	std::cout << "INFO [ResourceHog::dtor] free resources." << std::endl;
      }
    };


    class FreeResources : public IExitHelper
    {
    public:
      FreeResources(App &app ) :m_app(&app) {}
  
      void freeResources() {
	m_app->freeResources();
      }
  
    private:
      App *m_app;
    };


    class TestApp : public QApplication {
    public:

    class TestThread : public QThread 
    {
    public: 
      TestThread() { std::cout << "INFO [TestThread::ctor] construct." << std::endl; start(); }
      ~TestThread() {  std::cout << "INFO [TestThread::dtor] destruct." << std::endl; }

      void run() {
	std::cout << "INFO [TestThread::run] start." << std::endl;
	m_wait.wait();
	std::cout << "INFO [TestThread::run] end." << std::endl;
	m_exit.setFlag();
      }

      void kill() {
	m_wait.setFlag();
      }

      void wait() {
	m_exit.wait();
      }

    private:
      Flag m_wait;
      Flag m_exit;
    };


      TestApp(int argc, char **argv) : QApplication(argc,argv) {

	m_thread=std::unique_ptr<TestThread>(new TestThread);

      QMessageBox::warning(NULL,
			   "App",
			   "This is a qtapp ",
			   QMessageBox::No,QMessageBox::Yes);
      m_thread->kill();
      m_thread->wait();

      }
      ~TestApp() {}

    private:
      std::unique_ptr<TestThread> m_thread;

    };

    App() : m_resourceHog(new ResourceHog),m_exitHelper(*this) {

      std::cout << "INFO [App::ctor] setting up exit handler."  << std::endl;
      ExitHandler::instance()->addHelper( &m_exitHelper );
      if (!ExitHandler::instance()->isInstalled()) {
	std::vector<int> signal_list;
	signal_list.push_back(SIGTERM);
	signal_list.push_back(SIGINT);
	signal_list.push_back(SIGSEGV);
	signal_list.push_back(SIGABRT);
	//    signal_list.push_back(SIGKILL);
	signal_list.push_back(SIGILL);
	signal_list.push_back(SIGQUIT);
	signal_list.push_back(SIGSYS);
	ExitHandler::instance()->installHandler(signal_list);
	std::cout << "INFO [App::ctor] installed."  << std::endl;
      }
      int argc=1;
      char *argv[2]={"arg0",NULL};
      sleep(2);
      TestApp(argc,argv);
    }

    void freeResources() { m_resourceHog.reset(); }
  private:

    std::unique_ptr<ResourceHog> m_resourceHog;
    FreeResources m_exitHelper;
  };

}

int main()
{
  std::cout << "INFO [main] start."  << std::endl;

  PixCon::App app;
  sleep(30);

  std::cout << "INFO [main] done."  << std::endl;
  return 0;
}
