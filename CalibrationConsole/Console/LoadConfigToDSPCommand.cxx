#include <ICommand.h>
#include <memory>
#include "ActionCommandBase.h"
#include <PixActions/PixActionsMulti.h>


namespace PixCon {

  class LoadConfigurationToDSPCommand : public ActionCommandBase
  {
  public:
    LoadConfigurationToDSPCommand(const std::shared_ptr<ConsoleEngine> &engine)
      : ActionCommandBase(engine)
    {}

    const std::string &name() const { return commandName(); }

    static const std::string &commandName() {
      return s_name;
    }

  protected:

    void setActionSequence(std::vector<std::string> &action_sequence) {
      action_sequence.push_back("loadConfig");
    }

    bool callAction(PixLib::PixActionsMulti &action) {
      action.loadConfig();
      return true;
    }

  private:

    static const std::string s_name;
  };

  const std::string LoadConfigurationToDSPCommand::s_name("Upload config. to ROD");


  class LoadConfigurationToDSPKit : public PixCon::ICommandKit
  {
  protected:
    LoadConfigurationToDSPKit()
    {}

  public:
    PixCon::IConfigurableCommand *createCommand(const std::string &/*command*/, const std::shared_ptr<ConsoleEngine> &engine) const {
      assert(engine.get());
      return new LoadConfigurationToDSPCommand(engine);
    }

    static bool registerKit() {
      CommandKits::registerKit(LoadConfigurationToDSPCommand::commandName(),new LoadConfigurationToDSPKit, PixCon::UserMode::kExpert);
      //      CommandKits::registerKit(LoadConfigurationToDSPCommand::name(true),new LoadConfigurationToDSPKit(true));
      return true;
    }

  private:
    static bool s_registered;
  };

  bool LoadConfigurationToDSPKit::s_registered = LoadConfigurationToDSPKit::registerKit();

}
