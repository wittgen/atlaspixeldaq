// Dear emacs, this is -*-c++-*-x
#ifndef PARTWIN_H
#define PARTWIN_H

#include "ui_PartWinBase.h"
#include <list>
#include <string>
#include <utility>

class IPCPartition;

namespace PixCon
{

  class PartWin : public QDialog, public Ui_PartWinBase 
  {
    Q_OBJECT

  public:
    enum EChoices { kSelection, kDefault};
    enum EServerTypes { kOHServer, kISServer, kNServerTypes};

    PartWin(QApplication &app, QWidget* parent = 0);
    ~PartWin();

    void setPartitionName(const std::string &partition_name, const std::string &default_partition_name);

    void setOHServerName(const std::string &oh_server_name, const std::string &default_oh_server_name) {
      setServerNames(kOHServer,oh_server_name, default_oh_server_name);
    }
    void setISServerName(const std::string &is_server_name, const std::string &default_is_server_name) {
      setServerNames(kISServer,is_server_name, default_is_server_name);
    }

    bool isSelection() const {
      bool is_selection=true;
      for (unsigned int server_i=0; server_i<=kNServerTypes; server_i++) {
	is_selection &= m_isSelection[server_i];
      }
      return is_selection;
    }

  public slots:
    void partSelected(int);

  protected:
    static int find(QComboBox *combo_box, const std::string &text);
    static std::pair<int,bool> setComboBox(QComboBox *combo_box, const std::string &selection, const std::string &default_value);
  private:
    void setServerNames(EServerTypes type, const std::string &selection, const std::string &default_name);

    std::list< IPCPartition > m_partList;
    std::string m_choice[2][kNServerTypes+1];
    bool m_isSelection[kNServerTypes+1];
  };
}

#endif // PARTWIN_H
