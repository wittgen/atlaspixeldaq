
#ifndef UI_CONNECTIVITYPANELBASE_H
#define UI_CONNECTIVITYPANELBASE_H

#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QCheckBox>
#include <QDialog>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QPushButton>
#include <QSpacerItem>
#include <QVBoxLayout>
#include <QTreeWidget>
#include <QMouseEvent>
#include <QGridLayout>

#include <iostream>
#include <memory>

QT_BEGIN_NAMESPACE

class ContextMenuEater: public QObject {
  protected:
    bool eventFilter(QObject* obj, QEvent* evt) override {
      if (evt->type() == QEvent::ContextMenu) {
	auto treeWidget = static_cast<QTreeWidget*>(obj);
        for(int i = 0; i < treeWidget->topLevelItemCount(); ++i){
          auto item = treeWidget->topLevelItem(i);
          item->setSelected(false);
        }
        return true;
	}
      return false;
    }
};
class Ui_ConnectivityPanelBase
{
public:
    QLabel *partLabel;
    QTreeWidget *partList;
    QTreeWidgetItem *partListHeaders;
    QLabel *crateLabel;
    QTreeWidget *crateList;
    QTreeWidgetItem *crateListHeaders;
    QLabel *rodLabel;
    QTreeWidget *rodList;
    QTreeWidgetItem *rodListHeaders;
    QCheckBox *actGrpBox;
    QPushButton *allocateButton;
    QPushButton *applyDisable;
    QPushButton *skipButton;
    QPushButton *crateSelectAll;
    QPushButton *rodSelectAll;
    QPushButton *partitionSelectAll;
    QGridLayout* layout;
   // QPushButton *closeButton;
    std::unique_ptr<ContextMenuEater> menuClearFilter;

    void setupUi(QDialog *ConnectivityPanelBase)
    {   
	menuClearFilter = std::make_unique<ContextMenuEater>();
        if (ConnectivityPanelBase->objectName().isEmpty())
            ConnectivityPanelBase->setObjectName(QString::fromUtf8("ConnectivityPanelBase"));
        ConnectivityPanelBase->resize(602, 734);

	layout = new QGridLayout(ConnectivityPanelBase);

        partLabel = new QLabel(ConnectivityPanelBase);
        partLabel->setWordWrap(false);
	layout->addWidget(partLabel,0,0);

	partitionSelectAll = new QPushButton(ConnectivityPanelBase);
        partitionSelectAll->setChecked(true);
	layout->addWidget(partitionSelectAll,1,0);

        partList = new QTreeWidget(ConnectivityPanelBase);
        partListHeaders = new QTreeWidgetItem;
        partListHeaders->setText(0,"Partition names");
        partList->header()->resizeSection(0,195);
	partListHeaders->setText(1,"ID ");
        partList->setHeaderItem(partListHeaders);
        partList->setSelectionMode(QAbstractItemView::MultiSelection);
        partList->installEventFilter(menuClearFilter.get());
	layout->addWidget(partList,0,1,2,4);
        
        crateLabel = new QLabel(ConnectivityPanelBase);
        crateLabel->setWordWrap(false);
	layout->addWidget(crateLabel,3,0);

	crateSelectAll = new QPushButton(ConnectivityPanelBase);
        crateSelectAll->setChecked(true);
	layout->addWidget(crateSelectAll,4,0);

	crateList = new QTreeWidget(ConnectivityPanelBase);
        crateListHeaders = new QTreeWidgetItem;
        crateListHeaders->setText(0, "Crate names");
        crateList->header()->resizeSection(0,195);
        crateListHeaders->setText(1, "Partition ");
        crateListHeaders->setText(2, "ID ");
        crateList->setHeaderItem(crateListHeaders);
        crateList->setSelectionMode(QAbstractItemView::MultiSelection);
        crateList->installEventFilter(menuClearFilter.get());
	layout->addWidget(crateList,3,1,2,4);

        rodLabel = new QLabel(ConnectivityPanelBase);
        layout->addWidget(rodLabel,6,0);

	rodLabel->setWordWrap(false);
        rodSelectAll = new QPushButton(ConnectivityPanelBase);
        rodSelectAll->setChecked(true);
        layout->addWidget(rodSelectAll,7,0);

	rodList = new QTreeWidget(ConnectivityPanelBase);
        rodListHeaders = new QTreeWidgetItem;
        rodListHeaders->setText(0,"ROD names");
        rodList->header()->resizeSection(0,120);
        rodListHeaders->setText(1,"Crate        ");
        rodListHeaders->setText(2," Partition ");
        rodListHeaders->setText(3,"ID ");
        rodList->header()->resizeSection(3,30);
        rodList->setHeaderItem(rodListHeaders);
        rodList->setSelectionMode(QAbstractItemView::MultiSelection);
        rodList->installEventFilter(menuClearFilter.get());
        layout->addWidget(rodList,6,1,2,4);
	 
        actGrpBox = new QCheckBox(ConnectivityPanelBase);
        actGrpBox->setChecked(true);
        layout->addWidget(actGrpBox, 8, 3, 1, 2, Qt::AlignRight);

	layout->setRowMinimumHeight(9,15);

        allocateButton = new QPushButton(ConnectivityPanelBase);
        allocateButton->setEnabled(false);
        layout->addWidget(allocateButton,10,0);

        applyDisable = new QPushButton(ConnectivityPanelBase);
        layout->addWidget(applyDisable,10,2);

        skipButton = new QPushButton(ConnectivityPanelBase);
        layout->addWidget(skipButton,10,4);


        retranslateUi(ConnectivityPanelBase);
        QObject::connect(allocateButton, SIGNAL(clicked()), ConnectivityPanelBase, SLOT(accept()));
        QObject::connect(skipButton, SIGNAL(clicked()), ConnectivityPanelBase, SLOT(reject()));
        QObject::connect(partList, SIGNAL(itemSelectionChanged()), ConnectivityPanelBase, SLOT(insertCrates()));
        QObject::connect(crateList, SIGNAL(itemSelectionChanged()), ConnectivityPanelBase, SLOT(insertRods()));
        QObject::connect(actGrpBox, SIGNAL(clicked()), ConnectivityPanelBase, SLOT(insertRods()));
        QObject::connect(applyDisable, SIGNAL(clicked()), ConnectivityPanelBase, SLOT(loadPixDisable()));
        QObject::connect(partitionSelectAll, SIGNAL(clicked(bool)), ConnectivityPanelBase, SLOT(disablePartitions()));
        QObject::connect(crateSelectAll, SIGNAL(clicked(bool)), ConnectivityPanelBase, SLOT(disableCrates()));
        QObject::connect(rodSelectAll, SIGNAL(clicked(bool)), ConnectivityPanelBase, SLOT(disableRods()));
      //  QObject::connect(closeButton,SIGNAL(clicked()),ConnectivityPanelBase,SLOT(exitCalibrationConsole()));

        QMetaObject::connectSlotsByName(ConnectivityPanelBase);
    } // setupUi

    void retranslateUi(QDialog *ConnectivityPanelBase)
    {
        ConnectivityPanelBase->setWindowTitle(QApplication::translate("ConnectivityPanelBase", "Connectivity DB selection panel", 0));
        partLabel->setText(QApplication::translate("ConnectivityPanelBase", "Select...", 0));
        crateLabel->setText(QApplication::translate("ConnectivityPanelBase", "Select...", 0));
        rodLabel->setText(QApplication::translate("ConnectivityPanelBase", "Select...", 0));
        actGrpBox->setText(QApplication::translate("ConnectivityPanelBase", "show only available RODs", 0));
        allocateButton->setText(QApplication::translate("ConnectivityPanelBase", "Allocate selection", 0));
        applyDisable->setText(QApplication::translate("ConnectivityPanelBase", "Apply disable", 0));
        skipButton->setText(QApplication::translate("ConnectivityPanelBase", "Skip selection", 0));
    } // retranslateUi

};

namespace Ui {
    class ConnectivityPanelBase: public Ui_ConnectivityPanelBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONNECTIVITYPANELBASE_H
