#include "ApplyDisableCommand.h"
#include <PixActions/PixActionsMulti.h>
#include "IStatusMonitorActivator.h"
#include "ConsoleEngine.h"
#include <ConfigWrapper/ConnectivityUtil.h>

#include <DisablePanel.h>
#include <ConfigWrapper/PixDisableUtil.h>


namespace PixCon {
  namespace DisableCommand {

  class ApplyDisableCommand : public PixCon::IConfigurableCommand
  {
  public:
    ApplyDisableCommand(const std::shared_ptr<ConsoleEngine> &engine,
			const std::shared_ptr<const PixLib::PixDisable> &disable,
			bool changes_only,
			DisableCommand::EMode disable_mode);

    ~ApplyDisableCommand();

    PixCon::Result_t execute(IStatusMessageListener &listener);

    /** Nothing to do */
    void resetAbort() { }

    /** cannot do anything */
    void abort() { }

    /** cannot do anything */
    void kill() { }

    bool configure();

    bool hasDefaults() const { return m_disable.get(); }

    const std::string &name() const { return (m_disable.get() ? m_name : s_disableNothingName);  }

  protected:

  private:
    std::string m_name;
    std::shared_ptr<ConsoleEngine> m_engine;
    std::shared_ptr<const PixLib::PixDisable> m_disable;
    bool m_changesOnly;
    DisableCommand::EMode m_disableMode;
    static const std::string s_disableModeName[3];
    static const std::string s_disableNothingName;
  };

  const std::string ApplyDisableCommand::s_disableNothingName("Disable_Nothing");
  const std::string ApplyDisableCommand::s_disableModeName[3]={std::string("ApplyDisable_"), std::string("Disable_"), std::string("Enable_")};

  ApplyDisableCommand::ApplyDisableCommand(const std::shared_ptr<ConsoleEngine> &engine,
					   const std::shared_ptr<const PixLib::PixDisable> &disable,
					   bool changes_only,
					   EMode disable_mode)
    : m_engine(engine),
      m_disable(disable),
      m_changesOnly(changes_only),
      m_disableMode(disable_mode)
  {
    if (m_disable.get()) {
      assert(m_disableMode<3);
      m_name=s_disableModeName[m_disableMode];
      m_name+=const_cast<PixLib::PixDisable &>( *m_disable).name();
    }
  }

  ApplyDisableCommand::~ApplyDisableCommand() { }

  PixCon::Result_t ApplyDisableCommand::execute(IStatusMessageListener &)
  {
    PixCon::Result_t ret;
    if(!m_disable.get()) {
      ret.setSuccess();
      return ret;
    }
    //      m_engine->initialiseRods(m_uninitialisedRodsOnly);
    //       /* ConsoleEngine::EActionStatus status = */ m_engine->waitForCurrentAction(m_abort);

    ret.setFailure();
    try {
      Lock action_lock(m_engine->multiActionMutex());
      PixLib::PixActionsMulti &action = m_engine->multiAction();

      EMeasurementType measurement_type=kCurrent;
      SerialNumber_t serial_number = 0;
      PixA::ConnectivityRef conn = m_engine->calibrationData().connectivity(measurement_type, serial_number);

      if (!conn ) {
	return ret;
      }

      bool mask_active =false;
      if (m_disableMode==DisableCommand::kEnable) {
	mask_active = true;
      }

      bool tim_warning=false;
      if (conn) {
        std::vector<std::pair<std::string, bool> > rod_active_list;
        rod_active_list.reserve(144+15);
        std::vector<std::pair<std::string, std::pair< unsigned int, bool > > > module_active_list;
        module_active_list.reserve(1744+224+24);

	try {
	  std::cout << "INFO [ApplyDisableCommand::execute] Apply disable " << const_cast<PixLib::PixDisable &>(*m_disable).name() << std::endl;

          {
          PixCon::Lock lock( m_engine->calibrationData().calibrationDataMutex() );
 	  CalibrationData &calibration_data = m_engine->calibrationData().calibrationData();

	  PixA::DisabledListRoot disabled_list_root( m_disable );

	  // and set bare data lists for each object insided the connectivity
	  for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
	       crate_iter != conn.crates().end();
	       ++crate_iter) {

	    bool crate_enabled = PixA::enabled( disabled_list_root, crate_iter );

	    const PixA::DisabledList rod_disabled_list( PixA::rodDisabledList( disabled_list_root, crate_iter) );

	    bool has_enabled_rods=false;
	    PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
	    PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
	    for (PixA::RodList::const_iterator rod_iter = rod_begin;
		 rod_iter != rod_end;
		 ++rod_iter) {

	      bool rod_enabled = PixA::enabled( rod_disabled_list, rod_iter ) & crate_enabled;
	      std::string action_name = m_engine->actionName(PixA::connectivityName(rod_iter), PixA::connectivityName(crate_iter) );

	      {
		PixCon::CalibrationData::EnableState_t enable_state;
		try {
		  PixCon::CalibrationData::ConnObjDataList data_list( calibration_data.createConnObjDataList( PixA::connectivityName(rod_iter) ));
		  enable_state = data_list.enableState(measurement_type, serial_number);
		  if (!enable_state.isInConnectivity() || !enable_state.isInConfiguration() || !enable_state.isAllocated()) {
		    continue;
		  }

		  if (m_disableMode==DisableCommand::kApplyAll || rod_enabled == mask_active) {
		  if (enable_state.isTemporaryEnabled() != rod_enabled || !m_changesOnly) {
                     rod_active_list.push_back(std::make_pair(action_name, rod_enabled));
	          }
		  }
		}
		catch( PixCon::CalibrationData::MissingConnObject &) {
		  continue;
		}

	      }
	      has_enabled_rods=true;
	      if (!rod_enabled) continue;

	      const PixA::DisabledList pp0_disabled_list( PixA::pp0DisabledList( rod_disabled_list, rod_iter) );

	      PixA::Pp0List::const_iterator pp0_begin = PixA::pp0Begin(rod_iter);
	      PixA::Pp0List::const_iterator pp0_end = PixA::pp0End(rod_iter);
	      for (PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
		   pp0_iter != pp0_end;
		   ++pp0_iter) {

		bool pp0_enabled = PixA::enabled( pp0_disabled_list, pp0_iter );

		const PixA::DisabledList module_disabled_list(PixA::moduleDisabledList( pp0_disabled_list, pp0_iter) );

		PixA::ModuleList::const_iterator module_begin = PixA::modulesBegin(pp0_iter);
		PixA::ModuleList::const_iterator module_end = PixA::modulesEnd(pp0_iter);
		for (PixA::ModuleList::const_iterator module_iter = module_begin;
		     module_iter != module_end;
		     ++module_iter) {

		  bool mod_enabled = PixA::enabled( module_disabled_list, module_iter ) & pp0_enabled;

		  {
		    PixCon::CalibrationData::EnableState_t enable_state;
		    try {
		      PixCon::CalibrationData::ConnObjDataList data_list( calibration_data.createConnObjDataList( PixA::connectivityName(module_iter) ));
		      enable_state = data_list.enableState(measurement_type, serial_number);
		      if (enable_state.isInConfiguration() && enable_state.isInConnectivity() && enable_state.isAllocated()){
			if (m_disableMode==DisableCommand::kApplyAll || mod_enabled == mask_active) {
			  if ( enable_state.isTemporaryEnabled() != mod_enabled || !m_changesOnly ) {
                            module_active_list.push_back(std::make_pair(action_name, std::make_pair<unsigned int,bool>(PixA::modId(module_iter), (bool)mod_enabled)));
		          }
			}
		      }
		    }
		    catch( PixCon::CalibrationData::MissingConnObject &) {
		    }
		  }

		}
	      }
	    }

	    if (has_enabled_rods) {
	    const PixLib::TimConnectivity *tim_conn = PixA::responsibleTim(*crate_iter);
	    if (tim_conn) {

	      bool tim_enabled = PixA::timEnabled( rod_disabled_list, crate_iter )  & crate_enabled;
	      {
		PixCon::CalibrationData::EnableState_t enable_state;
		try {
		  PixCon::CalibrationData::ConnObjDataList data_list( calibration_data.createConnObjDataList( PixA::connectivityName(tim_conn) ));
		  enable_state = data_list.enableState(measurement_type, serial_number);
		  //		  if (enableState.isInConnectivit()&& enableState.isInConfiguration() && enableState.isAllocated()){
		  if (enable_state.isAllocated() && enable_state.isTemporaryEnabled() && !tim_enabled){
		    if (!tim_warning) {
		      std::cout << "WARNING [ApplyDisableCommand::execute] Cannot change the active state of TIMS." << std::endl;
		      tim_warning=true;
		    }
		  }
		}
		catch( PixCon::CalibrationData::MissingConnObject &) {
		}
	      }
	    }
	    }
          }
	  } // calibration data lock

          for(std::vector<std::pair<std::string, bool> >::const_iterator rod_iter = rod_active_list.begin();
              rod_iter != rod_active_list.end();
              ++rod_iter) {
//	     std::cout << "INFO [ApplyDisableCommand::execute] Set active " << rod_iter->first << " " << rod_iter->second << std::endl;
	     action.setRodActive( rod_iter->first , rod_iter->second );
          }
          for(std::vector<std::pair<std::string,std::pair< unsigned int , bool> > >::const_iterator module_iter = module_active_list.begin();
              module_iter != module_active_list.end();
              ++module_iter) {

//	     std::cout << "INFO [ApplyDisableCommand::execute] Set active " << module_iter->first << " " << module_iter->second.first 
//                       << " " << module_iter->second.second << std::endl;
	     action.setModuleActive( module_iter->first , module_iter->second.first, module_iter->second.second);
	  }
	  ret.setSuccess();
	}
	catch(...) {
	}
      }
    }
    catch(...) {
    }

    return ret;
  }

  bool ApplyDisableCommand::configure() {

    DisablePanel dp(m_engine->getConn(), NULL);
    if (dp.exec() == QDialog::Accepted) {
      m_disable = dp.getDisable();
      if (m_disable.get()) {
	assert(m_disableMode<3);
	m_name=s_disableModeName[m_disableMode];
	m_name+=const_cast<PixLib::PixDisable &>( *m_disable).name();
      }
      return true;
    }
    else {
      return false;
    }
  }

  IConfigurableCommand *create(const std::shared_ptr<ConsoleEngine> &engine,
						  const std::shared_ptr<const PixLib::PixDisable> &disable,
						  bool changes_only,
						  DisableCommand::EMode disable_mode) {
      assert(engine.get());
      return new ApplyDisableCommand(engine, disable, changes_only,disable_mode);
  }


  class ApplyDisableKit : public PixCon::ICommandKit
  {
  protected:
    ApplyDisableKit(DisableCommand::EMode disable_mode)
      : m_disableMode(disable_mode)
    {}

  public:
    PixCon::IConfigurableCommand *createCommand(const std::string &/*command*/, const std::shared_ptr<ConsoleEngine> &engine) const {
      return create(engine, true, m_disableMode);
    }

    static bool registerKit() {
      CommandKits::registerKit("Apply Disable",new ApplyDisableKit(DisableCommand::kApplyAll), PixCon::UserMode::kShifter);
      CommandKits::registerKit("Add Disable",new ApplyDisableKit(DisableCommand::kDisable), PixCon::UserMode::kShifter);
      CommandKits::registerKit("Add Enable",new ApplyDisableKit(DisableCommand::kEnable), PixCon::UserMode::kShifter);
      return true;
    }

  private:
    DisableCommand::EMode  m_disableMode;

    static bool s_registered;
  };

  bool ApplyDisableKit::s_registered = ApplyDisableKit::registerKit();

  }

}
