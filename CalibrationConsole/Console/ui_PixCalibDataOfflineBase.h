#ifndef UI_PIXCALIBDATAOFFLINEBASE_H
#define UI_PIXCALIBDATAOFFLINEBASE_H

#include <QGroupBox>
#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QDialog>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QSpacerItem>
#include <QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_PixCalibDataOfflineBase
{
public:
    QGroupBox *m_extractAnalysisGroupBox;
    QLabel *m_totLabel;
    QLabel *m_thresholdLabel;
    QLabel *m_intimeThresholdLabel;
    QLineEdit *m_totLineEdit;
    QLineEdit *m_thresholdLineEdit;
    QLineEdit *m_intimeThresholdLineEdit;
    QPushButton *m_extractAnalysisButton;
    QGroupBox *m_createSQLiteFileGroupBox;
    QLabel *m_onlineTagLabel;
    QLabel *m_calibrationTagLabel;
    QLabel *m_runPeriodStartLabel;
    QLabel *m_runPeriodEndLabel;
    QPushButton *m_createSQLiteFileButton;
    QLineEdit *m_onlineTagLineEdit;
    QLineEdit *m_calibrationTagLineEdit;
    QLineEdit *m_runPeriodStartLineEdit;
    QLineEdit *m_runPeriodEndLineEdit;
    QGroupBox *m_transferToCOOLGroupBox;
    QLabel *m_passwordLabel;
    QLineEdit *m_passwordLineEdit;
    QPushButton *m_transferToCOOLButton;
    QPushButton *m_closeButton;

    void setupUi(QDialog *PixCalibDataOfflineBase)
    {

        QVBoxLayout *pixCalibDataOfflineLayout = new QVBoxLayout(PixCalibDataOfflineBase);
        pixCalibDataOfflineLayout->setSpacing(6);

        m_extractAnalysisGroupBox = new QGroupBox;
        m_totLabel = new QLabel(m_extractAnalysisGroupBox);
        m_totLineEdit = new QLineEdit(m_extractAnalysisGroupBox);
        m_thresholdLabel = new QLabel(m_extractAnalysisGroupBox);
        m_thresholdLineEdit = new QLineEdit(m_extractAnalysisGroupBox);
        m_intimeThresholdLabel = new QLabel(m_extractAnalysisGroupBox);
        m_intimeThresholdLineEdit = new QLineEdit(m_extractAnalysisGroupBox);
        m_extractAnalysisButton = new QPushButton(m_extractAnalysisGroupBox);

        QHBoxLayout *totLayout = new QHBoxLayout;
        totLayout->addWidget(m_totLabel);
        totLayout->addWidget(m_totLineEdit);
        QHBoxLayout *threshholdLayout = new QHBoxLayout;
        threshholdLayout->addWidget(m_thresholdLabel);
        threshholdLayout->addWidget(m_thresholdLineEdit);
        QHBoxLayout *intimeLayout = new QHBoxLayout;
        intimeLayout->addWidget(m_intimeThresholdLabel);
        intimeLayout->addWidget(m_intimeThresholdLineEdit);

        QVBoxLayout *extractAnalysisLayout = new QVBoxLayout;
        extractAnalysisLayout->addLayout(totLayout);
        extractAnalysisLayout->addLayout(threshholdLayout);
        extractAnalysisLayout->addLayout(intimeLayout);
        extractAnalysisLayout->addWidget(m_extractAnalysisButton);
        extractAnalysisLayout->addStretch(1);

        m_extractAnalysisGroupBox->setLayout(extractAnalysisLayout);

        m_createSQLiteFileGroupBox = new QGroupBox;
        m_onlineTagLabel = new QLabel(m_createSQLiteFileGroupBox);
        m_onlineTagLineEdit = new QLineEdit(m_createSQLiteFileGroupBox);
        m_calibrationTagLabel = new QLabel(m_createSQLiteFileGroupBox);
        m_calibrationTagLineEdit = new QLineEdit(m_createSQLiteFileGroupBox);
        m_runPeriodStartLabel = new QLabel(m_createSQLiteFileGroupBox);
        m_runPeriodStartLineEdit = new QLineEdit(m_createSQLiteFileGroupBox);
        m_runPeriodEndLabel = new QLabel(m_createSQLiteFileGroupBox);
        m_runPeriodEndLineEdit = new QLineEdit(m_createSQLiteFileGroupBox);
        m_createSQLiteFileButton = new QPushButton(m_createSQLiteFileGroupBox);

        QHBoxLayout *onlineTagLayout = new QHBoxLayout;
        onlineTagLayout->addWidget(m_onlineTagLabel);
        onlineTagLayout->addWidget(m_onlineTagLineEdit);
        QHBoxLayout *calibrationTagLayout = new QHBoxLayout;
        calibrationTagLayout->addWidget(m_calibrationTagLabel);
        calibrationTagLayout->addWidget(m_calibrationTagLineEdit);
        QHBoxLayout *runStartLayout = new QHBoxLayout;
        runStartLayout->addWidget(m_runPeriodStartLabel);
        runStartLayout->addWidget(m_runPeriodStartLineEdit);
        QHBoxLayout *runEndLayout = new QHBoxLayout;
        runEndLayout->addWidget(m_runPeriodEndLabel);
        runEndLayout->addWidget(m_runPeriodEndLineEdit);

        QVBoxLayout *createSQLiteLayout = new QVBoxLayout;
        createSQLiteLayout->addLayout(onlineTagLayout);
        createSQLiteLayout->addLayout(calibrationTagLayout);
        createSQLiteLayout->addLayout(runStartLayout);
        createSQLiteLayout->addLayout(runEndLayout);
        createSQLiteLayout->addWidget(m_createSQLiteFileButton);
        createSQLiteLayout->addStretch(1);

        m_createSQLiteFileGroupBox->setLayout(createSQLiteLayout);


        m_transferToCOOLGroupBox = new QGroupBox;
        m_passwordLabel = new QLabel(m_transferToCOOLGroupBox);
        m_passwordLineEdit = new QLineEdit(m_transferToCOOLGroupBox);
        m_transferToCOOLButton = new QPushButton(m_transferToCOOLGroupBox);
        m_closeButton = new QPushButton(m_transferToCOOLGroupBox);

        QHBoxLayout *transferToCOOLLayout = new QHBoxLayout;
        transferToCOOLLayout->addWidget(m_passwordLabel);
        transferToCOOLLayout->addWidget(m_passwordLineEdit);
        transferToCOOLLayout->addWidget(m_transferToCOOLButton);
        transferToCOOLLayout->addWidget(m_closeButton);
        transferToCOOLLayout->addStretch(1);

        m_transferToCOOLGroupBox->setLayout(transferToCOOLLayout);

        pixCalibDataOfflineLayout->addWidget(m_extractAnalysisGroupBox);
        pixCalibDataOfflineLayout->addWidget(m_createSQLiteFileGroupBox);
        pixCalibDataOfflineLayout->addWidget(m_transferToCOOLGroupBox);


        retranslateUi(PixCalibDataOfflineBase);
        QObject::connect(m_extractAnalysisButton, SIGNAL(clicked()), PixCalibDataOfflineBase, SLOT(extractAnalysis()));
        QObject::connect(m_createSQLiteFileButton, SIGNAL(clicked()), PixCalibDataOfflineBase, SLOT(createSQLiteFile()));
        QObject::connect(m_transferToCOOLButton, SIGNAL(clicked()), PixCalibDataOfflineBase, SLOT(transferToCOOL()));
        QObject::connect(m_closeButton, SIGNAL(clicked()), PixCalibDataOfflineBase, SLOT(close()));

        QMetaObject::connectSlotsByName(PixCalibDataOfflineBase);
    } // setupUi

    void retranslateUi(QDialog *PixCalibDataOfflineBase)
    {
        PixCalibDataOfflineBase->setWindowTitle(QApplication::translate("PixCalibDataOfflineBase", "PCD Creator", 0));
        m_extractAnalysisGroupBox->setTitle(QApplication::translate("PixCalibDataOfflineBase", "ExtractAnalysis", 0));
        m_totLabel->setText(QApplication::translate("PixCalibDataOfflineBase", "ToT:", 0));
        m_thresholdLabel->setText(QApplication::translate("PixCalibDataOfflineBase", "Threshold:", 0));
        m_intimeThresholdLabel->setText(QApplication::translate("PixCalibDataOfflineBase", "IntimeThreshold:", 0));
#ifndef QT_NO_TOOLTIP
        m_totLineEdit->setProperty("toolTip", QVariant(QApplication::translate("PixCalibDataOfflineBase", "enter time-over-threshold analysis number", 0)));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        m_thresholdLineEdit->setProperty("toolTip", QVariant(QApplication::translate("PixCalibDataOfflineBase", "enter threshold analysis number", 0)));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        m_intimeThresholdLineEdit->setProperty("toolTip", QVariant(QApplication::translate("PixCalibDataOfflineBase", "enter intime-threshold analysis number", 0)));
#endif // QT_NO_TOOLTIP
        m_extractAnalysisButton->setText(QApplication::translate("PixCalibDataOfflineBase", "ExtractAnalysis", 0));
        m_createSQLiteFileGroupBox->setTitle(QApplication::translate("PixCalibDataOfflineBase", "CreateSQLiteFile", 0));
        m_onlineTagLabel->setText(QApplication::translate("PixCalibDataOfflineBase", "OnlineTags:", 0));
        m_calibrationTagLabel->setText(QApplication::translate("PixCalibDataOfflineBase", "CalibrationTag:", 0));
        m_runPeriodStartLabel->setText(QApplication::translate("PixCalibDataOfflineBase", "RunPeriodStart:", 0));
        m_runPeriodEndLabel->setText(QApplication::translate("PixCalibDataOfflineBase", "RunPeriodEnd:", 0));
        m_createSQLiteFileButton->setText(QApplication::translate("PixCalibDataOfflineBase", "CreateSQLiteFile", 0));
#ifndef QT_NO_TOOLTIP
        m_onlineTagLineEdit->setProperty("toolTip", QVariant(QApplication::translate("PixCalibDataOfflineBase", "enter online tags that correspond to this new calibration", 0)));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        m_calibrationTagLineEdit->setProperty("toolTip", QVariant(QApplication::translate("PixCalibDataOfflineBase", "enter offline calibration tag in which this new calibration to be included", 0)));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        m_runPeriodStartLineEdit->setProperty("toolTip", QVariant(QApplication::translate("PixCalibDataOfflineBase", "enter run number this new calibration is first used", 0)));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        m_runPeriodEndLineEdit->setProperty("toolTip", QVariant(QApplication::translate("PixCalibDataOfflineBase", "enter run number this new calibration is last used. leave empty if it still applies", 0)));
#endif // QT_NO_TOOLTIP
        m_transferToCOOLGroupBox->setTitle(QApplication::translate("PixCalibDataOfflineBase", "TransferToCOOL", 0));
        m_passwordLabel->setText(QApplication::translate("PixCalibDataOfflineBase", "Password:", 0));
#ifndef QT_NO_TOOLTIP
        m_passwordLineEdit->setProperty("toolTip", QVariant(QApplication::translate("PixCalibDataOfflineBase", "enter admin password to modify pixel folder in COOL", 0)));
#endif // QT_NO_TOOLTIP
        m_transferToCOOLButton->setText(QApplication::translate("PixCalibDataOfflineBase", "TransferToCOOL", 0));
        m_closeButton->setText(QApplication::translate("PixCalibDataOfflineBase", "Close", 0));
    } // retranslateUi

};

namespace Ui {
    class PixCalibDataOfflineBase: public Ui_PixCalibDataOfflineBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PIXCALIBDATAOFFLINEBASE_H
