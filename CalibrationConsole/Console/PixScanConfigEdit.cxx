#include "PixScanConfigEdit.h"
#include "PixScanPanel.h"
#include "ScanSelection.h"

namespace PixCon {

  PixScanConfigEdit::PixScanConfigEdit(std::vector<PixA::ConfigHandle> &vconfig,
                       const std::string &type_name,
                       const std::string &tag_name,
                       QComboBox &tag_list,
                       QWidget* parent)
    : ConfigEdit(parent)
  {
    QScrollArea* sv = new QScrollArea(m_configTabFrameFEI3);
    sv->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    sv->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    m_configTabFrameFEI3Layout->addWidget(sv);
    m_pixScanPanel = new PixScanPanel(vconfig[ScanSelection::TBase], std::string("Edit FE-I3 scan configuration of type ") + type_name, m_configTabFrameFEI3);
    sv->setWidget(m_pixScanPanel);
    sv->setWidgetResizable(true);
    sv->viewport()->adjustSize();

    QScrollArea* sv2 = new QScrollArea(m_configTabFrameFEI4);
    sv2->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    sv2->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    m_configTabFrameFEI4Layout->addWidget(sv2);
    m_pixScanPanelFEI4 = new PixScanPanel(vconfig[ScanSelection::TBase_I4], std::string("Edit FE-I4 scan configuration of type ") + type_name, m_configTabFrameFEI4);
    sv2->setWidget(m_pixScanPanelFEI4);
    sv2->setWidgetResizable(true);
    sv2->viewport()->adjustSize();

    for(unsigned int item_i=0; item_i < (unsigned int)tag_list.count(); item_i++) {
        QString listString;
        listString = tag_list.currentText();
        m_tagSelector->insertItem(item_i,listString,Qt::DisplayRole);//
      if (tag_name == listString.toStdString()) {//
        m_tagSelector->setCurrentIndex(m_tagSelector->count()-1);
      }
    }
    //show();
  }

  bool PixScanConfigEdit::changed() const {
    //@todo compare current with orig config and find out whether something has changed.
    return true;
  }

  std::string PixScanConfigEdit::tagName() const {
    std::string temp;
    if (m_tagSelector->currentText().length()>0) {
      temp = m_tagSelector->currentText().toLatin1().data();
    }
    return temp;
  }
  /*
  std::string PixScanConfigEdit::Get_tagName(std::string tag_name) {
    return tag_name;
  }
  */
  PixLib::Config &PixScanConfigEdit::config() {
    return m_pixScanPanel->config();
  }

  PixLib::Config &PixScanConfigEdit::config(std::string tag_name) {
     /* yosuke    if(tag_name.compare(tag_name.size()-3, 3, "_I3") == 0) return m_pixScanPanel->config();
       else */
	if(tag_name.compare(tag_name.size()-3, 3, "_I4") == 0) return m_pixScanPanelFEI4->config();
    else return m_pixScanPanel->config();
  
    /*
    if(tag_name == "Base_I3") return m_pixScanPanel->config();
    else if(tag_name == "Base_I4") return m_pixScanPanelFEI4->config();
    else return m_pixScanPanel->config();
    */
  }

  PixScanConfigEdit::~PixScanConfigEdit() {
  }

}
