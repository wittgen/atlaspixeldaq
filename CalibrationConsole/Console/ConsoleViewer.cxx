#include "ConsoleViewer.h"
#include "ConsoleEngine.h"
#include "ConfigWrapper/ConnectivityUtil.h"
#include "ActiveExtractor.h"
#include "DetectorView.h"

#include <CommandExecutor.h>
#include <CommandFactory.h>
#include <CommandList.h>
#include <CommandListExecutionListener.h>

#include "CanStatusPanel.h"

#include "ActionStatusMonitor.h"
#include "StatusMonitorActivator.h"
#include <IsActiveMonitor.h>
#include <IsMonitorManager.h>
#include <IsReceptor.h>

#include <QMessageBox>
#include <QStatusBar>

#include "ConnectivityPanel.h"
#include "CloneTags.h"

#include "InitialiseRodCommand.h"
#include "ApplyDisableCommand.h"

#include <BusyLoop.h>
#include "ProgressEvent.h"

#include <DefaultColourCode.h>
#include "SetActiveCommand.h"
#include "DontPanelStandby.h"

#ifdef HAVE_DDC
//#  include <DcsSubscriptions.h>
//#  include <DcsMonitorFactory.h>
//#  include <DcsMonitor.h>
//#  include <ddc/DdcRequest.hxx>
#  include <DcsMonitorManager.h>
#endif

#include <SaveDisable.h>
#include <ModuleMapH2Factory.h>

#include "ChangeTagsCommand.h"

#include "KillDialogue.h"

namespace PixCon {

  ActiveExtractor s_activeExtractor;

  ConsoleViewer::ConsoleViewer( std::shared_ptr<PixCon::UserMode>& user_mode,
				IPCPartition &is_partition,
				const std::string &is_server_name,
				const std::string &orig_ddc_partition_name,
				const std::string &orig_ddc_is_name,
				const std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data,
				PixCon::OHHistogramProviderBase &oh_histogram_provider,
				std::shared_ptr<PixCon::MetaHistogramProvider> meta_histogram_provider,
				const std::shared_ptr<PixCon::HistogramCache> &histogram_cache,
				const std::shared_ptr<PixCon::CalibrationDataPalette> &palette,
				const std::shared_ptr<PixCon::CategoryList> &categories,
				const std::shared_ptr<PixCon::IContextMenu> &context_menu,
				const std::shared_ptr<PixCon::IMetaDataUpdateHelper> &update_helper,
				const std::shared_ptr<PixCon::VisualiserList> &visualiser_list,
				const std::shared_ptr<PixCon::ConsoleEngine> &engine,
				const std::shared_ptr<PixCon::IBocEditor> &boc_editor,
				QApplication &the_application,
				QWidget* parent)
    : OHViewer(user_mode,
	       is_server_name,
	       is_partition,
	       calibration_data,
	       engine->statusChangeTransceiver(),
	       oh_histogram_provider,
	       meta_histogram_provider,
	       histogram_cache,
	       palette,
	       categories,
	       context_menu,
	       update_helper,
	       visualiser_list,
	       the_application,
	       parent),
      m_userMode(user_mode),
      m_engine(engine),
      m_isPartition(&is_partition),
      m_commandList(new PixCon::CommandList(user_mode, m_engine->executor(),
					    std::shared_ptr<PixCon::ICommandFactory>(new CommandFactory(m_engine)))),
      m_canStatusPanel(NULL),
      m_bocEditor(boc_editor),
      m_isShutdown(false)
  {
    setWindowTitle("Calibration Console");

    // remove entry about selecting connectivity tags;
//     for (unsigned int menu_item_i=0; menu_item_i < File->count(); menu_item_i++) {
//       if (File->findItem(menu_item_i)) {
// 	QMenuItem *menu_item(File->findItem(File->idAt(menu_item_i)));
// 	if (menu_item->text()=="Select Connectivity Tags") {
// 	  File->removeItem(menu_item->id());
// 	  break;
// 	}
//       }
//     }

    QAction *menuAct;

    File->insertSeparator(m_sepReconnAction);

    menuAct = new QAction("(Re-)A&llocate Actions",this);
    menuAct->setShortcut(Qt::CTRL+Qt::Key_A);
    connect(menuAct, SIGNAL( triggered(bool) ), this, SLOT( allocateActions() ));
    File->insertAction(m_sepReconnAction, menuAct); // insert above separator to IPC reconnect action

    menuAct = new QAction("Change Connectivity &Tags",this);
    menuAct->setShortcut(Qt::CTRL+Qt::Key_T);
    connect(menuAct, SIGNAL( triggered(bool) ), this, SLOT( selectConnectivityTags() ));
    File->insertAction(m_sepReconnAction, menuAct); // insert above separator to IPC reconnect action, pushes allocate action up
    m_actionItems.push_back(menuAct);

    menuAct = new QAction("&Clone Config Tag",this);
    menuAct->setShortcut(Qt::CTRL+Qt::Key_C);
    connect(menuAct, SIGNAL( triggered(bool) ), this, SLOT( showCloneTags() ));
    File->insertAction(m_sepReconnAction, menuAct);
    m_actionItems.push_back(menuAct);

    File->insertSeparator(m_sepReconnAction);

    menuAct = new QAction("&Save Disable",this);
    menuAct->setShortcut(Qt::CTRL+Qt::Key_S);
    connect(menuAct, SIGNAL( triggered(bool) ), this, SLOT( saveDisable() ));
    File->insertAction(m_sepReconnAction, menuAct);

    if (m_bocEditor) {
      if(!Utils->menuAction()->isVisible()) Utils->menuAction()->setVisible(true);
      //if (MenuBar->isItemVisible(3) == false) MenuBar->setItemVisible(3, true);
      m_bocEdtAct = Utils->addAction("&BOC Editor",this, SLOT( showBocEditor() ), Qt::CTRL+Qt::Key_B);//, kUtilsBocEditor, utils_index++);
    } else
      m_bocEdtAct = 0;

#ifdef HAVE_DDC

    {
      std::string ddc_partition_name = orig_ddc_partition_name;
      std::string ddc_is_name = orig_ddc_is_name; 

    if (ddc_is_name.empty()) {
      ddc_is_name = is_server_name;
    }
    if (ddc_partition_name.empty()) {
       ddc_partition_name = is_partition.name();
    }


    std::unique_ptr<IPCPartition> ddc_partition;
    std::shared_ptr<IsReceptor> ddc_receptor;
    if (ddc_partition_name != is_partition.name()) {
      ddc_partition = std::unique_ptr<IPCPartition>( new IPCPartition(ddc_partition_name));
      std::cout << "INFO [RodStatusMonitor::setupIsMonitor] Use " << ddc_partition_name << " as DDC partition." << std::endl;
    }
    if (ddc_partition_name != is_partition.name() || ddc_is_name != is_server_name) {    
      ddc_receptor = std::shared_ptr<PixCon::IsReceptor>(new PixCon::IsReceptor(&the_application,
										  this,
										  ddc_is_name, 
										  ( ddc_partition.get() ? *ddc_partition : is_partition),
										  calibrationData()));
       std::cout << "INFO [RodStatusMonitor::setupIsMonitor] Use " << ddc_is_name << " as DDC IS server in partition." << std::endl;
    }
    else {
      ddc_receptor = receptor();
    }

    m_dcsMonitorManager=std::unique_ptr<DcsMonitorManager>(new DcsMonitorManager(ddc_receptor,
									       monitorManager(),
									       calibrationData(),
									       categories));
    connect(m_dcsMonitorManager.get(),SIGNAL(updateCategories()), this, SLOT(updateAllCategories()));
    if(!Utils->menuAction()->isVisible()) Utils->menuAction()->setVisible(true);
    //if (MenuBar->isItemVisible(3) == false) MenuBar->setItemVisible(3, true);
    if (Utils->actions().count() > 0) Utils->addSeparator();//utils_index++);
    Utils->addAction("&DCS Monitor", m_dcsMonitorManager.get(), SLOT( showDcsSubscription() ), Qt::CTRL+Qt::Key_D);//, kUtilsDCSMonitor, utils_index++);

    Utils->addSeparator();//utils_index++);
    m_pcdAct = Utils->addAction("&PCD Creator", this, SLOT( showPCDCreator() ), Qt::CTRL+Qt::Key_P);//, kUtilsPCDCreator, utils_index++);
    //if (m_userMode->getUserMode() != PixCon::UserMode::kExpert) Utils->setItemEnabled(kUtilsPCDCreator, false);
    if (m_userMode->getUserMode() != PixCon::UserMode::kExpert) m_pcdAct->setEnabled(false);
    }
#endif

    connect(this,SIGNAL(progress(unsigned int, unsigned int)), m_commandList, SLOT(progress(unsigned int, unsigned int)));
    connect(this,SIGNAL(statusMessage(const QString &)), m_commandList, SLOT(setStatusMessage(const QString &)));

    m_canStatusPanel=new CanStatusPanel(the_application, m_engine->partition().name(),palette);
    connect(m_canStatusPanel, SIGNAL( statusMessage(const QString &) ), this->statusBar(), SLOT( showMessage(const QString &) ) );

    mainWindowRightPanel->addTab(m_commandList,"&Command Tool");
    if (m_userMode->getUserMode() == PixCon::UserMode::kExpert) mainWindowRightPanel->addTab(m_canStatusPanel,"CAN &Status");

    connect(this, SIGNAL( userModeChanged(PixCon::UserMode::EUserMode) ),
	    this, SLOT( updateViewerForUserMode(PixCon::UserMode::EUserMode) ));

    m_engine->executor()->setExecutionListener(std::shared_ptr<PixCon::IExecutionListener>(new PixCon::CommandListExecutionListener(*application(),*m_commandList)));

    // @todo read variable definition from db
    try {
      const VarDef_t var_def( VarDefList::instance()->getVarDef(SetActiveCommand::activeName()) );
      if ( !var_def.isStateWithOrWithoutHistory() ) {
	std::cout << "ERROR [ScanStatusMonitor::ctor] variable " << SetActiveCommand::activeName() << " already exists but it is not a state variable." << std::endl;
      }
    }
    catch (UndefinedCalibrationVariable &) {
      VarDef_t var_def( VarDefList::instance()->createStateVarDef(SetActiveCommand::activeName(), kAllConnTypes, SetActiveCommand::kTransition+1) );
      if (var_def.isStateWithOrWithoutHistory() ) {
	//	scan_variables.addVariable(histogram_state_name);
	StateVarDef_t &state_var_def = var_def.stateDef();

	state_var_def.addState(SetActiveCommand::kTransition,   DefaultColourCode::kConfigured,   "Change Requested.");
	state_var_def.addState(SetActiveCommand::kEnabled,      DefaultColourCode::kSuccess,      "Enabled");
	state_var_def.addState(SetActiveCommand::kDisabled,     DefaultColourCode::kFailed,       "Disabled");

	application()->postEvent(this, new QNewVarEvent(kCurrent, 0, "General", SetActiveCommand::activeName() ) );

      }
      else {
	std::cout << "ERROR [ScanStatusMonitor::ctor] variable " << SetActiveCommand::activeName() << " already exists but it is not a state variable." << std::endl;
      }
    }

    // monitor module / ROD active states
    monitorManager()->addMonitor("Active", 
				 m_activeMonitor = std::shared_ptr<IsMonitorBase>(new IsActiveMonitor(receptor(), 
													PixCon::s_activeExtractor,
													".*/Active")));

    //    std::cout << "Started active monitor" << std::endl;

    m_actionStatusMonitor = std::shared_ptr<IStatusMonitor>(new ActionStatusMonitor(receptor(),
										      calibrationData(),
										      statusChangeTransceiver()));

    m_scanProgressMonitor = std::shared_ptr<IStatusMonitor>(new ScanProgressMonitor(receptor(),
										      monitorManager(),
										      calibrationData(),
										      statusChangeTransceiver(),
										      (scanStatusMonitor().calibrationDataVarName()) ));

    m_engine->setStatusMonitorActivator(std::shared_ptr<IStatusMonitorActivator>( new StatusMonitorActivator(scanStatusMonitorPtr(),
													       analysisStatusMonitorPtr(),
													       m_actionStatusMonitor,
													       m_scanProgressMonitor) ) );

    addStatusMonitor(m_actionStatusMonitor);
    addStatusMonitor(m_scanProgressMonitor);

    updateItems();

  }

  ConsoleViewer::~ConsoleViewer() { hide(); shutdown(); }

  bool ConsoleViewer::event(QEvent *an_event)
  {
    // @todo event processing should be done by the "parent" of all detector
    //       views and all views which show the right thing should be updated

    if (an_event && an_event->type() == QEvent::User) {
      PixCon::ProgressEvent *progress_event  = dynamic_cast< ProgressEvent *>(an_event);
      if (progress_event) {
	emit progress(progress_event->step(),progress_event->totalSteps());
	return true;
      }
    }

    return OHViewer::event(an_event);
  }

  void ConsoleViewer::connectivityChanged(PixCon::EMeasurementType measurement_type, PixCon::SerialNumber_t serial_number)
  {
    if (measurement_type==kCurrent && serial_number==0) {
      m_activeMonitor->update();
    }
    OHViewer::connectivityChanged(measurement_type, serial_number);
  }

  void ConsoleViewer::closeEvent(QCloseEvent* ce) {
    if (m_engine->executor()->isRunning()) {

      if (QMessageBox::warning(this,
			       "Abort running commands?",
			       "There are still running commands. Shall all running commands be aborted ? ",
			       QMessageBox::No,QMessageBox::Yes) == QMessageBox::No) {
	ce->ignore();
	return;
      }
    }

    //    QMessageBox  *message = new QMessageBox("Aborting commands.","Wait until commands are aborted.", QMessageBox::Information, QMessageBox::Ok,0,0,NULL);
    //    BusyLoop busy(application(), message ,1500,100 );
    shutdown();
    OHViewer::closeEvent(ce);
    ce->accept();
    //     shutdown();
    //     if (!isHidden()) {
    //       hide();
    //       application()->closeAllWindows();
    //     }
  }


  void ConsoleViewer::shutdown()
  {
    KillDialogue kill_dialog_timer(this, "Console",60);
    if (m_isShutdown) return;
    {
    std::unique_ptr<PixA::IBusyLoop> busy_loop(PixA::BusyLoopFactory::busyLoop(" Shutdown all background tasks ..."));
#ifdef HAVE_DDC
    std::cout << "INFO [ConsoleViewer::shutdown] dcs monitoring." << std::endl;
    m_dcsMonitorManager->shutdown();
#endif
    OHViewer::shutdown();

    m_canStatusPanel->initiateShutdown();
    std::cout << "INFO [ConsoleViewer::shutdown] shutdown IS receptor." << std::endl;
    receptor()->shutdown();

    std::cout << "INFO [ConsoleViewer::shutdown] shutdown executor." << std::endl;
    m_engine->executor()->shutdown();
    m_engine->executor()->shutdownWait();

    std::cout << "INFO [ConsoleViewer::shutdown] deallocate actions." << std::endl;
    m_engine->deallocate();
    std::cout << "INFO [ConsoleViewer::shutdown] shutdown can status monitor." << std::endl;
    m_canStatusPanel->shutdown();
    std::cout << "INFO [ConsoleViewer::shutdown] all done (really?)." << std::endl;

    m_isShutdown=true;
    }
  }


  void ConsoleViewer::allocateActions() {
    if (!m_engine->haveAllocatedActions() 
	|| QMessageBox::warning(this,
				"Deallocate current actions",
				"Deallocate the currently allocated actions ? ",
				QMessageBox::No,QMessageBox::Yes) == QMessageBox::Yes) {
      try {
      m_engine->deallocate();

      m_engine->reconnect();
      m_engine->updateConnectivity();
      PixCon::ConnectivityPanel cp(m_userMode->getUserMode(), *m_engine, application());
      if(cp.exec()==QDialog::Accepted){
	bool isSafe = true;
	if (m_userMode->getUserMode() != PixCon::UserMode::kExpert) {
	  isSafe = m_engine->loadPixelSafeCfg();
	}
	else {
	  DontPanelStandby notifyExpert;
	  if (notifyExpert.exec() == QDialog::Accepted) isSafe = true;
	  else isSafe = false;
	}
	if (isSafe) {
	  std::map<std::string, std::vector<std::string> > rodList;
	  cp.allocate(rodList);
	  std::shared_ptr<const PixLib::PixDisable> dis = cp.getDisable();
	  if (dis.get() != NULL) {
	    m_engine->setTmpDis(dis);
	  }
	  initActions();
	}
      }
      changeConnectivity(kCurrent,0);
      updateItems();
      }
      catch(std::exception &err) {
	std::cout << "FATAL [ConsoleViewer::allocateActions] Caught exception : " << err.what() << std::endl;
      }
      catch(...) {
	std::cout << "FATAL [ConsoleViewer::allocateActions] Caught unhandled exception." << std::endl;
      }
    }
  }

  void ConsoleViewer::initActions() {
    if (m_engine->haveAllocatedActions()) {
      ConfigurableCommandSequence *command_sequence = new ConfigurableCommandSequence("Init Actions");
      command_sequence->add( initialiseRodCommand(m_engine));
      //command_sequence->add( resetVisetCommand(m_engine) );
      command_sequence->add( DisableCommand::create(m_engine, m_engine->currentDisable(),true /*changes only*/, DisableCommand::kApplyAll) );

      m_commandList->executeCommand( command_sequence );
    }
  }

  void ConsoleViewer::toggleEnableState(PixCon::EConnItemType conn_item_type, const std::string &connectivity_name) {

    PixCon::Lock lock( calibrationData()->calibrationDataMutex() );
    try {
      bool new_state=false;
      if (conn_item_type==kModuleItem || conn_item_type==kRodItem) {
	CalibrationData::ConnObjDataList conn_object_data( calibrationData()->calibrationData().getConnObjectData( connectivity_name ) );
	CalibrationData::EnableState_t &state = conn_object_data.enableState(kCurrent, 0);
	if (state.isInConnectivity() && state.isInConfiguration()) {
	  // @todo warn if parent is not enabled.
	  new_state = !state.enabled();
	}
      }
      else if (conn_item_type==kPp0Item) {

	PixA::ModuleList module_list=calibrationData()->currentConnectivity().modules(connectivity_name);
	//PixA::Pp0List rod = module_list.parent();
	//PixA::RodList crate = rod.parent();

	unsigned int enabled=0;
	unsigned int disabled=0;
	PixA::ModuleList::const_iterator module_begin = module_list.begin();
	PixA::ModuleList::const_iterator module_end = module_list.end();
	for(PixA::ModuleList::const_iterator module_iter = module_begin;
	    module_iter != module_end;
	    ++module_iter) {

	  try {
	    CalibrationData::ConnObjDataList conn_object_data( calibrationData()->calibrationData().getConnObjectData( PixA::connectivityName(module_iter) ) );
	    const CalibrationData::EnableState_t &state = conn_object_data.enableState(kCurrent, 0);
	    CalibrationData::EnableState_t temp_state(state);
	    temp_state.setEnabled(true);
	    if (temp_state.enabled() ) {
	      if (state.enabled()) {
		enabled++;
	      }
	      else {
		disabled++;
	      }
	    }
	  }
	  catch( CalibrationData::MissingConnObject &) {
	  }

	}
	new_state = (disabled>0) ;
      }
      else if (conn_item_type==kCrateItem) {
	PixA::RodList rod_list=calibrationData()->currentConnectivity().rods(connectivity_name);

	PixA::RodList::const_iterator rod_begin = rod_list.begin();
	PixA::RodList::const_iterator rod_end = rod_list.end();

	unsigned int enabled=0;
	unsigned int disabled=0;
	for(PixA::RodList::const_iterator rod_iter = rod_begin;
	    rod_iter != rod_end;
	    ++rod_iter) {
	  try {

	    CalibrationData::ConnObjDataList conn_object_data( calibrationData()->calibrationData().getConnObjectData( connectivityName(rod_iter) ) );
	    const CalibrationData::EnableState_t &state = conn_object_data.enableState(kCurrent, 0);
	    CalibrationData::EnableState_t temp_state(state);
	    temp_state.setEnabled(true);
	    if (temp_state.enabled() ) {
	      if (state.enabled()) {
		enabled++;
	      }
	      else {
		disabled++;
	      }
	    }
	  }
	  catch( CalibrationData::MissingConnObject &) {
	  }

	}
	new_state = (disabled>0) ;

      }
      else {
	std::cout << "WARNING [ConsoleViewer::toggleEnableState] Nothing known about " << connectivity_name << "." << std::endl;
      }
      ICommand *set_active = new SetActiveCommand(m_engine, conn_item_type, connectivity_name, new_state);
      m_commandList->executeCommand( set_active );
    }
    catch (CalibrationData::MissingConnObject &err) {
    }
    std::cout << "WARNING [ConsoleViewer::toggleEnableState] Nothing known about " << connectivity_name << "." << std::endl;
  }

  void ConsoleViewer::selectConnectivityTags() {
    if (m_engine->haveAllocatedActions()) {

      std::unique_ptr<PixCon::IConfigurableCommand> change_tags( ChangeTagsKit::command(m_engine) );
      if (change_tags->configure()) {
	m_commandList->executeCommand( change_tags.release() );
      }
    }
  }

  void ConsoleViewer::showCloneTags() {
    CloneTags ctp(NULL,m_engine->partition().name(),"PixelDbServer");
    ctp.exec();
  }

  void ConsoleViewer::saveDisable() {

    PixA::ConnectivityRef conn(m_engine->getConn());
    if (!conn) {
      std::cerr << "ERROR [ConsoleViewer::saveDisable] No connectivity, don't know where to get tags from." << std::endl;
      return;
    }
    std::pair<PixCon::EMeasurementType, PixCon::SerialNumber_t>  measurement =selectedMeasurement();

    std::unique_ptr<ISelectionFactory> selection_factory( createSelectionFactory() );
    SaveDisable save_disable_dialog(conn,
				    measurement,
				    calibrationData(),
				    *selection_factory,
				    this);
    save_disable_dialog.execute();

  }

  void ConsoleViewer::showBocEditor() {
    if (m_bocEditor) {
      m_bocEditor->showEditor();
    }
  }

  void ConsoleViewer::updateItems() {
    bool have_allocated_actions = m_engine->haveAllocatedActions();
    /*
    for(std::vector<int>::const_iterator menu_entry_iter =m_actionItems.begin();
	menu_entry_iter != m_actionItems.end();
	++menu_entry_iter) {
      File->setItemEnabled(*menu_entry_iter, have_allocated_actions);
    }
    if (Utils->indexOf(kUtilsBocEditor) != -1) Utils->setItemEnabled(kUtilsBocEditor, have_allocated_actions);
    */
    for(std::vector<QAction*>::iterator act_iter = m_actionItems.begin(); act_iter!= m_actionItems.end(); act_iter++){
      (*act_iter)->setEnabled(have_allocated_actions);
    }
    if(m_bocEdtAct!=0) m_bocEdtAct->setEnabled(true);//have_allocated_actions);
  }

  void ConsoleViewer::updateAllCategories() {
    updateCategories(categories());
  }

  void ConsoleViewer::updateViewerForUserMode(PixCon::UserMode::EUserMode new_user) {
    if (new_user == PixCon::UserMode::kExpert) {
      mainWindowRightPanel->addTab(m_canStatusPanel, "CAN &Status");
      //Utils->setItemEnabled(kUtilsPCDCreator, true);
      m_pcdAct->setEnabled(true);
    }
    else {
      mainWindowRightPanel->removeTab(mainWindowRightPanel->indexOf(m_canStatusPanel));
      //Utils->setItemEnabled(kUtilsPCDCreator, false);
      m_pcdAct->setEnabled(false);
    }
  }

  void ConsoleViewer::showPCDCreator() {
    int answer = QMessageBox::warning(this, "WARNING",
				       "PCD Creator can be used to extract pixel calibration data from online calibration analyses\n"
				       "to create an sqlite file and update the pixel folder in COOL for use in offline reconstruction.\n\n" 
				       "PCD Creator is for experts only. Do you really want to continue ?",
				       QMessageBox::No, QMessageBox::Yes);
    if (answer == QMessageBox::Yes) {
      m_pcdCreator = new PixCalibDataOffline(this);
      m_pcdCreator->show();
    }
  }

}
