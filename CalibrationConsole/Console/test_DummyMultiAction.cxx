#include "test_DummyMultiAction.h"
#include "test_DummyActionController_impl.h"
//#include <PixUtilities/PixMessages.h>
#include <is/infodictionary.h>
#include <ConfigWrapper/ConnectivityUtil.h>
#include <ConfigWrapper/ConfigRef.h>
#include <ConfigWrapper/DbManager.h>
#include <ConfigWrapper/ConfObjUtil.h>

#include "isUtil.h"

#include <PixBroker/PixBroker.h>
#include <Lock.h>

#include <PixHistoServer/PixHistoServerInterface.h>
#include "test_DummyHistoInput.h"

#include <Histo/Histo.h>
#include <TH2.h>
#include <DataContainer/GenericHistogramAccess.h>

#include <algorithm>
#include <for_each.hh>


namespace PixA {
  template <class T_dest_, class T_src_>
  T_dest_ *transform2D(const T_src_ *h_src)
  {
    if (!h_src || !PixA::is2D(h_src) ) return NULL;

    T_dest_ *h_dest = PixA::createHisto<T_dest_>(PixA::histoName(h_src),
						 PixA::histoTitle(h_src),
						 PixA::nColumns(h_src),
						 PixA::axisMin(h_src,0),
						 PixA::axisMax(h_src,0),
						 PixA::nRows(h_src),
						 PixA::axisMin(h_src,1),
						 PixA::axisMax(h_src,1));

    float a_min=FLT_MAX;
    float a_max=-FLT_MAX;
    for (unsigned int col_i=0; col_i<PixA::nColumns(h_src); col_i++) {
      for (unsigned int row_i=0; row_i<PixA::nRows(h_src); row_i++) {
	float bin_content = PixA::binContent(h_src, PixA::startColumn(h_src)+col_i, PixA::startRow(h_src)+row_i);
	PixA::setBinContent(h_dest,
			    PixA::startColumn(h_dest)+col_i,
			    PixA::startRow(h_dest)+row_i,
			    bin_content);
	if (bin_content>a_max) a_max = bin_content;
	if (bin_content<a_min) a_min = bin_content;
      }
    }
    PixA::setMinMax(h_dest,a_min,a_max);
    return h_dest;
  }

  // template <class T_dest_, class T_src_>
  // T_src_ *transform1D<T_dest_>(const T_dest *h_src)
  // {
  //   if (!h_src || PixA::is2D(h_src) ) return NULL;

  //   T_dest_ *h_dest = PixA::createHisto(PixA::histoName(h_src),
  // 				      PixA::histoTitle(h_src),
  // 				      PixA::nColumns(h_src),
  // 				      PixA::axisMin(h_src,0),
  // 				      PixA::axisMax(h_src,0));

  //   float a_min=FLT_MAX;
  //   float a_max=-FLT_MAX;
  //   for (unsigned int col_i=0; col_i<PixA::nColumns(h_src); col_i++) {
  //     float bin_content = PixA::binContent(h_src, PixA::startColumn(h_src)+col_i);
  //       PixA::setBinContent(h_dest,
  // 			  PixA::startColumn(h_dest)+col_i,
  // 			  bin_content);
  //     if (bin_content>a_max) a_max = bin_content;
  //     if (bin_content<a_min) a_min = bin_content;
  //   }
  //   PixA::setMinMax(h_dest,a_min,a_max);
  //   return h_dest;
  // }

  template <class T_dest_1d_, class T_dest_2d_, class T_src_>
  T_dest_1d_ *transform(const T_src_ *h_src)
  {
    if (PixA::is2D(h_src)) {
      return transform2D<T_dest_2d_,T_src_>(h_src);
    }
    else {
      throw std::runtime_error("Not implemented.");
      //    return transform1D<T_dest_1d_,T_src_>(h_src);
    }
  }


}

namespace PixLib {

  PixCon::Mutex gRootDbMutex;

  DummyActionData::~DummyActionData(){  }

  std::string DummyActionData::makeIsName(CORBA::Long serial_number,
					  const std::string &crate_or_rod_name,
					  const std::string &rod_or_module_name,
					  const std::string &var_name) const {
    std::stringstream is_name;
    is_name << m_isServerName
	    << ".S" << std::setw(9) << std::setfill('0')
	    << serial_number
	    << '/' << crate_or_rod_name;
    if (!rod_or_module_name.empty()) {
      is_name << '/' << rod_or_module_name;
    }
    is_name << '/' << var_name;

    return is_name.str();
  }


  void DummyActionData::setHistoInputFileName(const std::string &histo_input_file_name)
  {
    m_histoInputFileName = histo_input_file_name;
  }

  PixCon::DummyHistoInput *DummyActionData::histoInput()
  {
    if (!m_dummyHistoInput.get()) {
      m_dummyHistoInput = std::unique_ptr<PixCon::DummyHistoInput>(new PixCon::DummyHistoInput(m_histoInputFileName, PixCon::DummyHistoInput::kRead));
      if (verbose()) {
	std::cout << "INFO [DummyActionData::setHistoInputFileName] set file name " << m_histoInputFileName << "." << std::endl;
      }
    }
    return m_dummyHistoInput.get();
  }

  void DummyActionData::closeHistoInput()
  {
    m_dummyHistoInput.reset();
  }


  const std::string DummyActionData::s_finalStatus[2]={std::string("DONE"),std::string("FAILED")};
  std::vector<std::string> DummyActionData::s_allHistogramNames;

  class Activation : public DummyActionData::IActionCommand
  {
  public:
    Activation(DummyActionData &action_data)
      : m_actionData(&action_data),
        m_activationProb(.80),
	m_activationWait(200) //ms
    {}

    void execute(bool &abort);

  private:
    DummyActionData                *m_actionData;
    float                           m_activationProb;
    unsigned int                    m_activationWait;
  };


  void Activation::execute(bool &abort)
  {

    std::map<std::string, std::shared_ptr<ISInfoInt> > enabled_modules;
    ISType is_int_type;
    {
      ISInfoInt temp;
      is_int_type =temp.type();
    }
    bool has_disabled_modules=false;

    timespec sleep;
    sleep.tv_sec=m_activationWait/1000;
    sleep.tv_nsec=(m_activationWait%1000)*1000000;

    int activation_rate = static_cast<int>(m_activationProb * RAND_MAX);

    PixA::ConnectivityRef conn=m_actionData->conn();
    if (!conn) return;

    do {

      nanosleep(&sleep, NULL);

      for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
	   crate_iter != conn.crates().end();
	   ++crate_iter) {

	PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
	PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
	for (PixA::RodList::const_iterator rod_iter = rod_begin;
	     rod_iter != rod_end;
	     ++rod_iter) {

	  if (!m_actionData->hasRod( PixA::connectivityName( rod_iter ) )) continue;

	  if (enabled_modules.find(PixA::connectivityName(rod_iter)) == enabled_modules.end()) {
	    if (rand() < activation_rate) {
	      std::pair< std::string, std::shared_ptr<ISInfoInt> > 
		new_elm = std::make_pair(PixA::connectivityName(rod_iter), std::shared_ptr<ISInfoInt>(new ISInfoInt));
	      new_elm.second->setValue(1);
	      setIsValue(*(m_actionData->dictionary()),
			 is_int_type,
			 m_actionData->makeActiveName(PixA::connectivityName(rod_iter)),
			 *(new_elm.second));
	      enabled_modules.insert( new_elm );
	    }
	    else {
	      has_disabled_modules =true;
	    }
	  }


	  PixA::Pp0List::const_iterator pp0_begin = PixA::pp0Begin(rod_iter);
	  PixA::Pp0List::const_iterator pp0_end = PixA::pp0End(rod_iter);
	  for (PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
	       pp0_iter != pp0_end;
	       ++pp0_iter) {

	    PixA::ModuleList::const_iterator module_begin = PixA::modulesBegin(pp0_iter);
	    PixA::ModuleList::const_iterator module_end = PixA::modulesEnd(pp0_iter);
	    for (PixA::ModuleList::const_iterator module_iter = module_begin;
		 module_iter != module_end;
		 ++module_iter) {

	      if (enabled_modules.find(PixA::connectivityName(module_iter)) == enabled_modules.end()) {
		if (rand() < activation_rate) {
		  std::pair< std::string, std::shared_ptr<ISInfoInt> > 
		    new_elm = std::make_pair(PixA::connectivityName(module_iter), std::shared_ptr<ISInfoInt>(new ISInfoInt));
		  new_elm.second->setValue(1);
		  setIsValue(*(m_actionData->dictionary()),
			     is_int_type,
			     m_actionData->makeActiveName(PixA::connectivityName(rod_iter),
							  PixA::connectivityName(module_iter)),
			     *(new_elm.second));
		  enabled_modules.insert( new_elm );
		}
		else {
		  has_disabled_modules =true;
		}
	      }

	    }
	  }

	}
      }
    } while (has_disabled_modules || abort);

    // disable all remaining modules
    for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
	 crate_iter != conn.crates().end();
	 ++crate_iter) {

      PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
      PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
      for (PixA::RodList::const_iterator rod_iter = rod_begin;
	   rod_iter != rod_end;
	   ++rod_iter) {

	if (enabled_modules.find(PixA::connectivityName(rod_iter)) == enabled_modules.end()) {
	  std::pair< std::string, std::shared_ptr<ISInfoInt> > 
	    new_elm = std::make_pair(PixA::connectivityName(rod_iter), std::shared_ptr<ISInfoInt>(new ISInfoInt));
	  new_elm.second->setValue(0);

	  setIsValue(*(m_actionData->dictionary() ),
		     is_int_type,
		     m_actionData->makeActiveName(PixA::connectivityName(rod_iter)),
		     *(new_elm.second));
	}

	PixA::Pp0List::const_iterator pp0_begin = PixA::pp0Begin(rod_iter);
	PixA::Pp0List::const_iterator pp0_end = PixA::pp0End(rod_iter);
	for (PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
	     pp0_iter != pp0_end;
	     ++pp0_iter) {

	  PixA::ModuleList::const_iterator module_begin = PixA::modulesBegin(pp0_iter);
	  PixA::ModuleList::const_iterator module_end = PixA::modulesEnd(pp0_iter);
	  for (PixA::ModuleList::const_iterator module_iter = module_begin;
	       module_iter != module_end;
	       ++module_iter) {
	    if (enabled_modules.find(PixA::connectivityName(module_iter)) == enabled_modules.end()) {

	      std::pair< std::string, std::shared_ptr<ISInfoInt> > 
		new_elm = std::make_pair(PixA::connectivityName(module_iter), std::shared_ptr<ISInfoInt>(new ISInfoInt));
	      new_elm.second->setValue(0);

	      setIsValue(*(m_actionData->dictionary() ),
			 is_int_type,
			 m_actionData->makeActiveName(PixA::connectivityName(rod_iter),
						      PixA::connectivityName(module_iter)),
			 *(new_elm.second));
	    }
	  }
	}

      }
    }

  }



  class PerformAction : public DummyActionData::IActionCommand
  {
  public:
    PerformAction(DummyActionData &action_data, std::vector<DummyActionData::Transition_t> &transitions)
      : m_actionData(&action_data),
	m_transitions(transitions)
    {}

    void execute(bool &abort);

  private:
    DummyActionData                *m_actionData;
    std::vector<DummyActionData::Transition_t> m_transitions;
  };



  void PerformAction::execute(bool &abort) {

    std::map<std::string, std::pair< unsigned short, std::shared_ptr<ISInfoString> > > rod_list;
    ISType is_string_type;
    {
      ISInfoString temp;
      is_string_type =temp.type();
    }

    PixA::ConnectivityRef conn=m_actionData->conn();
    if (!conn) return;

    unsigned int wait_rods;

    if (m_actionData->verbose()) {
      std::cout << "INFO [PerformAction::execute] start."
		<< std::endl;
    }

    do {

     unsigned int wait_time = 0;
     wait_rods=0;
     

     for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
	   crate_iter != conn.crates().end();
	   ++crate_iter) {

	PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
	PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
	for (PixA::RodList::const_iterator rod_iter = rod_begin;
	     rod_iter != rod_end;
	     ++rod_iter) {

	  if ( !m_actionData->hasRod( PixA::connectivityName( rod_iter ) )) continue;

	  std::map<std::string, std::pair< unsigned short, std::shared_ptr<ISInfoString> > >::iterator
	    rod_data_iter = rod_list.find(PixA::connectivityName(rod_iter) );

	  if (m_actionData->verbose()) {
	    std::cout << "INFO [PerformAction::execute] process " << PixA::connectivityName(rod_iter) ;
	    if (rod_data_iter != rod_list.end()) {
	      cout << " state = " << rod_data_iter->second.first;
	    }
	    else {
	      cout << " NEW ";
	    }
	    cout << std::endl;
	  }
	  unsigned int state_i = 0;
	  bool is_transition = true;
	  if ( rod_data_iter == rod_list.end()) {
	    std::pair< std::map<std::string, std::pair< unsigned short, std::shared_ptr<ISInfoString> > >::iterator, bool >
	      new_elm = rod_list.insert( std::make_pair(PixA::connectivityName(rod_iter),
							std::make_pair(static_cast<unsigned short>(0),
								       std::shared_ptr<ISInfoString>(new ISInfoString))) );
	    rod_data_iter = new_elm.first;
	  }
	  else {
	    state_i = (rod_data_iter->second.first)/2;
	    is_transition = ((rod_data_iter->second.first%2)==0);

	    if (state_i >= m_transitions.size()) continue;

	    if (is_transition) {
	      if (rand() >= m_transitions[state_i].transitionProb()  * RAND_MAX) {

		if (m_actionData->verbose()) {
		  std::cout << "INFO [PerformAction::execute] " << PixA::connectivityName(rod_iter) << " waiting in " << state_i 
			    << " for transition." << std::endl ;
		}
		wait_rods++;

		continue;
	      }
	    }
	    else {
	      if (rand() >= m_transitions[state_i].stateChangeProb()  * RAND_MAX) {

		if (m_actionData->verbose()) {
		  std::cout << "INFO [PerformAction::execute] " << PixA::connectivityName(rod_iter) << " waiting for termination of "
			    << rod_data_iter->second.second->getValue() << " (" << state_i << ")"
			    << std::endl ;
		}
		wait_rods++;

		continue;
	      }
	    }
	  }

	  if ( rod_data_iter->second.first/2 >= m_transitions.size()) continue;

	  if (is_transition) {
	    rod_data_iter->second.second->setValue( m_transitions[state_i].stateName() );

	    if (m_actionData->verbose()) {
	      std::cout << "INFO [PerformAction::execute] transition for " << PixA::connectivityName(rod_iter) << " to state  "
			<< rod_data_iter->second.second->getValue()
			<< " (" << state_i << ")"
			<< std::endl;
	    }

	    setIsValue(*(m_actionData->dictionary()),
		       is_string_type,
		       m_actionData->makeActionName(PixA::connectivityName(crate_iter),
						    PixA::connectivityName(rod_iter)),
		       *(rod_data_iter->second.second));

	    rod_data_iter->second.second->setValue( m_transitions[state_i].stateName());
	    wait_time += m_transitions[state_i].stateTime();
	    wait_rods++;

	  }
	  else {
	    bool success=true;
	    if (rand() < m_transitions[state_i].failProb() * RAND_MAX) {
	      success=false;
	    }

	    if (m_actionData->verbose()) {
	      std::cout << "INFO [PerformAction::execute] processing of " << PixA::connectivityName(rod_iter) << " in state "
			<< rod_data_iter->second.second->getValue()
			<< " was " << (success ? " a success " : " a failure")
			<< "." << std::endl;
	    }

	    rod_data_iter->second.second->setValue( m_actionData->finalStatus(success)  );
	    setIsValue(*(m_actionData->dictionary()),
		       is_string_type,
		       m_actionData->makeActionStatusName(PixA::connectivityName(crate_iter),
							  PixA::connectivityName(rod_iter)),
		       *(rod_data_iter->second.second));
	    wait_rods++;
	  }

	  if ( rod_data_iter->second.first/2 < m_transitions.size()) {
	    rod_data_iter->second.first++;

	    if (m_actionData->verbose()) {
	      if (rod_data_iter->second.first/2>=m_transitions.size()) {
		std::cout << "INFO [PerformAction::execute] done with " << PixA::connectivityName(rod_iter) << "."
			  << std::endl;
	      }
	    }

	  }

	}
      }

      timespec sleep;
      double the_wait_time = static_cast<double>(wait_time) / wait_rods;

      sleep.tv_sec=static_cast<unsigned int> (the_wait_time/1000);
      sleep.tv_nsec=static_cast<unsigned int> ((the_wait_time-sleep.tv_sec*1000)*1000000);

      nanosleep(&sleep, NULL);
    } while (!abort && wait_rods>0);

    if (m_actionData->verbose()) {
      std::cout << "INFO [PerformAction::execute] done."
		<< std::endl;
    }

  }


  class SetPixScanAction : public DummyActionData::IActionCommand
  {
  public:
    SetPixScanAction(DummyActionData &action_data, const std::string &file_name )
      : m_actionData(&action_data),
	m_fileName(file_name)
    {}

    void execute(bool &abort)
    {
      if (!m_fileName.empty() && !abort) {
	try {
	  PixCon::Lock lock(gRootDbMutex);
	  std::string file_name = m_fileName;
	  std::string::size_type pos = file_name.find(":");
	  if (pos != std::string::npos) {
	    file_name=m_fileName.substr(0,pos);
	  }
	  PixA::ConfigHandle scan_config_handle( PixA::DbManager::configHandle( PixA::DbManager::dbHandle(file_name) ) );
	  if (scan_config_handle) {
	    PixA::ConfigRef scan_config_ref( scan_config_handle.ref().createFullCopy());
	    PixA::ConfigHandle full_config(scan_config_ref);
	    scan_config_handle  = full_config;
	  }
	  m_actionData->setScanConfig(scan_config_handle);
	}
	catch(PixDBException &) {
	}
	catch(PixA::ConfigException &) {
	}
      }
    }

  private:
    DummyActionData   *m_actionData;
    std::string        m_fileName;
  };

  class ActiveAction : public DummyActionData::IActionCommand
  {
  public:
    ActiveAction(DummyActionData &action_data, const std::vector<std::string> &conn_names, bool enable, unsigned int active_wait=40)
      : m_actionData(&action_data),
	m_connNames(conn_names),
	m_activeWait(active_wait),
	m_enable(enable)
    {}
    ActiveAction(DummyActionData &action_data, const std::string &conn_name, bool enable)
      : m_actionData(&action_data),
	m_activeWait(50),
	m_enable(enable)
    { m_connNames.push_back(conn_name);}

    void execute(bool &abort);

  private:
    DummyActionData                *m_actionData;
    std::vector<std::string>        m_connNames;
    unsigned int                    m_activeWait;
    bool                            m_enable;
  };

  void ActiveAction::execute(bool &abort) {

    if (m_actionData->verbose()) {
      std::cout << "INFO [ActiveAction::execute] Set ";
      for (std::vector<std::string>::const_iterator conn_iter = m_connNames.begin();
	   conn_iter != m_connNames.end();
	   ++conn_iter) {
	std::cout << *conn_iter << " ";
      }
      std::cout << " to " << m_enable
		<< std::endl;
    }


    std::map<std::string, std::shared_ptr<ISInfoInt> > &enabled_modules = m_actionData->activeState();
    ISType is_int_type;
    {
      ISInfoInt temp;
      is_int_type =temp.type();
    }

    timespec sleep;
    sleep.tv_sec=m_activeWait/1000;
    sleep.tv_nsec=(m_activeWait%1000)*1000000;


    PixA::ConnectivityRef conn=m_actionData->conn();
    if (!conn) return;

    {

      nanosleep(&sleep, NULL);

      for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
	   crate_iter != conn.crates().end();
	   ++crate_iter) {

	PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
	PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
	for (PixA::RodList::const_iterator rod_iter = rod_begin;
	     rod_iter != rod_end;
	     ++rod_iter) {

	  if (!m_actionData->hasRod( PixA::connectivityName( rod_iter ) )) continue;

	  if (std::find(m_connNames.begin(),m_connNames.end(),PixA::connectivityName(rod_iter)) != m_connNames.end()) {

	    std::map< std::string, std::shared_ptr<ISInfoInt> >::iterator enable_iter = enabled_modules.find(PixA::connectivityName(rod_iter));
	    if (enable_iter  == enabled_modules.end()) {
	      std::pair< std::map< std::string, std::shared_ptr<ISInfoInt> >::iterator, bool>
		ret = enabled_modules.insert( std::make_pair(PixA::connectivityName(rod_iter), std::shared_ptr<ISInfoInt>(new ISInfoInt)) );
	      enable_iter = ret.first;
	    }

	    enable_iter->second->setValue(m_enable);
	    setIsValue(*(m_actionData->dictionary()),
		       is_int_type,
		       m_actionData->makeActiveName(PixA::connectivityName(rod_iter)),
		       *(enable_iter->second));
	  }



	  PixA::Pp0List::const_iterator pp0_begin = PixA::pp0Begin(rod_iter);
	  PixA::Pp0List::const_iterator pp0_end = PixA::pp0End(rod_iter);
	  for (PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
	       pp0_iter != pp0_end;
	       ++pp0_iter) {

	    PixA::ModuleList::const_iterator module_begin = PixA::modulesBegin(pp0_iter);
	    PixA::ModuleList::const_iterator module_end = PixA::modulesEnd(pp0_iter);
	    for (PixA::ModuleList::const_iterator module_iter = module_begin;
		 module_iter != module_end;
		 ++module_iter) {

	      if (std::find(m_connNames.begin(),m_connNames.end(),PixA::connectivityName(module_iter)) != m_connNames.end()) {

		std::map< std::string, std::shared_ptr<ISInfoInt> >::iterator enable_iter = enabled_modules.find(PixA::connectivityName(module_iter));
		if (enable_iter  == enabled_modules.end()) {
		  std::pair< std::map< std::string, std::shared_ptr<ISInfoInt> >::iterator, bool>
		    ret = enabled_modules.insert( std::make_pair(PixA::connectivityName(module_iter), std::shared_ptr<ISInfoInt>(new ISInfoInt)) );
		  enable_iter = ret.first;
		}

		enable_iter->second->setValue(m_enable);

		setIsValue(*(m_actionData->dictionary()),
			   is_int_type,
			   m_actionData->makeActiveName(PixA::connectivityName(rod_iter),
							PixA::connectivityName(module_iter)),
			   *(enable_iter->second));
	      }
	    }
	  }
	}
      }
    }

    if (m_actionData->verbose()) {
      std::cout << "INFO [ActiveAction::execute] done.";
    }

  }


  class ScanAction : public DummyActionData::IActionCommand
  {
  public:
    ScanAction(DummyActionData &action_data,
	       CORBA::Long serial_number,
	       CORBA::ULong a_time=300,
	       CORBA::ULong get_stuck_after_n_steps=static_cast<unsigned int>(-1),
	       CORBA::ULong fail_after_n_steps=static_cast<unsigned int>(-1),
	       const std::map<std::string, PixCon::DummyHistoInput::EQuality> &histo_quality_list = s_emptyList)
      : m_actionData(&action_data),
	m_serialNumber(serial_number),
	m_time(a_time),
	m_getStuckAfterNSteps(get_stuck_after_n_steps),
	m_failAfterNSteps(fail_after_n_steps),
	m_histoQualityList(histo_quality_list),
	m_isStuck(false)
    {}

    void execute(bool &abort)
    {
      if (m_actionData->scanConfig() && !abort ) {
	try {
	  std::vector<unsigned int> n_loops=getLoops();

	  PixA::ConnectivityRef conn=m_actionData->conn();
	  if (!conn) return;

	  if (m_actionData->verbose()) {
	    std::cout << "INFO [PerformAction::execute] start."
		      << std::endl;
	  }

	  std::map< std::string, std::vector<ISInfoT<int> > > idx_list;
	  std::map< std::string, std::vector<bool> > reset_list;
	  std::vector< ISInfoT<std::string> > action_names;
	  std::map< std::string, ISInfoT<std::string>  > scan_status;
	  m_totalCounter=0;

	  for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
	       crate_iter != conn.crates().end();
	       ++crate_iter) {

	    PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
	    PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
	    for (PixA::RodList::const_iterator rod_iter = rod_begin;
		 rod_iter != rod_end;
		 ++rod_iter) {
	      if ( !m_actionData->hasRod( PixA::connectivityName( rod_iter ) )) continue;
	      idx_list[ PixA::connectivityName( rod_iter ) ].resize( n_loops.size(), 0 );
	      reset_list[ PixA::connectivityName( rod_iter ) ].resize( n_loops.size(), false );

	      action_names.push_back( ISInfoT<std::string>() );
	      action_names.back().setValue("scan");
	      ISType is_type=action_names.back().type();
	      setIsValue(*(m_actionData->dictionary()),
			 is_type,
			 m_actionData->makeActionName(PixA::connectivityName(crate_iter),
						      PixA::connectivityName(rod_iter)),
			 action_names.back());

	      ISInfoT<std::string> &a_scan_status = scan_status[ PixA::connectivityName(rod_iter) ];
	      a_scan_status.setValue("PROCESSING");

	      ISType is_string_type=a_scan_status.type();
	      setIsValue(*(m_actionData->dictionary()),
			 is_string_type,
			 m_actionData->makeActionStatusName(m_serialNumber,
							    PixA::connectivityName(crate_iter),
							    PixA::connectivityName(rod_iter)),
			 a_scan_status);

	    }
	  }

	  if (n_loops.size()>0) {
	    bool something_to_do=true;
	    for (;something_to_do; ) {
	      if (abort) break;

	      something_to_do=false;
	      for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
		   crate_iter != conn.crates().end();
		   ++crate_iter) {
		if (abort) break;

		PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
		PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
		for (PixA::RodList::const_iterator rod_iter = rod_begin;
		     rod_iter != rod_end;
		     ++rod_iter) {
		  if (abort) break;
		  if ( !m_actionData->hasRod( PixA::connectivityName( rod_iter ) )) continue;

		  std::vector<ISInfoT<int> > &idx = idx_list [ PixA::connectivityName( rod_iter ) ];
		  std::vector< bool > &reset = reset_list [ PixA::connectivityName( rod_iter ) ];

		  if (idx.empty()) { throw std::logic_error("The index list should not be empty.");}
		  ISType is_type=idx[0].type();

		  if (scan_status[ PixA::connectivityName(rod_iter) ].getValue() == "DONE" ) continue;

		  if (m_isStuck || rand() < static_cast<int>(RAND_MAX * s_delayProb)) {
		    something_to_do =true;
		    continue;
		  }

		  bool executed_reset=false;
		  for (unsigned int index_i=0; index_i<n_loops.size(); index_i++) {
		    if (reset[index_i] && idx[index_i].getValue() != 0) {
		      idx[index_i].setValue(0);
		      setIsValue(*(m_actionData->dictionary()),
				 is_type,
				 makeIdxName(PixA::connectivityName(crate_iter),
					     PixA::connectivityName(rod_iter),
					     index_i),
				 idx[index_i]);
		      reset[index_i]=false;
		      executed_reset = true;
		      something_to_do=true;
		      break;
		    }
		  }
		  if (!executed_reset) {

		  for (unsigned int index_i=n_loops.size(); index_i-->0; ) {
		    if (abort) break;
		    if (m_totalCounter < m_failAfterNSteps && static_cast<unsigned int>(idx[index_i].getValue()+1)<n_loops[index_i]) {


		      idx[index_i].setValue(idx[index_i].getValue()+1);
		      setIsValue(*(m_actionData->dictionary()),
				 is_type,
				 makeIdxName(PixA::connectivityName(crate_iter),
					     PixA::connectivityName(rod_iter),
					     index_i),
				 idx[index_i]);

		      m_totalCounter++;
		      if (m_totalCounter > m_getStuckAfterNSteps ) {
			if (m_actionData->verbose()) {
			  std::cout << "INFO [PerformAction::execute] " << PixA::connectivityName(rod_iter) << " is stuck."
				    << std::endl;
			}
			m_isStuck=true;
		      }
		      something_to_do=true;
		      break;
		    }
		    else{
		      reset[index_i]=true;

		      if (m_totalCounter>= m_failAfterNSteps || index_i==0) {
			setIsValue(*(m_actionData->dictionary()),
				   is_type,
				   makeIdxName(PixA::connectivityName(crate_iter),
					       PixA::connectivityName(rod_iter),
					       index_i),
				   idx[index_i]);
			ISInfoT<std::string> &a_scan_status = scan_status[ PixA::connectivityName(rod_iter) ];
			if (m_totalCounter>=m_failAfterNSteps) {
			  if (m_actionData->verbose()) {
			    std::cout << "INFO [PerformAction::execute] " << PixA::connectivityName(rod_iter) << " failed."
				      << std::endl;
			  }
			  a_scan_status.setValue("FAILED");
			}
			else {
			  a_scan_status.setValue("DONE");
			  publishHistograms(rod_iter);
			}
			ISType is_string_type=a_scan_status.type();
			setIsValue(*(m_actionData->dictionary()),
				   is_string_type,
				   m_actionData->makeActionStatusName(m_serialNumber,
								      PixA::connectivityName(crate_iter),
								      PixA::connectivityName(rod_iter)),
				   a_scan_status);

			if (m_actionData->verbose()) {
			  std::cout << "INFO [PerformAction::execute] " << PixA::connectivityName(rod_iter) << " final counters : ";
			  for (unsigned int temp_index_i=0; temp_index_i<n_loops.size(); temp_index_i++ ) {
			    std::cout << "temp_index_i:" << idx[temp_index_i].getValue() << "  ";
			  }
			  std::cout << std::endl;
			}


			break;
		      }

		      //something_to_do=true;
		    }
		  }

		  }
		}
	      }

	      timespec sleep;
	      sleep.tv_sec=m_time/1000;
	      sleep.tv_nsec=(m_time%1000)*1000000;
	      nanosleep(&sleep, NULL);
	    }

	    if (abort) {
	      for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
		   crate_iter != conn.crates().end();
		   ++crate_iter) {

		PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
		PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
		for (PixA::RodList::const_iterator rod_iter = rod_begin;
		     rod_iter != rod_end;
		     ++rod_iter) {
		  if ( !m_actionData->hasRod( PixA::connectivityName( rod_iter ) )) continue;

		  ISInfoT<std::string> &a_scan_status = scan_status[ PixA::connectivityName(rod_iter) ];
		  if (a_scan_status.getValue()=="PROCESSING") {
		    a_scan_status.setValue("ABORTED");
		    ISType is_string_type=a_scan_status.type();
		    setIsValue(*(m_actionData->dictionary()),
			       is_string_type,
			       m_actionData->makeActionStatusName(m_serialNumber,
								  PixA::connectivityName(crate_iter),
								  PixA::connectivityName(rod_iter)),
			       a_scan_status);
		  }
		}
	      }
	    }
	  }
	}
	catch(PixDBException &) {
	}
	catch(PixA::ConfigException &) {
	}
      }

      PixCon::Lock root_db_log(gRootDbMutex);
      PixCon::Lock lock(m_actionData->histoInputMutex());
      m_actionData->closeHistoInput();
    }

  protected:
    std::string makeIdxName(const std::string &crate_name, const std::string &rod_name, unsigned int index_i)
    {
      if (index_i==3) {
	return m_actionData->makeIsName(crate_name, rod_name, "Mask stage");
      }
      else {
	std::stringstream idx_name;
	idx_name << "IDX-" << (index_i<3 ? 2-index_i : index_i);
	return m_actionData->makeIsName(crate_name, rod_name, idx_name.str());
      }
    }

    std::vector<unsigned int> getLoops()
    {
      PixCon::Lock lock(gRootDbMutex);
      std::vector<unsigned int>  n_loops; n_loops.resize(4,0);

      PixA::ConfigRef scan_config(m_actionData->scanConfig().ref());
      const PixA::ConfGrpRef &loop_config(scan_config["loops"]);
      for (unsigned int order_loop_i=3; order_loop_i-->0; ) {
	unsigned int loop_i = 2-order_loop_i;
	std::stringstream loop_idx;
	loop_idx << loop_i;

	if (!confVal<bool>(loop_config[std::string("activeLoop_")+loop_idx.str()]))  continue;

	bool uniform = confVal<bool>(loop_config[std::string("loopVarUniformLoop_")+loop_idx.str()]);

	if (uniform)  {
	  unsigned int   steps   =confVal<int>(loop_config[std::string("loopVarNStepsLoop_")+loop_idx.str()]);
	  n_loops[order_loop_i]=steps;
	}
	else {
	  const std::vector<float> &
	    values ( confVal<std::vector<float> >(loop_config[std::string("loopVarValuesLoop_")+loop_idx.str()]));
	  n_loops[order_loop_i]=values.size();
	}
      }

      {
	const PixA::ConfGrpRef &general_config(scan_config["general"]);
	n_loops[3]=confVal<int>(general_config[std::string("maskStageSteps")]);
      }
      return n_loops;
    }

    void publishHistograms(PixA::RodList::const_iterator rod_iter)
    {
      PixCon::DummyHistoInput::EQuality rod_quality = histoQuality(PixCon::DummyHistoInput::kOk, PixA::connectivityName(rod_iter));
      const std::vector<std::string> histo_list = m_actionData->histoNameList();

      PixCon::Lock rootdb_lock(gRootDbMutex);
      PixCon::Lock histo_input_lock(m_actionData->histoInputMutex());
      if (!m_actionData->histoInput()) return;

	PixA::Pp0List::const_iterator pp0_begin = PixA::pp0Begin(rod_iter);
	PixA::Pp0List::const_iterator pp0_end  = PixA::pp0End(rod_iter);

	for (PixA::Pp0List::const_iterator pp0_iter=pp0_begin;
	     pp0_iter != pp0_end;
	     ++pp0_iter) {

	  PixCon::DummyHistoInput::EQuality pp0_quality = histoQuality(rod_quality, PixA::connectivityName(pp0_iter));

	  // it may be more efficient to call modulesBegin and modulesEnd only once:
	  PixA::ModuleList::const_iterator modules_begin=PixA::modulesBegin(pp0_iter);
	  PixA::ModuleList::const_iterator modules_end=PixA::modulesEnd(pp0_iter);
	  for (PixA::ModuleList::const_iterator module_iter=modules_begin;
	       module_iter != modules_end;
	       ++module_iter) {

	    PixCon::DummyHistoInput::EQuality module_quality = histoQuality(pp0_quality, PixA::connectivityName(module_iter));

      for( std::vector<std::string>::const_iterator histo_name_iter = histo_list.begin();
	   histo_name_iter != histo_list.end();
	   histo_name_iter++) {

	    HistoInfo_t info;
	    m_actionData->histoInput()->numberOfHistograms(*histo_name_iter, m_actionData->scanConfig(), module_quality,info);

	    PixA::Index_t index;
	    info.makeDefaultIndexList(index);
	    if ( index.size() > 0) {
	      PixA::Index_t min_index(index);

	      // loop over all indices
	      for( ;index[0]<info.maxIndex(0); ) {

		std::unique_ptr<TH1> a_root_histo( m_actionData->histoInput()->histo(*histo_name_iter, m_actionData->scanConfig(), module_quality, index) );
		if (a_root_histo.get() && a_root_histo->InheritsFrom(TH2::Class())) {
		  std::unique_ptr<PixLib::Histo> pixlib_histo( PixA::transform<PixLib::Histo,PixLib::Histo,TH2>(static_cast<TH2 *>(a_root_histo.get())) );
		  if (pixlib_histo.get()) {
		  std::string histogram_server_histo_name = makeHistogramServerHistoName(m_serialNumber,
											 PixA::connectivityName(rod_iter),
											 PixA::connectivityName(module_iter),
											 *histo_name_iter,index);
		  try {
		    m_actionData->histoServerInterface()->sendHisto(histogram_server_histo_name, *(pixlib_histo.get()) );
		  }
		  catch(...) {
		    std::cout << "ERROR [ScanAction::publishHistograms] Failed to sent histograms " << histogram_server_histo_name
			      << " for scan " << m_serialNumber
			      << " to histogram server."
			      << std::endl;
		    break;
		  }
		  }
		}

		// next index;
		unsigned int level_i=index.size();
		for (; level_i-->0; ) {
		  if (index[level_i]+1<info.maxIndex(level_i)) {
		    index[level_i]++;
		    break;
		  }
		  else {
		    index[level_i]=min_index[level_i];
		  }
		}
		if (level_i>index.size()) break;
	      }
	    }

	  }


	}
      }


    }

    static std::string makeHistogramServerHistoName(CORBA::Long scan_serial_number,
						    const std::string &rod_name,
						    const std::string &conn_name,
						    const std::string &histo_name,
						    const PixA::Index_t &index)
    {
      std::stringstream full_name;
      full_name << "/S" << std::setw(9) << std::setfill('0') << scan_serial_number << '/';
      if (!rod_name.empty()) {
	full_name << rod_name <<'/';
      }
      if (!conn_name.empty()) {
	full_name <<  conn_name << '/';
      }
      full_name << histo_name;

      char letter='A';
      for (PixA::Index_t::const_iterator level_iter = index.begin();
	   level_iter!= index.end();
	   level_iter++) {
	full_name << '/';
	if( index.end() - level_iter > 1) {
	  full_name << letter++;
	}
	full_name << *level_iter;
      }
      full_name << ':';
      return full_name.str();
    }

    PixCon::DummyHistoInput::EQuality histoQuality(PixCon::DummyHistoInput::EQuality default_quality,
						   const std::string &conn_name) const
    {
      std::map<std::string, PixCon::DummyHistoInput::EQuality>::const_iterator histo_quality = m_histoQualityList.find(conn_name);
      if ( histo_quality != m_histoQualityList.end() ) {
	return histo_quality->second;
      }
      return default_quality;
    }

  private:
    DummyActionData                *m_actionData;
    CORBA::Long                     m_serialNumber;

    unsigned int m_time;
    unsigned int m_getStuckAfterNSteps;
    unsigned int m_failAfterNSteps;

    unsigned int m_totalCounter;
    std::map<std::string, PixCon::DummyHistoInput::EQuality> m_histoQualityList;
    static std::map<std::string, PixCon::DummyHistoInput::EQuality> s_emptyList;

    bool m_isStuck;
    static const float s_delayProb;
  };

  const float ScanAction::s_delayProb = .1;
  std::map<std::string, PixCon::DummyHistoInput::EQuality> ScanAction::s_emptyList;


  DummyActionData::DummyActionData(IPCPartition &partition,
				   const std::string &is_server_name,
				   const std::string &oh_server_name,
				   const std::string &histo_name_server_name,
				   const PixA::ConnectivityRef &conn, bool verbose)
    : m_conn(conn),
      m_isDictionary(new ISInfoDictionary(partition)),
      m_isServerName(is_server_name),
      m_histoServerInterface(new  PixLib::PixHistoServerInterface(&partition, oh_server_name, histo_name_server_name,"DummyActionData")),
      m_verbose(verbose)
  {
  }

  std::vector<std::string> DummyActionData::histoNameList() {
    std::vector<std::string> histo_name_list;
    if (!scanConfig()) return histo_name_list;

    PixA::ConfigRef scan_config(scanConfig().ref());
    if (s_allHistogramNames.empty()) {
      s_allHistogramNames.push_back(std::string("LVL1"));
      s_allHistogramNames.push_back(std::string("OCCUPANCY"));
      s_allHistogramNames.push_back(std::string("TOT"));
      s_allHistogramNames.push_back(std::string("TOT_MEAN"));
      s_allHistogramNames.push_back(std::string("TOT_SIGMA"));
      s_allHistogramNames.push_back(std::string("FDAC_T"));
      s_allHistogramNames.push_back(std::string("FDAC_TOT"));
      s_allHistogramNames.push_back(std::string("GDAC_T"));
      s_allHistogramNames.push_back(std::string("GDAC_THR"));
      s_allHistogramNames.push_back(std::string("IF_T"));
      s_allHistogramNames.push_back(std::string("IF_TOT"));
      s_allHistogramNames.push_back(std::string("SCURVE_CHI2"));
      s_allHistogramNames.push_back(std::string("SCURVE_MEAN"));
      s_allHistogramNames.push_back(std::string("SCURVE_SIGMA"));
      s_allHistogramNames.push_back(std::string("TDAC_T"));
      s_allHistogramNames.push_back(std::string("TDAC_THR"));
      s_allHistogramNames.push_back(std::string("TIMEWALK"));

      s_allHistogramNames.push_back(std::string("RAW_DATA_0"));
      s_allHistogramNames.push_back(std::string("RAW_DATA_1"));
      s_allHistogramNames.push_back(std::string("RAW_DATA_DIFF_1"));
      s_allHistogramNames.push_back(std::string("RAW_DATA_DIFF_2"));
      s_allHistogramNames.push_back(std::string("RAW_DATA_REF"));
      s_allHistogramNames.push_back(std::string("FMTC_LINKMAP"));
      s_allHistogramNames.push_back(std::string("FMT_COUNTERS"));
    }
    try {
      const PixA::ConfGrpRef &histograms_config(scan_config["histograms"]);

      for (std::vector<std::string>::const_iterator histo_iter = s_allHistogramNames.begin();
	   histo_iter != s_allHistogramNames.end();
	   histo_iter++) {
	try {
	  if (   confVal<bool>(histograms_config[std::string("histogramFilled")+*histo_iter])
	      && confVal<bool>(histograms_config[std::string("histogramKept")+*histo_iter])) {
	    histo_name_list.push_back( *histo_iter );
	  }
	}
	catch(PixA::ConfigException &) {
	}
      }
    }
    catch(PixA::ConfigException &) {
    }
    return histo_name_list;
  }

  class GatherModuleNames : public std::unary_function<const PixA::ModuleList::const_iterator &, void>
  {
  public:
    GatherModuleNames(std::vector<std::string> *name_list)
      : m_nameList(name_list)
    { assert(name_list); }

    void operator()( const PixA::ModuleList::const_iterator &module_iter) {
      m_nameList->push_back(PixA::connectivityName(module_iter));
    }

  private:
    std::vector<std::string> *m_nameList;
  };


  DummySingleAction::DummySingleAction(IPCPartition &partition,
				       const std::string &is_server_name,
				       const std::string &oh_server_name,
				       const std::string &histo_name_server_name,
				       const PixA::ConnectivityRef &conn,
				       const std::set<std::string> &sub_action_list, bool verbose)
    : PixActions(&partition,( sub_action_list.size()==1 ? *(sub_action_list.begin()) : std::string("DUMMY_MULTI_ACTION")) ),
      m_actionData(partition,
		   is_server_name,
		   oh_server_name,
		   histo_name_server_name,
		   conn,
		   verbose),
      m_scanTimePerStep(300),
      m_getStuckAfterNSteps(static_cast<unsigned int>(-1)),
      m_failAfterNSteps(static_cast<unsigned int>(-1)),
      m_abort(false),
      m_shutdown(false)
  {
    delete m_is;
    m_is = new PixISManager(partition.name(), is_server_name);

    //    m_pixMessages=std::unique_ptr<PixMessages>(m_myMessages);`

    for(std::set<std::string>::const_iterator sub_action_iter = sub_action_list.begin();
	sub_action_iter != sub_action_list.end();
	sub_action_iter++) {

      unsigned int len = strlen("SINGLE_ROD:");
      if (sub_action_iter->compare(0,len, "SINGLE_ROD:")==0) {
	std::string::size_type pos = sub_action_iter->find("/",len+1);
	if (pos != std::string::npos && pos+1 < sub_action_iter->size()) {
	  m_actionData.addRod( sub_action_iter->substr(pos+1, sub_action_iter->size()-pos-1));
	  if (m_actionData.verbose()) {
	    std::cout << "INFO [DummySingleAction::ctor] add ROD " << sub_action_iter->substr(pos, sub_action_iter->size()-pos) << "." << std::endl;
	  }
	}
      }
    }

    m_descriptor.name = ( sub_action_list.size()==1 ? *(sub_action_list.begin()) : std::string("DUMMY_MULTI_ACTION"));
    m_descriptor.available = true;
    m_descriptor.type = SINGLE_ROD;
    m_descriptor.allocatingBrokerName = "";
    m_descriptor.managingBrokerName = "";


    std::vector<std::string> conn_names; 

    PixA::ConnectivityRef the_conn(actionData().conn());
    const PixA::DisabledListRoot empty_disabled_list;
    // set active RODs and modules
    GatherModuleNames gather_module_names(&conn_names);
    for (std::set<std::string>::const_iterator rod_iter = actionData().beginRod();
	 rod_iter != actionData().endRod();
	 ++rod_iter) {
      conn_names.push_back(*rod_iter);
      CAN::for_each_module(the_conn, *rod_iter,empty_disabled_list, gather_module_names );
    }

    addCommand(new ActiveAction(actionData(), conn_names, true /*active*/, 0 ));

    start_undetached();

  }

  DummySingleAction::~DummySingleAction() {
    if (m_actionData.verbose()) {
      std::cout << "INFO [DummySingleAction::dtor] start." <<  "." << std::endl;
    }
    if (m_ipcPartition) {
      for (std::set<std::string>::const_iterator rod_iter = actionData().beginRod();
	   rod_iter != actionData().endRod();
	   rod_iter++) {
	DummyActionController_impl::instance(*m_ipcPartition)->deregisterAction( *rod_iter );
      }
    }

    if (m_actionData.verbose()) {
      std::cout << "INFO [DummySingleAction::dtor] waiting until execution loop stopped." <<  "." << std::endl;
    }
    stop();
    if (m_actionData.verbose()) {
      std::cout << "INFO [DummySingleAction::dtor] Terminated." <<  "." << std::endl;
    }
  }

  void DummySingleAction::allocate      (const char* allocatingBrokerName)
  {
    if (m_actionData.verbose()) {
      std::cout << "INFO [DummySingleAction::allocate] allocated by " << allocatingBrokerName<<  "." << std::endl;
    }
    PixActions::allocate(allocatingBrokerName);
  }
  void DummySingleAction::deallocate    (const char* deallocatingBrokerName)
  {
    if (m_actionData.verbose()) {
      std::cout << "INFO [DummySingleAction::deallocate] Deallocated by " << deallocatingBrokerName<<  "." << std::endl;
    }
    PixActions::deallocate(deallocatingBrokerName);
  }

  void DummySingleAction::addCommand(DummyActionData::IActionCommand *a_command) {
    {
      PixCon::Lock lock(m_commandListMutex);
      m_commandList.push( std::shared_ptr<DummyActionData::IActionCommand>(a_command));
    }
    m_commandFlag.setFlag();
  }

  std::shared_ptr<DummyActionData::IActionCommand> DummySingleAction::getCommand() {
    PixCon::Lock lock(m_commandListMutex);
    std::shared_ptr<DummyActionData::IActionCommand> a_command = m_commandList.front();
    m_commandList.pop();
    return a_command;
  }


  void *DummySingleAction::run_undetached(void *)
  {
    while (!m_shutdown) {
      if (m_commandList.empty()) {
	m_commandFlag.wait(m_shutdown);
      }
      if (m_shutdown) break;
      if (m_commandList.empty()) continue;
      std::shared_ptr<DummyActionData::IActionCommand>  a_command = getCommand();
      try {
	a_command->execute( m_abort );
      }
      catch (...) {
      }
      m_abort=false;
    }
    m_exitFlag.setFlag();
    return NULL;
  }


  void DummySingleAction::initCal(PixActions::SyncType) {

    std::vector<DummyActionData::Transition_t> transitions;
    transitions.push_back(DummyActionData::Transition_t("reset",200,.01,.5, .8));
    transitions.push_back(DummyActionData::Transition_t("loadConfig",200,.05,.2, .8));
    addCommand(new PerformAction(actionData(),transitions ));
  }


  void DummySingleAction::resetRod(PixActions::SyncType) {
    if (m_actionData.conn()) {
      std::cout << "INFO [DummySingleAction::resetRod] reset : ";
      for (std::set<std::string>::const_iterator rod_iter = m_actionData.beginRod();
	   rod_iter != m_actionData.endRod();
	   ++rod_iter) {
	std::cout << " "<< *rod_iter;
      }
      std::cout << std::endl;
    }
  }

  void DummySingleAction::configureBoc(PixActions::SyncType) {
    if (m_actionData.conn()) {
      std::cout << "INFO [DummySingleAction::configureBoc] configure BOCs of  : ";
      for (std::set<std::string>::const_iterator rod_iter = m_actionData.beginRod();
	   rod_iter != m_actionData.endRod();
	   ++rod_iter) {
	std::cout << " "<< *rod_iter;
      }
      std::cout << std::endl;
    }
  }

  void DummySingleAction::loadConfig(PixActions::SyncType) {
    if (m_actionData.conn()) {
      std::cout << "INFO [DummySingleAction::loadConfig] load config of  : ";
      for (std::set<std::string>::const_iterator rod_iter = m_actionData.beginRod();
	   rod_iter != m_actionData.endRod();
	   ++rod_iter) {
	std::cout << " "<< *rod_iter;
      }
      std::cout << std::endl;
    }
  }
  
  void DummySingleAction::setPixScan(std::string config_file_name, PixActions::SyncType) {
    addCommand(new SetPixScanAction(actionData(), config_file_name));
  }

  void DummySingleAction::scan(std::string /*file_name*/, CORBA::Long serial_number, PixActions::SyncType) {
    addCommand(new ScanAction(actionData(),serial_number,m_scanTimePerStep, m_getStuckAfterNSteps, m_failAfterNSteps, m_histoQualityList));
  }


  void DummySingleAction::resetViset(PixActions::SyncType) {

    std::vector<DummyActionData::Transition_t> transitions;
    transitions.push_back(DummyActionData::Transition_t("resetViset",200,.01,.5, .8));
    addCommand(new PerformAction(actionData(),transitions ));
  }

  std::string DummySingleAction::getTag(PixA::IConnectivity::EConnTag tag_type) {
    if (m_actionData.conn()) {
      return m_actionData.conn().tag(tag_type);
    }
    return "";
  }

  void DummySingleAction::getRodActive  (std::string /*name*/, PixActions::SyncType /*sync*/) {
    assert(false);
  }

  void DummySingleAction::setRodActive(bool active, PixActions::SyncType sync) {
    if (actionData().beginRod() != actionData().endRod()) {
      std::string action_name("/");
      action_name += *(m_actionData.beginRod());
      setRodActive(action_name, active, sync);
    }
  }

  void DummySingleAction::setRodActive  (std::string action_name, bool active, PixActions::SyncType /*sync*/) {

    std::string::size_type pos=action_name.rfind("/");
    std::string rod_name;
    if (pos!=std::string::npos && pos+1 < action_name.size()) {
      rod_name=action_name.substr(pos+1,action_name.size()-pos);
      if (!actionData().hasRod(rod_name )) return;
    }

    std::vector<std::string> conn_names;

    PixA::ConnectivityRef conn=actionData().conn();
    if (!conn) return;

    {

      for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
	   crate_iter != conn.crates().end();
	   ++crate_iter) {

	PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
	PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
	for (PixA::RodList::const_iterator rod_iter = rod_begin;
	     rod_iter != rod_end;
	     ++rod_iter) {

	  if (!actionData().hasRod( PixA::connectivityName( rod_iter ) ) || (!rod_name.empty() && PixA::connectivityName( rod_iter )!=rod_name)) continue;

	  if (actionData().verbose()) {
	    std::cout << "INFO [DummySingleAction::setModuleActive] " << action_name << "  " << PixA::connectivityName( rod_iter ) << std::endl;
	  }
	  conn_names.push_back(PixA::connectivityName(rod_iter));
	}
      }
    }
    addCommand(new ActiveAction(actionData(),conn_names,active));

  }

  void DummySingleAction::setModuleActive(std::string action_name, PixLibLong module_id, bool active, PixActions::SyncType) {
    std::string::size_type pos=action_name.rfind("/");
    std::string rod_name;
    if (pos!=std::string::npos && pos+1 < action_name.size()) {
      rod_name=action_name.substr(pos+1,action_name.size()-pos);
      if (!actionData().hasRod(rod_name )) return;
    }
    std::vector<std::string> conn_names;

    PixA::ConnectivityRef conn=actionData().conn();
    if (!conn) return;

    for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
	 crate_iter != conn.crates().end();
	 ++crate_iter) {

      PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
      PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
      for (PixA::RodList::const_iterator rod_iter = rod_begin;
	   rod_iter != rod_end;
	   ++rod_iter) {

	if (!actionData().hasRod( PixA::connectivityName( rod_iter ) ) || (!rod_name.empty() && PixA::connectivityName( rod_iter )!=rod_name)) continue;

	if (actionData().verbose()) {
	  std::cout << "INFO [DummySingleAction::setModuleActive] " << action_name << "  " << PixA::connectivityName( rod_iter ) << std::endl;
	}

	PixA::Pp0List::const_iterator pp0_begin = PixA::pp0Begin(rod_iter);
	PixA::Pp0List::const_iterator pp0_end = PixA::pp0End(rod_iter);
	for (PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
	     pp0_iter != pp0_end;
	     ++pp0_iter) {

	  PixA::ModuleList::const_iterator module_begin = PixA::modulesBegin(pp0_iter);
	  PixA::ModuleList::const_iterator module_end = PixA::modulesEnd(pp0_iter);
	  for (PixA::ModuleList::const_iterator module_iter = module_begin;
	       module_iter != module_end;
	       ++module_iter) {
	    if( static_cast<unsigned int>(module_id) == PixA::modId(module_iter)) {
	      conn_names.push_back(PixA::connectivityName(module_iter));
	    }
	  }
	}
      }
    }
    addCommand(new ActiveAction(actionData(), conn_names, active ));
  }

  void DummySingleAction::getModuleActive(std::string, PixLibLong, PixActions::SyncType) {
    assert(false);
  }
  
  
  void DummySingleAction::setDuration( CORBA::ULong needed_time ) {
    m_scanTimePerStep = needed_time;
    if (actionData().verbose()) {
      for (std::set<std::string>::const_iterator rod_iter = actionData().beginRod();
	   rod_iter != actionData().endRod();
	   rod_iter++) {
	std::cout << "INFO [DummySingleAction::setDuration] A scan step on " << *rod_iter << " whill take "<< m_scanTimePerStep << " ms." << std::endl;
      }
    }
  }

  void DummySingleAction::getStuckAfterNSteps( CORBA::ULong n_steps) {
    m_getStuckAfterNSteps = n_steps;
    if (actionData().verbose()) {
      for (std::set<std::string>::const_iterator rod_iter = actionData().beginRod();
	   rod_iter != actionData().endRod();
	   rod_iter++) {
	std::cout << "INFO [DummySingleAction::getStuckAfterNSteps] " << *rod_iter << " whill get stuck down after "<< m_getStuckAfterNSteps << "." << std::endl;
      }
    }
  }
  void DummySingleAction::failAfterNSteps( CORBA::ULong fail_after_n_steps) {
    m_failAfterNSteps = fail_after_n_steps;
    if (actionData().verbose()) {
      for (std::set<std::string>::const_iterator rod_iter = actionData().beginRod();
	   rod_iter != actionData().endRod();
	   rod_iter++) {
	std::cout << "INFO [DummySingleAction::failAfterNSteps] " << *rod_iter << " whill fail after "<< m_failAfterNSteps << "." << std::endl;
      }
    }
  }

  void DummySingleAction::setHistoInputFileName(const char *file_name) {
    actionData().setHistoInputFileName(file_name);
  }

  bool DummySingleAction::inCharge(const std::string &conn_name)
  {
    PixA::ConnectivityRef conn(actionData().conn());
    const PixLib::RodBocConnectivity *rod_conn = conn.findRod(conn_name);
    if (!rod_conn) {
      const PixLib::ModuleConnectivity *module_conn = conn.findModule(conn_name);
      if (module_conn) {
	rod_conn = PixA::rodBoc(module_conn);
      }
      else {
	const PixLib::Pp0Connectivity *pp0_conn = conn.findPp0(conn_name);
	if (pp0_conn) {
	  rod_conn = PixA::rodBoc(pp0_conn);
	}
      }
    }
    if (!rod_conn) return false;
    std::set<std::string>::const_iterator rod_iter = actionData().findRod(PixA::connectivityName(rod_conn) );
    if (rod_iter == actionData().endRod()) return false;
    return true;
  }

  void DummySingleAction::setHistoQuality(const char *conn_name, PixCon::DummyHistoInput::EQuality quality) {
    if (!inCharge(conn_name)) return;

    if (quality>=PixCon::DummyHistoInput::kNQualityLevels) {

      std::map<std::string, PixCon::DummyHistoInput::EQuality>::iterator quality_iter = m_histoQualityList.find(conn_name);
      if (quality_iter != m_histoQualityList.end()) {
	m_histoQualityList.erase(quality_iter);
	if (m_actionData.verbose()) {
	  std::cout << "INFO [DummyActionData::setHistoQuality] revert histo quality setting for  " << conn_name << " to default : Ok." << std::endl;
	}
      }
    }
    else {
      m_histoQualityList[conn_name]=quality;
      if (m_actionData.verbose()) {
	std::cout << "INFO [DummyActionData::setHistoQuality] set histo quality setting for  " << conn_name << " to : " << quality << "." << std::endl;
      }
    }
  }

  void DummySingleAction::clearHistoQuality() {
    m_histoQualityList.clear();
    if (m_actionData.verbose()) {
      std::cout << "INFO [DummyActionData::setHistoQuality] Revert all histo quality settings  to the  default : Ok." << std::endl;
    }
  }


  DummyMultiAction::DummyMultiAction(IPCPartition &partition, const PixA::ConnectivityRef &conn, const std::string &name, bool verbose)
    : PixActions(&partition,name),
      m_conn(conn),
      m_verbose(verbose)
  {
    //    m_pixMessages=std::unique_ptr<PixMessages>(m_myMessages);
  }

  template<class T> inline void controllDestryer(T * x ) {
    x->_destroy();
  }


  DummyMultiAction::~DummyMultiAction() {
    std::cout << "INFO [DummyMultiAction::dtor] actions=" << m_actionList.size() << std::endl;
    for (std::map<std::string, std::shared_ptr<PixActions> >::iterator  action_iter = m_actionList.begin();
	 action_iter != m_actionList.end();
	 action_iter++) {

      DummySingleAction *single_action = dynamic_cast<DummySingleAction *>(action_iter->second.get());
      if (single_action ) {
	for (std::set<std::string>::const_iterator rod_iter = single_action->actionData().beginRod();
	     rod_iter != single_action->actionData().endRod();
	     rod_iter++) {
	  DummyActionController_impl::instance(*m_ipcPartition)->deregisterAction( *rod_iter);
	}
      }
      std::cout << "INFO [DummyMultiAction::dtor] " << action_iter->first << " used = " << action_iter->second.use_count() << std::endl;
    }

    m_actionList.clear();
    std::cout << "INFO [DummyMultiAction::dtor] actions=" << m_actionList.size() << std::endl;
  }

  PixActions *DummyMultiAction::addAction(const std::string &action_name, PixActions *an_action)
  {
    if (m_actionList.find(action_name) == m_actionList.end()) {
    std::pair<std::map<std::string, std::shared_ptr< PixActions > >::iterator, bool> 
      ret = m_actionList.insert(std::make_pair(action_name, 
					       std::shared_ptr<PixActions>(an_action,destroyer<PixActions>)));
    if (!ret.second && m_verbose) {
      std::cout << "ERROR [DummyMultiAction::addAction] failed to add action " << action_name << ". Maybe it exists already ? "
		<< std::endl;
    }
    if (ret.second) {
      if (m_ipcPartition) {
	DummySingleAction *dummy_single_action = dynamic_cast<DummySingleAction *>(ret.first->second.get());
	if (dummy_single_action) {
	  for (std::set<std::string>::const_iterator rod_iter = dummy_single_action->actionData().beginRod();
	       rod_iter != dummy_single_action->actionData().endRod();
	       rod_iter++) {
	    DummyActionController_impl::instance(*m_ipcPartition)->registerAction( *rod_iter, ret.first->second );
	  }
	}
      }
      return ret.first->second.get();
    }
    else {
      return NULL;
    }
    }
    else {
      std::cout << "ERROR [DummyMultiAction::addAction] Action " << action_name << " already exists. Cannot add the action again. "
		<< std::endl;
      return NULL;
    }
  }

  std::string DummyMultiAction::getTag(PixA::IConnectivity::EConnTag tag_type) {
    if (m_conn) {
      return m_conn.tag(tag_type);
    }
    return "";
  }

  void DummyMultiAction::removeAction(PixActions *an_action)
  {
    for (std::map<std::string, std::shared_ptr<PixActions> >::iterator  action_iter = m_actionList.begin();
	 action_iter != m_actionList.end();
	 action_iter++) {

      if (action_iter->second.get() == an_action) {
	m_actionList.erase(action_iter);
	return;
      }
    }
    std::cerr << "ERROR [DummyMultiAction::deallocateActions] Tried to deallocate an unknown action. Logic error."  << std::endl;
  }

  bool DummyMultiAction::isAvailable(const std::string &action_name) const {
    std::map<std::string, std::shared_ptr< PixActions > >::const_iterator action_iter = m_actionList.find(action_name);
    if (action_iter != m_actionList.end()) {
      if (action_iter->second->descriptor().allocatingBrokerName.empty()) {
	if (action_iter->second->descriptor().available) {
	  return true;
	}
      }
      else {
	ipc::PixBroker_var a_broker = m_ipcPartition->lookup<ipc::PixBroker,ipc::no_cache,ipc::narrow>(action_iter->second->descriptor().allocatingBrokerName);
	if(CORBA::is_nil(a_broker)) {
	  return true;
	}
      }
    }
    return false;
  }

  bool DummyMultiAction::deallocateActions(const char *action_name, const char *deallocating_broker_name)
  {
    std::map<std::string, std::shared_ptr<PixActions> >::iterator  action_iter = m_actionList.find(action_name);
    if (action_iter != m_actionList.end()) {
      if (action_iter->second->descriptor().allocatingBrokerName == deallocating_broker_name) {
	action_iter->second->deallocate(deallocating_broker_name);
	return true;
      }
      else {
	std::cerr << "ERROR [DummyMultiAction::deallocateActions] " << deallocating_broker_name << " tried to deallocate action " << action_name << "." << std::endl;
      }
    }
    if (m_verbose) {
      std::cerr << "ERROR [DummyMultiAction::deallocateActions] Nothing kown about the action " << action_name << "." << std::endl;
    }
    return false;
  }


  bool DummyMultiAction::deallocateActions(PixActions *an_action, const char *deallocating_broker_name)
  {
    for (std::map<std::string, std::shared_ptr<PixActions> >::iterator  action_iter = m_actionList.begin();
	 action_iter != m_actionList.end();
	 action_iter++) {
      if (action_iter->second.get() == an_action) {
	if (an_action->descriptor().allocatingBrokerName == deallocating_broker_name) {
	  an_action->deallocate(deallocating_broker_name);
	  return true;
	}
	else {
	  std::cerr << "ERROR [DummyMultiAction::deallocateActions] Action allocated by different broker, " << an_action->descriptor().allocatingBrokerName 
		    << ",  than the calling one, " << deallocating_broker_name << "." << std::endl;
	}
	return false;
      }
    }
    std::cerr << "ERROR [DummyMultiAction::deallocateActions] Nothing kown about the given action." << std::endl;
    return false;
  }

  std::set<std::string> DummyMultiAction::listActions(bool checkAvailability,
						      PixActions::Type type,
						      std::string allocatingBrokerName,
						      std::string managingBrokerName,
						      std::string caller) {
    std::set<std::string> actions;
    std::string extended_managing_broker_Name;
    extended_managing_broker_Name += ':';
    extended_managing_broker_Name += managingBrokerName;
    extended_managing_broker_Name += "/";
    if (m_verbose) {
      std::cout << "INFO [DummyMultiAction::listActions] List actions allocating broker = \"" << allocatingBrokerName << "\""
		<< " managing broker = \"" << managingBrokerName << "\""
		<< "." << std::endl;
    }
    for (std::map<std::string, std::shared_ptr<PixActions> >::iterator  action_iter = m_actionList.begin();
	 action_iter != m_actionList.end();
	 action_iter++) {
      const struct PixActionsDescriptor &desc=action_iter->second->descriptor();
      if (    (type == PixActions::ANY_TYPE || type == desc.type)
	   && (!checkAvailability || desc.available)
           && (allocatingBrokerName.empty() || desc.allocatingBrokerName == allocatingBrokerName)) {
	if (!managingBrokerName.empty()) {
	   if (desc.managingBrokerName != managingBrokerName) {
	     if (action_iter->first.find(extended_managing_broker_Name) == std::string::npos) {
	       continue;
	     }
	   }
	}
	actions.insert(action_iter->first);
	if (m_verbose) {
	  std::cout << "INFO [DummyMultiAction::listActions] added " << action_iter->first << " to list."
		    << std::endl;
	}
      }
    }
    return actions;
  }


}
