#include <ICommand.h>
#include <memory>
#include "ActionCommandBase.h"
#include <PixActions/PixActionsMulti.h>


namespace PixCon {

  class ResetRodCommand : public ActionCommandBase
  {
  public:
    ResetRodCommand(const std::shared_ptr<ConsoleEngine> &engine)
      : ActionCommandBase(engine)
    {}

    const std::string &name() const { return commandName(); }

    static const std::string &commandName() {
      return s_name;
    }

  protected:

    void setActionSequence(std::vector<std::string> &action_sequence) {
      action_sequence.push_back("resetRod");
    }

    bool callAction(PixLib::PixActionsMulti &action) {
      action.resetRod();
      return true;
    }

  private:

    static const std::string s_name;
  };

  const std::string ResetRodCommand::s_name("Reset ROD");


  class ResetRodKit : public PixCon::ICommandKit
  {
  protected:
    ResetRodKit()
    {}

  public:
    PixCon::IConfigurableCommand *createCommand(const std::string &/*command*/, const std::shared_ptr<ConsoleEngine> &engine) const {
      assert(engine.get());
      return new ResetRodCommand(engine);
    }

    static bool registerKit() {
      CommandKits::registerKit(ResetRodCommand::commandName(),new ResetRodKit, PixCon::UserMode::kExpert);
      //      CommandKits::registerKit(ResetRodCommand::name(true),new ResetRodKit(true));
      return true;
    }

  private:
    static bool s_registered;
  };

  bool ResetRodKit::s_registered = ResetRodKit::registerKit();

}
