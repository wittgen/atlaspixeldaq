#include "AnalysisBaseCommand.h"

//#include <IScanParameterDb.h>
#include "ScanSelection.h"
//#include "PixMetaException.hh"

//#include <ipc/core.h>

namespace PixCon {

  class AnalysisCommand : public AnalysisBaseCommand
  {
  public:
    AnalysisCommand(const std::shared_ptr<ConsoleEngine> &engine, const std::string &/*analysis_type_name*/)
      : AnalysisBaseCommand(engine)
    {
    }

    PixCon::Result_t execute(IStatusMessageListener&listener) {
      Result_t ret;
      startAnalysis(ret);
      if (ret.analysisSerialNumber()>0) {
	listener.statusMessage(std::string("Running analysis ")+makeSerialNumberString(kAnalysis,
										      ret.analysisSerialNumber()));
      }
      if (ret.isUnfinished()) {
	waitForAnalysis(ret);
      }
      return ret;
    }

    bool configure() {

      ScanSelection selection(name(),"",false,true);

      if ( selection.exec() == QDialog::Accepted ) {

	SerialNumber_t scan_serial_number = selection.scanSerialNumber();
//	if (scan_serial_number==0) {
//	  QMessageBox::warning(NULL,"ERROR [AnalysisCommand::configure]","Illegal serial number.","OK");
//	  return false; 
//	}
	return configureAnalysis(selection, scan_serial_number);
      }
      else {
	return false;
      }
    }

    bool hasDefaults() const { return false ; }
  };

  class AnalysisKit : public PixCon::ICommandKit
  {
  protected:
    AnalysisKit() {}

  public:
    PixCon::IConfigurableCommand *createCommand(const std::string &command, const std::shared_ptr<ConsoleEngine> &engine) const {
      assert(engine.get());
      return new AnalysisCommand(engine, command);
    }

     static bool registerKit() {

       CommandKits::registerKit("Scan Analysis",new AnalysisKit, PixCon::UserMode::kShifter);

//        std::unique_ptr<CAN::IConfigDb> parameter_db( CAN::ScanConfigDbInstance::create() );

//        std::set<std::string> type_list;
//        try {
// 	 type_list = parameter_db->listTypes(CAN::ScanConfigDbInstance::scanClassName() );
//        }
//        catch (...) {
//        }

//        for (std::set<std::string>::const_iterator type_iter = type_list.begin();
// 	    type_iter != type_list.end();
// 	    type_iter++) {
//       CommandKits::registerKit(*type_iter,new AnalysisKit);
       //       }
//       CommandKits::registerKit(AnalysisCommand::name(false),new AnalysisKit(false));
//       CommandKits::registerKit(AnalysisCommand::name(true),new AnalysisKit(true));
       return true;
     }

  private:
    //    std::string m_presetName;
    static bool s_registered;
  };

  bool AnalysisKit::s_registered = AnalysisKit::registerKit();

}
