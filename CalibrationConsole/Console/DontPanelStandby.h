/**
 * @file DontPanelStandby.h
 * @brief Check user sanity for running in expert mode 
 * @author Tayfun Ince <tayfun.ince@cern.ch>
 * @date 2015/04/03
 */

#ifndef _DontPanelStandby_H_
#define _DontPanelStandby_H_
#include <QWidget>
#include "ui_DontPanelStandbyBase.h"
#include <qstring.h>
#include <qvariant.h>
#include <qdialog.h>

class DontPanelStandby : public QDialog, public Ui_DontPanelStandbyBase
{
  Q_OBJECT
 public:
  DontPanelStandby();
  ~DontPanelStandby(); 
};

#endif
