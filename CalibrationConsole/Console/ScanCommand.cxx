#include <AnalysisBaseCommand.h>
#include <PixActions/PixActionsMulti.h>
#include <ConvertGlobalStatus.h>

#include <CoralDbClient.hh>

#include <ConfigWrapper/ConfObjUtil.h>

//#include <IScanParameterDb.h>
#include <ScanConfigDbInstance.hh>
#include "ScanSelection.h"
//#include "PixMetaException.hh"

#include <PixHistoServer/PixHistoServerInterface.h>

#include "DontPanel.h"

#include <qmessagebox.h>

#include <Flag.h>
#include <PixDbGlobalMutex.hh>

namespace PixCon {

  class ScanCommand : public PixCon::AnalysisBaseCommand
  {
  public:
    ScanCommand(const std::shared_ptr<ConsoleEngine> &engine, const std::string &/*scan_type_name*/)
      : AnalysisBaseCommand(engine),
	m_scanInfo(new PixLib::ScanInfo_t)
    {
      PixA::ConnectivityRef conn = m_engine->getConn();
     // m_scanInfo->setScanType(scan_type_name);
      m_scanInfo->setScanType("");
      if (conn) {

	std::string conn_tag = conn.tag(PixA::IConnectivity::kConn);
	std::string id_tag = conn.tag(PixA::IConnectivity::kId);

	m_scanInfo->setIdTag( id_tag );
	m_scanInfo->setConnTag( conn_tag );
	m_scanInfo->setDataTag( conn.tag(PixA::IConnectivity::kPayload) );
	m_scanInfo->setAliasTag( conn.tag(PixA::IConnectivity::kAlias) );
	m_scanInfo->setCfgTag( conn.tag(PixA::IConnectivity::kConfig) );
	m_scanInfo->setCfgRev( conn.revision() );
	m_scanInfo->setModCfgTag( conn.tag(PixA::IConnectivity::kModConfig) );
	m_scanInfo->setModCfgRev( conn.revision() );

	CAN::ScanLocation location = CAN::kNowhere;
	// guess location from connectivity tag names
	if (conn_tag.find("PixelLab") != std::string::npos ) {
	  location = CAN::kPixelLab;
	}
	else if (id_tag=="CT" || id_tag=="SR1" || id_tag=="SR1-20" || conn_tag.find("TOOTHPIX") != std::string::npos) {
	  location = CAN::kSR1;
	}
	else if (conn_tag.find("PIT") != std::string::npos || id_tag.find("PIXEL") != std::string::npos ||
		 conn.tag(PixA::IConnectivity::kConfig)=="PIT") {
	  location = CAN::kPIT;
	}
	m_scanInfo->setLocation(location);

	m_scanInfo->setQuality(CAN::kNormal);
      }
      //      m_scanInfo->setComment(comment);
      //      if (!archive_file_name.empty()) {
      //	m_scanInfo->setFile( archive_file_name );
      //      }

    }

    PixCon::Result_t execute(IStatusMessageListener&listener) {
      PixCon::Result_t ret;
      try {
	startScan(ret);
	if(ret.exited()){
	  std::cerr << "ERROR [ScanCommand::execute] Scan exited during startScan" << std::endl;
	  return ret;
	}
	std::string scan_serial_number_string;
	if (ret.scanSerialNumber()>0) {
	  scan_serial_number_string=makeSerialNumberString(kScan, ret.scanSerialNumber());
	  listener.statusMessage(std::string("Running scan ")+scan_serial_number_string);
	}
	if (hasAnalysis() && ret.scanSerialNumber()>0) {
	  if (changeScanSerialNumber(ret.scanSerialNumber())) {
	    std::cout << "~~~~~~analysis is called.~~~~~~~~~~~" << std::endl;
	    startAnalysis(ret);
	    if (ret.analysisSerialNumber()>0) {
	      std::string message("Running ");
	      if (!scan_serial_number_string.empty()) {
		message+=" scan ";
		message+=scan_serial_number_string;
		message+=" and ";
	      }
	      message+=" analysis ";
	      message+=makeSerialNumberString(kAnalysis, ret.analysisSerialNumber());
	      listener.statusMessage(message);
	    }
	    // since the PixDisable is now understood by the CalibrationConsole and
	    // the calibration analysis this is not needed anymore
	    //            m_engine->abortScansOfDisabledRods(ret.scanSerialNumber());
	  }
	  else {
	    std::cerr << "ERROR [ScanCommand::execute] Invalid scan serial number. Will not start an analysis." << std::endl;
	    ret.setFailure();
	  }
	}
	waitForScan(ret);

	ret = triggerResultWriting(ret);

	if (hasAnalysis()) {
	  waitForAnalysis(ret);
	}

      }
      catch (SctPixelRod::BaseException &err) {
	std::cerr << "ERROR [ScanCommand::execute] Caught base exception :  " << err.getDescriptor() << std::endl;
	ret.setFailure();
      }
      catch (std::exception &err) {
	std::cerr << "ERROR [ScanCommand::execute] Caught std exception :  " << err.what() << std::endl;
	ret.setFailure();
      }
      catch(...) {
	// not even wrote scan meta data
	ret.setFailure();
      }

      if (ret.scanSerialNumber()>0) {
	// to make sure that scan monitoring is turned off.
	m_engine->statusMonitorActivator()->abortScanMonitoring( ret.scanSerialNumber() );
      }

      if (ret.analysisSerialNumber()>0) {
	// to make sure that analysis monitoring is turned off.
	m_engine->statusMonitorActivator()->abortAnalysisMonitoring( ret.scanSerialNumber() );
      }

      return ret;
    }

    PixCon::Result_t &startScan(PixCon::Result_t &ret);
    PixCon::Result_t &waitForScan(PixCon::Result_t &ret);
    PixCon::Result_t &triggerResultWriting(PixCon::Result_t &ret);
    PixCon::Result_t &abortScan(PixCon::Result_t &ret);

    void resetAbort() {
      m_abort=false;
      m_kill=false;
    }

    void abort() {
      m_abort=true;
      //      m_engine->abortCurrentAction();
      m_engine->statusChangeTransceiver()->abortSignal(kScan);
      m_wait.setFlag();

      if (hasAnalysis()) {
	AnalysisBaseCommand::abort();
      }
    }

    void kill() {
      m_kill=true;
      m_abort=true;
      m_engine->statusChangeTransceiver()->abortSignal(kScan);
      m_engine->statusMonitorActivator()->abortScanMonitoring(0);
      m_wait.setFlag();
      if (hasAnalysis()) {
	AnalysisBaseCommand::kill();
      }
    }

    const std::string &name() const { return m_scanInfo->scanType(); }

    bool configure() {
      try {
	std::string default_file_name;
	default_file_name  = m_engine->resultFileTemplate();

	ScanSelection selection(name(),default_file_name, true,false);
	for (;;) {
	  if ( selection.exec() == QDialog::Accepted ) {
	    if (selection.checkFileName() && (!isDangerousScan(selection.selectedType(ScanSelection::kScan)) || warnPanel(name()))) {
	      std::string new_path_name = selection.fileName();
	      m_engine->setResultFileTemplate(new_path_name );

	      m_scanInfo->setScanType(  selection.selectedType(ScanSelection::kScan) );
	      m_scanInfo->setScanTypeTag( selection.selectedTag(ScanSelection::kScan) );
	      m_scanInfo->setScanTypeRev(  selection.selectedRevision(ScanSelection::kScan)  );
	      m_fileName = selection.fileName();
	      configureAnalysis(selection, 0, true); // the scan serial number will be set once it is known
	      std::cout << "INFO [ScanCommand::configure] " << selection.fileName() << std::endl;
	      return true;
	    }
	  }
	  else {
	    return false;
	  }
	}
      }
      catch (SctPixelRod::BaseException &err) {
	std::cerr << "ERROR [ScanCommand::configure] Caught base exception :  " << err.getDescriptor() << std::endl;

      }
      catch (std::exception &err) {
	std::cerr << "ERROR [ScanCommand::configure] Caught standard exception :  " << err.what() << std::endl;
      }
      return false;
    }

    bool hasDefaults() const { return false ; }

    static bool warnPanel(const std::string &command_name) {
	DontPanel notifyOperator;
	notifyOperator.m_nameLabel->setText(command_name.c_str() );
	if ( notifyOperator.exec() == QDialog::Accepted ) {
	  int ret = QMessageBox::question(NULL,
					  QMessageBox::tr("Confirm Scan Start"),
					  QMessageBox::tr("Really run the potentiallly dangerous %1 scan ? - Last chance to cancel !" ).arg(command_name.c_str()),
					  QMessageBox::tr("&CANCEL"), QMessageBox::tr("Queue &Scan Command"),
					  QString::null, 0, 1 );
	  return (ret==1 ? true : false);
	}
	else {
	  return false;
	}
    }

    static bool isDangerousScan(const std::string &command_name) {
      if (s_dangerousScanList.empty()) {
	s_dangerousScanList.insert(std::string("BOC_INLINKSCAN"));
      }
      return (s_dangerousScanList.find(command_name)!=s_dangerousScanList.end());
    }


  protected:
    /** Set the start and maximum value of the mask_stage and the loop0-2 counters.
     * The array is always ordered mask stage, loop0 loop1, loop2.
     */

    bool cloneRequested(PixA::ConfigHandle &scan_config_handle)
    {
      try {
	bool cloneRod, cloneMod;

	PixA::ConfigRef the_scan_config = scan_config_handle.ref();

	PixA::ConfGrpRef general_config( the_scan_config["general"] );

	cloneRod = confVal<bool>(general_config["cloneCfgTag"]);
	cloneMod = confVal<bool>(general_config["cloneModCfgTag"]);
	std::cout << "After clone in cloneReq" << std::endl;
	std::cout << "DEBUG [ScanCommand::cloneRequested] Clone ROD cfg? " << cloneRod << " Clone mod cfg? " << cloneMod << std::endl;
	return cloneRod || cloneMod;
      }
      catch (PixA::ConfigException &err) {
	std::cerr << "ERROR [ScanCommand::cloneRequested] Exception while reading scan config (general_cloneCfgTag and general_cloneModCfgTag) : " << err.getDescriptor() << "." << std::endl;
	return false;
      }
    }

    void setTagSuffix(PixA::ConfigHandle &scan_config_handle, SerialNumber_t scan_serial_number)
    {
      try {
	std::stringstream suff;
	
	PixA::ConfigRef the_scan_config = scan_config_handle.ref();
	
	PixA::ConfGrpRef general_config( the_scan_config["general"] );
	suff << "-S";
	suff << std::setw(9) << std::setfill('0') << scan_serial_number;
	confVal<std::string>(general_config["tagSuffix"])+=suff.str();
	std::cout << "DEBUG [ScanCommand::setTagSuffix] " << confVal<std::string>(general_config["tagSuffix"]) << std::endl;
      }
      catch (PixA::ConfigException &err) {
	std::cerr << "ERROR [ScanCommand::setTagSuffix] Exception while reading loop ranges : " << err.getDescriptor() << "." << std::endl;
      }
    }

    void cloneTag(PixA::ConfigHandle &scan_config_handle) 
    {
      bool cloneRod = false;
      bool cloneMod = false;
      std::string suff = "-TEST";
      try {
	PixA::ConfigRef the_scan_config = scan_config_handle.ref();
	
	PixA::ConfGrpRef general_config( the_scan_config["general"] );
	cloneRod = confVal<bool>(general_config["cloneCfgTag"]);
	cloneMod = confVal<bool>(general_config["cloneModCfgTag"]);
	suff = confVal<std::string>(general_config["tagSuffix"]);
      }
      catch (PixA::ConfigException &err) {
	std::cerr << "ERROR [ScanCommand::setTagSuffix] Exception while reading clone parameters : " << err.getDescriptor() << "." << std::endl;
      }
      if (cloneRod || cloneMod) {
	std::unique_ptr<IPCPartition> partition;
	partition = std::make_unique<IPCPartition>(m_engine->partition().name().c_str());
	if (!partition->isValid()) {
	  std::cerr << "ERROR [ScanCommand::cloneTag] Not a valid partition " << m_engine->partition().name()
		    << " for the db server." << std::endl;
	  return;
	}
	bool ready=false;
	PixLib::PixDbServerInterface dbs(partition.get(), "PixelDbServer", PixLib::PixDbServerInterface::CLIENT, ready);
	if(!ready) {
	  std::cerr << "ERROR [ScanComman::cloneTag] Impossible to connect to DbServer PixelDbServer" << std::endl;
	  return;
	}
	std::string domain, origTag, destTag, pendTag;
	// This will not work if the console is not started using the DbServer, I guess...
	PixA::ConnectivityRef conn = m_engine->getConn();
	domain = "Configuration-"+conn.tag(PixA::IConnectivity::kId);
	pendTag = "_Tmp";
	if (cloneRod) {
	  origTag = conn.tag(PixA::IConnectivity::kConfig);
	}
	if (cloneMod) {
	  origTag = conn.tag(PixA::IConnectivity::kModConfig);
	}
	destTag = origTag + suff;
	dbs.cloneTag(domain, origTag, destTag, pendTag);
      }
    }

    std::string replacePlaceHolder(const std::string &file_name, SerialNumber_t scan_serial_number) 
    {
      std::string::size_type pos = file_name.find("%f");
      if (pos != std::string::npos) {
	std::string temp(file_name);
	temp.erase(pos,2);
	temp.insert(pos,makeSerialNumberName(kScan,scan_serial_number));
	return temp;
      }
      else {
	return file_name;
      }
    }


  private:

    // MetaData_t:  enum EStatus      {kOk, kFailed, kAborted, kUnknown, kRunning, kNStates, kUnset};
    // ScanInfo_t:: enum GlobalStatus {kNotSet,kSuccess, kFailure, kAborted};


    std::unique_ptr<PixLib::ScanInfo_t>        m_scanInfo;
    std::string                              m_fileName;

    static std::string            s_commError;
    static std::set<std::string>  s_dangerousScanList;

    Flag                                     m_wait;

  };

  std::string ScanCommand::s_commError("CORBA communication error. Action server down or restarted ? Partition down ? Network problems ? ");
  std::set<std::string> ScanCommand::s_dangerousScanList;



  PixCon::Result_t &ScanCommand::startScan(PixCon::Result_t &ret)
  {

    PixA::ConnectivityRef conn1 = m_engine->getConn();
    if (!conn1) {
      ret.setExited();
      return ret;
    }

    {
      Lock action_lock(m_engine->multiActionMutex());
      if (!m_engine->haveAllocatedActions()) {
	ret.setExited();
	std::cerr << "ERROR [ScanCommand::startScan] No allocated actions." << std::endl;
	return ret;
      }
    }
    
    if(!m_engine->isSafe(m_scanInfo->scanType())){
      std::string message = "Skipping "+m_scanInfo->scanType()+", looks like HV are off";
      QMessageBox::warning(nullptr, "HV OFF", QString::fromStdString(message));
      std::cout << "ERROR [ScanCommand::startScan] Cannot verify that HV are ON while trying to run a scan type "<< m_scanInfo->scanType() << " skipping..." <<std::endl;
      ret.setExited();
      return ret;
    }

    std::string error_message;
    try {

      // reset the action values in IS
      // @todo should verify before that no action monitor is active. Probably should move the
      // the reset code to the action monitor itself
      m_engine->resetActionIsStatus();

      SerialNumber_t scan_serial_number =0;
      std::string scan_config_dec_name;
      std::vector<unsigned short> start_counter;
      std::vector<unsigned short> max_counter;
      unsigned int mask_stage_loop_index=1;

      // Create meta data for the scan
      {

	// update the tags and revisions
	// a change action tags command may have changed the configuration tags or revision
	{
	  PixA::ConnectivityRef conn2( m_engine->getConn());
	  std::string conn_tag = conn2.tag(PixA::IConnectivity::kConn);
	  std::string id_tag = conn2.tag(PixA::IConnectivity::kId);
         
          if (m_scanInfo->scanType().empty()) {
	    throw std::runtime_error("ERROR [ScanCommand::startScan] Invalid scan type.");
          } 
	  m_scanInfo->setIdTag( id_tag );
	  m_scanInfo->setConnTag( conn_tag );
	  m_scanInfo->setDataTag( conn2.tag(PixA::IConnectivity::kPayload) );
	  m_scanInfo->setAliasTag( conn2.tag(PixA::IConnectivity::kAlias) );
	  m_scanInfo->setCfgTag( conn2.tag(PixA::IConnectivity::kConfig) );
	  m_scanInfo->setCfgRev( conn2.revision() );
	  m_scanInfo->setModCfgTag( conn2.tag(PixA::IConnectivity::kModConfig) );
	  m_scanInfo->setModCfgRev( conn2.revision() );
        }


        CAN::PixCoralDbClient meta_data_client(true);
        PixA::ConfigHandle the_scan_config;
	std::vector <PixA::ConfigHandle> vconfig;
	//PixA::ConfigHandle the_scan_config_fix; // T: some provisional ugly fix
	//@todo remove global lock once the configuration comes from oracle
	{
	Lock globalLock(CAN::PixDbGlobalMutex::mutex());

	std::unique_ptr<CAN::IConfigDb> parameter_db(CAN::ScanConfigDbInstance::create());

	the_scan_config=  parameter_db->config( m_scanInfo->scanType(),
					        m_scanInfo->scanTypeTag(),
					        m_scanInfo->scanTypeRev());
	/*
	the_scan_config_fix=  parameter_db->config( m_scanInfo->scanType(),     // T: some provisional ugly fix
						    "Base_I3",
						    m_scanInfo->scanTypeRev());
	*/

	for(int i=1; i<ScanSelection::NTBaseMax; i++){
	  std::string fe_flavor;
	  	  /* yosuke
	  if(i == ScanSelection::TBase_I3) fe_flavor = "_I3";
	  else */

	  if(i == ScanSelection::TBase_I4) fe_flavor = "_I4";
	  else fe_flavor = "";
	  std::string tag_name_flavor = m_scanInfo->scanTypeTag();//selectedTag(class_i);
	  tag_name_flavor.insert(tag_name_flavor.size(), fe_flavor);
	  std::cout<<"~~~~~~~~~~~~~ first time"<<std::endl;
	  vconfig.push_back( parameter_db->config( m_scanInfo->scanType(),
						   tag_name_flavor,
						   m_scanInfo->scanTypeRev()) );
	}

	setLoopCounters(the_scan_config, start_counter,max_counter, mask_stage_loop_index); // T: some provisional ugly fix
	//setLoopCounters(the_scan_config_fix, start_counter,max_counter, mask_stage_loop_index); // T: some provisional ugly fix
	scan_config_dec_name = parameter_db->configFileName( m_scanInfo->scanType(),
							     m_scanInfo->scanTypeTag(),
							     m_scanInfo->scanTypeRev() );
	scan_config_dec_name += ":/rootRecord;1";

	scan_serial_number = meta_data_client.getNewSerialNumber(CAN::kScanNumber);
	ret.setScanSerialNumber( scan_serial_number );
	m_scanInfo->setFile( replacePlaceHolder(m_fileName, ret.scanSerialNumber() ) );

	std::cout << "DEBUG [ScanCommand::startScan] Check if the tag has to be cloned" <<std::endl;
	if (cloneRequested(the_scan_config)) {     // T: some provisional ugly fix
	  //if (cloneRequested(the_scan_config_fix)) {   // T: some provisional ugly fix
	  std::cout << "DEBUG [ScanCommand::startScan] Add suffix to tag name" << std::endl;
	  setTagSuffix(the_scan_config,scan_serial_number);
	  //setTagSuffix(the_scan_config_fix,scan_serial_number);
	  std::cout << "DEBUG [ScanCommand::startScan] Clone tag" <<std::endl;
	  cloneTag(the_scan_config);
	  //cloneTag(the_scan_config_fix);
	}
	}
	m_scanInfo->setPixDisableString(m_engine->calibrationData().enableString(kCurrent, 0));

	m_scanInfo->setScanStart(time(0));
	m_scanInfo->setScanEnd(m_scanInfo->scanStart());
	meta_data_client.storeMetadata<PixLib::ScanInfo_t>(scan_serial_number,m_scanInfo.get());

        std::shared_ptr<PixLib::PixDisable> pix_disable;
	{
	Lock lock(m_engine->calibrationData().calibrationDataMutex());
	m_engine->calibrationData().addScan(scan_serial_number,
					    m_scanInfo->scanStart(),
					    static_cast<unsigned int>(m_scanInfo->scanEnd() - m_scanInfo->scanStart()),
					    m_scanInfo->scanType(),
					    m_scanInfo->comment(),
					    static_cast<MetaData_t::EQuality>( m_scanInfo->quality() ),
					    the_scan_config, 
					    //the_scan_config_fix, /* change !!*/
					    "",
					    m_engine->getConn());

	m_engine->calibrationData().setScanConfigVector(vconfig);

	try {
	  ScanMetaData_t &scan_meta_data  = m_engine->calibrationData().scanMetaData(scan_serial_number);
	  scan_meta_data.setScanConfigTag(m_scanInfo->scanTypeTag());
	  scan_meta_data.setScanConfigRevision(m_scanInfo->scanTypeRev());
	}
	catch(...) {
	}

	m_engine->calibrationData().calibrationData().copyCurrentEnableStateToScan(scan_serial_number);

// 	pix_disable = m_engine->calibrationData().getPixDisable(kScan,scan_serial_number);

//  	if (pix_disable) {
//  	  std::string pendTag ( PixCon::makeSerialNumberName(kScan, scan_serial_number) ) ;
//  	  try {
// 	    PixA::ConnObjConfigDbTagRef config_db( conn.configDb() );
// 	    config_db.storeConfig(pix_disable->config(), m_scanInfo->scanStart(),  pendTag);
//  	  }
//  	  catch(PixA::ConfigException &) {
// 	    pix_disable.reset();
//  	  }
//  	}

// 	if (!pix_disable.get()) {
//  	  std::cerr << "FATAL [ScanCommand::startScan] Failed to create PixDisable to store the disable state of modules." << std::endl;
//  	  m_engine->calibrationData().updateStatus(kScan,scan_serial_number,MetaData_t::kFailed);
//  	  ret.setFailure();
//  	  return ret;
//  	}
	m_engine->calibrationData().updateStatus(kScan,scan_serial_number,MetaData_t::kRunning);

	}


	m_engine->statusChangeTransceiver()->signal(kScan,scan_serial_number);



	//@todo notify the gui about the new scan
      }


      // Now start the scan monitoring and the scan
      CAN::GlobalStatus scan_status=CAN::kNotSet;
      bool comm_error=false;
      try {
	std::string scan_serial_number_string(makeSerialNumberName(kScan, scan_serial_number));
	m_engine->resetIsLoopCounter();
	std::cout << "INFO [ScanCommand::startScan] Activate progress monitoring "<< scan_serial_number_string << std::endl;
	if (!m_engine->statusMonitorActivator()->activateScanMonitoring( scan_serial_number,  mask_stage_loop_index, start_counter, max_counter, 60)) {
	  ret.setFailure();
	  return ret;
	}
	else {
	  // the engine will throw an exception if there is no multi action.
	  // Atlernatively, the engine could return a pointer ?
	  {
	    std::cout << "INFO [ScanCommand::startScan] set scan : " << scan_serial_number_string << std::endl;
	    Lock action_lock(m_engine->multiActionMutex());
	    PixLib::PixActionsMulti &action = m_engine->multiAction();
	    action.setPixScan(scan_config_dec_name);
	    std::string file_name = m_scanInfo->scanType();
	    file_name += ':';
	    file_name +=m_scanInfo->file();
	    std::cout << "INFO [ScanCommand::startScan] start scan :" << scan_serial_number_string<<std::endl;
	    action.scan( file_name , scan_serial_number);
	    std::cout << "INFO [ScanCommand::startScan] scan is started:" << scan_serial_number_string << std::endl;
	  }
	}
      }
      catch (CORBA::TRANSIENT& err) {
	comm_error=true;
      }
      catch (CORBA::COMM_FAILURE &err) {
	comm_error=true;
      }
      catch (std::exception &err) {
	error_message="Caught standard exception : ";
	error_message += err.what();
      }
      catch (...) {
	error_message="Unknown exception while aborting scan.";
      }

      if (comm_error) {
	error_message=s_commError;
      }

      if (!error_message.empty()) {
	std::cerr << "ERROR [ScanCommand::startScan] " << error_message << std::endl;

	// @todo produce helpful error messages

	ret.setFailure();
	scan_status=CAN::kFailure;
	{
	Lock lock(m_engine->calibrationData().calibrationDataMutex());
	if (m_engine->calibrationData().status(kScan, scan_serial_number) != MetaData_t::kOk) {
	  m_engine->calibrationData().updateStatus(kScan,scan_serial_number,ConvertGlobalStatus::convertStatus(scan_status) );
	}
	}
	m_engine->statusMonitorActivator()->abortScanMonitoring( scan_serial_number );
      }
      return ret;

    }
    catch (SctPixelRod::BaseException &err) {
      error_message = "Caught base exception :  ";
      error_message +=  err.getDescriptor();
    }
    catch (std::exception &err) {
      error_message = "Caught std exception :  ";
      error_message +=  err.what();
    }
    catch(...) {
      error_message = "Caught unhandled exception :  ";
      // not even wrote scan meta data
    }

    if (!error_message.empty()) {
      //@todo publish message somewhere. MRS ? GUI ? 
      std::cerr << "ERROR [ScanCommand::startScan] " << error_message << std::endl;
      ret.setFailure();
    }
    return ret;
  }

  PixCon::Result_t &ScanCommand::waitForScan(PixCon::Result_t &ret)
  {
    if (ret.scanSerialNumber()>0) {

      //      try {
      m_engine->markRodsWithoutEnabledModulesAsDone(ret.scanSerialNumber());
      m_engine->statusChangeTransceiver()->waitFor(kScan, ret.scanSerialNumber(),  m_abort);
      CAN::GlobalStatus scan_status = CAN::kNotSet;

      // only need the lock to query the status
      if (m_abort && m_engine->calibrationData().getStatusSafe(kScan, ret.scanSerialNumber() ) == MetaData_t::kRunning) {
	abortScan(ret);
      }
      else {
	ret.setSuccess();
	// the scan monitor should set the status in the meta data.
	scan_status=ConvertGlobalStatus::convertToScanInfoStatus( static_cast<MetaData_t::EStatus>(m_engine->calibrationData().status(kScan, ret.scanSerialNumber() )) ) ;
      }
      //       catch(...) {
      // 	// Exceptions are only expected in abortScan, but abortScan cataches all exceptions
      // 	// so this catch is presumably useless

      // 	ret.setFailure();
      // 	scan_status=CAN::kFailure;
      // 	if (m_engine->calibrationData().status(kScan, ret.scanSerialNumber()) != MetaData_t::kOk) {
      // 	  m_engine->calibrationData().updateStatus(kScan,ret.scanSerialNumber(),convertStatus(scan_status) );
      // 	}
      //       }_sc

      if (ret.scanSerialNumber()>0) {
	CAN::PixCoralDbClient meta_data_client(true);
	m_scanInfo->setScanEnd(time(0));
	meta_data_client.updateScanMetadataStatusAndEndTime(ret.scanSerialNumber(), scan_status, m_scanInfo->scanEnd() );
	{
	  Lock lock(m_engine->calibrationData().calibrationDataMutex());
	  ScanMetaData_t &scan_meta_data = m_engine->calibrationData().scanMetaData(ret.scanSerialNumber());
	  scan_meta_data.setDuration(m_scanInfo->scanEnd() - m_scanInfo->scanStart());
	}

	//@todo notify the gui that the meta data has changed
	m_engine->statusChangeTransceiver()->signal(kScan,ret.scanSerialNumber());
      }
    }
    return ret;
  }

  PixCon::Result_t &ScanCommand::abortScan(PixCon::Result_t &ret)
  {
    if (ret.scanSerialNumber()==0) return ret;

    bool comm_error=false;
    std::string error_message;
    try {
      Lock action_lock(m_engine->multiActionMutex());
      PixLib::PixActionsMulti &action = m_engine->multiAction();
      action.abortScan();
      //@todo will this work ? it should...
      m_engine->statusChangeTransceiver()->waitFor(kScan, ret.scanSerialNumber(),  m_kill);
    }
    catch (CORBA::TRANSIENT& err) {
      comm_error=true;
    }
    catch (CORBA::COMM_FAILURE &err) {
      comm_error=true;
    }
    catch (std::exception &err) {
      error_message="Caught standard exception : ";
      error_message += err.what();
    }
    catch (...) {
      error_message="Unknown exception while aborting scan.";
    }

    if (comm_error) {
      error_message=s_commError;
    }

    if (!error_message.empty()) {
      std::cerr << "ERROR [ScanCommand::abort] " << error_message << std::endl;
    }

    m_engine->statusMonitorActivator()->abortScanMonitoring( ret.scanSerialNumber()  );
    if (!m_kill) {
      ret.setAborted();
    }
    else {
      ret.setKilled();
    }

    Lock lock(m_engine->calibrationData().calibrationDataMutex());
    if (m_engine->calibrationData().status(kScan, ret.scanSerialNumber()) != MetaData_t::kOk) {
      m_engine->calibrationData().updateStatus(kScan,ret.scanSerialNumber(),ConvertGlobalStatus::convertStatus( CAN::kAborted ) );
    }
    return ret;
  }

  PixCon::Result_t &ScanCommand::triggerResultWriting(PixCon::Result_t &ret)
  {
    // retry 5-times in case of errors
    unsigned int wait_time=5;
    for (unsigned int pass_i=0; pass_i<1; pass_i++) {
    try {
      std::string provName = "CalibrationConsole";
      std::unique_ptr<PixLib::PixHistoServerInterface>  hInt( new PixLib::PixHistoServerInterface(&(m_engine->partition()),
												m_engine->ohServerName(),
												m_engine->nameServerName(),
												PROVIDERBASE+provName));
      std::stringstream folderstream;
      folderstream << "/";
      folderstream <<makeSerialNumberName(kScan, ret.scanSerialNumber());
      folderstream << "/";
      std::cout << "folderstream is : " << folderstream.str() << std::endl;
      std::string outFileName;
      std::cout << "outFileName is : " << m_scanInfo->file() << std::endl;
      outFileName = m_scanInfo->file();
      bool result = hInt->writeRootHistoServer(folderstream.str(), outFileName, false);
      if (result) 	{
	std::cout << " result is true !!" << std::endl;
	// update file name in meta data and signal (hopefully the meta histogram provider to pick up the
	// file  name as a fall-back solution.
	{
	  Lock lock(m_engine->calibrationData().calibrationDataMutex());
	  ScanMetaData_t &scan_meta_data = m_engine->calibrationData().scanMetaData(ret.scanSerialNumber());
	  scan_meta_data.setFileName(outFileName);
	}
	std::cout << " after result ok ... !!" << std::endl;
	m_engine->statusChangeTransceiver()->signal(kScan,ret.scanSerialNumber());
	std::cout << " after statusChangeTransceiver()" << std::endl;
	std::cout << "INFO [ScanCommand::triggerResultWriting] Saved file to: " << outFileName << std::endl;
	return ret;
      }

    }
    catch (SctPixelRod::BaseException &err) {
      std::cerr << "ERROR [ScanCommand::triggerResultWriting] Failed to trigger file writing. Caught exception : " << err.getDescriptor() << std::endl;
    }
    catch (std::exception &err) {
      std::cerr << "ERROR [ScanCommand::triggerResultWriting] Failed to trigger file writing. Caught exception : " << err.what() << std::endl;
    }
    catch (...) {
      std::cerr << "ERROR [ScanCommand::triggerResultWriting] Failed to trigger file writing. Caught unknown exception.(IPC timeout error, check if the file is there)" << std::endl;
    }

    std::cerr << "WARNING [ScanCommand::triggerResultWriting] Failed to trigger file writing.  Will retry in " << wait_time << " sec. ." << std::endl;
    m_wait.timedwait(wait_time,m_abort);
    if (m_abort || m_kill) break;
    wait_time=10;

    }
    if(ret.status()!=Result_t::kAborted&&ret.status()!=Result_t::kKilled)ret.setFailure();
    return ret;

  }

  class ScanKit : public PixCon::ICommandKit
  {
  protected:
    ScanKit() {}

  public:
    PixCon::IConfigurableCommand *createCommand(const std::string &/*command*/, const std::shared_ptr<ConsoleEngine> &engine) const {
      assert(engine.get());
      return new ScanCommand(engine,""/*, command*/);
    }
    static bool registerKit() {

//      try { 
//      std::unique_ptr<CAN::IConfigDb> parameter_db( CAN::ScanConfigDbInstance::create() );
//
//      std::set<std::string> type_list;
//      try {
//	type_list = parameter_db->listTypes(CAN::ScanConfigDbInstance::scanClassName(), CAN::IConfigDb::kNormal);
//     }
//      catch (std::exception &err) {
//	std::cerr << "ERROR [ScanCommand::registerKit] caught std::exception : " << err.what() << std::endl;
//      }
//      catch (...) {
//      }

//      for (std::set<std::string>::const_iterator type_iter = type_list.begin();
//	   type_iter != type_list.end();
//	   type_iter++) {

//        if (type_iter->empty()) continue;
//       std::string::size_type pos = 0;
//        while (pos < type_iter->size() && isdigit( (*type_iter)[pos])) pos++;
//        if (pos>=type_iter->size()) continue;
//	CommandKits::registerKit(*type_iter,new ScanKit);
	CommandKits::registerKit("Run scan",new ScanKit, PixCon::UserMode::kShifter);
//      }
      return true;
//      }
//      catch(CAN::MissingConfig &err) {
//	std::cerr << "ERROR [ScanKit::registerKit] Caught exception : " << err.getDescriptor() << std::endl;
//      }
//      return false;
    }

  private:
    static bool s_registered;
  };


  bool ScanKit::s_registered = ScanKit::registerKit();

  ICommand *scanCommand(const std::shared_ptr<ConsoleEngine> &engine, const std::string &scan_type_name) {
    return new ScanCommand(engine, scan_type_name);
  }  
}
