// Dear emacs, this is -*-c++-*-
#ifndef __CHANGE_TAGS_COMMAND_H__
#define __CHANGE_TAGS_COMMAND_H__
#include <ICommand.h>
#include <memory>
#include <CommandKits.h>

namespace PixLib {
  class PixISManager;
  class PixDisable;
}

namespace PixCon {

  class ConsoleEngine;

  class ChangeTagsCommand : public IConfigurableCommand
  {
  public:
    ChangeTagsCommand(const std::shared_ptr<ConsoleEngine> &engine, const std::shared_ptr<PixLib::PixDisable> &restore_disable_to_be_filled)
      : m_engine(engine),
	m_restoreDisable(restore_disable_to_be_filled)
    {}

    ~ChangeTagsCommand();

    const std::string &name() const { return commandName(); }

    static const std::string &commandName() {
      return s_name;
    }

    PixCon::Result_t execute(IStatusMessageListener &listener);

    /** Nothing to do */
    void resetAbort() { }

    /** cannot do anything */
    void abort() { }

    /** cannot do anything */
    void kill() { }

    bool configure();

    bool hasDefaults() const { return false; }

  private :

    std::unique_ptr<PixLib::PixISManager> m_isManager;
    std::shared_ptr<ConsoleEngine> m_engine;
    std::shared_ptr<PixLib::PixDisable> m_restoreDisable;

    std::string m_newIdTag;
    std::string m_newConnTag;
    std::string m_newCfgTag;
    std::string m_newModCfgTag;

    static const std::string s_name;
  };


  class ChangeTagsKit : public PixCon::ICommandKit
  {
  protected:
    ChangeTagsKit()
    {}

  public:
    PixCon::IConfigurableCommand *createCommand(const std::string &/*command*/, const std::shared_ptr<ConsoleEngine> &engine) const;

    static bool registerKit() {
      CommandKits::registerKit(ChangeTagsCommand::commandName(),new ChangeTagsKit, PixCon::UserMode::kShifter);
      //      CommandKits::registerKit(ChangeTagsCommand::name(true),new ChangeTagsKit(true));
      return true;
    }

    static PixCon::IConfigurableCommand *command(const std::shared_ptr<ConsoleEngine> &engine) {
      ChangeTagsKit temp;
      return temp.createCommand("Change tags", engine);
    }

  private:
    static bool s_registered;
  };



}
#endif
