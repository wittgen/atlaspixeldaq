#include "ActionStatus.h"

namespace PixCon {

  std::string ActionStatus::s_statusNameToState[kNStates];

  bool ActionStatus::initialseStatusNameTranslationMap()
  {
    s_statusNameToState[kRunning]="RUNNING";
    s_statusNameToState[kOk]="DONE";
    s_statusNameToState[kFailure]="FAILED";
    s_statusNameToState[kAborted]="ABORTED";
    s_statusNameToState[kUnknown]="UNKNOWN";
    return true;
  }

  bool ActionStatus::s_initialised = ActionStatus::initialseStatusNameTranslationMap();


}
