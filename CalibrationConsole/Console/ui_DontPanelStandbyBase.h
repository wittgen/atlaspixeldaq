#ifndef UI_DONTPANELSTANDBYBASE_H
#define UI_DONTPANELSTANDBYBASE_H

#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QDialog>
#include <QFrame>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QPushButton>
#include <QSpacerItem>
#include <QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_DontPanelStandbyBase
{
public:
    QVBoxLayout *vboxLayout;
    QLabel *m_startLabel;
    QHBoxLayout *hboxLayout;
    QSpacerItem *spacer3;
    QLabel *m_nameLabel;
    QSpacerItem *spacer4;
    QLabel *m_explainationLabel;
    QLabel *m_dontLabel;
    QLabel *m_whatLabel;
    QLabel *m_adviceLabel;
    QFrame *line1;
    QHBoxLayout *hboxLayout1;
    QPushButton *m_cancelButton;
    QLabel *textLabel1_2;
    QVBoxLayout *vboxLayout1;
    QSpacerItem *spacer2;
    QHBoxLayout *hboxLayout2;
    QSpacerItem *spacer1;
    QPushButton *m_doAnyWayButton;

    void setupUi(QDialog *DontPanelStandbyBase)
    {
        if (DontPanelStandbyBase->objectName().isEmpty())
            DontPanelStandbyBase->setObjectName(QString::fromUtf8("DontPanelStandbyBase"));
        DontPanelStandbyBase->resize(476, 410);
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(DontPanelStandbyBase->sizePolicy().hasHeightForWidth());
        DontPanelStandbyBase->setSizePolicy(sizePolicy);
        vboxLayout = new QVBoxLayout(DontPanelStandbyBase);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        m_startLabel = new QLabel(DontPanelStandbyBase);
        m_startLabel->setObjectName(QString::fromUtf8("m_startLabel"));
        m_startLabel->setWordWrap(false);

        vboxLayout->addWidget(m_startLabel);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        spacer3 = new QSpacerItem(41, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacer3);

        m_nameLabel = new QLabel(DontPanelStandbyBase);
        m_nameLabel->setObjectName(QString::fromUtf8("m_nameLabel"));
        QFont font;
        font.setPointSize(14);
        font.setBold(true);
        font.setWeight(75);
        m_nameLabel->setFont(font);
        m_nameLabel->setAlignment(Qt::AlignVCenter);
        m_nameLabel->setWordWrap(false);

        hboxLayout->addWidget(m_nameLabel);

        spacer4 = new QSpacerItem(51, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacer4);


        vboxLayout->addLayout(hboxLayout);

        m_explainationLabel = new QLabel(DontPanelStandbyBase);
        m_explainationLabel->setObjectName(QString::fromUtf8("m_explainationLabel"));
        m_explainationLabel->setWordWrap(false);

        vboxLayout->addWidget(m_explainationLabel);

        m_dontLabel = new QLabel(DontPanelStandbyBase);
        m_dontLabel->setObjectName(QString::fromUtf8("m_dontLabel"));
        QFont font1;
        font1.setPointSize(70);
        font1.setBold(true);
        font1.setWeight(75);
        m_dontLabel->setFont(font1);
        m_dontLabel->setWordWrap(false);

        vboxLayout->addWidget(m_dontLabel);

        m_whatLabel = new QLabel(DontPanelStandbyBase);
        m_whatLabel->setObjectName(QString::fromUtf8("m_whatLabel"));
        m_whatLabel->setWordWrap(false);

        vboxLayout->addWidget(m_whatLabel);

        m_adviceLabel = new QLabel(DontPanelStandbyBase);
        m_adviceLabel->setObjectName(QString::fromUtf8("m_adviceLabel"));
        m_adviceLabel->setWordWrap(false);

        vboxLayout->addWidget(m_adviceLabel);

        line1 = new QFrame(DontPanelStandbyBase);
        line1->setObjectName(QString::fromUtf8("line1"));
        line1->setFrameShape(QFrame::HLine);
        line1->setFrameShadow(QFrame::Sunken);

        vboxLayout->addWidget(line1);

        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setSpacing(6);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        m_cancelButton = new QPushButton(DontPanelStandbyBase);
        m_cancelButton->setObjectName(QString::fromUtf8("m_cancelButton"));
        QSizePolicy sizePolicy1(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(m_cancelButton->sizePolicy().hasHeightForWidth());
        m_cancelButton->setSizePolicy(sizePolicy1);
        QPalette palette;
        QBrush brush(QColor(0, 0, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(255, 0, 0, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush1);
        QBrush brush2(QColor(255, 127, 127, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Light, brush2);
        QBrush brush3(QColor(255, 63, 63, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Midlight, brush3);
        QBrush brush4(QColor(127, 0, 0, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Dark, brush4);
        QBrush brush5(QColor(170, 0, 0, 255));
        brush5.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Mid, brush5);
        QBrush brush6(QColor(211, 211, 211, 255));
        brush6.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Text, brush6);
        QBrush brush7(QColor(255, 255, 255, 255));
        brush7.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::BrightText, brush7);
        QBrush brush8(QColor(214, 214, 214, 255));
        brush8.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush8);
        palette.setBrush(QPalette::Active, QPalette::Base, brush7);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette.setBrush(QPalette::Active, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Active, QPalette::AlternateBase, brush2);
        QBrush brush9(QColor(255, 255, 220, 255));
        brush9.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ToolTipBase, brush9);
        palette.setBrush(QPalette::Active, QPalette::ToolTipText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Light, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Inactive, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Inactive, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::BrightText, brush7);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush8);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush7);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush9);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Light, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::BrightText, brush7);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush9);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush);
        m_cancelButton->setPalette(palette);
        QFont font2;
        font2.setPointSize(28);
        font2.setBold(true);
        font2.setWeight(75);
        m_cancelButton->setFont(font2);
        m_cancelButton->setDefault(true);

        hboxLayout1->addWidget(m_cancelButton);

        textLabel1_2 = new QLabel(DontPanelStandbyBase);
        textLabel1_2->setObjectName(QString::fromUtf8("textLabel1_2"));
        textLabel1_2->setMinimumSize(QSize(76, 0));
        QFont font3;
        font3.setPointSize(50);
        font3.setBold(true);
        font3.setWeight(75);
        textLabel1_2->setFont(font3);
        textLabel1_2->setWordWrap(false);

        hboxLayout1->addWidget(textLabel1_2);

        vboxLayout1 = new QVBoxLayout();
        vboxLayout1->setSpacing(6);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        spacer2 = new QSpacerItem(20, 48, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout1->addItem(spacer2);

        hboxLayout2 = new QHBoxLayout();
        hboxLayout2->setSpacing(6);
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        spacer1 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout2->addItem(spacer1);

        m_doAnyWayButton = new QPushButton(DontPanelStandbyBase);
        m_doAnyWayButton->setObjectName(QString::fromUtf8("m_doAnyWayButton"));
        QPalette palette1;
        palette1.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush10(QColor(0, 255, 0, 255));
        brush10.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Button, brush10);
        QBrush brush11(QColor(127, 255, 127, 255));
        brush11.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Light, brush11);
        QBrush brush12(QColor(63, 255, 63, 255));
        brush12.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Midlight, brush12);
        QBrush brush13(QColor(0, 127, 0, 255));
        brush13.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Dark, brush13);
        QBrush brush14(QColor(0, 170, 0, 255));
        brush14.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Mid, brush14);
        palette1.setBrush(QPalette::Active, QPalette::Text, brush);
        palette1.setBrush(QPalette::Active, QPalette::BrightText, brush7);
        palette1.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette1.setBrush(QPalette::Active, QPalette::Base, brush7);
        palette1.setBrush(QPalette::Active, QPalette::Window, brush10);
        palette1.setBrush(QPalette::Active, QPalette::Shadow, brush);
        palette1.setBrush(QPalette::Active, QPalette::AlternateBase, brush11);
        palette1.setBrush(QPalette::Active, QPalette::ToolTipBase, brush9);
        palette1.setBrush(QPalette::Active, QPalette::ToolTipText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Button, brush10);
        palette1.setBrush(QPalette::Inactive, QPalette::Light, brush11);
        palette1.setBrush(QPalette::Inactive, QPalette::Midlight, brush12);
        palette1.setBrush(QPalette::Inactive, QPalette::Dark, brush13);
        palette1.setBrush(QPalette::Inactive, QPalette::Mid, brush14);
        palette1.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::BrightText, brush7);
        palette1.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush7);
        palette1.setBrush(QPalette::Inactive, QPalette::Window, brush10);
        palette1.setBrush(QPalette::Inactive, QPalette::Shadow, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush11);
        palette1.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush9);
        palette1.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::WindowText, brush13);
        palette1.setBrush(QPalette::Disabled, QPalette::Button, brush10);
        palette1.setBrush(QPalette::Disabled, QPalette::Light, brush11);
        palette1.setBrush(QPalette::Disabled, QPalette::Midlight, brush12);
        palette1.setBrush(QPalette::Disabled, QPalette::Dark, brush13);
        palette1.setBrush(QPalette::Disabled, QPalette::Mid, brush14);
        palette1.setBrush(QPalette::Disabled, QPalette::Text, brush13);
        palette1.setBrush(QPalette::Disabled, QPalette::BrightText, brush7);
        palette1.setBrush(QPalette::Disabled, QPalette::ButtonText, brush13);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush10);
        palette1.setBrush(QPalette::Disabled, QPalette::Window, brush10);
        palette1.setBrush(QPalette::Disabled, QPalette::Shadow, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush10);
        palette1.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush9);
        palette1.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush);
        m_doAnyWayButton->setPalette(palette1);
        m_doAnyWayButton->setStyleSheet(QString::fromUtf8("setbackground-color:rgb(0, 255, 0)"));
        m_doAnyWayButton->setAutoDefault(false);

        hboxLayout2->addWidget(m_doAnyWayButton);


        vboxLayout1->addLayout(hboxLayout2);


        hboxLayout1->addLayout(vboxLayout1);


        vboxLayout->addLayout(hboxLayout1);


        retranslateUi(DontPanelStandbyBase);
        QObject::connect(m_cancelButton, SIGNAL(clicked()), DontPanelStandbyBase, SLOT(reject()));
        QObject::connect(m_doAnyWayButton, SIGNAL(clicked()), DontPanelStandbyBase, SLOT(accept()));

        QMetaObject::connectSlotsByName(DontPanelStandbyBase);
    } // setupUi

    void retranslateUi(QDialog *DontPanelStandbyBase)
    {
        DontPanelStandbyBase->setWindowTitle(QApplication::translate("DontPanelStandbyBase", "Form1", 0));
        m_startLabel->setText(QApplication::translate("DontPanelStandbyBase", "<p align=\"center\">you are about to switch on the</p>", 0));
        m_nameLabel->setText(QApplication::translate("DontPanelStandbyBase", "EXPERT MODE", 0));
        m_explainationLabel->setText(QApplication::translate("DontPanelStandbyBase", "<p align=\"center\">this operation is potentially dangerous</p>", 0));
        m_dontLabel->setText(QApplication::translate("DontPanelStandbyBase", "<p align=\"center\">DON'T</p>", 0));
        m_whatLabel->setText(QApplication::translate("DontPanelStandbyBase", "<html><head/><body><p>switch on this mode <span style=\" font-weight:600; color:#ff0000;\">if beam is circulating the machine</span></p></body></html>", 0));
        m_adviceLabel->setText(QApplication::translate("DontPanelStandbyBase", "<font size=\"+2\"><b>only with confirmation from the pixel run coordinators</b></font><br>", 0));
        m_cancelButton->setText(QApplication::translate("DontPanelStandbyBase", "CANCEL", 0));
        textLabel1_2->setText(QString());
        m_doAnyWayButton->setText(QApplication::translate("DontPanelStandbyBase", "Continue", 0));
    } // retranslateUi

};

namespace Ui {
    class DontPanelStandbyBase: public Ui_DontPanelStandbyBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DONTPANELSTANDBYBASE_H
