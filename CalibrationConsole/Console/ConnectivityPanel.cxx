#include "ConnectivityPanel.h"

#include "ConsoleEngine.h"
#include "MainPanel.h"
#include <DisablePanel.h>

#include <QMessageBox>

#include <TemporaryCursorChange.h>
#include <BusyLoop.h>

using namespace std;

namespace PixLib {
  class PartitionConnectivity;
}

using namespace PixLib;

namespace PixCon {
  ConnectivityPanel::ConnectivityPanel(const PixCon::UserMode::EUserMode& current_user,
				       PixCon::ConsoleEngine &engine,
				       QApplication *app,
				       QWidget* parent)
: QDialog(parent), m_engine(engine), m_app(app)
  {
setupUi(this);
    if (current_user != PixCon::UserMode::kExpert) actGrpBox->setDisabled(true);
    m_conn = engine.getConn();
    insertPartitions();

  }

  void ConnectivityPanel::insertPartitions() 
  { 
    disconnect(partList->selectionModel(),SIGNAL(selectionChanged(QItemSelection,QItemSelection)),this,SLOT(insertCrates()));
    crateList->setEnabled(false);
    rodList->setEnabled(false);
    allocateButton->setEnabled(false);
    partList->clear();
    partList->setSelectionMode(QAbstractItemView::MultiSelection);
    partLabel->setText("Select Partition(s):");
    partitionSelectAll->setText("Disable all");
    m_parts.clear();
    m_localPartCrate.clear();
    if (m_conn) {
      PixA::CrateList clist = m_conn.crates();
      for (PixA::CrateList::const_iterator cit = clist.begin(); cit != clist.end(); ++cit) {
    std::map<std::string, std::vector<std::string> >::iterator it = m_localPartCrate.find(PixA::partitionName(cit));
	if (it == m_localPartCrate.end()) {
	  std::vector<std::string> cl;
	  cl.push_back(PixA::connectivityName(cit));
	  m_localPartCrate.insert(std::make_pair(PixA::partitionName(cit),cl));
	}
	else {
	  (*it).second.push_back(PixA::connectivityName(cit));
	}
      }
    }
    QItemSelection sel;
    std::map<std::string, std::vector<std::string> >::iterator it;
    for (it = m_localPartCrate.begin(); it != m_localPartCrate.end(); it++) {
      QTreeWidgetItem *an_item = new QTreeWidgetItem;
      an_item->setText(0,QString((*it).first.c_str()));
      an_item->setText(1,QString::number(partList->topLevelItemCount()));
      partList->addTopLevelItem(an_item);
      m_parts.insert(std::make_pair(partList->topLevelItemCount()-1, (*it).first));
      if (m_disabledList.enabled((*it).first)) {
	//an_item->setSelected(true);
	  sel.select(partList->model()->index(partList->topLevelItemCount()-1, 0, QModelIndex()),
		     partList->model()->index(partList->topLevelItemCount()-1, 1, QModelIndex()));
      }
    }
    partList->selectionModel()->select(sel, QItemSelectionModel::Select);
    if(partList->topLevelItemCount()>0){
      insertCrates();
    }
   partList->setEnabled(true);
   connect(partList->selectionModel(),SIGNAL(selectionChanged(QItemSelection,QItemSelection)),this,SLOT(insertCrates()));
  }

  ConnectivityPanel::~ConnectivityPanel() {}

  void ConnectivityPanel::insertCrates()
  {
    disconnect(crateList->selectionModel(),SIGNAL(selectionChanged(QItemSelection,QItemSelection)),this,SLOT(insertRods()));
    TemporaryCursorChange busy_cursor;
    crateList->setEnabled(false);
    rodList->setEnabled(false);
    allocateButton->setEnabled(false);
    crateList->clear();
    m_crates.clear();
    crateLabel->setText("Select Crate(s):");
    crateSelectAll->setText("Disable all");
    crateList->setSelectionMode(QAbstractItemView::MultiSelection);
   // crateList->setSorting(2,true);
    QTreeWidgetItemIterator item(partList);
    QItemSelection sel;
    while((*item)){
      if((*item)->isSelected()){
    bool isOK;
    int partID = (*item)->text(1).toInt(&isOK);
    if(isOK && m_parts.find(partID)!=m_parts.end()){
      std::vector<std::string> cr = m_localPartCrate[m_parts[partID]];
      std::vector<std::string>::iterator it;
      for (it = cr.begin(); it != cr.end(); it++){
        QTreeWidgetItem *nit = new QTreeWidgetItem;
        nit->setText(0,QString((*it).c_str()));
        nit->setText(1,QString(m_parts[partID].c_str()));
        nit->setText(2, QString::number(crateList->topLevelItemCount()));
        crateList->addTopLevelItem(nit);
        m_crates.insert(std::make_pair(crateList->topLevelItemCount()-1, *it));
        if (m_conn) {
          PixA::CrateList clist = m_conn.crates();
          PixA::CrateList::const_iterator cit = clist.begin();
          for (cit = clist.begin(); cit != clist.end(); ++cit) {
        if (PixA::connectivityName(cit) == *it) {
          break;
        }
          }
          if (cit != clist.end() && !const_cast<PixLib::RodCrateConnectivity *>(*cit)->enableReadout){
	    //nit->setDisabled(true);
	    sel.select(crateList->model()->index(crateList->topLevelItemCount()-1, 0, QModelIndex()),
		       crateList->model()->index(crateList->topLevelItemCount()-1, 2, QModelIndex()));
          }
        }
        if (m_disabledList.enabled(*it)) {
          nit->setSelected(true);
        }
      }
    }
      }
      ++item;
    }
    crateList->selectionModel()->select(sel, QItemSelectionModel::Select);
    if(crateList->topLevelItem(0)==0){
      rodList->clear();
      m_rods.clear();
      return;
    }
    insertRods();
    crateList->setEnabled(true);
    connect(crateList->selectionModel(),SIGNAL(selectionChanged(QItemSelection,QItemSelection)),this,SLOT(insertRods()));
  }


  void ConnectivityPanel::insertRods()
  {
    rodList->setEnabled(false);
    allocateButton->setEnabled(false);
    rodList->clear();
    m_rods.clear();
    rodLabel->setText("Select ROD(s):");
    rodSelectAll->setText("Disable all");
    rodList->setSelectionMode(QAbstractItemView::MultiSelection);
    QTreeWidgetItemIterator item(crateList);
    QItemSelection sel;
    while((*item)){
      if((*item)->isSelected()){
    bool isOK;
    int crateID = (*item)->text(2).toInt(&isOK);
    if (isOK && m_crates.find(crateID)!=m_crates.end()){
      if (m_conn) {
        PixA::RodList rlist = m_conn.rods(m_crates[crateID]);
        for (PixA::RodList::const_iterator it = rlist.begin(); it != rlist.end(); ++it) {
          if (actGrpBox->isChecked()) {

        std::string s_tmp;
        std::set<std::string> actionsList;
        {
          s_tmp= m_engine.actionName(PixA::connectivityName(it), m_crates[crateID]);
          actionsList = m_engine.getActionsList(m_crates[crateID]);
        }
        std::set<std::string>::iterator actions;
        if (actionsList.find(s_tmp) == actionsList.end()){
          continue;
        }
          }
          std::string rodName = const_cast<PixLib::RodBocConnectivity *>(*it)->name();
          if( getenv("CONSOLE_ROD_LIST") && strlen(getenv("CONSOLE_ROD_LIST")) && std::string(getenv("CONSOLE_ROD_LIST")).find(rodName + ":") == std::string::npos ) {
            std::cout << "KP: Skipping " << const_cast<PixLib::RodBocConnectivity *>(*it)->name() << std::endl;
            continue;
          }

          QTreeWidgetItem *nit = new QTreeWidgetItem;
          nit->setText(0,QString(PixA::connectivityName(it).c_str()));
          nit->setText(1,m_crates[crateID].c_str());
          nit->setText(2,(*item)->text(1).toLatin1().data());
          nit->setText(3,QString::number(rodList->topLevelItemCount()));
          rodList->addTopLevelItem(nit);
          m_rods.insert(std::make_pair(rodList->topLevelItemCount()-1, PixA::connectivityName(it)));
         if (!const_cast<PixLib::RodBocConnectivity *>(*it)->enableReadout){
             nit->setDisabled(true);
          }
          if (m_disabledList.enabled(PixA::connectivityName(it))) {
	    //nit->setSelected(true);
	    sel.select(rodList->model()->index(rodList->topLevelItemCount()-1, 0, QModelIndex()),
		       rodList->model()->index(rodList->topLevelItemCount()-1, 3, QModelIndex()));
          }
        }
      }
    }
      }
      ++item;
    }
    rodList->selectionModel()->select(sel, QItemSelectionModel::Select);
    if(rodList->topLevelItem(0)==0) {
      return;
    }
    rodList->setEnabled(true);
    allocateButton->setEnabled(true);
  }
  
  void ConnectivityPanel::getSelected(std::map<std::string, std::vector<std::string> > &crateRodList)
  {
      QTreeWidgetItemIterator item(crateList);
    while((*item)){
      if((*item)->isSelected()){
	bool isOK;
    int itID = (*item)->text(2).toInt(&isOK);
	std::string crateName = "";
	if(isOK && m_crates.find(itID)!=m_crates.end()) {
	  crateName = m_crates[itID];
	  std::vector<std::string> rodNames;
      QTreeWidgetItemIterator item1(rodList);
      while ((*item1)){
        if ((*item1)->isSelected()){
	      bool isOK1;
          int it1ID = (*item1)->text(3).toInt(&isOK1);
	      if (isOK1 && m_rods.find(it1ID)!=m_rods.end()){
		rodNames.push_back(m_rods[it1ID]);
	      }
        }
            ++item1;
	  }
	  crateRodList.insert(std::make_pair(crateName,rodNames));
	}
      }
      ++item;
    }
    return;
  }

  void ConnectivityPanel::allocate(std::map<std::string, std::vector<std::string> > &crateRodList)
  {

    TemporaryCursorChange busy;
    getSelected(crateRodList);

    m_engine.allocate(crateRodList);
  }

  void ConnectivityPanel::loadPixDisable() {
    DisablePanel dp(m_engine.getConn(), this);
    if (dp.exec() == QDialog::Accepted) {
      m_disable = dp.getDisable();
      m_disabledList = PixA::DisabledListRoot(m_disable);
      insertPartitions();
    }
  }

  void ConnectivityPanel::disablePartitions(){
      for(int i = 0; i < partList->topLevelItemCount(); ++i){
          QTreeWidgetItem *item = partList->topLevelItem(i);
          if(!partitionSelectAll->isChecked()){
              item->setSelected(false);
          }
      }
  }


  void ConnectivityPanel::disableCrates(){
      for(int i = 0; i < crateList->topLevelItemCount(); ++i){
          QTreeWidgetItem *item = crateList->topLevelItem(i);
          if(!crateSelectAll->isChecked()){
              item->setSelected(false);
          }
      }
  }
  void ConnectivityPanel::disableRods(){
      for(int i = 0; i < rodList->topLevelItemCount(); ++i){
          QTreeWidgetItem *item = rodList->topLevelItem(i);
          if(!rodSelectAll->isChecked()){
              item->setSelected(false);
          }
      }
  }




}
