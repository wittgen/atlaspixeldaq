#ifndef _PixCon_ResetVisetCommand_h_
#define _PixCon_ResetVisetCommand_h_

#include "CommandKits.h"
#include <memory>

namespace PixCon {

  class ConsoleEngine;
  
  ICommand *resetVisetCommand(const std::shared_ptr<ConsoleEngine> &engine);

}

#endif
