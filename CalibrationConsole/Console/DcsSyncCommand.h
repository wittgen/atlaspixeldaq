#ifndef _PixCon_DcsSyncCommand_h_
#define _PixCon_DcsSyncCommand_h_

#include "CommandKits.h"
#include <memory>

namespace PixCon {

  class ConsoleEngine;
  
  ICommand *dcsSyncCommand(const std::shared_ptr<ConsoleEngine> &engine);

}

#endif
