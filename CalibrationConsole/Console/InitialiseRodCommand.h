#ifndef _PixCon_InitialiseRodCommand_h_
#define _PixCon_InitialiseRodCommand_h_

#include "CommandKits.h"
#include <memory>

namespace PixCon {

  class ConsoleEngine;
  
  ICommand *initialiseRodCommand(const std::shared_ptr<ConsoleEngine> &engine);

}

#endif
