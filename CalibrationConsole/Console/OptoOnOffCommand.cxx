#include <ICommand.h>
#include <memory>
#include "ActionCommandBase.h"
#include <PixActions/PixActionsMulti.h>


namespace PixCon {

  class OptoOnOffCommand : public ActionCommandBase
  {
  public:
    OptoOnOffCommand(const std::shared_ptr<ConsoleEngine> &engine, bool on)
      : ActionCommandBase(engine),
        m_on(on)
    {}

    const std::string &name() const { return commandName(m_on); }

    static const std::string &commandName(bool on) {
      return s_name[(on ? 0:1 )];
    }

  protected:

    void setActionSequence(std::vector<std::string> &action_sequence) {
      action_sequence.push_back("optoOnOff");
    }

    bool callAction(PixLib::PixActionsMulti &action) {
      action.optoOnOff(m_on);
      return true;
    }

  private:

    bool m_on;
    static const std::string s_name[2];
  };

  const std::string OptoOnOffCommand::s_name[]={std::string("Switch Opto on"), std::string("Switch Opto off")};


  class OptoOnOffKit : public PixCon::ICommandKit
  {
  protected:
    OptoOnOffKit(bool on)
      :m_on(on)
    {}

  public:
    PixCon::IConfigurableCommand *createCommand(const std::string &/*command*/, const std::shared_ptr<ConsoleEngine> &engine) const {
      assert(engine.get());
      return new OptoOnOffCommand(engine, m_on);
    }

    static bool registerKit() {
      CommandKits::registerKit(OptoOnOffCommand::commandName(true),new OptoOnOffKit(true), PixCon::UserMode::kShifter);
      CommandKits::registerKit(OptoOnOffCommand::commandName(false),new OptoOnOffKit(false), PixCon::UserMode::kShifter);
//      CommandKits::registerKit(OptoOnOffCommand::name(true),new OptoOnOffKit(true));
      return true;
    }

  private:
    static bool s_registered;
    bool m_on;
  };

  bool OptoOnOffKit::s_registered = OptoOnOffKit::registerKit();

  ICommand *optoOnOffCommand(const std::shared_ptr<ConsoleEngine> &engine, bool on) {
    return new OptoOnOffCommand(engine, on);
  }  
}
