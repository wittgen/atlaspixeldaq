// Dear emacs, this is -*-c++-*-
#ifndef _PixCon_IConfigEdit_h_
#define _PixCon_IConfigEdit_h_

#include <string>

namespace PixLib {
  class Config;
}

namespace PixCon {

  class IConfigEdit
  {
  public:
    virtual ~IConfigEdit() {}

    virtual int exec () = 0;

    virtual bool changed() const = 0;

    virtual PixLib::Config &config() = 0;

    virtual PixLib::Config &config(std::string tag_name) = 0;

    virtual std::string tagName() const =0;

    virtual void errorOnSave() = 0;
  };

}
#endif
