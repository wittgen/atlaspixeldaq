#include <ICommand.h>
#include <memory>
#include "ActionCommandBase.h"
#include <PixActions/PixActionsMulti.h>


namespace PixCon {

  class SetHalfclockModCommand : public ActionCommandBase
  {
  public:
    SetHalfclockModCommand(const std::shared_ptr<ConsoleEngine> &engine)
      : ActionCommandBase(engine)
    {}

    const std::string &name() const { return commandName(); }

    static const std::string &commandName() {
      return s_name;
    }

  protected:

    void setActionSequence(std::vector<std::string> &action_sequence) {
      action_sequence.push_back("setHalfclockMod");
    }

    bool callAction(PixLib::PixActionsMulti &action) {
      action.setHalfclockMod();
      return true;
    }

  private:

    static const std::string s_name;
  };

  const std::string SetHalfclockModCommand::s_name("Set Module Return Halfclock");


  class SetHalfclockModKit : public PixCon::ICommandKit
  {
  protected:
    SetHalfclockModKit()
    {}

  public:
    PixCon::IConfigurableCommand *createCommand(const std::string &/*command*/, const std::shared_ptr<ConsoleEngine> &engine) const {
      assert(engine.get());
      return new SetHalfclockModCommand(engine);
    }

    static bool registerKit() {
      CommandKits::registerKit(SetHalfclockModCommand::commandName(),new SetHalfclockModKit, PixCon::UserMode::kExpert);
      //      CommandKits::registerKit(SetHalfclockModCommand::name(true),new SetHalfclockModKit(true));
      return true;
    }

  private:
    static bool s_registered;
  };

  bool SetHalfclockModKit::s_registered = SetHalfclockModKit::registerKit();

}
