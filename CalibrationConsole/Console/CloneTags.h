// Dear emacs, this is -*-c++-*-
#ifndef CLONETAGS
#define CLONETAGS

#include "ui_CloneTagsBase.h"
#include <memory>
#include "PixDbServer/PixDbServerInterface.h"

#include <qcombobox.h>
#include <qlineedit.h>

namespace PixCon {
  class CloneTags : public QDialog, public Ui_CloneTagsBase {
    
    Q_OBJECT
    
  public:
    CloneTags(QWidget* parent = 0, std::string partition_name = "", std::string dbServer_name = "");
    ~CloneTags();
    
  public slots:
    void changeDomain(const QString &domain);
    
  protected slots:
    void accept();

  private:
    std::shared_ptr<PixLib::PixDbServerInterface> m_dbs;
  
  };
}

#endif // CLONETAGS
