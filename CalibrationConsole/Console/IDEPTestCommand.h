// Dear emacs, this is -*-c++-*-
#include <ICommand.h>
#include <memory>
#include <CommandKits.h>

namespace PixLib {
  class PixISManager;
}

namespace PixCon {

  class ConsoleEngine;

  class IDEPTestCommand : public IConfigurableCommand
  {
  public:
    IDEPTestCommand(const std::shared_ptr<ConsoleEngine> &engine)
      : m_engine(engine)
    {}

    ~IDEPTestCommand();

    const std::string &name() const { return commandName(); }

    static const std::string &commandName() {
      return s_name;
    }

    PixCon::Result_t execute();

    /** Nothing to do */
    void resetAbort() { }

    /** cannot do anything */
    void abort() { }

    /** cannot do anything */
    void kill() { }

    bool configure() { return false; }

    bool hasDefaults() const { return true; }
    

  private :

    std::unique_ptr<PixLib::PixISManager> m_isManager;
    std::shared_ptr<ConsoleEngine> m_engine;

    static const std::string s_name;
  };


  class IDEPTestKit : public PixCon::ICommandKit
  {
  protected:
    IDEPTestKit();

  public:
    PixCon::IConfigurableCommand *createCommand(const std::string &/*command*/, const std::shared_ptr<ConsoleEngine> &engine) const;

    static bool registerKit() {
      CommandKits::registerKit(IDEPTestCommand::commandName(),new IDEPTestKit);
      return true;
    }

  private:
    static bool s_registered;

    std::vector<std::string> m_disable;
  };



}
