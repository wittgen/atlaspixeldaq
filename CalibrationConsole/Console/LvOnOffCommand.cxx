#include <ICommand.h>
#include <memory>
#include "ActionCommandBase.h"
#include <PixActions/PixActionsMulti.h>


namespace PixCon {

  class LvOnOffCommand : public ActionCommandBase
  {
  public:
    LvOnOffCommand(const std::shared_ptr<ConsoleEngine> &engine, bool on)
      : ActionCommandBase(engine),
        m_on(on)
    {}

    const std::string &name() const { return commandName(m_on); }

    static const std::string &commandName(bool on) {
      return s_name[(on ? 0:1 )];
    }

  protected:

    void setActionSequence(std::vector<std::string> &action_sequence) {
      action_sequence.push_back("lvOnOff");
    }

    bool callAction(PixLib::PixActionsMulti &action) {
      action.lvOnOff(m_on);
      return true;
    }

  private:

    bool m_on;
    static const std::string s_name[2];
  };

  const std::string LvOnOffCommand::s_name[]={std::string("Switch LV on"), std::string("Switch LV off")};


  class LvOnOffKit : public PixCon::ICommandKit
  {
  protected:
    LvOnOffKit(bool on)
      :m_on(on)
    {}

  public:
    PixCon::IConfigurableCommand *createCommand(const std::string &/*command*/, const std::shared_ptr<ConsoleEngine> &engine) const {
      assert(engine.get());
      return new LvOnOffCommand(engine, m_on);
    }

    static bool registerKit() {
      CommandKits::registerKit(LvOnOffCommand::commandName(true),new LvOnOffKit(true), PixCon::UserMode::kShifter);
      CommandKits::registerKit(LvOnOffCommand::commandName(false),new LvOnOffKit(false), PixCon::UserMode::kShifter);
//      CommandKits::registerKit(LvOnOffCommand::name(true),new LvOnOffKit(true));
      return true;
    }

  private:
    static bool s_registered;
    bool m_on;
  };

  bool LvOnOffKit::s_registered = LvOnOffKit::registerKit();

  ICommand *lvOnOffCommand(const std::shared_ptr<ConsoleEngine> &engine, bool on) {
    return new LvOnOffCommand(engine, on);
  }  
}
