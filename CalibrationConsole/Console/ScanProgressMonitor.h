/* Dear emacs, this is -*-c++-*- */
#ifndef _PixCon_ScanProgressMonitor_h_
#define _PixCon_ScanProgressMonitor_h_

#include <Flag.h>
#include <IStatusMonitor.h>
#include <CalibrationDataTypes.h>
#include "VarDefList.h"
#include <memory>

class IPCPartition;
class ISInfoReceiver;

class IsReceptor;

namespace PixCon {

  class StatusChangeTransceiver;
  class CalibrationDataManager;

  /** Monitors the progress of pix actions.
   * The monitor is started with a list of actions which are expected to get executed in 
   * the given sequence  e.g "reset", "loadConfig" for the action "initCal". The action
   * is considered to have terminated if the status of one of these methods is "FAILED" 
   * or if the sequence of the actions was violated.
   */
  class ScanProgressMonitor : public IStatusMonitor
  {
  public:
    class IsIndexMonitor;

    /** 
     * @param scan_status_var_name the name of the variable filled by the scan status monitor.
     */
    ScanProgressMonitor(const std::shared_ptr<IsReceptor> &receptor,
			const std::shared_ptr<IsMonitorManager> &is_monitor_manager,
			const std::shared_ptr<CalibrationDataManager> &calibration_data,
			const std::shared_ptr<StatusChangeTransceiver> &status_change_transceiver,
			const std::string &scan_status_var_name);

    ~ScanProgressMonitor();

    /** Process the accumulated status information of a monitor.
     */
    void process() ;

    /** Called if no monitor anounced changes to the main monitoring thread within a given amount of time.
     */
    void timeout(double elapsed_seconds);

    void setStatusChangeFlag( Flag &the_flag ) { m_statusChangeFlag = &the_flag; }

    /** Activate the monitoring of the loop index counter.
     * @param serial_number the serial number of the currently monitoried scan.
     * @param mask_stage_loop_index index of the loop before which the mask stage counter is incremented or 3 if it is the last loop.
     * @param start_value an array of the 4 initial counter values : mask stage, loop0, loop1, loop2.
     * @param max_value the number of steps for each of the 4 counter : mask stage, loop0, loop1, loop2.
     * @param max_step_time the maximum time counters may staty unchanged.
     */
    bool startMonitoring(SerialNumber_t serial_number,
			 unsigned int mask_stage_loop_index,
			 const std::vector<unsigned short> &start_value,
			 const std::vector<unsigned short> &max_value,
			 unsigned long max_step_time);

    /** Abort the monitoring of the current action.
     */
    void abortMonitoring() ;

    /** Stop the monitoring gracefully.
     * The monitoring will be aborted on the next time out if the progress did not terminated on its own.
     */
    void initiateMonitoringStop() {
      m_monitoringToBeStopped=true;
    }

    /** Return if an action sequence is currently monitored.
     */
    bool isMonitoring() const { return m_isMonitoring && !m_monitoringToBeStopped;}

    bool waitUntilStopped() { if (m_isMonitoring) { m_stopped.wait();} return true;}

  protected:

    /** Reread the values from IS.
     */
    void update();

    /** Cancel IS subscription.
     */
    void unsubscribe();

    /** Stop monitoring and set status of all RODs which are not yet done to unknown.
     */
    void stopMonitoring();

    /** Update the counter specified by the is name.
     * @param is_name the IS name of the counter.
     * @param time the time when the counter has changed in seconds.
     * @param counter the new value of the counter.
     */
    void updateCounter(const std::string &is_name, ::time_t time, int counter);

    //     /** extract the ROD name from the given is variable name.
    //      */
    //     static std::string extract(const std::string &is_name);

    void counterChanged(ISCallbackInfo *info);

    std::shared_ptr<CalibrationDataManager> &calibrationData() { return m_calibrationData; }

  private:
    std::shared_ptr<IsReceptor> m_receptor;                         /**< The receptor which receives the values from IS.*/
    std::shared_ptr<IsMonitorManager> m_isMonitorManager;           /**< Is monitor manager which initiates reconnects if necessary and 
									   processes the monitor buffers.*/
    std::shared_ptr<IsIndexMonitor>   m_isIndexMonitor;
    std::shared_ptr<CalibrationDataManager> m_calibrationData;      /**< Pointer to the calibration data manager.*/
    Flag               *m_statusChangeFlag;
    std::shared_ptr<StatusChangeTransceiver> m_statusChangeTransceiver;/**< Auxiliary class to notify listeners about status changes.*/

    enum EScanProgressStatus {kNotStarted, kScanning, kStuck, kDone, kAborted, kNStates, kUnknown=kNStates};
    /** The state of the currently running actions for a ROD.
     */
    class RodCounter_t {
    public:
      static const unsigned int kNCounter = 4;

      RodCounter_t()  { reset(); }

      RodCounter_t(const std::vector<unsigned short> &counter)
	: m_total(0)
      {
	if (counter.empty()) {
	  reset();
	}
	else {
	  for (unsigned int counter_i=0; counter_i<kNCounter; counter_i++) {
	    m_counter[counter_i]=counter[counter_i];
	    m_initialValue[counter_i]=counter[counter_i];
	  }
	}
      }

      void reset() {
	m_total = 0;
	for (unsigned int counter_i=0; counter_i<kNCounter; counter_i++) {
	  m_counter[counter_i]=0;
	  m_initialValue[counter_i]=0;
	}
	m_lastUpdate=0;
      }

      /** Set the given loop counter.
       * @param counter_i the counter index : IDX-2 .. IDX-0, Mask stage.
       * @param value the current counter value.
       * @param time time of last update in seconds.
       */
      void setCounter( unsigned int counter_i, unsigned short value, unsigned long time, unsigned int *multiplier) {
	m_wait=false;
	assert(counter_i<kNCounter);
	//	unsigned int old_total = m_total;
	m_total -= m_counter[counter_i]*multiplier[counter_i];
	m_counter[counter_i]=value;
	m_total += m_counter[counter_i]*multiplier[counter_i];
	for (unsigned int i=counter_i+1; counter_i<kNCounter; counter_i++) {
	  if (multiplier[i]>0 && m_counter[i]>0) {
	    m_wait=true;
	    break;
	  } 
	}
	//	if (m_total < old_total ) {
	//	  m_status = kConfused;
	//	}
	m_lastUpdate=time;
      }

      /** Get loop counter.
       */
      unsigned short counter( unsigned int counter_i) const {
	assert(counter_i<4);
	return m_counter[counter_i];
      }

      unsigned int totalCounter() const {
	return m_total;
      }

      /** Return true if all counters reached the maximum value.
       */
      bool isDone(unsigned int max_steps_per_rod/*, unsigned short *max_counter*/) const {
	if (m_wait || m_total+1 < max_steps_per_rod) return false;
	return true;

	// 	for (unsigned int counter_i=0; counter_i<kNCounter; counter_i++) {
	// 	  if (max_counter[counter_i]>0) {
	// 	    if ( m_counter[counter_i]>=max_counter[counter_i]) {
	// 	      return true;
	// 	    }
	// 	    break;
	// 	  }
	// 	}
	// 	for (unsigned int counter_i=0; counter_i<kNCounter; counter_i++) {
	// 	  if (m_counter[counter_i]+1<max_counter[counter_i]) return false;
	// 	}
	//	return true;
      }

      /** Return true if at least one counter is not 
       */
      bool hasStarted() const {
	// 	for (unsigned int counter_i=0; counter_i<kNCounter; counter_i++) {
	// 	  if (m_counter[counter_i]>m_initialValue[counter_i]) return true;
	// 	}
	return m_total>0;
      }

      unsigned long lastUpdate() const { return m_lastUpdate; }

      EScanProgressStatus status() const { return m_status;}

      void updateStatus( unsigned long reference, unsigned long max_step_time, unsigned int total_steps_per_rod/*, short *max_counter*/) {
	if (hasStarted()) {
	  if (isDone(total_steps_per_rod)) {
	    m_status = kDone;
	  }
	  else {
	    if (reference - lastUpdate() < max_step_time) {
	      m_status = kScanning;
	    }
	    else {
	      m_status = kStuck;
	    }
	  }
	}
	else {
	  m_status = kNotStarted;
	}
      }


    private:
      unsigned int   m_total;
      unsigned short m_counter[kNCounter];
      unsigned short m_initialValue[kNCounter];
      unsigned long  m_lastUpdate;
      EScanProgressStatus m_status;
      bool           m_wait;
    };

    /** Set the scan status of the current scan to stuck or back to running.
     */
    void propagateStuckState(EScanProgressStatus current_status, const std::string &rod_name);

    SerialNumber_t                      m_monitoredScan;                       /**< Serial number of the monitored scan.*/
    VarId_t                             m_scanStatusVar;                       /**< Id of the scan status variable. */
    Mutex                               m_rodCounterMutex;                     /**< Mutex to protect the rod status list.*/
    std::map<std::string, RodCounter_t> m_rodCounter;                          /**< The action satus per ROD.*/
    unsigned long                       m_maxStepTime;                         /**< maximum time which may elapse between counter changes for a ROD.*/
    unsigned short                      m_maxCounter[RodCounter_t::kNCounter]; /**< The number of steps for the 4 counters.*/
    unsigned int                        m_multiplier[RodCounter_t::kNCounter]; /**< The multiplier for each counter to calculate the total step number.*/

    class Stat_t {
    public:
      Stat_t() { reset(); }

      void reset() {
	m_n=0;
	m_sum=0;
	m_sum2=0;
      }

      double mean() const {
	if (m_n>0) {
	  return m_sum/m_n;
	}
	else {
	  return m_sum;
	}
      }

      double rms() const {
	if (m_n>1) {
	  return sqrt((m_sum2-m_sum*m_sum/m_n)/(m_n-1));
	}
	else {
	  return m_sum/2;
	}
      }

      void add(double value) {
	m_sum+=value;
	m_sum2+=value*value;
	m_n++;
      }

      unsigned int n() const {
	return m_n;
      }

    private:
      double m_sum;
      double m_sum2;
      unsigned int m_n;
    };

    unsigned long maxStepTime(unsigned int var_i) const {
      unsigned long max_step_time;
      if (m_maxCounterStepTime[var_i].n()>0) {
	max_step_time=static_cast<unsigned long>(m_maxCounterStepTime[var_i].mean()+2*m_maxCounterStepTime[var_i].rms());
      }
      else {
	max_step_time =m_maxStepTime;
      }
      return (max_step_time>1 ? max_step_time : 1);
    }

    unsigned long maxStepTime() const {
      unsigned int long max_step_time =m_maxStepTime;
      for (unsigned int var_i=0; var_i<RodCounter_t::kNCounter; ++var_i) {
	if (m_maxCounterStepTime[var_i].n()>0) {
	  unsigned long  counter_max_step_time = maxStepTime(var_i);
	  if (counter_max_step_time>max_step_time) {
	    max_step_time=counter_max_step_time;
	  }
	}
      }
      return max_step_time;
    }

    Stat_t                       m_maxCounterStepTime[RodCounter_t::kNCounter];
    unsigned int                        m_globalCounter;
    unsigned int                        m_stepsTotalPerRod;
    unsigned int                        m_stepsTotal;

    static const unsigned long          s_progressUpdateTime = 2;
    unsigned long                       m_lastProgressUpdate;

    static const std::string s_rodScanProgressStatusName;                      /**< The name of the scan progress in the calibration data container.*/
    static const char*       s_rodCounterVarNames[RodCounter_t::kNCounter];    /**< The name of the scan loop counter in IS. */
    static const char*       s_rodCounterVarPattern[2];                        /**< Patterns to match the IDX and mask stage counter. */

    Flag               m_stopped;

    bool m_isMonitoring;
    bool m_stopMonitoring;
    bool m_monitoringToBeStopped;
    bool m_verbose;  // for debugging
  };

}
#endif
