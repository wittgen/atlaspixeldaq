// Dear emacs, this is -*-c++-*-x
#ifndef ConnectivityPanel_H
#define ConnectivityPanel_H

#include "ui_ConnectivityPanelBase.h"
#include "UserMode.h"
#include <map>
#include <vector>
#include <string>
#include "ConfigWrapper/Connectivity.h"
#include <ConfigWrapper/PixDisableUtil.h>
#include <memory>

class QApplication;

namespace PixA{
  class ConnectivityRef;
}

namespace PixLib {
  class PixDisable;
}

namespace PixCon{
  class ConsoleEngine;
  class MainPanel;

  class ConnectivityPanel : public QDialog, public Ui_ConnectivityPanelBase
  {
    Q_OBJECT

  public:
    ConnectivityPanel(const PixCon::UserMode::EUserMode& current_user,
		      PixCon::ConsoleEngine &engine,
		      QApplication *app,
		      QWidget* parent = 0);
    ~ConnectivityPanel();
    std::shared_ptr<const PixLib::PixDisable> getDisable() {return m_disable;}


  public slots:
    void insertPartitions();
    void insertCrates();
    void insertRods();
    void getSelected(std::map<std::string, std::vector<std::string> > &crateRodList);
    void allocate(std::map<std::string, std::vector<std::string> > &crateRodList);
    void loadPixDisable();
    void disablePartitions();
    void disableCrates();
    void disableRods();

  private:
    std::map<int,std::string> m_parts, m_crates, m_rods;
    std::map<std::string, std::vector <std::string> > m_localPartCrate;
    std::shared_ptr<const PixLib::PixDisable> m_disable;
    PixA::ConnectivityRef m_conn;
    ConsoleEngine &m_engine;
    QApplication  *m_app;
    PixA::DisabledListRoot m_disabledList;
  };
}

#endif
