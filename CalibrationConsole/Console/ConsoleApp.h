// Dear emacs, this is -*-c++-*-
#ifndef _ConsoleApp_H_
#define _ConsoleApp_H_

#  if defined(HAVE_QTGSI)
#    include "TQRootApplication.h"
#    include "TQApplication.h"

typedef TQRootApplication QRootApplication;

#  else
#include <QApplication>

// dummy implementation
// no ROOT - histograms will not be displayed.
class QRootApplication : public QApplication
{
public:
  QRootApplication(int &argc, char **argv,int poll=0) : QApplication(argc, argv) {}
};

#  endif

#include <vector>
#include <string>

#include <memory>
#include <memory>
#include <Lock.h>
#include <CalibrationDataTypes.h>
#include "UserMode.h"

class IPCPartition;
class ISInfoReceiver;

namespace PixLib {
  class PixDbServerInterface;
}

namespace PixA {
  class IModuleMapH2Factory;
}

namespace PixCon {
  //  class OHHistogramProvider;
  class IHistogramProvider;
  class CategoryList;
  class CalibrationDataManager;
  class ConsoleViewer;
  class PartWin;
  class IsReceptor;
  class ConsoleEngine;
}

class ConsoleApp : public QRootApplication
{
public:
  ConsoleApp(std::shared_ptr<PixCon::UserMode>& user_mode,
	     const std::string &partition_name,
	     const std::string &is_server_name,
	     const std::string &oh_server_name,
	     const std::string &histo_name_server_name,
	     std::shared_ptr<PixLib::PixDbServerInterface> db_server,
	     const std::shared_ptr<PixA::IModuleMapH2Factory> &map_factory,
	     const std::string &ddc_partition_name,
	     const std::string &ddc_is_name,
	     int &qt_argc,
	     char **qt_argv,
	     int poll=0);

  ~ConsoleApp();

  //  void addScan( PixCon::SerialNumber_t scan_serial_number );

  //  void setTags(const std::string &object_tag_name, const std::string &tag_name, const std::string &cfg_tag_name);

protected:
  //  static unsigned int addOHFiles( PixCon::OHHistogramProvider &histogram_server, const std::vector<std::string> &result_files );

  std::shared_ptr<PixCon::CategoryList> m_categories;
  std::shared_ptr<PixCon::CalibrationDataManager> m_calibrationData;

private:
  std::shared_ptr<PixCon::UserMode> m_userMode;
  std::unique_ptr<PixCon::ConsoleViewer> m_viewer;
  std::unique_ptr<IPCPartition> m_partition;
  std::unique_ptr<ISInfoReceiver> m_infoReceiver;
  std::shared_ptr<PixCon::IsReceptor> m_receptor;

  std::shared_ptr<PixCon::IHistogramProvider> m_histogramProvider;
  std::shared_ptr<PixCon::IHistogramProvider> m_histogramProviderInterceptor;

  static const char *s_pixDbServerName;
};
#endif
