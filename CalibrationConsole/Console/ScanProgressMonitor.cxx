#include <IsMonitor.h>
#include "ScanProgressMonitor.h"
#include "DoNothingExtractor.h"
#include "ProgressEvent.h"
#include <QNewVarEvent.h>

#include <is/inforeceiver.h>
#include <is/infoiterator.h>

#include <ConfigWrapper/Connectivity.h>
#include <ConfigWrapper/ConnectivityUtil.h>

#include <DefaultColourCode.h>

#include <StatusChangeTransceiver.h>
#include <IsReceptor.h>
#include <IsMonitorManager.h>

#include <ScanIsStatus.hh>


#ifdef DEBUG
#  define DEBUG_TRACE(a) { a; }
#  include <iostream>
#else
#  define DEBUG_TRACE(a) {  }
#endif

namespace PixCon {

  const char *ScanProgressMonitor::s_rodCounterVarNames[ScanProgressMonitor::RodCounter_t::kNCounter]={
    "Mask stage",
    "IDX-2",
    "IDX-1",
    "IDX-0",
  };

  const char *ScanProgressMonitor::s_rodCounterVarPattern[2]={
    ".*ROD_CRATE.*/IDX-.",
    ".*ROD_CRATE.*/Mask stage"
  };

  // counter name :
  // [is-server].ROD_CRATE_?/ROD_??_S??/[var name]
  class CounterExtractor : public IVarNameExtractor
  {
  public:

    CounterExtractor(const std::string &is_name) 
    {
      std::string::size_type pos  = is_name.find(".");
      if (pos != std::string::npos && pos+1<is_name.size()) {
	std::string::size_type start_pos  = is_name.find("/",pos+1);
	if (start_pos != std::string::npos) {
	  std::string::size_type end_pos  = is_name.find("/",start_pos+1);
	  if (end_pos != std::string::npos && end_pos+1<is_name.size()) {
	    m_rodName = is_name.substr(start_pos+1,end_pos-start_pos-1);
	    m_varName = is_name.substr(end_pos+1,is_name.size()-end_pos-1);
	  }
	}
      }
    }

    void extractVar(const std::string &/*connectivity_name*/, ConnVar_t &conn_var) const
    {
      conn_var.connName() = m_rodName;
      conn_var.varName()  = m_varName;
    }

    const std::string &rodName() const { return m_rodName; }
    const std::string &varName() const { return m_varName; }

  private:
    std::string              m_rodName;
    std::string              m_varName;
  };

  const std::string ScanProgressMonitor::s_rodScanProgressStatusName="ScanProgress";


  class ScanProgressMonitor::IsIndexMonitor : public SimpleIsMonitorReceiver<int,int>
  {
  public:
    IsIndexMonitor(ScanProgressMonitor *scan_progress_monitor,
		   std::shared_ptr<IsReceptor> receptor,
		   EValueConnType conn_type,
		   unsigned int max_time_between_updates)
      : SimpleIsMonitorReceiver<int,int>(receptor,conn_type,0,".*",max_time_between_updates),
	m_scanProgressMonitor(scan_progress_monitor)
    { assert(m_scanProgressMonitor); }

    void init() {
      // ignore initial values
      // subscribe
      ISInfoT<int> is_value;
      for (unsigned int counter_pattern_i=0; counter_pattern_i<2; counter_pattern_i++) {

	DEBUG_TRACE( std::cout << "INFO [ScanProgressMonitor::init] subscribe to " \
		     << receptor()->isServerName().c_str() << " pattern=" \
		     <<  ScanProgressMonitor::s_rodCounterVarPattern[counter_pattern_i] << std::endl );

	ISCriteria criteria = is_value.type() && ScanProgressMonitor::s_rodCounterVarPattern[counter_pattern_i];
	receptor()->receiver().subscribe(receptor()->isServerName().c_str(),
					 criteria,
					 &IsMonitorReceiverBase<ISInfoT<int>, int>::valueChanged,
					 this);
      }
      DEBUG_TRACE( std::cout << "INFO [ScanProgressMonitor::IsIndexMonitor::init] Activated." << std::endl )
    }

    void _update()
    {
      this->setNeedUpdate(false);
      this->setLastUpdate(time(NULL));

      DEBUG_TRACE( std::cout << "INFO [ScanProgressMonitor::IsIndexMonitor::_update] " << this->varName()<< std::endl );

      ISInfoT<int> is_value;

      // Get current values
      for (unsigned int counter_pattern_i=0; counter_pattern_i<2; counter_pattern_i++) {
	ISCriteria criteria = is_value.type() && ScanProgressMonitor::s_rodCounterVarPattern[counter_pattern_i];

	ISInfoIterator info_iter( receptor()->partition(), receptor()->isServerName().c_str(), criteria );

	while( info_iter() ) {
	  // DEBUG_TRACE( std::cout << "INFO [IsMonitorReceiver<Src_t>::while ] start "<< std::endl );

	  info_iter.value(is_value);

	  // DEBUG_TRACE( std::cout << "INFO [IsMonitorReceiver<Src_t>::while ] read is_value "<< std::endl );
	  std::string var_name = info_iter.name();

	  DEBUG_TRACE( std::cout << "INFO [ScanProgressMonitor::IsIndexMonitor::_update ] var_name =  "<<var_name<< std::endl );

	  setValue(var_name, info_iter.time().c_time(), is_value.getValue());

	}
      }
    }

    void _unsubscribe() {

      const std::string &is_server_name = receptor()->isServerName();

      ISInfoT<int> is_value;
      for (unsigned int counter_pattern_i=0; counter_pattern_i<2; counter_pattern_i++) {
	try {
	  ISCriteria criteria = is_value.type() && ScanProgressMonitor::s_rodCounterVarPattern[counter_pattern_i];
	  receptor()->receiver().unsubscribe(is_server_name,criteria);
	}
	catch (...) {
	}
      }

    }

    void setValue(const std::string &is_name, ::time_t time_stamp, int value)
    {
      m_scanProgressMonitor->updateCounter(is_name, time_stamp, value);
    }

    void registerForCleanup(const std::string &is_name, std::map<std::string, std::set<std::string> > &clean_up_var_list) {
      ConnVar_t conn_var(valueConnType()  /* unused */);
      CounterExtractor extractor(is_name);
      if (!extractor.rodName().empty()) {
	for (unsigned int counter_i=0; counter_i<RodCounter_t::kNCounter; counter_i++) {
	  this->receptor()->registerForCleanup(kCurrent,0, extractor.rodName(), ScanProgressMonitor::s_rodCounterVarNames[counter_i], clean_up_var_list);
	}
      }
    }


  private:
    ScanProgressMonitor *m_scanProgressMonitor;
  };


  ScanProgressMonitor::ScanProgressMonitor(const std::shared_ptr<IsReceptor> &receptor,
					   const std::shared_ptr<IsMonitorManager> &is_monitor_manager,
					   const std::shared_ptr<CalibrationDataManager> &calibration_data,
					   const std::shared_ptr<StatusChangeTransceiver> &status_change_transceiver,
					   const std::string &scan_status_var_name)
    : m_receptor(receptor),
      m_isMonitorManager(is_monitor_manager),
      m_calibrationData(calibration_data),
      m_statusChangeTransceiver( status_change_transceiver),
      m_monitoredScan(0),
      m_scanStatusVar(VarDefBase_t::invalid().id()),
      m_isMonitoring(false),
      m_stopMonitoring(false),
      m_monitoringToBeStopped(false),
      m_verbose(false)
  {

    // @todo read variable definition from db
    try {
      const VarDef_t var_def( VarDefList::instance()->getVarDef(s_rodScanProgressStatusName) );
      if (!var_def.isStateWithOrWithoutHistory() ) {
	std::cerr << "ERROR [ScanProgressMonitor::ctor] variable " << s_rodScanProgressStatusName
		  << " already exists but it is not a state variable." << std::endl;
      }
    }
    catch (UndefinedCalibrationVariable &) {
      VarDef_t var_def( VarDefList::instance()->createStateVarDef(s_rodScanProgressStatusName, kRodValue, kNStates) );
      if ( var_def.isStateWithOrWithoutHistory() ) {
	//	scan_variables.addVariable(histogram_state_name);
	StateVarDef_t &state_var_def = var_def.stateDef();

	state_var_def.addState(kNotStarted,            DefaultColourCode::kConfigured,      "Not started.");
	state_var_def.addState(kScanning,              DefaultColourCode::kScanning,        "Scanning");
	state_var_def.addState(kDone,                  DefaultColourCode::kSuccess,         "Ok");
	state_var_def.addState(kStuck,                 DefaultColourCode::kOn,              "Stuck");
	state_var_def.addState(kAborted,               DefaultColourCode::kFailed,          "Aborted");

	m_receptor->notify(new QNewVarEvent(kCurrent, 0, "General", s_rodScanProgressStatusName) );

      }
      else {
	std::cerr << "ERROR [ScanProgressMonitor::ctor] variable " << s_rodScanProgressStatusName 
		  << " already exists but it is not a state variable." << std::endl;
      }
    }

    if (!scan_status_var_name.empty()) {
      try {
	m_scanStatusVar =  VarDefList::instance()->getVarDef(scan_status_var_name).id();
      }
      catch(UndefinedCalibrationVariable &) {
	std::cerr << "ERROR [ScanProgressMonitor::ctor] Scan status variable not yet defined."
		  << "The Scan progress monitor will not be able to propagate the \"stuck\" state to the scan status."
		  << " This is a logic error in the code. It means that the scan status monitor is created after the "
		  << " scan progress monitor." << std::endl;
      }
    }

  }

  ScanProgressMonitor::~ScanProgressMonitor()
  {
    if (m_isIndexMonitor) {
      m_isMonitorManager->removeMonitor(m_isIndexMonitor);
    }
  }

  void ScanProgressMonitor::process()
  {
    if (!m_isMonitoring) return ;

    if (!m_stopMonitoring) {
      // for debugging:
      if (m_verbose) {
	std::cout << "INFO [ScanProgressMonitor::process] start." << std::endl; 
      }

      {
	Lock lock(m_rodCounterMutex);
	for ( std::map<std::string, RodCounter_t>::iterator rod_iter = m_rodCounter.begin();
	      rod_iter != m_rodCounter.end();
	      rod_iter++) {

	  if (m_verbose) {
	    std::cout << "INFO [ScanProgressMonitor::process] Rod " << rod_iter->first << " status = " << rod_iter->second.status() << "." << std::endl; 
	  }

	  if (!rod_iter->second.isDone(m_stepsTotalPerRod)) {
	    if (m_verbose) {
	      std::cout << "INFO [ScanProgressMonitor::process] Rod " << rod_iter->first << " not done yet. wait." << std::endl; 
	    }
	    return;
	  }
	}
      }
    }

    if (m_verbose) {
      std::cout << "INFO [ScanProgressMonitor::process] global counter = "  << m_globalCounter << " / " << m_stepsTotal << std::endl; 
    }
    stopMonitoring();
    m_statusChangeTransceiver->signal(kCurrent,0);
  }

  void ScanProgressMonitor::timeout(double /*elapsed_seconds*/)
  {
    if (!m_isMonitoring) return;

    bool all_done=true;
    bool all_stuck_or_done=true;
    {
    Lock lock(m_rodCounterMutex);
    ::time_t current_time(time(NULL));
    DoNothingExtractor status_var_extractor(s_rodScanProgressStatusName);
    for ( std::map<std::string, RodCounter_t>::iterator rod_iter = m_rodCounter.begin();
	  rod_iter != m_rodCounter.end();
	  rod_iter++) {

      EScanProgressStatus last_status =  rod_iter->second.status();
      rod_iter->second.updateStatus(static_cast<unsigned long>(current_time), maxStepTime(), m_stepsTotalPerRod /*, m_maxCounter*/ );
      if (rod_iter->second.status() == kStuck) {
	std::cout << "INFO [ScanProgressMonitor::timeout] " << static_cast<unsigned long>(current_time) << " - " << rod_iter->second.lastUpdate()
		  << " = " << static_cast<unsigned long>(current_time) - rod_iter->second.lastUpdate()
		  << " < " << maxStepTime() << " (" << m_maxStepTime << ")" << std::endl;
      }
      if (last_status != rod_iter->second.status() ) {

	if (m_verbose) {
	  std::cout << "INFO [ScanProgressMonitor::timeout] Status of ROD " << rod_iter->first
		    << " changed to " << rod_iter->second.status()
		    << " ." << std::endl;
	}
	m_receptor->setValue(kRodValue, kCurrent,0, rod_iter->first , status_var_extractor, static_cast<State_t>(rod_iter->second.status() ) );
	if ((rod_iter->second.status() == kStuck || last_status == kStuck)
	    && VarDefList::instance()->isValid(m_scanStatusVar)) {
	  propagateStuckState(rod_iter->second.status(), rod_iter->first);
	}
      }
      if (rod_iter->second.status() < kStuck) {
	all_stuck_or_done = false;
	all_done = false;
      }
      else if (rod_iter->second.status() < kDone || rod_iter->second.status()>=kNStates) {
	all_done = false;
      }
    }
    }
    if (all_done || (all_stuck_or_done &&  m_monitoringToBeStopped) ) {
      if (m_verbose) {
	if (all_done) {
	  std::cout << "INFO [ScanProgressMonitor::timeout] All RODs are done. Clear list."
		    << " ." << std::endl;
	}
	else {
	  std::cout << "INFO [ScanProgressMonitor::timeout] Monitoring aborted."
		    << " ." << std::endl;
	}
      }
      //      m_rodCounter.clear();
      m_stopMonitoring=true;
      stopMonitoring();
    }
  }

  bool ScanProgressMonitor::startMonitoring(SerialNumber_t scan_serial_number,
					    unsigned int mask_stage_loop_index,
					    const std::vector<unsigned short> &start_value,
					    const std::vector<unsigned short> &max_value,
					    unsigned long max_step_time)
  {
    if (m_verbose) {
      std::cout << "INFO [ScanProgressMonitor::startMonitoring] first stop old monitoring if still active." << std::endl;
    }
    if (m_isMonitoring && m_monitoringToBeStopped) {
      abortMonitoring();
      waitUntilStopped();
      m_monitoringToBeStopped=false;
    }

    assert(    (start_value.empty() || start_value.size()==RodCounter_t::kNCounter)
	       &&  max_value.size() == RodCounter_t::kNCounter);
    const unsigned short *start_value_ptr=NULL;
    {
      Lock rod_counter_lock(m_rodCounterMutex);
      if (m_isMonitoring) {
	throw std::runtime_error("ERROR [ScanProgressMonitor::startMonitoring] Already/still monitoring the scan loop counters.");
      }

      unsigned short default_start_value[RodCounter_t::kNCounter];

      // calculate counter multiplier, total steps 
      {
	std::vector<unsigned int> order;
	unsigned int idx_i=1;

	// put the mask stage index at the right position
	for (unsigned int counter_i=0; counter_i< RodCounter_t::kNCounter; ++counter_i) {
	  if (mask_stage_loop_index==3-counter_i) {
	    order.push_back(0);
	  }
	  order.push_back(idx_i++);
	}
	m_stepsTotal=0;
	m_stepsTotalPerRod = 1;
	unsigned int multiplier = 1;
	std::cout << "INFO [ScanProgressMonitor::startMonitoring] order : ";
	for (unsigned int order_i=RodCounter_t::kNCounter; order_i-->0;) {
	  unsigned int counter_i=order[order_i];
	  std::cout << "  " << counter_i << ":" << s_rodCounterVarNames[counter_i] << " x"<<multiplier;
	  default_start_value[counter_i]=0;
	  std::cout << "~~~~~~~ScanProgressMonitor: " << mask_stage_loop_index << ", " << max_value[counter_i] << ", " << counter_i << std::endl;
    
	  m_maxCounter[counter_i] = max_value[counter_i];
	  if (m_maxCounter[counter_i]>0) {
	    m_stepsTotalPerRod *= m_maxCounter[counter_i];
	    m_multiplier[counter_i] = multiplier;
	    multiplier *= m_maxCounter[counter_i];
	  }
	  else {
	    m_multiplier[counter_i]=0;
	  }
	}
	std::cout << std::endl;
      }

      m_maxStepTime = (max_step_time > 1 ? max_step_time : 1);
      for (unsigned int counter_i=0; counter_i<RodCounter_t::kNCounter; ++counter_i) {
	m_maxCounterStepTime[counter_i].reset();
      }

      if (start_value.empty()) {
	start_value_ptr = default_start_value;
      }
      else {
	assert(start_value.size()==RodCounter_t::kNCounter);
	start_value_ptr = &(start_value[0]);
      }

      m_globalCounter=0;
      for (unsigned int counter_i=RodCounter_t::kNCounter; counter_i-->0;) {
	m_globalCounter += m_multiplier[counter_i]*start_value_ptr[counter_i];
      }

      m_monitoredScan=scan_serial_number;
      m_isMonitoring=true;
      m_stopMonitoring=false;
      m_monitoringToBeStopped=false;
      m_lastProgressUpdate=0;

      // initialise the ROD status variables
      EMeasurementType measurement_type = kCurrent;
      SerialNumber_t serial_number = 0;
      PixA::ConnectivityRef conn = calibrationData()->connectivity(measurement_type, serial_number);
      DoNothingExtractor extractor( s_rodScanProgressStatusName );

      unsigned int n_rods=0;
      if (conn) {
	VarDefList *var_def_list = VarDefList::instance();
	bool announce[RodCounter_t::kNCounter];
	for (unsigned int var_i=0; var_i<RodCounter_t::kNCounter; var_i++) {
	  announce[var_i]=false;
	  try {
	    VarDef_t var_def( var_def_list->getVarDef(s_rodCounterVarNames[var_i]) );
	    if (m_verbose) {
	      std::cout << "INFO [ScanProgressMonitor::startMonitoring] Var " <<  s_rodCounterVarNames[var_i] << " exists already."
			<< std::endl;
	    }
	    if (var_def.connType() != kRodValue) {
	      var_def.setAllConnType();
	    }
	    if (var_def.isValueWithOrWithoutHistory()) {
	      ValueVarDef_t &value_def = var_def.valueDef();
	      value_def.setMinMax(start_value_ptr[var_i],m_maxCounter[var_i]);
	    }
	    else {
	      std::cerr << "ERROR [ScanProgressMonitor::startMonitoring] Var " <<  s_rodCounterVarNames[var_i] << " already exists but is not a value."
			<< std::endl;
	    }
	  }
	  catch(UndefinedCalibrationVariable &err) {
	    var_def_list->createValueVarDef(s_rodCounterVarNames[var_i], kRodValue,start_value_ptr[var_i],m_maxCounter[var_i]);
	    announce[var_i]=true;
	  }
	}

	m_rodCounter.clear();

	for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
	     crate_iter != conn.crates().end();
	     ++crate_iter) {

	  PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
	  PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
	  for (PixA::RodList::const_iterator rod_iter = rod_begin;
	       rod_iter != rod_end;
	       ++rod_iter) {

	    // ignore disabled RODs
	    {
	      Lock calib_data_lock(calibrationData()->calibrationDataMutex());
	      try {
		const CalibrationData::ConnObjDataList conn_obj_data ( calibrationData()->calibrationData().getConnObjectData( PixA::connectivityName(rod_iter)) );
		if ( !conn_obj_data.enableState(measurement_type, serial_number).enabled() ) {
		  continue;
		}
	      }
	      catch(CalibrationData::MissingConnObject &) {
		// @todo Should not happen. But could it happen ? What to do if it happens ? 
		std::cerr << "ERROR [ScanProgressMonitor::startMonitoring] No calibration data for ROD " << PixA::connectivityName(rod_iter) 
			  << " in \"current\"."
			  << std::endl;
	      }

	      // ignore rods without enabled modules.
	      bool has_enabled_modules=false;

	      PixA::Pp0List::const_iterator pp0_begin = PixA::pp0Begin(rod_iter);
	      PixA::Pp0List::const_iterator pp0_end = PixA::pp0End(rod_iter);
	      for (PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
		   pp0_iter != pp0_end && !has_enabled_modules;
		   ++pp0_iter) {

		PixA::ModuleList::const_iterator module_begin = PixA::modulesBegin(pp0_iter);
		PixA::ModuleList::const_iterator module_end = PixA::modulesEnd(pp0_iter);
		for (PixA::ModuleList::const_iterator module_iter = module_begin;
		     module_iter != module_end;
		     ++module_iter) {

		  {
		    PixCon::CalibrationData::EnableState_t enable_state;
		    enable_state.setInConnectivity(false);

		    try {
		      const CalibrationData::ConnObjDataList conn_obj_data ( calibrationData()->calibrationData().getConnObjectData( PixA::connectivityName(module_iter)) );
		      //module_iter.getFeFlav();
		      enable_state = conn_obj_data.enableState(measurement_type, serial_number);
		      if (enable_state.enabled()) {
			has_enabled_modules=true;
			break;
		      }
		    }
		    catch( PixCon::CalibrationData::MissingConnObject &) {
		    }
		  }
		}
	      }
	      if (!has_enabled_modules) continue;

	    }
	    m_rodCounter.insert(std::make_pair(PixA::connectivityName(rod_iter) ,RodCounter_t(start_value)));

	    for (unsigned int var_i=0; var_i<RodCounter_t::kNCounter; var_i++) {
	      std::string temp(s_rodCounterVarNames[var_i]);
	      DoNothingExtractor idx_extractor( temp );
	      m_receptor->setValue(kRodValue, kCurrent,0,PixA::connectivityName(rod_iter) , idx_extractor, static_cast<float>(0) );
	    }

	    m_receptor->setValue(kRodValue, kCurrent,0,PixA::connectivityName(rod_iter) , extractor, static_cast<State_t>(kNotStarted) );
	    n_rods++;
	  }
	}
	m_stepsTotal = (m_stepsTotalPerRod-1) * n_rods;

	for (unsigned int var_i=0; var_i<RodCounter_t::kNCounter; var_i++) {
	  if (announce[var_i]) {
	    m_receptor->notify( new QNewVarEvent(kCurrent, 0, "General", s_rodCounterVarNames[var_i]) );
	    if (m_verbose) {
	      std::cout << "INFO [ScanProgressMonitor::startMonitoring] Announce new var  " <<  s_rodCounterVarNames[var_i] << "."
			<< std::endl;
	    }
	  }
	}

	if (m_rodCounter.empty()) {
	  std::cerr << "ERROR [ScanProgressMonitor::startMonitoring] No enabled RODs in \"current\". So nothing to monitor."
		    << std::endl;
	  m_isMonitoring=false;
	  return false;
	}
      }
      else {
	std::cerr << "ERROR [ScanProgressMonitor::startMonitoring] No connectivity so nothing to monitor in \"current\"."
		  << std::endl;
	m_isMonitoring=false;
	return false;
      }
    }

    if (!m_isIndexMonitor) {
      m_isIndexMonitor=std::make_shared<IsIndexMonitor>(this,m_receptor,kRodValue,8*60);
    }
    m_isMonitorManager->addMonitor("ScanProgress",m_isIndexMonitor);

    return true;
  }

  void ScanProgressMonitor::update() 
  {
    if (m_isIndexMonitor) {
      m_isIndexMonitor->setNeedUpdate(true);
    }
  }

  void ScanProgressMonitor::abortMonitoring()
  {
    if (m_isMonitoring) {
      if (m_verbose) {
	std::cout << "INFO [ScanProgressMonitor::abortMonitoring] " << std::endl; 
      }
      m_stopMonitoring=true;
      m_statusChangeFlag->setFlag();
    }
  }

  void ScanProgressMonitor::stopMonitoring() {
    if (!m_isMonitoring) return;
    if (!m_isIndexMonitor) return;

    if (m_verbose) {
      std::cout << "INFO [ScanProgressMonitor::stopMonitoring] stop monitoring." << std::endl;
    }

    m_isIndexMonitor->unsubscribe();
    m_isMonitorManager->removeMonitor(m_isIndexMonitor);
    m_isIndexMonitor->update();

    {
      Lock rod_list_lock(m_rodCounterMutex);
      ::time_t current_time = time(0);
      DoNothingExtractor status_var_extractor(s_rodScanProgressStatusName);
      for ( std::map<std::string, RodCounter_t>::iterator rod_iter = m_rodCounter.begin();
	    rod_iter != m_rodCounter.end();
	    rod_iter++) {

	rod_iter->second.updateStatus(static_cast<unsigned long>(current_time), maxStepTime(), m_stepsTotalPerRod /*, m_maxCounter*/ );
	EScanProgressStatus status =  rod_iter->second.status();

	if (status == kScanning ) {
	  status = kAborted;

	  if (m_verbose) {
	    std::cout << "INFO [ScanProgressMonitor::stopMonitoring] Status of ROD " << rod_iter->first 
		      << " is " << status
		      << " ." << std::endl;
	  }
	  m_receptor->setValue(kRodValue, kCurrent, 0, rod_iter->first , status_var_extractor, static_cast<State_t>(status) );
	}
      }
      m_receptor->notify(new ProgressEvent(m_globalCounter, m_stepsTotal));

      m_rodCounter.clear();
    }

    m_isMonitoring=false;
    m_stopMonitoring=false;
    m_monitoringToBeStopped=false;

    std::cout << "INFO [ScanProgressMonitor::stopMonitoring] "
	      << " per step times : ";
    for (unsigned int var_i=0; var_i<RodCounter_t::kNCounter; ++var_i) {
      std::cout << var_i << ":" << m_maxCounterStepTime[var_i].mean() << "+-" <<  m_maxCounterStepTime[var_i].rms() 
		<< "(" << m_maxCounterStepTime[var_i].n() << ")"
		<< ", ";
    }
    std::cout << std::endl;

    if (m_verbose) {
      std::cout << "INFO [ScanProgressMonitor::stopMonitoring] stopped." << std::endl;
    }
    m_stopped.setFlag();
  }


  void ScanProgressMonitor::updateCounter(const std::string &is_name, ::time_t time, int counter)
  {
    // for debugging:

         if (m_verbose) {
           std::cout << "INFO [ScanProgressMonitor::updateCounter] IS var " << is_name << " changed to " << counter << "." << std::endl; 
        }

    CounterExtractor extractor(is_name);
    if (!extractor.rodName().empty()) {

      Lock lock(m_rodCounterMutex);
      if (!m_isMonitoring) return;
      std::map<std::string, RodCounter_t >::iterator rod_iter = m_rodCounter.find( extractor.rodName() );
      if (rod_iter != m_rodCounter.end()) {

	// start from fastest counting variable
	for (unsigned int var_i=RodCounter_t::kNCounter; var_i-->0; ) {

	  if (s_rodCounterVarNames[var_i] == extractor.varName()) {
	    EScanProgressStatus last_status = rod_iter->second.status();
	    m_globalCounter -= rod_iter->second.totalCounter();
	    unsigned long elapsed_time=time - rod_iter->second.lastUpdate();
	    if (rod_iter->second.lastUpdate()>0) {
	      m_maxCounterStepTime[var_i].add(elapsed_time);
	    }

	    if (last_status==kStuck) {
	      std::cout << "INFO [ScanProgressMonitor::updateCounter] "
			<< time << " - " << rod_iter->second.lastUpdate()
			<< " elapsed=" << elapsed_time
			<< " < " << maxStepTime(var_i)
			<< " (" << m_maxStepTime << ")"
			 << std::endl;
	    }


	    rod_iter->second.setCounter(var_i, counter, time, m_multiplier);
	    m_globalCounter += rod_iter->second.totalCounter();

	    m_receptor->setValue(kRodValue, kCurrent,0, extractor.rodName() , extractor, static_cast<float>(counter) );
	    rod_iter->second.updateStatus( time,maxStepTime(var_i),m_stepsTotalPerRod /*, m_maxCounter*/);
	    //	     std::cout << "INFO [ScanProgressMonitor::updateCounter]  " << extractor.rodName() << " total = " << rod_iter->second.totalCounter() << std::endl;
	    if (rod_iter->second.status() != last_status ) {
	      if ((rod_iter->second.status()==kStuck || last_status==kStuck)
		  && VarDefList::instance()->isValid(m_scanStatusVar)) {
		propagateStuckState(rod_iter->second.status(), extractor.rodName());
	      }


	      if (m_verbose) {
		std::cout << "INFO [ScanProgressMonitor::updateCounter] Status for "
			  << " ROD " << extractor.rodName() << " changed to " << rod_iter->second.status()
			  << " steps = " << rod_iter->second.totalCounter() << " / " << m_stepsTotalPerRod
			  << " ." << std::endl;
	      }
	      DoNothingExtractor status_var_extractor(s_rodScanProgressStatusName);
	      m_receptor->setValue(kRodValue, kCurrent,0, extractor.rodName() , status_var_extractor, static_cast<State_t>(rod_iter->second.status()) );
	      if (rod_iter->second.status()>= kDone && rod_iter->second.status()<kNStates) {
		m_statusChangeFlag->setFlag();
	      }
	    }

	    if (time > static_cast< ::time_t >(m_lastProgressUpdate + s_progressUpdateTime) ) {
	      m_lastProgressUpdate = time;
	      m_receptor->notify(new ProgressEvent(m_globalCounter, m_stepsTotal));
	    }
	    break;
	  }

	}
      }
    }
  }

  void ScanProgressMonitor::propagateStuckState(EScanProgressStatus current_status, const std::string &rod_name)
  {

    State_t new_value=0;
    bool update=false;
    {
      Lock calib_data_lock(calibrationData()->calibrationDataMutex());
      try {

	const CalibrationData::ConnObjDataList
	  conn_obj_data ( calibrationData()->calibrationData().getConnObjectData( rod_name) );

	if ( conn_obj_data.enableState(kScan, m_monitoredScan).enabled() ) {
	  State_t value = conn_obj_data.value<State_t>(kScan,m_monitoredScan, m_scanStatusVar);

	  if (value == CAN::ScanIsStatus::kAborted+1 && current_status==kScanning) {
	    new_value=CAN::ScanIsStatus::kRunning;
	    update=true;
	  }
	  else if (value == CAN::ScanIsStatus::kRunning && current_status==kStuck) {
	    new_value=CAN::ScanIsStatus::kAborted+1;
	    update=true;
	  }
	}
      }
      catch(CalibrationData::MissingConnObject &) {
      }
    }
    if (update) {
      DoNothingExtractor do_noting_extractor(VarDefList::instance()->varName(m_scanStatusVar));
      m_receptor->setValue(kRodValue, kScan,m_monitoredScan, rod_name, do_noting_extractor, new_value );
    }
  }

}
