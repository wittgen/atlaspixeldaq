#include "CommandKits.h"

namespace PixCon {
  CommandKits *CommandKits::s_instance=NULL;
  const std::string CommandKits::s_empty;

  CommandKits::~CommandKits() {}

  bool ConfigurableCommandSequence::configure() {
    for(std::vector<std::pair<ICommand *, IClassification *> >::iterator command_iter = CommandSequence::begin();
	command_iter != CommandSequence::end();
	++command_iter) {

      if (!configureCommand(command_iter->first)) return false;
    }
    return true;

  }

  bool ConfigurableCommandSequence::configureCommand(ICommand *a_command) {

    IConfigurableCommand *a_configurable_command = dynamic_cast<IConfigurableCommand *>( a_command );
    if (a_configurable_command && !a_configurable_command->hasDefaults()) {
      if (!a_configurable_command->configure()) {
	return false;
      }
    }
    return true;
  }

  bool ConfigurableCommandSequence::hasDefaults() const {
    for(std::vector<std::pair<ICommand *, IClassification *> >::const_iterator command_iter = CommandSequence::begin();
	command_iter != CommandSequence::end();
	++command_iter) {
      IConfigurableCommand *a_configurable_command = dynamic_cast<IConfigurableCommand *>( command_iter->first );
      if (a_configurable_command && !a_configurable_command->hasDefaults()) {
	if (!a_configurable_command->hasDefaults()) {
	  return false;
	}
      }
    }
    return true;
  }


}
