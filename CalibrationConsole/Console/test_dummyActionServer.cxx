#include <ipc/partition.h>
#include <ipc/core.h>
#include <pmg/pmg_initSync.h>

#include <is/infoT.h>
#include <is/inforeceiver.h>
#include <is/infoiterator.h>
#include <is/infodictionary.h>

#include "test_isUtil.h"

#include "test_Broker.h"
//#include "test_DummyMultiAction.h"
#include <ConfigWrapper/ConnectivityManager.h>
#include <ConfigWrapper/Connectivity.h>
#include <ConfigWrapper/ConnectivityUtil.h>
#include <ConfigWrapper/createDbServer.h>
#include <Flag.hh>

#include <string>


bool s_abort;

template <class T>
class CORBA_ptr
{
public:
  CORBA_ptr(T *ptr) : m_ptr(ptr) {}
  ~CORBA_ptr() { if (m_ptr) m_ptr->_destroy(); m_ptr=NULL; }

  T &operator*() { return m_ptr; }
  const T &operator*() const { return m_ptr; }

  T *operator->() { return m_ptr; }
  const T *operator->() const { return m_ptr; }

private:
  T *m_ptr;
};

int main(int argc, char **argv)
{
  //  std::cout << "wait 40s" << std::endl;
  {
    const char *pmg_sync_file = getenv("PMG_SYNC_FILE");
    if (pmg_sync_file) {
      pmg_initSync();
    }
  }
  //sleep(10);
  std::cout << std::endl;

  std::string partition_name;
  std::string is_name = "RunParams";
  std::string oh_name = "OH";
  std::string histo_name_server = "HistoNameServer";
  std::string id_tag="CT";
  std::string tags="CT-C1OSP-V3";
  std::string cfg_tag="CT-C12OSP";
  std::string db_server_partition_name;
  std::string db_server_name;
  std::string broker_extra_name="";
  std::vector<std::string> rod_names;
  bool verbose=false;
  bool error=false;
  bool cleanup=false;

  for (int arg_i=1; arg_i<argc; arg_i++) {
    if (strcmp(argv[arg_i],"-p")==0 && arg_i+1<argc) {
      partition_name = argv[++arg_i];
    }
    else if ((strcmp(argv[arg_i],"-n")==0 || strcmp(argv[arg_i],"--scan-is-name")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      is_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-o")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      oh_name = argv[++arg_i];
    }
    else if ((strcmp(argv[arg_i],"--histo-name-server")==0 || strcmp(argv[arg_i],"-H")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      histo_name_server = argv[++arg_i];
    }
    else if ( (strcmp(argv[arg_i],"-D")==0
	       || strcmp(argv[arg_i],"--db-server-name")==0)
	      && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      db_server_name = argv[++arg_i];
    }
    else if ( strcmp(argv[arg_i],"--db-server-partition")==0
	      && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      db_server_partition_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-i")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      id_tag = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-t")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      tags = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-c")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      cfg_tag = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-b")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      broker_extra_name = "_";
      broker_extra_name += argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"--cleanup")==0 ) {
      cleanup = true;
    }
    else if (strcmp(argv[arg_i],"-v")==0 ) {
      verbose=true;
    }
    else {
      if (argv[arg_i][0]!='-') {
	rod_names.push_back(argv[arg_i]);
      }
      else {
	std::cout << "ERROR ["<< argv[0] << ":main] unhandled argument " << arg_i << " : " << argv[arg_i]<< "." << std::endl;
	error=true;
      }
    }
  }


  if (error || is_name.empty() || partition_name.empty()) {
    std::cout << "usage " << argv[0] << "-p partition [-n/--scan-is-name is-server-name] [-o OH-server-name]"
	      << "\n\t[--histo-name-server/-H] [-i id-tag] [-t tags] [-c cfg-tag] [--cleanup] [-v] [ROD, Crate name, ...]" << std::endl
              << "\t-D/--db-server-name\tSet the name of the db server." << std::endl
              << "\t--db-server-partition\tSet the name of the db server partition in case it is different from manitu's." << std::endl;

    return -1;
  }
  // Start IPCCore
  IPCCore::init(argc, argv);

  IPCServer ipc_server;
  
  IPCPartition partition(partition_name);
  ISInfoDictionary is_dictionary(partition);

  if (cleanup) {
    ISInfoT<std::string> is_value;
    std::vector<std::string> var_names;
    var_names.push_back("/STATUS");
    var_names.push_back("/CURRENT-ACTION");
    for (std::vector<std::string>::const_iterator var_name_iter = var_names.begin();
	 var_name_iter != var_names.end();
	 var_name_iter++) {

      ISCriteria criteria = is_value.type() && std::string(".*ROD_CRATE.*/")+*var_name_iter;
      ISInfoIterator ii( partition, is_name,  criteria);
      while( ii() ) {
	is_dictionary.remove(ii.name());
	//    std::cout << "INFO [IsListener::ctor] " << ii.name()  << " : " << is_value.getValue() << std::endl;
      }
    }
  }

  std::unique_ptr<IPCPartition> db_server_partition;
  if (!db_server_name.empty()) {
    if (!db_server_partition_name.empty()) {
      db_server_partition = std::unique_ptr<IPCPartition>(new IPCPartition(db_server_partition_name.c_str()));
      if (!db_server_partition->isValid()) {
	std::cerr << "ERROR [" << argv[0] << ":main] Not a valid partition " << db_server_partition_name
		  << " for the db server." << std::endl;
	return EXIT_FAILURE;
      }
    }

    std::shared_ptr<PixLib::PixDbServerInterface> db_server( PixA::createDbServer((db_server_partition.get() ?
										       *db_server_partition
										     : partition),
										    db_server_name) );
    if (db_server.get()) {
      PixA::ConnectivityManager::useDbServer( db_server );
    }
    else {
      std::cout << "WARNING [main:" << argv[0] << "] Failed to create the db server." << std::endl;
    }
  }

  PixA::ConnectivityRef conn( PixA::ConnectivityManager::getConnectivity( id_tag,
									  tags,
									  tags,
									  tags,
									  cfg_tag,
									  0 ) );

  std::set<std::string> all_actions;
  if (!rod_names.empty()) {

    // go through rod name list, remove crates and replace by connected rod  names.
    for (std::vector<std::string>::iterator rod_name_iter = rod_names.begin();
	 rod_name_iter != rod_names.end();
	 rod_name_iter++) {
      try {
	PixA::RodList rod_list = conn.rods(*rod_name_iter);

	PixA::RodList::const_iterator rod_begin = rod_list.begin();
	PixA::RodList::const_iterator rod_end = rod_list.end();
	for (PixA::RodList::const_iterator rod_iter = rod_begin;
	     rod_iter != rod_end;
	     ++rod_iter) {
	  all_actions.insert(PixLib::DummyBroker::makeSingleRodActionName(*rod_name_iter, PixA::connectivityName(rod_iter)));
	}

      }
      catch( PixA::ConfigException &) {

	try {
	  PixA::Pp0List pp0_list = conn.pp0s(*rod_name_iter);
	  all_actions.insert(PixLib::DummyBroker::makeSingleRodActionName(PixA::connectivityName(pp0_list.parent()), *rod_name_iter));
	}
	catch( PixA::ConfigException &) {
	}
      }
    }
  }
  else {

    for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
	 crate_iter != conn.crates().end();
	 ++crate_iter) {

      PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
      PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
      for (PixA::RodList::const_iterator rod_iter = rod_begin;
	   rod_iter != rod_end;
	   ++rod_iter) {

	all_actions.insert(PixLib::DummyBroker::makeSingleRodActionName(PixA::connectivityName(crate_iter), PixA::connectivityName(rod_iter)));
	
      }
    }
  }

  CORBA_ptr<PixLib::DummyBroker> broker(new PixLib::DummyBroker(all_actions,
								partition,
								is_name,
								oh_name,
								histo_name_server,
								id_tag,tags,
								cfg_tag,
								broker_extra_name,
								verbose));

  {
    const char *pmg_sync_file = getenv("PMG_SYNC_FILE");
    if (pmg_sync_file) {
      pmg_initSync();
    }
  }
  ipc_server.run();
  //  broker.deallocateActions( an_action );
}

