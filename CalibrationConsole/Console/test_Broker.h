#ifndef _PixLib_DummyBroker_h_
#define _PixLib_DummyBroker_h_

#include <string>
#include <vector>
#include <map>
#include <set>
#include <ConfigWrapper/Connectivity.h>
#include <memory>

#include <PixActions/PixActions.h>
#include <PixBroker/PixBroker.h>

class IPCPartition;

namespace PixLib {

  class DummyMultiAction;

  class DummyBroker : public PixBroker
  {
  public:
    DummyBroker(std::set<std::string> &actionNames,
		IPCPartition &partition,
		const std::string &is_server_name,
		const std::string &oh_server_name,
		const std::string &histo_name_server_name,
		const std::string &id_tag="CT",
		const std::string &tags="CT-C1OSP-V3",
		const std::string &cfg_tag="CT-C12OSP",
		const std::string &broker_name="",
		bool m_verbose=false);

    ~DummyBroker();

    std::set<std::string> listActions(bool checkAvailability = true,
				      PixActions::Type type = PixActions::SINGLE_ROD,
				      std::string allocatingBrokerName="",
				      std::string managingBrokerName="", std::string caller="");

    std::map<std::string,std::vector<std::string> > getConnectivityTags();

    void updateConnectivityFromIS(std::string idTag, std::string connTag, std::string cfgTag, std::string modCfgTag);

    /** Return a dummy multi action.
     * The dummy multi action is owned by the broker.
     */
    //    PixActions* allocateActions(std::set<std::string> actionNames);

    //    void deallocateActions(PixActions* action);

    bool ipc_deallocateAction(const char* actionName, const char* deallocatingBrokerName);

    PixActions* allocateActions(std::set<std::string> actionNames);
    PixActions* allocateActions(std::set<std::string> actionNames, const char* allocatingBrokerName) ;

    void deallocateActions(PixActions* action);
    void deallocateActions(PixActions* action, const char* deallocatingBrokerName);


    static std::string makeSingleRodActionName(const std::string &crate_name, const std::string &rod_name) 
    {
      std::string name("SINGLE_ROD:");
      name += crate_name;
      name += "/";
      name += rod_name;
      return name;
    }


  protected:
    void createActions(const std::set<std::string> &actionNames);

  private:
    IPCPartition                   *m_partition;
    std::string                     m_isServerName;
    std::string                     m_ohServerName;
    std::string			    m_histoNameServerName;


//    std::map< std::string, std::shared_ptr<PixAction> > m_actionList;
    std::set<std::string> m_actionNames;
    std::shared_ptr<DummyMultiAction> m_multiAction;
    std::map<std::string, std::shared_ptr<PixActions> > m_subMultiActionList;

    //    std::vector<std::string> m_connTags;
    PixA::ConnectivityRef    m_conn;
    bool m_verbose;

    static unsigned int s_multiActionCounter;
  };
}
#endif
