#ifndef UI_SCANSELECTIONBASE_H
#define UI_SCANSELECTIONBASE_H

#include <QGroupBox>
#include <QComboBox>
#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QCheckBox>
#include <QDialog>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QSpacerItem>
#include <QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ScanSelectionBase
{
public:
    QVBoxLayout *vboxLayout;
    QGroupBox *m_scanConfigGroup;
    QVBoxLayout *scanConfigLayout;
    QHBoxLayout *fileConfigLayout;
    QCheckBox *m_useDefaultCheckBox;
    QLabel *m_fileNameLabel;
    QLineEdit *m_fileNameEdit;
    QGridLayout *gridLayout;
    QLabel *scanNameLabel;
    QLabel *scanConfigRevSelectorLabel;
    QVBoxLayout *scanNameLayout;
    QVBoxLayout *configRevLayout;
    QHBoxLayout *scanConfigHorizontalLayout;
    QComboBox *m_scanConfigTagSelector;//
    QComboBox *m_scanConfigRevSelector;//
    QVBoxLayout *customiseScanButtonLayout;
    QPushButton *m_customiseScanButton;
    QLabel *customiseScanButtonLabel;
    QVBoxLayout *vboxLayout4;
    QComboBox *m_scanTypeSelector;//
    QSpacerItem *spacer1;
    QSpacerItem *spacer9_3;
    QGroupBox *m_analysisConfigGroup;
    QVBoxLayout *analysisConfigLayout;
    QCheckBox *m_analyseScanCheckBox;
    QGroupBox *m_scanAnalysisConfigGroup;
    QVBoxLayout *scanAnalysisConfigurationLayout;
    QHBoxLayout *scanSerialNumberLayout;
    QLabel *scanSerialNumberLabel;
    QLineEdit *m_scanSerialNumber;
    QGridLayout *gridLayout1;
    QLabel *classificationLabel;
    QLabel *postProcessingLabel;
    QLabel *configAnalysisTagRevisionLabel;
    QLabel *configClassificationTagRevisionLabel;
    QLabel *configPostProcessingTagRevisionLabel;
    QVBoxLayout *vboxLayout7;
    QComboBox *m_analysisConfigTagSelector;//
    QComboBox *m_analysisConfigRevSelector;//
    QVBoxLayout *vboxLayout8;
    QComboBox *m_classificationConfigTagSelector;//
    QComboBox *m_classificationConfigRevSelector;//
    QVBoxLayout *vboxLayout9;
    QComboBox *m_postProcessingConfigTagSelector;//
    QComboBox *m_postProcessingConfigRevSelector;//
    QVBoxLayout *vboxLayout10;
    QComboBox *m_postProcessingTypeSelector;//
    QSpacerItem *spacer9_2_2;
    QVBoxLayout *vboxLayout11;
    QComboBox *m_classificationTypeSelector;//
    QSpacerItem *spacer9_2;
    QVBoxLayout *vboxLayout12;
    QComboBox *m_analysisTypeSelector;//
    QSpacerItem *spacer9;
    QVBoxLayout *vboxLayout13;
    QLabel *customiseAnalysisButtonLabel;
    QLabel *customiseClassificationButtonLabel;
    QLabel *customisePostProcessingButtonLabel;
    QVBoxLayout *vboxLayout14;
    QPushButton *m_customisePostProcessingButton;
    QSpacerItem *spacer12;
    QSpacerItem *spacer11;
    QVBoxLayout *vboxLayout15;
    QPushButton *m_customiseAnalysisButton;
    QPushButton *m_customiseClassificationButton;
    QSpacerItem *spacer10;
    QLabel *analysisLabel;
    QHBoxLayout *cancelOkButtonLayout;
    QSpacerItem *spacer3;
    QPushButton *m_cancelButton;
    QSpacerItem *spacer4;
    QPushButton *m_okayButton;
    QSpacerItem *spacer5;

    void setupUi(QDialog *ScanSelectionBase)
    {
        ////// Overall layout for Run scan menu
        vboxLayout = new QVBoxLayout(ScanSelectionBase);
        vboxLayout->setSpacing(6);

        ////// Run scan menu
        m_scanConfigGroup = new QGroupBox;
        fileConfigLayout = new QHBoxLayout;
        m_useDefaultCheckBox = new QCheckBox(m_scanConfigGroup);
        m_fileNameLabel = new QLabel(m_scanConfigGroup);
        m_fileNameEdit = new QLineEdit(m_scanConfigGroup);

        fileConfigLayout->addWidget(m_useDefaultCheckBox);
        fileConfigLayout->addWidget(m_fileNameLabel);
        fileConfigLayout->addWidget(m_fileNameEdit);

        scanNameLabel = new QLabel(m_scanConfigGroup);
        m_scanTypeSelector = new QComboBox(m_scanConfigGroup);//
        scanNameLayout = new QVBoxLayout;
        scanNameLayout->addWidget(scanNameLabel);
        scanNameLayout->addWidget(m_scanTypeSelector);

        scanConfigRevSelectorLabel = new QLabel(m_scanConfigGroup);
        m_scanConfigRevSelector = new QComboBox(m_scanConfigGroup);//
        m_scanConfigTagSelector = new QComboBox(m_scanConfigGroup);//
        configRevLayout = new QVBoxLayout;
        configRevLayout->addWidget(scanConfigRevSelectorLabel);
        configRevLayout->addWidget(m_scanConfigTagSelector);
        configRevLayout->addWidget(m_scanConfigRevSelector);

        customiseScanButtonLayout = new QVBoxLayout;
        m_customiseScanButton = new QPushButton(m_scanConfigGroup);
        m_customiseScanButton->setEnabled(false);
        const QIcon icon = QIcon(QPixmap(":/icons/images/configure.png"));
        m_customiseScanButton->setIcon(icon);
        customiseScanButtonLabel = new QLabel(m_scanConfigGroup);
        QFont font;
        font.setPointSize(8);
        customiseScanButtonLabel->setFont(font);
        customiseScanButtonLabel->setAlignment(Qt::AlignCenter);
        customiseScanButtonLabel->setWordWrap(false);

        customiseScanButtonLayout->addWidget(m_customiseScanButton);
        customiseScanButtonLayout->addWidget(customiseScanButtonLabel);
        customiseScanButtonLayout->setAlignment(Qt::AlignBottom);

        scanConfigHorizontalLayout = new QHBoxLayout;
        scanConfigHorizontalLayout->addLayout(scanNameLayout);
        scanConfigHorizontalLayout->addLayout(configRevLayout);
        scanConfigHorizontalLayout->addLayout(customiseScanButtonLayout);

        scanConfigLayout = new QVBoxLayout;
        scanConfigLayout->addLayout(fileConfigLayout);
        scanConfigLayout->addLayout(scanConfigHorizontalLayout);
        m_scanConfigGroup->setLayout(scanConfigLayout);

        vboxLayout->addWidget(m_scanConfigGroup);

        m_analysisConfigGroup = new QGroupBox(ScanSelectionBase);
        m_analyseScanCheckBox = new QCheckBox(m_analysisConfigGroup);
        analysisConfigLayout = new QVBoxLayout;
        analysisConfigLayout->addWidget(m_analyseScanCheckBox);
        m_analysisConfigGroup->setLayout(analysisConfigLayout);

        vboxLayout->addWidget(m_analysisConfigGroup);

        ////// Scan Analysis menu
        m_scanAnalysisConfigGroup = new QGroupBox;
        scanSerialNumberLayout = new QHBoxLayout;
        scanSerialNumberLabel = new QLabel(m_scanAnalysisConfigGroup);
        scanSerialNumberLabel->setWordWrap(false);
        m_scanSerialNumber = new QLineEdit(m_scanAnalysisConfigGroup);

        scanSerialNumberLayout->addWidget(scanSerialNumberLabel);
        scanSerialNumberLayout->addWidget(m_scanSerialNumber);

        analysisLabel = new QLabel(m_scanAnalysisConfigGroup);
        analysisLabel->setWordWrap(false);
        m_analysisTypeSelector = new QComboBox(m_scanAnalysisConfigGroup);//
        m_analysisConfigTagSelector = new QComboBox(m_scanAnalysisConfigGroup);//
        m_analysisConfigRevSelector = new QComboBox(m_scanAnalysisConfigGroup);//
        m_customiseAnalysisButton = new QPushButton(m_scanAnalysisConfigGroup);
        m_customiseAnalysisButton->setEnabled(false);
        m_customiseAnalysisButton->setIcon(icon);
        configAnalysisTagRevisionLabel = new QLabel(m_scanAnalysisConfigGroup);
        configAnalysisTagRevisionLabel->setWordWrap(false);
        customiseAnalysisButtonLabel = new QLabel(m_scanAnalysisConfigGroup);
        customiseAnalysisButtonLabel->setFont(font);
        customiseAnalysisButtonLabel->setAlignment(Qt::AlignCenter);
        customiseAnalysisButtonLabel->setWordWrap(false);

        QVBoxLayout *analysisTypeLayout = new QVBoxLayout;
        analysisTypeLayout->addWidget(analysisLabel);
        analysisTypeLayout->addWidget(m_analysisTypeSelector);
        QVBoxLayout *analysisTypeLayout2 =new QVBoxLayout;
        analysisTypeLayout2->addWidget(configAnalysisTagRevisionLabel);
        analysisTypeLayout2->addWidget(m_analysisConfigTagSelector);
        analysisTypeLayout2->addWidget(m_analysisConfigRevSelector);
        QVBoxLayout *analysisTypeLayout3 = new QVBoxLayout;
        analysisTypeLayout3->addWidget(m_customiseAnalysisButton);
        analysisTypeLayout3->addWidget(customiseAnalysisButtonLabel);
        analysisTypeLayout3->setAlignment(Qt::AlignBottom);
        QHBoxLayout *analysisLayout = new QHBoxLayout;
        analysisLayout->addLayout(analysisTypeLayout);
        analysisLayout->addSpacing(10);
        analysisLayout->addLayout(analysisTypeLayout2);
        analysisLayout->addLayout(analysisTypeLayout3);

        classificationLabel = new QLabel(m_scanAnalysisConfigGroup);
        classificationLabel->setWordWrap(false);
        m_classificationTypeSelector = new QComboBox(m_scanAnalysisConfigGroup);//
        m_classificationConfigTagSelector = new QComboBox(m_scanAnalysisConfigGroup);//
        m_classificationConfigRevSelector = new QComboBox(m_scanAnalysisConfigGroup);//
        m_customiseClassificationButton = new QPushButton(m_scanAnalysisConfigGroup);
        m_customiseClassificationButton->setEnabled(false);
        m_customiseClassificationButton->setIcon(icon);
        configClassificationTagRevisionLabel = new QLabel(m_scanAnalysisConfigGroup);
        configClassificationTagRevisionLabel->setWordWrap(false);
        customiseClassificationButtonLabel = new QLabel(m_scanAnalysisConfigGroup);
        customiseClassificationButtonLabel->setFont(font);
        customiseClassificationButtonLabel->setAlignment(Qt::AlignCenter);
        customiseClassificationButtonLabel->setWordWrap(false);

        QVBoxLayout *classificationTypeLayout = new QVBoxLayout;
        classificationTypeLayout->addWidget(classificationLabel);
        classificationTypeLayout->addWidget(m_classificationTypeSelector);
        QVBoxLayout *classificationTypeLayout2 =new QVBoxLayout;
        classificationTypeLayout2->addWidget(configClassificationTagRevisionLabel);
        classificationTypeLayout2->addWidget(m_classificationConfigTagSelector);
        classificationTypeLayout2->addWidget(m_classificationConfigRevSelector);
        QVBoxLayout *classificationTypeLayout3 = new QVBoxLayout;
        classificationTypeLayout3->addWidget(m_customiseClassificationButton);
        classificationTypeLayout3->addWidget(customiseClassificationButtonLabel);
        classificationTypeLayout3->setAlignment(Qt::AlignBottom);
        QHBoxLayout *classificationLayout = new QHBoxLayout;
        classificationLayout->addLayout(classificationTypeLayout);
        classificationLayout->addSpacing(55);
        classificationLayout->addLayout(classificationTypeLayout2);
        classificationLayout->addLayout(classificationTypeLayout3);


        postProcessingLabel = new QLabel(m_scanAnalysisConfigGroup);
        postProcessingLabel->setWordWrap(false);
        m_postProcessingTypeSelector = new QComboBox(m_scanAnalysisConfigGroup);//
        m_postProcessingConfigTagSelector = new QComboBox(m_scanAnalysisConfigGroup);//
        m_postProcessingConfigRevSelector = new QComboBox(m_scanAnalysisConfigGroup);//
        m_customisePostProcessingButton = new QPushButton(m_scanAnalysisConfigGroup);
        m_customisePostProcessingButton->setEnabled(false);
        m_customisePostProcessingButton->setIcon(icon);
        configPostProcessingTagRevisionLabel = new QLabel(m_scanAnalysisConfigGroup);
        configPostProcessingTagRevisionLabel->setWordWrap(false);
        customisePostProcessingButtonLabel = new QLabel(m_scanAnalysisConfigGroup);
        customisePostProcessingButtonLabel->setFont(font);
        customisePostProcessingButtonLabel->setAlignment(Qt::AlignCenter);
        customisePostProcessingButtonLabel->setWordWrap(false);

        QVBoxLayout *postProcessingTypeLayout = new QVBoxLayout;
        postProcessingTypeLayout->addWidget(postProcessingLabel);
        postProcessingTypeLayout->addWidget(m_postProcessingTypeSelector);
        QVBoxLayout *postProcessingTypeLayout2 =new QVBoxLayout;
        postProcessingTypeLayout2->addWidget(configPostProcessingTagRevisionLabel);
        postProcessingTypeLayout2->addWidget(m_postProcessingConfigTagSelector);
        postProcessingTypeLayout2->addWidget(m_postProcessingConfigRevSelector);
        QVBoxLayout *postProcessingTypeLayout3 = new QVBoxLayout;
        postProcessingTypeLayout3->addWidget(m_customisePostProcessingButton);
        postProcessingTypeLayout3->addWidget(customisePostProcessingButtonLabel);
        postProcessingTypeLayout3->setAlignment(Qt::AlignBottom);
        QHBoxLayout *postProcessingLayout = new QHBoxLayout;
        postProcessingLayout->addLayout(postProcessingTypeLayout);
        postProcessingLayout->addSpacing(45);
        postProcessingLayout->addLayout(postProcessingTypeLayout2);
        postProcessingLayout->addLayout(postProcessingTypeLayout3);

     ///// Cancel and Ok buttons
        cancelOkButtonLayout = new QHBoxLayout;
        cancelOkButtonLayout->setSpacing(6);
        spacer3 = new QSpacerItem(21, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);
        cancelOkButtonLayout->addItem(spacer3);
        m_cancelButton = new QPushButton;
        cancelOkButtonLayout->addWidget(m_cancelButton);
        spacer4 = new QSpacerItem(21, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
        cancelOkButtonLayout->addItem(spacer4);
        m_okayButton = new QPushButton;
        m_okayButton->setDefault(true);
        cancelOkButtonLayout->addWidget(m_okayButton);
        spacer5 = new QSpacerItem(31, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
        cancelOkButtonLayout->addItem(spacer5);

        ///// Add it all up
        scanAnalysisConfigurationLayout = new QVBoxLayout;
        scanAnalysisConfigurationLayout->addLayout(scanSerialNumberLayout);
        scanAnalysisConfigurationLayout->addLayout(analysisLayout);
        scanAnalysisConfigurationLayout->addLayout(classificationLayout);
        scanAnalysisConfigurationLayout->addLayout(postProcessingLayout);
        m_scanAnalysisConfigGroup->setLayout(scanAnalysisConfigurationLayout);

        vboxLayout->addWidget(m_scanAnalysisConfigGroup);
        vboxLayout->addLayout(cancelOkButtonLayout);

        retranslateUi(ScanSelectionBase);
        QObject::connect(m_cancelButton, SIGNAL(clicked()), ScanSelectionBase, SLOT(reject()));
        QObject::connect(m_okayButton, SIGNAL(clicked()), ScanSelectionBase, SLOT(accept_aftercheck()));
        QObject::connect(m_customiseScanButton, SIGNAL(clicked()), ScanSelectionBase, SLOT(customiseScan()));
        QObject::connect(m_customiseAnalysisButton, SIGNAL(clicked()), ScanSelectionBase, SLOT(customiseAnalysis()));
        QObject::connect(m_customiseClassificationButton, SIGNAL(clicked()), ScanSelectionBase, SLOT(customiseClassification()));
        QObject::connect(m_customisePostProcessingButton, SIGNAL(clicked()), ScanSelectionBase, SLOT(customisePostProcessing()));
        QObject::connect(m_analyseScanCheckBox, SIGNAL(toggled(bool)), ScanSelectionBase, SLOT(setAnalyseScan(bool)));
        QObject::connect(m_useDefaultCheckBox, SIGNAL(clicked()), ScanSelectionBase, SLOT(enableFileNameEdit()));

        QMetaObject::connectSlotsByName(ScanSelectionBase);
    } // setupUi

    void retranslateUi(QDialog *ScanSelectionBase)
    {
        ScanSelectionBase->setWindowTitle(QApplication::translate("ScanSelectionBase", "ScanSelectionBase", 0));
        m_scanConfigGroup->setTitle(QApplication::translate("ScanSelectionBase", "Scan Configuration", 0));
        m_useDefaultCheckBox->setText(QApplication::translate("ScanSelectionBase", "Use default", 0));
        m_fileNameLabel->setText(QApplication::translate("ScanSelectionBase", "File Name :", 0));
        scanNameLabel->setText(QApplication::translate("ScanSelectionBase", "Scan Name :", 0));
        scanConfigRevSelectorLabel->setText(QApplication::translate("ScanSelectionBase", "Configuration Tag / Revision :", 0));
        m_customiseScanButton->setText(QString());
        customiseScanButtonLabel->setText(QApplication::translate("ScanSelectionBase", "Customise", 0));
        m_analysisConfigGroup->setTitle(QApplication::translate("ScanSelectionBase", "Analysis Configuration", 0));
        m_analyseScanCheckBox->setText(QApplication::translate("ScanSelectionBase", "Analyse", 0));
        scanSerialNumberLabel->setText(QApplication::translate("ScanSelectionBase", "Scan Serial Number :", 0));
        classificationLabel->setText(QApplication::translate("ScanSelectionBase", "Classification", 0));
        postProcessingLabel->setText(QApplication::translate("ScanSelectionBase", "Post-Processing", 0));
        configAnalysisTagRevisionLabel->setText(QApplication::translate("ScanSelectionBase", "Configuration Tag / Revision", 0));
        configClassificationTagRevisionLabel->setText(QApplication::translate("ScanSelectionBase", "Configuration Tag / Revision", 0));
        configPostProcessingTagRevisionLabel->setText(QApplication::translate("ScanSelectionBase", "Configuration Tag / Revision", 0));
        m_customiseClassificationButton->setText(QString());
        customiseAnalysisButtonLabel->setText(QApplication::translate("ScanSelectionBase", "Customise", 0));
        customiseClassificationButtonLabel->setText(QApplication::translate("ScanSelectionBase", "Customise", 0));
        customisePostProcessingButtonLabel->setText(QApplication::translate("ScanSelectionBase", "Customise", 0));
        m_customisePostProcessingButton->setText(QString());
        m_customiseAnalysisButton->setText(QString());
        analysisLabel->setText(QApplication::translate("ScanSelectionBase", "Analysis", 0));
        m_cancelButton->setText(QApplication::translate("ScanSelectionBase", "Cancel", 0));
        m_okayButton->setText(QApplication::translate("ScanSelectionBase", "Okay", 0));
    } // retranslateUi

};

namespace Ui {
    class ScanSelectionBase: public Ui_ScanSelectionBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCANSELECTIONBASE_H
