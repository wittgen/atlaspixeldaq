#include <ICommand.h>
#include <memory>
#include "ActionCommandBase.h"
#include <PixActions/PixActionsMulti.h>


namespace PixCon {

  class ResetVisetCommand : public ActionCommandBase
  {
  public:
    ResetVisetCommand(const std::shared_ptr<ConsoleEngine> &engine)
      : ActionCommandBase(engine)
    {}

    const std::string &name() const { return commandName(); }

    static const std::string &commandName() {
      return s_name;
    }

  protected:

    void setActionSequence(std::vector<std::string> &action_sequence) {
      action_sequence.push_back("resetViset");
    }

    bool callAction(PixLib::PixActionsMulti &action) {
      action.resetViset();
      return true;
    }

  private:

    //bool m_uninitialisedRodsOnly;
    static const std::string s_name;
  };

  const std::string ResetVisetCommand::s_name("Reset Viset");


  class ResetVisetKit : public PixCon::ICommandKit
  {
  protected:
    ResetVisetKit()
    {}

  public:
    PixCon::IConfigurableCommand *createCommand(const std::string &/*command*/, const std::shared_ptr<ConsoleEngine> &engine) const {
      assert(engine.get());
      return new ResetVisetCommand(engine);
    }

    static bool registerKit() {
      CommandKits::registerKit(ResetVisetCommand::commandName(),new ResetVisetKit, PixCon::UserMode::kShifter);
      //      CommandKits::registerKit(ResetVisetCommand::name(true),new ResetVisetKit(true));
      return true;
    }

  private:
    static bool s_registered;
  };

  bool ResetVisetKit::s_registered = ResetVisetKit::registerKit();

  ICommand *resetVisetCommand(const std::shared_ptr<ConsoleEngine> &engine) {
    return new ResetVisetCommand(engine);
  }

}
