#ifndef UI_PIXSCANPANELBASE_H
#define UI_PIXSCANPANELBASE_H

#include <QTableWidget>
#include <QStackedWidget>
#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QCheckBox>
#include <QComboBox>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QHeaderView>
#include <QLabel>
#include <QPushButton>
#include <QScrollArea>
#include <QSpacerItem>
#include <QSpinBox>
#include <QTabWidget>
#include <QVBoxLayout>
#include <QWidget>
#include <QLineEdit>
#include <QListWidget>
#include <QFileDialog>

QT_BEGIN_NAMESPACE

class Ui_PixScanPanelBase
{
public:
    QVBoxLayout *vboxLayout;
    QLabel *m_title;
    QTabWidget *tabwidget;
    QWidget *tab;
    QVBoxLayout *vboxLayout1;
    QGroupBox *GeneralBox;
    QGridLayout *GeneralBoxLayout;
    QLabel *TextLabel1;
    QSpinBox *nEvents;
    QCheckBox *restorConfig;
    QCheckBox *useEmulator;
    QLabel* emulatorOcc;
    QSpinBox *setHitEmu;
    QSpacerItem *SpacerEmu;
    QCheckBox *snglfeMode;
    QCheckBox *useAltModLinks;
    QCheckBox *tuningStartsFromConfigValues;
    QPushButton *m_feMaskAllEnable;
    QPushButton *m_feMaskAllDisable;
    QGroupBox *m_feMaskButtonGroup;
    QLabel *runTypeLabel;
    QComboBox *runType;
    QGroupBox *TriggerBox;
    QGridLayout *TriggerBoxLayout;
    QLabel *textLabel1_2;
    QSpinBox *nAccepts_A0;
    QLabel *textLabel1;
    QCheckBox *overwL1delBox;
    QSpinBox *l1Delay;
    QLabel *textLabel1_5;
    QSpinBox *trgLatency;
    QLabel *textLabel1_5_4;
    QSpinBox *superGrpDelay;
    QCheckBox *selfTrigger;
    QGroupBox *StrobeBox;
    QGridLayout *StrobeBoxLayout;
    QLabel *textLabel1_5_3;
    QSpinBox *strDuration;
    QLabel *textLabel1_5_3_2;
    QCheckBox *fixedSdelBox;
    QCheckBox *fixedSrangeBox;
    QSpinBox *strDelay;
    QLabel *textLabel1_5_3_4;
    QSpinBox *strDelRange;
    QCheckBox *injectionMode;
    QLabel *textLabel2_2;
    QSpinBox *setVcal;
    QCheckBox *fixedVCALBox;
    QCheckBox *capLowHigh;
    QGroupBox *MaskBox;
    QGridLayout *MaskBoxLayout;
    QComboBox *maskStage;
    QLabel *maskStageLabel;
    QComboBox *maskMode;
    QLabel *maskModeLabel;
    QComboBox *staticMode;
    QLabel *TextLabel1_2;
    QSpinBox *nMaskSteps;
    QWidget *tab1;
    QVBoxLayout *vboxLayout11;
    QVBoxLayout *vboxLayout12;
    QHBoxLayout *hboxLayout15;
    QCheckBox *stageAdvBox;
    QSpacerItem *spacer57;
    QCheckBox *maskOnDsp;
    QSpacerItem *spacer58;
    QSpacerItem *spacer113;
    QHBoxLayout *hboxLayout16;
    QLabel *loopSetLabel;
    QSpacerItem *spacer59;
    QSpinBox *m_showLoopIndex;
    QSpacerItem *spacer60;
    QStackedWidget *loopBoxStack;
    QWidget *loop0BoxPage;
    QVBoxLayout *vboxLayout13;
    QGroupBox *Loop0Box;
    QVBoxLayout *vboxLayout14;
    QVBoxLayout *vboxLayout15;
    QHBoxLayout *hboxLayout17;
    QComboBox *loop0Type;
    QSpacerItem *loop0Sp8;
    QCheckBox *loop0Active;
    QSpacerItem *loop0Sp9;
    QCheckBox *loop0DspProc;
    QHBoxLayout *hboxLayout18;
    QVBoxLayout *vboxLayout16;
    QSpacerItem *loop0Sp2;
    QVBoxLayout *vboxLayout17;
    QHBoxLayout *hboxLayout19;
    QLabel *loop0FromLabel;
    QSpinBox *loop0Start;
    QHBoxLayout *hboxLayout20;
    QLabel *loop0ToLabel;
    QSpinBox *loop0Stop;
    QHBoxLayout *hboxLayout21;
    QLabel *loop0StepsLabel;
    QSpinBox *loop0Step;
    QSpacerItem *loop0Sp1;
    QCheckBox *loop0Regular;
    QCheckBox *loop0Free;
    QSpacerItem *loop0Sp3;
    QVBoxLayout *vboxLayout18;
    QLabel *loop0PtlLabel;
    QListWidget *loop0PtsBox;//
    QPushButton *loop0LoadPoints;
    QSpacerItem *loop0Sp4;
    QHBoxLayout *hboxLayout22;
    QLabel *loop0PostActLabel;
    QComboBox *loop0EndAction;
    QComboBox *loopFitMethod;
    QLabel *loopFitMethodLabel;
    QSpacerItem *loop0Sp5;
    QCheckBox *loop0OnDsp;
    QSpacerItem *loop0Sp10;
    QWidget *loop1BoxPage;
    QVBoxLayout *vboxLayout19;
    QGroupBox *Loop1Box;
    QVBoxLayout *vboxLayout20;
    QVBoxLayout *vboxLayout21;
    QHBoxLayout *hboxLayout23;
    QComboBox *loop1Type;
    QSpacerItem *loop1Sp8;
    QCheckBox *loop1Active;
    QSpacerItem *loop1Sp9;
    QCheckBox *loop1DspProc;
    QHBoxLayout *hboxLayout24;
    QVBoxLayout *vboxLayout22;
    QSpacerItem *loop1Sp2;
    QVBoxLayout *vboxLayout23;
    QHBoxLayout *hboxLayout25;
    QLabel *loop1FromLabel;
    QSpinBox *loop1Start;
    QHBoxLayout *hboxLayout26;
    QLabel *loop1ToLabel;
    QSpinBox *loop1Stop;
    QHBoxLayout *hboxLayout27;
    QLabel *loop1StepsLabel;
    QSpinBox *loop1Step;
    QSpacerItem *loop1Sp1;
    QCheckBox *loop1Regular;
    QCheckBox *loop1Free;
    QSpacerItem *loop1Sp3;
    QVBoxLayout *vboxLayout24;
    QLabel *loop1PtlLabel;
    //Q3ListBox *loop1PtsBox;
    QListWidget *loop1PtsBox;
    QPushButton *loop1LoadPoints;
    QSpacerItem *loop1Sp4;
    QHBoxLayout *hboxLayout28;
    QLabel *loop1PostActLabel;
    QComboBox *loop1EndAction;
    QSpacerItem *loop1Sp5;
    QCheckBox *loop1OnDsp;
    QSpacerItem *loop1Sp10;
    QWidget *loop2BoxPage;
    QVBoxLayout *vboxLayout25;
    QGroupBox *Loop2Box;
    QVBoxLayout *vboxLayout26;
    QVBoxLayout *vboxLayout27;
    QHBoxLayout *hboxLayout29;
    QComboBox *loop2Type;
    QSpacerItem *loop2Sp8;
    QCheckBox *loop2Active;
    QSpacerItem *loop2Sp9;
    QCheckBox *loop2DspProc;
    QHBoxLayout *hboxLayout30;
    QVBoxLayout *vboxLayout28;
    QSpacerItem *loop2Sp2;
    QVBoxLayout *vboxLayout29;
    QHBoxLayout *hboxLayout31;
    QLabel *loop2FromLabel;
    QSpinBox *loop2Start;
    QHBoxLayout *hboxLayout32;
    QLabel *loop2ToLabel;
    QSpinBox *loop2Stop;
    QHBoxLayout *hboxLayout33;
    QLabel *loop2StepsLabel;
    QSpinBox *loop2Step;
    QSpacerItem *loop2Sp1;
    QCheckBox *loop2Regular;
    QCheckBox *loop2Free;
    QSpacerItem *loop2Sp3;
    QVBoxLayout *vboxLayout30;
    QLabel *loop2PtlLabel;
    //Q3ListBox *loop2PtsBox;
    QListWidget *loop2PtsBox;//
    QPushButton *loop2LoadPoints;
    QSpacerItem *loop2Sp4;
    QHBoxLayout *hboxLayout34;
    QLabel *loop2PostActLabel;
    QComboBox *loop2EndAction;
    QSpacerItem *loop2Sp5;
    QCheckBox *loop2OnDsp;
    QSpacerItem *loop2Sp10;
    //QHBoxLayout *hboxLayout35;
    QGridLayout *tuningParamsLayout;
    QLabel *textLabel1_8;
    QLabel *totchargelabel;
    QLabel *textLabel3_2;
    QSpinBox *thresvalue;
    QSpinBox *totvalue;
    QSpinBox *totcharge;
    QCheckBox *fastThrPix;
    QCheckBox *useTuningPointsFile;
    QLineEdit *tuningPointsFile;
    QPushButton* tuningPointsSelect;
    //QSpacerItem *spacer62;
    //QSpacerItem *spacer38_3;
    QWidget *TabPage11;
    QVBoxLayout *vboxLayout33;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents_2;
    QHBoxLayout *horizontalLayout;
    QGroupBox *histoFillBox;

    QVBoxLayout* histoSaveMenuVBox;
    QGridLayout* histoSaveMenu;
    QGridLayout* histoSaveMenu_2;

    QLabel *occLabel;
    QLabel *lvl1Label;
    QLabel *totmeanLabel;
    QLabel *totsigLabel;
    QLabel *fdactlabel;
    QLabel *fdactotlabel;
    QLabel *gdactlabel;
    QLabel *gdacthrlabel;
    QLabel *iftlabel;
    QLabel *iftotlabel;
    QLabel *scurvechi2label;
    QLabel *scurvemeanlabel;
    QLabel *scurvesigmalabel;
    QLabel *tdactlabel;
    QLabel *tdacthrlabel;
    QLabel *twlabel;
    QLabel *rd0label;
    QLabel *rd1label;
    QLabel *rd2label;
    QLabel *rdd1label;
    QLabel *rdd2label;
    QLabel *rdrlabel;
    QLabel *fcllabel;
    QLabel *fctlabel;
    QCheckBox *occFill;
    QCheckBox *lvl1Fill;
    QCheckBox *totmeanFill;
    QCheckBox *totsigFill;
    QCheckBox *fdactfill;
    QCheckBox *fdactotfill;
    QCheckBox *gdactfill;
    QCheckBox *gdacthrfill;
    QCheckBox *iftfill;
    QCheckBox *iftotfill;
    QCheckBox *scurvechi2fill;
    QCheckBox *scurvemeanfill;
    QCheckBox *scurvesigmafill;
    QCheckBox *tdactfill;
    QCheckBox *tdacthrfill;
    QCheckBox *twfill;
    QCheckBox *rd0fill;
    QCheckBox *rd1fill;
    QCheckBox *rd2fill;
    QCheckBox *rdd1fill;
    QCheckBox *rdd2fill;
    QCheckBox *rdrfill;
    QCheckBox *fclfill;
    QCheckBox *fctfill;
    QCheckBox *occKeep;
    QCheckBox *lvl1Keep;
    QCheckBox *totmeanKeep;
    QCheckBox *totsigKeep;
    QCheckBox *fdactkeep;
    QCheckBox *fdactotkeep;
    QCheckBox *gdactkeep;
    QCheckBox *gdacthrkeep;
    QCheckBox *iftkeep;
    QCheckBox *iftotkeep;
    QCheckBox *scurvechi2keep;
    QCheckBox *scurvemeankeep;
    QCheckBox *scurvesigmakeep;
    QCheckBox *tdactkeep;
    QCheckBox *tdacthrkeep;
    QCheckBox *twkeep;
    QCheckBox *rd0keep;
    QCheckBox *rd1keep;
    QCheckBox *rd2keep;
    QCheckBox *rdd1keep;
    QCheckBox *rdd2keep;
    QCheckBox *rdrkeep;
    QCheckBox *fclkeep;
    QCheckBox *fctkeep;
    QVBoxLayout *vboxLayout36;
    QSpacerItem *spacer55;
    QVBoxLayout *vboxLayout37;
    QWidget *TabPage;
    QVBoxLayout *vboxLayout40;
    QVBoxLayout *vboxLayout41;
    QVBoxLayout *vboxLayout42;
    QGroupBox *mccBox;
    QVBoxLayout *vboxLayout43;
    QVBoxLayout *vboxLayout44;
    QHBoxLayout *hboxLayout39;
    QLabel *BwidthLabel;
    QComboBox *MCC_Bwidth;
    QCheckBox *MCC_ErrorFlag;
    QCheckBox *MCC_FECheck;
    QCheckBox *MCC_TimeStampComp;
    QGroupBox *feBox;
    QHBoxLayout *hboxLayout40;
    QHBoxLayout *hboxLayout41;
    QVBoxLayout *vboxLayout45;
    QHBoxLayout *hboxLayout42;
    QLabel *PhiClockLabel_2_2_2;
    QComboBox *TOTmode;
    QHBoxLayout *hboxLayout43;
    QLabel *TextLabel1_9;
    QSpinBox *minTOT;
    QHBoxLayout *hboxLayout44;
    QLabel *TextLabel1_9_2_2;
    QSpinBox *dblTOT;
    QHBoxLayout *hboxLayout45;
    QLabel *PhiClockLabel_2_2_2_2;
    QComboBox *TOTLEmode;
    QSpacerItem *spacer46;
    QVBoxLayout *vboxLayout46;
    QSpacerItem *spacer133_2;
    QHBoxLayout *hboxLayout46;
    QLabel *TextLabel2;
    QComboBox *PhiClock;
    QSpacerItem *spacer133;
    QGroupBox *ErrorBox;
    QHBoxLayout *hboxLayout47;
    QHBoxLayout *hboxLayout48;
    QVBoxLayout *vboxLayout47;
    QCheckBox *hardReset;
    QCheckBox *softReset;
    QCheckBox *shortSync;
    QCheckBox *mediumSync;
    QVBoxLayout *vboxLayout48;
    QCheckBox *longSync;
    QCheckBox *sendBCR;
    QCheckBox *sendECR;
    QSpacerItem *spacer38;
    QWidget *TabPage1;
    QVBoxLayout *vboxLayout49;
    QTableWidget *configParameterTable;

    void setupUi(QWidget *PixScanPanelBase)
    {
        if (PixScanPanelBase->objectName().isEmpty())
            PixScanPanelBase->setObjectName(QString::fromUtf8("PixScanPanelBase"));
        //PixScanPanelBase->resize(640, 700);
        //QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        //sizePolicy.setHorizontalStretch(0);
        //sizePolicy.setVerticalStretch(0);
        //sizePolicy.setHeightForWidth(PixScanPanelBase->sizePolicy().hasHeightForWidth());
        //PixScanPanelBase->setSizePolicy(sizePolicy);
        //PixScanPanelBase->setMaximumSize(QSize(32767, 3000));
        vboxLayout = new QVBoxLayout(PixScanPanelBase);
        vboxLayout->setSpacing(4);
        //vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setContentsMargins(2, 2, 2, 2);
        m_title = new QLabel("PixScan Panel", PixScanPanelBase);
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        m_title->setFont(font);
        m_title->setAlignment(Qt::AlignCenter);

        vboxLayout->addWidget(m_title);

        tabwidget = new QTabWidget(PixScanPanelBase);
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        vboxLayout1 = new QVBoxLayout(tab);
        //vboxLayout1->setSpacing(4);
        //vboxLayout1->setContentsMargins(11, 11, 11, 11);

        constexpr int firstCol = 0;
        constexpr int lastCol = 4;
        constexpr int medColSize = 150;

        //+++++++++++++++++++++++++++++++++++++++++++++++++++++
        //This is the box for the "General" options of a scan
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++
        int generalBoxRow = 0;
        GeneralBox = new QGroupBox("General", tab);
        GeneralBoxLayout = new QGridLayout(GeneralBox);

        GeneralBoxLayout->setColumnMinimumWidth(firstCol, 100);
        GeneralBoxLayout->setColumnStretch(1, 100);
        GeneralBoxLayout->setColumnMinimumWidth(2, 100);
        GeneralBoxLayout->setColumnMinimumWidth(3, medColSize);
        GeneralBoxLayout->setColumnMinimumWidth(lastCol, medColSize);

        //Nuber of triggers textbox and spin box
        TextLabel1 = new QLabel("Events per scan point:", GeneralBox);
        nEvents = new QSpinBox(GeneralBox);
        nEvents->setMinimum(1);
        nEvents->setMaximum(9999999);
        nEvents->setValue(1);
        GeneralBoxLayout->addWidget(TextLabel1, generalBoxRow, firstCol);
        GeneralBoxLayout->addWidget(nEvents, generalBoxRow, lastCol);

        //Restore config checkbox and label
        restorConfig = new QCheckBox("restore config after scan", GeneralBox);
        GeneralBoxLayout->addWidget(restorConfig, ++generalBoxRow, firstCol);

        //Use emulator
        useEmulator = new QCheckBox("use BOC emulator", GeneralBox);
        emulatorOcc = new QLabel("hit occupancy:", GeneralBox);
        setHitEmu = new QSpinBox(GeneralBox);
        setHitEmu->setMinimumSize(QSize(100, 0));
        setHitEmu->setMaximum(255);
        setHitEmu->setValue(10);
        setHitEmu->setEnabled(false);
        GeneralBoxLayout->addWidget(useEmulator, ++generalBoxRow, firstCol);
        GeneralBoxLayout->addWidget(emulatorOcc, generalBoxRow, lastCol-1);
        GeneralBoxLayout->addWidget(setHitEmu, generalBoxRow, lastCol);

        //FE by FE, alt mod links, and tuning from cfg values
        snglfeMode = new QCheckBox("run FE by FE", GeneralBox);
        snglfeMode->setEnabled(false);
        useAltModLinks = new QCheckBox("use alternative module link",GeneralBox);
        tuningStartsFromConfigValues = new QCheckBox("tuning starts from cfg values",GeneralBox);
        GeneralBoxLayout->addWidget(snglfeMode, ++generalBoxRow, firstCol);
        GeneralBoxLayout->addWidget(useAltModLinks, ++generalBoxRow, firstCol);
        GeneralBoxLayout->addWidget(tuningStartsFromConfigValues, ++generalBoxRow, firstCol);

        //FE enable/disable buttons and their colour
        m_feMaskAllEnable = new QPushButton("Enable All", GeneralBox);
        QPalette palette;
        QBrush brush(QColor(0, 255, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush);
        palette.setBrush(QPalette::Active, QPalette::Window, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush);
        m_feMaskAllEnable->setPalette(palette);
        QFont font1;
        font1.setPointSize(7);
        m_feMaskAllEnable->setFont(font1);
        m_feMaskAllEnable->setStyleSheet(QString::fromUtf8("background-color:rgb(0, 255, 0)"));
        m_feMaskAllEnable->setAutoDefault(false);
        m_feMaskAllEnable->setFlat(false);

        m_feMaskAllDisable = new QPushButton("Disable All", GeneralBox);
        m_feMaskAllDisable->setFont(font1);
        m_feMaskAllDisable->setFlat(false);

        //We don't increment the row count here
        GeneralBoxLayout->addWidget(m_feMaskAllEnable, generalBoxRow, lastCol-1);
        GeneralBoxLayout->addWidget(m_feMaskAllDisable, generalBoxRow, lastCol);

        m_feMaskButtonGroup = new QGroupBox("",GeneralBox);
        GeneralBoxLayout->addWidget(m_feMaskButtonGroup, ++generalBoxRow, lastCol-2, 2, 3);
        //We're spanning two rows here, i.e. one extra increments
        ++generalBoxRow;

        //Run type and dropdown menu
        runTypeLabel = new QLabel("Run type:", GeneralBox);
        runType = new QComboBox(GeneralBox);
        runType->setMinimumSize(QSize(200, 0));
        GeneralBoxLayout->addWidget(runTypeLabel, ++generalBoxRow, firstCol);
        GeneralBoxLayout->addWidget(runType, generalBoxRow, lastCol-1, 1, 2);

        vboxLayout1->addWidget(GeneralBox);

        //+++++++++++++++++++++++++++++++++++++++++++++++++++++
        //This is the box for the "Trigger" options of a scan
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++
        int triggerBoxRow = 0;
        TriggerBox = new QGroupBox("Trigger", tab);
        TriggerBoxLayout = new QGridLayout(TriggerBox);

        TriggerBoxLayout->setColumnMinimumWidth(firstCol, 100);
        TriggerBoxLayout->setColumnStretch(1, 100);
        TriggerBoxLayout->setColumnMinimumWidth(2, 100);
        TriggerBoxLayout->setColumnMinimumWidth(3, medColSize);
        TriggerBoxLayout->setColumnMinimumWidth(lastCol, medColSize);

        //Number of Lvl1s
        textLabel1_2 = new QLabel("Consecutive Lvl1:", TriggerBox);
        nAccepts_A0 = new QSpinBox(TriggerBox);
        nAccepts_A0->setMaximum(16);
        nAccepts_A0->setValue(1);
        TriggerBoxLayout->addWidget(textLabel1_2, triggerBoxRow, firstCol);
        TriggerBoxLayout->addWidget(nAccepts_A0, triggerBoxRow, lastCol);

        //Lvl1 delay settings
        textLabel1 = new QLabel("Lvl1 delay:", TriggerBox);
        overwL1delBox = new QCheckBox("overwrite setting", TriggerBox);
        l1Delay = new QSpinBox(TriggerBox);
        l1Delay->setMaximum(10000);
        l1Delay->setValue(1);
        TriggerBoxLayout->addWidget(textLabel1, ++triggerBoxRow, firstCol);
        TriggerBoxLayout->addWidget(overwL1delBox, triggerBoxRow, lastCol-1);
        TriggerBoxLayout->addWidget(l1Delay, triggerBoxRow, lastCol);

        //Latency
        textLabel1_5 = new QLabel("Latency:", TriggerBox);
        trgLatency = new QSpinBox(TriggerBox);
        trgLatency->setMaximum(10000);
        trgLatency->setValue(1);
        TriggerBoxLayout->addWidget(textLabel1_5, ++triggerBoxRow, firstCol);
        TriggerBoxLayout->addWidget(trgLatency, triggerBoxRow, lastCol);

        //Noise scan checkbox and sleep
        selfTrigger = new QCheckBox("is a noise scan", TriggerBox);
        TriggerBoxLayout->addWidget(selfTrigger, ++triggerBoxRow, firstCol);

        textLabel1_5_4 = new QLabel("noise scan sleep duration:", TriggerBox);
        superGrpDelay = new QSpinBox(TriggerBox);
        superGrpDelay->setMinimum(-1);
        superGrpDelay->setMaximum(10000);
        superGrpDelay->setValue(1);
        superGrpDelay->setEnabled(false);
        TriggerBoxLayout->addWidget(textLabel1_5_4, triggerBoxRow, lastCol-1);
        TriggerBoxLayout->addWidget(superGrpDelay, triggerBoxRow, lastCol);

        vboxLayout1->addWidget(TriggerBox);

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //This is the box for the "Strobe/Injections" options of a scan
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        int strobeBoxRow = 0;
        StrobeBox = new QGroupBox("Strobe/Injection", tab);
        StrobeBoxLayout = new QGridLayout(StrobeBox);

        StrobeBoxLayout->setColumnMinimumWidth(firstCol, 100);
        StrobeBoxLayout->setColumnStretch(1, 100);
        StrobeBoxLayout->setColumnMinimumWidth(2, 100);
        StrobeBoxLayout->setColumnMinimumWidth(3, medColSize);
        StrobeBoxLayout->setColumnMinimumWidth(lastCol, medColSize);

        //Duration
        textLabel1_5_3 = new QLabel("Duration:", StrobeBox);
        strDuration = new QSpinBox(StrobeBox);
        strDuration->setMaximum(10000);
        strDuration->setValue(1);
        StrobeBoxLayout->addWidget(textLabel1_5_3, strobeBoxRow, firstCol);
        StrobeBoxLayout->addWidget(strDuration, strobeBoxRow, lastCol);

        //Delay
        textLabel1_5_3_2 = new QLabel("Delay:", StrobeBox);
        fixedSdelBox = new QCheckBox("overwrite delay", StrobeBox);
        strDelay = new QSpinBox(StrobeBox);
        strDelay->setMaximum(63);
        strDelay->setValue(0);
        strDelay->setEnabled(false);
        StrobeBoxLayout->addWidget(textLabel1_5_3_2, ++strobeBoxRow, firstCol);
        StrobeBoxLayout->addWidget(fixedSdelBox, strobeBoxRow, lastCol-1);
        StrobeBoxLayout->addWidget(strDelay, strobeBoxRow, lastCol);

        //Range
        textLabel1_5_3_4 = new QLabel("Delay range:",StrobeBox);
        fixedSrangeBox = new QCheckBox("overwrite range", StrobeBox);
        strDelRange = new QSpinBox(StrobeBox);
        strDelRange->setMaximum(255);
        strDelRange->setValue(0);
        strDelRange->setEnabled(false);
        StrobeBoxLayout->addWidget(textLabel1_5_3_4, ++strobeBoxRow, firstCol);
        StrobeBoxLayout->addWidget(fixedSrangeBox, strobeBoxRow, lastCol-1);
        StrobeBoxLayout->addWidget(strDelRange, strobeBoxRow, lastCol);

        //VCal
        textLabel2_2 = new QLabel("VCAL:", StrobeBox);
        setVcal = new QSpinBox(StrobeBox);
        setVcal->setMaximum(10000);
        setVcal->setValue(1);
        fixedVCALBox = new QCheckBox("leave unchanged", StrobeBox);
        StrobeBoxLayout->addWidget(textLabel2_2, ++strobeBoxRow, firstCol);
        StrobeBoxLayout->addWidget(fixedVCALBox, strobeBoxRow, lastCol-1);
        StrobeBoxLayout->addWidget(setVcal, strobeBoxRow, lastCol);

        //Injection mode and Chigh
        injectionMode = new QCheckBox("digital injection (off=analogue)", StrobeBox);
        capLowHigh = new QCheckBox("use large injection capacitance", StrobeBox);
        StrobeBoxLayout->addWidget(injectionMode, ++strobeBoxRow, firstCol);
        StrobeBoxLayout->addWidget(capLowHigh, strobeBoxRow, lastCol-1, 1, 2);

        vboxLayout1->addWidget(StrobeBox);

        //++++++++++++++++++++++++++++++++++++++++++++++++
        //This is the box for the "Mask" options of a scan
        //++++++++++++++++++++++++++++++++++++++++++++++++
        int maskBoxRow = 0;
        MaskBox = new QGroupBox("Mask", tab);
        MaskBoxLayout = new QGridLayout(MaskBox);

        MaskBoxLayout->setColumnMinimumWidth(firstCol, 100);
        MaskBoxLayout->setColumnStretch(1, 100);
        MaskBoxLayout->setColumnMinimumWidth(2, 100);
        MaskBoxLayout->setColumnMinimumWidth(3, medColSize);
        MaskBoxLayout->setColumnMinimumWidth(lastCol, medColSize);

        maskStageLabel = new QLabel("Mask step:", MaskBox);
        maskStage = new QComboBox(MaskBox);
        MaskBoxLayout->addWidget(maskStageLabel, maskBoxRow, firstCol);
        MaskBoxLayout->addWidget(maskStage, maskBoxRow, lastCol-1, 1, 2);

        maskModeLabel = new QLabel("Step mode:", MaskBox);
        maskMode = new QComboBox(MaskBox);
        maskMode->setEnabled(true);
        MaskBoxLayout->addWidget(maskModeLabel, ++maskBoxRow, firstCol);
        MaskBoxLayout->addWidget(maskMode, maskBoxRow, lastCol-1, 1, 2);

        staticMode = new QComboBox(MaskBox);
        staticMode->setEnabled(true);
        MaskBoxLayout->addWidget(staticMode, ++maskBoxRow, lastCol);

        TextLabel1_2 = new QLabel("Number of mask steps executed:", MaskBox);
        nMaskSteps = new QSpinBox(MaskBox);
        nMaskSteps->setMinimum(0);
        nMaskSteps->setMaximum(2880);
        nMaskSteps->setValue(1);
        MaskBoxLayout->addWidget(TextLabel1_2, ++maskBoxRow, firstCol);
        MaskBoxLayout->addWidget(nMaskSteps, maskBoxRow, lastCol);

        vboxLayout1->addWidget(MaskBox);


        //Next Tab *********************************
        tabwidget->addTab(tab, QString());
        tab1 = new QWidget();
        tab1->setObjectName(QString::fromUtf8("tab1"));
        vboxLayout11 = new QVBoxLayout(tab1);
        vboxLayout11->setSpacing(4);
        vboxLayout11->setContentsMargins(11, 11, 11, 11);
        //vboxLayout11->setObjectName(QString::fromUtf8("vboxLayout11"));
        vboxLayout12 = new QVBoxLayout();
        vboxLayout12->setSpacing(4);
        vboxLayout12->setObjectName(QString::fromUtf8("vboxLayout12"));
        hboxLayout15 = new QHBoxLayout();
        hboxLayout15->setSpacing(4);
        hboxLayout15->setObjectName(QString::fromUtf8("hboxLayout15"));
        stageAdvBox = new QCheckBox(tab1);
        stageAdvBox->setObjectName(QString::fromUtf8("stageAdvBox"));

        hboxLayout15->addWidget(stageAdvBox);

        spacer57 = new QSpacerItem(51, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout15->addItem(spacer57);

        maskOnDsp = new QCheckBox(tab1);
        maskOnDsp->setObjectName(QString::fromUtf8("maskOnDsp"));

        hboxLayout15->addWidget(maskOnDsp);

        spacer58 = new QSpacerItem(41, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout15->addItem(spacer58);


        vboxLayout12->addLayout(hboxLayout15);

        spacer113 = new QSpacerItem(20, 25, QSizePolicy::Minimum, QSizePolicy::Fixed);

        vboxLayout12->addItem(spacer113);

        hboxLayout16 = new QHBoxLayout();
        hboxLayout16->setSpacing(4);
        hboxLayout16->setObjectName(QString::fromUtf8("hboxLayout16"));
        loopSetLabel = new QLabel(tab1);
        loopSetLabel->setObjectName(QString::fromUtf8("loopSetLabel"));
        loopSetLabel->setWordWrap(false);

        hboxLayout16->addWidget(loopSetLabel);

        spacer59 = new QSpacerItem(31, 21, QSizePolicy::Fixed, QSizePolicy::Minimum);

        hboxLayout16->addItem(spacer59);

        m_showLoopIndex = new QSpinBox(tab1);
        m_showLoopIndex->setObjectName(QString::fromUtf8("m_showLoopIndex"));
        m_showLoopIndex->setMaximum(2);

        hboxLayout16->addWidget(m_showLoopIndex);

        spacer60 = new QSpacerItem(291, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout16->addItem(spacer60);


        vboxLayout12->addLayout(hboxLayout16);


        vboxLayout11->addLayout(vboxLayout12);

        loopBoxStack = new QStackedWidget(tab1);
        loopBoxStack->setObjectName(QString::fromUtf8("loopBoxStack"));
        loop0BoxPage = new QWidget(loopBoxStack);
        loop0BoxPage->setObjectName(QString::fromUtf8("loop0BoxPage"));
        loop0BoxPage->setGeometry(QRect(0, 0, 597, 230));
        vboxLayout13 = new QVBoxLayout(loop0BoxPage);
        vboxLayout13->setSpacing(0);
        vboxLayout13->setContentsMargins(0, 0, 0, 0);
        vboxLayout13->setObjectName(QString::fromUtf8("vboxLayout13"));
        Loop0Box = new QGroupBox(loop0BoxPage);
        Loop0Box->setObjectName(QString::fromUtf8("Loop0Box"));
        vboxLayout14 = new QVBoxLayout(Loop0Box);
        vboxLayout14->setSpacing(4);
        vboxLayout14->setContentsMargins(11, 11, 11, 11);
        vboxLayout14->setObjectName(QString::fromUtf8("vboxLayout14"));
        vboxLayout15 = new QVBoxLayout();
        vboxLayout15->setSpacing(4);
        vboxLayout15->setObjectName(QString::fromUtf8("vboxLayout15"));
        hboxLayout17 = new QHBoxLayout();
        hboxLayout17->setSpacing(4);
        hboxLayout17->setObjectName(QString::fromUtf8("hboxLayout17"));
        loop0Type = new QComboBox(Loop0Box);
        loop0Type->setObjectName(QString::fromUtf8("loop0Type"));
        loop0Type->setMinimumSize(QSize(200, 0));

        hboxLayout17->addWidget(loop0Type);

        loop0Sp8 = new QSpacerItem(50, 21, QSizePolicy::Maximum, QSizePolicy::Minimum);

        hboxLayout17->addItem(loop0Sp8);

        loop0Active = new QCheckBox(Loop0Box);
        loop0Active->setObjectName(QString::fromUtf8("loop0Active"));

        hboxLayout17->addWidget(loop0Active);

        loop0Sp9 = new QSpacerItem(61, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout17->addItem(loop0Sp9);

        loop0DspProc = new QCheckBox(Loop0Box);
        loop0DspProc->setObjectName(QString::fromUtf8("loop0DspProc"));

        hboxLayout17->addWidget(loop0DspProc);


        vboxLayout15->addLayout(hboxLayout17);

        hboxLayout18 = new QHBoxLayout();
        hboxLayout18->setSpacing(4);
        hboxLayout18->setObjectName(QString::fromUtf8("hboxLayout18"));
        vboxLayout16 = new QVBoxLayout();
        vboxLayout16->setSpacing(4);
        vboxLayout16->setObjectName(QString::fromUtf8("vboxLayout16"));
        loop0Sp2 = new QSpacerItem(20, 17, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout16->addItem(loop0Sp2);

        vboxLayout17 = new QVBoxLayout();
        vboxLayout17->setSpacing(4);
        vboxLayout17->setObjectName(QString::fromUtf8("vboxLayout17"));
        hboxLayout19 = new QHBoxLayout();
        hboxLayout19->setSpacing(4);
        hboxLayout19->setObjectName(QString::fromUtf8("hboxLayout19"));
        loop0FromLabel = new QLabel(Loop0Box);
        loop0FromLabel->setObjectName(QString::fromUtf8("loop0FromLabel"));
        loop0FromLabel->setWordWrap(false);

        hboxLayout19->addWidget(loop0FromLabel);

        loop0Start = new QSpinBox(Loop0Box);
        loop0Start->setObjectName(QString::fromUtf8("loop0Start"));
        loop0Start->setEnabled(true);
        QPalette palette1;
        QBrush brush1(QColor(0, 0, 0, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::WindowText, brush1);
        QBrush brush2(QColor(228, 228, 228, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Button, brush2);
        QBrush brush3(QColor(255, 255, 255, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Light, brush3);
        QBrush brush4(QColor(241, 241, 241, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Midlight, brush4);
        QBrush brush5(QColor(114, 114, 114, 255));
        brush5.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Dark, brush5);
        QBrush brush6(QColor(152, 152, 152, 255));
        brush6.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Mid, brush6);
        palette1.setBrush(QPalette::Active, QPalette::Text, brush1);
        palette1.setBrush(QPalette::Active, QPalette::BrightText, brush3);
        palette1.setBrush(QPalette::Active, QPalette::ButtonText, brush1);
        palette1.setBrush(QPalette::Active, QPalette::Base, brush3);
        QBrush brush7(QColor(235, 235, 235, 255));
        brush7.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Window, brush7);
        palette1.setBrush(QPalette::Active, QPalette::Shadow, brush1);
        QBrush brush8(QColor(0, 0, 128, 255));
        brush8.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Highlight, brush8);
        palette1.setBrush(QPalette::Active, QPalette::HighlightedText, brush3);
        palette1.setBrush(QPalette::Active, QPalette::Link, brush1);
        palette1.setBrush(QPalette::Active, QPalette::LinkVisited, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::WindowText, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Button, brush2);
        palette1.setBrush(QPalette::Inactive, QPalette::Light, brush3);
        palette1.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette1.setBrush(QPalette::Inactive, QPalette::Dark, brush5);
        palette1.setBrush(QPalette::Inactive, QPalette::Mid, brush6);
        palette1.setBrush(QPalette::Inactive, QPalette::Text, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::BrightText, brush3);
        palette1.setBrush(QPalette::Inactive, QPalette::ButtonText, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush3);
        palette1.setBrush(QPalette::Inactive, QPalette::Window, brush7);
        palette1.setBrush(QPalette::Inactive, QPalette::Shadow, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Highlight, brush8);
        palette1.setBrush(QPalette::Inactive, QPalette::HighlightedText, brush3);
        QBrush brush9(QColor(0, 0, 255, 255));
        brush9.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Inactive, QPalette::Link, brush9);
        QBrush brush10(QColor(255, 0, 255, 255));
        brush10.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Inactive, QPalette::LinkVisited, brush10);
        QBrush brush11(QColor(128, 128, 128, 255));
        brush11.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Disabled, QPalette::WindowText, brush11);
        palette1.setBrush(QPalette::Disabled, QPalette::Button, brush2);
        palette1.setBrush(QPalette::Disabled, QPalette::Light, brush3);
        palette1.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette1.setBrush(QPalette::Disabled, QPalette::Dark, brush5);
        palette1.setBrush(QPalette::Disabled, QPalette::Mid, brush6);
        palette1.setBrush(QPalette::Disabled, QPalette::Text, brush11);
        palette1.setBrush(QPalette::Disabled, QPalette::BrightText, brush3);
        palette1.setBrush(QPalette::Disabled, QPalette::ButtonText, brush11);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush3);
        palette1.setBrush(QPalette::Disabled, QPalette::Window, brush7);
        palette1.setBrush(QPalette::Disabled, QPalette::Shadow, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Highlight, brush8);
        palette1.setBrush(QPalette::Disabled, QPalette::HighlightedText, brush3);
        palette1.setBrush(QPalette::Disabled, QPalette::Link, brush9);
        palette1.setBrush(QPalette::Disabled, QPalette::LinkVisited, brush10);
        loop0Start->setPalette(palette1);
        loop0Start->setMinimum(-99999999);
        loop0Start->setMaximum(99999999);

        hboxLayout19->addWidget(loop0Start);


        vboxLayout17->addLayout(hboxLayout19);

        hboxLayout20 = new QHBoxLayout();
        hboxLayout20->setSpacing(4);
        hboxLayout20->setObjectName(QString::fromUtf8("hboxLayout20"));
        loop0ToLabel = new QLabel(Loop0Box);
        loop0ToLabel->setObjectName(QString::fromUtf8("loop0ToLabel"));
        loop0ToLabel->setWordWrap(false);

        hboxLayout20->addWidget(loop0ToLabel);

        loop0Stop = new QSpinBox(Loop0Box);
        loop0Stop->setObjectName(QString::fromUtf8("loop0Stop"));
        loop0Stop->setEnabled(true);
        QPalette palette2;
        palette2.setBrush(QPalette::Active, QPalette::WindowText, brush1);
        palette2.setBrush(QPalette::Active, QPalette::Button, brush2);
        palette2.setBrush(QPalette::Active, QPalette::Light, brush3);
        palette2.setBrush(QPalette::Active, QPalette::Midlight, brush4);
        palette2.setBrush(QPalette::Active, QPalette::Dark, brush5);
        palette2.setBrush(QPalette::Active, QPalette::Mid, brush6);
        palette2.setBrush(QPalette::Active, QPalette::Text, brush1);
        palette2.setBrush(QPalette::Active, QPalette::BrightText, brush3);
        palette2.setBrush(QPalette::Active, QPalette::ButtonText, brush1);
        palette2.setBrush(QPalette::Active, QPalette::Base, brush3);
        palette2.setBrush(QPalette::Active, QPalette::Window, brush7);
        palette2.setBrush(QPalette::Active, QPalette::Shadow, brush1);
        palette2.setBrush(QPalette::Active, QPalette::Highlight, brush8);
        palette2.setBrush(QPalette::Active, QPalette::HighlightedText, brush3);
        palette2.setBrush(QPalette::Active, QPalette::Link, brush1);
        palette2.setBrush(QPalette::Active, QPalette::LinkVisited, brush1);
        palette2.setBrush(QPalette::Inactive, QPalette::WindowText, brush1);
        palette2.setBrush(QPalette::Inactive, QPalette::Button, brush2);
        palette2.setBrush(QPalette::Inactive, QPalette::Light, brush3);
        palette2.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette2.setBrush(QPalette::Inactive, QPalette::Dark, brush5);
        palette2.setBrush(QPalette::Inactive, QPalette::Mid, brush6);
        palette2.setBrush(QPalette::Inactive, QPalette::Text, brush1);
        palette2.setBrush(QPalette::Inactive, QPalette::BrightText, brush3);
        palette2.setBrush(QPalette::Inactive, QPalette::ButtonText, brush1);
        palette2.setBrush(QPalette::Inactive, QPalette::Base, brush3);
        palette2.setBrush(QPalette::Inactive, QPalette::Window, brush7);
        palette2.setBrush(QPalette::Inactive, QPalette::Shadow, brush1);
        palette2.setBrush(QPalette::Inactive, QPalette::Highlight, brush8);
        palette2.setBrush(QPalette::Inactive, QPalette::HighlightedText, brush3);
        palette2.setBrush(QPalette::Inactive, QPalette::Link, brush9);
        palette2.setBrush(QPalette::Inactive, QPalette::LinkVisited, brush10);
        palette2.setBrush(QPalette::Disabled, QPalette::WindowText, brush11);
        palette2.setBrush(QPalette::Disabled, QPalette::Button, brush2);
        palette2.setBrush(QPalette::Disabled, QPalette::Light, brush3);
        palette2.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette2.setBrush(QPalette::Disabled, QPalette::Dark, brush5);
        palette2.setBrush(QPalette::Disabled, QPalette::Mid, brush6);
        palette2.setBrush(QPalette::Disabled, QPalette::Text, brush11);
        palette2.setBrush(QPalette::Disabled, QPalette::BrightText, brush3);
        palette2.setBrush(QPalette::Disabled, QPalette::ButtonText, brush11);
        palette2.setBrush(QPalette::Disabled, QPalette::Base, brush3);
        palette2.setBrush(QPalette::Disabled, QPalette::Window, brush7);
        palette2.setBrush(QPalette::Disabled, QPalette::Shadow, brush1);
        palette2.setBrush(QPalette::Disabled, QPalette::Highlight, brush8);
        palette2.setBrush(QPalette::Disabled, QPalette::HighlightedText, brush3);
        palette2.setBrush(QPalette::Disabled, QPalette::Link, brush9);
        palette2.setBrush(QPalette::Disabled, QPalette::LinkVisited, brush10);
        loop0Stop->setPalette(palette2);
        loop0Stop->setMinimum(-99999999);
        loop0Stop->setMaximum(99999999);

        hboxLayout20->addWidget(loop0Stop);


        vboxLayout17->addLayout(hboxLayout20);

        hboxLayout21 = new QHBoxLayout();
        hboxLayout21->setSpacing(4);
        hboxLayout21->setObjectName(QString::fromUtf8("hboxLayout21"));
        loop0StepsLabel = new QLabel(Loop0Box);
        loop0StepsLabel->setObjectName(QString::fromUtf8("loop0StepsLabel"));
        loop0StepsLabel->setWordWrap(false);

        hboxLayout21->addWidget(loop0StepsLabel);

        loop0Step = new QSpinBox(Loop0Box);
        loop0Step->setObjectName(QString::fromUtf8("loop0Step"));
        loop0Step->setEnabled(true);
        QPalette palette3;
        palette3.setBrush(QPalette::Active, QPalette::WindowText, brush1);
        palette3.setBrush(QPalette::Active, QPalette::Button, brush2);
        palette3.setBrush(QPalette::Active, QPalette::Light, brush3);
        palette3.setBrush(QPalette::Active, QPalette::Midlight, brush4);
        palette3.setBrush(QPalette::Active, QPalette::Dark, brush5);
        palette3.setBrush(QPalette::Active, QPalette::Mid, brush6);
        palette3.setBrush(QPalette::Active, QPalette::Text, brush1);
        palette3.setBrush(QPalette::Active, QPalette::BrightText, brush3);
        palette3.setBrush(QPalette::Active, QPalette::ButtonText, brush1);
        palette3.setBrush(QPalette::Active, QPalette::Base, brush3);
        palette3.setBrush(QPalette::Active, QPalette::Window, brush7);
        palette3.setBrush(QPalette::Active, QPalette::Shadow, brush1);
        palette3.setBrush(QPalette::Active, QPalette::Highlight, brush8);
        palette3.setBrush(QPalette::Active, QPalette::HighlightedText, brush3);
        palette3.setBrush(QPalette::Active, QPalette::Link, brush1);
        palette3.setBrush(QPalette::Active, QPalette::LinkVisited, brush1);
        palette3.setBrush(QPalette::Inactive, QPalette::WindowText, brush1);
        palette3.setBrush(QPalette::Inactive, QPalette::Button, brush2);
        palette3.setBrush(QPalette::Inactive, QPalette::Light, brush3);
        palette3.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette3.setBrush(QPalette::Inactive, QPalette::Dark, brush5);
        palette3.setBrush(QPalette::Inactive, QPalette::Mid, brush6);
        palette3.setBrush(QPalette::Inactive, QPalette::Text, brush1);
        palette3.setBrush(QPalette::Inactive, QPalette::BrightText, brush3);
        palette3.setBrush(QPalette::Inactive, QPalette::ButtonText, brush1);
        palette3.setBrush(QPalette::Inactive, QPalette::Base, brush3);
        palette3.setBrush(QPalette::Inactive, QPalette::Window, brush7);
        palette3.setBrush(QPalette::Inactive, QPalette::Shadow, brush1);
        palette3.setBrush(QPalette::Inactive, QPalette::Highlight, brush8);
        palette3.setBrush(QPalette::Inactive, QPalette::HighlightedText, brush3);
        palette3.setBrush(QPalette::Inactive, QPalette::Link, brush9);
        palette3.setBrush(QPalette::Inactive, QPalette::LinkVisited, brush10);
        palette3.setBrush(QPalette::Disabled, QPalette::WindowText, brush11);
        palette3.setBrush(QPalette::Disabled, QPalette::Button, brush2);
        palette3.setBrush(QPalette::Disabled, QPalette::Light, brush3);
        palette3.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette3.setBrush(QPalette::Disabled, QPalette::Dark, brush5);
        palette3.setBrush(QPalette::Disabled, QPalette::Mid, brush6);
        palette3.setBrush(QPalette::Disabled, QPalette::Text, brush11);
        palette3.setBrush(QPalette::Disabled, QPalette::BrightText, brush3);
        palette3.setBrush(QPalette::Disabled, QPalette::ButtonText, brush11);
        palette3.setBrush(QPalette::Disabled, QPalette::Base, brush3);
        palette3.setBrush(QPalette::Disabled, QPalette::Window, brush7);
        palette3.setBrush(QPalette::Disabled, QPalette::Shadow, brush1);
        palette3.setBrush(QPalette::Disabled, QPalette::Highlight, brush8);
        palette3.setBrush(QPalette::Disabled, QPalette::HighlightedText, brush3);
        palette3.setBrush(QPalette::Disabled, QPalette::Link, brush9);
        palette3.setBrush(QPalette::Disabled, QPalette::LinkVisited, brush10);
        loop0Step->setPalette(palette3);
        loop0Step->setMaximum(100000);

        hboxLayout21->addWidget(loop0Step);


        vboxLayout17->addLayout(hboxLayout21);


        vboxLayout16->addLayout(vboxLayout17);

        loop0Sp1 = new QSpacerItem(20, 17, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout16->addItem(loop0Sp1);

        loop0Regular = new QCheckBox(Loop0Box);
        loop0Regular->setObjectName(QString::fromUtf8("loop0Regular"));
        loop0Regular->setChecked(true);

        vboxLayout16->addWidget(loop0Regular);

        loop0Free = new QCheckBox(Loop0Box);
        loop0Free->setObjectName(QString::fromUtf8("loop0Free"));

        vboxLayout16->addWidget(loop0Free);


        hboxLayout18->addLayout(vboxLayout16);

        loop0Sp3 = new QSpacerItem(130, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout18->addItem(loop0Sp3);

        vboxLayout18 = new QVBoxLayout();
        vboxLayout18->setSpacing(4);
        vboxLayout18->setObjectName(QString::fromUtf8("vboxLayout18"));
        loop0PtlLabel = new QLabel(Loop0Box);
        loop0PtlLabel->setObjectName(QString::fromUtf8("loop0PtlLabel"));
        loop0PtlLabel->setWordWrap(false);

        vboxLayout18->addWidget(loop0PtlLabel);

        loop0PtsBox = new QListWidget(Loop0Box);//
        loop0PtsBox->setObjectName(QString::fromUtf8("loop0PtsBox"));

        vboxLayout18->addWidget(loop0PtsBox);

        loop0LoadPoints = new QPushButton(Loop0Box);
        loop0LoadPoints->setObjectName(QString::fromUtf8("loop0LoadPoints"));
        loop0LoadPoints->setEnabled(false);

        vboxLayout18->addWidget(loop0LoadPoints);


        hboxLayout18->addLayout(vboxLayout18);

        loop0Sp4 = new QSpacerItem(130, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout18->addItem(loop0Sp4);


        vboxLayout15->addLayout(hboxLayout18);

        hboxLayout22 = new QHBoxLayout();
        hboxLayout22->setSpacing(4);
        hboxLayout22->setObjectName(QString::fromUtf8("hboxLayout22"));
        loop0PostActLabel = new QLabel(Loop0Box);
        loop0PostActLabel->setObjectName(QString::fromUtf8("loop0PostActLabel"));
        loop0PostActLabel->setWordWrap(false);

        hboxLayout22->addWidget(loop0PostActLabel);

        loop0EndAction = new QComboBox(Loop0Box);
        loop0EndAction->setObjectName(QString::fromUtf8("loop0EndAction"));
        loop0EndAction->setMinimumSize(QSize(200, 0));

        hboxLayout22->addWidget(loop0EndAction);

        loop0Sp5 = new QSpacerItem(16, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        hboxLayout22->addItem(loop0Sp5);

        loop0OnDsp = new QCheckBox(Loop0Box);
        loop0OnDsp->setObjectName(QString::fromUtf8("loop0OnDsp"));

        hboxLayout22->addWidget(loop0OnDsp);

        loop0Sp10 = new QSpacerItem(130, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
        
        hboxLayout22->addItem(loop0Sp10);

        loopFitMethodLabel = new QLabel(Loop0Box);
        loopFitMethodLabel->setObjectName(QString::fromUtf8("loopFitMethodLabel"));
        loopFitMethodLabel->setWordWrap(false);

        hboxLayout22->addWidget(loopFitMethodLabel);

        loopFitMethod = new QComboBox(Loop0Box);
        loopFitMethod->setObjectName(QString::fromUtf8("loopFitMethod"));
        loopFitMethod->setMinimumSize(QSize(200, 0));

        hboxLayout22->addWidget(loopFitMethod);


        vboxLayout15->addLayout(hboxLayout22);


        vboxLayout14->addLayout(vboxLayout15);


        vboxLayout13->addWidget(Loop0Box);

        loopBoxStack->addWidget(loop0BoxPage);
        loop1BoxPage = new QWidget(loopBoxStack);
        loop1BoxPage->setObjectName(QString::fromUtf8("loop1BoxPage"));
        loop1BoxPage->setGeometry(QRect(0, 0, 435, 230));
        vboxLayout19 = new QVBoxLayout(loop1BoxPage);
        vboxLayout19->setSpacing(0);
        vboxLayout19->setContentsMargins(0, 0, 0, 0);
        vboxLayout19->setObjectName(QString::fromUtf8("vboxLayout19"));
        Loop1Box = new QGroupBox(loop1BoxPage);
        Loop1Box->setObjectName(QString::fromUtf8("Loop1Box"));
        vboxLayout20 = new QVBoxLayout(Loop1Box);
        vboxLayout20->setSpacing(4);
        vboxLayout20->setContentsMargins(11, 11, 11, 11);
        vboxLayout20->setObjectName(QString::fromUtf8("vboxLayout20"));
        vboxLayout21 = new QVBoxLayout();
        vboxLayout21->setSpacing(4);
        vboxLayout21->setObjectName(QString::fromUtf8("vboxLayout21"));
        hboxLayout23 = new QHBoxLayout();
        hboxLayout23->setSpacing(4);
        hboxLayout23->setObjectName(QString::fromUtf8("hboxLayout23"));
        loop1Type = new QComboBox(Loop1Box);
        loop1Type->setObjectName(QString::fromUtf8("loop1Type"));
        loop1Type->setMinimumSize(QSize(200, 0));

        hboxLayout23->addWidget(loop1Type);

        loop1Sp8 = new QSpacerItem(50, 21, QSizePolicy::Maximum, QSizePolicy::Minimum);

        hboxLayout23->addItem(loop1Sp8);

        loop1Active = new QCheckBox(Loop1Box);
        loop1Active->setObjectName(QString::fromUtf8("loop1Active"));

        hboxLayout23->addWidget(loop1Active);

        loop1Sp9 = new QSpacerItem(61, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout23->addItem(loop1Sp9);

        loop1DspProc = new QCheckBox(Loop1Box);
        loop1DspProc->setObjectName(QString::fromUtf8("loop1DspProc"));

        hboxLayout23->addWidget(loop1DspProc);


        vboxLayout21->addLayout(hboxLayout23);

        hboxLayout24 = new QHBoxLayout();
        hboxLayout24->setSpacing(4);
        hboxLayout24->setObjectName(QString::fromUtf8("hboxLayout24"));
        vboxLayout22 = new QVBoxLayout();
        vboxLayout22->setSpacing(4);
        vboxLayout22->setObjectName(QString::fromUtf8("vboxLayout22"));
        loop1Sp2 = new QSpacerItem(20, 17, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout22->addItem(loop1Sp2);

        vboxLayout23 = new QVBoxLayout();
        vboxLayout23->setSpacing(4);
        vboxLayout23->setObjectName(QString::fromUtf8("vboxLayout23"));
        hboxLayout25 = new QHBoxLayout();
        hboxLayout25->setSpacing(4);
        hboxLayout25->setObjectName(QString::fromUtf8("hboxLayout25"));
        loop1FromLabel = new QLabel(Loop1Box);
        loop1FromLabel->setObjectName(QString::fromUtf8("loop1FromLabel"));
        loop1FromLabel->setWordWrap(false);

        hboxLayout25->addWidget(loop1FromLabel);

        loop1Start = new QSpinBox(Loop1Box);
        loop1Start->setObjectName(QString::fromUtf8("loop1Start"));
        loop1Start->setEnabled(true);
        QPalette palette4;
        palette4.setBrush(QPalette::Active, QPalette::WindowText, brush1);
        palette4.setBrush(QPalette::Active, QPalette::Button, brush2);
        palette4.setBrush(QPalette::Active, QPalette::Light, brush3);
        palette4.setBrush(QPalette::Active, QPalette::Midlight, brush4);
        palette4.setBrush(QPalette::Active, QPalette::Dark, brush5);
        palette4.setBrush(QPalette::Active, QPalette::Mid, brush6);
        palette4.setBrush(QPalette::Active, QPalette::Text, brush1);
        palette4.setBrush(QPalette::Active, QPalette::BrightText, brush3);
        palette4.setBrush(QPalette::Active, QPalette::ButtonText, brush1);
        palette4.setBrush(QPalette::Active, QPalette::Base, brush3);
        palette4.setBrush(QPalette::Active, QPalette::Window, brush7);
        palette4.setBrush(QPalette::Active, QPalette::Shadow, brush1);
        palette4.setBrush(QPalette::Active, QPalette::Highlight, brush8);
        palette4.setBrush(QPalette::Active, QPalette::HighlightedText, brush3);
        palette4.setBrush(QPalette::Active, QPalette::Link, brush1);
        palette4.setBrush(QPalette::Active, QPalette::LinkVisited, brush1);
        palette4.setBrush(QPalette::Inactive, QPalette::WindowText, brush1);
        palette4.setBrush(QPalette::Inactive, QPalette::Button, brush2);
        palette4.setBrush(QPalette::Inactive, QPalette::Light, brush3);
        palette4.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette4.setBrush(QPalette::Inactive, QPalette::Dark, brush5);
        palette4.setBrush(QPalette::Inactive, QPalette::Mid, brush6);
        palette4.setBrush(QPalette::Inactive, QPalette::Text, brush1);
        palette4.setBrush(QPalette::Inactive, QPalette::BrightText, brush3);
        palette4.setBrush(QPalette::Inactive, QPalette::ButtonText, brush1);
        palette4.setBrush(QPalette::Inactive, QPalette::Base, brush3);
        palette4.setBrush(QPalette::Inactive, QPalette::Window, brush7);
        palette4.setBrush(QPalette::Inactive, QPalette::Shadow, brush1);
        palette4.setBrush(QPalette::Inactive, QPalette::Highlight, brush8);
        palette4.setBrush(QPalette::Inactive, QPalette::HighlightedText, brush3);
        palette4.setBrush(QPalette::Inactive, QPalette::Link, brush9);
        palette4.setBrush(QPalette::Inactive, QPalette::LinkVisited, brush10);
        palette4.setBrush(QPalette::Disabled, QPalette::WindowText, brush11);
        palette4.setBrush(QPalette::Disabled, QPalette::Button, brush2);
        palette4.setBrush(QPalette::Disabled, QPalette::Light, brush3);
        palette4.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette4.setBrush(QPalette::Disabled, QPalette::Dark, brush5);
        palette4.setBrush(QPalette::Disabled, QPalette::Mid, brush6);
        palette4.setBrush(QPalette::Disabled, QPalette::Text, brush11);
        palette4.setBrush(QPalette::Disabled, QPalette::BrightText, brush3);
        palette4.setBrush(QPalette::Disabled, QPalette::ButtonText, brush11);
        palette4.setBrush(QPalette::Disabled, QPalette::Base, brush3);
        palette4.setBrush(QPalette::Disabled, QPalette::Window, brush7);
        palette4.setBrush(QPalette::Disabled, QPalette::Shadow, brush1);
        palette4.setBrush(QPalette::Disabled, QPalette::Highlight, brush8);
        palette4.setBrush(QPalette::Disabled, QPalette::HighlightedText, brush3);
        palette4.setBrush(QPalette::Disabled, QPalette::Link, brush9);
        palette4.setBrush(QPalette::Disabled, QPalette::LinkVisited, brush10);
        loop1Start->setPalette(palette4);
        loop1Start->setMinimum(-99999999);
        loop1Start->setMaximum(99999999);

        hboxLayout25->addWidget(loop1Start);


        vboxLayout23->addLayout(hboxLayout25);

        hboxLayout26 = new QHBoxLayout();
        hboxLayout26->setSpacing(4);
        hboxLayout26->setObjectName(QString::fromUtf8("hboxLayout26"));
        loop1ToLabel = new QLabel(Loop1Box);
        loop1ToLabel->setObjectName(QString::fromUtf8("loop1ToLabel"));
        loop1ToLabel->setWordWrap(false);

        hboxLayout26->addWidget(loop1ToLabel);

        loop1Stop = new QSpinBox(Loop1Box);
        loop1Stop->setObjectName(QString::fromUtf8("loop1Stop"));
        loop1Stop->setEnabled(true);
        QPalette palette5;
        palette5.setBrush(QPalette::Active, QPalette::WindowText, brush1);
        palette5.setBrush(QPalette::Active, QPalette::Button, brush2);
        palette5.setBrush(QPalette::Active, QPalette::Light, brush3);
        palette5.setBrush(QPalette::Active, QPalette::Midlight, brush4);
        palette5.setBrush(QPalette::Active, QPalette::Dark, brush5);
        palette5.setBrush(QPalette::Active, QPalette::Mid, brush6);
        palette5.setBrush(QPalette::Active, QPalette::Text, brush1);
        palette5.setBrush(QPalette::Active, QPalette::BrightText, brush3);
        palette5.setBrush(QPalette::Active, QPalette::ButtonText, brush1);
        palette5.setBrush(QPalette::Active, QPalette::Base, brush3);
        palette5.setBrush(QPalette::Active, QPalette::Window, brush7);
        palette5.setBrush(QPalette::Active, QPalette::Shadow, brush1);
        palette5.setBrush(QPalette::Active, QPalette::Highlight, brush8);
        palette5.setBrush(QPalette::Active, QPalette::HighlightedText, brush3);
        palette5.setBrush(QPalette::Active, QPalette::Link, brush1);
        palette5.setBrush(QPalette::Active, QPalette::LinkVisited, brush1);
        palette5.setBrush(QPalette::Inactive, QPalette::WindowText, brush1);
        palette5.setBrush(QPalette::Inactive, QPalette::Button, brush2);
        palette5.setBrush(QPalette::Inactive, QPalette::Light, brush3);
        palette5.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette5.setBrush(QPalette::Inactive, QPalette::Dark, brush5);
        palette5.setBrush(QPalette::Inactive, QPalette::Mid, brush6);
        palette5.setBrush(QPalette::Inactive, QPalette::Text, brush1);
        palette5.setBrush(QPalette::Inactive, QPalette::BrightText, brush3);
        palette5.setBrush(QPalette::Inactive, QPalette::ButtonText, brush1);
        palette5.setBrush(QPalette::Inactive, QPalette::Base, brush3);
        palette5.setBrush(QPalette::Inactive, QPalette::Window, brush7);
        palette5.setBrush(QPalette::Inactive, QPalette::Shadow, brush1);
        palette5.setBrush(QPalette::Inactive, QPalette::Highlight, brush8);
        palette5.setBrush(QPalette::Inactive, QPalette::HighlightedText, brush3);
        palette5.setBrush(QPalette::Inactive, QPalette::Link, brush9);
        palette5.setBrush(QPalette::Inactive, QPalette::LinkVisited, brush10);
        palette5.setBrush(QPalette::Disabled, QPalette::WindowText, brush11);
        palette5.setBrush(QPalette::Disabled, QPalette::Button, brush2);
        palette5.setBrush(QPalette::Disabled, QPalette::Light, brush3);
        palette5.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette5.setBrush(QPalette::Disabled, QPalette::Dark, brush5);
        palette5.setBrush(QPalette::Disabled, QPalette::Mid, brush6);
        palette5.setBrush(QPalette::Disabled, QPalette::Text, brush11);
        palette5.setBrush(QPalette::Disabled, QPalette::BrightText, brush3);
        palette5.setBrush(QPalette::Disabled, QPalette::ButtonText, brush11);
        palette5.setBrush(QPalette::Disabled, QPalette::Base, brush3);
        palette5.setBrush(QPalette::Disabled, QPalette::Window, brush7);
        palette5.setBrush(QPalette::Disabled, QPalette::Shadow, brush1);
        palette5.setBrush(QPalette::Disabled, QPalette::Highlight, brush8);
        palette5.setBrush(QPalette::Disabled, QPalette::HighlightedText, brush3);
        palette5.setBrush(QPalette::Disabled, QPalette::Link, brush9);
        palette5.setBrush(QPalette::Disabled, QPalette::LinkVisited, brush10);
        loop1Stop->setPalette(palette5);
        loop1Stop->setMinimum(-99999999);
        loop1Stop->setMaximum(99999999);

        hboxLayout26->addWidget(loop1Stop);


        vboxLayout23->addLayout(hboxLayout26);

        hboxLayout27 = new QHBoxLayout();
        hboxLayout27->setSpacing(4);
        hboxLayout27->setObjectName(QString::fromUtf8("hboxLayout27"));
        loop1StepsLabel = new QLabel(Loop1Box);
        loop1StepsLabel->setObjectName(QString::fromUtf8("loop1StepsLabel"));
        loop1StepsLabel->setWordWrap(false);

        hboxLayout27->addWidget(loop1StepsLabel);

        loop1Step = new QSpinBox(Loop1Box);
        loop1Step->setObjectName(QString::fromUtf8("loop1Step"));
        loop1Step->setEnabled(true);
        QPalette palette6;
        palette6.setBrush(QPalette::Active, QPalette::WindowText, brush1);
        palette6.setBrush(QPalette::Active, QPalette::Button, brush2);
        palette6.setBrush(QPalette::Active, QPalette::Light, brush3);
        palette6.setBrush(QPalette::Active, QPalette::Midlight, brush4);
        palette6.setBrush(QPalette::Active, QPalette::Dark, brush5);
        palette6.setBrush(QPalette::Active, QPalette::Mid, brush6);
        palette6.setBrush(QPalette::Active, QPalette::Text, brush1);
        palette6.setBrush(QPalette::Active, QPalette::BrightText, brush3);
        palette6.setBrush(QPalette::Active, QPalette::ButtonText, brush1);
        palette6.setBrush(QPalette::Active, QPalette::Base, brush3);
        palette6.setBrush(QPalette::Active, QPalette::Window, brush7);
        palette6.setBrush(QPalette::Active, QPalette::Shadow, brush1);
        palette6.setBrush(QPalette::Active, QPalette::Highlight, brush8);
        palette6.setBrush(QPalette::Active, QPalette::HighlightedText, brush3);
        palette6.setBrush(QPalette::Active, QPalette::Link, brush1);
        palette6.setBrush(QPalette::Active, QPalette::LinkVisited, brush1);
        palette6.setBrush(QPalette::Inactive, QPalette::WindowText, brush1);
        palette6.setBrush(QPalette::Inactive, QPalette::Button, brush2);
        palette6.setBrush(QPalette::Inactive, QPalette::Light, brush3);
        palette6.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette6.setBrush(QPalette::Inactive, QPalette::Dark, brush5);
        palette6.setBrush(QPalette::Inactive, QPalette::Mid, brush6);
        palette6.setBrush(QPalette::Inactive, QPalette::Text, brush1);
        palette6.setBrush(QPalette::Inactive, QPalette::BrightText, brush3);
        palette6.setBrush(QPalette::Inactive, QPalette::ButtonText, brush1);
        palette6.setBrush(QPalette::Inactive, QPalette::Base, brush3);
        palette6.setBrush(QPalette::Inactive, QPalette::Window, brush7);
        palette6.setBrush(QPalette::Inactive, QPalette::Shadow, brush1);
        palette6.setBrush(QPalette::Inactive, QPalette::Highlight, brush8);
        palette6.setBrush(QPalette::Inactive, QPalette::HighlightedText, brush3);
        palette6.setBrush(QPalette::Inactive, QPalette::Link, brush9);
        palette6.setBrush(QPalette::Inactive, QPalette::LinkVisited, brush10);
        palette6.setBrush(QPalette::Disabled, QPalette::WindowText, brush11);
        palette6.setBrush(QPalette::Disabled, QPalette::Button, brush2);
        palette6.setBrush(QPalette::Disabled, QPalette::Light, brush3);
        palette6.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette6.setBrush(QPalette::Disabled, QPalette::Dark, brush5);
        palette6.setBrush(QPalette::Disabled, QPalette::Mid, brush6);
        palette6.setBrush(QPalette::Disabled, QPalette::Text, brush11);
        palette6.setBrush(QPalette::Disabled, QPalette::BrightText, brush3);
        palette6.setBrush(QPalette::Disabled, QPalette::ButtonText, brush11);
        palette6.setBrush(QPalette::Disabled, QPalette::Base, brush3);
        palette6.setBrush(QPalette::Disabled, QPalette::Window, brush7);
        palette6.setBrush(QPalette::Disabled, QPalette::Shadow, brush1);
        palette6.setBrush(QPalette::Disabled, QPalette::Highlight, brush8);
        palette6.setBrush(QPalette::Disabled, QPalette::HighlightedText, brush3);
        palette6.setBrush(QPalette::Disabled, QPalette::Link, brush9);
        palette6.setBrush(QPalette::Disabled, QPalette::LinkVisited, brush10);
        loop1Step->setPalette(palette6);
        loop1Step->setMaximum(100000);

        hboxLayout27->addWidget(loop1Step);


        vboxLayout23->addLayout(hboxLayout27);


        vboxLayout22->addLayout(vboxLayout23);

        loop1Sp1 = new QSpacerItem(20, 17, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout22->addItem(loop1Sp1);

        loop1Regular = new QCheckBox(Loop1Box);
        loop1Regular->setObjectName(QString::fromUtf8("loop1Regular"));
        loop1Regular->setChecked(true);

        vboxLayout22->addWidget(loop1Regular);

        loop1Free = new QCheckBox(Loop1Box);
        loop1Free->setObjectName(QString::fromUtf8("loop1Free"));

        vboxLayout22->addWidget(loop1Free);


        hboxLayout24->addLayout(vboxLayout22);

        loop1Sp3 = new QSpacerItem(130, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout24->addItem(loop1Sp3);

        vboxLayout24 = new QVBoxLayout();
        vboxLayout24->setSpacing(4);
        vboxLayout24->setObjectName(QString::fromUtf8("vboxLayout24"));
        loop1PtlLabel = new QLabel(Loop1Box);
        loop1PtlLabel->setObjectName(QString::fromUtf8("loop1PtlLabel"));
        loop1PtlLabel->setWordWrap(false);

        vboxLayout24->addWidget(loop1PtlLabel);

        loop1PtsBox = new QListWidget(Loop1Box);
        loop1PtsBox->setObjectName(QString::fromUtf8("loop1PtsBox"));

        vboxLayout24->addWidget(loop1PtsBox);

        loop1LoadPoints = new QPushButton(Loop1Box);
        loop1LoadPoints->setObjectName(QString::fromUtf8("loop1LoadPoints"));
        loop1LoadPoints->setEnabled(false);

        vboxLayout24->addWidget(loop1LoadPoints);


        hboxLayout24->addLayout(vboxLayout24);

        loop1Sp4 = new QSpacerItem(130, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout24->addItem(loop1Sp4);


        vboxLayout21->addLayout(hboxLayout24);

        hboxLayout28 = new QHBoxLayout();
        hboxLayout28->setSpacing(4);
        hboxLayout28->setObjectName(QString::fromUtf8("hboxLayout28"));
        loop1PostActLabel = new QLabel(Loop1Box);
        loop1PostActLabel->setObjectName(QString::fromUtf8("loop1PostActLabel"));
        loop1PostActLabel->setWordWrap(false);

        hboxLayout28->addWidget(loop1PostActLabel);

        loop1EndAction = new QComboBox(Loop1Box);
        loop1EndAction->setObjectName(QString::fromUtf8("loop1EndAction"));
        loop1EndAction->setMinimumSize(QSize(200, 0));

        hboxLayout28->addWidget(loop1EndAction);

        loop1Sp5 = new QSpacerItem(16, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        hboxLayout28->addItem(loop1Sp5);

        loop1OnDsp = new QCheckBox(Loop1Box);
        loop1OnDsp->setObjectName(QString::fromUtf8("loop1OnDsp"));

        hboxLayout28->addWidget(loop1OnDsp);

        loop1Sp10 = new QSpacerItem(130, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout28->addItem(loop1Sp10);


        vboxLayout21->addLayout(hboxLayout28);


        vboxLayout20->addLayout(vboxLayout21);


        vboxLayout19->addWidget(Loop1Box);

        loopBoxStack->addWidget(loop1BoxPage);
        loop2BoxPage = new QWidget(loopBoxStack);
        loop2BoxPage->setObjectName(QString::fromUtf8("loop2BoxPage"));
        loop2BoxPage->setGeometry(QRect(0, 0, 435, 230));
        vboxLayout25 = new QVBoxLayout(loop2BoxPage);
        vboxLayout25->setSpacing(0);
        vboxLayout25->setContentsMargins(0, 0, 0, 0);
        vboxLayout25->setObjectName(QString::fromUtf8("vboxLayout25"));
        Loop2Box = new QGroupBox(loop2BoxPage);
        Loop2Box->setObjectName(QString::fromUtf8("Loop2Box"));
        vboxLayout26 = new QVBoxLayout(Loop2Box);
        vboxLayout26->setSpacing(4);
        vboxLayout26->setContentsMargins(11, 11, 11, 11);
        vboxLayout26->setObjectName(QString::fromUtf8("vboxLayout26"));
        vboxLayout27 = new QVBoxLayout();
        vboxLayout27->setSpacing(4);
        vboxLayout27->setObjectName(QString::fromUtf8("vboxLayout27"));
        hboxLayout29 = new QHBoxLayout();
        hboxLayout29->setSpacing(4);
        hboxLayout29->setObjectName(QString::fromUtf8("hboxLayout29"));
        loop2Type = new QComboBox(Loop2Box);
        loop2Type->setObjectName(QString::fromUtf8("loop2Type"));
        loop2Type->setMinimumSize(QSize(200, 0));

        hboxLayout29->addWidget(loop2Type);

        loop2Sp8 = new QSpacerItem(50, 21, QSizePolicy::Maximum, QSizePolicy::Minimum);

        hboxLayout29->addItem(loop2Sp8);

        loop2Active = new QCheckBox(Loop2Box);
        loop2Active->setObjectName(QString::fromUtf8("loop2Active"));

        hboxLayout29->addWidget(loop2Active);

        loop2Sp9 = new QSpacerItem(61, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout29->addItem(loop2Sp9);

        loop2DspProc = new QCheckBox(Loop2Box);
        loop2DspProc->setObjectName(QString::fromUtf8("loop2DspProc"));

        hboxLayout29->addWidget(loop2DspProc);


        vboxLayout27->addLayout(hboxLayout29);

        hboxLayout30 = new QHBoxLayout();
        hboxLayout30->setSpacing(4);
        hboxLayout30->setObjectName(QString::fromUtf8("hboxLayout30"));
        vboxLayout28 = new QVBoxLayout();
        vboxLayout28->setSpacing(4);
        vboxLayout28->setObjectName(QString::fromUtf8("vboxLayout28"));
        loop2Sp2 = new QSpacerItem(20, 17, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout28->addItem(loop2Sp2);

        vboxLayout29 = new QVBoxLayout();
        vboxLayout29->setSpacing(4);
        vboxLayout29->setObjectName(QString::fromUtf8("vboxLayout29"));
        hboxLayout31 = new QHBoxLayout();
        hboxLayout31->setSpacing(4);
        hboxLayout31->setObjectName(QString::fromUtf8("hboxLayout31"));
        loop2FromLabel = new QLabel(Loop2Box);
        loop2FromLabel->setObjectName(QString::fromUtf8("loop2FromLabel"));
        loop2FromLabel->setWordWrap(false);

        hboxLayout31->addWidget(loop2FromLabel);

        loop2Start = new QSpinBox(Loop2Box);
        loop2Start->setObjectName(QString::fromUtf8("loop2Start"));
        loop2Start->setEnabled(true);
        QPalette palette7;
        palette7.setBrush(QPalette::Active, QPalette::WindowText, brush1);
        palette7.setBrush(QPalette::Active, QPalette::Button, brush2);
        palette7.setBrush(QPalette::Active, QPalette::Light, brush3);
        palette7.setBrush(QPalette::Active, QPalette::Midlight, brush4);
        palette7.setBrush(QPalette::Active, QPalette::Dark, brush5);
        palette7.setBrush(QPalette::Active, QPalette::Mid, brush6);
        palette7.setBrush(QPalette::Active, QPalette::Text, brush1);
        palette7.setBrush(QPalette::Active, QPalette::BrightText, brush3);
        palette7.setBrush(QPalette::Active, QPalette::ButtonText, brush1);
        palette7.setBrush(QPalette::Active, QPalette::Base, brush3);
        palette7.setBrush(QPalette::Active, QPalette::Window, brush7);
        palette7.setBrush(QPalette::Active, QPalette::Shadow, brush1);
        palette7.setBrush(QPalette::Active, QPalette::Highlight, brush8);
        palette7.setBrush(QPalette::Active, QPalette::HighlightedText, brush3);
        palette7.setBrush(QPalette::Active, QPalette::Link, brush1);
        palette7.setBrush(QPalette::Active, QPalette::LinkVisited, brush1);
        palette7.setBrush(QPalette::Inactive, QPalette::WindowText, brush1);
        palette7.setBrush(QPalette::Inactive, QPalette::Button, brush2);
        palette7.setBrush(QPalette::Inactive, QPalette::Light, brush3);
        palette7.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette7.setBrush(QPalette::Inactive, QPalette::Dark, brush5);
        palette7.setBrush(QPalette::Inactive, QPalette::Mid, brush6);
        palette7.setBrush(QPalette::Inactive, QPalette::Text, brush1);
        palette7.setBrush(QPalette::Inactive, QPalette::BrightText, brush3);
        palette7.setBrush(QPalette::Inactive, QPalette::ButtonText, brush1);
        palette7.setBrush(QPalette::Inactive, QPalette::Base, brush3);
        palette7.setBrush(QPalette::Inactive, QPalette::Window, brush7);
        palette7.setBrush(QPalette::Inactive, QPalette::Shadow, brush1);
        palette7.setBrush(QPalette::Inactive, QPalette::Highlight, brush8);
        palette7.setBrush(QPalette::Inactive, QPalette::HighlightedText, brush3);
        palette7.setBrush(QPalette::Inactive, QPalette::Link, brush9);
        palette7.setBrush(QPalette::Inactive, QPalette::LinkVisited, brush10);
        palette7.setBrush(QPalette::Disabled, QPalette::WindowText, brush11);
        palette7.setBrush(QPalette::Disabled, QPalette::Button, brush2);
        palette7.setBrush(QPalette::Disabled, QPalette::Light, brush3);
        palette7.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette7.setBrush(QPalette::Disabled, QPalette::Dark, brush5);
        palette7.setBrush(QPalette::Disabled, QPalette::Mid, brush6);
        palette7.setBrush(QPalette::Disabled, QPalette::Text, brush11);
        palette7.setBrush(QPalette::Disabled, QPalette::BrightText, brush3);
        palette7.setBrush(QPalette::Disabled, QPalette::ButtonText, brush11);
        palette7.setBrush(QPalette::Disabled, QPalette::Base, brush3);
        palette7.setBrush(QPalette::Disabled, QPalette::Window, brush7);
        palette7.setBrush(QPalette::Disabled, QPalette::Shadow, brush1);
        palette7.setBrush(QPalette::Disabled, QPalette::Highlight, brush8);
        palette7.setBrush(QPalette::Disabled, QPalette::HighlightedText, brush3);
        palette7.setBrush(QPalette::Disabled, QPalette::Link, brush9);
        palette7.setBrush(QPalette::Disabled, QPalette::LinkVisited, brush10);
        loop2Start->setPalette(palette7);
        loop2Start->setMinimum(-99999999);
        loop2Start->setMaximum(99999999);

        hboxLayout31->addWidget(loop2Start);


        vboxLayout29->addLayout(hboxLayout31);

        hboxLayout32 = new QHBoxLayout();
        hboxLayout32->setSpacing(4);
        hboxLayout32->setObjectName(QString::fromUtf8("hboxLayout32"));
        loop2ToLabel = new QLabel(Loop2Box);
        loop2ToLabel->setObjectName(QString::fromUtf8("loop2ToLabel"));
        loop2ToLabel->setWordWrap(false);

        hboxLayout32->addWidget(loop2ToLabel);

        loop2Stop = new QSpinBox(Loop2Box);
        loop2Stop->setObjectName(QString::fromUtf8("loop2Stop"));
        loop2Stop->setEnabled(true);
        QPalette palette8;
        palette8.setBrush(QPalette::Active, QPalette::WindowText, brush1);
        palette8.setBrush(QPalette::Active, QPalette::Button, brush2);
        palette8.setBrush(QPalette::Active, QPalette::Light, brush3);
        palette8.setBrush(QPalette::Active, QPalette::Midlight, brush4);
        palette8.setBrush(QPalette::Active, QPalette::Dark, brush5);
        palette8.setBrush(QPalette::Active, QPalette::Mid, brush6);
        palette8.setBrush(QPalette::Active, QPalette::Text, brush1);
        palette8.setBrush(QPalette::Active, QPalette::BrightText, brush3);
        palette8.setBrush(QPalette::Active, QPalette::ButtonText, brush1);
        palette8.setBrush(QPalette::Active, QPalette::Base, brush3);
        palette8.setBrush(QPalette::Active, QPalette::Window, brush7);
        palette8.setBrush(QPalette::Active, QPalette::Shadow, brush1);
        palette8.setBrush(QPalette::Active, QPalette::Highlight, brush8);
        palette8.setBrush(QPalette::Active, QPalette::HighlightedText, brush3);
        palette8.setBrush(QPalette::Active, QPalette::Link, brush1);
        palette8.setBrush(QPalette::Active, QPalette::LinkVisited, brush1);
        palette8.setBrush(QPalette::Inactive, QPalette::WindowText, brush1);
        palette8.setBrush(QPalette::Inactive, QPalette::Button, brush2);
        palette8.setBrush(QPalette::Inactive, QPalette::Light, brush3);
        palette8.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette8.setBrush(QPalette::Inactive, QPalette::Dark, brush5);
        palette8.setBrush(QPalette::Inactive, QPalette::Mid, brush6);
        palette8.setBrush(QPalette::Inactive, QPalette::Text, brush1);
        palette8.setBrush(QPalette::Inactive, QPalette::BrightText, brush3);
        palette8.setBrush(QPalette::Inactive, QPalette::ButtonText, brush1);
        palette8.setBrush(QPalette::Inactive, QPalette::Base, brush3);
        palette8.setBrush(QPalette::Inactive, QPalette::Window, brush7);
        palette8.setBrush(QPalette::Inactive, QPalette::Shadow, brush1);
        palette8.setBrush(QPalette::Inactive, QPalette::Highlight, brush8);
        palette8.setBrush(QPalette::Inactive, QPalette::HighlightedText, brush3);
        palette8.setBrush(QPalette::Inactive, QPalette::Link, brush9);
        palette8.setBrush(QPalette::Inactive, QPalette::LinkVisited, brush10);
        palette8.setBrush(QPalette::Disabled, QPalette::WindowText, brush11);
        palette8.setBrush(QPalette::Disabled, QPalette::Button, brush2);
        palette8.setBrush(QPalette::Disabled, QPalette::Light, brush3);
        palette8.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette8.setBrush(QPalette::Disabled, QPalette::Dark, brush5);
        palette8.setBrush(QPalette::Disabled, QPalette::Mid, brush6);
        palette8.setBrush(QPalette::Disabled, QPalette::Text, brush11);
        palette8.setBrush(QPalette::Disabled, QPalette::BrightText, brush3);
        palette8.setBrush(QPalette::Disabled, QPalette::ButtonText, brush11);
        palette8.setBrush(QPalette::Disabled, QPalette::Base, brush3);
        palette8.setBrush(QPalette::Disabled, QPalette::Window, brush7);
        palette8.setBrush(QPalette::Disabled, QPalette::Shadow, brush1);
        palette8.setBrush(QPalette::Disabled, QPalette::Highlight, brush8);
        palette8.setBrush(QPalette::Disabled, QPalette::HighlightedText, brush3);
        palette8.setBrush(QPalette::Disabled, QPalette::Link, brush9);
        palette8.setBrush(QPalette::Disabled, QPalette::LinkVisited, brush10);
        loop2Stop->setPalette(palette8);
        loop2Stop->setMinimum(-99999999);
        loop2Stop->setMaximum(99999999);

        hboxLayout32->addWidget(loop2Stop);


        vboxLayout29->addLayout(hboxLayout32);

        hboxLayout33 = new QHBoxLayout();
        hboxLayout33->setSpacing(4);
        hboxLayout33->setObjectName(QString::fromUtf8("hboxLayout33"));
        loop2StepsLabel = new QLabel(Loop2Box);
        loop2StepsLabel->setObjectName(QString::fromUtf8("loop2StepsLabel"));
        loop2StepsLabel->setWordWrap(false);

        hboxLayout33->addWidget(loop2StepsLabel);

        loop2Step = new QSpinBox(Loop2Box);
        loop2Step->setObjectName(QString::fromUtf8("loop2Step"));
        loop2Step->setEnabled(true);
        QPalette palette9;
        palette9.setBrush(QPalette::Active, QPalette::WindowText, brush1);
        palette9.setBrush(QPalette::Active, QPalette::Button, brush2);
        palette9.setBrush(QPalette::Active, QPalette::Light, brush3);
        palette9.setBrush(QPalette::Active, QPalette::Midlight, brush4);
        palette9.setBrush(QPalette::Active, QPalette::Dark, brush5);
        palette9.setBrush(QPalette::Active, QPalette::Mid, brush6);
        palette9.setBrush(QPalette::Active, QPalette::Text, brush1);
        palette9.setBrush(QPalette::Active, QPalette::BrightText, brush3);
        palette9.setBrush(QPalette::Active, QPalette::ButtonText, brush1);
        palette9.setBrush(QPalette::Active, QPalette::Base, brush3);
        palette9.setBrush(QPalette::Active, QPalette::Window, brush7);
        palette9.setBrush(QPalette::Active, QPalette::Shadow, brush1);
        palette9.setBrush(QPalette::Active, QPalette::Highlight, brush8);
        palette9.setBrush(QPalette::Active, QPalette::HighlightedText, brush3);
        palette9.setBrush(QPalette::Active, QPalette::Link, brush1);
        palette9.setBrush(QPalette::Active, QPalette::LinkVisited, brush1);
        palette9.setBrush(QPalette::Inactive, QPalette::WindowText, brush1);
        palette9.setBrush(QPalette::Inactive, QPalette::Button, brush2);
        palette9.setBrush(QPalette::Inactive, QPalette::Light, brush3);
        palette9.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette9.setBrush(QPalette::Inactive, QPalette::Dark, brush5);
        palette9.setBrush(QPalette::Inactive, QPalette::Mid, brush6);
        palette9.setBrush(QPalette::Inactive, QPalette::Text, brush1);
        palette9.setBrush(QPalette::Inactive, QPalette::BrightText, brush3);
        palette9.setBrush(QPalette::Inactive, QPalette::ButtonText, brush1);
        palette9.setBrush(QPalette::Inactive, QPalette::Base, brush3);
        palette9.setBrush(QPalette::Inactive, QPalette::Window, brush7);
        palette9.setBrush(QPalette::Inactive, QPalette::Shadow, brush1);
        palette9.setBrush(QPalette::Inactive, QPalette::Highlight, brush8);
        palette9.setBrush(QPalette::Inactive, QPalette::HighlightedText, brush3);
        palette9.setBrush(QPalette::Inactive, QPalette::Link, brush9);
        palette9.setBrush(QPalette::Inactive, QPalette::LinkVisited, brush10);
        palette9.setBrush(QPalette::Disabled, QPalette::WindowText, brush11);
        palette9.setBrush(QPalette::Disabled, QPalette::Button, brush2);
        palette9.setBrush(QPalette::Disabled, QPalette::Light, brush3);
        palette9.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette9.setBrush(QPalette::Disabled, QPalette::Dark, brush5);
        palette9.setBrush(QPalette::Disabled, QPalette::Mid, brush6);
        palette9.setBrush(QPalette::Disabled, QPalette::Text, brush11);
        palette9.setBrush(QPalette::Disabled, QPalette::BrightText, brush3);
        palette9.setBrush(QPalette::Disabled, QPalette::ButtonText, brush11);
        palette9.setBrush(QPalette::Disabled, QPalette::Base, brush3);
        palette9.setBrush(QPalette::Disabled, QPalette::Window, brush7);
        palette9.setBrush(QPalette::Disabled, QPalette::Shadow, brush1);
        palette9.setBrush(QPalette::Disabled, QPalette::Highlight, brush8);
        palette9.setBrush(QPalette::Disabled, QPalette::HighlightedText, brush3);
        palette9.setBrush(QPalette::Disabled, QPalette::Link, brush9);
        palette9.setBrush(QPalette::Disabled, QPalette::LinkVisited, brush10);
        loop2Step->setPalette(palette9);
        loop2Step->setMaximum(100000);

        hboxLayout33->addWidget(loop2Step);


        vboxLayout29->addLayout(hboxLayout33);


        vboxLayout28->addLayout(vboxLayout29);

        loop2Sp1 = new QSpacerItem(20, 17, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout28->addItem(loop2Sp1);

        loop2Regular = new QCheckBox(Loop2Box);
        loop2Regular->setObjectName(QString::fromUtf8("loop2Regular"));
        loop2Regular->setChecked(true);

        vboxLayout28->addWidget(loop2Regular);

        loop2Free = new QCheckBox(Loop2Box);
        loop2Free->setObjectName(QString::fromUtf8("loop2Free"));

        vboxLayout28->addWidget(loop2Free);


        hboxLayout30->addLayout(vboxLayout28);

        loop2Sp3 = new QSpacerItem(130, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout30->addItem(loop2Sp3);

        vboxLayout30 = new QVBoxLayout();
        vboxLayout30->setSpacing(4);
        vboxLayout30->setObjectName(QString::fromUtf8("vboxLayout30"));
        loop2PtlLabel = new QLabel(Loop2Box);
        loop2PtlLabel->setObjectName(QString::fromUtf8("loop2PtlLabel"));
        loop2PtlLabel->setWordWrap(false);

        vboxLayout30->addWidget(loop2PtlLabel);

        loop2PtsBox = new QListWidget(Loop2Box);
        loop2PtsBox->setObjectName(QString::fromUtf8("loop2PtsBox"));

        vboxLayout30->addWidget(loop2PtsBox);

        loop2LoadPoints = new QPushButton(Loop2Box);
        loop2LoadPoints->setObjectName(QString::fromUtf8("loop2LoadPoints"));
        loop2LoadPoints->setEnabled(false);

        vboxLayout30->addWidget(loop2LoadPoints);


        hboxLayout30->addLayout(vboxLayout30);

        loop2Sp4 = new QSpacerItem(130, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout30->addItem(loop2Sp4);


        vboxLayout27->addLayout(hboxLayout30);

        hboxLayout34 = new QHBoxLayout();
        hboxLayout34->setSpacing(4);
        hboxLayout34->setObjectName(QString::fromUtf8("hboxLayout34"));
        loop2PostActLabel = new QLabel(Loop2Box);
        loop2PostActLabel->setObjectName(QString::fromUtf8("loop2PostActLabel"));
        loop2PostActLabel->setWordWrap(false);

        hboxLayout34->addWidget(loop2PostActLabel);

        loop2EndAction = new QComboBox(Loop2Box);
        loop2EndAction->setObjectName(QString::fromUtf8("loop2EndAction"));
        loop2EndAction->setMinimumSize(QSize(200, 0));

        hboxLayout34->addWidget(loop2EndAction);

        loop2Sp5 = new QSpacerItem(16, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        hboxLayout34->addItem(loop2Sp5);

        loop2OnDsp = new QCheckBox(Loop2Box);
        loop2OnDsp->setObjectName(QString::fromUtf8("loop2OnDsp"));

        hboxLayout34->addWidget(loop2OnDsp);

        loop2Sp10 = new QSpacerItem(130, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout34->addItem(loop2Sp10);


        vboxLayout27->addLayout(hboxLayout34);


        vboxLayout26->addLayout(vboxLayout27);

        vboxLayout25->addWidget(Loop2Box);

        loopBoxStack->addWidget(loop2BoxPage);

        vboxLayout11->addWidget(loopBoxStack);	

        //hboxLayout35 = new QHBoxLayout();
        //hboxLayout35->setSpacing(20);
        //hboxLayout35->setObjectName(QString::fromUtf8("hboxLayout35"));

	tuningParamsLayout = new QGridLayout();
        tuningParamsLayout->setColumnMinimumWidth(0,15);
        tuningParamsLayout->setColumnMinimumWidth(1,50);
        tuningParamsLayout->setColumnMinimumWidth(2,15);
        tuningParamsLayout->setColumnMinimumWidth(3,350);
        tuningParamsLayout->setColumnMinimumWidth(4,15);
        tuningParamsLayout->setColumnMinimumWidth(5,15);
        tuningParamsLayout->setColumnMinimumWidth(6,15);

	textLabel1_8 = new QLabel(tab1);
        textLabel1_8->setWordWrap(false);
	tuningParamsLayout->addWidget(textLabel1_8,0,1);

	totchargelabel = new QLabel(tab1);
        totchargelabel->setWordWrap(false);
	tuningParamsLayout->addWidget(totchargelabel,1,1);

	textLabel3_2 = new QLabel(tab1);
        textLabel3_2->setWordWrap(false);
	tuningParamsLayout->addWidget(textLabel3_2,2,1);

	fastThrPix = new QCheckBox("Fast threshold scan: pseudo-pixel-mode", tab1);
	tuningParamsLayout->addWidget(fastThrPix,3,1);

	//test location
	useTuningPointsFile = new QCheckBox("Use module by module tuning points",tab1);
	tuningParamsLayout->addWidget(useTuningPointsFile,4,1);
	
	tuningPointsSelect = new QPushButton("Select", tab1);
	tuningPointsSelect->setEnabled(false);
	tuningParamsLayout->addWidget(tuningPointsSelect, 4, 2);

	tuningPointsFile = new QLineEdit(tab1);
        tuningPointsFile->setText(QString::fromStdString("<not set>"));
        tuningPointsFile->setEnabled(false);
        tuningParamsLayout->addWidget(tuningPointsFile,4,3);

        thresvalue = new QSpinBox(tab1);
        //thresvalue->setObjectName(QString::fromUtf8("thresvalue"));
        thresvalue->setMinimum(1);
        thresvalue->setMaximum(100000);
        thresvalue->setFixedWidth(100);
        tuningParamsLayout->addWidget(thresvalue,0,3, Qt::AlignLeft);

        totvalue = new QSpinBox(tab1);
        //totvalue->setObjectName(QString::fromUtf8("totvalue"));
        totvalue->setMinimum(1);
        totvalue->setMaximum(256);
	totvalue->setFixedWidth(100);
        tuningParamsLayout->addWidget(totvalue,1,3, Qt::AlignLeft);

        totcharge = new QSpinBox(tab1);
        //totcharge->setObjectName(QString::fromUtf8("totcharge"));
        totcharge->setMinimum(1);
        totcharge->setMaximum(100000);
	totcharge->setFixedWidth(100);
        tuningParamsLayout->addWidget(totcharge,2,3, Qt::AlignLeft);
	
        //hboxLayout35->addLayout(tuningParamsLayout);
        //spacer62 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
        //hboxLayout35->addItem(spacer62);

        vboxLayout11->addSpacing(15);
        vboxLayout11->addLayout(tuningParamsLayout);
        vboxLayout11->addSpacing(50);

        //spacer38_3 = new QSpacerItem(20, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);
        //vboxLayout11->addItem(spacer38_3);

        tabwidget->addTab(tab1, QString());
        TabPage11 = new QWidget();
        TabPage11->setObjectName(QString::fromUtf8("TabPage11"));
        vboxLayout33 = new QVBoxLayout(TabPage11);
        vboxLayout33->setSpacing(4);
        vboxLayout33->setContentsMargins(11, 11, 11, 11);
        vboxLayout33->setObjectName(QString::fromUtf8("vboxLayout33"));
        scrollArea = new QScrollArea(TabPage11);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents_2 = new QWidget();
        scrollAreaWidgetContents_2->setObjectName(QString::fromUtf8("scrollAreaWidgetContents_2"));
        scrollAreaWidgetContents_2->setEnabled(true);
        scrollAreaWidgetContents_2->setGeometry(QRect(0, 0, 578, 917));
        horizontalLayout = new QHBoxLayout(scrollAreaWidgetContents_2);
        horizontalLayout->setSpacing(4);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        histoFillBox = new QGroupBox(scrollAreaWidgetContents_2);
        histoFillBox->setObjectName(QString::fromUtf8("histoFillBox"));
        //_2 = new QVBoxLayout(histoFillBox);
        //_2->setSpacing(4);
        //_2->setContentsMargins(11, 11, 11, 11);
        //_2->setObjectName(QString::fromUtf8("_2"));
        //_3 = new QHBoxLayout();
        //_3->setSpacing(4);
        //_3->setObjectName(QString::fromUtf8("_3"));
        //laterUsedLayout = new QVBoxLayout();
        //laterUsedLayout->setSpacing(4);
        //laterUsedLayout->setObjectName(QString::fromUtf8("laterUsedLayout"));

        histoSaveMenuVBox = new QVBoxLayout(histoFillBox);
        histoSaveMenu = new QGridLayout(histoFillBox);
        histoSaveMenu_2 = new QGridLayout(histoFillBox);

        //occ
        occLabel = new QLabel(histoFillBox);
        occLabel->setWordWrap(false);
        histoSaveMenu->addWidget(occLabel, 2, 1);

	occFill = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(occFill, 2, 2);

        occKeep = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(occKeep, 2, 3);

        //lvl 1
        lvl1Label = new QLabel(histoFillBox);
        lvl1Label->setWordWrap(false);
        histoSaveMenu->addWidget(lvl1Label, 3, 1);

        lvl1Fill = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(lvl1Fill, 3, 2);

        lvl1Keep = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(lvl1Keep, 3, 3);

        //totmean
        totmeanLabel = new QLabel(histoFillBox);
        totmeanLabel->setWordWrap(false);
        histoSaveMenu->addWidget(totmeanLabel, 5, 1);

        totmeanFill = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(totmeanFill, 5, 2);

        totmeanKeep = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(totmeanKeep, 5, 3);

        //tot sig
        totsigLabel = new QLabel(histoFillBox);
        totsigLabel->setWordWrap(false);
        histoSaveMenu->addWidget(totsigLabel, 6, 1);

        totsigFill = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(totsigFill, 6, 2);

        totsigKeep = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(totsigKeep, 6, 3);

        //fdact
        fdactlabel = new QLabel(histoFillBox);
        fdactlabel->setWordWrap(false);
        histoSaveMenu->addWidget(fdactlabel, 7, 1);

        fdactfill = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(fdactfill, 7, 2);

        fdactkeep = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(fdactkeep, 7, 3);

        //fdactot
        fdactotlabel = new QLabel(histoFillBox);
        fdactotlabel->setWordWrap(false);
        histoSaveMenu->addWidget(fdactotlabel, 8, 1);

        fdactotfill = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(fdactotfill, 8, 2);

        fdactotkeep = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(fdactotkeep, 8, 3);

        //gdact
        gdactlabel = new QLabel(histoFillBox);
        gdactlabel->setWordWrap(false);
        histoSaveMenu->addWidget(gdactlabel, 9, 1);

        gdactfill = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(gdactfill, 9, 2);

        gdactkeep = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(gdactkeep, 9, 3);

        //gdacthr
        gdacthrlabel = new QLabel(histoFillBox);
        gdacthrlabel->setWordWrap(false);
        histoSaveMenu->addWidget(gdacthrlabel, 10, 1);

        gdacthrfill = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(gdacthrfill, 10 ,2);

        gdacthrkeep = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(gdacthrkeep, 10, 3);

        //ift       
        iftlabel = new QLabel(histoFillBox);
        iftlabel->setWordWrap(false);
        histoSaveMenu->addWidget(iftlabel, 11, 1);

        iftfill = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(iftfill, 11, 2);

        iftkeep = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(iftkeep, 11, 3);

        //iftot
        iftotlabel = new QLabel(histoFillBox);
        iftotlabel->setWordWrap(false);
        histoSaveMenu->addWidget(iftotlabel, 12, 1);

        iftotfill = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(iftotfill, 12, 2);

        iftotkeep = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(iftotkeep, 12, 3);

        //scurvechi2
        scurvechi2label = new QLabel(histoFillBox);
        scurvechi2label->setWordWrap(false);
        histoSaveMenu->addWidget(scurvechi2label, 13, 1);

        scurvechi2fill = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(scurvechi2fill, 13, 2);

        scurvechi2keep = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(scurvechi2keep, 13, 3);

        //scurvemean
        scurvemeanlabel = new QLabel(histoFillBox);
        scurvemeanlabel->setWordWrap(false);
        histoSaveMenu->addWidget(scurvemeanlabel, 14, 1);

        scurvemeanfill = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(scurvemeanfill, 14, 2);

        scurvemeankeep = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(scurvemeankeep, 14, 3);

        //scurvesigma
        scurvesigmalabel = new QLabel(histoFillBox);
        scurvesigmalabel->setWordWrap(false);
        histoSaveMenu->addWidget(scurvesigmalabel, 15, 1);

        scurvesigmafill = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(scurvesigmafill, 15, 2);

        scurvesigmakeep = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(scurvesigmakeep, 15, 3);

        //tdact
        tdactlabel = new QLabel(histoFillBox);
        tdactlabel->setWordWrap(false);
        histoSaveMenu->addWidget(tdactlabel, 16, 1);

        tdactfill = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(tdactfill, 16, 2);

        tdactkeep = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(tdactkeep, 16, 3);

        //tdacthr
        tdacthrlabel = new QLabel(histoFillBox);
        tdacthrlabel->setWordWrap(false);
        histoSaveMenu->addWidget(tdacthrlabel, 17, 1);

        tdacthrfill = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(tdacthrfill, 17, 2);

        tdacthrkeep = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(tdacthrkeep, 17, 3);

        //tw
        twlabel = new QLabel(histoFillBox);
        twlabel->setWordWrap(false);
        histoSaveMenu->addWidget(twlabel, 18, 1);

        twfill = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(twfill, 18, 2);

        twkeep = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(twkeep, 18, 3);

        //rd0
        rd0label = new QLabel(histoFillBox);
        rd0label->setWordWrap(false);
        histoSaveMenu->addWidget(rd0label, 19, 1);

        rd0fill = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(rd0fill, 19, 2);

        rd0keep = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(rd0keep, 19, 3);

        //rd1
        rd1label = new QLabel(histoFillBox);
        rd1label->setWordWrap(false);
        histoSaveMenu->addWidget(rd1label, 20, 1);

        rd1fill = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(rd1fill, 20, 2);

        rd1keep = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(rd1keep, 20, 3);

        //rd2
        rd2label = new QLabel(histoFillBox);
        rd2label->setWordWrap(false);
        histoSaveMenu->addWidget(rd2label, 21, 1);

        rd2fill = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(rd2fill, 21, 2);

        rd2keep = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(rd2keep, 21, 3);

        //rdd1     
        rdd1label = new QLabel(histoFillBox);
        rdd1label->setWordWrap(false);
        histoSaveMenu->addWidget(rdd1label, 22, 1);

        rdd1fill = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(rdd1fill, 22, 2);

        rdd1keep = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(rdd1keep, 22, 3);

        //rdd2
        rdd2label = new QLabel(histoFillBox);
        rdd2label->setWordWrap(false);
        histoSaveMenu->addWidget(rdd2label, 23, 1);

        rdd2fill = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(rdd2fill, 23, 2);

        rdd2keep = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(rdd2keep, 23, 3);

        //rdr
        rdrlabel = new QLabel(histoFillBox);
        rdrlabel->setWordWrap(false);
        histoSaveMenu->addWidget(rdrlabel, 24, 1);

        rdrfill = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(rdrfill, 24, 2);

        rdrkeep = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(rdrkeep, 24, 3);

        //fcl
        fcllabel = new QLabel(histoFillBox);
        fcllabel->setWordWrap(false);
        histoSaveMenu->addWidget(fcllabel, 25, 1);

        fclfill = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(fclfill, 25, 2);

        fclkeep = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(fclkeep, 25, 3);

        //fct
        fctlabel = new QLabel(histoFillBox);
        fctlabel->setWordWrap(false);
        histoSaveMenu->addWidget(fctlabel, 26, 1);

        fctfill = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(fctfill, 26, 2);

        fctkeep = new QCheckBox(histoFillBox);
        histoSaveMenu->addWidget(fctkeep, 26, 3);

        histoSaveMenuVBox->addLayout(histoSaveMenu);
        histoSaveMenuVBox->addLayout(histoSaveMenu_2);

        histoFillBox->setLayout(histoSaveMenuVBox);

        horizontalLayout->addWidget(histoFillBox);

        scrollArea->setWidget(scrollAreaWidgetContents_2);

        vboxLayout33->addWidget(scrollArea);

        tabwidget->addTab(TabPage11, QString());
        TabPage = new QWidget();
        TabPage->setObjectName(QString::fromUtf8("TabPage"));
        vboxLayout40 = new QVBoxLayout(TabPage);
        vboxLayout40->setSpacing(4);
        vboxLayout40->setContentsMargins(11, 11, 11, 11);
        vboxLayout40->setObjectName(QString::fromUtf8("vboxLayout40"));
        vboxLayout41 = new QVBoxLayout();
        vboxLayout41->setSpacing(4);
        vboxLayout41->setObjectName(QString::fromUtf8("vboxLayout41"));
        vboxLayout42 = new QVBoxLayout();
        vboxLayout42->setSpacing(4);
        vboxLayout42->setObjectName(QString::fromUtf8("vboxLayout42"));
        mccBox = new QGroupBox(TabPage);
        mccBox->setObjectName(QString::fromUtf8("mccBox"));
        vboxLayout43 = new QVBoxLayout(mccBox);
        vboxLayout43->setSpacing(4);
        vboxLayout43->setContentsMargins(11, 11, 11, 11);
        vboxLayout43->setObjectName(QString::fromUtf8("vboxLayout43"));
        vboxLayout44 = new QVBoxLayout();
        vboxLayout44->setSpacing(4);
        vboxLayout44->setObjectName(QString::fromUtf8("vboxLayout44"));
        hboxLayout39 = new QHBoxLayout();
        hboxLayout39->setSpacing(6);
        hboxLayout39->setContentsMargins(0, 0, 0, 0);
        hboxLayout39->setObjectName(QString::fromUtf8("hboxLayout39"));
        BwidthLabel = new QLabel(mccBox);
        BwidthLabel->setObjectName(QString::fromUtf8("BwidthLabel"));
        BwidthLabel->setWordWrap(false);

        hboxLayout39->addWidget(BwidthLabel);

        MCC_Bwidth = new QComboBox(mccBox);
        MCC_Bwidth->setObjectName(QString::fromUtf8("MCC_Bwidth"));
        MCC_Bwidth->setEnabled(true);

        hboxLayout39->addWidget(MCC_Bwidth);


        vboxLayout44->addLayout(hboxLayout39);

        MCC_ErrorFlag = new QCheckBox(mccBox);
        MCC_ErrorFlag->setObjectName(QString::fromUtf8("MCC_ErrorFlag"));
        MCC_ErrorFlag->setEnabled(true);

        vboxLayout44->addWidget(MCC_ErrorFlag);

        MCC_FECheck = new QCheckBox(mccBox);
        MCC_FECheck->setObjectName(QString::fromUtf8("MCC_FECheck"));

        vboxLayout44->addWidget(MCC_FECheck);

        MCC_TimeStampComp = new QCheckBox(mccBox);
        MCC_TimeStampComp->setObjectName(QString::fromUtf8("MCC_TimeStampComp"));

        vboxLayout44->addWidget(MCC_TimeStampComp);


        vboxLayout43->addLayout(vboxLayout44);


        vboxLayout42->addWidget(mccBox);

        feBox = new QGroupBox(TabPage);
        feBox->setObjectName(QString::fromUtf8("feBox"));
        hboxLayout40 = new QHBoxLayout(feBox);
        hboxLayout40->setSpacing(4);
        hboxLayout40->setContentsMargins(11, 11, 11, 11);
        hboxLayout40->setObjectName(QString::fromUtf8("hboxLayout40"));
        hboxLayout41 = new QHBoxLayout();
        hboxLayout41->setSpacing(4);
        hboxLayout41->setObjectName(QString::fromUtf8("hboxLayout41"));
        vboxLayout45 = new QVBoxLayout();
        vboxLayout45->setSpacing(4);
        vboxLayout45->setObjectName(QString::fromUtf8("vboxLayout45"));
        hboxLayout42 = new QHBoxLayout();
        hboxLayout42->setSpacing(6);
        hboxLayout42->setContentsMargins(0, 0, 0, 0);
        hboxLayout42->setObjectName(QString::fromUtf8("hboxLayout42"));
        PhiClockLabel_2_2_2 = new QLabel(feBox);
        PhiClockLabel_2_2_2->setObjectName(QString::fromUtf8("PhiClockLabel_2_2_2"));
        PhiClockLabel_2_2_2->setWordWrap(false);

        hboxLayout42->addWidget(PhiClockLabel_2_2_2);

        TOTmode = new QComboBox(feBox);
        TOTmode->setObjectName(QString::fromUtf8("TOTmode"));

        hboxLayout42->addWidget(TOTmode);


        vboxLayout45->addLayout(hboxLayout42);

        hboxLayout43 = new QHBoxLayout();
        hboxLayout43->setSpacing(4);
        hboxLayout43->setObjectName(QString::fromUtf8("hboxLayout43"));
        TextLabel1_9 = new QLabel(feBox);
        TextLabel1_9->setObjectName(QString::fromUtf8("TextLabel1_9"));
        TextLabel1_9->setWordWrap(false);

        hboxLayout43->addWidget(TextLabel1_9);

        minTOT = new QSpinBox(feBox);
        minTOT->setObjectName(QString::fromUtf8("minTOT"));
        minTOT->setMaximum(255);

        hboxLayout43->addWidget(minTOT);


        vboxLayout45->addLayout(hboxLayout43);

        hboxLayout44 = new QHBoxLayout();
        hboxLayout44->setSpacing(4);
        hboxLayout44->setObjectName(QString::fromUtf8("hboxLayout44"));
        TextLabel1_9_2_2 = new QLabel(feBox);
        TextLabel1_9_2_2->setObjectName(QString::fromUtf8("TextLabel1_9_2_2"));
        TextLabel1_9_2_2->setWordWrap(false);

        hboxLayout44->addWidget(TextLabel1_9_2_2);

        dblTOT = new QSpinBox(feBox);
        dblTOT->setObjectName(QString::fromUtf8("dblTOT"));
        dblTOT->setMaximum(255);

        hboxLayout44->addWidget(dblTOT);


        vboxLayout45->addLayout(hboxLayout44);

        hboxLayout45 = new QHBoxLayout();
        hboxLayout45->setSpacing(6);
        hboxLayout45->setContentsMargins(0, 0, 0, 0);
        hboxLayout45->setObjectName(QString::fromUtf8("hboxLayout45"));
        PhiClockLabel_2_2_2_2 = new QLabel(feBox);
        PhiClockLabel_2_2_2_2->setObjectName(QString::fromUtf8("PhiClockLabel_2_2_2_2"));
        PhiClockLabel_2_2_2_2->setWordWrap(false);

        hboxLayout45->addWidget(PhiClockLabel_2_2_2_2);

        TOTLEmode = new QComboBox(feBox);
        TOTLEmode->setObjectName(QString::fromUtf8("TOTLEmode"));

        hboxLayout45->addWidget(TOTLEmode);


        vboxLayout45->addLayout(hboxLayout45);


        hboxLayout41->addLayout(vboxLayout45);

        spacer46 = new QSpacerItem(31, 21, QSizePolicy::Fixed, QSizePolicy::Minimum);

        hboxLayout41->addItem(spacer46);

        vboxLayout46 = new QVBoxLayout();
        vboxLayout46->setSpacing(4);
        vboxLayout46->setObjectName(QString::fromUtf8("vboxLayout46"));
        spacer133_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout46->addItem(spacer133_2);

        hboxLayout46 = new QHBoxLayout();
        hboxLayout46->setSpacing(6);
        hboxLayout46->setContentsMargins(0, 0, 0, 0);
        hboxLayout46->setObjectName(QString::fromUtf8("hboxLayout46"));
        TextLabel2 = new QLabel(feBox);
        TextLabel2->setObjectName(QString::fromUtf8("TextLabel2"));
        TextLabel2->setWordWrap(false);

        hboxLayout46->addWidget(TextLabel2);

        PhiClock = new QComboBox(feBox);
        PhiClock->setObjectName(QString::fromUtf8("PhiClock"));

        hboxLayout46->addWidget(PhiClock);

        vboxLayout46->addLayout(hboxLayout46);

        spacer133 = new QSpacerItem(20, 76, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout46->addItem(spacer133);

        hboxLayout41->addLayout(vboxLayout46);


        hboxLayout40->addLayout(hboxLayout41);


        vboxLayout42->addWidget(feBox);

        ErrorBox = new QGroupBox(TabPage);
        ErrorBox->setObjectName(QString::fromUtf8("ErrorBox"));
        hboxLayout47 = new QHBoxLayout(ErrorBox);
        hboxLayout47->setSpacing(4);
        hboxLayout47->setContentsMargins(11, 11, 11, 11);
        hboxLayout47->setObjectName(QString::fromUtf8("hboxLayout47"));
        hboxLayout48 = new QHBoxLayout();
        hboxLayout48->setSpacing(4);
        hboxLayout48->setObjectName(QString::fromUtf8("hboxLayout48"));
        vboxLayout47 = new QVBoxLayout();
        vboxLayout47->setSpacing(4);
        vboxLayout47->setObjectName(QString::fromUtf8("vboxLayout47"));
        hardReset = new QCheckBox(ErrorBox);
        hardReset->setObjectName(QString::fromUtf8("hardReset"));

        vboxLayout47->addWidget(hardReset);

        softReset = new QCheckBox(ErrorBox);
        softReset->setObjectName(QString::fromUtf8("softReset"));

        vboxLayout47->addWidget(softReset);

        shortSync = new QCheckBox(ErrorBox);
        shortSync->setObjectName(QString::fromUtf8("shortSync"));

        vboxLayout47->addWidget(shortSync);

        mediumSync = new QCheckBox(ErrorBox);
        mediumSync->setObjectName(QString::fromUtf8("mediumSync"));

        vboxLayout47->addWidget(mediumSync);


        hboxLayout48->addLayout(vboxLayout47);

        vboxLayout48 = new QVBoxLayout();
        vboxLayout48->setSpacing(4);
        vboxLayout48->setObjectName(QString::fromUtf8("vboxLayout48"));
        longSync = new QCheckBox(ErrorBox);
        longSync->setObjectName(QString::fromUtf8("longSync"));

        vboxLayout48->addWidget(longSync);

        sendBCR = new QCheckBox(ErrorBox);
        sendBCR->setObjectName(QString::fromUtf8("sendBCR"));

        vboxLayout48->addWidget(sendBCR);

        sendECR = new QCheckBox(ErrorBox);
        sendECR->setObjectName(QString::fromUtf8("sendECR"));

        vboxLayout48->addWidget(sendECR);


        hboxLayout48->addLayout(vboxLayout48);


        hboxLayout47->addLayout(hboxLayout48);


        vboxLayout42->addWidget(ErrorBox);


        vboxLayout41->addLayout(vboxLayout42);

        spacer38 = new QSpacerItem(20, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout41->addItem(spacer38);


        vboxLayout40->addLayout(vboxLayout41);

        tabwidget->addTab(TabPage, QString());
        TabPage1 = new QWidget();
        TabPage1->setObjectName(QString::fromUtf8("TabPage1"));
        vboxLayout49 = new QVBoxLayout(TabPage1);
        vboxLayout49->setSpacing(4);
        vboxLayout49->setContentsMargins(11, 11, 11, 11);
        vboxLayout49->setObjectName(QString::fromUtf8("vboxLayout49"));
        configParameterTable = new QTableWidget(TabPage1);
        configParameterTable->setObjectName(QString::fromUtf8("configParameterTable"));
        configParameterTable->setColumnCount(4);
#if QT_VERSION < 0x050000
        configParameterTable->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
#else
        configParameterTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
#endif
        configParameterTable->verticalHeader()->setMinimumSectionSize(200);
       // configParameterTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
        configParameterTable->setContextMenuPolicy(Qt::CustomContextMenu);

        vboxLayout49->addWidget(configParameterTable);

        tabwidget->addTab(TabPage1, QString());

        vboxLayout->addWidget(tabwidget);


        retranslateUi(PixScanPanelBase);
        //TODO move to new QT5 syntax, not straigthforward
        QObject::connect(fixedVCALBox, SIGNAL(toggled(bool)), PixScanPanelBase, SLOT(fixVCAL(bool)));
        QObject::connect(loop0LoadPoints, SIGNAL(clicked()), PixScanPanelBase, SLOT(loadLoop0Pts()));
        QObject::connect(loop1LoadPoints, SIGNAL(clicked()), PixScanPanelBase, SLOT(loadLoop1Pts()));
        QObject::connect(loop2LoadPoints, SIGNAL(clicked()), PixScanPanelBase, SLOT(loadLoop2Pts()));
        //QObject::connect(configParameterTable, SIGNAL(customContextMenuRequested(const QPoint&)), PixScanPanelBase, SLOT(openTableMenu(const QPoint&)));
        //QObject::connect(configParameterTable, SIGNAL(cellChanged(int,int)), PixScanPanelBase, SLOT(readFromHandlersFromTable(int,int)));

        QObject::connect(setVcal, SIGNAL(valueChanged(int)), PixScanPanelBase, SLOT(VcalChanged(int)));
	QObject::connect(m_showLoopIndex, SIGNAL(valueChanged(int)), PixScanPanelBase, SLOT(showLoop(int)));

        QObject::connect(useTuningPointsFile, &QCheckBox::toggled, tuningPointsSelect, &QPushButton::setEnabled);
	QObject::connect(tuningPointsSelect, SIGNAL(clicked()), PixScanPanelBase, SLOT(loadTuningPoints() ));
	QObject::connect(useTuningPointsFile, &QCheckBox::toggled, tuningPointsFile, &QPushButton::setEnabled);

        QObject::connect(useEmulator, &QCheckBox::toggled, setHitEmu, &QSpinBox::setEnabled);
        QObject::connect(selfTrigger, &QCheckBox::toggled, superGrpDelay, &QSpinBox::setEnabled);
        QObject::connect(fixedSdelBox, &QCheckBox::toggled, strDelay, &QSpinBox::setEnabled);
        QObject::connect(fixedSrangeBox, &QCheckBox::toggled, strDelRange, &QSpinBox::setEnabled);
        QObject::connect(fixedVCALBox, &QCheckBox::toggled, setVcal, &QSpinBox::setDisabled);
        QObject::connect(loop0Regular, &QCheckBox::toggled, loop0LoadPoints, &QSpinBox::setDisabled);
        QObject::connect(loop1Regular, &QCheckBox::toggled, loop1LoadPoints, &QSpinBox::setDisabled);
        QObject::connect(loop2Regular, &QCheckBox::toggled, loop2LoadPoints, &QSpinBox::setDisabled);
        QObject::connect(loop0Regular, &QCheckBox::toggled, loop0Start, &QSpinBox::setEnabled);
        QObject::connect(loop0Regular, &QCheckBox::toggled, loop0Stop, &QSpinBox::setEnabled);
        QObject::connect(loop1Regular, &QCheckBox::toggled, loop1Start, &QSpinBox::setEnabled);
        QObject::connect(loop1Regular, &QCheckBox::toggled, loop1Stop, &QSpinBox::setEnabled);
        QObject::connect(loop2Regular, &QCheckBox::toggled, loop2Start, &QSpinBox::setEnabled);
        QObject::connect(loop2Regular, &QCheckBox::toggled, loop2Stop, &QSpinBox::setEnabled);
        QObject::connect(overwL1delBox,&QCheckBox::toggled, l1Delay, &QSpinBox::setEnabled);

	//QObject::connect(m_showLoopIndex, SIGNAL(valueChanged(int)), PixScanPanelBase, SLOT(editLoop(int)));
        //QObject::connect(m_showLoopIndex, SIGNAL(valueChanged(int)), loopBoxStack, SLOT(raiseWidget(int)));
	//QObject::connect(configParameterTable, SIGNAL(customContextMenuRequested(const QPoint&)), PixScanPanelBase, SLOT(rightClickCell()));

        tabwidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(PixScanPanelBase);
    } // setupUi

    void retranslateUi(QWidget *PixScanPanelBase)
    {
        PixScanPanelBase->setWindowTitle(QApplication::translate("PixScanPanelBase", "Scan Panel", 0));
        tabwidget->setTabText(tabwidget->indexOf(tab), QApplication::translate("PixScanPanelBase", "Basic Pars", 0));
        stageAdvBox->setText(QApplication::translate("PixScanPanelBase", "Innermost loop is the mask staging", 0));
        maskOnDsp->setText(QApplication::translate("PixScanPanelBase", "Mask is staged on PPC", 0));
        loopSetLabel->setText(QApplication::translate("PixScanPanelBase", "Show settings for loop ", 0));
        Loop0Box->setTitle(QApplication::translate("PixScanPanelBase", "Loop 0", 0));
        loop0Active->setText(QApplication::translate("PixScanPanelBase", "active", 0));
        loop0DspProc->setText(QApplication::translate("PixScanPanelBase", "PPC processing", 0));
        loop0FromLabel->setText(QApplication::translate("PixScanPanelBase", "Scan from", 0));
        loop0ToLabel->setText(QApplication::translate("PixScanPanelBase", "to", 0));
        loop0StepsLabel->setText(QApplication::translate("PixScanPanelBase", "#steps", 0));
        loop0Regular->setText(QApplication::translate("PixScanPanelBase", "regularly spaced points", 0));
        loop0Free->setText(QApplication::translate("PixScanPanelBase", "values free loop", 0));
        loop0PtlLabel->setText(QApplication::translate("PixScanPanelBase", "scan points", 0));
        loop0LoadPoints->setText(QApplication::translate("PixScanPanelBase", "Load", 0));
        loop0PostActLabel->setText(QApplication::translate("PixScanPanelBase", "Post-loop action:", 0));
        loop0OnDsp->setText(QApplication::translate("PixScanPanelBase", "on PPC", 0));
        loopFitMethodLabel->setText(QApplication::translate("PixScanPanelBase", "Fit method", 0));
        
        Loop1Box->setTitle(QApplication::translate("PixScanPanelBase", "Loop 1", 0));
        loop1Active->setText(QApplication::translate("PixScanPanelBase", "active", 0));
        loop1DspProc->setText(QApplication::translate("PixScanPanelBase", "PPC processing", 0));
        loop1FromLabel->setText(QApplication::translate("PixScanPanelBase", "Scan from", 0));
        loop1ToLabel->setText(QApplication::translate("PixScanPanelBase", "to", 0));
        loop1StepsLabel->setText(QApplication::translate("PixScanPanelBase", "#steps", 0));
        loop1Regular->setText(QApplication::translate("PixScanPanelBase", "regularly spaced points", 0));
        loop1Free->setText(QApplication::translate("PixScanPanelBase", "values free loop", 0));
        loop1PtlLabel->setText(QApplication::translate("PixScanPanelBase", "scan points", 0));
        loop1LoadPoints->setText(QApplication::translate("PixScanPanelBase", "Load", 0));
        loop1PostActLabel->setText(QApplication::translate("PixScanPanelBase", "Post-loop action:", 0));
        loop1OnDsp->setText(QApplication::translate("PixScanPanelBase", "on PPC", 0));
        Loop2Box->setTitle(QApplication::translate("PixScanPanelBase", "Loop 2", 0));
        loop2Active->setText(QApplication::translate("PixScanPanelBase", "active", 0));
        loop2DspProc->setText(QApplication::translate("PixScanPanelBase", "PPC processing", 0));
        loop2FromLabel->setText(QApplication::translate("PixScanPanelBase", "Scan from", 0));
        loop2ToLabel->setText(QApplication::translate("PixScanPanelBase", "to", 0));
        loop2StepsLabel->setText(QApplication::translate("PixScanPanelBase", "#steps", 0));
        loop2Regular->setText(QApplication::translate("PixScanPanelBase", "regularly spaced points", 0));
        loop2Free->setText(QApplication::translate("PixScanPanelBase", "values free loop", 0));
        loop2PtlLabel->setText(QApplication::translate("PixScanPanelBase", "scan points", 0));
        loop2LoadPoints->setText(QApplication::translate("PixScanPanelBase", "Load", 0));
        loop2PostActLabel->setText(QApplication::translate("PixScanPanelBase", "Post-loop action:", 0));
        loop2OnDsp->setText(QApplication::translate("PixScanPanelBase", "on PPC", 0));
        textLabel1_8->setText(QApplication::translate("PixScanPanelBase", "Threshold target value for TDAC tuning [e]", 0));
        totchargelabel->setText(QApplication::translate("PixScanPanelBase", "ToT target value for FDAC tuning [ToT]", 0));
        textLabel3_2->setText(QApplication::translate("PixScanPanelBase", "Reference charge for FDAC tuning and ToT verify injection [e]", 0));
        tabwidget->setTabText(tabwidget->indexOf(tab1), QApplication::translate("PixScanPanelBase", "Scan Pars", 0));
        histoFillBox->setTitle(QApplication::translate("PixScanPanelBase", "Filling", 0));
        occLabel->setText(QApplication::translate("PixScanPanelBase", "occupancy histos:", 0));
        lvl1Label->setText(QApplication::translate("PixScanPanelBase", "LVL1 histos:", 0));
        totmeanLabel->setText(QApplication::translate("PixScanPanelBase", "ToT mean histos:", 0));
        totsigLabel->setText(QApplication::translate("PixScanPanelBase", "ToT sigma histos:", 0));
        fdactlabel->setText(QApplication::translate("PixScanPanelBase", "FDAC T histos:", 0));
        fdactotlabel->setText(QApplication::translate("PixScanPanelBase", "FDAC ToT histos:", 0));
        gdactlabel->setText(QApplication::translate("PixScanPanelBase", "GDAC T histos:", 0));
        gdacthrlabel->setText(QApplication::translate("PixScanPanelBase", "GDAC thr histos:", 0));
        iftlabel->setText(QApplication::translate("PixScanPanelBase", "IF T histos:", 0));
        iftotlabel->setText(QApplication::translate("PixScanPanelBase", "IF ToT histos:", 0));
        scurvechi2label->setText(QApplication::translate("PixScanPanelBase", "S-curve chi2 histos:", 0));
        scurvemeanlabel->setText(QApplication::translate("PixScanPanelBase", "S-curve mean histos:", 0));
        scurvesigmalabel->setText(QApplication::translate("PixScanPanelBase", "S-curve sigma histos:", 0));
        tdactlabel->setText(QApplication::translate("PixScanPanelBase", "TDAC T histos:", 0));
        tdacthrlabel->setText(QApplication::translate("PixScanPanelBase", "TDAC thr histos:", 0));
        twlabel->setText(QApplication::translate("PixScanPanelBase", "timewalk histos:", 0));
        rd0label->setText(QApplication::translate("PixScanPanelBase", "Boc Raw Data 0:", 0));
        rd1label->setText(QApplication::translate("PixScanPanelBase", "Boc Raw Data 1:", 0));
        rd2label->setText(QApplication::translate("PixScanPanelBase", "Boc Raw Data 2:", 0));
        rdd1label->setText(QApplication::translate("PixScanPanelBase", "Boc Raw Data Diff 1:", 0));
        rdd2label->setText(QApplication::translate("PixScanPanelBase", "Boc Raw Data Diff 2:", 0));
        rdrlabel->setText(QApplication::translate("PixScanPanelBase", "Boc Raw Data Ref:", 0));
        fcllabel->setText(QApplication::translate("PixScanPanelBase", "FMT counter link map:", 0));
        fctlabel->setText(QApplication::translate("PixScanPanelBase", "FMT counters:", 0));
        occFill->setText(QApplication::translate("PixScanPanelBase", "fill", 0));
        lvl1Fill->setText(QApplication::translate("PixScanPanelBase", "fill", 0));
        totmeanFill->setText(QApplication::translate("PixScanPanelBase", "fill", 0));
        totsigFill->setText(QApplication::translate("PixScanPanelBase", "fill", 0));
        fdactfill->setText(QApplication::translate("PixScanPanelBase", "fill", 0));
        fdactotfill->setText(QApplication::translate("PixScanPanelBase", "fill", 0));
        gdactfill->setText(QApplication::translate("PixScanPanelBase", "fill", 0));
        gdacthrfill->setText(QApplication::translate("PixScanPanelBase", "fill", 0));
        iftfill->setText(QApplication::translate("PixScanPanelBase", "fill", 0));
        iftotfill->setText(QApplication::translate("PixScanPanelBase", "fill", 0));
        scurvechi2fill->setText(QApplication::translate("PixScanPanelBase", "fill", 0));
        scurvemeanfill->setText(QApplication::translate("PixScanPanelBase", "fill", 0));
        scurvesigmafill->setText(QApplication::translate("PixScanPanelBase", "fill", 0));
        tdactfill->setText(QApplication::translate("PixScanPanelBase", "fill", 0));
        tdacthrfill->setText(QApplication::translate("PixScanPanelBase", "fill", 0));
        twfill->setText(QApplication::translate("PixScanPanelBase", "fill", 0));
        rd0fill->setText(QApplication::translate("PixScanPanelBase", "fill", 0));
        rd1fill->setText(QApplication::translate("PixScanPanelBase", "fill", 0));
        rd2fill->setText(QApplication::translate("PixScanPanelBase", "fill", 0));
        rdd1fill->setText(QApplication::translate("PixScanPanelBase", "fill", 0));
        rdd2fill->setText(QApplication::translate("PixScanPanelBase", "fill", 0));
        rdrfill->setText(QApplication::translate("PixScanPanelBase", "fill", 0));
        fclfill->setText(QApplication::translate("PixScanPanelBase", "fill", 0));
        fctfill->setText(QApplication::translate("PixScanPanelBase", "fill", 0));
        occKeep->setText(QApplication::translate("PixScanPanelBase", "keep", 0));
        lvl1Keep->setText(QApplication::translate("PixScanPanelBase", "keep", 0));
        totmeanKeep->setText(QApplication::translate("PixScanPanelBase", "keep", 0));
        totsigKeep->setText(QApplication::translate("PixScanPanelBase", "keep", 0));
        fdactkeep->setText(QApplication::translate("PixScanPanelBase", "keep", 0));
        fdactotkeep->setText(QApplication::translate("PixScanPanelBase", "keep", 0));
        gdactkeep->setText(QApplication::translate("PixScanPanelBase", "keep", 0));
        gdacthrkeep->setText(QApplication::translate("PixScanPanelBase", "keep", 0));
        iftkeep->setText(QApplication::translate("PixScanPanelBase", "keep", 0));
        iftotkeep->setText(QApplication::translate("PixScanPanelBase", "keep", 0));
        scurvechi2keep->setText(QApplication::translate("PixScanPanelBase", "keep", 0));
        scurvemeankeep->setText(QApplication::translate("PixScanPanelBase", "keep", 0));
        scurvesigmakeep->setText(QApplication::translate("PixScanPanelBase", "keep", 0));
        tdactkeep->setText(QApplication::translate("PixScanPanelBase", "keep", 0));
        tdacthrkeep->setText(QApplication::translate("PixScanPanelBase", "keep", 0));
        twkeep->setText(QApplication::translate("PixScanPanelBase", "keep", 0));
        rd0keep->setText(QApplication::translate("PixScanPanelBase", "keep", 0));
        rd1keep->setText(QApplication::translate("PixScanPanelBase", "keep", 0));
        rd2keep->setText(QApplication::translate("PixScanPanelBase", "keep", 0));
        rdd1keep->setText(QApplication::translate("PixScanPanelBase", "keep", 0));
        rdd2keep->setText(QApplication::translate("PixScanPanelBase", "keep", 0));
        rdrkeep->setText(QApplication::translate("PixScanPanelBase", "keep", 0));
        fclkeep->setText(QApplication::translate("PixScanPanelBase", "keep", 0));
        fctkeep->setText(QApplication::translate("PixScanPanelBase", "keep", 0));
        tabwidget->setTabText(tabwidget->indexOf(TabPage11), QApplication::translate("PixScanPanelBase", "Histos", 0));
        mccBox->setTitle(QApplication::translate("PixScanPanelBase", "MCC scan settings", 0));
        BwidthLabel->setText(QApplication::translate("PixScanPanelBase", "Output Bandwidth", 0));
        MCC_ErrorFlag->setText(QApplication::translate("PixScanPanelBase", "MCC Register error check", 0));
        MCC_FECheck->setText(QApplication::translate("PixScanPanelBase", "individual check FE enable", 0));
        MCC_TimeStampComp->setText(QApplication::translate("PixScanPanelBase", "FE time stamp comparison check", 0));
        feBox->setTitle(QApplication::translate("PixScanPanelBase", "FE scan settings", 0));
        PhiClockLabel_2_2_2->setText(QApplication::translate("PixScanPanelBase", "TOT threshold mode [FEI3 ReadMode]", 0));
        TOTmode->clear();
        TOTmode->insertItems(0, QStringList()
         << QApplication::translate("PixScanPanelBase", "Both off [ReadMode = 0]", 0)
         << QApplication::translate("PixScanPanelBase", "Minimum only [ReadMode = 1] ", 0)
         << QApplication::translate("PixScanPanelBase", "Doubling only [ReadMode = 2]", 0)
         << QApplication::translate("PixScanPanelBase", "Both [ReadMode = 3]", 0)
        );
        TextLabel1_9->setText(QApplication::translate("PixScanPanelBase", "TOT min threshold [FEI3 THRMIN // FEI4 HDC]", 0));
        TextLabel1_9_2_2->setText(QApplication::translate("PixScanPanelBase", "TOT doubling thresh [FEI3 THRDUB]", 0));
        PhiClockLabel_2_2_2_2->setText(QApplication::translate("PixScanPanelBase", "TOT LE mode [FEI3 EOC_MUX]", 0));
        TOTLEmode->clear();
        TOTLEmode->insertItems(0, QStringList()
         << QApplication::translate("PixScanPanelBase", "Timestamp [EOC_MUX = 0]", 0)
         << QApplication::translate("PixScanPanelBase", "Leading Edge [EOC_MUX = 1]", 0)
        );
        TextLabel2->setText(QApplication::translate("PixScanPanelBase", "Phi clock [FEI3 CEU_Clock]", 0));
        PhiClock->clear();
        PhiClock->insertItems(0, QStringList()
         << QApplication::translate("PixScanPanelBase", "unknown [?]", 0)
         << QApplication::translate("PixScanPanelBase", "OFF [CEU_Clock = 0]", 0)
         << QApplication::translate("PixScanPanelBase", "10 MHz [CEU_Clock = 1]", 0)
         << QApplication::translate("PixScanPanelBase", "20 MHz [CEU_Clock = 2]", 0)
         << QApplication::translate("PixScanPanelBase", "40 MHz [CEU_Clock = 3]", 0)
        );
        ErrorBox->setTitle(QApplication::translate("PixScanPanelBase", "Resets", 0));
        hardReset->setText(QApplication::translate("PixScanPanelBase", "FE Hard Reset", 0));
        softReset->setText(QApplication::translate("PixScanPanelBase", "FE Soft Reset", 0));
        shortSync->setText(QApplication::translate("PixScanPanelBase", "FE Short Sync (L1ID)", 0));
        mediumSync->setText(QApplication::translate("PixScanPanelBase", "FE Medium Sync (BCID)", 0));
        longSync->setText(QApplication::translate("PixScanPanelBase", "FE Long Sync (RST)", 0));
        sendBCR->setText(QApplication::translate("PixScanPanelBase", "MCC BCR", 0));
        sendECR->setText(QApplication::translate("PixScanPanelBase", "MCC ECR", 0));
        tabwidget->setTabText(tabwidget->indexOf(TabPage), QApplication::translate("PixScanPanelBase", "MCC/FE", 0));
        tabwidget->setTabText(tabwidget->indexOf(TabPage1), QApplication::translate("PixScanPanelBase", "Table", 0));
    } // retranslateUi

};

namespace Ui {
    class PixScanPanelBase: public Ui_PixScanPanelBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PIXSCANPANELBASE_H
