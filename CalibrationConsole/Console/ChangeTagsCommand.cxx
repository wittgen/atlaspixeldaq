#include "ChangeTagsCommand.h"
#include "ConsoleEngine.h" 
#include "DbTagsPanel.h"
#include <ApplyDisableCommand.h>
#include <CommandExecutor.h>
#include <InitialiseRodCommand.h>

namespace PixCon {


  const std::string ChangeTagsCommand::s_name("Change action tags");

  bool ChangeTagsKit::s_registered = ChangeTagsKit::registerKit();

  ChangeTagsCommand::~ChangeTagsCommand(){}

  PixCon::Result_t ChangeTagsCommand::execute(IStatusMessageListener &listener) {
    PixCon::Result_t ret;
    std::cout << "INFO [ChangeTagsCommand::execute] Executing..." << std::endl;
    try {
      // first save the map of actions to reallocate
      std::map<std::string, std::vector<std::string> > rodList = m_engine->getRodList();
      if (m_restoreDisable.get()) {

	std::shared_ptr<PixLib::PixDisable> pix_disable;

	pix_disable = m_engine->calibrationData().getPixDisable(kCurrent,0);
	pix_disable->config().dump(std::cout);
	pix_disable->config().name("TempDis");

	// copy disable to the disable which is applied by a subsequent command.
	m_restoreDisable->config() = pix_disable->config();
      }

      m_engine->deallocate();

      if (!m_isManager.get()) {
	m_isManager=std::make_unique<PixLib::PixISManager>(m_engine->partition().name(),"PixelRunParams");
      }
      m_isManager->publish("DataTakingConfig-IdTag",m_newIdTag);
      m_isManager->publish("DataTakingConfig-ConnTag",m_newConnTag);
      m_isManager->publish("DataTakingConfig-CfgTag",m_newCfgTag);
      m_isManager->publish("DataTakingConfig-ModCfgTag",m_newModCfgTag);
      {
	std::string message("Change tags to : ");
	message+= m_newIdTag;
	message+= ": ";
	message+=m_newConnTag;
	message+= ", ";
	message+=m_newCfgTag;
	if (m_newCfgTag != m_newModCfgTag) {
	  message+=", ";
	  message+=m_newModCfgTag;
	}
	listener.statusMessage(message);
      }

      //@todo somehow guarantee that the values are actually published.
      std::cout << "INFO [ChangeTagsCommand::executing] update connectivity from IS."  << std::endl;
      m_engine->updateConnectivityFromIS();
      m_engine->updateConnectivity();
      std::cout << "INFO [ChangeTagsCommand::executing] allocate."  << std::endl;
      m_engine->allocate(rodList);
      if (m_engine->haveAllocatedActions()) {
      std::cout << "INFO [ChangeTagsCommand::executing] initialise."  << std::endl;

      // @todo this should be moved to configure
      // now done by subseqeuent commands in the command sequence.
      //	ICommand *init_rod_command = initialiseRodCommand(m_engine);
      //m_engine->executor()->appendCommand(init_rod_command,NULL,0);
      //ICommand *reset_viset_command = resetVisetCommand(m_engine);
      //m_engine->executor()->appendCommand(reset_viset_command,NULL,0);

	std::cout << "INFO [ChangeTagsCommand::executing] apply disable."  << std::endl;
	// @todo here also a command should be used
	// right now nothing guarantees that
	// apply disable is executed after init_rodo
	//m_engine->applyDisable();

	m_engine->statusChangeTransceiver()->signalConnectivityHasChanged();
	std::cout << "INFO [ChangeTagsCommand::executing] new_tags " << m_newIdTag << ":" << m_newConnTag << ";" << m_newCfgTag << "," << m_newModCfgTag << std::endl;
	ret.setSuccess();
      }
      else {
	ret.setFailure();
      }
      
    }
    catch(PixA::BaseException &err) {
      std::cout << "INFO [ChangeTagsCommand::execute] Exception caught : "  << err.getDescriptor() << std::endl;   
      ret.setFailure();
    }
    catch(std::exception &err) {
      std::cout << "INFO [ChangeTagsCommand::execute] Exception caught : "  << err.what() << std::endl;   
      ret.setFailure();
    }
    catch(...) {
      std::cout << "INFO [ChangeTagsCommand::execute] Exception caught" << std::endl;   
      ret.setFailure();
    }
    std::cout << "INFO [ChangeTagsCommand::execute] Finished" << std::endl;
    return ret;
  }

  bool ChangeTagsCommand::configure() {
    PixA::ConnectivityRef conn(m_engine->getConn());
    std::string current_id_tag;
    std::string current_conn_tag;
    std::string current_cfg_tag;
    std::string current_mod_cfg_tag;

    if (conn) {
      current_id_tag = conn.tag(PixA::IConnectivity::kId);
      current_conn_tag = conn.tag(PixA::IConnectivity::kConn);
      current_cfg_tag = conn.tag(PixA::IConnectivity::kConfig);
      current_mod_cfg_tag = conn.tag(PixA::IConnectivity::kModConfig);
    }
    
    DbTagsPanel tags_chooser(current_id_tag, current_conn_tag, current_cfg_tag, current_mod_cfg_tag,
			     NULL,m_engine->partition().name(),"PixelDbServer");
    try {
      if (tags_chooser.exec() == QDialog::Accepted){
	m_newIdTag     = tags_chooser.getObjectTag();
	m_newConnTag   = tags_chooser.getConnectivityTag();
	m_newCfgTag    = tags_chooser.getConfigurationTag();
	m_newModCfgTag = tags_chooser.getModuleConfigurationTag();
	std::cout << "Chosen tags " << m_newIdTag << " " << m_newConnTag << " " << m_newCfgTag << " " << m_newModCfgTag << std::endl;
	return true;
      }
    }
    catch(...) {
      std::cout << "INFO [ChangeTagsCommand::configure] Exception caught" << std::endl;
      return false;
    }
    return false;
  }

    PixCon::IConfigurableCommand *ChangeTagsKit::createCommand(const std::string &/*command*/, const std::shared_ptr<ConsoleEngine> &engine) const {
      assert(engine.get());

      std::shared_ptr<PixLib::PixDisable> to_be_filled_disable(new PixLib::PixDisable("RestoreDisable"));
      std::shared_ptr<const PixLib::PixDisable> restore_disable(to_be_filled_disable);

      ConfigurableCommandSequence *command_sequence = new ConfigurableCommandSequence("Change Tags");

      command_sequence->add(new ChangeTagsCommand(engine, to_be_filled_disable),NULL);
      command_sequence->add( initialiseRodCommand(engine) );
      //command_sequence->add( resetVisetCommand(engine) );
      command_sequence->add( DisableCommand::create(engine,restore_disable,true /*changes only*/, DisableCommand::kApplyAll) );

      std::cout << "INFO [ChangeTagsKit::createCommand] is CommandSequence ? " << static_cast<void *>(dynamic_cast<CommandSequence *>(command_sequence))
		<< std::endl;
      return command_sequence;
    }


}
