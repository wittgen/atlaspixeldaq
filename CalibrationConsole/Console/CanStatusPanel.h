// Dear emacs, this is -*-c++-*-
#ifndef _PixCon_CanStatusPanel_h_
#define _PixCon_CanStatusPanel_h_

#include "ui_CanStatusPanelBase.h"
#include "qevent.h"

#include <memory>
#include <memory>
#include <vector>

class QApplication;

namespace PixCon {

  class CalibrationDataPalette;

  class CanStatusPanel : public QDialog, public Ui_CanStatusPanelBase
  {
    Q_OBJECT
  public:
    CanStatusPanel( QApplication &app,
		    const std::string &partition_name,
		    const std::shared_ptr<PixCon::CalibrationDataPalette> &palette,
		    QWidget* parent = 0 );

    ~CanStatusPanel();

    void initiateShutdown();
    void shutdown();

    class QueueStatusEvent : public QEvent
    {
    public:
      QueueStatusEvent(unsigned int jobs_waiting, unsigned int jobs_running, unsigned int empty_slots) 
	: QEvent(QEvent::User),
	  m_jobsRunning(jobs_running),
	  m_jobsWaiting(jobs_waiting),
	  m_emptySlots(empty_slots)
      {}

      unsigned int runningJobs() const { return m_jobsRunning; }
      unsigned int waitingJobs() const { return m_jobsWaiting; }
      unsigned int emptySlots() const  { return m_emptySlots; }

    private:
      unsigned int m_jobsRunning;
      unsigned int m_jobsWaiting;
      unsigned int m_emptySlots;
    };

    class SchedulerStatusEvent : public QEvent
    {
    public:
      enum ESchedulerStatus {kUnknown, kOk, kCommError, kInternalError, kNoWorker, kNoScheduler, kNStati};

      SchedulerStatusEvent(ESchedulerStatus status) 
	: QEvent(QEvent::User),
	  m_status(status)
      {}

      ESchedulerStatus status() const { return m_status; }

      const char *statusName() const { return statusName(status());}

      static const char *statusName(ESchedulerStatus status) { return (status < kNStati ? s_statusName[status] :s_statusName[kUnknown] ); }

    private:
      static const char *s_statusName[SchedulerStatusEvent::kNStati];
      ESchedulerStatus m_status;
    };

    class IManitu {
    public:
      virtual ~IManitu() {}

      virtual void resetQueue() = 0;
      virtual void restartWorker() = 0;
      virtual void kill() = 0;

      virtual void shutdownMonitoring() = 0;
      virtual void waitShutdownMonitoring() = 0;

      virtual void setLogLevel(int log_level) = 0;

    };


    bool event(QEvent *an_event);
    void updateSchedulerStatus(SchedulerStatusEvent::ESchedulerStatus status);
    void updateQueueStatus(unsigned int jobs_waiting, unsigned int jobs_running, unsigned int empty_slots);

  signals:
    void statusMessage(const QString &);
  public slots:
    void changeLogLevel(int new_log_level);
    void resetCan();
    void killCan();
  private:
    std::shared_ptr<PixCon::CalibrationDataPalette> m_palette;
    QApplication                                     *m_app;
    std::unique_ptr<IManitu> m_manitu;
    unsigned int m_jobsRunning;
    unsigned int m_jobsWaiting;
    unsigned int m_emptySlots;

    SchedulerStatusEvent::ESchedulerStatus m_status;

    int          m_lastLogLevel;

    unsigned int mapStatus(SchedulerStatusEvent::ESchedulerStatus  status) {
      if (static_cast<unsigned int>(status)>=s_colorMap.size()) {
	status = SchedulerStatusEvent::kUnknown;
	if (static_cast<unsigned int>(status)>=s_colorMap.size()) {
	  return 0;
	}
      }
      return s_colorMap[status];
    }

    static std::vector<unsigned int> s_colorMap;
  };

  

}
#endif
