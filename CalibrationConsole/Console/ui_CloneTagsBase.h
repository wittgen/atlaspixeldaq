#ifndef UI_CLONETAGSBASE_H
#define UI_CLONETAGSBASE_H

#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QComboBox>
#include <QDialog>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QSpacerItem>
#include <QSplitter>
#include <QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_CloneTagsBase
{
public:
    QVBoxLayout *vboxLayout;
    QVBoxLayout *vboxLayout1;
    QVBoxLayout *vboxLayout2;
    QSplitter *splitter4;
    QLabel *textLabel1;
    QComboBox *domainBox;
    QSplitter *splitter41;
    QLabel *textLabel2;
    QComboBox *origBox;
    QSplitter *splitter5;
    QLabel *textLabel3;
    QComboBox *destBox;
    QSplitter *splitter6;
    QLabel *textLabel4;
    QLineEdit *pendEdit;
    QSpacerItem *spacer4;
    QHBoxLayout *hboxLayout;
    QPushButton *OkButton;
    QSpacerItem *spacer5;
    QPushButton *CancelButton;

    void setupUi(QDialog *CloneTagsBase)
    {
        if (CloneTagsBase->objectName().isEmpty())
            CloneTagsBase->setObjectName(QString::fromUtf8("CloneTagsBase"));
        CloneTagsBase->resize(494, 272);
        vboxLayout = new QVBoxLayout(CloneTagsBase);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        vboxLayout1 = new QVBoxLayout();
        vboxLayout1->setSpacing(6);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        vboxLayout2 = new QVBoxLayout();
        vboxLayout2->setSpacing(6);
        vboxLayout2->setObjectName(QString::fromUtf8("vboxLayout2"));
        splitter4 = new QSplitter(CloneTagsBase);
        splitter4->setObjectName(QString::fromUtf8("splitter4"));
        splitter4->setOrientation(Qt::Horizontal);
        textLabel1 = new QLabel(splitter4);
        textLabel1->setObjectName(QString::fromUtf8("textLabel1"));
        textLabel1->setWordWrap(false);
        splitter4->addWidget(textLabel1);
        domainBox = new QComboBox(splitter4);
        domainBox->setObjectName(QString::fromUtf8("domainBox"));
        splitter4->addWidget(domainBox);

        vboxLayout2->addWidget(splitter4);

        splitter41 = new QSplitter(CloneTagsBase);
        splitter41->setObjectName(QString::fromUtf8("splitter41"));
        splitter41->setOrientation(Qt::Horizontal);
        textLabel2 = new QLabel(splitter41);
        textLabel2->setObjectName(QString::fromUtf8("textLabel2"));
        textLabel2->setWordWrap(false);
        splitter41->addWidget(textLabel2);
        origBox = new QComboBox(splitter41);
        origBox->setObjectName(QString::fromUtf8("origBox"));
        splitter41->addWidget(origBox);

        vboxLayout2->addWidget(splitter41);

        splitter5 = new QSplitter(CloneTagsBase);
        splitter5->setObjectName(QString::fromUtf8("splitter5"));
        splitter5->setOrientation(Qt::Horizontal);
        textLabel3 = new QLabel(splitter5);
        textLabel3->setObjectName(QString::fromUtf8("textLabel3"));
        textLabel3->setWordWrap(false);
        splitter5->addWidget(textLabel3);
        destBox = new QComboBox(splitter5);
        destBox->setObjectName(QString::fromUtf8("destBox"));
        destBox->setEditable(true);
        splitter5->addWidget(destBox);

        vboxLayout2->addWidget(splitter5);

        splitter6 = new QSplitter(CloneTagsBase);
        splitter6->setObjectName(QString::fromUtf8("splitter6"));
        splitter6->setOrientation(Qt::Horizontal);
        textLabel4 = new QLabel(splitter6);
        textLabel4->setObjectName(QString::fromUtf8("textLabel4"));
        textLabel4->setWordWrap(false);
        splitter6->addWidget(textLabel4);
        pendEdit = new QLineEdit(splitter6);
        pendEdit->setObjectName(QString::fromUtf8("pendEdit"));
        splitter6->addWidget(pendEdit);

        vboxLayout2->addWidget(splitter6);


        vboxLayout1->addLayout(vboxLayout2);

        spacer4 = new QSpacerItem(20, 23, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout1->addItem(spacer4);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        OkButton = new QPushButton(CloneTagsBase);
        OkButton->setObjectName(QString::fromUtf8("OkButton"));

        hboxLayout->addWidget(OkButton);

        spacer5 = new QSpacerItem(161, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacer5);

        CancelButton = new QPushButton(CloneTagsBase);
        CancelButton->setObjectName(QString::fromUtf8("CancelButton"));

        hboxLayout->addWidget(CancelButton);


        vboxLayout1->addLayout(hboxLayout);


        vboxLayout->addLayout(vboxLayout1);


        retranslateUi(CloneTagsBase);
        QObject::connect(OkButton, SIGNAL(clicked()), CloneTagsBase, SLOT(accept()));
        QObject::connect(CancelButton, SIGNAL(clicked()), CloneTagsBase, SLOT(reject()));
        QObject::connect(domainBox, SIGNAL(activated(QString)), CloneTagsBase, SLOT(changeDomain(QString)));

        QMetaObject::connectSlotsByName(CloneTagsBase);
    } // setupUi

    void retranslateUi(QDialog *CloneTagsBase)
    {
        CloneTagsBase->setWindowTitle(QApplication::translate("CloneTagsBase", "DbServer Tags", 0));
        textLabel1->setText(QApplication::translate("CloneTagsBase", "Domain", 0));
        textLabel2->setText(QApplication::translate("CloneTagsBase", "Origin", 0));
        textLabel3->setText(QApplication::translate("CloneTagsBase", "Destination", 0));
        textLabel4->setText(QApplication::translate("CloneTagsBase", "Pending tag [\"_Tmp\" for temporary tags]", 0));
        pendEdit->setText(QApplication::translate("CloneTagsBase", "saveMe", 0));
        OkButton->setText(QApplication::translate("CloneTagsBase", "OK", 0));
        CancelButton->setText(QApplication::translate("CloneTagsBase", "Cancel", 0));
    } // retranslateUi

};

namespace Ui {
    class CloneTagsBase: public Ui_CloneTagsBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CLONETAGSBASE_H
