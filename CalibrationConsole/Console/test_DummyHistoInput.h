/* Dear emacs, this is -*-c++-*- */
#ifndef _PixCon_DummyHistoInput_H_
#define _PixCon_DummyHistoInput_H_

#include <stdexcept>
#include <map>

#include <DataContainer/HistoInfo_t.h>

class TFile;
class TDirectory;
class TH1;

namespace PixA {
  class ConfigHandle;
}

namespace PixCon {

  class DummyHistoInput
  {
  public:

    class AccessError : public std::runtime_error
    {
    public:
      AccessError(const std::string &message) : std::runtime_error(message) {}
    };

    class WriteError : public AccessError
    {
    public:
      WriteError(const std::string &message) : AccessError(message) {}
    };

    enum EQuality {kOk, kStillOk, kFailure, kNQualityLevels};
    enum EMode {kRead, kUpdate, kCreate,kNModes};

    DummyHistoInput(const std::string &file_name, EMode mode);
    ~DummyHistoInput();

    void add(const std::string &histo_name, const PixA::ConfigHandle &scan_config_handle, EQuality quality, const PixA::Index_t &index, TH1 *histo);

    void numberOfHistograms(const std::string &histo_name, const PixA::ConfigHandle &scan_config_handle, EQuality quality, HistoInfo_t &info);

    TH1 *histo(const std::string &histo_name, const PixA::ConfigHandle &scan_config_handle, EQuality quality, const PixA::Index_t &index);

  protected:
    TDirectory *dir(const std::string &histo_name, const PixA::ConfigHandle &scan_config_handle, EQuality quality, bool create);
    static std::string makeName(const std::string &histo_name, EQuality quality, const PixA::Index_t &index);
    static std::string makeConfigName(const PixA::ConfigHandle &scan_config_handle);

  private:
    std::unique_ptr<TFile>   m_file;

    static const char *s_modeNames[kNModes];
    static const char *s_qualityNames[kNModes];

    static std::map<std::string, int> s_totalMaskStageStepMap;
  };
}
#endif
