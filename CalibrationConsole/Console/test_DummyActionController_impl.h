#ifndef _PixCon_test_DummyActionController_hh_
#define _PixCon_test_DummyActionController_hh_

#include <PixActions/PixActions.h>
#include "test_DummyActionController.hh"
#include <ipc/object.h>
#include <ipc/server.h>
#include <ipc/partition.h>

#include <string>
#include <map>
#include <memory>
#include <algorithm>

namespace PixLib {

  class PixActions;

  class DummyActionController_impl : public IPCNamedObject<POA_PixLib::DummyActionController,ipc::single_thread>
  {
  public:

    typedef std::shared_ptr< PixActions > ActionPtr_t;

    template <class T_class_>
    class VoidZeroArgFunction :	public std::unary_function<std::pair<const std::string, ActionPtr_t>, void> {
    public:
      typedef void (T_class_::*VoidFunction_t )();

      VoidZeroArgFunction( VoidFunction_t a_func ) : m_func(a_func) {}

      void operator()(std::pair<const std::string, ActionPtr_t> &a) const
      {
	assert( dynamic_cast<T_class_ *>(a.second.get()) );
	(static_cast<T_class_ &>(*(a.second)).*m_func)();
      }

    private:
      VoidFunction_t m_func;
    };

    template <class T_class_, class T1>
    class VoidOneArgFunction :	public std::unary_function<std::pair<const std::string, ActionPtr_t>, void> {
    public:
      typedef void (T_class_::*VoidFunction_t )(T1 );

      VoidOneArgFunction( VoidFunction_t a_func, T1 arg1 ) : m_func(a_func), m_arg1(arg1) {}

      void operator()(std::pair<const std::string, ActionPtr_t> &a) const
      {
	assert( dynamic_cast<T_class_ *>(a.second.get()) );
	(static_cast<T_class_ &>(*(a.second)).*m_func)(m_arg1);
      }

    private:
      VoidFunction_t m_func;
      T1    m_arg1;
    };

    template <class T_class_, class T1, class T2>
    class VoidTwoArgFunction :	public std::unary_function<std::pair<const std::string, ActionPtr_t>, void> {
    public:
      typedef void (T_class_::*VoidFunction_t )(T1,T2 );

      VoidTwoArgFunction( VoidFunction_t a_func, T1 arg1, T2 arg2 ) : m_func(a_func), m_arg1(arg1), m_arg2(arg2) {}

      void operator()(std::pair<const std::string, ActionPtr_t> &a) const
      {
	assert( dynamic_cast<T_class_ *>(a.second.get()) );
	(static_cast<T_class_ &>(*(a.second)).*m_func)(m_arg1,m_arg2);
      }

    private:
      VoidFunction_t m_func;
      T1    m_arg1;
      T2    m_arg2;
    };


    DummyActionController_impl(IPCPartition &ipc_partition);

    /** Set the time needed to perform a scan.
     * @param rod_name the connectivity name of the ROD
     * @param needed_time the number of milliseconds the method will sleep.
     */
    void setDuration( const char *rod_name, CORBA::ULong needed_time);

    /** Mark the scan failing after this number of steps for the given ROD.
     * @param rod_name the connectivity name of the ROD.
     * @param n_steps number of steps after which the scan will fail.
     */
    void failAfterNSteps( const char *rod_name, CORBA::ULong fail_after_n_steps );

    /** Mark the scan for the the given ROD to stop progresing after this number of steps.
     * @param rod_name the connectivity name of the ROD.
     * @param n_steps number of steps after which the scan will stop progressing.
     */
    void getStuckAfterNSteps( const char *rod_name, CORBA::ULong n_steps);

    void setHistoInputFileName(const char *file_name);

    void setHistoQuality(const char *conn_name, EHistoQuality quality);

    void clearHistoQuality();


    bool registerAction( const std::string &rod_name, const std::shared_ptr<PixActions> &action );

    bool deregisterAction( const std::string &rod_name);

    void shutdown();

    static DummyActionController_impl *instance(IPCPartition &partition) {

      if (!s_instance) {
	s_instance=new DummyActionController_impl(partition);
	s_partitionName = partition.name();
      }
      else {
	assert(partition.name() == s_partitionName);
      }
      return s_instance;

    }

  private:
    static std::string makeName(const std::string &ipc_name);
    std::map<std::string, std::shared_ptr<PixActions> > m_actionList;

    // call the functor for all actions which correspond to the rod name
    template <class T> void for_each(const char *rod_name, const T &a_functor) {
      std::map<std::string, std::shared_ptr<PixActions> >::iterator begin_action;
      std::map<std::string, std::shared_ptr<PixActions> >::iterator end_action;

      unsigned int rod_name_length = strlen(rod_name);
      if (rod_name_length==0) {
	begin_action= m_actionList.begin();
	end_action= m_actionList.end();
      }
      else {
	begin_action = m_actionList.find( rod_name);
	end_action = begin_action;
	if (end_action != m_actionList.end()) end_action++;
      }

      std::for_each( begin_action , end_action, const_cast<T &>(a_functor) );

    }

    static DummyActionController_impl *s_instance;
    static std::string                 s_partitionName;
  };

}
#endif
