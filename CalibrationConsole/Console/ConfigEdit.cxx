#include <Highlighter.h>
#include <ConfigEdit.h>

namespace PixCon {

  ConfigEdit::~ConfigEdit() {
    delete m_highligher;
  }

  void ConfigEdit::highlightTagField() {

    if (!m_highligher) {
      m_highligher =new Highlighter(*m_tagSelector,
				    m_errorColour,
				    m_tagSelector->palette().color(QPalette::Active, QPalette::Window));
    }

    m_highligher->restart(m_errorColour);
  }

}
