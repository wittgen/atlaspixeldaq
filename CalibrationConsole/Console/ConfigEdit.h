/* Dear emacs, this is -*-c++-*- */
#ifndef _PixCon_ConfigEdit_h_
#define _PixCon_ConfigEdit_h_
#include "ui_ConfigEditBase.h"

class Highlighter;

namespace PixCon {
  
  class ConfigEdit : public QDialog, public Ui_ConfigEditBase
  {    Q_OBJECT
  public:
    ConfigEdit(QWidget* parent)

      : QDialog(parent),
	m_highligher(NULL),
	m_errorColour("yellow")
    {setupUi(this);}

    ~ConfigEdit();

    void highlightTagField();
  protected:
  private:
    Highlighter *m_highligher;
    QColor m_errorColour;
  };

}
#endif
