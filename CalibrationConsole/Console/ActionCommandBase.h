#ifndef ACTIONCOMMANDBASE_H
#define ACTIONCOMMANDBASE_H
#include <ICommand.h>
#include <memory>

#include "CommandKits.h"

namespace PixLib {
  class PixActionsMulti;
}

namespace PixCon {
  class ConsoleEngine;

  class ActionCommandBase : public PixCon::IConfigurableCommand
  {
  public:
    ActionCommandBase(const std::shared_ptr<ConsoleEngine> &engine)
      : m_engine(engine)
    {}

    ~ActionCommandBase();

    PixCon::Result_t execute(IStatusMessageListener &listener);

    void resetAbort() {
      m_abort=false;
    }

    void abort() {
      /* Initialise ROD cannot be aborted. So cannot do anything. */
      m_abort=true;
      //      m_engine->abortCurrentAction();
    }

    void kill();

    bool configure() { return false; } 

    bool hasDefaults() const { return true; }

  protected:

    /** Method to be used to pass action names to the action status monitoring.
     */
    virtual void setActionSequence(std::vector<std::string> &action_sequence) = 0;

    /** Method to be used to call an acion.
     * The calling method takes care of catching exceptions.
     */
    virtual bool callAction(PixLib::PixActionsMulti &action) = 0;

  private:

    std::shared_ptr<ConsoleEngine> m_engine;
    bool           m_abort;
    static const std::string s_name[2];
  };


}
#endif
