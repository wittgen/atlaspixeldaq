#include <ICommand.h>
#include <memory>
#include "ActionCommandBase.h"
#include <PixActions/PixActionsMulti.h>


namespace PixCon {

  class ConfigureBocCommand : public ActionCommandBase
  {
  public:
    ConfigureBocCommand(const std::shared_ptr<ConsoleEngine> &engine)
      : ActionCommandBase(engine)
    {}

    const std::string &name() const { return commandName(); }

    static const std::string &commandName() {
      return s_name;
    }

  protected:

    void setActionSequence(std::vector<std::string> &action_sequence) {
      action_sequence.push_back("configureBoc");
    }

    bool callAction(PixLib::PixActionsMulti &action) {
      action.configureBoc();
      return true;
    }

  private:

    static const std::string s_name;
  };

  const std::string ConfigureBocCommand::s_name("Configure BOC");


  class ConfigureBocKit : public PixCon::ICommandKit
  {
  protected:
    ConfigureBocKit()
    {}

  public:
    PixCon::IConfigurableCommand *createCommand(const std::string &/*command*/, const std::shared_ptr<ConsoleEngine> &engine) const {
      assert(engine.get());
      return new ConfigureBocCommand(engine);
    }

    static bool registerKit() {
      CommandKits::registerKit(ConfigureBocCommand::commandName(),new ConfigureBocKit, PixCon::UserMode::kExpert);
      //      CommandKits::registerKit(ConfigureBocCommand::name(true),new ConfigureBocKit(true));
      return true;
    }

  private:
    static bool s_registered;
  };

  bool ConfigureBocKit::s_registered = ConfigureBocKit::registerKit();

}
