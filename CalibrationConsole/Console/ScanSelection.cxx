#include <ConfigWrapper/PixConfigWrapper.h>

#include "ScanSelection.h"
#include <ScanConfigDbInstance.hh>
#include <PixDbConfigDb.hh>
#include <ObjectConfigDbInstance.hh>
#include <CalibrationDataUtil.h>
#include <CalibrationDataManager.h>
#include <PixScanConfigEdit.h>
#include <ObjectConfigEdit.h>
#include "SelectorMap.h"

#include <QTVisualiser/BusyLoopFactory.h>

#include <QMessageBox>
#include <QFileInfo>
#include <QListWidget>

#include <PixDbGlobalMutex.hh>

//***************

#include "PluginManager/DirIter.hh"
#include <iomanip>

//********************

namespace PixCon {

  SelectorMap::SelectorMap( QWidget *parent, unsigned int selector_id, QWidget *destination, const char* method)
    : QObject(parent),
      m_selectorId(selector_id)
  {
    connect(this,SIGNAL(selectorActivated(int,int)),destination, method);
  }

  void SelectorMap::activated(int item_i) {
    emit selectorActivated(m_selectorId, item_i);
  }

  const char *ScanSelection::s_className[4]={
    CAN::ScanConfigDbInstance::scanClassName(),
    CAN::ObjectConfigDbInstance::objectClassName(CAN::ObjectConfigDbInstance::kAnalysis),
    CAN::ObjectConfigDbInstance::objectClassName(CAN::ObjectConfigDbInstance::kClassification),
    CAN::ObjectConfigDbInstance::objectClassName(CAN::ObjectConfigDbInstance::kPostProcessing)
  };

  ScanSelection::ScanSelection(const std::string &name, const std::string &default_file_name, bool scan, bool analysis)
  { setupUi(this);


      scanana.assign(2,0);
      if(scan)scanana.at(0)=1;
      if(analysis)scanana.at(1)=1;
     
    std::cout << "DEBUG [ScanSelection::ctor] Constructor for: name = \"" << name << "\" filename = \"" << default_file_name << "\" scan/analysis flags: \"" << scan << "/" << analysis << "\"\n";
    m_configDb[0]=std::shared_ptr<CAN::IConfigDb>( CAN::ScanConfigDbInstance::create() );
    if (analysis || scan) {
      m_configDb[1]=std::shared_ptr<CAN::IConfigDb>( CAN::ObjectConfigDbInstance::create() );
      m_configDb[2]=m_configDb[1];
      m_configDb[3]=m_configDb[1];
    }
    if (!scan) {
      delete m_scanConfigGroup;
      m_scanConfigGroup=NULL;
    }
    if (scan) {
      m_scanAnalysisConfigGroup->hide();
      m_fileNameEdit->setText(QString::fromStdString(default_file_name));

      if (!checkFileName(true)) {
	m_useDefaultCheckBox->setChecked(false);
	m_fileNameEdit->setEnabled(true);
      }
      else {
	m_useDefaultCheckBox->setChecked(true);
	m_fileNameEdit->setEnabled(false);
      }
    }

    if (scan) {
      m_typeSelector[0]=m_scanTypeSelector;
      m_tagSelector[0]=m_scanConfigTagSelector;
      m_revSelector[0]=m_scanConfigRevSelector;
      m_customiseScanButton->setEnabled(false);
    }

    if (analysis || scan) {
    m_typeSelector[1]=m_analysisTypeSelector;
    m_tagSelector[1]=m_analysisConfigTagSelector;
    m_revSelector[1]=m_analysisConfigRevSelector;

    m_typeSelector[2]=m_classificationTypeSelector;
    m_tagSelector[2]=m_classificationConfigTagSelector;
    m_revSelector[2]=m_classificationConfigRevSelector;

    m_typeSelector[3]=m_postProcessingTypeSelector;
    m_tagSelector[3]=m_postProcessingConfigTagSelector;
    m_revSelector[3]=m_postProcessingConfigRevSelector;
    m_customiseAnalysisButton->setEnabled(false);
    m_customiseClassificationButton->setEnabled(false);
    m_customisePostProcessingButton->setEnabled(false);
    }

    for (unsigned int class_i=0; class_i<4; class_i++) {
      if (!scan && class_i==0) continue;
      if ((!analysis && !scan) && class_i>0) continue;

      m_tagSelector[class_i]->setSizeAdjustPolicy(QComboBox::AdjustToContents);
      m_revSelector[class_i]->setSizeAdjustPolicy(QComboBox::AdjustToContents);

      SelectorMap *type_map=new SelectorMap(this, class_i, this, SLOT(updateTags(int,int)));
      connect(m_typeSelector[class_i],SIGNAL(activated(int)),type_map, SLOT( activated(int)));

      SelectorMap *tag_map=new SelectorMap(this, class_i, this, SLOT(updateRevisions(int,int)));
      connect(m_tagSelector[class_i],SIGNAL(activated(int)),tag_map, SLOT( activated(int)));

    }

    if (scan) {
      updateTypes(ScanSelection::kScan, name);
    }


    if (analysis) {
      m_analyseScanCheckBox->setChecked(true);
      if (!scan) {
    m_analyseScanCheckBox->hide();
      }
      m_scanAnalysisConfigGroup->show();
    }
    else {
      m_analyseScanCheckBox->setChecked(false);
      m_scanAnalysisConfigGroup->hide();
    }

    if (analysis) {
      updateTypes(ScanSelection::kAnalysis, (scan ? name : ""));
      updateTypes(ScanSelection::kClassification, "");
      updateTypes(ScanSelection::kPostProcessing, "");
    }
    /*
    for (unsigned int class_i =0 ; class_i<4; class_i++) {
       if (class_i == 0 && !scan) continue;
       if (class_i>0 && !scan && !analysis) continue;
       // m_typeSelector[class_i]->adjustSize();
       // m_tagSelector[class_i]->adjustSize();
       // m_revSelector[class_i]->adjustSize();
    }
    */
    adjustSize();

    //    updateTags();
    //    updateRevisions();

    //    setFixedSize(480,100);
}
  std::string ScanSelection::selectedType(EClass class_i) const {
    assert(class_i<4);
    return ((m_typeSelector[class_i]->currentText().length()>0) ? m_typeSelector[class_i]->currentText().toLatin1().data() : "");
  }

  std::string ScanSelection::selectedTag(EClass class_i) const {
    assert(class_i<4);
    return (( m_tagSelector[class_i]->currentText().length()>0) ? m_tagSelector[class_i]->currentText().toLatin1().data() : "");
  }

  CAN::Revision_t ScanSelection::selectedRevision(EClass class_i) const {
    assert(class_i<4);
    int item_i = m_revSelector[class_i]->currentIndex();
    if (static_cast<unsigned int>(item_i)<m_revisions[class_i].size()) {
      return m_revisions[class_i][item_i];
    }
    return 0;
  }

  std::string ScanSelection::fileName() const {
    if (m_fileNameEdit->text().length()>0) {
      return m_fileNameEdit->text().toLatin1().data();
    }
    else {
      return "";
    }
  }

  unsigned int ScanSelection::scanSerialNumber() const {
    assert( m_analysisConfigGroup );
    assert( m_scanAnalysisConfigGroup );

    if (m_scanSerialNumber->text().length()==0) {
      return 0;
    }

    std::string serial_number_string( m_scanSerialNumber->text().toLatin1().data());
    std::string::size_type pos = 0;
    while (pos < serial_number_string.size() && isspace(serial_number_string[pos])) pos++;
    if (serial_number_string[pos]=='S' && serial_number_string.find("-",pos)!=std::string::npos) {
      std::pair<PixCon::EMeasurementType, PixCon::SerialNumber_t> a_serial_number = stringToSerialNumber( serial_number_string.substr(pos, serial_number_string.size()-pos));
      if (a_serial_number.second>0) {
	return a_serial_number.second;
      }
      pos++;
    }
    return atoi( &(serial_number_string[pos]));
  }

  std::map<std::string, std::pair<CAN::ESerialNumberType, CAN::SerialNumber_t> > ScanSelection::getNamedSources() const{
    assert( m_analysisConfigGroup );
    assert( m_scanAnalysisConfigGroup );

    std::map<std::string, std::pair<CAN::ESerialNumberType, CAN::SerialNumber_t> > named_sources;

    if (m_scanSerialNumber->text().length()==0) {
      return named_sources;
    }

    std::string serial_number_string( m_scanSerialNumber->text().toLatin1().data());
    std::string::size_type pos = 0;
    std::string buffer;
    std::string name;
    int number;
    bool getting_name = true;

    try {
      while (pos < serial_number_string.size()){
	if (serial_number_string[pos] != ' ' && serial_number_string[pos] != ':'){
	  buffer += serial_number_string[pos];
	}
	if (getting_name && serial_number_string[pos] == ':'){
	  name = buffer;
	  buffer.clear();
	  getting_name = false;
	}
	if (!getting_name && ( serial_number_string[pos] == ' ' || pos == serial_number_string.size()-1)){
	  number = atoi(buffer.c_str());
	  if (number == 0){
	    throw("Bad Scan Number");
	  }
	  buffer.clear();
	  named_sources[name]=std::make_pair(CAN::kScanNumber,number);
	  getting_name = true;
	}
	pos++;
      }
    }
    catch(...){
      named_sources.clear();
    }
    return named_sources;
 
  }

  void ScanSelection::updateTypes(int class_i, const std::string &name) {
    assert(class_i<4);
    assert( m_typeSelector[class_i]);
    assert( m_configDb[class_i]);
    std::set<std::string> type_list;
    {
      std::stringstream message;
      message << " Update type list " << " .... ";
      // @todo busy loops seems to impact adjustSize why ?
      //      std::unique_ptr<PixA::IBusyLoop> busy_loop(PixA::BusyLoopFactory::busyLoop(message.str()));

    try {
      type_list = m_configDb[class_i]->listTypes(s_className[class_i], CAN::IConfigDb::kDebug);
    }
    catch (...) {
    }
    }

    m_typeSelector[class_i]->clear();
    for (std::set<std::string>::const_iterator scan_type_iter = type_list.begin();
	 scan_type_iter != type_list.end();
	 scan_type_iter++) {
	if (scan_type_iter->empty()) continue;
        std::string::size_type pos = 0;
        while (pos < scan_type_iter->size() && isdigit( (*scan_type_iter)[pos])) pos++;
	if (pos>=scan_type_iter->size()) continue;
	m_typeSelector[class_i]->addItem(QString::fromStdString(*scan_type_iter));
	if (name == *scan_type_iter) {
	  m_typeSelector[class_i]->setCurrentIndex(m_typeSelector[class_i]->count()-1);
	}
    }
    // m_typeSelector[class_i]->adjustSize();
    // adjustSize();
    updateTags(class_i,0);
  }

  void ScanSelection::updateTags(int class_i, int) {
    assert(class_i<4);
    std::string current_item;
    if ( m_tagSelector[class_i]->currentText().toLatin1().data()) {
      current_item = m_tagSelector[class_i]->currentText().toLatin1().data();
    }
    std::set<std::string> tag_list;
    // @todo busy loop
    {
      std::stringstream message;
      message << " Update tags list " << " .... ";
      // @todo busy loops seems to impact adjustSize why ?
      //      std::unique_ptr<PixA::IBusyLoop> busy_loop(PixA::BusyLoopFactory::busyLoop(message.str()));

    if (m_typeSelector[class_i]->currentText().length()>0) {
      try {
	tag_list = m_configDb[class_i]->listTags( m_typeSelector[class_i]->currentText().toLatin1().data() );
      }
      catch (...) {
      }
    }
    }

    m_tagSelector[class_i]->clear();
    for (std::set<std::string>::const_iterator config_tag_iter = tag_list.begin();
	 config_tag_iter != tag_list.end();
	 config_tag_iter++) {
      std::string tag_name((*config_tag_iter));
	if( tag_name.compare(tag_name.size()-3, 3, "_I4") == 0 ) continue;
      m_tagSelector[class_i]->addItem(QString::fromStdString(*config_tag_iter));
      if (current_item == *config_tag_iter) {
	m_tagSelector[class_i]->setCurrentIndex(m_tagSelector[class_i]->count()-1);
      }
    }
    //m_tagSelector[class_i]->adjustSize();
    //adjustSize();
    updateRevisions(class_i, 0);
  }

  void ScanSelection::updateRevisions(int class_i, int) {
    assert(class_i<4);
    std::string current_item;
    if ( m_revSelector[class_i]->currentText().toLatin1().data()) {
      current_item = m_revSelector[class_i]->currentText().toLatin1().data();
    }

    std::set<CAN::Revision_t> rev_list;

    // @todo busy loop
    {
      std::stringstream message;
      message << " Update revision list " << " .... ";
      // @todo busy loops seems to impact adjustSize why ?
      //      std::unique_ptr<PixA::IBusyLoop> busy_loop(PixA::BusyLoopFactory::busyLoop(message.str()));
      if (!m_typeSelector[class_i]->currentText().isEmpty() && !m_tagSelector[class_i]->currentText().isEmpty()) {
      try {
	rev_list = m_configDb[class_i]->listRevisions( m_typeSelector[class_i]->currentText().toLatin1().data(), m_tagSelector[class_i]->currentText().toLatin1().data() );
      }
      catch (...) {
      }
    }
    }

    m_revSelector[class_i]->clear();
    m_revisions[class_i].clear();

    if (!rev_list.empty()) {
      const std::string most_recent("Most recent");
      m_revSelector[class_i]->addItem( QString::fromStdString(most_recent) );
      m_revisions[class_i].push_back( *(rev_list.rbegin()) ); // The most recent revision is identical to the one here
      if (current_item == most_recent ) {
	m_revSelector[class_i]->setCurrentIndex( m_revSelector[class_i]->count()-1 );
      }
    }

    for (std::set<CAN::Revision_t>::const_reverse_iterator rev_iter = rev_list.rbegin();
	 rev_iter != rev_list.rend();
	 rev_iter++) {

      time_t a_time = *rev_iter;
      if (a_time>0) {
	const char *time_string = asctime( gmtime(&a_time) );
	QString qtime_string = time_string;
	qtime_string.remove(QChar('\n'));// remove trailing line break

	if (time_string) {
	  m_revSelector[class_i]->addItem( qtime_string+" (UTC)" );
	  m_revisions[class_i].push_back(a_time);
	  if (current_item == time_string ) {
	    m_revSelector[class_i]->setCurrentIndex(m_revSelector[class_i]->count()-1);
	  }

	}
      }

    }
    //m_revSelector[class_i]->adjustSize();
    //adjustSize(); 
    QPushButton *customise_button[kNTypes]; 
    customise_button[kScan]=m_customiseScanButton;     
    customise_button[kAnalysis]=m_customiseAnalysisButton;
    customise_button[kClassification]=m_customiseClassificationButton;
    customise_button[kPostProcessing]=m_customisePostProcessingButton;
    if (m_revSelector[class_i]->count()>0) {
      if (!customise_button[class_i]->isEnabled()) {
	customise_button[class_i]->setEnabled(true);
      }
    }
    else {
      if (customise_button[class_i]->isEnabled()) {
	customise_button[class_i]->setEnabled(false);
      }
    }
  }

  IConfigEdit *ScanSelection::createConfigEdit(std::vector<PixA::ConfigHandle> &vconfig, EClass class_i) {
    if (class_i>=kNTypes) return NULL;
    switch (class_i) { 
    case ScanSelection::kScan:{
      PixScanConfigEdit *edt = new PixScanConfigEdit(vconfig,
						     selectedType(class_i),
						     selectedTag(class_i),
						     *m_tagSelector[class_i],
						     this);
      edt->setWindowTitle("Scan Config Editor");
      return edt;
    }
    case ScanSelection::kAnalysis:
    case ScanSelection::kClassification:
    case ScanSelection::kPostProcessing: {
       ObjectConfigEdit *edt = new ObjectConfigEdit(vconfig,
						    selectedType(class_i),
						    selectedTag(class_i),
						    selectedRevision(class_i),
						    *m_tagSelector[class_i],
						    this);
       edt->setWindowTitle("Analysis Config Editor");
       return edt;
    }
    default:
      return NULL;
    }
  }

  void ScanSelection::customise(EClass class_i) {
    if (class_i>=kNTypes) return;
    std::cout << "INFO [ScanSelection::customise] Trying to get config for " <<  selectedType(class_i) << " at tag/revision ("
	      << selectedTag(class_i) << " / " << selectedRevision(class_i) << ")\n";
    if (selectedType(class_i).empty() || selectedTag(class_i).empty()) return;
    
    std::unique_ptr<IConfigEdit> config_edit;
    std::set<std::string> tag_list;
    
    {
      {
	//std::stringstream message;
	//message << " Update tags list " << " .... ";
	// @todo busy loops seems to impact adjustSize why ?
	//      std::unique_ptr<PixA::IBusyLoop> busy_loop(PixA::BusyLoopFactory::busyLoop(message.str()));
	
	if (m_typeSelector[class_i]->currentText().length()>0) {
	  try {
	    tag_list = m_configDb[class_i]->listTags( m_typeSelector[class_i]->currentText().toLatin1().data() );
	  }
	  catch (...) {
	  }
	}
      }
      //=========================[maria elena]
        
        
        std::vector <PixA::ConfigHandle> vconfig= Create_configvector(class_i);
/////////////
        
        
        
        std::string tag_name_flavor = selectedTag(class_i);
        
      

      try{
                    

	config_edit=std::unique_ptr<IConfigEdit>(createConfigEdit(vconfig, class_i));
          
      }
      catch(...){
        PIX_ERROR("ERROR [ScanSelection::customise] Failed to read config for all processes!");
      }
      
      if (!config_edit.get()) return;
    }

    while (config_edit->exec() == QDialog::Accepted ) {
      std::string tag_name = config_edit->tagName();
      
      
      if (/*maria elena added the first condition*/tag_name.compare(tag_name.size()-3, 3, "_I4") != 0 && !tag_name.empty() && ( config_edit->changed() 
				 || (   m_tagSelector[class_i]->currentText().toLatin1().data() 
				     && tag_name != m_tagSelector[class_i]->currentText().toLatin1().data()))) {

	try {
	  {
	    std::stringstream message;
	    message << " Store scan configuration .... ";
	    std::unique_ptr<PixA::IBusyLoop> busy_loop(PixA::BusyLoopFactory::busyLoop(message.str()));
	    
	    time_t revision = time(0);
	    for(int i=0; i<NTBaseMax; i++){
	      std::string fe_flavor;
	     // if(i == TBase_I3) fe_flavor = "_I3";
	      if(i == TBase_I4) fe_flavor = "_I4";
	      else fe_flavor = "";
	      
	      std::string tag_name_flavor = config_edit->tagName();
	      tag_name_flavor.insert(tag_name_flavor.size(), fe_flavor);

	      // Save scan config files 
	      //if(class_i != 0 && i!=TBase) continue;
	     if(check_I4_config(class_i)){ m_configDb[class_i]->addConfig(s_className[class_i],
					     selectedType(class_i),
					     tag_name_flavor,
					     revision,
					     PixA::PixConfigWrapper::wrap(config_edit->config(tag_name_flavor)) );}
	     else{QMessageBox::warning(NULL,
                                   "No FE-I4 scan config",
                                   "Please call an expert",
                                   QMessageBox::Ok,0,0);}
	    }
	  }
	  
	  updateTags( class_i, 0 /* ignored */ );
	  for(int i=0; i<m_tagSelector[class_i]->count(); i++){
	    if(m_tagSelector[class_i]->itemText(i)==QString::fromStdString(tag_name )){
	      m_tagSelector[class_i]->setCurrentIndex( i );
	      break;
	    }
	  }
	  updateRevisions( class_i, 0 /* ignored */);
	  m_revSelector[class_i]->setCurrentIndex(0); // select the most recent revision. That should be the new one.
	  std::cout << "INFO [ScanSelection::customise] wrote new config " << selectedType(class_i) << " / " << tag_name<<"."<< std::endl;
	  break;
	}
	catch( PixA::ConfigException &err) {
	  config_edit->errorOnSave();
	  std::cout << "ERROR [ScanSelection::customise] failed to store config " <<  selectedType(class_i) << " with tag " << tag_name << "."<< std::endl;
	}
      }
      else if(tag_name.compare(tag_name.size()-3, 3, "_I4") == 0){
             QMessageBox::warning(NULL, "Incorrect File Name", "The '_I4' termination is invalid. Please change the path!", QMessageBox::Ok,0,0);

             }
    }
  }

  void ScanSelection::setAnalyseScan(bool analyse_scan) {
    if (analyse_scan) {

      updateTypes(ScanSelection::kAnalysis, "");
      updateTypes(ScanSelection::kClassification, "");
      updateTypes(ScanSelection::kPostProcessing, "");
      m_scanAnalysisConfigGroup->show();

    }
    else {

      m_scanAnalysisConfigGroup->hide();

    }
    adjustSize();
  }

  bool ScanSelection::checkFileName(bool silent) const {
    std::string output_file_name=fileName();
    if (output_file_name.empty()) {
      if (silent) return false;
      int ret = QMessageBox::warning(NULL,
				     "Incorrect File Name",
				     "No file name given. Scan results will not be saved!",
				     QMessageBox::Retry | QMessageBox::Default,
				     QMessageBox::Ok);
      if (ret != QMessageBox::Ok ) {
	return false;
      }
    }
    if (output_file_name.size()<5 || output_file_name.rfind(".root")!=output_file_name.size()-5) {
      if (!silent) {
	QMessageBox::warning(NULL,
			     "Incorrect File Name",
			     "The file name should end in '.root'. Please change the name!",
			     QMessageBox::Ok,0,0);
      }
      return false;
    }
    std::string::size_type pos = output_file_name.rfind("/");
    if (pos != std::string::npos) {

      if (output_file_name.find("%f",pos)==std::string::npos) {
	if (!silent) {
	  QMessageBox::warning(NULL,
			       "Incorrect File Name",
			       "The file name must containe %f. Please change the name!",
			       QMessageBox::Ok,0,0);
	}
	return false;
      }

      if (output_file_name[0]!='/') {
	if (!silent) {
	  QMessageBox::warning(NULL,
			       "Incorrect File Name",
			       "The path must be an absolut path name e.g. starting with '/'. Please change the path!",
			       QMessageBox::Ok,0,0);
	}
	return false;
      }
      else {
	QFileInfo dir_info(output_file_name.substr(0,pos+1).c_str());
	if (!dir_info.isDir() || !dir_info.isWritable()) {
	  if (!silent) {
	    QMessageBox::warning(NULL,
				 "Incorrect File Name",
				 "The directory is invalid or not writable. Please change the path!",
				 QMessageBox::Ok,0,0);
	  }
	  return false;
	}
      }
    }
    else {    
      if (!silent) {
	QMessageBox::warning(NULL,
			     "Incorrect File Name",
			     "No path name given. Please specify the full pathname",
			     QMessageBox::Ok,0,0);
      }
      return false;
    }
    return true;
  }
          
  void ScanSelection::accept_aftercheck(){
      bool acceptok=1;
std::cout << "INFO [ScanSelection::accept_aftercheck] " << std::endl;
      if(scanana.at(0)==1){
          if(!check_I4_config(kScan)){
		acceptok=0;
              QMessageBox::warning(NULL,
                                   "No FE-I4 scan config",
                                   "Please customise and save one",
                                   QMessageBox::Ok,0,0);}
          
      }
      if(scanana.at(1)==1){
              int cnt=0;
              for(EClass classs = kAnalysis; classs < kNTypes; classs=EClass(classs+1)){
                  if(!check_I4_config(classs)){ cnt++;
                  }
          }
              if(cnt!=0){ acceptok=0;
				QMessageBox::warning(NULL,
                                                "No all FE-I4 analysis configs",
                                                "Please customise and save one",
                                                QMessageBox::Ok,0,0);
			 
              }
      }
std::cout << "OK!!" << std::endl;
    if(acceptok)accept();
  }

  
    std::vector <PixA::ConfigHandle> ScanSelection::Create_configvector(EClass class_i){
      
        std::vector <PixA::ConfigHandle> vconfig;
        Revision_t max_revision = 0;
      //*************we take the path
      std::string path = m_configDb[class_i]->givepath();
      //bool is_i4=1;
      
      for(int i=TBase; i<NTBaseMax; i++){
          std::string fe_flavor;
          if(i == TBase_I4) fe_flavor = "_I4";
          else fe_flavor = "";
          std::string tag_name = selectedTag(class_i);
          std::string tag_name_flavor = selectedTag(class_i);
          tag_name_flavor.insert(tag_name_flavor.size(), fe_flavor);
          std::string type_name = selectedType(class_i);
          
          try{
              
              //********* verify the existence of the folder
              
              std::string tag_dir(path);
              tag_dir+= type_name;
              tag_dir+='/';
              tag_dir+= tag_name;
              
              std::string tag_dir_flavor(path);
              tag_dir_flavor+= type_name;
              tag_dir_flavor+='/';
              tag_dir_flavor +=tag_name_flavor;
              
              if(i == TBase_I4 && (path.compare(path.size()-9, 9, "scan-cfg/") == 0 ||
                                   path.compare(path.size()-9, 9, "scan_cfg/") == 0 ||
                                   path.compare(path.size()-13, 13, "analysis-cfg/") == 0 )){
                  
                  QFileInfo a_dir_info_I4(tag_dir_flavor.c_str());
                  //if(!a_dir_info_I4.isDir()){is_i4=0;}
                  //else is_i4=1;
                  
              }
              
              else{
                  QFileInfo a_dir_info(tag_dir.c_str());
                  if(!a_dir_info.isDir()){
                      std::cout << "ERROR [ScanSelection::customise] the directory does not exist!" << std::endl;
                  }
              }
              
             /* if(is_i4==0){
                  mkdir(tag_dir_flavor.c_str(), S_IRWXU | S_IRWXG | S_IRWXO);
              }*/
              
              //********* verify the existence of the configuration file
              
              std::unique_ptr<CAN::IDirIter> dir_iter(CAN::DirLister::fileIterator(tag_dir, std::string("^")+type_name+"_"+tag_name+"_.+\\.root$"));
              max_revision = 0;
              while (dir_iter->next()) {
                  std::string a_file_name=dir_iter->name();
                  std::string::size_type ext_pos = a_file_name.rfind(".root");
                  if (ext_pos == std::string::npos) continue;
                  
                  std::string::size_type rev_pos = a_file_name.rfind("_",ext_pos);
                  if (rev_pos == std::string::npos || rev_pos+1>=a_file_name.size()) continue;
                  Revision_t a_revision = strtol(&a_file_name[rev_pos+1],NULL,10);
                  if (selectedRevision(class_i)==0) {
                      if (a_revision>max_revision) {
                          max_revision = a_revision;
                      }
                  }
                  else {
                      if (a_revision<=selectedRevision(class_i) && a_revision>max_revision) {
                          max_revision = a_revision;
                      }
                  }
              }
              
              if (max_revision==0) {
                  std::string message("Directory : ");
                  message += tag_dir;
                  if (selectedRevision(class_i)==0) {
                      message += " does not contain a single revision.";
                  }
                  else {
                      message += " does not contain a matching revision.";
                  }
                  throw CAN::MissingConfig(message);
              }
                            
              if(i == TBase_I4 && (path.compare(path.size()-9, 9, "scan-cfg/") == 0 ||
                                   path.compare(path.size()-9, 9, "scan_cfg/") == 0 ||
                                   path.compare(path.size()-13, 13, "analysis-cfg/") == 0 ))
              {
                  
                  std::string file_name(tag_dir_flavor);
                  file_name+= '/';
                  file_name += type_name;
                  file_name += '_';
                  file_name += tag_name_flavor;
                  
                  file_name += '_';
                  std::stringstream revision_str;
                  revision_str << std::setw(9) << std::setfill('0') << max_revision;
                  file_name += revision_str.str();
                  file_name +=".root";
                  QFileInfo a_file(file_name.c_str());
                  
                  if (!a_file.isFile() ) {
                      
                      int ret = QMessageBox::warning(NULL,
                                                     "The Configuration File doesn't exist for FE_I4 ",
                                                     "If you want to copy it from the FE_I3 config push Ok "
                                                     "Otherwise a Default version is going to be created",
                                                     QMessageBox::Ok,0 ,0);
                      
                      if(ret==QMessageBox::Ok){
                          
                      /*    std::string original_name(tag_dir);
                          original_name+= '/';
                          original_name += type_name;
                          original_name += '_';
                          original_name += tag_name;
                          
                          original_name += '_';
                          std::stringstream revision_str;
                          revision_str << std::setw(9) << std::setfill('0') << max_revision;
                          original_name += revision_str.str();
                          original_name +=".root";
                          
                          
                          std::ifstream ifs(original_name.c_str());
                          std::ofstream ofs(file_name.c_str());
                          ofs << ifs.rdbuf() << std::flush;*/
                       m_configDb[class_i]->create_copy_config(max_revision, selectedType(class_i), s_className[class_i], tag_name_flavor,vconfig.at(0));
                      }
                      
                      
                      if(ret!=QMessageBox::Ok){
                          m_configDb[class_i]->create_default_config(max_revision, selectedType(class_i), s_className[class_i], tag_name_flavor);
                      }
                  }
                  
                  
              }
              std::string flav("");
              if(i == TBase_I4){flav+="_I4";}
              vconfig.push_back( m_configDb[class_i]->config_1(selectedType(class_i),
                                                             tag_name,
                                                             max_revision,flav) );
   
          }
          catch(...){
              std::cout << "ERROR [ScanSelection::customise] failed to read config for kScan!" << std::endl;
          }
      }
        return vconfig;
      }
    
    bool ScanSelection::check_I4_config(EClass class_i){
        bool flaggy=1;
        std::string path = m_configDb[class_i]->givepath();
        std::string tag_name = selectedTag(class_i);
        std::string tag_name_I4 = selectedTag(class_i);
        std::string type_name = selectedType(class_i);
        tag_name_I4.insert(tag_name_I4.size(), "_I4");
        std::string tag_dir(path);
        tag_dir+= type_name;
        tag_dir+='/';
        tag_dir+= tag_name;
        std::string tag_dir_I4(path);
        tag_dir_I4+= type_name;
        tag_dir_I4+='/';
        tag_dir_I4+= tag_name_I4;
        QFileInfo a_dir_info_I4(tag_dir_I4.c_str());
        if(!a_dir_info_I4.isDir()){flaggy=0;}
        std::unique_ptr<CAN::IDirIter> dir_iter(CAN::DirLister::fileIterator(tag_dir, std::string("^")+type_name+"_"+tag_name+"_.+\\.root$"));
        Revision_t max_revision = 0;
        while (dir_iter->next()) {
            std::string a_file_name=dir_iter->name();
            std::string::size_type ext_pos = a_file_name.rfind(".root");
            if (ext_pos == std::string::npos) continue;
            
            std::string::size_type rev_pos = a_file_name.rfind("_",ext_pos);
            if (rev_pos == std::string::npos || rev_pos+1>=a_file_name.size()) continue;
            Revision_t a_revision = strtol(&a_file_name[rev_pos+1],NULL,10);
            if (selectedRevision(class_i)==0) {
                if (a_revision>max_revision) {
                    max_revision = a_revision;
                }
            }
            else {
                if (a_revision<=selectedRevision(class_i) && a_revision>max_revision) {
                    max_revision = a_revision;
                }
            }
        }        
        if (max_revision==0) {
            std::string message("Directory : ");
            message += tag_dir;
            if (selectedRevision(class_i)==0) {
                message += " does not contain a single revision.";
            }
            else {
                message += " does not contain a matching revision.";
            }
            throw CAN::MissingConfig(message);
        }
        std::string file_name(tag_dir_I4);
        file_name+= '/';
        file_name += type_name;
        file_name += '_';
        file_name += tag_name_I4;
        
        file_name += '_';
        std::stringstream revision_str;
        revision_str << std::setw(9) << std::setfill('0') << max_revision;
        file_name += revision_str.str();
        file_name +=".root";
        QFileInfo a_file(file_name.c_str());
        if (!a_file.isFile() ) {flaggy=0;}
        return flaggy;
    }
    
    
    
  void ScanSelection::enableFileNameEdit() {
    if (m_useDefaultCheckBox->isChecked()) {
      m_fileNameEdit->setEnabled(false);
    }
    else {
      m_fileNameEdit->setEnabled(true);
    }
  }



  ScanSelection::~ScanSelection() {}
}

