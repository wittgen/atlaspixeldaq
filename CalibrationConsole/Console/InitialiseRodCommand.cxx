#include <ICommand.h>
#include <memory>
#include "ActionCommandBase.h"
#include <PixActions/PixActionsMulti.h>


namespace PixCon {

  class InitialiseRodCommand : public ActionCommandBase
  {
  public:
    InitialiseRodCommand(const std::shared_ptr<ConsoleEngine> &engine, bool uninitialised_rods_only)
      : ActionCommandBase(engine),
	m_uninitialisedRodsOnly(uninitialised_rods_only)
    {}

    const std::string &name() const { return name(m_uninitialisedRodsOnly); }

    // defined in ActionCommandBase
    // bool configure() { return false; } 
    // bool hasDefaults() const { return true; }

    static const std::string &name(bool uninitialised_rods_only) {
      return s_name[ (uninitialised_rods_only ? 1: 0 )];
    }

  protected:

    void setActionSequence(std::vector<std::string> &action_sequence) {
      action_sequence.push_back("reset");
      action_sequence.push_back("loadConfig");
    }

    bool callAction(PixLib::PixActionsMulti &action) {
      action.initCal();
      return true;
    }

  private:

    bool m_uninitialisedRodsOnly;
    static const std::string s_name[2];
  };

  const std::string InitialiseRodCommand::s_name[2]={std::string("Initialize RODs"), std::string("Initialize non-initialized RODs") };


  class InitialiseRodKit : public PixCon::ICommandKit
  {
  protected:
    InitialiseRodKit(bool uninitialised_rods_only)
      : m_uninitialisedRodsOnly(uninitialised_rods_only)
    {}

  public:
    PixCon::IConfigurableCommand *createCommand(const std::string &/*command*/, const std::shared_ptr<ConsoleEngine> &engine) const {
      assert(engine.get());
      return new InitialiseRodCommand(engine, m_uninitialisedRodsOnly);
    }

    static bool registerKit() {
      CommandKits::registerKit(InitialiseRodCommand::name(false),new InitialiseRodKit(false), PixCon::UserMode::kShifter);
      //      CommandKits::registerKit(InitialiseRodCommand::name(true),new InitialiseRodKit(true));
      return true;
    }

  private:
    bool m_uninitialisedRodsOnly;
    static bool s_registered;
  };

  bool InitialiseRodKit::s_registered = InitialiseRodKit::registerKit();

  ICommand *initialiseRodCommand(const std::shared_ptr<ConsoleEngine> &engine) {
    return new InitialiseRodCommand(engine, false);
  }

}
