#include "SetActiveCommand.h"
#include <PixActions/PixActionsMulti.h>
#include "IStatusMonitorActivator.h"
#include "ConsoleEngine.h"
#include <ConfigWrapper/ConnectivityUtil.h>

namespace PixCon {

  const std::string SetActiveCommand::s_activeVarName("Active");
  const std::string SetActiveCommand::s_name[2]={std::string("Disable"),std::string("Enable")};

  SetActiveCommand::~SetActiveCommand() { }

  PixCon::Result_t SetActiveCommand::execute(IStatusMessageListener& /*listener*/)
  {
    PixCon::Result_t ret;
    //      m_engine->initialiseRods(m_uninitialisedRodsOnly);
    //       /* ConsoleEngine::EActionStatus status = */ m_engine->waitForCurrentAction(m_abort);

    try {
      Lock action_lock(m_engine->multiActionMutex());
      PixLib::PixActionsMulti &action = m_engine->multiAction();

      PixCon::Lock lock( m_engine->calibrationData().calibrationDataMutex() );
      PixA::ConnectivityRef conn = m_engine->calibrationData().connectivity(kCurrent, 0);
      ret.setFailure();
      if (!conn || m_connName.empty() ) {
	return ret;
      }

      switch (m_connItemType) {
      case kModuleItem: {
	const PixLib::ModuleConnectivity *module_conn = conn.findModule(m_connName);
	if (!module_conn) return ret;

	const PixLib::RodBocConnectivity *rod_conn = PixA::rodBoc( module_conn);
	if (!rod_conn) return ret;

	const PixLib::RodCrateConnectivity *crate_conn = PixA::crate( rod_conn );
	if (!crate_conn) return ret;

	std::string action_name = m_engine->actionName(PixA::connectivityName(rod_conn), PixA::connectivityName(crate_conn));

	m_engine->calibrationData().calibrationData().addValue<State_t>(kAllConnTypes,
									m_connName,
									kCurrent,
									0,
									s_activeVarName,
									static_cast<State_t>(kTransition));

	std::cout << "INFO [SetActiveCommand::execute] action = " << action_name << " modId = " << PixA::modId(module_conn)
		  << "(" << module_conn << ")" << std::endl;

	action.setModuleActive( action_name , PixA::modId(module_conn), m_enable);
	ret.setSuccess();
	break;
      }
      case kPp0Item: {
	PixA::ModuleList module_list=conn.modules(m_connName);
	PixA::Pp0List rod = module_list.parent();
	PixA::RodList crate = rod.parent();
	std::string action_name = m_engine->actionName(PixA::connectivityName(rod), PixA::connectivityName(crate));

	PixA::ModuleList::const_iterator module_begin = module_list.begin();
	PixA::ModuleList::const_iterator module_end = module_list.end();
	for(PixA::ModuleList::const_iterator module_iter = module_begin;
	    module_iter != module_end;
	    ++module_iter) {

	  m_engine->calibrationData().calibrationData().addValue(kAllConnTypes,
								 PixA::connectivityName(module_iter),
								 kCurrent,
								 0,
								 s_activeVarName,
								 static_cast<State_t>(kTransition));

	  action.setModuleActive( action_name , PixA::modId(module_iter), m_enable);

	  std::cout << "INFO [SetActiveCommand::execute] action = " << action_name << " modId = " << PixA::modId(module_iter)
		    << "(" << PixA::connectivityName(module_iter) << ")" << std::endl;

	}
	ret.setSuccess();
	break;
      }
      case kRodItem: {
	PixA::Pp0List pp0_list=conn.pp0s(m_connName);
	PixA::RodList crate = pp0_list.parent();
	std::string action_name = m_engine->actionName(m_connName, PixA::connectivityName(crate) );

	m_engine->calibrationData().calibrationData().addValue(kAllConnTypes,
							       m_connName,
							       kCurrent,
							       0,
							       s_activeVarName,
							       static_cast<State_t>(kTransition));
	std::cout << "INFO [SetActiveCommand::execute] action = " << action_name << " ("<<m_connName << ")"
		  << std::endl;
	action.setRodActive( action_name , m_enable);
	ret.setSuccess();
	break;
      }
      case kCrateItem: {
	PixA::RodList rod_list=conn.rods(m_connName);

	PixA::RodList::const_iterator rod_begin = rod_list.begin();
	PixA::RodList::const_iterator rod_end = rod_list.end();
	for(PixA::RodList::const_iterator rod_iter = rod_begin;
	    rod_iter != rod_end;
	    ++rod_iter) {

	  m_engine->calibrationData().calibrationData().addValue(kAllConnTypes,
								 PixA::connectivityName(rod_iter),
								 kCurrent,
								 0,
								 s_activeVarName,
								 static_cast<State_t>(kTransition));

	  std::string action_name = m_engine->actionName(PixA::connectivityName(rod_iter), m_connName);

	  std::cout << "INFO [SetActiveCommand::execute] action = " << action_name << " ("<< PixA::connectivityName(rod_iter) << ")"
		  << std::endl;
	  action.setRodActive( action_name , m_enable);
	}
	ret.setSuccess();
	break;
      }
      default: {
	break;
      }
      }
    }
    catch(...) {
      ret.setFailure();
    }

    return ret;
  }

}
