#include <ipc/partition.h>
#include <ipc/core.h>
#include <ddc/DdcRequest.hxx>

#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <iomanip>
#include <memory>


class DdcRequest;

#include <Lock.h>
#include <memory>

#include <memory>
#include <DcsVariables.h>
#include <Lock.h>

#include <ddc/DdcData.hxx>
#include <is/infoiterator.h>
#include <is/criteria.h>

class IPCPartition;


  class ProtectedDdcRequest
  {
  public:
    ProtectedDdcRequest(DdcRequest *ddc_request)
      : m_ddcRequest(ddc_request)
    {}

    ~ProtectedDdcRequest() {}

    DdcRequest &ddcRequest() { return *m_ddcRequest; }
    PixCon::Mutex &mutex() { return m_ddcRequestMutex; }
  private:

    std::unique_ptr<DdcRequest>   m_ddcRequest;
    PixCon::Mutex                       m_ddcRequestMutex;
  };


template<class D> class MyDcsDataArray : public DdcData, public ISNamedInfo
{
public:
	MyDcsDataArray(time_t sec, IPCPartition &p, const std::string& obName) 
		: DdcData(sec), ISNamedInfo(p, obName.c_str()) {}
	const std::vector<D>* 	getValue() const { return(&value); }
	void addValue(D data) 		{ value.push_back(data); }
	void publishGuts(ISostream &ostrm);
	void refreshGuts(ISistream &istrm);
private:
	std::vector<D>	value;
};

template<class D> inline void MyDcsDataArray<D>::publishGuts(ISostream &ostrm) 
{
	ostrm << *getOWLTime();
	D* ptr = new D[value.size()];
	for(unsigned int i = 0; i < value.size(); i++) {
		ptr[i] = value[i];
	}
	ostrm.put(ptr, value.size());
	delete[] ptr;
}

template<class D> inline void MyDcsDataArray<D>::refreshGuts(ISistream &istrm) 
{
	updateTime(istrm);
	D * ptr;
	size_t size;
	istrm.get( &ptr, size );
	value.erase(value.begin(), value.end());
	for(size_t i = 0; i < size; i++)
		value.push_back(ptr[i]);
	delete[] ptr;
}
	

int main(int argc, char **argv)
{
  std::string partition_name;
  std::string is_name = "RunParams";
  std::string ctrl_name = "RunCtrl";
  std::string ddc_var_name;
  bool cancel=false;
  bool read=false;
  {
    bool error=false;
  for (int arg_i=1; arg_i<argc; arg_i++) {
    if (strcmp(argv[arg_i],"-p")==0 && arg_i+1<argc) {
      partition_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-n")==0  && arg_i+1<argc) {
      is_name= argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-s")==0  && arg_i+1<argc) {
      ctrl_name= argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-t" )==0  && arg_i+1<argc) {
      ddc_var_name= argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-c" )==0  ) {
      cancel=true;
    }
    else if (strcmp(argv[arg_i],"-r" )==0  ) {
      read=true;
    }
    else {
      error=true;
    }
  }

  if (error || is_name.empty() || partition_name.empty()) {
    std::cout << "usage " << argv[0] << " -p partition [-n is-server-name] -t var-name [-c] " << std::endl;
    return -1;
  }
  }
  // Start IPCCore
  IPCCore::init(argc, argv);
  
  IPCPartition partition(partition_name);

  try {
   std::unique_ptr<ProtectedDdcRequest> ddc_request(new ProtectedDdcRequest(new DdcRequest(partition,ctrl_name)));

   std::vector<std::string> var_list;
   var_list.push_back(ddc_var_name);
   std::vector<std::string> ddc_request_list;

   std::string a_ddc_request = ddc_var_name +  " => " + is_name;  
   ddc_request_list.push_back(a_ddc_request);

   if (!read) {
     std::cout << "INFO [] var = " << ddc_var_name << "  : request = " << a_ddc_request << std::endl;
   }
   if (cancel) {

     std::string error;
     bool ret = ddc_request->ddcRequest().ddcCancelImport(var_list,error);
     if (!ret) {
       std::cout << "ERROR [DcsMonitor::cancelDdcRequest] Request failed with : " << error << std::endl;
     }

   }
   else if (read) {
     std::cout << "INFO [] read var = " << ddc_var_name << " :"  << std::endl;
     MyDcsDataArray<std::string> is_value(0,partition, std::string(".*")+ddc_var_name);
     
     // Get current values
     {
       ISInfoIterator info_iter( partition, is_name.c_str(), ISCriteria(std::string(".*")+ddc_var_name));
       
       while( info_iter() ) {

	 info_iter.value(is_value);
	 std::string var_name = info_iter.name();
	 std::cout << var_name << " : ";
	 std::vector<std::string> *ptr=const_cast< std::vector<std::string> *>(is_value.getValue());
	 if (ptr) {
	   for (std::vector<std::string>::const_iterator iter = ptr->begin();
		iter != ptr->end();
		++iter) {
	     std::cout << *iter << " ";
	   }
	 }
	 std::cout << std::endl;
       }
     }
   }
   else {
       
     std::string error;
     bool ret= ddc_request->ddcRequest().ddcMoreImportOnChange(ddc_request_list, error);
     if (!ret) {
       std::cout << "ERROR [DcsMonitor::ddcRequest] Request failed with : " << error << std::endl;
     }

   }
  }
  catch(...) {
    // TODO: signal error if some action cannot be retrieved
    std::cerr << "FATAL [main:" << argv[0] << "] Caught unknown exception." << std::endl;
    return -1;
  }
  return 0;
}

