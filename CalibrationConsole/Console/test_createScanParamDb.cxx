#include <ScanConfigDbInstance.hh>
#include <PixDbConfigDb.hh>
#include <ConfigWrapper/PixConfigWrapper.h>
#include <ConfigWrapper/pixa_exception.h>
#include <PixController/PixScan.h>
#include <iostream>
#include <map>


int main(int argc, char **argv)
{
  std::string tag_name="Base";
  std::string list_tag_name;
  std::string list_name;
  std::string class_name;
  std::string db_path;
  std::string scan_type="AllScan";
  std::string fe_flavour="PM_FE_I3"; // T

  CAN::Revision_t revision=0;
  bool list_only=false;
  bool dump=false;
  bool rce=false;
  for (int arg_i=1; arg_i<argc; arg_i++) {
    if (strcmp(argv[arg_i],"-t")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      tag_name =argv[++arg_i];
      list_tag_name = tag_name;
    }
    else if (strcmp(argv[arg_i],"-n")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      list_name =argv[++arg_i];
      list_only=true;
    }
    else if (strcmp(argv[arg_i],"-c")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      class_name =argv[++arg_i];
      list_only=true;
    }
    else if (strcmp(argv[arg_i],"-r")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      revision =atoi(argv[++arg_i]);
      dump=true;
    }
    else if (strcmp(argv[arg_i],"-e")==0) {
      rce=true;
    }
    else if (strcmp(argv[arg_i],"-l")==0) {
      list_only=true;
    }
    else if (strcmp(argv[arg_i],"-p")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      list_only=true;
      db_path=argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-d")==0) {
      dump=true;
    }
    else if (strcmp(argv[arg_i],"-s")==0) {
      scan_type=argv[++arg_i];
      dump=false;
      list_only=false;
    }
    else if (strcmp(argv[arg_i],"-f")==0) {  // T: ask for FE-flavour
      fe_flavour=argv[++arg_i];
      if ( !(fe_flavour.compare("PM_FE_I3")==0 || fe_flavour.compare("PM_FE_I4")==0 ||  fe_flavour.compare("PM_FE_I4A")==0 ||   fe_flavour.compare("PM_FE_I4B")==0)  ) std::cout << " USAGE: " << argv[0] << "[-f PM_FE_I3] or [-f PM_FE_I4] or [-f PM_FE_I4A] or [-f PM_FE_I4B]" << std::endl;
      dump = false;
    }
    else {
      std::cout << "USAGE " << argv[0] << " [-s scan-name] [-t tag-name] [-n list-preset-name] [-r revision] [-l] [-d] [-f fe-flavour] [-e]" << std::endl;
      return -1;
    }
  }

  try {
    std::unique_ptr<CAN::IConfigDb> parameter_db((!db_path.empty() && list_only) ? 
					         static_cast<CAN::IConfigDb *>(new CAN::PixDbConfigDb(db_path))
					       : CAN::ScanConfigDbInstance::create());

    if (dump && (list_name.empty()  || list_tag_name.empty())) {
      list_only=true;
      dump=false;
    }
    if (class_name.empty()) {
      class_name = CAN::ScanConfigDbInstance::scanClassName();
    }
    if (dump) {
      std::string file_name = parameter_db->configFileName( list_name, list_tag_name, revision);
      std::cout << "Config for " << list_name << " / " << list_tag_name << " / " << revision << " : " << file_name << std::endl;
      PixA::ConfigHandle a_config( parameter_db->config( list_name, list_tag_name, revision) );
      PixA::ConfigRef scan_config(a_config.ref());
     const_cast<PixLib::Config &>(scan_config.config()).dump(std::cout);
    }
    else if (list_only) {
      std::set<std::string>    list;
      std::vector<std::pair<CAN::Revision_t , std::string> > time_list;
      if (list_name.empty()) {
	list=parameter_db->listTypes(class_name);
	std::cout << "List types : " << std::endl;
      }
      else if (list_tag_name.empty()) {
	list=parameter_db->listTags(list_name);
	std::cout << "List tags for " << list_name << "  : " << std::endl;
      }
      else {
	std::set<CAN::Revision_t> rev_list = parameter_db->listRevisions(list_name, list_tag_name);
	for (std::set<CAN::Revision_t>::const_iterator rev_iter = rev_list.begin();
	     rev_iter != rev_list.end();
	     rev_iter++) {
	  time_t a_time = *rev_iter;
	  if (a_time>0) {
	    const char *time_string = ctime(&a_time);
	    if (time_string) {
	      time_list.push_back( std::make_pair(a_time, time_string) );
	    }
	  }
	}
	std::cout << "List revisions for " << list_name << " / " << list_tag_name << "  : " << std::endl;
      }
      for (std::set<std::string>::const_iterator list_iter = list.begin();
	   list_iter != list.end();
	   list_iter++) {
	std::cout << *list_iter << std::endl;
      }
      for (std::vector< std::pair< CAN::Revision_t, std::string> >::const_iterator list_iter = time_list.begin();
	   list_iter != time_list.end();
	   list_iter++) {
	std::cout << list_iter->first << "  " << list_iter->second << std::endl;
      }
    }
    else {


      
      PixLib::PixScan * a_pix_scan=new PixLib::PixScan();
      
      //a_pix_scan->initConfig();
   
      
   

    std::map<std::string, int> type_list = a_pix_scan->getScanTypes();
    bool IsAddParam=false;
    
    for (std::map<std::string, int>::const_iterator type_iter = type_list.begin();
	 type_iter != type_list.end();
	 type_iter++) {
      
      if(scan_type!="AllScan"){
	bool IsMatch=false;
	if(scan_type == type_iter->first) IsMatch=true;
	else if(scan_type == std::string("BOC_INLINKOUTLINK")){
	  if(type_iter->first == std::string("BOC_LINKSCAN")) IsMatch=true;
	}
	if(!IsMatch) continue;
      } 

      
      try {
	// must be executed for *every* iteration in "AllScan"-mode, otherwise default values will not be correct for next scan
	a_pix_scan->initConfig();

	// T: easy way to convert string to enum
	/* ignore invalid scan types */
	  if (fe_flavour.compare("PM_FE_I3")==0) {
	      if(a_pix_scan->preset(static_cast<PixLib::PixScan::ScanType>(type_iter->second), PixLib::EnumFEflavour::PM_FE_I2)==false) throw PixLib::PixScanExc(PixLib::PixScanExc::WARNING,"invalid scan preset");
	  } else if (fe_flavour.compare("PM_FE_I4")==0 || fe_flavour.compare("PM_FE_I4A")==0 ) {
	      if(a_pix_scan->preset(static_cast<PixLib::PixScan::ScanType>(type_iter->second), PixLib::EnumFEflavour::PM_FE_I4)==false) throw PixLib::PixScanExc(PixLib::PixScanExc::WARNING,"invalid scan preset");
	  } else if( fe_flavour.compare("PM_FE_I4B")==0) {
	      if(a_pix_scan->preset(static_cast<PixLib::PixScan::ScanType>(type_iter->second), PixLib::EnumFEflavour::PM_FE_I4)==false) throw PixLib::PixScanExc(PixLib::PixScanExc::WARNING,"invalid scan preset");
	  }
	
	IsAddParam=true;
	// T
	std::cerr << a_pix_scan << std::endl;
	std::cout << a_pix_scan->getLVL1Latency() << std::endl;
	PixA::ConfigHandle config_handle(PixA::PixConfigWrapper::wrap(a_pix_scan->config()) );
	parameter_db->addConfig(class_name, type_iter->first, tag_name,revision, PixA::PixConfigWrapper::wrap(a_pix_scan->config()) ); // T: revision as param !
      }
     catch ( PixLib::PixScanExc &) {
	std::cerr << "ERROR: sth went wrong with preset/addConfig" << std::endl;
      }
    }
    if(!IsAddParam) std::cerr <<"Preset definition (PixController/PixScan.cxx) doesn't include "<< scan_type <<std::endl;
    }
  }
  catch(CAN::MissingConfig &err) {
    std::cerr << "ERROR [" << argv[0] << "] Caught MissingConfig exception : " << err.getDescriptor() << std::endl;
  }
  catch(PixA::ConfigException &err) {
    std::cerr << "ERROR [" << argv[0] << "] Caught ConfigError exception : " << err.getDescriptor() << std::endl;
  }
  catch(std::exception &err) {
    std::cerr << "ERROR [" << argv[0] << "] Caught std exception : " << err.what() << std::endl;
  }
  catch(...) {
    std::cerr << "ERROR [" << argv[0] << "] Caught unknown exception." << std::endl;
  }
  return 0;
}
