#include <IsMonitor.h>
#include "ActionStatusMonitor.h"
#include <QNewVarEvent.h>

#include <is/inforeceiver.h>
#include <is/infoiterator.h>

#include <ConfigWrapper/Connectivity.h>
#include <ConfigWrapper/ConnectivityUtil.h>

#include <StatusChangeTransceiver.h>

#include <DefaultColourCode.h>

#include <IsReceptor.h>
#include "ProgressEvent.h"

namespace PixCon {

  std::map<std::string, ActionStatusMonitor::EActionStatus> ActionStatusMonitor::s_actionStatusNames;
  const std::string ActionStatusMonitor::s_rodStatusVarName="ROD status";


  ActionStatusMonitor::ActionStatusMonitor(const std::shared_ptr<IsReceptor> &receptor,
					   const std::shared_ptr<CalibrationDataManager> &calibration_data,
					   const std::shared_ptr<StatusChangeTransceiver> &status_change_transceiver)
    : m_receptor(receptor),
      m_calibrationData(calibration_data),
      m_extractor( s_rodStatusVarName ),
      m_statusChangeTransceiver( status_change_transceiver),
      m_doneCounter(0),
      m_lastDoneCounter(0),
      m_isMonitoring(false),
      m_verbose(false)
  {
    if (s_actionStatusNames.empty()) {
      s_actionStatusNames["DONE"]=kActionSuccess;
      s_actionStatusNames["SUCCESS"]=kActionSuccess;
      s_actionStatusNames["FAILED"]=kActionFailed;
      s_actionStatusNames["FAILURE"]=kActionFailed;
      s_actionStatusNames["PROCESSING"]=kActionProcessing;
    }


    // @todo read variable definition from db
    try {
      const VarDef_t var_def( VarDefList::instance()->getVarDef(s_rodStatusVarName) );
      if ( !var_def.isStateWithOrWithoutHistory() ) {
	std::cout << "ERROR [ActionStatusMonitor::ctor] variable " << s_rodStatusVarName << " already exists but it is not a state variable." << std::endl;
      }
    }
    catch (UndefinedCalibrationVariable &) {
      VarDef_t var_def( VarDefList::instance()->createStateVarDef(s_rodStatusVarName, kRodValue, kNStates) );
      if ( var_def.isStateWithOrWithoutHistory() ) {
	//	scan_variables.addVariable(histogram_state_name);
	StateVarDef_t &state_var_def = var_def.stateDef();

	state_var_def.addState(kUnknown,           DefaultColourCode::kConfigured,      "Unknown");
	state_var_def.addState(kRunning,              DefaultColourCode::kScanning,        "Executing");
	state_var_def.addState(kSuccess,                 DefaultColourCode::kSuccess,         "Ok");
	//	state_var_def.addState(kStuck,                 DefaultColourCode::kAlarm,           "Stuck");
	state_var_def.addState(kFailed,               DefaultColourCode::kFailed,             "Failure");

	m_receptor->notify( new QNewVarEvent(kCurrent, 0, "General", s_rodStatusVarName) );

      }
      else {
	std::cout << "ERROR [ScanStatusMonitor::ctor] variable " << s_rodStatusVarName << " already exists but it is not a state variable." << std::endl;
      }
    }
  }

  ActionStatusMonitor::~ActionStatusMonitor()
  {
  }

  void ActionStatusMonitor::process()
  {
    if (!m_isMonitoring) {
      return;
    }
    // for debugging:
    if (m_verbose) {
      std::cout << "INFO [ActionStatusMonitor::process] start." << std::endl; 
    }

    if (m_doneCounter!=m_lastDoneCounter) {
      m_lastDoneCounter=m_doneCounter;
      m_receptor->notify(new ProgressEvent(m_lastDoneCounter, m_rodStatus.size()));
    }
    MetaData_t::EStatus status = MetaData_t::kUnset;
    for ( std::map<std::string, RodState_t>::iterator rod_iter = m_rodStatus.begin();
	  rod_iter != m_rodStatus.end();
	  rod_iter++) {

      if (!rod_iter->second.isDone()) {
	if (m_verbose) {
	  std::cout << "INFO [ActionStatusMonitor::process] Rod " << rod_iter->first << " not done yet. wait." << std::endl; 
	}
	return;
      }
      else {

	MetaData_t::EStatus rod_status;
	switch (rod_iter->second.status()) {
	case kSuccess:
	  rod_status = MetaData_t::kOk;
	  break;
	case kFailed:
	  rod_status = MetaData_t::kFailed;
	  break;
	default:
	  rod_status = MetaData_t::kUnknown;
	  break;
	}
	if (m_verbose) {
	  std::cout << "INFO [ActionStatusMonitor::process] Rod " << rod_iter->first << " status = "  << rod_status<< std::endl;
	}
	status = MetaData_t::combineStatus( status, rod_status);
      }
    }
    calibrationData()->updateStatus(kCurrent,0, status );
    if (m_verbose) {
      std::cout << "INFO [ActionStatusMonitor::process] global status = "  << status<< std::endl;
    }
    stopMonitoring();
    m_statusChangeTransceiver->signal(kCurrent,0);
    if (m_verbose) {
      std::cout << "INFO [ActionStatusMonitor::process] Done."  << status<< std::endl;
    }
  }

  void ActionStatusMonitor::timeout(double /*elapsed_seconds*/)
  {
  }

  bool ActionStatusMonitor::monitorAction(const std::vector<std::string> &expected_action_sequence)
  {
    {
    Lock rod_status_lock(m_rodStatusMutex);
    if (m_isMonitoring) {
      throw std::runtime_error("ERROR [ActionStatusMonitor::monitorAction] Already/still monitoring an action sequence. Maybe, call abbort first?");
    }

    calibrationData()->updateStatus(kCurrent,0, MetaData_t::kUnknown);
    m_isMonitoring=true;

    // the list of available and allocated actions should be marked in the calibration data
    // container, so rather use the callibration data container than the list of actions.

    //       for (std::set<std::string>::const_iterator action_iter = expected_action_sequence.begin();
    //        action_iter != expected_action_sequence.end();
    //        action_iter++) {
    //       unsigned int len = strlen("SINGLE_ROD:");
    //       if (action_iter->compare(0,len, "SINGLE_ROD:")==0) {
    // 	std::string::size_type pos = action_iter->find(":",len+1);
    // 	if (pos != std::string::npos && pos+1 < action_iter->size()) {
    // 	  m_rodStatus.insert(std::make_pair(action_iter->substr(pos+1, action_iter->size()-pos-1),RodState_t()));
    // 	}
    //       }

    // initialise the ROD status variables
    EMeasurementType measurement_type = kCurrent;
    SerialNumber_t serial_number = 0;
    PixA::ConnectivityRef conn = calibrationData()->connectivity(measurement_type, serial_number);
    if (conn) {
      Lock calib_data_lock(calibrationData()->calibrationDataMutex());
      m_rodStatus.clear();

      for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
	   crate_iter != conn.crates().end();
	   ++crate_iter) {

	PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
	PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
	for (PixA::RodList::const_iterator rod_iter = rod_begin;
	     rod_iter != rod_end;
	     ++rod_iter) {

	  // ignore disabled RODs
	  try {
	    const CalibrationData::ConnObjDataList conn_obj_data ( calibrationData()->calibrationData().getConnObjectData( PixA::connectivityName(rod_iter)) );
	    if ( !conn_obj_data.enableState(measurement_type, serial_number).enabled() ) {
	      continue;
	    }
	  }
	  catch(CalibrationData::MissingConnObject &) {
	    // @todo Should not happen. But could it happen ? What to do if it happens ? 
	    std::cout << "ERROR [StatusMonitor::monitorStatus] No calibration data for ROD " << PixA::connectivityName(rod_iter) 
		      << " in \"current\"."
		      << std::endl;
	  }
	  m_rodStatus.insert(std::make_pair(PixA::connectivityName(rod_iter) ,RodState_t()));

	}
      }
      if (m_rodStatus.empty()) {
	std::cout << "ERROR [StatusMonitor::monitorStatus] No enabled RODs in \"current\". So nothing to monitor."
		  << std::endl;
	m_isMonitoring=false;
	m_expectedActionSequence.clear();
	calibrationData()->updateStatus(kCurrent,0, MetaData_t::kFailed);
	return false;
      }
      else {
	m_expectedActionSequence = expected_action_sequence;
	calibrationData()->updateStatus(kCurrent,0, MetaData_t::kRunning);
      }
    }
    else {
      std::cout << "ERROR [StatusMonitor::monitorStatus] No connectivity so nothing to monitor in \"current\"."
		<< std::endl;
      m_isMonitoring=false;
      calibrationData()->updateStatus(kCurrent,0, MetaData_t::kFailed);
      return false;
    }
    }

    {
    Lock rod_list_lock(m_rodStatusMutex);
    for ( std::map<std::string, RodState_t>::iterator rod_iter = m_rodStatus.begin();
	  rod_iter != m_rodStatus.end();
	  rod_iter++) {
      m_receptor->setValue(kRodValue, kCurrent, 0, rod_iter->first , m_extractor, static_cast<State_t>(kUnknown) );
    }
    }
    m_lastDoneCounter=0;
    m_doneCounter=0;
    m_receptor->notify(new ProgressEvent(m_lastDoneCounter, m_rodStatus.size()));

    update();
    return true;
  }

  void ActionStatusMonitor::update() 
  {

    Lock rod_list_lock(m_rodStatusMutex);
    IPCPartition partition( this->m_receptor->receiver().partition());
    const std::string &is_server_name = this->m_receptor->isServerName();
      // get initial config values from IS server
    std::string name(".*");


    ISInfoT<std::string> is_value;
    {
      ISCriteria criteria = is_value.type() && "ROD_CRATE.*/CURRENT-ACTION";
      // should ignore the initial value 
      // maybe at this place the action status should be reset ?
       ISInfoIterator ii( partition, is_server_name.c_str(),  criteria);
       while( ii() ) {
 	ii.value(is_value);
	// 	updateAction(ii.name(), is_value.getValue());
       }
      m_receptor->receiver().subscribe(is_server_name.c_str(),criteria,&ActionStatusMonitor::actionChanged, this);
    }

    {
      ISCriteria criteria = is_value.type() && "ROD_CRATE.*/STATUS";

       ISInfoIterator ii( partition, is_server_name.c_str(),  criteria);
       while( ii() ) {

 	ii.value(is_value);
	// 	updateStatus(ii.name(), is_value.getValue());
       }
      m_receptor->receiver().subscribe(is_server_name.c_str(),criteria,&ActionStatusMonitor::statusChanged, this);
    }

  }

  void ActionStatusMonitor::abortMonitoring()
  {
    stopMonitoring();
  }

  void ActionStatusMonitor::stopMonitoring() {

    {
    Lock rod_list_lock(m_rodStatusMutex);
    if (!m_isMonitoring) return;

    unsigned int done_counter=0;
    for ( std::map<std::string, RodState_t>::iterator rod_iter = m_rodStatus.begin();
	  rod_iter != m_rodStatus.end();
	  rod_iter++) {

      if (!rod_iter->second.isDone()) {
	  rod_iter->second.setUnknown();

	  if (m_verbose) {
	    std::cout << "INFO [ActionStatusMonitor::stopMonitoring] Status of ROD " << rod_iter->first 
		      << " is " << static_cast<unsigned int>(rod_iter->second.status()) << " state="
		      << static_cast<unsigned int>(rod_iter->second.state()) << " ." << std::endl;
	  }

	  ERodStatus rod_status = rod_iter->second.status();
	  if (rod_status >= kNStates) rod_status = kUnknown;
	  m_receptor->setValue(kRodValue, kCurrent, 0, rod_iter->first , m_extractor, static_cast<State_t>(rod_status) );
      }
      else {
	done_counter++;
      }
    }

    m_receptor->notify(new ProgressEvent(done_counter, m_rodStatus.size()));

    m_rodStatus.clear();
    }

    unsubscribe();

    m_isMonitoring=false;
    m_statusChangeTransceiver->signal(kCurrent,0);
  }

  void ActionStatusMonitor::unsubscribe()
  {

      // get initial config values from IS server
      std::string name(".*");

    const std::string &is_server_name = this->m_receptor->isServerName();
    ISInfoT<std::string> is_value;
    try {
      ISCriteria criteria = is_value.type() && "ROD_CRATE.*/CURRENT-ACTION";
      m_receptor->receiver().unsubscribe(is_server_name.c_str(),criteria);
    }
    catch (...) {
    }

    try {
      ISCriteria criteria = is_value.type() && "ROD_CRATE.*/STATUS";
      m_receptor->receiver().unsubscribe(is_server_name.c_str(),criteria);
    }
    catch (...) {
    }
  }


  void ActionStatusMonitor::updateAction(const std::string &is_name, const std::string &action_name)
  {
    if (!m_isMonitoring) return;

    // for debugging:
    if (m_verbose) {
      std::cout << "INFO [ActionStatusMonitor::updateAction] IS var " << is_name << " changed to " << action_name << "." << std::endl; 
    }

    std::string rod_name = extract(is_name);
    if (!rod_name.empty()) {
      std::vector<std::string>::const_iterator sub_action_iter = find(m_expectedActionSequence.begin(),m_expectedActionSequence.end(), action_name);

      Lock lock(m_rodStatusMutex);
      std::map<std::string, RodState_t>::iterator rod_iter = m_rodStatus.find( rod_name);
      if (rod_iter != m_rodStatus.end()) {
	if (sub_action_iter != m_expectedActionSequence.end()) {
	  rod_iter->second.setCurrentAction( static_cast<unsigned int>(sub_action_iter - m_expectedActionSequence.begin()) );
	}
	else {
	  if (m_verbose) {
	    std::cout << "INFO [ActionStatusMonitor::updateAction] " << rod_name << "state is unknown ." << std::endl; 
	  }
	  rod_iter->second.setCurrentActionUnknown();
	}

	// for debugging:
	if (m_verbose) {
	  std::cout << "INFO [ActionStatusMonitor::updateAction] Set ROD " << rod_name << " to state " << rod_iter->second.currentAction() << "." << std::endl; 
	}
      }
    }
  }

  void ActionStatusMonitor::updateStatus(const std::string &is_name, const std::string &status_name)
  {
    if (!m_isMonitoring) return;

    if (m_verbose) {
      std::cout << "INFO [ActionStatusMonitor::updateStatus] IS var " << is_name << " changed to " << status_name << "." << std::endl; 
    }
    std::string rod_name = extract(is_name);
    if (!rod_name.empty()) {

      Lock lock(m_rodStatusMutex);
      std::map<std::string, RodState_t >::iterator rod_iter = m_rodStatus.find(rod_name);
      if (rod_iter != m_rodStatus.end()) {
	ERodStatus old_status = rod_iter->second.status();

	if (!rod_iter->second.currentActionIsUnknown()) {
	  std::map<std::string, EActionStatus>::const_iterator status_iter = s_actionStatusNames.find(status_name);
	  if (status_iter != s_actionStatusNames.end()) {
	    rod_iter->second.updateState( status_iter->second == kActionSuccess);
	  }
	  else {
	    rod_iter->second.setUnknown();
	  }
	}
	else {
	  rod_iter->second.setUnknown();
	}


	if (rod_iter->second.currentAction()+1==m_expectedActionSequence.size() ) {
	  if (!rod_iter->second.isDone()) {
	    rod_iter->second.setDone();
	    if (rod_iter->second.isDone()) {
	      m_doneCounter++;
	      m_statusChangeFlag->setFlag();
	    }
	  }
	}

	if (old_status != rod_iter->second.status()) {
	  ERodStatus rod_status = rod_iter->second.status();
	  if (rod_status >= kNStates) rod_status = kUnknown;
	  m_receptor->setValue(kRodValue, kCurrent, 0, rod_name , m_extractor, static_cast<State_t>(rod_status) );
	}


	if (m_verbose) {
	  std::cout << "INFO [ActionStatusMonitor::updateStatus] Status of ROD " << rod_name << " is " << static_cast<unsigned int>(rod_iter->second.status()) << " state="
		    << static_cast<unsigned int>(rod_iter->second.state()) 
		    << "(was = " << old_status <<  ", is_value = " << status_name << ") "
		    << " action = " << rod_iter->second.currentAction() 
		    << " is " << (rod_iter->second.isDone() ? " done " : " not done yet")
		    << " ." << std::endl;
	}

	if (rod_iter->second.currentAction()+1==m_expectedActionSequence.size()) {
	  if (m_verbose) {
	    std::cout << "INFO [ActionStatusMonitor::updateAction] ROD " << rod_name << " reach final state " << static_cast<unsigned int>(rod_iter->second.status()) << "." << std::endl;
	  }
	}

      }
    }
  }


  std::string ActionStatusMonitor::extract(const std::string &is_name)
  {
    std::string::size_type pos  = is_name.find(".");
    if (pos != std::string::npos && pos+1<is_name.size()) {
      std::string::size_type start_pos  = is_name.find("/",pos+1);
      if (start_pos != std::string::npos) {
	std::string::size_type end_pos  = is_name.find("/",start_pos+1);
	if (end_pos != std::string::npos) {
	  return is_name.substr(start_pos+1,end_pos-start_pos-1);
	}
      }
    }
    return "";
  }


  void ActionStatusMonitor::statusChanged(ISCallbackInfo *info)
  {
    ISInfoT<std::string> is_value;
    if (info->type() == is_value.type()) {
      info->value(is_value);
      updateStatus(info->name(), is_value.getValue());
    }
  }

  void ActionStatusMonitor::actionChanged(ISCallbackInfo *info)
  {
    ISInfoT<std::string> is_value;
    if (info->type() == is_value.type()) {
      info->value(is_value);
      updateAction(info->name(), is_value.getValue());
    }
  }


}
