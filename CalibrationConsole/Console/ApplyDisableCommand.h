// Dear emacs, this is -*-c++-*-
#ifndef _PixCon_ApplyDisable_h_
#define _PixCon_ApplyDisable_h_

#include <ICommand.h>
#include <memory>

#include "CommandKits.h"

namespace PixLib {
  class PixDisable;
}

namespace PixCon {
  class ConsoleEngine;

  namespace DisableCommand {
    enum EMode {kApplyAll, kDisable,kEnable};

    IConfigurableCommand *create(const std::shared_ptr<ConsoleEngine> &engine,
				 const std::shared_ptr<const PixLib::PixDisable> &disable,
				 bool changes_only=false,
				 EMode disable_mode=kApplyAll);

    inline IConfigurableCommand *create(const std::shared_ptr<ConsoleEngine> &engine,
					bool changes_only=false,
					EMode disable_mode=kApplyAll) {
      return create(engine, std::shared_ptr<const PixLib::PixDisable>(), changes_only,disable_mode);
    }

  }
}
#endif
