#include <ICommand.h>
#include <memory>

#include "CommandKits.h"

namespace PixLib {
  class PixActionsMulti;
}

namespace PixCon {
  class ConsoleEngine;

  class SetActiveCommand : public PixCon::IConfigurableCommand
  {
  public:
    SetActiveCommand(const std::shared_ptr<ConsoleEngine> &engine, PixCon::EConnItemType conn_item_type, const std::string &connectivity_name, bool enable)
      : m_engine(engine),
	m_connItemType( conn_item_type),
	m_connName(connectivity_name),
	m_commandName( s_name[ (enable ? 1 : 0) ]+"_"+m_connName),
	m_enable(enable)
    {}

    ~SetActiveCommand();

    PixCon::Result_t execute(IStatusMessageListener &listener);

    /** Nothing to do */
    void resetAbort() { }

    /** cannot do anything */
    void abort() { }

    /** cannot do anything */
    void kill() { }

    bool configure() { return false; } 

    bool hasDefaults() const { return true; }

    const std::string &name() const { return m_commandName; }

    enum EActiveStates {kDisabled, kEnabled, kTransition };
    static const std::string &activeName() { return s_activeVarName; }
  protected:

  private:

    std::shared_ptr<ConsoleEngine> m_engine;
    PixCon::EConnItemType m_connItemType;
    const std::string m_connName;
    std::string m_commandName;

    bool m_enable;

    static const std::string s_name[2];
    static const std::string s_activeVarName;
  };


}
