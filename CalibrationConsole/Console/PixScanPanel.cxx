#include "PixScanPanel.h"

#include <ConfigWrapper/ConfObjUtil.h>
#include <ConfigWrapper/ConfGroupUtil.h>
#include <QTVisualiser/BusyLoopFactory.h>

#include <QMessageBox>
#include <QFileDialog>

//#include "PlaceHolderLineEdit.h"

#include <PixController/PixScan.h>

#include <EditVectorForm.h>

namespace PixCon {

  PixLib::ConfObj::types reduced_type(const PixLib::ConfObj &obj) {
    
    switch ( const_cast<PixLib::ConfObj &>(obj).type() ) {
    case PixLib::ConfObj::LIST:
      return PixLib::ConfObj::INT;
    case PixLib::ConfObj::LINK:
    case PixLib::ConfObj::ALIAS:
      return PixLib::ConfObj::STRING;
    default :
      return const_cast<PixLib::ConfObj &>(obj).type();
    }
  }

  //  template <class T_dest_, class T_src_> simple_copy(PixLib::ConfObj &dest, const PixLib::ConfObj &src) {
  //    static_cast<T_dest_ &>(dest).value() = static_cast<T_src_ &>( const_cast<PixLib::ConfObj &>(src)).value();
  //  }

  void simple_copy(PixLib::ConfObj &dest, const PixLib::ConfObj &src) {    
    assert(  const_cast<PixLib::ConfObj &>(dest).type() == dest.type() );
    const_cast<PixLib::ConfObj &>(dest).copy(src);
  }

  template <class T_dest_type> 
  void int_copy(T_dest_type &dest, const PixLib::ConfObj &src) {
    assert(  reduced_type(src) == PixLib::ConfObj::INT);
    switch( src.subtype() ) {
    case PixLib::ConfInt::S32:
    case PixLib::ConfInt::U32:
      dest = static_cast<T_dest_type>(static_cast<PixLib::ConfInt &>(const_cast<PixLib::ConfObj&>(src)).valueU32());
      break;
    case PixLib::ConfInt::S16:
    case PixLib::ConfInt::U16:
      dest = static_cast<T_dest_type>(static_cast<PixLib::ConfInt &>(const_cast<PixLib::ConfObj&>(src)).valueU16());
      break;
    case PixLib::ConfInt::S8:
    case PixLib::ConfInt::U8:
      dest = static_cast<T_dest_type>(static_cast<PixLib::ConfInt &>(const_cast<PixLib::ConfObj&>(src)).valueU8());
      break;
    default: throw std::runtime_error("Invalid Config Subtype");
    }
  }

  template <class T_dest_, class T_src_>
  void vector_copy(std::vector<T_dest_> &dest, const std::vector<T_src_> &src) {
    dest.clear();
    for (typename std::vector<T_src_>::const_iterator src_iter = src.begin();
	 src_iter != src.end();
	 ++src_iter) {
      dest.push_back(static_cast<T_dest_>(*src_iter));
    }
  }

  template <class T>
  void vector_copy(std::vector<T> &dest, const PixLib::ConfObj &src) {
    assert(const_cast<PixLib::ConfObj &>(src).type() == PixLib::ConfObj::VECTOR );
    switch( (src) .subtype() ) {
      case PixLib::ConfVector::V_INT:
	vector_copy(dest,static_cast<PixLib::ConfVector &>( const_cast<PixLib::ConfObj &>(src) ).valueVInt());
	break;
      case PixLib::ConfVector::V_UINT:
	vector_copy(dest,static_cast<PixLib::ConfVector &>( const_cast<PixLib::ConfObj &>(src) ).valueVUint());
	break;
      case PixLib::ConfVector::V_FLOAT:
	vector_copy(dest,static_cast<PixLib::ConfVector &>( const_cast<PixLib::ConfObj &>(src) ).valueVFloat());
	break;
      default:
	PixLib::ConfVector::subtypes sub_type = static_cast<PixLib::ConfVector &>(const_cast<PixLib::ConfObj &>(src) ).subtype();
	assert( sub_type ==  PixLib::ConfVector::V_FLOAT || sub_type == PixLib::ConfVector::V_INT || sub_type == PixLib::ConfVector::V_UINT);
	break;
      }
  }

  void string_copy(std::string &dest, const PixLib::ConfObj &src) {
    assert(  reduced_type(src) == PixLib::ConfObj::STRING);
    switch( const_cast<PixLib::ConfObj &>(src).type() ) {   
      case PixLib::ConfObj::STRING:
	dest = static_cast<PixLib::ConfString &>(const_cast<PixLib::ConfObj&>(src)).value();
	break;
      case PixLib::ConfObj::ALIAS:
	dest = static_cast<PixLib::ConfAlias &>(const_cast<PixLib::ConfObj&>(src)).value();
	break;
      case PixLib::ConfObj::LINK:
	dest = static_cast<PixLib::ConfLink &>(const_cast<PixLib::ConfObj&>(src)).value();
	break;
      default:
	assert(    const_cast<PixLib::ConfObj&>(src).type()==PixLib::ConfObj::STRING
		|| const_cast<PixLib::ConfObj&>(src).type()==PixLib::ConfObj::ALIAS
		|| const_cast<PixLib::ConfObj&>(src).type()==PixLib::ConfObj::LINK);
    }
  }

//   template <class T_dest_type, class T_src_>
//   void conv_copy(T_dest_type &dest, const PixLib::ConfObj &src) {
//     switch( static_cast<PixLib::ConfInt &>(const_cast<PixLib::ConfInt>(src)).type() ) {   
//       case PixLib::ConfInt::S32:
//       case PixLib::ConfInt::U32:
// 	dest = static_cast<T_dest_type>(static_cast<PixLib::ConfInt &>(const_cast<PixLib::ConfInt>(src)).valueU32());
// 	break;
//       case PixLib::ConfInt::S16:
//       case PixLib::ConfInt::U16:
// 	dest = static_cast<T_dest_type>(static_cast<PixLib::ConfInt &>(const_cast<PixLib::ConfInt>(src)).valueU16());
// 	break;
//       case PixLib::ConfInt::S8:
//       case PixLib::ConfInt::U8:
// 	dest = static_cast<T_dest_type>(static_cast<PixLib::ConfInt &>(const_cast<PixLib::ConfInt>(src)).valueU8());
// 	break;
//     }
//   }

  void conf_obj_copy(PixLib::ConfObj &dest, const PixLib::ConfObj &src) {
    assert( reduced_type( dest) == reduced_type(src) 
	    || (dest.type()==PixLib::ConfObj::LIST &&  const_cast<PixLib::ConfObj &>(src).type()==PixLib::ConfObj::STRING));
    switch( dest.type() ) {
    case PixLib::ConfObj::STRING: {
      string_copy(static_cast<PixLib::ConfString &>(dest).value(),src);
      break;
    }
    case PixLib::ConfObj::ALIAS: {
      string_copy(static_cast<PixLib::ConfAlias &>(dest).value(),src);
      break;
    }
    case PixLib::ConfObj::LINK: {
      string_copy(static_cast<PixLib::ConfLink &>(dest).value(),src);
      break;
    }
    case PixLib::ConfObj::LIST: {
      if( const_cast<PixLib::ConfObj &>(src).type()==PixLib::ConfObj::STRING ) {
	std::string src_name = static_cast<PixLib::ConfString &>( const_cast<PixLib::ConfObj &>(src) ).value();
	std::map<std::string, int>::const_iterator symb_iter = static_cast<PixLib::ConfList &>(dest).symbols().find(src_name);
	if (symb_iter ==static_cast<PixLib::ConfList &>(dest).symbols().end()) {
	  std::cout << "WARNING [config_copy] Invalid list value in configuration of  "
		    << dest.name() 
		    << " : "
		    << src_name
		    << "."
		    << std::endl;
	}
	else {
	  static_cast<PixLib::ConfInt &>(dest).setValue(symb_iter->second);
	}
	break;
      }
      assert( const_cast<PixLib::ConfObj &>(src).type()==PixLib::ConfObj::INT || const_cast<PixLib::ConfObj &>(src).type()==PixLib::ConfObj::LIST);
    }
    case PixLib::ConfObj::INT: {
      // ignore difference between signed / unsigned since the underlying storage is identical 
      switch( (dest).subtype() ) {
      case PixLib::ConfInt::S32:
      case PixLib::ConfInt::U32:
	int_copy(static_cast<PixLib::ConfInt &>(dest).valueU32(), src);
	break;
      case PixLib::ConfInt::S16:
      case PixLib::ConfInt::U16:
	int_copy(static_cast<PixLib::ConfInt &>(dest).valueU16(), src);
	break;
      case PixLib::ConfInt::S8:
      case PixLib::ConfInt::U8:
	int_copy(static_cast<PixLib::ConfInt &>(dest).valueU8(), src);
	break;
      default:throw std::runtime_error("Invalid Config Subtype");
      };

      break;
    }
    case PixLib::ConfObj::VECTOR: {
      switch( (dest).subtype() ) {
      case PixLib::ConfVector::V_INT:
	vector_copy(static_cast<PixLib::ConfVector &>(dest).valueVInt(), src);
	break;
      case PixLib::ConfVector::V_UINT:
	vector_copy(static_cast<PixLib::ConfVector &>(dest).valueVUint(), src);
	break;
      case PixLib::ConfVector::V_FLOAT:
	vector_copy(static_cast<PixLib::ConfVector &>(dest).valueVFloat(), src);
	break;
      default:
	throw std::runtime_error("Invalid Config Subtype");
      };

      break;
    }
    default:
      std::cout << "WARNING [config_copy] Default copy for " << dest.name() << std::endl;
      
    case PixLib::ConfObj::MATRIX:
    case PixLib::ConfObj::FLOAT:
    case PixLib::ConfObj::STRVECT:
    case PixLib::ConfObj::BOOL: {
      simple_copy(dest,src);
      break;
    }
    case PixLib::ConfObj::VOID: {
      std::cout << "WARNING [config_copy] Tried to copy invalid object " << dest.name() << std::endl;
    }
    }
  }

  void conf_group_copy(PixLib::ConfGroup &dest, const PixLib::ConfGroup &src) {
    assert(dest.name() == const_cast<PixLib::ConfGroup &>(src).name());
    for (std::vector<PixLib::ConfObj *>::iterator dest_conf_obj_iter=dest.begin();
	 dest_conf_obj_iter != dest.end();
	 ++dest_conf_obj_iter) {
      if (*dest_conf_obj_iter) {
	std::vector<PixLib::ConfObj *>::const_iterator src_conf_obj_iter=src.begin();
	for (src_conf_obj_iter=src.begin();
	     src_conf_obj_iter != src.end();
	     ++src_conf_obj_iter) {
	
	  if ((*src_conf_obj_iter) && const_cast<PixLib::ConfObj *>(*src_conf_obj_iter)->name() == (*dest_conf_obj_iter)->name()) {
	    if (reduced_type(*(*src_conf_obj_iter)) != reduced_type( *(*dest_conf_obj_iter))
		&& (   (*dest_conf_obj_iter)->type()!=PixLib::ConfObj::LIST
		    || const_cast<PixLib::ConfObj *>(*src_conf_obj_iter)->type()!=PixLib::ConfObj::STRING)) {
	      std::cout << "WARNING [conf_group_copy] Types disagree for " << (*dest_conf_obj_iter)->name() 
			<< "  src:" << const_cast<PixLib::ConfObj *>(*src_conf_obj_iter)->type() 
			<< " !=  dest:" << (*dest_conf_obj_iter)->type()
			<< std::endl;
	      std::cout << "INFO [conf_group_copy] src :" << std::endl;
	      const_cast<PixLib::ConfObj &>(*(*src_conf_obj_iter)).dump(std::cout);
	      std::cout << "INFO [conf_group_copy] dest :" << std::endl;
	      const_cast<PixLib::ConfObj &>(*(*dest_conf_obj_iter)).dump(std::cout);
	    }
	    else {
	      conf_obj_copy(*(*dest_conf_obj_iter), *(*src_conf_obj_iter));
	    }
	    break;
	  }
	}
	if (src_conf_obj_iter == src.end()) {
	  std::cout << "WARNING [conf_group_copy] No src config object " << (*dest_conf_obj_iter)->name() 
		    << std::endl;
	}
      }
    }
  }

  void config_copy(PixLib::Config &dest, const PixLib::Config &src) {

    for (unsigned int group_i=0; group_i<dest.size(); group_i++) {
      unsigned int  src_group_i=0;
      std::string dest_conf_group_name = dest[group_i].name();
      for (src_group_i=0; src_group_i<const_cast<PixLib::Config &>(src).size(); src_group_i++) {
	if (const_cast<PixLib::Config&>(src)[src_group_i].name()==dest_conf_group_name) {
	  conf_group_copy(dest[group_i],const_cast<PixLib::Config &>(src)[src_group_i]);
	  break;
	}

      }
      if (src_group_i>=const_cast<PixLib::Config&>(src).size()) {
	if (dest[group_i].size()>0) {
	  std::cout << "WARNING [config_copy] No ConfGroup " << dest_conf_group_name << std::endl;
	}
      }
    }

    for (std::map<std::string, PixLib::Config*>::iterator dest_sub_config_iter = dest.m_config.begin();
	 dest_sub_config_iter != dest.m_config.end();
	 ++dest_sub_config_iter) {
      if (dest_sub_config_iter->second) {
	std::map<std::string, PixLib::Config*>::const_iterator  src_sub_config_iter = src.m_config.find(dest_sub_config_iter->first);
	if (src_sub_config_iter != src.m_config.end() && src_sub_config_iter->second) {
	  config_copy( *(dest_sub_config_iter->second), *(src_sub_config_iter->second));
	}
	else {
	  std::cout << "WARNING [config_copy] No sub Config " << dest_sub_config_iter->first << std::endl;
	}
      }
    }

  }

  // if defined then enabling and disabeling the fe-by-fe checkbox will reset the mask to all on
  // #define MASK_IN_FE_BY_FE_ONLY

  FEMaskButton::FEMaskButton(QWidget *parent, PixScanPanel *panel, unsigned int fe_i)
    : QPushButton(parent), m_scanPanel(panel), m_fe(fe_i)
  {
    setCheckable(true);
    connect(this,SIGNAL(toggled(bool)), this, SLOT(feMaskToggled(bool)));
  }

  void FEMaskButton::feMaskToggled(bool state) {
    if (!state) {
      
      if (isChecked()) {
	//setPaletteBackgroundColor( QColor( 0, 230, 0 ) );

 setStyleSheet(QString::fromUtf8("background-color: rgb( 0, 255, 0 );")); 
show();
      }
      else {
	//setPaletteBackgroundColor( QColor(150, 230, 150 ) );

	setStyleSheet(QString::fromUtf8("background-color: rgb( 0, 255, 0 );"));
	show();
      }
      if (QAbstractButton::isChecked()==true) {
	QAbstractButton::setChecked(false);  
      }

    }
    else {
     // setPaletteBackgroundColor( QColor( 200, 200, 200 ) );

 setStyleSheet(QString::fromUtf8("background-color: gray;"));
show();
      if (QAbstractButton::isChecked()==false) {
	QAbstractButton::setChecked(true);  
      }
    }
    repaint();
    m_scanPanel->feMaskToggled((state ? false : true),m_fe);
  }



  PixScanPanel::PixScanPanel( PixA::ConfigHandle &config_handle, const std::string &title, QWidget* parent )
: QWidget(parent),
      m_pixScan(new PixLib::PixScan(PixLib::PixScan::DIGITAL_TEST, PixLib::EnumFEflavour::PM_FE_I2)),
      m_scanConfig(config_handle)
    {setupUi(this);  
      m_title->setText(QString::fromStdString(title));
      // initialise the scan output file name

#ifdef MASK_IN_FE_BY_FE_ONLY
      connect( snglfeMode, SIGNAL(clicked()), this , SLOT(singleFEModeToggled()));
#endif

      // redraw histo-panel with scrollbars
      init();

      // translates individual loop_i variable names to arrays
      for(unsigned int sci=0;sci<MAX_LOOPS;sci++){
	loopType[sci] = 0;
	loopEndAction[sci] = 0;
	loopActive[sci] = 0;
	loopDspProc[sci] = 0;
	loopRegular[sci] = 0;
	loopFree[sci] = 0;
	loopOnDsp[sci] = 0;
	loopStart[sci] = 0;
	loopStop[sci] = 0;
	loopStep[sci] = 0;
	loopPtsBox[sci] = 0;
	loopLoadPoints[sci] = 0;
	loopFromLabel[sci] = 0;
	loopToLabel[sci] = 0;
	loopStepsLabel[sci] = 0;
	loopPtlLabel[sci] = 0;
	loopPostActLabel[sci] = 0;
	LoopBox[sci] = 0;
      }
      loopType[0] = loop0Type;
      loopEndAction[0] = loop0EndAction;
      loopActive[0] = loop0Active;
      loopDspProc[0] = loop0DspProc;
      loopRegular[0] = loop0Regular;
      loopFree[0] = loop0Free;
      loopOnDsp[0] = loop0OnDsp;
      loopStart[0] = loop0Start;
      loopStop[0] = loop0Stop;
      loopStep[0] = loop0Step;
      loopPtsBox[0] = loop0PtsBox;
      loopLoadPoints[0] = loop0LoadPoints;
      loopFromLabel[0] = loop0FromLabel;
      loopToLabel[0] = loop0ToLabel;
      loopStepsLabel[0] = loop0StepsLabel;
      loopPtlLabel[0] = loop0PtlLabel;
      loopPostActLabel[0] = loop0PostActLabel;
      LoopBox[0] = loop0BoxPage;
      loopType[1] = loop1Type;
      loopEndAction[1] = loop1EndAction;
      loopActive[1] = loop1Active;
      loopDspProc[1] = loop1DspProc;
      loopRegular[1] = loop1Regular;
      loopFree[1] = loop1Free;
      loopOnDsp[1] = loop1OnDsp;
      loopStart[1] = loop1Start;
      loopStop[1] = loop1Stop;
      loopStep[1] = loop1Step;
      loopPtsBox[1] = loop1PtsBox;
      loopLoadPoints[1] = loop1LoadPoints;
      loopFromLabel[1] = loop1FromLabel;
      loopToLabel[1] = loop1ToLabel;
      loopStepsLabel[1] = loop1StepsLabel;
      loopPtlLabel[1] = loop1PtlLabel;
      loopPostActLabel[1] = loop1PostActLabel;
      LoopBox[1] = loop1BoxPage;
      loopType[2] = loop2Type;
      loopEndAction[2] = loop2EndAction;
      loopActive[2] = loop2Active;
      loopDspProc[2] = loop2DspProc;
      loopRegular[2] = loop2Regular;
      loopFree[2] = loop2Free;
      loopOnDsp[2] = loop2OnDsp;
      loopStart[2] = loop2Start;
      loopStop[2] = loop2Stop;
      loopStep[2] = loop2Step;
      loopPtsBox[2] = loop2PtsBox;
      loopLoadPoints[2] = loop2LoadPoints;
      loopFromLabel[2] = loop2FromLabel;
      loopToLabel[2] = loop2ToLabel;
      loopStepsLabel[2] = loop2StepsLabel;
      loopPtlLabel[2] = loop2PtlLabel;
      loopPostActLabel[2] = loop2PostActLabel;
      LoopBox[2] = loop2BoxPage;

      QFont m_feMask_font( m_feMaskAllEnable->font() );
 
      QGridLayout *layout98=new QGridLayout( m_feMaskButtonGroup );//, 2, 8, 4, 4, "layout98");
      layout98->setSizeConstraint(QLayout::SetMinimumSize);
      m_feMaskButtonGroup->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);//, true);
      m_feMask_font.setPointSize( 7 );
      QRect rect;
      int margin=0;
      for (unsigned int fe_i=0; fe_i<16; fe_i++) {
	{
	  m_feMaskButton[fe_i] = new FEMaskButton(m_feMaskButtonGroup, this, fe_i); m_feMaskButton[fe_i]->show();
	}
	{
	  std::stringstream feMaskButtonText;
	  feMaskButtonText << fe_i;
	  m_feMaskButton[fe_i]->setText( feMaskButtonText.str().c_str() ); 
	}
	m_feMaskButton[fe_i]->setFont( m_feMask_font ); 
	//m_feMaskButton[fe_i]->setFlat( TRUE );
	//    m_feMaskButton[fe_i]->setLineWidth(1);
	//m_feMaskButton[fe_i]->setPaletteBackgroundColor( QColor( 200, 200, 200 ) );


	m_feMaskButton[fe_i]->setStyleSheet(QString::fromUtf8("background-color: rgb( 0, 255, 0);"));
	m_feMaskButton[fe_i]->update();   
	if (rect.width()<=0 || rect.height()<=0) {
	  rect=m_feMaskButton[fe_i]->fontMetrics().boundingRect(QString("99"));
	  //	  std::cout << " min width = " << rect.width() << ", " << rect.height() << std::endl;
	  margin=static_cast<int>(.2*rect.height()+.5+2);
	}
	if (rect.width()>0 || rect.height()>0) {
	  m_feMaskButton[fe_i]->setFixedWidth(rect.width()+margin*2);
	  m_feMaskButton[fe_i]->setFixedHeight(rect.height()+margin*2);
	}
    
	m_feMaskButton[fe_i]->setEnabled(false);
	unsigned int row_i=fe_i/8;
	unsigned int col_i=(row_i>=1 ? 7-(fe_i%8) : fe_i%8);
	// std::cout << feMaskButtonName.str() << " : " << col_i <<" , " << row_i << std::endl;
	layout98->addWidget( m_feMaskButton[fe_i], 2-row_i, col_i+1 );
      }

      connect(m_feMaskAllDisable,SIGNAL(clicked()),this,SLOT(feMaskAllDisabled()));
      connect(m_feMaskAllEnable,SIGNAL(clicked()),this,SLOT(feMaskAllEnabled()));
      feMaskAllEnabled();
     // m_feMaskAllEnable->setPaletteBackgroundColor( QColor(150, 230, 150 ) );
     // m_feMaskAllDisable->setPaletteBackgroundColor( QColor(210, 210, 210 ) );
      m_feMaskAllEnable->setStyleSheet(QString::fromUtf8("QPushButton{background-color: rgb(  150, 230, 150);}"));
      m_feMaskAllDisable->setStyleSheet(QString::fromUtf8("QPushButton{background-color: rgb( 210, 210, 210);}"));
      m_feMaskAllEnable->update();
      m_feMaskAllDisable->update(); 
      m_feMaskAllEnable->setEnabled(false);
      m_feMaskAllDisable->setEnabled(false);
      // fill map of known scan option handles
      m_knownHandles.insert(std::make_pair("general_repetitions",(QObject*)nEvents));
      m_knownHandles.insert(std::make_pair("general_restoreModuleConfig",(QObject*)restorConfig));
      m_knownHandles.insert(std::make_pair("general_useEmulator",(QObject*)useEmulator));
      m_knownHandles.insert(std::make_pair("general_nHitsEmu",(QObject*)setHitEmu));
      m_knownHandles.insert(std::make_pair("general_maskStageSteps",(QObject*)nMaskSteps));
      m_knownHandles.insert(std::make_pair("general_maskStageTotalSteps",(QObject*)maskStage));
      m_knownHandles.insert(std::make_pair("general_maskStageMode",(QObject*)maskMode));
      m_knownHandles.insert(std::make_pair("general_runType",(QObject*)runType));
      m_knownHandles.insert(std::make_pair("general_scanFEbyFE",(QObject*)snglfeMode));
      m_knownHandles.insert(std::make_pair("general_useAltModLinks",(QObject*)useAltModLinks));
      m_knownHandles.insert(std::make_pair("general_tuningStartsFromConfigValues",(QObject*)tuningStartsFromConfigValues));
      m_knownHandles.insert(std::make_pair("general_useTuningPointsFile",(QObject*)useTuningPointsFile));
      m_knownHandles.insert(std::make_pair("general_tuningPointsFile",(QObject*)tuningPointsFile));
      m_knownHandles.insert(std::make_pair("trigger_consecutiveLevl1TrigA_0",(QObject*)nAccepts_A0));
      m_knownHandles.insert(std::make_pair("trigger_strobeLVL1Delay",(QObject*)l1Delay));
      m_knownHandles.insert(std::make_pair("trigger_LVL1Latency",(QObject*)trgLatency));
      m_knownHandles.insert(std::make_pair("trigger_strobeLVL1DelayOverride",(QObject*)overwL1delBox));
      m_knownHandles.insert(std::make_pair("trigger_superGroupTrigDelay",(QObject*)superGrpDelay));
      m_knownHandles.insert(std::make_pair("trigger_self",(QObject*)selfTrigger));
      m_knownHandles.insert(std::make_pair("trigger_strobeDuration",(QObject*)strDuration));
      m_knownHandles.insert(std::make_pair("trigger_strobeMCCDelay",(QObject*)strDelay));
      m_knownHandles.insert(std::make_pair("trigger_overrideStrobeMCCDelay",(QObject*)fixedSdelBox));
      m_knownHandles.insert(std::make_pair("trigger_strobeMCCDelayRange",(QObject*)strDelRange));
      m_knownHandles.insert(std::make_pair("trigger_overrideStrobeMCCDelayRange",(QObject*)fixedSrangeBox));
      m_knownHandles.insert(std::make_pair("fe_digitalInjection",(QObject*)injectionMode));
      m_knownHandles.insert(std::make_pair("fe_chargeInjCapHigh",(QObject*)capLowHigh));
      m_knownHandles.insert(std::make_pair("fe_vCal",(QObject*)setVcal));
      m_knownHandles.insert(std::make_pair("fe_columnROFreq",(QObject*)PhiClock));
      m_knownHandles.insert(std::make_pair("fe_totThrMode",(QObject*)TOTmode));
      m_knownHandles.insert(std::make_pair("fe_totMin",(QObject*)minTOT));
      m_knownHandles.insert(std::make_pair("fe_totDHThr",(QObject*)dblTOT));
      m_knownHandles.insert(std::make_pair("fe_totTimeStampMode",(QObject*)TOTLEmode));
      m_knownHandles.insert(std::make_pair("mcc_mccBandwidth",(QObject*)MCC_Bwidth));
      m_knownHandles.insert(std::make_pair("histograms_histogramFilledLVL1",(QObject*)lvl1Fill));
      m_knownHandles.insert(std::make_pair("histograms_histogramKeptLVL1",(QObject*)lvl1Keep));
      m_knownHandles.insert(std::make_pair("histograms_histogramFilledOCCUPANCY",(QObject*)occFill));
      m_knownHandles.insert(std::make_pair("histograms_histogramKeptOCCUPANCY",(QObject*)occKeep));
      m_knownHandles.insert(std::make_pair("histograms_histogramFilledTOT_MEAN",(QObject*)totmeanFill));
      m_knownHandles.insert(std::make_pair("histograms_histogramKeptTOT_MEAN",(QObject*)totmeanKeep));
      m_knownHandles.insert(std::make_pair("histograms_histogramFilledTOT_SIGMA",(QObject*)totsigFill));
      m_knownHandles.insert(std::make_pair("histograms_histogramKeptTOT_SIGMA",(QObject*)totsigKeep));
      m_knownHandles.insert(std::make_pair("histograms_histogramFilledFDAC_T",(QObject*)fdactfill));
      m_knownHandles.insert(std::make_pair("histograms_histogramKeptFDAC_T",(QObject*)fdactkeep));
      m_knownHandles.insert(std::make_pair("histograms_histogramFilledFDAC_TOT",(QObject*)fdactotfill));
      m_knownHandles.insert(std::make_pair("histograms_histogramKeptFDAC_TOT",(QObject*)fdactotkeep));
      m_knownHandles.insert(std::make_pair("histograms_histogramFilledGDAC_T",(QObject*)gdactfill));
      m_knownHandles.insert(std::make_pair("histograms_histogramKeptGDAC_T",(QObject*)gdactkeep));
      m_knownHandles.insert(std::make_pair("histograms_histogramFilledGDAC_THR",(QObject*)gdacthrfill));
      m_knownHandles.insert(std::make_pair("histograms_histogramKeptGDAC_THR",(QObject*)gdacthrkeep));
      m_knownHandles.insert(std::make_pair("histograms_histogramFilledIF_T",(QObject*)iftfill));
      m_knownHandles.insert(std::make_pair("histograms_histogramKeptIF_T",(QObject*)iftkeep));
      m_knownHandles.insert(std::make_pair("histograms_histogramFilledIF_TOT",(QObject*)iftotfill));
      m_knownHandles.insert(std::make_pair("histograms_histogramKeptIF_TOT",(QObject*)iftotkeep));
      m_knownHandles.insert(std::make_pair("histograms_histogramFilledSCURVE_CHI2",(QObject*)scurvechi2fill));
      m_knownHandles.insert(std::make_pair("histograms_histogramKeptSCURVE_CHI2",(QObject*)scurvechi2keep));
      m_knownHandles.insert(std::make_pair("histograms_histogramFilledSCURVE_MEAN",(QObject*)scurvemeanfill));
      m_knownHandles.insert(std::make_pair("histograms_histogramKeptSCURVE_MEAN",(QObject*)scurvemeankeep));
      m_knownHandles.insert(std::make_pair("histograms_histogramFilledSCURVE_SIGMA",(QObject*)scurvesigmafill));
      m_knownHandles.insert(std::make_pair("histograms_histogramKeptSCURVE_SIGMA",(QObject*)scurvesigmakeep));
      m_knownHandles.insert(std::make_pair("histograms_histogramFilledTDAC_T",(QObject*)tdactfill));
      m_knownHandles.insert(std::make_pair("histograms_histogramKeptTDAC_T",(QObject*)tdactkeep));
      m_knownHandles.insert(std::make_pair("histograms_histogramFilledTDAC_THR",(QObject*)tdacthrfill));
      m_knownHandles.insert(std::make_pair("histograms_histogramKeptTDAC_THR",(QObject*)tdacthrkeep));
      m_knownHandles.insert(std::make_pair("histograms_histogramFilledTIMEWALK",(QObject*)twfill));
      m_knownHandles.insert(std::make_pair("histograms_histogramKeptTIMEWALK",(QObject*)twkeep));

      m_knownHandles.insert(std::make_pair("histograms_histogramFilledRAW_DATA_0",(QObject*)rd0fill));
      m_knownHandles.insert(std::make_pair("histograms_histogramKeptRAW_DATA_0",(QObject*)rd0keep));
      m_knownHandles.insert(std::make_pair("histograms_histogramFilledRAW_DATA_1",(QObject*)rd1fill));
      m_knownHandles.insert(std::make_pair("histograms_histogramKeptRAW_DATA_1",(QObject*)rd1keep));
      m_knownHandles.insert(std::make_pair("histograms_histogramFilledRAW_DATA_2",(QObject*)rd2fill));
      m_knownHandles.insert(std::make_pair("histograms_histogramKeptRAW_DATA_2",(QObject*)rd2keep));
      m_knownHandles.insert(std::make_pair("histograms_histogramFilledRAW_DATA_DIFF_1",(QObject*)rdd1fill));
      m_knownHandles.insert(std::make_pair("histograms_histogramKeptRAW_DATA_DIFF_1",(QObject*)rdd1keep));
      m_knownHandles.insert(std::make_pair("histograms_histogramFilledRAW_DATA_DIFF_2",(QObject*)rdd2fill));
      m_knownHandles.insert(std::make_pair("histograms_histogramKeptRAW_DATA_DIFF_2",(QObject*)rdd2keep));
      m_knownHandles.insert(std::make_pair("histograms_histogramFilledRAW_DATA_REF",(QObject*)rdrfill));
      m_knownHandles.insert(std::make_pair("histograms_histogramKeptRAW_DATA_REF",(QObject*)rdrkeep));
      m_knownHandles.insert(std::make_pair("histograms_histogramFilledFMTC_LINKMAP",(QObject*)fclfill));
      m_knownHandles.insert(std::make_pair("histograms_histogramKeptFMTC_LINKMAP",(QObject*)fclkeep));
      m_knownHandles.insert(std::make_pair("histograms_histogramFilledFMT_COUNTERS",(QObject*)fctfill));
      m_knownHandles.insert(std::make_pair("histograms_histogramKeptFMT_COUNTERS",(QObject*)fctkeep));

      for(unsigned int sci=0;sci<MAX_LOOPS;sci++){
	m_knownHandles.insert(std::make_pair(("loops_activeLoop_"+QString::number(sci)).toLatin1().data(),
					     (QObject*)loopActive[sci]));
	m_knownHandles.insert(std::make_pair(("loops_dspProcessingLoop_"+QString::number(sci)).toLatin1().data(),
					     (QObject*)loopDspProc[sci]));
	m_knownHandles.insert(std::make_pair(("loops_paramLoop_"+QString::number(sci)).toLatin1().data(),
					     (QObject*)loopType[sci]));
	m_knownHandles.insert(std::make_pair(("loops_loopVarNStepsLoop_"+QString::number(sci)).toLatin1().data(),
					     (QObject*)loopStep[sci]));
	m_knownHandles.insert(std::make_pair(("loops_loopVarMinLoop_"+QString::number(sci)).toLatin1().data(),
					     (QObject*)loopStart[sci]));
	m_knownHandles.insert(std::make_pair(("loops_loopVarMaxLoop_"+QString::number(sci)).toLatin1().data(),
					     (QObject*)loopStop[sci]));
	m_knownHandles.insert(std::make_pair(("loops_loopVarUniformLoop_"+QString::number(sci)).toLatin1().data(),
					     (QObject*)loopRegular[sci]));
	m_knownHandles.insert(std::make_pair(("loops_endActionLoop_"+QString::number(sci)).toLatin1().data(),
					     (QObject*)loopEndAction[sci]));
	m_knownHandles.insert(std::make_pair(("loops_dspActionLoop_"+QString::number(sci)).toLatin1().data(),
					     (QObject*)loopOnDsp[sci]));
	m_knownHandles.insert(std::make_pair(("loops_loopVarValuesFreeLoop_"+QString::number(sci)).toLatin1().data(),
					     (QObject*)loopFree[sci]));
	m_knownHandles.insert(std::make_pair(("loops_loopVarValuesLoop_"+QString::number(sci)).toLatin1().data(),
					     (QObject*)loopPtsBox[sci]));
      }
      m_knownHandles.insert(std::make_pair("loops_fitMethod",(QObject*)loopFitMethod ));
      m_knownHandles.insert(std::make_pair("loops_innerLoopSwap",(QObject*)stageAdvBox));
      m_knownHandles.insert(std::make_pair("loops_dspMaskStaging",(QObject*)maskOnDsp));

      m_knownHandles.insert(std::make_pair("scans_thresholdTargedValue",(QObject*)thresvalue));
      m_knownHandles.insert(std::make_pair("scans_totTargedCharge",(QObject*)totcharge));
      m_knownHandles.insert(std::make_pair("scans_totTargedValue",(QObject*)totvalue));
      m_knownHandles.insert(std::make_pair("scans_fastThrUsePseudoPix",(QObject*)fastThrPix));

      // unused handles
      staticMode->hide();
      ErrorBox->hide();
      MCC_ErrorFlag->hide();
      MCC_FECheck->hide();
      MCC_TimeStampComp->hide();

      showLoop(0);
      updateConfig();
  
    }
  PixScanPanel::~PixScanPanel(){
  }



  void PixScanPanel::test(int /*value*/) {
    //    std::cout << "INFO [PixScanPanel::test] new value " << value << std::endl;
  }


  void PixScanPanel::feMaskToggled(bool new_state_i, int fe_i) {
    if (!m_scanConfig) return;

    //    PixA::ConfigRef config_ref = m_scanConfig.ref();
    //    PixLib::Config &myconf = config_ref.config();
    PixLib::Config &myconf = m_pixScan->config();
      
    PixLib::ConfObj &obj=myconf["general"]["scanFEbyFEMask"];
    if (obj.type() == PixLib::ConfObj::INT) {
      PixLib::ConfInt *int_obj=static_cast<PixLib::ConfInt*>(&obj);
      int &value=int_obj->value();
      int mask=1<<fe_i;
      int new_val=new_state_i;
      new_val <<= fe_i;
      value = (value & (~mask )) | new_val;
    }
    else {
      std::cerr << "ERROR [PixScanPanel::feMaskToggled] ConfObj general scanFEbyFEMask is not of type int." << std::endl;
    }
  }

  void PixScanPanel::feMaskAllEnabled() {
    //  std::cout << "enable all" << std::endl;
    for (unsigned int fe_i=0; fe_i<16; fe_i++) {
      m_feMaskButton[fe_i]->feMaskToggled(false);
    }
  }

  void PixScanPanel::feMaskAllDisabled() {
    //  std::cout << "disable all" << std::endl;
    for (unsigned int fe_i=0; fe_i<16; fe_i++) {
      m_feMaskButton[fe_i]->feMaskToggled(true);


    }
  }


//   void PixScanPanel::loadConfig()
//   {
    //   Config &opts = m_engine.getOptions();
    //   std::string defPath = ((PixLib::ConfString&)opts["paths"]["defCfgPath"]).value();
    //   QString qpath = QString::null;
    //   if(defPath!=".") qpath = defPath.c_str();
    //   QStringList filter;
    //   filter += "DB ROOT config file (*.cfg.root)";
    //   filter += "Any file (*.*)";
    //   QFileDialog fdia(qpath,QString::null,this,"scanrootfile",TRUE);
    //   fdia.setCaption("Specify name of DB file with PixScan entries");
    //   fdia.setFilters(filter);
    //   if(fdia.exec() == QDialog::Accepted)
    //     m_engine.loadScanCfg(fdia.selectedFile().toLatin1().data());
    //   return;
  //  }

  //  void PixScanPanel::saveConfig()
  //  {
    //   Config &opts = m_engine.getOptions();
    //   std::string defPath = ((PixLib::ConfString&)opts["paths"]["defCfgPath"]).value();
    //   QString qpath = QString::null;
    //   if(defPath!=".") qpath = defPath.c_str();
    //   QStringList filter;
    //   filter += "DB ROOT config file (*.cfg.root)";
    //   filter += "Any file (*.*)";
    //   QFileDialog fdia(qpath,QString::null,this,"scanrootfile",TRUE);
    //   fdia.setCaption("Specify name of DB file to save PixScan entries");
    //   fdia.setFilters(filter);
    //   fdia.setMode(QFileDialog::AnyFile);
    //   if(fdia.exec() == QDialog::Accepted)
    //     m_engine.savePixScan(fdia.selectedFile().toLatin1().data());
    //   return;
  //  }


  void PixScanPanel::updateConfig(){
    //  snglfeMode->setChecked(false);
    updateConfigHandlers();
  }
  void PixScanPanel::updateConfigHandlers(){

    
    if (!m_scanConfig) return;

    //    PixA::ConfigRef config_ref = m_scanConfig.ref();
    //    PixLib::Config &myconf = config_ref.config();
    PixLib::Config &myconf = m_pixScan->config();
   // configParameterTable->setRowCount(0);

    // make sure none of the handles disturbs loading
    std::map<std::string, QObject*>::const_iterator hndlIT;
    for(hndlIT = m_knownHandles.begin(); hndlIT!=m_knownHandles.end();hndlIT++){
      if(dynamic_cast<QSpinBox*>(hndlIT->second) != 0)
	disconnect( hndlIT->second, SIGNAL( valueChanged(int) ), this, SLOT( readFromHandles() ) );
      if(dynamic_cast<QComboBox*>(hndlIT->second) != 0)
	disconnect( hndlIT->second, SIGNAL( activated(int) ), this, SLOT( readFromHandles() ) );
      if(dynamic_cast<QCheckBox*>(hndlIT->second) != 0)
	disconnect( hndlIT->second, SIGNAL( toggled(bool) ), this, SLOT( readFromHandles() ) );
      if(dynamic_cast<QLineEdit*>(hndlIT->second) != 0)
	disconnect( hndlIT->second, SIGNAL( textChanged(const QString &) ), this, SLOT( readFromHandles() ) );
    }
    disconnect(configParameterTable, SIGNAL(valueChanged(int, int)), this, SLOT ( readFromHandlersFromTable (int, int)));

    // loop over config items and see if it's in our "known" list
    // Loop over the ConfGroups
    assert(myconf.size()>=0);
    for (unsigned int i=0; i<static_cast<unsigned int>(myconf.size()); i++) {
      PixLib::ConfGroup &grp = myconf[i];
      // Loop over the ConfObj
      for (size_t j=0; j<grp.size(); j++) {
    QObject *handle = m_knownHandles[grp[j].name()];
    if(handle!=0){ // yeah, found something!
          ((QWidget*)handle)->setToolTip( grp[j].comment().c_str() );
	  switch( grp[j].type() ) {
	  case PixLib::ConfObj::INT :{
	    int ival=0;
	    switch( grp[j].subtype() ){
	    case PixLib::ConfInt::S32:
	      ival = (int)((PixLib::ConfInt&)grp[j]).valueS32();
	      break;
	    case PixLib::ConfInt::U32:
	      ival = (int)((PixLib::ConfInt&)grp[j]).valueU32();
	      break;
	    case PixLib::ConfInt::S16:
	      ival = (int)((PixLib::ConfInt&)grp[j]).valueS16();
	      break;
	    case PixLib::ConfInt::U16:
	      ival = (int)((PixLib::ConfInt&)grp[j]).valueU16();
	      break;
	    case PixLib::ConfInt::S8:
	      ival = (int)((PixLib::ConfInt&)grp[j]).valueS8();
	      break;
	    case PixLib::ConfInt::U8:
	      ival = (int)((PixLib::ConfInt&)grp[j]).valueU8();
	      break;
	    default: ;
	    }
	    QSpinBox *sbhandle = dynamic_cast<QSpinBox*>(handle);
	    QCheckBox *tbhandle = dynamic_cast<QCheckBox*>(handle);
	    QComboBox *cbhandle = dynamic_cast<QComboBox*>(handle);
	    if(sbhandle!=0)
	      sbhandle->setValue(ival);
	    else if(tbhandle!=0)
	      tbhandle->setChecked((bool)ival);
	    else if(cbhandle!=0)
	      cbhandle->setCurrentIndex(ival);
	    break;}
	  case PixLib::ConfObj::LIST : {
	    QComboBox *cbhandle = dynamic_cast<QComboBox*>(handle);
	    QCheckBox *tbhandle = dynamic_cast<QCheckBox*>(handle);
	    if(cbhandle!=0){
	      cbhandle->clear();
	      int currID=0;
	      std::map<std::string, int>::const_iterator mapIT;
	      for(mapIT = ((PixLib::ConfList&)grp[j]).m_symbols.begin(); mapIT != ((PixLib::ConfList&)grp[j]).m_symbols.end();mapIT++){
		cbhandle->addItem(mapIT->first.c_str());
		// std::cout << "INFO [PixScanPanel::updateConfigHandlers] " << mapIT->first << " =?= " << ((PixLib::ConfList&)grp[j]).sValue() << "." << std::endl;
		if(mapIT->first==((PixLib::ConfList&)grp[j]).sValue())
		  currID = cbhandle->count()-1;
	      }
	      cbhandle->setCurrentIndex(currID);
	    } else if(tbhandle!=0){
	      tbhandle->setChecked((bool)((PixLib::ConfList&)grp[j]).getValue());
	    }
	    break;}
	  case PixLib::ConfObj::BOOL : {
	    QCheckBox *cbhandle = dynamic_cast<QCheckBox*>(handle);
	    if(cbhandle!=0)
	      cbhandle->setChecked((bool)((PixLib::ConfBool&)grp[j]).value());
	    break;}
	  case PixLib::ConfObj::VECTOR : {
	    QListWidget *lbhandle = dynamic_cast<QListWidget*>(handle);//
	    if(lbhandle!=0){
	      lbhandle->clear();
	      switch( grp[j].subtype() ){
	      case PixLib::ConfVector::V_INT:{
		std::vector<int> &tmpVec = ((PixLib::ConfVector&)grp[j]).valueVInt();
		for(std::vector<int>::iterator IT = tmpVec.begin(); IT!=tmpVec.end(); IT++)
		  lbhandle->addItem(QString::number(*IT));
		break;}
	      case PixLib::ConfVector::V_UINT:{
		std::vector<unsigned int> &tmpVec = ((PixLib::ConfVector&)grp[j]).valueVUint();
		for(std::vector<unsigned int>::iterator IT = tmpVec.begin(); IT!=tmpVec.end(); IT++)
		  lbhandle->addItem(QString::number(*IT));
		break;}
	      case PixLib::ConfVector::V_FLOAT:{
		std::vector<float> &tmpVec = ((PixLib::ConfVector&)grp[j]).valueVFloat();
		for(std::vector<float>::iterator IT = tmpVec.begin(); IT!=tmpVec.end(); IT++)
		  lbhandle->addItem(QString::number(*IT, 'f',2));
		break;}
	      default:
		std::cout << "ERROR [PixScanPanel::updateConfigHandlers]  found unhandled config type for item "
			  << grp[j].name() << "." << std::endl;
	      }
	    }
	    break;}
	  case PixLib::ConfObj::FLOAT :{
	    QSpinBox *sbhandle = dynamic_cast<QSpinBox*>(handle);
	    if(sbhandle!=0)
	      sbhandle->setValue((int)((PixLib::ConfFloat&)grp[j]).value());
	    break;}
	  case PixLib::ConfObj::STRING : {
	    QLineEdit *lehandle = dynamic_cast<QLineEdit*>(handle);
	    
	    if(lehandle!=0) {
	      lehandle->setText( std::string (((PixLib::ConfString&)grp[j]).m_value ).c_str());
	      std::cout<<"DEBUG [PixScanPanel::updateConfigHandlers] PF::updateHandles::PF::FileLocation ********"<<(std::string)lehandle->text().toLatin1().data()<<"**************"<<std::endl;
	      }
	      //((PixLib::ConfString&)grp[j]).m_value = (std::string)lehandle->text().toLatin1().data(); }
	    break;}
	  case PixLib::ConfObj::VOID :
	  default:
	    std::cout << "ERROR [PixScanPanel::updateConfigHandlers]  found unknown/unhandled config type for item "
		      << grp[j].name() << "." << std::endl;
	  }
	} else{ // dump info to table

      if(grp[j].visible() && ( // do not list items that are auto-generated by us:
				  (grp[j].name().find("modGroups_mod")==std::string::npos && 
				   grp[j].name().find("modGroups_config")==std::string::npos && 
				   grp[j].name().find("modGroups_trigger")==std::string::npos && 
				   grp[j].name().find("modGroups_readout")==std::string::npos && 
				   grp[j].name().find("modGroups_strobe")==std::string::npos
                   ) /* || !(generSDSPcfg->isChecked()) @todo what todo about this*/) ){


          int k = configParameterTable->rowCount();
        QString typeStr, valStr;
	    switch( grp[j].type() ) {
	    case PixLib::ConfObj::INT : {
	      typeStr = "INT"; 
	      int ival=0;
	      switch( grp[j].subtype() ){
	      case PixLib::ConfInt::S32:
		ival = (int)((PixLib::ConfInt&)grp[j]).valueS32();
		break;
	      case PixLib::ConfInt::U32:
		ival = (int)((PixLib::ConfInt&)grp[j]).valueU32();
		break;
	      case PixLib::ConfInt::S16:
		ival = (int)((PixLib::ConfInt&)grp[j]).valueS16();
		break;
	      case PixLib::ConfInt::U16:
		ival = (int)((PixLib::ConfInt&)grp[j]).valueU16();
		break;
	      case PixLib::ConfInt::S8:
		ival = (int)((PixLib::ConfInt&)grp[j]).valueS8();
		break;
	      case PixLib::ConfInt::U8:
		ival = (int)((PixLib::ConfInt&)grp[j]).valueU8();
		break;
	      default: ;
	      }
	      valStr  = QString::number(ival);
	      break;}
	    case PixLib::ConfObj::FLOAT : 
	      typeStr = "FLOAT";
	      valStr  = QString::number(((PixLib::ConfFloat&)grp[j]).value(),'f',3);
	      break;
	    case PixLib::ConfObj::LIST : 
	      typeStr = "LIST"; 
	      valStr  = ((PixLib::ConfList&)grp[j]).sValue().c_str();
	      break;
	    case PixLib::ConfObj::BOOL : {
	      bool is_histo=false;
	      if (grp.name()=="histograms") {
		std::string::size_type pos;
		std::string obj_name( grp[j].name() );
		static const std::string::size_type header_end(strlen("histograms_"));
		if ( (pos = obj_name.find("Filled"))!=std::string::npos && pos>header_end) {
		  std::string keep_name/*=grp.name()*/;
		  //		  keep_name+="_";
		  keep_name+=obj_name.substr(header_end,pos-header_end);
		  keep_name+="Kept";
		  keep_name+=obj_name.substr(pos+6,obj_name.size()-pos-6);
		  PixLib::ConfObj &keep_obj=grp[keep_name];
		  if (keep_obj.type() == PixLib::ConfObj::BOOL) {

	            static int label_row = 1;
		    QLabel *histo_label= new QLabel( histoFillBox );
		    histo_label->setObjectName( obj_name.substr(pos+6,obj_name.size()-pos-6).c_str() );
		    histo_label->setText(obj_name.substr(pos+6,obj_name.size()-pos-6).c_str());
		    histoSaveMenu_2->addWidget( histo_label, label_row, 1);//    layout83_2->addWidget( histo_label );

		    QCheckBox *histo_fill = new QCheckBox( histoFillBox );
		    histo_fill->setObjectName( obj_name.c_str() );
		    histo_fill->setChecked( ((PixLib::ConfBool&)grp[j]).value() );
		    histo_fill->setText("fill");
		    histoSaveMenu_2->addWidget( histo_fill, label_row, 2);
		    // remove old handle in cas one existed
		    m_knownHandles[obj_name]=histo_fill;
 		    connect( histo_fill, SIGNAL( toggled(bool) ), this, SLOT( readFromHandles() ) );

		    QCheckBox *histo_keep = new QCheckBox( histoFillBox );
		    histo_keep->setObjectName( keep_name.c_str() );
		    histo_keep->setChecked( ((PixLib::ConfBool&)keep_obj).value() );
		    histo_keep->setText("keep");
		    histoSaveMenu_2->addWidget( histo_keep, label_row, 3 );
		    // remove old handle in cas one existed
		    m_knownHandles[keep_obj.name()]=histo_keep;
		    connect( histo_keep, SIGNAL( toggled(bool) ), this, SLOT( readFromHandles() ) );
		    is_histo=true;
		    label_row++;
		  }
		}
		else if ( (pos=obj_name.find("Kept"))!=std::string::npos && pos>header_end) {
		  std::string name/*=grp.name()*/;
		  //		  name+="_";
		  name+=obj_name.substr(header_end,pos-header_end);
		  name+="Filled";
		  name+=obj_name.substr(pos+4,obj_name.size()-pos-4);
		  if (grp[name].type() == PixLib::ConfObj::BOOL) {
		    is_histo=true;
		  }
		}
	      }

	      if (!is_histo) {
		typeStr = "BOOL";
		valStr  = ((PixLib::ConfBool&)grp[j]).value()?"true":"false";
	      }
	      else {
		typeStr="";
		valStr="";
	      }

	      break;
	    }
	    case PixLib::ConfObj::STRING : 
	      typeStr = "STRING";
	      valStr  = ((PixLib::ConfString&)grp[j]).value().c_str();
	      break;
	    case PixLib::ConfObj::VOID : 
	      typeStr = "VOID"; 
	      valStr  = "void";
	      break;
	    case PixLib::ConfObj::VECTOR :
	      typeStr = "VECTOR"; 
	      valStr  = "vector";
	      break;
	    default: 
	      typeStr = "Unrecognized";
	    }

        if (typeStr.length()>0) {
                //std::cout << "DEBUG [ Table values ] "
                //          << grp[j].name().c_str()
                //          << " "
                //          << grp[j].comment().c_str()
                //          << " "
                //          << i
                //          << " "
                //          << j
                //          << " slotted into "
                //          << k
                //          << std::endl;
                configParameterTable->insertRow(k);
                QTableWidgetItem *col0 = new QTableWidgetItem(grp[j].name().c_str());
                col0->setFlags(col0->flags() & ~Qt::ItemIsEditable);
                configParameterTable->setItem(k,0, col0);
                QTableWidgetItem *col1 = new QTableWidgetItem(grp[j].comment().c_str());
                col1->setFlags(col1->flags() & ~Qt::ItemIsEditable);
                configParameterTable->setItem(k,1, col1);
                QTableWidgetItem *col2 = new QTableWidgetItem(grp[j].comment().c_str());
                col2->setFlags(col2->flags() & ~Qt::ItemIsEditable);
                configParameterTable->setItem(k,2, col2);

	    if ( grp[j].type() == PixLib::ConfObj::BOOL) {
	      std::string temp(grp[j].name());
	      std::string::size_type pos = temp.find("_");
	      if (pos != std::string::npos && pos <temp.size()) {
		temp.erase(0,pos+1);
	      }
          QTableWidgetItem *check_item=new QTableWidgetItem(QString::fromStdString(temp));
          check_item->setFlags(check_item->flags() & ~Qt::ItemIsEditable);
          if (((PixLib::ConfBool&)grp[j]).value() == true){

              check_item->setCheckState(Qt::Checked);
          } else {
              check_item->setCheckState(Qt::Unchecked);
          }
          configParameterTable->setItem(k, 3, check_item );
	    }
        else if ( grp[j].type() != PixLib::ConfObj::LIST && grp[j].type() !=PixLib::ConfObj::VECTOR ) {
          configParameterTable->setItem(k, 3, new QTableWidgetItem(valStr));
        } else if(grp[j].type() == PixLib::ConfObj::LIST){ //deal with LIST
	      QStringList stringList;
	    
	      std::map<std::string, int> stdmap = ((PixLib::ConfList&)grp[j]).symbols();
	      std::map<std::string, int>::iterator pos;
	    
	      for(pos = stdmap.begin(); pos != stdmap.end(); pos ++) {
		stringList << pos->first.c_str();
	      }
	    
              QComboBox *comboItem = new QComboBox();
              comboItem->setContextMenuPolicy(Qt::CustomContextMenu);
              comboItem->addItems(stringList);
              comboItem->setCurrentIndex( ((PixLib::ConfList&)grp[j]).getValue() );
              comboItem->setProperty("row", (int) k);
              connect(comboItem, SIGNAL(customContextMenuRequested(const QPoint &)),this,SLOT(openTableMenu(const QPoint &)));
              configParameterTable->setCellWidget(k,3,comboItem);
        } else { // deal with vectors
	      QStringList stringList;
	      TypeComboTableItem::EType type=TypeComboTableItem::kInt;
	      switch( grp[j].subtype() ){
	      case PixLib::ConfVector::V_INT:{
		std::vector<int> &tmpVec = ((PixLib::ConfVector&)grp[j]).valueVInt();
		for(std::vector<int>::iterator IT = tmpVec.begin(); IT!=tmpVec.end(); IT++)
		  stringList << QString::number(*IT);
        configParameterTable->setItem(k,1, new QTableWidgetItem( "INT VECTOR" ));
		break;}
	      case PixLib::ConfVector::V_UINT:{
		std::vector<unsigned int> &tmpVec = ((PixLib::ConfVector&)grp[j]).valueVUint();
		for(std::vector<unsigned int>::iterator IT = tmpVec.begin(); IT!=tmpVec.end(); IT++)
		  stringList << QString::number(*IT);
        configParameterTable->setItem(k,1,new QTableWidgetItem( "UINT VECTOR" ));
		break;}
	      case PixLib::ConfVector::V_FLOAT:{
		type=TypeComboTableItem::kFloat;
		std::vector<float> &tmpVec = ((PixLib::ConfVector&)grp[j]).valueVFloat();
		for(std::vector<float>::iterator IT = tmpVec.begin(); IT!=tmpVec.end(); IT++)
		  stringList << QString::number(*IT, 'f',2);
        configParameterTable->setItem(k,1, new QTableWidgetItem( "FLOAT VECTOR" ));
		break;}
	      default:
        configParameterTable->setItem(k,1, new QTableWidgetItem( "unknown VECTOR" ));
		stringList << "unknown values";
		break;
	      }
             TypeComboTableItem *comboItem = new TypeComboTableItem ( type, configParameterTable, stringList, false );
             comboItem->setContextMenuPolicy(Qt::CustomContextMenu);
             comboItem->setCurrentIndex( 0 );
             comboItem->setProperty("row", (int) k);
             connect(comboItem, SIGNAL(customContextMenuRequested(const QPoint &)),this,SLOT(editVector(const QPoint &)));
             configParameterTable->setCellWidget(k,3,comboItem);
        }
        }
	  }
	}
      }
    }

    // make sure all related settings on loop panels are correct
    for(unsigned int sci=0;sci<MAX_LOOPS;sci++)
      updateLoopSettings(sci);

    // restore all connections
    for(hndlIT = m_knownHandles.begin(); hndlIT!=m_knownHandles.end();hndlIT++){
      if(dynamic_cast<QSpinBox*>(hndlIT->second) != 0)
	connect( hndlIT->second, SIGNAL( valueChanged(int) ), this, SLOT( readFromHandles() ) );
      if(dynamic_cast<QComboBox*>(hndlIT->second) != 0)
	connect( hndlIT->second, SIGNAL( activated(int) ), this, SLOT( readFromHandles() ) );
      if(dynamic_cast<QCheckBox*>(hndlIT->second) != 0)
	connect( hndlIT->second, SIGNAL( toggled(bool) ), this, SLOT( readFromHandles() ) );
      if(dynamic_cast<QLineEdit*>(hndlIT->second) != 0)
	connect( hndlIT->second, SIGNAL( textChanged(const QString &) ), this, SLOT( readFromHandles() ) );
    }
    connect(configParameterTable, SIGNAL(valueChanged(int, int)), this, SLOT (readFromHandlersFromTable (int, int)));

    {
      PixLib::ConfObj &obj=myconf["general"]["scanFEbyFEMask"];
      if (obj.type() == PixLib::ConfObj::INT) {
	PixLib::ConfInt *int_obj=static_cast<PixLib::ConfInt*>(&obj);

	unsigned int mask_i=1;
	int &value=int_obj->value();
#ifdef MASK_IN_FE_BY_FE_ONLY
	bool fe_by_fe=snglfeMode->isChecked();
#else
	bool fe_by_fe=true;
#endif
	m_feMaskAllEnable->setEnabled(fe_by_fe);
	m_feMaskAllDisable->setEnabled(fe_by_fe);
	if (fe_by_fe)  {
	//  m_feMaskAllEnable->setPaletteBackgroundColor( QColor(0, 230, 0 ) );
	 // m_feMaskAllDisable->setPaletteBackgroundColor( QColor(180, 180, 180 ) );


m_feMaskAllEnable->setStyleSheet(QString::fromUtf8("QPushButton{background-color: rgb( 0, 230, 0 );}"));

m_feMaskAllDisable->setStyleSheet(QString::fromUtf8("QPushButton{background-color: rgb(180, 180, 180  );}"));
    m_feMaskAllEnable->update();   m_feMaskAllDisable->update(); 
	}
	else {
	//  m_feMaskAllEnable->setPaletteBackgroundColor( QColor(150, 230, 150 ) );
	 // m_feMaskAllDisable->setPaletteBackgroundColor( QColor(210, 210, 210 ) );


m_feMaskAllEnable->setStyleSheet(QString::fromUtf8("QPushButton{background-color: rgb(150, 230, 150  );}"));


m_feMaskAllDisable->setStyleSheet(QString::fromUtf8("QPushButton{background-color: rgb(210, 210, 210 );}"));
    m_feMaskAllEnable->update();   m_feMaskAllDisable->update(); 
	}

	for (unsigned int fe_i=0; fe_i<16; fe_i++) {
	  m_feMaskButton[fe_i]->setEnabled(fe_by_fe); 
	  m_feMaskButton[fe_i]->feMaskToggled( (value & mask_i)==0 );
	  mask_i <<= 1;
	}
      }
    }
    return;
  }

  void PixScanPanel::readFromHandles(){
    
    
    
    if (!m_scanConfig) return;

    //    PixA::ConfigRef config_ref = m_scanConfig.ref();
    //    PixLib::Config &myconf = config_ref.config();
    PixLib::Config &myconf = m_pixScan->config();

    for(unsigned int sci=0;sci<MAX_LOOPS;sci++){
      updateLoopSettings(sci);
      updateLoopPts(sci);
    }


    // loop over config items and see if it's in our "known" list
    // Loop over the PixLib::ConfGroups
    assert(myconf.size()>=0);
    for (unsigned int i=0; i<static_cast<unsigned int>(myconf.size()); i++) {
      PixLib::ConfGroup &grp = myconf[i];
      // Loop over the PixLib::ConfObj
      for (size_t j=0; j<grp.size(); j++) {
	QObject *handle = m_knownHandles[grp[j].name()];
	if(handle!=0){ // yeah, found something!
	  switch( grp[j].type() ) {
	  case PixLib::ConfObj::INT :{
	    int read_val=-1;
	    QSpinBox *sbhandle = dynamic_cast<QSpinBox*>(handle);
	    QCheckBox *tbhandle = dynamic_cast<QCheckBox*>(handle);
	    QComboBox *cbhandle = dynamic_cast<QComboBox*>(handle);
	    
	    if(sbhandle!=0)
	      read_val = sbhandle->value();
	    else if(tbhandle!=0)
	      read_val = (int) tbhandle->isChecked();
	    else if(cbhandle!=0)
	      read_val = (int) cbhandle->currentIndex();
	    else{
	      std::cout << "ERROR [PixScanPanel::updateConfigHandlers]  found unhandled config type for item "
			<< grp[j].name() << "." << std::endl;
	      break;
	    }
	    switch( grp[j].subtype() ){
	    case PixLib::ConfInt::S32:
	      *((int *)((PixLib::ConfInt&)grp[j]).m_value) = (int)read_val;
	      break;
	    case PixLib::ConfInt::U32:
	      *((unsigned int *)((PixLib::ConfInt&)grp[j]).m_value) = (unsigned int)read_val;
	      break;
	    case PixLib::ConfInt::S16:
	      *((short int *)((PixLib::ConfInt&)grp[j]).m_value) = (short int)read_val;
	      break;
	    case PixLib::ConfInt::U16:
	      *((unsigned short int *)((PixLib::ConfInt&)grp[j]).m_value) = (unsigned short int)read_val;
	      break;
	    case PixLib::ConfInt::S8:
	      *((char *)((PixLib::ConfInt&)grp[j]).m_value) = (char)read_val;
	      break;
	    case PixLib::ConfInt::U8:
	      *((unsigned char *)((PixLib::ConfInt&)grp[j]).m_value) = (unsigned char)read_val;
	      break;
	    default: ;
	    }
	    break;}
	  case PixLib::ConfObj::LIST :{
	    int read_val = 0;
	    QComboBox *cbhandle = dynamic_cast<QComboBox*>(handle);
	    QCheckBox *tbhandle = dynamic_cast<QCheckBox*>(handle);
	    if(cbhandle!=0){
	      read_val = (int) ((PixLib::ConfList&)grp[j]).m_symbols[cbhandle->currentText().toLatin1().data()];
	    }else if(tbhandle!=0){
	      read_val = (int) tbhandle->isChecked();
	    } else{
	      std::cout << "ERROR [PixScanPanel::updateConfigHandlers]  found unhandled config type for item "
			<< grp[j].name() << "." << std::endl;
	      break;
	    }
	    switch( grp[j].subtype() ){
	    case PixLib::ConfList::S32:
	      *((int *)((PixLib::ConfList&)grp[j]).m_value) = (int) read_val;
	      break;
	    case PixLib::ConfList::U32:
	      *((unsigned int *)((PixLib::ConfList&)grp[j]).m_value) = (unsigned int) read_val;
	      break;
	    case PixLib::ConfList::S16:
	      *((short int *)((PixLib::ConfList&)grp[j]).m_value) = (short int) read_val;
	      break;
	    case PixLib::ConfList::U16:
	      *((unsigned short int *)((PixLib::ConfList&)grp[j]).m_value) = (unsigned short int) read_val;
	      break;
	    case PixLib::ConfList::S8:
	      *((char *)((PixLib::ConfList&)grp[j]).m_value) = (char) read_val;
	      break;
	    case PixLib::ConfList::U8:
	      *((unsigned char *)((PixLib::ConfList&)grp[j]).m_value) = (unsigned char) read_val;
	      break;
	    default:
	      std::cout << "ERROR [PixScanPanel::updateConfigHandlers]  found unhandled config type for item "
			<< grp[j].name() << "." << std::endl;
	    }
	    break;}
	  case PixLib::ConfObj::BOOL :{
	    QCheckBox *tbhandle = dynamic_cast<QCheckBox*>(handle);
	    if(tbhandle!=0)
	      ((PixLib::ConfBool&)grp[j]).m_value = tbhandle->isChecked();
	    break;}
	  case PixLib::ConfObj::VECTOR : {
	    QListWidget *lbhandle = dynamic_cast<QListWidget*>(handle);
	    if(lbhandle!=0){
	      bool isOK;
	      switch( grp[j].subtype() ){
	      case PixLib::ConfVector::V_INT:{
		int value;
		std::vector<int> &tmpVec = ((PixLib::ConfVector&)grp[j]).valueVInt();
		tmpVec.clear();
		for(int sci=0;sci<lbhandle->count();sci++){
		  value = lbhandle->item(sci)->text().toInt(&isOK);//
		  if(isOK) {
		    tmpVec.push_back(value);
		  }
		  else {
		    // try hexedecimal
		    value = lbhandle->item(sci)->text().toInt(&isOK,16);
		    if(isOK) {
		      tmpVec.push_back(value);
		    }
		  }
		}
		break;}
	      case PixLib::ConfVector::V_UINT:{
		unsigned int value;
		std::vector<unsigned int> &tmpVec = ((PixLib::ConfVector&)grp[j]).valueVUint();
		tmpVec.clear();
		for(int sci=0;sci<lbhandle->count();sci++){
		  value = (unsigned int) lbhandle->item(sci)->text().toInt(&isOK);

		  if(isOK) {
		    tmpVec.push_back(value);
		  }
		  else {
		    // try hexedecimal
		    value = lbhandle->item(sci)->text().toInt(&isOK,16);// same here as above
		    if(isOK) {
		      tmpVec.push_back(value);
		    }
		  }
		}
		break;}
	      case PixLib::ConfVector::V_FLOAT:{
		float value;
		std::vector<float> &tmpVec = ((PixLib::ConfVector&)grp[j]).valueVFloat();
		tmpVec.clear();
		for(int sci=0;sci<lbhandle->count();sci++){
		  value = lbhandle->item(sci)->text().toFloat(&isOK);//
		  if(isOK)
		    tmpVec.push_back(value);
		}
		break;}
	      default:
		std::cout << "ERROR [PixScanPanel::updateConfigHandlers] Found unhandled config type for item: \""  << grp[j].name() << "\".\n";
	      }
	    }
	    break;}
	  case PixLib::ConfObj::FLOAT :{
	    QSpinBox *sbhandle = dynamic_cast<QSpinBox*>(handle);
	    if(sbhandle!=0)
	      ((PixLib::ConfFloat&)grp[j]).m_value = (float)sbhandle->value();
	    break;}
	  case PixLib::ConfObj::STRING : {
	    QLineEdit *lehandle = dynamic_cast<QLineEdit*>(handle);
	    
	    if(lehandle!=0) {
	      std::cout<<"DEBUG [PixScanPanel::updateConfigHandlers] PF::ReadHandles::PF::FileLocation *****" << (std::string)lehandle->text().toLatin1().data()<<"**********"<<std::endl;
	      ((PixLib::ConfString&)grp[j]).m_value = (std::string)lehandle->text().toLatin1().data();}
	    break;}
	  case PixLib::ConfObj::VOID :
	  default:
	    std::cout << "ERROR [PixScanPanel::updateConfigHandlers]  found unhandled config type for item "
		      << grp[j].name() << "." << std::endl;
	  }
	}
      }
    }
    return;
  }
  void PixScanPanel::readFromHandlersFromTable(int row, int){

    if (!m_scanConfig) return;

    //    PixA::ConfigRef config_ref = m_scanConfig.ref();
    //    PixLib::Config &myconf = config_ref.config();
    PixLib::Config &myconf = m_pixScan->config();
    try {
    // Loop over the PixLib::ConfGroups
    assert(myconf.size()>=0);
    for (unsigned int ix=0; ix<static_cast<unsigned int>(myconf.size()); ix++) {
      PixLib::ConfGroup &grp = myconf[ix];

      //    PixLib::ConfObj &obj = grp[configParameterTable->text( row, 0 )]; // find PixLib::ConfObj according to its name
      //std::string obj_name = configParameterTable->text( row, 0 ).toLatin1().data();
      std::string obj_name = static_cast<std::string>(configParameterTable->item(row, 0)->text().toLatin1().data());

      std::string::size_type pos = obj_name.find("_");
      if(pos!=std::string::npos) // remove group name from table
	obj_name.erase(0,pos+1);
      PixLib::ConfObj &obj = grp[obj_name]; // find PixLib::ConfObj according to its name
      //      std::cout << "INFO [PixScanPanel::readFromHandlersFromTable] " << obj_name << " -> " << obj.name()
      //		<< " type = "<<static_cast<int>(obj.type()) << std::endl;
      if (obj.name() == "__TrashConfObj__") continue; //not found, try next PixLib::ConfGroup

      switch( obj.type() ) {
      case PixLib::ConfObj::INT :
	switch( obj.subtype() ){
	    case PixLib::ConfInt::S32:
            confVal<int>(obj)=(configParameterTable->item(row,3)->text()).toInt();
	      break;
	    case PixLib::ConfInt::U32:
          confVal<unsigned int>(obj)=static_cast<unsigned int>((configParameterTable->item(row,3)->text()).toInt());
	      break;
	    case PixLib::ConfInt::S16:
          confVal<short int>(obj)=static_cast<short int>((configParameterTable->item(row,3)->text()).toInt());
	      break;
	    case PixLib::ConfInt::U16:
          confVal<unsigned short int>(obj)=static_cast<unsigned short int>((configParameterTable->item(row,3)->text()).toInt());
	      break;
	    case PixLib::ConfInt::S8:
          confVal<char>(obj)=static_cast<char>((configParameterTable->item(row,3)->text()).toInt());
	      break;
	    case PixLib::ConfInt::U8:
          confVal<unsigned char>(obj)=static_cast<unsigned char>((configParameterTable->item(row,3)->text()).toInt());
	      break;
	    default: ;
	    }
        break;
      case PixLib::ConfObj::FLOAT :
        ((PixLib::ConfFloat&)obj).m_value = (configParameterTable->item(row,3)->text()).toFloat();
        break;
      case PixLib::ConfObj::BOOL : {
        QTableWidgetItem *check_item = dynamic_cast<QTableWidgetItem *>(configParameterTable->item(row,3));
	if (check_item) {
      ((PixLib::ConfBool&)obj).m_value =  check_item->checkState();
	}
	else {
      ((PixLib::ConfBool&)obj).m_value =  ((configParameterTable->item(row,3)->text()).toLower() == "true") ? true : false;
	}
        break;
      }
      case PixLib::ConfObj::STRING :
        ((PixLib::ConfString&)obj).m_value = configParameterTable->item(row,3)->text().toLatin1().data();
        break;
      case PixLib::ConfObj::LIST :{
        QComboBox *cb_new = dynamic_cast<QComboBox *>(configParameterTable->cellWidget(row, 3));
        QString str_val = cb_new->currentText();
        int read_val = (int) ((PixLib::ConfList&)obj).m_symbols[str_val.toLatin1().data()];
        switch( obj.subtype() ){
	case PixLib::ConfList::S32:
	  *((int *)((PixLib::ConfList&)obj).m_value) = (int) read_val;
	  break;
	case PixLib::ConfList::U32:
	  *((unsigned int *)((PixLib::ConfList&)obj).m_value) = (unsigned int) read_val;
	  break;
	case PixLib::ConfList::S16:
	  *((short int *)((PixLib::ConfList&)obj).m_value) = (short int) read_val;
	  break;
	case PixLib::ConfList::U16:
	  *((unsigned short int *)((PixLib::ConfList&)obj).m_value) = (unsigned short int) read_val;
	  break;
	case PixLib::ConfList::S8:
	  *((char *)((PixLib::ConfList&)obj).m_value) = (char) read_val;
	  break;
	case PixLib::ConfList::U8:
	  *((unsigned char *)((PixLib::ConfList&)obj).m_value) = (unsigned char) read_val;
	  break;
	default: ;
	}
      } break;
      case PixLib::ConfObj::VECTOR :{
        QComboBox *cb = dynamic_cast<QComboBox *>(configParameterTable->cellWidget(row,3));
	//	std::cout << "INFO [PixScanPanel::readFromHandlersFromTable] vector " << obj.name() <<  " item=" << static_cast<void *>(cb)<< std::endl;
	bool isOK;
	if(cb!=0){
	  int nent = cb->count();
	  switch( obj.subtype() ){
	  case PixLib::ConfVector::V_UINT:{
	    std::vector<unsigned int> &tmpVec = ((PixLib::ConfVector&)obj).valueVUint();
	    tmpVec.clear();
	    for(int i=0;i<nent;i++){
	      // 	      std::cout << "INFO [PixScanPanel::readFromHandlersFromTable] vector " << obj.name() <<  " item="
	      // 			<< "value=" << cb->text(i).toLatin1().data()
	      // 			<< " ->10: " << cb->text(i).toInt(&isOK,10) << " (" << isOK << ")"
	      // 			<< ", ->16: " << cb->text(i).toInt(&isOK,16) << " (" << isOK << ")"
	      // 			<< std::endl;
	      unsigned int value = (unsigned int)cb->itemText(i).toInt(&isOK);
	      if(isOK){
		tmpVec.push_back(value);
	      }
	      else {
		value = (unsigned int)cb->itemText(i).toInt(&isOK,16);
		if(isOK){
		  tmpVec.push_back(value);
		}
	      }
	    }
	    //	    obj.dump(std::cout);
	    break;}
	  case PixLib::ConfVector::V_INT:{
	    std::vector<int> &tmpVec = ((PixLib::ConfVector&)obj).valueVInt();
	    tmpVec.clear();
	    for(int i=0;i<nent;i++){
	      // 	      std::cout << "INFO [PixScanPanel::readFromHandlersFromTable] vector " << obj.name() <<  " item="
	      // 			<< "value=" << cb->text(i).toLatin1().data()
	      // 			<< " ->10: " << cb->text(i).toInt(&isOK,10) << " (" << isOK << ")"
	      // 			<< ", ->16: " << cb->text(i).toInt(&isOK,16) << " (" << isOK << ")"
	      // 			<< std::endl;
	      int value = cb->itemText(i).toInt(&isOK);
	      if(isOK){
		tmpVec.push_back(value);
	      }
	      else {
		value = (unsigned int)cb->itemText(i).toInt(&isOK,16);
		if(isOK){
		  tmpVec.push_back(value);
		}
	      }
	    }
	    break;}
	  case PixLib::ConfVector::V_FLOAT:{
	    std::vector<float> &tmpVec = ((PixLib::ConfVector&)obj).valueVFloat();
	    tmpVec.clear();
	    for(int i=0;i<nent;i++){
	      float value = cb->itemText(i).toFloat(&isOK);
	      if(isOK){
		tmpVec.push_back(value);
	      }
	    }
	    break;}
	  default:
	    break;
	  }
	}else
        std::cout << "ERROR [PixScanPanel::readFromHandlersFromTable]  Expecting object " << obj.name() << " to be a combo box but it is not." << std::endl;
	break;}
      case PixLib::ConfObj::VOID :
      default:
	std::cout << "ERROR [PixScanPanel::readFromHandlersFromTable]  Unknown config type " << obj.type() << " for obejct " << obj.name() << "."
		  << std::endl;
      }
    } //end of loop over PixLib::ConfGroups
    }
    catch (PixA::ConfigException &err) {
      std::cout << "ERROR [PixScanPanel::readFromHandlersFromTable] Caught config exception : " << err.getDescriptor() << std::endl;
    }
    //    myconf.dump(std::cout);
    return;
  }
  void PixScanPanel::updateLoopSettings(unsigned int ID){
    if(ID>=MAX_LOOPS) return;
    if(LoopBox[ID]==0) return;
    if(!loopActive[ID]->isChecked()){
      loopStep[ID]->setValue(1);
      loopStart[ID]->setValue(0);
      loopStop[ID]->setValue(0);
      loopRegular[ID]->setChecked(true);
      loopStart[ID]->setEnabled(false);
      loopStop[ID]->setEnabled(false);
      loopStep[ID]->setEnabled(false);
      loopRegular[ID]->setEnabled(false);
      loopPtsBox[ID]->setEnabled(false);
      loopDspProc[ID]->setEnabled(false);
      loopEndAction[ID]->setEnabled(false);
      loopOnDsp[ID]->setEnabled(false);
      loopType[ID]->setEnabled(false);
      if(ID==0)loopFitMethod->setEnabled(false);
    } else{
      loopStart[ID]->setEnabled(loopRegular[ID]->isChecked());
      loopStop[ID]->setEnabled(loopRegular[ID]->isChecked());
      loopStep[ID]->setEnabled(loopRegular[ID]->isChecked());
      loopRegular[ID]->setEnabled(true);
      loopPtsBox[ID]->setEnabled(true);
      loopDspProc[ID]->setEnabled(true);
      loopEndAction[ID]->setEnabled(true);
      loopOnDsp[ID]->setEnabled(true);
      loopType[ID]->setEnabled(true);
      if(ID==0)loopFitMethod->setEnabled(true);
    }
  }
  void PixScanPanel::updateLoopPts(unsigned int ID){
    if(ID>=MAX_LOOPS) return;
    if(LoopBox[ID]==0) return;

    if(loopRegular[ID]->isChecked()){
      loopPtsBox[ID]->clear();
      if(loopStep[ID]->value()>1){
	for(int i=0;i<loopStep[ID]->value();i++){
	  int scanpt = loopStart[ID]->value() + (int)(((float)(loopStop[ID]->value()-loopStart[ID]->value()))/
						      (float)(loopStep[ID]->value()-1)*(float)i);
      loopPtsBox[ID]->addItem(QString::number(scanpt));// originally insertItem(QString::number(scanpt));
	}
      }else if(loopStep[ID]->value()==1)
    loopPtsBox[ID]->addItem(loopStart[ID]->text());//
    } else{
      //    loopPtsBox[ID]->clear();
    }
    return;
  }
  void PixScanPanel::showLoop(int ID){
    // std::cout << "INFO [PixScanPanel::showLoop] Changed to " << ID << std::endl;
    if(ID>=MAX_LOOPS) return;
    if(LoopBox[ID]==0) return;
    loopBoxStack->setCurrentWidget(LoopBox[ID]);

    return;
  }

  void PixScanPanel::singleFEModeToggled() {

#ifdef MASK_IN_FE_BY_FE_ONLY
    for (unsigned int fe_i=0; fe_i<16; fe_i++) {
      m_feMaskButton[fe_i]->setEnabled(snglfeMode->isChecked());
      m_feMaskButton[fe_i]->feMaskToggled(false);
    }
    m_feMaskAllDisable->setEnabled(snglfeMode->isChecked());
    m_feMaskAllEnable->setEnabled(snglfeMode->isChecked());
    if (snglfeMode->isChecked())  {
    //  m_feMaskAllEnable->setPaletteBackgroundColor( QColor(0, 230, 0 ) );
      //m_feMaskAllDisable->setPaletteBackgroundColor( QColor(180, 180, 180 ) );


m_feMaskAllEnable->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 255, 0 );"));

m_feMaskAllDisable->setStyleSheet(QString::fromUtf8("background-color: rgb(180, 180, 180);"));   
  m_feMaskAllEnable->update();   m_feMaskAllDisable->show(); 
    }
    else {
    //  m_feMaskAllEnable->setPaletteBackgroundColor( QColor(150, 230, 150 ) );
    //  m_feMaskAllDisable->setPaletteBackgroundColor( QColor(210, 210, 210 ) );


m_feMaskAllEnable->setStyleSheet(QString::fromUtf8("QPushButton{background-color: rgb(150, 230, 150 );}"));

m_feMaskAllDisable->setStyleSheet(QString::fromUtf8("QPushButton{background-color: rgb(210, 210, 210);}"));
     m_feMaskAllEnable->update();   m_feMaskAllDisable->show(); 
    }
#endif
  }

  void PixScanPanel::loadLoopPts(unsigned int ID){
    if(ID>=MAX_LOOPS) return;
    if(LoopBox[ID]==0) return;

    int nPts=0;
    char line[2000];
    float testFloat;
    bool isOK;
    QStringList filter;
    filter += "Text file (*.txt)";
    filter += "Any file (*.*)";
    QFileDialog fdia(this,"scanpts",QString::null,QString::null);
    fdia.setWindowTitle("Load ascii-list of scan points");
    fdia.setNameFilters(filter);
    if(fdia.exec() == QDialog::Accepted){
      FILE *in = fopen(fdia.selectedFiles().at(0).toLatin1().data(),"r");
      if(in!=0){
	loopPtsBox[ID]->clear();
	while(fgets(line,2000,in)!=0){
	  QString tmpstr = line;
	  tmpstr.remove(tmpstr.length()-1,1);//remove trailing '\n'
	  testFloat = tmpstr.toFloat(&isOK);
	  if(isOK){
	    if(nPts==0)
	      loopStart[ID]->setValue((int)testFloat);
	    else
	      loopStop[ID]->setValue((int)testFloat);
	    nPts++;
        //loopPtsBox[ID]->insertItem(tmpstr);
        loopPtsBox[ID]->addItem(tmpstr);//
	  }
	}
	loopStep[ID]->setValue(nPts);
	fclose(in);
      } else
	QMessageBox::warning(this,"loadInnerPts()","Error opening file "+fdia.selectedFiles().at(0));
    }
    return;
  }

  void PixScanPanel::loadTuningPoints( ){

    std::cout<<__PRETTY_FUNCTION__<<std::endl;
    QStringList filter;
    filter += "Text file (*.txt)";
    filter += "Any file (*.*)";
    QFileDialog fdia(this,"tunpts",QString::null,QString::null);
    fdia.setWindowTitle("Load ascii-list of scan points");
    fdia.setNameFilters(filter);
      if(fdia.exec() != QDialog::Accepted){
        QMessageBox::warning(this,"loadInnerPts()","Error opening file "+fdia.selectedFiles().at(0));
        return;
      }
    std::cout<<"INFO [PixScanPanel::loadTuningPoints] Tuning points file "<<fdia.selectedFiles().at(0).toLatin1().data()<<std::endl;
    std::ifstream infile(fdia.selectedFiles().at(0).toLatin1().data());
      if (!infile.is_open() || !infile.good()) {
        std::cout<<"ERROR in opening tuning point file"<<std::endl;
        return;
      }

    tuningPointsFile->setText(fdia.selectedFiles().at(0).toLatin1().data());

    infile.close();
  }

  void PixScanPanel::fixVCAL(bool doFix){
    if(doFix)
      setVcal->setValue(0x1fff);
    else
      setVcal->setValue(0);
  }
  void PixScanPanel::VcalChanged(int value)
  {
    bool isFix = (value==0x1fff);
    disconnect( fixedVCALBox, SIGNAL( toggled(bool) ), this, SLOT( fixVCAL(bool) ) );
    fixedVCALBox->setChecked(isFix);
    connect( fixedVCALBox, SIGNAL( toggled(bool) ), this, SLOT( fixVCAL(bool) ) );
  }

  void PixScanPanel::openTableMenu(const QPoint &point){
      Q_UNUSED( point ) // Slot needs point parameter. But this is useless to us.
      int row = sender()->property("row").toInt(); // Use property instead of point.
      int col = 3;
      QComboBox *cb = dynamic_cast<QComboBox *>(configParameterTable->cellWidget(row,col));
     // QComboBox *cb =new QComboBox();
     // if(cb!=0){
    char line[2000];
    QStringList filter;
    filter += "Text file (*.txt)";
    filter += "Any file (*.*)";
    QFileDialog fdia(this,"vecpts",QString::null,QString::null);
    fdia.setWindowTitle("Load ascii-list of vector entries");
    fdia.setNameFilters(filter);
    if(fdia.exec() == QDialog::Accepted){
      FILE *in = fopen(fdia.selectedFiles().at(0).toLatin1().data(),"r");
      if(in!=0){
        cb->clear();
        while(fgets(line,2000,in)!=0){
          QString tmpstr = line;
          tmpstr.remove(tmpstr.length()-1,1);//remove trailing '\n'
          cb->addItem(tmpstr, QVariant(tmpstr));
        }
        cb->setCurrentIndex( 0 );
      }
      readFromHandlersFromTable (row,col);
    }
  }


  void PixScanPanel::init() 
  {
    if (m_scanConfig) {
      PixA::ConfigHandle full_config;
      {
	PixA::ConfigRef config ( m_scanConfig.ref() );

	std::stringstream message;
	message << " Get scan configuration .... ";
	std::unique_ptr<PixA::IBusyLoop> busy_loop(PixA::BusyLoopFactory::busyLoop(message.str()));

	full_config = PixA::ConfigHandle(config.createFullCopy());
      }
      PixA::ConfigRef full_config_ref(full_config.ref());
      config_copy(m_pixScan->config(), full_config_ref.config());
      updateConfList(m_pixScan->config(), full_config_ref.config());

    }

  }


  PixLib::Config &PixScanPanel::config() {
    return m_pixScan->config();
  }

  void PixScanPanel::editVector(const QPoint & point_vec) {
    Q_UNUSED(point_vec) // Slot needs a point paramater. But this is useless to us.
    int row = sender()->property("row").toInt(); // Use property instead of point.
    int col = 3;
    TypeComboTableItem *combo = dynamic_cast<TypeComboTableItem *>(configParameterTable->cellWidget(row, col));
      QString text;
      for(int i=0; i<combo->count(); ++i) {
        if (text.length()>0) text+=", ";
        text += combo->itemText(i);
      }
      PixCon::EditVectorForm edit(configParameterTable->item(row,0)->text(), text, true, this);
      for(;;) {
        if (edit.exec()==QDialog::Accepted) {
          text=edit.text();
          QStringList new_vector;
          if (edit.getVector(new_vector)) {
            combo->clear();
            combo->addItems(new_vector);
            readFromHandlersFromTable (row,3);
            break;
          }
        } else {
            break;
        }
      }
  }
}
