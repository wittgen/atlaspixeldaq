#ifndef _PixCon_IStatusMonitorActivator_h_
#define _PixCon_IStatusMonitorActivator_h_

#include <CalibrationDataTypes.h>
#include <string>
#include <vector>

namespace PixCon {

  /** Helper class to activate or abort status monitoring
   */
  class IStatusMonitorActivator
  {
  public:
    virtual ~IStatusMonitorActivator() {}

    /** Activate the monitoring of a scan.
     * @param scan_serial_number the serial number of the scan to be monitored.
     * @param start_value an array of the 4 initial counter values in the order : mask stage, loop 0-2.
     * @param max_value the number of steps for each of the 4 counter in the order: mask stage, loop 0-2.
     * @param max_step_time the maximum time counters may staty unchanged.
     * @return true if the monitoring was activated successfully, false in case the monitoring was not activated.
     */
    virtual bool activateScanMonitoring(SerialNumber_t scan_serial_number,
					unsigned int mask_stage_loop_index,
					const std::vector<unsigned short> &start_value,
					const std::vector<unsigned short> &max_value,
					unsigned long max_step_time) = 0;

    /** Abort the monitoring of the given scan.
     * @param scan_serial_number the serial number of the scan for which the monitoring should be aborted.
     */
    virtual void abortScanMonitoring(SerialNumber_t scan_serial_number) = 0;

    /** Activate the monitoring of an analysis.
     * @param analysis_serial_number the serial number of the analysis to be monitored.
     * @return true if the monitoring was activated successfully, false in case the monitoring was not activated.
     */
    virtual bool activateAnalysisMonitoring(SerialNumber_t analysis_serial_number) = 0;

    /** Abort the monitoring of the given analysis.
     * @param analysis_serial_number the serial number of the analysis for which the monitoring should be aborted.
     */
    virtual void abortAnalysisMonitoring(SerialNumber_t analysis_serial_number) = 0;

    /** Activate the monitoring of the given action sequence.
     * @param action_sequence the expected sequence of actions.
     * @return true if the monitoring was activated successfully, false in case the monitoring was not activated.
     */
    virtual bool activateActionMonitoring(const std::vector<std::string> &action_sequence) = 0;

    /** Abort the monitoring of the current action sequence.
     */
    virtual void abortActionMonitoring() = 0;
  };

}
#endif
