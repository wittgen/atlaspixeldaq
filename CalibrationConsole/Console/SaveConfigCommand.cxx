#include <ICommand.h>
#include <memory>
#include "ActionCommandBase.h"
#include <PixActions/PixActionsMulti.h>
#include <qinputdialog.h>

namespace PixCon {

  class SaveConfigCommand : public ActionCommandBase
  {
  public:
    SaveConfigCommand(const std::shared_ptr<ConsoleEngine> &engine,  const std::string &cfg_tag)
      : ActionCommandBase(engine),
	m_cfgTag(cfg_tag)
    {}

    const std::string &name() const { return s_name; }

    // defined in ActionCommandBase
    bool configure() {
      bool ok;
      QString text = QInputDialog::getText(0, "ConfigTag", "Name of a config Tag  (leave blank for current tag): ", QLineEdit::Normal, QString::null, &ok);
      if ( ok && !text.isEmpty() ) {
	m_cfgTag = text.toLatin1().data();
	return true;
      }
      else {
	return false;
      }
    }
    bool hasDefaults() const { return true; }


  protected:

    void setActionSequence(std::vector<std::string> &action_sequence) {
      action_sequence.push_back("saveConfig");
    }

    bool callAction(PixLib::PixActionsMulti &action) {
      action.saveConfig(m_cfgTag);
      return true;
    }

  private:
    std::string m_cfgTag;
    static const std::string s_name;
  };

  const std::string SaveConfigCommand::s_name("Save Configs To DB");

  IConfigurableCommand *createSaveConfigCommand(const std::shared_ptr<ConsoleEngine> &engine) {
    return new SaveConfigCommand(engine,"");
  }

  class SaveConfigKit : public PixCon::ICommandKit
  {
  protected:
    SaveConfigKit()
    {}

  public:
    PixCon::IConfigurableCommand *createCommand(const std::string &/*command*/, const std::shared_ptr<ConsoleEngine> &engine) const {
      assert(engine.get());
      return createSaveConfigCommand(engine);
    }

    static bool registerKit() {
      CommandKits::registerKit("Save Config To DB",new SaveConfigKit(), PixCon::UserMode::kExpert);
      return true;
    }

  private:
    static bool s_registered;
  };

  bool SaveConfigKit::s_registered = SaveConfigKit::registerKit();


}
