#include <QApplication>
#if QT_VERSION < 0x050000
  #include <QPlastiqueStyle>
#endif
#include "ConsoleApp.h"

#include <iostream>

#include <VisualisationRegister.h>
#include <ModuleMapH2Factory.h>

#include <ConfigWrapper/createDbServer.h>

#if defined(HAVE_QTGSI)
#include "TQRootApplication.h"
#include "TQApplication.h"
#else
#include <TApplication.h>
#endif

#include <TEnv.h>
#include <TSystem.h>
#include <TROOT.h>

const std::string s_appName("Console");
  
int main( int argc, char ** argv )
{
#if QT_VERSION < 0x050000
  QApplication::setStyle(new QPlastiqueStyle());
#else
  QApplication::setStyle("fusion");
#endif

  int dummy_argc=1;
  std::shared_ptr<PixCon::UserMode> user_mode(new PixCon::UserMode(PixCon::UserMode::kShifter));
  std::string partition_name;
  std::string oh_server_name;
  std::string is_server_name;
  std::string histo_name_server_name;
  std::string db_server_partition_name;
  std::string db_server_name;

  std::string ddc_partition_name;
  std::string ddc_is_name;

  bool error=false;

  for (int arg_i=1; arg_i<argc; arg_i++) {
    if (strcmp(argv[arg_i],"--expert")==0) {
      user_mode->setUserMode(PixCon::UserMode::kExpert);
    }
    else if (strcmp(argv[arg_i],"-p")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      partition_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-o")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      oh_server_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-n")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      is_server_name = argv[++arg_i];
    }
    else if ((strcmp(argv[arg_i],"--histo-name-server")==0 || strcmp(argv[arg_i],"-m")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      histo_name_server_name = argv[++arg_i];
    }
    else if ( (strcmp(argv[arg_i],"-D")==0
	       || strcmp(argv[arg_i],"--db-server-name")==0)
	      && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      db_server_name = argv[++arg_i];
    }
    else if ( (strcmp(argv[arg_i],"--db-server-partition")==0
	       || strcmp(argv[arg_i],"-p")==0)
	      && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      db_server_partition_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"--ddc-is-name")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      ddc_is_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"--ddc-partition-name")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      ddc_partition_name = argv[++arg_i];
    }
    else {
      std::cout << "ERROR ["<< argv[0] << ":main] unhandled argument " << arg_i << " : " << argv[arg_i]<< "." << std::endl;
      error=true;
    }
  }
  
  if (error) {
    std::cout << "usage: " << argv[0] << "[-p partition] [-o OH server name] [-n \"IS server name\"] [--histo-name-server \"histo name server name\"]" << std::endl
	      << "\t [--db-server-partition db-server-partition] [--db-server-name]" 
	      << " [--ddc-partition-name partition-name] [--ddc-is-name is-server-name]"
	      << std::endl << std::endl
	      << "\tGenerally it is sufficient to start the Console without any options." <<std::endl;
    
    return -1;
  }
  
  IPCCore::init(dummy_argc, argv);
  
  int ret=0;

  // QT-ROOT application
#if defined(HAVE_QTGSI)
  TQApplication root_app(s_appName.c_str(),&dummy_argc,argv);
  
  // process root logon script
  // the logonscript could for example define some proper styles.
  
  std::cout << "INFO [main:" << argv[0] << "] execute ROOT macros from : " << gEnv->GetValue("Rint.Logon", (char*)0) << std::endl;
  const char *logon = gEnv->GetValue("Rint.Logon", (char*)0);
  if (logon) {
    char *mac = gSystem->Which(TROOT::GetMacroPath(), logon, kReadPermission);
    if (mac) {
      root_app.ProcessFile(logon);
    }
    delete [] mac;
  }
#else
  TApplication root_app(s_appName.c_str(),&dummy_argc,argv);
  
  // process root logon script
  // the logonscript could for example define some proper styles.
  
  std::cout << "INFO [main:" << argv[0] << "] execute ROOT macros from : " << gEnv->GetValue("Rint.Logon", (char*)0) << std::endl;
  const char *logon = gEnv->GetValue("Rint.Logon", (char*)0);
  if (logon) {
    char *mac = gSystem->Which(TROOT::GetMacroPath(), logon, kReadPermission);
    if (mac) {
      root_app.ProcessFile(logon);
    }
    delete [] mac;
  }
#endif

  if (db_server_partition_name.empty() && !partition_name.empty()) {
    db_server_partition_name = partition_name;
  }
  
  std::shared_ptr<PixLib::PixDbServerInterface> db_server;
  IPCPartition * db_server_partition=0;
  if (!db_server_name.empty() && !db_server_partition_name.empty()) {
    db_server_partition=new IPCPartition(db_server_partition_name.c_str());
    if (!db_server_partition->isValid()) {
      std::cerr << "ERROR [" << argv[0] << ":main] Not a valid partition " << db_server_partition_name
		<< " for the db server." << std::endl;
      return EXIT_FAILURE;
    }

    db_server = std::shared_ptr<PixLib::PixDbServerInterface>( PixA::createDbServer(*db_server_partition,
										    db_server_name ));
  }

  try {
    std::unique_ptr<QApplication> app;
    std::shared_ptr<PixA::IModuleMapH2Factory> factory(new PixA::ModuleMapH2Factory);
    PixA::VisualisationRegister::registerMAVisualisers(factory);
    ConsoleApp *sc_app = new ConsoleApp(user_mode, partition_name, is_server_name, oh_server_name, 
					histo_name_server_name, db_server, factory, ddc_partition_name, ddc_is_name, dummy_argc, argv);
    app = std::unique_ptr<QApplication>(sc_app);
    
    ret  = (app.get() ? app->exec() : -1);
    std::cout << "INFO [main] done.";
  }
  catch (SctPixelRod::BaseException &err) {
    std::cout << "ERROR [main:" << argv[0] << "] Caught unhandled exception : " << err.getDescriptor() << std::endl;
  }
  catch (std::exception &err) {
    std::cout << "ERROR [main:" << argv[0] << "] Caught unhandled exception : " << err.what() << std::endl;
  }
  std::cout << "INFO [main:" << argv[0] << "] exited." << std::endl;
  delete db_server_partition;
  return ret;
}
