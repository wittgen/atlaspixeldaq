#ifndef _PixCon_LvOnOffCommand_h_
#define _PixCon_LvOnOffCommand_h_

#include "CommandKits.h"
#include <memory>

namespace PixCon {

  class ConsoleEngine;
  
  ICommand *lvOnOffCommand(const std::shared_ptr<ConsoleEngine> &engine, bool on);

}

#endif
