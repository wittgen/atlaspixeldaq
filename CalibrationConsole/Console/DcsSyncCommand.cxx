#include <ICommand.h>
#include <memory>
#include "ActionCommandBase.h"
#include <PixActions/PixActionsMulti.h>


namespace PixCon {

  class DcsSyncCommand : public ActionCommandBase
  {
  public:
    DcsSyncCommand(const std::shared_ptr<ConsoleEngine> &engine)
      : ActionCommandBase(engine)
    {}

    const std::string &name() const { return commandName(); }

    static const std::string &commandName() {
      return s_name;
    }

  protected:

    void setActionSequence(std::vector<std::string> &action_sequence) {
      action_sequence.push_back("DCSsync");
    }

    bool callAction(PixLib::PixActionsMulti &action) {
      action.DCSsync();
      return true;
    }

  private:

    //bool m_uninitialisedRodsOnly;
    static const std::string s_name;
  };

  const std::string DcsSyncCommand::s_name("DCS sync disable");

  class DcsSyncKit : public PixCon::ICommandKit
  {
  protected:
    DcsSyncKit()
    {}

  public:
    PixCon::IConfigurableCommand *createCommand(const std::string &/*command*/, const std::shared_ptr<ConsoleEngine> &engine) const {
      assert(engine.get());
      return new DcsSyncCommand(engine);
    }

    static bool registerKit() {
      CommandKits::registerKit(DcsSyncCommand::commandName(), new DcsSyncKit, PixCon::UserMode::kExpert);
      //      CommandKits::registerKit(DcsSyncCommand::name(true),new DcsSyncCommand(true));
      return true;
    }

  private:
    static bool s_registered;
  };

  bool DcsSyncKit::s_registered = DcsSyncKit::registerKit();

  ICommand *dcsSyncCommand(const std::shared_ptr<ConsoleEngine> &engine) {
    return new DcsSyncCommand(engine);
  }
}
