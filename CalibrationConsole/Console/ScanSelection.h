/* Dear emacs, this is -*-c++-*- */
#ifndef _PixCon_ScanSelection_h_
#define _PixCon_ScanSelection_h_
#include "ui_ScanSelectionBase.h"
#include <QComboBox>
#include <memory>
#include <IConfigDb.hh>

namespace PixA {
  class ConfigHandle;
}

namespace PixCon {

   
    
  class IConfigEdit;

  class ScanSelection : public QDialog, public Ui_ScanSelectionBase
  {    Q_OBJECT
  public:
    enum EClass { kScan, kAnalysis, kClassification, kPostProcessing, kNTypes};
    enum BaseType {TBase, TBase_I4, NTBaseMax};
    std::vector<int> scanana;

    ScanSelection(const std::string &name, const std::string &default_file_name, bool scan, bool analysis);
    ~ScanSelection();

    std::string selectedType(EClass class_i) const;
    std::string selectedTag(EClass class_i) const;
    CAN::Revision_t selectedRevision(EClass class_i) const;
    std::vector <PixA::ConfigHandle> Create_configvector(EClass class_i);
    bool check_I4_config(EClass class_i);

    unsigned int scanSerialNumber() const;
    std::map<std::string, std::pair<CAN::ESerialNumberType, CAN::SerialNumber_t> > getNamedSources() const;


    std::string fileName() const;

    bool checkFileName(bool silent=false) const;

  public slots:
    void customiseScan()           {  customise(kScan); }
    void accept_aftercheck();
    void customiseAnalysis()       { customise(kAnalysis); }
    void customiseClassification() { customise(kClassification); }
    void customisePostProcessing() { customise(kPostProcessing); }
    void setAnalyseScan(bool analyse_scan);
    void enableFileNameEdit();
void updateRevisions(int class_i, int tag_item_i);
void updateTags(int class_i,  int type_item_i);

  protected:
    void updateTypes(int class_i, const std::string &name);
   // void updateTags(int class_i,  int type_item_i);
  //  void updateRevisions(int class_i, int tag_item_i);

    IConfigEdit *createConfigEdit(std::vector<PixA::ConfigHandle> &vconfig, EClass class_i);
    //IConfigEdit *createConfigEdit(PixA::ConfigHandle &config, EClass class_i);
    void customise(EClass class_i);
    
    std::shared_ptr<CAN::IConfigDb> m_configDb[4];
    std::vector<CAN::Revision_t>      m_revisions[4];

    QComboBox *m_typeSelector[4];//
    QComboBox *m_tagSelector[4];//
    QComboBox *m_revSelector[4];//
    static const char *s_className[4];
  };

}
#endif

