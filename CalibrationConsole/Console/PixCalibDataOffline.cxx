#include <iostream>
#include <fstream>
#include <cstring>
#include <sstream>
#include <set>
#include <vector>
#include <boost/algorithm/string.hpp>

#include <qlineedit.h>
#include <qmessagebox.h>

#include "PixCalibDataOffline.h"

namespace PixCon {

  PixCalibDataOffline::PixCalibDataOffline(QWidget* parent)
  :QDialog(parent)
  {
    setupUi(this);
    m_totLineEdit->setText("13134");
    m_thresholdLineEdit->setText("13131");
    m_intimeThresholdLineEdit->setText("13132");
    m_nOnlineTags = 4;
    m_onlineTagLineEdit->setText("PIXEL11,PIT-ALL-V40,PIT_BOC,PIT_MOD_3500e_NM");
    m_onlineTagLineEdit->home(true);
    m_calibrationTagLineEdit->setText("PixCalib-003-01,PixCalib-ES1C-UPD1-002-02,PixCalib-BLKP-UPD4-000-00");
    m_calibrationTagLineEdit->home(true);
    m_runPeriodStartLineEdit->setText("188520");
    m_passwordLineEdit->setEchoMode(QLineEdit::Password);
    std::string pcdEnv(getenv("PIX_CALIB_DATA_OFFLINE"));
    m_pcdDir = pcdEnv+"/";
    m_pcdName = "pcd";
    m_dbName = "CONDBR2";
  }

  PixCalibDataOffline::~PixCalibDataOffline() {}

  void PixCalibDataOffline::extractAnalysis()
  {
    if (isAthenaSetup()) {
      std::string tot(m_totLineEdit->text().toLatin1().data());
      std::string threshold(m_thresholdLineEdit->text().toLatin1().data());
      std::string intimeThreshold(m_intimeThresholdLineEdit->text().toLatin1().data());
      if (tot.empty()) {
	std::cout << "ERROR [PixCalibDataOffline::extractAnalysis] missing ToT analysis number !" << std::endl;
	return;
      }
      if (threshold.empty()) {
	std::cout << "ERROR [PixCalibDataOffline::extractAnalysis] missing threshold analysis number !" << std::endl;
	return;
      }
      if (intimeThreshold.empty()) {
	std::cout << "ERROR [PixCalibDataOffline::extractAnalysis] missing intime-threshold analysis number !" << std::endl;
	return;
      }
      std::string workDir(getenv("PWD"));
      std::string extractAnalysisName("extractAnalysis");
      std::string extractAnalysisCmd(extractAnalysisName+".exe");
      std::string pcdTextFileName(m_pcdDir+m_pcdName+".dat");
      std::ifstream pcdTextFile(pcdTextFileName.c_str());
      bool pcdTextFileExists = (pcdTextFile) ? true : false;
      pcdTextFile.close();
      if (pcdTextFileExists) {
	std::string aliasesTextFileName(m_pcdDir+"aliases.dat");
	std::ifstream aliasesTextFile(aliasesTextFileName.c_str());
	bool aliasesTextFileExists = (aliasesTextFile) ? true : false;
	if (aliasesTextFileExists) {
	  std::stringstream appendtext;
	  appendtext << "_" << m_totLineEdit->text().toLatin1().data() << "_"
			    << m_thresholdLineEdit->text().toLatin1().data() << "_"
			    << m_intimeThresholdLineEdit->text().toLatin1().data();
	  std::string pcdFileNameToCreate(m_pcdName+appendtext.str()+".dat");
	  std::string pcdFilePathNameToCreate(m_pcdDir+pcdFileNameToCreate);
	  std::ifstream pcdFileToCreate(pcdFilePathNameToCreate.c_str());
	  bool pcdFileToCreateExists = (pcdFileToCreate) ? true : false;
	  pcdFileToCreate.close();
	  if (pcdFileToCreateExists) {
	    std::cout << "WARNING [PixCalibDataOffline::extractAnalysis] pixel calibration data already exists in "
		      << pcdFilePathNameToCreate.c_str() << ". No need to extract. Check analysis numbers." << std::endl;
	    return;
	  }
	  std::stringstream message;
	  message << "Pixel calibration data will be extracted to " << pcdFileNameToCreate.c_str() << ".\n"
		  << "This may take about 15 minutes. Do you want to continue ?";
	  int answer = QMessageBox::warning(this, "WARNING", QString::fromStdString(message.str()), QMessageBox::No, QMessageBox::Yes);
	  if (answer == QMessageBox::No) return;
	  std::string extractAnalysisLog(extractAnalysisName+appendtext.str()+".log");
	  std::stringstream unixcmd;
	  unixcmd << "cd " << m_pcdDir << " && "
		  << extractAnalysisCmd << " " << m_totLineEdit->text().toLatin1().data()
		  << " > " << extractAnalysisLog.c_str() << " 2>&1 && "
		  << "mv -f " << m_pcdName << ".sav " << m_pcdName << ".dat" << " && "
		  << extractAnalysisCmd << " -t " << m_thresholdLineEdit->text().toLatin1().data()
		  << " >> " << extractAnalysisLog.c_str() << " 2>&1 && "
		  << "mv -f " << m_pcdName << ".sav " << m_pcdName << ".dat" << " && "
		  << extractAnalysisCmd << " -w " << m_intimeThresholdLineEdit->text().toLatin1().data()
		  << " >> " << extractAnalysisLog.c_str() << " 2>&1 && "
		  << "mv -f " << m_pcdName << ".sav " << m_pcdName << ".dat" << " && "
		  << "cp -a " << m_pcdName << ".dat " << pcdFileNameToCreate.c_str() << " && "
		  << "chmod 444 " << extractAnalysisLog.c_str() << " && "
		  << "chmod 444 " << pcdFileNameToCreate.c_str() << " && "
		  << "cd " << workDir;
	  int rvalue = system(unixcmd.str().c_str());
	  if (rvalue == 0) {
	    std::cout << "INFO [PixCalibDataOffline::extractAnalysis] pixel calibration data is extracted to "
		      << pcdFileNameToCreate.c_str() << std::endl;
	  }
	  else {
	    std::cout << "ERROR [PixCalibDataOffline::extractAnalysis] pixel calibration data could not be extracted. "
		      << "See " << m_pcdDir.c_str() << extractAnalysisLog.c_str() << " !" << std::endl;
	  }
	} // if (aliasesTextFileExists)
	else {
	  std::cout << "ERROR [PixCalibDataOffline::extractAnalysis] module hashid aliases text file "
		    << aliasesTextFileName.c_str() << " does not exist !" << std::endl;
	}
      } // if (pcdTextFileExists)
      else {
	std::cout << "ERROR [PixCalibDataOffline::extractAnalysis] pixel calibration data text file "
		  << pcdTextFileName.c_str() << " does not exist !" << std::endl;
      }
    } // if (isAthenaSetup())

  }

  void PixCalibDataOffline::createSQLiteFile()
  {
    if (isAthenaSetup()) {
      std::string tot(m_totLineEdit->text().toLatin1().data());
      std::string threshold(m_thresholdLineEdit->text().toLatin1().data());
      std::string intimeThreshold(m_intimeThresholdLineEdit->text().toLatin1().data());
      std::string onlineTag(m_onlineTagLineEdit->text().toLatin1().data());
      std::string calibrationTag(m_calibrationTagLineEdit->text().toLatin1().data());
      std::string runPeriodStart(m_runPeriodStartLineEdit->text().toLatin1().data());
      if (tot.empty()) {
	std::cout << "ERROR [PixCalibDataOffline::createSQLiteFile] missing ToT analysis number !" << std::endl;
	return;
      }
      if (threshold.empty()) {
	std::cout << "ERROR [PixCalibDataOffline::createSQLiteFile] missing threshold analysis number !" << std::endl;
	return;
      }
      if (intimeThreshold.empty()) {
	std::cout << "ERROR [PixCalibDataOffline::createSQLiteFile] missing intime-threshold analysis number !" << std::endl;
	return;
      }
      std::vector<std::string> onlineTagVec;
      boost::split(onlineTagVec, onlineTag, boost::is_any_of(","));
      if (onlineTag.empty() || onlineTagVec.size() != m_nOnlineTags) {
	std::cout << "ERROR [PixCalibDataOffline::createSQLiteFile] missing online tags !" << std::endl;
	return;
      }
      std::vector<std::string> calibrationTagVec;
      boost::split(calibrationTagVec, calibrationTag, boost::is_any_of(","));
      if (calibrationTag.empty()) {
	std::cout << "ERROR [PixCalibDataOffline::createSQLiteFile] missing calibration tag !" << std::endl;
	return;
      }
      if (runPeriodStart.empty()) {
	std::cout << "ERROR [PixCalibDataOffline::createSQLiteFile] "
		  << "missing starting run number for interval of validity !" << std::endl;
	return;
      }
      std::string workDir(getenv("PWD"));
      std::stringstream appendtext;
      appendtext << "_" << m_totLineEdit->text().toLatin1().data() << "_"
		 << m_thresholdLineEdit->text().toLatin1().data() << "_"
		 << m_intimeThresholdLineEdit->text().toLatin1().data();
      std::string pcdTextFileName(m_pcdDir+m_pcdName+appendtext.str()+".dat");
      std::ifstream pcdTextFile(pcdTextFileName.c_str());
      bool pcdTextFileExists = (pcdTextFile) ? true : false;
      pcdTextFile.close();
      if (pcdTextFileExists) {
	std::string pcdSqlNameToCreate(m_pcdName+appendtext.str()+".sql");
	std::string pcdSqlPathNameToCreate(m_pcdDir+pcdSqlNameToCreate);
	std::ifstream pcdSqlToCreate(pcdSqlPathNameToCreate.c_str());
	bool pcdSqlToCreateExists = (pcdSqlToCreate) ? true : false;
	pcdSqlToCreate.close();
	if (pcdSqlToCreateExists) {
	  std::cout << "WARNING [PixCalibDataOffline::createSQLiteFile] pixel calibration data sqlite file already exists in "
		    << pcdSqlPathNameToCreate.c_str() << ". No need to create. Check analysis numbers "
		    << "or if existing sql file is empty." << std::endl;
	  return;
	}
	std::string makeCoolStrFileScript("make_coolstrfile.py");
	std::string makeCoolStrFileName(m_pcdDir+makeCoolStrFileScript);
	std::ifstream makeCoolStrFile(makeCoolStrFileName.c_str());
	bool makeCoolStrFileExists = (makeCoolStrFile) ? true : false;
	makeCoolStrFile.close();
	std::string writePixelCalibDBScript("WritePixelCalibDBsqlit.py");
	std::string writePixelCalibDBName(m_pcdDir+writePixelCalibDBScript);
	std::ifstream writePixelCalibDB(writePixelCalibDBName.c_str());
	bool writePixelCalibDBExists = (writePixelCalibDB) ? true : false;
	writePixelCalibDB.close();
	if (makeCoolStrFileExists && writePixelCalibDBExists) {
	  std::stringstream message;
	  message << "Pixel calibration data sqlite file will be created to " << pcdSqlNameToCreate.c_str() << ".\n"
		  << "This may take about 3 minutes per calibration tag. Do you want to continue ?";
	  int answer = QMessageBox::warning(this, "WARNING", QString::fromStdString(message.str()), QMessageBox::No, QMessageBox::Yes);
	  if (answer == QMessageBox::No) return;
	  std::string dbConnection("sqlite://X;schema="+pcdSqlNameToCreate+";dbname="+m_dbName);
	  std::string parsWPCDScript("parsWPCDsqlit"+appendtext.str()+".py");
	  std::string parsWPCDName(m_pcdDir+parsWPCDScript);
	  std::ofstream parsWPCDFile(parsWPCDName.c_str());
	  parsWPCDFile << "ParInputTextFile = \"" << m_pcdName << appendtext.str() << ".dat\"" << std::endl;
	  parsWPCDFile << "ParRun1 = " << m_runPeriodStartLineEdit->text().toLatin1().data() << std::endl;
	  std::string runPeriodEnd(m_runPeriodEndLineEdit->text().toLatin1().data());
	  if (runPeriodEnd.empty()) parsWPCDFile << "ParRun2 = " << std::numeric_limits<int32_t>::max() << std::endl;
	  else parsWPCDFile << "ParRun2 = " << runPeriodEnd.c_str() << std::endl;
	  std::string objectName("CondAttrListCollection#/PIXEL/PixCalib");
	  parsWPCDFile << "ParObjectList = []" << std::endl;
	  parsWPCDFile << "ParIOVTagList = []" << std::endl;
	  for (unsigned int i = 0; i < calibrationTagVec.size(); i++) {
	    parsWPCDFile << "ParObjectList += [\"" << objectName.c_str() << "\"]" << std::endl;
	    parsWPCDFile << "ParIOVTagList += [\"" << calibrationTagVec[i] << "\"]" << std::endl;
	  }
	  parsWPCDFile << "ParDbConnection = \"" << dbConnection.c_str() << "\"" << std::endl;
	  parsWPCDFile.close();
	  std::string createSQLiteFileLog("createSQLiteFile"+appendtext.str()+".log");
	  std::string dbPixCalibFolder("/PIXEL/PixCalib");
	  std::stringstream unixcmd;
	  unixcmd << "cd " << m_pcdDir << " && "
		  << "python ./" << makeCoolStrFileScript.c_str() << " \"" << dbConnection.c_str() << "\" \""
		  << dbPixCalibFolder.c_str() << "\"" << " > " << createSQLiteFileLog.c_str() << " 2>&1 && "
		  << "athena.py -bs ./" << parsWPCDScript.c_str() << " ./" << writePixelCalibDBScript.c_str()
		  << " >> " << createSQLiteFileLog.c_str() << " 2>&1 && "
		  << "chmod 444 " << parsWPCDScript.c_str() << " && "
		  << "chmod 444 " << createSQLiteFileLog.c_str() << " && "
		  << "chmod 444 " << pcdSqlNameToCreate.c_str() << " && "
		  << "rm -f dummy* PoolFileCatalog* hostnamelookup*" << " && "
		  << "cd " << workDir;
	  int rvalue = system(unixcmd.str().c_str());
	  if (rvalue == 0) {
	    std::cout << "INFO [PixCalibDataOffline::createSQLiteFile] pixel calibration data sqlite file is created to "
		      << pcdSqlNameToCreate.c_str() << std::endl;
	  }
	  else {
	    std::cout << "ERROR [PixCalibDataOffline::createSQLiteFile] pixel calibration data sqlite file could not be created. "
		      << "See " << m_pcdDir.c_str() << createSQLiteFileLog.c_str() << " !" << std::endl;
	  }
	} // if (makeCoolStrFileExists && writePixelCalibDBExists)
	else {
	  std::cout << "ERROR [PixCalibDataOffline::createSQLiteFile] pixel calibration data sqlite file creating scripts "
		    << makeCoolStrFileName.c_str() << " and/or "
		    << writePixelCalibDBName.c_str() << " does not exist !" << std::endl;
	}
      } // if (pcdTextFileExists)
      else {
	std::cout << "ERROR [PixCalibDataOffline::createSQLiteFile] pixel calibration data text file "
		  << pcdTextFileName.c_str() << " does not exist. Need to ExtractAnalysis first !" << std::endl;
      }
    } // if (isAthenaSetup())

  }

  void PixCalibDataOffline::transferToCOOL()
  {
    if (isAthenaSetup()) {
      std::string tot(m_totLineEdit->text().toLatin1().data());
      std::string threshold(m_thresholdLineEdit->text().toLatin1().data());
      std::string intimeThreshold(m_intimeThresholdLineEdit->text().toLatin1().data());
      std::string onlineTag(m_onlineTagLineEdit->text().toLatin1().data());
      std::string calibrationTag(m_calibrationTagLineEdit->text().toLatin1().data());
      std::string runPeriodStart(m_runPeriodStartLineEdit->text().toLatin1().data());
      std::string coolPswd(m_passwordLineEdit->text().toLatin1().data());
      if (tot.empty()) {
	std::cout << "ERROR [PixCalibDataOffline::transferToCOOL] missing ToT analysis number !" << std::endl;
	return;
      }
      if (threshold.empty()) {
	std::cout << "ERROR [PixCalibDataOffline::transferToCOOL] missing threshold analysis number !" << std::endl;
	return;
      }
      if (intimeThreshold.empty()) {
	std::cout << "ERROR [PixCalibDataOffline::transferToCOOL] missing intime-threshold analysis number !" << std::endl;
	return;
      }
      std::vector<std::string> onlineTagVec;
      boost::split(onlineTagVec, onlineTag, boost::is_any_of(","));
      if (onlineTag.empty() || onlineTagVec.size() != m_nOnlineTags) {
	std::cout << "ERROR [PixCalibDataOffline::transferToCOOL] missing online tags !" << std::endl;
	return;
      }
      std::vector<std::string> calibrationTagVec;
      boost::split(calibrationTagVec, calibrationTag, boost::is_any_of(","));
      if (calibrationTag.empty()) {
	std::cout << "ERROR [PixCalibDataOffline::transferToCOOL] missing calibration tag !" << std::endl;
	return;
      }
      if (runPeriodStart.empty()) {
	std::cout << "ERROR [PixCalibDataOffline::transferToCOOL] "
		  << "missing starting run number for interval of validity !" << std::endl;
	return;
      }
      if (coolPswd.empty()) {
	std::cout << "ERROR [PixCalibDataOffline::transferToCOOL] missing password for writing to COOL !" << std::endl;
	return;
      }
      std::string workDir(getenv("PWD"));
      std::stringstream appendtext;
      appendtext << "_" << m_totLineEdit->text().toLatin1().data() << "_"
		 << m_thresholdLineEdit->text().toLatin1().data() << "_"
		 << m_intimeThresholdLineEdit->text().toLatin1().data();
      std::string pcdSqlFileName(m_pcdName+appendtext.str()+".sql");
      std::string pcdSqlFilePathName(m_pcdDir+pcdSqlFileName);
      std::ifstream pcdSqlFile(pcdSqlFilePathName.c_str());
      bool pcdSqlFileExists = (pcdSqlFile) ? true : false;
      pcdSqlFile.close();
      if (pcdSqlFileExists) {
	std::string atlCoolMergeScript("AtlCoolMerge.py");
	std::string atlCoolMergeName(m_pcdDir+atlCoolMergeScript);
	std::ifstream atlCoolMergeFile(atlCoolMergeName.c_str());
	bool atlCoolMergeFileExists = (atlCoolMergeFile) ? true : false;
	atlCoolMergeFile.close();
	if (atlCoolMergeFileExists) {
	  std::stringstream message;
	  message << "Pixel calibration data will be transfered from " << pcdSqlFileName.c_str() << " to COOL.\n"
		  << "This may take about 2 minutes per calibration tag. Do you want to continue ?";
	  int answer = QMessageBox::warning(this, "WARNING", QString::fromStdString(message.str()), QMessageBox::No, QMessageBox::Yes);
	  if (answer == QMessageBox::No) return;
	  std::string oracleServer("ATLAS_COOLWRITE");
	  std::string oracleAccount("ATLAS_COOLOFL_PIXEL_W");
	  std::stringstream unixcmd;
	  unixcmd << "cd " << m_pcdDir << " && "
		  << "echo y | python " << atlCoolMergeScript.c_str()
		  << " --comment=\"update calibration data\" ./" << pcdSqlFileName.c_str() << " "
		  << m_dbName.c_str() << " " << oracleServer.c_str() << " " << oracleAccount.c_str() << " "
		  << coolPswd.c_str() << " && "
		  << "cd " << workDir;
	  int rvalue = system(unixcmd.str().c_str());
	  if (rvalue == 0) {
	    std::cout << "INFO [PixCalibDataOffline::transferToCOOL] pixel calibration data is transfered from "
		      << pcdSqlFileName.c_str() << " to COOL." << std::endl;
	  }
	  else {
	    std::cout << "ERROR [PixCalibDataOffline::transferToCOOL] "
		      << "pixel calibration data could not be transfered to COOL. See terminal for errors !" << std::endl;
	  }
	  std::string pcdKnowledgeSqlFileName(m_pcdName+"_knowledge.db");
	  std::string pcdKnowledgeSqlFilePathName(m_pcdDir+pcdKnowledgeSqlFileName);
	  std::ifstream pcdKnowledgeSqlFile(pcdKnowledgeSqlFilePathName.c_str());
	  bool pcdKnowledgeSqlFileExists = (pcdKnowledgeSqlFile) ? true : false;
	  pcdKnowledgeSqlFile.close();
	  if (pcdKnowledgeSqlFileExists) {
	    unixcmd.str(std::string());
	    unixcmd << "chmod 644 " << pcdKnowledgeSqlFilePathName.c_str();
	    system(unixcmd.str().c_str());
	  } // if (pcdKnowledgeSqlFileExists)
	  std::string dbConnection("sqlite_file:"+pcdKnowledgeSqlFilePathName);
	  std::string calibDbInterfaceCmd("calibDbInterface.exe");
	  unixcmd.str(std::string());
	  unixcmd << "export PIXEL_CAL_KNOWLEDGE=\"" << dbConnection.c_str() << "\" && "
		  << calibDbInterfaceCmd << " -s " << runPeriodStart.c_str() << " ";
	  for (unsigned int i = 0; i < onlineTagVec.size(); i++) unixcmd << onlineTagVec[i].c_str() << " ";
	  for (unsigned int i = 0; i < calibrationTagVec.size(); i++) unixcmd << calibrationTagVec[i].c_str() << " ";
	  unixcmd << "&& "
		  << "chmod 444 " << pcdKnowledgeSqlFilePathName.c_str();
	  rvalue = system(unixcmd.str().c_str());
	  if (rvalue == 0) {
	    std::cout << "INFO [PixCalibDataOffline::transferToCOOL] pixel calibration knowledge database "
		      << pcdKnowledgeSqlFileName.c_str() << " is updated with new calibration tag(s)." << std::endl;
	  }
	  else {
	    std::cout << "ERROR [PixCalibDataOffline::transferToCOOL] "
		      << "pixel calibration knowledge database could not be updated. See terminal for errors !" << std::endl;
	  }
	} // if (atlCoolMergeFileExists)
	else {
	  std::cout << "ERROR [PixCalibDataOffline::transferToCOOL] ATLAS COOL merge script "
		    << atlCoolMergeScript.c_str() << " does not exist !" << std::endl;
	}
      } // if (pcdSqlFileExists)
      else {
	std::cout << "ERROR [PixCalibDataOffline::transferToCOOL] pixel calibration data sqlite file "
		  << pcdSqlFilePathName.c_str() << " does not exist. Need to CreateSQLiteFile first !" << std::endl;
      }
    } // if (isAthenaSetup())

  }

  bool PixCalibDataOffline::isAthenaSetup()
  {
    bool isathenasetup = false;
    std::string atlasAreaEnvName("AtlasArea");
    char* atlasAreaEnv = getenv(atlasAreaEnvName.c_str());
    if (atlasAreaEnv == NULL) {
      std::cout << "ERROR [PixCalibDataOffline::isAthenaSetup] environment variable "
		<< atlasAreaEnvName.c_str() << " does not exist, so need to setup athena first !" << std::endl;
      isathenasetup = false;
      return isathenasetup;
    }
    else {
      std::cout << "INFO [PixCalibDataOffline::isAthenaSetup] environment variable "
		<< atlasAreaEnvName.c_str() << " exists, so athena is already setup ..." << std::endl;
      isathenasetup = true;
    }

    return isathenasetup;
  }

}
