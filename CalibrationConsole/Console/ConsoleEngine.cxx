#include "ConsoleEngine.h"

#include "PixActions/PixActionsMulti.h"
#include "DbTagsPanel.h"

#include "ConfigWrapper/ConnectivityUtil.h"
#include "ConfigWrapper/PixDisableUtil.h"
#include "isUtil.h"

#include "ExitHandler.h"
#include <CommandExecutor.h>

#include "IStatusMonitorActivator.h"
#include "SetActiveCommand.h"

namespace PixLib
{
  class PixActions;
}

namespace PixCon
{

  /** Do nothing status monitoring activator class
   */
  class NoStatusMonitorActivator : public IStatusMonitorActivator
  {
  public:
    bool activateScanMonitoring(SerialNumber_t /*scan_serial_number*/,
				unsigned int /*mask_stage_loop_index*/,
				const std::vector<unsigned short> &/*start_value*/,
				const std::vector<unsigned short> &/*max_value*/,
				unsigned long /*max_step_time*/) { return false; }
    void abortScanMonitoring(SerialNumber_t) { }
    bool activateAnalysisMonitoring(SerialNumber_t) { return false; }
    void abortAnalysisMonitoring(SerialNumber_t) { }
    bool activateActionMonitoring(const std::vector<std::string> &) { return false; }
    void abortActionMonitoring() { }
  };

  const std::string ConsoleEngine::s_currentActionIsName="CURRENT-ACTION";
  const std::string ConsoleEngine::s_currentActionStatusIsName="STATUS";
  const std::string ConsoleEngine::s_scanStatusIsName="STATUS";

  const char *ConsoleEngine::s_rodCounterVarNames[4]={
    "IDX-2",
    "IDX-1",
    "IDX-0",
    "Mask stage"
  };


  ConsoleEngine::ConsoleEngine(IPCPartition *partition,
			       const std::string &is_server_name,
			       const std::string &name_server_name,
			       const std::string &oh_server_name,
			       const std::shared_ptr<PixCon::CalibrationDataManager> &calibData,
			       const std::shared_ptr<StatusChangeTransceiver> &status_change_transceiver,
			       PixCon::UserMode::EUserMode userMode)
    : m_partition(partition),
      m_isServerName(is_server_name),
      m_nameServerName(name_server_name),
      m_ohServerName(oh_server_name),
      m_isDictionary(new ISInfoDictionary(*partition)),
      m_calibrationData(calibData),
      m_statusChangeTransceiver(status_change_transceiver),
      m_userMode(userMode),
      m_exitHelper(this),
      m_statusMonitorActivator(new NoStatusMonitorActivator),
      m_executor(new CommandExecutor())
  {
    installExitHandler();
    reconnect();

    const char *default_data_path = getenv("PIXSCAN_STORAGE_PATH");
    if (default_data_path) {
      setResultFileTemplate(default_data_path);
    }
  }

  bool ConsoleEngine::reconnect()
  {
    m_broker.reset();
    //if (!m_broker.get()) {
      std::string brokerName = "my_broker";
      brokerName = "CalibrationConsole_" + m_partition->name();
      try {
	{
	  Lock action_loc(multiActionMutex());
	  m_broker = std::shared_ptr<PixLib::PixBrokerMultiCrate>(new PixLib::PixBrokerMultiCrate(brokerName, m_partition),
								    destroyer<PixLib::PixBrokerMultiCrate>);
	}
	updateConnectivity();
	return true;
      }
      catch (CORBA::TRANSIENT& err) {
	std::cerr << "ERROR [ConsoleEngine::reconnect] CORBA error. Communication failure with remote object."
		  << " Did not get a multi crate broker." << std::endl;
      }
      catch (CORBA::COMM_FAILURE &err) {
	std::cerr << "ERROR [ConsoleEngine::reconnect] CORBA error. Communication failure with remote object."
		  << " Did not get a multi crate broker." << std::endl;
      }
      catch (std::exception &err) {
	std::cerr << "ERROR [ConsoleEngine::reconnect] Caught std exception : " << err.what()
		  << " Did not get a multi crate broker." << std::endl;
      }
    //}
    return false;
  }

  bool ConsoleEngine::loadPixelSafeCfg()
  {
    PixA::ConnectivityRef conn(getConn());
    std::string current_id_tag;
    std::string current_conn_tag;
    std::string current_cfg_tag;
    std::string current_mod_cfg_tag;
    if (conn) {
      current_id_tag = conn.tag(PixA::IConnectivity::kId);
      current_conn_tag = conn.tag(PixA::IConnectivity::kConn);
      current_cfg_tag = conn.tag(PixA::IConnectivity::kConfig);
      current_mod_cfg_tag = conn.tag(PixA::IConnectivity::kModConfig);
    }
    std::string safeModCfgContain("STANDBY");
    if (!current_mod_cfg_tag.empty() && current_mod_cfg_tag.find(safeModCfgContain) != std::string::npos) {
      std::cout << "INFO [ConsoleEngine::loadPixelSafeCfg] Pixel safe config "
		<< current_mod_cfg_tag << " is already loaded." << std::endl;
      return true;
    }
    if (!current_id_tag.empty() && current_id_tag.find("PIXEL") != std::string::npos && !current_conn_tag.empty()) {
      DbTagsPanel tags_chooser(current_id_tag, current_conn_tag, current_cfg_tag, current_mod_cfg_tag,
			       NULL,partition().name(),"PixelDbServer");
      for (int i = 0; i < tags_chooser.comboBoxTagModCfg->count(); i++) {
	std::string safeModTag(tags_chooser.comboBoxTagModCfg->itemText(i).toLatin1().data());
	if (safeModTag.find(safeModCfgContain) != std::string::npos) {
	  try {
	    std::cout << "INFO [ConsoleEngine::loadPixelSafeCfg] Loading pixel safe config: "
		      << tags_chooser.getObjectTag()
		      << "," << tags_chooser.getConnectivityTag()
		      << "," << tags_chooser.getConfigurationTag()
		      << "," << safeModTag << std::endl;
	    std::unique_ptr<PixLib::PixISManager> isMgr(new PixLib::PixISManager(partition().name(),"PixelRunParams"));
	    isMgr->publish("DataTakingConfig-IdTag",tags_chooser.getObjectTag());
	    isMgr->publish("DataTakingConfig-ConnTag",tags_chooser.getConnectivityTag());
	    isMgr->publish("DataTakingConfig-CfgTag",tags_chooser.getConfigurationTag());
	    isMgr->publish("DataTakingConfig-ModCfgTag",safeModTag);
	    updateConnectivityFromIS();
	    updateConnectivity();
	  }
	  catch(std::exception &err) {
	    std::cout << "FATAL [ConsoleEngine::loadPixelSafeCfg] Caught exception : " << err.what() << std::endl;
	    return false;
	  }
	  catch(...) {
	    std::cout << "FATAL [ConsoleEngine::loadPixelSafeCfg] Caught unhandled exception." << std::endl;
	    return false;
	  }
	  return true;
	}
      }
      std::cout << "WARNING [ConsoleEngine::loadPixelSafeCfg] Pixel safe config *"
		<< safeModCfgContain << "* does not exist in DbServer. Load from database first" << std::endl;
      return false;
    } // if (!current_id_tag.empty() && current_id_tag == "SR1" && !current_conn_tag.empty())
    return true;
  }

  const bool ConsoleEngine::isSafe(const std::string scanType)
  {
    if(m_userMode != PixCon::UserMode::kExpert ){
      if( scanType != "DIGITAL_TEST" && scanType != "BOC_THR_DEL_DSP" ){
          if (m_action.get()) {
            Lock action_loc(multiActionMutex());
            bool hvOff = m_action.get()->isHVOff( );
            if(hvOff)return false;
          } else {
             std::cout << "ERROR [ConsoleEngine::isSafe ] Cannot get actions"<<std::endl;
             return false;
          }
        }
      }
    return true;
  }

  bool ConsoleEngine::updateConnectivity()
  {
    if (!m_broker.get()) {
      reconnect();
      if (!m_broker.get()) {
	std::cerr << "ERROR [ConsoleEngine::getConnectivity] no broker.";
	return false;
      }
    }

    try {
      // Get the tags to construct the PixConnectivity class (no choice of tags allowed by now)
      std::map<std::string,std::vector<std::string> > tags_map;
      {
	Lock action_loc(multiActionMutex());
	tags_map = m_broker->getConnectivityTags();
      }
      // Just take first vector. Should actually check that they are all the same 
      if (!tags_map.empty()) {
	std::map<std::string,std::vector<std::string> >::iterator it = tags_map.begin();
	std::vector<std::string> tags_vec = (*it).second;
	if (tags_vec.size() !=0  ) {
	  time_t best_guess_of_revision = ::time(NULL);
	  if ( tags_vec.size()== 5 ) {
	    PixCon::Lock lock(m_calibrationData->calibrationDataMutex());
	    PixA::ConnectivityRef old_conn = getConn();
	    m_calibrationData->changeCurrentConnectivity(tags_vec[4],tags_vec[0],tags_vec[1],tags_vec[2],tags_vec[3],best_guess_of_revision,true);
	    if (old_conn && getConn() &&  old_conn == getConn()) {
	      return true;
	    }
	    // Get ConnectivityRef from calibrationData
	    //	  m_conn = m_calibrationData->currentConnectivity();
	  }
	  else if ( tags_vec.size()== 7 ) {
	    if (tags_vec[4]+"-CFG" != tags_vec[5]) {
	      std::cout << "FATAL [ConsoleEngine::updateConnectivity] The id tag for configurations may not be independent of the id tag of the connectivity : "
			<< tags_vec[4] << "-CFG" << " != " << tags_vec[5]  << "." << std::endl;
	    }
	    PixCon::Lock lock(m_calibrationData->calibrationDataMutex());
	    PixA::ConnectivityRef old_conn = getConn();
	    m_calibrationData->changeCurrentConnectivity(tags_vec[4],tags_vec[0],tags_vec[1],tags_vec[2],tags_vec[3],tags_vec[6],best_guess_of_revision,true);
	    if (old_conn && getConn() && old_conn == getConn()) {
	      return true;
	    }
	    // Get ConnectivityRef from calibrationData
	    //	  m_conn = m_calibrationData->currentConnectivity();
	  }
	  else {
	    std::cout << "ERROR [ConsoleEngine::updateConnectivity] Expected 5 or7  elements in the tags vector which is returned by the broker, but it contains "
		      << tags_vec.size() << " elements." << std::endl;
	  }
	}
      }
    }
    catch (CORBA::TRANSIENT& err) {
      std::cerr << "ERROR [ConsoleEngine::updateConnectivity] CORBA error. Communication failure with remote object."
		<< " Did not get new connectivity tags." << std::endl;
    }
    catch (CORBA::COMM_FAILURE &err) {
      std::cerr << "ERROR [ConsoleEngine::updateConnectivity] CORBA error. Communication failure with remote object."
		<< " Did not get new connectivity tags." << std::endl;
    }
    catch (std::exception &err) {
      std::cerr << "ERROR [ConsoleEngine::updateConnectivity] Caught std exception : " << err.what()
		<< " Did not get new connectivity tags." << std::endl;
    }
    return false;
  }
 
  ConsoleEngine::~ConsoleEngine() {
    m_executor->shutdown();
    m_executor->shutdownWait();
    deallocate();
    m_broker.reset();
  }

  PixA::ConnectivityRef ConsoleEngine::getConn()
  {
    return m_calibrationData->currentConnectivity();
  }

  std::set<std::string> ConsoleEngine::getActionsList(std::string crateName){
    std::set<std::string> actionsSet;
    Lock action_loc(multiActionMutex());
    _getActionsList(crateName, actionsSet);
    return actionsSet;
  }

  void ConsoleEngine::_getActionsList(std::string crateName, std::set<std::string> &actionsSet){
    if (m_broker.get()){

      // @todo should throw a common  exception in case of failure e.g. throw BrokerCommunicationFailure("No action list.");
      try {
	actionsSet =  m_broker->listActions(true,PixLib::PixActions::SINGLE_ROD,"",crateName,"");
      }
      catch (CORBA::TRANSIENT& err) {
	std::cerr << "ERROR [ConsoleEngine::getActionsList] CORBA error. Communication failure with remote object."
		  << " Did not get list of actions." << std::endl;
      }
      catch (CORBA::COMM_FAILURE &err) {
	std::cerr << "ERROR [ConsoleEngine::getActionsList] CORBA error. Communication failure with remote object."
		  << " Did not get list of actions." << std::endl;
      }
      catch (std::exception &err) {
	std::cerr << "ERROR [ConsoleEngine::getActionsList] Caught std exception : " << err.what()
		  << " Did not get list of actions." << std::endl;
      }

    }
  }

  std::string ConsoleEngine::actionName(std::string rodName, std::string crateName) {
    std::string s_tmp = "SINGLE_ROD:";
    s_tmp+=crateName;
    s_tmp+="/"; 
    s_tmp+=rodName;
    return s_tmp;
  }


  void ConsoleEngine::allocate(std::map<std::string, std::vector<std::string> > &rodList)
  {
    if (!m_broker.get()) {
      reconnect();
      if (!m_broker.get()) {
	std::cerr << "ERROR [ConsoleEngine::allocate] no broker.";
	return;
      }
    }

//     for(std::map<std::string, std::vector<std::string> >::iterator mit = rodList.begin(); mit != rodList.end(); mit++){
//       std::cout << "INFO [ConsoleEngine::allocate] allocate  : " << mit->first << std::endl;
//       for (std::vector<std::string>::const_iterator rod_iter = mit->second.begin();
// 	   rod_iter != mit->second.end();
// 	   rod_iter++) {
// 	std::cout << "INFO [ConsoleEngine::allocate] " << mit->first << " / " << *rod_iter << std::endl;
//       }
//     }

    Lock action_loc(multiActionMutex());
    if (!m_broker.get()) {
      std::cout << "ERROR [ConsoleEngine::allocate] No broker. Nothing allocated." <<std::endl;
      return;
    }

    std::set<std::string> actions;
    PixA::ConnectivityRef conn = getConn();
    if (!conn) {
      std::cout << "ERROR [ConsoleEngine::allocate] No connectivity. Nothing allocated." <<std::endl;
      return;
    }

    {
      std::map<std::string, std::vector<std::string> >::iterator crate_iter = rodList.begin();
      bool reached_crate_list_end = (crate_iter == rodList.end());
      while (!reached_crate_list_end) {

	std::map<std::string, std::vector<std::string> >::iterator current_crate_iter = crate_iter++;
	reached_crate_list_end = (crate_iter == rodList.end());

	// look for each crate in the connectivity DB
	PixA::CrateList crate_list = conn.crates();
	PixA::CrateList::const_iterator crate_list_iter = crate_list.begin();
	for (crate_list_iter = crate_list.begin(); crate_list_iter != crate_list.end(); ++crate_list_iter) {
	  if (PixA::connectivityName(crate_list_iter) == (*current_crate_iter).first) {
	    break;
	  }
	}
	if (   crate_list_iter == crate_list.end()
	       || !const_cast<PixLib::RodCrateConnectivity *>(*crate_list_iter)->enableReadout) {

	  if (crate_list_iter == crate_list.end()) {
	    std::cout << "ERROR [ConsoleEngine::allocate] Requested crate " << (*current_crate_iter).first  << " not in this connectivity DB" << std::endl;
	  }
	  else {
	    std::cout << "ERROR [ConsoleEngine::allocate] Requested crate " << (*current_crate_iter).first  << " is disabled" << std::endl;
	  }

	  rodList.erase(current_crate_iter);
	}
	else {
	  // crate is in DB
	  std::set<std::string> actionsList;
	  _getActionsList(PixA::connectivityName(crate_list_iter), actionsList);
	  std::vector<std::string> &crate_rod_list = (*current_crate_iter).second;
	  std::vector<std::string> existing_rod_list;
	  existing_rod_list.reserve(crate_rod_list.size());

	  for (std::vector<std::string>::iterator rod_iter = crate_rod_list.begin();
	       rod_iter != crate_rod_list.end();
	       rod_iter++) {

	    const PixLib::RodBocConnectivity *rod_boc_conn=NULL;
	    try {
	      rod_boc_conn = conn.findRod(*rod_iter);
	    }
	    catch(PixA::ConfigException &) {
	    }

	    if (rod_boc_conn) {
	      if(const_cast<PixLib::RodBocConnectivity *>(rod_boc_conn)->enableReadout){
		std::string action_name = actionName(PixA::connectivityName(rod_boc_conn),PixA::connectivityName(crate_list_iter));
		if (actionsList.find(action_name) != actionsList.end()) {
		  existing_rod_list.push_back(*rod_iter);
		  actions.insert(action_name);
		}
	      }
	    }
	    else {
	      std::cout << "ERROR [ConsoleEngine::allocate] Requested ROD " << *rod_iter  << " not in this connectivity DB" << std::endl;
	    }
	  }
	  current_crate_iter->second = existing_rod_list;

	}
      }
    }

    //@todo why the dynamic cast to PixActionsMulti ? Is a PixActions not good enough ?
    try {
      m_action = std::shared_ptr<PixLib::PixActionsMulti>(dynamic_cast<PixLib::PixActionsMulti *>(m_broker->allocateActions(actions)),
							    destroyer<PixLib::PixActionsMulti>);

      PixA::CrateList crate_list = conn.crates();
      for (PixA::CrateList::const_iterator crate_list_iter = crate_list.begin();
	   crate_list_iter != crate_list.end();
	   ++crate_list_iter) {

	std::map<std::string, std::vector<std::string> >::iterator crate_iter = rodList.find(PixA::connectivityName(crate_list_iter));
	if (crate_iter == rodList.end()){
	  // deallocate crate
	  m_calibrationData->setCrateAllocated(PixA::connectivityName(crate_list_iter), kCurrent, 0, false);
	}
	else {

	  PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_list_iter);
	  PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_list_iter);
	  for (PixA::RodList::const_iterator rod_list_iter = rod_begin;
	       rod_list_iter != rod_end;
	       ++rod_list_iter) {
	    // deallocate all RODs
	    m_calibrationData->setRodAllocated(PixA::connectivityName(rod_list_iter), kCurrent, 0, false);
	  }

	  std::vector<std::string> &rod_name_list = (*crate_iter).second;
	  for (std::vector<std::string>::const_iterator rod_name_iter = rod_name_list.begin();
	       rod_name_iter != rod_name_list.end();
	       rod_name_iter++) {

	    if (conn.findRod( *rod_name_iter )) {
	      m_calibrationData->setRodAllocated(*rod_name_iter, kCurrent, 0, true);
	      PixA::Pp0List pp0_list(conn.pp0s(*rod_name_iter));
	      PixA::Pp0List::const_iterator pp0_begin = pp0_list.begin();
	      PixA::Pp0List::const_iterator pp0_end = pp0_list.end();
	      
	      for (PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
		   pp0_iter != pp0_end;
		   ++pp0_iter) {
		PixA::ModuleList module_list(const_cast<PixLib::Pp0Connectivity&>(**pp0_iter));
		PixA::ModuleList::const_iterator module_begin = module_list.begin();
		PixA::ModuleList::const_iterator module_end = module_list.end();
		
		for (PixA::ModuleList::const_iterator module_iter = module_begin;
		     module_iter != module_end;
		     ++module_iter) {
		  m_calibrationData->setModuleAllocated(PixA::connectivityName(module_iter), kCurrent, 0, true);
		}
	      }
	    }
	  }
	}
      }
    }
    catch (CORBA::TRANSIENT& err) {
      std::cerr << "ERROR [ConsoleEngine::allocateActions] CORBA error. Communication failure with remote object."
		<< " Did not manage to allocate actions." << std::endl;
    }
    catch (CORBA::COMM_FAILURE &err) {
      std::cerr << "ERROR [ConsoleEngine::allocateActions] CORBA error. Communication failure with remote object."
		<< " Did not manage to allocate actions." << std::endl;
    }
    catch (std::exception &err) {
      std::cerr << "ERROR [ConsoleEngine::allocateActions] Caught std exception : " << err.what()
		<< " Did not manage to allocate actions." << std::endl;
    }

    m_rodList = rodList;
  }

  void ConsoleEngine::deallocate() {
    Lock action_loc(multiActionMutex());
    if (m_broker.get() != NULL && m_action.get() != NULL) {
      try {
	if (m_action.get()) {
          try {
            m_broker->deallocateActions(m_action.get());
          }
          catch (...) {
          }
	}
	m_action.reset();
	//	m_broker.reset();
      }
      catch (CORBA::TRANSIENT& err) {
	std::cerr << "ERROR [ConsoleEngine::deallocate] CORBA error. Communication failure with remote object."
		  << " Presumably, the actions are not properly deallocated." << std::endl;
      }
      catch (CORBA::COMM_FAILURE &err) {
	std::cerr << "ERROR [ConsoleEngine::deallocate] CORBA error. Communication failure with remote object."
		  << " Presumably, the actions are not properly deallocated." << std::endl;
      }
      catch (std::exception &err) {
	std::cerr << "ERROR [ConsoleEngine::deallocate] Caught std exception " << err.what()
		  << " Presumably, the actions are not properly deallocated." << std::endl;
      }
    }
  }

  void ConsoleEngine::emergencyDeallocate() {
    // ignores the action lock.
    std::cout << "INFO [ConsoleEngine::emergencyDeallocate] Deallocated actions and broker." << std::endl;
    if (m_broker.get() != NULL && m_action.get() != NULL) {

      try {
	m_broker->deallocateActions(m_action.get());
      }
      catch (...) {
      }

    }
    m_action.reset();
    m_broker.reset();
  }

  //  bool ConsoleEngine::getRodEnabled(std::string rodName) {
  //     bool en = false;
  //     for (std::map<std::string, std::vector<std::string> >::iterator cit = m_rods.begin(); cit != m_rods.end(); cit++) {
  //       std::vector<std::string> &vec = (*cit).second;
  //       for (std::vector<std::string>::iterator rit = vec.begin(); rit != vec.end(); rit++) {
  // 	if (*rit == rodName) {
  // 	  std::string mAct = rodName+"/Active";
  // 	  try{
  // 	    en = (bool)m_is->read<int>(mAct);
  // 	  }catch(...){
  // 	    std::cout << "Variable " << mAct << " is non-existing in the IS server" << std::endl;
  // 	    std::cout << "Will active = 0" << std::endl;
  // 	  }
  // 	  return en;
  // 	}
  //       }
  //     }
  //     return en;
  //  }

  //  bool ConsoleEngine::getModuleEnabled(std::string rodName, std::string moduleName) {
  //     bool en = false;
  //     for (std::map<std::string, std::vector<std::string> >::iterator cit = m_rods.begin(); cit != m_rods.end(); cit++) {
  //       std::vector<std::string> &vec = (*cit).second;
  //       for (std::vector<std::string>::iterator rit = vec.begin(); rit != vec.end(); rit++) {
  // 	if (*rit == rodName) {
  // 	  std::string mAct = rodName+"/"+moduleName+"/Active";
  // 	  try{
  // 	    en = (bool)m_is->read<int>(mAct);
  // 	  }catch(...){
	    
  // 	    std::cout << "Variable " << mAct << " is non-existing in the IS server" << std::endl;
  // 	    std::cout << "Will report module active = 0" << std::endl;
  // 	  }
  // 	  return en;
  // 	}
  //       }
  //     }
  //     return en;
  //  }

  void ConsoleEngine::resetActionIsStatus()
  {
    ISInfoString empty_string;
    empty_string.setValue("");
    ISType is_info_string_type=empty_string.type();

    PixA::ConnectivityRef conn=m_calibrationData->currentConnectivity();
    if (!conn) return;

    for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
	 crate_iter != conn.crates().end();
	 ++crate_iter) {

      PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
      PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
      for (PixA::RodList::const_iterator rod_iter = rod_begin;
	   rod_iter != rod_end;
	   ++rod_iter) {

	// only consider enabled RODs
	try {
	  const CalibrationData::ConnObjDataList conn_obj_data ( m_calibrationData->calibrationData().getConnObjectData( PixA::connectivityName(rod_iter)) );
	  if ( conn_obj_data.currentEnableState().enabled() ) {

	    setIsValue(*m_isDictionary,
		       is_info_string_type,
		       makeIsName(PixA::connectivityName(crate_iter),
				  PixA::connectivityName(rod_iter),
				  s_currentActionIsName),
		       empty_string);

	    setIsValue(*m_isDictionary,
		       is_info_string_type,
		       makeIsName(PixA::connectivityName(crate_iter),
				  PixA::connectivityName(rod_iter),
				  s_currentActionStatusIsName),
		       empty_string);
	  }
	}
	catch(CalibrationData::MissingConnObject &) {
	}
      }
    }
  }

  std::string ConsoleEngine::makeIsName(SerialNumber_t scan_serial_number,
                                        const std::string &crate_name,
                                        const std::string &rod_name,
                                        const std::string &var_name) {
     std::stringstream message;
     message << m_isServerName <<  ".S" << std::setfill('0') << std::setw(9) << scan_serial_number
             << '/'<< crate_name + "/" + rod_name + "/" + var_name;
     return message.str(); 
 }


  void ConsoleEngine::abortScansOfDisabledRods(SerialNumber_t scan_serial_number)
  {
    ISInfoString abort_string;
    abort_string.setValue("ABORTED");
    ISType is_info_string_type=abort_string.type();

    PixA::ConnectivityRef conn=m_calibrationData->currentConnectivity();
    if (!conn) return;

    for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
	 crate_iter != conn.crates().end();
	 ++crate_iter) {

      PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
      PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
      for (PixA::RodList::const_iterator rod_iter = rod_begin;
	   rod_iter != rod_end;
	   ++rod_iter) {

	// only consider enabled RODs
	try {
	  const CalibrationData::ConnObjDataList conn_obj_data ( m_calibrationData->calibrationData().getConnObjectData( PixA::connectivityName(rod_iter)) );
	  if ( !conn_obj_data.currentEnableState().enabled() ) {

	    setIsValue(*m_isDictionary,
		       is_info_string_type,
		       makeIsName(scan_serial_number,
	                          PixA::connectivityName(crate_iter),
				  PixA::connectivityName(rod_iter),
				  s_scanStatusIsName),
		       abort_string);
	  }
	}
	catch(CalibrationData::MissingConnObject &) {
	}
      }
    }
  }

  void ConsoleEngine::markRodsWithoutEnabledModulesAsDone(SerialNumber_t scan_serial_number)
  {
    ISInfoString ignore_string;
    ignore_string.setValue("DONE");
    ISType is_info_string_type=ignore_string.type();

    PixA::ConnectivityRef conn=m_calibrationData->currentConnectivity();
    if (!conn) return;

    for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
	 crate_iter != conn.crates().end();
	 ++crate_iter) {

      PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
      PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
      for (PixA::RodList::const_iterator rod_iter = rod_begin;
	   rod_iter != rod_end;
	   ++rod_iter) {

	// only consider enabled RODs
	try {
	  const CalibrationData::ConnObjDataList conn_obj_data ( m_calibrationData->calibrationData().getConnObjectData( PixA::connectivityName(rod_iter)) );
	  if ( conn_obj_data.enableState(kScan, scan_serial_number).enabled() ) {

	    bool has_enabled_modules=false;

	    PixA::Pp0List::const_iterator pp0_begin = PixA::pp0Begin(rod_iter);
	    PixA::Pp0List::const_iterator pp0_end = PixA::pp0End(rod_iter);
	    for (PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
		 pp0_iter != pp0_end;
		 ++pp0_iter) {

	      PixA::ModuleList::const_iterator module_begin = PixA::modulesBegin(pp0_iter);
	      PixA::ModuleList::const_iterator module_end = PixA::modulesEnd(pp0_iter);
	      for (PixA::ModuleList::const_iterator module_iter = module_begin;
		   module_iter != module_end;
		   ++module_iter) {

		try {
		  PixCon::CalibrationData::ConnObjDataList data_list( m_calibrationData->calibrationData().getConnObjectData( PixA::connectivityName(module_iter) ));
		  if (data_list.enableState(kScan, scan_serial_number).enabled()) {
		    has_enabled_modules=true;
		    break;
		  }
		}
		catch( PixCon::CalibrationData::MissingConnObject &) {
		}
	      }
	    }

	    if (!has_enabled_modules) {

	      setIsValue(*m_isDictionary,
			 is_info_string_type,
			 makeIsName(scan_serial_number,
				    PixA::connectivityName(crate_iter),
				    PixA::connectivityName(rod_iter),
				    s_scanStatusIsName),
			 ignore_string);
	    }
	  }
	}
	catch(CalibrationData::MissingConnObject &) {
	}
      }
    }
  }


  void ConsoleEngine::resetIsLoopCounter()
  {
    ISInfoT<int> counter_value;
    counter_value.setValue(0);
    ISType is_info_int_type=counter_value.type();

    PixA::ConnectivityRef conn=m_calibrationData->currentConnectivity();
    if (!conn) return;

    for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
	 crate_iter != conn.crates().end();
	 ++crate_iter) {

      PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
      PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
      for (PixA::RodList::const_iterator rod_iter = rod_begin;
	   rod_iter != rod_end;
	   ++rod_iter) {

	// only consider enabled RODs
	try {
	  const CalibrationData::ConnObjDataList conn_obj_data ( m_calibrationData->calibrationData().getConnObjectData( PixA::connectivityName(rod_iter)) );
	  if ( conn_obj_data.currentEnableState().enabled() ) {

	    for (unsigned int counter_i=0; counter_i<4;counter_i++) {
	      setIsValue(*m_isDictionary,
			 is_info_int_type,
			 makeIsName(PixA::connectivityName(crate_iter),
				    PixA::connectivityName(rod_iter),
				    s_rodCounterVarNames[counter_i]),
			 counter_value);
	    }
	  }
	}
	catch(CalibrationData::MissingConnObject &) {
	}
      }
    }
  }

  std::string ConsoleEngine::resultFileTemplate() const {
    std::string temp;
    {
      Lock data_lock(m_dataMutex);
      temp  = m_resultStorePath;
    }

    if (!temp.empty() && temp[temp.size()-1]=='/') {
      temp += "SCAN_%f.root";
    }
    return temp;
  }

  void ConsoleEngine::setResultFileTemplate(const std::string &new_result_store_path)
  {
    Lock lock(m_dataMutex);
    m_resultStorePath = new_result_store_path;
    if (!m_resultStorePath.empty()) {
      if ( m_resultStorePath[0]=='/') {
	if (m_resultStorePath[m_resultStorePath.size()-1]!='/') {
	  if (m_resultStorePath.size()>5 && m_resultStorePath.compare(m_resultStorePath.size()-5,5,".root")==0) {
	    std::string::size_type pos = m_resultStorePath.rfind("/");
	    if (pos == std::string::npos || m_resultStorePath.find("%f",pos)== std::string::npos) {
	      std::cerr << "ERROR [ConsoleEngine::ctor] PIXSCAN_STORAGE_PATH must be an absolut path name which may be extended by"
			<<  " a file name template which contains %f." << std::endl;
	      m_resultStorePath="";
	    }
	  }
	  else {
	    m_resultStorePath += '/';
	  }
	}
      }
      else {
	std::cerr << "ERROR [ConsoleEngine::ctor] PIXSCAN_STORAGE_PATH must be an absolut path name or root file name which contains %f." << std::endl;
	m_resultStorePath="";
      }
    }
  }

  std::shared_ptr<const PixLib::PixDisable> ConsoleEngine::currentDisable() {
    return m_disable;
  }

  void ConsoleEngine::applyDisable()
  {
    if (!getConn()) return;
    if (m_disable.get() == NULL) return;
    PixA::DisabledListRoot disList(m_disable);
    std::vector<std::pair< std::string, bool> > rod_active_list;
    rod_active_list.reserve(144);
    std::vector<std::pair< std::string ,bool > >  module_active_list;
    module_active_list.reserve(1744);

    {
    std::cout << "INFO [ConsoleEngine::applyDisable] disable is not null, I propagate it" << std::endl;
    PixCon::Lock lock(m_calibrationData->calibrationDataMutex());
    std::unique_ptr<PixLib::PixISManager> is_mgr(new PixLib::PixISManager(partition().name(),"RunParams"));
    PixA::CrateList crate_list = getConn().crates();
    for (PixA::CrateList::const_iterator crate_list_iter = crate_list.begin();
	 crate_list_iter != crate_list.end();
	 ++crate_list_iter) {
      bool part_enable = disList.enabled(PixA::partitionName(crate_list_iter));
      bool crate_enable = disList.enabled(PixA::connectivityName(crate_list_iter));
      PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_list_iter);
      PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_list_iter);
      for (PixA::RodList::const_iterator rod_list_iter = rod_begin;
	   rod_list_iter != rod_end;
	   ++rod_list_iter) {
	try {
	  CalibrationData::ConnObjDataList conn_object_rod( m_calibrationData->calibrationData().getConnObjectData( PixA::connectivityName(rod_list_iter) ) );
	  const CalibrationData::EnableState_t &state_rod = conn_object_rod.enableState(kCurrent, 0);
	  if (state_rod.isAllocated() && state_rod.isParentAllocated()) {
	    bool rod_enable = disList.enabled(PixA::connectivityName(rod_list_iter));
            {	
	        std::string rAct = PixA::connectivityName(rod_list_iter) +  "/Active";
	        bool curren_rod=1 ^ rod_enable;
	        try {
	           curren_rod = (is_mgr->read<int>(rAct) > 0) ? 1 : 0;
	        }
	        catch(...) {
	        }

//                bool curren = (is_mgr->read<int>(mAct) > 0) ? 1 : 0;
		std::cout << "INFO [ConsoleEngine::applyDisable] " << rAct << " is " << curren_rod << ", I want it to be " << (rod_enable && crate_enable && part_enable) << std::endl;  
		if (curren_rod != (rod_enable && crate_enable && part_enable)) {
	           rod_active_list.push_back(std::make_pair(PixA::connectivityName(rod_list_iter), rod_enable && crate_enable && part_enable) );
		}
            }
	    PixA::Pp0List::const_iterator pp0_begin = PixA::pp0Begin(rod_list_iter);
	    PixA::Pp0List::const_iterator pp0_end = PixA::pp0End(rod_list_iter);
	    for (PixA::Pp0List::const_iterator pp0_list_iter = pp0_begin;
		 pp0_list_iter != pp0_end;
		 ++pp0_list_iter) {
	      bool pp0en = disList.enabled(PixA::connectivityName(pp0_list_iter));
	      // Setting enable state of all PP0s, since the actual value doesn't seem to be always correct.
	      //	      ICommand *set_active = new SetActiveCommand(std::shared_ptr<PixCon::ConsoleEngine>(this),kPp0Item,PixA::connectivityName(pp0_list_iter),pp0en);
	      //	      m_executor->appendCommand(set_active,NULL,0);
	      PixA::ModuleList::const_iterator module_begin = PixA::modulesBegin(pp0_list_iter);
	      PixA::ModuleList::const_iterator module_end = PixA::modulesEnd(pp0_list_iter);
	      for (PixA::ModuleList::const_iterator module_list_iter = module_begin;
		   module_list_iter != module_end;
		   ++module_list_iter) {
		bool moden = disList.enabled(PixA::connectivityName(module_list_iter));
		// Changing enable state for modules different from the whole PP0.
		// 		if (pp0en != moden) {
		// 		  ICommand *set_active = new SetActiveCommand(std::shared_ptr<PixCon::ConsoleEngine>(this),kModuleItem,PixA::connectivityName(module_list_iter),moden);
		// 		  m_executor->appendCommand(set_active,NULL,0);
		//  		}
		std::string mAct = PixA::connectivityName(rod_list_iter) + "/" + PixA::connectivityName(module_list_iter) + "/Active";
		bool curren= 1 ^ (moden && pp0en);
		try {
		  curren = (is_mgr->read<int>(mAct) > 0) ? 1 : 0;
		}
		catch(...) {
		}
		std::cout << "INFO [ConsoleEngine::applyDisable] " << mAct << " is " << curren << ", I want it to be " << (moden&&pp0en) << std::endl;  
		if (curren != (moden && pp0en)) {
                  module_active_list.push_back(std::make_pair(PixA::connectivityName(*module_list_iter), moden&&pp0en));
//		  ICommand *set_active = new SetActiveCommand(std::shared_ptr<PixCon::ConsoleEngine>(this),kModuleItem,PixA::connectivityName(module_list_iter),moden&&pp0en);
//		  m_executor->appendCommand(set_active,NULL,0);
		}
	      }
	    }
	  }
	}
	catch(CalibrationData::MissingConnObject &) {
	}
      }
    }

    } //CalibrationData lock
    for (std::vector<std::pair<std::string, bool> >::const_iterator name_iter = rod_active_list.begin();
         name_iter != rod_active_list.end();
	 ++name_iter) {
       ICommand *set_active = new SetActiveCommand(std::shared_ptr<PixCon::ConsoleEngine>(this),kRodItem,name_iter->first, name_iter->second);
       m_executor->appendCommand(set_active,NULL,0);
    }
    for (std::vector<std::pair<std::string, bool> >::const_iterator name_iter = module_active_list.begin();
         name_iter != module_active_list.end();
	 ++name_iter) {
       ICommand *set_active = new SetActiveCommand(std::shared_ptr<PixCon::ConsoleEngine>(this),kModuleItem,name_iter->first, name_iter->second);
       m_executor->appendCommand(set_active,NULL,0);
    }
  }


  void ConsoleEngine::installExitHandler() {

    ExitHandler::instance()->addHelper( &m_exitHelper );
    if (!ExitHandler::instance()->isInstalled()) {
      std::vector<int> signal_list;
      signal_list.push_back(SIGTERM);
      signal_list.push_back(SIGINT);
      signal_list.push_back(SIGSEGV);
      signal_list.push_back(SIGABRT);
      //    signal_list.push_back(SIGKILL);
      signal_list.push_back(SIGILL);
      signal_list.push_back(SIGQUIT);
      signal_list.push_back(SIGSYS);

      ExitHandler::instance()->installHandler(signal_list);
    }
  }


}

