#ifndef UI_CANSTATUSPANELBASE_H
#define UI_CANSTATUSPANELBASE_H

#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QComboBox>
#include <QDialog>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QSpacerItem>
#include <QVBoxLayout>
#include <QGroupBox>

QT_BEGIN_NAMESPACE

class Ui_CanStatusPanelBase
{
public:
    QVBoxLayout *vboxLayout;
    QLineEdit *m_runningEntry;
    QLineEdit *m_waitingEntry;
    QLabel *textLabel2_2;
    QLabel *textLabel2;
    QLabel *textLabel3;
    QLineEdit *m_freeEntry;
    QLabel *textLabel1;
    QHBoxLayout *hboxLayout;
    QLabel *m_canStatus;
    QLabel *textLabel1_2;
    QHBoxLayout *hboxLayout1;
    QComboBox *m_logLevelSelector;
    QSpacerItem *spacer8;
    QHBoxLayout *hboxLayout2;
    QSpacerItem *spacer3;
    QPushButton *m_resetButton;
    QSpacerItem *spacer5;
    QPushButton *m_killButton;
    QSpacerItem *spacer4;
    QHBoxLayout *level2;
    QHBoxLayout *level3;
    QHBoxLayout *level4;



    void setupUi(QDialog *CanStatusPanelBase)
    {
        if (CanStatusPanelBase->objectName().isEmpty())
            CanStatusPanelBase->setObjectName(QString::fromUtf8("CanStatusPanelBase"));
        CanStatusPanelBase->resize(384, 275);
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(1), static_cast<QSizePolicy::Policy>(5));
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(CanStatusPanelBase->sizePolicy().hasHeightForWidth());
        CanStatusPanelBase->setSizePolicy(sizePolicy);
        vboxLayout = new QVBoxLayout(CanStatusPanelBase);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));

        QGroupBox *buttonGroup1 = new QGroupBox("CAN Status",CanStatusPanelBase);
        QVBoxLayout *groupBoxButtonGroup = new QVBoxLayout;

        m_runningEntry = new QLineEdit;
        textLabel2 = new QLabel;
        textLabel2->setWordWrap(false);
        textLabel2->setBuddy(m_runningEntry);

        m_waitingEntry = new QLineEdit;
        textLabel2_2 = new QLabel;
        textLabel2_2->setWordWrap(false);
        textLabel2_2->setBuddy(m_waitingEntry);

        m_freeEntry = new QLineEdit;
        textLabel1 = new QLabel;
        textLabel1->setWordWrap(false);
        textLabel1->setBuddy(m_freeEntry);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        m_canStatus = new QLabel;
        m_canStatus->setObjectName(QString::fromUtf8("m_canStatus"));
        QSizePolicy sizePolicy1(static_cast<QSizePolicy::Policy>(0), static_cast<QSizePolicy::Policy>(5));
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(m_canStatus->sizePolicy().hasHeightForWidth());
        m_canStatus->setSizePolicy(sizePolicy1);
        m_canStatus->setFrameShape(QFrame::Box);
        m_canStatus->setWordWrap(false);
        textLabel3 = new QLabel;
        textLabel3->setWordWrap(false);
        textLabel3->setBuddy(m_canStatus);

        textLabel1_2 = new QLabel;
        textLabel1_2->setObjectName(QString::fromUtf8("textLabel1_2"));
        textLabel1_2->setWordWrap(false);
        m_logLevelSelector = new QComboBox;
        m_logLevelSelector->setObjectName(QString::fromUtf8("m_logLevelSelector"));

        groupBoxButtonGroup->addWidget(textLabel3);
        groupBoxButtonGroup->addWidget(m_canStatus);

        level2 = new QHBoxLayout;
        level2->addWidget(textLabel1);
        level2->addWidget(m_freeEntry);
        groupBoxButtonGroup->addLayout(level2);

        level3 = new QHBoxLayout;
        level3->addWidget(textLabel2_2);
        level3->addWidget(m_waitingEntry);
        groupBoxButtonGroup->addLayout(level3);

        level4 = new QHBoxLayout;
        level4->addWidget(textLabel2);
        level4->addWidget(m_runningEntry);
        groupBoxButtonGroup->addLayout(level4);

        groupBoxButtonGroup->addWidget(textLabel1_2);
        groupBoxButtonGroup->addWidget(m_logLevelSelector);

        vboxLayout->addWidget(buttonGroup1);

        hboxLayout2 = new QHBoxLayout();
        hboxLayout2->setSpacing(6);
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        spacer3 = new QSpacerItem(31, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout2->addItem(spacer3);

        m_resetButton = new QPushButton(CanStatusPanelBase);
        m_resetButton->setObjectName(QString::fromUtf8("m_resetButton"));

        hboxLayout2->addWidget(m_resetButton);

        spacer5 = new QSpacerItem(20, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        hboxLayout2->addItem(spacer5);

        m_killButton = new QPushButton(CanStatusPanelBase);
        m_killButton->setObjectName(QString::fromUtf8("m_killButton"));

        hboxLayout2->addWidget(m_killButton);

        spacer4 = new QSpacerItem(31, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout2->addItem(spacer4);

        vboxLayout->addLayout(hboxLayout2);

        groupBoxButtonGroup->addStretch(true);
        buttonGroup1->setLayout(groupBoxButtonGroup);



        retranslateUi(CanStatusPanelBase);
        QObject::connect(m_resetButton, SIGNAL(pressed()), CanStatusPanelBase, SLOT(resetCan()));
        QObject::connect(m_killButton, SIGNAL(pressed()), CanStatusPanelBase, SLOT(killCan()));
        QObject::connect(m_logLevelSelector, SIGNAL(activated(int)), CanStatusPanelBase, SLOT(changeLogLevel(int)));

        QMetaObject::connectSlotsByName(CanStatusPanelBase);
    } // setupUi

    void retranslateUi(QDialog *CanStatusPanelBase)
    {
        CanStatusPanelBase->setWindowTitle(QApplication::translate("CanStatusPanelBase", "CANStatusPanel", 0));
        textLabel2_2->setText(QApplication::translate("CanStatusPanelBase", "Waiting :", 0));
        textLabel2->setText(QApplication::translate("CanStatusPanelBase", "Running :", 0));
        textLabel3->setText(QApplication::translate("CanStatusPanelBase", "Scheduler :", 0));
        textLabel1->setText(QApplication::translate("CanStatusPanelBase", "Free :", 0));
        m_canStatus->setText(QApplication::translate("CanStatusPanelBase", "Unknown", 0));
        textLabel1_2->setText(QApplication::translate("CanStatusPanelBase", "Logging level :", 0));
        m_resetButton->setText(QApplication::translate("CanStatusPanelBase", "Reset", 0));
        m_killButton->setText(QApplication::translate("CanStatusPanelBase", "Kill", 0));
    } // retranslateUi

};

namespace Ui {
    class CanStatusPanelBase: public Ui_CanStatusPanelBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CANSTATUSPANELBASE_H
