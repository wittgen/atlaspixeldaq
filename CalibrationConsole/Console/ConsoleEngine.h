// Dear emacs, this is -*-c++-*-x
#ifndef _ConsoleEngine_H_
#define _ConsoleEngine_H_

#include <memory>
#include <set>
#include <string>
#include <map>
#include <vector>
#include <qthread.h>
#include "ConfigWrapper/Connectivity.h"
#include "PixBroker/PixBrokerMultiCrate.h"
#include "UserMode.h"

#include <CalibrationDataTypes.h>
#include <StatusChangeTransceiver.h>

#include <memory>
#include <Lock.h>

#include "IExitHelper.h"


class IPCPartition;
class ISInfoDictionary;

namespace PixLib {
  class PixBrokerMultiCrate;
  class PixActionsMulti;
  class PixDisable;
}

namespace PixA {
  class ConnectivityRef;
}

namespace PixCon{

  class CalibrationDataManager;
  class CommandExecutor;
  class IStatusMonitorActivator;

  class ConsoleEngine
  {
  public:

    template<class T> static inline void destroyer(T * /*x*/ ) {
      // the broker takes care of this
      //      if (x) {
	//	x->_destroy();
      //      }
    }

    ConsoleEngine(IPCPartition *partition,
		  const std::string &is_server_name,
		  const std::string &name_server_name,
		  const std::string &oh_server_name,
		  const std::shared_ptr<PixCon::CalibrationDataManager> &calibData,
		  const std::shared_ptr<StatusChangeTransceiver> &status_change_transceiver,
		  PixCon::UserMode::EUserMode userMode);

    ~ConsoleEngine();

    /** Get a new multi crate broker.
     * @return false in case no multi crate broker was created.
     */
    bool reconnect();

    /** Publish pixel safe module configuration with preamps off */
    bool loadPixelSafeCfg();

    /** Check if scan is safe
     */
    const bool isSafe(const std::string scanType);

    /** Get the connectivity tags from the actions and change the current connectivity
     * @return returns true if the connectivity tags have actually changed.
     */
    bool updateConnectivity();

    /** Update the connectivity and config tags of the actions servers, reading them from IS
     */
    void updateConnectivityFromIS(){m_broker->updateConnectivityFromIS("","","","");};

    /** Get the current connectivity.
     */
    PixA::ConnectivityRef getConn();

    /** Get the map of allocated RODs for all crates
     */
    std::map<std::string, std::vector<std::string> > getRodList() {return m_rodList;};

    /** Get the action list for the given crate.
     */
    std::set<std::string> getActionsList(std::string crateName);

    /** Create the name of a single ROD action.
     */
    std::string actionName(std::string rodName, std::string crateName);

    /** allocate the given ROD.s
     */
    void allocate(std::map<std::string, std::vector<std::string> > &rodList);

    /** Deallocate all allocated RODs.
     */
    void deallocate();

    //    bool getRodEnabled(std::string rodName);
    //    bool getModuleEnabled(std::string rodName, std::string moduleName);

    /** Return true if there are actions allocated.
     */
    bool haveAllocatedActions() const {
      return m_action.get();
    }

    /** Get the mutex to protect the multi action.
     */
    Mutex &multiActionMutex() { return m_actionMutex; }

    /** Get the allocated multi action.
     */
    PixLib::PixActionsMulti &multiAction() { 
      if (!m_action) {
	throw std::runtime_error("No allocated actions.");
      }
      return *m_action;
    }


    /** Get the calibration data manager.
     */
    PixCon::CalibrationDataManager &calibrationData() { return *m_calibrationData;}

    /** Get the calibration data manager (read only).
     */
    const PixCon::CalibrationDataManager &calibrationData() const { return *m_calibrationData;}

    /** Reset current action name and status in IS.
     * This method should be called before restarting an action monitor to prevent false 
     * positve results triggered by values still in IS.
     */
    void resetActionIsStatus();

    /** Reset loop counter in IS.
     * This method should be called before restarting an scan progress monitor. Otherwise
     * the monitoring may exit immediately because the old counters indicate the end of the 
     * scan.
     */
    void resetIsLoopCounter();

    /** Set the scan status of all disables RODs to aborted.
     * For the time being the enable status is not forwarded to the 
     * calibration analysis to avoid that the analysis wait for ever
     * the ROD status of disabled RODs can be set to e.g. ABORTED. 
     */
    void abortScansOfDisabledRods(SerialNumber_t scan_serial_number);

    /** Mark all RODs without enabled modules as done.
     */
    void markRodsWithoutEnabledModulesAsDone(SerialNumber_t scan_serial_number);

    /** Wait for the termination of an action sequence.
     */
    void waitForAction(bool &abort) { m_statusChangeTransceiver->waitForCurrent(abort);}

    /** Wait for the termination of the given scan.
     */
    void waitForScan(SerialNumber_t serial_number, bool &abort) { m_statusChangeTransceiver->waitFor(kScan,serial_number, abort);}

    /** Wait for the termination of an analysis.
     */
    void waitForAnalysis(SerialNumber_t serial_number, bool &abort) { m_statusChangeTransceiver->waitFor(kAnalysis,serial_number, abort);}

    /** To be called by the exit handler.
     * Will deallocate RODs, delete the actions and broker.
     */
    void emergencyDeallocate();

    /** Get pointer to the command executor.
     */
    std::shared_ptr<PixCon::CommandExecutor>  executor()       { return m_executor; }

    /** Install a new status monitor activator class.
     * The default status monitor activator does nothing (@ref NoStatusMonitorActivator)
     */
    void setStatusMonitorActivator( const std::shared_ptr<IStatusMonitorActivator>  &status_monitor_activator ) {
      m_statusMonitorActivator = status_monitor_activator;
    }

    std::shared_ptr<IStatusMonitorActivator>  statusMonitorActivator() { return m_statusMonitorActivator; }

    /** Get the status change transceiver.
     */
    std::shared_ptr<StatusChangeTransceiver> statusChangeTransceiver() { return m_statusChangeTransceiver; }


    /** Return the partition with the action server, IS and OH.
     * Presumably this partition will also be used for the calibration
     * analysis.
     */
    IPCPartition &partition()        { return *m_partition; }

    /** Name of the histo name server to be used.
     * e.g. for triggering histogram writing.
     */
    const std::string &nameServerName() const { return m_nameServerName;}

    /** Name of the histo name server to be used.
     * e.g. for triggering histogram writing.
     */
    const std::string &ohServerName() const { return m_ohServerName;}

    /** Return the name of the IS server which is used for the scan and CAN status,  and Dcs publications.
     */
    const std::string &isServerName() const { return m_isServerName; }

    /** Get the file
     */
    const std::string &resultStorePath()  const { Lock lock(m_dataMutex); return m_resultStorePath; }

    /** Get file name template
     */
    std::string resultFileTemplate() const ;

    /** Get UserMode
     */
    const UserMode::EUserMode getUserMode() const {return m_userMode;};

    /** Alter the path or file name template to be used when storing histograms.
     */
    void setResultFileTemplate(const std::string &new_result_store_path);

    /** Set temporary disable to the given object
     */
    void setTmpDis(std::shared_ptr<const PixLib::PixDisable> dis) {m_disable = dis;};
    std::shared_ptr<const PixLib::PixDisable> currentDisable();

    /** Apply disable, propagating it to the actions
     */
    void applyDisable();

  private:
    void _getActionsList(std::string crateName, std::set<std::string> &actionsSet);

    IPCPartition *m_partition;
    std::string                      m_isServerName;
    std::string                      m_nameServerName;
    std::string                      m_ohServerName;
    std::string                      m_resultStorePath;
    std::map<std::string, std::vector<std::string> > m_rodList;

    std::unique_ptr<ISInfoDictionary>  m_isDictionary;
    std::shared_ptr<PixCon::CalibrationDataManager> m_calibrationData;

    std::shared_ptr<PixLib::PixBrokerMultiCrate>    m_broker;
    std::shared_ptr<PixLib::PixActionsMulti>        m_action;
    std::shared_ptr<const PixLib::PixDisable>       m_disable;


    Mutex                                  m_actionMutex;
    mutable Mutex                          m_dataMutex;

    std::shared_ptr<StatusChangeTransceiver> m_statusChangeTransceiver;

    UserMode::EUserMode                             m_userMode;

    //    PixA::ConnectivityRef m_conn;

    std::string makeIsName(const std::string &crate_name, const std::string &rod_name, const std::string &var_name) {
      return m_isServerName + "." + crate_name + "/" + rod_name + "/" + var_name;
    }

    std::string makeIsName(SerialNumber_t scan_serial_number,
                           const std::string &crate_name,
                           const std::string &rod_name,
                           const std::string &var_name); 

    std::string makeIsPattern(const std::string &var_name) {
      std::string temp(".*/");
      temp +=  "/";
      temp += var_name;
      return temp;
    }

    void installExitHandler();

    class DeallocateActions : public IExitHelper
    {
    public:
      DeallocateActions( ConsoleEngine *engine) :m_engine(engine) {}

      void freeResources() {
	m_engine->emergencyDeallocate();
      }

    private:
      ConsoleEngine *m_engine;
    };

    DeallocateActions m_exitHelper;

    static const std::string s_currentActionIsName;
    static const std::string s_currentActionStatusIsName;
    static const std::string s_scanStatusIsName;
    static const char *s_rodCounterVarNames[4];

    std::shared_ptr<IStatusMonitorActivator>  m_statusMonitorActivator;
    std::shared_ptr<PixCon::CommandExecutor>  m_executor;
    //int m_next;

  };
}

#endif
