/* Dear emacs, this is -*-c++-*- */
#ifndef _PixCon_StatusMonitorActivator_h_
#define _PixCon_StatusMonitorActivator_h_

#include "IStatusMonitorActivator.h"

#include <CalibrationDataTypes.h>
#include <string>
#include <vector>
#include <stdexcept>

#include <StatusMonitor.h>
#include <ActionStatusMonitor.h>
#include <ScanProgressMonitor.h>

namespace PixCon {

  /** Helper class to activate or abort status monitoring
   */
  class StatusMonitorActivator : public IStatusMonitorActivator
  {
  public:
    /** Create a helper class to activate the analysis, scan and action status monitoring.
     * @param scan_status_monitor a shared pointer to an object of type @ref StatusMonitor (not just @ref IStatusMonitor) which monitors scans.
     * @param analysis_status_monitor a shared pointer to an object of type @ref StatusMonitor (not just @ref IStatusMonitor) which monitors analyses.
     * @param action_status_monitor a shared pointer to an object of type @ref ActionStatusMonitor (not just @ref IStatusMonitor).
     * action_status_monitor monitors the progress of an action sequence. 
     */
    StatusMonitorActivator(const std::shared_ptr<IStatusMonitor> &scan_status_monitor,
			   const std::shared_ptr<IStatusMonitor> &analysis_status_monitor,
			   const std::shared_ptr<IStatusMonitor> &action_status_monitor,
			   const std::shared_ptr<IStatusMonitor> &scan_progress_monitor)
      : m_scanStatusMonitor(scan_status_monitor),
	m_analysisStatusMonitor(analysis_status_monitor),
	m_actionStatusMonitor(action_status_monitor),
	m_scanProgressMonitor(scan_progress_monitor)
    {
      assert( dynamic_cast<StatusMonitor *>(scan_status_monitor.get()) );
      assert( dynamic_cast<StatusMonitor *>(analysis_status_monitor.get()) );
      assert( dynamic_cast<ActionStatusMonitor *>(action_status_monitor.get()) );
      assert( dynamic_cast<ScanProgressMonitor *>(scan_progress_monitor.get()) );
    }

    /** Activate the monitoring of a scan.
     * @param scan_serial_number the serial number of the scan to be monitored.
     * @param start_value an array of the 4 initial counter values
     * @param max_value the number of steps for each of the 4 counter.
     * @param max_step_time the maximum time counters may staty unchanged.
     * @return true if the monitoring was activated successfully, false in case the monitoring was not activated.
     */
    bool activateScanMonitoring(SerialNumber_t scan_serial_number,
				unsigned int mask_stage_loop_index,
				const std::vector<unsigned short> &start_value,
				const std::vector<unsigned short> &max_value,
				unsigned long max_step_time);

    /** Abort the monitoring of the given scan.
     * @param scan_serial_number the serial number of the scan for which the monitoring should be aborted.
     */
    void abortScanMonitoring(SerialNumber_t scan_serial_number) {
      scanStatusMonitor().abortMonitoring(scan_serial_number);
      scanProgressMonitor().initiateMonitoringStop();
    }

    /** Activate the monitoring of an analysis.
     * @param analysis_serial_number the serial number of the analysis to be monitored.
     * @return true if the monitoring was activated successfully, false in case the monitoring was not activated.
     */
    bool activateAnalysisMonitoring(SerialNumber_t analysis_serial_number);

    /** Abort the monitoring of the given analysis.
     * @param analysis_serial_number the serial number of the analysis for which the monitoring should be aborted.
     */
    void abortAnalysisMonitoring(SerialNumber_t analysis_serial_number) {
      analysisStatusMonitor().abortMonitoring(analysis_serial_number);
    }

    /** Activate the monitoring of the given action sequence.
     * @param action_sequence the expected sequence of actions.
     * @return true if the monitoring was activated successfully, false in case the monitoring was not activated.
     */
    bool activateActionMonitoring(const std::vector<std::string> &action_sequence) {
      try {
	if (actionStatusMonitor().isMonitoring()) return false;
	return actionStatusMonitor().monitorAction(action_sequence);
      }
      catch (std::runtime_error &) {
	// action monitoring alread running
	return false;
      }
    }

    /** Abort the monitoring of the current action sequence.
     */
    void abortActionMonitoring() {
      actionStatusMonitor().abortMonitoring();
    }
  private:

    StatusMonitor       &scanStatusMonitor()     { return static_cast<StatusMonitor &>(*m_scanStatusMonitor); }
    StatusMonitor       &analysisStatusMonitor() { return static_cast<StatusMonitor &>(*m_analysisStatusMonitor); }
    ActionStatusMonitor &actionStatusMonitor()   { return static_cast<ActionStatusMonitor &>(*m_actionStatusMonitor); }
    ScanProgressMonitor &scanProgressMonitor()   { return static_cast<ScanProgressMonitor &>(*m_scanProgressMonitor); }

    std::shared_ptr<IStatusMonitor>     m_scanStatusMonitor;
    std::shared_ptr<IStatusMonitor>     m_analysisStatusMonitor;
    std::shared_ptr<IStatusMonitor>     m_actionStatusMonitor;
    std::shared_ptr<IStatusMonitor>     m_scanProgressMonitor;
  };

}
#endif
