#include <ICommand.h>
#include <memory>
#include "ActionCommandBase.h"
#include <PixActions/PixActionsMulti.h>


namespace PixCon {

  class ResetModulesCommand : public ActionCommandBase
  {
  public:
    ResetModulesCommand(const std::shared_ptr<ConsoleEngine> &engine)
      : ActionCommandBase(engine)
    {}

    const std::string &name() const { return commandName(); }

    static const std::string &commandName() {
      return s_name;
    }

  protected:

    void setActionSequence(std::vector<std::string> &action_sequence) {
      action_sequence.push_back("resetmods");
    }

    bool callAction(PixLib::PixActionsMulti &action) {
      action.resetmods();
      return true;
    }

  private:

    //bool m_uninitialisedRodsOnly;
    static const std::string s_name;
  };

  const std::string ResetModulesCommand::s_name("Reset Modules");


  class ResetModulesKit : public PixCon::ICommandKit
  {
  protected:
    ResetModulesKit()
    {}

  public:
    PixCon::IConfigurableCommand *createCommand(const std::string &/*command*/, const std::shared_ptr<ConsoleEngine> &engine) const {
      assert(engine.get());
      return new ResetModulesCommand(engine);
    }

    static bool registerKit() {
      CommandKits::registerKit(ResetModulesCommand::commandName(),new ResetModulesKit, PixCon::UserMode::kExpert);
      //      CommandKits::registerKit(ResetModulesCommand::name(true),new ResetModulesKit(true));
      return true;
    }

  private:
    static bool s_registered;
  };

  bool ResetModulesKit::s_registered = ResetModulesKit::registerKit();

}
