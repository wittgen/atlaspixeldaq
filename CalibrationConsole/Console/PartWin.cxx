#include "PartWin.h"
#include <ipc/partition.h>
#include <is/serveriterator.h>

namespace PixCon
{

  PartWin::PartWin(QApplication &/*app*/, QWidget* parent)
: QDialog(parent)
  {
    setupUi(this);
    for (unsigned int server_i=0; server_i<=kNServerTypes; server_i++) {
      m_isSelection[server_i]=false;
    }
    IPCPartition::getPartitions ( m_partList );
    for(std::list< IPCPartition >::iterator it = m_partList.begin(); it!=m_partList.end(); it++)
      partBox->addItem((*it).name().c_str());
    if(partBox->count()>0) partSelected(0);
  }

  PartWin::~PartWin() {}

  void PartWin::partSelected(int iPart)
  {
    std::list< IPCPartition >::iterator it = m_partList.begin();
    QComboBox *server_box[2];
    server_box[kISServer]=issrvBox;
    server_box[kOHServer]=ohsrvBox;

    std::string last_server_name[2];
    for (unsigned int server_i=0; server_i<2; server_i++) {
      if (server_box[server_i]->currentText().length()>0) {
	last_server_name[server_i] = server_box[server_i]->currentText().toLatin1().data();
      }
      else {
	last_server_name[server_i] = "";
      }
      server_box[server_i]->clear();
      m_isSelection[server_i]=false;
    }
    for(int i=0;i<iPart && it!=m_partList.end();i++)
      it++;

    if (it == m_partList.end()) return;


    int current_selection[2]={-1,-1};
    bool is_default[2]={false,false};

    ISServerIterator srvit(*it);
    while(srvit()){
      for (unsigned int server_i=0; server_i<2; server_i++) {
	server_box[server_i]->addItem(srvit.name());

	if (   (!last_server_name[server_i].empty() && last_server_name[server_i]==srvit.name())) {
	  current_selection[server_i]=server_box[server_i]->count()-1;
	}
	else if (   (current_selection[server_i]<0 || is_default[server_i]==true) 
		 && !m_choice[kSelection][server_i].empty()  
		 && m_choice[kSelection][server_i] == srvit.name()) {
	  current_selection[server_i]=server_box[server_i]->count()-1;
	  is_default[server_i]=false;
	  m_isSelection[server_i]=true;

	}
	else if (current_selection[server_i]<0
		 && !m_choice[kDefault][server_i].empty()
		 && m_choice[kDefault][server_i] == srvit.name()) {
	  current_selection[server_i]=server_box[server_i]->count()-1;
	  is_default[server_i]=true;

	}
      }
    }

    for (unsigned int server_i=0; server_i<2; server_i++) {
      if (current_selection[server_i]>=0) {
	server_box[server_i]->setCurrentIndex(current_selection[server_i]);
      }
    }
  }

  void PartWin::setPartitionName(const std::string &partition_name_arg, const std::string &default_partition_name)
  {
    std::string selected_partition=partition_name_arg;
    if (partition_name_arg.empty()) {
      const char *user=getenv("USER");
      if (user) {
	for (int part_item=0; part_item < partBox->count(); part_item++) {
	  std::string partition_name = partBox->itemText(part_item).toUtf8().constData();
	  if (partition_name.find(user) != std::string::npos) {
	    selected_partition=partition_name;
	    break;
	  }
	}
      }
    }
     m_choice[kSelection][kNServerTypes]=selected_partition;
    std::pair<int,bool> entry_i =setComboBox(partBox, selected_partition, default_partition_name);
    m_isSelection[kNServerTypes] =entry_i.second;
    if (entry_i.first < partBox->count()) {
      partSelected(entry_i.first);
    }
  }

  void PartWin::setServerNames(EServerTypes type, const std::string &selection, const std::string &default_name) {
    assert(type < kNServerTypes);
    m_choice[kSelection][type]=selection;
    m_choice[kDefault][type]=default_name;
    QComboBox *server_box[2];
    server_box[kISServer]=issrvBox;
    server_box[kOHServer]=ohsrvBox;
    std::pair<int,bool> entry_i =setComboBox(server_box[type], selection, default_name);
    m_isSelection[type]=entry_i.second;
    if (entry_i.first < server_box[type]->count()) {
      server_box[type]->setCurrentIndex(entry_i.first);
    }
  }

  int PartWin::find(QComboBox *combo_box, const std::string &text) {
    if (!text.empty()) {
      QString temp(QString::fromStdString(text));
      for (int entry_i=0; entry_i < combo_box->count(); entry_i++) {
	if (combo_box->itemText(entry_i).compare(temp)==0) {
	  return entry_i;
	}
      }
    }
    return combo_box->count();
  }

  std::pair<int,bool> PartWin::setComboBox(QComboBox *combo_box, const std::string &selection, const std::string &default_value)
  {
    std::pair<int,bool> entry_i;
    entry_i.first = find(combo_box, selection);

    if (entry_i.first >= combo_box->count()) {
      entry_i.first = find(combo_box, default_value);
      entry_i.second=false;
    }
    else {
      entry_i.second = true;
    }

    if (entry_i.first<combo_box->count()) {
      combo_box->setCurrentIndex(entry_i.first);
    }
    return entry_i;
  }


}
