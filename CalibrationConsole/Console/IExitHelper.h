#ifndef _PixCon_IExitHelper_h_
#define _PixCon_IExitHelper_h_

namespace PixCon {

  class IExitHelper
  {
  public:
    virtual ~IExitHelper() {}
    virtual void freeResources() = 0;
  };

  class AutoExitHelper  : public IExitHelper
  {
  public:
    ~AutoExitHelper();

  };

}
#endif
