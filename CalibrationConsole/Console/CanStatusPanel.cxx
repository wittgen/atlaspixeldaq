#include "CanStatusPanel.h"
#include <QThread>

#include <Flag.h>

#include <exception.hh>

#include <ipc/partition.h>
#include <Scheduler_ref.hh>
#include <LogLevel.hh>

#include <CalibrationDataStyle.h>
#include "DefaultColourCode.h"

#include <BusyLoop.h>



namespace PixCon {

  const char *CanStatusPanel::SchedulerStatusEvent::s_statusName[SchedulerStatusEvent::kNStati] = {
    "Unknown",
    "Ok",
    "CommError",
    "InternalError",
    "NoWorker",
    "NoScheduler"
  };
  std::vector<unsigned int> CanStatusPanel::s_colorMap;

    class IVoidFunction {
    public:
      virtual ~IVoidFunction() {}
      virtual CanStatusPanel::SchedulerStatusEvent::ESchedulerStatus operator()(CAN::Scheduler_var a) const = 0;
    };

    class ZeroArgFunction : public IVoidFunction {
    public:
      typedef void (CAN::_objref_Scheduler::*VoidFunction_t )();

      ZeroArgFunction(VoidFunction_t func, CanStatusPanel::SchedulerStatusEvent::ESchedulerStatus final_status) : m_func(func), m_status(final_status) {}

      CanStatusPanel::SchedulerStatusEvent::ESchedulerStatus operator()(CAN::Scheduler_var a) const
      {
	  ((*a).*m_func)();
	  return m_status;
      }

    private:
      VoidFunction_t m_func;
      CanStatusPanel::SchedulerStatusEvent::ESchedulerStatus m_status;
    };

    class ZeroArgBaseFunction : public IVoidFunction {
    public:
      typedef void (ipc::_objref_servant::*VoidFunction_t )();

      ZeroArgBaseFunction(VoidFunction_t func, CanStatusPanel::SchedulerStatusEvent::ESchedulerStatus final_status) : m_func(func), m_status(final_status) {}

      CanStatusPanel::SchedulerStatusEvent::ESchedulerStatus operator()(CAN::Scheduler_var a) const
      {
	  ((*a).*m_func)();
	  return m_status;
      }

    private:
      VoidFunction_t m_func;
      CanStatusPanel::SchedulerStatusEvent::ESchedulerStatus m_status;
    };

    template <class T1, class T2, class T3>
    class ThreeArgFunction : public IVoidFunction {
    public:
      typedef void (CAN::_objref_Scheduler::*VoidFunction_t )(T1 a1, T2 a2, T3 a3);
      ThreeArgFunction(VoidFunction_t func, T1 a1, T2 a2, T3 a3, CanStatusPanel::SchedulerStatusEvent::ESchedulerStatus final_status) 
	: m_func(func),
	  m_a1(a1),
	  m_a2(a2),
	  m_a3(a3),
	  m_status(final_status)
      {}

      CanStatusPanel::SchedulerStatusEvent::ESchedulerStatus operator()(CAN::Scheduler_var a) const
	{ 
	  ((*a).*m_func)(m_a1, m_a2, m_a3); 
	  return m_status;
	}
    private:
      VoidFunction_t m_func;

      T1 m_a1;
      T2 m_a2;
      T3 m_a3;

      CanStatusPanel::SchedulerStatusEvent::ESchedulerStatus m_status;
    };

    template <class T1>
    class OneArgFunction : public IVoidFunction {
    public:
      typedef void (CAN::_objref_Scheduler::*VoidFunction_t )(T1 a1);
      OneArgFunction(VoidFunction_t func, T1 a1, CanStatusPanel::SchedulerStatusEvent::ESchedulerStatus final_status) 
	: m_func(func),
	  m_a1(a1),
	  m_status(final_status)
      {}

      CanStatusPanel::SchedulerStatusEvent::ESchedulerStatus operator()(CAN::Scheduler_var a) const
	{ 
	  ((*a).*m_func)(m_a1); 
	  return m_status;
	}
    private:
      VoidFunction_t m_func;

      T1 m_a1;
      CanStatusPanel::SchedulerStatusEvent::ESchedulerStatus m_status;
    };


  class Manitu : public CanStatusPanel::IManitu, public QThread
  {
  protected:
    class ShutdownFunction;
    friend class ShutdownFunction;

  public:
    Manitu(QApplication &application, QObject &receiver, const std::string &partition_name, bool verbose)
      : m_app(&application),
	m_receiver(&receiver),
	m_partitionName(partition_name),
	m_pollTime(30),
	m_jobsRunning(static_cast<unsigned int>(-1)),
	m_jobsWaiting(static_cast<unsigned int>(-1)),
	m_emptySlots(static_cast<unsigned int>(-1)),
	m_status(CanStatusPanel::SchedulerStatusEvent::kUnknown),
	m_shutdown(false),
	m_verbose(verbose)
    {
      sendSchedulerStatus(CanStatusPanel::SchedulerStatusEvent::kNoScheduler);
      start();
    }

    ~Manitu() {
      waitShutdownMonitoring();
    }

    void saveCall(const std::string &function_name, const IVoidFunction &func) {
      Lock lock(m_manituLock);
      CanStatusPanel::SchedulerStatusEvent::ESchedulerStatus status = CanStatusPanel::SchedulerStatusEvent::kUnknown;
      if (!CORBA::is_nil (m_manituPtr) ) {
	bool scheduler_comm_error=false;
	try {
	  status = func(m_manituPtr);
	}
	catch (CAN::MRSException &err) {
	  std::cerr << "ERROR [Manitu::" << function_name << "] Caught exception " << err.getDescriptor() << std::endl;
	  scheduler_comm_error=true;
	  status=CanStatusPanel::SchedulerStatusEvent::kInternalError;
	}
	catch (CORBA::TRANSIENT& err) {
	  scheduler_comm_error=true;
	  status=CanStatusPanel::SchedulerStatusEvent::kCommError;
	}
	catch (CORBA::COMM_FAILURE &err) {
	  scheduler_comm_error=true;
	  status=CanStatusPanel::SchedulerStatusEvent::kCommError;
	}
	catch (std::exception &err) {
	  std::cerr << "ERROR [Manitu::"<< function_name << "] Caught std exception " << err.what() << std::endl;
	  status=CanStatusPanel::SchedulerStatusEvent::kInternalError;
	}
	catch (...) {
	  std::cerr << "ERROR [Manitu::"<< function_name << "] Caught unknown exception " << std::endl;
	  status=CanStatusPanel::SchedulerStatusEvent::kInternalError;
	}
	if (scheduler_comm_error && (m_verbose || m_status != status) ) {
	  std::cerr << "ERROR [Manitu::"<< function_name << "] CORBA communication error. Scheduler down ? Partition down ? Network problems ? " << std::endl;
	}
      }
      else {
	status=CanStatusPanel::SchedulerStatusEvent::kNoScheduler;
	if (m_verbose || m_status != status) {
	  std::cerr << "ERROR [Manitu::"<< function_name << "] No scheduler (Failure at startup time)." << std::endl;
	}
      }
      sendSchedulerStatus(status);

    }

    void connect() {
      Lock lock(m_manituLock);
      bool scheduler_comm_error=false;
      CanStatusPanel::SchedulerStatusEvent::ESchedulerStatus status = CanStatusPanel::SchedulerStatusEvent::kUnknown;
      try {

	IPCPartition partition(m_partitionName.c_str());

	if (!partition.isValid()) {
	  if (m_verbose) {
	    std::cerr << "ERROR [Manitu::ctor] invalid partition " << m_partitionName << std::endl;
	  }
	  status = CanStatusPanel::SchedulerStatusEvent::kNoScheduler;
	}
	else {
	  if (m_verbose) {
	    std::cerr << "INFO [Manitu::connect] Try to connect to manitu in  " << m_partitionName << std::endl;
	  }
	  std::string manitu_name = (CAN::s_schedulerName);

	  if (partition.isObjectValid<CAN::Scheduler>(manitu_name)) {
	    m_manituPtr = partition.lookup<CAN::Scheduler>((manitu_name));
	    if (m_shutdown) return;
	    if (!CORBA::is_nil (m_manituPtr) ) {
	      m_manituPtr->alive();
	      status = CanStatusPanel::SchedulerStatusEvent::kOk;
	    }
	  }
	  else {
	    status = CanStatusPanel::SchedulerStatusEvent::kNoScheduler;
	  }
	}
      }
      catch (CAN::MRSException &err) {
	if (m_verbose) {
	  std::cerr << "ERROR [Manitu::ctor] Caught exception " << err.getDescriptor() << std::endl;
	}
	scheduler_comm_error=true;
	status = CanStatusPanel::SchedulerStatusEvent::kInternalError;
      }
      catch (CORBA::TRANSIENT& err) {
	scheduler_comm_error=true;
	status = CanStatusPanel::SchedulerStatusEvent::kCommError;
      }
      catch (CORBA::COMM_FAILURE &err) {
	scheduler_comm_error=true;
	status = CanStatusPanel::SchedulerStatusEvent::kCommError;
      }
      catch (std::exception &err) {
	if (m_verbose) {
	  std::cerr << "ERROR [Manitu::ctor] Caught std exception " << err.what() << std::endl;
	}
	status = CanStatusPanel::SchedulerStatusEvent::kInternalError;
      }
      catch (...) {
	if (m_verbose) {
	  std::cerr << "ERROR [Manitu::ctor] Caught unknown exception " << std::endl;
	}
	status = CanStatusPanel::SchedulerStatusEvent::kInternalError;
      }
      if (scheduler_comm_error) {
	if (m_verbose || m_status != status) {
	  std::cerr << "ERROR [Manitu::ctor] CORBA communication error. Scheduler down ? Partition down ? Network problems ? " << std::endl;
	}
	status = CanStatusPanel::SchedulerStatusEvent::kNoScheduler;
      }
      sendSchedulerStatus(status);

    }

    void resetQueue() {
      saveCall("resetQueue",ZeroArgFunction(&CAN::_objref_Scheduler::reset, CanStatusPanel::SchedulerStatusEvent::kOk));
    }

    void restartWorker() {
      saveCall("restartWorker",ZeroArgFunction(&CAN::_objref_Scheduler::restartWorker, CanStatusPanel::SchedulerStatusEvent::kOk));
    }

    void setLogLevel(int log_level) {
      saveCall("setLogLevel",OneArgFunction<unsigned short>(&CAN::_objref_Scheduler::setLogLevel,
							    static_cast<CORBA::UShort>(log_level),
							    CanStatusPanel::SchedulerStatusEvent::kOk));
    }

  protected:

    class ShutdownFunction : public IVoidFunction {
    public:
      ShutdownFunction(Manitu &manitu) : m_manitu(&manitu) {}

      CanStatusPanel::SchedulerStatusEvent::ESchedulerStatus operator()(CAN::Scheduler_var /*a*/) const
      {

	m_manitu->shutdownManitu();
	return CanStatusPanel::SchedulerStatusEvent::kNoScheduler;
      }

    private:
      Manitu *m_manitu;
    };

  public:

    void kill() {
      saveCall("kill",ShutdownFunction(*this));
    }

  protected:
    class UpdateQueueStatusFunction : public IVoidFunction {
    public:
      UpdateQueueStatusFunction(Manitu &manitu) : m_manitu(&manitu) {}

      CanStatusPanel::SchedulerStatusEvent::ESchedulerStatus operator()(CAN::Scheduler_var a) const
      {

	  CORBA::ULong jobs_waiting_for_data;
	  CORBA::ULong jobs_waiting;
	  CORBA::ULong jobs_running;
	  CORBA::ULong empty_slots;
	  a->getQueueStatus(jobs_waiting_for_data, jobs_waiting, jobs_running, empty_slots);

	  m_manitu->sendQueueStatus(static_cast<unsigned int>(jobs_waiting+ jobs_waiting_for_data),
				    static_cast<unsigned int>(jobs_running),
				    static_cast<unsigned int>(empty_slots));
	  if (empty_slots+jobs_running==0) {
	    return CanStatusPanel::SchedulerStatusEvent::kNoWorker;
	  }
	  else {
	    return CanStatusPanel::SchedulerStatusEvent::kOk;
	  }
      }

    private:
      Manitu *m_manitu;
      //CanStatusPanel::SchedulerStatusEvent::ESchedulerStatus m_status;
    };

  public:
    void updateQueueStatus() {

      //      CanStatusPanel::SchedulerStatusEvent::ESchedulerStatus status = CanStatusPanel::SchedulerStatusEvent::kUnknown;
      if (m_status==CanStatusPanel::SchedulerStatusEvent::kNoScheduler
	  || m_status==CanStatusPanel::SchedulerStatusEvent::kCommError
	  || CORBA::is_nil (m_manituPtr)) {
	connect();
	sendQueueStatus(0,0,0);
      }

      CanStatusPanel::SchedulerStatusEvent::ESchedulerStatus old_status = m_status;
      if (m_shutdown) return;
      saveCall("updateQueueStatus",UpdateQueueStatusFunction(*this));

      if (m_status==CanStatusPanel::SchedulerStatusEvent::kNoScheduler
	  || m_status==CanStatusPanel::SchedulerStatusEvent::kCommError
	  || CORBA::is_nil (m_manituPtr)) {
	if (m_status != old_status) {
	  sendQueueStatus(0,0,0);
	}
      }
    }

    void sendQueueStatus(unsigned int jobs_waiting, unsigned int jobs_running, unsigned int empty_slots) 
    {
      if (m_jobsWaiting != jobs_waiting
	  || m_jobsRunning != jobs_running
	  || m_emptySlots != empty_slots) {

	m_jobsWaiting = jobs_waiting;
	m_jobsRunning = jobs_running;
	m_emptySlots = empty_slots;

	m_app->postEvent(m_receiver, new CanStatusPanel::QueueStatusEvent(m_jobsWaiting, m_jobsRunning, m_emptySlots) );
      }
    }

    void sendSchedulerStatus(CanStatusPanel::SchedulerStatusEvent::ESchedulerStatus status) {
      if (status != m_status) {
	if (m_status==CanStatusPanel::SchedulerStatusEvent::kOk) {
	  if (!CORBA::is_nil (m_manituPtr) || status != CanStatusPanel::SchedulerStatusEvent::kNoScheduler) {
	    std::cerr << "ERROR [Manitu::sendSchedulerStatus] Status of Calibration Analysis infrastructure changed to "
		      << CanStatusPanel::SchedulerStatusEvent::statusName(status) << std::endl;
	  }
	}
	else if (status==CanStatusPanel::SchedulerStatusEvent::kOk) {
	  std::cerr << "DEBUG [Manitu::sendSchedulerStatus] Status of Calibration Analysis infrastructure is now "
		    << CanStatusPanel::SchedulerStatusEvent::statusName(status) << "." << std::endl;
	}
	m_status = status;
	m_app->postEvent(m_receiver, new CanStatusPanel::SchedulerStatusEvent(m_status) );
      }
    }

    void shutdownMonitoring() {
      m_shutdown = true;
      m_monitor.setFlag();
    }

    void waitShutdownMonitoring()  {
      shutdownMonitoring();
      if (isRunning()) {
	std::cout << "INFO [Manitu::waitShutdownMonitoring]" << std::endl;
	m_exit.wait();
	std::cout << "INFO [Manitu::waitShutdownMonitoring] wait while thread is still running." << std::endl;
	struct timespec wait_time;
	wait_time.tv_sec = 0;
	wait_time.tv_nsec = 100*1000000;
	while (isRunning()) {
	  nanosleep(&wait_time, NULL);
	}
      }
    }

    void run()
    {
      monitoring();
      m_exit.setFlag();
      std::cout << "INFO [Manitu::run] exit" << std::endl;
    }

    void monitoring()
    {
      std::cout << "INFO [Manitu::monitoring] Started run loop" << std::endl;
      for(;;) {

	updateQueueStatus();
	if (m_shutdown) break;
	m_monitor.timedwait( m_pollTime, m_shutdown);
	if (m_shutdown) break;
      }
      std::cout << "INFO [Manitu::monitoring] Terminated run loop" << std::endl;
    }


  protected:
    void shutdownManitu() {
      m_manituPtr->shutdown();

      CAN::Scheduler_var invalid;
      m_manituPtr=invalid;
      if (m_status == CanStatusPanel::SchedulerStatusEvent::kOk) {
	std::cout << "INFO [Manitu::shutdownManitu] Calibration Analysis infrastructure was shutdown." << std::endl;
      }
    }

  private:
    QApplication *m_app;
    QObject      *m_receiver;
    std::string   m_partitionName;
    unsigned int  m_pollTime;

    unsigned int m_jobsRunning;
    unsigned int m_jobsWaiting;
    unsigned int m_emptySlots;
    CanStatusPanel::SchedulerStatusEvent::ESchedulerStatus m_status;

    CAN::Scheduler_var m_manituPtr;
    Mutex              m_manituLock;

    Flag m_exit;
    Flag m_monitor;
    bool m_shutdown;
    bool m_verbose;
  };

  CanStatusPanel::CanStatusPanel( QApplication &application,
				  const std::string &partition_name,
				  const std::shared_ptr<PixCon::CalibrationDataPalette> &palette,
				  QWidget* parent)
: QDialog(parent),
      m_palette(palette),
      m_app(&application),
      m_manitu(new Manitu(application, *this, partition_name,false)),
      m_jobsRunning(static_cast<unsigned int>(-1)),
      m_jobsWaiting(static_cast<unsigned int>(-1)),
      m_emptySlots(static_cast<unsigned int>(-1)),
      m_status(SchedulerStatusEvent::kUnknown),
      m_lastLogLevel(CAN::kAllDiagnosticsLog+2)
  {
    setupUi(this);
    if (s_colorMap.size()!=SchedulerStatusEvent::kNStati) {
      s_colorMap.resize(SchedulerStatusEvent::kNStati);
      s_colorMap[SchedulerStatusEvent::kOk]= static_cast<unsigned int>(DefaultColourCode::kOk);
      s_colorMap[SchedulerStatusEvent::kNoScheduler]= static_cast<unsigned int>(DefaultColourCode::kFailed);
      s_colorMap[SchedulerStatusEvent::kCommError]= static_cast<unsigned int>(DefaultColourCode::kFailed);
      s_colorMap[SchedulerStatusEvent::kInternalError]= static_cast<unsigned int>(DefaultColourCode::kFailed);
      s_colorMap[SchedulerStatusEvent::kNoWorker]= static_cast<unsigned int>(DefaultColourCode::kFailed);
      s_colorMap[SchedulerStatusEvent::kUnknown]= static_cast<unsigned int>(DefaultColourCode::kUnknown);
    }

    m_logLevelSelector->addItem("Default",QVariant(0));
    m_logLevelSelector->addItem("Quiet",QVariant(CAN::kSilentLog+1));
    m_logLevelSelector->addItem("Fatal",QVariant(CAN::kFatalLog+1));
    m_logLevelSelector->addItem("Error",QVariant(CAN::kErrorLog+1));
    m_logLevelSelector->addItem("Warning",QVariant(CAN::kWarnLog+1));
    m_logLevelSelector->addItem("Slow Information",QVariant(CAN::kSlowInfoLog+1));
    m_logLevelSelector->addItem("Information",QVariant(CAN::kInfoLog+1));
    m_logLevelSelector->addItem("Diagnostics 0",QVariant(CAN::kDiagnosticsLog+1));
    m_logLevelSelector->addItem("Diagnostics 1",QVariant(CAN::kDiagnostics1Log+1));
    m_logLevelSelector->addItem("Diagnostics 2",QVariant(CAN::kDiagnostics2Log+1));
    m_logLevelSelector->addItem("Diagnostics 3",QVariant(CAN::kDiagnostics3Log+1));
    m_logLevelSelector->addItem("Diagnostics 4",QVariant(CAN::kDiagnostics4Log+1));
    m_logLevelSelector->addItem("Diagnostics 5",QVariant(CAN::kDiagnostics5Log+1));
    m_logLevelSelector->addItem("Diagnostics All",QVariant(CAN::kAllDiagnosticsLog+1));
  }

  CanStatusPanel::~CanStatusPanel()
  {
    shutdown();
  }

  void CanStatusPanel::resetCan()
  {
    m_killButton->setEnabled(false);
    m_resetButton->setEnabled(false);
    {
      //      QMessageBox  *message = new QMessageBox("Reset  analysis queues.","Reset Analysis qeueus.", QMessageBox::Information, QMessageBox::Ok,0,0,this);

      emit statusMessage("Reset Analysis queues ... ");      // must be emitted before the busy thread is started
      BusyLoop busy(m_app, NULL /*message*/ ,UINT_MAX,100 );
      m_manitu->resetQueue();
    }
    m_killButton->setEnabled(true);
    m_resetButton->setEnabled(true);
  }

  void CanStatusPanel::changeLogLevel(int index) {
    QVariant var = m_logLevelSelector->itemData(index);
    if(var!=QVariant::Invalid){
      int new_log_level = var.toInt();
      if (new_log_level != m_lastLogLevel && new_log_level>=1 && new_log_level<=CAN::kAllDiagnosticsLog+1) {
	BusyLoop busy(m_app, NULL /*message*/ ,UINT_MAX,100 );
	m_manitu->setLogLevel(new_log_level-1);
	m_lastLogLevel=new_log_level;
      }
    }
  }


  void CanStatusPanel::killCan()
  {
    m_killButton->setEnabled(false);
    m_resetButton->setEnabled(false);
    {
      //      QMessageBox  *message = new QMessageBox("Shutdown analysis infrastrucutre.","Shutdown analysis infrastrucutre.", QMessageBox::Information, QMessageBox::Ok,0,0,this);
      emit statusMessage("Shutting down analysis infrastrucutre ... "); // must be emitted before the busy thread is started
      BusyLoop busy(m_app, NULL /*message*/ ,UINT_MAX,100 );
      m_manitu->kill();
    }
    m_killButton->setEnabled(true);
    m_resetButton->setEnabled(true);
  }

  bool CanStatusPanel::event(QEvent *an_event)
  {
    // @todo event processing should be done by the "parent" of all detector
    //       views and all views which show the right thing should be updated

    if (an_event && an_event->type() == QEvent::User) {
      PixCon::CanStatusPanel::QueueStatusEvent * queue_status_event  = dynamic_cast< PixCon::CanStatusPanel::QueueStatusEvent *>(an_event);
      if (queue_status_event) {
	updateQueueStatus( queue_status_event->waitingJobs(), queue_status_event->runningJobs(), queue_status_event->emptySlots());
	return true;
      }
      PixCon::CanStatusPanel::SchedulerStatusEvent * scheduler_status_event  = dynamic_cast< PixCon::CanStatusPanel::SchedulerStatusEvent *>(an_event);
      if (scheduler_status_event) {
	updateSchedulerStatus(scheduler_status_event->status());
	return true;
      }
    }
    return QDialog::event(an_event);
  }

  void CanStatusPanel::updateQueueStatus(unsigned int jobs_waiting, unsigned int jobs_running, unsigned int empty_slots)
  {
    unsigned int values[3];
    unsigned int *ref_values[3];
    QLineEdit *line_edits[3];
    line_edits[0]=m_runningEntry;
    values[0]=jobs_running;
    ref_values[0]=&m_jobsRunning;

    line_edits[1]=m_waitingEntry;
    values[1]=jobs_waiting;
    ref_values[1]=&m_jobsWaiting;

    line_edits[2]=m_freeEntry;
    values[2]=empty_slots;
    ref_values[2]=&m_emptySlots;


    for (unsigned int var_i=0; var_i<3; var_i++) {
      if (*(ref_values[var_i]) != values[var_i] ) {
	*(ref_values[var_i]) = values[var_i];
	std::stringstream value_string;
	value_string << *(ref_values[var_i]);
	line_edits[var_i]->setText( value_string.str().c_str());
      }
    }
  }

  void CanStatusPanel::updateSchedulerStatus(SchedulerStatusEvent::ESchedulerStatus status) {
    if (status != m_status) {
      m_status = status;
      if (m_status>=SchedulerStatusEvent::kNStati) {
	m_status = SchedulerStatusEvent::kUnknown;
      }

      std::pair<QBrush , QPen > state_colour = m_palette->brushAndPen(true,1, 0, static_cast<VarId_t>(-1), static_cast<State_t>(mapStatus(m_status )) );
      m_canStatus->setStyleSheet("background: "+state_colour.first.color().name());
      m_canStatus->setText(CanStatusPanel::SchedulerStatusEvent::statusName(m_status));
    }
  }

  void CanStatusPanel::shutdown() {
    std::cout << "INFO [CanStatusPanel::shutdown]" << std::endl;
    m_manitu->shutdownMonitoring();
    m_manitu->waitShutdownMonitoring();
  }
  
  void CanStatusPanel::initiateShutdown() {
    m_manitu->shutdownMonitoring();
  }
}
