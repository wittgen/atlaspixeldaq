// Dear emacs, this is -*-c++-*-
#ifndef _PixCon_PixScanConfigEdit_h_
#define _PixCon_PixScanConfigEdit_h_
#include "ConfigEdit.h"
#include "IConfigEdit.h"
#include <string>
#include <vector>

namespace PixA {
  class ConfigHandle;
}

class QComboBox;

namespace PixCon {

  class PixScanPanel;

  class PixScanConfigEdit : public ConfigEdit, public IConfigEdit
  {
  public:
    PixScanConfigEdit(std::vector<PixA::ConfigHandle> &vconfig,
              //PixA::ConfigHandle &config,
              const std::string &type_name,
              const std::string &tag_name,
              QComboBox &tag_list,
              QWidget* parent = 0);
    ~PixScanConfigEdit();


    int exec () {
      return ConfigEdit::exec();
    }

    bool changed() const;

    PixLib::Config &config();

    PixLib::Config &config(std::string tag_name);

    std::string tagName() const;

    void errorOnSave() { highlightTagField(); }

  private:
    PixScanPanel *m_pixScanPanel;
    PixScanPanel *m_pixScanPanelFEI4;
  };
}
#endif
