// Dear emacs, this is -*-c++-*-
#ifndef _PixCon_ObjectConfigEdit_h_
#define _PixCon_ObjectConfigEdit_h_

#include "ConfigEdit.h"
#include "IConfigEdit.h"
#include <string>
#include <ConfigWrapper/ConfigHandle.h>
#include <Common.hh>

class QComboBox;
class optionsFrame;

namespace CAN {
  class IAnalysisObject;
}

namespace PixCon {

  class ObjectConfigEdit : public ConfigEdit, public IConfigEdit
  {
  public:
    ObjectConfigEdit(std::vector<PixA::ConfigHandle> &vconfig,
		     const std::string &type_name,
		     const std::string &tag_name,
		     CAN::Revision_t revision,
		     QComboBox &tag_list,
		     QWidget* parent = 0 );
    ~ObjectConfigEdit();


    int exec () {
      return ConfigEdit::exec();
    }

    bool changed() const;

    PixLib::Config &config();

    PixLib::Config &config(std::string tag_name);

    std::string tagName() const;

    void errorOnSave() { highlightTagField(); }

  private:
    optionsFrame       *m_configPanelFEI3;
    optionsFrame       *m_configPanelFEI4;
    std::unique_ptr<CAN::IAnalysisObject> m_obj_fei3;
    std::unique_ptr<CAN::IAnalysisObject> m_obj_fei4;
    //@todo config should either return a const PixLib::Config or m_config and m_configRef should be mutable handles and references
    PixA::ConfigHandle  m_config_fei3;
    PixA::ConfigHandle  m_config_fei4;
    std::vector<PixA::ConfigRef> m_configRef_fei3;
    std::vector<PixA::ConfigRef> m_configRef_fei4;
  };
}
#endif
