#include <ConfigWrapper/ConfGroupUtil.h>

#include "ObjectConfigEdit.h"
#include "ScanSelection.h"
#include <QTVisualiser/BusyLoopFactory.h>
#include "QTEditors/OptionsFrame.h"

#include <DummyConfigDb.hh>
#include <AnalysisInfo_t.hh>
#include <AnalysisFactory.hh>
#include <IAnalysisObject.hh>
#include <PixDbGlobalMutex.hh>
#include <Lock.h>

namespace PixCon {

  ObjectConfigEdit::ObjectConfigEdit(std::vector<PixA::ConfigHandle> &vconfig,
				     //PixA::ConfigHandle &config,
				     const std::string &type_name,
				     const std::string &tag_name,
				     CAN::Revision_t revision,
				     QComboBox &tag_list,
				     QWidget* parent)
    : ConfigEdit(parent),
      m_config_fei3(vconfig[ScanSelection::TBase]),
      m_config_fei4(vconfig[ScanSelection::TBase_I4])
  {
    if (m_config_fei3 && m_config_fei4) {
      PixA::ConfigHandle full_copy_fei3;
      PixA::ConfigHandle full_copy_fei4;
      {
	//@todo remove global lock once the configuration comes from oracle
	Lock globalLock(CAN::PixDbGlobalMutex::mutex());
	std::stringstream message;
	message << " Get configuration of " << type_name << " .... ";
	std::unique_ptr<PixA::IBusyLoop> busy_loop(PixA::BusyLoopFactory::busyLoop(message.str()));
	full_copy_fei3 = PixA::ConfigHandle(m_config_fei3.ref().createFullCopy());
	full_copy_fei4 = PixA::ConfigHandle(m_config_fei4.ref().createFullCopy());
      }
      PixA::ConfigRef config_ref_fei3( full_copy_fei3.ref() );
      PixA::ConfigRef config_ref_fei4( full_copy_fei4.ref() );
      try {
	std::string tag_fei3 = tag_name;
	std::string tag_fei4 = tag_name;
	//tag_fei3.insert(tag_fei3.size(), "_I3");
	tag_fei4.insert(tag_fei4.size(), "_I4");
	CAN::ObjectInfo_t obj_info_fei3(type_name, tag_fei3, revision);
	CAN::ObjectInfo_t obj_info_fei4(type_name, tag_fei4, revision);
	std::unique_ptr<CAN::IObjectConfigDb> db( static_cast<CAN::IObjectConfigDb *>(new CAN::DummyConfigDb  ));

	//	std::cerr << "INFO [ObjectConfigEdit::ctor] create object  " <<  type_name << " : " << tag_name << " / " << revision << std::endl;
	m_obj_fei3 = std::unique_ptr<CAN::IAnalysisObject>(CAN::AnalysisFactory::get()->create(0,obj_info_fei3, db.get()));
	m_obj_fei4 = std::unique_ptr<CAN::IAnalysisObject>(CAN::AnalysisFactory::get()->create(0,obj_info_fei4, db.get()));

	//	std::cerr << "INFO [ObjectConfigEdit::ctor] created object." << std::endl;
	m_obj_fei3->config().copy(config_ref_fei3.config() );
	m_obj_fei4->config().copy(config_ref_fei4.config() );
	updateConfList(m_obj_fei3->config(), config_ref_fei3.config());
	updateConfList(m_obj_fei4->config(), config_ref_fei4.config());
	// m_obj_fei3->config().dump(std::cout);

	//	std::cout << "INFO [ObjectConfigEdit::ctor] create options Frame." << std::endl;

	m_configPanelFEI3 = new optionsFrame(m_obj_fei3->config(),m_configPanelFrame,true);
	//vboxLayout1->addWidget( m_configPanelFEI3 );	
        m_configTabFrameFEI3Layout->addWidget( m_configPanelFEI3 );
	connect( m_saveButton, SIGNAL( pressed() ),m_configPanelFEI3 , SLOT( save() ) );

	m_configPanelFEI4 = new optionsFrame(m_obj_fei4->config(),m_configPanelFrameFEI4,true);
	//vboxLayout2->addWidget( m_configPanelFEI4 );	
	m_configTabFrameFEI4Layout->addWidget( m_configPanelFEI4 );
	connect( m_saveButton, SIGNAL( pressed() ),m_configPanelFEI4 , SLOT( save() ) );

	m_configPanelFEI3->show();
	m_configPanelFEI4->show();
      }
      catch( CAN::MRSException &err) {
	std::cerr << "ERROR [ObjectConfigEdit::ctor] Caught exception : "
		  << err.getDescriptor()
		  << std::endl;
      }
      catch( std::exception &err) {
	std::cerr << "ERROR [ObjectConfigEdit::ctor] Caught exception : "
		  << err.what()
		  << std::endl;
      }
      catch(...) {
	std::cerr << "ERROR [ObjectConfigEdit::ctor] Caught unknown exception."
		  << std::endl;
      }


    }
/*
    //    std::cout << "INFO [ObjectConfigEdit::ctor] Set tags." << std::endl;
    for(unsigned int item_i=0; item_i < tag_list.count(); item_i++) {
      m_tagSelector->insertItem(tag_list.text(item_i));
      if (tag_name == tag_list.text(item_i).latin1()) {
    m_tagSelector->setCurrentItem(m_tagSelector->count()-1);
      }
    }
QListView* view ; // The view of interest

QAbstractItemModel* model = view->model() ;
QStringList strings ;
for ( int i = 0 ; i < model->rowCount() ; ++i )
{
  // Get item at row i, col 0.
  strings << model->index( i, 0 ).data( Qt::DisplayRole ).toString() ;
}
*/
    //    std::cout << "INFO [ObjectConfigEdit::ctor] Set tags." << std::endl;
  for(unsigned int item_i=0; item_i < (unsigned int)tag_list.count(); item_i++) {//Originally tag_list.count()
        QString listString;
        listString = tag_list.currentText();
        m_tagSelector->insertItem(item_i,listString,Qt::DisplayRole);//
      if (tag_name == listString.toStdString()) {//
        m_tagSelector->setCurrentIndex(m_tagSelector->count()-1);
      }
    }

    {
    QSize a_size(m_tagSelector->sizeHint());
    std::cout << "INFO [ObjectConfigEdit::ctor] tag selector size = " << a_size.width() << " x " << a_size.height() << std::endl;
    {
      QFontMetrics fm(m_tagSelector->font());
      int text_width(fm.width("M"));
      a_size.setWidth(a_size.width()+text_width*2);
      if (m_tagSelector->view()) {
    std::cout << "INFO [ObjectConfigEdit::ctor] list box size  = " <<  m_tagSelector->view()->maximumWidth() << std::endl;
    if (m_tagSelector->view()->maximumWidth()+text_width*2>a_size.width()) {
      a_size.setWidth(static_cast<int>(m_tagSelector->view()->maximumWidth()>+text_width*2));
    }
      }
      std::cout << "INFO [ObjectConfigEdit::ctor] tag selector new size = " << a_size.width() << " x " << a_size.height() << std::endl;
    }

    
    m_tagSelector->resize( a_size );
    }
    //    std::cout << "INFO [ObjectConfigEdit::ctor] show." << std::endl;
    adjustSize();
    show();
  }

  bool ObjectConfigEdit::changed() const {
    //@todo compare current with orig config and find out whether something has changed.
    return true;
  }

  std::string ObjectConfigEdit::tagName() const {
    std::string temp;
    if (m_tagSelector->currentText().length()>0) {
      temp = m_tagSelector->currentText().toLatin1().data();
    }
    return temp;
  }

  PixLib::Config &ObjectConfigEdit::config() {
    if (m_obj_fei3.get()) {
      return m_obj_fei3->config();
    }
    else {
      m_configRef_fei3.clear();
      m_configRef_fei3.push_back(m_config_fei3.ref().createFullCopy());
      return const_cast<PixLib::Config &>(m_configRef_fei3.back().config());
    }

  }

  PixLib::Config &ObjectConfigEdit::config(std::string tag_name) {
std::cout << "~~~~~~~~~~~~~~ ObjectConfigEdit.cxx1" << std::endl;
 /*   if(tag_name.compare(tag_name.size()-3, 3, "_I3") == 0){
      if (m_obj_fei3.get()) {
	return m_obj_fei3->config();
      }
      else {
	m_configRef_fei3.clear();
	m_configRef_fei3.push_back(m_config_fei3.ref().createFullCopy());
	return const_cast<PixLib::Config &>(m_configRef_fei3.back().config());
      }
    }else */if(tag_name.compare(tag_name.size()-3, 3, "_I4") == 0){
      if (m_obj_fei4.get()) {
	return m_obj_fei4->config();
      }
      else {
	m_configRef_fei4.clear();
	m_configRef_fei4.push_back(m_config_fei4.ref().createFullCopy());
	return const_cast<PixLib::Config &>(m_configRef_fei4.back().config());
      }
    }else{
      if (m_obj_fei3.get()) {
	return m_obj_fei3->config();
      }
      else {
	m_configRef_fei3.clear();
	m_configRef_fei3.push_back(m_config_fei3.ref().createFullCopy());
	return const_cast<PixLib::Config &>(m_configRef_fei3.back().config());
      }
    }
  }

  ObjectConfigEdit::~ObjectConfigEdit() {
    // to ensure that the reference disapears before the handle is destructed
    m_configRef_fei3.clear();
    m_configRef_fei4.clear();
  }

}
