//#include <ipc/partition.h>
//#include <ipc/core.h>

#include <iostream>
#include <iomanip>

#include <ConfigWrapper/pixa_exception.h>
#include <ConfigWrapper/Connectivity.h>
#include <ConfigWrapper/ConnectivityManager.h>
#include <ConfigWrapper/ConnectivityUtil.h>
#include <ConfigWrapper/PixDisableUtil.h>

#include <PixDbServer/PixDbServerInterface.h>
#include <ipc/partition.h>
#include <ipc/core.h>

#include <CoralDbClient.hh>
#include <PixMetaException.hh>

#include <ConfigWrapper/createDbServer.h>

class IPCInstance
{

 private:
  IPCInstance(int argc, char **argv, const std::string &partition_name) {
    IPCCore::init(argc, argv);
    m_ipcPartition=std::unique_ptr<IPCPartition>(new IPCPartition(partition_name.c_str()));

    if (!m_ipcPartition->isValid()) {
      std::stringstream message;
      message << "ERROR [IPCInstance] Not a valid partition " << partition_name << std::endl;
      throw std::runtime_error(message.str());
    }
  }
 public:

  static IPCInstance *instance(int argc, char **argv, const std::string &partition_name) {
    if (!s_instance) {
      s_instance = new IPCInstance(argc,argv,partition_name);
    }
    return s_instance;
  }

  IPCPartition &partition() { return *m_ipcPartition;}
  
 private:
  std::unique_ptr<IPCPartition> m_ipcPartition;
  static IPCInstance *s_instance;

};

IPCInstance *IPCInstance::s_instance = NULL;

class MyPixDbServerInterface : public PixLib::PixDbServerInterface
{
public:
  MyPixDbServerInterface(IPCPartition *ipcPartition, std::string ipcName,  PixLib::PixDbServerInterface::type serviceType,  bool &ready)
    : PixLib::PixDbServerInterface(ipcPartition, ipcName, serviceType, ready) {
    std::cout << "INFO [MyPixDbServerInterface] new instance " << static_cast<void *>(this) << std::endl;
  }

  ~MyPixDbServerInterface()
  {
    std::cout << "INFO [MyPixDbServerInterface] destruct instance " << static_cast<void *>(this) << std::endl;
  }

};

bool setDbServer(const std::string &db_server_partition_name,
		 const std::string &db_server_name,
		 int argc,
		 char **argv)
{
  int temp_argc=0;
  char *temp_argv[1]={NULL};
  IPCInstance *ipc_instance = IPCInstance::instance(temp_argc,temp_argv, db_server_partition_name);

  std::shared_ptr<PixLib::PixDbServerInterface> db_server( PixA::createDbServer(ipc_instance->partition(),db_server_name) );
  if (db_server.get()) {
    PixA::ConnectivityManager::useDbServer(db_server);
    return true;
  }
  return false;
}

std::string enableString( bool enabled ) {
  if (enabled) return "ENABLED ";
  else return         "disabled";
}


void analysePixDisableString(const std::string &pix_disable_string) {
  unsigned int n_rods=0;
  unsigned int n_modules=0;
  unsigned int max_modules_per_rod=0;
  unsigned int modules_per_rod=0;
  std::string::size_type start_pos=0;
  for(;start_pos+1<pix_disable_string.size() ;) {
    std::string::size_type pos =  pix_disable_string.find(":",start_pos+1);
    if (pos != std::string::npos) {
      if (pos+1<pix_disable_string.size() && pix_disable_string[pos+1]==':') {
	n_rods++;
	if (modules_per_rod>max_modules_per_rod) {
	  max_modules_per_rod=modules_per_rod;
	}
	modules_per_rod=0;
	pos++;
      }
      else {
	modules_per_rod++;
	n_modules++;
      }
      start_pos=pos;
    }
  }

  if (modules_per_rod>max_modules_per_rod) {
    max_modules_per_rod=modules_per_rod;
  }
  std::cout << "INFO [analysePixDisableString] rods = " << n_rods << " modules = " << n_modules << "  max. modules / ROD " << max_modules_per_rod << std::endl;
}

int main(int argc, char **argv)
{
  std::string id_name="CT";
  std::string tag_name;
  std::string cfg_tag_name;
  std::string mod_cfg_tag_name;
  bool db_server_tags=false;
  bool show_disabled_objects=false;
  int verbose=1;
  bool error=false;
  bool simulation=true;
  bool read_only=false;
  bool unset_only=true;
  bool read_stdin=false;

  std::vector<CAN::SerialNumber_t> serial_number;
  std::shared_ptr<PixLib::PixDisable> pix_disable;

  for (int arg_i=1; arg_i<argc; arg_i++) {

    if (strcmp(argv[arg_i],"--db-server")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {

      try {
	std::string db_server_partition_name = argv[++arg_i];
	std::string db_server_name = "PixelDbServer";
	if (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	  db_server_name = argv[++arg_i];
	}
	db_server_tags = setDbServer(db_server_partition_name, db_server_name, argc, argv);
      }
      catch (std::runtime_error &err) {
	std::cout << "FATAL [main] caught exception (std::exception) : " << err.what() << std::endl;
	return -1;
      }

    }
    else if  (strcmp(argv[arg_i],"-")==0) {
      read_stdin=true;
    }
    else if (strcmp(argv[arg_i],"-s")==0) {
      if (strcmp(argv[arg_i],"-")==0) {
       read_stdin=true;
      }
      else {
      while (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	serial_number.push_back( atoi(argv[++arg_i]) );
      }
      }
    }
    else if (strcmp(argv[arg_i],"-v")==0 || strcmp(argv[arg_i],"--verbose")==0) {
      if (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	verbose=atoi(argv[++arg_i]);
      }
      else {
	verbose++;
      }
    }
    else if (strcmp(argv[arg_i],"-q")==0 || strcmp(argv[arg_i],"--quiet")==0) {
      verbose=0;
    }
    else if (strcmp(argv[arg_i],"-r")==0) {
      read_only=true;
    }
    else if (strcmp(argv[arg_i],"-x")==0 || strcmp(argv[arg_i],"--execute")==0) {
      simulation=false;
    }
    else if (strcmp(argv[arg_i],"--simulation")==0) {
      simulation=true;
    }    
    else if (strcmp(argv[arg_i],"--unset-only")==0 || strcmp(argv[arg_i],"-u")==0) {
      unset_only=true;
    }    
    else if (strcmp(argv[arg_i],"--set-all")==0 || strcmp(argv[arg_i],"-a")==0) {
      unset_only=false;
    }    
    else {
      std::cout << "ERROR ["<< argv[0] << ":main] unhandled argument " << arg_i << " : " << argv[arg_i]<< "." << std::endl;
      error=true;
      break;
    }
  }

  if (error) {
    std::cout << "USAGE ["<<argv[0] << " [--db-server partition [name] ] [ -s serial-number  ...  ] [-v [level] ] [-q]  -x " << std::endl
	      << "--db-server partition [name]\tConsider the given db server when trying to load a connectivity." << std::endl
	      << "-s\t\t\t\t\t\tserial-number Update the disable meta data for the given list of scans or - for stdin." << std::endl
	      << "-v [level]\t\tIncrease the verbosity or set the verbosity to the given level." << std::endl
	      << "-q\t\tQuiet mode." << std::endl
	      << "-x/--execute\tChange the meta data in the database instead of just simulating." << std::endl
	      << "-u/--unset-only\tOnly set enable information if still empty." << std::endl
	      << "-a/--set-all\t\tAlways set standard information." << std::endl
	      << std::endl;
    return -1;
  }

  if (serial_number.empty()) {
     read_stdin=true;
  }

  if (read_stdin) {
    while (std::cin) {
       std::string line_in;	
       std::cin >> line_in;
       if (verbose>10) {
	 std::cout << "DIAGNOSTICS [" << argv[0] << ":main] read " << line_in << std::endl;
       }
       std::string::size_type pos=0;
       while (pos+1<line_in.size()) {
         while (isspace(line_in[pos]) && pos<line_in.size()) pos++;
         if (pos+1>=line_in.size()) break;

         if (line_in[pos]!='S' || !isdigit(line_in[pos+1])) break;
         pos++;
	 std::string number;
	 std::string::size_type pos2=pos;
	 for(;pos2<line_in.size();) {
	   while (isdigit(line_in[pos2]) && pos2<line_in.size()) pos2++;
	   number+=line_in.substr(pos,pos2-pos);
	   pos=pos2;
	   if (pos2<line_in.size() && line_in[pos2]=='-' && pos2+1<line_in.size() && isdigit(line_in[pos2])) {
	     pos2++;
	     continue;
	   }
           break;
	 }
         if ((pos2>=line_in.size() || isspace(line_in[pos2])) && (number.size()==3 || number.size()==6 || number.size()==9)) {
	   serial_number.push_back(atoi(&(number[0])));
	   if (verbose>4) {
	     std::cout << "INFO [" << argv[0] << ":main] Added " << serial_number.back() << std::endl;
           }
         }
       }
    }
  }

  try {
    CAN::PixCoralDbClient::fixMode();
    CAN::PixCoralDbClient meta_data_client(verbose>1);

    for (std::vector<CAN::SerialNumber_t>::const_iterator serial_number_iter = serial_number.begin();
	 serial_number_iter != serial_number.end();
	 ++serial_number_iter) {

      PixLib::ScanInfo_t scan_info = meta_data_client.getMetadata<PixLib::ScanInfo_t>(*serial_number_iter);
      if (read_only) {
	if (verbose>1) {
	  std::cout << "INFO ["<< argv[0] << ":main] enable string : " << scan_info.pixDisableString() << std::endl;
	}
	continue;
      }
     if (unset_only && !scan_info.pixDisableString().empty()) { 
          if (verbose>2) {
             if (verbose>3) {
       	        std::cout << "INFO ["<< argv[0] << ":main] Enable string already set for  " << *serial_number_iter  << " to : " <<scan_info.pixDisableString() << "." << std::endl;
             }
             else {
	        std::cout << "INFO ["<< argv[0] << ":main] Enable string already set for  " << *serial_number_iter  << " (size=" <<scan_info.pixDisableString().size() << ")." << std::endl;
             }
          }
          continue; 
      }
      // @todo should also get the ROD status and propaget it to the calibration data container

      CAN::Revision_t best_guess_of_cfg_revision = (scan_info.cfgRev()==0 ? scan_info.scanStart() : scan_info.cfgRev());
      PixA::ConnectivityRef conn;
      unsigned int n_rods=0;
      unsigned int n_modules=0;
      {
	try {
	  conn =PixA::ConnectivityManager::getConnectivity( scan_info.idTag(),
							    scan_info.connTag(),
							    scan_info.aliasTag(),
							    scan_info.dataTag(),
							    scan_info.cfgTag(),
							    scan_info.modCfgTag(),
							    best_guess_of_cfg_revision);

	  if (!conn) {
	    std::cerr << "ERROR ["<<argv[0] << ":main] Did not get a connectivity for " << *serial_number_iter << std::endl;
	    continue;
	  }

	  std::stringstream disable_name;
	  disable_name << "Disable-S" << std::setfill('0') << std::setw(9)  << *serial_number_iter;
	  std::shared_ptr<const PixLib::PixDisable> disable( conn.getDisable(disable_name.str()) );
	  if (!disable.get()) {
	    std::cerr << "ERROR ["<<argv[0] << ":main] Did not get a disable for " << *serial_number_iter << std::endl;
	    continue;    
	  }
	  if (verbose>3) {
	    const_cast<PixLib::PixDisable &>(*disable).config().dump(std::cout);
	  }

	  PixA::DisabledListRoot disabled_list_root( disable );
	  std::string enabled_rod_string;

	  for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
	       crate_iter != conn.crates().end();
	       ++crate_iter) {

	    bool crate_enabled = PixA::enabled( disabled_list_root, crate_iter );

	    if (verbose>2) {
	      std::cout << "Crate : " << PixA::connectivityName(crate_iter)  << " " << enableString(crate_enabled) << std::endl;
	    }
	    if (!crate_enabled && !show_disabled_objects) continue;

	    const PixA::DisabledList rod_disabled_list( PixA::rodDisabledList( disabled_list_root, crate_iter) );

	    PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
	    PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
	    for (PixA::RodList::const_iterator rod_iter = rod_begin;
		 rod_iter != rod_end;
		 ++rod_iter) {

	      std::string enabled_modules_string;
	      // for testing get a rod disable list from the root list
	      const PixA::DisabledList rod_disabled_list_2( PixA::rodDisabledList( disabled_list_root, *rod_iter ) );

	      bool rod_enabled = PixA::enabled( rod_disabled_list, rod_iter );

	      if (verbose>2) {
		std::cout << "ROD : " << PixA::connectivityName(rod_iter)  << " " << enableString(rod_enabled)  << std::endl;
	      }
	
              if (!rod_enabled) continue;
	      const PixA::DisabledListRoot pp0_disabled_list( PixA::createPp0DisabledList( conn, disabled_list_root, PixA::connectivityName(rod_iter)) );

	      PixA::Pp0List::const_iterator pp0_begin = PixA::pp0Begin(rod_iter);
	      PixA::Pp0List::const_iterator pp0_end = PixA::pp0End(rod_iter);
	      for (PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
		   pp0_iter != pp0_end;
		   ++pp0_iter) {

		bool pp0_enabled = PixA::enabled( pp0_disabled_list, pp0_iter );
		if (!pp0_enabled && !show_disabled_objects) continue;

		const PixA::DisabledList module_disabled_list( PixA::moduleDisabledList( pp0_disabled_list, pp0_iter) );

		std::set<std::string> module_list;

		PixA::ModuleList::const_iterator module_begin = PixA::modulesBegin(pp0_iter);
		PixA::ModuleList::const_iterator module_end = PixA::modulesEnd(pp0_iter);
		for (PixA::ModuleList::const_iterator module_iter = module_begin;
		     module_iter != module_end;
		     ++module_iter) {

		  bool module_enabled = PixA::enabled( module_disabled_list, module_iter );
		  if (verbose>2) {
		    std::cout << "Module : " << PixA::connectivityName(module_iter)  << " " << enableString(module_enabled) << std::endl;
		  }
		  if (module_enabled && pp0_enabled) {
		    module_list.insert( PixA::connectivityName(module_iter) );
		  }
		}

                for(std::set<std::string>::const_iterator module_iter =  module_list.begin();
		    module_iter != module_list.end();
		    ++module_iter) {

		  enabled_modules_string+=*module_iter;
		  enabled_modules_string+=":";
		  n_modules++;
		}
	      }

	      if (!enabled_modules_string.empty()) {
		enabled_rod_string += PixA::connectivityName(rod_iter);
		enabled_rod_string += "::";
		enabled_rod_string += enabled_modules_string;
		n_rods++;
	      }
	    }
	  }

	  if (enabled_rod_string.empty()) {
	    std::cerr << "WARNING ["<< argv[0] <<":main] No enabled modules for scan " << *serial_number_iter << "." << std::endl;
	  }
	  else {
	    enabled_rod_string=std::string(":")+enabled_rod_string;
	    if (verbose>1 || simulation) {
	      std::cout << "INFO ["<< argv[0] << ":main] enable string : " << enabled_rod_string << std::endl;
	    }
	    if (verbose>0 || simulation) {
	      std::cout << "INFO ["<< argv[0] << ":main] enable string length = " << enabled_rod_string.size() << " contains " 
			<< n_rods << " ROD and " << n_modules << " module names." << std::endl;
	    }
	    if (!simulation) {
	      meta_data_client.updateScanMetadataDisable(*serial_number_iter, enabled_rod_string);
	    }
	    else {
	      analysePixDisableString(enabled_rod_string);
	      std::cout << "INFO ["<< argv[0] << ":main] simulation. Did not updatedate metadata." << std::endl;
	    }
	  }
	}
	catch (PixA::ConfigException &err) {
	  std::cout << "FATAL [main] caught exception (ConfigException) : " << err.getDescriptor() << std::endl;
	  return -1;
	}
	catch (PixA::BaseException &err) {
	  std::cout << "FATAL [main] caught exception (BaseException) : " << err.getDescriptor() << std::endl;
	  return -1;
	}
	catch (std::exception &err) {
	  std::cout << "FATAL [main] caught exception (std::exception) : " << err.what() << std::endl;
	  return -1;
	}
	catch (...) {
	  std::cout << "FATAL [main] unhandled exception. "  << std::endl;
	  return -1;
	}
      }
    }
  }
  catch (PixA::ConfigException &err) {
    std::cout << "FATAL [main] caught exception (ConfigException) : " << err.getDescriptor() << std::endl;
    return -1;
  }
  catch (PixA::BaseException &err) {
    std::cout << "FATAL [main] caught exception (BaseException) : " << err.getDescriptor() << std::endl;
    return -1;
  }
  catch (std::exception &err) {
    std::cout << "FATAL [main] caught exception (std::exception) : " << err.what() << std::endl;
    return -1;
  }
  catch (...) {
    std::cout << "FATAL [main] unhandled exception. "  << std::endl;
    return -1;
  }
  return 0;
}
