/* Dear emacs, this is -*-c++-*- */
#ifndef _PixCon_ProgressEvent_h_
#define _PixCon_ProgressEvent_h_

#include <CalibrationDataTypes.h>
#include <qevent.h>

namespace PixCon {

  class ProgressEvent : public QEvent
  {
  public:
    ProgressEvent(unsigned int step, unsigned int total_steps)
      : QEvent(User),
	//m_measurement(measurement),
	m_step(step),
	m_totalSteps(total_steps)
    {}

    unsigned int step() const  { return m_step; }
    unsigned int totalSteps() const  { return m_totalSteps; }

  private:
    //EMeasurementType m_measurement;
    unsigned int     m_step;
    unsigned int     m_totalSteps;
  };

}
#endif
