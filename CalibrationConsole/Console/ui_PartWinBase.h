#ifndef UI_PARTWINBASE_H
#define UI_PARTWINBASE_H

#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QComboBox>
#include <QDialog>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QPushButton>
#include <QSpacerItem>
#include <QSplitter>
#include <QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_PartWinBase
{
public:
    QVBoxLayout *vboxLayout;
    QVBoxLayout *vboxLayout1;
    QLabel *textLabel1;
    QSpacerItem *spacer1;
    QVBoxLayout *vboxLayout2;
    QSplitter *splitter1;
    QLabel *textLabel2;
    QComboBox *partBox;
    QSplitter *splitter2;
    QLabel *textLabel3;
    QComboBox *issrvBox;
    QSplitter *splitter2_2;
    QLabel *textLabel3_2;
    QComboBox *ohsrvBox;
    QSpacerItem *spacer4;
    QHBoxLayout *hboxLayout;
    QSpacerItem *spacer2;
    QPushButton *pushButton1;
    QSpacerItem *spacer3;

    void setupUi(QDialog *PartWinBase)
    {
        if (PartWinBase->objectName().isEmpty())
            PartWinBase->setObjectName(QString::fromUtf8("PartWinBase"));
        PartWinBase->resize(574, 256);
        vboxLayout = new QVBoxLayout(PartWinBase);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        vboxLayout1 = new QVBoxLayout();
        vboxLayout1->setSpacing(6);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        textLabel1 = new QLabel(PartWinBase);
        textLabel1->setObjectName(QString::fromUtf8("textLabel1"));
        textLabel1->setWordWrap(false);

        vboxLayout1->addWidget(textLabel1);

        spacer1 = new QSpacerItem(21, 31, QSizePolicy::Minimum, QSizePolicy::Fixed);

        vboxLayout1->addItem(spacer1);

        vboxLayout2 = new QVBoxLayout();
        vboxLayout2->setSpacing(6);
        vboxLayout2->setObjectName(QString::fromUtf8("vboxLayout2"));
        splitter1 = new QSplitter(PartWinBase);
        splitter1->setObjectName(QString::fromUtf8("splitter1"));
        splitter1->setOrientation(Qt::Horizontal);
        textLabel2 = new QLabel(splitter1);
        textLabel2->setObjectName(QString::fromUtf8("textLabel2"));
        textLabel2->setWordWrap(false);
        splitter1->addWidget(textLabel2);
        partBox = new QComboBox(splitter1);
        partBox->setObjectName(QString::fromUtf8("partBox"));
        splitter1->addWidget(partBox);

        vboxLayout2->addWidget(splitter1);

        splitter2 = new QSplitter(PartWinBase);
        splitter2->setObjectName(QString::fromUtf8("splitter2"));
        splitter2->setOrientation(Qt::Horizontal);
        textLabel3 = new QLabel(splitter2);
        textLabel3->setObjectName(QString::fromUtf8("textLabel3"));
        textLabel3->setWordWrap(false);
        splitter2->addWidget(textLabel3);
        issrvBox = new QComboBox(splitter2);
        issrvBox->setObjectName(QString::fromUtf8("issrvBox"));
        splitter2->addWidget(issrvBox);

        vboxLayout2->addWidget(splitter2);

        splitter2_2 = new QSplitter(PartWinBase);
        splitter2_2->setObjectName(QString::fromUtf8("splitter2_2"));
        splitter2_2->setOrientation(Qt::Horizontal);
        textLabel3_2 = new QLabel(splitter2_2);
        textLabel3_2->setObjectName(QString::fromUtf8("textLabel3_2"));
        textLabel3_2->setWordWrap(false);
        splitter2_2->addWidget(textLabel3_2);
        ohsrvBox = new QComboBox(splitter2_2);
        ohsrvBox->setObjectName(QString::fromUtf8("ohsrvBox"));
        splitter2_2->addWidget(ohsrvBox);

        vboxLayout2->addWidget(splitter2_2);


        vboxLayout1->addLayout(vboxLayout2);

        spacer4 = new QSpacerItem(20, 29, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout1->addItem(spacer4);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        spacer2 = new QSpacerItem(191, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacer2);

        pushButton1 = new QPushButton(PartWinBase);
        pushButton1->setObjectName(QString::fromUtf8("pushButton1"));

        hboxLayout->addWidget(pushButton1);

        spacer3 = new QSpacerItem(211, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacer3);


        vboxLayout1->addLayout(hboxLayout);


        vboxLayout->addLayout(vboxLayout1);


        retranslateUi(PartWinBase);
        QObject::connect(pushButton1, SIGNAL(clicked()), PartWinBase, SLOT(accept()));
        QObject::connect(partBox, SIGNAL(activated(int)), PartWinBase, SLOT(partSelected(int)));

        QMetaObject::connectSlotsByName(PartWinBase);
    } // setupUi

    void retranslateUi(QDialog *PartWinBase)
    {
        PartWinBase->setWindowTitle(QApplication::translate("PartWinBase", "TDAQ setup", 0));
        textLabel1->setText(QApplication::translate("PartWinBase", "Please select the following for TDAQ set-up:", 0));
        textLabel2->setText(QApplication::translate("PartWinBase", "TDAQ partition", 0));
        textLabel3->setText(QApplication::translate("PartWinBase", "IS server name", 0));
        textLabel3_2->setText(QApplication::translate("PartWinBase", "OH server name", 0));
        pushButton1->setText(QApplication::translate("PartWinBase", "OK", 0));
    } // retranslateUi

};

namespace Ui {
    class PartWinBase: public Ui_PartWinBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PARTWINBASE_H
