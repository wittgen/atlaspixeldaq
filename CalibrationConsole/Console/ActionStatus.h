#ifndef _Pixcon_ActionStatus_h_
#define _Pixcon_ActionStatus_h_

#include <string>
#include <CalibrationDataTypes.h>
#include <VarDefList.h>

namespace PixCon {

  class ActionStatus
  {
  public:
    enum EStatus {
      kRunning,
      kOk,
      kFailure,
      kAborted,
      kUnknown,
      kNStates
    };

    static State_t status(const std::string &status_name) {
      for (unsigned int state_i=0; state_i<kNStates; state_i++) {
	if ( status_name == s_statusNameToState[state_i]) return static_cast<State_t>(state_i);
      }
      return kUnknown;
    }

    static const std::string &name(State_t status) {
      if (status>=kNStates) {
        return s_statusNameToState[kUnknown];
      }
      else {
        return s_statusNameToState[status];
      }
    }

    static bool isTerminated(EStatus the_status) { return the_status > kRunning && the_status < kUnknown; }
    static bool isSuccess(EStatus the_status) { return the_status == kOk; }

  private:
    static bool initialseStatusNameTranslationMap();

    static bool s_initialised;

    static std::string s_statusNameToState[kNStates];

  };

}
#endif
