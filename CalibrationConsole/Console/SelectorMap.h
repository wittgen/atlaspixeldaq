/* Dear emacs, this is -*-c++-*- */
#ifndef _PixCon_ScanSelectorMap_h_
#define _PixCon_ScanSelectorMap_h_

#include <qobject.h>
class QWidget;

namespace PixCon {

  class SelectorMap : public QObject
  {
    Q_OBJECT
  public:
    SelectorMap( QWidget *parent, unsigned int selector_id, QWidget *destination, const char* method);

  public slots:

    virtual void activated(int item_i);

  signals:
    void selectorActivated(int selctor_id, int item_i);

  private:
    unsigned int m_selectorId;
  };

}
#endif
