#ifndef _PixCon_ExitHandler_h_
#define _PixCon_ExitHandler_h_

#include <sys/types.h>
#include <unistd.h>
#include <map>
#include <vector>
#include <signal.h>

#include <IExitHelper.h>

namespace PixCon {

  class ExitHandler
  {
  public:
    static ExitHandler *instance() {
      if (!s_instance) {
	s_instance=new ExitHandler;
      }
      return s_instance;
    }

    int installHandler( std::vector<int> signal_list);

    void restoreHandler();


    void addHelper(IExitHelper *helper) ;
    void removeHelper(IExitHelper *helper) ;

    bool isInstalled() const { return m_installed;}
  private:

    ExitHandler() : m_pid( getpid() ),m_counter(1),m_installed(false) {}

    int installHandler( int signal, struct sigaction &sa);


    void freeResources(int signum);

    static void exitHandler(int signum);

    static ExitHandler *s_instance;

    pid_t        m_pid;
    unsigned int m_counter;
    std::map<int, struct sigaction>     m_oldHandler;
    std::vector< IExitHelper * > m_exitHelper;
    bool m_installed;
  };

}
#endif
