#include <ipc/partition.h>
#include <ipc/core.h>
#include "test_DummyActionController.hh"

#include <vector>
#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <climits>
#include <algorithm>

#include <time.h>



template <class T_class_, class T1>
class VoidTwoArgRodFunction :	public std::unary_function<const std::string, void> {
public:
  typedef void (T_class_::*VoidFunction_t )(const char *, T1 );
  
  VoidTwoArgRodFunction( T_class_ &obj, VoidFunction_t a_func, T1 arg1 ) : m_obj(&obj), m_func(a_func), m_arg1(arg1) {}
  
  void operator()(const std::string &a) const
  {
    std::cout << "Try to activate action on " << a.c_str() << "." << std::endl;
    (m_obj->*m_func)(a.c_str(), m_arg1);
  }
  
private:
  T_class_      *m_obj;
  VoidFunction_t m_func;
  T1    m_arg1;
};


int main(int argc, char **argv) {

  std::map<std::string, PixLib::EHistoQuality> quality_map;
  quality_map.insert(std::make_pair<std::string, PixLib::EHistoQuality>("Ok",PixLib::kOk));
  quality_map.insert(std::make_pair<std::string, PixLib::EHistoQuality>("ok",PixLib::kOk));
  quality_map.insert(std::make_pair<std::string, PixLib::EHistoQuality>("Failure",PixLib::kFailure));
  quality_map.insert(std::make_pair<std::string, PixLib::EHistoQuality>("failure",PixLib::kFailure));
  quality_map.insert(std::make_pair<std::string, PixLib::EHistoQuality>("StillOk",PixLib::kStillOk));
  quality_map.insert(std::make_pair<std::string, PixLib::EHistoQuality>("stillok",PixLib::kStillOk));


  std::string partition_name;
  unsigned int get_stuck_after_n_steps=UINT_MAX;
  bool get_stuck_after_n_steps_set = false;
  unsigned int fail_after_n_steps=UINT_MAX;
  bool fail_after_n_steps_set = false;
  unsigned int time_per_scan_step=0;
  bool time_per_scan_step_set=false;

  std::string histo_input_file_name;
  bool histo_input_file_name_set=false;
  
  std::vector<std::pair<std::string, PixLib::EHistoQuality> > quality_list;
  
  bool clear_histo_quality_list_set=false;

  std::vector<std::string> rod_list;
  bool error=false;
  for (int arg_i=1; arg_i<argc; arg_i++) {

    if ((strcmp(argv[arg_i],"-f")==0 || strcmp(argv[arg_i],"--fail-after-steps")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      fail_after_n_steps=atoi(argv[++arg_i]);
      fail_after_n_steps_set=true;
    }
    else if ((strcmp(argv[arg_i],"-s")==0 || strcmp(argv[arg_i],"--get-stuck-after-steps")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      get_stuck_after_n_steps=atoi(argv[++arg_i]);
      get_stuck_after_n_steps_set=true;
    }
    else if ((strcmp(argv[arg_i],"-t")==0 || strcmp(argv[arg_i],"--time-per-scan-step")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      time_per_scan_step=atoi(argv[++arg_i]);
      time_per_scan_step_set = true;
    }
    else if ((strcmp(argv[arg_i],"-i")==0 || strcmp(argv[arg_i],"--histo-input")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      histo_input_file_name=argv[++arg_i];
      histo_input_file_name_set = true;
    }
    else if ((strcmp(argv[arg_i],"-q")==0 || strcmp(argv[arg_i],"--quality")==0) && arg_i+2<argc && argv[arg_i+1][0]!='-' && argv[arg_i+2][0]!='-') {
      std::string conn_name = argv[++arg_i];
      std::string quality_name = argv[++arg_i];
      std::map<std::string,PixLib::EHistoQuality>::const_iterator quality_iter = quality_map.find(quality_name);
      if (quality_iter != quality_map.end()) {
	quality_list.push_back(std::make_pair(conn_name, quality_iter->second));
      }
      else {
	error=true;
	std::cerr << "ERROR [" << argv[0] << ":main] unknown quality tag : " << quality_name << std::endl;
      }
    }
    else if ((strcmp(argv[arg_i],"-c")==0 || strcmp(argv[arg_i],"--clear-quality-list")==0)) {
      histo_input_file_name_set=true;
    }
    else if ((strcmp(argv[arg_i],"-r")==0 || strcmp(argv[arg_i],"--rod")==0)) {
      while (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	rod_list.push_back(argv[++arg_i]);
      }
    }
    else if ((strcmp(argv[arg_i],"-p")==0 || strcmp(argv[arg_i],"--partition")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      partition_name=argv[++arg_i];
    }
    else {
      std::cerr << "ERROR [" << argv[0] << ":main] unexpected argument : " << argv[arg_i] << std::endl;
      error=true;
    }
  }

  if(error || partition_name.empty()) {
    std::cout << "USAGE: "<<argv[0] << " -p partition-name [-f/--fail-after-steps steps] [-s/--get-stuck-after-steps steps] [-r rod-name ...]"
	      << "\n\t [-i/--histo-input histo-input-file-name] [-q/--quality conn-name quality(Ok, Failure,StillOk)] [-t/--time-per-scan-stap time(ms)] "
	      << "\n\t [-c/--clear-quality-list] "
	      << std::endl;
    return EXIT_FAILURE;
  }

  if (rod_list.empty()) {
    rod_list.push_back("");
  }
  // START IPC

  // Start IPCCore
  IPCCore::init(argc, argv);

  IPCPartition partition(partition_name.c_str());

  if (!partition.isValid()) {
    std::cerr << "ERROR [" << argv[0] << ":main] Not a valid partition " << partition_name << std::endl;
    return EXIT_FAILURE;
  }

  bool comm_error=false;
  try {
    std::map<std::string,PixLib::DummyActionController_var> controller_list;
    partition.getObjects<PixLib::DummyActionController>(controller_list);

    for(std::map<std::string,PixLib::DummyActionController_var>::iterator controller_iter = controller_list.begin();
	controller_iter != controller_list.end();
	controller_iter++) {

      if(!CORBA::is_nil(controller_iter->second)) {

	if (clear_histo_quality_list_set) {
	  controller_iter->second->clearHistoQuality();
	}

	for (std::vector<std::pair<std::string, PixLib::EHistoQuality> >::const_iterator quality_iter = quality_list.begin();
	     quality_iter != quality_list.end();
	     quality_iter++) {
	  controller_iter->second->setHistoQuality(quality_iter->first.c_str(), quality_iter->second);
	}

	if (histo_input_file_name_set) {
	  controller_iter->second->setHistoInputFileName(histo_input_file_name.c_str());
	}

	if (fail_after_n_steps_set) {
	  // _objref_DummyActionController is presumably ORB dependent 
	  std::for_each(rod_list.begin(),
			rod_list.end(),
			VoidTwoArgRodFunction<PixLib::_objref_DummyActionController, CORBA::ULong > (*(controller_iter->second),
												     &PixLib::_objref_DummyActionController::failAfterNSteps,
												     fail_after_n_steps));
   	  std::cout << "INFO [" << argv[0] << ":main] set fail after to " << fail_after_n_steps << " on ROD list." <<std::endl;
	}
	if (get_stuck_after_n_steps_set) {
	  std::for_each(rod_list.begin(),
			rod_list.end(),
			VoidTwoArgRodFunction<PixLib::_objref_DummyActionController, CORBA::ULong > (*(controller_iter->second),
												     &PixLib::_objref_DummyActionController::getStuckAfterNSteps,
												     get_stuck_after_n_steps));
   	  std::cout << "INFO [" << argv[0] << ":main] set stuck after to " << get_stuck_after_n_steps << " on ROD list." <<std::endl;
	}
	if (time_per_scan_step_set) {
	  std::for_each(rod_list.begin(),
			rod_list.end(),
			VoidTwoArgRodFunction<PixLib::_objref_DummyActionController,CORBA::ULong>( (*controller_iter->second),
												  &PixLib::_objref_DummyActionController::setDuration,
												  time_per_scan_step));
   	  std::cout << "INFO [" << argv[0] << ":main] set per step scan time  to " << time_per_scan_step << " on ROD list." <<std::endl;
	}
      }
    }
  }
  catch (CORBA::TRANSIENT& err) {
    comm_error=true;
  }
  catch (CORBA::COMM_FAILURE &err) {
    comm_error=true;
  }
  catch (std::exception &err) {
    std::cerr << "ERROR [" << argv[0] << ":main] Caught standard exception " << err.what() << std::endl;
    return EXIT_FAILURE;
  }
  catch (...) {
    comm_error=true;
  }
  if (comm_error) {
    std::cerr << "ERROR [" << argv[0] << ":main] Failed to communicate with dummy action controller in partition " << partition_name << std::endl;
    return EXIT_FAILURE;
  }


}
