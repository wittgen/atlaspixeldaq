#include "EditVectorForm.h"

namespace PixCon {

  int unspace(const QString &text,int pos) {
    while (pos<text.length() && text[pos].isSpace()) ++pos;
    return pos;
  }

  int wordEnd(const QString &text, int pos)
  { 
    while (pos<text.length() && !text[pos].isSpace() && text[pos]!=',') ++pos;
    return pos; 
  }

  int parseText(const QString &text, QStringList &vector, bool integer)
  {
    vector.clear();
    int pos=unspace(text,0);
    if (pos>=text.length()) return -1;

    for(;;) {
      //     std::cout << "INFO [parseText] pos = " << pos << std::endl;

      int end=wordEnd(text,pos);
      if (pos==end) return pos;

      QString word(text.mid(pos,end-pos));

      bool ok=false;
      if (integer) {
	/*int a_int =*/ word.toInt(&ok,10);
	if (!ok) {
	  /*int a_int =*/ word.toInt(&ok,16);
	}
      }
      else {
	word.toDouble(&ok);
      }
      //  std::cout << "INFO [parseText] range = " << pos << "-" << end << " -> " << word  << " is number ? " << (ok ? "yes" : "no") << std::endl;

      if (!ok) return pos;
      vector += word;
      pos=unspace(text, end);
      if (pos<text.length() && text[pos]!=',') {
	return pos;
      }
      else {
	pos=unspace(text,pos+1);
    if (pos>=text.length()) {
	  return -1;
	}
      }
    }
  }

  std::pair<int,int> countPara(const QString &text, int end_pos)
  {
    int para=0;
    int para_pos=0;
    //    std::cout << "INFO [countPara] " << text.latin1() <<  " end = " << end_pos << std::endl;
    for (int i=0; i<end_pos; ++i) {
      //std::cout << "INFO [countPara] " << i << ":" << text[i].latin1() << " -> " << text[i].category() << std::endl;
      if (   text[i].category() == QChar::Separator_Paragraph
	     || text[i].category() == QChar::Separator_Line
	     || text[i].category() == QChar::Other_Control) {
        para_pos=0;
        ++para;
      }
      else {
	++para_pos;
      }
    }
    //    std::cout << "INFO [countPara] " << end_pos << " -> " << para  << " " << para_pos  << std::endl;
    return std::make_pair(para,para_pos);
  }

  EditVectorForm::EditVectorForm(const QString &title, const QString &text, bool integer, QWidget* parent ) 
    : QDialog(parent),
      m_origText(text),
      m_integer(integer)
  {
    setupUi(this);
    m_changeVectorFormTitle->setText(title);
    m_textEdit->setText(text);
    if (!m_integer) {
      m_baseGroup->hide();
    }
  }

  void EditVectorForm::changeBaseToDec() {
    changeBase(10,m_textEdit->toPlainText());
  }

  void EditVectorForm::changeBaseToHex() {
    changeBase(16,m_textEdit->toPlainText());
  }

  void EditVectorForm::changeBase(int base, const QString &a_text) {
    if (m_integer) {
    QStringList a_vector;
    QString out;
    int a_pos = parseText(a_text,a_vector,m_integer);
    if (a_pos==-1) {
      for(int i=0; i<a_vector.size(); i++) {
	bool ok=false;
	int a_int = a_vector[i].toInt(&ok,10);
	if (!ok) {
	  a_int = a_vector[i].toInt(&ok,16);
	}
	if (out.length()>0) {
	  out += ", ";
	}
	if (base==16 /*&& a_int>=10*/) {
	  out+="0x";
	}
	out += QString().setNum(a_int,base);
      }
      m_textEdit->setText(out);
    }
   // else {
   //   std::pair<int,int> para = countPara(a_text,a_pos);
   //   int end = unspace(a_text,wordEnd(a_text,a_pos+1));
   //   setSelection(para.first,para.second, para.first, para.second + end - a_pos);
   // }
    }
  }

  void EditVectorForm::reset() {
    changeBase(( m_hexRadio->isChecked() ? 16 : 10), m_origText);
  }
  
  QString EditVectorForm::text() const {
    return m_textEdit->toPlainText();
  }

//  void EditVectorForm::setSelection( int paraFrom, int indexFrom, int paraTo, int indexTo, int selNum  )
//  {
//    m_textEdit->setSelection(paraFrom, indexFrom, paraTo, indexTo, selNum );
//  }

  bool EditVectorForm::getVector(QStringList &new_vector)
  {
    QString a_text(text());
    int pos=parseText(a_text,new_vector, m_integer);
    if (pos==-1) {
      return true;
    }
  //  else {
  //    std::pair<int,int> para = countPara(a_text,pos);
  //    int end = unspace(a_text,wordEnd(a_text,pos+1));
  //    //       std::cout << "INFO [TableForm::editVector] set selection "
  //    // 		<< " start = " << para.first << ", " << para.second
  //    // 		<< " end="    <<  para.first << ", " << para.second + end - pos
  //    // 		<< std::endl;
  //    setSelection(para.first,para.second, para.first, para.second + end - pos);
  //  }
    return false;
  }

}
