#include "IDEPTestCommand.h"
#include "PixUtilities/PixISManager.h"
#include "ConsoleEngine.h" 
#include <ApplyDisableCommand.h>
#include <SetActiveCommand.h>
#include <CommandExecutor.h>
#include <iostream>
#include <ConfigWrapper/pixa_exception.h>
#include <ConfigWrapper/ConnectivityUtil.h>
#include <CalibrationData.h>
#include <ConfigWrapper/PixDisableUtil.h>
#include <DcsSyncCommand.h>
#include <OptoOnOffCommand.h>
#include <LvOnOffCommand.h>
#include <ScanCommand.h>


namespace PixCon {


  const std::string IDEPTestCommand::s_name("IDEP opto test");

  bool IDEPTestKit::s_registered = IDEPTestKit::registerKit();

  IDEPTestCommand::~IDEPTestCommand(){}

  PixCon::Result_t IDEPTestCommand::execute() {
    PixCon::Result_t ret;

    // Just building a sequence of other commands. The command itself does nothing.

    ret.setSuccess();
    return ret;
  }

  IDEPTestKit::IDEPTestKit() {
    m_disable.push_back("WARM_ONE");
    m_disable.push_back("WARM_TWO");
    m_disable.push_back("WARM_THREE");
    m_disable.push_back("WARM_FOUR");
  }

  PixCon::IConfigurableCommand *IDEPTestKit::createCommand(const std::string &/*command*/, const std::shared_ptr<ConsoleEngine> &engine) const {
    assert(engine.get());
     
    ConfigurableCommandSequence *command_sequence = new ConfigurableCommandSequence("IDEP opto test");
    
    PixA::ConnectivityRef conn = engine->getConn();
    PixA::ConnObjConfigDb config_db(PixA::ConnObjConfigDb::sharedInstance());
    PixA::ConnObjConfigDbTagRef config_tag(config_db.getTag(conn.tag(PixA::IConnectivity::kId),conn.tag(PixA::IConnectivity::kConfig),0));
 
    for (unsigned int imod = 0; imod < 2; imod++) {
      for (unsigned int idis = 0; idis < m_disable.size(); idis++) {

        std::shared_ptr<const PixLib::PixDisable> dis = config_tag.disablePtr(m_disable[idis]);

	command_sequence->add( DisableCommand::create(engine, dis, true /*changes only*/, DisableCommand::kApplyAll ));
	
	std::vector<std::string> module_list;
	{ // lock calibration data
	  PixCon::Lock lock( engine->calibrationData().calibrationDataMutex() );
	  CalibrationData &calibration_data = engine->calibrationData().calibrationData();
	  PixA::DisabledListRoot disabled_list_root( dis );
	  // loop over crates
	  for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin(); crate_iter != conn.crates().end(); ++crate_iter) {
	    // only consider enabled crates
	    bool crate_enabled = PixA::enabled( disabled_list_root, crate_iter );
	    if (!crate_enabled) {
	      continue;
	    }
	    // loop over RODs
	    PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
	    PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
	    for (PixA::RodList::const_iterator rod_iter = rod_begin; rod_iter != rod_end; ++rod_iter) {
	      // only consider allocated and enabled RODs
	      PixCon::CalibrationData::EnableState_t enable_state;
	      try {
		PixCon::CalibrationData::ConnObjDataList data_list( calibration_data.createConnObjDataList( PixA::connectivityName(rod_iter) ));
		enable_state = data_list.enableState(kCurrent, 0);
		if (!enable_state.isInConnectivity() || !enable_state.isInConfiguration() || !enable_state.isAllocated() || !enable_state.isParentAllocated()) {
		  continue;
		}
	      }
	      catch( PixCon::CalibrationData::MissingConnObject &) {
		continue;
	      }
	      const PixA::DisabledList rod_disabled_list( PixA::rodDisabledList( disabled_list_root, crate_iter) );
	      bool rod_enabled = PixA::enabled( rod_disabled_list, rod_iter );
	      if (!rod_enabled) {
		continue;
	      }
	      // loop over PP0s
	      PixA::Pp0List::const_iterator pp0_begin = PixA::pp0Begin(rod_iter);
	      PixA::Pp0List::const_iterator pp0_end = PixA::pp0End(rod_iter);
	      for (PixA::Pp0List::const_iterator pp0_iter = pp0_begin; pp0_iter != pp0_end; ++pp0_iter) {
		// only consider enabled PP0s
		const PixA::DisabledList pp0_disabled_list( PixA::pp0DisabledList( rod_disabled_list, rod_iter) );
		bool pp0_enabled = PixA::enabled( pp0_disabled_list, pp0_iter );
		if (!pp0_enabled) {
		  continue;
		}
		// loop over modules
		PixA::ModuleList::const_iterator module_begin = PixA::modulesBegin(pp0_iter);
		PixA::ModuleList::const_iterator module_end = PixA::modulesEnd(pp0_iter);
		for (PixA::ModuleList::const_iterator module_iter = module_begin; module_iter != module_end; ++module_iter) {
		  // only consider enabled modules
		  PixCon::CalibrationData::EnableState_t enable_state;
		  try {
		    std::string module_name = PixA::connectivityName(module_iter);
		    PixCon::CalibrationData::ConnObjDataList data_list( calibration_data.createConnObjDataList(module_name));
		    enable_state = data_list.enableState(kCurrent, 0);
		    if (enable_state.isInConfiguration() && enable_state.isInConnectivity() && enable_state.isAllocated() && enable_state.isParentAllocated()){
		      const PixA::DisabledList module_disabled_list(PixA::moduleDisabledList( pp0_disabled_list, pp0_iter) );
		      bool mod_enabled = PixA::enabled( module_disabled_list, module_iter );
		      if (!mod_enabled) {
			continue;
		      }
		      unsigned int mod_id = PixA::modId(module_iter);
		      if (mod_id % 2 == imod) {
			module_list.push_back(module_name);
		      }
		    }
		  }
		  catch( PixCon::CalibrationData::MissingConnObject &) {
		    continue;
		  }   
		}
	      }
	    }
	  }
	}
	for (std::vector<std::string>::const_iterator iname = module_list.begin(); iname != module_list.end(); ++iname) {
	  command_sequence->add( new SetActiveCommand(engine, kModuleItem, *iname, false) );
	}
	command_sequence->add( dcsSyncCommand(engine) );
	command_sequence->add( optoOnOffCommand(engine, true) );
	command_sequence->add( lvOnOffCommand(engine, true) );
	command_sequence->add( scanCommand(engine, "BOC_INLINKOUTLINK") );
	command_sequence->add( lvOnOffCommand(engine, false) );
	command_sequence->add( optoOnOffCommand(engine, false) );
      }
    }
    std::cout << "INFO [IDEPTestKit::createCommand] is CommandSequence ? " << static_cast<void *>(dynamic_cast<CommandSequence *>(command_sequence))
	      << std::endl;
    return command_sequence;
  }
  

}
