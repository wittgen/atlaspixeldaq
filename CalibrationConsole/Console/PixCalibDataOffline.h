/**
 * @file PixCalibDataOffline.h
 * @brief Extract pixel calibration data for offline use and transfer to COOL
 * @author Tayfun Ince <tayfun.ince@cern.ch>
 * @date 2011/02/01
 */

#ifndef _PixCon_PixCalibDataOffline_H_
#define _PixCon_PixCalibDataOffline_H_

#include "ui_PixCalibDataOfflineBase.h"

#include <string>

namespace PixCon {

  class PixCalibDataOffline : public QDialog, public Ui_PixCalibDataOfflineBase
  {
    Q_OBJECT

  public:
    /** Constructor */
    PixCalibDataOffline(QWidget* parent = 0);
    
    /** Destructor */
    ~PixCalibDataOffline();

    bool isAthenaSetup();

  public slots:
    void extractAnalysis();
    void createSQLiteFile();
    void transferToCOOL();

  private:
    std::string m_pcdDir;
    std::string m_pcdName;
    std::string m_dbName;
    unsigned int m_nOnlineTags;
  };

}
#endif
