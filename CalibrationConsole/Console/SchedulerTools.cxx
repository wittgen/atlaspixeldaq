#include <ipc/partition.h>
#include <Scheduler/Scheduler.hh>
#include <Common.hh>

#include <exception.hh>


//#include <Scheduler_ref.hh>

#include "ConsoleEngine.h"

#include <ICommand.h>
#include "CommandKits.h"

namespace PixCon {


  class CANCommandBase : public PixCon::IConfigurableCommand
  {

  public:

    CANCommandBase(const std::shared_ptr<ConsoleEngine> &engine) : m_engine(engine) {}
      //    ~CANCommandBase();

    PixCon::Result_t execute(IStatusMessageListener &listener);

    void resetAbort() { /*intentionally does nothing */ }
    void abort()      { /*intentionally does nothing */ }
    void kill()       { /*intentionally does nothing */ }

    bool configure() { return true; }
    bool hasDefaults() const { return true; }

    static bool registerKits();
  protected:

    virtual void action(CAN::Scheduler_var &scheduler) = 0;
  private:
    std::shared_ptr<ConsoleEngine> m_engine;
    static bool s_registered;

  };


  /** Helper class to cleanup and set return status upon e.g. exceptions.
   */
  class ReturnFailedHelper {
  public:
    ReturnFailedHelper(Result_t &result)
      : m_result(&result)
    {}

    ~ReturnFailedHelper() {
      if (m_result) {
	m_result->setFailure();
      }
    }

    void success() {
      if (m_result) {
	m_result->setSuccess();
	m_result=NULL;
      }
    }
  private:
    Result_t       *m_result;
  };

  PixCon::Result_t CANCommandBase::execute(IStatusMessageListener &) {

    bool scheduler_comm_error=false;

    PixCon::Result_t ret;

    IPCPartition &the_partition = m_engine->partition();
    if (!the_partition.isValid()) {
      std::cerr << "ERROR [AnalysisCommand::submitAnalysis] No valid partition." << std::endl;
      ret.setFailure();
      return ret;
    }

    try {
      ReturnFailedHelper return_helper(ret);

      // get CORBA reference to the scheduler
      // Should throw an exception if the scheduler does not exist in the 

      std::string manitu_name = (the_partition.name()+CAN::s_schedulerName);
      if (the_partition.isObjectValid<CAN::Scheduler>(manitu_name)) {
	CAN::Scheduler_var  manitu = the_partition.lookup<CAN::Scheduler>(manitu_name);
	action(manitu);

	return_helper.success();
      }

    }
    catch (CAN::MRSException &err) {
      std::cerr << "ERROR [AnalysisCommand::submitAnalysis] Caught exception " << err.getDescriptor() << std::endl;
      scheduler_comm_error=true;
    }
    catch (CORBA::TRANSIENT& err) {
      scheduler_comm_error=true;
    }
    catch (CORBA::COMM_FAILURE &err) {
      scheduler_comm_error=true;
    }
    catch (std::exception &err) {
      std::cerr << "ERROR [AnalysisCommand::submitAnalysis] Caught std exception " << err.what() << std::endl;
      scheduler_comm_error=true;
    }
    catch (...) {
      scheduler_comm_error=true;
    }

    if (scheduler_comm_error) {
      std::cerr << "ERROR [AnalysisCommand::submitAnalysis] Communication error with scheduler." << std::endl;
    }
    return ret;
  }

  class SchedulerResetQueues : public CANCommandBase
  {
  public:
    SchedulerResetQueues(const std::shared_ptr<ConsoleEngine> &engine) : CANCommandBase(engine) {}

    void action(CAN::Scheduler_var &manitu) {
      manitu->reset();
    }

    class Kit : public PixCon::ICommandKit
    {
    public:
      PixCon::IConfigurableCommand *createCommand(const std::string &/*command*/, const std::shared_ptr<ConsoleEngine> &engine) const {
	return new SchedulerResetQueues(engine);
      }

      static const std::string &name() { return s_name; }
    private:
      static std::string s_name;
    };

    const std::string &name() const { return Kit::name(); }

  };

  class SchedulerRestartWorker : public CANCommandBase
  {
  public:

    SchedulerRestartWorker(const std::shared_ptr<ConsoleEngine> &engine) : CANCommandBase(engine) {}

    void action(CAN::Scheduler_var &manitu) {
      manitu->restartWorker();
    }

    class Kit : public PixCon::ICommandKit
    {
    public:
      PixCon::IConfigurableCommand *createCommand(const std::string &/*command*/, const std::shared_ptr<ConsoleEngine> &engine) const {
	return new SchedulerRestartWorker(engine);
      }
      static const std::string &name() { return s_name; }
    private:
      static std::string s_name;
    };

    const std::string &name() const { return Kit::name(); }

  };

  class SchedulerSchutdown : public CANCommandBase
  {
  public:

    SchedulerSchutdown(const std::shared_ptr<ConsoleEngine> &engine) : CANCommandBase(engine) {}

    void action(CAN::Scheduler_var &manitu) {
      manitu->shutdown();
    }

    class Kit : public PixCon::ICommandKit
    {
    public:
      PixCon::IConfigurableCommand *createCommand(const std::string &/*command*/, const std::shared_ptr<ConsoleEngine> &engine) const {
	return new SchedulerSchutdown(engine);
      }
      static const std::string &name() { return s_name; }
    private:
      static std::string s_name;
    };

    const std::string &name() const { return Kit::name(); }

  };

  std::string SchedulerResetQueues::Kit::s_name("Reset Analysis Queues");
  std::string SchedulerRestartWorker::Kit::s_name( "Restart Analysis Worker");
  std::string SchedulerSchutdown::Kit::s_name("Shutdown Analysis Infrastructure");

  bool CANCommandBase::registerKits() {
    CommandKits::registerKit(SchedulerResetQueues::Kit::name(),new SchedulerResetQueues::Kit, PixCon::UserMode::kExpert);
    CommandKits::registerKit(SchedulerRestartWorker::Kit::name(),new SchedulerRestartWorker::Kit, PixCon::UserMode::kExpert);
    CommandKits::registerKit(SchedulerSchutdown::Kit::name(),new SchedulerSchutdown::Kit, PixCon::UserMode::kExpert);
    return true;
  }

  bool CANCommandBase::s_registered = CANCommandBase::registerKits();

}
