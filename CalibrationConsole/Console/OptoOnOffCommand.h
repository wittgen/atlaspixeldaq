#ifndef _PixCon_OptoOnOffCommand_h_
#define _PixCon_OptoOnOffCommand_h_

#include "CommandKits.h"
#include <memory>

namespace PixCon {

  class ConsoleEngine;
  
  ICommand *optoOnOffCommand(const std::shared_ptr<ConsoleEngine> &engine, bool on);

}

#endif
