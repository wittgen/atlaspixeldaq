#include "test_DummyActionController_impl.h"
#include "test_DummyMultiAction.h"
#include "test_DummyHistoInput.h"

#include <unistd.h>
#include <cerrno>

namespace PixLib {

  DummyActionController_impl *DummyActionController_impl::s_instance=NULL;
  std::string DummyActionController_impl::s_partitionName;

  std::string DummyActionController_impl::makeName(const std::string &ipc_name) {
    std::string temp;
    temp.resize(128);
    while (gethostname(&(temp[0]), temp.size())!=0 && (errno == ENAMETOOLONG || errno == EINVAL) && temp.size()<4096) {
      temp.resize(temp.size()*2);
    }
    std::string::size_type end_pos = strlen(temp.c_str());
    temp.erase(end_pos,temp.size()-end_pos);
    std::stringstream pid_string;
    pid_string << getpid();

    return ipc_name + "_DummyActionController_" + temp + "_" + pid_string.str();
  }

  DummyActionController_impl::DummyActionController_impl(IPCPartition &ipc_partition)
    : IPCNamedObject<POA_PixLib::DummyActionController,ipc::single_thread>(ipc_partition,makeName(ipc_partition.name()))
  {
    publish();
  }

  /** Set the time needed to perform a scan.
   * @param rod_name the connectivity name of the ROD
   * @param needed_time the number of milliseconds the method will sleep.
   */
  void DummyActionController_impl::setDuration( const char *rod_name, CORBA::ULong needed_time) {

    for_each( rod_name , VoidOneArgFunction<DummySingleAction,CORBA::ULong>(&DummySingleAction::setDuration, needed_time) );

  }

  /** Set whether the scan will fail or succeed for the given ROD.
   * @param rod_name the connectivity name of the ROD
   * @param fail if set to true the ROD will fail.
   */
  void DummyActionController_impl::failAfterNSteps( const char *rod_name, CORBA::ULong fail_after_n_steps )
  {

    for_each( rod_name , VoidOneArgFunction<DummySingleAction,CORBA::ULong>(&DummySingleAction::failAfterNSteps, fail_after_n_steps) );

  }

  void DummyActionController_impl::getStuckAfterNSteps( const char *rod_name, CORBA::ULong n_steps )
  {
    for_each( rod_name , VoidOneArgFunction<DummySingleAction,CORBA::ULong>(&DummySingleAction::getStuckAfterNSteps, n_steps) );
  }


  void DummyActionController_impl::setHistoInputFileName(const char *file_name) {
    for_each( "" , VoidOneArgFunction<DummySingleAction,const char *>(&DummySingleAction::setHistoInputFileName, file_name) );
  }

  void DummyActionController_impl::setHistoQuality(const char *conn_name, EHistoQuality idl_quality) {
    PixCon::DummyHistoInput::EQuality quality = static_cast<PixCon::DummyHistoInput::EQuality>(idl_quality);
    for_each( "" , VoidTwoArgFunction<DummySingleAction,const char *, PixCon::DummyHistoInput::EQuality>(&DummySingleAction::setHistoQuality, conn_name, quality) );
  }

  void DummyActionController_impl::clearHistoQuality() {
    for_each( "" , VoidZeroArgFunction<DummySingleAction>(&DummySingleAction::clearHistoQuality) );
  }


  bool DummyActionController_impl::registerAction( const std::string &rod_name, const std::shared_ptr<PixActions> &action )
  {
    if (!dynamic_cast< DummySingleAction *>(action.get())) {
      std::cout << "ERROR [DummyActionController_impl::registerAction] Not registering action for ROD " << rod_name
	<< ". Only can deal with dummy single actions." << std::endl;
      return false;
    }

    std::pair<std::map<std::string, std::shared_ptr<PixActions> >::iterator,bool> ret
      = m_actionList.insert( std::make_pair(rod_name, action) );

    if (!ret.second) {
      std::cout << "ERROR [DummyActionController_impl::registerAction] Failed to register action for ROD " << rod_name << "." ;
      if (m_actionList.find(rod_name) != m_actionList.end()) {
	std::cout << " There is already an action registered for that ROD." << std::endl;
      }
    }
    return ret.second;
  }

  bool DummyActionController_impl::deregisterAction( const std::string &rod_name) {
    std::map<std::string, std::shared_ptr<PixActions> >::iterator rod_iter = m_actionList.find(rod_name);
    if (rod_iter != m_actionList.end()) {
      std::cout << "INFO [DummyActionController_impl::deregisterAction] Deregister action for for ROD " << rod_name << "."
		<< std::endl;
      m_actionList.erase(rod_iter);
      return true;
    }
    else {
      std::cout << "ERROR [DummyActionController_impl::deregisterAction] No action registered for for ROD " << rod_name 
		<< ". Nothing deregistered."
		<< std::endl;
      return false;
    }
  }

  void DummyActionController_impl::shutdown() {}

}
