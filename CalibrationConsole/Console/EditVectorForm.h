/* Dear emacs, this is -*-c++-*- */
#ifndef _EditVectorForm_H_
#define _EditVectorForm_H_

#include "ui_EditVectorFormBase.h"
#include <qstring.h>

namespace PixCon {
  class EditVectorForm : public QDialog, public Ui_EditVectorFormBase
  {    Q_OBJECT
  public:
    EditVectorForm(const QString &title, const QString &text, bool integer=true, QWidget* parent = 0 ) ;

   

    QString text() const;

  //  void setSelection( int paraFrom, int indexFrom, int paraTo, int indexTo, int selNum = 0 );

 

    void changeBase(int base, const QString &text);

    bool getVector(QStringList &new_vector);

public slots:
 void reset();
void changeBaseToDec();

    void changeBaseToHex();


  private:
    QString m_origText;
    bool m_integer;
  };

}

#endif
