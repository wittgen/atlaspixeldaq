#include <ipc/partition.h>
#include <ipc/core.h>
#include <PixActions/PixActions.h>

#include <ConfigWrapper/pixa_exception.h>

#include <map>
#include <string>
#include <iomanip>


int main(int argc, char **argv)
{
  std::string partition_name;
  std::string is_name = "CAN";
  std::string id_tag="CT";
  std::string tags="CT-C1OSP-V3";
  std::string cfg_tag="CT-C12OSP";
  std::string broker_extra_name="";
  std::vector<std::string> rod_names;
  bool verbose=false;
  bool error=false;
  bool cleanup=false;
  bool list_tags=false;

  for (int arg_i=1; arg_i<argc; arg_i++) {
    if (strcmp(argv[arg_i],"-p")==0 && arg_i+1<argc) {
      partition_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-t")==0 ) {
      list_tags=true;
    }
    else if (strcmp(argv[arg_i],"-v")==0 ) {
      verbose=true;
    }
    else {
      error=true;
    }
  }

  if (error || is_name.empty() || partition_name.empty()) {
    std::cout << "usage " << argv[0] << " -p partition [-t] [-v]" << std::endl;
  }
  // Start IPCCore
  IPCCore::init(argc, argv);
  
  IPCPartition partition(partition_name);

  try {
    std::map<std::string,ipc::PixActions_var> actions;
    partition.getObjects<ipc::PixActions>(actions);
    for(std::map<std::string,ipc::PixActions_var>::iterator action_iter = actions.begin();
	action_iter != actions.end();
	action_iter++) {
      if (verbose) {
	std::cout << "INFO [" << argv[0] << "main]" << action_iter->first << std::endl;
      }
      if(!CORBA::is_nil(action_iter->second)) {
	CORBA::String_var name = action_iter->second->ipc_name();
	CORBA::String_var manager_name = action_iter->second->ipc_managingBrokerName();
	CORBA::String_var allocator_name = action_iter->second->ipc_allocatingBrokerName();
	CORBA::Long type = action_iter->second->ipc_type();
	
	bool available = action_iter->second->ipc_available();
	std::string type_name;
	switch (type) {
	case PixLib::PixActions::ANY_TYPE: {
	  type_name="any";
	  break;
	}
	case PixLib::PixActions::SINGLE_ROD: {
	  type_name="single ROD";
	  break;
	}
	case PixLib::PixActions::SINGLE_TIM: {
	  type_name="single TIM";
	  break;
	}
	case PixLib::PixActions::MULTI: {
	  type_name="multi";
	  break;
	}
	default: {
	  type_name="unknown";
	  break;
	}
	}

	std::string tag_list;
	if (list_tags) {
	  CORBA::String_var conn_tag = action_iter->second->ipc_getConnTagC(name);
	  CORBA::String_var payload_tag = action_iter->second->ipc_getConnTagP(name);
	  CORBA::String_var alias_tag   = action_iter->second->ipc_getConnTagA(name);
	  CORBA::String_var id_tag      = action_iter->second->ipc_getConnTagOI(name);
	  CORBA::String_var cfg_tag     = action_iter->second->ipc_getCfgTag(name);
	  
	  if (id_tag) {
	    tag_list = id_tag;
	  }
	  tag_list += ":";
	  if (conn_tag) {
	    tag_list += conn_tag;
	  }
	  if ((!conn_tag && payload_tag) || (conn_tag && payload_tag && strcmp(conn_tag,payload_tag)!=0)) {
	    tag_list +=  ", ";
	    tag_list += alias_tag;
	  }
	  if ((!conn_tag && alias_tag) || (conn_tag && alias_tag && strcmp(conn_tag,alias_tag)!=0)) {
	    tag_list += ", ";
	    tag_list += alias_tag;
	  }
	  if (cfg_tag) {
	    tag_list += ";";
	    tag_list += cfg_tag;
	  }
	}


	std::cout << std::setw(40) << action_iter->first 
		  << ( available ? " available " : " allocated ")
		  << std::setw(12) << type_name << "(" << type << ")"
		  << " : name ="  << name
		  << ", manager = " <<manager_name
		  << ", allocator = " << allocator_name;
	if (!tag_list.empty()) {
	  std::cout << " tags:" << tag_list;
	}
	std::cout << std::endl;
      }
    }
  }
  catch(PixA::BaseException &err) {
    // TODO: signal error if some action cannot be retrieved
    std::cerr << "FATAL [main:" << argv[0] << "] Caught base exception : " << err.getDescriptor()  << std::endl;
    return -1;
  }
  catch (CORBA::TRANSIENT& err) {
    std::cerr << "FATAL [main:" << argv[0] << "] Caught CORBA exception (COMM_TRANSIENT)." << std::endl;
  }
  catch (CORBA::COMM_FAILURE &err) {
    std::cerr << "FATAL [main:" << argv[0] << "] Caught CORBA exception (COMM_FAILURE)." << std::endl;
  }
  catch(std::exception &err) {
    // TODO: signal error if some action cannot be retrieved
    std::cerr << "FATAL [main:" << argv[0] << "] Caught exception : " << err.what()  << std::endl;
    return -1;
  }
  catch(...) {
    // TODO: signal error if some action cannot be retrieved
    std::cerr << "FATAL [main:" << argv[0] << "] Caught unknown exception." << std::endl;
    return -1;
  }
  return 0;
}

