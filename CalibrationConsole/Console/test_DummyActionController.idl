#include <ipc/ipc.idl>

module PixLib {

  const string s_controllerName = "_dummyActionController";

  enum EHistoQuality {kOk, kStillOk, kFailure, kNQualityLevels};

  interface DummyActionController : ipc::servant {

    /** Set the time needed to perform a scan.
     * @param rod_name the connectivity name of the ROD
     * @param needed_time the number of milliseconds the method will sleep.
     */
    void setDuration( in string rod_name, in unsigned long needed_time);

    /** Mark the scan failing after this number of steps for the given ROD.
     * @param rod_name the connectivity name of the ROD.
     * @param n_steps number of steps after which the scan will fail.
     */
    void failAfterNSteps( in string rod_name, in unsigned long fail_after_n_steps );

    /** Mark the scan for the the given ROD to stop progresing after this number of steps.
     * @param rod_name the connectivity name of the ROD.
     * @param n_steps number of steps after which the scan will stop progressing.
     */
    void getStuckAfterNSteps( in string rod_name, in unsigned long n_steps);

    /** Set a new file for retrieving dummy histograms.
     * @param file_name the name of the file which contains histograms to be publishded by the acion.
     */
    void setHistoInputFileName(in string file_name);

    /** Set the quality tag of the histograms to be published for the given connectivity object.
     * @param conn_name the name of the connectivity object (module, PP0, ROD) for which the quality should be set.
     * @param quality the new quality tag.
     */
    void setHistoQuality(in string conn_name, in EHistoQuality quality);

    /** Use the quality tag kOk for all connectivity objects.
     */
    void clearHistoQuality();

  };
};
