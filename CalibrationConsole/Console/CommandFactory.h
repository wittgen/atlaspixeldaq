#ifndef _PixCon_CommandFactory_h_
#define _PixCon_CommandFactory_h_

#include "ICommandFactory.h"
#include "CommandKits.h"
#include "UserMode.h"

namespace PixCon {

  class ConsoleEngine;

  class CommandFactory  : public PixCon::ICommandFactory
  {
  public:
    CommandFactory(std::shared_ptr<ConsoleEngine> &engine) : m_engine(engine) {}

    unsigned int nCommands() const                                         { return CommandKits::instance()->nCommands(); }
    const std::string &commandName( unsigned int command_id ) const        { return CommandKits::instance()->commandName(command_id); }
    const PixCon::UserMode::EUserMode commandUser(unsigned int command_id) const { return CommandKits::instance()->commandUser(command_id); }

    unsigned int nClassifications() const                                  { return CommandKits::instance()->nClassifications(); }
    const std::string &classificationName( unsigned int command_id ) const { return CommandKits::instance()->classificationName( command_id ); }


    PixCon::ICommand *createCommand(const std::string &command_name) const {
      const ICommandKit *kit= CommandKits::instance()->commandKit(command_name);
      if (!kit) {
	throw std::runtime_error("Command does not exist.");
      }
      std::unique_ptr<ICommand>  a_command( kit->createCommand(command_name, m_engine) );
      if (!configureCommand(a_command.get())) return NULL;
      return a_command.release();
    }

    static ICommand *configureCommand(ICommand *a_command) {

      IConfigurableCommand *a_configurable_command = dynamic_cast<IConfigurableCommand *>( a_command );
      if (a_configurable_command && !a_configurable_command->hasDefaults()) {
	if (!a_configurable_command->configure()) {
	  return NULL;
	}
      }
      return a_command;
    }


    PixCon::IClassification *createClassification(const std::string &command_name) const {
      const IClassificationKit *kit = CommandKits::instance()->classificationKit(command_name);
      if ( !kit) {
	throw std::runtime_error("Classification does not exist.");
      }
      return kit->createClassification(command_name, m_engine);
    }
  private:
    std::shared_ptr<ConsoleEngine> m_engine;
  };

}
#endif
