#ifndef _PixCon_PixScanPanel_h_
#define _PixCon_PixScanPanel_h_

#include "ui_PixScanPanelBase.h"
#include <map>
#include <string>
#include <qlayout.h>
#include <qpushbutton.h>
#include <QLabel>
#include <QTreeWidget>

#include <ConfigWrapper/ConfigHandle.h>

class QWidget;
class QComboBox;
class QCheckBox;
class QSpinBox;
//class Q3ListBox;
class QListWidget;
class QLabel;

namespace PixLib{
  class PixScan;
}

namespace PixCon {

  class PixScanPanel;
  class TypeComboTableItem;

  class FEMaskButton :  public QPushButton {
    Q_OBJECT
    public:
    FEMaskButton(QWidget *parent, PixScanPanel *panel, unsigned int fe_i);
  public slots:
    virtual void feMaskToggled(bool status);
  protected:
    PixScanPanel *m_scanPanel;
    unsigned int m_fe;
  };


  class PixScanPanel : public QWidget, public Ui_PixScanPanelBase
  {

    Q_OBJECT
    friend class FEMaskButton;
  public:
    PixScanPanel( PixA::ConfigHandle &config, const std::string &title, QWidget* parent = 0);
    ~PixScanPanel();
    std::unique_ptr<PixLib::PixScan> m_pixScan;
    void init();

  public slots:
    void updateConfig();
    void updateConfigHandlers();
    /* creates a new PixScan object for user-config */

    //    void loadConfig();
    //    void clearConfig();
    // void saveConfig();
    void readFromHandles();
    void readFromHandlersFromTable(int row, int);
    void updateLoopPts(unsigned int loop_i);
    void updateLoopSettings(unsigned int loop_i);
    void loadLoopPts(unsigned int loop_i);
    void loadLoop0Pts(){loadLoopPts(0);}
    void loadLoop1Pts(){loadLoopPts(1);}
    void loadLoop2Pts(){loadLoopPts(2);}
    void loadTuningPoints();

    void singleFEModeToggled();
    void showLoop(int);

    // sets VCAL to the "magic" value to leave it unchanged during a scan
    void fixVCAL(bool);
    // if VCAL is set to this value manually, translate accordingly
    void VcalChanged(int);
    // right-click on a config table item: only works for combo-boxes in last column
    void openTableMenu(const QPoint &);
    void editVector(const QPoint &);

    //   void setScanCfg(Config &cfg_in) {
    //     m_scanCfg->initConfig();
    //     m_scanCfg->config() = cfg_in;
    //     m_scanCfg->resetScan();
    //   };	

//    PixA::ConfigHandle &scanConfig() {
//      return m_scanConfig;
//    };
    PixLib::Config &config();

    virtual void test(int value);

  signals:

  private:

    static const unsigned int MAX_LOOPS=3;
    PixA::ConfigHandle m_scanConfig;

    std::map<std::string,QObject*> m_knownHandles;
    std::vector<QString> m_scanLabels;
    // translates individual loop_i variable names to arrays
    QComboBox* loopType[MAX_LOOPS];
    QComboBox* loopEndAction[MAX_LOOPS];
    QCheckBox* loopActive[MAX_LOOPS];
    QCheckBox* loopDspProc[MAX_LOOPS];
    QCheckBox* loopRegular[MAX_LOOPS];
    QCheckBox* loopFree[MAX_LOOPS];
    QCheckBox* loopOnDsp[MAX_LOOPS];
    QSpinBox*  loopStart[MAX_LOOPS];
    QSpinBox*  loopStop[MAX_LOOPS];
    QSpinBox*  loopStep[MAX_LOOPS];
    //Q3ListBox*  loopPtsBox[MAX_LOOPS];
    QListWidget* loopPtsBox[MAX_LOOPS];//
    QPushButton* loopLoadPoints[MAX_LOOPS];
    QLabel*    loopFromLabel[MAX_LOOPS];
    QLabel*    loopToLabel[MAX_LOOPS];
    QLabel*    loopStepsLabel[MAX_LOOPS];
    QLabel*    loopPtlLabel[MAX_LOOPS];
    QLabel*    loopPostActLabel[MAX_LOOPS];
    QWidget* LoopBox[MAX_LOOPS];

    QLabel*    fillingLabel;


    FEMaskButton   *m_feMaskButton[16];
  protected:
    void feMaskToggled(bool new_state_i, int fe_i);
   
  public slots:
    void feMaskAllEnabled();
    void feMaskAllDisabled();
    

  };

 class TypeComboTableItem : public QComboBox
  {
  public:
    enum EType {kInt, kFloat};

    TypeComboTableItem(EType type, QTableWidget *table, const QStringList &string_list, bool editable = false) : QComboBox(table), m_type(type)
    {addItems(string_list);
     setEditable(editable);
    }
    EType type() const { return m_type;}
  private:
    EType m_type;
  };


}
#endif // PIXSCANPANEL

