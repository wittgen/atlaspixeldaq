#include "ConsoleApp.h"

#include <CalibrationDataManager.h>
#include <CalibrationDataStyle.h>
#include <CalibrationConsoleContextMenu.h>
#include <CategoryList.h>

#include <HistogramProviderInterceptor.h>
#include <HistogramCache.h>
#include <OHHistogramProvider.h>
#include <PixHistoServerInterfaceProvider.h>
#include <MetaHistogramProvider.h>
#include <ScanFileHistogramProvider.h>

#include "CoralClientMetaDataUpdateHelper.h"

#include "StatusChangeTransceiver.h"

#include "ConsoleViewer.h"
#include "PartWin.h"
#include "ConsoleEngine.h"
#include "ConnectivityPanel.h"

#include "qmessagebox.h"

#include <PixNameServerIDL/PixNameServerIDL.hh>

#include <ConfigWrapper/createDbServer.h>

#include <ModuleMapH2Factory.h>
#include <BocEditor.h>
#include "DontPanelStandby.h"

std::string convertFromLatin1(const char *latin1) {
  std::string temp;
  if (latin1) temp=latin1;
  return temp;
}
const char *ConsoleApp::s_pixDbServerName="PixelDbServer";

ConsoleApp::ConsoleApp(std::shared_ptr<PixCon::UserMode>& user_mode,
		       const std::string &partition_name,
		       const std::string &is_server_name,
		       const std::string &oh_server_name,
		       const std::string &histo_name_server_name,
		       std::shared_ptr<PixLib::PixDbServerInterface> db_server,
		       const std::shared_ptr<PixA::IModuleMapH2Factory> &map_factory,
		       const std::string &ddc_partition_name,
		       const std::string &ddc_is_name,
		       int &qt_argc,
		       char **qt_argv,
		       int poll)
  : QRootApplication(qt_argc, qt_argv, poll),
    m_categories(new PixCon::CategoryList),
    m_calibrationData(new PixCon::CalibrationDataManager),
    m_userMode(user_mode)
{
  bool verbose=false;

  if (m_userMode->getUserMode() == PixCon::UserMode::kExpert) {
    std::cout << "INFO [ConsoleApp::ctor] Setting up the console for an expert user" << std::endl;
  }
  else {
    std::cout << "INFO [ConsoleApp::ctor] Setting up the console for a shifter user" << std::endl;
  }

  // first ask the user to choose a partition (the same for IS and OH) and the IS and OH server names, then create the necessary objects to construct the main panel

  std::string PartName = partition_name;
  std::string ISName = is_server_name;
  std::string OHName = oh_server_name;
  PixCon::PartWin pw(*this,0);
  pw.setPartitionName(partition_name, "PixelInfr");
  pw.setOHServerName(oh_server_name, "pixel_histo_server_PixNameServerInternal");
  pw.setISServerName(is_server_name, "RunParams");

  pw.textLabel1->setText("Please select the partition, IS and OH server names for IPC set-up:");
  pw.exec();

  if (pw.partBox->currentText()!=""){
    PartName = convertFromLatin1(pw.partBox->currentText().toLatin1().data());
    ISName = convertFromLatin1(pw.issrvBox->currentText().toLatin1().data());
    OHName = convertFromLatin1(pw.ohsrvBox->currentText().toLatin1().data());
  }
  else {
    throw std::runtime_error("No partition chosen, stopping");
  }

  if (PartName.empty() || ISName.empty() || OHName.empty()) {
    throw std::runtime_error("Missing partition, IS or OH name");
  }

//   PartName = "Partition_PixelInfrastructure_lmasetti";
//   ISName = "RunParams";
//   OHName = "Histogramming";

  m_partition = std::make_unique<IPCPartition>(PartName);
  //IPCPartition *the_oh_partition=m_partition.get();

  std::string the_histo_name_server_name(histo_name_server_name);
  if (the_histo_name_server_name.empty())
  {
    std::cout << "INFO [ConsoleApp::ctor] Search for the Pixel histo name server." << std::endl;
    std::map< std::string, ipc::PixNameServer_var > objects;
    m_partition->getObjects<ipc::PixNameServer>( objects );
    if (objects.begin() != objects.end()) {
      the_histo_name_server_name = objects.begin()->first;
      std::cout << "INFO [ConsoleApp::ctor] found a  Pixel histo name server named = "  << the_histo_name_server_name << std::endl;
    }
    if (the_histo_name_server_name.empty()) {
      if (QMessageBox::warning(NULL,
			       "No histogram name server given !",
			       "Without the histogram name server histograms will >not< automatically be archived from OH.  ",
			       "Continue without","Exit") == 1) {
	throw std::runtime_error("No histogram name server given!");
      }
    }

  }

  if (!db_server.get())
  {
    std::cout << "INFO [ConsoleApp::ctor] Search for the DbServer ." << std::endl;
    std::map< std::string, ipc::PixDbServerInterface_var > objects;
    m_partition->getObjects<ipc::PixDbServerInterface>( objects );
    std::string db_server_name = s_pixDbServerName;
    std::map< std::string, ipc::PixDbServerInterface_var >::iterator normal_db_server = objects.find(db_server_name);
    if (normal_db_server == objects.end()) {
      if (objects.begin() != objects.end()) {
	db_server_name = objects.begin()->first;
      }
      else {
	db_server_name.clear();
      }
    }
    if (db_server_name.empty()) {
      if (QMessageBox::warning(NULL,
			       "No db server given and no db server found in the given partition!",
			       "Without the db server no configigurations or disable objects can be written.  ",
			       "Continue without","Exit") == 1) {
	throw std::runtime_error("No db server given!");
      }
    }
    else {
      std::cout << "INFO [ConsoleApp::ctor] Found the db server " << db_server_name << " in partition "
		<< m_partition->name() << ". Will use it." << std::endl;
      db_server = std::shared_ptr<PixLib::PixDbServerInterface>( PixA::createDbServer(*m_partition,
											db_server_name ));
    }
  }

  if (db_server.get()) {
    PixA::ConnectivityManager::useDbServer( db_server );
  }
  else {
    std::cout << "WARNING [ConsoleApp::ctor] No DB server found. Will not be able to write configurations and disable information." << std::endl;
  }


  std::shared_ptr<PixCon::OHHistogramProviderBase> 
    oh_histogram_provider( ( !the_histo_name_server_name.empty() ?
			     static_cast<PixCon::OHHistogramProviderBase *>(new PixCon::PixHistoServerInterfaceProvider(*(new IPCPartition(PartName)),
															 OHName,
															 the_histo_name_server_name,
															 verbose) )
			     : static_cast<PixCon::OHHistogramProviderBase *>(new PixCon::OHHistogramProvider(*(new IPCPartition(PartName)), OHName,verbose )) ) );

  std::shared_ptr<PixCon::IHistogramProvider> root_file_histogram_provider(new PixCon::ScanFileHistogramProvider(m_calibrationData));

  auto meta_histogram_provider=std::make_shared<PixCon::MetaHistogramProvider>(oh_histogram_provider,
								      root_file_histogram_provider);

  m_histogramProvider=meta_histogram_provider;
  m_histogramProviderInterceptor=m_histogramProvider;

  std::shared_ptr<PixCon::HistogramCache> histogram_cache( new PixCon::HistogramCache( m_histogramProviderInterceptor ) );

  std::shared_ptr<PixCon::CalibrationDataPalette> palette(new PixCon::CalibrationDataPalette);
  palette->setDefaultPalette();
  std::shared_ptr<PixCon::VisualiserList> visualiser_list(new PixCon::VisualiserList);
  std::shared_ptr<PixCon::IContextMenu> context_menu(  new PixCon::CalibrationConsoleContextMenu(m_calibrationData, histogram_cache, visualiser_list, PartName) );

  std::shared_ptr<PixCon::IMetaDataUpdateHelper> meta_data_update_helper(new PixCon::CoralClientMetaDataUpdateHelper(m_calibrationData));



  // now create the Engine with the chosen partition
  std::shared_ptr<PixCon::StatusChangeTransceiver> status_change_transceiver( new PixCon::StatusChangeTransceiver(m_calibrationData) );
  std::shared_ptr<PixCon::ConsoleEngine> the_engine(new PixCon::ConsoleEngine(m_partition.get(), ISName, the_histo_name_server_name, OHName,
										m_calibrationData, status_change_transceiver,
										m_userMode->getUserMode()) );

  // choose RODs to allocate via the ConnectivityPanel and update calibration data

  PixCon::ConnectivityPanel cp(m_userMode->getUserMode(), *the_engine, this);
  std::shared_ptr<const PixLib::PixDisable> dis;
  cp.insertCrates();
  if(cp.exec()==QDialog::Accepted){
    bool isSafe = true;
    if (m_userMode->getUserMode() != PixCon::UserMode::kExpert) {
      std::string message = "You started console in non expert mode. Make sure to switch on HV before running ANALOG, THRESHOLD or any TUNING.\n Do you want to continue?";
    int answer = QMessageBox::question(nullptr, "CONSOLE IN USER MODE", QString::fromStdString(message), QMessageBox::No, QMessageBox::Yes);

      if (answer != QMessageBox::Yes)isSafe = false;
   } else {
      DontPanelStandby notifyExpert;
      if (notifyExpert.exec() == QDialog::Accepted) isSafe = true;
      else isSafe = false;
    }
    if (isSafe) {
      std::map<std::string, std::vector<std::string> > rodList;
      cp.allocate(rodList);
      dis = cp.getDisable();
      if (dis.get() != NULL) {
	the_engine->setTmpDis(dis);
      }
    }
  }
  
  PixA::ModuleMapH2Factory *module_map_factory = dynamic_cast<PixA::ModuleMapH2Factory *>(map_factory.get());
  std::shared_ptr<PixCon::IBocEditor> boc_editor;
  if (module_map_factory) {
    boc_editor = std::shared_ptr<PixCon::IBocEditor>(new PixCon::BocEditor(m_calibrationData) );
    module_map_factory->setBocEditor(boc_editor);
  }
  m_viewer = std::make_unique<PixCon::ConsoleViewer>( user_mode, 
									      *m_partition,
									      ISName,
									      ddc_partition_name,
									      ddc_is_name,
									      m_calibrationData,
									      *oh_histogram_provider,
									      meta_histogram_provider,
									      histogram_cache,
									      palette,
									      m_categories,
									      context_menu,
									      meta_data_update_helper,
									      visualiser_list,
									      the_engine,
									      boc_editor,
									      *this );
  m_viewer->initActions();
  m_viewer->updateCategories(m_categories);  
  m_viewer->show();
  connect( this, SIGNAL( lastWindowClosed() ), this, SLOT( quit() ) );
}

// void OHViewerApp::addScan( PixCon::SerialNumber_t scan_serial_number ) 
// {
//   m_viewer->addSerialNumber( scan_serial_number );
// }

ConsoleApp::~ConsoleApp() {}

// void OHViewerApp::setTags(const std::string &object_tag_name, const std::string &tag_name, const std::string &cfg_tag_name)
// {
//   if (!object_tag_name.empty() && !tag_name.empty() && !cfg_tag_name.empty()) {
//     m_viewer->changeConnectivity(object_tag_name, tag_name,tag_name,tag_name,cfg_tag_name,0);
//   }
// }
