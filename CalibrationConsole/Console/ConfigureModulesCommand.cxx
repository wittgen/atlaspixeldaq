#include <ICommand.h>
#include <memory>
#include "ActionCommandBase.h"
#include <PixActions/PixActionsMulti.h>


namespace PixCon {

  class ConfigureModulesCommand : public ActionCommandBase
  {
  public:
    ConfigureModulesCommand(const std::shared_ptr<ConsoleEngine> &engine)
      : ActionCommandBase(engine)
    {}

    const std::string &name() const { return commandName(); }

    static const std::string &commandName() {
      return s_name;
    }

  protected:

    void setActionSequence(std::vector<std::string> &action_sequence) {
      action_sequence.push_back("sendConfig");
    }

    bool callAction(PixLib::PixActionsMulti &action) {
      action.sendConfig();
      return true;
    }

  private:

    //bool m_uninitialisedRodsOnly;
    static const std::string s_name;
  };

  const std::string ConfigureModulesCommand::s_name("Configure Modules");


  class ConfigureModulesKit : public PixCon::ICommandKit
  {
  protected:
    ConfigureModulesKit()
    {}

  public:
    PixCon::IConfigurableCommand *createCommand(const std::string &/*command*/, const std::shared_ptr<ConsoleEngine> &engine) const {
      assert(engine.get());
      return new ConfigureModulesCommand(engine);
    }

    static bool registerKit() {
      CommandKits::registerKit(ConfigureModulesCommand::commandName(),new ConfigureModulesKit, PixCon::UserMode::kShifter);
      //      CommandKits::registerKit(ConfigureModulesCommand::name(true),new ConfigureModulesKit(true));
      return true;
    }

  private:
    static bool s_registered;
  };

  bool ConfigureModulesKit::s_registered = ConfigureModulesKit::registerKit();

}
