#ifndef UI_CONFIGEDITBASE_H
#define UI_CONFIGEDITBASE_H

#include <QComboBox>
#include <QFrame>
#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QDialog>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QPushButton>
#include <QSpacerItem>
#include <QTabWidget>
#include <QVBoxLayout>
#include <QWidget>

QT_BEGIN_NAMESPACE

class Ui_ConfigEditBase
{
public:
    QVBoxLayout *vboxLayout;
    QTabWidget *m_configTabFrame;
    QWidget *m_configTabFrameFEI3;
    QVBoxLayout *m_configTabFrameFEI3Layout;
    QFrame *m_configPanelFrame;
    QWidget *m_configTabFrameFEI4;
    QVBoxLayout *m_configTabFrameFEI4Layout;
    QFrame *m_configPanelFrameFEI4;
    QHBoxLayout *hboxLayout;
    QSpacerItem *spacer81;
    QPushButton *m_cancelButton;
    QSpacerItem *spacer68;
    QLabel *textLabel1;
    QComboBox *m_tagSelector;
    QPushButton *m_saveButton;
    QSpacerItem *spacer82;

    void setupUi(QDialog *ConfigEditBase)
    {
        if (ConfigEditBase->objectName().isEmpty())
            ConfigEditBase->setObjectName(QString::fromUtf8("ConfigEditBase"));
        ConfigEditBase->resize(1000, 900);
        vboxLayout = new QVBoxLayout(ConfigEditBase);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        m_configTabFrame = new QTabWidget(ConfigEditBase);
        m_configTabFrame->setObjectName(QString::fromUtf8("m_configTabFrame"));
        m_configTabFrameFEI3 = new QWidget();
        m_configTabFrameFEI3->setObjectName(QString::fromUtf8("m_configTabFrameFEI3"));
        m_configTabFrameFEI3Layout = new QVBoxLayout(m_configTabFrameFEI3);
        m_configTabFrameFEI3Layout->setSpacing(6);
        m_configTabFrameFEI3Layout->setContentsMargins(11, 11, 11, 11);
        m_configTabFrameFEI3Layout->setObjectName(QString::fromUtf8("m_configTabFrameFEI3Layout"));
        m_configPanelFrame = new QFrame(m_configTabFrameFEI3);
        m_configPanelFrame->setObjectName(QString::fromUtf8("m_configPanelFrame"));
        m_configPanelFrame->setFrameShape(QFrame::NoFrame);
        m_configPanelFrame->setFrameShadow(QFrame::Plain);

        m_configTabFrameFEI3Layout->addWidget(m_configPanelFrame);

        m_configTabFrame->addTab(m_configTabFrameFEI3, QString());
        m_configTabFrameFEI4 = new QWidget();
        m_configTabFrameFEI4->setObjectName(QString::fromUtf8("m_configTabFrameFEI4"));
        m_configTabFrameFEI4Layout = new QVBoxLayout(m_configTabFrameFEI4);
        m_configTabFrameFEI4Layout->setSpacing(6);
        m_configTabFrameFEI4Layout->setContentsMargins(11, 11, 11, 11);
        m_configTabFrameFEI4Layout->setObjectName(QString::fromUtf8("vboxLayout2"));
        m_configPanelFrameFEI4 = new QFrame(m_configTabFrameFEI4);
        m_configPanelFrameFEI4->setObjectName(QString::fromUtf8("m_configPanelFrameFEI4"));
        m_configPanelFrameFEI4->setFrameShape(QFrame::NoFrame);
        m_configPanelFrameFEI4->setFrameShadow(QFrame::Plain);

        m_configTabFrameFEI4Layout->addWidget(m_configPanelFrameFEI4);

        m_configTabFrame->addTab(m_configTabFrameFEI4, QString());

        vboxLayout->addWidget(m_configTabFrame);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        spacer81 = new QSpacerItem(31, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacer81);

        m_cancelButton = new QPushButton(ConfigEditBase);
        m_cancelButton->setObjectName(QString::fromUtf8("m_cancelButton"));
        m_cancelButton->setDefault(true);

        hboxLayout->addWidget(m_cancelButton);

        spacer68 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacer68);

        textLabel1 = new QLabel(ConfigEditBase);
        textLabel1->setObjectName(QString::fromUtf8("textLabel1"));
        textLabel1->setWordWrap(false);

        hboxLayout->addWidget(textLabel1);

        m_tagSelector = new QComboBox(ConfigEditBase);
        m_tagSelector->setObjectName(QString::fromUtf8("m_tagSelector"));
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(3), static_cast<QSizePolicy::Policy>(0));
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(m_tagSelector->sizePolicy().hasHeightForWidth());
        m_tagSelector->setSizePolicy(sizePolicy);
        m_tagSelector->setEditable(true);

        hboxLayout->addWidget(m_tagSelector);

        m_saveButton = new QPushButton(ConfigEditBase);
        m_saveButton->setObjectName(QString::fromUtf8("m_saveButton"));

        hboxLayout->addWidget(m_saveButton);

        spacer82 = new QSpacerItem(41, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacer82);


        vboxLayout->addLayout(hboxLayout);


        retranslateUi(ConfigEditBase);
        QObject::connect(m_saveButton, SIGNAL(pressed()), ConfigEditBase, SLOT(accept()));
        QObject::connect(m_cancelButton, SIGNAL(pressed()), ConfigEditBase, SLOT(reject()));

        QMetaObject::connectSlotsByName(ConfigEditBase);
    } // setupUi

    void retranslateUi(QDialog *ConfigEditBase)
    {
        ConfigEditBase->setWindowTitle(QApplication::translate("ConfigEditBase", "ConfigEditBase", 0));
        m_configTabFrame->setTabText(m_configTabFrame->indexOf(m_configTabFrameFEI3), QApplication::translate("ConfigEditBase", "FEI3", 0));
        m_configTabFrame->setTabText(m_configTabFrame->indexOf(m_configTabFrameFEI4), QApplication::translate("ConfigEditBase", "FEI4", 0));
        m_cancelButton->setText(QApplication::translate("ConfigEditBase", "Cancel", 0));
        textLabel1->setText(QApplication::translate("ConfigEditBase", "Save to Tag :", 0));
        m_saveButton->setText(QApplication::translate("ConfigEditBase", "Save", 0));
    } // retranslateUi

};

namespace Ui {
    class ConfigEditBase: public Ui_ConfigEditBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONFIGEDITBASE_H
