#ifndef UI_EDITVECTORFORMBASE_H
#define UI_EDITVECTORFORMBASE_H

#include <QGroupBox>
#include <QTextEdit>
#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QDialog>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QPushButton>
#include <QRadioButton>
#include <QSpacerItem>
#include <QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_EditVectorFormBase
{
public:
    QVBoxLayout *vboxLayout;
    QLabel *m_changeVectorFormTitle;
    QTextEdit *m_textEdit;
    QHBoxLayout *hboxLayout;
    QPushButton *m_resetButton;
    QSpacerItem *spacer3;
    QGroupBox *m_baseGroup;
    QHBoxLayout *hboxLayout1;
    QRadioButton *m_decRadio;
    QRadioButton *m_hexRadio;
    QSpacerItem *spacer2;
    QPushButton *m_cancelButton;
    QPushButton *m_okButton;

    void setupUi(QDialog *EditVectorFormBase)
    {
        if (EditVectorFormBase->objectName().isEmpty())
            EditVectorFormBase->setObjectName(QString::fromUtf8("EditVectorFormBase"));
        EditVectorFormBase->resize(600, 256);
        vboxLayout = new QVBoxLayout(EditVectorFormBase);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        m_changeVectorFormTitle = new QLabel(EditVectorFormBase);
        m_changeVectorFormTitle->setObjectName(QString::fromUtf8("m_changeVectorFormTitle"));
        QFont font;
        font.setPointSize(14);
        font.setBold(true);
        m_changeVectorFormTitle->setFont(font);
        m_changeVectorFormTitle->setAlignment(Qt::AlignCenter);
        m_changeVectorFormTitle->setWordWrap(false);

        vboxLayout->addWidget(m_changeVectorFormTitle);

        m_textEdit = new QTextEdit(EditVectorFormBase);
        m_textEdit->setObjectName(QString::fromUtf8("m_textEdit"));

        vboxLayout->addWidget(m_textEdit);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        m_resetButton = new QPushButton(EditVectorFormBase);
        m_resetButton->setObjectName(QString::fromUtf8("m_resetButton"));

        hboxLayout->addWidget(m_resetButton);

        spacer3 = new QSpacerItem(40, 21, QSizePolicy::Fixed, QSizePolicy::Minimum);

        hboxLayout->addItem(spacer3);

        m_baseGroup = new QGroupBox(EditVectorFormBase);
        m_baseGroup->setFlat(true);
        m_baseGroup->setStyleSheet("border:0;");
        hboxLayout1 = new QHBoxLayout();
        m_decRadio = new QRadioButton(m_baseGroup);
        m_hexRadio = new QRadioButton(m_baseGroup);
        hboxLayout1->addWidget(m_decRadio);
        hboxLayout1->addWidget(m_hexRadio);
        m_baseGroup->setLayout(hboxLayout1);
        hboxLayout->addWidget(m_baseGroup);

        spacer2 = new QSpacerItem(31, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacer2);

        m_cancelButton = new QPushButton(EditVectorFormBase);
        m_cancelButton->setObjectName(QString::fromUtf8("m_cancelButton"));

        hboxLayout->addWidget(m_cancelButton);

        m_okButton = new QPushButton(EditVectorFormBase);
        m_okButton->setObjectName(QString::fromUtf8("m_okButton"));
        m_okButton->setDefault(true);

        hboxLayout->addWidget(m_okButton);


        vboxLayout->addLayout(hboxLayout);


        retranslateUi(EditVectorFormBase);
        QObject::connect(m_okButton, SIGNAL(clicked()), EditVectorFormBase, SLOT(accept()));
        QObject::connect(m_cancelButton, SIGNAL(clicked()), EditVectorFormBase, SLOT(reject()));
        QObject::connect(m_resetButton, SIGNAL(clicked()), EditVectorFormBase, SLOT(reset()));
        QObject::connect(m_decRadio, SIGNAL(clicked()), EditVectorFormBase, SLOT(changeBaseToDec()));
        QObject::connect(m_hexRadio, SIGNAL(clicked()), EditVectorFormBase, SLOT(changeBaseToHex()));

        QMetaObject::connectSlotsByName(EditVectorFormBase);
    } // setupUi

    void retranslateUi(QDialog *EditVectorFormBase)
    {
        EditVectorFormBase->setWindowTitle(QApplication::translate("EditVectorFormBase", "Form2", 0));
        m_changeVectorFormTitle->setText(QApplication::translate("EditVectorFormBase", "Change vector contents", 0));
        m_resetButton->setText(QApplication::translate("EditVectorFormBase", "Reset", 0));
#ifndef QT_NO_TOOLTIP
        m_resetButton->setProperty("toolTip", QVariant(QApplication::translate("EditVectorFormBase", "Revert all changes.", 0)));
#endif // QT_NO_TOOLTIP
        m_baseGroup->setTitle(QString());
        m_decRadio->setText(QApplication::translate("EditVectorFormBase", "dec", 0));
#ifndef QT_NO_TOOLTIP
        m_decRadio->setProperty("toolTip", QVariant(QApplication::translate("EditVectorFormBase", "Change numeric base to decimal", 0)));
#endif // QT_NO_TOOLTIP
        m_hexRadio->setText(QApplication::translate("EditVectorFormBase", "hex", 0));
#ifndef QT_NO_TOOLTIP
        m_hexRadio->setProperty("toolTip", QVariant(QApplication::translate("EditVectorFormBase", "Change numeric base to hexadecimal", 0)));
#endif // QT_NO_TOOLTIP
        m_cancelButton->setText(QApplication::translate("EditVectorFormBase", "Cancel", 0));
        m_okButton->setText(QApplication::translate("EditVectorFormBase", "Ok", 0));
    } // retranslateUi

};

namespace Ui {
    class EditVectorFormBase: public Ui_EditVectorFormBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDITVECTORFORMBASE_H
