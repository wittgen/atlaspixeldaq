#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <Histo/Histo.h>
#include "test_DummyHistoInput.h"

#include <DataContainer/GenericHistogramAccess.h>
#include <DataContainer/ScanResultStreamer.h>
#include <DataContainer/IScanResultListener.h>
#include <DataContainer/NopScanResultListener.h>
#include <ConfigWrapper/Regex_t.h>
#include <DataContainer/MakePixScanHistoName.h>

#include <ConfigWrapper/ScanConfig.h>
#include <DataContainer/HistoRef.h>

#include <memory>

#include <stdexcept>

template <class T_dest_, class T_src_>
T_dest_ *transform2D(const T_src_ *h_src)
{
  if (!h_src || !PixA::is2D(h_src) ) return NULL;

  T_dest_ *h_dest = PixA::createHisto<T_dest_>(PixA::histoName(h_src),
					       PixA::histoTitle(h_src),
					       PixA::nColumns(h_src),
					       PixA::axisMin(h_src,0),
					       PixA::axisMax(h_src,0),
					       PixA::nRows(h_src),
					       PixA::axisMin(h_src,1),
					       PixA::axisMax(h_src,1));

  float a_min=FLT_MAX;
  float a_max=-FLT_MAX;
  for (unsigned int col_i=0; col_i<PixA::nColumns(h_src); col_i++) {
    for (unsigned int row_i=0; row_i<PixA::nRows(h_src); row_i++) {
      float bin_content = PixA::binContent(h_src, PixA::startColumn(h_src)+col_i, PixA::startRow(h_src)+row_i);
      PixA::setBinContent(h_dest,
			  PixA::startColumn(h_dest)+col_i,
			  PixA::startRow(h_dest)+row_i,
			  bin_content);
      if (bin_content>a_max) a_max = bin_content;
      if (bin_content<a_min) a_min = bin_content;
    }
  }
  PixA::setMinMax(h_dest,a_min,a_max);
  return h_dest;
}

// template <class T_dest_, class T_src_>
// T_src_ *transform1D<T_dest_>(const T_dest *h_src)
// {
//   if (!h_src || PixA::is2D(h_src) ) return NULL;

//   T_dest_ *h_dest = PixA::createHisto(PixA::histoName(h_src),
// 				      PixA::histoTitle(h_src),
// 				      PixA::nColumns(h_src),
// 				      PixA::axisMin(h_src,0),
// 				      PixA::axisMax(h_src,0));

//   float a_min=FLT_MAX;
//   float a_max=-FLT_MAX;
//   for (unsigned int col_i=0; col_i<PixA::nColumns(h_src); col_i++) {
//     float bin_content = PixA::binContent(h_src, PixA::startColumn(h_src)+col_i);
//       PixA::setBinContent(h_dest,
// 			  PixA::startColumn(h_dest)+col_i,
// 			  bin_content);
//     if (bin_content>a_max) a_max = bin_content;
//     if (bin_content<a_min) a_min = bin_content;
//   }
//   PixA::setMinMax(h_dest,a_min,a_max);
//   return h_dest;
// }

template <class T_dest_1d_, class T_dest_2d_, class T_src_>
T_dest_1d_ *transform(const T_src_ *h_src)
{
  if (PixA::is2D(h_src)) {
    return transform2D<T_dest_2d_,T_src_>(h_src);
  }
  else {
    throw std::runtime_error("Not implemented.");
    //    return transform1D<T_dest_1d_,T_src_>(h_src);
  }
}

class DummyHistoInputExtractor : public PixA::NopScanResultListener
{
private:

  class Selection_t {
  public:
    Selection_t(const std::string &histo_name_pattern,
		const std::string &conn_name,
		PixCon::DummyHistoInput::EQuality quality)
      : m_histoNamePattern(histo_name_pattern),
	m_connName(conn_name),
	m_quality(quality)
    {
      assert(!histo_name_pattern.empty());
      assert(!m_connName.empty());
    }

    PixCon::DummyHistoInput::EQuality quality() const { return m_quality; }

    bool matches( const std::string &full_histo_path, const std::string &conn_name) const {
      return (conn_name == m_connName && m_histoNamePattern.match(full_histo_path));
    }

  private:
    PixA::Regex_t             m_histoNamePattern;
    const std::string         m_connName;
    PixCon::DummyHistoInput::EQuality m_quality;
  };

public:

  DummyHistoInputExtractor(PixCon::DummyHistoInput &input, bool simulation) 
    : m_dummyHistoInput(&input),
      m_moduleNamingScheme(PixA::kUnknownModuleNaming),
      m_simulation(simulation)
  {}

  void addSelection( const std::string &histo_path_pattern, const std::string &conn_name, PixCon::DummyHistoInput::EQuality quality)
  {
    m_selection.push_back(std::shared_ptr<Selection_t>(new Selection_t(histo_path_pattern, conn_name, quality)));
  }

protected:
  void newLabel(const std::string &name, const PixA::ConfigHandle &/*label_config*/) {
    m_currentLabel = name;
    m_currentPath = name;
  }

  void newFolder(const std::vector<std::string> &folder_hierarchy) {
    m_currentPath=m_currentLabel;
    for (std::vector<std::string>::const_iterator folder_iter = folder_hierarchy.begin();
	 folder_iter != folder_hierarchy.end();
	 folder_iter++) {
      m_currentPath+='/';
      m_currentPath+=*folder_iter;
    }
  }

  void newPixScanHisto(const std::string &histo_name,
		       const PixA::ScanConfigRef &scan_config,
		       const PixA::HistoHandle &histo_handle)
  {

    const Selection_t *selection = find(m_currentPath + "/" + histo_name , scan_config.connName());
    if (!selection) return;

    if (m_simulation) {
      std::cout << "INFO [DummyHistoInputExtractor::newPixScanHisto] add : " 
		<< m_currentPath << " / " <<  histo_name 
		<< " quality = " << selection->quality()
		<< std::endl;
      return;
    }

    try {

      PixA::HistoRef histo( histo_handle.ref());
      std::vector<std::string> full_histo_name;
      std::pair<PixA::EModuleNameingConvention, unsigned int> ret =
	PixA::makePixScanHistoName( m_moduleNamingScheme,
				    static_cast<unsigned int>(-1),
				    scan_config,
				    histo,
				    stripPath(histo_name),
				    full_histo_name );
      m_moduleNamingScheme = ret.first;

      HistoInfo_t info;
      histo.numberOfHistos(full_histo_name,info);
      PixA::Index_t index;
      info.makeDefaultIndexList(index);

      if ( index.size() > 0) {
	PixA::Index_t min_index(index);

	// loop over all indices
	for( ;index[0]<info.maxIndex(0); ) {

	  std::unique_ptr<PixLib::Histo> a_pixlib_histo( histo.histo(full_histo_name,index) );
	  if (a_pixlib_histo.get()) {
	    std::unique_ptr<TH1> a_histo(transform<TH1,TH2,PixLib::Histo>(a_pixlib_histo.get()));
	    m_dummyHistoInput->add(histo_name, scan_config.pixScanConfig(), selection->quality(),index, a_histo.get());
	  }

	  // next index;
	  unsigned int level_i=index.size();
	  for (; level_i-->0; ) {
	    if (index[level_i]+1<info.maxIndex(level_i)) {
	      index[level_i]++;
	      break;
	    }
	    else {
	      index[level_i]=min_index[level_i];
	    }
	  }
	  if (level_i>index.size()) break;
	}

      }
    }
    catch (PixLib::PixDBException &err ) {
      std::cout << "ERROR [DummyHistoInputExtractor::newPixScanHisto] Caught excpetion while retrieving histograms " << histo_name << m_currentPath << std::endl;
    }

  }

  const Selection_t *find(const std::string histo_path_name, const std::string &conn_name) const {
    for(std::list< std::shared_ptr<Selection_t> >::const_iterator selection_iter = m_selection.begin();
	selection_iter != m_selection.end();
	selection_iter++) {
      if ((*selection_iter)->matches(histo_path_name, conn_name)) {
	return selection_iter->get();
      }
    }
    return NULL;
  }

  std::string stripPath(const std::string &histo_name)
  {
    std::string::size_type pos = histo_name.rfind("/");
    if (pos != std::string::npos && pos+1 < histo_name.size()) {
      return std::string(histo_name,pos+1,histo_name.size()-pos-1);
    }
    else {
      return histo_name;
    }
  }

private:
  PixCon::DummyHistoInput *m_dummyHistoInput;
  std::list< std::shared_ptr<Selection_t> > m_selection;

  std::string m_currentLabel;
  std::string m_currentPath;
  PixA::EModuleNameingConvention m_moduleNamingScheme;

  bool        m_simulation;
};


class DummyScanConfigExtractor : public PixA::NopScanResultListener
{
private:

public:

  DummyScanConfigExtractor(const std::string &pattern) : m_pattern(pattern) {}

  const PixA::ConfigHandle &scanConfig() const { return m_scanConfig; }

protected:
  void newLabel(const std::string &name, const PixA::ConfigHandle &/*label_config*/) {
    m_currentLabel = name;
    m_currentPath = name;
  }

  void newFolder(const std::vector<std::string> &folder_hierarchy) {
    m_currentPath=m_currentLabel;
    for (std::vector<std::string>::const_iterator folder_iter = folder_hierarchy.begin();
	 folder_iter != folder_hierarchy.end();
	 folder_iter++) {
      m_currentPath+='/';
      m_currentPath+=*folder_iter;
    }
  }

  void newPixScanHisto(const std::string &histo_name,
		       const PixA::ScanConfigRef &scan_config,
		       const PixA::HistoHandle &histo_handle)
  {
    if (!m_pattern.match(m_currentPath+"/"+histo_name+"/"))  return;

    m_scanConfig = scan_config.pixScanConfigHandle();

  }


private:
  PixA::Regex_t m_pattern;

  std::string m_currentLabel;
  std::string m_currentPath;

  PixA::ConfigHandle m_scanConfig;
};


int main(int argc, char **argv) {

  std::map<std::string, PixCon::DummyHistoInput::EQuality> m_qualityMap;
  m_qualityMap.insert(std::make_pair<std::string, PixCon::DummyHistoInput::EQuality>("Ok",PixCon::DummyHistoInput::kOk));
  m_qualityMap.insert(std::make_pair<std::string, PixCon::DummyHistoInput::EQuality>("ok",PixCon::DummyHistoInput::kOk));
  m_qualityMap.insert(std::make_pair<std::string, PixCon::DummyHistoInput::EQuality>("Failure",PixCon::DummyHistoInput::kFailure));
  m_qualityMap.insert(std::make_pair<std::string, PixCon::DummyHistoInput::EQuality>("failure",PixCon::DummyHistoInput::kFailure));
  m_qualityMap.insert(std::make_pair<std::string, PixCon::DummyHistoInput::EQuality>("StillOk",PixCon::DummyHistoInput::kStillOk));
  m_qualityMap.insert(std::make_pair<std::string, PixCon::DummyHistoInput::EQuality>("stillok",PixCon::DummyHistoInput::kStillOk));

  std::string dummy_input_dest_file_name;
  bool simulation;

  std::unique_ptr<PixCon::DummyHistoInput> dummy_histo_input;

  std::string histo_name;
  bool error=false;
  for (int arg_i=1; arg_i<argc; arg_i++) {
    if (strcmp(argv[arg_i],"-f")==0 &&  arg_i+1<argc) {
      dummy_input_dest_file_name=argv[arg_i];
      break;
    }
  }

  for (int arg_i=1; arg_i<argc; arg_i++) {
    //
    if (strcmp(argv[arg_i],"-f")==0 &&  arg_i+1<argc) {
      dummy_input_dest_file_name=argv[++arg_i];
      dummy_histo_input.reset();
    }
    else if (strcmp(argv[arg_i],"-s")==0) {
      simulation=true;
    }
    else if (strcmp(argv[arg_i],"-x")==0) {
      simulation=false;
    }
    else if (strcmp(argv[arg_i],"-l")==0 &&  arg_i+2<argc && argv[arg_i+1][0]!='-' && argv[arg_i+2][0]!='-') {
      std::string file_name = argv[++arg_i];
      if (file_name.empty()) {
	std::cout << "ERROR [" << argv[0] << ":main] No file name before "<<arg_i << "." <<std::endl;
	error=true;
      }

      std::string histo_name = argv[++arg_i];
      if (histo_name.empty()) {
	std::cout << "ERROR [" << argv[0] << ":main] No histogram name before "<<arg_i << "." <<std::endl;
	error=true;
      }

      std::string quality_name = argv[++arg_i];
      std::map<std::string, PixCon::DummyHistoInput::EQuality>::const_iterator quality_iter = m_qualityMap.find(quality_name);
      if (quality_iter != m_qualityMap.end()) {

	// get scan config from input file
	if (!file_name.empty()) {
	  PixA::ConfigHandle scan_config;
	  {
	    DummyScanConfigExtractor extractor(std::string(".*/")+histo_name+"/.*");
	    try {
	      PixA::ScanResultStreamer streamer(file_name);
	      streamer.setListener( extractor );
	      streamer.setIgnoreConnAndCfg(true);
	      streamer.read();
	    }
	    catch (PixA::ConfigException &err) {
	      std::cout << "FATAL [main] " << file_name << " : caught exception (ConfigException) : " << err.getDescriptor() << std::endl;
	    }
	    catch (PixA::BaseException &err) {
	      std::cout << "FATAL [main] " << file_name << " : caught exception (BaseException) : " << err.getDescriptor() << std::endl;
	    }
	    catch (std::exception &err) {
	      std::cout << "FATAL [main] " << file_name << " : caught exception (std::exception) : " << err.what() << std::endl;
	    }
	    catch (...) {
	      std::cout << "FATAL [main] " << file_name << " : unhandled exception. "  << std::endl;
	    }
	    scan_config = extractor.scanConfig();
	  }
	  
	  try {
	    std::unique_ptr<PixCon::DummyHistoInput> histo_input(new PixCon::DummyHistoInput(dummy_input_dest_file_name, PixCon::DummyHistoInput::kRead));

	    HistoInfo_t info;
	    histo_input->numberOfHistograms(histo_name, scan_config, quality_iter->second,info);
	    std::cout << "idx:";
	    for (int i=0; i<3; i++) {
	      std::cout << info.maxIndex(i) << (i<2 ? "," : "");
	    }
	    std::cout << " n histos=" << info.minHistoIndex() << " - " << info.maxHistoIndex() << std::endl;

	    PixA::Index_t index;
	    //	  info.makeDefaultIndexList(index);

	    if (info.maxHistoIndex() > info.minHistoIndex() ) {
	      for (int i=0; i<3; i++) {
		if (info.maxIndex(i)<=0) break;
		index.push_back(rand()%info.maxIndex(i));
	      }
	      index.push_back(info.minHistoIndex() + rand()%(info.maxHistoIndex() - info.minHistoIndex()));
	      std::unique_ptr<TH1> ahist( histo_input->histo(histo_name, scan_config, quality_iter->second, index) );
	      if (ahist.get()) {
		std::cout << " got " << ahist->GetName() << std::endl;
	      }
	      else {
		std::cout << " Did not get a histo. " << std::endl;
	      }
	    }
	    else {
	      std::cout << "No histograms. " << std::endl;
	    }
	  }
	  catch (PixCon::DummyHistoInput::AccessError &err) {
	    std::cout << "FATAL [main] : caught exception (std::exception) : " << err.what() << std::endl;
	  }

	}
	else {
	  std::cout << "ERROR [" << argv[0] << ":main] No input file name givent before "<<arg_i << "." <<std::endl;
	  error=true;
	  break;
	}

      }
      else {
	std::cout << "ERROR [" << argv[0] << ":main] Unknown quality specifier before "<<arg_i << " : " << quality_name <<std::endl;
	error=true;
      }
    }
    else if (strcmp(argv[arg_i],"-i")==0 &&  arg_i+1<argc) {
      try {
	if (!dummy_histo_input.get()) {
	  dummy_histo_input = std::unique_ptr<PixCon::DummyHistoInput>(new PixCon::DummyHistoInput(dummy_input_dest_file_name, PixCon::DummyHistoInput::kUpdate));
	}
	DummyHistoInputExtractor extractor(*dummy_histo_input, simulation);

	std::string file_name = argv[++arg_i];
	unsigned int pattern_counter=0;
	while (arg_i+2<argc && argv[arg_i][0]!='-' && argv[arg_i+1][0]!='-' && argv[arg_i+2][0]!='-') {

	  std::string histo_pattern = argv[++arg_i];
	  if (histo_pattern.empty()) {
	    std::cout << "ERROR [" << argv[0] << ":main] No histogram path pattern before "<<arg_i << "." <<std::endl;
	    error=true;
	  }

	  std::string conn_name = argv[++arg_i];
	  if (conn_name.empty()) {
	    std::cout << "ERROR [" << argv[0] << ":main] No connectivity name before "<<arg_i << "." <<std::endl;
	    error=true;
	  }

	  std::string quality_name = argv[++arg_i];
	  std::map<std::string, PixCon::DummyHistoInput::EQuality>::const_iterator quality_iter = m_qualityMap.find(quality_name);
	  if (quality_iter != m_qualityMap.end()) {
	    if (!conn_name.empty() && !histo_pattern.empty()) {
	      extractor.addSelection( histo_pattern, conn_name, quality_iter->second);
	    }
	  }
	  else {
	    std::cout << "ERROR [" << argv[0] << ":main] Unknown quality specifier before "<<arg_i << " : " << quality_name <<std::endl;
	    error=true;
	  }
	  if (!error) {
	    pattern_counter++;
	  }
	}
	if (error) {
	  break;
	}
	if (!file_name.empty()) {
	  if (pattern_counter>0) {

	    try {
	      PixA::ScanResultStreamer streamer(file_name);
	      streamer.setListener( extractor );
	      streamer.setIgnoreConnAndCfg(true);
	      streamer.read();
	    }
	    catch (PixA::ConfigException &err) {
	      std::cout << "FATAL [main] " << file_name << " : caught exception (ConfigException) : " << err.getDescriptor() << std::endl;
	    }
	    catch (PixA::BaseException &err) {
	      std::cout << "FATAL [main] " << file_name << " : caught exception (BaseException) : " << err.getDescriptor() << std::endl;
	    }
	    catch (std::exception &err) {
	      std::cout << "FATAL [main] " << file_name << " : caught exception (std::exception) : " << err.what() << std::endl;
	    }
	    catch (...) {
	      std::cout << "FATAL [main] " << file_name << " : unhandled exception. "  << std::endl;
	    }
	  }
	}
	else {
	  std::cout << "ERROR [" << argv[0] << ":main] No input file name givent before "<<arg_i << "." <<std::endl;
	  error=true;
	  break;
	}
      }
      catch (PixCon::DummyHistoInput::AccessError &err) {
	std::cout << "ERROR [" << argv[0] << ":main] Failure when accessing file "<< dummy_input_dest_file_name 
		  << " : " << err.what() << std::endl;
      }
    }
    else {
      std::cout << "ERROR [" << argv[0] << ":main] Unhandeled argument "<<arg_i << " : " << argv[arg_i] <<std::endl;
      error=true;
      break;
    }
  }
  
  if (error) {
    std::cout << "USAGE : " << argv[0] << " -f \"destination file for dummy input\" [-i histo-path-pattern conn-name quality(Ok, StillOk, Failure) ] ..." <<std::endl;
  }


  
}
