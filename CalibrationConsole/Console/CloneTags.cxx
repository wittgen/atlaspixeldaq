#include "CloneTags.h"

namespace PixCon {

  CloneTags::CloneTags(QWidget* parent, std::string partition_name, std::string dbServer_name) 
    : QDialog(parent)
  {
    setupUi(this);
    std::unique_ptr<IPCPartition> partition;
    if (!partition_name.empty() && !dbServer_name.empty()) {
      partition = std::make_unique<IPCPartition>(partition_name.c_str());
      if (!partition->isValid()) {
	std::cerr << "ERROR [CloneTags::CloneTags] Not a valid partition " << partition_name
		  << " for the db server." << std::endl;
	return;
      }
      bool ready=false;
      m_dbs = std::make_shared<PixLib::PixDbServerInterface>(partition.get(), dbServer_name, PixLib::PixDbServerInterface::CLIENT, ready);
      if(!ready) {
	m_dbs.reset();
	std::cerr << "ERROR [Clonetags::Clonetags] Impossible to connect to DbServer " << dbServer_name << std::endl;
	return;
      }
      std::vector<std::string> listDomains;
      m_dbs->listDomainRem(listDomains);
      for (std::vector<std::string>::iterator itdom = listDomains.begin(); itdom!=listDomains.end(); itdom++) {
	std::cout << "INFO [CloneTags::CloneTags] Found domain: " << (*itdom) << std::endl;
	std::string s("Configuration");
	std::string::size_type pos = (*itdom).find(s);
	if (pos != std::string::npos){
	  domainBox->addItem(QString::fromStdString(*itdom));
	}
      }
    }
    
    if (!domainBox->currentText().isEmpty()) {
      changeDomain(domainBox->currentText().toLatin1().data());
    } 
  }
  
  CloneTags::~CloneTags()
  {
  }
  
  void CloneTags::changeDomain(const QString &domain)
  {
    std::vector<std::string> cfgTags;
    m_dbs->listTagsRem(domain.toLatin1().data(),cfgTags);
    std::cout << "INFO [CloneTags::changeDomain] Using domain " << domain.toLatin1().data() << std::endl;
    for (std::vector<std::string>::iterator itcfg = cfgTags.begin(); itcfg!=cfgTags.end(); itcfg++) {
      std::cout << "INFO [CloneTags::changeDomain] Found tag: " << (*itcfg) << std::endl;
      bool found = false;
      for (int i = 0; i < origBox->count(); i++) {
	if (std::string(origBox->itemText(i).toLatin1().data()) == *itcfg) {
	  found = true;
	}
      }
      if (!found){
	origBox->addItem(QString::fromStdString(*itcfg));
	destBox->addItem(QString::fromStdString(*itcfg));
      }
    }
  }
  
  void CloneTags::accept()
  {
    std::string domain = domainBox->currentText().toLatin1().data();
    std::string origTag = origBox->currentText().toLatin1().data();
    std::string destTag = destBox->currentText().toLatin1().data();
    std::string pendTag = pendEdit->displayText().toLatin1().data();
    std::cout << "Cloning from " << domain << " " << origTag << " to " << destTag << " with pending " << pendTag << std::endl; 
    m_dbs->cloneTag(domain, origTag, destTag, pendTag);
    QDialog::accept();
  }
}
