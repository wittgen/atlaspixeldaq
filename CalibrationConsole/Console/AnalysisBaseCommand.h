#ifndef _PixCon_AnalysisBaseCommand_h_
#define _PixCon_AnalysisBaseCommand_h_

#include <ICommand.h>
#include <memory>
#include "ConsoleEngine.h"
#include "IStatusMonitorActivator.h"
#include "CommandKits.h"
#include <AnalysisInfo_t.hh>

namespace PixCon {

  class ScanSelection;

  class AnalysisBaseCommand : public PixCon::IConfigurableCommand
  {
  public:
    AnalysisBaseCommand(const std::shared_ptr<ConsoleEngine> &engine)
      : m_engine(engine)
    {}

    /** Helper class to cleanup and set return status upon e.g. exceptions.
     */
    class AnalysisCleanUpHelper {
    public:
      AnalysisCleanUpHelper(ConsoleEngine &engine, Result_t &result)
	: m_engine(&engine),
	  m_result(&result)
      {}

      ~AnalysisCleanUpHelper() {
	if (m_result) {
	  //m_result->setFailure();
	  m_engine->statusMonitorActivator()->abortAnalysisMonitoring( m_result->analysisSerialNumber()  );
	}
      }

      void doNotCleanup() {
	if (m_result) {
	  m_result=NULL;
	}
      }

    private:
      ConsoleEngine  *m_engine;
      Result_t       *m_result;
    };

    PixCon::Result_t &loadScan(SerialNumber_t scan_serial_number, PixCon::Result_t &ret);

    PixCon::Result_t &startAnalysis( PixCon::Result_t &ret );

    PixCon::Result_t &waitForAnalysis( PixCon::Result_t &ret);

    void resetAbort() {
      m_abort=false;
      m_kill=false;
    }

    void abort() {
      m_abort=true;
      m_engine->statusChangeTransceiver()->abortSignal(kAnalysis);
    }

    void kill() {
      m_kill=true;
      abort();
    }

    const std::string &name() const { return  (m_analysisInfo.get() ?  m_analysisInfo->name(CAN::kAlg) : s_empty); }

    bool hasAnalysis() const {
      return m_analysisInfo.get() != NULL;
    }

    bool configureAnalysis(ScanSelection &selection, SerialNumber_t scan_serial_number) {
      return configureAnalysis(selection, scan_serial_number, false);
    }

    
  protected:
    bool configureAnalysis(ScanSelection &selection, SerialNumber_t scan_serial_number, bool scan_serial_number_is_set_later);

    bool changeScanSerialNumber(SerialNumber_t scan_serial_number);

    ///@todo should be shared between analysis and scan command.
    //   need basis class which provides shared functionality.
    void setLoopCounters( PixA::ConfigHandle &scan_config_handle,
			  std::vector<unsigned short> &start_value,
			  std::vector<unsigned short> &max_counter,
			  unsigned int &mask_stage_loop_index);

    void sendAbort(PixCon::Result_t &ret);

    static const std::string &emptyString() {return s_empty; }

    std::shared_ptr<ConsoleEngine>    m_engine;
    std::unique_ptr<CAN::AnalysisInfo_t>  m_analysisInfo;

    bool           m_abort;
    bool           m_kill;

    static std::string s_empty;
  };

}
#endif
