#include <test_Broker.h>
#include <ConfigWrapper/ConnectivityManager.h>
#include <ConfigWrapper/Connectivity.h>
#include <ConfigWrapper/ConnectivityUtil.h>

#include "test_DummyMultiAction.h"
#include <PixUtilities/PixMessages.h>
#include <PixActions/PixActionsMulti.h>
#include <PixUtilities/PixISManager.h>

// for reading tags from IS :
#include <is/infoT.h>
#include <is/infodictionary.h>

#include <set>


namespace PixLib {

  unsigned int DummyBroker::s_multiActionCounter =0;

  DummyBroker::DummyBroker(std::set<std::string> &actions,
			   IPCPartition &partition,
			   const std::string &is_server_name,
			   const std::string &oh_server_name,
			   const std::string &histo_name_server_name,
			   const std::string &id_tag,
			   const std::string &tags,
			   const std::string &cfg_tag,
			   const std::string &broker_name,
			   bool verbose)
    : PixBroker(&partition,std::string("IF:DummyBroker")+broker_name,new PixMessages),
      m_partition( &partition ),
      m_isServerName(is_server_name),
      m_ohServerName(oh_server_name),
      m_histoNameServerName(histo_name_server_name),
      m_conn( PixA::ConnectivityManager::getConnectivity( id_tag,
							  tags,
							  tags,
							  tags,
							  cfg_tag,
							  0 ) ),
      m_verbose(verbose)
  {

    PixBroker::m_name= std::string("IF:DummyBroker")+broker_name;

    createActions(actions);
  }

  DummyBroker::~DummyBroker() {
  }

  std::set<std::string> DummyBroker::listActions(bool checkAvailability,
						 PixActions::Type type,
						 std::string allocatingBrokerName,
						 std::string managingBrokerName,
						 std::string caller) {
    return m_multiAction->listActions(checkAvailability, type, allocatingBrokerName, managingBrokerName, caller);
  }

  std::map<std::string,std::vector<std::string> > DummyBroker::getConnectivityTags()
  {
    // CT:CT-C1OSP-V3;CT-C12OSP
    std::vector<std::string> conn_tags;
    if (m_conn) {
      conn_tags.push_back( m_conn.tag(PixA::IConnectivity::kConn) );
      conn_tags.push_back( m_conn.tag(PixA::IConnectivity::kAlias) );
      conn_tags.push_back( m_conn.tag(PixA::IConnectivity::kPayload) );
      conn_tags.push_back( m_conn.tag(PixA::IConnectivity::kConfig) );
      conn_tags.push_back( m_conn.tag(PixA::IConnectivity::kId) );
      std::string cfg_oi=m_conn.tag(PixA::IConnectivity::kId);
      cfg_oi+="-CFG";
      conn_tags.push_back( cfg_oi);
      conn_tags.push_back( m_conn.tag(PixA::IConnectivity::kModConfig) );
    }
    std::map<std::string,std::vector<std::string> > result;
    result.insert( std::pair<std::string,std::vector<std::string> >(std::string("MULTI"),conn_tags));
    return result;
  }

  class CallBackHelper
  {
  public:
    CallBackHelper(const ipc::PixBrokerCallback_var &call_back) : m_callBack(call_back) {}

    ~CallBackHelper() {
      m_callBack->decreaseConnections();
    }
  private:
    ipc::PixBrokerCallback_var m_callBack;
  };


  void DummyBroker::updateConnectivityFromIS(std::string idTag, std::string connTag, std::string cfgTag, std::string modCfgTag)
  {
    CallBackHelper call_back_helper(m_cb);

    std::cout << "INFO [DummyBroker::updateConnectivityFromIS] delete sub actions action." << std::endl;
    m_subMultiActionList.clear();
    std::cout << "INFO [DummyBroker::updateConnectivityFromIS] delete multi action." << std::endl;
    m_multiAction.reset();

    if (idTag.empty() || connTag.empty() || cfgTag.empty()) {
      std::string is_name_for_tags = "PixelRunParams";
	std::unique_ptr<ISInfoDictionary> info_dictionary( new ISInfoDictionary(*m_partition) );
	std::vector<std::pair<std::string, std::string *> > tag_list;
	if (idTag.empty())   tag_list.push_back(std::make_pair( "IdTag",&idTag));
	if (connTag.empty()) tag_list.push_back(std::make_pair( "ConnTag",&connTag));
	if (cfgTag.empty())  tag_list.push_back(std::make_pair( "CfgTag",&cfgTag));
	if (cfgTag.empty() && modCfgTag.empty())  tag_list.push_back(std::make_pair( "ModCfgTag",&modCfgTag));
	std::string is_var_header = is_name_for_tags + ".DataTakingConfig-";

	ISInfoString is_string;
	for (std::vector<std::pair<std::string, std::string *> >::const_iterator tag_iter = tag_list.begin();
	     tag_iter != tag_list.end();
	     ++tag_iter) {
	  if (tag_iter->second->empty()) {
	    try {
	      info_dictionary->getValue((is_var_header+tag_iter->first).c_str(),is_string);
	      *(tag_iter->second) = is_string.getValue();
	    }
	    catch(daq::is::Exception &ex) {
	      std::cout << "WARNING [DummyBroker::updateConnectivityFromIS] Exception while trying to read tags from IS : " << ex.what() <<std::endl;
	    }
	  }
	}

	std::cout << "INFO [DummyBroker::updateConnectivityFromIS] Final tags : "<< idTag << ":" << connTag << "," << cfgTag  << std::endl;
	if (!modCfgTag.empty()) {
	  std::cout << "," << modCfgTag;
	}
	std::cout << std::endl;

    }

    if (idTag.empty()) {
      idTag = m_conn.tag(PixA::IConnectivity::kId);
    }
    if (connTag.empty()) {
      connTag = m_conn.tag(PixA::IConnectivity::kConn);
    }
    if (cfgTag.empty()) {
      cfgTag = m_conn.tag(PixA::IConnectivity::kConfig);
    }

    if (cfgTag.empty()) {
      modCfgTag = m_conn.tag(PixA::IConnectivity::kModConfig);
    }

    m_conn=PixA::ConnectivityManager::getConnectivity( idTag,
						       connTag,
						       connTag,
						       connTag,
						       cfgTag,
						       modCfgTag,
						       0 ); 
    std::cout << "INFO [DummyBroker::updateConnectivityFromIS] recreate actions:" 
	      << m_conn.tag(PixA::IConnectivity::kId) << ":"
	      << m_conn.tag(PixA::IConnectivity::kConn) << ";"
	      << m_conn.tag(PixA::IConnectivity::kConfig) << ","
	      << m_conn.tag(PixA::IConnectivity::kModConfig) << ";"
	      << std::endl;

    std::set<std::string> copy(m_actionNames);
    createActions(copy);
    std::cout << "INFO [DummyBroker::updateConnectivityFromIS] done." << std::endl;
  }

  void DummyBroker::createActions(const std::set<std::string> &actionNames)
  {
    //     std::set<std::string> all_actions=listActions(true,PixActions::SINGLE_ROD, "", "", "");
    //     for (std::set<std::string>::iterator action_name_iter = actionNames.begin();
    // 	 action_name_iter != actionNames.end();
    // 	 ) {
    //       std::set<std::string>::iterator current_action_name_iter = action_name_iter++;
    //       if ( all_actions.find(*current_action_name_iter) == all_actions.end()) {
    // 	actionNames.erase( current_action_name_iter);
    //       }
    //     }
    m_actionNames = actionNames;
    std::map<std::string,ipc::PixActions_var> actions;
    m_partition->getObjects<ipc::PixActions>(actions);

    if (!m_multiAction.get()) {
      std::cout << "INFO [DummyBroker::createActions] create new multi action." << std::endl;
      std::set<std::string> temp;
      std::string multi_action_name("MULIT_DUMMY_ACTION");
      if (!m_name.empty()) {
	multi_action_name += ':';
	multi_action_name += m_name;
      }
      m_multiAction=std::shared_ptr<DummyMultiAction>(new DummyMultiAction(*m_partition,m_conn,multi_action_name,m_verbose ),destroyer<DummyMultiAction>);
      m_multiAction->descriptor().managingBrokerName = m_name;
    }

    if (!m_multiAction.get()) {
      std::cout << "FATAL [DummyBroker::createActions] No multi action." << std::endl;
      return;
    }

    std::cout << "INFO [DummyBroker::createActions] Now create single action." << std::endl;

    for (std::set<std::string>::const_iterator action_name_iter = actionNames.begin();
	 action_name_iter != actionNames.end();
	 action_name_iter++) {

      // only create actions which do not exist yet
      if (actions.find(*action_name_iter)==actions.end() ) {
	std::set<std::string> single_action;
	single_action.insert(*action_name_iter);
	DummySingleAction *an_action = new DummySingleAction( *m_partition,
							      m_isServerName,
							      m_ohServerName,
							      m_histoNameServerName,
							      m_conn,
							      single_action,
							      m_verbose);
	if (an_action) {
	  an_action->descriptor().managingBrokerName = m_name;
	  m_multiAction->addAction(*action_name_iter, an_action );
	  if (m_verbose) {
	    std::cout << "INFO [DummyBroker::createActions] created action : " << *action_name_iter << "." << std::endl;
	  }
	}
      }

    }
    std::cout << "INFO [DummyBroker::createActions] done." << std::endl;

  }

  PixActions* DummyBroker::allocateActions(std::set<std::string> actionNames) {
    return allocateActions(actionNames, m_name.c_str());
  }


  class MyPixActionsMulti : public PixActionsMulti
  {
  public:
    MyPixActionsMulti(std::string actionName,
		    std::string managingBrokerName,
		    std::string allocatingBrokerName,
		    std::set<std::string> subActions,
		    IPCPartition *ipcPartition,
		    const std::string &is_server_name,
		    PixMessages* myMessages = NULL)
      : PixActionsMulti(actionName,managingBrokerName, allocatingBrokerName, subActions, ipcPartition, myMessages)
    {
      delete m_is;
      m_is = new PixLib::PixISManager(ipcPartition->name(), is_server_name);
    }
  };


  PixActions* DummyBroker::allocateActions(std::set<std::string> action_names, const char* allocatingBrokerName) {

    {
      std::set<std::string>::iterator action_iter = action_names.begin();
      bool reached_action_names_end = (action_iter == action_names.end() );
      while (!reached_action_names_end) {

	 std::set<std::string>::iterator current_action_iter= action_iter++;
	 reached_action_names_end = (action_iter == action_names.end() );

	 if (!m_multiAction->isAvailable(*current_action_iter)) {
	   action_names.erase(current_action_iter);
	 }
      }
    }

    if (!action_names.empty()) {
      std::stringstream  action_name;

      action_name << m_name <<":MULTI_" << (++s_multiActionCounter);

      PixActions *multi_action = new PixLib::MyPixActionsMulti( action_name.str(), m_name, "", action_names, m_partition, m_isServerName );
      multi_action->allocate(allocatingBrokerName);
      multi_action->descriptor().managingBrokerName = m_name;
      m_subMultiActionList.insert(std::make_pair(action_name.str(), std::shared_ptr<PixLib::PixActions>(multi_action, destroyer<PixLib::PixActions>)));
      return multi_action;
    }
    else {
      if (m_verbose) {
	std::cout << "INFO [DummyBroker::allocateActions] Nothing allocated, since none of the given actions are available." << std::endl;
      }
    }
    return NULL;
  }

  bool DummyBroker::ipc_deallocateAction(const char* actionName, const char* deallocatingBrokerName) {

    {
      std::map<std::string, std::shared_ptr<PixLib::PixActions> >::iterator action_iter = m_subMultiActionList.find(actionName);
      if (action_iter != m_subMultiActionList.end()) {
	if (action_iter->second->descriptor().allocatingBrokerName == deallocatingBrokerName) {
	  action_iter->second->deallocate(deallocatingBrokerName);
	  m_subMultiActionList.erase(action_iter);
	  return true;
	}
	else {
	  if (m_verbose) {
	    std::cout << "INFO [DummyBroker::deallocateActions] not deallocating action : " << action_iter->second->descriptor().name 
		      << " because allocating broker, " << action_iter->second->descriptor().allocatingBrokerName
		      << ", does not seem to be the delallocating one, " << deallocatingBrokerName << "." << std::endl;
	    return false;
	  }
	}
      }
    }
    return m_multiAction->deallocateActions(actionName, deallocatingBrokerName);
  }


  void DummyBroker::deallocateActions(PixActions* action)
  {
    deallocateActions(action, m_name.c_str());
  }

  void DummyBroker::deallocateActions(PixActions* action, const char* deallocating_broker_name)
  {
    std::map<std::string, std::shared_ptr<PixLib::PixActions> >::iterator  sub_multi_action_iter = m_subMultiActionList.begin();
    bool reached_end = ( sub_multi_action_iter == m_subMultiActionList.end() );
    while (!reached_end) {

      std::map<std::string, std::shared_ptr<PixLib::PixActions> >::iterator  current_sub_multi_action_iter = sub_multi_action_iter++;
      reached_end = ( sub_multi_action_iter == m_subMultiActionList.end() );

      if (current_sub_multi_action_iter->second.get() == action ) {
	if (current_sub_multi_action_iter->second->descriptor().allocatingBrokerName == deallocating_broker_name) {
	  current_sub_multi_action_iter->second->deallocate(deallocating_broker_name);
	  m_subMultiActionList.erase(current_sub_multi_action_iter);
	}
	else {
	  if (m_verbose) {
	    std::cout << "INFO [DummyBroker::deallocateActions] not deallocating action : " << current_sub_multi_action_iter->second->descriptor().name 
		      << " because allocating broker, " << current_sub_multi_action_iter->second->descriptor().allocatingBrokerName
		      << ", does not seem to be the delallocating one, " << deallocating_broker_name << "." << std::endl;
	  }
	}
	return;
      }
    }

    m_multiAction->deallocateActions(action,deallocating_broker_name);
  }


}
