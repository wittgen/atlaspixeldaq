/* Dear emacs, this is -*-c++-*- */
#ifndef _PixCon_CommandKits_h_
#define _PixCon_CommandKits_h_

#include <KitList.h>
#include <memory>
#include <ICommand.h>
#include <CommandSequence.h>
#include "UserMode.h"

namespace PixCon {

  class ConsoleEngine;


  class IConfigurableCommand : public virtual ICommand
    {
  public:

    /** Returns true if the configuration has been changed
     */
    virtual bool configure() = 0;

    /** Returns true the command can be run with defaults.
     */
    virtual bool hasDefaults() const = 0;
  };


  class IConfigurableClassification : public IClassification
    {
  public:

    /** Returns true if the configuration has been changed
     */
    virtual bool configure() = 0;

    /** Returns true the command can be run with defaults.
     */
    virtual bool hasDefaults() const = 0;

  };

  class ConfigurableCommandSequence : virtual public IConfigurableCommand, public virtual CommandSequence
  {
  public:
    ConfigurableCommandSequence(const std::string &name) : CommandSequence(name) {}

    bool configure();

    /** Returns true the command can be run with defaults.
     */
    bool hasDefaults() const;

  protected:
    static bool configureCommand(ICommand *a_command);
  };

  class ICommandKit {
  public:
    virtual ~ICommandKit() {};
    virtual PixCon::IConfigurableCommand *createCommand(const std::string &command,  const std::shared_ptr<ConsoleEngine> &engine) const = 0;
  };

  class IClassificationKit {
  public:
    virtual ~IClassificationKit() {};
    virtual PixCon::IConfigurableClassification *createClassification(const std::string &classification,  const std::shared_ptr<ConsoleEngine> &engine) const = 0;
  };


  class CommandKits
  {
  protected:
    CommandKits() {}
    ~CommandKits();

  public:
    unsigned int nCommands()  {
      return m_commandKitList.size();
    }

    const std::string &commandName( unsigned int command_id ) {
      if (command_id<m_commandKitList.size()) {
	return m_commandKitList.name(command_id);
      }
      return s_empty;
    }

    const PixCon::UserMode::EUserMode commandUser( unsigned int command_id ) {
      if (command_id<m_commandKitList.size()) {
	return m_commandKitList.user(command_id);
      }
      return PixCon::UserMode::kUnknown;
    }

    unsigned int nClassifications()  {
      return m_classificationKitList.size();
    }

    const std::string &classificationName( unsigned int command_id ) {
      if (command_id<m_classificationKitList.size()) {
	return m_classificationKitList.name( command_id);
      }
      return s_empty;
    }

    const ICommandKit *commandKit(const std::string &command_name) {
      return  m_commandKitList.kit(command_name);
    }

    const PixCon::IClassificationKit *classificationKit(const std::string &command_name) {
      return m_classificationKitList.kit(command_name);
    }

    static void registerKit(const std::string &name, ICommandKit *a_command, const PixCon::UserMode::EUserMode& user) {
      assert (!name.empty() );
      assert( a_command );
      instance()->m_commandKitList.insert(name, a_command, user);
    }

    static void registerKit(const std::string &name, IClassificationKit *a_classification, const PixCon::UserMode::EUserMode& user) {
      assert (!name.empty() );
      assert( a_classification );
      instance()->m_classificationKitList.insert(name, a_classification, user);
    }

    static CommandKits *instance() {
      if (!s_instance) {
	s_instance = new CommandKits;
      }
      return s_instance;
    }

  protected:
    PixCon::KitList<ICommandKit>        m_commandKitList;
    PixCon::KitList<IClassificationKit> m_classificationKitList;
    
    static CommandKits *s_instance;
    static const std::string s_empty;
  };
}
#endif
