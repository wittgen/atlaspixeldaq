#ifndef _PixCon_DcsSubscription_h_
#define _PixCon_DcsSubscription_h_

#include "DcsSubscriptionsBase.h"

#include <vector>

class QCheckBox; 

namespace PixCon {

  class DcsSubscriptions : public DcsSubscriptionsBase {
    Q_OBJECT
  public:
    DcsSubscriptions( const std::vector< int > &subscriptions, QWidget* parent, const char* name, bool modal=false, WFlags fl =0 );

    void clearAllSubscriptions();

  private:
    std::map<int,QCheckBox *> m_checkBoxList;

  public slots:
    virtual void selectionChanged(int dcs_variable);
    void hide();
  signals:
    void changeSubscription(int dcs_variable, bool);
    void closed();
    
  };

}

#endif
