#ifndef _DummyMultiAction_H_
#define _DummyMultiAction_H_

#include <PixActions/PixActions.h>
#include <ConfigWrapper/Connectivity.h>
#include <ConfigWrapper/ConfigHandle.h>
#include <set>
#include <string>
#include <vector>
#include <memory>
#include <queue>
#include <algorithm>

#include "test_DummyHistoInput.h"


#include <Flag.h>

class ISInfoDictionary;

namespace PixCon {
  class DummyHistoInput;
}

namespace PixLib {

  typedef long int PixLibLong;
  typedef int PixLibInt; // T
  typedef unsigned long int PixLibULong;

  class PixHistoServerInterface;

  class DummyActionData
  {
  public:
    DummyActionData(IPCPartition &partition,
		    const std::string &is_server_name,
		    const std::string &oh_server_name,
		    const std::string &histo_name_server_name,
		    const PixA::ConnectivityRef &conn,
		    bool verbose=true);

    ~DummyActionData();

    std::string makeActiveName(const std::string &rod_name) const {
      return makeIsName(rod_name, "", "Active" );
    }

    std::string makeActiveName(const std::string &rod_name,
			       const std::string &module_name) const {
      return makeIsName(rod_name, module_name, "Active" );
    }

    std::string makeActionStatusName(const std::string &crate_name,
				     const std::string &rod_name) const {
      return makeIsName(crate_name, rod_name, "STATUS" );
    }

    std::string makeActionName(const std::string &crate_name,
			       const std::string &rod_name) const {
      return makeIsName(crate_name, rod_name, "CURRENT-ACTION" );
    }


    std::string makeActionStatusName(CORBA::Long serial_number,
				     const std::string &crate_name,
				     const std::string &rod_name) const {
      return makeIsName(serial_number,crate_name, rod_name, "STATUS" );
    }

    std::string makeIsName(const std::string &crate_or_rod_name,
			   const std::string &rod_or_module_name,
			   const std::string &var_name) const {
      std::string is_name(m_isServerName);
      is_name += ".";
      is_name += crate_or_rod_name;
      is_name += '/';
      if (!rod_or_module_name.empty()) {
	is_name += rod_or_module_name;
	is_name += '/';
      }
      is_name += var_name;

      return is_name;
    }

    std::string makeIsName(CORBA::Long serial_number,
			   const std::string &crate_or_rod_name,
			   const std::string &rod_or_module_name,
			   const std::string &var_name) const;

    bool hasRod(const std::string &rod_name) const {
      return m_rodNames.find( rod_name ) != m_rodNames.end();
    }

    void addRod(const std::string &rod_name) {
      m_rodNames.insert(  rod_name );
    }

    std::set<std::string>::const_iterator beginRod() const { return m_rodNames.begin(); }
    std::set<std::string>::const_iterator endRod() const { return m_rodNames.end(); }
    std::set<std::string>::const_iterator findRod(const std::string &rod_name) const { return m_rodNames.find(rod_name); }

    static const std::string &finalStatus(bool success) {
      return s_finalStatus[ (success ? 0 : 1) ];
    }

    ISInfoDictionary *dictionary() {
      return m_isDictionary.get();
    }

    bool verbose() const { return m_verbose; }

    const PixA::ConnectivityRef &conn() const { return m_conn; }

    void setScanConfig(PixA::ConfigHandle &scan_config_handle) { m_scanConfigHandle = scan_config_handle; }

    const PixA::ConfigHandle &scanConfig() const               { return m_scanConfigHandle; }

    void setHistoInputFileName(const std::string &histo_input_file_name);

    PixCon::Mutex &histoInputMutex() {return m_histoInputMutex; }

    std::vector<std::string> histoNameList();

    PixCon::DummyHistoInput *histoInput();

    void closeHistoInput();

    PixLib::PixHistoServerInterface *histoServerInterface() { return m_histoServerInterface.get();}

    std::map<std::string, std::shared_ptr<ISInfoInt> > &activeState() { return m_activeState;}

    class Transition_t {
    public:
      Transition_t(const std::string &state_name,
		   unsigned int state_time_in_ms,
		   float failed_probability,
		   float state_change_probability,
		   float transition_probability)
	: m_stateName(state_name),
	  m_stateTime(state_time_in_ms),
	  m_failedProb(failed_probability),
	  m_stateChangeProb(state_change_probability),
	  m_transitionProb(transition_probability)
      {}

      const std::string &stateName() const {return m_stateName; }
      unsigned int stateTime() const { return m_stateTime; }  //ms

      float failProb() const { return m_failedProb; }
      float transitionProb() const { return m_transitionProb; }
      float stateChangeProb() const { return m_stateChangeProb; }

    private:
      std::string m_stateName;
      unsigned int m_stateTime;
      float m_failedProb;
      float m_stateChangeProb;
      float m_transitionProb;

    };


    class IActionCommand
    {
    public:
      virtual ~IActionCommand() {}
      virtual void execute(bool &m_abort) = 0;
    };

  protected:

    std::set<std::string>           m_rodNames;
    const PixA::ConnectivityRef     m_conn;
    PixA::ConfigHandle              m_scanConfigHandle;
    std::unique_ptr<ISInfoDictionary> m_isDictionary;
    std::string                     m_isServerName;

    std::string                                    m_histoInputFileName;
    PixCon::Mutex                                  m_histoInputMutex;
    std::unique_ptr<PixCon::DummyHistoInput>         m_dummyHistoInput;
    std::unique_ptr<PixLib::PixHistoServerInterface> m_histoServerInterface;
    std::map<std::string, std::shared_ptr<ISInfoInt> > m_activeState;

    bool m_verbose;
    static const std::string s_finalStatus[2];
    static std::vector<std::string> s_allHistogramNames;
  };


  class DummyMultiAction;

  class DummySingleAction : public PixActions, public omni_thread
  {
  public:
    friend class DummyMultiAction;

    DummySingleAction(IPCPartition &partition,
		      const std::string &is_server_name,
		      const std::string &oh_server_name,
		      const std::string &histo_name_server_name,
		      const PixA::ConnectivityRef &conn,
		      const std::set<std::string>  &action_name,
		      bool m_verbose=false);

    ~DummySingleAction();

    void initRun(PixActions::SyncType) { assert(false); }

    void initCal(PixActions::SyncType);
    void resetRod(PixActions::SyncType);
    void configureBoc(PixActions::SyncType);

    void setup(PixActions::SyncType) { assert(false); }
    void configure(PixActions::SyncType) { assert(false); }
    void connect(PixActions::SyncType) { assert(false); }
    void prepareForRun(PixActions::SyncType) { assert(false); }
    void startTrigger(PixActions::SyncType) { assert(false); }
    void stopTrigger(PixActions::SyncType) { assert(false); }
    void stopEB(PixActions::SyncType) { assert(false); }
    void disconnect(PixActions::SyncType) { assert(false); }
    void unconfigure(PixActions::SyncType) { assert(false); }

    void reloadTags(std::string, std::string, long int, long int, PixLib::PixActions::SyncType) { assert(false); }

    void probe(PixActions::SyncType) { assert(false); }
    void resetmods(PixActions::SyncType) { assert(false); }
    void listRODs(PixActions::SyncType) { assert(false); }
    void disableFailed(PixActions::SyncType) { assert(false); }
    void reset(PixActions::SyncType) { assert(false); }
    void resetViset(PixActions::SyncType);

    void readConfig(PixActions::SyncType) { assert(false); }
    void loadConfig(PixActions::SyncType);
    void sendConfig(PixActions::SyncType) { assert(false); }
    void setActionsConfig(PixActions::SyncType) { assert(false); }

    void setPixScan(std::string, PixActions::SyncType);
    void setConnTest(std::string, PixActions::SyncType) { assert(false); }
    void getRodActive  (std::string name, PixActions::SyncType sync);

    void selectFE             (std::string /*actionName*/, long int /*modID*/, long int /*FE*/,     PixActions::SyncType /*sync*/) { assert(false); }
    void configSingleMod(std::string, PixLibLong, PixLibLong, PixActions::SyncType) { assert(false); }
    void setModuleActive(std::string action_name, PixLibLong module_id, bool, PixActions::SyncType);
    void getModuleActive(std::string, PixLibLong, PixActions::SyncType);

    void scan(std::string, PixLibLong, PixActions::SyncType);
    void presetScan(std::string, PixLibLong, PixLibLong, PixLibInt, PixActions::SyncType) { assert(false); } // T
    void execConnectivityTest(std::string, PixLibLong , PixActions::SyncType) { assert(false); }

    void setVcal(float, bool, PixActions::SyncType) { assert(false); }
//    void setMcc(PixActions::SyncType) { assert(false); }
    void incrMccDelay(float, bool, PixActions::SyncType) { assert(false); }

    void setRodActive(std::string, bool, PixActions::SyncType);
    void setRodActive(bool, PixActions::SyncType);

    void abortScan(PixActions::SyncType sync) { m_abort=true; m_commandFlag.setFlag(); }
    void abortConnTest(PixActions::SyncType ) { assert(false); };

    void rodStatus(PixActions::SyncType& ) { assert(false); }
    std::map<std::string,bool> rodStatus(){ assert(false); return std::map<std::string,bool>();}

    void saveConfig(std::string, PixActions::SyncType ) { assert(false); }
    void reloadConfig(std::string, PixActions::SyncType ) { assert(false); }

    void setRodBusyMask(PixLibLong, PixActions::SyncType) { assert(false); }

    void allocate      (const char* allocatingBrokerName);
    void deallocate    (const char* deallocatingBrokerName);

    std::string getConnTagC(const char*)  { return getTag(PixA::IConnectivity::kConn);}
    std::string getConnTagP(const char*)  { return getTag(PixA::IConnectivity::kPayload); }
    std::string getConnTagA(const char*)  { return getTag(PixA::IConnectivity::kAlias); }
    std::string getConnTagOI(const char*) { return getTag(PixA::IConnectivity::kId); }
    std::string getCfgTag(const char*)    { return getTag(PixA::IConnectivity::kConfig); }
    std::string getCfgTagOI  (const char* subActionName) { return getTag(PixA::IConnectivity::kId)+"-CFG"; };
    std::string getModCfgTag (const char* subActionName) { /*@todo add correct implementation*/ return getTag(PixA::IConnectivity::kModConfig); }

    std::string getTag(PixA::IConnectivity::EConnTag tag_type);

    /** Special methods to control the behaviour of the single action 
     * @group{
     */

    void setDuration( CORBA::ULong needed_time );
    void getStuckAfterNSteps( CORBA::ULong n_steps);
    void failAfterNSteps( CORBA::ULong fail_after_n_steps);

    void setHistoInputFileName(const char *file_name);
    void setHistoQuality(const char *conn_name, PixCon::DummyHistoInput::EQuality quality);
    void clearHistoQuality();

    /** }
    */

    void stop() {
      m_abort=true;
      m_shutdown=true;
      m_commandFlag.setFlag();
      m_exitFlag.wait();
    }

  protected:
    void *run_undetached(void *arg);

    DummyActionData &actionData() {return m_actionData; }

    const DummyActionData &actionData() const { return m_actionData; }

    void addCommand(DummyActionData::IActionCommand *a_command);

    std::shared_ptr<DummyActionData::IActionCommand> getCommand();

    bool inCharge(const std::string &conn_name);

  private:
    DummyActionData m_actionData;

    PixCon::Flag  m_commandFlag;
    PixCon::Flag  m_exitFlag;

    PixCon::Mutex m_commandListMutex;
    std::queue<std::shared_ptr< DummyActionData::IActionCommand > > m_commandList;
//    std::unique_ptr<PixMessages> m_pixMessages;

    unsigned int m_scanTimePerStep;
    unsigned int m_getStuckAfterNSteps;
    unsigned int m_failAfterNSteps;
    
    std::map<std::string, PixCon::DummyHistoInput::EQuality> m_histoQualityList;

    bool m_abort;
    bool m_shutdown;
  };

  template<class T> inline void destroyer(T * x ) {
    x->_destroy();
  }


  class DummyMultiAction : public PixActions
  {
  public:
    //    , destroyer<DummySingleAction>

    typedef std::shared_ptr< PixActions > ActionPtr_t;

    class VoidActionFunction :	public unary_function<std::pair<const std::string, ActionPtr_t>, void> {
    public:
      typedef void (PixActions::*VoidFunction_t )(PixActions::SyncType );
      VoidActionFunction( VoidFunction_t a_func, PixActions::SyncType arg) : m_func(a_func), m_arg(arg) {}
      void operator()(std::pair<const std::string, ActionPtr_t> &a) 
	{ 
	  ((*a.second).*m_func)(m_arg); 
	}
    private:
      VoidFunction_t m_func;
      PixActions::SyncType m_arg;
    };

    template <class T1>
    class VoidOneArgActionFunction :	public unary_function<std::pair<const std::string, ActionPtr_t>, void> {
    public:
      typedef void (PixActions::*VoidFunction_t )(T1, PixActions::SyncType );

      VoidOneArgActionFunction( VoidFunction_t a_func, T1 arg1, PixActions::SyncType arg ) : m_func(a_func), m_arg1(arg1), m_arg(arg) {}

      void operator()(std::pair<const std::string, ActionPtr_t> &a)
	{
	  ((*a.second).*m_func)(m_arg1, m_arg);
	}

    private:
      VoidFunction_t m_func;
      T1    m_arg1;
      PixActions::SyncType m_arg;
    };

    template <class T1, class T2>
    class VoidTwoArgActionFunction :	public unary_function<std::pair<const std::string, ActionPtr_t>, void> {
    public:
      typedef void (PixActions::*VoidFunction_t )(T1, T2, PixActions::SyncType );
      VoidTwoArgActionFunction( VoidFunction_t a_func, T1 arg1, T2 arg2, PixActions::SyncType arg ) : m_func(a_func), m_arg1(arg1), m_arg2(arg2), m_arg(arg) {}
      void operator()(std::pair<const std::string, ActionPtr_t> &a)
	{
	  ((*a.second).*m_func)(m_arg1, m_arg2, m_arg);
	}
    private:
      VoidFunction_t m_func;
      T1    m_arg1;
      T2    m_arg2;
      PixActions::SyncType m_arg;
    };

   template <class T1, class T2, class T3>
    class VoidThreeArgActionFunction :	public unary_function<std::pair<const std::string, ActionPtr_t>, void> {
    public:
      typedef void (PixActions::*VoidFunction_t )(T1, T2, T3, PixActions::SyncType );

      VoidThreeArgActionFunction( VoidFunction_t a_func, T1 arg1, T2 arg2, T3 arg3, PixActions::SyncType arg ) 
	: m_func(a_func), m_arg1(arg1), m_arg2(arg2), m_arg3(arg3), m_arg(arg) {}

      void operator()(std::pair<const std::string, ActionPtr_t> &a)
	{ 
	  ((*a.second).*m_func)(m_arg1, m_arg2, m_arg3, m_arg); 
	}
    private:
      VoidFunction_t m_func;
      T1    m_arg1;
      T2    m_arg2;
      T3    m_arg3;
      PixActions::SyncType m_arg;
    };


    DummyMultiAction(IPCPartition &partition, const PixA::ConnectivityRef &conn, const std::string &multi_action_name, bool m_verbose=false);

    ~DummyMultiAction();

    void initRun(PixActions::SyncType sync) { assert(false); }

    void initCal(PixActions::SyncType sync)      { for_each( m_actionList.begin(), m_actionList.end(), VoidActionFunction( &PixActions::initCal,     sync) );}
    void resetRod(PixActions::SyncType sync)     { for_each( m_actionList.begin(), m_actionList.end(), VoidActionFunction( &PixActions::resetRod,    sync) );}
    void configureBoc(PixActions::SyncType sync) { for_each( m_actionList.begin(), m_actionList.end(), VoidActionFunction( &PixActions::configureBoc, sync) );}

    void setup(PixActions::SyncType) { assert(false); }
    void configure(PixActions::SyncType) { assert(false); }
    void connect(PixActions::SyncType) { assert(false); }
    void prepareForRun(PixActions::SyncType) { assert(false); }
    void startTrigger(PixActions::SyncType) { assert(false); }
    void stopTrigger(PixActions::SyncType) { assert(false); }
    void stopEB(PixActions::SyncType) { assert(false); }
    void disconnect(PixActions::SyncType) { assert(false); }
    void unconfigure(PixActions::SyncType) { assert(false); }

    void reloadTags(std::string, std::string, long int, long int, PixLib::PixActions::SyncType) { assert(false); }

    void probe(PixActions::SyncType) { assert(false); }
    void resetmods(PixActions::SyncType) { assert(false); }
    void listRODs(PixActions::SyncType) { assert(false); }
    void disableFailed(PixActions::SyncType) { assert(false); }

    void reset(PixActions::SyncType) { assert(false); }
    void resetViset(PixActions::SyncType sync=false) { for_each( m_actionList.begin(), m_actionList.end(), VoidActionFunction( &PixActions::resetViset, sync) ); }

    void readConfig(PixActions::SyncType) { assert(false); }
    void loadConfig(PixActions::SyncType sync) { for_each( m_actionList.begin(), m_actionList.end(), VoidActionFunction( &PixActions::loadConfig,    sync) );}
    void sendConfig(PixActions::SyncType) { assert(false); }
    void setActionsConfig(PixActions::SyncType) { assert(false); }

    void setPixScan(std::string config_file_name, PixActions::SyncType sync)
    { for_each( m_actionList.begin(), m_actionList.end(), VoidOneArgActionFunction<std::string>(&PixActions::setPixScan, config_file_name, sync) );}
    void setConnTest(std::string, PixActions::SyncType) { assert(false); }
    void getRodActive(std::string, PixActions::SyncType) { assert(false); }

    void selectFE(std::string, PixLibLong, PixLibLong, PixActions::SyncType) { assert(false); }
    void configSingleMod(std::string, PixLibLong, PixLibLong, PixActions::SyncType) { assert(false); }
    void getModuleActive(std::string, PixLibLong, PixActions::SyncType) { assert(false); }
    void setModuleActive(std::string action_name, PixLibLong mod_id, bool active, PixActions::SyncType sync)
      { long int casted_mod_id = static_cast<long int>(mod_id);
	for_each( m_actionList.begin(), m_actionList.end(), VoidThreeArgActionFunction<std::string,long int,bool>(&PixActions::setModuleActive,
														  action_name,
														  casted_mod_id,
														  active,
														  sync) ); }

    void scan(std::string output_file_name, PixLibLong serial_number, PixActions::SyncType sync)
    { for_each( m_actionList.begin(),
		m_actionList.end(),
		VoidTwoArgActionFunction<std::string,PixLibLong>(&PixActions::scan, output_file_name, serial_number, sync) ); }

    void presetScan(std::string, PixLibLong, PixLibLong, PixLibInt, PixActions::SyncType) { assert(false); } // T:
    void execConnectivityTest(std::string, PixLibLong , PixActions::SyncType) { assert(false); }

    void setVcal(float, bool, PixActions::SyncType) { assert(false); }
    //    void setMcc(PixActions::SyncType) { assert(false); }
    //    void setTFDACs(std::string, PixActions::SyncType, PixActions::SyncType) { assert(false); }
    void incrMccDelay(float, bool, PixActions::SyncType) { assert(false); }
    //    void writeConfig(std::string, PixLibLong, PixActions::SyncType) { assert(false); }

    void abortScan(PixActions::SyncType sync=false)
    { for_each( m_actionList.begin(), m_actionList.end(), VoidActionFunction( &PixActions::abortScan, sync) ); }
    void abortConnTest(PixActions::SyncType ) { assert(false); };

    void rodStatus(bool&) {assert(false); }
    std::map<std::string,bool> rodStatus(){ assert(false); return std::map<std::string,bool>();}

    void saveConfig(std::string, PixActions::SyncType) { assert(false); }
    void reloadConfig(std::string, PixActions::SyncType) { assert(false); }

    void setRodBusyMask(PixLibLong, PixActions::SyncType) { assert(false); }

    std::string getConnTagC(const char*)  { return getTag(PixA::IConnectivity::kConn);}
    std::string getConnTagP(const char*)  { return getTag(PixA::IConnectivity::kPayload); }
    std::string getConnTagA(const char*)  { return getTag(PixA::IConnectivity::kAlias); }
    std::string getConnTagOI(const char*) { return getTag(PixA::IConnectivity::kId); }
    std::string getCfgTagOI(const char*) { return getTag(PixA::IConnectivity::kId)+"-CFG"; }
    std::string getCfgTag(const char*)    { return getTag(PixA::IConnectivity::kConfig); }
    std::string getModCfgTag(const char*)    { return getTag(PixA::IConnectivity::kModConfig); }

    std::string getTag(PixA::IConnectivity::EConnTag tag_type);

    void setRodActive(std::string action_name, bool active, PixActions::SyncType sync)
      { for_each( m_actionList.begin(), m_actionList.end(), VoidTwoArgActionFunction<std::string,bool>(&PixActions::setRodActive,
												  action_name,
												  active,
												  sync) ); }

    void setRodActive(bool active, PixActions::SyncType sync)
    { assert(false); }

    // non PixActions methods
    PixActions *addAction(const std::string &action_name, PixActions *an_action);
    void removeAction(PixActions *an_action);
    bool deallocateActions(const char *action_name, const char *deallocating_broker_name);
    bool  deallocateActions(PixActions *an_action, const char *deallocating_broker_name);
    bool isAvailable(const std::string &action_name) const;
    std::set<std::string> listActions(bool checkAvailability,
				      PixLib::PixActions::Type type,
				      std::string allocatingBrokerName,
				      std::string managingBrokerName,
				      std::string caller);


  private:
    std::map<std::string, std::shared_ptr< PixActions > > m_actionList;
//    std::unique_ptr<PixMessages> m_pixMessages;
    PixA::ConnectivityRef m_conn;
    bool m_verbose;
  };

}

#endif
