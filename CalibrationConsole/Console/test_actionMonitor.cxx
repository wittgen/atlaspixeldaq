#include <ipc/partition.h>
#include <ipc/core.h>

#include <is/infoT.h>
#include <is/inforeceiver.h>
#include <is/infoiterator.h>
#include <is/infodictionary.h>

#include "test_isUtil.h"

#include <qthread.h>
#include <qapplication.h>

#include "test_Broker.h"
//#include "test_DummyMultiAction.h"
#include <ConfigWrapper/Connectivity.h>
#include <ConfigWrapper/ConnectivityUtil.h>
#include <Flag.h>

#include <string>



bool s_abort;
PixCon::Flag s_flag;

template <class T>
class CORBA_ptr
{
public:
  CORBA_ptr(T *ptr) : m_ptr(ptr) {}
  ~CORBA_ptr() { if (m_ptr) m_ptr->_destroy(); m_ptr=NULL; }

  T &operator*() { return m_ptr; }
  const T &operator*() const { return m_ptr; }

  T *operator->() { return m_ptr; }
  const T *operator->() const { return m_ptr; }

private:
  T *m_ptr;
};

class IsListener : public QThread
{
public:
  enum ERodStatus { kUnknown, kRunning, kSuccess, kFailed};

  IsListener(const std::string &partition_name,
	     const std::string &is_server_name,
	     const std::set<std::string> &action_list,
	     const std::vector<std::string> &sub_action_list,
	     bool verbose=false);

  void run() {m_isReceiver.run();}

  void shutdown() { m_isReceiver.stop(); }
  void setVerbose(bool verbose) { m_verbose=true; }

  bool isAllDone() const;

protected:
  void statusChanged(ISCallbackInfo *info);
  void actionChanged(ISCallbackInfo *info);

  std::string extract(const std::string &is_name) const;
  void updateAction(const std::string &is_name, const std::string &action_name);
  void updateStatus(const std::string &is_name, const std::string &status_name);

private:
  IPCPartition   m_isPartition;
  ISInfoReceiver m_isReceiver;

  class RodState_t {
  public:
    RodState_t() : m_status(kUnknown), m_currentAction(s_unknownCurrentAction), m_state(s_unknownCurrentAction), m_isDone(false) {}
    RodState_t(ERodStatus status, unsigned int state) : m_status(status), m_currentAction(state), m_state(state), m_isDone(false) {}

    void setState( unsigned int new_state, bool success) {
      if (new_state==0 && success) {
	m_status=kRunning;
	m_state=0;
      }
      else if (!success) {
	m_status=kFailed;
      }
      else if (new_state!=m_state+1) {
	m_status=kFailed;
      }
      m_state=new_state;
    }

    void updateState( bool success) {
      unsigned int new_state = m_currentAction;
      if (new_state==0 && success) {
	m_status=kRunning;
	m_state=0;
      }
      else if (!success) {
	m_status=kFailed;
      }
      else if (new_state!=m_state+1) {
	m_status=kFailed;
      }
      m_state=new_state;
    }


    void setUnknown()  { if (m_status != kFailed) m_status = kUnknown; }

    ERodStatus status() const          { return m_status; }
    unsigned int state() const         { return m_state; }

    unsigned int currentAction() const { return m_currentAction; }

    void setDone()                     { m_isDone=true; }
    bool isDone() const                { return status()==kFailed || m_isDone; }

    void setCurrentAction( unsigned int current_action) { m_currentAction=current_action; }
    void setCurrentActionUnknown( )                     { m_currentAction=s_unknownCurrentAction; }
    bool currentActionIsUnknown( ) const                { return m_currentAction == s_unknownCurrentAction; }

  private:
    ERodStatus   m_status;
    unsigned int m_currentAction;
    unsigned int m_state;
    bool         m_isDone;
    static const unsigned int s_unknownCurrentAction = 0-1;
  };

  enum EActionStatus {kActionStatusUnknown, kActionSuccess, kActionFailed};

  //  std::map<std::string, unsigned int> m_currentAction;
  std::map<std::string, RodState_t> m_rodStatus;
  std::vector<std::string> m_subActionList;
  std::map<std::string, EActionStatus> m_actionStatusNames;
  bool m_verbose;
};

IsListener::IsListener(const std::string &partition_name,
	     const std::string &is_server_name,
	     const std::set<std::string> &action_list,
	     const std::vector<std::string> &sub_action_list,
	     bool verbose)
  : m_isPartition(partition_name),
    m_isReceiver(m_isPartition),
    m_subActionList(sub_action_list),
    m_verbose(verbose)
{ 
  m_actionStatusNames["DONE"]=kActionSuccess;
  m_actionStatusNames["FAILED"]=kActionFailed;

  for (std::set<std::string>::const_iterator action_iter = action_list.begin();
       action_iter != action_list.end();
       action_iter++) {
      unsigned int len = strlen("SINGLE_ROD:");
      if (action_iter->compare(0,len, "SINGLE_ROD:")==0) {
	std::string::size_type pos = action_iter->find(":",len+1);
	if (pos != std::string::npos && pos+1 < action_iter->size()) {

	  m_rodStatus.insert(std::make_pair(action_iter->substr(pos+1, action_iter->size()-pos-1),RodState_t()));
	}
      }
  }
  // get initial config values from IS server
  std::string name(".*");


  ISInfoT<std::string> is_value;
  {
    ISCriteria criteria = is_value.type() && "ROD_CRATE.*/CURRENT-ACTION";
    ISInfoIterator ii( m_isPartition, is_server_name,  criteria);
    while( ii() ) {
      ii.value(is_value);
      updateAction(ii.name(), is_value.getValue());
    }
    m_isReceiver.subscribe(is_server_name.c_str(),criteria,&IsListener::actionChanged, this);
  }


  {
    ISCriteria criteria = is_value.type() && "ROD_CRATE.*/STATUS";

    ISInfoIterator ii( m_isPartition, is_server_name,  criteria);
    while( ii() ) {

      ii.value(is_value);
      updateStatus(ii.name(), is_value.getValue());
    }
    m_isReceiver.subscribe(is_server_name.c_str(),criteria,&IsListener::statusChanged, this);
  }
}

bool IsListener::isAllDone() const {
  ERodStatus combined_status=kSuccess;
  for (std::map<std::string, RodState_t>::const_iterator rod_iter = m_rodStatus.begin();
       rod_iter != m_rodStatus.end();
       rod_iter++) {
    if (!rod_iter->second.isDone()) {
      if (m_verbose) {
	std::cout << "INFO [IsListener::isAllDone] at least ROD " << rod_iter->first << " is not done yet." << std::endl; 
      }
      return false;
    }
    if (combined_status != kFailed) {
      if (rod_iter->second.status() == kFailed) {
	combined_status = kFailed;
      }
      else if (rod_iter->second.status() == kUnknown) {
	combined_status = kUnknown;
      }
    }
  }

  if (m_verbose) {
    std::cout << "INFO [IsListener::isAllDone] all done. Combined status = " << combined_status << std::endl; 
  }
  return true;
}


void IsListener::updateAction(const std::string &is_name, const std::string &action_name)
{
  if (m_verbose) {
    std::cout << "INFO [IsListener::updateAction] IS var " << is_name << " changed to " << action_name << "." << std::endl; 
  }
  std::string rod_name = extract(is_name);
  if (!rod_name.empty()) {
    std::vector<std::string>::const_iterator sub_action_iter = find(m_subActionList.begin(),m_subActionList.end(), action_name);
    
    std::map<std::string, RodState_t>::iterator rod_iter = m_rodStatus.find( rod_name);
    if (rod_iter != m_rodStatus.end()) {
      if (sub_action_iter != m_subActionList.end()) {
	rod_iter->second.setCurrentAction( static_cast<unsigned int>(sub_action_iter - m_subActionList.begin()) );
      }
      else {
	rod_iter->second.setCurrentActionUnknown();
      }
      if (m_verbose) {
	std::cout << "INFO [IsListener::updateAction] Set ROD " << rod_name << " to state" << rod_iter->second.currentAction() << "." << std::endl; 
      }
    }
  }
}

void IsListener::updateStatus(const std::string &is_name, const std::string &status_name)
{
  if (m_verbose) {
    std::cout << "INFO [IsListener::updateStatus] IS var " << is_name << " changed to " << status_name << "." << std::endl; 
  }
  std::string rod_name = extract(is_name);
  if (!rod_name.empty()) {
    std::map<std::string, RodState_t >::iterator rod_iter = m_rodStatus.find(rod_name);
    if (rod_iter != m_rodStatus.end()) {

      if (!rod_iter->second.currentActionIsUnknown()) {
	std::map<std::string, EActionStatus>::const_iterator status_iter = m_actionStatusNames.find(status_name);
	if (status_iter != m_actionStatusNames.end()) {
	  rod_iter->second.updateState( status_iter->second == kActionSuccess);
	}
	else {
	  rod_iter->second.setUnknown();
	}
      }
      else {
	rod_iter->second.setUnknown();
      }

      if (m_verbose) {
	std::cout << "INFO [IsListener::updateStatus] Status of ROD " << rod_name << " is " << static_cast<unsigned int>(rod_iter->second.status()) << " state="
		  << static_cast<unsigned int>(rod_iter->second.state()) << " ." << std::endl;
      }

      if (rod_iter->second.state()+1==m_subActionList.size() ) {
	rod_iter->second.setDone();
      }
      if (rod_iter->second.isDone()) {
	s_flag.setFlag();
      }

      if (rod_iter->second.state()+1==m_subActionList.size()) {
	if (m_verbose) {
	  std::cout << "INFO [IsListener::updateAction] ROD " << rod_name << " reach final state " << static_cast<unsigned int>(rod_iter->second.status()) << "." << std::endl;
	}
      }

    }
  }
}

std::string IsListener::extract(const std::string &is_name) const
{
  std::string::size_type pos  = is_name.find(".");
  if (pos != std::string::npos && pos+1<is_name.size()) {
    std::string::size_type start_pos  = is_name.find("/",pos+1);
    if (start_pos != std::string::npos) {
      std::string::size_type end_pos  = is_name.find("/",start_pos+1);
      if (end_pos != std::string::npos) {
	return is_name.substr(start_pos+1,end_pos-start_pos-1);
      }
    }
  }
  return "";
}

void IsListener::statusChanged(ISCallbackInfo *info)
{
  ISInfoT<std::string> is_value;
  if (info->type() == is_value.type()) {
    info->value(is_value);
    updateStatus(info->name(), is_value.getValue());
  }
}

void IsListener::actionChanged(ISCallbackInfo *info)
{
  ISInfoT<std::string> is_value;
  if (info->type() == is_value.type()) {
    info->value(is_value);
    updateAction(info->name(), is_value.getValue());
  }
}




int main(int argc, char **argv)
{
  std::string partition_name;
  std::string is_name = "CAN";
  std::string id_tag="CT";
  std::string tags="CT-C1OSP-V3";
  std::string cfg_tag="CT-C12OSP";
  std::string broker_name = "";
  std::vector<std::string> rod_names;
  bool verbose=false;
  bool error=false;


  for (int arg_i=1; arg_i<argc; arg_i++) {
    if (strcmp(argv[arg_i],"-p")==0 && arg_i+1<argc) {
      partition_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-n")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      is_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-i")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      id_tag = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-t")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      tags = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-c")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      cfg_tag = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-v")==0 ) {
      verbose=true;
    }
    else {
      if (argv[arg_i][0]!='-') {
	rod_names.push_back(argv[arg_i]);
      }
      else {
	std::cout << "ERROR ["<< argv[0] << ":main] unhandled argument " << arg_i << " : " << argv[arg_i]<< "." << std::endl;
	error=true;
      }
    }
  }

  if (error || is_name.empty() || partition_name.empty()) {
    std::cout << "usage " << argv[0] << "-p partition [-n is-server-name] [-i id-tag] [-t tags] [-c cfg-tag]" << std::endl;
  }
  // Start IPCCore
  IPCCore::init(argc, argv);
  
  IPCPartition partition(partition_name);
  ISInfoDictionary is_dictionary(partition);

  QApplication app( argc, argv );


  {
    ISInfoT<std::string> is_value;
  std::vector<std::string> var_names;
  var_names.push_back("/STATUS");
  var_names.push_back("/CURRENT-ACTION");
  for (std::vector<std::string>::const_iterator var_name_iter = var_names.begin();
       var_name_iter != var_names.end();
       var_name_iter++) {
    
    ISCriteria criteria = is_value.type() && std::string("ROD_CRATE.*/")+*var_name_iter;
    ISInfoIterator ii( partition, is_name,  criteria);
    while( ii() ) {
      is_dictionary.remove(ii.name());
      //    std::cout << "INFO [IsListener::ctor] " << ii.name()  << " : " << is_value.getValue() << std::endl;
    }
  }
  }




  //  CORBA_ptr<PixLib::DummyBroker> broker(new PixLib::DummyBroker(partition,id_tag,tags, cfg_tag, broker_name, verbose));
  std::set<std::string> all_actions = broker->listActions();
  if (!rod_names.empty()) {
    std::set<std::string> actions;
    for (std::vector<std::string>::const_iterator rod_name_iter = rod_names.begin();
	 rod_name_iter != rod_names.end();
	 rod_name_iter++) {

      for (std::set<std::string>::const_iterator action_iter = all_actions.begin();
	   action_iter != all_actions.end();
	   action_iter++) {
	if (action_iter->find(*rod_name_iter)!= std::string::npos) {
	  actions.insert( *action_iter );
	}
      }
    }
    all_actions = actions;
  }

  PixLib::PixActions *an_action = broker->allocateActions(all_actions);
  std::vector<std::string> sub_action_list;
  sub_action_list.push_back("reset");
  sub_action_list.push_back("loadConfig");

  IsListener listener(partition_name,is_name, all_actions, sub_action_list, verbose);


  an_action->initCal();

  while (!s_abort ) {
    s_flag.wait(s_abort);
    if (!s_abort) {
      if (listener.isAllDone()) break;
    }
  }
  listener.shutdown();
  
  //  broker.deallocateActions( an_action );
}

