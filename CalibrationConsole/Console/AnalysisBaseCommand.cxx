#include "AnalysisBaseCommand.h"

#include <CoralDbClient.hh>

#include <PixDbGlobalMutex.hh>
#include <ScanConfigDb.hh>
#include <ObjectConfigDbInstance.hh>
#include <DataContainer/RootResultFileAccessBase.h>
#include <ConvertGlobalStatus.h>

//#include <IScanParameterDb.h>
#include <ScanConfigDbInstance.hh>
#include "ScanSelection.h"
#include <qmessagebox.h>
//#include "PixMetaException.hh"

//#include <ipc/core.h>
#include <Scheduler_ref.hh>

#include <ConfigWrapper/ConfObjUtil.h>

#include <MetaDataUtil.h>

namespace PixCon {

  std::string AnalysisBaseCommand::s_empty;

  PixCon::Result_t &AnalysisBaseCommand::loadScan(SerialNumber_t scan_serial_number, PixCon::Result_t &ret)
  {

    if (!loadScanMetaData(m_engine->calibrationData(), scan_serial_number)) {
      ret.setFailure();
      return ret;
    }

    std::vector<unsigned short> start_counter;
    std::vector<unsigned short> max_counter;
    unsigned int mask_stage_loop_index=1;
    PixA::ConfigHandle scan_config;
    {
      Lock lock(m_engine->calibrationData().calibrationDataMutex());
      scan_config = m_engine->calibrationData().scanConfig(kScan, scan_serial_number, "");
    }
    if (!scan_config) {
      std::cerr << "ERROR [AnalysisBaseCommand::loadScan] No scan configuration for "
		<< makeSerialNumberString(kScan,scan_serial_number) << "." << std::endl;
      ret.setFailure();
      return ret;
    }
    setLoopCounters(scan_config, start_counter, max_counter, mask_stage_loop_index);

    m_engine->statusChangeTransceiver()->signal(kScan,scan_serial_number);

    //@todo need to call? Also in ScanCommand ? :  m_ohHistogramProvider->setUptodate(kScan,scan_serial_number);

    if (!m_engine->statusMonitorActivator()->activateScanMonitoring( scan_serial_number, mask_stage_loop_index, start_counter, max_counter, 60 )) {
      ret.setFailure();
    }

    return ret;
  }

  PixCon::Result_t &AnalysisBaseCommand::startAnalysis( PixCon::Result_t &ret ) {

    if (!m_analysisInfo.get()) {
      ret.setFailure();
      return ret;
    }

    IPCPartition &the_partition = m_engine->partition();
    if (!the_partition.isValid()) {
      std::cerr << "ERROR [AnalysisBaseCommand::submitAnalysis] No valid partition." << std::endl;
      ret.setFailure();
      return ret;
    }

    bool scheduler_comm_error=false;

    try {

      // get CORBA reference to the scheduler
      CAN::Scheduler_ref manitu (the_partition); // Should throw an exception if the scheduler does not exist in the 
      // given partition
      std::cout << "INFO [AnalysisBaseCommand::submitAnalysis] submit analysis : " << std::endl << "      "
		<< m_analysisInfo->objectInfo(CAN::kAlg).name() << "/"
		<< m_analysisInfo->objectInfo(CAN::kAlg).tag() << "/"
		<< m_analysisInfo->objectInfo(CAN::kAlg).revision() << std::endl << "      "
		<< m_analysisInfo->objectInfo(CAN::kClassification).name() << "/"
		<< m_analysisInfo->objectInfo(CAN::kClassification).tag() << "/"
		<< m_analysisInfo->objectInfo(CAN::kClassification).revision() << std::endl << "      "
		<< m_analysisInfo->objectInfo(CAN::kPostProcessing).name() << "/"
		<< m_analysisInfo->objectInfo(CAN::kPostProcessing).tag() << "/"
		<< m_analysisInfo->objectInfo(CAN::kPostProcessing).revision() << std::endl << "      "
		<< " analysis of "
		<< m_analysisInfo->scanSerialNumber()
		<< std::endl;

      CORBA::ULong jobs_waiting_for_data;
      CORBA::ULong jobs_waiting;
      CORBA::ULong jobs_running;
      CORBA::ULong empty_slots;

      // check whether the analysis infrastructure is up
      manitu.getQueueStatus(jobs_waiting_for_data, jobs_waiting, jobs_running, empty_slots);

      if (jobs_running+empty_slots<=0) {
	std::cerr << "WARNING [AnalysisBaseCommand::submitAnalysis] Scheduler does not seem to offer any processing slots. "
		  << "Maybe a new worker is about to be started automatically."  << std::endl;
      }
      
      {

	// check wehter the scan is already known
	{
	  const ScanMetaData_t *scan_meta_data = NULL;
	  {
	    Lock lock(m_engine->calibrationData().calibrationDataMutex());
	    scan_meta_data = m_engine->calibrationData().scanMetaDataCatalogue().find( m_analysisInfo->scanSerialNumber() );
	  }
	  if (!scan_meta_data) {
	    // otherwise load the scan meta data, and histogram information (how?)
	    loadScan( m_analysisInfo->scanSerialNumber(), ret );
	  }
	}

	// prepate analysis :
	// get serial number
	CAN::PixCoralDbClient meta_data_client(true);
	ret.setAnalysisSerialNumber( meta_data_client.getNewSerialNumber(CAN::kAnalysisNumber) );

	// update analysis meta data
	m_analysisInfo->setAnalysisStart( time(0) );

	// .. store meta data
	meta_data_client.storeMetadata<CAN::AnalysisInfo_t>(ret.analysisSerialNumber(),m_analysisInfo.get());

	// .. and copy meta data to internal catalogues.
	//@todo  how to deal with analysis configurations ? Do not store them ? Keep them in memory ? Load on demand ? 
	setAnalysisInfo(m_engine->calibrationData(), ret.analysisSerialNumber(), m_analysisInfo.get());


	// first activate monitoring
	if (!m_engine->statusMonitorActivator()->activateAnalysisMonitoring( ret.analysisSerialNumber() )) {
	  ret.setFailure();
	  return ret;
	}
	else {

	  // now submit the analysis

	  // the cleanup helper will abort the monitoring and set the return status in case of exceptions 
	  // to failed.
	  AnalysisCleanUpHelper helper( *m_engine, ret);

	  CAN::AnalysisInfoData_t submit_analysis_data;
	  CAN::copyAnalysisInfo(*m_analysisInfo, submit_analysis_data);

	  manitu.submitAnalysis(ret.analysisSerialNumber(), submit_analysis_data);
	  std::cerr << "INFO [AnalysisBaseCommand::startAnalysis] started analysis : " << ret.analysisSerialNumber() << "."  << std::endl;

	  // update meta data

	  m_engine->calibrationData().updateStatusSafe(kAnalysis, ret.analysisSerialNumber() ,MetaData_t::kRunning);
	  m_engine->statusChangeTransceiver()->signal(kAnalysis,ret.analysisSerialNumber() );
	  helper.doNotCleanup();
	}
      }
    }
    catch (CAN::MRSException &err) {
      std::cerr << "ERROR [AnalysisBaseCommand::submitAnalysis] Caught exception " << err.getDescriptor() << std::endl;
      scheduler_comm_error=true;
    }
    catch (CORBA::TRANSIENT& err) {
      scheduler_comm_error=true;
    }
    catch (CORBA::COMM_FAILURE &err) {
      scheduler_comm_error=true;
    }
    catch (std::exception &err) {
      std::cerr << "ERROR [AnalysisBaseCommand::submitAnalysis] Caught std exception " << err.what() << std::endl;
      scheduler_comm_error=true;
    }
    catch (...) {
      scheduler_comm_error=true;
    }

    if (scheduler_comm_error) {
    std::cerr << "ERROR [AnalysisCommand::submitAnalysis] Communication error with scheduler." << std::endl;
    }
    return ret;
  }

  PixCon::Result_t &AnalysisBaseCommand::waitForAnalysis( PixCon::Result_t &ret)
  {
    if (ret.analysisSerialNumber()>0) {
      AnalysisCleanUpHelper helper( *m_engine, ret);
      // wait until the analysis has terminated.
      std::cerr << "INFO [AnalysisBaseCommand::waitForAnalysis] wait for analysis : " << ret.analysisSerialNumber() << "."  << std::endl;
      m_engine->statusChangeTransceiver()->waitFor(kAnalysis, ret.analysisSerialNumber(),  m_abort );
      std::cerr << "INFO [AnalysisBaseCommand::waitForAnalysis] exit wait. " << ret.analysisSerialNumber() << "."  << std::endl;
      CAN::GlobalStatus analysis_status=CAN::kNotSet;

      if (m_abort && m_engine->calibrationData().getStatusSafe(kAnalysis, ret.analysisSerialNumber())== MetaData_t::kRunning) {
	std::cerr << "INFO [AnalysisBaseCommand::waitForAnalysis] aborted: " << ret.analysisSerialNumber() << "."  << std::endl;

	sendAbort(ret);

	if (m_kill) {
	  ret.setKilled();
	}
	else {
	  ret.setAborted();
	}
	analysis_status=CAN::kAborted;
      }
      else {
	helper.doNotCleanup();
	analysis_status=CAN::kSuccess;
	ret.setSuccess();
      }

      // update meta data
      try {
	if (m_engine->calibrationData().status(kAnalysis,ret.analysisSerialNumber()) >= MetaData_t::kUnknown) {

	  CAN::PixCoralDbClient meta_data_client(true);
	  meta_data_client.updateAnalysisMetadataEnd(ret.analysisSerialNumber(), time(0) );
	  meta_data_client.updateAnalysisMetadataStatus(ret.analysisSerialNumber(), analysis_status);
	  //@todo notify the gui that the meta data has changed
	  m_engine->calibrationData().updateStatusSafe(kAnalysis,ret.analysisSerialNumber(),ConvertGlobalStatus::convertStatus(analysis_status));
	  m_engine->statusChangeTransceiver()->signal(kAnalysis,ret.analysisSerialNumber());
	}
      }
      catch(...) {
	// not even wrote scan meta data
	ret.setFailure();
      }
    }

    return ret;
  }

  bool AnalysisBaseCommand::configureAnalysis(ScanSelection &selection, SerialNumber_t scan_serial_number_arg, bool scan_serial_number_is_set_later)
  {
    if (!selection.m_analyseScanCheckBox->isChecked()) return false;

    std::map<std::string, std::pair<CAN::ESerialNumberType, CAN::SerialNumber_t> > named_sources;

    if (scan_serial_number_arg==0 && !scan_serial_number_is_set_later) {
      SerialNumber_t scan_serial_number = selection.scanSerialNumber();
      if (scan_serial_number==0) {
	//inserted by peter
	named_sources = selection.getNamedSources();
      }
      if (named_sources.size()==0) {
	//end insert
	QMessageBox::warning(NULL,"ERROR [AnalysisBaseCommand::configure]","Illegal serial number.","OK");
	return false;
      }
    }

    std::vector<CAN::ObjectInfo_t> obj_list;
    ScanSelection::EClass classes[3]={ScanSelection::kAnalysis,ScanSelection::kClassification, ScanSelection::kPostProcessing};

    for (unsigned int obj_i=0; obj_i<3; obj_i++) {
      obj_list.push_back(CAN::ObjectInfo_t(selection.selectedType(classes[obj_i]),
					   selection.selectedTag(classes[obj_i]),
					   selection.selectedRevision(classes[obj_i])));
      if (obj_list.back().name().empty() || obj_list.back().tag().empty()) {
	std::cerr << "ERROR [AnalysisBaseCommand::configure] No name or configuration tag given for object "
		  << CAN::ObjectConfigDbInstance::objectClassName( static_cast<CAN::ObjectConfigDbInstance::EClass>(obj_i)) << "." << std::endl;
      }
    }
    assert(obj_list.size()==3);
    if (named_sources.size() != 0){
      scan_serial_number_arg =  named_sources.begin()->second.second;
    }

    m_analysisInfo = std::make_unique<CAN::AnalysisInfo_t>(obj_list[0],obj_list[1],obj_list[2], scan_serial_number_arg, 0);
    //inserted by peter to handle multiple sources
    if (named_sources.size() != 0){
      for(std::map<std::string, std::pair<CAN::ESerialNumberType, CAN::SerialNumber_t> >::const_iterator source_iter = named_sources.begin();
	  source_iter != named_sources.end();
	  source_iter++) {
	m_analysisInfo->addSource(source_iter->first, source_iter->second.first, source_iter->second.second);
	std::cout << "adding source " << source_iter->first << " with serial number " << source_iter->second.second << std::endl;
      }
    }
      //end insert

    return true;
  }

  bool AnalysisBaseCommand::changeScanSerialNumber(SerialNumber_t scan_serial_number) {
    if (scan_serial_number==0) return false;
    m_analysisInfo = std::make_unique<CAN::AnalysisInfo_t>(m_analysisInfo->objectInfo(CAN::kAlg),
						 m_analysisInfo->objectInfo(CAN::kClassification),
						 m_analysisInfo->objectInfo(CAN::kPostProcessing),
						 scan_serial_number,
						 m_analysisInfo->srcAnalysisSerialNumber(),
						 m_analysisInfo->quality(),
						 m_analysisInfo->comment());
    return true;
  }


  void AnalysisBaseCommand::setLoopCounters( PixA::ConfigHandle &scan_config_handle,
					     std::vector<unsigned short> &start_value,
					     std::vector<unsigned short> &max_counter,
					     unsigned int &mask_stage_loop)
  {
    start_value.clear();
    max_counter.clear();
    unsigned int first_loop_not_on_dsp=5;
    if (scan_config_handle) {
      try {
	// get loop counter range for monitoring
	PixA::ConfigRef the_scan_config = scan_config_handle.ref();

	PixA::ConfGrpRef general_config( the_scan_config["general"] );
	max_counter.push_back( static_cast<unsigned short>( confVal<int>(general_config["maskStageSteps"]) ));

	const PixA::ConfGrpRef &loop_config(the_scan_config["loops"]);
	// last index first
	for (unsigned int loop_i=3; loop_i-->0;) {
	  std::stringstream loop_idx;
	  loop_idx << loop_i;

	  if (!confVal<bool>(loop_config[std::string("dspProcessingLoop_")+loop_idx.str()])) {
	    if (first_loop_not_on_dsp < 3 && loop_i+1!=first_loop_not_on_dsp) {
	      std::cerr << "WARNING [AnalysisBaseCommand::setLoopCounters] loops executed on DSP are interleaved with external loops. Is that possible ?"
			<< std::endl;
	    }
	    first_loop_not_on_dsp=loop_i;
	  }

	  if (!confVal<bool>(loop_config[std::string("activeLoop_")+loop_idx.str()])) {
	    max_counter.push_back(0);
	  }
	  else {
	    bool uniform = confVal<bool>(loop_config[std::string("loopVarUniformLoop_")+loop_idx.str()]);
	    if (!uniform) {
	      const std::vector<float> &values ( confVal<std::vector<float> >(loop_config[std::string("loopVarValuesLoop_")+loop_idx.str()]));
	      max_counter.push_back( values.size() );
	    }
	    else {
	      max_counter.push_back( static_cast<unsigned short>( confVal<int>(loop_config[std::string("loopVarNStepsLoop_")+loop_idx.str()]) ));
	    }
	  }
// 	  std::cout << "INFO [setLoopCounters] " << loop_i << " max=" << max_counter.back() << " on dsp ? " 
// 		    << confVal<bool>(loop_config[std::string("dspProcessingLoop_")+loop_idx.str()]) 
// 		    << " -> last dsp loop =" << first_loop_not_on_dsp
// 		    << std::endl;

	}

	bool dsp_mask_staging=true;
	mask_stage_loop=1;
	try {
	  dsp_mask_staging = confVal<bool>(loop_config["dspMaskStaging"]);
	}
	catch(PixA::BaseException &err) {
	  std::cerr << "ERROR [AnalysisBaseCommand::setLoopCounters] Caught exception :" << err.getDescriptor() << std::endl;
	}
	catch(...) {
	}

	// This seems to be always set to true
	// 	try {
	// 	  if (confVal<bool>(loop_config["innerLoopSwap"])) {
	// 	    mask_stage_loop=0;
	// 	  }
	// 	}
	// 	catch(PixA::BaseException &err) {
	// 	  std::cerr << "ERROR [AnalysisBaseCommand::setLoopCounters] Caught exception :" << err.getDescriptor() << std::endl;
	// 	}
	// 	catch(...) {
	// 	}

	try {
	  int temp_mask_stage_loop = confVal<int>(loop_config["Mask Loop Position"]);
	  if (mask_stage_loop!=0 && temp_mask_stage_loop!=1 && temp_mask_stage_loop>=0) {
	    // only consider the mask loop position if it is not the default value.
	    mask_stage_loop=static_cast<unsigned int>(temp_mask_stage_loop);
	  }
	}
	catch(...) {
	  if (dsp_mask_staging) {
	    if (mask_stage_loop>=first_loop_not_on_dsp) {
	      mask_stage_loop=first_loop_not_on_dsp;
	    }
	  }
	  else {
	    if (mask_stage_loop<first_loop_not_on_dsp) {
	      mask_stage_loop=first_loop_not_on_dsp;
	    }
	  }
	}

	if (dsp_mask_staging) {
	  if (mask_stage_loop>=first_loop_not_on_dsp) {
	    std::cerr << "WARNING [AnalysisBaseCommand::setLoopCounters] Mask stage loop is positioned after an external loop. Does this make sense ?"
		      << std::endl;
	  }
	}
	else {
	  if (mask_stage_loop<first_loop_not_on_dsp) {
	    std::cerr << "WARNING [AnalysisBaseCommand::setLoopCounters] External mask stage loop is positioned before the loops exectued on the DSP. "
		      << "Does this make sense ?"
		      << std::endl;
	  }
	}

      }
      catch (PixA::ConfigException &err) {
	std::cerr << "ERROR [AnalysisBaseCommand::setLoopCounters] Exception while reading loop ranges : " << err.getDescriptor() << "." << std::endl;
      }
    }
    //     std::cout << "INFO [AnalysisBaseCommand::setLoopCounters] Counters : ";
    //     for (std::vector<unsigned short>::const_iterator iter=max_counter.begin(); iter!=max_counter.end(); ++iter) {
    //       std::cout << iter - max_counter.begin() << ":" << *iter << " ";
    //     }
    //     std::cout << " ms pos=" << mask_stage_loop << std::endl;
  }


  void AnalysisBaseCommand::sendAbort(PixCon::Result_t &ret)
  {
    if (ret.analysisSerialNumber()>0) { // necessary ?
      IPCPartition &the_partition = m_engine->partition();
      if (!the_partition.isValid()) {
	std::cerr << "ERROR [AnalysisBaseCommand::submitAnalysis] No valid partition." << std::endl;
	return;
      }

      bool scheduler_comm_error=false;
      try {

	// get CORBA reference to the scheduler
	CAN::Scheduler_ref manitu (the_partition); // Should throw an exception if the scheduler does not exist in the 
	manitu.abortAnalysis(ret.analysisSerialNumber());
	m_engine->statusChangeTransceiver()->waitFor(kAnalysis, ret.analysisSerialNumber(),  m_kill );

      }
      catch (CAN::MRSException &err) {
	std::cerr << "ERROR [AnalysisBaseCommand::submitAnalysis] Caught exception " << err.getDescriptor() << std::endl;
	return;
      }
      catch (CORBA::TRANSIENT& err) {
	scheduler_comm_error=true;
      }
      catch (CORBA::COMM_FAILURE &err) {
	scheduler_comm_error=true;
      }
      catch (std::exception &err) {
	std::cerr << "ERROR [AnalysisBaseCommand::submitAnalysis] Caught std exception " << err.what() << std::endl;
	scheduler_comm_error=true;
      }
      catch (...) {
	scheduler_comm_error=true;
      }

      if (scheduler_comm_error) {
	std::cerr << "ERROR [AnalysisBaseCommand::submitAnalysis] Communication error with scheduler." << std::endl;
      }
    }
  }

}
