#include <StatusMonitorActivator.h>
#include <qapplication.h>

namespace PixCon {
  bool StatusMonitorActivator::activateScanMonitoring(SerialNumber_t scan_serial_number,
						      unsigned int mask_stage_loop_index,
						      const std::vector<unsigned short> &start_value,
						      const std::vector<unsigned short> &max_value,
						      unsigned long max_step_time) {
    // ensure that progress monitoring is inactive
    scanProgressMonitor().abortMonitoring();
    scanProgressMonitor().waitUntilStopped();
    bool ret = scanStatusMonitor().monitorStatus(scan_serial_number) ;
    if (ret && max_value.size()>0) {
      scanProgressMonitor().startMonitoring(scan_serial_number,
					    mask_stage_loop_index,
					    start_value,
					    max_value,
					    max_step_time);
    }
    return ret;
  }

  bool StatusMonitorActivator::activateAnalysisMonitoring(SerialNumber_t analysis_serial_number) {
    return analysisStatusMonitor().monitorStatus(analysis_serial_number);
  }
}
