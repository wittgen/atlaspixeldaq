#include "CoralDbClient.hh"
#include "PixMetaException.hh"

#include <ipc/partition.h>
#include <ipc/server.h>
#include <ipc/core.h>

#include <PixHistoServer/PixHistoServerInterface.h>

#include <cstdlib>
#include <cstring>

#include <string>
#include <iostream>
#include <sstream>
#include <iomanip>


int main(int argc, char **argv)
{

  CAN::SerialNumber_t serial_number = 0;
  std::string new_file_name;
  std::string new_alt_file_name;
  std::string partition_name;
  std::string oh_name="PixelHistoServer";
  std::string name_server_name="nameServer";
  bool simulate=true;
  unsigned int verbose=1;
  bool error=false;
  try {

    for (int arg_i=1; arg_i<argc; arg_i++) {
      if ((strcmp(argv[arg_i],"-s")==0 || strcmp(argv[arg_i],"--serial-number")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	serial_number=atoi(argv[++arg_i]);
      }
      else if ((strcmp(argv[arg_i],"-f")==0 || strcmp(argv[arg_i],"--file-name")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	new_file_name=argv[++arg_i];
      }
      else if ((strcmp(argv[arg_i],"-x")==0 || strcmp(argv[arg_i],"--execute")==0)) {
	simulate=false;
      }
      else if ((strcmp(argv[arg_i],"-v")==0 || strcmp(argv[arg_i],"--verbose")==0)) {
	verbose++;
      }
      else if ((strcmp(argv[arg_i],"-q")==0 || strcmp(argv[arg_i],"--quiet")==0)) {
	verbose=0;
      }
      else if (strcmp(argv[arg_i],"-p")==0 && arg_i+1<argc) {
	partition_name = argv[++arg_i];
	if (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	  oh_name = argv[++arg_i];
	  if (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	    name_server_name = argv[++arg_i];
	  }
	}
      }
      else if (strcmp(argv[arg_i],"-o")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	oh_name = argv[++arg_i];
      }
      else if ((strcmp(argv[arg_i],"-n")==0 || strcmp(argv[arg_i],"--name-server")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	name_server_name = argv[++arg_i];
      }
      else {
	error=true;
	std::cout << "ERROR ["<<argv[0]<< ":main] Unhandled argument " << argv[arg_i] << std::endl;
      }

    }
    if (new_file_name.empty()) {
      std::stringstream file_name;
      const char *storage_path=getenv("PIXSCAN_STORAGE_PATH");
      if (!storage_path || strlen(storage_path)==0) {
	std::cout << "ERROR ["<<argv[0]<< ":main] Do not where to store the file. No file name given and PIXSCAN_STORAGE_PATH is not defined. "<< std::endl;
	return -1;
      }
      std::string path_string = storage_path;
      if (!path_string.empty() && path_string[path_string.size()-1]!='/') {
	path_string+='/';
      }
      file_name << path_string << "SCAN_S" << std::setw(9) << std::setfill('0') << serial_number << ".root";
      new_file_name = file_name.str();
    }

    if (partition_name.empty() || oh_name.empty() || name_server_name.empty()) {
      std::cout << "ERROR No partition, OH name, or name server name given." << std::endl;
      error=true;
    }
    if (serial_number==0 || (new_file_name.empty() )) {
      std::cout << "ERROR No serial number or file name given." << std::endl;
      error=true;
    }
    if (error) {
      std::cout << "USAGE: -p partition [histo-server [name-server] ] [-o histo-server-name] [-n name-server-name] [-f file-name] " << std::endl
		<< "\t[-v] [-q] -s serial-number -x" << std::endl
		<< std::endl
		<< "-p partition [histo-server [name-server] ]   set partition name (required)." << std::endl
		<< "-o histo-server-name\tfor non standard histogram server names (default=PixelHistoServer)." << std::endl
		<< "-n name-server-name\tfor non standard histogram server names (default=nameServer)." << std::endl
		<< "-f file-name\t\t for non default file names (defaul=${PIXSCAN_STORAG_PATH}/SCAN_S0....root)." << std::endl
		<< "-v\t\t\tincrease verbosity. Can be repeated." << std::endl
		<< "-q\t\t\tquiet." << std::endl
		<< "-x\t\t\tactually execute command (required)." << std::endl
		<< "-s serial-number\tthe serial number of the scan to be saved (required)." << std::endl
		<< std::endl
		<< "Example : " << argv[0] << " -p PixelInfr -s 1000 -v -x" << std::endl;
      return -1;
    }

    CAN::PixCoralDbClient coral_client(true);

    // Start IPCCore
    IPCCore::init(argc, argv);
    
    IPCServer ipc_server;
  
    IPCPartition partition(partition_name);
    
    // Get metadata for scan anal_id
    try {

      if (verbose) {
	std::cout << "INFO Trigger histogram writing for "<< "S" << std::setw(9) << std::setfill('0') << serial_number << std::setfill(' ') << std::endl;

      }

      std::unique_ptr<PixLib::PixHistoServerInterface>  hInt( new PixLib::PixHistoServerInterface(&partition,
												oh_name,
												name_server_name,
												"test_triggerHistogramWriting"));

      PixLib::ScanInfo_t scan_info = coral_client.getMetadata<PixLib::ScanInfo_t>(serial_number);
      if (verbose) {
	std::cout << "INFO change file name of " << std::setw(9) << std::setfill('0') << serial_number << std::setfill(' ') << std::endl;
	if (scan_info.file().empty()) {
	  std::cout << "\t" << scan_info.file() << std::endl << "\t -> " << new_file_name << std::endl;
	}
	else {
	  std::cout << "\t" << scan_info.alternateFile() << std::endl << "\t -> " << new_file_name << std::endl;
	}
      }
      if (!simulate) {

	std::stringstream folderstream;
	folderstream << "/";
	folderstream << 'S' << std::setw(9) << std::setfill('0') << serial_number;
	folderstream << "/";
//	std::string outFileName;
	std::cout << "INFO ["<<argv[0]<<":main] Save file to: " <<new_file_name << std::endl;
	bool result = hInt->writeRootHistoServer(folderstream.str(), new_file_name, false);
	if (!result) {
	  std::cout << "ERROR ["<<argv[0] << ":main] Failed to save histograms from " << partition_name 
		    << "/" << oh_name << " to  file to " << new_file_name << std::endl;
	}
	else {
	if (scan_info.file().empty()) {
	  bool ret = coral_client.updateScanMetadataFile(serial_number, new_file_name);
	  if (!ret) {
	    std::cout << "ERROR failed to change file name for " << std::setw(9) << std::setfill('0') << serial_number << std::setfill(' ')
		      << " to " << new_file_name << "." << std::endl;
	  }
	}
	else {
	  bool ret = coral_client.updateScanMetadataAlternateFile(serial_number, new_file_name);
	  if (!ret) {
	    std::cout << "ERROR failed to change alternate file name for " << std::setw(9) << std::setfill('0') << serial_number << std::setfill(' ')
		      << " to " << new_file_name << "." << std::endl;
	  }
	}
	}
      }
      else {
	std::cout << "WARNING [" << argv[0] << ":main] This was a simulation only. Rerun with -x do really safe the file and update the meta data." << std::endl;
      }

    } catch (CAN::PixMetaException & err) {
      std::cout<<"PixMetaException: "<<err.what()<<std::endl;
    }

  // COOL, CORAL POOL exceptions inherit from std exceptions: catching
  // std::exception will catch all errors from COOL, CORAL and POOL
  } catch ( std::exception& e ) {
    std::cout << "std::exception caught: " << e.what() << std::endl;
    return -1;
  }

  catch (...) {
    std::cout << "Unknown exception caught!" << std::endl;
    return -1;
  }

  return 0;

}
