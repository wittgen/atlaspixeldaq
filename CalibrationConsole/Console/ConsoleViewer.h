// Dear emacs, this is -*-c++-*-x
#ifndef _PixCon_ConsoleViewer_h_
#define _PixCon_ConsoleViewer_h_

#include "OHViewer.h"
#include "UserMode.h"
#include "PixCalibDataOffline.h"

#include <map>
#include <vector>
#include <string>
#include <memory>
//Added by qt3to4:
#include <QEvent>
#include <QCloseEvent>

#ifdef HAVE_DDC
#  include <DcsVariables.h>
#endif

class QCloseEvent;

namespace PixCon {

  class ConsoleEngine;
  //  class CalibrationDataManager;
  class CommandList;
  class CanStatusPanel;
  class IsMonitorBase;

#ifdef HAVE_DDC
  class DcsMonitorManager;
  //  class DcsMonitorFactory;
  //  class DdcRequestController;
  //  class DcsSubscriptions;

  //  typedef std::shared_ptr<DdcRequestController> DdcRequestHandle;

#endif

  class IBocEditor;

  class ConsoleViewer: public OHViewer
  {
     Q_OBJECT

  public:
     ConsoleViewer( std::shared_ptr<PixCon::UserMode>& user_mode,
		    IPCPartition &is_partition,
		    const std::string &is_server_name,
		    const std::string &ddc_partition_name,
		    const std::string &ddc_is_name,
		    const std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data,
		    PixCon::OHHistogramProviderBase &oh_histogram_provider,
		    std::shared_ptr<PixCon::MetaHistogramProvider> meta_histogram_provider,
		    const std::shared_ptr<PixCon::HistogramCache> &histogram_cache,
		    const std::shared_ptr<PixCon::CalibrationDataPalette> &palette,
		    const std::shared_ptr<PixCon::CategoryList> &categories,
		    const std::shared_ptr<PixCon::IContextMenu> &context_menu,
		    const std::shared_ptr<PixCon::IMetaDataUpdateHelper> &update_helper,
		    const std::shared_ptr<PixCon::VisualiserList> &visualiser_list,
		    const std::shared_ptr<PixCon::ConsoleEngine> &engine,
		    const std::shared_ptr<PixCon::IBocEditor> &boc_editor,
		    QApplication &application,
		    QWidget* parent = 0);

    ~ConsoleViewer();

    void closeEvent(QCloseEvent *ce);

  protected:
    void toggleEnableState(PixCon::EConnItemType conn_item_type, const std::string &connectivity_name);

    void selectConnectivityTags();

    void connectivityChanged(PixCon::EMeasurementType measurement_type, PixCon::SerialNumber_t serial_number);
    
    bool event(QEvent *an_event);

    void shutdown();

    void updateItems();

  private:
    std::shared_ptr<PixCon::UserMode> m_userMode;
    std::shared_ptr<PixCon::ConsoleEngine>    m_engine;
    IPCPartition *m_isPartition;
    PixCon::CommandList                        *m_commandList;
    CanStatusPanel                             *m_canStatusPanel;
    std::shared_ptr<PixCon::IBocEditor>       m_bocEditor;

    std::shared_ptr< PixCon::IsMonitorBase> m_activeMonitor;

    std::shared_ptr<IStatusMonitor>         m_actionStatusMonitor;
    std::shared_ptr<IStatusMonitor>         m_scanProgressMonitor;

#ifdef HAVE_DDC
    std::unique_ptr<DcsMonitorManager>          m_dcsMonitorManager;
    //    std::unique_ptr< DcsMonitorFactory >        m_dcsMonitorFactory;
    //    std::map< DcsVariables::EVariable, std::shared_ptr< IsMonitorBase > >        m_dcsMonitorList;
    //    std::map< DcsVariables::EVariable, DdcRequestHandle> m_activeDcsMonitorList;
    //    std::shared_ptr<DcsSubscriptions>       m_dcsSubscriptions;
#endif

    PixCalibDataOffline*                      m_pcdCreator;

    //std::vector<int>                          m_actionItems;
    std::vector<QAction*>                          m_actionItems;
    QAction *m_pcdAct, *m_bocEdtAct;

    bool m_isShutdown;
 public slots:
    void allocateActions();
    void initActions();

    ///    void showDcsSubscription();

    //    void changeSubscription(int dcs_variable, bool subscribe);

    //    void closeSubscribtionDialog();
    void updateAllCategories();

    void showCloneTags();

    void showBocEditor();

    void saveDisable();

    void updateViewerForUserMode(PixCon::UserMode::EUserMode new_user);

    void showPCDCreator();

  signals:
    void progress(unsigned int steps, unsigned int total_steps);
    void statusMessage(const QString &status_message);
  };
}

#endif
