

#include <ipc/partition.h>
#include <ipc/core.h>

#include <ConfigWrapper/pixa_exception.h>
#include <ConfigWrapper/Connectivity.h>
#include <ConfigWrapper/ConnectivityUtil.h>
#include <ConfigWrapper/ConnectivityManager.h>

#include <ConfigWrapper/createDbServer.h>
#include <memory>
#include <memory>


#include <is/infodictionary.h>
#include <is/infoT.h>

#include <memory>


class ExampleData : public omni_thread
{
public:
  ExampleData(const PixA::ConnectivityRef &conn, const IPCPartition &partition, const std::string &is_server_name) 
  : m_isDictionary (partition),
    m_isServerName(is_server_name),
    m_conn(conn),
    m_updateTime(10),
    m_changeProb(100),
    m_abort(false)
  {

  }

  void start() {
    start_undetached();
  }

  //  createState();
  void abort() { m_abort = true; }

  void *run_undetached(void *arg) {
    assert(arg==NULL);
    run();
    return NULL;
  }

  void setUpdateFreq(unsigned int update_freq, unsigned int change_prob) {
    if (update_freq>0) {
      m_updateTime=update_freq;
    }
    m_changeProb=change_prob%100;
    if (m_changeProb==0) {
      m_changeProb=100;
    }
  }

  void run();
  
  enum EVarType {kRodVar, kModuleVar, kDAQVar};

  template <class T>
  class IDataModel {
  public:
    virtual ~IDataModel() {};

    virtual T init() const = 0;
    virtual T next(T current) = 0;

  };

  template <class T>
  class Ramp : public IDataModel<T>
  {
  public:
    Ramp(T min, T max, T step,  T init) 
      : m_min(min), m_max(max), m_step(step), m_init(init)
    {}
    T init () const { return m_init; }
    T next(T current) { T temp = current + m_step; return ( (temp>m_max)  ? m_min + (temp-m_max) : temp) ; }

  private:
    T m_min;
    T m_max;
    T m_step;
    T m_init;
  };


  template <class T>
  void addDataSimulator(const std::string &name, EVarType type, IDataModel<T> *data_model);

 protected:
  template <class T>
  void update();

private:


  template <class T>
  class VarDescr_t {
  public:
    VarDescr_t(EVarType type, const std::shared_ptr< IDataModel<T> > &data_model) 
      : m_dataModel(data_model),
	m_varType(type)
    {}

    EVarType varType() const {return m_varType; }
    ISInfoT<T> &info() { return m_info; }
    IDataModel<T> &model() { return *m_dataModel; }

  private:
    std::shared_ptr< IDataModel<T> > m_dataModel;
    EVarType                           m_varType;
    ISInfoT<T>                           m_info;
  };


  template <class T>
  std::map<std::string, VarDescr_t<T> > &getMap();




  //   template <class T>
  //   class Oscillation : public IDataModel<T>
  //   {
  //   public:
  //     Oscillation(T mean, T amplitude, T period,  T phase) 
  //       : m_mean(mean), m_amplitude(amplitude), m_period(period), m_init(init)
  //     {}
  
  //     T init () const { return m_init; }
  //     T next(T current) { 
  //       double phase = 
  //       if (current+m_step>m_max) current= m_min + (m_current + m_step-m_max);
  //     }
  //   };


  ISInfoDictionary m_isDictionary;
  std::string m_isServerName;
  PixA::ConnectivityRef m_conn;

  std::map<std::string, VarDescr_t<float> >   m_floatVar;
  std::map<std::string, VarDescr_t<int> >     m_intVar;
  std::map<std::string, VarDescr_t<std::string> >  m_stringVar;

  unsigned int m_updateTime;
  unsigned int m_changeProb;
  bool m_abort;
};


template <>
std::map<std::string, ExampleData::VarDescr_t<float> > &ExampleData::getMap<float>() {
  return m_floatVar;
}

template <>
std::map<std::string, ExampleData::VarDescr_t<int> > &ExampleData::getMap<int>() {
  return m_intVar;
}

template <>
std::map<std::string, ExampleData::VarDescr_t<std::string> > &ExampleData::getMap<std::string>() {
  return m_stringVar;
}


template <class T>
void ExampleData::addDataSimulator(const std::string &name, EVarType type, IDataModel<T> *data_model) {
  std::shared_ptr< IDataModel<T> > data_model_ptr( data_model );
  std::map<std::string, VarDescr_t<T> > &the_map = getMap<T>();

  for (PixA::CrateList::const_iterator crate_iter = m_conn.crates().begin();
       crate_iter != m_conn.crates().end();
       ++crate_iter) {

    PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
    PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
    for (PixA::RodList::const_iterator rod_iter = rod_begin;
	 rod_iter != rod_end;
	 ++rod_iter) {

      if (type==kModuleVar || type==kDAQVar) {

	PixA::Pp0List::const_iterator pp0_begin = PixA::pp0Begin(rod_iter);
	PixA::Pp0List::const_iterator pp0_end = PixA::pp0End(rod_iter);
	for (PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
	     pp0_iter != pp0_end;
	     ++pp0_iter) {

	  PixA::ModuleList::const_iterator module_begin = PixA::modulesBegin(pp0_iter);
	  PixA::ModuleList::const_iterator module_end = PixA::modulesEnd(pp0_iter);
	  for (PixA::ModuleList::const_iterator module_iter = module_begin;
	       module_iter != module_end;
	       ++module_iter) {

	    std::string is_name(m_isServerName);
	    is_name +=".";
	    is_name +=PixA::connectivityName(rod_iter);
	    is_name += "/";
	    is_name += PixA::connectivityName(module_iter);
	    is_name += "/";
	    is_name += name;

	    std::pair< typename std::map<std::string, VarDescr_t<T> >::iterator, bool>
	      ret = the_map.insert(std::make_pair(is_name,VarDescr_t<T>(type, data_model_ptr)));

	    if (ret.second) {

	      ret.first->second.info().setValue(ret.first->second.model().init());

	      try {
		std::cout << "INFO [ExampleData::addDataSimulator] add " << ret.first->first << "." << std::endl;
		m_isDictionary.insert(ret.first->first, ret.first->second.info());
	      } catch ( daq::is::Exception & ex ) {
	      }
	    }
	  }
	}
      }
      if (type==kRodVar || type==kDAQVar) {
	std::string is_name(m_isServerName);
	is_name += ".";
	is_name += PixA::connectivityName(crate_iter);
	is_name += "/";
	is_name += PixA::connectivityName(rod_iter);
	is_name += "/";
	is_name += name;

	std::pair< typename std::map<std::string, VarDescr_t<T> >::iterator, bool>
	  ret = the_map.insert(std::make_pair(is_name,VarDescr_t<T>(type, data_model_ptr)) );
	  
	if (ret.second) {
	  ret.first->second.info().setValue(ret.first->second.model().init());

	  try {
	    std::cout << "INFO [ExampleData::addDataSimulator] add " << ret.first->first <<  "." << std::endl;
	    m_isDictionary.insert(ret.first->first, ret.first->second.info());
	  } catch ( daq::is::Exception & ) {
	  }
	    
	}
      }
    }
  }
    
}

template <class T>
void ExampleData::update() {
  std::map<std::string, VarDescr_t<T> > &the_map = getMap<T>();
  
  for (typename std::map<std::string, VarDescr_t<T> >::iterator var_iter = the_map.begin();
       var_iter != the_map.end();
       ++var_iter ) {

		
    if ( (static_cast<unsigned int>( rand() ) %100) <m_changeProb) {
      try {
	var_iter->second.info().setValue( var_iter->second.model().next( var_iter->second.info().getValue()) );
	var_iter->second.info().setValue( var_iter->second.model().next( var_iter->second.info().getValue()) );
	m_isDictionary.update(var_iter->first,var_iter->second.info());
      } catch ( daq::is::Exception & ex ) {
      }
    }
  }
}

void ExampleData::run() {
  for(;;) {
    sleep(m_updateTime);
    update<int>();
    update<float>();
    update<std::string>();
  }
}


template <class T>
inline void setIsValue(ISInfoDictionary &dictionary, ISType &type, const std::string &is_dictionary_name, ISInfoT<T> &info_val)
{
#ifdef TDAQ010601
  if (dictionary.findType(is_dictionary_name,type) == ISInfo::Success) {
    dictionary.insert(is_dictionary_name, info_val );
    // // throw exception in case the insert failes
    // std::stringstream message;
    // message << "Failed to insert " << is_dictionary_name << " into IS dictionary." 
    // throw daq::is::Exception(message.str());
  }
  else {
    dictionary.update(is_dictionary_name, info_val );
  }
#else
  try {
    dictionary.findType(is_dictionary_name,type);
    dictionary.update(is_dictionary_name, info_val );
  } catch ( daq::is::Exception & ex ) {
    dictionary.insert(is_dictionary_name, info_val );
  }
#endif
}
void publishTags(const std::string &id_tag,
		 const std::string &conn_tag,
		 const std::string &cfg_tag,
		 const std::string &disable_name,
		 const std::string &is_server_name,
		 IPCPartition &partition)
{
  if (!partition.isValid()) {
    std::cerr << "ERROR [publishTags] Not a valid partition " << partition.name() << std::endl;
    return;
  }

  ISInfoDictionary dictionary(partition);
  ISInfoString id_tag_info;
  id_tag_info.setValue(id_tag);
  ISInfoString conn_tag_info;
  conn_tag_info.setValue(conn_tag);
  ISInfoString cfg_tag_info;
  cfg_tag_info.setValue(cfg_tag);
  ISInfoString disable_info;
  disable_info.setValue(disable_name);

  ISType a_type(id_tag_info.type());
  setIsValue(dictionary, a_type, is_server_name+"."+"DataTakingConfig-IdTag", id_tag_info);
  setIsValue(dictionary, a_type, is_server_name+"."+"DataTakingConfig-ConnTag", conn_tag_info);
  setIsValue(dictionary, a_type, is_server_name+"."+"DataTakingConfig-CfgTag", cfg_tag_info);
  setIsValue(dictionary, a_type, is_server_name+"."+"DataTakingConfig-Disable", disable_info);
}

int main(int argc, char **argv)
{
  std::string partition_name;
  std::string tag_name;
  std::string cfg_tag_name="Test";
  std::string disable_name="ALL_ENABLED";
  std::string is_name = "PixFakeData";
  std::string object_tag_name = "CT";
  std::string db_server_partition_name;
  std::string db_server_name;
  bool publish_tags=false;
  std::string tags_is_server("PixelRunParams");
  int change_prob=100;

  unsigned int update_freq=10;

  for (int arg_i=1; arg_i<argc; arg_i++) {
    if (strcmp(argv[arg_i],"-p")==0 && arg_i+1<argc) {
      partition_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-i")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-' ) {
      object_tag_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-t")==0 && arg_i+2<argc && argv[arg_i+2][0]!='-' && argv[arg_i+1][0]!='-') {
      tag_name = argv[++arg_i];
      cfg_tag_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-n")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      is_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-u")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      update_freq = atoi(argv[++arg_i]);
    }
    else if ((strcmp(argv[arg_i],"-c")==0 || strcmp(argv[arg_i],"--change-prob")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      change_prob = atoi(argv[++arg_i])%100;
    }
    else if ( (strcmp(argv[arg_i],"-D")==0
	       || strcmp(argv[arg_i],"--db-server-name")==0)
	      && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      db_server_name = argv[++arg_i];
    }
    else if ( (strcmp(argv[arg_i],"--db-server-partition")==0
	       || strcmp(argv[arg_i],"-p")==0)
	      && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      db_server_partition_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"--publish-tags")==0) {
      publish_tags=true;
      if (arg_i+1<argc && argv[arg_i+1][0] != '-') {
	tags_is_server = argv[++arg_i];
      }
    }
    else {
      std::cout << "ERROR ["<< argv[0] << ":main] unhandled argument " << arg_i << " : " << argv[arg_i]<< "." << std::endl;
    }
  }

  if (tag_name.empty() || cfg_tag_name.empty() || partition_name.empty() || update_freq<=0) {
      std::cout << "usage: " << argv[0] << " -p partition [-i object-tag] -t tag cfg-tag [-n \"IS server name\" -u seconds] [--db-server-name]" << std::endl
		<< "\t[--publish-tags [tag-is-server-name] ]" << std::endl;
      return -1;
  }
  // START IPC

  // Start IPCCore
  IPCCore::init(argc, argv);

  if (db_server_partition_name.empty() && !partition_name.empty()) {
    db_server_partition_name = partition_name;
  }

  std::unique_ptr<IPCPartition> db_server_partition;
  if (!db_server_name.empty() && !db_server_partition_name.empty()) {
    db_server_partition = std::unique_ptr<IPCPartition>(new IPCPartition(db_server_partition_name.c_str()));
    if (!db_server_partition->isValid()) {
      std::cerr << "ERROR [" << argv[0] << ":main] Not a valid partition " << db_server_partition_name
		<< " for the db server." << std::endl;
      return EXIT_FAILURE;
    }

    std::shared_ptr<PixLib::PixDbServerInterface> db_server( PixA::createDbServer(*db_server_partition,
										    db_server_name ));
    if (publish_tags) {
      std::unique_ptr<PixLib::PixDisable> disable(new PixLib::PixDisable(disable_name));
      disable->writeConfig(db_server.get(),std::string("Configuration-")+object_tag_name,cfg_tag_name, "Disable_Tmp");
    }

    if (db_server.get()) {
      PixA::ConnectivityManager::useDbServer( db_server );
    }
    else {
      std::cout << "WARNING [main:" << argv[0] << "] Failed to create the db server." << std::endl;
    }
  }


  PixA::ConnectivityRef conn;
  try {
     conn=PixA::ConnectivityManager::getConnectivity( object_tag_name, tag_name, tag_name, tag_name, cfg_tag_name, 0 );
  }
  catch (PixA::ConfigException &err) {
    std::cout << "FATAL [main] caught exception (ConfigException) : " << err.getDescriptor() << std::endl;
    return -1;
  }
  catch (PixA::BaseException &err) {
    std::cout << "FATAL [main] caught exception (BaseException) : " << err.getDescriptor() << std::endl;
    return -1;
  }
  catch (std::exception &err) {
    std::cout << "FATAL [main] caught exception (std::exception) : " << err.what() << std::endl;
    return -1;
  }
  catch (...) {
    std::cout << "FATAL [main] unhandled exception. "  << std::endl;
    return -1;
  }


  // Create IPCPartition
  std::cout << "INFO [" << argv[0] << ":main] Start IS data simulator on " << partition_name  << " in IS server "  << is_name << std::endl;
  IPCPartition ipcPartition(partition_name.c_str());


  if (publish_tags) {
    publishTags(object_tag_name, tag_name, cfg_tag_name, disable_name, tags_is_server,ipcPartition);
  }

  std::cout << "INFO [" << argv[0] << ":main] install data simulators : " << std::endl;
  ExampleData example_data(conn, ipcPartition, is_name);
  example_data.setUpdateFreq(update_freq, change_prob);
  example_data.addDataSimulator("DAQEnabled",ExampleData::kDAQVar,new ExampleData::Ramp<int>(0,4,1,0));
  example_data.addDataSimulator("BUSY",ExampleData::kRodVar,new ExampleData::Ramp<int>(0,100,5,0));
  example_data.addDataSimulator("AVAILABLE",ExampleData::kRodVar,new ExampleData::Ramp<int>(0,1,1,0));
  example_data.addDataSimulator("LAST-WD-CALL",ExampleData::kRodVar,new ExampleData::Ramp<int>(0,123124394,1123,0));
  example_data.addDataSimulator("dataincorrect_errors",ExampleData::kRodVar,new ExampleData::Ramp<int>(0,1000,100,0));
  example_data.addDataSimulator("FragmentSize",ExampleData::kRodVar,new ExampleData::Ramp<int>(0,1<<12,1<<5,0));

  example_data.addDataSimulator("NHits",ExampleData::kModuleVar, new ExampleData::Ramp<int>(0,20,2,0));
  example_data.addDataSimulator("BCID_errors",ExampleData::kModuleVar, new ExampleData::Ramp<int>(0,20,2,0));
  example_data.addDataSimulator("LVL1ID_errors",ExampleData::kModuleVar, new ExampleData::Ramp<int>(0,20,2,0));
  example_data.addDataSimulator("timeout_errors",ExampleData::kModuleVar, new ExampleData::Ramp<int>(0,20,2,0));
  example_data.addDataSimulator("preamble_errors",ExampleData::kModuleVar, new ExampleData::Ramp<int>(0,20,2,0));
  example_data.addDataSimulator("trailerbit_errors",ExampleData::kModuleVar, new ExampleData::Ramp<int>(0,20,2,0));
  example_data.addDataSimulator("headtraillimit_errors",ExampleData::kModuleVar, new ExampleData::Ramp<int>(0,20,2,0));
  example_data.addDataSimulator("dataoverflow_errors",ExampleData::kModuleVar, new ExampleData::Ramp<int>(0,20,2,0));
  example_data.addDataSimulator("FE_errors",ExampleData::kModuleVar, new ExampleData::Ramp<int>(0,20,2,0));

  std::cout << "INFO [" << argv[0] << ":main] run : " << std::endl;
  example_data.run();
    //  ExampleStateData example_state_data(conn, ipcPartition, is_name);
    //  example_state_data.setUpdateFreq(update_freq);
    //  example_data.run();
    //  example_state_data.join(NULL);
    //  example_data.join(NULL);

  return 0;
}
