// Dear emacs, this is -*-c++-*-
#ifndef _PixCon_ActionTagsMonitor_h_
#define _PixCon_ActionTagsMonitor_h_

#include <ConfigWrapper/Connectivity.h>
#include <Flag.h>
#include <IsMonitor.h>
#include <VarDefList.h>

#include <qthread.h>
#include <QIsInfoEvent.h>

namespace PixCon {

  class IsReceptor;

  class ActionTagLister : public QThread
  {
  public:
    ActionTagLister(const std::shared_ptr<IsReceptor> &receptor,
		    const std::string &partition_name,
		    unsigned int wait_before_list,
		    const std::string &category_name);

    ~ActionTagLister() { shutdownWait(); }

    void availableAction() {
      incCounter();
      m_hasNewAction.setFlag();
    }

    void shutdown() {
      m_abort=true;
      m_listWait.setFlag();
      m_hasNewAction.setFlag();
    }

    void shutdownWait() {
      shutdown();
      m_exit.wait();
    }

  protected:
    void run();
    void incCounter() { m_counter++; }

    static VarDef_t addCfgVarDef(const std::string &tag_var_name);
    State_t getCfgTagId(const char  *tag_name);

    class DoNothingExtractor : public IVarNameExtractor
    {
    public:

      DoNothingExtractor(const std::string &var_name)
	: m_varName( var_name )
      {
      }


      void extractVar(const std::string &connectivity_name, ConnVar_t &conn_var) const
      {
	conn_var.connName() = connectivity_name;
	conn_var.varName()= m_varName;
      }

    private:
      std::string              m_varName;
    };


  private:
    const std::string m_partitionName;
    std::shared_ptr<IsReceptor> m_receptor;

    VarDef_t m_cfgVarDef;
    VarDef_t m_modCfgVarDef;
    std::string  m_categoryName;
    unsigned int m_waitBeforeList;

    Flag m_hasNewAction;
    Flag m_listWait;
    Flag m_exit;

    unsigned int m_counter;
    unsigned int m_verbose;
    bool m_abort;
    bool m_isRunning;
  };

  class ActionTagsMonitor : public SimpleIsMonitorReceiver<int,int>
  {
  public:
    ActionTagsMonitor(const std::shared_ptr<IsReceptor> &receptor,
		      const std::string &action_server_partition_name,
		      const IVarNameExtractor &extractor,
		      const std::string &category_name="ROD",
		      const std::string &is_regex  = ".*ROD.*S.*AVAILABLE",
		      unsigned int wait_before_list=20);

    ActionTagsMonitor(const std::shared_ptr<IsReceptor> &receptor,
		      const IVarNameExtractor &extractor,
		      const std::string &category_name="ROD",
		      const std::string &is_regex  = ".*ROD.*S.*AVAILABLE",
		      unsigned int wait_before_list=20);

    void setValue(const std::string &is_name, ::time_t time_stamp, int value);

    void registerForCleanup(const std::string &is_name, std::map<std::string, std::set<std::string> > &clean_up_var_list);

  protected:
    void defineAvailable();
  private:
    ActionTagLister   m_tagLister;
    const IVarNameExtractor *m_varNameExtractor;

    static const char *s_availableVar;
  };

}
#endif

