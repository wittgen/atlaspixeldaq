#include <ipc/partition.h>
#include <ipc/core.h>

#include <is/infodictionary.h>
#include <is/inforeceiver.h>
#include <is/infoiterator.h>
#include <is/callbackinfo.h>
#include <is/infoT.h>

#include <cassert>

#include <vector>
#include <iostream>
#include <iomanip>
#include <stdexcept>

#include <time.h>


class is_listener
{
public:
  is_listener(IPCPartition &partition, const std::string &is_server_name, const std::vector<std::string> &regex) 
    : m_partition(&partition),m_isReceiver(partition) {
    // get initial config values from IS server
    for(std::vector<std::string>::const_iterator regex_iter = regex.begin();
	regex_iter != regex.end();
	++regex_iter) {
 
      {
	ISInfoIterator ii( *m_partition, is_server_name, *regex_iter );
	ISInfoAny val;
	while( ii() ) {
	  ii.value( val );

	  std::cout << ii.name() << " - " << ii.time().str() << " : ";
	  val.print(std::cout);
	  std::cout << std::endl;
	}
      }
      m_isReceiver.subscribe(is_server_name.c_str(), *regex_iter, &is_listener::changed, this);
    }
    m_isReceiver.run();
  }


  void changed(ISCallbackInfo *info)
  {
    ISInfoAny val;
    info->value(val);
    
    std::cout << info->name() << " - " << info->time().str() << " : ";
    val.print(std::cout);
    std::cout << std::endl;
  }

private:
  IPCPartition   *m_partition;
  ISInfoReceiver m_isReceiver;
};


int main(int argc, char **argv) {
  // GET COMMAND LINE

  // Check arguments

  std::string ipcPartitionName;
  std::string is_server("CAN");
  std::vector< std::string > regex;


  for (int arg_i=1; arg_i<argc; arg_i++) {

    if (strcmp(argv[arg_i],"-p")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      ipcPartitionName = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-n")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      is_server = argv[++arg_i];
    }
    else if ((strcmp(argv[arg_i],"-r")==0 || strcmp(argv[arg_i],"-R")==0)  && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      while (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	regex.push_back( argv[++arg_i]);
      }
    }
    else {
      std::cout << "USAGE: " << argv[0] << " -p partition [-n IS-server-name] [-r regex [...] ] [-r regeex ...]."  << std::endl
		<< "\t-p partition name\t the partition of the CAN/PixScan is server." << std::endl
		<< "\t-n IS-server-name\t the name of the CAN/PixScan is server." << std::endl
		<< "\t-r regular-expression." << std::endl;
      return 1;
    }
  }

  if(ipcPartitionName.empty() || is_server.empty()) {
    std::cout << "USAGE: " << argv[0] << " -p partition [-n IS-server-name] [-r regex [...] ] ..."  << std::endl;
    return 1;
  }

  // START IPC

  // Start IPCCore
  IPCCore::init(argc, argv);

  IPCPartition ipcPartition(ipcPartitionName.c_str());
  if (!ipcPartition.isValid()) {
    std::cerr << "ERROR [" << argv[0] << ":main] Not a valid partition " << ipcPartitionName << std::endl;
    return EXIT_FAILURE;
  }

  is_listener a_listener(ipcPartition,is_server,regex);
  return 0;
}
