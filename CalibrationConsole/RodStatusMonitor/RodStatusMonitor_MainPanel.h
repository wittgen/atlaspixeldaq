#ifndef _PixCon_RodStatusMonitor_MainPanel_h_
#define _PixCon_RodStatusMonitor_MainPanel_h_

#include <ScanMainPanel.h>
#include "UserMode.h"
#include <vector>
#include <QEvent>

class IPCPartition;

namespace PixLib {
  class PixDisable;
}


namespace PixCon  {

#ifdef HAVE_DDC
  class DcsMonitorManager;
#endif

  class IsMonitorManager;
  class IsReceptor;

  class RodStatusMonitor_MainPanel : public ScanMainPanel
  {
    Q_OBJECT

  public:
    RodStatusMonitor_MainPanel(std::shared_ptr<PixCon::UserMode>& user_mode,
                   const std::string &db_server_partition_name,
                   const std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data,
                   const std::shared_ptr<PixCon::HistogramCache> &histogram_cache,
                   const std::shared_ptr<PixCon::CalibrationDataPalette> &palette,
                   const std::shared_ptr<PixCon::CategoryList> &categories,
                   const std::shared_ptr<PixCon::IContextMenu> &context_menu,
                   const std::shared_ptr<PixDet::ILogoFactory> &logo_factory,
                   const std::shared_ptr<VisualiserList> &visualiser_list,
                   const std::shared_ptr< PixCon::IsMonitorManager >  &monitor_manager,
                   QApplication &application,
                   QWidget* parent = 0);
    ~RodStatusMonitor_MainPanel();

    bool event(QEvent *an_event);

    void addMeasurement() { /*Nothing to do*/ }

    void updateMonitors();

    std::shared_ptr< PixCon::IsMonitorManager >  monitorManager() { return m_isMonitorManager; }

    void setupDcsMonitorManager(const std::shared_ptr<PixCon::IsReceptor> &receptor);

    void shutdown();

  public slots:
    void saveDisable();
    void loadDisable();
    void loadDisable(const std::string &name);

    void reconnectIPC();

    void recoverBusyROD();

  protected slots:
    void changeTags();
    void updateAllCategories();

  private:
    void loadDisable(const std::shared_ptr<const PixLib::PixDisable> &disable);
    std::unique_ptr<IPCPartition> m_dbServerPartition;
    QTimer                     *m_changeTagsTimer;
    QWidget                    *m_removedDataList;
    
    static const unsigned int s_updateTagsDelay = 10;
    std::string  m_nextIdTag;
    std::string  m_nextConnTag;
    std::string  m_nextCfgTag;
    std::string  m_nextModCfgTag;

    std::shared_ptr< PixCon::IsMonitorManager >  m_isMonitorManager;

#ifdef HAVE_DDC
    std::unique_ptr<DcsMonitorManager>          m_dcsMonitorManager;
#endif

  };

}
#endif
