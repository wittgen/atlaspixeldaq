#include "TagsMonitor.h"
#include "ConnectivityEvents.h"
#include <is/inforeceiver.h>

#include <IsReceptor.h>

namespace PixCon {

  TagsMonitor::TagsMonitor(std::shared_ptr<IsReceptor> receptor,
			   const std::string &tag_header)
    : SimpleIsMonitorReceiver<std::string, const std::string&>(receptor, kAllConnTypes, "<None>", tag_header+".*", 16 /* buffer size */ ),
      m_tagHeader(tag_header)
  {
    std::cout << "INFO [TagsMonitor::ctor]  subscription " << tag_header << ".*" << std::endl; 

    m_tagMap.insert(std::make_pair( std::string("IdTag"),     PixA::IConnectivity::kId));
    m_tagMap.insert(std::make_pair( std::string("ConnTag"),   PixA::IConnectivity::kConn));
    m_tagMap.insert(std::make_pair( std::string("CfgTag"),    PixA::IConnectivity::kConfig));
    m_tagMap.insert(std::make_pair( std::string("ModCfgTag"), PixA::IConnectivity::kModConfig));
    m_tagMap.insert(std::make_pair( std::string("Disable"),   static_cast<PixA::IConnectivity::EConnTag>(PixA::IConnectivity::kModConfig+1)));

    //    IsMonitorReceiver<std::string,const std::string&>::init();
  }

  void TagsMonitor::setValue(const std::string &is_name,  ::time_t /*time_stamp*/, const std::string &value) {
    std::string::size_type pos = is_name.find(m_tagHeader);
    std::cout << "INFO [TagsMonitor::setValue] tag changed : " << is_name << " == " << value <<"." << std::endl; 

    if (pos != std::string::npos) {
      pos += m_tagHeader.size();
      //       std::cout << "INFO [TagsMonitor::setValue] tag changed : " << is_name << " -> "
      // 		<< is_name.substr(pos, is_name.size()-pos)  <<"." << std::endl;
      std::map< std::string, PixA::IConnectivity::EConnTag>::const_iterator tag_map_iter = m_tagMap.find(is_name.substr(pos, is_name.size()-pos)) ;
      if (tag_map_iter != m_tagMap.end()) {
	if(tag_map_iter->second > PixA::IConnectivity::kModConfig) {
	  // it is a disable
	  receptor()->notify(new QNewDisableEvent(value));
	}
	else {
	  if (m_tag[tag_map_iter->second] !=  value ) {
	    m_tag[tag_map_iter->second] = value;
	    std::cout << "INFO [TagsMonitor::setValue] Notify application about new tags : "
		      << m_tag[PixA::IConnectivity::kId] << ":"
		      << m_tag[PixA::IConnectivity::kConn] << ";"
		      << m_tag[PixA::IConnectivity::kConfig] << ","
		      << m_tag[PixA::IConnectivity::kModConfig]<<"." << std::endl;
      
	    receptor()->notify(new QNewTagEvent(m_tag[PixA::IConnectivity::kId],
						m_tag[PixA::IConnectivity::kConn],
						m_tag[PixA::IConnectivity::kConfig],
						m_tag[PixA::IConnectivity::kModConfig]));
	  }
	}
      }
    }
  }

  void TagsMonitor::registerForCleanup(const std::string &/*is_name*/, std::map<std::string, std::set<std::string> > &/*clean_up_var_list*/) {
    //@todo Nothing to be done, or is there ?
  }


}
