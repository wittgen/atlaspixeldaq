#include "ActionTagsMonitor.h"
#include <PixActions/PixActions.h>
#include <DefaultColourCode.h>
#include <QNewVarEvent.h>

#include <IsReceptor.h>

namespace PixCon {

  ActionTagLister::ActionTagLister(const std::shared_ptr<IsReceptor> &receptor,
				   const std::string &partition_name,
				   unsigned int wait_before_list,
				   const std::string &category_name)
    : m_partitionName(partition_name),
      m_receptor(receptor),
      m_cfgVarDef( addCfgVarDef("CFG_TAG") ),
      m_modCfgVarDef( addCfgVarDef("MOD_CFG_TAG") ),
      m_categoryName(category_name),
      m_waitBeforeList(wait_before_list),
      m_counter(0),
      m_verbose(1),
      m_abort(false),
      m_isRunning(false)
  {
    m_isRunning=true;
    start();
  }

  VarDef_t ActionTagLister::addCfgVarDef(const std::string &tag_var_name)
  {
    VarDef_t the_var_def(VarDef_t::invalid());
    try {
      const VarDef_t var_def( VarDefList::instance()->getVarDef(tag_var_name) );
      if ( !var_def.isStateWithOrWithoutHistory() ) {
	std::cerr << "ERROR [ActionTagLister::addCfgVarDef] variable " << tag_var_name << " already exists but it is not a state variable." << std::endl;
      }
      else {
	the_var_def=var_def;
      }
    }
    catch (UndefinedCalibrationVariable &) {
      // @todo ensure that kAborted is the last state
      VarDef_t var_def( VarDefList::instance()->createStateVarDef(tag_var_name, kRodValue, 1) );
      if ( var_def.isStateWithOrWithoutHistory() ) {
	StateVarDef_t &state_var_def = var_def.stateDef();
	state_var_def.addState(0," UNKNOWN");
      }
      the_var_def=var_def;
    }
  
    if (!the_var_def.isValid()) {
      std::string message("FATAL [ActionTagLister::addCfgVarDef] Did not manage to add variable ");
      message += tag_var_name;
      message += ".";

      throw std::runtime_error(message);
    }
    if ( the_var_def.isStateWithOrWithoutHistory() ) { 
      return the_var_def;
    }
    else {
      return VarDef_t::invalid();
    }
  }

  State_t ActionTagLister::getCfgTagId(const char *tag_name)
  {
    if (!m_cfgVarDef.isValid() || !m_modCfgVarDef.isValid()) {
      throw std::runtime_error("FATAL [ActionTagLister::getCfgTagId] Variable for cfg or mod cfg tags is invalid");
    }
    // only valid var defs should get assigned to m_cfgVarDef and m_modCfgVarDef if they are state var defs.
    assert(m_cfgVarDef.isStateWithOrWithoutHistory() ); 
    assert(m_modCfgVarDef.isStateWithOrWithoutHistory() ); 

    StateVarDef_t &cfg_state_def = m_cfgVarDef.stateDef();
    StateVarDef_t &mod_cfg_state_def = m_modCfgVarDef.stateDef();

    for (unsigned int state_i=0; state_i < cfg_state_def.nStates(); state_i++) {
      if (m_cfgVarDef.stateName(state_i) == tag_name) {
	return state_i;
      }
    }
    unsigned int new_state = cfg_state_def.nStates();
    cfg_state_def.addState(new_state, tag_name);
    mod_cfg_state_def.addState(new_state, tag_name);
    return new_state;
  }

  void ActionTagLister::run()
  {

    DoNothingExtractor mod_cfg_tag_extractor("MOD_CFG_TAG");
    DoNothingExtractor cfg_tag_extractor("CFG_TAG");

    for(;;) {
      m_hasNewAction.wait(m_abort);
      if(m_abort) break;

      if (m_verbose>0) {
	std::cout << "INFO [ActionTagLister::run] Actions availability changed, will wait "
		  << m_waitBeforeList << " sec. before trying to get the tags." << std::endl;
      }

      // wait until there are no updates anymore
      unsigned int current_counter;
      do {
	current_counter = m_counter;
	m_listWait.timedwait(m_waitBeforeList, m_abort);
	if (m_verbose>0 && current_counter != m_counter && !m_abort) {
	  std::cout << "INFO [ActionTagLister::run] Action availability changed in the mean time, will wait another "
		    << m_waitBeforeList << " sec. ." << std::endl;
	}
      } while (current_counter != m_counter && !m_abort);
      
      if (m_abort) break;

      for (unsigned int retry=5; --retry>0; ) {
	try {

	  IPCPartition partition(m_partitionName);

	  std::map<std::string,ipc::PixActions_var> actions;
	  partition.getObjects<ipc::PixActions>(actions);
	  unsigned int n_actions=0;
	  for(std::map<std::string,ipc::PixActions_var>::iterator action_iter = actions.begin();
	      action_iter != actions.end() && !m_abort;
	      action_iter++) {
	    try {
	      if(!CORBA::is_nil(action_iter->second)) {

		std::string action_name = action_iter->second->ipc_name();
		CORBA::Long type = action_iter->second->ipc_type();
		switch (type) {
		case PixLib::PixActions::SINGLE_ROD: {

		  // 		  CORBA::String_var conn_tag = action_iter->second->ipc_getConnTagC(action_name.c_str());
		  // 		  CORBA::String_var payload_tag = action_iter->second->ipc_getConnTagP(action_name.c_str());
		  // 		  CORBA::String_var alias_tag   = action_iter->second->ipc_getConnTagA(action_name.c_str());
		  // 		  CORBA::String_var id_tag      = action_iter->second->ipc_getConnTagOI(action_name.c_str());


		  CORBA::String_var cfg_tag     = action_iter->second->ipc_getCfgTag(action_name.c_str());
		  CORBA::String_var mod_cfg_tag = action_iter->second->ipc_getModCfgTag(action_name.c_str());

		  if (m_verbose>1) {
		    std::cout << "INFO [ActionTagLister::run] action " << action_name << " : " << cfg_tag << ", "  << mod_cfg_tag<< std::endl;
		  }

		  std::string::size_type rod_name_start=action_name.rfind("/");
		  if (rod_name_start!=std::string::npos && rod_name_start+1<action_name.size()) {
		    rod_name_start++;
		    std::string rod_name= action_name.substr(rod_name_start, action_name.size() - rod_name_start);

		    {
		      State_t cfg_tag_id = getCfgTagId(cfg_tag);
		      if (m_verbose>1) {
			std::cout << "INFO [ActionTagLister::run] ROD " << rod_name << " : " << cfg_tag << " -> " << cfg_tag_id << "." << std::endl;
		      }
		      m_receptor->setValue(kRodValue, kCurrent,0, rod_name,cfg_tag_extractor , cfg_tag_id);
		    }
		    {
		      State_t mod_cfg_tag_id = getCfgTagId(mod_cfg_tag);
		      if (m_verbose>1) {
			std::cout << "INFO [ActionTagLister::run] ROD " << rod_name << " : " << mod_cfg_tag << " -> " << mod_cfg_tag_id << "." << std::endl;
		      }
		      m_receptor->setValue(kRodValue, kCurrent,0, rod_name, mod_cfg_tag_extractor, mod_cfg_tag_id);
		    }
		    n_actions++;
		  }
		  break;
		}
		default:
		  break;
		}
	      }
	    }
	    catch (CORBA::TRANSIENT& err) {
	    }
	    catch (CORBA::COMM_FAILURE &err) {
	    }
	    catch (std::exception &err) {
	      std::cerr << "ERROR [ActionTagLister::run] Caught std exception : " << err.what()
			<< " for name." << std::endl;
	    }
	  }

	  if (n_actions>0) {
	    m_receptor->notify(new QNewVarEvent(kCurrent, 0, m_categoryName, VarDefList::instance()->varName(m_cfgVarDef.id()) ));
	    m_receptor->notify(new QNewVarEvent(kCurrent, 0, m_categoryName, VarDefList::instance()->varName(m_modCfgVarDef.id()) ));

	    if (m_verbose>1) {
	      std::cout << "INFO [ActionTagLister::run] report new var " <<  VarDefList::instance()->varName(m_cfgVarDef.id())
			<< ", " << VarDefList::instance()->varName(m_modCfgVarDef.id())
			<< " for category " << m_categoryName << std::endl;
	    }
	  }

	  break; // was successful don't need to retry
	}
	catch (CORBA::TRANSIENT& err) {
	}
	catch (CORBA::COMM_FAILURE &err) {
	}
	catch (std::exception &err) {
	  std::cerr << "ERROR [ActionTagLister::run] Caught std exception : " << err.what()
		    << " Did not get new connectivity tags." << std::endl;
	  break;
	}
      }

    }
    m_isRunning=false;
    m_exit.setFlag();
  }

  const char *ActionTagsMonitor::s_availableVar("AVAILABLE");

  ActionTagsMonitor::ActionTagsMonitor(const std::shared_ptr<IsReceptor> &receptor,
				       const std::string &action_server_partition_name,
				       const IVarNameExtractor &extractor,
				       const std::string &category_name,
				       const std::string &is_regex,
				       unsigned int wait_before_list)
    : SimpleIsMonitorReceiver<int,int>(receptor,
				       kRodValue,
				       2,
				       is_regex,
				       2*(132+15) /* buffer size*/,
				       UINT_MAX /* maximum time between updates.*/),
      m_tagLister(receptor,action_server_partition_name, wait_before_list, category_name),
      m_varNameExtractor(&extractor)
  {
    defineAvailable();
    //    IsMonitorReceiver<int,int>::init();
  }

  ActionTagsMonitor::ActionTagsMonitor(const std::shared_ptr<IsReceptor> &receptor,
				       const IVarNameExtractor &extractor,
				       const std::string &category_name,
				       const std::string &is_regex,
				       unsigned int wait_before_list)
    : SimpleIsMonitorReceiver<int,int>(receptor,
				       kRodValue,
                                       2,
				       is_regex,
				       2*(132+15) /* buffer size*/,
				       UINT_MAX /* maximum time between updates.*/),
      m_tagLister(receptor,receptor->receiver().partition().name(),wait_before_list, category_name),
      m_varNameExtractor(&extractor)
  {
    defineAvailable();
    //    IsMonitorReceiver<int,int>::init();
  }

  void ActionTagsMonitor::defineAvailable() {

    try {
      const VarDef_t var_def( VarDefList::instance()->getVarDef(s_availableVar) );
      if ( !var_def.isStateWithOrWithoutHistory() ) {
	std::cerr << "ERROR [ActionTagsMonitor::defineAvailable] variable " << s_availableVar << " already exists but it is not a state variable." << std::endl;
      }
    }
    catch (UndefinedCalibrationVariable &) {
      // @todo ensure that kAborted is the last state
      VarDef_t var_def( VarDefList::instance()->createStateVarDef(s_availableVar, kRodValue, 1) );
      if ( var_def.isStateWithOrWithoutHistory() ) {
	var_def.stateDef().addState(0, PixCon::DefaultColourCode::kFailure, "Allocated");
	var_def.stateDef().addState(1, PixCon::DefaultColourCode::kSuccess, "Not Allocated");
      }
    }

  }

  void ActionTagsMonitor::setValue(const std::string &is_name,  ::time_t time_stamp, int value) {
    //    std::cout << "INFO [ActionTagsMonitor::setValue] " << is_name << " =  " << value << std::endl;
    if (value==1) {
      // if there is a new action available then notify the action tag lister
      m_tagLister.availableAction();
    }
    receptor()->setValue(valueConnType(), kCurrent, 0, is_name, *m_varNameExtractor, time_stamp, static_cast<State_t>(value));
  }

  void ActionTagsMonitor::registerForCleanup(const std::string &is_name, std::map<std::string, std::set<std::string> > &clean_up_var_list) {
    ConnVar_t conn_var(kAllConnTypes  /* unused */);
    m_varNameExtractor->extractVar(is_name,conn_var);
    this->receptor()->registerForCleanup(kCurrent,0,conn_var.connName(), conn_var.varName(), clean_up_var_list);
  }

}


