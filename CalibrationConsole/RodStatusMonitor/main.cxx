#include <QApplication>
#include <memory>

#if QT_VERSION < 0x050000
  #include <QPlastiqueStyle>
#endif
#include "RodStatusMonitor.h"

#include <ConfigWrapper/ConnectivityManager.h>
#include <ConfigWrapper/createDbServer.h>

#if defined(HAVE_QTGSI)
#include "TQRootApplication.h"
#include "TQApplication.h"
#else
#include <TApplication.h>
#endif

#include <TEnv.h>
#include <TSystem.h>
#include <TROOT.h>

// for reading tags from IS :
#include <is/infoT.h>
#include <is/infodictionary.h>

const std::string s_appName("RodStatusMonitor");

int main( int argc, char ** argv )
{
#if QT_VERSION < 0x050000
  QApplication::setStyle(new QPlastiqueStyle());
#else
  QApplication::setStyle("fusion");
#endif

    std::shared_ptr<PixCon::UserMode> user_mode(new PixCon::UserMode(PixCon::UserMode::kShifter));
    std::string partition_name;
    std::string dt_partition_name; // name of the partition which contains the IS server for DataTakingConfig-.* if different from partition
    std::string object_tag_name;
    std::string tag_name;
    std::string cfg_tag_name;
    std::string mod_cfg_tag_name;
    std::string is_name = "RunParams";
    std::string is_name_for_tags = "PixelRunParams";
    std::string db_server_partition_name = "";
    std::string ddc_partition_name;
    std::string ddc_is_name;
    std::string disable_name;
    bool error=false;
    bool monitor_action_server_tags=false;

    std::vector<std::string> result_files;

    for (int arg_i=1; arg_i<argc; arg_i++) {
      if (strcmp(argv[arg_i],"-e")==0) {
	user_mode->setUserMode(PixCon::UserMode::kExpert);
      }
      else if (strcmp(argv[arg_i],"-p")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	partition_name = argv[++arg_i];
      }
      else if (strcmp(argv[arg_i],"--data-taking-partition")==0 && arg_i+1<argc) {
	dt_partition_name = argv[++arg_i];
      }
      else if (strcmp(argv[arg_i],"--db-server-partition")==0 && arg_i+1<argc) {
        db_server_partition_name = argv[++arg_i];
      }
      else if (strcmp(argv[arg_i],"-i")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	object_tag_name = argv[++arg_i];
      }
      else if (strcmp(argv[arg_i],"-t")==0 && arg_i+2<argc && argv[arg_i+2][0]!='-' && argv[arg_i+1][0]!='-') {
	tag_name = argv[++arg_i];
	cfg_tag_name = argv[++arg_i];
	if (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	  mod_cfg_tag_name = argv[++arg_i];
	}
      }
      else if (strcmp(argv[arg_i],"-n")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	is_name = argv[++arg_i];
      }
      else if (strcmp(argv[arg_i],"--dt-is-name")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	is_name_for_tags = argv[++arg_i];
      }
      else if (strcmp(argv[arg_i],"--ddc-is-name")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	ddc_is_name = argv[++arg_i];
      }
      else if (strcmp(argv[arg_i],"--ddc-partition-name")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	ddc_partition_name = argv[++arg_i];
      }
      else if ((strcmp(argv[arg_i],"-d")==0 || strcmp(argv[arg_i],"--disable")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	disable_name = argv[++arg_i];
      }
      else if ((strcmp(argv[arg_i],"--monitor-tags")==0)) {
	monitor_action_server_tags=true;
      }
      else {
	std::cout << "ERROR ["<< argv[0] << ":main] unhandled argument " << arg_i << " : " << argv[arg_i]<< "." << std::endl;
	error=true;
      }
    }

    if (error ) {
      std::cout << "usage: " << argv[0] << "-p partition -n \"IS server name\" [ [--data-taking-partition name] --dt-is-name server-name]"
		<< "  [-d/--disable disable-name] [-i ID-tag] [-t conn-tag cfg-tag [mod-cfg-tag]] [--monitor-tags]"
                << " [--ddc-partition-name partition-name] [--ddc-is-name is-server-name] [--db-server-partition name]" << std::endl<<std::endl
		<< "\tGenerally it is sufficient to start the RodMonitor in the following ways : " <<std::endl
		<< "\t\t" << argv[0] << " -p partition-name" <<std::endl
		<< "\t\t" << argv[0] << " -p partition-name --monitor-tags" <<std::endl;
      return -1;
    }

    int dargc = 1;
    IPCCore::init(dargc, argv);
  // QT-ROOT application

#if  defined(HAVE_QTGSI)
    int dummy_argc=1;
    TQApplication root_app(s_appName.c_str(),&dummy_argc,argv);

    // process root logon script
    // the logonscript could for example define some proper styles.
    std::cout << "INFO [main:" << argv[0] << "] execute ROOT macros from : " << gEnv->GetValue("Rint.Logon", (char*)0) << std::endl;
    const char *logon = gEnv->GetValue("Rint.Logon", (char*)0);
    if (logon) {
      char *mac = gSystem->Which(TROOT::GetMacroPath(), logon, kReadPermission);
      if (mac) {
	root_app.ProcessFile(logon);
      }
      delete [] mac;
    }
#else
    int dummy_argc=1;
    TApplication root_app(s_appName.c_str(),&dummy_argc,argv);

    // process root logon script
    // the logonscript could for example define some proper styles.
    std::cout << "INFO [main:" << argv[0] << "] execute ROOT macros from : " << gEnv->GetValue("Rint.Logon", (char*)0) << std::endl;
    const char *logon = gEnv->GetValue("Rint.Logon", (char*)0);
    if (logon) {
      char *mac = gSystem->Which(TROOT::GetMacroPath(), logon, kReadPermission);
      if (mac) {
	root_app.ProcessFile(logon);
      }
      delete [] mac;
    }
#endif

    std::unique_ptr<IPCPartition> db_server_partition;
    db_server_partition = std::make_unique<IPCPartition>(( db_server_partition_name.empty() ? partition_name.c_str() : db_server_partition_name.c_str()) );
    if (!db_server_partition->isValid()) {
      std::cerr << "ERROR [" << argv[0] << ":main] Not a valid partition " << partition_name
		<< " for the db server." << std::endl;
      return EXIT_FAILURE;
    }

    std::shared_ptr<PixLib::PixDbServerInterface> db_server( PixA::createDbServer(*db_server_partition,
										    "PixelDbServer" ));
    if (db_server.get()) {
      PixA::ConnectivityManager::useDbServer( db_server );
    }
    else {
      std::cout << "WARNING [main:" << argv[0] << "] Failed to create the db server." << std::endl;
    }

    try{

      int ret=-1;
      int qt_argc=1;
      {
      std::unique_ptr<QApplication> app;
      {
	RodStatusMonitor *rod_status_monitor = new RodStatusMonitor(qt_argc, argv, user_mode, partition_name, is_name,
								    dt_partition_name, is_name_for_tags,
								    ddc_partition_name, ddc_is_name,
								    monitor_action_server_tags);
      app=std::unique_ptr<QApplication>(rod_status_monitor);
      if (!object_tag_name.empty() && !tag_name.empty() && !cfg_tag_name.empty()) {
	std::cout << "INFO [" << argv[0] << ":main] Set tags : "<< object_tag_name << ":" << tag_name << "," << cfg_tag_name << std::endl;
	rod_status_monitor->setTags(object_tag_name,tag_name,cfg_tag_name, mod_cfg_tag_name);
      }
      if (!disable_name.empty()) {
	std::cout << "INFO [" << argv[0] << ":main] Apply disable : "<< disable_name << std::endl;
	rod_status_monitor->applyDisable(disable_name);
      }
      }

      ret  = (app.get() ? app->exec() : -1);
      std::cout << "INFO [" << argv[0] << ":main] application has finished." << std::endl;
      }
      std::cout << "INFO [" << argv[0] << ":main] quit." << std::endl;
    return ret;
    }
    catch(std::exception &ex) {
      std::cout << "FATAL [" << argv[0] << ":main] Caught standard exception  : " << ex.what() <<std::endl;
    }
    catch(...) {
      std::cout << "FATAL [" << argv[0] << ":main] Caught unknown exception." << std::endl;
    }
    return -1;
}
