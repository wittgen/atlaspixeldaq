// Dear emacs, this is -*-c++-*-
#ifndef _RodStatusMonitor_H_
#define _RodStatusMonitor_H_

#include <memory>
#include <vector>
#include <string>

#include <Lock.h>
#include "UserMode.h"

//#include <QNewVarEvent.h>
#include <ipc/partition.h>
#include <is/inforeceiver.h>
#include <qapplication.h>

namespace PixCon {
  class RodStatusMonitor_MainPanel;
  class CalibrationDataManager;
  class CategoryList;
  class IsReceptor;
  class IsMonitorBase;
  class IsMonitorManager;
  //class QNewVarEvent;
}

class RodStatusMonitor : public QApplication
{
public:
  RodStatusMonitor(int &qt_argc, char **qt_argv,
		   std::shared_ptr<PixCon::UserMode>& user_mode,
		   const std::string &partition_name,
		   const std::string &is_name,
		   const std::string &dt_partition_name, 
		   const std::string &dt_is_name,
                   const std::string &ddc_partition_name,
                   const std::string &ddc_is_name,
		   bool monitor_action_server_tags=false);
  ~RodStatusMonitor();

  void setTags(const std::string &object_tag, const std::string &tag_name, const std::string &cfg_tag_name, const std::string &mod_cfg_tag);

  void applyDisable(const std::string &disable_name);

protected:

  void setupIsMonitor(int /*ipc_argc*/, char **/*ipc_argv*/,
		      const std::string &partition_name,
		      const std::string &is_name,
		      const std::string &dt_partition_name,
		      const std::string &dt_is_name,
                      const std::string &ddc_partition_name,
                      const std::string &ddc_is_name);


  std::shared_ptr<PixCon::UserMode> m_userMode;
  std::shared_ptr<PixCon::CategoryList>           m_categories;
  std::shared_ptr<PixCon::CalibrationDataManager> m_calibrationData;

  std::unique_ptr<IPCPartition>           m_ipcPartition;
  std::shared_ptr<PixCon::IsReceptor> m_receptor;

  std::unique_ptr<IPCPartition>           m_dtPartition;
  std::shared_ptr<PixCon::IsReceptor> m_dtReceptor;

  std::unique_ptr<IPCPartition>           m_ddcPartition;
  std::shared_ptr<PixCon::IsReceptor> m_ddcReceptor;

  std::unique_ptr<PixCon::RodStatusMonitor_MainPanel>  m_detectorView;

  std::shared_ptr< PixCon::IsMonitorManager > m_isMonitorManager;
  bool m_monitorActionServerTags;
  //PixCon::QNewVarEvent m_qNewVarEvent;

private:

};
#endif
