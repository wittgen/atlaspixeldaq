#include "RodStatusMonitor.h"
#include "RodStatusMonitor_MainPanel.h"

#include "IHistogramProvider.h"

#include <CalibrationConsoleContextMenu.h>
#include <RodRecovery.h>
#include <HistogramCache.h>
#include <DefaultColourCode.h>
#include <IsMonitor.h>
#include <DcsVariables.h>
#include <QIsInfoEvent.h>

#include <SaveDisable.h>

#include <FullDetectorLogoFactory.h>

#include <DisablePanel.h>

#include "ActionTagsMonitor.h"
#include "TagsMonitor.h"

#include <QMessageBox>

#include <ConnectivityEvents.h>
#include <ConsoleBusyLoopFactory.h>
#include <ConfigWrapper/PixDisableUtil.h>

#include <IsReceptor.h>
#include <IsMonitorManager.h>

#include <KillDialogue.h>

#ifdef HAVE_DDC
#  include <DcsMonitorManager.h>
#endif

time_t m_lastUpdate;

namespace PixCon {

  class IsStrListMonitor : public IsStringReceiver
  {
  public:

    IsStrListMonitor(std::shared_ptr<IsReceptor> &receptor,
		     EValueConnType conn_type,
		     const std::string &value_after_deletion,
		     const IVarNameExtractor &extractor,
		     const std::string &is_regex,
		     std::map<std::string, unsigned int> map)
      : IsStringReceiver(receptor, conn_type, value_after_deletion, is_regex),
	m_varNameExtractor(&extractor),
        m_map(map)
    { assert( IsStringReceiver::receptor().get() != NULL );  }

  protected:
    void setValue(const std::string &is_name, ::time_t time_stamp, const std::string &value) {
      unsigned short dest_value = 0;
      if (m_map.find(value) != m_map.end()) dest_value = m_map[value];
      IsStringReceiver::receptor()->setValue(IsStringReceiver::valueConnType(), is_name, *m_varNameExtractor, time_stamp, dest_value);
    }

    void registerForCleanup(const std::string &is_name, std::map<std::string, std::set<std::string> > &clean_up_var_list) {
      ConnVar_t conn_var(IsStringReceiver::valueConnType()  /* unused */);
      m_varNameExtractor->extractVar(is_name,conn_var);
      this->receptor()->registerForCleanup(kCurrent,0,conn_var.connName(), conn_var.varName(), clean_up_var_list);
    }

    IVarNameExtractor const *m_varNameExtractor;
    std::map<std::string, unsigned int> m_map;
  };


  /** Do nothing histogram server to make the context menu happy.
   */
  class DummyHistogramProvider : public IHistogramProvider
  {
  public:
    virtual HistoPtr_t loadHistogram(EMeasurementType /*type*/,
                                     SerialNumber_t /*serial_number*/,
                                     EConnItemType /*conn_item_type*/,
                                     const std::string &/*connectivity_name*/,
                                     const std::string &/*histo_name*/,
                                     const Index_t &/*index*/) { return NULL; }

    virtual void requestHistogramList(EMeasurementType /*type*/,
                                      SerialNumber_t /*serial_number*/,
                                      EConnItemType /*conn_item_type*/,
                                      std::vector< std::string > &histogram_name_list ) { histogram_name_list.clear();}

    virtual void requestHistogramList(EMeasurementType type,
				      SerialNumber_t serial_number,
				      EConnItemType conn_item_type,
				      std::vector< std::string > &histogram_name_list,
				      const std::string &/*rod_name*/) {
      requestHistogramList(type,serial_number, conn_item_type, histogram_name_list);
    }


    virtual void numberOfHistograms(EMeasurementType /*type*/,
                                    SerialNumber_t /*serial_number*/,
                                    EConnItemType /*conn_item_type*/,
                                    const std::string &/*connectivity_name*/,
                                    const std::string &/*histo_name*/,
                                    HistoInfo_t &info) { info.reset(); }
    void reset() {}
  };


  class DAQEnabledNameExtractor : public IVarNameExtractor 
  {
  public:
    void extractVar(const std::string &full_is_name, ConnVar_t &conn_var) const
    {
      // full_is_name = RunParams.ROD_CRATE_?/ROD_C??_S??/varname
      // full_is_name = RunParams.ROD_CRATE_?/ROD_C??_S??/??_B??_S?_??_M??/varname
      //std::cout << "full_is_name = "  << full_is_name << std::endl;
      unsigned int cslash = 0;
      for (unsigned int i = 0; i < full_is_name.length(); i++) {
	if (full_is_name[i] == '/') cslash++;
      }
      std::string::size_type conn_start = full_is_name.find("/");
      if (conn_start != std::string::npos) {
	if (cslash == 2) {
	  std::string::size_type conn_end = full_is_name.rfind("/");
	  if (conn_end != std::string::npos) {
	    conn_var.connName()=std::string(full_is_name,conn_start+1,conn_end-conn_start-1);
	    //std::cout << "connName = "  << conn_var.connName() << std::endl;
	    conn_var.varName()=std::string(full_is_name,conn_end+1,full_is_name.size()-conn_end-1);
	    //std::cout << "varName = " <<  conn_var.varName() << std::endl;
	  }
	}
	else {
	  std::string rd = full_is_name.substr(conn_start+1);
	  std::string::size_type md_start = rd.find("/");
	  std::string::size_type conn_end = rd.rfind("/");
	  if (conn_end != std::string::npos) {
	    conn_var.connName()=std::string(rd,md_start+1,conn_end-md_start-1);
	    //std::cout << "connName = "  << conn_var.connName() << std::endl;
	    conn_var.varName()=std::string(rd,conn_end+1,rd.size()-conn_end-1);
	    //std::cout << "varName = " <<  conn_var.varName() << std::endl;
	  }
	}
      }

    }
    
  };
  DAQEnabledNameExtractor s_daqEnabledNameExtractor;

  class RodAvailableNameExtractor : public IVarNameExtractor 
  {
  private:
  public:
    RodAvailableNameExtractor() { m_lastUpdate = time(NULL); }
    void extractVar(const std::string &full_is_name, ConnVar_t &conn_var) const 
    {
      if (time(NULL)-m_lastUpdate > 15) {
        {
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->getVarDef("LAST-WD-CALL");	
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(time(NULL)-200,time(NULL)+200);
	  }
	}
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->getVarDef("LAST-CMD-START");	
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(time(NULL)-900,time(NULL)+100);
	  }
	}
	m_lastUpdate = time(NULL);
      }
      // full_is_name = RunParams.ROD_CRATE_?/ROD_C??_S??/varname
      //std::cout << "full_is_name = "  << full_is_name << std::endl;
      std::string::size_type conn_start = full_is_name.find("/");
      if (conn_start != std::string::npos) {
	std::string::size_type conn_end = full_is_name.rfind("/");
	if (conn_end != std::string::npos) {
	  conn_var.connName()=std::string(full_is_name,conn_start+1,conn_end-conn_start-1);
	  //std::cout << "connName = "  << conn_var.connName() << std::endl;
	  conn_var.varName()=std::string(full_is_name,conn_end+1,full_is_name.size()-conn_end-1);
	  //std::cout << "varName = " <<  conn_var.varName() << std::endl;
	}
      }

    }

  };
  RodAvailableNameExtractor s_rodAvailableNameExtractor;

  class ModAvailableNameExtractor : public IVarNameExtractor 
  {
  public:
    void extractVar(const std::string &full_is_name, ConnVar_t &conn_var) const
    {
      // full_is_name = RunParams.ROD_C??_S??/??_B??_S?_??_M??/varname
      //std::cout << "full_is_name = "  << full_is_name << std::endl;
      std::string::size_type conn_start = full_is_name.find("/");
      if (conn_start != std::string::npos) {
	std::string::size_type conn_end = full_is_name.rfind("/");
	if (conn_end != std::string::npos) {
	  conn_var.connName()=std::string(full_is_name,conn_start+1,conn_end-conn_start-1);
	  //std::cout << "connName = "  << conn_var.connName() << std::endl;
	  conn_var.varName()=std::string(full_is_name,conn_end+1,full_is_name.size()-conn_end-1);
	  //std::cout << "varName = " <<  conn_var.varName() << std::endl;
	}
      }
      
    }
    
  };
  ModAvailableNameExtractor s_modAvailableNameExtractor;

  class ModFmtStatNameExtractor : public IVarNameExtractor 
  {
  public:
    void extractVar(const std::string &full_is_name, ConnVar_t &conn_var) const
    {
      // full_is_name = RunParams.ROD_CRATE_??/ROD_C??_S??/??_B??_S?_??_M??/varname
      //std::cout << "full_is_name = "  << full_is_name << std::endl;
      std::string::size_type conn_start = full_is_name.find("/");
      if (conn_start != std::string::npos) {
	std::string rd = full_is_name.substr(conn_start+1);
	std::string::size_type md_start = rd.find("/");
	std::string::size_type conn_end = rd.rfind("/");
	if (conn_end != std::string::npos) {
	  conn_var.connName()=std::string(rd,md_start+1,conn_end-md_start-1);
	  //std::cout << "connName = "  << conn_var.connName() << std::endl;
	  conn_var.varName()=std::string(rd,conn_end+1,rd.size()-conn_end-1);
	  //std::cout << "varName = " <<  conn_var.varName() << std::endl;
	}
      }
      
    }
    
  };
  ModFmtStatNameExtractor s_modFmtStatNameExtractor;


  //----------------New Variables----------------------------



  class ModFmtLinknRSTNameExtractor : public IVarNameExtractor
  {
  public:
    void extractVar(const std::string &full_is_name, ConnVar_t &conn_var) const
    {
      // full_is_name = RunParams.ROD_CRATE_??/ROD_C??_S??/??_B??_S?_??_M??/varname
      //std::cout << "full_is_name = "  << full_is_name << std::endl;
      std::string::size_type conn_start = full_is_name.find("/");
      if (conn_start != std::string::npos) {
	std::string rd = full_is_name.substr(conn_start+1);
	std::string::size_type md_start = rd.find("/");
	std::string::size_type conn_end = rd.rfind("/");
	if (conn_end != std::string::npos) {
	  conn_var.connName()=std::string(rd,md_start+1,conn_end-md_start-1);
	  //std::cout << "connName = "  << conn_var.connName() << std::endl;
	  conn_var.varName()=std::string(rd,conn_end+1,rd.size()-conn_end-1);
	  //std::cout << "varName = " <<  conn_var.varName() << std::endl;
	}
      }

    }

  };
  ModFmtLinknRSTNameExtractor s_modFmtLinknRSTNameExtractor;





  //----------------end of new variables-------------------------






  class RodBusyPercNameExtractor : public IVarNameExtractor 
  {
     public:
    void extractVar(const std::string &full_is_name, ConnVar_t &conn_var) const
    {
      // full_is_name = RunParams.ROD_C??_S??/varname
      //std::cout << "full_is_name = "  << full_is_name << std::endl;
      std::string::size_type conn_start = full_is_name.find(".");
      if (conn_start != std::string::npos) {
	std::string::size_type conn_end = full_is_name.rfind("/");
	if (conn_end != std::string::npos) {
	  conn_var.connName()=std::string(full_is_name,conn_start+1,conn_end-conn_start-1);
	  //std::cout << "connName = "  << conn_var.connName() << std::endl;
	  conn_var.varName()=std::string(full_is_name,conn_end+1,full_is_name.size()-conn_end-1);
	  //std::cout << "varName = " <<  conn_var.varName() << std::endl;
	}
      }

    }

  };
  RodBusyPercNameExtractor s_rodBusyPercNameExtractor;

  //-----------------------New Variables-------------------------------


  class RodQSModeNameExtractor : public IVarNameExtractor
  {
  public:
    void extractVar(const std::string &full_is_name, ConnVar_t &conn_var) const
    {
      // full_is_name = RunParams.ROD_CRATE_?/ROD_C??_S??/varname
      //std::cout << "full_is_name = "  << full_is_name << std::endl;
      std::string::size_type conn_start = full_is_name.find("/");
      if (conn_start != std::string::npos) {
	std::string::size_type conn_end = full_is_name.rfind("/");
	if (conn_end != std::string::npos) {
	  conn_var.connName()=std::string(full_is_name,conn_start+1,conn_end-conn_start-1);
	  //std::cout << "connName = "  << conn_var.connName() << std::endl;
	  conn_var.varName()=std::string(full_is_name,conn_end+1,full_is_name.size()-conn_end-1);
	  //std::cout << "varName = " <<  conn_var.varName() << std::endl;
	}
      }

    }

  };
  RodQSModeNameExtractor s_rodQSModeNameExtractor;

  //---------------end of new variables---------------------------------


  class DspMonNameExtractor : public IVarNameExtractor
  {
    public:
      void extractVar(const std::string &full_is_name, ConnVar_t &conn_var) const
      {
        //full_is_name = RunParams.ROD_C??_S??/DSPMon_varname
        //std::cout << "full_is_name = "  << full_is_name << std::endl;
        std::string::size_type conn_start = full_is_name.find(".");
        if (conn_start != std::string::npos) {
          std::string::size_type conn_end = full_is_name.rfind("/DSPMon_"); // Prefix size: 8
          if (conn_end != std::string::npos) {
            conn_var.connName()=std::string(full_is_name,conn_start+1,conn_end-conn_start-1);
            //std::cout << "connName = "  << conn_var.connName() << std::endl;
            conn_var.varName()=std::string(full_is_name,conn_end+8,full_is_name.size()-conn_end-8);
            //std::cout << "varName = " <<  conn_var.varName() << std::endl;
          }
        }

      }

  };
  DspMonNameExtractor s_dspMonNameExtractor;

  class BocMonitoringNameExtractor : public IVarNameExtractor 
  {
     public:
    void extractVar(const std::string &full_is_name, ConnVar_t &conn_var) const
    {
      // full_is_name = RunParams.Monitoring/ROD_C??_S??/varname
      //std::cout << "full_is_name = "  << full_is_name << std::endl;
      std::string::size_type conn_start = full_is_name.find("/");
      if (conn_start != std::string::npos) {
	std::string::size_type conn_end = full_is_name.rfind("/");
	if (conn_end != std::string::npos) {
	  conn_var.connName()=std::string(full_is_name,conn_start+1,conn_end-conn_start-1);
	  //std::cout << "connName = "  << conn_var.connName() << std::endl;
	  conn_var.varName()=std::string(full_is_name,conn_end+1,full_is_name.size()-conn_end-1);
	  //std::cout << "varName = " <<  conn_var.varName() << std::endl;
	}
      }

    }

  };
  BocMonitoringNameExtractor s_bocMonitoringNameExtractor;

  RodStatusMonitor_MainPanel::RodStatusMonitor_MainPanel(std::shared_ptr<PixCon::UserMode>& user_mode,
							 const std::string &db_server_partition_name,
							 const std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data,
							 const std::shared_ptr<PixCon::HistogramCache> &histogram_cache,
							 const std::shared_ptr<PixCon::CalibrationDataPalette> &palette,
							 const std::shared_ptr<PixCon::CategoryList> &categories,
							 const std::shared_ptr<PixCon::IContextMenu> &context_menu,
                        				 const std::shared_ptr<PixDet::ILogoFactory> &logo_factory,
							 const std::shared_ptr<VisualiserList> &visualiser_list,
							 const std::shared_ptr< PixCon::IsMonitorManager >  &monitor_manager,
							 QApplication &application,
							 QWidget* parent)
    : ScanMainPanel(user_mode,
		    calibration_data,
		    histogram_cache,
		    palette,
		    categories,
		    context_menu,
		    std::shared_ptr<PixCon::IMetaDataUpdateHelper>(),
          	    logo_factory,
		    visualiser_list,
		    application,
		    parent),
      m_dbServerPartition(new IPCPartition(db_server_partition_name.c_str()) ),
      m_changeTagsTimer(new QTimer(this)),
      m_isMonitorManager(monitor_manager)
  {
    setWindowTitle("ROD Status Monitor");
    if (mainWindowRightPanel->count()>1 && mainWindowRightPanel->widget(0)) {
      mainWindowRightPanel->setCurrentIndex(1);
      m_removedDataList = mainWindowRightPanel->widget(0);
      mainWindowRightPanel->removeTab(0);
    }

    clearFilterSelector();
    addItemToFilterSelector("Enabled");
    addItemToFilterSelector("Disabled");
    addItemToFilterSelector("DAQEnabled==ENABLED");
    addItemToFilterSelector("DAQEnabled!=ENABLED && DAQEnabled!=DISBYCFG");
    addItemToFilterSelector("NOcc>1e-4");
    addItemToFilterSelector("NOcc>1e-5");
    addItemToFilterSelector("NOcc>1e-6");
    addItemToFilterSelector("BCID_errors>1e+5");
    addItemToFilterSelector("LVL1ID_errors>1e+5");

    /*
    unsigned int file_index = 0;
    File->insertItem("&Load Disable",this, SLOT( loadDisable() ), Qt::CTRL+Qt::Key_L, kFileLoadDisable, file_index++);
    File->insertItem("&Save Disable",this, SLOT( saveDisable() ), Qt::CTRL+Qt::Key_S, kFileSaveDisable, file_index++);

    File->insertSeparator(file_index++);
    File->insertItem("Reconnect &IPC",this, SLOT( reconnectIPC()), Qt::CTRL+Qt::Key_I, kFileReconnectIPC, file_index++);
    */

    QAction *menuAct;

    menuAct = new QAction("&Load Disable",this);
    menuAct->setShortcut(Qt::CTRL+Qt::Key_L);
    connect(menuAct, SIGNAL( triggered(bool) ), this, SLOT( loadDisable() ));
    File->insertAction(m_sepExitAction, menuAct);

    menuAct = new QAction("&Save Disable",this);
    menuAct->setShortcut(Qt::CTRL+Qt::Key_S);
    connect(menuAct, SIGNAL( triggered(bool) ), this, SLOT( saveDisable() ));
    File->insertAction(m_sepExitAction, menuAct);

    menuAct = new QAction("Reconnect &IPC",this);
    menuAct->setShortcut(Qt::CTRL+Qt::Key_I);
    connect(menuAct, SIGNAL( triggered(bool) ), this, SLOT( reconnectIPC() ));
    File->insertAction(m_sepExitAction, menuAct);
    File->insertSeparator(menuAct);

    //unsigned int utils_index = 0;
    //if (MenuBar->isItemVisible(3) == false) MenuBar->setItemVisible(3, true);
    if(!Utils->menuAction()->isVisible()) Utils->menuAction()->setVisible(true);
    Utils->addAction("&ROD Recovery", this, SLOT( recoverBusyROD() ), Qt::CTRL+Qt::Key_R);//, kUtilsRODRecovery, utils_index++);

    connect(m_changeTagsTimer,SIGNAL( timeout() ),this,SLOT( changeTags() ) );

    for (int tab_i = 0; tab_i<mainWindowRightPanel->count(); tab_i++) {
      if (mainWindowRightPanel->tabText(tab_i)=="Data List") {
    mainWindowRightPanel->removeTab(tab_i);
      }
    }
    hideSerialNumberSelector();
    hideLogWindow();

  }

  RodStatusMonitor_MainPanel::~RodStatusMonitor_MainPanel() {
    shutdown();
  }

  void RodStatusMonitor_MainPanel::shutdown() {
    {
    KillDialogue kill_dialog_timer(this, "RodStatusMonitor",10);
    hide();
#ifdef HAVE_DDC
    //std::cout << "INFO [RodStatusMonitor_MainPanel::shutdown] dcs monitoring." << std::endl;
    m_dcsMonitorManager->shutdown();
#endif
    m_isMonitorManager->shutdown();
    mainWindowRightPanel->addTab(m_removedDataList, "Data List");
    }
    ScanMainPanel::shutdown();
    //    std::cout << "INFO [RodStatusMonitor_MainPanel::shutdown] reset pointer." << std::endl;
    //    m_dcsMonitorManager.reset();
    //    m_isMonitorManager.reset();
    //    std::cout << "INFO [RodStatusMonitor_MainPanel::shutdown] done." << std::endl;
  }

  void RodStatusMonitor_MainPanel::setupDcsMonitorManager(const std::shared_ptr<PixCon::IsReceptor> &receptor)
  {
#ifdef HAVE_DDC
    m_dcsMonitorManager=std::unique_ptr<DcsMonitorManager>(new DcsMonitorManager(receptor,
									       monitorManager(),
									       calibrationData(),
									       categories()));
    connect(m_dcsMonitorManager.get(),SIGNAL(updateCategories()), this, SLOT(updateAllCategories()));
    //unsigned int utils_index = 0;
    //if (MenuBar->isItemVisible(3) == false) MenuBar->setItemVisible(3, true);
    if(!Utils->menuAction()->isVisible()) Utils->menuAction()->setVisible(true);
    Utils->addAction("&DCS Monitor", m_dcsMonitorManager.get(), SLOT( showDcsSubscription() ), Qt::CTRL+Qt::Key_D);//, kUtilsDCSMonitor, utils_index++);
    Utils->addSeparator();//utils_index++);
#endif
  }


  bool RodStatusMonitor_MainPanel::event(QEvent *an_event)
  {
    if (an_event && an_event->type() == QEvent::User) {

      PixCon::QNewTagEvent *new_tag_event  = dynamic_cast< PixCon::QNewTagEvent *>(an_event);
      if (new_tag_event) {

	m_nextIdTag = new_tag_event->idTag();
	m_nextConnTag = new_tag_event->connTag();
	m_nextCfgTag = new_tag_event->cfgTag();
	m_nextModCfgTag = new_tag_event->modCfgTag();

	if (m_changeTagsTimer->isActive()) {
	  m_changeTagsTimer->stop();
	}
	if ( !m_nextIdTag.empty() && !m_nextConnTag.empty() && !m_nextCfgTag.empty()  ) {
	  m_changeTagsTimer->setSingleShot(true);
	  m_changeTagsTimer->start((s_updateTagsDelay > 0) ? s_updateTagsDelay : 0);
	}
      }
      else {
	PixCon::QNewDisableEvent *new_disable_event  = dynamic_cast< PixCon::QNewDisableEvent *>(an_event);
	if (new_disable_event) {
	  if (!new_disable_event->disableName().empty()) {
	    loadDisable(new_disable_event->disableName());
	  }
	}
	else {
	  PixCon::QConnectivityChangedEvent *new_conn_event  = dynamic_cast< PixCon::QConnectivityChangedEvent *>(an_event);
	  if (new_conn_event) {
	    changeConnectivity(kCurrent,0);
	  }
	}
      }
    }

    return ScanMainPanel::event(an_event);
  }


  void RodStatusMonitor_MainPanel::loadDisable()
  {
    PixA::ConnectivityRef conn(calibrationData()->currentConnectivity());
    if (conn) {
      DisablePanel dp(conn, NULL);
      if (dp.exec() == QDialog::Accepted) {
	loadDisable( dp.getDisable() );
      }
    }
  }

  void RodStatusMonitor_MainPanel::loadDisable(const std::string &name) {
    try {
      PixA::ConnectivityRef conn(calibrationData()->currentConnectivity());
      if (conn) {
	std::shared_ptr<const PixLib::PixDisable> disable;
	{
	  std::unique_ptr<PixA::IBusyLoop> busy_loop(PixA::BusyLoopFactory::busyLoop(std::string(" Load disable ")+name+" ..."));
	  // get the most recent version
	  PixA::ConnObjConfigDb config_db(PixA::ConnObjConfigDb::sharedInstance());
	  PixA::ConnObjConfigDbTagRef config_tag(config_db.getTag(conn.tag(PixA::IConnectivity::kId), conn.tag(PixA::IConnectivity::kConfig),0));
	  disable=config_tag.disablePtr(name);
	}
	loadDisable(disable);
      }
    }
    catch (PixA::ConfigException &err) {
      std::cerr << "WARNING [RodStatusMonitor_MainPanel::loadDisable] Caught exception while trying to load disable " <<name << ":" 
		<< err.getDescriptor()
		<< std::endl;
    }
    catch (...) {
      std::cerr << "WARNING [RodStatusMonitor_MainPanel::loadDisable] Caught unknown exception while trying to load disable " <<name << "." 
		<< std::endl;
    }
  }

  void RodStatusMonitor_MainPanel::loadDisable(const std::shared_ptr<const PixLib::PixDisable> &disable) {
    if (disable.get()) {
      {
	Lock lock(calibrationData()->calibrationDataMutex());
	PixA::DisabledListRoot disabled_list_root(disable);
	calibrationData()->setEnable(PixCon::kCurrent,0, disabled_list_root);
      }

      changeConnectivity(PixCon::kCurrent,0);
    }
  }

  void RodStatusMonitor_MainPanel::saveDisable() {

    PixA::ConnectivityRef conn(calibrationData()->currentConnectivity());
    if (!conn) {
      std::cerr << "ERROR [RodStatusMonitor_MainPanel::saveDisable] No connectivity, don't know where to get tags from." << std::endl;
      return;
    }
    std::pair<PixCon::EMeasurementType, PixCon::SerialNumber_t>  measurement =selectedMeasurement();

    std::unique_ptr<ISelectionFactory> selection_factory( createSelectionFactory() );
    SaveDisable save_disable_dialog(conn,
				    measurement,
				    calibrationData(),
				    *selection_factory,
				    this);
    save_disable_dialog.execute();
  }

  void RodStatusMonitor_MainPanel::changeTags() {
    if (!m_nextIdTag.empty() && !m_nextConnTag.empty() && !m_nextCfgTag.empty()) {
      std::cout << "INFO [RodStatusMonitor_MainPanel::changeTags] change tags ? "
		<< m_nextIdTag << ":" << m_nextConnTag << ";" << m_nextCfgTag << "," << m_nextModCfgTag << std::endl;
      PixA::ConnectivityRef conn(calibrationData()->currentConnectivity());
      if (!conn
	  || m_nextIdTag   != conn.tag(PixA::IConnectivity::kId)
	  || m_nextConnTag != conn.tag(PixA::IConnectivity::kConn)
	  || m_nextConnTag != conn.tag(PixA::IConnectivity::kAlias)
	  || m_nextConnTag != conn.tag(PixA::IConnectivity::kPayload)
	  || m_nextCfgTag  != conn.tag(PixA::IConnectivity::kConfig)
	  || m_nextModCfgTag  != conn.tag(PixA::IConnectivity::kModConfig)) {

	changeConnectivity(m_nextIdTag,
			   m_nextConnTag, m_nextConnTag, m_nextConnTag,
			   m_nextCfgTag, ( m_nextModCfgTag.empty() ? m_nextCfgTag : m_nextModCfgTag) , 0);

	updateMonitors();
      }
    }
  }

  void RodStatusMonitor_MainPanel::updateMonitors() {
    //Re-read all monitored values from IS
    try {
      std::cerr << "INFO [RodStatusMonitor_MainPanel::update] Request IPC reconnection for all IS monitors." << std::endl;
      m_isMonitorManager->reread();
    }
    catch(...) {
      std::cerr << "ERROR [RodStatusMonitor_MainPanel::update] Exception while trying to reread monitored values." << std::endl;
    }

    //enforce an update of categories
    updateCategories(categories());
  }

  void RodStatusMonitor_MainPanel::updateAllCategories() { 
    updateCategories(categories());
  }

  void RodStatusMonitor_MainPanel::reconnectIPC() {
    std::unique_ptr<PixA::IBusyLoop> busy_loop(PixA::BusyLoopFactory::busyLoop(" Reconnecting IPC ..."));
    monitorManager()->reconnect();
  }

  void RodStatusMonitor_MainPanel::recoverBusyROD()
  {
    RodRecovery rodRecovery;
    if (rodRecovery.initialize() == true) {
      int answer = QMessageBox::question(this, "ROD recovery",
					 "You have requested to execute the ROD recovery script.\n\n"
					 "Do you really want to continue with ROD recovery ?",
					 QMessageBox::No, QMessageBox::Yes);
      if (answer == QMessageBox::Yes) {
	int rvalue = rodRecovery.execute();
	if (rvalue == 0) {
	  std::cout << "INFO [RodStatusMonitor_MainPanel::recoverBusyROD] ROD recovery script executed successfully." << std::endl;
	}
	else {
	  std::cout << "ERROR [RodStatusMonitor_MainPanel::recoverBusyROD] Problem in execution of ROD recovery script !" << std::endl;
	}
      }
    }
    else {
      std::cout << "ERROR [RodStatusMonitor_MainPanel::recoverBusyROD] ROD recovery script "
		<< rodRecovery.getRecoveryScriptName() << " does not exist !" << std::endl;
    }
  }

}


RodStatusMonitor::RodStatusMonitor(int &qt_argc,
				   char **qt_argv,
				   std::shared_ptr<PixCon::UserMode>& user_mode,
				   const std::string &partition_name,
				   const std::string &is_name,
				   const std::string &dt_partition_name,
				   const std::string &dt_is_name,
	                           const std::string &ddc_partition_name,
                                   const std::string &ddc_is_name,
				   bool monitor_action_server_tags)
  : QApplication(qt_argc, qt_argv),
    m_userMode(user_mode),
    m_categories(new PixCon::CategoryList),
    m_calibrationData(new PixCon::CalibrationDataManager),
    m_monitorActionServerTags(monitor_action_server_tags)
{
  // histogram server will be unused but the context menu wants one
  std::shared_ptr<PixCon::HistogramCache> histogram_cache( new PixCon::HistogramCache(std::make_shared<PixCon::DummyHistogramProvider>() ));

  std::shared_ptr<PixCon::CalibrationDataPalette> palette(new PixCon::CalibrationDataPalette);
  palette->setDefaultPalette();

  std::shared_ptr<PixCon::VisualiserList> visualiser_list(new PixCon::VisualiserList);
  std::shared_ptr<PixCon::IContextMenu> context_menu(  new PixCon::CalibrationConsoleContextMenu(m_calibrationData, histogram_cache, visualiser_list, partition_name) );

  m_detectorView=std::make_unique<PixCon::RodStatusMonitor_MainPanel>( user_mode,
													   partition_name,
													   m_calibrationData,
													   histogram_cache,
													   palette,
													   m_categories,
													   context_menu,
                                                                                                        std::shared_ptr<PixDet::ILogoFactory>(new PixDet::FullDetectorLogoFactory),
                                                                                                        visualiser_list,
													   std::make_shared<PixCon::IsMonitorManager>(),
													   *this );
  std::cout << "INFO [RodStatusMonitor::ctor] setup is monitors." << std::endl;
  setupIsMonitor(0,qt_argv, partition_name, is_name, dt_partition_name, dt_is_name,ddc_partition_name, ddc_is_name);
  std::cout << "INFO [RodStatusMonitor::ctor] done." << std::endl;

  m_detectorView->updateCategories(m_categories);
  
  m_detectorView->show();
  connect( this, SIGNAL( lastWindowClosed() ), this, SLOT( quit() ) );
}

RodStatusMonitor::~RodStatusMonitor()
{
  //  std::cout << "INFO [RodStatusMonitor::dtor] enter." << std::endl;
  m_detectorView.reset();
  //std::cout << "INFO [RodStatusMonitor::dtor] done." << std::endl;
}

void RodStatusMonitor::setTags(const std::string &object_tag_name, const std::string &tag_name, const std::string &cfg_tag_name, const std::string &mod_cfg_tag_name)
{
  if (!object_tag_name.empty() && !tag_name.empty() && !cfg_tag_name.empty()) {
    m_detectorView->changeConnectivity(object_tag_name, tag_name,tag_name,tag_name,cfg_tag_name,mod_cfg_tag_name, 0);
    m_detectorView->updateMonitors();

  }
}

void RodStatusMonitor::applyDisable(const std::string &disable_name) {
  m_detectorView->loadDisable(disable_name);
}


void RodStatusMonitor::setupIsMonitor(int /*ipc_argc*/, char **/*ipc_argv*/,
				      const std::string &partition_name,
				      const std::string &is_name,
				      const std::string &dt_partition_name,
				      const std::string &dt_is_name,
                                      const std::string &orig_ddc_partition_name,
                                      const std::string &orig_ddc_is_name)
{
  std::string ddc_is_name = orig_ddc_is_name;
  std::string ddc_partition_name = orig_ddc_partition_name;
  if (!partition_name.empty() && !is_name.empty()) {
    //add category to hold variables that will be monitored
    //PixCon::CategoryList::Category dcs_cat=m_categories->addCategory("DCS");

    // start partition and info receiver that will be used by receptor
    m_ipcPartition = std::make_unique<IPCPartition>( partition_name.c_str() );

    //Check partition and IS
    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] partition_name = " << partition_name << "; is_name = " << is_name << std::endl;

    //receptor starts thread and communicates with main panel
    m_receptor = std::make_shared<PixCon::IsReceptor>(this, //QApplication
									      m_detectorView.get(), //QObject receiver object
									      is_name, 
									      *m_ipcPartition,
									      m_calibrationData);
    if (ddc_is_name.empty()) {
      ddc_is_name = is_name;
    }
    if (ddc_partition_name.empty()) {
       ddc_partition_name = partition_name;
    }

    if (ddc_partition_name != partition_name && !m_ddcPartition.get()) {
      m_ddcPartition = std::make_unique<IPCPartition>( ddc_partition_name);
      std::cout << "INFO [RodStatusMonitor::setupIsMonitor] Use " << ddc_partition_name << " as DDC partition." << std::endl;
    }
    if (ddc_partition_name != partition_name || ddc_is_name != is_name) {    
        m_ddcReceptor = std::make_shared<PixCon::IsReceptor>(this, //QApplication
  										    m_detectorView.get(), //QObject receiver object
										    ddc_is_name, 
										    ( m_ddcPartition.get() ? *m_ddcPartition : *m_ipcPartition),
										    m_calibrationData);
       std::cout << "INFO [RodStatusMonitor::setupIsMonitor] Use " << ddc_is_name << " as DDC IS server in partition." << std::endl;
    }
    else {
       m_ddcReceptor = m_receptor;
    }
    m_detectorView->setupDcsMonitorManager(m_ddcReceptor);

    //receptor starts thread and communicates with main panel
    if (!dt_is_name.empty()) {

      try { 
	m_dtPartition = std::make_unique<IPCPartition>( (dt_partition_name.empty() ? partition_name.c_str() : dt_partition_name.c_str()) );
	m_dtReceptor = std::make_shared<PixCon::IsReceptor>(this, //QApplication
										    m_detectorView.get(), //QObject receiver object
										    dt_is_name, 
										    *m_dtPartition,
										    m_calibrationData);
	std::cout << "INFO [RodStatusMonitor::setupIsMonitor] Started info receiver for IS server " 
		  << dt_is_name.c_str() 
		  << " in partition " << (dt_partition_name.empty() ? partition_name.c_str() : dt_partition_name.c_str()) << "." << std::endl;
      }
      catch(...) {
	std::cerr << "ERROR [RodStatusMonitor::setupIsMonitor] Caught exception while trying to setup the tags monitoring using IS server " 
		  << dt_is_name.c_str() 
		  << " in partition " << (dt_partition_name.empty() ? partition_name.c_str() : dt_partition_name.c_str()) << "." << std::endl;
      }

    }

    // each IsMonitor subscribes to a particular variable
    {
      enum EDAQStateVar {kDAQEnabled, kNDAQStates};
      enum ERodStateVar {kRODAvgOcc, kRODSlinkPercentage, kRODLargeFragmentPercentage, kRODregPartity, kRODrceFrac, kRODeocFrac, kRODhistoryRunning, kRODBusy,kRODQSMode,kRODAllocated, kRODCurAction, kRODAllocPart, kRODStatus, kWDTime, kCMDTime, kCMDLen, /*kRODBCIDerr, kRODLVL1IDerr, kRODTimeOutErr,*/ kRODDataIncorrectErr, /* kRODDataOverflowErr, */ kRODFragmentSize, kNRodStates};
      enum EModStateVar {kNHits, kModBCIDerr, kModLVL1IDerr, kModTimeOutErr, kModPreambleErr, kModTrailBitErr, kModHeadTrailLimErr, kModDataOverflowErr, kModFEEOCErr, kModFEHammingErr, kModFeregparityErr, kModFEhitparityErr, kModMCChitoverflowErr, kModMCCEOEoverflowErr, kModMCCL1IDErr, kModBCIDEoEErr, kModL1IDEoEErr, kModScanErrCount, kModFmtStat, kModBusyHist, kModNOcc, kModLinkUsage, kModOccupancy,kModFMTDesynchL1ID, kModFMTBusyFrac,kModTOTDesync ,kModBOCFrameCount, kModBOCDecodingError, kModBOCFrameError, kModFmtLinknRST,kModnCFG,kModnRODSYNC,kModnMODSYNC,kModnUSER,kNModStates};
      enum ETimStateVar {kLVL1IDcnt, kECRcnt, kRodBusyCnt, kTType, kBCIDoffset, kTriggerDelay, kTimOkChanges, kQpllLockChanges, kTTCrxFineDelay, kTTCrxCoarseDelay, kFFTVCounterValue, kFFTVBusyFraction, kFFTV_B_CounterValue, kFFTV_B_BusyFraction, kNTimStates};
      enum EBocStateVar {kClockOk, kInterlocksOk, kLocalInterlock, kRemoteInterlock, kVPinStatusOk, kTempRX, kTempTX, kPiNC_A, kPiNC_B, kPiNC_C, kPiNC_D, kPiNV_A, kPiNV_B, kPiNV_C, kPiNV_D, kNBocStates};
      enum EDspMonStateVar {kRODsyncEna, kRODsyncMinIdle, kNDspMonStates};

      const char* daq_state_name[kNDAQStates]={
	"DAQEnabled"
      };
    
      const char *rod_state_name[kNRodStates]={
	"AvgOcc",
	"SlinkPercentage",
	"LargeFragmentPercentage",
  "regParityErrorFraction",
  "rceFraction",
  "eocFraction",
  "HistoryRunning",
	"BUSY", //integer from 0->100
	"RODQSMode",
	"AVAILABLE", //0=Allocated ;  1=Not Allocated
	"CURRENT-ACTION",
	"ALLOCATING-PARTITION",
	"STATUS",
        "LAST-WD-CALL",
        "LAST-CMD-START",
        "LAST-CMD-TIME",
	//"BCID_errors",
	//"LVL1ID_errors",
	//"timeout_errors",
	"dataincorrect_errors",
	//"dataoverflow_errors",
	"FragmentSize"
      };
 
      const char *mod_state_name[kNModStates]={
	"NHits",
	"BCID_errors",
	"LVL1ID_errors",
	"timeout_errors",
	"preamble_errors",
	"trailerbit_errors",
	"headtraillimit_errors",
	"dataoverflow_errors",
	"FEEOC_errors",
	"FEHamming_errors",
	"FEregparity_errors",
	"FEhitparity_errors",
	"MCChitoverflow_errors",
	"MCCEoEoverflow_errors",
	"MCCL1ID_errors",
	"MCCBCIDEoE_errors",
	"MCCL1IDEoE_errors",
        "MODERR",
	"FMTSTAT",
	"BUSY_HIST",
	"NOcc",
  "FMTLinkUsage",
  "FMTOccupancy",
  "FMTDesynchL1ID",
  "FMTBusyFrac",
  "TOTDesync",
  "BOCFrameCount",
  "BOCDecodingError",
  "BOCFrameError",
	"nRST",
	"nCFG",
	"nRODSYNC",
	"nMODSYNC",
	"nUSER"
      };

      const char *tim_state_name[kNTimStates]={
	"L1IDCounterValue",
	"ECRCounterValue",
	"RodBusyDuration",
	"TriggerType",
	"TimBcidOffset",
	"TimTriggerDelay",
	"TimOkChanges",
	"QpllLockChanges",
	"TTCrxFineDelay",
	"TTCrxCoarseDelay",
	"FFTVCounterValue",
	"FFTVBusyFraction",
	"FFTV_B_BusyFraction",
	"FFTV_B_CounterValue"
      };

      const char *boc_state_name[kNBocStates]={
	"ClockOk",
	"InterlocksOk",
	"LocalInterlock",
	"RemoteInterlock",
	"VPinStatusOk", 
	"TempRX",
	"TempTX",
	"PiNC_A",
	"PiNC_B",
	"PiNC_C",
	"PiNC_D",
	"PiNV_A",
	"PiNV_B",
	"PiNV_C",
	"PiNV_D"
      };

      const char *dsp_mon_state_name[kNDspMonStates]={
        "RODsyncEna",
        "RODsyncMinIdle"
      };

      std::map<std::string, unsigned int> statusMap;
      std::map<std::string, unsigned int> curActMap;
      std::map<std::string, unsigned int> allocPartMap;
      std::map<std::string, unsigned int> fmtStatMap;
      std::map<std::string, unsigned int> RODQSMap;
      std::map<std::string, unsigned int> RODsyncEnaMap;


      // VarDef for kDAQEnabled
      {
	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createStateVarDef(daq_state_name[kDAQEnabled], PixCon::kModuleValue, 5);
	if (var_def.isStateWithOrWithoutHistory()) {
	  var_def.stateDef().addState(0, 6, "ENABLED");
	  var_def.stateDef().addState(1, 1, "DISBYCFG");
	  var_def.stateDef().addState(2, 8, "ADISTOUT");
	  var_def.stateDef().addState(3, 7, "ADISBUSY");
	  var_def.stateDef().addState(4, 2, "DISBYUSER");
	  var_def.stateDef().addState(5, 3, "DISBYQS");
	  std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << daq_state_name[kDAQEnabled] << " (DAQEnabled) states defined." << std::endl;
	} else {
	  std::cout << "ERROR [RodStatusMonitor::kDAQEnabled] variable " << daq_state_name[kDAQEnabled]  << " already exists but it is not a state variable." << std::endl;
	}
      }


      // VarDef for kRODAllocated
      if (!m_monitorActionServerTags)
       {
 	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createStateVarDef(rod_state_name[kRODAllocated],PixCon::kRodValue,2);
 	if ( var_def.isStateWithOrWithoutHistory() ) {
 	  var_def.stateDef().addState(0, PixCon::DefaultColourCode::kFailure, "Allocated");
 	  var_def.stateDef().addState(1, PixCon::DefaultColourCode::kSuccess, "Not Allocated");
 	  std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << rod_state_name[kRODAllocated] << " (ROD Allocated) states defined." << std::endl;
 	} else {
 	  std::cout << "ERROR [RodStatusMonitor::kRODAllocated] variable " << rod_state_name[kRODAllocated]  << " already exists but it is not a state variable." << std::endl;
 	}
       }


      // VarDef for kRODBusy
      {
	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(rod_state_name[kRODBusy], PixCon::kRodValue, 0., 100.,"%",1000);
	if (var_def.isValueWithOrWithoutHistory()) {
	  PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	  value_def.setMinMax(0.,100.);
	  std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << rod_state_name[kRODBusy] << " (ROD Busy) states defined." << std::endl;
	} else {
	  std::cout << "ERROR [RodStatusMonitor::kRODBusy] variable " << rod_state_name[kRODBusy]  << " already exists but it is not a value variable." << std::endl;
	}
      }


      //Var for kRODQSMode
 {
   PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createStateVarDef(rod_state_name[kRODQSMode],PixCon::kRodValue, 8);
	if (var_def.isStateWithOrWithoutHistory() ) {
	  var_def.stateDef().addState(0, 6, "OFF");
	  var_def.stateDef().addState(1, 2, "IDLE");
	  var_def.stateDef().addState(2, 3, "MON");
	  var_def.stateDef().addState(3, 9, "RST");
	  var_def.stateDef().addState(4, 5, "RST_DIS");
	  var_def.stateDef().addState(5, 11, "RST_CFG_ON");
	  var_def.stateDef().addState(6, 14, "RST_CFG_OFF");
	  var_def.stateDef().addState(6, 14, "RST_CFG_OFF");
	  var_def.stateDef().addState(7, 1, "UNKNOWN");
	  RODQSMap["QS_OFF"]=0;
	  RODQSMap["QS_IDLE"]=1;
	  RODQSMap["QS_MON"]=2;
	  RODQSMap["QS_RST"]=3;
	  RODQSMap["QS_RST_DIS"]=4;
	  RODQSMap["QS_RST_CFG_ON"]=5;
	  RODQSMap["QS_RST_CFG_OFF"]=6;
	  RODQSMap[""]=7;

	  std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << rod_state_name[kRODQSMode] << " RODQSMode  states defined." << std::endl;
	} else {
	  std::cout << "ERROR [RodStatusMonitor::kRODQSMode] variable " << rod_state_name[kRODQSMode]  << " already exists but it is not a state variable." << std::endl;
	}
      }

      // VarDef for kRODAvgOcc
      {
	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(rod_state_name[kRODAvgOcc], PixCon::kRodValue, 0., 0.003,"",1000);
	if (var_def.isValueWithOrWithoutHistory()) {
	  PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	  value_def.setMinMax(0.,0.003);
	  std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << rod_state_name[kRODAvgOcc] << " (ROD Avg Occ) states defined." << std::endl;
	} else {
	  std::cout << "ERROR [RodStatusMonitor::kRODAvgOcc] variable " << rod_state_name[kRODAvgOcc]  << " already exists but it is not a value variable." << std::endl;
	}
      }

      // VarDef for kRODSlinkPercentage
      {
	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(rod_state_name[kRODSlinkPercentage], PixCon::kRodValue, 0., 100.,"",1000);
	if (var_def.isValueWithOrWithoutHistory()) {
	  PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	  value_def.setMinMax(0.,100.);
	  std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << rod_state_name[kRODSlinkPercentage] << " (ROD Slink Occupancy Percentage) states defined." << std::endl;
	} else {
	  std::cout << "ERROR [RodStatusMonitor::kRODSlinkPercentage] variable " << rod_state_name[kRODSlinkPercentage]  << " already exists but it is not a value variable." << std::endl;
	}
      }
      // VarDef for kRODLargeFragmentPercentage
      {
	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(rod_state_name[kRODLargeFragmentPercentage], PixCon::kRodValue, 0., 100.,"",1000);
	if (var_def.isValueWithOrWithoutHistory()) {
	  PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	  value_def.setMinMax(0.,100.);
	  std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << rod_state_name[kRODLargeFragmentPercentage] << " (ROD Large Event Percentage) states defined." << std::endl;
	} else {
	  std::cout << "ERROR [RodStatusMonitor::kRODLargeFragmentPercentage] variable " << rod_state_name[kRODLargeFragmentPercentage]  << " already exists but it is not a value variable." << std::endl;
	}
      }

      {
  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(rod_state_name[kRODregPartity], PixCon::kRodValue, 0., 100.,"",100);
  if (var_def.isValueWithOrWithoutHistory()) {
    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
    value_def.setMinMax(0.,100.);
    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << rod_state_name[kRODregPartity] << " (ROD Global Register Parity Error Event Fraction) states defined." << std::endl;
  } else {
    std::cout << "ERROR [RodStatusMonitor::kRODregPartity] variable " << rod_state_name[kRODregPartity]  << " already exists but it is not a value variable." << std::endl;
  }
      }

      {
  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(rod_state_name[kRODrceFrac], PixCon::kRodValue, 0., 100.,"",100);
  if (var_def.isValueWithOrWithoutHistory()) {
    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
    value_def.setMinMax(0.,100.);
    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << rod_state_name[kRODrceFrac] << " (ROD Row Column Error Event Fraction) states defined." << std::endl;
  } else {
    std::cout << "ERROR [RodStatusMonitor::kRODrceFrac] variable " << rod_state_name[kRODrceFrac]  << " already exists but it is not a value variable." << std::endl;
  }
      }

      {
  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(rod_state_name[kRODeocFrac], PixCon::kRodValue, 0., 100.,"",1000);
  if (var_def.isValueWithOrWithoutHistory()) {
    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
    value_def.setMinMax(0.,100.);
    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << rod_state_name[kRODeocFrac] << " (ROD End of Column Overflow Error Event Fraction) states defined." << std::endl;
  } else {
    std::cout << "ERROR [RodStatusMonitor::kRODeocFrac] variable " << rod_state_name[kRODeocFrac]  << " already exists but it is not a value variable." << std::endl;
  }
      }

  {
    PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createStateVarDef(rod_state_name[kRODhistoryRunning],PixCon::kRodValue,2,false,10);
    if ( var_def.isStateWithOrWithoutHistory() ) {
      var_def.stateDef().addState(0, PixCon::DefaultColourCode::kFailure, "History Stopped");
      var_def.stateDef().addState(1, PixCon::DefaultColourCode::kSuccess, "History Running");
      std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << rod_state_name[kRODhistoryRunning] << " ROD history states defined." << std::endl;
    } else {
      std::cout << "ERROR [RodStatusMonitor::kRODhistoryRunning] variable " << rod_state_name[kRODhistoryRunning]  << " already exists but it is not a state variable." << std::endl;
    }
  }

      if (1 || m_userMode->getUserMode() == PixCon::UserMode::kExpert) {
	// VarDef for kRODStatus
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createStateVarDef(rod_state_name[kRODStatus],PixCon::kRodValue, 3);
	  if ( var_def.isStateWithOrWithoutHistory() ) {
	    var_def.stateDef().addState(0, 0, "UNKNOWN");
	    statusMap["UNKNOWN"] = 0;
	    var_def.stateDef().addState(1, 1, "SUCCESS");
	    statusMap["SUCCESS"] = 1;
	    var_def.stateDef().addState(2, 2, "FAILURE");
	    statusMap["FAILURE"] = 2;
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << rod_state_name[kRODStatus] << " (ROD Status) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kRODStatus] variable " << rod_state_name[kRODStatus]  << " already exists but it is not a state variable." << std::endl;
	  }
	}

	// VarDef for kRODCurAction
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createStateVarDef(rod_state_name[kRODCurAction],PixCon::kRodValue, 17);
	  if ( var_def.isStateWithOrWithoutHistory() ) {
	    // colours are defined in CalibrationData/CalibrationDataStyle.cxx
	    var_def.stateDef().addState(0, 1, "UNKNOWN");
	    curActMap["UNKNOWN"] = 0;
	    var_def.stateDef().addState(1, 2, "startTrigger");
	    curActMap["startTrigger"] = 1;
	    var_def.stateDef().addState(2, 3, "stopTrigger");
	    curActMap["stopTrigger"] = 2;
	    var_def.stateDef().addState(3, 4, "prepareForRun");
	    curActMap["prepareForRun"] = 3;
	    var_def.stateDef().addState(4, 5, "setup");
	    curActMap["setup"] = 4;
	    var_def.stateDef().addState(5, 6, "setupDb");
	    curActMap["setupDb"] = 5;
	    var_def.stateDef().addState(6, 7, "configure");
	    curActMap["configure"] = 6;
	    var_def.stateDef().addState(7, 8, "reset");
	    curActMap["reset"] = 7;
	    var_def.stateDef().addState(8, 9, "resetViset");
	    curActMap["resetViset"] = 8;
	    var_def.stateDef().addState(9, 13 /*2*/, "setActionsConfig");
	    curActMap["setActionsConfig"] = 9;
	    var_def.stateDef().addState(10, 10/*3*/, "loadConfig");
	    curActMap["loadConfig"] = 10;
	    var_def.stateDef().addState(11, 12/*4*/, "sendConfig");
	    curActMap["sendConfig"] = 11;
	    var_def.stateDef().addState(12, 14/*5*/, "saveConfig");
	    curActMap["saveConfig"] = 12;
	    var_def.stateDef().addState(13, 6, "reloadConfig");
	    curActMap["reloadConfig"] = 13;
	    var_def.stateDef().addState(14, 0 /*7*/, "resetMods");
	    curActMap["resetmods"] = 14;
	    var_def.stateDef().addState(15, 8, "execConnectivityTest");
	    curActMap["execConnectivityTest"] = 15;
	    var_def.stateDef().addState(16, 15/*9*/, "scan");
	    curActMap["scan"] = 16;
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << rod_state_name[kRODCurAction] << " (ROD Current Action) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kRODCurAction] variable " << rod_state_name[kRODCurAction]  << " already exists but it is not a state variable." << std::endl;
	  }
	}

	// VarDef for kRODAllocPart
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createStateVarDef(rod_state_name[kRODAllocPart],PixCon::kRodValue, 4);
	  if ( var_def.isStateWithOrWithoutHistory() ) {
	    // colours are defined in CalibrationData/CalibrationDataStyle.cxx
	    var_def.stateDef().addState(0, 1, "NONE");
	    allocPartMap["NONE"] = 0;
	    var_def.stateDef().addState(1, 2, "ATLAS");
	    allocPartMap["ATLAS"] = 1;
	    var_def.stateDef().addState(2, 3, "PIX-DAQSlice");
	    allocPartMap["PIX-DAQSlice"] = 2;
	    var_def.stateDef().addState(3, 4, "IBL-DAQSlice");
	    allocPartMap["IBL-DAQSlice"] = 3;
	    var_def.stateDef().addState(4, 5, "PixelInfr");
	    allocPartMap["PixelInfr"] = 4;
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << rod_state_name[kRODAllocPart] << " (ROD Allocating Parition) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kRODAllocPart] variable " << rod_state_name[kRODAllocPart]  << " already exists but it is not a state variable." << std::endl;
	  }
	}
      } // if (m_userMode->getUserMode() == PixCon::UserMode::kExpert)

      // VarDef for kWDTime
      {
	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(rod_state_name[kWDTime], PixCon::kRodValue, time(NULL)-200, time(NULL)+200,"s",1000); 
	if (var_def.isValueWithOrWithoutHistory()) {
	  PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	  value_def.setMinMax(time(NULL)-200,time(NULL)+200);
	  std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << rod_state_name[kWDTime] << " (WD Time) states defined." << std::endl;
	} else {
	  std::cout << "ERROR [RodStatusMonitor::kWDTime] variable " << rod_state_name[kWDTime]  << " already exists but it is not a value variable." << std::endl;
	}
      }

      // VarDef for kCMDTime
      {
	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(rod_state_name[kCMDTime], PixCon::kRodValue, time(NULL)-900, time(NULL)+100,"s",100);
	if (var_def.isValueWithOrWithoutHistory()) {
	  PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	  value_def.setMinMax(time(NULL)-900, time(NULL)+100.);
	  std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << rod_state_name[kCMDTime] << " (CMD Time) states defined." << std::endl;
	} else {
	  std::cout << "ERROR [RodStatusMonitor::kCMDTime] variable " << rod_state_name[kCMDTime]  << " already exists but it is not a value variable." << std::endl;
	}
      }

      // VarDef for kCMDLen
      {
	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(rod_state_name[kCMDLen], PixCon::kRodValue, 0., 600.,"",10);	
	if (var_def.isValueWithOrWithoutHistory()) {
	  PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	  value_def.setMinMax(0.,600.);
	  std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << rod_state_name[kCMDLen] << " (CMD Len) states defined." << std::endl;
	} else {
	  std::cout << "ERROR [RodStatusMonitor::kCMDLen] variable " << rod_state_name[kCMDLen]  << " already exists but it is not a value variable." << std::endl;
	}
      }

      /*
      // VarDef for kRODBCIDerr
      {
	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(rod_state_name[kRODBCIDerr], PixCon::kRodValue, 0., 100.);	
	if (var_def.isValueWithOrWithoutHistory()) {
	  PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	  value_def.setMinMax(0.,100.);
	  std::cout << "INFO [RodStatusMonitor::setupIsMonitor], " << rod_state_name[kRODBCIDerr] << " (ROD BCIDerr) states defined." << std::endl;
	} else {
	  std::cout << "ERROR [RodStatusMonitor::kRODBCIDerr] variable " << rod_state_name[kRODBCIDerr]  << " already exists but it is not a value variable." << std::endl;
	}
      }

      // VarDef for kRODLVL1IDErr
      {
	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(rod_state_name[kRODLVL1IDerr], PixCon::kRodValue, 0., 100.);	
	if (var_def.isValueWithOrWithoutHistory()) {
	  PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	  value_def.setMinMax(0.,100.);
	  std::cout << "INFO [RodStatusMonitor::setupIsMonitor], " << rod_state_name[kRODLVL1IDerr] << " (ROD LVL1IDerr) states defined." << std::endl;
	} else {
	  std::cout << "ERROR [RodStatusMonitor::kRODLVL1IDerr] variable " << rod_state_name[kRODLVL1IDerr]  << " already exists but it is not a value variable." << std::endl;
	}
      }

      // VarDef for kRODTimeOutErr
      {
	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(rod_state_name[kRODTimeOutErr], PixCon::kRodValue, 0., 100.);	
	if (var_def.isValueWithOrWithoutHistory()) {
	  PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	  value_def.setMinMax(0.,100.);
	  std::cout << "INFO [RodStatusMonitor::setupIsMonitor], " << rod_state_name[kRODTimeOutErr] << " (ROD TimeOut Err) states defined." << std::endl;
	} else {
	  std::cout << "ERROR [RodStatusMonitor::kRODTimeOutErr] variable " << rod_state_name[kRODTimeOutErr]  << " already exists but it is not a value variable." << std::endl;
	}
      }


      // VarDef for kRODDataOverflowErr
      {
	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(rod_state_name[kRODDataOverflowErr], PixCon::kRodValue, 0., 100.);	
	if (var_def.isValueWithOrWithoutHistory()) {
	  PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	  value_def.setMinMax(0.,100.);
	  std::cout << "INFO [RodStatusMonitor::setupIsMonitor], " << rod_state_name[kRODDataOverflowErr] << " (ROD DataOverflow) states defined." << std::endl;
	} else {
	  std::cout << "ERROR [RodStatusMonitor::kRODDataOverflowErr] variable " << rod_state_name[kRODDataOverflowErr]  << " already exists but it is not a value variable." << std::endl;
	}
      }
*/
      if (m_userMode->getUserMode() == PixCon::UserMode::kExpert) {
	// VarDef for kRODDataIncorrectErr
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(rod_state_name[kRODDataIncorrectErr], PixCon::kRodValue, 0., 100.,"",1000);	
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(0.,100.);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << rod_state_name[kRODDataIncorrectErr] << " (ROD DataIncorrect) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::setupIsMonitor] variable kRODDataIncorrectErr(" << rod_state_name[kRODDataIncorrectErr]  << ") already exists but it is not a value variable." << std::endl;
	  }
	}
	

	// VarDef for kRODFragmentSize
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(rod_state_name[kRODFragmentSize], PixCon::kRodValue, 0., 100.,"",100);
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(0.,100.);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << rod_state_name[kRODFragmentSize] << " (ROD Fragment Size) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kRODFragmentSize] variable " << rod_state_name[kRODFragmentSize]  << " already exists but it is not a value variable." << std::endl;
	  }
	}

	// VarDef for kRODsyncEna
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createStateVarDef(dsp_mon_state_name[kRODsyncEna], PixCon::kRodValue, 2);
	  if (var_def.isStateWithOrWithoutHistory()) {
	    var_def.stateDef().addState(0, 0, "OFF");
	    RODsyncEnaMap["OFF"] = 0;
	    var_def.stateDef().addState(1, 1, "ON");
	    RODsyncEnaMap["ON"] = 1;
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << dsp_mon_state_name[kRODsyncEna] << " (ROD sync ena.) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kRODsyncEna] variable " << dsp_mon_state_name[kRODsyncEna]  << " already exists but it is not a state variable." << std::endl;
	  }
	}

	// VarDef for kRODsyncEna
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(dsp_mon_state_name[kRODsyncMinIdle], PixCon::kRodValue, 0., 100.);
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(0.,100.);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << dsp_mon_state_name[kRODsyncMinIdle] << " (ROD sync min. idle) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kRODsyncMinIdle] variable " << dsp_mon_state_name[kRODsyncMinIdle]  << " already exists but it is not a state variable." << std::endl;
	  }
	}

	// ----------------Modules----------------------------------
    
	// VarDef for kNHits
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kNHits], PixCon::kModuleValue, 0., 1.,"",100);	
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(0.,1.);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kNHits] << " Mod (NHits) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kNHits] variable " << mod_state_name[kNHits]  << " already exists but it is not a value variable." << std::endl;
	  }
	}
      } // if (m_userMode->getUserMode() == PixCon::UserMode::kExpert)
    
      // VarDef for kModBCIDerr
      {
      	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModBCIDerr], PixCon::kModuleValue, 0., 100.,"",100);	
	if (var_def.isValueWithOrWithoutHistory()) {
	  PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	  value_def.setMinMax(0.,100.);
	  std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModBCIDerr] << " Mod (BCID Err) states defined." << std::endl;
	} else {
	  std::cout << "ERROR [RodStatusMonitor::kModBCIDerr] variable " << mod_state_name[kModBCIDerr]  << " already exists but it is not a value variable." << std::endl;
	}
      }

      // VarDef for kModLVL1IDerr
      {
      	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModLVL1IDerr], PixCon::kModuleValue, 0., 100.,"",100);	
	if (var_def.isValueWithOrWithoutHistory()) {
	  PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	  value_def.setMinMax(0.,100.);
	  std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModLVL1IDerr] << " Mod (LVL1ID Err) states defined." << std::endl;
	} else {
	  std::cout << "ERROR [RodStatusMonitor::kModLVL1IDerr] variable " << mod_state_name[kModLVL1IDerr]  << " already exists but it is not a value variable." << std::endl;
	}
      }

      if (m_userMode->getUserMode() == PixCon::UserMode::kExpert) {
	// VarDef for kModTimeOutErr
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModTimeOutErr], PixCon::kModuleValue, 0., 100.,"",100);	
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(0.,100.);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModTimeOutErr] << " Mod (BCID Err) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kModTimeOutErr] variable " << mod_state_name[kModTimeOutErr]  << " already exists but it is not a value variable." << std::endl;
	  }
	}

	// VarDef for kModPreambleErr
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModPreambleErr], PixCon::kModuleValue, 0., 100.,"",100);
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(0.,100.);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModPreambleErr] << " Mod (Preamble Err) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kModPreambleErr] variable " << mod_state_name[kModPreambleErr]  << " already exists but it is not a value variable." << std::endl;
	  }
	}
	
	// VarDef for kModTrailBitErr
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModTrailBitErr], PixCon::kModuleValue, 0., 100.);	
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(0.,100.);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModTrailBitErr] << " Mod (Trail bit Err) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kModTrailBitErr] variable " << mod_state_name[kModTrailBitErr]  << " already exists but it is not a value variable." << std::endl;
	  }
	}
	
	// VarDef for kModHeadTrailLimErr
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModHeadTrailLimErr], PixCon::kModuleValue, 0., 100.,"",100);	
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(0.,100.);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModHeadTrailLimErr] << " Mod (Head Trail Err) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kModHeadTrailLimErr] variable " << mod_state_name[kModHeadTrailLimErr]  << " already exists but it is not a value variable." << std::endl;
	  }
	}
	
	// VarDef for kModDataOverflowErr
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModDataOverflowErr], PixCon::kModuleValue, 0., 100.,"",100);
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(0.,100.);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModDataOverflowErr] << " Mod (Data Overflow Err) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kModDataOverflowErr] variable " << mod_state_name[kModDataOverflowErr]  << " already exists but it is not a value variable." << std::endl;
	  }
	}

	// VarDef for kModFEerr flags
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModFEEOCErr], PixCon::kModuleValue, 0., 100.,"",100);	
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(0.,100.);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModFEEOCErr] << " Mod (FE Err) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kModFEEOCErr] variable " << mod_state_name[kModFEEOCErr]  << " already exists but it is not a value variable." << std::endl;
	  }
	}
      } // if (m_userMode->getUserMode() == PixCon::UserMode::kExpert)

      {
      	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModFEHammingErr], PixCon::kModuleValue, 0., 100.,"",100);	
	if (var_def.isValueWithOrWithoutHistory()) {
	  PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	  value_def.setMinMax(0.,100.);
	  std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModFEHammingErr] << " Mod (FE Err) states defined." << std::endl;
	} else {
	  std::cout << "ERROR [RodStatusMonitor::kModFEHammingErr] variable " << mod_state_name[kModFEHammingErr]  << " already exists but it is not a value variable." << std::endl;
	}
      }
      {
      	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModFeregparityErr], PixCon::kModuleValue, 0., 100.,"",100);	
	if (var_def.isValueWithOrWithoutHistory()) {
	  PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	  value_def.setMinMax(0.,100.);
	  std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModFeregparityErr] << " Mod (FE Err) states defined." << std::endl;
	} else {
	  std::cout << "ERROR [RodStatusMonitor::kModFeregparityErr] variable " << mod_state_name[kModFeregparityErr]  << " already exists but it is not a value variable." << std::endl;
	}
      }
      {
      	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModFEhitparityErr], PixCon::kModuleValue, 0., 100.,"",100);	
	if (var_def.isValueWithOrWithoutHistory()) {
	  PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	  value_def.setMinMax(0.,100.);
	  std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModFEhitparityErr] << " Mod (FE Err) states defined." << std::endl;
	} else {
	  std::cout << "ERROR [RodStatusMonitor::kModFEhitparityErr] variable " << mod_state_name[kModFEhitparityErr]  << " already exists but it is not a value variable." << std::endl;
	}
      }
      {
      	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModMCChitoverflowErr], PixCon::kModuleValue, 0., 100.,"",100);	
	if (var_def.isValueWithOrWithoutHistory()) {
	  PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	  value_def.setMinMax(0.,100.);
	  std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModMCChitoverflowErr] << " Mod (FE Err) states defined." << std::endl;
	} else {
	  std::cout << "ERROR [RodStatusMonitor::kModMCChitoverflowErr] variable " << mod_state_name[kModMCChitoverflowErr]  << " already exists but it is not a value variable." << std::endl;
	}
      }
      {
      	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModMCCEOEoverflowErr], PixCon::kModuleValue, 0., 100.,"",100);	
	if (var_def.isValueWithOrWithoutHistory()) {
	  PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	  value_def.setMinMax(0.,100.);
	  std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModMCCEOEoverflowErr] << " Mod (FE Err) states defined." << std::endl;
	} else {
	  std::cout << "ERROR [RodStatusMonitor::kModMCCEOEoverflowErr] variable " << mod_state_name[kModMCCEOEoverflowErr]  << " already exists but it is not a value variable." << std::endl;
	}
      }

      if (m_userMode->getUserMode() == PixCon::UserMode::kExpert) {
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModMCCL1IDErr], PixCon::kModuleValue, 0., 100.,"",100);	
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(0.,100.);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModMCCL1IDErr] << " Mod (FE Err) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kModMCCL1IDErr] variable " << mod_state_name[kModMCCL1IDErr]  << " already exists but it is not a value variable." << std::endl;
	  }
	}
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModBCIDEoEErr], PixCon::kModuleValue, 0., 100.,"",100);	
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(0.,100.);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModBCIDEoEErr] << " Mod (FE Err) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kModBCIDEoEErr] variable " << mod_state_name[kModBCIDEoEErr]  << " already exists but it is not a value variable." << std::endl;
	  }
	}
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModL1IDEoEErr], PixCon::kModuleValue, 0., 100.,"",100);	
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(0.,100.);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModL1IDEoEErr] << " Mod (FE Err) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kModL1IDEoEErr] variable " << mod_state_name[kModL1IDEoEErr]  << " already exists but it is not a value variable." << std::endl;
	  }
	}
	

	// VarDef for kModScanErrCount
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModScanErrCount], PixCon::kModuleValue, 0., 60.,"",100);	
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(0.,60.);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModScanErrCount] << " Mod (Scan Error Count) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kModScanErrCount] variable " << mod_state_name[kModScanErrCount]  << " already exists but it is not a value variable." << std::endl;
	  }
	}
      } // if (m_userMode->getUserMode() == PixCon::UserMode::kExpert)

      // VarDef for kModFmtStat
      {
	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createStateVarDef(mod_state_name[kModFmtStat],PixCon::kModuleValue,7,false,100);
	if (var_def.isStateWithOrWithoutHistory() ) {
	  var_def.stateDef().addState(0, 1, "DISABLED");
	  var_def.stateDef().addState(1, 6, "ENABLED");
	  var_def.stateDef().addState(2, 7, "BUSY");
	  var_def.stateDef().addState(3, 8, "TIMEOUT");
	  var_def.stateDef().addState(4, 9, "H/T LIMIT");
	  var_def.stateDef().addState(5, 2, "OVERFLOW");
	  var_def.stateDef().addState(6, 3, "QSDisabled");
	 fmtStatMap["D"] = 0;
	  fmtStatMap["QSD"] = 6;
          fmtStatMap["E    "] = 1;
          fmtStatMap["ET   "] = 3;
          fmtStatMap["ETO  "] = 3;
          fmtStatMap["ET H "] = 3;
          fmtStatMap["ETOH "] = 3;
          fmtStatMap["E O  "] = 1;
          fmtStatMap["E  H "] = 4;
          fmtStatMap["E   B"] = 2;
          fmtStatMap["ET  B"] = 2;
          fmtStatMap["E O B"] = 2;
          fmtStatMap["E  HB"] = 2;
          fmtStatMap["ET HB"] = 2;
          fmtStatMap["ETO B"] = 2;
          fmtStatMap["E OHB"] = 2;
          fmtStatMap["ETOHB"] = 2;
	  std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModFmtStat] << " Mod (Fmt Stat) states defined." << std::endl;
	} else {
	  std::cout << "ERROR [RodStatusMonitor::kModFmtStat] variable " << mod_state_name[kModFmtStat]  << " already exists but it is not a state variable." << std::endl;
	}
      }

      // VarDef for kModBusyHist
      {
	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModBusyHist], PixCon::kModuleValue, 0., 10.,"",2000);
	if (var_def.isValueWithOrWithoutHistory()) {
	  PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	  value_def.setMinMax(0,2000);
	  std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModBusyHist] << " Mod (Scan Error Count) states defined." << std::endl;
	} else {
	  std::cout << "ERROR [RodStatusMonitor::kModBusyHist] variable " << mod_state_name[kModBusyHist]  << " already exists but it is not a value variable." << std::endl;
	}
      }


      // VarDef for kModNOcc
      {
      	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModNOcc], PixCon::kModuleValue, 0., 10.,"",100);	
	if (var_def.isValueWithOrWithoutHistory()) {
	  PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	  value_def.setMinMax(0.,10.);
	  std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModNOcc] << " Mod (Scan Error Count) states defined." << std::endl;
	} else {
	  std::cout << "ERROR [RodStatusMonitor::kModNOcc] variable " << mod_state_name[kModNOcc]  << " already exists but it is not a value variable." << std::endl;
	}
      }

      // VarDef for kModLinkUsage
      {
        PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModLinkUsage], PixCon::kModuleValue, 0., 100.,"",100);  
  if (var_def.isValueWithOrWithoutHistory()) {
    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
    value_def.setMinMax(0.,100.);
    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModLinkUsage] << " Mod (FMT based Link Usage) states defined." << std::endl;
  } else {
    std::cout << "ERROR [RodStatusMonitor::kModLinkUsage] variable " << mod_state_name[kModLinkUsage]  << " already exists but it is not a value variable." << std::endl;
  }
      }
      // VarDef for kModOccupancy
      {
        PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModOccupancy], PixCon::kModuleValue, 0., 0.003,"",1000);
  if (var_def.isValueWithOrWithoutHistory()) {
    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
    value_def.setMinMax(0.,0.003);
    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModOccupancy] << " Mod (FMT based Occupancy) states defined." << std::endl;
  } else {
    std::cout << "ERROR [RodStatusMonitor::kModOccupancy] variable " << mod_state_name[kModOccupancy]  << " already exists but it is not a value variable." << std::endl;
  }
      }
// VarDef for kModFMTDesyncL1ID
      {
        PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModFMTDesynchL1ID], PixCon::kModuleValue, 0., 4294967296,"",1000);
  if (var_def.isValueWithOrWithoutHistory()) {
    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
    value_def.setMinMax(0., 1000);
    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModFMTDesynchL1ID] << " Mod (FMT desynch L1ID) states defined." << std::endl;
  } else {
    std::cout << "ERROR [RodStatusMonitor::kModFMTDesynchL1ID] variable " << mod_state_name[kModFMTDesynchL1ID]  << " already exists but it is not a value variable." << std::endl;
  }
      }
      // VarDef for kModFMTBusyFrac
      {
        PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModFMTBusyFrac], PixCon::kModuleValue, 0., 1.,"",1000);
  if (var_def.isValueWithOrWithoutHistory()) {
    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
    value_def.setMinMax(0., 1.);
    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModFMTBusyFrac] << " Mod (FMT Busy) states defined." << std::endl;
  } else {
    std::cout << "ERROR [RodStatusMonitor::kModFMTBusyFrac] variable " << mod_state_name[kModFMTBusyFrac]  << " already exists but it is not a value variable." << std::endl;
  }
      }
      // VarDef for // VarDef for kModTOTDesync
      {
        PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModTOTDesync], PixCon::kModuleValue, 0., 4294967296,"",1000);
  if (var_def.isValueWithOrWithoutHistory()) {
    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
    value_def.setMinMax(0., 4294967296);
    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModTOTDesync] << " Mod (FMT total desync) states defined." << std::endl;
  } else {
    std::cout << "ERROR [RodStatusMonitor::kModTOTDesync] variable " << mod_state_name[kModTOTDesync]  << " already exists but it is not a value variable." << std::endl;
  }
      }
      // VarDef for // VarDef for kModBOCFrameCount
      {
        PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModBOCFrameCount], PixCon::kModuleValue, 0., 4294967296,"",1000);
  if (var_def.isValueWithOrWithoutHistory()) {
    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
    value_def.setMinMax(0., 4294967296);
    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModBOCFrameCount] << " Mod BOC Frame Count states defined." << std::endl;
  } else {
    std::cout << "ERROR [RodStatusMonitor::kModBOCFrameCount] variable " << mod_state_name[kModBOCFrameCount]  << " already exists but it is not a value variable." << std::endl;
  }
      }
      // VarDef for BOCDecodingError
      {
        PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModBOCDecodingError], PixCon::kModuleValue, 0., 4294967296,"",1000);
  if (var_def.isValueWithOrWithoutHistory()) {
    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
    value_def.setMinMax(0., 4294967296);
    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModBOCDecodingError] << " Mod BOC Decoding Error states defined." << std::endl;
  } else {
    std::cout << "ERROR [RodStatusMonitor::kModBOCDecodingError] variable " << mod_state_name[kModBOCDecodingError]  << " already exists but it is not a value variable." << std::endl;
  }
      }
      // VarDef for BOCFrameError
      {
        PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModBOCFrameError], PixCon::kModuleValue, 0., 4294967296,"",1000);
  if (var_def.isValueWithOrWithoutHistory()) {
    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
    value_def.setMinMax(0., 4294967296);
    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModBOCFrameError] << " Mod BOC Frame Error states defined." << std::endl;
  } else {
    std::cout << "ERROR [RodStatusMonitor::kModBOCFrameError] variable " << mod_state_name[kModBOCFrameError]  << " already exists but it is not a value variable." << std::endl;
  }
      }
      // VarDef for kModFmtLinknRST
      {
	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModFmtLinknRST], PixCon::kModuleValue, 0, 100,"%",100);
	if (var_def.isValueWithOrWithoutHistory()) {
	  PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	  value_def.setMinMax(0,15);
	  std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModFmtLinknRST] << " Mod Fmt Link nRST states defined." << std::endl;
	} else {
	  std::cout << "ERROR [RodStatusMonitor::kModFmtLinknRST] variable " << mod_state_name[kModFmtLinknRST]  << " already exists but it is not a value variable." << std::endl;
	}
      }


      // VarDef for kModnCFG
      {
	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModnCFG], PixCon::kModuleValue, 0, 100,"%",100);
	if (var_def.isValueWithOrWithoutHistory()) {
	  PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	  value_def.setMinMax(0,15);
	  std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModnCFG] << " Mod nCFG states defined." << std::endl;
	} else {
	  std::cout << "ERROR [RodStatusMonitor::kModnCFG] variable " << mod_state_name[kModnCFG]  << " already exists but it is not a value variable." << std::endl;
	}
      }


      // VarDef for kModnRODSYNC
      {
	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModnRODSYNC], PixCon::kModuleValue, 0, 100,"%",100);
	if (var_def.isValueWithOrWithoutHistory()) {
	  PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	  value_def.setMinMax(0,15);
	  std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModnRODSYNC] << " Mod nRODSYNC states defined." << std::endl;
	} else {
	  std::cout << "ERROR [RodStatusMonitor::kModnRODSYNC] variable " << mod_state_name[kModnRODSYNC]  << " already exists but it is not a value variable." << std::endl;
	}
      }


      // VarDef for kModnMODSYNC
      {
	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModnMODSYNC], PixCon::kModuleValue, 0, 100,"%",100);
	if (var_def.isValueWithOrWithoutHistory()) {
	  PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	  value_def.setMinMax(0,15);
	  std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModnMODSYNC] << " Mod nMODSYNC states defined." << std::endl;
	} else {
	  std::cout << "ERROR [RodStatusMonitor::kModnMODSYNC] variable " << mod_state_name[kModnMODSYNC]  << " already exists but it is not a value variable." << std::endl;
	}
      }



      // VarDef for kModnUSER
      {
	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(mod_state_name[kModnUSER], PixCon::kModuleValue, 0, 100,"%",100);
	if (var_def.isValueWithOrWithoutHistory()) {
	  PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	  value_def.setMinMax(0,15);
	  std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << mod_state_name[kModnUSER] << " Mod nUSER states defined." << std::endl;
	} else {
	  std::cout << "ERROR [RodStatusMonitor::kModnUSER] variable " << mod_state_name[kModnUSER]  << " already exists but it is not a value variable." << std::endl;
	}
      }


      if (m_userMode->getUserMode() == PixCon::UserMode::kExpert) {
	// --------------------------TIM----------------------------------------------
	// VarDef for kLVL1IDcnt
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(tim_state_name[kLVL1IDcnt], PixCon::kTimValue, -1., 1000.);	
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(-1.,1000.);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << tim_state_name[kLVL1IDcnt] << " Tim (LVL1ID Count) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kLVL1IDcnt] variable " << tim_state_name[kLVL1IDcnt]  << " already exists but it is not a value variable." << std::endl;
	  }
	}
	
	// VarDef for kECRcnt
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(tim_state_name[kECRcnt], PixCon::kTimValue, 0., 100.,"",1000);	
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(0.,1000.);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << tim_state_name[kECRcnt] << " Tim (ECR Count) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kECRcnt] variable " << tim_state_name[kECRcnt]  << " already exists but it is not a value variable." << std::endl;
	  }
	}
      } // if (m_userMode->getUserMode() == PixCon::UserMode::kExpert)

      // VarDef for kRodBusyCnt
      {
      	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(tim_state_name[kRodBusyCnt], PixCon::kTimValue, 0., 1000.,"",1000);	
	if (var_def.isValueWithOrWithoutHistory()) {
	  PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	  value_def.setMinMax(0.,1000.);
	  std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << tim_state_name[kRodBusyCnt] << " Tim (RodBusy Count) states defined." << std::endl;
	} else {
	  std::cout << "ERROR [RodStatusMonitor::kRodBusyCnt] variable " << tim_state_name[kRodBusyCnt]  << " already exists but it is not a value variable." << std::endl;
	}
      }

      if (m_userMode->getUserMode() == PixCon::UserMode::kExpert) {
	// VarDef for kTType
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(tim_state_name[kTType], PixCon::kTimValue, 0., 255.,"",100);	
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(0.,255.);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << tim_state_name[kTType] << " Tim (TType) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kTType] variable " << tim_state_name[kTType]  << " already exists but it is not a value variable." << std::endl;
	  }
	}
	
	// VarDef for kBCIDoffset
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(tim_state_name[kBCIDoffset], PixCon::kTimValue, 0., 255.,"",100);	
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(0.,255.);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << tim_state_name[kBCIDoffset] << " Tim (BCID offset) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kBCIDoffset] variable " << tim_state_name[kBCIDoffset]  << " already exists but it is not a value variable." << std::endl;
	  }
	}

	// VarDef for kTriggerDelay
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(tim_state_name[kTriggerDelay], PixCon::kTimValue, 0., 255.,"",100);	
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(0.,255.);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << tim_state_name[kTriggerDelay] << " Tim (Trigger Delay) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kTriggerDelay] variable " << tim_state_name[kTriggerDelay]  << " already exists but it is not a value variable." << std::endl;
	  }
	}

	// VarDef for kTimOkChanges
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(tim_state_name[kTimOkChanges], PixCon::kTimValue, 0., 50.);	
	  if (var_def.valueType() == PixCon::kValue) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(0.,50.);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << tim_state_name[kTimOkChanges] << " Tim (TimOk changes) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kTimOkChanges] variable " << tim_state_name[kTimOkChanges]  << " already exists but it is not a value variable." << std::endl;
	  }
	}
      } // if (m_userMode->getUserMode() == PixCon::UserMode::kExpert)

      // VarDef for kQpllLockChanges
      {
      	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(tim_state_name[kQpllLockChanges], PixCon::kTimValue, 0., 50.);	
	if (var_def.valueType() == PixCon::kValue) {
	  PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	  value_def.setMinMax(0.,50.);
	  std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << tim_state_name[kQpllLockChanges] << " Tim (QpllLock changes) states defined." << std::endl;
	} else {
	  std::cout << "ERROR [RodStatusMonitor::kQpllLockChanges] variable " << tim_state_name[kQpllLockChanges]  << " already exists but it is not a value variable." << std::endl;
	}
      }

      if (m_userMode->getUserMode() == PixCon::UserMode::kExpert) {
	// VarDef for kTTCrxFineDelay
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(tim_state_name[kTTCrxFineDelay], PixCon::kTimValue, 0., 25.,"",100);	
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(0.,25.);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << tim_state_name[kTTCrxFineDelay] << " Tim (TTCrx fine delay) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kTTCrxFineDelay] variable " << tim_state_name[kTTCrxFineDelay]  << " already exists but it is not a value variable." << std::endl;
	  }
	}
	
	// VarDef for kTTCrxCoarseDelay
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(tim_state_name[kTTCrxCoarseDelay], PixCon::kTimValue, 0., 16.);	
	  if (var_def.valueType() == PixCon::kValue) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(0.,25.);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << tim_state_name[kTTCrxCoarseDelay] << " Tim (TTCrx fine delay) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kTTCrxCoarseDelay] variable " << tim_state_name[kTTCrxCoarseDelay]  << " already exists but it is not a value variable." << std::endl;
	  }
	}

	// VarDef for kFFTVCounterValue
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(tim_state_name[kFFTVCounterValue], PixCon::kTimValue, 0., 100., "", 100);	
	  if (var_def.valueType() == PixCon::kValue) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(0.,100.);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << tim_state_name[kFFTVCounterValue] << " Tim fixed frequency protection counter value states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kFFTVCounterValue] variable " << tim_state_name[kFFTVCounterValue]  << " already exists but it is not a value variable." << std::endl;
	  }
	}

	// VarDef for kFFTVBusyFraction
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(tim_state_name[kFFTVBusyFraction], PixCon::kTimValue, 0., 1., "", 100);	
	  if (var_def.valueType() == PixCon::kValue) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(0.,1.);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << tim_state_name[kFFTVBusyFraction] << " Tim fixed frequency protection busy fraction states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::FFTVBusyFraction] variable " << tim_state_name[kFFTVBusyFraction]  << " already exists but it is not a value variable." << std::endl;
	  }
	}

  // VarDef for kFFTV_B_CounterValue
  {
  	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(tim_state_name[kFFTV_B_CounterValue], PixCon::kTimValue, 0., 100., "", 100); 
  	if (var_def.valueType() == PixCon::kValue) 
  	{
	   PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	   value_def.setMinMax( 0., 100. );
           std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << tim_state_name[kFFTV_B_CounterValue] << " Tim fixed frequency protection counter value states defined." << std::endl;
	} 
        else
        {
           std::cout << "ERROR [RodStatusMonitor::kFFTV_B_CounterValue] variable " << tim_state_name[kFFTV_B_CounterValue]  << " already exists but it is not a value variable." << std::endl;
 	}
  }
  // VarDef for kFFTV_B_BusyFraction
  {
  	PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(tim_state_name[kFFTV_B_BusyFraction], PixCon::kTimValue, 0., 1., "", 100); 
  	if (var_def.valueType() == PixCon::kValue) 
  	{
    	   PixCon::ValueVarDef_t &value_def = var_def.valueDef();
  	   value_def.setMinMax( 0., 1. );
  	   std::cout << "INFO [RodStatusMonitor::setupIsMonitor] " << tim_state_name[kFFTV_B_BusyFraction] << " Tim fixed frequency protection busy fraction states defined." << std::endl;
	} 
  	else 
 	{
  	   std::cout << "ERROR [RodStatusMonitor::FFTV_B_BusyFraction] variable " << tim_state_name[kFFTV_B_BusyFraction]  << " already exists but it is not a value variable." << std::endl;
 	}
  }	
	// --------------------------BOC----------------------------------------------
	// VarDef for kClockOk
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createStateVarDef(boc_state_name[kClockOk],PixCon::kRodValue,2,false,50);
	  if ( var_def.isStateWithOrWithoutHistory() ) {
	    var_def.stateDef().addState(0, PixCon::DefaultColourCode::kFailure, "Clock not Ok");
	    var_def.stateDef().addState(1, PixCon::DefaultColourCode::kSuccess, "Clock Ok");
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor], " << boc_state_name[kClockOk] << " Boc (Clock Ok) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kClockOk] variable " << boc_state_name[kClockOk]  << " already exists but it is not a state variable." << std::endl;
	  }
	}
	
	// VarDef for kInterlocksOk
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createStateVarDef(boc_state_name[kInterlocksOk],PixCon::kRodValue,2,false,50);
	  if ( var_def.isStateWithOrWithoutHistory() ) {
	    var_def.stateDef().addState(0, PixCon::DefaultColourCode::kFailure, "Interlocks not Ok");
	    var_def.stateDef().addState(1, PixCon::DefaultColourCode::kSuccess, "Interlocks Ok");
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor], " << boc_state_name[kInterlocksOk] << " Boc (Interlocks Ok) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kInterlocksOk] variable " << boc_state_name[kInterlocksOk]  << " already exists but it is not a state variable." << std::endl;
	  }
	}
	
	// VarDef for kLocalInterlock
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createStateVarDef(boc_state_name[kLocalInterlock],PixCon::kRodValue,2,false,50);
	  if ( var_def.isStateWithOrWithoutHistory() ) {
	    var_def.stateDef().addState(0, PixCon::DefaultColourCode::kFailure, "Local Interlock not Ok");
	    var_def.stateDef().addState(1, PixCon::DefaultColourCode::kSuccess, "Local Interlock Ok");
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor], " << boc_state_name[kLocalInterlock] << " Boc (Local Interlock) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kLocalInterlock] variable " << boc_state_name[kLocalInterlock]  << " already exists but it is not a state variable." << std::endl;
	  }
	}
	
	// VarDef for kRemoteInterlock
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createStateVarDef(boc_state_name[kRemoteInterlock],PixCon::kRodValue,2,false,50);
	  if (var_def.isStateWithOrWithoutHistory() ) {
	    var_def.stateDef().addState(0, PixCon::DefaultColourCode::kFailure, "Remote Interlock not Ok");
	    var_def.stateDef().addState(1, PixCon::DefaultColourCode::kSuccess, "Remote Interlock Ok");
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor], " << boc_state_name[kRemoteInterlock] << " Boc (Remote Interlock Ok) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kRemoteInterlock] variable " << boc_state_name[kRemoteInterlock]  << " already exists but it is not a state variable." << std::endl;
	  }
	}
	
	// VarDef for kVPinStatusOk
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createStateVarDef(boc_state_name[kVPinStatusOk],PixCon::kRodValue,2,false,50);
	  if ( var_def.isStateWithOrWithoutHistory() ) {
	    var_def.stateDef().addState(0, PixCon::DefaultColourCode::kFailure, "VPin Status not Ok");
	    var_def.stateDef().addState(1, PixCon::DefaultColourCode::kSuccess, "VPin Status Ok");
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor], " << boc_state_name[kVPinStatusOk] << " Boc (VPin Status Ok) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kVPinStatusOk] variable " << boc_state_name[kVPinStatusOk]  << " already exists but it is not a state variable." << std::endl;
	  }
	}
	
	// VarDef for kTempRX
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(boc_state_name[kTempRX], PixCon::kRodValue, -1., 50.,"",50);	
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(-1.,50.);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor], " << boc_state_name[kTempRX] << " Boc (TempRX) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kTempRX] variable " << boc_state_name[kTempRX]  << " already exists but it is not a value variable." << std::endl;
	  }
	}
	
	// VarDef for kTempTX
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(boc_state_name[kTempTX], PixCon::kRodValue, -1., 50.,"",50);	
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(-1.,50.);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor], " << boc_state_name[kTempTX] << " Boc (TempTX) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kTempTX] variable " << boc_state_name[kTempTX]  << " already exists but it is not a value variable." << std::endl;
	  }
	}
	
	// VarDef for kPiNC_A
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(boc_state_name[kPiNC_A], PixCon::kRodValue, -1., 1.5,"",50);	
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(-1.,1.5);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor], " << boc_state_name[kPiNC_A] << " Boc (PiNC_A) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kPiNC_A] variable " << boc_state_name[kPiNC_A]  << " already exists but it is not a value variable." << std::endl;
	  }
	}
	
	// VarDef for kPiNC_B
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(boc_state_name[kPiNC_B], PixCon::kRodValue, -1., 1.5,"",50);	
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(-1.,1.5);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor], " << boc_state_name[kPiNC_B] << " Boc (PiNC_B) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kPiNC_B] variable " << boc_state_name[kPiNC_B]  << " already exists but it is not a value variable." << std::endl;
	  }
	}
	
	// VarDef for kPiNC_C
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(boc_state_name[kPiNC_C], PixCon::kRodValue, -1., 1.5,"",50);	
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(-1.,1.5);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor], " << boc_state_name[kPiNC_C] << " Boc (PiNC_C) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kPiNC_C] variable " << boc_state_name[kPiNC_C]  << " already exists but it is not a value variable." << std::endl;
	  }
	}
	
	// VarDef for kPiNC_D
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(boc_state_name[kPiNC_D], PixCon::kRodValue, -1., 1.5,"",50);	
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(-1.,1.5);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor], " << boc_state_name[kPiNC_D] << " Boc (PiNC_D) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kPiNC_D] variable " << boc_state_name[kPiNC_D]  << " already exists but it is not a value variable." << std::endl;
	  }
	}
	
	// VarDef for kPiNV_A
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(boc_state_name[kPiNV_A], PixCon::kRodValue, -1., 12.,"",50);	
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(-1.,12.);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor], " << boc_state_name[kPiNV_A] << " Boc (PiNV_A) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kPiNV_A] variable " << boc_state_name[kPiNV_A]  << " already exists but it is not a value variable." << std::endl;
	  }
	}
	
	// VarDef for kPiNV_B
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(boc_state_name[kPiNV_B], PixCon::kRodValue, -1., 12.,"",50);	
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(-1.,12.);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor], " << boc_state_name[kPiNV_B] << " Boc (PiNV_B) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kPiNV_B] variable " << boc_state_name[kPiNV_B]  << " already exists but it is not a value variable." << std::endl;
	  }
	}
	
	// VarDef for kPiNV_C
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(boc_state_name[kPiNV_C], PixCon::kRodValue, -1., 12.,"",50);	
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(-1.,12.);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor], " << boc_state_name[kPiNV_C] << " Boc (PiNV_C) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kPiNV_C] variable " << boc_state_name[kPiNV_C]  << " already exists but it is not a value variable." << std::endl;
	  }
	} 
	
	// VarDef for kPiNV_D
	{
	  PixCon::VarDef_t var_def= PixCon::VarDefList::instance()->createValueVarDef(boc_state_name[kPiNV_D], PixCon::kRodValue, -1., 12.,"",50);	
	  if (var_def.isValueWithOrWithoutHistory()) {
	    PixCon::ValueVarDef_t &value_def = var_def.valueDef();
	    value_def.setMinMax(-1.,12.);
	    std::cout << "INFO [RodStatusMonitor::setupIsMonitor], " << boc_state_name[kPiNV_D] << " Boc (PiNV_D) states defined." << std::endl;
	  } else {
	    std::cout << "ERROR [RodStatusMonitor::kPiNV_D] variable " << boc_state_name[kPiNV_D]  << " already exists but it is not a value variable." << std::endl;
	  }
	}
      } // if (m_userMode->getUserMode() == PixCon::UserMode::kExpert)

      


      //add category to hold variables that will be monitored
      PixCon::CategoryList::Category daq_cat=m_categories->addCategory("DAQ");
      PixCon::CategoryList::Category rod_cat=m_categories->addCategory("ROD");
      PixCon::CategoryList::Category mod_cat=m_categories->addCategory("Module");
      PixCon::CategoryList::Category tim_cat=m_categories->addCategory("TIM");
      PixCon::CategoryList::Category boc_cat=m_categories->addCategory("BOC");
      PixCon::CategoryList::Category dsp_mon_cat=m_categories->addCategory("DSPMon");
    
      //  AVAILABLE now  covered by the action tags monitor which uses the AVAILABLE as a trigger 
      //  i.e. if the AVAILABLE changes and is 1 i.e. available.

      std::shared_ptr<PixCon::IsMonitorManager> is_monitor_manager( m_detectorView->monitorManager() );


      //Add "DAQEnabled" monitoring
      is_monitor_manager->addMonitor(daq_state_name[kDAQEnabled],
				     std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntStateMonitor(m_receptor,
													    PixCon::kModuleValue,
													    5 /* -> undefined, when removed from IS */,
													    PixCon::s_daqEnabledNameExtractor,
													    ".*ROD.*DAQEnabled")));
      
      if (!m_monitorActionServerTags) {

      //Add "Available" monitoring
      is_monitor_manager->addMonitor("AVAILABLE", 
 				 std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntStateMonitor( m_receptor,
 													 PixCon::kRodValue,
                                                                                                         2 /* -> undefined */,
 													 PixCon::s_rodAvailableNameExtractor,
 													 ".*ROD.*S.*AVAILABLE")));
      }
      else {
	std::cout << "INFO [RodStatusMonitor::setupIsMonitor] Monitor tags reported by actions. " << std::endl; 
	is_monitor_manager->addMonitor("ActionTags",std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::ActionTagsMonitor(m_receptor,
													  PixCon::s_rodAvailableNameExtractor)));
      }

      if (m_dtReceptor.get()) {
	std::cout << "INFO [RodStatusMonitor::setupIsMonitor] Monitor connectivity / cfg tags." << std::endl; 
	is_monitor_manager->addMonitor("DataTakingConfig",std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::TagsMonitor(m_dtReceptor,
												   "DataTakingConfig-")));
      }
	

      //Add "BUSY" monitoring
      is_monitor_manager->addMonitor("BUSY", 
				 std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntValueMonitor( m_receptor,
													 PixCon::kRodValue,
													 0 /* mark not busy when value is deleted */,
													 PixCon::s_rodAvailableNameExtractor,
													 ".*ROD.*BUSY",2*(132+15),1000)));


      //Add "RODQSMode" monitoring
      is_monitor_manager->addMonitor("RODQSMode",
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsStrListMonitor(m_receptor,
													     PixCon::kRodValue,
													     "" /*when value deleted -> unknown */,
													     PixCon::s_rodQSModeNameExtractor,
													     ".*ROD.*QSMode",
													     RODQSMap)));

      //Add "RODAvgOcc" monitoring
      is_monitor_manager->addMonitor("AvgOcc", 
				 std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsDoubleValueMonitor(m_receptor,
													 PixCon::kRodValue,
                           0. /* when value is deleted from IS*/,
													   PixCon::s_rodQSModeNameExtractor,
													   ".*ROD.*AvgOcc")));

      //Add "RODSlinkPercentage" monitoring
      is_monitor_manager->addMonitor("SlinkPercentage", 
				 std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsDoubleValueMonitor(m_receptor,
													 PixCon::kRodValue,
                           0. /* when value is deleted from IS*/,
													   PixCon::s_rodQSModeNameExtractor,
													   ".*ROD.*SlinkPercentage")));

      //Add "RODLargeFragmentPercentage" monitoring
      is_monitor_manager->addMonitor("LargeFragmentPercentage", 
				 std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsDoubleValueMonitor(m_receptor,
													 PixCon::kRodValue,
                           0. /* when value is deleted from IS*/,
													   PixCon::s_rodQSModeNameExtractor,
													   ".*ROD.*LargeFragmentPercentage")));

      //Add "RODLargeFragmentPercentage" monitoring
      is_monitor_manager->addMonitor("regParityErrorFraction", 
         std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsDoubleValueMonitor(m_receptor,
                           PixCon::kRodValue,
                           0. /* when value is deleted from IS*/,
                             PixCon::s_rodQSModeNameExtractor,
                             ".*ROD.*regParityErrorFraction")));

      //Add "RODLargeFragmentPercentage" monitoring
      is_monitor_manager->addMonitor("rceFraction", 
         std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsDoubleValueMonitor(m_receptor,
                           PixCon::kRodValue,
                           0. /* when value is deleted from IS*/,
                             PixCon::s_rodQSModeNameExtractor,
                             ".*ROD.*rceFraction")));

      //Add "RODLargeFragmentPercentage" monitoring
      is_monitor_manager->addMonitor("eocFraction", 
         std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsDoubleValueMonitor(m_receptor,
                           PixCon::kRodValue,
                           0. /* when value is deleted from IS*/,
                             PixCon::s_rodQSModeNameExtractor,
                             ".*ROD.*eocFraction")));

//HistoryRunning
  is_monitor_manager->addMonitor("HistoryRunning", 
               std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntStateMonitor(m_receptor,
                                PixCon::kRodValue,
                                2 /* should become undefined when value is deleted from IS  */,
                                PixCon::s_rodQSModeNameExtractor,
                                ".*ROD.*HistoryRunning")));


      if (1 || m_userMode->getUserMode() == PixCon::UserMode::kExpert) {

	//Add "STATUS" monitoring
	is_monitor_manager->addMonitor("ROD_STATUS", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsStrListMonitor(m_receptor,
													     PixCon::kRodValue,
													     "" /* when value deleted -> unknown */,
													     PixCon::s_rodAvailableNameExtractor,
													     "ROD_CRATE.*ROD.*S.*STATUS",
													     statusMap)));
	//Add "CURRENT-ACTION" monitoring
	is_monitor_manager->addMonitor("CURRENT_ACTION", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsStrListMonitor(m_receptor,
													     PixCon::kRodValue,
													     "" /* when value deleted should result in unknown*/,
													     PixCon::s_rodAvailableNameExtractor,
													     ".*ROD.*CURRENT-ACTION",
													     curActMap)));
	//Add "ALLOCATING-PARTITION" monitoring
	is_monitor_manager->addMonitor("ALLOCATING_PARTITION",
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsStrListMonitor(m_receptor,
													     PixCon::kRodValue,
													     "" /* when value deleted should result in unknown*/,
													     PixCon::s_rodAvailableNameExtractor,
													     ".*ROD.*ALLOCATING-PARTITION",
													     allocPartMap)));
      } // if (m_userMode->getUserMode() == PixCon::UserMode::kExpert)

      //Add "LAST-WD-CALL" monitoring
      is_monitor_manager->addMonitor("LAST_CALL", 
				 std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntValueMonitor(m_receptor,
												        PixCon::kRodValue,
													0,
												        PixCon::s_rodAvailableNameExtractor,
												        ".*ROD.*LAST.*",
													2*(132+15) /* buffer size*/,
													60 /*Request reconnect if there is no update
													     at least every 60 sec.*/)));
      if (m_userMode->getUserMode() == PixCon::UserMode::kExpert) {
	//Add ROD "dataincorrect errors" monitoring
	is_monitor_manager->addMonitor("dataincorrect_errors", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntValueMonitor( m_receptor,
													       PixCon::kRodValue,
													       0 /*when value is deleted from IS*/,
													       PixCon::s_rodBusyPercNameExtractor,
													       ".*ROD.*S.*dataincorrect_errors")));
	
	//Add ROD "Fragment Size" monitoring
	is_monitor_manager->addMonitor("FragmentSize", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntValueMonitor( m_receptor,
													       PixCon::kRodValue,
													       -1/* when value is deleted from IS*/,
													       PixCon::s_rodBusyPercNameExtractor,
													       ".*ROD.*S.*FragmentSize")));

	//Add ROD DSPMon_RODsyncEna monitoring
	is_monitor_manager->addMonitor("RODsyncEna", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsStrListMonitor( m_receptor,
													       PixCon::kRodValue,
													       "" /* when value deleted -> unknown */,
													       PixCon::s_dspMonNameExtractor,
													       ".*ROD.*S.*DSPMon_RODsyncEna",
                                 RODsyncEnaMap)));

	//Add ROD DSPMon_RODsyncMinIdle monitoring
	is_monitor_manager->addMonitor("RODsyncMinIdle", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsUIntValueMonitor( m_receptor,
													       PixCon::kRodValue,
													       0 /* when value is deleted from IS */,
													       PixCon::s_dspMonNameExtractor,
													       ".*ROD.*S.*DSPMon_RODsyncMinIdle")));

	//----------------------Module variables---------------------------------------  
  
	//Add module active status monitoring
    
	is_monitor_manager->addMonitor("NHits", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntValueMonitor(m_receptor,
													      PixCon::kModuleValue,
													      -1 /* when value is deleted from IS*/,
													      PixCon::s_modAvailableNameExtractor,
													      ".*ROD.*S.*M.*NHits")));
      } // if (m_userMode->getUserMode() == PixCon::UserMode::kExpert)

      //Add module BCID error monitoring
      is_monitor_manager->addMonitor("BCID_errors", 
				 std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntValueMonitor(m_receptor,
												        PixCon::kModuleValue,
                                                                                                        0 /* when value is deleted from IS*/,
												        PixCon::s_modAvailableNameExtractor,
												        ".*ROD.*S.*M.*BCID_errors")));

      //Add module LVL1ID error monitoring
      is_monitor_manager->addMonitor("LVL1ID_errors", 
				 std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntValueMonitor(m_receptor,
												        PixCon::kModuleValue,
                                                                                                        0 /* when value is deleted from IS*/,
												        PixCon::s_modAvailableNameExtractor,
												        ".*ROD.*S.*M.*LVL1ID_errors")));

      if (m_userMode->getUserMode() == PixCon::UserMode::kExpert) {
	//Add module Timeout error monitoring
	is_monitor_manager->addMonitor("timeout_errors", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntValueMonitor(m_receptor,
													      PixCon::kModuleValue,
													      0 /* when value is deleted from IS*/,
													      PixCon::s_modAvailableNameExtractor,
													      ".*ROD.*S.*M.*timeout_errors")));

	//Add module preamble error monitoring
	is_monitor_manager->addMonitor("preamble_errors", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntValueMonitor(m_receptor,
													      PixCon::kModuleValue,
													      0 /* when value is deleted from IS*/,
													      PixCon::s_modAvailableNameExtractor,
													      ".*ROD.*S.*M.*preamble_errors")));
	//Add module trailer bit error monitoring
	is_monitor_manager->addMonitor("trailbit_errors", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntValueMonitor(m_receptor,
													      PixCon::kModuleValue,
													      0 /* when value is deleted from IS*/,
													      PixCon::s_modAvailableNameExtractor,
													      ".*ROD.*S.*M.*trailerbit_errors")));
	//Add module header trailer limit error monitoring
	is_monitor_manager->addMonitor("headtraillimit_errors", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntValueMonitor(m_receptor,
													      PixCon::kModuleValue,
													      0 /* when value is deleted from IS*/,
													      PixCon::s_modAvailableNameExtractor,
													      ".*ROD.*S.*M.*headtraillimit_errors")));

	//Add module data overflow error monitoring
	is_monitor_manager->addMonitor("dataoverflow_errors", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntValueMonitor(m_receptor,
													      PixCon::kModuleValue,
													      0 /* when value is deleted from IS*/,
													      PixCon::s_modAvailableNameExtractor,
													      ".*ROD.*S.*M.*dataoverflow_errors")));

	//Add module FE and MCC error monitoring
	is_monitor_manager->addMonitor("FEEOC_errors", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntValueMonitor(m_receptor,
													      PixCon::kModuleValue,
													      0 /* when value is deleted from IS*/,
													      PixCon::s_modAvailableNameExtractor,
													      ".*ROD.*S.*M.*FEEOC_errors")));
      } // if (m_userMode->getUserMode() == PixCon::UserMode::kExpert)

      is_monitor_manager->addMonitor("FEHamming_errors", 
				 std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntValueMonitor(m_receptor,
												        PixCon::kModuleValue,
													0 /* when value is deleted from IS*/,
												        PixCon::s_modAvailableNameExtractor,
												        ".*ROD.*S.*M.*FEHamming_errors")));											
      is_monitor_manager->addMonitor("FEregparity_errors", 
				 std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntValueMonitor(m_receptor,
												        PixCon::kModuleValue,
													0 /* when value is deleted from IS*/,
												        PixCon::s_modAvailableNameExtractor,
												        ".*ROD.*S.*M.*FEregparity_errors")));											
      is_monitor_manager->addMonitor("FEhitparity_errors", 
				 std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntValueMonitor(m_receptor,
												        PixCon::kModuleValue,
													0 /* when value is deleted from IS*/,
												        PixCon::s_modAvailableNameExtractor,
												        ".*ROD.*S.*M.*FEhitparity_errors")));											
      is_monitor_manager->addMonitor("MCChitoverflow_errors", 
				 std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntValueMonitor(m_receptor,
												        PixCon::kModuleValue,
													0 /* when value is deleted from IS*/,
												        PixCon::s_modAvailableNameExtractor,
												        ".*ROD.*S.*M.*MCChitoverflow_errors")));											
      is_monitor_manager->addMonitor("MCCEoEoverflow_errors", 
				 std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntValueMonitor(m_receptor,
												        PixCon::kModuleValue,
													0 /* when value is deleted from IS*/,
												        PixCon::s_modAvailableNameExtractor,
												        ".*ROD.*S.*M.*MCCEoEoverflow_errors")));											
      if (m_userMode->getUserMode() == PixCon::UserMode::kExpert) {
	is_monitor_manager->addMonitor("MCCL1ID_errors", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntValueMonitor(m_receptor,
													      PixCon::kModuleValue,
													      0 /* when value is deleted from IS*/,
													      PixCon::s_modAvailableNameExtractor,
													      ".*ROD.*S.*M.*MCCL1ID_errors")));											
	is_monitor_manager->addMonitor("MCCBCIDEoE_errors", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntValueMonitor(m_receptor,
													      PixCon::kModuleValue,
													      0 /* when value is deleted from IS*/,
													      PixCon::s_modAvailableNameExtractor,
													      ".*ROD.*S.*M.*MCCBCIDEoE_errors")));											
	is_monitor_manager->addMonitor("MCCL1IDEoE_errors", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntValueMonitor(m_receptor,
													      PixCon::kModuleValue,
													      0 /* when value is deleted from IS*/,
													      PixCon::s_modAvailableNameExtractor,
													      ".*ROD.*S.*M.*MCCL1IDEoE_errors")));											
	//Add module scan error count
	is_monitor_manager->addMonitor("MODERR", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsUIntValueMonitor(m_receptor,
													       PixCon::kModuleValue,
													       0 /* when value is deleted from IS*/,
													       PixCon::s_modFmtStatNameExtractor,
													       ".*ROD.*M.*MODERR")));
      } // if (m_userMode->getUserMode() == PixCon::UserMode::kExpert)

      //Add module fmt stat
      is_monitor_manager->addMonitor("FMTSTAT", 
				 std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsStrListMonitor(m_receptor,
												       PixCon::kModuleValue,
                                                                                                       "-----" /* should result in undefined when value is deleted from IS */,
												       PixCon::s_modFmtStatNameExtractor,
												       ".*ROD.*M.*FMTSTAT",
				                                                                       fmtStatMap)));

      //Add module busy counter (from readRodSlaveBusy)
      is_monitor_manager->addMonitor("BUSY_HIST",
				 std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsUIntValueMonitor(m_receptor,
												       PixCon::kModuleValue,
                                                                                                       0 /* when value is deleted from IS */,
												       PixCon::s_modFmtStatNameExtractor,
												       ".*ROD.*M.*BUSY_HIST")));
      //Add module scan error count
      is_monitor_manager->addMonitor("NOcc", 
				 std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsDoubleValueMonitor(m_receptor,
													 PixCon::kModuleValue,
                                                                                                         0 /* when value is deleted from IS*/,
													 PixCon::s_modFmtStatNameExtractor,
													 ".*ROD.*M.*NOcc")));

      //Add module FMT based link usage
      is_monitor_manager->addMonitor("FMTLinkUsage", 
         std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsDoubleValueMonitor(m_receptor,
                           PixCon::kModuleValue,
                                                                                                         0 /* when value is deleted from IS*/,
                           PixCon::s_modFmtStatNameExtractor,
                           ".*ROD.*M.*FMTLinkUsage")));
      //Add module FMT based occupancy
      is_monitor_manager->addMonitor("FMTOccupancy", 
         std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsDoubleValueMonitor(m_receptor,
                           PixCon::kModuleValue,
                                                                                                         0 /* when value is deleted from IS*/,
                           PixCon::s_modFmtStatNameExtractor,
                           ".*ROD.*M.*FMTOccupancy")));

      //Add module FMT desynch L1ID events
      is_monitor_manager->addMonitor("FMTDesynchL1ID",
         std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsDoubleValueMonitor(m_receptor,
                           PixCon::kModuleValue,
                                                                                                         0 /* when value is deleted from IS*/,
                           PixCon::s_modFmtStatNameExtractor,
                           ".*ROD.*M.*FMTDesynchL1ID")));

      //Add module FMT Busy
      is_monitor_manager->addMonitor("FMTBusyFrac",
         std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsDoubleValueMonitor(m_receptor,
                           PixCon::kModuleValue,
                                                                                                         0 /* when value is deleted from IS*/,
                           PixCon::s_modFmtStatNameExtractor,
                           ".*ROD.*M.*FMTBusyFrac")));
      //Add module FMT Total desynch
      is_monitor_manager->addMonitor("TotalDesync",
         std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsDoubleValueMonitor(m_receptor,
                           PixCon::kModuleValue,
                                                                                                         0 /* when value is deleted from IS*/,
                           PixCon::s_modFmtStatNameExtractor,
                           ".*ROD.*M.*TOTDesync")));
            //Add module BOC Frame Count
      is_monitor_manager->addMonitor("BOCFrameCount",
         std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsUIntValueMonitor(m_receptor,
                           PixCon::kModuleValue,
                                                                                                         0 /* when value is deleted from IS*/,
                           PixCon::s_modFmtStatNameExtractor,
                           ".*ROD.*M.*BOCFrameCount")));
      //Add module BOC Decoding Error
      is_monitor_manager->addMonitor("BOCDecodingError",
         std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsUIntValueMonitor(m_receptor,
                           PixCon::kModuleValue,
                                                                                                         0 /* when value is deleted from IS*/,
                           PixCon::s_modFmtStatNameExtractor,
                           ".*ROD.*M.*BOCDecodingError")));
      //Add module BOC Frame Error
      is_monitor_manager->addMonitor("BOCFrameError",
         std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsUIntValueMonitor(m_receptor,
                           PixCon::kModuleValue,
                                                                                                         0 /* when value is deleted from IS*/,
                           PixCon::s_modFmtStatNameExtractor,
                           ".*ROD.*M.*BOCFrameError")));
      //Add module nRST
      is_monitor_manager->addMonitor("nRST",
				 std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsUIntValueMonitor(m_receptor,
													 PixCon::kModuleValue,
                                                                                                         0 /* when value is deleted from IS*/,
													 PixCon::s_modFmtLinknRSTNameExtractor,
													 ".*ROD.*M.*nRST")));




      //Add module nCFG
      is_monitor_manager->addMonitor("nCFG",
				     std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsUIntValueMonitor(m_receptor,
													     PixCon::kModuleValue,
													     0 /* when value is deleted from IS*/,
													     PixCon::s_modFmtLinknRSTNameExtractor,
													 ".*ROD.*M.*nCFG")));

      //Add module nRODSYNC
      is_monitor_manager->addMonitor("nRODSYNC",
				     std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsUIntValueMonitor(m_receptor,
													     PixCon::kModuleValue,
													     0 /* when value is deleted from IS*/,
													     PixCon::s_modFmtLinknRSTNameExtractor,
													     ".*ROD.*M.*nRODSYNC")));

      //Add module nMODSYNC
      is_monitor_manager->addMonitor("nMODSYNC",
				     std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsUIntValueMonitor(m_receptor,
													     PixCon::kModuleValue,
													     0 /* when value is deleted from IS*/,
													     PixCon::s_modFmtLinknRSTNameExtractor,
													     ".*ROD.*M.*nMODSYNC")));

      //Add module nUSER
      is_monitor_manager->addMonitor("nUSER",
				     std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsUIntValueMonitor(m_receptor,
													     PixCon::kModuleValue,
													     0 /* when value is deleted from IS*/,
													     PixCon::s_modFmtLinknRSTNameExtractor,
													     ".*ROD.*M.*nUSER")));



      if (m_userMode->getUserMode() == PixCon::UserMode::kExpert) {
	//----------------------TIM variables---------------------------------------
	//Add tim LVLID counter monitoring
	is_monitor_manager->addMonitor("L1IDCounterValue", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntValueMonitor(m_receptor,
													      PixCon::kTimValue,
													      -1 /* when value is deleted from IS  */,
													      PixCon::s_rodBusyPercNameExtractor,
													      "TIM.*L1IDCounterValue",100 /*buffer size*/)));
	//Add tim ECR counter monitoring
	is_monitor_manager->addMonitor("ECRCounterValue", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntValueMonitor(m_receptor,
													      PixCon::kTimValue,
													      0  /* when value is deleted from IS  */,
													      PixCon::s_rodBusyPercNameExtractor,
													      "TIM.*ECRCounterValue",100 /*buffer size*/)));
      } // if (m_userMode->getUserMode() == PixCon::UserMode::kExpert)

      //Add tim ROD Busy counter monitoring
      is_monitor_manager->addMonitor("RodBusyDuration", 
				 std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsDoubleValueMonitor(m_receptor,
												        PixCon::kTimValue,
													0  /* when value is deleted from IS  */,
												        PixCon::s_rodBusyPercNameExtractor,
												        "TIM.*RodBusyDuration",100 /*buffer size*/)));

      if (m_userMode->getUserMode() == PixCon::UserMode::kExpert) {
	//Add tim Trigger Type monitoring
	is_monitor_manager->addMonitor("TriggerType", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntValueMonitor(m_receptor,
													      PixCon::kTimValue,
													      -1 /* when value is deleted from IS  */,
													      PixCon::s_rodBusyPercNameExtractor,
													      "TIM.*TriggerType", 100 /*buffer size*/)));
	//Add tim BCID offset monitoring
	is_monitor_manager->addMonitor("TimBcidOffset", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntValueMonitor(m_receptor,
													      PixCon::kTimValue,
													      -1  /* when value is deleted from IS  */,
													      PixCon::s_rodBusyPercNameExtractor,
													      "TIM.*TimBcidOffset", 100 /*buffer size*/)));
	//Add tim Trigger Delay monitoring
	is_monitor_manager->addMonitor("TimTriggerDelay", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntValueMonitor(m_receptor,
													      PixCon::kTimValue,
													      -1  /* when value is deleted from IS  */,
													      PixCon::s_rodBusyPercNameExtractor,
													      "TIM.*TimTriggerDelay", 100 /*buffer size*/)));

	//Add tim Ok Status Changes monitoring
	is_monitor_manager->addMonitor("TimOkChanges", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntValueMonitor(m_receptor,
													      PixCon::kTimValue,
													      0 /* when value is deleted from IS  */,
													      PixCon::s_rodBusyPercNameExtractor,
													      "TIM.*TimOkChanges",100 /*buffer size*/)));
      } // if (m_userMode->getUserMode() == PixCon::UserMode::kExpert)

      //Add tim QPLL lock Changes monitoring
      is_monitor_manager->addMonitor("QPLLChanges",
				     std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntValueMonitor(m_receptor,
													    PixCon::kTimValue,
													    0  /* when value is deleted from IS  */,
													    PixCon::s_rodBusyPercNameExtractor,
													    "TIM.*QpllLockChanges",100 /*buffer size*/)));

      if (m_userMode->getUserMode() == PixCon::UserMode::kExpert) {
	//Add monitoring of TTCrxFineDelays
	is_monitor_manager->addMonitor("TTCrxFineDelay", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsDoubleValueMonitor(m_receptor,
														 PixCon::kTimValue,
														 -1 /* when value is deleted from IS  */,
														 PixCon::s_rodBusyPercNameExtractor,
														 "TIM.*TTCrxFineDelay", 100 /*buffer size*/)));
	//Add monitoring of TTCrxCoarseDelays
	is_monitor_manager->addMonitor("TTCrxCoarseDelay", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntValueMonitor(m_receptor,
													      PixCon::kTimValue,
													      -1 /* when value is deleted from IS  */,
													      PixCon::s_rodBusyPercNameExtractor,
													      "TIM.*TTCrxCoarseDelay", 100 /*buffer size*/)));

	//Add monitoring of FFTVCounterValue
	is_monitor_manager->addMonitor("FFTVCounterValue",
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsUIntValueMonitor(m_receptor,
													      PixCon::kTimValue,
													      -1 /* when value is deleted from IS  */,
													      PixCon::s_rodBusyPercNameExtractor,
													      "TIM.*FFTVCounterValue", 100 /*buffer size*/)));

	//Add monitoring of FFTVBusyFraction
	is_monitor_manager->addMonitor("FFTVBusyFraction",
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsDoubleValueMonitor(m_receptor,
														 PixCon::kTimValue,
														 -1 /* when value is deleted from IS  */,
														 PixCon::s_rodBusyPercNameExtractor,
														 "TIM.*FFTVBusyFraction", 100 /*buffer size*/)));

	//Add monitoring of FFTV_B_CounterValue
	is_monitor_manager->addMonitor("FFTV_B_CounterValue",
               std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsUIntValueMonitor(m_receptor,
                             PixCon::kTimValue,
                             -1 /* when value is deleted from IS  */,
                             PixCon::s_rodBusyPercNameExtractor,
                             "TIM.*FFTV_B_CounterValue", 100 /*buffer size*/)));

	//Add monitoring of FFTV_B_BusyFraction
	is_monitor_manager->addMonitor("FFTV_B_BusyFraction",
               std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsDoubleValueMonitor(m_receptor,
                                PixCon::kTimValue,
                                -1 /* when value is deleted from IS  */,
                                PixCon::s_rodBusyPercNameExtractor,
                                "TIM.*FFTV_B_BusyFraction", 100 /*buffer size*/))); 	

	//----------------------BOC variables---------------------------------------
	//Add BOC clock monitoring
	is_monitor_manager->addMonitor("ClockOk", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntStateMonitor(m_receptor,
													      PixCon::kRodValue,
													      2 /* should become undefined when value is deleted from IS  */,
													      PixCon::s_bocMonitoringNameExtractor,
													      "Monitoring.*ROD.*ClockOk")));
	
	//Add BOC Interlock monitoring
	is_monitor_manager->addMonitor("InterlocksOk", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntStateMonitor(m_receptor,
													      PixCon::kRodValue,
													      2 /* should become undefined when value is deleted from IS  */,
													      PixCon::s_bocMonitoringNameExtractor,
													      "Monitoring.*ROD.*InterlocksOk")));
	
	//Add BOC Local Interlock monitoring
	is_monitor_manager->addMonitor("LocalInterlock", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntStateMonitor(m_receptor,
													      PixCon::kRodValue,
													      2 /* should become undefined when value is deleted from IS  */,
													      PixCon::s_bocMonitoringNameExtractor,
													      "Monitoring.*ROD.*LocalInterlock")));

	//Add BOC Remote Interlock monitoring
	is_monitor_manager->addMonitor("RemoteInterlock", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntStateMonitor(m_receptor,
													      PixCon::kRodValue,
													      2 /* should become undefined when value is deleted from IS  */,
													      PixCon::s_bocMonitoringNameExtractor,
													      "Monitoring.*ROD.*RemoteInterlock")));

	//Add BOC VPinStatusOk monitoring
	is_monitor_manager->addMonitor("VPinStatusOk", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsIntStateMonitor(m_receptor,
													      PixCon::kRodValue,
													      2 /* should become undefined when value is deleted from IS  */,
													      PixCon::s_bocMonitoringNameExtractor,
													      "Monitoring.*ROD.*VPinStatusOk")));

	//Add BOC TempRX monitoring
	is_monitor_manager->addMonitor("TempRX", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsValueMonitor(m_receptor,
													   PixCon::kRodValue,
													   -1 /* when value is deleted from IS  */,
													   PixCon::s_bocMonitoringNameExtractor,
													   "Monitoring.*ROD.*TempRX")));

	//Add BOC TempTX monitoring
	is_monitor_manager->addMonitor("TempTX", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsValueMonitor(m_receptor,
													   PixCon::kRodValue,
													   -1 /* when value is deleted from IS  */,
													   PixCon::s_bocMonitoringNameExtractor,
													   "Monitoring.*ROD.*TempTX")));
	
	//Add BOC PiNC_A monitoring
	is_monitor_manager->addMonitor("PiNC_A", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsValueMonitor(m_receptor,
													   PixCon::kRodValue,
													   -1 /* when value is deleted from IS  */,
													   PixCon::s_bocMonitoringNameExtractor,
													   "Monitoring.*ROD.*PiNC_A")));
	//Add BOC PiNC_B monitoring
	is_monitor_manager->addMonitor("PiNC_B", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsValueMonitor(m_receptor,
													   PixCon::kRodValue,
													   -1 /* when value is deleted from IS  */,
													   PixCon::s_bocMonitoringNameExtractor,
													   "Monitoring.*ROD.*PiNC_B")));
	
	//Add BOC PiNC_C monitoring
	is_monitor_manager->addMonitor("PiNC_C", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsValueMonitor(m_receptor,
													   PixCon::kRodValue,
													   -1 /* when value is deleted from IS  */,
													   PixCon::s_bocMonitoringNameExtractor,
													   "Monitoring.*ROD.*PiNC_C")));
	
	//Add BOC PiNC_D monitoring
	is_monitor_manager->addMonitor("PinC_D", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsValueMonitor(m_receptor,
													   PixCon::kRodValue,
													   -1 /* when value is deleted from IS  */,
													   PixCon::s_bocMonitoringNameExtractor,
													   "Monitoring.*ROD.*PiNC_D")));
	
	//Add BOC PiNV_A monitoring
	is_monitor_manager->addMonitor("PinV_A", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsValueMonitor(m_receptor,
													   PixCon::kRodValue,
													   -1 /* when value is deleted from IS  */,
													   PixCon::s_bocMonitoringNameExtractor,
													   "Monitoring.*ROD.*PiNV_A")));
	//Add BOC PiNV_B monitoring
	is_monitor_manager->addMonitor("PinV_B", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsValueMonitor(m_receptor,
													   PixCon::kRodValue,
													   -1 /* when value is deleted from IS  */,
													   PixCon::s_bocMonitoringNameExtractor,
													   "Monitoring.*ROD.*PiNV_B")));
	
	//Add BOC PiNV_C monitoring
	is_monitor_manager->addMonitor("PinV_C", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsValueMonitor(m_receptor,
													   PixCon::kRodValue,
													   -1 /* when value is deleted from IS  */,
													   PixCon::s_bocMonitoringNameExtractor,
													   "Monitoring.*ROD.*PiNV_C")));
	
	//Add BOC PiNV_D monitoring
	is_monitor_manager->addMonitor("PinV_D", 
				       std::shared_ptr<PixCon::IsMonitorBase>(new PixCon::IsValueMonitor(m_receptor,
													   PixCon::kRodValue,
													   -1 /* when value is deleted from IS  */,
													   PixCon::s_bocMonitoringNameExtractor,
													   "Monitoring.*ROD.*PiNV_D")));
      } // if (m_userMode->getUserMode() == PixCon::UserMode::kExpert)

      daq_cat.addVariable(daq_state_name[kDAQEnabled]);

      rod_cat.addVariable(rod_state_name[kRODAllocated]);
      rod_cat.addVariable(rod_state_name[kRODBusy]);
      rod_cat.addVariable(rod_state_name[kRODQSMode]);
      rod_cat.addVariable(rod_state_name[kRODAvgOcc]);
      rod_cat.addVariable(rod_state_name[kRODSlinkPercentage]);
      rod_cat.addVariable(rod_state_name[kRODLargeFragmentPercentage]);
      rod_cat.addVariable(rod_state_name[kRODregPartity]);
      rod_cat.addVariable(rod_state_name[kRODrceFrac]);
      rod_cat.addVariable(rod_state_name[kRODeocFrac]);
      rod_cat.addVariable(rod_state_name[kRODhistoryRunning]);

      if (m_userMode->getUserMode() == PixCon::UserMode::kExpert) {
	rod_cat.addVariable(rod_state_name[kRODCurAction]);
	rod_cat.addVariable(rod_state_name[kRODAllocPart]);
	rod_cat.addVariable(rod_state_name[kRODStatus]);
      } // if (m_userMode->getUserMode() == PixCon::UserMode::kExpert)
      rod_cat.addVariable(rod_state_name[kWDTime]);
      rod_cat.addVariable(rod_state_name[kCMDTime]);
      rod_cat.addVariable(rod_state_name[kCMDLen]);
      if (m_userMode->getUserMode() == PixCon::UserMode::kExpert) {
	//rod_cat.addVariable(rod_state_name[kRODBCIDerr]);
	// rod_cat.addVariable(rod_state_name[kRODLVL1IDerr]);
	//rod_cat.addVariable(rod_state_name[kRODTimeOutErr]);
	rod_cat.addVariable(rod_state_name[kRODDataIncorrectErr]);
	//rod_cat.addVariable(rod_state_name[kRODDataOverflowErr]);
	rod_cat.addVariable(rod_state_name[kRODFragmentSize]);

	dsp_mon_cat.addVariable(dsp_mon_state_name[kRODsyncEna]);
	dsp_mon_cat.addVariable(dsp_mon_state_name[kRODsyncMinIdle]);
	
	mod_cat.addVariable(mod_state_name[kNHits]);
      } // if (m_userMode->getUserMode() == PixCon::UserMode::kExpert)
      mod_cat.addVariable(mod_state_name[kModBCIDerr]);
      mod_cat.addVariable(mod_state_name[kModLVL1IDerr]);
      if (m_userMode->getUserMode() == PixCon::UserMode::kExpert) {
	mod_cat.addVariable(mod_state_name[kModTimeOutErr]);
	mod_cat.addVariable(mod_state_name[kModPreambleErr]);
	mod_cat.addVariable(mod_state_name[kModTrailBitErr]);
	mod_cat.addVariable(mod_state_name[kModHeadTrailLimErr]);
	mod_cat.addVariable(mod_state_name[kModDataOverflowErr]);
	mod_cat.addVariable(mod_state_name[kModFEEOCErr]);
      } // if (m_userMode->getUserMode() == PixCon::UserMode::kExpert)
      mod_cat.addVariable(mod_state_name[kModFEHammingErr]);
      mod_cat.addVariable(mod_state_name[kModFeregparityErr]);
      mod_cat.addVariable(mod_state_name[kModFEhitparityErr]);
      mod_cat.addVariable(mod_state_name[kModMCChitoverflowErr]);
      mod_cat.addVariable(mod_state_name[kModMCCEOEoverflowErr]);
      if (m_userMode->getUserMode() == PixCon::UserMode::kExpert) {
	mod_cat.addVariable(mod_state_name[kModMCCL1IDErr]);
	mod_cat.addVariable(mod_state_name[kModBCIDEoEErr]);
	mod_cat.addVariable(mod_state_name[kModL1IDEoEErr]);
	mod_cat.addVariable(mod_state_name[kModScanErrCount]);
      } // if (m_userMode->getUserMode() == PixCon::UserMode::kExpert)
      mod_cat.addVariable(mod_state_name[kModFmtStat]);
      mod_cat.addVariable(mod_state_name[kModBusyHist]);
      mod_cat.addVariable(mod_state_name[kModNOcc]);
      mod_cat.addVariable(mod_state_name[kModLinkUsage]);
      mod_cat.addVariable(mod_state_name[kModOccupancy]);
      mod_cat.addVariable(mod_state_name[kModFMTDesynchL1ID]);
      mod_cat.addVariable(mod_state_name[kModFMTBusyFrac]);
      mod_cat.addVariable(mod_state_name[kModTOTDesync]);
      mod_cat.addVariable(mod_state_name[kModBOCFrameCount]);
      mod_cat.addVariable(mod_state_name[kModBOCDecodingError]);
      mod_cat.addVariable(mod_state_name[kModBOCFrameError]);
      mod_cat.addVariable(mod_state_name[kModFmtLinknRST]);
      mod_cat.addVariable(mod_state_name[kModnCFG]);
      mod_cat.addVariable(mod_state_name[kModnRODSYNC]);
      mod_cat.addVariable(mod_state_name[kModnMODSYNC]);
      mod_cat.addVariable(mod_state_name[kModnUSER]);

      if (m_userMode->getUserMode() == PixCon::UserMode::kExpert) {
	tim_cat.addVariable(tim_state_name[kLVL1IDcnt]);
	tim_cat.addVariable(tim_state_name[kECRcnt]);
      } // if (m_userMode->getUserMode() == PixCon::UserMode::kExpert)
      tim_cat.addVariable(tim_state_name[kRodBusyCnt]);
      if (m_userMode->getUserMode() == PixCon::UserMode::kExpert) {
	tim_cat.addVariable(tim_state_name[kTType]);
	tim_cat.addVariable(tim_state_name[kBCIDoffset]);
	tim_cat.addVariable(tim_state_name[kTriggerDelay]);
	tim_cat.addVariable(tim_state_name[kTimOkChanges]);
      } // if (m_userMode->getUserMode() == PixCon::UserMode::kExpert)
      tim_cat.addVariable(tim_state_name[kQpllLockChanges]);
      if (m_userMode->getUserMode() == PixCon::UserMode::kExpert) {
	tim_cat.addVariable(tim_state_name[kTTCrxFineDelay]);
	tim_cat.addVariable(tim_state_name[kTTCrxCoarseDelay]);
	tim_cat.addVariable(tim_state_name[kFFTVCounterValue]);
	tim_cat.addVariable(tim_state_name[kFFTVBusyFraction]);
	tim_cat.addVariable(tim_state_name[kFFTV_B_CounterValue]);
  	tim_cat.addVariable(tim_state_name[kFFTV_B_BusyFraction]);     
 
	boc_cat.addVariable(boc_state_name[kClockOk]);
	boc_cat.addVariable(boc_state_name[kInterlocksOk]);
	boc_cat.addVariable(boc_state_name[kLocalInterlock]);
	boc_cat.addVariable(boc_state_name[kRemoteInterlock]);
	boc_cat.addVariable(boc_state_name[kVPinStatusOk]);
	boc_cat.addVariable(boc_state_name[kTempRX]);
	boc_cat.addVariable(boc_state_name[kTempTX]);
	boc_cat.addVariable(boc_state_name[kPiNC_A]);
	boc_cat.addVariable(boc_state_name[kPiNC_B]);
	boc_cat.addVariable(boc_state_name[kPiNC_C]);
	boc_cat.addVariable(boc_state_name[kPiNC_D]);
	boc_cat.addVariable(boc_state_name[kPiNV_A]);
	boc_cat.addVariable(boc_state_name[kPiNV_B]);
	boc_cat.addVariable(boc_state_name[kPiNV_C]);
	boc_cat.addVariable(boc_state_name[kPiNV_D]);
      } // if (m_userMode->getUserMode() == PixCon::UserMode::kExpert)

    }
  }
}
