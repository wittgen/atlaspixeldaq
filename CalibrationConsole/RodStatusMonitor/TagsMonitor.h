// Dear emacs, this is -*-c++-*-
#ifndef _PixCon_TagsMonitor_h_
#define _PixCon_TagsMonitor_h_

#include "IsMonitor.h"
#include <ConfigWrapper/Connectivity.h>

namespace PixCon {

  class TagsMonitor : public SimpleIsMonitorReceiver<std::string, const std::string&>
  {
  public:
    /** Monitor the tags in IS.
     * @param tag_prefix the prefix in front of the tag specifier i.e. "DataTakingConfig-"
     */
    TagsMonitor(std::shared_ptr<IsReceptor> receptor,
		const std::string &tag_prefix);

    void setValue(const std::string &is_name, ::time_t time_stamp, const std::string &value);

    void registerForCleanup(const std::string &is_name, std::map<std::string, std::set<std::string> > &clean_up_var_list);

  private:
    std::string m_tagHeader;
    std::map< std::string, PixA::IConnectivity::EConnTag> m_tagMap;
    std::string m_tag[PixA::IConnectivity::kModConfig+1];
  };

}
#endif
