/**
 * @file ModuleListView.h
 * @brief Print module list in a text box for easy copy to elog
 * @author Tayfun Ince <tayfun.ince@cern.ch>
 * @date 2010/05/19
 */

#ifndef _PixCon_ModuleListView_H_
#define _PixCon_ModuleListView_H_

#include <memory>
#include <memory>
#include <string>

#include "ui_ModuleListViewBase.h"
#include "CalibrationDataManager.h"
#include "CalibrationDataStyle.h"
#include "IConnItemSelection.h"

namespace PixCon {

  class ModuleListView : public QDialog, public Ui_ModuleListViewBase
  {
    Q_OBJECT

  public:
    /**
     * Constructor
     * @param calibration_data Pointer to CalibrationDataManager for loop over all modules
     * @param data_selection Pointer to DataSelection for current selected data
     * @param selection_factory Pointer to IConnItemSelection for check if a module is selected
     */
    ModuleListView(const std::shared_ptr<CalibrationDataManager>& calibration_data,
		   const std::shared_ptr<PixCon::DataSelection> &data_selection,
		   IConnItemSelection* conn_item_selection,
		   QWidget* parent = 0);
    
    /** Destructor */
    ~ModuleListView();

    std::string printString(const std::string& conn_name);
    
  private:
    std::shared_ptr<CalibrationDataManager> m_calibrationData;
    std::shared_ptr<PixCon::DataSelection>  m_dataSelection;
    std::unique_ptr<IConnItemSelection>         m_connItemSelection;
    
  };
}
#endif
