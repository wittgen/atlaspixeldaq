#ifndef UI_DETECTORVIEWBASE_H
#define UI_DETECTORVIEWBASE_H

#include <QGroupBox>
#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QComboBox>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QPushButton>
#include <QSpacerItem>
#include <QVBoxLayout>
#include <QWidget>
#include <QCheckBox>
#include <QLabel>

QT_BEGIN_NAMESPACE

class Ui_DetectorViewBase
{
public:
    QVBoxLayout *vboxLayout;
    QHBoxLayout *hboxLayout;
    QPushButton *m_newWindowButton;
    //QPushButton *m_listViewButton;
    QSpacerItem *spacer2;
    QHBoxLayout *hboxLayout1;
    QPushButton *m_zoomOutButton;
    QPushButton *m_zoomInButton;
    QSpacerItem *spacer2_2;
    QPushButton *m_printButton;
    QPushButton *m_rotateButton;
    QSpacerItem *spacer1;
    QComboBox *m_categorySelector;
    QComboBox *m_serialNumberSelector;
    QComboBox *m_variableSelector;
    QGroupBox *groupBox1;
    QVBoxLayout *vboxLayout1;

    QLabel* showLabel;
    QCheckBox* showCrateCheckbox;
    QCheckBox* showDisksCheckbox;
    QCheckBox* showBarrelCheckbox;
    QCheckBox* showIBLCheckbox;
    QCheckBox* showDBMCheckbox;
    QCheckBox* showPP0Checkbox;
    QCheckBox* showLegendCheckbox;
    QHBoxLayout* botRowCheckboxesLayout;

    // define the setupUi function called in the DetectorView header file 
    void setupUi(QWidget *DetectorViewBase)
    {
        if (DetectorViewBase->objectName().isEmpty())
            DetectorViewBase->setObjectName(QString::fromUtf8("DetectorViewBase")); // This isn't actually needed but it is a relic of Qt3 -> Qt4 conversion.
        DetectorViewBase->resize(750, 480); // Set the window size
        DetectorViewBase->setFocusPolicy(Qt::WheelFocus);
        vboxLayout = new QVBoxLayout(DetectorViewBase); // Define the main layout. Things will be added in descending order.
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(0, 0, 0, 0);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));

        ////////////// Toolbar //////////////
        hboxLayout = new QHBoxLayout(); // A layout specifically for the toolbar
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        m_newWindowButton = new QPushButton(DetectorViewBase); // Defines a new button...
        m_newWindowButton->setObjectName(QString::fromUtf8("m_newWindowButton"));
        m_newWindowButton->setIcon(QIcon(QPixmap(":/icons/images/filenew_2.png"))); // ...with an image.
        m_newWindowButton->setFlat(true);
        hboxLayout->addWidget(m_newWindowButton); // Add this button to the toolbar horizontal layou

        spacer2 = new QSpacerItem(16, 20, QSizePolicy::Fixed, QSizePolicy::Minimum); // set some spacing between first button and the next.
        hboxLayout->addItem(spacer2); // Add the spacing to the horizontal layout

	// create a series of buttons and add them to their own layout (view + and -, rotate, view options)
        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setSpacing(0);
        hboxLayout1->setContentsMargins(0, 0, 0, 0);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        m_zoomOutButton = new QPushButton(DetectorViewBase);
        m_zoomOutButton->setObjectName(QString::fromUtf8("m_zoomOutButton"));
        m_zoomOutButton->setIcon(QIcon(QPixmap(":/icons/images/viewmag-.png")));
        m_zoomOutButton->setFlat(true);
        hboxLayout1->addWidget(m_zoomOutButton);

        m_zoomInButton = new QPushButton(DetectorViewBase);
        m_zoomInButton->setObjectName(QString::fromUtf8("m_zoomInButton"));
        m_zoomInButton->setIcon(QIcon(QPixmap(":/icons/images/viewmag+.png")));
        m_zoomInButton->setFlat(true);
        hboxLayout1->addWidget(m_zoomInButton);

        m_rotateButton = new QPushButton(DetectorViewBase);
        m_rotateButton->setObjectName(QString::fromUtf8("m_rotateButton"));
        m_rotateButton->setMaximumSize(QSize(100, 100));
        m_rotateButton->setIcon(QIcon(QPixmap(":/icons/images/rotate.png")));
        m_rotateButton->setFlat(true);
        hboxLayout->addWidget(m_rotateButton);

        hboxLayout->addLayout(hboxLayout1); // Add the horizontal layout to the parent horizontal layout

        spacer2_2 = new QSpacerItem(16, 20, QSizePolicy::Fixed, QSizePolicy::Minimum); // Create another spacer between buttons in the toolbar...
        hboxLayout->addItem(spacer2_2); // ... and add it to the main horizontal layout

	// Create the file print button and add to the horizontal layout
        m_printButton = new QPushButton(DetectorViewBase);
        m_printButton->setObjectName(QString::fromUtf8("m_printButton"));
        m_printButton->setMaximumSize(QSize(100, 100));
        m_printButton->setIcon(QIcon(QPixmap(":/icons/images/fileprint.png")));
        m_printButton->setFlat(true);
        hboxLayout->addWidget(m_printButton);

        spacer1 = new QSpacerItem(20, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);
        hboxLayout->addItem(spacer1);

        m_categorySelector = new QComboBox(DetectorViewBase);
        m_categorySelector->setObjectName(QString::fromUtf8("m_categorySelector"));
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(3), static_cast<QSizePolicy::Policy>(0));
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(m_categorySelector->sizePolicy().hasHeightForWidth());
        m_categorySelector->setSizePolicy(sizePolicy);
        m_categorySelector->setMinimumSize(QSize(75, 0)); // just some styling options
        hboxLayout->addWidget(m_categorySelector);

        m_serialNumberSelector = new QComboBox(DetectorViewBase);
        m_serialNumberSelector->setObjectName(QString::fromUtf8("m_serialNumberSelector"));
        hboxLayout->addWidget(m_serialNumberSelector); 

        m_variableSelector = new QComboBox(DetectorViewBase);
        m_variableSelector->setObjectName(QString::fromUtf8("m_variableSelector"));
        sizePolicy.setHeightForWidth(m_variableSelector->sizePolicy().hasHeightForWidth());
        m_variableSelector->setSizePolicy(sizePolicy);
        m_variableSelector->setMinimumSize(QSize(80, 0));
        hboxLayout->addWidget(m_variableSelector); // and add them to the H. layout

        vboxLayout->addLayout(hboxLayout); // add the horizonal layout to the topmost position of the vertical layout

        groupBox1 = new QGroupBox(DetectorViewBase);
        vboxLayout1 = new QVBoxLayout();
        vboxLayout1->addStretch(2);
        groupBox1->setLayout(vboxLayout1);

        vboxLayout->addWidget(groupBox1);

        botRowCheckboxesLayout = new QHBoxLayout();
        showLabel = new QLabel("Show: ");
        showCrateCheckbox = new QCheckBox("Crates");
        showCrateCheckbox->setCheckState(Qt::Checked);
        showDisksCheckbox = new QCheckBox("Disks");
        showDisksCheckbox->setCheckState(Qt::Checked);
        showBarrelCheckbox = new QCheckBox("Barrel");
        showBarrelCheckbox->setCheckState(Qt::Checked);
        showIBLCheckbox = new QCheckBox("IBL");
        showIBLCheckbox->setCheckState(Qt::Checked);
        showDBMCheckbox = new QCheckBox("DBM");
        showDBMCheckbox->setCheckState(Qt::Checked);
        showPP0Checkbox = new QCheckBox("PP0 Zoom");
        showPP0Checkbox->setCheckState(Qt::Checked);
        showLegendCheckbox = new QCheckBox("Legend");
        showLegendCheckbox->setCheckState(Qt::Checked);

        botRowCheckboxesLayout->addWidget(showLabel);
        botRowCheckboxesLayout->addWidget(showCrateCheckbox);
        botRowCheckboxesLayout->addWidget(showDisksCheckbox);
        botRowCheckboxesLayout->addWidget(showBarrelCheckbox);
        botRowCheckboxesLayout->addWidget(showIBLCheckbox);
        botRowCheckboxesLayout->addWidget(showDBMCheckbox);
        botRowCheckboxesLayout->addWidget(showPP0Checkbox);
        botRowCheckboxesLayout->addWidget(showLegendCheckbox);

        vboxLayout->addLayout(botRowCheckboxesLayout);
	
	// connect verious clicked signals to various custom slots that are defined in .h and .cxx files. Note, you don't have to do it here, but its a good way to organise code. 
        retranslateUi(DetectorViewBase);
        QObject::connect(m_categorySelector, SIGNAL(activated(QString)), DetectorViewBase, SLOT(changedCategory(QString)));
        QObject::connect(m_variableSelector, SIGNAL(activated(QString)), DetectorViewBase, SLOT(changedVariable(QString)));
        QObject::connect(m_serialNumberSelector, SIGNAL(activated(QString)), DetectorViewBase, SLOT(changedSerialNumber(QString)));
        QObject::connect(m_newWindowButton, SIGNAL(clicked()), DetectorViewBase, SLOT(cloneView()));
        QObject::connect(m_zoomInButton, SIGNAL(clicked()), DetectorViewBase, SLOT(zoomIn()));
        QObject::connect(m_zoomOutButton, SIGNAL(clicked()), DetectorViewBase, SLOT(zoomOut()));
        QObject::connect(m_rotateButton, SIGNAL(clicked()), DetectorViewBase, SLOT(rotateView()));
        QObject::connect(m_printButton, SIGNAL(clicked()), DetectorViewBase, SLOT(printToFile())); // i.e.: Connect the print button widget, so that when pressed, it calls the printToFile() function defined in the cxx file.        
        QObject::connect(showCrateCheckbox, SIGNAL(stateChanged(int)), DetectorViewBase, SLOT(drawDetectorModel()));
        QObject::connect(showDisksCheckbox, SIGNAL(stateChanged(int)), DetectorViewBase, SLOT(drawDetectorModel()));
        QObject::connect(showBarrelCheckbox, SIGNAL(stateChanged(int)), DetectorViewBase, SLOT(drawDetectorModel()));
        QObject::connect(showIBLCheckbox, SIGNAL(stateChanged(int)), DetectorViewBase, SLOT(drawDetectorModel()));
        QObject::connect(showDBMCheckbox, SIGNAL(stateChanged(int)), DetectorViewBase, SLOT(drawDetectorModel()));
        QObject::connect(showPP0Checkbox, SIGNAL(stateChanged(int)), DetectorViewBase, SLOT(drawDetectorModel()));
        QObject::connect(showLegendCheckbox, SIGNAL(stateChanged(int)), DetectorViewBase, SLOT(drawDetectorModel()));

        QMetaObject::connectSlotsByName(DetectorViewBase);
    } // setupUi

    // some qt magic that sets further options.
    void retranslateUi(QWidget *DetectorViewBase){
        DetectorViewBase->setWindowTitle(QApplication::translate("DetectorViewBase", "DetectorView", 0));
        m_newWindowButton->setText(QString());
        m_zoomOutButton->setText(QString());
        m_zoomInButton->setText(QString());
        m_printButton->setText(QString());
        m_rotateButton->setText(QString());
#ifndef QT_NO_TOOLTIP
        DetectorViewBase->setProperty("toolTip", QVariant(QString()));
        m_newWindowButton->setProperty("toolTip", QVariant(QApplication::translate("DetectorViewBase", "New Window", 0)));
        m_zoomOutButton->setProperty("toolTip", QVariant(QApplication::translate("DetectorViewBase", "Zoom out", 0)));
        m_zoomInButton->setProperty("toolTip", QVariant(QApplication::translate("DetectorViewBase", "Zoom in", 0)));
        m_printButton->setProperty("toolTip", QVariant(QApplication::translate("DetectorViewBase", "Print detector canvas to file.", 0)));
        m_rotateButton->setProperty("toolTip", QVariant(QApplication::translate("DetectorViewBase", "Rotate view.", 0)));
        m_categorySelector->setProperty("toolTip", QVariant(QApplication::translate("DetectorViewBase", "Select the variable category", 0)));
        m_serialNumberSelector->setProperty("toolTip", QVariant(QApplication::translate("DetectorViewBase", "Choose a certain scan, analysis or the current values", 0)));
        m_variableSelector->setProperty("toolTip", QVariant(QApplication::translate("DetectorViewBase", "Variable to be shown", 0)));
#endif // QT_NO_TOOLTIP
        groupBox1->setTitle(QString());
    } // retranslateUi

};

namespace Ui {
    class DetectorViewBase: public Ui_DetectorViewBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DETECTORVIEWBASE_H
