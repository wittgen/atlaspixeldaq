// Dear emacs, this is -*-c++-*-
// ... thanks to Andrei

#ifndef _IContextMenu_H_
#define _IContextMenu_H_

#include <string>
#include <ConnItem.h>
#include <qobject.h>

#include <CalibrationDataTypes.h>

namespace PixCon {

  class IContextMenu : public QObject
  {
    Q_OBJECT

  public:

    IContextMenu(QObject * parent = 0 ) :  QObject(parent) {}

    virtual ~IContextMenu() {}

  public slots:
    /** Action activated on double click.
     */
    virtual void primaryAction(PixCon::EConnItemType type, const std::string &connectivity_name) = 0;

    /** Action activated on middle click.
     */
    virtual void secondaryAction(PixCon::EConnItemType type, const std::string &connectivity_name) = 0;

    /** Show menu with all actions.
     */
    virtual void showMenu(PixCon::EConnItemType type, const std::string &connectivity_name) = 0;
  };

}
#endif

