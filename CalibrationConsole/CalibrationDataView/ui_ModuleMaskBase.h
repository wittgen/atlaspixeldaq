#ifndef UI_MODULEMASKBASE_H
#define UI_MODULEMASKBASE_H

#include <QGroupBox>
#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QPushButton>
#include <QRadioButton>
#include <QSpacerItem>
#include <QVBoxLayout>
#include <QWidget>
#include <QTreeWidget>
#include <QTreeWidgetItem>
QT_BEGIN_NAMESPACE

class Ui_ModuleMaskBase
{
public:
    QVBoxLayout *vboxLayout;
    QHBoxLayout *hboxLayout;
    QPushButton *m_loadButton;
    QPushButton *m_deleteButton;
    //Q3ListView *m_maskList;
    QTreeWidget *m_maskList;//
    QTreeWidgetItem *m_maskListHeader;//
    QHBoxLayout *hboxLayout1;
    QPushButton *m_decreaseLevelButton;
    QPushButton *m_increaseLevelButton;
    QGroupBox *buttonGroup2;
    QVBoxLayout *vboxLayout1;
    QVBoxLayout *vboxLayout2;
    QRadioButton *m_filterDisabledButton;
    QLabel *textLabel2;
    QRadioButton *m_filterEnabledButton;
    QHBoxLayout *hboxLayout2;
    QPushButton *m_clearFilterButton;
    QPushButton *m_applyFilterButton;
    QHBoxLayout *hboxLayout3;

    void setupUi(QWidget *ModuleMaskBase)
    {
        if (ModuleMaskBase->objectName().isEmpty())
            ModuleMaskBase->setObjectName(QString::fromUtf8("ModuleMaskBase"));
        ModuleMaskBase->resize(382, 431);
        vboxLayout = new QVBoxLayout(ModuleMaskBase);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        m_loadButton = new QPushButton(ModuleMaskBase);
        m_loadButton->setObjectName(QString::fromUtf8("m_loadButton"));

        hboxLayout->addWidget(m_loadButton);

        m_deleteButton = new QPushButton(ModuleMaskBase);
        m_deleteButton->setObjectName(QString::fromUtf8("m_deleteButton"));

        hboxLayout->addWidget(m_deleteButton);


        vboxLayout->addLayout(hboxLayout);
/*
        m_maskList = new Q3ListView(ModuleMaskBase);
        m_maskList->addColumn(QApplication::translate("ModuleMaskBase", "Name", 0));
        m_maskList->header()->setClickEnabled(true, m_maskList->header()->count() - 1);
        m_maskList->header()->setResizeEnabled(true, m_maskList->header()->count() - 1);
        m_maskList->addColumn(QApplication::translate("ModuleMaskBase", "Revision", 0));
        m_maskList->header()->setClickEnabled(true, m_maskList->header()->count() - 1);
        m_maskList->header()->setResizeEnabled(true, m_maskList->header()->count() - 1);
        m_maskList->addColumn(QApplication::translate("ModuleMaskBase", "Active", 0));
        m_maskList->header()->setClickEnabled(true, m_maskList->header()->count() - 1);
        m_maskList->header()->setResizeEnabled(true, m_maskList->header()->count() - 1);
        m_maskList->addColumn(QApplication::translate("ModuleMaskBase", "Modules", 0));
        m_maskList->header()->setClickEnabled(true, m_maskList->header()->count() - 1);
        m_maskList->header()->setResizeEnabled(true, m_maskList->header()->count() - 1);
        m_maskList->addColumn(QApplication::translate("ModuleMaskBase", "Colour", 0));
        m_maskList->header()->setClickEnabled(true, m_maskList->header()->count() - 1);
        m_maskList->header()->setResizeEnabled(true, m_maskList->header()->count() - 1);
        m_maskList->setObjectName(QString::fromUtf8("m_maskList"));
*/
        m_maskList = new QTreeWidget(ModuleMaskBase);//
        m_maskList->setColumnCount(4);
        m_maskListHeader = new QTreeWidgetItem;
        m_maskListHeader->setText(0,"Name");
        m_maskList->header()->resizeSection(0,195);
        m_maskListHeader->setText(1,"Revision");
        m_maskListHeader->setText(2,"Active");
        m_maskListHeader->setText(3,"Colour");
        m_maskList->setHeaderItem(m_maskListHeader);

        vboxLayout->addWidget(m_maskList);

        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setSpacing(6);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        m_decreaseLevelButton = new QPushButton(ModuleMaskBase);
        m_decreaseLevelButton->setObjectName(QString::fromUtf8("m_decreaseLevelButton"));
        const QIcon icon = QIcon(QPixmap(":/icons/images/down.png"));
        m_decreaseLevelButton->setIcon(icon);

        hboxLayout1->addWidget(m_decreaseLevelButton);

        m_increaseLevelButton = new QPushButton(ModuleMaskBase);
        m_increaseLevelButton->setObjectName(QString::fromUtf8("m_increaseLevelButton"));
        const QIcon icon1 = QIcon(QPixmap(":/icons/images/up.png"));
        m_increaseLevelButton->setIcon(icon1);

        hboxLayout1->addWidget(m_increaseLevelButton);


        vboxLayout->addLayout(hboxLayout1);

        //// Stuff that goes into GroupBox ////
        buttonGroup2 = new QGroupBox(ModuleMaskBase);
        vboxLayout1 = new QVBoxLayout;
        hboxLayout2 = new QHBoxLayout;
        textLabel2 = new QLabel(buttonGroup2);
        vboxLayout2 = new QVBoxLayout;
        m_filterEnabledButton = new QRadioButton(buttonGroup2);
        m_filterDisabledButton = new QRadioButton(buttonGroup2);
        m_filterDisabledButton->setChecked(true);
        vboxLayout2->addWidget(m_filterDisabledButton);
        vboxLayout2->addWidget(m_filterEnabledButton);
        hboxLayout2->addWidget(textLabel2);
        hboxLayout2->addLayout(vboxLayout2);
        vboxLayout1->addLayout(hboxLayout2);
        hboxLayout3 = new QHBoxLayout;
	QSpacerItem *spacerLB = new QSpacerItem(41, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
        m_clearFilterButton = new QPushButton(buttonGroup2);
	QSpacerItem *spacerMB = new QSpacerItem(41, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
        m_applyFilterButton = new QPushButton(buttonGroup2);
	QSpacerItem *spacerRB = new QSpacerItem(41, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
	hboxLayout3->addItem(spacerLB);
        hboxLayout3->addWidget(m_clearFilterButton);
	hboxLayout3->addItem(spacerMB);
        hboxLayout3->addWidget(m_applyFilterButton);
	hboxLayout3->addItem(spacerRB);
        vboxLayout1->addLayout(hboxLayout3);
        buttonGroup2->setLayout(vboxLayout1);
        vboxLayout->addWidget(buttonGroup2);
        ///////////////////

        retranslateUi(ModuleMaskBase);

        QMetaObject::connectSlotsByName(ModuleMaskBase);
    } // setupUi

    void retranslateUi(QWidget *ModuleMaskBase)
    {
        ModuleMaskBase->setWindowTitle(QApplication::translate("ModuleMaskBase", "Module Mask", 0));
        m_loadButton->setText(QApplication::translate("ModuleMaskBase", "Load", 0));
        m_deleteButton->setText(QApplication::translate("ModuleMaskBase", "Delete", 0));
        /*
        m_maskList->header()->setLabel(0, QApplication::translate("ModuleMaskBase", "Name", 0));
        m_maskList->header()->setLabel(1, QApplication::translate("ModuleMaskBase", "Revision", 0));
        m_maskList->header()->setLabel(2, QApplication::translate("ModuleMaskBase", "Active", 0));
        m_maskList->header()->setLabel(3, QApplication::translate("ModuleMaskBase", "Modules", 0));
        m_maskList->header()->setLabel(4, QApplication::translate("ModuleMaskBase", "Colour", 0));
        */
        m_decreaseLevelButton->setText(QString());
        m_increaseLevelButton->setText(QString());
        buttonGroup2->setTitle(QApplication::translate("ModuleMaskBase", "Filter", 0));
        m_filterDisabledButton->setText(QApplication::translate("ModuleMaskBase", "Masked out", 0));
        textLabel2->setText(QApplication::translate("ModuleMaskBase", "Filter objects which are :", 0));
        m_filterEnabledButton->setText(QApplication::translate("ModuleMaskBase", "Included", 0));
        m_clearFilterButton->setText(QApplication::translate("ModuleMaskBase", "Clear", 0));
        m_applyFilterButton->setText(QApplication::translate("ModuleMaskBase", "Apply", 0));
    } // retranslateUi

};

namespace Ui {
    class ModuleMaskBase: public Ui_ModuleMaskBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MODULEMASKBASE_H
