#ifndef UI_SAVEDISABLEBASE_H
#define UI_SAVEDISABLEBASE_H

#include <QGroupBox>
#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QCheckBox>
#include <QComboBox>
#include <QDialog>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QSpacerItem>
#include <QVBoxLayout>


QT_BEGIN_NAMESPACE

class Ui_SaveDisableBase
{
public:
    QVBoxLayout *vboxLayout;
    QLabel *m_title;
    QHBoxLayout *hboxLayout;
    QLabel *textLabel2;
    QComboBox *m_disableNameSelector;
    QHBoxLayout *hboxLayout1;
    QLabel *textLabel3;
    QLineEdit *m_idTag;
    QLabel *textLabel4;
    QLineEdit *m_tag;
    QCheckBox *m_tmpButton;
    QGroupBox *buttonGroup3;
    QVBoxLayout *vboxLayout1;
    QCheckBox *m_passedFilterOnly;
    QCheckBox *m_enabledObjectsOnly;
    QCheckBox *m_ignoreTim;
    QHBoxLayout *hboxLayout2;
    QSpacerItem *spacer21;
    QPushButton *m_cancelButton;
    QSpacerItem *spacer23;
    QPushButton *m_saveButton;
    QSpacerItem *spacer22;

    void setupUi(QDialog *SaveDisableBase)
    {
        if (SaveDisableBase->objectName().isEmpty())
            SaveDisableBase->setObjectName(QString::fromUtf8("SaveDisableBase"));
        SaveDisableBase->resize(513, 321);
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(5), static_cast<QSizePolicy::Policy>(1));
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(SaveDisableBase->sizePolicy().hasHeightForWidth());
        SaveDisableBase->setSizePolicy(sizePolicy);
        vboxLayout = new QVBoxLayout(SaveDisableBase);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        vboxLayout->setSizeConstraint(QLayout::SetMinimumSize);
        m_title = new QLabel(SaveDisableBase);
        m_title->setObjectName(QString::fromUtf8("m_title"));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        m_title->setFont(font);
        m_title->setMargin(10);
        m_title->setWordWrap(false);

        vboxLayout->addWidget(m_title);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        textLabel2 = new QLabel(SaveDisableBase);
        textLabel2->setObjectName(QString::fromUtf8("textLabel2"));
        textLabel2->setWordWrap(false);

        hboxLayout->addWidget(textLabel2);

        m_disableNameSelector = new QComboBox(SaveDisableBase);
        m_disableNameSelector->setObjectName(QString::fromUtf8("m_disableNameSelector"));
        QSizePolicy sizePolicy1(static_cast<QSizePolicy::Policy>(7), static_cast<QSizePolicy::Policy>(0));
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(m_disableNameSelector->sizePolicy().hasHeightForWidth());
        m_disableNameSelector->setSizePolicy(sizePolicy1);
        m_disableNameSelector->setEditable(true);
        m_disableNameSelector->setAutoCompletion(true);
        m_disableNameSelector->setDuplicatesEnabled(false);

        hboxLayout->addWidget(m_disableNameSelector);


        vboxLayout->addLayout(hboxLayout);

        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setSpacing(6);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        textLabel3 = new QLabel(SaveDisableBase);
        textLabel3->setObjectName(QString::fromUtf8("textLabel3"));
        textLabel3->setWordWrap(false);

        hboxLayout1->addWidget(textLabel3);

        m_idTag = new QLineEdit(SaveDisableBase);
        m_idTag->setObjectName(QString::fromUtf8("m_idTag"));
        m_idTag->setReadOnly(true);

        hboxLayout1->addWidget(m_idTag);

        textLabel4 = new QLabel(SaveDisableBase);
        textLabel4->setObjectName(QString::fromUtf8("textLabel4"));
        textLabel4->setWordWrap(false);

        hboxLayout1->addWidget(textLabel4);

        m_tag = new QLineEdit(SaveDisableBase);
        m_tag->setObjectName(QString::fromUtf8("m_tag"));
        m_tag->setReadOnly(true);

        hboxLayout1->addWidget(m_tag);

        m_tmpButton = new QCheckBox(SaveDisableBase);
        m_tmpButton->setObjectName(QString::fromUtf8("m_tmpButton"));
        m_tmpButton->setChecked(false);

        hboxLayout1->addWidget(m_tmpButton);


        vboxLayout->addLayout(hboxLayout1);

        buttonGroup3 = new QGroupBox(SaveDisableBase);
        buttonGroup3->setObjectName(QString::fromUtf8("buttonGroup3"));
        QSizePolicy sizePolicy2(static_cast<QSizePolicy::Policy>(5), static_cast<QSizePolicy::Policy>(5));
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(buttonGroup3->sizePolicy().hasHeightForWidth());
        buttonGroup3->setSizePolicy(sizePolicy2);
        //buttonGroup3->setColumnLayout(0, Qt::Vertical);
	new QVBoxLayout(buttonGroup3);
        buttonGroup3->layout()->setSpacing(6);
        buttonGroup3->layout()->setContentsMargins(11, 11, 11, 11);
        vboxLayout1 = new QVBoxLayout();
        QBoxLayout *boxlayout = qobject_cast<QBoxLayout *>(buttonGroup3->layout());
        if (boxlayout)
            boxlayout->addLayout(vboxLayout1);
        vboxLayout1->setAlignment(Qt::AlignTop);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        m_passedFilterOnly = new QCheckBox(buttonGroup3);
        m_passedFilterOnly->setObjectName(QString::fromUtf8("m_passedFilterOnly"));
        m_passedFilterOnly->setChecked(false);

        vboxLayout1->addWidget(m_passedFilterOnly);

        m_enabledObjectsOnly = new QCheckBox(buttonGroup3);
        m_enabledObjectsOnly->setObjectName(QString::fromUtf8("m_enabledObjectsOnly"));
        m_enabledObjectsOnly->setChecked(true);

        vboxLayout1->addWidget(m_enabledObjectsOnly);

        m_ignoreTim = new QCheckBox(buttonGroup3);
        m_ignoreTim->setObjectName(QString::fromUtf8("m_ignoreTim"));
        m_ignoreTim->setChecked(true);

        vboxLayout1->addWidget(m_ignoreTim);


        vboxLayout->addWidget(buttonGroup3);

        hboxLayout2 = new QHBoxLayout();
        hboxLayout2->setSpacing(6);
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        spacer21 = new QSpacerItem(31, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout2->addItem(spacer21);

        m_cancelButton = new QPushButton(SaveDisableBase);
        m_cancelButton->setObjectName(QString::fromUtf8("m_cancelButton"));

        hboxLayout2->addWidget(m_cancelButton);

        spacer23 = new QSpacerItem(61, 31, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout2->addItem(spacer23);

        m_saveButton = new QPushButton(SaveDisableBase);
        m_saveButton->setObjectName(QString::fromUtf8("m_saveButton"));
        m_saveButton->setDefault(true);

        hboxLayout2->addWidget(m_saveButton);

        spacer22 = new QSpacerItem(41, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout2->addItem(spacer22);


        vboxLayout->addLayout(hboxLayout2);


        retranslateUi(SaveDisableBase);
        QObject::connect(m_cancelButton, SIGNAL(clicked()), SaveDisableBase, SLOT(reject()));
        QObject::connect(m_saveButton, SIGNAL(clicked()), SaveDisableBase, SLOT(accept()));

        QMetaObject::connectSlotsByName(SaveDisableBase);
    } // setupUi

    void retranslateUi(QDialog *SaveDisableBase)
    {
        SaveDisableBase->setWindowTitle(QApplication::translate("SaveDisableBase", "Form1", 0));
        m_title->setText(QApplication::translate("SaveDisableBase", "Save PixDisable from selected objects", 0));
        textLabel2->setText(QApplication::translate("SaveDisableBase", "Name :", 0));
        textLabel3->setText(QApplication::translate("SaveDisableBase", "Id tag :", 0));
        textLabel4->setText(QApplication::translate("SaveDisableBase", "tag :", 0));
        m_tmpButton->setText(QApplication::translate("SaveDisableBase", "Temporary", 0));
        buttonGroup3->setTitle(QApplication::translate("SaveDisableBase", "Object Selection", 0));
        m_passedFilterOnly->setText(QApplication::translate("SaveDisableBase", "Add all objects to PixDisable which do not pass the filter.", 0));
        m_enabledObjectsOnly->setText(QApplication::translate("SaveDisableBase", "Keep all disabled objects disabled.", 0));
        m_ignoreTim->setText(QApplication::translate("SaveDisableBase", "Do not add TIMs to the PixDisable.", 0));
        m_cancelButton->setText(QApplication::translate("SaveDisableBase", "Cancel", 0));
        m_saveButton->setText(QApplication::translate("SaveDisableBase", "Save", 0));
    } // retranslateUi

};

namespace Ui {
    class SaveDisableBase: public Ui_SaveDisableBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SAVEDISABLEBASE_H
