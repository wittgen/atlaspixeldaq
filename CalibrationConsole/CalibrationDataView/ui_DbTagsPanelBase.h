#ifndef UI_DBTAGSPANELBASE_H
#define UI_DBTAGSPANELBASE_H

#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QComboBox>
#include <QDialog>
#include <QFrame>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QSpacerItem>
#include <QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_DbTagsPanelBase
{
public:
    QVBoxLayout *vboxLayout;
    QHBoxLayout *hboxLayout;
    QVBoxLayout *vboxLayout1;
    QLabel *TagOI_label;
    QComboBox *comboBoxTagOI;
    QSpacerItem *spacer5;
    QSpacerItem *spacer6;
    QSpacerItem *spacer7;
    QVBoxLayout *vboxLayout2;
    QLabel *TagCFOI_label;
    QComboBox *comboBoxTagCfgOI;
    QSpacerItem *spacer8_2_2;
    QHBoxLayout *hboxLayout1;
    QVBoxLayout *vboxLayout3;
    QVBoxLayout *vboxLayout4;
    QLabel *TagC_label;
    QComboBox *comboBoxTagConn;
    QSpacerItem *spacer8_3;
    QSpacerItem *spacer7_2;
    QVBoxLayout *vboxLayout5;
    QHBoxLayout *hboxLayout2;
    QVBoxLayout *vboxLayout6;
    QLabel *TagCF_label;
    QComboBox *comboBoxTagCfg;
    QVBoxLayout *vboxLayout7;
    QLabel *TagModCF_label;
    QComboBox *comboBoxTagModCfg;
    QSpacerItem *spacer8_2;
    QHBoxLayout *hboxLayout3;
    QLabel *textLabel1;
    QLineEdit *m_revisionEntry;
    QSpacerItem *spacer9;
    QFrame *line1;
    QHBoxLayout *hboxLayout4;
    QPushButton *CancelButton;
    QSpacerItem *spacer8;
    QPushButton *OKButton;

    void setupUi(QDialog *DbTagsPanelBase)
    {
        if (DbTagsPanelBase->objectName().isEmpty())
            DbTagsPanelBase->setObjectName(QString::fromUtf8("DbTagsPanelBase"));
        DbTagsPanelBase->resize(631, 281);
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(5), static_cast<QSizePolicy::Policy>(1));
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(DbTagsPanelBase->sizePolicy().hasHeightForWidth());
        DbTagsPanelBase->setSizePolicy(sizePolicy);
        vboxLayout = new QVBoxLayout(DbTagsPanelBase);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        vboxLayout1 = new QVBoxLayout();
        vboxLayout1->setSpacing(6);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        TagOI_label = new QLabel(DbTagsPanelBase);
        TagOI_label->setObjectName(QString::fromUtf8("TagOI_label"));
        TagOI_label->setAlignment(Qt::AlignCenter);
        TagOI_label->setWordWrap(false);

        vboxLayout1->addWidget(TagOI_label);

        comboBoxTagOI = new QComboBox(DbTagsPanelBase);
        comboBoxTagOI->setObjectName(QString::fromUtf8("comboBoxTagOI"));

        vboxLayout1->addWidget(comboBoxTagOI);


        hboxLayout->addLayout(vboxLayout1);

        spacer5 = new QSpacerItem(0, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacer5);

        spacer6 = new QSpacerItem(0, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacer6);

        spacer7 = new QSpacerItem(0, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacer7);

        vboxLayout2 = new QVBoxLayout();
        vboxLayout2->setSpacing(6);
        vboxLayout2->setObjectName(QString::fromUtf8("vboxLayout2"));
        TagCFOI_label = new QLabel(DbTagsPanelBase);
        TagCFOI_label->setObjectName(QString::fromUtf8("TagCFOI_label"));
        sizePolicy.setHeightForWidth(TagCFOI_label->sizePolicy().hasHeightForWidth());
        TagCFOI_label->setSizePolicy(sizePolicy);
        TagCFOI_label->setAlignment(Qt::AlignCenter);
        TagCFOI_label->setWordWrap(false);

        vboxLayout2->addWidget(TagCFOI_label);

        comboBoxTagCfgOI = new QComboBox(DbTagsPanelBase);
        comboBoxTagCfgOI->setObjectName(QString::fromUtf8("comboBoxTagCfgOI"));

        vboxLayout2->addWidget(comboBoxTagCfgOI);


        hboxLayout->addLayout(vboxLayout2);


        vboxLayout->addLayout(hboxLayout);

        spacer8_2_2 = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Fixed);

        vboxLayout->addItem(spacer8_2_2);

        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setSpacing(6);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        vboxLayout3 = new QVBoxLayout();
        vboxLayout3->setSpacing(6);
        vboxLayout3->setObjectName(QString::fromUtf8("vboxLayout3"));
        vboxLayout4 = new QVBoxLayout();
        vboxLayout4->setSpacing(6);
        vboxLayout4->setObjectName(QString::fromUtf8("vboxLayout4"));
        TagC_label = new QLabel(DbTagsPanelBase);
        TagC_label->setObjectName(QString::fromUtf8("TagC_label"));
        sizePolicy.setHeightForWidth(TagC_label->sizePolicy().hasHeightForWidth());
        TagC_label->setSizePolicy(sizePolicy);
        TagC_label->setAlignment(Qt::AlignCenter);
        TagC_label->setWordWrap(false);

        vboxLayout4->addWidget(TagC_label);

        comboBoxTagConn = new QComboBox(DbTagsPanelBase);
        comboBoxTagConn->setObjectName(QString::fromUtf8("comboBoxTagConn"));
        QSizePolicy sizePolicy1(static_cast<QSizePolicy::Policy>(3), static_cast<QSizePolicy::Policy>(0));
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(comboBoxTagConn->sizePolicy().hasHeightForWidth());
        comboBoxTagConn->setSizePolicy(sizePolicy1);

        vboxLayout4->addWidget(comboBoxTagConn);


        vboxLayout3->addLayout(vboxLayout4);

        spacer8_3 = new QSpacerItem(20, 31, QSizePolicy::Minimum, QSizePolicy::Minimum);

        vboxLayout3->addItem(spacer8_3);


        hboxLayout1->addLayout(vboxLayout3);

        spacer7_2 = new QSpacerItem(70, 21, QSizePolicy::Minimum, QSizePolicy::Minimum);

        hboxLayout1->addItem(spacer7_2);

        vboxLayout5 = new QVBoxLayout();
        vboxLayout5->setSpacing(6);
        vboxLayout5->setObjectName(QString::fromUtf8("vboxLayout5"));
        hboxLayout2 = new QHBoxLayout();
        hboxLayout2->setSpacing(6);
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        vboxLayout6 = new QVBoxLayout();
        vboxLayout6->setSpacing(6);
        vboxLayout6->setObjectName(QString::fromUtf8("vboxLayout6"));
        TagCF_label = new QLabel(DbTagsPanelBase);
        TagCF_label->setObjectName(QString::fromUtf8("TagCF_label"));
        sizePolicy.setHeightForWidth(TagCF_label->sizePolicy().hasHeightForWidth());
        TagCF_label->setSizePolicy(sizePolicy);
        TagCF_label->setAlignment(Qt::AlignCenter);
        TagCF_label->setWordWrap(false);

        vboxLayout6->addWidget(TagCF_label);

        comboBoxTagCfg = new QComboBox(DbTagsPanelBase);
        comboBoxTagCfg->setObjectName(QString::fromUtf8("comboBoxTagCfg"));
        sizePolicy1.setHeightForWidth(comboBoxTagCfg->sizePolicy().hasHeightForWidth());
        comboBoxTagCfg->setSizePolicy(sizePolicy1);

        vboxLayout6->addWidget(comboBoxTagCfg);


        hboxLayout2->addLayout(vboxLayout6);

        vboxLayout7 = new QVBoxLayout();
        vboxLayout7->setSpacing(6);
        vboxLayout7->setObjectName(QString::fromUtf8("vboxLayout7"));
        TagModCF_label = new QLabel(DbTagsPanelBase);
        TagModCF_label->setObjectName(QString::fromUtf8("TagModCF_label"));
        sizePolicy.setHeightForWidth(TagModCF_label->sizePolicy().hasHeightForWidth());
        TagModCF_label->setSizePolicy(sizePolicy);
        TagModCF_label->setAlignment(Qt::AlignCenter);
        TagModCF_label->setWordWrap(false);

        vboxLayout7->addWidget(TagModCF_label);

        comboBoxTagModCfg = new QComboBox(DbTagsPanelBase);
        comboBoxTagModCfg->setObjectName(QString::fromUtf8("comboBoxTagModCfg"));
        sizePolicy1.setHeightForWidth(comboBoxTagModCfg->sizePolicy().hasHeightForWidth());
        comboBoxTagModCfg->setSizePolicy(sizePolicy1);

        vboxLayout7->addWidget(comboBoxTagModCfg);


        hboxLayout2->addLayout(vboxLayout7);


        vboxLayout5->addLayout(hboxLayout2);

        spacer8_2 = new QSpacerItem(20, 16, QSizePolicy::Minimum, QSizePolicy::Fixed);

        vboxLayout5->addItem(spacer8_2);

        hboxLayout3 = new QHBoxLayout();
        hboxLayout3->setSpacing(6);
        hboxLayout3->setObjectName(QString::fromUtf8("hboxLayout3"));
        textLabel1 = new QLabel(DbTagsPanelBase);
        textLabel1->setObjectName(QString::fromUtf8("textLabel1"));
        textLabel1->setWordWrap(false);

        hboxLayout3->addWidget(textLabel1);

        m_revisionEntry = new QLineEdit(DbTagsPanelBase);
        m_revisionEntry->setObjectName(QString::fromUtf8("m_revisionEntry"));

        hboxLayout3->addWidget(m_revisionEntry);


        vboxLayout5->addLayout(hboxLayout3);


        hboxLayout1->addLayout(vboxLayout5);


        vboxLayout->addLayout(hboxLayout1);

        spacer9 = new QSpacerItem(20, 16, QSizePolicy::Minimum, QSizePolicy::Fixed);

        vboxLayout->addItem(spacer9);

        line1 = new QFrame(DbTagsPanelBase);
        line1->setObjectName(QString::fromUtf8("line1"));
        line1->setFrameShape(QFrame::HLine);
        line1->setFrameShadow(QFrame::Sunken);

        vboxLayout->addWidget(line1);

        hboxLayout4 = new QHBoxLayout();
        hboxLayout4->setSpacing(6);
        hboxLayout4->setObjectName(QString::fromUtf8("hboxLayout4"));
        CancelButton = new QPushButton(DbTagsPanelBase);
        CancelButton->setObjectName(QString::fromUtf8("CancelButton"));

        hboxLayout4->addWidget(CancelButton);

        spacer8 = new QSpacerItem(201, 31, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout4->addItem(spacer8);

        OKButton = new QPushButton(DbTagsPanelBase);
        OKButton->setObjectName(QString::fromUtf8("OKButton"));
        OKButton->setDefault(true);

        hboxLayout4->addWidget(OKButton);


        vboxLayout->addLayout(hboxLayout4);


        retranslateUi(DbTagsPanelBase);
        QObject::connect(CancelButton, SIGNAL(clicked()), DbTagsPanelBase, SLOT(reject()));
        QObject::connect(comboBoxTagConn, SIGNAL(activated(QString)), DbTagsPanelBase, SLOT(synchronise(QString)));
        QObject::connect(OKButton, SIGNAL(clicked()), DbTagsPanelBase, SLOT(accept()));
        QObject::connect(comboBoxTagOI, SIGNAL(activated(QString)), DbTagsPanelBase, SLOT(changeObjectTag(QString)));
        QObject::connect(comboBoxTagCfgOI, SIGNAL(activated(QString)), DbTagsPanelBase, SLOT(changeCfgObjectTag(QString)));

        QMetaObject::connectSlotsByName(DbTagsPanelBase);
    } // setupUi

    void retranslateUi(QDialog *DbTagsPanelBase)
    {
        DbTagsPanelBase->setWindowTitle(QApplication::translate("DbTagsPanelBase", "DB tags", 0));
        TagOI_label->setText(QApplication::translate("DbTagsPanelBase", "Object Tag:", 0));
        comboBoxTagOI->clear();
        comboBoxTagOI->insertItems(0, QStringList()
         << QApplication::translate("DbTagsPanelBase", "SystemTest-Preliminary", 0)
        );
        TagCFOI_label->setText(QApplication::translate("DbTagsPanelBase", "Config Object tag:", 0));
        comboBoxTagCfgOI->clear();
        comboBoxTagCfgOI->insertItems(0, QStringList()
         << QApplication::translate("DbTagsPanelBase", "SystemTest-Preliminary", 0)
        );
        TagC_label->setText(QApplication::translate("DbTagsPanelBase", "Connectivity Tag:", 0));
        comboBoxTagConn->clear();
        comboBoxTagConn->insertItems(0, QStringList()
         << QApplication::translate("DbTagsPanelBase", "SystemTest-Preliminary", 0)
        );
        TagCF_label->setText(QApplication::translate("DbTagsPanelBase", "Config Tag:", 0));
        comboBoxTagCfg->clear();
        comboBoxTagCfg->insertItems(0, QStringList()
         << QApplication::translate("DbTagsPanelBase", "SystemTest-Preliminary", 0)
        );
        TagModCF_label->setText(QApplication::translate("DbTagsPanelBase", "Module Config Tag:", 0));
        comboBoxTagModCfg->clear();
        comboBoxTagModCfg->insertItems(0, QStringList()
         << QApplication::translate("DbTagsPanelBase", "SystemTest-Preliminary", 0)
        );
        textLabel1->setText(QApplication::translate("DbTagsPanelBase", "Revision :", 0));
#ifndef QT_NO_TOOLTIP
        m_revisionEntry->setProperty("toolTip", QVariant(QApplication::translate("DbTagsPanelBase", "Unix time stamp or date e.g. 2008-11-23 18:23:00", 0)));
#endif // QT_NO_TOOLTIP
        CancelButton->setText(QApplication::translate("DbTagsPanelBase", "Cancel", 0));
        OKButton->setText(QApplication::translate("DbTagsPanelBase", "OK", 0));
    } // retranslateUi

};

namespace Ui {
    class DbTagsPanelBase: public Ui_DbTagsPanelBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DBTAGSPANELBASE_H
