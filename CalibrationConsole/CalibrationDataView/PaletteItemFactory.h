#ifndef _PaletteItemFactory_h_
#define _PaletteItemFactory_h_

#include <PaletteAxisItem.h>

#include <memory>

#include <VarDefList.h>

#include <QGraphicsScene>

namespace PixCon {
  class CalibrationDataPalette;
}

class IPaletteItemFactory
{
public:
  virtual ~IPaletteItemFactory() {}

  virtual void create(QGraphicsScene *canvas,
          const QRectF &rec,
          std::vector< QGraphicsItem *> &list) = 0;

};

class PaletteItemFactory : IPaletteItemFactory
{
public:
  PaletteItemFactory(const std::vector< QColor > &colour_list, float min, float max);

  void create(QGraphicsScene *canvas,
          const QRectF &rec,
          std::vector< QGraphicsItem *> &list);

private:
  std::vector<QColor > m_colourList;
  float m_min;
  float m_max;

};

class StatePaletteItemFactory : IPaletteItemFactory
{
public:
  StatePaletteItemFactory(const std::vector<std::string> &state_names,
			  PixCon::VarId_t var_id,
			  const std::shared_ptr<PixCon::CalibrationDataPalette> &calibration_data_palette);

  void create(QGraphicsScene *canvas,
          const QRectF &rec,
          std::vector< QGraphicsItem *> &list);

private:
  std::vector< std::string >                              m_nameList;
  PixCon::VarId_t                                         m_varId;
  const std::shared_ptr<PixCon::CalibrationDataPalette> m_palette;
};

#endif
