#ifndef _PixCon_ModuleList_h_
#define _PixCon_ModuleList_h_

#include "ui_ModuleListBase.h"
#include "ModuleListView.h"
#include "ConnListItemView.h"
#include <memory>
#include <ConnItem.h>
#include <ConnListItem.h>

#include <qpixmap.h>

class Highlighter;

namespace PixCon {

  class CalibrationDataManager;
  class DataSelection;
  class IContextMenu;
  class IConnItemSelection;

  class ModuleList : public QWidget, public Ui_ModuleListBase
  {

  Q_OBJECT


  public:
    ModuleList( const std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data_manager,
		const std::shared_ptr<PixCon::DataSelection> &data_selection,
		std::shared_ptr<PixCon::IContextMenu> &context_menu,
		QWidget* parent = 0 ); 

    ~ModuleList();

    void recreateList() {
      m_connListItemView->createView();
    }

    void changeDataSelection(std::shared_ptr<DataSelection> data_selection) {
      m_connListItemView->changeDataSelection(data_selection);
    }

    void updateConnItem(const std::string &conn_name) {
      m_connListItemView->updateItem(conn_name);
    }

    bool isSelected(const std::string &conn_name) {
      return m_connListItemView->isSelected(conn_name);
    }

    bool isSelectedFromList(const std::string &conn_name) const {
      return m_connListItemView->isSelectedFromList(conn_name);
    }

    IConnItemSelection *currentSelection(bool selected, bool deselected);

    IConnItemSelection *selection(bool selected, bool deselected, EMeasurementType measurement_type, PixCon::SerialNumber_t serial_number);


    bool usesVariable(EMeasurementType measurement_type, SerialNumber_t serial_number, VarId_t id) const {
      return m_connListItemView->usesVariable(measurement_type, serial_number, id);
    }

    void listOfUsedMeasurements( std::map<EMeasurementType, SerialNumber_t> &used_measurements ) {
      m_connListItemView->listOfUsedMeasurements(used_measurements);
    }

    void clearFilterSelector();
    void addItemToFilterSelector(const std::string& filter_string);

  protected:
    void changeFilterAndSaveToList(bool save_in_list);
    std::string filterString() const;

  public slots:
    void changedVariable();

    void primaryAction(QTreeWidgetItem *, int);
    void contextMenu(const QPoint &pos);
    void mouseButtonClicked (QTreeWidgetItem * item, int c );

    void changeFilter() { changeFilterAndSaveToList(true); }
    void clearFilter();
    void setFilterString(const QString &filter);
    void showHideFilterEditor();
    void printModuleList();
    void setFullEditorUsed() {
      m_fullEditorUsed=true;
    }
    void setFullEditorUsedInt(int,int) {
      m_fullEditorUsed=true;
    }
    void setLineEditorUsed(const QString &) {
      m_fullEditorUsed=false;
    }
    void setLineEditorUsedInt(int) {
      m_fullEditorUsed=false;
    }
  signals:
    void changedSelection();
    void selectModule(EConnItemType type, const std::string &connectivity_name);

  private:
    ConnListItemView* m_connListItemView;
    std::shared_ptr<PixCon::IContextMenu> &m_contextMenu;
    std::unique_ptr< Highlighter > m_highlighter;
    std::unique_ptr< Highlighter > m_highlighterEditor;
    std::shared_ptr<CalibrationDataManager> m_calibrationData;
    std::shared_ptr<PixCon::DataSelection>  m_dataSelection;
    ModuleListView*                           m_moduleListView;
    int m_lastEditorSize;
    static const int s_maxNFilter = 25;

    QPixmap m_arrowRight;
    QPixmap m_arrowDown;

    bool m_fullEditorUsed;
  };

}
#endif
