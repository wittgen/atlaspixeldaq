/* Dear emacs, this is -*-c++-*- */
#ifndef _PixCon_IsMonitorReceiver_ixx_
#define _PixCon_IsMonitorReceiver_ixx_

#include "IsMonitorReceiver.h"
#undef DEBUG_TRACE

#include "IsReceptor.h"
#include "QIsInfoEvent.h"

#include <is/type.h>
#include <is/infoT.h>
#include <is/infoiterator.h>
#include <is/inforeceiver.h>
#include <is/callbackinfo.h>

#include <time.h>

#undef DEBUG_TRACE
#ifdef DEBUG
#  define DEBUG_TRACE(a) { a; }
#  include <iostream>
#else
#  define DEBUG_TRACE(a) {  }
#endif

namespace PixCon {

  //ISInfoT<Src_t>().type() && reg_ex_name
  template <class InfoSrc_t, class Src_t>
  IsMonitorReceiverBase<InfoSrc_t, Src_t>::IsMonitorReceiverBase(std::shared_ptr<IsReceptor> receptor,
						      EValueConnType conn_type,
                                                      Src_t value_after_deletion,
						      const ISCriteria &criteria,
						      unsigned int buffer_size,
						      unsigned int max_time_between_updates)
    : IsMonitorBase(receptor, conn_type, criteria , max_time_between_updates),
      m_buffer(buffer_size),
      m_valueAfterDeletion(value_after_deletion)
  { }


  template <class InfoSrc_t, class Src_t>
  void IsMonitorReceiverBase<InfoSrc_t, Src_t>::init() {
    assert(this->hasMonitorManager() );

    if (this->receptor()->partition() .isValid()) {
      DEBUG_TRACE( std::cout << "INFO [IsMonitorReceiver<Src_t>::init] " << this->varName()<< std::endl );

      // read all values from IS
      this->_update();

      DEBUG_TRACE( std::cout << "INFO [IsMonitorReceiver<Src_t>::init] subscribe" <<  this->varName()<< std::endl );
      DEBUG_TRACE( std::cout << "INFO [IsMonitorReceiver<Src_t>::init] receiver" << static_cast<void *>(this->receptor().get()) \
		   << " name=" << this->receptor()->isServerName().c_str() \
		   << " object=" << static_cast<void *>(this) \
		   << this->varName()<< std::endl );

      // ... now subscribe and listen for changes.
      this->receptor()->receiver().subscribe(this->receptor()->isServerName().c_str(),
					     this->criteria(),
					     &IsMonitorReceiverBase<InfoSrc_t, Src_t>::valueChanged, this);

      DEBUG_TRACE( std::cout << "INFO [IsMonitorReceiver<Src_t>::init] successfully subscribed to " << this->receptor()->isServerName().c_str() \
		   << "." <<  std::endl );
    }
  }


  class Counter {
  public:
    Counter(unsigned int &var) : m_counterVar(&var) { ++(*m_counterVar); assert(*m_counterVar==1); }
    ~Counter() { assert(*m_counterVar==1); --(*m_counterVar); }
  private:
    unsigned int *m_counterVar;
  };

//   template <class InfoSrc_t, class Src_t>
//   void IsMonitorReceiverBase<InfoSrc_t, Src_t>::valueChanged(ISCallbackInfo *info) {

//     Counter counter(this->m_concurrencyCounter); // for debugging/validation

//     InfoSrc_t is_value;
//     info->value(is_value);

//     this->bufferValue(info->name(), info->time().c_time(), is_value );
//   }

  template <class InfoSrc_t, class Src_t>
  void IsMonitorReceiverBase<InfoSrc_t, Src_t>::bufferValue(const std::string &var_name, ::time_t time_stamp, InfoSrc_t &is_value, bool value_was_deleted)
  {
    // Take an info container from the ring buffer of this monitor
    // and copy the contents of the callback info
    InfoBuffer<Src_t> *current;
    for(;;) {
      current = &( this->m_buffer.current() );
      if (current->isLocked()) {
	this->m_buffer.advance();
      }
      else {
	current->fill();
	break;
      }
    }
    current->set(this->newId(),var_name, time_stamp, is_value.getValue(), value_was_deleted);
    DEBUG_TRACE( std::cout << "INFO [IsMonitorReceiver::valueChanged] New value " << current->name() << " id=" << current->id() << std::endl )
    current->setValid();
    this->m_buffer.advance();
    this->setLastUpdate( time_stamp );

    // finally notify the monitor manager about the update
    this->requestUpdate();
  }


//   template <class InfoSrc_t, class Src_t, class SrcRef_t >
//   void IsMonitorReceiver<InfoSrc_t, Src_t, SrcRef_t>::_update()
//   {

//     DEBUG_TRACE( std::cout << "INFO [IsMonitorReceiver<Src_t>::_update] " << this->varName()<< std::endl );

//     InfoSrc_t is_value;
//     this->_updateLoop(is_value);
//   }


  template <class InfoSrc_t, class Src_t, class SrcRef_t >
  void IsMonitorReceiver<InfoSrc_t, Src_t, SrcRef_t>::_updateLoop(InfoSrc_t &is_value)
  {

    this->setNeedUpdate(false);
    this->setLastUpdate(time(NULL));

    // Get current values
    {
      std::map<std::string, std::set<std::string> > clean_up_var_list;

      ISInfoIterator info_iter( this->receptor()->partition(), this->receptor()->isServerName().c_str(), this->criteria() );
      
      while( info_iter() ) {
	// DEBUG_TRACE( std::cout << "INFO [IsMonitorReceiver<Src_t>::while ] start "<< std::endl );

	info_iter.value(is_value);

	// DEBUG_TRACE( std::cout << "INFO [IsMonitorReceiver<Src_t>::while ] read is_value "<< std::endl );
	std::string var_name = info_iter.name();

	DEBUG_TRACE( std::cout << "INFO [IsMonitorReceiver<Src_t>::while ] var_name =  "<<var_name<< std::endl );
	this->registerForCleanup(var_name, clean_up_var_list);
	setValue(var_name, info_iter.time().c_time(), is_value.getValue());

      }

      this->cleanup(clean_up_var_list);
    }
  }


  template <class InfoSrc_t, class Src_t, class SrcRef_t >
  bool IsMonitorReceiver<InfoSrc_t, Src_t, SrcRef_t>::processData()
  {
    unsigned int counter=100;
    while (this->m_buffer.hasData() && counter-->0) {

      InfoBuffer<Src_t> a_buffer = this->m_buffer.next();
      if (a_buffer.isValid() && a_buffer.lock()) {
	try {
	  unsigned int overflow = this->m_buffer.overflow() ;
	  this->m_buffer.clearOverflowCounter();
	  if (overflow>0) {
	    while (this->m_buffer.hasData()) {
	      this->m_buffer.next();
	    }
	    this->setNeedUpdate(true);
	    DEBUG_TRACE( std::cout << "WARNING [IsMonitor::processData] Buffer too small, " << overflow <<" value(s) Is values got lost. Will request a complete update."<< std::endl )
	    return true;
	  }
	  else if (!this->idOk( a_buffer.id())) {
	    this->errorMissedValue(a_buffer.name());
	  }
	  DEBUG_TRACE( std::cout << "INFO [IsMonitorReceiver::processData] " << a_buffer.name() << " id=" << a_buffer.id() << std::endl )
	  this->setValue(a_buffer.name(), a_buffer.time(), (a_buffer.valueWasDeleted() ? this->m_valueAfterDeletion : a_buffer.value()));

	  this->updateId( a_buffer.id() );
	}
	catch(std::exception &err) {
	  std::cerr << "WARNING [IsMonitor::processData] Caught exception : " << err.what() << std::endl;
	}
	catch(...) {
	  std::cerr << "WARNING [IsMonitor::processData] Caught unknown exception."<< std::endl;
	}
	a_buffer.done();
      }
    }
    return counter>0;
  }

  template <class InfoSrc_t, class Src_t, class SrcRef_t, class Dest_t>
  void IsMonitor<InfoSrc_t, Src_t, SrcRef_t, Dest_t>::setValue(const std::string &is_name, ::time_t time_stamp, SrcRef_t value)
  {
    Dest_t dest_value = static_cast<Dest_t>(value);
    this->receptor()->setValue(this->valueConnType(), is_name, *m_varNameExtractor, time_stamp, dest_value);
  }

  template <class InfoSrc_t, class Src_t, class SrcRef_t, class Dest_t>
  void IsMonitor<InfoSrc_t, Src_t, SrcRef_t, Dest_t>::registerForCleanup(const std::string &is_name, std::map<std::string, std::set<std::string> > &clean_up_var_list) {
    ConnVar_t conn_var(kAllConnTypes  /* unused */);
    m_varNameExtractor->extractVar(is_name,conn_var);
    DEBUG_TRACE( std::cout << "INFO [IsMonitorReceiver::registerForCleanup] veto = " <<  conn_var.connName() << " var = " << conn_var.varName()<< " " << std::endl; )
    this->receptor()->registerForCleanup(kCurrent,0,conn_var.connName(), conn_var.varName(), clean_up_var_list);
  }

}

#endif
