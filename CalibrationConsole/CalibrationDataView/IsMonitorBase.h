/* Dear emacs, this is -*-c++-*- */
#ifndef _PixCon_IsMonitorBase_h_
#define _PixCon_IsMonitorBase_h_

class IPCPartition;

#include <memory>
#include <VarDefList.h>

#include <is/criteria.h>
#include <time.h>

#include <map>
#include <set>

namespace PixCon {

  class IsMonitorManager;
  class IsReceptor;

  class IsMonitorBase
  {
    friend class IsMonitorManager;
  public:
    IsMonitorBase(std::shared_ptr<IsReceptor> receptor,
		  EValueConnType conn_type,
		  const ISCriteria &criteria,
		  unsigned int max_time_between_updates=0);

    virtual ~IsMonitorBase();

    void reconnect(IsMonitorManager *manager);

    void changePartition(IPCPartition &partition);

    void reconnect() {
      if (m_monitorManager) {
	unsubscribe();
	init();
      }
    }

    void disconnect() {
      unsubscribe();
      m_monitorManager=NULL;
    }

    void update() {
      if (m_monitorManager) {
	_update();
      }
    }

    unsigned int newId() {return ++m_currentId;}
    unsigned int idOk(unsigned int new_id) const { return new_id == m_lastId+1; }
    void updateId(unsigned int new_id)  { m_lastId = new_id; }

    void errorMissedValue(const std::string &name);

    bool needUpdate() const { return m_needUpdate; }
    void setNeedUpdate(bool need_update) { m_needUpdate=need_update; }

    /** Process the queue up to a maximum number of entries (to give other monitors a chance).
     * @return true in case the entire queue was processed otherwise false.
     */
    virtual bool processData() = 0;

    std::shared_ptr<IsReceptor> &receptor() { return m_receptor; }

    /** Unsubscribe and delete reference to monitor manager
     */
    void unsubscribe();

    /** unsubscribe and reinit.
     */
    void resubscribe();

    /** Return the time stamp of the last updated value.
     */
    ::time_t lastUpdate() const { return m_lastUpdate; }

    /** Return the maximum expected time between updates.
     */
    unsigned int maxTimeBetweenUpdates() const { return m_maxUpdateTime; }

  protected:

    virtual void init() = 0;

    virtual void _unsubscribe();

    const std::string &varName() { return m_varName; }

    ISCriteria &criteria()                    { return m_criteria; }
    const ISCriteria &criteria() const        { return m_criteria; }

    void overflowWarning( unsigned int overflow);

    /** Reread values from IS.
     */
    virtual void _update() =0;
    
    /** Called when the monitor is updated to give the monitor the possibility to register variables for cleanup.
     * @param is_name the full name in IS.
     * @param clean_up_var_list for each appearing variable the connectivity names for which the varible was updated should be added to the set.
     */
    virtual void registerForCleanup(const std::string &is_name, std::map<std::string, std::set<std::string> > &clean_up_var_list) = 0;

    /** Cleanup the current measurement for all variables which are in the map.
     * @param clean_up_var_list list of variables to be cleaned up with an associated list of objectes for which the variable should stay untouched.
     * Values are untouched of objects whose connectivity name is contained in the set which is associated to each variable name.
     */
    void cleanup(const std::map<std::string, std::set<std::string> > &clean_up_var_list);

    void requestUpdate();

    // for debugging / validation
    bool hasMonitorManager() const { return m_monitorManager!=NULL; }

    void setLastUpdate(::time_t time_stamp) { m_lastUpdate = time_stamp; }

    EValueConnType valueConnType() const { return m_connType; }

  private:
    std::shared_ptr<IsReceptor> m_receptor;
    IsMonitorManager              *m_monitorManager;
    EValueConnType                m_connType;
    std::string                   m_varName;
    ISCriteria                    m_criteria;
    unsigned int                  m_lastId;
    unsigned int                  m_currentId;
    unsigned int                  m_maxUpdateTime;
    ::time_t                      m_lastUpdate;
    bool                          m_needUpdate;
    bool                          m_subscribed;
  protected:
    unsigned int m_concurrencyCounter;
  };

}
#endif
