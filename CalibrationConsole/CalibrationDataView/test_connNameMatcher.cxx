#include <string>

#include <vector>
#include <iostream>
#include <sstream>
#include <cctype>

#include "checkConnName.h"

int main(int argc, char **argv)
{

  for (int arg_i=1; arg_i< argc; arg_i++) {
    //    unsigned int expr_i=0;

    if (checkConnName(argv[arg_i])) {
	std::cout << "INFO "  << argv[arg_i] << " seems to be a valid connectivity name." << "." << std::endl;
    }
  }
  return 0;
}
