// Dear emacs, this is -*-c++-*-x
#ifndef DISABLEPANEL_H
#define DISABLEPANEL_H

#include "ui_DisablePanelBase.h"
#include <list>
#include <string>
#include "ConfigWrapper/Connectivity.h"
#include <memory>

class IPCPartition;

namespace PixLib {
  class PixDisable;
  class PixDbServerInterface;
}

namespace PixCon {
  class ConsoleEngine;

  class DisablePanel : public QDialog, public Ui_DisablePanelBase 
  {
    Q_OBJECT

    public:
    DisablePanel(const PixA::ConnectivityRef &conn, QWidget* parent = 0);
    ~DisablePanel();

    std::shared_ptr<const PixLib::PixDisable> getDisable();
    unsigned int revision() { return m_revision;}
    std::string name() const { return m_name; }

    static std::string makeTimeString(time_t a_time);


    public slots:
      void nameSelected(int);
      void revSelected(int);

    private:
      std::set< std::string > m_names;
      std::set< unsigned int > m_revisions;
      std::vector<unsigned int> m_itemRevision;
      std::string m_name;
      unsigned int m_revision;
      PixA::ConnectivityRef m_conn;


      std::shared_ptr<const PixLib::PixDisable> m_disable;
      std::shared_ptr<PixLib::PixDbServerInterface> m_dbs;

  };
}

#endif // DISABLEPANEL_H
