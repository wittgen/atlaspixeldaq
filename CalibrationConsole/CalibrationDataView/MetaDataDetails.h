// Dear emacs, this is -*-c++-*-
#ifndef _PixCon_MetaDataDetails_h_
#define _PixCon_MetaDataDetails_h_

#include "ui_MetaDataDetailsBase.h"

#include <VarDefList.h>
#include <ScanMetaDataCatalogue.h>
#include <CalibrationDataTypes.h>
#include <CalibrationDataStyle.h>
#include <CalibrationDataManager.h>
#include <DefaultColourCode.h>
#include <qpen.h>
#include <qbrush.h>
#include <qcolor.h>
#include <QLabel>

namespace PixCon {

  class IMetaDataUpdateHelper;

  class MetaDataDetails : public QWidget, public Ui_MetaDataDetailsBase
  {
    Q_OBJECT

  public:
    MetaDataDetails(const std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data,
		    const std::shared_ptr<PixCon::CalibrationDataPalette> &palette,
		    const std::shared_ptr<PixCon::IMetaDataUpdateHelper>  &update_helper,
		    QWidget* parent = 0);

    ~MetaDataDetails();

    void reset();

    bool setMeasurement(EMeasurementType measurement_type, SerialNumber_t serial_number);

    EMeasurementType currentMeasurementType() const { return m_currentMeasurementType; }
    SerialNumber_t currentSerialNumber() const      { return m_currentSerialNumber; }

    void setType(const std::string &type_name, const std::string &tag, unsigned int revision);

    enum ECfgStatus {kNormal, kTemporary, kLost};
    void setCfg1(const std::string &label, const std::string &name, const std::string &cfg_tag, unsigned int revision, ECfgStatus cfg_status);
    void setCfg2(const std::string &label, const std::string &name, const std::string &cfg_tag, unsigned int revision, ECfgStatus cfg_status);

    void setStartTime(const time_t &start_time);
    void setDuration(unsigned int seconds );

    void setStatus(MetaData_t::EStatus status);

    void setComment( const std::string &comment);

    void setQuality(MetaData_t::EQuality quality);

    static State_t mapStatus(MetaData_t::EStatus status) {
      if (status>MetaData_t::kLoading) {
	if (status<=MetaData_t::kLoadingUnknown) {
	  return s_colorMap[MetaData_t::kLoading];
	}
	else {
	  return s_colorMap[MetaData_t::kUnknown];
	}
      }
      else {
	return s_colorMap[status];
      }
    }

    static const char *statusName(MetaData_t::EStatus status) {
      if (status<=MetaData_t::kLoading) {
	return s_statusName[status];
      }
      else {
	if (status>MetaData_t::kLoadingUnknown) {
	  return s_statusName[MetaData_t::kUnknown];
	}
	else {
	  return s_statusName[MetaData_t::kLoading];
	}
      }
    }

    static const char *qualityName(MetaData_t::EQuality quality) {
      if (quality<MetaData_t::kNQualityStates) {
	return s_qualityName[quality];
      }
      else {
	return s_qualityName[MetaData_t::kUndefined];
      }
    }


    std::pair<QBrush , QPen > statusColour(MetaData_t::EStatus status)  {
      return palette().brushAndPen(true,1, 0, static_cast<VarId_t>(-1), mapStatus(status) );
    }

    static std::string makeTimeString(time_t a_time);


    static MetaData_t::EQuality getSanitisedQualityTag(MetaData_t::EQuality quality) {
      if (quality >= MetaData_t::kNQualityStates) {
      quality = MetaData_t::kUndefined;
      }
      return quality;
    }

    bool hasChanges() const { return (m_changed[kQualityChanged] || m_changed[kCommentChanged]); }

    void commitChanges(bool ask);

    PixCon::CalibrationDataPalette &palette() {return *m_palette; }
    PixCon::CalibrationDataManager &calibrationData() {return *m_calibrationData; }

  signals:
    void metaDataHasChanged(PixCon::EMeasurementType, PixCon::SerialNumber_t);
public slots:
 void updateMetaData();
    void qualityChanged(int selected_quality);
    void commentChanged();
  protected:

   // void updateMetaData();
   // void qualityChanged(int selected_quality);
   // void commentChanged();


    static void initialise();

    void setCfg(QLabel *type_label,
		const std::string &type_name,
		QLabel *tag_label,
		const std::string &name,
		const std::string &tag,
		unsigned int revision,
		ECfgStatus status);

    void updateButton();

  private:
    std::shared_ptr<PixCon::CalibrationDataManager> m_calibrationData;
    std::shared_ptr<PixCon::CalibrationDataPalette> m_palette;
    std::shared_ptr<PixCon::IMetaDataUpdateHelper>  m_updateHelper;

    static const char *s_statusName[MetaData_t::kNStates];
    static const char *s_qualityName[MetaData_t::kNQualityStates];
    static std::vector<QColor> s_tagColor;
    static std::vector<QColor> s_tagForegroundColor;
    static std::vector<DefaultColourCode::EDefaultColourCode> s_colorMap;
    static bool s_initialise;
   
    EMeasurementType     m_currentMeasurementType;
    SerialNumber_t       m_currentSerialNumber;
   
    MetaData_t::EQuality m_currentQuality;
    MetaData_t::EQuality m_lastQuality;
    std::string          m_currentComment;
    
    enum EChanges {kCommentChanged, kQualityChanged };
    bool                 m_changed[2];
  };
}
#endif
