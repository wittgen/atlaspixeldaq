#include "LogicParser.h"
#include <sstream>
#include <iostream>
#ifdef HAVE_TIME_MEASURE
#include "TTimeMeasure.H"
#endif

enum EConditionOperators {kEqual, kNotEqual, kGreater, kGreaterEqual, kLess, kLessEqual};

int main(int argc, char * argv[]) {

  if (argc<3) return -1;

  std::cout << "Starting" << std::endl;


  PixCon::TokenDescription description;

  description.addOperator(">=",kGreaterEqual+PixCon::LogicParserBase<bool>::kNLogicOperators);
  description.addOperator(">",kGreater+PixCon::LogicParserBase<bool>::kNLogicOperators);
  description.addOperator("<",kLess+PixCon::LogicParserBase<bool>::kNLogicOperators);
  description.addOperator("<=",kLessEqual+PixCon::LogicParserBase<bool>::kNLogicOperators);
  description.addOperator("==",kEqual+PixCon::LogicParserBase<bool>::kNLogicOperators);
  description.addOperator("!=",kNotEqual+PixCon::LogicParserBase<bool>::kNLogicOperators);
  description.addOperator("&&",PixCon::LogicParserBase<bool>::kAnd);
  description.addOperator("||",PixCon::LogicParserBase<bool>::kOr);
  description.addOperator("!",PixCon::LogicParserBase<bool>::kNot); // must be after != otherwise != will always be interpreted as ! + =
  description.addOperator("\\!",PixCon::LogicParserBase<bool>::kNot); // must be after != otherwise != will always be interpreted as ! + =
  description.setAllowedSpecialCharList("_:.");

  PixCon::LogicParser p;
  std::string expression(argv[1]);
  PixCon::Tokeniser tokeniser(description,&expression);
  try {
    p.compileExpression(tokeniser);
  }
  catch (PixCon::LogicParser::ParseError &err) {
    std::cout << "ERROR [main:" << argv[0] << "] exception while compiling expression : " << err.what() << std::endl;
    return -1;
  }

  std::cout << "Ready" << std::endl;

  std::vector< std::pair<std::string,bool> > input;
  bool expected_result=atoi(argv[2]);
  for (int arg_i=3; arg_i<argc; arg_i++) {
    std::string temp(argv[arg_i]);
    std::string::size_type pos=temp.find("=");
    //    bool value;
    if (!isalpha(argv[arg_i][0]) || pos==std::string::npos || pos+2!=temp.size() || (temp[pos+1]!='0' && temp[pos+1]!='1')) {
      std::cout << "ERROR [main:" << argv[0] << "] expected something like var_name=0 or a=1 instead of argument " << arg_i 
		<< " which is " << argv[arg_i] << "."  << std::endl;
      return -1;
    }
    std::map<std::string, bool>::iterator iter = p.find(std::string(temp,0,pos));
    if (iter==p.end()) {
      std::cout << "WARNING [main:" << argv[0] << "]  Variable " << std::string(temp,0,pos) << " not contained in expression."  << std::endl;
   
    else {
      iter->second = atoi(&((temp.c_str())[pos+1]));
    }
  }
#ifdef HAVE_TIME_MEASURE
  TTimeMeasure timer;
  timer.Start();
#endif
  bool ret_a = p.evaluate();
  for (unsigned int i=0; i<2000; i++) {
    ret_a = p.evaluate();
  }
#ifdef HAVE_TIME_MEASURE
  timer.Stop();
  timer.Show();
#endif
  assert(expected_result == ret_a);

  std::cout << "Success" << std::endl;
  return 0;
}
