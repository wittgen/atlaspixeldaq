#ifndef _PixCon_ISelection_h_
#define _PixCon_ISelection_h_

#include <string>

namespace PixCon {

  class ISelection {
  public:
    virtual ~ISelection() {}
    virtual bool isSelected(const std::string &name) const = 0;

  };

}
#endif
