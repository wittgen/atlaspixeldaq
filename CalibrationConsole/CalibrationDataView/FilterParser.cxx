#include "FilterParser.h"
#include <algorithm>
#include "Tokeniser.h"
#include <VarDefList.h>
#include <EvaluatorFactory.h>

// #include <functional>

namespace PixCon {

  const char *FilterParser::s_enabledName="Enabled";
  const char *FilterParser::s_disabledName="Disabled";
  const char *FilterParser::s_allocatedName="IsAllocated";
  const char *FilterParser::s_definedFucntionName="defined";

  FilterParser::FilterParser( EvaluatorFactory &evaluator_factory) 
    : m_factory(&evaluator_factory)
  {
  }

  // possibilities
  // boolean i.e. connectivity name
  // value  comp op "value" variable
  // value-variable comp op "value"
  // state equal / not equal state variable
  // variable equal / not equal comp-op
  void FilterParser::ParseNucleus(PixCon::Tokeniser &tokeniser, IEvaluator *&node_ptr, bool strong_expression_only)
  {
//     Measurement_t measurement;
//     if (tokeniser.isSerialNumber()) {

//       measurement = Measurement_t(tokeniser.measurementType(), tokeniser.serialNumber() );

//       if (!tokeniser.nextToken() || !tokeniser.isName()) {
// 	std::stringstream message;
// 	message << "Expected variable after serial number but got \"" << tokeniser.token() << "\".";
// 	throw MissingOperand(message.str());
//       }
//     }

//     if (tokeniser.isOperator()) {
//       std::stringstream message;
//       message << "Expected operand but got operator \"" << tokeniser.token() << "\".";
//       throw MissingOperand(message.str());
//     }
//     else if (!tokeniser.isName() && !tokeniser.isValue() && !tokeniser.isWildcardPattern()) {
//       std::stringstream message;
//       message << "Expected operand but got empty token. Oh God, /(..)\\, that cannot be!";
//       throw MissingOperand(message.str());
//     }

    bool        is_string[2];
    float       value[2];
    Measurement_t measurement[2];
    EvaluatorFactory::ECompareOperatorType operator_id = static_cast<EvaluatorFactory::ECompareOperatorType>(UINT_MAX);
    std::string value_or_var[2];
    VarDef_t    var_def[2]={VarDef_t::invalid(), VarDef_t::invalid()};
    unsigned int op_counter_i=0;
    unsigned int value_counter=0;
    EValueType op_type[2] = {kNValueTypes, kNValueTypes};
    EValueType comb_op_type = kNValueTypes;

    for (unsigned short pass_i=0; pass_i<3; pass_i++) {
      if (pass_i==1) {

	// operator

	if (  !tokeniser.isOperator()
	    || tokeniser.operatorId() < kNLogicOperators
	    || tokeniser.operatorId() >= kNLogicOperators + EvaluatorFactory::kNCompareOperators) {

	  break;
	}

	operator_id = static_cast< EvaluatorFactory::ECompareOperatorType>( tokeniser.operatorId() - kNLogicOperators );
      }
      else {

	bool negate_operand=false;
	if (tokeniser.isOperator() && tokeniser.operatorId()> kNLogicOperators + EvaluatorFactory::kNCompareOperators) {
	  if (tokeniser.operatorId()==EvaluatorFactory::kPlus) {
	  }
	  else if (tokeniser.operatorId()==EvaluatorFactory::kMinus) {
	    negate_operand = true;
	  }
	  else {
	    std::stringstream message;
	    message << "ERROR [FilterParser::ParseNucelus] Unsupported operator istead of operand. Operator id = " << tokeniser.operatorId();
	    throw MissingOperand(message.str());
	  }
	}

	// operand 1 and 2
	std::string current_token = tokeniser.token();
	bool is_serial_number=false;
	if (tokeniser.isSerialNumber()) {
	  measurement[op_counter_i]=Measurement_t(tokeniser.measurementType(), tokeniser.serialNumber());
	  is_serial_number=true;
	}
	else if (tokeniser.isName()) {
	  if (current_token=="Current" || current_token=="Present Value") {
	    is_serial_number=true;
	    measurement[op_counter_i]=Measurement_t(kCurrent,0);
	  }
	}

	if (is_serial_number) {

	  if (!tokeniser.nextToken()
              || !tokeniser.isOperator() || tokeniser.operatorId() !=   kNLogicOperators
                                                                      + EvaluatorFactory::kNCompareOperators
                                                                      + EvaluatorFactory::kNArithmeticOperators
	                                                              + EvaluatorFactory::kSeparator) {

	    std::stringstream message;
	    message << "ERROR [FilterParser::ParseNucelus] Expected \".\" after measurement specifier : " << current_token << ".";
	    throw MissingOperand(message.str());
	}
	if (!tokeniser.nextToken()) {
	    std::stringstream message;
	    message << "ERROR [FilterParser::ParseNucelus] Expected variable after measurement specifier : " << current_token << ".";
	    throw MissingOperand(message.str());
	}
	current_token = tokeniser.token();
	}

	if (is_serial_number) {
	  if (!tokeniser.isName()) {
	    std::stringstream message;
	    message << "ERROR [FilterParser::ParseNucelus] Cannot have anything but a variable after measurement specifier. But got : " << current_token << ".";
	    throw MissingOperand(message.str());
	  }
	  else if (negate_operand) {
	    std::stringstream message;
	    message << "ERROR [FilterParser::ParseNucelus] A negation operator is illegal before a measurement. But got : " << current_token << ".";
	    throw MissingOperand(message.str());
	  }
	}

	if (tokeniser.isName()) {
	  is_string[op_counter_i]=true;
	  value_or_var[op_counter_i]=current_token;

	  if (current_token == s_definedFucntionName) {
	    bool error=true;
	    if (op_counter_i==0
		&& tokeniser.nextToken()
		&& tokeniser.isBlockOpen()
		&& tokeniser.nextToken()
		&& tokeniser.isName()) {

	      const std::string var_name = tokeniser.token();

	      if ( tokeniser.nextToken()
		   && tokeniser.isBlockClose()) {

		try {
		  var_def[op_counter_i] = VarDefList::instance()->getVarDef(var_name);
		  op_type[op_counter_i] = var_def[op_counter_i].valueType();
		  error=false;
		  op_counter_i++;

		}
		catch (UndefinedCalibrationVariable &err) {
		  var_def[op_counter_i] = VarDef_t::invalid();
		  std::stringstream message;
		  message << "ERROR [FilterParser::ParseNucelus] Expected variable name argument after " << current_token << ". Invalid variable name : "
			  << var_name << ".";
		  throw MissingOperand(message.str());
		}

	      }
	    }

	    if (error) {
	      std::stringstream message;
	      message << "ERROR [FilterParser::ParseNucelus] Inproper usage of " <<current_token<< "."
		      << current_token << " Expected expression  : "
		      << current_token << "(name) where name is a valid variable name.";
	      throw MissingOperand(message.str());
	    }
	  }
	  else {
	    if (negate_operand) {
	      std::stringstream message;
	      message << "ERROR [FilterParser::ParseNucelus] Operations on variables are not supported.";
	      throw MissingOperand(message.str());
	    }

	    try {
	      if (value_or_var[op_counter_i]==s_enabledName
		  || value_or_var[op_counter_i]==s_disabledName
                  || value_or_var[op_counter_i]==s_definedFucntionName
                  || value_or_var[op_counter_i]==s_allocatedName) {
		op_type[op_counter_i] = kNValueTypes;
	      }
	      else {
		var_def[op_counter_i] = VarDefList::instance()->getVarDef(value_or_var[op_counter_i]);
		if (var_def[op_counter_i].isValueWithOrWithoutHistory()) {
		  op_type[op_counter_i]=kValue;
		}
		else if (var_def[op_counter_i].isStateWithOrWithoutHistory()) {
		  op_type[op_counter_i]=kState;
		}
		else {
		  op_type[op_counter_i]=kNValueTypes;
		}
	      }
	      comb_op_type = op_type[op_counter_i];
	    }
	    catch (UndefinedCalibrationVariable &err) {
	      var_def[op_counter_i] = VarDef_t::invalid();
	      value_counter++;
	    }

	    op_counter_i++;
	  }
	}
	else if (tokeniser.isWildcardPattern()) {
	  is_string[op_counter_i]=true;
	  value_or_var[op_counter_i]=current_token;
	  op_type[op_counter_i] = kState;
	  comb_op_type = kState;
	  op_counter_i++;
	  value_counter++;
	}
	else if (tokeniser.isValue()) {
	  is_string[op_counter_i]=false;
	  value[op_counter_i]=static_cast<float>(tokeniser.convertedValue());
	  if (negate_operand) {
	    value[op_counter_i]=-value[op_counter_i];
	  }
	  op_type[op_counter_i] = kValue;
	  comb_op_type = kValue;
	  op_counter_i++;
	  value_counter++;
	}
	else {
	  std::stringstream message;
	  message << "ERROR [FilterParser::ParseNucelus] Expected operand ";
	  if (op_counter_i>0) {
	    message << " after operator ";
	  }
	  message << ", but got \"" << tokeniser.token() << "\".";
	  throw MissingOperand( message.str() );
	}

      }
      if (!tokeniser.nextToken()) break;
    }

    std::pair< IEvaluator *, bool> evaluator;

    if (op_counter_i==0) {
	std::stringstream message;
	message << "ERROR [FilterParser::ParseNucelus] Expression without operand.";
	throw MissingOperand(message.str());
    }
    else if (op_counter_i==1) {
      // expect connectivity name pattern

      if (value_or_var[0]==s_enabledName) {
	evaluator = m_factory->createEnableEvaluator(false, negation(), measurement[0]);
      }
      else if (value_or_var[0]==s_disabledName) {
	evaluator = m_factory->createEnableEvaluator(true, negation(), measurement[0]);
      }
      else if (value_or_var[0]==s_allocatedName) {
	evaluator = m_factory->createAllocatedEvaluator(true, negation(), measurement[0]);
      }
      else if (value_or_var[0]==s_definedFucntionName) {
	assert( var_def[0].isValid()); //should have been checked earlier

	if (var_def[0].isValueWithOrWithoutHistory() ) {
	  evaluator = m_factory->createDefinedEvaluator<float>(var_def[0], measurement[0], false, negation());
	}
	else if (var_def[0].isStateWithOrWithoutHistory() ) {
	  evaluator = m_factory->createDefinedEvaluator<State_t>(var_def[0], measurement[0], false, negation());
	}
	else {
	  // unsupported value type
	  assert( var_def[0].isStateWithOrWithoutHistory() || var_def[0].isValueWithOrWithoutHistory());
	}

      }
      else {
	// make upper case version
	transform (value_or_var[0].begin (), value_or_var[0].end (), value_or_var[0].begin (), (int(*)(int)) toupper);
	// disabled conn name expression check, because it does not work with tag names.
	//       if (!checkConnName(value_or_var[0])) {
	// 	std::stringstream message;
	// 	message << "ERROR [FilterParser::ParseNucelus] Expected connectivity name pattern, but got \""
	// 		<< value_or_var[0] << "\".";
	// 	throw MissingOperand(message.str());
	//       }
	evaluator = m_factory->createConnNameComparator(value_or_var[0]);
      }

    }
    else if (op_counter_i==2) {
      if (strong_expression_only) {
	throw OperandMismatch( "ERROR [FilterParser::ParseNucelus] Previous operator demands boolean operand but got comparison." );
      }
      if (op_type[0] != op_type[1] && op_type[0]!=kNValueTypes && op_type[1]!=kNValueTypes) {
	throw OperandMismatch( "ERROR [FilterParser::ParseNucelus] Operands do not match" );
      }
      if (value_counter> 1) {
	std::stringstream message;
	message << "ERROR [FilterParser::ParseNucelus] Expected one variable a comparison operator and a value. But ";
	if (value_counter==0) {
	  message << " did not get a value.";
	}
	else {
	  message << " got " << value_counter << " values.";
	}

	throw OperandMismatch(message.str());
      }
      // expect variable and value where the value can be a floating point value or a state name
      unsigned int other_operand;
      unsigned int operand_i;
      for (operand_i=0; operand_i<2; operand_i++) {
	other_operand = ((operand_i+1) % 2);
	if (op_type[operand_i]==kNValueTypes || !var_def[operand_i].isValid()) break;
      }
      if (operand_i==2) {
	operand_i = ((other_operand+1) % 2);
      }

      if (!var_def[other_operand].isValid()) {
	std::string message("ERROR [FilterParser::ParseNucelus] Neither ");
	message += value_or_var[0] ;
	message += "nor " ;
	message += value_or_var[1] ;
	message += " are valid variable names.";
	throw OperandMismatch( message );
      }

      if (operand_i==0) {
	operator_id = EvaluatorFactory::redirectOperator(operator_id);
      }

      // paranoia check
      assert( var_def[ other_operand ].isValid() );

      if (comb_op_type == kValue) {

	if (var_def[operand_i].isValid()) {
	  evaluator = m_factory->createValueComparator<float>(var_def[other_operand],measurement[other_operand],
							      var_def[operand_i],    measurement[operand_i],     operator_id );
	}
	else {
	  if (is_string[operand_i]) {
	    std::stringstream message;
	    message << "ERROR [FilterParser::ParseNucelus] Expected floating point value to compare with variable " << VarDefList::instance()->varName( var_def[other_operand].id() )
		    << ", but got " << value_or_var[operand_i] << ".";
	    throw MissingOperand( message.str() );
	  }

	  // create
	  evaluator = m_factory->createValueComparator<float>(var_def[other_operand],measurement[other_operand],value[operand_i],operator_id );
	}

      }
      else if (comb_op_type == kState) {

	if (var_def[operand_i].isValid()) {
	  evaluator = m_factory->createValueComparator<State_t>(var_def[other_operand],measurement[other_operand],
								var_def[operand_i],    measurement[operand_i],     operator_id );
	}
	else {
	  if (!is_string[operand_i]) {
	    std::stringstream message;
	    message << "ERROR [FilterParser::ParseNucelus] Expected state name to compare with variable " << VarDefList::instance()->varName( var_def[other_operand].id() )
		    << ", but got a floating point value.";
	    throw MissingOperand( message.str() );
	  }

	  StateVarDef_t state_var_def = var_def[ other_operand ].stateDef();
	  State_t state = state_var_def.state(value_or_var[operand_i]);
	  if (!state_var_def.isValidState(state)) {
	    std::stringstream message;
	    message << "ERROR [FilterParser::ParseNucelus] Expected state name to compare with variable " << VarDefList::instance()->varName( var_def[other_operand].id() )
		    << ", but got " << value_or_var[operand_i] << ".";
	    throw MissingOperand( message.str() );
	  }
	  // create comparison;
	  evaluator = m_factory->createValueComparator<State_t>(var_def[other_operand],measurement[other_operand], state,operator_id);
	}
      }
    }

    node_ptr =  (evaluator.second ^ negation() ? Negator::negate(evaluator.first) : evaluator.first);
  }

}
