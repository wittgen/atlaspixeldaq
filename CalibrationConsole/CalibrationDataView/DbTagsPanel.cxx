#include "DbTagsPanel.h"

#include <ConfigWrapper/ConnectivityManager.h>

DbTagsPanel::DbTagsPanel(const std::string &current_id_tag,
			 const std::string &current_conn_tag,
			 const std::string &current_cfg_tag,
			 const std::string &current_mod_tag,
			 QWidget* parent,
			 std::string partition_name,
			 std::string dbServer_name) 
: QDialog(parent)
{
  setupUi(this);
  m_dbs = NULL;
  comboBoxTagOI->clear();
  comboBoxTagCfgOI->clear();
  comboBoxTagConn->clear();
  comboBoxTagCfg->clear();
  comboBoxTagModCfg->clear();

  TagCFOI_label->hide();
  comboBoxTagCfgOI->hide();
  textLabel1->hide();
  m_revisionEntry->hide();

  if (partition_name.empty()) {
    try {
      std::vector<std::string> object_tags;
      PixA::ConnectivityManager::instance()->getObjectTags(&object_tags);
      for (std::vector<std::string>::iterator it = object_tags.begin(); it!=object_tags.end(); it++) {
	comboBoxTagOI->addItem(QString::fromStdString(*it));
      }
      PixA::ConnectivityManager::instance()->getConfigurationObjectTags(&object_tags);
      for (std::vector<std::string>::iterator it = object_tags.begin(); it!=object_tags.end(); it++) {
	comboBoxTagCfgOI->addItem(QString::fromStdString(*it));
      }
    }
    catch (PixA::BaseException &err) {
      std::cout << "FATAL [DbTagsPanel::DbTagsPanel] caught exception (BaseException) : " << err.getDescriptor() << std::endl;
    }
    catch (std::exception &err) {
      std::cout << "FATAL [DbTagsPanel::DbTagsPanel] caught exception (std::exception) : " << err.what() << std::endl;
    }
    catch (...) {
      std::cout << "FATAL [DbTagsPanel::DbTagsPanel] unhandled exception. "  << std::endl;
    }
  }
  else {
    std::unique_ptr<IPCPartition> partition;
    if (!partition_name.empty() && !dbServer_name.empty()) {
      partition = std::make_unique<IPCPartition>(partition_name.c_str());
      if (!partition->isValid()) {
	std::cerr << "ERROR [DbTagsPanel::DbTagsPanel] Not a valid partition " << partition_name
		  << " for the db server." << std::endl;
	return;
      }
      bool ready=false;
      m_dbs = new PixLib::PixDbServerInterface(partition.get(), dbServer_name, PixLib::PixDbServerInterface::CLIENT, ready);
      if(!ready) {
	delete m_dbs;
	m_dbs = NULL;
	std::cerr << "ERROR [DbTagsPanel::DbTagsPanel] Impossible to connect to DbServer " << dbServer_name << std::endl;
	return;
      }
      std::vector<std::string> listDomains;
      m_dbs->listDomainRem(listDomains);
      for (std::vector<std::string>::iterator it = listDomains.begin(); it!=listDomains.end(); it++) {
	std::cout << "INFO [DbTagsPanel::DbTagsPanel] Found domain: " << (*it) << std::endl;
	std::string s1("Connectivity");
	std::string::size_type pos = (*it).find(s1);
	if (pos != std::string::npos){
	  std::string idTag = (*it).substr(pos+s1.length()+1);
	  bool found = false;
	  for (int i = 0; i < comboBoxTagOI->count(); i++) {
	    if (comboBoxTagOI->itemText(i).toLatin1().data() == idTag) {
	      found = true;
	    }
	  }
	  if (!found){
	    comboBoxTagOI->addItem(QString::fromStdString(idTag));
	    if (!current_id_tag.empty() && idTag==current_id_tag) {
	      comboBoxTagOI->setCurrentIndex(comboBoxTagOI->count()-1);
	    }
	  }
	}
	std::string s2("Configuration");
	pos = (*it).find(s2);
	if (pos != std::string::npos){
	  std::string idTag = (*it).substr(pos+s2.length()+1) + "-CFG";
	  bool found = false;
	  for (int i = 0; i < comboBoxTagCfgOI->count(); i++) {
	    if (comboBoxTagCfgOI->itemText(i).toLatin1().data() == idTag) {
	      found = true;
	    }
	  }
	  if (!found){
	    comboBoxTagCfgOI->addItem(QString::fromStdString(idTag));
	    if (!current_id_tag.empty() && idTag==current_id_tag+"-CFG") {
	      comboBoxTagCfgOI->setCurrentIndex(comboBoxTagCfgOI->count()-1);
	    }

	  }
	}
      }
    }
  }
  if (!comboBoxTagOI->currentText().isEmpty()) {
    changeObjectTag(comboBoxTagOI->currentText().toLatin1().data());
  }
  if (!comboBoxTagCfgOI->currentText().isEmpty()) {
    changeCfgObjectTag(comboBoxTagCfgOI->currentText().toLatin1().data());
  }
  if (!current_conn_tag.empty()) {
  for (int i = 0; i < comboBoxTagConn->count(); i++) {
    if (comboBoxTagConn->itemText(i).toLatin1().data() == current_conn_tag) {
      comboBoxTagConn->setCurrentIndex(i);
    }
  }
  }

  if (!current_cfg_tag.empty()) {
  for (int i = 0; i < comboBoxTagCfg->count(); i++) {
    if (comboBoxTagCfg->itemText(i).toLatin1().data() == current_cfg_tag) {
      comboBoxTagCfg->setCurrentIndex(i);
    }
  }
  }

  if (!current_mod_tag.empty()) {
  for (int i = 0; i < comboBoxTagModCfg->count(); i++) {
    if (comboBoxTagModCfg->itemText(i).toLatin1().data() == current_mod_tag) {
      comboBoxTagModCfg->setCurrentIndex(i);
    }
  }
  }

}

void DbTagsPanel::changeObjectTag(const QString &object_tag)
{
  std::vector<std::string> conn_tags;
  std::vector<std::string> alias_tags;
  std::vector<std::string> payload_tags;
  comboBoxTagConn->clear();
  
  if (m_dbs == NULL) {
    PixA::ConnectivityManager::instance()->getConnectivityTags(object_tag.toLatin1().data(), &conn_tags, &alias_tags, &payload_tags, NULL);
    for (std::vector<std::string>::iterator it = conn_tags.begin(); it!=conn_tags.end(); it++) {
      comboBoxTagConn->addItem(QString::fromStdString(*it));
    }

  }
  else {
    std::vector<std::string> connTags;
    std::string a= "Connectivity-";   std::string connDomain = a + object_tag.toUtf8().constData();
    m_dbs->listTagsRem(connDomain, connTags);
    for (std::vector<std::string>::iterator it = connTags.begin(); it!=connTags.end(); it++) {
      comboBoxTagConn->addItem(QString::fromStdString(*it));
    }
  }    
  QString cfg_obj_tag(object_tag);
  cfg_obj_tag+= "-CFG";

  unsigned int newIndex = find(*comboBoxTagCfgOI,cfg_obj_tag);
  if (newIndex < static_cast<unsigned int>(comboBoxTagCfgOI->count())) {
    comboBoxTagCfgOI->setCurrentIndex(newIndex);
    changeCfgObjectTag(cfg_obj_tag);
  }
  else {
    newIndex = find(*comboBoxTagCfgOI,object_tag);
    if (newIndex < static_cast<unsigned int>(comboBoxTagCfgOI->count())) {
      comboBoxTagCfgOI->setCurrentIndex(newIndex);
      changeCfgObjectTag(object_tag);
    }

  }
  
  //  for (std::vector<std::string>::iterator it = cfg_tags.begin(); it!=cfg_tags.end(); it++) {
  //    comboBox4->addItem((*it));
  //  }
}

void DbTagsPanel::changeCfgObjectTag(const QString &object_tag)
{
  std::vector<std::string> cfg_tags;
  PixA::ConnectivityManager::instance()->getConnectivityTags(object_tag.toLatin1().data(), NULL, NULL, NULL, &cfg_tags);
  comboBoxTagCfg->clear();
  comboBoxTagModCfg->clear();
  if (m_dbs == NULL) {
    for (std::vector<std::string>::iterator it = cfg_tags.begin(); it!=cfg_tags.end(); it++) {
      comboBoxTagCfg->addItem(QString::fromStdString(*it));
      comboBoxTagModCfg->addItem(QString::fromStdString(*it));
    }
  }
  else {
    std::vector<std::string> cfgTags;
    std::string b="Configuration-"; std::string cfgDomain = b + object_tag.toUtf8().constData();
    std::string::size_type pos = cfgDomain.find("-CFG");
    std::string cfgDomain1;
    if (pos != std::string::npos){
     cfgDomain1  = cfgDomain.substr(0,pos);
    }
    m_dbs->listTagsRem(cfgDomain1,cfgTags);
    for (std::vector<std::string>::iterator it = cfgTags.begin(); it!=cfgTags.end(); it++) {
      comboBoxTagCfg->addItem(QString::fromStdString(*it));
      comboBoxTagModCfg->addItem(QString::fromStdString(*it));
    }
  }
}

DbTagsPanel::~DbTagsPanel()
{
  delete m_dbs;
}

void DbTagsPanel::synchronise(const QString &/*tag*/) 
{
//   if (find(*comboBoxTagAlias,tag)<static_cast<unsigned int>(comboBoxTagAlias->count())) {
//     comboBoxTagAlias->setCurrentText(tag);
//   }
//   if (find(*comboBoxTagPayload,tag)<static_cast<unsigned int>(comboBoxTagPayload->count())) {
//     comboBoxTagPayload->setCurrentText(tag);
//   }
//   if (find(*comboBoxTagCfg,tag)<static_cast<unsigned int>(comboBoxTagCfg->count())) {
//     comboBoxTagCfg->setCurrentText(tag);
//   }
//   else {
//     QString cfg_tag(tag);
//     int pos = cfg_tag.findRev("-V");
//     std::cout << "INFO [DbTagsPanel::synchronise] version start = " << pos << std::endl;
//     if (pos>=0) {
//       cfg_tag.remove(pos,cfg_tag.length()-pos);
//       std::cout << "INFO [DbTagsPanel::synchronise] cfg_tag = " << cfg_tag.toLatin1().data() << std::endl;
//       if (find(*comboBoxTagCfg,cfg_tag)<static_cast<unsigned int>(comboBoxTagCfg->count())) {
// 	comboBoxTagCfg->setCurrentText(cfg_tag);
//       }
//     }

//   }
}

unsigned int DbTagsPanel::find(const QComboBox &combobox, const QString &text)
{
  assert(combobox.count()>=0);
  for (unsigned int index_i=0; index_i< static_cast<unsigned int>(combobox.count()); index_i++) {
    if (combobox.itemText(index_i)==text) return index_i;
  }
  return static_cast<unsigned int>( combobox.count() );
}

