#ifndef _KillDialogue_h_
#define _KillDialogue_h_

#include <qtimer.h>
#include <string>
#include <time.h>

namespace PixCon {

  class KillDialogue : public QObject
  {
    Q_OBJECT

  public:
    KillDialogue(QWidget *object, const std::string &name, int timeout_in_sec);
    ~KillDialogue();

    void stop() {
      m_timer.stop();
    }

  public slots:
    void offerKillOption();

  private:
    QTimer      m_timer;
    std::string m_name;
    int         m_timeout;
    ::time_t    m_startTime;
  };

}
#endif
