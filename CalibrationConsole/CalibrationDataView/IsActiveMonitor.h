/* Dear emacs, this is -*-c++-*- */
#ifndef _PixCon_IsActiveMonitor_H_
#define _PixCon_IsActiveMonitor_H_

#include "IsMonitor.h"
#include <cassert>

namespace PixCon {

  class IsActiveMonitor : public SimpleIsMonitorReceiver<int, int>
  {
  public:
    IsActiveMonitor(const std::shared_ptr<IsReceptor> &receptor,
		    const IVarNameExtractor &extractor,
		    const std::string &is_regex)
      : SimpleIsMonitorReceiver<int,int>(receptor,kAllConnTypes, 0 /* value in case the active state is deleted.*/, is_regex),
	m_varNameExtractor(&extractor)
    { assert( (SimpleIsMonitorReceiver<int, int>::receptor().get() != NULL) ); }

  protected:
    void setValue(const std::string &is_name, ::time_t time_stamp, int value);
    void registerForCleanup(const std::string &is_name, std::map<std::string, std::set<std::string> > &clean_up_var_list);

    IVarNameExtractor const *m_varNameExtractor;
  };

}
#endif
