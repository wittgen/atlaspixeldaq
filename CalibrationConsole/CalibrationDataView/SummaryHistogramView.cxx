#include "SummaryHistogramView.h"
#include "MyQRootCanvas.h"
#include <TCanvas.h>
#include <TH1.h>
#include <QStatusBar>
#include <CalibrationDataFunctorBase.h>
#include "ISelection.h"
#include <DataContainer/Average_t.h>
#include <ConfigWrapper/Connectivity.h>
#include <ConfigWrapper/ConnectivityUtil.h>
#include <CalibrationDataManager.h>
#include "SlowUpdateList.h"


namespace PixCon {

  class HistogramFunctorBase : public CalibrationDataFunctorBase
  {
  public:
    static unsigned int createMask(const VarDef_t &var_def) {
      if (var_def.connType()==kModuleValue) {
	return (1<<kModuleItem);
      }
      else if (var_def.connType()==kRodValue) {
	return (1<<kRodItem);
      }
      else if (var_def.connType()==kTimValue) {
	return (1<<kTimItem);
      }
      else if (var_def.connType()==kAllConnTypes) {
	// @todo ignore tim ?
	return (1<<kModuleItem) | (1<<kRodItem);
      }
      return 0;
    }

    HistogramFunctorBase(const ISelection &selection, bool enabled_only, const VarDef_t &var_def)
     : CalibrationDataFunctorBase(createMask(var_def)),
       m_selection(&selection),
       m_varDef(var_def),
       m_enabledOnly(enabled_only)
    {}


    /** Called for all connectivity objects which passed the filter and for which the given measurement does not exist.
     */
    void operator()( EConnItemType /*item_type*/,
		     const std::string &/*conn_name*/,
		     const std::pair<EMeasurementType,SerialNumber_t> &/*measurement*/,
		     CalibrationData::EnableState_t /*new_enable_state*/) {}

    VarId_t id() { return m_varDef.id(); }
    const VarDef_t &varDef() { return m_varDef; }

    bool use(const std::string &conn_name, CalibrationData::EnableState_t is_enable_state) const {
      bool final=(m_selection->isSelected(conn_name) && (!m_enabledOnly || is_enable_state.enabled()) );
      // std::cout << "INFO [HistogramFunctorBase::use] " << conn_name << " enabled=" << is_enable_state.enabled()
      // 		<< " selected=" << m_selection->isSelected(conn_name) << " enabled_only =" << m_enabledOnly
      // 		<< " -> " << final 
      // 		<< std::endl;
      return final;
    }

    const ISelection &selection() const { return *m_selection; }
  private:
    const ISelection *m_selection;
    VarDef_t m_varDef;
    bool m_enabledOnly;
  };

  template <class T> void checkValueType(const VarDef_t &var_def);
  template <> void checkValueType<State_t>(const VarDef_t &var_def) {
    assert(var_def.isStateWithOrWithoutHistory());
  }

  template <> void checkValueType<float>(const VarDef_t &var_def) {
    assert(var_def.isValueWithOrWithoutHistory());
  }

  class DetermineBinningBase : public HistogramFunctorBase
  {
  public:

    DetermineBinningBase(const ISelection &selection, bool enabled_only, const VarDef_t &var_def) : HistogramFunctorBase(selection, enabled_only, var_def)  { }

    virtual PixA::HistoBinning determineBinning() {
      PixA::HistoBinning binning;
      if (m_av.n()>0) {
	if (m_av.n()==1 && m_av.max()==m_av.min()) {
	  float max_val=m_av.max();
	  float min_val=m_av.min();
	  if (m_av.min()==0) {
	    float order = m_av.min() *.5;
	    if (order == 0 ) order = .5;
	    max_val=m_av.min()+order;
	    min_val=m_av.min()-order;
	  }
	  else {
	    float range = (m_av.max() - m_av.min()) *.01;
	    min_val -= range;
	    max_val += range;
	  }
	  binning = PixA::HistoBinning(1,min_val,max_val);
	}
	else {
	  m_av.calculate();
	  //	double sigma=sqrt((sum2-sum*sum/n_pixel)/(n_pixel-1));
	  //      double mean=sum/n_pixel;
	  unsigned int n_bins;
	  if (m_av.rms() > (m_av.max()-m_av.min())*1e-3) {
	    n_bins=static_cast<unsigned int>(3*(m_av.max()-m_av.min())/m_av.rms());
	    if (n_bins>300) n_bins=300;
	    else if (n_bins<100) n_bins=100;
	  }
	  else {
	    n_bins=300;
	  }

	  float width=0.5*(m_av.max()-m_av.min())/n_bins;
	  binning = PixA::HistoBinning(n_bins,m_av.min()-width,m_av.max()+width);
	}
      }
      else {
	binning = PixA::HistoBinning(1,0.,1.);
      }
      return binning;
    }

  protected:
    PixA::Average_t m_av;

  };


  template <class T>
  class DetermineBinning : public DetermineBinningBase
  {
  public:

    DetermineBinning(const ISelection &selection, bool enabled_only, const VarDef_t &var_def) : DetermineBinningBase(selection, enabled_only, var_def)  { checkValueType<T>(var_def); }

    void operator()( EConnItemType /*item_type*/,
		     const std::string &conn_name,
		     const std::pair<EMeasurementType,SerialNumber_t> &measurement,
		     PixCon::CalibrationData::ConnObjDataList &data_list,
		     CalibrationData::EnableState_t is_enable_state,
		     CalibrationData::EnableState_t /*new_enable_state*/) {

      if (this->use(conn_name, is_enable_state)) {
	try {
	  float value = data_list.newestValue<T>(measurement.first,
						 measurement.second,
						 this->varDef());
	  m_av.add(value);
	}
	catch(...) {
	}
      }
    }

  };

  template <class T>
  class HistogramFunctor : public HistogramFunctorBase
  {
  public:

    HistogramFunctor(const ISelection &selection, bool enabled_only, const VarDef_t &var_def,TH1 *h1) 
      : HistogramFunctorBase(selection, enabled_only, var_def), m_h1(h1) 
    { checkValueType<T>(var_def); }

    void operator()( EConnItemType /*item_type*/,
		     const std::string &conn_name,
		     const std::pair<EMeasurementType,SerialNumber_t> &measurement,
		     PixCon::CalibrationData::ConnObjDataList &data_list,
		     CalibrationData::EnableState_t is_enable_state,
		     CalibrationData::EnableState_t /*new_enable_state*/) {
      if (this->use(conn_name, is_enable_state)) {
	try {
	  float value = data_list.newestValue<T>(measurement.first,
						 measurement.second,
						 this->varDef());
	  m_h1->Fill(value);
	}
	catch(...) {
	}
      }
    }

  private:
    TH1 *m_h1;
  };

  class HistogramEnableFunctor : public HistogramFunctorBase
  {
  public:

    HistogramEnableFunctor(const ISelection &selection, bool enabled_only, const VarDef_t &var_def,TH1 *h1)
      : HistogramFunctorBase(selection, enabled_only,var_def),
	m_h1(h1)
    {
      const char *axis_label[]={"Other dis.","Other ena.", "ROD dis.", "ROD ena.", "Module dis.", "Mod ena." };
      for (unsigned int type_i=0; type_i<6; ++type_i) {
	m_h1->GetXaxis()->SetBinLabel(type_i+1, axis_label[type_i]);
      }
    }

    bool filter(EConnItemType /*item_type*/) const { return true; }

    void operator()( EConnItemType item_type,
		     const std::string &conn_name,
		     const std::pair<EMeasurementType,SerialNumber_t> &/*measurement*/,
		     PixCon::CalibrationData::ConnObjDataList &/*data_list*/,
		     CalibrationData::EnableState_t is_enable_state,
		     CalibrationData::EnableState_t /*new_enable_state*/) {
      try {
	if (this->use(conn_name, is_enable_state)) {

      float value=0;
      if (item_type==kRodItem) {
	value= (is_enable_state.enabled() ? 2+1 : 2);
      }
      else if (item_type==kModuleItem) {
	value= (is_enable_state.enabled() ? 4+1 : 4);
      }
      else {
	value= (is_enable_state.enabled() ? 0+1 : 0);
      }
      m_h1->Fill(value);
      }
      }
      catch(...) {
	std::cout << "INFO [HistogramEnableFunctor::operator] Caught exception." << std::endl;
      }
    }

  private:
    TH1 *m_h1;
  };


  SummaryHistogramView *SummaryHistogramView::createHistogramView( const std::shared_ptr<SlowUpdateList> &slow_update_list,
								   const std::shared_ptr<ISelection> &selection,
								   const std::shared_ptr<CalibrationDataManager> &calibration_data,
								   EMeasurementType measurement_type,
								   SerialNumber_t serial_number,
								   VarDef_t var_def,
								   bool enabled_only)
  {

    std::string var_name;
    if (var_def.isValid()) {
      var_name=PixCon::VarDefList::instance()->varName(var_def.id());
    }
    else {
      var_name="Enabled";
    }
    
    std::string title = "Histogram : ";
    title += var_name;


    PixCon::SummaryHistogramView *histogram_view= new PixCon::SummaryHistogramView(slow_update_list,
										   selection,
										   calibration_data, 
										   measurement_type,
										   serial_number, 
										   var_def,
										   enabled_only,
										   NULL);
    histogram_view->show();
    return histogram_view;
 
  }

 void SummaryHistogramView::init() {
    // VarId_t var_id = var_def.id();

    // if (!PixCon::VarDefList::instance()->isValid(var_id)) return NULL;
   {
   Lock lock(m_calibrationData->calibrationDataMutex());

   //bool determine_binning = true;

    std::unique_ptr<DetermineBinningBase> binning_functor;
    if (m_listener->varDef().isValid()) {
      if (m_listener->varDef().isValueWithOrWithoutHistory() ) {
	const ValueVarDef_t &value_def = m_listener->varDef().valueDef();
	if (value_def.minVal() < value_def.maxVal()) {
	  //determine_binning = false;

	  m_binning = PixA::HistoBinning(100,value_def.minVal(),value_def.maxVal());

	}
	else {
	  binning_functor=std::unique_ptr<DetermineBinningBase>(new DetermineBinning<float>(*m_selection, m_enabledOnly, m_listener->varDef()));
	}
      }
      else if (m_listener->varDef().isStateWithOrWithoutHistory()) {
	const StateVarDef_t &state_def = m_listener->varDef().stateDef();
	if (state_def.nStates()>0) {
	  m_binning = PixA::HistoBinning(state_def.nStates(),-.5,state_def.nStates()-.5);
	}
	else {
	  binning_functor=std::unique_ptr<DetermineBinningBase>(new DetermineBinning<State_t>(*m_selection, m_enabledOnly, m_listener->varDef()));
	}
      }
    }
    else {
      m_binning=PixA::HistoBinning(6, -.5, 6-.5);
    }

    if (binning_functor.get()) {
      m_calibrationData->forEach(m_listener->measurementType(), m_listener->serialNumber(), *binning_functor, false);
      m_binning = binning_functor->determineBinning();
    }

    // std::cout << "INFO [SummaryHistogramView::createHistogramView::] binning : "
    // 		<< binning.min() << "-" << binning.max() << " / " << binning.nBins()
    // 		<< std::endl;
   }
    updateHistoView();
  }

  void SummaryHistogramView::updateHistoView() {

    if (m_binning.min()>=m_binning.max() && m_binning.nBins()==0)  return;

    std::unique_ptr<TH1> h1_temp;

    TH1 *h1=NULL;
    TCanvas *a_canvas = canvas();
    {
      TListIter iter(a_canvas->GetListOfPrimitives());
      TObject *obj;
      while ((obj=iter.Next())) {
	if (obj->InheritsFrom(TH1::Class())) {
	  h1=static_cast<TH1 *>(obj);
	  break;
	}
      }
    }
      

    if (!h1) {
      std::string var_name;
      if (m_listener->varDef().isValid()) {
	var_name=PixCon::VarDefList::instance()->varName(m_listener->varDef().id());
      }
      else {
	var_name="Enabled";
      }
       h1_temp=std::unique_ptr<TH1>(new TH1F(var_name.c_str(), var_name.c_str(), m_binning.nBins(), m_binning.min(), m_binning.max()));
      h1_temp->SetDirectory(0);
      h1=h1_temp.get();
    }
    else {
      h1->Reset();
    }

    std::unique_ptr<HistogramFunctorBase> histogram_functor;

    if (m_listener->varDef().isValid()) {
      if (m_listener->varDef().isStateWithOrWithoutHistory()) {
	const StateVarDef_t &state_def = m_listener->varDef().stateDef();
	for (unsigned int state_i=0; state_i<state_def.nStates(); ++state_i) {
	  h1->GetXaxis()->SetBinLabel(state_i+1, state_def.stateName(state_i).c_str());
	}
	histogram_functor=std::unique_ptr<HistogramFunctorBase>(new HistogramFunctor<State_t>(*m_selection, m_enabledOnly, m_listener->varDef(), h1));
      }
      else if (m_listener->varDef().isValueWithOrWithoutHistory()) {
	histogram_functor=std::unique_ptr<HistogramFunctorBase>(new HistogramFunctor<float>(*m_selection, m_enabledOnly, m_listener->varDef(), h1));
      }
    }
    else {
      histogram_functor=std::unique_ptr<HistogramFunctorBase>(new HistogramEnableFunctor(*m_selection, m_enabledOnly, m_listener->varDef(), h1));
    }

    if (!histogram_functor.get()) {
      return;
    }

    {
      Lock lock(m_calibrationData->calibrationDataMutex());
      m_calibrationData->forEach(m_listener->measurementType(), m_listener->serialNumber(), *histogram_functor, false);
    }

    // show histogram
    if (h1_temp.get()) {
      a_canvas->cd();
      h1_temp->Draw();

      TH1 *obj = h1_temp.release();
      obj->SetBit(kCanDelete);
    }
    a_canvas->Modified();
    a_canvas->Update();
  }


  SummaryHistogramView::SummaryHistogramView(const std::shared_ptr<SlowUpdateList> &slow_update_list,
					     const std::shared_ptr<ISelection> &selection,
					     const std::shared_ptr<CalibrationDataManager> &calibration_data,
					     EMeasurementType measurement_type,
					     SerialNumber_t serial_number,
					     VarDef_t var_def,
					     bool enabled_only,
					     QWidget* parent)
: QMainWindow(parent),
      m_slowUpdateList(slow_update_list),
      m_selection(selection),
      m_calibrationData(calibration_data),
      m_listener(new Listener(this, measurement_type, serial_number,var_def)),
      m_enabledOnly(enabled_only)
  {
    setupUi(this);
    delete m_canvas;
    m_canvas = new MyQRootCanvas( centralWidget(), "m_canvas" );
    QSizePolicy spol((QSizePolicy::Policy)7, (QSizePolicy::Policy)7);
    spol.setHorizontalStretch(0);
    spol.setVerticalStretch(0);
    spol.setHeightForWidth(m_canvas->sizePolicy().hasHeightForWidth());
    m_canvas->setSizePolicy( spol );
    //QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, m_canvas->sizePolicy().hasHeightForWidth() ) );
    vboxLayout->addWidget(m_canvas);

    connect(m_canvas,SIGNAL(statusMessage(const QString &)),statusBar(),SLOT(message(const QString &)));
    m_slowUpdateList->registerListener(m_listener->measurementType(),m_listener->serialNumber(),std::shared_ptr<ICalibrationDataListener>(m_listener));
    init();
  }

  SummaryHistogramView::~SummaryHistogramView()
  {
    std::string var_name;
    if (m_listener->varDef().isValid()) {
      try {
	var_name = VarDefList::instance()->varName(m_listener->varDef().id());
      }
      catch(...) {
      }
    }
    else {
      var_name="Enabled";
    }
    std::cout << "DEBUG [SummaryHistogramView::dtor] Destructor called for object " << this << ". Deregistering listener for " 
	      << makeSerialNumberString(m_listener->measurementType(),m_listener->serialNumber())
	      << " (var = " << var_name << ").\n";
    m_slowUpdateList->deregisterListener(std::shared_ptr<ICalibrationDataListener>(m_listener));
  }

  TCanvas *SummaryHistogramView::canvas()
  {
    return m_canvas->getCanvas();
  }

  bool SummaryHistogramView::Listener::needUpdate(EMeasurementType measurement, SerialNumber_t serial_number, VarId_t var_id, const std::string &/*conn_name */) {
    if (measurement == m_measurementType && serial_number == m_serialNumber) {
      if (var_id == m_varDef.id() || (var_id == VarDefBase_t::invalid().id() && !m_varDef.isValid() )) return true;
    }
    return false;
  }
  
  void SummaryHistogramView::closeEvent(QCloseEvent * ) {
     std::cout << "DEBUG [SummaryHistogramView::closeEvent] Requested deletion of object " << this << " owned by " << parent() << ".\n";
     deleteLater();
  }

}


