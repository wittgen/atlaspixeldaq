#ifndef _PixCon_Tokeniser_h_
#define _PixCon_Tokeniser_h_

#include <string>
#include <vector>
#include <utility>
#include <cctype>
#include <cassert>
#include <cstdlib>

#include <CalibrationDataTypes.h>

namespace PixCon {

  class TokenDescription
  {
  public:
    void addOperator(const std::string &op_string, unsigned short id) { m_operatorList.push_back(std::make_pair(op_string, id)); }
    void setAllowedSpecialCharList(const std::string &allowed_special_char_string) {m_allowedSpecialVarChars = allowed_special_char_string; }
    void addAllowedSpecialChar(char special_char) {m_allowedSpecialVarChars+=special_char; }

    const std::vector< std::pair<std::string, unsigned short > > &operatorList() const { return m_operatorList; }
    const std::string &allowedSpecialChars() const {  return m_allowedSpecialVarChars; }
  private:
    std::vector< std::pair<std::string, unsigned short > > m_operatorList;
    std::string                                            m_allowedSpecialVarChars;
  };

  class Tokeniser
  {
  public:
    enum ETokenType {kValue, kName, kWildCard , kOperator, kIllegalToken, kBlockOpen, kBlockClose, kFinished, kSerialNumber };

    Tokeniser(const TokenDescription &description, const std::string *text)
      : m_tokenDescription(&description),
	m_text(text),
	m_tokenStart(0),
	m_tokenEnd(0),
        m_quoteStart('\0'),
	m_type(kIllegalToken),
	m_operatorId(0)
    {}

    ~Tokeniser() {}

    bool nextToken() {
     // bool is_string=false;
      // Need to ignore the final quotation mark, since the qutes are not part of the token
      if (m_tokenStart>0
	  && m_tokenEnd < m_text->size()
	  && m_quoteStart==(*m_text)[m_tokenEnd]
	  && ((*m_text)[m_tokenEnd]=='\"' || (*m_text)[m_tokenEnd]=='\'')) {
	m_tokenEnd++;
	m_quoteStart='\0';
	//is_string=true;
      }

      m_tokenStart=m_tokenEnd;
      removeWhiteSpaces();
      if (m_tokenStart>=m_text->size()) {
	m_type=kFinished;
	return false;
      }
      m_tokenEnd=m_tokenStart;

//       const unsigned int actual_value_length = strlen("Present Value.");
//       const unsigned int current_length = strlen("Current.");
//       // check for a serial number header
//       if (m_text->compare(m_tokenStart, current_length, "Current.")==0) {
// 	m_tokenEnd+=current_length;
// 	m_type=kSerialNumber;
// 	return true;
//       }
//       else if (m_text->compare(m_tokenStart, actual_value_length, "Present Value.")==0){
// 	m_tokenEnd+=actual_value_length;
// 	m_type=kSerialNumber;
// 	return true;
//       }
      if (firstTokenChar()=='S' || firstTokenChar()=='A') {
	std::string::size_type m_oldEnd = m_tokenEnd;
	m_tokenEnd++;
	while ( hasChars() && isdigit(nextToLastTokenChar())) m_tokenEnd++;
	if (m_tokenEnd > m_oldEnd+1) {
	  m_type=kSerialNumber;
	  return true;
	}
	// not a serial number header
	m_tokenEnd=m_oldEnd;
      }

      if ( isdigit(firstTokenChar())) {/* || firstTokenChar() == '-' || firstTokenChar() == '+' ) {*/

	char *ptr;
	m_convertedValue = strtod(c_str(),&ptr);
	m_tokenEnd = static_cast<std::string::size_type>( ptr - m_text->c_str() ) ;
	assert (m_tokenEnd > m_tokenStart && m_tokenEnd <= m_text->size() );
	m_type = kValue;

      }
      else if ( firstTokenChar()=='\"') {
	m_tokenStart++;
	m_tokenEnd++;
	while ( hasChars() && nextToLastTokenChar()!='\"') m_tokenEnd++;
	if (nextToLastTokenChar()=='\"') {
	  m_type = kName;
	  // the quotes are not part of the token
	  //	  m_tokenEnd--;
	  m_quoteStart=(*m_text)[m_tokenStart-1];
	}
	else {
	  m_tokenEnd=m_tokenStart;
	  m_type=kIllegalToken;
	}

      }
      else if ( firstTokenChar()=='\'') {
	m_tokenStart++;
	m_tokenEnd++;
	while ( hasChars() && nextToLastTokenChar()!='\'') m_tokenEnd++;
	if (nextToLastTokenChar()=='\'') {
	  // the quotes are not part of the token
	  m_type = kName;
	  m_quoteStart=(*m_text)[m_tokenStart-1];
	}
	else {
	  m_tokenEnd=m_tokenStart;
	  m_type=kIllegalToken;
	}
      }
      else if ( isalpha(firstTokenChar()) || firstTokenChar()=='_' || (firstTokenChar() == '*')) {
	m_tokenEnd++;
	const std::string &allowed_special_chars = m_tokenDescription->allowedSpecialChars();
	while ( hasChars() && (    isalnum(nextToLastTokenChar())
				|| allowed_special_chars.find(nextToLastTokenChar())!=std::string::npos) ) m_tokenEnd++;

	if (( nextToLastTokenChar() == '*' || firstTokenChar() == '*')) {
	  while ( hasChars() && (isalnum(nextToLastTokenChar())
				 || nextToLastTokenChar()=='*'
				 || allowed_special_chars.find(nextToLastTokenChar())!=std::string::npos) ) m_tokenEnd++;
	  if (m_tokenEnd - m_tokenStart>1) {
	    m_type = kWildCard;
	  }
	}
	else {
	  m_type = kName;
	}

      }
      else {

	switch ( firstTokenChar() ) {
	case '(':
	  m_type=kBlockOpen;
	  break;
	case ')': {
	  m_type=kBlockClose;
	  break;
	}
	default: {
	  m_type=kIllegalToken;
	  std::vector< std::pair< std::string, unsigned short> >::const_iterator begin_op_list =  m_tokenDescription->operatorList().begin();
	  std::vector< std::pair< std::string, unsigned short> >::const_iterator end_op_list =  m_tokenDescription->operatorList().end();
	  for (std::vector< std::pair< std::string, unsigned short> >::const_iterator op_iter = begin_op_list;
	       op_iter != end_op_list;
	       op_iter++) {

	    if (m_text->compare(m_tokenStart, op_iter->first.size(), op_iter->first)==0) {
	      m_type = kOperator;
	      m_operatorId = op_iter->second;
	      m_tokenEnd = m_tokenStart + op_iter->first.size() - 1;
	      break;
	    }
	  }

	  // illegal token
	  break;
	}
	}
	m_tokenEnd++;

      }
      return true;
    }

    ETokenType type() const        { return m_type; } 
    bool isValue() const           { return m_type == kValue; }
    bool isOperator() const        { return m_type == kOperator; }
    bool isName() const            { return m_type == kName; }
    bool isSerialNumber() const    { return m_type == kSerialNumber; }
    bool isWildcardPattern() const { return m_type == kWildCard || m_type == kName; }
    bool isIllegal() const         { return m_type == kIllegalToken; }
    bool isBlockOpen() const       { return m_type == kBlockOpen; } 
    bool isBlockClose() const      { return m_type == kBlockClose; }
    bool isDone() const            { return m_tokenStart >= m_text->size(); }

    std::string token() const      { return std::string(*m_text, m_tokenStart, m_tokenEnd-m_tokenStart); } 

    const double &convertedValue() const { return m_convertedValue;}

    EMeasurementType measurementType() const {

      assert(m_type == kSerialNumber);
      switch ( firstTokenChar()) {
      case 'A':
      case 'a':
	if (isdigit(nextTokenChar())) {
	  return kAnalysis;
	}
	break;
      case 'S':
      case 's':
	return kScan;
	break;
      case 'C':
      case 'c':
	break;
      default:
	assert( false );
	break;
      }
      // 
      return kCurrent;
    }

    SerialNumber_t serialNumber() const {
      assert(m_type == kSerialNumber);
      if (firstTokenChar()=='c' || firstTokenChar()=='C') {
	return 0;
      }
      else {
	return atoi(&(m_text->c_str()[m_tokenStart+1]));
      }
    }

    unsigned short operatorId() const   { return m_operatorId; }

    void removeWhiteSpaces() {
      while ( isspace( firstTokenChar() ) ) m_tokenStart++;
    }

    char firstTokenChar() const { return (*m_text)[m_tokenStart]; }
  protected:
    const char *c_str() const { return &((m_text->c_str())[m_tokenStart]); }
    //    char lastTokenChar() const { return (*m_text)[m_tokenEnd]; }
    char nextToLastTokenChar() const { return (*m_text)[m_tokenEnd]; }
    char nextTokenChar() const { return (*m_text)[ (m_tokenStart+1 < m_text->size() ?  m_tokenStart+1 : m_tokenStart)]; }
    bool hasChars() const { return m_tokenEnd < m_text->size(); }

  private:
    const TokenDescription *m_tokenDescription;
    const std::string *m_text;
    std::string::size_type m_tokenStart;
    std::string::size_type m_tokenEnd;
    char m_quoteStart;
    ETokenType   m_type;
    double         m_convertedValue;
    unsigned short m_operatorId;
  };

}
#endif
