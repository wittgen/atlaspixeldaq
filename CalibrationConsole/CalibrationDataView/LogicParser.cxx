#include "LogicParser.h"

namespace PixCon {

  void LogicParser::ParseNucleus(PixCon::Tokeniser &tokeniser, IEvaluator*&node_ptr, bool /* strong_expression_only */ )
  {
    if (!tokeniser.isName()) {
      if (tokeniser.isOperator()) {
	std::stringstream message;
	message << "Expected operand but got operator \"" << tokeniser.token() << "\".";
	throw MissingOperand(message.str());
      }
      else {
	std::stringstream message;
	message << "Expected operand but got \"" << tokeniser.token() << "\".";
	throw MissingOperand(message.str());
      }
    }

    std::string var_name = tokeniser.token();
    std::map<std::string, bool>::iterator value_iter = m_variables.find(var_name);
    if (value_iter == m_variables.end()) {
      std::pair< std::map<std::string, bool>::iterator, bool> ret = m_variables.insert(std::make_pair(var_name,false));
      if (!ret.second) {
	throw std::runtime_error("Failed to add boolean variable to variable list. Out of memory ?");
      }
      value_iter = ret.first;
    }
    node_ptr = new BooleanValue(value_iter);
    if ( negation() ) {
      node_ptr = Negator::negate(node_ptr);
    }

    // done with this token
    tokeniser.nextToken();
  }

}
