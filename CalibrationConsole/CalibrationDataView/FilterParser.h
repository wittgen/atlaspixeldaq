#ifndef _PixCon_FilterParser_h_
#define _PixCon_FilterParser_h_

#include "LogicParserBase.h"
#include "IEvaluator.h"

namespace PixCon {

  class EvaluatorFactory;

  class FilterParser : public LogicParserBase<EvalResult_t>
  {
  public:
    FilterParser( EvaluatorFactory &evaluator_factory);
//     ValueList<float> &valueList()               { return m_valueList; }
//     const ValueList<float> &valueList() const   { return m_valueList; }

//     ValueList<State_t> &stateList()             { return m_stateList; }
//     const ValueList<State_t> &stateList() const { return m_stateList; }

  protected:
    void ParseNucleus(PixCon::Tokeniser &tokeniser, IEvaluator *&node_ptr, bool strong_expression_only);
  private:

    EvaluatorFactory *m_factory;
    static const char *s_enabledName;
    static const char *s_disabledName;
    static const char *s_allocatedName;
    static const char *s_definedFucntionName;
  };

}

#endif
