#include "MessageDetails.h"
#include <iostream>
//#include <q3textedit.h>
//Added by qt3to4:

namespace PixCon {

  MessageDetails::MessageDetails(const QString &id_string,
				 const QString &level_string,
				 const QString &time_string,
				 const QString &method_string,
				 const QString &message_string,
				 QWidget* parent)
: QDialog(parent)
  {
    setupUi(this);
    m_idString->setText(id_string);
    m_level->setText(level_string);
    m_time->setText(time_string);
    m_method->setText(method_string);
    m_message->setText(message_string);
    show();
  }
  MessageDetails::~MessageDetails() {  }

  void MessageDetails::closeEvent(QCloseEvent * event )
  {
   QDialog::closeEvent(event);
    std::cout << "DEBUG [MessageDetails::closeEvent] called\n";
    deleteLater();
  }

}
