#ifndef _LogWindow_h_
#define _LogWindow_h_

#include <string>
#include <memory>
#include <cassert>
#include <list>
#include <vector>
#include <iostream>
#include <time.h>
#include "Lock.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QComboBox>
#include <QLabel>
#include <QPushButton>

#include <QTableWidget>
#include <QEvent>
#include <QString>

#include "OStreamRedirector.h"

class QTimer;
class QApplication;


namespace PixCon {

  //  class StdStreamListener;
  //  class StdStreamRedirector;
  typedef ::IStreamListener StdStreamListener;
  typedef ::OStreamRedirector<> StdStreamRedirector;

    class Message_t
    {
    public:
      enum ELevel {kDiagnostic, kCalibration, kInformation, kWarning, kError, kFatal, kNLevels};
      Message_t(ELevel level, const std::string &message, const std::string &location, time_t time_stamp)
	: m_repetition(0),
	  m_time(time_stamp),
	  m_message(message),
	  m_location(location),
	  m_level(level),
	  m_repeatedNMessages(0)
      {}

      Message_t(ELevel level, const std::string &message, const std::string &location)
	: m_repetition(0),
	  m_time( ::time(NULL) ),
	  m_message(message),
	  m_location(location),
	  m_level(level),
	  m_repeatedNMessages(0)
      {}

      const std::string &message() const  { return m_message; }
      const std::string &location() const { return m_location; }
      const time_t &time() const          { return m_time; }
      ELevel level() const                { return m_level; }

      static const char *levelName(ELevel level) { assert(level < kNLevels); return s_levelName[level]; }

      unsigned int repetitions() const    { return m_repetition; }
      const time_t &lastTime() const      { return m_lastTime; }

      void repeat(unsigned char n_messages, time_t time_stamp) {
	m_lastTime = time_stamp;
	m_repetition++;
	m_repeatedNMessages=n_messages;
      }

      void repeat(unsigned char n_messages) {
	m_lastTime = ::time(NULL);
	m_repetition++;
	m_repeatedNMessages=n_messages;
      }

      unsigned char repeatedNMessages() const { return m_repeatedNMessages; }

      bool operator==(const Message_t &a) const {
	if (m_level==a.m_level && m_message == a.m_message && m_location == a.m_location) {
	  return true;
	}
	else {
	  return false;
	}
      }

    private:
      unsigned int m_repetition;
      time_t      m_time;
      time_t      m_lastTime;
      std::string m_message;
      std::string m_location;
      ELevel      m_level;
      static const char *s_levelName[kNLevels];
      unsigned char   m_repeatedNMessages;
    };



  class LogWindow : public QWidget
  {
    Q_OBJECT

  public:
    LogWindow(unsigned int messages_max, const std::string &log_file_name, QApplication *application, QWidget *parent = 0 );
    ~LogWindow();

    void addMessage( const Message_t &message );
    void restoreStream();
    void shutdown();
    static void removeOldLogFiles(const std::string &name_pattern, unsigned int keep_n_days);
    Message_t::ELevel m_verbosityLevel;

    std::array<size_t, Message_t::kNLevels> m_allMsgCount;
    std::array<size_t, Message_t::kNLevels> m_hidMsgCount;

  public slots:
    void updateMessageView();
    void changeVerbosityLevel(QString const & newVerbosityLevel) {
      std::cout << "FATAL [changeVerbosityLevel] changing to " << newVerbosityLevel.toStdString() << "\n";
      if( newVerbosityLevel == QString("INFO")) {
        m_verbosityLevel = Message_t::kInformation;
	std::cout << "INFO [changeVerbosityLevel] changing to kInfo\n";
      } else if (newVerbosityLevel == QString("WARNING")) {
        m_verbosityLevel = Message_t::kWarning;
        std::cout << "WARNING [changeVerbosityLevel] changing to kWarning\n";
      } else if (newVerbosityLevel == QString("DEBUG")) {
        m_verbosityLevel = Message_t::kDiagnostic;
        std::cout << "DEBUG [changeVerbosityLevel] changing to kDiagnostic\n";
      } else if (newVerbosityLevel == QString("CALIB")) {
        m_verbosityLevel = Message_t::kCalibration;
        std::cout << "CALIB [changeVerbosityLevel] changing to kCalibration\n";
      } else if (newVerbosityLevel == QString("ERROR")) {
        m_verbosityLevel = Message_t::kError;
        std::cout << "ERROR [changeVerbosityLevel] changing to kError\n";
      } else if (newVerbosityLevel == QString("FATAL")) {
        m_verbosityLevel = Message_t::kFatal;
        std::cout << "FATAL [changeVerbosityLevel] changing to kFatal\n";
      } else {
        m_verbosityLevel = Message_t::kDiagnostic;
        std::cout << "DEBUG [changeVerbosityLevel] changing to kDiagnostic\n";
      }
    }
    void showDetails( int row, int col );

  signals:
    void hiddenMessage(const QString &);
  protected:
    bool event(QEvent *an_event);

  private:

    Mutex m_messageListMutex;
    std::list<Message_t> m_messageList;
    std::unique_ptr<StdStreamListener>    m_listener;
    std::vector<StdStreamRedirector *>  m_redirector;

    QVBoxLayout* m_mainLogLayout;
    QHBoxLayout* m_botBarLayout;
    QTableWidget* m_logTable;
    QComboBox* m_logVerbosityComboBox;
    QLabel* m_verbosityLabel;
    QLabel* m_numberMessagesLabel;
    QLabel* m_logFileNameLabel;
    QPushButton* m_deleteLogsButton;

    QApplication *m_application;
    QTimer       *m_timer;

    unsigned int m_messageCounter;
    unsigned int m_messagesMax;
  };

}
#endif
