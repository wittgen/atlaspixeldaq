#ifndef _DetectorView_h_
#define _DetectorView_h_

#include "ui_DetectorViewBase.h"
#include "DetectorViewList.h"
#include <ConfigWrapper/Connectivity.h>
#include <CalibrationDataTypes.h>
#include <CalibrationDataStyle.h>
#include "SlowUpdateList.h"

#include <memory>
#include "Lock.h"
#include <qtimer.h>
#include <qmatrix.h>
//Added by qt3to4:
#include <QEvent>

class DetectorCanvasView;
class PaletteAxisItem;

namespace PixDet {
  class Detector;
  class ILogoFactory;
}

namespace PixCon {
  class CalibrationDataManager;
  class CalibrationDataPalette;
  class DataSelection;
  class CategoryList;
  class IConnItemFactory;
  class FocussedItemSelection;
  class DetectorCanvasTip;
  class SlowUpdateList;
}


class DetectorView : public QWidget, public Ui_DetectorViewBase
{
  friend class PixCon::DetectorViewList;
  friend class PixCon::FocussedItemSelection;
  Q_OBJECT

public:
  DetectorView(PixCon::DetectorViewList &detector_view_list,
	       bool setup_calibration_data,
	       std::shared_ptr<PixCon::IContextMenu> &context_menu,
	       const std::shared_ptr<PixDet::ILogoFactory> &logo_factory,
	       const std::shared_ptr<PixCon::SlowUpdateList> &slow_update_list,
	       QApplication &app,
           QWidget* parent = nullptr );
  ~DetectorView();

  virtual void updateChanged() { update(true); }
  void updateCategories(std::shared_ptr<PixCon::CategoryList> &categories);
  void hideSerialNumberSelector();
  void clearAllSerialNumbers();
  void addSerialNumber(const std::string &serial_number_string);
  void removeSerialNumber(const std::string &serial_number_string);
  void addCurrentSerialNumber() { addSerialNumber(s_currentValueLabel); }

 public slots:
  virtual void drawDetectorModel();
  virtual void changedVariable(const QString &, const QString &);
 public:
  void selectSerialNumber(PixCon::EMeasurementType measurement_type, PixCon::SerialNumber_t serial_number);

  PixCon::EMeasurementType selectedMeasurementType() const {
    return m_dataSelection->type();
  }

  PixCon::SerialNumber_t selectedSerialNumber() const {
    return m_dataSelection->serialNumber();
  }

  const PixCon::VarDef_t &selectedVarId() const {
    return m_dataSelection->varId();
  }

  virtual bool updateConnItem(PixCon::EConnItemType conn_type,
			      const std::string &conn_name,
			      PixCon::EMeasurementType measurement_type,
			      PixCon::SerialNumber_t serial_number);

  virtual bool updateConnItem(PixCon::EConnItemType conn_type,
			      const std::string &conn_name,
			      PixCon::EMeasurementType measurement_type,
			      PixCon::SerialNumber_t serial_number,
			      bool propagate_to_childs);

  void copySelection(const DetectorView &a);

  std::shared_ptr<PixCon::CalibrationDataManager> calibrationDataManagerPtr() { return m_calibrationData; }
  std::shared_ptr<PixCon::DataSelection>          dataSelectionPtr() { return m_dataSelection; }

  QApplication &app() {return *m_app;}

  void copySerialNumbers( const DetectorView &a_view);

  std::shared_ptr<PixCon::IContextMenu>          &contextMenu() { return m_contextMenu; }

 protected:
  virtual bool isFocussed(const std::string &/*name*/) const { return true;}
  void update(bool redraw_updates_only);
  const std::shared_ptr<PixCon::SlowUpdateList> &slowUpdateList() { return m_slowUpdateList; }

 public slots:
  void changedCategory(const QString & the_category_name);
  void changedVariable(const QString & variable_name);
  void changedSerialNumber(const QString & serial_number);
  void cloneView();
  void zoomIn();
  void zoomOut();
  void rotateView();

  void printToFile();
  void buildMessage(const QString &, const QRectF &);
  void selectItem(const QString &, const QRectF &);

  void showAxisRangeDialog();
  void changeAxis(float min, float max);
  void cycleStates(unsigned int new_page);
  void buildMessage(PaletteAxisItem *axis_item, const QRectF &rect);

  void changeDataSelection(PixCon::EMeasurementType measurement_type, PixCon::SerialNumber_t serial_number);

  /** To be called if the category has been changed for the given measurement.
   * @param measurement_type the type of the measurement  (scan or analysis).
   * @param serial_number the serial number of the measurement.
   * @param category_name the name of the category.
   */
  void updateCategory(PixCon::EMeasurementType measurement_type, PixCon::SerialNumber_t serial_number, const QString &category_name);

  /** Create a histogram of the currently displayed variable.
   */
  void histogramCurrentVariable() { histogramCurrentVariable(false); }
  void histogramCurrentVariableEnabledItems() { histogramCurrentVariable(true);}
  void histogramCurrentVariable(bool enabled_only);

  void selectConnItemOnCanvas(PixCon::EConnItemType type, const std::string &conn_name);

  void sAdjustRange();
  void sFullRange();
  void sCycleStates();
  void sShowHistogram();
  void sShowHistogramEnabled();

 private slots:
  void updateView();

 signals:
  void statusMessage(const QString &);
  void changedDataSelection();
  void axisChangeRequest(float min, float max);
  void axisChangeRequest(unsigned int new_page);

 private:
  void initCalibrationData();

  void updateZoomViewRedraw(bool redraw_updates_only);

  void requestRedraw() {
      if (!m_update) {
          m_update=true;
          if (!m_redrawTimer->isActive()) {
              m_redrawTimer->setSingleShot(true);
              m_redrawTimer->start((s_redrawTime > 0) ? s_redrawTime : 0);
          }
      }
  }

  void drawPalette();
  void updateTitle();

  void _changeDataSelection(PixCon::EMeasurementType measurement_type, PixCon::SerialNumber_t serial_number);

  enum EDetectorModel {kFull, kFullOld, kToothPix};
  EDetectorModel guessDetectorModel(const PixA::ConnectivityRef &connectivity);

  PixCon::CalibrationDataManager                    &calibrationDataManager() { return *m_calibrationData; }
  std::shared_ptr<PixCon::CalibrationDataPalette> &calibrationDataPalette() { return m_detectorViewList->calibrationDataPalette(); }

  bool event(QEvent *an_event);

  PixCon::DetectorViewList                         *m_detectorViewList;
  std::shared_ptr<PixCon::CalibrationDataManager> m_calibrationData;
  std::shared_ptr<PixCon::DataSelection>          m_dataSelection;
  std::shared_ptr<PixCon::IContextMenu>           m_contextMenu;

  std::unique_ptr<PixDet::Detector>                 m_detector;
  DetectorCanvasView                               *m_detectorView;
  PixA::ConnectivityRef                             m_factoryConn;
  std::unique_ptr<PixCon::IConnItemFactory>         m_factory;
  std::shared_ptr<PixDet::ILogoFactory>             m_logoFactory;
  std::shared_ptr<PixCon::SlowUpdateList>           m_slowUpdateList;

  static std::string s_currentValueLabel;

  std::unique_ptr<PixCon::DetectorCanvasTip>          m_tip;

  QTimer             *m_redrawTimer;
  bool                m_update;
  static const unsigned int s_redrawTime = 300;
  unsigned int        m_zoomLevel;
  unsigned int        m_rotateLevel;
  QMatrix            m_normalMatrix;
  static constexpr std::array<float,12> s_zoomLevelList {0.5, 0.66, 0.75, 0.9, 0.95, 1, 1.3, 1.6, 2, 2.5, 3, 4};
  static constexpr std::array<int, 3> s_rotatedView {0, 90, 180};

  QApplication       *m_app;

  EDetectorModel      m_detectorModel;  
};

#endif
