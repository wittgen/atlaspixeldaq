#ifndef __SAVE_DISABLE_H__
#define __SAVE_DISABLE_H__
#include "ui_SaveDisableBase.h"
#include "CalibrationDataUtil.h"
#include <utility>
#include <ConfigWrapper/Connectivity.h>
#include <Highlighter.h>

#include <qcolor.h>
#include <memory>
#include <memory>

#include "ISelectionFactory.h"

namespace PixCon {

  class CalibrationDataManager;
  class IConnItemSelection;

  class SaveDisable : public QDialog, public Ui_SaveDisableBase
  {
  public:
    SaveDisable(PixA::ConnectivityRef conn,
		const std::pair<PixCon::EMeasurementType, PixCon::SerialNumber_t>  &measurement,
		const std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data_manager,
		ISelectionFactory &selection_factory,
		QWidget* parent = 0);
    ~SaveDisable();

    void execute();
    bool saveDisable();

  private:
    bool nameCorrect(const std::string &name);

    PixA::ConnectivityRef      m_conn;
    const std::pair<PixCon::EMeasurementType, PixCon::SerialNumber_t>  m_measurement;
    std::shared_ptr<PixCon::CalibrationDataManager>                  m_calibrationDataManager;
    ISelectionFactory         *m_selectionFactory;

    QColor                     m_yellow;
    std::unique_ptr<Highlighter> m_highlighter;

  };

}
#endif
