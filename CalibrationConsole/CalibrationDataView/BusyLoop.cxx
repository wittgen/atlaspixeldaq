#include "BusyLoop.h"
#include <QMessageBox>
#include <QScrollBar>
#include <QMetaMethod>

#include <iostream>
#include <LogWindow.h>
#include <MessageDetails.h>

unsigned int wait_counter=0;
unsigned int unlock_counter=0;
BusyLoop::BusyLoop(QApplication *app,
		   QDialog* dialog,
		   unsigned int dialog_after_time,
		   unsigned int update_time)
  : m_app(app),
    m_eventFilter(dialog),
    m_dialog(std::unique_ptr<QDialog>(dialog)),
    m_showTimer(QTimer(this)),
    m_updateTime(update_time),
    m_abort(false),
    m_veto(false),
    m_vetoConnected(false),
    m_exitFlag(false)
{

  if (m_dialog.get()) {
 //   QMetaObject meta_obj = m_dialog->metaObject();
 const   QMetaObject* meta_obj = m_dialog->metaObject();
    if (meta_obj) {
QStringList signal_names;
 for(int i = meta_obj->methodOffset(); i < meta_obj->methodCount(); ++i){
#if QT_VERSION < 0x050000
   signal_names << QString::fromLatin1(meta_obj->method(i).signature());
#else
   signal_names << QString::fromLatin1(meta_obj->method(i).methodSignature());
#endif
 }

 if(signal_names.contains("vetoAbort()", Qt::CaseInsensitive) && signal_names.contains("releaseVeto()", Qt::CaseInsensitive)) {
	connect(m_dialog.get(),SIGNAL(vetoAbort()), this, SLOT(vetoAbort()) );
	connect(m_dialog.get(),SIGNAL(releaseVeto()), this, SLOT(releaseVeto()) );
	m_vetoConnected=true;
      }

    }
  }

  if (m_app->hasPendingEvents() ) {
    m_app->processEvents();
  }

  connect(&m_showTimer, SIGNAL(timeout()), this, SLOT(showDialog()));
  m_showTimer.setSingleShot(true);
  m_showTimer.start((dialog_after_time) > 0 ? dialog_after_time : 0);
  m_app->installEventFilter(&m_eventFilter);
  QApplication::setOverrideCursor( Qt::WaitCursor );
  //   s_evOut=std::unique_ptr<std::ostream>(new std::ofstream("/tmp/goetz/ev.out",std::ios_base::app));
  //   (*s_evOut) << "INFO [BusyLoop::ctor]" << std::endl;

  // have to unlock to application such that this thread can process events
  // JGK TO DO
  //m_app->unlock(false);
  m_exitFlag=true;
  m_lockCounter=1;
  m_isLocked=true;
  m_started=false;
  wait_counter = 0;
  unlock_counter=0;
  unsigned int last_lock_counter = m_lockCounter;
  m_startMutex.lock();
  DoSetup(cThread); //added by MJK
  moveToThread(&cThread); // added by MJK
    cThread.start(); // added by MJK ==> start signal emition will call DoWork()

  // wait until the new GUI thread has ensured that the application is unlocked.
  if (!m_started) {
    do {
      if (m_isLocked && last_lock_counter != m_lockCounter) {
	// JGK TO DO
	//m_app->unlock(false);
	last_lock_counter = m_lockCounter;
	unlock_counter++;
	m_sleep.wakeOne();
      }

      if (m_isLocked) {
	m_start.wait(&m_startMutex);
      }
      wait_counter++;
    } while (m_isLocked);
    m_startMutex.unlock();
  }
}

BusyLoop::~BusyLoop() {
  //  assert( MessageEvent::counter() == 0);
  closeWait();
  m_app->removeEventFilter(&m_eventFilter);
  QApplication::restoreOverrideCursor();

  // now revert the unlocking
  // JGK TO DO
//   while (m_lockCounter-->0) {
//     m_app->lock();
//   }
  // end JGK TO DO


 // m_app->wakeUpGuiThread();

  //   (*s_evOut) << "INFO [BusyLoop::dtor]" << std::endl;
  //   s_evOut.reset();
}

class FlagHelper
{
public:
  FlagHelper(QMutex &mutex, std::atomic<bool> &flag) : m_mutex(&mutex), m_flag(flag) { }
  ~FlagHelper() { QMutexLocker lock(m_mutex); m_flag=true; }
private:
  QMutex *m_mutex;
  std::atomic<bool>   & m_flag;
};

void BusyLoop::DoWork() {

  //  AppLockHelper app_unlocker( m_app );

  m_startMutex.lock();
  // JGK TO DO
//   while (m_app->locked()) {
//     m_lockCounter++;
//     m_isLocked=true;
//     m_start.wakeOne();
//     m_sleep.wait(&m_startMutex);
//   }

  m_isLocked=false;
  m_started=true;
  m_startMutex.unlock();
  m_start.wakeOne();

  {
    QMutexLocker lock(&m_exitMutex);
    m_exitFlag=false;
  }

  //  std::cout << "INFO [BusyLoop::run] start." << std::endl;
  {
    FlagHelper flag(m_exitMutex, m_exitFlag);

  while (!m_abort || m_veto) {
    //    std::cout << "INFO [BusyLoop::run] process events." << std::endl;
  // JGK TO DO
//     if (m_app->hasPendingEvents() && !m_app->locked()) {
//       m_app->processEvents();
//     }
    //Karolos ToFix: thread waiting for itself --  cThread.wait(m_updateTime);
  }
  //  hide();
  //  m_app->processEvents();

  //  std::cout << "INFO [BusyLoop::run] done." << std::endl;
  }
  m_exit.wakeAll(); emit isFinished();
}


void BusyLoop::DoSetup(QThread &cThreadArg)
{
    connect(&cThreadArg,SIGNAL(started()),this,SLOT(DoWork()));
    connect(this,SIGNAL(isFinished()),&cThreadArg,SLOT(quit()));

}

void BusyLoop::close() {
  m_abort = true;
  m_sleep.wakeOne();
}

void BusyLoop::closeWait() {

  m_abort = true;
  if (cThread.isRunning()) {
    m_sleep.wakeOne();
    m_exitMutex.lock();
    while (!m_exitFlag) {
      m_exit.wait(&m_exitMutex);
    }
    m_exitMutex.unlock();
    while (cThread.isRunning()) {
      //      bool is_finished =finished();
      //      bool is_running = running();
      //      std::cout << "INFO [BusyLoop] finished = " << is_finished << " running = " << is_running << std::endl;
      //::sleep(1);
      //      msleep(10);
       emit isFinished();
    }
  }
}

void BusyLoop::showDialog()
{
  if (m_dialog.get()) {
    m_dialog->show();
  }
}

bool BusyLoop::event(QEvent *an_event)
{
  if (an_event->type()==QEvent::User) {
    MessageEvent *message = dynamic_cast<MessageEvent *>(an_event);
    if (message) {
      emit updateMessage(message->message());
      return true;
    }
  }
  return QObject::event(an_event);
}

bool BusyLoop::InputFilter::eventFilter( QObject *obj, QEvent *an_event )
{
  QObject *parent=obj;
  while (parent) {
    if (parent == m_busyDialogue || dynamic_cast<QMessageBox*>(parent) ) {
      return false;
    }
    else if (dynamic_cast<PixCon::LogWindow *>(parent) ) return false;
    parent=parent->parent();
  }

  // allow to move scrollbars and splitters
//   if (dynamic_cast<QSplitterHandle *>(obj) ) {
//     (*s_evOut) << "INFO [BusyLoop::InputFilter::eventFilter]  " << obj->name() << " / " << obj->className() << " QSplitter event = " << an_event->type() << std::endl;
//     return false;
//   }
  if (dynamic_cast<QScrollBar *>(obj) ) return false;
  if (dynamic_cast<PixCon::LogWindow *>(parent) ) return false;

  switch (an_event->type()) {
//   case QEvent::None:
//   case QEvent::Timer:
  case QEvent::Close: {
    if (dynamic_cast<PixCon::MessageDetails *>(obj) ) {
      return false;
    }
  }
  case QEvent::MouseButtonPress:
  case QEvent::MouseButtonRelease: {
    if (obj->inherits("QSplitterHandle")) {
      return false;
    }
  }
  case QEvent::MouseButtonDblClick:
//   case QEvent::MouseMove:
  case QEvent::KeyPress:
  case QEvent::KeyRelease:
    // focus events ?
  case QEvent::FocusIn:
  case QEvent::FocusOut:
  case QEvent::Enter:
  case QEvent::Leave:
//   case QEvent::Paint:
//   case QEvent::Move:
//   case QEvent::Resize:
  case QEvent::Create:
  case QEvent::Destroy:
//   case QEvent::Show:
//   case QEvent::Hide:
  case QEvent::Quit:
//   case QEvent::Reparent:
//   case QEvent::ShowMinimized:
//   case QEvent::ShowNormal:
//   case QEvent::WindowActivate:
//   case QEvent::WindowDeactivate:
//   case QEvent::ShowToParent:
//   case QEvent::HideToParent:
//   case QEvent::ShowMaximized:
//   case QEvent::ShowFullScreen:
  case QEvent::Shortcut:
  case QEvent::Wheel:
//   case QEvent::AccelAvailable:
//   case QEvent::CaptionChange:
//   case QEvent::IconChange:
//   case QEvent::ParentFontChange:
//   case QEvent::ApplicationFontChange:
//   case QEvent::ParentPaletteChange:
//   case QEvent::ApplicationPaletteChange:
//   case QEvent::PaletteChange:
  case QEvent::Clipboard:
  case QEvent::Speech:
//   case QEvent::SockAct:
  case QEvent::ShortcutOverride:
  case QEvent::DeferredDelete:
  case QEvent::DragEnter:
  case QEvent::DragMove:
  case QEvent::DragLeave:
  case QEvent::Drop:
  case QEvent::DragResponse:
//   case QEvent::ChildInserted:
//   case QEvent::ChildRemoved:
//   case QEvent::LayoutHint:
//   case QEvent::ShowWindowRequest:
//   case QEvent::WindowBlocked:
//   case QEvent::WindowUnblocked:
  case QEvent::ActivateControl:
  case QEvent::DeactivateControl:
  case QEvent::ContextMenu:
 // case QEvent::InputMethodStart: 
 // case QEvent::InputMethodCompose:
 // case QEvent::InputMethodEnd:
case QEvent::InputMethod:
 // case QEvent::Accessibility:
//   case QEvent::TabletMove:
//   case QEvent::LocaleChange:
//   case QEvent::LanguageChange:
//   case QEvent::LayoutDirectionChange:
//   case QEvent::Style:
  case QEvent::TabletPress:
  case QEvent::TabletRelease:
  case QEvent::OkRequest:
  case QEvent::HelpRequest: {
    //  case QEvent::IconDrag:
    // (*s_evOut)
    //    std::cout << "INFO [BusyLoop::InputFilter::event] Filter event of type " << an_event->type() << " for object "
    //     	       <<  obj->name() << " / " << obj->className() << " removed " << std::endl;

    return true;  // remove event
  }
//   case QEvent::WindowStateChange:
  default: {
    return false;
  }
  }
}

