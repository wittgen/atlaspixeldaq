#ifndef _MetaDataList_H_
#define _MetaDataList_H_
#include <QTreeWidgetItem>
#include "ui_MetaDataListBase.h"
#include <memory>
#include <CalibrationDataTypes.h>
#include "MetaDataDetails.h"
#include "IMetaDataUpdateHelper.h"
#include <QVBoxLayout>
//class Q3ListViewItem;
class QTreeWidgetItem;
class QPoint;
class Highlighter;

namespace PixA {
  class Regex_t;
}

namespace PixCon {

  class CalibrationDataManager;
  class CalibrationDataPalette;
  class CategoryList;
  class MetaDataItem;
  class MetaDataItemBase;

  class AnalysisMetaData_t;
  class ScanMetaData_t;

  class VariableContextMenuAction;

  class ShowScanConfigAction;
  class ShowAnalysisConfigAction;

  class ISelectionFactory;

  class MetaDataList : public QDialog, public Ui_MetaDataListBase
  {
    friend class MetaDataItem;
    friend class ShowScanConfigAction;
    friend class ShowAnalysisConfigAction;
    friend class VariableContextMenuAction;
    Q_OBJECT

  public:
    MetaDataList(const std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data,
		 const std::shared_ptr<PixCon::CalibrationDataPalette> &palette,
		 const std::shared_ptr<PixCon::CategoryList> &categories,
		 ISelectionFactory *selection_factory,
		 const std::shared_ptr<PixCon::IMetaDataUpdateHelper> &update_helper,
		 QWidget* parent = 0 );

    ~MetaDataList();


    bool hasMetaData(EMeasurementType measurement_type, SerialNumber_t serial_number) const;
    void updateMetaData(EMeasurementType measurement_type, SerialNumber_t serial_number);

    void addMetaData(EMeasurementType measurement_type, SerialNumber_t serial_number);
    void removeMetaData(EMeasurementType measurement_type, SerialNumber_t serial_number);

    void selectMeasurement(EMeasurementType measurement_type, SerialNumber_t serial_number);

    /** Rebuild the whole meta datalist for the scan and analysis meta data catalogues
     */
    void rebuildMetaDataList();

    /** Add child items to a measurement.
     * The items will comprise variables and histograms.
     */
  //  void showHideVariables(Q3ListViewItem *item);

   // void clearFilter();

 //   void changeFilter();

  signals:
    /** Signal to be emitted when a new measurement got selected.
     * @param measurement_type the type of the measurement  (scan or analysis).
     * @param serial_number the serial number of the measurement.
     * This signal can be used to synchronise the selection of a detector view.
     */
    void changeDataSelection(PixCon::EMeasurementType, PixCon::SerialNumber_t);

    /** Signal to be emitted when a measurement was removed from the meta data list.
     * @param measurement_type the type of the measurement  (scan or analysis).
     * @param serial_number the serial number of the measurement.
     * This signal can be used to remove the associated calibration data, information
     * in the meta data catalogues and the corresponding connectivity from memory.
     */
    void removeMeasurement(PixCon::EMeasurementType, PixCon::SerialNumber_t);

    /** Signal to be emitted when a variable was selected from a measurement.
     */
    void changeVariable(const QString &category_name, const QString &variable_name);

    /** Signal to be emitted when the variable context menu is to be shown.
     */
    void showVariableContextMenu();

    /** Signal to be emitted when a scan configuration is to be shown.
     */
    void showScanConfiguration(PixCon::EMeasurementType, PixCon::SerialNumber_t);

    /** Signal to be emitted when an analysis configuration is to be shown.
     */
    void showAnalysisConfiguration(PixCon::EMeasurementType, PixCon::SerialNumber_t, unsigned short);

    /** Emitted if a measurement should be added.
     */
    void addMeasurement();

//     void updateQuality(PixCon::EMeasurementType measurement_type, SerialNumber_t current_serial_number, int new_quality);
//
//     void updateComment(PixCon::EMeasurementType measurement_type, SerialNumber_t current_serial_number, const std::string &comment);
public slots:
/*void select(Q3ListViewItem *item);
void showContextMenu(Q3ListViewItem *item, const QPoint&, int);
void showHideVariables(Q3ListViewItem *item);
void open(Q3ListViewItem *item);*/
void select(QTreeWidgetItem *item, int);
void select(QTreeWidgetItem *item);
void showContextMenu(const QPoint&);
void showHideVariables(QTreeWidgetItem *item);
void open(QTreeWidgetItem *item);
//void select(int button,QTreeWidgetItem *item, const QPoint&, int);
void clearFilter();
void changeFilter();
//new slots for QMenu
void AaddMeasurement();
void showScanParameter();
void showAnalysisParameter();
void showClassificationPar();
void showPostProcessingParameter();
void AcombineAnalysisResults();
void dumpVariables();
void remove();


  protected slots:
    void updateMetaDataItem(PixCon::EMeasurementType, PixCon::SerialNumber_t);

    void combineAnalysisResults();
    void showHideDetails();

  protected:
    void showDetails(EMeasurementType measurement_type, SerialNumber_t serial_number);
    void showNoDetails();
    void updateDetails();
    QVBoxLayout* m_detailsLayout;
    void addScanMetaData(SerialNumber_t serial_number, const  ScanMetaData_t * scan_meta_data);

    void addAnalysisMetaData(SerialNumber_t analysis_serial_number, 
			     const  AnalysisMetaData_t * analysis_meta_data);

    void addAnalysisMetaData(QTreeWidgetItem *scan_meta_data_item,
			     SerialNumber_t analysis_serial_number,
			     const  AnalysisMetaData_t * analysis_meta_data);

  //  void select(Q3ListViewItem *item);

  //  void select(int button, Q3ListViewItem *item, const QPoint&, int);

   // void showContextMenu(Q3ListViewItem *item, const QPoint&, int);

    void showMetaDataContextMenu(QTreeWidgetItem *item, const QPoint&, int);

//    void open(Q3ListViewItem *item);

    PixCon::CalibrationDataPalette &palette() {return m_metaDataDetails->palette(); }

    PixCon::CalibrationDataManager &calibrationData() {return m_metaDataDetails->calibrationData(); }

    std::pair<QBrush , QPen > statusColour(MetaData_t::EStatus status)  { return m_metaDataDetails->statusColour(status); }

    void clearPatternList();
    void updatedVariableLists();

  private:
    using QDialog::open;
    std::unique_ptr<ISelectionFactory>                  m_selectionFactory;
    std::shared_ptr<PixCon::CategoryList>           m_categories;

    std::vector< std::pair<PixA::Regex_t *, bool> >   m_variableFilter;
    std::unique_ptr< Highlighter > m_highlighter;

    MetaDataDetails  *m_metaDataDetails;
    //Q3ListViewItem 	 *m_Q3ListViewItem;
    QTreeWidgetItem *m_QTreeWidgetItem;
  };

  

}//end of namespace

//Q_DECLARE_METATYPE(PixCon::MetaDataList) //has to be outside namespace 

#endif
