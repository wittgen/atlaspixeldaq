#include "MetaDataDetails.h"
#include "IMetaDataUpdateHelper.h"
#include <qmessagebox.h>

namespace PixCon {

  std::vector<QColor>                                MetaDataDetails::s_tagColor;
  std::vector<QColor>                                MetaDataDetails::s_tagForegroundColor;
  std::vector<DefaultColourCode::EDefaultColourCode> MetaDataDetails::s_colorMap;

  void MetaDataDetails::initialise() {
    if (s_colorMap.size()<MetaData_t::kNStates || !s_initialise) {
      //	s_color.clear();
	// 	s_color.push_back(QColor("darkgreen"));
	// 	s_color.push_back(QColor("red"));
	// 	s_color.push_back(QColor(230,196,0));
	// 	s_color.push_back(QColor("cyan"));
      s_colorMap.resize(MetaData_t::kNStates);
      s_colorMap[MetaData_t::kOk]= DefaultColourCode::kOk;
      s_colorMap[MetaData_t::kFailed]= DefaultColourCode::kFailed;
      s_colorMap[MetaData_t::kUnknown]= DefaultColourCode::kUnknown;
      s_colorMap[MetaData_t::kRunning]= DefaultColourCode::kRunning;
      s_colorMap[MetaData_t::kLoading]= DefaultColourCode::kConfigured;
      s_colorMap[MetaData_t::kAborted]= DefaultColourCode::kOff;

      // to be used for the text in case of failed scans
      s_tagColor.reserve(kLost+1);
      s_tagColor.push_back(QColor("grey"));
      s_tagColor.push_back(QColor("grey"));
      s_tagColor.push_back(QColor("red"));
      //	s_color.push_back(QColor("white"));
      s_initialise=true;
    }
  }

  bool MetaDataDetails::s_initialise = false;

  const char *MetaDataDetails::s_statusName[MetaData_t::kNStates] = {"OK", "Failed", "Aborted","Unknown", "Running", "Loading"};

  const char *MetaDataDetails::s_qualityName[MetaData_t::kNQualityStates] = { "Unknown","Normal", "Trash","Debug"  };


  MetaDataDetails::MetaDataDetails(const std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data,
				   const std::shared_ptr<PixCon::CalibrationDataPalette> &palette,
				   const std::shared_ptr<PixCon::IMetaDataUpdateHelper>  &update_helper,
				   QWidget* parent)
    : QWidget(parent),
      m_calibrationData(calibration_data),
      m_palette(palette),
      m_updateHelper(update_helper)
    {
      setupUi(this);
      for (unsigned int changable_property_i=0; changable_property_i<2; ++changable_property_i) {
	m_changed[changable_property_i]=false;
      }

      initialise();
      if (s_tagColor.size()!=3) {
	s_tagColor.clear();
	/*
	s_tagColor.push_back(m_name->paletteBackgroundColor());    // normal
	s_tagColor.push_back(m_cfgTagLabel->paletteBackgroundColor());     // temporary
	s_tagColor.push_back(m_modCfgTagLabel->paletteBackgroundColor());  // lost
	*/
	s_tagColor.push_back(m_name->palette().color(QPalette::Active, QPalette::Window));    // normal
	s_tagColor.push_back(m_cfgTagLabel->palette().color(QPalette::Active, QPalette::Window));     // temporary
	s_tagColor.push_back(m_modCfgTagLabel->palette().color(QPalette::Active, QPalette::Window));  // lost
      }

      if (s_tagForegroundColor.size()!=3) {
	/*
	s_tagForegroundColor.push_back(m_name->paletteForegroundColor());    // normal
	s_tagForegroundColor.push_back(m_cfgTagLabel->paletteForegroundColor());     // temporary
	s_tagForegroundColor.push_back(m_modCfgTagLabel->paletteForegroundColor());  // lost
	*/
	s_tagForegroundColor.push_back(m_name->palette().color(QPalette::Active, QPalette::WindowText));    // normal
	s_tagForegroundColor.push_back(m_cfgTagLabel->palette().color(QPalette::Active, QPalette::WindowText));     // temporary
	s_tagForegroundColor.push_back(m_modCfgTagLabel->palette().color(QPalette::Active, QPalette::WindowText));  // lost
      }
      m_quality->clear();
      for (unsigned int state_i=0; state_i<MetaData_t::kNQualityStates; ++state_i) {
	m_quality->addItem(qualityName( static_cast<MetaData_t::EQuality>(state_i)), static_cast<int>(state_i));
      }
      //      m_quality->setCurrentItem(MetaData_t::kNormal);
      m_updateButton->setEnabled(false);

    }

  MetaDataDetails::~MetaDataDetails() {}

  std::string MetaDataDetails::makeTimeString(time_t a_time)
  {
    std::string a_time_string = asctime( gmtime(&a_time) );
    std::string::size_type ret_pos = a_time_string.find("\r");
    std::string::size_type new_line_pos = a_time_string.find("\n");
    if (new_line_pos == std::string::npos || (new_line_pos>ret_pos && ret_pos != std::string::npos)) {
      new_line_pos =ret_pos;
    }
    if (new_line_pos != std::string::npos ) {
      a_time_string.erase(new_line_pos, a_time_string.size()-new_line_pos);
    }
    return a_time_string;
  }

  void MetaDataDetails::reset() {

    commitChanges(true);
    m_currentSerialNumber=0;
    m_currentMeasurementType=kNMeasurements;
    m_currentComment="";
    m_currentQuality=MetaData_t::kUndefined;

    m_scanNumber->setText("");
    m_startTime->setText("");
    m_duration->setText("");
    m_name->setText("");
    m_globalStatus->setText("");
    //m_globalStatus->setPaletteBackgroundColor( m_duration->paletteBackgroundColor() );
    m_globalStatus->setStyleSheet(m_duration->styleSheet());
    m_comment->setText("");
    m_comment->setEnabled(false);
    m_quality->setEnabled(false);

    m_cfgTagLabel->setText("");
    m_modCfgTagLabel->setText("");
    m_changed[0]=false;
    m_changed[1]=false;
    updateButton();
  }


  bool MetaDataDetails::setMeasurement(EMeasurementType measurement_type, SerialNumber_t serial_number)
  {
    if (m_currentSerialNumber==serial_number && measurement_type== m_currentMeasurementType) return false;

    m_scanNumber->setText(makeSerialNumberString(measurement_type, serial_number).c_str() );
    m_currentSerialNumber = serial_number;
    m_currentMeasurementType = measurement_type;

    return true;
  }

  void MetaDataDetails::setCfg(QLabel *type_label,
			       const std::string &type_name,
			       QLabel *tag_label,
			       const std::string &name,
			       const std::string &tag,
			       unsigned int revision,
			       ECfgStatus status) {

    if (type_label) {
      type_label->setText( type_name.c_str() );
    }

    if (tag_label) {
      std::string tag_string;
      if (!type_label && !type_name.empty()) {
	tag_string += type_name;
      }
      if (!name.empty()) {
	if (!tag_string.empty()) {
	  tag_string += " ";
	}
	tag_string += name;
      }

      if (!tag.empty() || revision>0) {
	if (!tag_string.empty()) {
	  tag_string += " ";
	}
	if (!type_name.empty()) {
	  tag_string += "[ ";
	}
	if (!tag.empty()) {
	  tag_string += tag;
	  tag_string += " / ";
	}
	if (revision>0) {
	  tag_string += makeTimeString(revision);
	}
	else {
	  tag_string += "0";
	}
	if (!type_name.empty()) {
	  tag_string += " ]";
	}
      }
      if (tag_string != tag_label->text().toLatin1().data()) {
	tag_label->setText(tag_string.c_str() );
      }
      /*
      if (status==kNormal) {
	if (tag_label->paletteBackgroundColor()  != m_name->paletteBackgroundColor()) {
	  tag_label->setPaletteBackgroundColor(m_name->paletteBackgroundColor());
	  tag_label->setPaletteForegroundColor(m_name->paletteForegroundColor());
	}

	tag_label->setFrameShape( QLabel::NoFrame );
      }
      else if(status <= kLost ) {
	if (tag_label->paletteBackgroundColor()  != s_tagColor[status]) {
	  tag_label->setPaletteBackgroundColor(s_tagColor[status]);
	  tag_label->setPaletteForegroundColor(s_tagForegroundColor[status]);
	}

	tag_label->setFrameShape( (status == kNormal ? QLabel::NoFrame : QLabel::Box ) );

      }
      */
      if (status==kNormal) {
	if (tag_label->palette().color(QPalette::Active, QPalette::Window)  != m_name->palette().color(QPalette::Active, QPalette::Window)) {
	  QPalette pal(tag_label->palette());
	  pal.setColor(QPalette::Active, QPalette::Window, m_name->palette().color(QPalette::Active, QPalette::Window));
	  pal.setColor(QPalette::Active, QPalette::WindowText, m_name->palette().color(QPalette::Active, QPalette::WindowText));
	  tag_label->setPalette(pal);
	}

	tag_label->setFrameShape( QLabel::NoFrame );
      }
      else if(status <= kLost ) {
	if (tag_label->palette().color(QPalette::Active, QPalette::Window)  != s_tagColor[status]) {
	  QPalette pal(tag_label->palette());
	  pal.setColor(QPalette::Active, QPalette::Window, s_tagColor[status]);
	  pal.setColor(QPalette::Active, QPalette::WindowText, s_tagForegroundColor[status]);
	  tag_label->setPalette(pal);
	}

	tag_label->setFrameShape( (status == kNormal ? QLabel::NoFrame : QLabel::Box ) );

      }
    }
  }

  void MetaDataDetails::setType(const std::string &type_name, const std::string &tag, unsigned int revision) {
    setCfg(m_name, type_name, m_scanTagLabel, "", tag, revision, kNormal);
  }

  void MetaDataDetails::setCfg1(const std::string &label, const std::string &name, const std::string &cfg_tag, unsigned int revision, ECfgStatus cfg_status) {
    setCfg(m_cfg1Label, label, m_cfgTagLabel, name, cfg_tag, revision, cfg_status);
  }

  void MetaDataDetails::setCfg2(const std::string &label, const std::string &name, const std::string &cfg_tag, unsigned int revision, ECfgStatus cfg_status) {
    setCfg(m_cfg2Label, label, m_modCfgTagLabel, name, cfg_tag, revision, cfg_status);
  }

  void MetaDataDetails::setStartTime(const ::time_t &start_time) {
    m_startTime->setText( QString::fromStdString(makeTimeString( start_time)) );
  }

  void MetaDataDetails::setDuration(unsigned int seconds ) {
    if (seconds==0 && m_duration->isVisible()) {
      m_duration->setText("");
      m_duration->hide();
    }
    else {
      std::stringstream duration_string;
      //	unsigned int duration = seconds;
      if (seconds<15*60) {
	if (seconds>=60) {
	  duration_string << seconds/60 << " min ";
	}
	if (seconds<60 || (seconds%60)!=0) {
	  duration_string << seconds%60 << " sec";
	}
      }
      else {
	if (seconds>=60*60) {
	  duration_string << seconds/(60*60) << " h ";
	}
	if (seconds<60*60 || (seconds%(60*60)) != 0) {
	  duration_string << (seconds%(60*60))/60 << " min";
	}
      }

      if (duration_string.str() != m_duration->text().toLatin1().data()) {
	m_duration->setText( duration_string.str().c_str() );
      }
      if (!m_duration->isVisible()) {
	m_duration->show();
      }
    }
  }


  void MetaDataDetails::setStatus(MetaData_t::EStatus status) {
    if (strcmp(statusName(status),m_globalStatus->text().toLatin1().data())==0) return;

    std::pair<QBrush , QPen > state_colour = statusColour(status) ;

    m_globalStatus->setText( statusName( status ) );
    m_globalStatus->setStyleSheet("background: "+state_colour.first.color().name());
    //m_globalStatus->setPaletteBackgroundColor( state_colour.first.color() );
  }

  void MetaDataDetails::setQuality(MetaData_t::EQuality quality) {
    quality =  getSanitisedQualityTag( quality );
    if (m_quality->currentIndex() != quality ) {
      m_quality->setCurrentIndex( quality );
      m_currentQuality = quality;
      m_changed[kQualityChanged]=false;
      m_quality->setEnabled(true);
      updateButton();
    }
    if (!m_quality->isEnabled() ) {
      m_quality->setEnabled(true);
    }
  }

  void MetaDataDetails::setComment( const std::string &comment) {
    if (m_currentSerialNumber!=0 && m_currentMeasurementType != kCurrent) {
      m_comment->setEnabled(true);
    }

    if (comment == m_comment->toPlainText().toLatin1().data()) return;

    m_comment->setPlainText( comment.c_str() );
    m_currentComment=comment;
    m_changed[kCommentChanged]=false;
    updateButton();
  }

  void MetaDataDetails::qualityChanged(int selected_quality) {
    if (m_currentSerialNumber>0 && m_currentMeasurementType != kCurrent && m_updateHelper.get()) {
      if (selected_quality != m_currentQuality) {
	m_changed[kQualityChanged]=true;
      }
      updateButton();
    }
    if (!m_quality->isEnabled() ) {
      m_quality->setEnabled(true);
    }
  }

  void MetaDataDetails::commentChanged() {
    if (m_currentSerialNumber>0 && m_currentMeasurementType != kCurrent && m_updateHelper.get()) {
      if (m_currentComment != m_comment->toPlainText().toLatin1().data() ) {
	m_changed[kCommentChanged]=true;
      }
      updateButton();
    }						    
  }

  
  void MetaDataDetails::updateMetaData()
  {
    commitChanges(false);
  }

  void MetaDataDetails::commitChanges(bool ask)
  {
    if (hasChanges() && m_currentMeasurementType<kNMeasurements) {

      if (ask) {
	std::stringstream message ;
	message << "The meta data of " << makeSerialNumberString(m_currentMeasurementType, m_currentSerialNumber) <<std::endl 
		<< " was changed. Shall the changes by written to the data base ?";
	bool update_db = (QMessageBox::question(NULL,"Update Meta Data",QString::fromStdString(message.str()),QMessageBox::No, QMessageBox::Yes | QMessageBox::Default) == QMessageBox::Yes);
	if (!update_db) return;
      }


      // update the calibration data meta data catalogue
      try {
	Lock lock(m_calibrationData->calibrationDataMutex());
	MetaData_t *meta_data_current=NULL;
	if (m_currentMeasurementType == kScan) {
	  meta_data_current = &(m_calibrationData->scanMetaData(m_currentSerialNumber));
	}
	else if (m_currentMeasurementType == kAnalysis) {
	  meta_data_current = &(m_calibrationData->analysisMetaData(m_currentSerialNumber));
	}
	if (meta_data_current) {
	  if (m_changed[kQualityChanged]) {
	    meta_data_current->setQuality( getSanitisedQualityTag( static_cast<MetaData_t::EQuality>(m_quality->currentIndex()) ) );
	  }
	  if (m_changed[kCommentChanged]) {
	    meta_data_current->setComment( m_comment->toPlainText().toLatin1().data() );
	  }
	}
      }
      catch(...) {
      }

      // notify GUI of further changes
      emit metaDataHasChanged(m_currentMeasurementType,m_currentSerialNumber);
      
      if (m_updateHelper.get()) {
	// trigger some additional activities.
	m_updateHelper->update(m_currentMeasurementType,
			       m_currentSerialNumber,
			       m_quality->currentIndex(),
			       m_comment->toPlainText().toLatin1().data(),
			       ((m_changed[kQualityChanged] & 1)<<0) | ((m_changed[kCommentChanged] & 1)<<1) );
      }
      m_changed[kCommentChanged]=0;
      m_changed[kQualityChanged]=0;
      updateButton();
    }
  }

  void MetaDataDetails::updateButton()
  {
    bool enabled = hasChanges() && m_updateHelper.get();
    if (enabled != m_updateButton->isEnabled()) {
      m_updateButton->setEnabled(enabled);
    }
  }

}
