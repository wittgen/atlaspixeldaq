#ifndef _PixCon_ValueList_h_
#define _PixCon_ValueList_h_

//#include <VarDefList.h>
//#include <CalibrationDataTypes.h>
#include <CalibrationData.h>

namespace PixCon {

  class ValueDescription_t
  {
  public:
    ValueDescription_t(VarDef_t var_def, EMeasurementType measurement_type, SerialNumber_t serial_number) 
      :m_varDef(var_def),
      m_type(measurement_type),
      m_serialNumber(serial_number) {  }

    explicit ValueDescription_t(VarDef_t var_def) 
      :m_varDef(var_def),
      m_type(kNMeasurements),
      m_serialNumber(0) {  }

    bool operator<(const ValueDescription_t &b) const {
      return    m_varDef.id() < b.m_varDef.id()
	    || (m_varDef.id()==b.m_varDef.id() && (     m_serialNumber < b.m_serialNumber 
				       || ( m_serialNumber == b.m_serialNumber && m_type < b.m_type ) ));
    }

    bool isFixedMeasurment() const           { return m_type < kNMeasurements; }

    EMeasurementType measurementType() const { return m_type; }
    SerialNumber_t serialNumber() const      { return m_serialNumber; }
    VarDef_t varDef() const                  { return m_varDef;}
    VarId_t id() const                       { return m_varDef.id(); }

  private:
    VarDef_t        m_varDef;
    EMeasurementType m_type;
    SerialNumber_t m_serialNumber;
  };

  class ValueListKey_t 
  {
  public:
    explicit ValueListKey_t (VarId_t var_id) :m_varId(var_id), m_type(kNMeasurements), m_serialNumber(0) {}
    ValueListKey_t (VarId_t var_id, EMeasurementType measurement_type, SerialNumber_t serial_number) 
      :m_varId(var_id),
      m_type(measurement_type),
      m_serialNumber(serial_number) {  }

    bool operator<(const ValueListKey_t &b) const {
      return    m_varId < b.m_varId 
	    || (m_varId==b.m_varId && (     m_serialNumber < b.m_serialNumber 
				       || ( m_serialNumber == b.m_serialNumber && m_type < b.m_type ) ));
    }
    
    VarId_t id() const { return m_varId;}
    EMeasurementType measurementType() const { return m_type; }
    SerialNumber_t serialNumber() const      { return m_serialNumber; }
    
  private:
    VarId_t        m_varId;
    EMeasurementType m_type;
    SerialNumber_t m_serialNumber;
  };

  class EvaluationParameter
  {
    friend class IEvaluator;
  public:
    EvaluationParameter()
      : m_connectivityName( "" ),
	m_serialNumber( 0 ),
	m_measurementType( kCurrent ),
	m_dataList( s_dummyData ),
	m_counter(0)
    {}

    EvaluationParameter(const std::string &connectivity_name,
			EMeasurementType type,
			SerialNumber_t serial_number,
			CalibrationData &calibration_data)
      : m_connectivityName( connectivity_name ),
	m_serialNumber( serial_number ),
	m_measurementType( type ),
	m_dataList( calibration_data.getConnObjectData(connectivity_name) ),
	m_counter(0)
    {
      setObject(connectivity_name, type, serial_number, calibration_data);
    }

    void setObject(const std::string &connectivity_name,
		   EMeasurementType type,
		   SerialNumber_t serial_number,
		   CalibrationData &calibration_data)
    {
      m_connectivityName = connectivity_name;
      m_serialNumber = serial_number;
      m_measurementType = type,
      m_dataList=calibration_data.getConnObjectData(connectivity_name);
      m_counter++;
    }

    const CalibrationData::ConnObjDataList &dataList() const  { return m_dataList; }
    const std::string &connectivityName() const               { return m_connectivityName; }
    SerialNumber_t serialNumber() const                       { return m_serialNumber; }
    EMeasurementType measurementType() const                  { return m_measurementType; }

    unsigned int changeCounter()                        const { return m_counter;}
  protected:
    std::string                       m_connectivityName;
    SerialNumber_t                    m_serialNumber;
    EMeasurementType                  m_measurementType;
    CalibrationData::ConnObjDataList  m_dataList;
    unsigned int                      m_counter;

    static CalibrationData::ConnObjDataList_t          s_dummyData;
  };

  template <class T>
  class IValue {
  public:
    virtual ~IValue() {}
    virtual T value() const =  0;
    virtual bool changed() const = 0;
    virtual void reset()= 0;
  };

  template <class T>
  class CalibrationDataValue  : public IValue<T>
  {
  public:
    CalibrationDataValue(EvaluationParameter &parameter, const VarDef_t &variable_def)
      : m_parameter(&parameter),
	m_variableDef(variable_def),
	m_changeCounter(m_parameter->changeCounter()-1)
    {}

    T value() const {
      return m_parameter->dataList().CalibrationData::ConnObjDataList::newestValue<T>(m_parameter->measurementType(), m_parameter->serialNumber(), m_variableDef);
    }

    bool changed() const {
      return m_parameter->changeCounter() != m_changeCounter;
    }

    void reset() {
      m_changeCounter = m_parameter->changeCounter()-1;
    }

  protected:
    EvaluationParameter *m_parameter;
    VarDef_t m_variableDef;
    unsigned int m_changeCounter;
  };

  template <class T>
  class FixedMeasurementCalibrationDataValue  : public CalibrationDataValue<T>
  {
  public:
    FixedMeasurementCalibrationDataValue(EvaluationParameter &parameter,
					 const VarDef_t &variable_def,
					 EMeasurementType measurement_type,
					 SerialNumber_t serial_number)
      : CalibrationDataValue<T>(parameter, variable_def),
	m_measurementType(measurement_type),
        m_serialNumber( serial_number)
    {  }

    T value() const {
      return this->m_parameter->dataList().CalibrationData::ConnObjDataList::template newestValue<T>(m_measurementType, m_serialNumber, this->m_variableDef);
    }

  private:
    EMeasurementType m_measurementType;
    SerialNumber_t   m_serialNumber;
  };

  template <class T> class ValueList;

  template <class T>
  class Value
  {
    friend class ValueList<T>;

  protected:
    Value(IValue<T> *value) : m_evaluator(value) {}
    Value(T value) : m_evaluator(NULL), m_value(value) {}

    ~Value() {delete m_evaluator; }


    void reset() {
      if (m_evaluator) m_evaluator->reset();
    }
  public:

    T value() const {
      if (m_evaluator && m_evaluator->changed()) {
	//	assert( m_evaluator );
	m_value = m_evaluator->value();
      }
      return m_value;
    }

  private:
    IValue<T>         *m_evaluator;
    mutable T          m_value;
  };

  template <class T>
  class ValueList
  {
  public:

    ~ValueList()
    {
      for (typename std::map< ValueListKey_t, Value<T> * >::iterator value_iter = m_valueList.begin();
	   value_iter != m_valueList.end();
	   value_iter++) {
	delete value_iter->second ;
      }
      for (typename std::vector< Value<T> * >::iterator value_iter = m_constValueList.begin();
	   value_iter != m_constValueList.end();
	   value_iter++) {
	delete *value_iter ;
      }
    }

    Value<T> *addValue(const VarDef_t &def, EvaluationParameter &parameter) { 
      ValueListKey_t a_key(def.id());
      typename std::map<ValueListKey_t, Value<T> * >::iterator iter = m_valueList.find(a_key);
      if (iter != m_valueList.end()) return iter->second;
      std::pair< typename std::map<ValueListKey_t, Value<T> * >::iterator, bool>
	ret = m_valueList.insert( std::make_pair( a_key, new Value<T>(new CalibrationDataValue<T>(parameter,def)) ));
      if (!ret.second) {
	throw std::runtime_error("Failed to add value evaluator.");
      }
      return ret.first->second;
    }

    Value<T> *addValue(const VarDef_t &def,
		       EvaluationParameter &parameter, 
		       EMeasurementType measurement_type,
		       SerialNumber_t serial_number) { 
      ValueListKey_t a_key(def.id(),measurement_type, serial_number);
      typename std::map<ValueListKey_t, Value<T> * >::iterator iter = m_valueList.find(a_key);
      if (iter != m_valueList.end()) return iter->second;

      std::pair< typename std::map<ValueListKey_t, Value<T> * >::iterator, bool>  
	ret = m_valueList.insert( std::make_pair( a_key, new Value<T>(new FixedMeasurementCalibrationDataValue<T>(parameter,
													   def,
													   measurement_type,
													   serial_number)) ));

      if (!ret.second) {
	throw std::runtime_error("Failed to add value evaluator.");
      }
      return ret.first->second;
    }

    Value<T> *addValue(T value) {
      m_constValueList.push_back(new Value<T>(value) );
      return m_constValueList.back();
    }

    void reset()  {
      for (typename std::map<ValueListKey_t, Value<T> * >::iterator value_iter = m_valueList.begin();
	   value_iter != m_valueList.end();
	   value_iter++) {
	value_iter->second->reset();
      }
    }

    typename std::map<ValueListKey_t, Value<T>  *>::const_iterator begin() const { return  m_valueList.begin(); }
    typename std::map<ValueListKey_t, Value<T>  *>::const_iterator end()   const { return  m_valueList.end();   }
    typename std::map<ValueListKey_t, Value<T>  *>::const_iterator find(const ValueListKey_t &key)
      const { return  m_valueList.find(key); }

    void getVarDefs(std::vector<ValueDescription_t> &var_list) const {
      VarDefList *var_def_list = VarDefList::instance();
      for (typename std::map<ValueListKey_t, Value<T>  *>::const_iterator iter = begin();
	   iter != end();
	   iter++) {
	if (var_def_list->isValid(iter->first.id())) {
	  try {
	    var_list.push_back( ValueDescription_t( var_def_list->getVarDef( var_def_list->varName(iter->first.id()) ),
						    iter->first.measurementType(),
						    iter->first.serialNumber()) );
	  }
	  catch (UndefinedCalibrationVariable &err) {
	  }
	}
      }
    }

    void clear() {
      m_valueList.clear();
      m_constValueList.clear();
    }

  private:

    std::map<ValueListKey_t, Value<T>  *> m_valueList;
    std::vector< Value<T> *> m_constValueList;
  };

}

#endif
