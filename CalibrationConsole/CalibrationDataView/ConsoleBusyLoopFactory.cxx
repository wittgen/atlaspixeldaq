#include "ConsoleBusyLoopFactory.h"
#include "BusyLoop.h"
#include <iostream>

namespace PixCon {
  
  class FactoryCreatedBusyLoop : public PixA::IBusyLoop
  {
  public:
    FactoryCreatedBusyLoop(QApplication *app,
			   QDialog *dialog,
			   unsigned int dialog_after_time,
			   unsigned int update_time,
			   ConsoleBusyLoopFactory &factory) 
      : m_busyLoop(app, dialog, dialog_after_time, update_time),
	m_factory(&factory)
    {}

    ~FactoryCreatedBusyLoop() {
      m_factory->resetMessage();
    }

    void changeMessage(const std::string &message) {
      m_busyLoop.changeMessage(message);
    }

  private:
    BusyLoop m_busyLoop;
    ConsoleBusyLoopFactory *m_factory;
  };
  

  ConsoleBusyLoopFactory::ConsoleBusyLoopFactory(QApplication *app) : m_app(app) { 
    PixA::BusyLoopFactory::setInstance( this );
  }

  void ConsoleBusyLoopFactory::destroy() {
    PixA::BusyLoopFactory::setInstance( NULL );
  }

  PixA::IBusyLoop *ConsoleBusyLoopFactory::busyLoop(const std::string &message,
						    QDialog *dialog,
						    unsigned int dialog_after_time,
						    unsigned int update_time) {
    emit statusMessage(message.c_str());
    return new FactoryCreatedBusyLoop(m_app, dialog, dialog_after_time, update_time, *this);
  }

  ConsoleBusyLoopFactory::~ConsoleBusyLoopFactory() {
    std::cout << "INFO [ConsoleBusyLoopFactory::dtor] " << std::endl;
  }

}
