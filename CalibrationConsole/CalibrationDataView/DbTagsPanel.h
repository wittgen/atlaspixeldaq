// Dear emacs, this is -*-c++-*-
#ifndef DBTAGSPANEL
#define DBTAGSPANEL

#include "ui_DbTagsPanelBase.h"
#include "PixDbServer/PixDbServerInterface.h"

#include <qcombobox.h>


class DbTagsPanel : public QDialog, public Ui_DbTagsPanelBase {

  Q_OBJECT

  public:
  DbTagsPanel(const std::string &current_id_tag="",
	      const std::string &current_conn_tag="",
	      const std::string &current_cfg_tag="",
	      const std::string &current_mod_tag="",
	      QWidget* parent = 0,
	      std::string partition_name = "",
	      std::string dbServer_name = "");
  ~DbTagsPanel();

  std::string getConnectivityTag() const {
    return comboBoxTagConn->currentText().toLatin1().data();
  }

  std::string getConfigurationTag() const {
    return comboBoxTagCfg->currentText().toLatin1().data();
  }

  std::string getModuleConfigurationTag() const {
    return comboBoxTagModCfg->currentText().toLatin1().data();
  }

  std::string getObjectTag() const {
    return comboBoxTagOI->currentText().toLatin1().data();
  }

 // void changeObjectTag(const QString &object_tag);
 // void changeCfgObjectTag(const QString &object_tag);

  public slots:
  void synchronise(const QString &tag);
 void changeObjectTag(const QString &object_tag);
  void changeCfgObjectTag(const QString &object_tag);
  static unsigned int find(const QComboBox &combobox, const QString &text);

  private:
  PixLib::PixDbServerInterface *m_dbs;
  
};

#endif // DBTAGSPANEL
