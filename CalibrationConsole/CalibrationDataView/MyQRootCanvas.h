#ifndef _MyQRootCanvas_h_
#define _MyQRootCanvas_h_

#if  defined(HAVE_QTGSI)
  #include "TQRootCanvas.h"
#endif
#include <QWidget>

class QPaintEvent;
class QResizeEvent;
class QMouseEvent;
class QPushButton;
class QTimer;
class TCanvas;

class MyQRootCanvas 
#if  defined(HAVE_QTGSI)
: public TQRootCanvas
#else 
: public QWidget
#endif
{
  Q_OBJECT

 public:
  MyQRootCanvas(QWidget *parent, const char *name);
  ~MyQRootCanvas();


  TCanvas *getCanvas();

 public slots:
   void updateCanvas();

 signals:
   void statusMessage(const QString &message);

 protected:
   virtual void mouseMoveEvent( QMouseEvent *e ) override;
#if  !defined(HAVE_QTGSI)
   virtual void changeEvent(QEvent * e) override;
   virtual void mousePressEvent( QMouseEvent *e ) override;
   virtual void mouseReleaseEvent( QMouseEvent *e ) override;
   virtual void paintEvent( QPaintEvent *e ) override;
   virtual void resizeEvent( QResizeEvent *e ) override;
   virtual QPaintEngine* paintEngine() const Q_DECL_OVERRIDE;
#endif

 private:
   QTimer* m_timer;
   TCanvas* m_canvas;

};

#endif
