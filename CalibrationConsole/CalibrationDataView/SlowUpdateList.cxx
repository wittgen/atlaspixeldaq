#include "SlowUpdateList.h"

namespace PixCon {

  SlowUpdateList::SlowUpdateList() {
    m_listener.resize(kNMeasurements);
  }

  SlowUpdateList::~SlowUpdateList() {}

  void SlowUpdateList::registerListener(EMeasurementType measurement, SerialNumber_t serial_number,
					const std::shared_ptr<ICalibrationDataListener> &listener)
  {
    if (static_cast<unsigned int>(measurement)>=m_listener.size()) throw std::logic_error("ERROR [SlowUpdateList::registerListener] Unconsidered measurement type (BUG).");

    ListenerList_t &listener_list = m_listener[measurement].element()[serial_number].element();
    if ( findListener( listener_list, listener) == listener_list.end()) {
      listener_list.push_back(listener);
    }
  }

  void SlowUpdateList::deregisterListener(const std::shared_ptr<ICalibrationDataListener> &listener)
  {
    for ( ListenerRegister_t::iterator
	    measurement_iter = m_listener.begin();
	  measurement_iter != m_listener.end();
	  ++measurement_iter) {

      for ( ListenerUpdateMap_t::iterator
	      number_iter = measurement_iter->element().begin();
	    number_iter != measurement_iter->element().end();
	    ++number_iter) {

	ListenerList_t::iterator listener_iter = findListener(number_iter->second.element(), listener);
	if (listener_iter != number_iter->second.element().end()) {
	  number_iter->second.element().erase(listener_iter);
	}

      }
    }
  }

  bool SlowUpdateList::checkUpdate(EMeasurementType measurement, SerialNumber_t serial_number, VarId_t var_id, const std::string &conn_name) {
    if (static_cast<unsigned int>(measurement)>=m_listener.size()) return false;

    ListenerUpdateMap_t::iterator 
      listener_list = m_listener[measurement].element().find(serial_number) ;
    bool ret=false;
    if (listener_list != m_listener[measurement].element().end()) {
      for(ListenerList_t::iterator listener_iter = listener_list->second.element().begin();
	  listener_iter != listener_list->second.element().end();
	  ++listener_iter ) {
	ret |= (*listener_iter)->needUpdate(measurement, serial_number, var_id, conn_name);
      }
      if (ret) {
	m_listener[measurement].setNeedUpdate(true);
	listener_list->second.setNeedUpdate(true);
      }
    }
    return ret;
  }

  void SlowUpdateList::update() {
    for ( ListenerRegister_t::iterator
	    measurement_iter = m_listener.begin();
	  measurement_iter != m_listener.end();
	  ++measurement_iter) {
      if (measurement_iter->needUpdate()) {

	for ( ListenerUpdateMap_t::iterator
		number_iter = measurement_iter->element().begin();
	      number_iter != measurement_iter->element().end();
	      ++number_iter) {

	  if (number_iter->second.needUpdate()) {
	    for(ListenerList_t::iterator listener_iter = number_iter->second.element().begin();
		listener_iter != number_iter->second.element().end();
		++listener_iter ) {
	      (*listener_iter)->update();
	    }
	  }

	}
      }
    }
  }
}

