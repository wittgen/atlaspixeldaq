#include "MetaDataList.h"
//#include <qpainter.h>
//#include <qpen.h>
#include <QFileDialog>
#include <QShortcut>
#include <QList>
#include <QMenu>

#include <CategoryList.h>

#include "TemporaryCursorChange.h"
#include <QTVisualiser/BusyLoopFactory.h>

// for the analysis combiner
#include <IConnItemSelection.h>
#include <ISelectionFactory.h>
#include <CalibrationDataFunctorBase.h>

// definition of ObjectConfigDbInstance::EClass :
// #include <ObjectConfigDbInstance.hh>

//#include <CoralDbClient.hh>

#include "Highlighter.h"
#include <ConfigWrapper/Regex_t.h>
#include "Tokeniser.h"

namespace PixCon {

  std::string extractFirstSentence(const std::string &multi_line) {
    std::string::size_type pos_sentence_start=0;
    for(;pos_sentence_start<multi_line.size();) {
      while (pos_sentence_start<multi_line.size() && !isalnum(multi_line[pos_sentence_start]) && !ispunct(multi_line[pos_sentence_start])) pos_sentence_start++;
    std::string::size_type pos_sentence_end = multi_line.find(".",pos_sentence_start);
    std::string::size_type pos_line_end = multi_line.find("\n", pos_sentence_start);
    if (pos_sentence_end ==std::string::npos || pos_line_end < pos_sentence_end) {
      pos_sentence_end = pos_line_end;
    }
    if (pos_sentence_end-pos_sentence_start>1) {
      if (pos_sentence_end != std::string::npos) {
	return multi_line.substr(pos_sentence_start,pos_sentence_end-pos_sentence_start);
      }
      else {
	return multi_line;
      }
    }
    pos_sentence_start=pos_sentence_end+1;
    }
    return multi_line;
  }


  class MetaDataItemBase : public QTreeWidgetItem
  {
  public:
    enum EItemType {kUnknown, kMetaData, kCategory, kVariable, kNBaseItemTypes,kMetaDataScan, kMetaDataAnalysis};
    MetaDataItemBase(EItemType type,
		     QTreeWidget *parent,
		     const std::string &text_0,
		     const char* text_1,
		     const char* text_2,
		     const char* text_3,
		     const char* text_4,
		     const char* text_5)
      : QTreeWidgetItem(parent,{QString::fromStdString(text_0),QString::fromStdString(text_1),QString::fromStdString(text_2),QString::fromStdString(text_3),QString::fromStdString(text_4),QString::fromStdString(text_5)}),
	m_type(type)
    {}


    MetaDataItemBase(EItemType type,
		     QTreeWidgetItem *parent,
		     const std::string &text_0,
		     const char* text_1,
		     const char* text_2,
		     const char* text_3,
		     const char* text_4,
		     const char* text_5)
      : QTreeWidgetItem(parent,{QString::fromStdString(text_0),QString::fromStdString(text_1),QString::fromStdString(text_2),QString::fromStdString(text_3),QString::fromStdString(text_4),QString::fromStdString(text_5)}),
	m_type(type)
    {}


    MetaDataItemBase(EItemType type,
		     QTreeWidget *parent,
		     const std::basic_string<char, std::char_traits<char>, std::allocator<char> > &text_0
		   )
      : QTreeWidgetItem(parent,QStringList(QString::fromStdString(text_0))),
	m_type(type)
    {}

    MetaDataItemBase(EItemType type,
		     QTreeWidgetItem *parent,
		     const std::basic_string<char, std::char_traits<char>, std::allocator<char> > &text_0
		   )
      : QTreeWidgetItem(parent,QStringList(QString::fromStdString(text_0))),
	m_type(type)
    {}


    MetaDataItemBase(EItemType type,
		     QTreeWidgetItem *parent,
		     const char* text_0,
		     const char* text_1,
		     const std::basic_string<char, std::char_traits<char>, std::allocator<char> > &text_2
		   )
      : QTreeWidgetItem(parent,{QString::fromStdString(text_0),QString::fromStdString(text_1),QString::fromStdString(text_2)}),
	m_type(type)
    {}

//////////////////////////////////////////////////////////////////////////////////////////////

    MetaDataItemBase(EItemType type,
		     QTreeWidget *parent,
		     const QString &text_0,
		     const QString &text_1,
		     const QString &text_2,
		     const QString &text_3,
		     const QString &text_4,
		     const QString &text_5)
      : QTreeWidgetItem(parent,{text_0,text_1,text_2,text_3,text_4,text_5}),
	m_type(type)
    {}

    MetaDataItemBase(EItemType type,
		     QTreeWidgetItem *parent,
		     const QString &text_0,
		     const QString &text_1,
		     const QString &text_2,
		     const QString &text_3,
		     const QString &text_4,
		     const QString &text_5)
      : QTreeWidgetItem(parent,{text_0,text_1,text_2,text_3,text_4,text_5}),
	m_type(type)
    {}

    MetaDataItemBase(EItemType type,
		     QTreeWidget *parent,
		     const QString &text_0,
		     const QString &text_1,
		     const QString &text_2)
      : QTreeWidgetItem(parent,{text_0,text_1,text_2}),
	m_type(type)
     {}

    MetaDataItemBase(EItemType type,
		     QTreeWidgetItem *parent,
		     const QString &text_0,
		     const QString &text_1,
		     const QString &text_2)
      : QTreeWidgetItem(parent,{text_0,text_1,text_2}),
	m_type(type)
     {}

    MetaDataItemBase(EItemType type,
		     QTreeWidget *parent,
		     const QString &text_0)
      : QTreeWidgetItem(parent,QStringList(text_0)),
	m_type(type)
     {}

    MetaDataItemBase(EItemType type,
		     QTreeWidgetItem *parent,
		     const QString &text_0)
      : QTreeWidgetItem(parent,QStringList(text_0)),
	m_type(type)
     {}

    EItemType getType() const {return m_type;}

    bool typeMatches(EItemType type) const {
      if (type == m_type) return true;
      if (type==kMetaData) return m_type == kMetaDataAnalysis || m_type == kMetaDataScan;
      return false;
    }

    static bool typesMatch(EItemType a_type, EItemType b_type) {
      if (a_type == b_type) return true;

      if (a_type==kMetaDataScan || a_type==kMetaDataAnalysis) a_type = kMetaData;
      if (b_type==kMetaDataScan || b_type==kMetaDataAnalysis) b_type = kMetaData;

      return (a_type == b_type);
    }


    bool isHigherLevelType(EItemType type) const {
      EItemType a_type=m_type;
      if (a_type == kMetaDataAnalysis || a_type == kMetaDataScan) {
	a_type = kMetaData;
      }
      return a_type > type;
    }

    static EItemType getType(const /*Q3ListViewItem*/QTreeWidgetItem *item);
  private:
    EItemType m_type;
  };

  template <class T_MetaDataItem>
  class ItemAction_ {
  public:
    virtual ~ItemAction_() {};
    virtual void operator()(T_MetaDataItem &item) = 0;
  };

  typedef class ItemAction_<const MetaDataItemBase> ItemAction;
   typedef class ItemAction_<MetaDataItemBase> NonConstItemAction;

   template <class QTreeWidgetItem, class T_MetaDataItem>
   void forEach_( QTreeWidgetItem *first_child, MetaDataItemBase::EItemType type, ItemAction_<T_MetaDataItem> &action, bool selected_only) {
     std::vector<QTreeWidgetItem *> stack;
     if (!first_child) return;

     stack.push_back( first_child);
     while (!stack.empty()) {
       QTreeWidgetItem *item = stack.back();
       stack.pop_back();
       
       // get parent
       QTreeWidgetItem *temp_parent = item->parent();
       T_MetaDataItem *parent_item=NULL;
       while (temp_parent ){
	 T_MetaDataItem *parent_base_item = dynamic_cast<T_MetaDataItem *>(temp_parent);
	 if (parent_base_item) {
	   parent_item = parent_base_item;
	   break;
	 }
	 temp_parent = temp_parent->parent();
       }
       while (item) {
	 T_MetaDataItem *base_item = dynamic_cast<T_MetaDataItem *>(item);
	 //std::cout << "INFO [forEach] item = " << item->text(0).toLatin1().data() <<  " type = " << base_item->type() << std::endl;
	 QTreeWidgetItem *next_item = 0;
	 if(item->parent())
	   next_item = item->parent()->child(item->parent()->indexOfChild(item)+1);
	 else // no parent => top-level item, so let's use list to get next item
	   next_item = item->treeWidget()->topLevelItem(item->treeWidget()->indexOfTopLevelItem(item)+1);
	 if (item->child(0)) {
	   if (next_item) {
	     stack.push_back(next_item);
	   }
	   
	   if (base_item) {
	     parent_item = base_item;
	   }
	   item = item->child(0);
	 }
	 else {
	   item = next_item;
	 }
	 
	 if (base_item) {
	   if (parent_item && parent_item->isHigherLevelType(type) ) {
	     item = next_item;
	     if (!stack.empty() && stack.back() == next_item) {
	       stack.pop_back();
	     }
	   }
	   if ((!selected_only || base_item->isSelected()) && base_item->typeMatches(type)) {
	     // call action on selected items
	     action(*base_item);
	   }
	 }
       }//end of while(item)
     }
   }


  void forEach(QTreeWidgetItem *item, MetaDataItemBase::EItemType type, ItemAction &action, bool selected_only=true) {
    forEach_< QTreeWidgetItem, const MetaDataItemBase>(item, type,action, selected_only);
  }

  void forEach(QTreeWidgetItem *item, MetaDataItemBase::EItemType type, NonConstItemAction &action, bool selected_only=true) {
    forEach_<QTreeWidgetItem, MetaDataItemBase>(item, type,action, selected_only);
  }

  class MetaDataItem : public MetaDataItemBase
  {
  public:

    MetaDataItem(MetaDataList &meta_data_list,
		 QTreeWidget *parent,
		 EMeasurementType measurement_type,
		 SerialNumber_t serial_number,
		 bool loaded,
		 const std::string &date,
		 const std::string &name,
		 const std::string &quality,
		 const std::string &comment,
		 MetaData_t::EStatus status)
      : MetaDataItemBase(( measurement_type == kScan ? MetaDataItemBase::kMetaDataScan
			   : ( measurement_type == kAnalysis ? MetaDataItemBase::kMetaDataAnalysis
			       : MetaDataItemBase::kMetaData)),
			 parent,
			 makeSerialNumberString(measurement_type, serial_number),
			 date.c_str(),
			 name.c_str(),
			 quality.c_str(),
			 statusName(status),
			 extractFirstSentence(comment).c_str()),
	m_metaDataList(&meta_data_list),
	m_measurementType(measurement_type),
	m_serialNumber(serial_number),
	m_status(status),
	m_loaded(loaded)
    { init(); }

    MetaDataItem(MetaDataList &meta_data_list,
		 QTreeWidgetItem *parent,
		 EMeasurementType measurement_type,
		 SerialNumber_t serial_number,
		 bool loaded,
		 const std::string &date,
		 const std::string &name,
		 const std::string &quality,
		 const std::string &comment,
		 MetaData_t::EStatus status)
      : MetaDataItemBase(( measurement_type == kScan ? MetaDataItemBase::kMetaDataScan
			   : ( measurement_type == kAnalysis ? MetaDataItemBase::kMetaDataAnalysis
			       : MetaDataItemBase::kMetaData)),
			 parent,
			 makeSerialNumberString(measurement_type, serial_number),
			 date.c_str(),
			 name.c_str(),
			 quality.c_str(),
			 statusName(status),
			 extractFirstSentence(comment).c_str()),
	m_metaDataList(&meta_data_list),
	m_measurementType(measurement_type),
	m_serialNumber(serial_number),
	m_status(status),
	m_loaded(loaded)
    { init(); }

    ~MetaDataItem() {
      //std::cout << "INFO [MetaDataItem::dtor] deleted type = " << m_measurementType << " number = " << m_serialNumber << std::endl;
      emit m_metaDataList->removeMeasurement(measurementType(), serialNumber() );
    }

    SerialNumber_t serialNumber() const       { return m_serialNumber; }
    EMeasurementType measurementType() const  { return m_measurementType; }

    bool isLoaded() const                     { return m_loaded; }

    void setStatus( MetaData_t::EStatus status) { 
      if (status != m_status) { 
	m_status = status; 
	setText(4,statusName(m_status)); 
	setBackground(0, m_metaDataList->statusColour(m_status).first);
	setBackground(4, m_metaDataList->statusColour(m_status).first);
      } 
    }

    static State_t mapStatus(MetaData_t::EStatus status) {
      return MetaDataDetails::mapStatus(status);
    }

    static const char *statusName(MetaData_t::EStatus status) {
      return MetaDataDetails::statusName(status);
    }

    void setQuality(const std::string &quality_name) {
      setText(3,QString::fromStdString(quality_name));
    }

    void setComment(const std::string &comment) {
      setText(5,extractFirstSentence(comment).c_str());
    }

    static bool initialise();

  protected:

  private:
    void init() {
      if (m_status>=MetaData_t::kNStates) {
	m_status = MetaData_t::kUnknown;
      }
      // ensure that enough colors are defined.
      //      assert(s_color.size() == MetaData_t::kNStates );
      assert(s_whiteColor.size() == 1 );
      setBackground(0, m_metaDataList->statusColour(m_status).first); 
      setBackground(4, m_metaDataList->statusColour(m_status).first);
    }

    MetaDataList    *m_metaDataList;
    EMeasurementType m_measurementType;
    SerialNumber_t   m_serialNumber;
    MetaData_t::EStatus          m_status;
    bool             m_loaded;
    static const char *s_statusName[MetaData_t::kNStates];
    //    static std::vector<QColor> s_color;
    static std::vector<QColor> s_whiteColor;
    static bool s_initialise;
  };//end MetaDataItem



  //  std::vector<QColor> MetaDataItem::s_color;
  std::vector<QColor>                                MetaDataItem::s_whiteColor;

  bool MetaDataItem::initialise() {
    if (s_whiteColor.empty()) {
      // to be used for the text in case of failed scans
      s_whiteColor.reserve(1);
      s_whiteColor.push_back(QColor("white"));
    }
    return true;
  }

  bool MetaDataItem::s_initialise = MetaDataItem::initialise();

  class CategoryItem : public MetaDataItemBase
  {
  public:
    CategoryItem(QTreeWidgetItem *parent, const std::string &category_name)
      : MetaDataItemBase(MetaDataItemBase::kCategory,
			 parent,
			 category_name)
    {}

    QString categoryName() const { return text(0);}
  };

  class VariableItem : public MetaDataItemBase
  {
  public:
    VariableItem(QTreeWidgetItem *parent, const std::string &variable_name, unsigned int child_counter)
      : MetaDataItemBase(MetaDataItemBase::kVariable,
			 parent,
			 "",
			 "-",
			 variable_name),
	m_even( (child_counter & 1)==0)
    {}

    void setChildIndex(unsigned int child_counter) {
      m_even=((child_counter & 1)==0);
    }

    QString variableName() const { return text(2);}

  protected:
/*    void paintCell ( QPainter * painter, const QColorGroup & colour_group, int column, int width, int align )
    {
      painter->save();
      if ( isSelected() ) {
        QColorGroup new_colour_group(colour_group);
	new_colour_group.setColor(QColorGroup::Base, new_colour_group.highlight() );
	new_colour_group.setColor(QColorGroup::Text, new_colour_group.highlightedText() );
        Q3ListViewItem::paintCell(painter, new_colour_group, column, width, align);
      }
      else if (m_even) {
        QColorGroup new_colour_group(colour_group);
	new_colour_group.setColor(QColorGroup::Base, s_background );
        Q3ListViewItem::paintCell(painter, new_colour_group, column, width, align);
      }
      else {
        Q3ListViewItem::paintCell(painter, colour_group, column, width, align);
      }
      painter->restore();
    }*/
  private:
    bool  m_even;
    static QColor s_background;
    //    static QColor s_selected;
    //    static QColor s_selectedForeground;
  };

  QColor VariableItem::s_background=QColor(static_cast<unsigned int>(255*.93),
					   static_cast<unsigned int>(255*.93),
					   static_cast<unsigned int>(255*.93));


  inline MetaDataItemBase::EItemType MetaDataItemBase::getType(const QTreeWidgetItem *item) {
    if (item) {
      if (dynamic_cast<const MetaDataItem *>(item)) {
	return static_cast<const MetaDataItem *>(item)->getType();
      }
      else if (dynamic_cast<const VariableItem *>(item)) {
	return kVariable;
      }
      else if (dynamic_cast<const CategoryItem *>(item)) {
	return kCategory;
      }
    }
    return kUnknown;
  }


  class CountItems : public ItemAction
  {
  public:
    CountItems(MetaDataItemBase::EItemType type) : m_type(type), m_generalType(type), m_count(0), m_countGeneral(0) { 
      if (type==MetaDataItemBase::kMetaDataAnalysis || type==MetaDataItemBase::kMetaDataScan) {
	m_generalType=MetaDataItemBase::kMetaData;
      }
    }

    void operator()(const MetaDataItemBase &item) {
      if (item.typeMatches(m_generalType)) {
	if (m_generalType != m_type && item.typeMatches(m_type)) {
	  m_count++;
	}
	else {
	  m_countGeneral++;
	}
      }
    }

    unsigned int count() const                       { return m_count + m_countGeneral; }
    unsigned int specificCount() const               { return m_count; }
    MetaDataItemBase::EItemType specificType() const { return m_type; }
    void dump() {
      std::cout << "INFO [CountItems::dump] total = " << m_count + m_countGeneral << " specific = " << m_count << " (type = "<< m_type << ")." << std::endl;
    }

  private:
    MetaDataItemBase::EItemType m_type;
    MetaDataItemBase::EItemType m_generalType;
    unsigned int m_count;
    unsigned int m_countGeneral;
  };


  class RemoveMetaDataAction : public ItemAction
  {
  public:
    RemoveMetaDataAction(MetaDataList &list) : m_metaDataList(&list){}
    ~RemoveMetaDataAction() { remove(); }

    void operator()(const MetaDataItemBase &item) {
      assert ( dynamic_cast<const MetaDataItem *>(&item) );
      const MetaDataItem *meta_data_item = static_cast<const MetaDataItem *>(&item);
      m_removeList.push_back( std::make_pair(meta_data_item->measurementType(), meta_data_item->serialNumber()));
    }

    void remove() {
      for(std::vector< std::pair<EMeasurementType, SerialNumber_t> >::const_iterator remove_iter= m_removeList.begin();
	  remove_iter != m_removeList.end();
	  remove_iter++) {
	m_metaDataList->removeMetaData( remove_iter->first, remove_iter->second);
      }
    }

  private:
    MetaDataList *m_metaDataList;
    std::vector< std::pair<EMeasurementType, SerialNumber_t> > m_removeList;
  };

  class VariableContextMenuAction : public ItemAction
  {
  public:
    VariableContextMenuAction(MetaDataList &list) : m_metaDataList(&list){}

    void operator()(const MetaDataItemBase &item) {
      assert ( dynamic_cast<const VariableItem *>(&item) );
      //      VariableItem *variable_item = static_cast<Variable *>(item);
      emit m_metaDataList->showVariableContextMenu();
    }

  private:
    MetaDataList *m_metaDataList;
  };

  class ShowScanConfigAction : public ItemAction
  {
  public:
    ShowScanConfigAction(MetaDataList &list) : m_metaDataList(&list){}

    void operator()(const MetaDataItemBase &item) {
      assert ( dynamic_cast<const MetaDataItem *>(&item) );
      const MetaDataItem *meta_data_item = static_cast<const MetaDataItem *>(&item);
      emit m_metaDataList->showScanConfiguration( meta_data_item->measurementType(), meta_data_item->serialNumber()  );
    }

  private:
    MetaDataList *m_metaDataList;
  };

  class ShowAnalysisConfigAction : public ItemAction
  {
  public:
    ShowAnalysisConfigAction(MetaDataList &list, unsigned short class_i) : m_metaDataList(&list), m_class(class_i) {}

    void operator()(const MetaDataItemBase &item) {
      assert ( dynamic_cast<const MetaDataItem *>(&item) );
      const MetaDataItem *meta_data_item = static_cast<const MetaDataItem *>(&item);
      //      std::cout << "INFO [ShowAnalysisConfigAction::operator()] " << makeSerialNumberString(meta_data_item->measurementType(), meta_data_item->serialNumber()) << " " << m_class <<std::endl;
      emit m_metaDataList->showAnalysisConfiguration( meta_data_item->measurementType(), meta_data_item->serialNumber(), m_class);
    }

  private:
    MetaDataList *m_metaDataList;
    unsigned short m_class;
  };


  class VariableNameCollector : public ItemAction
  {
  public:
    VariableNameCollector() : m_lastParent(NULL) { m_varList.resize(kNMeasurements);}

    void operator()(const MetaDataItemBase &item) {
      if ((!item.parent() || item.parent() != m_lastParent) && (&item !=m_lastParent)) {
    const /*Q3ListViewItem*/QTreeWidgetItem *parent = &item;
	m_currentVarList = NULL;
	while ( parent ) {
	  const MetaDataItem *meta_data_item = dynamic_cast<const MetaDataItem *>(parent);
	  if (meta_data_item) {
	    //	    m_currentMeasurement = meta_data_item;
	    assert(meta_data_item->measurementType()<kNMeasurements);
	    m_currentVarList = &(m_varList[meta_data_item->measurementType()][meta_data_item->serialNumber()]);
	    break;
	  }
	  parent = parent->parent();
	}
	if (item.parent()) {
	  m_lastParent = item.parent();
	}
	else {
	  m_lastParent = &item;
	}
      }

      if (item.typeMatches(MetaDataItemBase::kVariable)) {
	const VariableItem *variable_item = static_cast<const VariableItem *>(&item );
	m_currentVarList->push_back((variable_item->variableName()).toUtf8().constData());
      }
      else if (item.typeMatches(MetaDataItemBase::kCategory) || item.typeMatches(MetaDataItemBase::kMetaData)) {
	std::vector< std::string > selected_variables;
	std::vector< std::string > not_selected_variables;

    std::vector< /*Q3ListViewItem*/QTreeWidgetItem *> stack;
    stack.push_back(item.child(0));
	while ( !stack.empty()) {
      /*Q3ListViewItem*/QTreeWidgetItem *an_item = stack.back();
	  stack.pop_back();
	  while (an_item) {
	    VariableItem *variable_item = dynamic_cast<VariableItem *>(an_item);
	    if (variable_item) {
	      if (variable_item->isSelected()) {
		selected_variables.push_back((variable_item->variableName()).toUtf8().constData());
	      }
	      else {
		not_selected_variables.push_back((variable_item->variableName()).toUtf8().constData());
	      }
	    }
        int chld_count = an_item->parent()->childCount();
        int indx = an_item->parent()->indexOfChild(an_item);
        if (an_item->child(0)) {
          //if (an_item->nextSibling()) {
              if (indx <chld_count-1) {
          //stack.push_back(an_item->nextSibling());
          stack.push_back(an_item->parent()->child(indx+1));
	      }
          an_item = an_item->child(0);
	    }
	    else {
            if (indx <chld_count-1) {
          an_item = an_item->parent()->child(indx+1);
	    }
        else an_item = 0;
	  }
      }
	}
	if (m_currentVarList) {
	  if (!selected_variables.empty()) {
	    m_currentVarList->insert(m_currentVarList->end(), selected_variables.begin(), selected_variables.end());
	  }
	  else {
	    m_currentVarList->insert(m_currentVarList->end(), not_selected_variables.begin(), not_selected_variables.end());
	  }
	}
      }
    }


    void dump(std::ostream &out_stream, CalibrationDataManager &calibration_data, bool comma_separated) {

      Lock lock(calibration_data.calibrationDataMutex());

      const char *seperator=(comma_separated ? ", " : " " );
      // serial number headers
      out_stream << seperator;
      for (std::vector< std::map< SerialNumber_t, std::vector<std::string> > >::const_iterator  measurement_type_iter = m_varList.begin();
	   measurement_type_iter != m_varList.end();
	   measurement_type_iter++) {
	for (std::map< SerialNumber_t, std::vector<std::string> >::const_iterator  serial_number_iter = measurement_type_iter->begin();
	     serial_number_iter != measurement_type_iter->end();
	     serial_number_iter++) {

	  for (std::vector<std::string>::const_iterator  variable_iter = serial_number_iter->second.begin();
	       variable_iter != serial_number_iter->second.end();
	       variable_iter++) {
	    if (variable_iter-serial_number_iter->second.begin()==0) {
	      out_stream << makeSerialNumberString( static_cast<EMeasurementType>(measurement_type_iter-m_varList.begin()), serial_number_iter->first);
	    }
	    if (serial_number_iter->second.end()-variable_iter>1) {
	      out_stream << seperator;
	    }
	  }
	}
      }
      out_stream << std::endl;

      // variable names
      out_stream << seperator;
      for (std::vector< std::map< SerialNumber_t, std::vector<std::string> > >::const_iterator  measurement_type_iter = m_varList.begin();
	   measurement_type_iter != m_varList.end();
	   measurement_type_iter++) {

	for (std::map< SerialNumber_t, std::vector<std::string> >::const_iterator  serial_number_iter = measurement_type_iter->begin();
	     serial_number_iter != measurement_type_iter->end();
	     serial_number_iter++) {

	  for (std::vector<std::string>::const_iterator  variable_iter = serial_number_iter->second.begin();
	       variable_iter != serial_number_iter->second.end();
	       variable_iter++) {
	    out_stream << *variable_iter;
	    if (serial_number_iter->second.end()-variable_iter>1) {
	       out_stream << seperator;
	    }
	  }
	}
      }
      out_stream << std::endl;

      // units
      out_stream << seperator;
      for (std::vector< std::map< SerialNumber_t, std::vector<std::string> > >::const_iterator  measurement_type_iter = m_varList.begin();
	   measurement_type_iter != m_varList.end();
	   measurement_type_iter++) {

	for (std::map< SerialNumber_t, std::vector<std::string> >::const_iterator  serial_number_iter = measurement_type_iter->begin();
	     serial_number_iter != measurement_type_iter->end();
	     serial_number_iter++) {

	  for (std::vector<std::string>::const_iterator  variable_iter = serial_number_iter->second.begin();
	       variable_iter != serial_number_iter->second.end();
	       variable_iter++) {

	    VarDef_t var_def = VarDefList::instance()->getVarDef( *variable_iter );
	    if (var_def.isValueWithOrWithoutHistory() ) {
	      ValueVarDef_t &value_def = var_def.valueDef();
	      out_stream << value_def.unit();
	    }
	    if (serial_number_iter->second.end()-variable_iter>1) {
	      out_stream << seperator;
	    }
	  }
	}
      }
      out_stream << std::endl;


      // values
      for (std::map<std::string, CalibrationData::ConnObjDataList_t >::const_iterator conn_iter = calibration_data.calibrationData().begin();
	   conn_iter != calibration_data.calibrationData().end();
	   conn_iter++) {

	std::stringstream conn_out_line;
	bool not_empty = false;
	for (std::vector< std::map< SerialNumber_t, std::vector<std::string> > >::const_iterator  measurement_type_iter = m_varList.begin();
	     measurement_type_iter != m_varList.end();
	     measurement_type_iter++) {

	  EMeasurementType measurement_type = static_cast<EMeasurementType>( measurement_type_iter-m_varList.begin() );
	  if (static_cast<unsigned int>(measurement_type) < conn_iter->second.size()) {
	    const CalibrationData::ConnObjMeasurementList_t &measurement_list = conn_iter->second[ measurement_type ];

	    for (std::map< SerialNumber_t, std::vector<std::string> >::const_iterator  serial_number_iter = measurement_type_iter->begin();
		 serial_number_iter != measurement_type_iter->end();
		 serial_number_iter++) {

	      CalibrationData::ConnObjMeasurementList_t::const_iterator measurement_data_list_iter = measurement_list.find(serial_number_iter->first);
	      if (measurement_data_list_iter != measurement_list.end()) {

		for (std::vector<std::string>::const_iterator  variable_iter = serial_number_iter->second.begin();
		     variable_iter != serial_number_iter->second.end();
		     variable_iter++) {
		  VarDef_t var_def = VarDefList::instance()->getVarDef( *variable_iter );

		  if (var_def.isValue() ) {
		    //		    ValueVarDef_t &value_def = var_def.valueDef();
		    const std::map<VarId_t, float> &value_map( measurement_data_list_iter->second.getValueMap<float>() );
		    std::map<VarId_t, float>::const_iterator value_iter = value_map.find(var_def.id());
		    if (value_iter != value_map.end()) {
		      conn_out_line << value_iter->second;
		      not_empty=true;
		    }
		  }
		  else if (var_def.isState()  ) {
		    StateVarDef_t &state_def = var_def.stateDef();
		    const std::map<VarId_t, State_t> &state_map( measurement_data_list_iter->second.getValueMap<State_t>() );
		    std::map<VarId_t, State_t >::const_iterator state_iter = state_map.find(var_def.id());

		    if (state_iter != state_map.end()) {
		      if (state_def.isValidState(state_iter->second)) {
			const std::string &state_name = state_def.stateName( state_iter->second );
			if (!state_name.empty()) {
			  conn_out_line << state_name;
			}
			else {
			  conn_out_line << "state_" << state_iter->second;
			}
		      }
		      else {
			conn_out_line << "unknown";
		      }
		      not_empty=true;
		    }
		  }
		  else if (var_def.isValueWithHistory() ) {
		    //		    ValueVarDef_t &value_def = var_def.valueDef();
		    const std::map<VarId_t, ValueHistory<float> > &value_map( measurement_data_list_iter->second.getValueMap< ValueHistory<float> >() );
		    std::map<VarId_t, ValueHistory<float> >::const_iterator value_iter = value_map.find(var_def.id());
		    if (value_iter != value_map.end()) {
		      conn_out_line << value_iter->second.newest().value();
		      not_empty=true;
		    }
		  }
		  else if (var_def.isStateWithHistory()  ) {
		    StateVarDef_t &state_def = var_def.stateDef();
		    const std::map<VarId_t, ValueHistory< State_t> > &state_map( measurement_data_list_iter->second.getValueMap< ValueHistory<State_t> >() );
		    std::map<VarId_t, ValueHistory<State_t> >::const_iterator state_iter = state_map.find(var_def.id());

		    if (state_iter != state_map.end()) {
		      State_t newest_state =  state_iter->second.newest().value();
		      if (state_def.isValidState(newest_state)) {
			const std::string &state_name = state_def.stateName( newest_state );
			if (!state_name.empty()) {
			  conn_out_line << state_name;
			}
			else {
			  conn_out_line << "state_" << newest_state;
			}
		      }
		      else {
			conn_out_line << "unknown";
		      }
		      not_empty=true;
		    }
		  }
		  else {
		    std::cerr << "WARNING [VariableNameCollector::dump] Unsupported type : " << var_def.valueType() << std::endl;
		  }

		  if (serial_number_iter->second.end()-variable_iter>1) {
		    conn_out_line << seperator;
		  }
		}
	      }
	      else {
		for (std::vector<std::string>::const_iterator  variable_iter = serial_number_iter->second.begin();
		     variable_iter != serial_number_iter->second.end();
		     variable_iter++) {
		  if (serial_number_iter->second.end()-variable_iter>1) {
		    conn_out_line << seperator;
		  }
		}
	      }
	    }
	  }
	  else {
	    for (std::map< SerialNumber_t, std::vector<std::string> >::const_iterator  serial_number_iter = measurement_type_iter->begin();
		 serial_number_iter != measurement_type_iter->end();
		 serial_number_iter++) {

	      for (std::vector<std::string>::const_iterator  variable_iter = serial_number_iter->second.begin();
		   variable_iter != serial_number_iter->second.end();
		   variable_iter++) {
		if (serial_number_iter->second.end()-variable_iter>1) {
		  conn_out_line << seperator;
		}
	      }

	    }
	  }
	}

	if (not_empty) {
	  out_stream << conn_iter->first << seperator << conn_out_line.str() << std::endl;
	}
      }

    }

    //    std::vector< std::map< SerialNumber_t, std::vector<std::string> > variableList() { return m_varList; }

  private:
    const QTreeWidgetItem *m_lastParent;
    //    const MetaDataItem *m_currentMeasurement;
    std::vector<std::string>                                           *m_currentVarList;
    std::vector< std::map< SerialNumber_t, std::vector<std::string> > > m_varList; 
  };


  MetaDataList::MetaDataList(const std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data,
			     const std::shared_ptr<PixCon::CalibrationDataPalette> &palette,
			     const std::shared_ptr<PixCon::CategoryList> &categories,
			     ISelectionFactory *selection_factory,
			     const std::shared_ptr<PixCon::IMetaDataUpdateHelper> &update_helper,
			     QWidget* parent )
    : QDialog(parent),
      m_selectionFactory(selection_factory),
      m_categories(categories)
    {
      setupUi(this);
      m_metaDataDetails = new MetaDataDetails(calibration_data, palette, update_helper, m_details);
    m_variableFilter.push_back(std::make_pair(new PixA::Regex_t("^_"),true));
    m_filterString->setText("!^_");
    m_metaDataDetails->show();
    vboxLayout1->addWidget(m_metaDataDetails);// m_detailsLayout->addWidget(m_metaDataDetails);
    adjustSize();
    m_metaDataList->setSortingEnabled(true);
    m_metaDataList->sortItems(0, Qt::AscendingOrder);
    m_metaDataList->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(m_metaDataList, SIGNAL(itemClicked(QTreeWidgetItem*, int)), this, SLOT(select(QTreeWidgetItem*, int)));
    connect(m_metaDataDetails, SIGNAL( metaDataHasChanged(PixCon::EMeasurementType,PixCon::SerialNumber_t) ),
	    this,              SLOT( updateMetaDataItem(PixCon::EMeasurementType,PixCon::SerialNumber_t) ) );
    connect( m_metaDataList, SIGNAL( customContextMenuRequested (const QPoint &)),  this, SLOT(showContextMenu(const QPoint&) ) );

    m_metaDataList->setSelectionMode(QTreeWidget::ExtendedSelection);
    rebuildMetaDataList();
    showNoDetails();
    //QValueList<int> sizes = m_splitter->sizes();
    //    assert(sizes.count()==2);

    QShortcut *focus_filter_selector_accel = new QShortcut(QKeySequence(tr("Ctrl+F")),this);
    connect(focus_filter_selector_accel,SIGNAL(activated()) ,m_filterString, SLOT(setFocus()));

    QShortcut *select_list_accel = new QShortcut(QKeySequence(tr("Ctrl+L")),this);
    connect(select_list_accel,SIGNAL(activated()) ,m_metaDataList, SLOT(setFocus()));

    QShortcut *show_hide_details_accel = new QShortcut(QKeySequence(tr("Ctrl+I")),this);
    connect(show_hide_details_accel,SIGNAL(activated()) ,this, SLOT(showHideDetails()));

    //m_splitter->setResizeMode(m_details,QSplitter::KeepSize);
    m_splitter->setStretchFactor(m_splitter->indexOf(m_details), 0);
    m_details->hide();
  }

  MetaDataList::~MetaDataList() {
  }


  bool MetaDataList::hasMetaData(EMeasurementType measurement_type, SerialNumber_t serial_number) const {
    std::string serial_number_string = makeSerialNumberString(measurement_type, serial_number);
    // QTreeWidgetItem::findItems seems to consider only top-level items -> analyses ignored!
    bool haveItem = false;
    for(int ip=0;ip<m_metaDataList->topLevelItemCount();ip++){
      if(m_metaDataList->topLevelItem(ip)->text(0)==QString(serial_number_string.c_str())){
	haveItem=true;
	break;
      }
      for(int ic=0;ic<m_metaDataList->topLevelItem(ip)->childCount();ic++){
	if(m_metaDataList->topLevelItem(ip)->child(ic)->text(0)==QString(serial_number_string.c_str())){
	  haveItem=true;
	  break;
	}
      }
      if(haveItem) break;
    }
    return haveItem;

//     QList <QTreeWidgetItem *> matches = m_metaDataList->findItems ( serial_number_string.c_str(), Qt::MatchExactly  );
//     if (matches.empty()){
//         return false;
//     }
//     else{
//         return true;
//     }
  }

  void MetaDataList::updateMetaData(EMeasurementType measurement_type, SerialNumber_t serial_number) {
    updateMetaDataItem(measurement_type, serial_number);
    updateDetails();
  }

  void MetaDataList::updateMetaDataItem(EMeasurementType measurement_type, SerialNumber_t serial_number)
  {
    std::cout << "INFO [MetaDataList::updateMetaDataStatus] update " << makeSerialNumberString(measurement_type, serial_number) << "." << std::endl;
    assert( measurement_type < kNMeasurements);
    if (measurement_type ==kCurrent) return;

    std::string serial_number_string = makeSerialNumberString(measurement_type, serial_number);
    // QTreeWidgetItem::findItems seems to consider only top-level items -> analyses ignored!
    QList<QTreeWidgetItem *> matches;// = m_metaDataList->findItems(serial_number_string.c_str(),Qt::MatchExactly);
    for(int ip=0;ip<m_metaDataList->topLevelItemCount();ip++){
      if(m_metaDataList->topLevelItem(ip)->text(0)==QString(serial_number_string.c_str())){
	matches<<m_metaDataList->topLevelItem(ip);
      }
      for(int ic=0;ic<m_metaDataList->topLevelItem(ip)->childCount();ic++){
	if(m_metaDataList->topLevelItem(ip)->child(ic)->text(0)==QString(serial_number_string.c_str())){
	  matches<<m_metaDataList->topLevelItem(ip)->child(ic);
	}
      }
    }
    QTreeWidgetItem * item;
    if (matches.empty()) {
      addMetaData(measurement_type, serial_number);
    }
    else {
      item = matches.first();
      MetaDataItem *meta_data_item = dynamic_cast<MetaDataItem *>(item);
      if (!meta_data_item) {
	std::cerr << "FATAL [MetaDataList::updateMetaDataStatus] Item for " << serial_number_string << " is not a meta data item." << std::endl;
      }
      else {

	const  MetaData_t * meta_data = NULL;
	if (measurement_type == kScan ) {
	  const ScanMetaDataCatalogue &scan_meta_data_catalogue = calibrationData().scanMetaDataCatalogue();
	  meta_data = scan_meta_data_catalogue.find(serial_number);
	}
	else if (measurement_type == kAnalysis ) {
	  const AnalysisMetaDataCatalogue &analysis_meta_data_catalogue = calibrationData().analysisMetaDataCatalogue();
	  meta_data = analysis_meta_data_catalogue.find(serial_number);
	}
	if (meta_data) {
	  meta_data_item->setStatus(static_cast<MetaData_t::EStatus>(meta_data->status()));
	  meta_data_item->setQuality(MetaDataDetails::qualityName(meta_data->quality()));
	  meta_data_item->setComment(meta_data->comment());
	}
      }
    }

  }

  void MetaDataList::addMetaData(EMeasurementType measurement_type, SerialNumber_t serial_number)
  {
    assert( measurement_type < kNMeasurements);
    if (measurement_type ==kCurrent) return;

    if (measurement_type == kScan ) {
      const ScanMetaDataCatalogue &scan_meta_data_catalogue = calibrationData().scanMetaDataCatalogue();
      const  ScanMetaData_t * scan_meta_data = scan_meta_data_catalogue.find(serial_number);

      // If an item of the same serial number exists already delete it.
      std::string serial_number_string = makeSerialNumberString(measurement_type, serial_number);
      bool haveItem = false;
      for(int ip=0;ip<m_metaDataList->topLevelItemCount();ip++){
	if(m_metaDataList->topLevelItem(ip)->text(0)==QString(serial_number_string.c_str())){
	  haveItem=true;
	  break;
	}
      }
      if (haveItem) {
	std::cerr << "WARNING [MetaDataList::addMetaData] Have already meta data for " << serial_number_string << "." << std::endl;
      }
      else {
	addScanMetaData(serial_number, scan_meta_data);
      }
    }
    else if (measurement_type == kAnalysis ) {
      const AnalysisMetaDataCatalogue &analysis_meta_data_catalogue = calibrationData().analysisMetaDataCatalogue();
      const  AnalysisMetaData_t * analysis_meta_data = analysis_meta_data_catalogue.find(serial_number);

      // If an item of the same serial number exists already delete it.
      std::string serial_number_string = makeSerialNumberString(measurement_type, serial_number);
      bool haveItem = false;
      for(int ip=0;ip<m_metaDataList->topLevelItemCount();ip++){
	for(int ic=0;ic<m_metaDataList->topLevelItem(ip)->childCount();ic++){
	  if(m_metaDataList->topLevelItem(ip)->child(ic)->text(0)==QString(serial_number_string.c_str())){
	    haveItem=true;
	    break;
	  }
	}
	if(haveItem) break;
      }
      if (haveItem) {
	std::cerr << "WARNING [MetaDataList::addMetaData] Have already meta data for " << serial_number_string << "." << std::endl;
      }
      else {
	addAnalysisMetaData(serial_number, analysis_meta_data);
      }
    }

  }


  void MetaDataList::addScanMetaData(SerialNumber_t serial_number, const  ScanMetaData_t * scan_meta_data)
  {
    if (scan_meta_data) {
      MetaData_t::EQuality quality = MetaDataDetails::getSanitisedQualityTag( scan_meta_data->quality() );
      std::string dateStrg = scan_meta_data->dateString();
      if(dateStrg.substr(dateStrg.length()-1,1)=="\n") dateStrg = dateStrg.substr(0,dateStrg.length()-1);
      //@todo use factory to create items to facilitate porting to QT4
      MetaDataItem *scan_meta_data_item = new MetaDataItem(*this,
							   m_metaDataList,
							   kScan,
							   serial_number,
							   true,
							   dateStrg,
							   scan_meta_data->name(),
							   MetaDataDetails::qualityName(quality),
							   scan_meta_data->comment(),
							   static_cast<MetaData_t::EStatus>(scan_meta_data->status()));

      const AnalysisMetaDataCatalogue &analysis_meta_data_catalogue = calibrationData().analysisMetaDataCatalogue();
      for (std::vector<SerialNumber_t>::const_iterator analysis_iter = scan_meta_data->beginAnalysis();
	   analysis_iter !=  scan_meta_data->endAnalysis();
	   analysis_iter++) {
	const AnalysisMetaData_t *analysis_meta_data = analysis_meta_data_catalogue.find(*analysis_iter);
	if (!analysis_meta_data) {
	  std::cerr << "ERROR [MetaDataList::rebuildMetaDataList] Meta data missing for analysis " << makeSerialNumberString(kAnalysis, *analysis_iter) << std::endl;
	}
	else {
	  addAnalysisMetaData(scan_meta_data_item, *analysis_iter, analysis_meta_data);
	}
      }
      //if (scan_meta_data_item->firstChild()) {
      if (scan_meta_data_item->child(0)) {
            scan_meta_data_item->setExpanded(true);
      }

      //	m_metaDataList->insertItem(m_metaDataList,
    }
  }

  void MetaDataList::addAnalysisMetaData(SerialNumber_t analysis_serial_number, const  AnalysisMetaData_t * analysis_meta_data)
  {
    if (analysis_meta_data) {

      if (analysis_meta_data->scanSerialNumber()==0) {
	std::cerr  << "FATAL [MetaDataList::addAnalysisMetaData] Analyses without scan are currently not supported ("
		   << makeSerialNumberString(kAnalysis, analysis_serial_number) << ")"
		   << std::endl;
	return;
      }

      std::string scan_serial_number_string = makeSerialNumberString(kScan, analysis_meta_data->scanSerialNumber());
      //Q3ListViewItem * scan_meta_data_item = m_metaDataList->findItems( scan_serial_number_string.c_str(), 0 );
      QList <QTreeWidgetItem*> scan_meta_data_item = m_metaDataList->findItems( scan_serial_number_string.c_str(), Qt::MatchExactly );
      if (scan_meta_data_item.empty()) {
	const ScanMetaDataCatalogue &scan_meta_data_catalogue = calibrationData().scanMetaDataCatalogue();
	const  ScanMetaData_t * scan_meta_data = scan_meta_data_catalogue.find( analysis_meta_data->scanSerialNumber() );
	if (scan_meta_data) {
	  addScanMetaData(analysis_meta_data->scanSerialNumber(), scan_meta_data);
	}
	else {
	  std::cerr  << "WARNING [MetaDataList::addAnalysisMetaData] Cannot add scan meta for scan " << makeSerialNumberString(kAnalysis, analysis_serial_number)
		     << " since nothing is known about the associated scan " << scan_serial_number_string << "."
		     << std::endl;
	  return;
	}
	scan_meta_data_item = m_metaDataList->findItems( scan_serial_number_string.c_str(), Qt::MatchExactly);
      }

      if (!scan_meta_data_item.empty()) {
	//QTreeWidgetItem *analysis_meta_data_item = scan_meta_data_item.first()->child(0);
	QTreeWidgetItemIterator itr(scan_meta_data_item.first());
	std::string analysis_serial_number_string1 = makeSerialNumberString(kAnalysis, analysis_serial_number );
	//while (analysis_meta_data_item && analysis_meta_data_item->text(0) != QString::fromStdString(analysis_serial_number_string)) {
	while ((*itr) && (*itr)->text(0) != QString::fromStdString(analysis_serial_number_string1)) {
	  //analysis_meta_data_item = analysis_meta_data_item->nextSibling();
	  ++itr;
	}
	if (*itr) {//analysis_meta_data_item) {
	  std::string analysis_serial_number_string2 = makeSerialNumberString(kAnalysis, analysis_serial_number );
	  std::cout << "WARNING [MetaDataList::addAnalysisMetaData] Cannot add meta data for analysis " << analysis_serial_number_string2
		    << " since meta data already exists for this serial number. " << scan_serial_number_string
		    << std::endl;
	}
	else {
	  addAnalysisMetaData(scan_meta_data_item.first(), analysis_serial_number, analysis_meta_data);
	  if (scan_meta_data_item.first()->child(0)) {
	    //scan_meta_data_item->setOpen(true);
	    scan_meta_data_item.first()->setExpanded(true);
	  }
	}
      }
    }
  }

  void MetaDataList::addAnalysisMetaData(/*Q3ListViewItem*/QTreeWidgetItem *scan_meta_data_item,
					 SerialNumber_t analysis_serial_number,
					 const  AnalysisMetaData_t * analysis_meta_data)
  {
    if (analysis_meta_data) {
      MetaData_t::EQuality quality = MetaDataDetails::getSanitisedQualityTag( analysis_meta_data->quality() );

      /*MetaDataItem *analysis_meta_data_item = */
      std::string analysis_serial_number_string = makeSerialNumberString(kAnalysis, analysis_serial_number );
      std::cout << "INFO [MetaDataList::rebuildMetaDataList] Meta data analysis " << analysis_serial_number_string
		<< " " << analysis_meta_data->name() << std::endl;
      std::string dateStrg = analysis_meta_data->dateString();
      if(dateStrg.substr(dateStrg.length()-1,1)=="\n") dateStrg = dateStrg.substr(0,dateStrg.length()-1);
      //@todo use factory to create items to facilitate porting to QT4
      new MetaDataItem(*this,
		       scan_meta_data_item,
		       kAnalysis,
		       analysis_serial_number,
		       true,
		       dateStrg,
		       analysis_meta_data->name(),
		       MetaDataDetails::qualityName(quality),
		       analysis_meta_data->comment(),
		       static_cast<MetaData_t::EStatus>(analysis_meta_data->status()));
    }
  }

  void MetaDataList::removeMetaData(EMeasurementType measurement_type, SerialNumber_t serial_number)
  {
    assert( measurement_type < kNMeasurements);
    if (measurement_type ==kCurrent) return;

    if (measurement_type == m_metaDataDetails->currentMeasurementType() && serial_number == m_metaDataDetails->currentSerialNumber()) {
      showNoDetails();
    }
    std::string serial_number_string = makeSerialNumberString(measurement_type, serial_number);
    // QTreeWidgetItem::findItems seems to consider only top-level items -> analyses ignored!
    QList<QTreeWidgetItem *> matches;// = m_metaDataList->findItems(serial_number_string.c_str(),Qt::MatchExactly);
    for(int ip=0;ip<m_metaDataList->topLevelItemCount();ip++){
      if(m_metaDataList->topLevelItem(ip)->text(0)==QString(serial_number_string.c_str())){
	matches<<m_metaDataList->topLevelItem(ip);
      }
      for(int ic=0;ic<m_metaDataList->topLevelItem(ip)->childCount();ic++){
	if(m_metaDataList->topLevelItem(ip)->child(ic)->text(0)==QString(serial_number_string.c_str())){
	  matches<<m_metaDataList->topLevelItem(ip)->child(ic);
	}
      }
    }
    QTreeWidgetItem *item;
    if (!matches.empty()) {
      item = matches.first();
      if (item->parent() == NULL) {
	//m_metaDataList->takeItem(item);
	m_metaDataList->takeTopLevelItem(m_metaDataList->indexOfTopLevelItem(item));
	delete item;
      }
      else {
	//item->parent()->takeItem(item);
	item->parent()->removeChild(item);
	delete item;
      }
    }

  }

  void MetaDataList::showHideDetails()
  {
    if (m_details->isVisible()) {
      m_details->hide();
    }
    else {
      m_details->show();
    }
  }

  void MetaDataList::showNoDetails()
  {
    assert(m_metaDataDetails);

    m_metaDataDetails->reset();
    if (m_details->isVisible()) {
      m_details->hide();
    }
  }

  void MetaDataList::showDetails(EMeasurementType measurement_type, SerialNumber_t serial_number)
  {
    assert(m_metaDataDetails);
    if (measurement_type == kCurrent) {
      if (m_metaDataDetails->isVisible()) {
	showNoDetails();
      }
      return;
    }

    if (!m_metaDataDetails->setMeasurement(measurement_type, serial_number) ) return;

    assert( measurement_type == kScan || measurement_type == kAnalysis);
    const MetaData_t *meta_data = NULL;
    if (measurement_type == kScan) {
      const ScanMetaDataCatalogue &scan_meta_data_catalogue = calibrationData().scanMetaDataCatalogue();
      const ScanMetaData_t *scan_meta_data = scan_meta_data_catalogue.find(serial_number);

      if (scan_meta_data) {
	m_metaDataDetails->setType(scan_meta_data->name(), scan_meta_data->scanConfigTag(), scan_meta_data->scanConfigRevision());
	m_metaDataDetails->setDuration(scan_meta_data->duration());
	PixA::ConnectivityRef conn( calibrationData().connectivity(measurement_type, serial_number) );
	if (conn) {
	  m_metaDataDetails->setCfg1( "Configration :", "", conn.tag(PixA::IConnectivity::kConfig),conn.revision(), MetaDataDetails::kNormal );
	  m_metaDataDetails->setCfg2( "Module Config. :", "", conn.tag(PixA::IConnectivity::kModConfig),conn.revision(),
				      MetaDataDetails::kNormal /*conn.configDb().isTemporary()*/);
	}

      }
      meta_data = scan_meta_data;
    }
    else {
      const AnalysisMetaDataCatalogue &analysis_meta_data_catalogue = calibrationData().analysisMetaDataCatalogue();
      const AnalysisMetaData_t *analysis_meta_data = analysis_meta_data_catalogue.find(serial_number);
      if (analysis_meta_data) {
	// @todo show scan serial number
	m_metaDataDetails->setDuration(0);

	m_metaDataDetails->setType(analysis_meta_data->analysisName(), analysis_meta_data->analysisTag(),
				   analysis_meta_data->analysisRevision());
	m_metaDataDetails->setCfg1( "Classification :", analysis_meta_data->decisionName(), analysis_meta_data->decisionTag(), 
				    analysis_meta_data->decisionRevision(), MetaDataDetails::kNormal);
	m_metaDataDetails->setCfg2( "Post-processing:", analysis_meta_data->postProcessingName(), analysis_meta_data->postProcessingTag(), 
				    analysis_meta_data->postProcessingRevision(), MetaDataDetails::kNormal);

	meta_data = analysis_meta_data;
      }
    }

    if (!meta_data) {
      showNoDetails();
    }
    else {
      m_metaDataDetails->setStartTime( meta_data->date());

      m_metaDataDetails->setStatus( meta_data->status() );
      m_metaDataDetails->setQuality(meta_data->quality() );
      m_metaDataDetails->setComment(meta_data->comment() );
      if (!m_details->isVisible()) {
	m_details->show();
	QList<int> sizes = m_splitter->sizes();
	if (sizes.size()==2 && sizes[1]<=1) {
	  sizes[1]=m_details->minimumSizeHint().height();
	  sizes[0]-=sizes[1];
	  if (sizes[0]>=m_metaDataList->minimumSizeHint().height()) {
	    m_splitter->setSizes(sizes);
	  }
	}
      }
      emit changeDataSelection(measurement_type, serial_number);
    }
  }

  void MetaDataList::updateDetails()
  {
    EMeasurementType measurement_type = m_metaDataDetails->currentMeasurementType();
    SerialNumber_t serial_number = m_metaDataDetails->currentSerialNumber();

    if (measurement_type == kCurrent || measurement_type>=kNMeasurements) return;
    assert( measurement_type == kScan || measurement_type == kAnalysis);

    const MetaData_t *meta_data = NULL;
    if (measurement_type == kScan) {
      const ScanMetaDataCatalogue &scan_meta_data_catalogue = calibrationData().scanMetaDataCatalogue();
      const ScanMetaData_t *scan_meta_data = scan_meta_data_catalogue.find(serial_number);
      m_metaDataDetails->setDuration(scan_meta_data->duration());
      meta_data = scan_meta_data;
    }
    else if (measurement_type==kAnalysis) {
      const AnalysisMetaDataCatalogue &analysis_meta_data_catalogue = calibrationData().analysisMetaDataCatalogue();
      const AnalysisMetaData_t *analysis_meta_data = analysis_meta_data_catalogue.find(serial_number);
      meta_data = analysis_meta_data;
    }

    if (meta_data) {
      m_metaDataDetails->setStatus( meta_data->status() );
      m_metaDataDetails->setQuality( meta_data->quality() );
      m_metaDataDetails->setComment( meta_data->comment() );
    }
  }

  void MetaDataList::rebuildMetaDataList()
  {
    m_metaDataList->clear();
    std::map<SerialNumber_t, /*Q3ListViewItem*/ QTreeWidgetItem *> scan_item_map;
    const ScanMetaDataCatalogue &scan_meta_data_catalogue = calibrationData().scanMetaDataCatalogue();
    for (std::map< SerialNumber_t, ScanMetaData_t >::const_iterator scan_iter = scan_meta_data_catalogue.begin();
	 scan_iter != scan_meta_data_catalogue.end();
	 scan_iter++) {
      addScanMetaData(scan_iter->first, &scan_iter->second);
      //	m_metaDataList->insertItem(m_metaDataList,
    }
  }

  void MetaDataList::select(/*Q3ListViewItem*/QTreeWidgetItem *item) {
    //std::cout << "INFO [MetaDataList::select]" << std::endl;
    if (!item) return;
    const MetaDataItem *meta_data_item1 = dynamic_cast<MetaDataItem *>(item);
    if (meta_data_item1 ) {
      showDetails(meta_data_item1->measurementType(), meta_data_item1->serialNumber());
    }
    else {
      const VariableItem *variable_item = dynamic_cast<VariableItem *>(item);
      if (variable_item) {
	//Q3ListViewItem *parent = variable_item->parent();
	QTreeWidgetItem *parent = variable_item->parent();
	while (parent) {
	  std::string category_name = parent->text(0).toLatin1().data();
	  const MetaDataItem *meta_data_item2 = dynamic_cast<MetaDataItem *>(parent);
	  if (meta_data_item2) {
	    showDetails(meta_data_item2->measurementType(), meta_data_item2->serialNumber());
	    emit changeVariable(QString::fromStdString(category_name), variable_item->variableName());
	    break;
	  }
	  parent = parent->parent();
	}
      }
    }
  }

  void MetaDataList::select(QTreeWidgetItem *item, int) {
    select(item);
  }

// Q3ListViewItem *findItem(Q3ListViewItem *sibling, const QString &name) {
 QTreeWidgetItem *findItem(QTreeWidgetItem *sibling, const QString &name) {
 QTreeWidgetItemIterator itr(sibling);
     while (*itr) {
      VariableItem *variable_item = dynamic_cast<VariableItem *>((*itr));
      if (variable_item) {
            if (variable_item->variableName()==name) return (*itr);
      }
     // sibling=sibling->nextSibling();
      ++itr;
    }
    return NULL;
  }

 // void MetaDataList::showHideVariables(Q3ListViewItem *item) {
 void MetaDataList::showHideVariables(QTreeWidgetItem *item) {
      TemporaryCursorChange busy;
    if (!item) return;
    //    std::cout << "INFO [MetaDataList::showHideVariables] " << item->text(0) << " is open ? " << (item->isOpen() ? "yes" : "no" ) << std::endl;
    MetaDataItem *meta_data_item1 = dynamic_cast<MetaDataItem *>(item);
    if (meta_data_item1) {
      bool has_variables=false;
      // check whether there are variable items
      //Q3ListViewItem *child = meta_data_item1->firstChild();
      QTreeWidgetItemIterator chld(meta_data_item1);
      while (*chld) {
    if (!dynamic_cast<MetaDataItem *>((*chld))) {
	  has_variables=true;
	}
    //child=child->nextSibling();
            ++chld;
      }
      if (has_variables) {

	// close and remove all children
	// ... except of meta data items
    //meta_data_item1->setOpen(false);
    meta_data_item1->setExpanded(false);
    QTreeWidgetItemIterator itr1(meta_data_item1);
   // for (Q3ListViewItem *child_iter = meta_data_item1->firstChild();
    //     child_iter != NULL;
    //     ) {
      //Q3ListViewItem *next_child_iter = child_iter->nextSibling();
    while(*itr1){
      MetaDataItem *meta_data_item2 = dynamic_cast<MetaDataItem *>((*itr1));
      if (!meta_data_item2) {
        delete (*itr1);
        }
            ++itr1;
        }

         }
      else {

	// add categories and variables.
	std::set<std::string> var_list;
	calibrationData().calibrationData().varList( meta_data_item1->measurementType(), meta_data_item1->serialNumber(), var_list);
	for( std::map<std::string, std::shared_ptr< std::vector< std::string > > >::const_iterator  category_iter = m_categories->begin();
	     category_iter != m_categories->end();
	     ++category_iter) {
             //Q3ListViewItem *category_item=new CategoryItem( meta_data_item1, category_iter->first );
             QTreeWidgetItem *category_item=new CategoryItem( meta_data_item1, category_iter->first );
            unsigned int child_counter1=0;
         for (std::vector<std::string>::const_iterator var_iter = category_iter->second->begin();
	       var_iter != category_iter->second->end();
	       var_iter++) {
        // Q3ListViewItem *parent = NULL;
             QTreeWidgetItem *parent = NULL;
	    // variables which start with "_" for which there an item withe the same name minus the heading "_" and an extension
	    // are put grouped as children.
	    if (var_iter->size()>0 && (*var_iter)[0]=='_') {
	      std::string::size_type pos = var_iter->rfind("_t");
	      if (pos != std::string::npos) {
        parent = findItem( category_item->/*firstChild()*/child(0), QString::fromStdString( var_iter->substr(1,pos-1)) );
	      }
	    }
	    if (!parent) parent=category_item;
	    if (var_list.find(*var_iter) != var_list.end()) {
	      bool matches=m_variableFilter.empty();
	      for (std::vector< std::pair<PixA::Regex_t *, bool> >::const_iterator pattern_iter = m_variableFilter.begin();
		   pattern_iter !=m_variableFilter.end();
		   ++pattern_iter) {
		if (pattern_iter->first->match(*var_iter) ^ pattern_iter->second) {
		  matches=true;
		  break;
		}
	      }
	      if (matches) {
		new VariableItem( parent, *var_iter,child_counter1++ );
	      }
	    }

	  }
      if (!category_item->child(0)) {
	    delete category_item;
	  }
	  else {
	    unsigned int child_counter2=0;
	    category_item->sortChildren(0,Qt::AscendingOrder); //Sort from first column in ascending order
            QTreeWidgetItemIterator itr2(category_item);
//       for (Q3ListViewItem *child_iter = category_item->firstChild();
//		 child_iter != NULL;
//		 child_iter=child_iter->nextSibling()) {
            while(*itr2){
          VariableItem *variable_item = dynamic_cast<VariableItem *>((*itr2));
	      if (variable_item) {
		variable_item->setChildIndex( child_counter2++);
	      }
            ++itr2;
        }
	  }
	}
    //meta_data_item1->setOpen(true);
      meta_data_item1->setExpanded(true);
      }

    }
  }

 // void MetaDataList::open(Q3ListViewItem *item)
    void MetaDataList::open(QTreeWidgetItem *item)
  {
    if (!item) return;
    MetaDataItem *meta_data_item = dynamic_cast<MetaDataItem *>(item);
    if (meta_data_item) {
      showHideVariables(item);
    }
    else {
      //item->setOpen(!item->isOpen());
        item->setExpanded(!item->isExpanded());
    }
  }

  void MetaDataList::showContextMenu(const QPoint& point) {

    QTreeWidgetItem *item = m_metaDataList->itemAt(point);
    if(item==0) // click in empty part, un-select all
      m_metaDataList->clearSelection();
    else
      item->setSelected(true);

    MetaDataItemBase::EItemType type = MetaDataItemBase::getType(item);
    CountItems count(type);
    forEach(m_metaDataList->topLevelItem(0), type,count);
    //    count.dump();

    if (MetaDataItemBase::typesMatch(type,MetaDataItemBase::kVariable) && count.count()==1) {
      VariableContextMenuAction variable_context_menu(*this);
      forEach(m_metaDataList->topLevelItem(0), type,variable_context_menu);
    }
    else {
      QMenu context_menu_q4(item ? item->text(0).toLatin1().data() : "AddMetaData");
      QVariant qType(type);
      QList <QVariant> qvList;
      qvList.append(qType);

//       if (item) {
//@todo search MetaDataItem and display serial number ?
// 	context_menu.insertItem( item->text(0), kTitle);
// 	context_menu.setItemEnabled(0,false);
// 	context_menu.insertSeparator();
//       }
      bool need_separator=false;
      if (count.count()==0) {
      context_menu_q4.addAction("Add Scan / Analysis",this,SLOT(AaddMeasurement() ) );
	need_separator=true;
      }

      //const MetaDataItem *scan_item=NULL;

      if (MetaDataItemBase::typesMatch(type,MetaDataItemBase::kMetaData) && count.count()==1) {
	need_separator=true;

	assert ( dynamic_cast<const MetaDataItem *>(item) );
	const MetaDataItem *meta_data_item = static_cast<const MetaDataItem *>(item);

	if (meta_data_item->measurementType()==kScan) {
	  //scan_item = meta_data_item;
	  
	  QAction *aScanParameter = context_menu_q4.addAction("Show Scan Parameter",this,SLOT(showScanParameter() ) );
	  aScanParameter->setData(qvList);
	}
	else if (meta_data_item->measurementType()==kAnalysis) {

	  const AnalysisMetaDataCatalogue &analysis_meta_data_catalogue = calibrationData().analysisMetaDataCatalogue();
	  const  AnalysisMetaData_t * analysis_meta_data = analysis_meta_data_catalogue.find(meta_data_item->serialNumber());
	  if (analysis_meta_data->analysisConfig() || !analysis_meta_data->analysisTag().empty() ) {
	    //@todo verify that the configuration could exist ?
	    QAction *new_act = context_menu_q4.addAction("Show Analysis Parameter",this,SLOT(showAnalysisParameter() ) );
	    new_act->setData(qvList);
	  }
	  if (analysis_meta_data->decisionConfig() || !analysis_meta_data->decisionTag().empty() ) {
	    QAction *new_act = context_menu_q4.addAction("Show Analysis Classification Parameter",this,SLOT(showClassificationPar() ) );
	    new_act->setData(qvList);
	  }
	  if (analysis_meta_data->postProcessingConfig() || !analysis_meta_data->postProcessingTag().empty() ) {
	    QAction *new_act = context_menu_q4.addAction("Show Analysis Post-processing Parameter",this,SLOT(showPostProcessingParameter() ) );
	    new_act->setData(qvList);
	  }
	}
      }

      if (   MetaDataItemBase::typesMatch(type,MetaDataItemBase::kCategory)
	  || MetaDataItemBase::typesMatch(type,MetaDataItemBase::kVariable) 
	  || MetaDataItemBase::typesMatch(type,MetaDataItemBase::kMetaData)) {

	if (type == MetaDataItemBase::kMetaDataAnalysis && count.specificCount()>1) {
	  need_separator=true;
    QAction *new_act = context_menu_q4.addAction("Combine Analysis Results",this,SLOT(combineAnalysisResults() ) );
    new_act->setData(qvList);
	}
    if (!MetaDataItemBase::typesMatch(type,MetaDataItemBase::kMetaData) || item->isExpanded()) {
	  need_separator=true;
    QAction *new_act = context_menu_q4.addAction("Dump Variables",this,SLOT(dumpVariables() ) );
    new_act->setData(qvList);
	}

      }
      if (MetaDataItemBase::typesMatch(type,MetaDataItemBase::kMetaData)) {
	if (need_separator) {
	  context_menu_q4.addSeparator();
	}
       QAction *removeItem = context_menu_q4.addAction("Remove",this,SLOT(remove() ) );
       removeItem->setData(qvList);
      }

      context_menu_q4.exec(QCursor::pos());

    }
  }

  void MetaDataList::AaddMeasurement(){
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(sender==0) return;
    emit addMeasurement();
  }
  void MetaDataList::showScanParameter(){
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(sender==0) return;
    QVariant var = sender->data();
    QList<QVariant> varList = var.toList();
    //MetaDataList as valList[0] as this already known and available 
    //MetaDataItem *scan_item=varList[1].value<MetaDataItem*>();
    MetaDataItemBase::EItemType type = (MetaDataItemBase::EItemType)varList[0].toInt();
    //new part for scan item:
    //const MetaDataItem *scan_item = static_cast<const MetaDataItem *>(m_Q3ListViewItem);
    ShowScanConfigAction show_scan_config(*this);
    //if (scan_item) {
    //  show_scan_config(*scan_item);
    //}
   // else {
      forEach(m_metaDataList->topLevelItem(0), type,show_scan_config);
   // }
  }
  void MetaDataList::showAnalysisParameter(){
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(sender==0) return;
    QVariant var = sender->data();
    QList<QVariant> varList = var.toList();
    MetaDataItemBase::EItemType type = (MetaDataItemBase::EItemType)varList[0].toInt();
    // should used EClass definition of ObjectConfigDbInstanvce, but don't like to add dependency on CAN for this sub package
    ShowAnalysisConfigAction show_analysis_config(*this, static_cast<unsigned short>( AnalysisMetaData_t::kAnalysis ));
    forEach(m_metaDataList->topLevelItem(0), type,show_analysis_config);
  }
  void MetaDataList::showClassificationPar(){
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(sender==0) return;
    QVariant var = sender->data();
    QList<QVariant> varList = var.toList();
    MetaDataItemBase::EItemType type = (MetaDataItemBase::EItemType)varList[0].toInt();
    ShowAnalysisConfigAction show_analysis_config(*this, static_cast<unsigned short>( AnalysisMetaData_t::kDecision ) );
    forEach(m_metaDataList->topLevelItem(0), type,show_analysis_config);
  }
  void MetaDataList::showPostProcessingParameter(){
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(sender==0) return;
    QVariant var = sender->data();
    QList<QVariant> varList = var.toList();
    MetaDataItemBase::EItemType type = (MetaDataItemBase::EItemType)varList[0].toInt();
    ShowAnalysisConfigAction show_analysis_config(*this, static_cast<unsigned short>( AnalysisMetaData_t::kPostProcessing ) );
    forEach(m_metaDataList->topLevelItem(0), type,show_analysis_config);
  }
  void MetaDataList::AcombineAnalysisResults(){
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(sender==0) return;
    combineAnalysisResults();
  }
  void MetaDataList::dumpVariables(){
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(sender==0) return;
    QVariant var = sender->data();
    QList<QVariant> varList = var.toList();
    MetaDataItemBase::EItemType type = (MetaDataItemBase::EItemType)varList[0].toInt();
    QStringList filter;
    filter +=  "Comma separated values (*.csv)";
    filter +=  "Variable Dump (*.txt)";
    filter += "Any file (*.*)";

    //Q3FileDialog file_chooser(QString::null, QString::null, this,"Select output file for variables.",TRUE);
    QFileDialog file_chooser(this, "Select output file for variables.", QString::null, QString::null);
    file_chooser.setFileMode(QFileDialog::AnyFile);
    file_chooser.setNameFilters(filter);
    if(file_chooser.exec() == QDialog::Accepted && file_chooser.selectedFiles().at(0).length()>0) {
      std::ofstream out_stream(file_chooser.selectedFiles().at(0).toLatin1().data());
      if (out_stream) {
	std::string file_name = file_chooser.selectedFiles().at(0).toLatin1().data();
	bool comma_separated =  (file_name.find(".csv")==file_name.size()-4) ;
	VariableNameCollector variable_name_collector;
    forEach(m_metaDataList->topLevelItem(0), type,variable_name_collector);
	variable_name_collector.dump(out_stream, calibrationData(), comma_separated );
	std::cout << "INFO [MetaDataList::showContextMenu] Dump selected variables into file : "
		  << file_chooser.selectedFiles().at(0).toLatin1().data() << std::endl;
      }
    }
  }
  void MetaDataList::remove(){
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(sender==0) return;
    QVariant var = sender->data();
    QList<QVariant> varList = var.toList();
    MetaDataItemBase::EItemType type = (MetaDataItemBase::EItemType)varList[0].toInt();
    RemoveMetaDataAction remove_meta_data(*this);
    forEach(m_metaDataList->topLevelItem(0), type,remove_meta_data);
  }
  
  
  void MetaDataList::showMetaDataContextMenu(/*Q3ListViewItem*/QTreeWidgetItem * /*item*/, const QPoint&, int)
  {
  }

//   void MetaDataList::showValueContextMenu(QListViewItem *item, const QPoint&, int)
//   {
//     const VariableItem *variable_item = dynamic_cast<VariableItem *>(item);
//     if (variable_item ) {
//       try {
// 	VarDef_t var_def =VarDefList::instance()->getVarDef(variable_item->variableName().toLatin1().data());

// 	QPopupMenu context_menu(NULL,variable_item->variableName());
// 	enum EMenuChoice {kTitle, kShowHistogram, kNItems};

// 	context_menu.insertItem( item->text(0), kTitle);
// 	context_menu.setItemEnabled(0,false);
// 	context_menu.insertSeparator();

// 	context_menu.insertItem("Show Histogram", kShowHistogram);
// 	int ret=context_menu.exec(QCursor::pos());
// 	switch (ret) {
// 	case kSelect: {
// 	  showDetails( meta_data_item->measurementType(), meta_data_item->serialNumber());
// 	  break;
// 	}
// 	case kRemove: {
// 	  removeMetaData( meta_data_item->measurementType(), meta_data_item->serialNumber() );
// 	  break;
// 	}
// 	default:
// 	  break;
// 	}
//       }
//     }
//     catch (CalibrationDataException &) {
//     }
//   }

  void MetaDataList::selectMeasurement(EMeasurementType measurement_type, SerialNumber_t serial_number) {
    std::string serial_number_string = makeSerialNumberString(measurement_type, serial_number);
    if (measurement_type == kScan ) {
      // QTreeWidgetItem::findItems seems to consider only top-level items -> analyses ignored!
      QList<QTreeWidgetItem *> matches;// = m_metaDataList->findItems(serial_number_string.c_str(),Qt::MatchExactly);
      for(int ip=0;ip<m_metaDataList->topLevelItemCount();ip++){
	if(m_metaDataList->topLevelItem(ip)->text(0)==QString(serial_number_string.c_str())){
	  matches<<m_metaDataList->topLevelItem(ip);
	}
	for(int ic=0;ic<m_metaDataList->topLevelItem(ip)->childCount();ic++){
	  if(m_metaDataList->topLevelItem(ip)->child(ic)->text(0)==QString(serial_number_string.c_str())){
	    matches<<m_metaDataList->topLevelItem(ip)->child(ic);
	  }
	}
      }
      QTreeWidgetItem *item;
      if (!matches.empty()) {
	item = matches.first();
	MetaDataItem *meta_data_item = dynamic_cast<MetaDataItem *>(item);
	if (meta_data_item ) {
	  //m_metaDataList->clearSelection();
	  //m_metaDataList->setSelected(item,true);
	  meta_data_item->setSelected(true);
	}
      }
    }

  }

  class CollectAnalyses : public ItemAction
  {
  public:
    //    CollectAnalyses()  { }

    void operator()(const MetaDataItemBase &item) {
      const MetaDataItem *meta_data_item = dynamic_cast<const MetaDataItem *>(&item);
      if (meta_data_item && meta_data_item->measurementType()==kAnalysis) {
	m_analysisList.insert(meta_data_item->serialNumber());
      }
    }
    //    std::vector< std::map< SerialNumber_t, std::vector<std::string> > variableList() { return m_varList; }
    const std::set<SerialNumber_t> &analysisList() const { return m_analysisList; }

  private:
    std::set<SerialNumber_t> m_analysisList;
  };

  
  class AnalysisCombiner : public CalibrationDataFunctorBase
  {
  public:
    AnalysisCombiner(CalibrationData &calibration_data,
		     EMeasurementType destination_type,
		     SerialNumber_t destination_serial_number)
      : /*CalibrationDataFunctorBase(), all */
	m_calibrationData(&calibration_data),
	m_destinationMeasurementType(destination_type),
	m_destinationSerialNumber(destination_serial_number)
    { 
      // the combiner currently only supports float and state.
      // assume that there are just 2 value types : float , state
      //  now there are also the float and states with history.
      // Since the analysis does not create variables with history
      // the analysis combiner is still fine.
      assert(kNValueTypes==4); 
    }

    void setSelection(IConnItemSelection *selection) {
      m_itemSelection=std::unique_ptr<IConnItemSelection>(selection);
    }

    /** Copy the values from the given conn obj if it is selected and write them to the destination container.
     * Old values will be overwritter i.e. the last value survives.
     */
    void operator()( EConnItemType /*item_type*/,
		     const std::string &conn_name,
		     const std::pair<EMeasurementType,SerialNumber_t> &measurement,
		     PixCon::CalibrationData::ConnObjDataList &data_list,
		     CalibrationData::EnableState_t /*is_enable_stat*/,
		     CalibrationData::EnableState_t /*new_enable_state*/) 
    {
      if (measurement.first!=kAnalysis) return;
      if (!m_itemSelection.get() || m_itemSelection->isSelected(conn_name)) {
	copyValues<float>(conn_name, data_list, measurement);
	copyValues<State_t>(conn_name, data_list, measurement);
      }
    }

    void setVarList(const std::set<std::string> &var_list) {
      VarDefList *var_def_list = VarDefList::instance();
      for (unsigned int value_type = 0; value_type< kNValueTypes; value_type++) {
	m_varList[value_type].clear();
      }
      for (std::set<std::string>::const_iterator var_iter = var_list.begin();
	   var_iter != var_list.end();
	   ++var_iter) {
	try {
	  VarDef_t var_def = var_def_list->getVarDef( *var_iter );
	  if (var_def.isValueWithOrWithoutHistory() ) {
	    m_varList[kValue].push_back( var_def  );
	  }
	  else if (var_def.isStateWithOrWithoutHistory() ) {
	    m_varList[kState].push_back( var_def  );
	  }
	  else {
	    std::cerr << "WARNING [AnalysisCombiner::setVarList] Unsupported type for " << *var_iter << " : " << var_def.valueType() << std::endl;
	  }
	}
	catch( ... ) {
	}
      }
    }


    // do nothing intentionally
    void operator()( EConnItemType /*item_type*/,
     		     const std::string &/*conn_name*/,
     		     const std::pair<EMeasurementType,SerialNumber_t> &/*measurement*/,
     		     CalibrationData::EnableState_t /*new_enable_state*/)
    {
    }
    
  private:

    template <class T>
    EValueType valueType();

    template <class T>
    void copyValues(const std::string &conn_name,
		    PixCon::CalibrationData::ConnObjDataList &data_list,
		    const std::pair<EMeasurementType,SerialNumber_t> &src_measurement) {
      EValueType value_type = this->valueType<T>();
      assert(value_type < kNValueTypes );
      for(std::vector<VarDef_t>::const_iterator var_iter = m_varList[value_type].begin();
	  var_iter != m_varList[value_type].end();
	  ++var_iter) {
	try {
	  T a_value = data_list.value<T>(src_measurement.first, src_measurement.second, var_iter->id());
	  m_calibrationData->addValue<T>(conn_name, m_destinationMeasurementType, m_destinationSerialNumber, *var_iter, a_value);
	}
	catch (CalibrationData::MissingValue &) {
	}
      }
    }

    std::unique_ptr<IConnItemSelection> m_itemSelection;

    CalibrationData    *m_calibrationData;
    EMeasurementType m_destinationMeasurementType;
    SerialNumber_t m_destinationSerialNumber;
    std::vector< VarDef_t > m_varList[kNValueTypes];
  };

  template <> EValueType AnalysisCombiner::valueType<float>() {
    return kValue;
  }

  template <> EValueType AnalysisCombiner::valueType<State_t>() {
    return kState;
  }

  void MetaDataList::combineAnalysisResults() {
    CollectAnalyses analysis_collector;
    forEach(m_metaDataList->topLevelItem(0), MetaDataItemBase::kMetaDataAnalysis,analysis_collector);

    if (analysis_collector.analysisList().size()>1) {

      // search for the first free analysis serial number below 1000
      SerialNumber_t destination_serial_number=0;
      const AnalysisMetaDataCatalogue &analysis_catalogue( calibrationData().analysisMetaDataCatalogue() );
      for (std::map< SerialNumber_t, AnalysisMetaData_t >::const_iterator analysis_iter = analysis_catalogue.begin();
	   analysis_iter != analysis_catalogue.end() && analysis_iter->first<1000;
	   ++analysis_iter) {
	destination_serial_number=analysis_iter->first;
      }
      destination_serial_number++;
      if (destination_serial_number<1000) {
	bool created_meta_data=false;
	{
	std::unique_ptr<PixA::IBusyLoop> busy_loop(PixA::BusyLoopFactory::busyLoop(" Combine analysis results ..."));

	try {
	  AnalysisCombiner analysis_combiner(calibrationData().calibrationData(), kAnalysis, destination_serial_number);
	  // the last value survives, so start with lowest analysis serial number
	  for(std::set<SerialNumber_t>::const_iterator analysis_iterator = analysis_collector.analysisList().begin();
	      analysis_iterator != analysis_collector.analysisList().end();
	      ++analysis_iterator) {
	    std::set<std::string> var_list;

	    {
	      Lock lock(calibrationData().calibrationDataMutex());
	      calibrationData().calibrationData().varList(kAnalysis, *analysis_iterator, var_list);
	    }
	    analysis_combiner.setVarList(var_list);

	    analysis_combiner.setSelection( m_selectionFactory->selection(true,false, kAnalysis, *analysis_iterator) );

	    if (!created_meta_data) {
	      std::string combined_analysis_comment;
	      combined_analysis_comment = "CombinationOfAnalyses:";
	      {
	      bool sep=false;
	      for(std::set<SerialNumber_t>::const_iterator analysis_iter = analysis_collector.analysisList().begin();
		  analysis_iter != analysis_collector.analysisList().end();
		  ++analysis_iter) {
		if (sep) {
		  combined_analysis_comment += ":";
		}
		combined_analysis_comment += makeSerialNumberName(kAnalysis, *analysis_iter);
		sep=true;
	      }
	      }

	      const AnalysisMetaData_t *analysis_info(calibrationData().analysisMetaDataCatalogue().find(*analysis_iterator));
	      if (!analysis_info) {
		std::cerr << "ERROR [MetaDataList::combineAnalysisResults] Analysis " << makeSerialNumberString(kAnalysis, *analysis_iterator)
			  << " given for combination, but nothing known about the analysis. Will be ignored." << std::endl;
		continue;
	      }
	      PixA::ConfigHandle invalid_config_handle;
	      calibrationData().addAnalysis(destination_serial_number,
					    ::time(0),
					    "AnalysisMerge",
					    combined_analysis_comment,
					    MetaData_t::kNormal,
					    analysis_info->scanSerialNumber(),
					    invalid_config_handle,
					    "",
					    invalid_config_handle,
					    "",
					    invalid_config_handle);
	      created_meta_data=true;
	    }
	      
	    {
	      Lock lock(calibrationData().calibrationDataMutex());
	      calibrationData().forEach(kAnalysis, *analysis_iterator, analysis_combiner, false);
	    }
	  }
	}
	catch(PixA::BaseException &err) {
	  std::cerr << "ERROR [MetaDataList::combineAnalysisResults] Caught exception : " << err.getDescriptor() << std::endl;
	}
	catch(std::exception &err) {
	  std::cerr << "ERROR [MetaDataList::combineAnalysisResults] Caught exception : " << err.what() << std::endl;
	}
	catch( ...) {
	  std::cerr << "ERROR [MetaDataList::combineAnalysisResults] Caught unknown exception." << std::endl;
	}
	}
	if (created_meta_data) {
	  addMetaData(kAnalysis, destination_serial_number);
	}
      }
      else {
	std::cerr << "ERROR [MetaDataList::combineAnalysisResults] No free analysis serial number below 1000."
	  " Remove one of the analyses with an analysis serial number below 1000 and try again." << std::endl;
      }
    }
    else {
	std::cerr << "WARNING [MetaDataList::combineAnalysisResults] Only one analysis selected."
		  << std::endl;
    }
  }

  void MetaDataList::clearFilter() {
    std::cout << "INFO [MetaDataList::clearFilter]" << std::endl;
    clearPatternList();
    m_filterString->setText("");
    updatedVariableLists();
  }

  void MetaDataList::clearPatternList() {
    for(std::vector< std::pair<PixA::Regex_t *, bool> >::iterator pattern_iter=m_variableFilter.begin();
	pattern_iter != m_variableFilter.end();
	++pattern_iter) {
      delete pattern_iter->first;
      pattern_iter->first=NULL;
    }
    m_variableFilter.clear();
  }

  void MetaDataList::changeFilter() {
    std::cout << "INFO [MetaDataList::changeFilter]" << std::endl;

    clearPatternList();

    std::string pattern_list(m_filterString->text().toLatin1().data());
    TokenDescription negation;
    negation.addOperator("!",1);
    negation.setAllowedSpecialCharList("_.:");
    Tokeniser tokeniser(negation, &pattern_list);

    bool error=false;
    bool negate=false;
    while (tokeniser.nextToken()) {

      if (tokeniser.isOperator()) {
	if (tokeniser.operatorId()!=1) {
	  error=true;
	}
	else {
	  negate = !negate;
	}
      }
      else if (tokeniser.isWildcardPattern()) {

	std::string pattern=tokeniser.token();
	regex_t m_regex;
	if (regcomp(&m_regex, pattern.c_str(), REG_EXTENDED)==0) {
	  m_variableFilter.push_back(std::make_pair(new PixA::Regex_t(pattern),negate));
	}
	else {
	  error=true;
	}
	negate=false;
      }
      else {
	error=true;
      }
      
    }

    if (error) {
      QColor white("white");
      QColor highlight_color("yellow");
      
      if (!m_highlighter.get()) {
	m_highlighter=std::make_unique<Highlighter>(*m_filterString,highlight_color,white,2000);
      }
      else {
	m_highlighter->restart(highlight_color,1500);
      }
    }
    else {
      updatedVariableLists();
    }

  }


  class CategoryUpdater : public NonConstItemAction
  {
  public:
    CategoryUpdater(MetaDataList &meta_data_list) : m_metaDataList(&meta_data_list) { }

    ~CategoryUpdater() {
      update();
    }

    void operator()(MetaDataItemBase &item) {
      if (item.isExpanded()) {
	m_itemList.push_back(&item);
     }
    }

    void update() {

      std::vector<std::string> open_childs;
      //for(std::vector<Q3ListViewItem *>::iterator item_iter = m_itemList.begin();
      for(std::vector<QTreeWidgetItem *>::iterator item_iter = m_itemList.begin();
      item_iter != m_itemList.end();
      ++item_iter) {

    {
      //Q3ListViewItem *child = (*item_iter)->firstChild();
      QTreeWidgetItem *child = (*item_iter)->child(0);
      int chld_count = (*item_iter)->childCount();
      int indx = 0;
	  while (child) {
	    std::cout << "INFO [CategoryUpdater::update] " << child->text(0).toLatin1().data() << std::endl;
        if (child->isExpanded()) {
	      open_childs.push_back(child->text(0).toLatin1().data());
	    }
        //Q3ListViewItem *next_child = child->nextSibling();
        QTreeWidgetItem *next_child;
        if (indx+1 < chld_count){
            next_child = (*item_iter)->child(indx+1);
            ++indx;
        } else {
            next_child = 0;
        }
	    MetaDataItemBase *meta_data = dynamic_cast<MetaDataItemBase *>(child);
	    if (meta_data && !meta_data->typeMatches(MetaDataItemBase::kMetaData)) {
	      delete child;
	    }
	    child = next_child;
	  }
    } // end of wierd loop
    (*item_iter)->setExpanded(false);
	m_metaDataList->showHideVariables(*item_iter);
    (*item_iter)->setExpanded(true);
	// re-open items
	{
      QTreeWidgetItem *child = (*item_iter)->child(0);
      int chld_count = (*item_iter)->childCount();
      int indx = 0;
	  while (child) {
	    std::string name(child->text(0).toLatin1().data());
	    if (find(open_childs.begin(),open_childs.end(),name)!=open_childs.end()) {
          child->setExpanded(true);
	    }
        if (indx+1<chld_count){
        //child = child->nextSibling();
            child = (*item_iter)->child(indx+1);
        } else{
            child = 0;
        }
	  }
	}
      } // for loop ends
    }

  private:
    MetaDataList *m_metaDataList;
    //std::vector<Q3ListViewItem *> m_itemList;
    std::vector<QTreeWidgetItem *> m_itemList;
  };


  void MetaDataList::updatedVariableLists()
  {
    CategoryUpdater updater(*this);
    forEach(m_metaDataList->topLevelItem(0), MetaDataItemBase::kMetaData,updater,false);
  }



}
//Q_DECLARE_METATYPE(PixCon::MetaDataItem)
