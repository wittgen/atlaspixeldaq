#include "DisablePanel.h"

//#include <TemporaryCursorChange.h>
#include <QTVisualiser/BusyLoopFactory.h>


namespace PixCon {


  DisablePanel::DisablePanel(const PixA::ConnectivityRef &conn, QWidget* parent)
    : QDialog(parent),
      m_conn(conn)
  {
    setupUi(this);
    if (m_conn) {
    nameBox->addItem("");
    {
      std::unique_ptr<PixA::IBusyLoop> busy_loop(PixA::BusyLoopFactory::busyLoop(" Get disable objects from db server CORAL ..."));
      PixA::ConnObjConfigDb config_db(PixA::ConnObjConfigDb::sharedInstance());
      config_db.getNames(m_conn.tag(PixA::IConnectivity::kId),m_conn.tag(PixA::IConnectivity::kConfig),PixA::IConnObjConfigDbManager::kDisableObjects, m_names);
    }
    for(std::set<std::string>::iterator it = m_names.begin(); it!=m_names.end(); it++) {
      if ((*it).find("Disable-S") == std::string::npos)
      nameBox->addItem((*it).c_str());
    }
    }
    if(nameBox->count()>0) nameSelected(0);
  }

  DisablePanel::~DisablePanel()
  {
  }

  void DisablePanel::nameSelected(int iName)
  {
    //    TemporaryCursorChange busy_cursor;
    revBox->clear();
    if (iName > 0 && m_conn){
      m_name = nameBox->itemText(iName).toLatin1().data();

      {
	std::string message(" Get revisions of ");
	message += m_name ;
	message += " objects from db server ...";
	std::unique_ptr<PixA::IBusyLoop> busy_loop(PixA::BusyLoopFactory::busyLoop(message));
	PixA::ConnObjConfigDb config_db(PixA::ConnObjConfigDb::sharedInstance());
	config_db.getRevisions(m_conn.tag(PixA::IConnectivity::kId),m_conn.tag(PixA::IConnectivity::kConfig),m_name, m_revisions);
      }

      m_itemRevision.clear();
      if (!m_revisions.empty()) {
	revBox->addItem("Most recent");
	m_itemRevision.push_back(0);
      }

      for(std::set<unsigned int>::reverse_iterator it = m_revisions.rbegin(); it!=m_revisions.rend(); it++) {

	time_t a_time = *it;
	if (a_time>0) {
	  revBox->addItem( (makeTimeString(a_time)+" (UTC)").c_str() );
	  m_itemRevision.push_back(a_time);
	}

      }
      revSelected(0);
    }
  }

  void DisablePanel::revSelected(int iRev)
  {
    m_revision=0;
    //    TemporaryCursorChange busy_cursor;
    if (!m_revisions.empty())  {

      // most recent revision is equivalent to second entry
      if (iRev==0) {
	iRev=1;
      }
      if (static_cast<unsigned int>(iRev) <= m_itemRevision.size()) {
	m_revision=m_itemRevision[iRev];
      }
    }
  }

  std::shared_ptr<const PixLib::PixDisable> DisablePanel::getDisable() {
    bool load_disable=false;
    if (!m_disable.get() &&  m_conn) {
      try {
	load_disable=true;
	std::stringstream message;
	message << "Load disable " << m_name
		<< m_conn.tag(PixA::IConnectivity::kId) << ":" << m_conn.tag(PixA::IConnectivity::kConfig) << ";" << m_revision << "...";
	std::cout << "INFO [DisablePanel::getDisable] " << message.str() << std::endl;
	std::unique_ptr<PixA::IBusyLoop> busy_loop(PixA::BusyLoopFactory::busyLoop(message.str()));

	PixA::ConnObjConfigDb config_db(PixA::ConnObjConfigDb::sharedInstance());
	PixA::ConnObjConfigDbTagRef config_tag(config_db.getTag(m_conn.tag(PixA::IConnectivity::kId),m_conn.tag(PixA::IConnectivity::kConfig),m_revision));
	m_disable = config_tag.disablePtr(m_name);
      }
      catch (...) {
      }
    }
    if (!m_disable.get() && load_disable) {
      std::cerr << "ERROR [DisablePanel::getDisable] Failed to load disable " <<  m_name;
      if (m_conn) {
        std::cerr << m_conn.tag(PixA::IConnectivity::kId) << ":" << m_conn.tag(PixA::IConnectivity::kConfig) << ";" << m_revision;
      }
      std::cerr << "." <<std::endl;
    }
    return m_disable;
  }

  std::string DisablePanel::makeTimeString(time_t a_time)
  {
    std::string a_time_string = asctime( gmtime(&a_time) );
    std::string::size_type ret_pos = a_time_string.find("\r");
    std::string::size_type new_line_pos = a_time_string.find("\n");
    if (new_line_pos == std::string::npos || (new_line_pos>ret_pos && ret_pos != std::string::npos)) {
      new_line_pos =ret_pos;
    }
    if (new_line_pos != std::string::npos ) {
      a_time_string.erase(new_line_pos, a_time_string.size()-new_line_pos);
    }
    return a_time_string;
  }

}
