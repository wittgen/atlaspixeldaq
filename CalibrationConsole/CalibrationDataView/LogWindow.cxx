#include "LogWindow.h"

#include <qthread.h>
#include <qapplication.h>
#include <qtimer.h>
#include <qpainter.h>
#include <QDateTime>
#include <qfileinfo.h>
#include <QDir>
#include <QList>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <unistd.h>
#include "Flag.h"
#include <QDebug>
#include <cstdlib>

#include "MessageDetails.h"

#include "timeUtil.h"

namespace PixCon {


  //  const char *Message_t::s_levelName[kNLevels]={"DIAGNOSTIC", "INFORMATION", "WARNING", "ERROR", "FATAL"};
  const char *Message_t::s_levelName[kNLevels]={"DEBUG", "CALIB", "INFO", "WARN", "ERROR", "FATAL"};

  class QNewMessageEvent : public QEvent
  {
  public:
    QNewMessageEvent()
      : QEvent(QEvent::User)
    {}

  };

  class SCounter
  {
  public:
    SCounter() : m_val(0) {}
    SCounter &operator++() { m_val++; return *this;}
    SCounter &operator++(int) { m_val++; return *this;}
    SCounter &operator--() { m_val--; return *this;}
    SCounter &operator--(int) { m_val--; return *this;}
    bool operator==(unsigned int a) {return m_val==a;}
    bool operator>(unsigned int a) {return m_val>a;}

    unsigned int &val() { return m_val; }
    const unsigned int &val() const { return m_val; }
  private:
    unsigned int m_val;
  };

  struct LWFData_t {
    LWFData_t() : m_theForwarder(NULL) {}
    
    SCounter m_putLineCounter;
    SCounter m_nLogWindowForwarder;
    SCounter m_invalidCalls;
    SCounter m_shutdown;
    SCounter m_shutdownTotal;
    SCounter m_qthreads;
    SCounter m_nLogWindow;
    SCounter m_exit;
    SCounter m_exitWait;
    const StdStreamListener *m_theForwarder;
  } s_lwfData;

  std::ostream &operator<<(std::ostream &out, SCounter &a) {
    out << a.val();
    return out;
  }

  std::string timeString(const ::time_t &a_time, bool time_only) {
    struct tm the_time;
    localtime_r(&a_time,&the_time);
    std::stringstream time_string;
    time_string	<< std::setfill('0');

    if (!time_only) {
      time_string << std::setw(4) << the_time.tm_year+1900
		  << "-" << std::setw(2) << the_time.tm_mon+1
		  << "-" << std::setw(2) << the_time.tm_mday
		  << " ";
    }
    time_string << std::setw(2) << the_time.tm_hour
		<< ":" << std::setw(2) << the_time.tm_min
		<< ":" << std::setw(2) << the_time.tm_sec;
    return time_string.str();
  }


  class MyQThread : public QThread
  {
  public:
    MyQThread() { s_lwfData.m_qthreads++; }
    ~MyQThread() { s_lwfData.m_qthreads--; }
  };

  class LogWindowForwarder : public StdStreamListener, public MyQThread
  {
  public:
    LogWindowForwarder(LogWindow &log_window, const std::string &log_file_name)
      : m_logWindow(&log_window),
	m_logOut(NULL),
	m_waiting(0),
	m_abort(false)
    {
      s_lwfData.m_nLogWindowForwarder++;
      s_lwfData.m_theForwarder=this;
      if (s_keyWords.empty()) {
	s_keyWords.push_back(std::make_pair(std::string("ERROR"),Message_t::kError));
	s_keyWords.push_back(std::make_pair(std::string("WARNING"),Message_t::kWarning));
	s_keyWords.push_back(std::make_pair(std::string("INFORMATION"),Message_t::kInformation));
	s_keyWords.push_back(std::make_pair(std::string("INFO"),Message_t::kInformation));
	s_keyWords.push_back(std::make_pair(std::string("Info"),Message_t::kInformation));
	s_keyWords.push_back(std::make_pair(std::string("FATAL"),Message_t::kFatal));
	s_keyWords.push_back(std::make_pair(std::string("Error"),Message_t::kError));
	s_keyWords.push_back(std::make_pair(std::string("error"),Message_t::kError));
	s_keyWords.push_back(std::make_pair(std::string("Warning"),Message_t::kWarning));
	s_keyWords.push_back(std::make_pair(std::string("Debug"),Message_t::kDiagnostic));
	s_keyWords.push_back(std::make_pair(std::string("DEBUG"),Message_t::kDiagnostic));
	s_keyWords.push_back(std::make_pair(std::string("Calib"),Message_t::kCalibration));
	s_keyWords.push_back(std::make_pair(std::string("CALIB"),Message_t::kCalibration));
      }
      if (!log_file_name.empty()) {
	m_logOut=new std::ofstream(log_file_name.c_str());
      }
      start();
    }

    ~LogWindowForwarder() {
      s_lwfData.m_shutdown++;
      s_lwfData.m_nLogWindowForwarder--;

      shutdownWait();
      {
	Lock lock(m_messageListMutex);
	m_messageList.clear();
      }
      delete m_logOut;
      m_logOut=NULL;

      s_lwfData.m_shutdown--;
      s_lwfData.m_shutdownTotal++;
    }

    void shutdown() {
      m_abort=true;
      m_newMessage.setFlag();
    }

    void shutdownWait() {
      shutdown();

      if (isRunning()) {
	s_lwfData.m_exitWait++;
	m_exit.wait();

    while (isRunning()) {
	  struct timespec wait_time;
	  wait_time.tv_sec = 0;
	  wait_time.tv_nsec = 100*1000000;
      while (isRunning()) {
	    nanosleep(&wait_time, NULL);
	  }

	}
      }
    }

  protected:

    bool hasMessages() {
      Lock lock(m_messageListMutex);
      return !m_messageList.empty() ;
    }

    void run() {      

      // forward loop
      while (!m_abort) {
	m_newMessage.wait(m_abort);
	if (m_abort) break;

	{
	  Lock lock(m_messageListMutex);
	  if (m_messageList.empty()) continue;
	}

	while (hasMessages() && !m_abort) {

	  {	  
	    Lock lock(m_messageListMutex);
	    sendMessage(m_messageList.back());
	    m_messageList.pop_back();
	  }
	}
      }

      if (m_waiting>0) {
	struct timespec wait_time;
	wait_time.tv_sec = 0;
	wait_time.tv_nsec = 100*1000000;
	while (m_waiting>0) {
	  nanosleep(&wait_time, NULL);
	}
      }
      s_lwfData.m_exitWait++;
      m_exit.setFlag();
    }

    class AliveCounter
    {
    public:
      AliveCounter(unsigned int &counter) : m_counter(&counter) { ++(*m_counter); }
      AliveCounter(SCounter &counter) : m_counter(&(counter.val())) { ++(*m_counter); }
      ~AliveCounter() { --(*m_counter); }
    private:
      unsigned int *m_counter;
    };

    void putLine(const std::string &line) {
      AliveCounter alive2(s_lwfData.m_putLineCounter);
      AliveCounter alive(m_waiting);

      if (s_lwfData.m_nLogWindowForwarder==0) {
	s_lwfData.m_invalidCalls++;
      }

      if (!m_abort) {
	Lock lock(m_messageListMutex);
	if (m_abort) return;

	assert(s_lwfData.m_nLogWindowForwarder>0);

	m_messageList.push_front(std::make_pair(::time(NULL), line));
	m_newMessage.setFlag();
      }
    }

    void sendMessage(const std::pair<time_t, std::string> &message) {

      if (m_logOut) {
	(*m_logOut) << timeString(message.first) << " " << message.second << std::endl;
      }

      Message_t::ELevel level = Message_t::kDiagnostic;
      std::string::size_type pos = 0;
      std::string::size_type method_start = std::string::npos;
      std::string::size_type method_end = std::string::npos;
      std::vector<std::pair<std::string,Message_t::ELevel> >::const_iterator keyword_iter1 = s_keyWords.begin();
      for (;
	   keyword_iter1 != s_keyWords.end();
	   keyword_iter1++) {
	if (message.second.compare(0,keyword_iter1->first.size(), keyword_iter1->first)==0) {
	  level = keyword_iter1->second;
	  pos = keyword_iter1->first.size();
	  break;
	}
      }

      if (keyword_iter1 == s_keyWords.end() ) {
	if (!message.second.empty() && isalpha(message.second[0])) {
	  pos=0;
	  unsigned int n_words=0;
	  do {
	    while (pos<message.second.size() && isalpha(message.second[pos])) pos++;
	    n_words++;
	    pos++;
	  } while (pos<message.second.size() && message.second[pos-1]=='/');
	  pos--;
	  if (n_words>1 && pos<message.second.size() && isspace(message.second[pos])) {
	    method_start = 0;
	    method_end = pos;
	    while (pos<message.second.size() && isspace(message.second[pos])) pos++;
	  }
	}

	for (std::vector<std::pair<std::string,Message_t::ELevel> >::const_iterator keyword_iter2 = s_keyWords.begin();
	     keyword_iter2 != s_keyWords.end();
	     keyword_iter2++) {
	  if (message.second.find( keyword_iter2->first, pos) != std::string::npos) {
	    level = keyword_iter2->second;
	  }
	}
      }
      else {
	/*const char *ptr =*/ message.second.c_str();
	while (isspace(message.second[pos])) pos++;
	if (message.second[pos]=='[') {
	  pos++;
	  method_start = pos;
	  while (pos<message.second.size() && message.second[pos]!=']') pos++;
	  if (pos<message.second.size()) {
	    pos++;
	  }
	  method_end =pos-1;
	}
      }
      if (method_start == std::string::npos || method_end == std::string::npos){
	method_end=0;
	method_start=0;
      }
	 //   std::cout << "FATAL [sendMessage] sending, level:" << level << " log window: " << m_logWindow->m_verbosityLevel << "\n";
      m_logWindow->m_allMsgCount[level]++;
      if(level >= m_logWindow->m_verbosityLevel) {
        m_logWindow->addMessage(Message_t(level,
	  				message.second.substr(pos,message.second.size()-pos),
					message.second.substr(method_start,method_end-method_start),
					message.first ));
        } else {
           m_logWindow->m_hidMsgCount[level]++;
	}
      }

  private:
    LogWindow     *m_logWindow;
    std::ofstream *m_logOut;
    Mutex m_messageListMutex;

    std::list< std::pair<time_t,std::string> > m_messageList;
    Flag m_newMessage;
    Flag m_exit;
    unsigned int m_waiting;
    bool m_abort;
    static std::vector<std::pair<std::string, Message_t::ELevel> > s_keyWords;
  };

  std::vector<std::pair<std::string, Message_t::ELevel> > LogWindowForwarder::s_keyWords;

  class SeverityTableItem : public QTableWidgetItem
  {
  public:
    SeverityTableItem( Message_t::ELevel level )
      : QTableWidgetItem(Message_t::levelName(level)),
	m_level( level < Message_t::kNLevels ? level : Message_t::kDiagnostic) 
    { paint();}

    void paint();

  private:
    static QColor s_levelColor[Message_t::kNLevels];
    Message_t::ELevel m_level;
  };


  class TimeItem : public QTableWidgetItem
  {
  public:
    TimeItem(time_t time )
      : QTableWidgetItem( timeString(time, true).c_str() ),
	m_time(time) 
    { }

    const time_t &time() const { return m_time; }

  private:
    time_t m_time;
  };
  
  QColor SeverityTableItem::s_levelColor[Message_t::kNLevels]={
    QColor("darkGrey"),
    QColor("blue"),
    QColor("green"),
    QColor("orange"),
    QColor("red"),
    QColor("magenta"),
  };

  void SeverityTableItem::paint()
  {
    assert(m_level < Message_t::kNLevels );
    QTableWidgetItem::setTextColor(s_levelColor[m_level]);
  }


  LogWindow::LogWindow(unsigned int messages_max, const std::string &log_file_name, QApplication *application, QWidget *parent)
    : QWidget(parent),
      m_verbosityLevel(Message_t::kInformation),
      m_listener(new LogWindowForwarder( *this, log_file_name )),
      m_application(application),
      m_timer(new QTimer(this)),
      m_messageCounter(0),
      m_messagesMax(messages_max)
   	{

    auto initInDebugMode = std::getenv("PIX_CONSOLE_DEBUG");
    if(initInDebugMode) {
      m_verbosityLevel = Message_t::kDiagnostic;
    }
    m_mainLogLayout = new QVBoxLayout(this);
    m_mainLogLayout->setContentsMargins(1,1,1,1);
    m_logTable = new QTableWidget(0,5,parent);
    m_logTable->setHorizontalHeaderItem(0, new QTableWidgetItem("ID"));
    m_logTable->setHorizontalHeaderItem(1, new QTableWidgetItem("Level"));
    m_logTable->setHorizontalHeaderItem(2, new QTableWidgetItem("Time"));
    m_logTable->setHorizontalHeaderItem(3, new QTableWidgetItem("Location"));
    m_logTable->setHorizontalHeaderItem(4, new QTableWidgetItem("Message"));

    m_logTable->setSortingEnabled(false);
    m_logTable->verticalHeader()->hide();
    m_logTable->horizontalHeader()->setStretchLastSection(true);
    m_logTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
  
    m_mainLogLayout->addWidget(m_logTable);

    m_botBarLayout = new QHBoxLayout();
    m_botBarLayout->setContentsMargins(1,1,1,1);

 
    m_verbosityLabel = new QLabel("Verbosity level: ");
    m_verbosityLabel->setFixedWidth(60);
 
    m_numberMessagesLabel = new QLabel(std::string("Number of messages: ").c_str());
    m_logVerbosityComboBox = new QComboBox();
    m_logVerbosityComboBox->addItem("FATAL");
    m_logVerbosityComboBox->addItem("ERROR");
    m_logVerbosityComboBox->addItem("WARN");
    m_logVerbosityComboBox->addItem("INFO");
    m_logVerbosityComboBox->addItem("CALIB");
    m_logVerbosityComboBox->addItem("DEBUG");
    if(m_verbosityLevel == Message_t::kDiagnostic) {
      m_logVerbosityComboBox->setCurrentIndex(5);
    } else {
      m_logVerbosityComboBox->setCurrentIndex(3);
    }
    m_logVerbosityComboBox->setFixedWidth(80);

   if(log_file_name.empty()) {
	  m_logFileNameLabel = new QLabel(std::string("No log file, set $PIX_LOGS to the output path to enable").c_str());
   } else {  
          m_logFileNameLabel = new QLabel(("Log file: "+log_file_name).c_str());
   }
    m_deleteLogsButton = new QPushButton("Clear Logs");
    m_deleteLogsButton->setFixedWidth(80);

    m_botBarLayout->addWidget(m_verbosityLabel); 
    m_botBarLayout->addWidget(m_logVerbosityComboBox); 
    m_botBarLayout->addWidget(m_numberMessagesLabel);
    m_botBarLayout->addWidget(m_logFileNameLabel);
    m_botBarLayout->addWidget(m_deleteLogsButton);

    m_mainLogLayout->addLayout(m_botBarLayout);

    m_allMsgCount.fill(0);
    m_hidMsgCount.fill(0);

    s_lwfData.m_nLogWindow++;

    QObject::connect(m_logVerbosityComboBox, &QComboBox::currentTextChanged, this, &LogWindow::changeVerbosityLevel);
    QObject::connect(m_deleteLogsButton, &QPushButton::clicked, [=](){m_logTable->setRowCount(0);});

    {
      const QFontMetrics font_metrics( fontMetrics());

      QString temp("0000");
      m_logTable->setColumnWidth( 0, font_metrics.width( temp )+10 ); // ID

      temp=Message_t::levelName(Message_t::kError);
      m_logTable->setColumnWidth( 1, font_metrics.width( temp )+10 ); // ID

      time_t current_time = time(NULL);
      temp=QString::fromStdString(timeString( current_time,true ));
      m_logTable->setColumnWidth( 2, font_metrics.width( temp )+10 );

      temp="RootFileHistogramProvider::requestHistogramList";
      m_logTable->setColumnWidth( 3, font_metrics.width( temp )+5 );
    }

    m_redirector.reserve(3);
    m_redirector.push_back(new StdStreamRedirector(std::cout, m_listener.get()) );
    m_redirector.push_back(new StdStreamRedirector(std::cerr, m_listener.get()) );
    m_redirector.push_back(new StdStreamRedirector(std::clog, m_listener.get()) );
    connect(m_timer,SIGNAL( timeout() ),this,SLOT( updateMessageView() ) );
    connect(m_logTable, SIGNAL(cellDoubleClicked(int,int)), this, SLOT(showDetails(int,int)));
    if (m_verbosityLevel == Message_t::kInformation) {
      std::cout << "INFO [LogWindow::ctor] ConsoleApp started with INFO verbosity. To enable DEBUG during start-up, set PIX_CONSOLE_DEBUG to 1 in your enviroment and restart.\n";
   }
  }

  LogWindow::~LogWindow() {
    shutdown();
    s_lwfData.m_nLogWindow--;
  }

  void LogWindow::removeOldLogFiles(const std::string &name_pattern, unsigned int keep_n_days) {
    // cleanup
    try {
      std::string::size_type pos = name_pattern.rfind("/");
      if (pos != std::string::npos) {
    std::cout << "INFO [LogWindow::removeOldLogFiles] remove all files in "
          << name_pattern.substr(0,pos).c_str()
          << " matching " << name_pattern.substr(pos+1,name_pattern.size()-pos-1)
          << " which are older than " << keep_n_days << " days."
          << std::endl;

    std::unique_ptr<QDir> log_file_dir(new QDir( name_pattern.substr(0,pos).c_str(), name_pattern.substr(pos+1,name_pattern.size()-pos-1).c_str(), QDir::Unsorted ));
    QDateTime keep_time_threshold = QDateTime::currentDateTime();
    keep_time_threshold = keep_time_threshold.addDays(-keep_n_days);

    QStringList namefilter;
    namefilter<<name_pattern.substr(pos+1,name_pattern.size()-pos-1).c_str();
    const QList<QFileInfo> *file_info_list = new QList<QFileInfo>( log_file_dir->entryInfoList(namefilter,QDir::Files)) ;
    unsigned int removed=0;
    uid_t uid = getuid();
    if (file_info_list) {
      QListIterator<QFileInfo> file_iter (  *file_info_list );
      while ( file_iter.hasNext() ) {
        const QFileInfo &info = file_iter.next();
        if (uid == info.ownerId()) {
          if (info.lastModified()<keep_time_threshold ) {
        // 	      std::cout << "INFO [LogWindow::removeOldLogFiles] will remove " << info->fileName()
        // 			<< timeString(info.lastModified().toTime_t()) << " < " << timeString(keep_time_threshold.toTime_t())
        // 			<< std::endl;

        removed++;
        log_file_dir->remove(info.fileName());
          }
          // 	    else {
          // 	      std::cout << "INFO [LogWindow::removeOldLogFiles] Will keep " << info.fileName() << std::endl;
          // 	    }
        }
      }
    }
    std::cout << "INFO [LogWindow::removeOldLogFiles] removed " << removed << " file(s) matching " << name_pattern << "." << std::endl;
      }
    }
    catch(...) {
      std::cout << "WARNING [LogWindow::removeOldLogFiles] caught exception when tryinh to removed log files which  match " << name_pattern << "." << std::endl;
    }

  }

  bool LogWindow::event(QEvent *an_event) {
    if (an_event && an_event->type() == QEvent::User) {
      QNewMessageEvent *message_event = dynamic_cast<QNewMessageEvent *>(an_event);
      if (message_event) {
    if (!m_timer->isActive()) {
      m_timer->start(300);
    }
    return true;
      }
    }
    return QWidget::event(an_event);
  }

  void LogWindow::addMessage( const Message_t &message ) {
    {
    Lock lock(m_messageListMutex);
    if (!m_messageList.empty() && m_messageList.front()==message) {
      m_messageList.front().repeat(1);
      return;
    }

    std::list<Message_t>::const_iterator message_iter = m_messageList.begin();
    std::list<Message_t>::const_iterator message_iter_1 = message_iter++;
    std::list<Message_t>::const_iterator message_iter_2 = message_iter++;

    if (m_messageList.size()>3 && *message_iter_2==message && *message_iter==*message_iter_1) {
      m_messageList.pop_front();
      m_messageList.front().repeat(2);
      return;
    }
    m_messageList.push_front(message);
    }
    m_application->postEvent(this, new QNewMessageEvent);
  }



  void LogWindow::updateMessageView() {
    Lock lock(m_messageListMutex);

    std::string fat_mes = std::to_string(m_hidMsgCount[Message_t::kFatal])+"/"+std::to_string(m_allMsgCount[Message_t::kFatal]);
    std::string err_mes = std::to_string(m_hidMsgCount[Message_t::kError])+"/"+std::to_string(m_allMsgCount[Message_t::kError]);
    std::string war_mes = std::to_string(m_hidMsgCount[Message_t::kWarning])+"/"+std::to_string(m_allMsgCount[Message_t::kWarning]);
    std::string inf_mes = std::to_string(m_hidMsgCount[Message_t::kInformation])+"/"+std::to_string(m_allMsgCount[Message_t::kInformation]);
    std::string cal_mes = std::to_string(m_hidMsgCount[Message_t::kCalibration])+"/"+std::to_string(m_allMsgCount[Message_t::kCalibration]);
    std::string deb_mes = std::to_string(m_hidMsgCount[Message_t::kDiagnostic])+"/"+std::to_string(m_allMsgCount[Message_t::kDiagnostic]);
    std::string box_message = "Messages (hidden/total): <b>F:</b> "+fat_mes+" <b>E:</b> "+err_mes+" <b>W:</b> "+war_mes+" <b>I:</b> "+inf_mes+" <b>C:</b> "+cal_mes+" <b>D:</b> "+deb_mes;
    m_numberMessagesLabel->setText(box_message.c_str());

    if (m_messageList.empty()) return;
    unsigned int n_repetitions=0;
    if (/*isHidden() || */ m_messageList.size()> 0 /*m_messagesMax*/) {
      std::list<Message_t>::reverse_iterator most_severe_error = m_messageList.rbegin();
      unsigned int id=m_messageCounter;
      //unsigned int most_severe_error_id=id;
      for(std::list<Message_t>::reverse_iterator message_iter = m_messageList.rbegin();
	  message_iter != m_messageList.rend();
	  ++message_iter) {
	id++;
	if ( message_iter->repetitions()) {
	  id += message_iter->repetitions();
	  n_repetitions++;
	}
	if (message_iter->level()>=Message_t::kWarning && (message_iter->level()>=most_severe_error->level() )) {
	  most_severe_error = message_iter;
	  //most_severe_error_id = id;
          //if (message_iter->level()>= Message_t::kNLevels-1) break;
	}
      }
      if (most_severe_error->level()>=Message_t::kWarning) {
	std::stringstream message;
	message << std::setw(5) << id
		<< " " << timeString(most_severe_error->time())
		<< " " << Message_t::levelName( most_severe_error->level() )
		<< " " << most_severe_error->location()
		<< " " << most_severe_error->message();
	QString temp(message.str().c_str());
	emit hiddenMessage(temp);
      }
    }

    if (m_messageList.size()+n_repetitions>m_messagesMax) {
      //      m_messageCounter+= m_messageList.size()-m_messagesMax;
      while (m_messageList.size()+n_repetitions>m_messagesMax) {
	m_messageCounter++;
	if (m_messageList.back().repetitions()>0) {
	  m_messageCounter+= m_messageList.back().repetitions();
	  n_repetitions--;
	}

	m_messageList.pop_back();
      }
    }

   if (m_logTable->rowCount()+m_messageList.size()+n_repetitions>m_messagesMax) {
     QVector<int> delete_row_list(m_logTable->rowCount()+m_messageList.size()+n_repetitions-m_messagesMax);
     for (int elm_i=0; elm_i<delete_row_list.size(); elm_i++) {
         delete_row_list[elm_i] = m_logTable->rowCount()-delete_row_list.size()+elm_i;
     }
     for (int row_remover = 0; row_remover < delete_row_list.size(); row_remover++){
         m_logTable->removeRow(row_remover);
     }
   }
   if (!isHidden()) {
       for (unsigned int row_counter = 0; row_counter < m_messageList.size()+n_repetitions;row_counter++){
           m_logTable->insertRow(0);
       }
     unsigned int row_i=m_messageList.size()+n_repetitions;
     while (!m_messageList.empty()) {
   m_messageCounter++;
   unsigned int repetitions=m_messageList.back().repetitions();
   bool added_row=false;
   if (row_i<=1) {
     unsigned int need_rows =1;
     if (repetitions>0 ) need_rows++;
     if (row_i<need_rows) {
      // insertRows(0,need_rows-row_i);
         m_logTable->insertRow(0);
       added_row=true;
     }
   }
   row_i--;
   std::stringstream message_id_1;
   message_id_1 << m_messageCounter;
   if (added_row) {
     message_id_1 << "*";
   }
   unsigned int col_i_1=0;
   m_logTable->setItem(row_i,col_i_1++, new QTableWidgetItem(message_id_1.str().c_str()));
   m_logTable->setItem(row_i,col_i_1++, new SeverityTableItem( m_messageList.back().level() ));
   m_logTable->setItem(row_i,col_i_1++, new TimeItem( m_messageList.back().time()));
   m_logTable->setItem(row_i,col_i_1++, new QTableWidgetItem(m_messageList.back().location().c_str()));
   m_logTable->setItem(row_i,col_i_1++, new QTableWidgetItem(m_messageList.back().message().c_str()));
   if (repetitions>0) {
     m_messageCounter+=repetitions;
     std::stringstream message_id_2;
     message_id_2 << m_messageCounter;
     unsigned int col_i_2=0;
     row_i--;
     m_logTable->setItem(row_i,col_i_2++, new QTableWidgetItem(message_id_2.str().c_str()));
     m_logTable->setItem(row_i,col_i_2++, new SeverityTableItem( Message_t::kDiagnostic ));
     m_logTable->setItem(row_i,col_i_2++, new TimeItem(m_messageList.back().time()));
     m_logTable->setItem(row_i,col_i_2++, new QTableWidgetItem(m_messageList.back().location().c_str()));
     std::stringstream the_message;
     the_message << "Last ";
     if (m_messageList.back().repeatedNMessages()==1) {
       the_message << "message was";
     }
     else {
       the_message << static_cast<unsigned int>(m_messageList.back().repeatedNMessages()) << " messages were";
     }
     the_message << " repeated " << m_messageList.back().repetitions() << " times.";
      m_logTable->setItem(row_i,col_i_2++, new QTableWidgetItem(the_message.str().c_str()));
   }
   m_messageList.pop_back();
     }
   }
  }

  void LogWindow::shutdown() {
    // Unfortunately, it is rather difficult to revert the stream redirection
    // There may be still threads which use the redirected threads. It its
    // impossibile when everybody stopped using the re-directed buffers.
    // So there is good chance that this will fail:

    //    std::cout << "DIAGNOSTIC [LogWindow::shutdown] shutdown." << std::endl;
    for (std::vector<StdStreamRedirector *>::iterator redirector_iter = m_redirector.begin();
	 redirector_iter != m_redirector.end();
	 ++redirector_iter) {
      (*redirector_iter)->restoreStream();
    }

    // give other threads (if there are any) some time to use the re-re-directed streams
    // hopefully .1 sec is sufficient
    struct timespec wait_time;
    wait_time.tv_sec = 0;
    wait_time.tv_nsec = 300*1000000;
    nanosleep(&wait_time, NULL);

    // lets try :
    std::cout << "DIAGNOSTIC [LogWindow::shutdown] restored streams. Now delete the unused buffers." << std::endl;

    for (std::vector<StdStreamRedirector *>::iterator redirector_iter = m_redirector.begin();
	 redirector_iter != m_redirector.end();
	 ++redirector_iter) {
      delete *redirector_iter;
      *redirector_iter = NULL;
    }
    //    std::cout << "DIAGNOSTIC [LogWindow::shutdown] killed redirectors" << std::endl;
    m_redirector.clear();
    m_listener.reset();
    std::cout << "DIAGNOSTIC [LogWindow::shutdown] all done" << std::endl;
  }

  void  LogWindow::showDetails( int row, int /* col*/ ) {
    std::string time_string;
    QTableWidgetItem *an_item = m_logTable->item(row,2);
    if (an_item) {
      TimeItem *time_item =dynamic_cast<TimeItem *>(an_item);
      if (time_item) {
    time_string = timeString( time_item->time());
      }
    }
    new MessageDetails(
       m_logTable->item(row,0)->text(),//id_string
       m_logTable->item(row,1)->text(),//level
       (time_string.empty() ? m_logTable->item(row, 2)->text() : QString(time_string.c_str())), //time
       m_logTable->item(row,3)->text(),//method
       m_logTable->item(row,4)->text());//message
  }
}
