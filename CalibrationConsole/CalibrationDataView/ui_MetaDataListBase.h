#ifndef UI_METADATALISTBASE_H
#define UI_METADATALISTBASE_H

#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QDialog>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLineEdit>
#include <QPushButton>
#include <QSplitter>
#include <QVBoxLayout>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QGroupBox>

QT_BEGIN_NAMESPACE

class Ui_MetaDataListBase
{
public:
    QVBoxLayout *vboxLayout;
    QHBoxLayout *hboxLayout;
    QPushButton *m_clearFilterButton;
    QPushButton *m_filterButton;
    QLineEdit *m_filterString;
    QSplitter *m_splitter;
    //Q3ListView *m_metaDataList;
    QTreeWidget *m_metaDataList;
    QTreeWidgetItem *m_metaDataListHeaders;
    //Q3GroupBox *m_details;
    QGroupBox * m_details;
    QVBoxLayout *vboxLayout1;

    void setupUi(QDialog *MetaDataListBase)
    {
        if (MetaDataListBase->objectName().isEmpty())
            MetaDataListBase->setObjectName(QString::fromUtf8("MetaDataListBase"));
        MetaDataListBase->resize(594, 542);
        vboxLayout = new QVBoxLayout(MetaDataListBase);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        m_clearFilterButton = new QPushButton(MetaDataListBase);
        m_clearFilterButton->setObjectName(QString::fromUtf8("m_clearFilterButton"));
        m_clearFilterButton->setIcon(QIcon(QPixmap(":/icons/images/gtk-clear.png")));

        m_clearFilterButton->setAutoDefault(false);

        hboxLayout->addWidget(m_clearFilterButton);

        m_filterButton = new QPushButton(MetaDataListBase);
        m_filterButton->setObjectName(QString::fromUtf8("m_filterButton"));
        const QIcon icon = QIcon(QPixmap(":/icons/images/stock_autofilter.png"));
        m_filterButton->setIcon(icon);
        m_filterButton->setFlat(true);

        hboxLayout->addWidget(m_filterButton);

        m_filterString = new QLineEdit(MetaDataListBase);
        m_filterString->setObjectName(QString::fromUtf8("m_filterString"));
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(7), static_cast<QSizePolicy::Policy>(0));
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(m_filterString->sizePolicy().hasHeightForWidth());
        m_filterString->setSizePolicy(sizePolicy);
        m_filterString->setMaximumSize(QSize(32767, 32767));

        hboxLayout->addWidget(m_filterString);


        vboxLayout->addLayout(hboxLayout);

        m_splitter = new QSplitter(MetaDataListBase);
        m_splitter->setObjectName(QString::fromUtf8("m_splitter"));
        m_splitter->setOrientation(Qt::Vertical);
/*
        m_metaDataList = new Q3ListView(m_splitter);
        m_metaDataList->addColumn(QApplication::translate("MetaDataListBase", "Number", 0));
        m_metaDataList->header()->setClickEnabled(true, m_metaDataList->header()->count() - 1);
        m_metaDataList->header()->setResizeEnabled(true, m_metaDataList->header()->count() - 1);
        m_metaDataList->addColumn(QApplication::translate("MetaDataListBase", "Date", 0));
        m_metaDataList->header()->setClickEnabled(true, m_metaDataList->header()->count() - 1);
        m_metaDataList->header()->setResizeEnabled(true, m_metaDataList->header()->count() - 1);
        m_metaDataList->addColumn(QApplication::translate("MetaDataListBase", "Name", 0));
        m_metaDataList->header()->setClickEnabled(true, m_metaDataList->header()->count() - 1);
        m_metaDataList->header()->setResizeEnabled(true, m_metaDataList->header()->count() - 1);
        m_metaDataList->addColumn(QApplication::translate("MetaDataListBase", "Quality", 0));
        m_metaDataList->header()->setClickEnabled(true, m_metaDataList->header()->count() - 1);
        m_metaDataList->header()->setResizeEnabled(true, m_metaDataList->header()->count() - 1);
        m_metaDataList->addColumn(QApplication::translate("MetaDataListBase", "Status", 0));
        m_metaDataList->header()->setClickEnabled(true, m_metaDataList->header()->count() - 1);
        m_metaDataList->header()->setResizeEnabled(true, m_metaDataList->header()->count() - 1);
        m_metaDataList->addColumn(QApplication::translate("MetaDataListBase", "Comment", 0));
        m_metaDataList->header()->setClickEnabled(true, m_metaDataList->header()->count() - 1);
        m_metaDataList->header()->setResizeEnabled(true, m_metaDataList->header()->count() - 1);
        m_metaDataList->setObjectName(QString::fromUtf8("m_metaDataList"));
*/
        m_metaDataList = new QTreeWidget(m_splitter);
        m_metaDataList->setColumnCount(6);
#if QT_VERSION < 0x050000
        m_metaDataList->header()->setClickable(true);
        m_metaDataList->header()->setResizeMode(QHeaderView::ResizeToContents);
#else
	m_metaDataList->header()->setSectionsClickable(true);
        m_metaDataList->header()->setSectionResizeMode(QHeaderView::ResizeToContents);
#endif
        m_metaDataListHeaders = new QTreeWidgetItem;
        m_metaDataListHeaders->setText(0,"Number");
        m_metaDataListHeaders->setText(1,"Date");
        m_metaDataListHeaders->setText(2,"Name");
        m_metaDataListHeaders->setText(3,"Quality");
        m_metaDataListHeaders->setText(4,"Status");
        m_metaDataListHeaders->setText(5,"Comment");
        m_metaDataList->setHeaderItem(m_metaDataListHeaders);
        m_metaDataList->setObjectName(QString::fromUtf8("m_metaDataList"));

        m_splitter->addWidget(m_metaDataList);
        //m_details = new Q3GroupBox(m_splitter);
        m_details = new QGroupBox(m_splitter);
        m_details->setObjectName(QString::fromUtf8("m_details"));
        //m_details->setColumnLayout(0, Qt::Vertical);
	new QVBoxLayout(m_details);
        m_details->layout()->setSpacing(6);
        m_details->layout()->setContentsMargins(11, 11, 11, 11);
        vboxLayout1 = new QVBoxLayout();
        QBoxLayout *boxlayout = qobject_cast<QBoxLayout *>(m_details->layout());
        if (boxlayout)
            boxlayout->addLayout(vboxLayout1);
        vboxLayout1->setAlignment(Qt::AlignTop);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        m_splitter->addWidget(m_details);

        vboxLayout->addWidget(m_splitter);


        retranslateUi(MetaDataListBase);
        /*
        QObject::connect(m_metaDataList, SIGNAL(selectionChanged(Q3ListViewItem*)), MetaDataListBase, SLOT(select(Q3ListViewItem*)));
        QObject::connect(m_metaDataList, SIGNAL(doubleClicked(Q3ListViewItem*)), MetaDataListBase, SLOT(showHideVariables(Q3ListViewItem*)));
        QObject::connect(m_metaDataList, SIGNAL(spacePressed(Q3ListViewItem*)), MetaDataListBase, SLOT(open(Q3ListViewItem*)));
        QObject::connect(m_metaDataList, SIGNAL(mouseButtonClicked(int,Q3ListViewItem*,QPoint,int)), MetaDataListBase, SLOT(select(int,Q3ListViewItem*,QPoint,int)));
        QObject::connect(m_metaDataList, SIGNAL(returnPressed(Q3ListViewItem*)), MetaDataListBase, SLOT(select(Q3ListViewItem*)));*/
	/*
        QObject::connect(m_metaDataList, SIGNAL(doubleClicked(QTreeWidgetItem*)), MetaDataListBase, SLOT(showHideVariables(QTreeWidgetItem*)));
        QObject::connect(m_metaDataList, SIGNAL(spacePressed(QTreeWidgetItem*)), MetaDataListBase, SLOT(open(QTreeWidgetItem*)));
        QObject::connect(m_metaDataList, SIGNAL(mouseButtonClicked(int,QTreeWidgetItem*,QPoint,int)), MetaDataListBase, SLOT(select(int,QTreeWidgetItem*,QPoint,int)));
        QObject::connect(m_metaDataList, SIGNAL(returnPressed(QTreeWidgetItem*)), MetaDataListBase, SLOT(select(QTreeWidgetItem*)));
	*/
        QObject::connect(m_clearFilterButton, SIGNAL(clicked()), MetaDataListBase, SLOT(clearFilter()));
        QObject::connect(m_filterButton, SIGNAL(clicked()), MetaDataListBase, SLOT(changeFilter()));
        QObject::connect(m_filterString, SIGNAL(returnPressed()), MetaDataListBase, SLOT(changeFilter()));

        QMetaObject::connectSlotsByName(MetaDataListBase);
    } // setupUi

    void retranslateUi(QDialog *MetaDataListBase)
    {
        MetaDataListBase->setWindowTitle(QApplication::translate("MetaDataListBase", "Form1", 0));
#ifndef QT_NO_WHATSTHIS
        MetaDataListBase->setProperty("whatsThis", QVariant(QString()));
#endif // QT_NO_WHATSTHIS
        m_clearFilterButton->setText(QString());
#ifndef QT_NO_TOOLTIP
        m_clearFilterButton->setProperty("toolTip", QVariant(QApplication::translate("MetaDataListBase", "Clear the variable name filter.", 0)));
#endif // QT_NO_TOOLTIP
        m_filterButton->setText(QApplication::translate("MetaDataListBase", "Variables", 0));
#ifndef QT_NO_TOOLTIP
        m_filterButton->setProperty("toolTip", QVariant(QApplication::translate("MetaDataListBase", "Apply variable name filter.", 0)));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        m_filterString->setProperty("toolTip", QVariant(QApplication::translate("MetaDataListBase", "String for the variable name filter: multiple words are ORed, ! negates match", 0)));
#endif // QT_NO_TOOLTIP
       // m_metaDataList->header()->setLabel(0, QApplication::translate("MetaDataListBase", "Number", 0));
       // m_metaDataList->header()->setLabel(1, QApplication::translate("MetaDataListBase", "Date", 0));
       // m_metaDataList->header()->setLabel(2, QApplication::translate("MetaDataListBase", "Name", 0));
       // m_metaDataList->header()->setLabel(3, QApplication::translate("MetaDataListBase", "Quality", 0));
       // m_metaDataList->header()->setLabel(4, QApplication::translate("MetaDataListBase", "Status", 0));
       // m_metaDataList->header()->setLabel(5, QApplication::translate("MetaDataListBase", "Comment", 0));
        m_details->setTitle(QApplication::translate("MetaDataListBase", "Meta Data ", 0));
    } // retranslateUi

};

namespace Ui {
    class MetaDataListBase: public Ui_MetaDataListBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_METADATALISTBASE_H
