#ifndef _PixCon_Flag_hh_
#define _PixCon_Flag_hh_

#include <Lock.h>

namespace PixCon {

  class Flag
  {
  public:
    Flag() : m_condition(&m_mutex), m_flag(false) {}

    /** Wait until a signal arrives and the internal has changed.
     */
    void wait() {
      wait(m_flag);
    }

    /** Return the mutex to lock the flag.
     */
    Mutex &mutex() { return m_mutex; }

    /** Wait until a signal arrives and the internal or external flag has changed.
     */
    void wait(bool &abort) {
      Lock lock(m_mutex);
      while (!m_flag && !abort) {
	m_condition.wait();
      }

      m_flag=false;
    }

    /** wait until a signal arrives ant the internal or external flag has changed but at most the given amount of time.
     * @param timeout maximum waiting time in seconds.
     * @return 0 in case the time expired.
     */
    bool timedwait(unsigned long timeout) {
      return timedwait(timeout, m_flag);
    }

    /** wait until a signal arrives ant the internal or external flag has changed but at most the given amount of time.
     * @param timeout maximum waiting time in seconds.
     * @param abort reference of the external flag
     * @return 0 in case the time expired.
     */
    bool timedwait(unsigned long timeout, bool &abort) {
      Lock lock(m_mutex);
      while (!m_flag && !abort) {
	unsigned long abs_sec; 
	unsigned long abs_nsec;
	omni_thread::get_time(&abs_sec, &abs_nsec,timeout);
	if (!m_condition.timedwait(abs_sec,abs_nsec)) {
	  return false;
	}
      }

      m_flag=false;
      return true;
    }

    /** Change the state of the flag and send out a signal.
     */
    void setFlag() {
      m_flag=true;
      m_condition.signal();
    }

    /** Change the state of the flag and send out a signal to every listener.
     */
    void broadcast() {
      m_flag=true;
      m_condition.broadcast();
    }


    /** Reset the flag.
     */
    void clearFlag() {
      m_flag=false;
    }

    /** Return true if the flag is set.
     */
    bool isFlagged() const {return m_flag;}

  private:
    Mutex     m_mutex;
    Condition m_condition;
    bool      m_flag;
  };

}

#endif
