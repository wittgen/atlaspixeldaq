#ifndef _PixCon_ConnListItem_h_
#define _PixCon_ConnListItem_h_

#include <CalibrationDataTypes.h>
#include <QTreeWidget>

namespace PixCon {

  class ConnListItem : public QTreeWidgetItem
  {
  public:

    ConnListItem( QTreeWidget *list, const QStringList &connectivity_name, EConnItemType type)
      : QTreeWidgetItem(list, connectivity_name),
	m_type(type)
    {}

    ConnListItem( QTreeWidgetItem *parent, const QStringList &connectivity_name, EConnItemType type )
      : QTreeWidgetItem(parent, connectivity_name),
	m_type(type)
    {}

    ~ConnListItem() {}

    EConnItemType type() const {return m_type;}

  protected:
  private:
    EConnItemType m_type;
  };

}

#endif
