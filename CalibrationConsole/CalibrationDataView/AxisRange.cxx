#include "AxisRange.h"

//#include <q3whatsthis.h>

/*
 *  Constructs a AxisRangeBase as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
AxisRangeBase::AxisRangeBase( QWidget* parent ) : QDialog( parent )
{
    AxisRangeBaseLayout = new QVBoxLayout( this);
    AxisRangeBaseLayout->setMargin(11);
    AxisRangeBaseLayout->setSpacing(6);

    //layout1 = new QGridLayout( 0, 1, 1, 0, 6, "layout1");
    layout1 = new QGridLayout( this);
    layout1->setSpacing(6);

    textLabel1 = new QLabel( this );

    layout1->addWidget( textLabel1, 0, 0 );

    m_minEntry = new QLineEdit( this );

    layout1->addWidget( m_minEntry, 0, 1 );

    textLabel2 = new QLabel( this );

    layout1->addWidget( textLabel2, 1, 0 );

    m_maxEntry = new QLineEdit( this );

    layout1->addWidget( m_maxEntry, 1, 1 );
    AxisRangeBaseLayout->addLayout( layout1 );

    //layout2 = new Q3HBoxLayout( 0, 0, 6, "layout2");
    layout2 = new QHBoxLayout(this);
    layout2->setSpacing(6);

    m_cancelButton = new QPushButton( this );
    layout2->addWidget( m_cancelButton );

    m_acceptButton = new QPushButton( this );
    m_acceptButton->setDefault( true );
    layout2->addWidget( m_acceptButton );
    AxisRangeBaseLayout->addLayout( layout2 );
    languageChange();
    resize( QSize(237, 161).expandedTo(minimumSizeHint()) );
  //  clearWState( WState_Polished );

    // signals and slots connections
    connect( m_cancelButton, SIGNAL( clicked() ), this, SLOT( reject() ) );
    connect( m_acceptButton, SIGNAL( clicked() ), this, SLOT( accept() ) );
}

/*
 *  Destroys the object and frees any allocated resources
 */
AxisRangeBase::~AxisRangeBase()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void AxisRangeBase::languageChange()
{
    textLabel1->setText( tr( "Minimum :" ) );
    textLabel2->setText( tr( "Maximum :" ) );
    m_cancelButton->setText( tr( "Cancel" ) );
    m_acceptButton->setText( tr( "Ok" ) );
}

