#ifndef _PixCon_ConnItemSelection_h_
#define _PixCon_ConnItemSelection_h_

#include <memory>
#include <memory>

#include "ValueList.h"
#include "EvalResult_t.h"

namespace PixCon {

  class CalibrationDataManager;
  class DataSelection;
  class TokenDescription;
  class FilterParser;
  class EvaluatorFactory;

  class ConnItemSelection
  {
  public:
    //    ConnItemSelection();
    //    ConnItemSelection(CalibrationData &calibration_data, const std::string &selection_pattern);
    ConnItemSelection(const std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data_manager,
		      const std::shared_ptr<PixCon::DataSelection> &data_selection);
    ~ConnItemSelection();

    /** Set a filter string.
     *@param selection_pattern comparisons of variables with values or state names connected with logical operators (C++-operators).
     *@throw PixCon::LogicParserBase::ParseError in case of errors in the filter string.
     * The selection pattern can comprise several terms connected by C logical operators, where terms
     * could be comparisons between variables and values or state names or connectivity name patterns.
     */
    void setFilterString(const std::string &selection_pattern);

    EvalResult_t isSelected(const std::string &connectivity_name);

    std::vector< ValueDescription_t >::const_iterator beginVar() { return m_conditionVars.begin(); }
    std::vector< ValueDescription_t >::const_iterator endVar()   { return m_conditionVars.end(); }

    /** Return true if the selction uses the specified variable.
     */
    bool usesVariable(EMeasurementType measurement_type, SerialNumber_t serial_number, VarId_t var_id) const {
      ValueListKey_t key(var_id, measurement_type, serial_number);
      ValueListKey_t current_key(var_id, kNMeasurements, 0);
      return m_valueList.find(key) != m_valueList.end() || m_stateList.find(key) != m_stateList.end()
	|| m_valueList.find(current_key) != m_valueList.end() || m_stateList.find(current_key) != m_stateList.end();
    }

    const PixCon::ValueList<float> &valueList() const           { return m_valueList; }
    const PixCon::ValueList<PixCon::State_t> &stateList() const { return m_stateList; }

  protected:
  private:
    std::vector<ValueDescription_t>                   m_conditionVars;
    std::shared_ptr<PixCon::CalibrationDataManager> m_calibrationDataManager;
    std::shared_ptr<PixCon::DataSelection>          m_dataSelection;
    PixCon::ValueList<float>                          m_valueList;
    PixCon::ValueList<PixCon::State_t>                m_stateList;
    PixCon::EvaluationParameter                       m_evaluationParameter;
    std::unique_ptr<PixCon::EvaluatorFactory>           m_factory;
    std::unique_ptr<PixCon::FilterParser>               m_filterExpressionTree;

    static bool s_initialised;
    static PixCon::TokenDescription     *s_tokenDescription;
  };

}
#endif
