

#include "UserMode.h"

namespace PixCon {

  UserMode::UserMode(const EUserMode& user_mode)
  {
    setUserMode(user_mode);
  }
  
  UserMode::~UserMode() {}

  UserMode::EUserMode UserMode::getUserMode()
  {
    return m_userMode;
  }

  void UserMode::setUserMode(const EUserMode& user_mode)
  {
    m_userMode = user_mode;
  }

}
