#ifndef _MessageDetails_H_
#define _MessageDetails_H_

#include "ui_MessageDetailsBase.h"
//Added by qt3to4:
#include <QCloseEvent>

namespace PixCon {

  class MessageDetails : public QDialog, public Ui_MessageDetailsBase
  {Q_OBJECT
  public:
    MessageDetails(const QString &id_string,
		   const QString &level_string,
		   const QString &time_string,
		   const QString &method_string,
		   const QString &message_string,
		   QWidget* parent = 0);

    ~MessageDetails();

    void closeEvent(QCloseEvent * event );
  };

}
#endif
