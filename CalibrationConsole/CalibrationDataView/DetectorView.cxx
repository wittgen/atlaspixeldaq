#include "CoupledDetectorView.h"
#include "DetectorCanvasView.h"
#include "DetectorViewWindow.h"
#include "Detector.h"
#include "FullDetectorModel.h"
#include "ToothPixModel.h"

#include <QLineEdit>
#include <QMessageBox>
//#include <QPrinter>
//#include <QPrintDialog>
#include <QMenu>
#include <QImageWriter>
#include <QFileDialog>

#include <ConfigWrapper/ConnectivityUtil.h>

#include <CalibrationDataItemFactory.h>
#include <CalibrationDataManager.h>
#include "DataSelectionSpecificContextMenu.h"

#include "PaletteItemFactory.h"
#include "AxisRange.h"


#include <DetectorCanvasTip.h>

#include <QIsInfoEvent.h>


#include "guessConnItemType.h"

#include "SummaryHistogramView.h"
#include "ISelection.h"

#include "timeUtil.h"
#include <iomanip>

std::string DetectorView::s_currentValueLabel("Present Value");

DetectorView::DetectorView(PixCon::DetectorViewList &detector_view_list,
			   bool setup_calibration_data,
			   std::shared_ptr<PixCon::IContextMenu> &context_menu,
			   const std::shared_ptr<PixDet::ILogoFactory> &logo_factory,
			   const std::shared_ptr<PixCon::SlowUpdateList> &slow_update_list,
			   QApplication &app,
			   QWidget* parent)
: QWidget(parent),
    m_detectorViewList(&detector_view_list),
    m_calibrationData(m_detectorViewList->calibrationDataManager()),
    m_contextMenu(context_menu),
    m_logoFactory(logo_factory),
    m_slowUpdateList(slow_update_list),
    m_redrawTimer(new QTimer(this)),
    m_update(false),
    m_zoomLevel(2),
    m_rotateLevel(0),
    m_app(&app),
    m_detectorModel(kFull)
{
  setupUi(this);

//   PixCon::CategoryList categories;
//   PixCon::CategoryList::Category dcs_cat=categories.addCategory("DCS");
//   PixCon::CategoryList::Category status_cat=categories.addCategory("Status");
//   PixCon::CategoryList::Category analysis_cat=categories.addCategory("Analysis");
  PixCon::VarDef_t enabled_var = PixCon::CalibrationDataStyle::createEnabledVarDef();
  m_dataSelection=std::make_shared<PixCon::DataSelection>(calibrationDataManager().calibrationDataPtr(),
										     enabled_var,
										     enabled_var);

  QVBoxLayout  *groupBox1Layout = new QVBoxLayout( );
  groupBox1Layout->setAlignment( Qt::AlignBottom );
  vboxLayout1->addLayout(groupBox1Layout);

  m_detectorView=new DetectorCanvasView(groupBox1,"DetectorViewCanvas");
  m_normalMatrix=m_detectorView->matrix();

  m_tip=std::make_unique<PixCon::DetectorCanvasTip>( m_detectorView );

  //  connect(m_detectorView, SIGNAL( itemSelected(const QString &) ), statusBar(), SLOT( showMessage(const QString &) ) );
  connect(m_detectorView, SIGNAL( itemSelected(const QString &, const QRectF &) ), this, SLOT( selectItem(const QString &, const QRectF &) ) );
  connect(m_detectorView, SIGNAL( mouseOver(const QString &, const QRectF &) ), this, SLOT( buildMessage(const QString &, const QRectF &) ) );

  connect(m_detectorView,SIGNAL(  axisRightClick() ), this, SLOT( showAxisRangeDialog() ) );
  connect(m_detectorView,SIGNAL( mouseOverAxisItem(PaletteAxisItem *, const QRectF&) ),
      this,        SLOT( buildMessage(PaletteAxisItem *, const QRectF &) ) );

  connect(this,SIGNAL( axisChangeRequest(float,float) ), this, SLOT( changeAxis(float, float) ) );
  connect(this,SIGNAL( axisChangeRequest(unsigned int) ), this, SLOT( cycleStates(unsigned int) ) );

  connect(m_detectorView, SIGNAL( primaryAction(PixCon::EConnItemType, const std::string &) ),
	  m_contextMenu.get(), SLOT( primaryAction(PixCon::EConnItemType, const std::string &)));

  connect(m_detectorView, SIGNAL( secondaryAction(PixCon::EConnItemType, const std::string &) ), 
	  m_contextMenu.get(), SLOT( secondaryAction(PixCon::EConnItemType, const std::string &)));

  connect(m_detectorView, SIGNAL( showMenu(PixCon::EConnItemType, const std::string &) ), 
	  m_contextMenu.get(), SLOT( showMenu(PixCon::EConnItemType, const std::string &)));

  // auto-scaling of combo boxes
  m_categorySelector->setSizeAdjustPolicy(QComboBox::AdjustToContents);
  m_serialNumberSelector->setSizeAdjustPolicy(QComboBox::AdjustToContents);
  m_variableSelector->setSizeAdjustPolicy(QComboBox::AdjustToContents);

  groupBox1Layout->addWidget( m_detectorView );
  drawDetectorModel();
  if (setup_calibration_data) {
    initCalibrationData();
  }
  update(false);
  zoomOut();
  connect(m_redrawTimer,SIGNAL( timeout() ),this,SLOT( updateView() ) );

  PixCon::DataSelectionSpecificContextMenu * specific_context_menu = dynamic_cast<PixCon::DataSelectionSpecificContextMenu *>( m_contextMenu.get() );
  if (specific_context_menu) {
    specific_context_menu->dataSelectionChanged(*m_dataSelection);
  }


}

void DetectorView::selectSerialNumber(PixCon::EMeasurementType measurement_type, PixCon::SerialNumber_t serial_number)
{
  QString  serial_number_string( makeSerialNumberString(measurement_type, serial_number).c_str() );
  for (unsigned int index_i=0; index_i < static_cast<unsigned int>(m_serialNumberSelector->count()); index_i++) {
    if ( serial_number_string == m_serialNumberSelector->itemText(index_i) ) {
      m_serialNumberSelector->setCurrentIndex( index_i );
      changedSerialNumber(serial_number_string );
    }
  }
}


DetectorView::~DetectorView() {
  m_redrawTimer->stop();
  delete m_redrawTimer;
}

void DetectorView::drawDetectorModel() {
  // @todo do not get current but selected connectivity
  m_detector.reset();
  m_detectorView->clear();

  PixA::ConnectivityRef selected_conn( calibrationDataManager().connectivity(m_dataSelection->type(),
									     m_dataSelection->serialNumber())) ;
  if (!m_factory.get() || !m_factoryConn.haveSameConnectivity(selected_conn)) {
    m_factory=std::unique_ptr<PixCon::IConnItemFactory>(new PixCon::CalibrationDataItemFactory(selected_conn,
											     calibrationDataManager().calibrationDataPtr(),
											     m_dataSelection,
                                                 m_detectorViewList->calibrationDataPalette()));
    m_factoryConn = selected_conn;
  }

  if (selected_conn) {
    m_detectorModel = guessDetectorModel(selected_conn);
  }

  drawDetectorSections partsToDraw;
  partsToDraw.crates = showCrateCheckbox->isChecked();
  partsToDraw.disks = showDisksCheckbox->isChecked();
  partsToDraw.barrel = showBarrelCheckbox->isChecked();
  partsToDraw.IBL = showIBLCheckbox->isChecked();
  partsToDraw.DBM = showDBMCheckbox->isChecked();
  partsToDraw.PP0s = showPP0Checkbox->isChecked();
  partsToDraw.legend = showLegendCheckbox->isChecked();

  // create detector model
  if (m_detectorModel == kFull) {
    m_detector = std::unique_ptr<PixDet::Detector>(new PixDet::FullDetectorModel(*m_factory, m_logoFactory, m_detectorView,
                                                                                 false, partsToDraw) );
  }
  else if (m_detectorModel == kFullOld) {
    m_detector = std::unique_ptr<PixDet::Detector>(new PixDet::FullDetectorModel(*m_factory, m_logoFactory, m_detectorView,
                                                                                 true, partsToDraw) );
  }
  else {
    m_detector = std::unique_ptr<PixDet::Detector>(new PixDet::ToothPixModel(*m_factory, m_logoFactory,  m_detectorView,
                                                                             partsToDraw) );
  }
  drawPalette();
  updateTitle();
}

void DetectorView::drawPalette()
{
  if (m_detector->hasSpaceForPalette()) {
    PixCon::ItemList &palette_item_list = m_detectorView->paletteItemList();
    palette_item_list.clear();
    if (m_dataSelection->varId().isValueWithOrWithoutHistory()) {
      const PixCon::ValueVarDef_t &var_def = m_dataSelection->varId().valueDef();
      PaletteItemFactory factory(*(calibrationDataPalette()->getValueColourList()), var_def.minVal(), var_def.maxVal());

      factory.create(m_detectorView->scene(), m_detector->paletteGeometry(), palette_item_list);
      //m_detectorView->canvas()->setChanged(m_detector->paletteGeometry());

      requestRedraw();

    }
    else if (m_dataSelection->varId().isStateWithOrWithoutHistory() ) {
      const PixCon::StateVarDef_t state_def = m_dataSelection->varId().stateDef();
      StatePaletteItemFactory factory(state_def.stateNameList(), m_dataSelection->varId().id(), calibrationDataPalette());
      factory.create(m_detectorView->scene(), m_detector->paletteGeometry(), palette_item_list);
      //m_detectorView->canvas()->setChanged(m_detector->paletteGeometry());
    }
  }
}

void DetectorView::initCalibrationData()
{/*
  PixCon::CalibrationData &calibration_data = calibrationDataManager().calibrationData();

  // add rod data container
  for (DetectorCanvasView::CrateList_t::const_iterator crate_iter = m_detectorView->beginCrate();
       crate_iter != m_detectorView->endCrate();
       crate_iter++) {
    for (DetectorCanvasView::RodList_t::const_iterator rod_iter = crate_iter->second.begin();
         rod_iter != crate_iter->second.end();
         rod_iter++) {
*/
      //
      /* 
      try {
        PixCon::CalibrationData::ConnObjDataList conn_obj_data( calibration_data.getConnObjectData((*rod_iter)->name()) );
	//        PixCon::CalibrationData::EnableState_t &conn_obj_enable_state (conn_obj_data.enableState(PixCon::kCurrent, 0));
      }
      catch (PixCon::CalibrationData::MissingConnObject &) {
        calibration_data.setCurrentEnableState((*rod_iter)->name(), false,false,false,false);
      }*/
  //  }
//  }

  // add module data container
  /*
  for (DetectorCanvasView::ModuleList_t::const_iterator module_iter = m_detectorView->beginModule();
       module_iter != m_detectorView->endModule();
       module_iter++) {
    try {
      PixCon::CalibrationData::ConnObjDataList conn_obj_data( calibration_data.getConnObjectData((module_iter->second)->name()) );
      //      PixCon::CalibrationData::EnableState_t &conn_obj_enable_state(conn_obj_data.enableState(PixCon::kCurrent, 0));
    }
    catch (PixCon::CalibrationData::MissingConnObject &) {
      calibration_data.setCurrentEnableState((module_iter->second)->name(), false,false,false,false);
    }
  }*/
}

void DetectorView::update( bool redraw_updates_only)
{
  bool request_redraw=false;
  try {
  // update rods
  for(DetectorCanvasView::CrateList_t::iterator crate_iter = m_detectorView->beginCrate();
      crate_iter != m_detectorView->endCrate();
      crate_iter++) {

    for(DetectorCanvasView::RodList_t::iterator rod_iter = crate_iter->second.begin();
	rod_iter != crate_iter->second.end();
	rod_iter++) {
      bool needs_update;
      if (isFocussed((*rod_iter)->name())) {
	needs_update = (*rod_iter)->focus();
      }
      else {
	needs_update = (*rod_iter)->unfocus();
      }

      //bool needs_update = (*rod_iter)->updateStyle();
      if (needs_update || !redraw_updates_only) {

//      QGraphicsItem *canvas_item=(*rod_iter)->canvasItem();
     // m_detectorView->canvas()->setChanged(canvas_item->boundingRect());

	  request_redraw=true;
      }
    }

  }

  // update modules
  for(DetectorCanvasView::ModuleList_t::iterator module_iter = m_detectorView->beginModule();
      module_iter != m_detectorView->endModule();
      module_iter++) {

    bool needs_update;
    if (isFocussed((module_iter->second)->name())) {
      needs_update = (module_iter->second)->focus();
    }
    else {
      needs_update = (module_iter->second)->unfocus();
    }
    //    bool needs_update = module_iter->second->updateStyle();
    if (needs_update  || !redraw_updates_only) {
//      QGraphicsItem *canvas_item=module_iter->second->canvasItem();
      //m_detectorView->canvas()->setChanged(canvas_item->boundingRect());
      request_redraw=true;

    }
  }

  updateZoomViewRedraw(redraw_updates_only);
  }
  catch( PixCon::CalibrationData::MissingValue & err) {
    std::stringstream message;
    message  <<  " Caught exception : " << err.what();
    emit statusMessage(QString::fromStdString(message.str()));
    //    statusBar()->message(message.str().c_str());
  }
  if (request_redraw) {
    requestRedraw();
  }

}

void DetectorView::updateZoomViewRedraw(bool redraw_updates_only)
{
  DetectorCanvasView::ConnItemList_t::iterator begin_item = m_detectorView->beginInsetConnItem();
  DetectorCanvasView::ConnItemList_t::const_iterator end_item = m_detectorView->endInsetConnItem();

  bool request_redraw=false;

  for (DetectorCanvasView::ConnItemList_t::iterator item_iter = begin_item;
       item_iter != end_item;
       item_iter++) {

    bool needs_update;
    if (isFocussed((item_iter->second)->name())) {
      needs_update = (item_iter->second)->focus();
    }
    else {
      needs_update = (item_iter->second)->unfocus();
    }
    //    bool needs_update = (item_iter->second)->updateStyle();
    if (needs_update  || !redraw_updates_only) {

//      QGraphicsItem *canvas_item=(item_iter->second)->canvasItem();
      //m_detectorView->canvas()->setChanged(canvas_item->boundingRect());
      request_redraw = true;
    }
  }
  if (request_redraw) {
    requestRedraw();
  }
}



void DetectorView::buildMessage(const QString &conn_name, const QRectF &rect)
{
  std::stringstream message;
  {
  PixCon::Lock lock(calibrationDataManager().calibrationDataMutex());
  PixCon::CalibrationData &calibration_data = calibrationDataManager().calibrationData();
  try {
    const PixCon::CalibrationData::ConnObjDataList conn_obj = calibration_data.getConnObjectData(conn_name.toLatin1().data());
    message << conn_name.toUtf8().constData(); 
    PixCon::CalibrationData::EnableState_t enable_state = conn_obj.enableState(m_dataSelection->type(), m_dataSelection->serialNumber());
    //if (enable_state.enabled()) {
    if (!enable_state.isInConnectivity()) {
      message << " (unused) ";
    }
    else {
      bool is_focussed = isFocussed(conn_name.toUtf8().constData());
      try {
	if (m_dataSelection->varId().hasHistory()) {
	  if (m_dataSelection->varId().isValueWithHistory() ) {

	    const PixCon::ValueHistory<float> &value_history( m_dataSelection->selectedHistory<float>(conn_name.toLatin1().data()) );
	    const PixCon::TimedValue<float> &newest_value( value_history.newest() );

	    message << " - " << PixCon::timeString( value_history.extractTime(newest_value) ) << " : ";
	    if (!is_focussed) message << "[";
	    message << PixCon::VarDefList::instance()->varName(m_dataSelection->varId().id()) << " = " << newest_value.value();
	    if (!is_focussed) message << "]";

	  }
	  else if (m_dataSelection->varId().isStateWithHistory() ) {

	    const PixCon::ValueHistory<PixCon::State_t> &value_history( m_dataSelection->selectedHistory<PixCon::State_t>(conn_name.toLatin1().data()) );
	    const PixCon::TimedValue<PixCon::State_t> &newest_value( value_history.newest() );

	    message << " - " << PixCon::timeString( value_history.extractTime(newest_value) ) << " : ";
	    if (!is_focussed) message << "[";
	    message << PixCon::VarDefList::instance()->varName(m_dataSelection->varId().id()) << " = (" << newest_value.value() << ")";

	    const PixCon::StateVarDef_t &state_def(  m_dataSelection->varId().stateDef() );
	    if ( state_def.isValidState(newest_value.value() ) ) {
	      message << " " << state_def.stateName( newest_value.value() );
	    }
	    else {
	      message << " * invalid state  * ";
	    }
	    if (!is_focussed) message << "]";

	  }
	}
	else if (m_dataSelection->varId().isValue() ) {
	  float value = m_dataSelection->selectedValue<float>(conn_name.toLatin1().data());
	  message << " : ";
	  if (!is_focussed) message << "[";
	  message << PixCon::VarDefList::instance()->varName(m_dataSelection->varId().id()) << " = " << value;
	  if (!is_focussed) message << "]";
	}
	else if (m_dataSelection->varId().isState() ) {
	  PixCon::State_t value = m_dataSelection->selectedValue<PixCon::State_t>(conn_name.toLatin1().data());
	  message << " : ";
	  if (!is_focussed) message << "[";
	  message << PixCon::VarDefList::instance()->varName(m_dataSelection->varId().id()) << " = (" << value << ")";

	  const PixCon::StateVarDef_t &state_def(  m_dataSelection->varId().stateDef() );
	  if ( state_def.isValidState(value) ) {
	    message << " " << state_def.stateName(value);
	  }
	  else {
	    message << " * invalid state  * ";
	  }
	  if (!is_focussed) message << "]";

	}
	if (!enable_state.enabled()) {
	  message << " (disabled)";
	}
      }
      catch (PixCon::CalibrationData::MissingConnObject &err) {
	message << "(undefined object)";
      }
      catch(PixCon::CalibrationData::MissingValue &err) {
	message << "(unknown)";
      }
    }
  }
  catch(PixCon::FatalCalibrationDataException &err) {
    message << "Exception : " << err.what();
  }
  catch(PixCon::CalibrationDataException &err) {
    message << " Nothing known about " << conn_name.toUtf8().constData();
  }
  }
  QString the_message(QString::fromStdString(message.str()));
  m_tip->setCurrentItem(rect, the_message);
  emit statusMessage(the_message);
  //  emit 
  //  statusBar()->message(message.str().c_str());
}

void DetectorView::selectItem(const QString &conn_name, const QRectF &rect)
{
  if (conn_name.length()>2 && m_factory.get()) {

    PixCon::EConnItemType conn_type = PixCon::guessConnItemType(conn_name.toUtf8().constData());
    if (conn_type==PixCon::kRodItem) {
      PixA::ConnectivityRef conn = calibrationDataManager().connectivity(m_dataSelection->type(),
									 m_dataSelection->serialNumber());
      if (conn) {
	try {
	  if (m_detectorView->isNewZoom(conn_name.toUtf8().constData())) {
	    m_detectorView->setZoomConnName(conn_name.toUtf8().constData());
	    m_detector->showPP0Zoom(*m_factory,m_detectorView, conn.pp0s(conn_name.toUtf8().constData()));
      }
	  updateZoomViewRedraw(false);
	  buildMessage(conn_name, rect);
	}
	catch( PixCon::CalibrationData::MissingValue & err) {
	  std::stringstream message;
	  message  <<  " Caught exception : " << err.what(); 
	  //	  statusBar()->message(message.str().c_str());
	  emit statusMessage(message.str().c_str());
	}
	catch( PixA::ConfigException & err) {
	  std::stringstream message;
	  message  <<  " Caught exception : " << err.getDescriptor(); 
	  //statusBar()->message(message.str().c_str());
	  emit statusMessage(message.str().c_str());
	}
      }
    }
    else if (conn_type==PixCon::kModuleItem || conn_type==PixCon::kPp0Item) {
      PixA::ConnectivityRef conn = calibrationDataManager().connectivity(m_dataSelection->type(),
								     m_dataSelection->serialNumber());
      if (conn) {
	try {
	  //@todo implement something more clever to get the pp0 iterator
	  const PixLib::Pp0Connectivity *pp0_conn=NULL;
	  if (conn_type == PixCon::kModuleItem) {
	    const PixLib::ModuleConnectivity *module_conn = conn.findModule(conn_name.toUtf8().constData());
	    if (module_conn) {
	      pp0_conn=PixA::pp0( module_conn );
	    }
	  }
	  else if (conn_type==PixCon::kPp0Item) {
	    pp0_conn=conn.findPp0( conn_name.toUtf8().constData() );
	  }

	  if (pp0_conn) {
	    const PixLib::RodBocConnectivity *rod_boc_conn=PixA::rodBoc( pp0_conn );
	    if ( rod_boc_conn ) {
	      PixA::Pp0List pp0_list = conn.pp0s( PixA::connectivityName(rod_boc_conn) );
	      std::string pp0_conn_name = PixA::connectivityName(pp0_conn);
	      PixA::Pp0List::const_iterator pp0_iter = pp0_list.find( pp0_conn_name );
	      if (pp0_iter != pp0_list.end()) {
		PixA::Pp0List::const_iterator next_pp0_iter=pp0_iter;
		++next_pp0_iter;
		if (m_detectorView->isNewZoom( pp0_conn_name )) {
		  m_detectorView->setZoomConnName( pp0_conn_name );
		  m_detector->showPP0Zoom(*m_factory,m_detectorView, pp0_iter, next_pp0_iter);
		}
		updateZoomViewRedraw(false);
		buildMessage(conn_name, rect);
		return;
	      }
	      std::stringstream message;
	      message  <<  " No module " << conn_name.toUtf8().constData() << " found in PP0 " << PixA::connectivityName(pp0_conn) << "."; 
	      // statusBar()->message(message.str().c_str());
	      emit statusMessage(message.str().c_str());

	    }
	  }

	  std::stringstream message;
	  message  <<  " No module " << conn_name.toUtf8().constData() << " found in connectivity."; 
	  //	  statusBar()->message(message.str().c_str());
	  emit statusMessage(message.str().c_str());

	}
	catch( PixCon::CalibrationData::MissingValue & err) {
	  std::stringstream message;
	  message  <<  " Caught exception : " << err.what(); 
	  // statusBar()->message(message.str().c_str());
	  emit statusMessage(message.str().c_str());
	}
	catch( PixA::ConfigException & err) {
	  std::stringstream message;
	  message  <<  " Caught exception : " << err.getDescriptor(); 
	  // statusBar()->message(message.str().c_str());
	  emit statusMessage(message.str().c_str());
	}
      }
    }
  }
}

void DetectorView::selectConnItemOnCanvas(PixCon::EConnItemType type, const std::string &conn_name) {
  m_detectorView->selectConnItem(type, conn_name);
}

void DetectorView::updateCategories(std::shared_ptr<PixCon::CategoryList> &categories)
{
  QString current_category( m_categorySelector->currentText() );
  bool has_category = false;
  m_categorySelector->clear();
  for (std::map<std::string, std::shared_ptr< std::vector< std::string > > >::const_iterator category_iter = categories->begin();
       category_iter != categories->end();
       category_iter++) {
    if (current_category == category_iter->first.c_str() ) {
      has_category = true;
    }
    m_categorySelector->addItem(category_iter->first.c_str());
  }
  if (m_categorySelector->view()) {
    QSizePolicy spol(QSizePolicy::Preferred,QSizePolicy::Minimum);
    spol.setHeightForWidth(false);
    m_categorySelector->view()->setSizePolicy( spol );
  }
  if (has_category) {
    changedCategory(current_category);
  }
  
}

void DetectorView::hideSerialNumberSelector() 
{
  m_serialNumberSelector->hide();
}

void DetectorView::clearAllSerialNumbers() 
{
  m_serialNumberSelector->clear();
}

void DetectorView::addSerialNumber(const std::string &serial_number_string)
{
  for (int item_i=0; item_i<m_serialNumberSelector->count(); item_i++) {
    if (m_serialNumberSelector->itemText(item_i)==QString(serial_number_string.c_str())) return;
  }
  if (m_serialNumberSelector->view()) {
    QSizePolicy spol(QSizePolicy::Preferred,QSizePolicy::Minimum);
    spol.setHeightForWidth(false);
    m_serialNumberSelector->view()->setSizePolicy( spol );
  }
  m_serialNumberSelector->addItem(serial_number_string.c_str());

}

void DetectorView::removeSerialNumber(const std::string &serial_number_string)
{

  std::string selected_serial_number_string = PixCon::makeSerialNumberString( m_dataSelection->type(), m_dataSelection->serialNumber() );
  if (serial_number_string == selected_serial_number_string) {
    //    m_dataSelection->setSelection(PixCon::kCurrent);
    //    m_dataSelection->setSerialNumber(0);
    changeDataSelection(PixCon::kCurrent, 0);

  }
  for (unsigned int count_i=0; count_i < static_cast<unsigned int>( m_serialNumberSelector->count() ); count_i++) {
    if (QString(serial_number_string.c_str()) == m_serialNumberSelector->itemText(count_i)) {
      m_serialNumberSelector->removeItem(count_i);
      break;
    }
  }
  for (unsigned int count_i=0; count_i < static_cast<unsigned int>( m_serialNumberSelector->count() ); count_i++) {
    if (QString(selected_serial_number_string.c_str()) == m_serialNumberSelector->itemText(count_i)) {
      m_serialNumberSelector->setCurrentIndex(count_i);
      return;
    }
  }
  m_serialNumberSelector->setCurrentIndex(0);

}


void DetectorView::changedCategory(const QString & the_category_name) 
{
  if (m_categorySelector->currentText() != the_category_name) {
    for(int i=0;i<m_categorySelector->count();i++){
      if(m_categorySelector->itemText(i)==the_category_name){
	m_categorySelector->setCurrentIndex(i);
	break;
      }
    }
  }
  std::string category_name(the_category_name.toLatin1().data());
  bool has_variable=false;
  try {
    QString current_var = m_variableSelector->currentText();
    m_variableSelector->clear();
    const PixCon::CategoryList::Category &category=m_detectorViewList->categories().category(category_name);
    std::set<std::string> var_list;
    calibrationDataManager().calibrationData().varList( selectedMeasurementType(), selectedSerialNumber(), var_list);
    PixCon::VarDef_t enabled_var_def = m_dataSelection->enabledVarDef();
    std::string enabled_var_name;
    if (PixCon::VarDefList::instance()->isValid(enabled_var_def.id())) {
      enabled_var_name = PixCon::VarDefList::instance()->varName(enabled_var_def.id());
      if (!enabled_var_name.empty()) {
	m_variableSelector->addItem(QString::fromStdString(enabled_var_name));
	if (current_var == enabled_var_name.c_str()) {
	  has_variable=true;
	}
      }
    }
    for (std::vector<std::string>::const_iterator var_iter = category.begin();
	 var_iter != category.end();
	 var_iter++) {

      if (var_iter->empty() || (*var_iter)[0]=='_') continue;  

      if (var_list.find(*var_iter) != var_list.end() && *var_iter != enabled_var_name) {
	m_variableSelector->addItem(QString::fromStdString(*var_iter));
	if (current_var == var_iter->c_str()) {
	  has_variable=true;
	}
      }
    }

    // provoce a recalculation of the combobox size
    m_variableSelector->setFont( m_variableSelector->font() ) ;
    m_variableSelector->updateGeometry() ;
    
    if (m_variableSelector->view()) {
      QSizePolicy spol(QSizePolicy::Preferred,QSizePolicy::Minimum);
      spol.setHeightForWidth(false);
      m_variableSelector->view()->setSizePolicy( spol );
      m_variableSelector->view()->setFont( m_variableSelector->view()->font() ) ;
      m_variableSelector->view()->updateGeometry();
    }

    if (has_variable) {
      if (current_var.compare(m_variableSelector->currentText())!=0) {
	changedVariable(current_var);
      }
    }
    else {
      if ( m_variableSelector->currentText().length()>0) {
	changedVariable(m_variableSelector->currentText());
      }
    }
  }
  catch(PixCon::CategoryList::MissingCategory &) {
  }

}

void DetectorView::updateCategory(PixCon::EMeasurementType measurement_type, PixCon::SerialNumber_t serial_number, const QString &category_name)
{
  if (m_categorySelector->currentText() == category_name && measurement_type == selectedMeasurementType() && serial_number == selectedSerialNumber()) {
    changedCategory(category_name);
  }
}

void DetectorView::changedVariable(const QString & variable_name)
{
  if (m_variableSelector->currentText() != variable_name) {
    for(int i=0;i<m_variableSelector->count();i++){
      if(m_variableSelector->itemText(i)==variable_name){
	m_variableSelector->setCurrentIndex(i);
	break;
      }
    }
  }

  try {
    const PixCon::VarDef_t var_def = PixCon::VarDefList::instance()->getVarDef(variable_name.toLatin1().data());
    m_dataSelection->setSelection(m_dataSelection->type(), var_def);
    update(true);
    drawPalette();
    updateTitle();

    PixCon::DataSelectionSpecificContextMenu * specific_context_menu = dynamic_cast<PixCon::DataSelectionSpecificContextMenu *>( m_contextMenu.get() );
    if (specific_context_menu) {
      specific_context_menu->dataSelectionChanged(*m_dataSelection);
    }

    emit changedDataSelection();
  }
  catch (PixCon::UndefinedCalibrationVariable &) {
  }

}

void DetectorView::changedVariable(const QString &the_category_name, const QString &variable_name)
{
  if (the_category_name != m_categorySelector->currentText()) {
    for(int i=0;i<m_categorySelector->count();i++){
      if(m_categorySelector->itemText(i)==the_category_name){
	m_categorySelector->setCurrentIndex(i);
	break;
      }
    }
    changedCategory( the_category_name);
  }
  if (variable_name != m_variableSelector->currentText()) {
    for(int i=0;i<m_variableSelector->count();i++){
      if(m_variableSelector->itemText(i)==variable_name){
	m_variableSelector->setCurrentIndex(i);
	break;
      }
    }
    changedVariable( variable_name );
  }
}


void DetectorView::changedSerialNumber(const QString & serial_number_name)
{
  PixCon::SerialNumber_t new_serial_number;
  PixCon::EMeasurementType measurement_type;
  if (s_currentValueLabel.compare(serial_number_name.toLatin1().data())==0) {
    new_serial_number=0;
    measurement_type=PixCon::kCurrent;
  }
  else if (serial_number_name.length()>2 && (serial_number_name[0]=='S' || serial_number_name[0]=='A')
	   && isdigit(serial_number_name[1].toLatin1())) {

    std::pair<PixCon::EMeasurementType, PixCon::SerialNumber_t> ret = PixCon::stringToSerialNumber( serial_number_name.toLatin1().data() );
    measurement_type = ret.first;
    new_serial_number = ret.second;
//     // scan
//     if (serial_number_name[0]=='S') {
//       measurement_type = PixCon::kScan;
//     }
//     else {
//       measurement_type = PixCon::kAnalysis;
//     }

//     new_serial_number= static_cast<PixCon::SerialNumber_t>( atoi(&((serial_number_name.toLatin1().data())[2])) )*1000000000;
//     new_serial_number+=static_cast<PixCon::SerialNumber_t>( atoi(&((serial_number_name.toLatin1().data())[4])) )*1000000;
//     new_serial_number+=static_cast<PixCon::SerialNumber_t>( atoi(&((serial_number_name.toLatin1().data())[8])) )*1000;
//     new_serial_number+=static_cast<PixCon::SerialNumber_t>( atoi(&((serial_number_name.toLatin1().data())[12])) );

//    std::cout << "INFO [DetectorView::changedSerialNumber] " << serial_number_name << " -> " << new_serial_number << std::endl;
  }
  else {
    std::cerr << "ERROR [DetectorView::changedSerialNumber] Serial number selector contains unsupported elment : " << serial_number_name.toUtf8().constData() << "." << std::endl;
    return;
  }
  changeDataSelection(measurement_type, new_serial_number);
}

void DetectorView::changeDataSelection(PixCon::EMeasurementType measurement_type, PixCon::SerialNumber_t new_serial_number)
{
  _changeDataSelection(measurement_type, new_serial_number);

  std::string serial_number_string = PixCon::makeSerialNumberString( measurement_type, new_serial_number);

  for (unsigned int item_i = 0; item_i < static_cast<unsigned int>(m_serialNumberSelector->count()); item_i++) {
    if (m_serialNumberSelector->itemText(item_i) == QString(serial_number_string.c_str())) {
      m_serialNumberSelector->setCurrentIndex(item_i);
      break;
    }
  }
}


void DetectorView::_changeDataSelection(PixCon::EMeasurementType measurement_type, PixCon::SerialNumber_t new_serial_number)
{
  PixA::ConnectivityRef selected_conn[2];
  for (unsigned int pass_i=0; pass_i<2; pass_i++) {
    try {
      switch (m_dataSelection->type()) {
      case PixCon::kCurrent:
	selected_conn[pass_i] = calibrationDataManager().currentConnectivity();
	break;
      case PixCon::kScan:
	selected_conn[pass_i] = calibrationDataManager().scanConnectivity(m_dataSelection->serialNumber());
	break;
      case PixCon::kAnalysis:
	selected_conn[pass_i] = calibrationDataManager().analysisConnectivity(m_dataSelection->serialNumber());
	break;
      default:
	break;
      }
    }
    catch (PixCon::CalibrationData::MissingConnObject &err) {
      if (m_dataSelection->type() == PixCon::kCurrent) {
	std::cerr << "WARNING [DetectorView::_changeDataSelection] No current connectivity." << std::endl;
      }
      else {
	std::cerr << "WARNING [DetectorView::_changeDataSelection] No connectivity for " <<  ((m_dataSelection->type() == PixCon::kScan) ? "scan" : "analysis")
		  << " " << m_dataSelection->serialNumber() << "." << std::endl;
      }
    }
    catch (std::runtime_error &err) {
      // may occure if a measurement got deleted but is still selected
    }
    PixCon::EMeasurementType old_measurement_type=m_dataSelection->type();
    PixCon::SerialNumber_t old_serial_number=m_dataSelection->serialNumber();
    m_dataSelection->setSelection(measurement_type);
    m_dataSelection->setSerialNumber(new_serial_number);
    if (   old_measurement_type != m_dataSelection->type() 
	|| old_serial_number != m_dataSelection->serialNumber() ) {
      changedCategory( m_categorySelector->currentText() );
    }
  }

  // NOTE:
  // It is assumed that only the serial number has an impact on the context menu.
  // If this assumption is false, the context menu needs to be updated also on 
  // other changes of the data selection.

  PixCon::DataSelectionSpecificContextMenu * specific_context_menu = dynamic_cast<PixCon::DataSelectionSpecificContextMenu *>( m_contextMenu.get() );
  if (specific_context_menu) {
    specific_context_menu->dataSelectionChanged(*m_dataSelection);
  }

  if (   (!selected_conn[1] && selected_conn[0])
      || (selected_conn[1] && !selected_conn[0])
      || (selected_conn[1] && !selected_conn[1].haveSameConnectivity(selected_conn[0]))) {
    m_factoryConn=PixA::ConnectivityRef();
    m_factory.release();
    drawDetectorModel();
    update(false);
  }
  else {
    drawPalette();
    updateTitle();
    updateChanged();
  }

  emit changedDataSelection();
}

bool DetectorView::event(QEvent *an_event)
{
    // @todo event processing should be done by the "parent" of all detector
    //       views and all views which show the right thing should be updated

    if (an_event && an_event->type() == QEvent::User) {
      PixCon::QIsInfoEvent * info_event  = dynamic_cast< PixCon::QIsInfoEvent *>(an_event);
      if (info_event) {

	if (info_event->valid()) {
	  if ( m_dataSelection->type()==info_event->measurementType() ) {

	    // if the variable name is empty then the enable state has changed
	    if (info_event->varName().size()!=0) {
	      // otherwise the view is only updated if the variable value has changed.
	      PixCon::VarDef_t var_def = PixCon::VarDefList::instance()->getVarDef(info_event->varName());
	      if (m_dataSelection->varId().id() != var_def.id()) return false;
	    }
	    PixCon::Lock lock(calibrationDataManager().calibrationDataMutex());
	    PixCon::EConnItemType conn_item_type = PixCon::guessConnItemType(info_event->connName() );
	    updateConnItem( conn_item_type,
			    info_event->connName(),
			    info_event->measurementType(),
			    info_event->serialNumber(),
			    info_event->propagateToChilds());
	  }
	  return true;
	}
      }
    }
  //  DetectorView::event(an_event);
    return false;
}

bool DetectorView::updateConnItem(PixCon::EConnItemType conn_type,
				  const std::string &conn_name,
				  PixCon::EMeasurementType measurement_type,
				  PixCon::SerialNumber_t serial_number,
				  bool propagate_to_childs)
{
  bool request_redraw = false;
  request_redraw |= updateConnItem(conn_type,conn_name, measurement_type, serial_number);
  if (propagate_to_childs && conn_type != PixCon::kModuleItem) {
    PixA::ConnectivityRef conn( calibrationDataManager().connectivity(measurement_type,serial_number) );
    if (conn) {
      switch (conn_type) {
      case PixCon::kCrateItem: {
	try {
	  PixA::RodList rod_list = conn.rods( conn_name );
	  PixA::RodList::const_iterator rod_begin = rod_list.begin();
	  PixA::RodList::const_iterator rod_end = rod_list.end();

	  if (rod_begin != rod_end) {
	    const PixLib::RodCrateConnectivity *crate = PixA::crate( *rod_begin );
	    if (crate && const_cast<PixLib::RodCrateConnectivity *>(crate)->tim()) {
	      request_redraw |= updateConnItem(PixCon::kTimItem, PixA::connectivityName( const_cast<PixLib::RodCrateConnectivity *>(crate)->tim() ), measurement_type, serial_number);
	    }
	  }

	  for(PixA::RodList::const_iterator rod_iter = rod_begin;
	      rod_iter != rod_end;
	      ++rod_iter) {

	    request_redraw |= updateConnItem(PixCon::kRodItem,PixA::connectivityName(rod_iter), measurement_type, serial_number);

	    PixA::Pp0List::const_iterator pp0_end = pp0End(rod_iter);
	    for(PixA::Pp0List::const_iterator pp0_iter = pp0Begin(rod_iter);
		pp0_iter != pp0_end;
		++pp0_iter) {

	      //@todo add pp0 item
	      request_redraw |= updateConnItem(PixCon::kPp0Item,PixA::connectivityName(pp0_iter), measurement_type, serial_number);

	      PixA::ModuleList::const_iterator module_end = PixA::modulesEnd(pp0_iter);
	      for(PixA::ModuleList::const_iterator module_iter = PixA::modulesBegin(pp0_iter);
		  module_iter != module_end;
		  ++module_iter) {
		request_redraw |= updateConnItem(PixCon::kModuleItem,PixA::connectivityName(module_iter), measurement_type, serial_number);
	      }
	    }
	  }
	}
	catch (PixA::ConfigException &err) {
	  std::cerr << "WARNING [DetectorView::updateConnItem] caught exception : " << err.getDescriptor() << std::endl;
	}
	break;
      }
      case PixCon::kRodItem: {
	try {
	  PixA::Pp0List pp0_list = conn.pp0s( conn_name );
	  PixA::Pp0List::const_iterator pp0_end = pp0_list.end();

	  for(PixA::Pp0List::const_iterator pp0_iter = pp0_list.begin();
	      pp0_iter != pp0_end;
	      ++pp0_iter) {

	    request_redraw |= updateConnItem(PixCon::kPp0Item,PixA::connectivityName(pp0_iter), measurement_type, serial_number);

	    PixA::ModuleList::const_iterator module_end = PixA::modulesEnd(pp0_iter);
	    for(PixA::ModuleList::const_iterator module_iter = PixA::modulesBegin(pp0_iter);
		module_iter != module_end;
		++module_iter) {
	      request_redraw |=  updateConnItem(PixCon::kModuleItem,PixA::connectivityName(module_iter), measurement_type, serial_number);
	    }
	  }
	}
	catch (PixA::ConfigException &err) {
	  std::cerr << "WARNING [DetectorView::updateConnItem] caught exception : " << err.getDescriptor() << std::endl;
	}
	break;
      }
      case PixCon::kPp0Item: {
	try {
	  PixA::ModuleList module_list = conn.modules( conn_name );
	  PixA::ModuleList::const_iterator module_end = module_list.end();
	  for(PixA::ModuleList::const_iterator module_iter = module_list.begin();
	      module_iter != module_end;
	      ++module_iter) {
	    request_redraw |=  updateConnItem(PixCon::kModuleItem,PixA::connectivityName(module_iter), measurement_type, serial_number);
	  }
	}
	catch (PixA::ConfigException &err) {
	  std::cerr << "WARNING [DetectorView::updateConnItem] caught exception : " << err.getDescriptor() << std::endl;
	}
	break;
      }
      case PixCon::kNConnItemTypes:
	// avoid warning about missing cases
      case PixCon::kTimItem:
      case PixCon::kModuleItem: {
	// no childs
	break;
      }
      }
    }
  }
  return request_redraw;
}


bool DetectorView::updateConnItem(PixCon::EConnItemType conn_type,
				  const std::string &conn_name,
				  PixCon::EMeasurementType measurement_type,
				  PixCon::SerialNumber_t serial_number)
{
  // should already been verified by the calling method :
  assert ( m_dataSelection->type()== measurement_type );
  assert ( m_dataSelection->serialNumber()== serial_number );
  bool request_redraw=false;

  if (conn_type == PixCon::kModuleItem) {
    DetectorCanvasView::ModuleList_t::iterator module_iter = m_detectorView->findModule(conn_name);

    if (module_iter != m_detectorView->endModule()) {
      do {
	if ( module_iter->second->updateStyle() ) {
//      QGraphicsItem *canvas_item=module_iter->second->canvasItem();
//      QRectF rect=canvas_item->boundingRect();
      //m_detectorView->canvas()->setChanged(canvas_item->boundingRect());
	  request_redraw=true;
	}
      } while ( ++module_iter != m_detectorView->endModule() && module_iter->first == conn_name);
    }

  }
  else if (conn_type == PixCon::kRodItem) {
    PixCon::ConnItem *conn_item= m_detectorView->findRod(conn_name);

    if ( conn_item && conn_item->updateStyle() ) {
//      QGraphicsItem *canvas_item=conn_item->canvasItem();
      //m_detectorView->canvas()->setChanged(canvas_item->boundingRect());

      request_redraw=true;

    }
  }

  // update items of inset view
  PixCon::ConnItem *conn_item_update = m_detectorView->getInsetConnItem(conn_name);
  if (conn_item_update  && conn_item_update->updateStyle() ) {

//    QGraphicsItem *canvas_item=conn_item->canvasItem();
    //m_detectorView->canvas()->setChanged(canvas_item->boundingRect());

    request_redraw=true;
  }

  if (conn_type != PixCon::kModuleItem) {
    // extra conn items e.g. zoom view.
    PixCon::ConnItem *conn_item = m_detectorView->findExtraItem(conn_name);
    if (conn_item && conn_item->updateStyle() ) {
//      QGraphicsItem *canvas_item=conn_item->canvasItem();
      //m_detectorView->canvas()->setChanged(canvas_item->boundingRect());

      request_redraw=true;
    }
  }

  if (request_redraw) {
    requestRedraw();
  }

  return true;
}

void DetectorView::updateView() 
{
  if (m_update) {
    m_update=false;
    m_detectorView->scene()->update();
  }
}

void DetectorView::cloneView() {
  PixCon::DetectorViewWindow * new_view = new PixCon::DetectorViewWindow(*m_detectorViewList,*m_app);
  new_view->detectorView().copySelection(*this);
  new_view->show();
}

void DetectorView::copySelection(const DetectorView &a)
{
  if (a.m_categorySelector->currentText().length()>0) {
    for(int i=0;i<m_categorySelector->count();i++){
      if(m_categorySelector->itemText(i)==a.m_categorySelector->currentText()){
	m_categorySelector->setCurrentIndex(i);
	break;
      }
    }
    changedCategory(m_categorySelector->currentText());
  }
  if (a.m_serialNumberSelector->currentText().length()>0) {
    for(int i=0;i<m_serialNumberSelector->count();i++){
      if(m_serialNumberSelector->itemText(i)==a.m_serialNumberSelector->currentText()){
	m_serialNumberSelector->setCurrentIndex(i);
	break;
      }
    }
    changedSerialNumber(m_serialNumberSelector->currentText());
  }
  if (a.m_serialNumberSelector->isHidden() == true) m_serialNumberSelector->hide();
  if (a.m_variableSelector->currentText().length()>0) {
    for(int i=0;i<m_variableSelector->count();i++){
      if(m_variableSelector->itemText(i)==a.m_variableSelector->currentText()){
	m_variableSelector->setCurrentIndex(i);
	break;
      }
    }
    changedVariable(m_variableSelector->currentText());
  }
}


void DetectorView::zoomIn() {
  if (m_zoomLevel<s_zoomLevelList.size()) {
    if (!m_zoomOutButton->isEnabled()) {
      m_zoomOutButton->setEnabled(true);
    }
    m_zoomLevel++;
    QMatrix w_matrix( m_normalMatrix );
    w_matrix.rotate(s_rotatedView[m_rotateLevel]);
    w_matrix.scale(s_zoomLevelList[m_zoomLevel],s_zoomLevelList[m_zoomLevel]);
    m_detectorView->setMatrix(w_matrix);
    if (m_zoomLevel+1>=s_zoomLevelList.size()) {
      m_zoomInButton->setEnabled(false);
    }
  }
}

void DetectorView::zoomOut() {
  if (m_zoomLevel>0) {
    if (!m_zoomInButton->isEnabled()) {
      m_zoomInButton->setEnabled(true);
    }
    m_zoomLevel--;
    QMatrix w_matrix( m_normalMatrix );
    w_matrix.rotate(s_rotatedView[m_rotateLevel]);
    w_matrix.scale(s_zoomLevelList[m_zoomLevel],s_zoomLevelList[m_zoomLevel]);
    m_detectorView->setMatrix(w_matrix);
    if (m_zoomLevel==0) {
      m_zoomOutButton->setEnabled(false);
    }
  }
}

void DetectorView::rotateView() {
    m_rotateLevel = (m_rotateLevel+1)%s_rotatedView.size();
        QMatrix w_matrix(m_normalMatrix);
        w_matrix.scale(s_zoomLevelList[m_zoomLevel], s_zoomLevelList[m_zoomLevel]);
        w_matrix.rotate(s_rotatedView[m_rotateLevel]);
        m_detectorView->setMatrix(w_matrix);
}

void DetectorView::showAxisRangeDialog()
{
  QMenu mymenu_q4("palette_menu");
  if (m_dataSelection->varId().isValueWithOrWithoutHistory() ) {
    mymenu_q4.addAction("Adjust Range",this,SLOT(sAdjustRange() ) );
    mymenu_q4.addAction("Full Range",this,SLOT(sFullRange() ) );
  }
  else if (m_dataSelection->varId().isStateWithOrWithoutHistory() ) {
    mymenu_q4.addAction("Cycle States",this,SLOT(sCycleStates() ) );
  }
  else {
    std::cout << "INFO [DetectorView::showAxisRangeDialog] not a value or state." << std::endl;
    return;
  }
  mymenu_q4.addAction("Show Histogram (all)", this, SLOT(sShowHistogram() ) );
  mymenu_q4.addAction("Show Histogram (enabled)", this, SLOT(sShowHistogramEnabled() ) );
  mymenu_q4.exec(QCursor::pos());
}

// slot functions
void DetectorView::sAdjustRange() {
  QAction *sender = dynamic_cast<QAction*>(QObject::sender());
  if(sender==0) return;

  AxisRangeBase dialog(this);
  if (m_dataSelection->varId().isValueWithOrWithoutHistory() ) {
      const PixCon::ValueVarDef_t &var_def = m_dataSelection->varId().valueDef();
      std::stringstream temp_text;
      temp_text << var_def.minVal();
      dialog.m_minEntry->setText(temp_text.str().c_str());
      temp_text.str("");
      temp_text << var_def.maxVal();
      dialog.m_maxEntry->setText(temp_text.str().c_str());
  }
  if (dialog.exec()  == QDialog::Accepted){
      double a_min = strtod(dialog.m_minEntry->text().toLatin1().data(),NULL);
      double a_max = strtod(dialog.m_maxEntry->text().toLatin1().data(),NULL);
      if (a_min>a_max) {
        double temp = a_max;
        a_max=a_min;
        a_min=temp;
      }
      if (a_min <a_max) {
        emit axisChangeRequest(static_cast<float>(a_min), static_cast<float>(a_max));
      }
  }

}

void DetectorView::sFullRange() {
  QAction *sender = dynamic_cast<QAction*>(QObject::sender());
  if(sender==0) return;
  m_dataSelection->setMinMaxValue();
  update(true);
  drawPalette();
}
void DetectorView::sCycleStates() {
  QAction *sender = dynamic_cast<QAction*>(QObject::sender());
  if(sender==0) return;
  if (m_dataSelection->varId().isStateWithOrWithoutHistory() ) {
      const PixCon::StateVarDef_t &var_def = m_dataSelection->varId().stateDef();
      unsigned int page = var_def.page() + 12;
      if (page>=var_def.nStates()) {
        page=0;
      }
      else if (page+10>var_def.nStates()) {
        if (var_def.nStates()>12) {
          page=var_def.nStates()-12;
        }
        else {
          page=0;
        }
      }
      if (page != var_def.page()) {
        emit axisChangeRequest(page);
      }
  }
}
void DetectorView::sShowHistogram() {
  QAction *sender = dynamic_cast<QAction*>(QObject::sender());
  if(!sender) return;
  histogramCurrentVariable();
}
void DetectorView::sShowHistogramEnabled() {
  QAction *sender = dynamic_cast<QAction*>(QObject::sender());
  if(!sender) return;
  histogramCurrentVariableEnabledItems();
}


void DetectorView::changeAxis(float min, float max)
{
  if (min<max) {
    if (m_dataSelection->varId().isValueWithOrWithoutHistory() ) {
      PixCon::ValueVarDef_t &var_def = m_dataSelection->varId().valueDef();
      if (min<max) {
	var_def.setMinMax(min,max);
	update(true);
	drawPalette();
      }
    }
  }
}

void DetectorView::cycleStates(unsigned int new_page)
{
  if (m_dataSelection->varId().isStateWithOrWithoutHistory() ) {
    PixCon::StateVarDef_t &var_def = m_dataSelection->varId().stateDef();
    if (new_page!= var_def.page()) {
      var_def.setPage(new_page);
      update(true);
      drawPalette();
    }
  }
}


void DetectorView::buildMessage(PaletteAxisItem *axis_item, const QRectF &rect)
{
  if (axis_item) {
    std::stringstream message;
    QString the_message(QString::fromStdString(message.str()));
    m_tip->setCurrentItem(rect, the_message);
    emit statusMessage(the_message);
  }
}

void DetectorView::copySerialNumbers( const DetectorView &a_view)
{
  for (int index_i = 0; index_i < a_view.m_serialNumberSelector->count(); index_i++) {
    m_serialNumberSelector->addItem( a_view.m_serialNumberSelector->itemText(index_i) );
  }
  
}
void DetectorView::printToFile()
{
    QString print_name;
    print_name += "PixelView_";
    switch (m_dataSelection->type()) {
        case PixCon::kCurrent:
            break;
        case PixCon::kScan:
        case PixCon::kAnalysis: {
            print_name+=(m_dataSelection->type()==PixCon::kScan ? 'S' : 'A');
            std::stringstream number;
            number << std::setw(9) << std::setfill('0') << m_dataSelection->serialNumber();
            print_name += QString::fromStdString(number.str());
            print_name += "_";
            break;
          }
          default:
            break;
          }
    print_name += QString::fromStdString(PixCon::VarDefList::instance()->varName(m_dataSelection->varId().id()));
    QString default_name = print_name+".png";
    QFileDialog* dialog = new QFileDialog(this, "Print/save canvas to file", QDir::homePath()+"/"+default_name, "Supported formats (*.png *.jpg *.eps *.bmp *.ppm)" );
    dialog->setFileMode(QFileDialog::AnyFile);
    dialog->setAcceptMode(QFileDialog::AcceptSave);

    if(dialog->exec() == QDialog::Accepted) {
        QString saveAs = dialog->selectedFiles().at(0).toLatin1().data();
        QByteArray suffix = saveAs.right(3).toLatin1();
        QImage bitmap(this->size(), QImage::Format_ARGB32);
        QPainter painter(&bitmap);
        this->render(&painter,QPoint(),QRegion(),QWidget::DrawChildren);
        QImageWriter writer(saveAs,suffix);
        writer.write(bitmap);
        std::cout << "INFO [DetectorView::printToFile] Wrote file " << std::endl;
    }
}

DetectorView::EDetectorModel DetectorView::guessDetectorModel(const PixA::ConnectivityRef &connectivity) {

  std::array<std::string, 22> SR1_PP0S{
	  "L0_B12_S1_A6","L0_B12_S1_C7","L0_B12_S2_A7","L0_B12_S2_C6",
	  "L2_B27_S1_A6","L2_B27_S1_C7","L2_B27_S2_A7","L2_B27_S2_C6",
	  "LI_S16_A","LI_S16_C","LI_S17_A","LI_S17_C","LI_S18_A","LI_S18_C",
	  "L1_B21_S1_A6","L1_B21_S1_C7",
	  "L2_B28_S1_A6","L2_B28_S1_C7","L2_B28_S2_A7","L2_B28_S2_C6",
      "LX_B99_S1_A7", "LX_B99_S1_C7" };

  for (auto const & PP0: SR1_PP0S) {
    if (connectivity.findPp0(PP0)) {
      //return kFull; //for debug to get the kFull even in SR1
      return kToothPix;
    }
  }
  if (connectivity.tag(PixA::IConnectivity::kId)=="CT") {
    return kFullOld;
  } else {
    return kFull;
  }
}

namespace PixCon {

  class FocussedItemSelection : public ISelection
  {
  public:
    FocussedItemSelection(const DetectorView &detector_view) : m_detectorView(&detector_view) {}
    bool isSelected(const std::string &name) const { return m_detectorView->isFocussed(name);}

  private:
    const DetectorView *m_detectorView;
  };
}

void DetectorView::histogramCurrentVariable(bool enabled_only)
{
  if (m_dataSelection->varId().isValid()) {
  {
      std::shared_ptr<PixCon::ISelection> selection(new PixCon::FocussedItemSelection( *this ));
      PixCon::SummaryHistogramView::createHistogramView(m_slowUpdateList,
							selection,
							calibrationDataManagerPtr(),
							m_dataSelection->type(),
							m_dataSelection->serialNumber(),
							(m_dataSelection->isEnabledVar() ? 
							 PixCon::VarDef_t::invalid()
							 : m_dataSelection->varId()),
							enabled_only);
    }
  }
}

void DetectorView::updateTitle() {
  if (m_detector.get()) {
    std::string title;
    std::string sub_title;

    title = makeSerialNumberString(m_dataSelection->type(),m_dataSelection->serialNumber());

    const PixCon::MetaData_t *meta_data = NULL;
    if (m_dataSelection->type() == PixCon::kScan) {
      const PixCon::ScanMetaDataCatalogue &scan_meta_data_catalogue = calibrationDataManager().scanMetaDataCatalogue();
      const PixCon::ScanMetaData_t *scan_meta_data = scan_meta_data_catalogue.find(m_dataSelection->serialNumber());
      meta_data = scan_meta_data;
    }
    else if (m_dataSelection->type() == PixCon::kAnalysis) {
      const PixCon::AnalysisMetaDataCatalogue &analysis_meta_data_catalogue = calibrationDataManager().analysisMetaDataCatalogue();
      const PixCon::AnalysisMetaData_t *analysis_meta_data = analysis_meta_data_catalogue.find(m_dataSelection->serialNumber());
	meta_data = analysis_meta_data;
    }

    if (meta_data) {
      title += " ";
      title += meta_data->name();
    }


    title += " - ";
    title += PixCon::VarDefList::instance()->varName(m_dataSelection->varId().id());

    PixA::ConnectivityRef selected_conn( calibrationDataManager().connectivity(m_dataSelection->type(),
									       m_dataSelection->serialNumber())) ;
    if (selected_conn ) {
      std::string cfg_tag( selected_conn.tag(PixA::IConnectivity::kConfig) );
      std::string mod_cfg_tag( selected_conn.tag(PixA::IConnectivity::kModConfig) );
      sub_title += cfg_tag;
      if (cfg_tag != mod_cfg_tag) {
	sub_title += ", ";
	sub_title += mod_cfg_tag;
      }
    }
    m_detector->updateTitle(title,sub_title);
    requestRedraw();
  }

}



