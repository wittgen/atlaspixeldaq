#include "IsMonitor.h"
#include "IsMonitorReceiver.ixx"

#undef DEBUG_TRACE
#ifdef DEBUG
#  define DEBUG_TRACE(a) { a; }
#  include <iostream>
#else
#  define DEBUG_TRACE(a) {  }
#endif

namespace PixCon {

  template <class Src_t, class SrcRef_t >
  void SimpleIsMonitorReceiver<Src_t, SrcRef_t>::_update()
  {

    DEBUG_TRACE( std::cout << "INFO [IsMonitorReceiver<Src_t>::_update] " << this->varName()<< std::endl )

    ISInfoT<Src_t> is_value;
    this->_updateLoop(is_value);
  }

    template <class Src_t, class SrcRef_t >
  void SimpleIsMonitorReceiver<Src_t, SrcRef_t>::valueChanged(ISCallbackInfo *info) {

    Lock lock(m_monitorLock);

    // strange way to check for multiple calls, replaced by mutex lock
    //    Counter counter(this->m_concurrencyCounter); // for debugging/validation

    ISInfoT<Src_t> is_value;
    info->value(is_value);

    this->bufferValue(info->name(), info->time().c_time(), is_value, info->reason() == ISInfo::Deleted );
  }


  template <class Src_t, class SrcRef_t, class Dest_t>
  void SimpleIsMonitor<Src_t, SrcRef_t,Dest_t>::_update()
  {

    DEBUG_TRACE( std::cout << "INFO [IsMonitorReceiver<Src_t>::_update] " << this->varName()<< std::endl )

    ISInfoT<Src_t> is_value;
    this->_updateLoop(is_value);
  }

    template <class Src_t, class SrcRef_t ,class Dest_t>
  void SimpleIsMonitor<Src_t, SrcRef_t,Dest_t>::valueChanged(ISCallbackInfo *info) {

    Lock lock(m_monitorLock);

    // strange way to check for multiple calls, replaced by mutex lock
    //Counter counter(this->m_concurrencyCounter); // for debugging/validation

    ISInfoT<Src_t> is_value;
    info->value(is_value);

    this->bufferValue(info->name(), info->time().c_time(), is_value, info->reason() == ISInfo::Deleted );

  }



  // instantiation of the defined monitors
  template class IsMonitor<ISInfoT<unsigned short>, unsigned short, unsigned short, unsigned short>;  //IsStateMonitor
  template class IsMonitor<ISInfoT<int>,int, int, unsigned short>;                                    //IsIntStateMonitor
  template class IsMonitor<ISInfoT<double>, double, double, float>;                                   //IsDoubleValueMonitor;
  template class IsMonitor<ISInfoT<float>, float, float, float>;                                      //IsValueMonitor;
  template class IsMonitor<ISInfoT<int>,int, int, float>;                                             //IsIntValueMonitor;
  template class IsMonitor<ISInfoT<unsigned int>,unsigned int, unsigned int, float>;                  //IsUIntValueMonitor


  template class SimpleIsMonitor<unsigned short, unsigned short, unsigned short>;  //IsStateMonitor
  template class SimpleIsMonitor<int, int, unsigned short>;                        //IsIntStateMonitor
  template class SimpleIsMonitor<double, double, float>;                           //IsDoubleValueMonitor;
  template class SimpleIsMonitor<float, float, float>;                             //IsValueMonitor;
  template class SimpleIsMonitor<int, int, float>;                                 //IsIntValueMonitor;
  template class SimpleIsMonitor<unsigned int, unsigned int, float>;               //IsUIntValueMonitor

  template class SimpleIsMonitorReceiver<std::string,const std::string&>;
  template class SimpleIsMonitorReceiver<unsigned short,unsigned short>;
  template class SimpleIsMonitorReceiver<double, double>;
  template class SimpleIsMonitorReceiver<float, float>;
  template class SimpleIsMonitorReceiver<int, int>;
  template class SimpleIsMonitorReceiver<unsigned int, unsigned int>;


  template class IsMonitorReceiver<ISInfoT<std::string>,std::string,const std::string&>;

  template class IsMonitorReceiver<ISInfoT<unsigned short>,unsigned short,unsigned short>;
  template class IsMonitorReceiver<ISInfoT<double>,double, double>;
  template class IsMonitorReceiver<ISInfoT<float>,float, float>;
  template class IsMonitorReceiver<ISInfoT<int>, int, int>;
  template class IsMonitorReceiver<ISInfoT<unsigned int>,unsigned int, unsigned int>;

  template class IsMonitorReceiverBase<ISInfoT<unsigned short>,unsigned short>;
  template class IsMonitorReceiverBase<ISInfoT<unsigned int>,unsigned int>;
  template class IsMonitorReceiverBase<ISInfoT<double>,double>;
  template class IsMonitorReceiverBase<ISInfoT<float>,float>;
  template class IsMonitorReceiverBase<ISInfoT<int>,int>;
  template class IsMonitorReceiverBase<ISInfoT<std::string>,std::string>;
}
