// Dear emacs, this is -*-c++-*-
#ifndef _SummaryHistogramView_H_
#define _SummaryHistogramView_H_

#include <ui_SummaryHistogramViewBase.h>
#include <CalibrationDataTypes.h>
#include <memory>
#include <DataContainer/HistoUtil.h>
#include "ICalibrationDataListener.h"
//Added by qt3to4:
#include <QCloseEvent>
//Added by qt3to4:
//#include <Q3VBoxLayout>

class TCanvas;

namespace PixCon {

  class ISelection;
  class CalibrationDataManager;
  class VarDef_t;
  class SlowUpdateList;

  class SummaryHistogramView : public QMainWindow, public Ui_SummaryHistogramViewBase
  {
    Q_OBJECT
  public:

    class Listener : public ICalibrationDataListener
    {
    public:
      Listener(SummaryHistogramView *histogram_view, EMeasurementType measurement_type, SerialNumber_t serial_number, VarDef_t var_def) 
	: m_measurementType(measurement_type),
	  m_serialNumber(serial_number),
	  m_varDef(var_def),
	  m_histogramView(histogram_view)
      {}

      void update() {
	m_histogramView->update();
      }

      bool needUpdate(EMeasurementType measurement, SerialNumber_t serial_number, VarId_t var_id, const std::string &conn_name );

      EMeasurementType  measurementType() const { return m_measurementType; };
      SerialNumber_t    serialNumber() const    { return m_serialNumber; }
      const VarDef_t   &varDef() const          { return m_varDef; }

    private:
      EMeasurementType  m_measurementType;
      SerialNumber_t    m_serialNumber;
      VarDef_t          m_varDef;

      SummaryHistogramView *m_histogramView;
    };

    SummaryHistogramView(const std::shared_ptr<SlowUpdateList> &slow_update_list,
			 const std::shared_ptr<ISelection> &selection,
			 const std::shared_ptr<CalibrationDataManager> &calibration_data,
			 EMeasurementType measurement_type,
			 SerialNumber_t serial_number,
			 VarDef_t var_def,
			 bool enabled_only=true,
			 QWidget* parent = 0);

    ~SummaryHistogramView();

    TCanvas *canvas();

    bool needUpdate(EMeasurementType measurement, SerialNumber_t serial_number, VarId_t var_id, const std::string &conn_name );
    
    void updateHistoView();

    static SummaryHistogramView *createHistogramView( const std::shared_ptr<SlowUpdateList> &slow_update_list,
						      const std::shared_ptr<ISelection> &selection,
						      const std::shared_ptr<CalibrationDataManager> &calibration_data,
						      EMeasurementType measurement_type,
						      SerialNumber_t serial_number,
						      VarDef_t var_def,
						      bool enabled_only=true);
  protected:
    void init();

    void closeEvent(QCloseEvent * );

  private:

    PixA::HistoBinning                        m_binning;
    std::shared_ptr<SlowUpdateList>         m_slowUpdateList;
    std::shared_ptr<ISelection>             m_selection;
    std::shared_ptr<CalibrationDataManager> m_calibrationData;
    std::shared_ptr<Listener>               m_listener;
    
    bool              m_enabledOnly; //**< only show values of enabled connectivity objects.*/
  };

}
#endif
