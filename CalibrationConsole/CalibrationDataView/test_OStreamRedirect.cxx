#include "OStreamRedirector.h"
#include <fstream>
#include <iostream>
#include <memory>

#include <time.h>

#include "LogWindow.h"
#include <qapplication.h>
#include <qthread.h>

class Listener : public IStreamListener
{
public:
  Listener(const std::string &log_name)
   : m_out( new std::ofstream(log_name.c_str()))
  {
  }

  void putLine(const std::string &line) {
    time_t current_time = time(NULL);
      (*m_out) << ctime(&current_time) << " : " << line << std::endl;
  }

  std::unique_ptr<std::ofstream> m_out;
};



void simpleTest()
{
  Listener listener("/tmp/test.log");
  OStreamRedirector<> redirect1(std::cout, &listener );
  //  OStreamRedirector<> redirect2(std::cerr, &listener );

  std::cout <<"hallo" << std::endl;
  std::cout <<"das" << std::endl;
  std::cout <<"ist" << std::endl;
  std::cout <<"ein" << std::endl;
  std::cout <<"test" << std::endl;
  std::cout.flush();
}

class test : public QThread
{
public:
  test() { start(); }
  void run() {
    for (unsigned int i=0; i<300; i++) {
      std::cout << " line " << i << std::endl;
      if ((i-1)%30==0) {
	sleep(1);
      }
    }
  }
};

void qtTest(int argc, char **argv)
{
  QApplication app( argc, argv );
  app.connect( &app, SIGNAL( lastWindowClosed() ), &app, SLOT( quit() ) );
  std::unique_ptr<PixCon::LogWindow> log_window ( new PixCon::LogWindow(20,"/tmp/goetz/test.log", &app) );
  log_window->show();

  std::cout <<"INFO [qtTest::test] hallo" << std::endl;
  std::cout <<"WARNING [qtTest::test2]  das" << std::endl;
  std::cout <<"ERROR [anaotherqtTest::test] ist" << std::endl;
  std::cerr <<"FATAL [anaotherqtTest::test] ein" << std::endl;
  std::cout <<" Error!! test" << std::endl;
  std::cout <<"INFO [qtTest::test] hallo" << std::endl;
  std::cout <<"WARNING [qtTest::test2]  das" << std::endl;
  std::cout <<"ERROR [anaotherqtTest::test] ist" << std::endl;
  std::cerr <<"FATAL [anaotherqtTest::test] ein" << std::endl;
  std::cout <<" Error!! test" << std::endl;
  std::cout <<"INFO [qtTest::test] hallo" << std::endl;
  std::cout <<"WARNING [qtTest::test2]  das" << std::endl;
  std::cout <<"ERROR [anaotherqtTest::test] ist" << std::endl;
  std::cerr <<"FATAL [anaotherqtTest::test] ein" << std::endl;
  std::cout <<" Error!! test" << std::endl;
  test a_test;
  app.exec();
}

int main()
{
  char *empty=NULL;
  qtTest(0,&empty);
  return 0;
}
