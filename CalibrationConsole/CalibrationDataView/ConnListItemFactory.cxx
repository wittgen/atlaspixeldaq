#include "ConnListItemFactory.h"
#include "CalibrationDataStyle.h"
#include "ConnItemSelection.h"

namespace PixCon {

  ConnListItemFactory::ConnListItemFactory(QTreeWidget *list_view,
                       CalibrationData   &calibration_data,
                       DataSelection     &data_selection,
                       ConnItemSelection &item_selection,
                       std::map<std::string, ConnListItem *> &item_map)
    : m_listView(list_view),
      m_calibrationData(&calibration_data),
      m_dataSelection(&data_selection),
      m_itemSelection(&item_selection),
      m_itemMap(&item_map)
  {

    // memorise which items where open
    for (std::map<std::string, ConnListItem *>::const_iterator item_iter = m_itemMap->begin();
	 item_iter != m_itemMap->end();
	 item_iter++) {
      if (item_iter->second->isExpanded()) {
	m_openList.insert(item_iter->first);
      }
      else {
	std::set<std::string>::const_iterator iter = m_openList.find(item_iter->first);
	if (iter != m_openList.end()) {
	  m_openList.erase(iter);
	}
      }
    }

    m_itemMap->clear();
    m_listView->clear();

    unsigned int column_i=0;
    m_listView->setHeaderLabel("Name");
    m_listView->setHeaderLabel("Enabled");
    if (m_dataSelection->varId().isValid()) {
        m_listView->setHeaderLabel(QString::fromStdString(VarDefList::instance()->varName(m_dataSelection->varId().id())));
    }
    else {
      m_listView->setHeaderLabel("(nothing)");
    }

    for (std::vector<ValueDescription_t>::const_iterator var_iter = m_itemSelection->beginVar();
	  var_iter != m_itemSelection->endVar();
	  var_iter++, column_i++) {
      m_listView->setHeaderLabel(QString::fromStdString(VarDefList::instance()->varName( var_iter->id() )));
    }
  }


  ConnListItem *ConnListItemFactory::createItem(ConnListItem *parent_item, const std::string &connectivity_name, EConnItemType type)
  {

    QStringList connectivity_name_as_list;
    connectivity_name_as_list << QString::fromStdString(connectivity_name);

    ConnListItem *an_item = ( parent_item ? 
                  new ConnListItem( parent_item,connectivity_name_as_list, type)
                  : new ConnListItem( m_listView, connectivity_name_as_list,type));

    m_itemMap->insert( std::make_pair(connectivity_name, an_item) );

    if (!parent_item) {
        an_item->setExpanded(true);
    }
    else {
      std::set<std::string>::const_iterator iter = m_openList.find(connectivity_name);
      if (iter != m_openList.end()) {
          an_item->setExpanded(true);
      }
    }
     unsigned int column_i=1;
     try {
       CalibrationData::ConnObjDataList data( m_calibrationData->getConnObjectData(connectivity_name) );
       if (m_dataSelection->enableState(data).enabled()) {
	 an_item->setText(column_i,"yes");
	 if (parent_item && type == kModuleItem && parent_item->type()==kPp0Item) {
	   if (parent_item->text(column_i) != "yes") {
	     parent_item->setText(column_i,"yes");
	   }
	 }
	 column_i++;
       }
       else {
	 an_item->setText(column_i++,"-");
       }
       if (m_dataSelection->varId().isValid()) {
	 try {
	   setText(*m_dataSelection, data, an_item, column_i, ValueDescription_t(m_dataSelection->varId()) );
	 }
	 catch (CalibrationData::MissingValue &err) {
	   an_item->setText(column_i,"");
	 }
       }
       column_i++;

       for (std::vector<ValueDescription_t>::const_iterator var_iter = m_itemSelection->beginVar();
	    var_iter != m_itemSelection->endVar();
	    var_iter++, column_i++) {

	 try {
	   setText(*m_dataSelection, data, an_item, column_i, *var_iter );
	 }
	 catch (CalibrationData::MissingValue &err) {
	   an_item->setText(column_i,"");
	 }

       }
     }
     catch (CalibrationData::MissingValue &err) {
     }
     return an_item;
  }

  void ConnListItemFactory::setEnable(DataSelection &data_selection,
				      CalibrationData::ConnObjDataList &data,
				      ConnListItem *an_item)
  {
    unsigned int column_i = 1;
    if (data_selection.enableState(data).enabled()) {
      if (an_item->text(column_i) != "yes") {
	an_item->setText(column_i,"yes");
	if (an_item->type() == kModuleItem ) {
      QTreeWidgetItem *parent = an_item->parent();
	  if (parent) {
	    ConnListItem *parent_conn_item = dynamic_cast<ConnListItem *>(parent);
	    if (parent_conn_item && parent_conn_item->type()==kPp0Item) {
	      if (parent_conn_item->text(column_i) != "yes") {
		parent_conn_item->setText(column_i,"yes");
	      }
	    }
	  }
	}
      }
    }
    else {
      if (an_item->text(column_i)!="-") {
	an_item->setText(column_i,"-");
	if (an_item->type() == kModuleItem) {
      QTreeWidgetItem *parent = an_item->parent();
	  if (parent) {
	    ConnListItem *parent_conn_item = dynamic_cast<ConnListItem *>(parent);
	    if (parent_conn_item && parent_conn_item->type()==kPp0Item) {
	      if (parent_conn_item->text(column_i) == "yes" || parent_conn_item->text(column_i) == "(yes)") {
		bool has_enabled_modules=false;
        QTreeWidgetItemIterator child(parent_conn_item);
        while ((*child)){
          ConnListItem *child_conn_item = dynamic_cast<ConnListItem *>((*child));
		  if (child_conn_item->text(column_i) == "yes") {
		    has_enabled_modules=true;
		    break;
		  }
            ++child;
		}
		if (!has_enabled_modules) {
		  parent_conn_item->setText(column_i,"");
		}
	      }
	    }
	  }
	}
      }
    }
  }

  void ConnListItemFactory::setText(DataSelection &data_selection,
				    CalibrationData::ConnObjDataList &data,
				    ConnListItem *an_item,
				    unsigned int column_i,
				    const ValueDescription_t &value_description)
  {
    try {
    if (value_description.varDef().isValueWithOrWithoutHistory() ) {
      std::stringstream text;
      float a_value;
      // @todo history
      if (value_description.isFixedMeasurment()) {
	a_value = data.newestValue<float>(value_description.measurementType(),
					  value_description.serialNumber(),
					  value_description.varDef() );
      }
      else {
	a_value = ( data_selection.newestValue<float>(data, value_description.varDef() ));
      }
      text << a_value;
      an_item->setText(column_i, text.str().c_str() );
    }
    else if (value_description.varDef().isStateWithOrWithoutHistory() ) {
      // @to be implemented;
      // @todo history
      //State_t state;
      if (value_description.isFixedMeasurment()) {
	  //state =
	          (void)data.newestValue<State_t>(value_description.measurementType(),
					    value_description.serialNumber(),
					    value_description.varDef() );
      }
      else {
	State_t state = data_selection.newestValue<State_t>( data, value_description.varDef() );
	if (value_description.varDef().stateDef().isValidState( state ) ) {
	  an_item->setText(column_i, QString::fromStdString(value_description.varDef().stateDef().stateName( state )  ));
	}
	else {
	  an_item->setText(column_i, "invalid" );
	}
      }
    }
    }
    catch(...) {
      an_item->setText(column_i, "" );
    }

  }

}
