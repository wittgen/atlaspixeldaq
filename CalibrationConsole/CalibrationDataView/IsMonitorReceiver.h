/* Dear emacs, this is -*-c++-*- */
#ifndef _PixCon_IsMonitorReceiver_h_
#define _PixCon_IsMonitorReceiver_h_

#include "IsMonitorBase.h"
#include "LossyRingFifo_t.h"

class ISCallbackInfo;

#include <time.h>
#include <cassert>

namespace PixCon {
  class IVarNameExtractor;

  template <class InfoSrc_t, class Src_t, class SrcRef_t>
  class IsMonitorReceiver;

  /** Container to transport IS information from the callback to the application
   * The base container contains the name, the time stamp and a unique ID which is
   * used to identify dropped information. The latter may happen if information
   * come in at a higher speed than the speed at which they are processed.
   *<p>
   * Usage:
   * <verb>
   * write:
   *   only use if (!info_delay.isLocked())
   *   then info_delay.fill();
   *   then change contents
   *   then info_delay.valid();
   * read:
   *   only use if (info_delay.isValid() && info_delay.lock())
   *   then use contents
   *   finally when done info_delay.done()
   * </verb>
   */
  class InfoBufferBase
  {
  public:
    enum EStatus { kValid, kLocked, kEmpty, kEmpty2, kBeingRefilled, kBeingRefilled2 };

    InfoBufferBase() : m_id(0),m_status(kEmpty) {}
    InfoBufferBase(unsigned int id, const std::string &name) : m_id(id), m_name(name), m_status(kEmpty),m_deleted(false) {}

    const std::string &name() { return m_name; }

    /** Get the IS information time stamp (seconds resolution).
     */
    ::time_t           time() { return m_timeStamp; }

    /** Serial number of this IS information packet
     */
    unsigned int id() const { return m_id;}

    /** Copy new IS information in this buffer.
     */
    void set(unsigned int id, const std::string &name, ::time_t time_stamp, bool deleted)  { m_id = id; m_name = name; m_timeStamp = time_stamp; m_deleted=deleted; }

    /** Is true in case this information packed is being processed by a @ref IsMonitorReceiver.
     * An info buffer which is locked is not overwritten with new information.
     */
    bool isLocked() const { return m_status == kLocked; }

    /** Is true in case the packed contains valid IS information.
     * Only packets with valid information should be processed.
     */
    bool isValid()  const { return m_status == kValid; }

    /** Lock this buffer.
     */
    bool lock() { return ( ++m_status == kLocked ); }

    /** Unlock this buffer.
     */
    void done()     { ++m_status; }

    /** Mark this buffer as beeing filled with new information.
     */
    void fill()     { m_status = kBeingRefilled; }

    /** Mark this buffer to contain valid IS information.
     */
    void setValid() { m_status = kValid; }

    /** Value was deleted.
    */
    bool valueWasDeleted() const { return m_deleted; }

  private:
    unsigned int   m_id;
    std::string    m_name;
    ::time_t       m_timeStamp;
    unsigned char  m_status;
    bool           m_deleted;
  };


  /** The IS information buffer for a particular type.
   */
  template <class T>
  class InfoBuffer : public InfoBufferBase
  {
  public:
    InfoBuffer() {}

    /** Copy new IS information to this buffer.
     */
    void set(unsigned int id, const std::string &name, ::time_t time_stamp, const T value, bool deleted) {
      InfoBufferBase::set(id,name,time_stamp, deleted);
      m_value=value;
    }

    /** Get the previously stored value from this buffer.
     */
    const T &value() const { return m_value; }

  private:
    T                     m_value;
  };



  /** The IS information buffer for a string.
   * The difference is just the passing by reference instead of passing by value (useful ?)
   */
  template <>
  class InfoBuffer<std::string> : public InfoBufferBase
  {
  public:
    InfoBuffer() {}

    /** Copy new IS information to this buffer.
     */
    void set(unsigned int id, const std::string &name, ::time_t time_stamp, const std::string &value, bool value_was_deleted) {
      InfoBufferBase::set(id,name,time_stamp, value_was_deleted);
      m_value=value;
    }

    /** Get the previously stored value from this buffer.
     */
    const std::string &value() const { return m_value; }

  private:
    std::string           m_value;
  };



  /** The basic receiver class for a particular type.
   * It receives the values from IS and fills the IS information
   * into to ring buffer which constains a predefined number of
   * IS information buffers (@ref InfoBuffer).
   */
  template <class InfoSrc_t, class Src_t>
  class IsMonitorReceiverBase : public IsMonitorBase
  {
  public:
    IsMonitorReceiverBase(std::shared_ptr<IsReceptor> receptor,
			  EValueConnType conn_type,
                          Src_t value_after_deletion,
			  const ISCriteria &criteria,
			  unsigned int buffer_size,
			  unsigned int max_time_between_updates=0);

    /** Function which gets called by the IS server in case information has changed.
     * The change is propagated after creating the IS helper object via
     * valueChanged(InfoSrc_t &is_value, const std::string &var_name);
     */
    virtual void valueChanged(ISCallbackInfo *info) = 0;

  protected:

    /** Construct ISInfo object that reread values via _updateLoop
     */
    virtual void _update() = 0;

    /** propagate
     */
    void bufferValue(const std::string &var_name, ::time_t time_stamp, InfoSrc_t &is_value, bool value_was_deleted);

    /** Read values from IS and subscribe.
     */
    void init();

  protected:
    LossyRingFifo_t< InfoBuffer<Src_t> > m_buffer;
    Src_t m_valueAfterDeletion;
  };


  /** The receiver class for a particular type.
   * Updates all information from IS or precesses the information
   * stored in the ring buffer.
   * In most cases the template areguments are to be chosen identical:
   * e.g. IsMonitorReceiver<int,int>
   * except for cases in which the value should be passed by reference 
   * e.g. string : IsMonitorReceiver<string,const string&>
   */
  template <class InfoSrc_t, class Src_t, class SrcRef_t >
  class IsMonitorReceiver : public IsMonitorReceiverBase<InfoSrc_t, Src_t>
  {
  public:
    /** Create an IsMonitorReceiver.*/
    IsMonitorReceiver(std::shared_ptr<IsReceptor> receptor,
		      EValueConnType conn_type,
                      SrcRef_t value_after_deletion,
		      const ISCriteria &criteria,
		      unsigned int buffer_size,
		      unsigned int max_time_between_updates=0)
      : IsMonitorReceiverBase<InfoSrc_t, Src_t>(receptor, conn_type, value_after_deletion, criteria, buffer_size,max_time_between_updates)
    {}

    /** Create an IsMonitorReceiver with default buffer size which is different for module and ROD values. 
     * The reconnect feature if no updates occure within a certain time will be disabled.
     */
    IsMonitorReceiver(std::shared_ptr<IsReceptor> receptor,
		      EValueConnType conn_type,
                      SrcRef_t value_after_deletion,
		      const ISCriteria &criteria)
      : IsMonitorReceiverBase<InfoSrc_t, Src_t>(receptor,
						conn_type,
                                                value_after_deletion,
						criteria,
						(conn_type==kRodValue ? 2*(132+15) 
						 : (conn_type==kModuleValue ? 2*(1744+224+24)
						    : (conn_type==kAllConnTypes ? (2*(1744+132+15)) : 500))) /* buffer size*/,
						0 /* max time between updates =0 -> disabled*/)
    {}

    virtual void setValue(const std::string &is_name, ::time_t time_stamp, SrcRef_t value) = 0;

    /** Processes some of the new IS information which are sotred in the ring buffer.
     * @return the method returns false in case there is still information to be processed
     *         i.e. it has to be called again, and true in case all IS information was processed.
     */
    bool processData();

  protected:
    /** Reread values from IS.
     */
    //    void _update();

    /** Reread the values using the given helper info object.
     */
    void _updateLoop(InfoSrc_t &is_value);

  };


  /** Receives values from IS and translates them into a desitnation type.
   * To create a new monitor several templates need to be manually instantiated :
   * <verb>
   * #include "IsMonitorReceiver.h"
   * #include "IsMonitorReceiver.ixx"
   * // ...
   * namespace PixCon {
   *    template class IsMonitor<a, a_ref, a_dest>;
   *    template class IsMonitorReceiver<a, a_ref>;
   *    template class IsMonitorReceiverBase<a>;
   *
   *  // example 1 :
   *    template class IsMonitor<double, double&, float>;
   *    template class IsMonitorReceiver<double, double&>;
   *    template class IsMonitorReceiverBase<double>;
   *
   *  // example 2 :
   *    template class IsMonitor<int, int, int>;
   *    template class IsMonitorReceiver<int, int>;
   *    template class IsMonitorReceiverBase<int>;
   * }
   * </verb>
   */
  template <class InfoSrc_t, class Src_t, class SrcRef_t, class Dest_t>
  class IsMonitor : public IsMonitorReceiver<InfoSrc_t, Src_t, SrcRef_t>
  {
  public:
    /** Create an IsMonitor with input output value conversion, with default buffer size and without a maximum tolerated time between updates.*/
    IsMonitor(std::shared_ptr<IsReceptor> &receptor,
	      EValueConnType conn_type,
              SrcRef_t value_after_deletion,
	      const IVarNameExtractor &extractor,
	      const ISCriteria &criteria)
      : IsMonitorReceiver<InfoSrc_t, Src_t,SrcRef_t>(receptor,conn_type, value_after_deletion, criteria),
	m_lastValue(0),
	m_varNameExtractor(&extractor)
    { assert( this->receptor().get() != NULL ); assert(m_varNameExtractor); }

    /** Create an IsMonitor with input output value conversion.*/
    IsMonitor(std::shared_ptr<IsReceptor> &receptor,
	      EValueConnType conn_type,
              SrcRef_t value_after_deletion,
	      const IVarNameExtractor &extractor,
	      const ISCriteria &criteria,
	      unsigned int buffer_size,
	      unsigned int max_time_between_updates=0)
      : IsMonitorReceiver<InfoSrc_t, Src_t,SrcRef_t>(receptor,conn_type, value_after_deletion, criteria, buffer_size, max_time_between_updates),
	m_lastValue(0),
	m_varNameExtractor(&extractor)
    { assert( this->receptor().get() != NULL ); assert(m_varNameExtractor); }

  protected:

    /** Translate the source IS type into a destination type and forward to the receptor.
     */
    void setValue(const std::string &is_name, ::time_t time_stamp, SrcRef_t value);

    void _checkCleanup(const std::string &var_name, std::set<std::string> &cleaned_up_var_list);

    void registerForCleanup(const std::string &is_name, std::map<std::string, std::set<std::string> > &clean_up_var_list);

    Src_t m_lastValue;
    IVarNameExtractor const *m_varNameExtractor;
  };


}
#endif

