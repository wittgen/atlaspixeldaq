#ifndef _ICalibrationDataListener_h_
#define _ICalibrationDataListener_h_

#include <CalibrationDataTypes.h>
#include <VarDefList.h>
#include <string>

namespace PixCon {
  /** Interface of a class which listens for updates .
   * Example :
   * <verb>
   * class SimpleCalibrationDataListenerBase : public ICalibrationDataListener 
   * {
   *  public:
   *   SimpleCalibrationDataListenerBase() : m_needsUpdate(true) {}
   *
   *   bool needUpdate(EMeasurementType, SerialNumber_t, const std::string &, const std::string &) { m_needsUpdate=true;} 
   *   void update() { 
   *      if (m_needsUpdate) {
   *   	    m_needsUpdate=false;
   *	    //  do something
   *   }
   * };
   * </verb>
   * private:
   *  bool m_needsUpdate;
   * };
   */ 

  class ICalibrationDataListener {
  public:
    virtual ~ICalibrationDataListener() {};

    /** Use this function to memorise whether a listener needs to be updated or not.
     */
    virtual bool needUpdate(EMeasurementType measurement, SerialNumber_t serial_number, VarId_t var_id, const std::string &conn_name ) = 0;
    
    virtual void update() = 0;
  };

}
#endif
