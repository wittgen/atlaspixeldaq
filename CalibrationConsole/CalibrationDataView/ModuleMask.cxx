#include "ModuleMask.h"
#include <CalibrationDataManager.h>
#include <CalibrationDataStyle.h>
#include <CalibrationDataFunctorBase.h>
#include "ModuleList.h"
#include "MainPanel.h"
#include "DisablePanel.h"

#include <ConsoleBusyLoopFactory.h>

#include <QPainter>
#include <QInputDialog>
#include <QFileDialog>
#include <QClipboard>

#include <ConfigWrapper/Regex_t.h>

#include <DefaultColourCode.h>

#include <guessConnName.h>
#include <ConfigWrapper/PixDisableUtil.h>
#include <iomanip>

namespace PixCon {

  class ModuleMaskItem : public QTreeWidgetItem
  {
  public:
    enum ECombinationMode {kApply, kDisable, kEnable, kNModes};
    ModuleMaskItem(QTreeWidget *parent,
		   QStringList &/*ListofStrings*/,
		   const std::string &name,
		   unsigned int revision,
		   ECombinationMode mode,
		   QColor colour,
		   bool active)
      : QTreeWidgetItem(parent,{QString::fromStdString(name), QString::fromStdString(DisablePanel::makeTimeString(revision)),QString::fromStdString(activeName(active))},0),
	m_mode((mode < kNModes ? mode : kDisable) ),
	m_revision(revision),
	m_colour(colour),
	m_active(active)
    {
      setBackground(3, QBrush(m_colour));
    }

    ModuleMaskItem(QTreeWidgetItem *parent,
		   QStringList &/*ListofStrings*/,
		   const std::string &name,
		   unsigned int revision,
		   ECombinationMode mode,
		   QColor colour,
		   bool active)
      : QTreeWidgetItem(parent,{QString::fromStdString(name), QString::fromStdString(DisablePanel::makeTimeString(revision)),QString::fromStdString(activeName(active))},0),
	m_mode((mode < kNModes ? mode : kDisable) ),
	m_revision(revision),
	m_colour(colour),
	m_active(active)
    {
      setBackground(3, QBrush(m_colour));
    }

    std::string name() const      { return text(0).toLatin1().data(); }
    ECombinationMode mode() const { return m_mode;}
    unsigned int revision() const { return m_revision; }

    bool isActive() const         { return m_active; }

    void setActive(bool active)   { 
      if (m_active != active) {
	m_active=active;
	setText(2, activeName(m_active));
      }
    }

  private:
    static const char *modeName(ECombinationMode mode) {
      return s_modeName[(mode<kNModes ? mode : kNModes)];
    }

    static const char *activeName(bool active) {
      return (active ? "active" : "(Inactive)" );
    }

    ECombinationMode m_mode;
    unsigned int     m_revision;
    QColor           m_colour;
    bool             m_active;

    static QColor      s_inactive;
    static const char *s_modeName[];
  };

  QColor ModuleMaskItem::s_inactive(200,200,200);
  const char *ModuleMaskItem::s_modeName[]={"Enable / Disable","Disable only", "Enable only" ," Unknown"};

  namespace ModuleMaskGroup {
    static std::string name(QTreeWidgetItem *item) {
        return (item ? std::string(item->text(0).toLatin1().data()) : std::string());
    }
  }


  class CreateMaskOperator : public CalibrationDataFunctorBase
  {
  public:
    CreateMaskOperator(const std::string &mask_name, CalibrationData &calibration_data, State_t masked_out_value, bool also_set_on_state)
      : CalibrationDataFunctorBase(UINT_MAX & (~(1<<kPp0Item))),
	m_calibrationData(&calibration_data),
	m_maskVarDef( createVar(std::string("MASK_")+mask_name)),
	m_maskedOutValue(masked_out_value),
	m_alsoSetOnState(also_set_on_state)
    { 
    }

    static VarDef_t createVar(const std::string &full_mask_name)
    {
      try {
	const VarDef_t var_def( VarDefList::instance()->getVarDef(full_mask_name) );
	if (!var_def.isState() ) {
	  std::cerr << "ERROR [ModuleMask::createMaskVar] variable " << full_mask_name << " already exists but it is not a state variable." << std::endl;
	}
	return var_def;
      }
      catch (UndefinedCalibrationVariable &) {
	// @todo ensure that kAborted is the last state
	VarDef_t var_def( VarDefList::instance()->createStateVarDef( full_mask_name, kAllConnTypes, 2) );
	if ( var_def.isState() ) {
	  StateVarDef_t &state_var_def = var_def.stateDef();

	  state_var_def.addState(0,DefaultColourCode::kSuccess,"On");
	  state_var_def.addState(1,DefaultColourCode::kOff,"Masked");
	}
	else {
	  std::cerr << "ERROR [ModuleMask::createStateVarDef] variable " << full_mask_name << " already exists but it is not a state variable." << std::endl;
	}
	return var_def;
      }
    }

    void operator()( EConnItemType /*item_type*/,
		     const std::string &conn_name,
		     const std::pair<EMeasurementType,SerialNumber_t> &measurement,
		     PixCon::CalibrationData::ConnObjDataList &/*data_list*/,
		     CalibrationData::EnableState_t /*is_enable_state*/,
		     CalibrationData::EnableState_t new_enable_state) {
      if (!new_enable_state.enabled()  || m_alsoSetOnState) {
	// 	if (false) {
	// 	  std::cout << "INFO [CreateMaskOperator::operator()] set " << conn_name <<  " in  mask " << VarDefList::instance()->varName(m_maskVarDef.id())
	// 		    << " to " << new_enable_state.enabled() << std::endl;
	// 	}
	m_calibrationData->addValue<State_t>(conn_name,
					     measurement.first,
					     measurement.second, 
					     m_maskVarDef,
					     (new_enable_state.enabled() ? 0 : m_maskedOutValue) );
      }
      
      //      setEnableState(conn_name, measurement, new_enable_state);
    }

    void operator()( EConnItemType /*item_type*/,
		     const std::string &/*conn_name*/,
		     const std::pair<EMeasurementType,SerialNumber_t> &/*measurement*/,
		     CalibrationData::EnableState_t /*new_enable_state*/) {
      //      assert(PixCon::CalibrationData::checkMeasurement(measurement.first, measurement.second));
      //      data_list.addValue<Stat_t>(item_type, conn_name, measurement.first,measurement.second, var_iter->second, new_enable_state /* i.e. (new_enable_state ? 1 : 0)*/ );
      //      setEnableState(conn_name, measurement, new_enable_state);
    }

    //     void setEnableState(const std::string &conn_name,
    // 			const std::pair<EMeasurementType,SerialNumber_t> &measurement,
    // 			CalibrationData::EnableState_t new_enable_state) 
    //     {
    //       m_calibrationData->setEnableState(conn_name,
    // 					measurement.first,
    // 					measurement.second,
    // 					new_enable_state.isInConnectivity(),
    // 					new_enable_state.isInConfiguration(),
    // 					new_enable_state.isParentEnabled(),
    // 					new_enable_state.isTemporaryEnabled());
    //     }

  private:
    CalibrationData *m_calibrationData;
    VarDef_t         m_maskVarDef;

    State_t m_maskedOutValue;
    bool    m_alsoSetOnState;
    
  };

  const std::string ModuleMask::s_defaultMaskVarName("COMBINED");
  const std::string ModuleMask::s_maskHeaderName("MASK_");
  const std::string ModuleMask::s_maskCategory("Masks");

  ModuleMask::ModuleMask( const std::shared_ptr<CalibrationDataManager> &calibration_data,
			  const std::shared_ptr<PixCon::CalibrationDataPalette> &palette,
			  QApplication *app,
			  ModuleList *module_list,
			  MainPanel  *main_panel,
			  QWidget* parent )
: QWidget(parent),
      m_calibrationData(calibration_data),
      m_palette(palette),
      m_moduleList(module_list),
      m_mainPanel(main_panel),
      m_parent(NULL),
      m_selectedItem(NULL),
      m_prevSelectedItem( NULL),
      m_app(app),
      m_logLevel(0)
  {
    setupUi(this);
    m_deleteButton->setEnabled(false);
    m_increaseLevelButton->setEnabled(false);
    m_decreaseLevelButton->setEnabled(false);
    m_applyFilterButton->setEnabled(false);
    m_maskList->sortByColumn(-1);
    /*for (unsigned int column_i = m_maskList->columnCount(); column_i-->0; ) {
      m_maskList->removeColumn(column_i);
    }
    m_maskList->addColumn( tr( "Name" ) );
    m_maskList->addColumn( tr( "Revision" ) );
    m_maskList->addColumn( tr( "Active" ) );
    m_maskList->addColumn( tr( "Colour" ) );
*/ //Remove this, since alredy declared in the ui_ModuleMaskBase.h
    ModuleMask::createEmptyGroup( s_defaultMaskVarName );
    m_parent=findMaskGroup(s_defaultMaskVarName);
    PixA::ConnectivityRef conn(calibrationData()->currentConnectivity());
    if (conn) {
      addMask("CONNECTIVITY", conn.revision(), std::shared_ptr<PixLib::PixDisable>());
    }
    if (m_parent) {
      m_parent->setSelected(true);
    }
    //maskSelectionChanged();

    m_maskList->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(m_loadButton, SIGNAL(clicked()), this, SLOT(loadMask()));
    connect(m_deleteButton, SIGNAL(clicked()), this, SLOT(deleteSelectedMasks()));
    connect(m_increaseLevelButton, SIGNAL(clicked()), this, SLOT(increaseLevel()));
    connect(m_decreaseLevelButton, SIGNAL(clicked()), this, SLOT(decreaseLevel()));
    connect(m_applyFilterButton, SIGNAL(clicked()), this, SLOT(applyFilter()));
    connect(m_clearFilterButton, SIGNAL(clicked()), this, SLOT(clearFilter()));
    connect(m_maskList, SIGNAL(itemClicked(QTreeWidgetItem*,int)), this, SLOT(maskSelectionChanged()));
    connect(m_maskList, SIGNAL(itemDoubleClicked(QTreeWidgetItem*,int)), this, SLOT(activateDeactivate(QTreeWidgetItem*,int)));
    connect(m_maskList, SIGNAL(customContextMenuRequested (const QPoint &) ), this, SLOT(maskContextMenu(const QPoint&)));
  }

/*  void ModuleMask::createEmptyGroup(const std::string &mask_group_name)
  {
    VarDef_t var_def = createMaskVar( mask_group_name);
    Q3ListViewItem *item=new Q3ListViewItem(m_maskList, QString::fromStdString(mask_group_name) );
    item->setOpen(true);
    m_app->postEvent( m_mainPanel, new QNewVarEvent(kCurrent,0,s_maskCategory,s_maskHeaderName + mask_group_name ));
  }*/
  void ModuleMask::createEmptyGroup(const std::string &mask_group_name)
  {
    QStringList stringList;
    stringList << QString::fromStdString(mask_group_name);
    //VarDef_t var_def = 
    createMaskVar( mask_group_name);
    QTreeWidgetItem *item=new QTreeWidgetItem(m_maskList, stringList);
    item->setExpanded(true);
    m_app->postEvent( m_mainPanel, new QNewVarEvent(kCurrent,0,s_maskCategory,s_maskHeaderName + mask_group_name ));
  }

  ModuleMask::~ModuleMask() {}

  VarDef_t ModuleMask::createMaskVar(const std::string &combined_mask_name) {

    assert( combined_mask_name.compare(0,s_maskHeaderName.size(),s_maskHeaderName)!=0 );

    try {
      const VarDef_t var_def( VarDefList::instance()->getVarDef(s_maskHeaderName + combined_mask_name) );
      if ( !var_def.isState() ) {
	std::cerr << "ERROR [ModuleMask::createMaskVar] variable " << s_maskHeaderName + combined_mask_name
		  << " already exists but it is not a state variable." << std::endl;
      }
      return var_def;
    }
    catch (UndefinedCalibrationVariable &) {
      // @todo ensure that kAborted is the last state
      VarDef_t var_def( VarDefList::instance()->createStateVarDef( s_maskHeaderName + combined_mask_name, kAllConnTypes, 1) );
      if (var_def.isState() ) {
	StateVarDef_t &state_var_def = var_def.stateDef();

	state_var_def.addState(0,DefaultColourCode::kSuccess,"On");
      }
      else {
	std::cerr << "ERROR [ModuleMask::createStateVarDef] variable " << s_maskHeaderName + combined_mask_name 
		  << " already exists but it is not a state variable." << std::endl;
      }
      return var_def;
    }

  }

  void ModuleMask::loadMask() {

    if (!m_parent) {
      //Q3ListViewItem *an_item = findMaskGroup(s_defaultMaskVarName );
        QTreeWidgetItem *an_item = findMaskGroup(s_defaultMaskVarName );
      if (!an_item) {
	std::cerr << "ERROR [ModuleMask::loadMask] No mask group selected to add the disable to."<< std::endl;
	return;
      }
      m_parent = an_item;
    }

    PixA::ConnectivityRef conn(calibrationData()->currentConnectivity());
    if (conn) {

      DisablePanel disable_panel(conn, NULL);
      if (disable_panel.exec() == QDialog::Accepted) {

	DisableMap_t::iterator disable_iter = m_disableMap.find(disable_panel.name());
	if (disable_iter != m_disableMap.end()) {
	  DisableRevisionMap_t::const_iterator revision_iter = disable_iter->second.find(disable_panel.revision());
	  if (revision_iter != disable_iter->second.end()) {
	    if (m_logLevel>0) {
	      std::cout << "INFO [ModuleMask::loadMask] The disable is already loaded : " << disable_iter->first
			<< " ("  << DisablePanel::makeTimeString(revision_iter->first) << ")."<< std::endl;
	    }
	  return;
	  }
	}

	addMask(disable_panel.name(), disable_panel.revision(), disable_panel.getDisable());
      }
    }
  }
  
  void ModuleMask::addMask( const std::string &disable_name, unsigned int revision, const std::shared_ptr<const PixLib::PixDisable> &disable)
  {
    if (!disable_name.empty()) {

      DisableMap_t::iterator disable_iter = m_disableMap.find(disable_name);
      if (disable_iter != m_disableMap.end()) {
	DisableRevisionMap_t::const_iterator revision_iter = disable_iter->second.find(revision);
	if (revision_iter != disable_iter->second.end()) {
	    if (m_logLevel>0) {
	      std::cout << "INFO [ModuleMask::addMask] The disable exists already : " << disable_iter->first
			<< " ("  << DisablePanel::makeTimeString(revision_iter->first) << ")."<< std::endl;
	    }
	  return;
	}
      }
      else {
	std::pair<DisableMap_t::iterator, bool> ret = m_disableMap.insert( std::make_pair(disable_name, DisableRevisionMap_t()));
	if (!ret.second) {
	  std::cerr << "ERROR [ModuleMask::addMask] Failed to add : " << disable_name << "."<< std::endl;
	  return;
	}
	disable_iter = ret.first;
      }

      std::stringstream versioned_name;
      versioned_name << disable_name;
      if (disable_iter->second.size()>0) {
	versioned_name << ";" << disable_iter->second.size();
      }
      disable_iter->second.insert(std::make_pair(revision, std::make_pair(disable, versioned_name.str())));
      if (!m_parent) {
    //Q3ListViewItem *an_item = findMaskGroup(s_defaultMaskVarName );
      QTreeWidgetItem *an_item = findMaskGroup(s_defaultMaskVarName );

	if (!an_item) {
	  std::cerr << "ERROR [ModuleMask::loadMask] No mask group selected to add " << disable_name << ";" << revision << " to."<< std::endl;
	}
	m_parent = an_item;
      }
      addMaskItem(m_parent, versioned_name.str(), revision);
      createMaskVariable(versioned_name.str(), disable);
      m_app->postEvent( m_mainPanel, new QNewVarEvent(kCurrent,0,s_maskCategory,s_maskHeaderName + versioned_name.str() ));
      createCombinedMaskVariable( m_parent );
    }
  }

  void ModuleMask::addMaskItem(QTreeWidgetItem *parent, const std::string &name, unsigned int revision) {
    if (!parent) return;

    QTreeWidgetItem *child=parent->child(0);
    QTreeWidgetItemIterator it(parent);
    ModuleMaskItem::ECombinationMode mode = ModuleMaskItem::kDisable;
    while (*it){
      if (!*it) {
	mode = ModuleMaskItem::kApply;
      }
      else {
	//Q3ListViewItem *next_child;
	//while ((next_child = child->nextSibling())) {
	//child = next_child;
        child = *it;
      }
      ++it;
    }

    State_t state = maskState( ModuleMaskGroup::name(parent), name ).second;
    std::pair<QBrush , QPen > state_brush_and_pen = m_palette->brushAndPen(true,1, 0, static_cast<VarId_t>(-1), state );
    QColor  state_colour = state_brush_and_pen.first.color();
    QStringList ListofStrings;//
    //ModuleMaskItem::ECombinationMode mode;
    ModuleMaskItem *new_disable = new ModuleMaskItem(parent, ListofStrings, name,revision, mode, state_colour,true);
    //new_disable
    if (child) {
      //new_disable->moveItem(child);
      new_disable->addChild(child);
    }
    //if (!parent->isOpen()) {
      if (!parent->isExpanded()) {
      //parent->setOpen(true);
        parent->setExpanded(true);
    }

    if (!m_applyFilterButton->isEnabled()) {
      m_applyFilterButton->setEnabled(true);
    }
  }


  std::pair<State_t,State_t> ModuleMask::maskState(const std::string &name)
  {
    if (!m_parent) {
      std::stringstream message;
      message << "ERROR [ModuleMask::maskState] No mask group selected to add mask  " << name << " to." ;
      throw std::runtime_error(message.str());
    }
    return maskState( ModuleMaskGroup::name(m_parent) ,name);
  }

  std::pair<State_t,State_t> ModuleMask::maskState(const std::string &combined_mask_name,  const std::string &name)
  {
    VarDef_t var_def( VarDefList::instance()->getVarDef( s_maskHeaderName + combined_mask_name ) );
    if ( !var_def.isState() ) {
      std::stringstream message ;
      message << "ERROR [ModuleMask::maskState] Variable " << s_maskHeaderName + combined_mask_name<< " but it is not a state.";
      throw std::runtime_error(message.str());
    }
    StateVarDef_t &state_var_def = var_def.stateDef();
    unsigned int first_free_state = state_var_def.nStates();
    std::set<State_t> colours;
    colours.insert(DefaultColourCode::kSuccess);
    for (unsigned int state_i=0; state_i < state_var_def.nStates(); state_i++) {
      if (state_var_def.stateName(state_i)==name) {
	if (m_logLevel>2) std::cout << "INFO [ModuleMask::maskState] " << combined_mask_name << " / " << name 
		  << " -> " << state_i <<  " -> " << state_var_def.remapState(state_i) << std::endl;
	return std::make_pair(state_i,state_var_def.remapState(state_i));
      }
      else if (state_var_def.stateName(state_i)=="(deleted)") {
	if (first_free_state == state_var_def.nStates()) {
	  first_free_state = state_i;
	}
      }
      else {
	colours.insert(state_var_def.remapState(state_i));
      }
    }

    State_t first_free_colour=1;
    std::set<State_t>::const_iterator colour_iter=colours.begin();
    if (m_logLevel>2) std::cout << "INFO [ModuleMask::maskState] ";
    while (colour_iter != colours.end() && first_free_colour==*colour_iter) {
      if (m_logLevel>2) std::cout << *colour_iter << "/" << first_free_colour << " ";
      colour_iter++;
      first_free_colour++;
    }
    if (m_logLevel>2) std::cout << " -> (" << *colour_iter << ") " << first_free_colour << std::endl;

    //    State_t maped_colour = first_free_colour;
    //     if (maped_colour>=DefaultColourCode::kSuccess) {
    //       maped_colour++;
    //     }
    state_var_def.addState(first_free_state, first_free_colour, name);

    if (m_logLevel>2) {
      std::cout << "INFO [ModuleMask::maskState] new " << combined_mask_name << " / " << name 
		<< " -> " << first_free_state <<  " -> " << state_var_def.remapState(first_free_state) << std::endl;
    }

    return std::make_pair(first_free_state,first_free_colour);
  }

  void ModuleMask::reorderStates() {
    reorderStates(m_parent);
  }

  void ModuleMask::reorderStates(QTreeWidgetItem *parent) {

    if (!parent) return;

    try {

      const std::string combined_mask_name = ModuleMaskGroup::name(parent);
      VarDef_t var_def( VarDefList::instance()->getVarDef( s_maskHeaderName + combined_mask_name)  );
      if (!var_def.isState() ) {
	std::cerr << "ERROR [ModuleMask::reorderStates] Variable " << s_maskHeaderName + combined_mask_name << " but it is not a state."  << std::endl;
	return;
      }
      StateVarDef_t &state_var_def = var_def.stateDef();
      
      std::vector< std::pair<std::string, State_t> > state_list;
      state_list.push_back(std::make_pair(state_var_def.stateName(0), state_var_def.remapState(0)));
      state_list.reserve(state_var_def.nStates());
      
      for (int i = 0; i < parent->childCount();i++){
	QTreeWidgetItem *child=parent->child(i);
	ModuleMaskItem *mask_item =dynamic_cast<ModuleMaskItem *>(child);
	if (mask_item) {
	  std::string mask_name = mask_item->name();
	  if (m_logLevel>2) {
	    std::cout << "INFO [ModuleMask::reorderStates] " << mask_name << " " << static_cast<unsigned int>(state_var_def.state(mask_name))
		      << " -> "  <<  static_cast<unsigned int>(state_var_def.remapState(state_var_def.state(mask_name))) << std::endl;
	  }
	  state_list.push_back( std::make_pair(mask_name, state_var_def.remapState(state_var_def.state(mask_name))));
	}
      }
      
      State_t state_i1=0;
      state_var_def.reset();
      for (std::vector< std::pair<std::string, State_t> >::const_iterator state_iter = state_list.begin();
	   state_iter != state_list.end();
	   ++state_iter, ++state_i1) {
	state_var_def.addState(state_i1,state_iter->second, state_iter->first);
	if (m_logLevel>2) {
	  std::cout << "INFO [ModuleMask::reorderStates] add " << state_iter->first << " " << state_i1 << " -> " << state_iter->second
		    << " : " << state_var_def.stateName(state_i1) << " -> " << state_var_def.remapState(state_i1) << std::endl;
	}
	
	for(unsigned int empty_state_i=state_i1;
	    empty_state_i<state_var_def.nStates();
	    ++empty_state_i) {
	  state_var_def.addState(empty_state_i,0, "(deleted)");
	  if (m_logLevel>2) {
	    std::cout << "INFO [ModuleMask::reorderStates] delete " << " " << empty_state_i << " -> " << 0
		      << " : (deleted) " << std::endl;
	  }
	}
      }
      
      // debug
      if (m_logLevel>1) {
	for (State_t state_i2=0; state_i2 < state_var_def.nStates(); state_i2++) {
	  std::cout << "INFO [ModuleMask::reorderStates]  " << std::setw(2) << state_i2
		    << " -> " << std::setw(2) << state_var_def.remapState(state_i2) 
		    << " : " << state_var_def.stateName(state_i2)
		    << " -> " << state_var_def.state( state_var_def.stateName(state_i2) )
		    <<  std::endl;
	}
      }
      m_app->postEvent( m_mainPanel, new QNewVarEvent(kCurrent,0,s_maskCategory,s_maskHeaderName + combined_mask_name ));

    }
    catch(CalibrationDataException &err) {
      std::cerr << "ERROR [ModuleMask::reorderStates] Caught exception : " << err.what()  << std::endl;
    }
    catch(...) {
      std::cerr << "ERROR [ModuleMask::reorderStates] Caught unknown exception "  << std::endl;
    }
  }
/*
  Q3ListViewItem *ModuleMask::findMaskGroup(const std::string &name) {
    Q3ListViewItem *parent=m_maskList->firstChild();
    while (parent) {
      Q3ListViewItem *next = parent->nextSibling();
      if (m_logLevel>2) {
	std::cout << "INFO [ModuleMask::findMaskGroup] " << name << " == " << ModuleMaskGroup::name(parent) << std::endl;
      }
      if (name==ModuleMaskGroup::name(parent)) {
	return parent;
      }
      parent=next;
    }
    return NULL;
  }
*/ //Porting here
  QTreeWidgetItem *ModuleMask::findMaskGroup(const std::string &name) {
      QTreeWidgetItemIterator it(m_maskList);
      while (*it){
          if (m_logLevel>2) {
        std::cout << "INFO [ModuleMask::findMaskGroup] " << name << " == " << ModuleMaskGroup::name(*it) << std::endl;
          }
          if (name==ModuleMaskGroup::name(*it)) {
        return *it;
          }

      ++it;
  }
      return NULL;

  }


  void ModuleMask::deleteSelectedMasks() {

    QTreeWidgetItem *parent=m_maskList->topLevelItem(0);
    for (int i = 0; i<m_maskList->topLevelItemCount(); i++){
    while (parent) {
      //Q3ListViewItem *child=parent->firstChild();
        QTreeWidgetItem *child=parent->child(0);
      bool delete_all_children=false;
      if (parent->isSelected()) {
	delete_all_children=true;
      }

      std::string combined_mask_name_str( ModuleMaskGroup::name(parent) );
      VarDef_t mask_var_def( VarDefList::instance()->getVarDef(s_maskHeaderName + combined_mask_name_str) );
      if (mask_var_def.isValid() &&mask_var_def.isState() ) {
    QTreeWidgetItemIterator it(parent);
    while (*it){
      //Q3ListViewItem *next = child->nextSibling();
	  if (child->isSelected() || delete_all_children) {
	    ModuleMaskItem *mask_item =dynamic_cast<ModuleMaskItem *>(child);
	    if (mask_item) {

	      StateVarDef_t &state_var_def = mask_var_def.stateDef();
	      std::string child_name = mask_item->name();
	      State_t state_i = maskState( combined_mask_name_str, child_name ).first;
	      if (state_i < state_var_def.nStates()) {
		state_var_def.addState(state_i,0,"(deleted)");
	      }
	      std::string::size_type pos = child_name.rfind(";");
	      if (pos != std::string::npos) {
		child_name.erase(pos,child_name.size()-pos);
	      }
	      DisableMap_t::iterator rev_list_iter = m_disableMap.find(child_name);
	      if (rev_list_iter!=m_disableMap.end()) {
		DisableRevisionMap_t::iterator disable_iter = rev_list_iter->second.find(mask_item->revision());
		if (disable_iter != rev_list_iter->second.end()) {
		  rev_list_iter->second.erase(disable_iter);
		  if (rev_list_iter->second.empty()) {
		    m_disableMap.erase(rev_list_iter);
		  }
		}
		else {
		  std::cerr << "WARNING [ModuleMask::deleteSelectedMasks] No disable to delete for : " << rev_list_iter->first << ";"
			    << mask_item->revision() << "."  << std::endl;
		}
	      }
	      else {
		std::cerr << "WARNING [ModuleMask::deleteSelectedMasks] No disable to delete for : " << mask_item->name() << "."  << std::endl;
	      }
	    }
	    delete mask_item;
	  }
      //child = next;
      ++it;
      child = *it;
   }
    //Q3ListViewItem *last_parent =parent;
    QTreeWidgetItem *last_parent =parent;
    //parent = parent->nextSibling();
    parent = m_maskList->topLevelItem(i+1);
	if (delete_all_children) {
	  std::string combined_mask_name( ModuleMaskGroup::name(last_parent) );
	  if (combined_mask_name != s_defaultMaskVarName) {
	    m_calibrationData->calibrationData().removeVariable<State_t>(kCurrent,0,mask_var_def.id());
	    delete last_parent;
	  }
	  else {
	    CreateMaskOperator create_mask(combined_mask_name, m_calibrationData->calibrationData(), 0 , true);
	    m_calibrationData->forEach(PixCon::kCurrent,0, create_mask, false /* create missing conn object container*/ );
	    m_app->postEvent( m_mainPanel, new QNewVarEvent(kCurrent,0,s_maskCategory,s_maskHeaderName + combined_mask_name ));
	  }
	}
	else {
	  reorderStates(last_parent);
	  createCombinedMaskVariable(last_parent);
	}
}
      else {
	std::cerr << "WARNING [ModuleMask::deleteSelectedMasks] No state variable for mask group  : " << combined_mask_name_str << "."  << std::endl;
      }

    }
  }
  }

  void ModuleMask::increaseLevel() {
    if (m_parent && m_selectedItem && m_prevSelectedItem) {

      int index = m_parent->indexOfChild(m_selectedItem);
      if (m_logLevel>2) {
	std::cout << "INFO [ModuleMask::increaseLevel] swapping items no. " << index << ", " << std::string(m_selectedItem->text(0).toLatin1().data())
		  << ", with " << (m_prevSelectedItem?std::string(m_prevSelectedItem->text(0).toLatin1().data()):"NULL")
		  << std::endl;
	
      }

      m_parent->takeChild(index);
      m_parent->takeChild(index-1);
      m_parent->insertChild(index-1,m_selectedItem);
      m_parent->insertChild(index,m_prevSelectedItem);
      maskSelectionChanged();
      reorderStates();
      createCombinedMaskVariable(m_parent);
    }
  }

  void ModuleMask::decreaseLevel() {
    if (m_parent && m_selectedItem) {
      int index = m_parent->indexOfChild(m_selectedItem);
      if (m_parent->child(index+1)){
	if (m_logLevel>2) {
	  std::cout << "INFO [ModuleMask::decreaseLevel] swapping items no. " << index << ", " << std::string(m_selectedItem->text(0).toLatin1().data())
		    << ", with " << (m_parent->child(index+1)?std::string(m_parent->child(index+1)->text(0).toLatin1().data()):"NULL")
		    << std::endl;
	}
	
	QTreeWidgetItem *itBelow = m_parent->takeChild(index+1);
	m_parent->takeChild(index);
	m_parent->insertChild(index,itBelow);
	m_parent->insertChild(index+1,m_selectedItem);
	maskSelectionChanged();
	reorderStates();
	createCombinedMaskVariable(m_parent);
      }
    }
  }

  void ModuleMask::maskSelectionChanged() {
    ModuleMaskItem *last_selection = m_selectedItem;
    if (m_nSelectedItems!=1) {
      last_selection=NULL;
    }
    m_prevSelectedItem=NULL;
    m_nSelectedItems=0;
    m_parent=NULL;
    unsigned int n_parents=0;
    for(int ip=0;ip<m_maskList->topLevelItemCount(); ip++){
      QTreeWidgetItem *parent = m_maskList->topLevelItem(ip);
      if (parent->isSelected()) {
	m_parent=parent;
	n_parents++;
      }
      ModuleMaskItem *last_item =NULL;
      for(int ic=0;ic<parent->childCount();ic++){
	QTreeWidgetItem *child = parent->child(ic);
	ModuleMaskItem *mask_item =dynamic_cast<ModuleMaskItem *>(child);
	if (mask_item) {
	  if (mask_item->isSelected()) {
	    if (m_parent!=NULL && m_parent != parent) {
	      m_nSelectedItems++;
	      n_parents++;
	    }
	    m_parent=parent;
	    m_nSelectedItems++;
	    m_selectedItem = mask_item;
	    m_prevSelectedItem=last_item;
	  }
	  last_item=mask_item;
	}
      }
    }

    if (m_logLevel>2) {
      std::cout << "INFO [ModuleMask::maskSelectionChanged] selected = " << m_nSelectedItems << " parents=" << n_parents
		<< " selected=" << static_cast<void *>(m_selectedItem);
      if(m_selectedItem!=0) std::cout << " ("<< std::string(m_selectedItem->text(0).toLatin1().data()) <<") ";
      std::cout << " prev= " << static_cast<void *>(m_prevSelectedItem);
      if(m_prevSelectedItem!=0) std::cout << " ("<< std::string(m_prevSelectedItem->text(0).toLatin1().data()) <<") ";
      std::cout << " parent= " << static_cast<void *>(m_parent);
      if(m_parent!=0) std::cout << " ("<< std::string(m_parent->text(0).toLatin1().data()) <<") ";
      std::cout << std::endl;
    }

    if (m_nSelectedItems==1 && last_selection != m_selectedItem) {
      emit changeVariable(QString::fromStdString(s_maskCategory), QString::fromStdString(s_maskHeaderName) + QString::fromStdString(m_selectedItem->name()));
      emit selectSerialNumber(kCurrent,0);
    }
    else if (m_nSelectedItems==0 && n_parents==1 && m_parent && m_parent->isSelected()) {
      emit changeVariable(QString::fromStdString(s_maskCategory), QString::fromStdString(s_maskHeaderName) + QString::fromStdString(ModuleMaskGroup::name(m_parent)) );
      emit selectSerialNumber(kCurrent,0);
    }

    bool enable_delete_button=m_nSelectedItems>0 || n_parents>0;
    if (enable_delete_button != m_deleteButton->isEnabled()) {
      m_deleteButton->setEnabled(enable_delete_button);
    }

    bool enable_load_button=(m_nSelectedItems==0 && m_parent!=0);
    if (enable_load_button != m_loadButton->isEnabled()) {
      m_loadButton->setEnabled(enable_load_button);
    }
    
    bool enable_increase_button = (m_nSelectedItems==1 && m_selectedItem && m_parent && m_prevSelectedItem);
    if (enable_increase_button != m_increaseLevelButton->isEnabled()) {
      m_increaseLevelButton->setEnabled(enable_increase_button);
    }
    int index = -1;
    if(m_parent!=0) index = m_parent->indexOfChild(m_selectedItem);
    bool enable_decrease_button = (m_nSelectedItems==1 && m_parent && m_selectedItem && index>=0 && m_parent->child(index+1));
    if (enable_decrease_button != m_decreaseLevelButton->isEnabled()) {
      m_decreaseLevelButton->setEnabled(enable_decrease_button);
    }

  }

  void ModuleMask::activateDeactivate(QTreeWidgetItem *item, int /*column*/) {
    if (m_parent) {
    ModuleMaskItem *mask_item =dynamic_cast<ModuleMaskItem *>(item);
    if (mask_item) {
      bool activate = !mask_item->isActive();
      if (m_logLevel>2) {
	std::cout << "INFO [ModuleMask::activateDeactivate] "<< activate << std::endl;
      }
      mask_item->setActive( activate );
      //      reorderStates();
      createCombinedMaskVariable(m_parent);
    }
    }
  }

  void ModuleMask::createMaskVariable(const std::string &mask_name, const std::shared_ptr<const PixLib::PixDisable> &a_pix_disable) {
    Lock lock(m_calibrationData->calibrationDataMutex());
    {
      CreateMaskOperator create_mask(mask_name, m_calibrationData->calibrationData(),1,true);
      m_calibrationData->forEach(PixCon::kCurrent,0, create_mask, false /* create missing conn object container*/ );
    }
    {
      CreateMaskOperator create_mask(mask_name, m_calibrationData->calibrationData(),1,true);
      m_calibrationData->forEach(PixCon::kCurrent,0, PixA::DisabledListRoot(a_pix_disable), create_mask, false /* create missing conn object container*/ );
    }
  }

  void ModuleMask::createCombinedMaskVariable(const std::string &combined_mask_name)
  {
    createCombinedMaskVariable( findMaskGroup(combined_mask_name) );
  }

  void ModuleMask::createCombinedMaskVariable(/*Q3ListViewItem*/QTreeWidgetItem *parent) {
    if (parent) {
      //Q3ListViewItem *child = parent->firstChild();
      bool first = true;
      std::string combined_mask_name = ModuleMaskGroup::name(parent);

      std::vector<ModuleMaskItem *> mask_list;
      QTreeWidgetItemIterator itr(parent);
      while (*itr) {
    ModuleMaskItem *mask_item =dynamic_cast<ModuleMaskItem *>((*itr));
	if (mask_item && mask_item->isActive()) {
	  if (m_logLevel>2) {
	    std::cout << "INFO [ModuleMask::createCombinedMaskVariable] child " << mask_item->name() << std::endl;
	  }
	  mask_list.push_back(mask_item);
	}
    //child = child->nextSibling();
    ++itr;
      }

      for (std::vector<ModuleMaskItem *>::reverse_iterator mask_iter = mask_list.rbegin();
	   mask_iter != mask_list.rend();
	   ++mask_iter) {
	ModuleMaskItem *mask_item = *mask_iter;

	std::string a_name = mask_item->name();
	if (a_name.empty()) continue;
	Lock lock(m_calibrationData->calibrationDataMutex());
	State_t mask_state = maskState( combined_mask_name , a_name).first;
	if (m_logLevel>2) {
	  std::cout << "INFO [createCombinedMaskVariable] " << combined_mask_name << " / " << a_name << " -> " << mask_state << std::endl;
	}
	CreateMaskOperator create_mask(combined_mask_name, m_calibrationData->calibrationData(), mask_state , first);

	std::string::size_type pos = a_name.rfind(";");
	if (pos != std::string::npos) {
	  a_name.erase(pos,a_name.size()-pos);
	}

	DisableMap_t::iterator rev_list_iter = m_disableMap.find(a_name);
	if (rev_list_iter!=m_disableMap.end()) {
	  DisableRevisionMap_t::iterator disable_iter = rev_list_iter->second.find(mask_item->revision());
	  if (disable_iter != rev_list_iter->second.end()) {

	    m_calibrationData->forEach(PixCon::kCurrent,0, PixA::DisabledListRoot(disable_iter->second.first), create_mask, false /* create missing conn object container*/ );
	    first=false;

	  }
	  else {
	    std::cerr << "WARNING [ModuleMask::deleteSelectedMasks] No disable for : " << rev_list_iter->first << ";"
		      << mask_item->revision() << "."  << std::endl;
	  }
	}
	else {
	  std::cerr << "WARNING [ModuleMask::deleteSelectedMasks] No disable for : " << mask_item->name() << "."  << std::endl;
	}

      }
      m_app->postEvent( m_mainPanel, new QNewVarEvent(kCurrent,0,s_maskCategory,s_maskHeaderName + combined_mask_name ));

    }
    else {
      std::cerr << "ERROR [ModuleMask::createStateVarDef] No combined selected." << std::endl;
    }

  }

  void ModuleMask::applyFilter() {
    applyFilter(m_filterDisabledButton->isChecked());
  }

  void ModuleMask::loadTextFile() {
    QStringList filter;
    filter +=  "Text File (*.txt)";
    filter += "Any file (*.*)";

    //Q3FileDialog file_chooser(m_lastPath, QString::null, this,"Select RootDb data file.",TRUE);
    QFileDialog file_chooser(this, "Select output file for variables.", m_lastPath, QString::null);
    file_chooser.setFileMode(QFileDialog::ExistingFiles);
    file_chooser.setNameFilters(filter);
    if(file_chooser.exec() == QDialog::Accepted) {
      QStringList file_list = file_chooser.selectedFiles();
      for (QStringList::const_iterator  file_iter = file_list.begin();
	   file_iter != file_list.end();
	   ++file_iter) {
	createMaskFromTextFile((*file_iter).toLatin1().data());
      }

    }

    if (file_chooser.directory().exists()) {
      m_lastPath=file_chooser.directory().path();
    }

  }

  void ModuleMask::createMaskFromTextFile(const std::string &file_name)
  {
    if (file_name.empty()) return;
    std::string::size_type ext_pos = file_name.rfind(".");
    if (ext_pos==std::string::npos) {
      ext_pos = file_name.size();
    }
    std::string::size_type path_pos = file_name.rfind("/");
    if (ext_pos==std::string::npos) {
      path_pos=0;
    }
    else {
      path_pos++;
    }
    if (ext_pos>path_pos) {
      std::string name = file_name.substr(path_pos, ext_pos-path_pos);
      std::ifstream in(file_name.c_str());
      createMaskFromText(in,name);
    }
    else {
      std::cerr << "ERROR [ModuleMaske::createMaskFromTextFile] Failed to extract mask_name from file name \"" << file_name << "\"." << std::endl;
    }
  }

  void ModuleMask::createMaskFromTextSelection()
  {
    QClipboard *cb = QApplication::clipboard();
    QString selection = cb->text(QClipboard::Selection);

    bool ok;
    QString text = QInputDialog::getText(this, "Create Mask From Selection", "Mask Name:", QLineEdit::Normal,
					 QString::null, &ok );
    if ( ok && !text.isEmpty() ) {
      if (selection.isEmpty()) {
	selection = cb->text(QClipboard::Selection);
      }
      std::stringstream text_stream;
      text_stream << selection.toLatin1().data();
      createMaskFromText(text_stream, text.toLatin1().data());
    }
  }

  void ModuleMask::createMaskFromFilter()
  {
    bool ok;
    QString text = QInputDialog::getText(this, "Create Mask From Filter:", "Mask Name:", QLineEdit::Normal,
					 QString::null, &ok );
    if ( ok && !text.isEmpty() ) {
      std::unique_ptr<ISelectionFactory> selection_factory(m_mainPanel->createSelectionFactory());

      std::unique_ptr<IConnItemSelection> selection(selection_factory->selection(true,false));
      std::shared_ptr<PixLib::PixDisable> pix_disable ( m_calibrationData->getPixDisable(kCurrent,
											   0,
											   *selection,
											   true /*ignore tim.*/,
											   true /*ignore enable.*/) );
      addMask(text.toLatin1().data(),::time(0),pix_disable);
    }
  }



  void ModuleMask::createMaskFromText(std::istream &in, const std::string &name_arg)
  {

    {
      std::shared_ptr<PixLib::PixDisable>  disable(new PixLib::PixDisable(name_arg));
      bool disabled_something=false;
      PixA::ConnectivityRef conn(m_calibrationData->currentConnectivity());
      while (in) {
	std::string name;
	in >> name;
	if (m_logLevel>2) {
	  std::cout << "INFO [ModuleMask::createMaskFromText] " << name <<std::endl;
	}
	std::string::size_type pos =0;
	while (pos<name.size()) {
	  std::pair<std::string, std::string::size_type> ret = guessConnName(conn, name, pos);
	  if (!ret.first.empty()) {
	    disable->disable(ret.first);
	    disabled_something=true;
	    break;
	  }

	  if (ret.second==pos) {
	    ret.second++;
	  }
	  pos = ret.second;
	}
      }
      disable->config().dump(std::cout);
      if (disabled_something) {
	addMask(disable->config().name(),::time(0),disable);
      }
    }
    
  }


  void ModuleMask::applyFilter(bool invert) {
    if (m_moduleList) {
      std::string mask_name;
      if (m_nSelectedItems==1 && m_selectedItem) {
	mask_name = m_selectedItem->name();
      }
      else if (m_nSelectedItems==0 && m_parent) {
	mask_name = ModuleMaskGroup::name(m_parent);
      }

      std::string filter_string="\"Present Value\".\"";
      filter_string += s_maskHeaderName;
      filter_string += mask_name;
      if (invert) {
	filter_string += "\"!=";
      }
      else {
	filter_string += "\"==";
      }
      filter_string += "On";
      m_moduleList->setFilterString(QString::fromStdString(filter_string));
    }
  }

  void ModuleMask::clearFilter() {
    m_moduleList->clearFilter();
  }

  void ModuleMask::maskContextMenu(const QPoint &pos) {
    QTreeWidgetItem *item = m_maskList->itemAt(pos);
    if(item==0) // click in empty part, un-select all
      m_maskList->clearSelection();
    else
      item->setSelected(true);
    maskSelectionChanged();
    
    QMenu context_menu_q4(item ? item->text(0).toLatin1().data() : "New Mask Group");
    
    bool need_separator=false;
    if (m_nSelectedItems==0 && m_parent==0) {
      context_menu_q4.addAction("New Combined Mask",this,SLOT(sNewCombinedMask() ) );
      need_separator=true;
    }
    if (m_nSelectedItems==0 && m_parent) {
      context_menu_q4.addAction("Load Mask",this,SLOT(sLoadMask() ) );
      context_menu_q4.addAction("Load Mask from Text File",this,SLOT(sLoadModuleList() ) );
      context_menu_q4.addAction("Create Mask from Current Selection",this,SLOT(sMaskFromSelection() ) );
      context_menu_q4.addAction("Create Mask from Filter",this,SLOT(sMaskFromFilter() ) );
      
      need_separator=true;
    }
    if (m_nSelectedItems>0 || m_parent) {
      context_menu_q4.addAction("Delete Mask(s)",this,SLOT(sDeleteMask() ) );
      need_separator=true;
    }
    if ((m_nSelectedItems==1 && m_selectedItem) || (m_nSelectedItems==0 && m_parent)) {

      if (need_separator) {
	context_menu_q4.addSeparator();
	need_separator=false;
      }

      context_menu_q4.addAction("Apply Mask Filter",this,SLOT(sApplyFilter() ) );
      context_menu_q4.addAction("Apply Inverted Mask Filter",this,SLOT(sApplyAntiFilter() ) );
      
      need_separator=true;
    }
    if (m_nSelectedItems>0 && m_selectedItem) {

      //bool is_active=false;
      bool is_inactive=false;

      QTreeWidgetItemIterator par(m_maskList);
      while (*par) {
	QTreeWidgetItemIterator chld((*par));
	while (*chld) {
	  if ((*chld)->isSelected()) {
	    ModuleMaskItem *mask_item =dynamic_cast<ModuleMaskItem *>((*chld));
	    if (mask_item) {
	      is_inactive=!mask_item->isActive();
	    }
	  }
	  ++chld;
	}
	++par;
      }

      if (need_separator) {
	context_menu_q4.addSeparator();
	need_separator=false;
      }

      QAction *aToggleActive = context_menu_q4.addAction("Active", this,  SLOT(sToggleActive() ) );
      aToggleActive->setCheckable(true);
      aToggleActive->setChecked( !is_inactive );
      
    }

    context_menu_q4.exec(QCursor::pos());
  }

  void ModuleMask::sNewCombinedMask() {
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(sender==0) return;
    bool ok;
    QString text = QInputDialog::getText(this, "Combined Mask Name", "Combined Mask Name:", QLineEdit::Normal,
					   QString::null, &ok );
    if ( ok && !text.isEmpty() ) {
      if (!findMaskGroup(text.toLatin1().data())) {
	  createEmptyGroup( text.toLatin1().data() );
      }
      else {
	  std::cerr << "WARNING [ModuleMask::maskContextMenu] Combined mask " << text.toLatin1().data() << " exists already." << std::endl;
      }
    }
  }
  void ModuleMask::sLoadMask() {
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(sender==0) return;
    loadMask();
  }
  void ModuleMask::sLoadModuleList() {
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(sender==0) return;
    loadTextFile();
  }
  void ModuleMask::sMaskFromSelection() {
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(sender==0) return;
    createMaskFromTextSelection();
  }
  void ModuleMask::sMaskFromFilter() {
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(sender==0) return;
    createMaskFromFilter();
  }  
  void ModuleMask::sDeleteMask() {
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(sender==0) return;
    deleteSelectedMasks();
  }
  void ModuleMask::sApplyFilter() {
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(sender==0) return;
    applyFilter(false);
  }
  void ModuleMask::sApplyAntiFilter() {
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(sender==0) return;
    applyFilter(true);
  }
  void ModuleMask::sToggleActive() {
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(sender==0) return;
    bool activate = sender->isChecked();

    for(int ip=0;ip<m_maskList->topLevelItemCount(); ip++){
      QTreeWidgetItem *parent = m_maskList->topLevelItem(ip);
      for(int ic=0;ic<parent->childCount();ic++){
	QTreeWidgetItem *child = parent->child(ic);
	if (child->isSelected()) {
	  ModuleMaskItem *mask_item =dynamic_cast<ModuleMaskItem *>(child);
	  if (mask_item) {
	    mask_item->setActive(activate);
	  }
	}
      }
      createCombinedMaskVariable(parent);
    }
  }

}
