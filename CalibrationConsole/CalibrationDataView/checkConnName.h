#ifndef _checkConnName_h_
#define _checkConnName_h_

#include <string>

bool checkConnName(const std::string &token);

#endif
