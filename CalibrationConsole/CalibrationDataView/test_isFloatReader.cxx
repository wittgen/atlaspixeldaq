
#include <ipc/partition.h>
#include <ipc/core.h>

#include <is/infoT.h>
#include <is/inforeceiver.h>
#include <is/infoiterator.h>

#include <qthread.h>
#include <qapplication.h>


template <class T>
class IsListener : public QThread
{
public:
  IsListener(const std::string &partition_name, const std::string &is_server_name, const std::string &var_name);

  void run() {m_isReceiver.run();}
  
protected:
  void valueChanged(ISCallbackInfo *info);
  static void _valueChanged(ISCallbackInfo *info);

private:
  IPCPartition   m_isPartition;
  ISInfoReceiver m_isReceiver;
};


template <class T>
IsListener<T>::IsListener(const std::string &partition_name, const std::string &is_server_name, const std::string &var_name) 
  : m_isPartition(partition_name),
    m_isReceiver(m_isPartition)
{ 
  // get initial config values from IS server
  std::string name(".*");
  name+=var_name;


  ISInfoT<T> is_value;
  ISCriteria criteria = is_value.type() && name;

  ISInfoIterator ii( m_isPartition, is_server_name,  criteria);
  while( ii() ) {

    ii.value(is_value);

    std::cout << "INFO [IsListener::ctor] " << ii.name()  << " : " << is_value.getValue() << std::endl;
  }

  m_isReceiver.subscribe(is_server_name.c_str(),criteria,&IsListener<T>::valueChanged, this);
}


template <class T>
void IsListener<T>::valueChanged(ISCallbackInfo *info)
{
  ISInfoT<T> is_value;
  if (info->type() == is_value.type()) {
    info->value(is_value);

    std::cout << "INFO [IsListener::ctor] " << info->name()  << " : " << is_value.getValue() << std::endl;

  }
}


template <class T>
void IsListener<T>::_valueChanged(ISCallbackInfo *info)
{
  IsListener *the_listener=reinterpret_cast<IsListener *>( info->parameter() );
  the_listener->valueChanged(info);
}


int main(int argc, char **argv)
{
  std::string partition_name;
  std::string var_name;
  std::string is_name = "PixFakeData";
  bool error=false;

  for (int arg_i=1; arg_i<argc; arg_i++) {
    if (strcmp(argv[arg_i],"-p")==0 && arg_i+1<argc) {
      partition_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-v")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      var_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-n")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      is_name = argv[++arg_i];
    }
    else {
      std::cout << "ERROR ["<< argv[0] << ":main] unhandled argument " << arg_i << " : " << argv[arg_i]<< "." << std::endl;
      error=true;
    }
  }

  if (var_name.empty() || partition_name.empty()  || error ) {
      std::cout << "usage: " << argv[0] << "-p partition -v var-name [-n \"IS server name\"]" << std::endl;
      return -1;
  }
  // START IPC

  // Start IPCCore
  IPCCore::init(argc, argv);

  // Create IPCPartition
  std::cout << "INFO [" << argv[0] << ":main] listen for changes of " << var_name << " on " << partition_name  << " in IS server "  << is_name << std::endl;

  QApplication app( argc, argv );

  IsListener<float> listener(partition_name,is_name,var_name);
  listener.start();

  listener.wait();

  return 0;
}
