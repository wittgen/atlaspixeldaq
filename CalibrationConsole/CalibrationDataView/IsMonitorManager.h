/* Dear emacs, this is -*-c++-*- */
#ifndef _PixCon_IsMonitorManager_h_
#define _PixCon_IsMonitorManager_h_

#include <string>
#include <map>
#include <vector>
#include <memory>
#include "omnithread.h"

#include "Flag.h"

namespace PixCon {

  class IsMonitorBase;
  class IsReceptor;

  class ABool {
  public:
    ABool() : m_flag(false) {}

    operator bool () const { return m_flag; }
    ABool &operator=(bool a) { m_flag = a; return *this; }

    void set() { m_flag=true;}
    void clear() { m_flag=false;}

    bool &flag() { return m_flag; }
  private:
    bool m_flag;
  };


  /** Monitor manager and queue processor.
   * The monitor manager waits for monitors to signal the arrival of new data.
   * Then it will wake up and process all queues until all queues are empty.
   * In case monitors signal a complete update, a complete update will be
   * preformed instead of processing the queue. Though, it is up to the
   * monitor to clear the queue in case of a complete update.
   */
  class IsMonitorManager : public omni_thread
  {
  protected:
    friend class IsMonitorBase;

  private:

    // helper classes to act on the list of monitors :
    class Update;
    class Reread;
    class Reconnect;
    class ReconnectReceptors;
    class Disconnect;
    class Shutdown;
    class GetCheckTime;

    friend class GetCheckTime;

  public:
    IsMonitorManager();

    ~IsMonitorManager();

    void *run_undetached(void *arg);

    void initiateShutdown();

    void shutdown();

  protected:
    void updateRequest() {
      m_updateRequests.setFlag();
    }

  public:
    void reconnect() {
      m_reconnect=true;
      m_updateRequests.setFlag();
    }

    void reread() {
      m_reread=true;
      m_updateRequests.setFlag();
    }

    /** Hand a new monitor over to the monitor manager.
     * @param name the name of the monitor
     * @param monitor the monitor
     */
    void addMonitor(const std::string &name,  const std::shared_ptr<IsMonitorBase> &monitor);

    /** Remove a monitor from the manager.
     * @param name the name of the monitor
     */
    std::shared_ptr<IsMonitorBase> removeMonitor(const std::string &name);

    /** Remove a monitor from the manager.
     * @param monitor the monitor to be removed.
     */
    std::shared_ptr<IsMonitorBase> removeMonitor(const std::shared_ptr<IsMonitorBase> &monitor);


  protected:
    /** Change the maximum time between checks.
     */
    void setCheckTime(unsigned int check_time) {
      bool wake_up =  (m_checkTime==0 && check_time>0) || (m_checkTime  > check_time + m_checkTime / 8 );
      m_checkTime = check_time;

      if (wake_up) {
	m_updateRequests.setFlag();
      }
    }
  private:

    Mutex                      m_monitorMutex;
    std::vector< std::shared_ptr<IsReceptor> >              m_receptor;
    std::map< std::string, std::shared_ptr<IsMonitorBase> > m_monitorList;
    Flag  m_updateRequests;
    Flag  m_exit;

    unsigned int m_checkTime;

    ABool m_running;
    ABool m_reconnect;
    ABool m_reread;
    ABool m_shutdown;
  };

}
#endif
