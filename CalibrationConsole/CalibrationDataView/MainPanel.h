#ifndef _MainPanel_H_
#define _MainPanel_H_

#include "ui_MainPanelBase.h"
#include "UserMode.h"
#include "DetectorViewList.h"
#include "ModuleListView.h"

#include "ISelectionFactory.h"
#include "QNewVarEvent.h"
#include <QCloseEvent>
#include <QEvent>
#include <list>
#include <QVBoxLayout>
#include <time.h>

class QTimer;

namespace PixCon {

  class HistogramView;
  class CoupledDetectorView;
  class ModuleList;
  class MetaDataList;
  class IMetaDataUpdateHelper;
  class LogWindow;
  class ConsoleBusyLoopFactory;
  class IConnItemSelection;
  class ICalibrationDataListener;
  class Slowupdatelist;

  class MainPanel;

  class MainPanelSelectionFactory :  public ISelectionFactory
  {
  public:
    MainPanelSelectionFactory(MainPanel &main_panel) 
      : m_mainPanel(&main_panel)
    {}

    ~MainPanelSelectionFactory();

    IConnItemSelection *selection(bool selected, bool deselected);
    IConnItemSelection *selection(bool selected, bool deselected, EMeasurementType measurement, SerialNumber_t serial_number);

  private:
    MainPanel *m_mainPanel;
  };

  class MainPanel : public QMainWindow, public Ui_MainPanelBase
  {
    friend class MainPanelSelectionFactory;

    Q_OBJECT
  public:
    enum EPopupMenuItemId
    {
      kFileExit=0,
      kFileOpenFile,
      kFileOpenDirectory,
      kFileOpenScanAnalysis,
      kFileReconnectIPC,
      kFileReAllocateActions,
      kFileChangeConnectivityTags,
      kFileCloneConfigTag,
      kFileLoadDisable,
      kFileSaveDisable,
      kUtilsDCSMonitor,
      kUtilsBocEditor,
      kUtilsRODRecovery,
      kUtilsPCDCreator,
      kUserShifter,
      kUserExpert
    };

    MainPanel(std::shared_ptr<PixCon::UserMode>& user_mode,
	      const std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data,
	      const std::shared_ptr<PixCon::CalibrationDataPalette> &palette,
	      const std::shared_ptr<PixCon::CategoryList> &categories,
	      const std::shared_ptr<PixCon::IContextMenu> &context_menu,
	      const std::shared_ptr<PixCon::IMetaDataUpdateHelper> &update_helper,
	      const std::shared_ptr<PixDet::ILogoFactory> &logo_factory,
	      QApplication &application,
	      QWidget* parent = 0);

    ~MainPanel();

  //  void selectConnectivityTags();

    void changeConnectivity(const std::string &object_tag,
			    const std::string &conn_tag,
			    const std::string &alias_tag,
			    const std::string &data_tag,
			    const std::string cfg_tag,
			    const std::string mod_cfg_tag,
			    const PixA::Revision_t &rev)
    {
      m_detectorViewList.changeConnectivity(object_tag,conn_tag,alias_tag,data_tag,cfg_tag, mod_cfg_tag, rev);
      connectivityChanged(kCurrent, 0);
    }

    /** Notify about a changed connectivity.
     */
    void changeConnectivity(EMeasurementType measurement_type,
			    SerialNumber_t serial_number) {
      m_detectorViewList.updateViews(measurement_type,serial_number);
      connectivityChanged(measurement_type, serial_number);
    }

    void updateCategories(const std::shared_ptr<PixCon::CategoryList> &categories) {
      m_detectorViewList.updateCategories(categories);
    }

    /** Clear and add items to module list filter selector
     */
    void clearFilterSelector();
    void addItemToFilterSelector(const std::string& filter_string);

    void hideSerialNumberSelector();

    void setSerialNumbers() {
      m_detectorViewList.setSerialNumbers();
      setMetaData();
    }

    void selectSerialNumber(PixCon::EMeasurementType measurement_type, PixCon::SerialNumber_t serial_number);


    /** Update the value of the given variable for the given conn item if the value has changed.
     */
    void updateConnItem(EConnItemType conn_type,
			const std::string &conn_name,
			EMeasurementType measurement_type,
			SerialNumber_t serial_number,
			const std::string &var_name) {
      // paranoia checks
      assert (measurement_type < kNMeasurements);
      assert ( measurement_type != kCurrent || serial_number ==0 );
      m_detectorViewList.updateConnItem(conn_type, conn_name, measurement_type, serial_number, var_name);
    }

    /** Update values and enable state for the given conn item if the value or enable state has changed.
     */
    void updateConnItem(EConnItemType conn_type,
			const std::string &conn_name,
			EMeasurementType measurement_type,
			SerialNumber_t serial_number) {
      // paranoia checks
      assert ( measurement_type < kNMeasurements);
      assert ( measurement_type != kCurrent || serial_number ==0 );
      m_detectorViewList.updateConnItem(conn_type, conn_name, measurement_type, serial_number);
    }

    /** To be called if the category has been modified for the given measurement.
     * @param measurement_type the type of the measurement  (scan or analysis).
     * @param serial_number the serial number of the measurement.
     * @param category_name the name of the category.
     * This method should be called if variables are added or removed from a category.
     */
    void updateCategory(PixCon::EMeasurementType measurement_type, PixCon::SerialNumber_t serial_number, const QString &category_name)
    {
      m_detectorViewList.updateCategory(measurement_type, serial_number, category_name);
    }

    /** Register a listener for slow updates.
     * Slow updates are objects which react slowly O(seconds) to updates of calibration data.
     */
    void registerListener(EMeasurementType measurement, SerialNumber_t serial_number, const std::shared_ptr<ICalibrationDataListener> &listener);

    /** Deregister a listener for slow updates.
     */ 
    void deregisterListener(const std::shared_ptr<ICalibrationDataListener> &listener);
  protected:
    std::shared_ptr<SlowUpdateList> slowUpdateList() { return m_detectorViewList.slowUpdateList(); }

  public slots:
void selectConnectivityTags();
    /** Make a new serial number known to the detector views.
     * @param measurement_type the type of the measurement  (scan or analysis).
     * @param serial_number the serial number of the measurement.
     * Unlike @ref removeMeasurement, this does not manipulate the calibration data,
     * the meta data catalogues or add the corresponding connectivity.
     */
    void addSerialNumber(EMeasurementType measurement_type, SerialNumber_t serial_number);

    /** Remove a measurement from memory.
     * @param measurement_type the type of the measurement  (scan or analysis).
     * @param serial_number the serial number of the measurement.
     * This will remove all data from the calibration data container @ref CalibrationData,
     * the meta data catalogues and the connectivity. If a scan is removed, in addition all associated
     * analyses will be removed.
     * @todo Maybe this is not the right place for this functionality
     */
    virtual void removeMeasurement(PixCon::EMeasurementType measurement_type, PixCon::SerialNumber_t serial_number);


    /** Method called if the the scan config should be shown.
     */
    virtual void showScanConfiguration(PixCon::EMeasurementType measurement, PixCon::SerialNumber_t serial_number) = 0;

    /** Show the configuration of an analysis, classification and post-processing according to a given type.
     */
    virtual void showAnalysisConfiguration(PixCon::EMeasurementType type, PixCon::SerialNumber_t serial_number, unsigned short class_i) = 0;

    /** Method called if a measurement should be added.
     */
    virtual void addMeasurement() = 0;

    void cycleLogWindowSizeUp();

    void cycleLogWindowSizeDown();

    void cycleLogWindowSize(bool up);

    void hideLogWindow();

    ISelectionFactory *createSelectionFactory() {
      return new MainPanelSelectionFactory(*this);
    }

    /** Update all variables which were collected in the list @ref m_updateVariables
     */
    void updateVariables();

    void processSlowUpdates();

    /** Print module list */
    void showModuleListView();

    /** Change user role */
    void setUserMode(PixCon::UserMode::EUserMode new_user);
    void setUserModeShifter();
    void setUserModeExpert();

  protected:
    bool event(QEvent *an_event);
    void closeEvent(QCloseEvent * event);

    QVBoxLayout* m_logFrameLayout;
    /** Will be called on exit.
     * Can be used to gracefull stop threads etc.
     */
    virtual void shutdown() {}

    /** Will be called if the connectivity has changed for the given measurment.
     * E.g. the method will be called after @ref changeConnectivity with 
     * measurement_type = kCurrent, and serial_number = 0.
     */
    virtual void connectivityChanged(PixCon::EMeasurementType measurement_type, PixCon::SerialNumber_t serial_number);

    QApplication *application() { return m_app; }

    std::pair<PixCon::EMeasurementType, PixCon::SerialNumber_t> selectedMeasurement();

    IConnItemSelection *currentSelection(bool selected, bool deselected);

    IConnItemSelection *selection(bool selected, bool deselected, EMeasurementType measurement_type, PixCon::SerialNumber_t serial_number);

    std::shared_ptr<PixCon::CategoryList> categories() { return m_categories; }

    QAction *m_shifterAction, *m_expertAction, *m_sepExitAction;

  signals:
    void newMeasurement(PixCon::EMeasurementType measurement_type, PixCon::SerialNumber_t serial_number);

    void userModeChanged(PixCon::UserMode::EUserMode new_user);

  private:

    std::shared_ptr<PixCon::UserMode> m_userMode;
    ModuleListView* m_moduleListView;
    void setMetaData() ;

    QApplication *m_app;
    DetectorViewList m_detectorViewList;

    CoupledDetectorView *m_coupledDetectorView;

    bool                 m_moduleListUpdateDataSelection;
    ModuleList          *m_moduleList;

    MetaDataList        *m_metaDataList;
    LogWindow           *m_logWindow;
    ConsoleBusyLoopFactory *m_busyLoopFactory;

    std::shared_ptr<PixCon::CategoryList>    m_categories;
    QTimer                                    *m_updateVarListTimer;
    static const unsigned int s_updateTime = 300;                    /**< Delay updates of the detector canvas by this number of milli seconds. */
    std::list<NewVar_t>                        m_updateVariables;

    static const unsigned int s_minUpdatePeriode = 5000;             /**< Consecutive of slow updates are delayed at least by this number of milli seconds. */
    static const unsigned int s_extraDelayPerListener = 300;         /**< number of milliseconds added to the s_minUpdatePeriode per listener. */
    static const unsigned int s_minDelay = 1000;                     /**< Slow updates are delayed by at least this number of milli seconds. */
    
    QTimer                                      *m_slowUpdateTimer;
    ::time_t                                     m_lastSlowUpdate;

  };


  inline IConnItemSelection *MainPanelSelectionFactory::selection(bool selected, bool deselected) {
    return m_mainPanel->currentSelection(selected, deselected);
  }

  inline IConnItemSelection *MainPanelSelectionFactory::selection(bool selected, bool deselected, EMeasurementType measurement, SerialNumber_t serial_number) {
    return m_mainPanel->selection(selected, deselected, measurement, serial_number);
  }


}
#endif
