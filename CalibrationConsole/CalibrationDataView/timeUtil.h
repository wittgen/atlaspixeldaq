#ifndef _PixCon_timeUtil_h_
#define _PixCon_timeUtil_h_

#include <ctime>

namespace PixCon {
  std::string timeString(const ::time_t &a_time, bool time_only=false); // implemented in LogWindow.cxx
}

#endif
