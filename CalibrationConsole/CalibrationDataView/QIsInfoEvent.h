#ifndef _PixCon_QIsInfoEvent_h_
#define _PixCon_QIsInfoEvent_h_

#include <qevent.h>
#include <string>
#include <CalibrationDataTypes.h>

namespace PixCon {

  class ConnVar_t {
  public:
    ConnVar_t(EValueConnType conn_type, const std::string &conn_name, const std::string &var_name )
      : m_varName(var_name),
        m_connName(conn_name),
	m_connType(conn_type)
    {}
    
    ConnVar_t(EValueConnType conn_type )
      : m_connType(conn_type)
    {}

    std::string &connName()             { return m_connName; }
    const std::string &connName() const { return m_connName; }
    //    EValueConnType     connType() const { return m_connType; }
    EValueConnType     connType() const { return m_connType; }

    std::string &varName()              { return m_varName;  }
    const std::string &varName()  const { return m_varName;  }
  protected:
    std::string     m_varName;
    std::string     m_connName;
    EValueConnType  m_connType;
  };

  class IVarNameExtractor 
  {
  public:
    virtual ~IVarNameExtractor() {}
    virtual void extractVar(const std::string &full_name, ConnVar_t &conn_var) const = 0 ;
  };

  class QIsInfoEvent : public QEvent, public ConnVar_t
  {
  public:
    QIsInfoEvent(const std::string &is_name, const IVarNameExtractor &extractor, EValueConnType type, EMeasurementType measurement, SerialNumber_t serial_number)
      : QEvent(QEvent::User), ConnVar_t(type), m_measurementType(measurement), m_serialNumber(serial_number),m_propagate(false)
    { assert( measurement < kNMeasurements); assert(type < kNValueConnTypes); assert( measurement != kCurrent || serial_number ==0); extractor.extractVar(is_name, *this); }

    QIsInfoEvent(const std::string &conn_name, const std::string &var_name, EValueConnType type, EMeasurementType measurement, SerialNumber_t serial_number)
      : QEvent(QEvent::User), ConnVar_t(type, conn_name, var_name), m_measurementType(measurement), m_serialNumber(serial_number),m_propagate(false)
    { assert( measurement < kNMeasurements); assert(type < kNValueConnTypes); assert( measurement != kCurrent || serial_number ==0); }

    QIsInfoEvent(const std::string &is_name, const IVarNameExtractor &extractor, EValueConnType type)
      : QEvent(QEvent::User), ConnVar_t(type), m_measurementType(kCurrent),m_serialNumber(0), m_propagate(false)
      { assert(type < kNValueConnTypes); extractor.extractVar(is_name, *this); }

    bool valid() const {return !(connName().empty() /* @todo varName().emtpy() is now used if the enable state has changed.|| varName().empty()*/ ); }

//     const std::string &connName() const { return m_connName; }
//     EValueConnType     connType() const { return m_connType; }
//     const std::string &varName()  const { return m_varName;  }

    EMeasurementType measurementType()  const { return m_measurementType; }
    SerialNumber_t   serialNumber()     const { return m_serialNumber; }

    /** Propagate the event to all childs of the connectivity object.
     */
    void setPropagateToChilds() { m_propagate=true; } 
    bool propagateToChilds() const { return m_propagate; }

  private:

//     std::string     m_varName;
//     std::string     m_connName;
//     EValueConnType  m_connType;

    EMeasurementType m_measurementType;
    SerialNumber_t   m_serialNumber;
    bool             m_propagate;
  };

}


#endif
