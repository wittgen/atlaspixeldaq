#ifndef _PixCon_DetectorViewWindow_h_
#define _PixCon_DetectorViewWindow_h_

#include "ui_DetectorViewWindowBase.h"

#include <string>
#include <QCloseEvent>
#include <QVBoxLayout>
#include <QMainWindow>
class DetectorView;

namespace PixCon {

  class DetectorViewList;
  class CoupledDetectorView;
  class ModuleList;

  class DetectorViewWindow : public QMainWindow, public Ui_DetectorViewWindowBase
  {
    Q_OBJECT

  public:
    friend class ::DetectorView;

    DetectorViewWindow(PixCon::DetectorViewList &detector_view_list,
		       QApplication &app,
		       QWidget* parent = NULL);

    ~DetectorViewWindow();
 
//  public slots:
//    void toggleModuleListView();
//
  protected:
    void closeEvent(QCloseEvent * event);
   
    static std::string makeName();
    DetectorView &detectorView() { return *m_detectorView; }
    QVBoxLayout* m_leftPanelLayout;
    QVBoxLayout* m_rightPanelLayout;
  private:
    DetectorViewList    *m_detectorViewList;
    DetectorView        *m_detectorView;
    CoupledDetectorView *m_coupledDetectorView;

    bool                 m_moduleListUpdateDataSelection;
    ModuleList          *m_moduleList;

    unsigned int        m_view;
    static unsigned int s_counter;

  };

}
#endif
