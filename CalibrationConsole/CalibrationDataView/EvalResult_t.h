#ifndef _PixCon_EvalResult_t_h_
#define _PixCon_EvalResult_t_h_

namespace PixCon {

  /** Extended boolean which allows for an unknown state
   * logic:
   * true  && true     = true
   * true  && false    = false
   * false && false    = false
   * false && unknown  = false
   * true  && unknown  = unknonw
   *
   * true || true     = true
   * true || false    = true
   * false || false   = false
   * false || unknown = unknown
   * true  || unknown  = true
   *
   * !EvalResult_t : true -> false, false->true, unknown -> unknown :
   *
   * !(true  && true)     = !true
   * !(true  && false)    = !false
   * !(false && false)    = !false
   * (!false || !unknown)  = !false
   * (!true  || !unknown)  = unknonw
   *
   * !(true || true)     = false
   * !(true || false)    = false
   * !(false || false)   = true
   * (!false && !unknown) = unknown
   * (!true  && !unknown)  = false
   */
  class EvalResult_t
  {
  public:
    enum EResult {kFalse, kTrue, kUnknown};

    EvalResult_t()  : m_result(kUnknown) {}
    EvalResult_t(bool result)  : m_result(static_cast<EResult>(result)) {}
    EvalResult_t(EResult result)  : m_result(result) {}
    EvalResult_t(const EvalResult_t &result)  : m_result(result.m_result) {}

    operator bool() const { return m_result==kTrue;}

    bool isUnknown() const { return m_result==kUnknown; }

    EvalResult_t andWith( const EvalResult_t &b) const {
      return EvalResult_t( s_combMap[kAnd][m_result][b.m_result] );
    }

    EvalResult_t orWith( const EvalResult_t &b) const {
      return EvalResult_t( s_combMap[kOr][m_result][b.m_result] );
    }

    EvalResult_t invert() const {
      if (m_result==kUnknown) return *this;
      else return EvalResult_t(!m_result);
    }

    static EvalResult_t trueResult()    { return EvalResult_t(kTrue); }
    static EvalResult_t falseResult()   { return EvalResult_t(kFalse); }
    static EvalResult_t unknownResult() { return EvalResult_t(kUnknown); }

    static bool init();

  private:

    static EResult orResult( const EResult &a, const EResult &b) {
      if (a==kTrue) return a;
      else if (b==kTrue) return b;
      else if (a==kFalse && b==kFalse) return a;
      else return kUnknown;
    }

    static EResult andResult( const EResult &a, const EResult &b) {
      if (a==kFalse) return a;
      else if (b==kFalse) return b;
      else if (a==kTrue && b==kTrue) return a;
      else return kUnknown;
    }


    EResult m_result;
    enum EOp {kAnd, kOr};
    static EResult s_combMap[2][3][3];
    static bool s_initialised;
  };

  inline EvalResult_t operator&&(const EvalResult_t &a, const EvalResult_t &b) {
    return a.andWith(b);
  }

  inline EvalResult_t operator||(const EvalResult_t &a, const EvalResult_t &b) {
    return a.orWith(b);
  }
  
  inline EvalResult_t operator!(EvalResult_t &a) {
    return a.invert();
  }

}
#endif
