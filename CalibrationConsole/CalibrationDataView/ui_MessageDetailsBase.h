#ifndef UI_MESSAGEDETAILSBASE_H
#define UI_MESSAGEDETAILSBASE_H

#include <QTextEdit>
#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QDialog>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_MessageDetailsBase
{
public:
    QVBoxLayout *vboxLayout;
    QHBoxLayout *hboxLayout;
    QLabel *m_idString;
    QLabel *m_level;
    QLabel *textLabel1;
    QLineEdit *m_time;
    QHBoxLayout *hboxLayout1;
    QLabel *textLabel3;
    QLineEdit *m_method;
    QTextEdit *m_message;

    void setupUi(QDialog *MessageDetailsBase)
    {
        if (MessageDetailsBase->objectName().isEmpty())
            MessageDetailsBase->setObjectName(QString::fromUtf8("MessageDetailsBase"));
        MessageDetailsBase->resize(781, 152);
        vboxLayout = new QVBoxLayout(MessageDetailsBase);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        m_idString = new QLabel(MessageDetailsBase);
        m_idString->setObjectName(QString::fromUtf8("m_idString"));
        m_idString->setWordWrap(false);

        hboxLayout->addWidget(m_idString);

        m_level = new QLabel(MessageDetailsBase);
        m_level->setObjectName(QString::fromUtf8("m_level"));
        m_level->setFrameShape(QFrame::Box);
        m_level->setFrameShadow(QFrame::Plain);
        m_level->setAlignment(Qt::AlignCenter);
        m_level->setWordWrap(false);

        hboxLayout->addWidget(m_level);

        textLabel1 = new QLabel(MessageDetailsBase);
        textLabel1->setObjectName(QString::fromUtf8("textLabel1"));
        textLabel1->setWordWrap(false);

        hboxLayout->addWidget(textLabel1);

        m_time = new QLineEdit(MessageDetailsBase);
        m_time->setObjectName(QString::fromUtf8("m_time"));
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(3), static_cast<QSizePolicy::Policy>(0));
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(m_time->sizePolicy().hasHeightForWidth());
        m_time->setSizePolicy(sizePolicy);
        m_time->setCursorPosition(0);
        m_time->setAlignment(Qt::AlignLeft);
        m_time->setDragEnabled(true);
        m_time->setReadOnly(true);

        hboxLayout->addWidget(m_time);


        vboxLayout->addLayout(hboxLayout);

        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setSpacing(6);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        textLabel3 = new QLabel(MessageDetailsBase);
        textLabel3->setObjectName(QString::fromUtf8("textLabel3"));
        textLabel3->setWordWrap(false);

        hboxLayout1->addWidget(textLabel3);

        m_method = new QLineEdit(MessageDetailsBase);
        m_method->setObjectName(QString::fromUtf8("m_method"));
        m_method->setAlignment(Qt::AlignLeft);
        m_method->setDragEnabled(false);
        m_method->setReadOnly(true);

        hboxLayout1->addWidget(m_method);


        vboxLayout->addLayout(hboxLayout1);

        m_message = new QTextEdit(MessageDetailsBase);
        m_message->setObjectName(QString::fromUtf8("m_message"));
        m_message->setReadOnly(true);

        vboxLayout->addWidget(m_message);


        retranslateUi(MessageDetailsBase);

        QMetaObject::connectSlotsByName(MessageDetailsBase);
    } // setupUi

    void retranslateUi(QDialog *MessageDetailsBase)
    {
        MessageDetailsBase->setWindowTitle(QApplication::translate("MessageDetailsBase", "MessageDetails", 0));
        m_idString->setText(QApplication::translate("MessageDetailsBase", "00000", 0));
        m_level->setText(QApplication::translate("MessageDetailsBase", "INFORMATION", 0));
        textLabel1->setText(QApplication::translate("MessageDetailsBase", "Time :", 0));
        m_time->setText(QApplication::translate("MessageDetailsBase", "Tue May 20 10:08:55 CEST 2008", 0));
        textLabel3->setText(QApplication::translate("MessageDetailsBase", "Method :", 0));
    } // retranslateUi

};

namespace Ui {
    class MessageDetailsBase: public Ui_MessageDetailsBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MESSAGEDETAILSBASE_H
