#include "CoupledDetectorView.h"
#include "ModuleList.h"

namespace PixCon {

  CoupledDetectorView::CoupledDetectorView(PixCon::DetectorViewList &detector_view_list,
					   bool setup_calibration_data,
					   std::shared_ptr<PixCon::IContextMenu> &context_menu,
					   const std::shared_ptr<PixDet::ILogoFactory> &logo_factory,
					   const std::shared_ptr<PixCon::SlowUpdateList> &slow_update_list,
					   QApplication &app,
					   QWidget* parent ) 
    : DetectorView(detector_view_list,setup_calibration_data, context_menu, logo_factory, slow_update_list, app, parent),
      m_moduleList(NULL),
      m_selectTimer(new QTimer)
  {
    connect(m_selectTimer,SIGNAL( timeout() ),this,SLOT( setItemSelection() ) );
  }

  CoupledDetectorView::~CoupledDetectorView() {
    removeListener();
    delete m_selectTimer;
  }


  void CoupledDetectorView::setModuleList(ModuleList *module_list) 
  {
    if (m_moduleList) {
      disconnect(m_moduleList, NULL, this, NULL );
    }
    m_moduleList=module_list;
    if (m_moduleList) {
      connect(m_moduleList,SIGNAL(changedSelection() ), this, SLOT( updateItemFocus() ) );
      connect(m_moduleList,SIGNAL(selectModule(EConnItemType , const std::string &) ), this, SLOT( initiateConnItemSselection(EConnItemType , const std::string &) ) );
   }
  }

  bool CoupledDetectorView::updateConnItem(PixCon::EConnItemType conn_type,
					   const std::string &conn_name,
					   PixCon::EMeasurementType measurement_type,
					   SerialNumber_t serial_number) 
  {
    // the calling function should have verified that this function is only called if the selection
    // matches the changed properties
    assert( selectedMeasurementType() == measurement_type && selectedSerialNumber() == serial_number );

    bool ret = DetectorView::updateConnItem(conn_type, conn_name, measurement_type,serial_number);
    if (m_moduleList) {
      m_moduleList->updateConnItem(conn_name);
    }
    return ret;
  }

  void CoupledDetectorView::drawDetectorModel() {
    DetectorView::drawDetectorModel();
    if (m_moduleList) {
      m_moduleList->recreateList();
    }
  }

  bool CoupledDetectorView::isFocussed(const std::string &conn_name) const
  {
    if (!m_moduleList) {
      return true;
    }
    else {
      bool ret = m_moduleList->isSelectedFromList(conn_name);
      return ret;
    }
  }

  void CoupledDetectorView::updateItemFocus() {
    std::map<EMeasurementType, SerialNumber_t> used_measurements;
    removeListener();
    if (m_moduleList) {
    m_moduleList->listOfUsedMeasurements( used_measurements );

    // now insert new listener
    for(std::map<EMeasurementType, SerialNumber_t>::const_iterator measurement_iter = used_measurements.begin();
	measurement_iter != used_measurements.end();
	++measurement_iter) {
      m_listener.push_back( std::shared_ptr<ICalibrationDataListener>(new Listener(this)));
      slowUpdateList()->registerListener(measurement_iter->first, measurement_iter->second, m_listener.back());
      // std::cout << "INFaO [CoupledDetectorView::removeListener] deregister listener for "
      // 		<< makeSerialNumberString(measurement_iter->first, measurement_iter->second)
      // 		<< std::endl;
    }
    }

    update(true);
  }

  void CoupledDetectorView::updateChanged() {
    DetectorView::updateChanged();
    if (m_moduleList) {
      m_moduleList->recreateList();
    }
  }

  void CoupledDetectorView::removeListener() {
    std::cout << "INFO [CoupledDetectorView::removeListener] deregister listener." << std::endl;
    for(std::vector< std::shared_ptr<ICalibrationDataListener > >::iterator listener_iter = m_listener.begin();
	listener_iter != m_listener.end();
	++listener_iter) {
      slowUpdateList()->deregisterListener( *listener_iter );
      listener_iter->reset();
    }
    m_listener.clear();
  }

  void CoupledDetectorView::applyFilter() {
    DetectorView::updateChanged();
    if (m_moduleList) {
      m_moduleList->recreateList();
    }
  }

  IConnItemSelection *CoupledDetectorView::currentSelection(bool selected, bool deselected){
    if (m_moduleList && !(selected & deselected)) {
      return m_moduleList->currentSelection(selected, deselected);
    }
    else {
      return new EntireSelection;
    }
  }

  IConnItemSelection *CoupledDetectorView::selection(bool selected, bool deselected, EMeasurementType measurement_type, PixCon::SerialNumber_t serial_number) {
    if (m_moduleList && !(selected & deselected)) {
      return m_moduleList->selection(selected, deselected, measurement_type, serial_number);
    }
    else {
      return new EntireSelection;
    }
  }
  void CoupledDetectorView::initiateConnItemSselection(EConnItemType type, const std::string &connectivity_name) {
    m_selectedConnItem=connectivity_name;
    m_selectedConnItemType=type;

    if (m_selectTimer->isActive()) {
      m_selectTimer->stop();
    }
    m_selectTimer->setSingleShot(true);
    m_selectTimer->start(300);

  }

  void CoupledDetectorView::setItemSelection() {
    QRect rect;
    QString conn_name(QString::fromStdString(m_selectedConnItem));
    selectItem(QString::fromStdString(m_selectedConnItem),rect);
    selectConnItemOnCanvas(m_selectedConnItemType,m_selectedConnItem);
  }

  bool CoupledDetectorView::usesVariable(EMeasurementType measurement_type, SerialNumber_t serial_number, VarId_t id) const {
    if (m_moduleList) {
      return m_moduleList->usesVariable(measurement_type, serial_number, id);
    }
    else {
      return false;
    }
  }

  void CoupledDetectorView::listOfUsedMeasurements( std::map<EMeasurementType, SerialNumber_t> &used_measurements ) {
    m_moduleList->listOfUsedMeasurements(used_measurements);
  }

}
