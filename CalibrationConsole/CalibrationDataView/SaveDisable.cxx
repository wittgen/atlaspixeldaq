#include "SaveDisable.h"
#include <CalibrationDataManager.h>

#include <IConnItemSelection.h>

#include <ConfigWrapper/PixDisableUtil.h>
#include <ConfigWrapper/Regex_t.h>

#include <QTVisualiser/BusyLoopFactory.h>

namespace PixCon {


  SaveDisable::SaveDisable(PixA::ConnectivityRef conn,
			   const std::pair<PixCon::EMeasurementType, PixCon::SerialNumber_t>  &measurement,
			   const std::shared_ptr<CalibrationDataManager> &calibration_data_manager,
			   ISelectionFactory &selection_factory,
			   QWidget* parent )
 : QDialog(parent),   
   m_conn(conn),
   m_measurement(measurement),
   m_calibrationDataManager(calibration_data_manager),
   m_selectionFactory(&selection_factory),
   m_yellow("yellow")
  {
    setupUi(this);
    if (!m_conn) {
      std::cerr << "ERROR [SaveDisable::ctor] No connectivity, don't know where to get tags from." << std::endl;
      return;
    }

    //    QString text = QInputDialog::getText("PixDisable", "Name of PixDisable object: ", QLineEdit::Normal, QString::null, &ok, this );
    std::string serial_number_string = makeSerialNumberName(measurement.first,measurement.second);
    std::string name("SaveDisable_");
    name+=serial_number_string;

    std::string title = m_title->text().toLatin1().data();
    if (measurement.first==kCurrent && measurement.second==0) {
      title += " (Present Value) ";
    }
    else {
      title += " (";
      title += serial_number_string;
      title += ")";
    }
    m_title->setText(title.c_str());
    m_idTag->setText(conn.tag(PixA::IConnectivity::kId).c_str());
    m_tag->setText(conn.tag(PixA::IConnectivity::kConfig).c_str());

    if (m_conn) {
      std::vector<PixA::Regex_t *> ignore_pattern_list;
      ignore_pattern_list.push_back(new PixA::Regex_t("^Disable-S[[:digit:]]+$"));
      ignore_pattern_list.push_back(new PixA::Regex_t("^CL_[[:digit:]]+$"));
      ignore_pattern_list.push_back(new PixA::Regex_t("^Q[[:digit:]]+$"));

	m_disableNameSelector->addItem("");
	std::set<std::string> disable_names;
	{
	  std::unique_ptr<PixA::IBusyLoop> busy_loop(PixA::BusyLoopFactory::busyLoop(" Get disable objects from db server and CORAL ..."));
	  PixA::ConnObjConfigDb config_db(PixA::ConnObjConfigDb::sharedInstance());
	  config_db.getNames(m_conn.tag(PixA::IConnectivity::kId),m_conn.tag(PixA::IConnectivity::kConfig),PixA::IConnObjConfigDbManager::kDisableObjects, disable_names);
	}

	for(std::set<std::string>::iterator it = disable_names.begin(); it!=disable_names.end(); it++) {

	  std::vector<PixA::Regex_t *>::const_iterator pattern_iter = ignore_pattern_list.begin();
	  for(;
	      pattern_iter != ignore_pattern_list.end();
	      ++pattern_iter) {
	    if ((*pattern_iter)->match(*it)) {
	      break;
	    }
	  }
	  if (pattern_iter == ignore_pattern_list.end()) {
	    m_disableNameSelector->addItem((*it).c_str());
	  }
	}
      
    }
    adjustSize();
  }

  void SaveDisable::execute()
  {
    for(;;){
      if ( exec() == QDialog::Accepted ) {
	if (saveDisable()) break;
      }
      else {
	break;
      }

    }
  }

  bool SaveDisable::nameCorrect(const std::string &name) {
    std::string::size_type pos = name.size();
    while (pos>0 && isdigit(name[pos-1])) --pos;
    if (pos+1>= name.size() || name[pos]!='_') {
      return true;
    }
    else {
      return false;
    }

  }

  bool SaveDisable::saveDisable()
  {
    std::string disable_name =m_disableNameSelector->currentText().toLatin1().data();
    if (disable_name.empty()) return false;

    std::string::size_type pos=0;
    while (pos< disable_name.size() && isspace(disable_name[pos])) pos++;
    disable_name.erase(0,pos);

    bool is_name_correct = nameCorrect(disable_name);
    if (disable_name.empty() || !is_name_correct) {
      if (!m_highlighter.get()) {
	m_highlighter = std::make_unique<Highlighter>(*m_disableNameSelector,
								   Qt::yellow,
								     m_disableNameSelector->palette().color(QPalette::Active, QPalette::Window));
      }
      m_highlighter->restart(m_yellow);
      if (disable_name.empty()) {
	std::cerr << "WARNING [Savedisable::savedisable] The name cannot be empty." << std::endl;
      }
      else {
	std::cerr << "WARNING [Savedisable::savedisable] The name must not end in _[digits] e.g. _0."  << std::endl;
      }
    }
    else {

      bool selected = true; // always keep all modules which pass the filter
      bool deselected = !m_passedFilterOnly->isChecked(); // disable objects which do not pass the filter if check box is checked.

      std::unique_ptr<IConnItemSelection> selection(m_selectionFactory->selection(selected,deselected));
      std::shared_ptr<PixLib::PixDisable> pix_disable ( m_calibrationDataManager->getPixDisable(m_measurement.first,
												  m_measurement.second, 
												  *selection,
												  m_ignoreTim->isChecked(),
												  !m_enabledObjectsOnly->isChecked()) );
      pix_disable->config().name( m_disableNameSelector->currentText().toLatin1().data() );
      if (pix_disable) {
	std::string pending_tag("CC-dis");
	if (m_tmpButton->isChecked())  {
	  pending_tag += "_Tmp";
	}
	try {
	  PixA::ConnObjConfigDbTagRef config_db( m_conn.configDb() );
	  config_db.storeConfig(pix_disable->config(),0,pending_tag);
	}
	catch(PixA::ConfigException &) {
	  std::cout << "ERROR [ConsoleViewer::saveDisable] Failed to write disable " << pix_disable->config().name() << std::endl;
	}
	return true;
      }
    }
    return false;
  }

  SaveDisable::~SaveDisable() {}

}
