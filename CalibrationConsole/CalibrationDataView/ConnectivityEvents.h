// Dear emacs, this is -*-c++-*-
#ifndef _PixCon_ConnectivityEvents_h_
#define _PixCon_ConnectivityEvents_h_

#include <qevent.h>

namespace PixCon {

  class QConnectivityChangedEvent : public QEvent
  {
  public:
    QConnectivityChangedEvent() : QEvent(QEvent::User) {}
  };

  class QNewDisableEvent : public QConnectivityChangedEvent
  {
  public:
    QNewDisableEvent(const std::string &disable_name)
      : m_disableName(disable_name)
    {}
    const std::string &disableName() const { return m_disableName; }

  private:
    const std::string m_disableName;
  };

  class QNewTagEvent : public QConnectivityChangedEvent
  {
  public:
    QNewTagEvent(const std::string &id_tag, const std::string &conn_tag, const std::string &cfg_tag, const std::string &mod_cfg_tag) 
      : m_idTag(id_tag),
        m_connTag(conn_tag),
	m_cfgTag(cfg_tag),
	m_modCfgTag(mod_cfg_tag)
    {}

    const std::string &idTag() const                { return m_idTag; }
    const std::string &connTag() const              { return m_connTag; }
    const std::string &cfgTag() const               { return m_cfgTag; }
    const std::string &modCfgTag() const            { return m_modCfgTag; }

  private:

    std::string m_idTag;
    std::string m_connTag;
    std::string m_cfgTag;
    std::string m_modCfgTag;
  };

}
#endif
