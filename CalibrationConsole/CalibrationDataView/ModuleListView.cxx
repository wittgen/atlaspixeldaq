#include <iostream>
#include <fstream>
#include <cstring>
#include <sstream>
#include <set>

#include "ModuleListView.h"
#include "CalibrationDataFunctorBase.h"

namespace PixCon {

  class FillModuleListOperator : public CalibrationDataFunctorBase
  {
  public:
    FillModuleListOperator(EConnItemType conn_type, IConnItemSelection& selection)
      : CalibrationDataFunctorBase(connItemMask(conn_type)),
	m_connItemSelection(&selection)
    {}

    void operator()(EConnItemType /*item_type*/,
		    const std::string& conn_name,
		    const std::pair<EMeasurementType,SerialNumber_t>& /*measurement*/,
		    PixCon::CalibrationData::ConnObjDataList& /*data_list*/,
		    CalibrationData::EnableState_t /*is_enable_state*/,
		    CalibrationData::EnableState_t /*new_enable_state*/) {
      if (m_connItemSelection->isSelected(conn_name)) m_list.insert(conn_name);
    }
    
    void operator()(EConnItemType /*item_type*/,
		    const std::string& /*conn_name*/,
		    const std::pair<EMeasurementType,SerialNumber_t>& /*measurement*/,
		    CalibrationData::EnableState_t /*new_enable_state*/) {
    }
    
    std::set<std::string> getList() { return m_list; }

  private:
    IConnItemSelection* m_connItemSelection;
    std::set<std::string> m_list;
  };

  ModuleListView::ModuleListView(const std::shared_ptr<CalibrationDataManager>& calibration_data,
				 const std::shared_ptr<PixCon::DataSelection> &data_selection,
				 IConnItemSelection* conn_item_selection,
				 QWidget* parent)
: QDialog(parent),
      m_calibrationData(calibration_data),
      m_dataSelection(data_selection),
      m_connItemSelection(conn_item_selection)
  {
    setupUi(this);
    modulesTextEdit->setReadOnly(true);
    rodsTextEdit->setReadOnly(true);

    FillModuleListOperator fill_module_list_operator(kModuleItem, *m_connItemSelection);
    m_calibrationData->forEach(kCurrent, 0, fill_module_list_operator, false);
    std::set<std::string> module_list(fill_module_list_operator.getList());
    for (std::set<std::string>::const_iterator listItr =  module_list.begin(); listItr != module_list.end(); listItr++) {
      std::string name = *listItr;
      modulesTextEdit->insertPlainText(QString::fromStdString(printString(name)));
    }

    FillModuleListOperator fill_rod_list_operator(kRodItem, *m_connItemSelection);
    m_calibrationData->forEach(kCurrent, 0, fill_rod_list_operator, false);
    std::set<std::string> rod_list(fill_rod_list_operator.getList());
    for (std::set<std::string>::const_iterator listItr =  rod_list.begin(); listItr != rod_list.end(); listItr++) {
      std::string name = *listItr;
      rodsTextEdit->insertPlainText(QString::fromStdString(printString(name)));
    }
  }

  ModuleListView::~ModuleListView() {}

  std::string ModuleListView::printString(const std::string& conn_name)
  {
    std::stringstream printtext;
    printtext << conn_name;
    try {
      if (m_dataSelection->varId().hasHistory()) {
	if (m_dataSelection->varId().isValueWithHistory()) {
	  const PixCon::ValueHistory<float>& value_history(m_dataSelection->selectedHistory<float>(conn_name));
	  const PixCon::TimedValue<float>& newest_value(value_history.newest());
	  printtext << " (" << newest_value.value() << ")";
	}
	else if (m_dataSelection->varId().isStateWithHistory()) {
	  const PixCon::ValueHistory<PixCon::State_t>& value_history(m_dataSelection->selectedHistory<PixCon::State_t>(conn_name));
	  const PixCon::TimedValue<PixCon::State_t>& newest_value(value_history.newest());
	  printtext << " (" << newest_value.value() << ")";
	}
      }
      else if (m_dataSelection->varId().isValue()) {
	float value = m_dataSelection->selectedValue<float>(conn_name);
	printtext << " (" << value << ")";
      }
      else if (m_dataSelection->varId().isState()) {
	PixCon::State_t value = m_dataSelection->selectedValue<PixCon::State_t>(conn_name);
	printtext << " (" << value << ")";
      }
    }
    catch (CalibrationData::MissingValue&) {}
    printtext << "\n";

    return printtext.str();
  }

}
