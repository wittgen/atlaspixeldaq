#ifndef UI_MAINPANELBASE_H
#define UI_MAINPANELBASE_H

#include <QFrame>
#include <QGroupBox>
#include <QMainWindow>
#include <QTableWidget>
#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QCheckBox>
#include <QFrame>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLineEdit>
#include <QMenu>
#include <QMenuBar>
#include <QSplitter>
#include <QTabWidget>
#include <QVBoxLayout>
#include <QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainPanelBase
{
public:
    QWidget *mainWidget;
    QVBoxLayout *mainWidgetLayout;
    QSplitter *logWindowSplitter;
    QSplitter *mainPanelSplitter;
    QGroupBox *mainWindowLeftPanel;
    QVBoxLayout *leftPanelLayout;
    QTabWidget *mainWindowRightPanel;
    QWidget *tab;
    QVBoxLayout *tabLayout;
    QHBoxLayout *hboxLayout;
    QCheckBox *m_listenOnIsSwitch;
    QLineEdit *m_isServer;
    QFrame *line;
    QFrame *dataListFrame;
    QFrame *logFrame;
    QVBoxLayout *logFrameLayout;
    QTableWidget *logFrameTable;
    QMenuBar *MenuBar;
    QMenu *User;
    QMenu *File;
    QMenu *Utils;

    void setupUi(QMainWindow *MainPanelBase) {
        if (MainPanelBase->objectName().isEmpty())
        MainPanelBase->resize(1050, 800);

        MenuBar = new QMenuBar(0);
        User = new QMenu(MenuBar);
        File = new QMenu(MenuBar);
        Utils = new QMenu(MenuBar);

        MenuBar->addAction(User->menuAction());
        MenuBar->addAction(File->menuAction());
        MenuBar->addAction(Utils->menuAction());

        mainWidget = new QWidget(MainPanelBase);
        mainWidget->setGeometry(QRect(0, 0, 1050, 800));
        mainWidgetLayout = new QVBoxLayout(mainWidget);
        mainWidgetLayout->setSpacing(6);
        mainWidgetLayout->setContentsMargins(2, 2, 2, 2);

        logWindowSplitter = new QSplitter(Qt::Vertical, mainWidget);
        mainPanelSplitter = new QSplitter(Qt::Horizontal, logWindowSplitter);

        mainWindowLeftPanel = new QGroupBox(mainPanelSplitter);
        mainWindowLeftPanel->setFlat(true);
        mainWindowLeftPanel->setMinimumSize(750, 0);
        leftPanelLayout = new QVBoxLayout();
        leftPanelLayout->addStretch(2);
        mainWindowLeftPanel->setLayout(leftPanelLayout);

        mainPanelSplitter->addWidget(mainWindowLeftPanel);

        mainWindowRightPanel = new QTabWidget(mainPanelSplitter);
        tab = new QWidget();
        tabLayout = new QVBoxLayout(tab);
        tabLayout->setSpacing(6);
        tabLayout->setContentsMargins(11, 11, 11, 11);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        m_listenOnIsSwitch = new QCheckBox(tab);

        hboxLayout->addWidget(m_listenOnIsSwitch);

        m_isServer = new QLineEdit(tab);

        hboxLayout->addWidget(m_isServer);

        tabLayout->addLayout(hboxLayout);
        line = new QFrame(tab);
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);
        tabLayout->addWidget(line);

        dataListFrame = new QFrame(tab);
        dataListFrame->setFrameShape(QFrame::StyledPanel);
        dataListFrame->setFrameShadow(QFrame::Raised);
        dataListFrame->setLineWidth(0);

        tabLayout->addWidget(dataListFrame);

        mainWindowRightPanel->addTab(tab, QString());
        mainPanelSplitter->addWidget(mainWindowRightPanel);
        logWindowSplitter->addWidget(mainPanelSplitter);
        logFrame = new QFrame(logWindowSplitter);
        logFrame->setFrameShape(QFrame::NoFrame);
        logFrame->setFrameShadow(QFrame::Plain);
        logFrameLayout = new QVBoxLayout(logFrame);
        logFrameLayout->setSpacing(6);
        logFrameLayout->setContentsMargins(11, 11, 11, 11);

        logFrameTable = new QTableWidget(logFrame);
        logFrameTable->setRowCount(3);
        logFrameTable->setColumnCount(3);

        logFrameLayout->addWidget(logFrameTable);

        logWindowSplitter->addWidget(logFrame);

        mainWidgetLayout->addWidget(MenuBar);
        mainWidgetLayout->addWidget(logWindowSplitter);

        MainPanelBase->setCentralWidget(mainWidget);
        retranslateUi(MainPanelBase);

        QMetaObject::connectSlotsByName(MainPanelBase);
    } // setupUi

    void retranslateUi(QMainWindow *MainPanelBase)
    {
        MainPanelBase->setWindowTitle(QApplication::translate("MainPanelBase", "Calibration Console", 0));
        mainWindowLeftPanel->setTitle(QString());
        m_listenOnIsSwitch->setText(QApplication::translate("MainPanelBase", "Listen on IS server :", 0));
        mainWindowRightPanel->setTabText(mainWindowRightPanel->indexOf(tab), QApplication::translate("MainPanelBase", "Data List", 0));
        User->setTitle(QApplication::translate("MainPanelBase", "Use&r", 0));
        File->setTitle(QApplication::translate("MainPanelBase", "&File", 0));
        Utils->setTitle(QApplication::translate("MainPanelBase", "&Utils", 0));
    } // retranslateUi

};

namespace Ui {
    class MainPanelBase: public Ui_MainPanelBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINPANELBASE_H
