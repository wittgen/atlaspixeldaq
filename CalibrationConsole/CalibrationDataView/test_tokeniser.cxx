#include "Tokeniser.h"
#include <iostream>

int main(int argc, char **argv)
{

  std::vector<std::string> token_type_names;

  token_type_names.push_back("Value");
  token_type_names.push_back("Name");
  token_type_names.push_back("WildCard");
  token_type_names.push_back("Operator");
  token_type_names.push_back("IllegalToken");
  token_type_names.push_back("BlockOpen");
  token_type_names.push_back("BlockClose");

  PixCon::TokenDescription description;
  enum EOperators {kAnd, kOr, kNot, kGreater, kGreaterEqual,  kLess, kLessEqual, kEqual, kNotEqual};

  description.addOperator(">=",kGreaterEqual);
  description.addOperator(">",kGreater);
  description.addOperator("<",kLess);
  description.addOperator("<=",kLessEqual);
  description.addOperator("==",kEqual);
  description.addOperator("!=",kNotEqual);
  description.addOperator("&&",kAnd);
  description.addOperator("||",kOr);
  description.addOperator("!",kNot); // must be after != otherwise != will always be interpreted as ! + =
  description.setAllowedSpecialCharList("_:.");

  for (int arg_i=1; arg_i<argc; arg_i++) {
    std::string expression(argv[arg_i]);
    PixCon::Tokeniser tokeniser(description, &expression);
    while ( tokeniser.nextToken() ) {
      PixCon::Tokeniser::ETokenType type = tokeniser.type(); 
      std::string type_name="unknown";
      if (static_cast<unsigned int>(type) < token_type_names.size()) {
	type_name = token_type_names[type];
      }
      std::cout << type_name << " : \"" << tokeniser.token() << "\"" << std::endl;
    }
  }
  return 0;
}
