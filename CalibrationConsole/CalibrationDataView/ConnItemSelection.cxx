#include "ConnItemSelection.h"
#include <CalibrationDataManager.h>
#include <CalibrationDataStyle.h>


#include "Tokeniser.h"
#include "FilterParser.h"
#include "EvaluatorFactory.h"

namespace PixCon {
//   ConnItemSelection::ConnItemSelection()
//     : m_hasExpression(false)
//   {
//   }

  bool ConnItemSelection::s_initialised=false;
  PixCon::TokenDescription     *ConnItemSelection::s_tokenDescription;

  ConnItemSelection::ConnItemSelection(const std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data_manager,
				       const std::shared_ptr<PixCon::DataSelection> &data_selection) 
    : m_calibrationDataManager(calibration_data_manager),
      m_dataSelection(data_selection),
      m_factory(new EvaluatorFactory( m_evaluationParameter,m_valueList, m_stateList)),
      m_filterExpressionTree()
  {
    if (!s_initialised) {
      s_tokenDescription=new TokenDescription;
      s_tokenDescription->addOperator(">=",PixCon::EvaluatorFactory::kGreaterEqual+PixCon::LogicParserBase<EvalResult_t>::kNLogicOperators);
      s_tokenDescription->addOperator(">",PixCon::EvaluatorFactory::kGreater+PixCon::LogicParserBase<EvalResult_t>::kNLogicOperators);
      s_tokenDescription->addOperator("<=",PixCon::EvaluatorFactory::kLessEqual+PixCon::LogicParserBase<EvalResult_t>::kNLogicOperators);
      s_tokenDescription->addOperator("<",PixCon::EvaluatorFactory::kLess+PixCon::LogicParserBase<EvalResult_t>::kNLogicOperators);
      s_tokenDescription->addOperator("==",PixCon::EvaluatorFactory::kEqual+PixCon::LogicParserBase<EvalResult_t>::kNLogicOperators);
      s_tokenDescription->addOperator("!=",PixCon::EvaluatorFactory::kNotEqual+PixCon::LogicParserBase<EvalResult_t>::kNLogicOperators);
      s_tokenDescription->addOperator("&&",PixCon::LogicParserBase<EvalResult_t>::kAnd);
      s_tokenDescription->addOperator("||",PixCon::LogicParserBase<EvalResult_t>::kOr);
      s_tokenDescription->addOperator("!",PixCon::LogicParserBase<EvalResult_t>::kNot); // must be after != otherwise != will always be interpreted as ! + =
      unsigned int offset =PixCon::LogicParserBase<EvalResult_t>::kNLogicOperators + PixCon::EvaluatorFactory::kNCompareOperators;
      s_tokenDescription->addOperator("-",PixCon::EvaluatorFactory::kMinus+offset); // must be after != otherwise != will always be interpreted as ! + =
      s_tokenDescription->addOperator("+",PixCon::EvaluatorFactory::kPlus+offset);
      s_tokenDescription->addOperator("*",PixCon::EvaluatorFactory::kMultiply+offset);
      s_tokenDescription->addOperator("/",PixCon::EvaluatorFactory::kDivide+offset);
      offset+=PixCon::EvaluatorFactory::kNArithmeticOperators;
      s_tokenDescription->addOperator(".",PixCon::EvaluatorFactory::kSeparator+offset);
      s_tokenDescription->setAllowedSpecialCharList("_:.");
      s_initialised=true;
    }
  }

//   ConnItemSelection::ConnItemSelection(CalibrationData &calibration_data, const std::string &selection_pattern) 
//     : m_hasExpression( !selection_pattern.empty() )
//   {
//     try {
//       m_filterExpressionTree.compileExpression();
//     }
//     catch (PixCon::LogicParserBase::ParseError &err) {
//     }

//   }

  ConnItemSelection::~ConnItemSelection() 
  {
  }

  void ConnItemSelection::setFilterString(const std::string &selection_pattern) 
  {
    
    if (selection_pattern.empty()) {
      m_filterExpressionTree.reset();
      return;
    }
    assert(s_tokenDescription);

    m_valueList.clear();
    m_stateList.clear();
    m_filterExpressionTree = std::make_unique<FilterParser>(*m_factory);
    PixCon::Tokeniser tokeniser(*s_tokenDescription, &selection_pattern);
    m_filterExpressionTree->compileExpression(tokeniser);
     m_conditionVars.clear();
    m_valueList.getVarDefs(m_conditionVars);
    m_stateList.getVarDefs(m_conditionVars);
  }

  EvalResult_t ConnItemSelection::isSelected(const std::string &connectivity_name)
  {
    if (!m_filterExpressionTree) return true;
    try {
      m_evaluationParameter.setObject(connectivity_name,
				      m_dataSelection->type(),
				      m_dataSelection->serialNumber(),
				      m_calibrationDataManager->calibrationData());

      return m_filterExpressionTree->evaluate();
    }
    catch (CalibrationData::MissingValue &err) {
    }
    return EvalResult_t();
  }


}
