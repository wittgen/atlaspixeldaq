#include <ipc/partition.h>
#include <ipc/core.h>

#include <is/infodictionary.h>
#include <is/inforeceiver.h>
#include <is/infoiterator.h>
#include <is/callbackinfo.h>
#include <is/infoT.h>

#include "test_isUtil.h"

#include <cassert>

#include <vector>
#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <sstream>

#include <time.h>

int main(int argc, char **argv) {
  // GET COMMAND LINE

  // Check arguments

  std::string ipcPartitionName;
  std::string is_server("RunParams");
  std::string rod_name;
  std::string module_name;

  bool error=false;
  bool enabled=true;
  
  //  bool ping_only=false;
  //  bool query_queue_states=false;
  //  bool reset_queues=false;
  for (int arg_i=1; arg_i<argc; arg_i++) {

    if (strcmp(argv[arg_i],"-p")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      ipcPartitionName = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-n")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      is_server = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-r")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      rod_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-m")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      module_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-e")==0) {
      enabled = true;
    }
    else if (strcmp(argv[arg_i],"-d")==0) {
      enabled = false;
    }
    else {
      error=true;
    }
  }


  if( error || ipcPartitionName.empty() || is_server.empty() || rod_name.empty() ) {
      std::cout << "USAGE: " << argv[0] << " -p partition [-n IS-server-name] -r rod_name."  << std::endl
		<< "\t-p partition name\t the partition of the CAN/PixScan is server." << std::endl
		<< "\t-n IS-server-name\t the name of the CAN/PixScan is server." << std::endl
		<< "\t-r ROD name\t\tThe ROD name whose active status should be changed or the rod of the module." << std::endl
		<< "\t-r module name\t\tThe module for whose active status should be changed ." << std::endl
		<< "\t-e \t\t\t enable the ROD or module." << std::endl
		<< "\t-d \t\t\t disable the ROD or module." << std::endl
		<< std::endl;
      return 1;
  }

  // START IPC

  // Start IPCCore
  IPCCore::init(argc, argv);

  IPCPartition ipcPartition(ipcPartitionName.c_str());
  if (!ipcPartition.isValid()) {
    std::cerr << "ERROR [" << argv[0] << ":main] Not a valid partition " << ipcPartitionName << std::endl;
    return EXIT_FAILURE;
  }

  std::stringstream is_val_name;
  is_val_name << is_server << '.' << rod_name << '/';
  if (!module_name.empty()) {
    is_val_name << module_name << '/';
  }
  is_val_name << "Active";

  int active_val = (enabled ?  1 : 0);
  std::cout << "INFO [" << argv[0] << ":main] set " << is_val_name.str() << " to "  << active_val  << std::endl;

  ISInfoDictionary dictionary(ipcPartition);
  ISInfoInt status;
  status.setValue(active_val);
  ISType a_type(status.type());
  setIsValue(dictionary, a_type, is_val_name.str() , status);
  return 0;
}
