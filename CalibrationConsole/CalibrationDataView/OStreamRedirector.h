#ifndef _OStreamRedirector_h_
#define _OStreamRedirector_h_

#include <string>
#include <ostream>
#include <Lock.h>
// source http://www.qtforum.org/article/24554/Displaying-stdcout-in-a-Text-Box.html

class IStreamListener
{
public:
  virtual ~IStreamListener() {}
  virtual void putLine(const std::string &line) = 0;
};

template< class Elem = char, class Tr = std::char_traits< Elem > >
class OStreamRedirector : public std::basic_streambuf< Elem, Tr >
{

public:
  /**
   * Constructor.
   * @param a_Stream the stream to redirect
   * @param a_Cb the callback function
   * @param a_pUsrData user data passed to callback
   */
  OStreamRedirector( std::ostream& a_stream, IStreamListener *listener) :
    m_stream( &a_stream ),
    m_listener(listener),
    m_abort(false)
  {
    m_lineBuffer.reserve(4096);

    //redirect stream
    m_pBuf = m_stream->rdbuf( this );
  }

  /**
   * Destructor.
   * Restores the original stream.
   */
  ~OStreamRedirector()
  {
    m_abort=true;
    PixCon::Lock lock(m_mutex);
    if (m_stream) {
      m_stream->rdbuf( m_pBuf );
    }
  }

  void restoreStream() {
    if  (m_stream) {
       m_stream->rdbuf( m_pBuf);
       m_stream = NULL;
    }
  }

  /**
   * Override xsputn and make it forward data to the callback function.
   */
  std::streamsize xsputn( const Elem* _ptr, std::streamsize _count )
  {
    forwardLine(_ptr, _count);
    return _count;
  }

  /**
   * Override overflow and make it forward data to the callback function.
   */
  typename Tr::int_type overflow( typename Tr::int_type v )
  {
    Elem ch = Tr::to_char_type( v );
    forwardLine( &ch, 1);
    return Tr::not_eof( v );
  }

protected:

  void forwardLine(const Elem *_elm, std::streamsize _count) {
    if (m_abort) return;
    PixCon::Lock lock(m_mutex);
    for (std::streamsize i=_count; i-->0; ) {
      if (!*_elm || *_elm=='\n') {
	m_listener->putLine(m_lineBuffer);
	m_lineBuffer="";
	m_lineBuffer.reserve(4096);
      }
      else {
	m_lineBuffer+=*_elm;
      }
      ++_elm;
    }
  }

  PixCon::Mutex                 m_mutex;
  std::basic_ostream<Elem, Tr> *m_stream;
  std::streambuf*               m_pBuf;
  IStreamListener              *m_listener;
  std::string                   m_lineBuffer;

  bool                          m_abort;
};



#endif
