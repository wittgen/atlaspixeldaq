#ifndef _PixCon_CoupledDetectorView_h_
#define _PixCon_CoupledDetectorView_h_

#include "DetectorView.h"
#include "ICalibrationDataListener.h"

class QTimer;

namespace PixCon {
  class IConnItemSelection;
  class ModuleList;

  class CoupledDetectorView : public DetectorView
  {

    Q_OBJECT

  public:
    CoupledDetectorView(PixCon::DetectorViewList &detector_view_list,
			bool setup_calibration_data,
			std::shared_ptr<PixCon::IContextMenu> &context_menu,
			const std::shared_ptr<PixDet::ILogoFactory> &logo_factory,
			const std::shared_ptr<PixCon::SlowUpdateList> &slow_update_list,
			QApplication &app,
			QWidget* parent = 0 );

    ~CoupledDetectorView();

    void setModuleList(ModuleList *module_list);

    bool updateConnItem(PixCon::EConnItemType conn_type,
			const std::string &conn_name,
			PixCon::EMeasurementType measurement_type,
			SerialNumber_t serial_number);

    void drawDetectorModel();

    void updateChanged();

    IConnItemSelection *currentSelection(bool selected, bool deselected);

    IConnItemSelection *selection(bool selected, bool deselected, EMeasurementType measurement_type, PixCon::SerialNumber_t serial_number);

  protected:
    bool isFocussed(const std::string &conn_name) const;

    bool usesVariable(EMeasurementType measurement_type, SerialNumber_t serial_number, VarId_t id) const;

    void listOfUsedMeasurements( std::map<EMeasurementType, SerialNumber_t> &used_measurements );

    void applyFilter();

  private slots:
    void updateItemFocus();
    void initiateConnItemSselection(EConnItemType type, const std::string &connectivity_name);
    void setItemSelection();

  private:

    class Listener : public ICalibrationDataListener
    {
    public:
      Listener(CoupledDetectorView *detector_view)
	: m_detectorView(detector_view)
      {}

      void update() {
	m_detectorView->updateChanged();
      }

      bool needUpdate(EMeasurementType measurement_type, SerialNumber_t serial_number, VarId_t var_id, const std::string &/*conn_name*/ ) {
	return m_detectorView->usesVariable(measurement_type, serial_number, var_id);
      }

    private:
      CoupledDetectorView *m_detectorView;
    };

    void removeListener();

    ModuleList *m_moduleList;
    std::vector< std::shared_ptr< ICalibrationDataListener > > m_listener;

    QTimer *m_selectTimer;
    std::string m_selectedConnItem;
    EConnItemType     m_selectedConnItemType;
  };

}
#endif
