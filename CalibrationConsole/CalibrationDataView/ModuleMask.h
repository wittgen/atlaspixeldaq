/* Dear emacs, this is -*-c++-*- */
#ifndef _PixCon_ModuleMask_h_
#define _PixCon_ModuleMask_h_

#include "ui_ModuleMaskBase.h"
#include <memory>

#include <CalibrationDataTypes.h>
#include <VarDefList.h>

#include <istream>

namespace PixA {
  class Regex_t;
  class ConnectivityRef;
}

namespace PixLib {
  class PixDisable;
}

namespace PixCon {

  class CalibrationDataManager;
  class CalibrationDataPalette;
  class MainPanel;
  class ModuleList;
  class ModuleMaskItem;

  class ModuleMask : public QWidget, public Ui_ModuleMaskBase
  {
    Q_OBJECT

  public:
    ModuleMask( const std::shared_ptr<CalibrationDataManager> &calibration_data,
		const std::shared_ptr<PixCon::CalibrationDataPalette> &palette,
		QApplication *app,
		ModuleList *module_list,
		MainPanel  *main_panel,
		QWidget* parent = 0 );

    ~ModuleMask();

    void addMask( const std::string &disable_name, unsigned int revision, const std::shared_ptr<const PixLib::PixDisable> &disable);

  public slots:

    void loadMask();
    void loadTextFile();
    void createMaskFromTextSelection();
    void createMaskFromFilter();

    void deleteSelectedMasks();
    void increaseLevel();
    void decreaseLevel();

    void maskSelectionChanged();
    void applyFilter();
    void applyFilter(bool invert);
    void clearFilter();

    void activateDeactivate(QTreeWidgetItem *item, int column);
    void maskContextMenu(const QPoint&);

    //new public slots:
    void sNewCombinedMask();
    void sLoadMask();
    void sLoadModuleList();
    void sMaskFromSelection();
    void sMaskFromFilter();
    void sDeleteMask();
    void sApplyFilter();
    void sApplyAntiFilter();
    void sToggleActive();


  signals:
    void changeVariable(const QString &category_name, const QString &variable_name);
    void selectSerialNumber(PixCon::EMeasurementType measurement_type, PixCon::SerialNumber_t serial_number);

  protected:
    void addMaskItem(QTreeWidgetItem *parent, const std::string &name, unsigned int revision);

    void createMaskFromText(std::istream &in, const std::string &name);

    void createEmptyGroup(const std::string &mask_group_name);
    static VarDef_t createMaskVar(const std::string &combined_mask_name);

    QTreeWidgetItem *findMaskGroup(const std::string &name);

    std::pair<State_t,State_t> maskState(const std::string &name);
    std::pair<State_t,State_t> maskState(const std::string &combined_mask_name,  const std::string &name);

    std::shared_ptr<CalibrationDataManager> calibrationData() const { return m_calibrationData; }

    void reorderStates();
    void reorderStates(QTreeWidgetItem *parent);

    void createMaskVariable(const std::string &mask_name, const std::shared_ptr<const PixLib::PixDisable> &a_pix_disable);
    void createCombinedMaskVariable(const std::string &mask_name);
    void createCombinedMaskVariable(QTreeWidgetItem *parent);

    void createMaskFromTextFile(const std::string &file_name);

  private:


    std::shared_ptr<CalibrationDataManager>   m_calibrationData;
    std::shared_ptr< CalibrationDataPalette > m_palette;

    typedef std::map< unsigned int , std::pair<std::shared_ptr<const PixLib::PixDisable>, std::string> > DisableRevisionMap_t;
    typedef std::map< std::string ,  DisableRevisionMap_t> DisableMap_t;

    DisableMap_t  m_disableMap;


    ModuleList                               *m_moduleList;
    MainPanel                                *m_mainPanel;

    ModuleMaskItem                           *m_lastDisable;

    unsigned int m_nSelectedItems;
    QTreeWidgetItem  *m_parent;
    ModuleMaskItem *m_selectedItem;
    ModuleMaskItem *m_prevSelectedItem;
    QApplication   *m_app;
    
    unsigned int    m_logLevel;

    QString         m_lastPath;

    static const std::string s_defaultMaskVarName;
    static const std::string s_maskHeaderName;
    static const std::string s_maskCategory;

  };

}
#endif
