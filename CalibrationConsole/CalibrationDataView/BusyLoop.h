#ifndef _BusyLoop_h_
#define _BusyLoop_h_

#include <qwaitcondition.h>
#include <qthread.h>
#include <qevent.h>
#include <qapplication.h>
#include <qdialog.h>
#include <memory>
#include <QTVisualiser/BusyLoopFactory.h>
#include <QTVisualiser/BusyLoopFactory.h>
#include <QMutex>
#include <QTimer>
#include <atomic>

class MessageEvent : public QEvent
{
public:
  MessageEvent(const QString &message) : QEvent(QEvent::User),m_message(message) { /*s_counter++;*/ }
  MessageEvent(const char *message) : QEvent(QEvent::User),m_message(message) { /*s_counter++;*/}

  ~MessageEvent() {/*s_counter--;*/ }

  const QString &message() { return m_message; }

/*  static unsigned int counter() {return s_counter; }*/
private:
  QString m_message;
/*  static unsigned int s_counter;*/
};

class BusyLoop :  public QObject, public PixA::IBusyLoop
{
  Q_OBJECT
public:
  /** Show a dialog after a given amount of time to show that the main application is busy.
   * @param app the qt application.
   * @param dialog the dialog to be shown after the given amount of time.
   * @param dialog_after_time show the dialog after this period (in milli seconds)
   * @param update_time process pending events every this (milli seconds)
   * @param parent the parent widget.
   * @param modal whether the widget should be modal or not.
   */
QMutex mutex;
  BusyLoop(QApplication *app,
	   QDialog* dialog = 0,
	   unsigned int dialog_after_time = 2500,
	   unsigned int update_time = 100);

  ~BusyLoop();

  /** Change the text displayed in the dialog.
   */
  void changeMessage(const std::string &message) { m_app->postEvent(this, new MessageEvent(QString::fromStdString(message)) ); }
//void changeMessage(const QString &message) { m_app->postEvent(this, new MessageEvent(message) ); }
  /** Ask the thread to close the dialog.
   */
  void close();
 QThread cThread;
  /** Close the dialog and wait until the thread has ended.
   */
  void closeWait();

  bool isVetoConnected() const {
    return m_vetoConnected;
  }
   void DoSetup(QThread &cThread);
public slots:
  void vetoAbort() {
    m_veto=true;
  }
   void DoWork();
  void releaseVeto() {
    m_veto=false;
    m_sleep.wakeOne();
  }

 signals:
  void updateMessage(const QString &);
   void isFinished();
protected:

  class InputFilter : public QObject 
  {
  public:
    InputFilter(QObject *busy_dialogue)
      : m_busyDialogue(busy_dialogue)
    {}

    bool eventFilter( QObject *the_watched_object, QEvent *an_event);
  private:
    QObject *m_busyDialogue;
  };

  bool event(QEvent *an_event);
  void run();

protected slots:
  void showDialog();

private:
  QMutex         m_exitMutex;
  QWaitCondition m_exit;
  QWaitCondition m_sleep;
  QApplication *m_app;
  InputFilter   m_eventFilter;

  std::unique_ptr<QDialog> m_dialog;
  QTimer  m_showTimer;

  unsigned int   m_updateTime; // ms

  QWaitCondition m_start;
  QMutex         m_startMutex;
  unsigned int m_lockCounter;

  std::atomic<bool>           m_abort;
  std::atomic<bool>           m_veto;
  std::atomic<bool>           m_vetoConnected;
  std::atomic<bool>           m_exitFlag;

  std::atomic<bool> m_isLocked;
  std::atomic<bool> m_started;

};
#endif

