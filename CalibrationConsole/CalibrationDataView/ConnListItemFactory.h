#ifndef _PixCon_ConnListItemFactory_H_
#define _PixCon_ConnListItemFactory_H_

#include "ConnListItem.h"
#include "CalibrationData.h"
#include <QTreeWidget>
#include <string>
#include <set>

namespace PixCon {


  class DataSelection;
  class ConnItemSelection;
  class ConnListItem;
  class ValueDescription_t;

  class ConnListItemFactory
  {
  public:

    ConnListItemFactory(QTreeWidget         *list_view,
			CalibrationData   &data_calibration,
			DataSelection     &data_selection,
			ConnItemSelection &item_selection,
			std::map<std::string, ConnListItem *> &item_map);

    ~ConnListItemFactory() {}

    ConnListItem *createModuleItem(ConnListItem *parent_item, const std::string &connectivity_name) {
      return createItem(parent_item, connectivity_name, kModuleItem);
    }

    ConnListItem *createPp0Item(ConnListItem *parent_item, const std::string &connectivity_name) {
      return createItem(parent_item, connectivity_name, kPp0Item);
    }

    ConnListItem *createRodItem(ConnListItem *parent_item, const std::string &connectivity_name)  {
      return createItem(parent_item, connectivity_name, kRodItem);
    }

    ConnListItem *createTimItem(ConnListItem *parent_item, const std::string &connectivity_name)  {
      return createItem(parent_item, connectivity_name, kTimItem);
    }

    ConnListItem *createCrateItem(ConnListItem *parent_item, const std::string &connectivity_name)  {
      return createItem(parent_item, connectivity_name, kCrateItem);
    }

    ConnListItem *createItem(ConnListItem *parent_item, const std::string &connectivity_name, EConnItemType type);

    static void setText(DataSelection &m_dataSelection,
			CalibrationData::ConnObjDataList &data,
			ConnListItem *an_item,
			unsigned int column_i,
			const ValueDescription_t &var_def);

    static void setEnable(DataSelection &data_selection,
			  CalibrationData::ConnObjDataList &data,
			  ConnListItem *an_item);

  protected:


  private:
    QTreeWidget              *m_listView;
    CalibrationData          *m_calibrationData;
    DataSelection            *m_dataSelection;
    ConnItemSelection        *m_itemSelection;
    std::map<std::string, ConnListItem *>  *m_itemMap;
    std::set<std::string>     m_openList;
  };

  
}
#endif
