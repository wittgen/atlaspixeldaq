#include "DetectorViewList.h"
#include "DetectorView.h"
#include "CoupledDetectorView.h"

#include <ConfigWrapper/ConnectivityManager.h>
#include <ConfigWrapper/ConnectivityUtil.h>
#include "CalibrationDataManager.h"
#include "DataSelectionSpecificContextMenu.h"

#include <BusyLoop.h>

namespace PixCon {

  DetectorViewList::DetectorViewList(const std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data,
                     const std::shared_ptr<PixCon::CalibrationDataPalette> &palette,
                     const std::shared_ptr<PixCon::CategoryList> &categories,
                     const std::shared_ptr<PixCon::IContextMenu> &context_menu,
		     const std::shared_ptr<PixDet::ILogoFactory> &logo_factory,
                     bool add_current)
    : m_calibrationData(calibration_data),
      m_palette(palette),
      m_categories(categories),
      m_contextMenu(context_menu),
      m_logoFactory(logo_factory),
      m_slowUpdateList(new SlowUpdateList),
      m_addCurrent(add_current)
  {
    assert( m_categories.get() );
  }

  DetectorViewList::~DetectorViewList()
  {
    for (std::vector<DetectorView *>::iterator view_iter = m_detectorView.begin();
	 view_iter != m_detectorView.end();
	 view_iter++) {
      (*view_iter)->hide();
      delete *view_iter;
    }
    m_detectorView.clear();
  }

  DetectorView *DetectorViewList::createNewView(QApplication &app,
						QWidget* parent )
  {
    std::shared_ptr<IContextMenu> the_context_menu;
    DataSelectionSpecificContextMenu * specific_context_menu = dynamic_cast<DataSelectionSpecificContextMenu *>(m_contextMenu.get());
    if (specific_context_menu) {
      the_context_menu = std::shared_ptr<IContextMenu>(specific_context_menu->clone());
    }
    else {
      the_context_menu = m_contextMenu;
    }

    m_detectorView.push_back( new CoupledDetectorView(*this,
						      m_detectorView.size()==0,
						      the_context_menu,
						      m_logoFactory,
						      m_slowUpdateList,
						      app,
						      parent) );

    m_detectorView.back()->updateCategories(m_categories);
    if (m_addCurrent) {
      if (m_detectorView.front() != m_detectorView.back() ) {
	m_detectorView.back()->copySerialNumbers( *(m_detectorView.front()) );
      }
      else {
	m_detectorView.back()->addCurrentSerialNumber();
      }
    }
    return m_detectorView.back();
  }

  void DetectorViewList::destroyView(DetectorView *view) {
    std::vector<DetectorView *>::iterator view_iter = find(m_detectorView.begin(),m_detectorView.end(),view);
    if (view_iter != m_detectorView.end() ) {
      QWidget *parent = (*view_iter)->parentWidget();

      if (parent) {
	delete parent;
      }
      else {
	delete *view_iter;
      }

      m_detectorView.erase(view_iter);
    }
  }

  void DetectorViewList::destroyAllButThis(QWidget *view)
  {
    DetectorView *this_view=NULL;
    for (std::vector<DetectorView *>::iterator view_iter = m_detectorView.begin();
	 view_iter != m_detectorView.end();
	 view_iter++) {
      if (*view_iter != view) {

	QWidget *parent = *view_iter;
	QWidget *tempWidget;
	while ( (tempWidget = parent->parentWidget())) {
	  parent = tempWidget;
	}

	if (parent != view ) {
	  if (parent) {
	    // remove element from list to prevent the destructor of parent
	    // to remove itself from the list
	    *view_iter = NULL;
	    delete parent;
	  }
	  else {
	    // remove element from list to prevent the destructor of parent
	    // to remove itself from the list
	    QWidget *temp=*view_iter;
	    *view_iter = NULL;
	    delete temp;
	  }
	}
	else {
	  this_view=*view_iter;
	}
      }
      else {
	this_view=*view_iter;
      }
    }
    m_detectorView.clear();
    if (this_view) {
      m_detectorView.push_back(this_view);
    }
  }


  void DetectorViewList::updateCategories(const std::shared_ptr<PixCon::CategoryList> &categories)
  {
    assert( categories.get() );
    m_categories = categories;
    for (std::vector<DetectorView *>::iterator view_iter = m_detectorView.begin();
	 view_iter != m_detectorView.end();
	 view_iter++) {

      (*view_iter)->updateCategories( m_categories ); 
    }
  }

  void DetectorViewList::updateConnItem(EConnItemType conn_type,
					const std::string &conn_name,
					EMeasurementType measurement_type,
					SerialNumber_t serial_number,
					const std::string &var_name,
					bool propagate_to_childs)
  {
    try {
    VarDef_t var_def = VarDef_t::invalid();
    for (std::vector<DetectorView *>::iterator view_iter = m_detectorView.begin();
	 view_iter != m_detectorView.end();
	 view_iter++) {

      if ((*view_iter)->selectedMeasurementType() == measurement_type && (*view_iter)->selectedSerialNumber() == serial_number ) {
	if (!var_def.isValid() && !var_name.empty()) {
	  var_def = PixCon::VarDefList::instance()->getVarDef(var_name);
	}
	if (var_name.empty() || var_def.id() == (*view_iter)->selectedVarId().id()) {
	  if (conn_type == kRodItem && var_def.connType() == kRodValue) {
	    // in case of rod values also update all connected modules
	    const PixA::ConnectivityRef conn = m_calibrationData->connectivity(measurement_type, serial_number);

	    PixA::Pp0List pp0_list = conn.pp0s(conn_name);
	    PixA::Pp0List::iterator pp0_begin = pp0_list.begin();
	    PixA::Pp0List::iterator pp0_end = pp0_list.end();
	    for (PixA::Pp0List::iterator pp0_iter = pp0_begin; pp0_iter != pp0_end; ++pp0_iter) {
	      PixA::ModuleList::iterator module_begin = PixA::modulesBegin(pp0_iter);
	      PixA::ModuleList::iterator module_end = PixA::modulesEnd(pp0_iter);
	      for (PixA::ModuleList::iterator module_iter = module_begin; module_iter != module_end; ++module_iter) {
		(*view_iter)->updateConnItem(kModuleItem, PixA::connectivityName(module_iter), measurement_type,serial_number);
	      }
	    }
	  }
	  (*view_iter)->updateConnItem(conn_type, conn_name, measurement_type,serial_number, propagate_to_childs);
	}
      }

    }
    }
    catch(...) {
    }

  }

  void DetectorViewList::updateConnItem(EConnItemType conn_type,
					const std::string &conn_name,
					EMeasurementType measurement_type,
					SerialNumber_t serial_number,
					bool propagate_to_childs)
  {
    for (std::vector<DetectorView *>::iterator view_iter = m_detectorView.begin();
	 view_iter != m_detectorView.end();
	 view_iter++) {

      if ((*view_iter)->selectedMeasurementType() == measurement_type && (*view_iter)->selectedSerialNumber() == serial_number ) {
	(*view_iter)->updateConnItem(conn_type, conn_name,measurement_type, serial_number, propagate_to_childs);
      }

    }

  }

  void DetectorViewList::hideSerialNumberSelector() {
    for (std::vector<DetectorView *>::iterator view_iter = m_detectorView.begin();
	 view_iter != m_detectorView.end();
	 view_iter++) {
      (*view_iter)->hideSerialNumberSelector();
    }
  }

  void DetectorViewList::clearAllSerialNumbers() {
    for (std::vector<DetectorView *>::iterator view_iter = m_detectorView.begin();
	 view_iter != m_detectorView.end();
	 view_iter++) {
      (*view_iter)->clearAllSerialNumbers();
    }
  }

  void DetectorViewList::addSerialNumber(EMeasurementType measurement_type, SerialNumber_t serial_number) {
    assert(measurement_type < kNMeasurements);
    std::string serial_number_string = makeSerialNumberString(measurement_type, serial_number);
    for (std::vector<DetectorView *>::iterator view_iter = m_detectorView.begin();
	 view_iter != m_detectorView.end();
	 view_iter++) {
      (*view_iter)->addSerialNumber(serial_number_string);
    }
  }

  void DetectorViewList::removeMeasurement(EMeasurementType measurement_type, SerialNumber_t serial_number) {
    assert(measurement_type < kNMeasurements);
    std::string serial_number_string = makeSerialNumberString(measurement_type, serial_number);
    for (std::vector<DetectorView *>::iterator view_iter = m_detectorView.begin();
	 view_iter != m_detectorView.end();
	 view_iter++) {
      (*view_iter)->removeSerialNumber(serial_number_string);
    }
    switch (measurement_type) {
    case kScan: {
      std::cout << "INFO [DetectorViewList::removeSerialNumber] remove analyses of scan  "<< serial_number_string << "." << std::endl;

      PixCon::Lock lock(m_calibrationData->calibrationDataMutex());
      //       const ScanMetaDataCatalogue &scan_catalogue=m_calibrationData->scanMetaDataCatalogue();
      //       const ScanMetaData_t *scan_meta_data = scan_catalogue.find(serial_number);
      //       std::vector<SerialNumber_t> analyses_of_scan(scan_meta_data->beginAnalysis(), scan_meta_data->endAnalysis());
      //       for (std::vector<SerialNumber_t>::const_iterator analysis_iter = analyses_of_scan.begin();
      // 	   analysis_iter !=  analyses_of_scan.end();
      // 	   analysis_iter++) {
      // 	m_calibrationData->removeAnalysis(serial_number);
      //       }

      std::cout << "INFO [DetectorViewList::removeSerialNumber] remove scan  "<< serial_number_string << "." << std::endl;
      m_calibrationData->removeScan(serial_number);
      break;
    }
    case kAnalysis: {
      std::cout << "INFO [DetectorViewList::removeSerialNumber] remove analysis " << serial_number_string << "." << std::endl;
      PixCon::Lock lock(m_calibrationData->calibrationDataMutex());
      m_calibrationData->removeAnalysis(serial_number);
      //       ScanMetaData_t *scan_meta_data = scan_catalogue.find(serial_number);
      //       if (scan_meta_data) {
      // 	scan_meta_data->removeAnalysis( serial_number );
      //       }
      break;
    }
    default: {
      std::cout << "ERROR [DetectorViewList::removeSerialNumber] Illegal specifier : type = " << measurement_type << " number = " << serial_number << std::endl;
      break;
    }
    }
  }

  void DetectorViewList::addCurrentSerialNumber() {
    for (std::vector<DetectorView *>::iterator view_iter = m_detectorView.begin();
	 view_iter != m_detectorView.end();
	 view_iter++) {
      (*view_iter)->addCurrentSerialNumber();
    }
  }

  void DetectorViewList::changeConnectivity(const std::string &object_tag,
					    const std::string &conn_tag,
					    const std::string &alias_tag,
					    const std::string &data_tag,
					    const std::string &cfg_tag,
					    const std::string &mod_cfg_tag,
					    PixA::Revision_t rev)
  {

    if (   object_tag != m_lastConnTag[0]
	   || conn_tag  != m_lastConnTag[1]
	   || alias_tag != m_lastConnTag[2]
	   || data_tag != m_lastConnTag[3]
	   || cfg_tag != m_lastConnTag[4]
	   || mod_cfg_tag != m_lastConnTag[5]) {
      {
      std::unique_ptr<PixA::IBusyLoop> busy_loop(PixA::BusyLoopFactory::busyLoop(std::string(" Loading connectivity : ")+conn_tag));

      PixCon::Lock lock(m_calibrationData->calibrationDataMutex());
      // reset the (enable) state of all connectivity objects
      m_calibrationData->calibrationData().resetCurrentEnableState();

      // @todo load connectivity in separate task to avoid 
      //       and process events from time to time until connectivity is loaded.
      m_calibrationData->changeCurrentConnectivity( object_tag, conn_tag, alias_tag, data_tag, cfg_tag, mod_cfg_tag, rev, true );
      
      }
      // remember the active connectiivty
      m_lastConnTag[0]=object_tag;
      m_lastConnTag[1]=conn_tag;
      m_lastConnTag[2]=alias_tag;
      m_lastConnTag[3]=data_tag;
      m_lastConnTag[4]=cfg_tag;
      m_lastConnTag[5]=mod_cfg_tag;
      
      updateViews(kCurrent,0);
    }
  }

  void DetectorViewList::updateViews(EMeasurementType measurement_type, SerialNumber_t serial_number)
  {
    assert(measurement_type<kNMeasurements && (serial_number>0 || measurement_type==kCurrent));
    // Changes of the connection between RODs and modules have an impact on visualisation.
    // So, the detector needs to be redrawn.

    for (std::vector<DetectorView *>::iterator view_iter = m_detectorView.begin();
	 view_iter != m_detectorView.end();
	 view_iter++) {

      if ((*view_iter)->selectedMeasurementType() == measurement_type &&  (*view_iter)->selectedSerialNumber() == serial_number) {
	(*view_iter)->drawDetectorModel();
	(*view_iter)->update(false);
      }
    }

  }

  void DetectorViewList::setSerialNumbers()
  {
    clearAllSerialNumbers();
    if (m_addCurrent) {
      addCurrentSerialNumber();
    }

    const ScanMetaDataCatalogue &scan_catalogue=m_calibrationData->scanMetaDataCatalogue();
    for (std::map<SerialNumber_t, ScanMetaData_t>::const_iterator scan_iter = scan_catalogue.begin();
	 scan_iter != scan_catalogue.end();
	 scan_iter++) {

      addSerialNumber(kScan, scan_iter->first);

      for (std::vector<SerialNumber_t>::const_iterator analysis_iter = scan_iter->second.beginAnalysis();
	   analysis_iter !=  scan_iter->second.endAnalysis();
	   analysis_iter++) {
	addSerialNumber(kAnalysis,*analysis_iter);
      }

    }

  }

  void DetectorViewList::updateCategory(PixCon::EMeasurementType measurement_type, PixCon::SerialNumber_t serial_number, const QString &category_name)
  {
    for (std::vector<DetectorView *>::iterator view_iter = m_detectorView.begin();
	 view_iter != m_detectorView.end();
	 view_iter++) {
      (*view_iter)->updateCategory(measurement_type, serial_number, category_name);
    }
  }

  /** Register a listener for calibration data updates which is updated with a certain delay only.
   */
  void DetectorViewList::registerListener(EMeasurementType measurement, SerialNumber_t serial_number, const std::shared_ptr<ICalibrationDataListener> &listener) {
    m_slowUpdateList->registerListener(measurement, serial_number, listener);
  }

    /** Deregister a listener for calibration data updates.
     */
  void DetectorViewList::deregisterListener(const std::shared_ptr<ICalibrationDataListener> &listener) {
    m_slowUpdateList->deregisterListener(listener);
  }

  bool DetectorViewList::checkSlowUpdates(EMeasurementType measurement, SerialNumber_t serial_number, VarId_t var_id, const std::string &conn_name) {
    return m_slowUpdateList->checkUpdate(measurement, serial_number, var_id, conn_name);
  }

  void DetectorViewList::processSlowUpdates() {
    m_slowUpdateList->update();
  }

  /** Return the number of listener for slow updates.
   */
  unsigned int DetectorViewList::nSlowUpdateListener() const {
    return m_slowUpdateList->nListener();
  }

}
