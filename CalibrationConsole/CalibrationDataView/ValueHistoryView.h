// Dear emacs, this is -*-c++-*-
#ifndef _ValueHistoryView_H_
#define _ValueHistoryView_H_

#include <ui_SummaryHistogramViewBase.h>
#include <SummaryHistogramView.h>
#include <CalibrationDataTypes.h>
#include "ICalibrationDataListener.h"
#include <CalibrationData.h>
#include <memory>
#include <QCloseEvent>
class TCanvas;
class TGraph;
class MyQRootCanvas;

namespace PixCon {

  class ISelection;
  class CalibrationDataManager;
  class SlowUpdateList;
  class VarDef_t;

  class ValueHistoryView : public QMainWindow, public Ui_SummaryHistogramViewBase
  {
    Q_OBJECT
  public:
    ValueHistoryView(const std::shared_ptr<SlowUpdateList> &slow_update_list,
		     const std::shared_ptr<ISelection> &selection,
		     const std::shared_ptr<CalibrationDataManager> &calibration_data,
		     EMeasurementType measurement_type,
		     SerialNumber_t serial_number,
		     VarDef_t var_def,
		     const std::string &conn_name,
		     bool enabled_only=true,
		     QWidget* parent = 0);

    ~ValueHistoryView();

    TCanvas *canvas();

    void updateGraphView();

    static ValueHistoryView *createHistoryView( const std::shared_ptr<SlowUpdateList> &slow_update_list,
						const std::shared_ptr<ISelection> &selection,
						const std::shared_ptr<CalibrationDataManager> &calibration_data,
						EMeasurementType measurement_type,
						SerialNumber_t serial_number,
						VarDef_t var_def,
						bool enabled_only=true);

    static ValueHistoryView *createHistoryView( const std::shared_ptr<SlowUpdateList> &slow_update_list,
						const std::shared_ptr<ISelection> &selection,
						const std::shared_ptr<CalibrationDataManager> &calibration_data,
						EMeasurementType measurement_type,
						SerialNumber_t serial_number,
						VarDef_t var_def,
						const std::string &conn_name,
						bool enabled_only=true);

  protected:
    void closeEvent(QCloseEvent*);
    void init();

    void init( EMeasurementType measurement_type,
	       SerialNumber_t serial_number,
	       VarDef_t var_def,
	       bool enabled_only=true);

    void init( const std::string &conn_name,
	       EMeasurementType measurement_type,
	       SerialNumber_t serial_number,
	       VarDef_t var_def,
	       bool enabled_only=true);

    TGraph *createGraph(TGraph *old_graph, 
			const PixCon::CalibrationData::ConnObjDataList &data_list,
			EMeasurementType measurement_type,
			SerialNumber_t serial_number,
			VarDef_t var_def,
			const std::string &conn_name);


    static std::string getTitle(const VarDef_t &var_def);

  private:

    class Listener : public ICalibrationDataListener
    {
    public:
      Listener(ValueHistoryView *history_view, EMeasurementType measurement_type, SerialNumber_t serial_number, VarDef_t var_def, const std::string &conn_name) 
	: m_measurementType(measurement_type),
	  m_serialNumber(serial_number),
	  m_varDef(var_def),
	  m_historyView(history_view)
      { addConnName(conn_name); }

      void update() {
	m_historyView->updateGraphView();
      }

      bool needUpdate(EMeasurementType measurement, SerialNumber_t serial_number, VarId_t var_id, const std::string &conn_name );

      EMeasurementType   measurementType() const { return m_measurementType; };
      SerialNumber_t     serialNumber() const    { return m_serialNumber; }
      const VarDef_t    &varDef() const          { return m_varDef; }
      const std::vector<std::string> &connNameList() const        { return m_connNameList; }

      void addConnName(const std::string &conn_name) { if (!conn_name.empty()) m_connNameList.push_back(conn_name);}

    private:
      EMeasurementType  m_measurementType;
      SerialNumber_t    m_serialNumber;
      VarDef_t          m_varDef;

      std::vector<std::string> m_connNameList;

      ValueHistoryView *m_historyView;
    };

    bool addConnObj(const std::string &conn_obj, bool enabled_only);

    std::shared_ptr<SlowUpdateList>         m_slowUpdateList;
    std::shared_ptr<ISelection>             m_selection;
    std::shared_ptr<CalibrationDataManager> m_calibrationData;
    std::shared_ptr<Listener>               m_listener;

    std::map< std::string, CalibrationData::ConnObjDataList > m_connObjects;
  };

}
#endif
