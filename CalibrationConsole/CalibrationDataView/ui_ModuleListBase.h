#ifndef UI_MODULELISTBASE_H
#define UI_MODULELISTBASE_H

#include <QTreeWidget>
#include <QTextEdit>
#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QComboBox>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QPushButton>
#include <QSplitter>
#include <QVBoxLayout>
#include <QWidget>

QT_BEGIN_NAMESPACE

class Ui_ModuleListBase
{
public:
    QVBoxLayout *vboxLayout;
    QHBoxLayout *hboxLayout;
    QPushButton *m_printButton;
    QPushButton *m_clearFilterButton;
    QPushButton *m_filterButton;
    QPushButton *m_fullLineEdit;
    QComboBox *m_filterSelector;
    QSplitter *m_splitter;
    QTextEdit *m_filterStringEditor;
    QTreeWidget *m_moduleList;

    void setupUi(QWidget *ModuleListBase)
    {
        if (ModuleListBase->objectName().isEmpty())
            ModuleListBase->setObjectName(QString::fromUtf8("ModuleListBase"));
        ModuleListBase->resize(586, 480);
        ModuleListBase->setMinimumSize(QSize(0, 40));
        vboxLayout = new QVBoxLayout(ModuleListBase);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        m_printButton = new QPushButton(ModuleListBase);
        m_printButton->setObjectName(QString::fromUtf8("m_printButton"));
        m_printButton->setIcon(QIcon(QPixmap(":/icons/images/fileprint.png")));
        m_printButton->setFlat(true);

        hboxLayout->addWidget(m_printButton);

        m_clearFilterButton = new QPushButton(ModuleListBase);
        m_clearFilterButton->setObjectName(QString::fromUtf8("m_clearFilterButton"));
        m_clearFilterButton->setIcon(QIcon(QPixmap(":/icons/images/gtk-clear.png")));
        m_clearFilterButton->setAutoDefault(false);
        m_clearFilterButton->setFlat(true);

        hboxLayout->addWidget(m_clearFilterButton);

        m_filterButton = new QPushButton(ModuleListBase);
        m_filterButton->setObjectName(QString::fromUtf8("m_filterButton"));
        const QIcon icon = QIcon(QPixmap(":/icons/images/stock_autofilter.png"));
        m_filterButton->setIcon(icon);
        m_filterButton->setFlat(true);

        hboxLayout->addWidget(m_filterButton);

        m_fullLineEdit = new QPushButton(ModuleListBase);
        m_fullLineEdit->setObjectName(QString::fromUtf8("m_fullLineEdit"));
        m_fullLineEdit->setIcon(QIcon(QPixmap(":/icons/images/edit-right.png")));
        m_fullLineEdit->setFlat(true);

        hboxLayout->addWidget(m_fullLineEdit);

        m_filterSelector = new QComboBox(ModuleListBase);
        m_filterSelector->setObjectName(QString::fromUtf8("m_filterSelector"));
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(7), static_cast<QSizePolicy::Policy>(5));
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(m_filterSelector->sizePolicy().hasHeightForWidth());
        m_filterSelector->setSizePolicy(sizePolicy);
        m_filterSelector->setEditable(true);
        m_filterSelector->setAutoCompletion(true);

        hboxLayout->addWidget(m_filterSelector);


        vboxLayout->addLayout(hboxLayout);

        m_splitter = new QSplitter(ModuleListBase);
        m_splitter->setObjectName(QString::fromUtf8("m_splitter"));
        m_splitter->setOrientation(Qt::Vertical);
        m_filterStringEditor = new QTextEdit(m_splitter);
        m_filterStringEditor->setObjectName(QString::fromUtf8("m_filterStringEditor"));
        m_filterStringEditor->setMinimumSize(QSize(0, 40));
        m_splitter->addWidget(m_filterStringEditor);
        m_moduleList = new QTreeWidget(m_splitter);

        QStringList headerLabels;
        headerLabels.push_back("Name");
        headerLabels.push_back("Status");
        headerLabels.push_back("Value");
        m_moduleList->setColumnCount(headerLabels.count());
        m_moduleList->setHeaderLabels(headerLabels);
        m_moduleList->setSortingEnabled(true);
        m_moduleList->header()->resizeSection(0,320);
        m_moduleList->setContextMenuPolicy(Qt::CustomContextMenu);

        m_splitter->addWidget(m_moduleList);

        vboxLayout->addWidget(m_splitter);


        retranslateUi(ModuleListBase);

        QMetaObject::connectSlotsByName(ModuleListBase);
    } // setupUi

    void retranslateUi(QWidget *ModuleListBase)
    {
        ModuleListBase->setWindowTitle(QApplication::translate("ModuleListBase", "ModuleList", 0));
        ModuleListBase->setWindowIconText(QString());
        m_printButton->setText(QString());
#ifndef QT_NO_TOOLTIP
        m_printButton->setProperty("toolTip", QVariant(QApplication::translate("ModuleListBase", "Print module list", 0)));
#endif // QT_NO_TOOLTIP
        m_clearFilterButton->setText(QString());
#ifndef QT_NO_TOOLTIP
        m_clearFilterButton->setProperty("toolTip", QVariant(QApplication::translate("ModuleListBase", "Clear module filter", 0)));
#endif // QT_NO_TOOLTIP
        m_filterButton->setText(QApplication::translate("ModuleListBase", "Modules", 0));
#ifndef QT_NO_TOOLTIP
        m_filterButton->setProperty("toolTip", QVariant(QApplication::translate("ModuleListBase", "Apply module filter", 0)));
#endif // QT_NO_TOOLTIP
        m_fullLineEdit->setText(QString());
#ifndef QT_NO_TOOLTIP
        m_fullLineEdit->setProperty("toolTip", QVariant(QApplication::translate("ModuleListBase", "Show/hide filter string editor", 0)));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        m_filterSelector->setProperty("toolTip", QVariant(QApplication::translate("ModuleListBase", "Filter expression for module list : variable values or name wildcards", 0)));
#endif // QT_NO_TOOLTIP
//        m_moduleList->header()->setLabel(0, QApplication::translate("ModuleListBase", "Name", 0));
//        m_moduleList->header()->setLabel(1, QApplication::translate("ModuleListBase", "Status", 0));
//        m_moduleList->header()->setLabel(2, QApplication::translate("ModuleListBase", "Value", 0));
    } // retranslateUi

};

namespace Ui {
    class ModuleListBase: public Ui_ModuleListBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MODULELISTBASE_H
