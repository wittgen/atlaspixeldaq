#ifndef _PixCon_StatusChangeEvent_h_
#define _PixCon_StatusChangeEvent_h_

#include <qevent.h>
#include <CalibrationDataTypes.h>

namespace PixCon {

  class StatusChangeEvent : public QEvent
  {
  public:
    StatusChangeEvent(EMeasurementType measurement_type, SerialNumber_t serial_number)
      : QEvent(QEvent::User),
	m_measurementType(measurement_type),
	m_serialNumber(serial_number)
    {}

    EMeasurementType   measurement() const { return m_measurementType; }
    SerialNumber_t serialNumber() const { return m_serialNumber; }

  private:
    EMeasurementType   m_measurementType;
    SerialNumber_t m_serialNumber;
  };


}

#endif
