#include "IsActiveMonitor.h"
#include "IsReceptor.h"
#include "QIsInfoEvent.h"

namespace PixCon {

  void IsActiveMonitor::setValue(const std::string &is_name, ::time_t /*time_stamp*/, int value) {
    receptor()->setEnabled(valueConnType(), kCurrent, 0, is_name, *m_varNameExtractor, (value==1));
  }

  void IsActiveMonitor::registerForCleanup(const std::string &is_name, std::map<std::string, std::set<std::string> > &clean_up_var_list) {
    ConnVar_t conn_var(valueConnType()  /* unused */);
    m_varNameExtractor->extractVar(is_name,conn_var);
    this->receptor()->registerForCleanup(kCurrent,0,conn_var.connName(), conn_var.varName(), clean_up_var_list);
  }


}
