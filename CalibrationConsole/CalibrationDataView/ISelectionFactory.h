#ifndef _PixCon_ISelectionFactory_h_
#define _PixCon_ISelectionFactory_h_

namespace PixCon {
  class IConnItemSelection;

  class ISelectionFactory
  {
  public:
    virtual ~ISelectionFactory() {}

    virtual IConnItemSelection *selection(bool selected, bool deselected) = 0;
    virtual IConnItemSelection *selection(bool selected, bool deselected, EMeasurementType measurement, SerialNumber_t serial_number) = 0;
  };

}
#endif
