#ifndef UI_MODULELISTVIEWBASE_H
#define UI_MODULELISTVIEWBASE_H

#include <QTextEdit>
#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QDialog>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QPushButton>
#include <QSpacerItem>
#include <QTabWidget>
#include <QVBoxLayout>
#include <QWidget>

QT_BEGIN_NAMESPACE

class Ui_ModuleListViewBase
{
public:
    QVBoxLayout *vboxLayout;
    QTabWidget *listTabs;
    QWidget *modulesTab;
    QVBoxLayout *vboxLayout1;
    QTextEdit *modulesTextEdit;
    QWidget *rodsTab;
    QVBoxLayout *vboxLayout2;
    QTextEdit *rodsTextEdit;
    QHBoxLayout *hboxLayout;
    QSpacerItem *spacer1;
    QPushButton *closeButton;

    void setupUi(QDialog *ModuleListViewBase)
    {
        if (ModuleListViewBase->objectName().isEmpty())
            ModuleListViewBase->setObjectName(QString::fromUtf8("ModuleListViewBase"));
        ModuleListViewBase->resize(684, 591);
        ModuleListViewBase->setSizeGripEnabled(true);
        vboxLayout = new QVBoxLayout(ModuleListViewBase);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        listTabs = new QTabWidget(ModuleListViewBase);
        listTabs->setObjectName(QString::fromUtf8("listTabs"));
        modulesTab = new QWidget();
        modulesTab->setObjectName(QString::fromUtf8("modulesTab"));
        vboxLayout1 = new QVBoxLayout(modulesTab);
        vboxLayout1->setSpacing(6);
        vboxLayout1->setContentsMargins(11, 11, 11, 11);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        modulesTextEdit = new QTextEdit(modulesTab);
        modulesTextEdit->setObjectName(QString::fromUtf8("modulesTextEdit"));

        vboxLayout1->addWidget(modulesTextEdit);

        listTabs->addTab(modulesTab, QString());
        rodsTab = new QWidget();
        rodsTab->setObjectName(QString::fromUtf8("rodsTab"));
        vboxLayout2 = new QVBoxLayout(rodsTab);
        vboxLayout2->setSpacing(6);
        vboxLayout2->setContentsMargins(11, 11, 11, 11);
        vboxLayout2->setObjectName(QString::fromUtf8("vboxLayout2"));
        rodsTextEdit = new QTextEdit(rodsTab);
        rodsTextEdit->setObjectName(QString::fromUtf8("rodsTextEdit"));

        vboxLayout2->addWidget(rodsTextEdit);

        listTabs->addTab(rodsTab, QString());

        vboxLayout->addWidget(listTabs);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        spacer1 = new QSpacerItem(631, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacer1);

        closeButton = new QPushButton(ModuleListViewBase);
        closeButton->setObjectName(QString::fromUtf8("closeButton"));

        hboxLayout->addWidget(closeButton);


        vboxLayout->addLayout(hboxLayout);


        retranslateUi(ModuleListViewBase);
        QObject::connect(closeButton, SIGNAL(clicked()), ModuleListViewBase, SLOT(close()));

        QMetaObject::connectSlotsByName(ModuleListViewBase);
    } // setupUi

    void retranslateUi(QDialog *ModuleListViewBase)
    {
        ModuleListViewBase->setWindowTitle(QApplication::translate("ModuleListViewBase", "ModuleListView", 0));
        listTabs->setTabText(listTabs->indexOf(modulesTab), QApplication::translate("ModuleListViewBase", "Modules", 0));
        listTabs->setTabText(listTabs->indexOf(rodsTab), QApplication::translate("ModuleListViewBase", "Rods", 0));
        closeButton->setText(QApplication::translate("ModuleListViewBase", "&Close", 0));
        closeButton->setShortcut(QApplication::translate("ModuleListViewBase", "Alt+C", 0));
    } // retranslateUi

};

namespace Ui {
    class ModuleListViewBase: public Ui_ModuleListViewBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MODULELISTVIEWBASE_H
