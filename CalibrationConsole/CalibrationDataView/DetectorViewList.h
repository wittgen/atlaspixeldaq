#ifndef _DetectorViewList_H_
#define _DetectorViewList_H_

#include "Lock.h"
#include <memory>
#include <vector>
#include <string>

#include <ConfigWrapper/Connectivity.h>

#include <CalibrationDataTypes.h>
#include <VarDefList.h>
#include "CategoryList.h"

#include <qwidget.h>

class QWidget;
class DetectorView;

namespace PixDet {
  class ILogoFactory;
}

namespace PixCon {

  class CalibrationDataManager;
  class CalibrationDataPalette;
  class IContextMenu;
  class MainPanel;
  class SlowUpdateList;
  class ICalibrationDataListener;

  class DetectorViewList
  {
    friend class ::DetectorView;
    friend class MainPanel;

  public:
    /** Create the list of detector views.
     * @param calibration_data the calibration data to be shared among the views.
     * @param calibration_data_mutex the mutex to protect against concurrent changes to the structure of the calibration data
     * @param palette the color palette to be used in the views.
     * @param add_current if set to true the current measurement will be a possible selection.
     */
    DetectorViewList(const std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data,
		     const std::shared_ptr<PixCon::CalibrationDataPalette> &palette,
		     const std::shared_ptr<PixCon::CategoryList> &categories,
		     const std::shared_ptr<PixCon::IContextMenu> &context_menu,
    		     const std::shared_ptr<PixDet::ILogoFactory> &logo_factory,
		     bool add_current);

    ~DetectorViewList();

    DetectorView *createNewView(QApplication &app,
				QWidget* parent = 0);

    void destroyView(DetectorView *view);
    void destroyAllButThis(QWidget *view);

    /** Update the value of the given variable for the given conn item if the value has changed.
     */
    void updateConnItem(EConnItemType conn_type,
			const std::string &conn_name,
			EMeasurementType measurement_type,
			SerialNumber_t serial_number,
			const std::string &var_name,
			bool propagate_to_childs=false);

    /** Update values and enable state for the given conn item if the value or enable state has changed.
     */
    void updateConnItem(EConnItemType conn_type,
			const std::string &conn_name,
			EMeasurementType measurement_type,
			SerialNumber_t serial_number,
			bool propagate_to_childs=false);

    /** Hide serial number selector combo box of all views
     */
    void hideSerialNumberSelector();

    /** Clear the serial number lists of all views.
     */
    void clearAllSerialNumbers();

    /** Make a serial number known to the detector views..
     * @param measurement_type the type of the measurement (scan or analysis).
     * @param serial_number the serial number of the measurement.
     */
    void addSerialNumber(EMeasurementType measurement_type, SerialNumber_t serial_number);

    /** Remove a measurement from memory.
     * @param measurement_type the type of the measurement  (scan or analysis).
     * @param serial_number the serial number of the measurement.
     * This will remove all data from the calibration data container @ref CalibrationData,
     * the meta data catalogues and the connectivity. If a scan is removed, in addition all
     * associated analyses will be removed.
     * @todo Maybe this is not the right place for this functionality.
     */
    void removeMeasurement(EMeasurementType measurement_type, SerialNumber_t serial_number);

    void addCurrentSerialNumber();
    //    void removeSerialNumber(const std::string &serial_number);

    void changeConnectivity(const std::string &object_tag,
			    const std::string &conn_tag,
			    const std::string &alias_tag,
			    const std::string &data_tag,
			    const std::string &cfg_tag,
			    const std::string &mod_cfg_tag,
			    PixA::Revision_t rev);

    /** Enforce a redraw of all views which show the given measurement.
     * This is primarily of interest if the connectivity was changed and needed
     * some additional modifications e.g. deallocation of RODs.
     */
    void updateViews(EMeasurementType measurement_type, SerialNumber_t serial_number);

    void updateCategories(const std::shared_ptr<PixCon::CategoryList> &categories);

    /** Set the scan and analysis serial numbers from the meta data catalogues.
     * The metat data catalogues are provided by the calibration data manager.
     */
    void setSerialNumbers();

    unsigned int nViews() const {return m_detectorView.size(); }

    /** To be called if the category has been changed for the given measurement.
     * @param measurement_type the type of the measurement  (scan or analysis).
     * @param serial_number the serial number of the measurement.
     * @param category_name the name of the category.
     */
    void updateCategory(PixCon::EMeasurementType measurement_type, PixCon::SerialNumber_t serial_number, const QString &category_name);


    /** Register a listener for calibration data updates which is updated with a certain delay only.
     */
    void registerListener(EMeasurementType measurement, SerialNumber_t serial_number, const std::shared_ptr<ICalibrationDataListener> &listener);

    /** Deregister a listener for calibration data updates.
     */
    void deregisterListener(const std::shared_ptr<ICalibrationDataListener> &listener);

    /** Check whether a listener for slow updates need to be updated.
     * @return true if a listener requests a alow update.
     */
    bool checkSlowUpdates(EMeasurementType measurement, SerialNumber_t serial_number, VarId_t var_id, const std::string &conn_name);
  
    /** allow listener to update.
     */
    void processSlowUpdates();

    /** Return the number of listener for slow updates.
     */
    unsigned int nSlowUpdateListener() const;

  protected:

    std::shared_ptr<PixCon::CalibrationDataManager> &calibrationDataManager() { return m_calibrationData; }
    std::shared_ptr<PixCon::CalibrationDataPalette> &calibrationDataPalette() { return m_palette; }
    PixCon::CategoryList           &categories()                                { return *m_categories; }

    std::shared_ptr<SlowUpdateList> slowUpdateList() { return m_slowUpdateList; }

  private:
    std::shared_ptr<PixCon::CalibrationDataManager> m_calibrationData;
    std::shared_ptr<PixCon::CalibrationDataPalette> m_palette;
    std::shared_ptr<PixCon::CategoryList>           m_categories;
    std::shared_ptr<PixCon::IContextMenu>           m_contextMenu;
    std::shared_ptr<PixDet::ILogoFactory>           m_logoFactory;
    std::shared_ptr<SlowUpdateList>                 m_slowUpdateList;
    
    bool                                              m_addCurrent;

    std::string m_lastConnTag[6];
  
    std::vector< DetectorView *>                  m_detectorView;

  };

}
#endif
