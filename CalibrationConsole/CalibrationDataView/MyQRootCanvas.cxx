#include "MyQRootCanvas.h"
#include <TNamed.h>
#include <TCanvas.h>
#include <TSystem.h>
#include <QMouseEvent>
#include <QTimer>
#include "TVirtualX.h"

class PadRestore
{
public:
  PadRestore() : m_pad(gPad) {}
  ~PadRestore() {gPad = m_pad;}
private:
  TVirtualPad *m_pad;
};

#if  defined(HAVE_QTGSI)
MyQRootCanvas::MyQRootCanvas(QWidget *parent, const char *name)
  : TQRootCanvas(parent, name), m_canvas(0) {
#else
MyQRootCanvas::MyQRootCanvas(QWidget *parent, const char *)
  : QWidget(parent), m_canvas(0) {

  // set options needed to properly update the canvas when resizing the widget
  // and to properly handle context menus and mouse move events
  setAttribute(Qt::WA_PaintOnScreen, true);
  setAttribute(Qt::WA_OpaquePaintEvent, true);
  setMinimumSize(300, 200);
  setUpdatesEnabled(false);
  setMouseTracking(true);
  
  // register the QWidget in TVirtualX, giving its native window id
  int wid = gVirtualX->AddWindow((ULong_t)winId(), 600, 400);
  // create the ROOT TCanvas, giving as argument the QWidget registered id
  m_canvas = new TCanvas("Root Canvas", width(), height(), wid);
#endif

  m_timer = new QTimer(this);
  m_timer->setSingleShot(false);
  connect(m_timer, SIGNAL(timeout()), this, SLOT(updateCanvas()));
  m_timer->start(500);
}

MyQRootCanvas::~MyQRootCanvas()
{
  m_timer->stop();
}

void MyQRootCanvas::updateCanvas()
{
#if  defined(HAVE_QTGSI)
  this->Update();
#else
  gSystem->ProcessEvents();
#endif
}

void MyQRootCanvas::mouseMoveEvent(QMouseEvent *e) {
#if  defined(HAVE_QTGSI)
  TQRootCanvas::mouseMoveEvent(e);
#else
   if (m_canvas) {
      if (e->buttons() & Qt::LeftButton) {
         m_canvas->HandleInput(kButton1Motion, e->x(), e->y());
      } else if (e->buttons() & Qt::MidButton) {
         m_canvas->HandleInput(kButton2Motion, e->x(), e->y());
      } else if (e->buttons() & Qt::RightButton) {
         m_canvas->HandleInput(kButton3Motion, e->x(), e->y());
      } else {
         m_canvas->HandleInput(kMouseMotion, e->x(), e->y());
      }
   }
#endif

  if (e) {
    TCanvas *the_canvas=getCanvas();
    if (the_canvas) {
      PadRestore save_pad;
      gPad = the_canvas->GetSelectedPad();
      TObject *selected = the_canvas->GetSelected();
      if (selected) {
	const char *message_text = NULL;
	if (!selected->InheritsFrom(TVirtualPad::Class())) {
	  message_text = selected->GetObjectInfo(e->x(), e->y());
	}
	QString message;
	if (selected->InheritsFrom(TNamed::Class())) {
	  message += static_cast<TNamed *>(selected)->GetName();
	  message += " : ";
	}
	if (message_text) {
	  message+=message_text;
	}
	emit statusMessage(message);
      }
    }
  }
}

#if  !defined(HAVE_QTGSI)
void MyQRootCanvas::mousePressEvent( QMouseEvent *e )
{
   // Handle mouse button press events.

   if (m_canvas) {
      switch (e->button()) {
         case Qt::LeftButton :
            m_canvas->HandleInput(kButton1Down, e->x(), e->y());
            break;
         case Qt::MidButton :
            m_canvas->HandleInput(kButton2Down, e->x(), e->y());
            break;
         case Qt::RightButton :
            m_canvas->HandleInput(kButton3Down, e->x(), e->y());
            break;
         default:
            break;
      }
   }
}

void MyQRootCanvas::mouseReleaseEvent( QMouseEvent *e )
{
   // Handle mouse button release events.

   if (m_canvas) {
      switch (e->button()) {
         case Qt::LeftButton :
            m_canvas->HandleInput(kButton1Up, e->x(), e->y());
            break;
         case Qt::MidButton :
            m_canvas->HandleInput(kButton2Up, e->x(), e->y());
            break;
         case Qt::RightButton :
            m_canvas->HandleInput(kButton3Up, e->x(), e->y());
            break;
         default:
            break;
      }
   }
}

void MyQRootCanvas::resizeEvent( QResizeEvent * )
{
   // Handle resize events.

   if (m_canvas) {
      m_canvas->Resize();
      m_canvas->Update();
   }
}

void MyQRootCanvas::paintEvent( QPaintEvent * )
{
   // Handle paint events.

   if (m_canvas) {
      m_canvas->Resize();
      m_canvas->Update();
   }
}

void MyQRootCanvas::changeEvent(QEvent * e)
{
   if (e->type() == QEvent ::WindowStateChange) {
      QWindowStateChangeEvent * event = static_cast< QWindowStateChangeEvent * >( e );
      if (( event->oldState() & Qt::WindowMaximized ) ||
          ( event->oldState() & Qt::WindowMinimized ) ||
          ( event->oldState() == Qt::WindowNoState && 
            this->windowState() == Qt::WindowMaximized )) {
	if (m_canvas) {
	  m_canvas->Resize();
	  m_canvas->Update();
	}
      }
   }
}

QPaintEngine* MyQRootCanvas::paintEngine() const{
  return 0;
}
#endif

TCanvas* MyQRootCanvas::getCanvas() {
#if  defined(HAVE_QTGSI)
  return GetCanvas();
#else
  return m_canvas;
#endif
}
