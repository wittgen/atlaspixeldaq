#include <vector>
#include <iostream>
#include <memory>

#include "EvalResult_t.h"

//#include <TTimeMeasure.H>
//dummy implementation :
class TTimeMeasure {
public:
  void Start() {}
  void Stop() {}
  void Show() {}
};


class IEval
{
public:
  virtual ~IEval() {}
  virtual bool eval(unsigned int value) = 0;
};

class IEval2
{
public:
  virtual ~IEval2() {}
  virtual PixCon::EvalResult_t eval(unsigned int value) = 0;
};


class TestEval : public IEval
{
public:
  TestEval( unsigned int threshold) : m_threshold(threshold) {}
  bool eval(unsigned int value) { return value>m_threshold ;}

private:
  unsigned int m_threshold;
};

class TestEval2 : public IEval2
{
public:
  TestEval2( unsigned int threshold) : m_threshold(threshold) {}
  PixCon::EvalResult_t eval(unsigned int value) { return PixCon::EvalResult_t(value > m_threshold);}

private:
  unsigned int m_threshold;
};

class ExtTestEval2 : public IEval2
{
public:
  ExtTestEval2( unsigned int threshold) : m_threshold(threshold) {}
  PixCon::EvalResult_t eval(unsigned int value) { if (value==0) return PixCon::EvalResult_t::unknownResult(); else return PixCon::EvalResult_t(value > m_threshold);}

private:
  unsigned int m_threshold;
};

int main()
{
  
  assert( PixCon::EvalResult_t::trueResult() );
  assert( !PixCon::EvalResult_t::falseResult() );
  assert( !PixCon::EvalResult_t::unknownResult() );

  assert( PixCon::EvalResult_t(true) );
  assert( !PixCon::EvalResult_t(false) );
  assert( !PixCon::EvalResult_t() );

  assert( PixCon::EvalResult_t(!true) ==  !PixCon::EvalResult_t(true));
  assert( PixCon::EvalResult_t(!false) ==  !PixCon::EvalResult_t(false));
  assert( !PixCon::EvalResult_t() == PixCon::EvalResult_t());

  assert( PixCon::EvalResult_t(true) && PixCon::EvalResult_t(true) );
  assert( !( PixCon::EvalResult_t(true) && PixCon::EvalResult_t(false) ) );
  assert( !( PixCon::EvalResult_t(false) && PixCon::EvalResult_t(true) ) );
  assert( !( PixCon::EvalResult_t(false) && PixCon::EvalResult_t(false) ) );
  assert( !( PixCon::EvalResult_t(true) && PixCon::EvalResult_t() ) );
  assert( !( PixCon::EvalResult_t(false) && PixCon::EvalResult_t() ) );
  assert( !( PixCon::EvalResult_t() && PixCon::EvalResult_t(true) ) );
  assert( !( PixCon::EvalResult_t() && PixCon::EvalResult_t(false) ) );


  assert( PixCon::EvalResult_t(true) ||PixCon::EvalResult_t(true) );
  assert( ( PixCon::EvalResult_t(true) || PixCon::EvalResult_t(false) ) );
  assert( ( PixCon::EvalResult_t(false) || PixCon::EvalResult_t(true) ) );
  assert( !( PixCon::EvalResult_t(false) || PixCon::EvalResult_t(false) ) );
  assert( ( PixCon::EvalResult_t(true) || PixCon::EvalResult_t() ) );
  assert( !( PixCon::EvalResult_t(false) || PixCon::EvalResult_t() ) );
  assert( ( PixCon::EvalResult_t() || PixCon::EvalResult_t(true) ) );
  assert( !( PixCon::EvalResult_t() || PixCon::EvalResult_t(false) ) );

  std::vector<unsigned int> data;
  std::vector<unsigned int> data2;
  unsigned int sample_size=1000;
  unsigned int range=1000;
  for (unsigned int sample_i=0; sample_i<sample_size; sample_i++) {
    data.push_back( rand() % range );
    data2.push_back( rand() % range );
  }

  {
    std::unique_ptr<IEval> eval(new TestEval(range/2));

    TTimeMeasure timer;
    timer.Start();
    unsigned int passed=0;
    unsigned int sum=0;
    for (std::vector<unsigned int>::const_iterator iter2 = data2.begin();
	 iter2 != data2.end();
	 iter2++) {
    for (std::vector<unsigned int>::const_iterator iter = data.begin();
	 iter != data.end();
	 iter++) {
      if (eval->eval(*iter) && eval->eval(*iter2)) {
	passed++;
	sum += sum ^ *iter;
      }
    }
    }
    timer.Stop();
    timer.Show();
    std::cout << " ret = " << sum << " passed = " << passed <<std::endl;
  }

  {
    std::unique_ptr<IEval2> eval(new TestEval2(range/2));

    TTimeMeasure timer;
    timer.Start();
    unsigned int passed=0;
    unsigned int sum=0;
    for (std::vector<unsigned int>::const_iterator iter2 = data2.begin();
	 iter2 != data2.end();
	 iter2++) {
    for (std::vector<unsigned int>::const_iterator iter = data.begin();
	 iter != data.end();
	 iter++) {
      if (eval->eval(*iter) && eval->eval(*iter2)) {
	passed++;
	sum += sum ^ *iter;
      }
    }
    }
    timer.Stop();
    timer.Show();
    std::cout << " ret = " << sum << " passed = " << passed <<std::endl;
  }

  {
    std::unique_ptr<IEval2> eval(new ExtTestEval2(range/2));

    TTimeMeasure timer;
    timer.Start();
    unsigned int passed=0;
    unsigned int sum=0;
    for (std::vector<unsigned int>::const_iterator iter2 = data2.begin();
	 iter2 != data2.end();
	 iter2++) {
    for (std::vector<unsigned int>::const_iterator iter = data.begin();
	 iter != data.end();
	 iter++) {
      if (eval->eval(*iter) && eval->eval(*iter2)) {
	passed++;
	sum += sum ^ *iter;
      }
    }
    }
    timer.Stop();
    timer.Show();
    std::cout << " ret = " << sum << " passed = " << passed <<std::endl;
  }

  {
    std::unique_ptr<IEval2> eval(new ExtTestEval2(range/2));

    TTimeMeasure timer;
    timer.Start();
    unsigned int passed=0;
    unsigned int sum=0;
    unsigned int is_unknown=0;
    for (std::vector<unsigned int>::const_iterator iter2 = data2.begin();
	 iter2 != data2.end();
	 iter2++) {
    for (std::vector<unsigned int>::const_iterator iter = data.begin();
	 iter != data.end();
	 iter++) {
      PixCon::EvalResult_t res = eval->eval(*iter) && eval->eval(*iter2);
      if (res.isUnknown()) {
	is_unknown++;
      }
      if (res) {
	passed++;
	sum += sum ^ *iter;
      }
    }
    }
    timer.Stop();
    timer.Show();
    std::cout << " ret = " << sum  << " unknown = "  << is_unknown << " passed = " << passed <<std::endl;
  }
	  
}
