#ifndef AXISRANGEBASE_H
#define AXISRANGEBASE_H
#include "ui_AxisRangeBase.h"
#include <qvariant.h>
#include <qdialog.h>
#include <QGridLayout>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QLabel;
class QLineEdit;
class QPushButton;

class AxisRangeBase : public QDialog, Ui_AxisRangeBase
{
    Q_OBJECT

public:
    AxisRangeBase( QWidget* parent = 0 );
    ~AxisRangeBase();

    QLabel* textLabel1;
    QLineEdit* m_minEntry;
    QLabel* textLabel2;
    QLineEdit* m_maxEntry;
    QPushButton* m_cancelButton;
    QPushButton* m_acceptButton;

protected:
    QVBoxLayout* AxisRangeBaseLayout;
    QGridLayout* layout1;
    QHBoxLayout* layout2;

protected slots:
    virtual void languageChange();

};

#endif // AXISRANGEBASE_H
