#ifndef _PixCon_LogicParser_h_
#define _PixCon_LogicParser_h_

#include "LogicParserBase.h"

namespace PixCon {

  class LogicParser : public LogicParserBase<bool>
  {
  public:

    class BooleanValue : public IEvaluator
    {
    public:
      BooleanValue(std::map<std::string, bool>::const_iterator value_iter) : m_valueIter(value_iter) {}
      bool eval() const { return m_valueIter->second; }
    private:
      std::map<std::string, bool>::const_iterator m_valueIter;
    };

    std::map<std::string, bool>::iterator begin()                         { return m_variables.begin(); }
    std::map<std::string, bool>::iterator end()                           { return m_variables.end();   }
    std::map<std::string, bool>::iterator find(const std::string &name)   { return m_variables.find(name);   }

  protected:
    void ParseNucleus(PixCon::Tokeniser &tokeniser, IEvaluator *&node_ptr, bool strong_expression_only);

  private:
    std::map<std::string, bool> m_variables;
  };

}

#endif
