#include <string>

#include <vector>
#include <iostream>
#include <sstream>

std::vector<std::string> s_connectivityNameParts;

bool checkConnName(const std::string &token)
{
  std::stringstream errors;
  if (s_connectivityNameParts.empty()) {
    s_connectivityNameParts.push_back("ROD");
    s_connectivityNameParts.push_back("CRATE");
  }

  // still could be a connectivity name pattern
  // try to find out whether it is a reasonable connectivity name pattern
  std::string::size_type pos=0;
  char next_char='\0';
  if (token[0]=='*') {
    next_char=token[0];
    pos++;
  }
  //std::string::size_type start_pos=pos;
  bool have_errors=false;
  while (pos < token.size() ) {
    std::string::size_type start_pos=pos;
    char prev_char =next_char;
    while (pos < token.size() && token[pos]!='_' && token[pos]!='*') pos++;
    if (pos != start_pos) {
      if (pos < token.size() ) next_char=token[pos];
      else next_char='\0';

      std::string sub_pattern(token,start_pos, pos-start_pos);
      //      std::cout << "INFO test sub expression = " << sub_pattern << std::endl;
      bool valid=false;
      for (std::vector<std::string>::const_iterator iter=s_connectivityNameParts.begin();
	   iter != s_connectivityNameParts.end();
	   iter++) {
	std::string::size_type a_pos = iter->find(sub_pattern);
	if ( a_pos != std::string::npos ) {
	  if ( (prev_char == '*' || a_pos==0)
	       && (next_char == '*' || a_pos + sub_pattern.size() == iter->size())) {
	    valid=true;
	    break;
	  }
	}
      }
      if (!valid && pos-start_pos<=3) {
	std::string::size_type a_pos=start_pos;
	bool side_tail=true;
	int max_number=26;
	int min_number=1;
	unsigned int min_digits=1;
	unsigned int max_digits=2;
	valid=true;
	switch (token[a_pos++]) {
	case 'A':
	case 'C':
	  max_number=7;
	  min_number=1;
	  min_digits=1;
	  max_digits=1;
	  side_tail=false;
	  break;
	case 'B':
	  max_number=26;
	  min_number=1;
	  min_digits=1;
	  max_digits=2;
	  side_tail=false;
	  break;
	case 'D':
	  max_number=3;
	  min_number=1;
	  min_digits=1;
	  max_digits=1;
	  side_tail=true;
	  break;
	case 'L':
	  max_number=2;
	  min_number=0;
	  min_digits=1;
	  max_digits=1;
	  side_tail=false;
	  break;
	case 'M':
	  max_number=6;
	  min_number=1;
	  min_digits=1;
	  max_digits=1;
	  side_tail=true;
	  if (a_pos<pos) {
	    if (token[a_pos]=='0') {
	      min_digits=1;
	      max_digits=1;
	      min_number=0;
	      side_tail=false;
	    }
	  }
	  break;
	case 'S':
	  max_number=21;
	  min_number=1;
	  min_digits=1;
	  max_digits=2;
	  side_tail=false;
	  break;
	default:
	  if (prev_char!='*') {
	    if (isdigit(token[a_pos-1])) {
	      // could be crate number
	      min_digits=1;
	      max_digits=1;
	      min_number=0;
	      max_number=8;
	      side_tail=false;
	      a_pos--;
	    }
	    else {
	      valid=false;
	    }
	  }
	  break;
	}
	if (valid) {
	  std::string::size_type number_pos;
	  for (number_pos=a_pos; number_pos < pos ; number_pos++) {
	    if (!isdigit(token[number_pos])) break;
	  }
	  if ( number_pos-a_pos > max_digits || ( (number_pos!=pos || next_char!='*') && (number_pos-a_pos<min_digits ))) {
	    valid=false;
	  }
	  else {
	    int number=atoi(&(token.c_str())[a_pos]);
	    if ( (number<min_number && (next_char!='*' || number_pos-a_pos == max_digits)) || number > max_number) {
	      valid=false;
	    }
	    if (side_tail) {
	      if (number_pos==pos) {
		if (next_char!='*') {
		  valid=false;
		}
	      }
	      else if (token[number_pos]!='A' && token[number_pos]!='C') {
		valid=false;
	      }
	    }
	    else {
	      if (number_pos!=pos) valid=false;
	    }
	  }
	}
      }

//       if (valid) {
// 	std::cout << " is valid." << std::endl;
//       }
      if (!valid) {
	if (have_errors) {
	  errors << ", ";
	}
	errors << sub_pattern;
	have_errors=false;
      }
    }
    if (pos < token.size()) {
      pos++;
    }
  }
  if (have_errors) {
    std::cerr << "ERROR [checkConnName] The following sub tokens will defnitely not match connectivity names : " << errors.str() << std::endl;
  }
  return !have_errors;
}
