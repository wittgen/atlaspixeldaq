
#include <EvalResult_t.h>
#include <FilterParser.h>
#include <EvaluatorFactory.h>
#include <CalibrationData.h>
//#include <ConnItemTypeBase.h>
#include "Tokeniser.h"
#include "EvaluatorFactory.h"

int main(int argc, char **argv)
{
  PixCon::CalibrationData calib_data;

  PixCon::VarDefList::instance()->createValueVarDef("Threshold Dispersion",PixCon::kModuleValue,-1,1);
  PixCon::VarDefList::instance()->createValueVarDef("T",PixCon::kModuleValue,0,40);
  enum EScanStatus {kScanUnknown, kScanConfiguring, kScanScanning, kScanSuccess, kScanFailure};
  {
    PixCon::StateVarDef_t &state_var_def = PixCon::VarDefList::instance()->createStateVarDef("Scan_Status",PixCon::kRodValue, kScanFailure+1).stateDef();
    state_var_def.addState(kScanUnknown,"Unknown");
    state_var_def.addState(kScanConfiguring,"Configuring");
    state_var_def.addState(kScanScanning,"Scanning");
    state_var_def.addState(kScanSuccess,"Success");
    state_var_def.addState(kScanFailure,"Failure");
  }

  enum EAnalysisStatus {kAnaUnknown,kAnaPending,kAnaWaiting,kAnaAnalysing,kAnaSuccess,kAnaFailure};
  {
    PixCon::StateVarDef_t &state_var_def = PixCon::VarDefList::instance()->createStateVarDef("Analysis_Status",PixCon::kRodValue, kAnaFailure+1).stateDef();
    state_var_def.addState(kAnaUnknown,"Unknown");
    state_var_def.addState(kAnaPending,"Pending");
    state_var_def.addState(kAnaWaiting,"Waiting");
    state_var_def.addState(kAnaAnalysing,"Analysing");
    state_var_def.addState(kAnaSuccess,"Success");
    state_var_def.addState(kAnaFailure,"Failure");
  }

  std::vector< std::string > conn_obj_list;
  conn_obj_list.push_back("ROD_C1_S1");
  conn_obj_list.push_back("ROD_C1_S2");
  conn_obj_list.push_back("ROD_C1_S3");
  conn_obj_list.push_back("ROD_C1_S4");
  conn_obj_list.push_back("D1A_B2_S1_M1");
  conn_obj_list.push_back("D1A_B2_S1_M2");
  conn_obj_list.push_back("D1A_B2_S1_M3");
  conn_obj_list.push_back("D1A_B2_S1_M4");

  calib_data.addAnalysisValue<float>(PixCon::kModuleValue, "D1A_B2_S1_M1", 1, "Threshold Dispersion",0.);
  calib_data.addAnalysisValue<float>(PixCon::kModuleValue, "D1A_B2_S1_M2", 1, "Threshold Dispersion",0.5);
  calib_data.addAnalysisValue<float>(PixCon::kModuleValue, "D1A_B2_S1_M3", 1, "Threshold Dispersion",-0.5);
  calib_data.addAnalysisValue<float>(PixCon::kModuleValue, "D1A_B2_S1_M4", 1, "Threshold Dispersion",-0.3);

  calib_data.addScanValue<PixCon::State_t>(PixCon::kRodValue, "ROD_C1_S1", 1,  "Scan_Status",static_cast<PixCon::State_t>(kScanScanning));
  calib_data.addScanValue<PixCon::State_t>(PixCon::kRodValue, "ROD_C1_S2", 1,  "Scan_Status",static_cast<PixCon::State_t>(kScanSuccess));
  calib_data.addScanValue<PixCon::State_t>(PixCon::kRodValue, "ROD_C1_S3", 1,  "Scan_Status",static_cast<PixCon::State_t>(kScanSuccess));
  calib_data.addScanValue<PixCon::State_t>(PixCon::kRodValue, "ROD_C1_S4", 1,  "Scan_Status",static_cast<PixCon::State_t>(kScanFailure));

  calib_data.addAnalysisValue<PixCon::State_t>(PixCon::kRodValue, "ROD_C1_S1", 1, "Analysis_Status" ,static_cast<PixCon::State_t>(kAnaWaiting));
  calib_data.addAnalysisValue<PixCon::State_t>(PixCon::kRodValue, "ROD_C1_S2", 1, "Analysis_Status" ,static_cast<PixCon::State_t>(kAnaPending));
  calib_data.addAnalysisValue<PixCon::State_t>(PixCon::kRodValue, "ROD_C1_S3", 1, "Analysis_Status" ,static_cast<PixCon::State_t>(kAnaPending));
  calib_data.addAnalysisValue<PixCon::State_t>(PixCon::kRodValue, "ROD_C1_S4", 1, "Analysis_Status" ,static_cast<PixCon::State_t>(kAnaSuccess));

  calib_data.addCurrentValue<float>(PixCon::kModuleValue, "D1A_B2_S1_M1", "T" ,20.);
  calib_data.addCurrentValue<float>(PixCon::kModuleValue, "D1A_B2_S1_M2", "T" ,25.);
  calib_data.addCurrentValue<float>(PixCon::kModuleValue, "D1A_B2_S1_M3", "T" ,30.);
  calib_data.addCurrentValue<float>(PixCon::kModuleValue, "D1A_B2_S1_M4", "T" ,35.);

//   {
//     unsigned int var_id=0;
//     std::cout << "INFO [" << argv[0] << ":main] variables : ";
//     while (PixCon::VarDefList::instance()->isValid(var_id)) {
//       std::cout << "\"" << PixCon::VarDefList::instance()->varName(var_id) << "\",";
//       var_id++;
//     }
//     std::cout << std::endl;
//   }
  PixCon::TokenDescription description;
 PixCon::TokenDescription *s_tokenDescription=&description;
      s_tokenDescription->addOperator(">=",PixCon::EvaluatorFactory::kGreaterEqual+PixCon::LogicParserBase<PixCon::EvalResult_t>::kNLogicOperators);
      s_tokenDescription->addOperator(">",PixCon::EvaluatorFactory::kGreater+PixCon::LogicParserBase<PixCon::EvalResult_t>::kNLogicOperators);
      s_tokenDescription->addOperator("<=",PixCon::EvaluatorFactory::kLessEqual+PixCon::LogicParserBase<PixCon::EvalResult_t>::kNLogicOperators);
      s_tokenDescription->addOperator("<",PixCon::EvaluatorFactory::kLess+PixCon::LogicParserBase<PixCon::EvalResult_t>::kNLogicOperators);
      s_tokenDescription->addOperator("==",PixCon::EvaluatorFactory::kEqual+PixCon::LogicParserBase<PixCon::EvalResult_t>::kNLogicOperators);
      s_tokenDescription->addOperator("!=",PixCon::EvaluatorFactory::kNotEqual+PixCon::LogicParserBase<PixCon::EvalResult_t>::kNLogicOperators);
      s_tokenDescription->addOperator("&&",PixCon::LogicParserBase<PixCon::EvalResult_t>::kAnd);
      s_tokenDescription->addOperator("||",PixCon::LogicParserBase<PixCon::EvalResult_t>::kOr);
      s_tokenDescription->addOperator("!",PixCon::LogicParserBase<PixCon::EvalResult_t>::kNot); // must be after != otherwise != will always be interpreted as ! + =
      unsigned int offset =PixCon::LogicParserBase<PixCon::EvalResult_t>::kNLogicOperators + PixCon::EvaluatorFactory::kNCompareOperators;
      s_tokenDescription->addOperator("-",PixCon::EvaluatorFactory::kMinus+offset); // must be after != otherwise != will always be interpreted as ! + =
      s_tokenDescription->addOperator("+",PixCon::EvaluatorFactory::kPlus+offset);
      s_tokenDescription->addOperator("*",PixCon::EvaluatorFactory::kMultiply+offset);
      s_tokenDescription->addOperator("/",PixCon::EvaluatorFactory::kDivide+offset);
      offset+=PixCon::EvaluatorFactory::kNArithmeticOperators;
      s_tokenDescription->addOperator(".",PixCon::EvaluatorFactory::kSeparator+offset);
      s_tokenDescription->setAllowedSpecialCharList("_:.");

  description.addOperator(">=",PixCon::EvaluatorFactory::kGreaterEqual+PixCon::LogicParserBase<PixCon::EvalResult_t>::kNLogicOperators);
  description.addOperator(">",PixCon::EvaluatorFactory::kGreater+PixCon::LogicParserBase<PixCon::EvalResult_t>::kNLogicOperators);
  description.addOperator("<=",PixCon::EvaluatorFactory::kLessEqual+PixCon::LogicParserBase<PixCon::EvalResult_t>::kNLogicOperators);
  description.addOperator("<",PixCon::EvaluatorFactory::kLess+PixCon::LogicParserBase<PixCon::EvalResult_t>::kNLogicOperators);
  description.addOperator("==",PixCon::EvaluatorFactory::kEqual+PixCon::LogicParserBase<PixCon::EvalResult_t>::kNLogicOperators);
  description.addOperator("!=",PixCon::EvaluatorFactory::kNotEqual+PixCon::LogicParserBase<PixCon::EvalResult_t>::kNLogicOperators);
  description.addOperator("&&",PixCon::LogicParserBase<PixCon::EvalResult_t>::kAnd);
  description.addOperator("||",PixCon::LogicParserBase<PixCon::EvalResult_t>::kOr);
  description.addOperator("!",PixCon::LogicParserBase<PixCon::EvalResult_t>::kNot); // must be after != otherwise != will always be interpreted as ! + =
  description.setAllowedSpecialCharList("_:.");

  PixCon::Tokeniser tokeniser();
  bool error=false;
  for (int arg_i=1; arg_i<argc; arg_i++) {
    std::string expression_string(argv[arg_i]);
    std::cout << "INFO [" << argv[0] << ":main] evaluate expression : \"" << expression_string << "\"." << std::endl;
    PixCon::Tokeniser tokeniser(description, &expression_string);

    PixCon::ValueList<float>    value_list;
    PixCon::ValueList<PixCon::State_t>  state_list;
    PixCon::EvaluationParameter eval_parameter("ROD_C1_S1",PixCon::kCurrent,0,calib_data );
    PixCon::EvaluatorFactory factory(eval_parameter,value_list, state_list);
    PixCon::FilterParser expression(factory);

    try {
      expression.compileExpression(tokeniser);

      std::cout << "INFO [" << argv[0] << ":main] Current" << std::endl;
      for (std::vector<std::string>::const_iterator conn_iter = conn_obj_list.begin(); conn_iter != conn_obj_list.end(); conn_iter++) {
	try {
	  //	  std::cout << "INFO [" << argv[0] << ":main] test for " << *conn_iter << std::endl;
	  eval_parameter.setObject(*conn_iter, PixCon::kCurrent,0,calib_data);
	  if (expression.evaluate()) {
	    std::cout << "INFO [" << argv[0] << ":main] selected " << *conn_iter << std::endl;
	  }
	}
	catch (PixCon::CalibrationData::MissingValue &err) {
	  //	  std::cout << "ERROR [" << argv[0] << ":main] caught exception : " << err.what() << std::endl;
	  //	  error=true;
	}
      }

      std::cout << "INFO [" << argv[0] << ":main] Scan" << std::endl;
      for (std::vector<std::string>::const_iterator conn_iter = conn_obj_list.begin(); conn_iter != conn_obj_list.end(); conn_iter++) {
	try {
	  //	  std::cout << "INFO [" << argv[0] << ":main] test for " << *conn_iter << std::endl;
	  eval_parameter.setObject(*conn_iter, PixCon::kScan,1,calib_data);
	  if (expression.evaluate()) {
	    std::cout << "INFO [" << argv[0] << ":main] selected "<< *conn_iter << std::endl;
	  }
	}
	catch (PixCon::CalibrationData::MissingValue &err) {
	  //	  std::cout << "ERROR [" << argv[0] << ":main] caught exception : " << err.what() << std::endl;
	  //	  error=true;
	}
      }

      std::cout << "INFO [" << argv[0] << ":main] Analysis" << std::endl;
      for (std::vector<std::string>::const_iterator conn_iter = conn_obj_list.begin(); conn_iter != conn_obj_list.end(); conn_iter++) {
	try {
	  //	  std::cout << "INFO [" << argv[0] << ":main] test for " << *conn_iter << std::endl;
	  eval_parameter.setObject(*conn_iter, PixCon::kAnalysis,1,calib_data);
	  if (expression.evaluate()) {
	    std::cout << "INFO [" << argv[0] << ":main] selected "<< *conn_iter << std::endl;
	  }
	}
	catch (PixCon::CalibrationData::MissingValue &err) {
	  //	  std::cout << "ERROR [" << argv[0] << ":main] caught exception : " << err.what() << std::endl;
	  //	  error=true;
	}
      }
    }
    catch (PixCon::LogicParserBase<PixCon::EvalResult_t>::ParseError &err) {
      std::cout << "ERROR [" << argv[0] << ":main] caught exception : " << err.what() << std::endl;
      error=true;
    }
  }

  return (error ? -1  : 0);
}
