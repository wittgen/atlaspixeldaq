/* Dear emacs, this is -*-c++-*- */
#ifndef _TemporaryCursorChange_h_
#define _TemporaryCursorChange_h_

#include <qapplication.h>
#include <qcursor.h>

class TemporaryCursorChange
{
public:
  TemporaryCursorChange( const QCursor & cursor = Qt::WaitCursor )
  { QApplication::setOverrideCursor( cursor ); }

  ~TemporaryCursorChange()
  { QApplication::restoreOverrideCursor(); }
};

#endif
