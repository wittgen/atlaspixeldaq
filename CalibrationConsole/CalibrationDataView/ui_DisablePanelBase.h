#ifndef UI_DISABLEPANELBASE_H
#define UI_DISABLEPANELBASE_H

#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QComboBox>
#include <QDialog>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QPushButton>
#include <QSpacerItem>
#include <QSplitter>
#include <QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_DisablePanelBase
{
public:
    QVBoxLayout *vboxLayout;
    QVBoxLayout *vboxLayout1;
    QLabel *textLabel1;
    QSpacerItem *spacer1;
    QVBoxLayout *vboxLayout2;
    QSplitter *splitter4;
    QLabel *textLabel2;
    QComboBox *nameBox;
    QSplitter *splitter5;
    QLabel *textLabel3;
    QComboBox *revBox;
    QSpacerItem *spacer4;
    QHBoxLayout *hboxLayout;
    QPushButton *pushButton1;
    QSpacerItem *spacer5;
    QPushButton *pushButton2;

    void setupUi(QDialog *DisablePanelBase)
    {
        if (DisablePanelBase->objectName().isEmpty())
            DisablePanelBase->setObjectName(QString::fromUtf8("DisablePanelBase"));
        DisablePanelBase->resize(577, 256);
        vboxLayout = new QVBoxLayout(DisablePanelBase);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        vboxLayout1 = new QVBoxLayout();
        vboxLayout1->setSpacing(6);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        textLabel1 = new QLabel(DisablePanelBase);
        textLabel1->setObjectName(QString::fromUtf8("textLabel1"));
        textLabel1->setWordWrap(false);

        vboxLayout1->addWidget(textLabel1);

        spacer1 = new QSpacerItem(21, 31, QSizePolicy::Minimum, QSizePolicy::Fixed);

        vboxLayout1->addItem(spacer1);

        vboxLayout2 = new QVBoxLayout();
        vboxLayout2->setSpacing(6);
        vboxLayout2->setObjectName(QString::fromUtf8("vboxLayout2"));
        splitter4 = new QSplitter(DisablePanelBase);
        splitter4->setObjectName(QString::fromUtf8("splitter4"));
        splitter4->setOrientation(Qt::Horizontal);
        textLabel2 = new QLabel(splitter4);
        textLabel2->setObjectName(QString::fromUtf8("textLabel2"));
        textLabel2->setWordWrap(false);
        splitter4->addWidget(textLabel2);
        nameBox = new QComboBox(splitter4);
        nameBox->setObjectName(QString::fromUtf8("nameBox"));
        splitter4->addWidget(nameBox);

        vboxLayout2->addWidget(splitter4);

        splitter5 = new QSplitter(DisablePanelBase);
        splitter5->setObjectName(QString::fromUtf8("splitter5"));
        splitter5->setOrientation(Qt::Horizontal);
        textLabel3 = new QLabel(splitter5);
        textLabel3->setObjectName(QString::fromUtf8("textLabel3"));
        textLabel3->setWordWrap(false);
        splitter5->addWidget(textLabel3);
        revBox = new QComboBox(splitter5);
        revBox->setObjectName(QString::fromUtf8("revBox"));
        splitter5->addWidget(revBox);

        vboxLayout2->addWidget(splitter5);


        vboxLayout1->addLayout(vboxLayout2);

        spacer4 = new QSpacerItem(20, 65, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout1->addItem(spacer4);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        pushButton1 = new QPushButton(DisablePanelBase);
        pushButton1->setObjectName(QString::fromUtf8("pushButton1"));

        hboxLayout->addWidget(pushButton1);

        spacer5 = new QSpacerItem(351, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacer5);

        pushButton2 = new QPushButton(DisablePanelBase);
        pushButton2->setObjectName(QString::fromUtf8("pushButton2"));

        hboxLayout->addWidget(pushButton2);


        vboxLayout1->addLayout(hboxLayout);


        vboxLayout->addLayout(vboxLayout1);


        retranslateUi(DisablePanelBase);
        QObject::connect(pushButton1, SIGNAL(clicked()), DisablePanelBase, SLOT(accept()));
        QObject::connect(pushButton2, SIGNAL(clicked()), DisablePanelBase, SLOT(reject()));
        QObject::connect(nameBox, SIGNAL(activated(int)), DisablePanelBase, SLOT(nameSelected(int)));
        QObject::connect(revBox, SIGNAL(activated(int)), DisablePanelBase, SLOT(revSelected(int)));

        QMetaObject::connectSlotsByName(DisablePanelBase);
    } // setupUi

    void retranslateUi(QDialog *DisablePanelBase)
    {
        DisablePanelBase->setWindowTitle(QApplication::translate("DisablePanelBase", "PixDisable", 0));
        textLabel1->setText(QApplication::translate("DisablePanelBase", "Please select the PixDisable object you want to load:", 0));
        textLabel2->setText(QApplication::translate("DisablePanelBase", "Name", 0));
        textLabel3->setText(QApplication::translate("DisablePanelBase", "Revision", 0));
        pushButton1->setText(QApplication::translate("DisablePanelBase", "OK", 0));
        pushButton2->setText(QApplication::translate("DisablePanelBase", "Cancel", 0));
    } // retranslateUi

};

namespace Ui {
    class DisablePanelBase: public Ui_DisablePanelBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DISABLEPANELBASE_H
