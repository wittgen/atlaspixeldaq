#include "IsMonitorBase.h"
#include "IsReceptor.h"
#include "IsMonitorManager.h"

#include <is/inforeceiver.h>

#ifdef DEBUG
#  ifdef DEBUG_TRACE
#    undef DEBUG_TRACE
#  endif
#  define  DEBUG_TRACE(a) { a; }
#  include <iostream>
#else
#  define  DEBUG_TRACE(a) { }
#endif

namespace PixCon {

  IsMonitorBase::IsMonitorBase(std::shared_ptr<IsReceptor> receptor,
			       EValueConnType conn_type,
			       const ISCriteria &criteria,
			       unsigned int max_time_between_updates)
    : m_receptor(receptor),
      m_connType(conn_type),
      m_criteria(criteria),
      m_lastId(0),
      m_currentId(0),
      m_maxUpdateTime( (max_time_between_updates > INT_MAX ? INT_MAX : max_time_between_updates )),
      m_lastUpdate(0),
      m_needUpdate(false),
      m_subscribed(false),
      m_concurrencyCounter(0) // @todo move before m_needUpdate
  { 
    if (m_maxUpdateTime==0) m_maxUpdateTime=INT_MAX;
    m_monitorManager=NULL;
  }


  void IsMonitorBase::overflowWarning( unsigned int overflow) {
    setNeedUpdate(true);
    std::cout << "ERROR [IsMonitorBase::overflowWarning] Buffers are too small, Missed some values (overflow counter = " << overflow << "." << std::endl;
  }

  void IsMonitorBase::errorMissedValue( const std::string &name) {
    setNeedUpdate(true);
    std::cout << "ERROR [IsMonitorBase::errorMissedValue] Missed some values (last id = " << m_lastId << ", current id = "
	      << m_currentId << " [" << name << "] )." << std::endl;
  }

  void IsMonitorBase::reconnect(IsMonitorManager *manager) {
    try {
      if (m_monitorManager) {
	unsubscribe();
      }
      m_monitorManager=manager;
      if (m_monitorManager) {
	m_subscribed=true;
	init();
      }
    }
    catch(std::exception &err) {
      setNeedUpdate(true);
       if (IsMonitorBase::receptor() && !IsMonitorBase::receptor()->isServerName().empty()) {
	 std::cerr << "ERROR [IsMonitorBase::reconnect] Failed to reconnect monitor for criteria " <<  criteria()
		   << " on " << IsMonitorBase::receptor()->isServerName().c_str() << " : " << err.what()
		   << std::endl;
       }
       else {
 	std::cerr << "ERROR [IsMonitorBase::reconnect] Failed to reconnect monitor for criteria " <<  criteria()
 		  << " : " << err.what()
 		  << std::endl;
       }
      throw err;
    }
    catch(...) {
      std::cerr << "ERROR [IsMonitorBase::reconnect] Caught unknwon exception in monitor with criteria " <<  criteria()
		<< std::endl;
      setNeedUpdate(true);
      throw;
    }
    if (!m_monitorManager || !receptor()->partition().isValid()) {
      std::cerr << "ERROR [IsMonitorBase::reconnect] Not connect : monitor with criteria " <<  criteria()
		<< std::endl;
      setNeedUpdate(true);
      throw std::runtime_error("Not connected.");
    }
//     }
//     catch(std::exception &err) {
//       if (IsMonitorBase::receptor() && !IsMonitorBase::receptor()->isServerName().empty()) {
// 	std::cout << "ERROR [IsMonitorBase::reconnect] Failed to reconnect monitor for criteria " <<  criteria()
// 		  << " on " << IsMonitorBase::receptor()->isServerName().c_str() << " : " << err.what()
// 		  << std::endl;
//       }
//       else {
// 	std::cout << "ERROR [IsMonitorBase::reconnect] Failed to reconnect monitor for criteria " <<  criteria()
// 		  << " : " << err.what()
// 		  << std::endl;
//       }
//     }
//     catch(...) {
//       if (IsMonitorBase::receptor() && !IsMonitorBase::receptor()->isServerName().empty()) {
// 	std::cout << "ERROR [IsMonitorBase::reconnect] Failed to reconnect monitor for criteria " <<  criteria()
// 		  << " on " << IsMonitorBase::receptor()->isServerName().c_str()
// 		  << std::endl;
//       }
//       else {
// 	std::cout << "ERROR [IsMonitorBase::reconnect] Failed to reconnect monitor for criteria " <<  criteria()
// 		  << std::endl;
//       }
//     }
  }


  IsMonitorBase::~IsMonitorBase() {
    unsubscribe();
  }

  void IsMonitorBase::unsubscribe() {
    if (m_monitorManager && IsMonitorBase::receptor()->partition().isValid()) {
      _unsubscribe();
      m_monitorManager=NULL;
    }
  }

  void IsMonitorBase::resubscribe() {
    reconnect(m_monitorManager);
    std::cout << "WARNING [IsMonitorBase::resubscribe] Resubscription was successful " << criteria() << "."  << std::endl;
//     if (m_monitorManager && IsMonitorBase::receptor()->partition().isValid()) {
//       try {
// 	_unsubscribe();
//       }
//       catch(...) {
//       }
//       init();
//     }
  }

  void IsMonitorBase::_unsubscribe() {
    if (m_subscribed && m_monitorManager && IsMonitorBase::receptor()->partition().isValid()) {
      DEBUG_TRACE( std::cout << "INFO [IsMonitorBase::unsubscribe] unsubscribe for criteria " << criteria() << "."  << std::endl );
      try {
	m_subscribed=false;
	IsMonitorBase::receptor()->receiver().unsubscribe(IsMonitorBase::receptor()->isServerName().c_str(),m_criteria);
      }
      catch(...) {
	std::cout << "WARNING [IsMonitorBase::dtor] failed to unsubscribe from IS for variable " << criteria() << "."  << std::endl;
	// if the unsubscription fails what can be done ?
      }
    }
  }

  void IsMonitorBase::changePartition(IPCPartition &partition)
  {
    if (m_monitorManager) {
      unsubscribe();
    }
    m_receptor->reconnect(partition);
    init();
  }

  void IsMonitorBase::requestUpdate() {
    if (m_subscribed) {
      assert (m_monitorManager);
      m_monitorManager->updateRequest();
    }
  }

  void IsMonitorBase::cleanup(const std::map<std::string, std::set<std::string> > &clean_up_var_list) {
    receptor()->cleanup(clean_up_var_list);
  }
}
