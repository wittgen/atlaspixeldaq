#ifndef _PixCon_IEvaluator_h_
#define _PixCon_IEvaluator_h_

#include "EvalResult_t.h"

namespace PixCon {

  class IEvaluator
  {
  public:

    virtual ~IEvaluator() {}
    virtual EvalResult_t eval() const = 0 ;

  };

}
#endif
