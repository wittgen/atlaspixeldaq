#ifndef _Highlighter_h_
#define _Highlighter_h_

#include <qtimer.h>
#include <qlineedit.h>
#include <qcolor.h>

class Highlighter : public QTimer
{
  Q_OBJECT

public:
  Highlighter(QWidget &line,
		   const QColor &color_new,
		   const QColor &color_old,
		   int timeout=5*1000) 
    : QTimer(&line), m_line(&line),m_colorOld(color_old) 
  {
    connect(this, SIGNAL(timeout()), this, SLOT(unHighlight()));
    restart(color_new, timeout);
  }

  ~Highlighter() {
    unHighlight();
    stop();
  }

  void restart(const QColor &color_new, int timeout=5*1000) {
    //m_line->setPaletteBackgroundColor(color_new);
    m_line->setStyleSheet("background: "+color_new.name());
    start((timeout > 0) ? timeout : 0);
  }

public slots:

  void stop() {
    QTimer::stop();
    unHighlight();
  }

 void unHighlight() {
   //m_line->setPaletteBackgroundColor(m_colorOld);
   m_line->setStyleSheet("background: "+m_colorOld.name());
 }

private:
  QWidget *m_line;
  QColor     m_colorOld;
};

#endif
