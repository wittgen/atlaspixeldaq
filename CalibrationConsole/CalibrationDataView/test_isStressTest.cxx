
#include <ipc/partition.h>
#include <ipc/core.h>

#include <is/infoT.h>
#include <is/inforeceiver.h>
#include <is/infoiterator.h>
#include <is/infodictionary.h>

#include "test_isUtil.h"

#include <qthread.h>
#include <qapplication.h>

#include <map>
#include <set>
#include <stdexcept>

template <class T>
class IsListener : public QThread
{
public:
  IsListener(const std::string &partition_name, const std::string &is_server_name, const std::string &var_name, bool verbose=false);

  void run() {m_isReceiver.run();}
  void initRod(const std::string &rod_name, T value) { m_rodValue[rod_name]=value;}

  bool compare(const std::string &rod_name, T value) const
  {
    typename std::map<std::string, T>::const_iterator iter = m_rodValue.find(rod_name);
    if (iter == m_rodValue.end()) return false;
    return iter->second == value;
  }

  T result(const std::string &rod_name) const
  {
    typename std::map<std::string, T>::const_iterator iter = m_rodValue.find(rod_name);
    if (iter == m_rodValue.end()) {
      throw std::runtime_error("ROD does not exist.");
    }
    return iter->second;
  }

  unsigned int counter() const { return m_counter; }

  void shutdown() { m_isReceiver.stop(); }
  void setVerbose(bool verbose) { m_verbose=true; }
protected:
  void valueChanged(ISCallbackInfo *info);

  void extract(const std::string value_name, T value);

private:
  IPCPartition   m_isPartition;
  ISInfoReceiver m_isReceiver;

  unsigned int             m_counter;
  std::map<std::string, T> m_rodValue;
  bool                     m_verbose;
};


// @todo finish:
// class Noise : public QThread
// {
// public:
//   void initRod(const std::string &rod_name) { m_rodList.insert(rod_name);}

//   void run() {
//     while (!m_rodList.empty()) {
//       for (std::set<std::string>::const_iterator rod_iter = m_rodList.begin();
// 	   rod_iter != m_rodList.end();
// 	   rod_iter++) {
// 	rand()
//       }
//       wait.tv_sec=0;
//       wait.tv_nsec=rand();
//       nanosleep( wait );
//     }
//   }
  
// private:
//   std::set<std::string> m_rodList;
// };


template <class T>
IsListener<T>::IsListener(const std::string &partition_name, const std::string &is_server_name, const std::string &var_name, bool verbose)
  : m_isPartition(partition_name),
    m_isReceiver(m_isPartition),
    m_counter(0),
    m_verbose(verbose)
{ 
  // get initial config values from IS server
  std::string name(".*");
  name+=var_name;


  ISInfoT<T> is_value;
  ISCriteria criteria = is_value.type() && name;

  ISInfoIterator ii( m_isPartition, is_server_name,  criteria);
  while( ii() ) {

    ii.value(is_value);
    extract(ii.name(), is_value.getValue());
    //    std::cout << "INFO [IsListener::ctor] " << ii.name()  << " : " << is_value.getValue() << std::endl;
  }

  m_isReceiver.subscribe(is_server_name.c_str(),criteria,&IsListener<T>::valueChanged, this);
}

template <class T>
void IsListener<T>::extract(const std::string value_name, T value) 
{
  std::string::size_type pos  = value_name.find(".");
  if (pos != std::string::npos && pos+1<value_name.size()) {
    std::string::size_type end_pos  = value_name.find("/",pos+1);
    if (end_pos != std::string::npos) {
      m_rodValue[value_name.substr(pos+1,end_pos-pos-1)] += value;
      if (m_verbose) {
	std::cout << "INFO [IsListener::extract] " <<  value_name.substr(pos+1,end_pos-pos-1) << " : " << m_rodValue[value_name.substr(pos+1,end_pos-pos-1)] << std::endl;
      }
      m_counter++;
    }
  }
}


template <class T>
void IsListener<T>::valueChanged(ISCallbackInfo *info)
{
  ISInfoT<T> is_value;
  if (info->type() == is_value.type()) {
    info->value(is_value);
    extract(info->name(), is_value.getValue());

  }
}


template <class T>
class templ_Data_t {
public:
  templ_Data_t() { m_value[0]=0; m_value[1]=0; m_info.setValue(m_value[0]);}
  templ_Data_t(const templ_Data_t &a) { m_value[0]=a.m_value[0]; m_value[1]=a.m_value[1]; m_info.setValue(m_value[0]);}

  templ_Data_t(T v1 , T v2 ) { m_value[0]=v1; m_value[1]=v2; m_info.setValue(m_value[0]);}

  T value() const { return m_value[0]; }
  T result() const { return m_value[1]; }

  void nextValue() { T temp=m_value[0]+m_value[1]; m_value[0]=m_value[1]; m_value[1]=temp; m_info.setValue(value());}
  
  ISInfoT<T> &info() { return m_info; }
private:
  ISInfoT<T> m_info;
  T m_value[2];
};

typedef templ_Data_t<int> Data_t;

int main(int argc, char **argv)
{
  std::string partition_name;
  std::string var_name = "data";
  std::string is_name = "PixFakeData";
  timespec change_wait;
  change_wait.tv_nsec = 100*1000*1000;
  change_wait.tv_sec = 0;
  unsigned int n_loops = 2;
  unsigned int n_crates=1;
  unsigned int n_slots=1;
  bool error=false;
  bool verbose=true;

  for (int arg_i=1; arg_i<argc; arg_i++) {
    if (strcmp(argv[arg_i],"-p")==0 && arg_i+1<argc) {
      partition_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-q")==0 ) {
      verbose=false;
    }
    else if (strcmp(argv[arg_i],"-v")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      var_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-n")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      is_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-s")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      n_slots = atoi(argv[++arg_i]);
    }
    else if (strcmp(argv[arg_i],"-c")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      n_crates = atoi(argv[++arg_i]);
    }
    else if (strcmp(argv[arg_i],"-l")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      n_loops = atoi(argv[++arg_i]);
    }
    else if (strcmp(argv[arg_i],"-w")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      change_wait.tv_nsec = atoi(argv[++arg_i])*1000;
    }
    else {
      std::cout << "ERROR ["<< argv[0] << ":main] unhandled argument " << arg_i << " : " << argv[arg_i]<< "." << std::endl;
      error=true;
    }
  }

  if (var_name.empty() || partition_name.empty()  || error ) {
      std::cout << "usage: " << argv[0] << "-p partition -v var-name [-n \"IS server name\"]" << std::endl;
      return -1;
  }
  // START IPC

  // Start IPCCore
  IPCCore::init(argc, argv);

  // Create IPCPartition
  std::cout << "INFO [" << argv[0] << ":main] listen for changes of " << var_name << " on " << partition_name  << " in IS server "  << is_name << std::endl;

  QApplication app( argc, argv );

  IPCPartition partition(partition_name);
  ISInfoDictionary is_dictionary(partition);

  {
  ISInfoT<int> is_value;
  ISCriteria criteria = is_value.type() && std::string(".*/")+var_name;
  ISInfoIterator ii( partition, is_name,  criteria);
  while( ii() ) {
    is_dictionary.remove(ii.name());
    //    std::cout << "INFO [IsListener::ctor] " << ii.name()  << " : " << is_value.getValue() << std::endl;
  }
  }


  IsListener<int> listener(partition_name,is_name,std::string(".*/")+var_name, verbose);
  

  std::map<std::string, Data_t > rod_values;

  for(unsigned int crate_i=0; crate_i<0+n_crates; crate_i++) {
    for (unsigned int slot_i=5; slot_i<5+n_slots; slot_i++) {
      std::stringstream rod_name;
      rod_name << "ROD_C" << crate_i << "_S" << slot_i;
      rod_values[rod_name.str()]=Data_t(1,1);
    }
  }



  for(std::map<std::string, Data_t>::const_iterator rod_iter = rod_values.begin();
      rod_iter != rod_values.end();
      rod_iter++) {
    listener.initRod(rod_iter->first, rod_iter->second.value());
  }
  listener.start();

  unsigned int counter=0;
  for(std::map<std::string, Data_t>::iterator rod_iter = rod_values.begin();
      rod_iter != rod_values.end();
      rod_iter++) {
    ISType type(rod_iter->second.info().type());
    setIsValue(is_dictionary, type, std::string(is_name)+"."+rod_iter->first+std::string("/")+var_name, rod_iter->second.info());
    rod_iter->second.nextValue();
    int value = rod_iter->second.value();
    int result = rod_iter->second.result();
    counter++;
  }

  for (unsigned int loop_i=0; loop_i<n_loops; loop_i++) {
    for(std::map<std::string, Data_t >::iterator rod_iter = rod_values.begin();
	rod_iter != rod_values.end();
	rod_iter++) {
      updateIsValue(is_dictionary, std::string(is_name)+"."+rod_iter->first+std::string("/")+var_name, rod_iter->second.info());
      rod_iter->second.nextValue();
      int value = rod_iter->second.value();
      int result = rod_iter->second.result();
      counter++;
    }
    nanosleep(&change_wait,NULL);
  }

  std::cout << "INFO [main] Wait until all values arrived at listener : " << std::endl;
  while (listener.counter() < counter ) {
    std::cout << "INFO [main] Wait counters : " << listener.counter() << " != " << counter <<std::endl;
    sleep(1);
  }
  listener.shutdown();

  std::cout << "INFO [main] counters : " << listener.counter() << " =?= " << counter <<std::endl;
  unsigned int errors = 0;
  for(std::map<std::string, Data_t>::const_iterator rod_iter = rod_values.begin();
      rod_iter != rod_values.end();
      rod_iter++) {
    try {
      if (!listener.compare(rod_iter->first, rod_iter->second.result())) {
	std::cout << "ERROR [main] " << rod_iter->first << " results differ : " << rod_iter->second.result()
		  << " != " << listener.result(rod_iter->first) 
		  << std::endl;
	errors++;
      }
    }
    catch(...) {
      std::cout << "ERROR [main] " << rod_iter->first << " does not exist for listener." 
		<< std::endl;
      errors++;
    }
  }
  std::cout << "INFO [main] errors = " << errors << std::endl;

  return errors==0;
}
