#include <EvaluatorFactory.h>
namespace PixCon {

  CalibrationData::ConnObjDataList_t EvaluationParameter::s_dummyData;

  ConnNameComparator::ConnNameComparator(EvaluationParameter &parameter, const std::string &pattern)
    : EvaluatorWithParameter(parameter), m_fromBeginning(true)
  {
    if (!pattern.empty()) {
      std::string::size_type pos=0;
      if (pattern[pos]=='*') {
	m_fromBeginning=false;
	pos++;
      }
      while (pos<pattern.size()) {
	std::string::size_type start_pos = pos;
	while (pos<pattern.size() && pattern[pos]!='*') pos++;
	if (pos != start_pos) {
	  m_patterns.push_back(std::string(pattern,start_pos, pos - start_pos));
	  pos++;
	}
      }
    }

  }
  EvalResult_t ConnNameComparator::eval() const {
    if (m_patterns.empty()) return EvalResult_t::trueResult();
    EvalResult_t result = eval(parameter().connectivityName());

    if (!static_cast<bool>(result) ) {
      for (std::vector<std::string>::const_iterator tag_iter = parameter().dataList().beginTags();
	   tag_iter != parameter().dataList().endTags();
	   ++tag_iter) {
	result = eval(*tag_iter);
	if (result) {
	  return result;
	}
      }
    }
    return result;
  }

  EvalResult_t ConnNameComparator::eval(const std::string &match_name) const {
    if (m_patterns.empty()) return EvalResult_t::trueResult();
    std::vector<std::string>::const_iterator pattern_iter = m_patterns.begin();
    std::string::size_type pos=0;

    if (m_fromBeginning) {
      if (match_name.compare(0,pattern_iter->size(), *pattern_iter) != 0) return EvalResult_t::unknownResult();
      pos += pattern_iter->size();
      pattern_iter++;
    }

    while ( pattern_iter != m_patterns.end()) {
      std::string::size_type pattern_pos = match_name.find(*pattern_iter, pos);
      if (pattern_pos == std::string::npos) return EvalResult_t::unknownResult();
      pos = pattern_pos + pattern_iter->size();
      pattern_iter++;
    }

    return EvalResult_t::trueResult();
  }

}

