#include <is/inforeceiver.h>
#include "IsReceptor.h"

#include <qapplication.h>
#include "QIsInfoEvent.h"
//Added by qt3to4:

#include <CalibrationDataManager.h>
#include <CalibrationDataFunctorBase.h>
#include <guessConnItemType.h>

#include <ConfigWrapper/PixDisableUtil.h>

#ifdef DEBUG_TRACE
#  undef DEBUG_TRACE
#endif
#ifdef DEBUG
#  define DEBUG_TRACE(a) { a; }
#  include <iostream>
#else
#  define DEBUG_TRACE(a) {  }
#endif

namespace PixCon {

  unsigned int s_setValueWait=0;

  IsReceptor::IsReceptor(QApplication *app,
			 QObject *receiver_object,
			 const std::string &is_server_name,
			 IPCPartition &partition,
			 const std::shared_ptr<CalibrationDataManager> &calibration_data)
    : m_isServerName(is_server_name),
      m_partition(partition),
      m_app(app),
      m_receiverObject(receiver_object),
      m_calibrationData(calibration_data),
      m_shutdown(false)
  {
    m_running=true;
    start_undetached();
  }


  void *IsReceptor::run_undetached(void *arg) {
    assert(arg==NULL);
    run(nullptr);
    return NULL;
  }

  void IsReceptor::run(void*)
  {
    DEBUG_TRACE( std::cout << "INFO [IsReceptor::run] will start receptor for " << m_partition.name() << " ." \
		 << " partition is valid ? " <<  ( m_partition.isValid() ? "yes" : "no") << std::endl );

    for(;!m_shutdown;) {

      if (m_partition.isValid()) {
	DEBUG_TRACE( std::cout << "INFO [IsReceptor::run] Restart receiver." << std::endl );
	{
	  {
	    Lock lock(m_receiverLock);
	    if (m_isReceiver.get()) {
	      //m_isReceiver->stop();
	      m_semaphore.post();
	    }

	    m_hasPartition.clearFlag();
	    m_isReceiver = std::make_unique<ISInfoReceiver>(m_partition);
	  }
	  if (m_cleanupVariables.empty()) {
	    swap(m_cleanupVariables, m_receivedVariables);
	  }
	  else {
	    // in principle this means that something went wrong
	    for (std::map<std::string, std::set<std::string> >::const_iterator var_iter = m_receivedVariables.begin();
		 var_iter != m_receivedVariables.end();
		 ++var_iter) {
	      m_cleanupVariables.insert( *var_iter );
	    }
	    m_receivedVariables.clear();
	  }

	  DEBUG_TRACE( std::cout << "INFO [IsReceptor::run] variables registered for cleanup :"; \
	  for (std::map<std::string, std::set<std::string> >::const_iterator cleanup_iter = m_cleanupVariables.begin(); \
	       cleanup_iter != m_cleanupVariables.end(); \
	       ++cleanup_iter) { \
	    std::cout << cleanup_iter->first; \
	  } \
		       std::cout << std::endl; );

	  m_connected.broadcast();
	  DEBUG_TRACE( std::cout << "INFO [IsReceptor::run] Receiver ready for reconnection. Will start receiver" << std::endl );
	  //	  m_isReceiver->run();
	  m_semaphore.wait();
	}

	std::cout << "INFO [IsReceptor::run] IS info receiver for " << m_partition.name() << " should be up and running." << std::endl;
      }
      else {
	m_hasPartition.wait();
      }
      DEBUG_TRACE( std::cout << "INFO [IsReceptor::run] woke up." << std::endl );
    }
    DEBUG_TRACE( std::cout << "INFO [IsReceptor::run] Finished loop." << std::endl );

    if (m_isReceiver.get()) {
      Lock lock(m_receiverLock);
      //      m_isReceiver->stop();
      m_semaphore.post();
    }
    m_running=false;
    DEBUG_TRACE( std::cout << "INFO [IsReceptor::run] All done." << std::endl );
    m_exit.setFlag();
    DEBUG_TRACE( std::cout << "INFO [IsReceptor::run] exit." << std::endl );
  }

  IsReceptor::~IsReceptor() {
    shutdown();
  }

  void IsReceptor::shutdown() {
    m_shutdown=true;
    if (m_running) {
      {
	Lock lock(m_receiverLock);
	if (m_isReceiver.get()) {
	  //	  m_isReceiver->stop();
	  m_semaphore.post();
	}
      }
      m_running=false;
      std::cout << "INFO [IsReceptor::shutdown] request shutdown." << std::endl;
      m_hasPartition.setFlag();
      m_exit.wait();
      std::cout << "INFO [IsReceptor::shutdown] done." << std::endl;
    }
  }

  void IsReceptor::reconnect() {
    if (m_running) {
      if (m_isReceiver.get()) {
	Lock lock(m_receiverLock);
	//	m_isReceiver->stop();
	m_semaphore.post();
	m_connected.clearFlag();
      }
    }
    m_hasPartition.setFlag();
  }


  void IsReceptor::reconnect(IPCPartition &partition) {
    if (m_running) {
      DEBUG_TRACE( std::cout << "INFO [IsReceptor::reconnect] stop is receiver " << std::endl )
      if (m_isReceiver.get()) {
	Lock lock(m_receiverLock);
	//	m_isReceiver->stop();
	m_semaphore.post();
      }	  
      m_partition = partition;
      m_hasPartition.setFlag();
    }
  }

  void IsReceptor::setEnabled(EValueConnType conn_type,
			      EMeasurementType measurement,
			      SerialNumber_t serial_number,
			      const std::string &is_name,
			      const IVarNameExtractor &extractor,
			      bool enabled) {
    std::unique_ptr<QIsInfoEvent> event( new QIsInfoEvent(is_name, extractor, conn_type, measurement, serial_number));
    if (event->valid()) {
      {
	try {
	  Lock lock(m_calibrationData->calibrationDataMutex());
	  CalibrationData::ConnObjDataList conn_obj_data( m_calibrationData->calibrationData().getConnObjectData(event->connName()) );
	  CalibrationData::EnableState_t &conn_obj_enable_state(conn_obj_data.enableState(measurement, serial_number));
	  bool old_enabled = conn_obj_enable_state.isTemporaryEnabled();
	  //	  bool old_all_enabled = conn_obj_enable_state.enabled();

	  CalibrationData::EnableState_t enable_test ( conn_obj_enable_state  );
	  enable_test.setEnabled(true);

	  // only allow to toggle the enable bit if the connectivity object can be enabled
	  //	  if ( !enable_test.enabled() ) {
	  if (    !conn_obj_enable_state.isInConnectivity() 
	       || !conn_obj_enable_state.isAllocated() 
	       || !conn_obj_enable_state.isParentAllocated() ) {
	    DEBUG_TRACE( std::cout << "INFO [IsReceptor::setEnabled] ignore " << event->connName() << std::endl );
	    return;
	  }

	  bool update_active_value=false;
	  try {
	    VarDef_t var_def = VarDefList::instance()->getVarDef("Active");
	    State_t old_state = conn_obj_data.value<State_t>(measurement, serial_number, var_def.id() );
	    if (old_state != static_cast<State_t>(enabled)) update_active_value=true;
	  }
	  catch(CalibrationData::MissingValue &err) {
	    update_active_value=true;
	  }
	  if (update_active_value) {

	    DEBUG_TRACE(std::cout << "INFO [IsReceptor::setEnabled] update active of  " << event->connName() << " value = " << enabled \
			<< " in connectivity ? " << (conn_obj_enable_state.isInConnectivity()) \
			<< " in configuration ? " << (conn_obj_enable_state.isInConfiguration()) \
			<< " allocated ? " << (conn_obj_enable_state.isAllocated()) \
			<< " parent enabled ? " << (conn_obj_enable_state.isParentEnabled()) \
			<< std::endl );

	    m_calibrationData->calibrationData().addValue(event->connType(),
							  event->connName(),
							  measurement,
							  serial_number,
							  "Active",
							  static_cast<State_t>(enabled));
	  }

	  if ( old_enabled != enabled) {
	    try {
	      DEBUG_TRACE( std::cout << "INFO [IsReceptor::setEnabled] name = " << event->connName() << " value = " << enabled << std::endl );
	      EConnItemType conn_item_type = guessConnItemType(event->connName());
	      switch (conn_item_type) {
	      case kRodItem: {
		m_calibrationData->setRodEnable(event->connName(),measurement,serial_number, enabled);
		event->setPropagateToChilds();
		break;
	      }
	      case kCrateItem: {
		m_calibrationData->setCrateEnable(event->connName(),measurement,serial_number, enabled);
		event->setPropagateToChilds();
		break;
	      }
	      case kPp0Item: {
		m_calibrationData->setPp0Enable(event->connName(),measurement,serial_number, enabled);
		break;
	      }
	      case kModuleItem:
	      default: {
		conn_obj_enable_state.setEnabled(enabled);
		//		m_calibrationData->setCrateEnable(event->connName(),measurement,serial_number, enabled);
		//		event->setPropagateToChilds();
		break;
	      }
	      }

	      update_active_value=true;
	    }
	    catch (MissingConnectivity &err) {
	    }
	    catch (PixA::ConfigException &err) {
	    }
	  }
	  if (update_active_value) {
	    notify(event.release());
	  }
 	  else DEBUG_TRACE( std::cout << "INFO [IsReceptor::setEnabled] nothing updated for  " << event->connName() << std::endl );

	}
	catch (CalibrationData::MissingConnObject &) {
	}
	//	  m_calibrationData->addValue(conn_type,event->connName(),measurement, serial_number, event->varName(),active_state);
      }
    }
  }

  template <class T>
  void IsReceptor::setValue(EValueConnType conn_type,
			    EMeasurementType measurement,
			    SerialNumber_t serial_number,
			    const std::string &is_name,
			    const IVarNameExtractor &extractor,
			    ::time_t time_stamp,
			    T value) {

    //    DEBUG_TRACE( std::cout << "INFO [IsReceptor::setValue] " << makeSerialNumberString(measurement, serial_number)
    //		 <<  " name="  << is_name << " value=" << value  << " time=" << time_stamp << std::endl );
    std::unique_ptr<QIsInfoEvent> event(new QIsInfoEvent(is_name, extractor, conn_type, measurement, serial_number));
    if (event->valid()) {
      //      DEBUG_TRACE( std::cout << "INFO [IsReceptor::setValue] Valid event var=" << event->varName() << " conn=" << event->connName() << std::endl );
      try {
	VarDef_t var_def = VarDefList::instance()->getVarDef(event->varName());
	Lock lock(m_calibrationData->calibrationDataMutex());
	if (var_def.hasHistory()) {
	  //	  DEBUG_TRACE( std::cout << "INFO [IsReceptor::setValue] has history "  << makeSerialNumberString(measurement, serial_number)
	  //                       << " set " << event->varName() << " to " << value << " "  << time_stamp << std::endl );
	  m_calibrationData->calibrationData().addValue(event->connName(),measurement, serial_number, var_def, time_stamp,value);
	}
	else {
	  m_calibrationData->calibrationData().addValue(event->connName(),measurement, serial_number, var_def ,value);
	}
      }
      catch (std::runtime_error &err) {
	DEBUG_TRACE( std::cout << "WARNING [IsReceptor::setValue] Caught exception :" << err.what()  << std::endl );
      }
      catch (...) {
	  DEBUG_TRACE( std::cout << "WARNING [IsReceptor::setValue] Caught exception"  << std::endl );
      }
      m_receivedVariables[event->varName()];
      m_app->postEvent(m_receiverObject, event.release());
    }
  }


  void IsReceptor::notify(QEvent *event) {
    m_app->postEvent(m_receiverObject, event);
  }

  void IsReceptor::registerForCleanup(EMeasurementType measurement,
					  SerialNumber_t serial_number,
					  const std::string &conn_name,
					  const std::string &var_name,
					  std::map<std::string, std::set<std::string> > &clean_up_var_list) {
    if (measurement==kCurrent && serial_number==0) {
      //      DEBUG_TRACE( std::cout << "INFO [IsReceptor::registerForCleanup] var_name=" << var_name << " veto=" << conn_name  << std::endl );
      clean_up_var_list[var_name].insert(conn_name);
    }
  }


  class CleanupVariableListBase : public CalibrationDataFunctorBase
  {
  public:
    CleanupVariableListBase(CalibrationData &calibration_data,
			    const VarDef_t &var_def,
			    const std::set<std::string> &veto_list, 
			    IsReceptor &receptor)
      : CalibrationDataFunctorBase( ( (var_def.connType() == kRodValue || var_def.connType() == kAllConnTypes) ? (1<<kRodItem) : 0)
				     |( (var_def.connType() == kModuleValue || var_def.connType() == kAllConnTypes) ? (1<<kModuleItem) : 0 )),
	m_calibrationData(&calibration_data),
	m_varDef(var_def),
	m_vetoList(&veto_list),
	m_receptor(&receptor)
    { assert(m_calibrationData); 
    }

  protected:
    bool veto(const std::string &conn_name) const {  return m_vetoList->find(conn_name) != m_vetoList->end(); };

    CalibrationData       &calibrationData() { return *m_calibrationData; }
    const VarDef_t       &varDef() const     { return m_varDef; }

    /*Do nothing */
    void operator()( EConnItemType /*item_type*/,
		     const std::string &/*conn_name*/,
		     const std::pair<EMeasurementType,SerialNumber_t> &/*measurement*/,
		     CalibrationData::EnableState_t /*new_enable_state*/) { }

    void postEvent(const std::string &conn_name, const std::string &var_name, EValueConnType conn_type, EMeasurementType measurement, SerialNumber_t serial_number)
    {
      std::unique_ptr<QIsInfoEvent> event(new QIsInfoEvent(conn_name, var_name, conn_type, measurement, serial_number));
      if (event->valid()) {
	m_receptor->notify(event.release());
      }
    }

  private:
    CalibrationData             *m_calibrationData;
    VarDef_t                     m_varDef;
    const std::set<std::string> *m_vetoList;
    IsReceptor                  *m_receptor;
  };

  template <class T>
  class CleanupUnTimedVariableList : public CleanupVariableListBase
  {
  public:
    CleanupUnTimedVariableList(CalibrationData &calibration_data,
			       const VarDef_t &var_def,
			       const std::set<std::string> &veto_list, 
			       IsReceptor &receptor)
      : CleanupVariableListBase(calibration_data, var_def, veto_list,receptor)
    { 
      if (!varDef().hasHistory()) throw std::logic_error("FATAL [CleanupTimedVariableList::ctor] Requires a variable with history.");
    }

    void operator()( EConnItemType /*item_type*/,
		     const std::string &conn_name,
		     const std::pair<EMeasurementType,SerialNumber_t> &measurement,
		     PixCon::CalibrationData::ConnObjDataList &data_list,
		     CalibrationData::EnableState_t /*is_enable_state*/,
		     CalibrationData::EnableState_t /*new_enable_state*/) {

      if (veto(conn_name)) return;

      data_list.removeVariable<T>(measurement.first, measurement.second, varDef().id());
      postEvent(conn_name, VarDefList::instance()->varName(varDef().id()), varDef().connType(), measurement.first, measurement.second);
    }


  };

  typedef CleanupUnTimedVariableList<State_t> CleanupStateVariableList;
  typedef CleanupUnTimedVariableList<float>   CleanupValueVariableList;


  class CleanupHistoryStateVariableList : public CleanupVariableListBase
  {
  public:
    CleanupHistoryStateVariableList(CalibrationData &calibration_data,
				    const VarDef_t &var_def,
				    const std::set<std::string> &veto_list,
				    IsReceptor &receptor,
				    time_t cleanup_time)
      : CleanupVariableListBase(calibration_data, var_def, veto_list,receptor),
	m_cleanupTime(cleanup_time)
    {
      if (!varDef().isStateWithOrWithoutHistory())
	throw std::logic_error("FATAL [CleanupStateVariableList::ctor] Requires a state variable.");
      if (varDef().isExtendedType()) {
	const ExtendedStateVarDef_t &extended_state_def = varDef().extendedStateDef();
	m_missingState = extended_state_def.missingState();
      }
      else {
	const StateVarDef_t &state_def = varDef().stateDef();
	m_missingState = state_def.nStates();
      }
    }

    void operator()( EConnItemType /*conn_type*/,
		     const std::string &conn_name,
		     const std::pair<EMeasurementType,SerialNumber_t> &measurement,
		     PixCon::CalibrationData::ConnObjDataList &data_list,
		     CalibrationData::EnableState_t /*is_enable_state*/,
		     CalibrationData::EnableState_t /*new_enable_state*/) {

      DEBUG_TRACE( std::cout << "INFO [CleanupHistoryStateVariableList] cleanup  " << conn_name  << " ? " << std::endl );
      if (veto(conn_name)) return;
      DEBUG_TRACE( std::cout << "INFO [CleanupHistoryStateVariableList] cleanup  " << conn_name << " -> "<< m_missingState << std::endl );

      //      const ExtendedStateVarDef_t &extended_state_def = varDef().extendedStateDef();
      data_list.addValue(measurement.first, measurement.second, varDef(), m_cleanupTime, m_missingState);
      this->postEvent(conn_name, VarDefList::instance()->varName(varDef().id()), varDef().connType(), measurement.first, measurement.second);

    }
  private:
    State_t m_missingState;
    time_t                       m_cleanupTime;
  };

  class CleanupHistoryValueVariableList : public CleanupVariableListBase
  {
  public:
    CleanupHistoryValueVariableList(CalibrationData &calibration_data,
				    const VarDef_t &var_def,
				    const std::set<std::string> &veto_list,
				    IsReceptor &receptor,
				    time_t cleanup_time)
      : CleanupVariableListBase(calibration_data, var_def, veto_list,receptor),
	m_cleanupTime(cleanup_time)
    {
      if (!varDef().isValueWithOrWithoutHistory()) {
	throw std::logic_error("FATAL [CleanupValueVariableList::ctor] Requires a value variable.");
      }
    }

    void operator()( EConnItemType /*item_type*/,
		     const std::string &conn_name,
		     const std::pair<EMeasurementType,SerialNumber_t> &measurement,
		     PixCon::CalibrationData::ConnObjDataList &data_list,
		     CalibrationData::EnableState_t /*is_enable_state*/,
		     CalibrationData::EnableState_t /*new_enable_state*/) {

      if (veto(conn_name)) return;

      DEBUG_TRACE( std::cout << "INFO [CleanupHistoryValueVariableList] cleanup  " << conn_name  << " ? " << std::endl );
      const ValueVarDef_t &value_def = varDef().valueDef();
      DEBUG_TRACE( std::cout << "INFO [CleanupHistoryValueVariableList] cleanup  " << conn_name << " ->" << value_def.missing() << std::endl );
      data_list.addValue(measurement.first, measurement.second, varDef(), m_cleanupTime, value_def.missing());
      DEBUG_TRACE( std::cout << "INFO [CleanupHistoryValueVariableList] cleanup  " << conn_name << " ->" << value_def.missing() << "=?=" 
		   << std::endl );
      postEvent(conn_name, VarDefList::instance()->varName(varDef().id()), varDef().connType(), measurement.first, measurement.second);

    }

  private:
    //State_t m_missingState;
    time_t                       m_cleanupTime;

  };

  void IsReceptor::cleanup()
  {
    if (!m_cleanupVariables.empty()) {
      cleanup(m_cleanupVariables);
      m_cleanupVariables.clear();
    }
  }

  void IsReceptor::cleanup(const std::map<std::string, std::set<std::string> > &clean_up_var_list)
  {
//     std::map<std::string, std::set<std::string> > local_clean_up_var_list;
//     const std::map<std::string, std::set<std::string> > *p_clean_up_var_list = &clean_up_var_list;
//     if (m_reconnect) {
//       local_clean_up_var_list=clean_up_var_list;
//       for (std::set<std::string>::const_iterator var_iter = m_receivedVariables.begin();
// 	   var_iter != m_receivedVariables.end();
// 	   ++var_iter) {
// 	local_clean_up_var_list[*var_iter] ;
//       }
//       m_receivedVariables.clear();
//       m_reconnect=false;
//       p_clean_up_var_list = & local_clean_up_var_list;
//     }

    Lock lock(m_calibrationData->calibrationDataMutex());
    try {
    std::time_t cleanup_time(time(0));
    DEBUG_TRACE( std::cout << "INFO [IsReceptor::cleanup] start " << std::endl );
    for (std::map<std::string, std::set<std::string> >::const_iterator var_iter=clean_up_var_list.begin();
	 var_iter != clean_up_var_list.end();
	 ++var_iter) {

      std::map<std::string, std::set<std::string> >::iterator erase_iter = m_cleanupVariables.find(var_iter->first);
      if (erase_iter != m_cleanupVariables.end() && erase_iter != var_iter ) {	
	DEBUG_TRACE( std::cout << "INFO [IsReceptor::cleanup] cleanup and erase variable " << var_iter->first << " from cleanup list" << std::endl )
	m_cleanupVariables.erase(erase_iter);
      }

      DEBUG_TRACE(std::cout << "INFO [IsReceptor::cleanup] start " << var_iter->first << std::endl )

      PixA::DisabledListRoot empty_disabled_list_root;
      try {
	VarDef_t var_def = VarDefList::instance()->getVarDef(var_iter->first);

	std::unique_ptr<ICalibrationDataFunctor> functor;
	if (!var_def.hasHistory()) {
	  DEBUG_TRACE( std::cout << "INFO [IsReceptor::cleanup] cleanup non history variable  " << var_iter->first << std::endl );

	  if (var_def.isState()) {
	    functor=std::unique_ptr<ICalibrationDataFunctor>(new CleanupStateVariableList(m_calibrationData->calibrationData(),
											var_def,
											var_iter->second,
											*this));
	  }
	  else if (var_def.isValue()) {
	    functor=std::unique_ptr<ICalibrationDataFunctor>(new CleanupValueVariableList(m_calibrationData->calibrationData(),
											var_def,
											var_iter->second,
											*this));
	  }
	}
	else if (var_def.isStateWithHistory()) {
	  DEBUG_TRACE( std::cout << "INFO [IsReceptor::cleanup] cleanup state with history  " << var_iter->first << std::endl );

	  functor=std::unique_ptr<ICalibrationDataFunctor>(new CleanupHistoryStateVariableList(m_calibrationData->calibrationData(),
											     var_def,
											     var_iter->second,
											     *this,
											     cleanup_time));
	}
	else if (var_def.isValueWithHistory()) {
	  DEBUG_TRACE( std::cout << "INFO [IsReceptor::cleanup] cleanup value with history  " << var_iter->first << std::endl );

	  functor=std::unique_ptr<ICalibrationDataFunctor>(new CleanupHistoryValueVariableList(m_calibrationData->calibrationData(),
											     var_def,
											     var_iter->second,
											     *this,
											     cleanup_time));
	}
	else {
	  std::stringstream message;
	  message << "FATAL [IsMonitorBase::cleanup] tried to cleanup variable " << var_iter->first << " which is of unsupported type.";
	  throw std::logic_error(message.str());
	}

	DEBUG_TRACE( std::cout << "INFO [IsReceptor::cleanup] Run clean up " << var_iter->first << std::endl );
	m_calibrationData->forEach(kCurrent, 0, empty_disabled_list_root, *functor, true);
      }
      catch(std::exception &err) {
	DEBUG_TRACE( std::cout << "ERROR [IsReceptor::cleanup] Caught exception when cleaning up " << var_iter->first  << " : " << err.what() << std::endl );

      }
      catch(...) {
	DEBUG_TRACE( std::cout << "ERROR [IsReceptor::cleanup] Caught exception when cleaning up " << var_iter->first << std::endl );

      }
    }
    }
    catch (std::exception &err) {
      std::cout << "ERROR [IsReceptor::cleanup] Caught exception : " << err.what() << std::endl;
    }
    catch (...) {
      std::cout << "ERROR [IsReceptor::cleanup] Caught unknown exception." << std::endl;
    }
  }




  // instantiate receiver methods for the possible calibration data types
  template
  void IsReceptor::setValue<State_t>(EValueConnType conn_type,
				     EMeasurementType measurement,
				     SerialNumber_t serial_number,
				     const std::string &is_name,
				     const IVarNameExtractor &extractor,
				     ::time_t time_stamp,
				     State_t value);

  template
  void IsReceptor::setValue<float>(EValueConnType conn_type,
				     EMeasurementType measurement,
				     SerialNumber_t serial_number,
				     const std::string &is_name,
				     const IVarNameExtractor &extractor,
				     ::time_t time_stamp,
				     float value);


}
