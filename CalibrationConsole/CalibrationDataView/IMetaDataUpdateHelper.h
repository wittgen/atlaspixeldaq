#ifndef _PixCon_IMetaDataUpdateHelper_h_
#define _PixCon_IMetaDataUpdateHelper_h_

#include <CalibrationDataTypes.h>
#include <string>

namespace PixCon {

  class IMetaDataUpdateHelper
  {
  public:
    enum EMask { kQuality=1, kComment=2};

    virtual ~IMetaDataUpdateHelper() {}

    virtual void update(EMeasurementType measurement_type,
			SerialNumber_t serial_number,
			int new_quality,
			const std::string &new_comment,
			unsigned short mask) = 0;
  };
}
#endif
