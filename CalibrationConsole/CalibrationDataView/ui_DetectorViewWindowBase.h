#ifndef UI_DETECTORVIEWWINDOWBASE_H
#define UI_DETECTORVIEWWINDOWBASE_H

#include <QFrame>
#include <QMainWindow>
#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QHeaderView>
#include <QSplitter>
#include <QVBoxLayout>
#include <QWidget>

QT_BEGIN_NAMESPACE

class Ui_DetectorViewWindowBase
{
public:
    QWidget *widget;
    QVBoxLayout *vboxLayout;
    QSplitter *m_splitter;
    QFrame *m_leftPanel;
    QVBoxLayout *vboxLayout1;
    QFrame *m_rightPanel;
    QVBoxLayout *vboxLayout2;

    void setupUi(QMainWindow *DetectorViewWindowBase) {
        widget = new QWidget(DetectorViewWindowBase);
        m_leftPanel = new QFrame(widget);
        vboxLayout = new QVBoxLayout(widget);
        vboxLayout->addWidget(m_leftPanel);
        widget->setLayout(vboxLayout);
        DetectorViewWindowBase->setCentralWidget(widget);
        retranslateUi(DetectorViewWindowBase);
        QMetaObject::connectSlotsByName(DetectorViewWindowBase);
    } // setupUi

    void retranslateUi(QMainWindow *DetectorViewWindowBase){
        DetectorViewWindowBase->setWindowTitle(QApplication::translate("DetectorViewWindowBase", "DetectorViewWindow", 0));
    } // retranslateUi
};

namespace Ui {
    class DetectorViewWindowBase: public Ui_DetectorViewWindowBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DETECTORVIEWWINDOWBASE_H
