#ifndef UI_METADATADETAILSBASE_H
#define UI_METADATADETAILSBASE_H

#include <QTextEdit>
#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QComboBox>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QPushButton>
#include <QSpacerItem>
#include <QVBoxLayout>
#include <QWidget>

QT_BEGIN_NAMESPACE

class Ui_MetaDataDetailsBase
{
public:
    QVBoxLayout *vboxLayout;
    QHBoxLayout *hboxLayout;
    QLabel *m_scanNumber;
    QSpacerItem *spacer3;
    QComboBox *m_quality;
    QPushButton *m_updateButton;
    QHBoxLayout *hboxLayout1;
    QLabel *m_name;
    QSpacerItem *spacer9;
    QLabel *m_scanTagLabel;
    QSpacerItem *spacer4_2;
    QHBoxLayout *hboxLayout2;
    QGridLayout *gridLayout;
    QLabel *m_cfgTagLabel;
    QHBoxLayout *hboxLayout3;
    QSpacerItem *spacer7;
    QLabel *m_cfg1Label;
    QHBoxLayout *hboxLayout4;
    QSpacerItem *spacer12;
    QLabel *m_cfg2Label;
    QLabel *m_modCfgTagLabel;
    QSpacerItem *spacer9_2;
    QVBoxLayout *vboxLayout1;
    QLabel *textLabel6;
    QLabel *m_globalStatus;
    QHBoxLayout *hboxLayout5;
    QLabel *textLabel9;
    QLabel *m_startTime;
    QSpacerItem *spacer4;
    QLabel *textLabel8;
    QLabel *m_duration;
    QTextEdit *m_comment;

    void setupUi(QWidget *MetaDataDetailsBase)
    {
        if (MetaDataDetailsBase->objectName().isEmpty())
            MetaDataDetailsBase->setObjectName(QString::fromUtf8("MetaDataDetailsBase"));
        MetaDataDetailsBase->resize(503, 299);
        vboxLayout = new QVBoxLayout(MetaDataDetailsBase);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(0, 0, 0, 0);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        m_scanNumber = new QLabel(MetaDataDetailsBase);
        m_scanNumber->setObjectName(QString::fromUtf8("m_scanNumber"));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        m_scanNumber->setFont(font);
        m_scanNumber->setWordWrap(false);

        hboxLayout->addWidget(m_scanNumber);

        spacer3 = new QSpacerItem(174, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacer3);

        m_quality = new QComboBox(MetaDataDetailsBase);
        m_quality->setObjectName(QString::fromUtf8("m_quality"));

        hboxLayout->addWidget(m_quality);

        m_updateButton = new QPushButton(MetaDataDetailsBase);
        m_updateButton->setObjectName(QString::fromUtf8("m_updateButton"));

        hboxLayout->addWidget(m_updateButton);


        vboxLayout->addLayout(hboxLayout);

        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setSpacing(6);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        m_name = new QLabel(MetaDataDetailsBase);
        m_name->setObjectName(QString::fromUtf8("m_name"));
        m_name->setFont(font);
        m_name->setWordWrap(false);

        hboxLayout1->addWidget(m_name);

        spacer9 = new QSpacerItem(21, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        hboxLayout1->addItem(spacer9);

        m_scanTagLabel = new QLabel(MetaDataDetailsBase);
        m_scanTagLabel->setObjectName(QString::fromUtf8("m_scanTagLabel"));
        QFont font1;
        m_scanTagLabel->setFont(font1);
        m_scanTagLabel->setWordWrap(false);

        hboxLayout1->addWidget(m_scanTagLabel);

        spacer4_2 = new QSpacerItem(210, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout1->addItem(spacer4_2);


        vboxLayout->addLayout(hboxLayout1);

        hboxLayout2 = new QHBoxLayout();
        hboxLayout2->setSpacing(6);
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        m_cfgTagLabel = new QLabel(MetaDataDetailsBase);
        m_cfgTagLabel->setObjectName(QString::fromUtf8("m_cfgTagLabel"));
        QFont font2;
        font2.setPointSize(8);
        m_cfgTagLabel->setFont(font2);
        m_cfgTagLabel->setFrameShape(QFrame::Box);
        m_cfgTagLabel->setWordWrap(false);

        gridLayout->addWidget(m_cfgTagLabel, 0, 1, 1, 1);

        hboxLayout3 = new QHBoxLayout();
        hboxLayout3->setSpacing(6);
        hboxLayout3->setObjectName(QString::fromUtf8("hboxLayout3"));
        spacer7 = new QSpacerItem(0, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout3->addItem(spacer7);

        m_cfg1Label = new QLabel(MetaDataDetailsBase);
        m_cfg1Label->setObjectName(QString::fromUtf8("m_cfg1Label"));
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(1), static_cast<QSizePolicy::Policy>(1));
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(m_cfg1Label->sizePolicy().hasHeightForWidth());
        m_cfg1Label->setSizePolicy(sizePolicy);
        m_cfg1Label->setFont(font2);
        m_cfg1Label->setWordWrap(false);

        hboxLayout3->addWidget(m_cfg1Label);


        gridLayout->addLayout(hboxLayout3, 0, 0, 1, 1);

        hboxLayout4 = new QHBoxLayout();
        hboxLayout4->setSpacing(6);
        hboxLayout4->setObjectName(QString::fromUtf8("hboxLayout4"));
        spacer12 = new QSpacerItem(21, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout4->addItem(spacer12);

        m_cfg2Label = new QLabel(MetaDataDetailsBase);
        m_cfg2Label->setObjectName(QString::fromUtf8("m_cfg2Label"));
        sizePolicy.setHeightForWidth(m_cfg2Label->sizePolicy().hasHeightForWidth());
        m_cfg2Label->setSizePolicy(sizePolicy);
        m_cfg2Label->setFont(font2);
        m_cfg2Label->setWordWrap(false);

        hboxLayout4->addWidget(m_cfg2Label);


        gridLayout->addLayout(hboxLayout4, 1, 0, 1, 1);

        m_modCfgTagLabel = new QLabel(MetaDataDetailsBase);
        m_modCfgTagLabel->setObjectName(QString::fromUtf8("m_modCfgTagLabel"));
        m_modCfgTagLabel->setFont(font2);
        m_modCfgTagLabel->setFrameShape(QFrame::Box);
        m_modCfgTagLabel->setWordWrap(false);

        gridLayout->addWidget(m_modCfgTagLabel, 1, 1, 1, 1);


        hboxLayout2->addLayout(gridLayout);

        spacer9_2 = new QSpacerItem(50, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout2->addItem(spacer9_2);

        vboxLayout1 = new QVBoxLayout();
        vboxLayout1->setSpacing(6);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        textLabel6 = new QLabel(MetaDataDetailsBase);
        textLabel6->setObjectName(QString::fromUtf8("textLabel6"));
        QSizePolicy sizePolicy1(static_cast<QSizePolicy::Policy>(1), static_cast<QSizePolicy::Policy>(5));
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(textLabel6->sizePolicy().hasHeightForWidth());
        textLabel6->setSizePolicy(sizePolicy1);
        textLabel6->setWordWrap(false);

        vboxLayout1->addWidget(textLabel6);

        m_globalStatus = new QLabel(MetaDataDetailsBase);
        m_globalStatus->setObjectName(QString::fromUtf8("m_globalStatus"));
        sizePolicy1.setHeightForWidth(m_globalStatus->sizePolicy().hasHeightForWidth());
        m_globalStatus->setSizePolicy(sizePolicy1);
        m_globalStatus->setFrameShape(QFrame::Box);
        m_globalStatus->setWordWrap(false);

        vboxLayout1->addWidget(m_globalStatus);


        hboxLayout2->addLayout(vboxLayout1);


        vboxLayout->addLayout(hboxLayout2);

        hboxLayout5 = new QHBoxLayout();
        hboxLayout5->setSpacing(6);
        hboxLayout5->setObjectName(QString::fromUtf8("hboxLayout5"));
        textLabel9 = new QLabel(MetaDataDetailsBase);
        textLabel9->setObjectName(QString::fromUtf8("textLabel9"));
        textLabel9->setFont(font2);
        textLabel9->setWordWrap(false);

        hboxLayout5->addWidget(textLabel9);

        m_startTime = new QLabel(MetaDataDetailsBase);
        m_startTime->setObjectName(QString::fromUtf8("m_startTime"));
        m_startTime->setWordWrap(false);

        hboxLayout5->addWidget(m_startTime);

        spacer4 = new QSpacerItem(100, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout5->addItem(spacer4);

        textLabel8 = new QLabel(MetaDataDetailsBase);
        textLabel8->setObjectName(QString::fromUtf8("textLabel8"));
        textLabel8->setWordWrap(false);

        hboxLayout5->addWidget(textLabel8);

        m_duration = new QLabel(MetaDataDetailsBase);
        m_duration->setObjectName(QString::fromUtf8("m_duration"));
        m_duration->setWordWrap(false);

        hboxLayout5->addWidget(m_duration);


        vboxLayout->addLayout(hboxLayout5);

        m_comment = new QTextEdit(MetaDataDetailsBase);
        m_comment->setObjectName(QString::fromUtf8("m_comment"));
        QSizePolicy sizePolicy2(static_cast<QSizePolicy::Policy>(7), static_cast<QSizePolicy::Policy>(3));
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(m_comment->sizePolicy().hasHeightForWidth());
        m_comment->setSizePolicy(sizePolicy2);
        m_comment->setMinimumSize(QSize(0, 75));

        vboxLayout->addWidget(m_comment);


        retranslateUi(MetaDataDetailsBase);
        QObject::connect(m_comment, SIGNAL(textChanged()), MetaDataDetailsBase, SLOT(commentChanged()));
        QObject::connect(m_quality, SIGNAL(activated(int)), MetaDataDetailsBase, SLOT(qualityChanged(int)));
        QObject::connect(m_updateButton, SIGNAL(clicked()), MetaDataDetailsBase, SLOT(updateMetaData()));

        QMetaObject::connectSlotsByName(MetaDataDetailsBase);
    } // setupUi

    void retranslateUi(QWidget *MetaDataDetailsBase)
    {
        MetaDataDetailsBase->setWindowTitle(QApplication::translate("MetaDataDetailsBase", "Form1", 0));
        m_scanNumber->setText(QApplication::translate("MetaDataDetailsBase", "S0000001", 0));
        m_updateButton->setText(QApplication::translate("MetaDataDetailsBase", "Update", 0));
        m_name->setText(QApplication::translate("MetaDataDetailsBase", "DIGITAL_TEST", 0));
        m_scanTagLabel->setText(QApplication::translate("MetaDataDetailsBase", "[Preset / 2008-09-10 20:00:00]", 0));
        m_cfgTagLabel->setText(QApplication::translate("MetaDataDetailsBase", "PIT-20C / 2008-09-10 20:00:00 (lost)", 0));
        m_cfg1Label->setText(QApplication::translate("MetaDataDetailsBase", "Configuration tags :", 0));
        m_cfg2Label->setText(QApplication::translate("MetaDataDetailsBase", "module :", 0));
        m_modCfgTagLabel->setText(QApplication::translate("MetaDataDetailsBase", "PIT-20C / 2008-09-10 20:00:00 (temporary)", 0));
        textLabel6->setText(QApplication::translate("MetaDataDetailsBase", "Global status", 0));
        m_globalStatus->setText(QApplication::translate("MetaDataDetailsBase", "unknown", 0));
        textLabel9->setText(QApplication::translate("MetaDataDetailsBase", "Start :", 0));
        m_startTime->setText(QApplication::translate("MetaDataDetailsBase", "2007-11-07 10:20:00", 0));
        textLabel8->setText(QApplication::translate("MetaDataDetailsBase", "Duration :", 0));
        m_duration->setText(QApplication::translate("MetaDataDetailsBase", "15 min", 0));
    } // retranslateUi

};

namespace Ui {
    class MetaDataDetailsBase: public Ui_MetaDataDetailsBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_METADATADETAILSBASE_H
