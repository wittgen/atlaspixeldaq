#ifndef _PixCon_EvaluatorFactory_h_
#define _PixCon_EvaluatorFactory_h_

#include "CalibrationData.h"
#include "guessConnItemType.h"
#include <vector>
#include <stdexcept>

#include "IEvaluator.h"
#include "ValueList.h"

namespace PixCon {

  class Measurement_t
  {
  public:
    Measurement_t(const Measurement_t &a)
      :  m_measurementType(a.m_measurementType),
	 m_serialNumber(a.m_serialNumber),
	 m_fixedSerialNumber(a.m_fixedSerialNumber)
    {}

    Measurement_t()
      : m_fixedSerialNumber(false)
    {}

    Measurement_t(EMeasurementType measurement_type, SerialNumber_t serial_number, bool fixed_measurement=true)
      : m_measurementType(measurement_type),
	m_serialNumber(serial_number),
	m_fixedSerialNumber(fixed_measurement)
    {
    }

    EMeasurementType measurementType() const { return m_measurementType; }
    SerialNumber_t serialNumber()      const { return m_serialNumber; }
    bool fixedMeasurement()            const { return m_fixedSerialNumber; }

  private:

    EMeasurementType m_measurementType;
    SerialNumber_t m_serialNumber;
    bool m_fixedSerialNumber;
  };


  class EvaluatorWithParameter : public IEvaluator
  {
  public:
    EvaluatorWithParameter(EvaluationParameter &parameter) : m_parameter(&parameter) {}
    const EvaluationParameter &parameter() const {return *m_parameter; }

  private:
    EvaluationParameter *m_parameter;
  };

  class VariableMeasurementEvaluator : public EvaluatorWithParameter
  {
  public:

    // only define measurement name if in debugging mode to avoid compile warnings
    VariableMeasurementEvaluator(EvaluationParameter &parameter,
				 const Measurement_t &
#ifndef NDEBUG
				 variable_measurement
#endif
				 )
      : EvaluatorWithParameter(parameter)
    {
      assert(!variable_measurement.fixedMeasurement());
    }

    SerialNumber_t serialNumber()  const { return parameter().serialNumber(); }
    EMeasurementType measurementType() const { return parameter().measurementType(); }
  private:
    //EvaluationParameter *m_parameter;
  };

  class FixedMeasurementEvaluator : public EvaluatorWithParameter
  {
  public:
    FixedMeasurementEvaluator(EvaluationParameter &parameter, const Measurement_t &fixed_measurement)
      : EvaluatorWithParameter(parameter),
	m_fixedMeasurement( fixed_measurement )
    {
      assert(fixed_measurement.fixedMeasurement()
	     && (   ((fixed_measurement.measurementType()==kCurrent) && (fixed_measurement.serialNumber()==0))
   	         || (((fixed_measurement.measurementType()==kScan) ||  (fixed_measurement.measurementType()==kAnalysis)) && (fixed_measurement.serialNumber()>0))));
    }
    SerialNumber_t serialNumber() const  { return m_fixedMeasurement.serialNumber(); }
    EMeasurementType measurementType() const { return m_fixedMeasurement.measurementType(); }

  private:
    Measurement_t m_fixedMeasurement;
  };


  class ConnNameComparator  : public EvaluatorWithParameter
  {
  public:
    ConnNameComparator(EvaluationParameter &parameter, const std::string &pattern);

    EvalResult_t eval() const;

  protected:
    EvalResult_t eval(const std::string &match_name) const;
  private:
    std::vector<std::string> m_patterns;
    bool m_fromBeginning;
  };

  template <class T_Evaluator_t>
  class EnableEvaluator  : public T_Evaluator_t
  {
  public:
    EnableEvaluator(EvaluationParameter &parameter, const Measurement_t &measurement, bool enabled)
      : T_Evaluator_t(parameter, measurement),
	m_enabled(enabled)
    {}

    EvalResult_t eval() const {
      //      if (  guessConnItemType( parameter().connectivityName() ) == kModuleItem ) {
	// only consider disable state of modules
	try {
	  CalibrationData::EnableState_t enable_state = this->parameter().dataList().enableState(this->measurementType(), this->serialNumber());
	  if (enable_state.enabled() ^ m_enabled) {
	    return EvalResult_t::falseResult();
	    //	    return !m_enabled;
	  }
	  else {
	    return EvalResult_t::trueResult();
	    //	    return m_enabled;
	  }
	}
	catch(CalibrationData::MissingConnObject &) {
	}
	//      }
      return EvalResult_t::unknownResult();
    }
  private:
    bool m_enabled;
  };

  template <class T_Evaluator_t>
  class AllocatedEvaluator  : public T_Evaluator_t
  {
  public:
    AllocatedEvaluator(EvaluationParameter &parameter, const Measurement_t &measurement, bool allocated)
      : T_Evaluator_t(parameter, measurement),
	m_allocated(allocated)
    {}

    EvalResult_t eval() const {
      if (  guessConnItemType( this->parameter().connectivityName() ) == kRodItem ) {
	// only consider disable state of modules
	try {
	  CalibrationData::EnableState_t enable_state = this->parameter().dataList().enableState(this->measurementType(), this->serialNumber());
	  if (enable_state.isAllocated() ^ m_allocated) {
	    return EvalResult_t::falseResult();
	  }
	  else {
	    return EvalResult_t::trueResult();
	  }
	}
	catch(CalibrationData::MissingConnObject &) {
	}
      }
      return EvalResult_t::unknownResult();
    }
  private:
    bool m_allocated;
  }; 

  template <class T, class T_Evaluator_t>
  class DefinedEvaluator  : public T_Evaluator_t
  {
  public:
    DefinedEvaluator(EvaluationParameter &parameter, const Measurement_t &measurement, const VarDef_t &var_def, bool true_if_undefined)
      : T_Evaluator_t(parameter, measurement),
        m_variableDef(var_def),
	m_trueIfUndefined(true_if_undefined)
    {}

    EvalResult_t eval() const {
      //      if (  guessConnItemType( parameter().connectivityName() ) == kModuleItem ) {
	// only consider disable state of modules
      bool defined=false;
      try {
	this->parameter().dataList().CalibrationData::ConnObjDataList::template newestValue<T>(this->measurementType(), this->serialNumber(), m_variableDef);
	defined=true;
      }
/*       catch(CalibrationData::MissingConnObject &) { */
/* 	return EvalResult_t::unknownResult(); */
/*       } */
      catch(CalibrationData::MissingValue &) {
      }
      if (defined ^ m_trueIfUndefined) {
	    return EvalResult_t::trueResult();
      }
      else {
	return EvalResult_t::falseResult();
      }
    }
  private:
    VarDef_t m_variableDef;
    bool m_trueIfUndefined;
  };


  template <class T>
  class ValueComparator : public IEvaluator
  {
  protected:
    ValueComparator(Value<T> *a, Value<T> *b) { m_value[0]=a; m_value[1]=b;}
    T a() const { return m_value[0]->value(); }
    T b() const { return m_value[1]->value(); }

  private:
    Value<T> *m_value[2];
  };

  template <class T>
  class Greater : public ValueComparator<T>
  {
  public:
    Greater(Value<T> *a, Value<T> *b) : ValueComparator<T>(a,b) {}
    EvalResult_t eval() const {
      try {
	return EvalResult_t(ValueComparator<T>::a() > ValueComparator<T>::b()); 
      }
      catch(CalibrationData::MissingValue &) {
      }
      return EvalResult_t::unknownResult();
    }
  };

  template <class T>
  class GreaterEqual : public ValueComparator<T>
  {
  public:
    GreaterEqual(Value<T> *a, Value<T> *b) : ValueComparator<T>(a,b) {}
    EvalResult_t eval() const {
      try {
	return ValueComparator<T>::a() >= ValueComparator<T>::b(); 
      }
      catch(CalibrationData::MissingValue &) {
      }
      return EvalResult_t::unknownResult();
    }
  };

  template <class T>
  class Equal : public ValueComparator<T>
  {
  public:
    Equal(Value<T> *a, Value<T> *b) : ValueComparator<T>(a,b) {}
    EvalResult_t eval() const {
      try {
	return ValueComparator<T>::a() == ValueComparator<T>::b(); 
      }
      catch(CalibrationData::MissingValue &) {
      }
      return EvalResult_t::unknownResult();
    }
  };

  class EvaluatorFactory
  {
  public:
    enum ECompareOperatorType {kEqual, kNotEqual, kGreater, kLess, kGreaterEqual, kLessEqual, kNCompareOperators };
    enum EArithmeticOperatorType {kPlus,kMinus, kMultiply, kDivide, kNArithmeticOperators };
    enum ESpecialOperator {kSeparator,kNSpecialOperators};

    EvaluatorFactory(EvaluationParameter &parameter, ValueList<float> &value_list, ValueList<State_t> &state_list) 
      : m_parameter(&parameter),
	m_valueList( &value_list),
	m_stateList( &state_list)
    {  }
    ~EvaluatorFactory() {}

    std::pair<IEvaluator *, bool> createConnNameComparator(const std::string &pattern);
    std::pair<IEvaluator *, bool> createEnableEvaluator(bool negate, bool negation, const Measurement_t &measurement);
    std::pair<IEvaluator *, bool> createAllocatedEvaluator(bool negate, bool negation, const Measurement_t &measurement);

    template <class T> inline std::pair<IEvaluator *, bool> createDefinedEvaluator(const VarDef_t &var_def,
										   const Measurement_t &measurement,
										   bool negate,
										   bool negation);

    template <class T> std::pair<IEvaluator *, bool> createValueComparator(const VarDef_t &var_def,
									   const Measurement_t &measurement,
									   T value,
									   ECompareOperatorType type);

    template <class T> std::pair<IEvaluator *, bool> createValueComparator(T value,
									   const VarDef_t &var_def,
									   const Measurement_t &measurement,
									   ECompareOperatorType type);

    template <class T> std::pair<IEvaluator *, bool> createValueComparator(const VarDef_t &var_a,
									   const Measurement_t &measurement_a,
									   const VarDef_t &var_b,
									   const Measurement_t &measurement_b,
									   ECompareOperatorType type);

    //    template <class T> std::pair<IEvaluator *, bool> createValueComparator(const std::string &variable_a, const std::string &variable_b, ECompareOperatorType type);

    static ECompareOperatorType redirectOperator(ECompareOperatorType operator_type) {
      return (operator_type>kNotEqual ? static_cast<ECompareOperatorType>(operator_type ^ 1) : operator_type );
    }

  private:
    template <class T> std::pair<IEvaluator *, bool> createValueComparator(Value<T> *a, Value<T> *b, ECompareOperatorType type);

    template <class T> ValueList<T> &valueList();

    template <class T> Value<T> *createConstValue(T value) {
      return valueList<T>().addValue(value);
    }

    template <class T> Value<T> *createValue(const VarDef_t &def) {
      return valueList<T>().addValue(def, *m_parameter);
    }

    template <class T> Value<T> *createValue(const VarDef_t &def, const Measurement_t &measurement) {
      if (measurement.fixedMeasurement()) {
	return valueList<T>().addValue(def, *m_parameter, measurement.measurementType(), measurement.serialNumber());
      }
      else {
	return valueList<T>().addValue(def, *m_parameter);
      }
    }

    EvaluationParameter *m_parameter;
    ValueList<float>    *m_valueList;
    ValueList<State_t>  *m_stateList;
  };

  template <> inline ValueList<float>   &EvaluatorFactory::valueList<float>()   { return *m_valueList; }
  template <> inline ValueList<State_t> &EvaluatorFactory::valueList<State_t>() { return *m_stateList; }

  inline std::pair<IEvaluator *, bool> EvaluatorFactory::createConnNameComparator(const std::string &pattern) {
    return std::make_pair(new ConnNameComparator(*m_parameter, pattern), false);
  }


  inline std::pair<IEvaluator *, bool> EvaluatorFactory::createEnableEvaluator(bool negate, bool negation, const Measurement_t &measurement) {
    if(measurement.fixedMeasurement()) {
      return std::make_pair(new EnableEvaluator<FixedMeasurementEvaluator>(*m_parameter, measurement, !negate ^ negation), negation);
    }
    else {
      return std::make_pair(new EnableEvaluator<VariableMeasurementEvaluator>(*m_parameter, measurement, !negate ^ negation), negation);
    }
  }

  inline std::pair<IEvaluator *, bool> EvaluatorFactory::createAllocatedEvaluator(bool negate, bool negation, const Measurement_t &measurement) {
    if(measurement.fixedMeasurement()) {
      return std::make_pair(new AllocatedEvaluator<FixedMeasurementEvaluator>(*m_parameter, measurement, negate ^ negation), negation);
    }
    else {
      return std::make_pair(new AllocatedEvaluator<VariableMeasurementEvaluator>(*m_parameter, measurement, negate ^ negation), negation);
    }
  }
  template <class T> inline std::pair<IEvaluator *, bool> EvaluatorFactory::createDefinedEvaluator(const VarDef_t &var_def, const Measurement_t &measurement, bool negate, bool negation) {
    if(measurement.fixedMeasurement()) {
      return std::make_pair(new DefinedEvaluator<T,FixedMeasurementEvaluator>(*m_parameter, measurement, var_def, negate ^ negation), negation);
    }
    else {
      return std::make_pair(new DefinedEvaluator<T,VariableMeasurementEvaluator>(*m_parameter, measurement, var_def, negate ^ negation), negation);
    }
  }

  template <class T>
  inline std::pair<IEvaluator *, bool> EvaluatorFactory::createValueComparator(const VarDef_t &var_a, const Measurement_t &measurement_a,
									       const VarDef_t &var_b, const Measurement_t &measurement_b,
									       EvaluatorFactory::ECompareOperatorType type) {
    Value<T> *value_eval_a = createValue<T>(var_a,measurement_a);
    Value<T> *value_eval_b = createValue<T>(var_b,measurement_b);
    return createValueComparator<T>(value_eval_a,value_eval_b,type );
  }

  template <class T>
  inline std::pair<IEvaluator *, bool> EvaluatorFactory::createValueComparator(const VarDef_t &var_a,
									       const Measurement_t &measurement_a,
									       T  value,
									       EvaluatorFactory::ECompareOperatorType type) {
    Value<T> *value_eval_a = createValue<T>(var_a,measurement_a);
    Value<T> *value_eval_b = createConstValue<T>(value);
    return createValueComparator<T>(value_eval_a,value_eval_b,type );
  }

  template <class T>
  inline std::pair<IEvaluator *, bool> EvaluatorFactory::createValueComparator(T value,
									       const VarDef_t &var_a,
									       const Measurement_t &measurement_a,
									       EvaluatorFactory::ECompareOperatorType type) {
    return createValueComparator(var_a,measurement_a, value, redirectOperator(type));
  }

  template <class T>
  std::pair<IEvaluator *, bool> EvaluatorFactory::createValueComparator(Value<T> *a, Value<T> *b, EvaluatorFactory::ECompareOperatorType type) {
    switch (type) {
    case kGreater:
      return std::make_pair(new Greater<T>(a,b),false);
    case kGreaterEqual:
      return std::make_pair(new GreaterEqual<T>(a,b),false);
    case kLess:
      return std::make_pair(new GreaterEqual<T>(a,b),true);
    case kLessEqual:
      return std::make_pair(new Greater<T>(a,b),true);
    case kEqual:
      return std::make_pair(new Equal<T>(a,b),false);
    case kNotEqual:
      return std::make_pair(new Equal<T>(a,b),true);
    default: {
      throw std::runtime_error("Invalid comparison operator.");
    }
    }
  }

}
#endif
