#ifndef _ConnListItemView_H_
#define _ConnListItemView_H_

#include <ConfigWrapper/Connectivity.h>
#include "ConnItemSelection.h"
#include <QTreeWidget>

namespace PixCon {

  class CalibrationDataManager;
  class DataSelection;
  class ConnListItem;

  class ConnListItemView
  {
    enum EViewType {kGeographical, kReadout};

  public:
    ConnListItemView(QTreeWidget *list_view,
		     const std::shared_ptr<CalibrationDataManager> &calibration_data_manager,
		     const std::shared_ptr<DataSelection> data_selection)
      : m_listView(list_view),
	m_viewType(kReadout),
	m_calibrationDataManager(calibration_data_manager),
	m_dataSelection(data_selection),
	m_itemSelection(m_calibrationDataManager, m_dataSelection)
    {createView(); }

    ~ConnListItemView() {}

    void setGeographicalListView() {
      if (m_viewType != kGeographical) {
	m_viewType = kGeographical;
	createGeographicalView( );
      }
    }

    void setReadoutView() {
      if (m_viewType != kReadout) {
	m_viewType = kReadout;
	createReadoutView();
      }
    }

//     void filterItems(const ConnItemSelection &selection) {
//       m_itemSelection=selection;
//       createView();
//     }

    void filterItems(const std::string &filter_pattern) {
      m_itemSelection.setFilterString( filter_pattern );
      createView();
    }

    void changeDataSelection(std::shared_ptr<DataSelection> data_selection) {
      m_dataSelection = data_selection;
      createView();
    }

    void updateItem(const std::string &conn_name);

    void changedVariable();

    void createView();

    bool isSelected(const std::string &conn_name) {
      return m_itemSelection.isSelected(conn_name);
    }

    bool isSelectedFromList(const std::string &conn_name) const {
      return m_itemMap.find(conn_name) != m_itemMap.end();
    }

    bool usesVariable(EMeasurementType measurement_type, SerialNumber_t serial_number, VarId_t id) const {
      return m_itemSelection.usesVariable(measurement_type, serial_number, id);
    }

    void listOfUsedMeasurements( std::map<EMeasurementType, SerialNumber_t> &used_measurements ) const;

  protected:

    void createGeographicalView( );
    void createReadoutView( );

  private:
    QTreeWidget            *m_listView;
    EViewType               m_viewType;
    std::shared_ptr<CalibrationDataManager> m_calibrationDataManager;
    std::shared_ptr<DataSelection>          m_dataSelection;
    ConnItemSelection       m_itemSelection;
    std::map<std::string, ConnListItem *>     m_itemMap;
  };
}
#endif
