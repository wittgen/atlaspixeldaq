#ifndef _EmbeddedRootCanvas_h_
#define _EmbeddedRootCanvas_h_

class TCanvas;
class TQRootCanvas;
TCanvas *getCanvas(TQRootCanvas *a_canvas);

// helper file which includes the correct header
//#  if  defined(HAVE_QTGSI)
#    include "TQRootCanvas.h"
inline TCanvas *getCanvas(TQRootCanvas *a_canvas) {
  return a_canvas->GetCanvas();
}
/*#  elif defined(HAVE_TDAQ_QTROOT)
#    include "tqrootcanvas.h"
inline TCanvas *getCanvas(TQRootCanvas *a_canvas) {
  return a_canvas->getCanvas();
}
#  endif
*/



#endif
