#include <EvalResult_t.h>
namespace PixCon {

  EvalResult_t::EResult EvalResult_t::s_combMap[2][3][3];

  bool EvalResult_t::init() {
    for (unsigned short i=0; i<=kUnknown; i++) {
      for (unsigned short j=0; j<=kUnknown; j++) {
	s_combMap[kOr][i][j]=orResult(static_cast<EResult>(i), static_cast<EResult>(j));
	s_combMap[kAnd][i][j]=andResult(static_cast<EResult>(i), static_cast<EResult>(j));
      }
    }
    return true;
  }

  bool EvalResult_t::s_initialised = EvalResult_t::init();

}
