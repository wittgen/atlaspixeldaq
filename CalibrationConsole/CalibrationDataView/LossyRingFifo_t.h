/* Dear emacs, this is -*-c++-*- */
#ifndef _PixCon_LossyRingFifo_t_h_
#define _PixCon_LossyRingFifo_t_h_

#ifdef DEBUG
#  define DEBUG_TRACE(a) { a; }
#  include <iostream>
#else 
#  define DEBUG_TRACE(a) { }
#endif

namespace PixCon {

  template <class T>
  class LossyRingFifo_t {
  public:
    LossyRingFifo_t(unsigned int size)
      : m_buffer(NULL),
	m_bufferEnd(NULL),
	m_readPtr(NULL),
	m_writePtr(NULL),
        m_overflow(0)
    {
      m_buffer=new T[size];
      m_bufferEnd=m_buffer+size;
      m_readPtr=m_buffer;
      m_writePtr=m_buffer;
    }

    ~LossyRingFifo_t() { delete [] m_buffer; }

    bool hasData() const { return m_readPtr != m_writePtr; }

    T &current() {
      return *m_writePtr;
    }

    void add(const T &a) {
      *m_writePtr = a;
      advance();
    }

    void advance(){
      T *next_write_ptr = next(m_writePtr);
      if (next_write_ptr == m_readPtr) {
	m_readPtr = next(m_readPtr);
	m_overflow++;
      }
      m_writePtr = next_write_ptr;

      //      this->debug();
    }

    T next() { assert( m_readPtr !=m_writePtr); T val=*m_readPtr; 
#ifdef DEBUG
    //      T *old_ptr = m_readPtr;
#endif
      m_readPtr=next(m_readPtr);
      //      DEBUG_TRACE( std::cout << "INFO [LossyRingFifo_t] read " << static_cast<void *>(old_ptr) <<  std::endl )
      return val; }

    unsigned int overflow() const {
      return m_overflow;
    } 

    void clearOverflowCounter() { m_overflow=0; }

    void debug();

  private:

    T *next( T* ptr) {
      ptr++;
      if (ptr==m_bufferEnd) {
	ptr=m_buffer;
      }
      return ptr;
    }

    T *m_buffer;
    T *m_bufferEnd;
    T *m_readPtr;
    T *m_writePtr;
    unsigned int m_overflow;
  };

  template <class T>
  inline void LossyRingFifo_t<T>::debug() {
#  ifdef DEBUG
    if (m_overflow>0) {
      unsigned long a=static_cast<unsigned long>(m_writePtr - m_buffer);
      unsigned long b=static_cast<unsigned long>(m_readPtr - m_buffer);
      unsigned long c=static_cast<unsigned long>(m_bufferEnd - m_buffer);
      unsigned long distance=0;
      if (b>=a) {
	distance = b-a;
      }
      else {
	distance = c-a+b;
      }

      std::cout << "INFO [LossyRingFifo_t::advance] read = "  << static_cast<void *>(m_readPtr) << "  write = " << static_cast<void *>(m_writePtr) 
		<<  " size = " << c
		<< " distance =" <<distance
		<< " overflow=" << m_overflow
		<< std::endl;
    }
#  endif
  }
}
#endif
