#ifndef VARLIST_H
#define VARLIST_H

#include <qvariant.h>
#include <qdialog.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QListView;
class QListViewItem;

class VarList : public QDialog
{
    Q_OBJECT

public:
    VarList( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~VarList();


    QListView* listView1;

protected:
    QVBoxLayout* VarListLayout;

protected slots:
    virtual void languageChange();

};


#endif // VARLIST_H
