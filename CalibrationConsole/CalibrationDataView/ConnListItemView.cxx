#include "ConnListItemView.h"
#include "ConnListItemFactory.h"
#include <ConfigWrapper/ConnectivityUtil.h>
#include <CalibrationDataManager.h>
#include <CalibrationDataStyle.h>

namespace PixCon {

  class ReadoutHierarchy
  {
  public:
    void push_back(const std::string &conn_name, EConnItemType type) {
      m_connNameList.push_back(std::make_pair(conn_name,type) );
    }

    void pop() {
      if (m_connNameList.size()==m_connItemList.size()) {
	m_connItemList.pop_back();
      }
      m_connNameList.pop_back();
    }

    ConnListItem *getParent(ConnListItemFactory &factory) 
    {
      while (m_connItemList.size()<m_connNameList.size()) {
	unsigned int level_i = m_connItemList.size();
	m_connItemList.push_back(factory.createItem( (level_i>0 ? m_connItemList[level_i-1] : NULL ), m_connNameList[level_i].first, m_connNameList[level_i].second) );
      }
      return m_connItemList.back();
    }

  private:

    std::vector< std::pair< std::string, EConnItemType > >   m_connNameList;
    std::vector<PixCon::ConnListItem *>                                    m_connItemList;
  };

  void ConnListItemView::createReadoutView( ) 
  {

    ConnListItemFactory factory(m_listView, m_calibrationDataManager->calibrationData(), *m_dataSelection, m_itemSelection, m_itemMap);
    ReadoutHierarchy readout_hierarchy;

    PixA::ConnectivityRef conn = m_calibrationDataManager->connectivity(m_dataSelection->type(),
									m_dataSelection->serialNumber());

    if (!conn) return;
    for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
	 crate_iter != conn.crates().end();
	 ++crate_iter) {

      readout_hierarchy.push_back(PixA::connectivityName(crate_iter), PixCon::kCrateItem);

      EvalResult_t crate_selected = m_itemSelection.isSelected(PixA::connectivityName(crate_iter));
      if (crate_selected) {
	const PixLib::TimConnectivity *tim_conn = PixA::responsibleTim(*crate_iter);
	if (tim_conn) {
	  PixCon::ConnListItem *crate_item = readout_hierarchy.getParent(factory);
	  factory.createTimItem(crate_item,PixA::connectivityName(tim_conn));
	}
      }

      PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
      PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
      for (PixA::RodList::const_iterator rod_iter = rod_begin;
	   rod_iter != rod_end;
	   ++rod_iter) {

	readout_hierarchy.push_back(PixA::connectivityName(rod_iter), PixCon::kRodItem);

	EvalResult_t rod_selected=(m_itemSelection.isSelected(PixA::connectivityName(rod_iter)));
	if ( rod_selected.isUnknown() ) rod_selected = crate_selected;

	if (rod_selected) {
	  readout_hierarchy.getParent(factory);
	}

	PixA::Pp0List::const_iterator pp0_begin = PixA::pp0Begin(rod_iter);
	PixA::Pp0List::const_iterator pp0_end = PixA::pp0End(rod_iter);
	for (PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
	     pp0_iter != pp0_end;
	     ++pp0_iter) {

	  readout_hierarchy.push_back(PixA::connectivityName(pp0_iter), PixCon::kPp0Item);

	  EvalResult_t pp0_selected=(m_itemSelection.isSelected(PixA::connectivityName(pp0_iter)));
	  if (pp0_selected.isUnknown()) pp0_selected = rod_selected;

	  if (pp0_selected) {
	    readout_hierarchy.getParent(factory);
	  }

	  PixA::ModuleList::const_iterator module_begin = PixA::modulesBegin(pp0_iter);
	  PixA::ModuleList::const_iterator module_end = PixA::modulesEnd(pp0_iter);
	  for (PixA::ModuleList::const_iterator module_iter = module_begin;
	       module_iter != module_end;
	       ++module_iter) {
	    EvalResult_t module_selected = m_itemSelection.isSelected(PixA::connectivityName(module_iter));
	    if (static_cast<bool>(module_selected) || (module_selected.isUnknown() && static_cast<bool>(pp0_selected))) {
	      PixCon::ConnListItem *pp0_item = readout_hierarchy.getParent(factory);
	      factory.createModuleItem(pp0_item,PixA::connectivityName(module_iter));
	    }

	  }
	  readout_hierarchy.pop(); //pp0
	}
	readout_hierarchy.pop(); //rod
      }
      readout_hierarchy.pop(); //crate
    }
  }

  void ConnListItemView::updateItem(const std::string &conn_name)
  {

    if (m_dataSelection->varId().isValid()) {

      std::map<std::string, ConnListItem *>::iterator item_iter = m_itemMap.find(conn_name);
      if (item_iter != m_itemMap.end() ) {
	try {
	  CalibrationData::ConnObjDataList data( m_calibrationDataManager->calibrationData().getConnObjectData(conn_name) );
	  ConnListItemFactory::setEnable(*m_dataSelection, data,item_iter->second);
	  ConnListItemFactory::setText(*m_dataSelection, data,item_iter->second,2, ValueDescription_t(m_dataSelection->varId()));
	}
	catch (CalibrationData::MissingValue &err) {
	  item_iter->second->setText(2,"");
	}

      }

    }
  }

  void ConnListItemView::changedVariable() {
    if (m_listView->columnCount()>2) {


      if (m_dataSelection->varId().isValid()) {

    if (QString::fromStdString(VarDefList::instance()->varName(m_dataSelection->varId().id())) != m_listView->headerItem()->text(2)) {
      m_listView->headerItem()->setText(2,QString::fromStdString(VarDefList::instance()->varName(m_dataSelection->varId().id())));
	}

	for (std::map<std::string, ConnListItem *>::const_iterator item_iter = m_itemMap.begin();
	     item_iter != m_itemMap.end();
	     item_iter++) {

	  try {
	    CalibrationData::ConnObjDataList data( m_calibrationDataManager->calibrationData().getConnObjectData(item_iter->first) );
	    ConnListItemFactory::setText(*m_dataSelection, data,item_iter->second,2, ValueDescription_t(m_dataSelection->varId()));
	  }
	  catch (CalibrationData::MissingValue &err) {
	  }

	}

      }
      else {

	for (std::map<std::string, ConnListItem *>::const_iterator item_iter = m_itemMap.begin();
	     item_iter != m_itemMap.end();
	     item_iter++) {
	  item_iter->second->setText(2,"");
	}

      }

    }
    else {
      createView();
    }
  }

  void ConnListItemView::createView()
  {
    switch (m_viewType) {
    case kGeographical: {
      createGeographicalView();
      break;
    }
    case kReadout: {
     createReadoutView();
      break;
    }

    }

  }

  void ConnListItemView::createGeographicalView( ) {
    // @tod implement
  }

  void ConnListItemView::listOfUsedMeasurements( std::map<EMeasurementType, SerialNumber_t> &used_measurements ) const {
    {
      const PixCon::ValueList<float> &value_list = m_itemSelection.valueList() ;
      for (std::map<ValueListKey_t, Value< float >* >::const_iterator list_iter = value_list.begin();
	   list_iter != value_list.end();
	   ++list_iter) {
	if (list_iter->first.measurementType()<kNMeasurements) {
	  used_measurements.insert( std::make_pair(list_iter->first.measurementType(), list_iter->first.serialNumber()) );
	}
	else {
	  used_measurements.insert( std::make_pair(m_dataSelection->type(), m_dataSelection->serialNumber()) );
	}
      }
    }

    {
      const PixCon::ValueList<PixCon::State_t> &state_list = m_itemSelection.stateList();
      for (std::map<ValueListKey_t, Value< PixCon::State_t >* >::const_iterator list_iter = state_list.begin();
	   list_iter != state_list.end();
	   ++list_iter) {
	if (list_iter->first.measurementType()<kNMeasurements) {
	  used_measurements.insert( std::make_pair(list_iter->first.measurementType(), list_iter->first.serialNumber()) );
	}
	else {
	  used_measurements.insert( std::make_pair(m_dataSelection->type(), m_dataSelection->serialNumber()) );
	}
      }
    }

  }

}
