#include "ModuleList.h"
#include "Highlighter.h"
#include "IContextMenu.h"

#include "LogicParserBase.h"

#include <QList>
#include <QShortcut>

//#include <ConnItem.h>

namespace PixCon {

  ModuleList::ModuleList( const std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data_manager,
			  const std::shared_ptr<PixCon::DataSelection> &data_selection,
			  std::shared_ptr<PixCon::IContextMenu> &context_menu,
			  QWidget* parent )
:QWidget(parent),
    // m_connListItemView(m_moduleList, calibration_data_manager, data_selection),
      m_contextMenu(context_menu),
      m_calibrationData(calibration_data_manager),
      m_dataSelection(data_selection),
      m_arrowRight(QPixmap(":/icons/images/edit-right.png")),
      m_arrowDown(QPixmap(":/icons/images/edit-down.png")),
      m_fullEditorUsed(false)
  { 
    setupUi(this); 
    m_connListItemView = new ConnListItemView(m_moduleList, calibration_data_manager, data_selection);

    m_moduleList->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(m_moduleList,SIGNAL(itemClicked(QTreeWidgetItem*,int)), this, SLOT(mouseButtonClicked(QTreeWidgetItem*,int)));
    connect(m_moduleList, SIGNAL(customContextMenuRequested(const QPoint&)),this,SLOT(contextMenu(const QPoint&)));
    connect(m_moduleList, SIGNAL(itemDoubleClicked(QTreeWidgetItem*, int)), this, SLOT(primaryAction(QTreeWidgetItem*, int)));
    connect(m_filterSelector, SIGNAL(activated(QString)), this, SLOT(setFilterString(QString)));
    connect(m_clearFilterButton, SIGNAL(clicked()), this, SLOT(clearFilter()));
    connect(m_filterButton, SIGNAL(clicked()), this, SLOT(changeFilter()));
    connect(m_fullLineEdit, SIGNAL(clicked()), this, SLOT(showHideFilterEditor()));
    connect(m_filterStringEditor, SIGNAL(textChanged()), this, SLOT(setFullEditorUsed()));
    // nothing like "clicked" available for QTextEdit Class
    //connect(m_filterStringEditor, SIGNAL(clicked(int,int)), this, SLOT(setFullEditorUsedInt(int,int)));
    connect(m_filterSelector, SIGNAL(editTextChanged(QString)), this, SLOT(setLineEditorUsed(QString)));
    connect(m_filterSelector, SIGNAL(activated(int)), this, SLOT(setLineEditorUsedInt(int)));
    connect(m_printButton, SIGNAL(clicked()), this, SLOT(printModuleList()));

    //    m_filterSaveButton->setEnabled(false);
    m_filterSelector->addItem("Enabled");
    m_filterSelector->addItem("Disabled");
    m_filterSelector->addItem("Histograms: HISTOGRAMS==Ok");
    m_filterSelector->addItem("Scan_Ok: HISTOGRAMS==Ok && Enabled");
    m_filterSelector->addItem("Scan_NotOk: (HISTOGRAMS!=Ok || !defined(HISTOGRAMS)) && Enabled");
    //    m_filterSelector->addItem("Analysis_Ok: ( (CAN_Status==true || CAN_Classification==true))  && Enabled");
    //    m_filterSelector->addItem("Analysis_NotOk: ( (!defined(CAN_Status) || CAN_Status==false) && (!defined(CAN_Classification) || CAN_Classification==false))  && Enabled");
    m_filterSelector->addItem("Analysis_Ok: CAN_Status==true  && Enabled");
    m_filterSelector->addItem("Analysis_NotOk: (!defined(CAN_Status) || CAN_Status==false)  && Enabled");

    m_lastEditorSize = 0;
    //    m_splitter->setResizeMode(m_filterStringEditor,QSplitter::KeepSize);

    QShortcut *show_hide_editor_accel = new QShortcut(QKeySequence("Ctrl+F") ,this);
    connect(show_hide_editor_accel, SIGNAL(activated()), this, SLOT(showHideFilterEditor()));


    QShortcut *focus_filter_selector_accel = new QShortcut(QKeySequence("Ctrl+Shift+F") ,this);
    connect(focus_filter_selector_accel, SIGNAL(activated()), m_filterSelector, SLOT(setFocus()));

    QShortcut *editor_execute = new QShortcut(QKeySequence("Return+Ctrl") ,this);
    connect(editor_execute, SIGNAL(activated()), this, SLOT(changeFilter()));

    m_filterStringEditor->hide();
  }


  ModuleList::~ModuleList()
  {delete m_connListItemView;
  }

  void ModuleList::clearFilterSelector() {
    m_filterSelector->clear();
  }

  void ModuleList::addItemToFilterSelector(const std::string& filter_string) {
    m_filterSelector->addItem(filter_string.c_str());
  }

  void ModuleList::changedVariable() {
    m_connListItemView->changedVariable();
  }

  void ModuleList::printModuleList() {
    m_moduleListView = new ModuleListView(m_calibrationData, m_dataSelection, currentSelection(true, false), this);
    m_moduleListView->show();
  }

  void ModuleList::clearFilter() {
    m_filterStringEditor->setText("");
    m_filterSelector->setEditText("");
    m_connListItemView->filterItems("");
    emit changedSelection();
  }

  void ModuleList::setFilterString(const QString &filter) {
    m_filterStringEditor->setText(filter);
    m_filterSelector->setEditText(filter);
    setLineEditorUsed(filter);
    changeFilterAndSaveToList(false);
  }

  std::string ModuleList::filterString() const
  {
    std::string filter_string;
    if (m_filterStringEditor->toPlainText().length()>0) {
      filter_string = m_filterStringEditor->toPlainText().toLatin1().data();
      std::string::size_type pos =0;
      while (pos < filter_string.size() && isspace(filter_string[pos])) pos++;
      //	std::string::size_type start=0;
      while (pos < filter_string.size() && (isalpha(filter_string[pos]) || filter_string[pos]=='_')) pos++;
      if (pos < filter_string.size() && filter_string[pos]==':') {
	// is filter name
	pos++;
	while (pos < filter_string.size() && isspace(filter_string[pos])) pos++;
	filter_string = filter_string.substr(pos,filter_string.size()-pos);
      }
    }
    return filter_string;
  }
  
  void ModuleList::changeFilterAndSaveToList(bool save_in_list)
  {
    bool full_editor_used=m_fullEditorUsed;
    try {
      if (!m_fullEditorUsed) {
	m_filterStringEditor->setText( m_filterSelector->currentText() );
      }

      std::string filter_string = filterString();

      m_connListItemView->filterItems(filter_string);
      if (save_in_list) {
	for (unsigned int filter_i=0; filter_i<static_cast<unsigned int>(m_filterSelector->count()); filter_i++) {
	  if (m_filterSelector->itemText(filter_i)==m_filterStringEditor->toPlainText()) {
	    m_filterSelector->removeItem( filter_i );
	    break;
	  }
	}

	if (m_filterSelector->count()>s_maxNFilter) {
	  assert(s_maxNFilter>0);
	  m_filterSelector->removeItem( m_filterSelector->count()-1);
	}

	m_filterSelector->addItem(m_filterStringEditor->toPlainText(),0);
	if (m_filterSelector->currentIndex()!=0) {
	  m_filterSelector->setCurrentIndex(0);
	}
	//	m_filterSelector->adjustSize();
      }
      emit changedSelection();
    }
    catch (PixCon::LogicParserBase<EvalResult_t>::ParseError &err) {

      //QColor Qt::white("white");
      QColor highlight_color("yellow");

      std::unique_ptr<Highlighter> *ptr=((full_editor_used && m_filterStringEditor->isVisible()) ? &m_highlighterEditor : &m_highlighter);
      if (!(*ptr).get()) {
	(*ptr)=std::make_unique<Highlighter>(*((full_editor_used && m_filterStringEditor->isVisible()) ?
							   static_cast<QWidget *>(m_filterStringEditor)
							   : static_cast<QWidget *>(m_filterSelector)),
							  highlight_color,Qt::white,3*1000);
      }
      else {
	(*ptr)->restart(highlight_color,3*1000);
      }
      std::cerr << err.what() << std::endl;
    }
    m_fullEditorUsed=full_editor_used;
  }

  void ModuleList::primaryAction(QTreeWidgetItem *item, int) {
    ConnListItem *conn_item = dynamic_cast<ConnListItem *>(item);
    if (conn_item) {
      EConnItemType conn_type = conn_item->type();
      //      if (conn_type <= kRodItem) {
      m_contextMenu->primaryAction(conn_type, conn_item->text(0).toLatin1().data());
      //    }
    }
  }

  void ModuleList::contextMenu(const QPoint &pos) {
    QTreeWidgetItem *item = m_moduleList->itemAt(pos);
    if(item==0) // click in empty part, un-select all
      m_moduleList->clearSelection();
    else
      item->setSelected(true);

    ConnListItem *conn_item = dynamic_cast<ConnListItem *>(item);
    if (conn_item) {
      EConnItemType conn_type = conn_item->type();
      m_contextMenu->showMenu(conn_type, conn_item->text(0).toLatin1().data());
    }
    else {
      EConnItemType conn_type = kNConnItemTypes;
      const std::string conn_name("");
      m_contextMenu->showMenu(conn_type, conn_name);
    }
  }

  void ModuleList::mouseButtonClicked (QTreeWidgetItem * item, int) {
      ConnListItem *conn_item = dynamic_cast<ConnListItem *>(item);
      if (conn_item) {
	EConnItemType conn_type = conn_item->type();
	  emit selectModule(conn_type, conn_item->text(0).toLatin1().data());
      }
  }


  class ModuleListSelection : public IConnItemSelection
  {
  public:

    ModuleListSelection(const ConnListItemView &conn_list_item_view, bool selected)
      : m_connListItemView(&conn_list_item_view),
	m_maskNotSelected(!selected)
    { }

    bool isSelected(const std::string &conn_name) const {
      bool selected = m_connListItemView->isSelectedFromList(conn_name);
      bool comb = m_maskNotSelected ^ selected;
      return comb;
    }

  private:
   const ConnListItemView *m_connListItemView;
    bool m_maskNotSelected;
  };

  IConnItemSelection *ModuleList::currentSelection(bool selected, bool deselected){
    if (selected==deselected) {
      return new EntireSelection;
    }
    else {
      return new ModuleListSelection(*m_connListItemView, selected);
    }
  }

  class OnTheFlySelection : public IConnItemSelection
  {
  public:
    OnTheFlySelection(const std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data_manager,
		      EMeasurementType measurement_type,
		      SerialNumber_t serial_number,
		      const std::string &filter_string)
      : m_connItemSelection(calibration_data_manager,
			    std::make_shared<DataSelection>( calibration_data_manager->calibrationDataPtr(), 
										measurement_type,
										serial_number,
										VarDef_t::invalid())),
	m_valid(false)
    {
      try {
        m_connItemSelection.setFilterString(filter_string);
	m_valid=true;
      }  
      catch ( PixCon::LogicParserBase<EvalResult_t>::ParseError &err) {
      }
    }

    bool isSelected(const std::string &conn_name) const { 
      return (m_valid ? static_cast<bool>(m_connItemSelection.isSelected(conn_name)) : true);
    }

  private:
    mutable ConnItemSelection m_connItemSelection;
    bool              m_valid;
  };

  IConnItemSelection *ModuleList::selection(bool selected, bool deselected, EMeasurementType measurement_type, PixCon::SerialNumber_t serial_number) {

    std::string filter_string = filterString();
    if (filter_string.empty() || selected==deselected) {
      return new EntireSelection;
    }
    else {
      return new OnTheFlySelection(m_calibrationData, measurement_type, serial_number, filter_string);
    }
  }

  void ModuleList::showHideFilterEditor() {
    QList<int> sizes( m_splitter->sizes() );
    int first_size=(sizes.size()>0 ? sizes[0] : 0 );
    if (m_filterStringEditor->isVisible()) {
      if(m_filterStringEditor->hasFocus()) {
	m_filterSelector->setFocus();
      }
      m_filterStringEditor->hide();
      m_lastEditorSize=first_size;
      QPalette palette;
      palette.setBrush(m_fullLineEdit->backgroundRole(), QBrush(m_arrowRight));
      m_fullLineEdit->setPalette(palette);
      //m_fullLineEdit->setPixmap( m_arrowRight);
    }
    else {
      int total_size=0;
      for (QList<int>::const_iterator sizes_iter = sizes.begin();
	   sizes_iter != sizes.end();
	   ++sizes_iter) {
	total_size+=*sizes_iter;
      }
      QPalette palette;
      palette.setBrush(m_fullLineEdit->backgroundRole(), QBrush(m_arrowDown));
      m_fullLineEdit->setPalette(palette);
      //m_fullLineEdit->setPixmap( m_arrowDown );
      m_filterStringEditor->show();
      if (m_lastEditorSize==0) {
	m_lastEditorSize=m_filterStringEditor->minimumHeight();
      }
      sizes.clear();
      sizes.push_back(m_lastEditorSize);
      sizes.push_back(total_size-m_lastEditorSize);
      m_splitter->setSizes(sizes);

      m_filterStringEditor->setFocus();
    }
  }


}
