#ifndef _PixCon_ConsoleBusyLoopFactory_h_
#define _PixCon_ConsoleBusyLoopFactory_h_

#include <QTVisualiser/BusyLoopFactory.h>
#include <qobject.h>

class QApplication;

namespace PixCon {

  class ConsoleBusyLoopFactory : public QObject, public PixA::IBusyLoopFactory
  {
    Q_OBJECT

  public:
    ConsoleBusyLoopFactory(QApplication *app);
    void destroy();

    void resetMessage() {
      emit statusMessage("");
    }
  protected:
    PixA::IBusyLoop *busyLoop(const std::string &message,
			      QDialog *dialog,
			      unsigned int dialog_after_time,
			      unsigned int update_time);

  signals:
    void statusMessage(const QString &message);
    
  protected:
    ~ConsoleBusyLoopFactory();

  public:
    QApplication *m_app;
  };

}
#endif
