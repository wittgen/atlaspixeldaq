//
// Parser for logic expressions like: "(!a&(peter)|!(c&d))"
// 
// First the string is parsed and its logical structure is stored into a bineary tree structure.
// This can then be used to evaluate quickly many assigned boolean patterns without having to parse again.
//
// Claus Horn 2007

#ifndef CLOGICPARSER
#define CLOGICPARSER

#include <string>
#include <vector>
#include <iostream>
#include <stdexcept>

#include "IEvaluator.h"

namespace PixCon {
  class Tokeniser;

  template <class T_Bool_t>
  class LogicParserBase
  {
  public:

    class ParseError : public std::runtime_error
    {
    protected:
      ParseError(const std::string &message) : std::runtime_error(message) {}
    };

    class OperandMismatch : public ParseError
    {
    public:
      OperandMismatch(const std::string &message) : ParseError(message) {}
    };

    class MissingOperand : public ParseError
    {
    public:
      MissingOperand(const std::string &message) : ParseError(message) {}
    };

    class MissingOperator : public ParseError
    {
    public:
      MissingOperator(const std::string &message) : ParseError(message) {}
    };

    class UnhandledToken : public ParseError
    {
    public:
      UnhandledToken(const std::string &message) : ParseError(message) {}
    };

    enum ELogicOperators {kAnd, kOr, kNot, kNLogicOperators};

    LogicParserBase() : m_head(NULL) { };

    virtual ~LogicParserBase() {
      delete m_head;
    };

    void compileExpression(PixCon::Tokeniser &tokeniser);

    T_Bool_t evaluate();

  protected:

    class Negator : public IEvaluator
    {
    public:
      Negator(IEvaluator *evaluator) : m_evaluator(evaluator) {}
      ~Negator() { delete m_evaluator; }

      T_Bool_t eval() const { return !m_evaluator->eval(); };

      IEvaluator *releaseEvaluator() { IEvaluator *evaluator = m_evaluator; m_evaluator=NULL; return evaluator;}

      static IEvaluator *negate( IEvaluator *evaluator) {
	Negator *child = dynamic_cast<Negator *>(evaluator);
	if (child) {
	  IEvaluator *child_evaluator = child->releaseEvaluator();
	  delete evaluator;
	  return child_evaluator;
	}
	else {
	  return new Negator(evaluator);
	}
      }
    private:
      IEvaluator *m_evaluator;
    };


    class BinaryOperator : public IEvaluator
    {
    public:
      BinaryOperator() { m_input[0]=NULL; m_input[1]=NULL; }
      ~BinaryOperator() { delete m_input[0]; delete m_input[1]; }

      IEvaluator *&first()              { return m_input[0]; }
      IEvaluator *&second()             { return m_input[1]; }

      const IEvaluator *first()  const  { return m_input[0]; }
      const IEvaluator *second() const  { return m_input[1]; }

      void setFirst(IEvaluator *evaluator) {m_input[0]=evaluator;}
      void setSecond(IEvaluator *evaluator) {m_input[1]=evaluator;}
    private:
      IEvaluator *m_input[2];
    };

    class LogicalAnd : public BinaryOperator {
    public:
      T_Bool_t eval() const { return this->first()->eval() && this->second()->eval(); }
    };

    class LogicalOr : public BinaryOperator {
    public:
      T_Bool_t eval() const { return this->first()->eval() || this->second()->eval(); }
    };

    void ParseExpression(PixCon::Tokeniser &tokeniser, IEvaluator*&); // example: a&b | c 
    void ParseTerm(PixCon::Tokeniser &tokeniser, IEvaluator*&);       // example: a&b

    /** Parse a factor e.g. variables, expressions encapsulated in parenthesis, negations, expressions which result in a boolean
     * @param tokeniser class which splits the string into tokens.
     * @param node pointer to the location which stores the pointer of the node
     * @param strong_expression_only only allow expressions which have contain operators with higher precedence than !
     */
    void ParseFactor(PixCon::Tokeniser &tokeniser, IEvaluator*&node_ptr,bool strong_expression_only);     // example: a ; b ; (a|b) ; !a

    virtual void ParseNucleus(PixCon::Tokeniser &tokeniser, IEvaluator*&node_ptr, bool strong_expression_only) = 0;

    bool negation() const { return m_negation;}
    void toggleNegation() { m_negation=!m_negation;}
    void resetNegation()  { m_negation=false;}

  private:

    IEvaluator *m_head;
    bool m_negation; // handel expresions like !(a&b) -> !a|!b

  };

}

#endif
