#ifndef _ActiveExtractor_H_
#define _ActiveExtractor_H_

#include "QIsInfoEvent.h"

namespace PixCon {

  class ActiveExtractor : public IVarNameExtractor 
  {
  public:

    void extractVar(const std::string &full_is_name, ConnVar_t &conn_var) const
    {
      std::string::size_type conn_end = full_is_name.rfind("/");
      if (conn_end != std::string::npos && conn_end>0) {
	std::string::size_type conn_start = full_is_name.rfind("/", conn_end-1);
	if (conn_start == std::string::npos) {
	  conn_start = full_is_name.rfind(".");
	}
	if (conn_start != std::string::npos) {
	  conn_var.connName()=std::string(full_is_name,conn_start+1,conn_end-conn_start-1);
	  conn_var.varName()=""; /* Var name of QIsEvent set to "" means all variables */
	}
      }

    }

  };
}
#endif
