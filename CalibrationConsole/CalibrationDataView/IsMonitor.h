#ifndef _PixCon_IsMonitor_h_
#define _PixCon_IsMonitor_h_

//#include <string>
//#include <memory>
//#include <is/criteria.h>

//#include <ipc/partition.h>

//#include "omnithread.h"
//#include "Flag.hh"

//#include <unistd.h>
//#include <time.h>


//class ISInfoReceiver;
//class IPCPartition;
//class ISCallbackInfo;

// class QObject;

////#include "QIsInfoEvent.h"
////#include <qapplication.h>

#include "IsMonitorReceiver.h"
#include <is/infoT.h>
#include <Lock.h>

namespace PixCon {

  template <class Src_t, class SrcRef_t>
  class SimpleIsMonitorReceiver : public IsMonitorReceiver<ISInfoT<Src_t>, Src_t, SrcRef_t>
  {
  public:
    SimpleIsMonitorReceiver(std::shared_ptr<IsReceptor> receptor,
		      EValueConnType conn_type,
                      SrcRef_t value_after_deletion,
		      const std::string &is_regex,
		      unsigned int buffer_size,
		      unsigned int max_time_between_updates=0)
      : IsMonitorReceiver<ISInfoT<Src_t>, Src_t,SrcRef_t>(receptor,conn_type, value_after_deletion, ISInfoT<Src_t>().type() && is_regex, buffer_size, max_time_between_updates)
    {}

    /** Create an IsMonitorReceiver with default buffer size which is different for module and ROD values. 
     * The reconnect feature if no updates occure within a certain time will be disabled.
     */
    SimpleIsMonitorReceiver(std::shared_ptr<IsReceptor> receptor,
			    EValueConnType conn_type,
                            SrcRef_t value_after_deletion,
			    const std::string &is_regex)
      : IsMonitorReceiver<ISInfoT<Src_t>, Src_t,SrcRef_t>(receptor,conn_type,  value_after_deletion, ISInfoT<Src_t>().type() && is_regex)
    {}

    void _update();
    void valueChanged(ISCallbackInfo *info);

  private:
    Mutex                         m_monitorLock;

  };

  template <class Src_t, class SrcRef_t, class Dest_t>
  class SimpleIsMonitor : public IsMonitor<ISInfoT<Src_t>, Src_t, SrcRef_t, Dest_t>
  {
  public:
    /** Create an IsMonitor with input output value conversion, with default buffer size and without a maximum tolerated time between updates.*/
    SimpleIsMonitor(std::shared_ptr<IsReceptor> &receptor,
		    EValueConnType conn_type,
                    SrcRef_t value_after_deletion,
		    const IVarNameExtractor &extractor,
		    const std::string &is_regex)
      : IsMonitor<ISInfoT<Src_t>, Src_t,SrcRef_t,Dest_t>(receptor,conn_type, value_after_deletion, extractor,   ISInfoT<Src_t>().type() && is_regex)
    {}

    /** Create an IsMonitor with input output value conversion.*/
    SimpleIsMonitor(std::shared_ptr<IsReceptor> &receptor,
		    EValueConnType conn_type,
                    SrcRef_t value_after_deletion,
		    const IVarNameExtractor &extractor,
		    const std::string &is_regex,
		    unsigned int buffer_size,
		    unsigned int max_time_between_updates=0)
      : IsMonitor<ISInfoT<Src_t>, Src_t,SrcRef_t, Dest_t>(receptor,conn_type, value_after_deletion, extractor, ISInfoT<Src_t>().type() && is_regex, buffer_size, max_time_between_updates)
    {}

    void _update();
    void valueChanged(ISCallbackInfo *info);

  private:
    Mutex                         m_monitorLock;

  };


  typedef SimpleIsMonitorReceiver<std::string,const std::string&>         IsStringReceiver;

  typedef SimpleIsMonitor<unsigned short, unsigned short, unsigned short> IsStateMonitor;
  typedef SimpleIsMonitor<int, int, unsigned short>                       IsIntStateMonitor;

  typedef SimpleIsMonitor<double, double, float>                          IsDoubleValueMonitor;
  typedef SimpleIsMonitor<float, float, float>                            IsValueMonitor;
  typedef SimpleIsMonitor<int, int, float>                                IsIntValueMonitor;
  typedef SimpleIsMonitor<unsigned int, unsigned int, float>              IsUIntValueMonitor;

}
#endif
