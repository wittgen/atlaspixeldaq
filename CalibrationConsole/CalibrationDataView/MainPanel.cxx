#include "MainPanel.h"

#include "DbTagsPanel.h"
#include <qstatusbar.h>
#include <QIsInfoEvent.h>
#include <StatusChangeEvent.h>
#include "DetectorView.h"
#include "CoupledDetectorView.h"
#include "ModuleList.h"
#include "MetaDataList.h"

#include "TemporaryCursorChange.h"

#include <PixConnectivity/PixConnectivity.h>
#include <QMessageBox>
#include <QShortcut>

#include "guessConnItemType.h"

#include "LogWindow.h"

// getpid, gethostname

#include "ConsoleBusyLoopFactory.h"
//#include <QTVisualiser/BusyLoopFactory.h>

#include "ModuleMask.h"

namespace PixCon {

  MainPanelSelectionFactory::~MainPanelSelectionFactory() {}

  MainPanel::MainPanel(std::shared_ptr<PixCon::UserMode>& user_mode,
               const std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data,
               const std::shared_ptr<PixCon::CalibrationDataPalette> &palette,
               const std::shared_ptr<PixCon::CategoryList> &categories,
               const std::shared_ptr<PixCon::IContextMenu> &context_menu,
               const std::shared_ptr<PixCon::IMetaDataUpdateHelper> &update_helper,
	       const std::shared_ptr<PixDet::ILogoFactory> &logo_factory,
               QApplication &application,
               QWidget* parent)
: QMainWindow(parent),
      m_userMode(user_mode),
      m_app(&application),
      m_detectorViewList(calibration_data, palette,categories, context_menu, logo_factory,true),
      m_coupledDetectorView(NULL),
      m_moduleList(NULL),
      m_logWindow(NULL),
      m_busyLoopFactory(new ConsoleBusyLoopFactory(&application) ),
      m_categories(categories),
      m_updateVarListTimer(new QTimer(this)),
      m_slowUpdateTimer(new QTimer(this))
  {
    setupUi(this);
    //unsigned int user_index = 0;
    m_shifterAction = User->addAction("Shifter", this, SLOT( setUserModeShifter() ));//, 0, kUserShifter, user_index++);
    m_expertAction = User->addAction("Expert", this, SLOT( setUserModeExpert() ));//, 0, kUserExpert, user_index++);
    setUserMode(m_userMode->getUserMode());

    //    MenuBar->setItemVisible(3, false);
    Utils->menuAction()->setVisible(false);

    //unsigned int file_index = 0;
    m_sepExitAction = File->addSeparator();//file_index++);
    File->addAction("E&xit", this, SLOT( close() ));//, 0, kFileExit, file_index++);

    QVBoxLayout *a_layout = new QVBoxLayout();
    a_layout->setAlignment( Qt::AlignBottom );
    leftPanelLayout->addLayout(a_layout);

    DetectorView *detector_view = m_detectorViewList.createNewView(*m_app,mainWindowLeftPanel);//,"MainDetectorView");
    a_layout->addWidget( detector_view);

    delete tab;
    tab=NULL;
    m_metaDataList=new MetaDataList(calibration_data, palette, categories, createSelectionFactory(), update_helper, this);
    mainWindowRightPanel->addTab(m_metaDataList,"&Data List");


    m_coupledDetectorView = dynamic_cast<CoupledDetectorView *>( detector_view );
    if (m_coupledDetectorView) {
      m_moduleList=new ModuleList(calibration_data,
				  detector_view->dataSelectionPtr(),
				  detector_view->contextMenu(),
				  NULL);
      m_coupledDetectorView->setModuleList(m_moduleList);

      connect( detector_view, SIGNAL( changedDataSelection() ), m_moduleList, SLOT( changedVariable() ) );
      mainWindowRightPanel->addTab(m_moduleList,"&Module List");
    }

    ModuleMask *module_mask = new ModuleMask(calibration_data, palette, m_app, m_moduleList, this,this);
    if (module_mask) {
      mainWindowRightPanel->addTab(module_mask,"Module Mas&k");

      connect(module_mask, SIGNAL( changeVariable(const QString&, const QString&) ),
	      m_coupledDetectorView, SLOT( changedVariable(const QString&, const QString&)) );

      connect(module_mask, SIGNAL( selectSerialNumber(PixCon::EMeasurementType, PixCon::SerialNumber_t) ),
	      m_coupledDetectorView, SLOT(changeDataSelection(PixCon::EMeasurementType, PixCon::SerialNumber_t)) );
    }


    connect(m_metaDataList, SIGNAL( changeDataSelection(PixCon::EMeasurementType, PixCon::SerialNumber_t) ),
	    m_coupledDetectorView, SLOT(changeDataSelection(PixCon::EMeasurementType, PixCon::SerialNumber_t)) );

    connect(m_metaDataList, SIGNAL( removeMeasurement(PixCon::EMeasurementType, PixCon::SerialNumber_t) ),
	    this, SLOT( removeMeasurement(PixCon::EMeasurementType, PixCon::SerialNumber_t)) );

    connect(m_metaDataList, SIGNAL( changeVariable(const QString&, const QString&) ),
	    m_coupledDetectorView, SLOT( changedVariable(const QString&, const QString&)) );

    connect(m_metaDataList, SIGNAL( showVariableContextMenu() ), m_coupledDetectorView, SLOT( showAxisRangeDialog()) );

    connect(m_metaDataList, SIGNAL( showScanConfiguration(PixCon::EMeasurementType , PixCon::SerialNumber_t) ),
	    this, SLOT( showScanConfiguration(PixCon::EMeasurementType , PixCon::SerialNumber_t)) );

    connect(m_metaDataList, SIGNAL( showAnalysisConfiguration(PixCon::EMeasurementType , PixCon::SerialNumber_t, unsigned short) ),
	    this, SLOT( showAnalysisConfiguration(PixCon::EMeasurementType , PixCon::SerialNumber_t, unsigned short)) );

    connect(m_metaDataList, SIGNAL( addMeasurement() ), this, SLOT( addMeasurement() ));

    m_detectorViewList.setSerialNumbers();

    connect(m_updateVarListTimer,SIGNAL( timeout() ),this,SLOT( updateVariables() ) );
    connect(m_slowUpdateTimer,SIGNAL( timeout() ),this,SLOT( processSlowUpdates() ) );

   // detector_view->m_listViewButton->hide();

    connect(detector_view, SIGNAL( statusMessage(const QString &) ), this->statusBar(), SLOT( showMessage(const QString &) ) );
    if (m_busyLoopFactory) {
      connect(m_busyLoopFactory, SIGNAL( statusMessage(const QString &) ), this->statusBar(), SLOT( showMessage(const QString &) ) );
    }
/*
    QList<int> splitter_sizes;
    auto totalWidth = mainPanelSplitter->width();
    auto detViewWidth = static_cast<int>(totalWidth*0.65);
    splitter_sizes.push_back(detViewWidth);
    splitter_sizes.push_back(totalWidth-detViewWidth);
    //    splitter_sizes.push_back(m_mainPanelSplitter.width());
    //    splitter_sizes.push_back(0);
    mainPanelSplitter->setSizes(splitter_sizes);
*/
    //    m_mainPanelTabs->hide();
    delete logFrameTable;

    std::string log_file_name;
    const char *log_dir = getenv("PIX_LOGS");
    if (log_dir) {
      log_file_name=log_dir;
      if (!log_file_name.empty()) {
	if (log_file_name[log_file_name.size()-1]!='/') {
	  log_file_name+='/';
	}
	char temp[1024];
	gethostname(temp,1024);

	std::stringstream name_ext;
	name_ext << temp
		 << '_'
	         << getpid()
		 << '_'
		 << ::time(NULL);
// 	if (name && strlen(name)) {
// 	  log_file_name += name;
// 	}
// 	else {
	  log_file_name += "CalibrationConsole";
// 	}
	log_file_name += '_';
	// remove log files older then 5 days
	LogWindow::removeOldLogFiles(log_file_name+"*.log",5);

	log_file_name += name_ext.str();
	log_file_name += ".log";
      }
    }
    if (true) {
    m_logWindow=new LogWindow( 1000, log_file_name, m_app, logFrame );
 logFrameLayout->addWidget( m_logWindow );

    connect(m_logWindow, SIGNAL( hiddenMessage(const QString&) ), statusBar() , SLOT (showMessage(const QString&)) );

    {
      QShortcut *up = new QShortcut(QKeySequence("Ctrl+W"), this );
      connect(up, SIGNAL(activated()),this, SLOT(cycleLogWindowSizeUp()));

      QShortcut *down = new QShortcut(QKeySequence("Ctrl+W+Shift"), this);
      connect(down, SIGNAL(activated()),this, SLOT(cycleLogWindowSizeDown()));
    }
    }
//     QValueList<int> splitter_sizes = m_messageBoxSplitter->sizes();
//     if (splitter_sizes.size()==2) {
//       splitter_sizes[0]+=splitter_sizes[1];
//       splitter_sizes[1]=0;
//     }
//     m_messageBoxSplitter->setSizes(splitter_sizes);
    //    m_mainPanelSplitter.setSizes();

    detector_view->show();
  }

  MainPanel::~MainPanel() 
  {
    if (m_busyLoopFactory) {
      m_busyLoopFactory->destroy();
      m_busyLoopFactory=NULL;
    }
  }


  void MainPanel::selectConnectivityTags()
  {

    PixA::ConnectivityRef conn(m_detectorViewList.calibrationDataManager()->currentConnectivity());
    std::string current_id_tag;
    std::string current_conn_tag;
    std::string current_cfg_tag;
    std::string current_mod_cfg_tag;

    if (conn) {
      current_id_tag = conn.tag(PixA::IConnectivity::kId);
      current_conn_tag = conn.tag(PixA::IConnectivity::kConn);
      current_cfg_tag = conn.tag(PixA::IConnectivity::kConfig);
      current_mod_cfg_tag = conn.tag(PixA::IConnectivity::kModConfig);
    }

    DbTagsPanel tags_chooser(current_id_tag, current_conn_tag, current_cfg_tag, current_mod_cfg_tag,this);
    try {

      if (tags_chooser.exec() == QDialog::Accepted){
	TemporaryCursorChange cursor_change;

	m_detectorViewList.changeConnectivity(tags_chooser.getObjectTag(),
					      tags_chooser.getConnectivityTag(),
					      tags_chooser.getConnectivityTag(),
					      tags_chooser.getConnectivityTag(),
					      tags_chooser.getConfigurationTag(),
					      tags_chooser.getModuleConfigurationTag(),
					      0 /* == most recent revision of the configurations */);
	connectivityChanged(kCurrent, 0);
      }
    }
    catch(PixA::ConfigException &err) {
      // use MRS ?
      std::stringstream msg;
      msg << "Caught exception while loading connectivity : "
	  << err;
      statusBar()->showMessage(msg.str().c_str());
    }
    catch(PixLib::PixConnectivityExc &err) {
      // use MRS ?
      std::stringstream msg;
      msg << "Caught exception while loading connectivity : "
	  << tags_chooser.getConnectivityTag() << ", "
	  << tags_chooser.getConfigurationTag() << ", "
	  << tags_chooser.getModuleConfigurationTag() 
	  << ". Exception : "
	  << err;
      statusBar()->showMessage(msg.str().c_str());
      return;
    }
    return;
  }

  void MainPanel::connectivityChanged(PixCon::EMeasurementType /*measurement_type*/, PixCon::SerialNumber_t /*serial_number*/) 
  { /*intentionally does nothing */ }


  void MainPanel::addSerialNumber(EMeasurementType measurement_type, SerialNumber_t serial_number) {
    m_detectorViewList.addSerialNumber(measurement_type, serial_number);
    m_metaDataList->addMetaData(measurement_type, serial_number);
  }

  void MainPanel::removeMeasurement(EMeasurementType measurement_type, SerialNumber_t serial_number) {
    m_detectorViewList.removeMeasurement(measurement_type, serial_number);
  }

  bool MainPanel::event(QEvent *an_event)
  {
    // @todo event processing should be done by the "parent" of all detector
    //       views and all views which show the right thing should be updated

	//std::cout << "INFO [MainPanel::event] Testoutput in MainPanel function " << std::endl;
    if (an_event && an_event->type() == QEvent::User) {
      PixCon::QIsInfoEvent * info_event  = dynamic_cast< PixCon::QIsInfoEvent *>(an_event);
      if (info_event) {
	if (info_event->valid()) {
	  PixCon::EConnItemType conn_item_type=PixCon::guessConnItemType(info_event->connName());
	  VarId_t var_id=VarDefBase_t::invalid().id();

	  if (!info_event->varName().empty()) {
	    m_detectorViewList.updateConnItem( conn_item_type,
					       info_event->connName(),
					       info_event->measurementType(),
					       info_event->serialNumber(),
					       info_event->varName(),
					       info_event->propagateToChilds());
	  try {
	      VarDef_t var_def( VarDefList::instance()->getVarDef(info_event->varName()) );
	      var_id=var_def.id();
	    }
	    catch(...) {
	    }
	  }
	  else {
	    m_detectorViewList.updateConnItem( conn_item_type,
					       info_event->connName(),
					       info_event->measurementType(),
					       info_event->serialNumber(),
					       info_event->propagateToChilds());
	  }

	  if (!m_slowUpdateTimer->isActive()) {
	  if (m_detectorViewList.checkSlowUpdates(info_event->measurementType(),
						  info_event->serialNumber(),
						  var_id,
						  info_event->connName())) {
	    ::time_t current_time = ::time(0);
	    unsigned int delay = s_minUpdatePeriode + m_detectorViewList.nSlowUpdateListener() * s_extraDelayPerListener;

	    // limit the update frequency to s_minUpdatePeriode + ...
	    // but do not wait unecessarily long
	    if (current_time - m_lastSlowUpdate > static_cast< ::time_t >(delay) ) {
	      delay = s_minDelay;
	    }
	    m_slowUpdateTimer->setSingleShot(true);
	    m_slowUpdateTimer->start((delay > 0) ? delay : 0);
	  }
	  }

	  return true;
	}
      }
      else {
	PixCon::StatusChangeEvent *status_change_event  = dynamic_cast< PixCon::StatusChangeEvent *>(an_event);
	if (status_change_event) {
	  if (!m_metaDataList->hasMetaData(status_change_event->measurement(), status_change_event->serialNumber())) {
	    m_metaDataList->addMetaData(status_change_event->measurement(), status_change_event->serialNumber());
	    m_detectorViewList.addSerialNumber(status_change_event->measurement(), status_change_event->serialNumber());
	    emit newMeasurement(status_change_event->measurement(), status_change_event->serialNumber() );
	  }
	  else {
	    m_metaDataList->updateMetaData(status_change_event->measurement(), status_change_event->serialNumber());
	  }

	  return true;
	}
	else {
	  // @todo event processing should be done by the "parent" of all detector
	  //       views and all views which show the right thing should be updated

	  PixCon::QNewVarEvent *new_var_event  = dynamic_cast< PixCon::QNewVarEvent *>(an_event);
	  if (new_var_event) {
	    m_updateVariables.push_back(new_var_event->varData());
	    if (!m_updateVarListTimer->isActive()) {
	      m_updateVarListTimer->setSingleShot(true);
	      m_updateVarListTimer->start((s_updateTime > 0) ? s_updateTime : 0);
	    }
	    return true;
	  }

	}
      }
    }
   QMainWindow::event(an_event);// MainPanelBase::event(an_event);
    return false;
  }

  void MainPanel::closeEvent(QCloseEvent * event )
  {
    if (m_detectorViewList.nViews()>1) {
      std::stringstream message;
      message << "There are " << m_detectorViewList.nViews() << " views open." << std::endl
	      << "Do You want to close all of them  and quit the application ? ";

      QMessageBox mb( "Closing the Calibration Console",
		      message.str().c_str(),
		      QMessageBox::Warning,
		      QMessageBox::Cancel  | QMessageBox::Escape,
		      QMessageBox::Ok | QMessageBox::Default,QMessageBox::NoButton);
      if ( mb.exec() != QMessageBox::Ok ) return;
    }
    {
      std::unique_ptr<PixA::IBusyLoop> busy_loop(PixA::BusyLoopFactory::busyLoop(" Shutdown all background tasks ..."));
      shutdown();
    }
    {
      m_detectorViewList.destroyAllButThis(this);
      hide();
      if (m_logWindow) {
	std::unique_ptr<PixA::IBusyLoop> busy_loop(PixA::BusyLoopFactory::busyLoop(" Shutdown logging task  ..."));
	m_logWindow->shutdown();
      }
    }
 QMainWindow::closeEvent(event); //  MainPanelBase::closeEvent(event);
    std::cout << "INFO [MainPanel::closeEvent] will close all windows. " << std::endl;
    application()->closeAllWindows();
    std::cout << "INFO [MainPanel::closeEvent] closed all windows. " << std::endl;
  }

  void MainPanel::updateVariables()
  {
    for(  std::list<NewVar_t>::const_iterator var_iter = m_updateVariables.begin();
	  var_iter != m_updateVariables.end();
	  var_iter++) {
      bool category_exists = m_categories->exists( var_iter->categoryName() );
      PixCon::CategoryList::Category a_category = m_categories->addCategory( var_iter->categoryName() );
      if ( std::find(a_category.begin(), a_category.end(), var_iter->varName()) == a_category.end() ) {
	a_category.addVariable( var_iter->varName() );
      }
      if (!category_exists) {
	updateCategories(m_categories);
      }
      updateCategory(var_iter->measurementType(), var_iter->serialNumber(), var_iter->categoryName().c_str() );

    }
    m_updateVariables.clear();
  }


  void MainPanel::setMetaData()
  {

    const ScanMetaDataCatalogue &scan_catalogue=m_detectorViewList.calibrationDataManager()->scanMetaDataCatalogue();
    for (std::map<SerialNumber_t, ScanMetaData_t>::const_iterator scan_iter = scan_catalogue.begin();
	 scan_iter != scan_catalogue.end();
	 scan_iter++) {

      m_metaDataList->addMetaData(kScan, scan_iter->first);

      for (std::vector<SerialNumber_t>::const_iterator analysis_iter = scan_iter->second.beginAnalysis();
	   analysis_iter !=  scan_iter->second.endAnalysis();
	   analysis_iter++) {
	m_metaDataList->addMetaData(kAnalysis,*analysis_iter);
      }

    }

  }

  void MainPanel::selectSerialNumber(PixCon::EMeasurementType measurement_type, PixCon::SerialNumber_t serial_number) {
    m_coupledDetectorView->selectSerialNumber(measurement_type, serial_number);
  }

  std::pair<PixCon::EMeasurementType, PixCon::SerialNumber_t> MainPanel::selectedMeasurement() {
    std::pair<PixCon::EMeasurementType, PixCon::SerialNumber_t> selected_measurement ;
    selected_measurement.first = m_coupledDetectorView->dataSelectionPtr()->type();
    selected_measurement.second = m_coupledDetectorView->dataSelectionPtr()->serialNumber();
    return selected_measurement;
  }

  IConnItemSelection *MainPanel::currentSelection(bool selected, bool deselected) {
    assert(m_coupledDetectorView);
    return m_coupledDetectorView->currentSelection(selected, deselected);
  }

  IConnItemSelection *MainPanel::selection(bool selected, bool deselected, EMeasurementType measurement_type, PixCon::SerialNumber_t serial_number) {
    assert(m_coupledDetectorView);
    return m_coupledDetectorView->selection(selected, deselected, measurement_type, serial_number);
  }


  void MainPanel::cycleLogWindowSizeUp() {
    cycleLogWindowSize(true);
  }

  void MainPanel::cycleLogWindowSizeDown() {
    cycleLogWindowSize(false);
  }

  void MainPanel::cycleLogWindowSize(bool up) {
    QList<int> splitter_sizes = logWindowSplitter->sizes();
    if (splitter_sizes.size()>=2) {
      if (splitter_sizes[0]==0) {
	if (up) {
	  splitter_sizes[0]=splitter_sizes[1];
	  splitter_sizes[1]=0;
	}
	else {
	  splitter_sizes[0]=splitter_sizes[1]*4/5;
	  splitter_sizes[1]-= splitter_sizes[1]*4/5;
	}
      }
      else if ( splitter_sizes[1]==0) {
	if (up) {
	  splitter_sizes[1]=splitter_sizes[0]/5;
	  splitter_sizes[0]-= splitter_sizes[0]/5;
	}
	else {
	  splitter_sizes[1]=splitter_sizes[0];
	  splitter_sizes[0]=0;
	}
      }
      else {
	if (up) {
	  splitter_sizes[1]=splitter_sizes[0] + splitter_sizes[1];
	  splitter_sizes[0]=0;
	}
	else {
	  splitter_sizes[0]=splitter_sizes[0] + splitter_sizes[1];
	  splitter_sizes[1]=0;
	}
      }
      logWindowSplitter->setSizes(splitter_sizes);
    }
  }

  void MainPanel::hideLogWindow() {
    QList<int> splitter_sizes = logWindowSplitter->sizes();
    if (splitter_sizes.size()>=2) {
      if (splitter_sizes[1]!=0) {
	splitter_sizes[0]=splitter_sizes[0] + splitter_sizes[1];
	splitter_sizes[1]=0;
    logWindowSplitter->setSizes(splitter_sizes);
      }
    }
  }

  void MainPanel::clearFilterSelector() {
    m_moduleList->clearFilterSelector();
  }

  void MainPanel::addItemToFilterSelector(const std::string& filter_string) {
    m_moduleList->addItemToFilterSelector(filter_string.c_str());
  }

  void MainPanel::hideSerialNumberSelector() {
    m_detectorViewList.hideSerialNumberSelector();
  }

  void MainPanel::registerListener(EMeasurementType measurement, SerialNumber_t serial_number, const std::shared_ptr<ICalibrationDataListener> &listener) {
    m_detectorViewList.registerListener(measurement, serial_number, listener);
  }

  void MainPanel::deregisterListener(const std::shared_ptr<ICalibrationDataListener> &listener) {
    m_detectorViewList.deregisterListener(listener);
  }

  void MainPanel::processSlowUpdates() {
    m_lastSlowUpdate=::time(0);
    m_detectorViewList.processSlowUpdates();
  }

  void MainPanel::showModuleListView() {
    m_moduleListView = new ModuleListView(m_detectorViewList.calibrationDataManager(),
					  m_coupledDetectorView->dataSelectionPtr(), currentSelection(true, false), this);
    m_moduleListView->show();
  }

  void MainPanel::setUserMode(PixCon::UserMode::EUserMode new_user) {
    QIcon icon;
    QIcon icon2;
    icon.addFile(QString::fromUtf8(":/icons/images/expert.png"), QSize(), QIcon::Normal, QIcon::Off);
    icon2.addFile(QString::fromUtf8(":/icons/images/shifter.png"), QSize(), QIcon::Normal, QIcon::Off);

    if (new_user == PixCon::UserMode::kExpert) {
//       User->setItemEnabled(kUserShifter, true);
//       User->setItemEnabled(kUserExpert, false);
      m_shifterAction->setEnabled(true);
      m_expertAction->setEnabled(false);
      
      User->setIcon(icon);

    }
    else {
//       User->setItemEnabled(kUserShifter, false);
//       User->setItemEnabled(kUserExpert, true);
      m_shifterAction->setEnabled(false);
      m_expertAction->setEnabled(true);
      User->setIcon(icon2);
 
    }
    if (new_user != m_userMode->getUserMode()) {
      m_userMode->setUserMode(new_user);
      emit userModeChanged(new_user); 
    }
  }

  void MainPanel::setUserModeShifter() {
    setUserMode(PixCon::UserMode::kShifter);
  }

  void MainPanel::setUserModeExpert() {
    setUserMode(PixCon::UserMode::kExpert);
  }
}
