#include "ValueHistoryView.h"
#include "MyQRootCanvas.h"
#include <TCanvas.h>
#include <TGraph.h>
#include <TLegend.h>
#include <TAxis.h>
#include <qstatusbar.h>
//Added by qt3to4:
#include "ISelection.h"
#include <DataContainer/Average_t.h>
#include <ConfigWrapper/Connectivity.h>
#include <ConfigWrapper/ConnectivityUtil.h>
#include <CalibrationDataManager.h>
#include "SlowUpdateList.h"
#include <guessConnItemType.h>

namespace PixCon {

  const unsigned int s_maxGraphs = 10;

  template <class T> TGraph *createGraph(TGraph *old_graph, VarDef_t var_info, const ValueHistory<T> &history);

  template <class T>
  TGraph *createGraph(TGraph *old_graph, VarDef_t var_info, const ValueHistory<T> &history) {
    TGraph *a_graph=old_graph;
    if (a_graph) {
      a_graph->Set(0);
      a_graph->Set(var_info.historySize());
    }
    else {
      a_graph=new TGraph(var_info.historySize());
    }

    typename std::vector< PixCon::TimedValue<T> >::const_iterator history_iter = history.begin();
    typename std::vector< PixCon::TimedValue<T> >::const_iterator history_end = history.end();
    unsigned int point_i=0;
    do {
      if (history.isValid(*history_iter)) {
	a_graph->SetPoint(point_i,history_iter->time(0),history_iter->value());
      }
      point_i++;
    } while ( (history_iter=history.const_increment(history_iter)) != history_end);
    a_graph->Set(point_i);
    if (a_graph->GetXaxis()) {
      a_graph->GetXaxis()->SetTimeDisplay(1);
      a_graph->GetXaxis()->SetTimeFormat("%H:%M");
      a_graph->GetXaxis()->SetTimeOffset(history.base());
    }

    return a_graph;
  }


  class FullSelection : public ISelection
  {
  public:
    bool isSelected(const std::string &/*name*/) const { return true;}
  };


  TGraph *ValueHistoryView::createGraph(TGraph *old_graph, 
					const PixCon::CalibrationData::ConnObjDataList &data_list,
					EMeasurementType measurement_type,
					SerialNumber_t serial_number,
					VarDef_t var_def,
					const std::string &conn_name)
  {
    TGraph *a_graph =NULL;
    try {
      switch (var_def.valueType()) {
      case kTimedValue: {
	a_graph = PixCon::createGraph<float>(old_graph, var_def, data_list.valueHistory<float>(measurement_type, serial_number, var_def.id()) );
	break;
      }
      case kTimedState: {
	a_graph = PixCon::createGraph<PixCon::State_t>(old_graph, var_def, data_list.valueHistory<PixCon::State_t>(measurement_type, serial_number, var_def.id()) );
	break;
      }
      case kValue:
      case kState:
      case kNValueTypes:
	break;
      }
    }
    catch(...) {
    }

    if (a_graph && a_graph->GetYaxis() && var_def.isStateWithHistory() ) {
      const StateVarDef_t &state_var_def = var_def.stateDef();
      a_graph->GetYaxis()->Set( state_var_def.nStates(),-.5, state_var_def.nStates()-.5);
      for (unsigned int state_i=0; state_i<state_var_def.nStates(); ++state_i) {
	a_graph->GetYaxis()->SetBinLabel( state_i, state_var_def.stateName(state_i).c_str() );
      }
    }

    if (a_graph) {
      if (a_graph->GetYaxis()) {
	a_graph->GetYaxis()->SetTitle(VarDefList::instance()->varName(var_def.id()).c_str() );
      }
      a_graph->SetTitle(conn_name.c_str());
      a_graph->SetName(conn_name.c_str());
    }

    return a_graph;
  }

  void ValueHistoryView::init( EMeasurementType measurement_type,
			       SerialNumber_t serial_number,
			       VarDef_t var_def,
			       bool enabled_only)
  {
    if (!var_def.hasHistory()) return /*NULL*/;

    VarId_t var_id = var_def.id();

    if (!PixCon::VarDefList::instance()->isValid(var_id)) return/* NULL*/;

    Lock lock(m_calibrationData->calibrationDataMutex());
    PixA::ConnectivityRef connectivity( m_calibrationData->connectivity(measurement_type,
									serial_number) );
    if (connectivity) {

      try {
	for (PixA::CrateList::const_iterator crate_iter = connectivity.crates().begin();
	     crate_iter != connectivity.crates().end();
	     ++crate_iter) {

	  PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
	  PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
	  for (PixA::RodList::const_iterator rod_iter = rod_begin;
	       rod_iter != rod_end;
	       ++rod_iter) {

	    if (var_def.connType()==kRodValue || var_def.connType()==kAllConnTypes) {
	      if (!addConnObj( PixA::connectivityName(rod_iter), enabled_only )) break;
	    }

	    if (var_def.connType()==kModuleValue || var_def.connType()==kAllConnTypes) {

	      PixA::Pp0List::const_iterator pp0_begin = PixA::pp0Begin(rod_iter);
	      PixA::Pp0List::const_iterator pp0_end = PixA::pp0End(rod_iter);
	      for (PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
		   pp0_iter != pp0_end;
		   ++pp0_iter) {

		PixA::ModuleList::const_iterator module_begin = PixA::modulesBegin(pp0_iter);
		PixA::ModuleList::const_iterator module_end = PixA::modulesEnd(pp0_iter);

		for (PixA::ModuleList::const_iterator module_iter = module_begin;
		     module_iter != module_end;
		     ++module_iter) {

		  if (!addConnObj( PixA::connectivityName(module_iter), enabled_only )) break;

		}
	      }
	    }
	  }
	}

      }
      catch(...) {
      }
    }
  }



  void ValueHistoryView::init( const std::string &conn_name,
			       EMeasurementType measurement_type,
			       SerialNumber_t serial_number,
			       VarDef_t var_def,
			       bool enabled_only)
  {
    //     std::cout << "INFO [ValueHistoryView::createHistoryView] conn_name " << conn_name << " var_name=" << VarDefList::instance()->varName(var_def.id())
    // 	      << " has history ? " << (var_def.hasHistory() ? "yes" : "no")
    // 	      << std::endl;
    if (!var_def.hasHistory()) return /*NULL*/;

    VarId_t var_id = var_def.id();

    if (!PixCon::VarDefList::instance()->isValid(var_id)) return /*NULL*/;

    Lock lock(m_calibrationData->calibrationDataMutex());
    PixA::ConnectivityRef connectivity( m_calibrationData->connectivity(measurement_type,
								      serial_number) );
        std::vector<std::string> conn_names;
    if (connectivity) {
      EConnItemType conn_item_type = guessConnItemType(conn_name);
      if (var_def.connType()==kAllConnTypes) {
	addConnObj(conn_name, false);
      }
      else if (var_def.connType()==kRodValue && conn_item_type == kModuleItem) {
	const PixLib::RodBocConnectivity *rod_boc( PixA::rodBoc(connectivity.findModule(conn_name)) );
	if (rod_boc) {
	  addConnObj(PixA::connectivityName(rod_boc), enabled_only);
	}
      }
      else if (var_def.connType()==kRodValue && conn_item_type == kPp0Item) {
	const PixLib::RodBocConnectivity *rod_boc( PixA::rodBoc(connectivity.findPp0(conn_name)) );
	if (rod_boc) {
	  addConnObj(PixA::connectivityName(rod_boc), enabled_only);
	}
      }
      else if (var_def.connType()==kModuleValue && conn_item_type == kRodItem) {
	PixA::Pp0List pp0s = connectivity.pp0s(conn_name);
	PixA::Pp0List::const_iterator pp0_begin = pp0s.begin();
	PixA::Pp0List::const_iterator pp0_end = pp0s.end();
	for (PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
	     pp0_iter != pp0_end;
	     ++pp0_iter) {
	  
	  PixA::ModuleList::const_iterator module_begin = PixA::modulesBegin(pp0_iter);
	  PixA::ModuleList::const_iterator module_end = PixA::modulesEnd(pp0_iter);
	  for (PixA::ModuleList::const_iterator module_iter = module_begin;
	       module_iter != module_end;
	       ++module_iter) {
	    if (!addConnObj(PixA::connectivityName(module_iter), enabled_only)) break;
	  }
	}
      }
      else if (var_def.connType()==kModuleValue && conn_item_type == kPp0Item) {
	PixA::ModuleList modules = connectivity.modules(conn_name);
	PixA::ModuleList::const_iterator module_begin = modules.begin();
	PixA::ModuleList::const_iterator module_end = modules.end();
	for (PixA::ModuleList::const_iterator module_iter = module_begin;
	     module_iter != module_end;
	     ++module_iter) {
	  if (!addConnObj(PixA::connectivityName(module_iter), enabled_only)) break;
	}
      }
      else {
	addConnObj(conn_name, false);
      }

    }
  }

  void ValueHistoryView::updateGraphView() {
    std::map<std::string, TGraph * > graphs;

    TCanvas *a_canvas = canvas();
    if (!a_canvas) return;

    TLegend *old_legend=NULL;
    {
      TListIter iter(a_canvas->GetListOfPrimitives());
      TObject *obj;
      while ((obj=iter.Next())) {
	if (obj->InheritsFrom(TGraph::Class())) {
	  TGraph *a_graph=static_cast<TGraph *>(obj);
	  std::string name( a_graph->GetName());
	  if (m_connObjects.find( name) != m_connObjects.end()) {
	    graphs[name]=a_graph;
	  }
	}
	else if (obj->InheritsFrom(TLegend::Class())) {
	  old_legend=static_cast<TLegend *>(obj);
	}

      }
    }

    Lock lock(m_calibrationData->calibrationDataMutex());
    bool new_graph=false;
    unsigned int color_i=1;
    std::vector< TGraph * >  graph_list;
    a_canvas->cd();
    std::string options("ALP");
    for (std::map<std::string, CalibrationData::ConnObjDataList>::const_iterator conn_obj_iter = m_connObjects.begin();
	 conn_obj_iter != m_connObjects.end();
	 ++conn_obj_iter) {

      std::map<std::string, TGraph *>::iterator graph_iter = graphs.find(conn_obj_iter->first);
      TGraph *old_graph =NULL;
      if (graph_iter != graphs.end()) {
	old_graph = graph_iter->second;
      }

      TGraph *a_graph = createGraph(old_graph, conn_obj_iter->second,
				    m_listener->measurementType(),
				    m_listener->serialNumber(),
				    m_listener->varDef(),  
				    conn_obj_iter->first );

      if (a_graph) {
	if (!old_graph) {
	  new_graph=true;
	  a_graph->SetBit(kCanDelete);
	  a_graph->SetLineColor(color_i);
	  a_graph->SetMarkerColor(color_i);
	  a_graph->SetMarkerStyle(25);
	  a_graph->SetMarkerSize(a_graph->GetMarkerSize()*.8);
	  a_graph->Draw( options.c_str() );
	  options="LP";
	  ++color_i;
	}
	graph_list.push_back(a_graph);
      }
    }
    if (new_graph) {
      delete old_legend;

      TLegend *legend=new TLegend(0.69,0.88-(graph_list.size()*3.3e-2)-4.6e-2, 0.87, 0.88 );
      legend->SetBorderSize(1);
      legend->SetFillColor(0);

      for( std::vector<TGraph *>::iterator graph_iter = graph_list.begin();
	   graph_iter != graph_list.end();
	   ++graph_iter) {
	    
	legend->AddEntry((*graph_iter),(*graph_iter)->GetTitle(),"LP");
      }
      legend->Draw();
    }
    a_canvas->Modified();
    a_canvas->Update();
  }
  

  bool ValueHistoryView::addConnObj(const std::string &conn_name, bool enabled_only) {
    if (m_selection->isSelected( conn_name)) {
      try {
	PixCon::CalibrationData::ConnObjDataList data_list(m_calibrationData->calibrationData().getConnObjectData( conn_name));
	if (!enabled_only || data_list.enableState(m_listener->measurementType(), m_listener->serialNumber()).enabled()) {
	  if (m_connObjects.size()>=s_maxGraphs)return false;

	  m_connObjects.insert(std::make_pair( conn_name,data_list));
	  m_listener->addConnName(conn_name);
	}
      }
      catch(...) {
      }
    }
    return true;
  }


  ValueHistoryView::ValueHistoryView(const std::shared_ptr<SlowUpdateList> &slow_update_list,
				     const std::shared_ptr<ISelection> &selection,
				     const std::shared_ptr<CalibrationDataManager> &calibration_data,
				     EMeasurementType measurement_type,
				     SerialNumber_t serial_number,
				     VarDef_t var_def,
				     const std::string &conn_name,
				     bool enabled_only,
				     QWidget* parent)
    : QMainWindow(parent),
      m_slowUpdateList(slow_update_list),
      m_selection(selection),
      m_calibrationData(calibration_data),
      m_listener(new Listener(this, measurement_type, serial_number,var_def,""))
  {
    setupUi(this);
    delete m_canvas;
    MyQRootCanvas *my_canvas = new MyQRootCanvas(centralWidget(), "m_canvas");
    m_canvas = my_canvas;
    QSizePolicy spol((QSizePolicy::Policy)7, (QSizePolicy::Policy)7);
    spol.setHorizontalStretch(0);
    spol.setVerticalStretch(0);
    spol.setHeightForWidth(m_canvas->sizePolicy().hasHeightForWidth());
    m_canvas->setSizePolicy( spol );
    //m_canvas->setSizePolicy(QSizePolicy((QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, m_canvas->sizePolicy().hasHeightForWidth()));
    vboxLayout->addWidget(m_canvas);

    connect(my_canvas,SIGNAL(statusMessage(const QString &)),statusBar(),SLOT(showMessage(const QString &)));

    if (!m_selection) {
      m_selection = std::shared_ptr<ISelection>(new FullSelection);
    }

    if (!conn_name.empty()) {
      init(conn_name, m_listener->measurementType(), m_listener->serialNumber(),m_listener->varDef(), enabled_only);
    }
    else {
      init(m_listener->measurementType(), m_listener->serialNumber(),m_listener->varDef(), enabled_only);
    }

    updateGraphView();

    m_slowUpdateList->registerListener(m_listener->measurementType(),m_listener->serialNumber(),std::shared_ptr<ICalibrationDataListener>(m_listener));

  }
 
  ValueHistoryView::~ValueHistoryView()
  {
    m_slowUpdateList->deregisterListener(std::shared_ptr<ICalibrationDataListener>(m_listener));
  } 

  void ValueHistoryView::closeEvent(QCloseEvent*)
  {
    std::cout << "DEBUG [ValueHistoryView::closeEvent] called\n";
    deleteLater();
  }

  TCanvas *ValueHistoryView::canvas()
  {
    return m_canvas->getCanvas();
  }

  ValueHistoryView *ValueHistoryView::createHistoryView( const std::shared_ptr<SlowUpdateList> &slow_update_list,
							 const std::shared_ptr<ISelection> &selection,
							 const std::shared_ptr<CalibrationDataManager> &calibration_data,
							 EMeasurementType measurement_type,
							 SerialNumber_t serial_number,
							 VarDef_t var_def,
							 bool enabled_only) {
    std::string title(getTitle(var_def));
    if (title.empty()) return NULL;
    return new ValueHistoryView(slow_update_list, selection, calibration_data, measurement_type, serial_number, var_def, "", enabled_only, NULL);
  }
  
  ValueHistoryView *ValueHistoryView::createHistoryView( const std::shared_ptr<SlowUpdateList> &slow_update_list,
							 const std::shared_ptr<ISelection> &selection,
							 const std::shared_ptr<CalibrationDataManager> &calibration_data,
							 EMeasurementType measurement_type,
							 SerialNumber_t serial_number,
							 VarDef_t var_def,
							 const std::string &conn_name,
							 bool enabled_only) {
    std::string title(getTitle(var_def));
    if (title.empty()) return NULL;
    return new ValueHistoryView(slow_update_list, selection, calibration_data, measurement_type, serial_number, var_def, conn_name, enabled_only, NULL);
  }

  std::string ValueHistoryView::getTitle(const VarDef_t &var_def) {
    try {
      std::string title = "History : ";
      const std::string &var_name=PixCon::VarDefList::instance()->varName(var_def.id());
      title += var_name;
      return title;
    }
    catch(...) {
      return "";
    }
  }

  bool ValueHistoryView::Listener::needUpdate(EMeasurementType measurement, SerialNumber_t serial_number, VarId_t var_id, const std::string &conn_name ) {
    if (measurement == m_measurementType && serial_number == m_serialNumber) {
      if (var_id == m_varDef.id() || (var_id == VarDefBase_t::invalid().id() && !m_varDef.isValid() )){
	if (std::find(m_connNameList.begin(), m_connNameList.end(), conn_name)!=m_connNameList.end()) {
	  return true;
	}
      }
    }
    return false;
  }

}


