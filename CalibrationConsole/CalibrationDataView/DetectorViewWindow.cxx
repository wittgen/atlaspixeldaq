#include "DetectorViewWindow.h"
#include "DetectorViewList.h"
#include "DetectorView.h"
#include "CoupledDetectorView.h"

#include "ModuleList.h"

#include <qstatusbar.h>
#include <qdesktopwidget.h>

// to delete DetectorViewWindowBase::m_listView

namespace PixCon {
  unsigned int DetectorViewWindow::s_counter=0;

  DetectorViewWindow::DetectorViewWindow(PixCon::DetectorViewList &detector_view_list,
					 QApplication &app,
					 QWidget* parent)
: QMainWindow(parent),
      m_detectorViewList(&detector_view_list),
      m_detectorView(NULL),
      m_coupledDetectorView(NULL),
      m_moduleList(NULL),
      m_view(s_counter)
  {
    setupUi(this);
    std::stringstream new_name;
    new_name << "DetectorView [" << m_view << "]";
    setWindowTitle(QString::fromStdString(new_name.str()));
    QVBoxLayout *a_layout =new QVBoxLayout( m_leftPanel);
    a_layout->setAlignment( Qt::AlignTop );

    //std::stringstream view_name;
    //view_name << "DetectorView_" << m_view;
    m_detectorView = m_detectorViewList->createNewView(app, m_leftPanel);//,view_name.str().c_str());
    m_coupledDetectorView = dynamic_cast<CoupledDetectorView *>( m_detectorView );

    connect(m_detectorView, SIGNAL( statusMessage(const QString &) ), this->statusBar(), SLOT( showMessage(const QString &) ) );
   // connect(m_detectorView->m_listViewButton, SIGNAL( clicked() ), this, SLOT( toggleModuleListView() ) );

  //  delete m_listView;
  //  m_listView=NULL;
   // m_rightPanel->hide();
    a_layout->addWidget( m_detectorView);
    m_detectorView->show();
  }

  DetectorViewWindow::~DetectorViewWindow()
  {
    m_detectorViewList->destroyView(m_detectorView);
  }

  void DetectorViewWindow::closeEvent(QCloseEvent *)
  {
    std::cout << "DEBUG [DetectorViewWindow::closeEvent] called\n";
    deleteLater();
  }

  std::string DetectorViewWindow::makeName() {
    std::stringstream new_name;
    new_name << "DetectorView_" << ++s_counter;
    return new_name.str();
  }

//  void DetectorViewWindow::toggleModuleListView() {
//    if (m_coupledDetectorView) {
//    if (m_rightPanel->isHidden()) {
//      if (!m_moduleList) {
//
//	//	QSize old_size = m_rightPanel->sizeHint();
//	QSize old_size = m_rightPanel->size();
//	QSize old_left_size = m_leftPanel->size();
//
//	std::shared_ptr<CalibrationDataManager> manager = m_detectorView->calibrationDataManagerPtr();
//	std::shared_ptr<DataSelection> data_selection = m_detectorView->dataSelectionPtr();
//	m_moduleList=new ModuleList(manager,
//				    data_selection,
//				    m_detectorView->contextMenu(),
//				    m_rightPanel,"ModuleList",0);
//
//	m_coupledDetectorView->setModuleList(m_moduleList);
//
//	connect( m_detectorView, SIGNAL( changedDataSelection() ), m_moduleList, SLOT( changedVariable() ) );
//
//	m_rightPanelLayout->add(m_moduleList);
//
//	QSize new_size = m_rightPanel->sizeHint();
//
//	QSize current_size = size();
//	new_size.setWidth(current_size.width()+new_size.width()-old_size.width()+66);
//	new_size.setHeight(current_size.height());
//	QRect screen_size = m_detectorView->app().desktop()->screenGeometry(this);
//	int splitter_width_incr=new_size.width()-old_size.width();
//	if (x()+new_size.width() > screen_size.x() + screen_size.width()) {
//	  splitter_width_incr=screen_size.width()+screen_size.x()-x()-old_size.width()-current_size.width();
//	  new_size.setWidth(screen_size.width()+screen_size.x()-x());
//	}
//    QList<int> old_splitter_sizes=m_splitter->sizes();
//
//	resize(new_size);
//    QList<int> new_splitter_sizes=m_splitter->sizes();
//	if (new_splitter_sizes.size()==2) {
//	  new_splitter_sizes[1] = new_splitter_sizes[0]+new_splitter_sizes[1]-old_splitter_sizes[0] ;
//	  new_splitter_sizes[0] = old_splitter_sizes[0];
//	}
//	if (   new_splitter_sizes[1]!=old_splitter_sizes[1] ) {
//	  m_splitter->setSizes(new_splitter_sizes);
//	}
//      }
//      else if (m_moduleListUpdateDataSelection) {
//	std::shared_ptr<DataSelection> data_selection = m_detectorView->dataSelectionPtr();
//	m_moduleList->changeDataSelection(data_selection);
//      }
//      m_moduleList->show();
//      m_rightPanel->show();
//
//    }
//    else {
//      QSize new_size = size();
//      QSize right_panel_size = m_rightPanel->size();
//
//      m_rightPanel->hide();
//      QList<int> splitter_sizes=m_splitter->sizes();
//      if (splitter_sizes.size()>=2) {
//    new_size.setWidth(new_size.width()-right_panel_size.width());
//    splitter_sizes[1]=1;
//    m_leftPanel->resize(new_size);
//    resize(new_size);
//    m_splitter->setSizes(splitter_sizes);
//      }
//    }
//    }
//  }

}
