/**
 * @file UserMode.h
 * @brief Define roles for users of the console 
 * @author Tayfun Ince <tayfun.ince@cern.ch>
 * @date 2010/06/02
 */

#ifndef _PixCon_UserMode_H_
#define _PixCon_UserMode_H_

namespace PixCon {

  class UserMode
  {
  public:
    /**
     * User roles
     */
    enum EUserMode
      {
	kShifter=0, /**< Limited access to functionality */
	kExpert=1, /**< Full access to functionality */
	kUnknown=99 /**< No access to functionality */
      };

    /**
     * Constructor
     * @param user_mode Input user role
     */
    UserMode(const EUserMode& user_mode);
    
    /** Destructor */
    ~UserMode();

    /** Get user role */
    EUserMode getUserMode();

    /** Set user role */
    void setUserMode(const EUserMode& user_mode);

  private:
    EUserMode m_userMode;
    
  };

}
#endif
