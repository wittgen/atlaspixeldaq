#include "LogicParserBase.h"
#include <sstream>
#include "Tokeniser.h"

namespace PixCon {
  // Parsing section

  template <class T_Bool_t>
  void LogicParserBase<T_Bool_t>::compileExpression(PixCon::Tokeniser &tokeniser)
  {

    resetNegation();
    if (tokeniser.nextToken()) {
      ParseExpression(tokeniser,m_head);
      if (!tokeniser.isDone()) {
	std::stringstream message;
	message << "Unhandled token \"" << tokeniser.token() << "\".";
	throw UnhandledToken(message.str());
      }
    }
  }

  template <class T_Bool_t>
  void LogicParserBase<T_Bool_t>::ParseExpression(PixCon::Tokeniser &tokeniser, IEvaluator *& parent) 
  {
    //  std::cout << "LogicParserBase::ParseExpression  : " << tokeniser.token() << std::endl;
    IEvaluator * left;
    ParseTerm(tokeniser,left);
    parent=left; //honor your parents

    while(tokeniser.isOperator() && tokeniser.operatorId() == kOr)  { //be clever to loop 
      //    std::cout << "LogicParserBase::ParseExpression or : " << tokeniser.token() << std::endl;
      BinaryOperator * n = (negation() ? static_cast<BinaryOperator *>(new LogicalAnd) : static_cast<BinaryOperator *>(new LogicalOr) );
      parent = n;
      n->setFirst(left);

      if ( !tokeniser.nextToken() ) {
	throw MissingOperand("Missing operand after logical or.");
      }
      ParseTerm(tokeniser,n->second());
      left=parent; //think about tomorrow
    }

  }

  template <class T_Bool_t>
  void LogicParserBase<T_Bool_t>::ParseTerm(PixCon::Tokeniser &tokeniser, IEvaluator *& parent) 
  {
    //  std::cout << "LogicParserBase::ParseTerm : " << tokeniser.token() << std::endl;
    IEvaluator * left;
    ParseFactor(tokeniser,left, false);
    parent=left;

    while(tokeniser.isOperator() && tokeniser.operatorId() == kAnd) {
      //    std::cout << "LogicParserBase::ParseTerm and : " << tokeniser.token() << std::endl;
      BinaryOperator * n = ( negation() ? static_cast<BinaryOperator *>(new LogicalOr) : static_cast<BinaryOperator *>(new LogicalAnd));
      parent = n;
      n->setFirst( left );

      if ( !tokeniser.nextToken() ) {
	throw MissingOperand("Missing operand after logical and.");
      }
      ParseFactor(tokeniser,n->second(),false);
      left=parent;
    }
  }

  template <class T_Bool_t>
  void LogicParserBase<T_Bool_t>::ParseFactor(PixCon::Tokeniser &tokeniser, IEvaluator *& parent, bool strong_expression_only)
  {
    //  std::cout << "LogicParserBase::ParseFactor : " << tokeniser.token() << std::endl;
    if(tokeniser.isOperator() && tokeniser.operatorId() == kNot) {
      toggleNegation(); //negates nuclei and switches &&<->|| in expressions
      if (!tokeniser.nextToken()) {
	throw MissingOperand("Missing operand after logical not.");
      }
      else {
	ParseFactor(tokeniser,parent,true);
      }
    
      toggleNegation();
    }
    else if( tokeniser.isBlockOpen() ) {
      if (tokeniser.nextToken()) {
	ParseExpression(tokeniser,parent);
      }
      else {
	throw MissingOperand("Missing operand after \"(\".");
      }
      //    std::cout << "LogicParserBase::ParseFactor () : " << tokeniser.token() << std::endl;

      if(!tokeniser.isBlockClose()) {
	std::stringstream message;
	if (!tokeniser.isDone()) {
	  message << "Expected \")\" after sub expression but got \"" << tokeniser.token() << "\".";
	}
	else {
	  message << "Missing \")\" after sub expression.";
	}
	throw MissingOperator(message.str());
      }
      tokeniser.nextToken();
    }
    else { //we are at a nucleus
      ParseNucleus(tokeniser, parent, strong_expression_only);
    }
  }

  // Evaluation section
  template <class T_Bool_t>
  T_Bool_t LogicParserBase<T_Bool_t>::evaluate()
  {
    return m_head->eval();
  }

  template class LogicParserBase<EvalResult_t>;

}


