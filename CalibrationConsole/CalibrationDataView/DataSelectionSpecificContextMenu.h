// Dear emacs, this is -*-c++-*-
#ifndef _PixCon_DataSelectionSpecificContextmenu_h_
#define _PixCon_DataSelectionSpecificContextmenu_h_

#include "IContextMenu.h"
#include <CalibrationDataManager.h>

#include <vector>

namespace PixCon {

  class DataSelectionSpecificContextMenu : public IContextMenu
  {

  public:
    DataSelectionSpecificContextMenu(QObject * parent = 0 ) : IContextMenu(parent) {}

    virtual void dataSelectionChanged(const DataSelection &data_selection)  = 0;

    virtual DataSelectionSpecificContextMenu *clone() const = 0;

  };

}
#endif
