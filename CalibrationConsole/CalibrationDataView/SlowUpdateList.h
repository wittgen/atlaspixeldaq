/* Dear emacs, this is -*-c++-*- */
#ifndef _PixCon_SlowUpdateList_h_
#define _PixCon_SlowUpdateList_h_

#include "ICalibrationDataListener.h"

#include <vector>
#include <map>
#include <memory>

namespace PixCon {

  template<class T>
  class UpdateInfo_t {
  public:
    UpdateInfo_t() : m_needsUpdate(false) {}
    UpdateInfo_t(const T &a) : m_needsUpdate(true), m_element(a) {}

    void setNeedUpdate(bool needs_update) { m_needsUpdate=needs_update; }
    bool needUpdate() const { return m_needsUpdate; }

    T &element()             { return m_element; }
    const T &element() const { return m_element; }

  private:
    T m_element;
    bool m_needsUpdate;
  };

  class SlowUpdateList
  {
  public:
    SlowUpdateList();
    ~SlowUpdateList();

    void registerListener(EMeasurementType measurement, SerialNumber_t serial_number, const std::shared_ptr<ICalibrationDataListener> &listener);
    void deregisterListener(const std::shared_ptr<ICalibrationDataListener> &listener);

    bool checkUpdate(EMeasurementType measurement, SerialNumber_t serial_number, VarId_t var_id, const std::string &conn_name);
    void update();

    unsigned int nListener() const { return m_listener.size(); }

  protected:
  private:
    typedef std::vector<std::shared_ptr<ICalibrationDataListener> > ListenerList_t;
    typedef std::map< SerialNumber_t, UpdateInfo_t< ListenerList_t > > ListenerUpdateMap_t;
    typedef std::vector< UpdateInfo_t< ListenerUpdateMap_t > > ListenerRegister_t;

    static ListenerList_t::iterator
    findListener(ListenerList_t &listener_list,
		 const std::shared_ptr<ICalibrationDataListener> &a_listener) {
      for(ListenerList_t::iterator listener_iter = listener_list.begin();
	  listener_iter != listener_list.end();
	  ++listener_iter) {
	if ( listener_iter->get() == a_listener.get() ) return listener_iter;
      }
      return listener_list.end();
    }
    
    ListenerRegister_t m_listener;
  };
}
#endif
