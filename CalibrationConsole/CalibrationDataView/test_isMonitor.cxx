

#include <ipc/partition.h>
#include <ipc/core.h>

#include <is/inforeceiver.h>

#include <ConfigWrapper/pixa_exception.h>
#include <ConfigWrapper/ConnectivityManager.h>
#include <ConfigWrapper/Connectivity.h>
#include <ConfigWrapper/ConnectivityUtil.h>
#include "CalibrationData.h"
#include "IsMonitor.h"
#include "Lock.h"
#include "DcsVariables.h"
#include "QIsInfoEvent.h"
#include <qapplication.h>

#include <qdialog.h>

#include <qvariant.h>
#include <qheader.h>
#include <qlistview.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>

#include "test_isMonitorVarList.h"

VarList::VarList( QWidget* parent, const char* name, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl )
{
    if ( !name )
	setName( "VarList" );
    VarListLayout = new QVBoxLayout( this, 11, 6, "VarListLayout"); 

    listView1 = new QListView( this, "listView1" );
    listView1->addColumn( tr( "Name" ) );
    listView1->addColumn( tr( "Value" ) );
    listView1->addColumn( tr( "Modification" ) );
    listView1->setResizeMode( QListView::AllColumns );
    listView1->show();
    VarListLayout->addWidget( listView1 );
    languageChange();
    resize( QSize(600, 480).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
}

VarList::~VarList()
{
}

void VarList::languageChange()
{
    setCaption( tr( "Form1" ) );
    listView1->header()->setLabel( 0, tr( "Name" ) );
    listView1->header()->setLabel( 1, tr( "Value" ) );
    listView1->header()->setLabel( 2, tr( "Modification" ) );
}


class CalibVarList : public VarList
{
public:
  CalibVarList( PixCon::CalibrationData &calib_data,
		PixCon::Mutex &calib_data_mutex,  
		QWidget* parent = 0, 
		const char* name = 0,
		bool modal = FALSE, 
		WFlags fl = 0 ) 
    : VarList(parent,name,modal,fl),
      m_calibData(&calib_data),
      m_calibDataMutex(&calib_data_mutex)
  {
  }

  bool event(QEvent *an_event)
  {
    if (an_event && an_event->type() == QEvent::User) {
      PixCon::QIsInfoEvent * info_event  = dynamic_cast< PixCon::QIsInfoEvent *>(an_event);
      if (info_event) {
	//	std::cout << "INFO [CalibVarList::event] info event " << info_event->connName() << " :  " << info_event->varName() << std::endl;
	if (info_event->valid()) {
	  PixCon::Lock lock(*m_calibDataMutex);
	  if (info_event->connType() == PixCon::kModConn) {
	    std::map<std::string, std::map< std::string, QListViewItem *> >::const_iterator iter = m_modItems.find(info_event->connName());
	    if (iter != m_modItems.end()) {
	      std::map< std::string, QListViewItem *>::const_iterator val_iter = iter->second.find(info_event->varName());
	      if (val_iter != iter->second.end()) {
		PixCon::CalibrationData::ConnObjDataList data_list = m_calibData->getConnObjectData(info_event->connName());
		std::stringstream value_str;
		value_str << data_list.currentValue<float>(PixCon::VarDefList::instance()->getVarDef(info_event->varName()).id());
		val_iter->second->setText(1,value_str.str().c_str());
	      }
	    }
	  }
	  return true;
	}
      }
    }
    VarList::event(an_event);
    return false;
  }

  std::map<std::string, std::map<std::string, QListViewItem *> > m_modItems;
  PixCon::CalibrationData *m_calibData;
  PixCon::Mutex          *m_calibDataMutex;
  
};



int main(int argc, char **argv)
{
  std::string partition_name;
  std::string tag_name;
  std::string cfg_tag_name;
  std::string is_name = "PixFakeData";
  bool error=false;

  for (int arg_i=1; arg_i<argc; arg_i++) {
    if (strcmp(argv[arg_i],"-p")==0 && arg_i+1<argc) {
      partition_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-t")==0 && arg_i+2<argc && argv[arg_i+2][0]!='-' && argv[arg_i+1][0]!='-') {
      tag_name = argv[++arg_i];
      cfg_tag_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-n")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      is_name = argv[++arg_i];
    }
    else {
      std::cout << "ERROR ["<< argv[0] << ":main] unhandled argument " << arg_i << " : " << argv[arg_i]<< "." << std::endl;
      error=true;
    }
  }

  if (tag_name.empty() || cfg_tag_name.empty() || partition_name.empty()  || error ) {
      std::cout << "usage: " << argv[0] << "-p partition -t tag cfg-tag [-n \"IS server name\"]" << std::endl;
      return -1;
  }
  // START IPC

  PixA::ConnectivityRef conn;
  try {
     conn=PixA::ConnectivityManager::getConnectivity( tag_name, tag_name, tag_name, cfg_tag_name, 0 );
  }
  catch (PixA::ConfigException &err) {
    std::cout << "FATAL [main] caught exception (ConfigException) : " << err.getDescriptor() << std::endl;
    return -1;
  }
  catch (PixA::BaseException &err) {
    std::cout << "FATAL [main] caught exception (BaseException) : " << err.getDescriptor() << std::endl;
    return -1;
  }
  catch (std::exception &err) {
    std::cout << "FATAL [main] caught exception (std::exception) : " << err.what() << std::endl;
    return -1;
  }
  catch (...) {
    std::cout << "FATAL [main] unhandled exception. "  << std::endl;
    return -1;
  }

  // Start IPCCore
  IPCCore::init(argc, argv);

  // Create IPCPartition
  //  std::cout << "INFO [" << argv[0] << ":main] generate fake DCS data on " << partition_name  << " in IS server "  << is_name << std::endl;
  IPCPartition ipcPartition(partition_name.c_str());

  PixCon::CalibrationData calibration_data;
  PixCon::Mutex calibration_data_mutex;


  QApplication app( argc, argv );
  CalibVarList main(calibration_data, calibration_data_mutex);

  ISInfoReceiver info(ipcPartition);
  std::shared_ptr<PixCon::IsReceptor> receptor(new PixCon::IsReceptor(&app,
									&main,
									is_name,
									info,
									calibration_data,
									calibration_data_mutex));

  std::vector<std::shared_ptr< PixCon::IsValueMonitor > > is_monitor_list;

  {

    for (std::vector<PixCon::DcsVariables::EVariable>::const_iterator module_var_iter = PixCon::DcsVariables::beginModuleVar();
	 module_var_iter != PixCon::DcsVariables::endModuleVar();
	 module_var_iter++) {
      is_monitor_list.push_back( std::shared_ptr<PixCon::IsValueMonitor>(new PixCon::IsValueMonitor(ipcPartition,
												      receptor,
												      PixCon::kModConn,
												      PixCon::DcsVariables::varName(*module_var_iter))));
    }


    for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
	 crate_iter != conn.crates().end();
	 ++crate_iter) {

      QListViewItem *crate_item = new QListViewItem(main.listView1,PixA::connectivityName(crate_iter));
      crate_item->setOpen(true);
      PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
      PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
      for (PixA::RodList::const_iterator rod_iter = rod_begin;
	   rod_iter != rod_end;
	   ++rod_iter) {

	QListViewItem *rod_item = new QListViewItem(crate_item,PixA::connectivityName(rod_iter));
	rod_item->setOpen(true);

	PixA::Pp0List::const_iterator pp0_begin = PixA::pp0Begin(rod_iter);
	PixA::Pp0List::const_iterator pp0_end = PixA::pp0End(rod_iter);
	for (PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
	     pp0_iter != pp0_end;
	     ++pp0_iter) {
	  QListViewItem *pp0_item = new QListViewItem(rod_item,PixA::connectivityName(pp0_iter));


	  PixA::ModuleList::const_iterator module_begin = PixA::modulesBegin(pp0_iter);
	  PixA::ModuleList::const_iterator module_end = PixA::modulesEnd(pp0_iter);
	  for (PixA::ModuleList::const_iterator module_iter = module_begin;
	       module_iter != module_end;
	       ++module_iter) {

	    QListViewItem *mod_item = new QListViewItem(pp0_item,PixA::connectivityName(module_iter));

	    std::pair<std::map<std::string, std::map<std::string, QListViewItem *> >::iterator , bool>
	      mod_item_list = main.m_modItems.insert(std::make_pair(PixA::connectivityName(module_iter),std::map<std::string, QListViewItem *>()) );

	    if (mod_item_list.second) {
	      for (std::vector<PixCon::DcsVariables::EVariable>::const_iterator module_var_iter = PixCon::DcsVariables::beginModuleVar();
		   module_var_iter != PixCon::DcsVariables::endModuleVar();
		   module_var_iter++) {

		QListViewItem *value_item = new QListViewItem(mod_item,PixCon::DcsVariables::varName(*module_var_iter));
		mod_item_list.first->second.insert(std::make_pair(PixCon::DcsVariables::varName(*module_var_iter),value_item));
	      }
	    }
	  }
	}
      }
    }
  }
  //  receptor->start();
  main.show();
  app.connect( &app, SIGNAL( lastWindowClosed() ), &app, SLOT( quit() ) );
  return main.exec();

  return 0;
}
