#include "IsReceptor.h"
#include "IsMonitorManager.h"
#include "IsMonitorBase.h"
#include <is/inforeceiver.h>

#ifdef DEBUG_TRACE
#  undef DEBUG_TRACE
#endif
#ifdef DEBUG
#  define DEBUG_TRACE(a) { a; }
#  include <iostream>
#else
#  define DEBUG_TRACE(a) {  }
#endif

namespace PixCon {

  unsigned int s_updateMonitors=0;
  unsigned int s_reconnectMonitors=0;
  unsigned int s_reconnectReceptor=0;
  unsigned int s_nMonitors=0;

  void IsMonitorManagementStat() {
    std::cout << "INFO [IsMonitorManagerStat] "
	      << " mon_updates=" << s_updateMonitors
	      << " mon_reconnect=" << s_reconnectMonitors
	      << " receptor_reconnect=" << s_reconnectReceptor
	      << " (n-monitors=" << s_nMonitors << ")"
	      << std::endl;
  }

  extern bool s_reconnect;

  class IsMonitorManager::Reconnect {
  public:
    Reconnect(IsMonitorManager &manager, const std::map< std::string, std::shared_ptr<IsMonitorBase> > & monitor_list)
      : m_manager(&manager),
	m_monitorList(&monitor_list) 
    {}

    ~Reconnect() {
      for(std::map< IsReceptor *, std::vector< std::shared_ptr<IsMonitorBase> > > ::iterator receptor_iter = m_problems.begin();
	  receptor_iter != m_problems.end();
          ++receptor_iter) {
	try {
	  std::cout << "INFO [IsMonitorManager::Reconnect::dor] Try to reconnect " << receptor_iter->first->isServerName() << "." << std::endl;
	  for(std::vector< std::shared_ptr<IsMonitorBase> >::iterator monitor_iter = receptor_iter->second.begin();
	      monitor_iter != receptor_iter->second.begin();
	      ++monitor_iter) {
	    (*monitor_iter)->disconnect();
	  }
	  s_reconnectReceptor++;

	  receptor_iter->first->reconnect();
	  // to wait until the receptor is up and running :
	  if (!receptor_iter->first->receiverIsRunning()) {
	    if (!receptor_iter->first->connectTimedWait(20)) {
	      throw std::runtime_error("Connection timed out.");
	    }
	  }

	  bool success=false;
	  bool failure=false;
	  for(std::map< std::string, std::shared_ptr<IsMonitorBase> >::const_iterator monitor_iter = m_monitorList->begin();
	      monitor_iter != m_monitorList->end();
	      ++monitor_iter) {
	    if (monitor_iter->second->receptor().get() == receptor_iter->first) {
	      try {
		s_reconnectMonitors++;
		monitor_iter->second->reconnect(m_manager);
		success=true;
	      }
	      catch(std::exception &err) {
		failure=true;
		std::cerr << "ERROR [IsMonitorManager::Reconnect::dor] Failed to reconnect a monitor to " 
			  << receptor_iter->first->isServerName() << " : " 
			  << err.what()
			  << std::endl;
	      }
	      catch(...) {
		failure=true;
		std::cerr << "ERROR [IsMonitorManager::Reconnect::dor] Failed to reconnect a monitor to " 
			  << receptor_iter->first->isServerName() << "." << std::endl;
	      }
	    }
	  }
	  // cleanup everything which has not been cleaned up yet
	  if (success) {
	    receptor_iter->first->cleanup();
	    if (!failure) {
	      std::cout << "INFO [IsMonitorManager::Reconnect::dtor] Successfully reconnected to IS server " << receptor_iter->first->isServerName() << "." << std::endl;
	    }
	    else {
	      std::cerr << "WARNING [IsMonitorManager::Reconnect::dtor] Some monitors did not correctly reconnected to IS server "
			<< receptor_iter->first->isServerName() << "." << std::endl;
	    }
	  }

	}
	catch (...) {

	  for(std::vector< std::shared_ptr<IsMonitorBase> >::iterator monitor_iter = receptor_iter->second.begin();
	      monitor_iter != receptor_iter->second.begin();
	      ++monitor_iter) {
	    (*monitor_iter)->setNeedUpdate(true);
	  }

	  std::cerr << "FATAL [IsMonitorManager::Reconnect::dtor] Failed to reconnect " << receptor_iter->first->isServerName() << "." << std::endl;
	}
      }

      for (std::set<IsReceptor *>::const_iterator iter = m_cleanup.begin();
 	   iter != m_cleanup.end();
 	   ++iter ) {
 	if (m_problems.find(*iter) != m_problems.end()) continue;
 	(*iter)->cleanup();
      }
      if (!m_problems.empty()) {
	IsMonitorManagementStat();
      }
    }

    void operator()(std::pair< const std::string, std::shared_ptr<IsMonitorBase> > &a_monitor) {
      s_reconnectMonitors++;
      try {
	a_monitor.second->reconnect(m_manager);
      }
      catch(...) {
	addProblem(a_monitor.second->receptor().get(),a_monitor.second);
      }
    }

  protected:
    void addProblem(IsReceptor *a_receptor, const std::shared_ptr<IsMonitorBase> &a_monitor) {
	m_problems[a_receptor].push_back(a_monitor);
    }

    void needCleanup(IsReceptor *a_receptor) {
      if (a_receptor->needCleanup()) {
	m_cleanup.insert(a_receptor);
      }
    }


    IsMonitorManager &isManager()             { return *m_manager; }
    const IsMonitorManager &isManager() const { return *m_manager; }
    
  private:
    IsMonitorManager *m_manager;
    const std::map< std::string, std::shared_ptr<IsMonitorBase> >     *m_monitorList;
    std::map< IsReceptor *, std::vector< std::shared_ptr<IsMonitorBase> > >  m_problems;
    std::set< IsReceptor * >  m_cleanup;
  };



  class IsMonitorManager::Update : public Reconnect {
  public:
    Update(IsMonitorManager &manager, const std::map< std::string, std::shared_ptr<IsMonitorBase> > & monitor_list) 
      : Reconnect(manager, monitor_list),m_currentTime(time(NULL)) {}

    ~Update() {}

    /** Process queue or reread everything.
     * @return true if there is nothting more to be done for this monitor, false in case the queue is not fully empty yet.
     */
    bool operator()(std::pair< const std::string, std::shared_ptr<IsMonitorBase> > &a_monitor) {

      // full update if nothing changed for a too long period or if an update was requested.
      needCleanup(a_monitor.second->receptor().get());

      ::time_t last_update = a_monitor.second->lastUpdate();
      if (a_monitor.second->needUpdate()
	  || (last_update<m_currentTime && m_currentTime - last_update > static_cast< ::time_t >(a_monitor.second->maxTimeBetweenUpdates())) ) {

	DEBUG_TRACE( std::cout << "INFO [IsMonitorManager::Update] need update =" \
	             << a_monitor.second->needUpdate() \
	             << " current time =" << m_currentTime << " last update = " << last_update \
	             << " elapsed = " << m_currentTime - last_update << " > " \
	             << a_monitor.second->maxTimeBetweenUpdates() << std::endl );

	//	if (a_monitor.second->hasMonitorManager()) {
	  try {
	    a_monitor.second->resubscribe();
	    return true;
	  }
	  catch(...) {
	    std::cout << "WARNING [IsMonitorManager::Update] Failed to resubscribe. Will try to reconnect." << std::endl;
	  }
	  //	}
	addProblem(a_monitor.second->receptor().get(),a_monitor.second);
	return false;
      }
      else {
	return a_monitor.second->processData();
      }
    }

  private:
    ::time_t m_currentTime;
  };

  class IsMonitorManager::Reread {
  public:
    Reread() : m_skip(false) {}

    /** Process queue or reread everything.
     * @return false if a reconnect is needed.
     */
    bool operator()(std::pair< const std::string, std::shared_ptr<IsMonitorBase> > &a_monitor) {

      if (!m_skip) {
	DEBUG_TRACE( 	::time_t current_time; std::cout << "INFO [IsMonitorManager::Update] need update =" \
		     << a_monitor.second->needUpdate() \
		     << " current time =" << current_time << " last update = " << a_monitor.second->lastUpdate() \
		     << " elapsed = " << current_time - a_monitor.second->lastUpdate() << " > " \
		     << a_monitor.second->maxTimeBetweenUpdates() << std::endl );

	try {
	  s_updateMonitors++;
	  a_monitor.second->update();
	  return true;
	}
	catch( ... ) {
	  m_skip=true;
	}
      }
      return false;
    }

  private:
    bool m_skip;
  };

  /** Get the maximum allowed time between updates
   */
  class IsMonitorManager::GetCheckTime {
  public:
    GetCheckTime(IsMonitorManager &manager) : m_manager(&manager),m_checkTime(UINT_MAX) { assert(m_manager); }

    ~GetCheckTime() { m_manager->setCheckTime(m_checkTime); }

    /** Process queue or reread everything.
     * @return true if there is nothting more to be done for this monitor, false in case the queue is not fully empty yet.
     */
    void operator()(std::pair< const std::string, std::shared_ptr<IsMonitorBase> > &a_monitor) {
       if (a_monitor.second->maxTimeBetweenUpdates() > 0 && a_monitor.second->maxTimeBetweenUpdates()<m_checkTime) {
	 m_checkTime = a_monitor.second->maxTimeBetweenUpdates();
       }
    }

  private:
    IsMonitorManager *m_manager;
    unsigned int m_checkTime;
  };




  /**  Helper class to collect all receptors and ask them to reconnect.
   */
  class IsMonitorManager::ReconnectReceptors {
  public:

    void operator()(std::pair< const std::string, std::shared_ptr<IsMonitorBase> > &a_monitor) {
      if (m_receptorList.find(a_monitor.second->receptor().get()) == m_receptorList.end()) {
	std::cout << "INFO [IsMonitorManager::ReconnectReceptors] reconnect to IS server "  << a_monitor.second->receptor()->isServerName() << std::endl;
	m_receptorList.insert( a_monitor.second->receptor().get() );
	try {
	  s_reconnectReceptor++;
	  a_monitor.second->receptor()->reconnect();
	}
	catch(...) {
	  std::cerr << "ERROR [IsMonitorManager::ReconnectReceptors] Failed to reconnect monitor " <<  a_monitor.first
		    << " to IS server "  << a_monitor.second->receptor()->isServerName() << std::endl;
	}
      }
    }

  private:
    std::set< void *> m_receptorList;
  };

  class IsMonitorManager::Disconnect {
  public:
    void operator()(std::pair< const std::string, std::shared_ptr<IsMonitorBase> > &a_monitor) const {
      try {
	std::cout << "INFO [IsMonitorManager::Disconnect::operator()] Disconnect " << a_monitor.first << std::endl;
	a_monitor.second->disconnect();
      }
      catch(...) {
	// do not care whether a disconnect fails.
      }
    }
  };

  class IsMonitorManager::Shutdown : public IsMonitorManager::Disconnect {
  public:
    void operator()(std::pair< const std::string, std::shared_ptr<IsMonitorBase> > &a_monitor) {
      IsMonitorManager::Disconnect::operator()(a_monitor);
      if (a_monitor.second) {
	a_monitor.second->disconnect();
	m_receptorList.insert(a_monitor.second->receptor().get());
      }
    }

    ~Shutdown() {
      for (std::set<IsReceptor *>::iterator receptor_iter = m_receptorList.begin();
	   receptor_iter != m_receptorList.end();
	   ++receptor_iter) {
	if (*receptor_iter) {
	  std::cout << "INFO [IsMonitorManager::Shutdown::dtor] shutdown " << (*receptor_iter)->isServerName() << std::endl;
	  (*receptor_iter)->shutdown();
	}
      }
    }

  private:
    std::set<IsReceptor *> m_receptorList;
  };


  // helper method to iterate over all containers and produce an "or"-ed boolean flag.
  template <class T_container_iterator, class T_operator>
  bool for_each_until_done(T_container_iterator begin_iter, T_container_iterator end_iter, T_operator func)
  {
    bool ret=true;
    for(T_container_iterator iter = begin_iter; iter != end_iter; ++iter) {
      ret |= func(*iter);
    }
    return ret;
  }

  IsMonitorManager::IsMonitorManager()
    : m_checkTime(UINT_MAX)
  {
    m_running=true;
    start_undetached();
  }


  IsMonitorManager::~IsMonitorManager() {
    IsMonitorManagementStat();
    shutdown();
  }

  void *IsMonitorManager::run_undetached(void *arg) {
    assert(arg==NULL);

    std::cout << "INFO [InfoUpdater::run_undetached] Startet run loop." << std::endl;
    for(;;) {

      if (m_checkTime>0) {
	m_updateRequests.timedwait(m_checkTime, m_shutdown.flag());
      }
      else {
        m_updateRequests.wait(m_shutdown.flag());
      }

      if (m_shutdown) break;
      Lock lock(m_monitorMutex);
      if (m_reconnect) {
	std::cout << "INFO [InfoUpdater::run_undetached] Reconnect." << std::endl;

	// reconnect all monitors
	m_reconnect=false;
	for_each(m_monitorList.begin(), m_monitorList.end(), Disconnect() );
	ReconnectReceptors reconnect_receptors;
	for_each(m_monitorList.begin(), m_monitorList.end(), reconnect_receptors );
	//	  for_each(m_monitorList.begin(), m_monitorList.end(), VoidFunction(&IsMonitorManager::reconnect, *this));
	for_each(m_monitorList.begin(), m_monitorList.end(), Reconnect(*this, m_monitorList));
      }
      else if (m_reread) {
	std::cout << "INFO [InfoUpdater::run_undetached] Reread." << std::endl;
	m_reread=false;
	if (! for_each_until_done(m_monitorList.begin(), m_monitorList.end(), Reread())) {
	  reconnect();
	}
      }
      else {

	// process the queues of all monitors
	// reread everything if needed
	bool all_done;
	do {
	  all_done = for_each_until_done(m_monitorList.begin(), m_monitorList.end(), Update(*this, m_monitorList));
	}  while (!all_done && !m_shutdown && !m_reconnect);

	if (m_shutdown) break;
      }
    }

    // disconnect monitors and exit
    std::cout << "INFO [IsMonitorManager::run_undetached] Shutdown monitors." << std::endl;
    for_each(m_monitorList.begin(), m_monitorList.end(), Shutdown() );
    m_monitorList.clear();

    std::cout << "INFO [IsMonitorManager::run_undetached] done." << std::endl;
    m_exit.setFlag();
    m_running=false;
    return NULL;
  }

  void IsMonitorManager::initiateShutdown()
  {
    std::cout << "INFO [IsMonitorManager::dtor] "
	      << " mon_updates=" << s_updateMonitors
	      << " mon_reconnect=" << s_reconnectMonitors
	      << " receptor_reconnect=" << s_reconnectReceptor
	      << " (n-monitors=" << s_nMonitors << ")"
	      << std::endl;
    
    if (m_running) {
       m_shutdown = true;
       m_updateRequests.setFlag();
     }
  }

  void IsMonitorManager::shutdown() {

    if (m_running) {
      initiateShutdown();
      m_exit.wait();
      while (state()!=omni_thread::STATE_TERMINATED) {
	usleep(1000);
      }
      m_running=false;
    }
  }

  void IsMonitorManager::addMonitor(const std::string &name,  const std::shared_ptr<IsMonitorBase> &monitor) {
    if (!monitor.get()) return;
    if (name.empty()) {
      throw std::logic_error("FATAL [IsMonitorManager::addMonitor] monitor tried to add a monitor without a name.");
    }

    if (!monitor->receptor().get()) {
      std::cerr << "ERROR [IsMonitorManager::addMonitor] monitor " << name << " has no receptor. Not adding." << std::endl;
      return;
    }
    Lock lock(m_monitorMutex);
    std::pair<std::map<std::string, std::shared_ptr<IsMonitorBase> >::iterator, bool> ret =  m_monitorList.insert( std::make_pair(name, monitor) );
    if (!ret.second) {
      std::cerr << "ERROR [IsMonitorManager::addMonitor] failed to add monitor " << name << ". Maybe a monitor of the same name exists already." << std::endl;
      return;
    }
    ++s_nMonitors;
    unsigned int max_time_between_updates = ret.first->second->maxTimeBetweenUpdates();
    if (max_time_between_updates>0 && max_time_between_updates<m_checkTime) {
      m_checkTime=max_time_between_updates;
    }
    try {
      ret.first->second->reconnect(this);
    }
    catch(daq::is::Exception &err) {
      std::cerr << "ERROR [IsMonitorManager::addMonitor] Caught IS exception while trying to add monitor " << name << " : " << err.message() << std::endl;
    }
    catch(...) {
      std::cerr << "ERROR [IsMonitorManager::addMonitor] Caught unknown exception while trying to add monitor " << name << "." << std::endl;
    }

  }

  std::shared_ptr<IsMonitorBase> IsMonitorManager::removeMonitor(const std::string &name) {
    Lock lock(m_monitorMutex);
    std::map<std::string, std::shared_ptr<IsMonitorBase> >::iterator monitor_iter = m_monitorList.find(name);
    if (monitor_iter != m_monitorList.end()) {
      std::shared_ptr<IsMonitorBase> a_monitor = monitor_iter->second;
      a_monitor->disconnect();
      m_monitorList.erase(monitor_iter);
      for_each(m_monitorList.begin(), m_monitorList.end(), GetCheckTime(*this));
      return a_monitor;
    }
    return std::shared_ptr<IsMonitorBase>();
  }

  std::shared_ptr<IsMonitorBase> IsMonitorManager::removeMonitor(const std::shared_ptr<IsMonitorBase> &monitor) {
    if (!monitor.get()) return monitor;
    Lock lock(m_monitorMutex);

    for (std::map<std::string, std::shared_ptr<IsMonitorBase> >::iterator monitor_iter = m_monitorList.begin();
	 monitor_iter != m_monitorList.end();
	 ++monitor_iter) {

      if (monitor_iter->second.get() == monitor.get()) {
	std::shared_ptr<IsMonitorBase> a_monitor = monitor_iter->second;
	a_monitor->disconnect();
	m_monitorList.erase(monitor_iter);
	for_each(m_monitorList.begin(), m_monitorList.end(), GetCheckTime(*this));
	return a_monitor;
      }
    }
    return std::shared_ptr<IsMonitorBase>();
  }

}
