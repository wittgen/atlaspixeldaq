/* Dear emacs, this is -*-c++-*- */
#ifndef _PixCon_IsReceptor_H_
#define _PixCon_IsReceptor_H_

#include "omnithread.h"
#include "Flag.h"

#include <ipc/partition.h>
#include <owl/semaphore.h>

#include <string>
#include <memory>
#include <map>
#include <set>

#ifdef DEBUG
#  define DEBUG_TRACE(a) { a; }
#  include <iostream>
#else
#  define DEBUG_TRACE(a) {  }
#endif

// @todo for validation
#include <unistd.h>

#include <CalibrationDataTypes.h>
#include <VarDefList.h>
#include <memory>
//Added by qt3to4:
#include <QEvent>

class ISInfoReceiver;
class QObject;
class QApplication;
class QEvent;

namespace PixCon {

  class IVarNameExtractor;
  class CalibrationDataManager;

  extern unsigned int s_setValueWait;

  /** Receives IS information and forwards them to IS monitor receivers.
   * Also provides the means to send the information to the GUI
   */
  class IsReceptor : public omni_thread
  {
  public:
    IsReceptor(QApplication *app,
	       QObject *receiver_object,
	       const std::string &is_server_name,
	       IPCPartition &partition,
	       const std::shared_ptr<CalibrationDataManager> &calibration_data);

    ~IsReceptor();

    /** Shutdown the IS info receiver
     */
    void shutdown();


    /** Reconnect the IS info receiver with the IS server.
     */
    void reconnect();


    /** Disconnect the IS infor receiver and reconnect it to an IS server of a different partition.
     */
    void reconnect(IPCPartition &partition);

    bool needCleanup() const {
      return !m_cleanupVariables.empty();
    }

  protected:

    void *run_undetached(void *arg);
    void run(void*);

  public:

    /** Return the info receiver.
     * If no receiver is running yet, a new receiver will be started.
     * @return a valid info receiver.
     */
    ISInfoReceiver &receiver() {
      if (!m_isReceiver.get()) {
	DEBUG_TRACE( std::cout << "INFO [IsReceptor::receiver] no info receiver will wait until receiver for  " << m_partition.name() << " is started ." << std::endl );
	reconnect();
	m_connected.wait();
	DEBUG_TRACE( std::cout << "INFO [IsReceptor::receiver] Okay info receiver for " << m_partition.name() << " should now be running." << std::endl );
      }
      return *m_isReceiver;
    }
    
    bool connectTimedWait(unsigned long timeout) {
      return m_connected.timedwait(timeout);
    }

    Mutex &receiverLock() { return m_receiverLock; }

    bool receiverIsRunning() const {
      return m_connected.isFlagged();
    }

    /** Get the IS server name to which the receiver is connected or will connect.
     */
    const std::string &isServerName() { return m_isServerName; }

    /** Get the partition of the IS server to which the receiver is connected or will connect.
     */
    const IPCPartition &partition()   { return m_partition; }



    /** Set current value without history.
     */
    template <class T>
    void setValue(EValueConnType conn_type, const std::string &is_name, const IVarNameExtractor &extractor, ::time_t time_stamp, T value) {
      setValue(conn_type, kCurrent,0, is_name, extractor, time_stamp, value);
    }

    /** Set value with history.
     */
    template <class T>
    void setValue(EValueConnType conn_type,
		  EMeasurementType measurement,
		  SerialNumber_t serial_number,
		  const std::string &is_name,
		  const IVarNameExtractor &extractor,
		  ::time_t time_stamp,
		  T value);

    /** Set value without history.
     */
    template <class T>
    void setValue(EValueConnType conn_type,
		  EMeasurementType measurement,
		  SerialNumber_t serial_number,
		  const std::string &is_name,
		  const IVarNameExtractor &extractor,
		  T value) {
      setValue(conn_type, measurement, serial_number, is_name, extractor, 0, value);
    }


    void setEnabled(EValueConnType conn_type,
		    EMeasurementType measurement,
		    SerialNumber_t serial_number,
		    const std::string &is_name,
		    const IVarNameExtractor &extractor,
		    bool enabled);

    /** Register the given variable for cleanup.
     * @param conn_type the connectivity type of the variable
     * @param measurement the measurement type to be cleanedup
     * @param serial number the measurement type to be cleanedup
     */

    void registerForCleanup(EMeasurementType measurement,
			     SerialNumber_t serial_number,
			     const std::string &conn_name,
			     const std::string &var_name,
			     std::map<std::string, std::set<std::string> > &clean_up_var_list);

    /** Cleanup the current measurement for all variables which are in the map.
     * @param clean_up_var_list list of variables to be cleaned up with an associated list of objectes for which the variable should stay untouched.
     * Values are untouched of objects whose connectivity name is contained in the set which is associated to each variable name.
     */
    void cleanup(const std::map<std::string, std::set<std::string> > &clean_up_var_list);

    /** Cleanup all variables which have not been cleaned up yet.
     */
    void cleanup();

    void notify(QEvent *event);

  private:
    std::string                   m_isServerName;
    IPCPartition                  m_partition;
    std::unique_ptr<ISInfoReceiver> m_isReceiver;
    // added to replace run/stop from ISInfoReceiver which no longer exists
    OWLSemaphore                  m_semaphore;

    QApplication                 *m_app;
    QObject                      *m_receiverObject;

    std::shared_ptr<CalibrationDataManager> m_calibrationData;

    std::map<std::string, std::set<std::string> >   m_receivedVariables;
    std::map<std::string, std::set<std::string> >   m_cleanupVariables;

    bool                          m_shutdown;
    bool                          m_running;
    Flag                          m_hasPartition;
    Flag                          m_connected;
    Flag                          m_exit;
    Mutex                         m_receiverLock;
  };

}
#endif
