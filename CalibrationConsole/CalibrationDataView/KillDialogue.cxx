#include "KillDialogue.h"
#include <qmessagebox.h>

#include <unistd.h>
#include <signal.h>

#include <iostream>

namespace PixCon {

  KillDialogue::KillDialogue(QWidget *object, const std::string &name, int timeout_in_sec)
    : QObject(object),
      m_timer(object),
      m_name(name),
      m_timeout((timeout_in_sec>1e9/1000) ? timeout_in_sec : timeout_in_sec*1000),
      m_startTime(::time(0))

  {
    connect(&m_timer,SIGNAL( timeout() ),this,SLOT( offerKillOption() ) );
    if (m_timeout>0) {
      std::cout << "INFO [KillDialogue::ctor] Will offer dialogue to send kill signal in " << m_timeout<< " msecs." << std::endl;
      m_timer.setSingleShot(true);
      m_timer.start((m_timeout > 0) ? m_timeout : 0);
    }
  }

  KillDialogue::~KillDialogue() {
    std::cout << "INFO [KillDialogue::dtor] Time this dialogue was kept alive=" << (::time(0)-m_startTime) << std::endl;
  }

  void KillDialogue::offerKillOption() {
    int ret = QMessageBox::warning(static_cast<QWidget *>(parent()),
				   (std::string("Kill ")+m_name).c_str(),
				   (m_name+" still trying to shutdown.\n"
				   +"This takes a lot of time !\n"
				   +"Shall the "+m_name+" be killed violently (first SIGINT then SIGKILL) ?\n"
				   +"It is suggest to select NO and give the system a bit more time to \n"
				    +"shutdown cleanly. When selecting NO You will be asked again.").c_str(),
				   "Wait for ever",
				   "No",
				   "Kill",
				   1,
				   1);
    switch (ret) {
    case 2: {
      // send kill signal to console
      kill(getpid(), SIGINT);
      sleep(30);
      kill(getpid(), SIGKILL);
      break;
    }
    case 0: {
      break;
    }
    case 1:
    default: {
      std::cout << "INFO [KillDialogue::offerKillOption] again in " << m_timeout << std::endl;
      m_timer.setSingleShot(true);
      m_timer.start((m_timeout > 0) ? m_timeout : 0);
      break;
    }
    }
  }
}
