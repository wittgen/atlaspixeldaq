/* Dear emacs, this is -*-c++-*- */
#ifndef _TimedValue_H_
#define _TimedValue_H_

namespace PixCon {

  typedef unsigned int TimeType_t;

  class TimedValueBase {
  public:
    static void ExceptionTimeBaseTooLate(TimeType_t time, TimeType_t base);
  };

  template <class T>
  class TimedValue : protected TimedValueBase
  {
  public:
    TimedValue() : m_time(0), m_value(0) {}

    TimedValue(TimeType_t base, TimeType_t time, T value)
      : m_time(static_cast<unsigned short>(time-base)), m_value(value)
    {}

    /** Change the time with respect to which seconds are counted.
     * @param old_base the old time base
     * @param new_base the new time base
     */
    void rebase(TimeType_t old_base, TimeType_t new_base) {
      TimeType_t temp = m_time;
      temp += old_base;
      temp -= new_base;
      m_time = static_cast<unsigned short>(temp);
    }

    T value()                        const { return m_value; }
    TimeType_t time(TimeType_t base) const { return base+m_time; }
    void setValue(TimeType_t base, TimeType_t time, T value) { 
      if (time<base) {
	ExceptionTimeBaseTooLate(time,base);
      }
      m_time =static_cast<unsigned short>(time-base);
    }
    

  protected:
  private:
    unsigned short m_time; // in seconds since a certain base time
    T              m_value;
  };

}
#endif
