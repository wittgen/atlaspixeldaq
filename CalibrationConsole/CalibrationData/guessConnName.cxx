#include <string>
#include <vector>
#include <ConfigWrapper/Regex_t.h>
#include <ConfigWrapper/Connectivity.h>
#include "guessConnName.h"

namespace PixCon {

  class IConnNameCorrector {
  public:
    virtual ~IConnNameCorrector() {}
    virtual std::string correctConnName(const PixA::ConnectivityRef &conn, const std::string &incomplete_conn_name) const = 0;
  };

  class NoConnNameCorrection : public IConnNameCorrector {
  public:
    std::string correctConnName(const PixA::ConnectivityRef &, const std::string &incomplete_conn_name) const { return incomplete_conn_name;}
  };

  class AddRodHeader :public IConnNameCorrector {
  public:
    std::string correctConnName(const PixA::ConnectivityRef &, const std::string &incomplete_conn_name) const { 
      if (incomplete_conn_name.size()>0) {
	return std::string("ROD_")+incomplete_conn_name;
      }
      return incomplete_conn_name;
    }
  };

  class AddModuleSideExtension :public IConnNameCorrector {
  public:
    std::string correctConnName(const PixA::ConnectivityRef &, const std::string &incomplete_conn_name) const {
	//std::cout << "here in loop conn correction a " << std::endl; 
      if (incomplete_conn_name.size()>0) {
	if (incomplete_conn_name[0]=='L') {
	  std::string::size_type module_ext = incomplete_conn_name.rfind("_");
	  if (module_ext>2 && (incomplete_conn_name[module_ext-1]=='6' || incomplete_conn_name[module_ext-1]=='7')
	      && (incomplete_conn_name[module_ext-2]=='A' || incomplete_conn_name[module_ext-2]=='C')) {
	    return incomplete_conn_name+incomplete_conn_name[module_ext-2];
	  }
	}
      }
      return incomplete_conn_name;
    }
  };

  class AddModuleStaveExtension :public IConnNameCorrector {
  public:

    std::string correctConnName(const PixA::ConnectivityRef &conn, const std::string &incomplete_conn_name) const { 
	//std::cout << "here in loop conn correction b " << std::endl;
      if (incomplete_conn_name.size()>0 && conn) {
	if (incomplete_conn_name[0]=='L') {
	  std::string::size_type module_ext = incomplete_conn_name.rfind("_");
	  if (incomplete_conn_name[incomplete_conn_name.size()-1]=='0'
	      && module_ext>2 && module_ext+2<incomplete_conn_name.size() && incomplete_conn_name[module_ext+1]=='M') {
	    char side[2]={'A','C'};
	    for (unsigned int side_i=0; side_i<2; side_i++) {
	      std::stringstream module_name;
	      module_name << incomplete_conn_name.substr(0,module_ext) 
			  << side[side_i]
			  << 7
			  << "_"
			  << incomplete_conn_name.substr(module_ext+1, incomplete_conn_name.size()-module_ext-1);
	      if (conn.findModule(module_name.str())) {
		return module_name.str();
	      }
	    }
	  }
	  else if (module_ext>2 && module_ext+3<incomplete_conn_name.size() && incomplete_conn_name[module_ext+1]=='M'
	      && isdigit(incomplete_conn_name[module_ext+2])
	      && (incomplete_conn_name[module_ext+3]=='A' || incomplete_conn_name[module_ext+3]=='C')) {
	    for (unsigned int n_modules=6; n_modules<=7; ++n_modules) {
	      std::stringstream module_name;
	      module_name << incomplete_conn_name.substr(0,module_ext) 
			  << incomplete_conn_name[module_ext+3]
			  << n_modules
			  << "_"
			  << incomplete_conn_name.substr(module_ext+1, incomplete_conn_name.size()-module_ext-1);
	      if (conn.findModule(module_name.str())) {
		return module_name.str();
	      }
	    }
	  }
	}
      }
      return incomplete_conn_name;
    }
  };

  std::vector< std::pair<PixA::Regex_t *, IConnNameCorrector *> > s_connPattern;

  void guessConnNameInit()
  {
    if (s_connPattern.empty()) {
      IConnNameCorrector *no_conn_name_corrector = new NoConnNameCorrection;
      s_connPattern.push_back(std::make_pair( new PixA::Regex_t("^D[123][AC]_B[0-9][0-9]_S[12](|_M[1-6])$"), no_conn_name_corrector));
      s_connPattern.push_back(std::make_pair( new PixA::Regex_t("^L[012]_B[0-9][0-9]_S[12]_[AC][67](|_M[1-6][AC])$"), no_conn_name_corrector));
      s_connPattern.push_back(std::make_pair( new PixA::Regex_t("^L[012]_B[0-9][0-9]_S[12]_[AC][67]_M0$"), no_conn_name_corrector));
      s_connPattern.push_back(std::make_pair( new PixA::Regex_t("^LI_S[01][0-9]_[AC](|_M[1-4]_[AC][1-8])$"), no_conn_name_corrector));
      s_connPattern.push_back(std::make_pair( new PixA::Regex_t("^LI_S[0-9][0-9]_[AC]_[13][24]_M[1-4]_[AC]([1-9]|1[0-2])$"), no_conn_name_corrector));
      //for LX_ dummy
      s_connPattern.push_back(std::make_pair( new PixA::Regex_t("^LX_B[0-9][0-9]_S[12]_[AC][67](|_M[1-6][AC])$"), no_conn_name_corrector));
      s_connPattern.push_back(std::make_pair( new PixA::Regex_t("^LX_B[0-9][0-9]_S[12]_[AC][67]_M0$"), no_conn_name_corrector));
      s_connPattern.push_back(std::make_pair( new PixA::Regex_t("^ROD_[IBCDL][0-4]_S([5-9]|1[0-9]|2[0-1])$"), no_conn_name_corrector));

      s_connPattern.push_back(std::make_pair( new PixA::Regex_t("^[IBCDL][0-4]_S([5-9]|1[0-9]|2[0-1])$"), new AddRodHeader));
 
      s_connPattern.push_back(std::make_pair( new PixA::Regex_t("^L[012]_B[0-9][0-9]_S[12]_[AC][67]_M[1-6]$"), new AddModuleSideExtension));

      IConnNameCorrector *stave_extension_corrector = new AddModuleStaveExtension;
      s_connPattern.push_back(std::make_pair( new PixA::Regex_t("^L[012]_B[0-9][0-9]_S[12]_M[1-6][AC]$"), stave_extension_corrector));
      s_connPattern.push_back(std::make_pair( new PixA::Regex_t("^L[012]_B[0-9][0-9]_S[12]_M0$"), stave_extension_corrector));
    }
  }

  std::pair<std::string,std::string::size_type> guessConnName(PixA::ConnectivityRef &conn, const std::string &name, std::string::size_type pos)
  {
    if (s_connPattern.empty()) {
      guessConnNameInit();
    }

    while (pos<name.size()) {
      while (pos<name.size() && isspace(name[pos])) pos++;
      std::string::size_type end_pos=pos;
      while ( end_pos < name.size() && (name[end_pos]=='_' || !ispunct(name[end_pos])) ) end_pos++;
      if (end_pos==std::string::npos) {
	end_pos=name.size();
      }

      std::string sub_name;
      for (std::string::size_type a_pos=pos; a_pos < end_pos; ++a_pos) {
	sub_name += toupper(name[a_pos]);
      }

      for (std::vector<std::pair<PixA::Regex_t *, IConnNameCorrector *> >::const_iterator pattern_iter = s_connPattern.begin();
	   pattern_iter != s_connPattern.end();
	   ++pattern_iter) {
	if ((pattern_iter->first)->match(sub_name)) {
	  std::string corrected_name = pattern_iter->second->correctConnName(conn, sub_name);
	  return std::make_pair(corrected_name, end_pos+1);
	}
	pos = end_pos+1;
      }
    }
    return std::make_pair("", name.size());
  }
}
