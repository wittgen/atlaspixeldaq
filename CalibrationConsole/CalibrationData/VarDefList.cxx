#include <VarDefList.h>

namespace PixCon {
  EValueConnType s_connItemToConnValueType[kNConnItemTypes] = {kModuleValue, kNValueConnTypes /* Pp0 */, kRodValue, kNValueConnTypes /* crate */};

  void VarDefList::checkId(VarId_t id) {
    if (id>=m_varName.size()) {
      throw UndefinedCalibrationVariable("Invalid Variable");
    }
  }

}
