#ifndef _PixCon_CategoryList_h_
#define _PixCon_CategoryList_h_

#include <map>
#include <memory>

#include <string>
#include <memory>
#include "CalibrationDataException.h"

namespace PixCon {

  /** Convenience which provides a list of categories of which each contains a list of variable names.
   */
  class CategoryList
  {
  public:

    /** Exception thrown if a category does not exist.
     */
    class MissingCategory : public CalibrationDataException
    {
    public:
      MissingCategory(const std::string &message) : CalibrationDataException(message) {};
    };

    class Category {
    public:
      Category(const std::shared_ptr< std::vector<std::string > > &a_category) : m_category(a_category) {}
      const std::string &addVariable(const std::string &var_name) {m_category->push_back(var_name); return var_name; }

      std::vector<std::string>::const_iterator begin() const { return m_category->begin(); }
      std::vector<std::string>::const_iterator end()   const { return m_category->end(); }

    private:
      std::shared_ptr< std::vector<std::string> > m_category;
    };

    CategoryList() {}

    /** Add a category.
     * @throw FatalCalibrationDataException if adding fails.
     */
    Category addCategory(const std::string &category_name) {
      std::map<std::string, std::shared_ptr< std::vector< std::string > >  >::iterator category_iter = m_categoryList.find(category_name);
      if (category_iter == m_categoryList.end()) {
	std::pair< std::map<std::string, std::shared_ptr< std::vector< std::string > >  >::iterator, bool> ret =
	  m_categoryList.insert(std::make_pair(category_name, std::make_shared< std::vector<std::string> > (  )));
	if (!ret.second) {
	  std::stringstream message;
	  message << "FATAL [CategoryList::Category] Failed to add category " << category_name << ".";
	  throw FatalCalibrationDataException(message.str());
	}
	category_iter = ret.first;
      }
      return category_iter->second;
    }


    /** Return the catogory of the given name.
     * @throw MissingCalibrationDataCategory if the category does not exist.
     */
    const Category category(const std::string &category_name) const {
      std::map<std::string, std::shared_ptr< std::vector< std::string > >  >::const_iterator category_iter = m_categoryList.find(category_name);
      if (category_iter == m_categoryList.end()) {
	std::stringstream message;
	message << "FATAL [CategoryList::Category] Category " << category_name << " not found.";
	throw MissingCategory(message.str());
      }
      return Category(category_iter->second);
    }

    /** Return true if the given category already exists.
     */
    bool exists(const std::string &category_name) const {
      return m_categoryList.find(category_name) != m_categoryList.end();
    }

    std::map<std::string, std::shared_ptr< std::vector< std::string > > >::const_iterator  begin() const {return m_categoryList.begin(); }
    std::map<std::string, std::shared_ptr< std::vector< std::string > > >::const_iterator  end()   const {return m_categoryList.end(); }

  private:
    std::map<std::string, std::shared_ptr< std::vector< std::string > > > m_categoryList;
  };

}

#endif
