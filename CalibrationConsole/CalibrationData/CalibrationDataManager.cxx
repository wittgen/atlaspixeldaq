#include <PixConnectivity/RosConnectivity.h>
#include "VarDefList.h"
#include "CalibrationDataManager.h"

#include <ConfigWrapper/ConnectivityUtil.h>
#include <ConfigWrapper/PixDisableUtil.h>


#include "IConnItemSelection.h"

#include "CalibrationDataFunctorBase.h"

namespace PixCon {

  void CalibrationDataManager::setConnectivity(EMeasurementType measurement_type,
					       SerialNumber_t serial_number,
					       const PixA::ConnectivityRef &a_connectivity,
					       bool force_recreate)
  {
    assert(measurement_type < kNMeasurements);

    if (measurement_type != kCurrent ) {

      // If a connectivity was already stored for the scan then the connectivity must be the same.
      // So, nothing to do.
      //      if ( m_connectivity[measurement_type].find(serial_number) != m_connectivity[measurement_type].end()) {
	//	std::cout << "INFO [CalibrationDataManager::setConnectivity] connectivity exists already for " << measurement_type << " id " << serial_number << "." << std::endl;
	//	return;
	//      }

    }
    else {
      // paranoia check
      assert( serial_number==0 );
    }

    // only re-create measurement if connectivity tags have changed.
    // A changed confiuguration tag has no impact on the layout.
    {
    PixA::ConnectivityRef old_conn = m_connectivity[measurement_type][serial_number];
    m_connectivity[measurement_type][serial_number] = a_connectivity;

    // 
    bool re_create_measurement=true;
    if (old_conn && a_connectivity && !force_recreate) {
      re_create_measurement = false;
      PixA::IConnectivity::EConnTag tag_list[]={PixA::IConnectivity::kId,
						PixA::IConnectivity::kConn,
						PixA::IConnectivity::kAlias,
						PixA::IConnectivity::kPayload};
      for (unsigned int tag_i=0; tag_i<4; tag_i++) {
	if (old_conn.tag(tag_list[tag_i]) != a_connectivity.tag(tag_list[tag_i])) {
	  re_create_measurement = true;
	  break;
	}
      }
      if (!re_create_measurement) return;
    }
    }

    // now enable all objects which are in the new connectivity
    //    PixCon::CalibrationData &calibration_data = m_calibrationData;

    // remove all data stored for this measurement from memory
    //    m_calibrationData->remove(measurement_type, serial_number);
    m_calibrationData->createEmptyMeasurement(measurement_type, serial_number);
    if (!a_connectivity) return ;

    // and set bare data lists for each object insided the connectivity
    for (PixA::CrateList::const_iterator crate_iter = a_connectivity.crates().begin();
	 crate_iter != a_connectivity.crates().end();
	 ++crate_iter) {

      const PixLib::TimConnectivity *tim_conn = PixA::responsibleTim(*crate_iter);
      if (tim_conn) {
	m_calibrationData->setEnableState(PixA::connectivityName(tim_conn), measurement_type, serial_number, true,true,true,true);
      }

      PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
      PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
      for (PixA::RodList::const_iterator rod_iter = rod_begin;
	   rod_iter != rod_end;
	   ++rod_iter) {

	// mark rod as "contained in connectivity", enabled in configuration, "temporary enabled" (, parent enabled)
	m_calibrationData->setEnableState(PixA::connectivityName(rod_iter), measurement_type, serial_number, true,true,true,true);
	const PixLib::RobinConnectivity *robin_conn = PixA::robinConn(*rod_iter);
	if (robin_conn) {
	  // add robin name as tag
	  CalibrationData::ConnObjDataList conn_obj = m_calibrationData->createConnObjDataList(PixA::connectivityName(rod_iter));
	  conn_obj.addTag( PixA::connectivityName(robin_conn) ) ;
	}

	PixA::Pp0List::const_iterator pp0_begin = PixA::pp0Begin(rod_iter);
	PixA::Pp0List::const_iterator pp0_end = PixA::pp0End(rod_iter);
	for (PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
	     pp0_iter != pp0_end;
	     ++pp0_iter) {

	  PixA::ModuleList::const_iterator module_begin = PixA::modulesBegin(pp0_iter);
	  PixA::ModuleList::const_iterator module_end = PixA::modulesEnd(pp0_iter);
	  for (PixA::ModuleList::const_iterator module_iter = module_begin;
	       module_iter != module_end;
	       ++module_iter) {

	    // mark module as "contained in connectivity", enabled in configuration, "temporary enabled", parent (i.e. ROD) enabled.
	    m_calibrationData->setEnableState(PixA::connectivityName(module_iter), measurement_type, serial_number,true,true,true,true);
	    CalibrationData::ConnObjDataList conn_obj = m_calibrationData->createConnObjDataList(PixA::connectivityName(module_iter));
	    // add cooling loop and module production ID as tag
	    conn_obj.addTag( PixA::coolingLoopName( *pp0_iter) ) ;
	    conn_obj.addTag( PixA::productionName( module_iter) ) ;
	  }
	}
      }
    }
  }

  void CalibrationDataManager::operateOnObject( EConnItemType item_type,
						const std::string &conn_name,
						const std::pair<EMeasurementType,SerialNumber_t> &measurement,
						ICalibrationDataFunctor &functor,
						CalibrationData::EnableState_t new_enable_state,
						bool create)
  {
    if (functor.filter(item_type)) {
      try {

	PixCon::CalibrationData::ConnObjDataList data_list(  (create ?
							      calibrationData().createConnObjDataList( conn_name ) 
							      : calibrationData().getConnObjectData( conn_name )));
	PixCon::CalibrationData::EnableState_t is_enable_state(data_list.enableState(measurement.first, measurement.second));

	new_enable_state.setInConnectivity((item_type==kPp0Item)?true:is_enable_state.isInConnectivity());
	new_enable_state.setInConfiguration((item_type==kPp0Item)?true:is_enable_state.isInConfiguration());

	functor(item_type, conn_name, measurement, data_list, is_enable_state, new_enable_state);

      }
      catch( PixCon::CalibrationData::MissingConnObject &) {

	if (create) {

	  // mark rod as "contained in connectivity", enabled in configuration, "temporary enabled" (, parent enabled)
	  m_calibrationData->setEnableState(conn_name,
					    measurement.first,
					    measurement.second,
					    true          /* in connectivity  */,
					    true          /* in configuration */,
					    new_enable_state.isParentEnabled() /* parent enabled   */, 
					    new_enable_state.isTemporaryEnabled());

	  PixCon::CalibrationData::ConnObjDataList data_list( calibrationData().createConnObjDataList( conn_name ));
	  CalibrationData::EnableState_t is_enable_state( data_list.enableState(measurement.first, measurement.second) );

	  new_enable_state.setInConnectivity(is_enable_state.isInConnectivity());
	  new_enable_state.setInConfiguration(is_enable_state.isInConfiguration());

	  functor(item_type, conn_name, measurement, data_list, is_enable_state, new_enable_state);

	}
	else {

	  new_enable_state.setInConnectivity(false);
	  new_enable_state.setInConfiguration(false);

	  functor(item_type, conn_name, measurement, new_enable_state);

	}
      }
    }
  }

  /** Operate on each connectivity object.
   */
  void CalibrationDataManager::forEach(EMeasurementType measurement_type,
				       SerialNumber_t serial_number,
				       ICalibrationDataFunctor &functor,
				       bool create) {
    PixA::DisabledListRoot disabled_list_root;
    forEach(measurement_type, serial_number, disabled_list_root, functor, create);
  }

  void CalibrationDataManager::forEach(EMeasurementType measurement_type,
				       SerialNumber_t serial_number,
				       const PixA::DisabledListRoot &disabled_list_root,
				       ICalibrationDataFunctor &functor,
				       bool create)
  {
    assert( measurement_type < kNMeasurements);
    assert( serial_number==0 || measurement_type != kCurrent);

    std::pair<EMeasurementType, SerialNumber_t> measurement(std::make_pair(measurement_type, serial_number));

    PixA::ConnectivityRef conn( connectivity( measurement_type, serial_number) );
    if (conn) {
      try {

	// and set bare data lists for each object insided the connectivity
	for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
	     crate_iter != conn.crates().end();
	     ++crate_iter) {

	  bool crate_enabled = PixA::enabled( disabled_list_root, crate_iter );

	  const PixA::DisabledList rod_disabled_list( PixA::rodDisabledList( disabled_list_root, crate_iter) );

	  const PixLib::TimConnectivity *tim_conn = PixA::responsibleTim(*crate_iter);
	  if (tim_conn) {

	    CalibrationData::EnableState_t tim_enable_state;
	    tim_enable_state.setParentEnabled(crate_enabled);
	    tim_enable_state.setEnabled( PixA::timEnabled( rod_disabled_list, crate_iter ) );

	    operateOnObject(kTimItem, PixA::connectivityName(tim_conn), measurement, functor, tim_enable_state, create);

	  }


	  PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
	  PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
	  for (PixA::RodList::const_iterator rod_iter = rod_begin;
	       rod_iter != rod_end;
	       ++rod_iter) {

	    bool rod_enabled = PixA::enabled( rod_disabled_list, rod_iter );

	    {
	      CalibrationData::EnableState_t enable_state;
	      enable_state.setParentEnabled(crate_enabled);
	      enable_state.setEnabled( rod_enabled );

	      operateOnObject(kRodItem, PixA::connectivityName(rod_iter), measurement, functor, enable_state, create);
	    }

	    const PixA::DisabledList pp0_disabled_list( PixA::pp0DisabledList( rod_disabled_list, rod_iter) );

	    PixA::Pp0List::const_iterator pp0_begin = PixA::pp0Begin(rod_iter);
	    PixA::Pp0List::const_iterator pp0_end = PixA::pp0End(rod_iter);
	    for (PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
		 pp0_iter != pp0_end;
		 ++pp0_iter) {

	      bool pp0_enabled = PixA::enabled( pp0_disabled_list, pp0_iter );

	      {
		CalibrationData::EnableState_t enable_state;
		enable_state.setParentEnabled(rod_enabled && crate_enabled);
		enable_state.setEnabled( pp0_enabled );
		
		operateOnObject(kPp0Item, PixA::connectivityName(pp0_iter), measurement, functor, enable_state, create);
	      }

	      const PixA::DisabledList module_disabled_list(PixA::moduleDisabledList( pp0_disabled_list, pp0_iter) );

	      PixA::ModuleList::const_iterator module_begin = PixA::modulesBegin(pp0_iter);
	      PixA::ModuleList::const_iterator module_end = PixA::modulesEnd(pp0_iter);
	      for (PixA::ModuleList::const_iterator module_iter = module_begin;
		   module_iter != module_end;
		   ++module_iter) {

		bool mod_enabled = PixA::enabled( module_disabled_list, module_iter );
		{
		  CalibrationData::EnableState_t enable_state;
		  enable_state.setParentEnabled(rod_enabled && crate_enabled && pp0_enabled);
		  enable_state.setEnabled( mod_enabled );
		  
		  operateOnObject(kModuleItem, PixA::connectivityName(module_iter), measurement, functor, enable_state, create);
		}

	      }
	    }
	  }
	}
      }
      catch(...) {
      }
    }
  }

  class EnableOperator : public CalibrationDataFunctorAllItems
  {
  public:
    EnableOperator(CalibrationData &calibration_data)
      : m_calibrationData(&calibration_data)
    { assert(m_calibrationData); }

    void operator()( EConnItemType /*item_type*/,
		     const std::string &conn_name,
		     const std::pair<EMeasurementType,SerialNumber_t> &measurement,
		     PixCon::CalibrationData::ConnObjDataList &/*data_list*/,
		     CalibrationData::EnableState_t /*is_enable_state*/,
		     CalibrationData::EnableState_t new_enable_state) {
      setEnableState(conn_name, measurement, new_enable_state);
    }

    void operator()( EConnItemType /*item_type*/,
		     const std::string &conn_name,
		     const std::pair<EMeasurementType,SerialNumber_t> &measurement,
		     CalibrationData::EnableState_t new_enable_state) {
      setEnableState(conn_name, measurement, new_enable_state);
    }


    void setEnableState(const std::string &conn_name,
			const std::pair<EMeasurementType,SerialNumber_t> &measurement,
			CalibrationData::EnableState_t new_enable_state) 
    {
      m_calibrationData->setEnableState(conn_name,
					measurement.first,
					measurement.second,
					new_enable_state.isInConnectivity(),
					new_enable_state.isInConfiguration(),
					new_enable_state.isParentEnabled(),
					new_enable_state.isTemporaryEnabled());
    }

  private:
    CalibrationData *m_calibrationData;
  };


  void CalibrationDataManager::setEnable(EMeasurementType measurement_type,
					 SerialNumber_t serial_number,
					 const PixA::DisabledListRoot &disabled_list_root)
  {
    EnableOperator enable_operator(calibrationData());
    forEach(measurement_type, serial_number, disabled_list_root, enable_operator, true);
  }


  class CreateEnableStringOperator : public CalibrationDataFunctorAllItems
  {
  public:
    CreateEnableStringOperator(CalibrationData &calibration_data)
      : m_calibrationData(&calibration_data)
    { assert(m_calibrationData); 
      m_enableString=":";
    }

    void operator()( EConnItemType item_type,
		     const std::string &conn_name,
		     const std::pair<EMeasurementType,SerialNumber_t> &measurement,
		     PixCon::CalibrationData::ConnObjDataList &/*data_list*/,
		     CalibrationData::EnableState_t is_enable_state,
		     CalibrationData::EnableState_t /*new_enable_state*/) {
      if (is_enable_state.enabled()) {
	if (item_type==kRodItem) {
	  setModuleNames();
	  //	  m_enableString+=conn_name;
	  //	  m_enableString+="::";
	  m_rodName = conn_name;
	}
	else if (item_type==kModuleItem) {
	  if (!m_rodName.empty()) {
	    m_enableString+=m_rodName;
	    m_enableString+="::";
	    m_rodName="";
          }
	  m_moduleList.insert(conn_name);
	  //	  m_enableString+=conn_name;
	  //	  m_enableString+=":";
	}
      }
    }

    void operator()( EConnItemType /*item_type*/,
		     const std::string &/*conn_name*/,
		     const std::pair<EMeasurementType,SerialNumber_t> &/*measurement*/,
		     CalibrationData::EnableState_t /*new_enable_state*/) {
    }


    void setEnableState(const std::string &/*conn_name*/,
			const std::pair<EMeasurementType,SerialNumber_t> &/*measurement*/,
			CalibrationData::EnableState_t /*new_enable_state*/) 
    {
    }

    std::string enableString() {
      if (m_enableString.size()==1) {
	return "";
      }
      else {
	setModuleNames();
	return m_enableString;
      }
    }

  protected:

    void setModuleNames() {
      if (!m_moduleList.empty()) {
	for(std::set<std::string>::const_iterator module_iter =  m_moduleList.begin();
	    module_iter != m_moduleList.end();
	    ++module_iter) {
	  
	  m_enableString+=*module_iter;
	  m_enableString+=":";
	}
	m_moduleList.clear();
      }
    }
  private:
    CalibrationData *m_calibrationData;
    std::string m_enableString;
    std::string m_rodName;
    std::set<std::string> m_moduleList;
  };

  std::string CalibrationDataManager::enableString(EMeasurementType measurement_type,
						   SerialNumber_t serial_number)
  {
    PixA::DisabledListRoot empty_disabled_list_root;
    CreateEnableStringOperator create_enable_string_operator(calibrationData());
    forEach(measurement_type, serial_number, empty_disabled_list_root, create_enable_string_operator, true);
    return create_enable_string_operator.enableString();
  }

  class HasEnabledChilds {
  public:
    HasEnabledChilds() : m_hasEnabledChilds(false) {};
    bool hasEnabledChilds() const { return m_hasEnabledChilds; }
    void setHasEnabledChilds() { m_hasEnabledChilds=true; }
  private:
    bool m_hasEnabledChilds;
  };


  std::shared_ptr<PixLib::PixDisable>  CalibrationDataManager::getPixDisable(EMeasurementType measurement_type,
									       SerialNumber_t serial_number,
									       bool ignore_tim) {
    return getPixDisable(measurement_type, serial_number, EntireSelection(), ignore_tim,false);
  }

  std::shared_ptr<PixLib::PixDisable>  CalibrationDataManager::getPixDisable(EMeasurementType measurement_type,
									       SerialNumber_t serial_number,
									       const IConnItemSelection &selection,
									       bool ignore_tim,
                                                                               bool ignore_enable)
  {
    assert( measurement_type < kNMeasurements);
    assert( serial_number==0 || measurement_type != kCurrent);

    std::shared_ptr<PixLib::PixDisable> pix_disable;

    PixA::ConnectivityRef conn( connectivity( measurement_type, serial_number) );
    if (conn) {

      try {

	{
	  std::string disable_name("Disable-");
	  disable_name += makeSerialNumberName(measurement_type, serial_number);
	  pix_disable=std::make_shared<PixLib::PixDisable>( disable_name);
	}
	std::map<std::string, std::pair< std::vector<std::string>, HasEnabledChilds > > partition_info;

	// and set bare data lists for each object insided the connectivity
	for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
	     crate_iter != conn.crates().end();
	     ++crate_iter) {

	  std::vector<std::string> disabled_rods;
	  bool has_enabled_rods = false;


	  if (!ignore_tim) {
	  const PixLib::TimConnectivity *tim_conn = PixA::responsibleTim(*crate_iter);
	  if (tim_conn) {
	    try {
	      PixCon::CalibrationData::ConnObjDataList data_list( calibrationData().createConnObjDataList( PixA::connectivityName(tim_conn) ));
	      PixCon::CalibrationData::EnableState_t enable_state( data_list.enableState(measurement_type, serial_number) );

	      if ((enable_state.isInConfiguration() && enable_state.isInConnectivity())) {
		bool is_selected  = selection.isSelected(PixA::connectivityName(tim_conn));

		if (!is_selected  || (!ignore_enable && !enable_state.enabled())) {
		  disabled_rods.push_back(PixA::connectivityName(tim_conn));
		}
		else {
		  has_enabled_rods = true;
		}
	      }

	    }
	    catch( PixCon::CalibrationData::MissingConnObject &) {
	    }

	  }
	  }

	  PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
	  PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
	  for (PixA::RodList::const_iterator rod_iter = rod_begin;
	       rod_iter != rod_end;
	       ++rod_iter) {

	    {
	    PixCon::CalibrationData::EnableState_t enable_state;
	    enable_state.setInConnectivity(false);
	    try {
	      PixCon::CalibrationData::ConnObjDataList data_list( calibrationData().createConnObjDataList( PixA::connectivityName(rod_iter) ));
	      enable_state = data_list.enableState(measurement_type, serial_number);
	    }
	    catch( PixCon::CalibrationData::MissingConnObject &) {
	    }

	    if (!enable_state.isInConfiguration() || !enable_state.isInConnectivity()) continue;

	    bool rod_is_selected  = selection.isSelected(PixA::connectivityName(rod_iter));

	    if (rod_is_selected && (ignore_enable ||  enable_state.enabled())) {
	      has_enabled_rods=true;
	    }
	    else {
	      disabled_rods.push_back( PixA::connectivityName(rod_iter) );
	      continue;
	    }
	    }

	    std::vector<std::string> disabled_modules;
	    bool has_enabled_modules = false;

	    PixA::Pp0List::const_iterator pp0_begin = PixA::pp0Begin(rod_iter);
	    PixA::Pp0List::const_iterator pp0_end = PixA::pp0End(rod_iter);
	    for (PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
		 pp0_iter != pp0_end;
		 ++pp0_iter) {

	      bool pp0_is_selected  = selection.isSelected(PixA::connectivityName(pp0_iter));

	      PixA::ModuleList::const_iterator module_begin = PixA::modulesBegin(pp0_iter);
	      PixA::ModuleList::const_iterator module_end = PixA::modulesEnd(pp0_iter);
	      for (PixA::ModuleList::const_iterator module_iter = module_begin;
		   module_iter != module_end;
		   ++module_iter) {

		{
		  PixCon::CalibrationData::EnableState_t enable_state;
		  enable_state.setInConnectivity(false);

		  try {
		    PixCon::CalibrationData::ConnObjDataList data_list( calibrationData().getConnObjectData( PixA::connectivityName(module_iter) ));
		    enable_state = data_list.enableState(measurement_type, serial_number);
		  }
		  catch( PixCon::CalibrationData::MissingConnObject &) {
		  }

		  if (!enable_state.isInConfiguration() || !enable_state.isInConnectivity()) continue;

		  bool module_is_selected  = selection.isSelected(PixA::connectivityName(module_iter))  & pp0_is_selected;

		  if (module_is_selected && (ignore_enable || enable_state.enabled())) {
		    has_enabled_modules=true;
		  }
		  else {
		    disabled_modules.push_back( PixA::connectivityName(module_iter) );
		  }

		}

	      }
	    }

	    if (!has_enabled_modules) {
	      // if all modules of the rod are disabled
	      // just mark the entire ROD as disabled
	      disabled_rods.push_back( PixA::connectivityName(rod_iter) );
	    }
	    else {
	      // ... otherwise add all disabled modules to the disable
	      for (std::vector<std::string>::const_iterator module_iter = disabled_modules.begin();
		   module_iter != disabled_modules.end();
		   ++module_iter) {
		pix_disable->disable( *module_iter );
	      }
	    }
	  }

	  if (!has_enabled_rods) {
	    // if all RODs of the crate are disabled
	    // just mark the entire Crate as disabled

	    // only disable a crate if it contains RODs
	    // to avoid that the LTP crate appears in the disable list
	    if (rod_begin != rod_end) {
	      partition_info[ PixA::partitionName(crate_iter) ].first.push_back( PixA::connectivityName(crate_iter));
	    }
	  }
	  else {
	    // ... otherwise add all disabled modules to the disable
	    for (std::vector<std::string>::const_iterator rod_iter = disabled_rods.begin();
		 rod_iter != disabled_rods.end();
		 ++rod_iter) {
	      pix_disable->disable( *rod_iter );
	    }
	    partition_info[ PixA::partitionName(crate_iter) ].second.setHasEnabledChilds();

	  }
	}

	for (std::map<std::string, std::pair< std::vector<std::string>, HasEnabledChilds > >::const_iterator partition_iter =  partition_info.begin();
	     partition_iter != partition_info.end();
	     ++partition_iter) {

	  if (partition_iter->second.second.hasEnabledChilds() ) {
	    // if the partition has enabled crates
	    // disable the crates
	    for (std::vector<std::string>::const_iterator crate_iter = partition_iter->second.first.begin();
		 crate_iter != partition_iter->second.first.end();
		 ++crate_iter) {
	      pix_disable->disable( *crate_iter );
	    }
	  }
	  else {
	    // ... otherwise disable the entire partition
	    pix_disable->disable( partition_iter->first);
	  }
	}

      }
      catch(...) {
	std::cout << "FATAL [CalibrationDataManager::getPixDisable] Failed to create pix disable." << std::endl;
	pix_disable.reset();
      }
    }

    //    if (pix_disable) {
    //      pix_disable->config().dump(std::cout);
    //    }

    return pix_disable;
  }


  void CalibrationDataManager::setCrateEnable(const std::string &crate_conn_name, EMeasurementType measurement_type, SerialNumber_t serial_number, bool enabled)
  {
    assert( measurement_type < kNMeasurements);
    assert( serial_number==0 || measurement_type != kCurrent);

    PixA::ConnectivityRef conn( connectivity( measurement_type, serial_number) );
    if (conn) {
      try {
	bool enable_readout = false;
	if (conn.findCrate(crate_conn_name) != NULL){
	  enable_readout = conn.findCrate(crate_conn_name)->enableReadout;
	}
	PixA::RodList rod_list( conn.rods(crate_conn_name) );

	try {
	  PixCon::CalibrationData::ConnObjDataList data_list( calibrationData().createConnObjDataList(crate_conn_name ));
	  PixCon::CalibrationData::EnableState_t &enable_state = data_list.enableState(measurement_type, serial_number);
	  enable_state.setInConnectivity(enable_readout);
	  enable_state.setEnabled(enabled);
	}
	catch( PixCon::CalibrationData::MissingConnObject &) {
	  // everything must have been enabled otherwise no reference would be in the file.
	  calibrationData().setEnableState(crate_conn_name,
					   measurement_type,
					   serial_number,
					   enable_readout /*enabled_in_connectivity*/,
					   true /*enabled_in_configuration*/,
					   true /*parent_enabled */,
					   enabled /*temporary_enabled*/);
	}
	propagateCrateEnable(rod_list, measurement_type, serial_number, (enabled&enable_readout));
      }
      catch (PixA::ConfigException &) {
      }
    }

  }

  void CalibrationDataManager::setRodEnable(const std::string &rod_conn_name, EMeasurementType measurement_type, SerialNumber_t serial_number, bool enabled)
  {
    assert( measurement_type < kNMeasurements);
    assert( serial_number==0 || measurement_type != kCurrent);

    PixA::ConnectivityRef conn( connectivity( measurement_type, serial_number) );
    if (conn) {
      try {
	bool enable_readout = false;
	if (conn.findRod(rod_conn_name) != NULL) {
	  enable_readout = conn.findRod(rod_conn_name)->enableReadout;
	}
	PixA::Pp0List pp0_list( conn.pp0s(rod_conn_name) );

	try {
	  PixCon::CalibrationData::ConnObjDataList data_list( calibrationData().createConnObjDataList(rod_conn_name ));
	  PixCon::CalibrationData::EnableState_t &enable_state = data_list.enableState(measurement_type, serial_number);
	  enable_state.setInConnectivity(enable_readout);
	  enable_state.setEnabled(enabled);
	}
	catch( PixCon::CalibrationData::MissingConnObject &) {
	  // everything must have been enabled otherwise no reference would be in the file.
	  calibrationData().setEnableState(rod_conn_name,
					   measurement_type,
					   serial_number,
					   enable_readout /*enabled_in_connectivity*/,
					   true /*enabled_in_configuration*/,
					   true /*parent_enabled */,
					   enabled /*temporary_enabled*/);
	}
	propagateRodEnable(pp0_list, measurement_type, serial_number, (enabled&enable_readout));
      }
      catch (PixA::ConfigException &) {
      }
    }

  }

  //    void setPp0Enable(EMeasurementType measurement_type, SerialNumber_t serial_number, const std::string &pp0_conn_name, bool enabled);

  void CalibrationDataManager::setPp0Enable(const std::string &pp0_conn_name, EMeasurementType measurement_type, SerialNumber_t serial_number, bool enabled)
  {

    assert( measurement_type < kNMeasurements);
    assert( serial_number==0 || measurement_type != kCurrent);

    PixA::ConnectivityRef conn( connectivity( measurement_type, serial_number) );
    if (conn) {
      try {
	PixA::ModuleList module_list( conn.modules(pp0_conn_name) );
	propagatePp0Enable(module_list, measurement_type, serial_number, enabled);
      }
      catch (PixA::ConfigException &) {
      }
    }
    
  }


  void CalibrationDataManager::setModuleEnable(const std::string &module_conn_name, EMeasurementType measurement_type, SerialNumber_t serial_number, bool enabled)
  {
    assert( measurement_type < kNMeasurements);
    assert( serial_number==0 || measurement_type != kCurrent);

    PixA::ConnectivityRef conn( connectivity( measurement_type, serial_number) );
    if (conn) {
      bool enable_readout = false;
      if (conn.findModule(module_conn_name) != NULL) {
	enable_readout = conn.findModule(module_conn_name)->enableReadout;
      }
      try {
	PixCon::CalibrationData::ConnObjDataList data_list( calibrationData().createConnObjDataList(module_conn_name ));
	PixCon::CalibrationData::EnableState_t &enable_state = data_list.enableState(measurement_type, serial_number);
	enable_state.setInConnectivity(enable_readout);
	enable_state.setEnabled(enabled);
      }
      catch( PixCon::CalibrationData::MissingConnObject &) {
	// everything must have been enabled otherwise no reference would be in the file.
	calibrationData().setEnableState(module_conn_name,
					 measurement_type,
					 serial_number,
					 true /*enabled_in_connectivity*/,
					 true /*enabled_in_configuration*/,
					 true /*parent_enabled */,
					 enabled /*temporary_enabled*/);
      }
    }

  }

  void CalibrationDataManager::setCrateAllocated(const std::string &crate_conn_name, EMeasurementType measurement_type, SerialNumber_t serial_number, bool allocated)
  {
    assert( measurement_type < kNMeasurements);
    assert( serial_number==0 || measurement_type != kCurrent);

    PixA::ConnectivityRef conn( connectivity( measurement_type, serial_number) );
    if (conn) {
      try {
	bool enable_readout = false;
	if (conn.findCrate(crate_conn_name) != NULL){
	  enable_readout = conn.findCrate(crate_conn_name)->enableReadout;
	}
	PixA::RodList rod_list( conn.rods(crate_conn_name) );

	try {
	  PixCon::CalibrationData::ConnObjDataList data_list( calibrationData().createConnObjDataList(crate_conn_name ));
	  PixCon::CalibrationData::EnableState_t &enable_state = data_list.enableState(measurement_type, serial_number);
	  enable_state.setInConnectivity(enable_readout);
	  enable_state.setAllocated(allocated);
	}
	catch( PixCon::CalibrationData::MissingConnObject &) {
	  // everything must have been enabled otherwise no reference would be in the file.
	  calibrationData().setEnableState(crate_conn_name,
					   measurement_type,
					   serial_number,
					   enable_readout /*enabled_in_connectivity*/,
					   true /*enabled_in_configuration*/,
					   true /*parent_enabled */,
					   true /*temporary_enabled*/);
	  calibrationData().setAllocatedState(crate_conn_name,
					      measurement_type,
					      serial_number,
					      true,
					      allocated);
	}
	propagateCrateAllocated(rod_list, measurement_type, serial_number, (allocated&enable_readout));
      }
      catch (PixA::ConfigException &) {
      }
    }

  }

  void CalibrationDataManager::setRodAllocated(const std::string &rod_conn_name, EMeasurementType measurement_type, SerialNumber_t serial_number, bool allocated)
  {
    assert( measurement_type < kNMeasurements);
    assert( serial_number==0 || measurement_type != kCurrent);

    PixA::ConnectivityRef conn( connectivity( measurement_type, serial_number) );
    if (conn) {
      try {
	bool enable_readout = false;
	if (conn.findRod(rod_conn_name) != NULL) {
	  enable_readout = conn.findRod(rod_conn_name)->enableReadout;
	}
	PixA::Pp0List pp0_list( conn.pp0s(rod_conn_name) );

	try {
	  PixCon::CalibrationData::ConnObjDataList data_list( calibrationData().createConnObjDataList(rod_conn_name ));
	  PixCon::CalibrationData::EnableState_t &enable_state = data_list.enableState(measurement_type, serial_number);
	  enable_state.setInConnectivity(enable_readout);
	  enable_state.setAllocated(allocated);
	}
	catch( PixCon::CalibrationData::MissingConnObject &) {
	  // everything must have been enabled otherwise no reference would be in the file.
	  calibrationData().setEnableState(rod_conn_name,
					   measurement_type,
					   serial_number,
					   enable_readout /*enabled_in_connectivity*/,
					   true /*enabled_in_configuration*/,
					   true /*parent_enabled */,
					   true /*temporary_enabled*/);
	  calibrationData().setAllocatedState(rod_conn_name,
					      measurement_type,
					      serial_number,
					      true,
					      allocated);
	}
	propagateRodAllocated(pp0_list, measurement_type, serial_number, (allocated&enable_readout));
      }
      catch (PixA::ConfigException &) {
      }
    }

  }

  void CalibrationDataManager::setModuleAllocated(const std::string &module_conn_name, EMeasurementType measurement_type, SerialNumber_t serial_number, bool allocated)
  {
    assert( measurement_type < kNMeasurements);
    assert( serial_number==0 || measurement_type != kCurrent);

    PixA::ConnectivityRef conn( connectivity( measurement_type, serial_number) );
    if (conn) {
      bool enable_readout = false;
      if (conn.findModule(module_conn_name) != NULL) {
	enable_readout = conn.findModule(module_conn_name)->enableReadout;
      }     
      try {
	PixCon::CalibrationData::ConnObjDataList data_list( calibrationData().createConnObjDataList(module_conn_name ));
	PixCon::CalibrationData::EnableState_t &enable_state = data_list.enableState(measurement_type, serial_number);
	enable_state.setInConnectivity(enable_readout);
	enable_state.setAllocated(allocated);
      }
      catch( PixCon::CalibrationData::MissingConnObject &) {
	// everything must have been enabled otherwise no reference would be in the file.
	calibrationData().setEnableState(module_conn_name,
					 measurement_type,
					 serial_number,
					 enable_readout /*enabled_in_connectivity*/,
					 true /*enabled_in_configuration*/,
					 true /*parent_enabled */,
					 true /*temporary_enabled*/);
	calibrationData().setAllocatedState(module_conn_name,
					    measurement_type,
					    serial_number,
					    true,
					    allocated);
      }
    }

  }


  void CalibrationDataManager::propagateCrateEnable(const PixA::RodList &rod_list,
						    EMeasurementType measurement_type,
						    SerialNumber_t serial_number,
						    bool parent_enabled)
  {

    PixA::RodList::const_iterator rod_begin = rod_list.begin();
    PixA::RodList::const_iterator rod_end = rod_list.end();
    for (PixA::RodList::const_iterator rod_iter = rod_begin;
	 rod_iter != rod_end;
	 ++rod_iter) {

      bool rod_enabled;
      {
	PixCon::CalibrationData::ConnObjDataList data_list( calibrationData().createConnObjDataList(PixA::connectivityName(rod_iter)) );
	PixCon::CalibrationData::EnableState_t &enable_state = data_list.enableState(measurement_type, serial_number);
	bool old_enabled = enable_state.enabled();
	enable_state.setParentEnabled(parent_enabled);
	bool new_enabled = enable_state.enabled();
	if (old_enabled == new_enabled) continue;
	rod_enabled=new_enabled;
      }
      propagateRodEnable( PixA::Pp0List(const_cast<PixLib::RodBocConnectivity&>(**rod_iter)), measurement_type, serial_number, rod_enabled);
    }
  }


  void CalibrationDataManager::propagateRodEnable(const PixA::Pp0List &pp0_list,
						  EMeasurementType measurement_type,
						  SerialNumber_t serial_number,
						  bool parent_enabled)
  {
    PixA::Pp0List::const_iterator pp0_begin = pp0_list.begin();
    PixA::Pp0List::const_iterator pp0_end = pp0_list.end();

    for (PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
	 pp0_iter != pp0_end;
	 ++pp0_iter) {

      bool pp0_enabled = parent_enabled;
//       {
// 	PixCon::CalibrationData::ConnObjDataList data_list( calibrationData().createConnObjDataList(PixA::connectivityName(pp0_iter)) );
// 	PixCon::CalibrationData::EnableState_t &enable_state = data_list.enableState(measurement_type, serial_number);
// 	bool old_enabled = enable_state.enabled();
// 	enable_state.setParentEnabled(parent_enabled);
// 	bool new_enabled = enable_state.enabled();
// 	if (old_enabled == new_enabled) continue;
// 	pp0_enabled=new_enabled;
//       }
      propagatePp0Enable(PixA::ModuleList( const_cast<PixLib::Pp0Connectivity&>(**pp0_iter)), measurement_type, serial_number, pp0_enabled);
    }
  }


  void CalibrationDataManager::propagatePp0Enable(const PixA::ModuleList &module_list,
						  EMeasurementType measurement_type,
						  SerialNumber_t serial_number,
						  bool parent_enabled)
  {
    PixA::ModuleList::const_iterator module_begin = module_list.begin();
    PixA::ModuleList::const_iterator module_end = module_list.end();

    for (PixA::ModuleList::const_iterator module_iter = module_begin;
	 module_iter != module_end;
	 ++module_iter) {
      PixCon::CalibrationData::ConnObjDataList data_list( calibrationData().createConnObjDataList(PixA::connectivityName(module_iter)) );
      PixCon::CalibrationData::EnableState_t &enable_state = data_list.enableState(measurement_type, serial_number);
      enable_state.setParentEnabled(parent_enabled);
    }

  }


  void CalibrationDataManager::propagateCrateAllocated(const PixA::RodList &rod_list,
						       EMeasurementType measurement_type,
						       SerialNumber_t serial_number,
						       bool parent_allocated)
  {

    PixA::RodList::const_iterator rod_begin = rod_list.begin();
    PixA::RodList::const_iterator rod_end = rod_list.end();
    for (PixA::RodList::const_iterator rod_iter = rod_begin;
	 rod_iter != rod_end;
	 ++rod_iter) {

      bool rod_allocated;
      {
	PixCon::CalibrationData::ConnObjDataList data_list( calibrationData().createConnObjDataList(PixA::connectivityName(rod_iter)) );
	PixCon::CalibrationData::EnableState_t &enable_state = data_list.enableState(measurement_type, serial_number);
	bool old_enabled = enable_state.enabled();
	enable_state.setParentAllocated(parent_allocated);
	bool new_enabled = enable_state.enabled();
	if (old_enabled == new_enabled) continue;
	rod_allocated=new_enabled;
      }
      propagateRodAllocated( PixA::Pp0List(const_cast<PixLib::RodBocConnectivity&>(**rod_iter)), measurement_type, serial_number, rod_allocated);
    }
  }


  void CalibrationDataManager::propagateRodAllocated(const PixA::Pp0List &pp0_list,
						     EMeasurementType measurement_type,
						     SerialNumber_t serial_number,
						     bool parent_allocated)
  {
    PixA::Pp0List::const_iterator pp0_begin = pp0_list.begin();
    PixA::Pp0List::const_iterator pp0_end = pp0_list.end();

    for (PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
	 pp0_iter != pp0_end;
	 ++pp0_iter) {

      bool pp0_allocated = parent_allocated;
//       {
// 	PixCon::CalibrationData::ConnObjDataList data_list( calibrationData().createConnObjDataList(PixA::connectivityName(pp0_iter)) );
// 	PixCon::CalibrationData::EnableState_t &enable_state = data_list.enableState(measurement_type, serial_number);
// 	bool old_enabled = enable_state.enabled();
// 	enable_state.setParentEnabled(parent_enabled);
// 	bool new_enabled = enable_state.enabled();
// 	if (old_enabled == new_enabled) continue;
// 	pp0_enabled=new_enabled;
//       }
      propagatePp0Allocated(PixA::ModuleList( const_cast<PixLib::Pp0Connectivity&>(**pp0_iter)), measurement_type, serial_number, pp0_allocated);
    }
  }


  void CalibrationDataManager::propagatePp0Allocated(const PixA::ModuleList &module_list,
						     EMeasurementType measurement_type,
						     SerialNumber_t serial_number,
						     bool parent_allocated)
  {
    PixA::ModuleList::const_iterator module_begin = module_list.begin();
    PixA::ModuleList::const_iterator module_end = module_list.end();

    for (PixA::ModuleList::const_iterator module_iter = module_begin;
	 module_iter != module_end;
	 ++module_iter) {
      PixCon::CalibrationData::ConnObjDataList data_list( calibrationData().createConnObjDataList(PixA::connectivityName(module_iter)) );
      PixCon::CalibrationData::EnableState_t &enable_state = data_list.enableState(measurement_type, serial_number);
      enable_state.setParentAllocated(parent_allocated);
    }

  }


  void CalibrationDataManager::removeScan(SerialNumber_t scan_serial_number) {

    // remove connectivity from memory
    remove(kScan, scan_serial_number);

    m_calibrationData->remove(kScan, scan_serial_number);

    // remove meta data from memory
    // ... and all data of  associated analyses
    const ScanMetaData_t *scan_meta_data = m_scanMetaData.find(scan_serial_number);
    if (scan_meta_data) {
      for (std::vector<SerialNumber_t>::const_iterator analysis_iter = scan_meta_data->beginAnalysis();
	   analysis_iter != scan_meta_data->endAnalysis();
	   analysis_iter++) {
	remove(kAnalysis, *analysis_iter);
	m_calibrationData->remove(kAnalysis, *analysis_iter);
	m_analysisMetaData.removeMetaData(*analysis_iter);
      }
    }
    m_scanMetaData.removeMetaData(scan_serial_number);
  }

  void CalibrationDataManager::removeAnalysis(SerialNumber_t analysis_serial_number)
  {
      ScanMetaData_t *scan_meta_data = m_scanMetaData.find(scanSerialNumber(kAnalysis, analysis_serial_number));
      if (scan_meta_data) {
	scan_meta_data->removeAnalysis( analysis_serial_number );
      }

      remove(kAnalysis, analysis_serial_number);
      m_calibrationData->remove(kAnalysis, analysis_serial_number);
      m_analysisMetaData.removeMetaData(analysis_serial_number);

  }


  void CalibrationDataManager::removeScansUpTo(SerialNumber_t scan_serial_number) {

    // remove connectivities from memory
    removeUpTo(kScan, scan_serial_number);

    // remove calibration data from memory
    m_calibrationData->removeUpTo(kScan, scan_serial_number);

    // remove scan meta data from memory
    //  ... and remove all data of associated analyses
    ScanMetaData::iterator scan_iter = m_scanMetaData.begin();
    bool reached_end = (scan_iter == m_scanMetaData.end());
    while (!reached_end && scan_iter->first <= scan_serial_number) {

      ScanMetaData::iterator current_scan_iter = scan_iter++;
      reached_end = (scan_iter == m_scanMetaData.end());

      const ScanMetaData_t *scan_meta_data = m_scanMetaData.find(current_scan_iter->first);
      if (scan_meta_data) {
	// remove data of associated analyses
	for (std::vector<SerialNumber_t>::const_iterator analysis_iter = scan_meta_data->beginAnalysis();
	     analysis_iter != scan_meta_data->endAnalysis();
	     analysis_iter++) {
	  removeAnalysis(*analysis_iter);
	}
      }
      m_scanMetaData.erase(current_scan_iter);
    }

  }


  void CalibrationDataManager::addScanConfig(EMeasurementType measurement_type, SerialNumber_t serial_number,
					     const std::string &folder_name,
					     PixA::ConfigHandle &pix_scan_config)
  {
    if ( measurement_type== kScan ) {
      ScanMetaData_t *scan_meta_data = m_scanMetaData.find(serial_number);
      if (scan_meta_data) {
	scan_meta_data->addScanConfig(folder_name,pix_scan_config);
      }
      else {
	std::cout << "ERROR [CalibrationDataManager::addScanConfig] No scan meta data for " << makeSerialNumberString(measurement_type, serial_number)
		  << std::endl;
      }
    }
  }

  PixA::ConfigHandle CalibrationDataManager::scanConfig(EMeasurementType measurement_type,
							SerialNumber_t serial_number,
							const std::string &folder_name) 
  {
    if (measurement_type==kScan) {
      PixA::ConfigHandle a_handle( scanMetaDataCatalogue().metaData(serial_number).scanConfig(folder_name) );
      if (a_handle) {
	return a_handle;
      }
    }

    std::cout << "WARNING [CalibrationDataManager::scanConfig] No scan config for " 
	      << makeSerialNumberString(measurement_type, serial_number)
	      << " "
	      << folder_name
	      << "."
	      << std::endl;

    return PixA::ConfigHandle();
  }


  void CalibrationDataManager::updateStatus(EMeasurementType measurement_type, SerialNumber_t serial_number, State_t status)
  {
    if (status>=MetaData_t::kNStates) {
      status=MetaData_t::kUnknown;
    }
    switch (measurement_type) {
    case kScan: {
      ScanMetaData_t *scan_meta_data = m_scanMetaData.find(serial_number);
      if (scan_meta_data) {
	scan_meta_data->setStatus( status );
      }
      break;
    }
    case kAnalysis: {
      AnalysisMetaData_t *analysis_meta_data = m_analysisMetaData.find(serial_number);
      if (analysis_meta_data) {
	analysis_meta_data->setStatus( status );
      }
      break;
    }
    case kCurrent: {
      assert( serial_number == 0 );
      m_currentStatus=static_cast<MetaData_t::EStatus>(status);
    }
    case kNMeasurements: {
      // to make gcc happy
      break;
    }
    }
  }

}
