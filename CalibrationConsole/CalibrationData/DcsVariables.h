#ifndef _DcsVariables_H_
#define _DcsVariables_H_

#include <string>
#include <vector>
#include <cassert>
#include <map>

namespace PixCon {

  class DcsVariables
  {
  public:

    enum EVariable {
      HVON,
      HV_MON,
      IHV_MON,
      VDDD,
      VDDD_MON,
      WIENERD_MON,
      VDDA,
      VDDA_MON,
      WIENERA_MON,
      IDDD_MON,
      IDDA_MON,
      TMODULE,
      TOPTO,
      VISET,
      VISET_MON,
      IVISET_MON,
      VPIN,
      VPIN_MON,
      IPIN_MON,
      VVDC,
      VVDC_MON,
      IVDC_MON,
      FSM_STATE,
      NDcsVariables
    };

    enum EFsmState {
      kReady,
      kLvOn,
      kUndefined,
      kOff,
      kLockedOut,
      kUnknown,
      kNFsmStates
    };


    static std::string pattern(EVariable variable) {
      assert(variable<NDcsVariables);
      return s_connNameSeparator+s_dcsName[variable];
    }

    static const char *varName(EVariable variable) {
      assert(variable<NDcsVariables);
      return s_dcsName[variable];
    }

    static std::string makeIsPattern(EVariable variable) {
      assert(variable<NDcsVariables);
      std::string temp(s_any);
      return s_any + s_connNameSeparator+s_dcsName[variable];
    }

    static const char *getStateName(DcsVariables::EFsmState state);
    static DcsVariables::EFsmState getState(const std::string &state_name);

    static std::string makeIsName(const std::string &is_server_name, const std::string &conn_name, EVariable variable);
    static std::string makeRequestName(const std::string &conn_name, EVariable variable);


    static std::vector<EVariable>::const_iterator beginModuleVar() { return s_moduleVar.begin();}
    static std::vector<EVariable>::const_iterator endModuleVar()   { return s_moduleVar.end();}

    static std::vector<EVariable>::const_iterator beginPp0Var() { return s_pp0Var.begin();}
    static std::vector<EVariable>::const_iterator endPp0Var()   { return s_pp0Var.end();}

    static std::vector<EVariable>::const_iterator beginPp2Var() { return s_pp2Var.begin();}
    static std::vector<EVariable>::const_iterator endPp2Var()   { return s_pp2Var.end();}

    static bool init();
  private:

    static const char *s_dcsName[NDcsVariables];
    static const char *s_fsmStateName;
    static const char *s_fsmStatusPrefix;
    static const char *s_fsmStatusVarName;
    static const char *s_any;

    static const char *s_stateName[kNFsmStates];

    static const std::string s_connNameSeparator;
    static const char s_isServerNameSeparator;

    static std::vector<EVariable> s_moduleVar;
    static std::vector<EVariable> s_pp0Var;
    static std::vector<EVariable> s_pp2Var;

    static std::map<std::string, EFsmState> s_stateMap;

    static bool              s_init;
  };

}
#endif
