#ifndef _PixA_CalibrationDataItemFactory_H_
#define _PixA_CalibrationDataItemFactory_H_

#include <memory>

#include "IConnItemFactory.h"
#include <ConfigWrapper/Connectivity.h>
#include <QPolygon>

namespace PixCon {

  class CalibrationData;
  class DataSelection;
  class CalibrationDataPalette;

  class CalibrationDataItemFactory : public IConnItemFactory
  {
  public:
    CalibrationDataItemFactory(const PixA::ConnectivityRef &connectivity,
			       const std::shared_ptr<CalibrationData> &calibration_data,
			       const std::shared_ptr<DataSelection> &data_selection,
			       const std::shared_ptr<CalibrationDataPalette> &palette);

    ~CalibrationDataItemFactory();

    ConnItem *rodItem(const std::string &connectivity_name,
              const QRectF &rect);

    ConnItem *timItem(const std::string &connectivity_name,
              const QRectF &rect);

    ConnItem *pp0Item(const std::string &connectivity_name,
              const QRectF &rect);

    ConnItem *diskModuleItem(const std::string &module_connectivity_name,
                 unsigned int layer,
                 const QPolygon &pa);

    ConnItem *barrelModuleItem(const std::string &module_connectivity_name,
			       unsigned int layer,
                   const QRectF &rect);

  protected:
    /** Get the ROD name to which the module is connected.
     * @param module_connectivity_name the module name.
     * @return the ROD name to which the module is connected or the name of the module.
     */
    const std::string &getRODName(const std::string &module_connectivity_name) const;

    static ConnItem *updateStyle(ConnItem *an_item);
    
  private:
    PixA::ConnectivityRef                     m_connectivity;
    std::shared_ptr<CalibrationData>        m_calibrationData;
    std::shared_ptr<DataSelection>          m_dataSelection;
    std::shared_ptr<CalibrationDataPalette> m_palette;
    
  };

}
#endif
