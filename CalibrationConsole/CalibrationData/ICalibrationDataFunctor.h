/* Dear emacs, this is -*-c++-*- */
#ifndef _PixCon_ICalibrationDataFunctor_h_
#define _PixCon_ICalibrationDataFunctor_h_

#include "CalibrationData.h"
#include <ConnItemBase.h>
#include <utility>

namespace PixCon {

  /** Class to be called for each connectivity object in a certain connectivity.
   * Each object is : ROD, TIM, PP0, module.
   * By chosing an apropriate filter function the class will only be called for 
   * specific object types.
   */
  class ICalibrationDataFunctor
  {
  public:
    virtual ~ICalibrationDataFunctor() {};

    /** Return true if this object shall be called for the given type.
     */
    virtual bool filter(EConnItemType item_type) const = 0;

    /** Called for all connectivity objects which passed the filter and for which the given measurement exists.
     */
    virtual void operator()( EConnItemType item_type,
			     const std::string &conn_name,
			     const std::pair<EMeasurementType,SerialNumber_t> &measurement,
			     PixCon::CalibrationData::ConnObjDataList &data_list,
			     CalibrationData::EnableState_t is_enable_state,
			     CalibrationData::EnableState_t new_enable_state) = 0;

    /** Called for all connectivity objects which passed the filter and for which the given measurement does not exist.
     */
    virtual void operator()( EConnItemType item_type,
			     const std::string &conn_name,
			     const std::pair<EMeasurementType,SerialNumber_t> &measurement,
			     CalibrationData::EnableState_t new_enable_state) = 0;
  };

}
#endif
