#include "CalibrationDataItemFactory.h"
#include "CalibrationDataStyle.h"
#include "RodItem.h"
#include "BarrelModItem.h"
#include "DiskModItem.h"
#include <Pp0Item.h>

#include <ConfigWrapper/ConnectivityUtil.h>

namespace PixCon {

  CalibrationDataItemFactory::CalibrationDataItemFactory(const PixA::ConnectivityRef &connectivity,
							 const std::shared_ptr<CalibrationData> &calibration_data,
							 const std::shared_ptr<DataSelection> &data_selection,
							 const std::shared_ptr<CalibrationDataPalette> &palette) 
    : m_connectivity(connectivity),
      m_calibrationData(calibration_data),
      m_dataSelection(data_selection),
      m_palette(palette)
  {
  }

  CalibrationDataItemFactory::~CalibrationDataItemFactory() {}

  ConnItem *CalibrationDataItemFactory::rodItem(const std::string &connectivity_name,
                     const QRectF &rect)
  {
    return updateStyle(new RodItem(connectivity_name,
				   new CalibrationDataStyle(ModuleRODDataList::create(*m_calibrationData,
										      connectivity_name),
							    m_dataSelection,
							    m_palette,
							    0),
                   rect));
  }

  ConnItem *CalibrationDataItemFactory::timItem(const std::string &connectivity_name,
                     const QRectF &rect)
  {
    return updateStyle(new TimItem(connectivity_name,
				   new CalibrationDataStyle(ModuleRODDataList::create(*m_calibrationData,
										      connectivity_name),
							    m_dataSelection,
							    m_palette,
							    0),
                   rect));
  }

  ConnItem *CalibrationDataItemFactory::pp0Item(const std::string &connectivity_name,
                        const QRectF &rect) {
    return updateStyle(new Pp0Item(connectivity_name, rect));
  }


  ConnItem *CalibrationDataItemFactory::diskModuleItem(const std::string &module_connectivity_name,
					    unsigned int layer,
                        const  QPolygon &pa)
  {
    return updateStyle(new DiskModItem(module_connectivity_name, 
				       new CalibrationDataStyle(ModuleRODDataList::create(*m_calibrationData,
											  getRODName(module_connectivity_name),
											  module_connectivity_name),
								m_dataSelection,
								m_palette,
								layer),
                       pa));
  }

  ConnItem *CalibrationDataItemFactory::barrelModuleItem(const std::string &module_connectivity_name,
					      unsigned int layer,
                          const QRectF &rect)
  {
    return updateStyle(new BarrelModItem(module_connectivity_name,
					 new CalibrationDataStyle(ModuleRODDataList::create(*m_calibrationData,
											    getRODName(module_connectivity_name),
											    module_connectivity_name),
								  m_dataSelection,
								  m_palette,
								  layer),
                     rect));
  }

  const std::string &CalibrationDataItemFactory::getRODName(const std::string &module_connectivity_name) const {
    if (m_connectivity) {
      const PixLib::ModuleConnectivity *module = m_connectivity.findModule(module_connectivity_name);
      if (module) {
	
	const PixLib::RodBocConnectivity *rod = PixA::connectedRod(module);
	if (rod) {
	  return PixA::connectivityName(rod);
	}
      }
    }
    return module_connectivity_name;
  }

  ConnItem *CalibrationDataItemFactory::updateStyle(ConnItem *item) {
    try {
      item->updateStyle();
    }
    catch (CalibrationData::MissingValue &err) {
    }
    return item;
  }
  
}
