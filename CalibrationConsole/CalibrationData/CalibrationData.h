/* Dear emacs, this is -*-c++-*- */
#ifndef _PixCon_CalibrationData_h_
#define _PixCon_CalibrationData_h_

#include <set>
#include <map>
#include <vector>
#include <string>

//@todo is this the correct header to get min/max values for floats ?
#include <values.h>

#include <memory>

#include <cassert>
#include <sstream>

#include "CalibrationDataException.h"
#include "CalibrationDataTypes.h"
#include "VarDefList.h"
#include "ValueHistory.h"

#include <iostream>

namespace PixCon {

  enum EDefaultStates {
    kUnknown,
    kOff,
    kOn,
    kConfigured,
    kScanning,
    kAnalysing,
    kSuccess,
    kCloseToFailure,
    kFailure,
    kAlarm
  };

  class CalibrationData;

  class ModuleRODDataList;
  class CalibrationDataStyle;
  class CalibrationDataManager;
  class DataSelection;

  /** Container for status information or analysis values for connectivity objects.
   * May throw FatalCalibrationDataException in case of resource limitations, 
   * and @ref CalibrationData::MissingConnObject or @ref CalibrationData::MissingValue 
   * if the information does not exist in the container.
   */
  class CalibrationData
  {
    friend class CalibrationDataManager;
  private:

    template <class T>
    static EValueType getType();

    typedef std::map<VarId_t, float>   ValueMap_t;
    typedef std::map<VarId_t, ValueHistory<float> >   ValueHistoryMap_t;
    typedef std::map<VarId_t, State_t> StateMap_t;
    typedef std::map<VarId_t, ValueHistory<State_t> > StateHistoryMap_t;

  public:

    /** Return true if the measurement specifiers are correct.
     */
    static bool checkMeasurement(EMeasurementType measurement_type, SerialNumber_t serial_number) {
      if (measurement_type==kCurrent) {
	return serial_number==0;
      }
      else {
	return measurement_type<kNMeasurements && serial_number>0;
      }
    }

    /** Will be thrown if a value does not exist for a particular measurement and connectivity object.
     */
    class MissingValue : public CalibrationDataException
    {
    public:
      MissingValue(const std::string &message) : CalibrationDataException(message) {};
    };

    /** Will be thrown if a connectivity does not exist e.g. for a particular measurement.
     */
    class MissingConnObject : public MissingValue
    {
    public:
      MissingConnObject(const std::string &message) : MissingValue(message) {};
    };

    /** Interface to connectivity object enable bits.
     */
    class EnableState_t {
    public:
      enum EEnableBits {kInConnectivity, kInConfiguration, kParent, kTemporary, kParentAllocated, kAllocated, kNEnableBits};

      static const unsigned short s_allEnabled = ( (1<<kInConnectivity) | (1<<kInConfiguration) | (1<<kParent) | (1<<kTemporary) | 
						   (1<<kParentAllocated) | (1<<kAllocated));

      /** Default constructor will clear all enabled bits.
       * In particular, the flag is cleared, which marks whether an object is in the connectivity or not.
       * An object is enabled if it is present in the connectivity, if it is enabled
       * in the configuration, if its parent (e.g. ROD) is enabled and if its is temporily enabled.
       */
      EnableState_t(): m_enableBits(0) {
	setParentAllocated(true);
	setAllocated(true);
      }

      /** Return true if the connectivity object is enabled.
       */
      bool enabled() const {
	return (m_enableBits==s_allEnabled);
      }

      /** Return true if the object is in the connectivity.
       */
      bool isInConnectivity() const {
	return getBit(kInConnectivity);
      }

      /** Return true if the object is enabled in the configuration.
       */
      bool isInConfiguration() const {
	return getBit(kInConfiguration);
      }

      /** Return true if the object is also temporary enabled.
       */
      bool isTemporaryEnabled() const {
	return getBit(kTemporary);
      }

      /** Return true if the parent of an object is enabled.
       */
      bool isParentEnabled() const {
	return getBit(kParent);
      }

      /** Return true if the object is allocated.
       */
      bool isAllocated() const {
	return getBit(kAllocated);
      }

      /** Return true if the parent of an object is allocated.
       */
      bool isParentAllocated() const {
	return getBit(kParentAllocated);
      }

      /** Mark the connectivity object as present in the connectivity.
       */
      void setInConnectivity(bool enabled) {
	setBit(kInConnectivity,enabled);
      }

      /** Use this function to set the enable state this object has in the configuration.
       * @param enabled if false(true) the object will be marked as disabled(enabled) in the configuration
       * By default the object would be disabled in the configuration.
       */
      void setInConfiguration(bool enabled) {
	setBit(kInConfiguration,enabled);
      }

      /** Use this function if the parent object is disabled (enabled).
       * @param enabled if false (true) the parent object will be marked as disabled(enabled).
       * By default the parent object is considered to be disabled.
       */
      void setParentEnabled(bool enabled) {
	setBit(kParent,enabled);
      }

      /** Use this function to temporily enable/disable the object.
       * @param enabled if false(true) the  object is temporily disabled (enabled).
       * By default the object is considered to be disabled.
       */
      void setEnabled(bool enabled) {
	setBit(kTemporary,enabled);
      }

      void setAllocated(bool enabled) {
	setBit(kAllocated,enabled);
      }

      void setParentAllocated(bool enabled) {
	setBit(kParentAllocated,enabled);
      }

      EnableState_t &operator &=(const EnableState_t &a) {
	m_enableBits &= a.m_enableBits;
	return *this;
      }

      bool operator ==(const EnableState_t &a) const {
	return m_enableBits == a.m_enableBits;
      }

    private:
      void setBit(EEnableBits bit,bool value)  { (value ? setBit(bit) : clearBit(bit)); }
      void setBit(EEnableBits bit)             { m_enableBits |= (1<<bit) ; };
      void clearBit(EEnableBits bit)           { m_enableBits &= ~(1<<bit) ; };
      bool getBit(EEnableBits bit)   const     { return m_enableBits & (1<<bit) ; };

      unsigned short m_enableBits;
    };

    class DataList_t {
    private:

      ValueMap_t        m_valueList;
      ValueHistoryMap_t m_valueHistoryList;
      StateMap_t        m_stateList;
      StateHistoryMap_t m_stateHistoryList;
      EnableState_t     m_enableState;

    public:
      /** Get the enable state of connectivity object.
       */
      EnableState_t &enableState() {
	return m_enableState;
      }

      /** Get the enable state of connectivity object (read only).
       */
      const EnableState_t enableState() const {
	return m_enableState;
      }

      void reset() {
	m_valueList.clear();
	m_stateList.clear();
	m_valueHistoryList.clear();
	m_stateHistoryList.clear();
	m_enableState = EnableState_t();
      }

      /** Get the value map for a given type from the given data list.
       */
      template <class T>
      std::map<VarId_t, T> &getValueMap();

      /** Get the value map for a given type from the given data list (read only).
       */
      template <class T>
      const std::map<VarId_t, T> &getValueMap() const { return const_cast<DataList_t *>(this)->getValueMap<T>();}

      /** Add the list of all variables of the given type to the set.
       */
      template <class T>
      void getVarList(std::set<std::string> &var_list) const;

      /** Add the ids of all variables of the given type to the set.
       */
      template <class T>
      void getVarList(std::set<VarId_t> &var_list) const;
    };

    typedef std::map<SerialNumber_t, DataList_t > ConnObjMeasurementList_t;

    //    typedef std::vector< ConnObjMeasurementList_t >  ConnObjDataList_t;



  public:

    class ConnObjDataList_t : public std::vector< ConnObjMeasurementList_t >
    {
    private:
      void resize(std::vector< ConnObjMeasurementList_t >::size_type) {};
      void clear() {}
    public:
      ConnObjDataList_t()  {std::vector< ConnObjMeasurementList_t >::resize(kNMeasurements); }

      /** Add a tag to the list of tags assigned to this object.
       * The tag will only be added if it does not exist already in the list.
       */
      void addTag(const std::string &tag_name);

      /** Return the begin iterator of the tag list of this object.
       */
      std::vector<std::string>::const_iterator beginTags() const {
	return m_tags.begin();
      }

      /** Return the end iterator of the tag list of this object.
       */
      std::vector<std::string>::const_iterator endTags() const {
	return m_tags.end();
      }

      /** Return true if this object carries the given tag.
       */
      bool findTag(const std::string &a_tag) const;

    private:
      std::vector<std::string> m_tags;

    };

    class ConnObjDataList {
      friend class ModuleRODDataList;
      friend class CalibrationData;
      friend class CalibrationDataStyle;
      friend class DataSelection;

    public:
      ConnObjDataList(const ConnObjDataList_t &data_list) : m_dataList(&data_list) { assert(data_list.size()==kNMeasurements); }

      /** Get a value which was obtained during an analyisis from the data list of this connectivity object.
       * @param number the serial number of the analysis.
       * @param id the ID of the variable.
       * @return the value.
       * @throw May result in an exception MissingValue if no value of the given type and for the given serial number exists for this connectivity object.
       */
      template <class T>
      T analysisValue(SerialNumber_t number, VarId_t id) const {
	return value<T>(kAnalysis, number, id);
      }

      /** Add a value which was obtained during a scan to the data list of a connectivity object.
       * @param number the serial number of the scan.
       * @param id the ID of the variable.
       * @return the value.
       * @throw May result in an exception @ref MissingValue if no value of the given type and for the given serial number exists for this connectivity object.
       */
      template <class T>
      T scanValue(SerialNumber_t number, VarId_t id) const {
	return value<T>(kScan, number, id);
      }

      /** Add a current status or measurement to the data list of the given connectivity object.
       * @param id the ID of the variable.
       * @return the value.
       * @throw May result in an exception @ref MissingValue if no value of the given type and for the given serial number exists for this connectivity object.
       */
      template <class T>
      T currentValue(VarId_t id) const {
	return value<T>(kCurrent, 0, id);
      }

      /** Get the current enable state (read/write).
       */
      EnableState_t &currentEnableState() {
	return enableState(kCurrent,0);
      };

      /** Get the current enable state (read only).
       */
      const EnableState_t currentEnableState() const {
	return enableState(kCurrent,0);
      };

      /** Get the enable state of the given scan(read only).
       * @param scan_serial_number the serial number of the scan.
       */
      const EnableState_t enableStateOfScan(SerialNumber_t scan_serial_number) const {
	return enableState(kScan,scan_serial_number);
      };

      /** Get the enable state of the given analysis (read only).
       * @param analysis_serial_number the serial number of the analysis.
       */
      const EnableState_t enableStateOfAnalysis(SerialNumber_t analysis_serial_number) const {
	return enableState(kAnalysis, analysis_serial_number);
      };

      EnableState_t &enableState(EMeasurementType type, SerialNumber_t serial_number) {
	DataList_t &a_dataList = const_cast<DataList_t &>(getDataList(type, serial_number));
	return a_dataList.enableState();
      };

      const EnableState_t enableState(EMeasurementType type, SerialNumber_t serial_number) const {
	const DataList_t &a_dataList = getDataList(type, serial_number);
	return a_dataList.enableState();
      };

      /** Return the value of a certain variable or throw a runtime error if no value exists.
       * @param type the measurement type (scan, analysis or current).
       * @param serial_number the serial number of the analysis or the scan ( or zero).
       * @param id the ID of the variable.
       * @return the value.
       * @throw @ref MissingValue if no value exists for the given variable
       * There is also a method @ref newestValue which transparently handles values and values with 
       * history.
       */
      template <class T>
      T value(EMeasurementType type, SerialNumber_t serial_number, VarId_t id) const {
	const DataList_t &a_dataList = getDataList(type, serial_number);
	const std::map<VarId_t, T> &var_map = a_dataList.getValueMap<T>();
	typename std::map<VarId_t, T>::const_iterator var_iter = var_map.find(id);
	if (var_iter == var_map.end()) {
	  missingValue(type, serial_number, id);
	}
	return var_iter->second;
      }

      /** Return the newest value of a certain variable with history or throw a runtime error if no value exists.
       * @param type the measurement type (scan, analysis or current).
       * @param serial_number the serial number of the analysis or the scan ( or zero).
       * @param id the ID of the variable.
       * @return the value.
       * @throw @ref MissingValue if no value exists for the given variable
       */
      template <class T>
      T newestHistoryValue(EMeasurementType type, SerialNumber_t serial_number, VarId_t id) const {
	const DataList_t &a_dataList = getDataList(type, serial_number);
	const std::map<VarId_t, ValueHistory<T> > &var_map = a_dataList.getValueMap<ValueHistory<T> >();
	typename std::map<VarId_t, ValueHistory<T> >::const_iterator var_iter = var_map.find(id);
	if (var_iter == var_map.end()) {
	  missingValue(type, serial_number, id);
	}
	return var_iter->second.newest().value();
      }

      /** Return the value of a certain variable (or newest value in case the variable has a history) or throw a runtime error if no value exists.
       * @param type the measurement type (scan, analysis or current).
       * @param serial_number the serial number of the analysis or the scan ( or zero).
       * @param id the ID of the variable.
       * @return the value.
       * @throw @ref MissingValue if no value exists for the given variable
       */
      template <class T>
      T newestValue(EMeasurementType type, SerialNumber_t serial_number, const VarDef_t &var_def) const;

//       {
// 	const DataList_t &a_dataList = getDataList(type, serial_number);
// 	const std::map<VarId_t, ValueHistory<T> > &var_map = a_dataList.getValueMap<ValueHistory<T> >();
// 	typename std::map<VarId_t, ValueHistory<T> >::const_iterator var_iter = var_map.find(id);
// 	if (var_iter == var_map.end()) {
// 	  missingValue(type, serial_number, id);
// 	}
// 	return var_iter->second.newest().value();
//       }


      /** Return the value history of a certain variable or throw a runtime error if no such history exists exists.
       * @param type the measurement type (scan, analysis or current).
       * @param serial_number the serial number of the analysis or the scan ( or zero).
       * @param id the ID of the variable.
       * @return the value history.
       * The value history is an optional feature which is actived with the definition of a the variable
       * @throw @ref MissingValue if no value exists for the given variable
       */
      template <class T>
      const ValueHistory<T> &valueHistory(EMeasurementType type, SerialNumber_t serial_number, VarId_t id) const {
	const DataList_t &a_dataList = getDataList(type, serial_number);
	const std::map<VarId_t, ValueHistory<T> > &var_map = a_dataList.getValueMap<ValueHistory<T> >();
	typename std::map<VarId_t, ValueHistory<T> >::const_iterator var_iter = var_map.find(id);
	if (var_iter == var_map.end()) {
	  std::stringstream message;
	  message << "FATAL [ConnObjDataList::valueHistory<T>] no history for type " << type << " , serial number "
		  << serial_number << " and ID " << id << ".";
	  throw MissingValue(message.str());
	}
	return var_iter->second;
      }


      /** Remove the given variable from the data list;
       */
      template <class T>
      void removeVariable(EMeasurementType type, SerialNumber_t serial_number, VarId_t id) {
	DataList_t &a_dataList = getDataList(type, serial_number);
	std::map<VarId_t, T> &var_map = a_dataList.getValueMap<T>();
	typename std::map<VarId_t, T>::iterator var_iter = var_map.find(id);
	if (var_iter != var_map.end()) {
	  var_map.erase(var_iter);
	}
	else {
	  std::map<VarId_t, ValueHistory<T> > &var_history_map = a_dataList.getValueMap<ValueHistory<T> >();
	  typename std::map<VarId_t, ValueHistory<T> >::iterator var_history_iter = var_history_map.find(id);
	  
	  if (var_history_iter != var_history_map.end()) {
	    var_history_map.erase(var_history_iter);
	  }
	}
      }

      /** Add a value of the given type to the data list.
       * @param measurement type specifies whether the value is a current measurement (or status), was obtained in a scan or analysis.
       * @param number the serial number of the scan or analysis (or zero in case of a current measurement/status).
       * @param var_def the variable definition.
       * @param value the actual value.
       * @throw FatalCalibrationDataException if the value etc. cannot be added due to resource limits.
       */
      template <class T>
      void addValue(EMeasurementType measurement_type,
		    SerialNumber_t number,
		    const VarDef_t &var_def,
		    const T value) {
	addValueUnsafe(measurement_type,number,var_def,value);
      }

    /** Add a value of the given type to the data list.
     * @param measurement type specifies whether the value is a current measurement (or status), was obtained in a scan or analysis.
     * @param number the serial number of the scan or analysis (or zero in case of a current measurement/status).
     * @param var_def the definition of the variable.
     * @param time the time stamp of a value.
     * @param value the actual value.
     * The history has to be created before hand.
     * @throw FatalCalibrationDataException if the value etc. cannot be added due to resource limits.
     */
      template <class T>
      void addValue(EMeasurementType measurement_type,
		    SerialNumber_t number,
		    const VarDef_t &var_def,
		    TimeType_t time,
		    const T value) {
	addValueUnsafe(measurement_type,number,var_def,time,value);
      }


      /** Add a tag to the list of tags assigned to this object.
       * The tag will only be added if it does not exist already in the list.
       */
      void addTag(const std::string &tag_name) {
	assert(m_dataList);
	const_cast<ConnObjDataList_t *>(m_dataList)->addTag(tag_name);
      }

      /** Return the begin iterator of the tag list of this object.
       */
      std::vector<std::string>::const_iterator beginTags() const {
	assert(m_dataList);
	return m_dataList->beginTags();
      }

      /** Return the end iterator of the tag list of this object.
       */
      std::vector<std::string>::const_iterator endTags() const {
	assert(m_dataList);
	return m_dataList->endTags();
      }

      /** Return true if this object carries the given tag.
       */
      bool findTag(const std::string &a_tag) const {
	assert(m_dataList);
	return m_dataList->findTag(a_tag);
      }

    protected:
      /** Get the datalist for the given measurement.
       * @param type the type of the measurement (current, scan or analysis)
       * @param serial_number the serial number of the scan or the analysis or zero.
       * @throw @ref MissingConnObject if the connectivity object does not contain data for the given measurement.
       */
      const DataList_t &getDataList(EMeasurementType measurement_type, SerialNumber_t serial_number) const {
	assert( CalibrationData::checkMeasurement( measurement_type, serial_number) );
	ConnObjMeasurementList_t::const_iterator data_iter = (*m_dataList)[measurement_type].find(serial_number);
	if (data_iter == (*m_dataList)[measurement_type].end()) {
// 	  unsigned int counter=0;
// 	  for (ConnObjDataList_t::const_iterator iter = m_dataList->begin();
// 	       iter !=m_dataList->end();
// 	       iter++, counter++) {

// 	    for (ConnObjMeasurementList_t::const_iterator meas_iter = iter->begin();
// 		 meas_iter !=iter->end();
// 		 meas_iter++) {
// 	      std::cout << counter << ":" << meas_iter->first << std::endl;
// 	    }
// 	  }

	  std::stringstream message;
	  message << "FATAL [ConnObjDataList::getDataList] no data for type " << measurement_type << " and serial number " << serial_number << ".";
	  throw MissingConnObject(message.str());
	}
 	return data_iter->second;
      }

      DataList_t &getDataList(EMeasurementType measurement_type, SerialNumber_t serial_number)  {
	return const_cast<DataList_t &>(const_cast<const ConnObjDataList *>(this)->getDataList(measurement_type,serial_number));
      }

      /** Add a value of the given type to the data list.
       * @param measurement type specifies whether the value is a current measurement (or status), was obtained in a scan or analysis.
       * @param number the serial number of the scan or analysis (or zero in case of a current measurement/status).
       * @param var_def the variable definition.
       * @param value the actual value.
       * @throw FatalCalibrationDataException if the value etc. cannot be added due to resource limits.
       */
      template <class T>
      void addValueUnsafe(EMeasurementType measurement_type,
			  SerialNumber_t number,
			  const VarDef_t &var_def,
			  const T value) {
	DataList_t &a_dataList = getDataList(measurement_type,number);
	std::map<VarId_t, T> &var_map = a_dataList.getValueMap<T>();
	var_map[var_def.id()]=value;
      }


    /** Add a value of the given type to the data list.
     * @param measurement type specifies whether the value is a current measurement (or status), was obtained in a scan or analysis.
     * @param number the serial number of the scan or analysis (or zero in case of a current measurement/status).
     * @param var_def the definition of the variable.
     * @param time the time stamp of a value.
     * @param value the actual value.
     * The history has to be created before hand.
     * @throw FatalCalibrationDataException if the value etc. cannot be added due to resource limits.
     */
    template <class T>
    void addValueUnsafe(EMeasurementType measurement_type,
			SerialNumber_t number,
			const VarDef_t &var_def,
			TimeType_t time, 
			const T value);


      /** Return true if this and the given connectivity object data list point to the same connectivity object.
       */
      bool operator==(const ConnObjDataList &a) const {
       return (m_dataList == a.m_dataList);
      }

      /** Handle values with history
       */
      template <class T> T handleValueHistory(const EMeasurementType type,
					      const SerialNumber_t serial_number,
					      const VarId_t id,
					      const DataList_t &a_dataList) const;


    private:

      void missingValue(const EMeasurementType type,
			const SerialNumber_t serial_number,
			const VarId_t id) const;

      const ConnObjDataList_t *m_dataList;
    };


    CalibrationData() {}
    ~CalibrationData() {}

    /** Get the Id of the given variable.
     * @param var_name the name of the variable
     * @return the ID of the variable
     * @throw FatalCalibrationDataException if the variable does not exist and could not be added to the list of defined variables.
     */
    const VarDef_t getVarDef(const std::string &var_name) const {
      return VarDefList::instance()->getVarDef(var_name);
    }

    void varList(EMeasurementType measurement_type, SerialNumber_t serial_number, std::set<std::string> &var_list_out);

    /** Add a value which was obtained during an analyisis to the data list of a connectivity object.
     * @param conn_name the name of the connectivity object to which this property should be associated.
     * @param number the serial number of the analysis.
     * @param value_name the name of the variable.
     * @param value the actual value.
     */
    template <class T>
    void addAnalysisValue(EValueConnType conn_type,
			  const std::string &conn_name,
			  SerialNumber_t number,
			  const std::string &value_name,
			  const T value) {
      addValueUnsafe(conn_type, conn_name, kAnalysis, number, value_name, value);
    }

    /** Add a value which was obtained during a scan to the data list of a connectivity object.
     * @param conn_name the name of the connectivity object to which this property should be associated.
     * @param number the serial number of the scan.
     * @param value_name the name of the variable.
     * @param value the actual value.
     */
    template <class T>
    void addScanValue(EValueConnType conn_type,
		      const std::string &conn_name,
		      SerialNumber_t number,
		      const std::string &value_name,
		      const T value) {
      addValueUnsafe(conn_type, conn_name, kScan, number, value_name, value);
    }

    /** Add a current status or measurement to the data list of the given connectivity object.
     * @param conn_name the name of the connectivity object to which this property should be associated.
     * @param value_name the name of the variable.
     * @param value the actual value.
     */
    template <class T>
    void addCurrentValue(EValueConnType conn_type,
			 const std::string &conn_name,
			 const std::string &value_name,
			 const T value) {
      addValueUnsafe(conn_type, conn_name, kCurrent, 0, value_name, value);
    }

    /** Add a current status or measurement to the data list of the given connectivity object.
     * @param conn_name the name of the connectivity object to which this property should be associated.
     * @param measurement_type the measurement type : scan, analysis, current;
     * @param number the serial number of the scan.
     * @param value_name the name of the variable.
     * @param value the actual value.
     */
    template <class T>
    void addValue(EValueConnType conn_type,
		  const std::string &conn_name,
		  EMeasurementType measurement_type,
		  SerialNumber_t serial_number,
		  const std::string &value_name,
		  const T value) {
      assert( checkMeasurement(measurement_type, serial_number) );
      addValueUnsafe(conn_type, conn_name, measurement_type, serial_number, value_name, value);
    }

    /** Add a current status or measurement to the data list of the given connectivity object.
     * @param conn_name the name of the connectivity object to which this property should be associated.
     * @param measurement_type the measurement type : scan, analysis, current;
     * @param number the serial number of the scan.
     * @param var_def the definition of the variable.
     * @param value the actual value.
     */
    template <class T>
    void addValue(const std::string &conn_name,
		  EMeasurementType measurement_type,
		  SerialNumber_t serial_number,
		  const VarDef_t &var_def,
		  const T value) {
      assert( checkMeasurement(measurement_type, serial_number) );
      addValueUnsafe(conn_name, measurement_type, serial_number, var_def, value);
    }

    /** Add a status or measurement to the history of the data list of the given connectivity object.
     * @param conn_name the name of the connectivity object to which this property should be associated.
     * @param measurement_type the measurement type : scan, analysis, current;
     * @param number the serial number of the scan.
     * @param var_def the definition of the variable.
     * @param time the time stampe of the value.
     * @param value the actual value.
     */
    template <class T>
    void addValue(const std::string &conn_name,
		  EMeasurementType measurement_type,
		  SerialNumber_t serial_number,
		  const VarDef_t &var_def,
		  TimeType_t time,
		  const T value) {
      assert( checkMeasurement(measurement_type, serial_number) );
      addValueUnsafe(conn_name, measurement_type, serial_number, var_def, time, value);
    }



    /** Toggle the current, temporary enabled state of the given connectivity object.
     * @param conn_name the name of the connectivity object.
     * @param temporary_enable if the object is temporary enabled.
     * NOTE: will not change the state of its childs if any.
     */
    void setEnabled(const std::string &conn_name,
		    bool temporary_enabled=true)
    {
      DataList_t &a_dataList = createDataList(conn_name,kCurrent,0);
      EnableState_t &enable_state = a_dataList.enableState();

      enable_state.setEnabled(temporary_enabled);
    }

    /** Toggle the current, parent enabled state of the given connectivity object.
     * @param conn_name the name of the connectivity object.
     * @param parent_enabled if the parent of the object is enabled.
     * NOTE: will not change the state of its childs if any.
     */
    void setParentEnabled(const std::string &conn_name,
			  bool parent_enabled=true)
    {
      DataList_t &a_dataList = createDataList(conn_name,kCurrent,0);
      EnableState_t &enable_state = a_dataList.enableState();

      enable_state.setParentEnabled(parent_enabled);
    }


    /** Set the current enabled stati of a connectivity object.
     * @param conn_name the name of the connectivity object.
     * @param enabled_in_connectivity true if the connectivity object is in the connnectivity.
     * @param enabled_in_configuration true if the connectivity object is enabled in the configuration.
     * @param parent_enabled true if the parent of the connectivity object is enabled.
     * @param temporary_enabled if the object is temporary enabled.
     * NOTE: will not automatically change the state of its childs if any.
     */
    void setCurrentEnableState(const std::string &conn_name,
			       bool enabled_in_connectivity,
			       bool enabled_in_configuration,
			       bool parent_enabled=true,
			       bool temporary_enabled=true)
    {
      setEnableStateUnsafe(conn_name,
			   kCurrent,
			   0,
			   enabled_in_connectivity,
			   enabled_in_configuration,
			   parent_enabled,
			   temporary_enabled);
    }

    /** Set the current enabled stati of a connectivity object.
     * @param conn_name the name of the connectivity object.
     * @param number the serial number of the scan or analysis (or zero in case of a current measurement/status).
     * @param enabled_in_connectivity true if the connectivity object is in the connnectivity.
     * @param enabled_in_configuration true if the connectivity object is enabled in the configuration.
     * @param parent_enabled true if the parent of the connectivity object is enabled.
     * @param temporary_enabled if the object is temporary enabled.
     * NOTE: will not automatically change the state of its childs if any.
     */
    void setScanEnableState(const std::string &conn_name,
			    SerialNumber_t serial_number,
			    bool enabled_in_connectivity,
			    bool enabled_in_configuration,
			    bool parent_enabled=true,
			    bool temporary_enabled=true)
    {
      setEnableStateUnsafe(conn_name,
			   kScan,
			   serial_number,
			   enabled_in_connectivity,
			   enabled_in_configuration,
			   parent_enabled,
			   temporary_enabled);
    }

    /** Set the enabled stati of a connectivity object.
     * @param conn_name the name of the connectivity object.
     * @param measurement type specifies whether the value is a current measurement (or status), was obtained in a scan or analysis.
     * @param number the serial number of the scan or analysis (or zero in case of a current measurement/status).
     * @param enabled_in_connectivity true if the connectivity object is in the connnectivity.
     * @param enabled_in_configuration true if the connectivity object is enabled in the configuration.
     * @param parent_enabled true if the parent of the connectivity object is enabled.
     * @param temporary_enabled true if the object is temporary enabled.
     * NOTE: will not automatically change the state of its childs if any.
     * @throw FatalCalibrationDataException if no data list exist for the specified measurement and connectivity object and cannot be created.
     */
    void setEnableState(const std::string &conn_name,
			EMeasurementType measurement_type,
			SerialNumber_t serial_number,
			bool enabled_in_connectivity,
			bool enabled_in_configuration,
			bool parent_enabled=true,
			bool temporary_enabled=true)
    {
      assert( checkMeasurement(measurement_type, serial_number) );
      setEnableStateUnsafe(conn_name,
			   measurement_type,
			   serial_number,
			   enabled_in_connectivity,
			   enabled_in_configuration,
			   parent_enabled,
			   temporary_enabled);
    }

    /** Set the allocated status of a connectivity object.
     * @param conn_name the name of the connectivity object.
     * @param measurement type specifies whether the value is a current measurement (or status), was obtained in a scan or analysis.
     * @param number the serial number of the scan or analysis (or zero in case of a current measurement/status).
     * @param parent_allocated true if the parent of the connectivity object is allocated.
     * @param allocated true if the object is allocated.
     * NOTE: will not automatically change the state of its childs if any.
     * @throw FatalCalibrationDataException if no data list exist for the specified measurement and connectivity object and cannot be created.
     */
    void setAllocatedState(const std::string &conn_name,
			   EMeasurementType measurement_type,
			   SerialNumber_t serial_number,
			   bool parent_allocated,
			   bool allocated)
    {
      assert( checkMeasurement(measurement_type, serial_number) );

      DataList_t &a_dataList = createDataList(conn_name, measurement_type, serial_number);
      EnableState_t &enable_state = a_dataList.enableState();

      enable_state.setParentAllocated(parent_allocated);
      enable_state.setAllocated(allocated);
    }

    /** Copy the current enabled state to a scan.
     * @param conn_name the name of the connectivity object.
     * @param src_measurement type specifies whether the value is a current measurement (or status), was obtained in a scan or analysis.
     * @param src_number the serial number of the scan or analysis (or zero in case of a current measurement/status).
     * @param dest_measurement type specifies whether the value is a current measurement (or status), was obtained in a scan or analysis.
     * @param dest_number the serial number of the scan or analysis (or zero in case of a current measurement/status).
     * NOTE: will not automatically change the state of its childs if any.
     */
    void copyCurrentEnableStateToScan(SerialNumber_t dest_scan_serial_number)
    {
      copyEnableState(kCurrent,0, kScan, dest_scan_serial_number);
    }

    /** Copy the enabled states of a scan to another scan.
     * @param src_measurement type specifies whether the value is a current measurement (or status), was obtained in a scan or analysis.
     * @param src_number the serial number of the scan or analysis (or zero in case of a current measurement/status).
     * @param dest_measurement type specifies whether the value is a current measurement (or status), was obtained in a scan or analysis.
     * @param dest_number the serial number of the scan or analysis (or zero in case of a current measurement/status).
     * NOTE: will not automatically change the state of its childs if any.
     */
    void copyScanEnableStateToScan(SerialNumber_t   src_scan_serial_number,
			     SerialNumber_t   dest_scan_serial_number)
    {
      copyEnableState(kScan,src_scan_serial_number, kScan, dest_scan_serial_number);
    }

    /** Copy the enabled states of scan to an analysis.
     * @param src_measurement type specifies whether the value is a current measurement (or status), was obtained in a scan or analysis.
     * @param src_number the serial number of the scan or analysis (or zero in case of a current measurement/status).
     * @param dest_measurement type specifies whether the value is a current measurement (or status), was obtained in a scan or analysis.
     * @param dest_number the serial number of the scan or analysis (or zero in case of a current measurement/status).
     * NOTE: will not automatically change the state of its childs if any.
     */
    void copyScanEnableStateToAnalysis(SerialNumber_t   src_scan_serial_number,
				       SerialNumber_t   dest_analysis_serial_number)
    {
      copyEnableState(kScan,src_scan_serial_number, kAnalysis, dest_analysis_serial_number);
    }


    /** Reset the current value lists and the enable state.
     * NOTE: will not automatically change the state of its childs if any.
     */
    void resetCurrent()
    {
      reset(kCurrent,0);
    }


    /** Remove all data for the given scan.
     */
    void removeScan(SerialNumber_t scan_serial_number)
    {
      remove(kScan,scan_serial_number);
    }

    /** Remove all data for the given analysis.
     */
    void removeAnalysis(SerialNumber_t analysis_serial_number)
    {
      remove(kAnalysis,analysis_serial_number);
    }

    /** Remove all data for the given scan and of prior scans.
     */
    void removeScanUpTo(SerialNumber_t scan_serial_number)
    {
      removeUpTo(kScan,scan_serial_number);
    }

    /** Remove all data for the given analysis and prior analyses.
     */
    void removeAnalysisUpTo(SerialNumber_t analysis_serial_number)
    {
      removeUpTo(kAnalysis,analysis_serial_number);
    }

    /** Get the data list for the specified connectivity object.
     * @param conn_name the name of the connectivity object.
     * @return the data list of the given connectivity object.
     * @throw MissingConnObject if the connectivity object does not exist.
     * getConnObjectData should become deprecated
     */
    const ConnObjDataList getConnObjectData(const std::string &conn_name) const {
      return getConnObjDataList(conn_name);
    }

    /** Get the data list for the specified connectivity object.
     * @param conn_name the name of the connectivity object.
     * @return the data list of the given connectivity object.
     * @throw MissingConnObject if the connectivity object does not exist.
     */
    const ConnObjDataList getConnObjDataList(const std::string &conn_name) const  {
      std::map<std::string, ConnObjDataList_t >::const_iterator data_list_iter =m_connObjDataList.find(conn_name);
      if (data_list_iter == m_connObjDataList.end()) {
	std::stringstream message;
	message << "FATAL [CalibrationData::getConnObjectData] No data list for connectivity object " << conn_name << ".";
	throw MissingConnObject(message.str());
      }
      return data_list_iter->second;
    }

    std::map<std::string, ConnObjDataList_t >::const_iterator begin() const { return m_connObjDataList.begin(); }
    std::map<std::string, ConnObjDataList_t >::const_iterator end()   const { return m_connObjDataList.end(); };

    ConnObjDataList_t &createConnObjDataList(const std::string &conn_name) {
      return createEmptyConnObjDataList(conn_name)->second;
    }

    /** Remove a certain variable from a measurement.
     * @param measurement_type the measurement.
     * @param serial_number ther serial number of the measurement.
     * @param var_id the id of the variable which should be removed.
     */
    template <class T> 
    void removeVariable(EMeasurementType measurement_type,
			SerialNumber_t   serial_number,
			VarId_t var_id) {
      for (std::map<std::string, ConnObjDataList_t >::iterator conn_iter = m_connObjDataList.begin(); 
	   conn_iter != m_connObjDataList.end();
	   ++conn_iter) {
	try {
	  ConnObjDataList(conn_iter->second).removeVariable<T>(measurement_type, serial_number, var_id);
	}
	catch (MissingConnObject &) {
	}
      }
    }

    /** Reset the current enable state of all connectivity objects.
     * After this operation, all connectivity objects will be disabled in the connectivity, configuration 
     * will be temporarily disabled and will have its parent disabled in the current measurement.
     */
    void resetCurrentEnableState();

    /** Create empty data lists for the given measurement and for all connectivity objects.
     * @param measurement_type type specifies whether the value is a current measurement (or status), was obtained in a scan or analysis.
     * @param serial_number the serial number of the scan or analysis (or zero in case of a current measurement/status).
     * NOTE: will not automatically change the state of its childs if any.
     */
    void createEmptyMeasurement(EMeasurementType measurement_type,
				SerialNumber_t   serial_number);

  protected:
    /** Remove and reset all values stored for the given measurement.
     * @param measurement type specifies whether the value is a current measurement (or status), was obtained in a scan or analysis.
     * @param number the serial number of the scan or analysis (or zero in case of a current measurement/status).
     * NOTE: will not automatically change/adapt the state of its childs if any.
     */
    void remove(EMeasurementType measurement_type,
		SerialNumber_t   serial_number);

    /** Remove and reset all values stored for the given measurement and all prior measurements of the same type (i.e. scan or analysis).
     * @param measurement type specifies whether the value is a current measurement (or status), was obtained in a scan or analysis.
     * @param number the serial number of the scan or analysis (or zero in case of a current measurement/status).
     * NOTE: will not automatically change/adapt the state of its childs if any.
     */
    void removeUpTo(EMeasurementType measurement_type,
		    SerialNumber_t   serial_number);

    /** Return true if the measurement type and the serial number are valid.
     */
    static bool isValid(EMeasurementType measurement_type, SerialNumber_t serial_number) 
    {
      return (measurement_type < kNMeasurements && (measurement_type!=kCurrent || serial_number==0));
    }

  private:

//     std::map<std::string, std::vector<std::string > >       m_category;        /**< List of categories, which contains a list of variable names each*/

    std::map<std::string, ConnObjDataList_t >                  m_connObjDataList; /**< List of data lists per connectivity object.*/

    /** Return the container for an existing connectivity object or create an empty container.
     * @return iterator to an eventually empty container for the given connectivity object.
     * @throw FatalCalibrationDataException if the connectivity object does not exist and cannot be added.
     */
    std::map<std::string, ConnObjDataList_t>::iterator createEmptyConnObjDataList(const std::string &conn_name) {
      std::map<std::string, ConnObjDataList_t>::iterator conn_iter = m_connObjDataList.find(conn_name);
      if (conn_iter == m_connObjDataList.end()) {
	std::pair<std::map<std::string, ConnObjDataList_t>::iterator, bool> ret=
	  m_connObjDataList.insert(std::make_pair(conn_name, ConnObjDataList_t()));
	if (!ret.second) {
	  std::stringstream message;
	  message << "FATAL [CalibrationData::createDataList] Failed to create data list for connectivity object " << conn_name << ".";
	  throw FatalCalibrationDataException(message.str());
	}
	//	ret.first->second.resize(kNMeasurements);
	conn_iter= ret.first;
      }
      return conn_iter;
    }

    /** Get an existing or create a new data list for the given connectivity object and measurement.
     * @throw FatalCalibrationDataException if the datalist does not exist and cannot be created due to strange reasons.
     */
    DataList_t &createDataList(const std::string &conn_name, EMeasurementType type, SerialNumber_t serial_number) {
      std::map<std::string, ConnObjDataList_t>::iterator conn_iter = createEmptyConnObjDataList(conn_name);
      return createDataList(conn_iter, type, serial_number);
    }

    /** Create a data list for the given connectivity measurement, 
     * @throw FatalCalibrationDataException if the datalist does not exist and cannot be created due to strange reasons.
     */
    DataList_t &createDataList(std::map<std::string, ConnObjDataList_t>::iterator conn_iter, EMeasurementType type, SerialNumber_t serial_number)
    {
      ConnObjMeasurementList_t &measurement_list = (conn_iter->second)[type];
      ConnObjMeasurementList_t::iterator measurement_iter = measurement_list.find(serial_number);

      if (measurement_iter == measurement_list.end()) {
	std::pair<ConnObjMeasurementList_t::iterator, bool> ret=
	  measurement_list.insert(std::make_pair(serial_number, DataList_t()));
	if (!ret.second) {
	  std::stringstream message;
	  message << "FATAL [CalibrationData::createDataList] Failed to insert data list for connectivity object " << conn_iter->first
		  << ", type " << type << ", serial number " << serial_number << " .";
	  throw FatalCalibrationDataException(message.str());
	}
	measurement_iter= ret.first;
      }
      return measurement_iter->second;

    }

    /** Add a value of the given type to the data list.
     * @param conn_name the name of the connectivity object to which this property should be associated.
     * @param measurement type specifies whether the value is a current measurement (or status), was obtained in a scan or analysis.
     * @param number the serial number of the scan or analysis (or zero in case of a current measurement/status).
     * @param value_name the name of the variable.
     * @param value the actual value.
     * @throw FatalCalibrationDataException if the value etc. cannot be added due to resource limits.
     */
    template <class T>
    void addValueUnsafe(EValueConnType conn_type,
			const std::string &conn_name,
			EMeasurementType measurement_type,
			SerialNumber_t number,
			const std::string &value_name,
			const T value) {
      VarDef_t var_def=getVarDef(value_name, conn_type, getType<T>());
      DataList_t &a_dataList = createDataList(conn_name,measurement_type,number);
      std::map<VarId_t, T> &var_map = a_dataList.getValueMap<T>();
      var_map[var_def.id()]=value;
    }

    /** Add a value of the given type to the data list.
     * @param conn_name the name of the connectivity object to which this property should be associated.
     * @param measurement type specifies whether the value is a current measurement (or status), was obtained in a scan or analysis.
     * @param number the serial number of the scan or analysis (or zero in case of a current measurement/status).
     * @param var_def the definition of the variable.
     * @param value the actual value.
     * @throw FatalCalibrationDataException if the value etc. cannot be added due to resource limits.
     */
    template <class T>
    void addValueUnsafe(const std::string &conn_name,
			EMeasurementType measurement_type,
			SerialNumber_t number,
			const VarDef_t &var_def,
			const T value) {
      assert( VarDefList::instance()->isValid(var_def.id()) );
      assert( var_def.valueType() == getType<T>() );
      DataList_t &a_dataList = createDataList(conn_name,measurement_type,number);
      std::map<VarId_t, T> &var_map = a_dataList.getValueMap<T>();
      var_map[var_def.id()]=value;
    }

    /** Add a value of the given type to the data list.
     * @param conn_name the name of the connectivity object to which this property should be associated.
     * @param measurement type specifies whether the value is a current measurement (or status), was obtained in a scan or analysis.
     * @param number the serial number of the scan or analysis (or zero in case of a current measurement/status).
     * @param var_def the definition of the variable.
     * @param time the time stamp of a value.
     * @param value the actual value.
     * The history has to be created before hand.
     * @throw FatalCalibrationDataException if the value etc. cannot be added due to resource limits.
     */
    template <class T>
    void addValueUnsafe(const std::string &conn_name,
			EMeasurementType measurement_type,
			SerialNumber_t number,
			const VarDef_t &var_def,
			TimeType_t time, 
			const T value);

    /** Get an Id for the given variable name.
     * @param var_name the name of the variable
     * @param type the type of the variable which will be associated to the variable name if the variable is not yet registered.
     * @throw FatalCalibrationDataException if the variable does not exist and cannot be defined due to resource limits.
     */
    const VarDef_t getVarDef(const std::string &var_name, EValueConnType conn_type, EValueType value_type) {
      const VarDef_t a=VarDefList::instance()->getVarDef(var_name,conn_type,value_type);
      if (a.valueType() != value_type ) {
	errorTypeMismatch(a, value_type);
      }
      //      assert( a.valueType() == value_type );
      assert( a.connType() == conn_type || a.connType()==kAllConnTypes);
      return a;
    }

    /** Set the enabled stati of a connectivity object.
     * @param conn_name the name of the connectivity object.
     * @param measurement type specifies whether the value is a current measurement (or status), was obtained in a scan or analysis.
     * @param number the serial number of the scan or analysis (or zero in case of a current measurement/status).
     * @param enabled_in_connectivity true if the connectivity object is in the connnectivity.
     * @param enabled_in_configuration true if the connectivity object is enabled in the configuration.
     * @param parent_enabled true if the parent of the connectivity object is enabled.
     * @param temporary_enabled true if the object is temporary enabled.
     * NOTE: will not automatically change the state of its childs if any.
     * @throw FatalCalibrationDataException if no data list exist for the specified measurement and connectivity object and cannot be created.
     */
    void setEnableStateUnsafe(const std::string &conn_name,
			      EMeasurementType measurement_type,
			      SerialNumber_t number,
			      bool enabled_in_connectivity,
			      bool enabled_in_configuration,
			      bool parent_enabled=true,
			      bool temporary_enabled=true)
    {
      DataList_t &a_dataList = createDataList(conn_name,measurement_type,number);
      EnableState_t &enable_state = a_dataList.enableState();

      enable_state.setInConnectivity(enabled_in_connectivity);
      enable_state.setInConfiguration(enabled_in_configuration);
      enable_state.setParentEnabled(parent_enabled);
      enable_state.setEnabled(temporary_enabled);
    }

    /** Remove and reset all values stored for the given measurement.
     * @param measurement type specifies whether the value is a current measurement (or status), was obtained in a scan or analysis.
     * @param number the serial number of the scan or analysis (or zero in case of a current measurement/status).
     * NOTE: will not automatically change/adapt the state of its childs if any.
     */
    void reset(EMeasurementType measurement_type,
	       SerialNumber_t   serial_number);

    /** Copy the enabled states.
     * @param src_measurement type specifies whether the value is a current measurement (or status), was obtained in a scan or analysis.
     * @param src_number the serial number of the scan or analysis (or zero in case of a current measurement/status).
     * @param dest_measurement type specifies whether the value is a current measurement (or status), was obtained in a scan or analysis.
     * @param dest_number the serial number of the scan or analysis (or zero in case of a current measurement/status).
     * NOTE: will not automatically change the state of its childs if any.
     */
    void copyEnableState(EMeasurementType src_measurement_type,
			 SerialNumber_t   src_number,
			 EMeasurementType dest_measurement_type,
			 SerialNumber_t   dest_number);

    void errorTypeMismatch(VarDef_t var_def, EValueType requested_type);

  };


  template <>
    inline EValueType CalibrationData::getType<State_t>() {return kState;}

  template <>
  inline std::map<VarId_t, State_t> &CalibrationData::DataList_t::getValueMap() { return m_stateList; }

  template <>
  inline EValueType CalibrationData::getType<ValueHistory<State_t> >() {return kTimedState;}

  template <>
  inline std::map<VarId_t, ValueHistory<State_t> > &CalibrationData::DataList_t::getValueMap() { return m_stateHistoryList; }


  template <>
  inline EValueType CalibrationData::getType<float>() {return kValue;}

  template <>
  inline std::map<VarId_t, float> &CalibrationData::DataList_t::getValueMap() { return m_valueList; }


  template <>
  inline EValueType CalibrationData::getType<ValueHistory<float> >() {return kTimedValue;}

  template <>
  inline std::map<VarId_t, ValueHistory<float> > &CalibrationData::DataList_t::getValueMap() { return m_valueHistoryList; }


  inline CalibrationData::EnableState_t operator&(const CalibrationData::EnableState_t &a, const CalibrationData::EnableState_t &b) {
    CalibrationData::EnableState_t temp(a);
    temp &= b;
    return temp;
  }

  class ModuleRODDataList {
    friend class CalibrationDataStyle;
  public:
    /** A datalist for a ROD.
     * @param calibration_data the calibration data container.
     * @param rod_name the ROD connectivity name of the ROD to which the module is connected.
     * @param module_name the module connectivity name.
     */
    ModuleRODDataList(CalibrationData &calibration_data, const std::string &rod_name, const std::string &module_name) 
      : m_rodDataList(calibration_data.getConnObjectData(rod_name)),
	m_moduleDataList(calibration_data.getConnObjectData(module_name))
    {
    }

    /** A datalist for a module which also contains the data list of the connected ROD.
     * @param calibration_data the calibration data container.
     * @param rod_name the ROD connectivity name of the ROD to which the module is connected.
     */
    ModuleRODDataList(CalibrationData &calibration_data, const std::string &rod_name) 
      : m_rodDataList(calibration_data.getConnObjectData(rod_name)),
	m_moduleDataList(m_rodDataList)
    {
    }

    /** Get a value which was obtained during an analyisis from the data list of this connectivity object.
     * @param number the serial number of the analysis.
     * @param var_def the definition of the variable.
     * @throw @ref CalibrationData::MissingValue if no value exists for the given variable and measurement.
     */
    template <class T>
    T analysisValue(SerialNumber_t number, const VarDef_t &var_def) const {
      return value<T>(kAnalysis,number,var_def);
    }

    /** Add a value which was obtained during a scan to the data list of a connectivity object.
     * @param conn_name the name of the connectivity object to which this property should be associated.
     * @param number the serial number of the scan.
     * @param var_def the definition of the variable.
     * @throw @ref CalibrationData::MissingValue if no value exists for the given variable and measurement.
     */
    template <class T>
    T scanValue(SerialNumber_t number, const VarDef_t &var_def) const {
      return value<T>(kScan, number, var_def);
    }

    /** Add a current status or measurement to the data list of the given connectivity object.
     * @param conn_name the name of the connectivity object to which this property should be associated.
     * @param var_def the definition of the variable.
     * @throw @ref CalibrationData::MissingValue if no value exists for the given variable and measurement.
     */
    template <class T>
    T currentValue(const VarDef_t &var_def) const {
      return value<T>(kCurrent, 0, var_def);
    }

    /** Get existing or create new ROD and module data lists for the the given connectivity object. 
     * @param calibration_data the container for all connectivity objects
     * @param rod_name the name of the ROD.
     * @param module_name the name of the module.
     * @throw @ref FatalCalibrationDataException the the ROD or module connectivity objects do not exist and cannot be creted due to resource limits.
     */
    static ModuleRODDataList create(CalibrationData &calibration_data, const std::string &rod_name, const std::string &module_name) {
      CalibrationData::ConnObjDataList rod_data_list = calibration_data.createConnObjDataList(rod_name);
      CalibrationData::ConnObjDataList module_data_list = calibration_data.createConnObjDataList(module_name);
      return ModuleRODDataList(rod_data_list, module_data_list);
    }


    /** Get existing or create new ROD and module data lists for the the given connectivity object. 
     * @param calibration_data the container for all connectivity objects
     * @param rod_name the name of the ROD.
     * @param module_name the name of the module.
     * @throw @ref FatalCalibrationDataException the the ROD or module connectivity objects do not exist and cannot be creted due to resource limits.
     */
    static ModuleRODDataList create(CalibrationData &calibration_data, const std::string &rod_name) {
      CalibrationData::ConnObjDataList rod_data_list = calibration_data.createConnObjDataList(rod_name);
      return ModuleRODDataList(rod_data_list, rod_data_list);
    }

  protected:

    /** A datalist for a ROD.
     * @param calibration_data the calibration data container.
     * @param rod_name the ROD connectivity name of the ROD to which the module is connected.
     * @param module_name the module connectivity name.
     */
    ModuleRODDataList(CalibrationData::ConnObjDataList &rod_data_list, CalibrationData::ConnObjDataList &module_data_list) 
      : m_rodDataList(rod_data_list),
	m_moduleDataList(module_data_list)
    {
    }


    /** Get the eneable state for the given measurement.
     * @param value_type the type of the measurement : current, scan, analysis.
     * @param serial_number the serial number of the measurement (or zero).
     */
    CalibrationData::EnableState_t enableState(EMeasurementType value_type, SerialNumber_t serial_number) const {
      CalibrationData::EnableState_t enable_state = m_moduleDataList.enableState(value_type, serial_number);
      //CalibrationData::EnableState_t parent_enable_state = m_rodDataList.enableState(value_type, serial_number);
      //       if (enable_state.isParentEnabled() != parent_enable_state.enabled()) {
      // 	enable_state.setParentEnabled(parent_enable_state.enabled());
      //       }
      return enable_state;
    }

    /** Get the valye for given measurement.
     * @throw @ref CalibrationData::MissingValue if no value exists for the given variable and measurement.
     */
    template <class T>
    T value(EMeasurementType value_type, SerialNumber_t serial_number, const VarDef_t &var_def) const
    {
      EValueConnType conn_type=var_def.connType();
      if (conn_type == kAllConnTypes) {
	conn_type = (isRodConnObj() ? kRodValue : kModuleValue );
      }
      switch (conn_type) {
      case kRodValue:
      case kTimValue: {
	if (this->isRodConnObj() || m_moduleDataList.enableState(value_type, serial_number).enabled()) {
	  return m_rodDataList.newestValue<T>(value_type, serial_number, var_def);
	}
	else {
	  this->objectIsDisabled();
	}
      }
      case kModuleValue:
	return m_moduleDataList.newestValue<T>(value_type, serial_number, var_def);
	//       case kAllConnTypes: {
	// 	if (isRodConnObj()) {
	// 	  return m_rodDataList.value<T>(value_type, serial_number, var_def.id());
	// 	}
	// 	else {
	// 	  return m_moduleDataList.value<T>(value_type, serial_number, var_def.id());
	// 	}
	//       }
      default: {
	// this would be a bug somewhere.
	std::stringstream message;
	message << "FATAL [ModuleDataList::value] Unsupported connectivity type " << var_def.connType() << ".";
	throw std::logic_error(message.str());
      }
      }
    }

  private:
    void objectIsDisabled() const {
      throw CalibrationData::MissingValue("Object is disabled.");
    }

    bool isRodConnObj() const { return m_rodDataList == m_moduleDataList; }
    CalibrationData::ConnObjDataList m_rodDataList;
    CalibrationData::ConnObjDataList m_moduleDataList;
  };

  inline bool operator==(const CalibrationData::EnableState_t &a, const CalibrationData::EnableState_t &b) {
    return b.operator==(a);
  }

  inline bool operator!=(const CalibrationData::EnableState_t &a, const CalibrationData::EnableState_t &b) {
    return !(b.operator==(a));
  }

}
#endif
