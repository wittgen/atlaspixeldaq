#ifndef _PixCon_CalibrationDataAccessor_h_

#include "CalibrationData.h"

namespace PixCon
{

  /** Helper class to convert the calibration data values into floats.
   */
  class ICalibrationDataAccessor
  {
  public:
    virtual ~ICalibrationDataAccessor() {}
    virtual float value(const PixCon::CalibrationData::ConnObjDataList  &data_list,
			PixCon::EMeasurementType measurement_type,
			PixCon::SerialNumber_t serial_number,
			PixCon::VarId_t var_id ) = 0;
  };

  template <class T>
  class CalibrationDataAccessor : public ICalibrationDataAccessor 
  {
  public:
    virtual float value(const PixCon::CalibrationData::ConnObjDataList &data_list,
			PixCon::EMeasurementType measurement_type,
			PixCon::SerialNumber_t serial_number,
			PixCon::VarId_t var_id ) {

      return data_list.value<T>(measurement_type,
				serial_number,
				var_id);

    }
  };

  template <class T>
  class CalibrationHistoryDataAccessor : public ICalibrationDataAccessor 
  {
  public:
    virtual float value(const PixCon::CalibrationData::ConnObjDataList &data_list,
			PixCon::EMeasurementType measurement_type,
			PixCon::SerialNumber_t serial_number,
			PixCon::VarId_t var_id ) {

      return data_list.newestHistoryValue<T>(measurement_type,
					     serial_number,
					     var_id);

    }
  };

  ICalibrationDataAccessor *createCalibrationDataAccessor( PixCon::VarDef_t &var_def);
}
#endif
