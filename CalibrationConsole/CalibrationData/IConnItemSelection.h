// Dear emacs, this is -*-c++-*-
#ifndef _PixCon_IConnItemSelection_h_
#define _PixCon_IConnItemSelection_h_

#include <string>

namespace PixCon {

  class IConnItemSelection
  {
  public:
    virtual ~IConnItemSelection() {}
    virtual bool isSelected(const std::string &conn_name) const = 0;
  };

  class EntireSelection : public IConnItemSelection
  {
  public:
    bool isSelected(const std::string &) const { return true; }
  };


}
#endif
