#include "CalibrationDataManager.h"
#include "CalibrationData.h"
#include "CategoryList.h"
#include <iostream>
#include <cassert>
#include "CalibrationDataFunctorBase.h"

#include <PixConnectivity/PixDisable.h>

#include <PixDbServer/PixDbServerInterface.h>
#include <ipc/partition.h>
#include <ipc/core.h>

#include <ConfigWrapper/ConfigHandle.h>
#include <ConfigWrapper/createDbServer.h>

#include <CalibrationDataException.h>

#include <ConfigWrapper/PixDisableUtil.h>

  class CalibrationDataDump : public PixCon::CalibrationDataFunctorBase
  {
  public:
    CalibrationDataDump(PixCon::CalibrationData &calibration_data, unsigned int mask)
      : PixCon::CalibrationDataFunctorBase(mask),
	m_calibrationData(&calibration_data),
	m_measurement(std::make_pair(PixCon::kNMeasurements,0))
    { 
    }

    CalibrationDataDump(PixCon::CalibrationData &calibration_data)
      : m_calibrationData(&calibration_data),
	m_measurement(std::make_pair(PixCon::kNMeasurements,0))
    { 
    }

    const std::vector<std::pair< std::string, PixCon::VarDef_t> > &getVarList(const std::pair<PixCon::EMeasurementType, PixCon::SerialNumber_t> &measurement) {

      if (measurement.first != m_measurement.first || measurement.second != m_measurement.second) {
	m_varList.clear();
	if (PixCon::CalibrationData::checkMeasurement(measurement.first, measurement.second)) {
	  try {

	    std::set<std::string> temp;
	    m_calibrationData->varList(measurement.first, measurement.second, temp);
	    m_measurement = measurement;
	    for(std::set<std::string>::const_iterator var_iter=temp.begin();
		var_iter != temp.end();
		++var_iter) {
	      m_varList.push_back(std::make_pair( *var_iter, PixCon::VarDefList::instance()->getVarDef(*var_iter)));
	    }

	  }
	  catch(PixCon::CalibrationDataException &) {
	  }
	}
      }
      return m_varList;
    }

    void operator()( PixCon::EConnItemType /*item_type*/,
		     const std::string &conn_name,
		     const std::pair<PixCon::EMeasurementType, PixCon::SerialNumber_t> &measurement,
		     PixCon::CalibrationData::ConnObjDataList &data_list,
		     PixCon::CalibrationData::EnableState_t /*is_enable_state*/,
		     PixCon::CalibrationData::EnableState_t new_enable_state) {

      const std::vector<std::pair<std::string, PixCon::VarDef_t> > &var_list=getVarList(measurement);
      for (std::vector<std::pair<std::string, PixCon::VarDef_t> >::const_iterator var_iter = var_list.begin();
	   var_iter != var_list.end();
	   ++var_iter) {
	  std::cout << PixCon::makeSerialNumberString(measurement.first,measurement.second) 
		    << " " << conn_name
		    << ( new_enable_state.enabled() ? " <enabled> " : " -DISABLED- " )
		    << " : " <<  var_iter->first << " = ";
	try {
	  if (var_iter->second.isValueWithOrWithoutHistory()) {
	    std::cout << data_list.newestValue<float>(measurement.first,measurement.second, var_iter->second);
	  }
	  else if (var_iter->second.isStateWithOrWithoutHistory()) {
	    std::cout << static_cast<int>(data_list.newestValue<PixCon::State_t>(measurement.first,measurement.second, var_iter->second));
	  }
	  else {
	    std::cout << " (unsupported type) ";
	  }
	}
	catch(PixCon::CalibrationData::MissingValue &) {
	}
	std::cout << std::endl;
      }

    }

    void operator()( PixCon::EConnItemType /*item_type*/,
		     const std::string &conn_name,
		     const std::pair<PixCon::EMeasurementType, PixCon::SerialNumber_t> &measurement,
		     PixCon::CalibrationData::EnableState_t new_enable_state) {
      std::cout << PixCon::makeSerialNumberString(measurement.first,measurement.second) 
		<< " " << conn_name
		<< ( new_enable_state.enabled() ? " <enabled> " : " -DISABLED- " )
		<< " :  -- " <<  std::endl;
    }

  private:
    PixCon::CalibrationData                                     *m_calibrationData;
    std::vector<std::pair<std::string, PixCon::VarDef_t> >       m_varList;

    std::pair<PixCon::EMeasurementType, PixCon::SerialNumber_t > m_measurement;
  };


class IPCInstance
{

 private:
  IPCInstance(int argc, char **argv, const std::string &partition_name) {
    IPCCore::init(argc, argv);
    m_ipcPartition=std::unique_ptr<IPCPartition>(new IPCPartition(partition_name.c_str()));

    if (!m_ipcPartition->isValid()) {
      std::stringstream message;
      message << "ERROR [IPCInstance] Not a valid partition " << partition_name << std::endl;
      throw std::runtime_error(message.str());
    }
  }
 public:

  static IPCInstance *instance(int argc, char **argv, const std::string &partition_name) {
    if (!s_instance) {
      s_instance = new IPCInstance(argc,argv,partition_name);
    }
    return s_instance;
  }

  IPCPartition &partition() { return *m_ipcPartition;}
  
 private:
  std::unique_ptr<IPCPartition> m_ipcPartition;
  static IPCInstance *s_instance;

};

IPCInstance *IPCInstance::s_instance = NULL;


bool setDbServer(const std::string &db_server_partition_name,
		 const std::string &db_server_name,
		 int argc,
		 char **argv)
{


  int temp_argc=0;
  char *temp_argv[1]={NULL};
  IPCInstance *ipc_instance = IPCInstance::instance(temp_argc,temp_argv, db_server_partition_name);

  std::shared_ptr<PixLib::PixDbServerInterface> db_server( PixA::createDbServer(ipc_instance->partition(),db_server_name) );
  if (db_server.get()) {
    PixA::ConnectivityManager::useDbServer(db_server);
    return true;
  }
  return false;
}

std::vector<std::string> *getStringVec(const PixLib::PixDisable *pix_disable) {
  if (!pix_disable) return NULL;
  const PixLib::ConfObj &obj = const_cast<PixLib::PixDisable *>(pix_disable)->config()["Disabled"]["Objects"];
  assert( const_cast<PixLib::ConfObj &>(obj).type() == PixLib::ConfObj::STRVECT);
  const PixLib::ConfStrVect &str_vec = static_cast<const PixLib::ConfStrVect&>( obj );
  return &( const_cast<PixLib::ConfStrVect &>(str_vec).value());
}

bool operator==(std::vector<std::string> &a, std::vector<std::string> &b) {
  if (a.size() != b.size()) return false;
  std::vector<std::string>::const_iterator b_iter = b.begin();
  for (std::vector<std::string>::const_iterator a_iter = a.begin();
       a_iter != a.end();
       ++a_iter, ++b_iter) {
    assert( b_iter != b.end());
    if (*a_iter != *b_iter) return false;
  }
  return true;
}

int main1(int argc, char **argv)
{

  std::string id_name="SR1";
  std::string tag_name="TOOTHPIX-V5";
  std::string cfg_tag_name="TOOTHPIX-DEFAULT";
  std::string mod_cfg_tag_name;
  int verbose=1;

  std::vector<std::string> deactivate_list;
  std::vector<std::string> disable_readout_list;
  std::vector<std::string> rod_names;
  
  std::shared_ptr<PixLib::PixDisable> pix_disable;

  for (int arg_i=1; arg_i<argc; arg_i++) {

    if (strcmp(argv[arg_i],"--db-server")==0 && arg_i+2<argc && argv[arg_i+1][0]!='-' && argv[arg_i+2][0]!='-') {

      try {
	std::string db_server_partition_name = argv[++arg_i];
	std::string db_server_name = argv[++arg_i];
	setDbServer(db_server_partition_name, db_server_name, argc, argv);
      }
      catch (std::runtime_error &err) {
	std::cout << "FATAL [main] caught exception when trying to create db server interface (std::exception) : " << err.what() << std::endl;
	return -1;
      }

    }
    else if (strcmp(argv[arg_i],"-v")==0 || strcmp(argv[arg_i],"--verbose")==0) {
      verbose++;
    }
    else if (strcmp(argv[arg_i],"-q")==0 || strcmp(argv[arg_i],"--quiet")==0) {
      verbose=0;
    }
    else if (strcmp(argv[arg_i],"-i")==0 && arg_i+1<argc) {
      id_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-t")==0 && arg_i+2<argc && argv[arg_i+2][0]!='-' && argv[arg_i+1][0]!='-') {
      tag_name = argv[++arg_i];
      cfg_tag_name = argv[++arg_i];
      if (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	mod_cfg_tag_name = argv[++arg_i];
      }
    }
    else if (strcmp(argv[arg_i],"-d")==0 &&  arg_i+1<argc && argv[arg_i+1][0]!='-' ) {
      if (!pix_disable.get()) {
	pix_disable=std::shared_ptr<PixLib::PixDisable>(new PixLib::PixDisable("test"));
      }
      while (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	pix_disable->disable(argv[++arg_i]);;
      }
    }
    else {
      std::cout << "ERROR ["<< argv[0] << ":main] unhandled argument " << arg_i << " : " << argv[arg_i]<< "." << std::endl;
    }
  }

  if (tag_name.empty() || cfg_tag_name.empty()) {
      std::cout << "usage: " << argv[0] << "[-i ID-tag] -t tag cfg-tag  [--db-server partition name]" << std::endl;
      return -1;
  }
  if (mod_cfg_tag_name.empty()) {
    mod_cfg_tag_name = cfg_tag_name;
  }

  if (pix_disable.get()) {
    pix_disable->config().dump(std::cout);
  }

  PixCon::CalibrationDataManager a;
  //  a.changeCurrentConnectivity("CT","C34_ISP_v32","C34_ISP_v32","C34_ISP_v32","CT-C34ISP",0);
  a.changeCurrentConnectivity(id_name, tag_name, tag_name, tag_name,cfg_tag_name, mod_cfg_tag_name,0);
  a.addScan(1,
	    ::time(0),
	    0,
	    "test",
	    "",
	    PixCon::MetaData_t::kDebug,
	    PixA::ConfigHandle(),
	    "",
	    a.currentConnectivity());

  a.addScan(2,
	    ::time(0),
	    0,
	    "test",
	    "",
	    PixCon::MetaData_t::kDebug,
	    PixA::ConfigHandle(),
	    "",
	    a.currentConnectivity());

  a.addScan(3,
	    ::time(0),
	    0,
	    "test",
	    "",
	    PixCon::MetaData_t::kDebug,
	    PixA::ConfigHandle(),
	    "",
	    a.currentConnectivity());   

  a.addScan(4,
	    ::time(0),
	    0,
	    "test",
	    "",
	    PixCon::MetaData_t::kDebug,
	    PixA::ConfigHandle(),
	    "",
	    a.currentConnectivity());   

  PixCon::CategoryList categories;
  PixCon::CategoryList::Category dcs_cat=categories.addCategory("DCS");
  PixCon::CategoryList::Category status_cat=categories.addCategory("Status");
  PixCon::CategoryList::Category analysis_cat=categories.addCategory("Analysis");

  // There are different types of values
  a.calibrationData().addAnalysisValue<float>(PixCon::kModuleValue, "L0_B12_S2_C6_M1C",1,analysis_cat.addVariable("Threshold Dispersion"),0.);

  enum EScanStatus {kScanUnknown, kScanConfiguring, kScanScanning, kScanSuccess, kScanFailure};
  a.calibrationData().addScanValue<PixCon::State_t>(PixCon::kRodValue,
						    "ROD_C0_S11",
						    1,
						    status_cat.addVariable("Scan Status"),
						    static_cast<PixCon::State_t>(kScanScanning));

  PixCon::VarDef_t scan_status_def = PixCon::VarDefList::instance()->getVarDef("Scan Status");
  a.calibrationData().addValue<PixCon::State_t>("ROD_C0_S15",
						PixCon::kScan,
						1,
						scan_status_def,
						static_cast<PixCon::State_t>(kScanScanning));

  // the same variables can be in several categories
  enum EAnalysisStatus {kAnaUnknown,kAnaPending,kAnaWaiting,kAnaAnalysing,kAnaSuccess,kAnaFailure};
  a.calibrationData().addCurrentValue<PixCon::State_t>(PixCon::kRodValue,
						       "ROD_C0_S11",
						       analysis_cat.addVariable( status_cat.addVariable("Analysis Status")),
						       static_cast<PixCon::State_t>(kAnaWaiting));

  CalibrationDataDump dumper(a.calibrationData());
  std::cout << "INFO ["<<argv[0]<<":main] dump calibration data:" << std::endl;
  a.forEach(PixCon::kScan,1, dumper, false);
  a.forEach(PixCon::kCurrent,0, dumper, false);

  a.addAnalysis(1,::time(0),"test","",PixCon::MetaData_t::kNormal,1,PixA::ConfigHandle(),"",PixA::ConfigHandle(),"",PixA::ConfigHandle());
  a.copyScanConnectivityToAnalysis(1,1);
  
  PixA::ConnectivityRef ref=a.scanConnectivity(1);
  const PixA::ConnectivityRef const_ref=a.scanConnectivity(1);

  std::cout << "INFO ["<<argv[0]<<":main] Remove:" << "Scan Status"<< std::endl;
  a.calibrationData().removeVariable<PixCon::State_t>(PixCon::kScan,1, PixCon::VarDefList::instance()->getVarDef("Scan Status").id());
  a.forEach(PixCon::kScan,1,dumper, false);

  {
    PixA::DisabledListRoot disabled_list(pix_disable);
    a.setEnable(PixCon::kScan,2,disabled_list);
  }
  {
    std::string enable_string = a.enableString(PixCon::kScan,2);
    std::cout << "INFO ["<<argv[0]<<":main] Enable string  : " << enable_string << std::endl;
    std::shared_ptr<PixLib::PixDisable>  a_disable(a.getPixDisable(PixCon::kScan,2,true));
    if (a_disable.get()) {
      a_disable->config().dump(std::cout);
    }

    {
      PixA::DisabledListRoot disabled_list( PixA::createEnableMap(enable_string));    
      a.setEnable(PixCon::kScan,3,disabled_list);
    }

    {
      PixA::DisabledListRoot disabled_list( a_disable);
      a.setEnable(PixCon::kScan,4,disabled_list);
    }

    {
      std::string enable_string2 = a.enableString(PixCon::kScan,3);
      std::cout << "INFO ["<<argv[0]<<":main] Enable string 3: " << enable_string2 << std::endl;
      assert( enable_string2 == enable_string);

      std::shared_ptr<PixLib::PixDisable>  a_disable2(a.getPixDisable(PixCon::kScan,3,true));
      if (a_disable2.get()) {
	a_disable2->config().dump(std::cout);
      }

      assert ( getStringVec(a_disable.get()) != NULL );
      assert ( getStringVec(a_disable2.get()) != NULL );
      assert( *getStringVec(a_disable.get()) == *getStringVec(a_disable2.get()) );
    }

    {
      std::string enable_string4 = a.enableString(PixCon::kScan,4);
      std::cout << "INFO ["<<argv[0]<<":main] Enable string 4: " << enable_string4 << std::endl;
      assert( enable_string4 == enable_string);

      std::shared_ptr<PixLib::PixDisable>  a_disable4(a.getPixDisable(PixCon::kScan,4,true));
      if (a_disable4.get()) {
	a_disable4->config().dump(std::cout);
      }

      assert ( getStringVec(a_disable.get()) != NULL );
      assert ( getStringVec(a_disable4.get()) != NULL );
      assert( *getStringVec(a_disable.get()) == *getStringVec(a_disable4.get()) );
    }
    
  }

  // cleanup
  a.resetCurrent();        // reset  current values
  a.removeScan(1);         // remove calibration values and connectivity of scan up S0000001
  a.removeScansUpTo(1);     // remove calibration values and connectivity of all scans up to and including S0000001
  a.removeAnalysis(1);     // remove calibration values and connectivity of analysis A0000001
  a.removeAnalysesUpTo(1); // remove calibration values and connectivity of all analyses up to and including A0000001
  // @todo verify that everything is clean now.


  return 0;
}
