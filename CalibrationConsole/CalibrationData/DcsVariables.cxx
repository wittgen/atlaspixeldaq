#include "DcsVariables.h"
#include <iostream>

namespace PixCon {


  std::vector<DcsVariables::EVariable> DcsVariables::s_moduleVar;
  std::vector<DcsVariables::EVariable> DcsVariables::s_pp0Var;
  std::vector<DcsVariables::EVariable> DcsVariables::s_pp2Var;

  std::map<std::string, DcsVariables::EFsmState> DcsVariables::s_stateMap;

  const char *DcsVariables::s_stateName[kNFsmStates]= {
    "READY",
    "LV_ON",
    "UNDEFINED",
    "OFF",
    "LOCKED_OUT",
    "Unknown"
  };


  bool DcsVariables::s_init = DcsVariables::init();

  bool DcsVariables::init() 
  {
    s_moduleVar.push_back(HVON);
    s_moduleVar.push_back(HV_MON);
    s_moduleVar.push_back(IHV_MON);
    s_moduleVar.push_back(VDDD);
    s_moduleVar.push_back(VDDD_MON);
    s_moduleVar.push_back(VDDA);
    s_moduleVar.push_back(VDDA_MON);
    s_moduleVar.push_back(IDDD_MON);
    s_moduleVar.push_back(IDDA_MON);
    s_moduleVar.push_back(TMODULE);
    s_moduleVar.push_back(FSM_STATE);

    //    std::cout << "INFO [DcsVariables::init] " << s_moduleVar.size() << " module variables." << std::endl;

    s_pp0Var.push_back(TOPTO);
    s_pp0Var.push_back(VISET);
    s_pp0Var.push_back(VISET_MON);
    s_pp0Var.push_back(IVISET_MON);
    s_pp0Var.push_back(VPIN);
    s_pp0Var.push_back(VPIN_MON);
    s_pp0Var.push_back(IPIN_MON);
    s_pp0Var.push_back(VVDC);
    s_pp0Var.push_back(VVDC_MON);
    s_pp0Var.push_back(IVDC_MON);
    s_pp0Var.push_back(FSM_STATE);

    //    std::cout << "INFO [DcsVariables::init] " << s_pp0Var.size() << " module variables." << std::endl;

    s_pp2Var.push_back(WIENERD_MON);
    s_pp2Var.push_back(WIENERA_MON);

    // initialise state map
    for (unsigned int state_i=0; state_i<kNFsmStates; ++state_i) {
      s_stateMap.insert(std::make_pair<std::string,DcsVariables::EFsmState>( s_stateName[state_i],static_cast<EFsmState>(state_i) ));  
    }

    return true;
  }

  const std::string DcsVariables::s_connNameSeparator("_");
  const char DcsVariables::s_isServerNameSeparator('.');

  const char *DcsVariables::s_dcsName[NDcsVariables]={
    "HV.ON",
    "HV.Vmon",
    "HV.Imon",
    "VDD1.Vset",
    "VDD1.Vmon",
    "VDD2.Vmon",
    "VDDA1.Vset",
    "VDDA1.Vmon",
    "VDDA2.Vmon",
    "VDD1.Imon",
    "VDDA1.Imon",
    "TModule",
    "TOpto",
    "VISET.Vset",
    "VISET.Vmon",
    "VISET.Imon",
    "VPIN.Vset",
    "VPIN.Vmon",
    "VPIN.Imon",
    "VVDC1.Vset",
    "VVDC1.Vmon",
    "VVDC1.Imon",
    "STATE"
  };
  
  const char *DcsVariables::s_fsmStateName=".fsm.currentState";
  const char *DcsVariables::s_fsmStatusPrefix="STATUS_";
  const char *DcsVariables::s_fsmStatusVarName="STATUS_fsm.currentState";
  const char *DcsVariables::s_any=".*";


  const char *DcsVariables::getStateName(DcsVariables::EFsmState state) {
    return s_stateName[(state<kNFsmStates ? state : kUnknown) ];
  }

  DcsVariables::EFsmState DcsVariables::getState(const std::string &state_name) {
    std::map<std::string, EFsmState>::const_iterator state_iter = s_stateMap.find(state_name);
    if ( state_iter == s_stateMap.end()) return kUnknown;
    else return state_iter->second;
  }

  std::string DcsVariables::makeIsName(const std::string &is_server_name, const std::string &conn_name, EVariable variable) {
    assert(variable<NDcsVariables);
    return is_server_name + s_isServerNameSeparator + conn_name + s_connNameSeparator+s_dcsName[variable];
  }


  std::string DcsVariables::makeRequestName(const std::string &conn_name, EVariable variable) {
    assert(variable<NDcsVariables);
    return conn_name + s_connNameSeparator+s_dcsName[variable];
  }

}
