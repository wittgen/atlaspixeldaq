/* Dear emacs, this is -*-c++-*- */
#ifndef _PixCon_ValueHistory_h_
#define _PixCon_ValueHistory_h_

#include "TimedValue.h"
#include "RingBuffer.h"

namespace PixCon {

  template <class T>
  /** A ring buffer container for values plus time stamp.
   * To safe storage, the time is only stored in units of seconds in 16bit words.
   * This requires to rebase the time from time.
   * The values and time stamps can be extracted in the following way :
   * <verb>
   *  for(typename std::vector< TimedValue<T> >::const_iterator iter = a.begin();
   *	  iter != a.end();
   *	  a.const_increment(iter)) {
   *      T value = iter->value();
   *      TimeType_t time = a.extractTime(*iter);
   *  }
   *</verb>
   */

  class ValueHistory : public  RingBuffer_t< TimedValue<T>,  TimedValue<T> &>
  {
  public:
    ValueHistory(unsigned int size) : RingBuffer_t<TimedValue<T>,TimedValue<T> & >(size), m_timeBase(0) {}
    ~ValueHistory() {}

    bool addValue( TimeType_t time, T value ) {
      if (time<=m_timeBase) {
	TimedValueBase::ExceptionTimeBaseTooLate(time, m_timeBase);
      }
      if (time-m_timeBase>=USHRT_MAX) {
	// magic number : time==m_timeBase means the value is invalid
	rebase(time-1);
      }
      TimedValue<T> temp(m_timeBase, time, value);
      return this->addElement( temp );
    }

    TimeType_t base() const { return m_timeBase; }

    static bool isValid(const TimedValue<T> &a_value) {
      return a_value.time(0)>0;
    }

    TimeType_t extractTime( const TimedValue<T> &a) const {
      return a.time(m_timeBase);
    }

  protected:
    void rebase( TimeType_t new_base) {
      for(typename std::vector<TimedValue<T> >::iterator iter = this->begin();
	  iter != this->end();
	  iter = this->increment(iter) ) {

	if (!isValid(*iter)) continue;

	if (iter->time(m_timeBase)<new_base) {
	  // set too old values to an illegal value
	  iter->setValue(0, 0, 0);
	}
	else {
	  iter->rebase(m_timeBase, new_base);
	}
	
      }
      m_timeBase=new_base;
    }
  public :
    TimeType_t m_timeBase;
  };
}
#endif
