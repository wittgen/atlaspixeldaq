#include "CalibrationDataStyle.h"
#include "DefaultColourCode.h"

namespace PixCon {

  void DataSelection::_setMinMaxValue()
  {

    float min_val=FLT_MAX;
    float max_val=-FLT_MAX;

    // iterate over connectivity objects
    for ( std::map<std::string, CalibrationData::ConnObjDataList_t >::const_iterator conn_iter = m_calibrationData->begin();
	  conn_iter != m_calibrationData->end();
	  conn_iter++ ) {

      // iterate over scan, analysis or current values
      for (CalibrationData::ConnObjDataList_t::const_iterator type_iter = conn_iter->second.begin();
	   type_iter != conn_iter->second.end();
	   type_iter++) {

	// iterate over serial numbers
	for (CalibrationData::ConnObjMeasurementList_t::const_iterator serial_number_iter = type_iter->begin();
	     serial_number_iter != type_iter->end();
	     serial_number_iter++) {

	  const std::map<VarId_t, float> &value_map=serial_number_iter->second.getValueMap<float>();
	  std::map<VarId_t, float>::const_iterator  value_iter = value_map.find(m_varDef.id());
	  if (  value_iter != value_map.end() && !isnan(value_iter->second) && !isinf(value_iter->second)) {
	    if (value_iter->second > max_val) max_val = value_iter->second;
	    if (value_iter->second < min_val) min_val = value_iter->second;
	  }
	}
      }
    }

    //    std::cout << "INFO [CalibrationDataPalette::setMinMax] new range : " << min_val << " - " << max_val << std::endl;
    // @todo optimise range 
    float extent=(max_val-min_val)*.1;
    m_varDef.setMinMax(min_val-extent,max_val+extent);

  }


  CalibrationDataPalette::CalibrationDataPalette()
  {
    // set some default values

    // special colors
    m_objectUnavailable=QColor(255,255,255);

    // pens
    setNormalPen(QPen(QColor(80,80,80),1));
    setFadedPen(QPen(QColor(120,120,120),1));
    setSelectedPen(QPen(QColor(0,0,255),2));
    setUnavailPen(QPen(QColor(200,200,200),1));

    // lighten
    //    setLayerLighteningFactors(115,100,85);
    setLayerLighteningFactors(100,100,100); // all layers get the same colours
    setFadeLighteningFactors(100,170);

    // special brushes
    m_fadeBrush[0]=Qt::SolidPattern;
    m_fadeBrush[1]=Qt::Dense4Pattern; //QBrush::SolidPattern

    m_specialBrush[kBrushUnknownState]=QBrush(QColor(255,227,70), Qt::Dense5Pattern);
    m_specialBrush[kBrushDisabled]=QBrush(QColor(200,200,200), Qt::Dense4Pattern);
  }


  void CalibrationDataPalette::setDefaultPalette() 
  {
    {
      std::shared_ptr< std::vector< QColor > > value_colour_list(new std::vector< QColor >);

      for (unsigned int color_i=20; color_i-->0;) {
	QColor col;
	col.setHsv((250*color_i)/20,255,255);//,QColor::Hsv),
	value_colour_list->push_back(col);
      }
      setValueColourList(value_colour_list);
    }

    {
      std::shared_ptr< std::vector< QColor > > default_state_colour_list(new std::vector< QColor >);
      default_state_colour_list->push_back(QColor(255,243,107));     // 0: light yellow, unknown 
      default_state_colour_list->push_back(QColor(100,100,100));     // 1: dark grey,    off
      default_state_colour_list->push_back(QColor(255,170,100));     // 2: orange,       on
      default_state_colour_list->push_back(QColor( 97,119,245));     // 3: blue,         configured
      default_state_colour_list->push_back(QColor( 97,169,135));     // 4: dark green,   scanning
      default_state_colour_list->push_back(QColor( 97,233,245));     // 5: cyan,         analysing
      default_state_colour_list->push_back(QColor( 0,255,0));        // 6: green,        success
      default_state_colour_list->push_back(QColor( 255,234,0));      // 7: yellow,       close to failure
      //      default_state_colour_list->push_back(QColor( 255,174,0));      // 8:failure
      default_state_colour_list->push_back(QColor( 246,79,7));       // 8: red,          failure
      default_state_colour_list->push_back(QColor( 239,17,183));     // 9: magenta,      alarm
      default_state_colour_list->push_back(QColor( 196,206,255));    // 10:pale blue
      default_state_colour_list->push_back(QColor( 255,163,163));    // 11:pale red
      default_state_colour_list->push_back(QColor( 162,222,162));    // 12:pale green
      default_state_colour_list->push_back(QColor( 191,159,80));     // 13:pale brown
      default_state_colour_list->push_back(QColor( 115,192,199));    // 14:darker cyan
      default_state_colour_list->push_back(QColor( 255,138,225));    // 15:pale magenta
      default_state_colour_list->push_back(QColor( 216,152,107));    // 16:pale brown red
      setDefaultColourList(default_state_colour_list);
    }

  }

  std::pair<QBrush , QPen > CalibrationDataStyle::brushAndPen(bool is_selected, bool is_unfocused) {
    try {
    //      if (m_dataSelection->hasChanged(m_lastSelectionId)) {
    m_lastSelectionId = m_dataSelection->selectionId();
    SerialNumber_t serial_number = ( m_dataSelection->type()==kCurrent ? 0 : m_dataSelection->serialNumber() );
    CalibrationData::EnableState_t enable_state;
    try {
      enable_state = m_moduleRodDataList.enableState(m_dataSelection->type(),serial_number);
    }
    catch (CalibrationData::MissingConnObject &err) {
    }

    unsigned int fade_i = (is_unfocused ? 1 : 0);
    if (enable_state.isInConnectivity()) {
      VarDef_t var_def = m_dataSelection->varId();
      if (var_def.isValid()) {
      switch ( var_def.valueType() ) {
      case kValue:
      case kTimedValue: {
	try {
	  float value = m_moduleRodDataList.value<float>(m_dataSelection->type(),
							 serial_number,
							  var_def);
	  if (var_def.isMinMaxSet()) {
	    value -= var_def.minVal();
	    value /= (var_def.maxVal() - var_def.minVal());
	  }

	  m_lastBrushAndPen=m_palette->brushAndPen(is_selected, m_layer,fade_i, var_def.id(), value);
	}
	catch (std::runtime_error &err) {
	  if (enable_state.enabled()) {
	    m_lastBrushAndPen=m_palette->brushAndPenUnknown(m_layer,fade_i);
	  }
	  else {
	    m_lastBrushAndPen=m_palette->brushAndPenDisabled(m_layer,fade_i);
	  }
	}
	break;
      }
      case kTimedState:
      case kState: {
	try {

	  State_t state;
	  if ( m_dataSelection->isEnabledVar()) {
	    if (enable_state.enabled()) {
	      state=0;
	    }
	    else if (!enable_state.isParentAllocated() || !enable_state.isAllocated()) {
	      m_lastBrushAndPen=m_palette->brushAndPenDisabled(m_layer,fade_i);
	      break;
	    }
	    else if (enable_state.isTemporaryEnabled() && !enable_state.isParentEnabled()) {
	      state=2;
	    }
	    else {
	      state=1;
	    }
	  }
	  else {
	    state = m_moduleRodDataList.value<State_t>(m_dataSelection->type(),
							serial_number,
							var_def);
	  }

	  StateVarDef_t state_def = var_def.stateDef();
	  State_t mapped_state = state_def.remapState(state);
	  if (state>state_def.nStates()) {
	    mapped_state=state_def.nStates();
	  }

	  m_lastBrushAndPen=m_palette->brushAndPen(is_selected, m_layer,fade_i, var_def.id(), mapped_state);

	}
	catch (std::runtime_error &err) {
	  if (enable_state.enabled()) {
	    m_lastBrushAndPen=m_palette->brushAndPenUnknown(m_layer,fade_i);
	  }
	  else {
	    m_lastBrushAndPen=m_palette->brushAndPenDisabled(m_layer,fade_i);
	  }
	}
	break;
      }
      case kNValueTypes: {
	// invalid var
	m_lastBrushAndPen=m_palette->brushAndPenUnknown(m_layer,fade_i);
	break;
      }
      default: {
	std::stringstream message;
	message << "FATAL [CalibrationDataStyler::brushAndPen] unsupported variable type " << var_def.valueType() << "." ;
	throw std::runtime_error(message.str());
      }
      }
      }
      else {
	if (enable_state.enabled()) {
	  m_lastBrushAndPen=m_palette->brushAndPenUnknown(m_layer,fade_i);
	}
	else {
	  m_lastBrushAndPen=m_palette->brushAndPenDisabled(m_layer,fade_i);
	}
      }
    }
    else {
      m_lastBrushAndPen=m_palette->brushAndPenUnavail(m_layer, fade_i);
    }
    //      }
    return m_lastBrushAndPen;
    }
    catch( PixCon::CalibrationData::MissingValue & err) {
      return m_palette->brushAndPenUnknown(m_layer,0);
    }


  }

  std::string CalibrationDataStyle::s_enableVar("Enabled");

  VarDef_t CalibrationDataStyle::createEnabledVarDef(const std::string &enabled_var_def) {
    try {
      const VarDef_t var_def( VarDefList::instance()->getVarDef(s_enableVar) );
      if ( var_def.isStateWithOrWithoutHistory() ) {
	//	std::cout << "INFO [CalibrationDataStyle::createEnableVarDef] variable " << s_enableVar
	//                << " already defined as the variable for the enable status." << std::endl;
	return var_def;
      }
    }
    catch (UndefinedCalibrationVariable &) {
    }
    s_enableVar = enabled_var_def;
    return createEnabledVarDef();
  }

  VarDef_t CalibrationDataStyle::createEnabledVarDef()
  {
    const std::string &enabled_var_name = s_enableVar;
    try {
      const VarDef_t var_def( VarDefList::instance()->getVarDef(enabled_var_name) );
      if ( !var_def.isStateWithOrWithoutHistory() ) {
	std::cout << "ERROR [CalibrationDataStyle::createEnableVarDef] variable " << enabled_var_name << " already exists but it is not a state variable." << std::endl;
      }
      return var_def;
    }
    catch (UndefinedCalibrationVariable &) {
      VarDef_t var_def( VarDefList::instance()->createStateVarDef(enabled_var_name, kAllConnTypes, 3) );
      if (var_def.isStateWithOrWithoutHistory() ) {
	//	scan_variables.addVariable(histogram_state_name);
	StateVarDef_t &state_var_def = var_def.stateDef();

	state_var_def.addState(kItemEnabled,        DefaultColourCode::kSuccess,        "Enabled");
	state_var_def.addState(kItemDisabled,       DefaultColourCode::kFailed,         "Disabled");
	state_var_def.addState(kParentItemDisabled, 16 /*pale red*/, "Parent dis.");
	
      }
      else {
	std::cout << "ERROR [ScanStatusMonitor::ctor] variable " << enabled_var_name << " already exists but it is not a state variable." << std::endl;
      }
      return var_def;
    }

  }


}
