#ifndef _PixCon_DefaultColourCode_h_
#define _PixCon_DefaultColourCode_h_

namespace PixCon {

  class DefaultColourCode
  {
  public:
    enum EDefaultColourCode {
      kUnknown=0,                       // light yellow
      kOff=1,                           // dark grey
      kOn=2,                            // orange
      kConfigured=3,                    // blue
      kScanning=4,        kRunning=4,   // dark green
      kAnalysing=5,                     // cyan
      kSuccess=6,         kOk=6,        // green
      kCloseToFailure=7,                // yellow
      kFailure=8,         kFailed=8,    // red
      kAlarm=9                          // magenta
    };

  };

}
#endif
