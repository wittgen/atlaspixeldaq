// Dear emacs, this is -*-c++-*-
#ifndef _PixCon_CalibrationDataStyle_h_
#define _PixCon_CalibrationDataStyle_h_

#include <qcolor.h>
#include <qnamespace.h>
#include <qbrush.h>
#include <qpen.h>

#include <cassert>

#include "IItemStyle.h"
#include "CalibrationData.h"


namespace PixCon {

  class DataSelection
  {
  public:
    DataSelection (std::shared_ptr<CalibrationData> calibration_data,
		   EMeasurementType type,
		   SerialNumber_t serial_number,
		   const VarDef_t var_def,
		   const VarDef_t &enabled_var_def = VarDef_t::invalid())
      : m_selectionId(0),
	m_serialNumber(serial_number),
	m_varDef(var_def),
	m_enabledVarDef(enabled_var_def),
	m_calibrationData(calibration_data)
    { setSelection(type,var_def); }

    DataSelection (std::shared_ptr<CalibrationData> calibration_data,
		   const VarDef_t &var_def,
		   const VarDef_t &enabled_var_def = VarDef_t::invalid())
      : m_selectionId(0),
	m_serialNumber(0),
	m_varDef(var_def),
	m_enabledVarDef(enabled_var_def),
	m_calibrationData(calibration_data)
    { setSelection(kCurrent,var_def); }

    void setSelection(EMeasurementType type)
      { m_selectionId++; m_type=type; if (m_type==kCurrent) {m_serialNumber=0;} }

    void setSelection(EMeasurementType type, const VarDef_t &var_def)
      { m_selectionId++; m_type=type; m_varDef = var_def; checkMinMaxValue(); if (m_type==kCurrent) {m_serialNumber=0;} }

    void setSerialNumber(SerialNumber_t serial_number)   { m_selectionId++; if (m_type!=kCurrent) { m_serialNumber = serial_number;} }

    unsigned int     selectionId() const  { return m_selectionId; }
    EMeasurementType type() const         { return m_type; }
    SerialNumber_t   serialNumber() const { return m_serialNumber; }
    const VarDef_t  &varId() const        { return m_varDef; }

    VarDef_t  &varId()                    { return m_varDef; }
    
    const VarDef_t  &enabledVarDef()      { return m_enabledVarDef; }

    bool isEnabledVar() const { return m_varDef.id() == m_enabledVarDef.id(); }

    bool hasChanged(unsigned int last_selection_id ) const { return last_selection_id != m_selectionId; }

    template <class T>
    T selectedValue(const std::string &conn_name) const {
      CalibrationData::ConnObjDataList data_list(m_calibrationData->getConnObjectData(conn_name));
      return data_list.CalibrationData::ConnObjDataList::newestValue<T>(type(),serialNumber(),varId());
    }

    //     template <class T>
    //     T selectedHistoryValue(const std::string &conn_name) const {
    //       assert( varId().hasHistory() );
    //       CalibrationData::ConnObjDataList data_list(m_calibrationData->getConnObjectData(conn_name));
    //       return data_list.CalibrationData::ConnObjDataList::newestHistoryValue<T>(type(),serialNumber(),varId().id());
    //     }

    template <class T>
    const ValueHistory<T> &selectedHistory(const std::string &conn_name) const {
      assert( varId().hasHistory() );
      CalibrationData::ConnObjDataList data_list(m_calibrationData->getConnObjectData(conn_name));
      return data_list.CalibrationData::ConnObjDataList::valueHistory<T>(type(),serialNumber(),varId().id());
    }

    template <class T>
    T value(const CalibrationData::ConnObjDataList &data, VarId_t var_id) {
      return data.value<T>(type(), serialNumber(), var_id );
    }

    template <class T>
    T newestValue(const CalibrationData::ConnObjDataList &data, VarDef_t var_id) {
      return data.newestValue<T>(type(), serialNumber(), var_id );
    }

    template <class T>
    T value(const CalibrationData::ConnObjDataList &data) {
      return newestValue<T>(data,varId() );
    }

    CalibrationData::EnableState_t enableState(const CalibrationData::ConnObjDataList &data) {
      return data.enableState(type(), serialNumber());
    }

    void setMinMaxValue() {

      if (m_varDef.isValueWithOrWithoutHistory() ) {
	_setMinMaxValue();
      }
    }

  private:
    void checkMinMaxValue() {

      if (m_varDef.isValueWithOrWithoutHistory() && !m_varDef.isMinMaxSet() ) {
	_setMinMaxValue();
      }
    }

    void _setMinMaxValue();

    unsigned int                        m_selectionId;
    EMeasurementType   m_type;
    SerialNumber_t                      m_serialNumber;
    VarDef_t                            m_varDef;
    VarDef_t                            m_enabledVarDef;
    std::shared_ptr<CalibrationData>  m_calibrationData;
  };

  class CalibrationDataPalette
  {
  public:
    // initialisation
    CalibrationDataPalette();

    void setDefaultPalette();

    // set style
    std::pair<QBrush , QPen > brushAndPen(bool selected, unsigned int layer_i, unsigned int fade_i, VarId_t /*var_id*/, float normalised_value) const {

      //      if (layer_i>=s_nLayers)   layer_i = s_nLayers-1;
      if (fade_i>=s_nFadeSteps+1) fade_i  = s_nFadeSteps;

      EPenType pen_type = (selected ? kPenSelected : ( fade_i == 0 ? kPenNormal : kPenFaded) );

      if (normalised_value<0.) {
	//underflow
	return std::make_pair(QBrush((*m_valueColours)[0].light(lighten(layer_i,fade_i)),
				     m_fadeBrush[fade_i]),
			      pen(pen_type,1,fade_i));
      }
      else {

	unsigned int color_i=m_valueColours->size()-1; // overflow
	if (normalised_value<1.) {
	  color_i = static_cast<unsigned int>( normalised_value *  (m_valueColours->size()-2) + 1);
	  if ( color_i >= m_valueColours->size() ) color_i = m_valueColours->size()-1;
	}

	return std::make_pair(QBrush((*m_valueColours)[color_i].light(lighten(layer_i,fade_i)), 
				     m_fadeBrush[fade_i]),
			      pen(pen_type,1,fade_i));
      }
    }

    std::pair<QBrush , QPen > brushAndPenUnavail(unsigned int /*layer_i*/, unsigned int /*fade_i*/) const {
      return std::make_pair(QBrush(m_objectUnavailable, Qt::SolidPattern),
			    m_pen[kPenUnavail]);
    }

    std::pair<QBrush , QPen > brushAndPenUnknown(unsigned int layer_i, unsigned int fade_i) const {
	return std::make_pair(QBrush(m_specialBrush[kBrushUnknownState].color().light(lighten(layer_i,fade_i)),
				     m_specialBrush[kBrushUnknownState].style()),
			      pen( ( fade_i == 0 ? kPenNormal : kPenFaded),1,fade_i));
    }

    std::pair<QBrush , QPen > brushAndPenDisabled(unsigned int layer_i, unsigned int fade_i) const {
      QBrush disabled_brush(m_specialBrush[kBrushDisabled]);
      (void)disabled_brush.color().light(lighten(layer_i,fade_i));
      return std::make_pair(disabled_brush, pen( ( fade_i == 0 ? kPenNormal : kPenFaded),1,fade_i));
    }

    std::pair<QBrush , QPen > brushAndPen(bool selected, unsigned int layer_i, unsigned int fade_i, VarId_t var_id, State_t state) const {

      if (layer_i>=s_nLayers)   layer_i = s_nLayers-1;
      if (fade_i>=s_nFadeSteps+1) fade_i  = s_nFadeSteps;

      const ColourList_t *the_colour_list=NULL;

      std::map< VarId_t , std::shared_ptr< ColourList_t > >::const_iterator colour_iter = m_stateColours.find(var_id);
      if ( colour_iter != m_stateColours.end() ) {
	the_colour_list = colour_iter->second.get();
      }
      else {
	the_colour_list = m_defaultColours.get();
      }

      EPenType pen_type = (selected ? kPenSelected : ( fade_i == 0 ? kPenNormal : kPenFaded));

      if (state>=the_colour_list->size()) {
	return std::make_pair(QBrush(m_specialBrush[kBrushUnknownState].color().light(lighten(layer_i,fade_i)),
				     m_specialBrush[kBrushUnknownState].style()),
			      pen(pen_type,1,fade_i));
      }

      return std::make_pair(QBrush((*the_colour_list)[state].light(lighten(layer_i,fade_i)),m_fadeBrush[fade_i]),
			    pen(pen_type,1,fade_i));

    }

    QPen pen(unsigned int pen_type, unsigned int /*layer_i*/, unsigned int fade_i) const {
      assert(pen_type < kNPenTypes);
      return QPen(m_pen[pen_type].color().light(lighten(1,fade_i)),
		  m_pen[pen_type].width(),
		  m_pen[pen_type].style());
    }

    // adjust style
    void setNormalPen(const QPen &normal_pen) {
      m_pen[kPenNormal]=normal_pen;
    }

    void setSelectedPen(const QPen &selected_pen) {
      m_pen[kPenSelected]=selected_pen;
    }

    void setUnavailPen(const QPen &unavail_pen) {
      m_pen[kPenUnavail]=unavail_pen;
    }

    void setFadedPen(const QPen &faded_pen) {
      m_pen[kPenFaded]=faded_pen;
    }

    void setValueColourList ( const std::shared_ptr< std::vector< QColor > > &colour_list ) {
      assert (colour_list->size()>2);
      m_valueColours = colour_list;
    }

    const std::shared_ptr< std::vector< QColor > > getValueColourList() const {
      return m_valueColours;
    }

    void setDefaultColourList ( const std::shared_ptr< std::vector< QColor > > &colour_list ) {
      m_defaultColours = colour_list;
    }

    void setColourList ( const std::vector<VarId_t> &id, const std::shared_ptr< std::vector< QColor > > &colour_list ) {
      for (std::vector<VarId_t>::const_iterator id_iter = id.begin();
	   id_iter != id.end();
	   id_iter++) {
	m_stateColours[*id_iter]=colour_list;
      }
    }

    void setLayerLighteningFactors(int layer0, int layer1, int layer2)
    {
      m_layerLighten[0]=layer0;
      m_layerLighten[1]=layer1;
      m_layerLighten[2]=layer2;
    }

    void setFadeLighteningFactors(int fade1, int fade2)
    {
      m_fadeLighten[0]=fade1;
      m_fadeLighten[1]=fade2;
    }

    void setFadeBrushes(Qt::BrushStyle fade1, Qt::BrushStyle fade2)
    {
      m_fadeBrush[0]=fade1;
      m_fadeBrush[1]=fade2;
    }

    void setDisabledBrush( const QBrush &disabled)
    {
      m_specialBrush[kBrushDisabled]=disabled;
    }

    void setUnknownStateBrush( const QBrush &unknown_state)
    {
      m_specialBrush[kBrushUnknownState]=unknown_state;
    }

  private:

    int lighten(unsigned int layer_i, unsigned int fade_i) const {
      return m_layerLighten[layer_i]*m_fadeLighten[fade_i] / 100;
    }

    typedef std::vector< QColor > ColourList_t;

    // std::vector< std::shared_ptr< ColourList_t > >     m_colourLists;

    enum EPenType {kPenNormal, kPenSelected, kPenFaded, kPenUnavail, kNPenTypes};
    QPen                                                    m_pen[kNPenTypes];

    enum ESpecialBrushTypes {kBrushDisabled, kBrushUnknownState, kNSpecialBrushes};
    QBrush                                                  m_specialBrush[kNSpecialBrushes];

    static const unsigned int s_nLayers = 3;
    int                                                     m_layerLighten[s_nLayers];

    static const unsigned int s_nFadeSteps = 2;
    int                                                     m_fadeLighten[s_nFadeSteps];

    Qt::BrushStyle                                          m_fadeBrush[s_nFadeSteps];

    QColor                                                  m_objectUnavailable;
    std::shared_ptr< ColourList_t >                       m_defaultColours;
    std::map< VarId_t , std::shared_ptr< ColourList_t > > m_stateColours;
    std::shared_ptr< ColourList_t >                       m_valueColours;
  };

  class CalibrationDataStyle : public IItemStyle
  {
  public:
    CalibrationDataStyle(const ModuleRODDataList &data_list,
			 std::shared_ptr<DataSelection> &data_selection,
			 std::shared_ptr<CalibrationDataPalette> &palette,
			 unsigned int layer)
      : m_moduleRodDataList(data_list),
	m_dataSelection(data_selection),
	m_palette(palette),
	m_layer(layer)
    { assert(layer<3);
    }

    std::pair<QBrush , QPen > brushAndPen(bool is_selected, bool is_unfocused);

    enum EEnableStates { kItemEnabled, kItemDisabled, kParentItemDisabled };
    static VarDef_t createEnabledVarDef();
    static VarDef_t createEnabledVarDef(const std::string &var_name);

  protected:
  private:
    ModuleRODDataList   m_moduleRodDataList;
    std::shared_ptr< DataSelection > m_dataSelection;
    std::shared_ptr< CalibrationDataPalette > m_palette;
    unsigned int                       m_layer;

    unsigned int                       m_lastSelectionId;
    std::pair<QBrush,QPen>             m_lastBrushAndPen;
    static std::string                 s_enableVar;
  };

}
#endif
