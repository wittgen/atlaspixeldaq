#include "VarDefList.h"
#include "ScanMetaDataCatalogue.h"

namespace PixCon {

  const PixA::ConfigHandle    &ScanMetaData_t::scanConfig(const std::string &protected_folder_name) const { 
    std::string temp_folder_name = protected_folder_name;
    for(;;) {
      std::map<std::string, PixA::ConfigHandle>::const_iterator scan_config_iter1 =  m_scanConfig.find(temp_folder_name);
      if (scan_config_iter1 != m_scanConfig.end()) return scan_config_iter1->second;

      std::string::size_type pos = temp_folder_name.rfind("/");
      if (pos == std::string::npos) break;
      temp_folder_name.erase(pos,temp_folder_name.size()-pos);
    }

    std::map<std::string, PixA::ConfigHandle>::const_iterator scan_config_iter2 =  m_scanConfig.find("");
    if (scan_config_iter2 != m_scanConfig.end()) return scan_config_iter2->second;

    for (std::map<std::string, PixA::ConfigHandle>::const_iterator scan_config_iter3 =  m_scanConfig.begin();
	 scan_config_iter3 != m_scanConfig.end();
	 scan_config_iter3++) {
      assert( scan_config_iter3->second );
      std::cout << "INFO [ScanMetaData_t::scanConfig] have " << scan_config_iter3->first << std::endl;
    }
	 
    return s_invalidConfig;
  }

  PixA::ConfigHandle ScanMetaData_t::s_invalidConfig;

}
