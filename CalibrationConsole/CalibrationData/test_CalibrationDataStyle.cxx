#include "CalibrationDataStyle.h"
#include "CategoryList.h"

int main2()
{

  std::shared_ptr<PixCon::CalibrationData> a(new PixCon::CalibrationData);

  PixCon::CategoryList categories;
  PixCon::CategoryList::Category dcs_cat=categories.addCategory("DCS");
  PixCon::CategoryList::Category status_cat=categories.addCategory("Status");
  PixCon::CategoryList::Category analysis_cat=categories.addCategory("Analysis");

  // There are different types of values
  a->addAnalysisValue<float>(PixCon::kModuleValue, "D1A_B2_S1_M1",1,analysis_cat.addVariable("Threshold Dispersion"),0.);
  enum EScanStatus {kScanUnknown, kScanConfiguring, kScanScanning, kScanSuccess, kScanFailure};
  a->addScanValue<PixCon::State_t>(PixCon::kRodValue, "ROD_C1_S1",1,  status_cat.addVariable("Scan Status"),static_cast<PixCon::State_t>(kScanScanning));
  a->addAnalysisValue<PixCon::State_t>(PixCon::kRodValue, "ROD_C1_S1",1,  status_cat.addVariable("Analysis Status"),static_cast<PixCon::State_t>(kScanSuccess));

  // the same variables can be in several categories
  enum EAnalysisStatus {kAnaUnknown,kAnaPending,kAnaWaiting,kAnaAnalysing,kAnaSuccess,kAnaFailure};
  a->addCurrentValue<PixCon::State_t>(PixCon::kRodValue, "ROD_C1_S1", analysis_cat.addVariable( status_cat.addVariable("Analysis Status")) ,
				      static_cast<PixCon::State_t>(kAnaWaiting));


  std::shared_ptr<PixCon::CalibrationDataPalette> palette(new PixCon::CalibrationDataPalette);
  palette->setDefaultPalette();

  PixCon::VarDef_t a_def ( a->getVarDef("Analysis Status") );

  std::shared_ptr<PixCon::DataSelection> data_selection(new PixCon::DataSelection(a,PixCon::kAnalysis,1, a_def));

  std::shared_ptr<PixCon::IItemStyle> style(new PixCon::CalibrationDataStyle(PixCon::ModuleRODDataList(*a,"ROD_C1_S1","D1A_B2_S1_M1"),
									       data_selection,
									       palette,0));
  
  style->brushAndPen(false,false); // normal
  style->brushAndPen(true,false);  // object is selected
  style->brushAndPen(false,true);  // object is unfocused.

}
