/* Dear emacs, this is -*-c++-*- */
#ifndef _PixCon_ScanMetaDataCatalogue_h_
#define _PixCon_ScanMetaDataCatalogue_h_

#include <map>
#include <stdexcept>
#include <algorithm>

#include <ConfigWrapper/ConfigHandle.h>

namespace PixCon {

  typedef unsigned int SerialNumber_t;

  typedef unsigned int Revision_t;

  class MetaData_t
  {
  public:
    enum EQuality {kUndefined, kNormal, kTrash, kDebug, kNQualityStates};
    enum EStatus  {kOk, kFailed, kAborted, kUnknown, kRunning, kLoading, kLoadingFailed, kLoadingAborted, kLoadingUnknown, kNStates, kUnset};

    /** Basic meta data.
     * @todo should not store times as strings, should store the file name ?
     */
    MetaData_t(const time_t &date,
	       const std::string &name,
	       const std::string &comment,
	       EQuality quality)
      : m_date(date),
	m_name(name),
	m_comment(comment),
	m_status(kUnset)
    { setQuality(quality); }

    std::string dateString() const     { return ctime(&m_date);}
    const time_t &date() const         { return m_date;}
    const std::string &name() const    { return m_name;}
    const std::string &comment() const { return m_comment;}
    void setComment( const std::string &new_comment) { m_comment = new_comment; }

    EQuality quality() const           { return m_quality;}
    void setQuality(EQuality quality)  { m_quality = (quality>=kNQualityStates ? kUndefined : quality);}

    EStatus  status() const            { return m_status;}
    void setStatus(EStatus status)     { m_status = status;}

    void setStatus(State_t status) 
    {
      if (status>kNStates) status=kUnknown;
      m_status = static_cast<EStatus>(status);
    }

    static bool isLoading(EStatus status)     { return status>=kLoading && status<=kLoadingUnknown; }
    static bool isNotLoading(EStatus status)  { return status<=kUnknown; }
    static EStatus makeLoading(EStatus status) { if (status<=kUnknown) {return static_cast<EStatus>(status + kLoading); } else { return status; } }
    static EStatus clearLoading(EStatus status) { if (isLoading(status)) {return static_cast<EStatus>(status - kLoading); } else { return status; } }

    bool isLoading() const {
      return isLoading(m_status);
    }

    void setLoading()  { m_status = makeLoading(m_status); }
    void clearLoading() { m_status = clearLoading(m_status); }

    static EStatus combineStatus(EStatus current_status, EStatus new_status)
    {
      bool is_loading=isLoading(new_status);
      new_status = clearLoading(new_status);

      if (new_status == kUnset) return current_status;
      if (new_status>=kNStates) new_status = kUnknown;

      if (   current_status == kUnset
	  || new_status == kFailed
	  || (new_status == kUnknown && current_status == kOk) ) {
	current_status = new_status;
      }

      if (is_loading) {
	current_status = makeLoading(current_status);
      }
      return current_status;
      //      if (current_status>=kNStates) current_status = kUnknown;
    }

    static bool isTerminated(EStatus status) {
      return status<=MetaData_t::kUnknown;
    }

  protected:

    ::time_t    m_date;
    std::string m_name;
    std::string m_comment;
    EQuality    m_quality;
    EStatus     m_status;
  };

  /** scan meta data for the calibration console.
   * @todo use the ScanInfo_t structure instead ScanMetaData_t.
   */
  class ScanMetaData_t : public MetaData_t
  {
  public:
    ScanMetaData_t(const time_t &date,
		   unsigned int duration,
		   const std::string &name,
		   const std::string &comment,
		   EQuality quality,
		   const PixA::ConfigHandle &scan_config,
		   const std::string &result_file)
      : MetaData_t(date,name,comment,quality),
	m_scanConfigRevision(0),
	m_duration(duration),
	m_resultFile(result_file)
    { addScanConfig(scan_config); }

    void addAnalysis(SerialNumber_t analysis_serial_number)    { m_analysisList.push_back(analysis_serial_number);}

    void removeAnalysis(SerialNumber_t analysis_serial_number)
    {
      std::vector<SerialNumber_t>::iterator  analysis_iter = std::find(m_analysisList.begin(), m_analysisList.end(),analysis_serial_number);
      if (analysis_iter != m_analysisList.end()) {
	m_analysisList.erase(analysis_iter);
      }
    }

    std::vector<SerialNumber_t >::const_iterator beginAnalysis() const {return m_analysisList.begin(); }
    std::vector<SerialNumber_t >::const_iterator endAnalysis()   const {return m_analysisList.end(); }

    void setScanConfigTag(const std::string &config_tag)   { m_scanConfigTagName=config_tag; }
    void setScanConfigRevision(const Revision_t &revision) { m_scanConfigRevision=revision; }
    const std::string &scanConfigTag() const        { return m_scanConfigTagName; }
    Revision_t scanConfigRevision() const           { return m_scanConfigRevision; }

    void addScanConfig(const PixA::ConfigHandle &handle) {
      addScanConfig("",handle);
    }

    void addScanConfig(const std::string &protected_folder_name, const PixA::ConfigHandle &handle) {
      if (handle) {
	m_scanConfig[protected_folder_name]=handle;
      }
    }

    const PixA::ConfigHandle    &scanConfig() const { return scanConfig(""); }
    const PixA::ConfigHandle    &scanConfig(const std::string &protected_folder_name) const;

    unsigned int duration() const      { return m_duration; }
    void setDuration(unsigned int a_duration)     { m_duration = a_duration; }

    const std::string &fileName() const { return m_resultFile; }

    void setFileName(const std::string &file_name) { m_resultFile = file_name; }

  protected:
    std::string                  m_scanConfigTagName;
    Revision_t                   m_scanConfigRevision;
    std::map<std::string, PixA::ConfigHandle>  m_scanConfig;
    std::vector<SerialNumber_t > m_analysisList;
    unsigned int                 m_duration;
    static PixA::ConfigHandle    s_invalidConfig;
    std::string                  m_resultFile;
  };

  /** scan meta data for the calibration console.
   * @todo use the AnalysisInfo_t structure instead AnalysisMetaData_t.
   */
  class AnalysisMetaData_t : public MetaData_t
  {
  public:
    enum EConfigTypes {kDecision, kPostProcessing, kAnalysis, kNConfigs};

    AnalysisMetaData_t(const time_t &date,
		       const std::string &name,
		       const std::string &comment,
		       EQuality quality,
		       SerialNumber_t scan_serial_number,
		       const PixA::ConfigHandle &analysis_config,
		       const std::string &decision_name,
		       const PixA::ConfigHandle &decision_config,
		       const std::string &post_processing_name,
		       const PixA::ConfigHandle &post_processing_config)
      : MetaData_t(date,name,comment,quality),
	m_scanSerialNumber(scan_serial_number)
    {
      m_config[kAnalysis]=analysis_config;

      m_objectName[kDecision]=decision_name;
      m_config[kDecision]=decision_config;

      m_objectName[kPostProcessing]=post_processing_name;
      m_config[kPostProcessing]=post_processing_config;
    }

    AnalysisMetaData_t(const time_t &date,
		       const std::string &name,
		       const std::string &comment,
		       EQuality quality,
		       SerialNumber_t scan_serial_number,
		       const std::string  &analysis_tag,
		       Revision_t          analysis_rev,
		       const std::string  &decision_name,
		       const std::string  &decision_tag,
		       Revision_t          decision_rev,
		       const std::string  &post_processing_name,
		       const std::string  &post_processing_tag,
		       Revision_t          post_processing_rev)
      : MetaData_t(date,name,comment,quality),
	m_scanSerialNumber(scan_serial_number)
    {
      m_tagName[kAnalysis]=analysis_tag;
      m_revision[kAnalysis]=analysis_rev;

      m_objectName[kDecision]=decision_name;
      m_tagName[kDecision]=decision_tag;
      m_revision[kDecision]=decision_rev;

      m_objectName[kPostProcessing]=post_processing_name;
      m_tagName[kPostProcessing]=post_processing_tag;
      m_revision[kPostProcessing]=post_processing_rev;
    }

    SerialNumber_t scanSerialNumber()       const { return m_scanSerialNumber; }
    const std::string &analysisName()       const { return MetaData_t::name(); }
    const std::string &decisionName()       const { return m_objectName[kDecision]; }
    const std::string &postProcessingName() const { return m_objectName[kPostProcessing]; }

    void setConfig(EConfigTypes config_type, const PixA::ConfigHandle &handle) {
      assert( config_type < kNConfigs);
      m_config[config_type]=handle;
    }

    const PixA::ConfigHandle &analysisConfig()       const { return m_config[kAnalysis]; }
    const PixA::ConfigHandle &decisionConfig()       const { return m_config[kDecision]; }
    const PixA::ConfigHandle &postProcessingConfig() const { return m_config[kPostProcessing]; }

    const std::string &analysisTag()       const { return m_tagName[kAnalysis]; }
    const std::string &decisionTag()       const { return m_tagName[kDecision]; }
    const std::string &postProcessingTag() const { return m_tagName[kPostProcessing]; }

    Revision_t analysisRevision()       const { return m_revision[kAnalysis]; }
    Revision_t decisionRevision()       const { return m_revision[kDecision]; }
    Revision_t postProcessingRevision() const { return m_revision[kPostProcessing]; }


    const std::string &name() const { return MetaData_t::name(); }

    const std::string &name(EConfigTypes config_type) const {
      assert(config_type < kNConfigs);
      if (config_type == kAnalysis) {
	return analysisName();
      }
      else {
	return m_objectName[config_type];
      }
    }

    PixA::ConfigHandle configHandle(EConfigTypes config_type) const {
      assert(config_type < kNConfigs);
      return m_config[config_type];
    }
    const std::string &tagName(EConfigTypes config_type) const {
      assert(config_type < kNConfigs);
      return m_tagName[config_type];
    }

    Revision_t revision(EConfigTypes config_type) const {
      assert(config_type < kNConfigs);
      return m_revision[config_type];
    }

  private:
    SerialNumber_t m_scanSerialNumber;

    PixA::ConfigHandle m_config[kNConfigs];
    std::string m_objectName[kAnalysis];
    std::string m_tagName[kNConfigs];
    Revision_t m_revision[kNConfigs];
  };

  typedef std::map< SerialNumber_t, ScanMetaData_t >     ScanMetaData;
  typedef std::map< SerialNumber_t, AnalysisMetaData_t > AnalysisMetaData;

    /** Will be thrown if there is already meta data for meta to be added.
     */
    class MetaDataExistsAlready : public std::runtime_error
    {
    public:
      MetaDataExistsAlready(const std::string &message) : std::runtime_error(message) {};
    };

    class MetaDataMissing : public std::runtime_error
    {
    public:
      MetaDataMissing(const std::string &message) : std::runtime_error(message) {};
    };


  template <class T_MetaData_t>
  class MetaDataCatalogue
  {
  public:

    //    MetaDataCatalogue() {}
    //    ~MetaDataCatalogue() {}
    void addMetaData(SerialNumber_t serial_number, const T_MetaData_t &meta_data) {
      typename std::map< SerialNumber_t, T_MetaData_t >::iterator meta_data_iter = m_metaData.find(serial_number);
      if (meta_data_iter != m_metaData.end()) {
	throw MetaDataExistsAlready("Meta data exists already.");
      }
      m_metaData.insert(std::make_pair(serial_number, meta_data) );
    }

    void removeMetaData(SerialNumber_t serial_number) {
      typename std::map< SerialNumber_t, T_MetaData_t >::iterator meta_data_iter = m_metaData.find(serial_number);
      if (meta_data_iter != m_metaData.end()) {
	m_metaData.erase(meta_data_iter);
      }
    }

    void removeMetaDataUpTo(SerialNumber_t serial_number) {
      // typename std::map< SerialNumber_t, T_MetaData_t >::iterator iter = m_metaData.find(serial_number);
      // if (iter !=  m_metaData.end()) {
      //   m_metaData.erase( m_metaData.begin(), end_iter );
      // }
     
      typename std::map< SerialNumber_t, T_MetaData_t >::iterator iter = m_metaData.begin();
      bool reached_end = (iter == m_metaData.end() );
      while (!reached_end && iter->first <=serial_number ) {
	typename std::map< SerialNumber_t, T_MetaData_t >::iterator current_iter = iter++;
	reached_end = (iter == m_metaData.end() );
	m_metaData.erase(current_iter);
      }
    }

    const T_MetaData_t * metaDataSave(SerialNumber_t serial_number) const {
      typename std::map< SerialNumber_t, T_MetaData_t >::const_iterator meta_data_iter = m_metaData.find(serial_number);
      if (meta_data_iter == m_metaData.end()) {
	return NULL;
      }
      return &(meta_data_iter->second);
    }

    const T_MetaData_t &metaData(SerialNumber_t serial_number) const {
      typename std::map< SerialNumber_t, T_MetaData_t >::const_iterator meta_data_iter = m_metaData.find(serial_number);
      if (meta_data_iter == m_metaData.end()) {
	//@todo throw dedicated exception, to allow for more specific exception catching.
	throw MetaDataMissing("Meta data does not exist.");
      }
      return meta_data_iter->second;
    }

    const T_MetaData_t *find(SerialNumber_t serial_number) const {
      typename std::map< SerialNumber_t, T_MetaData_t >::const_iterator meta_data_iter = m_metaData.find(serial_number);
      if (meta_data_iter == m_metaData.end()) return NULL;
      return &meta_data_iter->second;
    }

    T_MetaData_t *find(SerialNumber_t serial_number) {
      typename std::map< SerialNumber_t, T_MetaData_t >::iterator meta_data_iter = m_metaData.find(serial_number);
      if (meta_data_iter == m_metaData.end()) return NULL;
      return &meta_data_iter->second;
    }

    typename std::map< SerialNumber_t, T_MetaData_t >::const_iterator begin() const {
      return m_metaData.begin();
    }

    typename std::map< SerialNumber_t, T_MetaData_t >::iterator begin()  {
      return m_metaData.begin();
    }

    typename std::map< SerialNumber_t, T_MetaData_t >::const_iterator end() const {
      return m_metaData.end();
    }

    void erase(typename std::map< SerialNumber_t, T_MetaData_t >::iterator iter) {
      m_metaData.erase(iter);
    }

  protected:
  private:

    std::map< SerialNumber_t, T_MetaData_t > m_metaData;

  };

  typedef MetaDataCatalogue<ScanMetaData_t> ScanMetaDataCatalogue;
  typedef MetaDataCatalogue<AnalysisMetaData_t> AnalysisMetaDataCatalogue;
}

#endif

