/* Dear emacs, this is -*-c++-*- */
#ifndef _CalibrationDataManager_H_
#define _CalibrationDataManager_H_

#include "CalibrationData.h"
#include "CalibrationDataUtil.h"
#include <ConfigWrapper/Connectivity.h>
#include <ConfigWrapper/ConnectivityManager.h>
#include "ScanMetaDataCatalogue.h"
#include "Lock.h"
#include <utility>

namespace PixLib {
  class PixDisable;
}

namespace PixA {
  class DisabledListRoot;
}

namespace PixCon {

  class IConnItemSelection;
  class ICalibrationDataFunctor;

  /** Exception which gets thrown in case a connectivity is missing.
   */
  class MissingConnectivity : public std::runtime_error {
  public:
    MissingConnectivity(const std::string &message) : std::runtime_error(message) {}
  };

  class CalibrationDataManager
  {
  private:
    typedef std::map<SerialNumber_t, PixA::ConnectivityRef > ConnMap_t;
  public:

    CalibrationDataManager()
      : m_calibrationData(new CalibrationData),
	m_currentStatus(MetaData_t::kUnset)
    {
      m_connectivity.resize(kNMeasurements);
      m_connectivity[kCurrent][0]=PixA::ConnectivityRef();
    }

    ~CalibrationDataManager() {}

    /** Get a pointer to the calibration data container.
     */
    std::shared_ptr<CalibrationData> calibrationDataPtr() { return m_calibrationData; }

    /** Get the calibration data container.
     */
    CalibrationData &calibrationData() { return *m_calibrationData; }

    /** Get the calibration data container (read only).
     */
    const CalibrationData &calibrationData() const { return *m_calibrationData; }

    /** Get the mutex to protect the access to the calibration data.
     */
    PixCon::Mutex &calibrationDataMutex() { return m_calibrationDataMutex; }

    /** Change the current connectivity ot the one specified by the tags and revision.
     * @param conn_tag the tag of the connectivity.
     * @param alias_tag the tag of the aliases.
     * @param payload_tag the tag of the payload.
     * @param cfg_tag the tag of the configurations.
     * @param rev the revision of the configurations.
     */
    void changeCurrentConnectivity(const std::string &object_tag_name,
				   const std::string &conn_tag,
				   const std::string &alias_tag,
				   const std::string &data_tag,
				   const std::string &cfg_tag,
				   PixA::Revision_t rev,
				   bool force_recreate = false) {

      setConnectivity(kCurrent,0,PixA::ConnectivityManager::getConnectivity( object_tag_name,
									     conn_tag,
									     alias_tag,
									     data_tag,
									     cfg_tag,
									     rev ),
		      force_recreate);
    }

    /** Change the current connectivity ot the one specified by the tags and revision.
     * @param conn_tag the tag of the connectivity.
     * @param alias_tag the tag of the aliases.
     * @param payload_tag the tag of the payload.
     * @param cfg_tag the tag of the configurations.
     * @param mod_cfg_tag the tag of the configurations.
     * @param rev the revision of the configurations.
     */
    void changeCurrentConnectivity(const std::string &object_tag_name,
				   const std::string &conn_tag,
				   const std::string &alias_tag,
				   const std::string &data_tag,
				   const std::string &cfg_tag,
				   const std::string &mod_cfg_tag,
				   PixA::Revision_t rev,
				   bool force_recreate = false) {

      setConnectivity(kCurrent,0,PixA::ConnectivityManager::getConnectivity( object_tag_name,
									     conn_tag,
									     alias_tag,
									     data_tag,
									     cfg_tag,
									     mod_cfg_tag,
									     rev),
		      force_recreate);
    }

    void setScanConnectivity(SerialNumber_t serial_number, const PixA::ConnectivityRef &a_connectivity)
    {
      setConnectivity(kScan, serial_number, a_connectivity);
    }


    /** Get the current connectivity.
     */
    PixA::ConnectivityRef currentConnectivity() {
      try {
	auto result=connectivity(kCurrent,0);
	return result;
      } catch(...) { throw MissingConnectivity("Missing Connectivity"); }
    }

    /** Get the current connectivity (read only).
     * @return a reference of the current connectivity.
     */
    const PixA::ConnectivityRef currentConnectivity() const   {
       try {
	auto result= connectivity(kCurrent,0);
	return result;
       } catch(...) { throw MissingConnectivity("Missing Connectivity"); }
    }

    /** Get the connectivity of the given scan (read only).
     * @param serial_number the serial number of the scan.
     * @return a reference of the connectivity of the specified scan.
     */
    PixA::ConnectivityRef scanConnectivity(SerialNumber_t serial_number)  {
      try {
	auto result= connectivity(kScan,serial_number);
	return result;
      } catch(...)  { throw MissingConnectivity("Missing Connectivity"); }
    }

    /** Get the connectivity of the given scan (read only).
     * @param serial_number the serial number of the scan.
     * @return a reference of the connectivity of the specified scan.
     */
    const PixA::ConnectivityRef scanConnectivity(SerialNumber_t serial_number) const {
      try {
      auto result=connectivity(kScan,serial_number);
      return result;
      } catch(...)  { throw MissingConnectivity("Missing Connectivity"); }
    }

    /** Get the connectivity of the given analysis.
     * @param serial_number the serial number of the analysis.
     * @return a reference of the connectivity of the specified analysis.
     */
    PixA::ConnectivityRef analysisConnectivity(SerialNumber_t serial_number) {
      try {
	auto result=connectivity(kAnalysis,serial_number);
	return result;
      } catch(...)  { throw MissingConnectivity("Missing Connectivity"); }
    }

    /** Get the connectivity of the given analysis (read only).
     * @param serial_number the serial number of the analysis.
     * @return a reference of the connectivity of the specified analysis.
     */
    const PixA::ConnectivityRef analysisConnectivity(SerialNumber_t serial_number) const {
      try {
      auto result=connectivity(kAnalysis,serial_number);
      return result;
      } catch(...)  { throw MissingConnectivity("Missing Connectivity"); }
    }

    /** Set the enable state of a crate and propagate the state to its children (i.e. RODs, etc.).
     * NOTE: This method only manipulates the calibrtion data container, but does not update any views.
     */
    void setCrateEnable(const std::string &crate_conn_name, EMeasurementType measurement_type, SerialNumber_t serial_number, bool enabled);

    /** Set the enable state of a ROD and propagate the state to its children (i.e. Pp0s etc.).
     * NOTE: This method only manipulates the calibrtion data container, but does not update any views.
     */
    void setRodEnable(const std::string &rod_conn_name, EMeasurementType measurement_type, SerialNumber_t serial_number, bool enabled);

    /** Set the enable state of the modules of a Pp0.
     * NOTE: This method only manipulates the calibrtion data container, but does not update any views.
     */
    void setPp0Enable(const std::string &pp0_conn_name, EMeasurementType measurement_type, SerialNumber_t serial_number, bool enabled);

    /** Set the enable state of a module.
     * NOTE: This method only manipulates the calibrtion data container, but does not update any views.
     */
    void setModuleEnable(const std::string &rod_conn_name, EMeasurementType measurement_type, SerialNumber_t serial_number, bool enabled);

    /** Set the allocated state of a crate and propagate the state to its children (i.e. RODs, etc.).
     * NOTE: This method only manipulates the calibrtion data container, but does not update any views.
     */
    void setCrateAllocated(const std::string &crate_conn_name, EMeasurementType measurement_type, SerialNumber_t serial_number, bool allocated);

    /** Set the allocated state of a ROD and propagate the state to its children (i.e. Pp0s etc.).
     * NOTE: This method only manipulates the calibrtion data container, but does not update any views.
     */
    void setRodAllocated(const std::string &rod_conn_name, EMeasurementType measurement_type, SerialNumber_t serial_number, bool allocated);

    //    void setPp0Allocated(const std::string &pp0_conn_name, EMeasurementType measurement_type, SerialNumber_t serial_number, bool allocated);

    /** Set the enable state of a module.
     * NOTE: This method only manipulates the calibrtion data container, but does not update any views.
     */
    void setModuleAllocated(const std::string &rod_conn_name, EMeasurementType measurement_type, SerialNumber_t serial_number, bool allocated);

    /** Disable the obejcts of the given measurement which are disabled in the pix disable.
     */
    void setEnable(EMeasurementType measurement_type,
		   SerialNumber_t serial_number,
		   const PixA::DisabledListRoot &disabled_list_root);

    /** Operate on each connectivity object.
     */
    void forEach(EMeasurementType measurement_type,
		 SerialNumber_t serial_number,
		 const PixA::DisabledListRoot &disabled_list_root,
		 ICalibrationDataFunctor &functor,
		 bool create);

    /** Operate on each connectivity object.
     */
    void forEach(EMeasurementType measurement_type,
		 SerialNumber_t serial_number,
		 ICalibrationDataFunctor &functor,
		 bool create);

    /** Create a pix disable from the enable states of the given measurment.
     * @return shared pointer to a pix disable object or a NULL pointer in case of errors.
     */
    std::shared_ptr<PixLib::PixDisable>  getPixDisable(EMeasurementType measurement_type,
							 SerialNumber_t serial_number,
							 const IConnItemSelection &selection,
                                                         bool ignore_tim=true,
                                                         bool ignore_enable=false);

    /** Create a pix disable from the enable states of the given measurment.
     * @param measurement type the measurement type : scan, analysis, current.
     * @param serial_number the serial number of the measurement.
     * @param ignore_tim do not consider the tim when assembling a pix_disable
     * @return shared pointer to a pix disable object or a NULL pointer in case of errors.
     */
    std::shared_ptr<PixLib::PixDisable>  getPixDisable(EMeasurementType measurement_type,
							 SerialNumber_t serial_number,
                                                         bool ignore_tim=true);

    /** Create a string containing all enabled RODs and modules.
     * @param measurement type the measurement type : scan, analysis, current.
     * @param serial_number the serial number of the measurement.
     */
    std::string enableString(EMeasurementType measurement_type,
			     SerialNumber_t serial_number);

    /** Copy the current connectivity to the specified scan.
     * @param serial_number the serial number of the scan.
     */
    void copyCurrentConnectivityToScan(SerialNumber_t scan_serial_number) {
      copyConnectivity(kCurrent,0, kScan,scan_serial_number);
    }

    /** Copy the  connectivity of the specified scan to the specified analysis.
     * @param scan_serial_number the serial number of the scan.
     * @param analysis_serial_number the serial number of the analysis to which the connectivity gets copied.
     */
    void copyScanConnectivityToAnalysis(SerialNumber_t scan_serial_number, SerialNumber_t analysis_serial_number) {
      copyConnectivity(kScan, scan_serial_number, kAnalysis, analysis_serial_number);
    }

    /** Remove the connectivty and the calibration values for the given scan from memory.
     */
    void removeScan(SerialNumber_t scan_serial_number);

    /** Remove the connectivty and the calibration values for the given analyisis from memory.
     */
    void removeAnalysis(SerialNumber_t analysis_serial_number);

    /** Remove the connectivty and the calibration values for the given scan and all prior scans from memory.
     */
    void removeScansUpTo(SerialNumber_t scan_serial_number);

    /** Remove the connectivty and the calibration values for the given analyisis and all prior analyses from memory.
     */
    void removeAnalysesUpTo(SerialNumber_t analysis_serial_number) {
      removeUpTo(kAnalysis, analysis_serial_number);
      m_calibrationData->removeUpTo(kAnalysis, analysis_serial_number);
      m_analysisMetaData.removeMetaDataUpTo(analysis_serial_number);
    }

    /** Remove or reset all current values from the data container.
     */
    void resetCurrent() {
      m_calibrationData->resetCurrent();
    }

    const PixA::ConnectivityRef connectivity(EMeasurementType measurement_type, SerialNumber_t serial_number) {
      assert( static_cast<unsigned int>(measurement_type) < m_connectivity.size() );
      try {
	if (measurement_type == kAnalysis) {
	  // use the connectivity of the associated scan
	  const AnalysisMetaData_t *analysis_meta_data = m_analysisMetaData.find(serial_number);
	  if (analysis_meta_data) {
	    const PixA::ConnectivityRef scan_connectivity(connectivity(kScan, analysis_meta_data->scanSerialNumber()));
	    if (scan_connectivity) {
	      return scan_connectivity;
	    }
	  }
	}
  
      // As fall back use use the connectivity of the analysis
      // @todo remove fall-back ?
      ConnMap_t::iterator conn_iter = m_connectivity[measurement_type].find(serial_number);
      if (conn_iter == m_connectivity[measurement_type].end()) {
	std::stringstream message;
	message << "ERROR [CalibrationDataManager::connectivity] No connectivity for type " << measurement_type << " and serial number " << serial_number << ".";
	throw MissingConnectivity(message.str());
      }
      return conn_iter->second;
      } catch(...)  {throw MissingConnectivity("MissingConnectivity");}
    }

    /** Add scan meta data.
     * @todo use the ScanInfo_t structure instead ScanMetaData_t.
     */
    void addScan(SerialNumber_t scan_serial_number,
		 const time_t &date,
		 unsigned int duration,
		 const std::string &scan_preset_name,
		 const std::string &comment,
		 MetaData_t::EQuality quality,
		 const PixA::ConfigHandle &scan_config,
		 const std::string &file_name,
		 const PixA::ConnectivityRef &connectivity)
    {
      if (connectivity) {
	setScanConnectivity(scan_serial_number, connectivity);
	addScanMetaData(scan_serial_number, ScanMetaData_t(date, duration, scan_preset_name, comment, quality, scan_config, file_name) );
      }
    }

    /** Add the analysis meta data.
     * @todo use the AnalysisInfo_t structure instead of AnalysisMetaData_t.
     */
    void addAnalysis(SerialNumber_t analysis_serial_number,
		     const time_t &date,
		     const std::string &analysis_name,
		     const std::string &comment,
		     MetaData_t::EQuality quality,
		     SerialNumber_t scan_serial_number,
		     const PixA::ConfigHandle &analysis_config,
		     const std::string &decision_name,
		     const PixA::ConfigHandle &decision_config,
		     const std::string &post_processing_name,
		     const PixA::ConfigHandle &post_processing_config)
    {
      ScanMetaData_t *scan_meta_data = m_scanMetaData.find(scan_serial_number);
      if (!scan_meta_data) {
	// @todoneed to load scan meta data.!
	std::cout << "FATAL [CalibrationDataManager::addAnalysis] Scan meta data missing for scan " << makeSerialNumberString(kScan,scan_serial_number)
		  << " referenced by analysis" << makeSerialNumberString(kAnalysis,analysis_serial_number) << "." << std::endl;
	return;
      }
      scan_meta_data->addAnalysis(analysis_serial_number);

      addAnalysisMetaData(analysis_serial_number, AnalysisMetaData_t(date,
								     analysis_name,
								     comment,
								     quality,
								     scan_serial_number,
								     analysis_config,
								     decision_name,
								     decision_config,
								     post_processing_name,
								     post_processing_config));
      m_calibrationData->copyScanEnableStateToAnalysis(scan_serial_number, analysis_serial_number);
    }

    void addAnalysis(SerialNumber_t analysis_serial_number,
		     const time_t &date,
		     const std::string &analysis_name,
		     const std::string &comment,
		     MetaData_t::EQuality quality,
		     SerialNumber_t scan_serial_number,
		     const std::string  &analysis_tag,
		     Revision_t          analysis_rev,
		     const std::string  &decision_name,
		     const std::string  &decision_tag,
		     Revision_t          decision_rev,
		     const std::string  &post_processing_name,
		     const std::string  &post_processing_tag,
		     Revision_t          post_processing_rev)
    {
      ScanMetaData_t *scan_meta_data = m_scanMetaData.find(scan_serial_number);
      if (!scan_meta_data) {
	// @todoneed to load scan meta data.!
	std::cout << "FATAL [CalibrationDataManager::addAnalysis] Scan meta data missing for scan " << makeSerialNumberString(kScan,scan_serial_number)
		  << " referenced by analysis" << makeSerialNumberString(kAnalysis,analysis_serial_number) << "." << std::endl;
	return;
      }
      scan_meta_data->addAnalysis(analysis_serial_number);

      addAnalysisMetaData(analysis_serial_number, AnalysisMetaData_t(date,
								     analysis_name,
								     comment,
								     quality,
								     scan_serial_number,
								     analysis_tag,
								     analysis_rev,
								     decision_name,
								     decision_tag,
								     decision_rev,
								     post_processing_name,
								     post_processing_tag,
								     post_processing_rev));
      m_calibrationData->copyScanEnableStateToAnalysis(scan_serial_number, analysis_serial_number);

    }

    const ScanMetaDataCatalogue &scanMetaDataCatalogue() const{
      return m_scanMetaData;
    }

    const AnalysisMetaDataCatalogue &analysisMetaDataCatalogue() const{
      return m_analysisMetaData;
    }

    ScanMetaData_t &scanMetaData(SerialNumber_t scan_serial_number) {
      ScanMetaData_t *scan_meta_data=m_scanMetaData.find(scan_serial_number);
      if (!scan_meta_data) {
	throw  MetaDataMissing("Meta data does not exist.");
      }
      else {
	return *scan_meta_data;
      }
    }

    AnalysisMetaData_t &analysisMetaData(SerialNumber_t analysis_serial_number) {
      AnalysisMetaData_t *analysis_meta_data=m_analysisMetaData.find(analysis_serial_number);
      if (!analysis_meta_data) {
	throw  MetaDataMissing("Meta data does not exist.");
      }
      else {
	return *analysis_meta_data;
      }
    }



    /** Update the  status stored for the given scan or analysis in the meta data catalogue
     */
    void updateStatus(EMeasurementType measurement_type, SerialNumber_t serial_number, State_t status);

    /** Return the current status stored in the meta data catalogue for the given scan or analysis.
     */
    void updateStatusSafe(EMeasurementType measurement_type, SerialNumber_t serial_number, State_t status) {
      Lock lock(m_calibrationDataMutex);
      return updateStatus(measurement_type, serial_number, status);
    }

    /** Update the  status stored for the given scan or analysis in the meta data catalogue
     */
    void addScanConfig(EMeasurementType measurement_type,
		       SerialNumber_t serial_number,
		       const std::string &folder_name,
		       PixA::ConfigHandle &pix_scan_config);

    /** Get the scan configuration for the given measurement.
     * @param measurement_type the measurement_type 
     * @param serial_number the serial number of the measurement
     * @param folder_name the name of the folder
     */
    PixA::ConfigHandle scanConfig(EMeasurementType measurement_type,
				  SerialNumber_t serial_number,
				  const std::string &folder_name);

    /** Return the current status stored in the meta data catalogue for the given scan or analysis.
     */
    State_t status(EMeasurementType measurement_type, SerialNumber_t serial_number) const {
      switch (measurement_type) {
      case kScan: {
	const ScanMetaData_t *scan_meta_data = m_scanMetaData.find(serial_number);
	if (scan_meta_data) {
	  return scan_meta_data->status();
	}
      }
      case kAnalysis: {
	const AnalysisMetaData_t *analysis_meta_data = m_analysisMetaData.find(serial_number);
	if (analysis_meta_data) {
	  return analysis_meta_data->status();
	}
      }
      case kCurrent: {
	return m_currentStatus;
      }
      case kNMeasurements: {
	// to make gcc happy
      }
      }
      return MetaData_t::kUnknown;
    }


    /** Return the current status stored in the meta data catalogue for the given scan or analysis.
     */
    State_t getStatusSafe(EMeasurementType measurement_type, SerialNumber_t serial_number) const {
      Lock lock(m_calibrationDataMutex);
      return status(measurement_type, serial_number);
    }


    /** Get the serial number of the scan associated to the given measurement.
     * If the measurement is actually a scan than the associated scan is the given scan itself.
     * If the measurement is the current measurement then there is no associated scan and zero is returned.
     * If the measurement is an analysis then the serial number of the analysed scan is returned.
     */
    SerialNumber_t scanSerialNumber(EMeasurementType measurement_type, SerialNumber_t serial_number)
    {
      switch(measurement_type) {
      case kScan:
	return serial_number;
	break;
      case kAnalysis: {
	const AnalysisMetaData_t *analysis_meta_data = m_analysisMetaData.find(serial_number);
	if (analysis_meta_data) {
	  return analysis_meta_data->scanSerialNumber();
	}

	// @todo do proper exception handling
	std::cerr << "ERROR [CalibrationDataManager::scanSerialNumber] No meta data for analysis " << serial_number << "." << std::endl;
	break;
      }
      case kCurrent:
	break;
      default:
	// paranoia check
	assert( measurement_type < kNMeasurements );
      }
      return 0;
    }

    void setScanConfigVector(std::vector <PixA::ConfigHandle> vconfig){
      m_vconfig = vconfig;
    }

    std::vector <PixA::ConfigHandle> getScanConfigVector(){
      return m_vconfig;
    }

  protected:

    PixA::ConnectivityRef connectivity(EMeasurementType measurement_type, SerialNumber_t serial_number) const {
      return const_cast<CalibrationDataManager *>(this)->connectivity(measurement_type, serial_number);
    }

    void copyConnectivity(EMeasurementType src_measurement_type, 
			  SerialNumber_t src_serial_number,
			  EMeasurementType dest_measurement_type, 
			  SerialNumber_t dest_serial_number) {

      assert(dest_measurement_type < kNMeasurements );
      m_connectivity[dest_measurement_type][dest_serial_number] = connectivity(src_measurement_type, src_serial_number);

    }

    /** Propagate the enable status of a crate.
     */
    void propagateCrateEnable(const PixA::RodList &rod_list,
			      EMeasurementType measurement_type,
			      SerialNumber_t serial_number,
			      bool parent_enabled);

    /** Propagate the enable status of a rod.
     */
    void propagateRodEnable(const PixA::Pp0List &pp0_list,
			    EMeasurementType measurement_type,
			    SerialNumber_t serial_number,
			    bool parent_enabled);

    /** Propagate the enable status of a pp0.
     */
    void propagatePp0Enable(const PixA::ModuleList &module_list,
			    EMeasurementType measurement_type,
			    SerialNumber_t serial_number,
			    bool parent_enabled);

    /** Propagate the allocated status of a crate.
     */
    void propagateCrateAllocated(const PixA::RodList &rod_list,
				 EMeasurementType measurement_type,
				 SerialNumber_t serial_number,
				 bool parent_enabled);

    /** Propagate the allocated status of a rod.
     */
    void propagateRodAllocated(const PixA::Pp0List &pp0_list,
			       EMeasurementType measurement_type,
			       SerialNumber_t serial_number,
			       bool parent_enabled);

    /** Propagate the allocated status of a pp0.
     */
    void propagatePp0Allocated(const PixA::ModuleList &module_list,
			       EMeasurementType measurement_type,
			       SerialNumber_t serial_number,
			       bool parent_enabled);

    /** Remove the connectivity used for the given measurement.
     */
    void remove(EMeasurementType measurement_type, SerialNumber_t serial_number)
    {
      assert(measurement_type < kNMeasurements );
      ConnMap_t::iterator conn_iter = m_connectivity[measurement_type].find(serial_number);
      if (conn_iter != m_connectivity[measurement_type].end()) {
	m_connectivity[measurement_type].erase(conn_iter);
      }
    }

    /** Remove all connectivities used for the all measurement prior and including the measurement of the given serial number.
     */
    void removeUpTo(EMeasurementType measurement_type, SerialNumber_t serial_number)
    {
      assert(measurement_type < kNMeasurements );
      ConnMap_t::iterator end_iter = m_connectivity[measurement_type].find(serial_number);
      if (end_iter != m_connectivity[measurement_type].end()) {
	m_connectivity[measurement_type].erase(m_connectivity[measurement_type].begin(),end_iter);
      }
    }

    /** Add scan meta data to the catalogue.
     */
    void addScanMetaData(SerialNumber_t scan_serial_number, const ScanMetaData_t &scan_meta_data) {
      m_scanMetaData.addMetaData(scan_serial_number, scan_meta_data);
    }

    /** Add analysis meta data to the catalogue.
     */
    void addAnalysisMetaData(SerialNumber_t analysis_serial_number, const AnalysisMetaData_t &analysis_meta_data) {
      m_analysisMetaData.addMetaData(analysis_serial_number, analysis_meta_data);
    }

    /** Set the connectivity for the given measurement.
     */
    void setConnectivity(EMeasurementType measurement_type,
			 SerialNumber_t serial_number,
			 const PixA::ConnectivityRef &a_connectivity,
			 bool force_recreate = false);

  void operateOnObject( EConnItemType item_type,
			const std::string &conn_name,
			const std::pair<EMeasurementType,SerialNumber_t> &measurement,
			ICalibrationDataFunctor &functor,
			CalibrationData::EnableState_t new_enable_state,
			bool create);

  private:
    std::shared_ptr<CalibrationData> m_calibrationData;
    mutable PixCon::Mutex              m_calibrationDataMutex;

    std::vector< ConnMap_t >  m_connectivity;

    ScanMetaDataCatalogue     m_scanMetaData;
    AnalysisMetaDataCatalogue m_analysisMetaData;
    MetaData_t::EStatus       m_currentStatus;

    std::vector <PixA::ConfigHandle> m_vconfig;
  };


}
#endif
