#include <stdexcept>
#include <sstream>
#include "TimedValue.h"

namespace PixCon {

  void TimedValueBase::ExceptionTimeBaseTooLate(TimeType_t time, TimeType_t base) {
    std::stringstream message;
    message << "FATAL [TimedValue::setValue] time stamp " << time << " is earlier than the base timestamp " << base << ".";
    std::runtime_error(message.str());
  }

}
