#include "CalibrationDataUtil.h"
#include <sstream>
#include <iomanip>
#include <cassert>

namespace PixCon {

  const std::string s_currentMeasurement("Present Value");

  std::string makeSerialNumberString(EMeasurementType measurement_type, SerialNumber_t serial_number) {
    assert(measurement_type < kNMeasurements);

    static const char *s_measurementTypeHeader[kNMeasurements]={"","S","A"};
    std::stringstream serial_number_string;

    if (measurement_type==kCurrent) {
      return s_currentMeasurement;
    }
    else {
      unsigned int million = (serial_number % 1000000000)/1000000;
      unsigned int thousand = (serial_number % 1000000)/1000;
      unsigned int one = (serial_number % 1000);
      serial_number_string << s_measurementTypeHeader[measurement_type]
			   << std::setfill('0');
      if (serial_number>=1000000000) {
	serial_number_string << std::setw(1) << serial_number / 1000000000
			     << "-";
      }
      if (serial_number>=1000000) {
	serial_number_string << std::setw(3) << million
			     << "-";
      }
      serial_number_string << std::setw(3) << thousand
			   << "-"
			   << std::setw(3) << one;
      return serial_number_string.str();
    }

  }

  std::string makeSerialNumberName(EMeasurementType measurement_type, SerialNumber_t serial_number) {
    assert(measurement_type < kNMeasurements);

    static const char *s_measurementTypeHeader[kNMeasurements]={"","S","A"};
    std::stringstream serial_number_string;

    if (measurement_type==kCurrent) {
      return "0";
    }
    else {
      serial_number_string << s_measurementTypeHeader[measurement_type]
			   << std::setfill('0') << std::setw(9) << serial_number;
      return serial_number_string.str();
    }
  }


  std::pair<EMeasurementType, SerialNumber_t> stringToSerialNumber(const std::string &serial_number_string)
  {
    //           1
    // 012345678901234
    //  1 123 123 123
    // Sx-xxx-xxx-xxx

    std::pair<EMeasurementType,SerialNumber_t> ret;
    
    unsigned int pos_i=0;
    if (serial_number_string[pos_i++]=='S') {
      ret.first = PixCon::kScan;
    }
    else {
      if (serial_number_string == s_currentMeasurement) {
	ret.first = kCurrent;
	ret.second = 0;
	return ret;
      }
      ret.first = PixCon::kAnalysis;
    }
    ret.second = 0 ;

    if (serial_number_string.size()>2 && serial_number_string[pos_i+1]=='-') {
      ret.second = static_cast<PixCon::SerialNumber_t>( atoi(&(serial_number_string[pos_i])) );
      pos_i+=2;
    }

    while (pos_i < serial_number_string.size()) {
      ret.second *= 1000;
      ret.second +=static_cast<PixCon::SerialNumber_t>( atoi(&(serial_number_string[pos_i])) );
      if (pos_i+3 >= serial_number_string.size() || serial_number_string[pos_i+3]!='-') {
	break;
      }
      pos_i+=4;
    }
    return ret;
  }

}
