#ifndef _PixCon_guessConnItemType_h_
#define _PixCon_guessConnItemType_h_

#include <ConnItemBase.h>
#include <string>

namespace PixCon {
  // guess the connectivity item type from the connectivity name
  EConnItemType guessConnItemType(const std::string &connectivity_name);
}


#endif
