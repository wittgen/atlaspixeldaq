#ifndef _PixCon_CalibrationDataTypes_h_
#define _PixCon_CalibrationDataTypes_h_

#include <ConnItemBase.h>

namespace PixCon {

    typedef unsigned int SerialNumber_t;
    enum EMeasurementType {kCurrent, kScan, kAnalysis, kNMeasurements};

}
#endif
