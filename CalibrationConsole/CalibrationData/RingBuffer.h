#ifndef _PixCon_RingBuffer_t_hh_
#define _PixCon_RingBuffer_t_hh_

#include <vector>

namespace PixCon {

  template <class T, class T_ref >

  /** A ring buffer container.
   * To iterate over the container :
   * <verb>
   *  for(typename std::vector<T>::const_iterator iter = a.begin();
   *	  iter != a.end();
   *	  a.const_increment(iter)) {
   *      // ...
   *  }
   *</verb>
   * or
   * <verb>
   *  for(typename std::vector<T>::iterator iter = a.begin();
   *	  iter != a.end();
   *	  a.increment(iter)) {
   *      // ...
   *  }
   *</verb>
   */
  class RingBuffer_t {
  public:
    RingBuffer_t(unsigned int size) 
      : m_ring(size),
	m_writeIter(m_ring.begin())
    { 
      typename std::vector<T>::iterator iter = m_writeIter;
      while (iter != m_ring.end()) {
	m_writeIter = iter;
	iter++;
      }
      assert(size>1); 
    }

    RingBuffer_t(const RingBuffer_t<T,T_ref> &a) {
      m_ring.resize(a.m_ring.size());
      m_writeIter = m_ring.begin();

      for(typename std::vector<T>::const_iterator iter = a.begin();
	  iter != a.end();
	   ) {
	assert(m_writeIter != m_ring.end());

	*m_writeIter = *iter;
	++m_writeIter;

	iter=const_increment(iter);
      }

    }


    /** Add a new element to the ring buffer.
     * This function needs to be protected with a mutex, in case multiple threads add
     * elements simultaneously.
     */
    bool addElement( const T_ref elm ) {
      typename std::vector<T>::iterator new_elm = m_writeIter;
      ++new_elm;
      if (new_elm ==m_ring.end()) new_elm=m_ring.begin();
      m_writeIter = new_elm;
      *new_elm = elm;
      return true;
    }

    /** Return an iterator which points to the first element in the ring.
     * At startup the first element is not set to anything useful.
     */
    typename std::vector<T>::const_iterator begin() const { return const_increment(m_writeIter); }

    /** Return an iterator which points to the first element in the ring.
     * At startup the first element is not set to anything useful.
     */
    typename std::vector<T>::iterator begin() { return increment(m_writeIter); }


    /** Return an iterator which points to the last element in the ring.
     */
    typename std::vector<T>::const_iterator end()   const { return const_increment(m_writeIter); }

    /** Return an iterator which points to the last element in the ring.
     */
    typename std::vector<T>::iterator end()   { return increment(m_writeIter); }


    /** Return an iterator which points to the last element in the ring.
     */
    const T &newest()   const { 
      assert( m_writeIter != m_ring.end());
      //typename std::vector<T>::const_iterator the_iter = m_writeIter;
      //       typename std::vector<T>::const_iterator the_reverse_iter;
      //       if (m_writeIter==m_ring.begin()) {
      // 	the_reverse_iter = m_ring.end();
      //       }
      //       else {
      // 	the_reverse_iter = m_writeIter;
      //       }
      //       assert( the_reverse_iter != m_ring.begin());
      //       --the_reverse_iter;
      return *m_writeIter;
    }

    /** increment the given iterator.
     * The idea that the iterator is incremented from @ref begin to @ref end.
     * 
     */
    typename std::vector<T>::const_iterator const_increment(typename std::vector<T>::const_iterator src) const {
      assert( src != m_ring.end());
      src++;
      if (src==m_ring.end()) src=m_ring.begin();
      return src;
    }

    typename std::vector<T>::iterator increment(typename std::vector<T>::iterator src) {
      assert( src != m_ring.end());
      src++;
      if (src==m_ring.end()) src=m_ring.begin();
      return src;
    }

  private:
    std::vector<T> m_ring;
    typename std::vector<T>::iterator m_writeIter;
  };

}

#endif
