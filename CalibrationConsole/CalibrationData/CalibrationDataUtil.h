#ifndef _PixCon_CalibrationDataUtil_h_
#define _PixCon_CalibrationDataUtil_h_

#include "CalibrationDataTypes.h"
#include <string>

namespace PixCon {
  std::string makeSerialNumberString(EMeasurementType measurement_type, SerialNumber_t serial_number);
  std::pair<EMeasurementType, SerialNumber_t> stringToSerialNumber(const std::string &serial_number_string);

  std::string makeSerialNumberName(EMeasurementType measurement_type, SerialNumber_t serial_number);
}

#endif
