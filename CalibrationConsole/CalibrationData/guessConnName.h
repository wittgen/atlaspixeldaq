#ifndef _PixCon_guessConnName_h_
#define _PixCon_guessConnName_h_

#include <string>
#include <utility>
namespace PixA {
  class ConnectivityRef;
}

namespace PixCon {

  std::pair<std::string,std::string::size_type> guessConnName(PixA::ConnectivityRef &conn,
							      const std::string &name,
							      std::string::size_type pos);


}

#endif
