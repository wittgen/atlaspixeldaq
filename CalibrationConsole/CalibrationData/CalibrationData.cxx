#include "CalibrationData.h"
#include <algorithm>

namespace PixCon {

  VarDefList *VarDefList::s_instance=NULL;

  VarDefBase_t VarDef_t::s_invalid;

  void CalibrationData::varList(EMeasurementType measurement_type, SerialNumber_t serial_number, std::set<std::string> &var_list_out)
  {
    if (isValid(measurement_type, serial_number)) {
      std::set<VarId_t> var_list_temp;
      for (std::map<std::string, ConnObjDataList_t >::iterator conn_iter = m_connObjDataList.begin();
          conn_iter != m_connObjDataList.end();
          conn_iter++) {
       ConnObjMeasurementList_t::iterator data_iter = conn_iter->second[measurement_type].find(serial_number);
       if (data_iter != conn_iter->second[measurement_type].end()) {
         data_iter->second.getVarList<State_t>(var_list_out);
         data_iter->second.getVarList< ValueHistory<State_t> >(var_list_out);
         data_iter->second.getVarList<float>(var_list_out);
         data_iter->second.getVarList< ValueHistory<float> >(var_list_out);
         data_iter->second.getVarList<State_t>(var_list_temp);
         data_iter->second.getVarList< ValueHistory<State_t> >(var_list_temp);
         data_iter->second.getVarList<float>(var_list_temp);
         data_iter->second.getVarList< ValueHistory<float> >(var_list_temp);
       }
      }
     VarDefList *var_def_list = VarDefList::instance();
     for(std::set<VarId_t>::const_iterator var_id_iter = var_list_temp.begin();
         var_id_iter != var_list_temp.end();
         ++var_id_iter) {
        var_list_out.insert( var_def_list->varName( *var_id_iter ) );
      }
    }
  }


  template <class T>
  void CalibrationData::DataList_t::getVarList(std::set<std::string> &var_list) const
  {
    VarDefList *var_def_list = VarDefList::instance();
    const std::map<VarId_t, T> &value_list = getValueMap<T>();
    for (typename std::map<VarId_t, T>::const_iterator value_iter = value_list.begin();
	 value_iter != value_list.end();
	 value_iter++) {
      var_list.insert( var_def_list->varName(value_iter->first));
    }
  }

  // instantiate getVarList for State_t and float :
  template
  void CalibrationData::DataList_t::getVarList<State_t>(std::set<std::string> &var_list) const;

  template
  void CalibrationData::DataList_t::getVarList<float>(std::set<std::string> &var_list) const;

  template
  void CalibrationData::DataList_t::getVarList< ValueHistory<State_t> >(std::set<std::string> &var_list) const;

  template
  void CalibrationData::DataList_t::getVarList< ValueHistory<float> >(std::set<std::string> &var_list) const;


  template <class T>
  void CalibrationData::DataList_t::getVarList(std::set<VarId_t> &var_list) const
  {
    const std::map<VarId_t, T> &value_list = getValueMap<T>();
    for (typename std::map<VarId_t, T>::const_iterator value_iter = value_list.begin();
        value_iter != value_list.end();
        value_iter++) {
      var_list.insert( value_iter->first );
    }
  }


  // instantiate getVarList for State_t and float :
  template
  void CalibrationData::DataList_t::getVarList<State_t>(std::set<VarId_t> &var_list) const;

  template
  void CalibrationData::DataList_t::getVarList<float>(std::set<VarId_t> &var_list) const;

  template
  void CalibrationData::DataList_t::getVarList< ValueHistory<State_t> >(std::set<VarId_t> &var_list) const;

  template
  void CalibrationData::DataList_t::getVarList< ValueHistory<float> >(std::set<VarId_t> &var_list) const;


  void CalibrationData::ConnObjDataList_t::addTag(const std::string &tag_name) {
    std::string upper_case_tag=tag_name;
    transform (upper_case_tag.begin (), upper_case_tag.end (), upper_case_tag.begin (), (int(*)(int)) toupper);
    std::vector<std::string>::const_iterator tag_iter = beginTags();
    for (; tag_iter != endTags(); ++tag_iter) {
      if (*tag_iter == upper_case_tag) {
	break;
      }
    }
    if (tag_iter == endTags()) {
      m_tags.push_back(upper_case_tag);
    }
  }

  bool CalibrationData::ConnObjDataList_t::findTag(const std::string &tag_name) const {
    std::vector<std::string>::const_iterator tag_iter = find(beginTags(), endTags(), tag_name);
    std::cout << "INFO [CalibrationData::ConnObjDataList] tags :"  << m_tags.size();
    if (!m_tags.empty()) {
      std::cout << " :: "  << *(m_tags.begin()) << ", " << m_tags.back();
    }
    std::cout << std::endl;
    return (tag_iter != endTags());
  }


  void CalibrationData::resetCurrentEnableState()
  {
    for (std::map<std::string, ConnObjDataList_t >::iterator conn_iter = m_connObjDataList.begin();
	 conn_iter != m_connObjDataList.end();
	 conn_iter++) {

      ConnObjMeasurementList_t::iterator data_iter = conn_iter->second[kCurrent].find(0);
      if (data_iter != conn_iter->second[kCurrent].end()) {
	data_iter->second.enableState() = EnableState_t();
      }

    }
  }

  void CalibrationData::createEmptyMeasurement(EMeasurementType measurement_type,
					       SerialNumber_t   serial_number)
  {
    for (std::map<std::string, ConnObjDataList_t >::iterator conn_iter = m_connObjDataList.begin();
	 conn_iter != m_connObjDataList.end();
	 conn_iter++) {

      ConnObjMeasurementList_t::iterator data_iter = conn_iter->second[measurement_type].find(serial_number);
      if (data_iter == conn_iter->second[measurement_type].end()) {
	createDataList(conn_iter, measurement_type, serial_number);
      }
      else {
	data_iter->second.reset();
      }
    }
  }

  void CalibrationData::remove(EMeasurementType measurement_type,
			       SerialNumber_t   serial_number)
  {
    for (std::map<std::string, ConnObjDataList_t >::iterator conn_iter = m_connObjDataList.begin();
	 conn_iter != m_connObjDataList.end();
	 conn_iter++) {

      ConnObjMeasurementList_t::iterator data_iter = conn_iter->second[measurement_type].find(serial_number);
      if (data_iter != conn_iter->second[measurement_type].end()) {
	conn_iter->second[measurement_type].erase(data_iter);
      }

    }
  }

  void CalibrationData::removeUpTo(EMeasurementType measurement_type,
				   SerialNumber_t   serial_number)
  {
    for (std::map<std::string, ConnObjDataList_t >::iterator conn_iter = m_connObjDataList.begin();
	 conn_iter != m_connObjDataList.end();
	 conn_iter++) {

      ConnObjMeasurementList_t::iterator data_iter = conn_iter->second[measurement_type].find(serial_number);
      if (data_iter != conn_iter->second[measurement_type].end()) {
	conn_iter->second[measurement_type].erase(conn_iter->second[measurement_type].begin(),data_iter);
      }

    }
  }

  void CalibrationData::reset(EMeasurementType measurement_type,
			      SerialNumber_t   serial_number)
  {
    for (std::map<std::string, ConnObjDataList_t >::iterator conn_iter = m_connObjDataList.begin();
	 conn_iter != m_connObjDataList.end();
	 conn_iter++) {

      ConnObjMeasurementList_t::iterator data_iter = conn_iter->second[measurement_type].find(serial_number);
      if (data_iter != conn_iter->second[measurement_type].end()) {
	data_iter->second.reset();
      }

    }
  }

  void CalibrationData::copyEnableState(EMeasurementType src_measurement_type,
					SerialNumber_t   src_number,
					EMeasurementType dest_measurement_type,
					SerialNumber_t   dest_number)
  {
    for (std::map<std::string, ConnObjDataList_t >::iterator conn_iter = m_connObjDataList.begin();
	 conn_iter != m_connObjDataList.end();
	 conn_iter++) {

      try {
	const DataList_t &src_dataList = ConnObjDataList(conn_iter->second).getDataList(src_measurement_type, src_number);

	DataList_t &dest_dataList = createDataList(conn_iter,dest_measurement_type,dest_number);
	dest_dataList.enableState() = src_dataList.enableState();
      }
      catch (MissingConnObject &err) {
      }
    }
  }

  void CalibrationData::errorTypeMismatch(VarDef_t var_def, EValueType requested_type) {
    std::string var_name = VarDefList::instance()->varName(var_def.id());
    std::stringstream message;
    message << "FATAL [ConnObjDataList::errorTypeMismatch] Incompatible usage of variable "
	    << var_name << ". It is already defines as type " << var_def.valueType()
	    << ", so it cannot" << " be of type " << requested_type << " at the same time.";
    throw MissingValue(message.str());
  }


  void CalibrationData::ConnObjDataList::missingValue(const EMeasurementType type,
						      const SerialNumber_t serial_number,
						      const VarId_t id) const
  {
    std::stringstream message;
    message << "FATAL [ConnObjDataList::missingValue] no data for type " << type << " , serial number " << serial_number << " and ID " << id << ".";
    throw MissingValue(message.str());
  }

  template <class T> T CalibrationData::ConnObjDataList::newestValue(const EMeasurementType type,
								     const SerialNumber_t serial_number,
								     const VarDef_t &var_def) const
  {
    if (var_def.hasHistory()) {
      return newestHistoryValue<T>(type,serial_number, var_def.id());
    }
    else {
      return value<T>(type,serial_number, var_def.id());
    }
  }

  template <class T>
  void CalibrationData::ConnObjDataList::addValueUnsafe(EMeasurementType measurement_type,
							SerialNumber_t number,
							const VarDef_t &var_def,
							TimeType_t time,
							const T value) {
    assert( VarDefList::instance()->isValid(var_def.id()) );
    assert( var_def.valueType() == getType<ValueHistory<T> >() );
    DataList_t &a_dataList = getDataList(measurement_type,number);
    std::map<VarId_t, ValueHistory<T> > &var_map = a_dataList.getValueMap<ValueHistory<T> >();
    typename std::map<VarId_t, ValueHistory<T> >::iterator var_iter = var_map.find(var_def.id());
    if (var_iter == var_map.end()) {

      if (!var_def.hasHistory()) {
	std::stringstream message;
	message << "FATAL [ConnObjDataList::value<T>] Not a value with history. Cannot add value with time stamp for  " << measurement_type << " , serial number "
		<< number << " and ID " << var_def.id() << ".";
	throw MissingValue(message.str());
      }

      std::pair< typename std::map<VarId_t, ValueHistory<T> >::iterator, bool >  
	ret = var_map.insert( std::make_pair( var_def.id(), ValueHistory<T>( var_def.historySize()) ) );
      if (!ret.second) {
	std::stringstream message;
	message << "FATAL [ConnObjDataList::value<T>] Failed to add history for for  " << measurement_type << " , serial number "
		<< number << " and ID " << var_def.id() << ".";
	throw MissingValue(message.str());
      }
      var_iter = ret.first;
    }
    var_iter->second.addValue(time,value);
  }

  template <class T>
  void CalibrationData::addValueUnsafe(const std::string &conn_name,
				       EMeasurementType measurement_type,
				       SerialNumber_t number,
				       const VarDef_t &var_def,
				       TimeType_t time,
				       const T value) {
    assert( VarDefList::instance()->isValid(var_def.id()) );
    assert( var_def.valueType() == getType<ValueHistory<T> >() );
    DataList_t &a_dataList = createDataList(conn_name,measurement_type,number);
    std::map<VarId_t, ValueHistory<T> > &var_map = a_dataList.getValueMap<ValueHistory<T> >();
    typename std::map<VarId_t, ValueHistory<T> >::iterator var_iter = var_map.find(var_def.id());
    if (var_iter == var_map.end()) {

      if (!var_def.hasHistory()) {
	std::stringstream message;
	message << "FATAL [ConnObjDataList::value<T>] Not a value with history. Cannot add value with time stamp for  " << measurement_type << " , serial number "
		<< number << " and ID " << var_def.id() << ".";
	throw MissingValue(message.str());
      }

      std::pair< typename std::map<VarId_t, ValueHistory<T> >::iterator, bool >  
	ret = var_map.insert( std::make_pair( var_def.id(), ValueHistory<T>( var_def.historySize()) ) );
      if (!ret.second) {
	std::stringstream message;
	message << "FATAL [ConnObjDataList::value<T>] Failed to add history for for  " << measurement_type << " , serial number "
		<< number << " and ID " << var_def.id() << ".";
	throw MissingValue(message.str());
      }
      var_iter = ret.first;
    }
    var_iter->second.addValue(time,value);
  }



  // instanciate some templates 
//   template float CalibrationData::ConnObjDataList::handleValueHistory<float>(const EMeasurementType type,
// 									     const SerialNumber_t serial_number,
// 									     const VarId_t id,
// 									     const DataList_t &a_dataList) const;

//   template State_t CalibrationData::ConnObjDataList::handleValueHistory<State_t>(const EMeasurementType type,
// 										 const SerialNumber_t serial_number,
// 										 const VarId_t id,
// 										 const DataList_t &a_dataList) const;

  template void CalibrationData::ConnObjDataList::addValueUnsafe<float>(EMeasurementType measurement_type,
									SerialNumber_t number,
									const VarDef_t &var_def,
									TimeType_t time,
									const float value);

  template void CalibrationData::ConnObjDataList::addValueUnsafe<State_t>(EMeasurementType measurement_type,
									  SerialNumber_t number,
									  const VarDef_t &var_def,
									  TimeType_t time,
									  const State_t value);

  template void CalibrationData::addValueUnsafe<float>(const std::string &conn_name,
						       EMeasurementType measurement_type,
						       SerialNumber_t number,
						       const VarDef_t &var_def,
						       TimeType_t time,
						       const float value);

  template void CalibrationData::addValueUnsafe<State_t>(const std::string &conn_name,
							 EMeasurementType measurement_type,
							 SerialNumber_t number,
							 const VarDef_t &var_def,
							 TimeType_t time,
							 const State_t value);

  template float CalibrationData::ConnObjDataList::newestValue<float>(const EMeasurementType type,
								      const SerialNumber_t serial_number,
								      const VarDef_t &var_def) const;

  template State_t CalibrationData::ConnObjDataList::newestValue<State_t>(const EMeasurementType type,
									  const SerialNumber_t serial_number,
									  const VarDef_t &var_def) const;

}
