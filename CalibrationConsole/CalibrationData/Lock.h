#ifndef _PixCon_Lock_h_
#define _PixCon_Lock_h_

#include <omnithread.h>

namespace PixCon {

  typedef omni_mutex Mutex;

  typedef omni_condition Condition;

  class Lock {
  public:
    Lock(Mutex &a_mutex) : m_mutex(&a_mutex) {m_mutex->lock();}

    ~Lock()  {m_mutex->unlock();}

  private:
    Mutex *m_mutex;
  };

}

#endif
