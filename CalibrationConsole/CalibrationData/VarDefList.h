#ifndef _PixCon_VarDefList_h_
#define _PixCon_VarDefList_h_

#include <iostream>
#include <sstream>
//@todo is this the correct header to get min/max values for floats ?
#include <values.h>
#include "CalibrationDataException.h"
#include "ConnItemBase.h"
#include <vector>
#include <map>
#include <cassert>
#include "Lock.h"
#include <cmath>

namespace PixCon {

  enum EValueType {kState=0, kValue=1, kTimed=2, kTimedState=2, kTimedValue=3, kNValueTypes=4};
  enum EValueConnType  {kModuleValue, kRodValue, kTimValue, kAllConnTypes, kNValueConnTypes };

  extern EValueConnType s_connItemToConnValueType[kNConnItemTypes];

  inline EValueConnType translate(EConnItemType conn_item_type) {
    assert( conn_item_type < kNConnItemTypes);
    return s_connItemToConnValueType[conn_item_type];
  }

  typedef unsigned int VarId_t;
  typedef unsigned short State_t;

  /** Base information about calibration data variables
   */
  class VarDefBase_t {
    static const unsigned short s_all = kState | kValue;
  protected:
    VarDefBase_t(VarId_t id, EValueConnType conn_type) 
      : m_id(id),
      m_valueType(kValue),
      m_connType(conn_type),
      m_historySize(0),
      m_extendedType(false)
      { assert(conn_type < kNValueConnTypes); }

    VarDefBase_t(VarId_t id, EValueType value_type, EValueConnType conn_type, unsigned int history_size) 
      : m_id(id),
	m_valueType(value_type),
	m_connType(conn_type),
        m_historySize( (history_size>1 ? history_size : 0 )),
	m_extendedType(false)
    { assert( isValid() ); assert( conn_type < kNValueConnTypes ); }

  public:
    VarDefBase_t()
      : m_id(static_cast<VarId_t>(-1)),
	m_valueType(kNValueTypes),
	m_connType(kNValueConnTypes),
	m_extendedType(false)
      {}

    VarId_t    id() const              { return m_id; }

    bool isValue() const               { return m_valueType == kValue; }
    bool isState() const               { return m_valueType == kState; }
    bool isValueWithHistory() const    { return m_valueType == kTimedValue; }
    bool isStateWithHistory() const    { return m_valueType == kTimedState; }
    bool isValueWithOrWithoutHistory() const    { return (m_valueType & s_all)==kValue; }
    bool isStateWithOrWithoutHistory() const    { return (m_valueType & s_all)==kState; }
    bool isExtendedType() const        { return m_extendedType; }
    bool isValid() const               { return m_valueType < kNValueTypes; }

    bool hasHistory() const            { return m_historySize>0; }
    unsigned int historySize() const   { return m_historySize; }

    EValueType valueType() const       { return m_valueType ;}
    EValueConnType  connType()  const  { return m_connType; }

    void setAllConnType()         { m_connType = kAllConnTypes; }

    static VarDefBase_t invalid() {
      return VarDefBase_t();
    }

  protected:
    void setExtended() { m_extendedType=true; }

  private:
    VarId_t     m_id;
    EValueType  m_valueType;
    EValueConnType   m_connType;
    unsigned int     m_historySize;
    bool        m_extendedType;
  };

  /** Extra variable definition data for floating point variables.
   */
  class ValueVarDef_t : public VarDefBase_t
  {
  public:
    ValueVarDef_t(VarId_t id, float min_val, float max_val, const std::string &unit, EValueConnType conn_type, unsigned int history_size=0)
      : VarDefBase_t(id,( history_size>1 ? kTimedValue : kValue) ,conn_type, history_size),
	m_minVal(min_val),
        m_maxVal(max_val),
        m_missing(min_val),
        m_unit(unit),
        m_isInt(false)
    {}

    ValueVarDef_t(VarId_t id, float min_val, float max_val, EValueConnType conn_type)
      : VarDefBase_t(id,kValue,conn_type, 0),
	m_minVal(min_val),
        m_maxVal(max_val),
        m_missing(min_val),
        m_isInt(false)
      {}

    ValueVarDef_t(VarId_t id, EValueConnType conn_type)
      : VarDefBase_t(id,kValue,conn_type, 0),
	m_minVal(FLT_MAX),
	m_maxVal(-FLT_MAX),
        m_missing(NAN),
        m_isInt(false)
    {}

    bool isMinMaxSet() const { return m_minVal < m_maxVal; }

    void setMinMax(float min_val, float max_val) {
      m_minVal = min_val;
      m_maxVal = max_val;
    }

    void setMissing(float missing) {
      m_missing=missing;
    }

    void setIsInteger(bool is_integer) {
      m_isInt=is_integer;
    }

    void setUnit(const std::string &unit) {
      m_unit=unit;
    }

    const std::string &unit() const {return m_unit; }

    float minVal() const  { return m_minVal; }
    float maxVal() const  { return m_maxVal; }
    float missing() const { return m_missing; }
    bool isInteger() const      { return m_isInt; }

  private:
    float       m_minVal;
    float       m_maxVal;
    float       m_missing;
    std::string m_unit;
    bool        m_isInt;
  };

  class UndefinedState : public CalibrationDataException
  {
  public:
    UndefinedState(const std::string &message) : CalibrationDataException(message) {};
  };


  /** Extra variable definition data for state describing variables.
   */
  class StateVarDef_t : public VarDefBase_t
  {
  private:
    static const State_t s_nStateMax = 255;
  public:
    StateVarDef_t(VarId_t id, EValueConnType conn_type) 
      : VarDefBase_t(id, kState, conn_type, /*history size */ 0),
        m_page(0)
    {}

    StateVarDef_t(VarId_t id, EValueConnType conn_type, unsigned int n_states, bool remap=false, unsigned int history_size=0) 
      : VarDefBase_t(id, (history_size>1 ? kTimedState : kState), conn_type, history_size),
        m_page(0)
    {
      if (n_states>s_nStateMax) {
	throw std::runtime_error("Unreasonable large number of states.");
      }
      if (n_states>0) {
	m_stateNameList.reserve(n_states);
	if (remap) {
	  m_stateReMap.resize(n_states);
	}
      }
    }

    bool isValidState(State_t state_value) const {
      return state_value < m_stateNameList.size() && !m_stateNameList[state_value].empty();
    }

    /** Reset the state definition.
     */
    void reset() {
      m_stateNameMap.clear();
      m_stateNameList.clear();
      m_stateReMap.clear();
      m_page=0;
    }

    void addState(State_t state_value, const std::string &state_name) {
      if (state_value>s_nStateMax) {
	throw std::runtime_error("Unreasonable large state value.");
      }
      if (state_value>=m_stateNameList.size()) {
	m_stateNameList.resize(state_value+1);
      }
//       if (!m_stateNameList[state_value].empty()) {
// 	std::map<std::string, State_t>::iterator old_state_name_iter = m_stateNameMap.find(m_stateNameList[state_value]);
// 	if (old_state_name_iter!=m_stateNameMap.end()) {
// 	  m_stateNameMap.erase(old_state_name_iter);
// 	}
//       }
      m_stateNameMap[state_name]=state_value;
      m_stateNameList[state_value]=state_name;
    }

    void addState(State_t state_value, State_t mapped_state_value, const std::string &state_name) {
      if (state_value>s_nStateMax) {
	throw std::runtime_error("Unreasonable large state value.");
      }
      if (state_value>=m_stateNameList.size()) {
	m_stateNameList.resize(state_value+1);
      }
//       if (!m_stateNameList[state_value].empty()) {
// 	std::map<std::string, State_t>::iterator old_state_name_iter = m_stateNameMap.find(m_stateNameList[state_value]);
// 	if (old_state_name_iter!=m_stateNameMap.end()) {
// 	  m_stateNameMap.erase(old_state_name_iter);
// 	}
//       }
      //      m_stateNameMap.insert(std::make_pair(state_name, state_value ) );
      m_stateNameMap[state_name]=state_value;
      m_stateNameList[state_value]=state_name;
      if (state_value>=m_stateReMap.size()) {
	m_stateReMap.resize(state_value+1);
      }
      m_stateReMap[state_value]=mapped_state_value;
    }

    State_t state(const std::string &state_name) const {
      std::map<std::string, State_t>::const_iterator state_iter = m_stateNameMap.find(state_name);
      if (state_iter == m_stateNameMap.end()) return m_stateNameList.size();
      return static_cast<State_t>( state_iter->second );
    }

    const std::string &stateName(State_t state_value) const {
      if (state_value>=m_stateNameList.size()) {
	throw UndefinedState("Undefined state.");
      }
      return m_stateNameList[state_value];
    }

    /** Return the highest state value.
     */
    unsigned int nStates() const {
      return m_stateNameList.size();
    }

    const std::vector<std::string> &stateNameList() const {
      return m_stateNameList;
    }

    /** Map the state to a general scheme which better matches the colour scheme
     */
    State_t remapState(State_t state_in) const {
      if (state_in >= m_stateReMap.size()) {
	return state_in;
      }
      else {
	return m_stateReMap[ state_in ];
      }
    }

    void setPage(unsigned int page_i) {
      m_page=static_cast<State_t>(page_i % m_stateReMap.size());
    }

    unsigned int page() const {
      return m_page;
    }

  private:
    std::vector<std::string>       m_stateNameList;
    std::map<std::string, State_t> m_stateNameMap;
    std::vector< State_t >         m_stateReMap;
    State_t                        m_page;
  };


  /** Extended state which allows combination of multiple states.
   * States of an extended state are ordered where states with
   * higher order have precedence over states with lower order when.
   * when combining two states.
   * Moreover, states which indicate success are marked and there
   * Is a pseudo state which indicates the inavailablility of a state.
   */
  class ExtendedStateVarDef_t : public StateVarDef_t
  {
  public:
    ExtendedStateVarDef_t(VarId_t id, EValueConnType conn_type, State_t missing)
      : StateVarDef_t(id,conn_type, /* history size :*/ 0),
        m_missing(missing)
      { setExtended(); }

    ExtendedStateVarDef_t(VarId_t id, EValueConnType conn_type, unsigned int n_states, State_t missing, unsigned int history_size=0) 
      : StateVarDef_t(id, conn_type, n_states, false, history_size),
        m_missing(missing)
      { m_success.resize(n_states); m_precedence.resize(n_states);setExtended(); }

    /** Add an extended state.
     * @param state_value the integer state value
     * @param state_name the corresponding name
     * @param is_success should be set to true if the state indicates success otherwise false
     * @param precedence the precedence of this state
     * When combining states from multiple sources into one, the state with the highest precedence should
     * survive. Generally, the successful state should have a low precedence.
     */
    void addState(State_t state_value, const std::string &state_name, bool is_success, unsigned char precedence) {
      StateVarDef_t::addState(state_value, state_name);

      if (state_value>=m_success.size()) {
	m_success.resize(state_value+1);
	m_precedence.resize(state_value+1);
      }
      m_success[state_value]=is_success;
      m_precedence[state_value]=precedence;
    }

    /** Add an extended state.
     * @param state_value the integer state value
     * @param mapped_state_value the value which defines e.g. the colour
     * @param state_name the corresponding name
     * @param is_success should be set to true if the state indicates success otherwise false
     * @param precedence the precedence of this state
     * When combining states from multiple sources into one, the state with the highest precedence should
     * survive. Generally, the successful state should have a low precedence.
     */
    void addState(State_t state_value, State_t mapped_state_value, const std::string &state_name, bool is_success, unsigned char precedence) {
      StateVarDef_t::addState(state_value, mapped_state_value, state_name);

      if (state_value>=m_success.size()) {
	m_success.resize(state_value+1);
	m_precedence.resize(state_value+1);
      }
      m_success[state_value]=is_success;
      m_precedence[state_value]=precedence;
    }


    bool indicatesSuccess(State_t state_value) const {
      if (state_value>=m_success.size()) return false;
      return m_success[state_value];
    }

    unsigned char precedence(State_t state_value) const {
      if (state_value>=m_success.size()) return 0;
      return m_precedence[state_value];
    }

    State_t missingState() const {
      return m_missing;
    }

  protected:
    State_t                      m_missing;
    std::vector<unsigned char>   m_precedence;
    std::vector<bool>            m_success;
  };


  class VarDef_t {

  public:
    VarDef_t(VarDefBase_t &data)
      : m_varDef(&data)
      { assert(m_varDef); }


    bool isValid() const {return m_varDef->isValid(); }

    static VarDef_t invalid() {
      return VarDef_t(s_invalid);
    }

    VarId_t    id()             const  { return m_varDef->id(); }
    EValueType valueType()      const  { return m_varDef->valueType() ;}
    bool isValue() const               { return m_varDef->isValue(); }
    bool isState() const               { return m_varDef->isState(); }
    bool isValueWithHistory() const    { return m_varDef->isValueWithHistory(); }
    bool isStateWithHistory() const    { return m_varDef->isStateWithHistory(); }
    bool isValueWithOrWithoutHistory() const    { return m_varDef->isValueWithOrWithoutHistory(); }
    bool isStateWithOrWithoutHistory() const    { return m_varDef->isStateWithOrWithoutHistory(); }

    bool isExtendedType()       const  { return m_varDef->isExtendedType(); }
    EValueConnType  connType()  const  { return m_varDef->connType(); }
    bool hasHistory()           const  { return m_varDef->hasHistory(); }

    unsigned int historySize()  const  { return m_varDef->historySize(); }

    ValueVarDef_t &valueDef() {
      assert( isValueWithOrWithoutHistory() );
      return *static_cast<ValueVarDef_t *>(m_varDef);
    }

    const ValueVarDef_t &valueDef() const {
      assert( isValueWithOrWithoutHistory() );
      return *static_cast<ValueVarDef_t *>(m_varDef);
    }

    StateVarDef_t &stateDef() {
      assert( isStateWithOrWithoutHistory() );
      return *static_cast<StateVarDef_t *>(m_varDef);
    }

    const StateVarDef_t &stateDef() const {
      assert( isStateWithOrWithoutHistory() );
      return *static_cast<StateVarDef_t *>(m_varDef);
    }

    ExtendedStateVarDef_t &extendedStateDef() {
      assert( isStateWithOrWithoutHistory()  && isExtendedType());
      return *static_cast<ExtendedStateVarDef_t *>(m_varDef);
    }

    const ExtendedStateVarDef_t &extendedStateDef() const {
      assert( isStateWithOrWithoutHistory() && isExtendedType());
      return *static_cast<ExtendedStateVarDef_t *>(m_varDef);
    }

    // convenience functions

    bool isMinMaxSet() const {
      assert(  isValueWithOrWithoutHistory());
      return valueDef().isMinMaxSet(); 
    }

    void setMinMax(float min_val, float max_val) {
      assert( isValueWithOrWithoutHistory());
      return valueDef().setMinMax(min_val,max_val);
    }

    float minVal() const {
      assert( isValueWithOrWithoutHistory());
      return valueDef().minVal(); 
    }

    float maxVal() const {
      assert( isValueWithOrWithoutHistory());
      return valueDef().maxVal(); 
    }


    const std::string &stateName(State_t state_value) const {
      assert( isStateWithOrWithoutHistory() );
      return stateDef().stateName(state_value);
    }

    void setAllConnType()         { m_varDef->setAllConnType(); }

  private:
    VarDefBase_t *m_varDef;

    static VarDefBase_t s_invalid;
  };

  //@todo add method to remove all definitions which are not in 
  // a set<std::string>
  class VarDefList {
    typedef std::map<std::string, VarDefBase_t *>  VarIdList_t;
  public:
    friend class CalibrationData;

    VarDefList() { }

    /** Get the Id of the given variable.
     * @param var_name the name of the variable
     * @return the ID of the variable
     * @throw UndefinedCalibrationVariable if no variable was defined for the given name.
     */
    VarDef_t getVarDef(const std::string &var_name) const  {
      Lock lock(m_varListMutex);

      VarIdList_t::const_iterator var_iter = m_varIdList.find(var_name);
      if (var_iter == m_varIdList.end() ) {
	std::stringstream message;
	message << "FATAL [CalibrationData::getVarDef] Variable ," << var_name << ", does not exist.";
	throw UndefinedCalibrationVariable(message.str());
      }
      return VarDef_t(*(var_iter->second));
    }

    VarDef_t createStateVarDef(const std::string &var_name, EValueConnType conn_type, unsigned n_states, bool remap=false, unsigned int history_size=0)  {
      Lock lock(m_varListMutex);

      VarIdList_t::const_iterator var_iter = m_varIdList.find(var_name);
      if (var_iter != m_varIdList.end() ) {
	std::stringstream message;
	message << "Variable " << var_name << " exists already.";
	std::runtime_error(message.str());
      }

      unsigned int new_id = m_varName.size();
      var_iter=addVarDef(var_name, new StateVarDef_t(new_id, conn_type, n_states, remap, history_size) );

      return VarDef_t( *(var_iter->second) );
    }

    VarDef_t createExtendedStateVarDef(const std::string &var_name, EValueConnType conn_type, unsigned n_states, State_t missing, unsigned int history_size=0)  {
      Lock lock(m_varListMutex);

      VarIdList_t::const_iterator var_iter = m_varIdList.find(var_name);
      if (var_iter != m_varIdList.end() ) {
	std::stringstream message;
	message << "Variable " << var_name << " exists already.";
	std::runtime_error(message.str());
      }

      unsigned int new_id = m_varName.size();
      var_iter=addVarDef(var_name, new ExtendedStateVarDef_t(new_id, conn_type, n_states, missing, history_size) );

      return VarDef_t( *(var_iter->second) );
    }

    VarDef_t createValueVarDef(const std::string &var_name,
			       EValueConnType conn_type,
			       float min_val,
			       float max_val,
			       const std::string &unit="",
			       unsigned int history_size=0)  {
      Lock lock(m_varListMutex);

      VarIdList_t::const_iterator var_iter = m_varIdList.find(var_name);
      if (var_iter != m_varIdList.end() ) {
	std::stringstream message;
	message << "Variable " << var_name << " exists already.";
	std::runtime_error(message.str());
      }

      unsigned int new_id = m_varName.size();
      var_iter=addVarDef(var_name, new ValueVarDef_t(new_id, min_val, max_val, unit, conn_type, history_size) );

      return VarDef_t( *(var_iter->second) );
    }


    VarDef_t createAlias(const std::string &var_name, const std::string &alias_name, EValueConnType conn_type)  {
      Lock lock(m_varListMutex);
      VarIdList_t::iterator var_iter = m_varIdList.find(var_name);
      if (var_iter == m_varIdList.end() ) {
	std::stringstream message;
	message << "FATAL [CalibrationData::getVarDef] Variable ," << var_name << ", does not exist.";
	throw UndefinedCalibrationVariable(message.str());
      }
      if (var_iter->second->connType() != conn_type) {
	  var_iter->second->setAllConnType();
      }
      var_iter=addVarDef(alias_name, var_iter->second, true);

      return VarDef_t( *(var_iter->second) );
    }


    static VarDefList *instance() {
      if (!s_instance) {
	s_instance = new VarDefList;
      }
      return s_instance;
    }

    /** Get the name for the given variable.
     */
    const std::string &varName(VarId_t id) {
      try {
	checkId(id);
	return m_varName[id];
      } catch(...) {throw UndefinedCalibrationVariable("Invalid Variable");}
    }

    /** Return true if the variable id is valid.
     */
    bool isValid(VarId_t id) {
      return id<m_varName.size();
    }

  protected:

    /** Get an Id for the given variable name.
     * @param var_name the name of the variable
     * @param type the type of the variable which will be associated to the variable name if the variable is not yet registered.
     * @throw FatalCalibrationDataException if the variable could not be added to the list of defined variables.
     */
    VarDef_t getVarDef(const std::string &var_name, EValueConnType conn_type, EValueType type) {
      Lock lock(m_varListMutex);
      VarIdList_t::const_iterator var_iter = m_varIdList.find(var_name);
      if (var_iter == m_varIdList.end() ) {
	unsigned int new_id = m_varName.size();
	VarDefBase_t *var_def;
	switch (type) {
	case kState:
	  var_def=new StateVarDef_t(new_id, conn_type);
	  break;
	case kValue:
	  var_def=new ValueVarDef_t(new_id, conn_type);
	  break;
	default:
	  //assert( type < kNValueTypes);
	  throw std::runtime_error("Tried to create variable of illegal type.");
	}
	var_iter = addVarDef(var_name, var_def);
      }
      return VarDef_t( *(var_iter->second) );

    }

  private:
    VarIdList_t::iterator addVarDef(const std::string &var_name, VarDefBase_t *var_def, bool alias=false) 
    {
      std::pair<VarIdList_t::iterator, bool > ret = m_varIdList.insert(std::make_pair(var_name, var_def));
      if (!ret.second) {
	delete var_def;
	throw FatalCalibrationDataException("FATAL [CalibrationData::getVarDef] Failed to insert variable name into VarIdList.");
      }
      if (!alias) {
	assert(m_varName.size() == var_def->id() );
	m_varName.push_back(var_name);
      }
      return ret.first;
    }
    void checkId(VarId_t id);

    mutable Mutex                   m_varListMutex;
    VarIdList_t                     m_varIdList;       /**< map from variable names to an ID to which a variable type is associated.*/
    std::vector< std::string >      m_varName;
    static VarDefList *s_instance;
  };

}
#endif
