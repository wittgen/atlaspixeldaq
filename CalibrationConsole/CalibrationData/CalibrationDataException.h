#ifndef _PixCon_CalibrationDataException_h_
#define _PixCon_CalibrationDataException_h_

#include <stdexcept>
#include <string>

namespace PixCon {

  class CalibrationDataException : public std::runtime_error
  {
  protected:
    CalibrationDataException(const std::string &message) : std::runtime_error(message) {};
  };

  /** Exception in case of resource limitations, allocation failure, etc.. 
   */
  class FatalCalibrationDataException : public CalibrationDataException
  {
  public:
    FatalCalibrationDataException(const std::string &message) : CalibrationDataException(message) {};
  };

  /** Exception being thrown if no variable has been defined for a certain name.
   */
  class UndefinedCalibrationVariable : public CalibrationDataException
  {
  public:
    UndefinedCalibrationVariable(const std::string &message) : CalibrationDataException(message) {};
  };

}

#endif
