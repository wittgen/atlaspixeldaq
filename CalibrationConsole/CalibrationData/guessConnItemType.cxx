#include <guessConnItemType.h>
#include <iostream>
#include <vector>

namespace PixCon {
 
  EConnItemType guessConnItemType(const std::string &connectivity_name)
  {
    std::string name(connectivity_name);
    std::vector<std::string> tokens;
    std::string del("_");
    size_t pos = 0;
    while ((pos = name.find(del)) != std::string::npos) {
      std::string token = name.substr(0, pos);
      tokens.push_back(token);
      name.erase(0, pos+del.length());
    }
    if (name.length() > 0) tokens.push_back(name);

    switch (tokens.size()) {
    case 1:
      return kNConnItemTypes;
      break;
    case 2:
      if (tokens[0].compare("LI") == 0) {
       return kPp0Item;
      }
      else {
       return kTimItem;
      }
      break;
    case 3:
      if (tokens[1].compare("CRATE") == 0) {
	return kCrateItem;
      }
      else if (tokens[0].compare("ROD") == 0) {
	return kRodItem;
      }
      else {
	return kPp0Item;
      }
      break;
    case 4:
      if (tokens[0].substr(0,1).compare("D") == 0) {
	return kModuleItem;
      }
      else {
	return kPp0Item;
      }
      break;
    case 5:
    case 6:
      return kModuleItem;
      break;
    default:
      break;
    }
    std::cout << "ERROR  [PixCon::guessConnItemType] Failed to guess connectivity type (Module/Pp0/ROD) for  "
	      << connectivity_name << std::endl;
    return kNConnItemTypes;
  }
}
