#include "CalibrationDataAccessor.h"

template class PixCon::CalibrationDataAccessor<float>;
template class PixCon::CalibrationDataAccessor<PixCon::State_t>;

namespace PixCon {

  ICalibrationDataAccessor *createCalibrationDataAccessor( PixCon::VarDef_t &var_def)
  {
    if (var_def.isValue()) {
      return new CalibrationDataAccessor<float>;
    }
    if (var_def.isValueWithHistory()) {
      return new CalibrationHistoryDataAccessor<float>;
    }
    else if (var_def.isState()) {
      return new CalibrationDataAccessor<State_t>;
    }
    else if (var_def.isStateWithHistory()) {
      return new CalibrationHistoryDataAccessor<State_t>;
    }
    else {
      assert(var_def.isStateWithOrWithoutHistory() || var_def.isValueWithOrWithoutHistory() );
      return NULL;
    }
  }

}
