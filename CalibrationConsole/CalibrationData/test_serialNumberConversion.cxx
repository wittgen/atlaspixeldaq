#include "CalibrationDataTypes.h"
#include "CalibrationDataUtil.h"
#include <iostream>
#include <vector>
#include <assert.h>  

int main3() {

  std::vector< std::pair<PixCon::EMeasurementType,PixCon::SerialNumber_t> > nr_in;
  nr_in.push_back( std::make_pair<PixCon::EMeasurementType,PixCon::SerialNumber_t>(PixCon::kCurrent,0) );
  nr_in.push_back( std::make_pair<PixCon::EMeasurementType,PixCon::SerialNumber_t>(PixCon::kScan,1) );
  nr_in.push_back( std::make_pair<PixCon::EMeasurementType,PixCon::SerialNumber_t>(PixCon::kScan,1234) );
  nr_in.push_back( std::make_pair<PixCon::EMeasurementType,PixCon::SerialNumber_t>(PixCon::kScan,999) );
  nr_in.push_back( std::make_pair<PixCon::EMeasurementType,PixCon::SerialNumber_t>(PixCon::kScan,1000) );
  nr_in.push_back( std::make_pair<PixCon::EMeasurementType,PixCon::SerialNumber_t>(PixCon::kAnalysis,1234) );
  nr_in.push_back( std::make_pair<PixCon::EMeasurementType,PixCon::SerialNumber_t>(PixCon::kScan,123456) );
  nr_in.push_back( std::make_pair<PixCon::EMeasurementType,PixCon::SerialNumber_t>(PixCon::kScan,1234567) );
  nr_in.push_back( std::make_pair<PixCon::EMeasurementType,PixCon::SerialNumber_t>(PixCon::kScan,9999999) );
  nr_in.push_back( std::make_pair<PixCon::EMeasurementType,PixCon::SerialNumber_t>(PixCon::kScan,10000000) );
  nr_in.push_back( std::make_pair<PixCon::EMeasurementType,PixCon::SerialNumber_t>(PixCon::kScan,123456789) );
  nr_in.push_back( std::make_pair<PixCon::EMeasurementType,PixCon::SerialNumber_t>(PixCon::kScan,999999999) );
  nr_in.push_back( std::make_pair<PixCon::EMeasurementType,PixCon::SerialNumber_t>(PixCon::kScan,1000000000) );
  nr_in.push_back( std::make_pair<PixCon::EMeasurementType,PixCon::SerialNumber_t>(PixCon::kScan,1234567890) );
  nr_in.push_back( std::make_pair<PixCon::EMeasurementType,PixCon::SerialNumber_t>(PixCon::kScan,static_cast<unsigned int>(-1) ) );

  for (std::vector< std::pair<PixCon::EMeasurementType,PixCon::SerialNumber_t> >::const_iterator nr_iter = nr_in.begin();
       nr_iter != nr_in.end();
       nr_iter++) {

    std::string serial_number_string = PixCon::makeSerialNumberString( nr_iter->first, nr_iter->second );
    std::pair<PixCon::EMeasurementType,PixCon::SerialNumber_t> ret = PixCon::stringToSerialNumber( serial_number_string );
    std::cout << "INFO [stringToSerialNumber] "
	      << "type = " << nr_iter->first << " number = " << nr_iter->second
	      << " -> "
	      << serial_number_string << " -> type = "
	      << ret.first << " number = "
	      << ret.second << std::endl;
    assert( ret.first == nr_iter->first);
    assert( ret.second == nr_iter->second);
  }

  return 0;
}
