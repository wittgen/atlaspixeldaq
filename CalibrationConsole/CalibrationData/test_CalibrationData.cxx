#include "CalibrationData.h"
#include "CategoryList.h"
#include <iostream>
#include <cassert>
#include <time.h>

std::string timeString(const ::time_t &time)
{
    ::time_t tmp_time=time;
    std::string time_string(ctime(&tmp_time));
    std::string::size_type pos = time_string.find("\n");
    if (pos==std::string::npos) {
        pos = time_string.find("\r");
    }
    if (pos != std::string::npos) {
       time_string.erase(pos, time_string.size()-pos);
    }
    return time_string;
}

template <class T>
void dump(const PixCon::CalibrationData &a, const PixCon::VarDef_t &ext_hist_def)
{
  const PixCon::ValueHistory<T> *history_p;
  const PixCon::CalibrationData::ConnObjDataList data_list(a.getConnObjectData("ROD_C1_S1"));
  history_p= &(data_list.valueHistory<T>(PixCon::kCurrent,0, ext_hist_def.id())) ;
  const PixCon::ValueHistory<T> &history(*history_p);

  std::cout << "INFO [dump] start: base="  << history.base() << std::endl;
  unsigned int arr_i=0;
  typename std::vector< PixCon::TimedValue<T> >::const_iterator iter = history.begin();
  typename std::vector< PixCon::TimedValue<T> >::const_iterator history_end = history.end();
  do {
    if (history.isValid(*iter)) {
      std::cout << "INFO [dump] "<<  arr_i <<" : time = " << history.extractTime(*iter) 
		<< "[" << timeString(history.extractTime(*iter))  << ", " << iter->time(0) << "] -> value=" << iter->value() << std::endl;
    }
    arr_i++;
  } while ( (iter=history.const_increment(iter)) != history_end);

  const PixCon::TimedValue<T> &newest_val =history.newest();
  std::cout << "INFO [dump] newest : " <<" : time = " << history.extractTime( newest_val ) 
	    << "[" << timeString(history.extractTime(newest_val))  << ", " << newest_val.time(0) << "] -> value=" << newest_val.value() << std::endl;
}

int main()
{
  PixCon::CalibrationData a;
  PixCon::CalibrationData b;

  PixCon::CategoryList categories;
  PixCon::CategoryList::Category dcs_cat=categories.addCategory("DCS");
  PixCon::CategoryList::Category status_cat=categories.addCategory("Status");
  PixCon::CategoryList::Category analysis_cat=categories.addCategory("Analysis");

  // There are different types of values
  a.addAnalysisValue<float>(PixCon::kModuleValue, "D1A_B2_S1_M1",1,analysis_cat.addVariable("Threshold Dispersion"),0.);
  a.addScanValue<float>(PixCon::kModuleValue, "D1A_B2_S1_M1",1,status_cat.addVariable("Errors"),0.);
  a.setScanEnableState("D1A_B2_S1_M1",1,true,true,true,true);
  enum EScanStatus {kScanUnknown, kScanConfiguring, kScanScanning, kScanSuccess, kScanFailure};
  a.addScanValue<PixCon::State_t>(PixCon::kRodValue, "ROD_C1_S1",1,  status_cat.addVariable("Scan Status"),static_cast<PixCon::State_t>(kScanScanning));

  // the same variables can be in several categories
  enum EAnalysisStatus {kAnaUnknown,kAnaPending,kAnaWaiting,kAnaAnalysing,kAnaSuccess,kAnaFailure};
  a.addCurrentValue<PixCon::State_t>(PixCon::kRodValue, "ROD_C1_S1", analysis_cat.addVariable( status_cat.addVariable("Analysis Status")) ,static_cast<PixCon::State_t>(kAnaWaiting));

  // Var definitions are shared.
  PixCon::VarDef_t a_def ( a.getVarDef("Analysis Status") );
  PixCon::VarDef_t b_def ( b.getVarDef("Analysis Status") );
  assert( a_def.id() == b_def.id() );
  assert( a_def.valueType() == b_def.valueType() );
  assert( a_def.connType() == b_def.connType() );

  // Create var definition for a state with a history
  
  PixCon::VarDef_t a_hist_def( PixCon::VarDefList::instance()->createExtendedStateVarDef("ExtHistoStateVar", PixCon::kRodValue, 4, 0,6) );
  assert(a_hist_def.isExtendedType() && a_hist_def.valueType()==PixCon::kTimedState && a_hist_def.hasHistory());
  PixCon::ExtendedStateVarDef_t &ext_hist_def( a_hist_def.extendedStateDef());
  ext_hist_def.addState(0,"unknown",false,0);
  ext_hist_def.addState(1,"running",false,4);
  ext_hist_def.addState(2,"success",true,1);
  ext_hist_def.addState(3,"failed",false,2);

  ::time_t now  = ::time(0);

  a.addValue<PixCon::State_t>("ROD_C1_S1",PixCon::kCurrent,0,ext_hist_def, now+1, 1);
  dump<PixCon::State_t>(a,ext_hist_def);
  assert( a.getConnObjectData("ROD_C1_S1").newestHistoryValue<PixCon::State_t>(PixCon::kCurrent,0,ext_hist_def.id()) == 1);
  a.addValue<PixCon::State_t>("ROD_C1_S1",PixCon::kCurrent,0,ext_hist_def, now+2, 2);
  dump<PixCon::State_t>(a,ext_hist_def);
  assert( a.getConnObjectData("ROD_C1_S1").newestHistoryValue<PixCon::State_t>(PixCon::kCurrent,0,ext_hist_def.id()) == 2);
  a.addValue<PixCon::State_t>("ROD_C1_S1",PixCon::kCurrent,0,ext_hist_def, now+3, 3);
  dump<PixCon::State_t>(a,ext_hist_def);
  assert( a.getConnObjectData("ROD_C1_S1").newestHistoryValue<PixCon::State_t>(PixCon::kCurrent,0,ext_hist_def.id()) == 3);
  a.addValue<PixCon::State_t>("ROD_C1_S1",PixCon::kCurrent,0,ext_hist_def, now+4, 0);
  dump<PixCon::State_t>(a,ext_hist_def);
  a.addValue<PixCon::State_t>("ROD_C1_S1",PixCon::kCurrent,0,ext_hist_def, now+5, 1);
  dump<PixCon::State_t>(a,ext_hist_def);
  a.addValue<PixCon::State_t>("ROD_C1_S1",PixCon::kCurrent,0,ext_hist_def, now+6, 2);
  dump<PixCon::State_t>(a,ext_hist_def);
  a.addValue<PixCon::State_t>("ROD_C1_S1",PixCon::kCurrent,0,ext_hist_def, now+7, 3);
  dump<PixCon::State_t>(a,ext_hist_def);

  PixCon::VarDef_t a_hist_val_def( PixCon::VarDefList::instance()->createValueVarDef("HistoValVar", PixCon::kRodValue, -10, 15,"au",6) );

  now  = ::time(0);

  a.addValue<float>("ROD_C1_S1",PixCon::kCurrent,0,a_hist_val_def, now+1, -5);
  dump<float>(a,a_hist_val_def);
  a.addValue<float>("ROD_C1_S1",PixCon::kCurrent,0,a_hist_val_def, now+2, -1.);
  dump<float>(a,a_hist_val_def);
  assert( a.getConnObjectData("ROD_C1_S1").newestHistoryValue<float>(PixCon::kCurrent,0,a_hist_val_def.id()) == static_cast<float>(-1.));
  assert( a.getConnObjectData("ROD_C1_S1").newestValue<float>(PixCon::kCurrent,0,a_hist_val_def) == static_cast<float>(-1.));
  a.addValue<float>("ROD_C1_S1",PixCon::kCurrent,0,a_hist_val_def, now+3, -1.3);
  dump<float>(a,a_hist_val_def);
  std::cout << "newest = " << a.getConnObjectData("ROD_C1_S1").newestHistoryValue<float>(PixCon::kCurrent,0,a_hist_val_def.id()) << std::endl;
  assert( a.getConnObjectData("ROD_C1_S1").newestHistoryValue<float>(PixCon::kCurrent,0,a_hist_val_def.id()) == static_cast<float>(-1.3));
  assert( a.getConnObjectData("ROD_C1_S1").newestValue<float>(PixCon::kCurrent,0,a_hist_val_def) == static_cast<float>(-1.3) );
  a.addValue<float>("ROD_C1_S1",PixCon::kCurrent,0,a_hist_val_def, now+4, 0.05);
  dump<float>(a,a_hist_val_def);
  assert( a.getConnObjectData("ROD_C1_S1").newestHistoryValue<float>(PixCon::kCurrent,0,a_hist_val_def.id()) == static_cast<float>(0.05));
  a.addValue<float>("ROD_C1_S1",PixCon::kCurrent,0,a_hist_val_def, now+5, 1.2);
  dump<float>(a,a_hist_val_def);
  a.addValue<float>("ROD_C1_S1",PixCon::kCurrent,0,a_hist_val_def, now+6, 2.2);
  dump<float>(a,a_hist_val_def);
  a.addValue<float>("ROD_C1_S1",PixCon::kCurrent,0,a_hist_val_def, now+7, 3.5);
  dump<float>(a,a_hist_val_def);


  {
    {
    const PixCon::ValueHistory<PixCon::State_t> &history( a.getConnObjectData("ROD_C1_S1").valueHistory<PixCon::State_t>(PixCon::kCurrent,0, ext_hist_def.id()) );
    unsigned int arr_i=0;
    for(std::vector< PixCon::TimedValue<PixCon::State_t> >::const_iterator iter = history.begin();
	iter != history.end();
	iter=history.const_increment(iter),arr_i++) {

      std::cout << "INFO [test_CalibrationAnalysis] "<<  arr_i <<" : time = " << history.extractTime(*iter) 
		<< "[" << iter->time(0) << "] -> value=" << iter->value() << std::endl;
    }
    }
    std::cout << "INFO [test_CalibrationData] newest value=" << a.getConnObjectData("ROD_C1_S1").newestValue<PixCon::State_t>(PixCon::kCurrent,0, ext_hist_def )
	      << std::endl;
    assert( a.getConnObjectData("ROD_C1_S1").newestValue<PixCon::State_t>(PixCon::kCurrent,0, ext_hist_def ) == 3);

    const PixCon::ValueHistory<PixCon::State_t> &history( a.getConnObjectData("ROD_C1_S1").valueHistory<PixCon::State_t>(PixCon::kCurrent,0, ext_hist_def.id()) );
    unsigned int time_arr[6]={2,3,4,5,8,7};
    unsigned int val_arr[6]= {2,3,0,1,2,3};
    unsigned int arr_i=0;
    std::cout << "INFO [test_CalibrationAnalysis] base = " << history.base() << std::endl;
    for(std::vector< PixCon::TimedValue<PixCon::State_t> >::const_iterator iter = history.begin();
	iter != history.end();
	iter=history.const_increment(iter),arr_i++) {

      assert(arr_i<6);
      std::cout << "INFO [test_CalibrationAnalysis] "<<  arr_i <<" : time = " << iter->time(0)  << " -> value=" << iter->value() << std::endl;
      assert( iter->time(0) != time_arr[arr_i]+now );
      assert( history.extractTime(*iter) == time_arr[arr_i]+now) ;
      assert( iter->value() == val_arr[arr_i]) ;
    }
  }

  {
    assert(a_hist_val_def.valueType()==PixCon::kTimedValue && a_hist_val_def.hasHistory());
    PixCon::ValueVarDef_t &ext_hist_def( a_hist_val_def.valueDef());
    const PixCon::ValueHistory<float> &history( a.getConnObjectData("ROD_C1_S1").valueHistory<float>(PixCon::kCurrent,0, ext_hist_def.id()) );
    unsigned int time_arr[6]={2,3,4,5,8,7};
    float val_arr[6]= {-1.,1.3,0.05,1.2,2.2,3.5};
    unsigned int arr_i=0;
    std::cout << "INFO [test_CalibrationAnalysis] base = " << history.base() << std::endl;
    for(std::vector< PixCon::TimedValue<float> >::const_iterator iter = history.begin();
	iter != history.end();
	iter=history.const_increment(iter),arr_i++) {

      assert(arr_i<6);
      std::cout << "INFO [test_CalibrationAnalysis] "<<  arr_i <<" : time = " << iter->time(0)  << " -> value=" << iter->value() << std::endl;
      assert( iter->time(0) != time_arr[arr_i]+now );
      assert( history.extractTime(*iter) == time_arr[arr_i]+now) ;
      assert( iter->value() == val_arr[arr_i]) ;
    }

  }
  PixCon::VarDef_t a_hist_value_def( PixCon::VarDefList::instance()->createValueVarDef("ExtHistoFloatVar", PixCon::kModuleValue, 0,100,"%",10) );
  assert(a_hist_value_def.isValueWithHistory()  && a_hist_value_def.hasHistory());
  //PixCon::ValueVarDef_t &_hist_value_def( a_hist_value_def.valueDef());

  // get data list for a module
  PixCon::ModuleRODDataList a_mod_data_list(a,"ROD_C1_S1", "D1A_B2_S1_M1");

  // ... and for a ROD
  PixCon::ModuleRODDataList a_rod_data_list(a,"ROD_C1_S1");

  // get the threshold dispersion
  PixCon::VarDef_t thr_disp_def=a.getVarDef("Threshold Dispersion");
  assert (thr_disp_def.isValue() );
  std::cout << " module threshold dispersion = " << a_mod_data_list.analysisValue<float>(1,thr_disp_def) << std::endl;


  std::set<std::string> var_list_out;
  a.varList(PixCon::kScan,1, var_list_out);
  for (std::set<std::string>::const_iterator var_iter = var_list_out.begin();
       var_iter != var_list_out.end();
       ++var_iter ) {
    const PixCon::VarDef_t &var_def = PixCon::VarDefList::instance()->getVarDef(*var_iter);
    std::cout << "\"" << *var_iter << "\" type=" << var_def.valueType() << " conn=" << var_def.connType() << std::endl;
    
  }

  // get the scan status
  PixCon::VarDef_t scan_status_def=a.getVarDef("Scan Status");
  assert (scan_status_def.isState() );
  // ... for the module
  std::cout << " module scan status = " << a_mod_data_list.scanValue<PixCon::State_t>(1,scan_status_def) << std::endl;
  // ... for the ROD 
  std::cout << " ROD scan status = " << a_rod_data_list.scanValue<PixCon::State_t>(1,scan_status_def) << std::endl;

  // first it should not exist
  try {
    PixCon::ModuleRODDataList a_list(a,"ROD_C1_S10","D2A_B2_S1_M1");
    assert ( false );
  }
  catch (PixCon::CalibrationData::MissingConnObject &err) {
  }

  // Now it is created
  PixCon::ModuleRODDataList mod_d2a = PixCon::ModuleRODDataList::create(a,"ROD_C1_S10","D2A_B2_S1_M1");
  
  // ... and it should exist
  try {
    PixCon::ModuleRODDataList a_list(a,"ROD_C1_S10","D2A_B2_S1_M1");
  }
  catch (PixCon::CalibrationData::MissingConnObject &err) {
    assert ( false );
  }

  // test enable disable bits

  // Should only be used to initialise an object.
  // after the initialisation the enabled stated should be constant (except for the "current" values).
  a.setScanEnableState("D1A_B2_S1_M1",1,true,true,true,true); // The module is enabled.
  assert ( a.getConnObjectData("D1A_B2_S1_M1").enableStateOfScan(1).enabled() );
  a.setScanEnableState("D1A_B2_S1_M1",1,false,true,true,true); // not in connectivity
  assert ( !a.getConnObjectData("D1A_B2_S1_M1").enableStateOfScan(1).enabled() );
  a.setScanEnableState("D1A_B2_S1_M1",1,true,false,true,true); // in connectivity, but disabled in configuration
  assert ( !a.getConnObjectData("D1A_B2_S1_M1").enableStateOfScan(1).enabled() );
  a.setScanEnableState("D1A_B2_S1_M1",1,true,true,false,true); // The ROD, or crate the module is connected to is disabled
  assert ( !a.getConnObjectData("D1A_B2_S1_M1").enableStateOfScan(1).enabled() );
  a.setScanEnableState("D1A_B2_S1_M1",1,true,true,true,false); // The module is temporarily disabled.
  assert ( !a.getConnObjectData("D1A_B2_S1_M1").enableStateOfScan(1).enabled() );

  // For a new session
  a.setCurrentEnableState("ROD_C1_S1",true,true,true,true);    // ROD in connectivity and enabled
  a.setCurrentEnableState("D1A_B2_S1_M1",true,true,true,true); // in connectivity and enabled
  a.setCurrentEnableState("D1A_B2_S1_M2",true,false,true,true);  // in connectivity but disabled in configuration

  a.setCurrentEnableState("ROD_C1_S2",true,false,true,true);       // ROD in connectivity and but disabled in connectivity
  a.setCurrentEnableState("D1A_B3_S1_M0",true,true, false ,true);  // in connectivity but parent is disabled

  // if a ROD is disabled the parent disable flag of all connected modules
  // need to be set.
  a.setEnabled("ROD_C1_S2",false);           // Temporarily, disable ROD
  a.setParentEnabled("D1A_B3_S1_M0",false);  // Set parent disabled flag for all connected modules.
  // @todo enabled states have to be propagated manually -> tedious and error prone (better solution?)
  //       give connectivity to Calibration data such that dependencies can be handled automatically ?

  // start a new scan and copy enabled states
  a.copyCurrentEnableStateToScan(1);     // copy current enable states to scan S0000001
  a.copyScanEnableStateToAnalysis(1, 1); // copy enable states of scan S0000001 to analysis A0000001
  a.copyScanEnableStateToScan(1, 2);     // copy enable states of scan S0000001 to scan S0000002

  // D1A_B3_S1_M0 was disabled
  assert ( !a.getConnObjectData("D1A_B3_S1_M0").currentEnableState().enabled() );
  assert ( !a.getConnObjectData("D1A_B3_S1_M0").enableStateOfScan(1).enabled() );
  assert ( !a.getConnObjectData("D1A_B3_S1_M0").enableStateOfScan(2).enabled() );
  // ROD_C1_S2 was disabled
  assert ( !a.getConnObjectData("ROD_C1_S2").currentEnableState().enabled() );
  assert ( !a.getConnObjectData("ROD_C1_S2").enableStateOfScan(1).enabled() );
  assert ( !a.getConnObjectData("ROD_C1_S2").enableStateOfScan(2).enabled() );

  // D1A_B2_S1_M1 was enabled
  assert ( a.getConnObjectData("D1A_B2_S1_M1").currentEnableState().enabled() );
  assert ( a.getConnObjectData("D1A_B2_S1_M1").enableStateOfScan(1).enabled() );
  assert ( a.getConnObjectData("D1A_B2_S1_M1").enableStateOfScan(2).enabled() );

  // ROD_C1_S1
  assert ( a.getConnObjectData("ROD_C1_S1").currentEnableState().enabled() );
  assert ( a.getConnObjectData("ROD_C1_S1").enableStateOfScan(1).enabled() );
  assert ( a.getConnObjectData("ROD_C1_S1").enableStateOfScan(2).enabled() );

  {
  std::set<std::string> ref;
  ref.insert("Analysis Status");
  ref.insert("ExtHistoStateVar"); 
  ref.insert("HistoValVar");
  std::set<std::string> var_list;
  a.varList(PixCon::kCurrent, 0, var_list);
  for(std::set<std::string>::const_iterator var_iter = var_list.begin();
      var_iter != var_list.end();
      ++var_iter) {
    std::cout << *var_iter << std::endl;
  }
  assert(ref == var_list ) ;
 }

  // cleanup
  a.removeScan(2);     // renove all information for scan S000002
  a.removeScanUpTo(2); // remove all information for scans from S000000 to S000002
  a.removeAnalysis(1); // renove all information for anlysis A00000!
  a.removeAnalysisUpTo(1); // remove all information for analysis from A000000 to A000001
  a.resetCurrent();    // reset the current state and remove all current values

  return 0;
}
