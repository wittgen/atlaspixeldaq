/* Dear emacs, this is -*-c++-*- */
#ifndef _CalibrationDataFunctorBase_H_
#define _CalibrationDataFunctorBase_H_

#include "ICalibrationDataFunctor.h"

namespace PixCon {

  class CalibrationDataFunctorBase : public ICalibrationDataFunctor
  {
  public:

    CalibrationDataFunctorBase(unsigned int mask = s_rodModuleMask) : m_connItemMask(mask) {}

    bool filter(EConnItemType item_type) const { return connItemMask(item_type) & m_connItemMask; }

    static unsigned int connItemMask(EConnItemType item_type) { return (1<<item_type); }

    static unsigned int allItemMask() { return s_allMask; }

    static unsigned int rodModuleItemMask() { return s_rodModuleMask; }

  private:
    static const unsigned int s_allMask = UINT_MAX;
    static const unsigned int s_rodModuleMask = (1<<kRodItem) | (1<<kModuleItem);

    unsigned int m_connItemMask;
  };

  class CalibrationDataFunctorAllItems : public ICalibrationDataFunctor
  {
  public:

    bool filter(EConnItemType ) const { return true; }

  };

}
#endif
