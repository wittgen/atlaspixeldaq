#ifndef _ExampleData_H_
#define _ExampleData_H_

#include <map>
#include <string>
#include <cmath>

#include <is/infodictionary.h>
#include <is/infoT.h>

#include <omnithread.h>

class IPCPartition;

namespace PixA {
  class ConnectivityRef;
}

class ExampleData : public omni_thread
{
public:
  ExampleData(const PixA::ConnectivityRef &conn, const IPCPartition &partition, const std::string &is_server_name);

  //  createState();
  void abort() { m_abort = true; }

  void *run_undetached(void *arg) {
    assert(arg==NULL);
    run();
    return NULL;
  }

  void setUpdateFreq(unsigned int update_freq) {
    if (update_freq>0) {
      m_updateTime=update_freq;
    }
  }

 protected:
  void run();

private:
  class FakeData_t 
  {
  public:
    FakeData_t();
    FakeData_t(const FakeData_t &a);
    FakeData_t(float mean, float rms, float min, float max, float amplitude, float jitter, float period);

    float value();

  private:

    float filter(float value) const {
      if (m_max>m_min) {
	float ret= m_min + (m_max-m_min) * 1/(1+exp(-5*(value-(m_max+m_min)*.5)/(m_max-m_min)));
	return ret;
      }
      else {
	return value;
      }
    }

    float m_mean;
    float m_rms;
    float m_min;
    float m_max;
    float m_amplitude;
    float m_jitter;
    float m_current;
    float m_phase;
    float m_phaseVelocity;

  };


  ISInfoDictionary m_isDictionary;

  std::map<std::string, std::pair<FakeData_t, ISInfoFloat> > m_moduleVar;
  std::map<std::string, std::pair<FakeData_t, ISInfoFloat> > m_pp0Var;

  unsigned int m_updateTime;
  bool m_abort;
};
#endif
