#!/bin/sh

pid_file=/tmp/${USER}/.${USER}.is_test_pids
partition_name=${USER}_test
is_name=PixFakeData
wait_before_term=5
serial_number_path=/tmp/${USER}

# check existance of tools
for i in ipc_ls ipc_server mrs_server is_ls is_server; do
#$SN_SVC/serialNumberServer;
  if (which $i >/dev/null 2>/dev/null); then
    :
  else
    echo "ERROR [$0] $i does not exist. Maybe You have to adjust Your environment ?   EXIT." >&2
    exit -1
  fi
done

# no start or stop services

case $1 in
(start) 
if test -f $pid_file; then
  echo "WARNING: The servers may be running already. Stop them first." >&2
else

if (ipc_ls | grep -q initial); then
  echo "ipc root server already running. Will use the existing root ipc server"
  ipc_server_root_pid=""
else
echo "ipc root server"
ipc_root_server_id=$(if (ipc_ls 2>/dev/null | grep initial >/dev/null);  then
  :
else
  # echo "--- ipc_server  ----"
  ipc_server_root_pid=""
  ipc_err_file=`mktemp /tmp/${USER}/$USER.ipc_server_err_XXXX`
  ipc_server > /dev/null  2>$ipc_err_file &
  ipc_server_root_pid=$!
  while ! (ipc_ls 2>/dev/null | grep initial >/dev/null); do
      if test -s $ipc_err_file; then
	  echo "ERROR: while starting initial ipc server." >&2
	  cat $ipc_err_file >&2
	  exit -1
      fi
      sleep 1
  done
#  echo "Startet new root ipc server ($HOSTNAME:$ipc_server_root_pid)"
#  echo ""
  echo "ipc_root_server::$USER@$HOSTNAME:$ipc_server_root_pid"
fi) || exit -1
fi
echo "IPC partition server"
ipc_partition_server_id=$(if (ipc_ls 2>/dev/null | grep $partition_name >/dev/null);  then
  :
else
  # echo "---- ipc server partition ${partition_name} ----"
  ipc_partition_server_pid=""
  ipc_part_err_file=`mktemp /tmp/${USER}/$USER.ipc_part_server_err_XXXX`
  ipc_server -p $partition_name > /dev/null 2> ${ipc_part_err_file} &
  ipc_partition_server_pid=$!
  while ! (ipc_ls 2> /dev/null | grep $partition_name >/dev/null); do
      if test -s $ipc_part_err_file; then
	  echo "ERROR: while starting ipc server for partition ${partition_name}" >&2
	  cat $ipc_part_err_file >&2
	  exit -1
      fi
      sleep 1
  done
  # echo "Started new ipc server for partition ${partition_name} ($HOSTNAME:$ipc_partition_server_pid)"
  # echo ""
  echo "ipc_partition_server::$USER@$HOSTNAME:$ipc_partition_server_pid"
fi) || exit -1

if (false); then
echo "MRS server and worker." 
mrs_server_id=$(if (ipc_ls -p $partition_name 2> /dev/null | grep MRS >/dev/null);  then
  :
else
  # echo "---- ipc server partition ${partition_name} ----"
  mrs_server_server_pid=""
  ipc_part_err_file=`mktemp /tmp/${USER}/$USER.ipc_part_server_err_XXXX`
  mrs_server -p $partition_name > /dev/null 2> ${ipc_part_err_file} &
  mrs_server_server_pid=$!
  while ! (ipc_ls -p $partition_name 2> /dev/null | grep MRS >/dev/null); do
      if test -s $ipc_part_err_file; then
	  echo "ERROR: while starting mrs server in partition ${partition_name}" >&2
	  cat $ipc_part_err_file >&2
	  exit -1
      fi
      sleep 1
  done
  # echo "Started new ipc server for partition ${partition_name} ($HOSTNAME:$mrs_server_server_pid)"
  # echo ""
  echo "mrs_server_server::$USER@$HOSTNAME:$mrs_server_server_pid"

  if (which mrs_worker 2>/dev/null >/dev/null); then
  mrs_worker_pid=""
  ipc_part_err_file=`mktemp /tmp/${USER}/$USER.ipc_part_server__err_XXXX`
  mrs_worker -p $partition_name > /dev/null 2> ${ipc_part_err_file} &
  mrs_worker_pid=$!
  if test -e test_mrs; then
    ./test_mrs $partition_name >&2 ||	 ( echo "ERROR: MRS test on ${partition_name} failed" >&2; false ) || exit -1
  fi
   echo "mrs_worker::$USER@$HOSTNAME:$mrs_worker_pid"
  fi
fi
) || exit -1
fi

echo "IS server"
is_server_id=$(if (is_ls -p ${partition_name} 2>/dev/null | grep -v -E "^Partition" | grep $is_name >/dev/null);  then
  :
else
  is_server_pid=""
  is_server_err_file=`mktemp /tmp/${USER}/$USER.is_server_err_XXXX`
  is_server -n $is_name -p $partition_name >/dev/null 2> ${is_server_err_file} &
  is_server_pid=$!
  while ! (is_ls -p ${partition_name} 2>/dev/null | grep -v -E "^Partition" | grep $is_name >/dev/null); do
      is_ls -p ${partition_name} >&2
      if test -s $is_server_err_file; then
	  echo "ERROR: while starting is server for ${is_name}" >&2
	  cat $is_server_err_file >&2
	  exit -1
      fi
      sleep 1
  done
  # echo "Started new is server for ${server_name} (pid=$is_server_pid)"
  # echo ""
  echo "is_server::$USER@$HOSTNAME:$is_server_pid"
fi) || exit -1
echo "IS server : $(is_ls -p ${partition_name} 2>/dev/null| grep -v -E "^Partition" | grep $is_name)"

if (false); then
echo "Serial number service"
ipc_service_name=${partition_name}_SerialNumberService
sense_server_id=$(if (ipc_ls -p $partition_name 2> /dev/null | grep ${ipc_service_name} >/dev/null);  then
  :
else
  # echo "---- ipc server partition ${partition_name} ----"
  sense_server_server_pid=""
  ipc_part_err_file=`mktemp /tmp/${USER}/$USER.ipc_part_server_err_XXXX`
  sense_exe=${SN_SVC}/serialNumberServer
  export LD_LIBRARY_PATH=${PIX_ANA}/ConfigWrapper:${SN_SVC}:$LD_LIBRARY_PATH; 
  if test -x ${sense_exe}; then
  ${sense_exe} ${partition_name} ${serial_number_path} > /dev/null 2> ${ipc_part_err_file} &
  sense_server_server_pid=$!
  while ! (ipc_ls -p $partition_name 2> /dev/null | grep ${ipc_service_name} >/dev/null); do
      if test -s $ipc_part_err_file; then
	  echo "ERROR: while starting serial number service in partition ${partition_name}" >&2
	  cat $ipc_part_err_file >&2
	  exit -1
      fi
      sleep 1
  done
  # echo "Started new ipc server for partition ${partition_name} ($HOSTNAME:$mrs_server_server_pid)"
  # echo ""
  echo "${ipc_service_name}::$USER@$HOSTNAME:$sense_server_server_pid"
  else
      echo "ERROR [Serial number service] Not an executable : ${sense_exe} " >&2
      exit -1
  fi
fi) || exit -1
fi


echo $ipc_root_server_id $rdb_server_id $ipc_partition_server_id $mrs_server_id ${is_server_id} ${sense_server_id} > ${pid_file} 
fi
;;
(stop)
if test -f $pid_file; then
  for i in `cat $pid_file`; do
     name=${i%::*}
     if test "$name" = "$i"; then
       :
     else 
     i=${i#*::}
     user=${i%%\@*} 
     hostname=${i#*\@}
     pid=${hostname##*:}
     hostname=${hostname%:*}
     echo "kill $name (pid = $pid) : $user @ $hostname "
     if test x$HOSTNAME = x$hostname && test x$USER = x$user; then
	 ( kill $pid 2>/dev/null; sleep ${wait_before_term}; kill -9 $pid 2> /dev/null) &
     else 
	 ssh $user@$hostname sh -c "kill $pid 2>/dev/null; sleep ${wait_before_term}; kill -9 $pid 2>/dev/null " &
     fi
     fi
  done
  rm $pid_file
fi 
;;
(*)
   echo "USAGE $0 start | stop"
   echo ""
   echo "Will start (or stop) : "
   echo " * ipc root partition,"
   echo " * ipc partition ${partition_name},"
   echo " * is server ${is_name} in partition ${partition_name},"
   echo " * mrs server in partition ${partition_name},"
   echo " * serial number service in partition ${partition_name}."
   exit -1
   ;;
esac


