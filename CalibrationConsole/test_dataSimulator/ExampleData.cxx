#include <ipc/partition.h>
#include <ipc/core.h>
#include "ExampleData.h"
#include <ConfigWrapper/Connectivity.h>
#include <ConfigWrapper/ConnectivityUtil.h>
#include <TRandom.h>

#include <unistd.h>
#include <DcsVariables.h>

ExampleData::FakeData_t::FakeData_t()
  : m_mean(0.),
    m_rms(0.),
    m_min(0.),
    m_max(0.),
    m_amplitude(0.),
    m_jitter(0.),
    m_current(0.),
    m_phase(0.),
    m_phaseVelocity(0.)
{
}

ExampleData::FakeData_t::FakeData_t(float mean, float rms, float min, float max, float amplitude, float jitter, float period)
  : m_mean(mean),
    m_rms(rms),
    m_min(min),
    m_max(max),
    m_amplitude(amplitude),
    m_jitter(jitter),
    m_current(filter(gRandom->Gaus(mean,rms))),
    m_phase(gRandom->Rndm(0)*2*M_PI),
    m_phaseVelocity(2*M_PI/period*10)
{
}

ExampleData::FakeData_t::FakeData_t(const FakeData_t &a)
  : m_mean(a.m_mean),
    m_rms(a.m_rms),
    m_min(a.m_min),
    m_max(a.m_max),
    m_amplitude(a.m_amplitude),
    m_jitter(a.m_jitter),
    m_current(filter(gRandom->Gaus(m_mean,m_rms))),
    m_phase(a.m_phase+gRandom->Gaus(0,a.m_phaseVelocity*.1)),
    m_phaseVelocity(a.m_phaseVelocity)
{
}


ExampleData::ExampleData(const PixA::ConnectivityRef &conn, const IPCPartition &partition, const std::string &is_server_name)
  : m_isDictionary (partition),
    m_updateTime(10),
    m_abort(false)
{
  std::map<PixCon::DcsVariables::EVariable, FakeData_t > data;

  data[PixCon::DcsVariables::HVON]=FakeData_t(150,20,0,200,0,0,300);
  data[PixCon::DcsVariables::HV_MON]=FakeData_t(150,20,0,200,5,.5,300);
  data[PixCon::DcsVariables::IHV_MON]=FakeData_t(10,3,0,100,5,1,600);
  data[PixCon::DcsVariables::VDDD]=FakeData_t(2,.5,0,2.5,0,0,150);
  data[PixCon::DcsVariables::VDDD_MON]=FakeData_t(2,.5,0,2.5,.5,.1,150);
  data[PixCon::DcsVariables::IDDD_MON]=FakeData_t(1,.07,0,2,0.1,.01,300);
  data[PixCon::DcsVariables::WIENERD_MON]=FakeData_t(2,.5,0,2.5,.5,.1,150);
  data[PixCon::DcsVariables::VDDA_MON]=FakeData_t(2,.5,0,2.5,.5,.1,150);
  data[PixCon::DcsVariables::VDDA]=FakeData_t(2,.5,0,2.5,0,0,150);
  data[PixCon::DcsVariables::WIENERA_MON]=FakeData_t(2,.5,0,2.5,.5,.1,150);
  data[PixCon::DcsVariables::IDDA_MON]=FakeData_t(1,.07,0,2,0.1,.01,300);
  data[PixCon::DcsVariables::TMODULE]=FakeData_t(-15,5,-20,0,5,1,1000);
  data[PixCon::DcsVariables::TOPTO]=FakeData_t(12,10,-20,35,10,1,1000);
  data[PixCon::DcsVariables::VISET]=FakeData_t(.8,.3,0,1.5,0,0,600);
  data[PixCon::DcsVariables::VISET_MON]=FakeData_t(.8,.3,0,1.5,.2,.1,600);
  data[PixCon::DcsVariables::IVISET_MON]=FakeData_t(1,.07,0,2,0.1,.01,300);
  data[PixCon::DcsVariables::VPIN]=FakeData_t(.8,.3,0,1.5,0,0,600);
  data[PixCon::DcsVariables::VPIN_MON]=FakeData_t(.8,.3,0,1.5,.2,.1,600);
  data[PixCon::DcsVariables::IPIN_MON]=FakeData_t(1,.07,0,2,0.1,.01,300);
  data[PixCon::DcsVariables::VVDC]=FakeData_t(.8,.3,0,1.5,0,0,600);
  data[PixCon::DcsVariables::VVDC_MON]=FakeData_t(.8,.3,0,1.5,.2,.1,600);
  data[PixCon::DcsVariables::IVDC_MON]=FakeData_t(1,.07,0,2,0.1,.01,300);


  for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
       crate_iter != conn.crates().end();
       ++crate_iter) {

    PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
    PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
    for (PixA::RodList::const_iterator rod_iter = rod_begin;
	 rod_iter != rod_end;
	 ++rod_iter) {

      PixA::Pp0List::const_iterator pp0_begin = PixA::pp0Begin(rod_iter);
      PixA::Pp0List::const_iterator pp0_end = PixA::pp0End(rod_iter);
      for (PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
	   pp0_iter != pp0_end;
	   ++pp0_iter) {

	std::string pp0_name = PixA::connectivityName(pp0_iter);
	for (std::vector<PixCon::DcsVariables::EVariable>::const_iterator pp0_var_iter = PixCon::DcsVariables::beginPp0Var();
	     pp0_var_iter != PixCon::DcsVariables::endPp0Var();
	     pp0_var_iter++) {

	  std::string is_name = PixCon::DcsVariables::makeIsName(is_server_name,pp0_name,*pp0_var_iter);
	  m_pp0Var[is_name] = std::make_pair(data[*pp0_var_iter],ISInfoFloat());
	  m_pp0Var[is_name].second.setValue( m_pp0Var[is_name].first.value() );
	  
	  std::cout << "INFO [ExampleData::ctor] new IS variable " << is_name  << " value = " << m_pp0Var[is_name].second.getValue() << std::endl;
          try {
	    m_isDictionary.insert(is_name, m_pp0Var[is_name].second);
          } catch ( daq::is::Exception & ex ) {
          }
	}

	PixA::ModuleList::const_iterator module_begin = PixA::modulesBegin(pp0_iter);
	PixA::ModuleList::const_iterator module_end = PixA::modulesEnd(pp0_iter);
	for (PixA::ModuleList::const_iterator module_iter = module_begin;
	     module_iter != module_end;
	     ++module_iter) {

	  std::string module_name = PixA::connectivityName(module_iter);
	  for (std::vector<PixCon::DcsVariables::EVariable>::const_iterator module_var_iter = PixCon::DcsVariables::beginModuleVar();
	       module_var_iter != PixCon::DcsVariables::endModuleVar();
	       module_var_iter++) {

	    std::string is_name = PixCon::DcsVariables::makeIsName(is_server_name,module_name,*module_var_iter);
	    m_moduleVar[is_name] = std::make_pair(data[*module_var_iter],ISInfoFloat());
	    m_moduleVar[is_name].second.setValue( m_moduleVar[is_name].first.value() );

	    std::cout << "INFO [ExampleData::ctor] new IS variable " << is_name << " value = " << m_moduleVar[is_name].second.getValue() << std::endl;
	    try {
	      m_isDictionary.insert(is_name, m_moduleVar[is_name].second);
            } catch ( daq::is::Exception & ex ) {
            }

	  }
	}
      }
    }
  }
  start_undetached();
}

void ExampleData::run()
{
  for(;!m_abort;) {
    sleep(m_updateTime);
    if (m_abort) break;

    std::map<std::string, std::pair<FakeData_t, ISInfoFloat> > *var_listP[2]={&m_pp0Var,&m_moduleVar};

    for (unsigned int list_i=0; list_i<2; list_i++) {
      for (std::map<std::string, std::pair<FakeData_t, ISInfoFloat> >::iterator var_iter = var_listP[list_i]->begin();
	   var_iter != var_listP[list_i]->end();
	   var_iter++) {
	var_iter->second.second.setValue( var_iter->second.first.value() );
	m_isDictionary.update(var_iter->first,var_iter->second.second);
      }
    }

  }
}

float ExampleData::FakeData_t::value() {
  m_phase++;
  m_current = filter ( m_current + m_amplitude *cos(m_phase*m_phaseVelocity) + gRandom->Gaus(0,m_jitter) );
  return m_current;
}
