#include <ipc/partition.h>
#include <ipc/core.h>
#include "ExampleStateData.h"
#include <ConfigWrapper/Connectivity.h>
#include <ConfigWrapper/ConnectivityUtil.h>

#include <unistd.h>
#include <cassert>
#include <cstdlib>

const char *ExampleStateData::s_rodStateName[ExampleStateData::kNRodStates]={
  "RODBusy",
  "ROSBufferFull",
};

const std::string ExampleStateData::s_connNameSeparator("_");
const char ExampleStateData::s_isServerNameSeparator('.');


ExampleStateData::StateData_t::StateData_t(const std::vector<float > & transition_prob, const std::vector<float > & frequency) 
  : m_state(0)
{
  assert(transition_prob.size() == frequency.size()+1);
  float sum=0.;
  for (std::vector<float>::const_iterator freq_iter = frequency.begin();
       freq_iter != frequency.end();
       freq_iter++) {
    sum += *freq_iter;
  }
  assert(sum<1.);
  m_probabilities.push_back(1-sum);
  for (std::vector<float>::const_iterator freq_iter = frequency.begin();
       freq_iter != frequency.end();
       freq_iter++) {
    m_probabilities.push_back( *freq_iter );
  }
  m_transition = transition_prob;
  for (std::vector<float>::const_iterator trans_iter = transition_prob.begin();
       trans_iter != transition_prob.end();
       trans_iter++) {
    assert( *trans_iter < 1.);
  }
  assert(m_transition.size() == m_probabilities.size());
}

unsigned int ExampleStateData::StateData_t::value() 
{
  float random_transition=static_cast<float>(rand())/RAND_MAX;

  assert(m_state< m_transition.size());
  assert(m_probabilities.size() == m_transition.size());
  if (random_transition < m_transition[m_state] && m_probabilities[m_state]<1.) {
    float random_choice=static_cast<float>(rand())/RAND_MAX;
    float scale = 1./(1-m_probabilities[m_state]);
    unsigned int state_i=0;
    float sum=0;
    for (std::vector<float>::const_iterator prob_iter = m_probabilities.begin();
	 prob_iter != m_probabilities.end();
	 prob_iter++, state_i++) {
      if (state_i!= m_state) {
	sum += *prob_iter;
	if (sum*scale>random_choice) {
	  m_state=state_i;
	  return state_i;
	}
      }
    }
  }
  return m_state;
}


ExampleStateData::ExampleStateData(const PixA::ConnectivityRef &conn, const IPCPartition &partition, const std::string &is_server_name)
  : m_isDictionary (partition),
    m_updateTime(10),
    m_abort(false)
{
  std::map<ERodStateVar, FakeData_t > data;

  data[kRODBusy]=FakeData_t(.05,.05,.1);
  data[kROS]=FakeData_t(.05,.095,.10,.1,.03);

  for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
       crate_iter != conn.crates().end();
       ++crate_iter) {

    PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
    PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
    for (PixA::RodList::const_iterator rod_iter = rod_begin;
	 rod_iter != rod_end;
	 ++rod_iter) {

      std::string rod_name = PixA::connectivityName(rod_iter);
      for (unsigned short state_var_i=0; state_var_i < kNRodStates; state_var_i++) {

	std::string is_name = makeIsName(is_server_name,rod_name, s_rodStateName[state_var_i]);
	m_rodVar[is_name] = std::make_pair(data[static_cast<ERodStateVar>(state_var_i)],ISInfoInt());
	m_rodVar[is_name].second.setValue( m_rodVar[is_name].first.value() );

	std::cout << "INFO [ExampleStateData::ctor] new IS variable " << is_name << " value = " << m_rodVar[is_name].second.getValue() << std::endl;
        try {
	  m_isDictionary.insert(is_name, m_rodVar[is_name].second);
        } catch ( daq::is::Exception & ex ) {
        }

      }


//       PixA::Pp0List::const_iterator pp0_begin = PixA::pp0Begin(rod_iter);
//       PixA::Pp0List::const_iterator pp0_end = PixA::pp0End(rod_iter);
//       for (PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
// 	   pp0_iter != pp0_end;
// 	   ++pp0_iter) {

// 	std::string pp0_name = PixA::connectivityName(pp0_iter);
// 	for (std::vector<PixCon::DcsVariables::EVariable>::const_iterator pp0_var_iter = PixCon::DcsVariables::beginPp0Var();
// 	     pp0_var_iter != PixCon::DcsVariables::endPp0Var();
// 	     pp0_var_iter++) {

// 	  std::string is_name = PixCon::DcsVariables::makeIsName(is_server_name,pp0_name,*pp0_var_iter);
// 	  m_pp0Var[is_name] = std::make_pair(data[*pp0_var_iter],ISInfoFloat());
// 	  m_pp0Var[is_name].second.setValue( m_pp0Var[is_name].first.value() );
	  
// 	  std::cout << "INFO [ExampleStateData::ctor] new IS variable " << is_name  << " value = " << m_pp0Var[is_name].second.getValue() << std::endl;

// 	  m_isDictionary.insert(is_name, m_pp0Var[is_name].second);
// 	}

// 	PixA::ModuleList::const_iterator module_begin = PixA::modulesBegin(pp0_iter);
// 	PixA::ModuleList::const_iterator module_end = PixA::modulesEnd(pp0_iter);
// 	for (PixA::ModuleList::const_iterator module_iter = module_begin;
// 	     module_iter != module_end;
// 	     ++module_iter) {

// 	}
//      }
    }
  }

  start_undetached();
}

void ExampleStateData::run()
{
  for(;!m_abort;) {
    sleep(m_updateTime);
    if (m_abort) return;
    std::map<std::string, std::pair<FakeData_t, ISInfoInt> > *var_listP[1]={&m_rodVar};

    for (unsigned int list_i=0; list_i<1; list_i++) {
      for (std::map<std::string, std::pair<FakeData_t, ISInfoInt> >::iterator var_iter = var_listP[list_i]->begin();
	   var_iter != var_listP[list_i]->end();
	   var_iter++) {
	var_iter->second.second.setValue( var_iter->second.first.value() );
	m_isDictionary.update(var_iter->first,var_iter->second.second);
      }
    }

  }
}
