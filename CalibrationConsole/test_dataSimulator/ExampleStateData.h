#ifndef _ExampleStateData_H_
#define _ExampleStateData_H_

#include <map>
#include <vector>
#include <string>
#include <cmath>

#include <is/infodictionary.h>
#include <is/infoT.h>

#include <omnithread.h>

class IPCPartition;

namespace PixA {
  class ConnectivityRef;
}

class ExampleStateData : public omni_thread
{
public:
  ExampleStateData(const PixA::ConnectivityRef &conn, const IPCPartition &partition, const std::string &is_server_name);

  void abort() { m_abort = true; }

  void setUpdateFreq(unsigned int update_freq) {
    if (update_freq>0) {
      m_updateTime=update_freq;
    }
  }

 protected:
  //  createState();
  void *run_undetached(void *arg) {
    assert(arg==NULL);
    run();
    return NULL;
  }

  void run();


private:

  class StateData_t 
  {
  public:
    StateData_t()
      : m_state(0)
    {
      m_probabilities.push_back(1.);
      m_transition.push_back(0.);
    }

    StateData_t(const std::vector<float > & transition_prob, const std::vector<float > & frequency);

    StateData_t(float transition_prob1, float transition_prob2, float frequency2)
      : m_state(0)
    {
      m_probabilities.push_back(1-frequency2);
      m_probabilities.push_back(frequency2);
      m_transition.push_back(transition_prob1);
      m_transition.push_back(transition_prob2);
    }


    StateData_t(float transition_prob1, float transition_prob2, float transition_prob3, float frequency2, float frequency3)
      : m_state(0)
    {
      float sum = frequency2+frequency3;
      m_probabilities.push_back(1-sum);
      m_probabilities.push_back(frequency2);
      m_probabilities.push_back(frequency3);
      m_transition.push_back(transition_prob1);
      m_transition.push_back(transition_prob2);
      m_transition.push_back(transition_prob3);
    }

    unsigned int value();

  private:
    std::vector<float> m_probabilities;
    std::vector<float> m_transition;

    unsigned int m_state;
  };

  typedef StateData_t FakeData_t;

  ISInfoDictionary m_isDictionary;

  std::map<std::string, std::pair<FakeData_t, ISInfoInt> > m_rodVar;

  enum ERodStateVar {kRODBusy, kROS, kNRodStates};
  static const char *s_rodStateName[kNRodStates];
  static const std::string s_connNameSeparator;
  static const char s_isServerNameSeparator;

  static std::string makeIsName(const std::string &is_server_name, const std::string &conn_name, const char *variable_name) {
    return is_server_name + s_isServerNameSeparator + conn_name + s_connNameSeparator+variable_name;
  }

  unsigned int m_updateTime;
  bool m_abort;
};
#endif
