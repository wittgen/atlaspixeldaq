

#include <ipc/partition.h>
#include <ipc/core.h>

#include <ConfigWrapper/pixa_exception.h>
#include <ConfigWrapper/Connectivity.h>
#include <ConfigWrapper/ConnectivityManager.h>

#include <ConfigWrapper/createDbServer.h>
#include <memory>
#include <memory>

#include "ExampleData.h"
#include "ExampleStateData.h"

int main(int argc, char **argv)
{
  std::string partition_name;
  std::string tag_name;
  std::string cfg_tag_name;
  std::string is_name = "PixFakeData";
  std::string object_tag_name = "CT";
  std::string db_server_partition_name;
  std::string db_server_name;

  unsigned int update_freq=10;

  for (int arg_i=1; arg_i<argc; arg_i++) {
    if (strcmp(argv[arg_i],"-p")==0 && arg_i+1<argc) {
      partition_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-i")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-' ) {
      object_tag_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-t")==0 && arg_i+2<argc && argv[arg_i+2][0]!='-' && argv[arg_i+1][0]!='-') {
      tag_name = argv[++arg_i];
      cfg_tag_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-n")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      is_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-u")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      update_freq = atoi(argv[++arg_i]);
    }
    else if ( (strcmp(argv[arg_i],"-D")==0
	       || strcmp(argv[arg_i],"--db-server-name")==0)
	      && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      db_server_name = argv[++arg_i];
    }
    else if ( (strcmp(argv[arg_i],"--db-server-partition")==0
	       || strcmp(argv[arg_i],"-p")==0)
	      && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      db_server_partition_name = argv[++arg_i];
    }
    else {
      std::cout << "ERROR ["<< argv[0] << ":main] unhandled argument " << arg_i << " : " << argv[arg_i]<< "." << std::endl;
    }
  }

  if (tag_name.empty() || cfg_tag_name.empty() || partition_name.empty() || update_freq<=0) {
      std::cout << "usage: " << argv[0] << " -p partition [-i object-tag] -t tag cfg-tag [-n \"IS server name\" -u seconds] [--db-server-name]" << std::endl;
      return -1;
  }
  // START IPC

  // Start IPCCore
  IPCCore::init(argc, argv);

  if (db_server_partition_name.empty() && !partition_name.empty()) {
    db_server_partition_name = partition_name;
  }

  std::unique_ptr<IPCPartition> db_server_partition;
  if (!db_server_name.empty() && !db_server_partition_name.empty()) {
    db_server_partition = std::unique_ptr<IPCPartition>(new IPCPartition(db_server_partition_name.c_str()));
    if (!db_server_partition->isValid()) {
      std::cerr << "ERROR [" << argv[0] << ":main] Not a valid partition " << db_server_partition_name
		<< " for the db server." << std::endl;
      return EXIT_FAILURE;
    }

    std::shared_ptr<PixLib::PixDbServerInterface> db_server( PixA::createDbServer(*db_server_partition,
										    db_server_name ));
    if (db_server.get()) {
      PixA::ConnectivityManager::useDbServer( db_server );
    }
    else {
      std::cout << "WARNING [main:" << argv[0] << "] Failed to create the db server." << std::endl;
    }
  }


  PixA::ConnectivityRef conn;
  try {
     conn=PixA::ConnectivityManager::getConnectivity( object_tag_name, tag_name, tag_name, tag_name, cfg_tag_name, 0 );
  }
  catch (PixA::ConfigException &err) {
    std::cout << "FATAL [main] caught exception (ConfigException) : " << err.getDescriptor() << std::endl;
    return -1;
  }
  catch (PixA::BaseException &err) {
    std::cout << "FATAL [main] caught exception (BaseException) : " << err.getDescriptor() << std::endl;
    return -1;
  }
  catch (std::exception &err) {
    std::cout << "FATAL [main] caught exception (std::exception) : " << err.what() << std::endl;
    return -1;
  }
  catch (...) {
    std::cout << "FATAL [main] unhandled exception. "  << std::endl;
    return -1;
  }

  // Create IPCPartition
  std::cout << "INFO [" << argv[0] << ":main] generate fake DCS data on " << partition_name  << " in IS server "  << is_name << std::endl;
  IPCPartition ipcPartition(partition_name.c_str());

  ExampleData example_data(conn, ipcPartition, is_name);
  example_data.setUpdateFreq(update_freq);

  ExampleStateData example_state_data(conn, ipcPartition, is_name);
  example_state_data.setUpdateFreq(update_freq);
  //  example_data.run();
  example_state_data.join(NULL);
  example_data.join(NULL);

  return 0;
}
