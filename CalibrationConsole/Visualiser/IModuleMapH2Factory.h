/* Dear emacs, this is -*-c++-*- */
#ifndef _PixA_IModuleMapH2Factory_h_
#define _PixA_IModuleMapH2Factory_h_

#include <Visualiser/IScanDataProvider.h>

class TH2;

namespace PixA {

  class IModuleMapH2Factory
  {
  public:
    virtual ~IModuleMapH2Factory() {}
    virtual TH2 *createMapH2(const TH2 *h2) = 0;
    virtual TH2 *createMapH2(const TH2 *h2,     SerialNumber_t serial_number, const std::string &conn_name, unsigned int link) = 0;
    virtual TH2 *createBocEditH2(const TH2 *h2, SerialNumber_t serial_number, const std::string &conn_name, unsigned int link) = 0;
  };

}
#endif
