// @author Andrew Nelson (c) 2009
#include "RegisterTestVisualiser.h"
#include <DataContainer/HistoUtil.h>
#include <TCanvas.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TText.h>
#include <Histo/Histo.h>
#include <Visualiser/IScanDataProvider.h>

#include <Visualiser/VisualisationFactory.h>
#include <Visualiser/VisualisationUtil.h>


namespace PixA {

  template <class Histo_t> TH1 *convertToTH1(Histo_t *histo);

  inline TH1 *convertToTH1(const PixLib::Histo *histo) {
    std::unique_ptr<TObject> obj ( transform( histo ) );
    if ( obj.get() && obj->InheritsFrom(TH1::Class())) {
      return static_cast<TH1 *>(obj.release());
    }
    return NULL;
  }

  inline TH1 *convertToTH1(const TH1 *histo) {
    return static_cast<TH1 *>( histo ? histo->Clone() : NULL );
  }


  std::string RegisterTestVisualiser::s_name[2] = { 
    "Show FE Register Histograms",
    "Show MCC Register Histograms" };

  void RegisterTestVisualiser::fillIndexArray(IScanDataProvider &data_provider, const std::vector<std::string> &module_names,  HistoInfo_t &info) const {
    info.reset();
    if(module_names.empty()) return; 
    for (std::vector< std::string >::const_iterator module_iter=module_names.begin();
        module_iter != module_names.end();
        module_iter++) {

      HistoInfo_t an_info;
      data_provider.numberOfHistos(*module_iter, m_histoName, an_info);
      info.makeUnion(an_info);

    }
    m_info = info;
  }

  void RegisterTestVisualiser::visualise(IScanDataProvider &data_provider, const std::vector<std::string> &module_names, int option, const Index_t &index, int ) 
  {
    if(module_names.empty()) return;

    PlotCanvas_t plot_canvas=PlotCanvas_t::canvas(1);
    gStyle->SetOptStat(0);
    gStyle->SetOptTitle(0);

    TObject *obj=NULL;
    TH2 *hist = NULL;

    std::cout << "INFO [RegisterTestVisualiser::visualise] option: " << optionName(option) << std::endl;

    int ipad = 0, counter = 0;
    bool first = 1;
    // divide canvas if looking at more than one plot
    if(module_names.size()>=1) plot_canvas.plot()->Divide(2,4);
    std::vector<TH2*> feplot(module_names.size());
    std::vector<TH2*> mccplot(module_names.size());
    for(std::vector< std::string >::const_iterator module_iter=module_names.begin();
        module_iter != module_names.end();
        module_iter++, ipad++) {
      obj = convertToTH1( data_provider.histogram(*module_iter,m_histoName,index) );
      hist = static_cast<TH2*>(obj);

      std::string mod_name = *module_iter;
      if(first) {
        // overall plot titles
        plot_canvas.setTitle(m_histoName);
        plot_canvas.setId(data_provider.scanNumber());
        if (module_names.size()>1) {
          std::string::size_type pos = mod_name.find("_M");
          if (pos!=std::string::npos) {
            mod_name.erase(pos);
            plot_canvas.setName(mod_name);
          }
        }
        else {
          plot_canvas.setName( *module_iter );
        }
        first = 0; 
      }

      if (hist) {
        hist->SetBit(kCanDelete);
        // plot histograms
        // see RodPixController::getErrorHisto(...)
        if(module_names.size()==1) {
          plot_canvas.plot()->cd();
        } else {
          plot_canvas.plot()->cd(ipad+1);
        }
        if(option==0) {
          feplot[counter] = new TH2D("fe","fe",16,0,16,15,0,15);
          feplot[counter]->SetBit(kCanDelete);
          feplot[counter]->SetMinimum(0);
          setFELabels(feplot[counter]);
          for(int i=0; i<16; ++i) {
            for(int j=0; j<15; ++j) {
              feplot[counter]->SetBinContent( i+1,j+1,hist->GetBinContent(i+1,j+2) );
            }
          }
          feplot[counter]->Draw("colz");
        } else if(option==1) {
          mccplot[counter] = new TH2D("mcc","mcc",16,0,16,2,0,2);
          mccplot[counter]->SetBit(kCanDelete);
          mccplot[counter]->SetMinimum(0);
          setMCCLabels(mccplot[counter]);
          unsigned int fifoerror = (unsigned int)hist->GetBinContent(12,1);
std::cout << "INFO " << " fifoerror " << fifoerror << std::endl;
          for(int i=0; i<10; ++i) {
            mccplot[counter]->SetBinContent(i+1,1,hist->GetBinContent(i+1,1) );
          }
          for(int i=0; i<16; ++i) {
            mccplot[counter]->SetBinContent( i+1,2,( (fifoerror>>i)&0x1 ) );
          mccplot[counter]->Draw("colz");
        } 
        // write histogram title
        TText *a_text=new TText(0.15,0.95,mod_name.c_str());
        a_text->SetTextSize(a_text->GetTextSize()*1.2);
        a_text->SetNDC(kTRUE);
        a_text->SetBit(kCanDelete);
        a_text->Draw();
        counter++; 
      }
      if(plot_canvas.plot()->GetPad(ipad+1)) {
        plot_canvas.plot()->GetPad(ipad+1)->Modified();
      } 
    }
  }

  plot_canvas.update();
}

void RegisterTestVisualiser::setFELabels(TH2 *hist) {
  hist->GetXaxis()->SetBinLabel(1,"FE0");
  hist->GetXaxis()->SetBinLabel(2,"FE1");
  hist->GetXaxis()->SetBinLabel(3,"FE2");
  hist->GetXaxis()->SetBinLabel(4,"FE3");
  hist->GetXaxis()->SetBinLabel(5,"FE4");
  hist->GetXaxis()->SetBinLabel(6,"FE5");
  hist->GetXaxis()->SetBinLabel(7,"FE6");
  hist->GetXaxis()->SetBinLabel(8,"FE7");
  hist->GetXaxis()->SetBinLabel(9,"FE8");
  hist->GetXaxis()->SetBinLabel(10,"FE9");
  hist->GetXaxis()->SetBinLabel(11,"FE10");
  hist->GetXaxis()->SetBinLabel(12,"FE11");
  hist->GetXaxis()->SetBinLabel(13,"FE12");
  hist->GetXaxis()->SetBinLabel(14,"FE13");
  hist->GetXaxis()->SetBinLabel(15,"FE14");
  hist->GetXaxis()->SetBinLabel(16,"FE15");
  hist->GetXaxis()->LabelsOption("v");
  hist->GetXaxis()->SetLabelSize(0.03);
  hist->GetXaxis()->SetTitle("FE Number");

  hist->GetYaxis()->SetBinLabel(1,"GlobalRegister");
  hist->GetYaxis()->SetBinLabel(2,"HitBus");
  hist->GetYaxis()->SetBinLabel(3,"Select");
  hist->GetYaxis()->SetBinLabel(4,"Enable");
  hist->GetYaxis()->SetBinLabel(5,"TDAC0");
  hist->GetYaxis()->SetBinLabel(6,"TDAC1");
  hist->GetYaxis()->SetBinLabel(7,"TDAC2");
  hist->GetYaxis()->SetBinLabel(8,"TDAC3");
  hist->GetYaxis()->SetBinLabel(9,"TDAC4");
  hist->GetYaxis()->SetBinLabel(10,"TDAC5");
  hist->GetYaxis()->SetBinLabel(11,"TDAC6");
  hist->GetYaxis()->SetBinLabel(12,"FDAC0");
  hist->GetYaxis()->SetBinLabel(13,"FDAC1");
  hist->GetYaxis()->SetBinLabel(14,"FDAC2");
  hist->GetYaxis()->SetBinLabel(15,"Kill");
  hist->GetYaxis()->LabelsOption("v");
  hist->GetYaxis()->SetLabelSize(0.03);
}

void RegisterTestVisualiser::setMCCLabels(TH2 *hist) {
  hist->GetXaxis()->SetBinLabel(1,"0");
  hist->GetXaxis()->SetBinLabel(2,"1");
  hist->GetXaxis()->SetBinLabel(3,"2");
  hist->GetXaxis()->SetBinLabel(4,"3");
  hist->GetXaxis()->SetBinLabel(5,"4");
  hist->GetXaxis()->SetBinLabel(6,"5");
  hist->GetXaxis()->SetBinLabel(7,"6");
  hist->GetXaxis()->SetBinLabel(8,"7");
  hist->GetXaxis()->SetBinLabel(9,"8");
  hist->GetXaxis()->SetBinLabel(10,"9");
  hist->GetXaxis()->SetBinLabel(11,"10");
  hist->GetXaxis()->SetBinLabel(12,"11");
  hist->GetXaxis()->SetBinLabel(13,"12");
  hist->GetXaxis()->SetBinLabel(14,"13");
  hist->GetXaxis()->SetBinLabel(15,"14");
  hist->GetXaxis()->SetBinLabel(16,"15");
  hist->GetXaxis()->LabelsOption("v");
  hist->GetXaxis()->SetLabelSize(0.03);
  hist->GetXaxis()->SetTitle("FIFO or MCC Register Number"); 

  hist->GetYaxis()->SetBinLabel(1,"MCC Register");
  hist->GetYaxis()->SetBinLabel(2,"MCC FIFO");
  hist->GetYaxis()->LabelsOption("v");
  hist->GetYaxis()->SetLabelSize(0.03);
}

//  TCanvas *RegisterTestVisualiser::canvas() {
//    TCanvas *a_canvas = static_cast<TCanvas*>(gROOT->GetListOfCanvases()->FindObject("pixcan"));
//    if (!a_canvas) {
//      a_canvas = new TCanvas("pixcan","Pixel Module Analysis Canvas",10,10,700,850);
//    }
//    else {
//      a_canvas->Clear();
//      a_canvas->Show();
//    }
//    a_canvas->cd();
//    return a_canvas;
//  }

RegisterTestVisualisationKit::RegisterTestVisualisationKit(bool can_be_option) : m_canBeOption(can_be_option) {
}

IVisualiser *RegisterTestVisualisationKit::visualiser(const std::string &histo_name,
    const std::vector<std::string> &/*folder_hierarchy*/) const 
{
  return new RegisterTestVisualiser(histo_name);
}

RegisterTestVisualisationKit a_RegisterTestVisualisationKit_instance(true);

bool RegisterTestVisualisationKit::init() {
  VisualisationFactory::registerKit("(^|/)REGTEST",&a_RegisterTestVisualisationKit_instance);
  return true;
}

bool RegisterTestVisualisationKit::s_instantiated = RegisterTestVisualisationKit::init();

}
