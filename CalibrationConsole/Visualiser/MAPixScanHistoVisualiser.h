#ifndef _MAPixScanHistoVisualiser_h_
#define _MAPixScanHistoVisualiser_h_

#include <Visualiser/PixScanHistoVisualiser.h>
#include <Visualiser/IVisualisationKit.h>

#include "ModuleMapVisualisationKitBase.h"

class MAPixScanHistoVisualiser : public PixA::PixScanHistoVisualiser
{
public:

 MAPixScanHistoVisualiser(const std::shared_ptr<PixA::IModuleMapH2Factory> &factory, const std::string &histo_name,
			  bool can_visualise_one_fe)
    : PixScanHistoVisualiser(histo_name, can_visualise_one_fe),
      m_moduleMapFactory(factory)
  {}

protected:
  //    /** function which could be used by derived classes to extend the plot.
  //     * like overlaying the optimum boc parameters.
  //     */
  //    void enhancePlot(TVirtualPad *plot_pad, const PixA::ScanConfigRef &scan_config, TObject *obj );

  //    /** Could be overloaded to make custom titles.
  //     */
  //    virtual std::string makeTitle(const std::string &histo_title);

  /** Could be overloaded to create MATH2F instead of TH2.
   */
  TObject *transform(PixA::IScanDataProvider &data_provider,
		     const std::string &module_name,
		     const PixA::Histo *histo,
		     const PixA::Index_t &index,
		     unsigned int front_end);

protected:
  std::shared_ptr<PixA::IModuleMapH2Factory> m_moduleMapFactory;
};


class MAPixScanHistoVisualisationKit : public PixA::ModuleMapVisualisationKitBase
{
public:
 MAPixScanHistoVisualisationKit(const std::shared_ptr<PixA::IModuleMapH2Factory> &factory, short priority, bool can_visualise_one_fe) 
   : ModuleMapVisualisationKitBase(factory,priority,can_visualise_one_fe) {}

  /** Create a visualiser for the given inputs or return NULL.
   */
  virtual PixA::IVisualiser *visualiser(const std::string &histo_name,
					const std::vector<std::string> &/*folder_hierarchy*/) const
    { return new MAPixScanHistoVisualiser(moduleMapFactory(),histo_name, canVisualiseOneFe());}

};

#endif
