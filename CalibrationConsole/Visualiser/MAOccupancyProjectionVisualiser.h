#ifndef _MAOccupancyProjectionVisualiser_h_
#define _MAOccupancyProjectionVisualiser_h_

#include "MAProjectionVisualiser.h"

class MAOccupancyProjectionVisualiser : public MAProjectionVisualiser
{
public:

  MAOccupancyProjectionVisualiser(const std::shared_ptr<PixA::IModuleMapH2Factory> &factory, const std::string &histo_name,
				  bool can_visualise_one_fe)
    : MAProjectionVisualiser(factory, histo_name, can_visualise_one_fe)
  {}

  /** Get the name of the visualiser.
   */
  const std::string &name() const { return s_name;}

protected:
  /** Create a 1D projection from the 2d histogram for the given pixel types.
   * @param dest the 1d Histogram into which the projection gets filled.
   * @param src the 2d map from which the projection gets build.
   * @param chip the chip number or -1 if all chips should be considered.
   * @param pixel_types the types of the pixels which should be considered.
   */
  void fillHistoFromMap(PixA::IScanDataProvider &data_provider,
			const std::string &module_name,
			TH1 *dest,
			const PixA::Histo *src,
			unsigned int front_end,
			short unsigned int pixel_types);

  void prepare(PixA::IScanDataProvider &data_provider, const std::string &module_name, const PixA::Histo *histo, unsigned int front_end);

  static std::string s_name;
};


class MAOccupancyProjectionVisualisationKit : public MAPixScanHistoVisualisationKit 
{
public:
  MAOccupancyProjectionVisualisationKit(const std::shared_ptr<PixA::IModuleMapH2Factory> &factory, short priority, bool can_visualise_one_fe) 
    : MAPixScanHistoVisualisationKit(factory, priority, can_visualise_one_fe) {}

  /** Create a visualiser for the given inputs or return NULL.
   */
  virtual PixA::IVisualiser *visualiser(const std::string &histo_name,
					const std::vector<std::string> &/*folder_hierarchy*/) const
    { return new MAOccupancyProjectionVisualiser(moduleMapFactory(),histo_name, canVisualiseOneFe());}

};

#endif
