#ifndef _VisualisationRegister_h_
#define _VisualisationRegister_h_

#include <IModuleMapH2Factory.h>
#include <memory>
namespace PixA {

  class VisualisationRegister
  {
  protected:
    VisualisationRegister() {};

    static VisualisationRegister *instance() {
      if (!s_instance) s_instance = new VisualisationRegister;
      return s_instance;
    }

    void _registerMAVisualisers(const std::shared_ptr<PixA::IModuleMapH2Factory> &factory);

  public:
    /**
     * @todo clean up kits ? requires deregister method in the factory
     */
    ~VisualisationRegister();

    static  void registerMAVisualisers(const std::shared_ptr<PixA::IModuleMapH2Factory> &factory) {
      instance()->_registerMAVisualisers(factory);
    }

  private:
    //  std::vector<IVisualisationKit *> m_kits;
    static VisualisationRegister  *s_instance;
  };

}
#endif
