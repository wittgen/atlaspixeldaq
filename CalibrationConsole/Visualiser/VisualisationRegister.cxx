#include "VisualisationRegister.h"
#include "MAPixScanHistoVisualiser.h"
#include "InLinkScanHistoVisualiser.h"
#include "InLinkOutLinkHistoVisualiser.h"
#include "BOCScanHistoVisualiser.h"
#include "MAProjectionVisualiser.h"
#include "MAOccupancyProjectionVisualiser.h"
#include "MAModuleMapVisualisationKit.h"
#include "DACMapVisualisationKit.h"
#include <Visualiser/VisualisationFactory.h>
namespace PixA {

VisualisationRegister::~VisualisationRegister() 
{ 
  // for the time being nothing
  // ... @TODO ...
  // but should deregister (not yet supported) and delete the kits
}

void VisualisationRegister::_registerMAVisualisers(const std::shared_ptr<PixA::IModuleMapH2Factory> &factory)
{
  PixA::VisualisationFactory::registerKit("$",          new MAPixScanHistoVisualisationKit(factory, PixA::IVisualisationKit::fallBackOnly+1,   false));
  PixA::VisualisationFactory::registerKit("(/|^)OCCUPANCY$",new MAPixScanHistoVisualisationKit(factory, PixA::IVisualisationKit::normalPriority+1, true));
  PixA::VisualisationFactory::registerKit("(/|^)OCCUPANCY$",new MAOccupancyProjectionVisualisationKit(factory,   PixA::IVisualisationKit::normalPriority+2,   true));
  PixA::VisualisationFactory::registerKit("(/|^)OCCUPANCY$",new MAProjectionVisualisationKit(factory,   PixA::IVisualisationKit::normalPriority+3, true));
  //PixA::VisualisationFactory::registerKit("(/|^)SCURVE_",   new MAPixScanHistoVisualisationKit(factory, PixA::IVisualisationKit::normalPriority,   true));
  PixA::VisualisationFactory::registerKit("(/|^)SCURVE_",   new MAModuleMapVisualisationKit(factory,   PixA::IVisualisationKit::normalPriority+1));
  PixA::VisualisationFactory::registerKit("(/|^)TOTCAL_",   new MAModuleMapVisualisationKit(factory,   PixA::IVisualisationKit::normalPriority+1)); 
  PixA::VisualisationFactory::registerKit("(/|^)GDAC_THR$",   new MAPixScanHistoVisualisationKit(factory, PixA::IVisualisationKit::normalPriority,   true));
  PixA::VisualisationFactory::registerKit("(/|^)GDAC_THR$",   new MAModuleMapVisualisationKit(factory,   PixA::IVisualisationKit::normalPriority+1));
  PixA::VisualisationFactory::registerKit("(/|^)TDAC_T$",   new MAPixScanHistoVisualisationKit(factory, PixA::IVisualisationKit::normalPriority,   true));
  PixA::VisualisationFactory::registerKit("(/|^)TDAC_T$",   new MAModuleMapVisualisationKit(factory,   PixA::IVisualisationKit::normalPriority+1));
  PixA::VisualisationFactory::registerKit("(/|^)TDAC_THR$",   new MAPixScanHistoVisualisationKit(factory, PixA::IVisualisationKit::normalPriority,   true));
  PixA::VisualisationFactory::registerKit("(/|^)TDAC_THR$",   new MAModuleMapVisualisationKit(factory,   PixA::IVisualisationKit::normalPriority+1));
  PixA::VisualisationFactory::registerKit("(/|^)FDAC_T$",   new MAPixScanHistoVisualisationKit(factory, PixA::IVisualisationKit::normalPriority,   true));
  PixA::VisualisationFactory::registerKit("(/|^)FDAC_T$",   new MAModuleMapVisualisationKit(factory,   PixA::IVisualisationKit::normalPriority+1));
  PixA::VisualisationFactory::registerKit("(/|^)FDAC_TOT$",   new MAPixScanHistoVisualisationKit(factory, PixA::IVisualisationKit::normalPriority,   true));
  PixA::VisualisationFactory::registerKit("(/|^)FDAC_TOT$",   new MAModuleMapVisualisationKit(factory,   PixA::IVisualisationKit::normalPriority+1));
  PixA::VisualisationFactory::registerKit("(/|^)TOT_MEAN$",   new MAPixScanHistoVisualisationKit(factory, PixA::IVisualisationKit::normalPriority,   true));
  PixA::VisualisationFactory::registerKit("(/|^)TOT_MEAN$",   new MAModuleMapVisualisationKit(factory,   PixA::IVisualisationKit::normalPriority+1));
  PixA::VisualisationFactory::registerKit("(/|^)TOT_SIGMA$",   new MAPixScanHistoVisualisationKit(factory, PixA::IVisualisationKit::normalPriority,   true));
  PixA::VisualisationFactory::registerKit("(/|^)TOT_SIGMA$",   new MAModuleMapVisualisationKit(factory,   PixA::IVisualisationKit::normalPriority+1));
  PixA::VisualisationFactory::registerKit("(/|^)BCID_MEAN$",   new MAPixScanHistoVisualisationKit(factory, PixA::IVisualisationKit::normalPriority,   true));
  PixA::VisualisationFactory::registerKit("(/|^)BCID_MEAN$",   new MAModuleMapVisualisationKit(factory,   PixA::IVisualisationKit::normalPriority+1));
  PixA::VisualisationFactory::registerKit("(/|^)BCID_SIGMA$",   new MAPixScanHistoVisualisationKit(factory, PixA::IVisualisationKit::normalPriority,   true));
  PixA::VisualisationFactory::registerKit("(/|^)BCID_SIGMA$",   new MAModuleMapVisualisationKit(factory,   PixA::IVisualisationKit::normalPriority+1));
  // PixA::VisualisationFactory::registerKit("(/|^)TIMEWALK$",   new MAPixScanHistoVisualisationKit(factory, PixA::IVisualisationKit::normalPriority,   true));
  PixA::VisualisationFactory::registerKit("(/|^)TIMEWALK$",   new MAModuleMapVisualisationKit(factory,   PixA::IVisualisationKit::normalPriority+1));
  // PixA::VisualisationFactory::registerKit("(/|^)TIMEWALK$",new MAProjectionVisualisationKit(factory,   PixA::IVisualisationKit::normalPriority+3, true));
 // PixA::VisualisationFactory::registerKit("(/|^)DISCBIAS_T",   new MAPixScanHistoVisualisationKit(factory, PixA::IVisualisationKit::normalPriority,   true));
  //PixA::VisualisationFactory::registerKit("(/|^)DISCBIAS_T$",   new MAModuleMapVisualisationKit(factory,   PixA::IVisualisationKit::normalPriority+1));
  //PixA::VisualisationFactory::registerKit("(/|^)DISCBIAS_TIMEWALK$",   new MAPixScanHistoVisualisationKit(factory, PixA::IVisualisationKit::normalPriority,   true));
  //PixA::VisualisationFactory::registerKit("(/|^)DISCBIAS_TIMEWALK$",   new MAModuleMapVisualisationKit(factory,   PixA::IVisualisationKit::normalPriority+1));
  PixA::VisualisationFactory::registerKit("DAC_map$",   new DACMapVisualisationKit(factory,   PixA::IVisualisationKit::normalPriority+1));

  PixA::VisualisationFactory::registerKit("(/|^)RAW_DATA_",   new BOCScanHistoVisualisationKit(factory,   PixA::IVisualisationKit::normalPriority+1,false));

  PixA::VisualisationFactory::registerKit("(/|^)INLINKMAP",   new  InLinkScanHistoVisualisationKit(factory,   PixA::IVisualisationKit::normalPriority+1,false));
  PixA::VisualisationFactory::registerKit("(/|^)FMTC_LINKMAP",   new  InLinkOutLinkHistoVisualisationKit(factory,   PixA::IVisualisationKit::normalPriority+1,false));

  // need #include "ConfigVisualisationKit.h"
  //  PixA::VisualisationFactory::registerKit("_cfg$",      new ConfigVisualisationKit(factory,
  //										   PixA::IVisualisationKit::normalPriority));
}

VisualisationRegister  *VisualisationRegister::s_instance=NULL;
}
