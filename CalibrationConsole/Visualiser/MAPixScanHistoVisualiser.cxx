#include <TH2.h>
#include "MAPixScanHistoVisualiser.h"
#include "IModuleMapH2Factory.h"

TObject *MAPixScanHistoVisualiser::transform(PixA::IScanDataProvider &data_provider,
					     const std::string &module_name,
					     const PixA::Histo *histo,
					     const PixA::Index_t &index,
					     unsigned int front_end)
{
  TObject *obj = PixA::PixScanHistoVisualiser::transform(data_provider, module_name, histo, index, front_end);
  if (obj!=0 && obj->InheritsFrom(TH2F::Class())) {
    TH2 *ma_h2=m_moduleMapFactory->createMapH2(static_cast<TH2 *>(obj));
    ma_h2->GetXaxis()->SetTitle("Column");
    ma_h2->GetYaxis()->SetTitle("Row");
    delete obj;
    obj=ma_h2;
  }
  return obj;
}
