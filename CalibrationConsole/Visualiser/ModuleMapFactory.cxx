#include "TH2.h"
#include <DataContainer/PixelUtil.h>
#include <iomanip>
#include <sstream>
#include <string>

namespace PixA {
class ModuleMapH2 : public TH2F
{
 public:
  ModuleMapH2()  {}

  ModuleMapH2(const ModuleMapH2 &module_map_h2) : TH2F(module_map_h2) {}

  ModuleMapH2(const TH2F &h2) : TH2F(h2) {}

  char *GetObjectInfo(Int_t px, Int_t py) const {
   if (!gPad) return (char*)"";
   Double_t x  = gPad->PadtoX(gPad->AbsPixeltoX(px));
   Double_t y  = gPad->PadtoY(gPad->AbsPixeltoY(py));
   unsigned int binx   = fXaxis->FindFixBin(x);
   unsigned int biny   = fYaxis->FindFixBin(y);

   if (GetNbinsX()>18 && GetNbinsY()>160) {
     PixA::PixelCoord_t coord(static_cast<int>(fXaxis->GetBinCenter(binx)),static_cast<int>(fYaxis->GetBinCenter(biny)));

     std::stringstream message;
     message << "FE=" << std::setw(2) << coord.chip()
	     << "col=" << std::setw(2) << coord.col()
	     << ", row=" << std::setw(3) << coord.row()
	     << ",  content=" << GetBinContent(GetBin(binx,biny));
     m_currentInfo = message.str();
   }
   else {
     std::stringstream message;
     message << "col=" << std::setw(2) << static_cast<int>(fXaxis->GetBinCenter(binx))
	     << ", row=" << std::setw(3) << static_cast<int>(fYaxis->GetBinCenter(biny))
	     << ",  content=" << GetBinContent(GetBin(binx,biny));
     m_currentInfo = message.str();
   }
   return m_currentInfo.c_str();
  }

 private:
  std::string m_currentInfo;
};

  TH2 *ModuleMapH2Factory::createMapH2(const TH2 *h2) {
    return new ModuleMapH2(h2);
  }


}
