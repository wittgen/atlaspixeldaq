#include <TPad.h>
#include "MAProjectionVisualiser.h"
#include <DataContainer/HistoUtil.h>
#include <Visualiser/TH1Stack.h>
#include <Visualiser/IScanDataProvider.h>
#include <DataContainer/HistoExtraInfoList.h>
#ifdef _PIXA_HISTO_IS_ROOT_TH1
#endif
#include <DataContainer/GenericHistogramAccess.h>

using namespace PixLib;

std::string MAProjectionVisualiser::s_name="1D Projection";

unsigned int MAProjectionVisualiser::getTotalMaskStageSteps(const std::string &label) {
  if (s_totalMaskStageStepMap.empty()) {
    s_totalMaskStageStepMap.insert(std::make_pair<std::string,unsigned int>("STEPS_4",4));
    s_totalMaskStageStepMap.insert(std::make_pair<std::string,unsigned int>("STEPS_32",32));
    s_totalMaskStageStepMap.insert(std::make_pair<std::string,unsigned int>("STEPS_40",40));
    s_totalMaskStageStepMap.insert(std::make_pair<std::string,unsigned int>("STEPS_64",64));
    s_totalMaskStageStepMap.insert(std::make_pair<std::string,unsigned int>("STEPS_80",80));
    s_totalMaskStageStepMap.insert(std::make_pair<std::string,unsigned int>("STEPS_160",160));
    s_totalMaskStageStepMap.insert(std::make_pair<std::string,unsigned int>("STEPS_320",320));
    s_totalMaskStageStepMap.insert(std::make_pair<std::string,unsigned int>("STEPS_2880",2880));
    //FEI4 specific
    s_totalMaskStageStepMap.insert(std::make_pair<std::string,unsigned int>("STEPS_1_DC",1));
    s_totalMaskStageStepMap.insert(std::make_pair<std::string,unsigned int>("STEPS_2_DC",2));
    s_totalMaskStageStepMap.insert(std::make_pair<std::string,unsigned int>("STEPS_3_DC",3));
    s_totalMaskStageStepMap.insert(std::make_pair<std::string,unsigned int>("STEPS_4_DC",4));
    s_totalMaskStageStepMap.insert(std::make_pair<std::string,unsigned int>("STEPS_5_DC",5));
    s_totalMaskStageStepMap.insert(std::make_pair<std::string,unsigned int>("STEPS_6_DC",6));
    s_totalMaskStageStepMap.insert(std::make_pair<std::string,unsigned int>("STEPS_7_DC",7));
    s_totalMaskStageStepMap.insert(std::make_pair<std::string,unsigned int>("STEPS_8_DC",8));
    s_totalMaskStageStepMap.insert(std::make_pair<std::string,unsigned int>("STEPS_26880_DC",26880));
  }
  std::map<std::string, unsigned int>::const_iterator iter = s_totalMaskStageStepMap.find(label);

  if (iter != s_totalMaskStageStepMap.end()) {
    return iter->second;
  }
  return 0;
}

std::map<std::string, unsigned int> MAProjectionVisualiser::s_totalMaskStageStepMap;

void MAProjectionVisualiser::initPlot() {
  m_min=0;
  m_max=0;
  m_nBins=0;
}


void MAProjectionVisualiser::prepare(PixA::IScanDataProvider & data_provider, const std::string &/* histo_name */, const PixA::Histo *histo, unsigned int front_end)
{
  if (!histo ) return;
  //  PixA::ConfigHandle scan_config_handle = data_provider.scanConfig();
  PixGeometry geo(PixA::nRows(histo),PixA::nColumns(histo));
  std::string fe_tag;
  if(geo.pixType()==PixGeometry::FEI2_CHIP ||
	   geo.pixType()==PixGeometry::FEI2_MODULE){
    fe_tag = "Base";
  }else if(geo.pixType()==PixGeometry::FEI4_CHIP ||
	   geo.pixType()==PixGeometry::FEI4_MODULE){
    fe_tag = "Base_I4";
  }
  PixA::ConfigHandle scan_config_handle = data_provider.scanConfig(fe_tag);

  if (!scan_config_handle) return;
  PixA::ConfigRef pix_scan_config = scan_config_handle.ref();

  PixA::ConfGrpRef general_config( pix_scan_config["general"] );
  try {
  PixA::HistoBinning binning=PixA::guessHistoBinningForMapProjection(histo,
								     front_end,
								     PixA::PixelTypeMask_t::all(),
								     PixA::NoReference(),
								     confVal<int>(general_config["maskStageSteps"]),
								     getTotalMaskStageSteps(confVal<std::string>(general_config["maskStageTotalSteps"])));
  if (m_nBins==0) {
    m_min=binning.min();
    m_max=binning.max();
    m_nBins=binning.nBins();
  }
  else if (m_adjustScale) {
    float width=(binning.nBins()>0 ? (binning.max()-binning.min())/binning.nBins() : (binning.max()-binning.min())/100.);
    if (m_min>binning.min()) {
      m_min=binning.min();
    }
    if (m_max<binning.max()) {
      m_max=binning.max();
    }
    if (m_nBins>0) {
      float new_width=(m_max-m_min)/m_nBins;
      if (new_width>width) {
	m_nBins=static_cast<unsigned int>((m_max-m_min)/width);
	if (m_nBins>500) m_nBins=500;
      }
    }
    else {
      m_nBins=static_cast<unsigned int>((m_max-m_min)/width);
      if (m_nBins>500) m_nBins=500;
    }
  }
  }
  catch (PixA::ConfigException &err) {
    std::cout << "FATAL [MAProjectionVisualiser::prepare] Exception : " << err.getDescriptor() << std::endl;
  }

  if (m_nBins<100 && m_adjustScale) m_nBins=100;
  if (m_max<=m_min) {
    m_min=0;
    m_max=64.;
  }

}

void MAProjectionVisualiser::enhancePlot(TVirtualPad *plot_pad, PixA::IScanDataProvider &, const std::string &, const PixA::Index_t &, TObject * )
{
  if (plot_pad) {
    plot_pad->SetLogy(1);
  }
}


TObject *MAProjectionVisualiser::transform(PixA::IScanDataProvider &data_provider,
					   const std::string &module_name,
					   const PixA::Histo *histo,
					   const PixA::Index_t &/* index */,
					   unsigned int front_end)
{
  if (!histo ) return NULL;

  //  PixA::ConfigHandle scan_config_handle = data_provider.scanConfig();
  PixGeometry geo(PixA::nRows(histo),PixA::nColumns(histo));
  std::string fe_tag;
  if(geo.pixType()==PixGeometry::FEI2_CHIP ||
	   geo.pixType()==PixGeometry::FEI2_MODULE){
    fe_tag = "Base";
  }else if(geo.pixType()==PixGeometry::FEI4_CHIP ||
	   geo.pixType()==PixGeometry::FEI4_MODULE){
    fe_tag = "Base_I4";
  }
  PixA::ConfigHandle scan_config_handle = data_provider.scanConfig(fe_tag);

  if (!scan_config_handle) return NULL;
  PixA::ConfigRef pix_scan_config = scan_config_handle.ref();

  std::stringstream histo_name;
  histo_name << "proj1d_" << histoName();

  std::string old_title(PixA::histoName(histo));
  std::string::size_type pos = old_title.find(" mod ");
  if (pos != std::string::npos) {
    old_title.erase(pos);
  }

  std::stringstream histo_title;
  histo_title << "Projection " << old_title;


  unsigned short types[6]={PixA::PixelTypeMask_t::all(),
			   PixA::PixelTypeMask_t::kLongPixel 
			   | PixA::PixelTypeMask_t::kGangedPixel | PixA::PixelTypeMask_t::kInterGangedPixel 
			   | PixA::PixelTypeMask_t::kLongGangedPixel | PixA::PixelTypeMask_t::kLongInterGangedPixel,
			   PixA::PixelTypeMask_t::kLongPixel 
			   | PixA::PixelTypeMask_t::kGangedPixel
			   | PixA::PixelTypeMask_t::kLongGangedPixel | PixA::PixelTypeMask_t::kLongInterGangedPixel,
			   PixA::PixelTypeMask_t::kGangedPixel
			   | PixA::PixelTypeMask_t::kLongGangedPixel | PixA::PixelTypeMask_t::kLongInterGangedPixel,
			   PixA::PixelTypeMask_t::kLongGangedPixel | PixA::PixelTypeMask_t::kLongInterGangedPixel,
			   PixA::PixelTypeMask_t::kLongGangedPixel};

  const char *name_ext[6]={"","_interGanged","_long","_ganged","_longInterGanged","_longGanged"};
  const char *title_ext[6]={"","(inter-ganged)","(long)","(ganged)","(long inter-ganged)","(long ganged)"};

  unsigned short color[6]={5,191,6,106,4,109};

  TH1Stack *h1_stack=new TH1Stack(histo_name.str().c_str(),histo_title.str().c_str(),
				  m_nBins, m_min,m_max);
  if (h1_stack->GetXaxis()) {
    const PixA::HistoExtraInfo_t &extra_info(PixA::HistoExtraInfoList::histoExtraInfo(histoName()));
    h1_stack->GetXaxis()->SetTitle(extra_info.axis(PixA::HistoExtraInfo_t::kZaxis).title().c_str());
  }
  if (h1_stack->GetYaxis()) {
    h1_stack->GetYaxis()->SetTitle("Number of Pixel");
  }

  for (unsigned int type_i=0; type_i<6; type_i++) {

    std::string full_histo_name( histo_name.str() );
    full_histo_name += name_ext[type_i];
    std::string full_histo_title( histo_title.str() );
    full_histo_title += " ";
    full_histo_title += title_ext[type_i];

    TH1 *h1;
    if (type_i==0) {
      h1=h1_stack;
    }
    else {
      h1=h1_stack->NewH1(full_histo_name.c_str(),full_histo_title.c_str());
    }
    fillHistoFromMap(data_provider, module_name, h1,histo,front_end ,types[type_i]);

    h1->SetFillStyle(1001);
    h1->SetFillColor(color[type_i]);
    if (h1->GetXaxis()) h1->GetXaxis()->SetNdivisions(505);
  }
  return h1_stack;
}


void MAProjectionVisualiser::fillHistoFromMap(PixA::IScanDataProvider &data_provider,
					      const std::string &/*module_name*/,
					      TH1 *dest,
					      const PixA::Histo *src,
					      unsigned int front_end,
					      short unsigned int pixel_types)
{
  int mask_stage_steps = 1;
  int total_mask_stage_steps = 1;
  //  PixA::ConfigHandle scan_config_handle = data_provider.scanConfig();
  PixGeometry geo(PixA::nRows(src),PixA::nColumns(src));
  std::string fe_tag;
  if(geo.pixType()==PixGeometry::FEI2_CHIP ||
	   geo.pixType()==PixGeometry::FEI2_MODULE){
    fe_tag = "Base";
  }else if(geo.pixType()==PixGeometry::FEI4_CHIP ||
	   geo.pixType()==PixGeometry::FEI4_MODULE){
    fe_tag = "Base_I4";
  }
  PixA::ConfigHandle scan_config_handle = data_provider.scanConfig(fe_tag);

  if (scan_config_handle) {
    PixA::ConfigRef pix_scan_config = scan_config_handle.ref();
    PixA::ConfGrpRef general_config( pix_scan_config["general"] );
    mask_stage_steps = confVal<int>(general_config["maskStageSteps"]);
    total_mask_stage_steps = getTotalMaskStageSteps(confVal<std::string>(general_config["maskStageTotalSteps"]));
  }

  PixA::fillHistoFromMap(dest,
			 dest,
			 src,
			 front_end,
			 pixel_types,
			 PixA::NoReference(),
			 mask_stage_steps,
			 total_mask_stage_steps);
}
