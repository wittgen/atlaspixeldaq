#ifndef _MAProjectionVisualiser_h_
#define _MAProjectionVisualiser_h_

#include <string>
#include "MAPixScanHistoVisualiser.h"

class TH1;
class MAModuleMapVisualiser;

class MAProjectionVisualiser : public MAPixScanHistoVisualiser
{
  friend class MAModuleMapVisualiser;
public:

  MAProjectionVisualiser(const std::shared_ptr<PixA::IModuleMapH2Factory> &factory, const std::string &histo_name,
			 bool can_visualise_one_fe  )
    : MAPixScanHistoVisualiser(factory,histo_name, can_visualise_one_fe ),
    m_nBins(0),m_min(0),m_max(0),m_adjustScale(true)
  {}

  /** Get the name of the visualiser.
   */
  const std::string &name() const { return s_name;}

protected:
  void initPlot();

  /** show 1D projection of the 2D histogram.
   */
  TObject *transform(PixA::IScanDataProvider &data_provider,
		     const std::string &module_name,
		     const PixA::Histo *histo,
		     const PixA::Index_t &index,
		     unsigned int front_end);

  /** Create a 1D projection from the 2d histogram for the given pixel types.
   * @param data_provider provider of all scan data : scan and module configuration but also other configuratios and histograms.
   * @param module_name the name of the corresponding module.
   * @param dest the 1d Histogram into which the projection gets filled.
   * @param src the 2d map from which the projection gets build.
   * @param chip the chip number or -1 if all chips should be considered.
   * @param pixel_types the types of the pixels which should be considered.
   */
  virtual void fillHistoFromMap(PixA::IScanDataProvider &data_provider,
				const std::string &module_name,
				TH1 *dest,
				const PixA::Histo *src,
				unsigned int front_end,
				short unsigned int pixel_types);

  void prepare(PixA::IScanDataProvider &data_provider, const std::string &module_name, const PixA::Histo *histo, unsigned int front_end);

  void enhancePlot(TVirtualPad *plot_pad, PixA::IScanDataProvider &data_provider,
		   const std::string &module_name,
		   const PixA::Index_t &index,
		   TObject *obj );

  static unsigned int getTotalMaskStageSteps(const std::string &label);
  static std::map<std::string, unsigned int> s_totalMaskStageStepMap;


  unsigned int m_nBins;
  float        m_min;
  float        m_max;
  bool         m_adjustScale;
  static std::string s_name;
};


class MAProjectionVisualisationKit : public MAPixScanHistoVisualisationKit
{
public:
  MAProjectionVisualisationKit(const std::shared_ptr<PixA::IModuleMapH2Factory> &factory, short priority, bool can_visualise_one_fe)
    : MAPixScanHistoVisualisationKit(factory, priority, can_visualise_one_fe) {}

  /** Create a visualiser for the given inputs or return NULL.
   */
  virtual PixA::IVisualiser *visualiser(const std::string &histo_name,
				  const std::vector<std::string> &/*folder_hierarchy*/) const
    { return new MAProjectionVisualiser(moduleMapFactory(),histo_name,canVisualiseOneFe());}

};

#endif
