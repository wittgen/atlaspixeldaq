// Dear emacs, this is -*-c++-*-
#ifndef _ModuleCollectionVisualiser_h_
#define _ModuleCollectionVisualiser_h_

#include <vector>
#include <DataContainer/HistoRef.h>
#include <Visualiser/IVisualiser.h>
#include <Visualiser/RootDbScanDataProvider.h>

#include <memory>


class ModuleCollectionVisualiser
{
public:
  ModuleCollectionVisualiser(std::shared_ptr<PixA::RootDbScanDataProvider> &data_provider,
			     unsigned int label_id,
			     PixA::IVisualiser *visualiser,
			     const std::string &module_name)
    : m_dataProvider(data_provider),
      m_labelId(label_id),
      m_visualiser(visualiser)
  {
    assert(visualiser);
    m_moduleNameList.push_back(module_name);
  }

  ModuleCollectionVisualiser(std::shared_ptr<PixA::RootDbScanDataProvider> &data_provider,
			     unsigned int label_id,
			     PixA::IVisualiser *visualiser,
			     const std::vector<std::string> module_names)
    : m_dataProvider(data_provider),
      m_labelId(label_id),
      m_visualiser(visualiser),
      m_moduleNameList(module_names)
  {
    assert(visualiser);
  }

  ~ModuleCollectionVisualiser() {
    delete m_visualiser;
  }

  const std::string &name() const {
    return m_visualiser->name();
  }

  /** Get the number of options (there is at least one).
   * On option is a variatio of the default visualisation.
   * A visualiser may or may not offer options. Option zero 
   * referrs to the default option.
   */
  unsigned int nOptions() const {
    return m_visualiser->nOptions();
  }

  /** Get the name of visualisation option.
   * @param option the index of the option (must be smaller than the value returned by @nOptions.).
   * @return the name of the option.
   */
  const std::string &optionName(unsigned int option) const {
    return m_visualiser->optionName(option);
  }

  /** Returns true, if the visualiser can optionally display only one front-end.
   */
  bool canVisualiseOneFrontEnd() const {
    return m_visualiser->canVisualiseOneFrontEnd();
  }

  /** Fill the histogram info structure.
   * The structure contains the index ranges for each dimension.
   * @sa HistoInfo_t
   */
  void fillIndexArray(HistoInfo_t &info) const {
    showInfo( "ModuleCollectionVisualiser::fillIndexArray" );
    m_dataProvider->setLabelId(m_labelId);
    m_visualiser->fillIndexArray(*m_dataProvider, m_moduleNameList, info);
  }

  /** Perform the default visualisation for the given front_end and the given index set.
   * @param index the index set to specify the histogram in the multi dimensional histogram collection.
   * @param front_end the number of the front_end (0-15) which should be visualised or -1 for all front-ends
   * The front-end parameter will be ignored if canVisualiseOneFrontEnd returns false.
   */
  void visualise(const PixA::Index_t &index, int front_end) {
    showInfo( "ModuleCollectionVisualiser::visualise" );
    m_dataProvider->setLabelId(m_labelId);
    m_visualiser->visualise(*m_dataProvider, m_moduleNameList, index, front_end);
  }

  /** Perform the chosen visualisation for the given front_end and the given index set.
   * @param option the visualisation option (0 referrs to the default option).
   * @param index the index set to specify the histogram in the multi dimensional histogram collection.
   * @param front_end the number of the front_end (0-15) which should be visualised or -1 for all front-ends
   * The front-end parameter will be ignored if canVisualiseOneFrontEnd returns false.
   */
  void visualise(int option, const PixA::Index_t &index, int front_end) {
    showInfo( "ModuleCollectionVisualiser::visualise" );
    m_dataProvider->setLabelId(m_labelId);
    m_visualiser->visualise(*m_dataProvider, m_moduleNameList, option, index, front_end);
  }

protected:

  void showInfo(const std::string &name) const {
    std::cout << "INFO [" << name << "] label id = " << m_labelId;
    for (std::vector<std::string>::const_iterator name_iter = m_moduleNameList.begin();
	 name_iter != m_moduleNameList.end();
	 name_iter++) {
      std::cout << *name_iter << " ";
    }
    std::cout << " -" << static_cast<void *>(m_visualiser) << std::endl;
  }
private:
  std::shared_ptr<PixA::RootDbScanDataProvider> m_dataProvider;
  unsigned int m_labelId;
  PixA::IVisualiser *m_visualiser;
  std::vector<std::string> m_moduleNameList;
};

#endif
