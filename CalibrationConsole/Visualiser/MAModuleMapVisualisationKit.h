#ifndef _MAModuleMapVisualisationKit_h_
#define _MAModuleMapVisualisationKit_h_

#include "MAProjectionVisualiser.h"

class MAModuleMapVisualisationKit : public MAProjectionVisualisationKit
{
public:
  MAModuleMapVisualisationKit(const std::shared_ptr<PixA::IModuleMapH2Factory> &factory, short priority)
    : MAProjectionVisualisationKit(factory, priority, true) {}

  /** Create a visualiser for the given inputs or return NULL.
   */
  virtual PixA::IVisualiser *visualiser(const std::string &histo_name,
					const std::vector<std::string> &/*folder_hierarchy*/) const;

//  /** Create a visualiser for the histogram collection or return NULL.
//   */
//  virtual PixA::IVisualiser *visualiser(const std::string &histo_name,
//				    const std::vector<std::string> &/*folder_hierarchy*/,
//				    const std::vector< std::pair<PixA::ScanConfigRef, PixA::HistoHandle> > &module_list) const
//  { return new MAProjectionVisualiser(m_topWin,histo_name,module_list,m_canVisualiseOneFe,m_allowDoubleClick);}

};

#endif
