#include <TH2.h>
#include "MAOccupancyProjectionVisualiser.h"
#include <DataContainer/HistoUtil.h>

#include <Visualiser/IScanDataProvider.h>

std::string MAOccupancyProjectionVisualiser::s_name="1D Occupancy Projection";

#include "PixFe/PixGeometry.h"
#include <DataContainer/GenericHistogramAccess.h>
using namespace PixLib;

void MAOccupancyProjectionVisualiser::prepare(PixA::IScanDataProvider & data_provider, const std::string & /* module_name */, const PixA::Histo * histo, unsigned int front_end)
{

  if (!histo) return;

  PixGeometry geo(PixA::nRows(histo),PixA::nColumns(histo));
  std::string fe_tag;
  if(geo.pixType()==PixGeometry::FEI2_CHIP ||
	   geo.pixType()==PixGeometry::FEI2_MODULE){
    fe_tag = "Base";
  }else if(geo.pixType()==PixGeometry::FEI4_CHIP ||
	   geo.pixType()==PixGeometry::FEI4_MODULE){
    fe_tag = "Base_I4";
  }
  //  PixA::ConfigHandle scan_config_handle = data_provider.scanConfig();

  PixA::ConfigHandle scan_config_handle = data_provider.scanConfig(fe_tag);

  if (!scan_config_handle) return;

  PixA::ConfigRef pix_scan_config = scan_config_handle.ref();

  PixA::ConfGrpRef general_config( pix_scan_config["general"] );
  try {
  PixA::HistoBinning binning=PixA::guessHistoBinningForMapProjection(histo,
								     front_end,
								     PixA::PixelTypeMask_t::all(),
								     PixA::SimpleReference(confVal<int>(general_config["repetitions"])),
								     confVal<int>(general_config["maskStageSteps"]),
								     getTotalMaskStageSteps(confVal<std::string>(general_config["maskStageTotalSteps"])));
  // correct binning to integer boundaries
  float a_min=floor(binning.min())-.5;
  float a_max=ceil(binning.max())+.5;
  binning.setBinning(static_cast<unsigned int>(a_max - a_min),a_min,a_max);
  if (m_nBins==0 || m_min>=m_max) {
    m_min=binning.min();
    m_max=binning.max();
    m_nBins=binning.nBins();
  }
  else if (m_adjustScale) {
    if (a_min <  m_min) m_min=a_min;
    if (a_max >  m_max) m_max=a_max;
    m_nBins= static_cast<unsigned int>(m_max - m_min);
  }
  }
  catch (PixA::ConfigException &err) {
    std::cout << "FATAL [MAProjectionVisualiser::prepare] Exception : " << err.getDescriptor() << std::endl;
  }
}

void MAOccupancyProjectionVisualiser::fillHistoFromMap(PixA::IScanDataProvider &data_provider,
						       const std::string & /* module_name */,
						       TH1 *dest,
						       const PixA::Histo *src,
						       unsigned int front_end,
						       short unsigned int pixel_types)

{
  int repetitions = 0;
  int mask_stage_steps = 1;
  int total_mask_stage_steps = 1;

  //  PixA::ConfigHandle scan_config_handle = data_provider.scanConfig();
  PixGeometry geo(PixA::nRows(src),PixA::nColumns(src));
  std::string fe_tag;
  if(geo.pixType()==PixGeometry::FEI2_CHIP ||
	   geo.pixType()==PixGeometry::FEI2_MODULE){
    fe_tag = "Base";
  }else if(geo.pixType()==PixGeometry::FEI4_CHIP ||
	   geo.pixType()==PixGeometry::FEI4_MODULE){
    fe_tag = "Base_I4";
  }
  PixA::ConfigHandle scan_config_handle = data_provider.scanConfig(fe_tag);

  if (scan_config_handle) {
    PixA::ConfigRef pix_scan_config = scan_config_handle.ref();
    PixA::ConfGrpRef general_config( pix_scan_config["general"] );
    repetitions = confVal<int>(general_config["repetitions"]);
    mask_stage_steps = confVal<int>(general_config["maskStageSteps"]);
    total_mask_stage_steps = getTotalMaskStageSteps(confVal<std::string>(general_config["maskStageTotalSteps"]));
    std::cout << "~~~~~MAOcc total_mask: " << total_mask_stage_steps << std::endl;
  }


  PixA::fillHistoFromMap(dest,
			 dest,
			 src,
			 front_end,
			 pixel_types,
			 PixA::SimpleReference(repetitions),
			 mask_stage_steps,
			 total_mask_stage_steps);

  if (dest->GetXaxis()) {
    dest->GetXaxis()->SetNdivisions(505);
  }
}



