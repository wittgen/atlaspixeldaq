#ifndef _InLinkOutLinkHistoVisualiser_h_
#define _InLinkOutLinkHistoVisualiser_h_

#include "ModuleMapVisualisationKitBase.h"


class InLinkOutLinkHistoVisualisationKit : public PixA::ModuleMapVisualisationKitBase
{
public:

 InLinkOutLinkHistoVisualisationKit(const std::shared_ptr<PixA::IModuleMapH2Factory> &factory, short priority, bool can_visualise_one_fe)
   : ModuleMapVisualisationKitBase(factory, priority, can_visualise_one_fe) {}

  /** Create a visualiser for the given inputs or return NULL.
   */
  PixA::IVisualiser *visualiser(const std::string &histo_name,
				const std::vector<std::string> &/*folder_hierarchy*/) const ;

};



#endif
