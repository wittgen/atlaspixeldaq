// @author Andrew Nelson (c) 2009
#include "ErrorHistoVisualiser.h"
#include <DataContainer/HistoUtil.h>
#include <TCanvas.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TText.h>
#include <Histo/Histo.h>
#include <Visualiser/IScanDataProvider.h>

#include <Visualiser/VisualisationFactory.h>
#include <Visualiser/VisualisationUtil.h>


namespace PixA {

  template <class Histo_t> TH1 *convertToTH1(Histo_t *histo);

  inline TH1 *convertToTH1(const PixLib::Histo *histo) {
    std::unique_ptr<TObject> obj ( transform( histo ) );
    if ( obj.get() && obj->InheritsFrom(TH1::Class())) {
      return static_cast<TH1 *>(obj.release());
    }
    return NULL;
  }

  inline TH1 *convertToTH1(const TH1 *histo) {
    return static_cast<TH1 *>( histo ? histo->Clone() : NULL );
  }


  std::string ErrorHistoVisualiser::s_name[2] = { 
    "Show Module/ROD Histogram",
    "Show FE    /Bad Module Histogram" };

  void ErrorHistoVisualiser::fillIndexArray(IScanDataProvider &data_provider, const std::vector<std::string> &module_names,  HistoInfo_t &info) const {
    info.reset();
    if(module_names.empty()) return; 
    for (std::vector< std::string >::const_iterator module_iter=module_names.begin();
        module_iter != module_names.end();
        module_iter++) {

      HistoInfo_t an_info;
      data_provider.numberOfHistos(*module_iter, m_histoName, an_info);
      info.makeUnion(an_info);

    }
    m_info = info;
  }

  void ErrorHistoVisualiser::visualise(IScanDataProvider &data_provider, const std::vector<std::string> &module_names, int option, const Index_t &index, int ) 
  {
    if(module_names.empty()) return;

    PlotCanvas_t plot_canvas=PlotCanvas_t::canvas(1);
    gStyle->SetOptStat(0);
    gStyle->SetOptTitle(0);

    TObject *obj=NULL;
    TH2 *hist = NULL;

    std::cout << "INFO [ErrorHistoVisualiser::visualise] option: " << optionName(option) << std::endl;

    PixCon::EConnItemType type = PixCon::guessConnItemType(module_names.at(0));
    int ipad = 0, counter = 0;
    bool first = 1;
    // types are defined in PixDetectorView/ConnItemBase.h
    switch(type) {

      case PixCon::kModuleItem:
        {
          // divide canvas if looking at more than one plot
          if(module_names.size()>=1) plot_canvas.plot()->Divide(2,4);
          std::vector<TH1*> plot(module_names.size());
          std::vector<TH2*> feplot(module_names.size());
          for(std::vector< std::string >::const_iterator module_iter=module_names.begin();
              module_iter != module_names.end();
              module_iter++, ipad++) {
            obj = convertToTH1( data_provider.histogram(*module_iter,m_histoName,index) );
            hist = static_cast<TH2*>(obj);

            std::string mod_name = *module_iter;
            if(first) {
              // overall plot titles
              plot_canvas.setTitle(m_histoName);
              plot_canvas.setId(data_provider.scanNumber());
              if (module_names.size()>1) {
                std::string::size_type pos = mod_name.find("_M");
                if (pos!=std::string::npos) {
                  mod_name.erase(pos);
                  plot_canvas.setName(mod_name);
                }
              }
              else {
                plot_canvas.setName( *module_iter );
              }
              first = 0; 
            }

            if (hist) {
              hist->SetBit(kCanDelete);
              // plot histograms
              // see RodPixController::getErrorHisto(...)
              if(module_names.size()==1) {
                plot_canvas.plot()->cd();
              } else {
                plot_canvas.plot()->cd(ipad+1);
              }
              if(option==0) {
                plot[counter] = new TH1D("module","module",22,0,22);
                plot[counter]->SetBit(kCanDelete);
                setModuleLabels(plot[counter]);
                int unusedbit=0;
                for(int i=0; i<29; ++i) {
                  // we don't care about the unused bits in the MCC or FE errors
                  // so we ignore i=18,19,20,26,27,28
                  if(i!=2&&i!=18&&i!=19&&i!=20&&i!=26&&i!=27&&i!=28) {
                    plot[counter]->SetBinContent( unusedbit+1,hist->GetBinContent(i+1,1) );
                    ++unusedbit;
                  }
                }
                gPad->SetLogy(1);
		gPad->Modified();
                plot[counter]->Draw();
              } else if(option==1) {
                feplot[counter] = new TH2D("module","module",8,0,8,2,0,2);
                feplot[counter]->SetBit(kCanDelete);
                feplot[counter]->SetMinimum(0); 
                feplot[counter]->GetXaxis()->SetBinLabel(1,""); 
                feplot[counter]->GetYaxis()->SetBinLabel(1,""); 
                feplot[counter]->GetYaxis()->SetTitle("FE with Errors"); 
                for(int i=0; i<8; ++i) feplot[counter]->SetBinContent(i+1, 1, hist->GetBinContent(i+1,2) );
                for(int i=8; i<16; ++i) feplot[counter]->SetBinContent(16-i, 2, hist->GetBinContent(i+1,2) );
                gPad->SetLogy(0);
		gPad->Modified();
                feplot[counter]->Draw("colz");
              } 
              // write histogram title
              TText *a_text=new TText(0.15,0.95,mod_name.c_str());
              a_text->SetTextSize(a_text->GetTextSize()*1.2);
              a_text->SetNDC(kTRUE);
              a_text->SetBit(kCanDelete);
              a_text->Draw();
              counter++; 
            }
            if(plot_canvas.plot()->GetPad(ipad+1)) {
              plot_canvas.plot()->GetPad(ipad+1)->Modified();
            } 
          }
        }
        break;
      case PixCon::kRodItem:
        {
          // divide canvas if looking at more than one plot
          plot_canvas.plot()->Divide(2,2);
          std::vector<TH1*> plot(4);
          obj = convertToTH1( data_provider.histogram(module_names.at(0),m_histoName,index) );
          hist = static_cast<TH2*>(obj);

          std::string mod_name = module_names.at(0);
          // overall plot titles
          plot_canvas.setTitle(m_histoName);
          plot_canvas.setId(data_provider.scanNumber());
          plot_canvas.setName( module_names.at(0) );

          if(hist) {
            hist->SetBit(kCanDelete);
            // plot histograms
            // see RodPixController::getErrorHisto(...)
            for(ipad=0; ipad<4; ++ipad) {
              std::string rodname;
              rodname = module_names.at(0);
              char dspname[32];
              plot_canvas.plot()->cd(ipad+1);
              if(option==0) {
                int unusedbit=0;
                plot[counter] = new TH1D("module","module",26,0,26);
                plot[counter]->SetBit(kCanDelete); 
                setRodLabels(plot[counter]);
                for(int i=0; i<37; ++i) {
                  if(i<10||i>20) {
                    plot[counter]->SetBinContent( unusedbit+1,hist->GetBinContent(i+1,ipad+1) );
                    ++unusedbit;
                  }
                }
                gPad->SetLogy(1);
		gPad->Modified();
                plot[counter]->Draw();
              } else if(option==1) {
                int unusedbit=0;
                rodname.append(" Bad Module");
                plot[counter] = new TH1D("badmodule","badmodule",22,0,22);
                plot[counter]->SetBit(kCanDelete);
                setModuleLabels(plot[counter]);
                for(int i=0; i<29; ++i) {
                  // we don't care about the unused bits in the MCC or FE errors
                  // so we ignore i=18,19,20,26,27,28
                  if(i!=2&&i!=18&&i!=19&&i!=20&&i!=26&&i!=27&&i!=28) {
                    plot[counter]->SetBinContent( unusedbit+1,hist->GetBinContent(i+1,ipad+5) );
                    ++unusedbit;
                  }
                }
                gPad->SetLogy(1);
		gPad->Modified();
                plot[counter]->Draw();
              }
              sprintf(dspname," DSP %d",ipad);
              rodname.append(dspname);
              // write histogram title
              TText *a_text=new TText(0.15,0.95,rodname.c_str());
              a_text->SetTextSize(a_text->GetTextSize()*1.2);
              a_text->SetNDC(kTRUE);
              a_text->SetBit(kCanDelete);
              a_text->Draw();
              counter++;
            }
            if(plot_canvas.plot()->GetPad(ipad+1)) {
              plot_canvas.plot()->GetPad(ipad+1)->Modified();
            }
          }
        }
        break;
      default:
        break;
    }

    plot_canvas.update();
  }

  void ErrorHistoVisualiser::setModuleLabels(TH1 *hist) {
    hist->GetXaxis()->SetBinLabel(1,"Events");
    hist->GetXaxis()->SetBinLabel(2,"Errors");
    hist->GetXaxis()->SetBinLabel(3,"MissingTrlr");
    hist->GetXaxis()->SetBinLabel(4,"BCID");
    hist->GetXaxis()->SetBinLabel(5,"L1ID");
    hist->GetXaxis()->SetBinLabel(6,"TimeOut");
    hist->GetXaxis()->SetBinLabel(7,"Preamble");
    hist->GetXaxis()->SetBinLabel(8,"DataOvflw");
    hist->GetXaxis()->SetBinLabel(9,"HeaderTrlr");
    hist->GetXaxis()->SetBinLabel(10,"TrailerBit");
    hist->GetXaxis()->SetBinLabel(11,"InvRowCol");
    hist->GetXaxis()->SetBinLabel(12,"RawData");
    hist->GetXaxis()->SetBinLabel(13,"FEEoC");
    hist->GetXaxis()->SetBinLabel(14,"FEHamming");
    hist->GetXaxis()->SetBinLabel(15,"FERegPrty");
    hist->GetXaxis()->SetBinLabel(16,"FEHitPrty");
    hist->GetXaxis()->SetBinLabel(17,"FEBitFlip");
    hist->GetXaxis()->SetBinLabel(18,"MCCHitOvfw");
    hist->GetXaxis()->SetBinLabel(19,"MCCEoEOvfw");
    hist->GetXaxis()->SetBinLabel(20,"MCCL1FE");
    hist->GetXaxis()->SetBinLabel(21,"MCCBCID");
    hist->GetXaxis()->SetBinLabel(22,"MCCL1FEGbl");
    hist->GetXaxis()->LabelsOption("v");
    hist->GetXaxis()->SetLabelSize(0.03);
    hist->GetYaxis()->SetTitle("Module");
  }

  void ErrorHistoVisualiser::setRodLabels(TH1 *hist) {
    hist->GetXaxis()->SetBinLabel(1,"B0F");
    hist->GetXaxis()->SetBinLabel(2,"EventStart");
    hist->GetXaxis()->SetBinLabel(3,"E0F");
    hist->GetXaxis()->SetBinLabel(4,"Errors");
    hist->GetXaxis()->SetBinLabel(5,"BCID");
    hist->GetXaxis()->SetBinLabel(6,"L1ID");
    hist->GetXaxis()->SetBinLabel(7,"TimeOut");
    hist->GetXaxis()->SetBinLabel(8,"DataIncrrct");
    hist->GetXaxis()->SetBinLabel(9,"BufferOvflw");
    hist->GetXaxis()->SetBinLabel(10,"HeaderTrlr");
    hist->GetXaxis()->SetBinLabel(11,"DataOvflw");
    hist->GetXaxis()->SetBinLabel(12,"Header");
    hist->GetXaxis()->SetBinLabel(13,"SyncBit");
    hist->GetXaxis()->SetBinLabel(14,"InvRowCol");
    hist->GetXaxis()->SetBinLabel(15,"MCC Skip");
    hist->GetXaxis()->SetBinLabel(16,"FEEoC");
    hist->GetXaxis()->SetBinLabel(17,"FEHamming");
    hist->GetXaxis()->SetBinLabel(18,"FERegPrty");
    hist->GetXaxis()->SetBinLabel(19,"FEHitPrty");
    hist->GetXaxis()->SetBinLabel(20,"FEBitFlip");
    hist->GetXaxis()->SetBinLabel(21,"MCCHitOvflw");
    hist->GetXaxis()->SetBinLabel(22,"MCCEoEOvflw");
    hist->GetXaxis()->SetBinLabel(23,"MCCL1ChkFail");
    hist->GetXaxis()->SetBinLabel(24,"MCCBCIDChkFail");
    hist->GetXaxis()->SetBinLabel(25,"MCCL1GblChkFail");
    hist->GetXaxis()->SetBinLabel(26,"DataLss");
    hist->GetXaxis()->LabelsOption("v");
    hist->GetXaxis()->SetLabelSize(0.03);
    hist->GetYaxis()->SetTitle("ROD"); 
  }

//  TCanvas *ErrorHistoVisualiser::canvas() {
//    TCanvas *a_canvas = static_cast<TCanvas*>(gROOT->GetListOfCanvases()->FindObject("pixcan"));
//    if (!a_canvas) {
//      a_canvas = new TCanvas("pixcan","Pixel Module Analysis Canvas",10,10,700,850);
//    }
//    else {
//      a_canvas->Clear();
//      a_canvas->Show();
//    }
//    a_canvas->cd();
//    return a_canvas;
//  }

  ErrorHistoVisualisationKit::ErrorHistoVisualisationKit(bool can_be_option) : m_canBeOption(can_be_option) {
  }

  IVisualiser *ErrorHistoVisualisationKit::visualiser(const std::string &histo_name,
      const std::vector<std::string> &/*folder_hierarchy*/) const 
  {
    return new ErrorHistoVisualiser(histo_name);
  }

  ErrorHistoVisualisationKit a_ErrorHistoVisualisationKit_instance(true);

  bool ErrorHistoVisualisationKit::init() {
    VisualisationFactory::registerKit("(^|/)DSP_ERRORS",&a_ErrorHistoVisualisationKit_instance);
    return true;
  }

  bool ErrorHistoVisualisationKit::s_instantiated = ErrorHistoVisualisationKit::init();

}
