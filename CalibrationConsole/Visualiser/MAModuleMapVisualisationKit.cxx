
#include "MAModuleMapVisualisationKit.h"
#include <Visualiser/PixScanModuleMapVisualiser.h>
#include <Visualiser/IScanDataProvider.h>
#include "IModuleMapH2Factory.h"
#include <DataContainer/HistoUtil.h>

#include <TF1.h>
#include <TH2.h>
#include <TStyle.h>
#include <TVirtualPad.h>

#include "PixFe/PixGeometry.h"
#include <DataContainer/GenericHistogramAccess.h>
using namespace PixLib;

class MAModuleMapVisualiser : public PixA::PixScanModuleMapVisualiser
{
public:
  MAModuleMapVisualiser(const std::shared_ptr<PixA::IModuleMapH2Factory> &factory,
			const std::string &histo_name)
    : PixA::PixScanModuleMapVisualiser(histo_name),
      m_moduleMapFactory(factory)
  {}

  /** Get the 2d histogram of the chip / module.
   */
  TH2 *get2DHisto(PixA::IScanDataProvider &data_provider, const std::string &module_name, const PixA::Index_t &index, int front_end) {
    TH2 *h2=PixScanModuleMapVisualiser::get2DHisto(data_provider, module_name, index,front_end);
    if (h2) {
      TH2 *ma_h2=m_moduleMapFactory->createMapH2(h2);
      delete h2;
      h2=ma_h2;
    }
    return h2;
  }

  TH1 *getProjection(PixA::IScanDataProvider &data_provider,const std::string &/* module_name */, TH2 *h2, int option)
  {
    if(!h2) return 0;
    unsigned int mask_stages=1;
    unsigned int total_mask_stages=1;
    PixGeometry geo(PixA::nRows(h2),PixA::nColumns(h2));
    std::string fe_tag;
    if(geo.pixType()==PixGeometry::FEI2_CHIP ||
	   geo.pixType()==PixGeometry::FEI2_MODULE){
      fe_tag = "Base";
    }else if(geo.pixType()==PixGeometry::FEI4_CHIP ||
	     geo.pixType()==PixGeometry::FEI4_MODULE){
      fe_tag = "Base_I4";
    }
    PixA::ConfigHandle scan_config_handle = data_provider.scanConfig(fe_tag);
    if (scan_config_handle) {
      
      PixA::ConfigRef scan_config = scan_config_handle.ref();
      try {
	PixA::ConfGrpRef general_config( scan_config["general"] );
	mask_stages = static_cast<unsigned int>(confVal<int>(general_config["maskStageSteps"]));
	total_mask_stages =
	  static_cast<unsigned int>(MAProjectionVisualiser::getTotalMaskStageSteps(confVal<std::string>(general_config["maskStageTotalSteps"])));
      }
      catch(...) {
      }
    }
    
    PixA::HistoBinning binning(PixA::ModuleMapVisualiser::getBinning(h2,mask_stages,total_mask_stages));

     if (binning.nBins()==1) {
        double width=binning.max()-binning.min();
        if (width<=0) width=1;
        binning=PixA::HistoBinning(3,binning.min()-width,binning.min()+2*width);
      }

    //    std::cout << "INFO [ModuleMapVisualiser::getProjection] binning : " << binning.min() << " -  " << binning.max() << " / " << binning.nBins() << std::endl;
    TH1 *h1=PixA::ModuleMapVisualiser::getProjection(h2,
						     binning,
						     mask_stages,
						     total_mask_stages,
						     option);
    if (h1) {
      h1->SetStats(1);
      gStyle->SetOptFit(1);
      gStyle->SetOptStat(1);
    }
    return h1;
  }

  class TMyF1 : public TF1 
  {
   public:
    TMyF1(const char *name, const char *formula, double xmin, double xmax) : TF1(name, formula, xmin,xmax) { 
     ctorDump();
    }

    TMyF1(const char *name, const char *formula) : TF1(name, formula) { 
     ctorDump();
    }

    void ctorDump() const {
      //std::cout << "INFO [TMyF1::ctor] created function " << GetName() << " : " << static_cast<const void *>(this) << std::endl;
    }
    ~TMyF1() { 
      //std::cout << "INFO [TMyF1::dtor] destroy function " << GetName() << " : " << static_cast<const void *>(this) << std::endl;
    }
  };

  void enhanceProjection(PixA::IScanDataProvider &data_provider, const std::string &module_name, TVirtualPad* plot_pad, TH1 *h1) {
    PixScanModuleMapVisualiser::enhanceProjection(data_provider, module_name, plot_pad,h1);
    if (!h1 || !h1->GetXaxis() || h1->GetXaxis()->GetXmin() >= h1->GetXaxis()->GetXmax()) return;
    
    plot_pad->cd();
    std::string gaus_name("gaus_");
    gaus_name += h1->GetName();
    TF1 *a_gaus=NULL;
    if (h1->GetXaxis()->GetXmin()<=0.) {
      a_gaus=new TMyF1(gaus_name.c_str(),"gaus",h1->GetXaxis()->GetBinLowEdge(2),h1->GetXaxis()->GetXmax());
      a_gaus->SetBit(kCanDelete);
      h1->Fit(a_gaus,"QRN0");
    }
    else {
      a_gaus=new TMyF1(gaus_name.c_str(),"gaus");
      a_gaus->SetRange(h1->GetXaxis()->GetXmin(), h1->GetXaxis()->GetXmax());
      a_gaus->SetBit(kCanDelete);
      h1->Fit(a_gaus,"QN0");
    }
    if (std::isnan(a_gaus->GetXmin()) || std::isnan(a_gaus->GetXmax()) || std::isnan(a_gaus->GetMinimum()) || std::isnan(a_gaus->GetMaximum())) {
       delete a_gaus;
       a_gaus=NULL;
    }
    if (a_gaus) {
       a_gaus->Draw("LSAME");
       h1->GetListOfFunctions()->Add(a_gaus);
    }

  }

  TObject *getPixelGraph(PixA::IScanDataProvider &data_provider, const std::string &/* module_name */, TH2 *h2, int option)
  {
    if(!h2) return 0;
    unsigned int mask_stages=1;
    unsigned int total_mask_stages=1;
    PixGeometry geo(PixA::nRows(h2),PixA::nColumns(h2));
    std::string fe_tag;
    if(geo.pixType()==PixGeometry::FEI2_CHIP ||
	   geo.pixType()==PixGeometry::FEI2_MODULE){
      fe_tag = "Base";
    }else if(geo.pixType()==PixGeometry::FEI4_CHIP ||
	     geo.pixType()==PixGeometry::FEI4_MODULE){
      fe_tag = "Base_I4";
    }
    PixA::ConfigHandle scan_config_handle = data_provider.scanConfig(fe_tag);

    //    PixA::ConfigHandle scan_config_handle = data_provider.scanConfig();
    if (scan_config_handle) {

    PixA::ConfigRef scan_config = scan_config_handle.ref();
    try {
      PixA::ConfGrpRef general_config( scan_config["general"] );
      mask_stages = static_cast<unsigned int>(confVal<int>(general_config["maskStageSteps"]));
      total_mask_stages =
	static_cast<unsigned int>(MAProjectionVisualiser::getTotalMaskStageSteps(confVal<std::string>(general_config["maskStageTotalSteps"])));
    }
    catch (...) {
    }
    }

    return PixA::ModuleMapVisualiser::getPixelGraph(h2,
						    mask_stages,
						    total_mask_stages,
						    option);
  }

private:
  std::shared_ptr<PixA::IModuleMapH2Factory> m_moduleMapFactory;
};


PixA::IVisualiser *MAModuleMapVisualisationKit::visualiser(const std::string &histo_name,
							   const std::vector<std::string> &/*folder_hierarchy*/) const 
{ 
  return new MAModuleMapVisualiser(moduleMapFactory(),histo_name);
}
