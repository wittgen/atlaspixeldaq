
#include "DACMapVisualisationKit.h"
#include <Visualiser/ModuleMapVisualiser.h>
#include <Visualiser/IScanDataProvider.h>
#include "IModuleMapH2Factory.h"
#include <TROOT.h>
#include <TH2.h>
#include <TStyle.h>
#include <TVirtualPad.h>
#include <TF1.h>
#include <DataContainer/HistoUtil.h>

class DACMapVisualiser : public PixA::ModuleMapVisualiser
{
public:
  DACMapVisualiser(const std::shared_ptr<PixA::IModuleMapH2Factory> &factory,
		   const std::string &dac_name)
    : PixA::ModuleMapVisualiser(dac_name),
      m_dacName(dac_name),
      m_moduleMapFactory(factory)
  {
  }

  const std::string &name() const {
    return s_name;
  }

  void fillIndexArray(PixA::IScanDataProvider &, const std::vector<std::string> &, HistoInfo_t &info) const {
    info.reset();
    info.setMinHistoIndex(0);
    info.setMaxHistoIndex(1);
  }

  /** Get the 2d histogram of the chip / module.
   */
  TH2 *get2DHisto(PixA::IScanDataProvider &data_provider, const std::string &module_name, const PixA::Index_t &, int front_end) {
    PixA::ConfigHandle mod_config_handle = data_provider.moduleConfig(module_name);
    if (!mod_config_handle) return nullptr;

    TH2 *h2=nullptr;
    try {
      PixA::ConfigRef mod_config = mod_config_handle.ref();
      h2 = PixA::getDACMap<TH2>(front_end, m_dacName, mod_config);
      if (h2) {
	      gROOT->cd();
	      TH2 *ma_h2=m_moduleMapFactory->createMapH2(h2);
        ma_h2->GetXaxis()->SetTitle("Column");
	      ma_h2->GetYaxis()->SetTitle("Row");
	      ma_h2->SetStats(0);
	      delete h2;
	      h2=ma_h2;
      }
    }
    catch (PixA::ConfigException &err) {
      std::cerr << "ERROR [DACMapVisualiser::get2DHisto] Got exception while retrieving the " << m_dacName << " map for module " << module_name << "." << std::endl
	       << "  Excpetion: " << err.getDescriptor() << std::endl;
    }
    return h2;
  }

  TH1 *getProjection(PixA::IScanDataProvider &/*data_provider*/, const std::string &/*module_name*/, TH2 *h2, int option) 
  {
    constexpr unsigned int mask_stages=1;
    constexpr unsigned int total_mask_stages=1;
    PixA::HistoBinning binning;
    if (m_dacName=="TDAC") {
      binning.setBinning(128,-.5,127.5); // FE-I3: 0...127, FE-I4: 0...31 -> max possible val. is 127
    } else if (m_dacName=="FDAC") {
      binning.setBinning(16,-.5,15.5); // FE-I3: 0...7, FE-I4: 0...15 -> max possible val. is 15
    } else {
      binning = PixA::ModuleMapVisualiser::getBinning(h2,mask_stages,total_mask_stages);
    }
    std::cout << "INFO [ModuleMapVisualiser::getProjection] binning : " << binning.min() << " -  " << binning.max() << " / " << binning.nBins() << std::endl;
    TH1 *h1=PixA::ModuleMapVisualiser::getProjection(h2,
						     binning,
						     mask_stages,
						     total_mask_stages,
						     option);
    if (h1) {
      h1->SetStats(1);
      gStyle->SetOptFit(1);
      gStyle->SetOptStat(1);
    }
    return h1;

  }


  void enhanceProjection(PixA::IScanDataProvider &data_provider, const std::string &module_name, TVirtualPad* plot_pad, TH1* h1) {
    if (h1) {
    PixA::ModuleMapVisualiser::enhanceProjection(data_provider, module_name, plot_pad,h1);
    if (h1->GetXaxis() && h1->GetXaxis()->GetXmin() < h1->GetXaxis()->GetXmax()) {
    plot_pad->cd();
    std::string gaus_name("gaus_");
    gaus_name += h1->GetName();
    TF1 *a_gaus=new TF1(gaus_name.c_str(),"gaus");
    a_gaus->SetRange(h1->GetXaxis()->GetXmin(),h1->GetXaxis()->GetXmax());
    a_gaus->SetBit(kCanDelete);
    h1->Fit(a_gaus,"QN0");
    if (isnan(a_gaus->GetXmin()) || isnan(a_gaus->GetXmax()) || isnan(a_gaus->GetMinimum()) || isnan(a_gaus->GetMaximum())) {
         delete a_gaus;
         a_gaus=NULL;
     }
     if (a_gaus) {
         a_gaus->Draw("LSAME");
         h1->GetListOfFunctions()->Add(a_gaus);
     }
    }
    }
  }


  TObject *getPixelGraph(PixA::IScanDataProvider &/*data_provider*/, const std::string &/*module_name*/, TH2 *h2, int option) 
  {
    constexpr unsigned int mask_stages=1;
    constexpr unsigned int total_mask_stages=1;
    return PixA::ModuleMapVisualiser::getPixelGraph(h2,
						    mask_stages,
						    total_mask_stages,
						    option);
  }

private:
  std::string m_dacName;
  std::shared_ptr<PixA::IModuleMapH2Factory> m_moduleMapFactory;

  static std::string s_name;
};

std::string DACMapVisualiser::s_name = "Show DAC map";


PixA::IVisualiser *DACMapVisualisationKit::visualiser(const std::string &histo_name,
						      const std::vector<std::string> &/* folder_hierarchy */) const 
{ 
  return new DACMapVisualiser(moduleMapFactory(),histo_name);
}
