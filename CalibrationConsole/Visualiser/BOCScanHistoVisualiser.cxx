#include "MAPixScanHistoVisualiser.h"

#include "BOCScanHistoVisualiser.h"
#include <ConfigWrapper/ConnectivityManager.h>
#include <ConfigWrapper/ConnectivityUtil.h>
#include <Visualiser/IScanDataProvider.h>
#include <Visualiser/ScanDataRxLinkInfo_t.h>
#include <TGraph.h>
#include <TVirtualPad.h>
#include <TH2.h>
#include <TMarker.h>
#include <TText.h>

#include "IModuleMapH2Factory.h"

class BOCScanHistoVisualiser : public MAPixScanHistoVisualiser
{
public:

  BOCScanHistoVisualiser(const std::shared_ptr<PixA::IModuleMapH2Factory> &factory, const std::string &histo_name,
			 bool can_visualise_one_fe)
    : MAPixScanHistoVisualiser(factory,histo_name, can_visualise_one_fe)
  {}


  TObject *transform(PixA::IScanDataProvider &data_provider,
		     const std::string &module_name,
		     const PixA::Histo *histo,
		     const PixA::Index_t &index,
		     unsigned int front_end)
  {
    TObject *obj = PixA::PixScanHistoVisualiser::transform(data_provider, module_name, histo, index, front_end);
    if (obj->InheritsFrom(TH2F::Class())) {
      TH2 *ma_h2=m_moduleMapFactory->createBocEditH2(static_cast<TH2 *>(obj), data_provider.scanNumber(), module_name, (index.size()>0 ? index.back() : 0));
      delete obj;
      obj=ma_h2;
    }
    return obj;
  }

  unsigned int nOptions() const {return 3;} 

  const std::string &optionName(unsigned int option) const {
    if (option>=nOptions()) {
      return s_optionName[0];
    }
    return s_optionName[option];
  }


  /** Get the name of the visualiser.
   */
  const std::string &name() const { return s_optionName[0];}

  void visualise(PixA::IScanDataProvider &data_provider, const std::vector<std::string> &module_names, int option, const PixA::Index_t &index, int front_end) {
    if (index.size()==0) return;
    switch (option) {
    case 2: {
      visualiseLoop(data_provider, module_names, index.size()-1, index,front_end);
      break;
    }
    case 1: {
      unsigned int index_i=searchIndex(data_provider, "OB_VISET");
      if (index_i < info().maxLevels() ) {
	if (index_i+1 == info().maxLevels()) {
	  index_i=index.size()-1;
	}
	visualiseLoop(data_provider, module_names, index_i, index,front_end);
	return;
      }
    }
    case 0:
    default:
      MAPixScanHistoVisualiser::visualise(data_provider, module_names, 0, index, front_end);
    }
  }

protected:

  //    /** Could be overloaded to make custom titles.
  //     */
  //    virtual std::string makeTitle(const std::string &histo_title);

  /** Draw chosen BOC settings.
   */
  void enhancePlot(TVirtualPad *plot_pad, PixA::IScanDataProvider &data_provider, const std::string &module_name, const PixA::Index_t &index, TObject *obj );

  std::string makeLoopText( PixA::IScanDataProvider &data_provider, unsigned int index_i, unsigned int elm_i) {
    unsigned int n_levels=0;
    for (; n_levels<3; n_levels++) {
      if (info().maxIndex(n_levels)<=0) break;
    }
    if (index_i< n_levels) {
      return MAPixScanHistoVisualiser::makeLoopText(data_provider, index_i, elm_i);
    }
    else {
      return s_linkName[elm_i%2];
    }
  }


  PixA::ConnectivityRef m_newConn;
private:
  static const std::string s_optionName[3];
  static const std::string s_linkName[2];
};

const std::string BOCScanHistoVisualiser::s_optionName[3]={
  "Show BOC RAW Data Histogram",
  "Show All VI-set",
  "Show Both Links"
};

const std::string BOCScanHistoVisualiser::s_linkName[2]={
  "DTO1",
  "DTO2"
};

void BOCScanHistoVisualiser::enhancePlot(TVirtualPad *plot_pad,
					 PixA::IScanDataProvider &data_provider,
					 const std::string &module_name,
					 const PixA::Index_t &index,
					 TObject *objArg )
{
  if (!plot_pad || !objArg || !objArg->InheritsFrom(TH2::Class()) || index.empty()) return;

  PixA::ConfigHandle scan_config_handle(data_provider.scanConfig());
  if (!scan_config_handle) return;

  PixA::ConfigRef scan_config( scan_config_handle.ref() );
  
  std::string MBs = "";
  try {
    std::string speed = confVal<std::string>(scan_config["mcc"]["mccBandwidth"]);
    if(speed=="SINGLE_40" || speed=="DOUBLE_40") MBs = "_40_";
    else if(speed=="SINGLE_80" || speed=="DOUBLE_80") MBs = "_80_";
  } catch(...) {
  }

  int link;
  PixA::RxLinkInfo_t::ERxLink dto_link;
  if (index.back()==0) {
    dto_link = PixA::RxLinkInfo_t::kDTO1;
  }
  else if (index.back()==1) {
    // DTO2
    dto_link = PixA::RxLinkInfo_t::kDTO2;
  }
  else {
    std::cout << "WARNING [BOCScanHistoVisualiser::enhancePlot] Expected at most histograms with index 0 or 1 for DTO1 and DTO2, but not "
	      << index[3] << "." <<  std::endl;
    return;
  }

  PixA::ScanDataRxLinkInfo_t rx_link_info(data_provider, module_name);
  link=rx_link_info.inOrderBocLinkRx(dto_link);
  if (link<0) {
    std::cout << "WARNING [BOCScanHistoVisualiser::enhancePlot] No valid link information for DTO1."
	      << " DTO1 = " << rx_link_info.inOrderBocLinkRx(PixA::RxLinkInfo_t::kDTO1)
	      << " DTO2 = " << rx_link_info.inOrderBocLinkRx(PixA::RxLinkInfo_t::kDTO2)
	      << std::endl;
    return;
  }
  

  TH2 *h2=static_cast<TH2 *>(objArg);

  const unsigned int n_var=2;
  const char *var_names[n_var]={"BOC_RX_DELAY","BOC_RX_THR"};
  unsigned int axis_var[n_var]={n_var,n_var};
  bool var_used[n_var]={false,false};

  for (unsigned int var_i=0; var_i<n_var; var_i++) {
    if (h2->GetXaxis()) {
      if (strcmp(h2->GetXaxis()->GetName(),var_names[var_i])==0) {
	axis_var[0]=var_i;
	var_used[var_i]=true;
	continue;
      }
    }
    if (h2->GetYaxis()) {
      if (strcmp(h2->GetYaxis()->GetName(),var_names[var_i])==0) {
	axis_var[1]=var_i;
	var_used[var_i]=true;
      }
    }
  }

  // if neither the y nor the x axis is associated to the RX delay or threshold
  // then there nothing to be done.
  if (axis_var[0]==n_var && axis_var[1]==n_var) return;


  // In the following it is therefor assumed that at least one variable is associated to an axis.

  //  const int rx_stream_offset[4] = {0,3,4,7};
  const char rx_plugin_name[4]  = {'A','B','C','D'};

  unsigned int rx_slot    = link / 10;
  assert(rx_slot < 4);
  unsigned int channel = link % 10;
  assert(channel<10);

  std::stringstream config_name;
  config_name << "PixRx" << rx_slot;

  //   std::cout << "INFO [BOCScanHistoVisualiser::enhancePlot] link : RX" << rx_plugin_name[rx_slot] << " channel " << channel << std::endl;

  PixA::ConfigHandle boc_config[2];
  boc_config[0]=data_provider.bocConfig(module_name);

  // // un-comment the following block to display also the most recent BOC settings for the given tags.
  PixA::ConnectivityRef conn( data_provider.connectivity() );
  if (!conn || module_name.empty())return;

     if (!m_newConn) {
       // get a connectivty to get the current BOC config
       std::cout << "INFO [BOCScanHistoVisualiser::enhancePlot] open connectivity to get new BOC config." << std::endl;
       m_newConn = PixA::ConnectivityManager::getConnectivity(conn.tag(PixA::IConnectivity::kId),
                                                              conn.tag(PixA::IConnectivity::kConn),
                                                              conn.tag(PixA::IConnectivity::kAlias),
                                                              conn.tag(PixA::IConnectivity::kPayload),
                                                              conn.tag(PixA::IConnectivity::kConfig),
                                                              conn.tag(PixA::IConnectivity::kModConfig),
                                                              0);
     }

  const PixLib::ModuleConnectivity *module_conn(conn.findModule(module_name));
  const PixLib::RodBocConnectivity *rod_conn( PixA::rodBoc(  module_conn) );

  std::string ctrlType = "L12ROD";
    if(rod_conn)ctrlType = rod_conn->rodBocType( );
    else std::cerr << "ERROR [BOCScanHistoVisualiser::enhancePlot] Cannot get RodBocConnectivity " << std::endl;

    if(module_conn){
      std::string pp0Name = module_conn->pp0()->name();
      boc_config[1]=m_newConn.bocConf( pp0Name );
    }

  const int fill_color[2]={4,2};
  const int line_style[2]={1,2};
  const int marker_style[2]={20,25};

  for (unsigned int version_i=0; version_i<2; version_i++) {
    if (!boc_config[version_i]) continue;

  try {
    const PixA::ConfigRef rx_config(boc_config[version_i].ref().subConfig(config_name.str()));

    float values[2];
    // assert( /*channel+2>=2 && */ channel+2 <= 9);

    if(ctrlType =="CPPROD"){

        std::cout<<module_name<<" DTO"<<(int)(dto_link+1) <<" "<<config_name.str()<<" channel "<<channel+2<<" "<<(int)confVal<unsigned int>(rx_config["General"]["Phases_"+std::to_string(channel+2)])<<std::endl;
        if (var_used[0])values[0] = confVal<unsigned int>(rx_config["General"]["Phases_"+std::to_string(channel+2)]);

        if (var_used[1])values[1] = 0.5;//HardCoded

    } else{
      if (var_used[0]) {
        std::stringstream var_name;
        var_name << "DataDelayF" << MBs << channel+2;
        values[0] = confVal<float>(rx_config["General"][var_name.str()]);
      }

      if (var_used[1]) {
        std::stringstream var_name;
        var_name << "RxThresholdF" << MBs << channel+2;
        values[1] = confVal<float>(rx_config["Opt"][var_name.str()]);
      }
    }

    //     std::cout << "INFO [BOCScanHistoVisualiser::enhancePlot] link : RX" << rx_plugin_name[rx_slot] << " channel " << channel 
    // 	      << " threshold = " << values[1]
    // 	      << " delay = " << values[0]
    // 	      << (version_i==0 ? " old " : " now ")
    // 	      << std::endl;
    
    if (axis_var[0]<n_var && axis_var[1]<n_var) {
      TMarker *marker=new TMarker(values[axis_var[0]], values[axis_var[1]],marker_style[version_i]);
      marker->SetMarkerColor(fill_color[version_i]);
      marker->SetMarkerSize(marker->GetMarkerSize()*1.2);
      plot_pad->cd();
      marker->Draw();
    }
    else {
      float arr[2][2];
      unsigned int var_i=0;
      for (unsigned int axis_i=0; axis_i<2; axis_i++) {
	if (axis_var[axis_i]<n_var) {
	  arr[axis_i][0]=values[axis_var[axis_i]];
	  arr[axis_i][1]=values[axis_var[axis_i]];
	  var_i=axis_var[axis_i];
	}
	else {
	  TAxis *axis=(axis_i==0 ? h2->GetXaxis() : h2->GetYaxis());
	  if (axis)  {
	    arr[axis_i][0]=axis->GetXmin();
	    arr[axis_i][1]=axis->GetXmax();
	  }
	  else {
	    arr[axis_i][0]=0;
	    arr[axis_i][1]=0;
	    std::cerr << "ERROR [BOCScanHistoVisualiser::enhancePlot] Did not get range for axis " << axis_i << "." << std::endl;
	  }
	}
      }
      TGraph *a_graph=new TGraph(2,arr[0],arr[1]);
      if (a_graph && var_i<n_var) {
	a_graph->SetName(var_names[var_i]);
	a_graph->SetLineColor(fill_color[version_i]);
	a_graph->SetLineStyle(line_style[version_i]);
	a_graph->SetLineWidth(2);
	a_graph->Draw("L");
      }
    }
  }
  catch (PixA::ConfigException &err) {
    std::cerr << "ERROR [BOCScanHistoVisualiser::enhancePlot] caught : " << err << std::endl;
  }
  }
  // Add the RX name to the histogram title text
  {
    TListIter iter(plot_pad->GetListOfPrimitives());
    TObject *obj;
    while ( (obj =iter.Next()) ) {
      if (obj->InheritsFrom(TText::Class())) {
	TText *a_text=static_cast<TText *>(obj);
	std::string text(a_text->GetTitle());
	if (text.find(module_name)!= std::string::npos || text.find(data_provider.prodName(module_name))!= std::string::npos) {
	  std::stringstream rx_name;
	  rx_name << "RX" << rx_plugin_name[rx_slot] << " : " << channel;
	  text+=" - ";
	  text+=rx_name.str();
	  a_text->SetTitle(text.c_str());
	}
	break;
      }
    }
  }
}

PixA::IVisualiser *BOCScanHistoVisualisationKit::visualiser(const std::string &histo_name,
							    const std::vector<std::string> &/*folder_hierarchy*/) const 
{
  return new BOCScanHistoVisualiser(moduleMapFactory(),histo_name,canVisualiseOneFe());
}


