// Dear emacs, this is -*-c++-*-
#ifndef _PixA_ModuleMapVisualisationKitBase_h_
#define _PixA_ModuleMapVisualisationKitBase_h_

#include <Visualiser/IVisualisationKit.h>
#include <memory>

namespace PixA {

  class IModuleMapH2Factory;

  class ModuleMapVisualisationKitBase : public PixA::IVisualisationKit
  {
  public:
    ModuleMapVisualisationKitBase(const std::shared_ptr<IModuleMapH2Factory> &factory, short priority, bool can_visualise_one_fe)
        : m_moduleMapFactory(factory), m_priority(priority), m_canVisualiseOneFe(can_visualise_one_fe) {}

    unsigned int priority() const {return m_priority;}

  protected:
    bool   canVisualiseOneFe() const { return m_canVisualiseOneFe; }
    const std::shared_ptr<IModuleMapH2Factory> &moduleMapFactory() const { return m_moduleMapFactory;}

  private:
    std::shared_ptr<IModuleMapH2Factory> m_moduleMapFactory;
    short   m_priority;
    bool    m_canVisualiseOneFe;
  };

}
#endif
