#include "MAPixScanHistoVisualiser.h"

#include "InLinkScanHistoVisualiser.h"
#include <ConfigWrapper/ConnectivityUtil.h>
#include <ConfigWrapper/ConnectivityManager.h>
#include <Visualiser/IScanDataProvider.h>
#include <TGraph.h>
#include <TVirtualPad.h>
#include <TH2.h>
#include <TMarker.h>
#include <TText.h>

namespace PixA {
  class IModuleMapH2Factory;
}

class InLinkScanHistoVisualiser : public MAPixScanHistoVisualiser
{
public:

  InLinkScanHistoVisualiser(const std::shared_ptr<PixA::IModuleMapH2Factory> &factory, const std::string &histo_name,
			 bool can_visualise_one_fe)
    : MAPixScanHistoVisualiser(factory,histo_name, can_visualise_one_fe)
  { m_option=0;}


  unsigned int nOptions() const {return 2;} 

  const std::string &optionName(unsigned int option) const {
    if (option>=nOptions()) {
      return s_optionName[0];
    }
    return s_optionName[option];
  }


  /** Get the name of the visualiser.
   */
  const std::string &name() const { return s_optionName[0];}

  void visualise(PixA::IScanDataProvider &data_provider, const std::vector<std::string> &module_names, int option, const PixA::Index_t &index, int front_end) {
    if (index.size()==0) return;
    m_option= option; 
    MAPixScanHistoVisualiser::visualise(data_provider, module_names, 0, index, front_end);
  }

protected:

  //    /** Could be overloaded to make custom titles.
  //     */
  //    virtual std::string makeTitle(const std::string &histo_title);

  /** Draw chosen BOC settings.
   */
  void enhancePlot(TVirtualPad *plot_pad, PixA::IScanDataProvider &data_provider, const std::string &module_name, const PixA::Index_t &index, TObject *obj );

  std::string makeLoopText( PixA::IScanDataProvider &data_provider, unsigned int index_i, unsigned int elm_i) {
    unsigned int n_levels=0;
    for (; n_levels<3; n_levels++) {
      if (info().maxIndex(n_levels)<=0) break;
    }
    if (index_i< n_levels) {
      return MAPixScanHistoVisualiser::makeLoopText(data_provider, index_i, elm_i);
    }
    else {
      return "";
    }
  }


  PixA::ConnectivityRef m_newConn;
private:
  int m_option;
  static const std::string s_optionName[2];
};

const std::string InLinkScanHistoVisualiser::s_optionName[2]={
  "Show Histogram",
  "Show RAW Data Histogram",
};


void InLinkScanHistoVisualiser::enhancePlot(TVirtualPad *plot_pad,
					 PixA::IScanDataProvider &data_provider,
					 const std::string &rod_name,
					 const PixA::Index_t &index,
					 TObject *obj )
{
  if (!plot_pad || !obj || !obj->InheritsFrom(TH2::Class()) || index.size()==0) return;

  if(m_option) return;

  TH2 *h2=static_cast<TH2 *>(obj);

  if (h2->GetYaxis()) {
      h2->GetYaxis()->SetBinLabel(1,"IPin(Base)");
      h2->GetYaxis()->SetBinLabel(3,"IPIN(Probe)");
      h2->GetYaxis()->SetBinLabel(6,"IPIN(AllDefault)");
      h2->GetYaxis()->SetLabelSize(0.025);	 
  }

  PixA::ConnectivityRef conn( data_provider.connectivity() );
  TAxis* Xaxis = (TAxis*) h2->GetXaxis();
  if (Xaxis && conn &&  !rod_name.empty()) {
     char labelName[30];
     for(int i=0;i<32;i++){
       sprintf(labelName,"%2d",i%8);
       Xaxis->SetBinLabel(i+1,labelName);
     }  
     if (!m_newConn) {
       // get a connectivty to get the current BOC config
       std::cout << "INFO [BOCScanHistoVisualiser::enhancePlot] open connectivity to get new BOC config." << std::endl;
       m_newConn = PixA::ConnectivityManager::getConnectivity(conn.tag(PixA::IConnectivity::kId),
                                                              conn.tag(PixA::IConnectivity::kConn),
                                                              conn.tag(PixA::IConnectivity::kAlias),
                                                              conn.tag(PixA::IConnectivity::kPayload),
                                                              conn.tag(PixA::IConnectivity::kConfig),
                                                              conn.tag(PixA::IConnectivity::kModConfig),
                                                              0);
    }

    std::string pp0NameList[4];
    int pp0FirstBin[4]={-1,-1,-1,-1};
    TText* text[4]; 
    int nPP0=0;
    // Loop over pp0s
    const PixA::Pp0List &pp0_list = m_newConn.pp0s(rod_name);
    for (PixA::Pp0List::const_iterator pp0_iter=pp0_list.begin(); pp0_iter!=pp0_list.end(); ++pp0_iter) {
      pp0NameList[nPP0]= PixA::connectivityName(pp0_iter); 
      const PixA::ModuleList &module_list =m_newConn.modules(pp0_iter);
      int first_bin=100;  
      for(PixA::ModuleList::const_iterator module_iter=module_list.begin(); module_iter != module_list.end(); ++module_iter) {
          PixLib::ModuleConnectivity* module_conn = const_cast<PixLib::ModuleConnectivity*>(*module_iter);
          int bocLinkTx=module_conn->bocLinkTx();
          int inLink = (bocLinkTx/10)*8+ bocLinkTx%10;
          const char* modName = module_conn->name().c_str();
          modName+=pp0NameList[nPP0].length()+1;
          sprintf(labelName,"%s %2d",modName,inLink%8);
          Xaxis->SetBinLabel(inLink+1,labelName);        
          if(inLink<first_bin) first_bin=inLink;    
      }
      pp0FirstBin[nPP0]= first_bin;
      nPP0++;
    }
    Xaxis->CenterLabels();
    Xaxis->LabelsOption("v");
    Xaxis->SetLabelSize(0.025);

    //print PP0Name
    for(int idx=0;idx<4;idx++){
      if(pp0FirstBin[idx]==-1)continue;
      text[idx]=new TText(pp0FirstBin[idx],-0.9,pp0NameList[idx].c_str());
      text[idx]->SetTextSize(0.025);
      text[idx]->Draw();
    }
  }
}

PixA::IVisualiser *InLinkScanHistoVisualisationKit::visualiser(const std::string &histo_name,
							    const std::vector<std::string> &/*folder_hierarchy*/) const 
{
  return new InLinkScanHistoVisualiser(moduleMapFactory(),histo_name,canVisualiseOneFe());
}


