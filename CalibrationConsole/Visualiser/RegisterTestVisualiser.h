// @author Andrew Nelson (c) 2009
#ifndef _RegisterTestVisualiser_h_
#define _RegisterTestVisualiser_h_

#include <TH2.h>
#include <Visualiser/ISimpleVisualiser.h>
#include <Visualiser/IVisualisationKit.h>
#include <DataContainer/HistoRef.h>
#include <guessConnItemType.h>

class TCanvas;

namespace PixA {

  class RegisterTestVisualiser : public ISimpleVisualiser {
  public:

    RegisterTestVisualiser(const std::string &histo_name) : m_histoName(histo_name) {}

    unsigned int nOptions() const {return 2;}

    const std::string &optionName(unsigned int option) const {
      if (option>=nOptions()) {
        return s_name[0];
      }
      return s_name[option];
    }

    /** Get the name of the visualiser.
     */
    const std::string &name() const { return s_name[0];}

    /** Returns true, if the visualiser can optionally display only one front-end.
     */
    bool canVisualiseOneFrontEnd() const {return false;}

    /** Returns the maximum number of modules the visualiser can display simultaneously.
     */
    unsigned int canVisualiseNModules() const { return 1; }

    /** Fill the histogram info structure.
     * The structure contains the index ranges for each dimension.
     * @sa HistoInfo_t
     */
    void fillIndexArray(IScanDataProvider &data_provider, const std::vector<std::string> &module_names, HistoInfo_t &info) const;

    /** Perform the chosen visualisation for the given front_end and the given index set.
     * @param option the visualisation option.
     * @param index the index set to specify the histogram in the multi dimensional histogram collection.
     * @param front_end the number of the front_end (0-15) which should be visualised or -1 for all front-ends
     * The front-end parameter will be ignored if canVisualiseOneFrontEnd returns false.
     */
    void visualise(IScanDataProvider &data_provider, const std::vector<std::string> &module_names, int option, const Index_t &index, int front_end);

  protected:

//    TCanvas *canvas();

    static std::string s_name[2];
    std::string m_histoName;

    /** Get the information about the dimensionality of the current histogram array.
     */
    const HistoInfo_t &info() const { return m_info;}

  private: 
    void setFELabels(TH2 *plot);
    void setMCCLabels(TH2 *plot);
    mutable HistoInfo_t m_info; // caches the number of histos. 

  };

  class RegisterTestVisualisationKit : public IVisualisationKit 
  {
  public:
    RegisterTestVisualisationKit(bool can_be_option);

    unsigned int priority() const {return (m_canBeOption ? IVisualisationKit::normalPriority : IVisualisationKit::fallBackOnly);}

    /** Create a visualiser for the given inputs or return NULL.
     */
    IVisualiser *visualiser(const std::string &histo_name, 
			    const std::vector<std::string> &/*folder_hierarchy*/) const;

  private:
    bool m_canBeOption;
    static bool init();
    static bool s_instantiated;
  };

}

#endif
