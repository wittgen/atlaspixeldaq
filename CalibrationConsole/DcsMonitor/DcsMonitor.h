#ifndef _PixCon_DcsMonitor_h_
#define _PixCon_DcsMonitor_h_

#include <IsMonitorReceiver.h>
#include <VarDefList.h>
#include <DcsVariables.h>
#include <ddc/DdcData.hxx>
#include <is/infoT.h>
#include <Lock.h>


// Copy from ddc/DdcData.hxx :
// const-ness of DcsDataArray::getValue is incorrectly defined in ddc/DdcData.hxx
// such that it cannot be compiled.

template<class D> class MyDcsDataArray : public DdcData, public ISNamedInfo
{
public:
	MyDcsDataArray(time_t sec, IPCPartition &p, const std::string& obName) 
		: DdcData(sec), ISNamedInfo(p, obName.c_str()) {}
        const std::vector<D>* 	getValue() const { return(&value); }
	void addValue(D data) 		{ value.push_back(data); }
	void publishGuts(ISostream &ostrm);
	void refreshGuts(ISistream &istrm);
private:
	std::vector<D>	value;
};

template<class D> inline void MyDcsDataArray<D>::publishGuts(ISostream &) 
{ }

template<class D> inline void MyDcsDataArray<D>::refreshGuts(ISistream &istrm) 
{
	updateTime(istrm);
	D * ptr;
	size_t size;
	istrm.get( &ptr, size );
	value.erase(value.begin(), value.end());
	for(size_t i = 0; i < size; i++)
		value.push_back(ptr[i]);
	delete[] ptr;
}
//__ end copy fomr ddc/ 


template<class D> class FirstElementOfDcsDataArray : public MyDcsDataArray<D>
{
public:
  FirstElementOfDcsDataArray(time_t sec, IPCPartition &p, const std::string& obName) 
    : MyDcsDataArray<D>(sec,p, obName) {}
  
  D getValue() const { return( MyDcsDataArray<D>::getValue()->empty()  ? dummy : (*MyDcsDataArray<D>::getValue())[0] ); }
private:
  D dummy;
};


namespace PixCon {

  class ProtectedDdcRequest;
  class CalibrationDataManager;



//   template <class Src_t>
//   class DcsMonitorReceiverBase : public IsMonitorBase
//   {
//   public:
//     DcsMonitorReceiverBase(std::shared_ptr<IsReceptor> receptor,
// 			  EValueConnType conn_type,
// 			  const std::string &is_regex);

//     virtual void valueChanged(ISCallbackInfo *info) = 0;
//   protected:

//     /** Should read values from IS and subscribe.
//      */
//     virtual void init() = 0;
//   };


//   template <class Src_t,class SrcValue_t>
//   class DcsMonitorReceiver : public DcsMonitorReceiverBase<Src_t>
//   {
//   public:
//     DcsMonitorReceiver(std::shared_ptr<IsReceptor> receptor,
// 		      EValueConnType conn_type,
// 		      const std::string &is_regex)
//       : DcsMonitorReceiverBase<Src_t>(receptor, conn_type, is_regex),
//         m_regex(is_regex)
//     { }


//     void valueChanged(ISCallbackInfo *info);
//   protected:

//     /** Read values from IS and subscribe.
//      */
//     void init();

//     /** Reread values from IS.
//      */
//     void update();

//     virtual void setValue(const std::string &is_name, SrcValue_t value) = 0;

//   private:
//     const std::string m_regex;
//   };

  class DdcRequestController;


  class IDdcRequestHelper {
  public:
    virtual ~IDdcRequestHelper() {}

     virtual bool ddcRequest() = 0;
     virtual bool cancelDdcRequest() = 0;
  };

  class DdcRequestHelper : public IDdcRequestHelper
  {
  public:
    friend class DdcRequestController;

     DdcRequestHelper(const std::shared_ptr<ProtectedDdcRequest> &ddc_request,
		      const std::shared_ptr<IsReceptor> &receptor,
		      const std::shared_ptr<CalibrationDataManager> calibration_data,
		      DcsVariables::EVariable dcs_variable);

     ~DdcRequestHelper();

  protected:

     bool ddcRequestForType(EValueConnType conn_type);
     bool cancelDdcRequest();

  private:
     void prepareRequestList(CalibrationDataManager &calibration_data, 
			     EValueConnType conn_type,
			     DcsVariables::EVariable dcs_variable,
			     std::vector<std::string> &request_list);

     std::shared_ptr<ProtectedDdcRequest>    m_ddcRequest;
     std::shared_ptr<IsReceptor> m_receptor;
     std::shared_ptr<CalibrationDataManager> m_calibrationDataManager;
     DcsVariables::EVariable m_variable;

     std::vector<std::string> m_dcsVariables;
  };

  template <class InfoSrc_t, class Src_t, class SrcRef_t, class Dest_t>
  class DcsIsMonitor : public IsMonitorReceiver<InfoSrc_t, Src_t,  SrcRef_t >, public DdcRequestHelper
  {
  public:

    DcsIsMonitor(const std::shared_ptr<ProtectedDdcRequest> &ddc_request,
		 const std::shared_ptr<IsReceptor> &receptor,
		 const std::shared_ptr<CalibrationDataManager> &calibration_data_manager,
		 DcsVariables::EVariable variable, 
		 EValueConnType conn_type,
                 SrcRef_t value_after_deletion,
		 const IVarNameExtractor &extractor,
		 const ISCriteria &criteria,
		 const std::string &is_regex)
      : IsMonitorReceiver<InfoSrc_t, Src_t, SrcRef_t>(receptor,conn_type, value_after_deletion, criteria),
	DdcRequestHelper::DdcRequestHelper(ddc_request, receptor, calibration_data_manager, variable),
	m_varNameExtractor(&extractor),
	m_isRegex(is_regex)
    {  }

    bool ddcRequest() {
      return this->ddcRequestForType(this->valueConnType());
    }

    void registerForCleanup(const std::string &is_name, std::map<std::string, std::set<std::string> > &clean_up_var_list);

  protected:
    void _update();
    void valueChanged(ISCallbackInfo *info);

    void setValue(const std::string &is_name, ::time_t time_stamp, SrcRef_t value);
    IVarNameExtractor const *m_varNameExtractor;
    const std::string m_isRegex;
    Mutex                         m_monitorLock;
  };


  typedef DcsIsMonitor< DcsSingleData<float>, float, float, float > DcsValueIsMonitor;

  typedef DcsIsMonitor< FirstElementOfDcsDataArray<std::string> , std::string, const std::string &, State_t > DcsStateIsMonitor;

  class DcsMonitorController;



  class DcsMonitor : public DcsValueIsMonitor
  {
  public:
     DcsMonitor(const std::shared_ptr<ProtectedDdcRequest> &ddc_request,
		const std::shared_ptr<IsReceptor> &receptor,
		const std::shared_ptr<CalibrationDataManager> &calibration_data_manager,
		EValueConnType conn_type,
		DcsVariables::EVariable variable);

     ~DcsMonitor();

  };

  class DcsStateMonitor : public DcsStateIsMonitor
  {

  public:
     DcsStateMonitor(const std::shared_ptr<ProtectedDdcRequest> &ddc_request,
		     const std::shared_ptr<IsReceptor> &receptor,
		     const std::shared_ptr<CalibrationDataManager> &calibration_data_manager,
		     EValueConnType conn_type,
		     DcsVariables::EVariable variable);

     ~DcsStateMonitor();

  protected:

    void setValue(const std::string &is_name, ::time_t time_stamp, const std::string &value);

  };


  class DdcRequestController {
  public:

    DdcRequestController( const std::shared_ptr<IDdcRequestHelper> &request_helper) 
      : m_requestHelper(request_helper)
    {
      if (m_requestHelper.get()) 
	m_requestHelper->ddcRequest();
    }

    ~DdcRequestController() {
      if (m_requestHelper.get())
	m_requestHelper->cancelDdcRequest();
    }

  private:

    // disallowed:
    DdcRequestController( const DdcRequestController &) {}
    DdcRequestController &operator=(const DdcRequestController &) { return *this; }

    std::shared_ptr<IDdcRequestHelper> m_requestHelper;
  };

}

#endif
