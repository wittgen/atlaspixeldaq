#include "DcsMonitor.h"
#include "DcsMonitorFactory.h"
#include <ConfigWrapper/ConnectivityUtil.h>

#include <is/inforeceiver.h>

#include <CalibrationDataManager.h>
#include <QIsInfoEvent.h>
#include <IsReceptor.h>

#include <IsMonitorReceiver.ixx>


namespace PixCon {


  class DcsVarNameExtractor : public IVarNameExtractor 
  {
  public:
    void extractVar(const std::string &full_is_name, ConnVar_t &conn_var) const
    {
      std::string::size_type conn_start = full_is_name.find(".");
      if (conn_start != std::string::npos) {
	std::string::size_type conn_end = full_is_name.rfind("_");
	if (conn_end != std::string::npos) {
	  conn_var.connName()=std::string(full_is_name,conn_start+1,conn_end-conn_start-1);
	  conn_var.varName()=std::string(full_is_name,conn_end+1,full_is_name.size()-conn_end-1);
	}
      }

    }

  };

  DcsVarNameExtractor s_dcsVarNameExtractor;

  
  DcsMonitor::DcsMonitor(const std::shared_ptr<ProtectedDdcRequest> &ddc_request,
			 const std::shared_ptr<IsReceptor> &receptor,
			 const std::shared_ptr<CalibrationDataManager> &calibration_data,
			 EValueConnType conn_type,
			 DcsVariables::EVariable dcs_variable)
    : DcsValueIsMonitor(ddc_request,
			receptor,
			calibration_data,
			dcs_variable,
			conn_type,
                        -88 /* when the value is removed from IS.*/,
			PixCon::s_dcsVarNameExtractor,
			ISCriteria(PixCon::DcsVariables::makeIsPattern(dcs_variable)),
			PixCon::DcsVariables::makeIsPattern(dcs_variable))
  {
    DEBUG_TRACE( std::cout << "INFO [DcsMonitor::DcsMonitor] subscription pattern = " << PixCon::DcsVariables::makeIsPattern(dcs_variable) << std::endl; )
  }

  DcsMonitor::~DcsMonitor() {}

  DcsStateMonitor::DcsStateMonitor(const std::shared_ptr<ProtectedDdcRequest> &ddc_request,
				   const std::shared_ptr<IsReceptor> &receptor,
				   const std::shared_ptr<CalibrationDataManager> &calibration_data,
				   EValueConnType conn_type,
				   DcsVariables::EVariable dcs_variable)
    : DcsStateIsMonitor(ddc_request,
			receptor,
			calibration_data,
			dcs_variable,
			conn_type,
                        "" /* when value is removed from IS should result in undefined*/,
			PixCon::s_dcsVarNameExtractor,
			ISCriteria(PixCon::DcsVariables::makeIsPattern(dcs_variable)),
			PixCon::DcsVariables::makeIsPattern(dcs_variable))
  {
    //    std::cout << "INFO [DcsStateMonitor::DcsStateMonitor] subscription pattern = " << PixCon::DcsVariables::makeIsPattern(dcs_variable)
    //	      << std::endl;
  }

  DcsStateMonitor::~DcsStateMonitor() {}

  DdcRequestHelper::DdcRequestHelper(const std::shared_ptr<ProtectedDdcRequest> &ddc_request,
				     const std::shared_ptr<IsReceptor> &receptor,
				     const std::shared_ptr<CalibrationDataManager> calibration_data,
				     DcsVariables::EVariable variable) 
    : m_ddcRequest(ddc_request),
      m_receptor(receptor),
      m_calibrationDataManager(calibration_data),
      m_variable(variable)
  {}

  DdcRequestHelper::~DdcRequestHelper() {
    cancelDdcRequest();
  }

  void DdcRequestHelper::prepareRequestList(CalibrationDataManager &calibration_data,
					    EValueConnType conn_type,
					    DcsVariables::EVariable dcs_variable,
					    std::vector<std::string> &request_list)
  {
      request_list.clear();
      PixCon::Lock lock( calibration_data.calibrationDataMutex() );
      PixA::ConnectivityRef conn = calibration_data.connectivity(kCurrent, 0);
      if (!conn) return;
      PixA::CrateList crate_list = conn.crates();
      for (PixA::CrateList::const_iterator crate_iter = crate_list.begin();
	   crate_iter != crate_list.end();
	   ++crate_iter) {

	PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
	PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
	for (PixA::RodList::const_iterator rod_iter = rod_begin;
	     rod_iter != rod_end;
	     ++rod_iter) {

	  try {
	    PixCon::CalibrationData::ConnObjDataList conn_obj_data( calibration_data.calibrationData().getConnObjectData(PixA::connectivityName(rod_iter)));
	    const CalibrationData::EnableState_t &state = conn_obj_data.enableState(kCurrent, 0);
	    if (!state.enabled()) continue;
	  }
	  catch (PixCon::CalibrationData::MissingConnObject &) {
	    continue;
	  }

	  PixA::Pp0List::const_iterator pp0_begin = PixA::pp0Begin(rod_iter);
	  PixA::Pp0List::const_iterator pp0_end = PixA::pp0End(rod_iter);

	  for (PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
	       pp0_iter != pp0_end;
	       ++pp0_iter) {

	    if (conn_type == kModuleValue) {
	    PixA::ModuleList::const_iterator module_begin = PixA::modulesBegin(pp0_iter);
	    PixA::ModuleList::const_iterator module_end = PixA::modulesEnd(pp0_iter);   
	    for (PixA::ModuleList::const_iterator module_iter = module_begin;
		 module_iter != module_end;
		 ++module_iter) {

	      try {
		PixCon::CalibrationData::ConnObjDataList conn_obj_data( calibration_data.calibrationData().getConnObjectData(PixA::connectivityName(module_iter)));
		const CalibrationData::EnableState_t &state = conn_obj_data.enableState(kCurrent, 0);
		if (!state.enabled()) continue;
	      }
	      catch (PixCon::CalibrationData::MissingConnObject &) {
		continue;
	      }

	      //	      if (m_dcsVariables.size()>5) continue;
	      std::string a_ddc_request = PixCon::DcsVariables::makeRequestName(PixA::connectivityName(module_iter),dcs_variable);
	      m_dcsVariables.push_back(a_ddc_request); // for cancelling the requests;

	      a_ddc_request += " => " + m_receptor->isServerName();
	      request_list.push_back(a_ddc_request);
	    }
	  }
	  else {
	    // @todo are there variables per PP0 ? Or Rather per OB?
	    std::string a_ddc_request = PixCon::DcsVariables::makeRequestName(PixA::connectivityName(pp0_iter),dcs_variable);
	    m_dcsVariables.push_back(a_ddc_request); // for cancelling the requests;
	    
	    a_ddc_request += " => " + m_receptor->isServerName();
	    request_list.push_back(a_ddc_request);
	  }
	  }
	}
      }
  }

  bool DdcRequestHelper::ddcRequestForType(EValueConnType conn_type)
  {

    if (m_dcsVariables.empty()) {
      bool ret = false;
      std::vector<std::string> ddc_request_list;
      prepareRequestList(*m_calibrationDataManager, conn_type, m_variable, ddc_request_list);
      if (!ddc_request_list.empty()) {
	Lock lock(m_ddcRequest->mutex());
	//      for(std::vector<std::string>::const_iterator var_iter = ddc_request_list.begin();
	//	  var_iter != ddc_request_list.end();
	//	  var_iter++) {
	//	std::cout << "INFO [DcsCommandBase::ddcRequest] request " << *var_iter << "." << std::endl;
	//      }
	try {
	  std::cerr << "INFO [DcsRequestHelper::ddcRequest] Will request " << ddc_request_list.size() << " variables." << std::endl;
	  std::string error;
	  ret= m_ddcRequest->ddcRequest().ddcMoreImportOnChange(ddc_request_list, error);
	  if (!ret) {
	    std::cerr << "ERROR [DcsRequestHelper::ddcRequest] Request failed with : " << error << std::endl;
	  }
	}
	catch (std::exception &err) {
	  std::cerr << "ERROR [DcsRequestHelper::ddcRequest] Ddc import request failed. Caught exception : "  << err.what() << std::endl;
	}
	catch (...) {
	  std::cerr << "ERROR [DcsRequestHelper::ddcRequest] Ddc import request failed. Caught unknown exception."  << std::endl;
	}

      }
    }
    return !m_dcsVariables.empty();
  }

  bool DdcRequestHelper::cancelDdcRequest() {
    bool ret = false;
    if(!m_dcsVariables.empty()) {
      //       for(std::vector<std::string>::const_iterator var_iter = m_dcsVariables.begin();
      // 	  var_iter != m_dcsVariables.end();
      // 	  var_iter++) {
      // 	std::cout << "INFO [DcsCommandBase::cancelDdcRequest] request " << *var_iter << "." << std::endl;
      //       }
      Lock lock(m_ddcRequest->mutex());
      try {
	std::string error;
	std::cerr << "INFO [DcsRequestHelper::ddcRequest] Will cancel requests of " << m_dcsVariables.size() << " variables." << std::endl;
	ret = m_ddcRequest->ddcRequest().ddcCancelImport(m_dcsVariables,error);
	if (!ret) {
	  std::cout << "ERROR [DcsRequestHelper::cancelDdcRequest] Request failed with : " << error << std::endl;
	}
      }
      catch (std::exception &err) {
	std::cerr << "ERROR [DcsRequestHelper::cancelDdcRequest] Failed to cancel ddc import. Caught exception : "  << err.what() << std::endl;
      }
      catch (...) {
	std::cerr << "ERROR [DcsRequestHelper::cancelDdcRequest] Failed to cancel ddc import. Caught unknown exception."  << std::endl;
      }
    }
    if (!ret) {
      // @todo should the list of dcs variables be cleared ? 
      m_dcsVariables.clear();
    }
    return ret;
  }


//   template <class Src_t>
//   DcsMonitorReceiverBase<Src_t>::DcsMonitorReceiverBase(std::shared_ptr<IsReceptor> receptor,
// 						      EValueConnType conn_type,
// 						      const std::string &reg_ex_name)
//     : IsMonitorBase(receptor, conn_type,  reg_ex_name )
//   { }


//   template <class Src_t, class SrcValue_t>
//   void DcsMonitorReceiver<Src_t, SrcValue_t>::init() {
//     // read all values from IS
//     this->update();
//     // ... now subscribe and listen for changes.
//     this->m_receptor->receiver().subscribe(this->m_receptor->isServerName().c_str(), 
// 					   this->criteria(),
// 					   &DcsMonitorReceiver<Src_t,SrcValue_t>::valueChanged, this);
//   }

//   template <class Src_t, class SrcValue_t>
//   void DcsMonitorReceiver<Src_t,SrcValue_t>::update()
//   {
//     IPCPartition partition( this->m_receptor->receiver().partition() );
//     Src_t is_value(0,partition, m_regex);

//     // Get current values
//     {
//     ISInfoIterator info_iter( partition, this->m_receptor->isServerName().c_str(), this->criteria() );

//     while( info_iter() ) {

//       info_iter.value(is_value);
//       std::string var_name = info_iter.name();

//       setValue(var_name, is_value.getValue());

//     }
//     }
//   }
  
//   template <class Src_t, class SrcValue_t>
//   void DcsMonitorReceiver<Src_t, SrcValue_t >::valueChanged(ISCallbackInfo *info) {
//     IPCPartition partition( this->m_receptor->receiver().partition() );
//     Src_t is_value(0,partition, m_regex);

//     info->value(is_value);
//     std::string var_name = info->name();
//     setValue(var_name, is_value.getValue());
//   }


  template <class SrcInfo_t, class Src_t, class SrcRef_t, class Dest_t>
  void DcsIsMonitor<SrcInfo_t, Src_t, SrcRef_t, Dest_t>::setValue(const std::string &is_name, ::time_t time_stamp, SrcRef_t value)
  {
    // should also extract time stamps.
    // and multiple values ?
    Dest_t dest_value = static_cast<Dest_t>(value);
    //    DcsMonitorReceiver<Src_t,SrcValue_t>::m_receptor->setValue(DcsMonitorReceiver<Src_t,SrcValue_t>::m_connType, is_name, *m_varNameExtractor, time_stamp, dest_value);
    this->receptor()->setValue(this->valueConnType(), is_name, *m_varNameExtractor, time_stamp, dest_value);
  }

  template <>
  void DcsIsMonitor<FirstElementOfDcsDataArray<std::string> , std::string, const std::string &, State_t>::setValue(const std::string &, ::time_t, const std::string &)
  { 
    assert(false); // must not be used 
  }


  // need specialisation to create the IS helper object DcsSingleData

  template <class SrcInfo_t, class Src_t, class SrcRef_t, class Dest_t>
  void DcsIsMonitor<SrcInfo_t, Src_t, SrcRef_t, Dest_t>::registerForCleanup(const std::string &is_name, std::map<std::string, std::set<std::string> > &clean_up_var_list) {
    ConnVar_t conn_var(kAllConnTypes  /* unused */);
    m_varNameExtractor->extractVar(is_name,conn_var);
    this->receptor()->registerForCleanup(kCurrent,0,conn_var.connName(), conn_var.varName(), clean_up_var_list);
  }

  template <class SrcInfo_t, class Src_t, class SrcRef_t, class Dest_t>
  void DcsIsMonitor<SrcInfo_t, Src_t, SrcRef_t, Dest_t>::valueChanged(ISCallbackInfo *info) {

    Lock lock(m_monitorLock);

    // strange way to check for multiple calls, replaced by mutex lock
    //Counter counter(this->m_concurrencyCounter); // for debugging/validation

    SrcInfo_t is_value(0,const_cast<IPCPartition &>(this->receptor()->partition()), m_isRegex);
    info->value(is_value);

    this->bufferValue(info->name(), info->time().c_time(), is_value, info->reason() != ISInfo::Deleted );
  }


  // need specialisation to create the IS helper object DcsSingleData
  template <class SrcInfo_t, class Src_t, class SrcRef_t, class Dest_t>
  void DcsIsMonitor<SrcInfo_t, Src_t, SrcRef_t, Dest_t>::_update()
  {

    DEBUG_TRACE( std::cout << "INFO [IsMonitorReceiver<Src_t>::_update] " << this->varName()<< std::endl );

    SrcInfo_t is_value(0,const_cast<IPCPartition &>(this->receptor()->partition()), m_isRegex);
    this->_updateLoop(is_value);
  }




  void DcsStateMonitor::setValue(const std::string &is_name, ::time_t time_stamp, const std::string &value)
  {
    State_t dest_value = DcsVariables::getState(value);
    this->receptor()->setValue(this->valueConnType(), is_name, *m_varNameExtractor, time_stamp, dest_value);
  }




  template class DcsIsMonitor<DcsSingleData<float>, float, float , float>;                                          //DcsValueMonitor;

  template class IsMonitorReceiver<DcsSingleData<float>, float, float >;


  template class DcsIsMonitor< FirstElementOfDcsDataArray<std::string>, std::string, const std::string & , State_t>;                    //DcsStateMonitor;

  template class IsMonitorReceiver< FirstElementOfDcsDataArray<std::string>, std::string, const std::string & >;

}
