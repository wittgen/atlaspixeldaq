#ifndef _PixCon_DcsMonitorFactory_h_
#define _PixCon_DcsMonitorFactory_h_

class DdcRequest;

#include <Lock.h>
#include <memory>

#include <memory>
#include <DcsVariables.h>
#include <ddc/DdcRequest.hxx>

class IPCPartition;

namespace PixCon {

  class IsMonitorBase;
  class CalibrationDataManager;
  class IsReceptor;

  class ProtectedDdcRequest
  {
  public:
    ProtectedDdcRequest(DdcRequest *ddc_request)
      : m_ddcRequest(ddc_request)
    {}

    ~ProtectedDdcRequest();

    DdcRequest &ddcRequest() { return *m_ddcRequest; }
    Mutex &mutex() { return m_ddcRequestMutex; }
  private:

    std::unique_ptr<DdcRequest>   m_ddcRequest;
    Mutex                       m_ddcRequestMutex;
  };

  class DcsMonitorFactory
  {
  public:
    DcsMonitorFactory(const std::shared_ptr<IsReceptor> &receptor,
		      const std::string &reqest_is_server_name );

    ~DcsMonitorFactory() {}

    PixCon::IsMonitorBase *createDcsMonitor( DcsVariables::EVariable variable, const std::shared_ptr<CalibrationDataManager> &manager);

  private:
    //IPCPartition                          *m_partition;
    std::shared_ptr<IsReceptor>          m_isReceptor;
    const std::string                      m_isServerName;

    std::shared_ptr<ProtectedDdcRequest> m_ddcRequest;

  };

}

#endif
