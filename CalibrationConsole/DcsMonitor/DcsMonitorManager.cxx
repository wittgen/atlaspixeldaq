#include "ConfigWrapper/Connectivity.h"
#include "ConfigWrapper/ConnectivityUtil.h"
#include "CalibrationDataManager.h"
#include "qevent.h"
#include <DcsMonitorManager.h>
#include <IsMonitorManager.h>
#include <IsReceptor.h>
#include <qtabwidget.h>
#include <qmessagebox.h>
#include <qstatusbar.h>
#include <qinputdialog.h>


#include <BusyLoop.h>
#include <CategoryList.h>

#include <DefaultColourCode.h>

#ifdef HAVE_DDC
#  include <DcsSubscriptions.h>
#  include <DcsMonitorFactory.h>
#  include <DcsMonitor.h>
//#  include <ddc/DdcRequest.hxx>
#endif

#include <QNewVarEvent.h>
#include <TemporaryCursorChange.h>


#include <DcsVariables.h>


namespace PixCon {

  DcsMonitorManager::DcsMonitorManager( const std::shared_ptr<IsReceptor> &a_receptor,
					const std::shared_ptr<PixCon::IsMonitorManager> &monitor_manager,
					const std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data,
					const std::shared_ptr<PixCon::CategoryList> &categories)
    :
#ifdef HAVE_DDC
      m_monitorManager(monitor_manager),
      m_categories(categories),
      m_receptor(a_receptor),
      m_calibrationData(calibration_data),
      m_dcsMonitorFactory(new DcsMonitorFactory(a_receptor,"RunCtrl"))
#endif
  {

  }

  DcsMonitorManager::~DcsMonitorManager()
  {
    shutdown();
  }

  void DcsMonitorManager::shutdown()
  {
#ifdef HAVE_DDC
    if (!m_activeDcsMonitorList.empty() || !m_dcsMonitorList.empty()) {
      std::unique_ptr<PixA::IBusyLoop> busy_loop(PixA::BusyLoopFactory::busyLoop(" Cancel DDC subscriptions  ..."));
      if (!m_activeDcsMonitorList.empty()) {
	m_activeDcsMonitorList.clear();
      }

      if (!m_dcsMonitorList.empty()) {
	std::cout << "INFO [DcsMonitorManager::shutdown] Stop DCS monitoring." << std::endl;
	for (std::map< DcsVariables::EVariable, std::shared_ptr< IsMonitorBase > >::iterator
	       dcs_monitor_iter = m_dcsMonitorList.begin();
	     dcs_monitor_iter != m_dcsMonitorList.end();
	     ++dcs_monitor_iter) {

	  monitorManager()->removeMonitor( dcs_monitor_iter->second);

	}

	m_dcsMonitorList.clear();
      }
    }
#endif
  }

  void DcsMonitorManager::showDcsSubscription() {
#ifdef HAVE_DDC
    std::vector<int> subscriptions;
    for(std::map< DcsVariables::EVariable, PixCon::DdcRequestHandle >::const_iterator 
	  dcs_monitor_iter = m_activeDcsMonitorList.begin();
	dcs_monitor_iter != m_activeDcsMonitorList.end();
	dcs_monitor_iter++) {
      subscriptions.push_back( static_cast<int>(dcs_monitor_iter->first) );
    }
  //  if (!m_dcsSubscriptions) {
      m_dcsSubscriptions= std::shared_ptr<DcsSubscriptions>(new DcsSubscriptions (subscriptions, NULL));
      connect(m_dcsSubscriptions.get(),SIGNAL(changeSubscription(int,bool)),
	      this, SLOT( changeSubscription(int,bool)));
      connect(m_dcsSubscriptions.get(),SIGNAL(closed()),this, SLOT( closeSubscribtionDialog()));
      m_dcsSubscriptions->show(); 
   // }

    //    if (dcs_subscription.exec() == QDialog::Accepted)
#endif
  }


  void DcsMonitorManager::closeSubscribtionDialog() {
#ifdef HAVE_DDC
    m_dcsSubscriptions.reset();
#endif
  }


  void DcsMonitorManager::changeSubscription(int
#ifdef HAVE_DDC
					 dcs_variable
#endif
					 , bool
#ifdef HAVE_DDC
					 subscribe
#endif
) {
#ifdef HAVE_DDC
    DcsVariables::EVariable a_dcs_var=static_cast<DcsVariables::EVariable>(dcs_variable);
    if (a_dcs_var < DcsVariables::NDcsVariables) {
      TemporaryCursorChange busy;
      std::map< DcsVariables::EVariable, DdcRequestHandle >::iterator
	active_monitor_iter = m_activeDcsMonitorList.find(a_dcs_var);

      if (subscribe) {
	if (active_monitor_iter == m_activeDcsMonitorList.end() || !active_monitor_iter->second.get()) {

	  std::map< DcsVariables::EVariable, std::shared_ptr< PixCon::IsMonitorBase > >::iterator
	    monitor_iter = m_dcsMonitorList.find(a_dcs_var);

	  if (monitor_iter == m_dcsMonitorList.end()) {
	    PixCon::CategoryList::Category dcs_category = categories()->addCategory("Dcs");

	    std::vector<std::string>::const_iterator var_iter = std::find(dcs_category.begin(),
									  dcs_category.end(),
									  DcsVariables::varName(a_dcs_var));
	    if (var_iter == dcs_category.end()) {
	      if (a_dcs_var == DcsVariables::FSM_STATE) {

		try {
		  const VarDef_t var_def( VarDefList::instance()->getVarDef( DcsVariables::varName(a_dcs_var) ));
		  if (!var_def.isStateWithOrWithoutHistory() ) {
		    std::cerr << "ERROR [ScanStatusMonitor::ctor] variable " << DcsVariables::varName(a_dcs_var) 
			      << " already exists but it is not a state variable." << std::endl;
		  }
		}
		catch (UndefinedCalibrationVariable &) {
		  VarDef_t var_def( VarDefList::instance()->createStateVarDef(DcsVariables::varName(a_dcs_var),
									      kModuleValue, 
									      DcsVariables::kNFsmStates,
									      false,
                                                                              10/*history size*/) );
		  if ( var_def.isStateWithOrWithoutHistory() ) {
		    //	scan_variables.addVariable(histogram_state_name);
		    StateVarDef_t &state_var_def = var_def.stateDef();

		    state_var_def.addState(DcsVariables::kReady,       DefaultColourCode::kSuccess,       "READY");
		    state_var_def.addState(DcsVariables::kLvOn,        DefaultColourCode::kScanning,      "LV_ON");
		    state_var_def.addState(DcsVariables::kUndefined,   DefaultColourCode::kCloseToFailure,"UNDEFINED");
		    state_var_def.addState(DcsVariables::kOff,         2,           "OFF");
		    state_var_def.addState(DcsVariables::kLockedOut,   DefaultColourCode::kOff,           "LOCKED_OUT");
		    state_var_def.addState(DcsVariables::kUnknown,     DefaultColourCode::kUnknown,       "Unknown");

		    receptor()->notify(new QNewVarEvent(kCurrent, 0, "Dcs",  DcsVariables::varName(a_dcs_var) ) );
		  }
		  else {
		    std::cerr << "ERROR [ConsoleViewer::changeSubscription] variable " <<  DcsVariables::varName(a_dcs_var) 
			      << " already exists but it is not a state variable." << std::endl;
		  }
		}

	      }
	      else {
		//	    std::cout << "INFO [ConsoleViewer::changeSubscription] new var = " 
		//                    << PixCon::DcsVariables::varName(a_dcs_var) << std::endl;
		/*VarDef_t a_var_def= */
		VarDefList::instance()->createValueVarDef(PixCon::DcsVariables::varName(a_dcs_var),
										   kModuleValue, 0,0, "",
                                                                                   10 /*history size*/);
		//	    dcs_category.addVariable(PixCon::DcsVariables::varName(a_dcs_var));
	      }
	      receptor()->notify(new QNewVarEvent(kCurrent,0, "Dcs", PixCon::DcsVariables::varName(a_dcs_var)));
	      emit updateCategories();

	    }
	    std::shared_ptr<PixCon::IsMonitorBase>
	      a_dcs_monitor( m_dcsMonitorFactory->createDcsMonitor(a_dcs_var, calibrationData()));

	    std::pair< std::map< DcsVariables::EVariable,
	                         std::shared_ptr< PixCon::IsMonitorBase > >::iterator,
                       bool>
	      ret = m_dcsMonitorList.insert( std::make_pair(a_dcs_var,a_dcs_monitor));

	    if (!ret.second) {
	      std::cerr << "ERROR [ConsoleViewer::changeSubscription] failed to add monitor for "
			<< PixCon::DcsVariables::varName(a_dcs_var) << "." << std::endl;
	      return;
	    }
	    if (ret.first->second.get()) {
	      std::cout << "INFO [changeSubscription] add monitor " << PixCon::DcsVariables::varName(a_dcs_var)
			<< std::endl;
	      monitorManager()->addMonitor( PixCon::DcsVariables::varName(a_dcs_var), ret.first->second);
	    }
	    monitor_iter = ret.first;
	  }

	  std::shared_ptr<PixCon::IDdcRequestHelper> request_helper;
	  if (dynamic_cast<PixCon::IDdcRequestHelper *>(monitor_iter->second.get())) {
	    request_helper=boost::dynamic_pointer_cast<PixCon::IDdcRequestHelper>(monitor_iter->second);
	  }

	  if (!request_helper) {
	    std::cerr << "ERROR [ConsoleViewer::changeSubscription] Monitor for "
		      << PixCon::DcsVariables::varName(a_dcs_var)
		      << " did not provide a ddc request helper."<< std::endl;
	  }

	  m_activeDcsMonitorList[a_dcs_var]
	    = std::shared_ptr<DdcRequestController>( new DdcRequestController(request_helper));

	}
      }
      else if (active_monitor_iter != m_activeDcsMonitorList.end()) {
	  m_activeDcsMonitorList.erase(active_monitor_iter);
      }
    }
#endif
  }

  void DcsMonitorManager::resubscribe() {
#ifdef HAVE_DDC
    if (!m_activeDcsMonitorList.empty() || !m_dcsMonitorList.empty()) {
      std::vector<DcsVariables::EVariable> resubscribe;
      for (std::map< DcsVariables::EVariable, DdcRequestHandle>::const_iterator active_iter =  m_activeDcsMonitorList.begin();
	   active_iter != m_activeDcsMonitorList.end();
	   ++active_iter) {
	resubscribe.push_back(active_iter->first);
      }
      m_activeDcsMonitorList.clear();
      for (std::vector<DcsVariables::EVariable>::const_iterator resubscribe_iter = resubscribe.begin();
	   resubscribe_iter != resubscribe.end();
	   ++resubscribe_iter) {
	changeSubscription(*resubscribe_iter, true);
      }
    }
#endif
  }

}
