#include "DcsSubscriptions.h"
#include <DcsVariables.h>
#include <qcheckbox.h>
#include <qsignalmapper.h>

#include <iostream>

namespace PixCon {

  DcsSubscriptions::DcsSubscriptions(const std::vector< int > &subscriptions, QWidget* parent )
: QDialog(parent)
  {
    setupUi(this);
//     if (false) {
//     QScrollView* view = new QScrollView(m_container);
//     m_container->setMargin(0);
//     //    m_container->setSpacing(0);
//     m_containerLayout->remove(checkBox1);
//     m_containerLayout->remove(checkBox2);
//     m_containerLayout->remove(checkBox3);
//     m_containerLayout->addWidget(view);
//     delete checkBox1;
//     delete checkBox2;
//     delete checkBox3;
    
//     QVBoxLayout *variable_list_layout = new QVBoxLayout( view->viewport(), 11, 6, "VariableList"); 


//     QFrame *a_container = new QFrame( view->viewport(), "container" );
//     a_container->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)7, 0, 0, m_container->sizePolicy().hasHeightForWidth() ) );
//     a_container->setFrameShape( QFrame::StyledPanel );
//     a_container->setFrameShadow( QFrame::Raised );
//     QVBoxLayout *a_containerLayout = new QVBoxLayout( a_container, 11, 6, "a_containerLayout"); 
//     a_containerLayout->addWidget(a_container);


//     for (std::vector<PixCon::DcsVariables::EVariable>::const_iterator module_var_iter = PixCon::DcsVariables::beginModuleVar();
// 	 module_var_iter != PixCon::DcsVariables::endModuleVar();
// 	 module_var_iter++) {

//       QCheckBox *check_box = new QCheckBox( a_container, PixCon::DcsVariables::varName(*module_var_iter));
//       check_box->setText( PixCon::DcsVariables::varName(*module_var_iter) );
//       a_containerLayout->addWidget( check_box );
//       m_checkBoxList.push_back(check_box);

//     }
//     view->setFrameShape(QFrame::GroupBoxPanel);
//     view->setFrameShadow(QFrame::Sunken);
//     view->setVScrollBarMode(QScrollView::Auto);
//     view->setHScrollBarMode(QScrollView::AlwaysOff);
//     view->setLineWidth(1);

//     m_containerLayout->insertWidget(0,view);
//     m_containerLayout->insertSpacing(1,30);
//     m_containerLayout->insertSpacing(0,5);

//     view->updateContents();
//     view->repaintContents(); 
//     view->viewport()->adjustSize();

//     adjustSize();

//     }

//    m_containerLayout->remove(checkBox1);
//    m_containerLayout->remove(checkBox2);
//    m_containerLayout->remove(checkBox3);
    //    m_containerLayout->addWidget(view);
    //    delete checkBox1;
    //    delete checkBox2;
    //    delete checkBox3;
  
    QSignalMapper *signal_map = new QSignalMapper(this);
    connect(signal_map,SIGNAL(mapped(int)),this,SLOT(selectionChanged(int)));
    m_checkBoxList.clear();


    for (std::vector<PixCon::DcsVariables::EVariable>::const_iterator module_var_iter = PixCon::DcsVariables::beginModuleVar();
	 module_var_iter != PixCon::DcsVariables::endModuleVar();
	 ++module_var_iter) {

      QCheckBox *check_box = new QCheckBox( PixCon::DcsVariables::varName(*module_var_iter), m_container);
      check_box->setText( PixCon::DcsVariables::varName(*module_var_iter) );
      vboxLayout1->addWidget( check_box );
      m_checkBoxList.insert(std::make_pair(static_cast<int>(*module_var_iter),check_box));

      signal_map->setMapping(check_box , static_cast<int>(*module_var_iter));
      connect(check_box,SIGNAL(clicked()),signal_map,SLOT(map()));
    }
    for (std::vector<int>::const_iterator subscribed_dcs_var_iter = subscriptions.begin();
	 subscribed_dcs_var_iter != subscriptions.end();
	 ++subscribed_dcs_var_iter) {
      std::map<int,QCheckBox *>::iterator check_box_iter = m_checkBoxList.find( *subscribed_dcs_var_iter );
      if (check_box_iter != m_checkBoxList.end()) {
	check_box_iter->second->setChecked(true);
      }
    }
    m_container->adjustSize();
    adjustSize();

  }

  void DcsSubscriptions::selectionChanged(int dcs_variable) {
    //    std::cout << "INFO [DcsSubscriptions::subscriptionChanged] value = " << dcs_variable  << "." << std::endl;
    std::map<int,QCheckBox *>::const_iterator check_box_iter = m_checkBoxList.find(dcs_variable);
    if (check_box_iter != m_checkBoxList.end()) {
      DcsVariables::EVariable a_dcs_var = static_cast<DcsVariables::EVariable>(check_box_iter->first);
      if (a_dcs_var < DcsVariables::NDcsVariables ) {
	//	std::cout << "INFO [DcsSubscriptions::subscriptionChanged] " << DcsVariables::varName( a_dcs_var)  << " : " << check_box_iter->second->isChecked() << std::endl;
	emit changeSubscription( dcs_variable, check_box_iter->second->isChecked());
      }
    }
  }

  void DcsSubscriptions::clearAllSubscriptions() {
    for(std::map<int,QCheckBox *>::iterator check_box_iter = m_checkBoxList.begin();
	check_box_iter != m_checkBoxList.end();
	++check_box_iter) {
      check_box_iter->second->setChecked(false);
      emit changeSubscription( check_box_iter->first, false);
    }
  }

  void DcsSubscriptions::hide() {
    emit closed();
  }
}
