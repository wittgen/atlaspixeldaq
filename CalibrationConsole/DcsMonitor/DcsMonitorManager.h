// Dear emacs, this is -*-c++-*-x
#ifndef _PixCon_DcsMonitoring_h_
#define _PixCon_DcsMonitoring_h_

#include <map>
#include <vector>
#include <string>
#include <memory>
#include <qobject.h>
#include <QCloseEvent>
#include <memory>

#ifdef HAVE_DDC
#  include <DcsVariables.h>
#endif
class QCloseEvent;

namespace PixCon {
  class IsMonitorManager;
  class IsMonitorBase;

  class IsReceptor;
  class CategoryList;
  class CalibrationDataManager;

#ifdef HAVE_DDC
  class DcsMonitorFactory;
  class DdcRequestController;

  class DcsSubscriptions;
  
  typedef std::shared_ptr<DdcRequestController> DdcRequestHandle;

#endif


  class DcsMonitorManager : public QObject
  {
     Q_OBJECT

  public:
    DcsMonitorManager( const std::shared_ptr<IsReceptor> &receptor,
		       const std::shared_ptr<PixCon::IsMonitorManager> &monitor_manager,
		       const std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data,
		       const std::shared_ptr<PixCon::CategoryList> &categories);

    ~DcsMonitorManager();

    void shutdown();


  private:

    std::shared_ptr<PixCon::IsMonitorManager> monitorManager() { return m_monitorManager; }
    std::shared_ptr<PixCon::IsMonitorManager> m_monitorManager;

    std::shared_ptr<PixCon::CategoryList>     categories() { return m_categories; }
    std::shared_ptr<PixCon::CategoryList>     m_categories;

    std::shared_ptr<IsReceptor>               receptor()   { return m_receptor;}
    std::shared_ptr<IsReceptor>               m_receptor;

    std::shared_ptr<CalibrationDataManager>   calibrationData()   { return m_calibrationData;}
    std::shared_ptr<CalibrationDataManager>   m_calibrationData;

#ifdef HAVE_DDC
    std::unique_ptr< DcsMonitorFactory >        m_dcsMonitorFactory;
    std::map< DcsVariables::EVariable, std::shared_ptr< IsMonitorBase > >        m_dcsMonitorList;
    std::map< DcsVariables::EVariable, DdcRequestHandle> m_activeDcsMonitorList;
    std::shared_ptr<DcsSubscriptions>       m_dcsSubscriptions;
#endif


 signals:
    void updateCategories();

 public slots:

    void showDcsSubscription();

    void changeSubscription(int dcs_variable, bool subscribe);

    void closeSubscribtionDialog();

    void resubscribe();

  };
}

#endif
