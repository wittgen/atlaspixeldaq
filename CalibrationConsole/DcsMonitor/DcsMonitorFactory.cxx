#include "DcsMonitorFactory.h"
#include "DcsMonitor.h"
#include <IsReceptor.h>

#include <is/inforeceiver.h>

namespace PixCon {


  ProtectedDdcRequest::~ProtectedDdcRequest() {}

  // "const cast" arguments for DdcRequest
  ProtectedDdcRequest *createDdcRequest( const IPCPartition &a_partition, std::string reqest_is_server_name) 
  {
    IPCPartition partition(a_partition);
    return new ProtectedDdcRequest(new DdcRequest(partition,reqest_is_server_name));
  }

  DcsMonitorFactory::DcsMonitorFactory(const std::shared_ptr<IsReceptor> &receptor,
				       const std::string &reqest_is_server_name )
    : m_isReceptor(receptor),
      m_ddcRequest(createDdcRequest(receptor->receiver().partition(), reqest_is_server_name) )
  {
  }

  IsMonitorBase *DcsMonitorFactory::createDcsMonitor( DcsVariables::EVariable variable, const std::shared_ptr<CalibrationDataManager> &manager) {
    switch (variable) {
    case DcsVariables::FSM_STATE: {
      std::cout << "INFO [DcsMonitorFactory::createDcsMonitor] for " << DcsVariables::varName(variable) << std::endl;
      return new DcsStateMonitor(m_ddcRequest, m_isReceptor, manager, kModuleValue, variable);
    }
    default: {
      std::cout << "INFO [DcsMonitorFactory::createDcsMonitor] for " << DcsVariables::varName(variable) << std::endl;
      return new DcsMonitor(m_ddcRequest, m_isReceptor, manager, kModuleValue, variable);
    }
    }
  }

}
