#ifndef UI_DCSSUBSCRIPTIONSBASE_H
#define UI_DCSSUBSCRIPTIONSBASE_H

#include <QFrame>
#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QDialog>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QPushButton>
#include <QSpacerItem>
#include <QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_DcsSubscriptionsBase
{
public:
    QVBoxLayout *vboxLayout;
    QFrame *m_container;
    QVBoxLayout *vboxLayout1;
    QSpacerItem *m_varibleSpacer;
    QHBoxLayout *hboxLayout;
    QSpacerItem *spacer1;
    QPushButton *m_clearAllButton;
    QPushButton *m_closeButton;
    QSpacerItem *spacer2;

    void setupUi(QDialog *DcsSubscriptionsBase)
    {
        if (DcsSubscriptionsBase->objectName().isEmpty())
            DcsSubscriptionsBase->setObjectName(QString::fromUtf8("DcsSubscriptionsBase"));
        DcsSubscriptionsBase->resize(372, 289);
        vboxLayout = new QVBoxLayout(DcsSubscriptionsBase);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        m_container = new QFrame(DcsSubscriptionsBase);
        m_container->setObjectName(QString::fromUtf8("m_container"));
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(5), static_cast<QSizePolicy::Policy>(7));
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(m_container->sizePolicy().hasHeightForWidth());
        m_container->setSizePolicy(sizePolicy);
        m_container->setFrameShape(QFrame::StyledPanel);
        m_container->setFrameShadow(QFrame::Raised);
        vboxLayout1 = new QVBoxLayout(m_container);
        vboxLayout1->setSpacing(6);
        vboxLayout1->setContentsMargins(11, 11, 11, 11);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        m_varibleSpacer = new QSpacerItem(20, 71, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout1->addItem(m_varibleSpacer);


        vboxLayout->addWidget(m_container);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        spacer1 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacer1);

        m_clearAllButton = new QPushButton(DcsSubscriptionsBase);
        m_clearAllButton->setObjectName(QString::fromUtf8("m_clearAllButton"));

        hboxLayout->addWidget(m_clearAllButton);

        m_closeButton = new QPushButton(DcsSubscriptionsBase);
        m_closeButton->setObjectName(QString::fromUtf8("m_closeButton"));

        hboxLayout->addWidget(m_closeButton);

        spacer2 = new QSpacerItem(90, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacer2);


        vboxLayout->addLayout(hboxLayout);


        retranslateUi(DcsSubscriptionsBase);
        QObject::connect(m_closeButton, SIGNAL(clicked()), DcsSubscriptionsBase, SLOT(close()));
        QObject::connect(m_clearAllButton, SIGNAL(clicked()), DcsSubscriptionsBase, SLOT(clearAllSubscriptions()));

        QMetaObject::connectSlotsByName(DcsSubscriptionsBase);
    } // setupUi

    void retranslateUi(QDialog *DcsSubscriptionsBase)
    {
        DcsSubscriptionsBase->setWindowTitle(QApplication::translate("DcsSubscriptionsBase", "Dcs Subscriptions", 0));
        m_clearAllButton->setText(QApplication::translate("DcsSubscriptionsBase", "Clear All", 0));
        m_closeButton->setText(QApplication::translate("DcsSubscriptionsBase", "Ok", 0));
    } // retranslateUi

};

namespace Ui {
    class DcsSubscriptionsBase: public Ui_DcsSubscriptionsBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DCSSUBSCRIPTIONSBASE_H
