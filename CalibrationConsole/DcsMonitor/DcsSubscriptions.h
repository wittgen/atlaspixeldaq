#ifndef _PixCon_DcsSubscription_h_
#define _PixCon_DcsSubscription_h_

#include "ui_DcsSubscriptionsBase.h"
#include "QVBoxLayout"
#include <vector>

class QCheckBox; 

namespace PixCon {

  class DcsSubscriptions : public QDialog, Ui_DcsSubscriptionsBase {
    Q_OBJECT
  public:
    DcsSubscriptions( const std::vector< int > &subscriptions, QWidget* parent);
    QVBoxLayout* m_containerLayout;
    //void clearAllSubscriptions();

  private:
    std::map<int,QCheckBox *> m_checkBoxList;

  public slots:
    virtual void selectionChanged(int dcs_variable); void clearAllSubscriptions();
    void hide();
  signals:
    void changeSubscription(int dcs_variable, bool);
    void closed();
    
  };

}

#endif
