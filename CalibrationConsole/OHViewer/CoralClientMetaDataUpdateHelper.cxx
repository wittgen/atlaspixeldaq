#include "CoralClientMetaDataUpdateHelper.h"
#include <CoralDbClient.hh>
#include <CalibrationDataManager.h>

namespace PixCon {

  typedef bool (CAN::PixCoralDbClient::*updateQualityFuncPtr_t)( CAN::SerialNumber_t id, CAN::EQuality value );
  typedef bool (CAN::PixCoralDbClient::*updateCommentFuncPtr_t)( CAN::SerialNumber_t id, std::string value );

  CoralClientMetaDataUpdateHelper::~CoralClientMetaDataUpdateHelper() {}

  void CoralClientMetaDataUpdateHelper::update(EMeasurementType measurement_type,
					       SerialNumber_t serial_number,
					       int new_quality,
					       const std::string &new_comment,
					       unsigned short mask)
  {
    if (mask && serial_number>0 && (measurement_type==kScan || measurement_type==kAnalysis)) {
      try {
      // 	MetaData_t *meta_data_current=NULL;
      // 	if (measurement_type == kScan) {
      // 	  meta_data_current = &(m_calibrationData->scanMetaData(serial_number));
      // 	}
      // 	else if (measurement_type == kAnalysis) {
      // 	  meta_data_current = &(m_calibrationData->analysisMetaData(serial_number));
      // 	}
      // 	if (!meta_data_current) return;

	updateCommentFuncPtr_t updateCommentFuncArr[kNMeasurements]={NULL,
								     &CAN::PixCoralDbClient::updateScanMetadataComment, 
								     &CAN::PixCoralDbClient::updateAnalysisMetadataComment};
	updateQualityFuncPtr_t updateQualityFuncArr[kNMeasurements]={NULL,
								    &CAN::PixCoralDbClient::updateScanMetadataQuality, 
								    &CAN::PixCoralDbClient::updateAnalysisMetadataQuality};

	CAN::PixCoralDbClient meta_data_client(false);

	if (mask & IMetaDataUpdateHelper::kQuality) {
	  (meta_data_client.*(updateQualityFuncArr[measurement_type])) (serial_number, static_cast<CAN::EQuality>(new_quality));
	}

	if (mask &  IMetaDataUpdateHelper::kComment) {
	  (meta_data_client.*((updateCommentFuncArr[measurement_type]))) (serial_number,new_comment);
	}
      }
      catch (std::exception &err) {
	std::cerr << "FATAL [CoralClientMetaDataUpdateHelper::update] Caught exception : " << err.what() << std::endl;
      }
      catch (... ) {
	std::cerr << "FATAL [CoralClientMetaDataUpdateHelper::update] Caught unhnadled exception." << std::endl;
      }
      //       }
	//       catch ( MetaDataMissing &) {
	//       }

    }
  }

}
