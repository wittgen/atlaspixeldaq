#ifndef _PixCon_QNewVarEvent_h_
#define _PixCon_QNewVarEvent_h_

#include <qevent.h>
#include <string>
#include <CalibrationDataTypes.h>

namespace PixCon {

  class NewVar_t {
  public:
    NewVar_t (EMeasurementType measurement, SerialNumber_t serial_number, const std::string &category_name, const std::string &var_name)
      : m_measurementType(measurement), m_serialNumber(serial_number), m_categoryName(category_name), m_varName(var_name)
    { assert( measurement < kNMeasurements); assert( measurement != kCurrent || serial_number ==0);  }

    EMeasurementType measurementType() const { return m_measurementType; }
    SerialNumber_t   serialNumber() const    { return m_serialNumber; }
    const std::string &categoryName() const  { return m_categoryName; }
    const std::string &varName() const       { return m_varName; }

  private:

    EMeasurementType m_measurementType;
    SerialNumber_t   m_serialNumber;
    std::string      m_categoryName;
    std::string      m_varName;
  };

  /** Event to be send if a new variable is added for a particular scan
   */
  class QNewVarEvent : public QEvent
  {
  public:
    QNewVarEvent(EMeasurementType measurement, SerialNumber_t serial_number, const std::string &category_name, const std::string &var_name)
      : QEvent(QEvent::User), m_varData(measurement, serial_number, category_name, var_name)
    {  }

    const NewVar_t &varData() const { return m_varData; }
  private:
    NewVar_t m_varData;

  };

}


#endif
