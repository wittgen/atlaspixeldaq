#ifndef _RootFileHistogramProvider_H_
#define _RootFileHistogramProvider_H_

#include "IHistogramProvider.h"
#include <Lock.h>
#include <vector>
#include <map>
#include <memory>

#include <memory>
#include <TFile.h>

class TDirectory;

namespace PixCon {

  class CalibrationDataManager;

  class Dir_t {
  public:
    Dir_t() : m_dir(NULL) {}
    Dir_t(const Dir_t &a) : m_file(a.m_file), m_dir(a.m_dir) {  }
    Dir_t(const std::pair<TFile *,TDirectory *> &a_file) : m_file(a_file.first), m_dir(a_file.second) {}
    Dir_t(TFile *a_file, TDirectory *the_dir) : m_file(a_file), m_dir(the_dir) {}
    TFile *file()     { return m_file.get();}
    TDirectory *dir() { return m_dir;}
    void setDir(TDirectory *a_dir) { m_dir=a_dir; }
    operator bool() const { return m_file.get() && m_dir; }
  private:
    std::shared_ptr<TFile> m_file;
    TDirectory          *m_dir;
  };

  class RootFileHistogramProvider  : public IHistogramProvider
  {
  public:
    RootFileHistogramProvider(const std::shared_ptr<CalibrationDataManager> &calibration_data);
    ~RootFileHistogramProvider();
 
    /** Load a histogram from the server.
     * @param type the type of the measurement : scan, analysis or current.
     * @parem serial_number the serial number of the scan.
     * @param conn_item_type the connectivity item type of the histogram.
     * @param connectivity_name connectivity name of the module.
     * @param histo_name name of the histogram.
     * @param index vector which contains the indices for the pix scan histo.
     */
    HistoPtr_t loadHistogram(EMeasurementType type,
 			     SerialNumber_t serial_number,
 			     EConnItemType conn_item_type,
 			     const std::string &connectivity_name,
 			     const std::string &histo_name,
 			     const Index_t &index);

    /** Request the names of all histograms assigned to the given serial number and connectivity object type.
     * @param type the type of the measurement : scan, analysis or current.
     * @param serial_number the serial number of the scan.
     * @param conn_item_type the type of the connectivity item for which the histogram list should be requested.
     * @param histogram_name_list vector which will be extended by the histogram names associated to the serial number and object type.
     * Note, it is the responsibility of the caller to clear the given histogram name list before the call if the old content is not desired.
     * This allows to add histograms of different types in subsequent calls.
     * @sa EConnItemType
     */
    void requestHistogramList(EMeasurementType type,
			      SerialNumber_t serial_number,
			      EConnItemType conn_item_type,
			      std::vector< std::string > &histogram_name_list );


    void requestHistogramList(EMeasurementType type,
			      SerialNumber_t serial_number,
			      EConnItemType conn_item_type,
			      std::vector< std::string > &histogram_name_list,
			      const std::string &/*rod_name*/) {
      // not needed
      // just request the histogram name list of all RODS which should be the same anyway.
      requestHistogramList(type, serial_number,conn_item_type, histogram_name_list);
   }


    /** Return the dimensions of the pix scan histo levels.
     * @param type the type of the measurement : scan, analysis or current.
     * @parem serial_number the serial number of the scan.
     * @param conn_item_type the connectivity item type of the histogram.
     * @param connectivity_name connectivity name of the module.
     * @param histo_name name of the histogram.
     * @param index vector which will be filled with the dimensions of the pix scan histo levels.
     */
    void numberOfHistograms(EMeasurementType type,
			    SerialNumber_t serial_number,
			    EConnItemType conn_item_type,
			    const std::string &connectivity_name,
			    const std::string &histo_name,
			    HistoInfo_t &index);

    /** Add a new root file to provide histograms for the given measurement.
     */
    void addFile(EMeasurementType measurement, SerialNumber_t serial_number, const std::string &file_name);

    /** Remove the cached information for the given measurement.
     */
    void clearCache(EMeasurementType measurement, SerialNumber_t serial_number);

    void reset();

  protected:
    Dir_t open(EMeasurementType measurement_type,
	       SerialNumber_t serial_number,
	       EConnItemType conn_item_type,
	       const std::string &connectivity_name,
	       const std::string &histo_name);

    Dir_t open(EMeasurementType measurement, SerialNumber_t serialnumber_t);
    Dir_t open(const std::string &file_name, EMeasurementType measurement_type, SerialNumber_t serial_number);
    void numberOfHistograms(TDirectory *a_directory, HistoInfo_t &histo_info);

  private:

    class MeasurementInfo_t {
    public:
      MeasurementInfo_t()
      {}

      MeasurementInfo_t(const std::string &file_name)
	: m_fileName(file_name)
      {m_histogramList.resize(kNConnItemTypes); }

      const std::string &fileName() const { return m_fileName; }

      std::vector<std::string> &histogramList(EConnItemType conn_type)
      {
	assert(conn_type< kNConnItemTypes);
	if (m_histogramList.size()<=static_cast<unsigned int>(conn_type)) {
	  m_histogramList.resize(kNConnItemTypes);
	}
	return m_histogramList[conn_type];
      }

      const std::vector<std::string> &histogramList(EConnItemType conn_type) const 
      {
	assert(static_cast<unsigned int>(conn_type)<m_histogramList.size());
	return m_histogramList[conn_type];
      }

    private:
      std::string m_fileName;
      std::vector< std::vector<std::string> > m_histogramList;
    };

    Mutex  m_mutex;
    std::map<SerialNumber_t, MeasurementInfo_t > m_fileList[kNMeasurements];

    std::shared_ptr<CalibrationDataManager> m_calibrationData;

    bool                                    m_verbose;
  };

}
#endif
