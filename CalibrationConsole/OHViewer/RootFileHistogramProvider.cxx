#include "RootFileHistogramProvider.h"
#include "HistogramStatus.h"
#include <TKey.h>
#include <TH1.h>
#include <TROOT.h>
#include <ConfigWrapper/Regex_t.h>

#include <iomanip>

#include <CalibrationDataManager.h>
#include <ConfigWrapper/ConnectivityUtil.h>

#include <DataContainer/RootResultFileAccessBase.h>
#include <PixDbGlobalMutex.hh>

namespace PixCon {


  RootFileHistogramProvider::RootFileHistogramProvider(const std::shared_ptr<CalibrationDataManager> &calibration_data)
    : m_calibrationData(calibration_data), m_verbose(false)
  {}

  RootFileHistogramProvider::~RootFileHistogramProvider() {}

  void RootFileHistogramProvider::clearCache(EMeasurementType measurement, SerialNumber_t serial_number) {
    Lock lock(m_mutex);
    std::map<SerialNumber_t, MeasurementInfo_t >::iterator file_iter = m_fileList[measurement].find(serial_number);
    if (file_iter != m_fileList[measurement].end()) {
      m_fileList[measurement].erase(file_iter);
    }
  }

  void RootFileHistogramProvider::numberOfHistograms(EMeasurementType measurement_type,
						     SerialNumber_t serial_number,
						     EConnItemType conn_item_type,
						     const std::string &connectivity_name,
						     const std::string &histo_name,
						     HistoInfo_t &histo_info)
  {
    histo_info.reset();
    Lock lock(m_mutex);
    Lock globalLock(CAN::PixDbGlobalMutex::mutex());
    try {
      Dir_t histogram_dir( open(measurement_type, serial_number, conn_item_type, connectivity_name,histo_name) );

      if (!histogram_dir) return;
      numberOfHistograms(histogram_dir.dir(), histo_info);
    }
    catch (PixA::MissingResultDirectory &) {
    }
  }

  HistoPtr_t RootFileHistogramProvider::loadHistogram(EMeasurementType measurement_type,
						      SerialNumber_t serial_number,
						      EConnItemType conn_item_type,
						      const std::string &connectivity_name,
						      const std::string &histo_name,
						      const Index_t &index)
  {
    Lock lock(m_mutex);
    Lock globalLock(CAN::PixDbGlobalMutex::mutex());
    try {
      Dir_t histogram_dir( open(measurement_type, serial_number, conn_item_type, connectivity_name,histo_name) );
      if (!histogram_dir) return NULL;

      return PixA::RootResultFileAccessBase::histogram(histogram_dir.dir(), index, m_verbose);
    }
    catch (PixA::MissingResultDirectory &) {
    }

    return NULL;
  }


  void RootFileHistogramProvider::requestHistogramList(EMeasurementType type,
						       SerialNumber_t serial_number,
						       EConnItemType conn_item_type,
						       std::vector< std::string > &histogram_name_list ) {
    Lock lock(m_mutex);
    assert( type < kNMeasurements );
    assert( conn_item_type < kNHistogramConnItemTypes);

    std::map<SerialNumber_t, MeasurementInfo_t >::const_iterator measurement_iter = m_fileList[type].find(serial_number);
    if (measurement_iter != m_fileList[type].end()) {
      const std::vector<std::string> &a_histogram_name_list = measurement_iter->second.histogramList(conn_item_type);
      for (std::vector<std::string>::const_iterator histo_name_iter = a_histogram_name_list.begin();
	   histo_name_iter != a_histogram_name_list.end();
	   ++histo_name_iter) {
	histogram_name_list.push_back(*histo_name_iter);
      }
    }
  }

  class Counter
  {
  public:
    Counter() : m_counter(0) {}
    Counter(const Counter &a) : m_counter(a.m_counter) {}

    unsigned int operator++() { return m_counter++;  }
    unsigned int &operator++(int) { return ++m_counter;}

    operator unsigned int() const { return m_counter; }

  private:
    unsigned int m_counter;
  };

  class CountHistograms : public PixA::IHistogramDirectoryVerifier 
  {
  public:
    CountHistograms(std::vector< std::map<std::string, Counter> > &n_histos, bool verbose) 
      : m_nHistos(&n_histos),
	m_verbose(verbose)
    {}

    bool containsHistograms(IHistogramDirectoryVerifier::EConnItemType type,
			    const std::string &conn_name,
			    TDirectory *histo_directory)
    {
      bool contains_histos = PixA::RootResultFileAccessBase::containsHistograms( histo_directory, m_verbose);
      if (contains_histos && type < IHistogramDirectoryVerifier::kNConnItemTypes) {

	(*m_nHistos)[translateConnItemType(type)][conn_name]++;
      }
      return contains_histos;
    }
    static PixCon::EConnItemType translateConnItemType(IHistogramDirectoryVerifier::EConnItemType src_type) { 
      assert( src_type < IHistogramDirectoryVerifier::kNConnItemTypes );
      assert( IHistogramDirectoryVerifier::kNConnItemTypes == 2);
      if (src_type == IHistogramDirectoryVerifier::kModuleItem) {
	return PixCon::kModuleItem;
      }
      else {
	assert (src_type == IHistogramDirectoryVerifier::kRodItem);
	return PixCon::kRodItem;
      }
    }

  private:
    std::vector< std::map<std::string, Counter> > *m_nHistos;
    bool m_verbose;
  };

  inline PixA::RootResultFileAccessBase::ESerialNumberType translate( EMeasurementType type) {
    PixA::RootResultFileAccessBase::ESerialNumberType target_type;
    switch(type) {
    case kAnalysis:
      target_type = PixA::RootResultFileAccessBase::kAnalysisNumber;
      break;
    case kScan:
      target_type = PixA::RootResultFileAccessBase::kScanNumber;
      break;
    case kCurrent:
    default:
      throw PixA::NoResultFile("There are no histograms for \"Current\" in the ROOT file.");
    }
    return target_type;
  }

  void RootFileHistogramProvider::addFile(EMeasurementType measurement_type, SerialNumber_t serial_number, const std::string &file_name) {
    if (file_name.empty()) return;

    assert(measurement_type<kNMeasurements && serial_number>0);
    assert( measurement_type < kNMeasurements );

    std::vector< std::map<std::string, Counter> > n_histos;
    n_histos.resize(kNHistogramConnItemTypes);

    std::vector< std::set< std::string >  > a_histogram_list;
    a_histogram_list.resize(kNHistogramConnItemTypes);
    CountHistograms count_histos(n_histos,m_verbose);

    {
    Lock measurement_lock(m_mutex);
    MeasurementInfo_t &the_measurement=m_fileList[measurement_type][serial_number];
    the_measurement=MeasurementInfo_t(file_name);
    {
    Lock globalLock(CAN::PixDbGlobalMutex::mutex());
    Dir_t measurement_dir( open(the_measurement.fileName(), measurement_type, serial_number) );
    if (!measurement_dir) return;
    
    PixA::RootResultFileAccessBase::histogramList(measurement_dir.dir(), a_histogram_list[kRodItem],a_histogram_list[kModuleItem], &count_histos, m_verbose);

    }
    for (unsigned short type_i=0; type_i < static_cast<unsigned short>(kNHistogramConnItemTypes); type_i++) {
      std::vector<std::string> &the_histogram_list = the_measurement.histogramList(static_cast<EConnItemType>(type_i));

      for (std::set<std::string>::const_iterator histo_iter = a_histogram_list[type_i].begin();
	     histo_iter != a_histogram_list[type_i].end();
	     histo_iter++) {
	the_histogram_list.push_back(*histo_iter);
      }
    }

      VarDef_t histogram_status_var = HistogramStatus::defineVar();

      Lock calibration_data_lock(m_calibrationData->calibrationDataMutex());
      for (unsigned short type_i=0; type_i < static_cast<unsigned short>(kNHistogramConnItemTypes); type_i++) {
	unsigned int n_expected_histos = the_measurement.histogramList( static_cast<EConnItemType>(type_i)).size();
	for(std::map<std::string, Counter>::const_iterator conn_iter = n_histos[type_i].begin();
	    conn_iter != n_histos[type_i].end();
	    conn_iter++) {

	  try {
	    const CalibrationData::ConnObjDataList conn_obj_data ( m_calibrationData->calibrationData().getConnObjectData( conn_iter->first ) );
	    if (conn_obj_data.enableState(measurement_type, serial_number).enabled()) {
	      State_t histogram_number_status = HistogramStatus::state( n_expected_histos, conn_iter->second);
	      m_calibrationData->calibrationData().addValue(kAllConnTypes,
							    conn_iter->first,
							    measurement_type,
							    serial_number,
							    VarDefList::instance()->varName(histogram_status_var.id()),
							    histogram_number_status);
	    }
	  }
	  catch (CalibrationData::MissingValue &) {
	  }
	}
      }
    }
  }

  void RootFileHistogramProvider::numberOfHistograms(TDirectory *a_directory, HistoInfo_t &histo_info)
  {
    PixA::RootResultFileAccessBase::numberOfHistograms(a_directory, histo_info, m_verbose);
  }


  Dir_t RootFileHistogramProvider::open(EMeasurementType measurement_type,
					SerialNumber_t serial_number,
					EConnItemType conn_item_type,
					const std::string &connectivity_name,
					const std::string &histo_name)
  {
    Dir_t invalid_dir;
    if (conn_item_type != kModuleItem && conn_item_type != kRodItem) {
      std::cout << "WARNING [RootFileHistogramProvider::open] Histograms can only exists for modules or RODs."
		<< std::endl;
      return invalid_dir;
    }

    try {


      std::string rod_name = connectivity_name;
      std::string module_name;

      if (conn_item_type==kModuleItem) {
	// get rod name for modules

	PixA::ConnectivityRef conn(m_calibrationData->connectivity(measurement_type,serial_number));
	if (!conn) return invalid_dir;

	const PixLib::RodBocConnectivity *rod_conn = PixA::rodBoc( conn.findModule( connectivity_name ) );
	if (!rod_conn) {
	  std::cout << "ERROR [RootFileHistogramProvider::open] Module " << connectivity_name << " is unknown or is not connected to a ROD in "
		    << makeSerialNumberString(measurement_type, serial_number)
		    << "." << std::endl;
	  return invalid_dir;
	}
	rod_name = PixA::connectivityName( rod_conn );
	module_name = connectivity_name;
      }

      Dir_t measurement_dir( open(measurement_type,serial_number) );

      if (!measurement_dir) return invalid_dir;

      TDirectory *a_dir = PixA::RootResultFileAccessBase::enter(measurement_dir.dir(), rod_name, module_name, histo_name);
      measurement_dir.setDir( a_dir );
      return measurement_dir;

      std::cout << "ERROR [RootFileHistogramProvider::open] No histograms " << histo_name << " for " << connectivity_name << " in "
		<< makeSerialNumberString(measurement_type, serial_number)
		<< "." << std::endl;
    }
    catch (MissingConnectivity &err) {
    }
    return invalid_dir;
  }

  Dir_t RootFileHistogramProvider::open(EMeasurementType measurement_type, SerialNumber_t serial_number) 
  {
    std::map<SerialNumber_t, MeasurementInfo_t >::const_iterator file_iter = m_fileList[measurement_type].find(serial_number);
    if (file_iter == m_fileList[measurement_type].end()) {
      std::cout << "ERROR [RootFileHistogramProvider::open] No file assigned to the measurement "
		<< makeSerialNumberString(measurement_type, serial_number)
		<< std::endl;
      return Dir_t();
    }
    else {
      return open(file_iter->second.fileName(), measurement_type, serial_number);
    }

  }

  Dir_t RootFileHistogramProvider::open(const std::string &file_name, EMeasurementType measurement_type, SerialNumber_t serial_number)
  {

    try {
      Dir_t ret ( PixA::RootResultFileAccessBase::open(file_name, translate(measurement_type), serial_number) );
      return ret;
    }
    catch ( PixA::MissingResultDirectory &) {
    }
    return Dir_t();

  }

  void RootFileHistogramProvider::reset()
  {
    Lock measurement_lock(m_mutex);
    for (unsigned int measurement_i=0; measurement_i<kNMeasurements; ++measurement_i) {
      m_fileList[measurement_i].clear();
    }

  }
}

