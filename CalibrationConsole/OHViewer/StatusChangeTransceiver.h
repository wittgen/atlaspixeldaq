#ifndef _PixCon_StatusChangeTransceiver_h_
#define _PixCon_StatusChangeTransceiver_h_

#include "Lock.h"
#include <memory>
#include <CalibrationDataManager.h>

namespace PixCon {

  class IStatusChangeForwarder {
  public:
    virtual ~IStatusChangeForwarder() {}
    virtual void statusChanged(EMeasurementType measurement_type, SerialNumber_t serial_number) = 0;
    virtual void connectivityChanged() = 0;
  };



  class StatusChangeTransceiver
  {
  public:

    /** The default status change forwarder which does nothing
     */
    class NopForwarder : public IStatusChangeForwarder
    {
    public:
      void statusChanged(EMeasurementType, SerialNumber_t) {}
      void connectivityChanged() {}
    };

    /** Construct a status change transceiver which will use the given forwarder.
     */
    StatusChangeTransceiver(const std::shared_ptr<CalibrationDataManager> &calibration_data, IStatusChangeForwarder &status_change_forwarder);

    /** Construct a status change transceiver with out a forwarder.
     */
    StatusChangeTransceiver(const std::shared_ptr<CalibrationDataManager> &calibration_data);

    ~StatusChangeTransceiver();

    /** Return true if the default do nothing forwarder is installed.
     */
    bool hasDefaultForwarder() const {
      return dynamic_cast<NopForwarder *>(m_statusChangeForwarder.get()) != NULL;
    }

    /** Replace the current forwarder by the given one.
     */
    void setForwarder(IStatusChangeForwarder *new_status_change_forwarder) {
      assert( &new_status_change_forwarder );
      m_statusChangeForwarder = std::unique_ptr<IStatusChangeForwarder>(new_status_change_forwarder);
    }

    void waitForCurrent(bool &abort) {
      waitFor(kCurrent,0,abort);
    }

    /** Wait while  a signal that the status of the given measurement has changed.
     */
    void waitFor(EMeasurementType measurement_type, SerialNumber_t serial_number, bool &abort) {
      assert( measurement_type < kNMeasurements);
      Lock lock(m_mutex[measurement_type]);
      while (!abort && m_calibrationData->status(measurement_type, serial_number) == MetaData_t::kRunning) {
	m_terminated[measurement_type]->wait();
      }
    }

    /** Send out a signal that the status of the given measurement has changed.
     */
    void signal(EMeasurementType measurement_type , SerialNumber_t serial_number ) {
      assert( measurement_type < kNMeasurements);
      {
	Lock lock(m_mutex[measurement_type]);
	m_terminated[measurement_type]->broadcast();
      }
      m_statusChangeForwarder->statusChanged(measurement_type, serial_number);
    }

    /** Send out a signal that the status of the given measurement has changed.
     */
    void abortSignal(EMeasurementType measurement_type) {
      assert( measurement_type < kNMeasurements);
      Lock lock(m_mutex[measurement_type]);
      m_terminated[measurement_type]->broadcast();
    }

    /** Send out a signal that the status of the given measurement has changed.
     */
    void signalConnectivityHasChanged() {
      m_statusChangeForwarder->connectivityChanged();
    }

  private:
    /** Initialise the status change transceiver.
     * Should and only be called by the constructor.
     */
    void init();

    //  protected:
    std::shared_ptr<CalibrationDataManager> m_calibrationData;
    std::unique_ptr<IStatusChangeForwarder>     m_statusChangeForwarder;

    Mutex m_mutex[kNMeasurements];
    std::vector<Condition *> m_terminated;
  };

}
#endif
