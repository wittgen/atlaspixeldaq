#ifndef UI_SEARCHDBCORALFORMBASE_H
#define UI_SEARCHDBCORALFORMBASE_H

#include <QGroupBox>
#include <QDateTimeEdit>
#include <QTreeWidget>
#include <QStackedWidget>
#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QComboBox>
#include <QDialog>
#include <QFrame>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QRadioButton>
#include <QSpacerItem>
#include <QVBoxLayout>
#include <QWidget>

#include <iostream>

QT_BEGIN_NAMESPACE

class Ui_SearchDbCoralFormBase
{
public:
    QVBoxLayout *vboxLayout;
    QLabel *textLabel2;
    QGridLayout *gridLayout;
    QSpacerItem *spacer15_2_2;
    QDateTimeEdit *toDateEdit;
    QLabel *textLabel1_5_2;
    QLabel *textLabel1_3_2_3_2_2;
    QHBoxLayout *hboxLayout;
    QComboBox *searchStatusComboBox;
    QSpacerItem *spacer21;
    QLineEdit *searchCommentLineEdit;
    QDateTimeEdit *fromDateEdit;
    QLabel *textLabel1_3_2_4_4_3_2;
    QLineEdit *searchScanIDLineEdit;
    QLabel *textLabel1_3_2_4_3_2_2;
    QComboBox *searchComboBox;
    QLabel *textLabel1;
    QSpacerItem *spacer44;
    QSpacerItem *spacer44_2;
    QLabel *textLabel1_6;
    QSpacerItem *spacer15_2_2_2;
    QFrame *line1;
    QStackedWidget *widgetStack;
    QWidget *WStackPage;
    QVBoxLayout *vboxLayout1;
    QLabel *textLabel1_4;
    QHBoxLayout *hboxLayout1;
    QVBoxLayout *vboxLayout2;
    QHBoxLayout *hboxLayout2;
    QLabel *textLabel1_3_2;
    QComboBox *searchScanTypeComboBox;
    QLabel *textLabel1_3;
    QComboBox *searchLocationComboBox;
    QLabel *textLabel1_3_2_2;
    QComboBox *searchQualityComboBox;
    QGroupBox *buttonGroup1;
    QHBoxLayout *hboxLayout3;
    QLabel *textLabel1_7;
    QRadioButton *m_hasAnalysisButton;
    QRadioButton *m_hasNoAnalysisButton;
    QSpacerItem *spacer13;
    QLabel *textLabel1_8;
    QLineEdit *searchRodModuleLineEdit;
    QGridLayout *gridLayout1;
    QLineEdit *searchConnTagLineEdit;
    QLineEdit *searchCfgTagLineEdit;
    QLabel *textLabel1_3_2_4;
    QLabel *textLabel1_3_2_5;
    QSpacerItem *spacer16;
    QLabel *textLabel1_2;
    QTreeWidget * scanResultListView;
    QWidget *WStackPage1;
    QVBoxLayout *vboxLayout3;
    QLabel *textLabel1_5;
    QGridLayout *gridLayout2;
    QLineEdit *searchPostProcessingTagLineEdit;
    QLabel *textLabel1_3_2_6;
    QLineEdit *searchAnalysisIDLineEdit;
    QLabel *textLabel1_3_2_4_5_2;
    QLabel *textLabel1_3_2_5_3;
    QLabel *textLabel1_3_2_4_5;
    QLineEdit *searchClassificationNameLineEdit;
    QLabel *textLabel1_3_2_5_3_2;
    QLineEdit *searchPostProcessingNameLineEdit;
    QLineEdit *searchClassificationTagLineEdit;
    QComboBox *searchAnalysisTypeComboBox;
    QLabel *textLabel1_3_2_4_4_2;
    QSpacerItem *spacer15;
    QLabel *textLabel1_2_2;
    QTreeWidget *analysisListView;
    QSpacerItem *spacer16_2;
    QVBoxLayout *vboxLayout4;
    QVBoxLayout *vboxLayout5;
    QFrame *line1_2;
    QHBoxLayout *hboxLayout4;
    QPushButton *clearPushButton;
    QPushButton *cancelPushButton;
    QSpacerItem *spacer1;
    QPushButton *searchPushButton;
    QPushButton *getSelectedPushButton;
    QPushButton *closePushButton;
    QSpacerItem *spacer18;

    void setupUi(QDialog *SearchDbCoralFormBase)
    {

        std::cout<< "SearchDbCoralFormBase being setup!"<<std::endl;
        if (SearchDbCoralFormBase->objectName().isEmpty())
            SearchDbCoralFormBase->setObjectName(QString::fromUtf8("SearchDbCoralFormBase"));
        SearchDbCoralFormBase->resize(934, 775);
        vboxLayout = new QVBoxLayout(SearchDbCoralFormBase);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        textLabel2 = new QLabel(SearchDbCoralFormBase);
        textLabel2->setObjectName(QString::fromUtf8("textLabel2"));
        QFont font;
        font.setPointSize(14);
        font.setBold(true);
        textLabel2->setFont(font);
        textLabel2->setMargin(3);
        textLabel2->setAlignment(Qt::AlignCenter);
        textLabel2->setWordWrap(false);

        vboxLayout->addWidget(textLabel2);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        spacer15_2_2 = new QSpacerItem(80, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(spacer15_2_2, 0, 2, 1, 1);

        toDateEdit = new QDateTimeEdit(SearchDbCoralFormBase);
        toDateEdit->setObjectName(QString::fromUtf8("toDateEdit"));
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(5), static_cast<QSizePolicy::Policy>(5));
        sizePolicy.setHorizontalStretch(2);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(toDateEdit->sizePolicy().hasHeightForWidth());
        toDateEdit->setSizePolicy(sizePolicy);

        gridLayout->addWidget(toDateEdit, 1, 4, 1, 1);

        textLabel1_5_2 = new QLabel(SearchDbCoralFormBase);
        textLabel1_5_2->setObjectName(QString::fromUtf8("textLabel1_5_2"));
        textLabel1_5_2->setWordWrap(false);

        gridLayout->addWidget(textLabel1_5_2, 0, 0, 1, 1);

        textLabel1_3_2_3_2_2 = new QLabel(SearchDbCoralFormBase);
        textLabel1_3_2_3_2_2->setObjectName(QString::fromUtf8("textLabel1_3_2_3_2_2"));
        textLabel1_3_2_3_2_2->setWordWrap(false);

        gridLayout->addWidget(textLabel1_3_2_3_2_2, 0, 6, 1, 1);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        searchStatusComboBox = new QComboBox(SearchDbCoralFormBase);
        searchStatusComboBox->setObjectName(QString::fromUtf8("searchStatusComboBox"));

        hboxLayout->addWidget(searchStatusComboBox);

        spacer21 = new QSpacerItem(120, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacer21);


        gridLayout->addLayout(hboxLayout, 0, 7, 1, 1);

        searchCommentLineEdit = new QLineEdit(SearchDbCoralFormBase);
        searchCommentLineEdit->setObjectName(QString::fromUtf8("searchCommentLineEdit"));

        gridLayout->addWidget(searchCommentLineEdit, 1, 7, 1, 1);

        fromDateEdit = new QDateTimeEdit(SearchDbCoralFormBase);
        fromDateEdit->setObjectName(QString::fromUtf8("fromDateEdit"));
        sizePolicy.setHeightForWidth(fromDateEdit->sizePolicy().hasHeightForWidth());
        fromDateEdit->setSizePolicy(sizePolicy);

        gridLayout->addWidget(fromDateEdit, 0, 4, 1, 1);

        textLabel1_3_2_4_4_3_2 = new QLabel(SearchDbCoralFormBase);
        textLabel1_3_2_4_4_3_2->setObjectName(QString::fromUtf8("textLabel1_3_2_4_4_3_2"));
        textLabel1_3_2_4_4_3_2->setWordWrap(false);

        gridLayout->addWidget(textLabel1_3_2_4_4_3_2, 1, 0, 1, 1);

        searchScanIDLineEdit = new QLineEdit(SearchDbCoralFormBase);
        searchScanIDLineEdit->setObjectName(QString::fromUtf8("searchScanIDLineEdit"));

        gridLayout->addWidget(searchScanIDLineEdit, 1, 1, 1, 1);

        textLabel1_3_2_4_3_2_2 = new QLabel(SearchDbCoralFormBase);
        textLabel1_3_2_4_3_2_2->setObjectName(QString::fromUtf8("textLabel1_3_2_4_3_2_2"));
        textLabel1_3_2_4_3_2_2->setWordWrap(false);

        gridLayout->addWidget(textLabel1_3_2_4_3_2_2, 1, 6, 1, 1);

        searchComboBox = new QComboBox(SearchDbCoralFormBase);
        searchComboBox->setObjectName(QString::fromUtf8("searchComboBox"));
        QFont font1;
        searchComboBox->setFont(font1);

        gridLayout->addWidget(searchComboBox, 0, 1, 1, 1);

        textLabel1 = new QLabel(SearchDbCoralFormBase);
        textLabel1->setObjectName(QString::fromUtf8("textLabel1"));
        QFont font2;
        font2.setPointSize(8);
        textLabel1->setFont(font2);
        textLabel1->setWordWrap(false);

        gridLayout->addWidget(textLabel1, 0, 3, 1, 1);

        spacer44 = new QSpacerItem(80, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(spacer44, 1, 2, 1, 1);

        spacer44_2 = new QSpacerItem(80, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(spacer44_2, 1, 5, 1, 1);

        textLabel1_6 = new QLabel(SearchDbCoralFormBase);
        textLabel1_6->setObjectName(QString::fromUtf8("textLabel1_6"));
        textLabel1_6->setFont(font2);
        textLabel1_6->setWordWrap(false);

        gridLayout->addWidget(textLabel1_6, 1, 3, 1, 1);

        spacer15_2_2_2 = new QSpacerItem(80, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(spacer15_2_2_2, 0, 5, 1, 1);


        vboxLayout->addLayout(gridLayout);

        line1 = new QFrame(SearchDbCoralFormBase);
        line1->setObjectName(QString::fromUtf8("line1"));
        line1->setFrameShape(QFrame::HLine);
        line1->setFrameShadow(QFrame::Sunken);

        vboxLayout->addWidget(line1);

        widgetStack = new QStackedWidget(SearchDbCoralFormBase);
        widgetStack->setObjectName(QString::fromUtf8("widgetStack"));
        QSizePolicy sizePolicy1(static_cast<QSizePolicy::Policy>(5), static_cast<QSizePolicy::Policy>(3));
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(widgetStack->sizePolicy().hasHeightForWidth());
        widgetStack->setSizePolicy(sizePolicy1);
        WStackPage = new QWidget(widgetStack);
        WStackPage->setObjectName(QString::fromUtf8("WStackPage"));
        vboxLayout1 = new QVBoxLayout(WStackPage);
        vboxLayout1->setSpacing(6);
        vboxLayout1->setContentsMargins(11, 11, 11, 11);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        vboxLayout1->setContentsMargins(0, 0, 0, 0);
        textLabel1_4 = new QLabel(WStackPage);
        textLabel1_4->setObjectName(QString::fromUtf8("textLabel1_4"));
        textLabel1_4->setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
        textLabel1_4->setFrameShape(QLabel::NoFrame);
        textLabel1_4->setWordWrap(false);

        vboxLayout1->addWidget(textLabel1_4);

        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setSpacing(6);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        vboxLayout2 = new QVBoxLayout();
        vboxLayout2->setSpacing(6);
        vboxLayout2->setObjectName(QString::fromUtf8("vboxLayout2"));
        hboxLayout2 = new QHBoxLayout();
        hboxLayout2->setSpacing(6);
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        textLabel1_3_2 = new QLabel(WStackPage);
        textLabel1_3_2->setObjectName(QString::fromUtf8("textLabel1_3_2"));
        textLabel1_3_2->setWordWrap(false);

        hboxLayout2->addWidget(textLabel1_3_2);

        searchScanTypeComboBox = new QComboBox(WStackPage);
        searchScanTypeComboBox->setObjectName(QString::fromUtf8("searchScanTypeComboBox"));

        hboxLayout2->addWidget(searchScanTypeComboBox);

        textLabel1_3 = new QLabel(WStackPage);
        textLabel1_3->setObjectName(QString::fromUtf8("textLabel1_3"));
        textLabel1_3->setWordWrap(false);

        hboxLayout2->addWidget(textLabel1_3);

        searchLocationComboBox = new QComboBox(WStackPage);
        searchLocationComboBox->setObjectName(QString::fromUtf8("searchLocationComboBox"));

        hboxLayout2->addWidget(searchLocationComboBox);

        textLabel1_3_2_2 = new QLabel(WStackPage);
        textLabel1_3_2_2->setObjectName(QString::fromUtf8("textLabel1_3_2_2"));
        textLabel1_3_2_2->setWordWrap(false);

        hboxLayout2->addWidget(textLabel1_3_2_2);

        searchQualityComboBox = new QComboBox(WStackPage);
        searchQualityComboBox->setObjectName(QString::fromUtf8("searchQualityComboBox"));

        hboxLayout2->addWidget(searchQualityComboBox);


        vboxLayout2->addLayout(hboxLayout2);

        buttonGroup1 = new QGroupBox("buttonGroup1",WStackPage);
        QVBoxLayout *buttonGroupLayout = new QVBoxLayout;

        buttonGroup1->setStyleSheet("border:0;");
        hboxLayout3 = new QHBoxLayout();
        hboxLayout3->setAlignment(Qt::AlignTop);
        hboxLayout3->setObjectName(QString::fromUtf8("hboxLayout3"));

        textLabel1_7 = new QLabel;
        textLabel1_7->setObjectName(QString::fromUtf8("textLabel1_7"));
        textLabel1_7->setWordWrap(false);
        m_hasAnalysisButton = new QRadioButton;
        m_hasAnalysisButton->setObjectName(QString::fromUtf8("m_hasAnalysisButton"));

        m_hasNoAnalysisButton = new QRadioButton;
        m_hasNoAnalysisButton->setObjectName(QString::fromUtf8("m_hasNoAnalysisButton"));

        hboxLayout3->addWidget(textLabel1_7);
        hboxLayout3->addWidget(m_hasAnalysisButton);
        hboxLayout3->addWidget(m_hasNoAnalysisButton);

        spacer13 = new QSpacerItem(181, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout3->addItem(spacer13);

        textLabel1_8 = new QLabel;
        textLabel1_8->setObjectName(QString::fromUtf8("textLabel1_8"));
        textLabel1_8->setWordWrap(false);

        searchRodModuleLineEdit = new QLineEdit;
        searchRodModuleLineEdit->setObjectName(QString::fromUtf8("searchRodModuleLineEdit"));

        hboxLayout3->addWidget(textLabel1_8);
        hboxLayout3->addWidget(searchRodModuleLineEdit);

        buttonGroupLayout->addLayout(hboxLayout3);
        vboxLayout2->addLayout(buttonGroupLayout);

        hboxLayout1->addLayout(vboxLayout2);

        gridLayout1 = new QGridLayout();
        gridLayout1->setSpacing(6);
        gridLayout1->setObjectName(QString::fromUtf8("gridLayout1"));
        searchConnTagLineEdit = new QLineEdit(WStackPage);
        searchConnTagLineEdit->setObjectName(QString::fromUtf8("searchConnTagLineEdit"));

        gridLayout1->addWidget(searchConnTagLineEdit, 0, 1, 1, 1);

        searchCfgTagLineEdit = new QLineEdit(WStackPage);
        searchCfgTagLineEdit->setObjectName(QString::fromUtf8("searchCfgTagLineEdit"));

        gridLayout1->addWidget(searchCfgTagLineEdit, 1, 1, 1, 1);

        textLabel1_3_2_4 = new QLabel(WStackPage);
        textLabel1_3_2_4->setObjectName(QString::fromUtf8("textLabel1_3_2_4"));
        textLabel1_3_2_4->setWordWrap(false);

        gridLayout1->addWidget(textLabel1_3_2_4, 0, 0, 1, 1);

        textLabel1_3_2_5 = new QLabel(WStackPage);
        textLabel1_3_2_5->setObjectName(QString::fromUtf8("textLabel1_3_2_5"));
        textLabel1_3_2_5->setWordWrap(false);

        gridLayout1->addWidget(textLabel1_3_2_5, 1, 0, 1, 1);

        hboxLayout1->addLayout(gridLayout1);

        vboxLayout1->addLayout(hboxLayout1);

        spacer16 = new QSpacerItem(20, 4, QSizePolicy::Minimum, QSizePolicy::Fixed);

        vboxLayout1->addItem(spacer16);

        textLabel1_2 = new QLabel(WStackPage);
        textLabel1_2->setObjectName(QString::fromUtf8("textLabel1_2"));
        QSizePolicy sizePolicy2(static_cast<QSizePolicy::Policy>(1), static_cast<QSizePolicy::Policy>(5));
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(textLabel1_2->sizePolicy().hasHeightForWidth());
        textLabel1_2->setSizePolicy(sizePolicy2);
        textLabel1_2->setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
        textLabel1_2->setWordWrap(false);

        vboxLayout1->addWidget(textLabel1_2);

        scanResultListView = new QTreeWidget(WStackPage);
        QStringList headerLabels;
        headerLabels.push_back("Scan ID");
        headerLabels.push_back("Scan Name");
        headerLabels.push_back("ID Tag");
        headerLabels.push_back("Conn Tag");
        headerLabels.push_back("Config Tag");
        headerLabels.push_back("Status");
        headerLabels.push_back("Location");
        headerLabels.push_back("Quality");
        headerLabels.push_back("Time");
        headerLabels.push_back("Comment");
        headerLabels.push_back("File");
        scanResultListView->setColumnCount(headerLabels.count());
        scanResultListView->setHeaderLabels(headerLabels);
        scanResultListView->setSortingEnabled(true);

        vboxLayout1->addWidget(scanResultListView);

        widgetStack->addWidget(WStackPage);
        widgetStack->setCurrentIndex(1);
        WStackPage1 = new QWidget(widgetStack);
        WStackPage1->setObjectName(QString::fromUtf8("WStackPage1"));
        vboxLayout3 = new QVBoxLayout(WStackPage1);
        vboxLayout3->setSpacing(6);
        vboxLayout3->setContentsMargins(11, 11, 11, 11);
        vboxLayout3->setObjectName(QString::fromUtf8("vboxLayout3"));
        vboxLayout3->setContentsMargins(0, 0, 0, 0);
        textLabel1_5 = new QLabel(WStackPage1);
        textLabel1_5->setObjectName(QString::fromUtf8("textLabel1_5"));
        textLabel1_5->setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
        textLabel1_5->setWordWrap(false);

        vboxLayout3->addWidget(textLabel1_5);

        gridLayout2 = new QGridLayout();
        gridLayout2->setSpacing(6);
        gridLayout2->setObjectName(QString::fromUtf8("gridLayout2"));
        searchPostProcessingTagLineEdit = new QLineEdit(WStackPage1);
        searchPostProcessingTagLineEdit->setObjectName(QString::fromUtf8("searchPostProcessingTagLineEdit"));

        gridLayout2->addWidget(searchPostProcessingTagLineEdit, 1, 5, 1, 1);

        textLabel1_3_2_6 = new QLabel(WStackPage1);
        textLabel1_3_2_6->setObjectName(QString::fromUtf8("textLabel1_3_2_6"));
        textLabel1_3_2_6->setWordWrap(false);

        gridLayout2->addWidget(textLabel1_3_2_6, 0, 0, 1, 1);

        searchAnalysisIDLineEdit = new QLineEdit(WStackPage1);
        searchAnalysisIDLineEdit->setObjectName(QString::fromUtf8("searchAnalysisIDLineEdit"));

        gridLayout2->addWidget(searchAnalysisIDLineEdit, 1, 1, 1, 1);

        textLabel1_3_2_4_5_2 = new QLabel(WStackPage1);
        textLabel1_3_2_4_5_2->setObjectName(QString::fromUtf8("textLabel1_3_2_4_5_2"));
        textLabel1_3_2_4_5_2->setWordWrap(false);

        gridLayout2->addWidget(textLabel1_3_2_4_5_2, 0, 4, 1, 1);

        textLabel1_3_2_5_3 = new QLabel(WStackPage1);
        textLabel1_3_2_5_3->setObjectName(QString::fromUtf8("textLabel1_3_2_5_3"));
        textLabel1_3_2_5_3->setWordWrap(false);

        gridLayout2->addWidget(textLabel1_3_2_5_3, 1, 2, 1, 1);

        textLabel1_3_2_4_5 = new QLabel(WStackPage1);
        textLabel1_3_2_4_5->setObjectName(QString::fromUtf8("textLabel1_3_2_4_5"));
        textLabel1_3_2_4_5->setWordWrap(false);

        gridLayout2->addWidget(textLabel1_3_2_4_5, 0, 2, 1, 1);

        searchClassificationNameLineEdit = new QLineEdit(WStackPage1);
        searchClassificationNameLineEdit->setObjectName(QString::fromUtf8("searchClassificationNameLineEdit"));

        gridLayout2->addWidget(searchClassificationNameLineEdit, 0, 3, 1, 1);

        textLabel1_3_2_5_3_2 = new QLabel(WStackPage1);
        textLabel1_3_2_5_3_2->setObjectName(QString::fromUtf8("textLabel1_3_2_5_3_2"));
        textLabel1_3_2_5_3_2->setWordWrap(false);

        gridLayout2->addWidget(textLabel1_3_2_5_3_2, 1, 4, 1, 1);

        searchPostProcessingNameLineEdit = new QLineEdit(WStackPage1);
        searchPostProcessingNameLineEdit->setObjectName(QString::fromUtf8("searchPostProcessingNameLineEdit"));

        gridLayout2->addWidget(searchPostProcessingNameLineEdit, 0, 5, 1, 1);

        searchClassificationTagLineEdit = new QLineEdit(WStackPage1);
        searchClassificationTagLineEdit->setObjectName(QString::fromUtf8("searchClassificationTagLineEdit"));

        gridLayout2->addWidget(searchClassificationTagLineEdit, 1, 3, 1, 1);

        searchAnalysisTypeComboBox = new QComboBox(WStackPage1);
        searchAnalysisTypeComboBox->setObjectName(QString::fromUtf8("searchAnalysisTypeComboBox"));

        gridLayout2->addWidget(searchAnalysisTypeComboBox, 0, 1, 1, 1);

        textLabel1_3_2_4_4_2 = new QLabel(WStackPage1);
        textLabel1_3_2_4_4_2->setObjectName(QString::fromUtf8("textLabel1_3_2_4_4_2"));
        textLabel1_3_2_4_4_2->setWordWrap(false);

        gridLayout2->addWidget(textLabel1_3_2_4_4_2, 1, 0, 1, 1);


        vboxLayout3->addLayout(gridLayout2);

        spacer15 = new QSpacerItem(20, 4, QSizePolicy::Minimum, QSizePolicy::Fixed);

        vboxLayout3->addItem(spacer15);

        textLabel1_2_2 = new QLabel(WStackPage1);
        textLabel1_2_2->setObjectName(QString::fromUtf8("textLabel1_2_2"));
        textLabel1_2_2->setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
        textLabel1_2_2->setWordWrap(false);

        vboxLayout3->addWidget(textLabel1_2_2);

        analysisListView = new QTreeWidget(WStackPage1);
        QStringList headerLabels1;
        headerLabels1.push_back("AnalysisID");
        headerLabels1.push_back("Scan ID");
        headerLabels1.push_back("Analysis Type");
        headerLabels1.push_back("Analysis Tag");
        headerLabels1.push_back("Status");
        headerLabels1.push_back("Quality");
        headerLabels1.push_back("Time");
        headerLabels1.push_back("Class. Name");
        headerLabels1.push_back("Class. Tag");
        headerLabels1.push_back("Post Name");
        headerLabels1.push_back("Post Tag");
        headerLabels1.push_back("Comment");
        headerLabels1.push_back("File");
        analysisListView->setColumnCount(headerLabels1.count());
        analysisListView->setHeaderLabels(headerLabels1);
        analysisListView->setSortingEnabled(true);
        vboxLayout3->addWidget(analysisListView);

        widgetStack->addWidget(WStackPage1);
        widgetStack->setCurrentIndex(0);

        vboxLayout->addWidget(widgetStack);

        spacer16_2 = new QSpacerItem(20, 4, QSizePolicy::Minimum, QSizePolicy::Fixed);

        vboxLayout->addItem(spacer16_2);

        vboxLayout4 = new QVBoxLayout();
        vboxLayout4->setSpacing(6);
        vboxLayout4->setObjectName(QString::fromUtf8("vboxLayout4"));
        vboxLayout5 = new QVBoxLayout();
        vboxLayout5->setSpacing(6);
        vboxLayout5->setObjectName(QString::fromUtf8("vboxLayout5"));
        line1_2 = new QFrame(SearchDbCoralFormBase);
        line1_2->setObjectName(QString::fromUtf8("line1_2"));
        line1_2->setFrameShape(QFrame::HLine);
        line1_2->setFrameShadow(QFrame::Sunken);

        vboxLayout5->addWidget(line1_2);

        hboxLayout4 = new QHBoxLayout();
        hboxLayout4->setSpacing(6);
        hboxLayout4->setObjectName(QString::fromUtf8("hboxLayout4"));
        clearPushButton = new QPushButton(SearchDbCoralFormBase);
        clearPushButton->setObjectName(QString::fromUtf8("clearPushButton"));

        hboxLayout4->addWidget(clearPushButton);

        cancelPushButton = new QPushButton(SearchDbCoralFormBase);
        cancelPushButton->setObjectName(QString::fromUtf8("cancelPushButton"));

        hboxLayout4->addWidget(cancelPushButton);

        spacer1 = new QSpacerItem(480, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout4->addItem(spacer1);

        searchPushButton = new QPushButton(SearchDbCoralFormBase);
        searchPushButton->setObjectName(QString::fromUtf8("searchPushButton"));
        searchPushButton->setDefault(true);

        hboxLayout4->addWidget(searchPushButton);

        getSelectedPushButton = new QPushButton(SearchDbCoralFormBase);
        getSelectedPushButton->setObjectName(QString::fromUtf8("getSelectedPushButton"));

        hboxLayout4->addWidget(getSelectedPushButton);

        closePushButton = new QPushButton(SearchDbCoralFormBase);
        closePushButton->setObjectName(QString::fromUtf8("closePushButton"));

        hboxLayout4->addWidget(closePushButton);


        vboxLayout5->addLayout(hboxLayout4);


        vboxLayout4->addLayout(vboxLayout5);

        spacer18 = new QSpacerItem(20, 8, QSizePolicy::Minimum, QSizePolicy::Fixed);

        vboxLayout4->addItem(spacer18);


        vboxLayout->addLayout(vboxLayout4);

        QWidget::setTabOrder(clearPushButton, cancelPushButton);
        QWidget::setTabOrder(cancelPushButton, searchPushButton);
        QWidget::setTabOrder(searchPushButton, getSelectedPushButton);
        QWidget::setTabOrder(getSelectedPushButton, closePushButton);
        QWidget::setTabOrder(closePushButton, searchScanTypeComboBox);
        QWidget::setTabOrder(searchScanTypeComboBox, searchLocationComboBox);
        QWidget::setTabOrder(searchLocationComboBox, searchQualityComboBox);
        QWidget::setTabOrder(searchQualityComboBox, searchCfgTagLineEdit);
        QWidget::setTabOrder(searchCfgTagLineEdit, searchAnalysisTypeComboBox);
        QWidget::setTabOrder(searchAnalysisTypeComboBox, searchAnalysisIDLineEdit);
        QWidget::setTabOrder(searchAnalysisIDLineEdit, searchClassificationNameLineEdit);
        QWidget::setTabOrder(searchClassificationNameLineEdit, searchClassificationTagLineEdit);
        QWidget::setTabOrder(searchClassificationTagLineEdit, searchPostProcessingNameLineEdit);
        QWidget::setTabOrder(searchPostProcessingNameLineEdit, searchPostProcessingTagLineEdit);
        QWidget::setTabOrder(searchPostProcessingTagLineEdit, analysisListView);
        QWidget::setTabOrder(analysisListView, searchConnTagLineEdit);
        QWidget::setTabOrder(searchConnTagLineEdit, scanResultListView);
        QWidget::setTabOrder(scanResultListView, searchRodModuleLineEdit);

        retranslateUi(SearchDbCoralFormBase);
        QObject::connect(closePushButton, SIGNAL(clicked()), SearchDbCoralFormBase, SLOT(close()));
        QObject::connect(searchPushButton, SIGNAL(clicked()), SearchDbCoralFormBase, SLOT(search()));
        QObject::connect(clearPushButton, SIGNAL(clicked()), searchCfgTagLineEdit, SLOT(clear()));
        QObject::connect(clearPushButton, SIGNAL(clicked()), searchConnTagLineEdit, SLOT(clear()));
        //QObject::connect(getSelectedPushButton, SIGNAL(clicked()), SearchDbCoralFormBase, SLOT(getSelected()));
        QObject::connect(cancelPushButton, SIGNAL(clicked()), SearchDbCoralFormBase, SLOT(reject()));
        QObject::connect(closePushButton, SIGNAL(clicked()), SearchDbCoralFormBase, SLOT(destroyForm()));
        QObject::connect(clearPushButton, SIGNAL(clicked()), scanResultListView, SLOT(clear()));
        QObject::connect(getSelectedPushButton, SIGNAL(clicked()), SearchDbCoralFormBase, SLOT(loadSelected()));
        QObject::connect(clearPushButton, SIGNAL(clicked()), analysisListView, SLOT(clear()));
        QObject::connect(clearPushButton, SIGNAL(clicked()), searchClassificationTagLineEdit, SLOT(clear()));
        QObject::connect(clearPushButton, SIGNAL(clicked()), searchPostProcessingNameLineEdit, SLOT(clear()));
        QObject::connect(clearPushButton, SIGNAL(clicked()), searchClassificationNameLineEdit, SLOT(clear()));
        QObject::connect(clearPushButton, SIGNAL(clicked()), searchPostProcessingTagLineEdit, SLOT(clear()));
        QObject::connect(searchComboBox, SIGNAL(activated(int)), widgetStack, SLOT(setCurrentIndex(int)));
        QObject::connect(clearPushButton, SIGNAL(clicked()), SearchDbCoralFormBase, SLOT(resetComboBoxes()));
        QObject::connect(m_hasAnalysisButton, SIGNAL(clicked()), SearchDbCoralFormBase, SLOT(checkToggleAnalysis()));
        QObject::connect(m_hasNoAnalysisButton, SIGNAL(clicked()), SearchDbCoralFormBase, SLOT(checkToggleNoAnalysis()));
        //QObject::connect(scanResultListView, SIGNAL(doubleClicked(QTreeWidgetItem*)), SearchDbCoralFormBase, SLOT(listAnalyses(QTreeWidgetItem*)));
        QObject::connect(clearPushButton, SIGNAL(clicked()), searchRodModuleLineEdit, SLOT(clear()));

        QMetaObject::connectSlotsByName(SearchDbCoralFormBase);


    } // setupUi

    void retranslateUi(QDialog *SearchDbCoralFormBase)
    {
        SearchDbCoralFormBase->setWindowTitle(QApplication::translate("SearchDbCoralFormBase", "SearchDbCoralFormBase", 0));
        textLabel2->setText(QApplication::translate("SearchDbCoralFormBase", "Monitor / Search / Load Scan or Analysis", 0));
        textLabel1_5_2->setText(QApplication::translate("SearchDbCoralFormBase", "<p align=\"center\"><font size=\"+1\"><b>Search</b></font></p>", 0));
        textLabel1_3_2_3_2_2->setText(QApplication::translate("SearchDbCoralFormBase", "<b>Status:<b><font size=\"+1\"><font size=\"+1\"></font></font></b></b>", 0));
        searchStatusComboBox->clear();
        searchStatusComboBox->insertItems(0, QStringList()
         << QApplication::translate("SearchDbCoralFormBase", "Any Status", 0)
         << QApplication::translate("SearchDbCoralFormBase", "Success", 0)
         << QApplication::translate("SearchDbCoralFormBase", "Failure", 0)
         << QApplication::translate("SearchDbCoralFormBase", "Aborted", 0)
        );
        textLabel1_3_2_4_4_3_2->setText(QApplication::translate("SearchDbCoralFormBase", "<p align=\"right\"><b>ID : <b><font size=\"+1\"><font size=\"+1\"></font></font></b></b></p>", 0));
        textLabel1_3_2_4_3_2_2->setText(QApplication::translate("SearchDbCoralFormBase", "<b>Comment:<b><font size=\"+1\"><font size=\"+1\"></font></font></b></b>", 0));
        searchComboBox->clear();
        searchComboBox->insertItems(0, QStringList()
         << QApplication::translate("SearchDbCoralFormBase", "Scan", 0)
         << QApplication::translate("SearchDbCoralFormBase", "Analysis", 0)
        );
        textLabel1->setText(QApplication::translate("SearchDbCoralFormBase", "From\n"
"(DD/MM/YYYY) :", 0));
        textLabel1_6->setText(QApplication::translate("SearchDbCoralFormBase", "To\n"
"(DD/MM/YYYY) :", 0));
        textLabel1_4->setText(QApplication::translate("SearchDbCoralFormBase", "<font size=\"+1\"><b>Scan Search :</b></font>", 0));
        textLabel1_3_2->setText(QApplication::translate("SearchDbCoralFormBase", "<b>Scan Type:<b><font size=\"+1\"><font size=\"+1\"></font></font></b></b>", 0));
        searchScanTypeComboBox->clear();
        searchScanTypeComboBox->insertItems(0, QStringList()
         << QApplication::translate("SearchDbCoralFormBase", "Any Scan Type", 0)
         << QApplication::translate("SearchDbCoralFormBase", "CT_ThresholdScan", 0)
         << QApplication::translate("SearchDbCoralFormBase", "CT_ThresholdScan_HVoff", 0)
         << QApplication::translate("SearchDbCoralFormBase", "CT_FiberSignOff_Inlink", 0)
         << QApplication::translate("SearchDbCoralFormBase", "CT_FiberSignOff_InOutLink", 0)
         << QApplication::translate("SearchDbCoralFormBase", "CT_BocFastTuning_mod1212121", 0)
         << QApplication::translate("SearchDbCoralFormBase", "CT_BocFastTuning_noDDC", 0)
         << QApplication::translate("SearchDbCoralFormBase", "BOC_THR_DEL_FAST", 0)
         << QApplication::translate("SearchDbCoralFormBase", "THRESHOLD_SCAN", 0)
         << QApplication::translate("SearchDbCoralFormBase", "INTIME_THRESH_SCAN", 0)
         << QApplication::translate("SearchDbCoralFormBase", "DIGITAL_TEST", 0)
         << QApplication::translate("SearchDbCoralFormBase", "ANALOG_TEST", 0)
         << QApplication::translate("SearchDbCoralFormBase", "GDAC_TUNE", 0)
         << QApplication::translate("SearchDbCoralFormBase", "TDAC_TUNE", 0)
         << QApplication::translate("SearchDbCoralFormBase", "TDAC_FAST_TUNE", 0)
         << QApplication::translate("SearchDbCoralFormBase", "TDAC_SLOW_TUNE", 0)
         << QApplication::translate("SearchDbCoralFormBase", "SHAPESCAN", 0)
         << QApplication::translate("SearchDbCoralFormBase", "T0_Scan", 0)
         << QApplication::translate("SearchDbCoralFormBase", "TIMEWALK_MEASURE", 0)
         << QApplication::translate("SearchDbCoralFormBase", "BOC_RX_DELAY_SCAN", 0)
         << QApplication::translate("SearchDbCoralFormBase", "CROSSTALK_SCAN", 0)
         << QApplication::translate("SearchDbCoralFormBase", "OPTO_TUNE", 0)
        );
        textLabel1_3->setText(QApplication::translate("SearchDbCoralFormBase", "<b>Location:<b><font size=\"+1\"><font size=\"+1\"></font></font></b></b>", 0));
        searchLocationComboBox->clear();
        searchLocationComboBox->insertItems(0, QStringList()
         << QApplication::translate("SearchDbCoralFormBase", "Any Location", 0)
         << QApplication::translate("SearchDbCoralFormBase", "SR1/ToothPix", 0)
         << QApplication::translate("SearchDbCoralFormBase", "ATLAS/PIT", 0)
         << QApplication::translate("SearchDbCoralFormBase", "Pixel Lab", 0)
        );
        textLabel1_3_2_2->setText(QApplication::translate("SearchDbCoralFormBase", "<b>Quality:<b><font size=\"+1\"><font size=\"+1\"></font></font></b></b>", 0));
        searchQualityComboBox->clear();
        searchQualityComboBox->insertItems(0, QStringList()
         << QApplication::translate("SearchDbCoralFormBase", "Any Quality", 0)
         << QApplication::translate("SearchDbCoralFormBase", "Normal", 0)
         << QApplication::translate("SearchDbCoralFormBase", "Debug", 0)
         << QApplication::translate("SearchDbCoralFormBase", "Trash", 0)
        );
        buttonGroup1->setTitle(QString());
        textLabel1_7->setText(QApplication::translate("SearchDbCoralFormBase", "Has Analysis :", 0));
        m_hasAnalysisButton->setText(QApplication::translate("SearchDbCoralFormBase", "Yes", 0));
        m_hasNoAnalysisButton->setText(QApplication::translate("SearchDbCoralFormBase", "No", 0));
        textLabel1_8->setText(QApplication::translate("SearchDbCoralFormBase", "<b>ROD/Module:<b><font size=\"+1\"><font size=\"+1\"></font></font></b></b>", 0));
        textLabel1_3_2_4->setText(QApplication::translate("SearchDbCoralFormBase", "<b>Conn Tag:<b><font size=\"+1\"><font size=\"+1\"></font></font></b></b>", 0));
        textLabel1_3_2_5->setText(QApplication::translate("SearchDbCoralFormBase", "<b>Config Tag:<b><font size=\"+1\"><font size=\"+1\"></font></font></b></b>", 0));
        textLabel1_2->setText(QApplication::translate("SearchDbCoralFormBase", "<font size=\"+1\"><b>Scan Results :</b></font>", 0));
        textLabel1_5->setText(QApplication::translate("SearchDbCoralFormBase", "<font size=\"+1\"><b>Analysis Search :</b></font>", 0));
        textLabel1_3_2_6->setText(QApplication::translate("SearchDbCoralFormBase", "<b>Analysis Type:<b><font size=\"+1\"><font size=\"+1\"></font></font></b></b>", 0));
        textLabel1_3_2_4_5_2->setText(QApplication::translate("SearchDbCoralFormBase", "<b>Post Proc.\n"
"Name:<b><font size=\"+1\"><font size=\"+1\"></font></font></b></b>", 0));
        textLabel1_3_2_5_3->setText(QApplication::translate("SearchDbCoralFormBase", "<b>Classification\n"
"Tag:<b><font size=\"+1\"><font size=\"+1\"></font></font></b></b>", 0));
        textLabel1_3_2_4_5->setText(QApplication::translate("SearchDbCoralFormBase", "<b>Classification\n"
"Name:<b><font size=\"+1\"><font size=\"+1\"></font></font></b></b>", 0));
        textLabel1_3_2_5_3_2->setText(QApplication::translate("SearchDbCoralFormBase", "<b>PostProc.\n"
"Tag:<b><font size=\"+1\"><font size=\"+1\"></font></font></b></b>", 0));
        searchAnalysisTypeComboBox->clear();
        searchAnalysisTypeComboBox->insertItems(0, QStringList()
         << QApplication::translate("SearchDbCoralFormBase", "Any Analysis Type", 0)
         << QApplication::translate("SearchDbCoralFormBase", "THRESHOLDanalysis", 0)
         << QApplication::translate("SearchDbCoralFormBase", "BASICTHRanalysis", 0)
         << QApplication::translate("SearchDbCoralFormBase", "RxThresholdDelayCheck", 0)
         << QApplication::translate("SearchDbCoralFormBase", "BOCanalysis", 0)
         << QApplication::translate("SearchDbCoralFormBase", "BONDanalysis", 0)
         << QApplication::translate("SearchDbCoralFormBase", "ANALOGanalysis", 0)
         << QApplication::translate("SearchDbCoralFormBase", "TestAnalysis", 0)
         << QApplication::translate("SearchDbCoralFormBase", "algo", 0)
        );
        textLabel1_3_2_4_4_2->setText(QApplication::translate("SearchDbCoralFormBase", "<b>Analysis ID:<b><font size=\"+1\"><font size=\"+1\"></font></font></b></b>", 0));
        textLabel1_2_2->setText(QApplication::translate("SearchDbCoralFormBase", "<font size=\"+1\"><b>Analysis Results :</b></font>", 0));
        clearPushButton->setText(QApplication::translate("SearchDbCoralFormBase", "Clear", 0));
        cancelPushButton->setText(QApplication::translate("SearchDbCoralFormBase", "Cancel", 0));
        searchPushButton->setText(QApplication::translate("SearchDbCoralFormBase", "&Search", 0));
        getSelectedPushButton->setText(QApplication::translate("SearchDbCoralFormBase", "&Open Selected", 0));
        closePushButton->setText(QApplication::translate("SearchDbCoralFormBase", "Close", 0));
        closePushButton->setShortcut(QApplication::translate("SearchDbCoralFormBase", "Esc", 0));
    } // retranslateUi

};

namespace Ui {
    class SearchDbCoralFormBase: public Ui_SearchDbCoralFormBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SEARCHDBCORALFORMBASE_H
