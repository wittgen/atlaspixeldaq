/* Dear emacs, this is -*-c++-*- */
#ifndef _PixCon_StatusMonitor_h_
#define _PixCon_StatusMonitor_h_

#include <IsMonitor.h>
#include <list>
#include <set>
#include <map>
#include <Lock.h>
#include <Flag.h>

#include "IStatusMonitor.h"

#include <qthread.h>

namespace PixCon {

  class CalibrationDataManager;
  class MonitorThread;
  class StatusChangeTransceiver;

  /** Class to monitor a particular status e.g. scan or analysis and react on status changes.
   */
  class StatusMonitor : public IStatusMonitor
  {
    friend class MonitorThread;
  public:

    /** Function type to translate status names into an integer level.
     */
    typedef int (*TransLateStatusNameFuncPtr_t)( const std::string &name);

    /** Function type to extract the connectivity name from an is variable name.
     */
    typedef std::string (*ExtractConnNameFuncPtr_t)( const std::string &name);


    static std::string isStatusVarConnNameExtractFunction(const std::string &full_is_name);

    /** Data of the status monitor which is shared with its childs
     */
    class StatusMonitorData_t
    {
    public:
      StatusMonitorData_t (const std::shared_ptr<IsReceptor> &receptor,
			   const std::shared_ptr<IsMonitorManager> &is_monitor_manager,
			   EMeasurementType measurement_type,
			   const std::string &var_name,
			   const std::string &calibration_data_var_name,
			   const std::shared_ptr<CalibrationDataManager> &calibration_data,
			   int status_threshold,
			   TransLateStatusNameFuncPtr_t translate_status_name_func,
			   ExtractConnNameFuncPtr_t extract_conn_name_func = &isStatusVarConnNameExtractFunction)
	: m_receptor(receptor),
	  m_isMonitorManager(is_monitor_manager),
	  m_measurementType(measurement_type),
	  m_varName(var_name),
	  m_calibrationDataVarName(calibration_data_var_name),
	  m_calibrationData(calibration_data),
	  m_statusThreshold(status_threshold),
	  m_sequence(0),
	  m_statusChangeFlag(NULL),
	  m_translateStatusName(translate_status_name_func),
	  m_extractConnName(extract_conn_name_func)
      { if (m_calibrationDataVarName.empty()) m_calibrationDataVarName=m_varName; }

      StatusMonitorData_t (const std::shared_ptr<IsReceptor> &receptor,
			   const std::shared_ptr<IsMonitorManager> &is_monitor_manager,
			   EMeasurementType measurement_type,
			   const std::string &var_name,
			   const std::string &global_var_name,
			   const std::string &calibration_data_var_name,
			   const std::shared_ptr<CalibrationDataManager> &calibration_data,
			   int status_threshold,
			   TransLateStatusNameFuncPtr_t translate_status_name_func,
			   ExtractConnNameFuncPtr_t extract_conn_name_func = &isStatusVarConnNameExtractFunction)
	: m_receptor(receptor),
	  m_isMonitorManager(is_monitor_manager),
	  m_measurementType(measurement_type),
	  m_varName(var_name),
	  m_globalVarName(global_var_name),
	  m_calibrationDataVarName(calibration_data_var_name),
	  m_calibrationData(calibration_data),
	  m_statusThreshold(status_threshold),
	  m_sequence(0),
	  m_statusChangeFlag(NULL),
	  m_translateStatusName(translate_status_name_func),
	  m_extractConnName(extract_conn_name_func)
      { if (m_calibrationDataVarName.empty()) m_calibrationDataVarName=m_varName; }

      /** Pass a reference of the status change flag to the shred data structure.
       * The status change flag belongs to the monitoring thread
       *  The flag indicates that the status of a ROD exceeded the threshold.
       */
      void setStatusChangeFlag( Flag &the_flag ) { m_statusChangeFlag = &the_flag; }

      /** Get the status change flag of the monitoring thread.
       *  The flag indicates that the status of a ROD exceeded the threshold.
       */
      Flag &statusChangeFlag()                                     { assert(m_statusChangeFlag != NULL); return *m_statusChangeFlag; }

      /** Get the status change flag of the monitoring thread (read only).
       *  The flag indicates that the status of a ROD exceeded the threshold.
       */
      const Flag &statusChangeFlag() const                         { assert(m_statusChangeFlag != NULL); return *m_statusChangeFlag; }

      /** Get the type of the monitored measurement.
       */
      EMeasurementType measurementType() const                     { return m_measurementType; }

      /** Get an type identifier of the monitored measurement which is used in IS.
       */
      char measurementTypeId() const {
	return measurementTypeId( measurementType() );
      }

      /** Auxillary function to get the type identifier of the monitored measurement used in IS.
       */
      static char measurementTypeId(EMeasurementType measurement_type ) 
      { assert(measurement_type < kNMeasurements); return m_measurementTypeId[measurement_type]; }

      /** Get the IS receptor which further distributes status changes.
       */
      std::shared_ptr<IsReceptor> &receptor()                    { return m_receptor; }

      /** Get the IS receptor which further distributes status changes (read only).
       * @todo needed ?
       */
      const std::shared_ptr<IsReceptor> &receptor() const        { return m_receptor; }


      /** Get the IS monitor manager.
       */
      std::shared_ptr<IsMonitorManager> &monitorManager()        { return m_isMonitorManager; }

      /** Return true if there is a global variable to be monitored.
       */
      bool hasPerRodVar() const { return !m_varName.empty(); }

      /** Get the name of the IS variable which is monitored.
       */
      const std::string &varName() const                           { return m_varName; }

      /** Return true if there is a global variable to be monitored.
       */
      bool hasGlobalVar() const { return !m_globalVarName.empty(); }

      /** Get the name of the global IS variable which is monitored.
       * The global variable tells the status of the entire measurement.
       */
      const std::string &globalVarName() const                     { return m_globalVarName; }

      /** Get the name to be used in the calibration data
       */
      const std::string &calibrationDataVarName() const            { return m_calibrationDataVarName; }

      /** Get the calibration data manager.
       */
      std::shared_ptr<CalibrationDataManager> &calibrationData() { return m_calibrationData; }

      /** Get the calibration data manager (read only).
       */
      const std::shared_ptr<CalibrationDataManager> &calibrationData() const { return m_calibrationData; }

      /** Get the status threshold.
       * If the status exceeds this threshold then the scan is considered to have finished.
       */
      int statusThreshold() const                                  { return  m_statusThreshold; }

      /** Get the function to translate the status name into a level.
       */
      TransLateStatusNameFuncPtr_t translateStatusNameFunction() const { return m_translateStatusName; }

      /** Get the function to translate the status name into a level.
       */
      ExtractConnNameFuncPtr_t extractConnName() const { return m_extractConnName; }


      /** Notify the main monitoring thread that the status of a ROD has exceeded the threshold.
       */
      void statusChanged() {
	m_sequence++;
	statusChangeFlag().setFlag();
      }

      /** Get the sequence number of a status change.
       * Each status change gets a sequence number. If the sequence number did not change
       * during a particular period no status exceeded the threshold during that period.
       */
      unsigned int sequence() { return m_sequence; }

      /** Return the category name of the status variable.
       * @todo should all the status variables end up in one category.
       */
      static const std::string &statusCategoryName() { return s_statusCategoryName; }

    private:
      static const char m_measurementTypeId[kNMeasurements];

      std::shared_ptr<IsReceptor> m_receptor;
      std::shared_ptr<IsMonitorManager>    m_isMonitorManager; 

      EMeasurementType              m_measurementType;                  /**< The type of measurements which are monitored. */
      std::string                   m_varName;                          /**< The name in IS of the status variable. Only the bare
									     variable name without IS server, connectivity  name
									     and serial number.*/
      std::string                   m_globalVarName;                    /**< Name of the global status.*/
      std::string                   m_calibrationDataVarName;           /**< Name to be used in the calibration data.*/           

      std::shared_ptr<CalibrationDataManager> m_calibrationData;      /**< Pointer to the calibration data manager.*/
      int                           m_statusThreshold;                  /**< The status threshold. If the status exceeds this threshold
									     a e.g. scan or analysis is considered to have finished.*/

      unsigned int                  m_sequence;                         /**< The sequence number of the last status change.*/
      Flag                         *m_statusChangeFlag;                 /**< Pointer to the status change flag of the main monitoring thread.*/

      TransLateStatusNameFuncPtr_t  m_translateStatusName;              /**< Function to translate a status name into a level.*/
      ExtractConnNameFuncPtr_t      m_extractConnName;                  /**< Function to extract the connectivity name from a full is variable name.*/
      static const std::string      s_statusCategoryName;               /**< The category name to be used for the status variable.*/
    };




    /** Monitor per ROD status of a particular scan or analysis.
     * Whenever the status exceeds a certaion threshold (@ref StatusMonitorData_t),
     * the corresponding ROD name is added to the internal list @ref m_rodList and
     *  the main status montioring thread is notified.
     */
    class IsStatusMonitor : public SimpleIsMonitorReceiver<std::string,const std::string&>
    {
    public:

      IsStatusMonitor(StatusMonitorData_t &monitor_data,
		      SerialNumber_t serial_number,
		      EValueConnType conn_type,
		      bool verbose);

      ~IsStatusMonitor();

      /** Get the beginning of the list of RODs whose status exceeded a certain threshold.
       */
      std::list<std::string>::iterator begin()             { return m_rodList.begin(); }

      /** Get the beginning of the list of RODs whose status exceeded a certain threshold (read only).
       */
      std::list<std::string>::const_iterator begin() const { return m_rodList.begin(); }

      /** Get the end of the list of RODs whose status exceeded a certain threshold (read only).
       */
      std::list<std::string>::const_iterator end()   const { return m_rodList.end(); }

      /** Remove a ROD from the list.
       */
      void erase(std::list<std::string>::iterator &iter)   { m_rodList.erase(iter); }

      /** Return if there are no more RODs which exceed the threshold after the last processing
       */
      bool empty() const                                   { return m_rodList.empty(); }

      /** Get the mutex which protects the lost of RODs which recently exceeded the threshold.
       */
      Mutex &mutex() { return m_rodListMutex; }

    protected:

      /** Forward the new status to the main application and eventually notify the monitoring thread.
       * The status is copied to the @ref CalibrationData by @ref IsReceptor::setValue and en event
       * is sent to the main application.
       * Whenever the status exceeds a certain threshold the monitoring thread is notified @ref notify.
       */
      void setValue(const std::string &is_name,  ::time_t time_stamp, const std::string &value);

      /** Notify the monitoring thread.
       */
      void notify(const std::string &conn_name)  {
	{
	  Lock lock(m_rodListMutex);
	  m_rodList.push_back(conn_name);
	}
	m_statusMonitorData->statusChanged();

      }

      /** Create the regular expressing to selectt the IS variables.
       */
      static std::string makeRegex(const StatusMonitorData_t &m_statusMonitorData, SerialNumber_t serial_number);


      void registerForCleanup(const std::string &/*is_name*/, std::map<std::string, std::set<std::string> > &/*clean_up_var_list*/) {
	//@todo can something be done ?, the problem is the cleanup should happen for a certain measurement only
      }

    private:
      SerialNumber_t m_serialNumber;                 /**< Serial number of the measurement being monitored.*/
      StatusMonitorData_t    *m_statusMonitorData;   /**< Shared data between monitores of different measurements. */
      Mutex m_rodListMutex;                          /**< Mutex to protect the list of RODs whose the status exceeded
							  the threshold recently.*/
      std::list< std::string > m_rodList;            /**< The list of RODs whose status exceeded the threshold 
							  recently.*/
      bool m_verbose;
    };

    class GlobalStatusMonitorBase
    {
    public:
      GlobalStatusMonitorBase(bool exceeds_threshold_already=false, bool verbose=false) 
	: m_exceededThreshold(exceeds_threshold_already),m_verbose(verbose) {}
      virtual ~GlobalStatusMonitorBase() {}

      bool exceededThreshold() const { return m_exceededThreshold; }

    protected:
      bool                    m_exceededThreshold;   /**< Set to true if the global status exceeded the threshold. */
      bool                    m_verbose;

    };


    class IsMonitorReceiverInterceptor : public SimpleIsMonitorReceiver<std::string, const std::string&>
    {
    public:
      IsMonitorReceiverInterceptor(const std::shared_ptr<IsReceptor> &a_receptor,
				   EValueConnType conn_type,
				   const std::string &is_regex,
				   unsigned int buffer_size,
				   unsigned int max_time_between_updates)
	: SimpleIsMonitorReceiver<std::string,const std::string&>(a_receptor,
								  conn_type,
                                                                  "",
								  is_regex,
								  buffer_size,
								  max_time_between_updates)
      { std::cout << "INFO [IsMonitorReceiverInterceptor::ctor] construct "  << is_regex << " : " << static_cast<void *>(this); }

      ~IsMonitorReceiverInterceptor() { std::cout << "INFO [IsMonitorReceiverInterceptor::dtor] destruct " << static_cast<void *>(this); }
    };


    class GlobalIsStatusMonitorHelper;

    /** Monitor the global status of a particular scan or analysis.
     * Whenever the status exceeds a certaion threshold (@ref StatusMonitorData_t),
     * the measurement (e.g. scan or analysis) is considered to be finished.
     */
    class GlobalIsStatusMonitor : public GlobalStatusMonitorBase /*IsMonitorReceiver<std::string>*/
    {
    public:
      friend class GlobalIsStatusMonitorHelper;

      GlobalIsStatusMonitor(StatusMonitorData_t &monitor_data,
			    SerialNumber_t serial_number,
			    bool verbose);

      ~GlobalIsStatusMonitor();

    protected:

      /** Forward the new status to the main application and eventually notify the monitoring thread.
       * The status is copied to the @ref CalibrationData by @ref IsReceptor::setValue and en event
       * is sent to the main application.
       * Whenever the status exceeds a certain threshold the monitoring thread is notified @ref notify.
       */
      void setValue(const std::string &is_name,  ::time_t time_stamp, const std::string &value);

      /** Notify the monitoring thread.
       */
      void notify()  {
	m_statusMonitorData->statusChanged();
      }

      /** Create the regular expressing to select the IS variable.
       */
      static std::string makeRegex(const StatusMonitorData_t &m_statusMonitorData, SerialNumber_t serial_number);

    private:
      SerialNumber_t          m_serialNumber;        /**< Serial number of the measurement being monitored.*/
      StatusMonitorData_t    *m_statusMonitorData;   /**< Shared data between monitores of different measurements. */
      std::shared_ptr<GlobalIsStatusMonitorHelper> m_isMonitor;
    };

    class GlobalIsStatusMonitorHelper :  public IsMonitorReceiverInterceptor
    {
    public:
      GlobalIsStatusMonitorHelper(GlobalIsStatusMonitor *global_is_status_monitor,
				  const std::shared_ptr<IsReceptor> &is_receptor,
				  const std::string &regex);

      void setValue(const std::string &is_name,  ::time_t time_stamp, const std::string &value) {
	m_isMonitor->setValue(is_name, time_stamp, value);
      }

      EValueConnType valueConnType() const { return IsMonitorReceiverInterceptor::valueConnType(); }

      void registerForCleanup(const std::string &/*is_name*/, std::map<std::string, std::set<std::string> > &/*clean_up_var_list*/) {
	// @todo is it possible to do something ? 
      }


    private:
      GlobalIsStatusMonitor *m_isMonitor;
    };

    enum EStatus  {kOk, kFailed, kAborted, kUnknown, kRunning, kLoading, kLoadingFailed, kLoadingAborted, kLoadingUnknown, kNStates, kUnset};
    static unsigned short s_precedence[kUnset+1];

    static bool isLoading(EStatus status)     { return status>=kLoading && status<=kLoadingUnknown; }
    static bool isNotLoading(EStatus status)  { return status<=kUnknown; }
    static EStatus makeLoading(EStatus status) { if (status<=kUnknown) {return static_cast<EStatus>(status + kLoading); } else { return status; } }
    static EStatus clearLoading(EStatus status) { if (isLoading(status)) {return static_cast<EStatus>(status - kLoading); } else { return status; } }

    static EStatus combineStatus(EStatus status_old, EStatus status_new) {

      bool is_loading=isLoading(status_new);
      status_new = clearLoading(status_new);

      if (status_new>=kNStates) {
	status_new=kUnset;
      }
      if (status_old>=kNStates) {
	status_old=kUnset;
      }
      if (s_precedence[status_new] <= s_precedence[status_old]) {
	status_new = status_old;
      }
      if (is_loading) {
	status_new=makeLoading(status_new);
      }
      return status_new;
      //       if (status_old == kFailed || status_old == kAborted || (status_old==kUnknown && status_new != kFailed) || status_new>kUnknown) {
      // 	return status_old;
      //       }
      //       else {
      // 	return status_new;
      //       }
    }

    /* Contructor of a status monitor .
     * @param receptor reference to a receptor which recepts the status from IS and which also will copy the status to the calibration data.
     * @param is_monitor_manager pointer to the IS monitor manager to which the per scan is monitors will be added.
     * @param measurement_type the type of status which could be scan or analysis.
     * @param var_name the name of the status variable e.g. STATUS for scan.
     * @param calibration_data_var_name the variable name to be used in the calibration data container.
     * @param calibration_data the calibration data manager which will be filled with a flag derived from the number of histograms which exist per ROD.
     * @param status_threshold if the status gets equal to or larger than this value, the scan is considered to be finished.
     * @param status_change_transceiver auxiliary class to broadcast status changes of scans, analyses etc.
     * The status monitor will post events of type @ref QNewVarEvent to announce
     * the addition of the status variable to a measurement.
     */
    StatusMonitor(const std::shared_ptr<IsReceptor> &receptor,
		  const std::shared_ptr<IsMonitorManager> &is_monitor_manager,
		  EMeasurementType measurement_type,
		  const std::string &var_name,
		  const std::string &calibration_data_var_name,
		  const std::shared_ptr<CalibrationDataManager> &calibration_data,
		  int status_threshold,
		  TransLateStatusNameFuncPtr_t translate_status_name_func,
		  const std::shared_ptr<StatusChangeTransceiver> &status_change_transceiver);

    /* Contructor of a status monitor .
     * @param receptor reference to a receptor which recepts the status from IS and which also will copy the status to the calibration data.
     * @param is_monitor_manager pointer to the IS monitor manager to which the per scan is monitors will be added.
     * @param measurement_type the type of status which could be scan or analysis.
     * @param var_name the name of the per-ROD-status variable e.g. STATUS for scan.
     * @param global_var_name the name of a global status variable e.g. globalStatus for an analysis.
     * @param calibration_data_var_name the variable name to be used in the calibration data container.
     * @param calibration_data the calibration data manager which will be filled with a flag derived from the number of histograms which exist per ROD.
     * @param status_threshold if the status gets equal to or larger than this value, the scan is considered to be finished.
     * @param status_change_transceiver auxiliary class to broadcast status changes of scans, analyses etc.
     * The status monitor will post events of type @ref QNewVarEvent to announce
     * the addition of the status variable to a measurement.
     */
    StatusMonitor(const std::shared_ptr<IsReceptor> &receptor,
		  const std::shared_ptr<IsMonitorManager> &is_monitor_manager,
		  EMeasurementType measurement_type,
		  const std::string &var_name,
		  const std::string &global_var_name,
		  const std::string &calibration_data_var_name,
		  const std::shared_ptr<CalibrationDataManager> &calibration_data,
		  int status_threshold,
		  TransLateStatusNameFuncPtr_t translate_status_name_func,
		  const std::shared_ptr<StatusChangeTransceiver> &status_change_transceiver);

    virtual ~StatusMonitor();

    /** Add a scan to the list of monitored scans.
     * @param serial_number the serial number of the measurement i.e. scan or analysis.
     * @param global_status_exceeds_threshold_already set to true if the global status is already above threshold. 
     * @return true if the monitoring was activated successfully.
     */
    virtual bool monitorStatus(SerialNumber_t serial_number,
			       bool global_status_exceeds_threshold_already=false);

    /** Enforce the monitoring thread to finish.
     * @param serial_number serial number of the measurement
     */
    virtual void hasFinished(SerialNumber_t serial_number);

    /** Stop the monitoring of a particular status
     */
    virtual void abortMonitoring(SerialNumber_t serial_number);

    /** Process the status monitors if there were any changes.
     */
    void process();

    virtual void timeout(double /*elapsed_seconds*/) {};

    /** Get the status change transceiver.
     * The transceiver provides methods to wait for status changes.
     */
    std::shared_ptr<StatusChangeTransceiver> &statusChangeTransceiver() { return m_statusChangeTransceiver; }

    const std::string &varName() const                           { return m_data.varName(); }

    const std::string &calibrationDataVarName() const            { return m_data.calibrationDataVarName(); }

    const std::string &statusCategoryName() const                { return m_data.statusCategoryName(); }


  protected:

    /** Function to be called when a scan, analysis etc. terminates for a ROD.
     * @param serial_number the serial number of the measurement which is to be processed.
     * @param rod_name the name of the ROD which is to be processed.
     * @param var_list the list of variables which is currently assigned to a given measurement.
     * The var_list can be used to send events of type @ref QNewVarEvent to notify the application
     * about new variables. The var_list can be extended by the process method.
     */
    virtual EStatus process(SerialNumber_t serial_number, const std::string &rod_name, std::set<std::string> &var_list) = 0;

    /** Function to be called when a scan, analysis etc. has terminated for all RODs.
     * @return the global status.
     */
    virtual EStatus finish(SerialNumber_t serial_number, EStatus combined_status) = 0;

    /** \defgroup MonitorDataAccessors Accessor functions of the status monitor data.
     * @ref StatusMonitorData_t
     * \@{
     */
    Flag &statusChangeFlag()                                     { return m_data.statusChangeFlag(); }
    const Flag &statusChangeFlag() const                         { return m_data.statusChangeFlag(); }

    EMeasurementType measurementType() const                     { return m_data.measurementType(); }

    static char measurementTypeId(EMeasurementType measurement_type )
    { return StatusMonitorData_t::measurementTypeId(measurement_type); }

    std::shared_ptr<IsReceptor> &receptor()                    { return m_data.receptor(); }
    const std::shared_ptr<IsReceptor> &receptor() const        { return m_data.receptor(); }

    std::shared_ptr<CalibrationDataManager> &calibrationData() { return m_data.calibrationData(); }
    const std::shared_ptr<CalibrationDataManager> &calibrationData() const { return m_data.calibrationData(); }

    int statusThreshold() const                                  { return  m_data.statusThreshold(); }

    void setStatusChangeFlag( Flag &the_flag ) { m_data.setStatusChangeFlag(the_flag); }

    /** \@} */

    /** Announce the addition of a new variable.
     */
    void announceNewVaiable(SerialNumber_t serial_number, const std::string &category, const std::string &var_name);

//     /** Annouce a status change.
//      */
//     void announceStatusChange(SerialNumber_t serial_number);

  private:
    unsigned int        m_processedSequence;        /**< Sequence number of the last processed status change.*/

    StatusMonitorData_t m_data;                     /**< Data to be shared among monitores of different measurements
                                                         (i.e. serial numbers.).*/

    Mutex m_statusMonitorMutex;                     /**< Mutex to protect the list of measurements (i.e. serial numbers)
						       to be monitored.*/
    std::shared_ptr<StatusChangeTransceiver> m_statusChangeTransceiver;/**< Auxiliary class to notify listeners about status changes.*/

    /** Auxiliary class which contains a list of all RODs which need to be monitored and the monitoring helper classes.
     */
    class RodMonitor_t {
    public:
      /** Setup the monitoring of a measurement.
       * @param per_rod_monitor pointer to a per rod monitoring helper class.
       * @param global_monitor pointer to a  monitoring helper class for the entire measurement.
       * A monitoring of a measurement may have a per ROD monitor and a global monitor. But at least
       * one pointer has to point to a valid object.
       */
      RodMonitor_t(const std::shared_ptr<IsMonitorManager> &is_monitor_manager,
		   const std::string &monitor_name,
		   IsStatusMonitor *per_rod_monitor,
		   GlobalStatusMonitorBase *global_monitor);

      ~RodMonitor_t();
      /** Get the monitoring helper class.
       */
      IsStatusMonitor &statusMonitor() { assert(m_isStatusMonitor.get()); return *m_isStatusMonitor;};

      //       /** Get the monitoring helper class.
      //        */
      //       GlobalIsStatusMonitor &globalStatusMonitor() { return *m_globalIsStatusMonitor;};

      /** Add a new ROD to the list of the to-be-monitored RODs.
       */
      void insert(const std::string &rod_name) {
	m_rodList.insert(rod_name);
      }

      /** Remove a ROD from the list of the to-be-monitored RODs.
       * To be called, once the ROD status exceeded the threshold.
       */
      void erase(const std::string &rod_name) {
	std::set<std::string>::iterator rod_iter = m_rodList.find(rod_name);
	if (rod_iter != m_rodList.end()) {
	  m_rodList.erase(rod_iter);
	}
      }

      /** Return if there is a global status monitor and if the global status exceeded the threshold.
       */
      bool globalStatusExceededThreshold() const {
	return (m_globalStatusExceededThresholdCache || (m_globalIsStatusMonitor.get() && m_globalIsStatusMonitor->exceededThreshold()));
      }

      /** Return true if the status of all RODs exceeded the threshold.
       */
      bool empty() const { return m_rodList.empty(); }


      /** Add the given variable to the list of variables of this measurement.
       * @param variable_name the name of the new variable
       * @return true in case the variable was added or false if the variable exists already.
       */
      bool addVariable(const std::string &variable_name) {
	std::pair< std::set<std::string>::iterator, bool> ret  = m_varList.insert(variable_name);
	return ret.second;
      }

      /** Get the list of variables which are registered for this measurement.
       */
      std::set<std::string> &varList()             { return m_varList; }

      /** Get the list of variables which are registered for this measurement (read only).
       */
      const std::set<std::string> &varList() const { return m_varList; }

      /** Return the global status.
       */
      EStatus status() const { return m_status; }

      /** Set the status.
       */
      void updateStatus(EStatus status)     {
	m_status = combineStatus(status, m_status );
      }

      void hasFinished() {
	m_globalStatusExceededThresholdCache=true;
      }

      void setAbort()    { m_abort=true; }
      bool abort() const { return m_abort; }

    private:
      std::set<std::string> m_rodList;                                    /**< List of all RODs to be monitored.*/
      std::shared_ptr<IsMonitorManager>        m_isMonitorManager;      /**< Manager of Is monitors, which also steers the creation and 
                                                                               reconnection of  the IS receiver.*/
      std::shared_ptr<IsStatusMonitor>         m_isStatusMonitor;       /**< The monitoring helper class.*/
      std::shared_ptr<GlobalStatusMonitorBase> m_globalIsStatusMonitor; /**< The helper class to monitor the global status.*/
      std::set<std::string> m_varList;                                    /**< A list of all variables which are available for a
									       particular serial number. */
      EStatus               m_status;                                     /**< global status. */
      bool                  m_globalStatusExceededThresholdCache;
      bool                  m_abort;                                      /**< set to true to abort.*/
    };

    std::map<SerialNumber_t, std::shared_ptr<RodMonitor_t> > m_statusMonitorList;  /**< List of all measurements (i.e. serial numbers
								    which are currently beeing monitored.*/

  protected:
    bool m_verbose;
  };

}
#endif
