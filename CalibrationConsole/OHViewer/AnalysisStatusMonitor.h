#ifndef _PixCon_AnalysisStatusMonitor_h_
#define _PixCon_AnalysisStatusMonitor_h_

#include "StatusMonitor.h"
#include "VarDefList.h"

class IPixResultsDbClient;

namespace PixCon {

  class OHHistogramProviderBase;
  class HistogramCache;
  class AnalysisResultListener; 

  /** Monitors particular analyses until they are finished and set the status in the calibration data container.
   * The calibration data container will be filled with the analyses status and the analyses results.
   */
  class AnalysisStatusMonitor : public StatusMonitor
  {
  public:
    enum HistogramState {kPresumablyDisabled, kNoHistograms, kOk, kOkButBelowExpectation, kOkButAboveExpectation, kMissingHistograms, kTooManyHistograms, kNStates};

    /** Create am analysis status monitor which will automatically gather retrieve the analyses results once an analyses has finished.
     * @param receptor reference to a receptor which recepts the status from IS and which also will copy the status to the calibration data.
     * @param is_monitor_manager pointer to the IS monitor manager to which the per scan is monitors will be added.
     * @param measurement_type the type of status which could be scan or analysis.
     * @param var_name the name of the status variable e.g. Status for an anlysis.
     * @param global_var_name the name of a global status variable e.g. globalStatus for an analysis.
     * @param calibration_data_var_name the variable name to be used in the calibration data container.
     * @param calibration_data the calibration data manager which will be filled with a flag derived from the number of histograms which exist per ROD.
     * @param status_threshold if the status gets equal to or larger than this value, the scan is considered to be finished.
     * The analysis status monitor will post events of type @ref QNewVarEvent to announce
     * the addition of new variables to a measurement. Via the @ref IsReceptor it will announce
     * new or changed values.
     */
    AnalysisStatusMonitor(OHHistogramProviderBase &histogram_provider,
			  const std::shared_ptr<PixCon::HistogramCache> &histogram_cache,
			  std::shared_ptr<IsReceptor> &receptor,
			  const std::shared_ptr<IsMonitorManager> &is_monitor_manager,
			  EMeasurementType measurement_type,
			  const std::string &var_name,
			  const std::string &global_var_name,
			  const std::string &calibration_data_var_name,
			  const std::shared_ptr<CalibrationDataManager> &calibration_data,
			  int status_threshold,
			  std::shared_ptr<StatusChangeTransceiver> &status_change_transceiver);

    ~AnalysisStatusMonitor();

  protected:
    /** Retrieve the analyses results and add them to the calibration data container.
     * @param serial_number the serial number of the scan in question.
     * @param rod_name the connectivity name of the ROD.
     * @param var_list the list of variables which is currently assigned to a given measurement.
     * The function will copy the analyses results to the calibration data container.
     * The application will be notified in case new variables are added. The new variables are also
     * added to the var_list.
     */
    StatusMonitor::EStatus process(SerialNumber_t serial_number, const std::string &rod_name, std::set<std::string> &var_list);

    /** Function to be called when an analysis has terminated for all RODs. 
     */
    StatusMonitor::EStatus finish(SerialNumber_t serial_number, StatusMonitor::EStatus combined_status);

    /** Close the coral connection after a certian number of elapsed seconds
     */
    void timeout(double elapsed_seconds);

    //    void abortMonitoring(SerialNumber_t serial_number);

    /** Translate the given status name into a status value.
     */
    static int translateStatusName(const std::string &status_name);

    VarId_t m_statusVarId;

    static const double m_connectionTimeOut /*moved to impl file : = 10*/;
    //    time_t m_lastAccess;
    std::unique_ptr<IPixResultsDbClient> m_analysisResultDb;

    std::unique_ptr<AnalysisResultListener> m_listener;

    void processRequests();
    void processRequests(SerialNumber_t serial_number, bool enforce);

    class ProcessingRequest_t {
    public:
      ProcessingRequest_t() : m_nRods(0), m_hasAllRequests(false), m_finished(false) { clear(); }

      void addRequest(const std::string &rod_name) {
	m_rodList.push_back(rod_name);
      }

      bool hasRequests() const { return !m_rodList.empty(); }

      double elapsedTime() const {
	time_t current_time = time(0);
	return difftime(current_time, m_lastProcessing);
      }

      std::vector<std::string>::const_iterator begin() const {
	return m_rodList.begin();
      }

      std::vector<std::string>::const_iterator end()  const {
	return m_rodList.end();
      }

      std::set<std::string> &varList()             { return m_varList; }
      const std::set<std::string> &varList() const { return m_varList; }

      void clear() {
	m_rodList.clear();
	m_lastProcessing=time(0);
      }

      void setFinished() { m_finished=true; }
      bool isFinished() const { return m_finished; }

      void setHasAllRequests() { m_hasAllRequests=true; }
      bool hasAllRequests() const { return m_hasAllRequests; }

      void incRods() { m_nRods++; }

      unsigned int nRodsProcessed() const { return m_nRods; }
    private:
      time_t  m_lastProcessing;
      std::vector<std::string> m_rodList;
      std::set<std::string>    m_varList;
      unsigned int             m_nRods;   //debugging

      bool m_hasAllRequests;
      bool m_finished;
    };

  private:
    StatusMonitor::EStatus process(SerialNumber_t serial_number, const std::string &rod_name);
    using StatusMonitor::process;
    OHHistogramProviderBase *m_histoProvider;
    std::shared_ptr<PixCon::HistogramCache> m_histogramCache;
    VarId_t m_varId;
    static std::vector<StatusMonitor::EStatus> s_convertStatus; 
    static const std::string s_histogramCategoryName;



    void processRequests(std::map<SerialNumber_t, ProcessingRequest_t>::iterator serial_number_iter);
    Mutex m_processingRequestMutex;
    std::map<SerialNumber_t, ProcessingRequest_t> m_processingRequests;
    
    static const double m_processingDelay /* moved to impl file = 12*/;
  };

}


#endif
