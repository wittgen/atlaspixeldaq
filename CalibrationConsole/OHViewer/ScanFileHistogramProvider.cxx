#include "ScanFileHistogramProvider.h"
#include <CalibrationDataUtil.h>
#include <ScanInfo_t.hh>
#include <iostream>
#include <DataContainer/RootResultFileAccessBase.h>

namespace PixCon {

  void ScanFileHistogramProvider::addScan(EMeasurementType measurement_type, SerialNumber_t serial_number, PixLib::ScanInfo_t &scan_info) {

    std::string file_name = PixA::RootResultFileAccessBase::fileName( scan_info.file(), scan_info.alternateFile() );
    if (!file_name.empty()) {
      addFile(measurement_type, serial_number, file_name);
      return;
    }
    std::cout << "ERROR [ScanFileHistogramProvider::addScan] File not found for scan " 
	      << makeSerialNumberString(measurement_type, serial_number)
	      << std::endl;
  }

}
