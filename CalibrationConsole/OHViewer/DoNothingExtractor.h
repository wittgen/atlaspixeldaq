/* Dear emacs, this is -*-c++-*- */
#ifndef _PixCon_DoNothingExtractor_h_
#define _PixCon_DoNothingExtractor_h_

#include "QIsInfoEvent.h"

namespace PixCon {

  class DoNothingExtractor : public IVarNameExtractor
  {
  public:

    DoNothingExtractor(const std::string &var_name) 
      : m_varName( &var_name )
    {
    }


    void extractVar(const std::string &connectivity_name, ConnVar_t &conn_var) const
    {
      conn_var.connName() = connectivity_name;
      conn_var.varName()= *m_varName;
    }

  private:
    const std::string              *m_varName;
  };

}
#endif
