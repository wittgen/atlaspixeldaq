#include "MetaHistogramProvider.h"
#include "ScanFileHistogramProvider.h"
#include <ScanInfo_t.hh>
#include <PixDbGlobalMutex.hh>

namespace PixCon {

  MetaHistogramProvider::MetaHistogramProvider(std::shared_ptr<IHistogramProvider> oh_provider,
					       std::shared_ptr<IHistogramProvider> file_histo_provider) 
  {
    m_histogramProvider[0]=oh_provider;
    m_histogramProvider[1]=file_histo_provider;
  }

  MetaHistogramProvider::~MetaHistogramProvider() {}

    /** Load a histogram from the server.
     * @param type the type of the measurement : scan, analysis or current.
     * @parem serial_number the serial number of the scan.
     * @param conn_item_type the connectivity item type of the histogram.
     * @param connectivity_name connectivity name of the module.
     * @param histo_name name of the histogram.
     * @param index vector which contains the indices for the pix scan histo.
     */
  HistoPtr_t MetaHistogramProvider::loadHistogram(EMeasurementType measurement_type,
						  SerialNumber_t serial_number,
						  EConnItemType conn_item_type,
						  const std::string &connectivity_name,
						  const std::string &histo_name,
						  const Index_t &index) {

    std::map<SerialNumber_t, EPreferredProvider>::iterator preferred_provider_iter = preferredProvider(measurement_type, serial_number);
    for (unsigned int provider_i=0; provider_i<2; provider_i++) {
      if (tryProvider(measurement_type, preferred_provider_iter, provider_i)) {
	HistoPtr_t histo_ptr = m_histogramProvider[provider_i]->loadHistogram(measurement_type,
									      serial_number,
									      conn_item_type,
									      connectivity_name,
									      histo_name,
									      index);
	if (histo_ptr) {
	  return histo_ptr;
	}
      }
    }
    return NULL;
  }

  /** Request the names of all histograms assigned to the given serial number and connectivity object type.
   * @param type the type of the measurement : scan, analysis or current.
   * @param serial_number the serial number of the scan.
   * @param conn_item_type the type of the connectivity item for which the histogram list should be requested.
   * @param histogram_name_list vector which will be extended by the histogram names associated to the serial number and object type.
   * Note, it is the responsibility of the caller to clear the given histogram name list before the call if the old content is not desired.
   * This allows to add histograms of different types in subsequent calls.
   * @sa EConnItemType
   */
  void MetaHistogramProvider::requestHistogramList(EMeasurementType measurement_type,
						   SerialNumber_t serial_number,
						   EConnItemType conn_item_type,
						   std::vector< std::string > &histogram_name_list ) {

    std::map<SerialNumber_t, EPreferredProvider>::iterator preferred_provider_iter = preferredProvider(measurement_type, serial_number);
    unsigned int old_size = histogram_name_list.size();
    for (unsigned int provider_i=0; provider_i<2; provider_i++) {
      if (tryProvider(measurement_type, preferred_provider_iter, provider_i)) {
	m_histogramProvider[provider_i]->requestHistogramList(measurement_type,
							      serial_number,
							      conn_item_type,
							      histogram_name_list);
	if (old_size == histogram_name_list.size()) {
	  disable( measurement_type, preferred_provider_iter, provider_i);
	}
	else {
	  return;
	}
      }
    }

  }

  void MetaHistogramProvider::requestHistogramList(EMeasurementType measurement_type,
						   SerialNumber_t serial_number,
						   EConnItemType conn_item_type,
						   std::vector< std::string > &histogram_name_list,
						   const std::string &rod_name) {

    std::map<SerialNumber_t, EPreferredProvider>::iterator preferred_provider_iter = preferredProvider(measurement_type, serial_number);
    unsigned int old_size = histogram_name_list.size();
    for (unsigned int provider_i=0; provider_i<2; provider_i++) {
      if (tryProvider(measurement_type, preferred_provider_iter, provider_i)) {
	m_histogramProvider[provider_i]->requestHistogramList(measurement_type,
							      serial_number,
							      conn_item_type,
							      histogram_name_list,
							      rod_name);
	if (old_size == histogram_name_list.size()) {
	  disable( measurement_type, preferred_provider_iter, provider_i);
	}
	else {
	  return;
	}
      }
    }

  }


  /** Return the dimensions of the pix scan histo levels.
   * @param type the type of the measurement : scan, analysis or current.
   * @parem serial_number the serial number of the scan.
   * @param conn_item_type the connectivity item type of the histogram.
   * @param connectivity_name connectivity name of the module.
   * @param histo_name name of the histogram.
   * @param index vector which will be filled with the dimensions of the pix scan histo levels.
   */
  void MetaHistogramProvider::numberOfHistograms(EMeasurementType measurement_type,
						 SerialNumber_t serial_number,
						 EConnItemType conn_item_type,
						 const std::string &connectivity_name,
						 const std::string &histo_name,
						 HistoInfo_t &histo_info) {

    std::map<SerialNumber_t, EPreferredProvider>::iterator preferred_provider_iter = preferredProvider(measurement_type, serial_number);

    for (unsigned int provider_i=0; provider_i<2; provider_i++) {
      if (tryProvider(measurement_type, preferred_provider_iter, provider_i)) {

	m_histogramProvider[provider_i]->numberOfHistograms(measurement_type,
							    serial_number,
							    conn_item_type,
							    connectivity_name,
							    histo_name,
							    histo_info);

	if (histo_info.minHistoIndex() < histo_info.maxHistoIndex()) {
	  return;
	}
      }
    }
  }

  /** Add a new root file to provide histograms for the given measurement.
   */
  void MetaHistogramProvider::addScan(EMeasurementType measurement, SerialNumber_t serial_number, PixLib::ScanInfo_t &scan_info) {
    //#ibp
    //Lock globalLock(CAN::PixDbGlobalMutex::mutex());
    //#!ibp
    ScanFileHistogramProvider *scan_file_histogram_provider = dynamic_cast<ScanFileHistogramProvider *>(m_histogramProvider[1].get());
    if (scan_file_histogram_provider) {
      if (scan_info.status()!=CAN::kNotSet && ( !scan_info.file().empty() || !scan_info.alternateFile().empty())) {
	scan_file_histogram_provider->addScan(measurement, serial_number, scan_info);
	m_preferredProvider[measurement][serial_number]=( m_histogramProvider[0].get() ? kBoth : kFileBasedOnly);
      }
    }
    else {
      m_preferredProvider[measurement][serial_number]=( m_histogramProvider[0].get() ? kOHOnly : kNone);
    }
  }

  /** Add a new root file to provide histograms for the given measurement.
   * @param measurement the measurement e.g. kScan, kAnalysis
   * @param serial_number the serial number of the measurement
   * @param file_name the file name in case the measurement has finished otherwise an empty file_name.
   */
  void MetaHistogramProvider::addScan(EMeasurementType measurement, SerialNumber_t serial_number, const std::string &file_name) {
    RootFileHistogramProvider *root_file_histogram_provider = dynamic_cast<RootFileHistogramProvider *>(m_histogramProvider[1].get());
    if (root_file_histogram_provider) {
      if (!file_name.empty()) {
	root_file_histogram_provider->addFile(measurement, serial_number, file_name);
	m_preferredProvider[measurement][serial_number]=( m_histogramProvider[0].get() ? kBoth : kFileBasedOnly);
      }
    }
    else {
      m_preferredProvider[measurement][serial_number]=( m_histogramProvider[0].get() ? kOHOnly : kNone);
    }
  }

  /** Remove the cached information for the given measurement.
   */
  void MetaHistogramProvider::clearCache(EMeasurementType measurement, SerialNumber_t serial_number) {
    for (unsigned int provider_i=0; provider_i<2; provider_i++) {
      if (m_histogramProvider[provider_i]) {
	m_histogramProvider[provider_i]->clearCache(measurement, serial_number);
      }
    }
    std::map<SerialNumber_t, EPreferredProvider>::iterator provider_iter = m_preferredProvider[measurement].find(serial_number);
    if (provider_iter != m_preferredProvider[measurement].end()) {
      m_preferredProvider[measurement].erase(provider_iter);
    }

  }

  void MetaHistogramProvider::reset() {
    for (unsigned int option_i=0; option_i<2; ++option_i) {
      if (m_histogramProvider[option_i]) {
	m_histogramProvider[option_i]->reset();
      }
    }
    for (unsigned int measurement_i=0; measurement_i<kNMeasurements; ++measurement_i) {
      m_preferredProvider[measurement_i].clear();
    }
  }
}
