#include "ui_ScanSerialNumberDialogBase.h"
#include <Highlighter.h>
#include <CalibrationDataTypes.h>

#include <memory>

namespace PixCon {

  class ScanSerialNumberDialog : public QDialog, public Ui_ScanSerialNumberDialogBase
  {    Q_OBJECT
  public:

    ScanSerialNumberDialog ( QWidget* parent = 0 )
     : QDialog(parent),	m_serialNumber(0) 
    {setupUi(this);}

    //    ~ScanSerialNumberDialog() {}
    SerialNumber_t serialNumber()      const { return m_serialNumber; }
    EMeasurementType measurementType() const { return m_measurementType; }

public slots:
    void verifySerialNumberAndAccept();

  private:
    std::unique_ptr< Highlighter > m_highlighter;
    SerialNumber_t m_serialNumber;
    EMeasurementType m_measurementType;
  };

}
