/* Dear emacs, this is -*-c++-*- */

#ifndef _PixCon_SharedObjectConfigDbInstance_h_
#define _PixCon_SharedObjectConfigDbInstance_h_

#include <ObjectConfigDbInstance.hh>
#include <memory>

namespace PixCon {

  class SharedObjectConfigDbInstance
  {
  public:
    static std::shared_ptr<CAN::IConfigDb> instance() {
      if (!s_instance.get()) {
	s_instance = std::shared_ptr<CAN::IConfigDb>( CAN::ObjectConfigDbInstance::create());
      }
      return s_instance;
    }

  private:
    static std::shared_ptr<CAN::IConfigDb> s_instance;
  };

}

#endif
