#ifndef _PixCon_CoralClientMetaDataUpdateHelper_h_
#define _PixCon_CoralClientMetaDataUpdateHelper_h_

#include <string>
#include <IMetaDataUpdateHelper.h>
#include <memory>

namespace PixCon {

  class CalibrationDataManager;

  class CoralClientMetaDataUpdateHelper : public IMetaDataUpdateHelper
  {
  public:
    CoralClientMetaDataUpdateHelper(const std::shared_ptr<CalibrationDataManager> &calibration_data)
      : m_calibrationData(calibration_data)
    {}

    ~CoralClientMetaDataUpdateHelper();

    void update(EMeasurementType measurement_type,
		SerialNumber_t serial_number,
		int new_quality,
		const std::string &new_comment,
		unsigned short mask);

  private:
    std::shared_ptr<CalibrationDataManager> m_calibrationData;
  };
}
#endif
