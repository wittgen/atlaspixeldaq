#include "AnalysisStatusMonitor.h"
#include "OHHistogramProviderBase.h"
#include <HistogramStatus.h>
#include <HistogramCache.h>
#include <ConfigWrapper/Connectivity.h>
#include <ConfigWrapper/ConnectivityUtil.h>
#include <CalibrationDataManager.h>
#include <Status.hh>
#include <PixelCoralClientUtils/AnalysisResultList_t.hh>
#include <PixResultsDbClient.hh>
#include <ConfigWrapper/Regex_t.h>
#include "QNewVarEvent.h"
#include <DoNothingExtractor.h>
#include <DefaultColourCode.h>
#include "StatusChangeTransceiver.h"

#include <IsReceptor.h>

namespace PixCon {

  const double AnalysisStatusMonitor::m_connectionTimeOut = 10;
  const double AnalysisStatusMonitor::m_processingDelay = 12;

  int AnalysisStatusMonitor::translateStatusName(const std::string &status_name) {
    return static_cast<int>( CAN::AnalysisStatus::status(status_name) );
  }

  const std::string AnalysisStatusMonitor::s_histogramCategoryName("General");

  std::vector<StatusMonitor::EStatus> AnalysisStatusMonitor::s_convertStatus;

  class AnalysisResultListener : public IPixResultsDbListener {
  public:
    AnalysisResultListener(const std::shared_ptr<IsReceptor> &receptor,
			   const std::string &calibration_data_status_name,
			   const std::string &calibration_data_status_category_name)
      : m_receptor(receptor),
	m_analysisSerialNumber(0),
	m_varList(NULL),
	m_varDefList( VarDefList::instance()),
	m_calibrationDataStatusVarName(calibration_data_status_name),
	m_calibrationDataStatusCategoryName(calibration_data_status_category_name)
    {
      m_pp0Pattern.reserve(4);
      m_pp0Pattern.push_back(new PixA::Regex_t("L[012]_B[0-9][0-9]_S[12]_[AC][67]"));
      m_pp0Pattern.push_back(new PixA::Regex_t("LX_B[0-9][0-9]_S[12]_[AC][67]"));
      m_pp0Pattern.push_back(new PixA::Regex_t("LI_S[0-9][0-9]_[AC]"));
      m_pp0Pattern.push_back(new PixA::Regex_t("D[123][AC]_B[0-9][0-9]_S[12]"));
      m_modulePattern.reserve(7);
      m_modulePattern.push_back(new PixA::Regex_t("L[012]_B[0-9][0-9]_S[12]_[AC][67]_M[1-6][AC]"));
      m_modulePattern.push_back(new PixA::Regex_t("L[012]_B[0-9][0-9]_S[12]_[AC][7]_M0"));
      m_modulePattern.push_back(new PixA::Regex_t("LX_B[0-9][0-9]_S[12]_[AC][67]_M[1-6][AC]"));
      m_modulePattern.push_back(new PixA::Regex_t("LX_B[0-9][0-9]_S[12]_[AC][7]_M0"));
      m_modulePattern.push_back(new PixA::Regex_t("LI_S[0-9][0-9]_[AC]_M[1-4]_[AC][1-8]"));
      m_modulePattern.push_back(new PixA::Regex_t("LI_S[0-9][0-9]_[AC]_[13][24]_M[1-4]_[AC]([1-9]|1[0-2])"));
      m_modulePattern.push_back(new PixA::Regex_t("D[123][AC]_B[0-9][0-9]_S[12]_M[1-6]"));
    }

    ~AnalysisResultListener() {
      for (std::vector<PixA::Regex_t *>::iterator pattern_iter = m_pp0Pattern.begin();
	   pattern_iter != m_pp0Pattern.end();
	   pattern_iter++) {
	delete *pattern_iter;
	*pattern_iter = NULL;
      }
      m_pp0Pattern.clear();
      for (std::vector<PixA::Regex_t *>::iterator pattern_iter = m_modulePattern.begin();
	   pattern_iter != m_modulePattern.end();
	   pattern_iter++) {
	delete *pattern_iter;
	*pattern_iter = NULL;
      }
      m_modulePattern.clear();
    }

    void setMeasurement(SerialNumber_t analysis_serial_number, std::set<std::string> &var_list) {
      m_analysisSerialNumber = analysis_serial_number;
      Lock lock(m_varListMutex);
      m_varList = &var_list;
    }

    void newValue(const std::string &var_name_orig, const std::string &conn_name, bool value) {
      std::string var_name=makeCanVarName(var_name_orig);
      EValueConnType conn_type = connType(conn_name);
      VarDef_t var_def = varDef(var_name, conn_type);
      DoNothingExtractor extractor(var_name);
      if (!var_def.isValid()) {
	// @todo read definition from database

	var_def = m_varDefList->createStateVarDef(var_name, conn_type,2);
	StateVarDef_t &a_state_def = var_def.stateDef();
	a_state_def.addState(static_cast<State_t>(true),DefaultColourCode::kSuccess,"true");
	a_state_def.addState(static_cast<State_t>(false),DefaultColourCode::kFailure,"false");
	newVariable(var_name);
      }
      addVariable(var_name);
      receptor()->setValue(conn_type, kAnalysis, m_analysisSerialNumber, conn_name, extractor, static_cast<State_t>(value));
    }

    void newValue(const std::string &var_name_orig, const std::string &conn_name, unsigned int value) {

      if (var_name_orig.find("number")) {
	newValue(var_name_orig, conn_name, static_cast<float>(value));
	return;
      }
      std::string var_name=makeCanVarName(var_name_orig);
      EValueConnType conn_type = connType(conn_name);
      VarDef_t var_def = varDef(var_name, conn_type);
      DoNothingExtractor extractor(var_name);
      if (!var_def.isValid()) {
	// @todo read definition from database

	/*var_def = */ m_varDefList->createStateVarDef(var_name, conn_type,3);
	/*StateVarDef_t &a_state_def = var_def.stateDef(); */
	newVariable(var_name);
      }
      addVariable(var_name);
      receptor()->setValue(conn_type, kAnalysis, m_analysisSerialNumber, conn_name, extractor, static_cast<State_t>(value));
    }

    void newValue(const std::string &var_name_orig, const std::string &conn_name, float value) {
      std::string var_name=makeCanVarName(var_name_orig);
      EValueConnType conn_type = connType(conn_name);
      VarDef_t var_def = varDef(var_name, conn_type);
      DoNothingExtractor extractor(var_name);
      if (!var_def.isValid()) {
	// @todo read definition from database

	/*ValueVarDef_t &a_value_def =*/ m_varDefList->createValueVarDef(var_name, conn_type,value,value);
	newVariable(var_name);
      }
      else {
	setMinMax(var_def,value);
      }
      addVariable(var_name);

      receptor()->setValue(conn_type, kAnalysis, m_analysisSerialNumber, conn_name, extractor, value);
    }

    void newValue(const std::string &var_name_orig, const std::string &conn_name, const CAN::AverageResult_t &value) {
      std::string var_name=makeCanVarName(var_name_orig);
      std::string full_var_name;
      assert( CAN::AverageResult_t::kNValues == 4) ;
      EValueConnType conn_type = connType(conn_name);
      for (unsigned int value_i=0; value_i< CAN::AverageResult_t::kNValues; value_i++) {
	full_var_name=var_name;
	full_var_name += s_nameExt[value_i];

	DoNothingExtractor extractor(full_var_name);

	VarDef_t var_def = varDef(full_var_name,conn_type);
	float a_value = value.value(value_i);

	if (!var_def.isValid()) {
	  // @todo read definition from database

	  /*ValueVarDef_t &a_value_def =*/ m_varDefList->createValueVarDef(full_var_name, conn_type,a_value,a_value);
	  newVariable(full_var_name);
	}
	else {
	  setMinMax(var_def,a_value);
	}
	addVariable(full_var_name);

	receptor()->setValue(conn_type, kAnalysis, m_analysisSerialNumber, conn_name, extractor, a_value);
      }

      {
	full_var_name=var_name;
	full_var_name += s_nameExt[4];
	VarDef_t var_def = varDef(full_var_name, conn_type);
	DoNothingExtractor extractor(full_var_name);
	float a_value = value.n();
	if (!var_def.isValid()) {
	  // @todo read definition from database

	  /*var_def =*/ m_varDefList->createValueVarDef(full_var_name, conn_type,a_value,a_value);
	  //	ValueVarDef_t &a_value_def = var_def.ValueDef();
	  newVariable(full_var_name);
	}
	else {
	  setMinMax(var_def,a_value);
	}
	addVariable(full_var_name);

	receptor()->setValue(conn_type, kAnalysis, m_analysisSerialNumber, conn_name, extractor, a_value);
      }

    }
    void newValue(const std::string &var_name_orig, const std::string &conn_name, const PixelMap_t &special_pixels) {
      std::string var_name=makeCanVarName(var_name_orig);
      EValueConnType conn_type = connType(conn_name);
      VarDef_t var_def = varDef(var_name, conn_type);
      DoNothingExtractor extractor(var_name);
      float value = special_pixels.size();
      unsigned int bad_pixels=0;
      for (PixelMap_t::const_iterator pixel_iter = special_pixels.begin();
	   pixel_iter != special_pixels.end();
	   ++pixel_iter) {
	
	if (pixel_iter->first.first<0 || pixel_iter->first.first>=18*8 
	    || pixel_iter->first.second<0 || pixel_iter->first.second>2*160) {
	  std::cerr << "WARNING [AnalysisResultListener::newValue] PixelMap_t contains invalid column or row : "
		    << pixel_iter->first.first << ", " << pixel_iter->first.second << " = " << pixel_iter->second
		    << std::endl;
	}
	else {
	  if (pixel_iter->second>0) {
	    bad_pixels++;
	  }
	}
	    
      }
      if (bad_pixels>0) {
	value = bad_pixels;
      }
      
      if (!var_def.isValid()) {
	// @todo read definition from database

	/*ValueVarDef_t &a_value_def =*/ m_varDefList->createValueVarDef(var_name, conn_type,value,value);
	newVariable(var_name);
      }
      else {
	setMinMax(var_def,value);
      }
      addVariable(var_name);

      receptor()->setValue(conn_type, kAnalysis, m_analysisSerialNumber, conn_name, extractor, value);
    }
  protected:
    VarDef_t varDef(const std::string &var_name, EValueConnType conn_type) {
      try {
	VarDef_t var_def( m_varDefList->getVarDef(var_name) );
	if (var_def.connType() != conn_type) {
	  var_def.setAllConnType();
	}
	return var_def;
      }
      catch(UndefinedCalibrationVariable &err) {
	return VarDef_t::invalid();
      }
    }

    std::string makeCanVarName(const std::string &var_name_orig) {
      if (var_name_orig.empty() || var_name_orig[0]!='_') {
        return std::string("CAN_")+var_name_orig;
      }
      else {
        return std::string("_CAN")+var_name_orig;
      }
    }

    void newVariable(const std::string &/*var_name*/) {
    }

    void addVariable(const std::string &var_name) {
      assert(m_varList);
      std::pair<std::set<std::string>::iterator, bool> ret;
      {
	Lock lock (m_varListMutex);
	ret = m_varList->insert(var_name);
      }
      if (ret.second) {

	m_receptor->notify(new QNewVarEvent(kAnalysis,
					    m_analysisSerialNumber, 
					    (var_name != m_calibrationDataStatusVarName ? s_categoryName : m_calibrationDataStatusCategoryName),
					    var_name) );
      }
    }


    void setMinMax(VarDef_t &var_def, float value) {
      if (var_def.isValueWithOrWithoutHistory()) {
	ValueVarDef_t &value_def = var_def.valueDef();
	if (value>value_def.maxVal()) {
	  value_def.setMinMax(value_def.minVal(),value);
	}
	else if (value < value_def.minVal()) {
	  value_def.setMinMax(value,value_def.maxVal());
	}
      }
    }

    EValueConnType connType( std::string conn_name) {
      //std::string::size_type pos=conn_name.rfind('_');
      for (std::vector<PixA::Regex_t *>::const_iterator pattern_iter = m_modulePattern.begin();
	   pattern_iter != m_modulePattern.end();
	   pattern_iter++) {
	if ((*pattern_iter)->match(conn_name)) return kModuleValue;
      }
      for (std::vector<PixA::Regex_t *>::const_iterator pattern_iter = m_pp0Pattern.begin();
	   pattern_iter != m_pp0Pattern.end();
	   pattern_iter++) {
	if ((*pattern_iter)->match(conn_name)) {
	  // @todo kPp0Value;
	  return kRodValue;
	}
      }
      return kRodValue;
    }

    std::shared_ptr<IsReceptor> &receptor()             { return m_receptor; }
    const std::shared_ptr<IsReceptor> &receptor() const { return m_receptor; }

    /** Notify the application about new variables.
     */
    void notify(QNewVarEvent *event) {
      m_receptor->notify(event);
    }

  private:
    std::shared_ptr<IsReceptor> m_receptor;
    SerialNumber_t                m_analysisSerialNumber;
    Mutex                         m_varListMutex;
    std::set<std::string>        *m_varList;
    VarDefList                   *m_varDefList;
    std::vector<PixA::Regex_t *>       m_pp0Pattern;
    std::vector<PixA::Regex_t *>       m_modulePattern;
    static const char *s_nameExt[5];
    static const std::string      s_categoryName;
    const std::string             m_calibrationDataStatusVarName;
    const std::string             m_calibrationDataStatusCategoryName;

  };

  const std::string AnalysisResultListener::s_categoryName("Results");


  AnalysisStatusMonitor::AnalysisStatusMonitor(OHHistogramProviderBase &histogram_provider,
					       const std::shared_ptr<PixCon::HistogramCache> &histogram_cache,
					       std::shared_ptr<IsReceptor> &receptor,
					       const std::shared_ptr<IsMonitorManager> &is_monitor_manager,
					       EMeasurementType measurement_type,
					       const std::string &var_name,
					       const std::string &global_var_name,
					       const std::string &calibration_data_var_name,
					       const std::shared_ptr<CalibrationDataManager> &calibration_data,
					       int status_threshold,
					       std::shared_ptr<StatusChangeTransceiver> &status_change_transceiver)
    : StatusMonitor(receptor,
		    is_monitor_manager,
		    measurement_type,
		    var_name,
		    global_var_name,
		    calibration_data_var_name,
		    calibration_data,
		    status_threshold,
		    &translateStatusName,
		    status_change_transceiver),
      m_statusVarId(VarDefBase_t::invalid().id()),
      m_listener(new AnalysisResultListener(receptor, calibrationDataVarName(), statusCategoryName())),
      m_histoProvider(&histogram_provider),
      m_histogramCache(histogram_cache),
      m_varId(VarDefBase_t::invalid().id())
  {
    const std::string status_var_name(calibration_data_var_name);
    try {
      const VarDef_t var_def( VarDefList::instance()->getVarDef(status_var_name) );
      if ( !var_def.isStateWithOrWithoutHistory() ) {
	std::cout << "ERROR [AnalysisStatusMonitor::ctor] variable " << status_var_name << " already exists but it is not a state variable." << std::endl;
      }
      else {
	m_statusVarId=var_def.id();
      }
    }
    catch (UndefinedCalibrationVariable &) {
      // @todo ensure that kAborted is the last state
      VarDef_t var_def( VarDefList::instance()->createStateVarDef(status_var_name, kRodValue, CAN::AnalysisStatus::kAborted+1) );
      if ( var_def.isStateWithOrWithoutHistory() ) {
	StateVarDef_t &state_var_def = var_def.stateDef();

	bool in_list=true;
	unsigned short colour_map[]={DefaultColourCode::kUnknown,        //unknown    ->light yellow
				     13,                                 //submitted  ->pale brown
				     DefaultColourCode::kOn,             //pending    ->orange
				     DefaultColourCode::kConfigured,     //waiting    ->blue
				     DefaultColourCode::kAnalysing,      //ready      ->cyan
				     DefaultColourCode::kScanning,       //running    ->green
				     DefaultColourCode::kSuccess,        //success
				     DefaultColourCode::kFailure,        //failure
				     DefaultColourCode::kCloseToFailure, //timeout
				     DefaultColourCode::kAlarm,          //aborted
				     USHRT_MAX};
	for (unsigned short state_i=0; state_i<=CAN::AnalysisStatus::kAborted; state_i++) {
	  if (in_list && colour_map[state_i]>=USHRT_MAX) {
	    in_list=false;
	  }
	  state_var_def.addState(state_i,(in_list ? colour_map[state_i] : state_i ),
				 CAN::AnalysisStatus::name( static_cast<CAN::AnalysisStatus::EStatus>(state_i) ));
	}
	m_statusVarId=var_def.id();
      }
      else {
	std::cout << "ERROR [AnalysisStatusMonitor::ctor] variable " << status_var_name << " already exists but it is not a state variable." << std::endl;
      }
    }

    m_varId = HistogramStatus::defineVar().id();
  }


  AnalysisStatusMonitor::~AnalysisStatusMonitor() {}

  void AnalysisStatusMonitor::timeout(double elapsed_seconds)
  {
    bool has_requests;
    {
      Lock lock(m_processingRequestMutex);
      has_requests = !m_processingRequests.empty();
    }

    if (has_requests) {
      processRequests();
    }
    else {
      if (m_analysisResultDb.get() && elapsed_seconds > m_connectionTimeOut) {
	// close connection after a certain time.
	m_analysisResultDb.reset();
      }
    }

  }

//   void AnalysisStatusMonitor::abortMonitoring(SerialNumber_t serial_number) {
//     {
//       Lock lock(m_processingRequestMutex);
//       std::map<SerialNumber_t, ProcessingRequest_t>::iterator serial_number_iter = m_processingRequests.find(serial_number);
//       if (serial_number_iter != m_processingRequests.end()) {
// 	serial_number_iter->second.setFinished();
//       }
//     }
//     processRequests();
//     StatusMonitor::abortMonitoring(serial_number);
//   }

  const char *AnalysisResultListener::s_nameExt[5]={"_mean","_rms","_lower99","_upper99","_n"};

  StatusMonitor::EStatus AnalysisStatusMonitor::process(SerialNumber_t serial_number, const std::string &rod_name, std::set<std::string> &var_list)
  {
    if (m_verbose) {
      std::cout << "INFO [AnalysisStatusMonitor::process] Process " << rod_name << makeSerialNumberString(measurementType(), serial_number)
		<< "." << std::endl;
    }
    StatusMonitor::EStatus rod_status = process(serial_number, rod_name);

    std::string var_name_histograms =  VarDefList::instance()->varName(m_varId);
    std::pair< std::set<std::string>::iterator, bool> ret = var_list.insert(var_name_histograms);
    if (ret.second) {
      announceNewVaiable( serial_number, s_histogramCategoryName, var_name_histograms );
    }

    {
      Lock lock(m_processingRequestMutex);
      m_processingRequests[serial_number].addRequest(rod_name);
    }

    try {
      Lock lock(calibrationData()->calibrationDataMutex());
      const CalibrationData::ConnObjDataList conn_obj_data ( calibrationData()->calibrationData().getConnObjectData( rod_name ));
      if (conn_obj_data.enableState(measurementType(), serial_number).enabled()) {
	CAN::AnalysisStatus::EStatus analysis_status = static_cast<CAN::AnalysisStatus::EStatus>( conn_obj_data.value<State_t>(measurementType(),
															       serial_number, m_statusVarId)) ;
	if (analysis_status == CAN::AnalysisStatus::kSuccess) {
	  rod_status = StatusMonitor::kOk;
	}
	else if (analysis_status > CAN::AnalysisStatus::kSuccess) {
	  rod_status = StatusMonitor::kFailed;
	}
      }
    }
    catch (CalibrationData::MissingValue &) {
    }
    processRequests();

    return rod_status;
  }

  //giant thing inserted by peter from scanstatusmonitor
  StatusMonitor::EStatus AnalysisStatusMonitor::process(SerialNumber_t serial_number, const std::string &rod_name)
  {
    EStatus rod_status = StatusMonitor::kUnset;
    if (m_verbose) {
      std::cout << "INFO [AnalysisStatusMonitor::process] serial number = " << serial_number << " ROD=" << rod_name << std::endl;
    }

    std::string var_name_histograms =  VarDefList::instance()->varName(m_varId);
    DoNothingExtractor extractor( var_name_histograms );

    {
    std::shared_ptr< std::vector<std::string> > histogram_names_ptr;
    // get per ROD histograms, count histograms, and set histogram status in calibration data container
    {
      m_histoProvider->needToUpdate(measurementType(), serial_number, rod_name);

      if (m_verbose) {
	std::cout << "INFO [AnalysisStatusMonitor::process]  request histogram list from provider for "
		  <<  makeSerialNumberString(measurementType(),serial_number) << "." << std::endl;
      }
      //      m_histoProvider->requestHistogramList(measurementType(), serial_number, kRodItem, histogram_names, rod_name);
      histogram_names_ptr = m_histogramCache->updateHistogramList(measurementType(), serial_number, kRodItem, rod_name);
      const std::vector<std::string> &histogram_names( *histogram_names_ptr );
      unsigned int n_histos=0;
      for (std::vector<std::string>::const_iterator histo_iter = histogram_names.begin();
	   histo_iter != histogram_names.end();
	   ++histo_iter) {

	std::cout<<"histo_iter is pointing at "<<(*histo_iter)<<std::endl;
	HistoInfo_t histo_info;
	unsigned int histo_id = m_histogramCache->histogramIndex(measurementType(), serial_number, kRodItem, *histo_iter );

	m_histogramCache->numberOfHistograms(measurementType(), serial_number, kRodItem,  rod_name, histo_id, histo_info, true /* refresh */ );
	if (histo_info.minHistoIndex()<histo_info.maxHistoIndex()) {
	  n_histos++;
	}
      }

      // also need to clear the cache
      //       m_histogramCache->clearCache(measurementType(), serial_number);
      //       if (m_verbose) {
      // 	std::cout << "INFO [ScanStautsMonitaor::process]  Cleared cache for " <<  makeSerialNumberString(measurementType(),serial_number) << "." << std::endl;
      //       }

      State_t histogram_number_state = HistogramStatus::state(histogram_names.size(), n_histos);

//       Lock lock(calibrationData()->calibrationDataMutex());
//       calibrationData()->calibrationData().addValue(kRodValue,
// 						    rod_name,
// 						    measurementType(),
// 						    serial_number,
// 						    VarDefList::instance()->varName(m_varId),
// 						    histogram_number_state);

//       QIsInfoEvent *event=new QIsInfoEvent(rod_name, VarDefList::instance()->varName(m_varId), kRodItem, measurementType(), serial_number);

      bool update = true;
      try {
	const CalibrationData::ConnObjDataList conn_obj_data ( calibrationData()->calibrationData().getConnObjectData( rod_name ) );
	if (conn_obj_data.enableState(measurementType(), serial_number).enabled()) {
	  if (histogram_number_state != HistogramStatus::kOk && histogram_number_state  != HistogramStatus::kNoneExpected) {
	    //	    rod_status = StatusMonitor::kFailed;
	    rod_status = combineStatus(rod_status,StatusMonitor::kFailed);
	  }
	  else {
	    rod_status = combineStatus(rod_status,StatusMonitor::kOk);
	  }

	  try {
	    State_t scan_state = conn_obj_data.value<State_t>(measurementType(), serial_number, m_statusVarId) ;
	    if ( scan_state ) {
	      if (scan_state < s_convertStatus.size()) {
		rod_status = combineStatus(rod_status, s_convertStatus[scan_state]);
	      }
	      else {
		rod_status = combineStatus(rod_status, StatusMonitor::kUnknown);
	      }
	    }
	  }
	  catch(CalibrationData::MissingValue &) {
	  }

	}
	try {
	  State_t old_histogram_number_state = conn_obj_data.value<State_t>(measurementType(), serial_number, m_varId) ;
	  if ( old_histogram_number_state == histogram_number_state ) {
	    update=false;
	  }
	}
	catch(CalibrationData::MissingValue &) {
	}
	catch(...){
	}

      }
      catch(CalibrationData::MissingConnObject &) {
	// should not happen
	std::cerr << "ERROR [AnalysisStatusMonitor::process] connectivity object " <<  rod_name 
		  << " does not exist for " << makeSerialNumberString(measurementType(),serial_number) << "." << std::endl;
	//	if (rod_status == StatusMonitor::kOk ) {
	rod_status = combineStatus(rod_status,StatusMonitor::kUnknown);
	//	}
      }

      if (update) {
      receptor()->setValue(kRodValue, measurementType(), serial_number, rod_name, extractor, histogram_number_state);
      }
    }
    }

    {
    std::shared_ptr< std::vector<std::string> > histogram_names_ptr;
    // get module histogram list.
    //    m_histoProvider->requestHistogramList(measurementType(), serial_number, kModuleItem, histogram_names);
    histogram_names_ptr = m_histogramCache->updateHistogramList(measurementType(), serial_number, kModuleItem, rod_name);

    // count histograms and set status per module.
    PixA::ConnectivityRef conn = calibrationData()->analysisConnectivity(serial_number);

    //bool enabled=false;
    {
    Lock lock(calibrationData()->calibrationDataMutex());
    // disabled rods should not be considered.
    // but ignore them anyway.
/*
    try {
      //const CalibrationData::ConnObjDataList conn_obj_data ( calibrationData()->calibrationData().getConnObjectData(rod_name) );
      //enabled = conn_obj_data.enableState(measurementType(), serial_number).enabled();
    }
    catch(CalibrationData::MissingConnObject &) {
      // should not happen
    }
    */
    }

    const std::vector<std::string> &histogram_names( *histogram_names_ptr);
    std::vector<unsigned int> histo_id_list;
    histo_id_list.reserve( histogram_names.size() );
    for (std::vector<std::string>::const_iterator histo_iter = histogram_names.begin();
	 histo_iter != histogram_names.end();
	 ++histo_iter) {
      //      std::cout<<"AnalysisStatusMonitor is looking at: "<<(*histo_iter)<<std::endl;
      histo_id_list.push_back(m_histogramCache->histogramIndex(measurementType(), serial_number, kModuleItem, *histo_iter ) );
    }

    try {
    PixA::Pp0List pp0_list = conn.pp0s(rod_name);
    PixA::Pp0List::iterator pp0_begin = pp0_list.begin();
    PixA::Pp0List::iterator pp0_end = pp0_list.end();
    for (PixA::Pp0List::iterator pp0_iter = pp0_begin; pp0_iter != pp0_end; ++pp0_iter) {
      PixA::ModuleList::iterator module_begin = PixA::modulesBegin(pp0_iter);
      PixA::ModuleList::iterator module_end = PixA::modulesEnd(pp0_iter);
      for (PixA::ModuleList::iterator module_iter = module_begin; module_iter != module_end; ++module_iter) {
	unsigned int n_histos=0;
	for (std::vector<unsigned int>::const_iterator histo_id_iter = histo_id_list.begin();
	     histo_id_iter != histo_id_list.end();
	     ++histo_id_iter) {
	  HistoInfo_t histo_info;
	  m_histogramCache->numberOfHistograms(measurementType(), serial_number, kModuleItem,  PixA::connectivityName(module_iter), *histo_id_iter, histo_info , true);
	  if (histo_info.minHistoIndex()<histo_info.maxHistoIndex()) {
	    n_histos++;
	  }
	}
	State_t histogram_number_state = HistogramStatus::state(histogram_names.size(), n_histos);


	bool update = true;
	try {
	  {
	    Lock lock(calibrationData()->calibrationDataMutex());
	  const CalibrationData::ConnObjDataList conn_obj_data ( calibrationData()->calibrationData().getConnObjectData( PixA::connectivityName(module_iter) ) );
	  if (conn_obj_data.enableState(measurementType(), serial_number).enabled()) {
	    if (histogram_number_state != HistogramStatus::kOk && histogram_number_state  != HistogramStatus::kNoneExpected) {
	      rod_status = combineStatus(rod_status, StatusMonitor::kFailed);
	    }
	    else {
	      rod_status = combineStatus(rod_status, StatusMonitor::kOk);
	    }
	  }
	  try {
	    State_t old_histogram_number_state = conn_obj_data.value<State_t>(measurementType(), serial_number, m_varId) ;
	    if ( old_histogram_number_state == histogram_number_state ) {
	      update=false;
	    }
	  }
	  catch(CalibrationData::MissingValue &) {
	  }
	  catch(...){
	  }
	}
	  if (update) {
	    receptor()->setValue(kModuleValue, measurementType(), serial_number, PixA::connectivityName(module_iter), extractor, histogram_number_state);
	  }

	}
	catch(CalibrationData::MissingConnObject &) {
	    // should not happen
	  std::cout << "ERROR [AnalysisStatusMonitor::process] connectivity object " <<  PixA::connectivityName(module_iter) 
		    << " does not exist for " << makeSerialNumberString(measurementType(),serial_number) << "." << std::endl;
	  rod_status = combineStatus(rod_status, StatusMonitor::kUnknown);
// 	  if (rod_status == StatusMonitor::kOk ) {
// 	    rod_status = StatusMonitor::kUnknown;
// 	  }
	}
      }
    }
    }
    catch (PixA::ConfigException &) {
      std::cout << "ERROR [AnalysisStatusMonitor::process] Config exception for ROD " <<  rod_name  << std::endl;
      //      rod_status = StatusMonitor::kUnknown;
      rod_status = combineStatus(rod_status, StatusMonitor::kUnknown);
    }
    }
    if (m_verbose) {
      std::cout << "INFO [AnalysisStatusMonitor::process]  ROD " <<  rod_name  << " status = " << rod_status << std::endl;
    }
    return rod_status;
  }
  //end peter's insert



  StatusMonitor::EStatus AnalysisStatusMonitor::finish(SerialNumber_t serial_number, StatusMonitor::EStatus /*combined_status*/) {

    if (m_verbose) {
      std::cout << "INFO [AnalysisStatusMonitor::finish] " << makeSerialNumberString(measurementType(), serial_number)
		<< " has finished." << std::endl;
    }

    {
      Lock lock(m_processingRequestMutex);
      m_processingRequests[serial_number].setFinished();
      m_processingRequests[serial_number].setHasAllRequests();
    }

    // @todo read global status from meta data base

    StatusMonitor::EStatus global_status = kUnset;

    processRequests();
    return global_status;
  }

  void AnalysisStatusMonitor::processRequests() {

    std::vector<SerialNumber_t> process_serial_numbers;
    {
      Lock lock(m_processingRequestMutex);
      process_serial_numbers.reserve(m_processingRequests.size());
      for (std::map<SerialNumber_t, ProcessingRequest_t>::iterator serial_number_iter = m_processingRequests.begin();
	   serial_number_iter != m_processingRequests.end();
	   ++serial_number_iter) {

	if (       serial_number_iter->second.hasAllRequests()
	       || (serial_number_iter->second.hasRequests()
                   && serial_number_iter->second.elapsedTime() > m_processingDelay)) {

	  process_serial_numbers.push_back(serial_number_iter->first);
	}

      }
    }

    for (std::vector<SerialNumber_t>::iterator serial_number_iter = process_serial_numbers.begin();
	 serial_number_iter != process_serial_numbers.end();
	 ++serial_number_iter) {
      processRequests( *serial_number_iter, false );
    }

  }

  void AnalysisStatusMonitor::processRequests(SerialNumber_t serial_number, bool enforce) {

    std::map<SerialNumber_t, ProcessingRequest_t>::iterator serial_number_iter;
    {
      Lock lock(m_processingRequestMutex);
      serial_number_iter = m_processingRequests.find(serial_number);
      if (serial_number_iter == m_processingRequests.end()) {
	return;
      }
    }

    if (enforce
	|| serial_number_iter->second.hasAllRequests()
	|| (serial_number_iter->second.hasRequests()
	    && serial_number_iter->second.elapsedTime() > m_processingDelay)) {
      processRequests( serial_number_iter );
    }
  }

  void AnalysisStatusMonitor::processRequests(std::map<SerialNumber_t, ProcessingRequest_t>::iterator serial_number_iter) {

    try {
      if (!m_analysisResultDb.get()) {
	m_analysisResultDb=std::unique_ptr<IPixResultsDbClient>(PixResultsDbClient::createClient(m_verbose));
      }
    }
    catch ( std::exception& err ) {
      std::cout << "FATAL [AnalysisStatusMonitor::process] Failed to connect to results DB : " << err.what()  << std::endl;
      return;
    }
    catch ( ... ) {
      std::cout << "FATAL [AnalysisStatusMonitor::process] Caught Unknown exception when trying to connect to results DB." << std::endl;
      return;
    }
    if (!m_analysisResultDb.get()) {
      std::cout << "FATAL [AnalysisStatusMonitor::process] Failed to connect to results DB." << std::endl;
      return;
    }

    unsigned int initial_n_rods = serial_number_iter->second.nRodsProcessed();
    if (m_verbose) {
      std::cout << "INFO [AnalysisStatusMonitor::process] start processing requests for "
		<< makeSerialNumberString(measurementType(), serial_number_iter->first)  ;
      for (std::vector<std::string>::const_iterator rod_iter = serial_number_iter->second.begin();
	   rod_iter != serial_number_iter->second.end();
	   ++rod_iter) {
	std::cout << *rod_iter << " ";
      }
      std::cout << std::endl;
    }

    State_t current_status = calibrationData()->status(measurementType(), serial_number_iter->first);
    if (MetaData_t::isNotLoading(static_cast<MetaData_t::EStatus>(current_status))) {

      //paranoia check
      assert( static_cast<unsigned short>(MetaData_t::kUnset) == static_cast<unsigned short>(StatusMonitor::kUnset) ); 

      current_status = static_cast<State_t>( MetaData_t::makeLoading(static_cast<MetaData_t::EStatus>(current_status)) );
      calibrationData()->updateStatus( measurementType(), serial_number_iter->first, current_status );
      statusChangeTransceiver()->signal(measurementType(), serial_number_iter->first );
    }

    try {
      IPixResultsDbClient &analysis_result_db=*m_analysisResultDb;

      std::vector<std::string> conn_name_list;

      // build list of all (ROD,PP0, module) enabled connectivity objects
      {
	Lock lock(calibrationData()->calibrationDataMutex());
	PixA::ConnectivityRef conn = calibrationData()->analysisConnectivity(serial_number_iter->first);

	for (std::vector<std::string>::const_iterator rod_iter = serial_number_iter->second.begin();
	     rod_iter != serial_number_iter->second.end();
	     ++rod_iter) {

	  try {

	    if (calibrationData()->calibrationData()
		.getConnObjectData( *rod_iter )
		.enableState(measurementType(), serial_number_iter->first).enabled()) {

	      conn_name_list.push_back( *rod_iter );
	      serial_number_iter->second.incRods();

	      PixA::Pp0List pp0_list = conn.pp0s(*rod_iter);
	      PixA::Pp0List::iterator pp0_begin = pp0_list.begin();
	      PixA::Pp0List::iterator pp0_end = pp0_list.end();
	      //    Lock lock(calibrationData()->calibrationDataMutex());
	      for (PixA::Pp0List::iterator pp0_iter = pp0_begin; pp0_iter != pp0_end; ++pp0_iter) {

		// @todo there are no PP0s in the calibration console
		// 		try {
		// 		  if (calibrationData()->calibrationData()
		// 		      .getConnObjectData( PixA::connectivityName(pp0_iter) )
		// 		      .enableState(measurementType(), serial_number_iter->first).enabled()) {

		    conn_name_list.push_back(PixA::connectivityName(pp0_iter));

		    PixA::ModuleList::iterator module_begin = PixA::modulesBegin(pp0_iter);
		    PixA::ModuleList::iterator module_end = PixA::modulesEnd(pp0_iter);
		    for (PixA::ModuleList::iterator module_iter = module_begin; module_iter != module_end; ++module_iter) {

		      try {
			if (calibrationData()->calibrationData()
			    .getConnObjectData( PixA::connectivityName(module_iter) )
			    .enableState(measurementType(), serial_number_iter->first).enabled()) {

			  conn_name_list.push_back(PixA::connectivityName(module_iter));
			}
		      }
		      catch (CalibrationData::MissingConnObject &) {
		      }

		    }
		    //		  }
	      // 		}
	      // 		catch (CalibrationData::MissingConnObject &) {
	      // 		}

	      }
	    }
	  }
	  catch (CalibrationData::MissingConnObject &) {
	  }
	}
      }

      // clear list of processing requests;
      serial_number_iter->second.clear();

      // needs var list such that only new variables get announced.
      m_listener->setMeasurement(serial_number_iter->first, serial_number_iter->second.varList() );
      std::string error_message;
      for (unsigned int pass_i=0; pass_i<10 ; pass_i++) {

	try {
	  if (conn_name_list.size()>500) {
	    // if the conn_name_list is too long, everything is queried from oracle no matter whether
	    // something got retrieved already.
	    conn_name_list.clear();
	  }
	  analysis_result_db.getAnalysisResultsFromDB(serial_number_iter->first,conn_name_list, *m_listener);
	  analysis_result_db.disconnect();
	  break;
	}
	catch ( std::exception& e ) {
	  std::stringstream attempts;
	  attempts << pass_i;
	  error_message = "Std exception (";
	  error_message += attempts.str();
	  error_message += ") : ";
	  error_message += e.what();

	  // if the database is locked or this strange KEYGEN error appears, then try again
	  // otherwise throw an exception
	  if (   error_message.find("database is locked")== std::string::npos
		 && error_message.find("database schema has changed")==std::string::npos) {
	    break;
	  }
	  analysis_result_db.disconnect();
	}
	catch (...) {
	  std::stringstream attempts;
	  attempts << pass_i;
	  error_message = "Unknown exception caught (";
	  error_message += attempts.str();
	  error_message += ") .";
	  break;
	}
	sleep(1);
      }
      if (!error_message.empty()) {
	std::cout << "ERROR [AnalysisStatusMonitor::process] Caught exception while trying to retrieve the analysis results : " << error_message  << std::endl;
      }
    }
    catch ( std::exception& err ) {
      std::cout << "ERROR [AnalysisStatusMonitor::process] Caught exception while trying to retrieve the analysis results : " << err.what()  << std::endl;
    }
    catch ( ... ) {
      std::cout << "ERROR [AnalysisStatusMonitor::process] Caught unknown exception while trying to retrieve the analysis results." << std::endl;
    }
    if (m_verbose) {
      std::cout << "INFO [AnalysisStatusMonitor::process] Processed "  << serial_number_iter->second.nRodsProcessed()-initial_n_rods << " RODs for " 
		<< makeSerialNumberString(measurementType(), serial_number_iter->first)  << std::endl;
    }

    // if finished set the global status.
    if (serial_number_iter->second.isFinished()) {

      StatusMonitor::EStatus global_status = kUnset;
      if (current_status >= static_cast<State_t>(MetaData_t::kUnknown) ) {

      try {
	Lock lock(calibrationData()->calibrationDataMutex());
	const CalibrationData::ConnObjDataList conn_obj_data ( calibrationData()->calibrationData().getConnObjectData( "PIXDET" ));
	//      if (conn_obj_data.enableState(measurementType(), serial_number_iter->first).enabled()) {
	CAN::AnalysisStatus::EStatus analysis_status = static_cast<CAN::AnalysisStatus::EStatus>( conn_obj_data.value<State_t>(measurementType(),
															       serial_number_iter->first,
															       m_statusVarId)) ;

	std::cout << "INFO [AnalysisStatusMonitor::process] final status =  "  << analysis_status << " RODs for " 
		  << makeSerialNumberString(measurementType(), serial_number_iter->first)  << std::endl;

	if (analysis_status == CAN::AnalysisStatus::kSuccess) {
	  global_status = StatusMonitor::kOk;
	}
	else if (analysis_status == CAN::AnalysisStatus::kFailure) {
	  global_status = StatusMonitor::kFailed;
	}
	else if (analysis_status > CAN::AnalysisStatus::kFailure) {
	  global_status = StatusMonitor::kAborted;
	}
	//      }
      }
      catch (CalibrationData::MissingValue &) {
	MetaData_t::EStatus meta_data_status = static_cast<MetaData_t::EStatus>( calibrationData()->getStatusSafe( measurementType(), serial_number_iter->first));
	if(meta_data_status == MetaData_t::kRunning || meta_data_status >= MetaData_t::kNStates) {
	  global_status =StatusMonitor::kUnknown;
	}
      }
      }

      if (global_status == kUnset && MetaData_t::isLoading( static_cast<MetaData_t::EStatus>(current_status))) {
	global_status = static_cast<StatusMonitor::EStatus>( MetaData_t::clearLoading( static_cast<MetaData_t::EStatus>(current_status) ) );
      }
      if (global_status != kUnset) {
	//global_status = (global_status == kUnset ? combined_status : global_status);
	// @todo read global status from meta data base
	std::cout << "INFO [AnalysisStatusMonitor::process] Updated  "
		  << makeSerialNumberString(measurementType(), serial_number_iter->first)  
		  << ". Processed "  << serial_number_iter->second.nRodsProcessed() << " RODs for " 
		  << std::endl;

	calibrationData()->updateStatus( measurementType(), serial_number_iter->first, global_status);
	statusChangeTransceiver()->signal(measurementType(), serial_number_iter->first );
      }

      if (m_verbose) {
	std::cout << "INFO [AnalysisStatusMonitor::process] All done for "
		  << makeSerialNumberString(measurementType(), serial_number_iter->first)  
		  << ". Processed "  << serial_number_iter->second.nRodsProcessed() << " RODs for " 
		  << std::endl;
      }
      {
	Lock lock(m_processingRequestMutex);
	m_processingRequests.erase(serial_number_iter);
      }
    }

  }


}
