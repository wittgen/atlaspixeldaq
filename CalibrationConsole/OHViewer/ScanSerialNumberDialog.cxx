#include "ScanSerialNumberDialog.h"


namespace PixCon {

  void ScanSerialNumberDialog::verifySerialNumberAndAccept()
  {
    const char *serial_number_text = m_scanSerialNumber->text().toLatin1().data();
    if (serial_number_text) {
      char *end_ptr = NULL;
      SerialNumber_t serial_number = strtol(serial_number_text, &end_ptr, 10);

      if (end_ptr) {
	while ( *end_ptr && isspace(*end_ptr)) end_ptr++;
	if (*end_ptr) {

	  QColor white("white");
	  QColor highlight_color("yellow");

	  if (!m_highlighter.get()) {
	    m_highlighter=std::make_unique<Highlighter>(*m_scanSerialNumber,highlight_color,white,3*1000);
	  }
	  else {
	    m_highlighter->restart(highlight_color,3*1000);
	  }

	}
	else {
	  m_serialNumber=serial_number;
	  switch (m_measurementTypeSelector->currentIndex()) {
	  case 0:
	    m_measurementType=kScan;
	    break;
	  case 1:
	    m_measurementType=kAnalysis;
	    break;
	  }
	  accept();
	}
      }

    }
  }
}
