#include "OHViewerApp.h"

#include <CalibrationDataManager.h>
#include <CalibrationDataStyle.h>
#include <CalibrationConsoleContextMenu.h>
#include <CategoryList.h>

#include <HistogramProviderInterceptor.h>
#include <HistogramCache.h>
#include <OHHistogramProvider.h>
#include <PixHistoServerInterfaceProvider.h>
#include <MetaHistogramProvider.h>
#include <ScanFileHistogramProvider.h>
#include "CoralClientMetaDataUpdateHelper.h"

#include "StatusChangeTransceiver.h"
#include "OHViewer.h"

#include <is/inforeceiver.h>


OHViewerApp::OHViewerApp(int qt_argc, char **qt_argv,
			 std::shared_ptr<PixCon::UserMode>& user_mode,
			 const std::string &partition_name,
			 const std::string &is_server_name,
			 const std::string &oh_partition_name,
			 const std::string &oh_server_name,
			 const std::string &histo_name_server_name,
			 int poll)
  : QRootApplication(qt_argc, qt_argv, poll),
    m_categories(new PixCon::CategoryList),
    m_calibrationData(new PixCon::CalibrationDataManager),
    m_partition(new IPCPartition(partition_name)),
    m_verbose(false)
{
  //  assert( !partition_name.empty() );
  assert( !is_server_name.empty() );
  assert( !oh_server_name.empty() );

  //IPCPartition *the_oh_partition=m_partition.get();
  if (!oh_partition_name.empty()) {
    m_ohPartition = std::make_unique<IPCPartition>( oh_partition_name );
    //    the_oh_partition = m_ohPartition.get();
  }
  bool verbose = false;
  std::shared_ptr<PixCon::OHHistogramProviderBase> 
    oh_histogram_provider( ( !histo_name_server_name.empty() ?
			     static_cast<PixCon::OHHistogramProviderBase *>(new PixCon::PixHistoServerInterfaceProvider(*(m_ohPartition?(new IPCPartition(oh_partition_name)):new IPCPartition(partition_name)),
															 oh_server_name,
															 histo_name_server_name,
															 verbose) )
			     : static_cast<PixCon::OHHistogramProviderBase *>(new PixCon::OHHistogramProvider(*(m_ohPartition?(new IPCPartition(oh_partition_name)):new IPCPartition(partition_name)), oh_server_name,verbose )) ) );

  std::shared_ptr<PixCon::IHistogramProvider> root_file_histogram_provider(new PixCon::ScanFileHistogramProvider(m_calibrationData));

  auto meta_histogram_provider= std::make_shared<PixCon::MetaHistogramProvider>(oh_histogram_provider,
										root_file_histogram_provider);
  m_histogramProvider=meta_histogram_provider;
  
  std::shared_ptr<PixCon::CalibrationDataPalette> palette(new PixCon::CalibrationDataPalette);
  palette->setDefaultPalette();
  
  //  PixCon::OHHistogramProviderBase *oh_histogram_provider_ptr = oh_histogram_provider.get();
  if (m_verbose) {
    m_histogramProviderInterceptor = std::make_shared<PixCon::HistogramProviderInterceptor>(meta_histogram_provider.get());
  }
  else {
    m_histogramProviderInterceptor = m_histogramProvider;
  }

  std::shared_ptr<PixCon::HistogramCache> histogram_cache( new PixCon::HistogramCache( m_histogramProviderInterceptor ) );
  std::shared_ptr<PixCon::VisualiserList> visualiser_list(new PixCon::VisualiserList);
  std::shared_ptr<PixCon::IContextMenu> context_menu(  new PixCon::CalibrationConsoleContextMenu(m_calibrationData, histogram_cache, visualiser_list, partition_name) );

  std::shared_ptr<PixCon::IMetaDataUpdateHelper> meta_data_update_helper(new PixCon::CoralClientMetaDataUpdateHelper(m_calibrationData));

  //  m_infoReceiver = std::unique_ptr<ISInfoReceiver>( new ISInfoReceiver(*m_partition) );


  std::shared_ptr<PixCon::StatusChangeTransceiver> status_change_transceiver( new PixCon::StatusChangeTransceiver(m_calibrationData));
  m_viewer=std::make_unique<PixCon::OHViewer>( user_mode, 
								  is_server_name,
								  *m_partition,
								  m_calibrationData,
								  status_change_transceiver,
								  *oh_histogram_provider,
								  meta_histogram_provider,
								  histogram_cache,
								  palette,
								  m_categories,
								  context_menu,
								  meta_data_update_helper,
								  visualiser_list,
								  *this );


  m_viewer->updateCategories(m_categories);

    //    detector_view_window.updateCategories(categories);
    //    detector_view_window.addCurrentSerialNumber();

  m_viewer->show();
  connect( this, SIGNAL( lastWindowClosed() ), this, SLOT( quit() ) );

}

void OHViewerApp::addScan( PixCon::SerialNumber_t scan_serial_number ) 
{
  m_viewer->addScan( scan_serial_number );
}

void OHViewerApp::addAnalysis( PixCon::SerialNumber_t analysis_serial_number ) 
{
  m_viewer->addAnalysis( analysis_serial_number );
}

OHViewerApp::~OHViewerApp() {}

// void OHViewerApp::setTags(const std::string &object_tag_name, const std::string &tag_name, const std::string &cfg_tag_name, const std::string &mod_cfg_tag_name)
// {
//   if (!object_tag_name.empty() && !tag_name.empty() && !cfg_tag_name.empty()) {
//     m_viewer->changeConnectivity(object_tag_name, tag_name,tag_name,tag_name,cfg_tag_name,0);
//   }
// }
