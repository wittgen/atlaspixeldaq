#ifndef UI_SCANSERIALNUMBERDIALOGBASE_H
#define UI_SCANSERIALNUMBERDIALOGBASE_H

#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QComboBox>
#include <QDialog>
#include <QFrame>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QSpacerItem>
#include <QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ScanSerialNumberDialogBase
{
public:
    QVBoxLayout *vboxLayout;
    QLabel *textLabel2;
    QSpacerItem *spacer5;
    QHBoxLayout *hboxLayout;
    QSpacerItem *spacer8;
    QGridLayout *gridLayout;
    QLabel *textLabel1;
    QSpacerItem *spacer4_2_2;
    QComboBox *m_measurementTypeSelector;
    QSpacerItem *spacer4_2;
    QLabel *m_label;
    QLineEdit *m_scanSerialNumber;
    QSpacerItem *spacer9;
    QSpacerItem *spacer4;
    QFrame *line1;
    QHBoxLayout *hboxLayout1;
    QSpacerItem *spacer1;
    QPushButton *m_cancelButton;
    QSpacerItem *spacer2;
    QPushButton *m_okButton;
    QSpacerItem *spacer3;

    void setupUi(QDialog *ScanSerialNumberDialogBase)
    {
        if (ScanSerialNumberDialogBase->objectName().isEmpty())
            ScanSerialNumberDialogBase->setObjectName(QString::fromUtf8("ScanSerialNumberDialogBase"));
        ScanSerialNumberDialogBase->resize(365, 229);
        vboxLayout = new QVBoxLayout(ScanSerialNumberDialogBase);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        textLabel2 = new QLabel(ScanSerialNumberDialogBase);
        textLabel2->setObjectName(QString::fromUtf8("textLabel2"));
        QFont font;
        font.setPointSize(14);
        font.setBold(true);
        textLabel2->setFont(font);
        textLabel2->setMargin(3);
        textLabel2->setAlignment(Qt::AlignCenter);
        textLabel2->setWordWrap(false);

        vboxLayout->addWidget(textLabel2);

        spacer5 = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout->addItem(spacer5);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        spacer8 = new QSpacerItem(21, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacer8);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        textLabel1 = new QLabel(ScanSerialNumberDialogBase);
        textLabel1->setObjectName(QString::fromUtf8("textLabel1"));
        textLabel1->setWordWrap(false);

        gridLayout->addWidget(textLabel1, 0, 0, 1, 1);

        spacer4_2_2 = new QSpacerItem(20, 16, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(spacer4_2_2, 1, 0, 1, 1);

        m_measurementTypeSelector = new QComboBox(ScanSerialNumberDialogBase);
        m_measurementTypeSelector->setObjectName(QString::fromUtf8("m_measurementTypeSelector"));

        gridLayout->addWidget(m_measurementTypeSelector, 0, 1, 1, 1);

        spacer4_2 = new QSpacerItem(20, 16, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(spacer4_2, 1, 1, 1, 1);

        m_label = new QLabel(ScanSerialNumberDialogBase);
        m_label->setObjectName(QString::fromUtf8("m_label"));
        m_label->setWordWrap(false);

        gridLayout->addWidget(m_label, 2, 0, 1, 1);

        m_scanSerialNumber = new QLineEdit(ScanSerialNumberDialogBase);
        m_scanSerialNumber->setObjectName(QString::fromUtf8("m_scanSerialNumber"));

        gridLayout->addWidget(m_scanSerialNumber, 2, 1, 1, 1);


        hboxLayout->addLayout(gridLayout);

        spacer9 = new QSpacerItem(21, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacer9);


        vboxLayout->addLayout(hboxLayout);

        spacer4 = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout->addItem(spacer4);

        line1 = new QFrame(ScanSerialNumberDialogBase);
        line1->setObjectName(QString::fromUtf8("line1"));
        line1->setFrameShape(QFrame::HLine);
        line1->setFrameShadow(QFrame::Sunken);

        vboxLayout->addWidget(line1);

        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setSpacing(6);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        spacer1 = new QSpacerItem(50, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout1->addItem(spacer1);

        m_cancelButton = new QPushButton(ScanSerialNumberDialogBase);
        m_cancelButton->setObjectName(QString::fromUtf8("m_cancelButton"));

        hboxLayout1->addWidget(m_cancelButton);

        spacer2 = new QSpacerItem(40, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout1->addItem(spacer2);

        m_okButton = new QPushButton(ScanSerialNumberDialogBase);
        m_okButton->setObjectName(QString::fromUtf8("m_okButton"));
        m_okButton->setDefault(true);

        hboxLayout1->addWidget(m_okButton);

        spacer3 = new QSpacerItem(50, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout1->addItem(spacer3);


        vboxLayout->addLayout(hboxLayout1);


        retranslateUi(ScanSerialNumberDialogBase);
        QObject::connect(m_cancelButton, SIGNAL(clicked()), ScanSerialNumberDialogBase, SLOT(reject()));
        QObject::connect(m_okButton, SIGNAL(clicked()), ScanSerialNumberDialogBase, SLOT(verifySerialNumberAndAccept()));

        QMetaObject::connectSlotsByName(ScanSerialNumberDialogBase);
    } // setupUi

    void retranslateUi(QDialog *ScanSerialNumberDialogBase)
    {
        ScanSerialNumberDialogBase->setWindowTitle(QApplication::translate("ScanSerialNumberDialogBase", "Form1", 0));
        textLabel2->setText(QApplication::translate("ScanSerialNumberDialogBase", "Monitor / Load Scan or Analysis", 0));
        textLabel1->setText(QApplication::translate("ScanSerialNumberDialogBase", "Type :", 0));
        m_measurementTypeSelector->clear();
        m_measurementTypeSelector->insertItems(0, QStringList()
         << QApplication::translate("ScanSerialNumberDialogBase", "Scan", 0)
         << QApplication::translate("ScanSerialNumberDialogBase", "Analysis", 0)
        );
        m_label->setText(QApplication::translate("ScanSerialNumberDialogBase", "Serial Number :", 0));
        m_cancelButton->setText(QApplication::translate("ScanSerialNumberDialogBase", "Cancel", 0));
        m_okButton->setText(QApplication::translate("ScanSerialNumberDialogBase", "Ok", 0));
    } // retranslateUi

};

namespace Ui {
    class ScanSerialNumberDialogBase: public Ui_ScanSerialNumberDialogBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCANSERIALNUMBERDIALOGBASE_H
