/* Dear emacs, this is -*-c++-*- */
#ifndef _PixCon_ScanStatusMonitor_h_
#define _PixCon_ScanStatusMonitor_h_

#include "StatusMonitor.h"
#include "VarDefList.h"
#include <vector>

namespace PixCon {

  class OHHistogramProviderBase;
  class HistogramCache;
  class IsMonitorManager;
  class IsReceptor;

  /** Monitors particular scans until they are finished and set the status in the calibration data container.
   * The calibration data container will be filled with the processing status of the scan  and the 
   * a status word which is derived from the number of histograms.
   */
  class ScanStatusMonitor : public StatusMonitor
  {
  public:
    //    enum HistogramState {kPresumablyDisabled, kNoneExpected, kOk, kOkButBelowExpectation, kOkButAboveExpectation, kMissingHistograms, kTooManyHistograms, kNStates};

    /** Create a scan status monitor which will automatically gather information about histograms once a scan has finished.
     * @param histogram_provider a reference to a histogram provider which must be thread safe and which lives longer than the scan status monitor
     * @param receptor reference to a receptor which recepts the status from IS and which also will copy the status to the calibration data.
     * @param is_monitor_manager pointer to the IS monitor manager to which the per scan is monitors will be added.
     * @param measurement_type the type of status which could be scan or analysis.
     * @param var_name the name of the status variable e.g. STATUS for scan.
     * @param calibration_data_var_name the variable name to be used in the calibration data container instead of the is name.
     * @param calibration_data the calibration data manager which will be filled with a flag derived from the number of histograms which exist per ROD.
     * @param status_threshold if the status gets equal to or larger than this value, the scan is considered to be finished.
     * The analysis status monitor will post events of type @ref QNewVarEvent to announce
     * the addition of new variables to a measurement. Via the @ref IsReceptor it will announce
     * new or changed values.
     */
    ScanStatusMonitor(OHHistogramProviderBase &histogram_provider,
		      const std::shared_ptr<PixCon::HistogramCache> &histogram_cache,
		      const std::shared_ptr<IsReceptor> &receptor,
		      const std::shared_ptr<IsMonitorManager> &is_monitor_manager,
		      EMeasurementType measurement_type,
		      const std::string &var_name,
		      const std::string &calibration_data_var_name,
		      const std::shared_ptr<CalibrationDataManager> &calibration_data,
		      int status_threshold,
		      const std::shared_ptr<StatusChangeTransceiver> &status_change_transceiver);

    ~ScanStatusMonitor();

  protected:
    /** Analyse histogram which were created for the given scan and ROD.
     * @param serial_number the serial number of the scan in question.
     * @param rod_name the connectivity name of the ROD.
     * @param var_list the list of variables which is currently assigned to a given measurement.
     * The function will set the result of the histogram analysis in the calibration data container.
     * The application will be notified in case new variables are added. The new variables are also
     * added to the var_list.
     */
    StatusMonitor::EStatus process(SerialNumber_t serial_number, const std::string &rod_name, std::set<std::string> &var_list);

    /** Function to be called when a scan has terminated for all RODs.
     */
    StatusMonitor::EStatus finish(SerialNumber_t serial_number, EStatus combined_status);


    /** Translate the given status name into a status value.
     */
    static int translateStatusName(const std::string &status_name);

  private:
    StatusMonitor::EStatus process(SerialNumber_t serial_number, const std::string &rod_name);
    using StatusMonitor::process;
    OHHistogramProviderBase *m_histoProvider;
    std::shared_ptr<PixCon::HistogramCache> m_histogramCache;
    VarId_t m_varId;
    VarId_t m_statusVarId;
    static const std::string s_histogramCategoryName;
    static std::vector<StatusMonitor::EStatus> s_convertStatus; 
    bool m_verbose;
  };

}


#endif
