#include <QApplication>
#include <QComboBox>
#include <QDateTimeEdit>
#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QRadioButton>
#include <QShortcut>
#include <QStackedWidget>
#include <QTreeWidget>
#include <memory>

#include <cstring>
#include <sstream>
#include <iostream>
#include <iomanip>

using namespace std;

#include "SearchDbCoralForm.h"
#include <ScanConfigDbInstance.hh>
#include <ObjectConfigDbInstance.hh>
#include "PixMetaException.hh"
#include "OHViewer.h"
#include <BusyLoop.h>

namespace Aux {
  std::string extractFirstSentence(const std::string &multi_line) {
    std::string::size_type pos_sentence_start=0;
    for(;pos_sentence_start<multi_line.size();) {
      while (pos_sentence_start<multi_line.size() && !isalnum(multi_line[pos_sentence_start]) && !ispunct(multi_line[pos_sentence_start])) pos_sentence_start++;
    std::string::size_type pos_sentence_end = multi_line.find(".",pos_sentence_start);
    std::string::size_type pos_line_end = multi_line.find("\n", pos_sentence_start);
    if (pos_sentence_end ==std::string::npos || pos_line_end < pos_sentence_end) {
      pos_sentence_end = pos_line_end;
    }
    if (pos_sentence_end-pos_sentence_start>1) {
      if (pos_sentence_end != std::string::npos) {
	return multi_line.substr(pos_sentence_start,pos_sentence_end-pos_sentence_start);
      }
      else {
	return multi_line;
      }
    }
    pos_sentence_start=pos_sentence_end+1;
    }
    return multi_line;
  }
}

SearchDbCoralForm::SearchDbCoralForm( PixCon::OHViewer *ohViewer, QWidget* parent)
: QDialog(parent),
    m_ohViewer(ohViewer),
    m_hasAnalysis(0)
{
setupUi(this);
  searchLocation = CAN::kNowhere;
  searchQuality=CAN::kUndefined;
  searchStatus=CAN::kNotSet;
  searchAnalysisID=0;
  searchScanID=0;
  scan=false;
  analysis=false;
  searchBeginTime=-1;
  searchEndTime=-1;

  init();
}

SearchDbCoralForm::~SearchDbCoralForm() {}



void SearchDbCoralForm::init()
{
  //Build list of locations
  location_name_list.insert(std::make_pair(CAN::kNowhere,std::string("-")));
  location_name_list.insert(std::make_pair(CAN::kSR1,std::string("SR1/ToothPix")));
  location_name_list.insert(std::make_pair(CAN::kPIT,std::string("PIT")));
  location_name_list.insert(std::make_pair(CAN::kPixelLab,std::string("PixelLab")));
  //Build list of quality types
  quality_name_list.insert(std::make_pair(CAN::kUndefined,std::string("")));
  quality_name_list.insert(std::make_pair(CAN::kNormal,std::string("normal")));
  quality_name_list.insert(std::make_pair(CAN::kDebug,std::string("debug")));
  quality_name_list.insert(std::make_pair(CAN::kTrash,std::string("trash")));
  //Build list of status types
  status_name_list.insert(std::make_pair(CAN::kNotSet,std::string("Unknown")));
  status_name_list.insert(std::make_pair(CAN::kSuccess,std::string("Success")));
  status_name_list.insert(std::make_pair(CAN::kFailure,std::string("Failure")));
  status_name_list.insert(std::make_pair(CAN::kAborted,std::string("Aborted")));  

  //Set properties of ListViews
  scanResultListView->setSelectionMode(QAbstractItemView::MultiSelection);
  analysisListView->setSelectionMode(QAbstractItemView::MultiSelection);
  scanResultListView->setAllColumnsShowFocus(true);
  analysisListView->setAllColumnsShowFocus(true);
    
  //Set properties of DateEdits
  fromDateEdit->setDisplayFormat("dd.MM.yyyy");
  toDateEdit->setDisplayFormat("dd.MM.yyyy");
  toDateEdit->setDate( QDate::currentDate() );
  fromDateEdit->setDate( QDate::currentDate().addMonths(-1) );

  //fill Scan Type combo box
  m_configDb[kScan]=std::shared_ptr<CAN::IConfigDb>( CAN::ScanConfigDbInstance::create() );
  m_searchTypeComboBox[kScan]=searchScanTypeComboBox;
  s_className[kScan]= CAN::ScanConfigDbInstance::scanClassName();
  fillTypeComboBox(kScan);

  //fill Analysis Type combo box
  m_configDb[kAnalysis]=std::shared_ptr<CAN::IConfigDb>( CAN::ObjectConfigDbInstance::create() );
  m_searchTypeComboBox[kAnalysis]=searchAnalysisTypeComboBox;
  s_className[kAnalysis]= CAN::ObjectConfigDbInstance::objectClassName(CAN::ObjectConfigDbInstance::kAnalysis);
  fillTypeComboBox(kAnalysis);

  // hide selector for analysis ID 
  // now the (scan) ID line edit takes scan and analysis serial numbers
  textLabel1_3_2_4_4_2->hide();
  searchAnalysisIDLineEdit->hide();

  QShortcut *selectId = new QShortcut(QKeySequence("Ctrl+W"), this);
  connect(selectId,SIGNAL(activated()),searchScanIDLineEdit,SLOT(setFocus()));

  QShortcut *scan_sel = new QShortcut(QKeySequence("Ctrl+S"), this);
  connect(scan_sel,SIGNAL(activated()),this,SLOT(selectScanSearch()));

  QShortcut *ana_sel = new QShortcut(QKeySequence("Ctrl+A"),this);
  connect(ana_sel,SIGNAL(activated()),this,SLOT(selectAnalysisSearch()));

  QShortcut *list_sel = new QShortcut(QKeySequence("Ctrl+l"),this);
  connect(list_sel,SIGNAL(activated()),this,SLOT(focusListView()));
  
}

void SearchDbCoralForm::selectScanSearch() {
  std::cout << "INFO [SearchDbCoralForm::selectScanSearch] " << searchComboBox->currentIndex() << std::endl;
  int search_form=0;
  searchComboBox->setCurrentIndex(search_form);
  widgetStack->setCurrentIndex(0);
}

void SearchDbCoralForm::selectAnalysisSearch() {
  std::cout << "INFO [SearchDbCoralForm::selectAnalysisSearch] " << searchComboBox->currentIndex() << std::endl;
  int search_form=1;
  searchComboBox->setCurrentIndex(search_form);
  widgetStack->setCurrentIndex(1);
}

void SearchDbCoralForm::checkToggleAnalysis() {
  if (m_hasAnalysis & 1) {
    if (m_hasAnalysisButton->isChecked()) {
      m_hasAnalysisButton->setChecked(false);
    }
  }
  m_hasAnalysis &= ~1;
  if (m_hasAnalysisButton->isChecked()) {
    m_hasAnalysis |= 1;
  }
}

void SearchDbCoralForm::checkToggleNoAnalysis() {
  if (m_hasAnalysis & 2) {
    if (m_hasNoAnalysisButton->isChecked()) {
      m_hasNoAnalysisButton->setChecked(false);
    }
  }
  m_hasAnalysis &= ~2;
  if (m_hasNoAnalysisButton->isChecked()) {
    m_hasAnalysis |= 2;
  }
}

void SearchDbCoralForm::fillTypeComboBox(int type)
{
  m_searchTypeComboBox[type]->clear();
  m_searchTypeComboBox[type]->addItem("Any");
  std::set<std::string> type_list;
  try {
    type_list = m_configDb[type]->listTypes(s_className[type], CAN::IConfigDb::kDebug);
  }
  catch (...) {
  }
  
  for (std::set<std::string>::const_iterator scan_type_iter = type_list.begin();
       scan_type_iter != type_list.end();
       scan_type_iter++) {
    if (scan_type_iter->empty()) continue;
    std::string::size_type pos = 0;
    while (pos < scan_type_iter->size() && isdigit( (*scan_type_iter)[pos])) pos++;
    if (pos>=scan_type_iter->size()) continue;
    m_searchTypeComboBox[type]->addItem(QString::fromStdString(*scan_type_iter));
  }
  m_searchTypeComboBox[type]->adjustSize();
  adjustSize();

}

void SearchDbCoralForm::resetComboBoxes()
{
  //reset Scan and Analyis Type combo boxes
  fillTypeComboBox(kScan);
  fillTypeComboBox(kAnalysis);

  //reset DateEdits
  fromDateEdit->setDisplayFormat("dd.MM.yyyy");
  toDateEdit->setDisplayFormat("dd.MM.yyyy");
  toDateEdit->setDate( QDate::currentDate() );
  fromDateEdit->setDate( QDate::currentDate().addMonths(-1) );
  
  //reset Status
  searchStatusComboBox->clear();
  searchStatusComboBox->addItem("Any Status");
  searchStatusComboBox->addItem("Success");
  searchStatusComboBox->addItem("Failure");
  searchStatusComboBox->addItem("Aborted");
  
  //reset Location
  searchLocationComboBox->clear();
  searchLocationComboBox->addItem("Any Location");
  searchLocationComboBox->addItem("SR1/ToothPix");
  searchLocationComboBox->addItem("ATLAS/PIT");
  searchLocationComboBox->addItem("Pixel Lab");

  //reset Quality
  searchQualityComboBox->clear();
  searchQualityComboBox->addItem("Any Quality");
  searchQualityComboBox->addItem("Normal");
  searchQualityComboBox->addItem("Debug");
  searchQualityComboBox->addItem("Trash");

}

void SearchDbCoralForm::search()
{


  scanResultListView->clear();
  analysisListView->clear();
  scan=false;
  analysis=false;
  try {
    getSearchFormat();

    getSearchID();
    getSearchBeginTime();
    getSearchEndTime();
    getSearchStatus();
    getSearchComment();

    getSearchAnalysisType(); 
    getSearchAnalysisID();
    getSearchClassificationName();
    getSearchClassificationTag();
    getSearchPostName();
    getSearchPostTag();

    getSearchScanType();
    getSearchLocation();
    getSearchQuality();
    getSearchConnTag();
    getSearchCfgTag();
    getSearchRodModule();
    if (searchAnalysisID>0 && scan) {
      searchComboBox->setCurrentIndex(1);
      widgetStack->setCurrentIndex(1 /*Analysis*/);
      analysis=true;
      scan=false;
    }

    if (scan) {
      searchScan();
      if (scanResultListView->topLevelItemCount()>0) {
      searchPushButton->setDefault(false);
      getSelectedPushButton->setDefault(true);
      }
    }

    if (analysis) {
      searchAnalysis();
      if (analysisListView->topLevelItemCount()>0) {
      searchPushButton->setDefault(false);
      getSelectedPushButton->setDefault(true);
      }
    }

    // COOL, CORAL POOL exceptions inherit from std exceptions: catching
    // std::exception will catch all errors from COOL, CORAL and POOL
  } catch ( std::exception& e ) {
    std::cout << "std::exception caught: " << e.what() << std::endl;
  } catch (...) {
    std::cout << "Unknown exception caught!" << std::endl;
  }
  
}

void SearchDbCoralForm::searchScan()
{
  try {
    std::unique_ptr<CAN::PixCoralDbClient> s_coral_client;
    {
	  // search should be performed in the background.
      std::unique_ptr<PixA::IBusyLoop> busy_loop(PixA::BusyLoopFactory::busyLoop(" Connect to database ..."));
      s_coral_client=std::make_unique<CAN::PixCoralDbClient>( true );
    }
    if (!s_coral_client.get()) {
      std::cerr << "FATAL [SearchDbCoralForm::searchScan] Failed to connect to database. " << std::endl;
      return;
    }

    std::map<CAN::SerialNumber_t, PixLib::ScanInfo_t> scan_list;

    if (searchScanID>0 || searchAnalysisID>0) {
      // just use the scan or analysis serial number to query meta data 

      CAN::SerialNumber_t scan_serial_number = 0;
      if (searchAnalysisID>0) {

	// first get scan serial number
	try {
	  // search should be performed in the background.
	  std::unique_ptr<PixA::IBusyLoop> busy_loop(PixA::BusyLoopFactory::busyLoop(" Search meta data ..."));

	  CAN::AnalysisInfo_t analysis_info( s_coral_client->getMetadata<CAN::AnalysisInfo_t>(searchAnalysisID));
	  scan_serial_number = analysis_info.scanSerialNumber();
	}
	catch (CAN::PixMetaException &err) {
	  std::cerr << "WARNING [SearchDbCoralForm::searchScan] Did not get meta data for " << PixCon::makeSerialNumberName(PixCon::kAnalysis, searchAnalysisID) 
		    << " : " << err.what() << std::endl;
	  return;
	}
      }
      else  {
	scan_serial_number = searchScanID;
      }

      scan_list.insert( std::make_pair(scan_serial_number, s_coral_client->getMetadata<PixLib::ScanInfo_t>(scan_serial_number)) );

    }
    else {
      CAN::ETripleFlag has_analysis = CAN::kIndifferent;
      if (m_hasAnalysisButton->isChecked()) {
	has_analysis = CAN::kYes;
      }
      else if (m_hasNoAnalysisButton->isChecked()) {
	has_analysis = CAN::kNo;
      }

      scan_list = s_coral_client->searchScanMetadata(searchLocation,
						    searchBeginTime,
						    searchEndTime,
						    searchScanType,
						    tag,
						    searchQuality,
						    searchStatus,
						    searchComment,
						    has_analysis,
						    searchRodModule);
    }
    for (std::map<CAN::SerialNumber_t, PixLib::ScanInfo_t>::const_iterator scan_iter = scan_list.begin();
	 scan_iter != scan_list.end();
	 scan_iter++) {

      //Retrieve attributes of scan
      unsigned int scanID = scan_iter->first;
      scanType = scan_iter->second.scanType();
      scanTypeTag = scan_iter->second.scanTypeTag();
      connTag = scan_iter->second.connTag();
      cfgTag = scan_iter->second.cfgTag();
      std::string mod_cfg_tag = scan_iter->second.modCfgTag();
      status = status_name_list[scan_iter->second.status()];
      location = location_name_list[scan_iter->second.location()];
      quality = quality_name_list[scan_iter->second.quality()];
      startString = scan_iter->second.scanStartString();
      if (!scan_iter->second.comment().empty()) comment = scan_iter->second.comment();
      else comment = "empty";
      //comment = scan_iter->second.comment();
      file = scan_iter->second.file();

      // 	If searched-for qualities exist
      // 	Compare retrieved qualities with searched-for qualities
      if ((searchConnTag == "" || searchConnTag == connTag)
	  && (searchCfgTag == "" || searchCfgTag == cfgTag || searchCfgTag == mod_cfg_tag)
	  && (searchScanID==0 || searchScanID == scanID)) {

	//	std::cout << "scan_iter->second.comment() = " << scan_iter->second.comment() << std::endl;
	//comment = scan_iter->second.comment();
	//	std::cout << "comment = " << comment << std::endl;

	//Output results to QListView
	std::ostringstream scanIDstr;
	scanIDstr.str("");
	scanIDstr << scanID;
        QTreeWidgetItem *item = new QTreeWidgetItem(scanResultListView);
	item->setText(0, QString::fromStdString(scanIDstr.str()));
	item->setText(1, QString::fromStdString(scanType));
	item->setText(2, QString::fromStdString(scanTypeTag));
	item->setText(3, QString::fromStdString(connTag));
	if (cfgTag != mod_cfg_tag && !mod_cfg_tag.empty()) {
	  item->setText(4, QString::fromStdString(cfgTag)+QString::fromStdString(",")+QString::fromStdString(mod_cfg_tag));
	}
	else {
	  item->setText(4, QString::fromStdString(cfgTag));
	}
	item->setText(5, status.c_str());
	item->setText(6, location.c_str());
	item->setText(7, quality.c_str());
	item->setText(8, startString.c_str());
	item->setText(9, Aux::extractFirstSentence(comment).c_str());
	item->setText(10, file.c_str());
	listAnalyses(item);
      }
    }//end loop over scans
    //    scanResultListView->adjustSize();
  }
  catch (CAN::PixMetaException &err) {
    std::cerr << "WARNING [SearchDbCoralForm::searchScan] Caught exception :" << err.what() << std::endl;
  }
  catch(...) {
    std::cerr << "WARNING [SearchDbCoralForm::searchScan] Error while trying to get meta data."  << std::endl;
  }

}

void SearchDbCoralForm::listAnalyses(QTreeWidgetItem *scan_item)
{
  if (scan_item && scan_item->text(0).toLatin1().data()) {
    std::cout << "INFO [SearchDbCoralForm::listAnalyses] " << scan_item->text(0).toLatin1().data()  <<" : "
          << " " << static_cast<void *>(scan_item->child(0)) << std::endl;

    if (!scan_item->child(0)) {
      //      scan_item->setOpen(!scan_item->isOpen());
      //    }
      //    else {
      CAN::PixCoralDbClient a_coral_client(true);

      unsigned int scan_serial_number = atoi(scan_item->text(0).toLatin1().data());
      if (scan_serial_number>0) {
	std::map<CAN::SerialNumber_t, CAN::AnalysisInfo_t> analysis_list;
	analysis_list = a_coral_client.searchAnalysisMetadata(scan_serial_number);
	if (!analysis_list.empty()) {
	  for (std::map<CAN::SerialNumber_t, CAN::AnalysisInfo_t>::const_iterator analysis_iter = analysis_list.begin();
	       analysis_iter != analysis_list.end();
	       analysis_iter++) {

	    //Output results to QListView
        QTreeWidgetItem *item = new QTreeWidgetItem(scan_item);
	    item->setText(0, PixCon::makeSerialNumberString(PixCon::kAnalysis, analysis_iter->first).c_str() );
	    item->setText(1, analysis_iter->second.name(CAN::kAlg).c_str());
	    item->setText(2, analysis_iter->second.tag(CAN::kAlg).c_str());
	    item->setText(3, analysis_iter->second.name(CAN::kClassification).c_str());
	    item->setText(4, analysis_iter->second.name(CAN::kPostProcessing).c_str());
	    item->setText(5, status_name_list[analysis_iter->second.status()].c_str());
	    item->setText(6, "");
	    item->setText(7, quality_name_list[analysis_iter->second.quality()].c_str());
	    item->setText(8, analysis_iter->second.analysisStartString().c_str());
	    item->setText(9, Aux::extractFirstSentence(analysis_iter->second.comment()).c_str());
	    item->setText(10, "");
	  }
      //scan_item->setOpen(false);
	}
      }
    }
  }
}

void SearchDbCoralForm::searchAnalysis()
{
  try {
    std::unique_ptr<CAN::PixCoralDbClient> a_coral_client;
    {
      // search should be performed in the background.
      std::unique_ptr<PixA::IBusyLoop> busy_loop(PixA::BusyLoopFactory::busyLoop(" Connect to database ..."));
      a_coral_client=std::make_unique<CAN::PixCoralDbClient>( true );
    }
    if (!a_coral_client.get()) {
      std::cerr << "FATAL [SearchDbCoralForm::searchScan] Failed to connect to database. " << std::endl;
      return;
    }

    std::map<CAN::SerialNumber_t, CAN::AnalysisInfo_t> analysis_list;
    if (searchAnalysisID>0) {
      analysis_list.insert(std::make_pair(searchAnalysisID, a_coral_client->getMetadata<CAN::AnalysisInfo_t>(searchAnalysisID)));
    }
    else {
      if (searchScanID>0) {
	analysis_list = a_coral_client->searchAnalysisMetadata(searchScanID,
							      -1,
							      -1,
							      "",
							      "",
							      "",
							      "",
							      "",
							      "",
							      CAN::kUndefined,
							      CAN::kNotSet,
							      "");
      }
      else {
      analysis_list = a_coral_client->searchAnalysisMetadata(searchScanID,
							    searchBeginTime,
							    searchEndTime,
							    searchAnalysisType,
							    tag,
							    searchClassificationName,
							    searchClassificationTag,
							    searchPostName,
							    searchPostTag,
							    searchQuality,
							    searchStatus,
							    searchComment,
							    searchLocation);
      }
    }

    for (std::map<CAN::SerialNumber_t, CAN::AnalysisInfo_t>::const_iterator analysis_iter = analysis_list.begin();
	 analysis_iter != analysis_list.end();
	 analysis_iter++) {
   
      //Retrieve attributes of analysis
      unsigned int analysisID = analysis_iter->first;
      unsigned int scanID = analysis_iter->second.scanSerialNumber();
      analysisTag = analysis_iter->second.tag(CAN::kAlg);
      analysisType = analysis_iter->second.name(CAN::kAlg);
      status = status_name_list[analysis_iter->second.status()];
      quality = quality_name_list[analysis_iter->second.quality()];
      startString = analysis_iter->second.analysisStartString();
      classificationName = analysis_iter->second.name(CAN::kClassification);
      classificationTag = analysis_iter->second.tag(CAN::kClassification);
      postName = analysis_iter->second.name(CAN::kPostProcessing);
      postTag = analysis_iter->second.tag(CAN::kPostProcessing);
      comment = analysis_iter->second.comment();
      file = analysis_iter->second.file();

      if  ( (searchScanID==0 || searchScanID == scanID) &&
	    (searchAnalysisID==0 || searchAnalysisID == analysisID) ){

	std::ostringstream analysisIDstr;
	analysisIDstr.str("");
	analysisIDstr << analysisID;
	scanResultListView->clear();
	std::ostringstream scanIDstr;
	scanIDstr.str("");
	scanIDstr << scanID;

	//Output results to QListView
        QTreeWidgetItem *item = new QTreeWidgetItem(analysisListView);
	item->setText(0, QString::fromStdString(analysisIDstr.str()));
	item->setText(1, QString::fromStdString(scanIDstr.str()));
	item->setText(2, analysisType.c_str());
	item->setText(3, analysisTag.c_str());
	item->setText(4, status.c_str());
	item->setText(5, quality.c_str());
	item->setText(6, startString.c_str());
	item->setText(7, classificationName.c_str());
	item->setText(8, classificationTag.c_str());
	item->setText(9, postName.c_str());
	item->setText(10, postTag.c_str());
	item->setText(11, Aux::extractFirstSentence(comment).c_str());
	item->setText(12, file.c_str());
      }
    }//end loop over analyses
  }
  catch (CAN::PixMetaException &err) {
    std::cerr << "WARNING [SearchDbCoralForm::searchAnalysis] Caught exception :" << err.what() << std::endl;
  }
  catch(...) {
    std::cerr << "WARNING [SearchDbCoralForm::searchAnalysis] Error while trying to get meta data."  << std::endl;
  }
}

void SearchDbCoralForm::loadSelectedPvt(QTreeWidgetItemIterator &item_iter, PixCon::EMeasurementType default_measurement_type) {
  searchPushButton->setDefault(true);
  getSelectedPushButton->setDefault(false);

  if (!m_ohViewer) return;

  while ( (*item_iter) ){
    if ( (*item_iter)->isSelected() ) {
      if ((*item_iter)->text(0).length()>0 && (*item_iter)->text(0).toLatin1().data()) {
    std::string id_string = (*item_iter)->text(0).toLatin1().data();
	if (!id_string.empty()) {
	  std::pair<PixCon::EMeasurementType, PixCon::SerialNumber_t> measurement = std::make_pair(default_measurement_type, 0);
	  if (isdigit(id_string[0])) {
	    measurement.second = atoi(id_string.c_str());
	  }
	  else {
	    measurement = PixCon::stringToSerialNumber(id_string);
	  }
	  if (measurement.second>0) {
	    if (measurement.first == PixCon::kScan) {
	      std::cout << "INFO [SearchDbCoralForm::loadSelected] add scan " << makeSerialNumberName(measurement.first, measurement.second) << std::endl;
	      m_ohViewer->addScan(measurement.second);
	    }
	    else if (measurement.first == PixCon::kAnalysis) {
	      std::cout << "INFO [SearchDbCoralForm::loadSelected] add analysis" << makeSerialNumberName(measurement.first, measurement.second) << std::endl;
	      m_ohViewer->addAnalysis(measurement.second);
	    }
	    else {
	      std::cout << "INFO [SearchDbCoralForm::loadSelected] Do not know what to do with  " << makeSerialNumberName(measurement.first, measurement.second) << std::endl;
	    }

 	    if (measurement.first == PixCon::kScan || measurement.first == PixCon::kAnalysis) {
          (*item_iter)->setSelected(false);
         // (*item_iter)->setEnabled(false);
	    }

	  }
	}
      }
    }

    // children-items (i.e. analysis to a scan) will be covered by iterator increment.
    // the following creates several nested loops (analysis-iterator will *not* stop at 
    //  end of children list!) which takes ages to process
//     if ((*item_iter)->child(0)) {
//       QTreeWidgetItemIterator child_iter((*item_iter)->child(0));
//       if(*child_iter){
//         loadSelectedPvt(child_iter,default_measurement_type);
//       }
//     }

    ++item_iter;
  }
}

void SearchDbCoralForm::loadSelected()
{
  if (scan) {
    QTreeWidgetItemIterator scanit(scanResultListView);
    if(*scanit){
        loadSelectedPvt(scanit, PixCon::kScan);
    }
  }

  if (analysis) {
    QTreeWidgetItemIterator analysisit(analysisListView);
    if(*analysisit){
        loadSelectedPvt(analysisit, PixCon::kAnalysis);
    }
  }

}

void SearchDbCoralForm::getSearchFormat() 
{
  enum SearchFormat{
    Scan,
    Analysis
  };
  
  switch (searchComboBox->currentIndex() ) {
  case Scan:
    scan=true;
    analysis=false;
    break;
  case Analysis:
    analysis=true;
    scan=false;
    break;
  }
}


void SearchDbCoralForm::getSearchAnalysisID() 
{
  //if (searchAnalysisIDLineEdit->isShown() && searchAnalysisIDLineEdit->isVisible()) {
  if ( searchAnalysisIDLineEdit->isVisible()) {
    searchAnalysisIDstring = (searchAnalysisIDLineEdit->text()).toLatin1().data();
    if (searchAnalysisIDstring == "") searchAnalysisID=0;
    else searchAnalysisID = atoi(searchAnalysisIDstring.c_str());
  }
}

void SearchDbCoralForm::getSearchAnalysisType()
{
 searchAnalysisType=searchAnalysisTypeComboBox->currentText().toLatin1().data();
 if (searchAnalysisType == "Any") searchAnalysisType="";
}


void SearchDbCoralForm::getSearchClassificationName(){
 searchClassificationName = (searchClassificationNameLineEdit->text()).toLatin1().data();
}

void SearchDbCoralForm::getSearchClassificationTag(){
 searchClassificationTag = (searchClassificationTagLineEdit->text()).toLatin1().data();
}

void SearchDbCoralForm::getSearchPostName(){
  searchPostName = (searchPostProcessingNameLineEdit->text()).toLatin1().data();
}

void SearchDbCoralForm::getSearchPostTag(){
  searchPostTag = (searchPostProcessingTagLineEdit->text()).toLatin1().data();
}

void SearchDbCoralForm::getSearchScanType()
{
  searchScanType=searchScanTypeComboBox->currentText().toLatin1().data(); 
  if (searchScanType == "Any") searchScanType="";
}


void SearchDbCoralForm::getSearchID() 
{
  searchIDstring = (searchScanIDLineEdit->text()).toLatin1().data();
  std::string::size_type pos=0;
  searchAnalysisID=0;
  searchScanID=0;
  while (pos<searchIDstring.size() && isspace(searchIDstring[pos])) pos++;
  if (pos>= searchIDstring.size()) {
    searchScanID=0;
    searchAnalysisID=0;
  }
  else {
    unsigned int *serial_number_ptr = &searchScanID;
    getSearchFormat();
    bool explicit_measurement_type=false;
    if (analysis) {
      serial_number_ptr = &searchAnalysisID;
    }

    if (searchIDstring[pos] == 'S' || searchIDstring[pos] == 's') {
      serial_number_ptr = &searchScanID;
      searchAnalysisID=0;
      explicit_measurement_type=true;
      pos++;
    }
    else if (searchIDstring[pos] == 'A' || searchIDstring[pos] == 'a') {
      serial_number_ptr = &searchAnalysisID;
      searchScanID=0;
      explicit_measurement_type=true;
      pos++;
    }
    *serial_number_ptr = atoi(&(searchIDstring.c_str()[pos]));
    if (!explicit_measurement_type) {
      std::stringstream explicit_id_string;
      explicit_id_string << (analysis ? 'A' : 'S') << *serial_number_ptr;
      searchScanIDLineEdit->setText(explicit_id_string.str().c_str());
    }
  }
}

void SearchDbCoralForm::getSearchLocation() 
{
  enum LocationType{
    Any,
    ToothPix,
    Pit,
    PixelLab
  };
  switch ( searchLocationComboBox->currentIndex() ) {
  case Any:
    searchLocation = CAN::kNowhere;
    break;
  case ToothPix:
    searchLocation = CAN::kSR1;
    break;
  case Pit:
    searchLocation = CAN::kPIT;
    break;
  case PixelLab:
    searchLocation = CAN::kPixelLab;
    break;
  }
}


void SearchDbCoralForm::getSearchQuality()
{
  enum QualityType{
    Any,
    Normal,
    Debug,
    Trash
  };
  switch ( searchQualityComboBox->currentIndex() ) {
  case Any:
    searchQuality=CAN::kUndefined;
    break;
  case Normal:
    searchQuality=CAN::kNormal;
    break;
  case Debug:
    searchQuality=CAN::kDebug;
    break;
  case Trash:
    searchQuality=CAN::kTrash;
    break;
  }
}

void SearchDbCoralForm::getSearchStatus()
{
  enum StatusType{
    Any,
    Success,
    Failed,
    Aborted
  };
 switch ( searchStatusComboBox->currentIndex() ) {
  case Any:
    searchStatus=CAN::kNotSet;
    //CAN::GlobalStatus searchStatus;
    break;
  case Success:
    searchStatus=CAN::kSuccess;
    break;
  case Failed:
    searchStatus=CAN::kFailure;
    break;
  case Aborted:
    searchStatus=CAN::kAborted;
    break;
  }
}

void SearchDbCoralForm::getSearchConnTag()
{
  searchConnTag = (searchConnTagLineEdit->text()).toLatin1().data();
}

void SearchDbCoralForm::getSearchCfgTag()
{
  searchCfgTag = (searchCfgTagLineEdit->text()).toLatin1().data();
}

void SearchDbCoralForm::getSearchRodModule()
{
  searchRodModule = (searchRodModuleLineEdit->text()).toLatin1().data();
}

time_t SearchDbCoralForm::parseTime(const std::string &time_str)
{
  if (time_str.empty() || time_str=="now" || time_str=="NOW") {
    return time(0);
  }

  if (time_str[0]=='-') {
    char *end_ptr=0;
    long value = strtol(time_str.c_str(),&end_ptr,10);
    time_t now=time(0);
    switch( *end_ptr) {
    case 'w': {
      return now + value*3600*24*7;
    }
    case 'd': {
      return now + value*3600*24;
    }
     case 'h': {
      return now + value*3600;
    }
    case 's':
    default: {
      return now + value;
    }
    }
  }
  else {
    const char *start_time = time_str.c_str();
    bool time_is_given_in_utc = false;
    if (time_str.compare(0,3, "utc")==0 || time_str.compare(0,3, "UTC")==0) {
      time_is_given_in_utc=true;
      start_time+=3;
      while (isspace(*start_time)) start_time++;
    }

    struct ::tm time_struct = {0,0,0,0,0,0,0,0,0,0,0};
    // supported formats
    // Fri Mar 14 20:47:52 2008
    // Mar 14 20:47:52 2008
    // 2008-04-07 06:47:16
    std::vector<std::string> time_formats;
    time_formats.push_back("%a %b %d %H:%M:%S %Y");
    time_formats.push_back("%a %b %d %H:%M:%S %Y");
    time_formats.push_back("%b %d %H:%M:%S %Y");
    time_formats.push_back("%Y-%m-%d %H:%M:%S");
    time_formats.push_back("%Y-%m-%d %H:%M");
    time_formats.push_back("%Y-%m-%d");
    for (std::vector<std::string>::const_iterator format_iter = time_formats.begin();
	 format_iter != time_formats.end();
	 format_iter++) {
      char *end_ptr = strptime(start_time, format_iter->c_str(),&time_struct);
      std::cout << *format_iter << static_cast<void *>(end_ptr) << " " << static_cast<unsigned long>(end_ptr - start_time) << std::endl;
      if (end_ptr) {
        std::cout << " last_char = \"" << *end_ptr << "\" " << static_cast<unsigned int>(*end_ptr) << std::endl;
	while (isspace(*end_ptr)) end_ptr++;
	if (*end_ptr=='\0' || ((strncmp(end_ptr,"UTC",3)==0 || strncmp(end_ptr,"utc",3)==0) && *(end_ptr+3)=='\0')) {
	  if (!*end_ptr) {
	    time_is_given_in_utc=true;
	  }
	  time_t utc_time = mktime(&time_struct);
	  if (time_is_given_in_utc) {
	    struct ::tm gm_time_struct = {0,0,0,0,0,0,0,0,0,0,0};
	    gmtime_r( &utc_time , &gm_time_struct);
	    utc_time = mktime(&gm_time_struct);
	  }
	  std::cout << " input = " << time_str << std::endl;
	  std::cout << " utc = " << ctime(&utc_time) << std::endl;
	  return utc_time;
	}
      }
    }
  }
  // @todo should throw an exception  and the calling code should indicate the error to the user
  //     one possibility would be to use the highlighter (ScanSerialNumberDialog) to temporarily change 
  //     the background colour of the date field to e.g. yellow
  return time(0);
}

void SearchDbCoralForm::getSearchBeginTime()
{
  std::cout << fromDateEdit->date().toString(Qt::ISODate).toUtf8().constData()<<std::endl;
  std::ostringstream beginTime_strstream;
  beginTime_strstream.str("");
  beginTime_strstream << (fromDateEdit->date().toString(Qt::ISODate)).toUtf8().constData() << "00:00:00";
  beginTime_string = beginTime_strstream.str();
  searchBeginTime = parseTime(beginTime_string);

}

void SearchDbCoralForm::getSearchEndTime()
{
  std::cout << toDateEdit->date().toString(Qt::ISODate).toUtf8().constData()<<std::endl;
  std::ostringstream endTime_strstream;
  endTime_strstream.str("");
  endTime_strstream << toDateEdit->date().toString(Qt::ISODate).toUtf8().constData() << "23:59:59";
  endTime_string = endTime_strstream.str();
  searchEndTime = parseTime(endTime_string);

}


void SearchDbCoralForm::getSearchComment(){
 searchComment = (searchCommentLineEdit->text()).toLatin1().data();
}

void SearchDbCoralForm::focusListView() {
  if (scan) {
    std::cout << "INFO [SearchDbCoralForm::focusListView] focus scan." << std::endl;
    scanResultListView->setFocus();
  }
  else if (analysis) {
    std::cout << "INFO [SearchDbCoralForm::focusListView] focus analysis." << std::endl;
    analysisListView->setFocus();
  }

}

