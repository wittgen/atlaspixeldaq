#ifndef _SearchDbCoralForm_h_
#define _SearchDbCoralForm_h_

#include "ui_searchdbcoralformbase.h"
#include <memory>
#include "CoralDbClient.hh"
#include <cstdlib>
#include <string>
#include <qstring.h>
#include <vector>
#include <IConfigDb.hh>
#include <CalibrationDataTypes.h>
#include <QTreeWidgetItemIterator>

namespace PixCon {
  class OHViewer;
}


class SearchDbCoralForm : public QDialog, public Ui_SearchDbCoralFormBase
{
  Q_OBJECT
    
    public:
  enum EType { kScan, kAnalysis, kNTypes};
  SearchDbCoralForm(  PixCon::OHViewer *ohViewer, QWidget* parent = 0 );
  ~SearchDbCoralForm();

  void listAnalyses(QTreeWidgetItem *);

  public slots:
    void search();
    void destroyForm() { }
    void loadSelected();
    void resetComboBoxes();

    void selectScanSearch();
    void selectAnalysisSearch();

    void focusListView();
  void checkToggleNoAnalysis();
  void checkToggleAnalysis();

 protected:
  void init();
  void fillTypeComboBox(int type);
  void searchScan();
  void searchAnalysis();

  //get common search terms
  void getSearchFormat();
  void getSearchID();
  time_t parseTime(const std::string &time_str);
  void getSearchBeginTime();
  void getSearchEndTime();
  void getSearchStatus();
  void getSearchComment();

  //get analysis search terms
  void getSearchAnalysisType();
  void getSearchAnalysisID();
  void getSearchClassificationName();
  void getSearchClassificationTag();
  void getSearchPostName();
  void getSearchPostTag();

  //get scan search terms
  void getSearchScanType();
  void getSearchLocation();
  void getSearchQuality();
  void getSearchConnTag();
  void getSearchCfgTag();
  void getSearchRodModule();


  void loadSelectedPvt(QTreeWidgetItemIterator &item_iter, PixCon::EMeasurementType default_measurement_type);

  std::map<CAN::ScanLocation, std::string> location_name_list;
  std::map<CAN::EQuality,std::string>      quality_name_list;
  std::map<CAN::GlobalStatus,std::string>  status_name_list; 

  std::string tag;//empty string needed to make searchScanMetaData happy
  bool scan;
  bool analysis;

  //Common attributes of results
  std::string startString;
  std::string status;
  std::string comment;
  std::string file;

  //Attributes of analysis results
  std::string analysisType;
  std::string analysisTag;
  std::string classificationName;
  std::string classificationTag;
  std::string postName;
  std::string postTag;
 
  //Attributes of scan results
  std::string scanType;
  std::string scanTypeTag;
  std::string location;
  std::string quality;
  std::string connTag;
  std::string cfgTag;

  //common search terms
  std::string searchIDstring;
  unsigned int searchScanID;
  time_t searchBeginTime;
  time_t searchEndTime;
  int beginDay;
  int beginMonth;
  int beginYear;
  std::string beginTime_string;
  int endDay;
  int endMonth;
  int endYear;
  std::string endTime_string;
  CAN::GlobalStatus searchStatus;
  std::string searchComment;

  //analysis search terms
  std::string searchAnalysisType;
  unsigned int searchAnalysisID;
  std::string searchAnalysisIDstring;
  std::string searchClassificationName;
  std::string searchClassificationTag;
  std::string searchPostName;
  std::string searchPostTag;

  //scan search terms
  std::string searchScanType;
  CAN::ScanLocation searchLocation;
  CAN::EQuality searchQuality;
  std::string searchConnTag;
  std::string searchCfgTag;
  std::string searchRodModule;

  PixCon::OHViewer *m_ohViewer;
  std::shared_ptr<CAN::IConfigDb> m_configDb[2];
  QComboBox *m_searchTypeComboBox[2];
  const char *s_className[4];
  unsigned int m_hasAnalysis;

};
#endif
