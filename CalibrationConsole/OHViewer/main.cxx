#include <qapplication.h>
#if QT_VERSION < 0x050000
  #include <QPlastiqueStyle>
#endif
#include "OHViewerApp.h"
#include <CalibrationDataTypes.h>
#include "UserMode.h"

#include <memory>
#include <vector>
#include <iostream>

#include <VisualisationRegister.h>
#include <ModuleMapH2Factory.h>

#if  defined(HAVE_QTGSI)
#include "TQRootApplication.h"
#include "TQApplication.h"
#else
#include <TApplication.h>
#endif
#include <TRint.h>
#include <TEnv.h>
#include <TSystem.h>
#include <TROOT.h>

#include <ipc/core.h>
#include <ConfigWrapper/ConnectivityManager.h>
#include <ConfigWrapper/createDbServer.h>


const std::string s_appName("CTViewer");
  
int main( int argc, char ** argv )
{
#if QT_VERSION < 0x050000
  QApplication::setStyle(new QPlastiqueStyle());
#else
  QApplication::setStyle("fusion");
#endif

    std::shared_ptr<PixCon::UserMode> user_mode(new PixCon::UserMode(PixCon::UserMode::kShifter));
    std::string partition_name;
    //    std::string object_tag_name="CT";
    //    std::string tag_name;
    //    std::string cfg_tag_name;
    std::string is_server_name = "PixFakeData";
    std::string oh_server_name = "PixelHistoServer";
    std::string oh_partition_name;
    std::string histo_name_server_name;
    std::string db_server_partition_name;
    std::string db_server_name;

    bool error=false;

    std::vector<PixCon::SerialNumber_t> serial_numbers;
    IPCCore::init(argc, argv);

    for (int arg_i=1; arg_i<argc; arg_i++) {
      if (strcmp(argv[arg_i],"-e")==0) {
	user_mode->setUserMode(PixCon::UserMode::kExpert);
      }
      else if (strcmp(argv[arg_i],"-p")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	partition_name = argv[++arg_i];
      }
      else if (strcmp(argv[arg_i],"-o")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	oh_server_name = argv[++arg_i];
      }
      else if (strcmp(argv[arg_i],"--oh-partition")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	oh_partition_name = argv[++arg_i];
      }
      //      else if (strcmp(argv[arg_i],"-i")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      //	object_tag_name = argv[++arg_i];
      //      }
      //      else if (strcmp(argv[arg_i],"-t")==0 && arg_i+2<argc && argv[arg_i+2][0]!='-' && argv[arg_i+1][0]!='-') {
      //	tag_name = argv[++arg_i];
      //	cfg_tag_name = argv[++arg_i];
      //      }
      else if (strcmp(argv[arg_i],"-n")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	is_server_name = argv[++arg_i];
      }
      else if ((strcmp(argv[arg_i],"--histo-name-server")==0 || strcmp(argv[arg_i],"-m")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	histo_name_server_name = argv[++arg_i];
      }
      else if ( (strcmp(argv[arg_i],"-D")==0
		 || strcmp(argv[arg_i],"--db-server-name")==0)
		&& arg_i+1<argc && argv[arg_i+1][0]!='-') {
	db_server_name = argv[++arg_i];
      }
      else if ( (strcmp(argv[arg_i],"--db-server-partition")==0
		 || strcmp(argv[arg_i],"-p")==0)
		&& arg_i+1<argc && argv[arg_i+1][0]!='-') {
	db_server_partition_name = argv[++arg_i];
      }
      else if (strcmp(argv[arg_i],"-s")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	while (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	  ++arg_i;
	  unsigned int a_serial_number=atoi(argv[arg_i]);
	  if (a_serial_number>0) {
	    serial_numbers.push_back( a_serial_number );
	  }
	}
      }
      else {
	std::cout << "ERROR ["<< argv[0] << ":main] unhandled argument " << arg_i << " : " << argv[arg_i]<< "." << std::endl;
	error=true;
      }
    }

    if (error) {
      std::cout << "usage: " << argv[0] << "[-p partition] [-o OH server name] [--oh-partition OH partition name]  [-n \"IS server name\"]"  << std::endl
		<< "\t[-D/--db-server-partition db-server-partition] [--db-server-name db-server-name]"
		<< std::endl;
      return -1;
    }

    int ret=0;

    // QT-ROOT application
#if  defined(HAVE_QTGSI)
    int dummy_argc=1;
    TQApplication root_app(s_appName.c_str(),&dummy_argc,argv);
    
    // process root logon script
    // the logonscript could for example define some proper styles.
    
    std::cout << gEnv->GetValue("Rint.Logon", (char*)0) << std::endl;
    const char *logon = gEnv->GetValue("Rint.Logon", (char*)0);
    if (logon) {
      char *mac = gSystem->Which(TROOT::GetMacroPath(), logon, kReadPermission);
      if (mac) {
	root_app.ProcessFile(logon);
      }
      delete [] mac;
    }
#else
    int dummy_argc=1;
    TApplication root_app(s_appName.c_str(),&dummy_argc,argv);
    
    // process root logon script
    // the logonscript could for example define some proper styles.
    
    std::cout << gEnv->GetValue("Rint.Logon", (char*)0) << std::endl;
    const char *logon = gEnv->GetValue("Rint.Logon", (char*)0);
    if (logon) {
      char *mac = gSystem->Which(TROOT::GetMacroPath(), logon, kReadPermission);
      if (mac) {
	root_app.ProcessFile(logon);
      }
      delete [] mac;
    }
#endif
    if (db_server_partition_name.empty() && !partition_name.empty()) {
      db_server_partition_name = partition_name;
    }

    std::unique_ptr<IPCPartition> db_server_partition;
    if (!db_server_name.empty() && !db_server_partition_name.empty()) {
      db_server_partition = std::unique_ptr<IPCPartition>(new IPCPartition(db_server_partition_name.c_str()));
      if (!db_server_partition->isValid()) {
	std::cerr << "ERROR [" << argv[0] << ":main] Not a valid partition " << db_server_partition_name
		  << " for the db server." << std::endl;
	return EXIT_FAILURE;
      }

      std::shared_ptr<PixLib::PixDbServerInterface> db_server( PixA::createDbServer(*db_server_partition,
										      db_server_name ));
      if (db_server.get()) {
	PixA::ConnectivityManager::useDbServer( db_server );
      }
      else {
	std::cout << "WARNING [main:" << argv[0] << "] Failed to create the db server." << std::endl;
      }
    }

    std::unique_ptr<QApplication> app;
    {
      std::shared_ptr<PixA::IModuleMapH2Factory> factory(new PixA::ModuleMapH2Factory);
      PixA::VisualisationRegister::registerMAVisualisers(factory); 
            OHViewerApp *oh_viewer=new OHViewerApp(1, argv, user_mode, partition_name, is_server_name, oh_partition_name, oh_server_name, histo_name_server_name);


      for (std::vector<PixCon::SerialNumber_t>::const_iterator serial_number_iter=serial_numbers.begin();
	   serial_number_iter != serial_numbers.end();
	   serial_number_iter++) {
	oh_viewer->addScan(*serial_number_iter);
      }
      app = std::unique_ptr<QApplication>(oh_viewer);
      //      if (!object_tag_name.empty() && !tag_name.empty() && !cfg_tag_name.empty()) {
      //	
      //       oh_viewer->setTags(object_tag_name, tag_name,cfg_tag_name);
      //      }
    }

    ret  = (app.get() ? app->exec() : -1);
    return ret;
}
