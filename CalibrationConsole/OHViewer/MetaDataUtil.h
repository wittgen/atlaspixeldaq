#ifndef _PixCon_MetaDataUtil_h_
#define _PixCon_MetaDataUtil_h_

#include <CalibrationDataTypes.h>

#include <vector>
#include <CalibrationDataManager.h>

namespace CAN {
  class AnalysisInfo_t;
}

namespace PixCon {

  class CalibrationDataManager;

  /** Auxiliary method to load and set the scan meta data.
   */
  bool loadScanMetaData(CalibrationDataManager &calibration_data_manager, SerialNumber_t scan_serial_number);

  /** Auxiliary method to load the analysis meta data.
   */
  CAN::AnalysisInfo_t getAnalysisInfo(SerialNumber_t analysis_serial_number);

  /** Auxiliary method to set the analysis meta data.
   */
  void setAnalysisInfo(CalibrationDataManager &calibration_data_manager, 
		       SerialNumber_t analysis_serial_number,
		       const CAN::AnalysisInfo_t *analysis_info);

  std::vector <PixA::ConfigHandle> getScanConfigVector(SerialNumber_t scan_serial_number);
}

#endif
