#include "OHHistogramProvider.h"
#include <oh/OHRootReceiver.h>
#include <oh/OHIterator.h>
#include "PixScanHistoIndexer.h"

namespace PixCon {


  class PixScanHistoReceiver : public OHRootReceiver
  {
  public:
    PixScanHistoReceiver( /*std::vector< std::string> &histo_name_list,*/
			 std::map< std::string, std::map<std::string, ExtendedHistoInfo_t> >  &module_histo_info_map,
			 std::map< std::string, std::map<std::string, ExtendedHistoInfo_t> >  &rod_histo_info_map,
			 bool verbose)
      : m_indexer(module_histo_info_map,rod_histo_info_map, verbose) {}


    void receive( OHRootHistogram & h ) {
      std::string histogram_name( h.histogram->GetName() );
      if (histogram_name.size()<2) {
	std::cout << "ERROR [PixScanHistoIndex::receive] Histogram name too short: " << histogram_name << "." << std::endl;
	return;
      }
      m_indexer.add(histogram_name);

    }

    void receive( std::vector<OHRootHistogram*> & h_array) {

      for (std::vector<OHRootHistogram*>::iterator histo_iter=h_array.begin();
	   histo_iter != h_array.end();
	   histo_iter++) {

	receive(*(*histo_iter));

      }

    }

    /** Does nothing intentionally. */
    void receive( OHRootGraph & ) {}

    /** Does nothing intentionally. */
    void receive( OHRootGraph2D & ) {}
  private:
    PixScanHistoIndexer m_indexer;
  };



  void OHHistogramProvider::updateHistogramList(EMeasurementType type,
						SerialNumber_t serial_number,
						EConnItemType /*conn_item_type*/,
						const std::string &rod_name )
  {
    if (type != kScan && type != kAnalysis) return;

    std::stringstream pix_histo_name;
    if (type == kScan)     { pix_histo_name << "/*S";}
    if (type == kAnalysis) { pix_histo_name << "/*A";}
    pix_histo_name << std::setw(9) << std::setfill('0') << serial_number << '/';
    if (rod_name.size()>0) {
      pix_histo_name << rod_name << "/";
    }

    pix_histo_name << ".*";

    try {
      //if (m_verbose) {
      if (true){
    std::cout << "INFO [HistoReceiver::getHistograms] Search for histograms " << pix_histo_name.str() << " in " << m_ohServerName << "." << std::endl;
    }
    OHHistogramIterator  histo_iter( *m_partition,
				     m_ohServerName,
				     ".*" /* @todo Care about the provider name ? */,
				     pix_histo_name.str());
    {
    Lock lock( histoMutex() );
    HistoData_t &histo_data = histoData(type,serial_number);

    PixScanHistoReceiver receiver(histo_data[kModule], histo_data[kRod], m_verbose);
    //    unsigned int counter=0;
    while ( histo_iter++ ) {
      std::cout<<"OHHistogramProvider.cxx is touching "<<histo_iter.name()<<std::endl;
      histo_iter.retrieve( receiver );
    }
    if (m_verbose) {
      std::cout << "INFO [OHHistogramProvider::requestHistogramList] histograms module  = " << histo_data[kModule].size()
		<< " rod=" << histo_data[kRod].size()
		<< "." << std::endl;
    }
    histo_data.setUptodate(rod_name);
    }
    }
    catch(daq::is::Exception &err) {
      std::cout << "Error [OHHistogramProvider::requestHistogramList] Caught exception  : " << err.what()
		<< "." << std::endl;
    }

  }


  class PixScanHistoLoader : public OHRootReceiver {
  public:
    PixScanHistoLoader( )
    {}

    void receive( OHRootHistogram & h )
    {
      m_histo = std::move(h.histogram);
    }

    void receive( std::vector<OHRootHistogram*> & h_array)
    { 
      for (std::vector<OHRootHistogram*>::iterator histo_iter=h_array.begin();
	   histo_iter != h_array.end();
	   histo_iter++) {
	receive(*(*histo_iter));
      }
    }

    /** Does nothing intentionally. */
    void receive( OHRootGraph & ) {}

    /** Does nothing intentionally. */
    void receive( OHRootGraph2D & ) {}

    HistoPtr_t histo() { return m_histo.release(); }
  private:
    std::unique_ptr<Histo_t> m_histo;
  };

  HistoPtr_t OHHistogramProvider::histogram(const std::string &histo_name, const std::vector<unsigned int> &index)
  {
    std::stringstream pix_histo_name;
    pix_histo_name << ".*" << histo_name;
    char level_marker = 'A';
    for (Index_t::const_iterator index_iter = index.begin();
	 index_iter != index.end();
	 index_iter++) {
      if (index.end() - index_iter > 1) {
	pix_histo_name << level_marker << *index_iter  << "/";
	level_marker++;
      }
      else {
	pix_histo_name << *index_iter; 
      }
    }
    pix_histo_name << ":.*";


    if (m_verbose) {
      std::cout << "INFO [OHHistogramProvider::histogram] name= " << pix_histo_name.str() << std::endl;
    }
    PixScanHistoLoader receiver;
    OHHistogramIterator  histo_iter( *m_partition,
 				     m_ohServerName,
 				     ".*" /* @todo Care about the provider name ? */,
 				     pix_histo_name.str() );
    while ( histo_iter++ ) {
      histo_iter.retrieve( receiver );
      return receiver.histo();
    }
    return NULL;

  }

}
