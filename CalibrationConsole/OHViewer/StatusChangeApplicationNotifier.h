/* Dear emacs, this is -*-c++-*- */
#ifndef _PixCon_StatusChangeApplicationNotifier_h_
#define _PixCon_StatusChangeApplicationNotifier_h_

#include "StatusChangeTransceiver.h"
#include "StatusChangeEvent.h"
#include <qapplication.h>

#include "ConnectivityEvents.h"

namespace PixCon {

  class StatusChangeApplicationNotifier : public IStatusChangeForwarder
  {
  public:
    StatusChangeApplicationNotifier(QApplication &app, QObject &receiver)
      : m_app(&app),
	m_receiver(&receiver)
    {}

    void statusChanged(EMeasurementType measurement_type, SerialNumber_t serial_number) {
      m_app->postEvent(m_receiver, new StatusChangeEvent(measurement_type, serial_number));
    }

    void connectivityChanged() {
      m_app->postEvent(m_receiver, new QConnectivityChangedEvent);
    }

  private:
    QApplication *m_app;
    QObject      *m_receiver;
  };

}
#endif
