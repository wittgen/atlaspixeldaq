#include <CoralDbClient.hh>

//***************
#include <qfileinfo.h>
#include "PluginManager/DirIter.hh"
//********************

#include <PixDbGlobalMutex.hh>
#include <ScanConfigDb.hh>
#include <ObjectConfigDbInstance.hh>
#include <DataContainer/RootResultFileAccessBase.h>
#include <ConvertGlobalStatus.h>
#include <CalibrationDataManager.h>
#include <PixConnectivity/PixConnectivity.h>

#include <ConfigWrapper/PixDisableUtil.h>
#include "TRegexp.h"
//#include <MetaDataUtil.h>

namespace PixCon {

  bool loadScanMetaData(CalibrationDataManager &calibration_data_manager, SerialNumber_t scan_serial_number)
  {
    CAN::PixCoralDbClient meta_data_client(false);
    PixLib::ScanInfo_t scan_info = meta_data_client.getMetadata<PixLib::ScanInfo_t>(scan_serial_number);

    // @todo should also get the ROD status and propaget it to the calibration data container

    CAN::Revision_t best_guess_of_cfg_revision = (scan_info.cfgRev()==0 ? scan_info.scanStart() : scan_info.cfgRev());
    PixA::ConnectivityRef conn;
    {
      Lock globalLock(CAN::PixDbGlobalMutex::mutex());
      try {
	conn =PixA::ConnectivityManager::getConnectivity( scan_info.idTag(),
							  scan_info.connTag(),
							  scan_info.aliasTag(),
							  scan_info.dataTag(),
							  scan_info.cfgTag(),
							  scan_info.modCfgTag(),
							  best_guess_of_cfg_revision);
      }
      catch (PixLib::PixConnectivityExc &err) {
	if (err.getId().compare(0,strlen("CANTOPEN"),"CANTOPEN")==0 && err.getId().find("CFG")!=std::string::npos) {
	  conn =PixA::ConnectivityManager::getConnectivity( scan_info.idTag(),
							    scan_info.connTag(),
							    scan_info.aliasTag(),
							    scan_info.dataTag(),
							    "Base",
							    "Base",
							    best_guess_of_cfg_revision);

	  std::cerr << "ERROR [loadScan] Cannot open connectivity with correct config tags : " << scan_info.cfgTag();
	  if (scan_info.modCfgTag() != scan_info.cfgTag() ) {
	    std::cerr << " / " <<  scan_info.modCfgTag();
	  }
	  std::cerr << ". The \"Base\" tag is used instead. Thus incorrect ROD, Boc, module configurations will be shown / used."
		    << std::endl;
	}
      }

    }
    if (!conn) {
      std::cerr << "ERROR [loadScan] Failed to get connectivity for scan " << scan_serial_number << "." << std::endl;
      return false;
    }
    PixA::ConfigHandle scan_config;
    try {
      scan_config = CAN::ScanConfigDb::instance()->config(scan_info.scanType(), scan_info.scanTypeTag(), scan_info.scanTypeRev() );
    }
    catch (CAN::MRSException &err) {
    }
    if (!scan_config) {
      std::cerr << "ERROR [OHViewer::addScan] Failed to get scan configuration for scan " << scan_serial_number << "." << std::endl;
      return false;
    }

    {
      Lock lock(calibration_data_manager.calibrationDataMutex());
      //@todo pass time instead of time string
      calibration_data_manager.addScan(scan_serial_number,
				       scan_info.scanStart(),
				       static_cast<unsigned int>(scan_info.scanEnd() - scan_info.scanStart()),
				       scan_info.scanType(),
				       scan_info.comment(),
				       static_cast<MetaData_t::EQuality>( scan_info.quality() ),
				       scan_config,
				       PixA::RootResultFileAccessBase::fileName( scan_info.file(), scan_info.alternateFile() ),
				       conn);

      try {
	ScanMetaData_t &scan_meta_data  = calibration_data_manager.scanMetaData(scan_serial_number);
	scan_meta_data.setScanConfigTag(scan_info.scanTypeTag());
	scan_meta_data.setScanConfigRevision(scan_info.scanTypeRev());
      }
      catch(...) {
      }


      if (conn) {
	
	PixA::DisabledListRoot disabled_list_root;

	if (scan_info.pixDisableString().empty()) {
	  Lock globalLock(CAN::PixDbGlobalMutex::mutex());
	  std::string disableName("Disable-");
	  disableName += makeSerialNumberName(kScan, scan_serial_number);
	  disabled_list_root = PixA::DisabledListRoot( conn.getDisable(disableName) );
	}
	else {
	  std::cout << "INFO [MetaDataUtil] Take enable information from meta data (legnth=" << scan_info.pixDisableString().size() <<  ") for "
		    << makeSerialNumberString(kScan, scan_serial_number) << ".";
	  disabled_list_root = PixA::createEnableMap(scan_info.pixDisableString());
	}

	calibration_data_manager.setEnable(kScan,scan_serial_number, disabled_list_root);
      }
    }

    if (scan_info.status() != CAN::kNotSet) {
      calibration_data_manager.updateStatus(kScan, scan_serial_number, ConvertGlobalStatus::convertStatus(scan_info.status()));
    }
    return true;
  }

  CAN::AnalysisInfo_t getAnalysisInfo(SerialNumber_t analysis_serial_number)
  {
    CAN::PixCoralDbClient meta_data_client(false);
    return meta_data_client.getMetadata<CAN::AnalysisInfo_t>(analysis_serial_number);
  }

  void setAnalysisInfo(CalibrationDataManager &calibration_data_manager, 
				      SerialNumber_t analysis_serial_number,
				      const CAN::AnalysisInfo_t *analysis_info) {

    Lock lock(calibration_data_manager.calibrationDataMutex());
    calibration_data_manager.addAnalysis(analysis_serial_number,
					 analysis_info->analysisStart(),
					 analysis_info->name(CAN::kAlg),
					 analysis_info->comment(),
					 static_cast<MetaData_t::EQuality>(analysis_info->quality()),
					 analysis_info->scanSerialNumber(),
					 analysis_info->tag(CAN::kAlg),
					 analysis_info->revision(CAN::kAlg),
					 analysis_info->name(CAN::kClassification),
					 analysis_info->tag(CAN::kClassification),
					 analysis_info->revision(CAN::kClassification),
					 analysis_info->name(CAN::kPostProcessing),
					 analysis_info->tag(CAN::kPostProcessing),
					 analysis_info->revision(CAN::kPostProcessing));

  }

  std::vector <PixA::ConfigHandle> getScanConfigVector(SerialNumber_t scan_serial_number)
  {
    std::cout<<"INFO [MetaDataUtil] getScanConfigVector"<<std::endl;
    std::vector <PixA::ConfigHandle> vconfig;

    CAN::PixCoralDbClient meta_data_client(false);
    try{
      PixLib::ScanInfo_t scan_info = meta_data_client.getMetadata<PixLib::ScanInfo_t>(scan_serial_number,true);
      PixA::ConfigHandle scan_config;
      
      std::string tag_name_I3 = scan_info.scanTypeTag();
      //tag_name_I3.insert(tag_name_I3.size(), "_I3");
      vconfig.push_back( CAN::ScanConfigDb::instance()->config(scan_info.scanType(), 
							       tag_name_I3,
							       scan_info.scanTypeRev()) );
      
      // check if IBL ROD exist to find scan config
      bool is_ibl=false;
      std::map<std::string, PixLib::RODstatus_t>::const_iterator itrI = scan_info.statusMap().begin();
      std::map<std::string, PixLib::RODstatus_t>::const_iterator itrE = scan_info.statusMap().end();
      
      for (; itrI != itrE; itrI++) {
	TString rodName = itrI->first;
	TRegexp iblRodReg("ROD_[IC]");
	if (rodName.Contains(iblRodReg)) is_ibl=true;
      }
      
      if (is_ibl) {
	std::string tag_name_I4 = scan_info.scanTypeTag();
	tag_name_I4.insert(tag_name_I4.size(), "_I4");
	
	vconfig.push_back( CAN::ScanConfigDb::instance()->config(scan_info.scanType(),
								 tag_name_I4,scan_info.scanTypeRev()) );
      }
      return vconfig;
    }catch(...){
      return vconfig;
    }
  }

}


