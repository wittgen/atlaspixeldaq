#include "OHViewer.h"
#include <QStatusBar>

#include <OHHistogramProviderBase.h>
#include "MetaHistogramProvider.h"
#include <DataContainer/RootResultFileAccessBase.h>
#include <HistogramStatus.h>

#include "OHLogoFactory.h"

#include <ScanStatusMonitor.h>
#include <ScanIsStatus.hh>
#include "ScanSerialNumberDialog.h"
//#include "SearchDbCoralForm.h"


#include <CoralDbClient.hh>
#include <PixMetaException.hh>
#include <ScanConfigDb.hh>
#include <MonitorThread.h>
#include <IsMonitorManager.h>
#include <IsReceptor.h>
#include "ConvertGlobalStatus.h"

#include <PixDbGlobalMutex.hh>
#include <Status.hh>
#include "AnalysisStatusMonitor.h"

#include <StatusChangeTransceiver.h>
#include <StatusChangeApplicationNotifier.h>

#include <HistogramCache.h>

#include "SearchDbCoralForm.h"

#include <BusyLoop.h>
//#include <qmessagebox.h>

#include "SharedObjectConfigDbInstance.h"

#include "MetaDataUtil.h"

namespace PixCon {

  OHViewer::OHViewer( std::shared_ptr<PixCon::UserMode>& user_mode,
		      const std::string &is_server_name,
		      IPCPartition &is_partition,
		      const std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data,
		      const std::shared_ptr<StatusChangeTransceiver> &status_change_transceiver,
		      OHHistogramProviderBase &oh_histogram_provider,
		      std::shared_ptr<PixCon::MetaHistogramProvider> meta_histogram_provider,
		      const std::shared_ptr<PixCon::HistogramCache> &histogram_cache,
		      const std::shared_ptr<PixCon::CalibrationDataPalette> &palette,
		      const std::shared_ptr<PixCon::CategoryList> &categories,
		      const std::shared_ptr<PixCon::IContextMenu> &context_menu,
		      const std::shared_ptr<PixCon::IMetaDataUpdateHelper> &update_helper,
		      const std::shared_ptr<PixCon::VisualiserList> &visualiser_list,
		      QApplication &application,
		      QWidget* parent)
    : ScanMainPanel(user_mode,
		    calibration_data,
		    histogram_cache,
		    palette,
		    categories,
		    context_menu,
		    update_helper,
        	    std::shared_ptr<PixDet::ILogoFactory>(new OHLogoFactory),
		    visualiser_list,
		    application,
		    parent) ,
      m_ohHistogramProvider(&oh_histogram_provider),
      m_metaHistogramProvider(meta_histogram_provider),
      m_monitorManager(new PixCon::IsMonitorManager),
      m_receptor(new PixCon::IsReceptor(&application,
					this,
					is_server_name,
					is_partition,
					calibrationData())),
      m_statusChangeTransceiver(status_change_transceiver),
      m_scanStatusMonitor(new ScanStatusMonitor(oh_histogram_provider,
						histogram_cache,
						m_receptor,
						m_monitorManager,
						kScan,
						CAN::ScanIsStatus::varName(),
						"ScanStatus",
						calibration_data,
						CAN::ScanIsStatus::kSuccess,
						m_statusChangeTransceiver)),
      m_analysisStatusMonitor(new AnalysisStatusMonitor(oh_histogram_provider,
							histogram_cache,
							m_receptor,
							m_monitorManager,
							kAnalysis,
							CAN::AnalysisStatus::varName(),
							CAN::AnalysisStatus::globalVarName(),
							"ANAStatus",
							calibration_data,
							CAN::AnalysisStatus::kSuccess,
							m_statusChangeTransceiver)),
      m_statusMonitor( new MonitorThread(20) ),
      m_metaDataBrowser(NULL),
      m_shutdown(false)
  {
    setWindowTitle("OH Viewer");
    if (m_statusChangeTransceiver->hasDefaultForwarder()) {
      m_statusChangeTransceiver->setForwarder(new StatusChangeApplicationNotifier(application,*this));
    }

    /*
    //unsigned int file_index = 0;
    File->addAction("&Open Scan or Analysis",this, SLOT( monitorScan() ), Qt::CTRL+Qt::Key_O);//, kFileOpenScanAnalysis, file_index++);

    File->addSeparator();//file_index++);
    File->addAction("Reconnect &IPC",this, SLOT( reconnectIPC() ), Qt::CTRL+Qt::Key_I);//, kFileReconnectIPC, file_index++);
    */
    QAction *menuAct;

    menuAct = new QAction("&Open Scan or Analysis",this);
    menuAct->setShortcut(Qt::CTRL+Qt::Key_O);
    connect(menuAct, SIGNAL( triggered(bool) ), this, SLOT( monitorScan() ));
    File->insertAction(m_sepExitAction, menuAct);

    menuAct = new QAction("Reconnect &IPC",this);
    menuAct->setShortcut(Qt::CTRL+Qt::Key_I);
    connect(menuAct, SIGNAL( triggered(bool) ), this, SLOT( reconnectIPC() ));
    File->insertAction(m_sepExitAction, menuAct);
    m_sepReconnAction = File->insertSeparator(menuAct);


    connect( this, SIGNAL(newMeasurement(PixCon::EMeasurementType, PixCon::SerialNumber_t ) ), 
	     this, SLOT(updateMetaHistogramProvider(PixCon::EMeasurementType, PixCon::SerialNumber_t) ) );

    PixCon::CategoryList::Category general_cat = categories->addCategory("General");
    PixCon::CategoryList::Category results_cat = categories->addCategory("Results");

    VarDef_t enabled_var_def = CalibrationDataStyle::createEnabledVarDef();
    //    std::cout << "INFO [OHViewer::OHViewer] enabled_var = " << VarDefList::instance()->varName(enabled_var_def.id()) << "(" << enabled_var_def.id() << ")" << std::endl;
    general_cat.addVariable( VarDefList::instance()->varName(enabled_var_def.id()) );

    //    scan_cat.addVariable("STATUS");
    updateCategories(categories);

    m_statusMonitor->addMonitor(m_scanStatusMonitor);
    m_statusMonitor->addMonitor(m_analysisStatusMonitor);
  }


  OHViewer::~OHViewer() {
    if (m_monitorManager) {
      std::cout << "INFO [OHViewer::dtor] Shutdown monitor." << std::endl;
      m_monitorManager->shutdown();
    }
    // m_updateVarListTimer->stop();
    //    delete m_updateVarListTimer;
  }


  void OHViewer::addStatusMonitor(const std::shared_ptr<IStatusMonitor> &status_monitor) {
    if (m_shutdown) return;

    assert( status_monitor.get() );
    m_statusMonitor->addMonitor(status_monitor);
  }


  bool OHViewer::addScan( SerialNumber_t scan_serial_number)
  {
    if (m_shutdown) return false;
    try {

      bool has_scan;
      {
	Lock lock(calibrationData()->calibrationDataMutex());
	has_scan = calibrationData()->scanMetaDataCatalogue().find(scan_serial_number);
      }

      if (!has_scan) {
	{
	//	QMessageBox  *message = new QMessageBox("Loading Scan data.","Loading scan meta data and connectiivty.", QMessageBox::Information, QMessageBox::Ok,0,0,this);
	  statusBar()->showMessage("Loading scan meta data and connectiivty ... "); // must be set before the busy thread is started
	BusyLoop busy(application(), NULL /*message*/ ,UINT_MAX,100 ); // do not show the pop-up dialog

	if (!loadScanMetaData(*(calibrationData()), scan_serial_number)) return false;
	std::vector <PixA::ConfigHandle> vconfig = getScanConfigVector(scan_serial_number);
	calibrationData()->setScanConfigVector(vconfig);

	updateMetaHistogramProvider(kScan, scan_serial_number);
	}
	setSerialNumbers();
	selectSerialNumber( kScan, scan_serial_number);
      }
      return true;

    }
    catch(CAN::PixMetaException &err) {
      std::cerr << "ERROR [OHViewer::addScan]  " << err.what() << std::endl;
    }
    catch(PixA::BaseException &err) {
      std::cerr << "ERROR [OHViewer::addScan] Caught base exception : " << err.getDescriptor() << std::endl;
    }
    catch(std::exception &err) {
      std::cerr << "ERROR [OHViewer::addScan] Caught std exception : " << err.what() << std::endl;
    }
    catch(...) {
      std::cerr << "ERROR [OHViewer::addScan] Caught unknown exception." << std::endl;
    }
    return false;
  }

  bool OHViewer::addAnalysis( SerialNumber_t analysis_serial_number, unsigned int level)
  {
    if (m_shutdown) return false;

    if (level>3) {
      std::cerr << "ERROR [OHViewer::addAnalysis] Analysis dependency tree is too deep." << std::endl;
    }
    try {
      bool has_analysis;
      {
	Lock lock(calibrationData()->calibrationDataMutex());
	has_analysis = calibrationData()->analysisMetaDataCatalogue().find(analysis_serial_number);
      }
      if (!has_analysis) {

	CAN::AnalysisInfo_t analysis_info;
	{
	  //	  QMessageBox  *message = new QMessageBox("Loading analyis meta data.","Loading analysis meta data..", QMessageBox::Information, QMessageBox::Ok,0,0,this);
	  statusBar()->showMessage("Loading analysis meta data ..."); // must be set before the busy thread is started
	  BusyLoop busy(application(), NULL /*message*/ ,UINT_MAX,100 );
	  analysis_info = getAnalysisInfo(analysis_serial_number);
	}

	if (analysis_info.isAnalysisOfAnAnalysis()) {
	  if (!addAnalysis( analysis_info.srcAnalysisSerialNumber(),level+1 )) return false;
	}
	else {
	  if (!addScan( analysis_info.scanSerialNumber() )) {
	    std::cerr << "ERROR [OHViewer::addAnalysis] Failed to add scan  " << analysis_info.scanSerialNumber() << std::endl;
	    return false;
	  }
	}

	setAnalysisInfo(*(calibrationData()), analysis_serial_number, (&analysis_info) );

	//      m_ohHistogramProvider->setUptodate(kAnalysis,analysis_serial_number);

	analysisStatusMonitor().monitorStatus(analysis_serial_number, analysis_info.status() != CAN::kNotSet);

	updateMetaHistogramProvider(kAnalysis, analysis_serial_number);

	if (analysis_info.status() != CAN::kNotSet) {
	  calibrationData()->updateStatus(kAnalysis, analysis_serial_number, ConvertGlobalStatus::convertStatus(analysis_info.status()));
	}

	setSerialNumbers();
	selectSerialNumber( kAnalysis, analysis_serial_number);
	return true;
      }
    }
    catch(PixA::BaseException &err) {
      std::cerr << "ERROR [OHViewer::addAnalysis] Caught base exception : " << err.getDescriptor() << std::endl;
    }
    catch(std::exception &err) {
      std::cerr << "ERROR [OHViewer::addAnalysis] Caught std exception : " << err.what() << std::endl;
    }
    catch(...) {
      std::cerr << "ERROR [OHViewer::addAnalysis] Caught unknown exception." << std::endl;
    }
    return false;
  }

  void OHViewer::removeMeasurement(PixCon::EMeasurementType measurement_type, PixCon::SerialNumber_t serial_number)
  {
    switch (measurement_type) {
    case kScan:
      scanStatusMonitor().abortMonitoring(serial_number);
    case kAnalysis:
      analysisStatusMonitor().abortMonitoring(serial_number);
      break;
    case kCurrent:
      break;
    case kNMeasurements:
      break;
    }
    ScanMainPanel::removeMeasurement(measurement_type, serial_number);
  }

  void OHViewer::monitorScan()
  {
    if (!m_metaDataBrowser){
      m_metaDataBrowser = new SearchDbCoralForm(this);
    }
    m_metaDataBrowser->show();
    m_metaDataBrowser->raise();
  }


//   void OHViewer::monitorScan( )
//   {
//     ScanSerialNumberDialog newSerialNumber(this);
//     if (newSerialNumber.exec() == QDialog::Accepted) {
//       if (newSerialNumber.measurementType()==kScan) {
// 	addScan( newSerialNumber.serialNumber() );
//       }
//       else if (newSerialNumber.measurementType()==kAnalysis) {
// 	addAnalysis( newSerialNumber.serialNumber() );
//       }
//       else {
// 	std::cerr << "ERROR [OHViewer::monitorScan] Can only monitor a scan or an analysis not a type " << newSerialNumber.measurementType() << "." << std::endl;
//       }
//     }
//   }

  bool OHViewer::event(QEvent *an_event)
  {
	//std::cout << "INFO [OHViewer::event] testoutput 1 " << std::endl;
    if (an_event && an_event->type() == QEvent::User && !m_shutdown) {

      PixCon::QConnectivityChangedEvent *new_conn_event  = dynamic_cast< PixCon::QConnectivityChangedEvent *>(an_event);
      if (new_conn_event) {
	changeConnectivity(kCurrent,0);
      }
    }
	//std::cout << "INFO [OHViewer::event] testoutput 2 " << std::endl;
    return ScanMainPanel::event(an_event);
  }

  void OHViewer::shutdown() {
    if (!m_shutdown) {
      m_shutdown=true;
      m_statusMonitor->initiateShutdown();
      m_monitorManager->initiateShutdown();
    }
    //    m_monitorManager->dumpStat();
  }



  void OHViewer::updateMetaHistogramProvider(PixCon::EMeasurementType measurement_type, PixCon::SerialNumber_t serial_number) {
    if(m_shutdown) return;

    try {
    // @todo also an analysis may provide histograms
    MetaData_t::EStatus measurement_status = static_cast<MetaData_t::EStatus>( calibrationData()->status(measurement_type, serial_number) );
    bool use_oh;
    if (measurement_type == kAnalysis) {use_oh=true;}
    else{
      if ( MetaData_t::isTerminated(measurement_status) ) {
	//	std::cout << "INFO [OHViewer::updateMetaHistogramProvider] Scan has already finished. First try to get histogram list from OH." <<std::endl;
	// try to find out whether there are still histograms in OH
	// if at least one histogram list is non empty, assume that all histograms are still in OH.

	EConnItemType conn_item_type[]={kModuleItem,kRodItem};
	use_oh=false;
	for (unsigned int type_i=0; type_i<2; type_i++) {
	  std::shared_ptr< std::vector< std::string > > histogram_list_ptr = histogramCache()->histogramList( measurement_type,
														serial_number,
														conn_item_type[type_i]);
	  if (!histogram_list_ptr->empty()) {
	  std::cout << "INFO [OHViewer::updateMetaHistogramProvider] Got histogramlist from OH. Will use OH." <<std::endl;
	  use_oh=true;
	  break;
	  }
	}

	if (!use_oh) {
	  // otherwise the cache will memorise the empty histogram lists.
	  histogramCache()->clearCache(measurement_type,serial_number);
	}

      }
      else {
	use_oh=true;
      }
    }

    if (use_oh) {
      std::cout << "INFO [OHViewer::updateMetaHistogramProvider] Use OH." <<std::endl;
      m_ohHistogramProvider->setUptodate(measurement_type,serial_number);
      if (measurement_type == kScan) {
	if (scanStatusMonitor().monitorStatus(serial_number)) {
	  if (MetaData_t::isTerminated(measurement_status)) {
	    // notify the monitor that the scan has finished already
	    std::cout << "INFO [OHViewer::updateMetaHistogramProvider] Scan has finished." <<std::endl;
	    scanStatusMonitor().hasFinished(serial_number);
	  }
	}
	else {
	  std::cerr << "WARNING [OHViewer::updateMetaHistogramProvider] Failed to monitor scan." <<std::endl;
	  use_oh=false;
	}
      }

      if (measurement_type == kAnalysis) {
	if (analysisStatusMonitor().monitorStatus(serial_number)) {
	  if (MetaData_t::isTerminated(measurement_status)) {
	    std::cout << "INFO [OHViewer::updateMetaHistogramProvider] Analysis has finished." <<std::endl;
	    analysisStatusMonitor().hasFinished(serial_number);
	  }
	}
	else {
	  std::cerr << "WARNING [OHViewer::updateMetaHistogramProvider] Failed to monitor Analysis." <<std::endl;
	}
      }

    }

    if (measurement_type == kScan ) {
      if ( MetaData_t::isTerminated(measurement_status) ) {
	const ScanMetaData_t &scan_meta_data = calibrationData()->scanMetaData(serial_number);
	if (!scan_meta_data.fileName().empty()) {
	  if (use_oh) {
	    std::cout << "INFO [OHViewer::updateMetaHistogramProvider] Fall-back to  file " << scan_meta_data.fileName() 
		      << " if OH does not provide histograms." <<std::endl;
	  }
	  else {
	    std::cout << "INFO [OHViewer::updateMetaHistogramProvider] Try to read histograms from file : " << scan_meta_data.fileName() << "." <<std::endl;
	  }
	  m_metaHistogramProvider->addScan(measurement_type, serial_number, scan_meta_data.fileName() );
	}
      }

      if (!use_oh) {

	VarDef_t histogram_status_var = HistogramStatus::defineVar();
	application()->postEvent(this, new QNewVarEvent(kScan,
							serial_number, 
							"General",
							VarDefList::instance()->varName(histogram_status_var.id()) ) );
      }
    }
    }
    catch (PixA::BaseException &err) {
      std::cerr << "FATAL [OHViewer::updateMetaHistogramProvider] Caught exception for "
		<< makeSerialNumberString(measurement_type, serial_number)
		<< " Exception : " << err.getDescriptor()
		<< "Presumably, histograms access will not be availble."
		<< std::endl;
    }
    catch (std::exception &err) {
      std::cerr << "FATAL [OHViewer::updateMetaHistogramProvider] Caught exception for "
		<< makeSerialNumberString(measurement_type, serial_number)
		<< " Exception : " << err.what()
		<< "Presumably, histograms access will not be availble."
		<< std::endl;
    }
    catch (...) {
      std::cerr << "FATAL [OHViewer::updateMetaHistogramProvider] Caught unhandled exception. Presumably, histograms will not be available for "
		<< makeSerialNumberString(measurement_type, serial_number) << std::endl;
    }

  }

  void OHViewer::showAnalysisConfiguration(EMeasurementType measurement_type, SerialNumber_t analysis_serial_number, unsigned short class_i) {

    if (measurement_type != kAnalysis || class_i >= AnalysisMetaData_t::kNConfigs) {
      std::cerr << "ERROR [OHViewer::showAnalysisConfiguration]  not an analysis : "
		<< makeSerialNumberString(measurement_type, analysis_serial_number) 
		<< ", or invalid configuration type :  " << class_i << "." << std::endl;
      return;
    }

    // load configuration if not yet loaded
    AnalysisMetaData_t::EConfigTypes config_type = static_cast<AnalysisMetaData_t::EConfigTypes>(class_i);
    AnalysisMetaData_t &analysis_meta_data = calibrationData()->analysisMetaData(analysis_serial_number);
    PixA::ConfigHandle config_handle_1(analysis_meta_data.configHandle(config_type)) ;

    if (!config_handle_1) {
      if (!analysis_meta_data.tagName(config_type).empty()) {

      Lock globalLock(CAN::PixDbGlobalMutex::mutex());
      std::cout << "INFO [OHViewer::addAnalysis] Add configuration " << config_type << " for analysis  " << analysis_serial_number << std::endl;

      std::shared_ptr<CAN::IConfigDb> config_db( SharedObjectConfigDbInstance::instance() );

      std::vector<CAN::EObjectType> translation_table;
      translation_table.resize(AnalysisMetaData_t::kNConfigs);
      translation_table[AnalysisMetaData_t::kDecision]=CAN::kClassification;
      translation_table[AnalysisMetaData_t::kAnalysis]=CAN::kAlg;
      translation_table[AnalysisMetaData_t::kPostProcessing]=CAN::kPostProcessing;

      std::cout << "INFO [OHViewer::addAnalysis] Add configurations for analysis  " << analysis_serial_number << " : " << analysis_meta_data.name(config_type)<< std::endl;
      PixA::ConfigHandle config_handle_2;
      try {
	config_handle_2 = config_db->config(analysis_meta_data.name(config_type),
					    analysis_meta_data.tagName(config_type),
					    analysis_meta_data.revision(config_type));
      }
      catch( ... ) {
      }
      if (!config_handle_2) {
	std::cerr << "ERROR [OHViewer::showAnalysisConfiguration] Failed to retrieve configuration for "
		  << static_cast< unsigned int > ( config_type ) << " : "
		  << " name = " << analysis_meta_data.name(config_type)
		  << " tag = " << analysis_meta_data.tagName(config_type)
		  << " revision = " << analysis_meta_data.revision(config_type)
		  << std::endl;
      }

      analysis_meta_data.setConfig(config_type, config_handle_2);
    }
      else {
	std::cerr << "ERROR [OHViewer::showAnalysisConfiguration]  no analysis configuration type " << config_type << " for "
		  << makeSerialNumberString(measurement_type, analysis_serial_number) 
		  << ", and there are not tags given. Cannot load configuration.." << std::endl;
	return;
      }
    }
    // Now show the already loaded configuration
    ScanMainPanel::showAnalysisConfiguration(measurement_type, analysis_serial_number, class_i);
  }

  void OHViewer::reconnectIPC() {
    std::unique_ptr<PixA::IBusyLoop> busy_loop(PixA::BusyLoopFactory::busyLoop(" Reconnecting IPC ..."));
    monitorManager()->reconnect();
    resetHistogramCache();
  }

  void OHViewer::resetHistogramCache() {
    // clear cache
    histogramCache()->reset();

    // reread meta data ?
    for(std::map<SerialNumber_t, ScanMetaData_t>::const_iterator
	  scan_iter =  calibrationData()->scanMetaDataCatalogue().begin();
	scan_iter != calibrationData()->scanMetaDataCatalogue().end();
	++scan_iter) {
      updateMetaHistogramProvider(kScan, scan_iter->first);
    }
    for(std::map<SerialNumber_t, AnalysisMetaData_t>::const_iterator
	  scan_iter =  calibrationData()->analysisMetaDataCatalogue().begin();
	scan_iter != calibrationData()->analysisMetaDataCatalogue().end();
	++scan_iter) {
      updateMetaHistogramProvider(kAnalysis, scan_iter->first);
    }
  }

}
