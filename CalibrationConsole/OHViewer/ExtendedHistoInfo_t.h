// Dear emacs, this is -*-c++-*-
#ifndef _PixCon_ExtendedHistoInfo_t_h_
#define _PixCon_ExtendedHistoInfo_t_h_

#include <DataContainer/HistoInfo_t.h>
#include <string>

namespace PixCon {

  class ExtendedHistoInfo_t : public HistoInfo_t
  {
  public:
    ExtendedHistoInfo_t()  {reset(); }
    ExtendedHistoInfo_t(HistoInfo_t &a) : HistoInfo_t(a) {}
    ExtendedHistoInfo_t(const std::string &parent_conn_name, HistoInfo_t &a) : HistoInfo_t(a),m_parentConnName(parent_conn_name) {}

    void setParentConnName(const std::string parent_conn_name)  { m_parentConnName = parent_conn_name; }
    const std::string &parentConnName() const { return m_parentConnName; }

  private:
      std::string m_parentConnName;
  };

}
#endif
