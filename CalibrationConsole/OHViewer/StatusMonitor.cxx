#include "StatusMonitor.h"
#include <CalibrationDataManager.h>
#include <DoNothingExtractor.h>
#include <ConfigWrapper/ConnectivityUtil.h>
#include "QNewVarEvent.h"
#include "StatusChangeEvent.h"


#include "IsMonitorReceiver.ixx"
#include "IsMonitorManager.h"

namespace PixCon {

  // instantiate string receiver
  template class IsMonitorReceiver<ISInfoT<std::string>, std::string,const std::string&>;
  template class IsMonitorReceiverBase<ISInfoT<std::string>, std::string>;

  typedef SimpleIsMonitorReceiver<std::string,const std::string&> IsStatusMonitorBase;

  const char StatusMonitor::StatusMonitorData_t::m_measurementTypeId[kNMeasurements]={'\0', 'S', 'A'};

  const std::string StatusMonitor::StatusMonitorData_t::s_statusCategoryName("General");

  //    enum EStatus  {kOk, kFailed, kAborted, kUnknown, kRunning, kNStates, kUnset};
  unsigned short StatusMonitor::s_precedence[kUnset+1]={
    2,      //ok
    4,      //failed
    3,      //aborted
    1,      //unknown
    10,     //running
    2,      //loading ok
    4,      //loading failed
    3,      //loading aborted
    1,      //running 
    0,      //unset
    0       //unset
  };
  //-> unset, unknown, ok, failed, aborted, running

  StatusMonitor::IsStatusMonitor::IsStatusMonitor(StatusMonitorData_t &monitor_data,
						  SerialNumber_t serial_number,
						  EValueConnType conn_type,
						  bool verbose)
    :  IsStatusMonitorBase(monitor_data.receptor(),
			   conn_type,
                           "",
			   makeRegex(monitor_data, serial_number),
			   (conn_type == kRodValue ? 2*(132+15) : (conn_type == kModuleValue ? 2*(1744+224+24) : 1000)) /* buffer size */, 
			   15*60 /* max time between updates*/ ),

       m_serialNumber(serial_number),
       m_statusMonitorData(&monitor_data),
       m_verbose(verbose)
      //,
       //       m_lastStatus(static_cast<State_t>(-1))
  {
    assert( m_statusMonitorData->measurementType() < kNMeasurements );
  }

  StatusMonitor::IsStatusMonitor::~IsStatusMonitor() {
    if (m_verbose) {
      std::cout << "INFO [StatusMonitor::IsStatusMonitor::dtor] serial number "
		<< makeSerialNumberString(m_statusMonitorData->measurementType(),m_serialNumber) << "." << std::endl;
    }
  }

  std::string StatusMonitor::isStatusVarConnNameExtractFunction(const std::string &full_is_name)
  {
    std::string::size_type is_conn_name_end = full_is_name.rfind("/");
    if (is_conn_name_end != std::string::npos && is_conn_name_end>0) {
      std::string::size_type is_conn_name_start = full_is_name.rfind("/",is_conn_name_end-1);
      if (is_conn_name_start != std::string::npos && is_conn_name_start+1< is_conn_name_end) {
      is_conn_name_start++;
      return full_is_name.substr(is_conn_name_start, is_conn_name_end-is_conn_name_start);
      }
    }
//     std::string::size_type is_server_name_end = full_is_name.find(".");
//     if (is_server_name_end != std::string::npos && is_server_name_end+1<full_is_name.size()) {
//       if (full_is_name[is_server_name_end+1]=='/') is_server_name_end+=2;
//       std::string::size_type conn_start = full_is_name.find("/",is_server_name_end);
//       if (conn_start != std::string::npos && conn_start+1 < full_is_name.size()) {
//    conn_start++;
//    std::string::size_type conn_end = full_is_name.find("/",conn_start);
//    if (conn_end != std::string::npos) {
//      return full_is_name.substr(conn_start, conn_end - conn_start);
//    }
//       }
//     }
    return full_is_name.substr(0,0);
  }


  /** Extract connectivitity name from Scan / Analysis status IS variable at construction time.
   * The connectivity name is memorised and passed if @ref extractVar is called.
   */
  class StatusNameExtractor : public IVarNameExtractor
  {
  public:

    StatusNameExtractor(const std::string &conn_name, const std::string &var_name) 
      : m_connName(conn_name),
	m_statusName( &var_name )
    {}

    void extractVar(const std::string &/*full_is_name*/, ConnVar_t &conn_var) const
    {
      conn_var.connName() = m_connName;
      conn_var.varName()= *m_statusName;
    }

    const std::string &connName() { return m_connName; }

  private:
    std::string              m_connName; 
    const std::string       *m_statusName;
  };

  void StatusMonitor::IsStatusMonitor::setValue(const std::string &is_name,  ::time_t time_stamp, const std::string &status_name)
  {
    State_t status = static_cast<State_t>( (*m_statusMonitorData->translateStatusNameFunction() )(status_name) );
    //    if (m_lastStatus != status) {
    StatusNameExtractor extractor( (* m_statusMonitorData->extractConnName() )(is_name),
				   m_statusMonitorData->calibrationDataVarName() );

    if (m_verbose) {
      std::cout << "INFO [StatusMonitor::IsStatusMonitor::setValue] Is name = " << is_name << " = " << status_name << std::endl;
    }

      m_statusMonitorData->receptor()->IsReceptor::setValue(valueConnType(),
							    m_statusMonitorData->measurementType(),
							    m_serialNumber,
							    is_name,
							    extractor,
							    time_stamp,
							    status );

      if (status >= m_statusMonitorData->statusThreshold()) {
	notify(extractor.connName());
      }
      //      m_lastStatus = status;
      //    }
  }

  std::string StatusMonitor::IsStatusMonitor::makeRegex(const StatusMonitorData_t &status_monitor_data, SerialNumber_t serial_number)
  {
    // If the naming convention changes in IS the pattern for the scan and analysis status has to be changed here.
    std::stringstream pattern;
    pattern << ".*" /* << "/" */;
    if (status_monitor_data.measurementType()!= kCurrent) {
      pattern << status_monitor_data.measurementTypeId() << std::setw(9) << std::setfill('0') << serial_number;
    }
    pattern << "/.*/" << status_monitor_data.varName();
    //    std::cout << "INFO [StatusMonitor::IsStatusMonitor::makeRegex] Is pattern = " << pattern.str() << std::endl;
    return pattern.str();
  }


  StatusMonitor::GlobalIsStatusMonitorHelper::GlobalIsStatusMonitorHelper(GlobalIsStatusMonitor *global_is_status_monitor,
									  const std::shared_ptr<IsReceptor> &is_receptor,
									  const std::string &regex)
    : IsMonitorReceiverInterceptor(is_receptor,
				   kRodValue,
				   regex,
				   (132+15)*2  /*buffer size*/,
				   15*60  /*maximum time between updates in sec.*/),
      m_isMonitor(global_is_status_monitor)
  {
  }


  StatusMonitor::GlobalIsStatusMonitor::GlobalIsStatusMonitor(StatusMonitorData_t &monitor_data,
							      SerialNumber_t serial_number,
							      bool verbose)
    : GlobalStatusMonitorBase(false, verbose),
      // values is marked as kRodValue such that the rod values are propagated to the modules.
      /*IsMonitorReceiver<std::string>*/
      m_serialNumber( serial_number ),
      m_statusMonitorData(&monitor_data)
  {
    assert( m_statusMonitorData->measurementType() < kNMeasurements);
    assert( m_statusMonitorData->hasGlobalVar() );
    assert( m_statusMonitorData->monitorManager().get() );
    std::string monitor_regex=makeRegex(monitor_data, serial_number);
    m_isMonitor=std::shared_ptr<GlobalIsStatusMonitorHelper>(new GlobalIsStatusMonitorHelper(this,
											       m_statusMonitorData->receptor(),
											       monitor_regex));
    m_statusMonitorData->monitorManager()->addMonitor(monitor_regex, m_isMonitor);
  }

  StatusMonitor::GlobalIsStatusMonitor::~GlobalIsStatusMonitor()
  {
    if (m_verbose) {
      std::cout << "INFO [StatusMonitor::GlobalIsStatusMonitor::dtor] serial number = " <<
      makeSerialNumberString(m_statusMonitorData->measurementType(),m_serialNumber) << "." << std::endl;
    }
    m_statusMonitorData->monitorManager()->removeMonitor(m_isMonitor);
  }


  void StatusMonitor::GlobalIsStatusMonitor::setValue(const std::string &is_name, ::time_t time_stamp, const std::string &value)
  {
    State_t status = static_cast<State_t>( (*m_statusMonitorData->translateStatusNameFunction() )(value) );

    DoNothingExtractor extractor( m_statusMonitorData->calibrationDataVarName() );

    if (m_verbose) {
      std::cout << "INFO [StatusMonitor::GlobalIsStatusMonitor::setValue] Is name = " << is_name << " -> " << value << std::endl;
    }
    m_statusMonitorData->receptor()->IsReceptor::setValue(m_isMonitor->valueConnType(),
							  m_statusMonitorData->measurementType(),
							  m_serialNumber,
							  "PIXDET" /* connectivity_name */,
							  extractor,
							  time_stamp,
							  status );

    if (status >= m_statusMonitorData->statusThreshold()) {
      m_exceededThreshold=true;
      if (m_verbose) {
	std::cout << "INFO [StatusMonitor::GlobalIsStatusMonitor::setValue] global status  " << is_name << " indidcates termination." << std::endl;
      }
      notify();
    }
  }


  /** Create the regular expressing to select the IS variable.
   */
  std::string StatusMonitor::GlobalIsStatusMonitor::makeRegex(const StatusMonitorData_t &status_monitor_data, SerialNumber_t serial_number) {
    std::stringstream pattern;
    pattern << ".*" /* << "/" */;
    if (status_monitor_data.measurementType() != kCurrent ) {
      pattern << status_monitor_data.measurementTypeId() << std::setw(9) << std::setfill('0') << serial_number << "/";
    }
    else {
      pattern << "\\.";
    }
    pattern << status_monitor_data.globalVarName();
    // std::cout << "INFO [StatusMonitor::GlobalIsStatusMonitor::makeRegex] Is pattern = " << pattern.str() << std::endl;
    return pattern.str();
  }




  //  const std::string StatusNameExtractor::s_statusName("Status");

  StatusMonitor::StatusMonitor(const std::shared_ptr<IsReceptor> &receptor,
			       const std::shared_ptr<IsMonitorManager> &is_monitor_manager,
			       EMeasurementType measurement_type,
			       const std::string &var_name,
			       const std::string &calibration_data_var_name,
			       const std::shared_ptr<CalibrationDataManager> &calibration_data,
			       int status_threshold,
			       TransLateStatusNameFuncPtr_t translate_status_name_func,
			       const std::shared_ptr<StatusChangeTransceiver> &status_change_transceiver)
    : m_processedSequence(0),
      m_data(receptor,
	     is_monitor_manager,
	     measurement_type,
	     var_name,
	     calibration_data_var_name,
	     calibration_data,
	     status_threshold,
	     translate_status_name_func),
      m_statusChangeTransceiver(status_change_transceiver),
      m_verbose(false)
  {
    assert( !m_data.calibrationDataVarName().empty() );
  }

  StatusMonitor::StatusMonitor(const std::shared_ptr<IsReceptor> &receptor,
			       const std::shared_ptr<IsMonitorManager> &is_monitor_manager,
			       EMeasurementType measurement_type,
			       const std::string &var_name,
			       const std::string &global_var_name,
			       const std::string &calibration_data_var_name,
			       const std::shared_ptr<CalibrationDataManager> &calibration_data,
			       int status_threshold,
			       TransLateStatusNameFuncPtr_t translate_status_name_func,
			       const std::shared_ptr<StatusChangeTransceiver> &status_change_transceiver)
    : m_processedSequence(0),
      m_data(receptor,
	     is_monitor_manager,
	     measurement_type,
	     var_name,
	     global_var_name,
	     calibration_data_var_name,
	     calibration_data,
	     status_threshold,
	     translate_status_name_func),
      m_statusChangeTransceiver(status_change_transceiver),
      m_verbose(false)
  {
    assert( !m_data.calibrationDataVarName().empty() );

    Lock lock(calibrationData()->calibrationDataMutex());
    calibrationData()->calibrationData().createConnObjDataList("PIXDET");
  }

  StatusMonitor::~StatusMonitor()
  {
    Lock lock(m_statusMonitorMutex);
    m_statusMonitorList.clear();
  }


  bool StatusMonitor::monitorStatus(SerialNumber_t serial_number,
				    bool global_status_exceeds_threshold_already)
  {
    if (m_verbose) {
      std::cout << "INFO [StatusMonitor::monitorStatus] for S" <<  serial_number <<"." << std::endl;
    }

    std::string monitor_name("ScanStatus_");
    monitor_name += makeSerialNumberString(kScan, serial_number);

    if (global_status_exceeds_threshold_already) {
      if (m_verbose) {
	std::cout << "INFO [StatusMonitor::monitorStatus] Global status exceed already the threshold." << std::endl;
      }
      // hack to immediately start the processing
      std::pair< std::map<SerialNumber_t, std::shared_ptr<RodMonitor_t> >::iterator, bool>
	ret = m_statusMonitorList.insert(std::make_pair(serial_number,
							std::shared_ptr<RodMonitor_t>(new RodMonitor_t(m_data.monitorManager(),
													 monitor_name,
													 NULL,
													 new GlobalStatusMonitorBase(global_status_exceeds_threshold_already,
																     m_verbose)))));
      if (ret.second) {
	m_data.statusChanged();
      }
      return ret.second;
    }

    assert( !m_data.calibrationDataVarName().empty() );
    VarDef_t var_def = VarDefList::instance()->getVarDef( m_data.calibrationDataVarName() );

    //    if (!m_data.globalVarName().empty()) {
    //      var_def.setAllConnType();
    //}
    //  else

    if (!m_data.varName().empty() && var_def.connType()!=kRodValue) {
      var_def.setAllConnType();
    }

    PixA::ConnectivityRef conn = m_data.calibrationData()->connectivity(m_data.measurementType(), serial_number);
    if (conn) {

      Lock lock_statusmonitor(m_statusMonitorMutex);
      std::string message;
      for (std::map<SerialNumber_t, std::shared_ptr<RodMonitor_t> >::iterator serial_number_iter = m_statusMonitorList.begin();
	   serial_number_iter != m_statusMonitorList.begin();
	   ++serial_number_iter) {
	if (!message.empty()) {
	  message+=", ";
	}
	message += makeSerialNumberString(m_data.measurementType(),serial_number_iter->first);
      }
      if (!message.empty()) {
	std::cout << "INFO [StatusMonitor::monitor] Currently active monitors : " << message << std::endl;

      }
      std::map<SerialNumber_t, std::shared_ptr<RodMonitor_t> >::iterator serial_number_iter = m_statusMonitorList.find(serial_number);
      if (serial_number_iter == m_statusMonitorList.end()) {
	if (m_verbose) {
	  std::cout << "INFO [StatusMonitor::monitorStatus] Will create IS monitor for S" <<  serial_number <<"." << std::endl;
	}

	std::unique_ptr<IsStatusMonitor> per_rod_is_monitor;
	std::unique_ptr<GlobalIsStatusMonitor> global_is_monitor;
	if (m_data.hasPerRodVar() ) {
	  per_rod_is_monitor = std::make_unique<IsStatusMonitor>(m_data,
										  serial_number,
										  kRodValue,
										  m_verbose);
	}
	if (m_data.hasGlobalVar() ) {
	  global_is_monitor = std::make_unique<GlobalIsStatusMonitor>(m_data,
											     serial_number,
											     m_verbose);
	  Lock lock_calibrationdata(calibrationData()->calibrationDataMutex()); 
	  calibrationData()->calibrationData().setEnableState("PIXDET",m_data.measurementType(),serial_number, true,true,true,true);

	}

	std::pair< std::map<SerialNumber_t, std::shared_ptr<RodMonitor_t> >::iterator, bool>
	  ret = m_statusMonitorList.insert(std::make_pair(serial_number,
							  std::shared_ptr<RodMonitor_t>(new RodMonitor_t(m_data.monitorManager(),
													   monitor_name,
													   per_rod_is_monitor.release(),
													   global_is_monitor.release()))));

	if (!ret.second) {
	  std::cout << "ERROR [StatusMonitor::monitorScan] Failed to add status block for " << serial_number << " to the list."
		    << std::endl;
	  return false;
	}
	else {
	  serial_number_iter = ret.first;
	}
      }
      else {
	//	if (m_verbose) {
	  std::cerr << "WARNING [StatusMonitor::monitorStatus] Already have monitor for S" <<  serial_number <<"." << std::endl;
	  //	}
      }

      {
	Lock lock(calibrationData()->calibrationDataMutex()); 
      m_data.calibrationData()->calibrationData().varList(m_data.measurementType(),  serial_number, serial_number_iter->second->varList() );
      }
      if (m_verbose) {
      std::cout << "INFO [StatusMonitor::monitorScan] variables for  " << makeSerialNumberString(m_data.measurementType(),serial_number) << " : ";
      for (std::set<std::string>::const_iterator var_iter = serial_number_iter->second->varList().begin();
	   var_iter != serial_number_iter->second->varList().end();
	   var_iter++) {
	std::cout << *var_iter << " ";
      }
      std::cout << std::endl;

      std::cout << "INFO [StatusMonitor::monitorScan] Add variable " << m_data.calibrationDataVarName() << " for  "
		<< makeSerialNumberString(m_data.measurementType(),serial_number)
		<< "." << std::endl;
      }
      //      if (serial_number_iter->second.addVariable( m_data.calibrationDataVarName() ) ) {
      //      std::cout << "INFO [StatusMonitor::monitorScan] yes  " << std::endl;
      announceNewVaiable(serial_number_iter->first, m_data.statusCategoryName(), m_data.calibrationDataVarName() );
      //      }
      Lock calib_data_lock(calibrationData()->calibrationDataMutex());

      for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
	   crate_iter != conn.crates().end();
	   ++crate_iter) {

	PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
	PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
	for (PixA::RodList::const_iterator rod_iter = rod_begin;
	     rod_iter != rod_end;
	     ++rod_iter) {

	  // ignore disabled RODs
	  try {
	    const CalibrationData::ConnObjDataList conn_obj_data ( calibrationData()->calibrationData().getConnObjectData( PixA::connectivityName(rod_iter)) );
	    if ( !conn_obj_data.enableState(measurementType(), serial_number).enabled() ) {
	      continue;
	    }
	  }
	  catch(CalibrationData::MissingConnObject &) {
	    // @todo Should not happen. But could it happen ? What to do if it happens ? 
	    std::cerr << "ERROR [StatusMonitor::monitorStatus] No calibration data for ROD " << PixA::connectivityName(rod_iter) 
		      << " in " << makeSerialNumberString(measurementType(),serial_number) << "."
		      << std::endl;
	  }

	  if (m_verbose) {
	  std::cout << "INFO [StatusMonitor::monitorStatus] Monitor  status for ROD " << PixA::connectivityName(rod_iter) 
		    << " in " << makeSerialNumberString(measurementType(),serial_number) << "."
		    << std::endl;
	  }
	  serial_number_iter->second->insert(PixA::connectivityName(rod_iter) );
	}
      }
      if (m_verbose) {
	std::cout << "INFO [StatusMonitor::monitorStatus] should have activated monitoring for " << makeSerialNumberString(measurementType(),serial_number) << "."
		  << std::endl;
      }
      return true;
    }
    else {
      std::cerr << "ERROR [StatusMonitor::monitorStatus] Did not get a connectivity for " << makeSerialNumberString(measurementType(),serial_number) << "."
		<< std::endl;
      return false;
    }
  }

  void StatusMonitor::abortMonitoring(SerialNumber_t serial_number)
  {
    Lock lock(m_statusMonitorMutex);
    std::map<SerialNumber_t, std::shared_ptr<RodMonitor_t> >::iterator serial_number_iter = m_statusMonitorList.find(serial_number);
    if (serial_number_iter != m_statusMonitorList.end()) {
      if (m_verbose) {
      std::cout << "INFO [HistogramAnalyser::abortMonitoring] Stop monitoring of " << makeSerialNumberString(m_data.measurementType(),serial_number) << "."
		<< std::endl;
      }
      serial_number_iter->second->setAbort();
    }
  }

  void StatusMonitor::process()
  {
    // it is assumed that the maximum difference between m_processSequence and m_sequence is much small than MAX_UINT. 
    int delta = static_cast<int>(m_data.sequence() - m_processedSequence);
    if (delta > 0) {
      m_processedSequence = m_data.sequence();

      if (m_verbose) {
	std::cout << "INFO [StatusMonitor::process] status changed." << std::endl;
      }

      std::map<SerialNumber_t, std::shared_ptr<RodMonitor_t> >::iterator serial_number_iter;
      bool reached_serial_number_list_end;
      {
	Lock lock( m_statusMonitorMutex );
	if (m_statusMonitorList.empty()) {
	  return;
	}
	serial_number_iter = m_statusMonitorList.begin();
	reached_serial_number_list_end  = (serial_number_iter == m_statusMonitorList.end());
      }

	while (!reached_serial_number_list_end) {
	  std::map<SerialNumber_t, std::shared_ptr<RodMonitor_t> >::iterator current_serial_number;

	  {
	    Lock lock( m_statusMonitorMutex );
	    current_serial_number = serial_number_iter++;
	    reached_serial_number_list_end = (serial_number_iter == m_statusMonitorList.end());
	  }

	  bool error=true;
	  try {
	  if (current_serial_number->second->globalStatusExceededThreshold()) {
	    if (m_verbose) {
	    std::cout << "INFO [StatusMonitor::process] global status exceed threshold." << std::endl;
	    }
	    PixA::ConnectivityRef conn = m_data.calibrationData()->connectivity(m_data.measurementType(), current_serial_number->first);
	    if (conn) {

	      for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
		   crate_iter != conn.crates().end();
		   ++crate_iter) {

		PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
		PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
		for (PixA::RodList::const_iterator rod_iter = rod_begin;
		     rod_iter != rod_end;
		     ++rod_iter) {
		  EStatus status = process(current_serial_number->first, PixA::connectivityName(rod_iter), current_serial_number->second->varList() );
		  current_serial_number->second->updateStatus(status);
		}
	      }

	      finish( current_serial_number->first, current_serial_number->second->status() );
	      //	      announceStatusChange( current_serial_number->first );
	      m_statusChangeTransceiver->signal(measurementType(), current_serial_number->first);

	      {
		Lock lock( m_statusMonitorMutex );
		m_statusMonitorList.erase(current_serial_number);
	      }
	    }
	    // @todo what to do if there is no connectivity ?
	  }
	  else {

	    {
	      std::list< std::string >::iterator rod_iter;
	      bool reached_status_monitor_end;
	      {
		Lock lock( m_statusMonitorMutex );
		rod_iter = current_serial_number->second->statusMonitor().begin();
		reached_status_monitor_end = (rod_iter == current_serial_number->second->statusMonitor().end());
	      }

	      while (!reached_status_monitor_end)  {

		std::list< std::string >::iterator current_rod_iter;
		{
		  Lock lock( m_statusMonitorMutex );
		  current_rod_iter = rod_iter++;
		  reached_status_monitor_end = (rod_iter == current_serial_number->second->statusMonitor().end());
		}

		std::string rod_name;
		{
		  Lock rod_lock(current_serial_number->second->statusMonitor().mutex());
		  rod_name = *current_rod_iter;
		  current_serial_number->second->statusMonitor().erase(current_rod_iter);
		}

		current_serial_number->second->erase( rod_name );
		EStatus status = process(current_serial_number->first, rod_name, current_serial_number->second->varList() );
		current_serial_number->second->updateStatus(status);
	      }

	    }

	    if (current_serial_number->second->abort() || (current_serial_number->second->empty() && !m_data.hasGlobalVar())) {

	      if (current_serial_number->second->abort() &&  current_serial_number->second->status()> kAborted) {
		current_serial_number->second->updateStatus(kAborted);
	      }

	      finish(current_serial_number->first, current_serial_number->second->status());

	      //	    announceStatusChange( current_serial_number->first );

	      // 	      std::cout << "INFO [StatusMonitor::process] broadcast termination of "
	      // 			<< makeSerialNumberString(measurementType(), current_serial_number->first)
	      // 			<< "." << std::endl;

	      m_statusChangeTransceiver->signal(measurementType(), current_serial_number->first );
	      {
		Lock lock( m_statusMonitorMutex );
		m_statusMonitorList.erase(current_serial_number);
	      }
	    }
	  }

	  error=false;
	  }
	  catch (PixA::BaseException &err) {
	    std::cerr << "FATAL [StatusMonitor::process] Caught exception while processing "
		      << makeSerialNumberString(measurementType(), current_serial_number->first)
		      << " Exception : " << err.getDescriptor()
		      << " Will remove monitor."
		      << std::endl;
	  }
	  catch (std::exception &err) {
	    std::cerr << "FATAL [StatusMonitor::process] Caught exception while processing "
		      << makeSerialNumberString(measurementType(), current_serial_number->first)
		      << " Exception : " << err.what()
		      << " Will remove monitor."
		      << std::endl;
	  }
	  catch (...) {
	    std::cerr << "FATAL [StatusMonitor::process] Caught unhandled exception. Will remove monitor for "
		      << makeSerialNumberString(measurementType(), current_serial_number->first) << std::endl;
	  }
	  if (error) {

	    try {
	      m_statusChangeTransceiver->signal(measurementType(), current_serial_number->first );
	    }
	    catch(...) {
	    }

	    {
	      Lock lock( m_statusMonitorMutex );
	      m_statusMonitorList.erase(current_serial_number);
	    }
	  }
	}

    }
  }

  void StatusMonitor::announceNewVaiable(SerialNumber_t serial_number, const std::string &category, const std::string &var_name) {
    m_data.receptor()->notify( new QNewVarEvent(m_data.measurementType(), serial_number, category, var_name) );
  }

  void StatusMonitor::hasFinished(SerialNumber_t serial_number)
  {
    Lock lock( m_statusMonitorMutex );
    std::map<SerialNumber_t, std::shared_ptr<RodMonitor_t> >::iterator serial_number_iter = m_statusMonitorList.find(serial_number);
    if (serial_number_iter != m_statusMonitorList.end()) {
      serial_number_iter->second->hasFinished();
      m_data.statusChanged();
      //      statusChangeFlag().setFlag();
    }
  }

  //  void StatusMonitor::announceStatusChange(SerialNumber_t serial_number) {
  //    m_data.receptor()->notify(new StatusChangeEvent(m_data.measurementType(), serial_number) );
  //  }

  StatusMonitor::RodMonitor_t::RodMonitor_t(const std::shared_ptr<IsMonitorManager> &is_monitor_manager,
					    const std::string &monitor_name,
					    IsStatusMonitor *per_rod_monitor,
					    GlobalStatusMonitorBase *global_monitor)
    : m_isMonitorManager(is_monitor_manager),
      m_isStatusMonitor(std::shared_ptr<IsStatusMonitor>(per_rod_monitor)),
      m_globalIsStatusMonitor(std::shared_ptr<GlobalStatusMonitorBase>(global_monitor)),
      m_status(kUnset),
      m_globalStatusExceededThresholdCache(false),
      m_abort(false)
  { 
    assert(per_rod_monitor!=NULL || global_monitor !=NULL); 
    assert(is_monitor_manager.get());
    if (m_isStatusMonitor.get()) {
      m_isMonitorManager->addMonitor(monitor_name, std::shared_ptr<IsStatusMonitorBase>(m_isStatusMonitor));
    }
  }

  StatusMonitor::RodMonitor_t::~RodMonitor_t() { 
    m_isMonitorManager->removeMonitor(std::shared_ptr<IsStatusMonitorBase>(m_isStatusMonitor));
    m_isStatusMonitor.reset(); 
    m_globalIsStatusMonitor.reset(); 
  }

}
