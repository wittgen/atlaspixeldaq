#ifndef _PixCon_StatusChangeEvent_h_
#define _PixCon_StatusChangeEvent_h_

#include "StatusChangeTransceiver.h"
#include <qevent.h>

namespace PixCon {

  class StatusChangeEvent : public QEvent
  {
  public:
    StatusChangeEvent(EMeasurementType measurement_type, SerialNumber_t serial_number)
      : QEvent(QEvent::User),
	m_measurementType(measurement_type),
	m_serialNumber(serial_number)
    {}

    EMeasurementType   measurement() const { return m_measurementType; }
    SerialNumber_t serialNumber() const { return m_serialNumber; }

  private:
    EMeasurementType   m_measurementType;
    SerialNumber_t m_serialNumber;
  };

//   class StatusChangeEventGenerator : public IStatusChangeForwarder
//   {
//   public:
//     StatusChangeEventGenerator(QApplication &app,
// 			       QObject &m_receiver)
//       : m_app(app),
// 	m_receiver(receiver)
//     {}

//     void statusChanged(EMeasurement measurement_type, SerialNumber_t serial_number) {
//       m_app->postEvent(new StatusChangeEvent(measurement_type, serial_number));
//     }

//   private:
//     QApplication *m_app;
//     QObject       m_object;
//   };

}

#endif
