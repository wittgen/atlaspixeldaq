#ifndef _PixCon_OHViewer_h_
#define _PixCon_OHViewer_h_

#include "ScanMainPanel.h"
#include <CalibrationDataTypes.h>
#include "UserMode.h"
#include <memory>
#include "QNewVarEvent.h"
//Added by qt3to4:
#include <QEvent>
#include "StatusMonitor.h"
#include <memory>
//#include "searchdbcoralformbase.h"
//#include "SearchDbCoralForm.h"

class IPCPartition;

class ISInfoReceiver;
class SearchDbCoralForm;

namespace PixCon {

  class OHHistogramProviderBase;
  class MetaHistogramProvider;
  class IsReceptor;
  class IStatusMonitor;
  class MonitorThread;
  class StatusMonitor;
  class StatusChangeTransceiver;

  class OHViewer : public ScanMainPanel
  {
    Q_OBJECT

  public:
  OHViewer( std::shared_ptr<PixCon::UserMode>& user_mode,
	    const std::string &is_server_name,
	    IPCPartition &is_partition,
	    const std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data,
	    const std::shared_ptr<StatusChangeTransceiver> &status_change_transceiver,
	    PixCon::OHHistogramProviderBase &oh_histogram_provider,
	    std::shared_ptr<PixCon::MetaHistogramProvider> meta_histogram_provider,
	    const std::shared_ptr<PixCon::HistogramCache> &histogram_cache,
	    const std::shared_ptr<PixCon::CalibrationDataPalette> &palette,
	    const std::shared_ptr<PixCon::CategoryList> &categories,
	    const std::shared_ptr<PixCon::IContextMenu> &context_menu,
	    const std::shared_ptr<PixCon::IMetaDataUpdateHelper> &update_helper,
	    const std::shared_ptr<PixCon::VisualiserList> &visualiser_list,
	    QApplication &application,
	    QWidget* parent = 0);

    ~OHViewer();

    bool addScan( SerialNumber_t scan_serial_number);
    bool addAnalysis( SerialNumber_t analysis_serial_number) {
      return addAnalysis( analysis_serial_number, 0);
    }

    void removeMeasurement(PixCon::EMeasurementType measurement_type, PixCon::SerialNumber_t serial_number);

//     void setStatusMonitor( StatusMonitor *status_monitor) {
//       if (status_monitor) {
// 	m_statusMonitor=std::unique_ptr<StatusMonitor>(status_monitor);
//       }
//     }

    void addMeasurement() { monitorScan(); }

    void showAnalysisConfiguration(EMeasurementType type,SerialNumber_t serial_number, unsigned short class_i);

  public slots:
    void monitorScan( );

    void updateMetaHistogramProvider(PixCon::EMeasurementType measurement_type, PixCon::SerialNumber_t serial_number);

  protected slots:
    void reconnectIPC();

    void resetHistogramCache();

  protected:
    bool addAnalysis( SerialNumber_t analysis_serial_number, unsigned int level);

    void shutdown();

    bool event(QEvent *an_event);


    StatusMonitor &scanStatusMonitor() {
      assert( dynamic_cast<StatusMonitor *>(m_scanStatusMonitor.get()) );
      return *( static_cast<StatusMonitor *>(m_scanStatusMonitor.get()) );
    }

    StatusMonitor &analysisStatusMonitor() {
      assert( dynamic_cast<StatusMonitor *>(m_analysisStatusMonitor.get()) );
      return *( static_cast<StatusMonitor *>(m_analysisStatusMonitor.get()));
    }

    std::shared_ptr<IStatusMonitor> scanStatusMonitorPtr() { return m_scanStatusMonitor; }

    std::shared_ptr<IStatusMonitor> analysisStatusMonitorPtr() { return m_analysisStatusMonitor; }

    std::shared_ptr<PixCon::IsReceptor> receptor() {return m_receptor; }

    void addStatusMonitor(const std::shared_ptr<IStatusMonitor> &status_monitor);


    std::shared_ptr<StatusChangeTransceiver> statusChangeTransceiver() { return m_statusChangeTransceiver; }

    std::shared_ptr<PixCon::IsMonitorManager> monitorManager() { return m_monitorManager; }


    PixCon::OHHistogramProviderBase    *m_ohHistogramProvider;
    std::shared_ptr<PixCon::MetaHistogramProvider>      m_metaHistogramProvider;

    std::shared_ptr<PixCon::IsMonitorManager> m_monitorManager;
    std::shared_ptr<PixCon::IsReceptor>       m_receptor;

    std::shared_ptr<StatusChangeTransceiver> m_statusChangeTransceiver;
    std::shared_ptr<IStatusMonitor>     m_scanStatusMonitor;
    std::shared_ptr<IStatusMonitor>     m_analysisStatusMonitor;
    std::unique_ptr<MonitorThread>          m_statusMonitor;

    //SearchDbCoralFormBase              *m_metaDataBrowser;
    SearchDbCoralForm                    *m_metaDataBrowser;
    bool                                  m_shutdown;
    QAction *m_sepReconnAction;
  };

}
#endif
