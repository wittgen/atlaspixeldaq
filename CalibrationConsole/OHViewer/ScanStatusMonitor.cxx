#include "ScanStatusMonitor.h"
#include "OHHistogramProviderBase.h"
#include <HistogramCache.h>
#include <HistogramStatus.h>
#include <ConfigWrapper/Connectivity.h>
#include <ConfigWrapper/ConnectivityUtil.h>
#include <CalibrationDataManager.h>
#include <ScanIsStatus.hh>
#include <DoNothingExtractor.h>

#include <DefaultColourCode.h>
#include <IsReceptor.h>
#include <IsMonitorManager.h>

namespace PixCon {

  int ScanStatusMonitor::translateStatusName(const std::string &status_name) {
    return static_cast<int>( CAN::ScanIsStatus::status(status_name) );
  }

  const std::string ScanStatusMonitor::s_histogramCategoryName("General");

  std::vector<StatusMonitor::EStatus> ScanStatusMonitor::s_convertStatus;

  ScanStatusMonitor::ScanStatusMonitor(OHHistogramProviderBase &histogram_provider,
				       const std::shared_ptr<PixCon::HistogramCache> &histogram_cache,
				       const std::shared_ptr<IsReceptor> &receptor,
				       const std::shared_ptr<IsMonitorManager> &is_monitor_manager,
				       EMeasurementType measurement_type,
				       const std::string &var_name,
				       const std::string &calibration_data_var_name,
				       const std::shared_ptr<CalibrationDataManager> &calibration_data,
				       int status_threshold,
				       const std::shared_ptr<StatusChangeTransceiver> &status_change_transceiver)
    : StatusMonitor(receptor,
		    is_monitor_manager,
		    measurement_type,
		    var_name,
		    calibration_data_var_name,
		    calibration_data,
		    status_threshold,
		    &translateStatusName,
		    status_change_transceiver),
      m_histoProvider(&histogram_provider),
      m_histogramCache(histogram_cache),
      m_varId(VarDefBase_t::invalid().id()),
      m_statusVarId(VarDefBase_t::invalid().id()),
      m_verbose(false)
  {
    m_varId = HistogramStatus::defineVar().id();

    // @todo read variable definition from db
    const std::string status_var_name(calibration_data_var_name);
    try {
      const VarDef_t var_def( VarDefList::instance()->getVarDef(status_var_name) );
      if ( !var_def.isStateWithOrWithoutHistory() ) {
	std::cout << "ERROR [ScanStatusMonitor::ctor] variable " << status_var_name << " already exists but it is not a state variable." << std::endl;
      }
      else {
	m_statusVarId=var_def.id();
      }
    }
    catch (UndefinedCalibrationVariable &) {
      // @todo ensure that kAborted is the last state
      VarDef_t var_def( VarDefList::instance()->createStateVarDef(status_var_name, kRodValue, CAN::ScanIsStatus::kAborted+2) );
      if ( var_def.isStateWithOrWithoutHistory() ) {
	StateVarDef_t &state_var_def = var_def.stateDef();

	DefaultColourCode::EDefaultColourCode colour_map[CAN::ScanIsStatus::kAborted+2]={
	  DefaultColourCode::kUnknown,
	  DefaultColourCode::kOff,
	  DefaultColourCode::kConfigured,
	  DefaultColourCode::kRunning,
	  DefaultColourCode::kSuccess,
	  DefaultColourCode::kFailure,
	  DefaultColourCode::kAlarm,
	  DefaultColourCode::kOn
	};

	unsigned short state_i=0;
	for (; state_i<=CAN::ScanIsStatus::kAborted; state_i++) {
	  state_var_def.addState(state_i, colour_map[state_i],
				 CAN::ScanIsStatus::name( static_cast<CAN::ScanIsStatus::EStatus>(state_i) ));
	}
	state_var_def.addState(state_i, colour_map[CAN::ScanIsStatus::kAborted+1], "Stuck" );
	m_statusVarId=var_def.id();
      }
      else {
	std::cout << "ERROR [ScanStatusMonitor::ctor] variable " << status_var_name
		  << " already exists but it is not a state variable." << std::endl;
      }
    }

    if (s_convertStatus.empty()) {
      // CAN::ScanIsStatus enum EStatus {kUnknown, kNone, kInitialising, kRunning, kSuccess, kFailed, kAborted };
      s_convertStatus.resize(CAN::ScanIsStatus::kAborted+1);
      s_convertStatus[CAN::ScanIsStatus::kSuccess]=StatusMonitor::kOk;
      s_convertStatus[CAN::ScanIsStatus::kFailed]=StatusMonitor::kFailed;
      s_convertStatus[CAN::ScanIsStatus::kAborted]=StatusMonitor::kAborted;
      s_convertStatus[CAN::ScanIsStatus::kUnknown]=StatusMonitor::kUnknown;
      s_convertStatus[CAN::ScanIsStatus::kNone]=StatusMonitor::kUnknown;
      s_convertStatus[CAN::ScanIsStatus::kInitialising]=StatusMonitor::kRunning;
      s_convertStatus[CAN::ScanIsStatus::kRunning]=StatusMonitor::kRunning;
    }

  }


  ScanStatusMonitor::~ScanStatusMonitor() { }

  StatusMonitor::EStatus ScanStatusMonitor::process(SerialNumber_t serial_number, const std::string &rod_name, std::set<std::string> &var_list)
  {
    StatusMonitor::EStatus rod_status = process(serial_number, rod_name);

    std::string var_name_histograms =  VarDefList::instance()->varName(m_varId);
    std::pair< std::set<std::string>::iterator, bool> ret = var_list.insert(var_name_histograms);
    if (ret.second) {
      announceNewVaiable( serial_number, s_histogramCategoryName, var_name_histograms );
    }
    return rod_status;

  }

  StatusMonitor::EStatus ScanStatusMonitor::process(SerialNumber_t serial_number, const std::string &rod_name)
  {
    EStatus rod_status = StatusMonitor::kUnset;
    if (m_verbose) {
      std::cout << "INFO [ScanStautsMonitor::process] serial number = " << serial_number << " ROD=" << rod_name << std::endl;
    }

    std::string var_name_histograms =  VarDefList::instance()->varName(m_varId);
    DoNothingExtractor extractor( var_name_histograms );

    {
    std::shared_ptr< std::vector<std::string> > histogram_names_ptr;
    // get per ROD histograms, count histograms, and set histogram status in calibration data container
    {
      m_histoProvider->needToUpdate(measurementType(), serial_number, rod_name);

      if (m_verbose) {
	std::cout << "INFO [ScanStautsMonitaor::process]  request histogram list from provider for "
		  <<  makeSerialNumberString(measurementType(),serial_number) << "." << std::endl;
      }
      //      m_histoProvider->requestHistogramList(measurementType(), serial_number, kRodItem, histogram_names, rod_name);
      histogram_names_ptr = m_histogramCache->updateHistogramList(measurementType(), serial_number, kRodItem, rod_name);
      const std::vector<std::string> &histogram_names( *histogram_names_ptr );
      unsigned int n_histos=0;
      for (std::vector<std::string>::const_iterator histo_iter = histogram_names.begin();
	   histo_iter != histogram_names.end();
	   ++histo_iter) {

	HistoInfo_t histo_info;
	unsigned int histo_id = m_histogramCache->histogramIndex(measurementType(), serial_number, kRodItem, *histo_iter );

	m_histogramCache->numberOfHistograms(measurementType(), serial_number, kRodItem,  rod_name, histo_id, histo_info, true /* refresh */ );
	if (histo_info.minHistoIndex()<histo_info.maxHistoIndex()) {
	  n_histos++;
	}
      }

      // also need to clear the cache
      //       m_histogramCache->clearCache(measurementType(), serial_number);
      //       if (m_verbose) {
      // 	std::cout << "INFO [ScanStautsMonitaor::process]  Cleared cache for " <<  makeSerialNumberString(measurementType(),serial_number) << "." << std::endl;
      //       }

      State_t histogram_number_state = HistogramStatus::state(histogram_names.size(), n_histos);

//       Lock lock(calibrationData()->calibrationDataMutex());
//       calibrationData()->calibrationData().addValue(kRodValue,
// 						    rod_name,
// 						    measurementType(),
// 						    serial_number,
// 						    VarDefList::instance()->varName(m_varId),
// 						    histogram_number_state);

//       QIsInfoEvent *event=new QIsInfoEvent(rod_name, VarDefList::instance()->varName(m_varId), kRodItem, measurementType(), serial_number);

      bool update = true;
      bool rod_enabled=false;
      try {
	const CalibrationData::ConnObjDataList conn_obj_data ( calibrationData()->calibrationData().getConnObjectData( rod_name ) );
	rod_enabled = conn_obj_data.enableState(measurementType(), serial_number).enabled();

	if (rod_enabled) {
	  if (histogram_number_state != HistogramStatus::kOk && histogram_number_state  != HistogramStatus::kNoneExpected) {
	    //	    rod_status = StatusMonitor::kFailed;
	    rod_status = combineStatus(rod_status,StatusMonitor::kFailed);
	  }
	  else {
	    rod_status = combineStatus(rod_status,StatusMonitor::kOk);
	  }

	  try {
	    State_t scan_state = conn_obj_data.value<State_t>(measurementType(), serial_number, m_statusVarId) ;
	    if ( scan_state ) {
	      if (scan_state < s_convertStatus.size()) {
		rod_status = combineStatus(rod_status, s_convertStatus[scan_state]);
	      }
	      else {
		rod_status = combineStatus(rod_status, StatusMonitor::kUnknown);
	      }
	    }
	  }
	  catch(CalibrationData::MissingValue &) {
	  }
	  if (histogram_number_state == HistogramStatus::kPresumablyDisabled) {
	    histogram_number_state = HistogramStatus::kMissingHistograms;
	  }
	}
	try {
	  State_t old_histogram_number_state = conn_obj_data.value<State_t>(measurementType(), serial_number, m_varId) ;
	  if ( old_histogram_number_state == histogram_number_state) {
	    update=false;
	  }
	}
	catch(CalibrationData::MissingValue &) {
	  if (histogram_number_state==HistogramStatus::kPresumablyDisabled && !rod_enabled) {
	    update=false;
	  }

	}

      }
      catch(CalibrationData::MissingConnObject &) {
	// should not happen
	std::cout << "ERROR [ScanStautsMonitor::process] connectivity object " <<  rod_name 
		  << " does not exist for " << makeSerialNumberString(measurementType(),serial_number) << "." << std::endl;
	//	if (rod_status == StatusMonitor::kOk ) {
	rod_status = combineStatus(rod_status,StatusMonitor::kUnknown);
	if (histogram_number_state==HistogramStatus::kPresumablyDisabled) {
	  update=false;
	}
	//	}
      }

      if (update) {
      receptor()->setValue(kRodValue, measurementType(), serial_number, rod_name, extractor, histogram_number_state);
      }
    }
    }

    {
    std::shared_ptr< std::vector<std::string> > histogram_names_ptr;
    // get module histogram list.
    //    m_histoProvider->requestHistogramList(measurementType(), serial_number, kModuleItem, histogram_names);
    histogram_names_ptr = m_histogramCache->updateHistogramList(measurementType(), serial_number, kModuleItem, rod_name);

    // count histograms and set status per module.
    PixA::ConnectivityRef conn = calibrationData()->scanConnectivity(serial_number);

    //bool enabled=false;
    {
    Lock lock(calibrationData()->calibrationDataMutex());
    // disabled rods should not be considered.
    // but ignore them anyway.
    try {
      const CalibrationData::ConnObjDataList conn_obj_data ( calibrationData()->calibrationData().getConnObjectData(rod_name) );
      //enabled =
        (void)conn_obj_data.enableState(measurementType(), serial_number).enabled();
    }
    catch(CalibrationData::MissingConnObject &) {
      // should not happen
    }
    }

    const std::vector<std::string> &histogram_names( *histogram_names_ptr);
    std::vector<unsigned int> histo_id_list;
    histo_id_list.reserve( histogram_names.size() );
    for (std::vector<std::string>::const_iterator histo_iter = histogram_names.begin();
	 histo_iter != histogram_names.end();
	 ++histo_iter) {
      histo_id_list.push_back(m_histogramCache->histogramIndex(measurementType(), serial_number, kModuleItem, *histo_iter ) );
    }

    try {
    PixA::Pp0List pp0_list = conn.pp0s(rod_name);
    PixA::Pp0List::iterator pp0_begin = pp0_list.begin();
    PixA::Pp0List::iterator pp0_end = pp0_list.end();
    for (PixA::Pp0List::iterator pp0_iter = pp0_begin; pp0_iter != pp0_end; ++pp0_iter) {
      PixA::ModuleList::iterator module_begin = PixA::modulesBegin(pp0_iter);
      PixA::ModuleList::iterator module_end = PixA::modulesEnd(pp0_iter);
      for (PixA::ModuleList::iterator module_iter = module_begin; module_iter != module_end; ++module_iter) {
	unsigned int n_histos=0;
	for (std::vector<unsigned int>::const_iterator histo_id_iter = histo_id_list.begin();
	     histo_id_iter != histo_id_list.end();
	     ++histo_id_iter) {
	  HistoInfo_t histo_info;
	  m_histogramCache->numberOfHistograms(measurementType(), serial_number, kModuleItem,  PixA::connectivityName(module_iter), *histo_id_iter, histo_info , true);
	  if (histo_info.minHistoIndex()<histo_info.maxHistoIndex()) {
	    n_histos++;
	  }
	}
	State_t histogram_number_state = HistogramStatus::state(histogram_names.size(), n_histos);


	bool update = true;
	try {
	  {
	    Lock lock(calibrationData()->calibrationDataMutex());
	  const CalibrationData::ConnObjDataList conn_obj_data ( calibrationData()->calibrationData().getConnObjectData( PixA::connectivityName(module_iter) ) );
	  bool module_enabled = conn_obj_data.enableState(measurementType(), serial_number).enabled();
	  if (module_enabled) {
	    if (histogram_number_state == HistogramStatus::kPresumablyDisabled) {
	      histogram_number_state = HistogramStatus::kMissingHistograms;
	    }
	    if (histogram_number_state != HistogramStatus::kOk && histogram_number_state  != HistogramStatus::kNoneExpected) {
	      rod_status = combineStatus(rod_status, StatusMonitor::kFailed);
	    }
	    else {
	      rod_status = combineStatus(rod_status, StatusMonitor::kOk);
	    }
	  }
	  try {
	    State_t old_histogram_number_state = conn_obj_data.value<State_t>(measurementType(), serial_number, m_varId) ;
	    if ( old_histogram_number_state == histogram_number_state) {
	      update=false;
	    }
	  }
	  catch(CalibrationData::MissingValue &) {
	    if (histogram_number_state == HistogramStatus::kPresumablyDisabled && !module_enabled) {
	      update=false;
	    }
	  }
	}
	  if (update) {
	    receptor()->setValue(kModuleValue, measurementType(), serial_number, PixA::connectivityName(module_iter), extractor, histogram_number_state);
	  }

	}
	catch(CalibrationData::MissingConnObject &) {
	    // should not happen
	  std::cout << "ERROR [ScanStautsMonitaor::process] connectivity object " <<  PixA::connectivityName(module_iter) 
		    << " does not exist for " << makeSerialNumberString(measurementType(),serial_number) << "." << std::endl;
	  rod_status = combineStatus(rod_status, StatusMonitor::kUnknown);
// 	  if (rod_status == StatusMonitor::kOk ) {
// 	    rod_status = StatusMonitor::kUnknown;
// 	  }
	}
      }
    }
    }
    catch (PixA::ConfigException &) {
      std::cout << "ERROR [ScanStautsMonitaor::process] Config exception for ROD " <<  rod_name  << std::endl;
      //      rod_status = StatusMonitor::kUnknown;
      rod_status = combineStatus(rod_status, StatusMonitor::kUnknown);
    }
    }
    if (m_verbose) {
      std::cout << "INFO [ScanStautsMonitaor::process]  ROD " <<  rod_name  << " status = " << rod_status << std::endl;
    }
    return rod_status;
  }

  StatusMonitor::EStatus ScanStatusMonitor::finish(SerialNumber_t serial_number, StatusMonitor::EStatus /*combined_status*/) {
    // @todo should set the status of the scan meta data.
    StatusMonitor::EStatus global_status = StatusMonitor::kUnset;

    // count histograms and set status per module.
    PixA::ConnectivityRef conn = calibrationData()->scanConnectivity(serial_number);

    // Now the final number of histograms is knwown, can reconsider the histogram status
    for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
	 crate_iter != conn.crates().end();
	 ++crate_iter) {

      PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
      PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
      for (PixA::RodList::const_iterator rod_iter = rod_begin;
	   rod_iter != rod_end;
	   ++rod_iter) {

	// ignore disabled RODs
	try {
	  const CalibrationData::ConnObjDataList conn_obj_data ( calibrationData()->calibrationData().getConnObjectData( PixA::connectivityName(rod_iter)) );
	  if ( conn_obj_data.enableState(measurementType(), serial_number).enabled() ) {
	    EStatus rod_status = process(serial_number, PixA::connectivityName(rod_iter) );
	    global_status = combineStatus(global_status, rod_status);
	  }
	}
	catch(CalibrationData::MissingConnObject &) {
	  // @todo Should not happen. But could it happen ? What to do if it happens ? 
	  std::cout << "ERROR [StatusMonitor::monitorStatus] No calibration data for ROD " << PixA::connectivityName(rod_iter)
		    << " in " << makeSerialNumberString(measurementType(),serial_number) << "."
		    << std::endl;
	  global_status = combineStatus(global_status, StatusMonitor::kUnknown);
// 	  if (global_status==StatusMonitor::kOk) {
// 	    global_status=StatusMonitor::kUnknown;
// 	  }
	}

      }
    }
    calibrationData()->updateStatus( measurementType(), serial_number, global_status);
    return global_status;
  }

}
