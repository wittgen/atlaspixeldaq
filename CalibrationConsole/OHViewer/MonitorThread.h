#ifndef _PixCon_MonitorThread_h_
#define _PixCon_MonitorThread_h_

#include <IsMonitor.h>
#include <list>
#include <map>
#include <Lock.h>
#include <Flag.h>

#include <IStatusMonitor.h>

#include <qthread.h>

namespace PixCon {

  class MonitorThread  : public QThread
  {

  public:

    MonitorThread(unsigned long timeout=0) : m_timeout(timeout), m_abort(false) { start(); }

    virtual ~MonitorThread();

    /** To abort the monitoring process.
     */
    void abort();

    /** Add a scan to the list of monitored scans.
     */
    void addMonitor(const std::shared_ptr<IStatusMonitor> &a_status_monitor );


    /** Signal the monitoring thread to abort.
     */
    void initiateShutdown() {
      abort();
    }

    /** Signal the monitoring thread to abort and wait until the thread has stopped.
     */
    void shutdownWait();

    void setTimeout(unsigned long timeout) { m_timeout=timeout; }

  protected:
    void run();

    Flag &statusChangeFlag()             { return m_statusChangeFlag; }
    const Flag &statusChangeFlag() const { return m_statusChangeFlag; }
  protected:

    Mutex m_monitorMutex;
    std::list< std::shared_ptr< IStatusMonitor > > m_monitorList;
    Flag m_statusChangeFlag;
    Flag m_exit;

    unsigned long m_timeout;

    bool m_abort;
  };

}
#endif
