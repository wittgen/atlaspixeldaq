#include "OHHistogramProvider.h"
#include <oh/OHRootReceiver.h>

namespace PixCon {


  const std::string OHHistogramProviderBase::s_emptyString;
  const ExtendedHistoInfo_t OHHistogramProviderBase::s_dummyHistoInfo;

  HistoPtr_t OHHistogramProviderBase::loadHistogram(EMeasurementType type,
						SerialNumber_t serial_number,
						EConnItemType conn_item_type,
						const std::string &connectivity_name,
						const std::string &histo_name,
						const Index_t &index)
  {
    if (type != kScan && type != kAnalysis) return NULL;

    const ExtendedHistoInfo_t &histo_info = extendedHistoInfo(type,serial_number, conn_item_type, connectivity_name, histo_name);
    if (&histo_info == &s_dummyHistoInfo) return NULL;

    std::stringstream pix_histo_name;
    //  pix_histo_name << ".*";
    
    if (type == kScan)     { pix_histo_name << "/S";}
    if (type == kAnalysis) { pix_histo_name << "/A";}

    pix_histo_name << std::setw(9) << std::setfill('0') << serial_number << '/';
    if (!histo_info.parentConnName().empty()) {
      pix_histo_name << histo_info.parentConnName() << '/';
    }
    // pix_histo_name << ".*";

    if (!connectivity_name.empty()) {
      pix_histo_name << connectivity_name << "/";
    }
    pix_histo_name << histo_name << "/";

    return histogram( pix_histo_name.str(), index);

//     std::cout << "INFO [OHHistogramProviderBase::loadHistogram] name= " << pix_histo_name.str() << std::endl;
//     PixScanHistoLoader receiver;
//     OHHistogramIterator  histo_iter( *m_partition,
// 				     m_ohServerName,
// 				     ".*" /* @todo Care about the provider name ? */,
// 				     pix_histo_name.str());
//     while ( histo_iter++ ) {
//       histo_iter.retrieve( receiver );
//       return receiver.histo();
//     }
//     return NULL;
  }

//   void OHHistogramProviderBase::updateHistogramList(EMeasurementType type,
// 						SerialNumber_t serial_number,
// 						EConnItemType /*conn_item_type*/,
// 						const std::string &rod_name )
//   {
//     if (type != kScan ) return;

//     std::stringstream pix_histo_name;
//     pix_histo_name << "/S" << std::setw(9) << std::setfill('0') << serial_number << '/';
//     if (rod_name.size()>0) {
//       pix_histo_name << rod_name << "/";
//     }

//     pix_histo_name << ".*";

//     std::cout << "INFO [HistoReceiver::getHistograms] Search for histograms " << pix_histo_name.str() << " in " << m_ohServerName << "." << std::endl;
//     OHHistogramIterator  histo_iter( *m_partition,
// 				     m_ohServerName,
// 				     ".*" /* @todo Care about the provider name ? */,
// 				     pix_histo_name.str());
//     {
//     Lock lock(m_histoMutex);
//     HistoData_t &scan_data = m_histoInfoMap[serial_number];
//     PixScanHistoIndexer receiver(scan_data[kModule], scan_data[kRod]);
//     //    unsigned int counter=0;
//     while ( histo_iter++ ) {
//       histo_iter.retrieve( receiver );
//     }
//     std::cout << "INFO [OHHistogramProviderBase::requestHistogramList] histograms module  = " << scan_data[kModule].size()
// 	      << " rod=" << scan_data[kRod].size()
// 	      << "." << std::endl;
//     scan_data.setUptodate(rod_name);
//     }

//   }

  void OHHistogramProviderBase::requestHistogramList(EMeasurementType type,
						 SerialNumber_t serial_number,
						 EConnItemType conn_item_type,
						 std::vector< std::string > &histogram_name_list,
						 const std::string &rod_name )
  {

    EHistoConnItemTypes histo_conn_type = histoConnType (conn_item_type);
    if (histo_conn_type >= kNConnItemTypes ) return;

    bool is_uptodate;
    HistoData_t *histo_data=NULL;
    {
      Lock lock(m_histoMutex);
      histo_data = &(m_histoInfoMap[FullKey_t(type,serial_number)]);
      is_uptodate = histo_data->isUptodate(rod_name);
    }
    if (!is_uptodate) {
      updateHistogramList(type,serial_number,conn_item_type, rod_name);
    }

    Lock lock(m_histoMutex);
    std::map<std::string, std::map<std::string, ExtendedHistoInfo_t > > &histo_type_map = (*histo_data)[histo_conn_type];
    for(std::map<std::string, std::map<std::string, ExtendedHistoInfo_t > >::const_iterator histo_type_iter = histo_type_map.begin();
	histo_type_iter != histo_type_map.end();
	histo_type_iter++) {
      std::string tempstring = histo_type_iter->first;	  
      histogram_name_list.push_back( histo_type_iter->first );
    }
  }

  
  const ExtendedHistoInfo_t &OHHistogramProviderBase::extendedHistoInfo(EMeasurementType type,
									SerialNumber_t serial_number,
									EConnItemType conn_item_type,
									const std::string &connectivity_name,
									const std::string &histo_name)
  {

    if (type != kScan && type != kAnalysis) return s_dummyHistoInfo;

    EHistoConnItemTypes histo_conn_type = histoConnType (conn_item_type);
    if (histo_conn_type >= kNConnItemTypes ) return s_dummyHistoInfo;

    std::map<FullKey_t, HistoData_t >::const_iterator  serial_number_iter;
    bool is_uptodate;
    {
      Lock lock(m_histoMutex);
      serial_number_iter = m_histoInfoMap.find(FullKey_t(type,serial_number));

      if (serial_number_iter == m_histoInfoMap.end()) {
	return s_dummyHistoInfo;
      }

      is_uptodate = serial_number_iter->second.isUptodate(s_emptyString);
    }

    if (!is_uptodate) {
      updateHistogramList(type,serial_number,conn_item_type, s_emptyString);
    }

    Lock lock(m_histoMutex);
    std::map<std::string, std::map<std::string, ExtendedHistoInfo_t> >::const_iterator histo_type_iter = serial_number_iter->second[histo_conn_type].find(histo_name);
    if (histo_type_iter == serial_number_iter->second[histo_conn_type].end()) {
      return s_dummyHistoInfo;
    }

    std::map<std::string, ExtendedHistoInfo_t>::const_iterator conn_iter = histo_type_iter->second.find(connectivity_name);
    if (conn_iter == histo_type_iter->second.end()) {
      return s_dummyHistoInfo;
    }
    return conn_iter->second;
  }

  void OHHistogramProviderBase::reset()  {
    Lock lock(m_histoMutex);
    m_histoInfoMap.clear();
  }

}
