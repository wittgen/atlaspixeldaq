#ifndef _MetaHistogramProvider_H_
#define _MetaHistogramProvider_H_

#include "IHistogramProvider.h"
#include <Lock.h>
#include <vector>
#include <map>

#include <memory>

class TFile;
class TDirectory;

class Regex_t;

namespace PixLib {
  class ScanInfo_t;
}

namespace PixCon {

  class CalibrationDataManager;

  class MetaHistogramProvider  : public IHistogramProvider
  {
  public:
    MetaHistogramProvider(std::shared_ptr<IHistogramProvider> oh_provider,
			  std::shared_ptr<IHistogramProvider> file_histo_provider);
    ~MetaHistogramProvider();

    /** Load a histogram from the server.
     * @param type the type of the measurement : scan, analysis or current.
     * @parem serial_number the serial number of the scan.
     * @param conn_item_type the connectivity item type of the histogram.
     * @param connectivity_name connectivity name of the module.
     * @param histo_name name of the histogram.
     * @param index vector which contains the indices for the pix scan histo.
     */
    HistoPtr_t loadHistogram(EMeasurementType type,
 			     SerialNumber_t serial_number,
 			     EConnItemType conn_item_type,
 			     const std::string &connectivity_name,
 			     const std::string &histo_name,
 			     const Index_t &index);

    /** Request the names of all histograms assigned to the given serial number and connectivity object type.
     * @param type the type of the measurement : scan, analysis or current.
     * @param serial_number the serial number of the scan.
     * @param conn_item_type the type of the connectivity item for which the histogram list should be requested.
     * @param histogram_name_list vector which will be extended by the histogram names associated to the serial number and object type.
     * Note, it is the responsibility of the caller to clear the given histogram name list before the call if the old content is not desired.
     * This allows to add histograms of different types in subsequent calls.
     * @sa EConnItemType
     */
    void requestHistogramList(EMeasurementType type,
			      SerialNumber_t serial_number,
			      EConnItemType conn_item_type,
			      std::vector< std::string > &histogram_name_list );


    void requestHistogramList(EMeasurementType type,
			      SerialNumber_t serial_number,
			      EConnItemType conn_item_type,
			      std::vector< std::string > &histogram_name_list,
			      const std::string &/*rod_name*/);

    /** Return the dimensions of the pix scan histo levels.
     * @param type the type of the measurement : scan, analysis or current.
     * @parem serial_number the serial number of the scan.
     * @param conn_item_type the connectivity item type of the histogram.
     * @param connectivity_name connectivity name of the module.
     * @param histo_name name of the histogram.
     * @param index vector which will be filled with the dimensions of the pix scan histo levels.
     */
    void numberOfHistograms(EMeasurementType type,
			    SerialNumber_t serial_number,
			    EConnItemType conn_item_type,
			    const std::string &connectivity_name,
			    const std::string &histo_name,
			    HistoInfo_t &index);

    /** Add a new root file to provide histograms for the given measurement.
     */
    void addScan(EMeasurementType measurement, SerialNumber_t serial_number, PixLib::ScanInfo_t &scan_info);


    /** Add a new root file to provide histograms for the given measurement.
     */
    void addScan(EMeasurementType measurement, SerialNumber_t serial_number, const std::string &file_name);

    /** Remove the cached information for the given measurement.
     */
    void clearCache(EMeasurementType measurement, SerialNumber_t serial_number);

    /** Reset all provider.
     * And set preferred provider to none for all scans and analyses. Thus addScan needs to be
     * called again for all scans and analyses.
     */
    void reset();

  protected:
    enum EPreferredProvider { kNone, kBoth, kOHOnly, kFileBasedOnly };

    bool tryProvider(EMeasurementType measurement, std::map<SerialNumber_t, EPreferredProvider>::const_iterator preferred_provider_iter, unsigned int provider_i) const {
      EPreferredProvider preferred_provider = preferredProvider(measurement, preferred_provider_iter);
      return ( ((preferred_provider != kNone) && (provider_i==0 && preferred_provider !=kFileBasedOnly)) || (provider_i==1 && preferred_provider != kOHOnly));
    }

    void disable (EMeasurementType measurement, std::map<SerialNumber_t, EPreferredProvider>::iterator preferred_provider_iter, unsigned int provider_i)  {
      if (preferred_provider_iter!=m_preferredProvider[measurement].end()) {
	if (provider_i==0 && preferred_provider_iter->second == kBoth) {
	  preferred_provider_iter->second=kFileBasedOnly;
	}
	else if (provider_i==1 && preferred_provider_iter->second == kBoth) {
	  preferred_provider_iter->second=kOHOnly;
	}
      }
    }

    EPreferredProvider preferredProvider(EMeasurementType measurement, std::map<SerialNumber_t, EPreferredProvider>::const_iterator preferred_provider_iter) const {
      if (preferred_provider_iter==m_preferredProvider[measurement].end()) {
	return (m_histogramProvider[0].get() ? kOHOnly : kNone) ;
      }
      else {
	return preferred_provider_iter->second;
      }
    }

    std::map<SerialNumber_t, EPreferredProvider>::iterator preferredProvider(EMeasurementType measurement_type, SerialNumber_t serial_number) {
 
      std::map<SerialNumber_t, EPreferredProvider>::iterator provider_iter = m_preferredProvider[measurement_type].find(serial_number);
      if (provider_iter == m_preferredProvider[measurement_type].end()) {
	// If the preferred provider has not been defined the file is not known.
	// Thus, the only option is OH.
	std::pair< std::map<SerialNumber_t, EPreferredProvider>::iterator, bool > ret
	  = m_preferredProvider[measurement_type].insert(std::make_pair(serial_number, kOHOnly));
	if (!ret.second) {
	  return provider_iter;
	}
      }

      return provider_iter;
    }

    std::map<SerialNumber_t, EPreferredProvider>   m_preferredProvider[kNMeasurements];
    std::shared_ptr<IHistogramProvider>          m_histogramProvider[2];

  };

}
#endif
