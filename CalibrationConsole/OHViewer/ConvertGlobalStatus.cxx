#include "ConvertGlobalStatus.h"

namespace PixCon {

  MetaData_t::EStatus ConvertGlobalStatus::s_metaDataStatus[CAN::kAborted+1]={
    MetaData_t::kUnset,
    MetaData_t::kOk,
    MetaData_t::kFailed,
    MetaData_t::kAborted
  };

  CAN::GlobalStatus ConvertGlobalStatus::s_globalStatus[MetaData_t::kUnset+1]={
    CAN::kSuccess,
    CAN::kFailure,
    CAN::kAborted,
    CAN::kNotSet,
    CAN::kNotSet
  };

}
