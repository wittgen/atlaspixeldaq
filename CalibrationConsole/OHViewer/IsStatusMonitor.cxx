#include <Flag.h>
#include "CalibrationDataManager.h"
#include "IsMonitor.h"

class StatusMonitor
{

public:

  class IStatusChangeListener
  {
  public:
    virtual ~IStatusChangeListener() {}
    virtual void notify(const std::string &rod_name) = 0;
  };

  class StatusInfo_t : public IStatusChangeListener
  {
  public:
    ScanData( Flag &scan_status_change_flag ) : m_statusChanged( &scan_status_change_flag) {}

    void notify(const std::string &conn_name)  {
      {
	Lock lock(m_rodListMutex);
	m_rodList.push_back(conn_name);
      }
      m_statusChanged->setFlag();
    }

    std::list<std::string>::const_iterator begin() const { return m_rodList.begin(); }
    std::list<std::string>::const_iterator end()   const { return m_rodList.end(); }
    Mutex &mutex() { return m_rodListMutex; }

  private:
    Flag *m_statusChanged;
    Mutex m_rodListMutex;
    std::list<std::string> m_rodList;
  };

  void abort();

  void monitorStatus(SerialNUmber_t serial_number) {
    Lock lock(m_statusInfoMutex);
    std::map<SerialNumber_t, StatusInfo_t>::iterator serial_number_iter = m_statusInfo.find(serial_number);
    if (serial_number_iter == m_statusInfo.end()) {
      std::pair< std::map<SerialNumber_t, StatusInfo_t>::iterator, bool>
	ret = m_statusInfo.insert(std::make_pair(serial_number, StatusInfo_t(m_statusChanged)));
      if (!ret.second) {
	std::cout << "ERROR [HistogramAnalyser::monitorScan] Failed to add status block for " << serial_number << " to the list."
		  << std::endl;
      }
      serial_number_iter = ret.first;
    }
  }

protected:
  void run();
  virtual void process(SerialNumber_t serial_number, const std::string &rod_name) = 0;

private:
  Flag m_statusChanged;
  Flag m_exit;

  Mutex m_statusInfoMutex;
  std::map<SerialNUmber_t, StatusInfo_t> m_statusInfo

};

class HistogramAnalyser : public StatusMonitor
{

proteced:
  void process(SerialNumber_t serial_number, const std::string &rod_name);

  State_t  getHistogramState(unsigned int n_histograms_expected, unsigned int n_histograms_observed);
};


const char *IsStatusMonitor::m_measurementTypeId[kNMeasurements]={'\0', 'S', 'A','\0'};

IsStatusMonitor::IsStatusMonitor(IPCPartition &partition, 
				 std::shared_ptr<IsReceptor> &receptor,
				 EMeasurementType measurement_type,
				 SerialNUmber_t serial_number,
				 EValueConnType conn_type,
				 const std::string &var_name,
				 const std::shared_ptr<CalibrationDataManager> &calibration_data,
				 unsigned int status_threshold,
				 Flag &scan_status_change_flag)
  :  IsMonitorReceiver<int>(receptor, conn_type, var_name),
     m_calibrationData(calibration_data),
     m_statusThreshold(status_threshold),
     m_serialNumber(serial_number),
     m_measurementType(measurement_type),
     m_statusChanged( &scan_status_change_flag)
{
  assert(measurement_type< measurement_type && m_measurementTypeId[measurement_type]!='\0');
  IsMonitorReceiver<int>::init(partition);
}

/** Extract connectivitity name from Scan / Analysis status IS variable at construction time.
 * The connectivity name is memorised and passed if @ref extractVar is called.
 */
  class StatusNameExtractor : public IVarNameExtractor
  {
  public:

    StatusNameExtractor(const std::string &full_is_name) {
      std::string::size_type is_server_name_end = full_is_name.find(".");
      if (is_server_name_end != std::string::npos) {
	std::string::size_type conn_start = full_is_name.find("/",is_server_name_end);
	if (conn_start != std::string::npos && conn_start+1 < is_server_name_end.size()) {
	  conn_start++;
	  std::string::size_type conn_end = full_is_name.find("/",conn_start);
	  if (conn_end != std::string::npos) {
	    m_connName(std::string(full_is_name, conn_start, conn_end - conn_start));
	  }
	}
      }
    }


    void extractVar(const std::string &/*full_is_name*/, ConnVar_t &conn_var) const
    {
      conn_var.connName() = m_connName;
      conn_var.varName()=s_statusName;
    }

    const std::string &connName() { return m_connName; }

  private:
    std::string              m_connName;
    static const std::string s_statusName;
  };

std::string StatusVarNameExtractor::s_statusName("Status");


void IsStatusMonitor::setValue(const std::string &is_name, int status)
{
  if (m_lastStatus != status) {
    StatusNameExtractor extractor(is_name);
    IsMonitorReceiver<int>::m_receptor->setValue(IsMonitorReceiver<int>::m_connType, is_name, extractor, static_cast<State_t>(status) );
    if (status >= m_statusThreshold) {
      notify(extractor.connName());
    }
    m_lastStatus = status;
  }
}

std::string IsStatusMonitor::makeRegex()
{
  std::stringstream pattern;
  pattern << "/" << m_measurementTypeId[m_measurementType] << std::setw(9) << std::setfill('0') << m_serialNumber;
  return pattern.str();
}



void StatusMonitor::abort() {
  m_abort=true;
  m_statusChanged.setFlag();
}

void StatusMonitor::run()
{
  for(;;) {
    m_statusChanged.wait(m_abort);
    if (m_abort) break;

    Lock lock(m_histoListMutex);
    Lock calibration_data_lock(m_calibrationData->mutex());
    {
      std::map<SerialNUmber_t, std::list<std::string> >::iterator serial_number_iter = m_histoList.begin();
      bool reached_serial_number_list_end = (serial_number_iter == m_histoList.end());
      while (!reached_serial_number_list_end) {

	std::map<SerialNUmber_t, std::list<std::string> >::iterator current_serial_number = serial_number_iter++;
	reached_serial_number_list_end = (serial_number_iter == m_histoList.end());

	std::vector histogram_names;
	{
	  std::list<std::string>::iterator rod_iter = current_serial_number->second.begin();
	  bool reached_rod_list_end = (rod_iter == current_serial_number->second.end() );
	  while (!reached_rod_list_end) {

	    std::list<std::string>::iterator current_rod_iter = rod_iter++;
	    reached_rod_list_end = (rod_iter == current_serial_number->second.end() );

	    Lock rod_lock(current_serial_number->second.mutex());
	    process(current_serial_number->first, *current_rod_iter);
	    current_serial_number->second.erase(current_rod_iter);

	  }
	}

	if (current_serial_number->second.empty()) {
	  m_histoList.erase(current_serial_number);
	}
      }
    }
  }
  m_exit.setFlag();
}


void HistogramAnalyser::process(SerialNumber_t serial_number, const std::string &rod_name)
{
  // get per ROD histograms, count histograms, and set histogram status in calibration data container
  {
    histogram_names.clear();
    m_histoReceiver->requestHistogramList(kScan, serial_number, kRodItem, rod_name, histogram_names);
    unsigned int n_histos=0;
    for (std::vector<std::string>::const_iterator histo_iter = histogram_names.begin();
	 histo_iter != histogram_names.end();
	 ++histo_iter) {
      HistoInfo_t histo_info;
      m_histoReceiver->numberOfHistograms(kScan, serial_number, kRodItem,  rod_name, histo_info );
      if (histo_info.minHistoIndex()<histo_info.maxHistoIndex()) {
	n_histos++;
      }
    }

    State_t histogram_numbers = getHistogramState(histogram_names.size(), n_histos);
    m_calibrationData->calibrationData().setValue(kScan, serial_number, kRodItem, rod_name, var_id, histogram_numbers);
  }

  // get module histogram list.
  histogram_names.clear();
  m_histoReceiver->requestHistogramList(kScan, serial_number, kModuleItem, rod_name, histogram_names);

  // count histograms and set status per module.
  PixA::Pp0List pp0_list = m_conn.rodList(rod_name);
  PixA::Pp0List::iterator pp0_begin = pp0_list.begin();
  PixA::Pp0List::iterator pp0_end = pp0_list.end();
  for (PixA::Pp0List::iterator pp0_iter = pp0_begin; pp0_iter != pp0_end; ++pp0_iter) {
    PixA::ModuleList::iterator module_begin = pp0_iter->begin();
    PixA::ModuleList::iterator module_end = pp0_iter->end();
    for (PixA::ModuleList::iterator module_iter = module_begin; module_iter != module_end; ++module_iter) {
      unsigned int n_histos=0;
      for (std::vector<std::string>::const_iterator histo_iter = histogram_names.begin();
	   histo_iter != histogram_names.end();
	   ++histo_iter) {
	HistoInfo_t histo_info;
	m_histoReceiver->numberOfHistograms(kScan, serial_number, kModuleItem,  PixA::connectivityName(module_iter), histo_info );
	if (histo_info.minHistoIndex()<histo_info.maxHistoIndex()) {
	  n_histos++;
	}
      }
      State_t histogram_numbers = getHistogramState(histogram_names.size(), n_histos);
      m_calibrationData->calibrationData().setValue(kScan, serial_number, kModuleItem, PixA::connectivityName(module_iter), var_id, histogram_numbers);

    }
  }
}

State_t  HistogramAnalyser::getHistogramState(unsigned int n_histograms_expected, unsigned int n_histograms_observed) 
{
  State_t histogram_number_state;
  if (n_histograms_observed == n_histograms_expected) {
    histogram_number_state = kOkay;
  }
  else if (n_histograms_observed == 0) {
    histogram_names = kNoHistograms;
  }
  else if (n_histograms_observed < n_histograms_expected) {
    histogram_names = kMissingHistograms;
  }
  else if (n_histograms_observed > n_histograms_expected) {
    histogram_names = kTooManyHistgrams;
  }
  return histogram_state;
}
