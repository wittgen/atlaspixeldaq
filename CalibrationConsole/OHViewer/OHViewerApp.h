// Dear emacs, this is -*-c++-*-
#ifndef _OHViewerApp_H_
#define _OHViewerApp_H_

#  if defined(HAVE_QTGSI)
#    include "TQRootApplication.h"
#    include "TQApplication.h"

typedef TQRootApplication QRootApplication;

#  else
#include <QApplication>

// dummy implementation
// no ROOT - histograms will not be displayed.
class QRootApplication : public QApplication
{
public:
  QRootApplication(int argc, char **argv,int poll=0) : QApplication(argc, argv) {}
};

#  endif

#include <vector>
#include <string>

#include <memory>

#include <Lock.h>
#include <CalibrationDataTypes.h>
#include "UserMode.h"
#include <memory>

class IPCPartition;
class ISInfoReceiver;

namespace PixCon {
  class OHHistogramProvider;
  class IHistogramProvider;
  class CategoryList;
  class CalibrationDataManager;
  class OHViewer;

  class IsReceptor;
}

class OHViewerApp : public QRootApplication
{
public:
  OHViewerApp(int qt_argc, char **qt_argv,
	      std::shared_ptr<PixCon::UserMode>& user_mode,
	      const std::string &partition_name,
	      const std::string &is_server_name,
	      const std::string &oh_partition_name,
	      const std::string &oh_server_name,
	      const std::string &histo_name_server_name,
	      int poll=0);
  ~OHViewerApp();

  void addScan( PixCon::SerialNumber_t scan_serial_number );
  void addAnalysis( PixCon::SerialNumber_t analysis_serial_number );

  //  void setTags(const std::string &object_tag_name, const std::string &tag_name, const std::string &cfg_tag_name, const std::string &mod_cfg_tag_name="");

protected:
  //  static unsigned int addOHFiles( PixCon::OHHistogramProviderBase &histogram_server, const std::vector<std::string> &result_files );

  std::shared_ptr<PixCon::CategoryList> m_categories;
  std::shared_ptr<PixCon::CalibrationDataManager> m_calibrationData;

private:

  std::unique_ptr<PixCon::OHViewer> m_viewer;
  std::unique_ptr<IPCPartition>     m_partition;
  std::unique_ptr<IPCPartition>     m_ohPartition;

  std::unique_ptr<ISInfoReceiver>         m_infoReceiver;
  std::shared_ptr<PixCon::IsReceptor> m_receptor;

  std::shared_ptr<PixCon::IHistogramProvider>     m_histogramProvider;
  std::shared_ptr<PixCon::IHistogramProvider>     m_histogramProviderInterceptor;

  bool m_verbose;
};
#endif
