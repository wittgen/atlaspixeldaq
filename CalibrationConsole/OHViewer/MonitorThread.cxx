#include "MonitorThread.h"
#include "CalibrationDataManager.h"

namespace PixCon {


  MonitorThread::~MonitorThread() 
  {
    initiateShutdown();
    shutdownWait();
  }

  void MonitorThread::abort() {
    m_abort=true;
    statusChangeFlag().setFlag();
  }

  /** Signal the monitoring thread to abort and wait until the thread has stopped.
   */
  void MonitorThread::shutdownWait()
  {
    abort();
    if (isRunning()) {
      m_exit.wait();
      while (!isFinished()) {
	usleep(300000); //300ms
      }
    }
  }

  void MonitorThread::addMonitor(const std::shared_ptr<IStatusMonitor> &a_status_monitor )
  {
    assert( a_status_monitor.get() );
    a_status_monitor->setStatusChangeFlag( m_statusChangeFlag );
    Lock lock(m_monitorMutex);
    m_monitorList.push_back(a_status_monitor);
  }


  void MonitorThread::run()
  {
    //    std::cout << "INFO [MonitorThread::run] started." << std::endl;
    time_t last_status_change=time(NULL);
    for(;;) {

      bool status_change=true;
      if (m_timeout>0) {
	status_change = statusChangeFlag().timedwait(m_timeout, m_abort);
      }
      else {
	statusChangeFlag().wait(m_abort);
      }

      time_t current_time = time(NULL);
      double delta_time =0.;
      if (status_change) {
	last_status_change = current_time;
      }
      else {
	delta_time = difftime(current_time, last_status_change);
      }

      if (m_abort) break;
      //      std::cout << "INFO [MonitorThread::run] status change was signaled." << std::endl;

      std::list< std::shared_ptr< IStatusMonitor > >::iterator monitor_iter;
      {
	Lock lock( m_monitorMutex );
	monitor_iter = m_monitorList.begin();
	if (monitor_iter == m_monitorList.end() ) continue;
      }

      for(unsigned int counter=0;!m_abort;counter++) {
	// for debugging
	if (status_change) {
	  //	  std::cout << "INFO [MonitorThread::run] process monitor." << counter << std::endl;
	  (*monitor_iter)->process();
	}
	else {
	  //	  std::cout << "INFO [MonitorThread::run] timeout." << counter << std::endl;
	  (*monitor_iter)->timeout(delta_time);
	}

	{
	  Lock lock( m_monitorMutex );
	  monitor_iter++;
	  if (monitor_iter == m_monitorList.end() ) break;
	}
      }

    }
    m_exit.setFlag();
    //    std::cout << "INFO [MonitorThread::run] done." << std::endl;
  }

}



