#include "PixHistoServerInterfaceProvider.h"
#include "PixScanHistoIndexer.h"


namespace PixCon {

  PixHistoServerInterfaceProvider::PixHistoServerInterfaceProvider(IPCPartition &oh_partition,
								   const std::string &oh_server_name,
								   const std::string &name_server_name,
								   bool verbose)
    : m_partitionName(oh_partition.name()),
      m_ohName(oh_server_name),
      m_nameServerName(name_server_name),
      m_verbose(verbose) { reset(); }

  std::string PixHistoServerInterfaceProvider::makeProviderName(const std::string &provider_name_head)
  {
    std::stringstream full_name;
    pid_t the_pid = getpid();
    //@todo use console session id ?
    char hostname_temp[HOST_NAME_MAX];
    gethostname(hostname_temp,HOST_NAME_MAX);
    full_name << provider_name_head << '_' << hostname_temp << '_' << the_pid;
    return full_name.str();
  }

  void PixHistoServerInterfaceProvider::updateHistogramList(EMeasurementType type,
						SerialNumber_t serial_number,
						EConnItemType /*conn_item_type*/,
						const std::string &rod_name )
  {
    if (type != kScan && type != kAnalysis) return;

    std::stringstream pix_histo_name;
    if (type == kScan)     { pix_histo_name << "/S"; }
    if (type == kAnalysis) { pix_histo_name << "/A"; }
    pix_histo_name << std::setw(9) << std::setfill('0') << serial_number << '/';
    if (rod_name.size()>0) {
      pix_histo_name << rod_name << "/";
    }

    try {

      std::vector<std::string> histo_list = m_histoServerInterface->superLs(pix_histo_name.str(),true);

      {
	Lock lock( histoMutex() );
	HistoData_t &histo_data = histoData(type,serial_number);

	PixScanHistoIndexer indexer(histo_data[kModule], histo_data[kRod], m_verbose);
	//    unsigned int counter=0;
	for (std::vector<std::string>::const_iterator histo_iter = histo_list.begin();
	     histo_iter != histo_list.end();
	     histo_iter++) {
	  indexer.add( pix_histo_name.str(), *histo_iter );
	}
	if (m_verbose) {
	  std::cout << "INFO [PixHistoServerInterfaceProvider::requestHistogramList] histograms module  = " << histo_data[kModule].size()
		    << " rod=" << histo_data[kRod].size()
		    << "." << std::endl;
	}
	histo_data.setUptodate(rod_name);
      }
    }
    catch(  PixLib::PixHistoServerExc &err ) {
      if (m_verbose) {
	std::cout << err.dumpLevel() << " [PixHistoServerInterfaceProvider::updateHistogramList] caught pix histo name server exception :: "
		  << err.getId() << " : " << err.getDescriptor()
		  << std::endl;
      }
    }
    catch(  PixLib::PixNameServerExc &err ) {
      if (m_verbose) {
	std::cout << err.dumpLevel() << " [PixHistoServerInterfaceProvider::updateHistogramList] caught pix histo name server exception :: "
		  << err.getId() << " : " << err.getDescriptor()
		  << std::endl;
      }
    }
    catch (...) {
      std::cout << "WARNING [PixHistoServerInterfaceProvider::updateHistogramList] Caught unknown exception in pix histo name server."
	           " Don't know whether this is a problem or not. " << std::endl;
    }

  }

  HistoPtr_t PixHistoServerInterfaceProvider::histogram(const std::string &histo_name, const std::vector<unsigned int> &index)
  {
    OHRootHistogram temp;
    if (index.size()>4) {
      std::cout << "ERROR [PixHistoServerInterfaceProvider::histogram] Too many levels (" << index.size() << " <? 4)" << " for " << histo_name << "."  << std::endl;
    }

    try {
      m_histoServerInterface->scanHistoBack(histo_name,
					    (index.size()>1 ? static_cast<int>(index[0]) : -1 ),
					    (index.size()>2 ? static_cast<int>(index[1]) : -1 ),
					    (index.size()>3 ? static_cast<int>(index[2]) : -1 ),
					    (index.size()>0 ? static_cast<int>(index[index.size()-1]) : -1 ),
					    temp);
    }
    catch(  PixLib::PixNameServerExc &err ) {
      std::cout << err.dumpLevel() << " [PixHistoServerInterfaceProvider::updateHistogramList] caught pix histo name server exception :: "
		<< err.getId() << " : " << err.getDescriptor()
		<< std::endl;
    }
    catch (...) {
      std::cout << "ERROR [PixHistoServerInterfaceProvider::updateHistogramList] caught unknown exception in pix histo name server."
		<< std::endl;
    }
    if (m_verbose) {
      std::cout << "INFO [PixHistoServerInterfaceProvider::histogram] name= " <<  ( temp.histogram.get()  ? temp.histogram->GetName() : histo_name.c_str() )
		<< " ptr = " << static_cast<void *>(temp.histogram.get()) << std::endl;
    }
    return temp.histogram.release();

    return NULL;

  }

  void PixHistoServerInterfaceProvider::reset() {
    OHHistogramProviderBase::reset();
    Lock lock( histoMutex() );
    m_histoServerInterface.reset();
    m_partition.reset();

    m_partition=std::make_unique<IPCPartition>(m_partitionName);
    m_histoServerInterface=
      std::make_unique<PixLib::PixHistoServerInterface>(m_partition.get(),
											 m_ohName,
											 m_nameServerName,
											 makeProviderName("CalibrationConsole"));
  }
}
