#include "ScanFileHistogramProvider.h"
#include "CalibrationDataManager.h"
#include <MetaDataService.hh>
#include <ScanMetaDataCatalogue.h>
#include <DataContainer/RootResultFileAccessBase.h>
#include <iostream>
#include <memory>

#include <TH1.h>
#include <TH2.h>

#include <DataContainer/HistoInfo_t.h>
int main(int argc, char **argv)
{
  std::shared_ptr<PixCon::CalibrationDataManager> calibration_data(new PixCon::CalibrationDataManager);
  std::unique_ptr<PixCon::ScanFileHistogramProvider> histo_server(new PixCon::ScanFileHistogramProvider(calibration_data));
  std::vector<unsigned int> serial_number;
  std::vector<std::string> conn_names;
  bool error = false;
  for (int arg_i=1; arg_i<argc; arg_i++) {
    if (strcmp(argv[arg_i],"-s")==0 && arg_i+1 < argc && isdigit(argv[arg_i+1][0])) {
      serial_number.push_back(atoi(argv[++arg_i]));
    }
    else if (strcmp(argv[arg_i],"-m")==0 && arg_i+1 < argc && isalpha(argv[arg_i+1][0])) {
      conn_names.push_back(argv[++arg_i]);
    }
    else {
      error = true;
    }
  }
  if (error) {
    std::cout << "usage: " << argv[0] << " [-i \"input root file\"] [-s serial-number] [-m module-name] " << std::endl
	      << std::endl
	      << "-i, -s and -m can be repeated as often as needed. So can the serial number,"
	      << std::endl;
    return -1;
  }

  try {
    CAN::InfoPtr_t<PixLib::ScanInfo_t> scan_info;
    for (std::vector<unsigned int>::const_iterator serial_number_iter = serial_number.begin();
	 serial_number_iter != serial_number.end();
	 ++serial_number_iter ) {
      scan_info = CAN::InfoPtr_t<PixLib::ScanInfo_t>( CAN::MetaDataService::instance()->getScanInfo(*serial_number_iter) );
      if (scan_info) {
	PixA::ConnectivityRef conn;
	{
	  conn =PixA::ConnectivityManager::getConnectivity( scan_info->idTag(),
							    scan_info->connTag(),
							    scan_info->aliasTag(),
							    scan_info->dataTag(),
							    scan_info->cfgTag(),
							    scan_info->modCfgTag(),
							    scan_info->cfgRev());


	}
	if (!conn) {
	  std::cerr << "ERROR [OHViewer::addScan] Failed to get connectivity for scan " << *serial_number_iter << "." << std::endl;
	}
	else  {
	  const std::string result_file_name = PixA::RootResultFileAccessBase::fileName(scan_info->file(),scan_info->alternateFile());
	  calibration_data->addScan(*serial_number_iter,
				     scan_info->scanStart(),
				     static_cast<unsigned int>(scan_info->scanEnd() - scan_info->scanStart()),
				     scan_info->scanType(),
				     scan_info->comment(),
				     static_cast<PixCon::MetaData_t::EQuality>( scan_info->quality() ),
				     PixA::ConfigHandle(),
				     result_file_name,
				     conn);

	  histo_server->addFile(PixCon::kScan, *serial_number_iter, result_file_name);
	}
      }
    }

    std::vector<std::string> histogram_list;
    std::vector<PixCon::EConnItemType> conn_item_types;
    std::vector<std::string> conn_item_type_names;
    conn_item_types.push_back(PixCon::kModuleItem);
    conn_item_type_names.push_back("Module");
    conn_item_types.push_back(PixCon::kRodItem);
    conn_item_type_names.push_back("ROD");

    for (std::vector<unsigned int>::const_iterator serial_number_iter = serial_number.begin();
	 serial_number_iter != serial_number.end();
	 ++serial_number_iter ) {
      std::vector<std::string>::const_iterator conn_item_type_name_iter = conn_item_type_names.begin();

      for (std::vector<PixCon::EConnItemType>::const_iterator conn_type_iter = conn_item_types.begin();
	   conn_type_iter != conn_item_types.end();
	   ++conn_type_iter, ++conn_item_type_name_iter) {

	// paranoia check
	assert( conn_item_type_name_iter != conn_item_type_names.end() );

	histogram_list.clear();
	histo_server->requestHistogramList(PixCon::kScan,*serial_number_iter, *conn_type_iter, histogram_list);
	std::cout << *conn_item_type_name_iter <<  " hisotgrams : " << std::endl;
	for (std::vector<std::string>::const_iterator histo_iter = histogram_list.begin();
	     histo_iter != histogram_list.end();
	     ++histo_iter) {
	  std::cout << histo_iter - histogram_list.begin() << " : " << *histo_iter << std::endl;
	}

	for (std::vector<std::string>::const_iterator conn_name_iter = conn_names.begin();
	     conn_name_iter != conn_names.end();
	     conn_name_iter++) {
	  for (std::vector<std::string>::const_iterator histo_iter = histogram_list.begin();
	     histo_iter != histogram_list.end();
	     histo_iter++) {
	    HistoInfo_t histo_info;
	    histo_server->numberOfHistograms(PixCon::kScan,*serial_number_iter, *conn_type_iter, *conn_name_iter, *histo_iter, histo_info);
	    std::cout << *conn_name_iter << " :: " << histo_iter - histogram_list.begin() << " : " << *histo_iter << " : ";
	    for (unsigned int j=0; j<3; j++) {
	      std::cout << histo_info.maxIndex(j) << ", ";
	    }
	    std::cout << histo_info.minHistoIndex() <<  " - " << histo_info.maxHistoIndex() << std::endl;
	    PixA::Index_t index;
	    histo_info.makeDefaultIndexList(index);
	    if (index.size()>0) {
	    for (unsigned int i=0; i<index.size()-1; i++) {
	      if (histo_info.maxIndex(i)>0) {
		index[i]=histo_info.maxIndex(i)-1;
	      }
	    }
	    if (histo_info.maxHistoIndex()>0) {
	      index.back()=histo_info.maxHistoIndex()-1;
	    }
	    std::unique_ptr<TH1> h1( histo_server->loadHistogram(PixCon::kScan,*serial_number_iter, *conn_type_iter, *conn_name_iter, *histo_iter, index) );
	    if (h1.get()) {
	      std::cout << " INFO [main " << argv[0] << "] histo " << h1->GetName() << " : "
			<< h1->GetXaxis()->GetXmin() << " - " << h1->GetXaxis()->GetXmax() << " / " << h1->GetNbinsX();
	      if (h1->InheritsFrom(TH2::Class())) {
		TH2 *h2=static_cast<TH2 *>(h1.get());
		std::cout << "; " << h2->GetYaxis()->GetXmin() << " - " << h2->GetYaxis()->GetXmax() << " / " << h1->GetNbinsY();
	      }
	      std::cout << std::endl;

	    }
	    }
	  }
	}

      }

    }
    return 0;
  }
  catch(std::exception &err) {
    std::cout << "FATAL [main" << argv[0] << "] caught exception : "  << err.what() << std::endl;
  }
  catch(...) {
    std::cout << "FATAL [main" << argv[0] << "] caught unknown exception." << std::endl;
  }
  return -1;
}
