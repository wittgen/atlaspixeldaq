#ifndef _PixCon_ConvertGlobalStatus_h_
#define _PixCon_ConvertGlobalStatus_h_

#include <VarDefList.h>
#include <ScanMetaDataCatalogue.h>
#include <Common.hh>

namespace PixCon {

  class ConvertGlobalStatus
  {
  public:

    static MetaData_t::EStatus convertStatus(CAN::GlobalStatus scan_status) {
      if (scan_status<=CAN::kAborted ) {
	return s_metaDataStatus[scan_status];
      }
      else {
	return MetaData_t::kUnknown;
      }
    }

    static CAN::GlobalStatus convertToScanInfoStatus(MetaData_t::EStatus scan_status) {
      if (scan_status<MetaData_t::kNStates ) {
	return s_globalStatus[scan_status];
      }
      else {
	return CAN::kNotSet;
      }
    }

  private:
    static MetaData_t::EStatus s_metaDataStatus[CAN::kAborted+1];
    static CAN::GlobalStatus s_globalStatus[MetaData_t::kUnset+1];
  };

}
#endif
