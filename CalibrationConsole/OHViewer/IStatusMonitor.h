#ifndef _PixCon_IStatusMonitor_H_
#define _PixCon_IStatusMonitor_H_

#include <Flag.h>
#include <CalibrationDataTypes.h>

namespace PixCon {

  /** Interface of s status monitor.
   * For example a status monitor monitors IS, forwards changes to
   * the GUI, and notifies the main monitoring loop in case of
   * changes which require some further processing.
   */
  class IStatusMonitor
  {
  public:

    virtual ~IStatusMonitor() {}

    /** Process the accumulated status information of a monitor.
     */
    virtual void process() = 0;

    /** Called if no monitor anounced changes to the main monitoring thread within a given amount of time.
     */
    virtual void timeout(double elapsed_seconds) = 0;

    /** Hand the flag to the status monitor to notify the monitoring thread about status changes.
     */
    virtual void setStatusChangeFlag( Flag &the_flag ) = 0;



  };

}
#endif
