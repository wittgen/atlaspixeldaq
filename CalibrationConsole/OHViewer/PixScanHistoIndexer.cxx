#include "PixScanHistoIndexer.h"
#include <ConfigWrapper/Regex_t.h>
#include <iostream>
#include <cassert>

namespace PixCon {

  PixScanHistoIndexer::PixScanHistoIndexer(std::map< std::string, std::map<std::string, ExtendedHistoInfo_t> >  &module_histo_info_map,
					   std::map< std::string, std::map<std::string, ExtendedHistoInfo_t> >  &rod_histo_info_map,
					   bool verbose)
    : m_verbose(verbose)
    //      : m_histoNameList(histo_name_list)
  {
    m_histoInfoMap[kModule]=&module_histo_info_map;
    m_histoInfoMap[kRod]=&rod_histo_info_map;
    m_modulePattern.push_back(new PixA::Regex_t("L[012]_B[0-9][0-9]_S[12]_[AC][67]_M[1-6][AC]"));
    m_modulePattern.push_back(new PixA::Regex_t("L[012]_B[0-9][0-9]_S[12]_[AC][7]_M0"));
    m_modulePattern.push_back(new PixA::Regex_t("LX_B[0-9][0-9]_S[12]_[AC][67]_M[1-6][AC]"));
    m_modulePattern.push_back(new PixA::Regex_t("LX_B[0-9][0-9]_S[12]_[AC][7]_M0"));
    m_modulePattern.push_back(new PixA::Regex_t("LI_S[0-9][0-9]_[AC]_M[1-4]_[AC][1-8]"));
    m_modulePattern.push_back(new PixA::Regex_t("LI_S[0-9][0-9]_[AC]_[13][24]_M[1-4]_[AC]([1-9]|1[0-2])"));
    m_modulePattern.push_back(new PixA::Regex_t("D[123][AC]_B[0-9][0-9]_S[12]_M[1-6]"));
  }

  PixScanHistoIndexer::~PixScanHistoIndexer() {
    for (std::list<PixA::Regex_t *>::iterator pattern_iter = m_modulePattern.begin();
	 pattern_iter != m_modulePattern.end();
	 pattern_iter++) {
      delete *pattern_iter;
      *pattern_iter = NULL;
    }
    m_modulePattern.clear();
  }

  void PixScanHistoIndexer::add( const std::string &histogram_name )
  {
    std::string::size_type pos = 0;
    std::string::size_type last_pos = 0;

    //m_verbose = true;

    // serial number folder
    if ( (pos = histogram_name.find("/",last_pos+1) )==std::string::npos  || pos+2 >= histogram_name.size() ) {
      std::cout << "ERROR [PixScanHistoIndexr::add] Histogram name does not start with the serial number folder: " << histogram_name << "." << std::endl;
      return;
    }
    last_pos = pos;
    if (isdigit(histogram_name[pos+2]) && (histogram_name[pos+1]=='A' || histogram_name[pos+1]=='S')) {
      // is serial number
      if ( (pos = histogram_name.find("/",last_pos+1) )==std::string::npos  || pos+2 >= histogram_name.size() ) {
	std::cout << "ERROR [PixScanHistoIndexr::add] Histogram name does not seem to contain a ROD name: " << histogram_name << "." << std::endl;
	return;
      }
      last_pos = pos;
    }

    // ROD name
    std::string rod_name;
    if ( (pos = histogram_name.find("/",last_pos+1) )!=std::string::npos  && pos+1 < histogram_name.size() ) {
      rod_name=std::string(histogram_name, last_pos+1, pos-last_pos-1);
    }
    else {
      std::cout << "ERROR [PixScanHistoIndexr::add] Histogram name does not have a ROD folder: " << histogram_name << "." << std::endl;
      return;
    }
    last_pos = pos;
    std::string::size_type conn_end_pos=pos;

    std::string conn_name;
    bool is_module=false;
    if ( (pos = histogram_name.find("/",last_pos+1) )!=std::string::npos  && pos+1 < histogram_name.size() ) {
      conn_name=std::string(histogram_name, last_pos+1, pos-last_pos-1);

      unsigned int counter_i=0; // for debugging only
      for (std::list<PixA::Regex_t *>::iterator pattern_iter = m_modulePattern.begin();
	   pattern_iter != m_modulePattern.end();
	   pattern_iter++, counter_i++) {
	if ((*pattern_iter)->match(conn_name)) {
	  if (m_verbose) {
	    std::cout << "INFO [PixScanHistoIndexr::add] connectivity name "  << conn_name
		      << " matches pattern " << counter_i << std::endl;
	  }
	  is_module=true;
	  break;
	}
      }

    }
    last_pos=pos;

    // Determine connectivity type of the histogram : module or ROD
    std::map< std::string, std::map<std::string, ExtendedHistoInfo_t> >  *histo_map=NULL;
    if (is_module) {
      histo_map = m_histoInfoMap[kModule];
      conn_end_pos = pos;
    }
    else {
      // is a ROD histogram
      histo_map = m_histoInfoMap[kRod];
      if (m_verbose) {
	std::cout << "INFO [PixScanHistoIndexr::add] Histogram name does not have a module_name." 
		  << histogram_name << "." << std::endl;
      }
      conn_name = rod_name;
    }

    bool is_pix_scan_histo=false;
    std::string::size_type index_start_pos = std::string::npos;

    // search the beginning of the pix scan histo folders which have the form ("/(A[0-9]+/(B[0-9]+/(C[0-9]+/)))[0-9]+
    while ( (pos = histogram_name.find("/",last_pos+1) )!=std::string::npos  && pos+1 < histogram_name.size() ) {
      if (histogram_name[last_pos+1]=='A' || isdigit(histogram_name[last_pos+1])) {
	std::string::size_type digit_pos = last_pos+2;
	index_start_pos = last_pos+1;
	while (digit_pos < pos && isdigit(histogram_name[digit_pos])) {
	  digit_pos++;
	}
	if (digit_pos==pos) {
	  is_pix_scan_histo=true;
	  break;
	}
      }
      last_pos = pos;
    }

    if (!is_pix_scan_histo || pos == std::string::npos) {
      std::cout << "ERROR [PixScanHistoIndexr::add] Histogram name does not finish with a pix scan histo or pix histo name: " 
		<< histogram_name << "." << std::endl;
      return;
    }

    pos++;

    std::string::size_type end_pos=histogram_name.find(":",pos);
    if (end_pos == std::string::npos) {
      std::cout << "ERROR [PixScanHistoReceiver::add] No end marker which separates the original histogram name." << std::endl;
      return;
    }


    assert(histo_map);

    std::string histogram_type_name(histogram_name, conn_end_pos+1, last_pos-conn_end_pos-1);
    if (m_verbose) {
      std::cout <<  "INFO [PixScanHistoIndexr::add] Histogram type name = " << histogram_type_name << " ." << std::endl;
    }
    std::map<std::string, std::map<std::string, ExtendedHistoInfo_t> >::iterator histo_type_iter = histo_map->find(histogram_type_name);
    if (histo_type_iter == histo_map->end() ) {
      std::pair< std::map<std::string, std::map<std::string, ExtendedHistoInfo_t> >::iterator, bool> 
	ret = histo_map->insert( std::make_pair(histogram_type_name, std::map<std::string, ExtendedHistoInfo_t>() ) );
      if (!ret.second) {
	std::cout <<  "ERROR [PixScanHistoIndexr::add] Failed to add new histogram type  = " << histogram_type_name << " ." << std::endl;
	return;
      }
      histo_type_iter = ret.first;
    }

    //       if (!is_pix_scan_histo) {
    // 	std::cout << "ERROR [PixScanHistoIndexr::add] Histogram name does not look like a pix scan histo: " << histogram_name << "." << std::endl;
    // 	return;
    //       }

    //      PixA::Index_t index;

    std::map<std::string, ExtendedHistoInfo_t>::iterator info_iter = histo_type_iter->second.find(conn_name);
    if (info_iter == histo_type_iter->second.end()) {
      std::pair< std::map<std::string, ExtendedHistoInfo_t>::iterator, bool> 
	ret = histo_type_iter->second.insert(std::make_pair(conn_name, ExtendedHistoInfo_t()));
      if (!ret.second) {
	std::cout <<  "ERROR [PixScanHistoIndexr::add] Failed to add new info structor for connectivity object  " << conn_name << " ." << std::endl;
	return;
      }
      info_iter = ret.first;
      info_iter->second.reset();
    }

    ExtendedHistoInfo_t &histo_info = info_iter->second;

    if (rod_name != conn_name) {
      histo_info.setParentConnName(rod_name);
    }

    bool reached_end=false;

    const char *ptr_a = &(histogram_name[index_start_pos]);
    const char *ptr_end=&(histogram_name[end_pos]);

    // the levels are separated by '/'
    char sep='/';
    char level_marker='A';
    unsigned int level_counter_i=0;
    while (ptr_a < ptr_end) {

      // Although not excluded, up to now there havn't been any pix scan histos with more than 3 levels.
      if (level_counter_i>4) {
	std::cout << "ERROR [PixScanHistoReceiver::add] Too many indices." << std::endl;
	break;
      }

      // Each pix scan histo index starts with a capital letter from A to C.
      if (*ptr_a == level_marker) {
	level_marker++;
	ptr_a++;
      }
      else {
	reached_end=true;
	// the original histogram name is separated by ':'
	sep=':';
      }
      const char *ptr_b=ptr_a;

      // I think strtol does not modify the contents ptr_a and ptr_b is pointing to.
      // So, the const_cast should be fine.
      int an_index = strtol(const_cast<char *>(ptr_a), const_cast<char **>( &ptr_b),10 );

      if (ptr_b-ptr_a==0 || ptr_b > ptr_end || *ptr_b!=sep) {
	std::cout << "ERROR [PixScanHistoReceiver::add] Position of next index indicator out of range or incorrect separator." << std::endl;
	return;
      }
      if (an_index<0) {
	std::cout << "ERROR [PixScanHistoReceiver::add] Invalid index." << std::endl;
	return;
      }
      if (*ptr_b==sep) ptr_b++;
      ptr_a=ptr_b;

      if (reached_end) {
	if ( histo_info.minHistoIndex()>=histo_info.maxHistoIndex()) {
	  histo_info.setMinHistoIndex(an_index);
	  histo_info.setMaxHistoIndex(an_index+1);
	}
	else {
	  if ( static_cast<unsigned int>(an_index) < histo_info.minHistoIndex() || histo_info.minHistoIndex()>=histo_info.maxHistoIndex()) {
	    histo_info.setMinHistoIndex(an_index);
	  }
	  else if ( static_cast<unsigned int>(an_index) >=  histo_info.maxHistoIndex()) {
	    histo_info.setMaxHistoIndex(an_index+1);
	  }
	}
      }
      else if (static_cast<unsigned int>(an_index)>=histo_info.maxIndex(level_counter_i)) {
	histo_info.setMaxIndex(level_counter_i,an_index+1);
      }
      level_counter_i++;
      //	index.push_back( static_cast<unsigned int>(an_index) );
    }

    if (!reached_end) {
      std::cout << "ERROR [PixScanHistoReceiver::add] Last index is a pix scan histogram index not a histogram index : " << histogram_name << std::endl;
      return;
    }
    if (m_verbose) {
      std::cout << "INFO [PixScanHistoReceiver::add] " << conn_name << "histo info  = ";
      for (unsigned int level_i=0; level_i<3; level_i++) {
	std::cout << histo_info.maxIndex(level_i) << " ";
      }
      std::cout << histo_info.minHistoIndex() << "-" << histo_info.maxHistoIndex() << std::endl;
    }
  }


}
