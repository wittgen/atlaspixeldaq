// Dear emacs, this is -*-c++-*-
#ifndef _PixCon_OHHistogramProvider_h_
#define _PixCon_OHHistogramProvider_h_

#include <OHHistogramProviderBase.h>
#include <string>
#include <map>
#include <set>

#include <Lock.h>

class IPCPartition;

namespace PixCon {

  /** Basic interface for the histogram server.
   */
  class OHHistogramProvider : public OHHistogramProviderBase
  {
  public:
    OHHistogramProvider(IPCPartition &oh_partition, const std::string &oh_server_name, bool verbose)
      : m_partition(&oh_partition),m_ohServerName(oh_server_name), m_verbose(verbose) {}


  protected:
    /** Request the names of all histograms assigned to the given serial number and connectivity object type.
     * @param type the type of the measurement : scan, analysis or current.
     * @param serial_number the serial number of the scan.
     * @param conn_item_type the type of the connectivity item for which the histogram list should be requested.
     * @param rod_name the rod name or an empty string for which the histogram list is requested. 
     * Note, it is the responsibility of the caller to clear the given histogram name list before the call if the old content is not desired.
     * This allows to add histograms of different types in subsequent calls.
     * @sa EConnItemType
     */
    void updateHistogramList(EMeasurementType type,
			     SerialNumber_t scan_serial_number,
			     EConnItemType /*conn_item_type*/,
			     const std::string &rod_name );

    /** Get the histogram from the server.
     * @param full_histo_name the full histogram path name on the OH server
     * @return pointer of the histogram or NULL.
     * The owner gets the ownership over the histogram and is responsible of 
     * deleting the histogram.
     */
    HistoPtr_t histogram(const std::string &full_histo_name, const std::vector<unsigned int> &index);
  private:
    IPCPartition *m_partition;
    const std::string m_ohServerName;
    bool m_verbose;
  };

}

#endif
