// Dear emacs, this is -*-c++-*-
#ifndef _PixCon_PixHScanistoIndexer_h_
#define _PixCon_PixHScanistoIndexer_h_

#include "ExtendedHistoInfo_t.h"
#include <DataContainer/HistoInfo_t.h>

#include <map>
#include <list>
#include <vector>

namespace PixA {
  class Regex_t;
}

namespace PixCon {

  class PixScanHistoIndexer
  {
  public:
    PixScanHistoIndexer( std::map< std::string, std::map<std::string, ExtendedHistoInfo_t> >  &module_histo_info_map,
			 std::map< std::string, std::map<std::string, ExtendedHistoInfo_t> >  &rod_histo_info_map,
			 bool verbose);

    ~PixScanHistoIndexer();

    void add( const std::string &header, const std::string &histogram_name ) {
      add(header+histogram_name);
    }

    void add( const std::string &histogram_name );


  private:

    //    std::vector< std::string> *m_histoNameList;
    enum EConnItemTypes {kModule, kRod, kNConnItemTypes};
    std::map< std::string, std::map<std::string, ExtendedHistoInfo_t> >  *m_histoInfoMap[kNConnItemTypes];
    std::list<PixA::Regex_t *> m_modulePattern;
    bool m_verbose;
  };
}
#endif
