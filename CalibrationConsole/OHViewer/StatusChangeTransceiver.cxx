#include "StatusChangeTransceiver.h"

namespace PixCon {

  StatusChangeTransceiver::StatusChangeTransceiver(const std::shared_ptr<CalibrationDataManager> &calibration_data, IStatusChangeForwarder &status_change_forwarder)
    : m_calibrationData(calibration_data),
      m_statusChangeForwarder(&status_change_forwarder)
  {
    init();
  }

  StatusChangeTransceiver::StatusChangeTransceiver(const std::shared_ptr<CalibrationDataManager> &calibration_data)
    : m_calibrationData(calibration_data),
      m_statusChangeForwarder(new NopForwarder)
  {
    init();
  }

  void StatusChangeTransceiver::init()
  {
    assert( m_statusChangeForwarder.get() );
    m_terminated.reserve(kNMeasurements);
    for (unsigned int measurement_i=0; measurement_i<kNMeasurements; measurement_i++) {
      m_terminated.push_back(new Condition( &(m_mutex[measurement_i])));
    }
  }

  StatusChangeTransceiver::~StatusChangeTransceiver()
  { 
    for (std::vector<Condition *>::iterator cond_iter = m_terminated.begin();
	 cond_iter != m_terminated.end();
	 ) {
      std::vector<Condition *>::iterator current_cond = cond_iter++;
      delete *current_cond;
      *current_cond =NULL;
    }
  }


}
