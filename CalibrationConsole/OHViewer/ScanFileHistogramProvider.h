#ifndef _PixCon_ScanFileHistogramProvider_h_
#define _PixCon_ScanFileHistogramProvider_h_

#include "RootFileHistogramProvider.h"

namespace PixLib {
  class ScanInfo_t;
}

namespace PixCon {

  class ScanFileHistogramProvider : public RootFileHistogramProvider
  {
  public:
    ScanFileHistogramProvider(std::shared_ptr<CalibrationDataManager> &calibration_data)
      : RootFileHistogramProvider(calibration_data)
    {}

    void addScan(EMeasurementType measurement, SerialNumber_t serial_number, PixLib::ScanInfo_t &scan_info);
  };

}

#endif
