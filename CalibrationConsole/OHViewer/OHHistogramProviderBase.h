// Dear emacs, this is -*-c++-*-
#ifndef _PixCon_OHHistogramProviderBase_h_
#define _PixCon_OHHistogramProviderBase_h_

#include <IHistogramProvider.h>
#include "ExtendedHistoInfo_t.h"
#include <string>
#include <map>
#include <set>
#include <stdexcept>
#include <cassert>

#include <Lock.h>

class IPCPartition;

namespace PixCon {

  /** Basic interface for the histogram server.
   */
  class OHHistogramProviderBase : public IHistogramProvider
  {
  public:
    //    OHHistogramProviderBase()  {}

     /** Load a histogram from the server.
      * @param type the type of the measurement : scan, analysis or current.
      * @parem serial_number the serial number of the scan.
      * @param conn_item_type the connectivity item type of the histogram.
      * @param connectivity_name connectivity name of the module.
      * @param histo_name name of the histogram.
      * @param index vector which contains the indices for the pix scan histo.
      */
     HistoPtr_t loadHistogram(EMeasurementType type,
 			     SerialNumber_t serial_number,
 			     EConnItemType conn_item_type,
 			     const std::string &connectivity_name,
 			     const std::string &histo_name,
 			     const Index_t &index);

    /** Request the names of all histograms assigned to the given serial number and connectivity object type.
     * @param type the type of the measurement : scan, analysis or current.
     * @param serial_number the serial number of the scan.
     * @param conn_item_type the type of the connectivity item for which the histogram list should be requested.
     * @param histogram_name_list vector which will be extended by the histogram names associated to the serial number and object type.
     * Note, it is the responsibility of the caller to clear the given histogram name list before the call if the old content is not desired.
     * This allows to add histograms of different types in subsequent calls.
     * @sa EConnItemType
     */
    void requestHistogramList(EMeasurementType type, 
			      SerialNumber_t serial_number,
			      EConnItemType conn_item_type,
			      std::vector< std::string > &histogram_name_list ) {
      requestHistogramList(type,serial_number, conn_item_type, histogram_name_list, "");
    }

    /** Request the names of all histograms assigned to the given serial number and connectivity object type.
     * @param type the type of the measurement : scan, analysis or current.
     * @param serial_number the serial number of the scan.
     * @param conn_item_type the type of the connectivity item for which the histogram list should be requested.
     * @param histogram_name_list vector which will be extended by the histogram names associated to the serial number and object type.
     * @param rod_name the rod name or an empty string for which the histogram list is requested. 
     * Note, it is the responsibility of the caller to clear the given histogram name list before the call if the old content is not desired.
     * This allows to add histograms of different types in subsequent calls.
     * @sa EConnItemType
     */
    void requestHistogramList(EMeasurementType type,
			      SerialNumber_t serial_number,
			      EConnItemType conn_item_type,
			      std::vector< std::string > &histogram_name_list,
			      const std::string &rod_name );


    /** Return the dimensions of the pix scan histo levels.
     * @param type the type of the measurement : scan, analysis or current.
     * @parem serial_number the serial number of the scan.
     * @param conn_item_type the connectivity item type of the histogram.
     * @param connectivity_name connectivity name of the module.
     * @param histo_name name of the histogram.
     * @param index vector which will be filled with the dimensions of the pix scan histo levels.
     */
    void numberOfHistograms(EMeasurementType type,
			    SerialNumber_t serial_number,
			    EConnItemType conn_item_type,
			    const std::string &connectivity_name,
			    const std::string &histo_name,
			    HistoInfo_t &index) {
      index = extendedHistoInfo(type,serial_number,conn_item_type, connectivity_name, histo_name);
    }

    void clearCache(EMeasurementType type,
		    SerialNumber_t serial_number) {
      if (type != kScan && type != kAnalysis) return;

      Lock lock(m_histoMutex);
      std::map<FullKey_t, HistoData_t>::iterator serial_iter = m_histoInfoMap.find(FullKey_t(type,serial_number));
      if (serial_iter == m_histoInfoMap.end()) return;
      m_histoInfoMap.erase(serial_iter);
    }

    void reset() ;

  protected:
    /** Request the names of all histograms assigned to the given serial number and connectivity object type.
     * @param type the type of the measurement : scan, analysis or current.
     * @param serial_number the serial number of the scan.
     * @param conn_item_type the type of the connectivity item for which the histogram list should be requested.
     * @param rod_name the rod name or an empty string for which the histogram list is requested. 
     * Note, it is the responsibility of the caller to clear the given histogram name list before the call if the old content is not desired.
     * This allows to add histograms of different types in subsequent calls.
     * @sa EConnItemType
     */
    virtual void updateHistogramList(EMeasurementType type,
				     SerialNumber_t scan_serial_number,
				     EConnItemType /*conn_item_type*/,
				     const std::string &rod_name ) = 0;


    /** Get the histogram from the server.
     * @param full_histo_name the full histogram path name on the OH server.
     * @param index an array which describes the position in the pix scan histo tree.
     * @return pointer of the histogram or NULL.
     * The owner gets the ownership over the histogram and is responsible of 
     * deleting the histogram.
     */
    virtual HistoPtr_t histogram(const std::string &full_histo_name, const std::vector<unsigned int> &index) = 0;

  public:


    void needToUpdate(EMeasurementType type, SerialNumber_t serial_number, const std::string &rod_name) {
      if (type != kScan && type != kAnalysis) return;

      Lock lock(m_histoMutex);
      std::map<FullKey_t, HistoData_t>::iterator serial_iter = m_histoInfoMap.find(FullKey_t(type,serial_number));
      if (serial_iter == m_histoInfoMap.end()) return;
      serial_iter->second.needToUpdate(rod_name);
    }

    void setUptodate(EMeasurementType type, SerialNumber_t serial_number) {
      if (type != kScan && type != kAnalysis) return;

      Lock lock(m_histoMutex);
      m_histoInfoMap[FullKey_t(type,serial_number)].setUptodate();
    }

  private:
    Mutex         m_histoMutex;

  protected:

    enum EHistoConnItemTypes {kModule, kRod, kNConnItemTypes};


    const ExtendedHistoInfo_t &extendedHistoInfo(EMeasurementType type,
						 SerialNumber_t serial_number,
						 EConnItemType conn_item_type,
						 const std::string &connectivity_name,
						 const std::string &histo_name);

    class HistoData_t {
    public:
      HistoData_t() : m_needToUpdateAll(true) {}

      std::map< std::string, std::map<std::string, ExtendedHistoInfo_t> > &operator[](unsigned int index) 
        { assert(index<kNConnItemTypes); return m_histoList[index];}

      const std::map< std::string, std::map<std::string, ExtendedHistoInfo_t> > &operator[](unsigned int index) const 
        { assert(index<kNConnItemTypes); return m_histoList[index];}

//       std::map< std::string, std::map<std::string, HistoInfo_t> >::const_iterator begin() const { return m_histoList.begin(); }
//       std::map< std::string, std::map<std::string, HistoInfo_t> >::iterator begin()             { return m_histoList.begin(); }
//       std::map< std::string, std::map<std::string, HistoInfo_t> >::const_iterator end() const   { return m_histoList.end(); }
//       //      std::map< std::string, std::map<std::string, HistoInfo_t> >::size_type size() const       { return m_histoList.size(); }
//       bool empty() const       { return m_histoList.empty(); }

//       std::map< std::string, std::map<std::string, HistoInfo_t> >::const_iterator find(const std::string &type_name) const   { return m_histoList.find(name); }
//       std::map< std::string, std::map<std::string, HistoInfo_t> >::iterator find(const std::string &type_name)               { return m_histoList.find(name); }

      bool isUptodate(const std::string &rod_name) const
      {
	if (m_needToUpdateAll) return false;

	if (rod_name.empty()) {
	  if (m_needUpdateRodList.empty()) {
	    return true;
	  }
	  else {
	    return false;
	  }
	}
	else {
	  if (m_needUpdateRodList.find(rod_name) == m_needUpdateRodList.end()) {
	    return true;
	  }
	  else {
	    return false;
	  }
	}
      }

      void setUptodate() {
	m_needToUpdateAll=false;
      }

      void setUptodate(const std::string &rod_name) {
	if (rod_name.empty()) {
	  m_needUpdateRodList.clear();
	  m_needToUpdateAll=false;
	}
	else {
	  std::set<std::string>::iterator rod_iter = m_needUpdateRodList.find(rod_name);
	  if (rod_iter != m_needUpdateRodList.end()) m_needUpdateRodList.erase(rod_iter);
	}
      }

      void needToUpdate(const std::string &rod_name) {
	if (rod_name.empty()) {
	  m_needToUpdateAll = true;
	}
	else {
	  m_needUpdateRodList.insert(rod_name);
	}
      }

    protected:
      bool m_needToUpdateAll; 
      std::set<std::string> m_needUpdateRodList;
      std::map< std::string, std::map<std::string, ExtendedHistoInfo_t> > m_histoList[kNConnItemTypes];
    };


    EHistoConnItemTypes histoConnType(EConnItemType conn_item_type) {
      EHistoConnItemTypes histo_conn_type = kNConnItemTypes;
      switch (conn_item_type) {
      case PixCon::kModuleItem:
	histo_conn_type = kModule;
	break;
      case PixCon::kRodItem:
	histo_conn_type = kRod;
	break;
      default:
	break;
      }
      return histo_conn_type;
    }

    void setUptodate(EMeasurementType type, SerialNumber_t serial_number, const std::string &rod_name) {
      if (type != kScan && type != kAnalysis) return;

      std::map<FullKey_t, HistoData_t>::iterator serial_iter = m_histoInfoMap.find(FullKey_t(type,serial_number));
      if (serial_iter == m_histoInfoMap.end()) return;
      serial_iter->second.setUptodate(rod_name);
    }

    const HistoData_t &histoData(EMeasurementType type, SerialNumber_t serial_number) const {
      std::map<FullKey_t, HistoData_t >::const_iterator scan_iter = m_histoInfoMap.find(FullKey_t(type,serial_number));
      if (scan_iter == m_histoInfoMap.end() ) {
	throw std::runtime_error("No scan data for scan/analysis.");
      }
      return scan_iter->second;
    }

    HistoData_t &histoData(EMeasurementType type, SerialNumber_t serial_number) {
      return m_histoInfoMap[FullKey_t(type,serial_number)];
    }

    Mutex &histoMutex() { return m_histoMutex; }

  private:
    std::map<FullKey_t, HistoData_t >  m_histoInfoMap;

    static const std::string s_emptyString;
    static const ExtendedHistoInfo_t s_dummyHistoInfo;
  };

}

#endif
