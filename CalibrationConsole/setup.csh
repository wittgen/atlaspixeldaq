#!/bin/csh
set this=`pwd`

if (-d ${this}/CalibrationData ) then
  setenv LD_LIBRARY_PATH $this/lib:$PIX_ANA/ConfigWrapper:$PIX_ANA/Visualiser:$LD_LIBRARY_PATH ;
else
  echo "ERROR [setup.sh] You have to enter the directory of Karl the CalibrationConsole source code." >&2 
  false ;
endif

setenv KARL_DATA_PATH ${this}/share
