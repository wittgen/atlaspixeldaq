#ifndef _PP0ZoomView_H_
#define _PP0ZoomView_H_

#include "ZoomView.h"
#include <qrect.h>
#include <cassert>

#include <ConfigWrapper/Connectivity.h>

//namespace PixLib {
//   class PixConnectivity;
//}

namespace PixDet {

  class PP0ZoomView : public PixDet::ZoomView
  {
  public:
    PP0ZoomView(DetectorCanvasView *canvas_view, const QRect &rect, const std::shared_ptr<PixDet::ILogoFactory> &logo_factory)
      : PixDet::ZoomView(canvas_view,rect, logo_factory), m_x0(rect.x()), m_y0(rect.y()), m_width(rect.width()), m_height(rect.height()) {}
    ~PP0ZoomView();

    int x0() const { return m_x0; }
    int y0() const { return m_y0; }

    int width() const                 { return m_width; }
    int height() const                { return m_height; }

    int marginLeft() const            { return s_marginLeftRight; }
    int marginRight() const           { return s_marginLeftRight; }

    int marginTop() const             { return s_marginTopBottom; }
    int marginBottom() const          { return s_marginTopBottom; }

    int pp0ColumnSeparation() const   { return s_pp0ColumnSeparation ;}
    int pp0RowSeparation() const      { return s_pp0RowSeparation; }

    float rodFraction() const         { return s_rodFraction; }

    int connectionSepY() const        { return s_connectionSepY; }

    int connectionSepX() const        { return s_connectionSepX; }

    int connectionPluginSepX() const  { return s_connectionPluginSepX; }

    void drawPp0sOfRod(PixCon::IConnItemFactory &factory,
		       DetectorCanvasView *canvas_view,
		       PixA::Pp0List::const_iterator pp0_begin,
		       PixA::Pp0List::const_iterator pp0_end);

    unsigned int nModuleSlotsOnPp0(const std::string &pp0_name) {
      assert ( !pp0_name.empty() );
      if (pp0_name[0]=='D') return 6;
      if (pp0_name[1]=='I') {
	if (pp0_name.substr(3,3).compare("S15") == 0) return 12;//DBM with 12 modules
	if (pp0_name.substr(3,3).compare("S20") == 0) return 6;//DBM SR1 with 6 modules
	return 8;//IBL with 8 modules
      }
      return atoi(&(pp0_name[pp0_name.size()-1]));
    }
    
    static unsigned int extractPp0Slot(const std::string &conn_name);

  protected:

  private:
    int m_x0;
    int m_y0;
    int m_width;
    int m_height;

    static const int s_marginLeftRight = 10;
    static const int s_marginTopBottom = 10;
    static const int s_pp0ColumnSeparation = 20;
    static const int s_pp0RowSeparation = 30;
    static const float s_rodFraction /* moved to impl file = .4*/;
    static const int s_connectionSepY = 3;
    static const int s_connectionSepX = 5;
    static const int s_connectionPluginSepX = 5;
  };

}

//namespace PixLib {
//   enum CtrlType       { DUMMY, ROD, IBLROD, USBPIX, RCE, CPPROD };/
//}

#endif
