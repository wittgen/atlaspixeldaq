#ifndef _PixCon_DiskModItem_h_
#define _PixCon_DiskModItem_h_

#include <QGraphicsScene>
#include <QGraphicsPolygonItem>
#include <QPolygon>
#include <string>
#include "ConnItem.h"
#include <iostream>

namespace PixCon {

  class IItemStyle;

  class DiskModItem : virtual public ConnItem, public QGraphicsPolygonItem
  {
  public:
    DiskModItem(const std::string &name, IItemStyle *style, const  QPolygon &pa)
      : ConnItem(name,style,kModuleItem), QGraphicsPolygonItem() { setPolygon(pa); update();}

    QGraphicsItem *canvasItem()  { return this; }

    bool updateStyle();

  private:
    void drawShape ( QPainter & p );
    QPen m_pen;
  };

}
#endif
