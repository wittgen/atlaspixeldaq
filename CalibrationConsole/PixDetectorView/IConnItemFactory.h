#ifndef _PixA_IConnItemFactory_H_
#define _PixA_IConnItemFactory_H_

#include <string>
#include <QGraphicsScene>
#include <QPolygon>

class QRectF;

namespace PixCon {

  class ConnItem;

  class IConnItemFactory
  {
  public:
    virtual ~IConnItemFactory() {}

    virtual ConnItem *rodItem(const std::string &connectivity_name,
                  const QRectF &rect) = 0;

    virtual ConnItem *timItem(const std::string &connectivity_name,
                  const QRectF &rect) = 0;

    virtual ConnItem *pp0Item(const std::string &connectivity_name,
                  const QRectF &rect) = 0;

    virtual ConnItem *diskModuleItem(const std::string &module_connectivity_name,
				     unsigned int layer,
                     const  QPolygon &pa) = 0;

    virtual ConnItem *barrelModuleItem(const std::string &module_connectivity_name,
				       unsigned int layer,
                       const QRectF &rect) = 0;
  };

}
#endif
