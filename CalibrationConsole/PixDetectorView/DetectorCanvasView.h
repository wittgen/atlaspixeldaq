#ifndef _DetectorCanvasView_h_
#define _DetectorCanvasView_h_

#include <QGraphicsView>
#include <QMouseEvent>
#include <vector>

#include "InsetView.h"

#include "ConnItem.h"
#include "ItemList.h"

namespace PixDet {
  class Detector;
}

class QMouseEvent;
class PaletteAxisItem;

struct drawDetectorSections {
    bool crates;
    bool disks;
    bool barrel;
    bool IBL;
    bool DBM;
    bool PP0s;
    bool legend;
};

class DetectorCanvasView : public QGraphicsView
{
  Q_OBJECT
public:
  typedef std::multimap<std::string, PixCon::ConnItem *>   ConnItemList_t;
  typedef std::vector<PixCon::ConnItem *>                  RodList_t;
  typedef std::map<std::string, RodList_t >                CrateList_t;
  typedef std::multimap<std::string, PixCon::ConnItem * >  ModuleList_t;

  DetectorCanvasView(QWidget *parent, const char *name);
  ~DetectorCanvasView();

  void clear();

  const PixCon::ConnItem *findExtraItem(const std::string &conn_name) const {
    ConnItemList_t::const_iterator iter = m_extraItems.find(conn_name);
    if (iter != m_extraItems.end()) return iter->second;
    else return NULL;
  }

  PixCon::ConnItem *findExtraItem(const std::string &conn_name) {
    return const_cast<PixCon::ConnItem *>(const_cast<const DetectorCanvasView *>(this)->findExtraItem(conn_name));
  }

  CrateList_t::const_iterator beginCrate() const   { return m_rods.begin(); }
  CrateList_t::const_iterator endCrate() const     { return m_rods.end(); }

  CrateList_t::iterator beginCrate()               { return m_rods.begin(); }
  CrateList_t::iterator endCrate()                 { return m_rods.end(); }

  const PixCon::ConnItem *findRod(const std::string &rod_name) const;
  PixCon::ConnItem *findRod(const std::string &rod_name)
  { return const_cast<PixCon::ConnItem *>(const_cast<const DetectorCanvasView *>(this)->findRod(rod_name)); }

  ModuleList_t::const_iterator beginModule() const { return m_modules.begin(); }
  ModuleList_t::const_iterator endModule() const   { return m_modules.end(); }

  ModuleList_t::iterator beginModule()             { return m_modules.begin(); }
  ModuleList_t::iterator endModule()               { return m_modules.end(); }

  ModuleList_t::iterator findModule(const std::string &mod_name)             { return m_modules.lower_bound(mod_name); }
  ModuleList_t::const_iterator findModule(const std::string &mod_name) const { return m_modules.lower_bound(mod_name); }

  ConnItemList_t::const_iterator beginExtraItem() const {
    return m_extraItems.begin();
  }

  ConnItemList_t::iterator beginExtraItem() {
    return m_extraItems.begin();
  }

  ConnItemList_t::const_iterator endExtraItem() const {
    return m_extraItems.end();
  }
  ConnItemList_t::iterator endExtraItem() {
    return m_extraItems.end();
  }

  void addExtraItem(PixCon::ConnItem *extra_item) {
    m_extraItems.insert(std::make_pair(extra_item->name(),extra_item));
  }

  void addModule(PixCon::ConnItem   *module_item) {
    m_modules.insert(std::make_pair(module_item->name(), module_item));
  }

  std::vector<PixCon::ConnItem *> &newCrate(const std::string &crate_name) {
    std::vector<PixCon::ConnItem *> &crate = m_rods[crate_name];
    crate.clear();
    return crate;
  }

  void removeExtraItem(PixCon::ConnItem *extra_item) {
    ConnItemList_t::iterator extra_item_iter=m_extraItems.lower_bound(extra_item->name());
    bool reacehed_end = (extra_item_iter == m_extraItems.end());
    while (!reacehed_end) {

      ConnItemList_t::iterator  current_extra_item_iter = extra_item_iter++;
      reacehed_end = (extra_item_iter == m_extraItems.end()) || (extra_item_iter->second->name() != extra_item->name());

      if (extra_item == current_extra_item_iter->second) {
	  m_extraItems.erase(current_extra_item_iter);
      }
    }
  }

  void removeModule(PixCon::ConnItem   *module_item) {
    ModuleList_t::iterator module_iter=m_modules.lower_bound(module_item->name());
    bool reached_end = (module_iter == m_modules.end() );
    while (!reached_end) {

      ModuleList_t::iterator current_module_iter=module_iter++;
      reached_end = (module_iter == m_modules.end() ) || (module_iter->second->name() != module_item->name());

      if (module_item == current_module_iter->second) {
	m_modules.erase(current_module_iter);
      }
    }
  }

  void registerInsetItem(QGraphicsItem *item) {
    m_zoomView.registerInsetItem(this,item);
  }

  void registerInsetItem(PixCon::ConnItem *item) {
    m_zoomView.registerInsetItem(this,item);
  }

  void clearInset() {
    m_zoomView.clearInset(this);
  }

  ConnItemList_t::iterator beginInsetConnItem() {
    return m_zoomView.beginConnItem();
  }


  const PixCon::ConnItem *findInsetConnItem(const std::string &connectivity_name) const {
    return m_zoomView.findConnItem(connectivity_name);
  }

  PixCon::ConnItem *getInsetConnItem(const std::string &connectivity_name)  {
    return m_zoomView.getConnItem(connectivity_name);
  }

  ConnItemList_t::const_iterator beginInsetConnItem() const {
    return m_zoomView.beginConnItem();
  }

  ConnItemList_t::const_iterator endInsetConnItem() const {
    return m_zoomView.endConnItem();
  }

  void removeFromSelection(PixCon::ConnItem *item);

  bool isNewZoom(const std::string &conn_name) {
    return conn_name != m_zoomConnName;
  }

  void setZoomConnName(const std::string &conn_name) {
    m_zoomConnName = conn_name;
  }

  PixCon::ItemList &paletteItemList() {return m_paletteItemList;}
  const PixCon::ItemList &paletteItemList() const {return m_paletteItemList;}

  QGraphicsTextItem *title(const std::string &title);
  QGraphicsTextItem *subTitle(const std::string &sub_title);

  void selectConnItem(PixCon::EConnItemType type, const std::string &conn_name);

protected:
  void showAxisRangeDialog();

signals:
  void itemSelected(const QString &, const QRectF &);
  void mouseOver(const QString &, const QRectF &);
  void mouseOverAxisItem(PaletteAxisItem *, const QRectF &rect);
  void axisRightClick();

  void primaryAction(PixCon::EConnItemType type, const std::string &conn_name);
  void secondaryAction(PixCon::EConnItemType type, const std::string &conn_name);
  void showMenu(PixCon::EConnItemType type, const std::string &conn_name);

private:

  void mousePressEvent(QMouseEvent *e);
  void mouseMoveEvent(QMouseEvent *e);
  void contentsMouseEvent(QMouseEvent *e);

  CrateList_t    m_rods;
  ModuleList_t   m_modules;
  ConnItemList_t m_extraItems;
  QGraphicsTextItem   *m_title;
  QGraphicsTextItem   *m_subTitle;
  
  std::string    m_zoomConnName;
  InsetView      m_zoomView;

  PixCon::ItemList    m_paletteItemList;

  std::vector<PixCon::ConnItem *> m_selectedItem;


  //  QCanvasItem *m_lastItem;
};

#endif
