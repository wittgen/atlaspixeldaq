// Dear emacs, this is -*-c++-*-
#ifndef _InsetView_h_
#define _InsetView_h_

#include <string>
#include <map>
#include <vector>

namespace PixCon {
  class ConnItem;
}

class QGraphicsItem;
class DetectorCanvasView;

class InsetView {
 public:
  typedef std::map<std::string, PixCon::ConnItem *> ConnItemList_t;
  InsetView() {  }

  void registerInsetItem(DetectorCanvasView *, QGraphicsItem *item) {
    m_insetItems.push_back(item);
  }

  void registerInsetItem(DetectorCanvasView *canvas_view, PixCon::ConnItem *item);

  void clearInset(DetectorCanvasView *canvas_view);

  ConnItemList_t::const_iterator beginConnItem() const {
    return m_insetConnItems.begin();
  }

  ConnItemList_t::iterator beginConnItem() {
    return m_insetConnItems.begin();
  }

  ConnItemList_t::const_iterator endConnItem() const {
    return m_insetConnItems.end();
  }

  ConnItemList_t::iterator endConnItem() {
    return m_insetConnItems.end();
  }

  const PixCon::ConnItem *findConnItem(const std::string &conn_name) const {
    ConnItemList_t::const_iterator conn_iter = m_insetConnItems.find(conn_name);
    if (conn_iter != m_insetConnItems.end()) {
      return conn_iter->second;
    }
    return NULL;
  }

  PixCon::ConnItem *getConnItem(const std::string &conn_name) {
    const PixCon::ConnItem *item = findConnItem(conn_name);
    return const_cast<PixCon::ConnItem *>(item);
  }

 private:
  std::vector<QGraphicsItem *>           m_insetItems;
  ConnItemList_t                       m_insetConnItems;
};

#endif
