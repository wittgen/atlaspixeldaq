#include "DetectorCanvasView.h"


//#include <iostream>

DetectorCanvasView::DetectorCanvasView(QWidget *parent, const char *name) 
  : QGraphicsView(new QGraphicsScene(0,0,830,2100), parent),
    m_title(0),
    m_subTitle(0)
    //    ,m_lastItem(NULL)
{
  viewport()->setMouseTracking(true);
  viewport()->update();
}

void DetectorCanvasView::clear()
{
  m_selectedItem.clear();
  m_zoomView.clearInset(this);
  m_paletteItemList.clear();

  // clear canvas;
  auto items = scene()->items();
  for (auto i: items) {
    delete i;
  }
  items.clear();
  m_modules.clear();
  m_rods.clear();
  m_extraItems.clear();
  m_subTitle=NULL;
  m_title=NULL;
}

DetectorCanvasView::~DetectorCanvasView()
{
  clear();
  delete scene();
}


void DetectorCanvasView::mousePressEvent(QMouseEvent* e)
{
  contentsMouseEvent(e);
}
void DetectorCanvasView::mouseMoveEvent(QMouseEvent *e)
{
  contentsMouseEvent(e);
}

void DetectorCanvasView::contentsMouseEvent(QMouseEvent *e)
{
    // find all items that mouse can reach
    QGraphicsItem * p;
    p = itemAt(e->pos());
    PixCon::ConnItem *conn_item=dynamic_cast<PixCon::ConnItem *>(p);
    // check if connected
    if (conn_item) {
        if (e->button() == Qt::LeftButton) {
            if (e->type() == QEvent::MouseButtonDblClick) {
                emit primaryAction(conn_item->type(),conn_item->name());
            }
            else {
                e->accept();
            if (m_selectedItem.size()==0 || (*(m_selectedItem.begin()))->name() != conn_item->name() ) {
                QRectF bounding_rect(conn_item->canvasItem()->sceneBoundingRect());
                selectConnItem(conn_item->type(), conn_item->name());
                QRectF w_rect = matrix().mapRect(bounding_rect);
                emit itemSelected(conn_item->name().c_str(), w_rect);
            }
                else {
                QRectF w_rect = matrix().mapRect(conn_item->canvasItem()->sceneBoundingRect());
                emit mouseOver(conn_item->name().c_str(),w_rect);
            }
            }
        }
        else if (e->button() == Qt::RightButton) {
            emit showMenu(conn_item->type(),conn_item->name());
        }
        else if (e->button() == Qt::MidButton) {
            emit secondaryAction(conn_item->type(),conn_item->name());
        }
        else if (e->button() == Qt::NoButton) {
            QRectF w_rect = matrix().mapRect(conn_item->canvasItem()->sceneBoundingRect());
            emit mouseOver(conn_item->name().c_str(), w_rect);
        }
    }
    else {
        //Canvas is now right clickable anywhere and will open correct
        //menu. No mouseover, just too different to get this working well.
       // PaletteAxisItem * axis_item = dynamic_cast<PaletteAxisItem *>(p);
       // if (axis_item) {
            if (e->button() == Qt::RightButton) {
                if (e->type() == QEvent::MouseButtonPress) {
                    emit axisRightClick();
                    return;
                }
             }
      //      else if (e->button() == Qt::NoButton) {
      //          QRectF w_rect = matrix().mapRect(axis_item->sceneBoundingRect());
      //          emit mouseOverAxisItem(axis_item, w_rect);
      //        }
      //   }
      //      else if (e->button() == Qt::RightButton && e->type() == QEvent::MouseButtonPress) {
      //          LegendItem *legend_item=dynamic_cast<LegendItem  *>(p);
      //          if (legend_item) {
      //             std::cout<<"INFO [CLICK] found legend item"<< std::endl;
      //             emit axisRightClick();
      //          return;
      //        }
      //  }
    }
}


const PixCon::ConnItem *DetectorCanvasView::findRod(const std::string &rod_name) const
{ 
  //@todo crate names do not follow connectivity naming schema
  std::string crate_name("Crate ");

  std::string::size_type pos=rod_name.find("_",5);
  if (rod_name.compare(0,5,"ROD_C")==0) {
    // old naming convention e.g. still used for toothpix
    crate_name+=std::string(rod_name,5,pos-5);
  }
  else if (rod_name.compare(0,4,"ROD_")==0 && (rod_name.c_str()[4]=='L' || rod_name.c_str()[4]=='B' || rod_name.c_str()[4]=='D' || rod_name.c_str()[4]=='I') && isdigit(rod_name.c_str()[5])) {
    // ROD_L1_S12 - ROD_L4_S12, ROD_B1_S12 to ROD_B3_S12, ROD_D1_S12 to ROD_D2_S12
    crate_name += rod_name.substr(4,pos-4);
  }
  else {
    std::cout << "INFO [DetectorCanvasView::findRod] Unexpected ROD name : " << rod_name << std::endl;
  }

  CrateList_t::const_iterator iter = m_rods.find(crate_name);
  if (iter == m_rods.end()) return NULL;
  unsigned int slot_nr=atoi(&(rod_name[pos+2]));
  if (slot_nr<5) {
    std::cout << "WARNING [DetectorCanvasView::findRod] Slot of ROD is smaller than 5 : " << slot_nr << "." << std::endl;
    return NULL;
  }
  slot_nr-=5;
  if (slot_nr >= iter->second.size()) {
    std::cout << "WARNING [DetectorCanvasView::findRod] Slot of ROD is larger than number of slots : " << slot_nr+5 << " >= "
	      << iter->second.size()+5 << "." << std::endl;
    return NULL;
  }
  else return iter->second[slot_nr];
}

void DetectorCanvasView::removeFromSelection(PixCon::ConnItem *item) {
  std::vector<PixCon::ConnItem *>::iterator iter = std::find(m_selectedItem.begin(),m_selectedItem.end(),item);
  if (iter != m_selectedItem.end()) {
    m_selectedItem.erase(iter);
  }
}

QGraphicsTextItem *DetectorCanvasView::title(const std::string &title)
{
  if (!m_title) {
    m_title = new QGraphicsTextItem(title.c_str());
    scene() ->addItem(m_title);
  }
  else {
    m_title->setPlainText(title.c_str());
  }
  return m_title;
}

QGraphicsTextItem *DetectorCanvasView::subTitle(const std::string &sub_title)
{
  if (!m_subTitle) {
    m_subTitle = new QGraphicsTextItem(sub_title.c_str());
    scene()->addItem(m_subTitle);
  }
  else {
    m_subTitle->setPlainText(sub_title.c_str());
  }
  return m_subTitle;
}

void DetectorCanvasView::selectConnItem(PixCon::EConnItemType type, const std::string &conn_name)
{
 // QGraphicsItem *deselect_canvas_item=NULL;
  for (std::vector<PixCon::ConnItem *>::const_iterator iter=m_selectedItem.begin();
       iter != m_selectedItem.end();
       iter++) {
    (*iter)->deselect();
    //deselect_canvas_item=
            (void)(*iter)->canvasItem();
  }
  m_selectedItem.clear();

  PixCon::ConnItem *conn_item=NULL;
  if (type == PixCon::kModuleItem) {
    ModuleList_t::iterator module_iter=findModule(conn_name);
    if(module_iter !=endModule() ) {
      conn_item = module_iter->second;
    }
  }
  else if (type == PixCon::kRodItem) {
    conn_item=findRod(conn_name);
  }

  if (conn_item) {
    m_selectedItem.push_back(conn_item);
  }
  // to select and deselect cannot take const item
  conn_item = const_cast<PixCon::ConnItem *>(findInsetConnItem(conn_name));
  if (conn_item) {
    m_selectedItem.push_back(conn_item);
  }

  for (std::vector<PixCon::ConnItem *>::const_iterator iter=m_selectedItem.begin();
       iter != m_selectedItem.end();
       iter++) {

    //QRectF bounding_rect((*iter)->canvasItem()->sceneBoundingRect());
    (*iter)->select();
  }
  scene()->update();

}
