#ifndef _DetectorCanvasTip_H_
#define _DetectorCanvasTip_H_

#include <QRect>
#include <QString>
class QWidget;

namespace PixCon {

  class DetectorCanvasTip
  {
  public:
    DetectorCanvasTip(QWidget *parent) {
      m_parent = parent;
    }

    void setCurrentItem(const QRectF &rect, const QString &message) {
      m_message = message;
      m_rect = rect.toRect();
    }

  public:
    void maybeTip(const QPoint &point);

    QString m_message;
    QRect   m_rect;
    QWidget *m_parent;
  };

}
#endif
