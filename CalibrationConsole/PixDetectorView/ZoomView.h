// Dear emacs, this is -*-c++-*-
// .. and thanks for this tip
#ifndef _PixCon_ZoomView_h_
#define _PixCon_ZoomView_h_

class QRectF;
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <vector>
#include <map>
#include <utility>

#include <qfont.h>
#include <ConnItem.h>

#include <ILogoFactory.h>
#include <memory>

class DetectorCanvasView;

namespace PixCon {
  class IConnItemFactory;
}

namespace PixDet {


  class ZoomView
  {
  protected:
    ZoomView(DetectorCanvasView *canvas_view,
         const  QRectF &rec,
	     const std::shared_ptr<ILogoFactory> &logo_factory);

   ~ZoomView();

   void drawLogo(DetectorCanvasView *canvas_view, const QRectF &pos);

   void drawPP0(PixCon::IConnItemFactory &factory,
		DetectorCanvasView *canvas_view,
		const std::string &conn_name,
		unsigned int n_mods,
		int xpos0,
		int ypos0,
		int width,
		int height,
		std::vector< std::pair<int,int> > &jacks,
		bool label_above,
		bool big_label);

    void addSector(PixCon::IConnItemFactory &factory,
		   DetectorCanvasView *canvas_view,
		   const std::string &conn_name,
		   int xpos0,
		   int ypos0,
		   int width,
		   int height,
		   std::vector< std::pair<int,int> > &jacks);

    void addBarrel(PixCon::IConnItemFactory &factory,
		   DetectorCanvasView *canvas_view,
		   const std::string &conn_name,
		   unsigned int n_mods,
		   int xpos0,
		   int ypos0,
		   int width,
		   int height,
		   std::vector< std::pair<int,int> > &jacks,
		   bool label_above,
		   bool big_label);

    void addRod(PixCon::IConnItemFactory &factory,
		DetectorCanvasView *canvas_view,
		const std::string &conn_name,
		int mask,
		int xpos0,
		int ypos0,
		int width,
		int height,
		bool small_rod_label,
		std::vector< std::pair<int,int> > &jacks,
		std::string pp0_name,
		std::string ctrlType);

    static QColor &txColor() { return s_txColor; }
    const QFont &defaultFont() const {return m_defaultFont; }
  public:
    static unsigned int extractLayerNumber(const std::string &);

  private:
    QFont                                m_defaultFont;
    static QColor                        s_txColor;

    std::shared_ptr<ILogoFactory> m_logoFactory;
  };

}
#endif
