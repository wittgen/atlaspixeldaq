#include "BarrelModItem.h"
#include <qpainter.h>

namespace PixCon {

  bool BarrelModItem::updateStyle()
  {
    bool ret=false;
    std::pair<QBrush , QPen > style = brushAndPen();

    if (style.first != brush() ) {
      setBrush(style.first);
      ret=true;
    }
    if (style.second != pen() )  {
      setPen(style.second);
      ret=true;
    }
    return ret;
  }


}

