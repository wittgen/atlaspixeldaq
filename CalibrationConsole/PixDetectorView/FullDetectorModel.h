#ifndef _FullDetectorModel_H_
#define _FullDetectorModel_H_

#include "Detector.h"
#include "DetectorCanvasView.h"

namespace PixDet {

  class FullDetectorModel : public Detector
  {
  public:
    FullDetectorModel(PixCon::IConnItemFactory &factory,
		      const std::shared_ptr<ILogoFactory> &logo_factory,
		      DetectorCanvasView *canvas_view,
              bool old_crate_naming_convention,
              drawDetectorSections const & partsToDraw);

    ~FullDetectorModel() = default;

    void showPP0Zoom(PixCon::IConnItemFactory &factory,
		     DetectorCanvasView *canvas_view,
		     const PixA::Pp0List::const_iterator &pp0_begin,
		     const PixA::Pp0List::const_iterator &pp0_end)  ;

    void updateTitle(const std::string &title, const std::string &sub_title);

  private:
    float m_zoomSep;
    int m_diskHeight = 165;
    int m_barrelHeight = 400; //was 350
    int m_IBLDBMHeight = 20;
    int m_iblHeight = 80;

  };

}
#endif
