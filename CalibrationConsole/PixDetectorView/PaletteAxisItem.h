// Dear emacs, this is -*-c++-*-
#ifndef _PaletteAxisItem_h_
#define _PaletteAxisItem_h_

#include <qcolor.h>
#include <qpoint.h>
#include <qrect.h>
#include <QGraphicsScene>
#include <QGraphicsRectItem>

class PaletteAxisItem : public QGraphicsRectItem
{
public:
  PaletteAxisItem(const QRectF &rect, int start, int  end, float min, float max, int n_steps)
    : QGraphicsRectItem(rect),
      m_min(min),
      m_max(max),
      m_step((max-min)/n_steps),
      m_scale( static_cast<float>(n_steps) / (end-start)),
      m_nSteps(n_steps),
      m_start(start),
      m_end(end)
  {}

  void clear();

  bool isOverflow(const QPoint &point) const {
    return point.y()<m_start;
  }

  bool isUnderflow(const QPoint &point) const {
    return point.y()>m_end;
  }

  float step() const {
    return m_step;
  }

//  float lowerEdge(const QPoint &point) const {
//    if (width()<height()) {
//      // vertical
//      if (point.y()<m_start) {
//	return m_max;
//      }
//      if (point.y()>m_end) {
//	return m_min;
//      }
//      int index = static_cast<int>((m_end - point.y() ) * m_scale );
//      float edge = m_min + index * m_step;
//      return (edge<= m_max ? edge : m_max );
//    }
//    else {
//      // horizontal : @todo
//    }
//    return 0.;
//  }

private:
  float        m_min;
  float        m_max;
  float        m_step;
  float        m_scale;
  unsigned int m_nSteps;
  int          m_start;
  int          m_end;
};

#endif
