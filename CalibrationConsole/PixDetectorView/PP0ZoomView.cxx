#include "PP0ZoomView.h"
#include <PixConnectivity/RosConnectivity.h>
#include <ConfigWrapper/ConnectivityUtil.h>
#include "DetectorCanvasView.h"

#include "PixDbInterfaceFactory/PixDbInterfaceFactory.h"
//#include <PixConnectivity/PixConnectivity.h>

namespace PixDet {

  const float PP0ZoomView::s_rodFraction = .4;

  PP0ZoomView::~PP0ZoomView() {}

  void PP0ZoomView::drawPp0sOfRod(PixCon::IConnItemFactory &factory,
				  DetectorCanvasView *canvas_view,
				  PixA::Pp0List::const_iterator pp0_begin,
				  PixA::Pp0List::const_iterator pp0_end)
  {
      QGraphicsScene *canvas = canvas_view->scene();
      canvas_view->clearInset();

      {
	QRect bound_rect(x0(),y0(),width(),height());
        canvas_view->updateSceneRect(bound_rect);
      }

      int available_width=width()-marginRight()-marginLeft();
      int rod_space_width=static_cast<int>( available_width*rodFraction() );


      int a_x0=x0()+marginLeft();

      // determine y-positions of PP0 connection lines

      unsigned int pp0_counter=0;
      unsigned int plugin_mask=0;
      unsigned int n_connections=0;
      unsigned int plugin_index[4][3];
      for (PixA::Pp0List::const_iterator pp0_iter=pp0_begin;
	   pp0_iter != pp0_end;
	   ++pp0_iter, ++pp0_counter) {

	unsigned int tx_plugin      = plugin_index[pp0_counter][0] = PixA::txPlugin( pp0_iter );
	unsigned int rx_plugin_dto1 = plugin_index[pp0_counter][1] = PixA::rxPlugin( pp0_iter, PixA::DTO1 );
	unsigned int rx_plugin_dto2 = plugin_index[pp0_counter][2] = PixA::rxPlugin( pp0_iter, PixA::DTO2 );

	if (tx_plugin<4) {
	  plugin_mask |= (1<<tx_plugin);
	  n_connections++;
	}

	if (rx_plugin_dto1<4) {
	  plugin_mask |= (1<<(rx_plugin_dto1+4));
	  n_connections++;
	}

	if (rx_plugin_dto2<4) {
	  plugin_mask |= (1<<(rx_plugin_dto2+4));
	  n_connections++;
	}

      }

      unsigned int n_pp0s=pp0_counter;

      if (n_pp0s==0) return;
      assert ( n_pp0s <= 4);

      int pp0_width;
      int pp0_height;
      float connection_sep_x_scale;

      QFont link_font( defaultFont() );
      link_font.setPointSize(8); // @todo scale font-size
      QRectF link_text_size;

      if (n_pp0s>1) {
	pp0_width= static_cast<int>( ( available_width  - rod_space_width  - 2 * pp0ColumnSeparation() ) *.5);
	pp0_height=( height() - marginTop()  - marginBottom() - 2 * pp0RowSeparation() ) / 2;
	connection_sep_x_scale=1.;
      }
      else {

    {
        QGraphicsTextItem *link_test_text = new QGraphicsTextItem(QString::fromStdString("8"));
        link_test_text->setFont(link_font);
        link_text_size=link_test_text->boundingRect();
    }

    pp0_width= static_cast<int>( (available_width  - rod_space_width   ) * .8);
    pp0_height=static_cast<int>(( height() - marginTop()  - marginBottom() -  link_text_size.height() * 3.5 ) *.6);
    connection_sep_x_scale = 2.;
      }


      int connection_lines_y[8];
      int connection_sep=0;
      int connection_lines_y_offset=0;
      
      //Rod name as ROD_[crate]_[rod]
      const PixLib::RodBocConnectivity *rod_boc=PixA::rodBoc( *pp0_begin );
      
      std::string rodboc_name = PixA::connectivityName(rod_boc);
      
      //std::cout << "Here ROD name: " << rodboc_name << std::endl; 
       
      
      //Connectivity name with layer and side
      std::string pp0_name;
      for (PixA::Pp0List::const_iterator pp0_iter=pp0_begin;
	   pp0_iter != pp0_end;
	   ++pp0_iter, ++pp0_counter) {
      	pp0_name = PixA::connectivityName( pp0_iter );
      	//std::cout << "a pp0 iteration : " << pp0_name << std::endl;
      }      
      
      //New part to read CtrlType (needed to change also RodBocConnectivity.h)      
      std::string ctrlType = rod_boc->rodBocType();
      
      //std::cout << "Here type test: " << ctrlType << std::endl;
      
      std::vector< std::pair<int, int> > plugin_position;
      addRod(factory,
	     canvas_view,
	     PixA::connectivityName(rod_boc), //const_cast<PixLib::Pp0Connectivity *>(*pp0_begin)->ob()->rodBocName()
	     plugin_mask,
	     x0()+width()-marginRight()-rod_space_width,
	     y0()+marginTop(),
	     rod_space_width,
	     height()-marginTop()-marginBottom(),
	     false,
	     plugin_position,
	     pp0_name,
	     ctrlType);

      // Robin connection
      if (plugin_position.size()==9) {
	const PixLib::RobinConnectivity *robin( PixA::robinConn(rod_boc) );
	
	if (robin) {
      QGraphicsLineItem *line = new QGraphicsLineItem;
      canvas->addItem(line);
	  int line_end =  static_cast<int>(plugin_position.back().first-connectionSepX()*11.5);
	  line->setPen(QPen(QColor(130,130,130),2));

	  std::string ros_name;
	  ros_name = PixA::connectivityName(robin);

      QGraphicsTextItem *robin_label = canvas->addText(QString::fromStdString(ros_name));
	  robin_label->setFont(link_font);
      QRectF label_size=robin_label->boundingRect();
	  if (line_end-label_size.width() - connectionSepX() < marginLeft() + pp0_width*((n_pp0s+1)/2) && ros_name.size()>21) {
	    ros_name=ros_name.substr(0,8)+" ... "+ros_name.substr(ros_name.size()-8,8);
	    delete robin_label;
        robin_label = canvas->addText(QString::fromStdString(ros_name));
	    robin_label->setFont(link_font);
	    label_size=robin_label->boundingRect();
	  }

	  const PixLib::RosConnectivity *ros=PixA::rosConn(robin);
	  int full_line_end = line_end;
	  if (ros) {
	    ros_name = PixA::connectivityName(ros);
        QGraphicsTextItem *ros_label = canvas->addText(QString::fromStdString(ros_name));
	    ros_label->setFont(link_font);
        QRectF ros_label_size=ros_label->boundingRect();
	    if (ros_label_size.width()>label_size.width()) {
	      label_size = ros_label_size;
	    }
	    else {
	      full_line_end -= label_size.width();
	      full_line_end += ros_label_size.width();
	    }
	    ros_label->setY(plugin_position.back().second-label_size.height()+2);
	    int xpos = line_end-label_size.width() - connectionSepX();
	    ros_label->setX(xpos);
        ros_label->setDefaultTextColor(QColor(90,90,90));
	    ros_label->show();
	    canvas_view->registerInsetItem(ros_label);
	  }
	  else {
	    full_line_end -=label_size.width();
	  }

	  robin_label->setY(plugin_position.back().second-label_size.height()*2+2);
	  int xpos = line_end-label_size.width() - connectionSepX();
	  robin_label->setX(xpos);
      robin_label->setDefaultTextColor(QColor(90,90,90));
	  robin_label->show();
	  canvas_view->registerInsetItem(robin_label);


      canvas->addItem(line);
      line->setLine(plugin_position.back().first, plugin_position.back().second,
              full_line_end, plugin_position.back().second);
	  line->show();
	  canvas_view->registerInsetItem(line);
	}

	plugin_position.pop_back();
      }

      // determine the x-offset wrt. plugin position where connections have to turn upwards 
      // to connect to a plugin
      std::vector<int> connection_plugin_sep_x;
      {
	//	int connection_mean_y=static_cast<int>(pp0_counter==1 ? y0()+height() - connection_sep * (n_connections+0.5*n_pp0s) : y0()+height()*.5);
	int connection_mean_y=static_cast<int>( y0()+height()*.5 );
	unsigned int below=2;
	for (std::vector< std::pair<int, int> >::const_iterator plugin_iter = plugin_position.begin();
	     plugin_iter != plugin_position.end();
	     plugin_iter++) {
	  if (plugin_iter->first > x0()) {
	    if (plugin_iter->second < connection_mean_y) {
	      below++;
	    }
	  }
	}

	int a_connection_sep_x=below*connectionSepX();

	connection_plugin_sep_x.reserve(plugin_position.size());
	bool last_was_below=true;
	for (std::vector< std::pair<int, int> >::const_iterator plugin_iter = plugin_position.begin();
	     plugin_iter != plugin_position.end();
	     plugin_iter++) {
	  // 	  std::cout << "INFO [PP0ZoomView] plugin_pos = " << plugin_iter->first << ", " << plugin_iter->second  
	  // 		    << " ( < " << connection_mean_y << " )" 
	  // 		    << " connection = " << a_connection_sep_x << std::endl; 
	  connection_plugin_sep_x.push_back(a_connection_sep_x);
	  if (plugin_iter->first > x0()) {
	    if (plugin_iter->second < connection_mean_y) {
	      a_connection_sep_x -= connectionSepX();
	    }
	    else {
	      if (last_was_below) {
		a_connection_sep_x += static_cast<int>(2.5*connectionSepX());
		last_was_below=false;
	      }
	      a_connection_sep_x += connectionSepX();
	    }
	  }
	}
      }


      //      int connection_lines_y0=static_cast<int>(y0()+height()*.5 - (n_connections-1)*.5 *connectionSepY()+connectionSepY()*.5);

      pp0_counter=0;
      for (PixA::Pp0List::const_iterator pp0_iter=pp0_begin;
	   pp0_iter != pp0_end;
	   ++pp0_iter, ++pp0_counter) {

	unsigned int tx_plugin      = PixA::txPlugin( pp0_iter );
	unsigned int rx_plugin_dto1 = PixA::rxPlugin( pp0_iter, PixA::DTO1 );
	unsigned int rx_plugin_dto2 = PixA::rxPlugin( pp0_iter, PixA::DTO2 );

// 	std::cout << "INFO [PP0ZoomView::drawPp0sOfRod] " << PixA::connectivityName(pp0_iter) << " TX" << pp0_iter.slot() << "  in = " << tx_plugin << std::endl;
// 	std::cout << "INFO [PP0ZoomView::drawPp0sOfRod] " << PixA::connectivityName(pp0_iter) << " RX" << pp0_iter.slot() << "  dto1 = " << rx_plugin_dto1 << std::endl;
// 	std::cout << "INFO [PP0ZoomView::drawPp0sOfRod] " << PixA::connectivityName(pp0_iter) << " RX" << pp0_iter.slot() << "  dto2 = " << rx_plugin_dto2 << std::endl;

	unsigned int slot_i = tx_plugin /*pp0_iter.slot()*/;
	int pp0_x0=a_x0;
	int pp0_y0;

	bool label_above;
	if (n_pp0s>2) {
	  if (slot_i%2==0) {
	    pp0_x0 += pp0_width + pp0ColumnSeparation();
	  }
	  if (slot_i/2==1) {
	    pp0_y0 = y0()+ height()-marginBottom()-pp0_height;
	    label_above = false;
	  }
	  else {
	    pp0_y0 = y0()+marginTop();
	    label_above = true;
	  }
	}
	else if (n_pp0s==2) {
	  label_above = true;
	  pp0_y0 = y0()+marginTop();
	  unsigned int other_plugin_mask = plugin_mask & (1|2|4|8) & (~(1<<tx_plugin));
	  unsigned int other_plugin=0;

	  if (other_plugin_mask>0) {
	    while (!(other_plugin_mask & 1) ) {
	      other_plugin_mask >>=1;
	      other_plugin++;
	    }
	  }

	  if (other_plugin > tx_plugin) {
	    pp0_y0 = y0()+marginTop();
	    label_above = true;
	  }
	  else {
	    pp0_y0 = y0()+ height()-marginBottom()-pp0_height;
	    label_above = false;
	  }
	}
	else {
	  label_above = true;
	  pp0_y0 = y0()+marginTop();
	  unsigned int other_plugin_mask = plugin_mask & (1|2|4|8) & (~(1<<tx_plugin));
	  unsigned int other_plugin=0;

	  if (other_plugin_mask>0) {
	    while (!(other_plugin_mask & 1) ) {
	      other_plugin_mask >>=1;
	      other_plugin++;
	    }
	  }

	  if (other_plugin > tx_plugin) {
	    pp0_x0 += pp0_width + pp0ColumnSeparation();
	  }

	}



	pp0_name = PixA::connectivityName( pp0_iter );
	std::vector< std::pair< int, int > > jacks;
	drawPP0(factory, canvas_view, pp0_name, nModuleSlotsOnPp0(pp0_name) , pp0_x0, pp0_y0, pp0_width, pp0_height, jacks, label_above, (n_pp0s==1));

	if (pp0_counter==0) {
	  int new_pp0_height=0;
	  for(std::vector< std::pair< int, int > >::const_iterator jack_iter=jacks.begin();
	      jack_iter != jacks.end();
	      ++jack_iter) {
	    if (jack_iter->second>pp0_y0+pp0_height) {
	      new_pp0_height=jack_iter->second-pp0_y0;
	    }
	    else if (jack_iter->second<pp0_y0) {
	      new_pp0_height=pp0_y0+pp0_height-jack_iter->second;
	    }

	  }

	  if (new_pp0_height>pp0_height) {
	    new_pp0_height=pp0_height;
	  }
	  {
	    {
	      int connection_lines_y0;
	      if (n_pp0s>1) {

		int denominator = static_cast<int>((n_connections-1)+.5*(n_pp0s-1)+2);
		connection_sep = static_cast<int>( ( height() - marginTop() - marginBottom() - 2*new_pp0_height ) / denominator );

		if (connection_sep<2) connection_sep=2;
		if (connection_sep>4) connection_sep=4;

		connection_lines_y0=static_cast<int>(y0()+marginTop() + (height()-marginTop()-marginBottom()-connection_sep*(n_connections-1)
								       -  connection_sep * (n_pp0s-1) *.5 ) *.5);
	      }
	      else {

		int available_height= height() - marginTop() - marginBottom();
		connection_sep = static_cast<int>( ( available_height - new_pp0_height - link_text_size.height() * (2.4-.4)) / (n_connections+1) );
		if (connection_sep>10) connection_sep=10;
		connection_lines_y0=static_cast<int>(y0()+height()+marginBottom()-connection_sep * (n_connections+.5));
		connection_lines_y_offset=connection_lines_y0 - static_cast<int>(y0()+(height()-connection_sep * (n_connections+0.5*n_pp0s) ) *.5);
	      }

	      for(unsigned int pp0_i=0; pp0_i<n_pp0s; pp0_i++) {
		for (unsigned int type_i=0; type_i<3; type_i++) {
		  if (plugin_index[pp0_i][type_i]<4) {
		    connection_lines_y[ plugin_index[pp0_i][type_i] + (type_i==0 ? 0 : 4) ] = connection_lines_y0;
		    connection_lines_y0 += connection_sep;
		  }
		}
		connection_lines_y0+= static_cast<int>(connection_sep *.5);
	      }
	    }
	  }

	}


	if (n_pp0s==1 && nModuleSlotsOnPp0(pp0_name)+1<=jacks.size()) {

	  std::string cl_name = PixA::coolingLoopName(	pp0_iter );
	  if (!cl_name.empty()) {

      QGraphicsTextItem *cl_label = canvas->addText(QString::fromStdString(cl_name));
	  cl_label->setFont(link_font);
      QRectF cl_label_size=cl_label->boundingRect();

      QGraphicsLineItem *line = new QGraphicsLineItem;
      canvas->addItem(line);
      int top = y0()+marginTop()+cl_label_size.height();
      int xpos = static_cast<int>(jacks.back().first +connection_sep * connection_sep_x_scale);

      line->setLine( jacks.back().first, jacks.back().second,
               xpos ,  jacks.back().second);
      line->setPen(QPen(QColor(130,130,130),2));
      line->show();
      canvas_view->registerInsetItem(line);

      line = new QGraphicsLineItem;
      canvas->addItem(line);
      line->setLine( xpos, jacks.back().second,
               xpos, top);
      line->setPen(QPen(QColor(130,130,130),2));
      line->show();
      canvas_view->registerInsetItem(line);
	  cl_label->setY(top-cl_label_size.height());
	  cl_label->setX(xpos);
          cl_label->setDefaultTextColor(QColor(90,90,90));
	  cl_label->show();
	  canvas_view->registerInsetItem(cl_label);
	  }
	  jacks.pop_back();
	}


	assert( pp0_counter < plugin_position.size()); // paranoia check

	PixA::ModuleList::const_iterator module_begin = PixA::modulesBegin( pp0_iter );
	PixA::ModuleList::const_iterator module_end = PixA::modulesEnd( pp0_iter );


	assert(tx_plugin < plugin_position.size() );

	bool plugin_connected[3]={false,false,false};
	int min_conn_x[3]={x0()+width(),x0()+width(),x0()+width() };

	//	unsigned int swap = 0;

	for (PixA::ModuleList::const_iterator module_iter = module_begin;
	     module_iter != module_end;
	     ++module_iter) {

	  PixA::ModulePluginConnection tx_connection      = PixA::txConnection(module_iter);
	  PixA::ModulePluginConnection rx_connection_dto1 = PixA::rxConnection(module_iter, PixA::DTO1);
	  PixA::ModulePluginConnection rx_connection_dto2 = PixA::rxConnection(module_iter, PixA::DTO2);

	  // consistency checks
	  //	  assert( tx_connection.plugin() == tx_plugin);

	  //	  unsigned int this_swap=0;
	  //	  if (   (rx_connection_dto1.isValid() && rx_connection_dto1.plugin() != rx_plugin_dto1)
	  //	      || (rx_connection_dto2.isValid() && rx_connection_dto2.plugin() != rx_plugin_dto2)) {
	  // swap ?

	  // 	  std::cout << "INFO [PP0ZoomView::drawPp0sOfRod] swap dto links DTO1: pp0=" << rx_plugin_dto1
	  // 		    << " =?= module=" << rx_connection_dto1.plugin() << ")"
	  // 		    << " / DTO2: pp0=" << rx_plugin_dto2 
	  // 		    << " =?= module" << rx_connection_dto2.plugin() << ""
	  // 		    << std::endl;

	  //	  unsigned int temp = rx_plugin_dto2;
	  //	    rx_plugin_dto2 = rx_plugin_dto1;
	  //	    rx_plugin_dto1 = temp;
	  //this_swap = 2;
	  //}
	  //else {
	  //   if (rx_connection_dto1.isValid() || rx_connection_dto2.isValid()) {
// 	      this_swap = 1;
// 	    }
// 	  }
// 	  if (swap == 0) swap = this_swap;
// 	  else if (swap != this_swap) {
// 	    std::cout << "ERROR [PP0ZoomView::drawPp0sOfRod] Inconsistent swapping of DTO links for module "
// 		      << PixA::connectivityName( module_iter )
// 		      << "."
// 		      << std::endl;
// 	  }


// 	  std::cout << "INFO [PP0ZoomView::drawPp0sOfRod] " << PixA::connectivityName(pp0_iter) << " TX" << "  in = " << tx_plugin << std::endl;
// 	  std::cout << "INFO [PP0ZoomView::drawPp0sOfRod] " << PixA::connectivityName(module_iter) << " TX" << "  in = "
// 		    << tx_connection.plugin()  << std::endl << std::endl;

// 	  std::cout << "INFO [PP0ZoomView::drawPp0sOfRod] " << PixA::connectivityName(pp0_iter) << " RX" << "  dto1 = " << rx_plugin_dto1 << std::endl;
// 	  std::cout << "INFO [PP0ZoomView::drawPp0sOfRod] " << PixA::connectivityName(module_iter) << " RX" << "  dto1 = "
// 		    << rx_connection_dto1.plugin()  << " ch = " << rx_connection_dto1.channel() << std::endl << std::endl;

// 	  std::cout << "INFO [PP0ZoomView::drawPp0sOfRod] " << PixA::connectivityName(pp0_iter) << " RX" << "  dto2 = " << rx_plugin_dto2 << std::endl;
// 	  std::cout << "INFO [PP0ZoomView::drawPp0sOfRod] " << PixA::connectivityName(module_iter) << " RX" << "  dto2 = "
// 		    << rx_connection_dto2.plugin() << " ch = " << rx_connection_dto2.channel()  << std::endl  << std::endl;

	  if ( tx_connection.plugin() != tx_plugin) {
	    std::cerr << "WARNING [PP0ZoomView::drawPp0sOfRod] Inconsistent fibre mapping. Module " << PixA::connectivityName(module_iter) << " is connected to ";
	    char rx_name[4]={'A','B','C','D'};
	    std::cerr << " TX" << ( tx_connection.plugin()<4 ? rx_name[tx_connection.plugin()] : '?' )
		      << " but " << PixA::connectivityName(pp0_iter) << " is connected to TX" << ( tx_plugin<4 ? rx_name[ tx_plugin] : '?' )
		      << std::endl;

	  }

// 	  // there are  broken connectivities
// 	  if (   (rx_connection_dto1.isValid() && rx_connection_dto1.plugin() != rx_plugin_dto1)
// 		 || (rx_connection_dto2.isValid() && rx_connection_dto2.plugin() != rx_plugin_dto2)) {
// 	    char rx_name[4]={'A','B','C','D'};
// 	    if (rx_connection_dto1.isValid() && rx_connection_dto1.plugin() != rx_plugin_dto1) {
// 	      std::cout << "WARNING [PP0ZoomView::drawPp0sOfRod] Inconsistent fibre mapping. Module  " << PixA::connectivityName(module_iter)
// 			<< " connected to DTO1 RX" << ( rx_connection_dto1.plugin()<4 ? rx_name[rx_connection_dto1.plugin()] : '?' )
// 			<< " channel = " << rx_connection_dto1.channel()
// 			<< " but DTO1 of " << PixA::connectivityName(pp0_iter) << " is connected to  RX" << ( rx_plugin_dto1<4 ? rx_name[ rx_plugin_dto1] : '?' )
// 			<< std::endl;
// 	    }
// 	    if (rx_connection_dto2.isValid() && rx_connection_dto2.plugin() != rx_plugin_dto2) {
// 	      std::cout << "WARNING [PP0ZoomView::drawPp0sOfRod] Inconsistent fibre mapping. Module  " << PixA::connectivityName(module_iter) 
// 			<< " connected to DTO2 RX" << ( rx_connection_dto2.plugin()<4 ? rx_name[rx_connection_dto2.plugin()] : '?' )
// 			<< " channel = " << rx_connection_dto2.channel()
// 			<< " but DTO2 of " << PixA::connectivityName(pp0_iter) << " is connected to  RX" << ( rx_plugin_dto2<4 ? rx_name[ rx_plugin_dto2] : '?' )
// 			<< std::endl;
// 	    }

// 	  }
	  unsigned int pp0_slot = extractPp0Slot( PixA::connectivityName( module_iter) );
	  assert( pp0_slot < jacks.size() );
	  //std::cout << "INFO [PP0ZoomView::drawPp0sOfRod] Output pp0 slot : " << pp0_slot << std::endl;
	  std::string test_conn_name = PixA::connectivityName(module_iter);
	  //std::cout << "INFO [PP0ZoomView::drawPp0sOfRod] Module name : " << test_conn_name << std::endl;

	  // connection lines x-position
	  int conn_x[3];
	  conn_x[0] = jacks[pp0_slot].first+static_cast<int>(connectionSepX()*connection_sep_x_scale); // TX
	  conn_x[2] = jacks[pp0_slot].first-static_cast<int>(connectionSepX()*connection_sep_x_scale); // RX DTO2
	  conn_x[1] = static_cast<int>(conn_x[0]+(conn_x[2]-conn_x[0])*.66);                  // RX DTO1

	  bool connected[3]={tx_connection.isValid(),rx_connection_dto1.isValid(),rx_connection_dto2.isValid()};
	  unsigned int plugin_pos_index[3]={tx_connection.plugin(),rx_connection_dto1.plugin()+4,rx_connection_dto2.plugin()+4};
	  unsigned int link_number[3]={tx_connection.channel(),rx_connection_dto1.channel(),rx_connection_dto2.channel()};

	  //std::cout << "INFO [PP0ZoomView::drawPp0sOfRod] connected : " << connected[0] << " " << connected[1] << " " << connected[2] << std::endl;
	  //std::cout << "INFO [PP0ZoomView::drawPp0sOfRod] link_number : " << link_number[0] << " " << link_number[1] << " " << link_number[2] << std::endl;

	  for (unsigned int i=0; i<3; i++) {
	    if (connected[i]) {
          QGraphicsLineItem *line = new QGraphicsLineItem;

          line->setLine(conn_x[i], jacks[pp0_slot].second, conn_x[i], connection_lines_y[plugin_pos_index[i]]);

	      unsigned int j = i;
	      if (i==0 && plugin_pos_index[i]==tx_plugin)             j=0;
	      else if (rx_plugin_dto1<4 && plugin_pos_index[i]==rx_plugin_dto1+4) {
		j=1;
	      }
	      else if (rx_plugin_dto2<4 && plugin_pos_index[i]==rx_plugin_dto2+4) {
		j=2;
	      }
	      else {
		j=i;
		line->setPen(QPen(QColor(255,0,0),2));
	      }
	      if (conn_x[i]<min_conn_x[j]) min_conn_x[j]=conn_x[i];

              canvas->addItem(line);
	      line->show();
	      canvas_view->registerInsetItem(line);
	      plugin_connected[j]=true;

	      if (n_pp0s==1) {
		std::stringstream link_str;
		link_str << link_number[i];
        QGraphicsTextItem *link_text = canvas->addText(QString::fromStdString(link_str.str()));
        //link_text->setFont(link_font);
        link_text->setY(static_cast<int>(jacks[pp0_slot].second+link_text_size.height()*(i>0 ? 1*(i-1)+.4 : .2)));
        link_text->setX(static_cast<int>(conn_x[i]-link_text_size.width()*.5));
        link_text->setZValue(link_text->zValue()+3);

		int inflate=static_cast<int>(link_text_size.width()*.05);
		if (inflate<1) inflate=1;
        QGraphicsRectItem *a_rectangle = new QGraphicsRectItem(QRect(static_cast<int>(link_text->x()-inflate-1),
									   static_cast<int>(link_text->y()-inflate),
									   static_cast<int>(link_text_size.width()+inflate*2+1),
                                       static_cast<int>(link_text_size.height()+inflate*2)));
        a_rectangle->setZValue(link_text->zValue()-1);
        a_rectangle->setBrush(QBrush(QColor(255,255,255),Qt::SolidPattern));
		if (i==0) {
          link_text->setDefaultTextColor(txColor());
		  a_rectangle->setPen(QPen(txColor()));
		}
		else {
          a_rectangle->setPen(QPen(QColor(0,0,0)));
		}
            canvas->addItem(a_rectangle);
        a_rectangle->show();
        canvas_view->registerInsetItem(a_rectangle);

		link_text->show();
		canvas_view->registerInsetItem(link_text);
	      }


	    }
	  }
	}

	unsigned int plugin_pos_index[3]={tx_plugin,rx_plugin_dto1+4,rx_plugin_dto2+4};


	for (unsigned int i=0; i<3; i++) {
	  if (plugin_connected[i]) {
	    assert( plugin_pos_index[i] < plugin_position.size() );

        QList<QPointF> points;

        QPointF p1;
        p1.setX(min_conn_x[i]);
        p1.setY(connection_lines_y[plugin_pos_index[i]]);
        points.append(p1);

	    if (n_pp0s==1) {

            QPointF p2;
            p2.setX(static_cast<int>(x0()+marginLeft()+pp0_width /* *1.05*/ +pp0ColumnSeparation()+connectionSepX()*connection_sep_x_scale*i));
            p2.setY(connection_lines_y[plugin_pos_index[i]]);
            points.append(p2);

            QPointF p3;
            p3.setX(static_cast<int>(x0()+marginLeft()+pp0_width /* *1.05*/ +pp0ColumnSeparation()+connectionSepX()*connection_sep_x_scale*i));
            p3.setY(connection_lines_y[plugin_pos_index[i]]-connection_lines_y_offset);
            points.append(p3);
	    }

//Here differentiate between layer (for this single point)
        //if( pp0_name[1] != 'I' || i == 0) { //for old layers
        if( ctrlType == "IBLROD" || ctrlType == "CPPROD" ) {

             QPointF p4;
             p4.setX(plugin_position[plugin_pos_index[i]].first-connection_plugin_sep_x[plugin_pos_index[i]]-(i==0 ? connectionSepX()/2 :0)+10);
             p4.setY(connection_lines_y[plugin_pos_index[i]]-connection_lines_y_offset);
             points.append(p4);

             QPointF p5;
             p5.setX( plugin_position[plugin_pos_index[i]].first-connection_plugin_sep_x[plugin_pos_index[i]]-(i==0 ? connectionSepX()/2 :0)+10);
             p5.setY(plugin_position[plugin_pos_index[i]].second);
             points.append(p5);

         }
        else {

             QPointF p4;
             p4.setX(plugin_position[plugin_pos_index[i]].first-connection_plugin_sep_x[plugin_pos_index[i]]-(i==0 ? connectionSepX()/2 :0));
             p4.setY(connection_lines_y[plugin_pos_index[i]]-connection_lines_y_offset);
             points.append(p4);

             QPointF p5;
             p5.setX(plugin_position[plugin_pos_index[i]].first-connection_plugin_sep_x[plugin_pos_index[i]]-(i==0 ? connectionSepX()/2 :0));
             p5.setY(plugin_position[plugin_pos_index[i]].second);
             points.append(p5);

        }
             QPointF p6;
             p6.setX(plugin_position[plugin_pos_index[i]].first);
             p6.setY(plugin_position[plugin_pos_index[i]].second);
             points.append(p6);

        for(int j = 0 ; j < points.size()-1; ++j){ // -1 to handle boundary
            QLineF connLine(points[j],points[j+1]);
            QGraphicsLineItem *connLineItem = new QGraphicsLineItem(connLine);
            if(i==0){
                connLineItem->pen().setColor(txColor());
            }
            canvas->addItem(connLineItem);
            canvas_view->registerInsetItem(connLineItem);
        }
	  }
	}

      }
  }


  unsigned int PP0ZoomView::extractPp0Slot(const std::string &conn_name) {
    assert( conn_name.size()>3 );
    switch (conn_name[0]) {
    case 'D':
      assert( isdigit(conn_name[conn_name.size()-1]) );
      assert( conn_name[conn_name.size()-2] == 'M' );
      return atoi(&(conn_name[conn_name.size()-1]));
    case 'L':
      if (conn_name[1] != 'I') { // BARREL
        if (conn_name[conn_name.size()-1]=='0') {
          assert( conn_name[conn_name.size()-2] == 'M' );
          return 0;
        }
        assert( conn_name[conn_name.size()-3] == 'M' );
        return atoi( &(conn_name[conn_name.size()-2]) );
      }
      else { // IBL or DBM
        if (conn_name[4] == '1' && conn_name[5] == '5') { // DBM
          assert( conn_name[conn_name.size()-5] == 'M' || conn_name[conn_name.size()-6] == 'M' );
          if( conn_name[conn_name.size()-5] == 'M' )
            return atoi( &(conn_name[conn_name.size()-1]) );
          else
            return atoi( &(conn_name[conn_name.size()-2]) );
        } if (conn_name[4] == '2' && conn_name[5] == '0') {//DBM SR1
           assert( conn_name[conn_name.size()-5] == 'M' || conn_name[conn_name.size()-6] == 'M' );
           if( conn_name[conn_name.size()-5] == 'M' )
            return atoi( &(conn_name[conn_name.size()-1]) ) - 6;
          else
            return atoi( &(conn_name[conn_name.size()-2]) ) - 6;
        }
        assert( conn_name[conn_name.size()-5] == 'M' );
        return atoi( &(conn_name[conn_name.size()-1]) );
      }
    default:
      assert( conn_name[0]=='D' || conn_name[0]=='L' );
    }
    return static_cast<unsigned int>(-1);
  }
}
