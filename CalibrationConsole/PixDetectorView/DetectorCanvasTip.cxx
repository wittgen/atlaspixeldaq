#include "DetectorCanvasTip.h"
#include <QToolTip>

namespace PixCon {

  void DetectorCanvasTip::maybeTip( const QPoint &pos )
  {

    if (!m_rect.isValid() || !m_rect.contains(pos) ) return;

    QToolTip::showText( pos, m_message, m_parent, m_rect );
  }

}
