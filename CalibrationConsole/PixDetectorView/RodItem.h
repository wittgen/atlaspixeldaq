#ifndef _PixCon_RodItem_h_
#define _PixCon_RodItem_h_

#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <string>
#include "ConnItem.h"

namespace PixCon {

  class IItemStyle;

  class SlotItem : public ConnItem, public QGraphicsRectItem
  {
  public:
  SlotItem(const std::string &name, IItemStyle *style, const QRectF &rect, EConnItemType conn_item_type)
      : ConnItem(name,style,conn_item_type),QGraphicsRectItem(rect) { update(); }

    QGraphicsItem *canvasItem() {return this;}

    bool updateStyle() {
      bool ret=false;
      std::pair<QBrush , QPen > style = brushAndPen();

      if (style.first != brush() ) {
	setBrush(style.first);
	ret=true;
      }

      if (style.second != pen() )  {
	setPen(style.second);
	ret=true;
      }

      return ret;
    }
  };

  class RodItem : public SlotItem
  {
  public:
    RodItem(const std::string &name, IItemStyle *style, const QRectF &rect)
      : SlotItem(name,style,rect, kRodItem) {}
  };

  class TimItem : public SlotItem
  {
  public:
    TimItem(const std::string &name, IItemStyle *style, const QRectF &rect)
      : SlotItem(name,style,rect, kTimItem) {}
  };

}
#endif
