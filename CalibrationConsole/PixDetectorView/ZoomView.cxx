#include "ZoomView.h"

#include <cmath>
#include <sstream>

#include "IConnItemFactory.h"
#include "DetectorCanvasView.h"

namespace PixDet {

  QColor ZoomView::s_txColor(120,120,120);

  ZoomView::ZoomView(DetectorCanvasView *canvas_view, const QRectF &rec, const std::shared_ptr<ILogoFactory> &logo_factory)
    : m_logoFactory(logo_factory)
  {

    //    unsigned int margin=10;
    //    unsigned int title_margin=7;
    drawLogo(canvas_view, rec);
  }

  ZoomView::~ZoomView() { }

  void ZoomView::drawLogo(DetectorCanvasView *canvas_view, const QRectF &pos) {
    if (m_logoFactory.get()) {
      m_logoFactory->drawLogo(canvas_view,pos);
    }
  }


  void ZoomView::drawPP0(PixCon::IConnItemFactory &factory,
			 DetectorCanvasView *canvas_view,
			 const std::string &conn_name,
			 unsigned int n_mods,
			 int xpos0,
			 int ypos0,
			 int width,
			 int height,
			 std::vector< std::pair<int,int> > &jacks,
			 bool label_above,
			 bool big_label) 
  {
    assert ( !conn_name.empty() );
    if (conn_name[0]=='D') {
      // disk
      addBarrel(factory,canvas_view,conn_name, n_mods,xpos0,ypos0,width,height,jacks,label_above, big_label);
    }
    else {
      addBarrel(factory,canvas_view,conn_name, n_mods,xpos0,ypos0,width,height,jacks,label_above, big_label);
    }
  }



  void ZoomView::addSector(PixCon::IConnItemFactory &factory,
			   DetectorCanvasView *canvas_view,
			   const std::string &conn_name,
			   int xpos0,
			   int ypos0,
			   int width,
			   int height,
			   std::vector< std::pair<int,int> > &/* jacks */)
 {
    // margin left/right, sepration between both half disks, sector separtion
    int margin = 10;
    int odd_mod_slide_out = 3;

    //    float inner_layer_radius[4];

    float size= width/4. - margin;
    float radius = size ;

    float  radius_inner = .9 *radius;
    float  radius_outer = radius;

    float  mod_width= radius_outer * M_PI / 4 / 6;

    int centre_x = static_cast<int>(xpos0 + width/2 + margin );
    int centre_y = static_cast<int>(ypos0 + height - size - margin);


    QRectF module_dimensions(static_cast<int>(radius_inner),-static_cast<int>(mod_width*.5),static_cast<int>(radius_outer-radius_inner),static_cast<int>(mod_width));
    QRect module_dimensions_rect=module_dimensions.toRect();

    float x_pos = centre_x;
    float y_pos = centre_y;

    float rot_angle_step = 180/24.;
    float sec_angle0=90 - rot_angle_step*3;

    for (unsigned int pass_i=0; pass_i<2; pass_i++) {
      float rot_angle = sec_angle0 + rot_angle_step *.5;
      for (unsigned int mod_i=0; mod_i<6; mod_i++) {
	if (pass_i == mod_i%2) {
	  QMatrix final;
	  final.translate(x_pos, y_pos);
	  final.scale(1.,.7);
	  final.rotate(rot_angle);
	  if (mod_i%2==1) {
	    final.translate(odd_mod_slide_out,0.);
	  }
	  std::stringstream mod_name;
	  mod_name << conn_name <<  "_M" << mod_i%6+1;
      PixCon::ConnItem *conn_item=factory.diskModuleItem(mod_name.str(),
                                 extractLayerNumber(mod_name.str()),
                                 final.mapToPolygon(module_dimensions_rect));
	  canvas_view->registerInsetItem(conn_item);
      // mod_item->setZ((mod_i%2 ? -1 : 0));
      canvas_view->scene()->addItem(dynamic_cast<QGraphicsRectItem*>(conn_item));
      conn_item->canvasItem()->show();
	  //m_modules[mod_name.str()]=mod_item;

	}
	rot_angle += rot_angle_step;
      }
    }
  }

  void ZoomView::addBarrel(PixCon::IConnItemFactory &factory,
			   DetectorCanvasView *canvas_view,
			   const std::string &conn_name,
			   unsigned int n_mods,
			   int xpos0,
			   int ypos0,
			   int width,
			   int height,
			   std::vector< std::pair<int,int> > &jacks, 
			   bool label_above,
			   bool big_label)
  {
    QGraphicsScene *canvas = canvas_view->scene();

    int margin=0;
    unsigned int n_mods_max=12;//old value 7, now 12 because of DBM
    //unsigned int start_mod=(n_mods==6 ? 1 : 0);
    unsigned int start_mod=(n_mods==7 ? 0 : 1);//start module 1 in case of IBL/DBM modules

    float mod_height_scale_factor = 2/3.;

    width -= 2*margin;
    float mod_sep=width/n_mods_max;//width is scaled automatically
    float mod_width=width *.9 / n_mods_max;
    mod_sep -= mod_width;
    if (mod_sep<2) mod_sep=2;

    std::string::size_type pos=conn_name.rfind("_");
    std::string side;
    if (pos!=std::string::npos && pos<conn_name.size()) {
      pos++;
      if (conn_name[pos]=='A') {
	side='A';
      }
      else if (conn_name[pos]=='C') {
	side='C';
      }
    }

    float mod_height=mod_width * mod_height_scale_factor;
    unsigned int name_sep=static_cast<int>(mod_height*.05);
    if (name_sep<2) name_sep=2;


    jacks.clear();
    jacks.reserve(n_mods_max+1);

    QFont mod_font(m_defaultFont);
    mod_font.setPointSize(8); // @todo scale font-size

    QFont pp0_font(m_defaultFont);
    pp0_font.setPointSize((big_label ? 13 : 10));
    pp0_font.setBold(true);


    QGraphicsTextItem *title = new QGraphicsTextItem(conn_name.c_str());
    title->setFont(pp0_font);
    QRectF title_size=title->boundingRect();
    QRectF mod_title_size;
    {
      std::stringstream mod_name_str;
      mod_name_str << "M" << 0;
      QGraphicsTextItem *mod_title = canvas->addText(mod_name_str.str().c_str());
      mod_title->setFont(mod_font);
      mod_title_size=mod_title->boundingRect();
      delete mod_title;
    }

    float xpos=(n_mods == 7) ? xpos0+margin : xpos0+margin-20;//correction since 8 modules now in case of IBL 
    int mod_title_ypos;
    int title_ypos;
    float ypos;

    if (label_above) {
      title_ypos=ypos0+margin;
      mod_title_ypos=title_ypos+title_size.height();
      ypos=mod_title_ypos+mod_title_size.height()+name_sep;
    }
    else {
      title_ypos=ypos0+height-margin-title_size.height();
      mod_title_ypos=title_ypos-mod_title_size.height();
      ypos=mod_title_ypos-name_sep-mod_height;
    }

    for (unsigned int mod_i=0; mod_i<start_mod; mod_i++) {
      jacks.push_back(std::make_pair(xpos0,ypos0));
      xpos+= mod_width + mod_sep;
    }


    title->setY(title_ypos);
    title->setX(static_cast<int>(xpos));

    {
      // pp0item also should take a layer number
      PixCon::ConnItem *pp0_item=factory.pp0Item(conn_name,
                     title->boundingRect());

      pp0_item->canvasItem()->setY(title_ypos);
      pp0_item->canvasItem()->setX(static_cast<int>(xpos));
      // Make sure the pp0 item is always on top
      pp0_item->canvasItem()->setZValue(100);
      canvas_view->registerInsetItem(pp0_item);

      canvas->addItem(dynamic_cast<QGraphicsRectItem*>(pp0_item));
      pp0_item->canvasItem()->show();
    }


    title->show();
    canvas->addItem(title);
    canvas_view->registerInsetItem(title);

    //for (unsigned int mod_i=start_mod; mod_i<n_mods_max; mod_i++) { //does not work any longer, number of modules independent of starting point 
    unsigned int final_mod=(n_mods==7 ? n_mods : n_mods+1);//start module 1 in case of IBL modules
    for (unsigned int mod_i=start_mod; mod_i<final_mod; mod_i++) {
      std::stringstream mod_name;

      if (conn_name[1] != 'I') {
        mod_name << conn_name << "_M" << mod_i;
	  //std::cout << "Number of modules : " << mod_i << " with name : "  << mod_name.str() << std::endl; 
        if (mod_i>0) mod_name << side;
      }
      else {
	if (conn_name.substr(3,3).compare("S15") == 0) { // DBM
	  int mi = ((mod_i-1)/3)+1;
	  int q1234 = (mi < 3) ? 12 : 34; 
	  char buff[50];
	  snprintf(buff,50,"%s_%d_M%d_%s%d",conn_name.c_str(),q1234,mi,side.c_str(),mod_i);
	  mod_name<<buff;
	} else if (conn_name.substr(3,3).compare("S20") == 0 ) { // DBM SR1
	  int mi = ((mod_i-1)/3)+3;
	  int q1234 = 34;
	  char buff[50];
	  snprintf(buff,50,"%s_%d_M%d_%s%d",conn_name.c_str(),q1234,mi,side.c_str(),mod_i +6);
	  mod_name<<buff;
	}
	else {
	  int half_mod_n = (mod_i+1)/2;//modulo thus directly considered
	  mod_name << conn_name << "_M" << half_mod_n << "_";
	  if (mod_i>0) mod_name << side;
	  mod_name << mod_i;
	}
      }

      //std::cout << "in zoom view module nr : " << mod_i << " with module : " << mod_name.str() << std::endl;

      PixCon::ConnItem *mod_item=factory.barrelModuleItem(mod_name.str(),
							  extractLayerNumber(mod_name.str()),
                              QRectF(static_cast<int>(xpos),
								static_cast<int>(ypos),
								static_cast<int>(mod_width),
                                static_cast<int>(mod_height)));
      canvas_view->registerInsetItem(mod_item);
      canvas->addItem(dynamic_cast<QGraphicsRectItem*>(mod_item));
      mod_item->canvasItem()->show();
      //if (mod_i < 5) mod_item->canvasItem()->show();
      std::stringstream mod_name_str;

      //std::string mName =mod_name.str();
      //Name of individual modules;
      if(mod_name.str().substr(3,3).compare("S20") == 0){ //DBM SR1
      mod_name_str << "C" << mod_i+6;
      } else if (n_mods >= 8) {
	if(side == "A") mod_name_str << "A" << mod_i;
	else if(side == "C") mod_name_str << "C" << mod_i;
      } else mod_name_str << "M" << mod_i;

      QGraphicsTextItem *mod_title = canvas->addText(mod_name_str.str().c_str());
      canvas_view->registerInsetItem(mod_title);

      mod_title->setFont(mod_font);
      mod_title->setY(mod_title_ypos);
      mod_title->setX(static_cast<int>(xpos+mod_width/2-mod_title_size.width()/2));
      mod_title->show();
      
      jacks.push_back(std::make_pair(static_cast<int>(xpos+mod_width/2),static_cast<int>(ypos+(label_above ? mod_height : 0 ) )));
      xpos += mod_width + mod_sep;
    }

    if (big_label) {
      jacks.push_back( std::make_pair(static_cast<int>(xpos-mod_sep), static_cast<int>(ypos+mod_height*.5)) );
    }


  }

  void ZoomView::addRod(PixCon::IConnItemFactory &factory,
			DetectorCanvasView *canvas_view,
			const std::string &conn_name,
			int mask,
			int xpos0,
			int ypos0,
			int width,
			int height,
			bool small_rod_label,
			std::vector< std::pair<int,int> > &jacks,
			std::string pp0_name,
			std::string ctrlType)
  {
    
    QGraphicsScene *canvas = canvas_view->scene();
    int margin =0;
    int title_sep = 3;
    int connection_sep_x = 10;

    QFont rod_font(m_defaultFont);
    rod_font.setPointSize(13);
    rod_font.setBold(true);

    QFont plugin_type_font(m_defaultFont);
    plugin_type_font.setPointSize(10);

    QFont plugin_font(plugin_type_font);
    plugin_font.setPointSize(7);

    // @todo ROD name here:
    {
      QGraphicsTextItem *title = canvas->addText(conn_name.c_str());
      title->setFont(rod_font);
      QRectF title_size=title->boundingRect();

      int ypos = ypos0 + margin;
      title->setY(ypos);
      int xpos = xpos0 + width - margin - title_size.width();
      height -= title_size.height()+ title_sep;
      ypos0 += title_size.height() + title_sep;

      title->setX(xpos);
      if (small_rod_label) {
	rod_font.setPointSize(12);
	title->setFont(rod_font);
      }
      title->show();
      canvas_view->registerInsetItem(title);
    }

    // create TX label for size
    QGraphicsTextItem *plugin_type_label_tx = canvas->addText("TX");
    plugin_type_label_tx->setFont(plugin_type_font);
    QRectF label_size_1=plugin_type_label_tx->boundingRect();

    int rod_width=(width-2*margin-label_size_1.width() - 6 * connection_sep_x );
    if (rod_width>(width-2*margin)/3) rod_width=(width-2*margin)/3;
    if (rod_width>(height-2*margin)/3) rod_width=(height-2*margin)/3;

    // ROD
    PixCon::ConnItem *rod_item=factory.rodItem(conn_name,
                           QRectF(static_cast<int>(xpos0+width-margin-rod_width),
						     static_cast<int>(ypos0),
						     static_cast<int>(rod_width),
                             static_cast<int>(height)));
    canvas_view->registerInsetItem(rod_item);
    canvas->addItem(dynamic_cast<QGraphicsRectItem*>(rod_item));
    rod_item->canvasItem()->show();


//************************************************
//Here new if conditions for different connections
//************************************************
//if new layers (until approx 450)
//if( pp0_name[1] != 'I' ) { //for old layers!!!
if( ctrlType == "IBLROD" || ctrlType == "CPPROD" ) {
  
  // plugins prep

    int plugin_width=rod_width/2;
    int plugin_height=static_cast<int>(height/8.5);
    int x=xpos0+width-margin-rod_width-plugin_width;
    int y=ypos0;
    int y0 = y;

    // plugins

    //std::cout << "a IBL pp0 name : " << pp0_name << std::endl;

    std::string name;

    for (unsigned int plugin_type=0; plugin_type<2; plugin_type++) {
      
      if (plugin_type==0) y+=2.2*plugin_height;
      if (plugin_type==1) y=y0;
      //std::cout << "label y position : " << y << " and " << static_cast<int>(plugin_height*.5) << std::endl;
    
      for (unsigned int plugin_slot=0; plugin_slot<4; plugin_slot++) {
      
        if (plugin_type==1 && plugin_slot==2) y +=4.2*plugin_height+static_cast<int>(plugin_height*.5); 
      
	if (mask & (1<<(plugin_slot+plugin_type*4))) {
      QGraphicsRectItem *plugin_item=new QGraphicsRectItem(QRectF(x,y,plugin_width,plugin_height));
      if (plugin_type==0) {
	    QPen a_pen = plugin_item->pen();
	    a_pen.setColor(txColor());
        plugin_item->setPen( a_pen );
	  }

         canvas_view->scene()->addItem(plugin_item);
	  plugin_item->show();//rectangle around letter
	  canvas_view->registerInsetItem(plugin_item);

	  
	  name = 'A'+static_cast<char>(plugin_slot);
      QGraphicsTextItem *plugin_label = canvas->addText(name.c_str());
      plugin_label->setFont(plugin_font);
      QRectF label_size_2=plugin_label->boundingRect();
	  plugin_label->setX(static_cast<int>(x+(plugin_width-label_size_2.width())*.5));
	  plugin_label->setY(static_cast<int>(y+(plugin_height-label_size_2.height())*.5));
	  if (plugin_type==0) {
        plugin_label->setDefaultTextColor(txColor());
	  }
	  plugin_label->show();//letter for plugin
	  canvas_view->registerInsetItem(plugin_label);
	}
	jacks.push_back(std::make_pair<int,int>((int)x,static_cast<int>(y+plugin_height*.5)));
	y+=plugin_height;
      }
      y+=static_cast<int>(plugin_height*.5);
    }

    // ROL :
    jacks.push_back(std::make_pair<int,int>(xpos0+width-margin-rod_width,static_cast<int>(ypos0+height)-1));




    //set TX and RX labels
    if (name == "C" || name == "D")
    {
    // draw TX label
      int ypos = y0+label_size_1.height()*0.5; 
      plugin_type_label_tx->setY(ypos);
      int xpos = x-label_size_1.width()-6*connection_sep_x;
      plugin_type_label_tx->setX(xpos);
      plugin_type_label_tx->show();
      canvas_view->registerInsetItem(plugin_type_label_tx);
    // RX
      QGraphicsTextItem *plugin_type_label_rx = canvas->addText("RX");
      plugin_type_label_rx->setFont(plugin_type_font);
      QRectF label_size_3=plugin_type_label_rx->boundingRect();
      ypos = y;
      plugin_type_label_rx->setY(ypos-label_size_3.height()*3);
      xpos = x-label_size_3.width() -6*connection_sep_x;
      plugin_type_label_rx->setX(xpos);
      plugin_type_label_rx->show();
      canvas_view->registerInsetItem(plugin_type_label_rx);
    }
    else {
    // draw TX label
      int ypos = y0+label_size_1.height()*0.5;
      plugin_type_label_tx->setY(ypos);
      int xpos = x-label_size_1.width()-6*connection_sep_x;
      plugin_type_label_tx->setX(xpos);
      plugin_type_label_tx->show();
      canvas_view->registerInsetItem(plugin_type_label_tx);
    // RX
      QGraphicsTextItem *plugin_type_label_rx = canvas->addText("RX");
      plugin_type_label_rx->setFont(plugin_type_font);
      QRectF label_size_4=plugin_type_label_rx->boundingRect();
      //ypos = y-label_size.height()*2.0;
      ypos = y;
      plugin_type_label_rx->setY(ypos-label_size_4.height()*3);
      xpos = x-label_size_4.width() -6*connection_sep_x;
      plugin_type_label_rx->setX(xpos);
      plugin_type_label_rx->show();
      canvas_view->registerInsetItem(plugin_type_label_rx);
    
    }
        
    
  }//end first if loop
//if layer is old one  
else {
    
    // plugins

    int plugin_width=rod_width/2;
    int plugin_height=static_cast<int>(height/8.5);
    int x=xpos0+width-margin-rod_width-plugin_width;
    int y=ypos0;


    // ... draw TX label
    {
      int ypos = y-label_size_1.height()*.1;
      plugin_type_label_tx->setY(ypos);
      int xpos = x-label_size_1.width()-6*connection_sep_x;
      plugin_type_label_tx->setX(xpos);
      plugin_type_label_tx->show();
      canvas_view->registerInsetItem(plugin_type_label_tx);
    }

    // plugins

    //std::cout << "a pp0 name : " << pp0_name << std::endl;


    for (unsigned int plugin_type=0; plugin_type<2; plugin_type++) {
      for (unsigned int plugin_slot=0; plugin_slot<4; plugin_slot++) {
	if (mask & (1<<(plugin_slot+plugin_type*4))) {
      QGraphicsRectItem *plugin_item=new QGraphicsRectItem(QRectF(x,y,plugin_width,plugin_height));
	  if (plugin_type==0) {
	    QPen a_pen = plugin_item->pen();
	    a_pen.setColor(txColor());
	    plugin_item->setPen( a_pen );
	  }
         canvas_view->scene()->addItem(plugin_item);
	  plugin_item->show();//rectangle around letter
	  canvas_view->registerInsetItem(plugin_item);

	  std::string name;
	  name = 'A'+static_cast<char>(plugin_slot);
      QGraphicsTextItem *plugin_label = canvas->addText(name.c_str());
      plugin_label->setFont(plugin_font);
      QRectF label_size=plugin_label->boundingRect();
	  plugin_label->setX(static_cast<int>(x+(plugin_width-label_size.width())*.5));
	  plugin_label->setY(static_cast<int>(y+(plugin_height-label_size.height())*.5));
	  if (plugin_type==0) {
            plugin_label->setDefaultTextColor(txColor());
	  }
	  plugin_label->show();//letter for plugin
	  canvas_view->registerInsetItem(plugin_label);
	}
	jacks.push_back(std::make_pair<int,int>((int)x,static_cast<int>(y+plugin_height*.5)));
	y+=plugin_height;
      }
      y+=static_cast<int>(plugin_height*.5);
    }

    // ROL :
    jacks.push_back(std::make_pair<int,int>(xpos0+width-margin-rod_width,static_cast<int>(ypos0+height)-1));

    // RX label
    {
      QGraphicsTextItem *plugin_type_label_rx = canvas->addText("RX");
      plugin_type_label_rx->setFont(plugin_type_font);
      QRectF label_size_5=plugin_type_label_rx->boundingRect();
      int ypos = y;
      plugin_type_label_rx->setY(ypos-label_size_5.height()*3);
      int xpos = x-label_size_5.width() -6*connection_sep_x;
      plugin_type_label_rx->setX(xpos);
      plugin_type_label_rx->show();
      canvas_view->registerInsetItem(plugin_type_label_rx);
    }
    
    }//end layer else
    
  }

  unsigned int ZoomView::extractLayerNumber(const std::string &module_conn_name) {
    if ( module_conn_name.size()<2) return 0;

    switch (module_conn_name[0]) {
    case 'L':
      return atoi(&(module_conn_name[1]));
    case 'D': {
      int layer = atoi(&(module_conn_name[1]));
      assert(layer>0);
      return layer-1;
    }
    default:
      return 0;
    }
  }

}
