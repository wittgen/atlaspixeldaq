#include "FullDetectorModel.h"

// #include <iostream>

namespace PixDet {

  FullDetectorModel::FullDetectorModel(PixCon::IConnItemFactory &factory,
				       const std::shared_ptr<ILogoFactory> &logo_factory,
				       DetectorCanvasView *canvas_view,
				       bool old_crate_naming_convention,
                       drawDetectorSections const & shouldDraw ): Detector(factory, canvas_view) {
    QGraphicsScene *canvas = canvas_view->scene();

    m_zoomSep = static_cast<int>(m_diskHeight * .03/2);

    int large_space = 30;
    int small_space = 10;

    //The positions below give the y-position of the top edge for the given item. Positive y goes downwards
    //i.e. if you want to place somethign right below DBM, you take ibldbm_start_top and then add a tiny bit
    //Top refers to the side drawn on the top, i.e. the "A" side, whereas bot is the C side. ALL THE POSITIONS
    //mark the upper (top) edge of the item

    //For the layout, we start with the title, then without space we move to the crates
    //there are two layers of crates per section, each with m_cratesHeight (thus in total two)
    int crate_start_top = m_titleHeight;
    //After the crates we have a large space, then we start with the disks. In total they occuppy two m_diskHeight of space
    //one for each row. The row items span over the entire canvas, in order to make sure we have the right size, we insert
    // a small_space hight blank item
    int disk_start_top = crate_start_top + (shouldDraw.crates ? (2*m_cratesHeight + large_space) : small_space);
    //After the disks, small space and start with the barrel. The barrel occupies a
    //single m_barrelHeight for all the pixel barrel items
    int barrel_start_top = disk_start_top + (shouldDraw.disks ? (m_diskHeight + small_space) : 0);
    //The IBL draw function is a bit weird (still need to debug). In order to make it look good,
    //we add a large space before and after IBL
    int ibl_start_top = barrel_start_top + (shouldDraw.barrel ? (m_barrelHeight + (shouldDraw.IBL ? large_space : small_space)) : 0);
    //After IBL we add DBM (and the large space below IBL)
    int ibldbm_start_top = ibl_start_top + (shouldDraw.IBL ? (m_iblHeight + (shouldDraw.IBL ? large_space : small_space)) : 0);
    //Then we need space for the PP0 zoom panel
    int zoom_start = ibldbm_start_top + (shouldDraw.DBM ? (m_IBLDBMHeight + small_space) : 0);
    //And the other way back. If we don't insert a PP0 zoom we insert a large space to seperate everything visually
    int ibldbm_start_bot = zoom_start + (shouldDraw.PP0s ? (m_zoomHeight + small_space) : large_space);
    int ibl_start_bot = ibldbm_start_bot + (shouldDraw.DBM ? (m_IBLDBMHeight + (shouldDraw.IBL ? large_space : small_space)) : 0);
    int barrel_start_bot = ibl_start_bot + (shouldDraw.IBL ? (m_iblHeight + (shouldDraw.IBL ? large_space : small_space)) : 0 );
    int disk_start_bot = barrel_start_bot + (shouldDraw.barrel ? (m_barrelHeight + small_space) : 0);
    int crate_start_bot = disk_start_bot + (shouldDraw.disks ? (m_diskHeight + small_space) : 0 );

    //This is the layout for the x-direction. We start at x-position 0 for the left
    //column for half the canvas minus two times 25px in the centre
    int ref_canvas_width = canvas->width();
    int half_canvas_width = canvas->width()/2;
    int half_canvas_draw = half_canvas_width - 25;
    int left_x_start = 0;
    int right_x_start = half_canvas_width + 25;

    int PPOwidth = static_cast<int>(ref_canvas_width*0.9);
    //5 px margin on each side
    int legendWidth = ref_canvas_width - PPOwidth - 10;

    if (shouldDraw.PP0s) {
      m_zoomView = std::make_unique<PP0ZoomView>( canvas_view, QRect(5, zoom_start, PPOwidth, m_zoomHeight), logo_factory );
    }
    if (shouldDraw.legend) {
        // zoom_start is the top position when the zoom view is expanded, if it's collapsed,
        // we move the palette up by half its height, 5 px before and 5px after PP0 margin
        int yPalette = (shouldDraw.PP0s ? zoom_start : (zoom_start - m_paletteHeight/2));
        if(yPalette < 0) yPalette = m_titleHeight;
        //for some reason there is a slight shift down, correcting by 40px hardcoded
        m_paletteGeometry = QRect( PPOwidth+10, yPalette-40, legendWidth, m_paletteHeight );
    } else {
        m_paletteGeometry = QRect(0,0,0,0);
    }
    if (shouldDraw.crates) {
        if (old_crate_naming_convention) {
            addCrateRow(0, crate_start_top, ref_canvas_width, m_cratesHeight, 3, std::vector<std::string>{"Crate 0", "Crate 1"});
            addCrateRow(0, crate_start_top + m_cratesHeight, ref_canvas_width, m_cratesHeight, 3, std::vector<std::string>{"Crate 2", "Crate 3", "Crate 4"});
            addCrateRow(0, crate_start_bot, ref_canvas_width, m_cratesHeight, 3, std::vector<std::string>{"Crate 5", "Crate 6"});
            addCrateRow(0, crate_start_bot + m_cratesHeight, ref_canvas_width, m_cratesHeight, 3, std::vector<std::string>{"Crate 7", "Crate 8"});
        } else {
            addCrateRow(0, crate_start_top, ref_canvas_width, m_cratesHeight, 3, std::vector<std::string>{"Crate D1", "Crate B1"});
            addCrateRow(0, crate_start_top + m_cratesHeight, ref_canvas_width, m_cratesHeight, 3, std::vector<std::string>{"Crate L1", "Crate B2","Crate L3"});
            addCrateRow(0, crate_start_bot, ref_canvas_width, m_cratesHeight, 3, std::vector<std::string>{"Crate L2", "Crate I1","Crate L4"});
            addCrateRow(0, crate_start_bot + m_cratesHeight, ref_canvas_width, m_cratesHeight, 3, std::vector<std::string>{"Crate D2", "Crate B3"});
        }
    } else {
        addBlankBar(0, crate_start_top, ref_canvas_width, small_space);
    }
    if(shouldDraw.disks) {
        for (size_t layer_i=0; layer_i<3; layer_i++) {
            for (size_t sector_i=0; sector_i<4; sector_i++) {
                addDiskItems(0, layer_i, sector_i, 0, disk_start_top, ref_canvas_width / 2, m_diskHeight);
                addDiskItems(0, layer_i, sector_i + 4, ref_canvas_width / 2, disk_start_top, ref_canvas_width / 2, m_diskHeight);
                addDiskItems(1, layer_i, sector_i, 0, disk_start_bot, ref_canvas_width / 2, m_diskHeight);
                addDiskItems(1, layer_i, sector_i + 4, ref_canvas_width / 2, disk_start_bot, ref_canvas_width / 2, m_diskHeight);
            }
        }
    }
    if(shouldDraw.barrel) {
        for (size_t layer_i=3; layer_i-->0;) {
            int n_staves = getNstaves(layer_i) / 2;
            addBarrelItems(0, layer_i, n_staves, 0, left_x_start, barrel_start_top, half_canvas_draw, m_barrelHeight, 50);
            addBarrelItems(0, layer_i, n_staves, n_staves, right_x_start, barrel_start_top, half_canvas_draw, m_barrelHeight, 50);
            addBarrelItems(1, layer_i, n_staves, 0, left_x_start, barrel_start_bot, half_canvas_draw, m_barrelHeight, 50);
            addBarrelItems(1, layer_i, n_staves, n_staves, right_x_start, barrel_start_bot, half_canvas_draw, m_barrelHeight, 50);
        }
    }
    if(shouldDraw.IBL) {
        //IBL as innermost layer, referred to as LI
        //staves divided exlicitely, thus function called four times -> for side A and side C twice
        //There is some weird placement stuff going on here - it looks good like this, but doesn't make much sense
        int n_staves = getNstavesIBL()/2;
        addIBLBarrelItems(0, n_staves, 0, left_x_start, ibl_start_top - m_iblHeight/2, half_canvas_draw, m_iblHeight, 50);
        addIBLBarrelItems(0, n_staves, n_staves, right_x_start, ibl_start_top - m_iblHeight/2, half_canvas_draw, m_iblHeight, 50);
        addIBLBarrelItems(1, n_staves, 0, left_x_start, ibl_start_bot + m_iblHeight/2, half_canvas_draw, m_iblHeight, 50);
        addIBLBarrelItems(1, n_staves, n_staves, right_x_start, ibl_start_bot + m_iblHeight/2, half_canvas_draw, m_iblHeight, 50);
    }
    if(shouldDraw.DBM) {
        //IBL DBM staves as additional items
        addIBLDBMBarrelItems(0, 1, 1, left_x_start, ibldbm_start_top, half_canvas_draw, m_IBLDBMHeight);
        addIBLDBMBarrelItems(0, 0, 7, right_x_start, ibldbm_start_top, half_canvas_draw, m_IBLDBMHeight);
        addIBLDBMBarrelItems(1, 1, 1, left_x_start, ibldbm_start_bot, half_canvas_draw, m_IBLDBMHeight);
        addIBLDBMBarrelItems(1, 0, 7, right_x_start, ibldbm_start_bot, half_canvas_draw, m_IBLDBMHeight);
    }

    int label_font_size = 22;
    QFont label_font;
    QRectF label_size;
    //Define position and write letters A and C if only DBM is show, this looks very crappy so don't draw it
    if(shouldDraw.disks || shouldDraw.barrel || shouldDraw.IBL) {
        label_font.setPointSize(label_font_size);
        label_font.setBold(true);
        QGraphicsTextItem *label_side = canvas->addText("A-side");
        label_side->setFont(label_font);
        label_size = label_side->boundingRect();
        label_side->setX((ref_canvas_width - label_size.width()) / 2);
        label_side->setY(disk_start_top);
        label_side->show();
        label_side = canvas->addText("C-side");
        label_side->setFont(label_font);
        label_size = label_side->boundingRect();
        label_side->setX((ref_canvas_width - label_size.width()) / 2);
        label_side->setY(disk_start_bot + (shouldDraw.disks ? m_diskHeight : 0) - label_size.height());
        label_side->show();
    }
    label_font_size = 12;
    int disk_label_font_size = 9;
    label_font.setPointSize(label_font_size);
    QColor layer_colour(140,140,140);
    //Define position and write name of layers
    if(shouldDraw.barrel) {
        for (size_t layer_i = 3; layer_i-- > 0;){//so that layer_i = 2,1,0
            std::string label_str = "L"+std::to_string(layer_i);
            for (int side_i = 1; side_i>=-1; side_i-=2) {
                QGraphicsTextItem* label_layer = canvas->addText(label_str.c_str());
                label_layer->setFont(label_font);
                label_layer->setDefaultTextColor(layer_colour);
                label_size = label_layer->boundingRect();
                label_layer->setX((ref_canvas_width - label_size.width()) / 2);
                if (side_i>0) {
                    label_layer->setY(barrel_start_top + m_barrelHeight / 3 * (3 - layer_i) - 20 - label_size.height() / 2);
                }
                else {
                    label_layer->setY(barrel_start_bot + m_barrelHeight - m_barrelHeight / 3 * (3 - layer_i) + 20 - label_size.height() / 2);
                }
                label_layer->show();
            }
        }
    }
    //Labels for IBL
    if(shouldDraw.IBL) {
        for (int side_i = 1; side_i>=-1; side_i-=2) {
            QGraphicsTextItem *label_layer = canvas->addText("LI");
            label_layer->setFont(label_font);
            label_layer->setDefaultTextColor(layer_colour);
            label_size = label_layer->boundingRect();
            label_layer->setX((ref_canvas_width - label_size.width()) / 2);
            if (side_i>0) {
                label_layer->setY(ibl_start_top + m_iblHeight/2);
            } else {
                label_layer->setY(ibl_start_bot + m_iblHeight/2);
            }
            label_layer->show();
        }
    }
    //Labels for IBL DBM
    if(shouldDraw.DBM) {
        label_font_size = 10;
        label_font.setPointSize(label_font_size);
        std::stringstream labelIBLDBM_str;
        labelIBLDBM_str << "LI DBM";
        for (int side_i = 1; side_i>=-1; side_i-=2) {
            QGraphicsTextItem *label_layer = canvas->addText(labelIBLDBM_str.str().c_str());
            label_layer->setFont(label_font);
            label_layer->setDefaultTextColor(layer_colour);
            label_size = label_layer->boundingRect();
            label_layer->setX((ref_canvas_width - label_size.width()) / 2);
            if (side_i>0) {
                label_layer->setY(ibldbm_start_top + m_IBLDBMHeight/2 - label_size.height() / 2);
            } else {
                label_layer->setY(ibldbm_start_bot + m_IBLDBMHeight/2 - label_size.height() / 2);
            }
            label_layer->show();
        }
        //Labels for IBL DBM (individual parts)
        /*label_font_size = 8;
        label_font.setPointSize(label_font_size);
        std::stringstream labelIBLDBM_small_str;
        bool c_side, left_part;
        for (int side_i = 1; side_i>=-1; side_i-=2) {
            if (side_i > 0) c_side=0;
            else c_side=1;
            //Loop over all four DBM parts
            for (int module_m = 1; module_m < 4+1; module_m++) {
                if (module_m < 3) left_part = 1;
                else left_part=0;
                labelIBLDBM_small_str << (c_side ? "C" : "A") <<  (left_part ? "_12" : "_34") << "_M" << module_m;
                QGraphicsTextItem *label_layer = canvas->addText(labelIBLDBM_small_str.str().c_str());
                label_layer->setFont(label_font);
                label_layer->setDefaultTextColor(layer_colour);
                label_size = label_layer->boundingRect();
                if (left_part) {label_layer->setX( ref_canvas_width/4  - 110 + module_m*60 );}
                else label_layer->setX( ref_canvas_width*3/4  - 110 + (module_m-2)*60 );
                if (side_i>0) {
                    label_layer->setY(ibldbm_start_top + m_IBLDBMHeight + label_size.height() / 2);
                } else {
                    label_layer->setY(ibldbm_start_bot - label_size.height() / 2);
                }
                label_layer->show();
                labelIBLDBM_small_str.str("");
            }
        }*/

    }
    //Labels for disks
    if(shouldDraw.disks) {
        label_font.setPointSize(disk_label_font_size);
        float radius_factor = getRadiusFactor();
        int radius = getRadius();
        for (unsigned int disk_i = 3; disk_i-- > 0;){
            std::stringstream label_str;
            label_str << "D" << disk_i + 1;
            for (int side_i = 1; side_i>=-1; side_i-=2) {
                for (unsigned int half_i = 1; half_i<4; half_i+=2) {
                    QGraphicsTextItem *label_disk = canvas->addText(label_str.str().c_str());
                    label_disk->setFont(label_font);
                    label_disk->setDefaultTextColor(layer_colour);
                    label_size = label_disk->boundingRect();
                    label_disk->setX((ref_canvas_width * half_i / 2 - label_size.width() * 3/4 ) / 2 - radius * (1 - (1. / 3. * (2 - disk_i))* radius_factor));
                    if (side_i>0) {
                        label_disk->setY( disk_start_top + m_diskHeight - label_size.height()/2 );
                    } else {
                        label_disk->setY(disk_start_bot - label_size.height()/2);
                    }
                    label_disk->show();
                }
            }
        }
    }
    resizeView();
  }

  void FullDetectorModel::showPP0Zoom(PixCon::IConnItemFactory &factory,
                                      DetectorCanvasView *canvas_view,
                                      const PixA::Pp0List::const_iterator &pp0_begin,
                                      const PixA::Pp0List::const_iterator &pp0_end) {
      if (m_zoomView) {
          m_zoomView->drawPp0sOfRod(factory, canvas_view, pp0_begin, pp0_end);
      }
      resizeView();
  }

  void FullDetectorModel::updateTitle(const std::string &title, const std::string &sub_title) {
      int title_margin_height=m_zoomSep;
      int title_margin_width=15;
      drawTitle(title_margin_width, title_margin_height , width()- title_margin_width*2, m_titleHeight-title_margin_height*2, title, sub_title);
      resizeView();
  }
}
