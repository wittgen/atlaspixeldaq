#ifndef _Detector_h_
#define _Detector_h_

#include <QGraphicsScene>
#include <map>
#include <vector>
#include <string>
#include <memory>
#include "PP0ZoomView.h"
#include <ConfigWrapper/Connectivity.h>

namespace PixCon {
  class ConnItem;
  class IConnItemFactory;
}

class DetectorCanvasView;

namespace PixDet {

  class Detector
  {
  public:
    Detector(PixCon::IConnItemFactory &factory, DetectorCanvasView *canvas_view);
    virtual ~Detector() {};

    void OutlineItems();
    QRect addBarrelItems(bool c_side,
			 unsigned int layer,
			 unsigned int n_bistaves,
			 unsigned int first_stave,
			 int xpos0,
			 int ypos0,
			 int width,
			 int height,
			 float max_angle);

    QRect addStaveItems(bool c_side,
			 unsigned int layer,
			 unsigned int bistave,
			 int xpos0,
			 int ypos0,
			 int width,
			 int height);

/*     QRect addSectorItems(bool c_side, */
/* 		       unsigned int disk_layer, */
/* 		       unsigned int sector, */
/* 		       int xpos0, */
/* 		       int ypos0, */
/* 		       int disks_width, */
/* 		       int disks_height, */
/* 		       float sector_rot=0.); */

    QRect addSectorItems(bool c_side,
			 unsigned int layer,
			 unsigned int bi_sector,
			 unsigned int sector,
			 int xpos0,
			 int ypos0,
			 int width,
			 int height);
    
    QRect addDiskItems(bool c_side,
		       unsigned int disk_layer,
		       unsigned int sector,
		       int xpos0,
		       int ypos0,
		       int disks_width,
		       int disks_height,
		       float sector_rot=0.);

    QRect addCrateItem(const std::string &crate_name,
		       int xpos0,
		       int ypos0,
		       int crate_width,
		       int crate_height);

    QRect addCrateRow(int xpos0,
		      int ypos0,
		      int crates_width,
		      int crates_height,
		      unsigned int max_ncrates,
              std::vector<std::string> const & crate_names);

    QRect addIBLBarrelItems(bool c_side,
			 unsigned int n_bistaves,
			 unsigned int first_stave,
			 int xpos0,
			 int ypos0,
			 int width,
			 int height,
			 float max_angle);
//*/
    QRect addIBLDBMBarrelItems(bool c_side,
			 bool left_part,
			 unsigned int first_module,
			 int xpos0,
			 int ypos0,
			 int width,
			 int height);

    QRect addDummyStaveItems(bool c_side, std::string stave_name,
				      int xpos0, int ypos0,
				      int width, int height);

    QRect addIBLStaveItems(bool c_side,
			unsigned int stave,
			int xpos0,
			int ypos0,
			int width,
			int height);

    QRect addIBLDBMStaveItems(bool c_side,
			unsigned int stave,
			int xpos0,
			int ypos0,
			int width,
			int height);

    QRect addBlankBar(int xpos, int ypos, int width, int height);

    void ConnectionItems();
    void showPP0Zoom(PixCon::IConnItemFactory &factory, DetectorCanvasView *canvas_view, const PixA::Pp0List &list) {
      showPP0Zoom(factory,canvas_view, list.begin(),list.end());
    }

    virtual void showPP0Zoom(PixCon::IConnItemFactory &factory,
			     DetectorCanvasView *canvas_view,
			     const PixA::Pp0List::const_iterator &pp0_begin,
			     const PixA::Pp0List::const_iterator &pp0_end) =  0 ;

    virtual void updateTitle(const std::string &title, const std::string &sub_title) = 0;


    bool hasSpaceForPalette() const { return m_paletteGeometry.width()>0;}
    const QRect &paletteGeometry() const { return m_paletteGeometry; }

    int getNstaves(int layer){
      return m_staves[layer];
    }

    int getNstavesIBL(){
      return m_IBLstaves;
    }//*/

    float getRadiusFactor(){
      return m_radius_factor;
    }

    int getRadius(){
      return m_radius;
    }

    void drawTitle(int xpos0,
		   int ypos0,
		   int title_box_width,
		   int title_box_height,
		   const std::string &title,
		   const std::string &sub_title);

    int width()  const { return m_canvas->width(); }
    int height() const { return m_canvas->height(); }

  private:
    unsigned int m_disks;
    unsigned int m_layers;
    unsigned int m_sectors;
    std::vector<unsigned int> m_staves;
    unsigned int m_IBLstaves;
    int m_half_sep;
    int m_disk_margin;
    int m_barrel_margin;
    int m_crate_margin;
    int m_stave_margin;
    int m_y_margin;
    int m_radius;
    int m_bottom_extra_space = 50;

    std::vector<float> m_disk_scale;
    std::vector<float> m_layer_scale;
    float m_IBL_scale;
    float m_radius_factor;

    PixCon::IConnItemFactory *m_factory;
    DetectorCanvasView *m_canvas_view;
    QGraphicsScene *m_canvas;
  protected:
    QRect m_paletteGeometry;
    void resizeView();
    std::unique_ptr<PP0ZoomView> m_zoomView = nullptr;

    //Synchronising some sizes between the Toothpix SR1 and detector layout
    int m_titleHeight = 75;
    int m_cratesHeight = 75;
    int m_zoomHeight = 200;
    int m_paletteHeight = 200;
  private:
  };

}
#endif
