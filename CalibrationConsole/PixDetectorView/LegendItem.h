// Dear emacs, this is -*-c++-*-
#ifndef _LegendItem_h_
#define _LegendItem_h_

#include <qcolor.h>
#include <qpoint.h>
#include <qrect.h>
#include <QGraphicsScene>

class LegendItem : public QGraphicsRectItem
{
public:
  LegendItem(const QRectF &rect)
    : QGraphicsRectItem(rect)
  {}

private:
};

#endif
