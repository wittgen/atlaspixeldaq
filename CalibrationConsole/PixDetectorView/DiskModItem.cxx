#include "DiskModItem.h"
#include <qpainter.h>

namespace PixCon {

  void DiskModItem::drawShape ( QPainter & p ) {
    p.setBrush(brush());
    p.setPen(m_pen);
    p.drawPolygon(polygon());
  }

  bool DiskModItem::updateStyle()
  {
    bool ret=false;
    std::pair<QBrush , QPen > style = brushAndPen();

    if (style.first != brush() ) {
      setBrush(style.first);
      ret=true;
    }
    if (style.second != m_pen )  {
      m_pen = style.second;
      ret=true;
    }
    return ret;
  }


}

