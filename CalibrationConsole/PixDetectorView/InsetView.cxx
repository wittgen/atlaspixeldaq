#include "InsetView.h"
#include "DetectorCanvasView.h"

void InsetView::clearInset(DetectorCanvasView *canvas_view)
{
  for (std::vector<QGraphicsItem *>::iterator item_iter = m_insetItems.begin();
       item_iter != m_insetItems.end();
       item_iter++) {
    delete *item_iter;
  }
  m_insetItems.clear();

  for (ConnItemList_t::iterator item_iter = m_insetConnItems.begin();
       item_iter != m_insetConnItems.end();
       item_iter++) {

    canvas_view->removeFromSelection(item_iter->second);
//     if (item_iter->second->isModuleItem()) {
//       canvas_view->removeModule(item_iter->second);
//     }
//     else {
//       canvas_view->removeExtraItem(item_iter->second);
//     }
    delete item_iter->second;
  }
  m_insetConnItems.clear();
}

void InsetView::registerInsetItem(DetectorCanvasView * /* canvas_view */, PixCon::ConnItem *item) {
  m_insetConnItems.insert(std::make_pair(item->name(),item));
//   if (item->isModuleItem()) {
//     canvas_view->addModule(item);
//   }
//   else {
//     canvas_view->addExtraItem(item);
//   }
}
