#ifndef _PixDet_ILogoFactory_h_
#define _PixDet_ILogoFactory_h_

class QRectF;

class DetectorCanvasView;

namespace PixDet {

  class ILogoFactory
  {
  public:
    virtual ~ILogoFactory() {}
    virtual void drawLogo(DetectorCanvasView *canvas_view, const QRectF &pos) = 0;
  };

}
#endif
