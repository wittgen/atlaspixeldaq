#ifndef _PixCon_IItemStyle_h_
#define _PixCon_IItemStyle_h_

#include <utility>

class QPen;
class QBrush;

namespace PixCon {

  /** Abstract interface for a module, rod etc. item to query the current style.
   */
  class IItemStyle
  {
  public:
    virtual ~IItemStyle() {}

    virtual std::pair<QBrush , QPen > brushAndPen(bool is_selected, bool is_unfocused) = 0;

  };

}

#endif
