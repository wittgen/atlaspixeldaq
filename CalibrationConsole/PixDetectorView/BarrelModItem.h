#ifndef _PixCon_BarrelModItem_h_
#define _PixCon_BarrelModItem_h_

#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <string>
#include "ConnItem.h"
#include <iostream>

namespace PixCon {
  
  class IItemStyle;

  class BarrelModItem : public ConnItem, public QGraphicsRectItem
  {
  public:
    BarrelModItem(const std::string &name, IItemStyle *style, const  QRectF &rec)
      : ConnItem(name,style, kModuleItem),QGraphicsRectItem(rec) { update();}

    QGraphicsItem *canvasItem()  {return this;}

    bool updateStyle();
  };

}
#endif
