#ifndef _PixDet_FullDetectorLogoFactory_h_
#define _PixDet_FullDetectorLogoFactory_h_

#include "LogoFactory.h"

class DetectorCanvasView;

namespace PixDet {

  class FullDetectorLogoFactory : public LogoFactory
  {
  public:
    FullDetectorLogoFactory();
    void drawLogo(DetectorCanvasView *canvas_view, const QRectF &pos);
    const QFont &defaultFont() const {return m_defaultFont; }

  private:
    enum EImage {kDetector, kController, kNImages};

   // unsigned int m_imageKey[kNImages];
    QFont m_defaultFont;
  };

}
#endif
