#include "Detector.h"
#include "DetectorCanvasView.h"
#include "IConnItemFactory.h"
#include <iomanip>

namespace PixDet {
  Detector::Detector(PixCon::IConnItemFactory &factory, DetectorCanvasView *canvas_view) : m_factory(&factory), m_canvas_view(canvas_view), m_zoomView(nullptr)
  {
    m_disks = 3;
    m_sectors = 8;
    m_half_sep = static_cast<int>( canvas_view->scene()->width()*.01 );
    m_disk_scale.resize(m_disks);
    m_disk_scale[0] = 0.73;
    m_disk_scale[1] = 0.9;
    m_disk_scale[2] = 1.0;
    m_radius_factor = 2./3.*.85;
    m_radius = 0;
    m_layers = 3;
    m_staves.resize(m_layers);
    m_staves[0] = 22;
    m_staves[1] = 38;
    m_staves[2] = 52;
    m_IBLstaves = 14;
    m_layer_scale.resize(m_layers);
    m_layer_scale[0] = 1.0;
    m_layer_scale[1] = 1.0;
    m_layer_scale[2] = 1.0;
    m_IBL_scale = 1.0;

    m_disk_margin = 20;
    m_barrel_margin = 20;
    m_crate_margin = 18;
    m_stave_margin = 10;
    m_y_margin = 10;
    m_canvas = canvas_view->scene();
  }

  void Detector::OutlineItems()
  {
  }


  QRect Detector::addBarrelItems(bool c_side,
				 unsigned int layer,
				 unsigned int n_staves,
				 unsigned int first_stave,
				 int xpos0,
				 int ypos0,
				 int width,
				 int height,
				 float max_angle)
  {
    QRect dimension;
    dimension.setX(xpos0);
    dimension.setY(ypos0);

    int size_per_stave = width/26;
    int stave_width = size_per_stave*0.8;
    float stave_gap = (width - stave_width*26)/(25.);
    size_t missing_staves = 26 - n_staves;

    int max_xpos=xpos0;
    int max_ypos=ypos0;

    assert(layer<m_layers);
    float layer_sep_factor = .1;
    //float stave_sep_factor = .3;
    float bend_factor = .5;

    float size = width / 2. - m_barrel_margin;
    float proj_width[3];
    float bend = size * bend_factor;

    for (unsigned int layer_i=0; layer_i<m_layers; layer_i++) {
        assert (m_staves[layer_i]%2==0);
        float angle=-max_angle;
        float angle_step=max_angle/(m_staves[layer_i]/4.);
        angle += angle_step / 2.;
        float total_width = 0;
        for (unsigned int stave_i=0; stave_i<m_staves[layer_i]/2; stave_i++) {
            total_width += std::abs(cos(angle * M_PI / 180.));
            angle += angle_step;
        }
        proj_width[layer_i] = total_width;
    }
    float stave_width_scale = 2 * size / proj_width[2];

    //int centre_x = static_cast<int>(xpos0 + size + m_barrel_margin);
    float barrel_length = height - m_y_margin;
    int centre_y = static_cast<int>(ypos0 + (c_side ? barrel_length + m_y_margin : 0));
    //    std::cout << "Ypos0: " << ypos0 << "  centre y: " << centre_y << std::endl; 

    std::stringstream stave_name;
    stave_name << "L" << layer;
    stave_name << "_B";

    float stave_length = (barrel_length - ((1 - std::abs(cos(max_angle*M_PI/180.))) * bend * m_layer_scale[0]))/(m_layers + (m_layers - 1) * layer_sep_factor);
    unsigned int n_mods = 7; /*(c_side ? 6 :7 );*/
    float mod_length = stave_length / 7;
    float xpos = missing_staves/2*(stave_width+stave_gap)+ xpos0; //centre_x - size * proj_width[layer] / proj_width[m_layers - 1];

    float angle = -max_angle;
    float angle_step= max_angle / (m_staves[layer] / 4.);
    angle += angle_step / 2.;

    float a_direction = (c_side ? -1 : 1);
    for (unsigned int layer_i = layer + 1; layer_i < 3; layer_i++) {
        centre_y += static_cast<int>(stave_length * (1. + layer_sep_factor) * a_direction);
    }

    unsigned int not_drawn_staves = (first_stave < m_staves[layer] / 2 ? first_stave : first_stave - m_staves[layer] / 2);
    assert(n_staves + not_drawn_staves <= m_staves[layer] / 2);
    for (unsigned int stave_i = 0; stave_i < not_drawn_staves; stave_i++){
        angle += angle_step;
    }
    for (unsigned int stave_i = first_stave; stave_i < n_staves + first_stave; stave_i++) {
        std::stringstream full_stave_name;
        unsigned int stave_nr = stave_i;
        float width_2 = std::abs(cos(angle * M_PI / 180.) * stave_width_scale);
        float ypos = centre_y + (1. - std::abs(cos(angle * M_PI / 180.))) * bend * m_layer_scale[layer] * a_direction;
        //      std::cout << "Bottom y for layer " << layer << " stave " << stave_i << ": " << ypos << std::endl;
        unsigned int start_mod;
        unsigned int mod_number;
        if (c_side) {
            start_mod = (stave_nr%2==0 ? 0 : 1);
            mod_number = (stave_nr%2==0 ? 7 : 6);
        }
        else {
            start_mod = (stave_nr%2==1 ? 0 : 1);
            mod_number = (stave_nr%2==1 ? 7 : 6);
        }
        full_stave_name << stave_name.str() << std::setw(2) << std::setfill('0') <<  (stave_nr/2+1)
                        << "_S" << (stave_nr%2+1)
                        << "_" << (c_side ? "C" : "A") << mod_number << "_M";
        for (unsigned int mod_i = n_mods; mod_i > start_mod; mod_i--) {
            std::stringstream mod_name;
            int mod_n = mod_i;
            mod_n--;
            mod_name << full_stave_name.str() << mod_n;
            if (mod_n>0) {
                mod_name << (c_side ? "C" : "A");
            }
	ypos += mod_length * a_direction;
	//	std::cout << "Bottom y for layer " << layer << " stave " << stave_i << " module " << mod_n << ": " << ypos << std::endl;
	PixCon::ConnItem *mod_item=m_factory->barrelModuleItem(mod_name.str(),
							    layer,
                                QRect(static_cast<int>(xpos), static_cast<int>(ypos),
                                  stave_width,
                                  static_cast<int>((mod_length + 1.) * a_direction)));
	
	if (ypos > max_ypos) {
	  max_ypos = static_cast<int>(ypos);
	}
	if (xpos + width_2 > max_xpos) {
	  max_xpos = static_cast<int>(xpos + width_2);
	}
    m_canvas->addItem(dynamic_cast<QGraphicsRectItem*>(mod_item));
    m_canvas_view->addModule(mod_item);
      }
      xpos += stave_width+stave_gap;
      angle += angle_step;
      ypos -= mod_number * mod_length * a_direction;
    }

    dimension.setWidth(static_cast<int>(max_xpos-xpos0));
    dimension.setHeight(static_cast<int>(max_ypos-ypos0));
    return dimension;
  }

  QRect Detector::addStaveItems(bool c_side, unsigned int layer, unsigned int bistave,
				int xpos0, int ypos0, int width, int height) {
    int max_xpos = xpos0, max_ypos = ypos0;
    std::string stave_name = "L"+std::to_string(layer)+"_B";

    // number of modules etc
    int mod_length = width / 8.5;
    int ypos = ypos0;
  
    for(int stave_nr = 1; stave_nr<3; stave_nr++){
      int xpos = xpos0;
      int start_mod = 0;
      int mod_number = 0;
      if (c_side) {
	start_mod = (stave_nr==1 ? 0 : 1);
	mod_number = (stave_nr==1 ? 7 : 6);
      }
      else {
	start_mod = (stave_nr==2 ? 0 : 1);
	mod_number = (stave_nr==2 ? 7 : 6);
      }

      std::stringstream full_stave_name;
      full_stave_name << stave_name << std::setw(2) << std::setfill('0') <<  (bistave) 
		      << "_S" << (stave_nr)
		      << "_" << (c_side ? "C" : "A") << mod_number << "_M";
 
      ypos += (stave_nr-1)*(height + height/2.);
    
      for(int mod_i = 7; mod_i > start_mod; mod_i--) {
	int mod_n = mod_i - 1;
	std::string mod_name = full_stave_name.str() + std::to_string(mod_n);
	if (mod_n>0) {
	  mod_name += (c_side ? "C" : "A");
	}

	if (c_side) xpos -= mod_length;
	PixCon::ConnItem *mod_item=m_factory->barrelModuleItem(mod_name, layer, QRect(xpos, ypos, mod_length, height));
	if (!c_side) xpos += mod_length;
      
	if (ypos > max_ypos) {
	  max_ypos = ypos;
	}
	if (xpos + width > max_xpos) {
	  max_xpos = xpos+width;
	}
        m_canvas->addItem(dynamic_cast<QGraphicsRectItem*>(mod_item));
        m_canvas_view->addModule(mod_item);
      } //loop over modules
    } //loop over staves
    return QRect(xpos0, ypos0, max_xpos-xpos0, max_ypos-ypos0);
  }

  QRect Detector::addDummyStaveItems(bool c_side, std::string stave_name,
                                     int xpos0, int ypos0, int width, int height) {
    int max_xpos = xpos0, xpos = xpos0;
    int max_ypos = ypos0, ypos = ypos0;
    int mod_length = width / 8.5;

    //Stave name, eg: LX_B99_S1_A7 
    for(int mod_i = 6; mod_i >= 0; mod_i--) {
      
      std::string mod_name = stave_name + "_M" + std::to_string(mod_i) +
	                     //M0 has no A/C attached, other modules do!
	                     ( mod_i == 0 ? "" : (c_side ? "C" : "A"));

      if (c_side) xpos -= mod_length;
      PixCon::ConnItem *mod_item=m_factory->barrelModuleItem(mod_name, 1,//layer, so far layer = 1,2,3
							     QRect(xpos, ypos, mod_length, height));
      if (!c_side) xpos += mod_length;

      if (xpos + width > max_xpos) {
	max_xpos = xpos + width;
      }

      m_canvas->addItem(dynamic_cast<QGraphicsRectItem*>(mod_item));
      m_canvas_view->addModule(mod_item);
    }
    return QRect(xpos0, ypos0, max_xpos-xpos0, max_ypos-ypos0);
  }

  QRect Detector::addIBLStaveItems(bool c_side,	unsigned int stave,
				   int xpos0, int ypos0, int width, int height) {
    int max_xpos = xpos0, xpos = xpos0;
    int max_ypos = ypos0, ypos = ypos0;

    std::string stave_name = "LI_S"+std::to_string(stave) + (c_side ? "_C" : "_A");
    int mod_length = width / 8.5;

    for(int mod_i = 8; mod_i > 0; mod_i--) {
      
      int half_mod_i = (mod_i+1)/2;//modulo thus directly considered
      std::string mod_name = stave_name+"_M"+std::to_string(half_mod_i)+(c_side ? "_C" : "_A")+std::to_string( mod_i);

      if (c_side) xpos -= mod_length;
      PixCon::ConnItem *mod_item=m_factory->barrelModuleItem(mod_name, 1,//layer, so far layer = 1,2,3
							     QRect(xpos, ypos, mod_length, height));
      if (!c_side) xpos += mod_length;

      if (xpos + width > max_xpos) {
	max_xpos = static_cast<int>(xpos + width);
      }
      m_canvas->addItem(dynamic_cast<QGraphicsRectItem*>(mod_item));
      m_canvas_view->addModule(mod_item);
    }
    return QRect(xpos0, ypos0, max_xpos-xpos0, max_ypos-ypos0);
  }

  QRect Detector::addIBLDBMStaveItems(bool c_side, unsigned int stave,
				      int xpos0, int ypos0, int width, int height) {
    int max_xpos = xpos0, xpos = xpos0;
    int max_ypos = ypos0, ypos = ypos0;

    std::string stave_name = "LI_S"+std::to_string(stave)+(c_side ? "_C" : "_A");
    int mod_length = width / 8.5;

    for(int mod_i = 6; mod_i > 0; mod_i--) {
      int half_mod_i = (mod_i+5)/3 + 1;//modulo thus directly considered
      std::string mod_name = stave_name + (c_side ? "_34" : "_12") + "_M" + std::to_string(half_mod_i)
	                     + (c_side ? "_C" : "_A") + std::to_string(mod_i+6);

      if (c_side) xpos -= mod_length;
      PixCon::ConnItem *mod_item=m_factory->barrelModuleItem(mod_name, 1,//layer, so far layer = 1,2,3
							     QRect(xpos, ypos, mod_length, height));
      if (!c_side) xpos += mod_length;

      if (xpos + width > max_xpos) {
	max_xpos = static_cast<int>(xpos + width);
      }
      m_canvas->addItem(dynamic_cast<QGraphicsRectItem*>(mod_item));
      m_canvas_view->addModule(mod_item);
    }
    return QRect(xpos0, ypos0, max_xpos-xpos0, max_ypos-ypos0);
  }

  QRect Detector::addSectorItems(bool c_side,
				unsigned int layer,
				unsigned int bi_sector,
				unsigned int sector,
				int xpos0,
				int ypos0,
				int width,
				int height)
    
  {
    QRect dimension;
    dimension.setX(xpos0);
    dimension.setY(ypos0);
    
    int max_xpos=xpos0;
    int max_ypos=ypos0;


    std::stringstream sector_name;

    sector_name << "D" << layer << (c_side ? "C" : "A") << "_B";


    float mod_length = width / 8;
    float ypos = static_cast<int>(ypos0);
    float xpos = static_cast<int>(xpos0);

  
    std::stringstream full_sector_name;
    
    full_sector_name << sector_name.str() << std::setw(2) << std::setfill('0') <<  (bi_sector) 
		     << "_S" << (sector);

 
    for (unsigned int mod_i = 1; mod_i < 7; mod_i++) {
      std::stringstream mod_name;
      mod_name << full_sector_name.str() << "_M" << mod_i;
      
      if (sector == 2)	xpos += mod_length;
     
      PixCon::ConnItem *mod_item=m_factory->diskModuleItem(mod_name.str(),
							       layer,
							       QRect(static_cast<int>(xpos),
								     static_cast<int>(ypos),
								     static_cast<int>(mod_length),
                                     static_cast<int>(height)));

      if (sector == 1)     xpos += mod_length;

      
      if (ypos > max_ypos) {
	max_ypos = static_cast<int>(ypos);
      }
      if (xpos + width > max_xpos) {
	max_xpos = static_cast<int>(xpos + width);
      }
      m_canvas->addItem(dynamic_cast<QGraphicsRectItem*>(mod_item));
      m_canvas_view->addModule(mod_item);
    }
    
    
    
    dimension.setWidth(static_cast<int>(max_xpos-xpos0));
    dimension.setHeight(static_cast<int>(max_ypos-ypos0));
    return dimension;
    
  }
  
  QRect Detector::addDiskItems(bool c_side,
			       unsigned int disk_layer,
			       unsigned int sector,
			       int xpos0, int ypos0,
			       int disks_width,
			       int disks_height,
			       float sector_rot)
  {

    QRect dimension;
    dimension.setX(xpos0);
    dimension.setY(ypos0);

    assert(disk_layer < m_disks);
    assert(sector < m_sectors);
    assert(m_sectors % 2 == 0);

    // margin left/right, sepration between both half disks, sector separtion
    int half_disk_sep = m_half_sep;
    int margin = m_disk_margin;
    int sector_slide_out = m_disk_margin / 2;

    //    int bi_sector_slide_out = 0;

    int odd_mod_slide_out = 0;
    float layer_radius_sep_factor = .03;

    std::vector<float> inner_layer_radius(m_disks + 1,0);

    float x_scale = 1.0;
    float y_scale = 0.5;

    float size = std::min((disks_width / x_scale - half_disk_sep / 2.) / 2. - margin, static_cast<double>(disks_height / y_scale - m_y_margin)/2);
    float radius = size;
    m_radius = static_cast<int>(radius);
    float margin_x = disks_width - half_disk_sep / 2. - 2 * size;
    margin_x /= 2.;
    float margin_y = disks_height - size; 
    margin_y /= 2.;
    if (c_side){
      margin_y = -margin_y;
    }

    //    std::cout << "Disks width: " << disks_width << " disks height: " << disks_height << std::endl;
    //    std::cout << "Available width: " << (disks_width - half_disk_sep / 2.) / 2. - margin << " available height: " << disks_height - m_y_margin << std::endl;
    //    std::cout << "Size: " << size << " radius: " << radius << std::endl;

    float use_radius_factor = m_radius_factor;
    float use_factor_sum=0;
    for (unsigned int i = 0; i < m_disks; i++) {
      use_factor_sum += m_disk_scale[i];
    }

    inner_layer_radius[0]=radius*(1.-use_radius_factor);
    for (unsigned int i = 0; i < m_disks; i++) {
      inner_layer_radius[i+1] = inner_layer_radius[i] + radius * use_radius_factor * m_disk_scale[i] / use_factor_sum;
    }

    float radius_inner = inner_layer_radius[disk_layer] * (1 + layer_radius_sep_factor);
    float radius_outer = inner_layer_radius[disk_layer + 1];


    float  mod_width= radius_outer * M_PI / 4 / 6; 
    //float  mod_length=use_radius_factor/3 * (1-layer_radius_sep_factor);

    //    double scale[3]={1.,.9,.73};

    //    int slide_out = 10;
    //    int half_disk_sep =20;
    //    int margin=20;

    int centre_x = static_cast<int>(xpos0 + margin_x + size);
    int centre_y = static_cast<int>(ypos0 + (c_side ? m_y_margin : size));

    //    std::cout << "xpos0: " << xpos0 << " ypos0: " << ypos0 << std::endl;
    //    std::cout << "centre x: " << centre_x << " centre y: " << centre_y << std::endl; 

    QRect module_dimensions(static_cast<int>(radius_inner),-static_cast<int>(mod_width * 0.5),static_cast<int>(radius_outer-radius_inner),static_cast<int>(mod_width));
    //    QWMatrix rot;
    // QWMatrix skew;
    //skew.scale(scale[disk_layer]*1.,scale[disk_layer]*.7);
    //    rot.rotate(180);
    //    int rot_angle=(c_side ? -180+phi[0] : -180 );
    std::stringstream sector_only, sector_name;
    if (sector<7){
      sector_only << "B" << std::setfill('0') << std::setw(2) << ((sector+1) / 2)+1 << "_S"  << 2-(sector%2);
    }
    else{
      sector_only << "B01_S1";
    }
    sector_name << "D" << disk_layer+1 << (c_side ? "C" : "A") << "_" << sector_only.str();

    //    int x_offset = radius + margin + xpos0;
    //    int y_offset = radius * scale[disk_layer] + (c_side ? -1 : 1 ) * (disk_layer * 10) + ypos0;

    float x_pos = centre_x;
    float y_pos = centre_y;

    float sec_angle0;
    float rot_angle_step = 180 / (m_sectors / 2.) / 6.;
    if (sector_rot != 0.) {
      sec_angle0= sector_rot;
    }
    else if (c_side) {
      sec_angle0 = -180. - 180 / (m_sectors / 2) * (sector % (m_sectors / 2));
      rot_angle_step = -rot_angle_step;
    }
    else {
      sec_angle0 = -180. + 180 / (m_sectors / 2) * (sector % (m_sectors / 2));
    }

    // slide out bi-sector
    //    float bi_sec_angle_centre = 0;
    //    if (sector_rot == 0.) {
    //       bi_sec_angle_centre= ( c_side ?  -((sector/2) % 2) * 90 - 225 : ((sector/2) % 2) * 90 - 135);
    //       float s = sin(bi_sec_angle_centre * M_PI/180.);
    //       float c = cos(bi_sec_angle_centre * M_PI/180.);
    //       x_pos += c * bi_sector_slide_out;
    //       y_pos += s * bi_sector_slide_out;
    //    }

    // slide out sector
    float sec_angle_centre=sec_angle0 + rot_angle_step*6 *.5;
    float s = sin(sec_angle_centre * M_PI/180.);
    float c = cos(sec_angle_centre * M_PI/180.);
    x_pos += c * sector_slide_out;
    y_pos += s * sector_slide_out;
    
    int label_font_size = 8;
    QFont label_font;
    label_font.setPointSize(label_font_size);
    QGraphicsTextItem *label_sector = m_canvas->addText(sector_only.str().c_str());
    label_sector->setFont(label_font);
    QRectF label_size = label_sector->boundingRect();
    int label_xpos = static_cast<int>(x_pos + c * radius * x_scale + c * label_size.width() / 2);
    int label_ypos = static_cast<int>(y_pos + s * radius * y_scale + s * label_size.height() / 2);
    //    std::cout << ypos0 << " " << centre_y << " " << y_pos << " " << label_ypos << " " << label_size.height() << std::endl;
    label_sector->setX(label_xpos - label_size.width() / 2);
    label_sector->setY(label_ypos - label_size.height() / 2);
    label_sector->show(); 


        //   std::cout << "INFO [Detector::addDiskItems2] " << sector_name.str()
        //    << " : " << " radius = " << radius_inner << " - " << radius_outer
        //    << " ( " << radius << ") "
        //    << "  center = " << centre_x << ", " << centre_y
        //    << "  width = " << mod_width
        //    << "  angle = " << sec_angle0
        //  //  << "  bi-sector angle = " << bi_sec_angle_centre
        //    << "  sector angle = " << sec_angle_centre
        //    << "  slide = " << c * sector_slide_out << ", " << s * sector_slide_out
        //    << std::endl;

/////// Where sectors actually get drawn
    for (unsigned int pass_i=0; pass_i<2; pass_i++) {
      float rot_angle = sec_angle0 + rot_angle_step *.5;
      for (unsigned int mod_i=0; mod_i<6; mod_i++) {
	if (pass_i == mod_i%2) {

        QTransform final;
        final.translate(x_pos,y_pos);
        final.scale(x_scale,y_scale);
        final.rotate(rot_angle);
        if (mod_i%2 == 1) {
            final.translate(odd_mod_slide_out,0.);
        }

	  std::stringstream mod_name;
	  
	  mod_name << sector_name.str() <<  "_M" << mod_i%6+1; //  @todo should be (mod_i*2+pass_i)%6+1

	  PixCon::ConnItem *mod_item=m_factory->diskModuleItem(mod_name.str(),
							    disk_layer,
                                              final.mapToPolygon(module_dimensions));

      m_canvas->addItem(dynamic_cast<QGraphicsPolygonItem*>(mod_item));
      m_canvas_view->addModule(mod_item);

	}
	rot_angle += rot_angle_step;
      }
    }
    dimension.setWidth(static_cast<int>(2*radius));
    dimension.setHeight(static_cast<int>(radius));
    return dimension;
  }

  QRect Detector::addCrateRow(int xpos0,
			      int ypos0,
			      int crates_width,
			      int crates_height,
			      unsigned int max_ncrates,
                  std::vector<std::string> const & crate_names)
  {

    QRect dimension;
    dimension.setX(xpos0);
    dimension.setY(ypos0);
    auto n_crates = crate_names.size();
    int xpos = xpos0;
    int ypos = ypos0;

    assert(n_crates <= max_ncrates);

    int crate_width_step = static_cast<int>(crates_width / max_ncrates);
    int crate_width = static_cast<int>(crates_width / max_ncrates) - 2 * m_crate_margin;
    int crate_height = static_cast<int>(crates_height - m_y_margin / 2);

    xpos += static_cast<int>(crate_width_step * (max_ncrates - n_crates + 1) / 2. - crate_width / 2.);
    ypos += m_y_margin / 4;
    for(unsigned int crate_i = 0; crate_i < n_crates; crate_i++){
        addCrateItem(crate_names[crate_i], xpos, ypos, crate_width, crate_height);
      xpos += crate_width_step;
    }
    dimension.setWidth(m_canvas->width());
    dimension.setHeight(crates_height);
    return dimension;
  }


  QRect Detector::addCrateItem(const std::string &crate_name,
			       int xpos0,
			       int ypos0,
			       int crate_width,
			       int crate_height)
  {
    QRect dimension;
    dimension.setX(xpos0);
    dimension.setY(ypos0);

    std::vector<PixCon::ConnItem *> &crate = m_canvas_view->newCrate(crate_name);

    int xpos = xpos0; 
    int ypos = ypos0; 

    float label_crate_sep_top = 0.05;
    float label_crate_sep_bottom = 0.05;
    float label_crate_sep_right = 0.05;
    int fontsize_rod = 10;
    int fontsize_crate = 14;

    QFont crate_font;
    QFont rod_font(crate_font);
    rod_font.setPointSize(fontsize_rod);
    crate_font.setPointSize(fontsize_crate);
    crate_font.setBold(true);

    QRectF label_size=QRectF(0,0,0,fontsize_rod);//rod_label->boundingRect();

    QGraphicsTextItem *title = m_canvas->addText(crate_name.c_str());
    title->setFont(crate_font);
    QRectF title_size=title->boundingRect();

    title->setY(ypos);

    //rod_label->setX(xpos);

    xpos += static_cast<int>(label_size.width() * (1. + label_crate_sep_right));

    title->setX(xpos);
    title->show();

    auto width = static_cast<int>((crate_width - label_size.width() * (1. + label_crate_sep_right)) / (22. - 5.));
    auto height = static_cast<int>(crate_height - title_size.height() * (1. + label_crate_sep_bottom) - label_size.height() * (1. + label_crate_sep_top));

    ypos += static_cast<int>(title_size.height() * (1. + label_crate_sep_bottom));
    ypos += static_cast<int>(height + label_size.height() * label_crate_sep_top);
    //rod_label->setY(ypos);
    //rod_label->show();

    std::string short_crate_name = crate_name;
    std::string::size_type pos=short_crate_name.find("Crate ");
    if (pos != std::string::npos) {
      short_crate_name.erase(pos,6);
      if (isdigit(short_crate_name.c_str()[pos])) {
	// old crate naming convention sill used for toothpix setup :
	short_crate_name.insert(pos,"C");
      }
      else if (short_crate_name.size()>pos+1
	       && (short_crate_name.c_str()[pos]=='L'
		   || short_crate_name.c_str()[pos]=='D'
		   || short_crate_name.c_str()[pos]=='B')
	       && isdigit(short_crate_name.c_str()[pos+1])) {
	// new pit naming convention:
      }
    }
    std::string rod_crate = "ROD_"+short_crate_name+"_S";

    for (size_t slot_i=5; slot_i<22; slot_i++) {
        PixCon::ConnItem* slot_item = nullptr;
        std::string label_str = std::to_string(slot_i);

        //Slots 5, 13 (TIM) and 21 are drawn full size, the others a bit smaller
        if (slot_i==13) {
            std::string tim_name = "TIM_"+short_crate_name;
            slot_item=m_factory->timItem(tim_name.c_str(),
                                         QRect(xpos, static_cast<int>(ypos - height - label_size.height() * label_crate_sep_top),
                                               width, height));
        } else if (slot_i==5 || slot_i==21) {
            std::string rod_name = rod_crate+label_str;
            slot_item=m_factory->rodItem(rod_name.c_str(),
                                         QRect(xpos, static_cast<int>(ypos - height - label_size.height() * label_crate_sep_top),
                                               width, height));
        } else  {
            std::string rod_name = rod_crate+label_str;
            int smallerHeight = height*0.8;
            slot_item=m_factory->rodItem(rod_name.c_str(),
                                         QRect(xpos, static_cast<int>(ypos - smallerHeight - label_size.height() * label_crate_sep_top),
                                               width, smallerHeight));
        }

        m_canvas->addItem(dynamic_cast<QGraphicsRectItem*>(slot_item));
        crate.push_back(slot_item);

        if (slot_i == 5 || slot_i == 13 || slot_i == 21) {
            QGraphicsTextItem *rod_label = m_canvas->addText(label_str.c_str());
            rod_label->setFont(rod_font);
            QRectF this_size=rod_label->boundingRect();
            rod_label->setY(ypos);
            rod_label->setX(xpos + width / 2. - this_size.width() / 2.);
            rod_label->show();
        }
        xpos += width;
    }

    dimension.setWidth(crate_width);
    dimension.setHeight(crate_height);
    return dimension;
  }

  QRect Detector::addBlankBar(int xpos, int ypos, int width, int height) {
      QRect box(xpos,ypos, width, height);
      QGraphicsRectItem* graphics_box = new QGraphicsRectItem(box);
      graphics_box->setPen(Qt::NoPen);
      m_canvas->addItem(graphics_box);
      return box;
  }

  //function which serves to add IBL barrel items
  QRect Detector::addIBLBarrelItems(bool c_side,
				 unsigned int n_staves,
				 unsigned int first_stave,
				 int xpos0,
				 int ypos0,
				 int width,
				 int height,
				 float max_angle)
  {
    QRect dimension;
    dimension.setX(xpos0);//set left end
    dimension.setY(ypos0);//set top position
    
    int max_xpos=xpos0;
    int max_ypos=ypos0;

    float layer_sep_factor = .1;
    //float stave_sep_factor = .3;
    float bend_factor = .5;

    int size_per_stave = width/26;
    int stave_width = size_per_stave*0.8;
    float stave_gap = (width - stave_width*26)/(25.);
    size_t missing_staves = 26 - n_staves;

    float size = width / 2. - m_barrel_margin;//m_barrel_margin = 20
    float proj_width;
    float bend = size * bend_factor;

      assert (m_IBLstaves%2==0);
      float angle=-max_angle;
      float angle_step=max_angle/(m_IBLstaves/4.);
      angle += angle_step / 2.;
      float total_width = 0;
      for (size_t stave_i=0; stave_i<m_IBLstaves/2; stave_i++) {
         total_width += std::abs(cos(angle * M_PI / 180.));
         angle += angle_step;
      }
      proj_width = total_width;

    float stave_width_scale = 0.6 * size / proj_width;
    //std::cout << "Stave width: " << width << std::endl;
    
    //int centre_x = static_cast<int>(xpos0 + size + m_barrel_margin);
    float barrel_length = height - m_y_margin;//m_y_margin = 10; height as input parameter
    int centre_y = static_cast<int>(ypos0 + (c_side ? barrel_length + m_y_margin : 0));//position depends on whether side A or B is considered
    //    std::cout << "Ypos0: " << ypos0 << "  centre y: " << centre_y << std::endl; 

    std::stringstream stave_name;
    stave_name << "LI";
    stave_name << "_S";

    float stave_length = 8.0*(barrel_length - ((1 - std::abs(cos(max_angle*M_PI/180.))) * bend * m_IBL_scale ))/(m_layers + (m_layers - 1) * layer_sep_factor); //= barrel_length*0.9;

    unsigned int n_mods = 8; //(c_side ? 6 :7 );
    float mod_length = stave_length / 8;//length of one module 
    float xpos = missing_staves/2*(stave_width+stave_gap)+ xpos0; //centre_x - size * proj_width[layer] / proj_width[m_layers - 1]
//    float xpos = centre_x - 6*stave_width_scale/2; //- size*0.3; // * proj_width[layer] / proj_width[m_layers - 1];

    angle = -max_angle;
    angle_step= max_angle / (m_IBLstaves / 4.);
    angle += angle_step / 2.;

    float a_direction = (c_side ? -1 : 1);
    //for (unsigned int layer_i = layer + 1; layer_i < 3; layer_i++) {
      centre_y += static_cast<int>( stave_length/14.0 * (1. + layer_sep_factor) * a_direction);//new centre_y position considering the staves
    //}
    //    std::cout << "Centre y for layer " << layer << ": " << centre_y << std::endl; 

    unsigned int not_drawn_staves = (first_stave < m_IBLstaves / 2 ? first_stave : first_stave - m_IBLstaves/2);
    //assert(n_staves + not_drawn_staves <= m_IBLstaves / 2);
    for (unsigned int stave_i = 0; stave_i < not_drawn_staves; stave_i++){
      angle += angle_step;
    }

    //for loop to draw all staves
    for (unsigned int stave_i = first_stave; stave_i < n_staves + first_stave; stave_i++) {
      std::stringstream full_stave_name;
      unsigned int stave_nr = stave_i;
      float width_2 = std::abs(cos(angle * M_PI / 180.) * stave_width_scale);
      float ypos = centre_y + (1. - std::abs(cos(angle * M_PI / 180.))) * bend * m_IBL_scale * a_direction;
      //      std::cout << "Bottom y for layer " << layer << " stave " << stave_i << ": " << ypos << std::endl;
      unsigned int start_mod = 1;
      unsigned int mod_number = 8;

      //stave name
      full_stave_name << stave_name.str() << std::setw(2) << std::setfill('0') << stave_nr+1 //(stave_nr/2+1) 
		      << "_" << (c_side ? "C" : "A") << "_M";

      for (unsigned int mod_i = n_mods+1; mod_i > start_mod; mod_i--) {
	std::stringstream mod_name;
	int mod_n = mod_i;
        int half_mod_n = mod_n/2;//modulo thus directly considered
	mod_n--;
	//mod_name << full_stave_name.str() << mod_n;
	mod_name << full_stave_name.str() << half_mod_n;
	if (mod_n>0) {
	  mod_name << (c_side ? "_C" : "_A");
	}
	mod_name << mod_n;
	//std::cout << "in detector.cxx module nr : " << mod_n << " with module : " << mod_name.str() << std::endl;

	ypos += mod_length * a_direction;
	//	std::cout << "Bottom y for layer " << layer << " stave " << stave_i << " module " << mod_n << ": " << ypos << std::endl;
	PixCon::ConnItem *mod_item=m_factory->barrelModuleItem(mod_name.str(),
							    1,//layer, so far layer = 1,2,3
                                QRect(static_cast<int>(xpos), static_cast<int>(ypos),
                                      stave_width, static_cast<int>((mod_length + 1.) * a_direction)));
    if (ypos > max_ypos) {
	  max_ypos = static_cast<int>(ypos);
	}
	if (xpos + width_2 > max_xpos) {
	  max_xpos = static_cast<int>(xpos + width_2);
	}
    m_canvas->addItem(dynamic_cast<QGraphicsRectItem*>(mod_item));
    m_canvas_view->addModule(mod_item);
      }
      xpos += stave_width+stave_gap;
      angle += angle_step;
      ypos -= mod_number * mod_length * a_direction;
    }

    dimension.setWidth(static_cast<int>(max_xpos-xpos0));
    dimension.setHeight(static_cast<int>(max_ypos-ypos0));
    return dimension;

  }


 //function which serves to add IBL DBM barrel item
  QRect Detector::addIBLDBMBarrelItems(bool c_side,
				 bool left_part,
				 unsigned int first_module,
				 int xpos0,
				 int ypos0,
				 int width,
				 int height)
  {
    QRect dimension;
    dimension.setX(xpos0);//set left end
    dimension.setY(ypos0);//set top position
    
    int max_xpos=xpos0;
    int max_ypos=ypos0;

    //assert(layer<m_layers);//if statement is false, terminate program execution, m_layers = 3
    //float layer_sep_factor = .1;
    //float stave_sep_factor = .3;
    //float bend_factor = .5;

    float size = width / 2. - m_barrel_margin;//m_barrel_margin = 20
    //float proj_width;
    //float bend = size * bend_factor;

/*
    //for (unsigned int layer_i=0; layer_i<m_layers; layer_i++) {
      assert (m_IBLstaves%2==0);
      float angle=-max_angle;
      float angle_step=max_angle/(m_IBLstaves/4.);
      angle += angle_step / 2.;
      float total_width = 0;
      for (unsigned int stave_i=0; stave_i<m_IBLstaves/2; stave_i++) {
	total_width += std::abs(cos(angle * M_PI / 180.));
//   	std::cout << "INFO [Detector::addBarrelItems] " 
//   		  << " sum = " << total_width
//   		  << " angle = " << angle 
//		  << std::endl;
	angle += angle_step;
      }
      proj_width = total_width;
//       std::cout << "INFO [Detector::addBarrelItems] layer " << layer_i
//   		<< " quarter width = " << total_width
//   		<< std::endl;
    //}
    float stave_width_scale = 0.6 * size / proj_width;
    //std::cout << "Stave width: " << width << std::endl;
*/   
    int centre_x = static_cast<int>(xpos0 + size + m_barrel_margin);
    //float barrel_length = height - m_y_margin;//m_y_margin = 10; height as input parameter
    int centre_y = static_cast<int>(ypos0);// + (c_side ? barrel_length + m_y_margin : 0));//position depends on whether side A or B is considered
    //    std::cout << "Ypos0: " << ypos0 << "  centre y: " << centre_y << std::endl; 

    std::stringstream stave_name;
    stave_name << "LI";
    stave_name << "_S";

    //float stave_length = 0.8*barrel_length; //- ((1 - std::abs(cos(max_angle*M_PI/180.))) * bend * m_IBL_scale ))/(m_layers + (m_layers - 1) * layer_sep_factor); //= barrel_length*0.9;
    float stave_length = 0.25*width;
    //    std::cout << "Barrel length: " << barrel_length << "  extra bending: " << (1 - std::abs(cos(max_angle*M_PI/180.))) * bend * scale[0] << "  stave length: " << stave_length << std::endl;

    unsigned int n_mods = 6; //(c_side ? 6 :7 );
    float mod_length = stave_length / 8;//length of one module
    float ypos = centre_y; 
    float xpos = centre_x - stave_length/2 - m_barrel_margin/2 - 12;// - 6*stave_width_scale/2; //- size*0.3; // * proj_width[layer] / proj_width[m_layers - 1];
/* 
    angle = -max_angle;
    angle_step= max_angle / (m_IBLstaves / 4.);
    angle += angle_step / 2.;*/
    //    if (c_side) {
    //      angle=-angle;
    //      angle_step=-angle_step;
    //    }

    float a_direction = (c_side ? -1 : 1);
    //for (unsigned int layer_i = layer + 1; layer_i < 3; layer_i++) {
      //centre_y += static_cast<int>( stave_length/14.0 * (1. + layer_sep_factor) * a_direction);//new centre_y position considering the staves
    //}
    //    std::cout << "Centre y for layer " << layer << ": " << centre_y << std::endl; 
/*
    unsigned int not_drawn_staves = (first_stave < m_IBLstaves / 2 ? first_stave : first_stave - m_IBLstaves/2);
    //assert(n_staves + not_drawn_staves <= m_IBLstaves / 2);
    for (unsigned int stave_i = 0; stave_i < not_drawn_staves; stave_i++){
      angle += angle_step;
    }
*/
    //for loop to draw all staves
//    for (unsigned int stave_i = first_stave; stave_i < n_staves + first_stave; stave_i++) {
      std::stringstream full_stave_name;
      unsigned int stave_nr = 15;
//      float width = std::abs(cos(angle * M_PI / 180.) * stave_width_scale);
//      float ypos = centre_y + (1. - std::abs(cos(angle * M_PI / 180.))) * bend * m_IBL_scale * a_direction;
      //      std::cout << "Bottom y for layer " << layer << " stave " << stave_i << ": " << ypos << std::endl;
      unsigned int start_mod = first_module;
      unsigned int mod_number = 6;
//      if (c_side) {
//	start_mod = (stave_nr%2==0 ? 0 : 1);
//	mod_number = (stave_nr%2==0 ? 7 : 6);
//      }
//      else {
//	start_mod = (stave_nr%2==1 ? 0 : 1);
//	mod_number = (stave_nr%2==1 ? 7 : 6);
//      }

      //stave name
      full_stave_name << stave_name.str() << std::setw(2) << std::setfill('0') << stave_nr 
		      << "_" << (c_side ? "C" : "A") 
		      << (left_part ? "_12" : "_34") << "_M";
		      //<< "_" << (c_side ? "C" : "A") << "_M";
      //      ypos += a_length * static_cast<int>(n_mods);
//        std::cout << "INFO [Detector::addBarrelItems] " << full_stave_name.str()
//  		<< " pos= " << xpos << ", " << ypos
//  		<< " dimension = " << width << "x" << a_length
//  		<< " angle = " << angle
//  		<< std::endl;


      for (unsigned int mod_i = start_mod ; mod_i < n_mods+start_mod ; mod_i++) {
	std::stringstream mod_name;
	int mod_n = mod_i;
        //int half_mod_n = mod_n/2;//modulo thus directly considered
	int half_mod_n = (mod_i+2)/3;//modulo thus directly considered
	//mod_name << full_stave_name.str() << mod_n;
	mod_name << full_stave_name.str() << half_mod_n;
	if (mod_n>0) {
	  mod_name << (c_side ? "_C" : "_A");
	}
	mod_name << mod_n;

	//ypos += mod_length * a_direction;
	if (mod_n == 4 || mod_n == 10) xpos += mod_length + 20;
	else xpos += mod_length + 4; // correction to get distance between single modules
	//std::cout << "Bottom y for layer " << layer << " stave " << stave_i << " module " << mod_n << ": " << ypos << std::endl;
	PixCon::ConnItem *mod_item=m_factory->barrelModuleItem(mod_name.str(),
							    1,//layer, so far layer = 1,2,3
							    QRect(static_cast<int>(xpos),
								  static_cast<int>(ypos),
								  static_cast<int>(mod_length ),
                                  static_cast<int>(mod_length )));
	
	if (ypos > max_ypos) {
	  max_ypos = static_cast<int>(ypos);
	}
	if (xpos + width > max_xpos) {
	  max_xpos = static_cast<int>(xpos + width);
	}
    m_canvas->addItem(dynamic_cast<QGraphicsRectItem*>(mod_item));
    m_canvas_view->addModule(mod_item);
      }
      xpos += width;
      //angle += angle_step;
      ypos -= mod_number * mod_length * a_direction;
    //}

    dimension.setWidth(static_cast<int>(max_xpos-xpos0));
    dimension.setHeight(static_cast<int>(max_ypos-ypos0));
    return dimension;

  }

  void Detector::drawTitle(int xpos0,
			   int ypos0,
			   int title_box_width,
			   int title_box_height,
			   const std::string &title,
			   const std::string &sub_title)
  {

    float sub_title_sep = 0.05;
    //     float title_margin_width = 0.02;
    //     float title_margin_height = 0.02;
    int fontsize_sub_title = 16;
    int fontsize_title = 18;

    QFont title_font;
    QFont sub_title_font(title_font);
    title_font.setPointSize(fontsize_title);
    title_font.setBold(true);

    QGraphicsTextItem *title_label = m_canvas_view->title(title);
    QGraphicsTextItem *sub_title_label = m_canvas_view->subTitle(title);

    title_label->setFont(title_font);

    QRectF title_label_size=title_label->boundingRect();
    if (title_label_size.width()>title_box_width) {
      title_font.setPointSize(fontsize_sub_title);
      title_label->setFont(title_font);
      title_label_size=title_label->boundingRect();
      fontsize_sub_title=8;
    }
    title_label->show();

    sub_title_font.setPointSize(fontsize_sub_title);

    if (!sub_title_label) {
      sub_title_label = new QGraphicsTextItem(sub_title.c_str());
      m_canvas->addItem(sub_title_label);
    }
    else {
      sub_title_label->setPlainText(sub_title.c_str());
    }
    sub_title_label->setFont(sub_title_font);
    QRectF sub_title_label_size=sub_title_label->boundingRect();

    int ypos = ypos0 + static_cast<int>((title_box_height - title_label_size.height() - sub_title_sep - sub_title_label_size.height())/2);
    title_label->setY(ypos );
    sub_title_label->setY( static_cast<int>( title_label->y() +  sub_title_sep + title_label_size.height()) );

    float max_width = ( sub_title_label_size.width() > title_label_size.width() ? sub_title_label_size.width() : title_label_size.width() );
    title_label->setX(xpos0 + (title_box_width - max_width) / 2);
    sub_title_label->setX(xpos0 + (title_box_width - max_width) / 2);
    title_label->show();
    sub_title_label->show();
  }

  void Detector::resizeView() {
      auto canvas = m_canvas_view->scene();
      auto boundingBox = canvas->itemsBoundingRect();
      boundingBox.setHeight( boundingBox.height()+ m_bottom_extra_space );
      boundingBox.setWidth(830);
      canvas->setSceneRect(boundingBox);
  }

  void Detector::ConnectionItems()
  {
  }


}
