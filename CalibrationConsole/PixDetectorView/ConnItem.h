#ifndef _PixCon_ConnItem_h_
#define _PixCon_ConnItem_h_

#include <string>
#include <iostream>
#include <memory>
#include <cassert>

#include "IItemStyle.h"

#include <qpen.h>
#include <qbrush.h>

#include <map>
#include "ConnItemBase.h"
class QGraphicsItem;

namespace PixCon {

  /** Base class of connectivity items.
   */
  class ConnItem
  {
  public:

    //@todo agree on nomenclatura with @ref ConnListItem.
    //    enum EConnItemType {kModule, kPP0, kROD};

    /** Create a connectivity canvas item.
     * @param name the unique connectivity name of the item
     * @param style valid pointer to a class which sets the style of the item.
     * NOTE: the class takes owenership of the given style. 
     */
    ConnItem(const std::string &name, IItemStyle *style, EConnItemType item_type)
      : m_name(name),
	m_style(style),
	m_itemType(item_type),
	m_selected(false),
	m_unfocused(false)
    { assert(style);  }

    virtual ~ConnItem() {}

    /** Verify whether the style of the item has changed and update item if needed.
     * @return true if the item needs to be updated
     */
    virtual bool updateStyle() = 0;

    /** Return if the item is marked as selected.
     */
    bool isItemSelected() const {return m_selected;}

    /** Mark the item as selected.
     * Selected items have different styles.
     */
    bool select() {
      m_selected=true;
      return updateStyle();
    }

    /** Remove the selected-mark.
     */
    bool deselect() {
      m_selected=false;
      return updateStyle();
    }

    /*
     */
    bool isUnfocused() const {return m_unfocused;}

    /** Mark the item as selected.
     * Selected items have different styles.
     */
    bool focus() {
      m_unfocused=false;
      return updateStyle();
    }

    /** Remove the selected-mark.
     */
    bool unfocus() {
      m_unfocused=true;
      return updateStyle();
    }

    /** Return the underlying qcanvas item.
     */
    virtual QGraphicsItem *canvasItem()  = 0;

    /** Return the connectivity name of the item
     */
    const std::string &name() const { return m_name; }

    EConnItemType type() const      { return m_itemType; }

    bool isRodItem() {
      return m_itemType == kRodItem;
    }

    bool isModuleItem() {
      return m_itemType == kModuleItem;
    }

    bool isPp0Item() {
      return m_itemType == kPp0Item;
    }

    bool isCrateItem() {
      return m_itemType == kCrateItem;
    }

  protected:

    /** Return the pen and the brush for the current style.
     */
    std::pair<QBrush , QPen > brushAndPen() {
      return m_style->brushAndPen(m_selected,m_unfocused);
    }

  private:

    std::string               m_name;
    std::unique_ptr<IItemStyle> m_style;
    EConnItemType             m_itemType;
    bool                      m_selected;
    bool                      m_unfocused;

  };

}  
#endif
