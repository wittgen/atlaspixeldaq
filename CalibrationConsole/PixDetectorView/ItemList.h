#ifndef _PixCon_ItemList_h_
#define _PixCon_ItemList_h_

#include <vector>

#include <QGraphicsItem>

namespace PixCon {

  class ItemList : public std::vector< QGraphicsItem *>
  {
  public:
    ~ItemList() { clear() ; }

    void clear();

  };
}
#endif


