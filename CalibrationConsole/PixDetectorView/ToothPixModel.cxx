#include <memory>



#include "ToothPixModel.h"

namespace PixDet {

  ToothPixModel::ToothPixModel(PixCon::IConnItemFactory &factory, const std::shared_ptr<ILogoFactory> &logo_factory,
                               DetectorCanvasView *canvas_view, drawDetectorSections const & shouldDraw )
    : Detector(factory, canvas_view) {
    QGraphicsScene *canvas = canvas_view->scene();
    m_diskHeight = static_cast<int>(canvas->height()/8*.65/.7);
    m_stavesHeight = 30;

    m_zoomSep = static_cast<int>(m_diskHeight * .03);
    m_zoomHeight = 200;
   // canvas->resize(canvas->width(),static_cast<int>(canvas->width()*850/600.));
    //canvas->setSceneRect(0,0,canvas->width(),canvas->width()*850/600.);

    if (shouldDraw.crates) {
        addCrateRow(0, m_titleHeight, canvas->width(), m_cratesHeight, 3, std::vector<std::string>{"Crate 0", "Crate 1"});
        addCrateRow(0, m_titleHeight + m_cratesHeight, canvas->width(), m_cratesHeight, 3, std::vector<std::string>{"Crate 2", "Crate 3", "Crate 4"});
    }

    int detector_start = m_titleHeight + (shouldDraw.crates ? (2*m_cratesHeight + 30) : 30); //30 px margin
    int currentRowHeight = detector_start;

    std::vector<std::pair<int,std::string>> rowLabels;
    auto staveDrawWidth = (canvas->width()-m_leftSideMargin-m_rightSideMargin)/2; //we're plotting two half-staves
    auto rightStaveDrawXPos = canvas->width()-m_rightSideMargin;
    
    if (shouldDraw.barrel) {
        //L0 staves
        addStaveItems(false, 0, 13, m_leftSideMargin, currentRowHeight, staveDrawWidth, m_stavesHeight);
        addStaveItems(true, 0, 13, rightStaveDrawXPos, currentRowHeight, staveDrawWidth , m_stavesHeight);
        rowLabels.push_back(std::make_pair(currentRowHeight, "L0"));
        currentRowHeight += 3.5*m_stavesHeight; //two rows of staves + some distance in between ~2.5

        //L1 staves
        addStaveItems(false, 1, 21, m_leftSideMargin, currentRowHeight, staveDrawWidth, m_stavesHeight);
        addStaveItems(true, 1, 21, rightStaveDrawXPos, currentRowHeight, staveDrawWidth, m_stavesHeight);
        rowLabels.push_back(std::make_pair(currentRowHeight, "L1"));
        currentRowHeight += 3.5*m_stavesHeight;

        //L2 staves
        addStaveItems(false, 2, 28, m_leftSideMargin, currentRowHeight, staveDrawWidth, m_stavesHeight);
        addStaveItems(true, 2, 28, rightStaveDrawXPos, currentRowHeight, staveDrawWidth, m_stavesHeight);
        rowLabels.push_back(std::make_pair(currentRowHeight, "L2"));
        currentRowHeight += 3.5*m_stavesHeight;
    }
    if (shouldDraw.IBL) {
        //LI staves
        addIBLStaveItems(false, 17, m_leftSideMargin, currentRowHeight, staveDrawWidth, m_stavesHeight);
        addIBLStaveItems(true, 17, rightStaveDrawXPos, currentRowHeight, staveDrawWidth, m_stavesHeight);
        rowLabels.push_back(std::make_pair(currentRowHeight, "LI"));
        currentRowHeight += 1.5*m_stavesHeight; //one row and then some space ~1.5
        addIBLStaveItems(false, 18, m_leftSideMargin, currentRowHeight, staveDrawWidth, m_stavesHeight);
        addIBLStaveItems(true, 18, rightStaveDrawXPos, currentRowHeight, staveDrawWidth, m_stavesHeight);
        currentRowHeight += 2*m_stavesHeight;
    }
    if (shouldDraw.DBM) {
        //DBM staves
        addIBLDBMStaveItems(true, 20, rightStaveDrawXPos, currentRowHeight, staveDrawWidth, m_stavesHeight);
        rowLabels.push_back(std::make_pair(currentRowHeight, "DBM"));
        currentRowHeight += 2*m_stavesHeight;
    }
    if (shouldDraw.disks) {
        //LX staves
        addDummyStaveItems(false, "LX_B99_S1_A7", m_leftSideMargin, currentRowHeight, staveDrawWidth, m_stavesHeight);
        addDummyStaveItems(true, "LX_B99_S1_C7", rightStaveDrawXPos, currentRowHeight, staveDrawWidth, m_stavesHeight);
        rowLabels.push_back(std::make_pair(currentRowHeight, "LX"));
        currentRowHeight += 2*m_stavesHeight;
    }

    if (shouldDraw.PP0s ) {
        m_zoomView = std::make_unique<PP0ZoomView>( canvas_view,
                                      QRect(10, currentRowHeight, canvas->width()-80, m_zoomHeight),
                                      logo_factory );
    }
    if (shouldDraw.legend) {
        m_paletteGeometry = QRect(canvas->width()-70+10, currentRowHeight - 20, 70, m_paletteHeight-20);
    } else {
        m_paletteGeometry = QRect(0,0,0,0);
    }
    //Labels
    int label_font_size = 20;
    QFont label_font;
    label_font.setPointSize(label_font_size);
    label_font.setBold(true);
    QGraphicsTextItem *label_side = canvas->addText("A-side");
    label_side->setFont(label_font);

    QRectF label_size = label_side->boundingRect();
    label_side->setX(0);
    label_side->setY(20);
    label_side->show();
    
    label_side = canvas->addText("C-side");
    label_side->setFont(label_font);
    label_size = label_side->boundingRect();
    label_side->setX(canvas->width() - label_size.width());
    label_side->setY(20);
    label_side->show();

    label_font_size = 12;

    int label_xpos = canvas->width()/60;

    label_font.setPointSize(label_font_size);
    QColor layer_colour(140,140,140);
    QGraphicsTextItem *label_layer;

    for(auto& rL : rowLabels) {
      label_layer = canvas->addText(rL.second.c_str());
      label_layer->setFont(label_font);
      label_layer->setDefaultTextColor(layer_colour);
      label_size = label_layer->boundingRect();
      label_layer->setX(label_xpos);
      label_layer->setY(rL.first);
      label_layer->show();
    }
    resizeView();
  }

  void ToothPixModel::showPP0Zoom(PixCon::IConnItemFactory &factory,
				      DetectorCanvasView *canvas_view,
				      const PixA::Pp0List::const_iterator &pp0_begin,
				      const PixA::Pp0List::const_iterator &pp0_end) {
      if (m_zoomView) {
          m_zoomView->drawPp0sOfRod(factory, canvas_view, pp0_begin, pp0_end);
      }
      resizeView();
  }

  void ToothPixModel::updateTitle(const std::string &title, const std::string &sub_title)
  {
    int title_margin_height=m_zoomSep;
    int title_margin_width=15;
    drawTitle(title_margin_width, title_margin_height , width()- title_margin_width*2, m_titleHeight-title_margin_height*2, title, sub_title);
    resizeView();
  }

}

