#ifndef _ToothPixModel_H_
#define _ToothPixModel_H_

#include "Detector.h"
#include "DetectorCanvasView.h"
#include "PP0ZoomView.h"

namespace PixDet {

  class ToothPixModel : public Detector
  {
  public:
    ToothPixModel(PixCon::IConnItemFactory &factory,  const std::shared_ptr<ILogoFactory> &logo_factory,
                  DetectorCanvasView *canvas_view, drawDetectorSections const & shouldDraw );
    ~ToothPixModel() = default;

    void showPP0Zoom(PixCon::IConnItemFactory &factory,
		     DetectorCanvasView *canvas_view,
		     const PixA::Pp0List::const_iterator &pp0_begin,
		     const PixA::Pp0List::const_iterator &pp0_end)  ;

    void updateTitle(const std::string &title, const std::string &sub_title);

  protected:
  private:
    
    int m_zoomSep;
    int m_diskHeight;
    int m_stavesHeight;
    
    //These margins are for the section where the (half) staves are drawn
    //on the left side we also add the labels, so we need a larger margin
    int m_leftSideMargin = 45;
    int m_rightSideMargin = 5;
  };

}
#endif
