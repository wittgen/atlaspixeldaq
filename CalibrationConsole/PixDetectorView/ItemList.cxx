#include "ItemList.h"

namespace PixCon {

    void ItemList::clear() {

      for( std::vector<QGraphicsItem *>::iterator item_iter = begin();
	   item_iter != end();
	   item_iter++) {
	delete *item_iter;
      }
      std::vector<QGraphicsItem *>::clear();
    }

}

