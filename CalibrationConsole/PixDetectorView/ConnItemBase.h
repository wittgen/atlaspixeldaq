#ifndef _ConnItemBase_h_
#define _ConnItemBase_h_

namespace PixCon {

    enum EConnItemType {kModuleItem,
			kPp0Item,
			kRodItem,
			kCrateItem,
			kTimItem,
			kNConnItemTypes,
			kNHistogramConnItemTypes = kCrateItem /* should be kRodItem+1 */};

}
#endif
