#ifndef _PixCon_Pp0Item_h_
#define _PixCon_Pp0Item_h_

#include <QGraphicsScene>
#include <string>
#include "IItemStyle.h"
#include "RodItem.h"

namespace PixCon {

  class Pp0Item : public SlotItem
  {
  public:
    class DummyStyle : public IItemStyle
    {
    public:
      DummyStyle(const QBrush &brush, const QPen &pen) : m_brush(brush), m_pen(pen) {}

      std::pair<QBrush , QPen > brushAndPen(bool /*is_selected*/, bool /*is_unfocused*/) {
	return std::make_pair(m_brush,m_pen);
      }

    private:
      QBrush m_brush;
      QPen m_pen;
    };

    Pp0Item(const std::string &name, const QRectF &rect)
      : SlotItem(name,new DummyStyle(QBrush(), QPen(Qt::white)),rect, kPp0Item)
    {
       std::pair<QBrush , QPen > style = brushAndPen();

      if (style.first != brush() ) {
	setBrush(style.first);
      }

      if (style.second != pen() )  {
	setPen(style.second);
      }

    }

    bool updateStyle() {
      return false;
    }

  };

}
#endif
