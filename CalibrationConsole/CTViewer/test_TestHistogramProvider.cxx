#include "TestHistogramProvider.h"
#include "HistogramCache.h"
#include "CalibrationDataManager.h"
#include <iostream>
#include <memory>


int main(int argc, char **argv)
{
  std::shared_ptr<PixCon::CalibrationDataManager> calibration_data(new PixCon::CalibrationDataManager);
  std::shared_ptr<PixCon::CategoryList> categories(new PixCon::CategoryList);
  std::unique_ptr<PixCon::TestHistogramProvider> test_histo_server(new PixCon::TestHistogramProvider(calibration_data, categories,0,0));
  std::vector<unsigned int> serial_number;
  std::vector<std::string> conn_names;
  std::vector<std::string> pattern;
  bool error = false;
  for (int arg_i=1; arg_i<argc; arg_i++) {
    if (strcmp(argv[arg_i],"-i")==0 && arg_i+1 < argc && argv[arg_i+1][0]!='-') {
      test_histo_server->addFile(argv[++arg_i], pattern);
    }
    else if (strcmp(argv[arg_i],"-s")==0 && arg_i+1 < argc && isdigit(argv[arg_i+1][0])) {
      serial_number.push_back(atoi(argv[++arg_i]));
    }
    else if (strcmp(argv[arg_i],"-m")==0 && arg_i+1 < argc && isalpha(argv[arg_i+1][0])) {
      conn_names.push_back(argv[++arg_i]);
    }
    else {
      error = true;
    }
  }
  if (error) {
    std::cout << "usage: " << argv[0] << " [-i \"input root file\"] [-s serial-number] [-m module-name] " << std::endl
	      << std::endl
	      << "-i, -s and -m can be repeated as often as needed. So can the serial number,"
	      << std::endl;
    return -1;
  }

//   PixCon::HistogramCache histo_cache(test_histo_server.release());

//   std::vector<std::string> histo_list;
//   for (std::vector<unsigned int>::const_iterator serial_iter = serial_number.begin();
//        serial_iter != serial_number.end();
//        serial_iter++) {
//     //histo_list.clear();
//     const std::vector<std::string> &histo_list = histo_cache.histogramList(*serial_iter);
//     std::cout << "INFO [main:" << argv[0] << "]" << *serial_iter << " histogram list : " << std::endl;
//     for (std::vector<std::string>::const_iterator histo_iter=histo_list.begin();
// 	 histo_iter != histo_list.end();
// 	 histo_iter++) {
//       PixCon::Index_t index;
//       std::cout << "  " << *histo_iter << std::endl;

//       for (std::vector<std::string>::const_iterator module_iter=conn_names.begin();
// 	   module_iter != conn_names.end();
// 	   module_iter++) {
// 	std::cout << "  " << *module_iter << std::endl;
// 	unsigned int histo_id = histo_cache.histogramIndex(*serial_iter, *histo_iter);
// 	histo_cache.numberOfHistograms(*serial_iter, *module_iter, histo_id, index);
// 	for (PixCon::Index_t::const_iterator index_iter = index.begin();
// 	     index_iter !=  index.end();
// 	     index_iter++) {
// 	  std::cout << *index_iter << " ";
// 	}
// 	std::cout << std::endl;
//       }
//     }
//   }
}
