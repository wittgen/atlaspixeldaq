#ifndef _PixCon_CTLogoFactory_h_
#define _PixCon_CTLogoFactory_h_

#include "LogoFactory.h"

class DetectorCanvasView;

namespace PixCon {

  class CTLogoFactory : public PixDet::LogoFactory
  {
  public:
    CTLogoFactory();

    void drawLogo(DetectorCanvasView *canvas_view, const QRectF &pos);
    const QFont &defaultFont() const {return m_defaultFont; }

  private:
    enum EImage {kDetector, kController, kNImages};

   // unsigned int m_imageKey[kNImages];
    QFont m_defaultFont;
  };

}
#endif
