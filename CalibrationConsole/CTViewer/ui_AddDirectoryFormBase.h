#ifndef UI_ADDDIRECTORYFORMBASE_H
#define UI_ADDDIRECTORYFORMBASE_H

#include <QHeaderView>
#include <QGroupBox>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QDialog>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QSpacerItem>
#include <QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_AddDirectoryFormBase
{
public:
    QVBoxLayout *vboxLayout;
    QHBoxLayout *hboxLayout;
    QLabel *textLabel1;
    QLineEdit *m_directoryPath;
    QPushButton *m_browseButton;
    QGroupBox *groupBox1;
    QHBoxLayout *hboxLayout1;
    QTreeWidget *m_patternList;
    QTreeWidgetItem *m_patternListHeader;
    QVBoxLayout *vboxLayout1;
    QPushButton *m_newPatternButton;
    QPushButton *m_deletePatternButton;
    QPushButton *m_clearPatternListButton;
    QSpacerItem *spacer1;
    QHBoxLayout *hboxLayout2;
    QSpacerItem *spacer4;
    QPushButton *m_cancelButton;
    QSpacerItem *spacer6;
    QPushButton *m_okayButton;
    QSpacerItem *spacer5;

    void setupUi(QDialog *AddDirectoryFormBase)
    {
        if (AddDirectoryFormBase->objectName().isEmpty())
            AddDirectoryFormBase->setObjectName(QString::fromUtf8("AddDirectoryFormBase"));
        AddDirectoryFormBase->resize(600, 290);
        vboxLayout = new QVBoxLayout(AddDirectoryFormBase);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        textLabel1 = new QLabel(AddDirectoryFormBase);
        textLabel1->setObjectName(QString::fromUtf8("textLabel1"));
        textLabel1->setWordWrap(false);

        hboxLayout->addWidget(textLabel1);

        m_directoryPath = new QLineEdit(AddDirectoryFormBase);
        m_directoryPath->setObjectName(QString::fromUtf8("m_directoryPath"));

        hboxLayout->addWidget(m_directoryPath);

        m_browseButton = new QPushButton(AddDirectoryFormBase);
        m_browseButton->setObjectName(QString::fromUtf8("m_browseButton"));

        hboxLayout->addWidget(m_browseButton);


        vboxLayout->addLayout(hboxLayout);

        groupBox1 = new QGroupBox(AddDirectoryFormBase);
        groupBox1->setObjectName(QString::fromUtf8("groupBox1"));
        //groupBox1->setColumnLayout(0, Qt::Vertical);
	new QVBoxLayout(groupBox1);
	groupBox1->layout()->setSpacing(6);
	groupBox1->layout()->setContentsMargins(11, 11, 11, 11);
        hboxLayout1 = new QHBoxLayout();
        QBoxLayout *boxlayout = qobject_cast<QBoxLayout *>(groupBox1->layout());
        if (boxlayout)
            boxlayout->addLayout(hboxLayout1);
        hboxLayout1->setAlignment(Qt::AlignTop);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        m_patternList = new QTreeWidget(groupBox1);
        m_patternList->setColumnCount(1);
        m_patternListHeader =  new QTreeWidgetItem;
        m_patternListHeader->setText(0,"Patterns");
        m_patternList->header()->resizeSections(QHeaderView::Fixed);
#if QT_VERSION < 0x050000
        m_patternList->header()->setClickable(true);
#else
        m_patternList->header()->setSectionsClickable(true);
#endif
        m_patternList->setObjectName(QString::fromUtf8("m_patternList"));
        m_patternList->setHeaderItem(m_patternListHeader);

        hboxLayout1->addWidget(m_patternList);

        vboxLayout1 = new QVBoxLayout();
        vboxLayout1->setSpacing(6);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        m_newPatternButton = new QPushButton(groupBox1);
        m_newPatternButton->setObjectName(QString::fromUtf8("m_newPatternButton"));

        vboxLayout1->addWidget(m_newPatternButton);

        m_deletePatternButton = new QPushButton(groupBox1);
        m_deletePatternButton->setObjectName(QString::fromUtf8("m_deletePatternButton"));

        vboxLayout1->addWidget(m_deletePatternButton);

        m_clearPatternListButton = new QPushButton(groupBox1);
        m_clearPatternListButton->setObjectName(QString::fromUtf8("m_clearPatternListButton"));

        vboxLayout1->addWidget(m_clearPatternListButton);

        spacer1 = new QSpacerItem(20, 30, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout1->addItem(spacer1);


        hboxLayout1->addLayout(vboxLayout1);


        vboxLayout->addWidget(groupBox1);

        hboxLayout2 = new QHBoxLayout();
        hboxLayout2->setSpacing(6);
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        spacer4 = new QSpacerItem(61, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout2->addItem(spacer4);

        m_cancelButton = new QPushButton(AddDirectoryFormBase);
        m_cancelButton->setObjectName(QString::fromUtf8("m_cancelButton"));

        hboxLayout2->addWidget(m_cancelButton);

        spacer6 = new QSpacerItem(51, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout2->addItem(spacer6);

        m_okayButton = new QPushButton(AddDirectoryFormBase);
        m_okayButton->setObjectName(QString::fromUtf8("m_okayButton"));
        m_okayButton->setDefault(true);

        hboxLayout2->addWidget(m_okayButton);

        spacer5 = new QSpacerItem(111, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout2->addItem(spacer5);


        vboxLayout->addLayout(hboxLayout2);


        retranslateUi(AddDirectoryFormBase);
        QObject::connect(m_clearPatternListButton, SIGNAL(clicked()), m_patternList, SLOT(clear()));
        QObject::connect(m_cancelButton, SIGNAL(clicked()), AddDirectoryFormBase, SLOT(reject()));
        QObject::connect(m_okayButton, SIGNAL(clicked()), AddDirectoryFormBase, SLOT(verifyDirectoryNameAndAccept()));
        QObject::connect(m_deletePatternButton, SIGNAL(clicked()), AddDirectoryFormBase, SLOT(deletePattern()));
        QObject::connect(m_browseButton, SIGNAL(clicked()), AddDirectoryFormBase, SLOT(openDirectoryBrowser()));
        QObject::connect(m_newPatternButton, SIGNAL(clicked()), AddDirectoryFormBase, SLOT(addPattern()));

        QMetaObject::connectSlotsByName(AddDirectoryFormBase);
    } // setupUi

    void retranslateUi(QDialog *AddDirectoryFormBase)
    {
      AddDirectoryFormBase->setWindowTitle(QApplication::translate("AddDirectoryFormBase", "Add Directory", 0));
      textLabel1->setText(QApplication::translate("AddDirectoryFormBase", "Directory :", 0));
      m_browseButton->setText(QApplication::translate("AddDirectoryFormBase", "Browse", 0));
      groupBox1->setTitle(QApplication::translate("AddDirectoryFormBase", "Scan Selection Patterns", 0));
      m_newPatternButton->setText(QApplication::translate("AddDirectoryFormBase", "Add", 0));
      m_deletePatternButton->setText(QApplication::translate("AddDirectoryFormBase", "Delete", 0));
      m_clearPatternListButton->setText(QApplication::translate("AddDirectoryFormBase", "Clear", 0));
      m_cancelButton->setText(QApplication::translate("AddDirectoryFormBase", "Cancel", 0));
      m_okayButton->setText(QApplication::translate("AddDirectoryFormBase", "Okay", 0));
    } // retranslateUi

};

namespace Ui {
    class AddDirectoryFormBase: public Ui_AddDirectoryFormBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADDDIRECTORYFORMBASE_H
