// Dear emacs, this is -*-c++-*-
#ifndef _CTViewerApp_H_
#define _CTViewerApp_H_

#  if defined(HAVE_QTGSI)
#    include "TQRootApplication.h"
#    include "TQApplication.h"

typedef TQRootApplication QRootApplication;

#  else
#include <QApplication>

// dummy implementation
// no ROOT - histograms will not be displayed.
class QRootApplication : public QApplication
{
public:
  QRootApplication(int &argc, char **argv) : QApplication(argc, argv) {}
  QRootApplication(int &argc, char **argv,int) : QApplication(argc, argv) {}
};

#  endif


#include <vector>
#include <string>

#include <memory>

#include <Lock.h>
#include "UserMode.h"

namespace PixCon {
  class TestHistogramProvider;
  class CategoryList;
  class CalibrationDataManager;
  class CTViewer;
}


class CTViewerApp : public QRootApplication
{
public:
  CTViewerApp(int &qt_argc, char **qt_argv, std::shared_ptr<PixCon::UserMode>& user_mode, const std::vector<std::string> &ct_files,  const std::vector<std::string> &pattern_list, int poll=0);
  ~CTViewerApp();

  void setTags(const std::string &object_tag_name, const std::string &tag_name, const std::string &cfg_tag_name, const std::string &mod_cfg_tag_name);

protected:
  static unsigned int addCTFiles( PixCon::TestHistogramProvider &histogram_server,
				  const std::vector<std::string> &result_files,
				  const std::vector<std::string> &pattern_list );

  std::shared_ptr<PixCon::CategoryList> m_categories;
  std::shared_ptr<PixCon::CalibrationDataManager> m_calibrationData;

  std::unique_ptr<PixCon::CTViewer> m_viewer;
private:

};
#endif
