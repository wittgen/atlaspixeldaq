#ifndef _PixCon_IFileList_h_
#define _PixCon_IFileList_h_

#include <string>

namespace PixCon {

  class IFileList
  {
  public:
    virtual ~IFileList() {}

    /** Go to the next file name in the list.
     */
    virtual void next() = 0;

    /** Return the current file file name or zero.
     * @return the file name or zero if there are no more file names.
     */
    virtual const std::string fileName() = 0;

    /** Call to associate a problem with the current file.
     */
    virtual void assignProblem(const std::string &problem) = 0;
  };

}

#endif
