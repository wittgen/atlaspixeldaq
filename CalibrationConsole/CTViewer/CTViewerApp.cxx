#include "CTViewerApp.h"

#include <CalibrationDataManager.h>
#include <CalibrationDataStyle.h>
#include <CalibrationConsoleContextMenu.h>
#include <CategoryList.h>

#include "TestHistogramProvider.h"
#include <HistogramProviderInterceptor.h>
#include <HistogramCache.h>

#include "CTViewer.h"
#include <QFileInfo>
#include <QDir>
#include "FileList.h"


CTViewerApp::CTViewerApp(int &qt_argc, char **qt_argv,
			 std::shared_ptr<PixCon::UserMode>& user_mode,
			 const std::vector<std::string> &ct_files,
			 const std::vector<std::string> &pattern_list, int poll)
  : QRootApplication(qt_argc, qt_argv, poll),
    m_categories(new PixCon::CategoryList),
    m_calibrationData(new PixCon::CalibrationDataManager)
{
  std::unique_ptr<PixCon::TestHistogramProvider> ct_histogram_server( new PixCon::TestHistogramProvider(m_calibrationData, m_categories,0,0) );

  std::shared_ptr<PixCon::CalibrationDataPalette> palette(new PixCon::CalibrationDataPalette);
  palette->setDefaultPalette();

  addCTFiles(*ct_histogram_server,ct_files, pattern_list);

  PixCon::TestHistogramProvider *ct_histogram_server_ptr = ct_histogram_server.get();
  std::shared_ptr<PixCon::HistogramCache> histogram_cache( new PixCon::HistogramCache(std::make_shared<PixCon::HistogramProviderInterceptor>(ct_histogram_server.release())) );
  std::shared_ptr<PixCon::VisualiserList> visualiser_list(new PixCon::VisualiserList);
  std::shared_ptr<PixCon::IContextMenu> context_menu(  new PixCon::CalibrationConsoleContextMenu(m_calibrationData, histogram_cache, visualiser_list, std::string("")) );

  m_viewer=std::make_unique<PixCon::CTViewer>( user_mode, 
								  m_calibrationData,
								  *ct_histogram_server_ptr,
								  histogram_cache,
								  palette,
								  m_categories,
								  context_menu,
								  visualiser_list,
								  *this );

  m_viewer->updateCategories(m_categories);

    //    detector_view_window.updateCategories(categories);
    //    detector_view_window.addCurrentSerialNumber();

  m_viewer->show();
  connect( this, SIGNAL( lastWindowClosed() ), this, SLOT( quit() ) );

}

CTViewerApp::~CTViewerApp() {}

void CTViewerApp::setTags(const std::string &object_tag_name, const std::string &tag_name, const std::string &cfg_tag_name, const std::string &mod_cfg_tag_name)
{
  if (!object_tag_name.empty() && !tag_name.empty() && !cfg_tag_name.empty()) {
    m_viewer->changeConnectivity(object_tag_name, tag_name,tag_name,tag_name,cfg_tag_name, mod_cfg_tag_name, 0);
  }
}


class FileDoesNotExistAndRemberDirectoryContamination
{
public:
  FileDoesNotExistAndRemberDirectoryContamination() : m_containsDir(false) {}

  bool operator()(const std::string &a) {
    if (!a.empty()) {
      QFileInfo file_info(QString::fromStdString(a));
      if (!file_info.exists()) {
	std::cout << "WARNING [CTViewerApp::addCTFiles] Skip file " <<  a << " because it does not seem to exist." << std::endl;
	return true;
      }
      else if (file_info.isDir()) {
	m_containsDir=true;
      }
    }
    return false;
  }

  bool containsDirectories() const { return m_containsDir; }

private:
  bool m_containsDir;
};

unsigned int CTViewerApp::addCTFiles( PixCon::TestHistogramProvider &histogram_server,
				      const std::vector<std::string> &result_files,
				      const std::vector<std::string> &pattern_list)
{
  std::vector<std::string> temp_result_files = result_files;
  unsigned int serial_number = 1;

  FileDoesNotExistAndRemberDirectoryContamination file_does_not_exist_and_remeber_dir_conatmination;
  temp_result_files.erase(remove_if(temp_result_files.begin(),
				    temp_result_files.end(),
				    file_does_not_exist_and_remeber_dir_conatmination), 
			  temp_result_files.end());

  for (std::vector<std::string>::const_iterator iter = temp_result_files.begin();
       iter != temp_result_files.end();
       iter++) {
    std::cout << "INFO [CTViewerApp::addCTFiles] file " << *iter << " survived." << std::endl;
  }
  if (file_does_not_exist_and_remeber_dir_conatmination.containsDirectories() ) {
    std::cout << "INFO [CTViewerApp::addCTFiles] vector also contains directories." << std::endl;
  }


  if (file_does_not_exist_and_remeber_dir_conatmination.containsDirectories() ) {
    for(std::vector<std::string>::const_iterator file_iter = temp_result_files.begin();
	file_iter != temp_result_files.end();
	file_iter++) {
      if (!file_iter->empty()) {
	QFileInfo file_info(QString::fromStdString(*file_iter));
	if (!file_info.exists()) {
	  std::cout << "WARNING [CTViewerApp::addCTFiles] Skip file " <<  *file_iter << " because it does not seem to exist." << std::endl;
	  continue;
	}
	else if (file_info.isDir()) {
	  QDir a_dir( QString::fromStdString(*file_iter),QString::fromStdString( "*.root"), QDir::Time );
	  QStringList entry_list = a_dir.entryList();
	  if (pattern_list.empty() && entry_list.size()>1) {
	    std::cout << "WARNING [CTViewerApp::addCTFiles] Skip directory " << *file_iter 
		      << " because it contains more than one file and no pattern was specified." << std::endl;
	    continue;
	  }
	  PixCon::FileList a_file_list(entry_list,a_dir.absolutePath().toUtf8().constData());
	  serial_number = histogram_server.addFiles(a_file_list, pattern_list);
	}
	else {
	  serial_number = histogram_server.addFile(*file_iter, pattern_list);
	}
	//       if (serial_number>0) {
	// 	for (unsigned int ser_i = 0 ; ser_i < serial_number; ser_i++) {
	// 	  std::vector< std::string > histogram_name_list;
	// 	  for (unsigned int type_i=0; type_i< PixCon::kNHistogramConnItemTypes; type_i++) {
	// 	    histogram_name_list.clear();
	// 	    histogram_server.requestHistogramList( PixCon::kScan, ser_i, static_cast<PixCon::EConnItemType>(type_i), histogram_name_list );
	// 	    if (!histogram_name_list.empty()) {
	// 	      for (std::vector< std::string >::const_iterator iter = histogram_name_list.begin();
	// 		   iter != histogram_name_list.end();
	// 		   iter++) {
	// 		  std::cout << "INFO [CTViewerApp::addCTFiles] " << ser_i << "/" << type_i<< " : " << *iter << std::endl;
	// 	      }
	// 	    }
	// 	  }
	// 	}
	//       }
      }

    }
  
  }
  else {
    serial_number = histogram_server.addFiles(temp_result_files, pattern_list);
  }
  return serial_number;
}
