/****************************************************************************
** Form implementation generated from reading ui file 'EnterPatternFormBase.ui'
**
** Created by: The User Interface Compiler ($Id: qt/main.cpp   3.3.6   edited Aug 31 2005 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "EnterPatternForm.h"

#include <QWhatsThis>

/*
 *  Constructs a EnterPatternFormBase as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
EnterPatternForm::EnterPatternForm( QWidget* parent )
    : QDialog( parent )
{
    setupUi(this);

    EnterPatternFormBaseLayout = new QVBoxLayout( this );

    layout5 = new QHBoxLayout( 0 );

    textLabel2 = new QLabel( this );
    layout5->addWidget( textLabel2 );

    m_patternString = new QLineEdit( this );
    layout5->addWidget( m_patternString );
    EnterPatternFormBaseLayout->addLayout( layout5 );

    layout6 = new QHBoxLayout( 0 );
    spacer2 = new QSpacerItem( 31, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout6->addItem( spacer2 );

    m_cancelButton = new QPushButton( this );
    layout6->addWidget( m_cancelButton );

    m_okayButton = new QPushButton( this );
    m_okayButton->setDefault( true );
    layout6->addWidget( m_okayButton );
    spacer3 = new QSpacerItem( 41, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout6->addItem( spacer3 );
    EnterPatternFormBaseLayout->addLayout( layout6 );
    languageChange();
    resize( QSize(303, 97).expandedTo(minimumSizeHint()) );


    // signals and slots connections
    connect( m_cancelButton, SIGNAL( clicked() ), this, SLOT( reject() ) );
    connect( m_okayButton, SIGNAL( clicked() ), this, SLOT( accept() ) );
}

/*
 *  Destroys the object and frees any allocated resources
 */
EnterPatternForm::~EnterPatternForm()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void EnterPatternForm::languageChange()
{

    
}

