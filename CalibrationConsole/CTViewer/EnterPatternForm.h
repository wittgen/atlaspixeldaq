/****************************************************************************
** Form interface generated from reading ui file 'EnterPatternFormBase.ui'
**
** Created by: The User Interface Compiler ($Id: qt/main.cpp   3.3.6   edited Aug 31 2005 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef ENTERPATTERNFORM_H
#define ENTERPATTERNFORM_H
#include <ui_EnterPatternFormBase.h>
#include <QVariant>
#include <QDialog>
#include <QGridLayout>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QLabel;
class QLineEdit;
class QPushButton;

class EnterPatternForm : public QDialog, public Ui_EnterPatternFormBase
{
    Q_OBJECT

public:
    EnterPatternForm( QWidget* parent = 0 );
    ~EnterPatternForm();

    QLabel* textLabel2;
    QLineEdit* m_patternString;
    QPushButton* m_cancelButton;
    QPushButton* m_okayButton;

protected:
    QVBoxLayout* EnterPatternFormBaseLayout;
    QHBoxLayout* layout5;
    QHBoxLayout* layout6;
    QSpacerItem* spacer2;
    QSpacerItem* spacer3;

protected slots:
    virtual void languageChange();

};

#endif // ENTERPATTERNFORM_H
