#include "CTViewer.h"
#include <QDir>
#include <QFileDialog>
#include <QMessageBox>


#include "AddDirectoryForm.h"

#include <TestHistogramProvider.h>
#include <TemporaryCursorChange.h>

#include "CTLogoFactory.h"

#include "FileList.h"
#include "ProcessingFilesDialog.h"
#include <BusyLoop.h>

namespace PixCon {

  CTViewer::CTViewer( std::shared_ptr<PixCon::UserMode>& user_mode, 
		      const std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data,
		      PixCon::TestHistogramProvider &ct_histogram_server,
		      const std::shared_ptr<PixCon::HistogramCache> &histogram_cache,
		      const std::shared_ptr<PixCon::CalibrationDataPalette> &palette,
		      const std::shared_ptr<PixCon::CategoryList> &categories,
		      const std::shared_ptr<PixCon::IContextMenu> &context_menu,
		      const std::shared_ptr<PixCon::VisualiserList> &visualiser_list,
		      QApplication &application,
		      QWidget* parent)
    : ScanMainPanel(user_mode,
		    calibration_data,
		    histogram_cache,
		    palette,
		    categories,
		    context_menu,
		    std::shared_ptr<IMetaDataUpdateHelper>(),
		    std::shared_ptr<PixDet::ILogoFactory>(new CTLogoFactory),
		    visualiser_list,
		    application,
		    parent) ,
      m_ctHistogramProvider(&ct_histogram_server),
      m_calibrationData(calibration_data)
  {
    setWindowTitle("CT Viewer");
    QAction *menuAct;

    menuAct = new QAction("Open File",this);
    menuAct->setShortcut(Qt::CTRL+Qt::Key_F);
    connect(menuAct, SIGNAL( triggered(bool) ), this, SLOT( openFile() ));
    File->insertAction(m_sepExitAction, menuAct);

    menuAct = new QAction("Open Directory",this);
    menuAct->setShortcut(Qt::CTRL+Qt::Key_D);
    connect(menuAct, SIGNAL( triggered(bool) ), this, SLOT( openDirectory() ));
    File->insertAction(m_sepExitAction, menuAct);

//     unsigned int file_index = 0;
//     File->insertItem("Open File", kFileOpenFile, file_index++);
//     File->connectItem(kFileOpenFile, this, SLOT( openFile() ));

//     File->insertItem("Open Directory", kFileOpenDirectory, file_index++);
//     File->connectItem(kFileOpenDirectory, this, SLOT( openDirectory() ));
  }


  void CTViewer::addFile( const std::string &a_file, const std::vector<std::string> &std_pattern_list)
  {
    QStringList temp_file_list;
    temp_file_list.push_back(QString::fromStdString(a_file));
    QStringList pattern_list;
    for (std::vector<std::string>::const_iterator pattern_iter = std_pattern_list.begin();
	 pattern_iter != std_pattern_list.end();
	 pattern_iter++) {
      pattern_list.push_back( QString::fromStdString(*pattern_iter) );
    }

    addFiles( QString(""),temp_file_list,pattern_list);
  }


  void CTViewer::addFiles( const QString &path, const QStringList &file_list, const QStringList &pattern_list)
  {
    if (file_list.size()>0) {
      SerialNumber_t new_serial_number=0;
      {
	std::unique_ptr<ProcessingFilesDialog> processing_dialog(new ProcessingFilesDialog(*application(),"Processing files",(file_list[0]).toUtf8().constData()));
	ProcessingFilesDialog *processing_dialog_ptr = processing_dialog.get();
	ProcessingFilesDialog::MonitoredFileList a_file_list(*processing_dialog, file_list,path.toUtf8().constData());
	BusyLoop busy(application(), processing_dialog.release() ,1500,100 );

	std::vector<std::string> std_pattern_list;
	for(QStringList::const_iterator pattern_iter = pattern_list.begin();
	    pattern_iter != pattern_list.end();
	    ++pattern_iter) {
	  std_pattern_list.push_back( (*pattern_iter).toLatin1().data());
	}
	try {
	  new_serial_number = m_ctHistogramProvider->addFiles(a_file_list,  std_pattern_list);
	}
	catch (MetaDataExistsAlready &err) {
	  // should be catched by the TestHistogramProvider
	}
	if (a_file_list.hasProblems()) {
	  if (busy.isVetoConnected()) {
	    busy.vetoAbort();
	  }
	}
	processing_dialog_ptr->finished();
      }
      if (new_serial_number>0) {
	updateCatalogue(new_serial_number, new_serial_number );
      }
    }
  }


  void CTViewer::updateCatalogue(SerialNumber_t serial_number_begin, SerialNumber_t serial_number_end)
  {
    updateCategories(m_ctHistogramProvider->categories());
    setSerialNumbers();

    const ScanMetaDataCatalogue &scan_meta_data_catalogue = m_calibrationData->scanMetaDataCatalogue();
    for (unsigned int serial_number_i = serial_number_begin; serial_number_i <= serial_number_end; serial_number_i++) {
      if (scan_meta_data_catalogue.find(serial_number_i)) {

	selectSerialNumber( kScan, serial_number_i);
	break;
      }
    }
  }

  QString CTViewer::defaultPath()
  {
    QString path = m_lastPath;
    // guess initial search path
    if(m_lastPath.length()==0) {
      const char *ct_path = getenv("CT_DATA_PATH");
      if (ct_path) {
	if (strlen(ct_path)>0) {
	  QFileInfo info1(ct_path);
	  if (info1.exists() && info1.isDir()) {
	    path = ct_path;
	  }
	}
      }
      else {
	const char *home_path = getenv("HOME");
	if (home_path && strlen(home_path)>0) {
	  QFileInfo info2(home_path);
	  if (info2.exists() && info2.isDir()) {
	    QString data_path = home_path;
	    if (data_path.length()>0 && data_path[data_path.length()-1]!='/') {
	      data_path +=  '/';
	    }
	    const unsigned int n_dirs = 3;
	    const char *dirs[n_dirs]={"Data","DATA","data"};
	    for (unsigned int dir_i=0; dir_i<n_dirs; dir_i++) {
	      QFileInfo info3(data_path + dirs[dir_i]);
	      if (info3.exists() && info3.isDir()) {
		path = data_path + dirs[dir_i];
		break;
	      }
	    }
	  }
	}
      }
    }
    return path;
  }

  void CTViewer::openFile()
  {
    QString path = QString::null;//defaultPath();

    QStringList filter;
    filter +=  "ROOT data file (*.root)";
    filter += "Any file (*.*)";

    QFileDialog file_chooser(this,"rootdbbrws",path,QString::null);
    file_chooser.setFileMode(QFileDialog::ExistingFiles);
    file_chooser.setNameFilters(filter);
    file_chooser.setWindowTitle("Select RootDb data file");
    if(file_chooser.exec() == QDialog::Accepted) {
      return;
      TemporaryCursorChange busy;
      QStringList file_list = file_chooser.selectedFiles();
      QStringList pattern_list;
      if (file_list.empty() || (file_list.size()==1)) {
	std::unique_ptr<QDir> a_dir;
	if (file_list.size()==1) {
	  QFileInfo info(file_list[0]);
	  if (!info.isDir()) {
	    std::vector<std::string> std_pattern_list;
	    addFile(file_list[0].toLatin1().data(), std_pattern_list );
	  }
	  else {
	    a_dir = std::make_unique<QDir>( file_list[0], "*.root", QDir::Time );
	  }
	}
	else {
	  a_dir = std::make_unique<QDir>(file_chooser.directory().path(), "*.root", QDir::Time );
	}

	if (a_dir.get()) {
	  addFiles( a_dir->absolutePath(), a_dir->entryList(), pattern_list);
	}
      }
      else {
	addFiles("", file_chooser.selectedFiles(), pattern_list );
      }
    }

    // save the last selected directory
    QFileInfo info(file_chooser.directory().path());
    if (info.exists() && info.isDir()) {
      m_lastPath = file_chooser.directory().path();
    }
  }

  void CTViewer::openDirectory()
  {
    for(;;) {
      AddDirectoryForm add_directory(defaultPath().toUtf8().constData(), this);
      for(QStringList::const_iterator pattern_iter = m_lastPatternList.begin();
	  pattern_iter != m_lastPatternList.end();
	  ++pattern_iter) {
	add_directory.addPattern(*pattern_iter);
      }

      if (add_directory.exec() != QDialog::Accepted) {
	return;
      }
      m_lastPath = QString::fromStdString(add_directory.getDirectoryName());

      QStringList pattern_list;
      add_directory.getPatternList(pattern_list);
      if (!pattern_list.empty()) {
	m_lastPatternList = pattern_list;
      }

      QFileInfo info(QString::fromStdString( add_directory.getDirectoryName()));
      if (!info.isDir()) {

	std::vector<std::string> std_pattern_list;
	for(QStringList::const_iterator pattern_iter = pattern_list.begin();
	    pattern_iter != pattern_list.end();
	    ++pattern_iter) {
	  std_pattern_list.push_back( (*pattern_iter).toLatin1().data());
	}

	TemporaryCursorChange busy;
	addFile( add_directory.getDirectoryName(), std_pattern_list );
	return;
      }
      else {
	QDir a_dir( QString::fromStdString(add_directory.getDirectoryName()), "*.root", QDir::Time );
	QStringList entry_list = a_dir.entryList();
	if (pattern_list.empty() && entry_list.size()>1) {
	  QMessageBox continue_without_pattern( "CTViewer : Add Directory",
						"There are multiple entries in the specified directory,\n"
						"but no folder selection pattern was chosen.\n\n"
						"Continue nevertheless?",
						QMessageBox::Question,
						QMessageBox::Yes | QMessageBox::Escape,
						QMessageBox::No  | QMessageBox::Default,
						QMessageBox::NoButton);
	  if ( continue_without_pattern.exec() == QMessageBox::No ) {
	    continue;
	  }
	}

	TemporaryCursorChange busy;
	addFiles( a_dir.absolutePath(), a_dir.entryList(), pattern_list );
	return;
      }

    }
  }

  void CTViewer::removeMeasurement(PixCon::EMeasurementType measurement_type, PixCon::SerialNumber_t serial_number) {
    m_ctHistogramProvider->remove(measurement_type, serial_number);
    ScanMainPanel::removeMeasurement(measurement_type, serial_number);
  }

}
