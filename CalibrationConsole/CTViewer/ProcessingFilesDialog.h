#ifndef _PixCon_ProcessingFilesDialog_h_
#define _PixCon_ProcessingFilesDialog_h_

#include "ui_ProcessingFilesDialogBase.h"
#include "Lock.h"
#include <QTimer>
#include <QEvent>
#include <QApplication>
#include <QLineEdit>

#include "FileList.h"

namespace PixCon {

  class ProcessingFilesDialog : public QDialog, public Ui_ProcessingFilesDialogBase
  {
    Q_OBJECT
  public:

    class NewFileEvent : public QEvent
    {
    public:
      NewFileEvent() : QEvent(User) {}
    };

    class FinishedEvent : public QEvent
    {
    public:
      FinishedEvent() : QEvent(User) {}
    };


    class ProblemEvent : public QEvent
    {
    public:
      ProblemEvent(const std::string &problem, const std::string &file_name) : QEvent(User), m_problem(problem), m_fileName(file_name) {}
      const std::string &problem() const  { return m_problem; }
      const std::string &fileName() const { return m_fileName; }

    private:
      std::string m_problem;
      std::string m_fileName;
    };

    ProcessingFilesDialog( QApplication &app,
			   const std::string &title,
			   const std::string file_name,
			   QWidget* parent = 0 );

    ~ProcessingFilesDialog();
 
//     void updateMessage(const QString &a_message) {
//       m_messages->setText(a_message);
//     }

    void newFile() {
      if (!m_timerStarted) {
	m_timerStarted=true;
	m_app->postEvent(this, new NewFileEvent());
      }
    }

    void reportProblem(const std::string &problem_text, const std::string &file_name) {
      m_app->postEvent(this, new ProblemEvent(problem_text, file_name));
    }

    void finished() {
      m_finished=true;
      if (!m_timerStarted) {
	m_timerStarted=true;
	m_app->postEvent(this, new FinishedEvent);
      }
    }

    bool event(QEvent *event);

    void show();

    bool abort() const { return m_abort; }

    class MonitoredFileList : public FileList
    {
    public:
      MonitoredFileList(ProcessingFilesDialog &dialog, const QStringList &file_list, const std::string &path)
	: FileList(file_list,path),
	  m_dialog(&dialog),
	  m_hasProblems(false)
      {
	m_dialog->setCounterMax( size() );
      }

      void next();

      const std::string fileName();

      void assignProblem(const std::string &problem_text) {
	m_dialog->reportProblem(problem_text, FileList::fileName());
	m_hasProblems=true;
      }

      bool hasProblems() const { return m_hasProblems; }

    private:
      ProcessingFilesDialog *m_dialog;
      bool m_hasProblems;
    };

    Mutex &mutex() { return m_mutex; }
    void setCurrentFile(const std::string &current_file) { m_currentFile=current_file; }
    void setCounter(unsigned int counter)                { m_counter = counter; }
    void setCounterMax(unsigned int counter_max);

 public slots:
    void update();
    void stop();

  private:
    QApplication *m_app;
    QTimer       *m_timer;

    Mutex        m_mutex;
    std::string  m_currentFile;
    unsigned int m_counter;
    unsigned int m_size;

    bool m_timerStarted;
    bool m_finished;
    bool m_abort;
    bool m_appliedVeto;
    static const unsigned int s_update = 200;

  signals:
    void releaseVeto();
    void vetoAbort();

  };

}
#endif
