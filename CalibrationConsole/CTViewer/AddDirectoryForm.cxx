#include "AddDirectoryForm.h"
#include <QFileDialog>


#include "EnterPatternForm.h"

namespace PixCon {

  AddDirectoryForm::AddDirectoryForm( const std::string &start_path, QWidget* parent)
    :QDialog(parent)
  {
    setupUi(this);
    m_directoryPath->setText( start_path.c_str() );
    m_patternList->setSelectionMode(QAbstractItemView::MultiSelection);
  }

  void AddDirectoryForm::deletePattern() {
      QTreeWidgetItemIterator itr(m_patternList);
      while ((*itr)){
          if((*itr)->isSelected()){
              delete (*itr);
          }
          ++itr;
      }
 /* for(Q3ListViewItem *item = m_patternList->firstChild();
	item != NULL;)
      {
	Q3ListViewItem *next_item = item->nextSibling();
	if (item->isSelected()) {
	  delete item;
	}
	item = next_item;
    }*/
  }



  void AddDirectoryForm::addPattern(const QString &pattern_string)
  {
    QStringList StrList = (QStringList()<< pattern_string);
    if (pattern_string.length()>0) {
      //m_patternList->insertItem(new Q3ListViewItem(m_patternList, pattern_string));
        m_patternList->insertTopLevelItem(m_patternList->topLevelItemCount()+1,new QTreeWidgetItem(m_patternList, StrList));
    }
  }

  void AddDirectoryForm::addPattern()
  {
    EnterPatternForm enter_pattern(this);
    if (enter_pattern.exec() == QDialog::Accepted) {
      addPattern(enter_pattern.m_patternString->text());
    }
  }

  void AddDirectoryForm::getPatternList( QStringList &pattern_list )
  {
    QTreeWidgetItemIterator itr(m_patternList);
    while (*itr){
        if ((*itr)->text(0).length()>0){
            pattern_list.push_back((*itr)->text(0));
        }
        ++itr;
    }
    /*
    for(Q3ListViewItem *item = m_patternList->firstChild();
	item != NULL;
	item = item->nextSibling()) {
      if (item->text(0).length()>0) {
	pattern_list.push_back(item->text(0));
      }
    }*/
  }


  void AddDirectoryForm::openDirectoryBrowser() {
    QString path(m_directoryPath->text());
    int pos = path.lastIndexOf("/");
    if (pos >= 0 ) {
      path.remove(pos, path.length()-pos);
    }
    QStringList filter;
    filter +=  "ROOT data file (*.root)";
    filter += "Any file (*.*)";
    QFileDialog file_chooser(this,"seldir",path,QString::null);
    file_chooser.setFileMode(QFileDialog::Directory);
    file_chooser.setNameFilters(filter);
    file_chooser.setWindowTitle("Select Directory");

    if(file_chooser.exec() == QDialog::Accepted) {
      QStringList file_list = file_chooser.selectedFiles();
      if (file_list.size()==1)
	m_directoryPath->setText(file_list[0]);
    }
  }

  void AddDirectoryForm::verifyDirectoryNameAndAccept() {
    QFileInfo info(m_directoryPath->text());
    if (!info.exists() || (!info.isFile() && !info.isDir())) {

      QColor white("white");
      QColor highlight_color("yellow");

      if (!m_highlighter.get()) {
	m_highlighter=std::make_unique<Highlighter>(*m_directoryPath,highlight_color,white,3*1000);
      }
      else {
	m_highlighter->restart(highlight_color,3*1000);
      }
    }
    else {
      accept();
    }
  }

  std::string AddDirectoryForm::getDirectoryName() {
    return m_directoryPath->text().toLatin1().data();
  }

  void  AddDirectoryForm::toggleAddDeleteButtonEnable() 
  {
    bool has_selected_items=false;
    QTreeWidgetItemIterator itr(m_patternList);
    while (*itr){
        if((*itr)->isSelected()){
            has_selected_items = true;
            break;
        }
        ++itr;
    }

   /* for(Q3ListViewItem *item = m_patternList->firstChild();
	item != NULL;
	item = item->nextSibling()) {
      if (item->isSelected()) {
	has_selected_items=true;
	break;
      }
    }*/
    m_newPatternButton->setEnabled(    !has_selected_items );
    m_deletePatternButton->setEnabled(  has_selected_items );
  }

}
