#include <TH1.h>
#include <Visualiser/Histo.h>
#include "TestHistogramProvider.h"
#include <DataContainer/IScanResultListener.h>
#include <DataContainer/ScanResultStreamer.h>
#include <DataContainer/ResultFileIndexer.h>
#include <ConfigWrapper/ConfObjUtil.h>
#include <ConfigWrapper/ConnectivityUtil.h>

#include <CalibrationDataManager.h>

#include "SummaryHistoAnalyserFactory.h"
#include "StatusCombiner.h"
#include "ValueCombiner.h"

#include <DefaultColourCode.h>

#include <CoralDbClient.hh>
#include <PixMetaException.hh>

// for debugging

namespace PixCon {

  const std::string SimpleHistoList_t::s_emptyString;

  class CalibrationDataResultFileIndexer : public PixA::ResultFileIndexer
  {
  private:
    class HistoContainer_t
    {
    public:
      HistoContainer_t(const std::string &histo_name, const PixA::HistoHandle &handle)
	: m_histoName(histo_name),
	  m_histoHandle(handle),
	  m_serialNumber(0)
      {}

      HistoContainer_t(const std::string &histo_name, const PixA::HistoHandle &handle, unsigned int serial_number)
	: m_histoName(histo_name),
	  m_histoHandle(handle),
	  m_serialNumber(serial_number)
      {}

      void setSerialNumber(unsigned int serial_number) { m_serialNumber = serial_number; }

      bool hasSerialNumber() const          { return m_serialNumber!=0; }
      unsigned int serialNumber() const     { return m_serialNumber; }

      PixA::HistoHandle histoHandle() const { return m_histoHandle; }
      const std::string &name() const       { return m_histoName; } 

    private:
      std::string       m_histoName;
      PixA::HistoHandle m_histoHandle;
      unsigned int      m_serialNumber;
    };

  public:
    enum EHistogramState {kPresumablyDisabled, kNoneExpected, kOk, kOkButBelowExpectation, kOkButAboveExpectation, kMissingHistograms, kNStates};

    static void defineVariables(PixCon::CategoryList::CategoryList::Category &scan_variables) {

      const std::string histogram_state_name("HISTOGRAMS");
      VarDef_t var_def( VarDefList::instance()->createStateVarDef(histogram_state_name, kAllConnTypes, kNStates) ); //@todo create state with mapping
      if (var_def.valueType() == kState) {
	scan_variables.addVariable(histogram_state_name);
	StateVarDef_t &state_var_def = var_def.stateDef();
	state_var_def.addState(kPresumablyDisabled,    DefaultColourCode::kOff,            "Disabled (guess)");
	state_var_def.addState(kNoneExpected,          DefaultColourCode::kConfigured,     "None expected.");
	state_var_def.addState(kOk,                    DefaultColourCode::kSuccess,        "Ok");
	state_var_def.addState(kOkButBelowExpectation, DefaultColourCode::kCloseToFailure, "Ok (< exp.)");
	state_var_def.addState(kOkButAboveExpectation, DefaultColourCode::kScanning,       "Ok (> exp.)"); //@todo colour ?
	state_var_def.addState(kMissingHistograms,     DefaultColourCode::kFailure,        "Missing");
      }
    }

    CalibrationDataResultFileIndexer(ResultIndex_t &scan_result_index,
				     ResultIndex_t &analysis_result_index,
				     std::shared_ptr<CalibrationDataManager> &calibration_data,
				     std::shared_ptr<PixCon::CategoryList> category_list,
				     SerialNumber_t first_local_scan_serial_number,
				     SerialNumber_t first_local_analysis_serial_number,
				     IFileList *file_list=NULL)
      : ResultFileIndexer(true),
	m_calibrationData(calibration_data),
	m_categoryList(category_list),
	m_scanSerialNumberMax( first_local_scan_serial_number),
	m_analysisSerialNumberMax( first_local_analysis_serial_number ),
	m_problemListener(file_list)
    {
      assert( kScan < kAnalysis);
      m_resultIndex[kScan-kScan]=&scan_result_index;
      m_resultIndex[kAnalysis-kScan]=&analysis_result_index;

      for (unsigned int type_i=kScan-kScan; type_i < kAnalysis-kScan; type_i++) {
	m_currentCTHistoListSerialNumber[type_i]=0;
	m_currentCTHistoList[type_i]=0;
	m_currentHistoStat[type_i]=0;
      }

    }

    ~CalibrationDataResultFileIndexer() {
    }

    CAN::PixCoralDbClient *metaDataInstance()
    {
      if (!m_metaDataInstance.get()) {
	m_metaDataInstance = std::make_unique<CAN::PixCoralDbClient>(true);
      }
      return m_metaDataInstance.get();
    }

    void newLabel(const std::string &name, const PixA::ConfigHandle &label_config) {
      processHistogramStatistics();
      PixA::ResultFileIndexer::newLabel(name, label_config);
    }

    /** Will be called if a new serial number is needed.
     * Can be used to also write scan meta data.
     */
    PixA::SerialNumber_t allocateScanSerialNumber(const std::string &file_name, const PixA::ConfigHandle &pix_scan_config, bool matches_pattern) {
      // in case the dummy serial number is needed just count it upwards.
      ++m_scanSerialNumberMax;

      unsigned int scan_serial_number = currentScanNumber();

      if (matches_pattern) {
      unsigned int duration=0;
      std::string start_time_string;
      time_t start_time=0;
      time_t start_time_was_utc=0;

      PixA::ConfigHandle scan_info_handle( currentScanInfo() );
      PixA::ConnectivityRef conn( connectivity(scan_info_handle) );
      if (!conn) {
	std::stringstream message;
	message << "ERROR [CalibrationDataResultFileIndexer::allocateScanSerialNumber] No connectivity for label \"" << currentLabel() << "\"." << std::endl;
	throw PixA::BaseException(message.str());
      }

      try {
	PixA::ConfigRef  scan_info( scan_info_handle.ref() );

	PixA::ConfGrpRef time(scan_info["Time"]);
	start_time_string  = confVal<std::string>(time["ScanStart"]);
	std::string end_time_string  = confVal<std::string>(time["ScanEnd"]);

	struct tm start_tm;
	strptime(start_time_string.c_str(),"%Y-%m-%d %H:%M:%S", &start_tm);

	struct tm end_tm;
	strptime(end_time_string.c_str(),"%Y-%m-%d %H:%M:%S", &end_tm);

	start_time=mktime(&start_tm);
	start_tm.tm_isdst=0;
	start_time_was_utc=mktime(&start_tm);
	start_time_was_utc += start_tm.tm_gmtoff;

	//      struct tm gm_time_struct;
	// 	gmtime_r( &utc_time , &gm_time_struct);
	// 	gm_time_struct.tm_isdst=0;
	// 	time_t start_time_was_utc  utc_time = mktime(&gm_time_struct);

	// 	start_time_was_utc = mktime(&start_tm);
	// 	start_time = CAN::PixCoralDbClient::LocalToGMTTime( start_time_was_utc );

	time_t end_time = CAN::PixCoralDbClient::LocalToGMTTime( mktime(&end_tm) );
	duration = static_cast<unsigned int>(difftime(end_time, start_time ));

	std::cout << "INFO [ScanResultSpy::newScanInfo] time: start  = " << start_time_string 
		  << " end = " << end_time_string
		  << " duration = " << duration
		  << std::endl
		  << start_time << " / " << start_time_was_utc << std::endl
		  << "\t\t" << start_time << " - " << end_time 
		  << std::endl;
      }
      catch(...) {
      }

      if (scan_serial_number==0) {

	if (start_time>0) {
	  // get scan serial number for file
	  CAN::PixCoralDbClient *meta_data_service = metaDataInstance();
	  if (meta_data_service) {

	    try {
	      // get meta data which matches the start time +-60 s (?)
	      std::string empty;
	      time_t time_a = (start_time < start_time_was_utc ? start_time : start_time_was_utc ) - 60;
	      time_t time_b = (start_time > start_time_was_utc ? start_time : start_time_was_utc ) + 60;

	      std::map<CAN::SerialNumber_t, PixLib::ScanInfo_t>
		scan_list = meta_data_service->searchScanMetadata( CAN::kNowhere   /* location   */,
								   time_a          /* start time */,
								   time_b          /* end time   */,
								   empty           /* scan_name  */,
								   empty           /* cfg_tag    */,
								   CAN::kUndefined /* quality    */,
								   CAN::kNotSet    /* status     */,
								   empty           /* comment    */);


	      // search for entries which match the file name
	      std::string::size_type file_name_pos = file_name.rfind("/");
	      std::string file_name_base;
	      std::string file_name_label_base;
	      if (file_name_pos != std::string::npos) {
		file_name_label_base = file_name.substr(file_name_pos+1,file_name.size()-file_name_pos-1);
		std::string::size_type file_name_end_pos = file_name_label_base.find("::");
		if (file_name_end_pos != std::string::npos) {
		  file_name_base = file_name_label_base.substr(0,file_name_end_pos);
		}
	      }
	      std::cout << "INFO [ResultFileIndexer::allocateScanSerialNumber] Search for serial number for : "
			<< file_name << " : " << file_name_label_base << std::endl;
	      std::map<CAN::SerialNumber_t, PixLib::ScanInfo_t>::const_iterator matching_scan = scan_list.end();
	      for (std::map<CAN::SerialNumber_t, PixLib::ScanInfo_t>::const_iterator scan_iter = scan_list.begin();
		   scan_iter != scan_list.end();
		   scan_iter++) {

		std::cout << "INFO [ResultFileIndexer::allocateScanSerialNumber] Candidate " << scan_iter->first
			  << " \"" << scan_iter->second.file() << "\"." << std::endl;

		// try exact match
		if ( scan_iter->second.file() == file_name || scan_iter->second.alternateFile()==file_name) {
		  //		  scan_serial_number = scan_iter->first;
		  matching_scan = scan_iter;
		  std::cout << "INFO [ResultFileIndexer::allocateScanSerialNumber] EXACT MATCH : " << scan_iter->first
			    << "\"" << scan_iter->second.file() << " or " << scan_iter->second.alternateFile() << std::endl;
		  break;
		}
		else {
		  // try everything but the path name
		  std::string::size_type pos1 = scan_iter->second.file().find(file_name_label_base);
		  if (pos1+file_name_label_base.size() == scan_iter->second.file().size()) {
		    std::cout << "INFO [ResultFileIndexer::allocateScanSerialNumber] close MATCH : " << scan_iter->first
			      << "\"" << scan_iter->second.file() << std::endl;
		    //scan_serial_number = scan_iter->first;
		    matching_scan = scan_iter;
		  }
		  else {
		    if (pos1 != std::string::npos) {
		      std::cout << "INFO [ResultFileIndexer::allocateScanSerialNumber] found " << file_name_label_base << " at pos " << pos1
				<< " end = " << pos1 + file_name_label_base.size() << " =!= " << scan_iter->second.file().size() << std::endl;
}
		    std::string::size_type alt_pos1 = scan_iter->second.alternateFile().find(file_name_label_base);
		    if (alt_pos1+file_name_label_base.size() == scan_iter->second.alternateFile().size()) {
		      std::cout << "INFO [ResultFileIndexer::allocateScanSerialNumber] close MATCH : " << scan_iter->first
				<< "\"" << scan_iter->second.alternateFile() << std::endl;
		      //scan_serial_number = scan_iter->first;
		      matching_scan = scan_iter;
		    }
		    else if (matching_scan == scan_list.end()) {
		      std::string::size_type pos2 = scan_iter->second.file().find(file_name_base);
		      if (pos2 != std::string::npos ) {
			std::cout << "INFO [ResultFileIndexer::allocateScanSerialNumber] near MATCH : " << scan_iter->first
				  << "\"" << scan_iter->second.file() << std::endl;
			//			scan_serial_number = scan_iter->first;
			matching_scan = scan_iter;
		      }
		      else {
			std::string::size_type alt_pos2 = scan_iter->second.alternateFile().find(file_name_base);
			if (alt_pos2 != std::string::npos) {
			  std::cout << "INFO [ResultFileIndexer::allocateScanSerialNumber] near MATCH : " << scan_iter->first
				    << "\"" << scan_iter->second.alternateFile() << std::endl;
			  //scan_serial_number = scan_iter->first;
			  matching_scan = scan_iter;
			}
		      }
		    }
		  }
		}
	      }
	      if (matching_scan != scan_list.end()) {
		scan_serial_number = matching_scan->first;
		try {
		  CAN::Revision_t best_guess_of_cfg_revision = (matching_scan->second.cfgRev()==0 ? matching_scan->second.scanStart() : matching_scan->second.cfgRev());
		  PixA::ConnectivityRef conn_from_meta_data = PixA::ConnectivityManager::getConnectivity( matching_scan->second.idTag(),
													  matching_scan->second.connTag(),
													  matching_scan->second.aliasTag(),
													  matching_scan->second.dataTag(),
													  matching_scan->second.cfgTag(),
													  matching_scan->second.modCfgTag(),
													  best_guess_of_cfg_revision);
		  if (conn_from_meta_data) {
		    std::cout << "INFO [CalibrationDataResultFileIndexer::allocateScanSerialNumber] Replace connectivity which was created from the scan info by one created"
		                 " from the meta data. The latter should have the correct module configuration tags." << std::endl;
		    conn = conn_from_meta_data;
		  }
		}
		catch (...) {
		  if (conn) {
		    reportProblem("Failed to create connectivity from meta data. The one created from the ScanInfo may have wrong module configuration tags.");
		  }
		  else {
		    reportProblem("Missing connectivity.");
		  }
		}

	      }
            }
            catch (CAN::PixMetaException &) {
		reportProblem("Exception while trying to determine scan serial number from file name.");
            }
	  }

	  if (scan_serial_number>0) {
	    std::cout << "INFO [CalibrationDataResultFileIndexer::allocateScanSerialNumber] serial number for file " << file_name << " : " << scan_serial_number
		      << std::endl;
	  }
	}

	if (scan_serial_number == 0) {
	  reportProblem("No serial number associated to file. Invent something.");
	  scan_serial_number = ++m_scanSerialNumberMax;
	}
      }

      m_calibrationData->addScan(scan_serial_number,
				 start_time,
				 duration,
				 currentLabel(),
				 currentComment(),
				 MetaData_t::kNormal,
				 pix_scan_config,
				 "",
				 conn);

      m_usedScanSerialNumbers.insert( scan_serial_number);

      getNumberOfFilledHistograms( scan_serial_number, pix_scan_config );
      disableAllModules( scan_serial_number );
      }
      return scan_serial_number;
    }


    /** Will be called if a new serial number is needed.
     * Can be used to also write analysis meta data.
     */
    SerialNumber_t allocateAnalysisSerialNumber(PixA::SerialNumber_t scan_serial_number, bool matches_pattern) {

      SerialNumber_t analysis_serial_number = ++m_analysisSerialNumberMax;
      if (matches_pattern) {
      unsigned int duration=0;
      std::string start_time_string;
      time_t start_time;
      std::string label = currentLabel();
      PixA::ConfigHandle scan_info_handle( currentScanInfo() );
      if (scan_info_handle) {
      try {
	PixA::ConfigRef  scan_info( scan_info_handle.ref() );

	PixA::ConfGrpRef time(scan_info["Time"]);
	start_time_string  = confVal<std::string>(time["ScanStart"]);
	std::string end_time_string  = confVal<std::string>(time["ScanEnd"]);

	struct tm start_tm;
	strptime(start_time_string.c_str(),"%Y-%m-%d %H:%M:%S", &start_tm);

	struct tm end_tm;
	strptime(end_time_string.c_str(),"%Y-%m-%d %H:%M:%S", &end_tm);
	start_time = CAN::PixCoralDbClient::LocalToGMTTime( mktime(&start_tm) );
	time_t end_time = CAN::PixCoralDbClient::LocalToGMTTime( mktime(&end_tm) );
	duration = end_time - start_time;

	std::cout << "INFO [CalibrationDataResultFileIndexer::allocateAnalysisSerialNumber] time: start  = " << start_time_string 
		  << " end = " << end_time_string
		  << " duration = " << duration
		  << std::endl;
      }
      catch(...) {
      }
      }
      else {
	const ScanMetaData_t *scan_meta_data = m_calibrationData->scanMetaDataCatalogue().find(scan_serial_number);
	if (!scan_meta_data) {
	  std::cout << "ERROR [CalibrationDataResultFileIndexer::allocateAnalysisSerialNumber] No meta data for scan "
		    << PixCon::makeSerialNumberString( kScan, scan_serial_number)
		    << std::endl;
	}
	else {
	  duration = scan_meta_data->duration();
	  start_time_string = scan_meta_data->date();
	  label = scan_meta_data->name();
	}
      }

      PixA::ConfigHandle invalid_handle;
      m_calibrationData->addAnalysis(analysis_serial_number,
				     start_time,
				     label,
				     currentComment(),
				     MetaData_t::kNormal,
				     scan_serial_number,
				     invalid_handle,
				     "",
				     invalid_handle,
				     "",
				     invalid_handle);

      m_usedAnalysisSerialNumbers.insert(analysis_serial_number);

      }
      return analysis_serial_number;
    }

    /** Add a pix scan config for the given folder to the given scan serial number.
     * @param scan_serial_number the serial number of the scan in question
     * @param folder_name the folder to which this pix scan config is assigned.
     * @param pix_scan_config a handle to a pix scan config.
     */
    void addPixScanConfig( SerialNumber_t scan_serial_number,
			   const std::string &folder_name,
			   PixA::ConfigHandle &pix_scan_config)
    {
      std::string temp_folder_name(folder_name);
      if (!temp_folder_name.empty() && temp_folder_name[temp_folder_name.size()-1]=='/') {
	temp_folder_name.erase(temp_folder_name.size()-1,1);
      }
      m_calibrationData->addScanConfig(kScan, scan_serial_number,temp_folder_name, pix_scan_config);
    }


    /** Called for plain histograms assigned to an analysis.
     */
    void addAnalysisHisto(SerialNumber_t analysis_serial_number,
			  const std::string &rod_name,
			  const std::string &folder_name,
			  const std::string &histo_name,
			  const PixA::HistoHandle &histo_handle)
    {

      CTHistoList_t *ct_histo_list = ctHistoList(kAnalysis, analysis_serial_number);
      HistoStat_t *histo_stat = histoStat(kAnalysis, analysis_serial_number);
      if ( histo_handle ) {

	std::string full_histo_name = folder_name;
	if (folder_name.empty() || folder_name[folder_name.size()-1]!='/')
	  full_histo_name+='/';
	full_histo_name+=histo_name;

	std::cout << "INFO [CalibrationDataResultFileIndexer::addAnalysisHisto] " << PixCon::makeSerialNumberString(kAnalysis, analysis_serial_number)
		  << " new histo " << full_histo_name << " for ROD " << rod_name << "."
		    << std::endl;

	ct_histo_list->histoList().addHistogram( rod_name, full_histo_name, histo_handle );
	HistoInfo_t histo_info;

	PixA::HistoRef histo( histo_handle.ref());
	histo.numberOfHistos( TestHistogramProvider::stripPath(histo_name),histo_info);

	if ( histo_info.minHistoIndex() < histo_info.maxHistoIndex() ) {
	  histo_stat->incRodHistograms( rod_name );
	  analyseHistogram( full_histo_name, histo_handle, kAnalysis, analysis_serial_number, rod_name);
	}
	else {
	  std::cout << "WARNINING [CalibrationDataResultFileIndexer::addScanHisto] " << PixCon::makeSerialNumberString(kAnalysis, analysis_serial_number)
		    << " histo " << histo_name << " folder for ROD " << rod_name << " is empty."
		    << std::endl;
	}
      }
    }


    /** Called for plain histograms assigned to a scan.
     */
    void addScanHisto(SerialNumber_t scan_serial_number,
		      const std::string &rod_name,
		      const PixA::ScanConfigRef &/*scan_config*/,
		      const std::string &folder_name,
		      const std::string &histo_name,
		      const PixA::HistoHandle &histo_handle) {

      CTHistoList_t *ct_histo_list = ctHistoList(kScan, scan_serial_number);
      HistoStat_t *histo_stat = histoStat(kScan, scan_serial_number);
      if ( histo_handle ) {

	std::string full_histo_name = folder_name;
	if (folder_name.empty() || folder_name[folder_name.size()-1]!='/')
	  full_histo_name+='/';
	full_histo_name+=histo_name;
	std::cout << "INFO [CalibrationDataResultFileIndexer::addScanHisto] " << PixCon::makeSerialNumberString(kScan, scan_serial_number)
		  << " new histo " << full_histo_name << " for ROD " << rod_name << "."
		    << std::endl;

	ct_histo_list->histoList().addHistogram( rod_name, full_histo_name, histo_handle );
	HistoInfo_t histo_info;

	PixA::HistoRef histo( histo_handle.ref());
	histo.numberOfHistos( TestHistogramProvider::stripPath(histo_name),histo_info);

	if ( histo_info.minHistoIndex() < histo_info.maxHistoIndex() ) {
	  histo_stat->incRodHistograms( rod_name );
	  analyseHistogram( full_histo_name, histo_handle, kScan, scan_serial_number, rod_name);
	}
	else {
	  std::cout << "WARNINING [CalibrationDataResultFileIndexer::addScanHisto] " << PixCon::makeSerialNumberString(kScan, scan_serial_number)
		    << " histo " << full_histo_name << " folder for ROD " << rod_name << " is empty."
		    << std::endl;
	}
      }

    }

    /** Called for pix scan histograms assigned to a scan.
     */
    void addScanHisto(SerialNumber_t scan_serial_number,
		      const std::string &rod_name,
		      const std::string &module_name,
		      const PixA::ScanConfigRef &scan_config,
		      const std::string &folder_name,
		      const std::string &histo_name,
		      const PixA::HistoHandle &histo_handle) {

      CTHistoList_t *ct_histo_list = ctHistoList(kScan, scan_serial_number);
      HistoStat_t *histo_stat = histoStat(kScan, scan_serial_number);
      if ( histo_handle ) {

	std::string full_histo_name = folder_name;
	if (folder_name.empty() || folder_name[folder_name.size()-1]!='/')
	  full_histo_name+='/';
	full_histo_name+=histo_name;

	std::cout << "INFO [CalibrationDataResultFileIndexer::addScanHisto] " << PixCon::makeSerialNumberString(kScan, scan_serial_number)
		  << " new histo " << full_histo_name << " for module " << module_name << "."
		    << std::endl;

	ct_histo_list->pixScanHistoList().addHisto( full_histo_name, scan_config, module_name, histo_handle );
	HistoInfo_t histo_info;
	ct_histo_list->pixScanHistoList().numberOfHistograms(module_name, histo_name, histo_info);
	if ( histo_info.minHistoIndex() < histo_info.maxHistoIndex() ) {
	  histo_stat->incHistograms( module_name, rod_name );
	}
	else {
	  std::cout << "WARNINING [CalibrationDataResultFileIndexer::addScanHisto] " << PixCon::makeSerialNumberString(kScan, scan_serial_number)
		    << " histo " << histo_name << " folder for module " << module_name << " is empty."
		    << std::endl;
	}
      }

    }

    unsigned int localScanSerialNumberMax() const {return m_scanSerialNumberMax; }

    unsigned int localAnalysisSerialNumberMax() const {return m_analysisSerialNumberMax; }

    //    unsigned int histogramsPerSerialNumber() const { return m_nHistogramsPerSerialNumber; }

    void finish() {
      ResultFileIndexer::finish();
      processHistogramStatistics();
    }

//     void finishPixModuleGroup() {
//       std::cout << "INFO [CalibrationDataResultFileIndexer::finishPixModuleGroup]  " << m_currentRodName << " will map histos to serial numbers." << std::endl;
//       std::cout << "INFO [CalibrationDataResultFileIndexer::finishPixModuleGroup]  " << m_currentRodName << " will process histo statistics." << std::endl;
//       std::cout << "INFO [CalibrationDataResultFileIndexer::finishPixModuleGroup]  " << m_currentRodName << " done." << std::endl;
// //       for (std::map< std::string, std::vector< std::pair<std::string,PixA::HistoHandle> > >::const_iterator folder_iter=m_tempPixScanHistoList.begin();
// // 	   folder_iter != m_tempPixScanHistoList.end();
// // 	   folder_iter++) {
// // 	//	std::cout << "INFO [CalibrationDataResultFileIndexer::finishPixModuleGroup] process histos from folder " << folder_iter->first << ".";
// // 	std::map<std::string, std::pair<SerialNumber_t,unsigned int> >::const_iterator map_iter = m_folderToSerialNumber.find(folder_iter->first);
// // 	if (map_iter != m_folderToSerialNumber.end()) {

// // 	  std::cout << "INFO [CalibrationDataResultFileIndexer::finishPixModuleGroup] folder " << folder_iter->first << " maps to " << map_iter->second.first << std::endl;
// // 	  for(std::vector< std::pair< std::string, PixA::HistoHandle> >::const_iterator histo_iter= folder_iter->second.begin();
// // 	      histo_iter != folder_iter->second.end();
// // 	      histo_iter++) {
// // 	    std::cout << "INFO [CalibrationDataResultFileIndexer::finishPixModuleGroup] would add  " << histo_iter->first  << " to  " << map_iter->second.first << std::endl;
// // 	  }

// // 	}
// // 	else {
// // 	  std::cout << "ERROR [CalibrationDataResultFileIndexer::finishPixModuleGroup] Mo serial number found for folder " << folder_iter->first << "." << std::endl;
// // 	}
// //       }


//     }

    void processHistogramStatistics() {
      assert( kAnalysis>kScan ) ;
      for (unsigned int type_i_1=0; type_i_1<static_cast<unsigned int>(kAnalysis-kScan); type_i_1++) {
	EMeasurementType measurement_type = static_cast<EMeasurementType>(type_i_1+kScan);
	assert( measurement_type == kScan || measurement_type == kAnalysis );
	for (std::map<SerialNumber_t, HistoStat_t>::iterator serial_iter=m_histoStat[type_i_1].begin();
	     serial_iter != m_histoStat[type_i_1].end();
	     ++serial_iter) {
	  if (!serial_iter->second.isUptoDate()) {
	    serial_iter->second.update();

	    for (unsigned int type_i_2=0; type_i_2<2; type_i_2++) {
	      HistoStat_t::EHistogramType histogram_type_i=static_cast<HistoStat_t::EHistogramType>( type_i_2 );
	      for (std::map<std::string, unsigned int>::const_iterator conn_iter = serial_iter->second.begin(histogram_type_i) ;
		   conn_iter != serial_iter->second.end(histogram_type_i);
		   ++conn_iter) {
		EHistogramState histogram_state = serial_iter->second.status( histogram_type_i, conn_iter );
		assert( histogram_type_i==HistoStat_t::kModule || histogram_type_i==HistoStat_t::kRod );
		EValueConnType value_conn_type;
		if (histogram_type_i==HistoStat_t::kModule) {
		  m_calibrationData->setModuleEnable(conn_iter->first, measurement_type, serial_iter->first, true );
		  value_conn_type=kModuleValue;
		}
		else {
		  value_conn_type=kRodValue;
		  m_calibrationData->setRodEnable(conn_iter->first, measurement_type, serial_iter->first, true );
		}

		// @todo add auxiliary function to ConnObjDataList ?
		m_calibrationData->calibrationData().addValue(value_conn_type,
							      conn_iter->first,
							      measurement_type,
							      serial_iter->first,
							      "HISTOGRAMS",
							      static_cast<State_t>(histogram_state));
	      }
	    }

	    for (std::map<std::string, bool>::const_iterator rod_iter = serial_iter->second.rodEnableBegin();
		 rod_iter != serial_iter->second.rodEnableEnd();
		 ++rod_iter) {

	      // enable ROD
	      if (rod_iter->second) {
		m_calibrationData->setRodEnable(rod_iter->first, measurement_type, serial_iter->first, true );
	      }
	    }
	  }
	}
      }
    }

    void getNumberOfFilledHistograms(SerialNumber_t scan_serial_number, const PixA::ConfigHandle &config_handle) {
      std::map<SerialNumber_t, HistoStat_t>::iterator histo_stat = m_histoStat[kScan].find(scan_serial_number);
      if (histo_stat == m_histoStat[kScan].end()) {
	std::pair<std::map<SerialNumber_t, HistoStat_t>::iterator, bool> 
	  ret  = m_histoStat[kScan].insert(std::make_pair( scan_serial_number, HistoStat_t() ));
	histo_stat = ret.first;
      }
      if (histo_stat->second.moduleBegin() != histo_stat->second.moduleEnd() ) return;

      static const std::string s_histogramKept("histograms_histogramKept");
      static const std::string s_histogramFilled("histogramFilled");
      if (config_handle) {
	PixA::ConfigRef config_ref = config_handle.ref();
	const PixLib::Config &scan_config = config_ref.config();
	// Config, ConfGroup do not offer const functions, although they are const. 
	const PixLib::ConfGroup &conf_group = const_cast<PixLib::Config&>(scan_config)["histograms"];
	for (unsigned int obj_i=0; obj_i < static_cast<unsigned int>( const_cast<PixLib::ConfGroup&>(conf_group).size() ); obj_i++) {
	  const PixLib::ConfObj &conf_obj = const_cast<PixLib::ConfGroup&>(conf_group)[obj_i];
	  const std::string &name = const_cast<PixLib::ConfObj&>(conf_obj).name();

	  if (name.compare(0,s_histogramKept.size(),s_histogramKept)==0) {
	    if (confVal<bool>(conf_obj)) {
	      std::cout << "INFO [CalibrationDataResultFileIndexer::getNumberOfFilledHistograms] " << name << " will be kept and filled ? check "
			<< s_histogramFilled+std::string(name,s_histogramKept.size(),name.size()-s_histogramKept.size()) << std::endl;
	      if (confVal<bool>( const_cast<PixLib::ConfGroup&>(conf_group)[s_histogramFilled+std::string(name,s_histogramKept.size(),name.size()-s_histogramKept.size())] ))  {
		std::cout << "INFO [CalibrationDataResultFileIndexer::getNumberOfFilledHistograms] " << name << " will also be filled." << std::endl;
		histo_stat->second.incExpectedModuleHistograms();
	      }
	    }
	  }
        }
      }
    }

    void disableAllModules(SerialNumber_t scan_serial_number) {
      try {
	PixA::ConnectivityRef connectivity(m_calibrationData->scanConnectivity( scan_serial_number));
	if (connectivity) {

	  for (PixA::CrateList::const_iterator crate_iter = connectivity.crates().begin();
	       crate_iter != connectivity.crates().end();
	       ++crate_iter) {

	    PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
	    PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
	    for (PixA::RodList::const_iterator rod_iter = rod_begin;
		 rod_iter != rod_end;
		 ++rod_iter) {

	      {
		PixCon::CalibrationData::ConnObjDataList data_list( m_calibrationData->calibrationData().createConnObjDataList(PixA::connectivityName(rod_iter)) );
		PixCon::CalibrationData::EnableState_t &enable_state = data_list.enableState(kScan, scan_serial_number);
		enable_state.setEnabled(false);
	      }

	      PixA::Pp0List::const_iterator pp0_begin = PixA::pp0Begin(rod_iter);
	      PixA::Pp0List::const_iterator pp0_end = PixA::pp0End(rod_iter);

	      for (PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
		   pp0_iter != pp0_end;
		   ++pp0_iter) {

		PixA::ModuleList::const_iterator module_begin = PixA::modulesBegin(pp0_iter);
		PixA::ModuleList::const_iterator module_end = PixA::modulesEnd(pp0_iter);

		for (PixA::ModuleList::const_iterator module_iter = module_begin;
		     module_iter != module_end;
		     ++module_iter) {
		  PixCon::CalibrationData::ConnObjDataList data_list( m_calibrationData->calibrationData().createConnObjDataList(PixA::connectivityName(module_iter)) );
		  PixCon::CalibrationData::EnableState_t &enable_state = data_list.enableState(kScan, scan_serial_number);
		  enable_state.setEnabled(false);

		}
	      }
	    }
	  }
	}
      }
      catch(...) {
	std::cout << "ERROR [CalibrationDataResultFileIndexer::disableAllModules] Cannot initialise calibration data container for scan " 
		  << PixCon::makeSerialNumberString(kScan, scan_serial_number)
		  << " because there is no connectivity assigned to the scan yet." << std::endl;
      }
    }


    const std::set<SerialNumber_t> &usedScanSerialNumbers() const     { return m_usedScanSerialNumbers; }
    const std::set<SerialNumber_t> &usedAnalysisSerialNumbers() const { return m_usedAnalysisSerialNumbers; }

    void reportProblem(const std::string &message) {
      if (m_problemListener) {
	m_problemListener->assignProblem(message);
      }
    }


  private:

    void analyseHistogram(const std::string &name,
			  const PixA::HistoHandle &histo_handle,
			  EMeasurementType measurement_type,
			  SerialNumber_t serial_number,
			  const std::string &rod_name)
    {
      std::vector<SerialNumber_t> serial_number_list;
      serial_number_list.push_back(serial_number);
      analyseHistogram(name,histo_handle, measurement_type, serial_number_list, rod_name);
    }

    void analyseHistogram(const std::string &name,
			  PixA::HistoHandle histo_handle,
			  EMeasurementType measurement_type,
			  const std::vector<SerialNumber_t> &serial_numbers,
			  const std::string &rod_name)
    {
      const ISummaryHistoAnalyserKit *analyser_kit = SummaryHistoAnalyserFactory::kit(name);
      if (analyser_kit) {
	std::unique_ptr<ISummaryHistoAnalyser> analyser(analyser_kit->create(m_categoryList, m_calibrationData));
	if (analyser.get()) {
	  PixA::HistoRef histo_ref = histo_handle.ref();
	  HistoInfo_t info;
	  std::string stripped_histo_name = TestHistogramProvider::stripPath(name);
	  histo_ref.numberOfHistos(stripped_histo_name,info);
	  Index_t index;
	  Index_t min_index;
	  Index_t max_index;
	  for (unsigned int dim_i=0; dim_i<HistoInfo_t::s_nMaxLevels-1; dim_i++) {
	    if (info.maxIndex(dim_i)==0) break;
	    min_index.push_back(0);
	    max_index.push_back(info.maxIndex(dim_i));
	    index.push_back(min_index.back());
	  }
	  if (info.minHistoIndex() < info.maxHistoIndex()) {
	    min_index.push_back(info.minHistoIndex());
	    max_index.push_back(info.maxHistoIndex());
	    index.push_back(min_index.back());
	  }
	  assert(max_index.size() == min_index.size() && min_index.size()==index.size());
	  if (max_index.size()>0) {
	  for(;index[0]<max_index[0];) {
	    std::cout << "INFO [CalibrationDataResultFileIndexer::newHisto] analyse " << rod_name << " / " << name  << " : ";
	    for (unsigned int dim_i=0; dim_i<index.size(); dim_i++) {
	      std::cout << index[dim_i] << " ";
	    }
	    std::cout << std::endl;

	    PixCon::HistoPtr_t a_histo_ptr = PixA::convert( histo_ref.histo(stripped_histo_name,index) ) ;
	    std::unique_ptr<PixCon::Histo_t> a_histo(a_histo_ptr);
	    if (a_histo.get()) {
	      for (std::vector<SerialNumber_t>::const_iterator serial_iter = serial_numbers.begin();
		   serial_iter != serial_numbers.end();
		   serial_iter++) {
		analyser->analyseHistogram(measurement_type, *serial_iter, rod_name, a_histo.get() );
	      }
	    }

	    for (unsigned int dim_i=max_index.size(); dim_i-->0; ) {
	      if (++index[dim_i] < max_index[dim_i] || dim_i==0) {
		break;
	      }
	      index[dim_i]=min_index[dim_i];
	    }
	  }
	  }
	}
      }
    }

    std::shared_ptr<CalibrationDataManager> m_calibrationData;
    std::shared_ptr<PixCon::CategoryList>   m_categoryList;

    ResultIndex_t   *m_resultIndex[kAnalysis-kScan+1];
    SerialNumber_t   m_scanSerialNumberMax;
    SerialNumber_t   m_analysisSerialNumberMax;

    std::set<SerialNumber_t> m_usedScanSerialNumbers;
    std::set<SerialNumber_t> m_usedAnalysisSerialNumbers;

    //    std::map<std::string, std::vector< std::pair<std::string, PixA::HistoHandle> > > m_tempPixScanHistoList;

    class HistoStat_t
    {
    public:
      enum EHistogramType {kModule, kRod };
      //      enum EHistogramState { kNone, kMissing, kNoneButOkay, kNoneExpected, kMissingButOkay, kOk, kTooManyButOkay};

      HistoStat_t() : m_uptoDate(true) { for (unsigned int type_i=0; type_i<2; type_i++) { m_nExpectedHistograms[type_i]=0; m_nObservedHistograms[type_i]=0; } }
      void incExpectedModuleHistograms()             { m_nExpectedHistograms[kModule]++; m_uptoDate=false; }
      unsigned int nExpectedModuleHistograms() const { return m_nExpectedHistograms[kModule]; }

      void incHistograms(const std::string &conn_name, const std::string &rod_name) {
	incHistograms(kModule,conn_name);
	m_rodEnable[rod_name]=true;
      }

      void incRodHistograms(const std::string &conn_name) {
	incHistograms(kRod,conn_name);
	m_rodEnable[conn_name]=true;
      }

      bool isUptoDate() const { return m_uptoDate; }

      void update() {
	for (unsigned int type_i=0; type_i<2; type_i++) {
	  for(std::map<std::string, unsigned int>::const_iterator conn_iter = m_histograms[type_i].begin();
	      conn_iter != m_histograms[type_i].end();
	      conn_iter++) {
	    if (conn_iter->second > m_nObservedHistograms[type_i]) {
	      m_nObservedHistograms[type_i] = conn_iter->second;
	    }
	  }
	}
	m_nExpectedHistograms[kRod]=m_nObservedHistograms[kRod];
	m_uptoDate=true;
      }


      std::map<std::string, unsigned int>::const_iterator begin(EHistogramType type_i) { assert(type_i<=kRod); return m_histograms[type_i].begin(); }
      std::map<std::string, unsigned int>::const_iterator end(EHistogramType type_i)   { assert(type_i<=kRod); return m_histograms[type_i].end(); }

      std::map<std::string, unsigned int>::const_iterator moduleBegin() { return m_histograms[kModule].begin(); }
      std::map<std::string, unsigned int>::const_iterator moduleEnd() { return m_histograms[kModule].end(); }

      std::map<std::string, unsigned int>::const_iterator rodBegin() { return m_histograms[kRod].begin(); }
      std::map<std::string, unsigned int>::const_iterator rodEnd() { return m_histograms[kRod].end(); }

      std::map<std::string, bool>::const_iterator rodEnableBegin() { return m_rodEnable.begin(); }
      std::map<std::string, bool>::const_iterator rodEnableEnd()   { return m_rodEnable.end(); }


      EHistogramState statusModuleHistogram(const std::map<std::string, unsigned int>::const_iterator &iter) {
	return status(kModule,iter->second);
      }

      EHistogramState statusRodHistogram(const std::map<std::string, unsigned int>::const_iterator &iter) {
	return status(kRod,iter->second);
      }

      EHistogramState status(EHistogramType type_i, const std::map<std::string, unsigned int>::const_iterator &iter) {
	return status(type_i,iter->second);
      }

    private:
      EHistogramState status(EHistogramType type_i, unsigned n_histograms) {
	assert(type_i <= kRod);
	if (n_histograms == 0) {
	  if (m_nObservedHistograms[type_i]==0) {
	    if (m_nExpectedHistograms[type_i]==0) {
	      return kNoneExpected;
	    }
	    else {
	      return kPresumablyDisabled;
	    }
	  }
	  else {
	    return kPresumablyDisabled;
	  }
	}
	else if (n_histograms == m_nObservedHistograms[type_i]) {
	  if (n_histograms == m_nExpectedHistograms[type_i]) {
	    return kOk;
	  }
	  else if (m_nObservedHistograms[type_i]>m_nExpectedHistograms[type_i]) {
	    return kOkButAboveExpectation;
	  }
	  else {
	    return kOkButBelowExpectation;
	  }
	}
	else {
	    return kMissingHistograms;
	}
      }

      void incHistograms(EHistogramType type_i, const std::string &conn_name) {
	assert(type_i<=kRod);
	std::map<std::string, unsigned int>::iterator conn_iter = m_histograms[type_i].find(conn_name);
	if (conn_iter == m_histograms[type_i].end() ) {
	  std::pair< std::map<std::string, unsigned int>::iterator, bool>
	    ret = m_histograms[type_i].insert(std::make_pair(conn_name,static_cast<unsigned int>(0)));
	  conn_iter = ret.first;
	}
	++(conn_iter->second);
	m_uptoDate=false;
      }

      unsigned int m_nExpectedHistograms[2];
      unsigned int m_nObservedHistograms[2];
      std::map<std::string, unsigned int> m_histograms[2];
      std::map<std::string, bool>         m_rodEnable;
      bool m_uptoDate;
    };

    std::map<std::string, std::vector< HistoContainer_t > > m_tempHistoList;
    std::map<SerialNumber_t, HistoStat_t>                   m_histoStat[kAnalysis-kScan+1];
    //unsigned int m_nHistos;

    CTHistoList_t *ctHistoList( EMeasurementType measurement_type, SerialNumber_t serial_number) {
      assert(measurement_type>=kScan || measurement_type<=kAnalysis);
      assert(kScan < kAnalysis);
      unsigned int type_i = static_cast<unsigned int>(measurement_type - kScan);

      if (serial_number == m_currentCTHistoListSerialNumber[type_i] && m_currentCTHistoList[type_i]) {
	return m_currentCTHistoList[type_i];
      }
      m_currentCTHistoList[type_i] = &( (*m_resultIndex[type_i])[serial_number] );
      assert( m_currentCTHistoList[type_i] );

      // also set the histograms statistics list
      m_currentHistoStat[type_i] = &( m_histoStat[type_i][serial_number] );
      assert( m_currentHistoStat[type_i] );

      m_currentCTHistoListSerialNumber[type_i] = serial_number;

      return m_currentCTHistoList[type_i];
    }

    HistoStat_t *histoStat( EMeasurementType measurement_type, SerialNumber_t /*serial_number*/) {
      assert(measurement_type>=kScan || measurement_type<=kAnalysis);
      assert(kScan < kAnalysis);
      unsigned int type_i = static_cast<unsigned int>(measurement_type - kScan);
      assert( m_currentHistoStat[type_i] );
      return m_currentHistoStat[type_i];
    }

    SerialNumber_t m_currentCTHistoListSerialNumber[kAnalysis-kScan+1];
    CTHistoList_t *m_currentCTHistoList[kAnalysis-kScan+1];
    HistoStat_t   *m_currentHistoStat[kAnalysis-kScan+1];

    // 
    std::vector<std::string> m_allHistos;
    std::vector<std::string> m_allRODs;
    IFileList *m_problemListener;
    std::unique_ptr<CAN::PixCoralDbClient> m_metaDataInstance;

  };

  TestHistogramProvider::TestHistogramProvider(std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data,
					       const std::shared_ptr<PixCon::CategoryList> &categories,
					       SerialNumber_t local_scan_serial_number_max,
					       SerialNumber_t local_analysis_serial_number_max)
    : m_calibrationData(calibration_data),
      m_categories(categories),
      m_localScanSerialNumberMax(local_scan_serial_number_max),
      m_localAnalysisSerialNumberMax(local_analysis_serial_number_max)
  { 
    PixCon::CategoryList::Category scan_cat = m_categories->addCategory("Scan");
    CalibrationDataResultFileIndexer::defineVariables(scan_cat); 
  }

  TestHistogramProvider::~TestHistogramProvider() {}

  SerialNumber_t TestHistogramProvider::addFile(const std::string &file_name,
						const std::vector<std::string> &pattern_list) {
    std::vector<std::string> temp;
    temp.push_back(file_name);
    return addFiles(temp,pattern_list);
  }


  class FileVector : public IFileList
  {
  public:
    FileVector(const std::vector<std::string> &file_list)
      : m_iter(file_list.begin()),
	m_end(file_list.end()),
	m_nElements(file_list.size()),
	m_counter(0)
    {  }

    const std::string fileName()
    {
      if (m_iter!=m_end) {
	return *m_iter;
      }
      return "";
    }

    void assignProblem(const std::string &problem) {
      std::cout << "INFO [FileVector::hasProblem] For " << fileName() << " the following problem occurred : " << problem << std::endl;
    }


    void next() { if (m_iter != m_end) { ++m_iter; ++m_counter;};}

    unsigned int size() const { return m_nElements;}
    unsigned int counter() const { return m_counter;}

  private:
    std::vector<std::string>::const_iterator m_iter;
    std::vector<std::string>::const_iterator m_end;
    unsigned  int m_nElements;
    unsigned  int m_counter;
  };

  SerialNumber_t TestHistogramProvider::addFiles(const std::vector<std::string> &file_name_list,
						 const std::vector<std::string> &pattern_list)
  {
    FileVector file_vector(file_name_list);
    return addFiles(file_vector, pattern_list);
  }

  SerialNumber_t TestHistogramProvider::addFiles(IFileList &file_list,
						 const std::vector<std::string> &pattern_list)
  {
    std::unique_ptr<CalibrationDataResultFileIndexer> result_file_indexer( new CalibrationDataResultFileIndexer(m_resultIndex[ resultIndex(kScan) ],
													      m_resultIndex[ resultIndex(kAnalysis) ],
													      m_calibrationData,
													      m_categories,
													      m_localScanSerialNumberMax,
													      m_localAnalysisSerialNumberMax,
													      &file_list /* kludge to report problems*/) );

    for (std::vector<std::string>::const_iterator pattern_iter = pattern_list.begin();
	 pattern_iter != pattern_list.end();
	 ++pattern_iter) {
      result_file_indexer->addPattern(*pattern_iter);
    }

    std::string file_name;
    while ( !(file_name = file_list.fileName() ).empty()) {
      try {
	PixA::ScanResultStreamer streamer(file_name );
	streamer.setIgnoreConnAndCfg(true);
	result_file_indexer->setVerbose(false);
	streamer.setListener( *result_file_indexer );
	try {
	  streamer.read();
	}
	catch (MetaDataExistsAlready &err) {
	  file_list.assignProblem("Already loaded.");
	}
	catch (PixA::BaseException &err) {
	  std::cout << "ERROR [TestHistogramProvider::addFile] caught exception while processing file " << file_name
		    << " :  " <<  err.getDescriptor()
		    << std::endl;
	  file_list.assignProblem(err.getDescriptor());
	}
      }
      catch (PixA::BaseException &err) {
	std::cout << "ERROR [TestHistogramProvider::addFile] Caught exception while opening file " << file_name
		  << " : " << err.getDescriptor()
		  << std::endl;
      }
      file_list.next();
    }
    m_localScanSerialNumberMax = result_file_indexer->localScanSerialNumberMax();
    m_localAnalysisSerialNumberMax = result_file_indexer->localAnalysisSerialNumberMax();
    return finishFiles(result_file_indexer);
  }

  SerialNumber_t TestHistogramProvider::finishFiles(std::unique_ptr<CalibrationDataResultFileIndexer> &result_file_indexer) 
  {
    try {
      // copy enable state from scan to analyses
      const std::set<SerialNumber_t> &scan_serial_number_list = result_file_indexer->usedScanSerialNumbers();
      const ScanMetaDataCatalogue &scan_catalogue=m_calibrationData->scanMetaDataCatalogue();

      for (std::set<SerialNumber_t>::const_iterator scan_serial_number_iter = scan_serial_number_list.begin();
	   scan_serial_number_iter != scan_serial_number_list.end();
	   scan_serial_number_iter++) {

	const ScanMetaData_t *scan_meta_data = scan_catalogue.find(*scan_serial_number_iter);
	if (scan_meta_data) {
	  for (std::vector<SerialNumber_t>::const_iterator analysis_iter = scan_meta_data->beginAnalysis();
	       analysis_iter !=  scan_meta_data->endAnalysis();
	       analysis_iter++) {
	    m_calibrationData->calibrationData().copyScanEnableStateToAnalysis(*scan_serial_number_iter, *analysis_iter);
	  }
	}
      }

      checkHistogramNumber(kScan, scan_serial_number_list);
      const std::set<SerialNumber_t> &analysis_serial_number_list = result_file_indexer->usedAnalysisSerialNumbers();
      checkHistogramNumber(kAnalysis, analysis_serial_number_list);

      // combine status variables of all scans/analyses
      if (scan_serial_number_list.size()+analysis_serial_number_list.size()>1) {

	SerialNumber_t first_used_scan = 0;
	std::stringstream comment;
	if (!scan_serial_number_list.empty()) {
	  first_used_scan = *(scan_serial_number_list.begin());
	  comment << "Scans : " << PixCon::makeSerialNumberString( kScan, *(scan_serial_number_list.begin()));
	  if (scan_serial_number_list.size()>1) {
	    comment << " - " << PixCon::makeSerialNumberString( kScan, *(scan_serial_number_list.rbegin()));
	  }
	}

	if (!analysis_serial_number_list.empty()) {
	  if (first_used_scan == 0) {
	    first_used_scan = m_calibrationData->scanSerialNumber(kAnalysis,*(analysis_serial_number_list.begin()));
	  }

	  comment << "Analyses : " << PixCon::makeSerialNumberString( kAnalysis, *(analysis_serial_number_list.begin()));
	  if (analysis_serial_number_list.size()>1) {
	    comment << " - " << PixCon::makeSerialNumberString( kAnalysis, *(analysis_serial_number_list.rbegin()));
	  }
	}

	PixA::ConfigHandle invalid_handle;
	time_t start_time = time(0);
	SerialNumber_t combined_analysis_serial_number = ++m_localAnalysisSerialNumberMax;
	m_calibrationData->addAnalysis(combined_analysis_serial_number,
				       start_time,
				       "CombinedAnalysis",
				       comment.str(),
				       MetaData_t::kNormal,
				       first_used_scan,
				       invalid_handle,
				       "",
				       invalid_handle,
				       "",
				       invalid_handle);

	StatusCombiner status_combiner( m_calibrationData, combined_analysis_serial_number);
	ValueCombiner value_combiner( m_calibrationData, combined_analysis_serial_number);

	for (std::set<SerialNumber_t>::const_iterator scan_serial_number_iter = scan_serial_number_list.begin();
	     scan_serial_number_iter != scan_serial_number_list.end();
	     scan_serial_number_iter++) {

	  const ScanMetaData_t *scan_meta_data = scan_catalogue.find(*scan_serial_number_iter);
	  if (scan_meta_data) {
	    status_combiner.addMeasurement(kScan, *scan_serial_number_iter);
	    value_combiner.addMeasurement(kScan, *scan_serial_number_iter);

	    for (std::vector<SerialNumber_t>::const_iterator analysis_iter = scan_meta_data->beginAnalysis();
		 analysis_iter !=  scan_meta_data->endAnalysis();
		 analysis_iter++) {
	      status_combiner.addMeasurement(kAnalysis, *analysis_iter);
	      value_combiner.addMeasurement(kAnalysis, *analysis_iter);
	    }
	  }

	}
	MetaData_t::EStatus status = status_combiner.createGlobalStatus();
	m_calibrationData->updateStatus(kAnalysis,combined_analysis_serial_number, status);
        status_combiner.dumpReport();
        value_combiner.dumpReport();

      }
      else {
	std::cout << "INFO [TestHistogramProvider::finishFiles] Did not try to combine stati, since there is  no scan with connectivity."
		  << std::endl;
      }


      const char serial_number_header[2]={'S','A'};
      for( unsigned int ix=0; ix<2; ix++) {
	unsigned int result_index = resultIndex((ix==0 ? kScan : kAnalysis));
	for (ResultIndex_t::const_iterator iter = m_resultIndex[result_index].begin();
	     iter != m_resultIndex[result_index].end();
	     iter++ ) {
	  const SimpleHistoList_t &a = iter->second.histoList();
	  std::cout << "INFO [TestHistogramProvider::addFile] names registered for serial number  "
		    << serial_number_header[ix] << iter->first << " : ";
	  for(std::vector<std::string>::const_iterator name_iter = a.nameList().begin();
	      name_iter != a.nameList().end();
	      name_iter++) {
	    std::cout << *name_iter << " ";
	  }
	  std::cout << std::endl;
	  const std::map<std::string, std::vector< PixA::HistoHandle> > &a_histo_list = a.histoList();
	  for(std::map<std::string, std::vector< PixA::HistoHandle> >::const_iterator histo_list_iter = a_histo_list.begin();
	      histo_list_iter != a_histo_list.end();
	      histo_list_iter++) {

	    std::cout << "INFO [TestHistogramProvider::addFile] serial number " <<  iter->first << ",  connectivity_name " << histo_list_iter->first << " : ";
	    unsigned int is=0;
	    for(std::vector< PixA::HistoHandle>::const_iterator handle_iter = histo_list_iter->second.begin();
		handle_iter != histo_list_iter->second.end();
		handle_iter++, is++ ) {
	      if (*handle_iter) {
		std::cout << is << " ";
	      }
	    }
	    std::cout << std::endl;
	  }
	}

      }


      if (!scan_serial_number_list.empty()) {
	return *(scan_serial_number_list.begin());
      }
      else if (!analysis_serial_number_list.empty()) {
	return m_calibrationData->scanSerialNumber(kAnalysis, *(analysis_serial_number_list.begin()));
      }

    }
    catch (PixA::BaseException &err) {
      std::cout << "ERROR [TestHistogramProvider::addFile] Caught exception while post-processing scans : "
		<< err.getDescriptor()
		<< std::endl;
    }
    return 0;
  }

  PixA::ConfigHandle TestHistogramProvider::pixScanConfig(SerialNumber_t serial_number)
  {
    const ScanMetaData_t *scan_meta_data = m_calibrationData->scanMetaDataCatalogue().find(serial_number);
    if (scan_meta_data) {
      return scan_meta_data->scanConfig();
    }
    return PixA::ConfigHandle();
  }

  HistoPtr_t TestHistogramProvider::loadHistogram(EMeasurementType measurement_type,
						SerialNumber_t serial_number,
						EConnItemType conn_item_type,
						const std::string &connectivity_name,
						const std::string &histo_name, const Index_t &index)
  {
    if (measurement_type == kCurrent ) return NULL;

    unsigned int result_index = resultIndex(measurement_type);
    ResultIndex_t::const_iterator serial_iter = m_resultIndex[result_index].find(serial_number);
    if (serial_iter != m_resultIndex[result_index].end()) {
      std::unique_ptr<PixLib::Histo> a_histo_ptr;
      if (conn_item_type == kRodItem ) {
	PixA::HistoHandle histo_handle( serial_iter->second.histoList().find(connectivity_name, histo_name) );
	if (histo_handle) {
	  PixA::HistoRef histo( histo_handle.ref());
	  a_histo_ptr = std::unique_ptr<PixLib::Histo>( histo.histo( stripPath(histo_name), index) );
	}
      }
      else if (conn_item_type == kModuleItem ) {
      const PixA::PixScanHistoElmPtr_t histo_index = findHistogram(serial_iter,connectivity_name);
      if (histo_index.get()) {
	unsigned int histo_id = serial_iter->second.pixScanHistoList().histoId(histo_name);
	if (histo_id < histo_index->nHistos()) {
	  PixA::HistoHandle histo_handle(histo_index->histoHandle(histo_id));
	  if (histo_handle) {
	    PixA::HistoRef histo( histo_handle.ref());
	    std::vector<std::string> full_histo_name;
	    std::pair<PixA::EModuleNameingConvention, unsigned int> ret =
	      PixA::makePixScanHistoName( histo_index->moduleNameingScheme(),
					  histo_index->moduleId(),
					  histo_index->scanConfig(),
					  histo,
					  stripPath(histo_name),
					  full_histo_name );
	    histo_index->setModuleNameingScheme(ret.first,ret.second);
// 	    int index_arr[4];
// 	    unsigned int counter_i=0;
// 	    if (index.size()>0) {
// 	      for (;counter_i<4 && counter_i < index.size()-1; counter_i++) {
// 		index_arr[counter_i]=index[counter_i];
// 	      }
// 	      index_arr[3]=index.back();
// 	    }
// 	    else {
// 	      index_arr[3]=-1;
// 	    }
// 	    for (;counter_i<3; counter_i++) {
// 	      index_arr[counter_i]=-1;
// 	    }
//	    std::unique_ptr<PixLib::Histo> a_histo_ptr( histo.histo(full_histo_name, index_arr) );
	     a_histo_ptr = std::unique_ptr<PixLib::Histo>( histo.histo(full_histo_name, index) );
	  }
	}
      }
      }
      if (a_histo_ptr.get()) {
	return PixA::convert( a_histo_ptr.release() );
      }
    }
    return NULL;
  }

  void TestHistogramProvider::numberOfHistograms(EMeasurementType measurement_type,
					       SerialNumber_t serial_number,
					       EConnItemType conn_item_type,
					       const std::string &connectivity_name,
					       const std::string &histo_name,
					       HistoInfo_t &info)
  {
    info.reset();
    if (measurement_type == kCurrent) return;

    unsigned int result_index = resultIndex(measurement_type);
    ResultIndex_t::const_iterator serial_iter = m_resultIndex[result_index].find(serial_number);
    if (serial_iter != m_resultIndex[result_index].end()) {

      std::unique_ptr<PixLib::Histo> a_histo_ptr;
      if (conn_item_type == kRodItem) {
	PixA::HistoHandle histo_handle( serial_iter->second.histoList().find(connectivity_name, histo_name) );
	if (histo_handle) {
	  PixA::HistoRef histo( histo_handle.ref());
	  histo.numberOfHistos( stripPath(histo_name),info);
	}
      }
      else if (conn_item_type == kModuleItem ) {

      const PixA::PixScanHistoElmPtr_t histo_index = findHistogram(serial_iter,connectivity_name);
      if (histo_index.get()) {
	unsigned int histo_id = serial_iter->second.pixScanHistoList().histoId(histo_name);
	if (histo_id < histo_index->nHistos()) {
	  PixA::HistoHandle histo_handle(histo_index->histoHandle(histo_id));
	  if (histo_handle) {
	    PixA::HistoRef histo( histo_handle.ref());
	    std::vector<std::string> full_histo_name;
	    std::pair<PixA::EModuleNameingConvention, unsigned int> ret =
	      PixA::makePixScanHistoName( histo_index->moduleNameingScheme(),
					  histo_index->moduleId(),
					  histo_index->scanConfig(),
					  histo,
					  stripPath(histo_name),
					  full_histo_name );
	    histo_index->setModuleNameingScheme(ret.first,ret.second);
	    histo.numberOfHistos(full_histo_name,info);
	  }
	}
      }
      }
    }
  }

  void TestHistogramProvider::requestHistogramList(EMeasurementType measurement_type,
						 SerialNumber_t serial_number,
						 EConnItemType conn_item_type,
						 std::vector< std::string > &histogram_name_list )
  {
    assert(conn_item_type < kNConnItemTypes);

    if (conn_item_type >=kNHistogramConnItemTypes) return;
    //    histogram_name_list.clear();
    if (measurement_type == kCurrent) return;

    unsigned int result_index = resultIndex(measurement_type);
    ResultIndex_t::const_iterator serial_iter = m_resultIndex[result_index].find(serial_number);
    if (serial_iter != m_resultIndex[result_index].end()) {
      const std::vector<std::string> *name_list_ptr[kNHistogramConnItemTypes];
      for (unsigned int type_i=0; type_i<kNHistogramConnItemTypes; type_i++) name_list_ptr[type_i]=NULL;
      name_list_ptr[kModuleItem]=&(serial_iter->second.pixScanHistoList().nameList());
      name_list_ptr[kRodItem]=&(serial_iter->second.histoList().nameList());

      if (name_list_ptr[conn_item_type]) {
	for (std::vector<std::string>::const_iterator name_iter = name_list_ptr[conn_item_type]->begin();
	     name_iter != name_list_ptr[conn_item_type]->end();
	     name_iter++) {
	  histogram_name_list.push_back( *name_iter );
	}
      }

    }

  }

  const PixA::PixScanHistoElmPtr_t TestHistogramProvider::findHistogram(EMeasurementType measurement_type,
								      SerialNumber_t serial_number,
								      const std::string &connectivity_name)
  {
    unsigned int result_index = resultIndex(measurement_type);
    ResultIndex_t::const_iterator serial_iter = m_resultIndex[ result_index ].find(serial_number);
    if (serial_iter != m_resultIndex[result_index].end()) {
      return findHistogram(serial_iter, connectivity_name);
    }
    return PixA::PixScanHistoElmPtr_t();
  }

  const PixA::PixScanHistoElmPtr_t TestHistogramProvider::findHistogram(ResultIndex_t::const_iterator serial_iter, const std::string &connectivity_name) 
  {
    {
      // first try given connectivity name, then try an alias if one exists.
      const std::string *temp_name = &connectivity_name;
      for (unsigned int pass=2; pass-->0; ) {
	const PixA::PixScanHistoElmPtr_t histo_index = serial_iter->second.pixScanHistoList().moduleHistoList(*temp_name);
	if (histo_index.get()) {
	  return histo_index;
	}
	std::map<std::string, std::string>::const_iterator alias_iter = m_alias.find(connectivity_name);
	if (alias_iter == m_alias.end()) break;
	temp_name = &(alias_iter->second);
      }

      // for testing purpose:
      // is the histogram missing ?
      //
      //       if (m_histogramIsMissing.find(connectivity_name) == m_histogramIsMissing.end()) {
      // 	// should the histogram be missing ?
      // 	int value = rand();
      // 	if ((value % 100)==0) {
      // 	  m_histogramIsMissing.insert(connectivity_name);
      // 	}
      // 	else {
      // 	  // now assign a random existing histogram to the connectivity tag
      // 	  PixA::ModuleIndex_t::const_iterator selection = serial_iter->second.pixScanHistobegin();
      // 	  unsigned int counter = rand() % serial_iter->second.size();
      // 	  for (selection = serial_iter->second.begin();
      // 	       counter-->0 && selection != serial_iter->second.end();
      // 	       selection++);
      // 	  m_alias.insert(std::make_pair(connectivity_name, selection->first));
      // 	  const PixA::PixScanHistoElmPtr_t histo_index = serial_iter->second.moduleHistoList(selection->first);
      // 	  if (!histo_index.get()) {
      // 	    return histo_index;
      // 	  }
      // 	}
      //      }

    }
    return PixA::PixScanHistoElmPtr_t();
  }

  std::string TestHistogramProvider::stripPath(const std::string &histo_name)
  {
    std::string::size_type pos = histo_name.rfind("/");
    if (pos != std::string::npos && pos+1 < histo_name.size()) {
      return std::string(histo_name,pos+1,histo_name.size()-pos-1);
    }
    else {
      return histo_name;
    }
  }

  void TestHistogramProvider::checkHistogramNumber(EMeasurementType measurement_type, const std::set<SerialNumber_t> &serial_number_list) 
  {
    if (serial_number_list.empty()) return;

    const ScanMetaDataCatalogue &scan_catalogue=m_calibrationData->scanMetaDataCatalogue();

    VarDef_t n_histogram_var_def=VarDefList::instance()->getVarDef("HISTOGRAMS");

    unsigned int result_index = resultIndex(measurement_type);

//     std::cout << "INFO [TestHistogramProvider::checkHistogramNumber] check numbers " << *(serial_number_list.begin())
// 	      << " to " << *(serial_number_list.rbegin())
// 	      << "." << std::endl;

    for (std::set<SerialNumber_t>::const_iterator serial_number_iter = serial_number_list.begin();
	 serial_number_iter != serial_number_list.end();
	 serial_number_iter++) {

      ResultIndex_t::const_iterator result_iter = m_resultIndex[result_index].find(*serial_number_iter);

      //      std::cout << "INFO [TestHistogramProvider::checkHistogramNumber] check scan " << result_iter->first << "." << std::endl;
      const ScanMetaData_t *meta_data =NULL;
      if ( measurement_type == kScan ) {
	meta_data = scan_catalogue.find(result_iter->first);
	if (!meta_data) {
	  std::cout << "ERROR [TestHistogramProvider::checkHistogramNumber] No scan meta data for scan " << result_iter->first << "." << std::endl;
	}
      }

      // pixScanHistoList contains Module histograms
      {
	const PixA::HistoList_t &pix_scan_histo_list = result_iter->second.pixScanHistoList();
	unsigned int n_histos = pix_scan_histo_list.nameList().size();
	for(PixA::ModuleIndex_t::const_iterator module_iter = pix_scan_histo_list.begin();
	    module_iter != pix_scan_histo_list.end();
	    module_iter++) {

	  unsigned int histo_counter_i=0;
	  {
	    unsigned int n_module_histos=module_iter->second->nHistos();
	    for (unsigned int histo_i = 0; histo_i < n_module_histos; histo_i++) {
	      if (module_iter->second->histoHandle(histo_i))  {
		histo_counter_i++;
	      }
	    }
	  }

	  State_t histogram_state=CalibrationDataResultFileIndexer::kNStates;
	  PixCon::CalibrationData::ConnObjDataList data_list( m_calibrationData->calibrationData().createConnObjDataList(module_iter->first) );
	  try {
	    if (measurement_type == kScan) {
	      histogram_state = data_list.scanValue<State_t>(result_iter->first, n_histogram_var_def.id() );
	    }
	    else {
	      histogram_state = data_list.analysisValue<State_t>(result_iter->first, n_histogram_var_def.id() );
	    }

// 	    std::cout << "INFO [TestHistogramProvider::checkHistogramNumber] " << module_iter->first << " expected = " << n_histos
// 		      << " found " << histo_counter_i
// 		      << " stat alread set to " << histogram_state
// 		      << std::endl;

	    if (histo_counter_i != n_histos
		&& (   histogram_state == CalibrationDataResultFileIndexer::kOk
		       || histogram_state == CalibrationDataResultFileIndexer::kOkButBelowExpectation
		       || histogram_state == CalibrationDataResultFileIndexer::kOkButAboveExpectation) ) {

	      if (histo_counter_i < n_histos) {
		histogram_state = CalibrationDataResultFileIndexer::kMissingHistograms;
	      }
	      else if (histo_counter_i > n_histos && histogram_state == CalibrationDataResultFileIndexer::kOk) {
		std::cout << "ERROR [TestHistogramProvider::checkHistogramNumber] " << module_iter->first << " expected = " << n_histos
			  << " but found " << histo_counter_i << " i.e. too many. This should not happen."
			  << std::endl;
		histogram_state = CalibrationDataResultFileIndexer::kOkButAboveExpectation;
	      }


	      m_calibrationData->calibrationData().addValue(kModuleValue, module_iter->first, measurement_type, result_iter->first,"HISTOGRAMS", histogram_state);

// 	      std::cout << "INFO [TestHistogramProvider::checkHistogramNumber] " << module_iter->first << " expected = " << n_histos
// 			<< " found " << histo_counter_i
// 			<< " changed state to " << histogram_state
// 			<< std::endl;

	    }
	  }
	  catch (CalibrationData::MissingValue &err1) {

	    if (histo_counter_i == n_histos) {
	      if (n_histos == 0) {
		histogram_state = CalibrationDataResultFileIndexer::kNoneExpected;
	      }
	      else {
		histogram_state = CalibrationDataResultFileIndexer::kOk;
	      }
	    }
	    else if (histo_counter_i == 0 ) {
	      histogram_state = CalibrationDataResultFileIndexer::kPresumablyDisabled;
	    }
	    else if (histo_counter_i < n_histos) {
	      histogram_state = CalibrationDataResultFileIndexer::kMissingHistograms;
	    }
	    else if (histo_counter_i > n_histos) {
	      // pointless cannot be by construction.
	      histogram_state = CalibrationDataResultFileIndexer::kOkButAboveExpectation;
	      std::cout << "ERROR [TestHistogramProvider::checkHistogramNumber] " << module_iter->first << " expected = " << n_histos
			<< " but found " << histo_counter_i
			<< ", i.e. too many histograms. This should not happen." 
			<< std::endl;
	    }
// 	    std::cout << "INFO [TestHistogramProvider::checkHistogramNumber] " << module_iter->first << " expected = " << n_histos
// 		      << " found " << histo_counter_i
// 		      << " -> " << histogram_state
// 		      << std::endl;

	    if (measurement_type == kScan) {
	      try {
		PixCon::CalibrationData::EnableState_t &enable_state = data_list.enableState(measurement_type, result_iter->first);
		enable_state.setEnabled(true);
	      }
	      catch( PixCon::CalibrationData::MissingConnObject &err2) {
		m_calibrationData->calibrationData().setScanEnableState(module_iter->first,
									result_iter->first,
									true /*enabled_in_connectivity*/,
									true /*enabled_in_configuration*/,
									true /*parent_enabled */,
									(histogram_state != CalibrationDataResultFileIndexer::kPresumablyDisabled) /*temporary_enabled*/);
	      }
	    }

	    // @todo add auxiliary function to ConnObjDataList ?
	    if (measurement_type == kScan) {
	      m_calibrationData->calibrationData().addScanValue(kModuleValue, module_iter->first, result_iter->first,"HISTOGRAMS", histogram_state);
	    }
	    else {
	      m_calibrationData->calibrationData().addAnalysisValue(kModuleValue, module_iter->first, result_iter->first,"HISTOGRAMS", histogram_state);
	    }
	  }


	  if (meta_data) {

	    for (std::vector<SerialNumber_t>::const_iterator analysis_iter = meta_data->beginAnalysis();
		 analysis_iter !=  meta_data->endAnalysis();
		 analysis_iter++) {

	      m_calibrationData->calibrationData().addAnalysisValue(kModuleValue, module_iter->first, *analysis_iter,"HISTOGRAMS", histogram_state);

	    }
	  }

	}
      }

      // histoList contains ROD histograms
      {
	const SimpleHistoList_t &histo_list  = result_iter->second.histoList();

	unsigned int n_histos = histo_list.nameList().size();
	const std::map<std::string, std::vector< PixA::HistoHandle> > &conn_map = histo_list.histoList();
	for(std::map<std::string, std::vector< PixA::HistoHandle> >::const_iterator conn_iter = conn_map.begin();
	    conn_iter != conn_map.end();
	    conn_iter++) {
	  unsigned int histo_counter_i=0;
	  for (std::vector< PixA::HistoHandle>::const_iterator histo_iter = conn_iter->second.begin();
	       histo_iter != conn_iter->second.end();
	       histo_iter++) {
	    if (*histo_iter) {
	      histo_counter_i++;
	    }
	  }

	  State_t histogram_state= CalibrationDataResultFileIndexer::kNStates;
	  if (histo_counter_i == n_histos) {
	    histogram_state = CalibrationDataResultFileIndexer::kOk;
	  }
	  else if (histo_counter_i == 0 ) {
	    histogram_state = CalibrationDataResultFileIndexer::kPresumablyDisabled;
	  }
	  else if (histo_counter_i < n_histos) {
	    histogram_state = CalibrationDataResultFileIndexer::kMissingHistograms;
	  }
	  else if (histo_counter_i > n_histos) {
	    // pointless cannot be by construction.
	    std::cout << "ERROR [TestHistogramProvider::checkHistogramNumber] " << conn_iter->first << " expected = " << n_histos
		      << " but found " << histo_counter_i
		      << ", i.e. too many histograms. This should not happen."
		      << std::endl;
	    histogram_state = CalibrationDataResultFileIndexer::kOkButAboveExpectation;
	  }
// 	  std::cout << "INFO [TestHistogramProvider::checkHistogramNumber] " << conn_iter->first << " expected = " << n_histos 
// 		    << " found " << histo_counter_i
// 		    << " -> " << histogram_state
// 		    << std::endl;

	  PixCon::CalibrationData::ConnObjDataList data_list( m_calibrationData->calibrationData().createConnObjDataList(conn_iter->first) );
	  if (measurement_type == kScan) {
	    try {
	      PixCon::CalibrationData::EnableState_t &enable_state = data_list.enableState(measurement_type, result_iter->first);
	      enable_state.setEnabled(true);
	    }
	    catch( PixCon::CalibrationData::MissingConnObject &err) {

	      m_calibrationData->calibrationData().setScanEnableState(conn_iter->first,
								      result_iter->first,
								      true /*enabled_in_connectivity*/,
								      true /*enabled_in_configuration*/,
								      true /*parent_enabled */,
								      true /*temporary_enabled*/);

	      m_calibrationData->setRodEnable(conn_iter->first, measurement_type, result_iter->first,  true);
	    }
	  }

	  // @todo add auxiliary function to ConnObjDataList ?
	  if (measurement_type == kScan) {
	    m_calibrationData->calibrationData().addScanValue(kRodValue, conn_iter->first, result_iter->first,"HISTOGRAMS", histogram_state);
	  }
	  else {
	    m_calibrationData->calibrationData().addAnalysisValue(kRodValue, conn_iter->first, result_iter->first,"HISTOGRAMS", histogram_state);
	  }

	  if (meta_data) {

	    for (std::vector<SerialNumber_t>::const_iterator analysis_iter = meta_data->beginAnalysis();
		 analysis_iter !=  meta_data->endAnalysis();
		 analysis_iter++) {

	      m_calibrationData->calibrationData().addAnalysisValue(kRodValue, conn_iter->first, *analysis_iter,"HISTOGRAMS", histogram_state);

	    }
	  }
	}

      }
    }

  }

  void TestHistogramProvider::remove(EMeasurementType measurement_type, SerialNumber_t serial_number)
  {
    if (measurement_type!=kScan && measurement_type!=kAnalysis) return;

    unsigned int result_index = resultIndex(measurement_type);
    ResultIndex_t::iterator result_iter = m_resultIndex[result_index].find(serial_number);
    if (result_iter != m_resultIndex[result_index].end()) {
      m_resultIndex[result_index].erase(result_iter);
    }

  }

}
