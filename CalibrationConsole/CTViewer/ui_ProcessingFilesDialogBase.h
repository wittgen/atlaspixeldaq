#ifndef UI_PROCESSINGFILESDIALOGBASE_H
#define UI_PROCESSINGFILESDIALOGBASE_H

#include <QHeaderView>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QProgressBar>
#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QDialog>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QPushButton>
#include <QSpacerItem>
#include <QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ProcessingFilesDialogBase
{
public:
    QVBoxLayout *vboxLayout;
    QLabel *m_dialogTitle;
    QSpacerItem *spacer13;
    QGridLayout *gridLayout;
    QLabel *m_pathName;
    QLabel *m_fileNameLabel;
    QLabel *textLabel1;
    QLabel *m_fileName;
    QSpacerItem *spacer14;
    QProgressBar *m_progressBar;
    QSpacerItem *spacer5;
    //Q3ListView *m_problemList;
    QTreeWidget *m_problemList;
    QTreeWidgetItem *m_problemListHeaders;
    QSpacerItem *spacer8;
    QHBoxLayout *hboxLayout;
    QSpacerItem *spacer3;
    QPushButton *m_stopButton;
    QSpacerItem *spacer4;

    void setupUi(QDialog *ProcessingFilesDialogBase)
    {
        if (ProcessingFilesDialogBase->objectName().isEmpty())
            ProcessingFilesDialogBase->setObjectName(QString::fromUtf8("ProcessingFilesDialogBase"));
        ProcessingFilesDialogBase->resize(442, 290);
        vboxLayout = new QVBoxLayout(ProcessingFilesDialogBase);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        m_dialogTitle = new QLabel(ProcessingFilesDialogBase);
        m_dialogTitle->setObjectName(QString::fromUtf8("m_dialogTitle"));
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(5), static_cast<QSizePolicy::Policy>(1));
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(m_dialogTitle->sizePolicy().hasHeightForWidth());
        m_dialogTitle->setSizePolicy(sizePolicy);
        QFont font;
        font.setPointSize(14);
        font.setBold(true);
        m_dialogTitle->setFont(font);
        m_dialogTitle->setMargin(5);
        m_dialogTitle->setTextFormat(Qt::PlainText);
        m_dialogTitle->setWordWrap(false);

        vboxLayout->addWidget(m_dialogTitle);

        spacer13 = new QSpacerItem(20, 16, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout->addItem(spacer13);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        m_pathName = new QLabel(ProcessingFilesDialogBase);
        m_pathName->setObjectName(QString::fromUtf8("m_pathName"));
        QSizePolicy sizePolicy1(static_cast<QSizePolicy::Policy>(7), static_cast<QSizePolicy::Policy>(5));
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(m_pathName->sizePolicy().hasHeightForWidth());
        m_pathName->setSizePolicy(sizePolicy1);
        QFont font1;
        m_pathName->setFont(font1);
        m_pathName->setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
        m_pathName->setWordWrap(false);

        gridLayout->addWidget(m_pathName, 0, 1, 1, 1);

        m_fileNameLabel = new QLabel(ProcessingFilesDialogBase);
        m_fileNameLabel->setObjectName(QString::fromUtf8("m_fileNameLabel"));
        m_fileNameLabel->setWordWrap(false);

        gridLayout->addWidget(m_fileNameLabel, 1, 0, 1, 1);

        textLabel1 = new QLabel(ProcessingFilesDialogBase);
        textLabel1->setObjectName(QString::fromUtf8("textLabel1"));
        textLabel1->setWordWrap(false);

        gridLayout->addWidget(textLabel1, 0, 0, 1, 1);

        m_fileName = new QLabel(ProcessingFilesDialogBase);
        m_fileName->setObjectName(QString::fromUtf8("m_fileName"));
        sizePolicy1.setHeightForWidth(m_fileName->sizePolicy().hasHeightForWidth());
        m_fileName->setSizePolicy(sizePolicy1);
        m_fileName->setWordWrap(false);

        gridLayout->addWidget(m_fileName, 1, 1, 1, 1);


        vboxLayout->addLayout(gridLayout);

        spacer14 = new QSpacerItem(20, 16, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout->addItem(spacer14);

        m_progressBar = new QProgressBar(ProcessingFilesDialogBase);
        m_progressBar->setObjectName(QString::fromUtf8("m_progressBar"));

        vboxLayout->addWidget(m_progressBar);

        spacer5 = new QSpacerItem(20, 16, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout->addItem(spacer5);
/*
        m_problemList = new Q3ListView(ProcessingFilesDialogBase);
        m_problemList->addColumn(QApplication::translate("ProcessingFilesDialogBase", "Problem", 0, QApplication::UnicodeUTF8));
        m_problemList->header()->setClickEnabled(true, m_problemList->header()->count() - 1);
        m_problemList->header()->setResizeEnabled(true, m_problemList->header()->count() - 1);
        m_problemList->addColumn(QApplication::translate("ProcessingFilesDialogBase", "File", 0, QApplication::UnicodeUTF8));
        m_problemList->header()->setClickEnabled(true, m_problemList->header()->count() - 1);
        m_problemList->header()->setResizeEnabled(true, m_problemList->header()->count() - 1);*/

        m_problemList = new QTreeWidget(ProcessingFilesDialogBase);
        m_problemList->setColumnCount(2);
        m_problemListHeaders = new QTreeWidgetItem;
        m_problemListHeaders->setText(0,"Problem");
        m_problemListHeaders->setText(1,"File");
        m_problemList->header()->resizeSections(QHeaderView::ResizeToContents);
#if QT_VERSION < 0x050000
        m_problemList->header()->setClickable(true);
#else
        m_problemList->header()->setSectionsClickable(true);
#endif
        m_problemList->setObjectName(QString::fromUtf8("m_problemList"));
        m_problemList->setHeaderItem(m_problemListHeaders);



        vboxLayout->addWidget(m_problemList);

        spacer8 = new QSpacerItem(20, 21, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout->addItem(spacer8);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        spacer3 = new QSpacerItem(31, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacer3);

        m_stopButton = new QPushButton(ProcessingFilesDialogBase);
        m_stopButton->setObjectName(QString::fromUtf8("m_stopButton"));

        hboxLayout->addWidget(m_stopButton);

        spacer4 = new QSpacerItem(51, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacer4);


        vboxLayout->addLayout(hboxLayout);


        retranslateUi(ProcessingFilesDialogBase);

        QMetaObject::connectSlotsByName(ProcessingFilesDialogBase);
    } // setupUi

    void retranslateUi(QDialog *ProcessingFilesDialogBase)
    {
      ProcessingFilesDialogBase->setWindowTitle(QApplication::translate("ProcessingFilesDialogBase", "Processing Files", 0));
      m_dialogTitle->setText(QApplication::translate("ProcessingFilesDialogBase", "Processing Files", 0));
      m_pathName->setText(QApplication::translate("ProcessingFilesDialogBase", "<unspecified>", 0));
      m_fileNameLabel->setText(QApplication::translate("ProcessingFilesDialogBase", "File :", 0));
#ifndef QT_NO_TOOLTIP
      m_fileNameLabel->setProperty("toolTip", QVariant(QApplication::translate("ProcessingFilesDialogBase", "The path name of the current file", 0)));
#endif // QT_NO_TOOLTIP
      textLabel1->setText(QApplication::translate("ProcessingFilesDialogBase", "Path :", 0));
      m_fileName->setText(QApplication::translate("ProcessingFilesDialogBase", "<label>", 0));
      m_stopButton->setText(QApplication::translate("ProcessingFilesDialogBase", "Stop", 0));
    } // retranslateUi

};

namespace Ui {
    class ProcessingFilesDialogBase: public Ui_ProcessingFilesDialogBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PROCESSINGFILESDIALOGBASE_H
