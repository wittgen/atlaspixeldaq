// Dear emacs, this is -*-c++-*-
#ifndef _PixCon_TestHistogramProvider_h_
#define _PixCon_TestHistogramProvider_h_

#include "IHistogramProvider.h"
#include <DataContainer/HistoRef.h>
#include <vector>
#include <map>
#include <set>
#include <stdexcept>
#include <DataContainer/HistoList_t.h>
#include <ConfigWrapper/ScanConfig.h>

#include "CategoryList.h"

#include <memory>
#include "IFileList.h"

namespace PixCon {

  class CalibrationDataManager;
  class CalibrationDataResultFileIndexer;


  class SimpleHistoList_t
  {
  public:
    unsigned int histoId(const std::string &histo_name) const {
      unsigned int histo_id=0;
      for (std::vector<std::string>::const_iterator name_iter = m_histoNameList.begin();
	   name_iter != m_histoNameList.end();
	   name_iter++, histo_id++) {
	if (*name_iter == histo_name) return histo_id;
      }
      return s_invalidId;
    }

    bool isValid(unsigned int histo_id) const {
      return histo_id < m_histoNameList.size();
    }

    const std::string &name(unsigned int histo_id) const {
      if (isValid(histo_id)) {
	return m_histoNameList[histo_id];
      }
      return s_emptyString;
    }

    void addHistogram(const std::string &connectivity_name, const std::string &histo_name, const PixA::HistoHandle &histo_handle) {
      unsigned int histo_id = histoId(histo_name);
      if (!isValid(histo_id)) {
	unsigned int new_id = m_histoNameList.size();
	m_histoNameList.push_back(histo_name);
	if (m_histoNameList.size()!=new_id+1) {
	  throw std::runtime_error("FATAL [SimpleHistoList_t::addHistogram] Failed to add new histogram name.");
	}
	histo_id =new_id;
	std::cout << "INFO [SimpleHistoList_t::addHistogram] add new histogram " << histo_name << " -> " << new_id << std::endl;
      }

      std::vector<PixA::HistoHandle> &a_histo_list = m_histoList[connectivity_name];
      if (histo_id >= a_histo_list.size()) {
	a_histo_list.resize(histo_id+1);
      }
      if (a_histo_list[histo_id]) {
	std::cout << "WARNING [SimpleHistoList_t::addHistogram] Replacing histogram " << histo_name << " for " << connectivity_name << "." << std::endl;
      }
      a_histo_list[histo_id] = histo_handle;
      std::cout << "INFO [SimpleHistoList_t::addHistogram] add new histogram "<< histo_name << " for " << connectivity_name << " -> " << histo_id << std::endl;
    }

    PixA::HistoHandle find(const std::string &connectivity_name, const std::string &histogram_name) const {
      unsigned int histo_id = histoId(histogram_name);
      if (isValid(histo_id)) {
	std::map<std::string, std::vector< PixA::HistoHandle> >::const_iterator  iter = m_histoList.find(connectivity_name);
	if (iter != m_histoList.end()) {
	  if (histo_id < iter->second.size()) {
	    return iter->second[histo_id];
	  }
	}
      }
      return PixA::HistoHandle();
    }

    const std::vector<std::string> &nameList() const {
      return m_histoNameList;
    }

    const std::map<std::string, std::vector< PixA::HistoHandle> > histoList() const { return m_histoList; }

  private:
    static const unsigned int                   s_invalidId = static_cast<unsigned int>(-1);
    static const std::string                    s_emptyString;

    std::vector<std::string>         m_histoNameList;
    std::map<std::string, std::vector< PixA::HistoHandle> > m_histoList;
  };

  class CTHistoList_t
  {
  public:
    PixA::HistoList_t &pixScanHistoList()             { return m_pixScanHistos; }
    SimpleHistoList_t &histoList()                    { return m_histos; }

    const PixA::HistoList_t &pixScanHistoList() const { return m_pixScanHistos; }
    const SimpleHistoList_t &histoList() const        { return m_histos; }

  private:
    PixA::HistoList_t m_pixScanHistos;
    SimpleHistoList_t m_histos;
  };

  typedef std::map< SerialNumber_t, CTHistoList_t > ResultIndex_t;

  /** Histogram server implementation which servers histograms from CT-files.
   */
  class TestHistogramProvider : public IHistogramProvider
  {
  public:
    //    typedef std::vector< PixA::HistoRef > PixA::HistoList_t;
    typedef std::map< std::string, PixA::HistoList_t > ModuleList_t;
    typedef std::map< SerialNumber_t, PixA::HistoList_t > ScanList_t;

    TestHistogramProvider(std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data,
			  const std::shared_ptr<PixCon::CategoryList> &m_categories,
			  SerialNumber_t local_scan_serial_number_max,
			  SerialNumber_t local_analysis_serial_number_max);

    ~TestHistogramProvider();

    SerialNumber_t addFile(const std::string &file_name, const std::vector<std::string> &pattern_list);

    SerialNumber_t addFiles(const std::vector<std::string> &file_name_list, const std::vector<std::string> &pattern_list);

    SerialNumber_t addFiles(IFileList &file_list, const std::vector<std::string> &pattern_list);

    PixA::ConfigHandle pixScanConfig(SerialNumber_t first_serial_number);

    HistoPtr_t loadHistogram(EMeasurementType type,
			     SerialNumber_t serial_number,
			     EConnItemType conn_item_type,
			     const std::string &connectivity_name,
			     const std::string &histo_name,
			     const Index_t &index);

    void requestHistogramList(EMeasurementType type,
			      SerialNumber_t serial_number,
			      EConnItemType conn_item_type,
			      std::vector< std::string > &histogram_name_list );

    void requestHistogramList(EMeasurementType type,
			      SerialNumber_t serial_number,
			      EConnItemType conn_item_type,
			      std::vector< std::string > &histogram_name_list,
			      const std::string &/*rod_name*/) {
      // not needed
      // just request the histogram name list of all RODS which should be the same anyway.
      requestHistogramList(type, serial_number,conn_item_type, histogram_name_list);
   }

    void numberOfHistograms(EMeasurementType type,
			    SerialNumber_t serial_number,
			    EConnItemType conn_item_type,
			    const std::string &connectivity_name,
			    const std::string &histo_name,
			    HistoInfo_t &info);

    void clearCache(EMeasurementType type,
		    SerialNumber_t serial_number) {
      remove(type, serial_number);
    }

    const std::shared_ptr<PixCon::CategoryList> &categories() { return m_categories; }

    static std::string stripPath(const std::string &histo_name);

    SerialNumber_t localScanSerialNumberMax() { return m_localScanSerialNumberMax;}
    SerialNumber_t localAnalysisSerialNumberMax() { return m_localAnalysisSerialNumberMax;}

    void remove(EMeasurementType measurement_type, SerialNumber_t serial_number);

    void reset() { /* do nothing */}
  protected:
    void checkHistogramNumber(EMeasurementType measurement_type, const std::set<SerialNumber_t> &serial_number_list);

    SerialNumber_t finishFiles(std::unique_ptr<CalibrationDataResultFileIndexer> &result_file_indexer);

  private:
    const PixA::PixScanHistoElmPtr_t findHistogram(EMeasurementType measurement_type, SerialNumber_t serial_number, const std::string &connectivity_name) ;
    const PixA::PixScanHistoElmPtr_t findHistogram(ResultIndex_t::const_iterator serial_iter, const std::string &connectivity_name);

    std::set<std::string>                             m_histogramIsMissing;
    std::map<std::string, std::string>                m_alias;
    std::shared_ptr<PixCon::CalibrationDataManager> m_calibrationData;
    std::shared_ptr<PixCon::CategoryList>           m_categories;

    unsigned int resultIndex(EMeasurementType measurement_type) {
      assert(measurement_type==kScan || measurement_type == kAnalysis );
      return (measurement_type == kAnalysis ? 1 : 0);
    }

    SerialNumber_t m_localScanSerialNumberMax;
    SerialNumber_t m_localAnalysisSerialNumberMax;

    ResultIndex_t m_resultIndex[2];
  };

}


#endif
