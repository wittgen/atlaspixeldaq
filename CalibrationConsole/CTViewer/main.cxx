#include <QApplication>
#if QT_VERSION < 0x050000
  #include <QPlastiqueStyle>
#endif
#include "CTViewerApp.h"

#include <iostream>

#include <VisualisationRegister.h>
#include <ModuleMapH2Factory.h>
#include "HistogramView.h"

#include <ConfigWrapper/ConnectivityManager.h>
#include <ConfigWrapper/createDbServer.h>

#if defined(HAVE_QTGSI)
#include "TQRootApplication.h"
#include "TQApplication.h"
#else
#include <TApplication.h>
#endif

#include <TEnv.h>
#include <TSystem.h>
#include <TROOT.h>

const std::string s_appName("CTViewer");
  
int main( int argc, char ** argv )
{
#if QT_VERSION < 0x050000
    QApplication::setStyle(new QPlastiqueStyle());
#else
    QApplication::setStyle("fusion");
#endif

    std::shared_ptr<PixCon::UserMode> user_mode(new PixCon::UserMode(PixCon::UserMode::kShifter));
    std::string partition_name;
    std::string object_tag_name="CT";
    std::string tag_name;
    std::string cfg_tag_name;
    std::string mod_cfg_tag_name;
    std::string is_name = "PixFakeData";
    std::string db_server_partition_name;
    std::string db_server_name;
    bool error=false;

    std::vector<std::string> result_files;
    std::vector<std::string> pattern;
    for (int arg_i=1; arg_i<argc; arg_i++) {
      if (strcmp(argv[arg_i],"-e")==0) {
	user_mode->setUserMode(PixCon::UserMode::kExpert);
      }
      else if (strcmp(argv[arg_i],"-p")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	partition_name = argv[++arg_i];
      }
      else if (strcmp(argv[arg_i],"-i")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	object_tag_name = argv[++arg_i];
      }
      else if (strcmp(argv[arg_i],"-t")==0 && arg_i+2<argc && argv[arg_i+2][0]!='-' && argv[arg_i+1][0]!='-') {
	tag_name = argv[++arg_i];
	cfg_tag_name = argv[++arg_i];
	if (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	  mod_cfg_tag_name = argv[++arg_i];
	}
      }
      else if (strcmp(argv[arg_i],"-e")==0  && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	++arg_i;
	pattern.push_back(argv[arg_i]);
      }
      else if (strcmp(argv[arg_i],"-n")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	is_name = argv[++arg_i];
      }
      else if (strcmp(argv[arg_i],"-f")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	while (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	  ++arg_i;
	  result_files.push_back(argv[arg_i]);
	  if (!result_files.back().empty() && result_files.back()[0]=='\\') {
	    result_files.back().erase(0,1);
	  }
	}
      }
      else if ( (strcmp(argv[arg_i],"-D")==0
		 || strcmp(argv[arg_i],"--db-server-name")==0)
		&& arg_i+1<argc && argv[arg_i+1][0]!='-') {
	db_server_name = argv[++arg_i];
      }
      else if ( (strcmp(argv[arg_i],"--db-server-partition")==0
		 || strcmp(argv[arg_i],"-p")==0)
		&& arg_i+1<argc && argv[arg_i+1][0]!='-') {
	db_server_partition_name = argv[++arg_i];
      }
      else if (strcmp(argv[arg_i],"--single-fe")==0 || strcmp(argv[arg_i],"-1")==0) {
	PixCon::HistogramView::setOneFrontEndIsDefault();
      }
      else {
	std::cout << "ERROR ["<< argv[0] << ":main] unhandled argument " << arg_i << " : " << argv[arg_i]<< "." << std::endl;
	error=true;
      }
    }

    if (error ) {
      std::cout << "usage: " << argv[0] << "[-p partition]  [-n \"IS server name\"] [--single-fe/-1] [-e matching-folder-expression]" << std::endl;
      return -1;
    }
    int ret=0;

    // QT-ROOT application
#if  defined(HAVE_QTGSI)
    int dummy_argc=1;
    TQApplication root_app(s_appName.c_str(),&dummy_argc,argv);
    
    // process root logon script
    // the logonscript could for example define some proper styles.
    
    std::cout << gEnv->GetValue("Rint.Logon", (char*)0) << std::endl;
    const char *logon = gEnv->GetValue("Rint.Logon", (char*)0);
    if (logon) {
      char *mac = gSystem->Which(TROOT::GetMacroPath(), logon, kReadPermission);
      if (mac) {
	root_app.ProcessFile(logon);
      }
      delete [] mac;
    }
#else
    int dummy_argc=1;
    TApplication root_app(s_appName.c_str(),&dummy_argc,argv);

    // process root logon script
    // the logonscript could for example define some proper styles.
    std::cout << "INFO [main:" << argv[0] << "] execute ROOT macros from : " << gEnv->GetValue("Rint.Logon", (char*)0) << std::endl;
    const char *logon = gEnv->GetValue("Rint.Logon", (char*)0);
    if (logon) {
      char *mac = gSystem->Which(TROOT::GetMacroPath(), logon, kReadPermission);
      if (mac) {
	root_app.ProcessFile(logon);
      }
      delete [] mac;
    }

#endif


    // in case no partition was chosen specifically use the default
    // partition.
    if (db_server_partition_name.empty() && !partition_name.empty()) {
      db_server_partition_name = partition_name;
    }

    // Start IPCCore
    if (!partition_name.empty() || !db_server_partition_name.empty()) {
      IPCCore::init(argc, argv);
    }

    std::unique_ptr<IPCPartition> db_server_partition;
    if (!db_server_name.empty() && !db_server_partition_name.empty()) {
      db_server_partition = std::make_unique<IPCPartition>(db_server_partition_name.c_str());
      if (!db_server_partition->isValid()) {
	std::cerr << "ERROR [" << argv[0] << ":main] Not a valid partition " << db_server_partition_name
		  << " for the db server." << std::endl;
	return EXIT_FAILURE;
      }

      std::shared_ptr<PixLib::PixDbServerInterface> db_server( PixA::createDbServer(*db_server_partition,
										      db_server_name ));
      if (db_server.get()) {
	PixA::ConnectivityManager::useDbServer( db_server );
      }
      else {
	std::cout << "WARNING [main:" << argv[0] << "] Failed to create the db server." << std::endl;
      }
    }
    
      std::unique_ptr<QApplication> app;
    {
      std::shared_ptr<PixA::IModuleMapH2Factory> factory(new PixA::ModuleMapH2Factory);
      PixA::VisualisationRegister::registerMAVisualisers(factory);

      int qt_argc=1;
      CTViewerApp *ct_viewer=new CTViewerApp(qt_argc, argv, user_mode, result_files, pattern);
      app = std::unique_ptr<QApplication>(ct_viewer);
      if (!object_tag_name.empty() && !tag_name.empty() && !cfg_tag_name.empty()) {
	
       ct_viewer->setTags(object_tag_name, tag_name,cfg_tag_name, mod_cfg_tag_name);
      }
    }

    ret  = (app.get() ? app->exec() : -1);
    return ret;
}
