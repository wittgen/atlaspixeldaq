#include "ProcessingFilesDialog.h"

// debugging


namespace PixCon {

  ProcessingFilesDialog::ProcessingFilesDialog( QApplication &app,
						const std::string &title,
						const std::string file_name,
						QWidget* parent )
    : QDialog(parent),
      m_app(&app),
      m_timer(new QTimer(this)),
      m_currentFile(file_name),
      m_counter(0),
      m_timerStarted(false),
      m_finished(false),
      m_abort(false),
      m_appliedVeto(false)
  {
    setupUi(this);

    m_problemList->hide();
    m_dialogTitle->setText(QString::fromStdString(title));
    m_fileName->setText("");
    m_pathName->setText("");
    m_stopButton->setText("Stop");

    //    m_messages->setText("");
    connect(m_timer, SIGNAL(timeout()), this, SLOT(update()));
    connect(m_stopButton, SIGNAL(clicked()), this, SLOT(stop()));
    hide();
  }

  ProcessingFilesDialog::~ProcessingFilesDialog() {
    emit releaseVeto();
  }

  bool ProcessingFilesDialog::event(QEvent *event) {
    if (event->type()==QEvent::User) {
      {
	NewFileEvent *new_file_event=dynamic_cast<NewFileEvent *>(event);
	if (new_file_event) {
	  m_timer->setSingleShot(true);
	  m_timer->start((s_update > 0) ? s_update : 0);
	  return true;
	}
      }

      {
	ProblemEvent *problem_event=dynamic_cast<ProblemEvent *>(event);
	if (problem_event) {
	  m_stopButton->setEnabled(true);
	  m_appliedVeto=true;
	  emit vetoAbort();
      //new Q3ListViewItem(m_problemList,QString::fromStdString(problem_event->problem()), QString::fromStdString(problem_event->fileName()));
          new QTreeWidgetItem(m_problemList,{QString::fromStdString(problem_event->problem()), QString::fromStdString(problem_event->fileName())});
	  m_problemList->show();
	  if (isHidden()) {
	    show();
	  }

	  return true;
	}
      }

      {
	FinishedEvent *finish_event=dynamic_cast<FinishedEvent *>(event);
	if (finish_event) {
	  if (isHidden() && m_appliedVeto) {
	    show();
	  }
	  m_finished=true;
	  m_stopButton->setEnabled(true);
	  m_stopButton->setText("Done");
	  return true;
	}
      }
    }
    return ProcessingFilesDialog::event(event);
  }

  void ProcessingFilesDialog::show() {
    update();
    QDialog::show();
  }

  void ProcessingFilesDialog::update() {
    m_timerStarted=false;
    Lock lock(m_mutex);
    std::string::size_type pos = m_currentFile.rfind("/");
    if (pos != std::string::npos) {
      std::string path_name = m_currentFile.substr(0,pos);
      if (m_pathName->text().compare( path_name.c_str())!=0) {
	m_pathName->setText( QString::fromStdString(path_name));
      }
    }
    else {
      pos=0;
    }

    //if (m_problemList->firstChild() && m_problemList->isHidden()) {
    if (m_problemList->topLevelItem(0) && m_problemList->isHidden()) {
      m_problemList->show();
    }

    m_fileName->setText( QString::fromStdString(m_currentFile.substr(pos, m_currentFile.size() - pos )));
    //m_progressBar->setProgress( m_counter);
    m_progressBar->setValue(m_counter);
    if ((m_counter>=m_size && m_size>0) || m_finished) {
      QApplication::restoreOverrideCursor();
      if (!m_stopButton->isEnabled()) {
	m_stopButton->setEnabled(true);
      }
      m_stopButton->setText("Ok");
    }
  }

  void ProcessingFilesDialog::stop() {
    m_abort=true;
    m_stopButton->setEnabled(false);
    if ((m_counter>=m_size  && m_size>0) || m_finished) {
      //      if (m_problemList->firstChild()) {
      emit releaseVeto();
      //      }
    }
  }



  const std::string ProcessingFilesDialog::MonitoredFileList::fileName() {
    if (m_dialog->abort()) {
      return "";
    }
    return FileList::fileName();
  }

  void ProcessingFilesDialog::MonitoredFileList::next() {
    FileList::next();
    Lock lock(m_dialog->mutex());
    m_dialog->setCurrentFile(fileName());
    m_dialog->setCounter(counter() );
    m_dialog->newFile();
  }


  void ProcessingFilesDialog::setCounterMax(unsigned int counter_max)
  {
    m_size=counter_max;
    //m_progressBar->setTotalSteps(counter_max);
    m_progressBar->setMaximum(counter_max);
  }

}
