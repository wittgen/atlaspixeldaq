#ifndef _AddDirectoryForm_h_
#define _AddDirectoryForm_h_

#include "ui_AddDirectoryFormBase.h"
#include <Highlighter.h>
#include <memory>

//class Q3ListViewItem;

namespace PixCon {

  class AddDirectoryForm  : public QDialog, public Ui_AddDirectoryFormBase
  {    Q_OBJECT
  public:
    AddDirectoryForm( const std::string &start_path, QWidget* parent = 0 );
public slots:
    void deletePattern();
    void addPattern();
    void addPattern(const QString &pattern_string);

    void openDirectoryBrowser();
    void verifyDirectoryNameAndAccept();

    void  toggleAddDeleteButtonEnable();
public:
    std::string getDirectoryName();
    void getPatternList( QStringList &pattern_list );

  private:
    std::unique_ptr< Highlighter > m_highlighter;

  };


}

#endif
