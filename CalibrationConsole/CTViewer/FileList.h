#ifndef _PixCon_FileList_h_
#define _PixCon_FileList_h_

#include "IFileList.h"
#include <QStringList>
#include <string>
#include <iostream>

namespace PixCon {

  class FileList : public IFileList
  {
  public:
    FileList(const QStringList &file_list, const std::string &path, const char sep='/')
      : m_iter(file_list.begin()),
	m_end(file_list.end()),
	m_path(path),
	m_nElements(file_list.size()),
	m_counter(0)
    { if (!m_path.empty() && m_path[path.size()-1]!=sep)  m_path += sep; }

    const std::string fileName()
    {
      if (m_iter!=m_end) {
	m_temp=m_path;
	m_temp += (*m_iter).toLatin1().data(); 
      }
      else {
	m_temp="";
      }
      return m_temp;
    }

    void next() { if (m_iter != m_end) { ++m_iter; ++m_counter;};}

    unsigned int size() const { return m_nElements;}
    unsigned int counter() const { return m_counter;}

    void assignProblem(const std::string &problem) {
      std::cout << "INFO [FileVector::hasProblem] For " << fileName() << " the following problem occurred : " << problem << std::endl;
    }

  private:
    QStringList::const_iterator m_iter;
    QStringList::const_iterator m_end;
    std::string m_path;
    std::string m_temp;
    unsigned  int m_nElements;
    unsigned  int m_counter;
  };

}
#endif
