#ifndef _PixCon_CTViewer_h_
#define _PixCon_CTViewer_h_

#include "ScanMainPanel.h"
#include <CalibrationDataTypes.h>
#include "UserMode.h"

namespace PixCon {

  class TestHistogramProvider;

  class CTViewer : public ScanMainPanel
  {
    Q_OBJECT

  public:
  CTViewer( std::shared_ptr<PixCon::UserMode>& user_mode, 
	    const std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data,
	    PixCon::TestHistogramProvider &ct_histogram_server,
	    const std::shared_ptr<PixCon::HistogramCache> &histogram_cache,
	    const std::shared_ptr<PixCon::CalibrationDataPalette> &palette,
	    const std::shared_ptr<PixCon::CategoryList> &categories,
	    const std::shared_ptr<PixCon::IContextMenu> &context_menu,
	    const std::shared_ptr<PixCon::VisualiserList> &visualiser_list,
	    QApplication &application,
	    QWidget* parent = 0);

    ~CTViewer() {}

    void addFile( const std::string &a_file, const std::vector<std::string> &std_pattern_list);

    void removeMeasurement(PixCon::EMeasurementType measurement_type, PixCon::SerialNumber_t serial_number);

  public slots:
    void openFile();
    void openDirectory();

    void addFiles( const QString &path, const QStringList &file_list, const QStringList &pattern_list);

    void addMeasurement() { openDirectory(); }

  protected:
    QString defaultPath();
    void updateCatalogue(SerialNumber_t serial_number_begin, SerialNumber_t serial_number_end);

  private:

    QString m_lastPath;
    TestHistogramProvider *m_ctHistogramProvider;

    std::shared_ptr<PixCon::CalibrationDataManager> m_calibrationData;

    QStringList m_lastPatternList;
  };

}
#endif
