#ifndef UI_ENTERPATTERNFORMBASE_H
#define UI_ENTERPATTERNFORMBASE_H

#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QDialog>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QSpacerItem>
#include <QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_EnterPatternFormBase
{
public:
    QVBoxLayout *vboxLayout;
    QHBoxLayout *hboxLayout;
    QLabel *textLabel2;
    QLineEdit *m_patternString;
    QHBoxLayout *hboxLayout1;
    QSpacerItem *spacer2;
    QPushButton *m_cancelButton;
    QPushButton *m_okayButton;
    QSpacerItem *spacer3;

    void setupUi(QDialog *EnterPatternFormBase)
    {
        if (EnterPatternFormBase->objectName().isEmpty())
            EnterPatternFormBase->setObjectName(QString::fromUtf8("EnterPatternFormBase"));
        EnterPatternFormBase->resize(303, 97);
        vboxLayout = new QVBoxLayout(EnterPatternFormBase);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        textLabel2 = new QLabel(EnterPatternFormBase);
        textLabel2->setObjectName(QString::fromUtf8("textLabel2"));
        textLabel2->setWordWrap(false);

        hboxLayout->addWidget(textLabel2);

        m_patternString = new QLineEdit(EnterPatternFormBase);
        m_patternString->setObjectName(QString::fromUtf8("m_patternString"));

        hboxLayout->addWidget(m_patternString);


        vboxLayout->addLayout(hboxLayout);

        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setSpacing(6);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        spacer2 = new QSpacerItem(31, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout1->addItem(spacer2);

        m_cancelButton = new QPushButton(EnterPatternFormBase);
        m_cancelButton->setObjectName(QString::fromUtf8("m_cancelButton"));

        hboxLayout1->addWidget(m_cancelButton);

        m_okayButton = new QPushButton(EnterPatternFormBase);
        m_okayButton->setObjectName(QString::fromUtf8("m_okayButton"));
        m_okayButton->setDefault(true);

        hboxLayout1->addWidget(m_okayButton);

        spacer3 = new QSpacerItem(41, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout1->addItem(spacer3);


        vboxLayout->addLayout(hboxLayout1);


        retranslateUi(EnterPatternFormBase);
        QObject::connect(m_cancelButton, SIGNAL(clicked()), EnterPatternFormBase, SLOT(reject()));
        QObject::connect(m_okayButton, SIGNAL(clicked()), EnterPatternFormBase, SLOT(accept()));

        QMetaObject::connectSlotsByName(EnterPatternFormBase);
    } // setupUi

    void retranslateUi(QDialog *EnterPatternFormBase)
    {
      EnterPatternFormBase->setWindowTitle(QApplication::translate("EnterPatternFormBase", "Enter Pattern", 0));
      textLabel2->setText(QApplication::translate("EnterPatternFormBase", "Pattern :", 0));
      m_cancelButton->setText(QApplication::translate("EnterPatternFormBase", "Cancel", 0));
      m_okayButton->setText(QApplication::translate("EnterPatternFormBase", "Okay", 0));
    } // retranslateUi

};

namespace Ui {
    class EnterPatternFormBase: public Ui_EnterPatternFormBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ENTERPATTERNFORMBASE_H
