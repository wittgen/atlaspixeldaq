#ifndef QT_ROTATED_TEXT_H
#define QT_ROTATED_TEXT_H

#include <qcanvas.h>
#include <qstring.h>
#include <qpainter.h>

class RotatedText : public QCanvasText{

 public:
  QtRotatedText(QString &text, QCanvas *canvas) : QCanvasText(text,
canvas){};
    virtual ~QtRotatedText(){};
    virtual QRect boundingRect();
 protected:
  void draw(QPainter &p);
};
#endif


