#include "StatusStyle.h"

namespace PixDet {

  QPen StatusStyle::s_pens[2] = { QPen(QColor(0,0,0),1), QPen(QColor(0,0,255),2) };
  std::vector<StatusStyle> StatusStyle::s_statusStyleList;

  bool StatusStyle::s_initialised = StatusStyle::initialise();

  bool StatusStyle::initialise() {
    s_statusStyleList.resize(kNStati);

    s_statusStyleList[kUnknown]=StatusStyle(QColor(255,248,51));
    s_statusStyleList[kReserved]=StatusStyle(QColor(0,0,0));
    s_statusStyleList[kEnabled]=StatusStyle(QColor(65,205,120));
    s_statusStyleList[kDisabled]=StatusStyle(QColor(200,200,200));
    s_statusStyleList[kScanningWasOkay]=StatusStyle(QColor(0,255,0),Qt::Dense4Pattern);
    s_statusStyleList[kScanningWasNotOkay]=StatusStyle(QColor(255,0,0),Qt::Dense4Pattern);
    s_statusStyleList[kEvaluationWasOkay]=StatusStyle(QColor(0,255,0),Qt::Dense2Pattern);
    s_statusStyleList[kEvaluationWasNotOkay]=StatusStyle(QColor(255,0,0),Qt::Dense2Pattern);
    s_statusStyleList[kTransitionWasOkay]=StatusStyle(QColor(7,255,190));
    s_statusStyleList[kTransitionWasNotOkay]=StatusStyle(QColor(255,7,168));
    s_statusStyleList[kPassed]=StatusStyle(QColor(0,255,0));
    s_statusStyleList[kFailed]=StatusStyle(QColor(255,0,0));
    s_statusStyleList[kReserved1]=StatusStyle(QColor(0,0,0));
    s_statusStyleList[kWarning]=StatusStyle(QColor(255,195,7));
    s_statusStyleList[kReserved2]=StatusStyle(QColor(0,0,0));
    s_statusStyleList[kAlarm]=StatusStyle(QColor(255,0,255));
    return true;
  }
}
