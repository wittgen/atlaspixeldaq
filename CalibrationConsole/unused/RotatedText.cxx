#include "RotatedText.h"

void RotatedText::draw(QPainter &p){
   // Draw text string 'label at 'origin' with rotation 'orient'
   p.save();
   p.translate(origin.x, origin.y);
   p.rotate(90);
   //   paint.drawText(0, 0, label);
   QCanvasText::draw(p);
   p.restore();

   p.restore();
}

QRect RotatedText::boundingRect() {
  QRect bounding = QCanvasText::boundingRect();
  return QRect(bounding.x()-bounding.height(),bounding.y()+bounding.heigh()-bounding.width(),bounding.height(),bounding.width());
}
