#ifndef _Status_h_
#define _Status_h_

namespace PixDet {
  enum EStatus {kUnknown,kReserved,
		kEnabled,kDisabled,
		kScanningWasOkay, kScanningWasNotOkay,
		kEvaluationWasOkay, kEvaluationWasNotOkay,
		kTransitionWasOkay, kTransitionWasNotOkay,
		kPassed, kFailed,
		kReserved1,kWarning,
		kReserved2,kAlarm,
		kNStati };

  typedef EStatus Status_t;
}

#endif
