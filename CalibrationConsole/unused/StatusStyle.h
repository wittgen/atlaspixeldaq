#ifndef _StatusStyle_h_
#define _StatusStyle_h_

#include <qbrush.h>
#include <qpen.h>
#include "Status_t.h"

#include <vector>
#include <cassert>

namespace PixDet {

  class StatusStyle
  {
  public:
    StatusStyle( ) {}
    StatusStyle(const QColor &color, QBrush::BrushStyle style = QBrush::SolidPattern ) : m_brush(color,style) {}

    const QBrush &brush() const {return m_brush;}

    static const StatusStyle &style(Status_t status) {
      assert(static_cast<unsigned int>(status) < s_statusStyleList.size());
      return s_statusStyleList[status];
    }

    static QPen &selectedItem() {
      return s_pens[1];
    }

    static QPen &normalItem() {
      return s_pens[0];
    }
    static bool initialise();

  private:
    QBrush m_brush;

    static QPen s_pens[2];
    static std::vector<StatusStyle> s_statusStyleList;
    static bool s_initialised;
  };

}

#endif
