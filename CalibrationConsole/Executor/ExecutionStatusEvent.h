#ifndef _PixCon_ExecutionStatusEvent_h_
#define _PixCon_ExecutionStatusEvent_h_

#include <qevent.h>
#include <CommandExecutor.h>

namespace PixCon {

  class ExecutionStatusEvent : public QEvent
  {
  public:
    enum EStatus {kRunning, kFinished, kCommandSucceeded, kCommandFailed, kNextCommand, kStopped, kAborted, kKilled, kException, kWaiting};
    ExecutionStatusEvent(EStatus status, CommandNode::Id_t id, const std::string &message)
      : QEvent(QEvent::User),
        m_status(status),
	m_id(id),
	m_message(message)
    {}

//    const std::string &commandName() const { return m_commandName; }
    CommandNode::Id_t id() const           { return m_id; }
    EStatus status() const                 { return m_status; }
    const std::string &message() const     { return m_message; }

  private:

    EStatus m_status;
    CommandNode::Id_t m_id;
//    const std::string m_commandName;
    const std::string m_message;
  };

}
#endif
