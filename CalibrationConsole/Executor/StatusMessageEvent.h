/* Dear emacs, this is -*-c++-*- */
#ifndef _PixCon_StatusMessageEvent_h_
#define _PixCon_StatusMessageEvent_h_

#include <qstring.h>
#include <qevent.h>

namespace PixCon {

  class StatusMessageEvent : public QEvent
  {
  public:
    StatusMessageEvent(const QString &status_message)
      : QEvent(User),
	m_statusMessage(status_message)
    {}

    const QString &statusMessage() {return m_statusMessage; }

  private:
    QString m_statusMessage;
  };

}
#endif
