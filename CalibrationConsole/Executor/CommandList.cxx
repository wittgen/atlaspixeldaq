#include "CommandList.h"
#include <QMessageBox>
#include <QMenu>

#include "ICommandFactory.h"
#include "CommandSequence.h"
#include "ExecutionStatusEvent.h"
#include "StatusMessageEvent.h"

#include <sstream>

#include <iomanip>

namespace PixCon {

  std::string makeString(CommandNode::Id_t id) {
    std::stringstream name;
    name << std::setw(4) << std::setfill('0') << id;
    return name.str();
  }

  class CommandItem : public QTreeWidgetItem
  {
  public:
    CommandItem( QTreeWidget *list, const std::string &command_name, CommandNode::Id_t command_id)
      : QTreeWidgetItem(list),
	m_id(command_id)
    {
      setText(0, QString::fromStdString(makeString(command_id)));
      setText(1, QString::fromStdString(command_name));
      setText(2, "");
    }

    CommandItem( QTreeWidgetItem *item, const std::string &command_name, CommandNode::Id_t command_id)
      : QTreeWidgetItem(item),
	m_id(command_id)
    {
      setText(0, QString::fromStdString(makeString(command_id)));
      setText(1, QString::fromStdString(command_name));
      setText(2, "");
    }

    CommandItem( QTreeWidget *list, QTreeWidgetItem *after, const std::string &command_name, CommandNode::Id_t command_id)
      : QTreeWidgetItem(list, after),
	m_id(command_id)
    {
      setText(0, QString::fromStdString(makeString(command_id)));
      setText(1, QString::fromStdString(command_name));
      setText(2, "");
    }


    CommandNode::Id_t commandId() const {return m_id;}
    void setStatus(const std::string &name ) { setText(2,name.c_str());}
    void setPassed(unsigned int passed ) { setText(3,QString::fromStdString(makeString(passed)) );}

  private:
    CommandNode::Id_t m_id;
  };


  class BranchItem : public QTreeWidgetItem
  {
  public:
    BranchItem( QTreeWidgetItem *parent, const std::string &branch_name, unsigned int branch_i, CommandNode::Id_t command_id)
      : QTreeWidgetItem(parent),
	m_id(command_id),
	m_branch(branch_i)
    {
      setText(0, QString::fromStdString(makeString(command_id)));
      setText(1, QString::fromStdString(branch_name ));
    }

    void setConnection(CommandNode::Id_t command_id) {m_id=command_id; setText(0,QString::fromStdString(makeString(command_id)));}
    CommandNode::Id_t commandId() const {return m_id; }
    unsigned int branchId()             {return m_branch; }

  private:
    CommandNode::Id_t m_id;
    unsigned int      m_branch;
  };

  class CommandListCommandListener : public ICommandListener {
  public:
    CommandListCommandListener(QTreeWidget &command_list_view) 
      : m_commandListView(&command_list_view),
	m_currentItem(NULL)
    {
    }

    void command(const std::string &command_name, CommandNode::Id_t id) {
      if (m_currentItem) {
	m_currentItem=new CommandItem(m_commandListView, m_currentItem, command_name, id);
      }
      else {
	std::cout << "INFO [CommandListCommandListener::command]  add command " << command_name << std::endl;
	m_currentItem=new CommandItem(m_commandListView, command_name, id);
      }
      m_classificationName="";
      for(int i=0; i<m_commandListView->columnCount(); i++)
	m_commandListView->resizeColumnToContents(i);
    }

    void branch(const std::string &classification_name, const std::string &branch_name, unsigned int branch_i, CommandNode::Id_t id) {
      if (m_currentItem) {
	if (m_classificationName.empty() || m_classificationName==classification_name) {
	  m_classificationName = classification_name;
	  new BranchItem( m_currentItem, branch_name, branch_i, id);
	}
      }
      for(int i=0; i<m_commandListView->columnCount(); i++)
	m_commandListView->resizeColumnToContents(i);
    }

  private:
    QTreeWidget *m_commandListView;
    CommandItem *m_currentItem;
    std::string m_classificationName;
  };

  CommandList::CommandList(std::shared_ptr<PixCon::UserMode>& user_mode,
			   const std::shared_ptr<CommandExecutor> &executor,
			   const std::shared_ptr<ICommandFactory> &factory,
			   QWidget* parent)
: QDialog(parent),
      m_userMode(user_mode),
      m_executor(executor),
      m_factory(factory),
      m_stopButtonMode(kDisabled),
      m_lastActiveItem(NULL),
      m_lastNextItem(NULL)
  {
    setupUi(this); 
    //m_commandListView->setSelectionBehavior(QAbstractItemView::SelectItems);
    m_commandListView->setSelectionMode(QAbstractItemView::MultiSelection);
    m_commandListView->setSortingEnabled(false); // disable sorting 
    m_commandListView->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(m_commandListView , SIGNAL( customContextMenuRequested (const QPoint &)),  this, SLOT(contextMenu(const QPoint&) ) );
    connect(m_quitButton, SIGNAL(clicked()),  this, SLOT(accept()));
    connect(m_abortButton, SIGNAL(clicked()), this, SLOT(abortCommand()));
    connect(m_stopButton, SIGNAL(clicked()),  this, SLOT(stopCommand()));
    
    delete m_abortButton;
    delete m_quitButton;

    setButton();
    show();
  }


  CommandList::~CommandList() {}

  void CommandList::abortCommand() {}


  void CommandList::executeCommand(ICommand *command, IClassification *classification, bool start) {
    CommandSequence *command_sequence( dynamic_cast<CommandSequence *>(command) );
    if (command_sequence) {
      delete classification;
      executeCommandSequence(command_sequence);
    }
    else if (command) {
      try {
	CommandNode::Id_t id = m_executor->appendCommand(command, classification, 0);
	if (m_commandListView->topLevelItemCount()>0) {
	  new CommandItem(m_commandListView, m_commandListView->topLevelItem(m_commandListView->topLevelItemCount()-1), command->name(),id);
	}
	else {
	  new CommandItem(m_commandListView, command->name(),id);
	}
	if (!m_executor->isRunning() && start) {
	  m_executor->jump(id);
	  m_executor->continueProcessing();
	}
      }
      catch (std::exception &err) {
	std::string message;
	message += "Failed to add the command :\n";
	message += err.what();

	delete command;
	QMessageBox::warning(this,"ERROR [CommandList::exexcuteCommand]",message.c_str(),"OK");
      }
      catch (...) {
	delete command;
	QMessageBox::warning(this,"ERROR [CommandList::exexcuteCommand]","Failed to add the command.","OK");
      }
    }
  }

  void CommandList::executeCommandSequence(CommandSequence *command_sequence) {
    std::pair<ICommand *, IClassification *> next_command;
    while ( (next_command = command_sequence->takeFrontCommand()).first ) {
      executeCommand(next_command.first, next_command.second);
    }
  }

  void CommandList::contextMenu(const QPoint &pos) {
    QTreeWidgetItem *item = m_commandListView->itemAt( pos );

    if(item==0) { // click in empty part, un-select all
      m_commandListView->clearSelection();
    }

    if (item && !item->isSelected()) {
      item->setSelected(true);
    }

    QMenu context_menu_q4("Command");

    SelectedItems_t selected_items(nSelectedItems(m_commandListView));

    std::vector<QAction*> aCommands;
    QList <QVariant> qCounter_List;
    bool is_classification=false;
    
    if (selected_items.oneBranch() || selected_items.nothingSelected()) {
      for(unsigned int command_i=0; command_i < m_factory->nCommands(); command_i++) {
           
	QVariant qCounter(kCommand+command_i);
	QVariant qName(QString::fromStdString(m_factory->commandName(command_i)) );
	QVariant qClassification(is_classification);
	qCounter_List.append(qCounter);
	qCounter_List.append(qName);
	qCounter_List.append(qClassification);
	aCommands.push_back( context_menu_q4.addAction( QString::fromStdString(m_factory->commandName(command_i)), this, SLOT(sCommands() ) ) );
	aCommands.at(command_i)->setData(qCounter_List);
	qCounter_List.clear();
	
	if ( m_userMode->getUserMode() != PixCon::UserMode::kExpert) {
	  if (m_factory->commandUser(command_i) == PixCon::UserMode::kExpert) {
	    aCommands.at(command_i)->setEnabled(false);
	  }
	}
      }
      context_menu_q4.addSeparator();
    }
    else if (selected_items.oneCommand()) {
      if (m_executor->isRunning()) {
	context_menu_q4.addAction("Execute Next", this, SLOT(sNextCommand() ) );
      }
      else {
	context_menu_q4.addAction("Execute", this, SLOT(sStartCommand() ) );
      }
      context_menu_q4.addSeparator();
      
      for(unsigned int classification_i=0; classification_i < m_factory->nClassifications(); classification_i++) {
	QVariant qCounter(kCommand+classification_i);
	QVariant qName(QString::fromStdString(m_factory->classificationName(classification_i)) );
	is_classification=true;
	QVariant qClassification(is_classification);
	qCounter_List.append(qCounter);
	qCounter_List.append(qName);
	qCounter_List.append(qClassification);
	//aCommands = context_menu_q4.addAction( QString::fromStdString(m_factory->commandName(claddification_i)), this, SLOT(sCommands() ) );
	//aCommands->setData(qCounter_List);
	aCommands.push_back( context_menu_q4.addAction( QString::fromStdString(m_factory->commandName(classification_i)), this, SLOT(sCommands() ) ) );
	aCommands.at(classification_i)->setData(qCounter_List);
	qCounter_List.clear();
	
      }
      is_classification=true;
      if (m_factory->nClassifications()>0) {
	context_menu_q4.addSeparator();
      }
    }
    else if (selected_items.oneConnection()) {
      context_menu_q4.addAction("Connect", this, SLOT(sConnection() ) );
    }
    
    if (selected_items.nothingSelected()) {
      context_menu_q4.addAction("Remove All", this, SLOT(sRemove() ) );
    }
    else {
      context_menu_q4.addAction("Remove", this, SLOT(sRemove() ) );
    }
    
    if (!m_executor->isRunning()) {
      context_menu_q4.addSeparator();
      context_menu_q4.addAction("Rebuild List", this, SLOT(sRebuildCommand() ) );
    }
    
    context_menu_q4.exec(QCursor::pos());

  }
  
  void CommandList::sNextCommand() {
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(sender==0) return;
    for(int i=0; i<m_commandListView->topLevelItemCount(); i++){
      QTreeWidgetItem *item = m_commandListView->topLevelItem(i);
      if ( item->isSelected() ) {
	CommandItem *command_item = dynamic_cast<CommandItem *>(item);
	if (command_item) {
	  m_executor->jump(command_item->commandId());
	}
      }
    }
    updateButton();
  }
  void CommandList::sStartCommand() {
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(sender==0) return;
    for(int i=0; i<m_commandListView->topLevelItemCount(); i++){
      QTreeWidgetItem *item = m_commandListView->topLevelItem(i);
      if ( item->isSelected() ) {
	CommandItem *command_item = dynamic_cast<CommandItem *>(item);
	if (command_item) {
	  m_executor->jump(command_item->commandId());
	  m_executor->continueProcessing();
	}
      }
    }
    updateButton();
  }
  void CommandList::sConnection() {
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(sender==0) return;
    CommandItem *command_item=NULL;
    BranchItem *branch_item=NULL;
    CommandItem *branch_parent_item=NULL;
    for(int i=0; i<m_commandListView->topLevelItemCount(); i++){
      QTreeWidgetItem *item = m_commandListView->topLevelItem(i);
      if ( item->isSelected() ) {
	std::cout << "INFO [CommandList::contextMenu] selected : " << std::string(item->text(0).toLatin1().data()) << std::endl;
	CommandItem *a_command_item = dynamic_cast<CommandItem *>(item);
	if (!a_command_item) {
	  BranchItem *a_branch_item = dynamic_cast<BranchItem *>(item);
	  if (a_branch_item) {
	    branch_item = a_branch_item;
	    branch_parent_item=dynamic_cast<CommandItem *>( item->parent() );
	  }
	}
	else {
	  command_item = a_command_item;
	}
	if (branch_item && branch_parent_item && command_item) break;
      }
    }
    if (branch_item && branch_parent_item && command_item) {
	try {
	  m_executor->changeConnection(command_item->commandId(), branch_parent_item->commandId(), branch_item->branchId());
	  branch_item->setConnection(command_item->commandId());
	  branch_item->setSelected(false);
	  command_item->setSelected(false);
	}
	catch (std::exception &err) {
	  std::string message;
	  message += "Failed to change the connection :\n";
	  message += err.what();

	  QMessageBox::warning(this,"ERROR::changeConnection",message.c_str(),"OK");
	}
	catch (...) {
	  QMessageBox::warning(this,"ERROR::changeConnection","Failed to change the connection.","OK");
	}
    }  
  }
  void CommandList::sRemove() {
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(sender==0) return;
    SelectedItems_t selected_items(nSelectedItems(m_commandListView));
    if (selected_items.nothingSelected()) {
	bool error=true;
	if (!m_executor->isRunning()) {
	  m_stopButtonMode=kStop;
	  stopCommand();
	  if (!m_executor->isRunning()) {
	    m_executor->clearCommandList();
	    m_commandListView->clear();
	    m_lastNextItem=NULL;
	    m_lastActiveItem=NULL;
	    CommandListCommandListener command_list_builder(*m_commandListView);
	    m_executor->buildCommandTree( command_list_builder );
	    error=false;
	  }
	}
	if (error) {
	  QMessageBox::warning(this,"ERROR::removeCommands","Cannot delete commands if commands are still being executed.","OK");
	}
      }
      else {
 	std::vector<QTreeWidgetItem *> stack;
	std::string delete_errors;
	std::string disconnection_errors;
	std::vector<CommandNode::Id_t> removed_id;
	for(int i=0; i<m_commandListView->topLevelItemCount(); i++){
	  QTreeWidgetItem *item = m_commandListView->topLevelItem(i);
	  //QTreeWidgetItem *next_item = 0;
	  //bool has_next_item=false;
	  if ( item->isSelected() ) {
	    CommandItem *command_item_1 = dynamic_cast<CommandItem *>(item);
	    if (command_item_1) {
	      //next_item = m_commandListView->topLevelItem(i+1);//item->nextSibling();
	      //has_next_item=true;
	      try {
		m_executor->erase(command_item_1->commandId());
		removed_id.push_back(command_item_1->commandId());
		if (m_lastActiveItem == command_item_1) {
		  m_lastActiveItem = NULL;
		}
		if (m_lastNextItem == command_item_1) {
		  m_lastNextItem = NULL;
		}
		delete command_item_1;
	      }
	      catch(...) {
		if (!delete_errors.empty()) {
		  std::stringstream id_string;
		  id_string << command_item_1->commandId();

		  delete_errors += ", ";
		  delete_errors += id_string.str();
		}
	      }
	    }
	    else {
	      BranchItem *branch_item = dynamic_cast<BranchItem *>(item);
	      CommandItem *command_item_2 = dynamic_cast<CommandItem *>(item->parent());
	      if (branch_item && command_item_2) {
		try {
		  m_executor->disconnect(command_item_2->commandId(), branch_item->branchId() );
		}
		catch (...) {
		  std::stringstream id_string;
		  id_string << command_item_2->commandId();

		  disconnection_errors += ", ";
		  disconnection_errors += id_string.str();
		}
		//next_item = m_commandListView->topLevelItem(i+1);//item->nextSibling();
		//has_next_item=true;
	      }
	    }
	  }
	  /* this is totally unneccessary: sub-items are erased with to-level items -> not translated to QT4
	  if (!has_next_item) {
	    next_item = item->firstChild();
	    if (!next_item) {
	      next_item = item->nextSibling();
	      if (!next_item && !stack.empty() ) {
		next_item = stack.back();
		stack.pop_back();
	      }
	    }
	    else if (item->nextSibling()) {
	      stack.push_back(item->nextSibling());
	    }
	  }
	  item = next_item;
	  */
	}
	for(int i=0; i<m_commandListView->topLevelItemCount(); i++){
	  QTreeWidgetItem *item = m_commandListView->topLevelItem(i);
	  BranchItem *branch_item = dynamic_cast<BranchItem *>( item ); //*iter );

	  if (branch_item) {
	    for (std::vector<CommandNode::Id_t>::const_iterator removed_id_iter = removed_id.begin();
		 removed_id_iter != removed_id.end();
		 removed_id_iter++) {
	      if (branch_item->commandId() == *removed_id_iter ) {
		branch_item->setConnection( CommandNode::invalidId() );
	      }
	    }
	  }
	}

	if (!delete_errors.empty() || !disconnection_errors.empty()) {
	  std::string message;
	  if (!delete_errors.empty()) {
	    message += "Failed to remove the commands :\n";
	    message += delete_errors;
	  }
	  if (!disconnection_errors.empty()) {
	    message += "Failed to disconnect branches of the following commands :\n";
	    message += disconnection_errors;
	  }
	  QMessageBox::warning(this,QString::fromStdString("ERROR::remveCommands"),QString::fromStdString(message),QString::fromStdString("OK"));
	}
      }
      updateButton();  
  }
  void CommandList::sRebuildCommand() {
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(sender==0) return;
    std::cout << "Here: Rebuild Command " << std::endl;
    if (!m_executor->isRunning()) {
	m_commandListView->clear();
	m_lastNextItem=NULL;
	m_lastActiveItem=NULL;
	CommandListCommandListener command_list_builder(*m_commandListView);
	m_executor->buildCommandTree( command_list_builder );
	//break;
    }
    else {
	QMessageBox::warning(this,"ERROR::remveCommands","Cannot rebuild the list while commands are being executed.","OK");
    }
  }
  void CommandList::sCommands(){
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(sender==0) return;
    QVariant var = sender->data();
    QList<QVariant> varList = var.toList();
    EReset reset_type = (EReset)varList[0].toInt();
    std::string Cname = varList[1].toString().toLatin1().data();
    bool is_Classification = varList[2].toBool();
  
    //Check different possible EMenuIds:
    if (reset_type == kSetNextCommand) sNextCommand();
    else if (reset_type == kStartCommand) sStartCommand();
    else if (reset_type == kConnection) sConnection();
    else if (reset_type == kRemove) sRemove();
    else if (reset_type == kRebuildCommands) sRebuildCommand();
    else {
     
      SelectedItems_t selected_items(nSelectedItems(m_commandListView));
      
      if (    (is_Classification && static_cast<unsigned int>(reset_type-kCommand) < m_factory->nClassifications())
	  || (!is_Classification && static_cast<unsigned int>(reset_type-kCommand) < m_factory->nCommands()) ) {

      if (selected_items.nothingSelected()) {
	ICommand *command = m_factory->createCommand( Cname );
	if (command) {
	try {
	  executeCommand(command, NULL, false);
	}
	catch (std::exception &err) {
	  std::string message;
	  message += "Failed to add the command :\n";
	  message += err.what();

	  delete command;
	  QMessageBox::warning(this,"ERROR::contextMenu",message.c_str(),"OK");
	}
	catch (...) {
	  delete command;
	  QMessageBox::warning(this,"ERROR::contextMenu","Failed to add the command.","OK");
	}
	}
	//@todo generate status message
      }
      else {
	for(int i=0; i<m_commandListView->topLevelItemCount(); i++){
	  QTreeWidgetItem *item = m_commandListView->topLevelItem(i);
	  if ( item->isSelected() ) {
	    CommandItem *command_item_1 = dynamic_cast<CommandItem *>(item);
	    if (command_item_1) {
	      assert( is_Classification);
	      IClassification *classification = m_factory->createClassification( Cname );
	      if (classification) {
	      // configure classification

	      CommandNode::Id_t node = command_item_1->commandId();

	      // remove old branches
	      for(int k=(command_item_1->childCount()-1); k>=0; k--){
		QTreeWidgetItem *current = command_item_1->child(k);
		if (m_lastActiveItem == current) {
		  m_lastActiveItem = NULL;
		}
		if (m_lastNextItem == current) {
		  m_lastNextItem = NULL;
		}

		delete current;
	      }

	      // create new ones
	      try {
		m_executor->changeClassification( node, *classification );
		for (unsigned int branch_i=0; branch_i<classification->nBranches(); branch_i++) {
		  new BranchItem(command_item_1, classification->branchName(branch_i), branch_i, CommandNode::invalidId() );
		}
		command_item_1->setExpanded(true);
		command_item_1->setSelected(false);
	      }
	      catch (std::exception &err) {
		std::string message;
		message += "Failed to change the classification :\n";
		message += err.what();

		delete classification;
		QMessageBox::warning(this,"ERROR::changeClassification",message.c_str(),"OK");
	      }
	      catch (...) {
		delete classification;
		QMessageBox::warning(this,"ERROR::changeClassification","Failed to change the classification.","OK");
	      }
	      }
	      //@todo generate status message

	    }
	    else {
	      BranchItem *branch_item = dynamic_cast<BranchItem *>(item);
	      CommandItem *command_item_2 = dynamic_cast<CommandItem *>(item->parent());
	      if (branch_item && command_item_2) {
		assert( !is_Classification);

		ICommand *command = m_factory->createCommand( Cname );
		if (command) {
		// configure command
		CommandNode::Id_t node = command_item_2->commandId();
		unsigned int branch_i = branch_item->branchId();
		try {
		  CommandNode::Id_t id = m_executor->insertCommand(command, NULL, node, branch_i, 0);
		  branch_item->setConnection(id);
		  if (m_commandListView->topLevelItemCount()>0) {
		    new CommandItem(m_commandListView, m_commandListView->topLevelItem(m_commandListView->topLevelItemCount()-1), command->name(),id);
		  }
		  else {
		    new CommandItem(m_commandListView, command->name(),id);
		  }
		}
		catch (std::exception &err) {
		  std::string message;
		  message += "Failed to create a the command ";
		  message += Cname;
		  message += " :\n";
		  message += err.what();

		  delete command;
		  QMessageBox::warning(this,"ERROR::addCommand",message.c_str(),"OK");
		}
		catch (...) {
		  delete command;
		  QMessageBox::warning(this,"ERROR::addCommand","Failed to add the command.","OK");
		}
		}
		//@todo generate status message
	      }
	    }
	  }
	}
      }
      }
      for(int i=0; i<m_commandListView->columnCount(); i++)
	m_commandListView->resizeColumnToContents(i);
      updateButton();
    
    }   
  }


  CommandList::SelectedItems_t  CommandList::nSelectedItems(QTreeWidget *view)
  {
    SelectedItems_t selection_counter;
    for(int i=0; i<view->topLevelItemCount(); i++){
      QTreeWidgetItem *item = view->topLevelItem(i);
      if (item->isSelected()) {
	CommandItem *command = dynamic_cast<CommandItem *>( item);
	if (command) {
	  selection_counter.command();
	}
	else if (!command) {
	  BranchItem *branch = dynamic_cast<BranchItem *>( item);
	  if (branch) {
	    selection_counter.branch();
	  }
	  else {
	    selection_counter.other();
	  }
	}
      }

    }
    return selection_counter;
  }

  bool CommandList::event(QEvent* event ) { 
    if ( event->type()==QEvent::User) {
      const ExecutionStatusEvent *status_event = dynamic_cast<ExecutionStatusEvent *>(event);  
      if (status_event) {  
	CommandItem *command_item = NULL;
        if (CommandNode::isValidId(status_event->id())) {  
	  command_item = dynamic_cast<CommandItem *>( findItem(status_event->id()) );
	  if (command_item == m_lastNextItem) {  
	    m_lastNextItem =NULL;
	  }
	} 
	//	std::cout << "INFO [CommandList::event] Got event : " << status_event->status() << std::endl;
	switch (status_event->status()) { 
	case ExecutionStatusEvent::kRunning: { 
	  if (command_item) {
	    CommandItem *last_active_item = dynamic_cast<CommandItem *>( m_lastActiveItem);
	    if (last_active_item && m_lastActiveItem != command_item) {
	      last_active_item->setStatus("");
	    }           
	    const CommandNode &command_node = m_executor->commandNode(status_event->id());
	    command_item->setPassed( command_node.passed() );
	    command_item->setStatus("Running");
	    m_lastRunningCommand=command_node.command().name();
	    m_messageEntry->setText(QString::fromStdString(command_node.command().name())+QString::fromStdString(": is running ..."));
	    if (m_commandProgress->isEnabled()) {
	      m_commandProgress->reset();
	      m_commandProgress->setDisabled(true);
	    }       
	    m_lastActiveItem = command_item;
	  }  
	  break;
	} 
	case ExecutionStatusEvent::kFinished: {
	  try {
	    const CommandNode &command_node = m_executor->commandNode(status_event->id());
	    m_messageEntry->setText(QString::fromStdString(command_node.command().name()+": has finished."));
	  }   
	  catch(...) {
	  } 
	  m_lastRunningCommand="";
	  CommandItem *last_active_item = dynamic_cast<CommandItem *>( m_lastActiveItem);
	  if (last_active_item && m_lastActiveItem != command_item) {
	    last_active_item->setStatus("");
	  }
	  break;
	} 
	case ExecutionStatusEvent::kCommandSucceeded:
	case ExecutionStatusEvent::kCommandFailed: {
	  try {
	    const CommandNode &command_node = m_executor->commandNode(status_event->id());
	    m_messageEntry->setText(QString::fromStdString(command_node.command().name()+": "
				    +(status_event->status()== ExecutionStatusEvent::kCommandSucceeded
				      ? "execution finished." : "execution failed.")));
	  } 
	  catch(...) {
	  }       
	  if (command_item) {
	    CommandItem *last_active_item = dynamic_cast<CommandItem *>( m_lastActiveItem);
	    if (last_active_item && m_lastActiveItem != command_item) {
	      last_active_item->setStatus("");
	    }
	    command_item->setStatus( ( (status_event->status()==ExecutionStatusEvent::kCommandSucceeded) ? "Done" : "Failure"));
	    m_lastActiveItem=NULL;
	  }
	  break;
	}    
	case ExecutionStatusEvent::kNextCommand:
	  if (command_item) {
	    CommandItem *last_next_item = dynamic_cast<CommandItem *>( m_lastNextItem);
	    if (last_next_item && m_lastNextItem != command_item && m_lastNextItem == m_lastActiveItem) {
	      last_next_item->setStatus("");
	    }
	    command_item->setStatus("Next");
	    m_lastNextItem=command_item;
	  }
	  break;       
	case ExecutionStatusEvent::kStopped:
	  // 	  CommandItem *last_active_item = dynamic_cast<CommandItem *>( m_lastActiveItem);
	  // 	  if (last_active_item && m_lastActiveItem != command_item) {
	  // 	    last_active_item->setStatus("Stopped");
	  // 	  }
	  break; 
	case ExecutionStatusEvent::kAborted: {
	  try {
	    const CommandNode &command_node = m_executor->commandNode(status_event->id());
	    m_messageEntry->setText(QString::fromStdString(command_node.command().name()+": was aborted."));
	  }
	  catch(...) {
	  }   
	  if (command_item) {
	    CommandItem *last_active_item = dynamic_cast<CommandItem *>( m_lastActiveItem);
	    if (last_active_item && m_lastActiveItem != command_item) {
	      last_active_item->setStatus("");
	    }
	    command_item->setStatus("Aborted");
	    m_lastActiveItem=NULL;
	  }
	  break; 
	}
	case ExecutionStatusEvent::kKilled: {
	  try {
	    const CommandNode &command_node = m_executor->commandNode(status_event->id());
	    m_messageEntry->setText(QString::fromStdString(command_node.command().name()+": stopped monitoring execution."));
	  }
	  catch(...) {
	  } 
	  if (command_item) {

	    CommandItem *last_active_item = dynamic_cast<CommandItem *>( m_lastActiveItem);
	    if (last_active_item && m_lastActiveItem != command_item) {
	      last_active_item->setStatus("");
	    }
	    command_item->setStatus("Ignored");
	    m_lastActiveItem=NULL;
	  } 
	  break;
	} 
	case ExecutionStatusEvent::kException: {
	  if (command_item) {
	    CommandItem *last_active_item = dynamic_cast<CommandItem *>( m_lastActiveItem);
	    if (last_active_item && m_lastActiveItem != command_item) {
	      last_active_item->setStatus("");
	    }
	    command_item->setStatus("Exception");
	    m_lastActiveItem=NULL;
	  }
	  break;
	}
	case ExecutionStatusEvent::kWaiting:
	  break;
	}
	updateButton();
	return true;
      }
      else {
	 PixCon::StatusMessageEvent *status_message_event  = dynamic_cast< StatusMessageEvent *>(event);
	 if (status_message_event) {
	   QString message;
	   if (!m_lastRunningCommand.empty()) {
	     message+=QString::fromStdString(m_lastRunningCommand);
	     message+=" : ";
	   }
	   message+=status_message_event->statusMessage();
	   m_messageEntry->setText(message);
	   return true;
	 }
      }

    } 
    return QDialog::event(event); //here was CommandListBase::event(event);
 }


  QTreeWidgetItem *CommandList::findItem(CommandNode::Id_t id) {
    for(int i=0; i<m_commandListView->topLevelItemCount(); i++){
      CommandItem *item = dynamic_cast<CommandItem *>( m_commandListView->topLevelItem(i) ); //*iter );
      if (item && item->commandId()==id) {
	return item;
      }
    }
    return NULL;
  }

  void CommandList::stopCommand() {
    switch (m_stopButtonMode) {
    case kStart: {
      m_executor->continueProcessing();
      break;
    }
    case kContinue: {
      m_executor->continueProcessing();
      break;
    }
    case kStop: {
      m_executor->stopProcessing();
      setButton();
      break;
    }
    case kAbort:
      m_executor->abortCommand();
      setButton();
      break;
    case kKill: {
      int ret = QMessageBox::question(this,"Ignore command ?","Ignoring currently running command ?\n\n"
				      "You really do not want to wait for the termination of the current command ?\n"
				      "This may leave the system in an undefined state.",
				      QMessageBox::No, QMessageBox::Yes);
      if (ret==QMessageBox::Yes) {
	m_executor->killCommand();
	setButton();
      }
      break;
    }
    default:
      break;
    }
  }

  void CommandList::updateButton() {
    if (!m_executor->isRunning()) {
      setButton();
    }
    else if (m_executor->isRunning()) {
      if (m_stopButtonMode==kDisabled || m_stopButtonMode==kStart) {
	setButton(kStop);
      }
    }
  }

  void CommandList::setButton() {
    if (m_executor->isRunning() ){
      if (m_stopButtonMode == kAbort) {
	//	setButton(kAborting);
	setButton(kKill);
      }
      else if (m_stopButtonMode == kStart || m_stopButtonMode == kContinue) {
	setButton(kStop);
      }
      else if (m_stopButtonMode == kStop ) {
	setButton(kAbort);
      }
    }
    else if (m_executor->hasNextCommand()) {
      setButton(kStart);
    }
    else {
      setButton(kDisabled);
    }
  }

  void CommandList::setButton(EButtonMode mode) {
    if (m_stopButton->isEnabled()) {
      if (mode == kDisabled || mode==kAborting) {
	m_stopButton->setEnabled(false);
      }
    }
    else {
      if (mode != kDisabled && mode != kAborting) {
	m_stopButton->setEnabled(true);
      }
    }
    if (mode<kNButtonModes) {
      m_stopButtonMode=mode;
      if (m_stopButtonMode == kAbort) {
	QPalette palette(m_stopButton->palette());
	palette.setColor(QPalette::ButtonText,QColor("black"));
	palette.setColor(QPalette::Button,QColor("red"));
	m_stopButton->setPalette(palette);
	//m_stopButton->setPaletteBackgroundColor(QColor("red"));
	//m_stopButton->setPaletteForegroundColor( QColor("black") );
	//QToolTip::add( m_stopButton, "Try to abort the currently running command." );
	m_stopButton->setToolTip("Try to abort the currently running command." );
      }
      else if (m_stopButtonMode == kKill) {
	//m_stopButton->setPaletteBackgroundColor( QColor( "black" ) );
	QPalette palette(m_stopButton->palette());
 	QColor white_colour(200,200,200);
 	palette.setColor(QPalette::Text,white_colour);
 	palette.setColor(QPalette::ButtonText,white_colour);
	palette.setColor(QPalette::Button,QColor("black"));
 	m_stopButton->setPalette(palette);
	QFont m_stopButton_font(  m_stopButton->font() );
	m_stopButton->setFont( m_stopButton_font ); 
	//QToolTip::add( m_stopButton, "Do not wait for termination of currently running command." );
	m_stopButton->setToolTip("Do not wait for termination of currently running command.");
      }
      else if (m_stopButtonMode == kStart) {
	QPalette palette(m_stopButton->palette());
	palette.setColor(QPalette::ButtonText,QColor("black"));
	palette.setColor(QPalette::Button,QColor("green"));
	m_stopButton->setPalette(palette);
	//m_stopButton->setPaletteBackgroundColor(QColor("green"));
	//m_stopButton->setPaletteForegroundColor( QColor("black") );
	//QToolTip::add( m_stopButton, "Start execution of the command marked 'Next'" );
	m_stopButton->setToolTip("Start execution of the command marked 'Next'");
      }
      else if (m_stopButtonMode == kStop) {
	QPalette palette(m_stopButton->palette());
	palette.setColor(QPalette::ButtonText,QColor("black"));
	palette.setColor(QPalette::Button,QColor("yellow"));
	m_stopButton->setPalette(palette);
	//m_stopButton->setPaletteBackgroundColor(QColor("yellow"));
	//m_stopButton->setPaletteForegroundColor( QColor("black") );
	//QToolTip::add( m_stopButton, "Stop command execution after currently running command." );
	m_stopButton->setToolTip("Stop command execution after currently running command.");
      }
      else {
	QPalette palette(m_stopButton->palette());
	palette.setColor(QPalette::ButtonText,QColor("black"));
	palette.setColor(QPalette::Button,QColor("lightGray"));
	m_stopButton->setPalette(palette);
	//m_stopButton->setPaletteBackgroundColor( m_stopButton->palette().active().base());
	//QToolTip::add( m_stopButton, " Alter command execution. " );
	m_stopButton->setToolTip(" Alter command execution. ");
      }
      m_stopButton->setText( s_buttonText[m_stopButtonMode] );
    }
    else {
      //m_stopButton->setPaletteBackgroundColor( m_stopButton->palette().active().base());
      QPalette palette(m_stopButton->palette());
      palette.setColor(QPalette::Button,QColor("lightGray"));
      m_stopButton->setPalette(palette);
    }
  }

  void CommandList::progress(unsigned int step, unsigned int total_steps) {
    if (total_steps>0) {
      if (!m_commandProgress->isEnabled()) {
	m_commandProgress->setDisabled(false);
      }
      m_commandProgress->setMaximum(total_steps);
      m_commandProgress->setValue(step);
    }
    else {
      if (m_commandProgress->isEnabled()) {
	m_commandProgress->setDisabled(true);
      }
    }
  }

  void CommandList::setStatusMessage(const QString &message) {
    m_messageEntry->setText(message);
  }
  const char *CommandList::s_buttonText[kNButtonModes]={"Start", "Start", "Stop", "Continue","Abort", "Aborting", "Ignore"};

}
