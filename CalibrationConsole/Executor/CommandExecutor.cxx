#include "CommandExecutor.h"

namespace PixCon {


  class NopExecutionListener : public IExecutionListener {
  public:
    void statusMessage(const std::string &/*message*/) {};
    void startCommand(const std::string &, CommandNode::Id_t) {}
    void finishedCommand(const std::string &, CommandNode::Id_t, bool) {}
    void abortedCommaned(const std::string &, CommandNode::Id_t) {}
    void killedCommaned(const std::string &, CommandNode::Id_t) {}
    void nextCommand(const std::string &, CommandNode::Id_t) {}
    void caughtExceptionIn(const std::string &, CommandNode::Id_t, const std::string &) {}
    void stopped() {}
    void finished() {}
    void waiting() {}
  };

  class VerboseExecutionListener : public IExecutionListener {
  public:
    void statusMessage(const std::string &message)
    { std::cout << "INFO [VerboseExecutionListener::statusMessage] " << message << "." << std::endl; }
    void startCommand(const std::string &name, CommandNode::Id_t id)
    { std::cout << "INFO [VerboseExecutionListener::startCommand] " << id << " : " << name << "." << std::endl; }
    void finishedCommand(const std::string &name, CommandNode::Id_t id, bool success)
    { std::cout << "INFO [VerboseExecutionListener::finishedCommand] " << id << " : " << name << " " << (success ? "success" : "failure") << "." << std::endl; }

    void abortedCommand(const std::string &name, CommandNode::Id_t id)
    { std::cout << "INFO [VerboseExecutionListener::abortedCommand] " << id << " : " << name << "." << std::endl; }
    void killedCommand(const std::string &name, CommandNode::Id_t id)
    { std::cout << "INFO [VerboseExecutionListener::killedCommand] " << id << " : " << name << "." << std::endl; }
    void nextCommand(const std::string &name, CommandNode::Id_t id)
    { std::cout << "INFO [VerboseExecutionListener::nextCommand] " << id << " : " << name << "." << std::endl; }
    void caughtExceptionIn(const std::string &name, CommandNode::Id_t id, const std::string &message)
    { std::cout << "INFO [VerboseExecutionListener::caughtExceptionIn] " << id << " : " << name << " : " << message << "." << std::endl; }
    void stopped()
    { std::cout << "INFO [VerboseExecutionListener::stopped] stopped execution." << std::endl; }
    void finished()
    { std::cout << "INFO [VerboseExecutionListener::finished] finished commands." << std::endl; }
    void waiting()
    { std::cout << "INFO [VerboseExecutionListener::waiting] waiting." << std::endl; }
  };


  CommandExecutor::CommandExecutor()
    : m_currentCommandId( CommandNode::invalidId() ),
      m_lastExecutedCommandId( CommandNode::invalidId() ),
      m_nextCommandId( CommandNode::invalidId() ),
      m_lastNewCommandId( CommandNode::invalidId() ),
      m_commandIdMax( CommandNode::invalidId() ),
      m_jumpCommandId( CommandNode::invalidId() ),
      m_listener( new VerboseExecutionListener ),
      m_abort(false),
      m_stop(false),
      m_step(false),
      m_isRunning(false),
      m_testRunning(false)
    { start(); }

  CommandExecutor::CommandExecutor(const std::shared_ptr<IExecutionListener> &listener)
    : m_currentCommandId( CommandNode::invalidId() ),
      m_lastExecutedCommandId( CommandNode::invalidId() ),
      m_nextCommandId( CommandNode::invalidId() ),
      m_lastNewCommandId( CommandNode::invalidId() ),
      m_commandIdMax( CommandNode::invalidId() ),
      m_jumpCommandId( CommandNode::invalidId() ),
      m_listener( listener ),
      m_abort(false),
      m_stop(false),
      m_step(false),
      m_isRunning(false),
      m_testRunning(false)
    { start(); }

  void CommandExecutor::setExecutionListener(const std::shared_ptr<IExecutionListener> &listener)
  { 
    m_listener=listener;
  }


  CommandNode::Id_t CommandExecutor::appendCommand(ICommand *command, IClassification *classification, CommandNode::Id_t previous_command, unsigned int branch_i)
  {
    if (m_lastNewCommandId > m_commandIdMax) {
      m_lastNewCommandId = m_commandIdMax;
      if (m_lastExecutedCommandId > m_lastNewCommandId) m_lastExecutedCommandId = m_lastNewCommandId;
    }
    if (previous_command==CommandNode::invalidId()) {
      previous_command = m_lastNewCommandId;
    }
    std::map< CommandNode::Id_t, CommandNode >::iterator new_command_iter = insertCommand( command, classification );
    Lock lock(m_commandListMutex);
    connect( new_command_iter, previous_command, branch_i);
    m_lastNewCommandId = id( new_command_iter );
    if (!CommandNode::isValidId(m_nextCommandId)) {
      if (previous_command == m_lastExecutedCommandId || !CommandNode::isValidId(previous_command)) {
	m_nextCommandId=new_command_iter->first;
      }
    }
    if (CommandNode::isValidId(m_nextCommandId)) {
      m_hasCommandFlag.setFlag();
    }
    return m_lastNewCommandId;
  }

  CommandNode::Id_t CommandExecutor::insertCommand(ICommand *command, IClassification *classification,
						   CommandNode::Id_t previous_command,
						   unsigned int previous_command_branch_i,
						   unsigned int new_command_branch_i)
  {
    std::map< CommandNode::Id_t, CommandNode >::iterator new_command_iter = insertCommand( command, classification );
    Lock lock(m_commandListMutex);

    std::map< CommandNode::Id_t, CommandNode >::iterator previous_iter = find( previous_command);
    std::map< CommandNode::Id_t, CommandNode >::iterator child_iter = find( previous_iter->second.branchId( previous_command_branch_i) );

    connect(child_iter, new_command_iter, new_command_branch_i);
    connect(new_command_iter, previous_command, previous_command_branch_i);
    if (CommandNode::isValidId(m_nextCommandId)) {
      m_hasCommandFlag.setFlag();
    }
    return id(new_command_iter);
  }

  void CommandExecutor::erase(CommandNode::Id_t command) 
  {
    Lock lock(m_commandListMutex);
    if (command == m_currentCommandId) {
      throw std::runtime_error("ERROR [CommandExecutor::changeCommand] Cannot alter command. Command is busy.");
    }
    std::map<CommandNode::Id_t, CommandNode >::iterator command_node = find(command);
    if ( !isValid(command_node) ) {
      throw std::runtime_error("ERROR [CommandExecutor::commandNode] Invalid command id.");
    }
    m_commands.erase(command_node);

    // disconnect all branches from this command
    for (std::map<CommandNode::Id_t, CommandNode >::iterator command_iter = m_commands.begin();
	 command_iter != m_commands.end();
	 command_iter++) {
      for (unsigned int branch_i=0; branch_i<command_iter->second.nBranches();branch_i++) {
	if (command_iter->second.branchId(branch_i) == command) {
	  command_iter->second.disconnect(branch_i);
	}
      }
    }

    // reset the command id
    std::map<CommandNode::Id_t, CommandNode >::reverse_iterator last_node= m_commands.rbegin();
    if (last_node != m_commands.rend() && last_node->first < m_commandIdMax ) {
      m_commandIdMax=last_node->first;
    }
    
  }


  void CommandExecutor::showCommandTree()
  {
    std::cout << "next = " << m_nextCommandId << " current = " << m_currentCommandId << std::endl;
    std::vector<std::pair<CommandNode::Id_t, CommandNode::Id_t> > stack;
    stack.push_back( std::make_pair(CommandNode::invalidId(), m_nextCommandId));
    while ( !stack.empty() ) {
      std::pair<CommandNode::Id_t, CommandNode::Id_t> current = stack.back();
      stack.pop_back();
      if (current.first == m_nextCommandId) {
	std::cout << "> ";
      }
      else if (current.first == m_currentCommandId) {
	std::cout << "* ";
      }
      else {
	std::cout << "  ";
      }
      std::cout << current.first << " : ";

      while ( CommandNode::isValidId( current.second ) ) {
	const CommandNode &command_node = commandNode(current.second);
	std::cout << current.second << "(" << command_node.nReferences() << ") -> ";
	for (unsigned int branch_i =1; branch_i == command_node.nBranches(); branch_i++) {
	  stack.push_back( std::make_pair( current.second, command_node.branchId(branch_i)) );
	}
	current.first = current.second;
	current.second = command_node.branchId(0);
      }
      std::cout << std::endl;
    }
  }

  CommandExecutor::~CommandExecutor()
  { 
    std::cout << "INFO [CommandExecutor::dtor] shutdown and wait." << std::endl;
    shutdownWait();
  }


  void CommandExecutor::run()
  {
    //    std::cout << "INFO [CommandExecutor::run] started." << std::endl;
    for(;!m_abort;) {

      bool has_commands=false;

      {
	Lock lock(m_commandListMutex);
	std::map<CommandNode::Id_t, CommandNode>::iterator current_command = find(m_nextCommandId);
	if ( isValid(current_command))  {
	  has_commands =true;
	}
      }

      if (m_stop || !has_commands) {
	m_isRunning=false;
	m_testRunning=false;
	// 	if (!has_commands) {
	// 	  std::cout << "INFO [CommandExecutor::run] wait for new commands." << std::endl;
	// 	}
	// 	else {
	// 	  std::cout << "INFO [CommandExecutor::run] wait for new continuation signal." << std::endl;
	// 	}
	m_listener->waiting();
	do {
	  m_hasCommandFlag.wait(m_abort);
	} while (!m_abort  && !m_step && m_stop);
	m_testRunning=!m_abort && !m_stop;
	m_step=false;
      }

      if (m_abort) break;
      //      std::cout << "INFO [CommandExecutor::run] status change was signaled." << std::endl;

      // process commands
      Lock lock_commandRemove(m_commandRemoveMutex);
      for (;;) {
	std::map<CommandNode::Id_t, CommandNode >::iterator current_command;
	//	m_currentCommand = m_nextCommandId;
	{
	  Lock lock_commandList(m_commandListMutex);
	  if (m_abort || m_stop) break;

	  if (CommandNode::isValidId(m_jumpCommandId)) {
	    m_nextCommandId = m_jumpCommandId;
	    m_jumpCommandId = CommandNode::invalidId();
	  }

	  m_isRunning=true;
	  m_currentCommandId = m_nextCommandId;
	  current_command = find(m_nextCommandId);
	  if ( current_command == m_commands.end())  {
	    m_isRunning=false;
	    std::cout << "INFO [CommandExecutor::run] reached invalid command." << std::endl;
	    m_listener->finished();
	    break;
	  }
	}
	//	std::cout << "INFO [CommandExecutor::run] execute " << current_command->first << "." << std::endl;
	std::shared_ptr<ICommand> the_command;
	{
	  Lock lock(m_commandListMutex);
	  the_command = current_command->second.commandPtr();
	  current_command->second.use();
	}
	the_command->resetAbort();
	if (m_abort) break;

	m_listener->startCommand(the_command->name(), current_command->first);
	try {
	  Result_t result = the_command->execute(*m_listener);

	  if (result.killed()) {
	    m_listener->killedCommand(the_command->name(), m_currentCommandId);
	  }
	  else if (result.aborted()) {
	    m_listener->abortedCommand(the_command->name(), m_currentCommandId);
	  }
	  else {
	    m_listener->finishedCommand(the_command->name(), m_currentCommandId, (result.status()==Result_t::kSuccess) );
	  }

	  if (m_abort) break;

	  {
	    Lock lock(m_commandListMutex);
	    std::cout << "INFO [CommandExecutor::run] classify " << current_command->first << "." << std::endl;
	    m_lastExecutedCommandId = m_currentCommandId;
	    m_currentCommandId = CommandNode::invalidId();

	    // in case a specific id is requested for execution, then the classification is not executed.
	    if (CommandNode::isValidId(m_jumpCommandId)) {
	      m_nextCommandId = m_jumpCommandId;
	      m_jumpCommandId = CommandNode::invalidId();
	    }
	    else {
	      m_nextCommandId = current_command->second.next(result);
	    }
	    if (CommandNode::isValidId(m_nextCommandId)) {
	      std::map<CommandNode::Id_t, CommandNode>::iterator the_next_command = find(m_nextCommandId);
	      if (the_next_command != m_commands.end()) {
		m_listener->nextCommand(the_next_command->second.command().name(), the_next_command->first);
	      }
	    }
	  }
	}
	catch ( ... ) {
	  Lock lock(m_commandListMutex);
	  std::cout << "INFO [CommandExecutor::run] caught exception during execution of  " << current_command->first << "." << std::endl;
	  m_listener->caughtExceptionIn(current_command->second.command().name(), current_command->first,"");
	  m_lastExecutedCommandId = m_currentCommandId;
	  m_currentCommandId = CommandNode::invalidId();
	}

	//	std::cout << "INFO [CommandExecutor::run] will continue with " << m_nextCommandId << "." << std::endl;
	if (m_stop) {
	  m_listener->stopped();
	  m_isRunning=false;
	  break;
	}
      }
      m_isRunning=false;

    }
    m_testRunning=false;
    m_exit.setFlag();
    std::cout << "INFO [CommandExecutor::run] done." << std::endl;
  }

  void CommandExecutor::buildCommandTree(ICommandListener &listener) {
    Lock remove_lock(m_commandRemoveMutex);
    Lock lock(m_commandListMutex);
    for(std::map< CommandNode::Id_t, CommandNode >::const_iterator iter = m_commands.begin();
	iter != m_commands.end();
	iter++) {
      listener.command(iter->second.command().name(),iter->first );
      if (iter->second.hasClassification()) {
	for (unsigned int branch_i=0; branch_i<iter->second.nBranches(); branch_i++) {
	  listener.branch(iter->second.classification().name(),
			  iter->second.classification().branchName(branch_i),
			  branch_i,
			  iter->second.branchId(branch_i) );
	}
      }
      else {
	for (unsigned int branch_i=0; branch_i<iter->second.nBranches(); branch_i++) {
	  listener.branch("",
			  "next",
			  branch_i,
			  iter->second.branchId(branch_i) );
	}
      }
    }
  }

  void CommandExecutor::jump(CommandNode::Id_t id) {
    m_jumpCommandId=id;
    if (CommandNode::isValidId(m_jumpCommandId)) {
      Lock lock(m_commandListMutex);
      if (!m_stop) {
	m_isRunning=true;
	m_hasCommandFlag.setFlag();
      }
      std::map<CommandNode::Id_t, CommandNode>::iterator the_next_command = find(m_jumpCommandId);
      if (the_next_command != m_commands.end()) {
	m_listener->nextCommand(the_next_command->second.command().name(), the_next_command->first);
      }
    }
  }

  void CommandExecutor::sendAbort(bool kill) {
    m_hasCommandFlag.setFlag();
    std::shared_ptr<ICommand> the_command;
    CommandNode::Id_t the_command_id = CommandNode::invalidId();
    {
      Lock lock(m_commandListMutex);
      std::map< CommandNode::Id_t, CommandNode >::iterator  current_command = find(m_currentCommandId);
      if ( isValid(current_command )) {
	the_command = current_command->second.commandPtr();
	the_command_id = m_currentCommandId;
      }
    }

    if (the_command.get()) {
      try {
	if (!kill) {
	  the_command->abort();
	}
	else {
	  the_command->kill();
	}
      }
      catch (...) {
	m_listener->caughtExceptionIn(the_command->name(), the_command_id,"Exception while trying to abort or kill.");
      }
    }
  }


}
