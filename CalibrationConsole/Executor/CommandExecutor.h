// Dear emacs, this is -*-c++-*-
#ifndef _PixCon_CommandExecutor_h_
#define _PixCon_CommandExecutor_h_

#include <map>
#include <vector>
#include <Lock.h>
#include <Flag.h>
#include <CalibrationDataTypes.h>
#include <memory>
#include <cassert>
#include <stdexcept>
#include <iostream>

#include "ICommand.h"

#include <qthread.h>

namespace PixCon {


  class CommandNode {
  public:
    typedef unsigned int Id_t;

    CommandNode(ICommand *command, IClassification *classification)
      : m_command(std::shared_ptr<ICommand>(command)),
	m_classification(std::shared_ptr<IClassification>(classification)),
	m_referenced(0),
	m_passed(0)
    { }

    ~CommandNode() { /* debug aid: */ /*assert(m_referenced==0);*/ }

    // debug aid
    void resetReferences() { m_referenced=0; }

    void dereference() { assert(m_referenced>0); m_referenced--; }
    void reference()   { m_referenced++; }
    unsigned int nReferences() const  { return m_referenced; }

    void changeCommand(ICommand &a) {
      m_command = std::shared_ptr<ICommand>(&a);
    }

    void changeClassification(IClassification &a) {
      m_classification = std::shared_ptr<IClassification>(&a);
    }

    std::shared_ptr<ICommand> commandPtr() { return m_command; }

    ICommand &command()               { return *m_command; }
    const ICommand &command() const   { return *m_command; }

    bool hasClassification() const    {return m_classification != NULL; }
    IClassification &classification()             { return *m_classification; }
    const IClassification &classification() const { return *m_classification; }

    /** Connect a possible branch.
     * The classification may choose one of its possible branches.
     * Unconnected branches
     */
    void connect(unsigned int branch_i, Id_t id) {
      assert( branch_i < nBranches());

      if (m_branches.size()<=branch_i) {
	m_branches.resize(branch_i+1, invalidId() );
      }
      m_branches[branch_i]=id;
      //       std::cout << "INFO [CommandNod::connect] connections : ";
      //       for (unsigned int i=0; i<m_branches.size(); i++) {
      // 	std::cout << m_branches[i] << " ";
      //       }
      //       std::cout << std::endl;
    }

    void disconnect(unsigned int branch_i) {
      if (branch_i < m_branches.size()) {
	m_branches[branch_i]=invalidId();
      }
    }


    unsigned int nBranches() const {
      return (m_classification ? m_classification->nBranches() : 1 );
    }

    //     void resetAbort() {
    //       m_command->resetAbort();
    //     }

    void use() {
      m_passed++;
    }

    //     Result_t execute() {
    //       m_passed++;
    //       return m_command->execute();
    //     }

    //     void abort() {
    //       m_command->abort();
    //     }

    //     void kill() {
    //       m_command->kill();
    //     }
    // std::shared_ptr<ICommand> command() { return m_command; }

    Id_t next(const Result_t &result) {
      unsigned int branch_i = (m_classification ? m_classification->classify(result)  : 0 );
      if (branch_i >= m_branches.size()) return s_invalidId;
      return m_branches[branch_i];
    }

    Id_t branchId(unsigned int branch_i) const {
      if (branch_i >= m_branches.size()) return s_invalidId;
      return m_branches[branch_i];
    }

    void incPassed() { m_passed++; }
    unsigned int  passed() const { return m_passed; }

    static Id_t invalidId()           { return s_invalidId; }
    static bool isValidId(Id_t an_id) { return an_id != s_invalidId; }

  private:
    std::shared_ptr<ICommand>        m_command;
    std::shared_ptr<IClassification> m_classification;
    std::vector<Id_t>                  m_branches;
    unsigned int                       m_referenced;
    unsigned int                       m_passed;

    static const Id_t s_invalidId = 0;
  };

  class ICommandListener {
  public:
    virtual ~ICommandListener() {}
    virtual void command(const std::string &command_name, CommandNode::Id_t id) = 0;
    virtual void branch(const std::string &classification_name, const std::string &branch_name, unsigned int branch_i, CommandNode::Id_t id) = 0;
  };

  class IExecutionListener : public IStatusMessageListener {
  public:
    virtual ~IExecutionListener() {}
    virtual void startCommand(const std::string &name, CommandNode::Id_t id) = 0;
    virtual void finishedCommand(const std::string &name, CommandNode::Id_t id, bool success) = 0;
    virtual void abortedCommand(const std::string &name, CommandNode::Id_t id) = 0;
    virtual void killedCommand(const std::string &name, CommandNode::Id_t id) = 0;
    virtual void nextCommand(const std::string &name, CommandNode::Id_t id) = 0;
    virtual void caughtExceptionIn(const std::string &name, CommandNode::Id_t id, const std::string &message) = 0;
    virtual void stopped() = 0;
    virtual void finished() = 0;
    virtual void waiting() = 0;
  };

  class CommandExecutor : public QThread
  {
  public:

    CommandExecutor();
    CommandExecutor(const std::shared_ptr<IExecutionListener> &listener);

    void setExecutionListener(const std::shared_ptr<IExecutionListener> &listener);

    ~CommandExecutor();

//     CommandNode::Id_t appendCommand(ICommand *command, IClassification *classification, unsigned int branch_i=0) {
//       return appendCommand(command,classification, m_lastNewCommandId, branch_i);
//     }

    CommandNode::Id_t appendCommand(ICommand *command, IClassification *classification, CommandNode::Id_t previous_command, unsigned int branch_i=0);

    CommandNode::Id_t insertCommand(ICommand *command, IClassification *classification,
				    CommandNode::Id_t previous_command,
				    unsigned int previous_command_branch_i=0,
				    unsigned int new_command_branch_i=0);

    void clearCommandList()  {
      Lock remove_lock(m_commandRemoveMutex);
      Lock lock(m_commandListMutex);
      m_commands.clear();
      m_commandIdMax=0;
      m_lastNewCommandId=CommandNode::invalidId();
    }

    const CommandNode &commandNode(CommandNode::Id_t command) {
      Lock lock(m_commandListMutex);
      std::map<CommandNode::Id_t, CommandNode >::iterator command_node = find(command);
      if ( !isValid(command_node) ) {
	throw std::runtime_error("ERROR [CommandExecutor::commandNode] Invalid command id.");
      }
      return command_node->second;
    }

    void changeCommand(CommandNode::Id_t command, ICommand &a) {
      Lock lock(m_commandListMutex);
      if (command == m_currentCommandId) {
	throw std::runtime_error("ERROR [CommandExecutor::changeCommand] Cannot alter command. Command is busy.");
      }
      std::map<CommandNode::Id_t, CommandNode >::iterator command_node = find(command);
      if ( !isValid(command_node) ) {
	throw std::runtime_error("ERROR [CommandExecutor::commandNode] Invalid command id.");
      }
      command_node->second.changeCommand(a);
    }

    void changeClassification(CommandNode::Id_t command, IClassification &a) {
      Lock lock(m_commandListMutex);
      //       if (command == m_currentCommandId) {
      // 	throw std::runtime_error("ERROR [CommandExecutor::changeCommand] Cannot alter command. Command is busy.");
      //       }
      std::map<CommandNode::Id_t, CommandNode >::iterator command_node = find(command);
      if ( !isValid(command_node) ) {
	throw std::runtime_error("ERROR [CommandExecutor::commandNode] Invalid command id.");
      }
      command_node->second.changeClassification(a);
    }

    void change(CommandNode::Id_t command, ICommand &a_command, IClassification &a_classification) {
      Lock lock(m_commandListMutex);
      if (command == m_currentCommandId) {
	throw std::runtime_error("ERROR [CommandExecutor::changeCommand] Cannot alter command. Command is busy.");
      }
      std::map<CommandNode::Id_t, CommandNode >::iterator command_node = find(command);
      if ( !isValid(command_node) ) {
	throw std::runtime_error("ERROR [CommandExecutor::commandNode] Invalid command id.");
      }
      command_node->second.changeClassification(a_classification);
      command_node->second.changeCommand(a_command);
    }

    void changeConnection( CommandNode::Id_t child, CommandNode::Id_t parent, unsigned int branch_i) {
      Lock lock(m_commandListMutex);
      // It does not matter whether the child is running or not
      //      if ( /*child == m_currentCommandId || */ parent == m_currentCommandId ) {
      //	throw std::runtime_error("ERROR [CommandExecutor::changeCommand] Cannot alter connection. One of the commands is busy.");
      //      }
      // Why not ? 
      connect( child, parent, branch_i);
    }


    void disconnect(CommandNode::Id_t command, unsigned int branch_i) {
      Lock lock(m_commandListMutex);
      if (command == m_currentCommandId) {
	throw std::runtime_error("ERROR [CommandExecutor::changeCommand] Cannot alter command. Command is busy.");
      }
      std::map<CommandNode::Id_t, CommandNode >::iterator command_node = find(command);
      if ( !isValid(command_node) ) {
	throw std::runtime_error("ERROR [CommandExecutor::commandNode] Invalid command id.");
      }
      command_node->second.disconnect(branch_i);
    }

    void erase(CommandNode::Id_t command);


    // debug
    void showCommandTree();

    bool isRunning() {
      return m_isRunning | m_testRunning;
      //      Lock lock(m_commandListMutex);
      //      return CommandNode::isValidId(m_currentCommandId);
    }

    bool hasCommands() {
      Lock lock(m_commandListMutex);
      return m_commands.size()>0;
    }

    bool hasNextCommand() {
      Lock lock(m_commandListMutex);
      return CommandNode::isValidId(m_nextCommandId) || CommandNode::isValidId(m_jumpCommandId) ;
    }

    void stopProcessing()     { m_stop=true; }
    void continueProcessing() { m_stop=false; m_hasCommandFlag.setFlag(); }

    void abortCommand() {
      m_stop=true;
      sendAbort(false);
    }

    void killCommand() {
      m_stop=true;
      sendAbort(true);
    }

    void shutdown() {
      m_abort=true;
      sendAbort(false);
    }

    void shutdownWait() {
      if (!m_abort) {
	shutdown();
      }
      if (QThread::isRunning()) {
	// wait 30s for the abort to finish
	if (!m_exit.timedwait(30) && isRunning()) {
	  sendAbort(true);

	  if (QThread::isRunning()) {
	    m_exit.wait();
	  }
	}

	struct timespec wait_time;
	wait_time.tv_sec = 0;
	wait_time.tv_nsec = 100*1000000;
	while (QThread::isRunning()) {
	  nanosleep(&wait_time, NULL);
	}
      }
    }

    void buildCommandTree(ICommandListener &listener);

    void jump(CommandNode::Id_t id);

    void step() {
      m_step=true;
      m_hasCommandFlag.setFlag();
    }

  protected:
    void run();

    std::map< CommandNode::Id_t, CommandNode >::iterator insertCommand(ICommand *command, IClassification *classification) {
      Lock lock(m_commandListMutex);
      // ensure that the given commend, the classification exist, and that the number of branches
      // of the classification is reasonable.
      assert( command!=NULL && (classification==NULL || (classification->nBranches()<10 && classification->nBranches()>0))) ;

      std::pair<std::map< CommandNode::Id_t, CommandNode >::iterator, bool> 
	ret = m_commands.insert(std::make_pair( newId(), CommandNode(command, classification) ));

      if (!ret.second) {
	std::cout << "ERROR {CommandExecutor::addCommand} Failed to add command. Logic error." << std::endl;
      }
      return ret.first;
    }

    void sendAbort(bool kill);

  private:

    CommandNode::Id_t newId() {
      return ++m_commandIdMax;
    }

    void connect( CommandNode::Id_t child, CommandNode::Id_t parent, unsigned int branch_i) {
      connect( find(child), parent, branch_i);
    }

    void connect( std::map< CommandNode::Id_t, CommandNode >::iterator child, CommandNode::Id_t parent, unsigned int branch_i) {
      if (!isValid(child)) return;

      connect(child, find(parent), branch_i);
    }

    void connect( std::map< CommandNode::Id_t, CommandNode >::iterator child, std::map< CommandNode::Id_t, CommandNode >::iterator parent, unsigned int branch_i) {
      if (!isValid(child)) return;
      if (!isValid(parent)) {
	// 	if (CommandNode::isValidId(m_nextCommandId)) {
	// 	  disconnect(m_nextCommandId);
	// 	}
	m_nextCommandId = child->first;
      }
      else {
	disconnect( parent->second.branchId(branch_i));
	parent->second.connect( branch_i, child->first);
	child->second.reference();
      }
    }

    void disconnect( CommandNode::Id_t node_id) {
      std::map<CommandNode::Id_t, CommandNode >::iterator node = find(node_id);
      if (isValid(node)) {
	node->second.dereference();
      }
    }

    std::map<CommandNode::Id_t, CommandNode >::iterator find(CommandNode::Id_t an_id) {
      if (!CommandNode::isValidId(an_id)) return m_commands.end();
      else return m_commands.find(an_id);
    }

    bool isValid(std::map<CommandNode::Id_t, CommandNode >::iterator a_command_iterator) {
      return a_command_iterator != m_commands.end();
    }

    CommandNode::Id_t id(std::map<CommandNode::Id_t, CommandNode >::iterator a_command_iterator) {
      if (a_command_iterator == m_commands.end()) return CommandNode::invalidId();
      return a_command_iterator->first;
    }

    Mutex m_commandListMutex;
    Mutex m_commandRemoveMutex;
    std::map< CommandNode::Id_t, CommandNode >  m_commands;

    CommandNode::Id_t  m_currentCommandId;
    CommandNode::Id_t  m_lastExecutedCommandId;
    CommandNode::Id_t  m_nextCommandId;
    CommandNode::Id_t  m_lastNewCommandId;
    CommandNode::Id_t  m_commandIdMax;
    CommandNode::Id_t  m_jumpCommandId;

    std::shared_ptr<IExecutionListener>      m_listener;

    Flag m_exit;
    Flag m_hasCommandFlag;

    bool m_abort;
    bool m_stop;
    bool m_step;
    bool m_isRunning;
    bool m_testRunning;
  };

}
#endif
