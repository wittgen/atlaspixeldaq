#include "test_CommandListApplication.h"
#include "CommandList.h"
#include "ICommandFactory.h"

#include "ExecutionStatusEvent.h"
#include "CommandListExecutionListener.h"
#include "KitList.h"

class ICommandKit {
public:
  virtual ~ICommandKit() {};
  virtual PixCon::ICommand *createCommand(const std::string &command,  QApplication *app) const = 0;
};

class IClassificationKit {
public:
  virtual ~IClassificationKit() {};
  virtual PixCon::IClassification *createClassification(const std::string &classification,  QApplication *app) const = 0;
};



class TestCommand : public PixCon::ICommand
{
public:
  TestCommand(const std::string a_name, const std::string message, unsigned int execution_time)
      : m_name(a_name), m_message(message),m_executionTime(execution_time) {}

  void resetAbort() {
    m_abort = false;
  }


  PixCon::Result_t execute(PixCon::IStatusMessageListener &listener) {
    PixCon::Result_t ret;
    listener.statusMessage(m_message + " : start");
    m_execWait.timedwait(m_executionTime, m_abort);
    if (m_kill) {
      ret.setKilled();
    }
    else if (m_abort) {
      ret.setAborted();
    }
    else {
      ret.setSuccess();
    }
    listener.statusMessage(m_message + " : finish");
    return ret;
  }

  void abort() {
    m_abort=true;
    m_execWait.setFlag();
  }

  void kill() {
    m_abort=true;
    m_kill=true;
    m_execWait.setFlag();
  }

  const std::string &name() const { return m_name; }

private:

  std::string    m_name;
  std::string    m_message;

  bool           m_abort;
  bool           m_kill;
  unsigned int   m_executionTime;
  PixCon::Flag   m_execWait;
};

class TestCommandKit : public ICommandKit {
public:
  TestCommandKit(const std::string &message, unsigned int execution_time )
    : m_message(message),
      m_executionTime(execution_time)
  {}

  PixCon::ICommand *createCommand(const std::string &command, QApplication */*app*/) const {
    return new TestCommand(command, m_message, m_executionTime);
  }

private:
  std::string m_message;
  unsigned int m_executionTime;
};


  class QuitCommand : public PixCon::ICommand
  {
  public:
    QuitCommand(QApplication &app)  : m_app(&app) {}

    void resetAbort() {}

    PixCon::Result_t execute(PixCon::IStatusMessageListener &) {
      m_app->postEvent(m_app, new QEvent(QEvent::Quit));
      return PixCon::Result_t();
    }

    const std::string &name() const { return s_name; }

    void abort() {}

    void kill() {}

  private:
    QApplication *m_app;
    static const std::string    s_name;
  };

const std::string QuitCommand::s_name("quit");

class QuitCommandKit : public ICommandKit {
public:

  PixCon::ICommand *createCommand(const std::string &/*command*/, QApplication *app) const {
    assert(app);
    return new QuitCommand(*app);
  }

};


  class NopClassifcation : public PixCon::IClassification
  {
  public:
    const std::string &name() const { return s_name; }
    unsigned int classify(const PixCon::Result_t &/*result*/) {return 0; }
    unsigned int nBranches() const { return 1; }
    const std::string& branchName(unsigned int) const { return s_branchName; }
  private:
    static const std::string s_name;
    static const std::string s_branchName;
  };

const std::string NopClassifcation::s_name("Nop");
const std::string NopClassifcation::s_branchName("next");

class NopClassifcationKit : public IClassificationKit
{
public:
  PixCon::IClassification *createClassification(const std::string &/*classification*/,  QApplication */*app*/) const {
    return new NopClassifcation;
  }
};


  class SuccessFailureClassifcation : public PixCon::IClassification
  {
  public:
    SuccessFailureClassifcation(float success_rate) : m_successRate(success_rate) {}

    const std::string &name() const { return s_name; }
    unsigned int classify(const PixCon::Result_t &/*result*/) 
    { return (static_cast<float>(rand())/static_cast<float>(RAND_MAX)<m_successRate) ? 0 : 1; }

    unsigned int nBranches() const { return 2; }
    const std::string& branchName(unsigned int branch_i) const { assert(branch_i<2); return s_branchName[branch_i]; }
  private:
    float m_successRate;
    static const std::string s_name;
    static const std::string s_branchName[2];
  };


const std::string SuccessFailureClassifcation::s_name("SuccessFailure");
const std::string SuccessFailureClassifcation::s_branchName[2]={std::string("success"), std::string("failure")};

class SuccessFailureClassifcationKit : public IClassificationKit
{
public:
  SuccessFailureClassifcationKit(float success_rate) : m_successRate(success_rate) {}

  PixCon::IClassification *createClassification(const std::string &/*classification*/,  QApplication */*app*/) const {
    return new SuccessFailureClassifcation(m_successRate);
  }
private:
  float m_successRate;
};

  class LoopClassifcation : public PixCon::IClassification
  {
  public:
    LoopClassifcation(unsigned int n_loops) : m_nLoops(n_loops), m_counter(0) {}

    const std::string &name() const { return s_name; }
    unsigned int classify(const PixCon::Result_t &/*result*/) 
    { return ( (((++m_counter) % m_nLoops) == 0) ? 1 : 0); }

    unsigned int nBranches() const { return 2; }
    const std::string& branchName(unsigned int branch_i) const { assert(branch_i<2); return s_branchName[branch_i]; }
  private:
    unsigned int m_nLoops;
    unsigned int m_counter;

    static const std::string s_name;
    static const std::string s_branchName[2];
  };


const std::string LoopClassifcation::s_name("SuccessFailure");
const std::string LoopClassifcation::s_branchName[2]={std::string("loop"), std::string("exit")};

class LoopClassifcationKit : public IClassificationKit
{
public:
  LoopClassifcationKit(unsigned int n_loops) : m_nLoops(n_loops) {}

  PixCon::IClassification *createClassification(const std::string &/*classification*/,  QApplication */*app*/) const {
    return new LoopClassifcation(m_nLoops);
  }
private:
  unsigned int m_nLoops;
};


class CommandFactory : public PixCon::ICommandFactory
{
public:
  

  CommandFactory(QApplication *app) : m_app(app)
  {
    m_commandKitList.insert("quit", new QuitCommandKit, PixCon::UserMode::kExpert);
    m_commandKitList.insert("reset", new TestCommandKit("Reset",5), PixCon::UserMode::kExpert);
    m_commandKitList.insert("configure",new TestCommandKit("configure",10), PixCon::UserMode::kExpert);
    m_commandKitList.insert("quick",new TestCommandKit("Quick",1), PixCon::UserMode::kExpert);
    m_commandKitList.insert("scan",new TestCommandKit("Scan",30), PixCon::UserMode::kExpert);

    m_classificationKitList.insert("Nop",new NopClassifcationKit, PixCon::UserMode::kExpert);
    m_classificationKitList.insert("Success",new SuccessFailureClassifcationKit(.8), PixCon::UserMode::kExpert);
    m_classificationKitList.insert("Failure",new SuccessFailureClassifcationKit(.2), PixCon::UserMode::kExpert);
    m_classificationKitList.insert("Loop10",new LoopClassifcationKit(10), PixCon::UserMode::kExpert);
    m_classificationKitList.insert("Loop3",new LoopClassifcationKit(3), PixCon::UserMode::kExpert);
  }

  ~CommandFactory() {
  }


  unsigned int nCommands() const  {
    return m_commandKitList.size();
  }

  const std::string &commandName( unsigned int command_id ) const {
    if (command_id<m_commandKitList.size()) {
      return m_commandKitList.name(command_id);
    }
    return s_empty;
  }

  const PixCon::UserMode::EUserMode commandUser(unsigned int command_id) const {
    if (command_id<m_commandKitList.size()) {
      return m_commandKitList.user(command_id);
    }
    return PixCon::UserMode::kUnknown;
  }

  unsigned int nClassifications() const {
    return m_classificationKitList.size();
  }

  const std::string &classificationName( unsigned int command_id ) const {
    if (command_id<m_classificationKitList.size()) {
      return m_classificationKitList.name( command_id);
    }
    return s_empty;
  }

  PixCon::ICommand *createCommand(const std::string &command_name) const {
    const ICommandKit *kit= m_commandKitList.kit(command_name);
    if (!kit) {
      throw std::runtime_error("Command does not exist.");
    }
    return kit->createCommand(command_name, m_app);
  }

  PixCon::IClassification *createClassification(const std::string &command_name) const {
    const IClassificationKit *kit = m_classificationKitList.kit(command_name);
    if ( !kit) {
      throw std::runtime_error("Classification does not exist.");
    }
    return kit->createClassification(command_name, m_app);
  }

private:
  PixCon::KitList<ICommandKit>        m_commandKitList;
  PixCon::KitList<IClassificationKit> m_classificationKitList;

  QApplication *m_app;
  static const std::string s_empty;
};

const std::string CommandFactory::s_empty;

CommandListApplication::CommandListApplication(int argc, char ** argv) 
  : QApplication(argc, argv),
    m_timer(new QTimer(this)),
    m_testId(0),
    m_userMode(new PixCon::UserMode(PixCon::UserMode::kExpert)),
    m_executor(new PixCon::CommandExecutor),
    m_commandList(new PixCon::CommandList(m_userMode, m_executor,
					  std::shared_ptr<PixCon::ICommandFactory>(new CommandFactory(this))))
{
  m_commandList->m_progressFrame->setEnabled(false);
  m_commandList->m_progressFrame->hide();
  m_executor->setExecutionListener(std::shared_ptr<PixCon::IExecutionListener>(new PixCon::CommandListExecutionListener(*this,*m_commandList)));
  // queueCommand();
  //connect(m_timer.get(), SIGNAL(timeout()), this, SLOT(addCommand()));
}

CommandListApplication::~CommandListApplication() {}

bool CommandListApplication::event ( QEvent * event ) {
  if ( event->type()==QEvent::User) {
    QMessageEvent *message = dynamic_cast<QMessageEvent *>(event);
    if (message) {
      std::cout << "INFO [EventReceiver::event,got message : " << message->message() << std::endl;
      return true;
    }
  }
  else if (event->type()==QEvent::Quit) {
    m_executor->shutdown();
    m_executor->shutdownWait();
    quit();
    return true;
  }
  return false;
}

