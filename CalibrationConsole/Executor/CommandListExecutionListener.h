#ifndef _PixCon_CommandListExecutionListener_h_
#define _PixCon_CommandListExecutionListener_h_

#include <qapplication.h>
#include <qevent.h>
#include <iostream>
#include <CommandExecutor.h>
#include <ExecutionStatusEvent.h>
#include <StatusMessageEvent.h>

namespace PixCon {

  class CommandListExecutionListener  : public IExecutionListener {
  public:
    CommandListExecutionListener(QApplication &app, QObject &receiver)
      : m_app(&app),
        m_receiver(&receiver)
    {}

    void statusMessage(const std::string &message) {
      m_app->postEvent( m_receiver, new StatusMessageEvent(QString::fromStdString(message)) );
    }

    void startCommand(const std::string &name, CommandNode::Id_t id)
    {
      postEvent(ExecutionStatusEvent::kRunning,id);
      std::cout << "INFO [CommandListExecutionListener::startCommand] Started command: " << name << " (ID " << id << ")\n";
    }

    void finishedCommand(const std::string &name, CommandNode::Id_t id, bool success)
    {
      postEvent(( success ? ExecutionStatusEvent::kCommandSucceeded : ExecutionStatusEvent::kCommandFailed),id);
      std::cout << "INFO [CommandListExecutionListener::finishedCommand] Finished command: " << name << " (ID " << id << ") with success flag: " << success << std::endl;
    }

    void abortedCommand(const std::string &name, CommandNode::Id_t id)
    {
      postEvent(ExecutionStatusEvent::kAborted,id);
      std::cout << "INFO [CommandListExecutionListener::abortedCommand] Aborted command: " << name << " (ID " << id << ")\n";
    }

    void killedCommand(const std::string &name, CommandNode::Id_t id)
    {
      postEvent(ExecutionStatusEvent::kKilled,id);
      std::cout << "INFO [CommandListExecutionListener::killedCommand] Killed command: " << name << " (ID " << id << ")\n";
    }

    void nextCommand(const std::string &name, CommandNode::Id_t id)
    {
      postEvent(ExecutionStatusEvent::kNextCommand,id);
      std::cout << "INFO [CommandListExecutionListener::nextCommand] Next command: " << name << " (ID " << id << ")\n";
    }

    void caughtExceptionIn(const std::string &name, CommandNode::Id_t id, const std::string &message) {
      if (message.empty()) {
	postEvent(ExecutionStatusEvent::kException,id,"Unknown exception.");
      }
      else {
	postEvent(ExecutionStatusEvent::kException,id,message);
      }
      std::cout << "WARN [CommandListExecutionListener::caughtExceptionIn] Exception in command: " << name << " (ID " << id << "). Exception: " << message << "\n";
    }

    void stopped()
    {
      postEvent(ExecutionStatusEvent::kStopped);
      std::cout << "INFO [CommandListExecutionListener::stopped] Stopped execution" << std::endl;
    }

    void finished()
    { 
      postEvent(ExecutionStatusEvent::kFinished);
      std::cout << "INFO [CommandListExecutionListener::finished] Finished commands" << std::endl;
    }

    void waiting()
    {
      postEvent(ExecutionStatusEvent::kWaiting);
      std::cout << "INFO [CommandListExecutionListener::waiting] Waiting" << std::endl;
    }

    void postEvent( ExecutionStatusEvent::EStatus status, CommandNode::Id_t id=CommandNode::invalidId(), const std::string &command_name="") {
      m_app->postEvent( m_receiver, new ExecutionStatusEvent(status, id, command_name) );
    }
  private:
    QApplication *m_app;
    QObject      *m_receiver;
  };

}
#endif
