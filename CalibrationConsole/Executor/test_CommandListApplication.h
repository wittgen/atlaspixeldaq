#include <qapplication.h>
#include <qevent.h>
#include <qobject.h>
#include <qtimer.h>
#include <memory>
#include <string>
#include <sstream>
#include <CommandExecutor.h>
#include "UserMode.h"

namespace PixCon {
  class CommandList;
}

class CommandListApplication : public QApplication
{
  Q_OBJECT
public:
  CommandListApplication(int argc, char ** argv);

  ~CommandListApplication();

  class QMessageEvent : public QEvent
  {
  public:
    QMessageEvent(const std::string &message) : QEvent(QEvent::User), m_message(message) { }
    const std::string &message() {return m_message; }
  private:
    std::string m_message;
  };

//   void sendMessage(const std::string &message) {
//     postEvent(this, new QMessageEvent(message));
//   }

//   void sendQuitSignal() {
//     postEvent(this, new QEvent(QEvent::Quit));
//   }

  bool event ( QEvent * event );

//   void queueCommand() {
//     m_timer->start(500+(rand()%2000),true);
//   }

 public slots:

//  void addCommand() {
//    std::stringstream message;
//    message << "test " << ++m_testId << ".";
//    m_executor.appendCommand(new TestCommand("test",message.str(), rand()%3, *this ) , new NopClassifcation, PixCon::CommandNode::invalidId());
//    m_executor.showCommandTree();
//    queueCommand();
//  }

private:
  std::unique_ptr<QTimer>                       m_timer;
  unsigned int                                m_testId;
  std::shared_ptr<PixCon::UserMode>         m_userMode;
  std::shared_ptr<PixCon::CommandExecutor>  m_executor;
  //  std::shared_ptr<CommandFactory>           m_factory;
  std::unique_ptr<PixCon::CommandList>          m_commandList;

};
