#include "test_CommandListApplication.h"

int main( int argc, char ** argv )
{
  CommandListApplication a( argc, argv );
  return a.exec();
}
