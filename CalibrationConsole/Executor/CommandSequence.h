#ifndef _PixCon_CommandSequence_h_
#define _PixCon_CommandSequence_h_

#include "ICommand.h"
#include <memory>

namespace PixCon {

  /** For the time being just a dummy container to propagate command sequences to the command list
   */
  class CommandSequence : virtual public ICommand {
  public:

    CommandSequence(const std::string &name) : m_name(name){}

    ~CommandSequence() {
      for(std::vector<std::pair<ICommand *, IClassification *> >::iterator command_iter = m_commandSeq.begin();
	  command_iter != m_commandSeq.end();
	  ++command_iter) {
	delete command_iter->first;
	command_iter->first=NULL;
	delete command_iter->second;
	command_iter->second=NULL;
      }
    }

    void add(ICommand *command, IClassification *classification=NULL) {
      std::unique_ptr<ICommand> temp1(command);
      std::unique_ptr<IClassification> temp2(classification);
      m_commandSeq.push_back(std::make_pair(command, classification));
      temp1.release();
      temp2.release();
    }

    std::vector<std::pair<ICommand *, IClassification *> >::iterator begin() { return m_commandSeq.begin(); }
    std::vector<std::pair<ICommand *, IClassification *> >::iterator end() { return m_commandSeq.end(); }

    std::vector<std::pair<ICommand *, IClassification *> >::const_iterator begin() const { return m_commandSeq.begin(); }
    std::vector<std::pair<ICommand *, IClassification *> >::const_iterator end() const { return m_commandSeq.end(); }

    std::pair<ICommand *, IClassification *> takeFrontCommand() {
      if (m_commandSeq.empty()) return std::make_pair<ICommand *, IClassification *>(NULL,NULL);
      std::pair< ICommand *, IClassification *> front(*(m_commandSeq.begin()));
      std::unique_ptr<ICommand> temp1(front.first);
      std::unique_ptr<IClassification> temp2(front.second);
      m_commandSeq.erase(m_commandSeq.begin());
      temp1.release();
      temp2.release();
      return front;
    }

    const std::string &name() const {
      return m_name;
    }

    /** Can be used to initialise the command such that abort will work.
     */
    void resetAbort() {}

    /** Execute the command.
     * The method should abort if @ref abort got called.
     * The method should not exit before the command is terminated.
     */
    Result_t execute(IStatusMessageListener &) { assert(false); /*not meant for execution*/ }

    /** Abort the running command.
     */
    void abort() {}

    /** Kill the running command.
     * This will presumably leave parts of the system in an unusable state.
     */
    void kill() {};

  private:
    std::string m_name;
    std::vector<std::pair<ICommand *, IClassification *> > m_commandSeq;
  };
}

#endif
