#ifndef UI_COMMANDLISTBASE_H
#define UI_COMMANDLISTBASE_H

#include <QFrame>
#include <QProgressBar>
#include <QTreeWidget>
#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QDialog>
#include <QFrame>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QSpacerItem>
#include <QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_CommandListBase
{
public:
    QVBoxLayout *vboxLayout;
    QTreeWidget *m_commandListView;
    QLineEdit *m_messageEntry;
    QFrame *m_progressFrame;
    QVBoxLayout *vboxLayout1;
    QHBoxLayout *hboxLayout;
    QLabel *textLabel2;
    QSpacerItem *spacer6;
    QProgressBar *m_commandProgress;
    QFrame *line1;
    QHBoxLayout *hboxLayout1;
    QSpacerItem *spacer1;
    QPushButton *m_abortButton;
    QSpacerItem *spacer2;
    QPushButton *m_stopButton;
    QSpacerItem *spacer4;
    QPushButton *m_quitButton;
    QSpacerItem *spacer3;

    void setupUi(QDialog *CommandListBase)
    {
        if (CommandListBase->objectName().isEmpty())
            CommandListBase->setObjectName(QString::fromUtf8("CommandListBase"));
        CommandListBase->resize(380, 480);
        vboxLayout = new QVBoxLayout(CommandListBase);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        m_commandListView = new QTreeWidget(CommandListBase);
/*         m_commandListView->addColumn(QApplication::translate("CommandListBase", "Id", 0)); */
/*         m_commandListView->header()->setClickEnabled(true, m_commandListView->header()->count() - 1); */
/*         m_commandListView->header()->setResizeEnabled(true, m_commandListView->header()->count() - 1); */
/*         m_commandListView->addColumn(QApplication::translate("CommandListBase", "Command", 0)); */
/*         m_commandListView->header()->setClickEnabled(true, m_commandListView->header()->count() - 1); */
/*         m_commandListView->header()->setResizeEnabled(true, m_commandListView->header()->count() - 1); */
/*         m_commandListView->addColumn(QApplication::translate("CommandListBase", "Status", 0)); */
/*         m_commandListView->header()->setClickEnabled(true, m_commandListView->header()->count() - 1); */
/*         m_commandListView->header()->setResizeEnabled(true, m_commandListView->header()->count() - 1); */
/*         m_commandListView->addColumn(QApplication::translate("CommandListBase", "Cycles", 0)); */
/*         m_commandListView->header()->setClickEnabled(true, m_commandListView->header()->count() - 1); */
/*         m_commandListView->header()->setResizeEnabled(true, m_commandListView->header()->count() - 1); */
	m_commandListView->setColumnCount(4);
	QStringList headers;
	headers << QApplication::translate("CommandListBase", "Id", 0);
	headers << QApplication::translate("CommandListBase", "Command", 0);
	headers << QApplication::translate("CommandListBase", "Status", 0);
	headers << QApplication::translate("CommandListBase", "Cycles", 0);
	m_commandListView->setHeaderLabels(headers);
        m_commandListView->setObjectName(QString::fromUtf8("m_commandListView"));

        vboxLayout->addWidget(m_commandListView);

        m_messageEntry = new QLineEdit(CommandListBase);
        m_messageEntry->setObjectName(QString::fromUtf8("m_messageEntry"));
        m_messageEntry->setReadOnly(true);

        vboxLayout->addWidget(m_messageEntry);

        m_progressFrame = new QFrame(CommandListBase);
        m_progressFrame->setObjectName(QString::fromUtf8("m_progressFrame"));
        m_progressFrame->setFrameShape(QFrame::NoFrame);
        m_progressFrame->setFrameShadow(QFrame::Plain);
        vboxLayout1 = new QVBoxLayout(m_progressFrame);
        vboxLayout1->setSpacing(6);
        vboxLayout1->setContentsMargins(11, 11, 11, 11);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        textLabel2 = new QLabel(m_progressFrame);
        textLabel2->setObjectName(QString::fromUtf8("textLabel2"));
        textLabel2->setWordWrap(false);

        hboxLayout->addWidget(textLabel2);

        spacer6 = new QSpacerItem(101, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacer6);


        vboxLayout1->addLayout(hboxLayout);

        m_commandProgress = new QProgressBar(m_progressFrame);
        m_commandProgress->setObjectName(QString::fromUtf8("m_commandProgress"));

        vboxLayout1->addWidget(m_commandProgress);


        vboxLayout->addWidget(m_progressFrame);

        line1 = new QFrame(CommandListBase);
        line1->setObjectName(QString::fromUtf8("line1"));
        line1->setFrameShape(QFrame::HLine);
        line1->setFrameShadow(QFrame::Sunken);

        vboxLayout->addWidget(line1);

        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setSpacing(6);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        spacer1 = new QSpacerItem(51, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout1->addItem(spacer1);

        m_abortButton = new QPushButton(CommandListBase);
        m_abortButton->setObjectName(QString::fromUtf8("m_abortButton"));

        hboxLayout1->addWidget(m_abortButton);

        spacer2 = new QSpacerItem(31, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout1->addItem(spacer2);

        m_stopButton = new QPushButton(CommandListBase);
        m_stopButton->setObjectName(QString::fromUtf8("m_stopButton"));
        QFont font;
        font.setBold(true);
        m_stopButton->setFont(font);

        hboxLayout1->addWidget(m_stopButton);

        spacer4 = new QSpacerItem(31, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout1->addItem(spacer4);

        m_quitButton = new QPushButton(CommandListBase);
        m_quitButton->setObjectName(QString::fromUtf8("m_quitButton"));

        hboxLayout1->addWidget(m_quitButton);

        spacer3 = new QSpacerItem(71, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout1->addItem(spacer3);


        vboxLayout->addLayout(hboxLayout1);


        retranslateUi(CommandListBase);

        QMetaObject::connectSlotsByName(CommandListBase);
    } // setupUi

    void retranslateUi(QDialog *CommandListBase)
    {
        CommandListBase->setWindowTitle(QApplication::translate("CommandListBase", "CommandList", 0));
/*         m_commandListView->header()->setLabel(0, QApplication::translate("CommandListBase", "Id", 0)); */
/*         m_commandListView->header()->setLabel(1, QApplication::translate("CommandListBase", "Command", 0)); */
/*         m_commandListView->header()->setLabel(2, QApplication::translate("CommandListBase", "Status", 0)); */
/*         m_commandListView->header()->setLabel(3, QApplication::translate("CommandListBase", "Cycles", 0)); */
	QStringList headers;
	headers << QApplication::translate("CommandListBase", "Id", 0);
	headers << QApplication::translate("CommandListBase", "Command", 0);
	headers << QApplication::translate("CommandListBase", "Status", 0);
	headers << QApplication::translate("CommandListBase", "Cycles", 0);
	m_commandListView->setHeaderLabels(headers);
        textLabel2->setText(QApplication::translate("CommandListBase", "Progress :", 0));
        m_abortButton->setText(QApplication::translate("CommandListBase", "Abort", 0));
        m_stopButton->setText(QApplication::translate("CommandListBase", "Stop", 0));
        m_quitButton->setText(QApplication::translate("CommandListBase", "Quit", 0));
    } // retranslateUi

};

namespace Ui {
    class CommandListBase: public Ui_CommandListBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_COMMANDLISTBASE_H
