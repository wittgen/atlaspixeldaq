#ifndef _PixCon_ICommand_h_
#define _PixCon_ICommand_h_

#include <string>
#include <CalibrationDataTypes.h>

namespace PixCon {

  /** Result of a command.
   * Each command should set a status to one of @ref EStatus.
   * A command may allocate a sequence, scan, and/or analysis serial number.
   * The allocated serial numbers are stored in the result structure;
   */
  class Result_t {
  public:
    enum EStatus { kSuccess, kFailure, kAborted, kKilled, kUnknown, kExited };

    Result_t() : m_sequenceNumber(0), m_scan(0), m_analysis(0), m_status(kUnknown) {}

    void setSequenceNumber(SerialNumber_t sequence_number) { m_sequenceNumber = sequence_number; }
    void setScanSerialNumber(SerialNumber_t scan_serial_number) { m_scan = scan_serial_number; }
    void setAnalysisSerialNumber(SerialNumber_t analysis_serial_number) { m_analysis = analysis_serial_number; }

    void setKilled()  { m_status=kKilled; }
    void setAborted() { m_status=kAborted; }
    void setFailure() { m_status=kFailure; }
    void setSuccess() { m_status=kSuccess; }
    void setExited()  { m_status=kExited; }

    SerialNumber_t sequenceNumber() const       { return m_sequenceNumber; }
    SerialNumber_t scanSerialNumber() const     { return m_scan; }
    SerialNumber_t analysisSerialNumber() const { return m_analysis; }

    bool aborted() const { return m_status == kAborted; }
    bool killed()  const { return m_status == kKilled; }
    EStatus status() const { return m_status; }
    bool isUnfinished() const { return m_status == kUnknown; }
    bool exited() const { return m_status == kExited; }

  private:
    SerialNumber_t m_sequenceNumber;
    SerialNumber_t m_scan;
    SerialNumber_t m_analysis;
    EStatus        m_status;
  };


  class IStatusMessageListener {
  public:
    virtual ~IStatusMessageListener() {}
    virtual void statusMessage(const std::string &message) = 0;
  };

  /** The interface of a command.
   * Such commands can be executed by the @ref CommandExecutor
   */
  class ICommand {
  public:
    virtual ~ICommand() {}

    /** The name of the command.
     */
    virtual const std::string &name() const = 0;

    /** Can be used to initialise the command such that abort will work.
     */
    virtual void resetAbort() = 0;

    /** Execute the command.
     * The method should abort if @ref abort got called.
     * The method should not exit before the command is terminated.
     */
    virtual Result_t execute(IStatusMessageListener &listener)  = 0;

    /** Abort the running command.
     */
    virtual void abort() = 0;

    /** Kill the running command.
     * This will presumably leave parts of the system in an unusable state.
     */
    virtual void kill() = 0;

  };


  /** The interface of a classification which classifies the result of a command.
   * The result of a command @ref ICommand will be classified into a defined set of 
   * categories.
   */
  class IClassification {
  public:
    virtual ~IClassification() {}

    /** The name of the classification.
     */
    virtual const std::string &name() const = 0;

    /** Classify the given result into categories.
     */
    virtual unsigned int classify(const Result_t &result) =0;

    /** Return the number of possible categories (or branches).
     */
    virtual unsigned int nBranches() const = 0;

    /** Return the name of the given category (or branch).
     */
    virtual const std::string &branchName(unsigned int branch_i) const = 0;
  };

}
#endif
