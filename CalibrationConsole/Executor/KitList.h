// Dear emacs, this is -*-c++-*-
#ifndef _PixCon_KitList_h_
#define _PixCon_KitList_h_

#include <map>
#include <memory>
#include <cassert>
#include <string>
#include <stdexcept>
#include <vector>
#include "UserMode.h"

namespace PixCon {

  template <class T>
  class KitList
  {
  public:

    ~KitList() {
      for (typename std::map<std::string,  T *>::iterator kit_iter = m_list.begin();
	   kit_iter != m_list.end();
	   kit_iter++) {
	delete kit_iter->second;
	kit_iter->second=NULL;
      }
    }

    /** Add a new kit to the list.
     */
    void insert(const std::string &name, T *a, const PixCon::UserMode::EUserMode& user) {
      std::unique_ptr<T> ptr(a);
      typename std::map<std::string, T*>::iterator iter = m_list.find(name);
      if (iter != m_list.end()) {
	std::string message("FATAL [KitList::insert] Cannot register kit. Kit of name" );
	message += name;
	message += " exists already.";
	throw std::runtime_error(message);
      }
      m_list.insert(std::make_pair(name, ptr.release()));
      m_nameList.push_back(name);
      m_userList.push_back(user);
    }

    /** Get the number of kits.
     */
    unsigned int size() const { return m_list.size(); }

    /** Get a kit name.
     */
    const std::string &name(unsigned int index) const {
      assert(index<m_list.size());
      assert(m_list.size() == m_nameList.size());
      return m_nameList[index];
    }

    /** Get a kit user.
     */
    const PixCon::UserMode::EUserMode user(unsigned int index) const {
      assert(index<m_list.size());
      assert(m_list.size() == m_userList.size());
      return m_userList[index];
    }

    /** Get kit of the given name.
     */
    const T *kit(const std::string &name) const {
      typename std::map<std::string, T *>::const_iterator kit_iter = m_list.find(name);
      if (kit_iter == m_list.end()) return NULL;
      return kit_iter->second;
    }

  private:
    std::map<std::string, T *> m_list;
    std::vector<std::string>   m_nameList;
    std::vector<PixCon::UserMode::EUserMode> m_userList;
  };

}
#endif
