#ifndef _PixCon_CommandList_h_
#define _PixCon_CommandList_h_

#include <ui_CommandListBase.h>
#include <memory>
#include "CommandExecutor.h"
#include "UserMode.h"

class QEvent;

namespace PixCon {

//  class CommandExecutor;
  class ICommandFactory;
  class CommandSequence;

  class CommandList : public QDialog, public Ui_CommandListBase
  {
    Q_OBJECT

  public:
    enum EReset {kRemove, kConnection, kSetNextCommand, kAbortCommand, kStartCommand, kRebuildCommands, kCommand};

    CommandList(std::shared_ptr<PixCon::UserMode>& user_mode,
		const std::shared_ptr<CommandExecutor> &executor,
		const std::shared_ptr<ICommandFactory> &factory,
		QWidget* parent=NULL);

    ~CommandList();



    void clearCommand();
    //    void appendCommand(QListViewItem *);

  
    void stopExecution();

    void executeCommand(ICommand *command, IClassification *classification=NULL, bool start=true);

  public slots:
    void progress(unsigned int step, unsigned int total_steps);
    void setStatusMessage(const QString &message);
    void abortCommand();
    void stopCommand();
    void contextMenu(const QPoint &);
    
    void sCommands();
    void sNextCommand();
    void sStartCommand();
    void sConnection();
    void sRemove();
    void sRebuildCommand();
    
    
  protected:
    void executeCommandSequence(CommandSequence *command_sequence);

  private:
    std::shared_ptr<PixCon::UserMode> m_userMode;
    std::shared_ptr<CommandExecutor> m_executor;
    std::shared_ptr<ICommandFactory> m_factory;

    class SelectedItems_t
    {
    public:
      SelectedItems_t()
	: m_nBranches(0),
	  m_nCommands(0),
	  m_nOther()
      {}
      void other() { m_nOther++;}
      void command() { m_nCommands++;}
      void branch() { m_nBranches++;}

      bool oneBranch() const       { return m_nBranches==1 && m_nOther==0 && m_nCommands==0; }
      bool oneCommand() const      { return m_nBranches==0 && m_nOther==0 && m_nCommands==1; }
      bool oneOtherItem() const    { return m_nBranches==0 && m_nOther==1 && m_nCommands==0; }
      bool multipleItems() const   { return m_nBranches+m_nOther+m_nCommands > 1; }
      bool nothingSelected() const { return m_nBranches==0 && m_nOther==0 && m_nCommands==0; }
      bool oneConnection() const   { return m_nBranches==1 && m_nOther==0 && m_nCommands==1; }

    private:

      unsigned int m_nBranches;
      unsigned int m_nCommands;
      unsigned int m_nOther;
    };

    enum EButtonMode {kDisabled, kStart, kStop, kContinue, kAbort, kAborting, kKill, kNButtonModes};
    EButtonMode        m_stopButtonMode;
    static const char *s_buttonText[kNButtonModes];

    void setButton();
    void updateButton();
    void setButton(EButtonMode mode);

    QTreeWidgetItem *findItem(CommandNode::Id_t id);
    QTreeWidgetItem *m_lastActiveItem;
    QTreeWidgetItem *m_lastNextItem;

    bool event(QEvent *event );

    std::string m_lastRunningCommand;

    static SelectedItems_t  nSelectedItems(QTreeWidget *view) ;

  };


}
#endif
