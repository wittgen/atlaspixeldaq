// Dear emacs, this is -*-c++-*-
#ifndef _PixCon_ICommandFactory_h_
#define _PixCon_ICommandFactory_h_

#include "UserMode.h"

namespace PixCon {

  class ICommand;
  class IClassification;

  class ICommandFactory
  {
  public:
    virtual ~ICommandFactory() {}

    virtual unsigned int nCommands() const = 0;
    virtual const std::string &commandName( unsigned int command_id ) const = 0;
    virtual const PixCon::UserMode::EUserMode commandUser( unsigned int command_id ) const = 0;

    virtual unsigned int nClassifications() const  = 0;
    virtual const std::string &classificationName( unsigned int command_id ) const = 0;

    virtual ICommand *createCommand(const std::string &command_name) const = 0;
    virtual IClassification *createClassification(const std::string &classification_name) const = 0;
  };

}
#endif
