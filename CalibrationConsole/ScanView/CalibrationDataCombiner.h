// Dear emacs, this is -*-c++-*-
#ifndef _PixCon_CalibrationDataCombiner_h_
#define _PixCon_CalibrationDataCombiner_h_

#include <CalibrationDataTypes.h>
#include <VarDefList.h>
#include <vector>
#include <string>
#include <map>
#include <memory>
#include <CalibrationData.h>

namespace PixCon {

  class CalibrationDataManager;

  class CalibrationDataCombiner
  {
  public:
    CalibrationDataCombiner(const std::shared_ptr<CalibrationDataManager> &calibration_data,
			    SerialNumber_t dest_analysis_serial_number);

    virtual ~CalibrationDataCombiner();

    virtual void addMeasurement(EMeasurementType src_measurement_type,
				SerialNumber_t src_serial_number) = 0;

    void dumpReport();
  protected:
    virtual void updateValues(EMeasurementType src_measurement_type,
			      SerialNumber_t src_serial_number,
			      EConnItemType conn_item_type,
			      const std::string &conn_name,
			      const std::vector<VarDef_t> &var_def_list) = 0;

    virtual std::string makeValueString(CalibrationData::ConnObjDataList data_list,
					EMeasurementType measurement,
					SerialNumber_t serial_number,
					VarDef_t var_def) = 0;

    void _addMeasurement(EMeasurementType src_measurement_type,
			 SerialNumber_t src_serial_number,
			 EValueType type_restriction);

    void reportValue(const std::string &conn_name);

    static bool match(EValueConnType value_conn_type, EConnItemType conn_item_type) 
    {
      switch (value_conn_type) {
      case kAllConnTypes: return true;
      case kRodValue: return conn_item_type == kRodItem ;
      case kTimValue: return conn_item_type == kTimItem ;
      case kModuleValue: return conn_item_type == kModuleItem ;
      default:
	return false;
      }
    }


    std::shared_ptr<CalibrationDataManager>  m_calibrationData;
    SerialNumber_t                             m_analysisSerialNumber;

    std::map<std::string, std::map<std::string, std::pair<EMeasurementType, SerialNumber_t> > > m_log;
  };

}
#endif
