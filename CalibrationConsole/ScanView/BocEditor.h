/* Dear emacs, this is -*-c++-*- */
#ifndef _PixA_BocEditor_h_
#define _PixA_BocEditor_h_

#include "ModuleMapH2Factory.h"
#include "ui_BocEditorBase.h"
#include <memory>

#include <CalibrationDataTypes.h>
#include <ConfigWrapper/ConnectivityUtil.h>

#include <VarDefList.h>
//#include <q3listview.h>
#include <QTreeWidgetItem>//
#include <map>
#include <sstream>
#include <memory>

namespace PixA {
  class ConnObjConfigDbTagRef;
}

namespace PixLib {
  class RodBocConnectivity;
  class ModuleConnectivity;
  class PixModuleGroup;
}

class Highlighter;

namespace PixCon {
  class CalibrationDataManager;
  class BocSettingsItem;

  class BocEditor : public QDialog, public Ui_BocEditorBase, public IBocEditor {
  Q_OBJECT
  public:
    class BocSettings_t;
    enum EMccSpeed {kDefault, kMcc40, kMcc80};

    BocEditor(const std::shared_ptr<CalibrationDataManager> &calibration_data);
    ~BocEditor();

    void resetRxSettings(const std::string &conn_name);

    void changeRxSettings(const std::string &conn_name, unsigned int link, float rx_delay, float rx_threshold, SerialNumber_t ref_scan_serial_number);
      
    std::pair<float, float> rxSettings(const std::string &conn_name, unsigned int link, SerialNumber_t ref_scan_serial_number);

    std::pair<float, float> analysisRxSettings(const std::string &conn_name, unsigned int link, SerialNumber_t ref_scan_serial_number);

    void showEditor();

    void hideEditor();

    void saveChanges(const std::string& pending_tag);

  public slots:
    void saveChangesTemp();
    void saveChangesPerm();

    void removeAllChanges();

    void changeSettings(QTreeWidgetItem *boc_settings_item, int i);//

    void edit(const std::string &conn_name, unsigned int histo_link_index, EMccSpeed speed);

  protected:
    void createFullList();
    void createListOfChanges();

    void _changeRxSettings(const std::string &conn_name,
			   PixA::EDataLink data_link,
			   float rx_delay,
			   float rx_threshold,
			   SerialNumber_t ref_scan_serial_number);

    void _changeRxSettings(const std::string &conn_name,
			   PixA::EDataLink data_link,
			   EMccSpeed speed,
			   float rx_delay,
			   float rx_threshold,
			   SerialNumber_t ref_scan_serial_number);

    BocSettings_t &createSettings(const std::string &conn_name, PixA::EDataLink data_link, EMccSpeed speed);

    std::pair< std::pair<float, float> ,int> 
      loadSettings(const std::string &conn_name, PixA::EDataLink data_link, EMccSpeed speed_mode, const std::string &tag_name="");

    void updateTags();
    bool tagExist(const QString &tag) const;
    void _edit(const std::string &conn_name, PixA::EDataLink data_link, EMccSpeed speed);

  private:

    EMccSpeed speedMode( SerialNumber_t scan_serial_number);

    static std::string rxName(int plugin) {
      std::stringstream rx_id;
      rx_id << "PixRx" << plugin;
      return rx_id.str();
    }

    static std::string txName(int plugin) {
      std::stringstream tx_id;
      tx_id << "PixTx" << plugin;
      return tx_id.str();
    }
 
    static std::string channelName(const std::string &head, EMccSpeed speed, int channel) {
      std::stringstream data_delay;
      data_delay << head;
      if (speed==kMcc40) {
	data_delay << "_40_";
      }
      else if (speed==kMcc80) {
	data_delay << "_80_";
      }
      data_delay << channel;
      return data_delay.str();
    }

    static std::string laserCurrentName(int channel) {
      std::stringstream laser_current_name;
      laser_current_name << "LaserCurrent" << channel;
      return laser_current_name.str();
    }

    static std::string visetName(EMccSpeed speed) {
      std::string viset_name("Viset");
      if (speed==kMcc40) {
	viset_name+="_40";
      }
      else if (speed==kMcc80) {
	viset_name+="_80";
      }
      return viset_name;
    }

    static std::string dataDelayName(EMccSpeed speed, int channel) {
      return channelName("DataDelayF",speed, channel+2);
    }

    static std::string thresholdName(EMccSpeed speed, int channel) {
      return channelName("RxThresholdF",speed, channel+2);
    }

    static std::string phaseDelayName(int channel) {
      return "Phases_"+std::to_string(channel+2);
    }

  public:
    static const char *linkTitle(PixA::EDataLink data_link) {
      return s_linkTitle[(data_link==PixA::DTO1 ? PixA::DTO1 : PixA::DTO2)];
    }
    
    static const char *speedTitle(EMccSpeed speed) {
      assert(speed<=kMcc80);
      return s_speedTitle[speed];
    }

    static std::string rxPluginTitle(unsigned int plugin) {
      assert(plugin<4);
      std::string title("Rx");
      title += ('A'+plugin);
      return title;
    }

    static std::string txPluginTitle(unsigned int plugin) {
      assert(plugin<4);
      std::string title("Tx");
      title += ('A'+plugin);
      return title;
    }

  private:
    bool hasSomethingToSave() const;

    BocSettingsItem *createItem(const std::string &conn_name, PixA::EDataLink data_link, EMccSpeed speed_mode);
//     std::string rodName(const std::string &conn_name);
//     PixLib::RodBocConnectivity *rod(const std::string &conn_name);
//     PixLib::ModuleConnectivity *module(const std::string &conn_name);

    std::shared_ptr<PixLib::PixModuleGroup> createConfiguredModuleGroup(const PixA::ConnObjConfigDbTagRef &tag_ref,
									  const std::string &conn_name) const;

    CalibrationDataManager &calibrationData()             { assert(m_calibrationData); return *m_calibrationData;}
    const CalibrationDataManager &calibrationData() const { assert(m_calibrationData); return *m_calibrationData;}

    SerialNumber_t findAnalysis(SerialNumber_t scan_serial_number, const std::string &analysis_name);

  public:
    class BocSettings_t {
      enum EValueType {kRxThreshold,kRxDelay, kTxPower,kNValues};
    public:
      BocSettings_t()
	: m_serialNumber(0)
      { 
	reset(); 
	m_refValue[kTxPower]=0;
      }

      BocSettings_t(float rx_delay, float rx_threshold)
	: m_serialNumber(0)
      { reset(); setRxDelay(rx_delay); setRxThreshold(rx_threshold); }

      BocSettings_t(float rx_delay, float rx_threshold, int power)
	: m_serialNumber(0) 
      {reset(); setRxDelay(rx_delay); setRxThreshold(rx_threshold); setTxPower(power);}

      void reset() {
	for(unsigned int value_type_i=0; value_type_i<kNValues; value_type_i++) {
	  m_hasValue[value_type_i]=false;
	}
      }

      void setReference(SerialNumber_t serial_number) {
	m_serialNumber = serial_number;
      }

      SerialNumber_t referenceScan() const {
	return m_serialNumber;
      }

      void setRxReference(float delay, float threshold) {
	m_refValue[kRxDelay]=delay;
	m_refValue[kRxThreshold]=threshold;
      }

      void setTxReference(int power) {
	m_refValue[kTxPower]=power;
      }

      bool rxThresholdDiffers() const { return differs(kRxThreshold); }
      bool rxDelayDiffers() const { return differs(kRxDelay); }
      bool txPowerDiffers() const { return differs(kTxPower); }

      bool differs() const {
	return txPowerDiffers() || rxDelayDiffers() || rxThresholdDiffers();
      }

      void setRxThreshold(float threshold) {
	if (threshold<=255) {
	  m_value[kRxThreshold]=threshold;
	  m_hasValue[kRxThreshold]=true;
	}
	else{
	  m_hasValue[kRxThreshold]=false;
	}
      }

      bool hasRxThreshold() const {
	return m_hasValue[kRxThreshold];
      }

      float rxThresholdRef() const {
	return m_refValue[kRxThreshold];
      }

      float rxThreshold() const {
	return m_value[kRxThreshold];
      }

      void setRxDelay(float delay) {
	if (delay<=25) {
	  m_value[kRxDelay]=delay;
	  m_hasValue[kRxDelay]=true;
	}
	else {
	  m_hasValue[kRxDelay]=false;
	}
      }

      bool hasRxDelay() const {
	return m_hasValue[kRxDelay];
      }

      float rxDelayRef() const {
	return m_refValue[kRxDelay];
      }

      float rxDelay() const {
	return m_value[kRxDelay];
      }

      void setTxPower(int power) {
	if (power<=255) {
	  m_value[kTxPower]=(float)(power);
	  m_hasValue[kTxPower]=true;
	}
	else {
	  m_hasValue[kTxPower]=false;
	}
      }

      bool hasTxPower() const {
	return m_hasValue[kTxPower];
      }

      int txPowerRef() const {
	return (int)(m_refValue[kTxPower]);
      }

      int txPower() const {
	return (int)(m_value[kTxPower]);
      }


    private:
      bool differs(EValueType type) const {
	return m_hasValue[type] && m_value[type] != m_refValue[type];
      }

      SerialNumber_t m_serialNumber;
      float m_value[kNValues];
      float m_refValue[kNValues];
      bool m_hasValue[kNValues];
    };
  private:

    class Key_t {
    public:
      Key_t(const std::string &conn_name, PixA::EDataLink data_link, EMccSpeed speed)
	: m_name(conn_name),
	  m_link(data_link),
	  m_speed(speed)
      {  }

      EMccSpeed speed() const         { return m_speed; }
      const std::string &name() const { return m_name;}
      PixA::EDataLink link() const       { return m_link; }

      bool operator<(const Key_t &a) const {
	int ret = name().compare(a.name() );
	return ret<0 || (ret==0 && ( link() <a.link() || ( link()==a.link() && speed()<a.speed())));
      }

    private: 
      std::string    m_name;
      PixA::EDataLink m_link;
      EMccSpeed      m_speed;
    };

//     bool oeprator<(const Key_t &a, const Key_t &b) {
//       return a.operator<(b);
//     }

    std::shared_ptr<CalibrationDataManager> m_calibrationData;
    std::map<Key_t, BocSettings_t>      m_newSettings;
    std::unique_ptr<Highlighter>          m_highlighter;

    bool m_updateTags;

    static const char *s_tagExtension;
    static const char *s_linkTitle[2];
    static const char *s_speedTitle[3];

    static void initSpeedMap();

    static std::map<std::string, EMccSpeed> s_speedMap;
    static const char *s_optoLinkAnalysis;
    static const char *s_analysisOptimalSettings[2][2];
    VarId_t m_analysisOptimalVarId[2][2];
    bool m_haveAnalysisOptimalVar ;
  };

}
#endif
