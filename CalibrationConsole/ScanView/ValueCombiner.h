// Dear emacs, this is -*-c++-*-
#ifndef _PixCon_ValueCombiner_h_
#define _PixCon_ValueCombiner_h_

//#include <CalibrationDataTypes.h>
//#include <VarDefList.h>
//#include <vector>
//#include <string>
//#include <memory>
#include <CalibrationDataCombiner.h>

// #include <ScanMetaDataCatalogue.h>

namespace PixCon {

  class CalibrationDataManager;

  class ValueCombiner : public CalibrationDataCombiner
  {
  public:
    ValueCombiner(const std::shared_ptr<CalibrationDataManager> &calibration_data,
		   SerialNumber_t dest_analysis_serial_number)
      : CalibrationDataCombiner(calibration_data, dest_analysis_serial_number) 
    {}

    ~ValueCombiner() {}

    void addMeasurement(EMeasurementType src_measurement_type,
			SerialNumber_t src_serial_number)
    {
      _addMeasurement(src_measurement_type, src_serial_number, kValue);
    }


  protected:
    void updateValues(EMeasurementType src_measurement_type,
		      SerialNumber_t src_serial_number,
		      EConnItemType conn_item_type,
		      const std::string &conn_name,
		      const std::vector<VarDef_t> &var_def_list);

    std::string makeValueString(CalibrationData::ConnObjDataList data_list,
				EMeasurementType measurement,
				SerialNumber_t serial_number,
				VarDef_t var_def);

  };

}
#endif
