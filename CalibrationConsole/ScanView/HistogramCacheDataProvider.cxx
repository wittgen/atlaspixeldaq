#include "HistogramCache.h"
#include "HistogramCacheDataProvider.h"
#include <DataContainer/GenericHistogramAccess.h>

namespace PixCon {
  const std::string HistogramCacheDataProvider::s_emptyString;

  const PixA::Histo *HistogramCacheDataProvider::histogram(const std::string &connectivity_name,
							   const std::string &histo_name,
							   const PixA::Index_t &index)
  {
      EConnItemType conn_item_type = guessConnItemType(connectivity_name);
      if (conn_item_type>=kNHistogramConnItemTypes) return NULL;
      if (m_lastHistogramName != histo_name) {
	m_lastHistogramName = histo_name;
	m_lastHistogramIndex = m_histogramCache->histogramIndex(m_type, m_serialNumber,conn_item_type, m_lastHistogramName);
      }

      PixA::Histo *histo = m_histogramCache->histogram(m_type, m_serialNumber, conn_item_type, connectivity_name, m_lastHistogramIndex, index );
      if (histo) {
      std::string the_histo_name=PixA::histoName(histo);
      std::string::size_type server_name_end=the_histo_name.find(".");
      if (server_name_end!=std::string::npos) {
	std::string::size_type provider_header_end=the_histo_name.find(".",server_name_end);
	std::string::size_type bare_histo_name=the_histo_name.find(":",server_name_end);
	if(bare_histo_name != std::string::npos && bare_histo_name+2<the_histo_name.size()) {
	  provider_header_end=bare_histo_name;
	}

	//std::cout << "INFO [HistogramCacheDataProvider::histogram] " << the_histo_name << "( cut=" << provider_header_end<< ")"<< std::endl;
	//       std::string::size_type last_folder=histo_name.rfind("/");
	//       if (last_folder!=std::string::npos) {
	// 	provider_header_end=last_folder;
	//       }
	if (provider_header_end!= std::string::npos && the_histo_name.size()-provider_header_end>2) {
	  histo->SetName(the_histo_name.substr(provider_header_end+1,histo_name.size()-provider_header_end-1).c_str());
	}
      }
      }
      return histo;
    }

}
