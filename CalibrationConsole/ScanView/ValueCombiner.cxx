#include "ValueCombiner.h"
#include <CalibrationDataManager.h>

namespace PixCon {

  inline void updateEnableState(PixCon::CalibrationData::EnableState_t &dest_enable_state,  const PixCon::CalibrationData::EnableState_t &src_enable_state) {
    if (src_enable_state.isInConnectivity()) {
      dest_enable_state.setInConnectivity(true);
    }
    if (src_enable_state.isInConfiguration()) {
      dest_enable_state.setInConnectivity(true);
    }
    if (src_enable_state.isParentEnabled()) {
      dest_enable_state.setParentEnabled(true);
    }
    if (src_enable_state.isTemporaryEnabled()) {
      dest_enable_state.setEnabled(true);
    }
  }

  std::string ValueCombiner::makeValueString(CalibrationData::ConnObjDataList data_list,
					     EMeasurementType measurement,
					     SerialNumber_t serial_number,
					     VarDef_t var_def)
  {
    if ( !var_def.isValueWithOrWithoutHistory() ) { throw CalibrationData::MissingValue("Not a value"); }

    float src_value = data_list.newestValue<float>(measurement, serial_number, var_def);
    const ValueVarDef_t &value_var_def = var_def.valueDef();

    std::stringstream value_string;
    value_string << src_value << " " << value_var_def.unit();
    return value_string.str();
  }



  void ValueCombiner::updateValues(EMeasurementType src_measurement_type,
				    SerialNumber_t src_serial_number,
				    EConnItemType conn_item_type,
				    const std::string &conn_name,
				    const std::vector<VarDef_t> &var_def_list)
  {
    try {
      Lock lock(m_calibrationData->calibrationDataMutex());
      CalibrationData::ConnObjDataList conn_obj_data ( m_calibrationData->calibrationData().getConnObjectData(conn_name) );
      const PixCon::CalibrationData::EnableState_t &conn_obj_enable_state(conn_obj_data.enableState(src_measurement_type, src_serial_number));

      // only consider the object if it is enabled ?
      //      if (!conn_obj_enable_state.enabled()) return;
      // or demand at least the parent to be enabled ?
      //      if (!conn_obj_enable_state.enabled() && conn_obj_enable_state.isParentEnabled()) return;

      PixCon::CalibrationData::EnableState_t &dest_conn_obj_enable_state(conn_obj_data.enableState(kAnalysis, m_analysisSerialNumber));
      //      dest_conn_obj_enable_state = conn_obj_enable_state;

      //      bool now_enabled = conn_obj_enable_state.enabled() && !dest_conn_obj_enable_state.enabled();

      updateEnableState(dest_conn_obj_enable_state, conn_obj_enable_state);

      for ( std::vector<VarDef_t>::const_iterator var_iter = var_def_list.begin();
	    var_iter != var_def_list.end();
	    var_iter ++) {

	// only states should be in the list by construction.
	  assert ( var_iter->valueType() == kValue );
	  if ( !match( var_iter->connType(), conn_item_type) ) continue;

	  try {
	    float src_value = conn_obj_data.value<float>(src_measurement_type, src_serial_number, var_iter->id());

	    // if the object is not enabled only but the object was enabled in one of the measurements
	    // then only set the value if it was not set yet.
	    // Also ignore the new value if it is outside the given range unless (unless it has not been set yet).
	    if (( src_value<=0 || src_value>=9999.) || (!conn_obj_enable_state.enabled() && dest_conn_obj_enable_state.enabled())) {
	      try {
		float dest_value =  conn_obj_data.value<float>(kAnalysis, m_analysisSerialNumber, var_iter->id());
		if (src_value<=0 || src_value>=999) {
		  std::cout << "INFO [ValueCombiner::updateValues] add for " << conn_name << " : " << VarDefList::instance()->varName( var_iter->id())
			    << " value " << src_value << " of " << makeSerialNumberString(src_measurement_type, src_serial_number) << " ignored."
			    << " Kepp old value = " << dest_value
			    << std::endl;
		}
		continue;
	      }
	      catch (CalibrationData::MissingValue &){
	      }
	    }


	    //	    std::cout << "INFO [ValueCombiner::updateValues] add for " << conn_name << " : " << VarDefList::instance()->varName( var_iter->id())
	    //		      << " = " << src_value
	    //		      << ( dest_conn_obj_enable_state.enabled() ?  " (enabled) " : " (disabled)" )
	    //		      << std::endl;

	    m_calibrationData->calibrationData().addAnalysisValue(var_iter->connType(),
								  conn_name,
								  m_analysisSerialNumber, 
								  VarDefList::instance()->varName( var_iter->id()),
								  src_value);

	    // memorise the measurement which defined this value
	    m_log[VarDefList::instance()->varName( var_iter->id())][conn_name]=std::make_pair(src_measurement_type, src_serial_number);

	  }
	  catch (CalibrationData::MissingValue &){
	  }
	}
    }
    catch (CalibrationData::MissingConnObject &) {
    }
  }

}
