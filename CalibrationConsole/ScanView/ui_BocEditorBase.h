#ifndef UI_BOCEDITORBASE_H
#define UI_BOCEDITORBASE_H

#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QCheckBox>
#include <QDialog>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QPushButton>
#include <QSpacerItem>
#include <QVBoxLayout>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QComboBox>

QT_BEGIN_NAMESPACE

class Ui_BocEditorBase
{
public:
    QVBoxLayout *vboxLayout;
    QLabel *titleLable;
    QCheckBox *m_onlyModifiedRodsButton;
//    Q3ListView *m_bocSettingsListView;
    QTreeWidget * m_bocSettingsListView;//
    QTreeWidgetItem *m_bocSettingsListViewHeaders;//
    QHBoxLayout *hboxLayout;
    QPushButton *m_cancelButton;
    QSpacerItem *spacer37;
    QLabel *textLabel1;
    QComboBox *m_tagSelector;//
    QPushButton *m_saveTempButton;
    QPushButton *m_savePermButton;
    QPushButton *m_closeButton;

    void setupUi(QDialog *BocEditorBase)
    {
        if (BocEditorBase->objectName().isEmpty())
            BocEditorBase->setObjectName(QString::fromUtf8("BocEditorBase"));
        BocEditorBase->resize(587, 480);
        vboxLayout = new QVBoxLayout(BocEditorBase);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        titleLable = new QLabel(BocEditorBase);
        titleLable->setObjectName(QString::fromUtf8("titleLable"));
        QFont font;
        font.setPointSize(14);
        font.setBold(true);
        titleLable->setFont(font);
        titleLable->setIndent(16);
        titleLable->setWordWrap(false);

        vboxLayout->addWidget(titleLable);

        m_onlyModifiedRodsButton = new QCheckBox(BocEditorBase);
        m_onlyModifiedRodsButton->setObjectName(QString::fromUtf8("m_onlyModifiedRodsButton"));

        vboxLayout->addWidget(m_onlyModifiedRodsButton);
/*
        m_bocSettingsListView = new Q3ListView(BocEditorBase);
        m_bocSettingsListView->addColumn(QApplication::translate("BocEditorBase", "Name", 0));
        m_bocSettingsListView->header()->setClickEnabled(true, m_bocSettingsListView->header()->count() - 1);
        m_bocSettingsListView->header()->setResizeEnabled(true, m_bocSettingsListView->header()->count() - 1);
        m_bocSettingsListView->addColumn(QApplication::translate("BocEditorBase", "Link", 0));
        m_bocSettingsListView->header()->setClickEnabled(true, m_bocSettingsListView->header()->count() - 1);
        m_bocSettingsListView->header()->setResizeEnabled(true, m_bocSettingsListView->header()->count() - 1);
        m_bocSettingsListView->addColumn(QApplication::translate("BocEditorBase", "RX", 0));
        m_bocSettingsListView->header()->setClickEnabled(true, m_bocSettingsListView->header()->count() - 1);
        m_bocSettingsListView->header()->setResizeEnabled(true, m_bocSettingsListView->header()->count() - 1);
        m_bocSettingsListView->addColumn(QApplication::translate("BocEditorBase", "Delay", 0));
        m_bocSettingsListView->header()->setClickEnabled(true, m_bocSettingsListView->header()->count() - 1);
        m_bocSettingsListView->header()->setResizeEnabled(true, m_bocSettingsListView->header()->count() - 1);
        m_bocSettingsListView->addColumn(QApplication::translate("BocEditorBase", "Threshol", 0));
        m_bocSettingsListView->header()->setClickEnabled(true, m_bocSettingsListView->header()->count() - 1);
        m_bocSettingsListView->header()->setResizeEnabled(true, m_bocSettingsListView->header()->count() - 1);
        m_bocSettingsListView->addColumn(QApplication::translate("BocEditorBase", "TX", 0));
        m_bocSettingsListView->header()->setClickEnabled(true, m_bocSettingsListView->header()->count() - 1);
        m_bocSettingsListView->header()->setResizeEnabled(true, m_bocSettingsListView->header()->count() - 1);
        m_bocSettingsListView->addColumn(QApplication::translate("BocEditorBase", "Power", 0));
        m_bocSettingsListView->header()->setClickEnabled(true, m_bocSettingsListView->header()->count() - 1);
        m_bocSettingsListView->header()->setResizeEnabled(true, m_bocSettingsListView->header()->count() - 1);
        m_bocSettingsListView->addColumn(QApplication::translate("BocEditorBase", "Reference", 0));
        m_bocSettingsListView->header()->setClickEnabled(true, m_bocSettingsListView->header()->count() - 1);
        m_bocSettingsListView->header()->setResizeEnabled(true, m_bocSettingsListView->header()->count() - 1);
        m_bocSettingsListView->setObjectName(QString::fromUtf8("m_bocSettingsListView"));

*/
        m_bocSettingsListView = new QTreeWidget(BocEditorBase);//
        m_bocSettingsListView->setColumnCount(8);
        m_bocSettingsListViewHeaders = new QTreeWidgetItem;
        m_bocSettingsListViewHeaders->setText(0,"Name");
        m_bocSettingsListView->header()->resizeSections(QHeaderView::ResizeToContents);
#if QT_VERSION < 0x050000
        m_bocSettingsListView->header()->setClickable(true);
#else
        m_bocSettingsListView->header()->setSectionsClickable(true);
#endif
        m_bocSettingsListViewHeaders->setText(1,"Link");
        m_bocSettingsListViewHeaders->setText(2,"RX");
        m_bocSettingsListViewHeaders->setText(3,"Delay");
        m_bocSettingsListViewHeaders->setText(4,"Threshol");
        m_bocSettingsListViewHeaders->setText(5,"TX");
        m_bocSettingsListViewHeaders->setText(6,"Power");
        m_bocSettingsListViewHeaders->setText(7,"Reference");
        m_bocSettingsListView->setHeaderItem(m_bocSettingsListViewHeaders);



        vboxLayout->addWidget(m_bocSettingsListView);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        m_cancelButton = new QPushButton(BocEditorBase);
        m_cancelButton->setObjectName(QString::fromUtf8("m_cancelButton"));

        hboxLayout->addWidget(m_cancelButton);

        spacer37 = new QSpacerItem(30, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacer37);

        textLabel1 = new QLabel(BocEditorBase);
        textLabel1->setObjectName(QString::fromUtf8("textLabel1"));
        textLabel1->setWordWrap(false);

        hboxLayout->addWidget(textLabel1);

        m_tagSelector = new QComboBox(BocEditorBase);//
        m_tagSelector->setObjectName(QString::fromUtf8("m_tagSelector"));
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(3), static_cast<QSizePolicy::Policy>(1));
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(m_tagSelector->sizePolicy().hasHeightForWidth());
        m_tagSelector->setSizePolicy(sizePolicy);
        m_tagSelector->setEditable(true);
        m_tagSelector->setAutoCompletion(true);

        hboxLayout->addWidget(m_tagSelector);

        m_saveTempButton = new QPushButton(BocEditorBase);
        m_saveTempButton->setObjectName(QString::fromUtf8("m_saveTempButton"));

        hboxLayout->addWidget(m_saveTempButton);

        m_savePermButton = new QPushButton(BocEditorBase);
        m_savePermButton->setObjectName(QString::fromUtf8("m_savePermButton"));

        hboxLayout->addWidget(m_savePermButton);

        m_closeButton = new QPushButton(BocEditorBase);
        m_closeButton->setObjectName(QString::fromUtf8("m_closeButton"));

        hboxLayout->addWidget(m_closeButton);


        vboxLayout->addLayout(hboxLayout);


        retranslateUi(BocEditorBase);
        QObject::connect(m_cancelButton, SIGNAL(clicked()), BocEditorBase, SLOT(removeAllChanges()));
        QObject::connect(m_saveTempButton, SIGNAL(clicked()), BocEditorBase, SLOT(saveChangesTemp()));
        QObject::connect(m_savePermButton, SIGNAL(clicked()), BocEditorBase, SLOT(saveChangesPerm()));
        QObject::connect(m_closeButton, SIGNAL(clicked()), BocEditorBase, SLOT(close()));
        QObject::connect(m_bocSettingsListView, SIGNAL(itemDoubleClicked(QTreeWidgetItem*, int)), BocEditorBase, SLOT(changeSettings(QTreeWidgetItem*,int)));

        QMetaObject::connectSlotsByName(BocEditorBase);
    } // setupUi

    void retranslateUi(QDialog *BocEditorBase)
    {
        BocEditorBase->setWindowTitle(QApplication::translate("BocEditorBase", "BOC Settings Editor", 0));
        titleLable->setText(QApplication::translate("BocEditorBase", "BOC Settings Editor", 0));
        m_onlyModifiedRodsButton->setText(QApplication::translate("BocEditorBase", "Only show modified RODs", 0));
/*        m_bocSettingsListView->header()->setLabel(0, QApplication::translate("BocEditorBase", "Name", 0));
        m_bocSettingsListView->header()->setLabel(1, QApplication::translate("BocEditorBase", "Link", 0));
        m_bocSettingsListView->header()->setLabel(2, QApplication::translate("BocEditorBase", "RX", 0));
        m_bocSettingsListView->header()->setLabel(3, QApplication::translate("BocEditorBase", "Delay", 0));
        m_bocSettingsListView->header()->setLabel(4, QApplication::translate("BocEditorBase", "Threshol", 0));
        m_bocSettingsListView->header()->setLabel(5, QApplication::translate("BocEditorBase", "TX", 0));
        m_bocSettingsListView->header()->setLabel(6, QApplication::translate("BocEditorBase", "Power", 0));
        m_bocSettingsListView->header()->setLabel(7, QApplication::translate("BocEditorBase", "Reference", 0));*/
        m_cancelButton->setText(QApplication::translate("BocEditorBase", "Clear All", 0));
        textLabel1->setText(QApplication::translate("BocEditorBase", "Tag :", 0));
        m_saveTempButton->setText(QApplication::translate("BocEditorBase", "Save Temporary", 0));
        m_savePermButton->setText(QApplication::translate("BocEditorBase", "Save Permanent", 0));
        m_closeButton->setText(QApplication::translate("BocEditorBase", "Close", 0));
    } // retranslateUi

};

namespace Ui {
    class BocEditorBase: public Ui_BocEditorBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BOCEDITORBASE_H
