#include <iostream>
#include <fstream>
#include <cstring>
#include <sstream>

#include "RodBufferView.h"

#include <QShortcut>

namespace PixCon {

  std::string toupper(const std::string &text) {
    std::string out_text;
    out_text.resize(text.size());
    for (std::string::size_type pos = 0; pos < text.size(); pos++) {
      out_text[pos]=::toupper(text[pos]);
    }
    return out_text;
  }


  RodBufferView::RodBufferView( std::string rod_name, 
				QWidget* parent)
    : QDialog(parent),
      m_rodName(rod_name)
  {
    setupUi(this);
    setWindowTitle("EOD buffer view");
    bufferListView->setColumnCount(2);
    bufferListViewHeader = new QTreeWidgetItem;
    bufferListViewHeader->setText(0,"Line");
    bufferListViewHeader->setText(1,"Rod Buffer Contents");
    bufferListView->setHeaderItem(bufferListViewHeader);
    bufferListView->header()->resizeSections(QHeaderView::ResizeToContents);
#if QT_VERSION < 0x050000
    bufferListView->header()->setClickable(true);
#else
    bufferListView->header()->setSectionsClickable(true);
#endif
    //bufferListView->sortItems(0,Qt::AscendingOrder);

    //m_bookmarkList->setSorting(-1);
    
    {
      QShortcut *up =  new QShortcut((Qt::Key_F+Qt::ALT), m_findTextEntry,SLOT(setFocus()));
      up->setEnabled(true);
      //Q3Accel *up = new Q3Accel( this );
      //up->connectItem( up->insertItem(Qt::Key_F+Qt::ALT),
               //m_findTextEntry,
               //SLOT(setFocus()) );
    }

    {
        QShortcut *up =  new QShortcut((Qt::Key_F+Qt::ALT),partitionLineEdit,SLOT(setFocus()));
        up->setEnabled(true);
      //Q3Accel *up = new Q3Accel( this );
      //up->connectItem( up->insertItem(Qt::Key_P+Qt::ALT),
               //partitionLineEdit,
               //SLOT(setFocus()) );
    }

    {
      QFontMetrics fm(m_bookmarkList->font());
      int text_width(fm.width("00000 TIMESTAMP "));
      QSize asize(m_bookmarkList->sizeHint());
      asize.setWidth(text_width);
      m_bookmarkList->resize(asize);
      //Q3ValueList<int> splitter_sizes  = m_bookmarkSplitter->sizes();
      QList<int> splitter_sizes  = m_bookmarkSplitter->sizes();
      if (splitter_sizes.size()==2) {
	splitter_sizes[1]+=splitter_sizes[0];
	if (asize.width()<splitter_sizes[1]) {
	  splitter_sizes[1]-= asize.width();
	  splitter_sizes[0] = asize.width();
	}
	else {
	  splitter_sizes[0] = 0;
	}
	m_bookmarkSplitter->setSizes(splitter_sizes);
      }
    }

    m_pattern.push_back(toupper("ERROR"));
    m_pattern.push_back(toupper("WARNING"));
    m_pattern.push_back(toupper("Timestamp"));
    chooseBuffer();
    setTitle();
  }
  
  RodBufferView::~RodBufferView() {}

  void RodBufferView::setTitle(){
    std::string title = "Rod Buffer : " + m_rodName;
    titleLable->setText(QString::fromStdString(title));
  }

  void RodBufferView::chooseBuffer(){
    //m_chooseBufferView = new ChooseBufferView(connectivity_name, this);
    //m_rodBufferView->setTitle(connectivity_name);
    //m_rodBufferView->fillListView(connectivity_name, "PixelInfr");
    //m_rodBufferView->show();
    std::string buffer_name = "buffer";
    fillListView();
  }

  void RodBufferView::fillListView(){
    //fillListView(m_bufferTypeSelector->selectedId() );
    fillListView(m_BGbufferTypeSelector->checkedId() );
  }

  void RodBufferView::fillListView(int buffer_type_id){
    std::vector <std::string> bufferTypeNames;
        bufferTypeNames.push_back("info");
        bufferTypeNames.push_back("err");
        bufferTypeNames.push_back("diag");
        bufferTypeNames.push_back("xfer");
    //static char *bufferTypeNames[]={"info","err","diag","xfer"};
    std::cout << "INFO [RodBufferView::fillListView] type id = " << buffer_type_id << std::endl;
    if (buffer_type_id<0 || buffer_type_id>=4) {
      buffer_type_id=0;
    }

    int line = -1;
    //if (bufferListView->currentItem() && buffer_type_id == m_bufferTypeSelector->selectedId()) {
    if (bufferListView->currentItem() && buffer_type_id == m_BGbufferTypeSelector->checkedId()) {
      std::string text=bufferListView->currentItem()->text(0).toLatin1().data();
      std::string::size_type pos=0;
      while (pos<text.size() && !isdigit(text[pos])) pos++;
      if (pos<text.size()) {
	line = atoi(&(text[pos]));
      }
    }


    bufferListView->clear();
    m_bookmarkList->clear();
    //get partition name
    std::string partitionName = (partitionLineEdit->text()).toLatin1().data();
    std::cout << "partitionName = " << partitionName << std::endl;

    //open rodBuffer file
    std::string rodBufferFile_str = "/daq/logs";
    char *tdl_env = getenv("TDAQ_LOGS_PATH");
    if(tdl_env!=0) rodBufferFile_str = tdl_env;
    rodBufferFile_str += "/"+partitionName+"/DSPBuffers/"+m_rodName+"/"+bufferTypeNames[buffer_type_id]+".out";
    std::cout << "rodBufferFile_str = " << rodBufferFile_str << std::endl;
    const char *rodBufferFile_char = rodBufferFile_str.c_str();
    std::ifstream rodBuffer(rodBufferFile_char);
    
    std::string rodBufferLine;
    int i=1;
    //output rodBuffer file to QListView
    if ( rodBuffer.is_open() ) {
      std::cout << "Rod Buffer is open." << std::endl;
     // Q3ListViewItem *after = NULL;
      //Q3ListViewItem *last_bookmark = NULL;
      QTreeWidgetItem *after = NULL;
      QTreeWidgetItem *last_bookmark = NULL;

      while ( ! rodBuffer.eof() ){
	std::getline (rodBuffer, rodBufferLine);
	std::ostringstream line_number_strm;
	line_number_strm.str("");
	line_number_strm << i;
   
	//Q3ListViewItem *item = new Q3ListViewItem(bufferListView);
	QTreeWidgetItem *item = new QTreeWidgetItem(bufferListView);
	item->setText(0, QString::fromStdString(line_number_strm.str()) );
	item->setText(1, QString::fromStdString(rodBufferLine));


	for (std::string::size_type pos = 0; pos < rodBufferLine.size(); pos++) {
	    rodBufferLine[pos]=::toupper(rodBufferLine[pos]);
	}

	for(std::vector<std::string>::const_iterator pattern_iter=m_pattern.begin();
	    pattern_iter != m_pattern.end();
	    ++pattern_iter) {
	  if (rodBufferLine.find(*pattern_iter) != std::string::npos) {
	    std::cout << "INFO [RodBufferView::find] found " << *pattern_iter << "  " << line_number_strm.str() << " : " <<  item->text(1).toLatin1().data() << "." << std::endl;      
	    
	    //Q3ListViewItem *bookmark = new Q3ListViewItem(m_bookmarkList, last_bookmark);
	    QTreeWidgetItem *bookmark = new QTreeWidgetItem(m_bookmarkList, last_bookmark);
	    bookmark->setText(0,QString::fromStdString(line_number_strm.str())+QString::fromStdString(" ")+QString::fromStdString(*pattern_iter));
	    last_bookmark = bookmark;
	  }
	}

	//insert current item after last item
	if (after){
	  int indx=-1;
	  if(after->parent()!=0){
	    indx = after->parent()->indexOfChild(after);
	    after->parent()->insertChild(indx+1,item);
	  } else {
	    indx = bufferListView->indexOfTopLevelItem(after);
	    bufferListView->insertTopLevelItem(indx+1,item);
	  }
      //item->moveItem(after);
	}
	after = item;
	i++;  
      }
      selectLine(line);
    } else {
      std::cout << "[ERROR] Unable to open Rod Buffer file for " << m_rodName << "in partition " << partitionName << std::endl;
    }
  }

  void RodBufferView::find() {
    std::cout << "INFO [RodBufferView::find] search for " << m_findTextEntry->text().toLatin1().data() << "." << std::endl;
    //Q3ListViewItem *last_current = bufferListView->currentItem();
    QTreeWidgetItem *last_current = bufferListView->currentItem();
    if (last_current) {
        int indx = last_current->parent()->indexOfChild(last_current);
        int chld_count = last_current->parent()->childCount();
      //if (bufferListView->currentItem()->nextSibling()) {
          if (indx +1 < chld_count) {
    //bufferListView->setCurrentItem(bufferListView->currentItem()->nextSibling());
              bufferListView->setCurrentItem(last_current->parent()->child(indx+1));
      }
      else {
    bufferListView->setCurrentItem(bufferListView->takeTopLevelItem(0));
      }
    }
    const QString helperQStr = m_findTextEntry->text();
    QList <QTreeWidgetItem *> helper_item = bufferListView->findItems(helperQStr,Qt::MatchCaseSensitive,1);//,Qt::Contains);
    if (!helper_item.isEmpty()){
        QTreeWidgetItem *current_item = helper_item.takeFirst();
    if (current_item) {
      std::cout << "INFO [RodBufferView::find] found " << current_item->text(0).toLatin1().data() << " : " <<  current_item->text(1).toLatin1().data() << "." << std::endl;      
      bufferListView->setCurrentItem(current_item);
      //bufferListView->ensureItemVisible(current_item);
       bufferListView->scrollToItem(current_item, QAbstractItemView::EnsureVisible);
    }
  }
    else {
      bufferListView->setCurrentItem(last_current);
    }
  }

  void RodBufferView::setCursor(/*Q3ListViewItem*/QTreeWidgetItem *item) {
    if (item) {
      bufferListView->setCurrentItem(item);
    }
  }

  void RodBufferView::selectLine(/*Q3ListViewItem*/QTreeWidgetItem *item)
  {
    if (item) {
      int line = -1;
      std::string text=item->text(0).toLatin1().data();
      std::string::size_type pos=0;
      while (pos<text.size() && !isdigit(text[pos])) pos++;
      if (pos<text.size()) {
	line = atoi(&(text[pos]));
	selectLine(line);
      }
    }
  }

  void RodBufferView::selectLine(int line)
  {
    if (line>=1) {
      line--;
      QTreeWidgetItemIterator itr(bufferListView);
      //Q3ListViewItem *child=bufferListView->firstChild();
      while (line>0 && (*itr)/*child->nextSibling()*/) {
    //child = child->nextSibling();
          ++itr;
	line--;
      }
      if (line==0 && (*itr)) {
    bufferListView->setCurrentItem((*itr));
    //bufferListView->ensureItemVisible(child);
    bufferListView->scrollToItem((*itr),QAbstractItemView::EnsureVisible);//QT3 used only set visible.
      }
    }
  }
  
  void RodBufferView::changeBufferType(int buffer_type_id) {
    fillListView(buffer_type_id);
  }
  

}
