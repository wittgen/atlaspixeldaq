#ifndef _PixCon_ConnItemCollection_h_
#define _PixCon_ConnItemCollection_h_

#include <vector>
#include <cassert>
#include <ConnItemBase.h>

namespace PixCon {

  template <class T>
  class ConnItemCollection : protected std::vector<T>
  {
  public:
    ConnItemCollection()  { std::vector<T>::resize(kNHistogramConnItemTypes); }

    T &operator[](unsigned int index)  { assert(index < kNHistogramConnItemTypes ); return std::vector<T>::operator[](index); }
    const T &operator[](unsigned int index) const  { assert(index < kNHistogramConnItemTypes); return std::vector<T>::operator[](index); }

    typedef typename std::vector<T>::iterator             iterator;
    typedef typename std::vector<T>::const_iterator const_iterator;

    iterator  begin()             { return std::vector<T>::begin(); }
    iterator  end()             { return std::vector<T>::begin(); }
    const_iterator begin() const  { return std::vector<T>::begin(); }
    const_iterator end()   const  { return std::vector<T>::end(); }
  };

}
#endif
