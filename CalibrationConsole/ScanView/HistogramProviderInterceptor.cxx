#include "HistogramProviderInterceptor.h"
#include <iostream>
#include <cassert>

namespace PixCon {


  HistogramProviderInterceptor::HistogramProviderInterceptor(IHistogramProvider *a_server) : m_server(a_server) { assert(m_server.get()); }
  HistogramProviderInterceptor::~HistogramProviderInterceptor() {
  }

  HistoPtr_t HistogramProviderInterceptor::loadHistogram(EMeasurementType type,
						       SerialNumber_t serial_number,
						       EConnItemType conn_item_type,
						       const std::string &connectivity_name,
						       const std::string &histo_name,
						       const Index_t &index)  {

    HistoPtr_t  ret = m_server->loadHistogram(type, serial_number, conn_item_type, connectivity_name, histo_name, index);

    std::cout << "INFO [HistogramProviderInterceptor::loadHistogram] : ";
    for (Index_t::const_iterator iter = index.begin(); iter != index.end(); iter++) {
      if (iter != index.begin()) std::cout << ", ";
      std::cout << *iter;
    }
    std::cout << " : " << static_cast<void *>(ret) << "." << std::endl;

    return ret;
  }
  
  void HistogramProviderInterceptor::requestHistogramList(EMeasurementType type,
							SerialNumber_t serial_number,
							EConnItemType conn_item_type,
							std::vector< std::string > &histogram_name_list )  {

    m_server->requestHistogramList(type, serial_number, conn_item_type, histogram_name_list);

    std::cout << "INFO [HistogramProviderInterceptor::requestHistogramList] entry : type =" << type << ", serial_number = " << serial_number
	      << ", item_type = " << conn_item_type << " : ";

    for (std::vector<std::string>::const_iterator iter = histogram_name_list.begin();
	 iter != histogram_name_list.end();
	 iter++) {
      if (iter != histogram_name_list. begin()) {
	std::cout << ", ";
      }
      std::cout << *iter;
    }
    std::cout << std::endl;

  }

  void HistogramProviderInterceptor::requestHistogramList(EMeasurementType type,
							  SerialNumber_t serial_number,
							  EConnItemType conn_item_type,
							  std::vector< std::string > &histogram_name_list,
							  const std::string &rod_name)  {


    m_server->requestHistogramList(type, serial_number, conn_item_type, histogram_name_list, rod_name);

    std::cout << "INFO [HistogramProviderInterceptor::requestHistogramList] entry : type =" << type << ", serial_number = " << serial_number
	      << ", item_type = " << conn_item_type << ", ROD = " << rod_name << " : ";
    for (std::vector<std::string>::const_iterator iter = histogram_name_list.begin();
	 iter != histogram_name_list.end();
	 iter++) {
      if (iter != histogram_name_list. begin()) {
	std::cout << ", ";
      }
      std::cout << *iter;
    }
    std::cout << std::endl;

  }
  
  void HistogramProviderInterceptor::numberOfHistograms(EMeasurementType type,
						      SerialNumber_t serial_number,
						      EConnItemType conn_item_type,
						      const std::string &connectivity_name,
						      const std::string &histo_name,
						      HistoInfo_t &index) {
    m_server->numberOfHistograms(type, serial_number, conn_item_type, connectivity_name, histo_name, index);

    std::cout << "INFO [HistogramProviderInterceptor::numberOfHistograms] entry : type =" << type << ", serial_number = " << serial_number
	      << ", item_type = " << conn_item_type << ",  name = " << connectivity_name
	      << ", histo_name = " << histo_name << " : ";
    for (unsigned int i=0; i<HistoInfo_t::s_nMaxLevels-1; i++) {
      if (i>0) {
	std::cout << ", ";
      }
      std::cout << index.maxIndex(i);
    }
    std::cout << " / " << index.minHistoIndex() << " - " << index.maxHistoIndex()
	      << std::endl;


  }

  void HistogramProviderInterceptor::clearCache(EMeasurementType measurement_type, SerialNumber_t serial_number) {

    std::cout << "INFO [HistogramProviderInterceptor::clearCache] clear data for  type =" << measurement_type << ", serial_number = " << serial_number
	      << "." << std::endl;

    m_server->clearCache(measurement_type, serial_number);
  }

  void HistogramProviderInterceptor::reset() {
    std::cout << "INFO [HistogramProviderInterceptor::reset] ." << std::endl;
    m_server->reset();
  }
}
