#ifndef UI_CHANGEBOCSETTINGSBASE_H
#define UI_CHANGEBOCSETTINGSBASE_H

#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QDialog>
#include <QFrame>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QSpacerItem>
#include <QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ChangeBocSettingsBase
{
public:
    QVBoxLayout *vboxLayout;
    QLabel *titleLable;
    QFrame *m_frameDTO1;//
    QVBoxLayout *vboxLayout1;
    QHBoxLayout *hboxLayout;
    QLabel *textLabel2;
    QLabel *m_rxPluginName;
    QGridLayout *gridLayout;
    QLabel *textLabel4;
    QLabel *textLabel4_2;
    QLabel *m_oldThresholdLabel;
    QLabel *textLabel6;
    QLineEdit *m_delayEdit;
    QLabel *m_oldDelayLabel;
    QLabel *textLabel6_2;
    QLineEdit *m_thresholdEdit;
    QFrame *m_frameDTO2;//
    QVBoxLayout *vboxLayout2;
    QHBoxLayout *hboxLayout1;
    QLabel *textLabel2_2;
    QLabel *m_rxPluginName_2;
    QGridLayout *gridLayout1;
    QLabel *textLabel4_3;
    QLabel *textLabel4_2_2;
    QLabel *m_oldThresholdLabel_2;
    QLabel *textLabel6_3;
    QLineEdit *m_delayEdit_2;
    QLabel *m_oldDelayLabel_2;
    QLabel *textLabel6_2_2;
    QLineEdit *m_thresholdEdit_2;
    QFrame *m_frameTx;
    QVBoxLayout *vboxLayout3;
    QLabel *m_txPluginName;
    QHBoxLayout *hboxLayout2;
    QLabel *textLabel4_3_2;
    QLabel *m_oldPowerLabel;
    QLabel *textLabel6_3_2;
    QLineEdit *m_powerEdit;
    QFrame *line3;
    QHBoxLayout *hboxLayout3;
    QSpacerItem *spacer39;
    QPushButton *m_cancel;
    QSpacerItem *spacer40;
    QPushButton *m_applyButton;
    QSpacerItem *spacer41;

    void setupUi(QDialog *ChangeBocSettingsBase)
    {
        if (ChangeBocSettingsBase->objectName().isEmpty())
            ChangeBocSettingsBase->setObjectName(QString::fromUtf8("ChangeBocSettingsBase"));
        ChangeBocSettingsBase->resize(309, 371);
        vboxLayout = new QVBoxLayout(ChangeBocSettingsBase);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        titleLable = new QLabel(ChangeBocSettingsBase);
        titleLable->setObjectName(QString::fromUtf8("titleLable"));
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(1), static_cast<QSizePolicy::Policy>(1));
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(titleLable->sizePolicy().hasHeightForWidth());
        titleLable->setSizePolicy(sizePolicy);
        QFont font;
        font.setPointSize(14);
        font.setBold(true);
        titleLable->setFont(font);
        titleLable->setIndent(16);
        titleLable->setWordWrap(false);

        vboxLayout->addWidget(titleLable);

        m_frameDTO1 = new QFrame(ChangeBocSettingsBase);
        m_frameDTO1->setObjectName(QString::fromUtf8("m_frameDTO1"));
        m_frameDTO1->setFrameShape(QFrame::NoFrame);
        m_frameDTO1->setFrameShadow(QFrame::Raised);
        m_frameDTO1->setLineWidth(0);
        vboxLayout1 = new QVBoxLayout(m_frameDTO1);
        vboxLayout1->setSpacing(6);
        vboxLayout1->setContentsMargins(11, 11, 11, 11);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        textLabel2 = new QLabel(m_frameDTO1);
        textLabel2->setObjectName(QString::fromUtf8("textLabel2"));
        textLabel2->setWordWrap(false);

        hboxLayout->addWidget(textLabel2);

        m_rxPluginName = new QLabel(m_frameDTO1);
        m_rxPluginName->setObjectName(QString::fromUtf8("m_rxPluginName"));
        m_rxPluginName->setWordWrap(false);

        hboxLayout->addWidget(m_rxPluginName);


        vboxLayout1->addLayout(hboxLayout);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        textLabel4 = new QLabel(m_frameDTO1);
        textLabel4->setObjectName(QString::fromUtf8("textLabel4"));
        textLabel4->setWordWrap(false);

        gridLayout->addWidget(textLabel4, 0, 0, 1, 1);

        textLabel4_2 = new QLabel(m_frameDTO1);
        textLabel4_2->setObjectName(QString::fromUtf8("textLabel4_2"));
        textLabel4_2->setWordWrap(false);

        gridLayout->addWidget(textLabel4_2, 1, 0, 1, 1);

        m_oldThresholdLabel = new QLabel(m_frameDTO1);
        m_oldThresholdLabel->setObjectName(QString::fromUtf8("m_oldThresholdLabel"));
        m_oldThresholdLabel->setWordWrap(false);

        gridLayout->addWidget(m_oldThresholdLabel, 0, 1, 1, 1);

        textLabel6 = new QLabel(m_frameDTO1);
        textLabel6->setObjectName(QString::fromUtf8("textLabel6"));
        textLabel6->setWordWrap(false);

        gridLayout->addWidget(textLabel6, 0, 2, 1, 1);

        m_delayEdit = new QLineEdit(m_frameDTO1);
        m_delayEdit->setObjectName(QString::fromUtf8("m_delayEdit"));

        gridLayout->addWidget(m_delayEdit, 1, 3, 1, 1);

        m_oldDelayLabel = new QLabel(m_frameDTO1);
        m_oldDelayLabel->setObjectName(QString::fromUtf8("m_oldDelayLabel"));
        m_oldDelayLabel->setWordWrap(false);

        gridLayout->addWidget(m_oldDelayLabel, 1, 1, 1, 1);

        textLabel6_2 = new QLabel(m_frameDTO1);
        textLabel6_2->setObjectName(QString::fromUtf8("textLabel6_2"));
        textLabel6_2->setWordWrap(false);

        gridLayout->addWidget(textLabel6_2, 1, 2, 1, 1);

        m_thresholdEdit = new QLineEdit(m_frameDTO1);
        m_thresholdEdit->setObjectName(QString::fromUtf8("m_thresholdEdit"));

        gridLayout->addWidget(m_thresholdEdit, 0, 3, 1, 1);


        vboxLayout1->addLayout(gridLayout);


        vboxLayout->addWidget(m_frameDTO1);

        m_frameDTO2 = new QFrame(ChangeBocSettingsBase);//
        m_frameDTO2->setObjectName(QString::fromUtf8("m_frameDTO2"));
        m_frameDTO2->setFrameShape(QFrame::NoFrame);
        m_frameDTO2->setFrameShadow(QFrame::Raised);
        m_frameDTO2->setLineWidth(0);
        vboxLayout2 = new QVBoxLayout(m_frameDTO2);
        vboxLayout2->setSpacing(6);
        vboxLayout2->setContentsMargins(11, 11, 11, 11);
        vboxLayout2->setObjectName(QString::fromUtf8("vboxLayout2"));
        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setSpacing(6);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        textLabel2_2 = new QLabel(m_frameDTO2);
        textLabel2_2->setObjectName(QString::fromUtf8("textLabel2_2"));
        textLabel2_2->setWordWrap(false);

        hboxLayout1->addWidget(textLabel2_2);

        m_rxPluginName_2 = new QLabel(m_frameDTO2);
        m_rxPluginName_2->setObjectName(QString::fromUtf8("m_rxPluginName_2"));
        m_rxPluginName_2->setWordWrap(false);

        hboxLayout1->addWidget(m_rxPluginName_2);


        vboxLayout2->addLayout(hboxLayout1);

        gridLayout1 = new QGridLayout();
        gridLayout1->setSpacing(6);
        gridLayout1->setObjectName(QString::fromUtf8("gridLayout1"));
        textLabel4_3 = new QLabel(m_frameDTO2);
        textLabel4_3->setObjectName(QString::fromUtf8("textLabel4_3"));
        textLabel4_3->setWordWrap(false);

        gridLayout1->addWidget(textLabel4_3, 0, 0, 1, 1);

        textLabel4_2_2 = new QLabel(m_frameDTO2);
        textLabel4_2_2->setObjectName(QString::fromUtf8("textLabel4_2_2"));
        textLabel4_2_2->setWordWrap(false);

        gridLayout1->addWidget(textLabel4_2_2, 1, 0, 1, 1);

        m_oldThresholdLabel_2 = new QLabel(m_frameDTO2);
        m_oldThresholdLabel_2->setObjectName(QString::fromUtf8("m_oldThresholdLabel_2"));
        m_oldThresholdLabel_2->setWordWrap(false);

        gridLayout1->addWidget(m_oldThresholdLabel_2, 0, 1, 1, 1);

        textLabel6_3 = new QLabel(m_frameDTO2);
        textLabel6_3->setObjectName(QString::fromUtf8("textLabel6_3"));
        textLabel6_3->setWordWrap(false);

        gridLayout1->addWidget(textLabel6_3, 0, 2, 1, 1);

        m_delayEdit_2 = new QLineEdit(m_frameDTO2);
        m_delayEdit_2->setObjectName(QString::fromUtf8("m_delayEdit_2"));

        gridLayout1->addWidget(m_delayEdit_2, 1, 3, 1, 1);

        m_oldDelayLabel_2 = new QLabel(m_frameDTO2);
        m_oldDelayLabel_2->setObjectName(QString::fromUtf8("m_oldDelayLabel_2"));
        m_oldDelayLabel_2->setWordWrap(false);

        gridLayout1->addWidget(m_oldDelayLabel_2, 1, 1, 1, 1);

        textLabel6_2_2 = new QLabel(m_frameDTO2);
        textLabel6_2_2->setObjectName(QString::fromUtf8("textLabel6_2_2"));
        textLabel6_2_2->setWordWrap(false);

        gridLayout1->addWidget(textLabel6_2_2, 1, 2, 1, 1);

        m_thresholdEdit_2 = new QLineEdit(m_frameDTO2);
        m_thresholdEdit_2->setObjectName(QString::fromUtf8("m_thresholdEdit_2"));

        gridLayout1->addWidget(m_thresholdEdit_2, 0, 3, 1, 1);


        vboxLayout2->addLayout(gridLayout1);


        vboxLayout->addWidget(m_frameDTO2);

        m_frameTx = new QFrame(ChangeBocSettingsBase);//
        m_frameTx->setObjectName(QString::fromUtf8("m_frameTx"));
        m_frameTx->setFrameShape(QFrame::NoFrame);
        m_frameTx->setFrameShadow(QFrame::Raised);
        m_frameTx->setLineWidth(0);
        vboxLayout3 = new QVBoxLayout(m_frameTx);
        vboxLayout3->setSpacing(6);
        vboxLayout3->setContentsMargins(11, 11, 11, 11);
        vboxLayout3->setObjectName(QString::fromUtf8("vboxLayout3"));
        m_txPluginName = new QLabel(m_frameTx);
        m_txPluginName->setObjectName(QString::fromUtf8("m_txPluginName"));
        m_txPluginName->setWordWrap(false);

        vboxLayout3->addWidget(m_txPluginName);

        hboxLayout2 = new QHBoxLayout();
        hboxLayout2->setSpacing(6);
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        textLabel4_3_2 = new QLabel(m_frameTx);
        textLabel4_3_2->setObjectName(QString::fromUtf8("textLabel4_3_2"));
        textLabel4_3_2->setWordWrap(false);

        hboxLayout2->addWidget(textLabel4_3_2);

        m_oldPowerLabel = new QLabel(m_frameTx);
        m_oldPowerLabel->setObjectName(QString::fromUtf8("m_oldPowerLabel"));
        m_oldPowerLabel->setWordWrap(false);

        hboxLayout2->addWidget(m_oldPowerLabel);

        textLabel6_3_2 = new QLabel(m_frameTx);
        textLabel6_3_2->setObjectName(QString::fromUtf8("textLabel6_3_2"));
        textLabel6_3_2->setWordWrap(false);

        hboxLayout2->addWidget(textLabel6_3_2);

        m_powerEdit = new QLineEdit(m_frameTx);
        m_powerEdit->setObjectName(QString::fromUtf8("m_powerEdit"));

        hboxLayout2->addWidget(m_powerEdit);


        vboxLayout3->addLayout(hboxLayout2);


        vboxLayout->addWidget(m_frameTx);

        line3 = new QFrame(ChangeBocSettingsBase);
        line3->setObjectName(QString::fromUtf8("line3"));
        line3->setFrameShape(QFrame::HLine);
        line3->setFrameShadow(QFrame::Sunken);

        vboxLayout->addWidget(line3);

        hboxLayout3 = new QHBoxLayout();
        hboxLayout3->setSpacing(6);
        hboxLayout3->setObjectName(QString::fromUtf8("hboxLayout3"));
        spacer39 = new QSpacerItem(16, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout3->addItem(spacer39);

        m_cancel = new QPushButton(ChangeBocSettingsBase);
        m_cancel->setObjectName(QString::fromUtf8("m_cancel"));

        hboxLayout3->addWidget(m_cancel);

        spacer40 = new QSpacerItem(20, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout3->addItem(spacer40);

        m_applyButton = new QPushButton(ChangeBocSettingsBase);
        m_applyButton->setObjectName(QString::fromUtf8("m_applyButton"));

        hboxLayout3->addWidget(m_applyButton);

        spacer41 = new QSpacerItem(20, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout3->addItem(spacer41);


        vboxLayout->addLayout(hboxLayout3);


        retranslateUi(ChangeBocSettingsBase);
        QObject::connect(m_cancel, SIGNAL(clicked()), ChangeBocSettingsBase, SLOT(reject()));
        QObject::connect(m_applyButton, SIGNAL(clicked()), ChangeBocSettingsBase, SLOT(accept()));

        QMetaObject::connectSlotsByName(ChangeBocSettingsBase);
    } // setupUi

    void retranslateUi(QDialog *ChangeBocSettingsBase)
    {
        ChangeBocSettingsBase->setWindowTitle(QApplication::translate("ChangeBocSettingsBase", "Change BOC Settings", 0));
        titleLable->setText(QApplication::translate("ChangeBocSettingsBase", "<name>", 0));
        textLabel2->setText(QApplication::translate("ChangeBocSettingsBase", "DTO1 :", 0));
        m_rxPluginName->setText(QApplication::translate("ChangeBocSettingsBase", "<plugin>", 0));
        textLabel4->setText(QApplication::translate("ChangeBocSettingsBase", "Threshold :", 0));
        textLabel4_2->setText(QApplication::translate("ChangeBocSettingsBase", "Delay :", 0));
        m_oldThresholdLabel->setText(QApplication::translate("ChangeBocSettingsBase", "<old>", 0));
        textLabel6->setText(QApplication::translate("ChangeBocSettingsBase", "->", 0));
        m_oldDelayLabel->setText(QApplication::translate("ChangeBocSettingsBase", "<old>", 0));
        textLabel6_2->setText(QApplication::translate("ChangeBocSettingsBase", "->", 0));
        textLabel2_2->setText(QApplication::translate("ChangeBocSettingsBase", "DTO2 :", 0));
        m_rxPluginName_2->setText(QApplication::translate("ChangeBocSettingsBase", "<plugin>", 0));
        textLabel4_3->setText(QApplication::translate("ChangeBocSettingsBase", "Threshold :", 0));
        textLabel4_2_2->setText(QApplication::translate("ChangeBocSettingsBase", "Delay :", 0));
        m_oldThresholdLabel_2->setText(QApplication::translate("ChangeBocSettingsBase", "<old>", 0));
        textLabel6_3->setText(QApplication::translate("ChangeBocSettingsBase", "->", 0));
        m_oldDelayLabel_2->setText(QApplication::translate("ChangeBocSettingsBase", "<old>", 0));
        textLabel6_2_2->setText(QApplication::translate("ChangeBocSettingsBase", "->", 0));
        m_txPluginName->setText(QApplication::translate("ChangeBocSettingsBase", "<plugin>", 0));
        textLabel4_3_2->setText(QApplication::translate("ChangeBocSettingsBase", "Power :", 0));
        m_oldPowerLabel->setText(QApplication::translate("ChangeBocSettingsBase", "<old>", 0));
        textLabel6_3_2->setText(QApplication::translate("ChangeBocSettingsBase", "->", 0));
        m_cancel->setText(QApplication::translate("ChangeBocSettingsBase", "Cancel", 0));
        m_applyButton->setText(QApplication::translate("ChangeBocSettingsBase", "Apply", 0));
    } // retranslateUi

};

namespace Ui {
    class ChangeBocSettingsBase: public Ui_ChangeBocSettingsBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CHANGEBOCSETTINGSBASE_H
