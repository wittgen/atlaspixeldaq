#ifndef UI_SCANPANELBASE_H
#define UI_SCANPANELBASE_H

#include <QGroupBox>
#include <QProgressBar>
#include <QTextEdit>
#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QComboBox>
#include <QFrame>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QSpacerItem>
#include <QVBoxLayout>
#include <QWidget>

QT_BEGIN_NAMESPACE

class Ui_ScanPanelBase
{
public:
    QVBoxLayout *vboxLayout;
    QGroupBox *groupBox4;//
    QGridLayout *gridLayout;
    QComboBox *m_scanTypeSelector;
    QLabel *textLabel1_3_3;
    QComboBox *m_scanConfigTagSelector;
    QLabel *textLabel1_3;
    QSpacerItem *spacer19;
    QLabel *textLabel1_3_2;
    QLineEdit *m_scanConfigRevisionEdit;
    QSpacerItem *spacer16;
    QPushButton *m_scanSettingsButton;
    QSpacerItem *spacer17;
    QPushButton *m_createScanTypeButton;
    QSpacerItem *spacer20;
    QSpacerItem *spacer14;
    QSpacerItem *spacer36;
    QGroupBox *groupBox5;//
    QGridLayout *gridLayout1;
    QLabel *textLabel2;
    QSpacerItem *spacer21;
    QLineEdit *m_scanStatusDisplay;
    QProgressBar *m_scanProgressDisplay;//
    QLabel *m_serialNumberDisplay;
    QLabel *textLabel2_2_2;
    QLabel *textLabel2_2;
    QGroupBox *groupBox6;//
    QVBoxLayout *vboxLayout1;
    QHBoxLayout *hboxLayout;
    QVBoxLayout *vboxLayout2;
    QLabel *textLabel2_2_2_2;
    QSpacerItem *spacer27;
    QTextEdit *m_scanCommentEdit;//
    QHBoxLayout *hboxLayout1;
    QLabel *textLabel2_2_2_2_2;
    QComboBox *m_scanQualitySelector;
    QSpacerItem *spacer26;
    QFrame *line1;
    QHBoxLayout *hboxLayout2;
    QSpacerItem *spacer28;
    QPushButton *m_scanMetaInfoReject;
    QSpacerItem *spacer29;
    QPushButton *m_scanMetaInfoSave;
    QSpacerItem *spacer30;
    QSpacerItem *spacer45;
    QHBoxLayout *hboxLayout3;
    QSpacerItem *spacer23;
    QPushButton *m_scanStartButton;
    QSpacerItem *spacer24;
    QSpacerItem *spacer46;

    void setupUi(QWidget *ScanPanelBase)
    {
        if (ScanPanelBase->objectName().isEmpty())
            ScanPanelBase->setObjectName(QString::fromUtf8("ScanPanelBase"));
        ScanPanelBase->resize(600, 526);
        vboxLayout = new QVBoxLayout(ScanPanelBase);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        groupBox4 = new QGroupBox(ScanPanelBase);//
        groupBox4->setObjectName(QString::fromUtf8("groupBox4"));
        groupBox4->setColumnLayout(0, Qt::Vertical);
        groupBox4->layout()->setSpacing(6);
        groupBox4->layout()->setContentsMargins(11, 11, 11, 11);
        gridLayout = new QGridLayout();
        QBoxLayout *boxlayout = qobject_cast<QBoxLayout *>(groupBox4->layout());
        if (boxlayout)
            boxlayout->addLayout(gridLayout);
        gridLayout->setAlignment(Qt::AlignTop);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        m_scanTypeSelector = new QComboBox(groupBox4);
        m_scanTypeSelector->setObjectName(QString::fromUtf8("m_scanTypeSelector"));

        gridLayout->addWidget(m_scanTypeSelector, 0, 1, 1, 1);

        textLabel1_3_3 = new QLabel(groupBox4);
        textLabel1_3_3->setObjectName(QString::fromUtf8("textLabel1_3_3"));
        textLabel1_3_3->setWordWrap(false);

        gridLayout->addWidget(textLabel1_3_3, 0, 0, 1, 1);

        m_scanConfigTagSelector = new QComboBox(groupBox4);
        m_scanConfigTagSelector->setObjectName(QString::fromUtf8("m_scanConfigTagSelector"));

        gridLayout->addWidget(m_scanConfigTagSelector, 1, 1, 1, 1);

        textLabel1_3 = new QLabel(groupBox4);
        textLabel1_3->setObjectName(QString::fromUtf8("textLabel1_3"));
        textLabel1_3->setWordWrap(false);

        gridLayout->addWidget(textLabel1_3, 1, 0, 1, 1);

        spacer19 = new QSpacerItem(41, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(spacer19, 1, 4, 1, 1);

        textLabel1_3_2 = new QLabel(groupBox4);
        textLabel1_3_2->setObjectName(QString::fromUtf8("textLabel1_3_2"));
        textLabel1_3_2->setWordWrap(false);

        gridLayout->addWidget(textLabel1_3_2, 1, 2, 1, 1);

        m_scanConfigRevisionEdit = new QLineEdit(groupBox4);
        m_scanConfigRevisionEdit->setObjectName(QString::fromUtf8("m_scanConfigRevisionEdit"));

        gridLayout->addWidget(m_scanConfigRevisionEdit, 1, 3, 1, 1);

        spacer16 = new QSpacerItem(100, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(spacer16, 2, 0, 1, 1);

        m_scanSettingsButton = new QPushButton(groupBox4);
        m_scanSettingsButton->setObjectName(QString::fromUtf8("m_scanSettingsButton"));
        const QIcon icon = QPixmap(":/icons/images/configure.png");
        m_scanSettingsButton->setIcon(icon);

        gridLayout->addWidget(m_scanSettingsButton, 2, 1, 1, 1);

        spacer17 = new QSpacerItem(240, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(spacer17, 2, 2, 1, 3);

        m_createScanTypeButton = new QPushButton(groupBox4);
        m_createScanTypeButton->setObjectName(QString::fromUtf8("m_createScanTypeButton"));

        gridLayout->addWidget(m_createScanTypeButton, 0, 3, 1, 1);

        spacer20 = new QSpacerItem(50, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(spacer20, 0, 4, 1, 1);

        spacer14 = new QSpacerItem(50, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(spacer14, 0, 2, 1, 1);


        vboxLayout->addWidget(groupBox4);

        spacer36 = new QSpacerItem(20, 14, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout->addItem(spacer36);

        groupBox5 = new QGroupBox(ScanPanelBase);//
        groupBox5->setObjectName(QString::fromUtf8("groupBox5"));
        groupBox5->setColumnLayout(0, Qt::Vertical);
        groupBox5->layout()->setSpacing(6);
        groupBox5->layout()->setContentsMargins(11, 11, 11, 11);
        gridLayout1 = new QGridLayout();
        QBoxLayout *boxlayout1 = qobject_cast<QBoxLayout *>(groupBox5->layout());
        if (boxlayout1)
            boxlayout1->addLayout(gridLayout1);
        gridLayout1->setAlignment(Qt::AlignTop);
        gridLayout1->setObjectName(QString::fromUtf8("gridLayout1"));
        textLabel2 = new QLabel(groupBox5);
        textLabel2->setObjectName(QString::fromUtf8("textLabel2"));
        textLabel2->setWordWrap(false);

        gridLayout1->addWidget(textLabel2, 0, 0, 1, 1);

        spacer21 = new QSpacerItem(240, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout1->addItem(spacer21, 0, 2, 1, 1);

        m_scanStatusDisplay = new QLineEdit(groupBox5);
        m_scanStatusDisplay->setObjectName(QString::fromUtf8("m_scanStatusDisplay"));
        m_scanStatusDisplay->setDragEnabled(false);
        m_scanStatusDisplay->setReadOnly(true);

        gridLayout1->addWidget(m_scanStatusDisplay, 2, 1, 1, 2);

        m_scanProgressDisplay = new QProgressBar(groupBox5);//
        m_scanProgressDisplay->setObjectName(QString::fromUtf8("m_scanProgressDisplay"));

        gridLayout1->addWidget(m_scanProgressDisplay, 1, 1, 1, 2);

        m_serialNumberDisplay = new QLabel(groupBox5);
        m_serialNumberDisplay->setObjectName(QString::fromUtf8("m_serialNumberDisplay"));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        m_serialNumberDisplay->setFont(font);
        m_serialNumberDisplay->setWordWrap(false);

        gridLayout1->addWidget(m_serialNumberDisplay, 0, 1, 1, 1);

        textLabel2_2_2 = new QLabel(groupBox5);
        textLabel2_2_2->setObjectName(QString::fromUtf8("textLabel2_2_2"));
        textLabel2_2_2->setWordWrap(false);

        gridLayout1->addWidget(textLabel2_2_2, 2, 0, 1, 1);

        textLabel2_2 = new QLabel(groupBox5);
        textLabel2_2->setObjectName(QString::fromUtf8("textLabel2_2"));
        textLabel2_2->setWordWrap(false);

        gridLayout1->addWidget(textLabel2_2, 1, 0, 1, 1);


        vboxLayout->addWidget(groupBox5);

        groupBox6 = new QGroupBox(ScanPanelBase);//
        groupBox6->setObjectName(QString::fromUtf8("groupBox6"));
        groupBox6->setColumnLayout(0, Qt::Vertical);
        groupBox6->layout()->setSpacing(6);
        groupBox6->layout()->setContentsMargins(11, 11, 11, 11);
        vboxLayout1 = new QVBoxLayout();
        QBoxLayout *boxlayout2 = qobject_cast<QBoxLayout *>(groupBox6->layout());
        if (boxlayout2)
            boxlayout2->addLayout(vboxLayout1);
        vboxLayout1->setAlignment(Qt::AlignTop);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        vboxLayout2 = new QVBoxLayout();
        vboxLayout2->setSpacing(6);
        vboxLayout2->setObjectName(QString::fromUtf8("vboxLayout2"));
        textLabel2_2_2_2 = new QLabel(groupBox6);
        textLabel2_2_2_2->setObjectName(QString::fromUtf8("textLabel2_2_2_2"));
        textLabel2_2_2_2->setWordWrap(false);

        vboxLayout2->addWidget(textLabel2_2_2_2);

        spacer27 = new QSpacerItem(21, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout2->addItem(spacer27);


        hboxLayout->addLayout(vboxLayout2);

        m_scanCommentEdit = new QTextEdit(groupBox6);
        m_scanCommentEdit->setObjectName(QString::fromUtf8("m_scanCommentEdit"));

        hboxLayout->addWidget(m_scanCommentEdit);


        vboxLayout1->addLayout(hboxLayout);

        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setSpacing(6);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        textLabel2_2_2_2_2 = new QLabel(groupBox6);
        textLabel2_2_2_2_2->setObjectName(QString::fromUtf8("textLabel2_2_2_2_2"));
        textLabel2_2_2_2_2->setWordWrap(false);

        hboxLayout1->addWidget(textLabel2_2_2_2_2);

        m_scanQualitySelector = new QComboBox(groupBox6);
        m_scanQualitySelector->setObjectName(QString::fromUtf8("m_scanQualitySelector"));

        hboxLayout1->addWidget(m_scanQualitySelector);

        spacer26 = new QSpacerItem(300, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout1->addItem(spacer26);


        vboxLayout1->addLayout(hboxLayout1);

        line1 = new QFrame(groupBox6);
        line1->setObjectName(QString::fromUtf8("line1"));
        line1->setFrameShape(QFrame::HLine);
        line1->setFrameShadow(QFrame::Sunken);

        vboxLayout1->addWidget(line1);

        hboxLayout2 = new QHBoxLayout();
        hboxLayout2->setSpacing(6);
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        spacer28 = new QSpacerItem(70, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout2->addItem(spacer28);

        m_scanMetaInfoReject = new QPushButton(groupBox6);
        m_scanMetaInfoReject->setObjectName(QString::fromUtf8("m_scanMetaInfoReject"));

        hboxLayout2->addWidget(m_scanMetaInfoReject);

        spacer29 = new QSpacerItem(81, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout2->addItem(spacer29);

        m_scanMetaInfoSave = new QPushButton(groupBox6);
        m_scanMetaInfoSave->setObjectName(QString::fromUtf8("m_scanMetaInfoSave"));

        hboxLayout2->addWidget(m_scanMetaInfoSave);

        spacer30 = new QSpacerItem(31, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout2->addItem(spacer30);


        vboxLayout1->addLayout(hboxLayout2);


        vboxLayout->addWidget(groupBox6);

        spacer45 = new QSpacerItem(20, 13, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout->addItem(spacer45);

        hboxLayout3 = new QHBoxLayout();
        hboxLayout3->setSpacing(6);
        hboxLayout3->setObjectName(QString::fromUtf8("hboxLayout3"));
        spacer23 = new QSpacerItem(91, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout3->addItem(spacer23);

        m_scanStartButton = new QPushButton(ScanPanelBase);
        m_scanStartButton->setObjectName(QString::fromUtf8("m_scanStartButton"));
        QFont font1;
        font1.setPointSize(14);
        font1.setBold(true);
        m_scanStartButton->setFont(font1);

        hboxLayout3->addWidget(m_scanStartButton);

        spacer24 = new QSpacerItem(141, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout3->addItem(spacer24);


        vboxLayout->addLayout(hboxLayout3);

        spacer46 = new QSpacerItem(20, 19, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout->addItem(spacer46);


        retranslateUi(ScanPanelBase);

        QMetaObject::connectSlotsByName(ScanPanelBase);
    } // setupUi

    void retranslateUi(QWidget *ScanPanelBase)
    {
        ScanPanelBase->setWindowTitle(QApplication::translate("ScanPanelBase", "ScanPanel", 0));
        groupBox4->setTitle(QApplication::translate("ScanPanelBase", "Configuration", 0));
        textLabel1_3_3->setText(QApplication::translate("ScanPanelBase", "Scan Type :", 0));
#ifndef QT_NO_TOOLTIP
        m_scanConfigTagSelector->setProperty("toolTip", QVariant(QApplication::translate("ScanPanelBase", "Choose a certain scan, analysis or the current values", 0)));
#endif // QT_NO_TOOLTIP
        textLabel1_3->setText(QApplication::translate("ScanPanelBase", "Configuration Tag :", 0));
        textLabel1_3_2->setText(QApplication::translate("ScanPanelBase", "Revision :", 0));
        m_scanSettingsButton->setText(QApplication::translate("ScanPanelBase", "Settings", 0));
        m_createScanTypeButton->setText(QApplication::translate("ScanPanelBase", "Create New Scan", 0));
        groupBox5->setTitle(QApplication::translate("ScanPanelBase", "Status", 0));
        textLabel2->setText(QApplication::translate("ScanPanelBase", "Serial Number :", 0));
        m_serialNumberDisplay->setText(QApplication::translate("ScanPanelBase", "S000000000", 0));
        textLabel2_2_2->setText(QApplication::translate("ScanPanelBase", "Status :", 0));
        textLabel2_2->setText(QApplication::translate("ScanPanelBase", "Progress :", 0));
        groupBox6->setTitle(QApplication::translate("ScanPanelBase", "Meta Information", 0));
        textLabel2_2_2_2->setText(QApplication::translate("ScanPanelBase", "Comment :", 0));
        textLabel2_2_2_2_2->setText(QApplication::translate("ScanPanelBase", "Quality :", 0));
        m_scanQualitySelector->clear();
        m_scanQualitySelector->insertItems(0, QStringList()
         << QApplication::translate("ScanPanelBase", "Good", 0)
         << QApplication::translate("ScanPanelBase", "Trash", 0)
         << QApplication::translate("ScanPanelBase", "Debug", 0)
        );
        m_scanMetaInfoReject->setText(QApplication::translate("ScanPanelBase", "Cancel", 0));
        m_scanMetaInfoSave->setText(QApplication::translate("ScanPanelBase", "Save", 0));
        m_scanStartButton->setText(QApplication::translate("ScanPanelBase", "Start", 0));
    } // retranslateUi

};

namespace Ui {
    class ScanPanelBase: public Ui_ScanPanelBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCANPANELBASE_H
