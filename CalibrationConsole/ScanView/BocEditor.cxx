#include "BocEditor.h"
#include "ChangeBocSettings.h"

#include <ConfigWrapper/ConfObjUtil.h>
#include <CalibrationDataManager.h>
#include <Highlighter.h>
#include <QTVisualiser/BusyLoopFactory.h>

#include <PixBoc/PixBoc.h>

#include <QMessageBox>


namespace PixCon {

  class BocSettingsItem : public QTreeWidgetItem
  {
  public:
        BocSettingsItem(QTreeWidgetItem *rod_item, const std::string &conn_name)
	  : QTreeWidgetItem(rod_item){setText(0,conn_name.c_str());} 


//     void setRx1(unsigned int plugin,
// 		EMccSpeed speed,
// 		float rx_delay_old,
// 		float rx_delay,
// 		float rx_threshold_old,
// 		float rx_threshold) {
//       setRx(0, plugin, speed, rx_delay_old, rx_delay, rx_threshold_old, rx_threshold);
//     }

//     void setRx2(unsigned int plugin,
// 		EMccSpeed speed,
// 		float rx_delay_old,
// 		float rx_delay,
// 		float rx_threshold_old,
// 		float rx_threshold) {
//       setRx(1,plugin, speed, rx_delay_old, rx_delay, rx_threshold_old, rx_threshold);
//     }

    void updateRx(const BocEditor::BocSettings_t &settings) {
      _setRx(settings);
    }

    void updateTx(const BocEditor::BocSettings_t &settings) {
      _setTx(settings);
    }

    bool matches(const std::string &conn_name, PixA::EDataLink data_link, BocEditor::EMccSpeed the_speed) const {
      return   (conn_name == text(0).toLatin1().data()  && link() == data_link && the_speed==speed());
    }

    const char *name() const {
      return text(0).toLatin1().data();
    }

    PixA::EDataLink link() const {
      if (strcmp(text(1).toLatin1().data(),BocEditor::linkTitle(PixA::DTO2))==0) return PixA::DTO2;
      else return PixA::DTO1;
    }

    BocEditor::EMccSpeed speed() const {
      if (text(2).indexOf("80Mb/s")>=0) {
    return BocEditor::kMcc80;
      }
      else {
    return BocEditor::kMcc40;
      }
    }


    void setRx(PixA::EDataLink data_link,
           unsigned int plugin,
           BocEditor::EMccSpeed speed) {

      _setRxName(data_link,plugin,speed);
    }

    void setTx(unsigned int plugin) {

      _setTxName(plugin);
    }

    std::string pluginName(unsigned int plugin, BocEditor::EMccSpeed speed) const;

  private:
    void _setTxName(unsigned int plugin);

    void _setTx(const BocEditor::BocSettings_t &boc_settings) {
      if (boc_settings.txPowerRef()>0 || boc_settings.hasTxPower()) {
    std::stringstream value;
    value << boc_settings.txPowerRef();
    if (boc_settings.txPowerDiffers()) {
      value << " -> " << boc_settings.txPower();
    }
    setText(4+2, value.str().c_str());
      }
    }

    void _setRxName(PixA::EDataLink data_link,
            unsigned int plugin,
            BocEditor::EMccSpeed speed);

    void _setRx(const BocEditor::BocSettings_t &boc_settings) {
      setReferenceScan(boc_settings);
      {
    std::stringstream value;
    value << boc_settings.rxDelayRef();
    if (boc_settings.rxDelayDiffers()) {
      value << " -> " << boc_settings.rxDelay();
    }
    setText(3, value.str().c_str());
      }

      {
    std::stringstream value;
    value << boc_settings.rxThresholdRef();
    if (boc_settings.rxThresholdDiffers()) {
      value << " -> " << boc_settings.rxThreshold();
    }
    setText(4, value.str().c_str());
      }
    }

    void setReferenceScan(const BocEditor::BocSettings_t &boc_settings) {

      if (boc_settings.referenceScan()>0) {
    std::string reference_scan_string = makeSerialNumberString(kScan, boc_settings.referenceScan());
    if (text(7).length()==0 || reference_scan_string != text(7).toLatin1().data()) {
      setText(7,reference_scan_string.c_str());
    }
      }
    }

  };

  std::string BocSettingsItem::pluginName(unsigned int plugin, BocEditor::EMccSpeed speed) const
  {
    std::string plugin_name(BocEditor::rxPluginTitle(plugin));
    if (speed != BocEditor::kDefault) {
      plugin_name +=" ";
      plugin_name += BocEditor::speedTitle(speed);
    }
    return plugin_name;
  }

  void BocSettingsItem::_setTxName(unsigned int plugin)
  {

    assert(plugin < 4);

    std::string plugin_name(BocEditor::txPluginTitle(plugin));
    setText(4+1, plugin_name.c_str());
  }

  void BocSettingsItem::_setRxName(PixA::EDataLink data_link,
                   unsigned int plugin,
                   BocEditor::EMccSpeed speed) {

    assert(plugin < 4);

    setText(1, BocEditor::linkTitle(data_link));
    std::string plugin_name(pluginName(plugin, speed));
    setText(2, plugin_name.c_str());
  }


  const char *BocEditor::s_linkTitle[2] = { "DTO2", "DTO"};
  const char *BocEditor::s_speedTitle[3] = { "", "40Mb/s", "80Mb/s"};
  std::map<std::string, BocEditor::EMccSpeed> BocEditor::s_speedMap;


  BocEditor::BocEditor(const std::shared_ptr<CalibrationDataManager> &calibration_data)
    : m_calibrationData(calibration_data),
      m_updateTags(true),
      m_haveAnalysisOptimalVar(false)
  {
    setupUi(this);
    m_onlyModifiedRodsButton->setChecked(true);
    m_onlyModifiedRodsButton->hide();
    m_saveTempButton->setEnabled(false);
    m_savePermButton->setEnabled(false);
    initSpeedMap();
    for(int i=0; i<m_bocSettingsListView->columnCount(); i++)
      m_bocSettingsListView->resizeColumnToContents(i);

    assert( m_calibrationData );
  }

  BocEditor::~BocEditor() {
  }

  void BocEditor::initSpeedMap() {
    if (s_speedMap.empty()) {
      s_speedMap["SINGLE_40"]=kMcc40;
      s_speedMap["DOUBLE_40"]=kMcc40;
      s_speedMap["SINGLE_80"]=kMcc80;
      s_speedMap["DOUBLE_80"]=kMcc80;
    }
  }

//boceditorbase is called here.. QDialog is the base, try with that
  void BocEditor::showEditor() {
   if (BocEditor::isHidden()) { //if (BocEditorBase::isHidden()) {
      m_updateTags=true;
  BocEditor::show();  //  BocEditorBase::show();
  BocEditor::raise();  //  BocEditorBase::raise();
      updateTags();
    }
  }

  void BocEditor::hideEditor() {
   if (!BocEditor::isHidden()) {// if (!BocEditorBase::isHidden()) {
  BocEditor::hide();   // BocEditorBase::hide();
    }
  }

  inline PixA::EDataLink dataLink(unsigned int histo_link_id) {
    assert(histo_link_id<2);
    return (histo_link_id==0 ?  PixA::DTO2 : PixA::DTO1 );
  }


  void BocEditor::changeRxSettings(const std::string &conn_name,
				   unsigned int histo_link_id,
				   float rx_delay,
				   float rx_threshold,
				   SerialNumber_t ref_scan_serial_number)
  {
    PixA::EDataLink data_link=dataLink(histo_link_id);
    _changeRxSettings(conn_name, data_link, rx_delay, rx_threshold, ref_scan_serial_number);
    for(int i=0; i<m_bocSettingsListView->columnCount(); i++)
      m_bocSettingsListView->resizeColumnToContents(i);
  }

  void BocEditor::_changeRxSettings(const std::string &conn_name,
				   PixA::EDataLink data_link,
				   float rx_delay,
				   float rx_threshold,
				   SerialNumber_t ref_scan_serial_number)
  {
    EMccSpeed speed_mode = speedMode( ref_scan_serial_number);

    _changeRxSettings(conn_name, data_link, speed_mode, rx_delay, rx_threshold, ref_scan_serial_number);
  }

  void BocEditor::_changeRxSettings(const std::string &conn_name,
				   PixA::EDataLink data_link,
				   EMccSpeed speed_mode,
				   float rx_delay,
				   float rx_threshold,
				   SerialNumber_t ref_scan_serial_number)
  {

    if (rx_delay<=0 && rx_threshold<=0) {
      std::cerr << "ERROR [BocEditor::_changeRxSettings] Invalid settings given for " << conn_name << "." << std::endl;
    }

    PixA::ConnectivityRef conn( m_calibrationData->currentConnectivity());
    if (!conn) {
      std::cerr << "ERROR [BocEditor::_changeRxSettings] Cannot determine the ROD for " << conn_name << " without a connectivity." << std::endl;
      return;
    }
    const PixLib::ModuleConnectivity *module_conn(conn.findModule(conn_name));
    const PixLib::RodBocConnectivity *rod_conn( PixA::rodBoc(module_conn) );
    if (!module_conn || !rod_conn) return;
    std::string rod_name ( PixA::connectivityName(rod_conn) );

    try {
      BocSettings_t &settings = createSettings(conn_name,data_link,speed_mode);

      settings.setRxDelay(rx_delay);
      settings.setRxThreshold(rx_threshold);
      settings.setReference(ref_scan_serial_number);

      if (settings.differs()) {
	if (!m_saveTempButton->isEnabled()) m_saveTempButton->setEnabled(true);
	if (!m_savePermButton->isEnabled()) m_savePermButton->setEnabled(true);
      }

      BocSettingsItem *boc_item = createItem(conn_name, data_link, speed_mode);

      if (boc_item) {
	boc_item->updateRx(settings);
	boc_item->updateTx(settings);
	updateTags();
      }
    }
    catch ( std::runtime_error &err) {
      std::cerr << err.what();
    }
  }




  void BocEditor::resetRxSettings(const std::string &/*conn_name*/) {
  }

  std::pair<float, float> BocEditor::rxSettings(const std::string &conn_name, unsigned int histo_link_id, SerialNumber_t ref_scan_serial_number) {
    EMccSpeed speed_mode = speedMode( ref_scan_serial_number);
    PixA::EDataLink data_link = dataLink(histo_link_id);
    if (!m_newSettings.empty()) {
      Key_t settings_key(conn_name, data_link, speed_mode);
      std::map<Key_t, BocSettings_t>::iterator  new_settings_iter = m_newSettings.find( settings_key);
      if (new_settings_iter != m_newSettings.end()) {
	if (new_settings_iter->second.hasRxDelay() && new_settings_iter->second.hasRxThreshold()) {
	  return std::make_pair<float, float>( new_settings_iter->second.rxDelay(), new_settings_iter->second.rxThreshold());
	}
      }
    }
    else {
      std::string dest_tag = (m_tagSelector->currentText()).toUtf8().constData();
      if (!dest_tag.empty()) {
    return loadSettings(conn_name, data_link, speed_mode, dest_tag).first;
      }
    }
    return std::make_pair<float,float>(0.0,0.0);
  }

  SerialNumber_t BocEditor::findAnalysis(SerialNumber_t scan_serial_number, const std::string &analysis_name)
  {
    const ScanMetaDataCatalogue &scan_meta_data_catalogue = calibrationData().scanMetaDataCatalogue();
    const AnalysisMetaDataCatalogue &analysis_meta_data_catalogue = calibrationData().analysisMetaDataCatalogue();

    const  ScanMetaData_t * scan_meta_data = scan_meta_data_catalogue.find(scan_serial_number);
    SerialNumber_t analysis_serial_number=0;
    if (scan_meta_data) {
      for(std::vector<SerialNumber_t>::const_iterator analysis_iter = scan_meta_data->beginAnalysis();
      analysis_iter != scan_meta_data->endAnalysis();
      analysis_iter++) {
    const AnalysisMetaData_t *analysis_meta_data  = analysis_meta_data_catalogue.find(*analysis_iter);
    if (analysis_meta_data && analysis_meta_data->analysisName()==analysis_name) {
      analysis_serial_number = *analysis_iter;
    }
      }
    }
    return analysis_serial_number;
  }

  const char *BocEditor::s_analysisOptimalSettings[2][2]=    {
    { "CAN_optimalDelay_Link1",     "CAN_optimalDelay_Link2"},
    { "CAN_optimalThreshold_Link1", "CAN_optimalThreshold_Link2"}
  };

  const char *BocEditor::s_optoLinkAnalysis="OPTOLINKanalysis";

  std::pair<float, float> BocEditor::analysisRxSettings(const std::string &conn_name, unsigned int the_link, SerialNumber_t ref_scan_serial_number)
  {
    SerialNumber_t analysis_serial_number(findAnalysis(ref_scan_serial_number, s_optoLinkAnalysis));
    if (analysis_serial_number>0) {
    if (!m_haveAnalysisOptimalVar) {
      try {
    for (unsigned int var_i=0; var_i<2; var_i++) {
      for (unsigned int link_i=0; link_i<2; link_i++) {
        VarDef_t a_var_def(VarDefList::instance()->getVarDef(s_analysisOptimalSettings[var_i][link_i]));
        if (!a_var_def.isValid()) break;
        m_analysisOptimalVarId[var_i][link_i]=a_var_def.id();
      }
    }
    m_haveAnalysisOptimalVar=true;
      }
      catch (CalibrationDataException &) {
      }
    }

    if (m_haveAnalysisOptimalVar) {
      if (analysis_serial_number>0) {
	Lock lock(calibrationData().calibrationDataMutex());
	PixA::EDataLink data_link = dataLink(the_link);
	unsigned int link_i = (data_link == PixA::DTO1 ? 1 : 0);
	try {

	  float value[2];
	  for (unsigned int var_i=0; var_i<2; var_i++) {
	    CalibrationData::ConnObjDataList conn_data(calibrationData().calibrationData().getConnObjectData(conn_name));
	    value[var_i] = conn_data.analysisValue<float>(analysis_serial_number, m_analysisOptimalVarId[var_i][link_i]);
	    if (value[var_i]<=0) {
	      value[var_i]=0;
	    }
	  }

          value[0] = fmod(value[0], 25);
          if (value[0] < 0) value[0] += 25;          

	  if (value[1]>255) {
	    value[1]=0;
	  }
	  return std::make_pair<float,float>((float)(value[0]),(float)(value[1]));
	}
	catch (CalibrationData::MissingValue &) {
	}
      }
    }
    }

    return std::make_pair<float,float>(0.0,0.0);
  }

  bool BocEditor::hasSomethingToSave() const
  {
    bool something_to_save=false;
    for (std::map<Key_t, BocSettings_t>::const_iterator settings_iter = m_newSettings.begin();
     settings_iter != m_newSettings.end();
     ++settings_iter) {
      if (settings_iter->second.differs()) {
    something_to_save=true;
    break;
      }
    }
    return something_to_save;
  }

  void BocEditor::saveChangesTemp()
  {
    std::cout << "INFO [BocEditor::saveChangesTemp] " << std::endl;
    bool something_to_save=hasSomethingToSave();
    if (something_to_save) saveChanges("BocEdit_Tmp");
    m_saveTempButton->setEnabled(false);
  }

  void BocEditor::saveChangesPerm()
  {
    std::cout << "INFO [BocEditor::saveChangesPerm] " << std::endl;
    bool something_to_save=hasSomethingToSave();
    if (something_to_save) saveChanges("BocEdit");
    m_savePermButton->setEnabled(false);
    m_bocSettingsListView->clear();
    m_newSettings.clear();
  }

  void BocEditor::saveChanges(const std::string& pending_tag)
  {
    unsigned int error_counter=0;
    unsigned int save_counter=0;
   std::string dest_tag_str = (m_tagSelector->currentText()).toUtf8().constData();
    if (dest_tag_str.empty()) {
      std::cerr << "ERROR [BocEditor::saveChanges] No configuration tag selected." << std::endl;
      QColor white("white");
      QColor highlight_color("yellow");

      if (!m_highlighter.get()) {
    m_highlighter=std::make_unique<Highlighter>(*m_tagSelector,highlight_color,white,3*1000);
      }
      else {
    m_highlighter->restart(highlight_color,3*1000);
      }
    }
    else {
      try {
	std::string dest_tag_name = m_tagSelector->currentText().toLatin1().data() ;
	
	PixA::ConnectivityRef conn( m_calibrationData->currentConnectivity());
	if (!conn) {
	  std::cerr << "ERROR [BocEditor::saveChanges] No connectivity, no destination tag. Do not know what to do." << std::endl;
	  return;
	}
	PixA::Revision_t clone_revision=0;
	if (!tagExist(m_tagSelector->currentText())) {
	  
	  // clone the current tag
	  
	  std::stringstream message1;
	  message1 << "The tag " << dest_tag_name
		  << " does not seem to exist.\n"
		  << "Shall the tag be cloned from "
		  << conn.tag(PixA::IConnectivity::kId) << ":" << conn.tag(PixA::IConnectivity::kConfig)
		  << " ?";
	  
	  int answer = QMessageBox::question(this, "Clone Tag? -- BocEditor", message1.str().c_str(),
					     "&No", "&Yes",QString::null, 1,0);
	  if (answer==1) {
	    std::stringstream message2;
	    message2 << "Clone tag " << dest_tag_name<< " from "
		    << conn.tag(PixA::IConnectivity::kId) << ":" << conn.tag(PixA::IConnectivity::kConfig) 
		    << "...";
	    
	    std::unique_ptr<PixA::IBusyLoop> busy_loop(PixA::BusyLoopFactory::busyLoop(message2.str()));
	    PixA::ConnObjConfigDb config_db(PixA::ConnObjConfigDb::sharedInstance());
	    if (!config_db.cloneTag(conn.tag(PixA::IConnectivity::kId), conn.tag(PixA::IConnectivity::kConfig),dest_tag_name)) {
	      std::cerr << "ERROR [BocEditor::saveChanges] Failed to clone tag " 
			<< conn.tag(PixA::IConnectivity::kId) << ":" << conn.tag(PixA::IConnectivity::kConfig)
			<< " to " << dest_tag_name
			<< "." << std::endl;
	      return;
	    }
	    clone_revision = ::time(0);
	    m_updateTags=true;
	  }
	  else {
	    std::cout << "INFO [BocEditor::saveChanges] No existing destination tag selected. Will not save configuration." << std::endl;
	    return;
	  }
	}
	
	{
	  std::stringstream message;
	  message << "Saving changed BOC configurations in " << dest_tag_name << "...";
	  
	  std::unique_ptr<PixA::IBusyLoop> busy_loop(PixA::BusyLoopFactory::busyLoop(message.str()));
	  PixA::ConnObjConfigDb config_db(PixA::ConnObjConfigDb::sharedInstance());
	  PixA::ConnObjConfigDbTagRef src_tag( config_db.getTag(conn.tag(PixA::IConnectivity::kId), conn.tag(PixA::IConnectivity::kConfig), 0) );
	  PixA::ConnObjConfigDbTagRef dest_tag_obj( config_db.getTag(conn.tag(PixA::IConnectivity::kId), dest_tag_name, 0) );
	  PixA::Revision_t revision=::time(0);
	  if (revision==clone_revision) revision++; // otherwise writing will fail
	  std::map<std::string, std::vector<Key_t> > rod_list;
	  
	  // create the list of rods for which configurations need to be stored.
	  for (std::map<Key_t, BocSettings_t>::const_iterator settings_iter = m_newSettings.begin();
	       settings_iter != m_newSettings.end();
	       ++settings_iter) {
	    
	    const PixLib::ModuleConnectivity *module_conn(conn.findModule(settings_iter->first.name()));
	    const PixLib::RodBocConnectivity *rod_conn( PixA::rodBoc(module_conn) );
	    if (!module_conn || !rod_conn) continue;
	    std::string rod_name ( PixA::connectivityName(rod_conn) );
	    
	    if (settings_iter->second.differs()) {
	      rod_list[rod_name].push_back(settings_iter->first);
	    }
	  }
	  
	  // create configured pixmodule group for each ROD apply the changes
	  // and store the final configuration in the destination tag
	  for (std::map<std::string, std::vector<Key_t> >::const_iterator rod_iter = rod_list.begin();
	       rod_iter != rod_list.end();
	       ++rod_iter) {
	    
	    std::shared_ptr<PixLib::PixModuleGroup> pix_module_group(createConfiguredModuleGroup(src_tag, rod_iter->first));
	    if (pix_module_group) {
	      
	      for (std::vector<Key_t>::const_iterator key_iter=rod_iter->second.begin();
		   key_iter != rod_iter->second.end();
		   ++key_iter) {
		
		std::map<Key_t, BocSettings_t>::const_iterator settings_iter = m_newSettings.find(*key_iter);
		if (settings_iter == m_newSettings.end()) {
		  // should never happen
		  std::cerr << "ERROR [BocEditor::saveChanges] Settings were existing for this key less than a second ago."
			    << "No idea what may have happend." << std::endl;
		  continue;
		}
		
		try {
		  const PixLib::ModuleConnectivity *module_conn(conn.findModule(key_iter->name()));
		  
		  if (settings_iter->second.rxThresholdDiffers() || settings_iter->second.rxDelayDiffers()) {
		    PixA::ModulePluginConnection rx_plugin(PixA::rxConnection(module_conn, key_iter->link()));
		    if (!rx_plugin.isValid()) {
		      std::cerr << "ERROR [BocEditor::saveChanges] Not a valid link " << linkTitle(key_iter->link())
				<< " for " << key_iter->name() << "." << std::endl;
		      continue;
		    }

		    PixLib::Config &rx_config( pix_module_group->getPixBoc()->getConfig()->subConfig( rxName(rx_plugin.plugin()) ) );

		    if(pix_module_group->getCtrlType() == PixLib::CPPROD){
		      unsigned int newPhase = round(settings_iter->second.rxDelay());
		        if( newPhase != confVal<unsigned int>(rx_config["General"][ phaseDelayName (rx_plugin.channel()) ]) )
		          confVal<unsigned int>(rx_config["General"][ phaseDelayName (rx_plugin.channel()) ]) = newPhase;

		    } else {

		      if (settings_iter->second.rxDelayDiffers()) {
		        confVal<float>(rx_config["General"][ dataDelayName(key_iter->speed(), rx_plugin.channel()) ])
			= settings_iter->second.rxDelay();
		      }
		      if (settings_iter->second.rxThresholdDiffers()) {
		      
		        confVal<float>(rx_config["Opt"][     thresholdName(key_iter->speed(), rx_plugin.channel()) ])
			= settings_iter->second.rxThreshold();
		      }
		    }
		  }
		}
		catch(PixA::ConfigException &err) {
		  std::cerr << "ERROR [BocEditor::saveChanges] Caught Exception while storing BOC configuration for " << ""  
			    << key_iter->name() << " " << (linkTitle(key_iter->link()))
			    << " : " << err.getDescriptor() << std::endl;
		  error_counter++;
		  continue;
		}
	      }
	      if (dest_tag_obj.storeConfig(*pix_module_group, revision, pending_tag)==0) {
		std::cerr << "ERROR [BocEditor::saveChanges] Failed to write new  BOC configuration for " << ""  
			  << rod_iter->first
			  << " to " << dest_tag_obj << "." << std::endl;
		error_counter++;
	      }
	      else {
		save_counter++;
	      }
	      
	    }
	  }
	}
	if (error_counter>0) {
	  std::cerr << "ERROR [BocEditor::saveChanges] Experienced " << error_counter
		    << " error" << (error_counter>1 ? "s" : "") <<  " while trying to save the BOC configurations to " << dest_tag_str << "." << std::endl;
	}
	else {
	  std::cerr << "INFO [BocEditor::saveChanges] Saved " << save_counter
		    << " BOC configuration" << (save_counter>1 ? "s" : "") <<  " to tag " << dest_tag_str << "." << std::endl;
	}
      }
      catch (PixA::BaseException &err) {
    std::cerr << "ERROR [BocEditor::saveChanges] Caught exception : " << err.getDescriptor() << std::endl;
    return;
      }
      catch (std::exception &err) {
    std::cerr << "ERROR [BocEditor::saveChanges] Caught exception : " << err.what() << std::endl;
    return;
      }
      catch (...) {
    std::cerr << "ERROR [BocEditor::saveChanges] Caught unknown exception." << std::endl;
    return;
      }
    }

  }

  void BocEditor::removeAllChanges() {

    unsigned int counter=0;
    for (std::map<Key_t, BocSettings_t>::const_iterator settings_iter = m_newSettings.begin();
     settings_iter != m_newSettings.end();
     ++settings_iter) {
      if (settings_iter->second.differs()) {
    counter++;
      }
    }

    if (counter>0) {
      std::stringstream message;
      message << "You want to remove all the " << counter << " unsaved changes ? ";

      int answer = QMessageBox::question(this, "Remove Changes ? -- BocEditor", message.str().c_str(),
                     "&No", "&Yes",QString::null, 1,0);
      if (answer==0) {
    return;
      }
    }

    m_bocSettingsListView->clear();
    m_newSettings.clear();
  }

  void BocEditor::updateTags()
  {
    if (!isHidden() && m_updateTags) {

      PixA::ConnectivityRef conn( m_calibrationData->currentConnectivity());
      if (!conn) {
    std::cerr << "ERROR [BocEditor::updateTags] Cannot query configuration tags without ID tag, but there is no connectivity." << std::endl;
    return;
      }

      std::vector<std::string> tags;
      {
    std::unique_ptr<PixA::IBusyLoop> busy_loop(PixA::BusyLoopFactory::busyLoop(" Get tags from db server and CORAL ..."));
    PixA::ConnObjConfigDb config_db(PixA::ConnObjConfigDb::sharedInstance());
    config_db.getTags(conn.tag(PixA::IConnectivity::kId),tags);
      }

      std::string current_tag = m_tagSelector->currentText().toLatin1().data();
      m_tagSelector->clear();
      unsigned int index=UINT_MAX;
      //QFontMetrics fm(m_tagSelector->font());
      //int max_text_width = 0;
      for(std::vector<std::string>::const_iterator tag_iter = tags.begin(); tag_iter !=tags.end(); tag_iter++) {
	m_tagSelector->addItem(tag_iter->c_str());
	//if(fm.width(QString(tag_iter->c_str()))>max_text_width) max_text_width = fm.width(QString(tag_iter->c_str()));
	if (current_tag == *tag_iter) {
	  index = m_tagSelector->count()-1;
	}
      }
      if (index != UINT_MAX) {
	m_tagSelector->setCurrentIndex(index);
      }
      else {
	for(int i=0;i<m_tagSelector->count();i++){
	  if(m_tagSelector->itemText(i)==QString(current_tag.c_str())){
	    m_tagSelector->setCurrentIndex(i);
	    break;
	  }
	}
      }
      m_tagSelector->setSizeAdjustPolicy(QComboBox::AdjustToContents);
      /*
      QSize a_size(m_tagSelector->sizeHint());
      std::cout << "DEBUG [BocEditor::updateTags] tag selector size = " << a_size.width() << " x " << a_size.height() << std::endl;
      a_size.setWidth(max_text_width);
      m_tagSelector->setMinimumSize(a_size);
      adjustSize();
      std::cout << "DEBUG [BocEditor::updateTags] tag selector new size = " << a_size.width() << " x " << a_size.height() << std::endl;
      */
      /*
      if (m_tagSelector->view()) { //change listbox() to view() from QComboBox
	std::cout << "INFO [BocEditor::updateTags] Combo box size  = " <<  m_tagSelector->view()->maxItemWidth() << std::endl;
	if (m_tagSelector->view()->maxItemWidth()+text_width*2>a_size.width()) {
	  a_size.setWidth(static_cast<int>(m_tagSelector->view()->maxItemWidth()+text_width*2));
	  m_tagSelector->resize(a_size);
	  adjustSize();
	  std::cout << "INFO [BocEditor::updateTags] tag selector new size = " << a_size.width() << " x " << a_size.height() << std::endl;
	}
      }
      */
      m_updateTags=false;
    }
  }
/*
  bool BocEditor::tagExist(const QString &tag) const {
    return m_tagSelector->view()->findItems(tag,Qt::CaseSensitive | Q3ListBox::ExactMatch) != NULL;
  } */ //New porting
  bool BocEditor::tagExist(const QString &tag) const {
    return m_tagSelector->findText ( tag,/* static_cast<Qt::MatchFlags> */ Qt::MatchExactly | Qt::MatchCaseSensitive) != -1;//
  }
  void BocEditor::createFullList() {
  }

  void BocEditor::createListOfChanges() {
  }

  BocSettingsItem *BocEditor::createItem(const std::string &conn_name, PixA::EDataLink data_link, EMccSpeed speed_mode) {

    PixA::ConnectivityRef conn( m_calibrationData->currentConnectivity());
    if (!conn) {
      std::cerr << "ERROR [BocEditor::createItem] Cannot determine the ROD for " << conn_name << " without a connectivity." << std::endl;
      return NULL;
    }

    const PixLib::ModuleConnectivity *module_conn(conn.findModule(conn_name));
    const PixLib::RodBocConnectivity *rod_conn( PixA::rodBoc(module_conn) );
    if (!module_conn || !rod_conn) return NULL;

    PixA::ModulePluginConnection rx_plugin(PixA::rxConnection(module_conn, data_link));
    if (!rx_plugin.isValid()) {
      std::cerr << "ERROR [BocEditor::createItem] Invalid RX connection for  " << conn_name << " " << linkTitle(data_link) << std::endl;
      return NULL;
    }

    std::string rod_name ( PixA::connectivityName(rod_conn) );

    // Porting of the code here
    QTreeWidgetItem *rod_item = NULL;
    for(int i=0; i<m_bocSettingsListView->topLevelItemCount();i++){
      QTreeWidgetItem *top_item = m_bocSettingsListView->topLevelItem(i);
      if (rod_name == top_item->text(0).toLatin1().data()) {
	rod_item = top_item;
	for(int k=0; k<top_item->childCount(); k++ ) {
	    BocSettingsItem *settings_item = dynamic_cast<BocSettingsItem*>(top_item->child(k));
	    if (settings_item){
	      if (settings_item->matches(conn_name, data_link, speed_mode)){
		return settings_item;
	      }
	    }
	}
	break;
      }
    }
    if (!rod_item){
      rod_item = new QTreeWidgetItem(m_bocSettingsListView);
      rod_item->setText(0, rod_name.c_str());
    }
    rod_item->setExpanded(true);

    BocSettingsItem *link_boc_item(new BocSettingsItem(rod_item, conn_name));
	
    link_boc_item->setRx(data_link,rx_plugin.plugin(), speed_mode);
    
    if (data_link==PixA::DTO2 || !PixA::rxConnection(module_conn, PixA::DTO2).isValid() ) {
      PixA::ModulePluginConnection tx_plugin(PixA::txConnection(module_conn));
      link_boc_item->setTx(tx_plugin.plugin());
    }
    
    return link_boc_item;

  }
/*
    Q3ListViewItem *child = m_bocSettingsListView->firstChild();//
    Q3ListViewItem *rod_item = NULL;//
    while (child) {
      if (rod_name == child->text(0).toLatin1().data()) {
    rod_item = child;
    child = child->firstChild();
    while (child) {
      BocSettingsItem *settings_item = dynamic_cast<BocSettingsItem *>(child);
      if (settings_item) {
        if (settings_item->matches(conn_name,data_link, speed_mode)) {
          return settings_item;
        }
      }
      child = child->nextSibling();
    }
    break;
      }
      child = child->nextSibling();
    }

    if (!rod_item) {
      rod_item = new Q3ListViewItem(m_bocSettingsListView, rod_name.c_str());//
    }
    rod_item->setOpen(true);

    BocSettingsItem *link_boc_item(new BocSettingsItem(rod_item, conn_name));

    link_boc_item->setRx(data_link,rx_plugin.plugin(), speed_mode);

    if (data_link==PixA::DTO2 || !PixA::rxConnection(module_conn, PixA::DTO2).isValid() ) {
      PixA::ModulePluginConnection tx_plugin(PixA::txConnection(module_conn));
      link_boc_item->setTx(tx_plugin.plugin());
    }

  }

return link_boc_item;*/



//   PixLib::RodBocConnectivity *BocEditor::rod(const std::string &conn_name) {

//     PixA::ConnectivityRef conn( m_calibrationData->currentConnectivity());
//     if (!conn) {
//       std::cerr << "ERROR [BocEditor::rodName] Cannot determine the ROD for " << conn_name << " without a connectivity." << std::endl;
//       return NULL;
//     }

//     PixLib::RodBocConnectivity *rod_boc ( conn.findModule(conn_name) );
//     if (!rod_boc) {
//       std::cerr << "ERROR [BocEditor::rodName] Did not find module " << conn_name << " in connectivity." << std::endl;
//       return NULL;
//     }
//     return rod_boc;
//   }

//   PixLib::PixModuleConnectivity *BocEditor::module(const std::string &conn_name) {

//     PixA::ConnectivityRef conn( m_calibrationData->currentConnectivity());
//     if (!conn) {
//       std::cerr << "ERROR [BocEditor::rodName] Cannot determine the ROD for " << conn_name << " without a connectivity." << std::endl;
//       return NULL;
//     }

//     PixLib::PixModuleConnectivity *module_conn ( conn.findModule(conn_name) );
//     return module_conn;
//   }

//    BocEditor::rodName(const std::string &conn_name) {

//     PixA::ConnectivityRef conn( m_calibrationData->currentConnectivity());
//     if (!conn) {
//       std::cerr << "ERROR [BocEditor::rodName] Cannot determine the ROD for " << conn_name << " without a connectivity." << std::endl;
//       return "";
//     }

//   std::string BocEditor::rodName(const std::string &conn_name) {

//     PixA::ConnectivityRef conn( m_calibrationData->currentConnectivity());

//     PixLib::RodBocConnectivity *rod_boc ( conn.findModule(conn_name) );
//     if (!rod_boc) {
//       std::cerr << "ERROR [BocEditor::rodName] Did not find module " << conn_name << " in connectivity." << std::endl;
//       return "";
//     }

//     return PixA::connectivityName( rod_boc );
//   }

  std::shared_ptr<PixLib::PixModuleGroup> BocEditor::createConfiguredModuleGroup(const PixA::ConnObjConfigDbTagRef &tag_ref, const std::string &conn_name) const
  {
    PixA::ConnObjConfigDbTagRef config_db(tag_ref);

    std::shared_ptr<PixLib::PixModuleGroup> obj(config_db.createConfiguredModuleGroup( conn_name));

    if (!obj.get() || !obj->getPixBoc() || !obj->getPixController()){
      std::cerr << "FATAL [BocEditor::createConfiguredModuleGroup] Failed to create module group "
        << conn_name
        << " does not contain PixBoc or PixController. "  << std::endl;
      return std::shared_ptr<PixLib::PixModuleGroup>();
    }
//     std::cout << "INFO [BocEditor::createConfiguredModuleGroup] config of " << conn_name << ": "
// 	      << std::endl;
//     obj->config().dump(std::cout);
//     std::cout << "INFO [BocEditor::createConfiguredModuleGroup] PixBoc config of " << conn_name << ": "
// 	      << std::endl;
//     obj->getPixBoc()->getConfig()->dump(std::cout);

//     std::cout << "INFO [BocEditor::createConfiguredModuleGroup] PixController config of " << conn_name << ": "
// 	      << std::endl;
//     obj->getPixController()->config().dump(std::cout);

    return obj;
  }

  void BocEditor::edit(const std::string &conn_name, unsigned int histo_link_id, EMccSpeed speed_mode) {
    PixA::EDataLink data_link(dataLink(histo_link_id));
    _edit(conn_name, data_link, speed_mode);
  }

  void BocEditor::_edit(const std::string &conn_name, PixA::EDataLink data_link, EMccSpeed speed_mode) {

    PixA::ConnectivityRef conn( m_calibrationData->currentConnectivity());
    if (!conn) {
      std::cerr << "ERROR [BocEditor::createItem] Cannot determine the ROD for " << conn_name << " without a connectivity." << std::endl;
      return;
    }

    const PixLib::ModuleConnectivity *module_conn(conn.findModule(conn_name));
    const PixLib::RodBocConnectivity *rod_conn( PixA::rodBoc(module_conn) );
    if (!module_conn || !rod_conn) return;

    PixA::ModulePluginConnection rx_plugin(PixA::rxConnection(module_conn, data_link));
    PixA::ModulePluginConnection tx_plugin(PixA::txConnection(module_conn));
    if (!rx_plugin.isValid() || !tx_plugin.isValid()) return;

    std::string rod_name ( PixA::connectivityName(rod_conn) );

    try {
      BocSettings_t &settings = createSettings(conn_name,data_link,speed_mode);
      std::stringstream title;
      title << conn_name << " : " << linkTitle(data_link) << ", " << speedTitle(speed_mode);
      ChangeBocSettings edit(title.str(), data_link, this);
      edit.setRxPluginName(rxPluginTitle(rx_plugin.plugin())+" "+speedTitle(speed_mode));
      if (settings.hasRxDelay()) edit.setRxDelay(settings.rxDelay());
      edit.setRxDelayReference(settings.rxDelayRef());
      if (settings.hasRxThreshold()) edit.setRxThreshold(settings.rxThreshold());
      edit.setRxThresholdReference(settings.rxThresholdRef());

      if (data_link == PixA::DTO2 || !PixA::rxConnection(module_conn, PixA::DTO2).isValid()) {
    edit.setTxPluginName(txPluginTitle(tx_plugin.plugin()));
    if (settings.hasTxPower())  edit.setTxPower(settings.txPower());
    edit.setTxPowerReference(settings.txPowerRef());
      }

      if (edit.exec()==QDialog::Accepted) {
    //	if (edit.hasTxPower()) {
    settings.setTxPower(edit.txPower());
      //	}
      //	i\f (edit.hasRxThreshold() || edit.hasRxDelay()) {
    _changeRxSettings(conn_name, data_link, speed_mode, edit.rxDelay(),edit.rxThreshold(), settings.referenceScan());
      //	}
      }
    }
    catch ( std::runtime_error &err) {
      std::cerr << err.what();
    }

  }

  void BocEditor::changeSettings(QTreeWidgetItem *item, int /*i*/) {//
    BocSettingsItem *boc_settings_item = dynamic_cast<BocSettingsItem *>(item);
    if (boc_settings_item) {
      _edit( boc_settings_item->name(), boc_settings_item->link(), boc_settings_item->speed());
    }
  }


  std::pair< std::pair<float, float> ,int> BocEditor::loadSettings(const std::string &conn_name,
					     PixA::EDataLink data_link,
					     EMccSpeed speed_mode,
					     const std::string &tag_name)
  {
      PixA::ConnectivityRef conn( m_calibrationData->currentConnectivity());
      if (!conn) {
    std::stringstream message;
    message << "ERROR [BocEditor::createItem] Cannot determine the ROD for " << conn_name << " without a connectivity.";
    throw std::runtime_error(message.str());
      }

      const PixLib::ModuleConnectivity *module_conn(conn.findModule(conn_name));
      const PixLib::RodBocConnectivity *rod_conn( PixA::rodBoc(module_conn) );
      if (!module_conn || !rod_conn) {
    std::stringstream message;
    message << "ERROR [BocEditor::createItem] Failed to get ROD or module connectivity information for " << conn_name << ".";
    throw std::runtime_error(message.str());
      }

      // and fill with current configuration
      PixA::ModulePluginConnection rx_plugin(PixA::rxConnection(module_conn, data_link));
      if (!rx_plugin.isValid()) {
    std::stringstream message;
    message << "ERROR [BocEditor::createSettings] No valid link " << linkTitle(data_link)
        << " defined for " << conn_name << " without a connectivity.";
    throw std::runtime_error(message.str());
      }


      float rx_threshold=0;
      float rx_delay=0;
      int tx_power =0;
      try {
	PixA::ConnObjConfigDb config_db(PixA::ConnObjConfigDb::sharedInstance());
	PixA::ConnObjConfigDbTagRef config_db_tag( config_db.getTag(conn.tag(PixA::IConnectivity::kId),
								    (tag_name.empty() ? conn.tag(PixA::IConnectivity::kConfig): tag_name ),
								    (tag_name.empty() ? conn.revision(): 0 )));

	PixA::ConfigHandle config_handle( config_db_tag.bocConfigHandle(PixA::connectivityName(rod_conn)) );
	PixA::ConfigRef config_ref(config_handle.ref());

	PixA::ConfigRef rx_config( config_ref.subConfig( rxName(rx_plugin.plugin()) ) );
	std::string ctrlType = rod_conn->rodBocType();

	  if(ctrlType == "CPPROD"){
	    rx_delay = confVal<unsigned int>(rx_config["General"][ phaseDelayName (rx_plugin.channel()) ]);
	    rx_threshold = 0;//No Threshold for IBL
	  }else {
	    rx_delay = confVal<float>(rx_config["General"][ dataDelayName(speed_mode, rx_plugin.channel()) ]);
	    rx_threshold = confVal<float>(rx_config["Opt"][     thresholdName(speed_mode, rx_plugin.channel()) ]);
	  }

	if (data_link==PixA::DTO2 || !PixA::rxConnection(module_conn, PixA::DTO2).isValid() ) {
	  PixA::ModulePluginConnection tx_plugin(PixA::txConnection(module_conn));

	  if (tx_plugin.isValid()) {
	    PixA::ConfigRef tx_config( config_ref.subConfig( txName(tx_plugin.plugin()) ) );
	    try {
	      tx_power = confVal<int>(tx_config["Opt"][ laserCurrentName(tx_plugin.channel()) ]);
	    }
	    catch (PixA::ConfigException &) {
	      std::cerr << "WARNING [BocEditor::createItem] Failed to get present TX configuration for " << conn_name
			<< " / " << linkTitle(data_link) << "." << std::endl;
	    }
	  }
	}
      }
      catch (PixA::ConfigException &) {
	std::cerr << "WARNING [BocEditor::createSettings] Failed to get present RX configuration for " << conn_name
          << " / " << linkTitle(data_link) << "." << std::endl;
      }

      return std::make_pair( std::make_pair(rx_delay, rx_threshold), tx_power);
  }

  BocEditor::BocSettings_t &BocEditor::createSettings(const std::string &conn_name, PixA::EDataLink data_link, EMccSpeed speed_mode)
  {
    Key_t settings_key(conn_name, data_link, speed_mode);
    std::map<Key_t, BocSettings_t>::iterator  new_settings_iter = m_newSettings.find( settings_key);
    if (new_settings_iter == m_newSettings.end()) {

      // create new BOC settings element
      std::pair<std::map<Key_t, BocSettings_t>::iterator,bool>
    ret = m_newSettings.insert(std::make_pair(settings_key, BocSettings_t()));

      if (!ret.second) {
    std::stringstream message;
    message << "FATAL [BocEditor::createSettings] Failed to add new settings for " << settings_key.name() << ".";
    throw std::runtime_error(message.str());
      }
      new_settings_iter = ret.first;
      std::pair< std::pair<float,float> ,int> reference_settings(loadSettings(conn_name,data_link, speed_mode));
      new_settings_iter->second.setRxReference(reference_settings.first.first, reference_settings.first.second);
      if (reference_settings.second>0) {
    new_settings_iter->second.setTxReference(reference_settings.second);
      }

    }
    return new_settings_iter->second;
  }

  BocEditor::EMccSpeed BocEditor::speedMode( SerialNumber_t scan_serial_number) {

    if (m_calibrationData) {
      PixA::ConfigHandle scan_config_handle = m_calibrationData->scanConfig(kScan,scan_serial_number,"");
      PixA::ConfigRef scan_config(scan_config_handle.ref());
      try {
    std::string speed = confVal<std::string>(scan_config["mcc"]["mccBandwidth"]);
    std::map<std::string, EMccSpeed>::const_iterator speed_iter = s_speedMap.find(speed);
    if (speed_iter != s_speedMap.end()) {
      return speed_iter->second;
    }
      }
      catch (PixA::ConfigException &err) {
    std::cerr << "ERROR [BocEditor::speedMode] Failed to get mcc bandwidth for scan " << makeSerialNumberString(kScan, scan_serial_number)
          << " : " << err.getDescriptor() << std::endl;
      }
      catch (...) {
    std::cerr << "ERROR [BocEditor::speedMode] Failed to get mcc bandwidth for scan " << makeSerialNumberString(kScan, scan_serial_number)
          << "." << std::endl;
      }
    }
    return kMcc40;
  }


}
