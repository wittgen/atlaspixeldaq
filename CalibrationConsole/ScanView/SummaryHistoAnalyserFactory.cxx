#include "SummaryHistoAnalyserFactory.h"
#include <TH2.h>
#include <DataContainer/GenericHistogramAccess.h>
#include <ConfigWrapper/Connectivity.h>
#include <ConfigWrapper/ConnectivityUtil.h>
#include <CalibrationData.h>
#include <CalibrationDataManager.h>
#include <CategoryList.h>

#include <DefaultColourCode.h>
#include <iomanip>

namespace PixCon {

  template class ObjectFactoryImpl<ISummaryHistoAnalyserKit>;

  class SummaryHisto_t
  {

  public:

    enum EBinType {BinVarType,BinTag, BinTime,BinPp0Mask, BinModMask,BinFeMask,NSpecialBins};

    enum ELogVarType {V_dd, V_dda, V_hv, I_d, I_a, I_hv, V_iset, V_vdc, V_pin, I_iset, I_vdc, I_pin, T_mod, T_opto, status_mod, status_opto, status_global,
		      I_pin_mod, iolink_ratio1, iolink_ratio2, iolink_drx, NLogVarTypes};

    enum ELogTag {Initial, Final, Sub1, Sub2, Sub3, Sub4, Sub5, Sub6, CheckIviset, CheckDI, CheckAI, CheckHVI, NLogTags};
    enum ELogStatus {Passed, Failed, Unknown, NLogStati};

    enum EVarType {ModVar,LinkVar, Pp0Var, GlobalVar, NVarTypes};


    class VarInfo_t {
    public:
      VarInfo_t() : m_varType(NVarTypes) {}

      VarInfo_t(const std::string &name, EVarType var_type=ModVar)
	: m_name(name), m_varType(var_type), m_min(0),m_max(0),m_state(true) {}

      VarInfo_t(const std::string &name, float a_min, float a_max, const std::string &unit, EVarType var_type=ModVar) 
	: m_name(name), m_varType(var_type),m_min(a_min),m_max(a_max), m_unit(unit), m_state(false) {}

      const std::string &name() const   {return m_name;}
      const EVarType varType() const    {return m_varType;}
      operator bool() const             {return m_varType < NVarTypes;}
      float min() const                 {return m_min;}
      float max() const                 {return m_max;}
      bool isValue() const              {return !m_state; }
      
      const std::string &unit() const   {return m_unit;}

    private:
      std::string  m_name;
      EVarType     m_varType;
      float        m_min;
      float        m_max;
      std::string  m_unit;
      bool         m_state;
    };

    SummaryHisto_t(const PixCon::Histo_t *a_histo)
      : m_histo(a_histo) 
    {
      if (!s_initialised) init();
    }

    static bool init() {
      if (s_initialised) return true;

      s_logVarType.resize(NLogVarTypes);
      s_logVarType[status_global] = VarInfo_t("status",GlobalVar);
      s_logVarType[status_mod] = VarInfo_t("status mod",ModVar);
      s_logVarType[V_dd] = VarInfo_t("V_dd",0,10,"V",ModVar);
      s_logVarType[V_dda] = VarInfo_t("V_dda",0,10,"V",ModVar);
      s_logVarType[V_hv] = VarInfo_t("V_hv",0,1000,"V",ModVar);
      s_logVarType[I_d] = VarInfo_t("I_d",0,4,"A",ModVar);
      s_logVarType[I_a] = VarInfo_t("I_a",0,4,"A",ModVar);
      s_logVarType[I_hv] = VarInfo_t("I_hv",0,4,"A",ModVar);
      s_logVarType[T_mod] = VarInfo_t("T_mod",-20,40,"C",ModVar);
      s_logVarType[I_pin_mod] = VarInfo_t("I_pin_mod",0,4,"mA",LinkVar);
      s_logVarType[iolink_ratio1] = VarInfo_t("iolink_ratio1",0,0,"A",ModVar);
      s_logVarType[iolink_ratio2] = VarInfo_t("iolink_ratio2",0,0,"A",ModVar);
      s_logVarType[iolink_drx] = VarInfo_t("iolink_drx",0,0,"muA",ModVar);

      s_logVarType[status_opto] = VarInfo_t("status opto",Pp0Var);
      s_logVarType[V_iset] = VarInfo_t("V_iset",0,10,"V",Pp0Var);
      s_logVarType[V_vdc] = VarInfo_t("V_vdc",0,10,"V",Pp0Var);
      s_logVarType[V_pin] = VarInfo_t( "V_pin",0,10,"V",Pp0Var);
      s_logVarType[I_iset] = VarInfo_t("I_iset",0,4,"A",Pp0Var);
      s_logVarType[I_vdc] = VarInfo_t("I_vdc",0,4,"A",Pp0Var);
      s_logVarType[I_pin] = VarInfo_t("I_pin",0,4,"mA",Pp0Var);
      s_logVarType[T_opto] = VarInfo_t("T_opto",-20,40,"C",Pp0Var);

      s_logStatus.resize(NLogStati);
      s_logStatus[Passed] = "Passed";
      s_logStatus[Failed] = "Failed";
      s_logStatus[Unknown] = "Unknown";

      s_logStatusSuccess.resize(NLogStati);
      s_logStatusSuccess[Passed] = true;
      s_logStatusSuccess[Failed] = false;
      s_logStatusSuccess[Unknown] = false;

      s_logStatusPrecedence.resize(NLogStati);
      s_logStatusPrecedence[Passed] = 1;
      s_logStatusPrecedence[Failed] = 3;
      s_logStatusPrecedence[Unknown] = 2;

      s_logStatusMapping.resize(NLogStati);
      s_logStatusMapping[Passed] =  DefaultColourCode::kSuccess;
      s_logStatusMapping[Failed] =  DefaultColourCode::kFailure;
      s_logStatusMapping[Unknown] = DefaultColourCode::kUnknown;


      s_logTag.resize(NLogTags);
      s_logTag[Initial] = "Initial";
      s_logTag[Final] = "Final";
      s_logTag[Sub1] = "Sub1";
      s_logTag[Sub2] = "Sub2";
      s_logTag[Sub3] = "Sub3";
      s_logTag[Sub4] = "Sub4";
      s_logTag[Sub5] = "Sub5";
      s_logTag[Sub6] = "Sub6";
      s_logTag[CheckIviset] = "CheckIviset";
      s_logTag[CheckDI] = "CheckDI";
      s_logTag[CheckAI] = "CheckAI";
      s_logTag[CheckHVI] = "CheckHVI";
      return true;
    }

    operator bool () const {
      return m_histo != NULL;
    }

    unsigned int nRows() const {
      return PixA::nRows(m_histo);
    }

    ELogVarType logVarTypeId(unsigned int row_i) const {
      assert( PixA::nRows(m_histo) > row_i );
      ELogVarType  log_var_type= static_cast<ELogVarType>( static_cast<unsigned int>( PixA::binContent(m_histo,
												       PixA::startColumn(m_histo)+BinVarType,
												       PixA::startRow(m_histo)+row_i)));
      if (log_var_type < NLogVarTypes) {
	return log_var_type;
      }
      return NLogVarTypes;
    }

    const VarInfo_t * const logVarType(unsigned int row_i) const {
      assert( PixA::nRows(m_histo) > row_i );
      unsigned int bin_content= static_cast<unsigned int>( PixA::binContent(m_histo,
									    PixA::startColumn(m_histo)+BinVarType,
									    PixA::startRow(m_histo)+row_i));
      if (bin_content < NLogVarTypes) {
	assert(bin_content < s_logVarType.size());
	return &(s_logVarType[bin_content]);
      }
      return NULL;
    }

    unsigned int moduleMask(unsigned int row_i) const {
      assert( PixA::nRows(m_histo) > row_i );
      unsigned int mod_mask = static_cast<unsigned int>( PixA::binContent(m_histo,PixA::startColumn(m_histo)+BinModMask,PixA::startRow(m_histo)+row_i) );
      return mod_mask;
    }

    unsigned int pp0Mask(unsigned int row_i) const {
      assert( PixA::nRows(m_histo) > row_i );
      unsigned int pp0_mask = static_cast<unsigned int>( PixA::binContent(m_histo,PixA::startColumn(m_histo)+BinPp0Mask,PixA::startRow(m_histo)+row_i) );
      return pp0_mask;
    }

    unsigned int feMask(unsigned int row_i) const {
      assert( PixA::nRows(m_histo) > row_i );
      unsigned int fe_mask = static_cast<unsigned int>(  PixA::binContent(m_histo,PixA::startColumn(m_histo)+BinFeMask,PixA::startRow(m_histo)+row_i));
      return fe_mask;
    }

    ELogTag logTag(unsigned int row_i) const {
      assert( PixA::nRows(m_histo) > row_i );
      ELogTag log_tag = static_cast<ELogTag>( static_cast<unsigned int>( PixA::binContent(m_histo,
											  PixA::startColumn(m_histo)+BinTag,
											  PixA::startRow(m_histo)+row_i)));
      if (log_tag < NLogTags) {
	return log_tag;
      }
      return NLogTags;
    }

    std::string logTagName(unsigned int row_i) const {
      assert( PixA::nRows(m_histo) > row_i );
      ELogTag log_tag = static_cast<ELogTag>( static_cast<unsigned int>( PixA::binContent(m_histo,
											  PixA::startColumn(m_histo)+BinTag,
											  PixA::startRow(m_histo)+row_i)));
      if (log_tag < NLogTags) {
	assert( static_cast<unsigned int>(log_tag) < s_logTag.size());
	return s_logTag[log_tag];
      }
      return s_emptyString;
    }

    unsigned int logTime(unsigned int row_i) const {
      assert( PixA::nRows(m_histo) > row_i );
      return makeTime(PixA::binContent(m_histo,PixA::startColumn(m_histo)+BinTime,PixA::startRow(m_histo)+row_i));
    } 

    static unsigned int makeTime(const double &value)  {
      return static_cast<unsigned int>(value);
    }

    unsigned int elapsedLogTime(unsigned int row_i) const {
      assert( PixA::nRows(m_histo) > row_i );
      unsigned int a_time0 = static_cast<unsigned int>(PixA::binContent(m_histo,PixA::startColumn(m_histo)+BinTime,PixA::startRow(m_histo)+0));
      unsigned int a_time1 = static_cast<unsigned int>(PixA::binContent(m_histo,PixA::startColumn(m_histo)+BinTime,PixA::startRow(m_histo)+row_i));
      return a_time1-a_time0;
    }

    bool isState(unsigned int row_i) const {
      assert( PixA::nRows(m_histo) > row_i );
      unsigned int bin_content= static_cast<unsigned int>( PixA::binContent(m_histo,
									    PixA::startColumn(m_histo)+BinVarType,
									    PixA::startRow(m_histo)+row_i));

      if ( bin_content < s_logVarType.size()) {
	return !(s_logVarType[bin_content].isValue());
      }
      return false;
    }

    float  value(unsigned int row_i, unsigned int mod_i) const {
      assert( PixA::nRows(m_histo) > row_i );
      assert( PixA::nColumns(m_histo) > NSpecialBins+mod_i );

      return PixA::binContent(m_histo,
			      PixA::startColumn(m_histo)+NSpecialBins+mod_i,
			      PixA::startRow(m_histo)+row_i);
    }

    ELogStatus state(unsigned int row_i, unsigned int mod_i) {

      assert( PixA::nRows(m_histo) > row_i );
      assert( PixA::nColumns(m_histo) > NSpecialBins+mod_i );

      ELogStatus log_status = static_cast<ELogStatus>( static_cast<unsigned int>(PixA::binContent(m_histo,
												  PixA::startColumn(m_histo)+NSpecialBins+mod_i,
												  PixA::startRow(m_histo)+row_i)));
      return log_status;
    }

    static const std::vector<VarInfo_t >   &logVarType()   { if (!s_initialised) init(); return s_logVarType;}
    static const std::vector<std::string > &logStatus()    { if (!s_initialised) init(); return s_logStatus;}
    static const std::vector<std::string > &logTag()       { if (!s_initialised) init(); return s_logTag;}
    static const std::vector<bool>   &logStatusSuccess()   { if (!s_initialised) init(); return s_logStatusSuccess;}
    static const std::vector<unsigned char> &logStatusPrecedence()  { if (!s_initialised) init(); return s_logStatusPrecedence;}
    static const std::vector<State_t> &logStatusMapping()  { if (!s_initialised) init(); return s_logStatusMapping;}

    const PixCon::Histo_t *m_histo;

    static bool s_initialised;
    static std::vector<VarInfo_t >   s_logVarType;
    static std::vector<std::string > s_logStatus;
    static std::vector<bool >        s_logStatusSuccess;
    static std::vector<unsigned char> s_logStatusPrecedence;
    static std::vector<State_t>       s_logStatusMapping;
    static std::vector<std::string > s_logTag;
    static std::string s_emptyString;

  };

  bool                                    SummaryHisto_t::s_initialised=false;
  std::vector<SummaryHisto_t::VarInfo_t > SummaryHisto_t::s_logVarType;
  std::vector<std::string >               SummaryHisto_t::s_logStatus;
  std::vector<bool >                      SummaryHisto_t::s_logStatusSuccess;
  std::vector<unsigned char>              SummaryHisto_t::s_logStatusPrecedence;
  std::vector<State_t>                    SummaryHisto_t::s_logStatusMapping;
  std::vector<std::string >               SummaryHisto_t::s_logTag;
  std::string                             SummaryHisto_t::s_emptyString;

  class SummaryHistoAnalyser : public ISummaryHistoAnalyser
  {
  public:
    SummaryHistoAnalyser(const std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data, 
			 const PixCon::CategoryList::Category &state_category,
			 const PixCon::CategoryList::Category &dcs_category)
      : m_calibrationDataManager(calibration_data),
	m_stateCategory(state_category),
	m_dcsCategory(dcs_category)
    {
    }

    void analyseHistogram(EMeasurementType measurement_type, SerialNumber_t serial_number, const std::string &rod_name, const PixCon::Histo_t *a_histo);
  protected:
    void combineStates(EValueConnType value_conn_type,
		       const std::string &conn_name,
		       EMeasurementType measurement_type,
		       SerialNumber_t serial_number,
		       const VarDef_t &var_def,
		       State_t a_state);

  private:
    VarDef_t addStatusVar(EValueConnType value_conn_type, const std::string &ct_status_var_name);
    VarDef_t addValueVar(const std::string &value_var, float min_val, float max_val, const std::string &unit);
    VarDef_t addValueVar(EValueConnType value_conn_type, const std::string &value_var, float min_val, float max_val, const std::string &unit);

    void combineStates(EValueConnType value_conn_type,
		       EMeasurementType measurement_type,
		       SerialNumber_t serial_number,
		       const VarDef_t &var_def,
		       const std::string &conn_name,
		       State_t a_state);

    MetaData_t::EStatus combineStatus(MetaData_t::EStatus dest_status, State_t a_state, const VarDef_t &var_def) {
      if (var_def.isStateWithOrWithoutHistory() && var_def.isExtendedType() ) {
	MetaData_t::EStatus new_status = MetaData_t::kOk;
	const ExtendedStateVarDef_t &extended_state_var = var_def.extendedStateDef();
	if ( !extended_state_var.indicatesSuccess( a_state ) ) {
	  if ( extended_state_var.missingState() ==  a_state  ) {
	    new_status = MetaData_t::kUnknown;
	  }
	  else {
	    new_status = MetaData_t::kFailed;
	  }
	}
	dest_status = MetaData_t::combineStatus(dest_status, new_status);
      }
      return dest_status;
    }

    std::shared_ptr<CalibrationDataManager> m_calibrationDataManager;
    PixCon::CategoryList::Category m_stateCategory;
    PixCon::CategoryList::Category m_dcsCategory;
  };


  VarDef_t SummaryHistoAnalyser::addStatusVar(EValueConnType value_conn_type, const std::string &ct_status_var_name)
  {
    // number of histograms
    try {
      return VarDefList::instance()->getVarDef(ct_status_var_name);
    }
    catch (UndefinedCalibrationVariable &err) {
      const std::vector<std::string> &log_status = SummaryHisto_t::logStatus();
      const std::vector<bool> &log_status_success = SummaryHisto_t::logStatusSuccess();
      const std::vector<unsigned char> &log_status_precedence = SummaryHisto_t::logStatusPrecedence();
      const std::vector<State_t> &log_status_mapping = SummaryHisto_t::logStatusMapping();
      VarDef_t var_def( VarDefList::instance()->createExtendedStateVarDef(ct_status_var_name, value_conn_type, log_status.size(), SummaryHisto_t::Unknown ) );
      if (var_def.isStateWithOrWithoutHistory() && var_def.isExtendedType()) {
	m_stateCategory.addVariable(ct_status_var_name);
	ExtendedStateVarDef_t &state_var_def = var_def.extendedStateDef();
	unsigned int var_i=0;
	assert( log_status.size() == log_status_success.size() && log_status_success.size() == log_status_precedence.size() );

	for (std::vector<std::string>::const_iterator name_iter = log_status.begin();
	     name_iter != log_status.end();
	     name_iter++, var_i++) {
	  state_var_def.addState(var_i, log_status_mapping[var_i], *name_iter, log_status_success[var_i], log_status_precedence[var_i]);
	}
      }
      return var_def;
    }
  }

  VarDef_t SummaryHistoAnalyser::addValueVar(const std::string &value_var, float min_val, float max_val, const std::string &unit)
  {
    return addValueVar(kAllConnTypes, value_var, min_val, max_val, unit);
  }

  VarDef_t SummaryHistoAnalyser::addValueVar(EValueConnType value_conn_type, const std::string &value_var, float min_val, float max_val, const std::string &unit)
  {
    try {
      return VarDefList::instance()->getVarDef(value_var);
    }
    catch (UndefinedCalibrationVariable &err) {
      VarDef_t var_def( VarDefList::instance()->createValueVarDef(value_var, value_conn_type, min_val, max_val, unit) );
      m_dcsCategory.addVariable(value_var);
      return var_def;
    }
  }

  void SummaryHistoAnalyser::combineStates(EValueConnType value_conn_type,
					   const std::string &conn_name,
					   EMeasurementType measurement_type,
					   SerialNumber_t serial_number,
					   const VarDef_t &var_def,
					   State_t a_state)
  {
    assert ( var_def.isStateWithOrWithoutHistory() );
    if (var_def.isExtendedType()) {
      CalibrationData::ConnObjDataList conn_obj_data ( m_calibrationDataManager->calibrationData().getConnObjectData(conn_name) );
      const ExtendedStateVarDef_t &extended_state_var = var_def.extendedStateDef();
      State_t dest_state = extended_state_var.missingState();
      try {
	dest_state = conn_obj_data.newestValue<State_t>(measurement_type, serial_number, var_def);
      }
      catch (CalibrationData::MissingValue &) {
      }

      try {
	// update state if the current state is "missing",
	// or the precedence of the new state is higher provided both, the new and the old state, indicate either 
	// success or not.

	if (   dest_state == extended_state_var.missingState() 
	       || (   extended_state_var.precedence( a_state ) >  extended_state_var.precedence( dest_state ) 
		      && a_state != extended_state_var.missingState() )) {

	  if ( !extended_state_var.indicatesSuccess( dest_state ) ||  extended_state_var.indicatesSuccess( a_state )) {
	    m_calibrationDataManager->calibrationData().addValue(conn_name, measurement_type, serial_number, var_def, a_state);
	  }
	}
      }
      catch (CalibrationData::MissingValue &){
      }

    }
    else {
      m_calibrationDataManager->calibrationData().addValue(conn_name, measurement_type, serial_number, var_def, a_state);
    }
  }


   void SummaryHistoAnalyser::analyseHistogram(EMeasurementType measurement_type, SerialNumber_t serial_number, const std::string &rod_name, const PixCon::Histo_t *a_histo) 
   {

     PixA::ConnectivityRef connectivity;
     try {
       connectivity = m_calibrationDataManager->connectivity(measurement_type,serial_number);
     }
     catch (MissingConnectivity &err) {
       std::cout << "ERROR [SummaryHistoAnalyser::analyseHistogram] While querying the connectivity for " << measurement_type << " " << serial_number  
		 << ", caught exception : " << err.what() << std::endl;
       return;
     }
     if (!connectivity) {
       std::cout << "ERROR [SummaryHistoAnalyser::analyseHistogram] No connectivity for " << measurement_type << " " << serial_number  << std::endl;
       return;
     }

     SummaryHisto_t summary_histo(a_histo);
     MetaData_t::EStatus rod_status = MetaData_t::kUnset;
     if (summary_histo && summary_histo.nRows()>0) {
       const PixA::Pp0List &pp0s=connectivity.pp0s(rod_name);
       PixA::Pp0List::const_iterator pp0_begin = pp0s.begin();
       PixA::Pp0List::const_iterator pp0_end = pp0s.end();

       unsigned int time0=summary_histo.logTime(0);

       std::map<std::string, bool>  time_vars;
       for (unsigned int row_i=0; row_i<summary_histo.nRows(); row_i++) {
	 const SummaryHisto_t::VarInfo_t * const var_info = summary_histo.logVarType(row_i);
	 if (var_info) {
	   std::pair< std::map<std::string, bool>::iterator,bool> 
	     ret = time_vars.insert(std::make_pair(var_info->name(),false));
	   if (!ret.second) {
	     ret.first->second=true;
	   }
	 }
       }
       bool enable_rod=false;

       Lock lock(m_calibrationDataManager->calibrationDataMutex());
       unsigned int status_module_mask=0;
       bool has_link_vars=false;
       bool has_time=false;
       for (unsigned int row_i=0; row_i<summary_histo.nRows(); row_i++) {
	 const SummaryHisto_t::VarInfo_t * const var_info = summary_histo.logVarType(row_i);
	 if (var_info) {
	   if (!has_time && var_info->varType()<SummaryHisto_t::NVarTypes && var_info->varType()!=SummaryHisto_t::GlobalVar ) {
	     time0=summary_histo.logTime(row_i);
	     has_time=true;
	   }

	   if (var_info->varType()==SummaryHisto_t::LinkVar) {
	     has_link_vars=true;
	   }
	   else if (summary_histo.logVarTypeId(row_i)==SummaryHisto_t::status_mod) {
	     if (summary_histo.isState(row_i)) {
	       unsigned int mask_i=1;
	       // to consider only linkvar modules fir which the status is not unknown
	       for (unsigned int mod_id=0; mod_id<32; mod_id++) {
		 State_t a_state = static_cast<State_t>(summary_histo.state(row_i, mod_id));
		 if (   static_cast<SummaryHisto_t::ELogStatus>(a_state) == SummaryHisto_t::Passed
			|| static_cast<SummaryHisto_t::ELogStatus>(a_state) == SummaryHisto_t::Failed) { 
		   status_module_mask |= mask_i;
		 }
		 mask_i<<=1;
	       }
	     }
	   }
	 }
       }

       for (unsigned int row_i=0; row_i<summary_histo.nRows(); row_i++) {
	 const SummaryHisto_t::VarInfo_t * const var_info = summary_histo.logVarType(row_i);
	 unsigned int module_mask = summary_histo.moduleMask(row_i);
	 unsigned int pp0_mask = summary_histo.pp0Mask(row_i);

	 if (var_info) {
	     std::map<std::string,bool>::const_iterator time_var_iter = time_vars.find(var_info->name());

	   double delta_t = static_cast<double>(summary_histo.logTime(row_i)) - static_cast<double>(time0 );
	   std::string var_name = var_info->name();
	   var_name +=  "_";
	   var_name += summary_histo.logTagName(row_i);
	   if (var_info->varType()==SummaryHisto_t::GlobalVar) {
	     var_name += "_global";
	   }

	   std::stringstream time_var_name;
	   time_var_name <<  "_" << var_name << "_t";
	   //	   if (delta_t>10) {
	     time_var_name << static_cast<int>(delta_t/10);
	     // 	   }
	     // 	   else {
	     // 	     var_name << 0 << "." << delta_t;
	     // 	   }
	     VarDef_t var_def( VarDef_t::invalid() );
	   if (var_info->isValue()) {
	     var_def = addValueVar(var_name,var_info->min(), var_info->max(),var_info->unit());
	     if (time_var_iter->second) {
	       addValueVar(time_var_name.str(),var_info->min(), var_info->max(),var_info->unit());
	     }
	   }
	   else {
	     EValueConnType value_conn_type = kRodValue;
	     if (var_info->varType()==SummaryHisto_t::ModVar || var_info->varType()==SummaryHisto_t::Pp0Var) {
	       value_conn_type = kModuleValue;
	     }
	     else if (var_info->varType()==SummaryHisto_t::LinkVar || var_info->varType()==SummaryHisto_t::GlobalVar) {
	       value_conn_type = kRodValue;
	     }

	     var_def = addStatusVar( value_conn_type, var_name);
	     if (time_var_iter->second) {
	       addStatusVar(value_conn_type, time_var_name.str());
	     }
	   }

	   //	   unsigned int mod_mask = summary_histo.moduleMask(row_i);
	   //	   unsigned int pp0_mask = summary_histo.pp0Mask(row_i);

	   if ( !has_link_vars && var_info->varType()==SummaryHisto_t::ModVar) {

	     for (PixA::Pp0List::const_iterator pp0_iter=pp0_begin;
		  pp0_iter != pp0_end; // may be slightly inefficient unless the compiler is smart enough.
		  ++pp0_iter) {
	       //	       unsigned int pp0_slot = pp0_iter.slot();
	       // it may be more efficient to call modulesBegin and modulesEnd only once:
	       PixA::ModuleList::const_iterator modules_begin=PixA::modulesBegin(pp0_iter);
	       PixA::ModuleList::const_iterator modules_end=PixA::modulesEnd(pp0_iter);
	       for (PixA::ModuleList::const_iterator module_iter=modules_begin;
		    module_iter != modules_end;
		    ++module_iter) {
		   unsigned int mod_id = PixA::modId(module_iter);
		   if (module_mask & (1<<mod_id)) {
		     m_calibrationDataManager->setModuleEnable(PixA::connectivityName(module_iter),
							       measurement_type,
							       serial_number,
							       true);
		     enable_rod=true;
		   }
		 if (summary_histo.isState(row_i)) {
		   assert( !var_info->isValue());
		   State_t a_state = static_cast<State_t>(summary_histo.state(row_i, mod_id));
		   // @todo defin state var
		   combineStates(kModuleValue,
				 PixA::connectivityName(module_iter),
				 measurement_type,
				 serial_number,
				 var_def,
				 a_state);

		   // only consider enabled modules for the combined status
		   if (module_mask & (1<<mod_id)) {
		     rod_status = combineStatus(rod_status, a_state, var_def);
		   }

		   if (time_var_iter->second) {
		     m_calibrationDataManager->calibrationData().addValue(kModuleValue,
									  PixA::connectivityName(module_iter),
									  measurement_type,
									  serial_number,
									  time_var_name.str(),
									  a_state);
		   }

		 }
		 else {
		   assert( var_info->isValue());
		   float a_value = summary_histo.value(row_i, mod_id);
		   m_calibrationDataManager->calibrationData().addValue(kModuleValue,
									PixA::connectivityName(module_iter),
									measurement_type,
									serial_number,
									var_name,
									a_value);

		   if (time_var_iter->second) {
		     m_calibrationDataManager->calibrationData().addValue(kModuleValue,
									  PixA::connectivityName(module_iter),
									  measurement_type,
									  serial_number,
									  time_var_name.str(),
									  a_value);
		   }
		 }
	       }
	     }
	   }
	   else if (var_info->varType()==SummaryHisto_t::LinkVar || (var_info->varType()==SummaryHisto_t::ModVar && has_link_vars)) {
	     // just get the enable states to find out whether the ROD should be enabled.
	     // Other than that link vars are ignored.

	     // only do something for extended states
// 	     if (summary_histo.isState(row_i)) {

// 	       if (var_def.isExtendedType()) {
// 		 // combine the module status into a rod status

// 		 CalibrationData::ConnObjDataList conn_obj_data ( m_calibrationDataManager->calibrationData().getConnObjectData(rod_name) );
// 		 const ExtendedStateVarDef_t &extended_state_var = var_def.extendedStateDef();
// 		 State_t rod_state = extended_state_var.missingState();

// 		 // initialise with highest precedence successful state.
// 		 for (unsigned int state_i =0; state_i<extended_state_var.nStates(); state_i++) {
// 		   if (extended_state_var.indicatesSuccess(state_i) && 
// 		       (   !extended_state_var.indicatesSuccess(rod_state) 
// 			   || extended_state_var.precedence(state_i) > extended_state_var.precedence(rod_state))) {
// 		     rod_state = state_i;
// 		   }
// 		 }

// 		 // now find lowest precedence non-successful state of all enabled modules
	     unsigned int mask_i=1;
	     for (unsigned int mod_id=0; mod_id<32; mod_id++) {
	       if ((mod_id % 8)!=7 && ((module_mask & mask_i) || (status_module_mask & mask_i))) {
		 enable_rod=true;

		 std::stringstream mod_extension;
		 mod_extension << "_mod" << std::setfill('0') << std::setw(2) << mod_id;


		 if (summary_histo.isState(row_i)) {
		   assert( !var_info->isValue());
		   State_t a_state = static_cast<State_t>(summary_histo.state(row_i, mod_id));

		   addStatusVar( kRodValue, var_name+mod_extension.str());
		   if (time_var_iter->second) {
		     addStatusVar(kRodValue, time_var_name.str()+mod_extension.str());
		   }

		   // 		       // @todo defin state var
		   // 		       combineStates(kModuleValue,
		   // 				     PixA::connectivityName(module_iter),
		   // 				     measurement_type,
		   // 				     serial_number,
		   // 				     var_def,
		   // 				     a_state);

		   m_calibrationDataManager->calibrationData().addValue(kRodValue,
									rod_name,
									measurement_type,
									serial_number,
									var_name+mod_extension.str(),
									a_state);

		   if (time_var_iter->second) {
		     m_calibrationDataManager->calibrationData().addValue(kRodValue,
									  rod_name,
									  measurement_type,
									  serial_number,
									  time_var_name.str()+mod_extension.str(),
									  a_state);
		   }
		 }
		 else {
		   assert( var_info->isValue());
		   float a_value = summary_histo.value(row_i, mod_id);

		   var_def = addValueVar(var_name+mod_extension.str(),var_info->min(), var_info->max(),var_info->unit());
		   if (time_var_iter->second) {
		     addValueVar(time_var_name.str()+mod_extension.str(),var_info->min(), var_info->max(),var_info->unit());
		   }

		   m_calibrationDataManager->calibrationData().addValue(kRodValue,
									rod_name,
									measurement_type,
									serial_number,
									var_name+mod_extension.str(),
									a_value);

		   if (time_var_iter->second) {
		     m_calibrationDataManager->calibrationData().addValue(kRodValue,
									  rod_name,
									  measurement_type,
									  serial_number,
									  time_var_name.str()+mod_extension.str(),
									  a_value);
		   }
		 }


		 // 		     State_t a_state = static_cast<State_t>(summary_histo.state(row_i, mod_id));
		 // 		     if (extended_state_var.precedence(a_state)< extended_state_var.precedence(rod_state)
		 // 			 || (!extended_state_var.indicatesSuccess(a_state) && extended_state_var.indicatesSuccess(rod_state))) {
		 // 		       rod_state = a_state;
		 // 		     }
	       }
	       mask_i<<=1;
	     }

// 		 m_calibrationDataManager->calibrationData().addValue(kRodValue,
// 								      rod_name,
// 								      measurement_type,
// 								      serial_number,
// 								      var_name,
// 								      rod_state);

// 		 if (time_var_iter->second) {
// 		   m_calibrationDataManager->calibrationData().addValue(kRodValue,
// 									rod_name,
// 									measurement_type,
// 									serial_number,
// 									time_var_name.str(),
// 									rod_state);
// 		 }
// 	       }
// 	     }

	   }
	   else if (var_info->varType()==SummaryHisto_t::Pp0Var) {
	     for (PixA::Pp0List::const_iterator pp0_iter=pp0_begin;
		  pp0_iter != pp0_end; // may be slightly inefficient unless the compiler is smart enough.
		  ++pp0_iter) {
	       unsigned int pp0_slot = pp0_iter.slot();

	       if (summary_histo.isState(row_i)) {
		 assert(!var_info->isValue());
		 State_t pp0_state = summary_histo.state(row_i, pp0_slot);
		 PixA::ModuleList::const_iterator modules_begin=PixA::modulesBegin(pp0_iter);
		 PixA::ModuleList::const_iterator modules_end=PixA::modulesEnd(pp0_iter);
		 for (PixA::ModuleList::const_iterator module_iter=modules_begin;
		      module_iter != modules_end;
		      ++module_iter) {
		   // @todo set to Pp0Value

		   combineStates(kModuleValue,
				 PixA::connectivityName(module_iter),
				 measurement_type,
				 serial_number,
				 var_def,
				 pp0_state);

		   // only consider enabled modules for the combined status
		   if (pp0_mask & (1<<pp0_slot)) {
		     enable_rod=true;
		     rod_status = combineStatus(rod_status, pp0_state, var_def);
		   }

		   if (time_var_iter->second) {
		     m_calibrationDataManager->calibrationData().addValue(kModuleValue,
									  PixA::connectivityName(module_iter),
									  measurement_type,
									  serial_number,
									  time_var_name.str(),
									  pp0_state);
		   }

		 }
	       }
	       else {
		 assert(var_info->isValue());
		 float pp0_value = summary_histo.value(row_i, pp0_slot);
		 // it may be more efficient to call modulesBegin and modulesEnd only once:
		 PixA::ModuleList::const_iterator modules_begin=PixA::modulesBegin(pp0_iter);
		 PixA::ModuleList::const_iterator modules_end=PixA::modulesEnd(pp0_iter);
		 for (PixA::ModuleList::const_iterator module_iter=modules_begin;
		      module_iter != modules_end;
		      ++module_iter) {
		   // @todo set to Pp0Value

		   unsigned int mod_id = PixA::modId(module_iter);
		   if (module_mask & (1<<mod_id)) {
		     enable_rod=true;
		     m_calibrationDataManager->setModuleEnable(PixA::connectivityName(module_iter),
							       measurement_type,
							       serial_number,
							       true);
		   }

		   m_calibrationDataManager->calibrationData().addValue(kModuleValue,
									PixA::connectivityName(module_iter),
									measurement_type,
									serial_number,
									var_name,
									pp0_value);

		   if (time_var_iter->second) {
		     m_calibrationDataManager->calibrationData().addValue(kModuleValue,
									  PixA::connectivityName(module_iter),
									  measurement_type,
									  serial_number,
									  time_var_name.str(),
									  pp0_value);
		   }
		 }
	       }
	     }
	   }
	   else if (var_info->varType()==SummaryHisto_t::GlobalVar) {
	     if (summary_histo.isState(row_i)) {
	       assert( !var_info->isValue());
	       State_t global_state = static_cast<State_t>(summary_histo.state(row_i, 0));
	       MetaData_t::EStatus  temp=MetaData_t::kUnset;
	       temp = combineStatus(temp, global_state, var_def);
	       if (rod_status!=MetaData_t::kUnset) {
		 if (rod_status != temp ) {
		   std::cout << "WARNING [SummaryHistoAnalyser::analyseHistogram] histogram global status and determined status do not agree : " 
			     << rod_status << " != " << temp
			     << std::endl;
		 }
	       }

	       if (temp != MetaData_t::kUnset) {
		 rod_status = temp;
	       }

 	       m_calibrationDataManager->calibrationData().addValue(kRodValue,
 								    rod_name,
 								    measurement_type,
 								    serial_number,
 								    var_name,
 								    global_state);

 	       if (time_var_iter->second) {
 		 m_calibrationDataManager->calibrationData().addValue(kRodValue,
 								      rod_name,
 								      measurement_type,
 								      serial_number,
 								      time_var_name.str(),
 								      global_state);
 	       }
	     }
	     else {
	       std::cout << "ERROR [SummaryHistoAnalyser::analyseHistogram] Do not expect a global value (only states) : " 
			 << var_info->varType() << " in row " << row_i << "." << std::endl;
	     }
	   }
	   else {
	     std::cout << "ERROR [SummaryHistoAnalyser::analyseHistogram] Invalid variable type " << var_info->varType() << " in row " << row_i << "." << std::endl;
	   }
	 }
	 else {
	   std::cout << "ERROR [SummaryHistoAnalyser::analyseHistogram] Invalid type in row " << row_i << "." << std::endl;
	 }
       }

       if (enable_rod) {
	 m_calibrationDataManager->setRodEnable(rod_name,
						measurement_type,
						serial_number,
						true);
       }
     }

     const MetaData_t *meta_data =NULL;
     if (measurement_type == kScan) {
       meta_data = m_calibrationDataManager->scanMetaDataCatalogue().find(serial_number);
       if (!meta_data) {
	 std::cout << "ERROR [SummaryHistoAnalyser::analyseHistogram] No meta data for scan "
		   << PixCon::makeSerialNumberString( measurement_type, serial_number)
		   << std::endl;
       }
     }
     else if (measurement_type == kAnalysis) {
       meta_data = m_calibrationDataManager->scanMetaDataCatalogue().find(serial_number);
       if (!meta_data) {
	 std::cout << "ERROR [SummaryHistoAnalyser::analyseHistogram] No meta data for analysis "
		   << PixCon::makeSerialNumberString( measurement_type, serial_number)
		   << std::endl;
       }
     }
     if (meta_data) {
       MetaData_t::EStatus final_status = meta_data->status();
       MetaData_t::EStatus new_final_status = MetaData_t::combineStatus( final_status, rod_status);
       if (new_final_status != final_status) {
	 m_calibrationDataManager->updateStatus( measurement_type, serial_number, new_final_status);
       }
     }

   }


  class SummaryHistoAnalyserKit : public ISummaryHistoAnalyserKit 
  {
  public:
    ISummaryHistoAnalyser *create(const std::shared_ptr<PixCon::CategoryList> &m_categories,
				  const std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data) const
    {
      return new SummaryHistoAnalyser(calibration_data, 
				      m_categories->addCategory("Scan"), 
				      m_categories->addCategory("Dcs"));
    }
  };


  bool registerSummaryHistoAnalyser() {
    SummaryHistoAnalyserFactory::registerKit("/HistoSummary$",new SummaryHistoAnalyserKit);
    SummaryHistoAnalyserFactory::registerKit("/InlinkOutlink$",new SummaryHistoAnalyserKit);
    return true;
  }

  bool s_summaryHistoAnalyserRegistered = registerSummaryHistoAnalyser();
}
