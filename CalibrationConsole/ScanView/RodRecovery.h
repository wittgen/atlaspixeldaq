/**
 * @file RodRecovery.h
 * @brief Empty ROS buffer to recover busy ROD
 * @author Tayfun Ince <tayfun.ince@cern.ch>
 * @date 2011/04/13
 */

#ifndef _PixCon_RodRecovery_H_
#define _PixCon_RodRecovery_H_

#include <string>

namespace PixCon {

  class RodRecovery
  {
  public:
    /** Constructor */
    RodRecovery();
    
    /** Destructor */
    ~RodRecovery();

    bool initialize();
    int execute();
    std::string getRecoveryScriptName();
    
  private:
    std::string m_recoveryScriptName;

  };

}
#endif
