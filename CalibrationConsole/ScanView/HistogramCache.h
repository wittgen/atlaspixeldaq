// Dear emacs, this is -*-c++-*-
#ifndef _PixCon_HistogramCache_h_
#define _PixCon_HistogramCache_h_

#include "IHistogramProvider.h"
#include <memory>
#include <CalibrationDataTypes.h>

#include <assert.h>

#include <map>
#include <vector>
#include <Lock.h>

namespace PixCon {

  /** Multi dimensional array of histograms.
   */
  class HistoArray
  {
  protected:
    HistoArray(unsigned int level) : m_level(level), m_magic(87654321) { }
  public:
    HistoArray() : m_level(0), m_magic(87654321) {}
    ~HistoArray();

    /** Add a histogram at the location specified by the multidimensional index.
     * @param index the multi dimensional index.
     * @param histo_ptr pointer of the histogram.
     * The histogram array will claim ownership.
     */
    void addHisto(const Index_t &index, HistoPtr_t histo_ptr) {
      if (!histo_ptr) return;

      addHisto(index, 0, histo_ptr);
    }

    /** Get the histogram from the location specified by the multi dimensional index.
     * @param index the multi dimensional index.
     * @return pointer of the histogram or NULL if no histogram exists at the specified location.
     * The histogram is owned by the histogram array and must not be deleted e.g.
     * <pre>
     * // in case Histo_t is a ROOT TH1 :
     * HistoPtr_t h1 = array.hist(index);
     * h1->Draw();                               // ERROR: will segfault at some point
     * h1=static_cast<HistoPtr_t>(h1->Clone());  // this should work
     * </pre>
     */
    HistoPtr_t histo(const Index_t &index) const {
      if (index.size()>0) {
	return histo(index, 0);
      }
      else {
	return NULL;
      }
    }

  protected:

    void addHisto(const Index_t &index, unsigned int level, HistoPtr_t histo_ptr);

    HistoPtr_t histo(const Index_t &index, unsigned int level) const {
      assert(level<index.size());
      if (level+1==index.size()) {
	if (index[level] < m_histo.size()) return m_histo[ index[level] ];
      }
      else {
	if (m_child.size() <= index[level] || !m_child[ index[level] ]) return NULL;
	return m_child[ index[level] ]->histo(index, level+1);
      }
      return NULL;
    }

    std::vector<HistoPtr_t>   m_histo;
    std::vector<HistoArray *> m_child;
    unsigned int m_level;
    unsigned int m_magic;
  };

  class TopLevelHistoArray : public HistoArray
  {
  public:

    TopLevelHistoArray() {
      // initialise the info array with an unpossible value.
      invalidate();
    }

    void invalidate()
    {
      m_info.setMaxIndex(0,static_cast<unsigned int>(-1));
    }

    /** Return true if the array does not contain any histograms.
     * If the cache contains such an element the histogram array can
     * be considered as missing for the given connectivity object.
     */
    bool empty() const {
      return !hasValidInfo() && m_histo.empty() && m_child.empty();
    }

    /** Return the histogram information.
     * The histogram information contains the number of childs or histograms per level.
     */
    const HistoInfo_t &info() const {
      return m_info;
    }

    /** Return true if the histogram info is initialised.
     */
    bool hasValidInfo() const {
      return m_info.maxIndex(0)!=static_cast<unsigned int>(-1);
    }

    /** Set histogram info.
     * The histogram information contains the number of childs or histograms per level.
     */
    void setInfo(const HistoInfo_t &info) {
      m_info = info;
    }

    void addHisto(const Index_t &index, HistoPtr_t histo_ptr) {
      if (!histo_ptr) return;

      // paranoia checks
      updateInfo( index );
      HistoArray::addHisto(index,histo_ptr);

    }

    bool checkIndex(const Index_t &index) const {
      if (index.empty()) return false;

      if (hasValidInfo()) {
	if (index.size()>m_info.maxLevels()) return false;

	for (unsigned int level_i=0; level_i+1<index.size(); level_i++) {
	  if  ( index[level_i] >= m_info.maxIndex(level_i) ) return false;
	}
	if ( index.back() < m_info.minHistoIndex() || index.back()>= m_info.maxHistoIndex() ) return false;
      }
      return true;
    }

    void updateInfo(const Index_t &index) {
      if (index.empty() || index.size()>m_info.maxLevels()) return;

      for (unsigned int level_i=0; level_i<index.size()-1; level_i++) {

	if  ( index[level_i] >= m_info.maxIndex(level_i) ) {
	  m_info.setMaxIndex(level_i, index[level_i] + 1);
	}
      }
      if (index.back()>= m_info.maxHistoIndex()) {
	m_info.setMaxHistoIndex(index.back()+1);
      }
      if (index.back()< m_info.minHistoIndex()) {
	m_info.setMinHistoIndex(index.back());
      }
    }


  private:
    HistoInfo_t m_info;
  };

  class HistogramCacheEntry
  {
  public:
    HistogramCacheEntry() : m_level(0) {};

    ~HistogramCacheEntry() {  }

    TopLevelHistoArray *createHistoArray() {
      if (!m_histoArray.get()) {
	m_level=0;
	m_histoArray=std::make_shared<TopLevelHistoArray>();
      }
      return m_histoArray.get();
    }

    TopLevelHistoArray *histoArray()             { return m_histoArray.get(); }
    const TopLevelHistoArray *histoArray() const { return m_histoArray.get(); }

    void clear() {
      m_histoArray.reset();
    }

    bool isValid() const { return (!m_histoArray) ? false : true; }
 
   operator bool() const { return isValid(); }

    void use() {
      m_level = s_passes;
    }

    bool shallDelete() {
      return (m_level--<=0);
    }

  private:
    static const int s_passes = 2;

    int         m_level;
    std::shared_ptr<TopLevelHistoArray>   m_histoArray;
  };
 
  /** Cache for histograms
   */
  class HistogramCache
  {
  public:

    template <class T>
    class ConnItemCollection : protected std::vector<T>
    {
    public:
      ConnItemCollection()  { std::vector<T>::resize(kNHistogramConnItemTypes); m_filled.resize(kNHistogramConnItemTypes,false); }

      bool filled(unsigned int index) const { assert(index < kNHistogramConnItemTypes); return m_filled[index]; }
      void setInvalid(unsigned int index) const { assert(index < kNHistogramConnItemTypes); m_filled[index]=false; }

      T &operator[](unsigned int index)  { assert(index < kNHistogramConnItemTypes); m_filled[index]=true; return std::vector<T>::operator[](index); }
      const T &operator[](unsigned int index) const  { assert(index < kNHistogramConnItemTypes); return std::vector<T>::operator[](index); }

      typedef typename std::vector<T>::iterator             iterator;
      typedef typename std::vector<T>::const_iterator const_iterator;

      iterator  begin()             { return std::vector<T>::begin(); }
      const_iterator begin() const  { return std::vector<T>::begin(); }
      const_iterator end()   const  { return std::vector<T>::end(); }
      std::vector<bool> m_filled;
    };

    /** Create a histogram cache for the given histogram server.
     * @param server the histogram server.
     * The cache will claim ownership over the server i.e. the server must not be deleted.
     */
    HistogramCache(std::shared_ptr<IHistogramProvider> server);

    ~HistogramCache();

    /** clear the histogram cache for the given serial number.
     */
    void clearCache(EMeasurementType type, SerialNumber_t serial_number);

    //     /** Mark the histogram information as invalid.
    //      */
    //     void setInvalid(EMeasurementType type, SerialNumber_t serial_number, const std::string &connectivity_name, unsigned int histo_index);

    /** Invalidate the given cache entry.
     */
    void invalidate(EMeasurementType measurement_type,
		    SerialNumber_t serial_number,
		    EConnItemType conn_item_type,
		    const std::string &connectivity_name,
		    unsigned int histo_id);

    /** clear the entire histogram cache.
     */
    void clearCache();

    /** Clear all cache entries which have not been used recently.
    */
    void clearUnused();

    /** Clear cache and reset the histogram provider.
     */
    void reset();

    /** Get a histogram access key for the given histogram name and scan serial number.
     * @param measurement_type the type of the measurement : scan, analysis or current.
     * @param serial_number serial number of the scan
     * @param histogram_name name of the histogram
     * @return histogram access key or the result of @ref invalidIndex()
     * The histogram access key is only valid for the given serial number. The result is
     * undefined when using this key together with a different serial number.
     */
    unsigned int histogramIndex(EMeasurementType measurement_type,
				SerialNumber_t serial_number,
				EConnItemType conn_item_type,
				const std::string &histogram_name);

    /** Get a histogram of an histogram array with the given access key of a specific scan and module.
     * @param measurement_type the type of the measurement : scan, analysis or current.
     * @param serial_number the serial number of the scan.
     * @param connectivity_name the connectivity_name of the module.
     * @param histo_index the histogram acces key which is valid for only one serial number (@sa getHistogramIndex).
     * @param index multi diemensional index which describes the location in the histogram array.
     * @return pointer to the histogram.
     * The histogram is owned by the cache and must not be deleted.
     */
    const HistoPtr_t histogram(EMeasurementType measurement_type,
			       SerialNumber_t serial_number,
			       EConnItemType conn_item_type,
			       const std::string &connectivity_name,
			       unsigned int histo_index,
			       const Index_t &index);

    /** Request the names of all histograms assigned to the given serial number.
     * @param measurement_type the type of the measurement : scan, analysis or current.
     * @param serial_number the serial number of the scan.
     * @param conn_item_type the type of connectivity items for which the histogram list should be valid.
     * @return a pointer to a valid list of histograms.
     * It is guaranteed that the returned boost shared pointer points to a valid histogram name list.
     */
    std::shared_ptr< std::vector< std::string > > histogramList( EMeasurementType measurement_type,
								   SerialNumber_t serial_number,
								   EConnItemType conn_item_type);

    /** Merge the histogram list of the given ROD with the internal histogram list.
     * @param measurement_type the type of the measurement : scan, analysis or current.
     * @param serial_number the serial number of the scan.
     * @param conn_item_type the type of connectivity items for which the histogram list should be valid.
     * @param update histogram list for a particular ROD. 
     * @return a pointer to a valid list of histograms.
     * It is guaranteed that the returned boost shared pointer points to a valid histogram name list.
     */
    std::shared_ptr< std::vector< std::string > > updateHistogramList( EMeasurementType measurement_type,
									 SerialNumber_t serial_number,
									 EConnItemType conn_item_type,
									 const std::string &rod_name);


    /** Return the dimensions of the pix scan histo levels.
     * @param measurement_type the type of the measurement : scan, analysis or current.
     * @parem serial_number the serial number of the scan.
     * @param connectivity_name connectivity name of the module.
     * @param histo_name name of the histogram.
     * @param info  will be filled with the dimensions of the pix scan histo levels.
     * @param refresh if refresh is set to true the cached entry will be updated.
     */
    void numberOfHistograms(EMeasurementType measurement_type,
			    SerialNumber_t serial_number,
			    EConnItemType conn_item_type,
			    const std::string &connectivity_name,
			    unsigned int histo_index,
			    HistoInfo_t &info,
			    bool refresh=false);

    /** Return the invalid access key.
     */
    static unsigned int invalidIndex() {return s_unknownHistogram;}

  private:

    const std::string &histogramName(EMeasurementType type, SerialNumber_t serial_number, EConnItemType conn_item_type, unsigned int histo_id);

    std::shared_ptr<std::vector<std::string> > mergeHistogramLists( EMeasurementType measurement_type,
								      SerialNumber_t serial_number,
								      EConnItemType conn_item_type,
								      const std::vector<std::string> &new_histogram_list);


    class NameList_t
    {
    public:
      NameList_t() { init(); }

      std::shared_ptr< std::vector< std::string > > &createList() {
	assert( m_nameList.get() );
	if (m_nameList.use_count()>1) {
	  // need to create a new list
	  init();
	}
	assert( m_nameList.get());
	return m_nameList;
      }

      const std::shared_ptr< std::vector< std::string > > &list() const { assert( m_nameList.get()); return m_nameList; }

    private:
      void init() { m_nameList = std::make_shared< std::vector< std::string > >(); }

      std::shared_ptr< std::vector< std::string > > m_nameList;
    };

    std::map< SerialNumber_t, ConnItemCollection<NameList_t > > m_histogramList[kNMeasurements];
    static const unsigned int s_unknownHistogram = static_cast<unsigned int>(-1);
    Mutex m_histogramListMutex;


    typedef std::vector< HistogramCacheEntry >     HistoList_t;
    typedef std::map< std::string, HistoList_t >   ConnItemList_t;
    typedef std::map<SerialNumber_t, ConnItemCollection<ConnItemList_t> > Cache_t;

    Mutex m_cacheMutex;
    Cache_t m_cache[kNMeasurements];
    std::shared_ptr<IHistogramProvider> m_server;
    static std::shared_ptr< std::vector< std::string > >  s_emptyHistogramNameList;
    static std::string                             s_emptyString;

    bool m_verbose;
  };
}
#endif

