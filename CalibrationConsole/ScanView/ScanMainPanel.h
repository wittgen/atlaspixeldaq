#ifndef _PixCon_ScanMainPanel_h_
#define _PixCon_ScanMainPanel_h_

#include "MainPanel.h"
#include "UserMode.h"
#include "Lock.h"
#include <memory>
#include <ConfigWrapper/Connectivity.h>
#include <CalibrationData.h>
#include <ConnItem.h>

namespace PixA {
  class IVisualiser;
}

namespace PixCon {

  class HistogramCache;
  class HistogramView;
  class VisualiserList;
  class RodBufferView;
  class ObjectDataView;

  class ScanMainPanel : public MainPanel
  {
    Q_OBJECT

      enum EConfigVisualiser { kScanConfigVisualiser,
			       kModuleConfigVisualiser,
			       kModuleGroupConfigVisualiser,
			       kBocConfigVisualiser,
			       kRodConfigVisualiser,
			       kTimConfigVisualiser,
			       kLinkMapVisualiser,
			       kNConfigVisualiser};

  public:
    ScanMainPanel(std::shared_ptr<PixCon::UserMode>& user_mode,
		  const std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data,
		  const std::shared_ptr<PixCon::HistogramCache> &histogram_cache,
		  const std::shared_ptr<PixCon::CalibrationDataPalette> &palette,
		  const std::shared_ptr<PixCon::CategoryList> &categories,
		  const std::shared_ptr<PixCon::IContextMenu> &context_menu,
		  const std::shared_ptr<PixCon::IMetaDataUpdateHelper> &update_helper,
		  const std::shared_ptr<PixDet::ILogoFactory> &logo_factory,
		  const std::shared_ptr<VisualiserList> &visualiser_list,
		  QApplication &application,
		  QWidget* parent = 0);

    ~ScanMainPanel();

    public slots:
      /** Toggle the enable state of the given connectivity item. 
       * This will only change the "current" state.
       */
      virtual void toggleEnableState(PixCon::EConnItemType conn_item_type, const std::string &connectivity_name);


      void deleteHistogramView(const QString &view_name);
      void changeConnection(const QString &view_name);

      void showHistogram(PixCon::EConnItemType conn_item_type,
			 const std::string &connectivity_name,
			 EMeasurementType type,
			 SerialNumber_t serial_number,
			 const std::string &histogram_name,
			 unsigned int visualiser_option) {
	showHisto(conn_item_type, connectivity_name, type, serial_number, histogram_name, visualiser_option,false);
      }

      void createHistogramView(PixCon::EConnItemType conn_item_type,
			       const std::string &connectivity_name,
			       EMeasurementType type,
			       SerialNumber_t serial_number,
			       const std::string &histogram_name,
			       unsigned int visualiser_option) {
	std::cout << "DEBUG [ScanMainPanel::createHistogramView] ctor\n";
	showHisto(conn_item_type, connectivity_name, type, serial_number, histogram_name, visualiser_option,true);
      }

      void showScanConfiguration(EMeasurementType type,SerialNumber_t serial_number);


      /** Show the configuration of an analysis, classification and post-processing according to a given type.
       */
      void showAnalysisConfiguration(EMeasurementType type,SerialNumber_t serial_number, unsigned short class_i);

      void showConfiguration(PixCon::EConnItemType conn_item_type,
			     const std::string &connectivity_name,
			     EMeasurementType type,
			     SerialNumber_t serial_number)
      {
	switch (conn_item_type) {
	case kModuleItem:
	  showModuleConfiguration(conn_item_type, connectivity_name, type, serial_number);
	  break;
	case kPp0Item:
	  showBocConfiguration(conn_item_type, connectivity_name, type, serial_number);
	  break;
	case kRodItem:
	  showModuleGroupConfiguration(conn_item_type, connectivity_name, type, serial_number);
	  break;
	case kTimItem:
	  showTimConfiguration(conn_item_type, connectivity_name, type, serial_number);
	  break;
	default:
	  break;
	}
      }

      void showModuleConfiguration(PixCon::EConnItemType conn_item_type,
				   const std::string &connectivity_name,
				   EMeasurementType type,
				   SerialNumber_t serial_number)
      {
	showConfig(conn_item_type, connectivity_name, type, serial_number, kModuleConfigVisualiser);
      }


      void showTDACMap(PixCon::EConnItemType conn_item_type,
		       const std::string &connectivity_name,
		       EMeasurementType type,
		       SerialNumber_t serial_number,
		       unsigned int visualiser_option=0)
      {
	//	showConfig(conn_item_type, connectivity_name, type, serial_number, kTDACMapVisualiser);
	showHisto(conn_item_type, connectivity_name, type, serial_number, "TDAC",visualiser_option,false);
      }

      void showFDACMap(PixCon::EConnItemType conn_item_type,
		       const std::string &connectivity_name,
		       EMeasurementType type,
		       SerialNumber_t serial_number,
		       unsigned int visualiser_option=0)
      {
	//	showConfig(conn_item_type, connectivity_name, type, serial_number, kFDACMapVisualiser);
	showHisto(conn_item_type, connectivity_name, type, serial_number, "FDAC",visualiser_option,false);
      }

      void showBocConfiguration(PixCon::EConnItemType conn_item_type,
				const std::string &connectivity_name,
				EMeasurementType type,
				SerialNumber_t serial_number)
      {
	showConfig(conn_item_type, connectivity_name, type, serial_number, kBocConfigVisualiser);
      }

      void showRodConfiguration(PixCon::EConnItemType conn_item_type,
				const std::string &connectivity_name,
				EMeasurementType type,
				SerialNumber_t serial_number)
      {
	showConfig(conn_item_type, connectivity_name, type, serial_number, kRodConfigVisualiser);
      }

      void showModuleGroupConfiguration(PixCon::EConnItemType conn_item_type,
					const std::string &connectivity_name,
					EMeasurementType type,
					SerialNumber_t serial_number)
      {
	showConfig(conn_item_type, connectivity_name, type, serial_number, kModuleGroupConfigVisualiser);
      }

      void showLinkConfiguration(PixCon::EConnItemType conn_item_type,
				 const std::string &connectivity_name,
				 EMeasurementType type,
				 SerialNumber_t serial_number)
      {
	showConfig(conn_item_type, connectivity_name, type, serial_number, kLinkMapVisualiser);
      }

      void showTimConfiguration(PixCon::EConnItemType conn_item_type,
				const std::string &connectivity_name,
				EMeasurementType type,
				SerialNumber_t serial_number)
      {
	showConfig(conn_item_type, connectivity_name, type, serial_number, kTimConfigVisualiser);
      }

      void removeMeasurement(EMeasurementType measurement_type, SerialNumber_t serial_number);
      void showRodBuffer(PixCon::EConnItemType conn_item_type,
			 const std::string &connectivity_name,
			 EMeasurementType type,
			 SerialNumber_t serial_number);
      void showObjectData(PixCon::EConnItemType conn_item_type,
			  const std::string &connectivity_name,
			  EMeasurementType type,
			  SerialNumber_t serial_number);

      void showHistory(PixCon::EConnItemType conn_item_type,
		       const std::string &connectivity_name,
		       EMeasurementType type,
		       SerialNumber_t serial_number,
		       VarId_t);
      void autoDisable(PixCon::EConnItemType conn_item_type,
		       const std::string &connectivity_name,
		       EMeasurementType type,
		       SerialNumber_t serial_number,
		       const std::string &partition);
      void recoverRod (PixCon::EConnItemType conn_item_type,
		       const std::string &connectivity_name,
		       EMeasurementType type,
		       SerialNumber_t serial_number,
		       const std::string &partition);
      void QSReconfig (PixCon::EConnItemType conn_item_type,
		       const std::string &connectivity_name,
		       EMeasurementType type,
		       SerialNumber_t serial_number,
		       const std::string &partition);

  protected:
      std::shared_ptr<PixCon::CalibrationDataManager> calibrationData() { return m_calibrationData;}
      std::shared_ptr<HistogramCache> histogramCache()                  { return m_histogramCache;}


    private:

      void showHisto(PixCon::EConnItemType conn_item_type,
			 const std::string &connectivity_name,
			 EMeasurementType type,
			 SerialNumber_t serial_number,
			 const std::string &histogram_name,
			 unsigned int visualiser_option,
			 bool create_new_view);

      void showConfig(PixCon::EConnItemType conn_item_type,
			     const std::string &connectivity_name,
			     EMeasurementType type,
			     SerialNumber_t serial_number,
			     EConfigVisualiser visualiser);
      bool getParStoplessRecovery(PixCon::EConnItemType conn_type, const std::string &connectivity_name,EMeasurementType type, SerialNumber_t serial_number, const std::string &partition, std::string &partitionDT ,std::string &rodName, std::string &item);

      std::shared_ptr<PixCon::CalibrationDataManager> m_calibrationData;
      std::shared_ptr<HistogramCache>                 m_histogramCache;
      std::shared_ptr<VisualiserList>                 m_visualiserList;
      std::shared_ptr<PixCon::CategoryList>           m_categories;
      std::shared_ptr<PixCon::CalibrationDataPalette> m_palette;
      std::vector< HistogramView *  >                   m_histogramViewList;
      RodBufferView*                                    m_rodBufferView;
      ObjectDataView*                                   m_objectDataView;
      std::vector<std::shared_ptr< PixA::IVisualiser > > m_configVisualiser;
    };

  }
#endif
