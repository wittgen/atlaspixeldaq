/* Dear emacs, this is -*-c++-*- */
#ifndef _PixA_ModuleMapH2Factory_h_
#define _PixA_ModuleMapH2Factory_h_

#include <IModuleMapH2Factory.h>
#include <CalibrationDataTypes.h>
#include <memory>

namespace PixCon {
  class IBocEditor {
  public:
    virtual ~IBocEditor() {};
    virtual void changeRxSettings(const std::string &conn_name, unsigned int link, float rx_delay, float rx_threshold, SerialNumber_t reference) = 0;
    virtual std::pair<float, float> rxSettings(const std::string &conn_name, unsigned int link, SerialNumber_t scan_serial_number) = 0;
    virtual std::pair<float, float> analysisRxSettings(const std::string &conn_name, unsigned int link, SerialNumber_t scan_serial_number) = 0;

    virtual void showEditor() = 0;
  };
}

namespace PixA {

  class ModuleMapH2Factory : public IModuleMapH2Factory
  {
  public:
    ModuleMapH2Factory(const std::shared_ptr<PixCon::IBocEditor> &boc_editor);
    ModuleMapH2Factory();
    ~ModuleMapH2Factory();

    void setBocEditor(const std::shared_ptr<PixCon::IBocEditor> &boc_editor) { m_bocEditor=boc_editor; }

    TH2 *createMapH2(const TH2 *h2);

    TH2 *createMapH2(const TH2 *h2, SerialNumber_t, const std::string &, unsigned int) {
      return createMapH2(h2);
    }

    TH2 *createBocEditH2(const TH2 *h2, SerialNumber_t serial_number, const std::string &conn_name, unsigned int link);
  private:
    std::shared_ptr<PixCon::IBocEditor> m_bocEditor;
  };

}
#endif
