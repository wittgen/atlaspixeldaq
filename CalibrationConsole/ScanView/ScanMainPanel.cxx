#include "ScanMainPanel.h"
#include "CalibrationConsoleContextMenu.h"
#include "HistogramView.h"
#include "RodBufferView.h"
#include "ObjectDataView.h"
#include <ConfigWrapper/ConnectivityUtil.h>
#include <Visualiser/IVisualiser.h>
#include <Visualiser/VisualisationKitSubSet_t.h>
#include <Visualiser/VisualisationFactory.h>
#include <HistogramCacheDataProvider.h>
#include <MetaDataUtil.h>

#include <QSignalMapper>
#include <qmessagebox.h>
#include <QFuture>
#include <QtConcurrent/QtConcurrent>

#include <ConfigVisualisationKit.h>
#include <LinkVisualisationKit.h>
#include <ValueHistoryView.h>

// for the analysis configuration:
#include <BusyLoop.h>
#include <ConfigViewer.h>

#include "PixUtilities/PixISManager.h"
#include "PixUtilities/PixStoplessRecovery.h"


namespace PixCon {

  QSignalMapper *mapTo(QWidget *a, const char * member, const QString &a_name) {
    if (!a) return NULL;
    QSignalMapper *sigmap = new QSignalMapper(a);
    sigmap->setMapping(a,a_name);
    sigmap->connect(sigmap,SIGNAL(mapped(const QString&)),a,member);
    return sigmap;
  }

  ScanMainPanel::ScanMainPanel(std::shared_ptr<PixCon::UserMode>& user_mode,
			       const std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data,
			       const std::shared_ptr<PixCon::HistogramCache> &histogram_cache,
			       const std::shared_ptr<PixCon::CalibrationDataPalette> &palette,
			       const std::shared_ptr<PixCon::CategoryList> &categories,
			       const std::shared_ptr<PixCon::IContextMenu> &context_menu,
			       const std::shared_ptr<PixCon::IMetaDataUpdateHelper> &update_helper,
                  	       const std::shared_ptr<PixDet::ILogoFactory> &logo_factory,
			       const std::shared_ptr<VisualiserList> &visualiser_list,
			       QApplication &application,
			       QWidget* parent)
    : MainPanel(user_mode,
		calibration_data,
		palette, categories,
		context_menu,
		update_helper,
                logo_factory,
		application,
		parent),
      m_calibrationData(calibration_data),
      m_histogramCache(histogram_cache),
      m_visualiserList(visualiser_list),
      m_categories(categories),
      m_palette(palette)
  {
    assert( visualiser_list.get() );
    assert( histogram_cache.get() );
    assert( categories.get() );
    assert( palette.get() );
    assert( context_menu.get() );
    assert( calibration_data.get() );

    // are there already suitable visualisers ? If not register generic config visualiser
    registerConfigVisualiser(this,PixA::IVisualisationKit::normalPriority-1,true);
    registerLinkVisualiser(this,PixA::IVisualisationKit::normalPriority-1,true);

    CalibrationConsoleContextMenu *console_context_menu = dynamic_cast<CalibrationConsoleContextMenu *>(context_menu.get());
    if (console_context_menu) {
      connect( &console_context_menu->constextMenuSignals(),&CalibrationConsoleContextMenuSignals::toggleEnableState,
	       this, &ScanMainPanel::toggleEnableState );

      connect( &console_context_menu->constextMenuSignals(),&CalibrationConsoleContextMenuSignals::showHistogram,
	       this, &ScanMainPanel::showHistogram );

      connect( &console_context_menu->constextMenuSignals(),&CalibrationConsoleContextMenuSignals::showConfiguration,
	       this, &ScanMainPanel::showConfiguration);

      connect( &console_context_menu->constextMenuSignals(),&CalibrationConsoleContextMenuSignals::showBocConfiguration,
	       this, &ScanMainPanel::showBocConfiguration);

      connect( &console_context_menu->constextMenuSignals(),&CalibrationConsoleContextMenuSignals::showRodConfiguration,
	       this, &ScanMainPanel::showRodConfiguration );

      connect( &console_context_menu->constextMenuSignals(),&CalibrationConsoleContextMenuSignals::showLinkConfiguration,
	       this, &ScanMainPanel::showLinkConfiguration );

      connect( &console_context_menu->constextMenuSignals(),&CalibrationConsoleContextMenuSignals::showTimConfiguration,
	       this, &ScanMainPanel::showTimConfiguration);

      connect( &console_context_menu->constextMenuSignals(),&CalibrationConsoleContextMenuSignals::showRodBuffer,
	       this, &ScanMainPanel::showRodBuffer );
      connect( &console_context_menu->constextMenuSignals(),&CalibrationConsoleContextMenuSignals::showObjectData,
	       this, &ScanMainPanel::showObjectData );

      connect( &console_context_menu->constextMenuSignals(),&CalibrationConsoleContextMenuSignals::showHistory,
	       this, &ScanMainPanel::showHistory );
      connect( &console_context_menu->constextMenuSignals(),&CalibrationConsoleContextMenuSignals::autoDisable,
	       this, &ScanMainPanel::autoDisable );
      connect( &console_context_menu->constextMenuSignals(),&CalibrationConsoleContextMenuSignals::recoverRod,
	       this, &ScanMainPanel::recoverRod );
      connect( &console_context_menu->constextMenuSignals(),&CalibrationConsoleContextMenuSignals::QSReconfig,
	       this, &ScanMainPanel::QSReconfig );
      connect( &console_context_menu->constextMenuSignals(),&CalibrationConsoleContextMenuSignals::showModuleListView,
	       this, &ScanMainPanel::showModuleListView );
    }


    std::vector< std::pair< EConfigVisualiser, std::string > > search_visualiser_list;
    search_visualiser_list.push_back( std::make_pair(kScanConfigVisualiser, std::string("Scan_cfg")) );
    search_visualiser_list.push_back( std::make_pair(kModuleConfigVisualiser, std::string("Module_cfg")) );
    search_visualiser_list.push_back( std::make_pair(kModuleGroupConfigVisualiser, std::string("ModuleGroup_cfg")) );
    search_visualiser_list.push_back( std::make_pair(kBocConfigVisualiser, std::string("Boc_cfg")) );
    search_visualiser_list.push_back( std::make_pair(kRodConfigVisualiser, std::string("Rod_cfg")) );
    search_visualiser_list.push_back( std::make_pair(kTimConfigVisualiser, std::string("Tim_cfg")) );
    search_visualiser_list.push_back( std::make_pair(kLinkMapVisualiser, std::string("Link_map")) );

    m_configVisualiser.resize(kNConfigVisualiser);

    std::vector<std::string>       empty_folder_list;
    PixA::VisualisationKitSubSet_t config_display_kits;

    for ( auto const & visualiser_pair: search_visualiser_list ) {
      config_display_kits.clear();
      PixA::VisualisationFactory::getKits(visualiser_pair.second, empty_folder_list, config_display_kits);
      m_configVisualiser[visualiser_pair.first]
	= std::shared_ptr<PixA::IVisualiser>(PixA::VisualisationFactory::visualiser(config_display_kits,
										      visualiser_pair.second,
										      empty_folder_list));
      std::cout << "DEBUG [ScanMainPanel::ctor] config visualiser " << m_configVisualiser[visualiser_pair.first]->name() << " for " 
		<< visualiser_pair.second << std::endl;
    }
  }

  ScanMainPanel::~ScanMainPanel()
  {
  }

  void ScanMainPanel::toggleEnableState(PixCon::EConnItemType conn_item_type, const std::string &connectivity_name) 
  {
    PixCon::Lock lock( m_calibrationData->calibrationDataMutex() );
    if (conn_item_type == kPp0Item) {

      // get enable state of ROD
      // if ROD disabled do nothing unless a pp0 conn object exist. Then disable pp0 and parent of modules
      // have PP0 conn object -> disable pp0
      // else check whether modules are disabled or enabled
      // if all modules are enabled then disable PP0
      // else enable pp0

      // @todo should also enable ROD if necessary?


      PixA::ConnectivityRef conn = m_calibrationData->connectivity(kCurrent, 0);
      if (!conn) return;
      PixA::ModuleList module_list=conn.modules(connectivity_name);

      bool rod_is_enabled=true;
      if (module_list.hasParent()) {
	PixA::Pp0List pp0_list(module_list.parent());
	try {
	  CalibrationData::ConnObjDataList
	    conn_object_data( m_calibrationData->calibrationData().getConnObjectData( pp0_list.location() ) );
	  rod_is_enabled = conn_object_data.enableState(kCurrent, 0).enabled();
	}
	catch (CalibrationData::MissingConnObject &err) {
	}
      }

      PixA::ModuleList::const_iterator modules_begin = module_list.begin();
      PixA::ModuleList::const_iterator modules_end = module_list.end();

      bool pp0_enabled=true;
      //bool has_pp0;
      //bool changed=false;
      try {
	CalibrationData::ConnObjDataList
	  conn_object_data( m_calibrationData->calibrationData().getConnObjectData( connectivity_name ) );
	CalibrationData::EnableState_t &state = conn_object_data.enableState(kCurrent, 0);
	//has_pp0=true;
	if (state.isInConnectivity() && state.isInConfiguration()) {
	  bool old_enabled_parent = state.enabled();
	  // @todo warn if parent is not enabled.

	  bool new_state = !state.enabled();
	  state.setEnabled( new_state  );

          std::cout << "INFO [ScanMainPanel::toggleEnableState] toggle  enable of "
		    << connectivity_name
		    << old_enabled_parent << " -> " << new_state
		    << std::endl;

	  // @todo PP0 conn is not yet defined.
	  //updateConnItem(connectivity_name, kPp0Item, kCurrent, 0);
	  pp0_enabled = new_state;
	  //changed = true;

	}
	else {
	  // if not in configuration or connectivity do not do anything.
	  return;
	}
      }
      catch (CalibrationData::MissingConnObject &err1) {
	//has_pp0=false;


	// if the rod is not enabled then nothing will change
	if (!rod_is_enabled) return;

	// since there is no PP0 connectivity object
	// consider pp0 to be enabled if at least one module is enabled
	// consider pp0 disabled if all modules are disabled.

	pp0_enabled=true; // new pp0 state
	for (PixA::ModuleList::const_iterator module_iter = modules_begin;
	     module_iter != modules_end;
	     ++module_iter) {

	  try {
	    CalibrationData::ConnObjDataList
	      conn_object_data( m_calibrationData->calibrationData().getConnObjectData( PixA::connectivityName(module_iter) ) );

	    CalibrationData::EnableState_t module_state = conn_object_data.enableState(kCurrent, 0);

	    if (module_state.isInConnectivity() && module_state.isInConfiguration()) {
	      if (module_state.enabled()) {
		pp0_enabled = false; // one module is enabled -> pp0 considered enabled -> disable pp0
	      }
	    }
	  }
	  catch (CalibrationData::MissingConnObject &err2) {
	  }
	}
      }

      for(PixA::ModuleList::const_iterator module_iter = modules_begin;
	  module_iter != modules_end;
	  ++module_iter) {

	// toggle enable state of Module if there is a Module connectivity object
	try {
	  CalibrationData::ConnObjDataList
	    conn_object_data( m_calibrationData->calibrationData().getConnObjectData( PixA::connectivityName(module_iter) ) );
	  CalibrationData::EnableState_t &module_state =conn_object_data.enableState(kCurrent, 0);
	  bool module_is_enabled = module_state.enabled();
	  module_state.setParentEnabled(rod_is_enabled && pp0_enabled);
	  if (module_is_enabled != module_state.enabled()) {
	    updateConnItem(kModuleItem, PixA::connectivityName(module_iter), kCurrent, 0);
	  }
	}
	catch (CalibrationData::MissingConnObject &err) {
	}
      }

    }
    else if ( conn_item_type == kModuleItem || conn_item_type == kRodItem) {

      // @todo should also enable ROD if necessary
      // and verify wether the ROD can be allocated
      // or issue a warning

      CalibrationData::ConnObjDataList conn_object_data_1( m_calibrationData->calibrationData().getConnObjectData(connectivity_name) );
      CalibrationData::EnableState_t &state = conn_object_data_1.enableState(kCurrent, 0);
      if (state.isInConnectivity() && state.isInConfiguration()) {

	bool new_state = !state.enabled();
	state.setEnabled( new_state  );
	updateConnItem(conn_item_type, connectivity_name, kCurrent, 0);

	//if ROD propagate parent enable to PP0s and modules
	if (conn_item_type == kRodItem) {

	  PixA::ConnectivityRef conn = m_calibrationData->connectivity(kCurrent, 0);
	  if (conn) {
	    PixA::Pp0List pp0_list=conn.pp0s(connectivity_name);

	    PixA::Pp0List::const_iterator pp0_begin = pp0_list.begin();
	    PixA::Pp0List::const_iterator pp0_end = pp0_list.end();
	    
	    for(PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
		pp0_iter != pp0_end;
		++pp0_iter) {

	      bool pp0_is_enabled=true;
	      // toggle enable state of PP0 if there is a PP0 connectivity object
	      try {
		CalibrationData::ConnObjDataList conn_object_data_2( m_calibrationData->calibrationData().getConnObjectData( PixA::connectivityName(pp0_iter) ) );
		CalibrationData::EnableState_t &pp0_state =conn_object_data_2.enableState(kCurrent, 0);
 		pp0_is_enabled = pp0_state.enabled();
		pp0_state.setParentEnabled(new_state);
		if (pp0_is_enabled != pp0_state.enabled()) {
		  updateConnItem(kPp0Item, PixA::connectivityName(pp0_iter), kCurrent, 0);
		  pp0_is_enabled = pp0_state.enabled();
		}
	      }
	      catch (CalibrationData::MissingConnObject &err) {
	      }

	      PixA::ModuleList::const_iterator modules_begin = PixA::modulesBegin(pp0_iter);
	      PixA::ModuleList::const_iterator modules_end = PixA::modulesEnd(pp0_iter);

	      for(PixA::ModuleList::const_iterator module_iter = modules_begin;
		  module_iter != modules_end;
		  ++module_iter) {

		// toggle enable state of Module if there is a Module connectivity object
		try {
		  CalibrationData::ConnObjDataList
		    conn_object_data_3( m_calibrationData->calibrationData().getConnObjectData( PixA::connectivityName(module_iter) ) );
		  CalibrationData::EnableState_t &module_state =conn_object_data_3.enableState(kCurrent, 0);
		  bool module_is_enabled = module_state.enabled();
		  module_state.setParentEnabled(new_state && pp0_is_enabled);
		  if (module_is_enabled != module_state.enabled()) {
		    updateConnItem(kModuleItem, PixA::connectivityName(module_iter), kCurrent, 0);
		  }
		}
		catch (CalibrationData::MissingConnObject &err) {
		}
	      }
	    }
	  }
	}
      }
    }
  }

  void ScanMainPanel::showHisto(PixCon::EConnItemType conn_item_type,
				    const std::string &connectivity_name,
				    EMeasurementType type,
				    SerialNumber_t serial_number,
				    const std::string &histogram_name,
				    unsigned int visualiser_option,
				    bool create_new_view)
  {
    std::cout << "DEBUG [ScanMainPanel::showHistogram] start. "<< time(0) << std::endl;
    if (create_new_view || m_histogramViewList.empty() || !m_histogramViewList.back()->isConnectedToMainPanel()) {

      std::cout << "DEBUG [ScanMainPanel::showHistogram] Create histogram view. " << time(0) << std::endl;
      m_histogramViewList.push_back(  new HistogramView(type,
							serial_number,
							m_calibrationData,
							m_histogramCache,
							m_visualiserList,
							createSelectionFactory(),
							conn_item_type,
							connectivity_name,
							histogram_name,
							this) ); // the parent widget (this) will takte of cleaning up.
      connect(m_histogramViewList.back(),SIGNAL(close(const QString &)),this, SLOT(deleteHistogramView(const QString &)));
      connect(m_histogramViewList.back(),SIGNAL(connectionToMainPanel(const QString &)),this, SLOT(changeConnection(const QString &)));
      m_histogramViewList.back()->setObjectName("HistogramView_"+QString::number(m_histogramViewList.size()));
      m_histogramViewList.back()->connectToMainPanel();
      m_histogramViewList.back()->show();
      std::cout << "DEBUG [ScanMainPanel::showHistogram] View is created and shown." << time(0) << std::endl;
    }
    if (m_histogramViewList.empty() || !m_histogramViewList.back()) return;
    std::cout << "DEBUG [ScanMainPanel::showHistogram] Show histogram " << serial_number << " / " << connectivity_name<< " " << histogram_name << ". " 
 	      << time(0) << std::endl;
    m_histogramViewList.back()->showHistogram(type,serial_number, conn_item_type, connectivity_name, histogram_name, visualiser_option);
    std::cout << "DEBUG [ScanMainPanel::showHistogram] Done. " << time(0) << std::endl;
    m_histogramViewList.back()->raise();
  }

  void ScanMainPanel::deleteHistogramView(const QString &view_name)
  {

    for (std::vector<HistogramView *>::iterator histo_view_iter = m_histogramViewList.begin();
	 histo_view_iter != m_histogramViewList.end();
	 histo_view_iter++) {
      if ( (*histo_view_iter)->objectName() == view_name) {
	(*histo_view_iter)->deleteLater();
	*histo_view_iter = nullptr;
	m_histogramViewList.erase(histo_view_iter);
	break;
      }
    }
  }

  void ScanMainPanel::changeConnection(const QString &view_name)
  {
    for (std::vector<HistogramView *>::iterator histo_view_iter = m_histogramViewList.begin();
	 histo_view_iter != m_histogramViewList.end();
	 histo_view_iter++) {
      if ( (*histo_view_iter)->objectName() == view_name) {
	if ((*histo_view_iter)->isConnectedToMainPanel()) {
	  HistogramView *temp =  *histo_view_iter;
	  if (temp != m_histogramViewList.back()) {
	    m_histogramViewList.back()->disconnectFromMainPanel();
	    temp->connectToMainPanel();
	    *histo_view_iter = NULL;
	    m_histogramViewList.erase(histo_view_iter);
	    m_histogramViewList.push_back(temp);
	  }
	}
	break;
      }
    }
  }

  void ScanMainPanel::showConfig(PixCon::EConnItemType conn_item_type,
					const std::string &connectivity_name,
					EMeasurementType measurement_type,
					SerialNumber_t serial_number,
					EConfigVisualiser visualiser)
  {
    std::cout << "INFO [ScanMainPanel::showConfiguration] conn_type connectivity_name m_measurementType m_currentSerialNumber "
	      << conn_item_type << " " << connectivity_name << " " << measurement_type << " " << serial_number << std::endl;
    std::cerr << "INFO [ScanMainPanel::visualise] Configuration " << visualiser << " for " << connectivity_name << " ." << std::endl;
    assert( visualiser < kNConfigVisualiser );
    if (!m_configVisualiser[visualiser].get()) {
      std::cerr << "ERROR [ScanMainPanel::visualise] No visualiser for config " << visualiser << "." << std::endl;
      return;
    }

    std::vector <PixA::ConfigHandle> vscan_config_handle;
    if (measurement_type!= kCurrent) {
      unsigned int scan_serial_number = m_calibrationData->scanSerialNumber(measurement_type,serial_number);
      try {
	vscan_config_handle = getScanConfigVector(scan_serial_number);
      }
      catch (std::runtime_error &err) {
	//@todo the MetaDataCatalogue should throw a dedicated exception for missing meta data.
      }
    }

    try {
      HistogramCacheDataProvider data_provider(m_histogramCache,
					       measurement_type,
					       serial_number,
					       m_calibrationData->connectivity(measurement_type, serial_number),
					       vscan_config_handle );

      std::vector< std::string > module_names;
      if (conn_item_type == kPp0Item) {
       	const PixA::ConnectivityRef &conn = m_calibrationData->connectivity(measurement_type,serial_number);
       	PixA::ModuleList modules = conn.modules(connectivity_name);
       	PixA::ModuleList::const_iterator begin_module = modules.begin();
       	PixA::ModuleList::const_iterator end_module = modules.end();
       	for (PixA::ModuleList::const_iterator module_iter = begin_module;
       	     module_iter != end_module;
	     ++module_iter) {
      	  module_names.push_back(PixA::connectivityName( module_iter) );
      	}
      }
      else {
       	module_names.push_back(connectivity_name);
      }

      PixA::Index_t dummy_index;
      m_configVisualiser[visualiser]->visualise( data_provider, module_names,  0 /*visualisation option*/, dummy_index, -1);
    }
    catch (std::exception &err) {
      std::cerr << "ERROR [ScanMainPanel::visualise] Caught std exception : " << err.what() << std::endl;
    }


  }

  void ScanMainPanel::showScanConfiguration(EMeasurementType type,SerialNumber_t serial_number)
  {
    showConfig(kRodItem, "FE-I3", type, serial_number, kScanConfigVisualiser);
    showConfig(kRodItem, "FE-I4", type, serial_number, kScanConfigVisualiser);
  }

  void ScanMainPanel::showAnalysisConfiguration(EMeasurementType measurement_type, SerialNumber_t serial_number, unsigned short class_i) {
    std::cout << "INFO [ScanMainPanel::showAnalysisConfiguration]  for " << makeSerialNumberString(kAnalysis, serial_number) << " class = " << class_i
		<< "." << std::endl;

    if (measurement_type != kAnalysis || class_i >= AnalysisMetaData_t::kNConfigs) return;
    AnalysisMetaData_t::EConfigTypes config_type = static_cast<AnalysisMetaData_t::EConfigTypes>(class_i);
    const AnalysisMetaData_t &analysis_meta_data = calibrationData()->analysisMetaData(serial_number);
    PixA::ConfigHandle config_handle(analysis_meta_data.configHandle(config_type)) ;
    std::string analysis_serial_number_string = makeSerialNumberString(kAnalysis, serial_number);
    if (!config_handle) {
      std::cout << "ERROR [ScanMainPanel::showAnalysisConfiguration] No configuration \"" << analysis_meta_data.name(config_type) << "\" for " << analysis_serial_number_string
		<< "." << std::endl;
      return;
    }
    PixA::ConfigHandle full_config_handle;

    QDialog *a_dialog=NULL;

    {
      std::stringstream message;
      message << " Get " <<  analysis_meta_data.name(config_type)  << " for " << analysis_serial_number_string << " .... ";
      std::unique_ptr<PixA::IBusyLoop> busy_loop(PixA::BusyLoopFactory::busyLoop(message.str()));

      full_config_handle = PixA::ConfigHandle( config_handle.ref().createFullCopy());
    }

    std::string a_title = analysis_meta_data.name(config_type);
    a_title += " (";
    a_title += analysis_serial_number_string;
    a_title += ")";

    std::string dialog_name = analysis_meta_data.name(config_type);
    dialog_name+="_";
    dialog_name+= analysis_serial_number_string;
    PixA::ConfigRef full_config_ref( full_config_handle.ref());
    a_dialog=new configViewer(a_title, full_config_ref.config(), this);//, dialog_name.c_str(),false);
    a_dialog->show();

  }


  void ScanMainPanel::showRodBuffer(PixCon::EConnItemType /*conn_item_type*/,
				    const std::string &connectivity_name,
				    EMeasurementType /*type*/,
				    SerialNumber_t /*serial_number*/)
  {
    m_rodBufferView = new RodBufferView(connectivity_name, this);
    m_rodBufferView->setWindowTitle(connectivity_name.c_str());
    //m_rodBufferView->fillListView(connectivity_name, "PixelInfr");
    m_rodBufferView->show();
  }

   void ScanMainPanel::showObjectData(PixCon::EConnItemType conn_item_type,
				      const std::string &connectivity_name,
				      EMeasurementType /*type*/,
				      SerialNumber_t /*serial_number*/)
   {
    m_objectDataView = new ObjectDataView(connectivity_name, conn_item_type, m_calibrationData, m_categories, m_palette, this);
    m_objectDataView->show();
  }

  void ScanMainPanel::showHistory(PixCon::EConnItemType /*conn_item_type*/,
				  const std::string &connectivity_name,
				  EMeasurementType type,
				  SerialNumber_t serial_number,
				  VarId_t id)
  {
    try {
      VarDef_t var_def = VarDefList::instance()->getVarDef( VarDefList::instance()->varName(id) );
      if (var_def.hasHistory()) {
	ValueHistoryView *value_history_view=ValueHistoryView::createHistoryView(slowUpdateList(),
										 std::shared_ptr<ISelection>(),
										 calibrationData(), 
										 type,
										 serial_number,
										 var_def,
										 connectivity_name,
										 true );
	if (value_history_view) {
	  value_history_view->show();
	}
      }
      else {
	std::cout << "ERROR [ScanMainPanel::showHistory] Variable " << VarDefList::instance()->varName(id) << " has no history." << std::endl;
      }
    }
    catch(...) {
      std::cout << "ERROR [ScanMainPanel::showHistory] Invalid variable." << std::endl;
    }
  }

  void ScanMainPanel::removeMeasurement(EMeasurementType measurement_type, SerialNumber_t serial_number) {
    m_histogramCache->clearCache(measurement_type, serial_number);
    MainPanel::removeMeasurement(measurement_type,serial_number);
  }


  void ScanMainPanel::autoDisable(PixCon::EConnItemType conn_item_type, const std::string &connectivity_name,EMeasurementType type, SerialNumber_t serial_number,const std::string &partition){

    std::string partitionDT, rodName, item;
    if(!getParStoplessRecovery(conn_item_type, connectivity_name, type, serial_number,partition, partitionDT, rodName, item))return;
    
    std::string message= "You have requested to DISABLE " + item + " from current data-taking.\n\n"
	    +"Do you really want to continue?";
    int answer = QMessageBox::question(nullptr, "AutoDisable", QString::fromStdString(message), QMessageBox::No, QMessageBox::Yes);

    if (answer == QMessageBox::Yes){

        QFuture<void> future = QtConcurrent::run([=]() {
          // Code in this block will run in another thread
          PixLib::PixStoplessRecovery::autoDisable(partition, std::string("DISABLE"), rodName, item);
        });
    } else std::cout<<"Autodisable "<<item<<" aborted"<<std::endl;

  }

  void ScanMainPanel::recoverRod(PixCon::EConnItemType conn_item_type, const std::string &connectivity_name,EMeasurementType type, SerialNumber_t serial_number, const std::string &partition){

    std::string partitionDT, rodName, item;
    if(!getParStoplessRecovery(conn_item_type, connectivity_name, type, serial_number,partition, partitionDT, rodName, item))return;

    std::string message = "You have requested to recover (DISABLE/CONFIG/ENABLE) " + rodName + " from current data-taking.\n\n"
	   + "Do you really want to continue the recovery of this ROD ?";
    int answer = QMessageBox::question(nullptr, "ROD Recovery", QString::fromStdString(message), QMessageBox::No, QMessageBox::Yes);

    if (answer == QMessageBox::Yes){
        QFuture<void> future = QtConcurrent::run([=]() {
          // Code in this block will run in another thread
          PixLib::PixStoplessRecovery::autoRecover(partition, rodName);
        });
    } else std::cout<<"AutoRecover "<<rodName<<" aborted"<<std::endl;
  
  
  }

  void ScanMainPanel::QSReconfig(PixCon::EConnItemType conn_item_type, const std::string &connectivity_name,EMeasurementType type, SerialNumber_t serial_number,const std::string &partition){

   std::string partitionDT, rodName, item;
   if(!getParStoplessRecovery(conn_item_type, connectivity_name, type, serial_number,partition, partitionDT, rodName, item))return;
  
    std::string message = "You have requested to reconfigure via QuickStatus " + item +"\n\n"
	    + "Do you really want to continue?";
    int answer = QMessageBox::question(nullptr, "QS reconfig", QString::fromStdString(message), QMessageBox::No, QMessageBox::Yes);
    
    if (answer == QMessageBox::Yes){

        QFuture<void> future = QtConcurrent::run([=]() {
          // Code in this block will run in another thread
          PixLib::PixISManager isM (partition.c_str(),"RunParams");
          std::string isString = rodName+"/QS_SINGLE_CFG/QuickStatusAction";
          isM.publish(isString.c_str(), item);
        });

    } else std::cout<<"QuickStatus reconfiguration "<<item<<" aborted"<<std::endl;
  
  }


bool ScanMainPanel::getParStoplessRecovery(PixCon::EConnItemType conn_type, const std::string &connectivity_name,EMeasurementType type, SerialNumber_t serial_number, const std::string &partition, std::string &partitionDT ,std::string &rodName, std::string &item){

    PixA::ConnectivityRef connRef = m_calibrationData->connectivity(type, serial_number);

    partitionDT="";

    std::unique_ptr<PixLib::PixISManager> m_is2 ( new PixLib::PixISManager(partition,"RunParams"));

    std::string allPP0Modules="";
    if(!m_is2){std::cout<<"Cannot get ISManager for "<<partition<<std::endl;return false;}
    rodName ="";
      try{
        if(conn_type ==kRodItem){
         rodName = connectivity_name;
        } else if (conn_type == kModuleItem) {
         const PixLib::ModuleConnectivity *module = connRef.findModule(connectivity_name);
      	  if (module) {
	   const PixLib::RodBocConnectivity *rod = PixA::connectedRod(module);
	     if (rod) {
	     rodName = PixA::connectivityName(rod);
	     } else {std::cout<<"Cannot find ROD name for "<<connectivity_name<<std::endl;return false;}
      	  }
        } else if (conn_type == kPp0Item) {
          const PixLib::Pp0Connectivity *pp0 = connRef.findPp0(connectivity_name);
      	  if (pp0) {
	     PixA::ModuleList module_list=connRef.modules(connectivity_name);
	     PixA::ModuleList::const_iterator module_iter;

		for (module_iter = module_list.begin(); module_iter != module_list.end();++module_iter){
		 CalibrationData::ConnObjDataList conn_object_data( m_calibrationData->calibrationData().getConnObjectData( PixA::connectivityName(module_iter) ) );
                  CalibrationData::EnableState_t &module_state =conn_object_data.enableState(kCurrent, 0);
                    if(module_state.enabled())allPP0Modules +=PixA::connectivityName(module_iter)+",";
                      const PixLib::ModuleConnectivity *module = connRef.findModule(PixA::connectivityName(module_iter));
      	  		if (module) {
	   		const PixLib::RodBocConnectivity *rod = PixA::connectedRod(module);
	     		  if (rod) {
	     		   rodName = PixA::connectivityName(rod);
	     		  } else {std::cout<<"Cannot find ROD name for "<<connectivity_name<<std::endl;return false;}
		      }
      	       }
      	  }
        } else {
        std::cout<<"Object that you try to auto-disable "<<connectivity_name<<" is not a ROD, PP0 or Module"<<std::endl;return false;
        }

	std::vector<std::string> Rodis = m_is2->listVariables("*"+rodName+"/ALLOCATING-PARTITION");

	if(Rodis.empty()){std::cout<<rodName<<"/ALLOCATING-PARTITION not found in IS"<<std::endl;return false;}

	partitionDT = m_is2->read<std::string>(Rodis[0].c_str());
      } catch(...){
	std::cout << "ERROR [CalibrationConsoleContextMenu::showMenu] Exception caught while checking ROD allocation with " <<
		  ("*"+rodName+"/ALLOCATING-PARTITION") << "! Nothing to be disabled!" << std::endl;
	return false;
      }
    
    
    if(partitionDT=="NONE" || partitionDT=="" || partitionDT.find("PixelInfr")!= std::string::npos ){
      std::cout << "ERROR [CalibrationConsoleContextMenu::showMenu] ROD NOT ALLOCATED! Nothing to be disabled!" << std::endl;
      return false;
    }
    
    if(conn_type == kPp0Item && allPP0Modules == ""){
      std::cout << "ERROR Cannot find any module enabled for PP0 "<< connectivity_name<<"!" << std::endl;
      return false;
    }

      item=connectivity_name;
      if(conn_type == kPp0Item)item = allPP0Modules.substr(0, allPP0Modules.size()-1);//To remove last comma

      return true;
  
  }


}
