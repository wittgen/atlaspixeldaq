// Dear emacs, this is -*-c++-*-
#ifndef _PixCon_CalibrationConsoleContextmenu_h_
#define _PixCon_CalibrationConsoleContextmenu_h_

#include "DataSelectionSpecificContextMenu.h"
//#include <CalibrationDataManager.h>
#include <CalibrationDataTypes.h>

#include "ConnItemCollection.h"
#include "VisualiserList.h"

#include <vector>
#include <memory>

namespace PixCon {

  class VisualiserList;
  class VisualiserList_t;
  class HistogramCache;

  class CalibrationConsoleContextMenu;

  class CalibrationConsoleContextMenuSignals : public QObject
  {
    friend class CalibrationConsoleContextMenu;

    Q_OBJECT
  public:
    CalibrationConsoleContextMenuSignals(QObject * parent = 0 ) :  QObject(parent) {}

  signals:
    void toggleEnableState(PixCon::EConnItemType conn_item_type, const std::string &connectivity_name);
    void showHistogram(PixCon::EConnItemType conn_item_type,
		       const std::string &connectivity_name,
		       EMeasurementType type,
		       SerialNumber_t serial_number,
		       const std::string &histogram_name,
		       unsigned int visualiser_option);
    void showConfiguration(PixCon::EConnItemType conn_item_type, const std::string &connectivity_name, EMeasurementType type, SerialNumber_t serial_number);
    void showBocConfiguration(PixCon::EConnItemType conn_item_type, const std::string &connectivity_name, EMeasurementType type, SerialNumber_t serial_number);
    void showRodBuffer(PixCon::EConnItemType conn_item_type, const std::string &connectivity_name, EMeasurementType type, SerialNumber_t serial_number);
    void showRodConfiguration(PixCon::EConnItemType conn_item_type, const std::string &connectivity_name, EMeasurementType type, SerialNumber_t serial_number);
    void showTimConfiguration(PixCon::EConnItemType conn_item_type, const std::string &connectivity_name, EMeasurementType type, SerialNumber_t serial_number);
    void showLinkConfiguration(PixCon::EConnItemType conn_item_type, const std::string &connectivity_name, EMeasurementType type, SerialNumber_t serial_number);
    void showObjectData(PixCon::EConnItemType conn_item_type, const std::string &connectivity_name, EMeasurementType type, SerialNumber_t serial_number);
    void showHistory(PixCon::EConnItemType conn_item_type, const std::string &connectivity_name, EMeasurementType type, SerialNumber_t serial_number, VarId_t var_id);
    void autoDisable(PixCon::EConnItemType conn_item_type, const std::string &connectivity_name, EMeasurementType type, SerialNumber_t serial_number, std::string const & partition);
    void recoverRod (PixCon::EConnItemType conn_item_type, const std::string &connectivity_name, EMeasurementType type, SerialNumber_t serial_number, std::string const & partition);
    void QSReconfig (PixCon::EConnItemType conn_item_type, const std::string &connectivity_name, EMeasurementType type, SerialNumber_t serial_number, std::string const & partition);
    void showModuleListView();

  };

  class CalibrationConsoleContextMenu : public DataSelectionSpecificContextMenu
  {
    Q_OBJECT

    enum EMenuId {kTitle=0,
                  kEnable=1,
		  kValues=2,
		  kModuleConfiguration=3,
		  kTDACMap=4,
		  kFDACMap=5,
		  kModuleGroupConfiguration=6,
		  kRodConfiguration=7,
		  kBocConfiguration=8,
		  kTimConfiguration=9,
		  kLinkMap=10,
		  kHistogramPopup=11,
		  kRodBuffer=12,
		  kObjectData=13,
		  kCopyObjectName=14,
		  kShowHistory=15,
		  kPrintModules=16,
		  kDAQEnable=17,
		  kAutoDis=18,
		  kRodRecover=19,
		  kQSReconfig=20,
		  kHistogram=21 /*Must be the last in the list*/};

  protected:
    CalibrationConsoleContextMenu(const CalibrationConsoleContextMenu &context_menu) 
      : DataSelectionSpecificContextMenu(nullptr), 
      m_calibrationData(context_menu.m_calibrationData),
      m_partitionName(context_menu.m_partitionName),
      m_lastVisualisationChoice(context_menu.m_lastVisualisationChoice),
      m_histogramCache(context_menu.m_histogramCache),
      m_lastScanSerialNumber(context_menu.m_lastScanSerialNumber),
      m_measurementType(context_menu.m_measurementType),
      m_currentSerialNumber(context_menu.m_currentSerialNumber),
      m_currentVar( VarDef_t::invalid()),
      m_visualiserList(context_menu.m_visualiserList),
      m_signals(context_menu.m_signals)
    {
    }

  public:
    CalibrationConsoleContextMenu(const std::shared_ptr<CalibrationDataManager> &calibration_data,
				  const std::shared_ptr<HistogramCache> &histogram_cache,
				  const std::shared_ptr<VisualiserList> &visualiser_list,
				  std::string const & partition_name);

    //    void setHistogramList(const std::vector<std::string> &histogram_list) {
    //      m_histogramNameList = histogram_list;
    //      m_lastHistoChoice=0;
    //      m_lastVisualiserChoice=0;
    //    }

    //    ~CalibrationConsoleContextMenu();

    /** Action activated on double click.
     */
    void primaryAction(PixCon::EConnItemType conn_type, const std::string &connectivity_name) {
      if (conn_type < kNHistogramConnItemTypes) {
      if (!m_histoVisualiser[conn_type].isValid()) {
	updateHistogramList(conn_type);
      }
      if (!m_histoVisualiser[conn_type].histogramNameList().empty()) {
	//	if (m_lastHistoChoice >= m_histogramNameList.size()) {
	m_lastVisualisationChoice.reset();
	//	}
	signalShowHistogram(conn_type, connectivity_name);
      }
      }
    }

    /** Action activated on middle click.
     */
    void secondaryAction(PixCon::EConnItemType conn_type, const std::string &connectivity_name) {
      emit m_signals->showObjectData(conn_type, connectivity_name,m_measurementType, m_currentSerialNumber);
    }

    /** Show menu with all actions.
     */
    void showMenu(PixCon::EConnItemType conn_type, const std::string &connectivity_name);


    /** Should be called if the data selection has changed.
     */
    void dataSelectionChanged(const DataSelection &data_selection);

    DataSelectionSpecificContextMenu *clone() const {
      std::unique_ptr<CalibrationConsoleContextMenu> cloned_context_menu( new CalibrationConsoleContextMenu(*this) );
      //       cloned_context_menu->m_histogramNameList = m_histogramNameList ;
      //       cloned_context_menu->m_lastHistoChoice = m_lastHistoChoice;
      //       cloned_context_menu->m_visualiserList = m_visualiserList;
      return cloned_context_menu.release();
    }

    CalibrationConsoleContextMenuSignals &constextMenuSignals() { return *m_signals; }

    
  public slots:
    void printModules();
    void toggleEnableState();
    void copyClipboard();
    void showConfiguration();
    void showHistogramTdac();
    void showHistogramFdac();
    void showRodConfiguration();
    void showBocConfiguration();
    void showRodBuffer();
    void recoverRod();
    void autoDisable();
    void QSReconfig();
    void showTimConfiguration();
    void showLinkConfiguration();
    void showObjectData();
    void showHistory();
    void signalShowHistogram();
    void CheckCases();

  protected:
    const VisualiserList_t &visualiserList();

    void signalShowHistogram(PixCon::EConnItemType conn_type, const std::string &connectivity_name);

  private:

    std::shared_ptr<CalibrationDataManager> m_calibrationData;
    std::string m_partitionName;

    class VisualisationChoice_t {
    public:
      VisualisationChoice_t()
      { reset(); }

      VisualisationChoice_t(PixCon::EConnItemType histo_conn_item_type,
			    unsigned int histo_id,
			    unsigned int visualisation_option) 
	: m_connItemType(histo_conn_item_type),
	  m_histoId(histo_id),
	  m_visualisationOption(visualisation_option)
      { }

      void reset() {
	m_connItemType=kNConnItemTypes; // invalid connectivity item
	m_histoId = static_cast<unsigned int>(-1);
	m_visualisationOption = static_cast<unsigned int>(-1);
      }

      EConnItemType connItemType() const    { return m_connItemType; }
      unsigned int histoId() const          { return m_histoId; }
      unsigned int visualiserOption() const { return m_visualisationOption; }

    private:
      EConnItemType m_connItemType;
      unsigned int m_histoId;
      unsigned int m_visualisationOption;
    };

    VisualisationChoice_t                     m_lastVisualisationChoice;
    std::vector< VisualisationChoice_t >      m_histo_id_list;

    std::shared_ptr<HistogramCache>         m_histogramCache;
    SerialNumber_t                            m_lastScanSerialNumber;

    EMeasurementType                          m_measurementType;
    SerialNumber_t                            m_currentSerialNumber;
    VarDef_t                                  m_currentVar;

    std::shared_ptr<VisualiserList>         m_visualiserList;

    void updateHistogramList(PixCon::EConnItemType conn_type_i);

    class HistoVisualiser_t
    {
    public:
      HistoVisualiser_t() : m_valid(false) {}

      std::vector<std::string> &histogramNameList()             { return m_histogramNameList; }
      const std::vector<std::string> &histogramNameList() const { return m_histogramNameList; }

      void setScanVisualiserList(const VisualiserListRef_t &scan_visualiser_list) { m_scanVisualiserList = scan_visualiser_list; }
      VisualiserListRef_t &scanVisualiserList()             {return m_scanVisualiserList; }
      const VisualiserListRef_t &scanVisualiserList() const {return m_scanVisualiserList; }

      void setAnalysisVisualiserList(const VisualiserListRef_t &analysis_visualiser_list) { m_analysisVisualiserList = analysis_visualiser_list; }
      const VisualiserListRef_t &analysisVisualiserList() const {return m_analysisVisualiserList; }
      VisualiserListRef_t &analysisVisualiserList()             {return m_analysisVisualiserList; }

      bool isValid() const { return m_valid; }

      void invalidate() {
	m_scanVisualiserList= VisualiserListRef_t();
	m_analysisVisualiserList=VisualiserListRef_t();
	m_histogramNameList.clear();
	m_valid=false;
      }

    private:
      bool m_valid;
      std::vector<std::string> m_histogramNameList;
      VisualiserListRef_t      m_scanVisualiserList;
      VisualiserListRef_t      m_analysisVisualiserList;
    };


    ConnItemCollection<HistoVisualiser_t > m_histoVisualiser;
    ConnItemCollection<unsigned int>       m_nScanHistograms;

    std::shared_ptr<CalibrationConsoleContextMenuSignals> m_signals;
  };

}
#endif
