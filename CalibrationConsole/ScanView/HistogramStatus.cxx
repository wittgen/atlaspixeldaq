#include "HistogramStatus.h"
#include "DefaultColourCode.h"

namespace PixCon {

  const std::string HistogramStatus::s_histogramStateName("HISTOGRAMS");

  VarDef_t HistogramStatus::defineVar() {
    VarDef_t histogram_state_var = VarDef_t::invalid();
    // @todo read variable definition from db
    try {
      const VarDef_t var_def( VarDefList::instance()->getVarDef(s_histogramStateName) );
      if ( !var_def.isStateWithOrWithoutHistory() ) {
	std::cout << "ERROR [ScanStatusMonitor::ctor] variable " << s_histogramStateName << " already exists but it is not a state variable." << std::endl;
      }
      else {
	histogram_state_var=var_def;
      }
    }
    catch (UndefinedCalibrationVariable &) {
      VarDef_t var_def( VarDefList::instance()->createStateVarDef(s_histogramStateName, kAllConnTypes, kNStates) );
      if (var_def.isStateWithOrWithoutHistory() ) {
	//	scan_variables.addVariable(histogram_state_name);
	StateVarDef_t &state_var_def = var_def.stateDef();
	state_var_def.addState(kPresumablyDisabled,    DefaultColourCode::kOff,            "Disabled (guess)");
	state_var_def.addState(kNoneExpected,          DefaultColourCode::kConfigured,     "None expected.");
	state_var_def.addState(kOk,                    DefaultColourCode::kSuccess,        "Ok");
	state_var_def.addState(kOkButBelowExpectation, DefaultColourCode::kCloseToFailure, "Ok (< exp.)");
	state_var_def.addState(kOkButAboveExpectation, DefaultColourCode::kScanning,       "Ok (> exp.)"); //@todo colour ?
	state_var_def.addState(kMissingHistograms,     DefaultColourCode::kFailure,        "Missing");
	state_var_def.addState(kTooManyHistograms,     DefaultColourCode::kAlarm,          "Too Many");

	histogram_state_var=var_def;
      }
      else {
	std::cout << "ERROR [ScanStatusMonitor::ctor] variable " << s_histogramStateName << " already exists but it is not a state variable." << std::endl;
      }
    }
    return histogram_state_var;
  }

}
