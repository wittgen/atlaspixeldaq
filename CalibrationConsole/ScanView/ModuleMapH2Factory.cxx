#include "ModuleMapH2Factory.h"
#include <TH2.h>
#include <PixFe/PixGeometry.h>

#include <TPad.h>
#include <TMarker.h>
#include <iomanip>

namespace PixA {

  template <class TH2_class>
  class ModuleMapH2 : public TH2_class
  {
  public:
    ModuleMapH2() {}

    ModuleMapH2(const ModuleMapH2 &module_map_h2) : TH2_class(module_map_h2){}

    ModuleMapH2(const TH2_class &h2) : TH2_class(h2) {}
    char *GetObjectInfo(Int_t px, Int_t py) const {
      if (!gPad) return (char*)"";
      Double_t x  = gPad->PadtoX(gPad->AbsPixeltoX(px));
      Double_t y  = gPad->PadtoY(gPad->AbsPixeltoY(py));
      unsigned int binx   = TH2_class::fXaxis.FindFixBin(x);
      unsigned int biny   = TH2_class::fYaxis.FindFixBin(y);

      PixLib::PixGeometry geo(TH2_class::GetNbinsY(), TH2_class::GetNbinsX());

      //      if (TH2_class::GetNbinsX()>18 && TH2_class::GetNbinsY()>160) {
      if(geo.pixType()==PixLib::PixGeometry::FEI2_MODULE || geo.pixType()==PixLib::PixGeometry::FEI4_MODULE){
	//PixA::PixelCoord_t coord(static_cast<int>(TH2_class::fXaxis.GetBinCenter(binx)),static_cast<int>(TH2_class::fYaxis.GetBinCenter(biny)));
	PixLib::PixCoord coord = geo.coord(static_cast<int>(TH2_class::fXaxis.GetBinCenter(binx)),static_cast<int>(TH2_class::fYaxis.GetBinCenter(biny)));

	std::stringstream message;
	message << "FE=" << std::setw(2) << coord.chip()
		<< ", col=" << std::setw(2) << coord.col()
		<< ", row=" << std::setw(3) << coord.row()
		<< ",  content=" << TH2_class::GetBinContent(TH2_class::GetBin(binx,biny));
	m_currentInfo = message.str();
      }
      else {
	std::stringstream message;
	message << "col=" << std::setw(2) << static_cast<int>(TH2_class::fXaxis.GetBinCenter(binx))
		<< ", row=" << std::setw(3) << static_cast<int>(TH2_class::fYaxis.GetBinCenter(biny))
		<< ",  content=" << TH2_class::GetBinContent(TH2_class::GetBin(binx,biny));
	m_currentInfo = message.str();
      }
      return const_cast<char *>(m_currentInfo.c_str());
    }

  private:

    mutable std::string m_currentInfo;
  };


  template <class TH2_class>
  class BocEditorH2 : public ModuleMapH2<TH2_class>
  {
  public:
    BocEditorH2()
    {}

    BocEditorH2(const BocEditorH2 &boc_editor_h2) 
      : ModuleMapH2<TH2_class>(boc_editor_h2),
	m_bocEditor(boc_editor_h2.m_bocEditor),
	m_scanSerialNumber(boc_editor_h2.m_scanSerialNumber),
	m_connName(boc_editor_h2.m_connName),
	m_link(boc_editor_h2.m_link)
    { /*std::cout << "INFO [BocEditorH2::ctor] boc_editor" << static_cast<void *>(m_bocEditor.get())  << std::endl; */ }

    BocEditorH2(const TH2_class &h2,
		SerialNumber_t scan_serial_number,
		const std::string &conn_name,
		unsigned int link,
		const std::shared_ptr<PixCon::IBocEditor> &boc_editor) 
      : ModuleMapH2<TH2_class>(h2), 
	m_bocEditor(boc_editor),
	m_scanSerialNumber(scan_serial_number),
	m_connName(conn_name),
	m_link(link)
    { /*std::cout << "INFO [BocEditorH2::ctor] boc_editor" << static_cast<void *>(boc_editor.get())  << std::endl;*/ }

    ~BocEditorH2() 
    { /*std::cout << "INFO [BocEditorH2::dtor] boc_editor" << static_cast<void *>(m_bocEditor.get())  << std::endl;*/  }

    void ExecuteEvent(Int_t event, Int_t px, Int_t py){
      if (!gPad) return;

      Double_t x  = gPad->PadtoX(gPad->AbsPixeltoX(px));
      Double_t y  = gPad->PadtoY(gPad->AbsPixeltoY(py));
      switch (event) {
      case 21: {
	TMarker *the_marker = findMarker(gPad, s_newSettingsMarker);
	if (the_marker) {
	  delete the_marker;
	  //	  gPad->Modified();
	  // gPad->ls();
	}
	break;
      }
      case 11: {
	placeMarker(gPad, x,y,s_newSettingsMarker, s_newSettingsMarkerSize);
	if (m_bocEditor && fabs(x)<static_cast<double>(INT_MAX) && fabs(y)<static_cast<double>(INT_MAX)) {
	  m_bocEditor->changeRxSettings(m_connName, m_link, x, y, m_scanSerialNumber);
	}
	break;
      }
      default: {
	break;
      }
      }
    }

    void	Draw(Option_t* option = "") {
      ModuleMapH2<TH2_class>::Draw(option);
      showMarker();
    }

    TObject*	DrawClone(Option_t* option = "") const {
      TObject *obj = ModuleMapH2<TH2_class>::DrawClone(option);
      showMarker();

      return obj;
    }

    TH1*	DrawCopy(Option_t* option = "") const {
      TH1 *h1 = ModuleMapH2<TH2_class>::DrawCopy(option);
      showMarker();

      return h1;
    }
  private:
    using TH1::DrawCopy;
    TMarker *findMarker(TVirtualPad *a_pad, Style_t marker_style) const {
      assert(a_pad);
      if (!a_pad->GetListOfPrimitives()) return NULL;

      TIter next(a_pad->GetListOfPrimitives());
      TObject *obj;
      while ((obj = next())) {
	if (obj->InheritsFrom(TMarker::Class())) {
	  TMarker *marker_obj = static_cast<TMarker *>(obj);
	  if (marker_obj->GetMarkerStyle()==marker_style) {
	    return marker_obj;
	  }
	}
      }
      return NULL;
    }

    TMarker *createMarker(TVirtualPad *a_pad, Double_t x, Double_t y, Style_t marker_style, float marker_scale_factor) const {
      a_pad->cd();
      TMarker *marker = new TMarker(x,y, marker_style);
      marker->SetMarkerSize(marker->GetMarkerSize() * marker_scale_factor);
      marker->Draw("P");
      a_pad->Modified();
      return marker;
    }

    void placeMarker(TVirtualPad *a_pad, Double_t x, Double_t y, Style_t marker_style, float marker_scale_factor) const {
      TMarker *marker(findMarker(a_pad,marker_style));
      if (!marker) {
	marker = createMarker(a_pad, x,y, marker_style, marker_scale_factor);
      }
      else {
	marker->SetX(x);
	marker->SetY(y);
	a_pad->Modified();
      }
    }

    void showMarker() const {
      if (m_bocEditor && gPad) {

	{
	  std::pair<int,int> rx_delay_threshold  = m_bocEditor->analysisRxSettings(m_connName, m_link, m_scanSerialNumber);
	  if (rx_delay_threshold.first>0  || rx_delay_threshold.second>0) {
	    placeMarker(gPad, rx_delay_threshold.first, rx_delay_threshold.second, s_optimumSettingsMarker, s_optimumSettingsMarkerSize);
	  }
	}
	
	{
	std::pair<int,int> rx_delay_threshold = m_bocEditor->rxSettings(m_connName, m_link, m_scanSerialNumber);
	if (rx_delay_threshold.first>0  || rx_delay_threshold.second>0) {
	  placeMarker(gPad, rx_delay_threshold.first, rx_delay_threshold.second, s_newSettingsMarker, s_newSettingsMarkerSize);
	}
	}
      }
    }

    std::shared_ptr<PixCon::IBocEditor> m_bocEditor;

    PixCon::SerialNumber_t m_scanSerialNumber;
    std::string            m_connName;
    unsigned int           m_link;

    mutable std::string m_currentInfo;
    static const Style_t  s_newSettingsMarker = 29 ;
    static const Style_t  s_optimumSettingsMarker = 28;
    static const float    s_newSettingsMarkerSize;
    static const float    s_optimumSettingsMarkerSize;
  };


  template <> const float    BocEditorH2<TH2F>::s_newSettingsMarkerSize=2;
  template <> const float    BocEditorH2<TH2F>::s_optimumSettingsMarkerSize=2;
  template <> const float    BocEditorH2<TH2D>::s_newSettingsMarkerSize=2;
  template <> const float    BocEditorH2<TH2D>::s_optimumSettingsMarkerSize=2;

  ModuleMapH2Factory::ModuleMapH2Factory(const std::shared_ptr<PixCon::IBocEditor> &boc_editor) 
    : m_bocEditor(boc_editor)
  { /*std::cout << "INFO [ModuleMapH2Factory::ctor] boc_editor" << static_cast<void *>(m_bocEditor.get()) << std::endl;*/  }

  ModuleMapH2Factory::ModuleMapH2Factory() 
  {}


  ModuleMapH2Factory::~ModuleMapH2Factory()
  {
    /*std::cout << "INFO [ModuleMapH2Factory::dtor] this=" << static_cast<void *>(this) << " boc_editor=" 
      << static_cast<void *>(m_bocEditor.get()) << std::endl; */
  }

  TH2 *ModuleMapH2Factory::createMapH2(const TH2 *h2) {
    if (h2) {
      if (h2->InheritsFrom(TH2F::Class())) return new ModuleMapH2<TH2F>(static_cast<const TH2F &>(*h2));
      if (h2->InheritsFrom(TH2D::Class())) return new ModuleMapH2<TH2D>(static_cast<const TH2D &>(*h2));
    }
    return NULL;
  }

  TH2 *ModuleMapH2Factory::createBocEditH2(const TH2 *h2, SerialNumber_t serial_number, const std::string &conn_name, unsigned int link)
  {
//     std::cout << "INFO [ModuleMapH2Factory::createBocEditH2] this=" << static_cast<void *>(this) << " boc_editor="
// 	      << static_cast<void *>(m_bocEditor.get())  << std::endl; 
    if (h2) {
//       if ((link&1)==0) {
// 	link=1;
//       }
//       else {
// 	link=0;
//       }
      
      if (h2->InheritsFrom(TH2F::Class())) return new BocEditorH2<TH2F>(static_cast<const TH2F &>(*h2), serial_number, conn_name, link%2, m_bocEditor);
      if (h2->InheritsFrom(TH2D::Class())) return new BocEditorH2<TH2D>(static_cast<const TH2D &>(*h2), serial_number, conn_name, link%2, m_bocEditor);
    }
    return NULL;
  }


}
