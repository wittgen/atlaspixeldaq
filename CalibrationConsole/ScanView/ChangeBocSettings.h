/* Dear emacs, this is -*-c++-*- */
#ifndef _PixCon_ChangeBocSettings_h_
#define _PixCon_ChangeBocSettings_h_

#include <qlabel.h>
#include <qlineedit.h>
//#include <q3frame.h>

#include "ui_ChangeBocSettingsBase.h"
#include <string>
#include <sstream>

namespace PixCon {

  class ChangeBocSettings : public QDialog, Ui_ChangeBocSettingsBase
  {    Q_OBJECT
  public:
    ChangeBocSettings( const std::string &title, unsigned int link, QWidget* parent = 0);
    ~ChangeBocSettings();

    void setTitle(const std::string &name) {
      titleLable->setText(name.c_str());
    }

    void setRxPluginName(const std::string &rx_plugin_name) {
      m_rxPluginName->setText(rx_plugin_name.c_str());
    }

    void setRxDelay(float rx_delay) {
      std::stringstream message;
      message << rx_delay;
      m_delayEdit->setText(message.str().c_str() );
    }

    bool hasRxDelay() const;

    float  rxDelay() const;

    void setRxDelayReference(float rx_delay) {
      m_rxDelayRef=rx_delay;
      std::stringstream message;
      message << rx_delay;
      m_oldDelayLabel->setText(message.str().c_str() );
    }

    void setRxThreshold(float rx_threshold) {
      std::stringstream message;
      message << rx_threshold;
      m_thresholdEdit->setText(message.str().c_str() );
    }

    bool hasRxThreshold() const;

    float rxThreshold() const;

    void setRxThresholdReference(float rx_threshold) {
      m_rxThresholdRef=rx_threshold;
      std::stringstream message;
      message << rx_threshold;
      m_oldThresholdLabel->setText(message.str().c_str() );
    }

    void setTxPluginName(const std::string &tx_plugin_name) {
      showTxFrame();
      m_txPluginName->setText(tx_plugin_name.c_str());
    }

    bool hasTxPower() const;

    void setTxPower(int tx_power) {
      showTxFrame();
      std::stringstream message;
      message << tx_power;
      m_powerEdit->setText(message.str().c_str() );
    }

    void setTxPowerReference(int tx_power_reference) {
      showTxFrame();
      m_txPowerRef = tx_power_reference;
      std::stringstream message;
      message << tx_power_reference;
      m_oldPowerLabel->setText(message.str().c_str() );
    }

    int txPower() const;

  private:
    
    void showTxFrame() {
      if (m_frameTx->isHidden()) {
	m_frameTx->show();
	adjustSize();
      }
    }

    float m_rxDelayRef;
    float m_rxThresholdRef;
    int m_txPowerRef;
  };

}
#endif
