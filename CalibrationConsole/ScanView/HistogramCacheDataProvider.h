/* Dear emacs, this is -*-c++-*- */
#ifndef _PixCon_HistogramCacheDataProvider_h_
#define _PixCon_HistogramCacheDataProvider_h_

#include <Visualiser/IScanDataProvider.h>
#include <ConfigWrapper/Connectivity.h>
#include <ConfigWrapper/ConnectivityUtil.h>
#include <DataContainer/HistoList_t.h>
#include <CalibrationDataTypes.h>

#include "HistogramCache.h"

#include "guessConnItemType.h"

#include <vector>

namespace PixCon {

  /** Simple scan data provider which reads the histograms from RootDb files.
   * This provider is intended to be used by ModuleAnalysis to give histogram
   * and configuration data access to the visualiser.
   */
  class HistogramCacheDataProvider : public PixA::IScanDataProvider
  {
  public:
    HistogramCacheDataProvider(const std::shared_ptr<HistogramCache> &histogram_cache,
			       EMeasurementType measurement_type,
			       SerialNumber_t serial_number,
			       const PixA::ConnectivityRef &connectivity,
			       const std::vector <PixA::ConfigHandle> &vscan_config)
      : m_histogramCache(histogram_cache),
	m_type(measurement_type),
	m_serialNumber(serial_number),
	m_connectivity(connectivity),
	m_vscanConfigHandle(vscan_config)
    {}

    ~HistogramCacheDataProvider() {}

    SerialNumber_t scanNumber() const {
      return m_serialNumber;
    }

    /** Return the connectivity used for the current scan.
     * The connectivity may be invalid.
     */
    const PixA::ConnectivityRef &connectivity() {
      return m_connectivity;
    }

    void numberOfHistos(const std::string &connectivity_name,
			const std::string &histo_name,
			HistoInfo_t &info)
    {
      info.reset();
      EConnItemType conn_item_type = guessConnItemType(connectivity_name);
      if (conn_item_type>=kNHistogramConnItemTypes) return;
      if (m_lastHistogramName != histo_name) {
	m_lastHistogramName = histo_name;
	m_lastHistogramIndex = m_histogramCache->histogramIndex(m_type, m_serialNumber, conn_item_type, m_lastHistogramName);
      }
      m_histogramCache->numberOfHistograms(m_type, m_serialNumber, conn_item_type, connectivity_name, m_lastHistogramIndex , info);
    }

    const PixA::Histo *histogram(const std::string &connectivity_name,
				 const std::string &histo_name,
				 const PixA::Index_t &index);

    const std::string  &prodName(const std::string &connectivity_name)
    {
      EConnItemType conn_item_type = guessConnItemType(connectivity_name);
      if (conn_item_type ==kModuleItem) {
	const PixLib::ModuleConnectivity *module_connectivity = m_connectivity.findModule(connectivity_name);
	if (module_connectivity) {
	  return const_cast<PixLib::ModuleConnectivity *>(module_connectivity)->prodId();
      }
      }
      return s_emptyString;
    }

    const PixA::ConfigHandle scanConfig()
    {
      if(m_vscanConfigHandle.begin()!=m_vscanConfigHandle.end())
	return *(m_vscanConfigHandle.begin());
      else
	return PixA::ConfigHandle();
    }

    const PixA::ConfigHandle scanConfig(std::string &fe_tag){
      //return m_scanConfigHandle;
      //std::cout << "DEBUG [HistogramCacheDataProvider::scanConfig] requesting config. for FE type \"" << fe_tag << "\"" << std::endl;
	
      std::vector <PixA::ConfigHandle>::iterator it = m_vscanConfigHandle.begin(); 
      
      // I4 configuration should be in 2nd element of config vector m_vscanConfigHandle, but may be missing
      // for old data - in which case an empty PixA::ConfigHandle is returned
      if(fe_tag == "FE-I4" || fe_tag == "Base_I4"){
	if((it+1)==m_vscanConfigHandle.end()) return PixA::ConfigHandle();
	//std::cout << "DEBUG [HistogramCacheDataProvider::scanConfig] found requested FE-I4 config."<<std::endl;
	it++;
      }
      
      return *it;
    }

    const PixA::ConfigHandle moduleConfig(const std::string &connectivity_name) 
    {
      EConnItemType conn_item_type = guessConnItemType(connectivity_name);
      if (conn_item_type == kModuleItem) {
	if (m_connectivity) {
	  return m_connectivity.modConf(connectivity_name);
	}
      }
      return PixA::ConfigHandle();
    }

    const PixA::ConfigHandle moduleGroupConfig(const std::string &connectivity_name) 
    {
      std::string rod_name = rodName( connectivity_name);
      if ( !rod_name.empty() ) {
	try {
	  return m_connectivity.modGroupConf(rod_name);
	}
	catch (PixA::ConfigException &err) {
	}
      }
      return PixA::ConfigHandle();
    }

    const PixA::ConfigHandle bocConfig(const std::string &connectivity_name)
    {
      std::string rod_name = rodName( connectivity_name);
      if ( !rod_name.empty() ) {
	try {
	  return m_connectivity.bocConfFromRodLocation(rod_name);
	}
	catch (PixA::ConfigException &err) {
	}
      }
      return PixA::ConfigHandle();
    }

    const PixA::ConfigHandle rodConfig(const std::string &connectivity_name)
    {
      std::string rod_name = rodName( connectivity_name);
      if ( !rod_name.empty() ) {
	try {
	  return m_connectivity.rodConfFromRodLocation(rod_name);
	}
	catch (PixA::ConfigException &err) {
	}
      }
      return PixA::ConfigHandle();
    }

    const PixA::ConfigHandle timConfig(const std::string &connectivity_name) 
    {
      EConnItemType conn_item_type = guessConnItemType(connectivity_name);
      if (conn_item_type == kTimItem) {
	if (m_connectivity) {
	  return m_connectivity.timConfFromTimLocation(connectivity_name);
	}
      }
      else if (conn_item_type == kCrateItem) {
	  return m_connectivity.timConf(connectivity_name);
      }
      return PixA::ConfigHandle();
    }

//     PixA::ConfigHandle rodConfig(const std::string &connectivity_name) 
//     {
//       return PixA::ConfigHandle();
//     }
  protected:

    std::string rodName(const std::string &connectivity_name) {
      if (m_connectivity) {
	EConnItemType conn_item_type = guessConnItemType(connectivity_name);
	if (conn_item_type == kModuleItem) {
	  const PixLib::ModuleConnectivity *module_connectivity = m_connectivity.findModule(connectivity_name);
	  if (module_connectivity) {
	    const PixLib::RodBocConnectivity *rod_connectivity = PixA::rodBoc( module_connectivity);
	    if (rod_connectivity) {
	      return PixA::connectivityName(rod_connectivity);
	    }
	  }
	}
	else if (conn_item_type == kPp0Item) {

	  const PixLib::Pp0Connectivity *pp0_connectivity = m_connectivity.findPp0(connectivity_name);
	  if (pp0_connectivity) {
	    const PixLib::RodBocConnectivity *rod_connectivity = PixA::rodBoc( pp0_connectivity);
	    if (rod_connectivity) {
	      return PixA::connectivityName(rod_connectivity);
	    }
	  }
	}
	else if (conn_item_type == kRodItem) {
	  return connectivity_name;
	}
      }
      return s_emptyString;
    }

    std::string pp0Name(const std::string &connectivity_name) {
      if (m_connectivity) {
	EConnItemType conn_item_type = guessConnItemType(connectivity_name);
	if (conn_item_type == kModuleItem) {
	  const PixLib::ModuleConnectivity *module_connectivity = m_connectivity.findModule(connectivity_name);
	  if (module_connectivity) {
	    const PixLib::Pp0Connectivity * pp0_conn = PixA::pp0(module_connectivity);
	    if (pp0_conn) {
	      return PixA::connectivityName(pp0_conn);
	    }
	  }
	}
	else if (conn_item_type == kPp0Item) {
	  try {
	    return connectivity_name;
	  }
	  catch (PixA::ConfigException &err) {
	  }
	}
	else if (conn_item_type == kRodItem) {
	  PixA::Pp0List pp0_list = m_connectivity.pp0s(connectivity_name);
	  PixA::Pp0List::const_iterator pp0_iter = pp0_list.begin();
	  if (pp0_list.begin() != pp0_list.end() ) {
	    return PixA::connectivityName(pp0_iter);
	  }
	}
      }
      return s_emptyString;
    }

  private:
    std::shared_ptr<HistogramCache> m_histogramCache;
    EMeasurementType                  m_type;
    SerialNumber_t                    m_serialNumber;
    PixA::ConnectivityRef             m_connectivity;

    std::string           m_lastHistogramName;
    unsigned int          m_lastHistogramIndex;
    static const std::string          s_emptyString;

    std::vector <PixA::ConfigHandle> m_vscanConfigHandle;
  };

}
#endif
