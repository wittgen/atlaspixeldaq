#ifndef UI_OBJECTDATAVIEWBASE_H
#define UI_OBJECTDATAVIEWBASE_H

#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QDialog>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QSpacerItem>
#include <QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ObjectDataViewBase
{
public:
    QVBoxLayout *vboxLayout;
    QLabel *titleLabel;
    QHBoxLayout *hboxLayout;
    QLabel *filterLabel;
    QPushButton *clearFilterButton;
    QPushButton *filterButton;
    QLineEdit *filterLineEdit;
    //Q3ListView *objectDataListView;
    QTreeWidget *objectDataListView;
    QTreeWidgetItem *objectDataListViewHeaders;
    QHBoxLayout *hboxLayout1;
    QSpacerItem *spacer3_2;
    QPushButton *closeButton;

    void setupUi(QDialog *ObjectDataViewBase)
    {
        if (ObjectDataViewBase->objectName().isEmpty())
            ObjectDataViewBase->setObjectName(QString::fromUtf8("ObjectDataViewBase"));
        ObjectDataViewBase->resize(616, 503);
        QFont font;
        ObjectDataViewBase->setFont(font);
        vboxLayout = new QVBoxLayout(ObjectDataViewBase);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        titleLabel = new QLabel(ObjectDataViewBase);
        titleLabel->setObjectName(QString::fromUtf8("titleLabel"));
        QFont font1;
        font1.setPointSize(14);
        font1.setBold(true);
        titleLabel->setFont(font1);
        titleLabel->setIndent(16);
        titleLabel->setWordWrap(false);

        vboxLayout->addWidget(titleLabel);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        filterLabel = new QLabel(ObjectDataViewBase);
        filterLabel->setObjectName(QString::fromUtf8("filterLabel"));
        filterLabel->setWordWrap(false);

        hboxLayout->addWidget(filterLabel);

        clearFilterButton = new QPushButton(ObjectDataViewBase);
        clearFilterButton->setObjectName(QString::fromUtf8("clearFilterButton"));
        clearFilterButton->setIcon(QIcon(QPixmap(":/icons/images/gtk-clear.png")));

        hboxLayout->addWidget(clearFilterButton);

        filterButton = new QPushButton(ObjectDataViewBase);
        filterButton->setObjectName(QString::fromUtf8("filterButton"));
        const QIcon icon = QPixmap(":/icons/images/stock_autofilter.png");
        filterButton->setIcon(icon);

        hboxLayout->addWidget(filterButton);

        filterLineEdit = new QLineEdit(ObjectDataViewBase);
        filterLineEdit->setObjectName(QString::fromUtf8("filterLineEdit"));
        QFont font2;
        font2.setPointSize(12);
        filterLineEdit->setFont(font2);

        hboxLayout->addWidget(filterLineEdit);


        vboxLayout->addLayout(hboxLayout);

        //objectDataListView = new Q3ListView(ObjectDataViewBase);
        objectDataListView = new QTreeWidget(ObjectDataViewBase);
        objectDataListView->setObjectName(QString::fromUtf8("objectDataListView"));
        objectDataListView->setColumnCount(4);
        objectDataListViewHeaders = new QTreeWidgetItem;
	objectDataListViewHeaders->setText(0,"Name");
	objectDataListViewHeaders->setText(1,"Type");
	objectDataListViewHeaders->setText(2,"Value");
	objectDataListViewHeaders->setText(3,"Value Color");
        objectDataListView->setHeaderItem(objectDataListViewHeaders);
        objectDataListView->header()->resizeSections(QHeaderView::ResizeToContents);
#if QT_VERSION < 0x050000
        objectDataListView->header()->setClickable(true);
#else
        objectDataListView->header()->setSectionsClickable(true);
#endif
	//objectDataListView->setSorting(-1);

        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(7), static_cast<QSizePolicy::Policy>(3));
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(objectDataListView->sizePolicy().hasHeightForWidth());
        objectDataListView->setSizePolicy(sizePolicy);
        objectDataListView->setFont(font2);

        vboxLayout->addWidget(objectDataListView);

        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setSpacing(6);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        spacer3_2 = new QSpacerItem(502, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout1->addItem(spacer3_2);

        closeButton = new QPushButton(ObjectDataViewBase);
        closeButton->setObjectName(QString::fromUtf8("closeButton"));

        hboxLayout1->addWidget(closeButton);


        vboxLayout->addLayout(hboxLayout1);

        QWidget::setTabOrder(objectDataListView, filterLineEdit);
        QWidget::setTabOrder(filterLineEdit, filterButton);
        QWidget::setTabOrder(filterButton, clearFilterButton);
        QWidget::setTabOrder(clearFilterButton, closeButton);

        retranslateUi(ObjectDataViewBase);
        QObject::connect(closeButton, SIGNAL(clicked()), ObjectDataViewBase, SLOT(close()));
        QObject::connect(filterButton, SIGNAL(clicked()), ObjectDataViewBase, SLOT(filter()));
        QObject::connect(clearFilterButton, SIGNAL(clicked()), ObjectDataViewBase, SLOT(clearFilter()));

        QMetaObject::connectSlotsByName(ObjectDataViewBase);
    } // setupUi

    void retranslateUi(QDialog *ObjectDataViewBase)
    {
        ObjectDataViewBase->setWindowTitle(QApplication::translate("ObjectDataViewBase", "ObjectDataView", 0));
        titleLabel->setText(QApplication::translate("ObjectDataViewBase", "Title", 0));
        filterLabel->setText(QApplication::translate("ObjectDataViewBase", "Filter : ", 0));
        clearFilterButton->setText(QString());
#ifndef QT_NO_TOOLTIP
        clearFilterButton->setProperty("toolTip", QVariant(QApplication::translate("ObjectDataViewBase", "Clear the variable filter", 0)));
#endif // QT_NO_TOOLTIP
        filterButton->setText(QApplication::translate("ObjectDataViewBase", "Apply Filter", 0));
#ifndef QT_NO_TOOLTIP
        filterButton->setProperty("toolTip", QVariant(QApplication::translate("ObjectDataViewBase", "Apply the variable filter", 0)));
#endif // QT_NO_TOOLTIP
        closeButton->setText(QApplication::translate("ObjectDataViewBase", "Close", 0));
    } // retranslateUi

};

namespace Ui {
    class ObjectDataViewBase: public Ui_ObjectDataViewBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OBJECTDATAVIEWBASE_H
