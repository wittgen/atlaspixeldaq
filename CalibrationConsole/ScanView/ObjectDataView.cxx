#include <iostream>
#include <cstring>

#include "ObjectDataView.h"
#include <CalibrationDataManager.h>
#include <CalibrationDataStyle.h>
#include <CategoryList.h>
#include <ConfigWrapper/ConnectivityUtil.h>
#include "qpainter.h"


namespace PixCon {

  class ObjectDataItem : public QTreeWidgetItem
  {
  public:

    enum EColumnType {kName, kNColumns};

    ///////////general item
    //ObjectDataItem(Q3ListViewItem *parent_item,
    ObjectDataItem(QTreeWidgetItem *parent_item,
           const QStringList &nameText,
		   bool open) :
      QTreeWidgetItem(parent_item,
		    nameText)
    {
      //setOpen(open);
        setExpanded(open);
    }

    ///////////general item
    //ObjectDataItem(Q3ListView *parent,
    ObjectDataItem(QTreeWidget *parent,
           const QStringList &nameText,
		   bool open) :
      QTreeWidgetItem(parent,
		    nameText)
    {
      //setOpen(open);
        setExpanded(open);
    }

    //////////value variable item
    void setValue(float value,
		  const VarDef_t &var_def,
		  const std::shared_ptr<PixCon::CalibrationDataPalette> &palette,
		  unsigned int object_i)
    {
      unsigned int col_i=object_i+kNColumns;

      //fill value
      QString value_str;
      value_str = value_str.setNum(value);
      setText(col_i, value_str);

      //fill color
      if (var_def.isMinMaxSet()) {
	value -= var_def.minVal();
	value /= (var_def.maxVal() - var_def.minVal());
      }
      std::pair<QBrush, QPen> value_brush_and_pen = palette->brushAndPen(true, 1, 0, var_def.id(), value);
      QPixmap pm( 50, sizeHint(col_i).height() );//Check this
      pm.fill( value_brush_and_pen.first.color() );
      //QLabel *label = new QLabel(value_str);
      //QLabel::setPixmap(label);
    }

    //////////state variable item
    void setStateValue(State_t state,
		       VarId_t var_id,
		       const StateVarDef_t &state_def,
		       const std::shared_ptr<PixCon::CalibrationDataPalette> &palette,
		       unsigned int object_i)
    {
      unsigned int col_i=object_i+kNColumns;

      //fill state
      std::pair<QBrush, QPen> state_brush_and_pen;
      if (state_def.isValidState(state)) {
	const std::string &state_name = state_def.stateName(state);
	if ( !state_name.empty() ) setText(col_i, QString::fromStdString(state_name));

	//fill color
	State_t mapped_state = state_def.remapState(state);
	if (state>state_def.nStates()) {
	  mapped_state=state_def.nStates();
	}
	state_brush_and_pen = palette->brushAndPen(true, 1, 0, var_id, mapped_state);
      }
      else {
	setText(col_i, "(unknown)");
	state_brush_and_pen = palette->brushAndPenUnknown(1,0);
      }

      QPixmap pm( 50, sizeHint(col_i).height() ); //check this as well
      pm.fill( state_brush_and_pen.first.color() );
      //setPixmap(col_i, pm);
    }

    ~ObjectDataItem() {}

  private: 
    // void paintCell (QPainter *painter, const QColorGroup &color_group, int column, int width, int align)
    // {
    //   if (column>=kNColumns) {

    //     painter->save();
    //     QColorGroup new_colour_group(colour_group);

    // 	std::pair<QBrush , QPen > state_colour = m_metaDataList->statusColour(m_status);

    // 	//	if (m_status==MetaData_t::kFailed) {
    // 	//	  assert( s_whiteColor.size()==1);
    // 	//	  new_colour_group.setColor(QColorGroup::Text, s_whiteColor[0]);
    // 	new_colour_group.setColor(QColorGroup::Base, state_colour.first.color());
    // 	//	}
    // 	//	else  {
    // 	//	  new_colour_group.setColor(QColorGroup::Base, state_colour.first.color());
    // 	//	}

    //     QListViewItem::paintCell(painter, new_colour_group, column, width, align);
    //     painter->restore();
    //   }
    //   else {
    // 	QListViewItem::paintCell(painter, color_group, column, width, align);
    //   }
    // }

    //ObjectDataView* m_objectDataView;
  };

  class SerialNumberDataItem : public QTreeWidgetItem //
  {
  public:
    //////////serial number item
    SerialNumberDataItem(QTreeWidgetItem *parent_item,//
			 PixCon::EMeasurementType measurement_type,
			 SerialNumber_t serial_number,
			 const std::shared_ptr<CalibrationDataManager> &calibration_data_manager,
			 const std::shared_ptr<PixCon::CalibrationDataPalette> &palette ):
      QTreeWidgetItem(parent_item),
      m_calibration_data_manager(calibration_data_manager),
      m_palette(palette),
      m_measurement_type(measurement_type),
      m_serial_number(serial_number)
    {
      //std::cout << " Making serial number item " << serial_number << std::endl;
      initSerialNumberItem();
    }

    SerialNumberDataItem(/*Q3ListView*/QTreeWidget *parent_item,
			 PixCon::EMeasurementType measurement_type,
			 SerialNumber_t serial_number,
			 const std::shared_ptr<CalibrationDataManager> &calibration_data_manager,
			 const std::shared_ptr<PixCon::CalibrationDataPalette> &palette):
      QTreeWidgetItem(parent_item),
      m_calibration_data_manager(calibration_data_manager),
      m_palette(palette),
      m_measurement_type(measurement_type),
      m_serial_number(serial_number)
    {
      //std::cout << " Making serial number item " << serial_number << std::endl;
      initSerialNumberItem();
    }

    ~SerialNumberDataItem() { }

  private:
    void initSerialNumberItem(){
      //set serial number
      std::string serial_number_string = makeSerialNumberString(m_measurement_type, m_serial_number);
      setText(ObjectDataItem::kName, QString::fromStdString(serial_number_string));

      //set scan/analysis type
      if (m_measurement_type == kScan || m_measurement_type == kCurrent) {
	//std::cout << " Trying to find scan type" << std::endl;
	const ScanMetaDataCatalogue &scan_catalogue = m_calibration_data_manager->scanMetaDataCatalogue();
	std::string scan_type;
	if (scan_catalogue.find(m_serial_number) != NULL ) scan_type = (scan_catalogue.find(m_serial_number))->name();
	//if ( !scan_type.empty() ) setText(m_objectDataView->kType, scan_type);
      }
      else if (m_measurement_type == kAnalysis || m_measurement_type == kCurrent) {
	//std::cout << " Trying to find analysis type" << std::endl;
	const AnalysisMetaDataCatalogue &analysis_catalogue = m_calibration_data_manager->analysisMetaDataCatalogue();
	std::string analysis_type;
	if (analysis_catalogue.find(m_serial_number) != NULL) analysis_type = (analysis_catalogue.find(m_serial_number))->name();
	// if ( !analysis_type.empty() ) setText(m_objectDataView->kType, analysis_type);
      }
      //std::cout << " Finished making serial_number_item" << std::endl;
      //setOpen(true);
      setExpanded(true);//
    }
/* Check Significance of this:: ISHAN
    void paintCell (QPainter *painter, const QColorGroup &color_group, int column, int width, int align)
    {
      if (column==0 and m_measurement_type!=kCurrent) {
 	painter->save();
	QColorGroup new_color_group(color_group);

	MetaData_t::EStatus status;
        if (m_measurement_type == kScan
	    && ( m_calibration_data_manager->scanMetaDataCatalogue()).find(m_serial_number) != NULL)
	  status = (( m_calibration_data_manager->scanMetaDataCatalogue()).find(m_serial_number) )->status();
	else if (m_measurement_type == kAnalysis
		 && ( m_calibration_data_manager->analysisMetaDataCatalogue()).find(m_serial_number) != NULL )
	  status = (( m_calibration_data_manager->analysisMetaDataCatalogue()).find(m_serial_number) )->status();

	std::pair<QBrush, QPen> brush_and_pen;
	if (status == MetaData_t::kOk)
	  brush_and_pen = m_palette->brushAndPen(true,1, 0, static_cast<VarId_t>(-1), static_cast<State_t>(DefaultColourCode::kOk) );
	else if (status == MetaData_t::kFailed)
	  brush_and_pen = m_palette->brushAndPen(true,1, 0, static_cast<VarId_t>(-1), static_cast<State_t>(DefaultColourCode::kFailed) );
	else if (status == MetaData_t::kRunning)
	  brush_and_pen = m_palette->brushAndPen(true,1, 0, static_cast<VarId_t>(-1), static_cast<State_t>(DefaultColourCode::kRunning) );
	else if (status == MetaData_t::kLoading)
	  brush_and_pen = m_palette->brushAndPen(true,1, 0, static_cast<VarId_t>(-1), static_cast<State_t>(DefaultColourCode::kConfigured) );
	else if (status == MetaData_t::kAborted)
	  brush_and_pen = m_palette->brushAndPen(true,1, 0, static_cast<VarId_t>(-1), static_cast<State_t>(DefaultColourCode::kOff) );
	else 
	  brush_and_pen = m_palette->brushAndPen(true,1, 0, static_cast<VarId_t>(-1), static_cast<State_t>(DefaultColourCode::kUnknown) );

	new_color_group.setColor(QColorGroup::Base, brush_and_pen.first.color() );
    //Q3ListViewItem::paintCell(painter, new_color_group, column, width, align);
    QTreeWidgetItem::paintCell(painter, new_color_group, column, width, align);
	painter->restore();

// 	else if (status == MetaData_t::kFailed) {
// 	  assert( s_whiteColor.size()==1);
// 	  new_colour_group.setColor(QColorGroup::Text, QColor("white"));
// 	  new_colour_group.setColor(QColorGroup::Base, DefaultColourCode::kFailed);
// 	}
      }
      else {
    //Q3ListViewItem::paintCell(painter, color_group, column, width, align);
    QTreeWidgetItem::paintCell(painter, new_color_group, column, width, align);
      }
    }
*/
    //ObjectDataView* m_objectDataView;
    std::shared_ptr<CalibrationDataManager> m_calibration_data_manager;
    std::shared_ptr<PixCon::CalibrationDataPalette> m_palette;
    PixCon::EMeasurementType m_measurement_type;
    SerialNumber_t m_serial_number;
  };



  ObjectDataView::ObjectDataView( const std::string &conn_name,
				  PixCon::EConnItemType conn_item_type,
				  const std::shared_ptr<CalibrationDataManager> &calibration_data_manager,
				  const std::shared_ptr<PixCon::CategoryList> &categories,
				  const std::shared_ptr<PixCon::CalibrationDataPalette> &palette,
				  QWidget* parent)
: QDialog(parent),
      m_connType(conn_item_type),
      m_calibrationDataManager(calibration_data_manager),
      m_categories(categories),
      m_palette(palette),
      m_filledRodModuleData(false)
  {
    setupUi(this);
    std::string title = "Object Data : " + conn_name;
    titleLabel->setText(QString::fromStdString(title));

    getVariables();
    initConnNameList(conn_name, conn_item_type);
    fillListView(0, "");
  }

  ObjectDataView::~ObjectDataView() {}

  void ObjectDataView::initConnNameList(const std::string &conn_name,
					PixCon::EConnItemType conn_item_type) {
    m_connNameList.clear();
    Lock lock(m_calibrationDataManager->calibrationDataMutex());

    if (conn_item_type==kPp0Item) {
      //loop over measurement types
      for( std::vector< std::map< SerialNumber_t, std::set<std::string> > >::const_iterator measurement_type_iter= m_varVector.begin();
	   measurement_type_iter != m_varVector.end();
	   measurement_type_iter++) {

	EMeasurementType measurement_type = static_cast<EMeasurementType>(measurement_type_iter-m_varVector.begin());

	for (std::map< SerialNumber_t, std::set<std::string> >::const_iterator  serial_number_iter = measurement_type_iter->begin();
	     serial_number_iter != measurement_type_iter->end();
	     serial_number_iter++) {

	  try {
	    PixA::ConnectivityRef conn = m_calibrationDataManager->connectivity(measurement_type, serial_number_iter->first);
	    if (conn) {
	      PixA::ModuleList module_list=conn.modules(conn_name);
	      //loop over modules on PP0
	      for (PixA::ModuleList::const_iterator module_iter = module_list.begin();
		   module_iter != module_list.end();
		   ++module_iter) {
		m_connNameList.insert( PixA::connectivityName(module_iter) );
	      }
	    }
	  }
	  catch (...) {
	    std::cout << "WARNING [ObjectDataView::fillPp0ModuleData] Exception generated when trying to get connectivity for "
		      << conn_name << std::endl;
	  }
	}
      }
    }
    else {
      m_connNameList.insert(conn_name);
    }
  }

  //SerialNumberDataItem* ObjectDataView::makeSerialNumberItem(Q3ListViewItem* parent_item, PixCon::EMeasurementType measurement_type, SerialNumber_t serial_number){
    SerialNumberDataItem* ObjectDataView::makeSerialNumberItem(QTreeWidgetItem* parent_item, PixCon::EMeasurementType measurement_type, SerialNumber_t serial_number){
    //std::cout << " makeSerialNumberItem for measurement_type = " << measurement_type << " and serial_number = " << serial_number << std::endl;
    //for current, ????
    //for scans, the parent item = parent_item (i.e. module or ROD or nothing)
    //for analyses, parent item = scan
    SerialNumberDataItem *serial_number_item=nullptr;
    if (measurement_type == kScan || measurement_type == kCurrent){
      if (parent_item == NULL ) serial_number_item = new SerialNumberDataItem(objectDataListView, measurement_type, serial_number, m_calibrationDataManager, m_palette);
      else serial_number_item = new SerialNumberDataItem(parent_item, measurement_type, serial_number, m_calibrationDataManager, m_palette);
    }
    else if (measurement_type==kAnalysis){
      //get scan serial number associated with analysis
      SerialNumber_t scan_serial_number=0;
      if ( (m_calibrationDataManager->analysisMetaDataCatalogue()).find(serial_number) != NULL )
	scan_serial_number = ( (m_calibrationDataManager->analysisMetaDataCatalogue()).find(serial_number) )->scanSerialNumber();
      std::string scan_serial_number_string = makeSerialNumberString(kScan, scan_serial_number);    
      //std::cout << " scan number associated with analysis = " << scan_serial_number_string << std::endl;

      //loop over loaded scans to find match
      bool match_found = false;
      QTreeWidgetItemIterator child_item = parent_item ? QTreeWidgetItemIterator(parent_item) : QTreeWidgetItemIterator(objectDataListView);
      //if (parent_item == NULL) QTreeWidgetItemIterator child_item(objectDataListView);
      //else QTreeWidgetItemIterator child_item(parent_item);

      //initialize the QTreeWidgetItemIterator
      while((*child_item)){
          if ((*child_item)->text(ObjectDataItem::kName) == QString::fromStdString(scan_serial_number_string)) {
           serial_number_item = new SerialNumberDataItem((*child_item), measurement_type, serial_number, m_calibrationDataManager, m_palette);
          (*child_item)->addChild(serial_number_item);
          match_found = true;
          break;
          }//if (*child_item loop

      ++child_item; //GOTO next Sibling of  child_item
      } //while *it1 loop
      //if no match, make matching item
        if ( !match_found ) {
      SerialNumberDataItem* scan_match_item = makeSerialNumberItem(parent_item, kScan, scan_serial_number);
      serial_number_item = new SerialNumberDataItem(scan_match_item, measurement_type, serial_number, m_calibrationDataManager, m_palette);
        }
/*
      QTreeWidgetItem *child_item;
      //if (parent_item == NULL) child_item = objectDataListView->firstChild();
      if (parent_item == NULL) child_item = objectDataListView->topLevelItem(0)->child(0);
      //else child_item = parent_item->firstChild();
      else child_item = parent_item->child(0);
      while( child_item ) {
	if ( child_item->text(ObjectDataItem::kName) == QString::fromStdString(scan_serial_number_string)) {
	  //std::cout << " Found " << child_item->text(kName) << std::endl;
	  serial_number_item = new SerialNumberDataItem(child_item, measurement_type, serial_number, m_calibrationDataManager, m_palette);
	  //put analyses after categories
      //Q3ListViewItem *scan_child_item = child_item->firstChild();
      QTreeWidgetItem *scan_child_item = child_item->child(0);
	  while (scan_child_item) {
	    if (scan_child_item->nextSibling() == 0) {
	      serial_number_item->moveItem(scan_child_item);
	      break;
	    }
	    scan_child_item = scan_child_item->nextSibling();
      }//end of while statement (scan_child_item)
	  match_found = true;
	  break;
	}
	//std::cout << " Looping over possible scan items" << std::endl;
	child_item = child_item->nextSibling();
      }
      //if no match, make matching item
      if ( !match_found ) {
	//std::cout << " No matching scan item found" << std::endl;
	SerialNumberDataItem* scan_match_item = makeSerialNumberItem(parent_item, kScan, scan_serial_number); 
	serial_number_item = new SerialNumberDataItem(scan_match_item, measurement_type, serial_number, m_calibrationDataManager, m_palette);
      }*/
    }
    
    return serial_number_item;
  }
  
  
  void ObjectDataView::getVariables(){
    Lock lock(m_calibrationDataManager->calibrationDataMutex());

    std::map< SerialNumber_t, std::set<std::string> > scan_map;
    std::map< SerialNumber_t, std::set<std::string> > analysis_map;
    std::map< SerialNumber_t, std::set<std::string> > current_map;

    const ScanMetaDataCatalogue &scan_meta_data_catalogue = m_calibrationDataManager->scanMetaDataCatalogue();
    const AnalysisMetaDataCatalogue &analysis_meta_data_catalogue = m_calibrationDataManager->analysisMetaDataCatalogue();

    //loop over Scan serial numbers to get variable list
    for (std::map< SerialNumber_t, ScanMetaData_t >::const_iterator scan_iter = scan_meta_data_catalogue.begin();
	 scan_iter != scan_meta_data_catalogue.end();
	 scan_iter++) {
      std::set<std::string> var_list;
      m_calibrationDataManager->calibrationData().varList(kScan, scan_iter->first, var_list);
      //only add if var_list is not empty
      if ( !var_list.empty() ){
	scan_map.insert(std::pair< SerialNumber_t, std::set<std::string> >(scan_iter->first, var_list));
      }
    }

    std::set<std::string> current_var_list;
    m_calibrationDataManager->calibrationData().varList(kCurrent, 0, current_var_list);
    //only add if var_list is not empty
    if ( !current_var_list.empty() ){
      current_map.insert(std::pair< SerialNumber_t, std::set<std::string> >(0, current_var_list));
    }

    //loop over Analysis serial numbers to get variable list
    for (std::map< SerialNumber_t, AnalysisMetaData_t >::const_iterator analysis_iter = analysis_meta_data_catalogue.begin();
	 analysis_iter != analysis_meta_data_catalogue.end();
	 analysis_iter++) {
      std::set<std::string> var_list;
      m_calibrationDataManager->calibrationData().varList(kAnalysis, analysis_iter->first, var_list);
      //only add if var_list is not empty
      if ( !var_list.empty() ){
	analysis_map.insert(std::pair< SerialNumber_t, std::set<std::string> >(analysis_iter->first, var_list));
      }
    }

    //make vector = meas_type | serial_num | var_list
    m_varVector.clear();
    m_varVector.push_back(current_map);
    m_varVector.push_back(scan_map);
    m_varVector.push_back(analysis_map);

  }


  void ObjectDataView::fillListView(/*Q3ListViewItem*/QTreeWidgetItem* parent_item,
				    const std::string &filter_string)
  {
    Lock lock(m_calibrationDataManager->calibrationDataMutex());
    //std::cout << " beginning to fill ListView for " << conn_name << std::endl;

    if (m_connNameList.empty()) return;

    if (objectDataListView->columnCount() < 1) {
      //objectDataListView->addColumn("Name");
        objectDataListView->setColumnCount(1);
        objectDataListViewHeaders->setText(0,"Name");
        objectDataListView->setHeaderItem(objectDataListViewHeaders);
    }
    int object_i=0;
    for(std::set<std::string>::const_iterator conn_iter = m_connNameList.begin();
	conn_iter != m_connNameList.end();
	++conn_iter, ++object_i) {
      int col_i = (object_i)  + ObjectDataItem::kNColumns;
      if (col_i >= objectDataListView->columnCount()) {
    while (col_i+1<objectDataListView->columnCount()) {
      //objectDataListView->addColumn("");
        objectDataListViewHeaders->setText(col_i,"");
	}
    //objectDataListView->addColumn(conn_iter->c_str());
    objectDataListView->setHeaderLabel(conn_iter->c_str());
    //objectDataListView->setColumnAlignment(col_i, Qt::AlignRight);
      }
      else {
    //objectDataListView->setColumnText(col_i, conn_iter->c_str());
          objectDataListViewHeaders->setText(col_i, conn_iter->c_str());
      }
      // ++col_i;
      // if (col_i <= objectDataListView->columns()) {
      // 	while (col_i+1<objectDataListView->columns()) {
      // 	  objectDataListView->addColumn("");
      // 	}
      // 	objectDataListView->addColumn( "");
      // }
      // else {
      // 	objectDataListView->setColumnText(col_i, "");
      // }
    }

    //loop over measurement types
    for( std::vector< std::map< SerialNumber_t, std::set<std::string> > >::const_iterator measurement_type_iter= m_varVector.begin();
	 measurement_type_iter != m_varVector.end();
	 measurement_type_iter++) {

      EMeasurementType measurement_type = static_cast<EMeasurementType>(measurement_type_iter-m_varVector.begin());
      //std::cout << " measurement_type = " << measurement_type << std::endl;
      //loop over serial numbers

      for (std::map< SerialNumber_t, std::set<std::string> >::const_iterator  serial_number_iter = measurement_type_iter->begin();
	   serial_number_iter != measurement_type_iter->end();
	   serial_number_iter++) {

	fillObjectData(m_connNameList, serial_number_iter->first, measurement_type, serial_number_iter->second, parent_item, filter_string);
      }
    }
    objectDataListView->header()->resizeSections(QHeaderView::ResizeToContents);
    //std::cout << " finished filling ListView for " << conn_name << std::endl;
  }

  void ObjectDataView::fillObjectData(const std::set<std::string> &conn_name,
				      SerialNumber_t serial_number,
				      EMeasurementType measurement_type,
				      const std::set<std::string> &var_list,
                      //Q3ListViewItem* parent_item,
                      QTreeWidgetItem* parent_item,
				      const std::string &filter_string)
  {

    //std::cout << " fillObjectData for " << conn_name << ", " << serial_number << ", " << measurement_type << std::endl;
    SerialNumberDataItem* serial_number_item;
    serial_number_item =  makeSerialNumberItem(parent_item, measurement_type, serial_number);

    //loop over categories
    for( std::map<std::string, std::shared_ptr< std::vector< std::string > > >::const_iterator  category_iter = m_categories->begin();
	 category_iter != m_categories->end();
	 ++category_iter) {

      ObjectDataItem* category_item;
      if (serial_number_item != NULL) {
    if ( !filter_string.empty() )category_item = new ObjectDataItem(serial_number_item, QStringList(QString::fromStdString(category_iter->first)), true);
    else category_item = new ObjectDataItem(serial_number_item, QStringList(QString::fromStdString(category_iter->first)), true);
      }
      else category_item = new ObjectDataItem(objectDataListView, QStringList(category_iter->first.c_str()), true);

      //loop over variables in categories
      for (std::vector<std::string>::const_iterator var_iter = category_iter->second->begin();
	   var_iter != category_iter->second->end();
	   var_iter++) {

	//std::cout << " Looping over variables in categories" << std::endl;
	//add variable to this category if it is a variable associated with this serial number
	//and if variable matches regex filter
	std::unique_ptr<PixA::Regex_t> regex(new PixA::Regex_t(filter_string));
	if ( var_list.find(*var_iter) != var_list.end() && regex->match(*var_iter) ) {

	  //std::cout << " Found a match for var_iter = " << (*var_iter) << std::endl;
	  VarDef_t var_def = VarDefList::instance()->getVarDef( (*var_iter) );

	  unsigned int object_i=0;
	  ObjectDataItem* var_item=NULL;
	  for (std::set<std::string>::const_iterator conn_name_iter = conn_name.begin();
	       conn_name_iter != conn_name.end();
	       ++conn_name_iter, ++object_i) {
	  //kValues
	  if (var_def.isValueWithOrWithoutHistory()) {
	    try{
	      const CalibrationData::ConnObjDataList
		conn_data_list = m_calibrationDataManager->calibrationData().getConnObjectData(*conn_name_iter);

	      float value = conn_data_list.newestValue<float>(measurement_type, serial_number, var_def);
	      if (!var_item) {
        var_item =  new ObjectDataItem(category_item, QStringList(QString::fromStdString(*var_iter)),false);
	      }
	      var_item->setValue( value, var_def, m_palette, object_i);
	    } catch (CalibrationData::MissingConnObject &err) {
	    } catch (...){
	    }
	  }
	  //kStates
	  else if (var_def.isStateWithOrWithoutHistory() ) {
	    try{
	      const CalibrationData::ConnObjDataList
		conn_data_list = m_calibrationDataManager->calibrationData().getConnObjectData(*conn_name_iter);
	      State_t state = conn_data_list.newestValue<State_t>(measurement_type, serial_number, var_def);
	      const StateVarDef_t &state_def = var_def.stateDef();
	      if (!var_item) {
        var_item = new ObjectDataItem(category_item, QStringList(QString::fromStdString(*var_iter)), false);
	      }
	      var_item->setStateValue(state, var_def.id(), state_def, m_palette, object_i);
	    } catch (CalibrationData::MissingConnObject &err) {
	    }catch (...){
	    }
	  }
	  }
	}
      }
      //remove categories for which there are no values
      //if (category_item->firstChild() == NULL) delete category_item;
      if (category_item->child(0) == NULL) delete category_item;
    } 
    //remove serial numbers for which there are no children
    //if ( serial_number_item->firstChild() == NULL ) {
    if ( serial_number_item->child(0) == NULL ) {
      if ( measurement_type == kAnalysis ) {
    //Q3ListViewItem* scan_parent = serial_number_item->parent();
    QTreeWidgetItem* scan_parent = serial_number_item->parent();
	delete serial_number_item;
    //if (scan_parent->firstChild() == NULL) delete scan_parent;
    if (scan_parent->child(0) == NULL) delete scan_parent;
      }
      else delete serial_number_item;
    }
  }

  void ObjectDataView::filter(){
    objectDataListView->clear();
    m_filledRodModuleData=false;
    try {
      std::string filter_string;
      if (filterLineEdit->text().length()>0) {
	filter_string = filterLineEdit->text().toLatin1().data();
	std::string::size_type pos =0;
	while (pos < filter_string.size() && isspace(filter_string[pos])) pos++;
	while (pos < filter_string.size() && (isalpha(filter_string[pos]) || filter_string[pos]=='_') ) pos++;
	if (pos < filter_string.size() && filter_string[pos]==':') {
	  pos++;
	  while (pos < filter_string.size() && isspace(filter_string[pos])) pos++;
	  filter_string = filter_string.substr(pos,filter_string.size()-pos);
	}
	fillListView(0, filter_string);
      }
    } catch (...) {
      std::cout << "WARNING [ObjectDataView::filter] Exception while filtering." << std::endl;
    }   
  }

  void ObjectDataView::clearFilter(){
    filterLineEdit->clear();
    objectDataListView->clear();
    m_filledRodModuleData=false;
    fillListView(0, "");
  }

}//end of namespace PixCon
