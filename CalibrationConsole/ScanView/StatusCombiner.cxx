#include "StatusCombiner.h"
#include <CalibrationDataManager.h>

#include <ConfigWrapper/ConnectivityUtil.h>

namespace PixCon {

  inline void updateEnableState(PixCon::CalibrationData::EnableState_t &dest_enable_state, const PixCon::CalibrationData::EnableState_t &src_enable_state) {
    if (src_enable_state.isInConnectivity()) {
      dest_enable_state.setInConnectivity(true);
    }
    if (src_enable_state.isInConfiguration()) {
      dest_enable_state.setInConnectivity(true);
    }
    if (src_enable_state.isParentEnabled()) {
      dest_enable_state.setParentEnabled(true);
    }
    if (src_enable_state.isTemporaryEnabled()) {
      dest_enable_state.setEnabled(true);
    }
  }


  std::string StatusCombiner::makeValueString(CalibrationData::ConnObjDataList data_list,
					      EMeasurementType measurement,
					      SerialNumber_t serial_number,
					      VarDef_t var_def)
  {
    if ( !var_def.isStateWithOrWithoutHistory()  ) { throw CalibrationData::MissingValue("Not a state"); }

    State_t a_state = data_list.value<State_t>(measurement, serial_number, var_def.id());
    const StateVarDef_t &state_var_def = var_def.stateDef();

    std::stringstream value_string;
    value_string << state_var_def.stateName(a_state) << " ( = " <<  a_state << ")" << std::endl;
    return value_string.str();
  }

  void StatusCombiner::updateValues(EMeasurementType src_measurement_type,
				    SerialNumber_t src_serial_number,
				    EConnItemType conn_item_type,
				    const std::string &conn_name,
				    const std::vector<VarDef_t> &var_def_list)
  {
    try {
      Lock lock(m_calibrationData->calibrationDataMutex());
      CalibrationData::ConnObjDataList conn_obj_data ( m_calibrationData->calibrationData().getConnObjectData(conn_name) );
      const PixCon::CalibrationData::EnableState_t &conn_obj_enable_state(conn_obj_data.enableState(src_measurement_type, src_serial_number));

      // also consider disabled objects
      //      if (!conn_obj_enable_state.enabled()) return;

      PixCon::CalibrationData::EnableState_t &dest_conn_obj_enable_state(conn_obj_data.enableState(kAnalysis, m_analysisSerialNumber));

      bool now_enabled = conn_obj_enable_state.enabled() && !dest_conn_obj_enable_state.enabled();
      //      dest_conn_obj_enable_state = conn_obj_enable_state;
      updateEnableState(dest_conn_obj_enable_state, conn_obj_enable_state);

      for ( std::vector<VarDef_t>::const_iterator var_iter = var_def_list.begin();
	    var_iter != var_def_list.end();
	    var_iter ++) {

	// only states should be in the list by construction.
	  assert ( var_iter->isStateWithOrWithoutHistory() );
            
          // for now only extended states are supported
	  if (!var_iter->isExtendedType()) continue;

	  if ( !match( var_iter->connType(), conn_item_type) ) continue;

	  const ExtendedStateVarDef_t &extended_state_var = var_iter->extendedStateDef();
	  State_t dest_state = extended_state_var.missingState();
	  try {
	    dest_state = conn_obj_data.newestValue<State_t>(kAnalysis, m_analysisSerialNumber, *var_iter);
	  }
	  catch (CalibrationData::MissingValue &) {
	  }

	  try {
	    State_t a_state = conn_obj_data.newestValue<State_t>(src_measurement_type, src_serial_number, *var_iter);

	    // update state if the current state is "missing" 
	    // or the conn object was disabled at the time the last value was set and is now enabled,
	    // or the precedence of the new state is higher provided both, the new and the old state, indicate either 
	    // success or not.

	    if (   now_enabled
                   || dest_state == extended_state_var.missingState() 
		   || (   extended_state_var.precedence( a_state ) >  extended_state_var.precedence( dest_state ) 
		       && a_state != extended_state_var.missingState() )) {

	      if ( now_enabled || !extended_state_var.indicatesSuccess( dest_state ) ||  extended_state_var.indicatesSuccess( a_state )) {

		std::cout << "INFO [ValueCombiner::updateValues] add for " << conn_name << " : " << VarDefList::instance()->varName( var_iter->id())
			  << " = " << a_state
			  << ( dest_conn_obj_enable_state.enabled() ?  " (enabled) " : " (disabled)" )
			  << std::endl;

		m_calibrationData->calibrationData().addValue(conn_name,
							      kAnalysis,
							      m_analysisSerialNumber, 
							      *var_iter,
							      a_state);

		// memorise the measurement which defined this value
		m_log[VarDefList::instance()->varName( var_iter->id())][conn_name]=std::make_pair(src_measurement_type, src_serial_number);

	      }
	    }
	  }
	  catch (CalibrationData::MissingValue &){
	  }
	}
    }
    catch (CalibrationData::MissingConnObject &) {
    }
  }

  MetaData_t::EStatus StatusCombiner::createGlobalStatus()
  {

    PixA::ConnectivityRef conn( m_calibrationData->connectivity(kAnalysis, m_analysisSerialNumber) );
    if (!conn) return MetaData_t::kUnknown;

    std::set<std::string> var_list;
    m_calibrationData->calibrationData().varList(kAnalysis, m_analysisSerialNumber, var_list);
    std::vector<VarDef_t> var_def_list;
    std::vector<State_t>  global_state;

    // get all extended state variables
    for (std::set<std::string>::const_iterator var_iter = var_list.begin();
	 var_iter != var_list.end();
	 var_iter++) {
      VarDef_t a_var_def = VarDefList::instance()->getVarDef(*var_iter);
      if (a_var_def.isStateWithOrWithoutHistory() && a_var_def.isExtendedType()) {
	var_def_list.push_back( a_var_def );
      }
    }

    for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
	 crate_iter != conn.crates().end();
	 ++crate_iter) {

      PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
      PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
      for (PixA::RodList::const_iterator rod_iter = rod_begin;
	   rod_iter != rod_end;
	   ++rod_iter) {

	globalState(kRodItem,
		    PixA::connectivityName(*rod_iter),
		    var_def_list,
		    global_state);

	PixA::Pp0List::const_iterator pp0_begin = PixA::pp0Begin(rod_iter);
	PixA::Pp0List::const_iterator pp0_end = PixA::pp0End(rod_iter);
	for (PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
	     pp0_iter != pp0_end;
	     ++pp0_iter) {

	  //       globalState(calibration_data,
	  // 			analysis_serial_number,
	  // 			PixA::connectivityName(*rod_iter),
	  // 			var_def_list,
	  // 			global_state);

	  PixA::ModuleList::const_iterator module_begin = PixA::modulesBegin(pp0_iter);
	  PixA::ModuleList::const_iterator module_end = PixA::modulesEnd(pp0_iter);
	  for (PixA::ModuleList::const_iterator module_iter = module_begin;
	       module_iter != module_end;
	       ++module_iter) {

	    globalState(kModuleItem,
			PixA::connectivityName(*module_iter),
			var_def_list,
			global_state);

	  }//pp0
	}//rod
      }//crate
    }


    MetaData_t::EStatus overall_status = MetaData_t::kUnset;

    std::vector<State_t>::iterator global_state_iter = global_state.begin();
    for ( std::vector<VarDef_t>::const_iterator var_iter = var_def_list.begin();
	  var_iter != var_def_list.end();
	  var_iter++) {
      assert( var_iter->isStateWithOrWithoutHistory() && var_iter->isExtendedType() );
      const ExtendedStateVarDef_t &extended_state_var = var_iter->extendedStateDef();
      if (*global_state_iter == extended_state_var.missingState() ) {
	overall_status = MetaData_t::combineStatus(overall_status, MetaData_t::kUnknown );
	std::cout << "INFO [StatusCombiner::createGlobalStatus] "
		  << PixCon::makeSerialNumberString(kAnalysis, m_analysisSerialNumber)
		  << " global " << VarDefList::instance()->varName( var_iter->id()) << " = " << overall_status << std::endl;
	break;
      }
      else if ( !extended_state_var.indicatesSuccess( *global_state_iter )) {
	overall_status = MetaData_t::combineStatus(overall_status, MetaData_t::kFailed );
	std::cout << "INFO [StatusCombiner::createGlobalStatus] "
		  << PixCon::makeSerialNumberString(kAnalysis, m_analysisSerialNumber)
		  << " global " << VarDefList::instance()->varName( var_iter->id()) << " = " << overall_status << std::endl;
      }
      else {
	overall_status = MetaData_t::combineStatus(overall_status, MetaData_t::kOk );
      }
      global_state.push_back( extended_state_var.missingState());
    }
    std::cout << "INFO [StatusCombiner::createGlobalStatus] "
	      << PixCon::makeSerialNumberString(kAnalysis, m_analysisSerialNumber)
	      << " global status = " << overall_status << std::endl;
    return overall_status;
  }


  void StatusCombiner::globalState(EConnItemType conn_item_type,
				   const std::string &conn_name,
				   const std::vector<VarDef_t> &var_def_list,
				   std::vector<State_t> &global_state)
  {

    try {
      Lock lock(m_calibrationData->calibrationDataMutex());
      CalibrationData::ConnObjDataList conn_obj_data ( m_calibrationData->calibrationData().getConnObjectData(conn_name) );
      if (!conn_obj_data.enableState(kAnalysis, m_analysisSerialNumber).enabled())  return;

      bool first;
      if (global_state.size() != var_def_list.size()) {
	first=true;
	for ( std::vector<VarDef_t>::const_iterator var_iter = var_def_list.begin();
	      var_iter != var_def_list.end();
	      var_iter++) {
	  assert( var_iter->isStateWithOrWithoutHistory() && var_iter->isExtendedType() );
	  const ExtendedStateVarDef_t &extended_state_var = var_iter->extendedStateDef();
	  State_t init_state=0;
	  for (unsigned int state_i=1; state_i < extended_state_var.nStates(); state_i++) {
	    if (extended_state_var.indicatesSuccess( state_i ) && 
		(extended_state_var.precedence( state_i ) > extended_state_var.precedence( init_state )
		 || !extended_state_var.indicatesSuccess( init_state ))) {
	      init_state = state_i;
	    }
	  }
	  global_state.push_back( init_state );
	}
      }
      else {
	first=false;
      }


      std::vector<State_t>::iterator global_state_iter = global_state.begin();
      for ( std::vector<VarDef_t>::const_iterator var_iter = var_def_list.begin();
	    var_iter != var_def_list.end();
	    var_iter++, global_state_iter++) {

	if ( !match( var_iter->connType(), conn_item_type) ) continue;

	// paranoia check
	assert( global_state_iter != global_state.end() );

	const ExtendedStateVarDef_t &extended_state_var = var_iter->extendedStateDef();
	try {
	  State_t a_state = conn_obj_data.newestValue<State_t>(kAnalysis, m_analysisSerialNumber, *var_iter);
	  if ( first || extended_state_var.precedence( a_state ) < extended_state_var.precedence( *global_state_iter )) {
	    *global_state_iter = a_state;
	    std::cout << "INFO [StatusCombiner::globalState] "
		      << PixCon::makeSerialNumberString(kAnalysis, m_analysisSerialNumber)
		      << conn_name << " " << VarDefList::instance()->varName( var_iter->id()) << " = " << a_state << std::endl;
	  }
	}
	catch (CalibrationData::MissingValue &) {
	  if ( extended_state_var.precedence( extended_state_var.missingState() ) <  extended_state_var.precedence( *global_state_iter )
	       || extended_state_var.indicatesSuccess( *global_state_iter ) ) {
	    *global_state_iter = extended_state_var.missingState();
	    std::cout << "INFO [StatusCombiner::globalState] "
		      << PixCon::makeSerialNumberString(kAnalysis, m_analysisSerialNumber)
		      << conn_name << " " << VarDefList::instance()->varName( var_iter->id()) << " = " << *global_state_iter << std::endl;
	  }
	}
      }
    }
    catch (CalibrationData::MissingConnObject &) {
//       // mark all variables as missing
//       std::vector<State_t>::iterator global_state_iter = global_state.begin();
//       for ( std::vector<VarDef_t>::const_iterator var_iter = var_def_list.begin();
// 	    var_iter != var_def_list.end();
// 	    var_iter++, global_state_iter++) {

// 	// paranoia check
// 	assert( global_state_iter != global_state.end() );

// 	const ExtendedStateVarDef_t &extended_state_var = var_iter->extendedStateDef();
// 	if ( extended_state_var.precedence( extended_state_var.missingState() ) >  extended_state_var.precedence( *global_state_iter )) {
// 	  *global_state_iter = extended_state_var.missingState();
// 	}
//       }

    }
  }

}
