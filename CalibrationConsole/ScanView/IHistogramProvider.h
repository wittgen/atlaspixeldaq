// Dear emacs, this is -*-c++-*-
#ifndef _PixCon_IHistogramProvider_h_
#define _PixCon_IHistogramProvider_h_

#include <vector>
#include <string>
#include <DataContainer/HistoInfo_t.h>
#include <CalibrationDataTypes.h>

class TH1;

namespace PixCon {

  typedef TH1 Histo_t;                      /**< the histogram type. */
  typedef Histo_t *HistoPtr_t;              /**< pointer of a histogram. */

#ifndef _PixCon_CalibrationData_h_
  typedef unsigned int SerialNumber_t;
#endif
  typedef std::pair<EMeasurementType,SerialNumber_t> FullKey_t; /* to deal with histograms in analyses, we need to be specify both type & serial number */

  typedef std::vector<unsigned int> Index_t; /**< multi dimensional index which describes the location of a histogram in
						  a histogram array. */

  /** Basic interface for the histogram server.
   */
  class IHistogramProvider
  {
  public:
    virtual ~IHistogramProvider() {}

    /** Load a histogram from the server.
     * @param type the type of the measurement : scan, analysis or current.
     * @parem serial_number the serial number of the scan.
     * @param conn_item_type the connectivity item type of the histogram.
     * @param connectivity_name connectivity name of the module.
     * @param histo_name name of the histogram.
     * @param index vector which contains the indices for the pix scan histo.
     */
    virtual HistoPtr_t loadHistogram(EMeasurementType type,
				     SerialNumber_t serial_number,
				     EConnItemType conn_item_type,
				     const std::string &connectivity_name,
				     const std::string &histo_name,
				     const Index_t &index) = 0;

    /** Request the names of all histograms assigned to the given serial number and connectivity object type.
     * @param type the type of the measurement : scan, analysis or current.
     * @param serial_number the serial number of the scan.
     * @param conn_item_type the type of the connectivity item for which the histogram list should be requested.
     * @param histogram_name_list vector which will be extended by the histogram names associated to the serial number and object type.
     * Note, it is the responsibility of the caller to clear the given histogram name list before the call if the old content is not desired.
     * This allows to add histograms of different types in subsequent calls.
     * @sa EConnItemType
     */
    virtual void requestHistogramList(EMeasurementType type,
				      SerialNumber_t serial_number,
				      EConnItemType conn_item_type,
				      std::vector< std::string > &histogram_name_list ) = 0;

    /** Request the names of all histograms assigned to the given serial number and connectivity object type where the request can be restricted to the given ROD.
     * @param type the type of the measurement : scan, analysis or current.
     * @param serial_number the serial number of the scan.
     * @param conn_item_type the type of the connectivity item for which the histogram list should be requested.
     * @param histogram_name_list vector which will be extended by the histogram names associated to the serial number and object type.
     * @param rod_name the name of the ROD which can be used to restrict
     * Note, it is the responsibility of the caller to clear the given histogram name list before the call if the old content is not desired.
     * This allows to add histograms of different types in subsequent calls.
     * @sa EConnItemType
     */
    virtual void requestHistogramList(EMeasurementType type,
				      SerialNumber_t serial_number,
				      EConnItemType conn_item_type,
				      std::vector< std::string > &histogram_name_list,
				      const std::string &rod_name) = 0;


    /** Return the dimensions of the pix scan histo levels.
     * @param type the type of the measurement : scan, analysis or current.
     * @parem serial_number the serial number of the scan.
     * @param conn_item_type the connectivity item type of the histogram.
     * @param connectivity_name connectivity name of the module.
     * @param histo_name name of the histogram.
     * @param index vector which will be filled with the dimensions of the pix scan histo levels.
     */
    virtual void numberOfHistograms(EMeasurementType type,
				    SerialNumber_t serial_number,
				    EConnItemType conn_item_type,
				    const std::string &connectivity_name,
				    const std::string &histo_name,
				    HistoInfo_t &index) = 0;

    /** clear the data cacehd for the given measurement if any
     */
    virtual void clearCache(EMeasurementType /*measurement_type*/, SerialNumber_t /*serial_number*/) {}

    virtual void reset() = 0;
  };

}

#endif
