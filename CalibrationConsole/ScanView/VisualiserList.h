#ifndef _PixA_VisualiserList_h_
#define _PixA_VisualiserList_h_

#include <map>
#include <vector>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <stdexcept>
#include <CalibrationDataTypes.h>
#include <CalibrationDataUtil.h>
#include <ConnItemCollection.h>

#include <memory>

namespace PixA {
  class IVisualiser;
}

namespace PixCon {


  class VisualiserListRef_t;
  class VisualiserList;
  class VisualiserList_t;

  class VisualiserList_t
  {
    friend class VisualiserListRef_t;
    friend class VisualiserList;
    friend class ConnItemCollection<VisualiserList_t>;
    friend class std::vector<VisualiserList_t>;
  public:
    VisualiserList_t() : m_nHistograms(0), m_counter(0), m_unusedCounter(0) {}

  protected:
    /** Set the visualiser for the given histogram id.
     */
    void setVisualiser(unsigned int histo_id, std::shared_ptr<PixA::IVisualiser> &visualiser) {
      if (histo_id>=m_visualiserList.size()) {
	if (histo_id>=m_visualiserList.size()+100) {
	  std::stringstream message;
	  message << "ERROR [VisualiserList_t::visualiser] unreasonable histogram id : " << histo_id
		  << ". Currently the largest histgram id is " << m_visualiserList.size() << " .";
	  throw std::range_error("ERROR [VisualiserList_t::visualiser] ");
	}
	m_visualiserList.resize( histo_id+1 );
      } 
      m_visualiserList[histo_id] = visualiser;
    }

    /** Get teh visualiser for the given hsitogram id.
     * The histogram id has to be valid.
     */
    std::shared_ptr<PixA::IVisualiser> visualiser(unsigned int histo_id) const {

      if (histo_id>=m_visualiserList.size()) {
	throw std::range_error("ERROR [VisualiserList_t::visualiser] No visualiser for given histogram. Histogram index too larege.");
      }
      m_counter++;
      return m_visualiserList[histo_id];
    }

    /** The counter should be cleared from time to time.
     * @return true if the visualiser list has been used recently
     * If the counter is zero for consecutive clearing rounds, the visualiser list could be deleted.
     */
    bool clearUseCount() {
      if (m_locked>0) return true;

      bool is_used=isUsed();
      if (m_counter==0) {
	m_unusedCounter++;
      }
      else {
	m_unusedCounter=0;
      }
      m_counter=0;
      return is_used;
    }

    unsigned int nHistogramsConsidered() const                { return m_nHistograms; }
    void  setNHistogramsConsidered(unsigned int n_histograms) { m_nHistograms = n_histograms; }

    /** Return true if a visualiser of this list has been used recently.
     */
    bool isUsed() const { return m_counter>0 && m_unusedCounter>s_unusedThreshold;}

  protected:
    /** Get the use counter.
     * This counter will be incremented whenever a visualiser is used and cleared on @ref clearCounter.
     */
    unsigned int counter() const { return m_counter; }

  private:
    unsigned int m_nHistograms;
    std::vector< std::shared_ptr< PixA::IVisualiser > > m_visualiserList;
    mutable unsigned int m_counter;
    unsigned int m_unusedCounter;
    mutable unsigned int m_locked;

    static const unsigned int s_unusedThreshold = 2;
  };

  /** Reference to a visualiser list.
   * Will prevent the referenced visualiser list from being cleaned up.
   */ 
  class VisualiserListRef_t 
  {
    friend class VisualiserList;
  protected:

    /** Create a reference of the given list.
     */
    VisualiserListRef_t(const VisualiserList_t &list)
      : m_list(&list)
    {
      m_list->m_locked++;
    }

  public:

    /** Create an invalid reference.
     */
    VisualiserListRef_t() : m_list(NULL) {}

    /** Copy from a reference
     */
    VisualiserListRef_t(const VisualiserListRef_t &ref)
      : m_list(ref.m_list)
    {
      if (m_list) {
	m_list->m_locked++;
      }
    }

    ~VisualiserListRef_t() {
      if (m_list) {
	m_list->m_locked--;
      }
    }

    /** Return true if the reference is valid.
     */
    operator bool() const{ return m_list != NULL; }

    /** Get teh visualiser for the given hsitogram id.
     * The histogram id has to be valid.
     */
    std::shared_ptr<PixA::IVisualiser> visualiser(unsigned int histo_id) const {
      return m_list->visualiser(histo_id);
    }

  private:
    const VisualiserList_t *m_list;
  };


  /** Manages the visualiser lists for different scans.
   */
  class VisualiserList
  {
  public:

    VisualiserList() {};
    ~VisualiserList() {};

    const VisualiserListRef_t createVisualiserList(EMeasurementType type,
						   SerialNumber_t serial_number,
						   EConnItemType conn_item_type,
						   const std::vector<std::string> &histogram_name_list);

    const VisualiserListRef_t visualiserList(EMeasurementType type,
					     SerialNumber_t serial_number,
					     EConnItemType conn_item_type) const {
      assert( type < kNMeasurements );
      std::map<SerialNumber_t, ConnItemCollection<VisualiserList_t> >::const_iterator list_iter = m_visualiserMap[type].find(serial_number);
      if (list_iter == m_visualiserMap[type].end()) {
	std::stringstream message;
	message << "ERROR [VisualiserList::visualiserList] No visualiser for " << makeSerialNumberString(type,serial_number);
	throw std::runtime_error( message.str() );
      }
      return list_iter->second[conn_item_type];
    }

    std::shared_ptr<PixA::IVisualiser> visualiser(EMeasurementType type,
						    SerialNumber_t serial_number,
						    EConnItemType conn_item_type,
						    unsigned int histo_id) const {
      std::map<SerialNumber_t, ConnItemCollection<VisualiserList_t> >::const_iterator list_iter = m_visualiserMap[type].find(serial_number);
      if (list_iter == m_visualiserMap[type].end()) {
	return std::shared_ptr<PixA::IVisualiser>();
      }
      return list_iter->second[conn_item_type].visualiser(histo_id);
    }

  protected:

    std::map<SerialNumber_t, ConnItemCollection<VisualiserList_t> >::iterator beginList(EMeasurementType type)
    { assert( type < kNMeasurements ); return m_visualiserMap[type].begin(); }

    std::map<SerialNumber_t, ConnItemCollection<VisualiserList_t> >::const_iterator endList(EMeasurementType type) const
    { assert( type < kNMeasurements ); return m_visualiserMap[type].end(); }

    std::map<SerialNumber_t, ConnItemCollection<VisualiserList_t> >::const_iterator findList(EMeasurementType type, SerialNumber_t serial_number) const {
      assert( type < kNMeasurements );
      return m_visualiserMap[type].find(serial_number);
    }

    std::map<SerialNumber_t, ConnItemCollection<VisualiserList_t> >::iterator findList(EMeasurementType type, SerialNumber_t serial_number)  {
      assert( type < kNMeasurements );
      return m_visualiserMap[type].find(serial_number);
    }

    void eraseList(EMeasurementType type, std::map<SerialNumber_t, ConnItemCollection<VisualiserList_t> >::iterator &list_iter)
    { assert( type < kNMeasurements); m_visualiserMap[type].erase(list_iter); }
  protected:
  private:
    std::map<SerialNumber_t, ConnItemCollection<VisualiserList_t> > m_visualiserMap[kNMeasurements];
  };
}
#endif
