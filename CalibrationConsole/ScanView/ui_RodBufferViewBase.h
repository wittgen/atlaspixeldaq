#ifndef UI_RODBUFFERVIEWBASE_H
#define UI_RODBUFFERVIEWBASE_H

#include <QButtonGroup>
#include <QGroupBox>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QDialog>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QRadioButton>
#include <QSpacerItem>
#include <QSplitter>
#include <QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_RodBufferViewBase
{
public:
    QVBoxLayout *vboxLayout;
    QLabel *titleLable;
    QHBoxLayout *hboxLayout;
    QHBoxLayout *hboxLayout1;
    QHBoxLayout *hboxLayout2;
    QLabel *textLabel1;
    QLineEdit *partitionLineEdit;
    QPushButton *getBufferPushButton;
    //Q3ButtonGroup *m_bufferTypeSelector;
    QButtonGroup *m_BGbufferTypeSelector;
    QGroupBox * m_bufferTypeSelector;
    QHBoxLayout *hboxLayout3;
    QRadioButton *m_infoBufferButton;
    QRadioButton *m_errorBufferButton;
    QRadioButton *m_diagBufferButton;
    QRadioButton *m_xferBufferButton;
    QSpacerItem *spacer4;
    QSplitter *m_bookmarkSplitter;
    //Q3ListView *m_bookmarkList;
    //Q3ListView *bufferListView;
    QTreeWidget *m_bookmarkList;
    QTreeWidgetItem *m_bookmarkListHeader;
    QTreeWidget *bufferListView;
    QTreeWidgetItem *bufferListViewHeader;
    QHBoxLayout *hboxLayout4;
    QLabel *textLabel1_2;
    QLineEdit *m_findTextEntry;
    QSpacerItem *spacer3_2;
    QPushButton *closeButton;

    void setupUi(QDialog *RodBufferViewBase)
    {
        if (RodBufferViewBase->objectName().isEmpty())
            RodBufferViewBase->setObjectName(QString::fromUtf8("RodBufferViewBase"));
        RodBufferViewBase->resize(670, 516);
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(7), static_cast<QSizePolicy::Policy>(7));
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(RodBufferViewBase->sizePolicy().hasHeightForWidth());
        RodBufferViewBase->setSizePolicy(sizePolicy);
        vboxLayout = new QVBoxLayout(RodBufferViewBase);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        titleLable = new QLabel(RodBufferViewBase);
        titleLable->setObjectName(QString::fromUtf8("titleLable"));
        QSizePolicy sizePolicy1(static_cast<QSizePolicy::Policy>(5), static_cast<QSizePolicy::Policy>(1));
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(titleLable->sizePolicy().hasHeightForWidth());
        titleLable->setSizePolicy(sizePolicy1);
        QFont font;
        font.setPointSize(14);
        font.setBold(true);
        titleLable->setFont(font);
        titleLable->setIndent(16);
        titleLable->setWordWrap(false);

        vboxLayout->addWidget(titleLable);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setSpacing(6);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        hboxLayout2 = new QHBoxLayout();
        hboxLayout2->setSpacing(6);
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        textLabel1 = new QLabel(RodBufferViewBase);
        textLabel1->setObjectName(QString::fromUtf8("textLabel1"));
        textLabel1->setWordWrap(false);

        hboxLayout2->addWidget(textLabel1);


        hboxLayout1->addLayout(hboxLayout2);

        partitionLineEdit = new QLineEdit(RodBufferViewBase);
        partitionLineEdit->setObjectName(QString::fromUtf8("partitionLineEdit"));
        QSizePolicy sizePolicy2(static_cast<QSizePolicy::Policy>(0), static_cast<QSizePolicy::Policy>(0));
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(partitionLineEdit->sizePolicy().hasHeightForWidth());
        partitionLineEdit->setSizePolicy(sizePolicy2);

        hboxLayout1->addWidget(partitionLineEdit);


        hboxLayout->addLayout(hboxLayout1);

        getBufferPushButton = new QPushButton(RodBufferViewBase);
        getBufferPushButton->setObjectName(QString::fromUtf8("getBufferPushButton"));
        getBufferPushButton->setAutoDefault(false);

        hboxLayout->addWidget(getBufferPushButton);

        m_bufferTypeSelector = new QGroupBox(RodBufferViewBase);
        m_bufferTypeSelector->setObjectName(QString::fromUtf8("m_bufferTypeSelector"));
        //m_bufferTypeSelector->setFrameShape(Q3GroupBox::NoFrame);
        //m_bufferTypeSelector->setFrameShadow(Q3GroupBox::Sunken);
        //m_bufferTypeSelector->setLineWidth(0);
        m_bufferTypeSelector->setAlignment(Qt::AlignBottom);
        m_bufferTypeSelector->setFlat(true);
        m_bufferTypeSelector->setProperty("selectedId", QVariant(0));
        //m_bufferTypeSelector->setColumnLayout(0, Qt::Vertical);
        hboxLayout3 = new QHBoxLayout();
        QBoxLayout *boxlayout = qobject_cast<QBoxLayout *>(m_bufferTypeSelector->layout());
        if (boxlayout){
            boxlayout->addLayout(hboxLayout3);
	    m_bufferTypeSelector->layout()->setSpacing(6);
	    m_bufferTypeSelector->layout()->setContentsMargins(11, 11, 11, 11);
	}
        hboxLayout3->setAlignment(Qt::AlignTop);
        hboxLayout3->setObjectName(QString::fromUtf8("hboxLayout3"));
        m_infoBufferButton = new QRadioButton(m_bufferTypeSelector);
        m_infoBufferButton->setObjectName(QString::fromUtf8("m_infoBufferButton"));
        m_infoBufferButton->setChecked(true);
        //m_bufferTypeSelector->insert(m_infoBufferButton, 0);

        hboxLayout3->addWidget(m_infoBufferButton);

        m_errorBufferButton = new QRadioButton(m_bufferTypeSelector);
        m_errorBufferButton->setObjectName(QString::fromUtf8("m_errorBufferButton"));
        QSizePolicy sizePolicy3(static_cast<QSizePolicy::Policy>(1), static_cast<QSizePolicy::Policy>(5));
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(m_errorBufferButton->sizePolicy().hasHeightForWidth());
        m_errorBufferButton->setSizePolicy(sizePolicy3);
        //m_bufferTypeSelector->insert(m_errorBufferButton, 1);

        hboxLayout3->addWidget(m_errorBufferButton);

        m_diagBufferButton = new QRadioButton(m_bufferTypeSelector);
        m_diagBufferButton->setObjectName(QString::fromUtf8("m_diagBufferButton"));
        QSizePolicy sizePolicy4(static_cast<QSizePolicy::Policy>(1), static_cast<QSizePolicy::Policy>(0));
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(m_diagBufferButton->sizePolicy().hasHeightForWidth());
        m_diagBufferButton->setSizePolicy(sizePolicy4);
        //m_bufferTypeSelector->insert(m_diagBufferButton, 2);

        hboxLayout3->addWidget(m_diagBufferButton);

        m_xferBufferButton = new QRadioButton(m_bufferTypeSelector);
        m_xferBufferButton->setObjectName(QString::fromUtf8("m_xferBufferButton"));
        //m_bufferTypeSelector->insert(m_xferBufferButton, 3);

        hboxLayout3->addWidget(m_xferBufferButton);

        m_BGbufferTypeSelector = new QButtonGroup(m_bufferTypeSelector);
        m_BGbufferTypeSelector->addButton(m_infoBufferButton,0);
        m_BGbufferTypeSelector->addButton(m_errorBufferButton,1);
        m_BGbufferTypeSelector->addButton(m_diagBufferButton,2);
        m_BGbufferTypeSelector->addButton(m_xferBufferButton,3);

        m_bufferTypeSelector->setLayout(hboxLayout3);

        hboxLayout->addWidget(m_bufferTypeSelector);

        spacer4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacer4);


        vboxLayout->addLayout(hboxLayout);

        m_bookmarkSplitter = new QSplitter(RodBufferViewBase);
        m_bookmarkSplitter->setObjectName(QString::fromUtf8("m_bookmarkSplitter"));
        sizePolicy.setHeightForWidth(m_bookmarkSplitter->sizePolicy().hasHeightForWidth());
        m_bookmarkSplitter->setSizePolicy(sizePolicy);
        m_bookmarkSplitter->setMinimumSize(QSize(408, 66));
        m_bookmarkSplitter->setOrientation(Qt::Horizontal);
        //m_bookmarkList = new Q3ListView(m_bookmarkSplitter);
        m_bookmarkList = new QTreeWidget(m_bookmarkSplitter);
        //m_bookmarkList->addColumn(QApplication::translate("RodBufferViewBase", "Bookmarks", 0));
        m_bookmarkList->setColumnCount(1);
        m_bookmarkListHeader = new QTreeWidgetItem;
        m_bookmarkListHeader->setText(0,"Bookmarks");
        //m_bookmarkList->header()->setClickEnabled(true, m_bookmarkList->header()->count() - 1);
        //m_bookmarkList->header()->setResizeEnabled(true, m_bookmarkList->header()->count() - 1);
        m_bookmarkList->header()->resizeSections(QHeaderView::ResizeToContents);
#if QT_VERSION < 0x050000
        m_bookmarkList->header()->setClickable(true);
#else
        m_bookmarkList->header()->setSectionsClickable(true);
#endif
        m_bookmarkList->setObjectName(QString::fromUtf8("m_bookmarkList"));
        sizePolicy.setHeightForWidth(m_bookmarkList->sizePolicy().hasHeightForWidth());
        m_bookmarkList->setSizePolicy(sizePolicy);
        m_bookmarkList->setMinimumSize(QSize(100, 0));
        m_bookmarkSplitter->addWidget(m_bookmarkList);

        bufferListView = new QTreeWidget(m_bookmarkSplitter);
        bufferListView->setObjectName(QString::fromUtf8("bufferListView"));
        QSizePolicy sizePolicy5(static_cast<QSizePolicy::Policy>(3), static_cast<QSizePolicy::Policy>(7));
        sizePolicy5.setHorizontalStretch(0);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(bufferListView->sizePolicy().hasHeightForWidth());
        bufferListView->setSizePolicy(sizePolicy5);
        bufferListView->setMinimumSize(QSize(300, 0));
        m_bookmarkSplitter->addWidget(bufferListView);

        vboxLayout->addWidget(m_bookmarkSplitter);

        hboxLayout4 = new QHBoxLayout();
        hboxLayout4->setSpacing(6);
        hboxLayout4->setObjectName(QString::fromUtf8("hboxLayout4"));
        textLabel1_2 = new QLabel(RodBufferViewBase);
        textLabel1_2->setObjectName(QString::fromUtf8("textLabel1_2"));
        textLabel1_2->setWordWrap(false);

        hboxLayout4->addWidget(textLabel1_2);

        m_findTextEntry = new QLineEdit(RodBufferViewBase);
        m_findTextEntry->setObjectName(QString::fromUtf8("m_findTextEntry"));
        QSizePolicy sizePolicy6(static_cast<QSizePolicy::Policy>(7), static_cast<QSizePolicy::Policy>(0));
        sizePolicy6.setHorizontalStretch(0);
        sizePolicy6.setVerticalStretch(0);
        sizePolicy6.setHeightForWidth(m_findTextEntry->sizePolicy().hasHeightForWidth());
        m_findTextEntry->setSizePolicy(sizePolicy6);

        hboxLayout4->addWidget(m_findTextEntry);

        spacer3_2 = new QSpacerItem(70, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        hboxLayout4->addItem(spacer3_2);

        closeButton = new QPushButton(RodBufferViewBase);
        closeButton->setObjectName(QString::fromUtf8("closeButton"));
        closeButton->setAutoDefault(false);

        hboxLayout4->addWidget(closeButton);


        vboxLayout->addLayout(hboxLayout4);

        QWidget::setTabOrder(partitionLineEdit, getBufferPushButton);
        QWidget::setTabOrder(getBufferPushButton, bufferListView);
        QWidget::setTabOrder(bufferListView, closeButton);

        retranslateUi(RodBufferViewBase);
        QObject::connect(closeButton, SIGNAL(clicked()), RodBufferViewBase, SLOT(close()));
        QObject::connect(getBufferPushButton, SIGNAL(clicked()), RodBufferViewBase, SLOT(fillListView()));
        QObject::connect(m_bookmarkList, SIGNAL(clicked(QTreeWidgetItem*)), RodBufferViewBase, SLOT(selectLine(QTreeWidgetItem*)));
        QObject::connect(bufferListView, SIGNAL(clicked(QTreeWidgetItem*)), RodBufferViewBase, SLOT(setCursor(QTreeWidgetItem*)));
        QObject::connect(m_findTextEntry, SIGNAL(returnPressed()), RodBufferViewBase, SLOT(find()));
        QObject::connect(partitionLineEdit, SIGNAL(returnPressed()), RodBufferViewBase, SLOT(fillListView()));
        QObject::connect(m_bufferTypeSelector, SIGNAL(pressed(int)), RodBufferViewBase, SLOT(changeBufferType(int)));

        QMetaObject::connectSlotsByName(RodBufferViewBase);
    } // setupUi

    void retranslateUi(QDialog *RodBufferViewBase)
    {
        RodBufferViewBase->setWindowTitle(QApplication::translate("RodBufferViewBase", "RodBufferView", 0));
        titleLable->setText(QApplication::translate("RodBufferViewBase", "Title", 0));
        textLabel1->setText(QApplication::translate("RodBufferViewBase", "<b>Get ROD Buffer for Partition: </b>", 0));
        getBufferPushButton->setText(QApplication::translate("RodBufferViewBase", "Get ROD Buffer", 0));
        m_bufferTypeSelector->setTitle(QString());
        m_infoBufferButton->setText(QApplication::translate("RodBufferViewBase", "Info", 0));
        m_errorBufferButton->setText(QApplication::translate("RodBufferViewBase", "Error", 0));
        m_diagBufferButton->setText(QApplication::translate("RodBufferViewBase", "Diag", 0));
        m_xferBufferButton->setText(QApplication::translate("RodBufferViewBase", "Xfer", 0));
        //m_bookmarkList->header()->setLabel(0, QApplication::translate("RodBufferViewBase", "Bookmarks", 0));
        textLabel1_2->setText(QApplication::translate("RodBufferViewBase", "<u>F</u>ind :", 0));
        closeButton->setText(QApplication::translate("RodBufferViewBase", "Close", 0));
    } // retranslateUi

};

namespace Ui {
    class RodBufferViewBase: public Ui_RodBufferViewBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RODBUFFERVIEWBASE_H
