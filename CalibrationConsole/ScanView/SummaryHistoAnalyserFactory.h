#ifndef _SummaryHistoAnalyserFactory_H_
#define _SummaryHistoAnalyserFactory_H_

#include <ObjectFactoryImpl.h>
#include <memory>
#include <CalibrationDataTypes.h>
#include <IHistogramProvider.h>

namespace PixCon {

  class CalibrationDataManager;
  class Histo;
  class CategoryList;

  class ISummaryHistoAnalyser
  {
  public:
    virtual ~ISummaryHistoAnalyser() {}
    virtual void analyseHistogram(EMeasurementType measurement_type, SerialNumber_t serial_number, const std::string &rod_name, const PixCon::Histo_t *a_histo) = 0;
  };

  class ISummaryHistoAnalyserKit {
  public:
    virtual ~ISummaryHistoAnalyserKit() {}
    virtual ISummaryHistoAnalyser *create(const std::shared_ptr<PixCon::CategoryList> &m_categories,
					  const std::shared_ptr<PixCon::CalibrationDataManager> &calibration_data) const = 0;
  };

  typedef ObjectFactoryImpl<ISummaryHistoAnalyserKit>  SummaryHistoAnalyserFactory;

}
#endif
