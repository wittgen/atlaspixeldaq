#ifndef _PixCon_RodBufferView_H_
#define _PixCon_RodBufferView_H_
#include <QTreeWidgetItem>
#include "ui_RodBufferViewBase.h"
#include <vector>

namespace PixCon {
  class RodBufferView : public QDialog, Ui_RodBufferViewBase
    {
      Q_OBJECT
	
	public:
      RodBufferView( std::string rod_name, QWidget* parent = 0 );
      ~RodBufferView();

    public slots:

    
void fillListView();
void selectLine(/*Q3ListViewItem*/QTreeWidgetItem *current_item);
void setCursor(/*Q3ListViewItem*/QTreeWidgetItem *current_item);
void find();
void changeBufferType(int);
public:
    protected:
    void chooseBuffer();
   // void fillListView();
    void fillListView(int buffer_type_id);
    void setTitle();
  //  void find();
  //  void selectLine(Q3ListViewItem *current_item);
    void selectLine(int line);
   // void setCursor(Q3ListViewItem *current_item);
  //  void changeBufferType(int);
    
    private:
    std::string m_rodName;
    std::vector<std::string> m_pattern;
};
}
#endif
