#ifndef _PixCon_ObjectFactoryImpl_h_
#define _PixCon_ObjectFactoryImpl_h_

#include "IObjectFactory.h"
#include <map>
#include <memory>
#include <ConfigWrapper/Regex_t.h>

// for debugging
#include <iostream>

namespace PixCon {

  template <class T>
  class ObjectFactoryImpl : public IObjectFactory<T>
  {
  protected:
    ~ObjectFactoryImpl() {}

    const T * const _kit(const std::string &name) const {
      for (typename KitList_t::const_iterator kit_iter = m_kitList.begin();
	   kit_iter != m_kitList.end();
	   kit_iter++) {
	if (kit_iter->second.first->match(name)) {
	  return kit_iter->second.second.get();
	}
      }
      return NULL;
    }

    void _kitList(const std::string &name, std::vector< std::shared_ptr<T> > &a_list)  {
      for (typename KitList_t::const_iterator kit_iter = m_kitList.begin();
	   kit_iter != m_kitList.end();
	   kit_iter++) {
	if (kit_iter->second.first->match(name)) {
	  a_list.push_back(kit_iter->second.second);
	}
      }
    }

    void _registerKit( const std::string &pattern, T *a_kit, unsigned int priority ) {
      std::cout << "INFO [ObjectFactoryImpl<T>::_registerKit] register kit for pattern " <<  pattern << " with priority " << priority << "." << std::endl;
      m_kitList.insert(std::make_pair(priority, std::make_pair(std::make_shared<PixA::Regex_t>(pattern),std::shared_ptr<T>(a_kit))));
    }

  public:

    static IObjectFactory<T> *instance() {
      if (!s_instance ) {
	s_instance = new ObjectFactoryImpl<T>;
      }
      return s_instance;
    }

    static void registerKit( const std::string &pattern, T *a_kit, unsigned int priority = IObjectFactory<T>::kNormalPriority) {
      ObjectFactoryImpl<T>::instance()->_registerKit(pattern, a_kit, priority);
    }

    static const T * const kit(const std::string &name) {
      return instance()->_kit(name);
    }

    static void kitList(const std::string &name, std::vector< std::shared_ptr<T> > &list) {
      instance()->_kitList(name,list);
    }

  private:
     static IObjectFactory<T> *s_instance;

     typedef std::pair<std::shared_ptr<PixA::Regex_t> ,  std::shared_ptr<T>  > KitDef_t;
     typedef std::multimap<unsigned int, KitDef_t > KitList_t;

     KitList_t m_kitList;

  };

  template <class T>
  IObjectFactory<T> *ObjectFactoryImpl<T>::s_instance = NULL;

  template <class T>
  IObjectFactory<T> * IObjectFactory<T>::instance() 
  {
    if (!s_instance ) {
      s_instance = new ObjectFactoryImpl<T>;
    }
    return s_instance;
  }

}
#endif
