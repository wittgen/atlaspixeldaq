#ifndef _PixCon_HistogramView_H_
#define _PixCon_HistogramView_H_

#include <ui_HistogramView.h>
#include <ConnItem.h>
#include <CalibrationDataTypes.h>
#include <VisualiserList.h>
#include <DataContainer/HistoInfo_t.h>

#include <string>
#include <memory>

#include <Highlighter.h>
#include <QColor>
#include <QCloseEvent>

namespace PixCon {

  class HistogramCache;
  class VisualiserList;
  class CalibrationDataManager;
  class ISelectionFactory;

  class HistogramView : public QMainWindow, public Ui_HistogramView
  {
    Q_OBJECT

    enum EConfigVisualiser { kTDACMapVisualiser,kFDACMapVisualiser,kNConfigVisualiser};

  public:
    HistogramView(EMeasurementType measurement_type,
		  SerialNumber_t serial_number,
		  const std::shared_ptr<CalibrationDataManager> &calibration_data,
		  const std::shared_ptr<HistogramCache> &histogram_cache,
		  const std::shared_ptr<VisualiserList> &visualiser_list,
		  ISelectionFactory *selection_factory,
		  EConnItemType conn_type,
		  const std::string &connectivity_name,
		  const std::string &histogram_name,
		  QWidget* parent = 0, 
		  Qt::WindowFlags flags = Qt::Window);

    ~HistogramView();

    //void changeHistogram(int a_histogram_id);
   // void changeVisualiser(int visualiser_option);
    //    void changeHistogram(const std::string &histogram_name);
  public slots:
    // void changeVisualiser(const std::string &histogram_name);
    void changeHistogramIndex0(int new_histo_array_index)
    { changeHistogramArrayIndex(0,new_histo_array_index); }

    void changeHistogramIndex1(int new_histo_array_index)
    { changeHistogramArrayIndex(1,new_histo_array_index); }

    void changeHistogramIndex2(int new_histo_array_index)
    { changeHistogramArrayIndex(2,new_histo_array_index); }

    void changeHistogramIndex3(int new_histo_array_index)
    { changeHistogramArrayIndex(3,new_histo_array_index); }

    void changeVisualiser(int visualiser_option);
    void changeHistogram(int a_histogram_id);
    //  void changeLoopIndex(int index, int value);
    void toggleSingleFE(bool enabled);
    void changeFE(int front_end);
    void nextConnObj();
    void prevConnObj();
    void toggleConnectionToMainPanel();
    void printToFile();
    void changeConnObj();

  public:
    void changeHistogramArrayIndex(int loop_i, int new_histo_array_index);
    void setConnectivityName(EConnItemType type, const std::string &connectivity_name);

    void showHistogram(EMeasurementType measurement_type,
		       SerialNumber_t serial_number,
		       EConnItemType conn_item_type,
		       const std::string &connectivity_name,
		       const std::string &histogram_name,
		       unsigned int visualiser_option);// {
     // if (serial_number != m_serialNumber || measurement_type != m_measurementType || conn_item_type != m_connType) {
	//changeDataSelection(measurement_type, serial_number, conn_item_type);
	//m_connectivityName.clear();
      //}
      //showHistogram(conn_item_type,connectivity_name, histogram_name, visualiser_option);
  //  }

    void showHistogram(EConnItemType conn_item_type,
		       const std::string &connectivity_name,
		       const std::string &histogram_name,
		       unsigned int visualiser_option);

    void showTDACMap(EConnItemType conn_item_type,
		     const std::string &connectivity_name,
		     EMeasurementType measurement_type,
		     SerialNumber_t serial_number,
		     unsigned int visualiser_option);
  /*  {
      if (m_histogramChoiceList.isValid(m_tdacHistogramId)  ) {
	changeDataSelection(measurement_type, serial_number,conn_item_type);
	showHistogram(conn_item_type,connectivity_name, m_tdacHistogramId, visualiser_option);
      }
    }*/

    void showFDACMap(EConnItemType conn_item_type,
		     const std::string &connectivity_name,
		     EMeasurementType type,
		     SerialNumber_t serial_number,
		     unsigned int visualiser_option)
    {
      if ( m_histogramChoiceList.isValid(m_fdacHistogramId) ) {
	changeDataSelection(type, serial_number,conn_item_type);
	showHistogram(conn_item_type,connectivity_name, m_fdacHistogramId, visualiser_option);
      }
    }


    void visualise(EConnItemType type,
		   const std::string &connectivity_name,
		   unsigned int histo_id,
		   unsigned int visualiser_option);

    void changeDataSelection(EMeasurementType measurement_type, SerialNumber_t serial_number) {
      changeDataSelection(measurement_type, serial_number, m_connType);
    }

    void changeDataSelection(EMeasurementType measurement_type, SerialNumber_t serial_number, EConnItemType conn_item_type);

   // void toggleSingleFE(bool enabled);
 //   void changeFE(int front_end);
   // void changeConnObj();
   // void nextConnObj();
  //  void prevConnObj();

 //   void toggleConnectionToMainPanel();

    void disconnectFromMainPanel()
    { if (isConnectedToMainPanel()) toggleConnectionToMainPanel(); }

    void connectToMainPanel()
    { if (!isConnectedToMainPanel()) toggleConnectionToMainPanel(); }

    bool isConnectedToMainPanel() const { return m_connectedToMainPanel; }

    static void setOneFrontEndIsDefault() { s_oneFrontEndIsDefault=true; }

  protected:
    unsigned int getHistogramId(const std::string &histogram_name);

    void updateVisualiserList() ;
    void closeEvent(QCloseEvent *);
  private:

    void showHistogram(EConnItemType conn_item_type,
		       const std::string &connectivity_name,
		       unsigned int histogram_id,
		       unsigned int visualiser_option);

    void highlightConnName();
    void nextConnObj(bool backward);

    EMeasurementType                          m_measurementType;
    SerialNumber_t                            m_serialNumber;
    SerialNumber_t                            m_scanSerialNumber;
    std::shared_ptr<CalibrationDataManager> m_calibrationData;
    std::shared_ptr<HistogramCache>         m_histogramCache;

    EConnItemType                             m_connType;
    std::string                               m_connectivityName;

    std::shared_ptr<VisualiserList>         m_visualiserListManager;

    class VisualiserCache_t {
    public:
      VisualiserCache_t() {
	m_visualiserList.resize(kNMeasurements-1);
	for(std::vector< std::vector< VisualiserListRef_t> >::iterator measurement_iter = m_visualiserList.begin();
	    measurement_iter != m_visualiserList.end();
	    measurement_iter++) {
	  measurement_iter->resize( kNHistogramConnItemTypes);
	}
      }

      void setVisualiserList(EMeasurementType measurement_type,  EConnItemType conn_item_type, const VisualiserListRef_t &list) {
	assert(measurement_type < kNMeasurements && measurement_type>0);
	assert(conn_item_type < kNHistogramConnItemTypes);
	m_visualiserList[measurement_type-1][conn_item_type] = list;
      }

      VisualiserListRef_t &visualiserList(EMeasurementType measurement_type,  EConnItemType conn_item_type) {
	assert(measurement_type < kNMeasurements && measurement_type>0);
	assert(conn_item_type < kNHistogramConnItemTypes);
	return m_visualiserList[measurement_type-1][conn_item_type];
      }

      const VisualiserListRef_t &visualiserList(EMeasurementType measurement_type,  EConnItemType conn_item_type) const {
	assert(measurement_type < kNMeasurements && measurement_type>0);
	assert(conn_item_type < kNHistogramConnItemTypes);
	return m_visualiserList[measurement_type-1][conn_item_type];
      }

    private:
      std::vector< std::vector< VisualiserListRef_t> >  m_visualiserList;
    };
      
    VisualiserCache_t m_visualiserList;

    unsigned int m_canvasId;

    unsigned int m_histogramId;
    class HistogramChoice_t
    {
    protected:
      HistogramChoice_t()
	: m_histogramId(static_cast<unsigned int>(-1))
      {}

    public:
      HistogramChoice_t(EMeasurementType measurement_type, EConnItemType conn_type, unsigned int histogram_id, bool is_config_visualiser=false)
	: m_measurementType(measurement_type),
	  m_connItemType(conn_type),
	  m_histogramId(histogram_id),
	  m_configVisualiser(is_config_visualiser)
      {}

      bool             isValid() const            { return m_histogramId != static_cast<unsigned int>(-1); }
      bool             isConfigVisualiser() const { return m_configVisualiser; }
      EMeasurementType measurementType() const    { return m_measurementType; }
      EConnItemType    connItemType() const       { return m_connItemType; }
      unsigned int     histogramId() const        { return m_histogramId; }

      static HistogramChoice_t invalidChoice() { return HistogramChoice_t(); }

    private:
      EMeasurementType m_measurementType;
      EConnItemType    m_connItemType;
      unsigned int     m_histogramId;
      bool             m_configVisualiser;
    };

    class HistogramChoiceList_t {
    public: 
      void addChoice(const HistogramChoice_t &a_choice) { m_histogramChoice.push_back(a_choice); }
      void clear() { m_histogramChoice.clear(); }

      const HistogramChoice_t & operator[](unsigned int index) const {
	assert(index < m_histogramChoice.size() );
	return m_histogramChoice[index];
      }

      unsigned int findConfigVisualiser(unsigned int visualiser_id) const {
	unsigned int counter_i=m_histogramChoice.size();
	for(std::vector<HistogramChoice_t>::const_reverse_iterator choice_iter = m_histogramChoice.rbegin();
	    choice_iter != m_histogramChoice.rend();
	    choice_iter++,counter_i--) {
	  if (choice_iter->isConfigVisualiser() && choice_iter->histogramId()==visualiser_id) {
	    assert(counter_i>0);
	    return counter_i-1;
	  }
	}
	return invalid();
      }

      unsigned int findHistogram(EMeasurementType measurement_type, EConnItemType conn_item_type, unsigned int histogram_id) const {
	unsigned int counter_i=0;
	for(std::vector<HistogramChoice_t>::const_iterator choice_iter = m_histogramChoice.begin();
	    choice_iter != m_histogramChoice.end();
	    choice_iter++,counter_i++) {
	  if (   !choice_iter->isConfigVisualiser() 
	      && choice_iter->measurementType()==measurement_type
	      && choice_iter->connItemType()==conn_item_type
	      && choice_iter->histogramId()==histogram_id) {
	    return counter_i;
	  }
	}
	return invalid();
      }

      bool isValid(unsigned int histogram_id) const  {
	return histogram_id < m_histogramChoice.size();
      }

    protected:
      static unsigned int invalid() { return static_cast<unsigned int>(-1); }
    private:
      std::vector<HistogramChoice_t> m_histogramChoice;
    };

    HistogramChoiceList_t m_histogramChoiceList;
    unsigned int          m_tdacHistogramId;
    unsigned int          m_fdacHistogramId;

    std::unique_ptr<ISelectionFactory>   m_selectionFactory;
    QColor                     m_yellow;
    std::unique_ptr<Highlighter> m_highlighter;


    bool              m_rebuildInfo;
    HistoInfo_t       m_currentInfo;
    PixA::Index_t     m_histoIndex;

    unsigned int m_visualiserOption;

    bool              m_isExecuting;
    bool              m_connectedToMainPanel;

    EConfigVisualiser findConfigVisualiser(const std::string &name) const {
      for (unsigned int i=0; i<m_configVisualiserName.size(); i++) {
	if (m_configVisualiserName[i]==name) return static_cast<EConfigVisualiser>(i);
      }
      return kNConfigVisualiser;
    }

    std::vector< std::string >                           m_configVisualiserName;
    std::vector<std::shared_ptr< PixA::IVisualiser > > m_configVisualiser;

    std::string m_lastPrintPath;

    static std::string makeHistogramViewName();
    static unsigned int s_counter;
    static bool         s_init;
    static bool         s_oneFrontEndIsDefault;


  signals:
    void close(const QString &name);
    void connectionToMainPanel(const QString &name);
  };
}
#endif
