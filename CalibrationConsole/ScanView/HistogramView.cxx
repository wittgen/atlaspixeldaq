#include "HistogramView.h"
#include "TCanvas.h"
#include "MyQRootCanvas.h"


#include <QListWidget>
#include <QSignalMapper>
#include <QMessageBox>
#include <QPainter>
#include <QFileDialog>

#include <CalibrationDataManager.h>
#include <MetaDataUtil.h>

#include <Visualiser/IVisualiser.h>
#include <Visualiser/VisualisationUtil.h>
#include <Visualiser/VisualisationKitSubSet_t.h>
#include <Visualiser/VisualisationFactory.h>

#include <ConfigWrapper/ConnectivityUtil.h>

//#include "HistogramCache.h"
#include "HistogramCacheDataProvider.h"

#include "TemporaryCursorChange.h"

#include <guessConnName.h>
#include <IConnItemSelection.h>
#include <ISelectionFactory.h>
#include <CalibrationDataFunctorBase.h>

namespace PixCon {

  bool HistogramView::s_oneFrontEndIsDefault = false;

  class GreyedListBoxText : public /*Q3ListBoxText*/ QListWidgetItem
  {
  public:
    GreyedListBoxText(const QString & text) : /*Q3ListBoxText*/ QListWidgetItem (text) {this->setBackground(QColor(200,200,200));}
  protected:
   // QListWidgetItem* pItem =new QListWidgetItem(text);


    /*int width(const Q3ListBox *lb) {
      if (lb && lb->maximumWidth()>0) {
    return lb->maximumWidth();
      }
      else {
        return Q3ListBoxText::width(lb);
      }
    }*/

    /*void paint( QPainter *painter )
    {
      // evil trick: find out whether we are painted onto our listbox
      // (from qt doc 3.3)
      //bool in_list_box = listBox() && listBox()->viewport() == painter->device();
      if (painter->viewport() == painter->device()){
        //bool in_list_box = TRUE;
      }
      //QRect r ( 0, 0, width( listBox() ), height( listBox() ) );
      if (in_list_box && !isSelected()) {
         painter->fillRect( 0, 0, painter->viewport->width() , painter->viewport->height() , QColor(200,200,200) );
      }
      //Q3ListBoxText::paint(painter);
      QListWidget::paint(painter);
    }*/

  };


  QSignalMapper *bind_arg(QWidget *a, const char * member, int value) {
    if (!a) return NULL;
    QSignalMapper *sigmap = new QSignalMapper(a);
    sigmap->setMapping(a,value);
    sigmap->connect(sigmap,SIGNAL(mapped(int)),a,member);
    return sigmap;
  }
  unsigned int HistogramView::s_counter = 0;

  std::string HistogramView::makeHistogramViewName() {
    std::stringstream name;
    name << "PixCon - HistogramView";
    if (s_counter>0) {
      name << "[" << s_counter << "]";
    }
    s_counter++;
    return name.str();
  }

  bool HistogramView::s_init=false;

  HistogramView::HistogramView(EMeasurementType measurement_type,
			       SerialNumber_t serial_number,
			       const std::shared_ptr<CalibrationDataManager> &calibration_data,
			       const std::shared_ptr<HistogramCache> &histogram_cache,
			       const std::shared_ptr<VisualiserList> &visualiser_list,
			       ISelectionFactory *selection_factory,
			       EConnItemType conn_type,
			       const std::string &connectivity_name,
			       const std::string &histogram_name,
			       QWidget* parent, 
			       Qt::WindowFlags flags)
    : QMainWindow(parent,flags),
      m_measurementType(kNMeasurements),  // set to invalid value, such that change data selection will notice that there was no data selected yet
      m_serialNumber(0),
      m_calibrationData(calibration_data),
      m_histogramCache(histogram_cache),
      m_connType(conn_type),
      m_visualiserListManager(visualiser_list),
      m_histogramId(0),
      m_selectionFactory(selection_factory),
      m_yellow("yellow"),
      m_rebuildInfo(true),
      m_isExecuting(false),
      m_connectedToMainPanel(false)
  {
    setupUi(this);

    // actually the histogram view should not be created for a connectiivty item without histogram item types.
    assert( m_connType < kNHistogramConnItemTypes );

    m_showOneFrontEnd->setChecked(s_oneFrontEndIsDefault);
    delete m_canvas;
    m_canvas =  new MyQRootCanvas( frame, "m_canvas" );
    QSizePolicy spol((QSizePolicy::Policy)7, (QSizePolicy::Policy)7);
    spol.setHorizontalStretch(0);
    spol.setVerticalStretch(0);
    spol.setHeightForWidth(m_canvas->sizePolicy().hasHeightForWidth());
    m_canvas->setSizePolicy( spol );
    //m_canvas->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, m_canvas->sizePolicy().hasHeightForWidth() ) );
    frameLayout->addWidget( m_canvas );

    TCanvas *the_canvas = m_canvas->getCanvas();
    if (the_canvas) {
      PixA::PlotCanvas_t::incCanvas();
      m_canvasId=PixA::PlotCanvas_t::count();
      std::stringstream name;
      name << "pixcan_" << m_canvasId;
      the_canvas->SetName(name.str().c_str());
    }

    {
      std::vector<std::string> empty_folder_list;
      PixA::VisualisationKitSubSet_t dac_map_kits;
      PixA::VisualisationFactory::getKits("DAC_map", empty_folder_list, dac_map_kits);

      m_configVisualiserName.resize(kNConfigVisualiser);
      m_configVisualiser.reserve(kNConfigVisualiser);
      //     m_configVsualiser[kTDACMapVisualiser] = PixA::VisualisationFactory::visualiser(dac_map_kits,
      // 									      "TDAC",
      // 										   empty_folder_list);
      //     m_configVisualiser[kFDACMapVisualiser] = PixA::VisualisationFactory::visualiser(dac_map_kits,
      // 									      "FDAC",
      // 									      empty_folder_list);

      m_configVisualiserName[kTDACMapVisualiser]="TDAC";
      m_configVisualiserName[kFDACMapVisualiser]="FDAC";
      for (std::vector<std::string>::const_iterator config_visu_name_iter = m_configVisualiserName.begin();
	   config_visu_name_iter != m_configVisualiserName.end();
	   config_visu_name_iter++) {
	m_configVisualiser.push_back(std::shared_ptr<PixA::IVisualiser>(PixA::VisualisationFactory::visualiser(dac_map_kits,
														 *config_visu_name_iter,
														 empty_folder_list)));
      }
    }
    m_connObjName->setReadOnly( false );

    QObject::connect(m_selectorLoop0,     SIGNAL(valueChanged(int)),this,SLOT(changeHistogramIndex0(int)));
    QObject::connect(m_selectorLoop1,     SIGNAL(valueChanged(int)),this,SLOT(changeHistogramIndex1(int)));
    QObject::connect(m_selectorLoop2,     SIGNAL(valueChanged(int)),this,SLOT(changeHistogramIndex2(int)));
    QObject::connect(m_selectorHistoIndex,SIGNAL(valueChanged(int)),this,SLOT(changeHistogramIndex3(int)));
    QObject::connect(m_canvas,            SIGNAL(statusMessage(const QString &)),statusBar(),SLOT(showMessage(const QString &)));
    QObject::connect(m_histogramNameList, SIGNAL(activated(int)),   this, SLOT(changeHistogram(int)));
    QObject::connect(m_visualiserNameList,SIGNAL(activated(int)),   this, SLOT(changeVisualiser(int)));
    QObject::connect(m_prevConnObj,       SIGNAL(clicked()),        this, SLOT(prevConnObj()));
    QObject::connect(m_nextConnObj,       SIGNAL(clicked()),        this, SLOT(nextConnObj()));
    QObject::connect(m_showOneFrontEnd,   SIGNAL(toggled(bool)),    this, SLOT(toggleSingleFE(bool)));
    QObject::connect(m_frontEnd,          SIGNAL(valueChanged(int)),this, SLOT(changeFE(int)));
    QObject::connect(m_newWindowButton,   SIGNAL(clicked()),        this, SLOT(toggleConnectionToMainPanel()));
    QObject::connect(m_printButton,       SIGNAL(clicked()),        this, SLOT(printToFile()));
    QObject::connect(m_connObjName,       SIGNAL(returnPressed()),  this, SLOT(changeConnObj()));

    changeDataSelection(measurement_type, serial_number);

    if (!histogram_name.empty()) {
      unsigned int histogram_id = getHistogramId(histogram_name);

      if (m_histogramChoiceList.isValid(histogram_id) ) {
	m_histogramId = histogram_id;
      }
    }
    else {
      m_histogramId = 0;
      // guess a good initial histogram?
    }
    m_newWindowButton->setIcon(QIcon(QPixmap(":/icons/images/stock_connect.png")));
    m_prevConnObj->setIcon(QIcon(QPixmap(":/icons/images/gtk-go-back.png")));
    m_nextConnObj->setIcon(QIcon(QPixmap(":/icons/images/gtk-go-forward.png")));
    m_printButton->setIcon(QIcon(QPixmap(":/icons/images/fileprint.png")));

    setConnectivityName(conn_type,connectivity_name);
    if (!s_init) {
      PixA::PlotCanvas_t::setPalette(PixA::PlotCanvas_t::kMABlackRedYellow);
    }

  }

  HistogramView::~HistogramView() {
    std::cout << "DEBUG [HistogramView::~HistogramView] called dtor for " << this << "\n";
  }

  void HistogramView::changeConnObj() {

    try {
      PixA::ConnectivityRef conn(m_calibrationData->connectivity(m_measurementType, m_serialNumber));
      if (conn) {
	std::string conn_name = m_connObjName->text().toLatin1().data();

	std::pair<std::string, std::string::size_type> ret = guessConnName(conn, conn_name, 0);
	if (!ret.first.empty()) {
	  try {
	    EConnItemType type = guessConnItemType(ret.first);

	    if (type == kPp0Item) {
	      if (!conn.findPp0(ret.first)) {
		throw CalibrationData::MissingConnObject("");
	      }
	    }
	    else {
	      Lock lock(m_calibrationData->calibrationDataMutex());
	      m_calibrationData->calibrationData().getConnObjectData(conn_name);
	    }
	    if (type <= kNConnItemTypes) {
	      setConnectivityName(type, ret.first);
	      if (conn_name != ret.first) {
		m_connObjName->setText(ret.first.c_str());
	      }

	      return;
	    }
	  }
	  catch (CalibrationData::MissingConnObject &) {
	  }
	}
	std::cerr << "ERROR [HistogramView::changeConnObj] Invalid connectivity object name \"" << conn_name << "\" "
		  << "."  << std::endl;

      }
      else {
	std::cerr << "ERROR [HistogramView::changeConnObj] No connectivity for the given measurement "
		  << makeSerialNumberString(m_measurementType, m_serialNumber)
		  << "."  << std::endl;
      }
    }
    catch(...) {
    }

    highlightConnName(); 
  }

  void HistogramView::highlightConnName() 
  {
    if (!m_highlighter.get()) {
      m_highlighter = std::make_unique<Highlighter>(*m_connObjName,
								   Qt::yellow,
								   Qt::white, 
								   2000);
    }
    m_highlighter->restart(m_yellow);

  }

  class NextConnObjFunctor : public CalibrationDataFunctorBase
  {
  public:
    NextConnObjFunctor(EConnItemType type,
		       const std::string &current_conn_name,
		       IConnItemSelection &selection,
		       bool backward) 
      : CalibrationDataFunctorBase(connItemMask(type)),
	m_itemSelection(&selection),
	m_currentConnName(current_conn_name),
	m_backward(backward),
	m_next(false),
	m_locked(false)
    {}

    void operator()( EConnItemType /*item_type*/,
		     const std::string &conn_name,
		     const std::pair<EMeasurementType,SerialNumber_t> &/*measurement*/,
		     PixCon::CalibrationData::ConnObjDataList &/*data_list*/,
		     CalibrationData::EnableState_t is_enable_state,
		     CalibrationData::EnableState_t /*new_enable_state*/) 
    {
      selectNext(conn_name, is_enable_state);
    }


    // do nothing intentionally
    void operator()( EConnItemType /*item_type*/,
		     const std::string &conn_name,
		     const std::pair<EMeasurementType,SerialNumber_t> &/*measurement*/,
		     CalibrationData::EnableState_t new_enable_state)
    {
      selectNext(conn_name, new_enable_state);
    }

    const std::string &selected() { return m_selectedConnName; }
  private:

    void selectNext(const std::string &conn_name, CalibrationData::EnableState_t is_enable_state) {
      if (m_locked) return;

      if (conn_name==m_currentConnName) {
	if (m_backward) {
	  m_selectedConnName = m_lastConnName;
	  m_locked=true;
	}
	else {
	  m_next=true;
	}
      }
      else if (m_itemSelection->isSelected(conn_name) && is_enable_state.enabled()) {
	if (m_next) {
	  m_locked=true;
	  m_selectedConnName = conn_name;
	}
	else {
	  m_lastConnName = conn_name;
	}
      }
    }

    IConnItemSelection *m_itemSelection;
    std::string m_currentConnName;

    std::string m_lastConnName;
    std::string m_selectedConnName;

    bool m_backward;
    bool m_next;
    bool m_locked;
  };


  void HistogramView::nextConnObj() {
    nextConnObj(false);
  }

  void HistogramView::prevConnObj() {
    nextConnObj(true);
  }

  void HistogramView::nextConnObj(bool backward) {
    std::string conn_name = m_connObjName->text().toLatin1().data();
    EConnItemType type=guessConnItemType(conn_name);
    std::string next_conn_name;

    switch (type) {
    case kModuleItem:
    case kPp0Item:
    case kRodItem: {
      std::unique_ptr<IConnItemSelection> selection( m_selectionFactory->selection(true,false));
      if (selection.get()) {
	NextConnObjFunctor next_conn_obj(type, conn_name, *selection,backward);
	{
	  Lock lock(m_calibrationData->calibrationDataMutex());
	  m_calibrationData->forEach(m_measurementType, m_serialNumber, next_conn_obj, false);
	}
	next_conn_name = next_conn_obj.selected();
      }
      else {
	std::cerr << "ERROR [HistogramView::changeConnObj] Internal error. Failed to create connectivity object selection helper." << std::endl;
      }
      break;
    }
    default:
      ;
    }

    if (!next_conn_name.empty()) {
      m_connObjName->setText(next_conn_name.c_str());
      changeConnObj();
    }
    else {
      std::cerr << "ERROR [HistogramView::changeConnObj] No object " << (backward ? " before " : " after ") << conn_name << std::endl;
      highlightConnName();
    }
  }
  
  void HistogramView::changeHistogram(int a_histogram_id)
  {
    if (m_histogramChoiceList.isValid(static_cast<unsigned int>(a_histogram_id) )) {
      if (static_cast<unsigned int>(a_histogram_id) != m_histogramId) {
	m_rebuildInfo = true;
      }
      visualise(m_connType, m_connectivityName, static_cast<unsigned int>(a_histogram_id), m_visualiserOption);
    }
  }

  void HistogramView::changeVisualiser(int an_option) 
  {
    if (an_option < m_visualiserNameList->count()) {
      m_visualiserOption = static_cast<unsigned int>(an_option);
      visualise(m_connType, m_connectivityName, m_histogramId, m_visualiserOption);
    }
  }

  void HistogramView::toggleSingleFE(bool enabled) {
    m_frontEnd->setEnabled(enabled);
    visualise(m_connType, m_connectivityName, m_histogramId, m_visualiserOption);
  }

  void HistogramView::changeFE(int /*front_end*/) {
    if (m_showOneFrontEnd->isEnabled()) {
      visualise(m_connType, m_connectivityName, m_histogramId, m_visualiserOption);
    }
  }

  void HistogramView::setConnectivityName(EConnItemType conn_item_type, const std::string &connectivity_name)
  {
    assert( conn_item_type < kNConnItemTypes );
    if (conn_item_type<kNHistogramConnItemTypes) {
      if (conn_item_type != m_connType || connectivity_name != m_connectivityName ) {
	m_connObjName->setText(connectivity_name.c_str() );
	m_connType= conn_item_type;
	m_connectivityName = connectivity_name;
	m_rebuildInfo =true;
	visualise(m_connType, m_connectivityName, m_histogramId, m_visualiserOption);
      }
    }
  }

  void HistogramView::updateVisualiserList() {
    if ( !m_histogramChoiceList.isValid(m_histogramId) ) return;

    std::shared_ptr<PixA::IVisualiser> a_visualiser;

    const HistogramChoice_t &the_choice = m_histogramChoiceList[m_histogramId];
    if (the_choice.isConfigVisualiser()) {
      assert( the_choice.histogramId() < m_configVisualiser.size());
      a_visualiser =  m_configVisualiser[ the_choice.histogramId() ];
    }
    else if (the_choice.measurementType()==kScan || the_choice.measurementType()==kAnalysis) {
      a_visualiser =  m_visualiserList.visualiserList(the_choice.measurementType(),the_choice.connItemType()).visualiser( the_choice.histogramId() );
    }
    else {
      // should never occure.
      std::cout << "ERROR [HistogramView::updateVisualiserList] invalid histogram choice." << std::endl;
    }

    QString current_option = m_visualiserNameList->currentText();
    m_visualiserNameList->clear();
    m_visualiserOption = 0;
    if (a_visualiser) {
      for(unsigned int option_i=0; option_i< a_visualiser->nOptions(); option_i++) {
	m_visualiserNameList->addItem(a_visualiser->optionName(option_i).c_str(),option_i);
	if (current_option == QString(a_visualiser->optionName(option_i).c_str())) {
	  m_visualiserNameList->setCurrentIndex(option_i);
	  m_visualiserOption = option_i;
	}
      }
    }
    if (m_visualiserNameList->count()==0) {
      m_visualiserNameList->addItem("(none)",-1);
    }
  }


  class SingleExecutionProtect
  {
  public:
    SingleExecutionProtect(bool &is_executing) : m_isExecuting(&is_executing) { *m_isExecuting = true; }
    ~SingleExecutionProtect()                                                 { *m_isExecuting = false; }
  private:
    bool *m_isExecuting;
  };

  void HistogramView::visualise(EConnItemType conn_item_type,
				const std::string &connectivity_name,
				unsigned int histogram_id,
				unsigned int visualiser_option)
  {
    if (m_isExecuting || conn_item_type>=kNHistogramConnItemTypes) return;

    SingleExecutionProtect protect(m_isExecuting);
    TemporaryCursorChange cursor_change;

    if (!m_histogramChoiceList.isValid(histogram_id))  return;

    if (histogram_id !=m_histogramId) {
      m_rebuildInfo=true;
      m_histogramId = histogram_id;
      updateVisualiserList();
    }
    std::shared_ptr<PixA::IVisualiser> a_visualiser;
    const HistogramChoice_t &the_choice = m_histogramChoiceList[m_histogramId];
    if (the_choice.isConfigVisualiser()) {
      assert( the_choice.histogramId() < m_configVisualiser.size());
      a_visualiser =  m_configVisualiser[ the_choice.histogramId() ];
    }
    else if (the_choice.measurementType()==kScan || the_choice.measurementType()==kAnalysis) {
      a_visualiser =  m_visualiserList.visualiserList(the_choice.measurementType(),the_choice.connItemType()).visualiser(the_choice.histogramId() );
    }
    else {
      // should never occure.
      std::cout << "ERROR [HistogramView::visualise] invalid histogram choice." << std::endl;
      return;
    }
//     std::shared_ptr<PixA::IVisualiser> a_visualiser( ( (histogram_id < m_nScanHistograms) ?
// 							   m_scanVisualiserList.visualiser(histogram_id)
// 							 : (histogram_id < m_nAnalysisHistograms ? 
// 							    m_analysisVisualiserList.visualiser(histogram_id - m_nScanHistograms) 
// 							    : m_configVisualiser[histogram_id - m_nAnalysisHistograms] )) );

    if (a_visualiser.get()) {
      if (a_visualiser->canVisualiseOneFrontEnd()) {
	if (!m_showOneFrontEnd->isEnabled()) {
	  m_showOneFrontEnd->setEnabled(true);
	}
      }
      else {
	if (m_showOneFrontEnd->isEnabled()) {
	  m_showOneFrontEnd->setEnabled(false);
	}
	if (m_frontEnd->isEnabled()) {
	  m_frontEnd->setEnabled(false);
	}
      }

      if (visualiser_option >= a_visualiser->nOptions()) visualiser_option=0;
      
      std::vector <PixA::ConfigHandle> vscan_config_handle;
      try {
	std::string folder_name = m_histogramNameList->itemText(m_histogramId).toLatin1().data();
	std::string::size_type pos = folder_name.rfind("/");
	if (pos != std::string::npos) {
	  folder_name.erase(pos, folder_name.size());
	}
	if (folder_name.compare(0,2,"A:")==0) {
	  folder_name.erase(0,2);
	}	
      }
      catch (std::runtime_error &err) {
	//@todo the MetaDataCatalogue should throw a dedicated exception for missing meta data.
      }

      vscan_config_handle = getScanConfigVector((the_choice.measurementType() == kScan) ? m_scanSerialNumber : m_serialNumber);

      try {
	HistogramCacheDataProvider data_provider(m_histogramCache,
						 the_choice.measurementType(),
						 (the_choice.measurementType() == kScan ? m_scanSerialNumber : m_serialNumber),
						 m_calibrationData->connectivity(m_measurementType, m_serialNumber),
						 vscan_config_handle );
	
	std::vector< std::string > module_names;
	if (conn_item_type == kPp0Item && the_choice.connItemType()==kModuleItem) {
	  const PixA::ConnectivityRef &conn = m_calibrationData->connectivity(m_measurementType,m_serialNumber);
	  PixA::ModuleList modules = conn.modules(connectivity_name);
	  PixA::ModuleList::const_iterator begin_module = modules.begin();
	  PixA::ModuleList::const_iterator end_module = modules.end();
	  for (PixA::ModuleList::const_iterator module_iter = begin_module;
	       module_iter != end_module;
	       ++module_iter) {
	    module_names.push_back(PixA::connectivityName( module_iter) );
	  }
	}
	else {
	  module_names.push_back(connectivity_name);
	}
	
	if (!module_names.empty()) {
	  if (m_rebuildInfo) {
	    m_currentInfo.reset();
	    a_visualiser->fillIndexArray( data_provider, module_names,  m_currentInfo);
	    
	    QSpinBox *spinbox_arr[4]={m_selectorLoop0, m_selectorLoop1, m_selectorLoop2, m_selectorHistoIndex};
	    unsigned int n_max_levels=0;
	    unsigned int current_histo_index=0;
	    if (!m_histoIndex.empty()) {
	      current_histo_index = m_histoIndex.back();
	    }
	    for (unsigned int level_i=0; level_i<4; level_i++) {
	      unsigned int new_min_value;
	      unsigned int new_max_value =  m_currentInfo.maxIndex(level_i) ;
	      if (level_i==3) {
		new_min_value = m_currentInfo.minHistoIndex();
	      }
	      else {
		new_min_value = 0;
	      }
/*QString tooltip_text ;  tooltip_text.setNum(spinbox_arr[level_i]->value()) ;   //   QString tooltip_text = QToolTip::textFor(spinbox_arr[level_i]);
	      int pos = tooltip_text.findRev(" : ");
	      if (pos>0 && static_cast<unsigned int>(pos) < tooltip_text.length()) {
		tooltip_text.remove(pos, tooltip_text.length()-pos);
	      }*/
	      
	      if (new_min_value < new_max_value && (level_i==n_max_levels || level_i==3)) {
		if(new_max_value!=0) new_max_value--;
		bool update_text=false;
		if (!spinbox_arr[level_i]->isEnabled()) {
		  spinbox_arr[level_i]->setEnabled(true);
		  update_text=true;
		}
		if ( new_min_value != static_cast<unsigned int>(spinbox_arr[level_i]->minimum()) ) {
		  spinbox_arr[level_i]->setMinimum( new_min_value );
		  update_text=true;
		}
		if ( new_max_value != static_cast<unsigned int>(spinbox_arr[level_i]->maximum()) ) {
		  spinbox_arr[level_i]->setMaximum( new_max_value );
		  update_text=true;
		}
		if (update_text) {
		  std::stringstream range;
		  range << " : " << new_min_value << " - " << new_max_value;
		//  tooltip_text += range.str().c_str();
		  //		QToolTip::remove(spinbox_arr[level_i]);
	//	  QToolTip::add(spinbox_arr[level_i],tooltip_text);
		}
		
		if (m_histoIndex.size()<n_max_levels+1) {
		  m_histoIndex.resize(n_max_levels+1,0);
		}
		if (m_histoIndex[n_max_levels]<new_min_value) {
		  m_histoIndex[n_max_levels]=new_min_value;
		}
		if (m_histoIndex[n_max_levels]>new_max_value) {
		  m_histoIndex[n_max_levels]=new_max_value;
		}
		
		if (level_i==3) {
		  if (current_histo_index >= new_min_value && (current_histo_index <= new_max_value) ) {
		    m_histoIndex[n_max_levels]=current_histo_index;
		  }
		}
		if (m_histoIndex[n_max_levels]!=static_cast<unsigned int>(spinbox_arr[level_i]->value())) {
		  spinbox_arr[level_i]->setValue(m_histoIndex[n_max_levels]);
		}
		n_max_levels++;
	      }
	      else {
		if (spinbox_arr[level_i]->isEnabled()) {
		  spinbox_arr[level_i]->setEnabled(false);
		  //		QToolTip::remove(spinbox_arr[level_i]);
		//  QToolTip::add(spinbox_arr[level_i],tooltip_text);
		}
	      }
	    }
	    
	    if (m_histoIndex.size()>n_max_levels) {
	      m_histoIndex.resize(n_max_levels);
	    }
	    m_rebuildInfo=false;
// 	  std::cout << "INFO [HistogramView::visualise] info : ";
// 	  for (unsigned int i=0; i<3; i++) {
// 	    std::cout << m_currentInfo.maxIndex(i) << " ";
// 	  }
// 	  std::cout << m_currentInfo.minHistoIndex() << " - " << m_currentInfo.maxHistoIndex() << std::endl;

	  }
	  int front_end=( m_showOneFrontEnd->isChecked() ? m_frontEnd->value() : -1 );

// 	std::cout << "INFO [HistogramView::visualise] visualise : ";
// 	for (unsigned int level_i=0; level_i<m_histoIndex.size(); level_i++) {
// 	  std::cout << m_histoIndex[level_i] << ", ";
// 	}
//	std::cout << std::endl;

	  PixA::PlotCanvas_t::setCounter(m_canvasId);
	  a_visualiser->visualise( data_provider, module_names,  visualiser_option, m_histoIndex, front_end);
	}
      }
      catch (SctPixelRod::BaseException &err) {
	std::cerr << "ERROR [HistogramView::visualise] Caught base exception : " << err.getDescriptor() << std::endl;
      }
      catch (std::exception &err) {
	std::cerr << "ERROR [HistogramView::visualise] Caught std exception : " << err.what() << std::endl;
      }
    }
  }

  unsigned int HistogramView::getHistogramId(const std::string &histogram_name)
  {
    if (!histogram_name.empty()) {
      unsigned int histogram_id = UINT_MAX;
      EConfigVisualiser config_visualiser_i = findConfigVisualiser(histogram_name);
      if (config_visualiser_i < kNConfigVisualiser) {
	histogram_id = m_histogramChoiceList.findConfigVisualiser( config_visualiser_i );
	//	assert( histogram_id != m_histogramChoiceList::invalid() );
	if (!m_histogramChoiceList.isValid(histogram_id) ) {
	  std::cout << "ERROR [HistogramView::ctor] Choice " << histogram_name 
		    << " seems to be a configuration, but there is no visualiser for this configuration (program logic error)."
		    << std::endl;
	}
      }
      else {
	EConnItemType conn_type[2]={m_connType, kModuleItem};
	unsigned int n_conn_types;
	if (m_connType==kPp0Item) {
	  n_conn_types = 2;
	}
	else {
	  n_conn_types = 1;
	}

	for (unsigned int conn_type_i=0; conn_type_i<n_conn_types; conn_type_i++) {

	  unsigned int a_histogram_id = m_histogramCache->histogramIndex(kScan, m_scanSerialNumber, conn_type[conn_type_i], histogram_name);
	  if (a_histogram_id != HistogramCache::invalidIndex()) {

	    // @todo if the histogram is in the cache it should also be in the histogram list
	    // or could it be that the histogram list in the cache changes ?
	    histogram_id = m_histogramChoiceList.findHistogram( kScan, conn_type[conn_type_i], a_histogram_id );
	    //	    assert (histogram_id != HistogramChoiceList_t::invalid());
	    break;
	  }

	  a_histogram_id = m_histogramCache->histogramIndex(m_measurementType, m_serialNumber, conn_type[conn_type_i], histogram_name);
	  if (a_histogram_id != HistogramCache::invalidIndex()) {

	    // @todo if the histogram is in the cache it should also be in the histogram list
	    // or could it be that the histogram list in the cache changes ?
	    histogram_id = m_histogramChoiceList.findHistogram( m_measurementType, conn_type[conn_type_i], a_histogram_id );
	    //	    assert (histogram_id != HistogramChoiceList_t::invalid());
	    break;
	  }

	}
	if (!m_histogramChoiceList.isValid(histogram_id)) {
	  std::cout << "ERROR [HistogramView::ctor] Histogram " << histogram_name 
		    << " is in the cache but not in the histogram list of this histogram view (program logic error)."
		    << std::endl;
	}

      }

      return histogram_id;
    }
    else {
      return 0;
    }
  }
void HistogramView::showHistogram(EMeasurementType measurement_type,
		       SerialNumber_t serial_number,
		       EConnItemType conn_item_type,
		       const std::string &connectivity_name,
		       const std::string &histogram_name,
		       unsigned int visualiser_option) {
    if (serial_number != m_serialNumber || measurement_type != m_measurementType || conn_item_type != m_connType) {
	changeDataSelection(measurement_type, serial_number, conn_item_type);
	m_connectivityName.clear();
      }
      showHistogram(conn_item_type,connectivity_name, histogram_name, visualiser_option);
    }

void HistogramView::showTDACMap(EConnItemType conn_item_type,
		     const std::string &connectivity_name,
		     EMeasurementType measurement_type,
		     SerialNumber_t serial_number,
		     unsigned int visualiser_option)
   {
      if (m_histogramChoiceList.isValid(m_tdacHistogramId)  ) {
	changeDataSelection(measurement_type, serial_number,conn_item_type);
	showHistogram(conn_item_type,connectivity_name, m_tdacHistogramId, visualiser_option);
      }
    }

  void HistogramView::showHistogram(EConnItemType conn_item_type,
				    const std::string &connectivity_name,
				    const std::string &histogram_name,
				    unsigned int visualiser_option)
  {
    assert(conn_item_type < kNConnItemTypes);
    if (conn_item_type >= kNHistogramConnItemTypes) return;

    unsigned int histogram_id=m_histogramId;

    if (!histogram_name.empty()) {
      unsigned int new_histogram_id = getHistogramId(histogram_name);

      if (m_histogramChoiceList.isValid(new_histogram_id) ) {
	histogram_id=new_histogram_id;
      }
    }
    if (!m_histogramChoiceList.isValid(histogram_id)) {
      histogram_id=0;
    }

    showHistogram(conn_item_type, connectivity_name, histogram_id, visualiser_option);
  }

  void HistogramView::showHistogram(EConnItemType conn_item_type,
				    const std::string &connectivity_name,
				    unsigned int histogram_id,
				    unsigned int visualiser_option)
  {
    assert( conn_item_type < kNConnItemTypes );
    if (conn_item_type>=kNHistogramConnItemTypes) return;
    if (!m_histogramChoiceList.isValid(histogram_id) ) return;

    if ( histogram_id >= static_cast<unsigned int>(m_histogramNameList->count())) {
      std::cerr << "ERROR [HistogramView::showHistogram] Program logic error. Histogram " << histogram_id
		<< " does not seem to be listed in the combobox."
		<< std::endl;
      return;
    }
    else {
      if (histogram_id != static_cast<unsigned int>(m_histogramNameList->currentIndex())) {
	m_histogramNameList->setCurrentIndex( histogram_id );
      }
      if (m_histogramId != histogram_id) {
	m_histogramId=histogram_id;
	// enforce redraw
	m_connectivityName.clear();
      }
      updateVisualiserList() ;
    }

    if ( visualiser_option < static_cast<unsigned int>(m_visualiserNameList->count()) ) {
      if (visualiser_option != static_cast<unsigned int>(m_visualiserNameList->currentIndex())) {
	m_visualiserNameList->setCurrentIndex( visualiser_option );
      }
      if (m_visualiserOption != visualiser_option) {
	m_visualiserOption = visualiser_option;
	// enforce redraw
	m_connectivityName.clear();
      }
    }

    setConnectivityName(conn_item_type, connectivity_name);
  }

  void HistogramView::changeDataSelection(EMeasurementType measurement_type, SerialNumber_t serial_number, EConnItemType conn_item_type )
  {
    assert( measurement_type < kNMeasurements);
    if (measurement_type != m_measurementType || serial_number != m_serialNumber || conn_item_type != m_connType ) {

      m_rebuildInfo =true;// enforce repopulating histogram info structure

      // remember current histogram name, to recover the selected histogram later
      std::string current_histogram_name;
      const HistogramChoice_t current_choice=( m_histogramChoiceList.isValid( m_histogramId ) ? m_histogramChoiceList[m_histogramId] : HistogramChoice_t::invalidChoice());

      if (current_choice.isValid() && m_histogramId < static_cast<unsigned int>(m_histogramNameList->count())) {
	current_histogram_name = m_histogramNameList->itemText(m_histogramId).toLatin1().data();
	if (current_choice.measurementType()==kAnalysis) {
	  if (current_histogram_name.compare(0,2, "A:")==0) {
	    current_histogram_name.erase(0,2);
	  }
	}
      }

      m_measurementType = measurement_type;
      m_serialNumber = serial_number;
      m_connType = conn_item_type;

      m_tdacHistogramId = static_cast<unsigned int>(-1);
      m_fdacHistogramId = static_cast<unsigned int>(-1);

      m_scanSerialNumber = m_calibrationData->scanSerialNumber( m_measurementType, m_serialNumber );
      std::cout << "INFO [HistogramView::changeDataSelection] type = " << m_measurementType << " serial number = " << m_serialNumber << std::endl;

      // --- (re)create histogram name list
      m_histogramChoiceList.clear();

      // always create a new listbox
     /* Q3ListBox *list_box;
      list_box=new Q3ListBox(m_histogramNameList,"HistogramNameListBox");
      m_histogramNameList->setListBox(list_box);
      list_box->clear();*/

      QListWidget *list_box;
      list_box = new QListWidget(m_histogramNameList);
      m_histogramNameList->setModel(list_box->model());
      m_histogramNameList->setView(list_box);
      list_box->clear();

      EConnItemType conn_item_type_arr[2]={m_connType, kModuleItem};
      unsigned int n_conn_item_types;
      if (m_connType == kPp0Item) {
	n_conn_item_types =2;
      }
      else {
	n_conn_item_types=1;
      }

      for (unsigned int conn_type_i=0; conn_type_i<n_conn_item_types; conn_type_i++) {

	const std::shared_ptr< std::vector< std::string > >
	  scan_histo_name_list_ptr = m_histogramCache->histogramList( kScan,
								      m_scanSerialNumber ,
								      conn_item_type_arr[conn_type_i]);

	const std::vector< std::string > &scan_histo_name_list( *scan_histo_name_list_ptr );

	if (scan_histo_name_list.size()>0) {
	  m_visualiserList.setVisualiserList( kScan,
					      conn_item_type_arr[conn_type_i],
					      m_visualiserListManager->createVisualiserList(kScan,
											    m_scanSerialNumber,
											    conn_item_type_arr[conn_type_i],
											    scan_histo_name_list) );
	}

	unsigned int list_i=0;

	{
	  unsigned int histo_i=0;
	  VisualiserListRef_t &scan_visualiser_list = m_visualiserList.visualiserList( kScan, conn_item_type_arr[conn_type_i] );
	  for( std::vector< std::string >::const_iterator histo_name_iter = scan_histo_name_list.begin();
	       histo_name_iter != scan_histo_name_list.end();
	       histo_name_iter++, histo_i++) {
	    if ( scan_visualiser_list.visualiser(histo_i).get() ) {
	      //list_box->addItem( histo_name_iter->c_str(), histo_i );
	      //StringtoInsert = QString::fromStdString(histo_name_iter);
	      list_box->insertItem(histo_i, QString::fromStdString(histo_name_iter->c_str()));
	      m_histogramChoiceList.addChoice(HistogramChoice_t(kScan, conn_item_type_arr[conn_type_i], list_i + histo_i));

	      if ( !current_histogram_name.empty()
		   && !current_choice.isConfigVisualiser()
		   && current_choice.measurementType() == kScan
		   && current_choice.connItemType()==conn_item_type_arr[conn_type_i]
		   && current_histogram_name == *histo_name_iter) {
		m_histogramId = list_i + histo_i;
	      }

	    }
	  }
	  list_i+=histo_i;
	}

	if (m_measurementType == kAnalysis) {

	  const std::shared_ptr< std::vector< std::string > >
	    histo_name_list_ptr = m_histogramCache->histogramList( kAnalysis,
								   m_serialNumber,
								   conn_item_type_arr[conn_type_i]);

	  const std::vector< std::string > &histo_name_list( *histo_name_list_ptr );

	  if (histo_name_list.size()>0) {
	    m_visualiserList.setVisualiserList(kAnalysis,
					       conn_item_type_arr[conn_type_i],
					       m_visualiserListManager->createVisualiserList(kAnalysis,
											     m_serialNumber,
											     conn_item_type_arr[conn_type_i],
											     histo_name_list) );
	  }

	  VisualiserListRef_t &analysis_visualiser_list = m_visualiserList.visualiserList( kAnalysis, conn_item_type_arr[conn_type_i] );
	  unsigned int histo_i=0;
	  for( std::vector< std::string >::const_iterator histo_name_iter = histo_name_list.begin();
	       histo_name_iter != histo_name_list.end();
	       histo_name_iter++, histo_i++) {

	    if ( analysis_visualiser_list.visualiser(histo_i).get() ) {
	      if (std::find(scan_histo_name_list.begin(),scan_histo_name_list.end(),*histo_name_iter) != scan_histo_name_list.end() ) {
		//list_box->addItem(  (std::string("A:")+*histo_name_iter).c_str(), list_i + histo_i);
		list_box->insertItem(list_i + histo_i, QString::fromStdString( (std::string("A:")+*histo_name_iter).c_str() ));
	      }
	      else {
		list_box->insertItem( list_i + histo_i, QString::fromStdString( histo_name_iter->c_str()  ));
	      }
	      m_histogramChoiceList.addChoice(HistogramChoice_t(kAnalysis, conn_item_type_arr[conn_type_i], histo_i));
	      if ( !current_histogram_name.empty() 
		   && !current_choice.isConfigVisualiser()
		   && current_choice.measurementType() == kAnalysis
		   && current_choice.connItemType()==conn_item_type_arr[conn_type_i]
		   && current_histogram_name == *histo_name_iter) {
		m_histogramId = list_i + histo_i;
	      }
	    }
	  }
	  list_i+= histo_i;

	}

	if (list_box->count()==0) {
	  list_box->insertItem(INT_MAX,QString::fromStdString("none"));
	}

	if ( conn_item_type_arr[conn_type_i] == kModuleItem) {
	  m_histogramId=static_cast<unsigned int>(-1);
	  unsigned int histo_i=0;
	  m_tdacHistogramId = list_i + kTDACMapVisualiser;
	  m_fdacHistogramId = list_i + kFDACMapVisualiser;

	  for (std::vector<std::string>::const_iterator config_visu_iter=m_configVisualiserName.begin();
	       config_visu_iter != m_configVisualiserName.end();
	       config_visu_iter++, histo_i++) {
	    if (current_histogram_name == *config_visu_iter) {
	      m_histogramId=histo_i;
	    }
	    list_box->insertItem(list_i + histo_i, new GreyedListBoxText(config_visu_iter->c_str()) );
	    m_histogramChoiceList.addChoice(HistogramChoice_t(m_measurementType, kModuleItem, histo_i,true));

	    if (  !current_histogram_name.empty() 
		&& current_choice.isConfigVisualiser() && current_choice.connItemType()==kModuleItem && histo_i == current_choice.histogramId()) {
	      m_histogramId = list_i + histo_i;
	    }

	  }
	}

      }

      if (m_histogramChoiceList.isValid(m_histogramId) ) {
	m_histogramId=0;
      }

      if (   m_histogramId < static_cast<unsigned int>(m_histogramNameList->count())
	  && static_cast<unsigned int>(m_histogramNameList->currentIndex()) == m_histogramId) {

	m_histogramNameList->setCurrentIndex( m_histogramId );
      }


      updateVisualiserList();
    }
  }

  void HistogramView::changeHistogramArrayIndex(int loop_index, int new_histo_array_index)
  {
    unsigned int dimension_i;
    if (loop_index==3) {
      dimension_i=0;
      while (m_currentInfo.maxIndex(dimension_i)>0 && dimension_i<3) dimension_i++;
    }
    else {
      dimension_i=loop_index;
    }
    if (loop_index>=0 && loop_index<4 && new_histo_array_index>=0) {
      QSpinBox *spinbox_arr[4]={m_selectorLoop0, m_selectorLoop1, m_selectorLoop2, m_selectorHistoIndex};
      if (spinbox_arr[loop_index]->isEnabled()) {
	if (dimension_i >= m_histoIndex.size() ) {
	  m_histoIndex.resize(dimension_i+1,0);
	}
	if ( m_histoIndex[dimension_i] != static_cast<unsigned int>(new_histo_array_index) ) {
	  m_histoIndex[dimension_i] = static_cast<unsigned int>(new_histo_array_index);
	  visualise(m_connType, m_connectivityName, m_histogramId, m_visualiserOption);
	}
      }
    }
  }

  void HistogramView::toggleConnectionToMainPanel() {
    m_connectedToMainPanel ^= true;
    if (!m_connectedToMainPanel) {
      m_newWindowButton->setIcon(QIcon(QPixmap(":/icons/images/stock_disconnect.png")));
    }
    else {
      m_newWindowButton->setIcon(QIcon(QPixmap(":/icons/images/stock_connect.png")));
      emit connectionToMainPanel(objectName());
    }
  }

  void HistogramView::closeEvent(QCloseEvent *)
  {
    std::cout << "DEBUG [HistogramView::closeEvent] called for " << this << "\n";
    emit close(objectName());
  }

  void HistogramView::printToFile() 
  {
        QString path = QString::null;

    // guess initial search path
    if(m_lastPrintPath.length()==0) {
      path = m_lastPrintPath.c_str();
    }

    QStringList filter;
    filter <<  "Bitmap (*.png)";
    filter +=  "Encapsulated Postscript (*.eps)";
    filter +=  "Postscript (*.ps)";
    filter +=  "PDF (*.pdf)";
    filter +=  "ROOT data file (*.root)";
    filter +=  "C-Source (*.C)";
    filter +=  "Any file (*.*)";

    QFileDialog* dialog = new QFileDialog(this, "Print/save canvas to file", QDir::homePath()+"/histogram.png", QString::null);
    dialog->setFileMode(QFileDialog::AnyFile);
    dialog->setAcceptMode(QFileDialog::AcceptSave);
    dialog->setNameFilters(filter);
    if(dialog->exec() == QDialog::Accepted) {
      TCanvas *the_canvas = m_canvas->getCanvas();
      if (the_canvas) {
	the_canvas->SaveAs( dialog->selectedFiles().at(0).toLatin1().data() );
	std::cout << "INFO [DetectorView::printToFile] Wrote file " << std::endl;
      }
    }
  }

}
