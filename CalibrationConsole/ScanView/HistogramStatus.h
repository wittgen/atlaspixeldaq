#ifndef _PixCon_HistogramStatus_h_
#define _PixCon_HistogramStatus_h_

#include "VarDefList.h"

namespace PixCon {

  class HistogramStatus
  {
  public:

    enum EHistogramState {kPresumablyDisabled, kNoneExpected, kOk, kOkButBelowExpectation, kOkButAboveExpectation, kMissingHistograms, kTooManyHistograms, kNStates};

    /** Define the histogram status variable.
     */
    static VarDef_t defineVar();

    /** Internal function which produces a status depending on the number of found histograms.
     */
    static State_t state(unsigned int n_histograms_expected, unsigned int n_histograms_observed) 
    {
      State_t histogram_number_state=kNStates;
      if (n_histograms_observed == n_histograms_expected) {
	if (n_histograms_expected==0) {
	  histogram_number_state = kNoneExpected;
	}
	else {
	  histogram_number_state = kOk;
	}
      }
      else if (n_histograms_observed == 0) {
	histogram_number_state = kPresumablyDisabled;
      }
      else if (n_histograms_observed < n_histograms_expected) {
	histogram_number_state = kMissingHistograms;
      }
      else if (n_histograms_observed > n_histograms_expected) {
	histogram_number_state  = kTooManyHistograms;
      }
      return histogram_number_state ;
    }

  private:
    static const std::string s_histogramStateName;
  };
  
}
#endif
