#ifndef UI_HISTOGRAMVIEW_H
#define UI_HISTOGRAMVIEW_H

#include <QComboBox>
#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QCheckBox>
#include <QFrame>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QMainWindow>
#include <QPushButton>
#include <QSpinBox>
#include <QStatusBar>
#include <QVBoxLayout>
#include <QWidget>

class MyQRootCanvas;

QT_BEGIN_NAMESPACE

class Ui_HistogramView
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *hboxLayout;
    QPushButton *m_newWindowButton;
    QComboBox *m_histogramNameList;//
    QComboBox *m_visualiserNameList;
    QPushButton *m_prevConnObj;
    QLineEdit *m_connObjName;
    QPushButton *m_nextConnObj;
    QHBoxLayout *horizontalLayout;
    QPushButton *m_printButton;
    QLabel *textLabel1_6;
    QSpinBox *m_selectorLoop0;
    QLabel *textLabel1_6_2;
    QSpinBox *m_selectorLoop1;
    QLabel *textLabel1_6_2_2;
    QSpinBox *m_selectorLoop2;
    QLabel *textLabel1_6_2_2_2;
    QSpinBox *m_selectorHistoIndex;
    QCheckBox *m_showOneFrontEnd;
    QSpinBox *m_frontEnd;
    QFrame *line1;
    QFrame *frame;
    QVBoxLayout *frameLayout;
    MyQRootCanvas *m_canvas=0;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *HistogramView)
    {
        if (HistogramView->objectName().isEmpty())
            HistogramView->setObjectName(QString::fromUtf8("HistogramView"));
        HistogramView->resize(602, 832);
        centralwidget = new QWidget(HistogramView);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout_2 = new QVBoxLayout(centralwidget);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        hboxLayout = new QHBoxLayout();
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        m_newWindowButton = new QPushButton(centralwidget);
        m_newWindowButton->setObjectName(QString::fromUtf8("m_newWindowButton"));
        m_newWindowButton->setFlat(true);

        hboxLayout->addWidget(m_newWindowButton);

        m_histogramNameList = new QComboBox(centralwidget);//
        m_histogramNameList->setObjectName(QString::fromUtf8("m_histogramNameList"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(m_histogramNameList->sizePolicy().hasHeightForWidth());
        m_histogramNameList->setSizePolicy(sizePolicy);

        hboxLayout->addWidget(m_histogramNameList);

        m_visualiserNameList = new QComboBox(centralwidget);
        m_visualiserNameList->setObjectName(QString::fromUtf8("m_visualiserNameList"));
        sizePolicy.setHeightForWidth(m_visualiserNameList->sizePolicy().hasHeightForWidth());
        m_visualiserNameList->setSizePolicy(sizePolicy);

        hboxLayout->addWidget(m_visualiserNameList);

        m_prevConnObj = new QPushButton(centralwidget);
        m_prevConnObj->setObjectName(QString::fromUtf8("m_prevConnObj"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(m_prevConnObj->sizePolicy().hasHeightForWidth());
        m_prevConnObj->setSizePolicy(sizePolicy1);

        hboxLayout->addWidget(m_prevConnObj);

        m_connObjName = new QLineEdit(centralwidget);
        m_connObjName->setObjectName(QString::fromUtf8("m_connObjName"));
        m_connObjName->setReadOnly(true);

        hboxLayout->addWidget(m_connObjName);

        m_nextConnObj = new QPushButton(centralwidget);
        m_nextConnObj->setObjectName(QString::fromUtf8("m_nextConnObj"));
        sizePolicy1.setHeightForWidth(m_nextConnObj->sizePolicy().hasHeightForWidth());
        m_nextConnObj->setSizePolicy(sizePolicy1);

        hboxLayout->addWidget(m_nextConnObj);


        verticalLayout->addLayout(hboxLayout);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        m_printButton = new QPushButton(centralwidget);
        m_printButton->setObjectName(QString::fromUtf8("m_printButton"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(m_printButton->sizePolicy().hasHeightForWidth());
        m_printButton->setSizePolicy(sizePolicy2);
        m_printButton->setMinimumSize(QSize(16, 8));
        m_printButton->setMaximumSize(QSize(32767, 20));
        m_printButton->setFlat(true);

        horizontalLayout->addWidget(m_printButton);

        textLabel1_6 = new QLabel(centralwidget);
        textLabel1_6->setObjectName(QString::fromUtf8("textLabel1_6"));
        textLabel1_6->setWordWrap(false);

        horizontalLayout->addWidget(textLabel1_6);

        m_selectorLoop0 = new QSpinBox(centralwidget);
        m_selectorLoop0->setObjectName(QString::fromUtf8("m_selectorLoop0"));
        sizePolicy.setHeightForWidth(m_selectorLoop0->sizePolicy().hasHeightForWidth());
        m_selectorLoop0->setSizePolicy(sizePolicy);
        m_selectorLoop0->setMinimumSize(QSize(50, 0));
        m_selectorLoop0->setMinimum(-1);
        m_selectorLoop0->setMaximum(9999);

        horizontalLayout->addWidget(m_selectorLoop0);

        textLabel1_6_2 = new QLabel(centralwidget);
        textLabel1_6_2->setObjectName(QString::fromUtf8("textLabel1_6_2"));
        textLabel1_6_2->setWordWrap(false);

        horizontalLayout->addWidget(textLabel1_6_2);

        m_selectorLoop1 = new QSpinBox(centralwidget);
        m_selectorLoop1->setObjectName(QString::fromUtf8("m_selectorLoop1"));
        sizePolicy.setHeightForWidth(m_selectorLoop1->sizePolicy().hasHeightForWidth());
        m_selectorLoop1->setSizePolicy(sizePolicy);
        m_selectorLoop1->setMinimumSize(QSize(50, 0));
        m_selectorLoop1->setMinimum(-1);
        m_selectorLoop1->setMaximum(9999);

        horizontalLayout->addWidget(m_selectorLoop1);

        textLabel1_6_2_2 = new QLabel(centralwidget);
        textLabel1_6_2_2->setObjectName(QString::fromUtf8("textLabel1_6_2_2"));
        textLabel1_6_2_2->setWordWrap(false);

        horizontalLayout->addWidget(textLabel1_6_2_2);

        m_selectorLoop2 = new QSpinBox(centralwidget);
        m_selectorLoop2->setObjectName(QString::fromUtf8("m_selectorLoop2"));
        sizePolicy.setHeightForWidth(m_selectorLoop2->sizePolicy().hasHeightForWidth());
        m_selectorLoop2->setSizePolicy(sizePolicy);
        m_selectorLoop2->setMinimumSize(QSize(50, 0));
        m_selectorLoop2->setMinimum(-1);
        m_selectorLoop2->setMaximum(9999);

        horizontalLayout->addWidget(m_selectorLoop2);

        textLabel1_6_2_2_2 = new QLabel(centralwidget);
        textLabel1_6_2_2_2->setObjectName(QString::fromUtf8("textLabel1_6_2_2_2"));
        textLabel1_6_2_2_2->setWordWrap(false);

        horizontalLayout->addWidget(textLabel1_6_2_2_2);

        m_selectorHistoIndex = new QSpinBox(centralwidget);
        m_selectorHistoIndex->setObjectName(QString::fromUtf8("m_selectorHistoIndex"));
        sizePolicy.setHeightForWidth(m_selectorHistoIndex->sizePolicy().hasHeightForWidth());
        m_selectorHistoIndex->setSizePolicy(sizePolicy);
        m_selectorHistoIndex->setMinimumSize(QSize(50, 0));
        m_selectorHistoIndex->setMinimum(-1);
        m_selectorHistoIndex->setMaximum(9999);

        horizontalLayout->addWidget(m_selectorHistoIndex);

        m_showOneFrontEnd = new QCheckBox(centralwidget);
        m_showOneFrontEnd->setObjectName(QString::fromUtf8("m_showOneFrontEnd"));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(m_showOneFrontEnd->sizePolicy().hasHeightForWidth());
        m_showOneFrontEnd->setSizePolicy(sizePolicy3);

        horizontalLayout->addWidget(m_showOneFrontEnd);

        m_frontEnd = new QSpinBox(centralwidget);
        m_frontEnd->setObjectName(QString::fromUtf8("m_frontEnd"));
        m_frontEnd->setEnabled(true);
        sizePolicy3.setHeightForWidth(m_frontEnd->sizePolicy().hasHeightForWidth());
        m_frontEnd->setSizePolicy(sizePolicy3);
        m_frontEnd->setMaximum(15);

        horizontalLayout->addWidget(m_frontEnd);


        verticalLayout->addLayout(horizontalLayout);

        line1 = new QFrame(centralwidget);
        line1->setObjectName(QString::fromUtf8("line1"));
        line1->setFrameShape(QFrame::HLine);
        line1->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line1);

        frame = new QFrame(centralwidget);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setMinimumSize(QSize(0, 0));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        frame->setLineWidth(0);
        frameLayout = new QVBoxLayout(frame);
        frameLayout->setContentsMargins(0, 0, 0, 0);
        frameLayout->setObjectName(QString::fromUtf8("frameLayout"));
/*         m_canvas = new TQRootCanvas(frame); */
/*         m_canvas->setObjectName(QString::fromUtf8("m_canvas")); */
/*         QSizePolicy sizePolicy4(QSizePolicy::Expanding, QSizePolicy::Expanding); */
/*         sizePolicy4.setHorizontalStretch(0); */
/*         sizePolicy4.setVerticalStretch(0); */
/*         sizePolicy4.setHeightForWidth(m_canvas->sizePolicy().hasHeightForWidth()); */
/*         m_canvas->setSizePolicy(sizePolicy4); */

/*         frameLayout->addWidget(m_canvas); */


        verticalLayout->addWidget(frame);


        verticalLayout_2->addLayout(verticalLayout);

        HistogramView->setCentralWidget(centralwidget);
        statusbar = new QStatusBar(HistogramView);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        HistogramView->setStatusBar(statusbar);

        retranslateUi(HistogramView);

        QMetaObject::connectSlotsByName(HistogramView);
    } // setupUi

    void retranslateUi(QMainWindow *HistogramView)
    {
        HistogramView->setWindowTitle(QApplication::translate("HistogramView", "HistogramView", 0));
#ifndef QT_NO_TOOLTIP
        m_newWindowButton->setToolTip(QApplication::translate("HistogramView", "New Window", 0));
#endif // QT_NO_TOOLTIP
        m_newWindowButton->setText(QString());
#ifndef QT_NO_TOOLTIP
        m_histogramNameList->setToolTip(QApplication::translate("HistogramView", "The histogram to be visualised.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        m_visualiserNameList->setToolTip(QApplication::translate("HistogramView", "The visualiser to be used.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        m_prevConnObj->setToolTip(QApplication::translate("HistogramView", "Go to the previous module / PP0.", 0));
#endif // QT_NO_TOOLTIP
        m_prevConnObj->setText(QString());
#ifndef QT_NO_TOOLTIP
        m_nextConnObj->setToolTip(QApplication::translate("HistogramView", "Go to the next module / PP0.", 0));
#endif // QT_NO_TOOLTIP
        m_nextConnObj->setText(QString());
#ifndef QT_NO_TOOLTIP
        m_printButton->setToolTip(QApplication::translate("HistogramView", "Print canvas to file", 0));
#endif // QT_NO_TOOLTIP
        m_printButton->setText(QString());
        textLabel1_6->setText(QApplication::translate("HistogramView", "Loop Indices :: 2 :", 0));
#ifndef QT_NO_TOOLTIP
        m_selectorLoop0->setToolTip(QApplication::translate("HistogramView", "First index (highest) in the histogram tree.", 0));
#endif // QT_NO_TOOLTIP
        textLabel1_6_2->setText(QApplication::translate("HistogramView", "1 :", 0));
#ifndef QT_NO_TOOLTIP
        m_selectorLoop1->setToolTip(QApplication::translate("HistogramView", "Second level index in the histogram tree.", 0));
#endif // QT_NO_TOOLTIP
        textLabel1_6_2_2->setText(QApplication::translate("HistogramView", "0 :", 0));
#ifndef QT_NO_TOOLTIP
        m_selectorLoop2->setToolTip(QApplication::translate("HistogramView", "Third level index in the histogram tree.", 0));
#endif // QT_NO_TOOLTIP
        textLabel1_6_2_2_2->setText(QApplication::translate("HistogramView", "/", 0));
#ifndef QT_NO_TOOLTIP
        m_selectorHistoIndex->setToolTip(QApplication::translate("HistogramView", "Histogram Index (lowest level).", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        m_showOneFrontEnd->setToolTip(QApplication::translate("HistogramView", "Show only one front-end instead of the whole module", 0));
#endif // QT_NO_TOOLTIP
        m_showOneFrontEnd->setText(QApplication::translate("HistogramView", "One FE :", 0));
#ifndef QT_NO_TOOLTIP
        m_frontEnd->setToolTip(QApplication::translate("HistogramView", "The front-end to be shown.", 0));
#endif // QT_NO_TOOLTIP
    } // retranslateUi

};

namespace Ui {
    class HistogramView: public Ui_HistogramView {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_HISTOGRAMVIEW_H
