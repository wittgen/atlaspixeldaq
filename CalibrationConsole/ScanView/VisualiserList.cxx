#include "VisualiserList.h"
#include <Visualiser/VisualisationKitSubSet_t.h>
#include <Visualiser/VisualisationFactory.h>
#include <Visualiser/IVisualiser.h>

namespace PixCon {


  const VisualiserListRef_t VisualiserList::createVisualiserList(EMeasurementType type,
								 SerialNumber_t serial_number,
								 EConnItemType conn_item_type,
								 const std::vector<std::string> &histogram_name_list)
  {

    std::map<SerialNumber_t, ConnItemCollection<VisualiserList_t> >::iterator list_iter_1 = findList(type, serial_number);
    if (list_iter_1 != endList(type)) {
      if (list_iter_1->second[conn_item_type].nHistogramsConsidered()==histogram_name_list.size()) {
	return list_iter_1->second[conn_item_type];
      }
    }

    if (list_iter_1 == endList(type)) {
    // cleanup
    {
      std::map<SerialNumber_t, ConnItemCollection<VisualiserList_t> >::iterator list_iter_2 = beginList(type);
      bool reached_serial_number_list_end = (list_iter_2 == endList(type));
      while (!reached_serial_number_list_end) {

	std::map<SerialNumber_t, ConnItemCollection<VisualiserList_t> >::iterator current_list_iter = list_iter_2++;
	reached_serial_number_list_end = (list_iter_2 == endList(type));

	if (!current_list_iter->second[conn_item_type].clearUseCount()) {
	  eraseList(type, current_list_iter);
	}
      }
    }

    std::pair< std::map<SerialNumber_t, ConnItemCollection<VisualiserList_t> >::iterator , bool> 
      ret = m_visualiserMap[type].insert( std::make_pair( serial_number, ConnItemCollection<VisualiserList_t>() ) );
    if (!ret.second) {
      std::stringstream message;
	message << "ERROR [VisualiserList::visualiserList] Failed to add visualiser for " << makeSerialNumberString(type,serial_number);
      throw std::runtime_error( message.str() );
    }
      list_iter_1 = ret.first;
    }

    // add new visualiser list.
    VisualiserList_t &visualiser_list = list_iter_1->second[conn_item_type];

    unsigned int histo_id=0;
    for (std::vector<std::string>::const_iterator histo_name_iter = histogram_name_list.begin();
	 histo_name_iter != histogram_name_list.end();
	 histo_name_iter++, histo_id++) {
      PixA::VisualisationKitSubSet_t kits;
      PixA::VisualisationFactory::getKits( *histo_name_iter, kits );

      std::vector<std::string> empty_folder_list;
      std::shared_ptr<PixA::IVisualiser> a_visualiser( PixA::VisualisationFactory::visualiser(kits,*histo_name_iter, empty_folder_list) );
      if (a_visualiser.get() ) {
	visualiser_list.setVisualiser(histo_id, a_visualiser);
      }
    }
    visualiser_list.setNHistogramsConsidered( histogram_name_list.size());

    return visualiser_list;
  }

}
