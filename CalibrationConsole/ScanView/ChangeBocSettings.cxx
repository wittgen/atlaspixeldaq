#include "ChangeBocSettings.h"

#include<iostream>

namespace PixCon {
  std::pair<int,bool> getInt(const std::string &text) {
    std::string::size_type pos=0;
    while (pos < text.size() && isspace(text[pos])) pos++;

    if (pos>=text.size()) {
      return std::make_pair(0,false);
    }

    long int ret;
    char *pEnd;
    if (pos+3<text.size() && text[pos]=='0' && text[pos+1]=='x') {
      ret = strtol(&(const_cast<char *>(text.c_str())[pos]),&pEnd,16);
    }
    else {
      ret = strtol(&(const_cast<char *>(text.c_str())[pos]),&pEnd,10);
    }

    if (pEnd && (!*pEnd || isspace(*pEnd)) && abs(ret) < INT_MAX) {
      //      std::cout << "INFO [getInt] " << text << " -> " <<ret << "." << std::endl;
      return std::make_pair<int,bool>(static_cast<int>(ret),true);
    }
    else {
      //      std::cout << "INFO [getInt] " << text << " -> failed " <<  std::endl;
      return std::make_pair<int,bool>(static_cast<int>(0),false);
    }
  }

  int getInt(int ref, QLineEdit *field) {
    std::pair<int,bool> field_value = getInt(field->text().toLatin1().data());
    if (field_value.second) {
      //       std::cout << "INFO [getInt] "<<  field->text().toLatin1().data()
      // 		<< "[" << ref << "] -> " << field_value.first <<  std::endl;
      return field_value.first;
    }

    return ref;
  }

  std::pair<float,bool> getFloat(const std::string &text) {
    std::string::size_type pos=0;
    while (pos < text.size() && isspace(text[pos])) pos++;

    if (pos>=text.size()) {
      return std::make_pair(0.0,false);
    }

    float ret;
    char *pEnd;
    ret = strtof(&(const_cast<char *>(text.c_str())[pos]),&pEnd);

    if (pEnd && (!*pEnd || isspace(*pEnd))) {
      std::cout << "INFO [getFloat] " << text << " -> " <<ret << "." << std::endl;
      return std::make_pair<float,bool>(static_cast<float>(ret),true);
    }
    else {
      std::cout << "INFO [getFloat] " << text << " -> failed " <<  std::endl;
      return std::make_pair<float,bool>(static_cast<float>(0),false);
    }
  }

  float getFloat(float ref, QLineEdit *field) {
    std::pair<float,bool> field_value = getFloat(field->text().toLatin1().data());
    if (field_value.second) {
      //       std::cout << "INFO [getInt] "<<  field->text().toLatin1().data()
      // 		<< "[" << ref << "] -> " << field_value.first <<  std::endl;
      return field_value.first;
    }

    return ref;
  }

  ChangeBocSettings::ChangeBocSettings( const std::string &title, unsigned int link, QWidget* parent )
: QDialog(parent),
      m_rxDelayRef(0),
      m_rxThresholdRef(0),
      m_txPowerRef(0)
  {
    setupUi(this);
    setTitle(title); 
    if (link%2==0) {
      textLabel2->setText(textLabel2_2->text());
    }
    m_frameDTO2->hide();
    m_frameTx->hide();
    adjustSize();
    //  }
    //    else {
    //      m_frameDTO1->setEnabed(false);
    //  }
  }

  ChangeBocSettings::~ChangeBocSettings() {}

  bool ChangeBocSettings::hasRxDelay() const {
    std::pair<float,bool> field_value = getFloat(m_delayEdit->text().toLatin1().data());
    return field_value.second;
  }

  float ChangeBocSettings::rxDelay() const {
    return getFloat(m_rxDelayRef, m_delayEdit);
  }

  bool ChangeBocSettings::hasRxThreshold() const {
    std::pair<float,bool> field_value = getFloat(m_thresholdEdit->text().toLatin1().data());
    return field_value.second;
  }

 float ChangeBocSettings::rxThreshold() const {
    return getFloat(m_rxThresholdRef, m_thresholdEdit);
  }

  bool ChangeBocSettings::hasTxPower() const {
    std::pair<int,bool> field_value = getInt(m_powerEdit->text().toLatin1().data());
    return field_value.second;
  }

 int ChangeBocSettings::txPower() const {
    return getInt(m_txPowerRef, m_powerEdit);
  }

}
