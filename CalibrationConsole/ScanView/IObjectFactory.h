#ifndef _PixCon_IObjectFactory_h_
#define _PixCon_IObjectFactory_h_

#include <string>
#include <memory>
#include <vector>

namespace PixCon {

  template <class T>
  class IObjectFactory
  {  
  protected:
  public:
    enum EPriority { kLowestPriority=0, kNormalPriority=50 };

    virtual ~IObjectFactory() {}

    virtual const T * const _kit(const std::string &name) const = 0;

    virtual void _kitList(const std::string &name, std::vector< std::shared_ptr<T> > &a_list)  = 0;

    virtual void _registerKit( const std::string &pattern, T *a_kit, unsigned int priority ) = 0;

  public:

    static IObjectFactory<T> *instance();

    static void registerKit( const std::string &pattern, T *a_kit, unsigned int priority = kNormalPriority) {
      instance()->_registerKit(pattern, a_kit, priority);
    }

    static const T * const kit(const std::string &name) {
      return instance()->_kit(name);
    }

    static void kitList(const std::string &name, std::vector< std::shared_ptr<T> > &list) {
      return instance()->_kitList(name,list);
    }


  private:

    static IObjectFactory *s_instance;

  };

}
#endif
