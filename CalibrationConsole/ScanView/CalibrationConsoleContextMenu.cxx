#include "CalibrationConsoleContextMenu.h"
#include "CalibrationDataStyle.h"
#include "HistogramCache.h"
#include <Visualiser/IVisualiser.h>
#include <ConfigWrapper/ConnectivityUtil.h>

#include <QMenu>
//#include <q3popupmenu.h>
#include <qclipboard.h>
#include <qapplication.h>
#include <qmessagebox.h>

#include "PixUtilities/PixISManager.h"

namespace PixCon {

  CalibrationConsoleContextMenu::CalibrationConsoleContextMenu(const std::shared_ptr<CalibrationDataManager> &calibration_data,
							       const std::shared_ptr<HistogramCache> &histogram_cache,
							       const std::shared_ptr<VisualiserList> &visualiser_list,
				                               std::string const & partition_name)
    : DataSelectionSpecificContextMenu(nullptr),
      m_calibrationData(calibration_data),
      m_partitionName(partition_name),
      m_histogramCache(histogram_cache),
      m_lastScanSerialNumber(0),
      m_measurementType(kCurrent),
      m_currentSerialNumber(0),
      m_currentVar(VarDef_t::invalid()),
      m_visualiserList(visualiser_list),
      m_signals(new CalibrationConsoleContextMenuSignals(this))
  {
    std::fill(m_nScanHistograms.begin(),m_nScanHistograms.end(),static_cast<unsigned int>(0));
  }

  void CalibrationConsoleContextMenu::showMenu(EConnItemType conn_type, const std::string &connectivity_name)
  {
    if (conn_type == kNConnItemTypes) {
    
      QMenu context_menu_q4(QString::fromStdString(connectivity_name));
      /*QAction *aPrintModule = */
      (void)context_menu_q4.addAction("Print Module List ",this, SLOT(printModules()) );
      context_menu_q4.exec(QCursor::pos());

    }
    else {

      try {
	CalibrationData::EnableState_t state;
	assert(( m_measurementType == kCurrent && m_currentSerialNumber==0)|| (m_measurementType != kCurrent && m_currentSerialNumber>0) );
	bool has_enabled_modules=false;
	if(conn_type != kPp0Item) {
	  try {
	    CalibrationData::ConnObjDataList conn_object_data( m_calibrationData->calibrationData().getConnObjectData(connectivity_name));
	    state = conn_object_data.enableState(m_measurementType, m_currentSerialNumber);
	    if ( !state.isInConnectivity())  return;
	    if (state.enabled()) {
	      has_enabled_modules=true;
	    }
	  }
	  catch (CalibrationData::MissingConnObject &err) {
	    if (conn_type != kPp0Item) {
	      std::cout << "WARNING [CalibrationConsoleContextMenu::showMenu] no conn object for  " << connectivity_name << " ." << std::endl;
	      return;
	    }
	  }
	}
	else {
	  PixA::ConnectivityRef conn = m_calibrationData->connectivity(m_measurementType, m_currentSerialNumber);
	  try {
	    PixA::ModuleList module_list=conn.modules(connectivity_name);
	    
	    PixA::ModuleList::const_iterator module_begin = module_list.begin();
	    PixA::ModuleList::const_iterator module_end = module_list.end();
	    
	    state.setInConnectivity(true);
	    state.setInConfiguration(true);
	    state.setParentEnabled(true);
	    state.setEnabled(true);
	    
	    for (PixA::ModuleList::const_iterator module_iter = module_begin;
		 module_iter != module_end;
		 ++module_iter) {
	      
	      try {
		CalibrationData::ConnObjDataList conn_object_data( m_calibrationData->calibrationData().getConnObjectData( PixA::connectivityName(module_iter) ) );
		CalibrationData::EnableState_t module_state = conn_object_data.enableState(m_measurementType, m_currentSerialNumber);
		state &= module_state;
		if (module_state.enabled()) {
		  has_enabled_modules=true;
		}
	      }
	      catch (CalibrationData::MissingConnObject &err) {
		state.setInConnectivity(false);
		state.setInConfiguration(false);
		state.setEnabled(false);
	      }
	    }
	  }
	  catch (PixA::ConfigException &err) {
	    std::cout << "WARNING [CalibrationConsoleContextMenu::showMenu] no modules for Pp0 object " << connectivity_name << " ." << std::endl;
	  }
	}
	
	//std::vector< VisualisationChoice_t > histo_id_list;
	m_histo_id_list.clear();
	
	QMenu context_menu_q4(QString::fromStdString(connectivity_name));
	QAction *aTitle = context_menu_q4.addAction(QString::fromStdString(connectivity_name));
	aTitle->setEnabled(false);
	context_menu_q4.addSeparator();
	QAction *aEnable = 0;
	if (has_enabled_modules && (!state.enabled())) {
	  aEnable = context_menu_q4.addAction("Partially Enabled ", this,  SLOT(toggleEnableState()));
	  aEnable->setCheckable(true);
	  aEnable->setChecked(true);
	} else{
	  aEnable = context_menu_q4.addAction("Enabled ", this,  SLOT(toggleEnableState()));
	  aEnable->setCheckable(true);
	  aEnable->setChecked(state.enabled());
	}
	aEnable->setEnabled(state.isInConfiguration());
	QVariant qConnType(conn_type);
	QVariant qConnName(QString::fromStdString(connectivity_name));
	QList <QVariant> qvList;
	qvList.append(qConnType);
	qvList.append(qConnName);
	aEnable->setData(qvList);

	switch(conn_type) {
  	 case kModuleItem:
	 case kPp0Item:
	 case kTimItem:
	 case kRodItem: {
	  
	  std::string daqEnabledName("DAQEnabled");
	  std::string daqAllocatedName("AVAILABLE");
	  if ((conn_type == kModuleItem || conn_type == kRodItem)) {
	    VarDef_t daqAllocatedVar(VarDef_t::invalid());
	    try {
	      daqAllocatedVar = VarDefList::instance()->getVarDef(daqAllocatedName);
	    }
	    catch (UndefinedCalibrationVariable&) {
	    }
	    bool daqAllocated = false;
	    if (daqAllocatedVar.isValid() == true) {
	      std::string daqAllocatedRodName("");
	      if (conn_type == kRodItem) {
		daqAllocatedRodName = connectivity_name;
	      }
	      else {
		PixA::ConnectivityRef conn = m_calibrationData->connectivity(m_measurementType, m_currentSerialNumber);
		if (conn) {
		  const PixLib::ModuleConnectivity* module = conn.findModule(connectivity_name);
		  if (module) {
		    const PixLib::RodBocConnectivity* rod = PixA::connectedRod(module);
		    if (rod) {
		      daqAllocatedRodName = PixA::connectivityName(rod);
		    }
		  }
		}
	      }
	      CalibrationData::ConnObjDataList conn_object_data(m_calibrationData->calibrationData().getConnObjectData(daqAllocatedRodName));
	      try {
		State_t daqAllocatedState = conn_object_data.newestValue<State_t>(m_measurementType, m_currentSerialNumber, daqAllocatedVar);
		if (daqAllocatedState == 0) daqAllocated = true;
		else daqAllocated = false;
	      }
	      catch (CalibrationData::MissingValue&) {
	      }
	    }
	    VarDef_t daqEnabledVar(VarDef_t::invalid());
	    try {
	      daqEnabledVar = VarDefList::instance()->getVarDef(daqEnabledName);
	    }
	    catch (UndefinedCalibrationVariable&) {
	    }
	    if (daqEnabledVar.isValid() == true) {
	      QAction *aDAQEnable = context_menu_q4.addAction(QString::fromStdString(daqEnabledName));
	      aDAQEnable->setCheckable(true);
	      if (daqAllocated == false) {
		aDAQEnable->setChecked(false);
		aDAQEnable->setEnabled(false);
		std::cout << "WARNING [CalibrationConsoleContextMenu::showMenu] DAQ disable/enable is not possible for "
			  << connectivity_name << ", as ROD is not allocated." << std::endl;
	      }
	      else {
		CalibrationData::ConnObjDataList conn_object_data(m_calibrationData->calibrationData().getConnObjectData(connectivity_name));
		try {
		  State_t daqEnabledState = conn_object_data.newestValue<State_t>(m_measurementType, m_currentSerialNumber, daqEnabledVar);
		  if (daqEnabledState == 0) {
		    aDAQEnable->setChecked(true);
		    aDAQEnable->setEnabled(true);
		  }
		  else if (daqEnabledState == 1) {
		    aDAQEnable->setChecked(false);
		    aDAQEnable->setEnabled(false);
		    std::cout << "WARNING [CalibrationConsoleContextMenu::showMenu] " <<  connectivity_name
			      << " is DAQ disabled at run config, so cannot be re-enabled." << std::endl;
		  }
		  else if (daqEnabledState == 2) {
		    aDAQEnable->setChecked(false);
		    aDAQEnable->setEnabled(false);
		    std::cout << "WARNING [CalibrationConsoleContextMenu::showMenu] " <<  connectivity_name
			      << " is DAQ auto-disabled due to a timeout, so cannot be re-enabled." << std::endl;
		  }
		  else if (daqEnabledState == 3 || daqEnabledState == 4) {
		    aDAQEnable->setChecked(false);
		    aDAQEnable->setEnabled(true);
		  }
		  else {
		    aDAQEnable->setChecked(false);
		    aDAQEnable->setEnabled(false);
		    std::cout << "WARNING [CalibrationConsoleContextMenu::showMenu] "
			      << connectivity_name << " has an unrecognized " << daqEnabledName
			      << " state of " << daqEnabledState << "." << std::endl;
		  }
		}
		catch (CalibrationData::MissingValue&) {
		  aDAQEnable->setChecked(false);
		  aDAQEnable->setEnabled(false);
		    std::cout << "WARNING [CalibrationConsoleContextMenu::showMenu] " << daqEnabledName
			      << " variable is not defined for " << connectivity_name << "." << std::endl;
		}
	      }
	    }
	  }

	  QAction *aCopyObjectName = context_menu_q4.addAction("Copy Object Name", this, SLOT(copyClipboard()));
	  aCopyObjectName->setData(qvList);
	  context_menu_q4.addSeparator();
	  if (conn_type == kModuleItem) {
	    QAction *aModuleConfiguratio = context_menu_q4.addAction("Show Module Configuration ", this, SLOT(showConfiguration()));
	    aModuleConfiguratio->setData(qvList);
	    QAction *aTDACMap = context_menu_q4.addAction("Show TDAC Map ", this, SLOT(showHistogramTdac()));
	    aTDACMap->setData(qvList);
	    QAction *aFDACMap = context_menu_q4.addAction("Show FDAC Map ", this, SLOT(showHistogramFdac()));
	    aFDACMap->setData(qvList);
	    QAction *aObjectData = context_menu_q4.addAction("Show Module Data ", this, SLOT(showObjectData()));
	    aObjectData->setData(qvList);
	    QAction *aModDisable = context_menu_q4.addAction("Auto disable Module ", this, SLOT(autoDisable()));
	    aModDisable->setData(qvList);
	    QAction *aModReconfig = context_menu_q4.addAction("QS reconfigure Module ", this, SLOT(QSReconfig()));
	    aModReconfig->setData(qvList);
	  }
	  else if (conn_type == kPp0Item) {
	    QAction *aBocConfiguration = context_menu_q4.addAction("Show BOC Configuration ", this, SLOT(showBocConfiguration()));
	    aBocConfiguration->setData(qvList);
	    QAction *aObjectData = context_menu_q4.addAction("Show PP0 Data ", this, SLOT(showObjectData()));
	    aObjectData->setData(qvList);
	    QAction *aPP0Disable = context_menu_q4.addAction("Auto disable PP0 ", this, SLOT(autoDisable()));
	    aPP0Disable->setData(qvList);
	  }
	  else if (conn_type == kRodItem) {
	    QAction *aModuleGroupConfiguration = context_menu_q4.addAction("Show ModuleGroup Configuration ", this, SLOT(showConfiguration()));
	    aModuleGroupConfiguration->setData(qvList);
	    QAction *aRodConfiguration = context_menu_q4.addAction("Show ROD Configuration ", this, SLOT(showRodConfiguration()));
	    aRodConfiguration->setData(qvList);
	    QAction *aBocConfiguration = context_menu_q4.addAction("Show BOC Configuration ", this, SLOT(showBocConfiguration()));
	    aBocConfiguration->setData(qvList);
	    QAction *aLinkMap = context_menu_q4.addAction("Show Link Map ", this, SLOT(showLinkConfiguration()));
	    aLinkMap->setData(qvList);
	    QAction *aObjectData = context_menu_q4.addAction("Show ROD Data ", this, SLOT(showObjectData()));
	    aObjectData->setData(qvList);
	    QAction *aRodBuffer = context_menu_q4.addAction("Show Most Recent ROD Buffer ", this, SLOT(showRodBuffer()));
	    aRodBuffer->setData(qvList);
	    QAction *aRodRecov = context_menu_q4.addAction("Recover this ROD ", this, SLOT(recoverRod()));
	    aRodRecov->setData(qvList);
	    QAction *aRodDisable = context_menu_q4.addAction("AutoDisable ROD ", this, SLOT(autoDisable()));
	    aRodDisable->setData(qvList);
	  }
	  else if (conn_type == kTimItem) {
	    QAction *aTimConfiguration = context_menu_q4.addAction("Show Tim Configuration ", this, SLOT(showTimConfiguration()));
	    aTimConfiguration->setData(qvList);
	  }
	  if (m_currentVar.hasHistory()) {
	    QAction *aShowHistory = context_menu_q4.addAction("Show Variable History ", this, SLOT(showHistory()));
	    aShowHistory->setData(qvList);
	  }
	  
	  if (conn_type < kNHistogramConnItemTypes) {
	    if (!m_histoVisualiser[ conn_type ].isValid()) {
	      updateHistogramList(conn_type);
	      if (conn_type == kPp0Item) {
		updateHistogramList(kModuleItem);
	      }
	    }
	    
	    if (m_histoVisualiser[conn_type].histogramNameList().empty() && (conn_type != kPp0Item || m_histoVisualiser[kModuleItem].histogramNameList().empty())) {
	      
	      QAction *aHistogram = context_menu_q4.addAction("Show Histogram", this, SLOT(signalShowHistogram() ) );
	      aHistogram->setData(qvList);
	      aHistogram->setEnabled(false);	  
	    }
	    else {
	      QMenu *histogram_menu_q4=new QMenu("Show Histogram",&context_menu_q4);
	      context_menu_q4.addMenu(histogram_menu_q4);
	      QAction *aHistogram = histogram_menu_q4->addAction("Show Default Histogram", this, SLOT(signalShowHistogram() ) );
	      aHistogram->setData(qvList);
	      
	      histogram_menu_q4->addSeparator();
	      
	      unsigned int histo_id=0;
	      //unsigned int histo_id_offset=0;
	      EConnItemType conn_type_list[2];
	      unsigned int n_conn_types;
	      conn_type_list[0]=conn_type;
	      
	      if (conn_type == kPp0Item ) {
	 	conn_type_list[1]=kModuleItem;
		n_conn_types = 2;
	      }
	      else {
		n_conn_types = 1;
	      }
	      
	      std::vector<QAction*> aVisualiser;
	      std::vector<QAction*> aHistogramVec;
	      std::vector<QMenu*> visualiser_menu_q4_vector;
	      
	      for (unsigned int conn_type_i=0; conn_type_i < n_conn_types; conn_type_i++) {
		EConnItemType a_conn_type=conn_type_list[conn_type_i];
		
		VisualiserListRef_t visualiser_list = m_histoVisualiser[ a_conn_type ].scanVisualiserList();
		for (std::vector<std::string>::const_iterator histo_name_iter = m_histoVisualiser[a_conn_type].histogramNameList().begin();
		     histo_name_iter != m_histoVisualiser[a_conn_type].histogramNameList().end();
		     histo_name_iter++,histo_id++) {
		  unsigned int histo_id_offset=0;
		  if (histo_id>=m_nScanHistograms[a_conn_type]) {
		    visualiser_list = m_histoVisualiser[a_conn_type].analysisVisualiserList();
		    histo_id_offset = m_nScanHistograms[a_conn_type];
		  }
		  if (visualiser_list.visualiser(histo_id-histo_id_offset).get()) { 
		    QMenu *visualiser_menu_q4=new QMenu(histo_name_iter->c_str(),histogram_menu_q4);
		    
		    visualiser_menu_q4_vector.push_back(visualiser_menu_q4);
		    histogram_menu_q4->addMenu(visualiser_menu_q4_vector.back());      
		    
		    std::shared_ptr<PixA::IVisualiser> a_visualiser( visualiser_list.visualiser(histo_id-histo_id_offset) );
		    //unsigned int choice_counter = m_histo_id_list.size();
		    unsigned int start_counter = m_histo_id_list.size();
		    for(unsigned int option_i=0; option_i< a_visualiser->nOptions(); option_i++) {
		      unsigned int counter =  m_histo_id_list.size();
		      m_histo_id_list.push_back(VisualisationChoice_t(a_conn_type, histo_id,option_i));
		      
		      QVariant qCounter(counter + kHistogram);
		      QVariant qCounter2(counter);
		      QList <QVariant> qvisu_List;
		      qvisu_List.append(qConnType);
		      qvisu_List.append(qConnName);
		      qvisu_List.append(qCounter);
		      qvisu_List.append(qCounter2);
		      
		      aVisualiser.push_back( visualiser_menu_q4_vector.back()->addAction(a_visualiser->optionName(option_i).c_str(), this, SLOT(CheckCases() ) ) );
		      //aVisualiser.at(option_i)->setData(qvisu_List);
		      aVisualiser.back()->setData(qvisu_List);	
		      qvisu_List.clear();
		      
		      
		      if (counter-start_counter == m_lastVisualisationChoice.visualiserOption() ) {
			aVisualiser.at(option_i)->setChecked(true);//now QAction instead of PopupMenu
			//choice_counter = counter;
		      }
		    }
		    
		  }
		}
	      }
	      if (m_histo_id_list.size()>0) {
		//QAction *aHistogram_id = context_menu_q4.addAction("Show Histogram",histogram_menu_q4, SLOT(signalShowHistogram() ) );
		//aHistogram_id->setData(qvList);
	      }
	      else {
		delete histogram_menu_q4;
		/*QAction *aHistogram_id=*/ 
                (void)context_menu_q4.addAction("Show Histogram", this, SLOT(signalShowHistogram() ) );
		aHistogram->setData(qvList);
		aHistogram->setEnabled(false);
	      }
	    }
	    //m_histo_id_list.clear();
	
	  }
	  break;
	 }//from case list
	 default: {
	   assert(conn_type < kNHistogramConnItemTypes );
	   std::cerr << "ERROR [CalibrationConsoleContextMenu::showMenu] Connectivity object type not in the allowed range : " 
		     << conn_type << " < " << kNHistogramConnItemTypes
		     << std::endl;
	   break;
	 }
	} // end switch
	
	context_menu_q4.exec(QCursor::pos());
	return;
      
      }
      catch (CalibrationData::MissingConnObject &err) {
	std::cout << "INFO [CalibrationConsoleContextMenu::showMenu]  exception :"  << err.what() << std::endl;
	return;
      }
    
      // No connectivity name 
      // menu for selected stuff
      //  - enable / disable
      
      // Module / PP0  connectivity name
      // - enable / disable
      // - histogram list
      // - configuration
      // - variables
      
      // ROD
      // enable / disable
      // ROD - configuration
      // BOC - configuration 
      // TIM - configuration ?
      // variables
    }
  }

  void CalibrationConsoleContextMenu::dataSelectionChanged(const DataSelection &data_selection) 
  {
    //    std::cout << "INFO [CalibrationConsoleContextMenu::dataSelectionChanged] " << std::endl;
    m_measurementType = data_selection.type();

    SerialNumber_t serial_number = data_selection.serialNumber();

    SerialNumber_t scan_serial_number = m_calibrationData->scanSerialNumber( m_measurementType, serial_number );

    if (scan_serial_number != m_lastScanSerialNumber || serial_number != m_currentSerialNumber) {
      for (unsigned int conn_type_i=0; conn_type_i<kNHistogramConnItemTypes; conn_type_i++) {
	m_histoVisualiser[conn_type_i].invalidate();
      }
    }
    m_currentSerialNumber = serial_number;
    m_currentVar = data_selection.varId();
//     if (m_currentVar.isValid()) {
//       std::cout << "INFO [CalibrationConsoleContextMenu::dataSelectionChanged] new var =" << m_currentVar.id() 
// 		<< " (" << VarDefList::instance()->varName(m_currentVar.id() ) << ")"
// 		<< " " << (m_currentVar.hasHistory()  ? "has history" : "")
// 		<< std::endl;
//     }
  }

  void CalibrationConsoleContextMenu::updateHistogramList(EConnItemType conn_type_i)
  {
    //    assert(conn_type_i<kNConnItemTypes);
    // actually this method should only be called for items with histograms
    assert(conn_type_i<kNHistogramConnItemTypes);
    std::vector<std::string> &the_name_list = m_histoVisualiser[conn_type_i].histogramNameList();

    SerialNumber_t serial_number = m_currentSerialNumber;
    SerialNumber_t scan_serial_number = m_calibrationData->scanSerialNumber( m_measurementType, serial_number );

    the_name_list.clear();
    m_nScanHistograms[conn_type_i]=0;
    const std::shared_ptr< std::vector< std::string > > scan_histo_name_list_ptr = m_histogramCache->histogramList( kScan, scan_serial_number, conn_type_i);
    const std::vector< std::string > &scan_histo_name_list( *scan_histo_name_list_ptr );
    for( std::vector< std::string >::const_iterator histo_name_iter = scan_histo_name_list.begin();
	 histo_name_iter != scan_histo_name_list.end();
	 histo_name_iter++) {
	the_name_list.push_back( *histo_name_iter );
    }
    if (scan_histo_name_list.size()>0) {
      m_histoVisualiser[conn_type_i].setScanVisualiserList( m_visualiserList->createVisualiserList(kScan,
												   scan_serial_number,
												   conn_type_i,
												   scan_histo_name_list) );
    }

    m_nScanHistograms[conn_type_i] = the_name_list.size();
    m_lastScanSerialNumber = scan_serial_number;

    if (m_measurementType == kAnalysis) {
      const std::shared_ptr< std::vector< std::string > > analysis_histo_name_list_ptr = m_histogramCache->histogramList( kAnalysis, serial_number, conn_type_i );
      const std::vector< std::string > &analysis_histo_name_list( *analysis_histo_name_list_ptr );
      if (analysis_histo_name_list.size()>0) {
	m_histoVisualiser[conn_type_i].setAnalysisVisualiserList( m_visualiserList->createVisualiserList(kAnalysis,
													 serial_number,
													 conn_type_i,
													 analysis_histo_name_list) );
      }
      for( std::vector< std::string >::const_iterator histo_name_iter = analysis_histo_name_list.begin();
	   histo_name_iter != analysis_histo_name_list.end();
	   histo_name_iter++) {
	if (std::find(scan_histo_name_list.begin(),scan_histo_name_list.end(),*histo_name_iter) != scan_histo_name_list.end() ) {
	  the_name_list.push_back( std::string("A:")+*histo_name_iter );
	}
	else {
	  the_name_list.push_back( *histo_name_iter );
	}
      }
    }

  }
  
  void CalibrationConsoleContextMenu::signalShowHistogram(EConnItemType conn_type, const std::string &connectivity_name)
  {
    assert(conn_type < kNConnItemTypes);
    if (conn_type>=kNHistogramConnItemTypes) return;

    //@todo treat Pp0s properly : combine Pp0 histograms and module histograms.
    EConnItemType histo_conn_type;
    if (m_lastVisualisationChoice.connItemType() <kNHistogramConnItemTypes) {
      histo_conn_type = m_lastVisualisationChoice.connItemType();
    }
    else {
      histo_conn_type = conn_type;
    }
    if (m_histoVisualiser[histo_conn_type].isValid()) {
      updateHistogramList(histo_conn_type);
    }
    const std::vector<std::string> &a_histogram_name_list = m_histoVisualiser[histo_conn_type].histogramNameList();

    if (m_lastVisualisationChoice.histoId()  >= a_histogram_name_list.size()) {
      emit m_signals->showHistogram(conn_type,
				    connectivity_name,
				    m_measurementType,
				    m_currentSerialNumber,
				    "",
				    m_lastVisualisationChoice.visualiserOption() );
    }
    else if (m_lastVisualisationChoice.histoId() < m_nScanHistograms[histo_conn_type]) {
      emit m_signals->showHistogram(conn_type,
				    connectivity_name,
				    kScan,
				    m_lastScanSerialNumber,
				    a_histogram_name_list[m_lastVisualisationChoice.histoId()],
				    m_lastVisualisationChoice.visualiserOption() );
    }
    else {
      if (a_histogram_name_list[m_lastVisualisationChoice.histoId()].compare(0,2,"A:")==0) {
	emit m_signals->showHistogram(conn_type,
				      connectivity_name,
				      kAnalysis,
				      m_currentSerialNumber,
				      std::string(a_histogram_name_list[m_lastVisualisationChoice.histoId()],0,2), 
				      m_lastVisualisationChoice.visualiserOption() );
      }
      else {
	emit m_signals->showHistogram(conn_type,
				      connectivity_name,
				      kAnalysis,
				      m_currentSerialNumber,
				      a_histogram_name_list[m_lastVisualisationChoice.histoId()], 
				      m_lastVisualisationChoice.visualiserOption() );
      }
    }
  }

  void CalibrationConsoleContextMenu::printModules(){
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(!sender) return;
    emit m_signals->showModuleListView();
  }

  void CalibrationConsoleContextMenu::toggleEnableState(){
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(!sender) return;
    QVariant var = sender->data();
    QList<QVariant> varList = var.toList();
    EConnItemType conn_type = (EConnItemType)varList[0].toInt();
    std::string connectivity_name = varList[1].toString().toLatin1().data();
    emit m_signals->toggleEnableState(conn_type, connectivity_name);
  }
  void CalibrationConsoleContextMenu::copyClipboard(){
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(!sender) return;
    QVariant var = sender->data();
    QList<QVariant> varList = var.toList();
    //EConnItemType conn_type = (EConnItemType)varList[0].toInt();
    std::string connectivity_name = varList[1].toString().toLatin1().data();
    QClipboard *cb = QApplication::clipboard();
    cb->setText( QString::fromStdString(connectivity_name), QClipboard::Selection );
  }
  void CalibrationConsoleContextMenu::showConfiguration(){
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(!sender) return;
    QVariant var = sender->data();
    QList<QVariant> varList = var.toList();
    EConnItemType conn_type = (EConnItemType)varList[0].toInt();
    std::string connectivity_name = varList[1].toString().toLatin1().data();
    emit m_signals->showConfiguration(conn_type, connectivity_name, m_measurementType, m_currentSerialNumber);
  }
  void CalibrationConsoleContextMenu::showHistogramTdac(){
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(!sender) return;
    QVariant var = sender->data();
    QList<QVariant> varList = var.toList();
    EConnItemType conn_type = (EConnItemType)varList[0].toInt();
    std::string connectivity_name = varList[1].toString().toLatin1().data();
    emit m_signals->showHistogram(conn_type,connectivity_name,m_measurementType,m_currentSerialNumber,"TDAC", 0);
  }
  void CalibrationConsoleContextMenu::showHistogramFdac(){
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(!sender) return;
    QVariant var = sender->data();
    QList<QVariant> varList = var.toList();
    EConnItemType conn_type = (EConnItemType)varList[0].toInt();
    std::string connectivity_name = varList[1].toString().toLatin1().data();
    emit m_signals->showHistogram(conn_type,connectivity_name,m_measurementType,m_currentSerialNumber,"FDAC", 0);
  }
  void CalibrationConsoleContextMenu::showRodConfiguration(){
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(!sender) return;
    QVariant var = sender->data();
    QList<QVariant> varList = var.toList();
    EConnItemType conn_type = (EConnItemType)varList[0].toInt();
    std::string connectivity_name = varList[1].toString().toLatin1().data();
    emit m_signals->showRodConfiguration(conn_type, connectivity_name, m_measurementType, m_currentSerialNumber);
  }
  void CalibrationConsoleContextMenu::showBocConfiguration(){
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(!sender) return;
    QVariant var = sender->data();
    QList<QVariant> varList = var.toList();
    EConnItemType conn_type = (EConnItemType)varList[0].toInt();
    std::string connectivity_name = varList[1].toString().toLatin1().data();
    emit m_signals->showBocConfiguration(conn_type, connectivity_name, m_measurementType, m_currentSerialNumber);
  }
  void CalibrationConsoleContextMenu::showRodBuffer(){
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(!sender) return;
    QVariant var = sender->data();
    QList<QVariant> varList = var.toList();
    EConnItemType conn_type = (EConnItemType)varList[0].toInt();
    std::string connectivity_name = varList[1].toString().toLatin1().data();
    emit m_signals->showRodBuffer(conn_type, connectivity_name, m_measurementType, m_currentSerialNumber);
  }
  void CalibrationConsoleContextMenu::showTimConfiguration(){
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(!sender) return;
    QVariant var = sender->data();
    QList<QVariant> varList = var.toList();
    EConnItemType conn_type = (EConnItemType)varList[0].toInt();
    std::string connectivity_name = varList[1].toString().toLatin1().data();
    emit m_signals->showTimConfiguration(conn_type, connectivity_name, m_measurementType, m_currentSerialNumber);
  }
  void CalibrationConsoleContextMenu::showLinkConfiguration(){
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(!sender) return;
    QVariant var = sender->data();
    QList<QVariant> varList = var.toList();
    EConnItemType conn_type = (EConnItemType)varList[0].toInt();
    std::string connectivity_name = varList[1].toString().toLatin1().data();
    emit m_signals->showLinkConfiguration(conn_type, connectivity_name, m_measurementType, m_currentSerialNumber);
  }
  void CalibrationConsoleContextMenu::showObjectData(){
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(!sender) return;
    QVariant var = sender->data();
    QList<QVariant> varList = var.toList();
    EConnItemType conn_type = (EConnItemType)varList[0].toInt();
    std::string connectivity_name = varList[1].toString().toLatin1().data();
    emit m_signals->showObjectData(conn_type, connectivity_name, m_measurementType, m_currentSerialNumber);
  }
  void CalibrationConsoleContextMenu::showHistory(){
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(!sender) return;
    QVariant var = sender->data();
    QList<QVariant> varList = var.toList();
    EConnItemType conn_type = (EConnItemType)varList[0].toInt();
    std::string connectivity_name = varList[1].toString().toLatin1().data();
    emit m_signals->showHistory(conn_type, connectivity_name, m_measurementType, m_currentSerialNumber, m_currentVar.id());
  }
  void CalibrationConsoleContextMenu::autoDisable(){
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(!sender) return;
    QVariant var = sender->data();
    QList<QVariant> varList = var.toList();
    EConnItemType conn_type = (EConnItemType)varList[0].toInt();
    std::string connectivity_name = varList[1].toString().toLatin1().data();
    emit m_signals->autoDisable(conn_type, connectivity_name, m_measurementType, m_currentSerialNumber,m_partitionName);
  }
  void CalibrationConsoleContextMenu::recoverRod(){
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(!sender) return;
    QVariant var = sender->data();
    QList<QVariant> varList = var.toList();
    EConnItemType conn_type = (EConnItemType)varList[0].toInt();
    std::string connectivity_name = varList[1].toString().toLatin1().data();
    emit m_signals->recoverRod(conn_type, connectivity_name, m_measurementType, m_currentSerialNumber,m_partitionName);
  }
  void CalibrationConsoleContextMenu::QSReconfig(){
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(!sender) return;
    QVariant var = sender->data();
    QList<QVariant> varList = var.toList();
    EConnItemType conn_type = (EConnItemType)varList[0].toInt();
    std::string connectivity_name = varList[1].toString().toLatin1().data();
    emit m_signals->QSReconfig(conn_type, connectivity_name, m_measurementType, m_currentSerialNumber,m_partitionName);
  }
  void CalibrationConsoleContextMenu::signalShowHistogram(){
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(!sender) return;
    QVariant var = sender->data();
    QList<QVariant> varList = var.toList();
    EConnItemType conn_type = (EConnItemType)varList[0].toInt();
    std::string connectivity_name = varList[1].toString().toLatin1().data();
    m_lastVisualisationChoice.reset();
    signalShowHistogram(conn_type, connectivity_name);
  }
    
  void CalibrationConsoleContextMenu::CheckCases(){
    QAction *sender = dynamic_cast<QAction*>(QObject::sender());
    if(!sender) return;
    QVariant var = sender->data();
    QList<QVariant> varList = var.toList();
    EConnItemType conn_type = (EConnItemType)varList[0].toInt();
    std::string connectivity_name = varList[1].toString().toLatin1().data();
    EMenuId menu_type = (EMenuId)varList[2].toInt();
    int counter = varList[3].toInt();
    
    //Check different possible EMenuIds:
    if (menu_type == kEnable) {
      emit m_signals->toggleEnableState(conn_type, connectivity_name);}
    else if (menu_type == kCopyObjectName) {
      QClipboard *cb = QApplication::clipboard();
      cb->setText( QString::fromStdString(connectivity_name), QClipboard::Selection );}
    else if (menu_type == kModuleConfiguration || menu_type == kModuleGroupConfiguration) {
      emit m_signals->showConfiguration(conn_type, connectivity_name, m_measurementType, m_currentSerialNumber);}
    else if (menu_type == kTDACMap) {
      emit m_signals->showHistogram(conn_type,connectivity_name,m_measurementType,m_currentSerialNumber,"TDAC", 0);}
    else if (menu_type == kFDACMap) {
      emit m_signals->showHistogram(conn_type,connectivity_name,m_measurementType,m_currentSerialNumber,"FDAC", 0);}
    else if (menu_type == kRodConfiguration) {
      emit m_signals->showRodConfiguration(conn_type, connectivity_name, m_measurementType, m_currentSerialNumber);}
    else if (menu_type == kBocConfiguration) {
      emit m_signals->showBocConfiguration(conn_type, connectivity_name, m_measurementType, m_currentSerialNumber);}
    else if (menu_type == kRodBuffer) {
      emit m_signals->showRodBuffer(conn_type, connectivity_name, m_measurementType, m_currentSerialNumber);}
    else if (menu_type == kTimConfiguration) {
      emit m_signals->showTimConfiguration(conn_type, connectivity_name, m_measurementType, m_currentSerialNumber);}
    else  if (menu_type == kLinkMap) {
      emit m_signals->showLinkConfiguration(conn_type, connectivity_name, m_measurementType, m_currentSerialNumber);}
    else if (menu_type == kObjectData) {
      emit m_signals->showObjectData(conn_type, connectivity_name, m_measurementType, m_currentSerialNumber);}
    else if (menu_type == kShowHistory) {
      emit m_signals->showHistory(conn_type, connectivity_name, m_measurementType, m_currentSerialNumber, m_currentVar.id()); }
    else if (menu_type == kAutoDis) {
      emit m_signals->autoDisable(conn_type, connectivity_name, m_measurementType, m_currentSerialNumber,m_partitionName);}
    else if (menu_type == kRodRecover) {
      emit m_signals->recoverRod(conn_type, connectivity_name, m_measurementType, m_currentSerialNumber,m_partitionName);}
    else if (menu_type == kQSReconfig) {
      emit m_signals->QSReconfig(conn_type, connectivity_name, m_measurementType, m_currentSerialNumber,m_partitionName);}
    else if (menu_type == kHistogramPopup) {
      m_lastVisualisationChoice.reset();
      signalShowHistogram(conn_type, connectivity_name); }
    else if (menu_type >= kHistogram) {
            
    	//if (static_cast<unsigned int>(counter)>=kHistogram && static_cast<unsigned int>(counter)<=kHistogram+m_histo_id_list.size() ) {
	if (static_cast<unsigned int>(counter)< m_histo_id_list.size() ) { 
	  
	      unsigned id = counter;//-kHistogram;
	      assert (id < m_histo_id_list.size() );
	      
	      m_lastVisualisationChoice = m_histo_id_list[id];
	      
	      //	  m_histoConnType = histo_id_list[id].connItemType();
	      //	  m_lastHistoChoice = histo_id_list[id].histoId();
	      //	  m_lastVisualiserChoice = id - histo_id_list[id].visualiserOption();
	      
	      // 	  std::cout << "INFO [CalibrationConsoleContextMenu::showMenu]" << static_cast<void *>(this)  << " - show histogram : " 
	      // 		    << m_histogramNameList[m_lastHistoChoice] << ". " << std::endl;
	      // 	  if (!visualiser_list) {
	      // 	    std::cout << "ERRRO [CalibrationConsoleContextMenu::showMenu]" << static_cast<void *>(this)  << " no visualiser list for serial number " 
	      // 		      << m_currentSerialNumber << ". " << std::endl;
	      // 	  }
	      // 	  else {
	      // 	    visualise(type,connectivity_name,m_lastHistoChoice, m_lastVisualiserChoice);
	      // 	  }
	      signalShowHistogram(conn_type, connectivity_name);
    
      }
      
    } else {
    std::cout << "WARNING [CalibrationConsoleContextMenu::CheckCase] unrecognized menu type " << menu_type << std::endl;
    }
  }

}

