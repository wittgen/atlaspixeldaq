#ifndef _PixCon_ObjectDataView_H_
#define _PixCon_ObjectDataView_H_

#include "ui_ObjectDataViewBase.h"

#include <CalibrationData.h>
#include <ConfigWrapper/Regex_t.h>
#include <QTreeWidgetItem>
//#include <q3listview.h>
#include <set>

namespace PixCon {
  class CalibrationDataManager;
  class CalibrationData;
  class CalibrationDataPalette;
  class CategoryList;
  class ObjectDataItem;
  class SerialNumberDataItem;

  class ObjectDataView : public QDialog, Ui_ObjectDataViewBase
  {
    friend class ObjectDataItem;
    friend class SerialNumberDataItem;

    Q_OBJECT

  public:
    ObjectDataView( const std::string &conn_name,
		    PixCon::EConnItemType conn_item_type,
		    const std::shared_ptr<CalibrationDataManager> &calibration_data_manager,
		    const std::shared_ptr<PixCon::CategoryList> &categories,
		    const std::shared_ptr<PixCon::CalibrationDataPalette> &palette,
		    QWidget* parent = 0);
    ~ObjectDataView();

  public slots:
    void filter();
    void clearFilter();

  public:
  protected:

    SerialNumberDataItem* makeSerialNumberItem(/*Q3ListViewItem*/QTreeWidgetItem *parent_item,
					       PixCon::EMeasurementType measurement_type,
					       PixCon::SerialNumber_t serial_number);
    void getVariables();
    void fillListView(/*Q3ListViewItem*/QTreeWidgetItem *parent_item,
		      const std::string &filter_string);

    void fillObjectData(const std::set<std::string> &conn_name,
			SerialNumber_t serial_number,
			EMeasurementType measurement_type,
			const std::set<std::string> &var_list,
            //Q3ListViewItem* parent_item,
            QTreeWidgetItem* parent_item,
			const std::string &filter_string);

    void initConnNameList(const std::string &conn_name,
			  PixCon::EConnItemType conn_item_type);

  private:

    PixCon::EConnItemType m_connType;
    std::shared_ptr<CalibrationDataManager> m_calibrationDataManager;
    std::shared_ptr<PixCon::CategoryList>    m_categories;
    std::shared_ptr<PixCon::CalibrationDataPalette> m_palette;

    std::set<std::string> m_connNameList;
    std::vector< std::map< SerialNumber_t, std::set<std::string> > > m_varVector;
    bool m_filledRodModuleData;
    //QListViewItem *m_rod_item;
    ObjectDataItem *m_rod_item;
  };
}
#endif
