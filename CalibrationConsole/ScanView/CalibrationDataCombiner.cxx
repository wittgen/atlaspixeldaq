#include "CalibrationDataCombiner.h"
#include <CalibrationDataManager.h>
#include <ConfigWrapper/ConnectivityUtil.h>

namespace PixCon {

  CalibrationDataCombiner::CalibrationDataCombiner(const std::shared_ptr<CalibrationDataManager> &calibration_data,
						   SerialNumber_t dest_analysis_serial_number)
    : m_calibrationData(calibration_data),
      m_analysisSerialNumber(dest_analysis_serial_number)
  {
  }

  CalibrationDataCombiner::~CalibrationDataCombiner() {}

  void CalibrationDataCombiner::dumpReport()
  {
    try {
      PixA::ConnectivityRef conn( m_calibrationData->connectivity(kAnalysis,
								  m_analysisSerialNumber) );

      for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
	   crate_iter != conn.crates().end();
	   ++crate_iter) {

	PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
	PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
	for (PixA::RodList::const_iterator rod_iter = rod_begin;
	     rod_iter != rod_end;
	     ++rod_iter) {

	  reportValue(PixA::connectivityName(*rod_iter));

	  PixA::Pp0List::const_iterator pp0_begin = PixA::pp0Begin(rod_iter);
	  PixA::Pp0List::const_iterator pp0_end = PixA::pp0End(rod_iter);
	  for (PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
	       pp0_iter != pp0_end;
	       ++pp0_iter) {

	    //	  updateValues(calibration_data, dest_serial_number, src_measurement_type, src_serial_number, PixA::connectivityName(*pp0_iter), var_def_list);

	    PixA::ModuleList::const_iterator module_begin = PixA::modulesBegin(pp0_iter);
	    PixA::ModuleList::const_iterator module_end = PixA::modulesEnd(pp0_iter);
	    for (PixA::ModuleList::const_iterator module_iter = module_begin;
		 module_iter != module_end;
		 ++module_iter) {

	      reportValue(PixA::connectivityName(*module_iter));

	    }
	  }
	}
      }
    }
    catch(...) {
    }

  }

  void CalibrationDataCombiner::reportValue(const std::string &conn_name) {

    bool has_entries=false;
    try {
      CalibrationData::ConnObjDataList conn_obj_data ( m_calibrationData->calibrationData().getConnObjectData(conn_name) );

      for (std::map<std::string, std::map<std::string, std::pair<EMeasurementType, SerialNumber_t> > >::const_iterator var_iter = m_log.begin();
	   var_iter != m_log.end();
	   var_iter++) {

	std::map<std::string, std::pair<EMeasurementType, SerialNumber_t> >::const_iterator conn_iter = var_iter->second.find(conn_name);
	if (conn_iter != var_iter->second.end()) {
	  VarDef_t a_var_def = VarDefList::instance()->getVarDef(var_iter->first);

	  try {
	    std::string value_string = makeValueString(conn_obj_data, conn_iter->second.first, conn_iter->second.second, a_var_def);
	    if (!has_entries) {
	      const PixCon::CalibrationData::EnableState_t &dest_conn_obj_enable_state(conn_obj_data.enableState(kAnalysis, m_analysisSerialNumber));
	      char disable_char_begin = ' ';
	      char disable_char_end = ' ';
	      if (!dest_conn_obj_enable_state.enabled()) {
		disable_char_begin='[';
		disable_char_end=']';
	      }
	      std::cout << "INFO [CalibrationDataCombiner::reportValue] " << disable_char_begin << conn_name << disable_char_end << " : "
			<< makeSerialNumberString(conn_iter->second.first, conn_iter->second.second) << " -> "
			<< var_iter->first << " = " << value_string << std::endl;
	    }

	  }
	  catch (CalibrationData::MissingValue &){
	  }

	}
      }
    }
    catch(...) {
    }
  }

  void CalibrationDataCombiner::_addMeasurement(EMeasurementType src_measurement_type,
						SerialNumber_t src_serial_number,
						EValueType type_restriction)
  {
    PixA::ConnectivityRef conn( m_calibrationData->connectivity(src_measurement_type,
								src_serial_number) );
    if (!conn) return;

    std::set<std::string> var_list;
    m_calibrationData->calibrationData().varList(src_measurement_type, src_serial_number, var_list);
    std::vector<VarDef_t> var_def_list;

    for (std::set<std::string>::const_iterator var_iter = var_list.begin();
	 var_iter != var_list.end();
	 var_iter++) {
      if (var_iter->empty()) continue;
      if ((*var_iter)[0]=='_' && var_iter->rfind("_t") != std::string::npos) continue;

      VarDef_t a_var_def = VarDefList::instance()->getVarDef(*var_iter);
      if (a_var_def.isValid() && a_var_def.valueType()== type_restriction ) {
	var_def_list.push_back( a_var_def );
      }
    }

    for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
	 crate_iter != conn.crates().end();
	 ++crate_iter) {

      PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
      PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
      for (PixA::RodList::const_iterator rod_iter = rod_begin;
	   rod_iter != rod_end;
	   ++rod_iter) {

	updateValues(src_measurement_type,
		     src_serial_number,
		     kRodItem,
		     PixA::connectivityName(*rod_iter),
		     var_def_list);

	PixA::Pp0List::const_iterator pp0_begin = PixA::pp0Begin(rod_iter);
	PixA::Pp0List::const_iterator pp0_end = PixA::pp0End(rod_iter);
	for (PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
	     pp0_iter != pp0_end;
	     ++pp0_iter) {

	  //	  updateValues(calibration_data, dest_serial_number, src_measurement_type, src_serial_number, PixA::connectivityName(*pp0_iter), var_def_list);

	  PixA::ModuleList::const_iterator module_begin = PixA::modulesBegin(pp0_iter);
	  PixA::ModuleList::const_iterator module_end = PixA::modulesEnd(pp0_iter);
	  for (PixA::ModuleList::const_iterator module_iter = module_begin;
	       module_iter != module_end;
	       ++module_iter) {

	    updateValues(src_measurement_type,
			 src_serial_number,
			 kModuleItem,
			 PixA::connectivityName(*module_iter),
			 var_def_list);

	  }//pp0
	}//rod
      }//crate
    }

  }

}
