#include "HistogramCache.h"
#include "TH1.h"
#include <iostream>

namespace PixCon {

  HistoArray::~HistoArray() {
    m_magic = 0;
    for (std::vector<HistoPtr_t>::iterator histo_iter=m_histo.begin();
	 histo_iter != m_histo.end();
	 histo_iter++) {
      delete *histo_iter;
      *histo_iter=NULL;
    }
    m_histo.clear();

    for (std::vector<HistoArray *>::iterator child_iter=m_child.begin();
	 child_iter != m_child.end();
	 child_iter++) {
      delete *child_iter;
      *child_iter=NULL;
    }
    m_child.clear();
  }


  void HistoArray::addHisto(const Index_t &index, unsigned int level, HistoPtr_t histo_ptr) {
    // paranoia check
    assert(level < index.size());
    // check whether the index looks resonable
    assert(index.size()+1000 > index[level]);

    if (level+1 == index.size()) {

      if (index[level]+1>m_histo.size()) {

	m_histo.resize(index[level]+1,NULL);
      }
      delete m_histo[ index[level] ];
      m_histo[ index[level] ]=histo_ptr;
    }
    else {
      if (index[level]+1>m_child.size()) {
	//      unsigned int old_size=m_child.size();
	m_child.resize(index[level]+1,NULL);
      }
      if (!m_child[ index[level] ]) {
	m_child[ index[level] ]=new HistoArray(m_level+1);
      }
      m_child[ index[level] ]->addHisto(index,level+1, histo_ptr);
    }
  }

  std::shared_ptr< std::vector< std::string > > HistogramCache::s_emptyHistogramNameList( new std::vector<std::string> );
  std::string HistogramCache::s_emptyString;

  HistogramCache::HistogramCache(std::shared_ptr<IHistogramProvider> server) : m_server(server),m_verbose(false) {}

  HistogramCache::~HistogramCache() {}

  unsigned int HistogramCache::histogramIndex(EMeasurementType measurement_type,
					      SerialNumber_t serial_number,
					      EConnItemType conn_item_type,
					      const std::string &histogram_name)
  {
    assert( measurement_type < kNMeasurements );
    assert( conn_item_type < kNConnItemTypes);

    if (conn_item_type>=kNHistogramConnItemTypes) return s_unknownHistogram;

    std::shared_ptr< std::vector<std::string> > name_list_ptr;
    {
      Lock histo_list_list(m_histogramListMutex);
      std::map< SerialNumber_t, ConnItemCollection<NameList_t>  >::const_iterator iter = m_histogramList[measurement_type].find(serial_number);
      if (iter== m_histogramList[measurement_type].end()) {
	std::pair< std::map< SerialNumber_t, ConnItemCollection<NameList_t>  >::iterator, bool> ret
	  = m_histogramList[measurement_type].insert(std::make_pair(serial_number, ConnItemCollection<NameList_t>() ));
	if (!ret.second) return s_unknownHistogram;
	m_server->requestHistogramList( measurement_type, serial_number, conn_item_type, *ret.first->second[conn_item_type].createList() );
	iter = ret.first;
      }
      name_list_ptr = iter->second[conn_item_type].list();
    }

    // now the  name list ptr points to a name list which is guaranteed to exist unmodified for this scope
    unsigned int counter_i=0;
    std::vector<std::string> &a_name_list( *name_list_ptr );
    for (std::vector< std::string >::const_iterator list_iter = a_name_list.begin();
	 list_iter != a_name_list.end();
	 list_iter++, counter_i++) {
      if (*list_iter == histogram_name) return counter_i;
    }
    return s_unknownHistogram;
  }

  const HistoPtr_t HistogramCache::histogram(EMeasurementType measurement_type,
					     SerialNumber_t serial_number,
					     EConnItemType conn_item_type,
					     const std::string &connectivity_name,
					     unsigned int histo_id,
					     const Index_t &index)
  {
    assert( measurement_type < kNMeasurements ) ;
    assert( conn_item_type < kNConnItemTypes );

    if (histo_id == s_unknownHistogram || conn_item_type >=kNHistogramConnItemTypes ) return NULL;

    // first search histogram in cache
    {
      Lock lock(m_cacheMutex);
      Cache_t::iterator serial_iter = m_cache[measurement_type].find(serial_number);
      if ( serial_iter != m_cache[measurement_type].end() ) {
	ConnItemList_t &conn_item_list = serial_iter->second[conn_item_type];
	ConnItemList_t::iterator conn_item_iter = conn_item_list.find(connectivity_name);
	if (conn_item_iter != conn_item_list.end() ) {
	  if ( histo_id < conn_item_iter->second.size() && conn_item_iter->second[histo_id] ) {

	    const TopLevelHistoArray *histo_array = conn_item_iter->second[histo_id].histoArray();
	    if ( histo_array->empty() ) return NULL;

	    if (!histo_array->checkIndex(index)) return NULL;

	    HistoPtr_t histo_ptr = histo_array->histo(index);
	    if (histo_ptr) {
	      conn_item_iter->second[histo_id].use();
	      return histo_ptr;
	    }
	  }
	}
      }
    }

    // get the histogram from the server
    std::unique_ptr<Histo_t> histo_ptr;
    {
      std::shared_ptr< std::vector<std::string > > name_list_ptr;
      {
	Lock histo_list_list(m_histogramListMutex);
	std::map< SerialNumber_t, ConnItemCollection< NameList_t > >::const_iterator histo_name_iter = m_histogramList[measurement_type].find(serial_number);
	if (histo_name_iter == m_histogramList[measurement_type].end())  return NULL;
	name_list_ptr = histo_name_iter->second[conn_item_type].list();
      }

      // the name_list_ptr guarantees that the histogram name list will stay valid for this scope
      std::vector< std::string > &a_name_list( *name_list_ptr);

      // valid histogram index ?
      if (histo_id < a_name_list.size() ) {
	// load histogram from server
	histo_ptr = std::unique_ptr<Histo_t>( m_server->loadHistogram(measurement_type,
								    serial_number,
								    conn_item_type,
								    connectivity_name,
								    a_name_list[histo_id],
								    index) );
      }
    }
    if (!histo_ptr.get()) return NULL;

    Lock lock(m_cacheMutex);
    HistoList_t &histo_list = m_cache[measurement_type][serial_number][conn_item_type][connectivity_name];
    if (histo_id >= histo_list.size()) {
      histo_list.resize(histo_id+1);
    }
    HistoPtr_t temp = histo_ptr.release();
    histo_list[histo_id].createHistoArray()->addHisto(index, temp);
    histo_list[histo_id].use();
    return temp;
  }

  void HistogramCache::numberOfHistograms(EMeasurementType measurement_type,
					  SerialNumber_t serial_number,
					  EConnItemType conn_item_type,
					  const std::string &connectivity_name,
					  unsigned int histo_id,
					  HistoInfo_t &info,
					  bool refresh)
  {
    assert( measurement_type < kNMeasurements ) ;
    assert( conn_item_type < kNConnItemTypes );

    info.reset();
    if (histo_id == s_unknownHistogram || conn_item_type >= kNHistogramConnItemTypes) return;

    // search histo info in the cache
    if (!refresh) {
      Lock lock(m_cacheMutex);

      Cache_t::iterator serial_iter = m_cache[measurement_type].find(serial_number);
      if ( serial_iter != m_cache[measurement_type].end() ) {
	ConnItemList_t &conn_item_list = serial_iter->second[conn_item_type];
	ConnItemList_t::iterator conn_item_iter = conn_item_list.find(connectivity_name);
	if (conn_item_iter != conn_item_list.end() ) {
	  if ( histo_id < conn_item_iter->second.size() && conn_item_iter->second[histo_id] ) {
	    const TopLevelHistoArray *histo_array = conn_item_iter->second[histo_id].histoArray();
	    if (histo_array) {
	      if (histo_array->empty()) return;

	      if (histo_array->hasValidInfo()) {
		info = histo_array->info();
		return;
	      }
	    }
	  }
	}
      }
    }

    // get histogram info from server
    {
      std::shared_ptr< std::vector<std::string > > name_list_ptr;
      {
	Lock histo_list_list(m_histogramListMutex);
	std::map< SerialNumber_t, ConnItemCollection< NameList_t > >::const_iterator histo_name_iter = m_histogramList[measurement_type].find(serial_number);
	if (histo_name_iter == m_histogramList[measurement_type].end())  return;
	name_list_ptr = histo_name_iter->second[conn_item_type].list();
      }

      // the name_list_ptr guarantees that the histogram name list will stay valid for this scope
      std::vector< std::string > &a_name_list( *name_list_ptr);

      // valid histogram index ?
      if (histo_id < a_name_list.size() ) {

	m_server->numberOfHistograms( measurement_type,
				      serial_number,
				      conn_item_type,
				      connectivity_name,
				      a_name_list[histo_id],
				      info );
      }
    }


    Lock lock(m_cacheMutex);
    HistoList_t &histo_list = m_cache[measurement_type][serial_number][conn_item_type][connectivity_name];
    if (histo_id >= histo_list.size()) {
      histo_list.resize(histo_id+1);
    }

    histo_list[histo_id].clear();
    histo_list[histo_id].createHistoArray()->setInfo(info);
  }

  const std::string &HistogramCache::histogramName(EMeasurementType measurement_type, SerialNumber_t serial_number, EConnItemType conn_item_type, unsigned int histo_id)
  {
    assert( measurement_type < kNMeasurements ) ;
    assert( conn_item_type < kNConnItemTypes );
    if (conn_item_type < kNHistogramConnItemTypes) {
      Lock histo_list_lock(m_histogramListMutex);
      std::map< SerialNumber_t, ConnItemCollection<NameList_t> >::iterator serial_iter = m_histogramList[measurement_type].find(serial_number);
      if ( serial_iter != m_histogramList[measurement_type].end()) {

	// the histo_list_lock guarantees that the name list will stay valid
	const std::vector<std::string> &a_name_list( *serial_iter->second[conn_item_type].list() );
	if (a_name_list.size()>histo_id) {
	  return a_name_list[histo_id];
	}
      }
    }
    return s_emptyString;
  }

  std::shared_ptr< std::vector< std::string > > HistogramCache::histogramList( EMeasurementType measurement_type,
										 SerialNumber_t serial_number,
										 EConnItemType conn_item_type)
  {
    assert( measurement_type < kNMeasurements ) ;
    assert( conn_item_type < kNConnItemTypes);

    if (conn_item_type >=kNHistogramConnItemTypes) return s_emptyHistogramNameList;
    Lock histo_list_lock(m_histogramListMutex);
    if (m_verbose) {
      std::cout << "DIAGNOSTIC [HistogramCache::histogramList] Get histogram list for serial number " << serial_number << "."
    	      << std::endl;
    }

    std::map< SerialNumber_t, ConnItemCollection<NameList_t> >::iterator serial_iter = m_histogramList[measurement_type].find(serial_number);
    if ( serial_iter != m_histogramList[measurement_type].end() ) {
      if (serial_iter->second.filled(conn_item_type) ) {
	if (m_verbose) {
	  std::cout << "DIAGNOSTIC [HistogramCache::histogramList] Histogram list was cached for serial number " << serial_number << " size = " 
		    << serial_iter->second[conn_item_type].list()->size() << "."
		    << std::endl;
	}
	return serial_iter->second[conn_item_type].list();
      }
    }
    else {
      std::pair<std::map< SerialNumber_t, ConnItemCollection<NameList_t> >::iterator, bool> ret =
	m_histogramList[measurement_type].insert(std::make_pair(serial_number,ConnItemCollection<NameList_t >()));
      if (!ret.second) {
	if (m_verbose) {
	  std::cout << "ERROR [HistogramCache::histogramList] Failed to add histogram list for serial number " << serial_number << "." << std::endl;
	}
	return s_emptyHistogramNameList;
	//      throw std::runtime_error("ERROR [HistogramCache::histogramList] Failed to add histogram list for a serial number.");
      }
      serial_iter=ret.first;
    }

    if (m_verbose) {
      std::cout << "INFO [HistogramCache::histogramList] No histogram list in cache for serial number " << serial_number << "."  << std::endl;
    }

    // @todo Would it make sense to not lock the histogram list while requesting ?
    std::shared_ptr< std::vector<std::string> > name_list_ptr = serial_iter->second[conn_item_type].createList();
    m_server->requestHistogramList( measurement_type, serial_number, conn_item_type, *name_list_ptr );

    return name_list_ptr;
  }

  std::shared_ptr< std::vector< std::string > > HistogramCache::updateHistogramList( EMeasurementType measurement_type,
										       SerialNumber_t serial_number,
										       EConnItemType conn_item_type,
										       const std::string &rod_name)
  {
    assert( measurement_type < kNMeasurements ) ;
    assert( conn_item_type < kNConnItemTypes);

    if (conn_item_type >=kNHistogramConnItemTypes) return s_emptyHistogramNameList;

    std::vector<std::string> temp_histogram_list;
    m_server->requestHistogramList( measurement_type, serial_number, conn_item_type, temp_histogram_list, rod_name);

    return mergeHistogramLists(measurement_type, serial_number, conn_item_type, temp_histogram_list);

  }


  void HistogramCache::invalidate(EMeasurementType measurement_type,
				  SerialNumber_t serial_number,
				  EConnItemType conn_item_type,
				  const std::string &connectivity_name,
				  unsigned int histo_id)
  {
    assert( measurement_type < kNMeasurements ) ;
    assert( conn_item_type < kNConnItemTypes );

    if (histo_id == s_unknownHistogram || conn_item_type >=kNHistogramConnItemTypes ) return;

    {
      Lock lock(m_cacheMutex);
      Cache_t::iterator serial_iter = m_cache[measurement_type].find(serial_number);
      if ( serial_iter != m_cache[measurement_type].end() ) {
	ConnItemList_t &conn_item_list = serial_iter->second[conn_item_type];
	ConnItemList_t::iterator conn_item_iter = conn_item_list.find(connectivity_name);
	if (conn_item_iter != conn_item_list.end() ) {
	  if ( histo_id < conn_item_iter->second.size())
	    conn_item_iter->second[histo_id].clear();
	}
      }
    }
  }

//   void HistogramCache::setInvalid(EMeasurementType measurement_type, SerialNumber_t serial_number, EConnItemType conn_item_type, unsigned int histo_id)
//   {
//     assert( measurement_type < kNMeasurements ) ;
//     assert( conn_item_type < kNConnItemTypes);

//     if (conn_item_type >=kNHistogramConnItemTypes) return;

//     if (histo_id == s_unknownHistogram || conn_item_type >= kNHistogramConnItemTypes) return;

//     {
//       Lock lock(m_cacheMutex);

//       Cache_t::iterator serial_iter = m_cache[measurement_type].find(serial_number);
//       if ( serial_iter != m_cache[measurement_type].end() ) {
// 	ConnItemList_t &conn_item_list = serial_iter->second[conn_item_type];
// 	ConnItemList_t::iterator conn_item_iter = conn_item_list.find(connectivity_name);
// 	if (conn_item_iter != conn_item_list.end() ) {
// 	  if ( histo_id < conn_item_iter->second.size()) {
// 	    conn_item_iter->second[histo_id].clear();
// 	  }
// 	}
//       }
//     }
//   }

  void HistogramCache::clearCache(EMeasurementType measurement_type, SerialNumber_t serial_number)
  {
    assert( measurement_type < kNMeasurements ) ;
    m_server->clearCache(measurement_type, serial_number);


    {
      Lock lock(m_cacheMutex);
      Cache_t::iterator serial_iter = m_cache[measurement_type].find(serial_number);
      if ( serial_iter != m_cache[measurement_type].end() ) {
	m_cache[measurement_type].erase(serial_iter);
      }
    }

    {
      Lock histo_list_list(m_histogramListMutex);
      std::map< SerialNumber_t, ConnItemCollection<NameList_t> >::iterator serial_iter = m_histogramList[measurement_type].find(serial_number);
      if ( serial_iter != m_histogramList[measurement_type].end() ) {
	m_histogramList[measurement_type].erase(serial_iter);
      }
    }

  }

  void HistogramCache::clearCache()
  {
    Lock lock(m_cacheMutex);
    Lock histo_list_list(m_histogramListMutex);
    for (unsigned int measurement_type_i = 0; measurement_type_i < kNMeasurements; measurement_type_i++) {
      m_cache[measurement_type_i].clear();
      m_histogramList[measurement_type_i].clear();
    }
  }

  class ShallDelete
  {
  public:
    bool operator()(HistogramCacheEntry &cache_entry) const {
      return cache_entry.shallDelete();
    }
  };

  void HistogramCache::clearUnused()
  {
    Lock lock(m_cacheMutex);
    for (unsigned int measurement_type_i = 0; measurement_type_i< kNMeasurements; measurement_type_i++) {

      Cache_t::iterator serial_iter = m_cache[measurement_type_i].begin();
      bool reached_serial_number_list_end = (serial_iter == m_cache[measurement_type_i].end());
      while (!reached_serial_number_list_end) {

	Cache_t::iterator current_serial_iter = serial_iter++;
	reached_serial_number_list_end = (serial_iter == m_cache[measurement_type_i].end());

	bool is_empty=true;

	for (ConnItemCollection<ConnItemList_t>::iterator type_iter=current_serial_iter->second.begin();
	     type_iter != current_serial_iter->second.end();
	     type_iter++) {

	  ConnItemList_t::iterator conn_item_iter = type_iter->begin();
	  bool reached_conn_item_list_end = (conn_item_iter == type_iter->end() );

	  while (!reached_conn_item_list_end) {

	    ConnItemList_t::iterator current_conn_item_iter = conn_item_iter++;
	    reached_conn_item_list_end = (conn_item_iter == type_iter->end() );

	    current_conn_item_iter->second.erase( remove_if(current_conn_item_iter->second.begin(),
							    current_conn_item_iter->second.end(),
							    ShallDelete()),
					  current_conn_item_iter->second.end());

	    if (current_conn_item_iter->second.empty()) {
	      type_iter->erase(current_conn_item_iter);
	    }

	  }
	  if (!type_iter->empty()) {
	    is_empty=false;
	  }
	}
	if (is_empty) {
	  m_cache[measurement_type_i].erase(current_serial_iter);
	}
      }
    }
  }

  std::shared_ptr<std::vector<std::string> > HistogramCache::mergeHistogramLists( EMeasurementType measurement_type,
										    SerialNumber_t serial_number,
										    EConnItemType conn_item_type,
										    const std::vector<std::string> &new_histogram_list)
  {

    Lock histo_list_lock(m_histogramListMutex);

    std::map< SerialNumber_t, ConnItemCollection<NameList_t>  >::iterator serial_iter = m_histogramList[measurement_type].find(serial_number);
    if (serial_iter== m_histogramList[measurement_type].end()) {
      std::pair< std::map< SerialNumber_t, ConnItemCollection<NameList_t>  >::iterator, bool> ret
	= m_histogramList[measurement_type].insert(std::make_pair(serial_number, ConnItemCollection<NameList_t>() ));
      if (!ret.second) return s_emptyHistogramNameList;
      serial_iter = ret.first;
    }

    std::shared_ptr< std::vector<std::string> > &name_list_ptr = serial_iter->second[conn_item_type].createList();
    std::vector<std::string> &a_name_list = *name_list_ptr;
    if (a_name_list.empty()) {
      a_name_list = new_histogram_list;
      return name_list_ptr;
    }
    else {

  
      std::vector<std::string>::iterator old_name_list_end = a_name_list.end();

      for(std::vector<std::string>::const_iterator name_iter = new_histogram_list.begin();
	  name_iter != new_histogram_list.end();
	  name_iter++) {
	std::vector<std::string>::const_iterator old_name_iter = find(a_name_list.begin(),old_name_list_end, *name_iter);
	if (old_name_iter  == a_name_list.end()) {
	  // copy names;
	    if (m_verbose) {
	      std::cout << "INFO [HistogramCache::mergeHistogramLists] add "  << *name_iter << "." << std::endl;
	    }
	    a_name_list.push_back( *name_iter );
	}

      }

    }

    if (m_verbose) {
      std::cout << "INFO [HistogramCache::mergeHistogramLists] new list : ";
      for(std::vector<std::string>::const_iterator name_iter = a_name_list.begin();
	  name_iter != a_name_list.end();
	  name_iter++) {
	std::cout << *name_iter << ", ";
      }
      std::cout << std::endl;
    }
    return name_list_ptr;
  }

  void HistogramCache::reset() {
    clearCache();
    m_server->reset();
  }

}
