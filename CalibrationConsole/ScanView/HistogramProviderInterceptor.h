// Dear emacs, this is -*-c++-*-
#ifndef _HistogramProviderInterceptor_H_
#define _HistogramProviderInterceptor_H_

#include "IHistogramProvider.h"
#include <memory>

namespace PixCon {

  class HistogramProviderInterceptor : public IHistogramProvider
  {
  public:
    HistogramProviderInterceptor(IHistogramProvider *a_server);
    ~HistogramProviderInterceptor();

    HistoPtr_t loadHistogram(EMeasurementType type,
			     SerialNumber_t serial_number,
			     EConnItemType conn_item_type,
			     const std::string &connectivity_name,
			     const std::string &histo_name,
			     const Index_t &index) ;

    void requestHistogramList(EMeasurementType type, 
			      SerialNumber_t serial_number,
			      EConnItemType conn_item_type,
			      std::vector< std::string > &histogram_name_list ) ;

    void requestHistogramList(EMeasurementType type, 
			      SerialNumber_t serial_number,
			      EConnItemType conn_item_type,
			      std::vector< std::string > &histogram_name_list,
			      const std::string &rod_name) ;

    void numberOfHistograms(EMeasurementType type,
			    SerialNumber_t serial_number,
			    EConnItemType conn_item_type,
			    const std::string &connectivity_name,
			    const std::string &histo_name,
			    HistoInfo_t &index) ;

    void clearCache(EMeasurementType measurement_type, SerialNumber_t serial_number);

    void reset();

  private:
    std::unique_ptr<IHistogramProvider> m_server;
  };

}
#endif
