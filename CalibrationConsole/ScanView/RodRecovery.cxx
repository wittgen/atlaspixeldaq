#include <iostream>
#include <fstream>

#include "RodRecovery.h"

namespace PixCon {

  RodRecovery::RodRecovery()
  {
    m_recoveryScriptName = "/det/tdaq/scripts/PixelRecovery.sh";
  }
  
  RodRecovery::~RodRecovery() {}

  bool RodRecovery::initialize()
  {
    std::ifstream recoveryScriptFile(m_recoveryScriptName.c_str());
    bool recoveryScriptExists = (recoveryScriptFile) ? true : false;
    recoveryScriptFile.close();
    return recoveryScriptExists;
  }

  int RodRecovery::execute()
  {
    std::cout << "INFO [RodRecovery::execute] Executing ROD recovery script." << std::endl;
    std::string unixcmd("echo y | . ");
    unixcmd += m_recoveryScriptName;
    std::cout << "INFO [RodRecovery::execute] unixcmd: " << unixcmd.c_str() << std::endl;
    int rvalue = system(unixcmd.c_str());
    return rvalue;
  }

  std::string RodRecovery::getRecoveryScriptName()
  {
    return m_recoveryScriptName;
  }

}
