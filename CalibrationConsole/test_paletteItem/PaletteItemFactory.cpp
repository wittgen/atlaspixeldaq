#include "PaletteItemFactory.h"
#include <cmath>
#include <cassert>
#include <memory>
#include <sstream>
#include <iomanip>
#include <iostream>
#include <QGraphicsScene>
#include <QPainter>
#include <QPolygon>

PaletteItemFactory::PaletteItemFactory(const std::vector< QColor > &colour_list,
				       float min,
				       float max)
  : m_colourList(colour_list),
    m_min(min),
    m_max(max)
{}

void PaletteItemFactory::create(QGraphicsScene *canvas,
				const QRectF &rect,
				std::vector<QGraphicsItem *> &item_list)
{
  float min = m_min;
  float max = m_max;
  std::vector< QColor > &colour_list = m_colourList;

  assert(min <= max);
  if (min==max) {
    if (min==0.) {
      max=1;
    }
    else {
      max = min * 1.05;
    }
  }
  assert( colour_list.size() > 2);

  unsigned int label_font_size=7;
  unsigned int exponent_font_size=6;
  unsigned int ten_font_size=8;
  float margin_fraction = 5e-2;

  QFont label_font;
  label_font.setPointSize(label_font_size);

  QFont exponent_font(label_font);
  exponent_font.setBold(true);
  exponent_font.setPointSize(exponent_font_size);

  QFont ten_font(label_font);
  ten_font.setBold(true);
  ten_font.setPointSize(ten_font_size);



  double max_abs;
  double min_abs;
  if (std::abs(min)< std::abs(max) ) {
    max_abs=std::abs(max);
    min_abs=std::abs(min);
  }
  else {
    max_abs=std::abs(min);
    min_abs=std::abs(max);
  }
  if (min_abs==0.) {
    min_abs = max_abs;
  }

  double range = max - min;
  double dominant_term=0.;
  double unsign = 1.;
  if ( min*max > 0 && range<min_abs*1.e-2) {
    dominant_term=floor(min_abs *100) * 1e-2;
    if (min<0 || max<0) {
      dominant_term *= -1;
      unsign = -1.;
    }
    max -= dominant_term;
    min -= dominant_term;

    if (std::abs(min)< std::abs(max) ) {
      max_abs=std::abs(max);
      min_abs=std::abs(min);
    }
    else {
      max_abs=std::abs(max);
      min_abs=std::abs(min);
    }
    if (min_abs==0.) {
      min_abs = max_abs;
    }

  }


  int exponent = static_cast<unsigned int>(log10( min_abs ));
  double scale = 1./exp(exponent *log(10));
  std::stringstream exponent_string;
  exponent_string << exponent;

  int max_exponent = static_cast<unsigned int>(log10( max_abs ));
  if (exponent>max_exponent) {
    max_exponent=exponent;
  }

  int use_exponent=exponent;
  if (abs(exponent)<12) {
    use_exponent = static_cast<int>((use_exponent-1)/3)*3;
  }
  dominant_term *= exp(-(use_exponent)*log(10));

  double use_scale = exp(-use_exponent * log(10));

  QRectF palette_rect;
  int start_axis=rect.y();
  int end_axis=rect.y();
  if (rect.width()<rect.height()) {
    // horizontal
    int margin=static_cast<int>(rect.width() * margin_fraction);
    int y=rect.y()+margin;
    int x_right=rect.x()+rect.width()-margin;
    int x=x_right;

    std::stringstream exponent_string;
    exponent_string << use_exponent;

    std::unique_ptr<QGraphicsTextItem> exponent_text(canvas->addText(QString::fromStdString(exponent_string.str())));
    exponent_text->setFont(exponent_font);
    QRectF exponent_rect = exponent_text->boundingRect();
    exponent_text->setY(y);
    x -= exponent_rect.width();
    exponent_text->setX(x);
    y+=static_cast<int>(exponent_rect.height()*.5);

    std::unique_ptr<QGraphicsTextItem> ten_text( canvas->addText("x10"));
    ten_text->setFont(ten_font);
    ten_text->setY(y);
    QRectF ten_rect = ten_text->boundingRect();
    x -= ten_rect.width();
    ten_text->setX(x);
    y+=ten_rect.height();

    if (std::abs(dominant_term)>0.) {
      std::stringstream dominant_string;
      dominant_string << std::fixed << std::setprecision(0) << dominant_term << " +";

      QGraphicsTextItem *dominant_text= new QGraphicsTextItem(QString::fromStdString(dominant_string.str()));
      dominant_text->setFont(ten_font);
      QRectF dominant_rect = dominant_text->boundingRect();
      x=x_right-dominant_rect.width();
      dominant_text->setX(x);
      dominant_text->setY(y);
      y+= dominant_rect.height();
      item_list.push_back(dominant_text);
      dominant_text->show();
    }
    else {
      y += ten_rect.height();
    }

    if (abs(use_exponent)>0) {
      item_list.push_back(exponent_text.get());
      exponent_text.release()->show();
      item_list.push_back(ten_text.get());
      ten_text.release()->show();
    }
    else {
      exponent_text.reset();
      ten_text.reset();
    }

    QRectF label_rect;
    int y_step=0;
    int flow_margin=0;
    double value_step = range / ( colour_list.size()-2 );
    double label_value_step = value_step * scale;
    int each_i=1;
    {
      std::stringstream temp_string;
      temp_string << "-847156.9";

      QGraphicsTextItem temp_text(QString::fromStdString(temp_string.str()));
      temp_text.setFont(label_font);
      label_rect = temp_text.boundingRect();

      y_step=static_cast<int>((rect.height()-(y+label_rect.height()-rect.y())-static_cast<int>(label_rect.height()*.5))/(colour_list.size()+.4));
      flow_margin = static_cast<int>(y_step * .2);
      if (flow_margin<3) flow_margin=3;
      y_step=(rect.height()-(y+label_rect.height()-rect.y())-flow_margin*2-static_cast<int>(label_rect.height()*.5))/(colour_list.size());

      {
	int y_temp=y_step;
	while (y_temp < label_rect.height()*1.3) {
	  each_i++;
	  y_temp+=y_step;
	}
      }
      if (each_i>1) {
	label_value_step *= each_i;
      }
    }

    int step_precision=4;
    double digit=1e3;
    {
    //  label_value_step=std::abs(label_value_step);
    //  //      double temp_step = floor(value_step * 1e3);
    //  //      double old_temp_step;
      //	std::cout << "INFO [PaletteItem::ctor] range = " << min << " - " << max << " * " << scale <<  std::endl;
      //	std::cout << "INFO [PaletteItem::ctor] range = " << min*scale << " - " << max*scale << " / + " << value_step << " x 10^ " << exponent << std::endl;
      do {
	digit *= 1e-1;
	step_precision--;
	// 	  std::cout << "INFO [PaletteItem::ctor] digit = " << log10(digit)
	// 		    << " step_precision = " << step_precision
	// 		    << " a = " << label_value_step * digit 
	// 		    << " b = " << floor(label_value_step *digit+.5)
	// 		    << " comp. = " << (label_value_step * digit - floor(label_value_step *digit))
	// 		    << std::endl;
      } while (std::abs(label_value_step * digit - floor(label_value_step *digit+.5)) < .05  && step_precision>0);
    }

    //       std::cout << "INFO [PaletteItem::ctor] exponent = " << exponent
    // 		<< " label_value_step = " << label_value_step
    // 		<< " step_prec. = " << step_precision
    // 		<< std::endl;

    int digits = step_precision;
    int step_exponent = static_cast<int>(ceil(-log10(label_value_step)));
    if (step_exponent>0) {
      digits+=step_exponent;
    }
    int precision =  digits - (max_exponent-use_exponent+1);
    if (precision < 0 ) precision=0;
    bool need_dot = false;
    if (max_exponent-use_exponent+1>digits ) {
      digits = max_exponent-use_exponent+1;
    }
    else {
      need_dot =true;
    }

    //       std::cout << "INFO [PaletteItem::ctor] step precisition = " << step_precision << " step_exponent=" << step_exponent
    // 		<< " exponent-use_exponent=" << max_exponent-use_exponent << " -> digits = " << digits << std::endl;

    int min_digits=1;
    if (std::abs(min)>label_value_step) min_digits = static_cast<int>(log10(std::abs(min*use_scale)))+1;
    if (min_digits>digits) digits=min_digits;

    int max_digits = 1;
    if (std::abs(max)>label_value_step) max_digits = static_cast<int>(log10(std::abs(max*use_scale)))+1;
    if (max_digits>digits) digits=max_digits;

    if (need_dot) digits++;

    //       std::cout << "INFO [PaletteItem::ctor] use  ";
    //       if (std::abs(dominant_term)>0) {
    // 	std::cout << dominant_term  << " x 10^" << exponent+1 << " + ";
    //       }
    //       std::cout << min * use_scale << " - " << max * use_scale << " / + " << label_value_step/scale*use_scale 
    // 		<< " x 10^" << use_exponent
    // 		<< " digits = " << digits << " precision = " << precision
    // 		<< std::endl;

    double value = max*use_scale;
    value_step *= use_scale;
    double offset = .5*exp(-(precision)*log(10) );
    if (min*unsign<0) {
      offset = -offset;
    }

    std::stringstream label_string;
    label_string << std::setprecision(precision) << std::fixed;

    //      label_string <<  ( std::abs(value) > std::abs(offset) ?  (unsign * value+offset) : 0.);
    label_string <<  ( std::abs(value) > std::abs(offset) ?  (unsign * value) : 0.);

    std::stringstream min_label_string;
    min_label_string << std::setprecision(precision) << std::fixed << std::setw(digits) << ( std::abs(min) > std::abs(offset) ?  (unsign * min * use_scale+offset) : 0.);

    QGraphicsTextItem *min_label_text= canvas->addText(QString::fromStdString(min_label_string.str()));
    min_label_text->setFont(label_font);
    QRectF min_label_rect = min_label_text->boundingRect();
    delete min_label_text;
    min_label_string.str("");
    min_label_string << std::setw(digits) << unsign*(min*use_scale-value_step*each_i);
    min_label_text= canvas->addText(QString::fromStdString(min_label_string.str()));
    min_label_text->setFont(label_font);
    QRectF min_label_rect2 = min_label_text->boundingRect();
    if (min_label_rect2.width() > min_label_rect.width()) {
      min_label_rect = min_label_rect2;
    }

    QGraphicsTextItem *label_text= canvas->addText(QString::fromStdString(label_string.str()));
    label_text->setFont(label_font);
    //      QRect 
    label_rect = label_text->boundingRect();
    //       std::cout << "INFO [PaletteItem::ctor] label_width = " << label_rect.width() << " min " << min_label_rect.width() << " min2 = " << min_label_rect2.width()
    // 		<< std::endl;
      
    if (label_rect.width()>min_label_rect.width()) {
      x=x_right;//-label_rect.width();
    }
    else {
      x=x_right;//-min_label_rect.width();
    }

    y+= label_rect.height();
    //      int y_step=(rect.height()-(y-rect.y())-static_cast<int>(label_rect.height()*.5))/colour_list.size();

    //      QRect ten_rect = ten_text->boundingRect();
    //      y+=ten_rect.height();
    QRectF digit_rect;
    int element_width = x-margin-rect.x();
    {
      // Really not sure what these temp_strings are for.
      // Seems there could have been a better way to do this!
      std::stringstream temp_string;
      temp_string << "8";

      QGraphicsTextItem temp_text(QString::fromStdString(temp_string.str()));
      temp_text.setFont(label_font);
      digit_rect = temp_text.boundingRect();
    }
    element_width -= digit_rect.width()/2;

    if (element_width<1) element_width=1;
    if (element_width>static_cast<int>(y_step*16/9.)) element_width=static_cast<int>(y_step*16/9.);
    x-= element_width;
    x -= digit_rect.width()/2;

    palette_rect.setY(y);
    palette_rect.setX(x);
    int counter_i=1;
    std::vector<QColor>::/*const_*/reverse_iterator iter = colour_list.rbegin();
    // overflow
    {
      label_string.str(">");
      QGraphicsTextItem *overflow_text=canvas->addText(QString::fromStdString(label_string.str()));
      overflow_text->setFont(label_font);
      QRectF overflow_rect = overflow_text->boundingRect();
      overflow_text->setY(y-static_cast<int>(overflow_rect.height()*.3)-3);
      overflow_text->setX(x_right+20); //- overflow_rect.width());
      overflow_text->show();
      item_list.push_back(overflow_text);
	
      QGraphicsRectItem *colour_element=new QGraphicsRectItem( x,y-1,element_width,y_step+1);
      canvas->addItem(colour_element);
      colour_element->setBrush( QBrush(*iter) );
      colour_element->show();
      item_list.push_back(colour_element);
      y+= y_step + flow_margin;
      iter++;
    }
    start_axis = y;
    std::vector<QColor>::/*const_*/reverse_iterator prev_iter = iter;

    label_text->setY(y-static_cast<int>(label_rect.height()*.5));
    label_text->setX(x_right +8);//- label_rect.width());
    label_text->show();
    item_list.push_back(label_text);
    for (;
	 ++iter != colour_list.rend();
	 counter_i++) {

      value -= value_step;
      if (counter_i%each_i == 0) {
	label_string.str("");
	if (unsign * value * offset < 0) {
	  offset = -offset;
	}
	label_string << std::setw(digits) << ( std::abs(value) > std::abs(offset) ?  (unsign * value+offset) : 0.);
	// label_string << std::setw(digits) << ( std::abs(value) > std::abs(offset) ?  (unsign * value) : 0.);
	label_text=canvas->addText( QString::fromStdString(label_string.str()) );
	label_text->setFont(label_font);
	label_rect = label_text->boundingRect();
	label_text->setY(y+static_cast<int>(label_rect.height()*.5));
	label_text->setX(x_right +8);//- label_rect.width());
	label_text->show();
	item_list.push_back(label_text);
      }
      
      QGraphicsRectItem *colour_element=new QGraphicsRectItem( x,y-1,element_width,y_step+1);
      canvas->addItem(colour_element);
      colour_element->setBrush( QBrush(*prev_iter) );
      colour_element->show();
      item_list.push_back(colour_element);
      y+= y_step;
      prev_iter=iter;
    }
    end_axis = y;
    y+= flow_margin;

    // overflow
    {
      label_string.str("<");
      QGraphicsTextItem *overflow_text= canvas->addText(QString::fromStdString(label_string.str()));
      overflow_text->setFont(label_font);
      label_rect = overflow_text->boundingRect();
      overflow_text->setY(y+static_cast<int>(label_rect.height()*.3));
      overflow_text->setX(x_right+10);// - label_rect.width());
      overflow_text->show();
      item_list.push_back(overflow_text);
	
      QGraphicsRectItem *colour_element=new QGraphicsRectItem( x,y-1,element_width,y_step+1);
      canvas->addItem(colour_element);
      colour_element->setBrush( QBrush(*prev_iter) );
      colour_element->show();
      item_list.push_back(colour_element);
      y+= y_step + flow_margin;
      iter++;
    }
    palette_rect.setHeight(y-palette_rect.y());
    palette_rect.setWidth(element_width);
      
    //       std::cout << "INFO [PaletteItem::ctor] value = " <<  value << " -> " << unsign * value+offset << std::endl; 

    delete min_label_text;

      
  }
  else {
    // vertical
      
  }

  // invisible bounding box
  QGraphicsRectItem *a_rectangle = new QGraphicsRectItem(rect);
  canvas->addItem(a_rectangle);
  a_rectangle->setZValue(a_rectangle->zValue()+1);
  {
    QPen pen(a_rectangle->pen());
    //pen.setColor(QColor(255,0,0));
    pen.setStyle(Qt::NoPen);
    a_rectangle->setZValue(a_rectangle->zValue()+1);
    a_rectangle->setPen(pen);
  }
  //  a_rectangle->setPen(QPen(QColor(200,200,200),0));
  a_rectangle->show();
  item_list.push_back(a_rectangle);

  palette_rect.setX(palette_rect.x()-1);
  palette_rect.setY(palette_rect.y()-1);
  palette_rect.setWidth(palette_rect.width()+2);
  palette_rect.setHeight(palette_rect.height()+2);

  PaletteAxisItem *palette_item=new PaletteAxisItem(palette_rect, start_axis, end_axis,min,max,colour_list.size()-2);
  canvas->addItem(palette_item);
  QPen pen(palette_item->pen());
  pen.setStyle(Qt::NoPen);
  palette_item->setZValue(a_rectangle->zValue()+1);
  palette_item->setPen(pen);
  palette_item->show();
  item_list.push_back(palette_item);
}
