#ifndef _PaletteTestCanvas_H_
#define _PaletteTestCanvas_H_

#include <QGraphicsView>
#include <QMouseEvent>
#include <QRect>
#include "PaletteItemFactory.h"
#include <memory>

class QGraphicsItem;

class CanvasTip  : public QObject
{
  public:
    CanvasTip(QWidget *parent) : QObject(parent) {}

    void setCurrentItem(const QRectF &rect, const QString &message) {
      m_message = message;
      m_rect = rect;
    }

    void maybeTip(const QPoint &point);

  protected:
    QString m_message;
    QRectF  m_rect;
};


class PaletteTestCanvas : public QGraphicsView
{
  Q_OBJECT

public:
  PaletteTestCanvas(QWidget *parent);
  ~PaletteTestCanvas();

  void clear();

  void createPalette(const QRectF  &rect, std::vector<QColor > &colour_list, float min, float max) ;

signals:
  void axisChangeRequest(float min, float max);
  void mouseOverAxisItem(PaletteAxisItem *, const QPoint &point, const QRectF &rect);

public slots:
  void changeAxis(float min, float max);
  void showMessage(PaletteAxisItem *axis_item, const QPoint &point, const QRectF &rect);

private:

  void mousePressEvent(QMouseEvent* e);
  void mouseMoveEvent(QMouseEvent* e);
  void mouseEvent(QMouseEvent* e);

  QRectF                        m_rect;
  std::vector< QGraphicsItem* > m_itemList;
  std::vector< QColor >         m_colourList;

  std::unique_ptr<CanvasTip>    m_tip;


};
#endif
