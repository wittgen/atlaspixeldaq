#include <qapplication.h>
#include "CanvasDialog.h"
#include <iostream>

int main( int argc, char ** argv )
{
    QApplication app( argc, argv );

    float a=-20;
    float b=20;
    if (argc==3) {
      a=static_cast<float>(strtod(argv[1],NULL));
      b=static_cast<float>(strtod(argv[2],NULL));
    }
    if (a>b) {
      float temp=a;
      a=b;
      b=temp;
    }
    std::cout << "INFO [main] range = " << a << " - " << b << std::endl;
    CanvasDialog w(a,b);
    w.show();
    app.connect( &app, SIGNAL( lastWindowClosed() ), &app, SLOT( quit() ) );
    return app.exec();
}
