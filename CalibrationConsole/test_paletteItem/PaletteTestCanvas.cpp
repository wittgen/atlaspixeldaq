#include "PaletteTestCanvas.h"
#include <QMatrix>
#include <QMenu>
#include <QEvent>
#include <QCursor>
#include <QLineEdit>
#include <QApplication>
#include <QToolTip>
//Added by qt3to4:
#include <QMouseEvent>
#include <AxisRangeBase.h>

#include <sstream>
#include <iostream>

void CanvasTip::maybeTip( const QPoint &pos )
{

  //if (!m_rect.isValid() || !m_rect.contains(pos) ) return;

  QToolTip::showText( pos, m_message );
}


PaletteTestCanvas::PaletteTestCanvas(QWidget *parent)
  : QGraphicsView(new QGraphicsScene(0,0,300,330),parent),
    m_tip( new CanvasTip(this) )
{
  viewport()->setMouseTracking(true);

  connect(this,SIGNAL( axisChangeRequest(float,float) ), this, SLOT( changeAxis(float, float) ) );
  connect(this,SIGNAL( mouseOverAxisItem(PaletteAxisItem *, const QPoint &, const QRectF &) ),
	  this,        SLOT( showMessage(PaletteAxisItem *, const QPoint &, const QRectF &) ) );
}

PaletteTestCanvas::~PaletteTestCanvas()
{
}

void PaletteTestCanvas::createPalette(const QRectF &rect, std::vector<QColor > &colour_list, float min, float max) 
{
  clear();
  m_colourList = colour_list;
  m_rect = rect;
  PaletteItemFactory factory(m_colourList, min, max);
  factory.create(scene(), rect, m_itemList);
}


void PaletteTestCanvas::changeAxis(float min, float max)
{
  if (min<max) {
    clear();
    PaletteItemFactory factory(m_colourList, min, max);
    factory.create(scene(), m_rect, m_itemList);
    updateSceneRect(m_rect);
    scene()->update();
  }
}

void PaletteTestCanvas::mousePressEvent(QMouseEvent* e)
{
  mouseEvent(e);
}

void PaletteTestCanvas::mouseMoveEvent(QMouseEvent* e)
{
  mouseEvent(e);
}

void PaletteTestCanvas::mouseEvent(QMouseEvent* e) 
{
  QList<QGraphicsItem*> itList = items(e->pos());
  for(QList<QGraphicsItem*>::iterator it=itList.begin(); it!=itList.end(); it++){
    PaletteAxisItem  *axis_item=dynamic_cast<PaletteAxisItem  *>(*it);
    if (axis_item){
      if (e->button() == Qt::RightButton ) {
	//QEvent::Type type=e->type();
	//std::cout << "INFO [PaletteAxisItem::mouseEvent] type = " << type << "." << std::endl;
	if (e->type() == QEvent::MouseButtonPress) {
	  QMenu mymenu(this);
	  QAction *a = mymenu.addAction("Adjust Range");
	  if ( a == mymenu.exec(QCursor::pos())) {
	    AxisRangeBase dialog(this);
	    if (dialog.exec()  == QDialog::Accepted){ 
	      double a_min = strtod(dialog.m_minEntry->text().toLatin1().data(),NULL);
	      double a_max = strtod(dialog.m_maxEntry->text().toLatin1().data(),NULL);
	      if (a_min>a_max) {
		double temp = a_max;
		a_max=a_min;
		a_min=temp;
	      }
	      std::cout << "INFO [PaletteTestCanvas::mouseEvent] range = " << a_min << " - " << a_max  << std::endl;
	      if (a_min <a_max) {
		emit axisChangeRequest(static_cast<float>(a_min), static_cast<float>(a_max));
	      }
	    }

	    return;
	  }

	}
      }
      else {
// 	QRect w_rect = worldMatrix().mapRect(axis_item->boundingRect());
// 	w_rect.moveBy(-contentsX(),-contentsY());
	if (e->type() == QEvent::MouseButtonPress) {
	  QRectF w_rect = matrix().mapRect(axis_item->sceneBoundingRect());
	  emit mouseOverAxisItem(axis_item, e->pos(), w_rect);
	}
      }
    }
  }
  
}

void PaletteTestCanvas::showMessage(PaletteAxisItem *axis_item, const QPoint &point, const QRectF &rect)
{
  if (axis_item) {
    std::stringstream message;
    float value = axis_item->lowerEdge(point);
    if (axis_item->isUnderflow(point)) {
      message << " < " << value;
    }
    else if (axis_item->isOverflow(point)) {
      message << " > " << value;
    }
    else {
      message << value << " ... " << value + axis_item->step();
    }
    QString the_message(message.str().c_str());
    m_tip->setCurrentItem(rect, the_message);
    // alignment in y doesn't quite work, so comment for now
    //m_tip->maybeTip(QCursor::pos());
  }
}

void PaletteTestCanvas::clear()
{

  for( std::vector<QGraphicsItem*>::iterator item_iter = m_itemList.begin();
       item_iter != m_itemList.end();
       item_iter++) {
    delete *item_iter;
  }
  m_itemList.clear();

}

