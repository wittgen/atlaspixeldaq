#ifndef _CanvasDialog_h_
#define _CanvasDialog_h_

#include <vector>
#include <QDialog>

class PaletteTestCanvas;
class QColor;
class QVBoxLayout;
class QHBoxLayout;
class QFrame;
class QSpacerItem;
class QPushButton;

class CanvasDialog : public QDialog
{
    Q_OBJECT
public:
  CanvasDialog(float a, float b, QWidget* parent = 0);
  ~CanvasDialog();

protected:
  PaletteTestCanvas *m_canvasView;

private:
  std::vector< QColor > m_colourList;
  void setupUi();

  QVBoxLayout *vboxLayout;
  QFrame *m_frame;
  QVBoxLayout *vboxLayout1;
  QHBoxLayout *hboxLayout;
  QSpacerItem *spacer1;
  QPushButton *pushButton1;
  QSpacerItem *spacer2;

protected slots:
  virtual void languageChange();

};
#endif
