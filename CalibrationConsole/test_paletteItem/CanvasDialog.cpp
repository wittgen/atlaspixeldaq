#include "CanvasDialog.h"
#include <QLayout>
#include <QColor>
#include <QFrame>
#include <QApplication>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QSpacerItem>

#include "PaletteTestCanvas.h"

#include <iostream>

CanvasDialog::CanvasDialog(float a, float b, QWidget *parent) : QDialog(parent)
{
  setupUi();

  m_canvasView = new PaletteTestCanvas(m_frame);
  QLayout *frameLayout = m_frame->layout();
  if( frameLayout != 0 )
    frameLayout->addWidget( m_canvasView );
  else
    std::cout << "ERROR [CanvasDialog::CanvasDialog] Can't find frame layout!" << std::endl;

  m_canvasView->show();

  {
    for (unsigned int color_i=0; color_i<20;color_i++) {
      QColor col;
      col.setHsv((250*color_i)/20,255,255);//,QColor::Hsv)
      m_colourList.push_back(col);
    }
  }

  m_canvasView->createPalette(QRectF(20,20, 50,300), m_colourList, a,b);
}

CanvasDialog::~CanvasDialog()
{
}

void CanvasDialog::setupUi()
{
  if (objectName().isEmpty())
    setObjectName(QString::fromUtf8("CanvasDialog"));
  resize(600, 480);
  vboxLayout = new QVBoxLayout(this);
  vboxLayout->setSpacing(6);
  vboxLayout->setContentsMargins(11, 11, 11, 11);
  vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
  m_frame = new QFrame(this);
  m_frame->setObjectName(QString::fromUtf8("m_frame"));
  m_frame->setFrameShape(QFrame::StyledPanel);
  m_frame->setFrameShadow(QFrame::Raised);
  vboxLayout1 = new QVBoxLayout(m_frame);
  vboxLayout1->setSpacing(6);
  vboxLayout1->setContentsMargins(11, 11, 11, 11);
  vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
  
  vboxLayout->addWidget(m_frame);
  
  hboxLayout = new QHBoxLayout();
  hboxLayout->setSpacing(6);
  hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
  spacer1 = new QSpacerItem(131, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
  
  hboxLayout->addItem(spacer1);
  
  pushButton1 = new QPushButton(this);
  pushButton1->setObjectName(QString::fromUtf8("pushButton1"));
  
  hboxLayout->addWidget(pushButton1);
  
  spacer2 = new QSpacerItem(131, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);
  
  hboxLayout->addItem(spacer2);
  
  
  vboxLayout->addLayout(hboxLayout);
  
  languageChange();
  QObject::connect(pushButton1, SIGNAL(clicked()), this, SLOT(close()));
  
  QMetaObject::connectSlotsByName(this);
} // setupUi

void CanvasDialog::languageChange()
{
  setWindowTitle(QApplication::translate("CanvasDialog", "Form1", 0, QApplication::UnicodeUTF8));
  pushButton1->setText(QApplication::translate("CanvasDialog", "Quit", 0, QApplication::UnicodeUTF8));
}
