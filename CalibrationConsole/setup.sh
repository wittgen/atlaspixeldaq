#!/bin/sh

this=`pwd`

if test -d ${this}/CalibrationData; then
  export LD_LIBRARY_PATH=$this/lib:$PIX_ANA/ConfigWrapper:$PIX_ANA/Visualiser:$LD_LIBRARY_PATH
else
  echo "ERROR [setup.sh] You have to enter the directory of Karl the CalibrationConsole source code." >&2
  false
fi

if test -n "$CAN"; then
  if (echo $LD_LIBRARY_PATH | grep -q ${CAN}); then
      :
  else
    export LD_LIBRARY_PATH=${CAN}:${CAN}/PixCalibDbCoral:${CAN}/PixCoral:$PIX_ANA/Visualiser:$LD_LIBRARY_PATH
  fi
fi

export KARL_DATA_PATH=${this}/share
