#!/bin/bash

#Get the line for the CVMFS status and chech if server is transaction
server_status=`cvmfs_server list | grep atlas-pixel-daq`
if [[ $server_status == *"(stratum0 / S3)"* ]]; then
  echo "No ongoing CVMFS transaction detected, initializing new transaction."
  # Start transaction
  cvmfs_server transaction atlas-pixel-daq.cern.ch

  # Deploy the latest build
  echo "Deploying build: $2 ($3)"
  path=$1
  tag=$2
  branch=$3

  if [ "$tag" == "latest" ]; then
    echo "Deploying nightly..."
    if [[ -d "$path/nightly/$branch" ]]; then
      echo "Deleting old \"latest\" build for branch $branch"
      rm -rf /cvmfs/atlas-pixel-daq.cern.ch/software/atlaspixeldaq/latest/$branch
      echo "Moving new build into place"
      mv $path/nightly/$branch /cvmfs/atlas-pixel-daq.cern.ch/software/atlaspixeldaq/latest
    else
      echo "Did not find suitable path to install."
      echo "Aborting CVMFS transaction."
      cvmfs_server abort -f atlas-pixel-daq.cern.ch
      exit 1
    fi
  else
    echo "Deploying tag..."
    if [[ -d "$path/$tag" ]]; then
      echo "Moving new build into place"
      mv $path/$tag /cvmfs/atlas-pixel-daq.cern.ch/software/atlaspixeldaq/
    else
      echo "Did not find suitable path to install."
      echo "Aborting CVMFS transaction."
      cvmfs_server abort -f atlas-pixel-daq.cern.ch
      exit 1
    fi
  fi

  # Clean up old stuff
  #rm -rf $path

  # Publish changes
  echo "Publishing CVMFS transaction."
  cvmfs_server publish atlas-pixel-daq.cern.ch
  exit 0
else
  (>&2 echo "#################################")
  (>&2 echo "### CVMFS Transaction ongoing ###")
  (>&2 echo "######## Deploy cancelled #######")
  (>&2 echo "#################################")
  exit 1
fi
