#!/usr/bin/env python

import os
import json
import sys

# Check if we got token:
if len(sys.argv) != 5:
  print "wrong number of arguments" 
  sys.exit(1)

api_token = sys.argv[1]
ci_project_id = sys.argv[2]
ci_pipeline_id = sys.argv[3]
ci_step = sys.argv[4]

print "Downloading job list"
data = json.load(os.popen("curl -g --header \"PRIVATE-TOKEN:%s\" \"https://gitlab.cern.ch/api/v4/projects/%s/pipelines/%s/jobs?scope[]=success&per_page=200\"" % (api_token, ci_project_id, ci_pipeline_id)))

# Find last passed builds
for i in range(0, len(data)):
  print(data[i])
  if str(ci_step) not in data[i]['name']:
    continue
  print "Downloading artifact for job %s" % data[i]['id']
  path = ""
  if data[i]['tag'] is False:
    path = "./nightly/"+data[i]['ref']
  else:
    path = "./"+data[i]['ref']

  os.system("mkdir -p %s" % path)
  os.system("curl --location --header \"PRIVATE-TOKEN:%s\" \"https://gitlab.cern.ch/api/v4/projects/%s/jobs/%s/artifacts\" -o %s/artifact.zip" % (api_token, ci_project_id, data[i]['id'], path))
  os.system("cd %s; unzip artifact.zip; rm artifact.zip; pwd; ls; ls -l" % path)