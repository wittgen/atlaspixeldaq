#!/bin/bash
#
if test $1 != ""; then
  export TDAQ_PARTITION=$1
fi
echo $TDAQ_PARTITION
export date=`date +%Y%m%d-%H%M`
if test ! -d /det/pix/logs/${TDAQ_PARTITION}; then
  mkdir /det/pix/logs/${TDAQ_PARTITION}
fi

cdat=`date +%s`

if test -d /det/pix/logs/${TDAQ_PARTITION}; then
  cd /det/pix/logs/${TDAQ_PARTITION}
  if test -f ../.logArchiveAuth; then
    tar -cvzf /det/pix/logArchive/tdaq_${TDAQ_PARTITION}_${date}.tgz ./
    for f in `find .`; do
      fdat=`stat -c %Y $f `  
      diff=0
      ((diff = cdat - fdat)) 
      if ((diff > 180)); then
        /bin/rm $f
      fi
    done
    rm -rf .*
    mkdir -p DSPBuffers
    chmod -R a+rwx ../${TDAQ_PARTITION}
  fi

  for f in `ls /det/pix/logArchive/tdaq_${TDAQ_PARTITION}*.tgz`; do
    fdat=`stat -c %Y $f `  
    diff=0
    ((diff = cdat - fdat)) 
    if ((diff > 1500000)); then
      /bin/rm $f
      echo TO_REMOVE $f $diff $cdat $fdat
    else
      echo TO_KEEP $f $diff
    fi
  done 
fi
