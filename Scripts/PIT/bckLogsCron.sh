#!/bin/bash
#
dtime=$1
dtime1=2592000
dtime2=15552000

dtime=${dtime:=7}
((dtime = dtime * 86400))
((dtime1 = 37 * 86400))
((dtime2 = 187 * 86400))
echo $dtime " " $dtime1 " " $dtime2

export date=`date +%Y%m%d-%H%M`
cdat=`date +%s`
cdat1=0
((cdat1 = cdat - dtime)) 

cd /det/pix/logs

mkdir /detpriv/pix/data/archive/logArchive/${cdat1}
for f in `find .`; do
  if test -d $f; then
    if test $f != .; then
      mkdir /detpriv/pix/data/archive/logArchive/${cdat1}/$f
    fi            
  else
    fdat=`stat -c %Y $f `  
    diff=0
    ((diff = cdat - fdat)) 
    if ((diff > dtime)); then
      echo $f
      /bin/mv $f /detpriv/pix/data/archive/logArchive/${cdat1}/$f
    fi
  fi 
done

cd /detpriv/pix/data/archive/logArchive

((cdat1 = cdat - dtime1))
for a in `ls -d 1*`; do
  if test -d $a; then
    if ((a < cdat1)); then
      echo "Tar " $a
      tar -cvzf $a.tgz $a
      rm -rf $a
    fi
  fi 
done

((cdat1 = cdat - dtime2))
for a in `ls -d 1*.tgz`; do
  b=${a%.tgz}
  if ((b < cdat1)); then
    echo "Del " $a
    rm -rf $a
  fi
done



