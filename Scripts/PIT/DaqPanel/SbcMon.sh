#!/bin/sh
#source /det/pix/scripts/Default-pixel.sh
#
for var in `${PIX_LIB}/Examples/GetDefTags`; do 
  export $var
done

level="NONE"
for role in `amUserRoles -S`; do
  if test $role = "PIX:DAQ:expert"; then
    level="EXPERT"
  fi
  if test $role = "PIX:shifter"; then
    level="SHIFTER"
  fi
done

echo "User " $USER " role " $level
if test $level = "SHIFTER" -o $level = "EXPERT"; then
  source /det/pix/scripts/SbcMon.sh
fi
