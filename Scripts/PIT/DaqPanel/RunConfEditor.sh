#!/bin/sh
#source /det/pix/scripts/Default-pixel.sh
#
for var in `${PIX_LIB}/Examples/GetDefTags`; do 
  export $var
done

level="NONE"
for role in `amUserRoles -S`; do
  if test $role = "PIX:DAQ:expert"; then
    level="EXPERT"
  fi
  if test $role = "PIX:shifter"; then
    level="SHIFTER"
  fi
done

echo "User " $USER " role " $level
if test $level = "SHIFTER" -o $level = "EXPERT"; then
  xterm -e '${PIX_LIB}/Examples/RunConfigEditor --dbPart ${PIX_PART_INFR_NAME} --idTag ${PIX_ID_DEFTAG} --connTag ${PIX_CONN_DEFTAG} --cfgTag ${PIX_CFG_DEFTAG} --cfgModTag ${PIX_MODCFG_DEFTAG} --runConf ${PIX_RUN_DEFTAG} --disConf ${PIX_DIS_DEFTAG}'
fi
