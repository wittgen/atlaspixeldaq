#!/bin/sh
echo $1
source /det/pix/scripts/Default-pixel.sh
echo $PIX_LIB
#
scanlist=`${PIX_LIB}/Examples/ListScansInHistoServer $2`

for scan in `echo $scanlist`; do
  if [[ $scan == S0* ]]; then
    echo $scan
  fi
done

ans=y
while test $ans = "y" -o $ans = "Y"; do
  read -p "Do you want to delete a scan (y/n) ? " ans
  echo ""
  if test $ans = "y" -o $ans = "Y"; then
    read -p "  Scan number (no slashes) ? " scan_del
    ok=n;
    for scan in $scanlist; do
      if test $scan = ${scan_del}/; then
        ok=y
      fi
    done
    if test ok=y; then
      ${PIX_LIB}/Examples/HistoRemove $2 /${scan_del}/
    fi
  fi
done 
