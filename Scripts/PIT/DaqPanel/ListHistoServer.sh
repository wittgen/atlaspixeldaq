#!/bin/sh
#source /det/pix/scripts/Default-pixel.sh
#
for var in `${PIX_LIB}/Examples/GetDefTags`; do 
  export $var
done

level="NONE"
for role in `amUserRoles -S`; do
  if test $role = "PIX:DAQ:expert"; then
    level="EXPERT"
  fi
  if test $role = "PIX:shifter"; then
    level="SHIFTER"
  fi
done

echo "User " $USER " role " $level
echo $daq
if test $level = "SHIFTER" -o $level = "EXPERT"; then
  xterm -e '/det/pix/scripts/DaqPanel/ListHistoServer-app.sh ${daq} ${PIX_PART_INFR_NAME}' &
fi
