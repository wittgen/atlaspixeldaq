#!/bin/sh
#

if [ -z $1 ]; then
  daq="daq"
else
  daq=$1
fi
pixdaqdir="${HOME}"
pixdbdir="/det/pix/db"

alias daq_setup="source /det/pix/scripts/DEVEL-pixel.sh"

. /det/pix/scripts/BASE-1-6-1-pixel.sh
