#!/bin/sh
#

if [ -z $1 ]; then
  export daq="PixelDAQ-1-6-1"
else
  export daq=$1
fi
pixdaqdir="/det/pix/daq"
pixdbdir="/det/pix/db"

alias daq_setup="source /det/pix/scripts/PixelDAQ-1-6-1-pixel.sh $daq"
export DAQ_SETUP_SCRIPT="/det/pix/scripts/PixelDAQ-1-6-1-pixel.sh $daq"

. /det/pix/scripts/BASE-1-6-1-pixel.sh
