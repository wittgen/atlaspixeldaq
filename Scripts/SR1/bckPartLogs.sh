#!/bin/bash
#
export date=`date +%Y%m%d-%H%M`
cd /daq/logs/${TDAQ_PARTITION}
if test -f ../.logArchiveAuth; then
  tar -cvzf /daq/logArchive/tdaq_${TDAQ_PARTITION}_${date}.tgz ./
  for f in `ls`; do
    rm -rf $f 
  done
fi
 
cdat=`date +%s`
for f in `ls /daq/logArchive/tdaq_${TDAQ_PARTITION}*.tgz`; do
  fdat=`stat -c %Y $f `  
  diff=0
  ((diff = cdat - fdat)) 
  if ((diff > 1500000)); then
    /bin/rm -rf $f
    echo TO_REMOVE $f $diff $cdat $fdat
  else
    echo TO_KEEP $f $diff
  fi
done 
