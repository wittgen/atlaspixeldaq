#!/bin/bash
#
cd ../../..
export DaqDir=`pwd`

if test -d $DaqDir; then 
  echo "Fixing xml in $DaqDir"

  if test -f $DaqDir/Applications/Scripts/FixXmliGlobal.dat; then
    . ${DaqDir}/Applications/Scripts/FixXmlGlobal.dat
  else
    export baseDir="/det/pix/daq/PixelDAQ-1-4-0"
    export partSuffix="";
  fi 

  export DaqDirXml=${DaqDir}/Applications/PixRCD/PixRCD-00-01-00/PixRCDConfiguration/PIT-GLOBAL
  if test -d $DaqDirXml; then
    cd $DaqDirXml  
    perl -pi.bak -e "s|${baseDir}|${DaqDir}|g" */*.xml
    echo "export baseDir=${DaqDir}" > ${DaqDir}/Applications/Scripts/FixXmlGlobal.dat
    if test "$1" = "-"; then
      perl -pi.bak -e "s|\"PixelInfr${partSuffix}\"|\"PixelInfr\"|g" */*.xml
      perl -pi.bak -e "s|\"PixelDD${partSuffix}\"|\"PixelDD\"|g" */*.xml
      perl -pi.bak -e "s|PixelInfr${partSuffix}|PixelInfr|g" ../start_infr 
      perl -pi.bak -e "s|PixelDD${partSuffix}|PixelDD|g" ../start_dd 
      echo "export partSuffix=\"\"" >> ${DaqDir}/Applications/Scripts/FixXmlGlobal.dat
      echo "export PIX_PART_INFR_NAME=\"PixelInfr\"" > ${DaqDir}/Applications/Scripts/SetPartNames.sh
      echo "export PIX_PART_DD_NAME=\"PixelDD\"" >> ${DaqDir}/Applications/Scripts/SetPartNames.sh
      echo "setenv PIX_PART_INFR_NAME \"PixelInfr\"" > ${DaqDir}/Applications/Scripts/SetPartNames.csh
      echo "setenv PIX_PART_DD_NAME \"PixelDD\"" >> ${DaqDir}/Applications/Scripts/SetPartNames.csh
    else
      if test "$1" = ""; then
        perl -pi.bak -e "s|\"PixelInfr${partSuffix}\"|\"PixelInfr_${USER}\"|g" */*.xml
        perl -pi.bak -e "s|\"PixelDD${partSuffix}\"|\"PixelDD_${USER}\"|g" */*.xml
        perl -pi.bak -e "s|PixelInfr${partSuffix}|PixelInfr_${USER}|g" ../start_infr 
        perl -pi.bak -e "s|PixelDD${partSuffix}|PixelDD_${USER}|g" ../start_dd 
        echo "export partSuffix=\"_${USER}\"" >> ${DaqDir}/Applications/Scripts/FixXmlGlobal.dat
        echo "export PIX_PART_INFR_NAME=\"PixelInfr_${USER}\"" > ${DaqDir}/Applications/Scripts/SetPartNames.sh
        echo "export PIX_PART_DD_NAME=\"PixelDD_${USER}\"" >> ${DaqDir}/Applications/Scripts/SetPartNames.sh
        echo "setenv PIX_PART_INFR_NAME \"PixelInfr_${USER}\"" > ${DaqDir}/Applications/Scripts/SetPartNames.csh
        echo "setenv PIX_PART_DD_NAME \"PixelDD_${USER}\"" >> ${DaqDir}/Applications/Scripts/SetPartNames.csh
      else
        perl -pi.bak -e "s|\"PixelInfr${partSuffix}\"|\"PixelInfr_$1\"|g" */*.xml
        perl -pi.bak -e "s|\"PixelDD${partSuffix}\"|\"PixelDD_$1\"|g" */*.xml
        perl -pi.bak -e "s|PixelInfr${partSuffix}|PixelInfr_$1|g" ../start_infr 
        perl -pi.bak -e "s|PixelDD${partSuffix}|PixelDD_$1|g" ../start_dd 
        echo "export partSuffix=\"_$1\"" >> ${DaqDir}/Applications/Scripts/FixXmlGlobal.dat
        echo "export PIX_PART_INFR_NAME=\"PixelInfr_$1\"" > ${DaqDir}/Applications/Scripts/SetPartNames.sh
        echo "export PIX_PART_DD_NAME=\"PixelDD_$1\"" >> ${DaqDir}/Applications/Scripts/SetPartNames.sh
        echo "setenv PIX_PART_INFR_NAME \"PixelInfr_$1\"" > ${DaqDir}/Applications/Scripts/SetPartNames.csh
        echo "setenv PIX_PART_DD_NAME \"PixelDD_$1\"" >> ${DaqDir}/Applications/Scripts/SetPartNames.csh
      fi
    fi
  else
    echo "$DaqDiriXml does not exists"                    
  fi
else
  echo "$DaqDir does not exists"
fi
