#!/bin/bash
#
cd $(dirname ${BASH_SOURCE[0]})/..
export DaqDir=`pwd`

if test -d $DaqDir; then 
  echo "Fixing xml in $DaqDir"

  if test -f $DaqDir/Scripts/FixXml.dat; then
    . ${DaqDir}/Scripts/FixXml.dat
  else
    if test $PIXRCD_FOLDER = "PIT"; then
      export baseDir="/det/pix/daq/PixelDAQ-1-5-1"
    else
      export baseDir="/home/pixeldaq/daq"
    fi  
    export partSuffix="";
  fi 
  echo $baseDir

  export DaqDirXml=${DaqDir}/PixRCD/PixRCD-00-01-00/PixRCDConfiguration/${PIXRCD_FOLDER}
  echo $DaqDirXml
  echo "1"
  echo $1
  echo "2"
  echo $2

  if test -d $DaqDirXml; then
    cd $DaqDirXml 
    echo "In dir" 
    if test "$2" = ""; then
      perl -pi.bak -e "s|${baseDir}|${DaqDir}|g" */*.xml
      echo "export baseDir=${DaqDir}" > ${DaqDir}/Scripts/FixXml.dat
    else
      if test "$2" = "-"; then
        if test $PIXRCD_FOLDER = "PIT"; then
          export defDir="/det/pix/daq/PixelDAQ-1-5-1"
        else
          export defDir="/home/pixeldaq/daq"
        fi  
        perl -pi.bak -e "s|${baseDir}|${defDir}|g" */*.xml
        echo "export baseDir=${defDir}" > ${DaqDir}/Scripts/FixXml.dat
      else
        perl -pi.bak -e "s|${baseDir}|$2|g" */*.xml
        echo "export baseDir=$2" > ${DaqDir}/Scripts/FixXml.dat
      fi
    fi
    if test "$1" = "-"; then
      perl -pi.bak -e "s|\"PixelInfr${partSuffix}\"|\"PixelInfr\"|g" */*.xml
      perl -pi.bak -e "s|\(PixelInfr${partSuffix}\)|\(PixelInfr\)|g" */*.xml
      perl -pi.bak -e "s|\"SR1-DAQSlice${partSuffix}\"|\"SR1-DAQSlice\"|g" */*.xml
      perl -pi.bak -e "s|PixelInfr${partSuffix}|PixelInfr|g" ../start_infr 
      perl -pi.bak -e "s|SR1-DAQSlice${partSuffix}|SR1-DAQSlice|g" ../start_daqslice_sr1
      perl -pi.bak -e "s|SR1-DAQSlice${partSuffix}|SR1-DAQSlice|g" ../start_daqslice_sr1
      echo "export partSuffix=\"\"" >> ${DaqDir}/Scripts/FixXml.dat
      echo "export PIX_PART_INFR_NAME=\"PixelInfr\"" > ${DaqDir}/Scripts/SetPartNames.sh
      echo "export PIX_PART_DD_NAME=\"PixelDD\"" >> ${DaqDir}/Scripts/SetPartNames.sh
      echo "setenv PIX_PART_INFR_NAME \"PixelInfr\"" > ${DaqDir}/Scripts/SetPartNames.csh
      echo "setenv PIX_PART_DD_NAME \"PixelDD\"" >> ${DaqDir}/Scripts/SetPartNames.csh
    else
      if test "$1" = ""; then
	echo "HERE"
        perl -pi.bak -e "s|\"PixelInfr${partSuffix}\"|\"PixelInfr_${USER}\"|g" */*.xml
        echo "HERE2"
	perl -pi.bak -e "s|\(PixelInfr${partSuffix}\)|\(PixelInfr_${USER}\)|g" */*.xml
        perl -pi.bak -e "s|\"SR1-DAQSlice${partSuffix}\"|\"SR1-DAQSlice_${USER}\"|g" */*.xml
        perl -pi.bak -e "s|PixelInfr${partSuffix}|PixelInfr_${USER}|g" ../start_infr 
        sed -i.bak -e "/TDAQ_PARTITION/s|SR1-DAQSlice${partSuffix}|SR1-DAQSlice_${USER}|g" ../start_daqslice_sr1
        echo "export partSuffix=\"_${USER}\"" >> ${DaqDir}/Scripts/FixXml.dat
        echo "export PIX_PART_INFR_NAME=\"PixelInfr_${USER}\"" > ${DaqDir}/Scripts/SetPartNames.sh
        echo "export PIX_PART_DD_NAME=\"SR1-DAQSlice_${USER}\"" >> ${DaqDir}/Scripts/SetPartNames.sh
        echo "setenv PIX_PART_INFR_NAME \"PixelInfr_${USER}\"" > ${DaqDir}/Scripts/SetPartNames.csh
        echo "setenv PIX_PART_DD_NAME \"SR1-DAQSlice_${USER}\"" >> ${DaqDir}/Scripts/SetPartNames.csh
      else
        perl -pi.bak -e "s|\"PixelInfr${partSuffix}\"|\"PixelInfr_$1\"|g" */*.xml
        perl -pi.bak -e "s|\(PixelInfr${partSuffix}\)|\(PixelInfr_$1\)|g" */*.xml
        perl -pi.bak -e "s|\"SR1-DAQSlice${partSuffix}\"|\"SR1-DAQSlice_$1\"|g" */*.xml
        perl -pi.bak -e "s|PixelInfr${partSuffix}|PixelInfr_$1|g" ../start_infr 
        sed -i.bak -e "/TDAQ_PARTITION/s|SR1-DAQSlice${partSuffix}|SR1-DAQSlice_$1|g" ../start_daqslice_sr1
        echo "export partSuffix=\"_$1\"" >> ${DaqDir}/Scripts/FixXml.dat
        echo "export PIX_PART_INFR_NAME=\"PixelInfr_$1\"" > ${DaqDir}/Scripts/SetPartNames.sh
        echo "export PIX_PART_DD_NAME=\"SR1-DAQSlice_$1\"" >> ${DaqDir}/Scripts/SetPartNames.sh
        echo "setenv PIX_PART_INFR_NAME \"PixelInfr_$1\"" > ${DaqDir}/Scripts/SetPartNames.csh
        echo "setenv PIX_PART_DD_NAME \"SR1-DAQSlice_$1\"" >> ${DaqDir}/Scripts/SetPartNames.csh
      fi
    fi
  else
    echo "$DaqDirXml does not exists"                    
  fi
else
  echo "$DaqDir does not exists"
fi
