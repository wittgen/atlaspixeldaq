#include "StartLogo.h"

#include <iostream>

#include <QPixmap>
#include <QApplication>
#include <QFrame>
#include <QGridLayout>
#include <QLabel>
#include <QPixmap>
#include <QBrush>
#include <QPalette>
#include <QProgressBar>

#include <QDesktopWidget>

using namespace PixDbBrowser;

StartLogo::StartLogo() : QWidget(0)
{
  size = QApplication::desktop()->size();

  setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
  max=0; current=1;	
	
  layout=new QGridLayout(this);
  
  layout->setColumnMinimumWidth(0,400);
  layout->setColumnStretch(0,0);	
  layout->setRowMinimumHeight(0,249);
  layout->setRowStretch(0,0);
  layout->setRowMinimumHeight(1,40);
  layout->setRowStretch(1,0);
	
//   logoFrame=new QFrame(this);
//   //  logoFrame->setFrameStyle(QFrame::StyledPanel); // added by MJK
//   logoFrame->setStyleSheet("background-image: url(://logo.jpg)"); // added by MJK

//   layout->addWidget(logoFrame,0,0);
  QLabel *picture = new QLabel(this);
  QImage image(":/logo.png");
  picture->setPixmap(QPixmap::fromImage(image));
  layout->addWidget(picture);

  frame2=new QFrame(this);
  layout->addWidget(frame2,1,0);
	
  QGridLayout* grid=new QGridLayout(frame2);
	
  grid->setColumnMinimumWidth(0,10);
  grid->setColumnStretch(0,0);
  grid->setColumnMinimumWidth(1,380);
  grid->setColumnStretch(1,0);
  grid->setColumnMinimumWidth(2,10);
  grid->setColumnStretch(2,0);
	
  grid->setRowMinimumHeight(0,5);
  grid->setRowStretch(0,0);
  grid->setRowMinimumHeight(1,10);
  grid->setRowStretch(1,0);
  grid->setRowMinimumHeight(2,5);
  grid->setRowStretch(2,0);
  grid->setRowMinimumHeight(3,10);
  grid->setRowStretch(3,0);
  grid->setRowMinimumHeight(4,10);
  grid->setRowStretch(4,0);
  
  label=new QLabel("Starting up...",frame2);
  grid->addWidget(label,1,1);
  
  progress=new QProgressBar(frame2);
  progress->setFixedSize(380,10);
  grid->addWidget(progress,3,1);
  progress->setMinimum(0);
  progress->setMaximum(0);
  progress->setValue(0);
  //progress->setCenterIndicator(true);
  progress->show();
  //progress->setPercentageVisible(false);
  
  setMinimumSize(sizeHint());
  setMaximumSize(sizeHint());
  
  move((size.width()-400)/2,(size.height()-289)/2);
  
  hide();
  
}

void StartLogo::updateProgressBar()
{
  progress->setMaximum(max);
  progress->setValue(current);
  label->setText(text);
  QWidget::update();
}
