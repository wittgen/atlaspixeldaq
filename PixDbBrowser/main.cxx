#include "BrowserHandler.h"
#include "Browser.h"

#include <iostream>
#include <sstream>
#include <ipc/core.h>

#if QT_VERSION < 0x050000
  #include <QPlastiqueStyle>
#endif

using namespace PixDbBrowser;

int main(int argnum,char** args)
{
#if QT_VERSION < 0x050000
    QApplication::setStyle(new QPlastiqueStyle());
#else
    QApplication::setStyle("fusion");
#endif

  IPCCore::init(argnum,args);
  BrowserHandler* app=new BrowserHandler(argnum,args);
  Browser *brw = new Browser(*app);
  brw->show();

  int ret = 0;
  std::stringstream msg;
  try{
    ret  = app->exec();
  } catch(std::exception& s){
    msg << "Std-lib exception \"";
    msg << s.what();
  } catch(...){
    msg << "Unknown exception \"";
  }
  if(msg.str()!="")
    std::cerr << msg.str() << "\" not caught during execution of main window." << std::endl;

  // cleaning up
  delete brw;
  delete app;

  return ret;
}
