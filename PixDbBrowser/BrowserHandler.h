#ifndef _BrowserHandler_h_
#define _BrowserHandler_h_

#include <vector>
#include <map>
#include <string>
#include <QApplication>

namespace PixDbBrowser
{

  enum RevContentIndices{kObjLabel=0, kObjType, kDomain, kTag, kPenTag, kRev, kMaxNrev};

class DbConnection;

class BrowserHandler : public QApplication
{
  Q_OBJECT
    
 public:
  
  BrowserHandler(int& argnum,char* args[]);

  std::string getPartName(){return m_partitionName;}
  void setPartName(std::string name){m_partitionName = name;}// add precaution and check list?
  void getTagNames(std::string &idname, std::string &connname, std::string &cfgname, std::string &modcfgname){
    idname=m_idTag; cfgname=m_cfgTag; modcfgname=m_modCfgTag; connname=m_connTag;};
  void setTagNames(std::string idname, std::string connname, std::string cfgname, std::string modcfgname){
    m_idTag=idname; m_cfgTag=cfgname; m_modCfgTag=modcfgname; m_connTag=connname;};
  void getPartList(std::vector<std::string> &list);
  void getTagsList(std::vector<std::string> &idList, std::map<std::string, std::vector<std::string> > &connList,
		   std::map<std::string, std::vector<std::string> > &cfgList,
		   std::map<std::string, std::vector<std::string> > &modCfgList);
  void loadConn();
  const std::vector<unsigned int>& getRevisionList(std::string domainName,std::string tag,std::string objName);	

  DbConnection& getDbConn(){return *m_dbConn;};

 private:
  std::string m_partitionName, m_idTag, m_cfgTag, m_modCfgTag, m_connTag;
  DbConnection *m_dbConn;

};

}

#endif

