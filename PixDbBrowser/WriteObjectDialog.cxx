#include "WriteObjectDialog.h"
#include "BrowserHandler.h"

#include <QWidget>
#include <QLabel>
#include <QCheckBox>
#include <QLineEdit>
#include <QPushButton>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QIcon>

using namespace PixDbBrowser;

WriteObjectDialog::WriteObjectDialog(QStringList revInfo, QWidget* parent) : QDialog(parent) {

  //	this->revision=rev;
  setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
  
  QGridLayout* layout=new QGridLayout(this);
  layout->setColumnMinimumWidth(2,20);

  layout->addWidget(new QLabel("<b>Object Information</b>",this),1,1,1,2);
  layout->addWidget(new QLabel("Name:",this),2,1);
 
  QLabel* objLbl=new QLabel(revInfo.at(kObjLabel),this);
  layout->addWidget(objLbl,2,3);
  layout->addWidget(new QLabel("Type:",this),3,1);
  layout->addWidget(new QLabel(revInfo.at(kObjType),this),3,3);
  
  layout->addWidget(new QLabel("<b>Database Information</b>",this),4,1,1,2);
  layout->setRowMinimumHeight(4,15);
  layout->addWidget(new QLabel("Domain:",this),5,1);
  
  QLabel* domainLbl=new QLabel(revInfo.at(kDomain),this);
  layout->addWidget(domainLbl,5,3);
  
  layout->addWidget(new QLabel("Tag:",this),6,1);
  
  QLabel* tagLbl=new QLabel(revInfo.at(kTag),this);
  layout->addWidget(tagLbl,6,3);
  
  layout->addWidget(new QLabel("Pending tag:",this),7,1);
  
  QLabel* pendTagLbl=new QLabel(revInfo.at(kPenTag),this);
  layout->addWidget(pendTagLbl,7,3);
  
  layout->addWidget(new QLabel("New pending tag:",this),8,1);
  
  pendingTag=new QLineEdit("_Tmp",this);
  layout->addWidget(pendingTag,8,3);
  
  layout->setRowMinimumHeight(9,15);

  okButton=new QPushButton("Ok",this);
  layout->addWidget(okButton,10,1);
  
  cancelButton=new QPushButton("Cancel",this);
  layout->addWidget(cancelButton,10,3);
  
  connect(okButton,SIGNAL(clicked()),this,SLOT(accept()));
  connect(cancelButton,SIGNAL(clicked()),this,SLOT(reject()));
  
  setWindowTitle("Write object");
  setWindowIcon(QIcon(":/icon_main.png"));

}

std::string WriteObjectDialog::getPendingTag()
{
    return (this->pendingTag->text()).toStdString(); // .toStdString() added by MJK
}
