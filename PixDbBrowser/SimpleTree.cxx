#include "SimpleTree.h"

#include <QStringList>
#include <QString>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QApplication>
#include <QLabel>
#include <QTreeWidget>

using namespace PixDbBrowser;

SimpleTree::SimpleTree(QWidget *parent, QString title, QStringList headers) : QFrame(parent) {
  QVBoxLayout *layout = new QVBoxLayout(this);
  QLabel* lbl=new QLabel(title,this);
  layout->addWidget(lbl);
  m_tree = new QTreeWidget(this);
  m_tree->setColumnCount(headers.count());
  m_tree->setHeaderLabels(headers);
  layout->addWidget(m_tree);
  connect(m_tree, SIGNAL(currentItemChanged(QTreeWidgetItem*, QTreeWidgetItem*)), 
	  this, SLOT(processSelection(QTreeWidgetItem*, QTreeWidgetItem*)));
}
void SimpleTree::updateList(std::vector< std::vector<QString> > itemsText){
  m_tree->clear();
  QTreeWidgetItem *selItem=0;
  for(unsigned int i=0;i<itemsText.size();i++){
    if(itemsText.at(i).size()!=(unsigned int)m_tree->columnCount()) return;
    QTreeWidgetItem *item = new QTreeWidgetItem(m_tree);
    for(unsigned int j=0;j<itemsText.at(i).size();j++){
      item->setText(j, itemsText.at(i).at(j));
      if(j==(itemsText.at(i).size()-1))
	selItem = item;
    }
  }
  for(int i=0;i<m_tree->columnCount();i++)
    m_tree->resizeColumnToContents(i);
  if(selItem!=0){
    selItem->setSelected(true);
    processSelection(selItem, 0);
  }
}
void SimpleTree::processSelection(QTreeWidgetItem *item, QTreeWidgetItem *){
  if(item!=0){
    QStringList itemText;
    for(int i=0;i<m_tree->columnCount();i++)
      itemText<<item->text(i);
    emit itemSelected(itemText);
  }
}
