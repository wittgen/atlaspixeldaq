#include <CfgViewer.h>
#include <ConfigViewer.h>

using namespace PixDbBrowser;

CfgViewer::CfgViewer(QWidget *parent) : QFrame(parent) {
  m_layout = new QVBoxLayout(this);
  m_baseViewer=0;
  setMinimumHeight(400);
}
void CfgViewer::update(PixLib::Config& cfg){
  if(m_baseViewer) m_layout->removeWidget(m_baseViewer);
  delete m_baseViewer;
  m_baseViewer = new configViewer("Revision content", cfg, 0);
  m_baseViewer->m_closeButton->hide();
  m_layout->addWidget(m_baseViewer);
}
