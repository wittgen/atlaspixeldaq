#include "ObjectListView.h"
#include "DbConnection.h"
#include "BrowserHandler.h"

#include <QVBoxLayout>
#include <QListWidget>
#include <QTabWidget>
#include <QGroupBox>
#include <QStringList>
#include <QVariant>
#include <QApplication>

using namespace PixDbBrowser;

ObjectListView::ObjectListView(QWidget *parent, BrowserHandler &handler) : QFrame(parent), m_handler(handler) {
  QVBoxLayout *layout = new QVBoxLayout(this);
  m_tabs=new QTabWidget(this);
  layout->addWidget(m_tabs);
  QGroupBox *gb = dynamic_cast<QGroupBox*>(parent);
  if(gb!=0) gb->setLayout(layout);
  m_tabs->setEnabled(true);//false);
  m_tabs->setMinimumHeight(350);
}
void ObjectListView::updateContent(){
  DbConnection &dbConn = m_handler.getDbConn();
  for(int i=0;i<m_tabs->count();i++){
    QListWidget* list = dynamic_cast<QListWidget*>(m_tabs->widget(i));
    if(list!=0){
      disconnect(list, SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)), this,
		 SLOT(itemSelected(QListWidgetItem*, QListWidgetItem*)));
      delete list;
    }
  }
  m_tabs->clear();
  /* org. code, but will not work: have to select tags first (as is done now) and 
     can list only them in the tree view
  const std::vector<std::string>& domainList=dbConn.getDomainList();
  for(unsigned int i=0; i<domainList.size(); i++){
    const std::vector<std::string>& tagList=dbConn.getTagList(domainList.at(i).c_str());
    for (unsigned int j=0;j<tagList.size();j++){
  */
  std::string idname, connname, cfgname, modcfgname;
  m_handler.getTagNames(idname, connname, cfgname, modcfgname);
  for(unsigned int i=0;i<2;i++){
    std::string domName = "Configuration-"+idname;
    if(i) domName = "Connectivity-"+idname;
    std::vector<std::string> tagNames;
    if(i){
      tagNames.push_back(connname);
    } else {
      tagNames.push_back(cfgname);
      if(cfgname!=modcfgname) tagNames.push_back(modcfgname);
    }
    for(unsigned int j=0;j<tagNames.size();j++){
      const std::vector<std::pair<std::string,unsigned int> >& types=dbConn.getTypes(domName, tagNames.at(j));
      for (unsigned int k=0;k<types.size();k++){
	QListWidget* list = new QListWidget();
	int id = m_tabs->addTab(list, QString(types.at(k).first.c_str()));
	m_tabs->setTabEnabled(id, true);
	connect(list, SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)), this,
		SLOT(itemSelected(QListWidgetItem*, QListWidgetItem*)));
	const std::vector<std::string>& objList=dbConn.getObjectList(domName, tagNames.at(j),types.at(k).first);
	for(unsigned int n=0; n<objList.size(); n++){
	  QListWidgetItem *item = new QListWidgetItem(objList.at(n).c_str(), (QListWidget*)m_tabs->widget(id));
	  QStringList slist;
	  slist.insert(kObjLabel, QString(objList.at(n).c_str()));
	  slist.insert(kObjType, QString(types.at(k).first.c_str()));
	  slist.insert(kDomain, QString(domName.c_str()));
	  slist.insert(kTag, QString(tagNames.at(j).c_str()));
	  slist.insert(kRev,"");
	  slist.insert(kPenTag,"");
	  QVariant var(slist);
	  item->setData(Qt::UserRole, var);
	}
      }
    }
  }
  adjustSize();
}
void ObjectListView::selectTab(QString name){
  QListWidget* list = 0;
  for(int i=0;i<m_tabs->count();i++){
    if(m_tabs->tabText(i)==name){
      list = dynamic_cast<QListWidget*>(m_tabs->widget(i));
      m_tabs->setCurrentWidget(list);
      QApplication::processEvents();
      break;
    }
  }
  if(list!=0 && list->count()>0){
    if(list->currentRow()==0) itemSelected(list->currentItem(), 0);
    else list->setCurrentRow(0);
  }
}
void ObjectListView::itemSelected(QListWidgetItem *item, QListWidgetItem*){
  if(item!=0){
    QVariant var = item->data(Qt::UserRole);
    if(var.type()==QVariant::StringList){
      QStringList slist = var.toStringList();
      if(slist.size()==kMaxNrev) emit dbObjSelected(slist);
    }
  }
}
