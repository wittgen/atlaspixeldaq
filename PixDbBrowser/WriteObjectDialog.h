#ifndef _WriteObjectDialog_h_
#define _WriteObjectDialog_h_

#include <QDialog>
#include <QCheckBox>

class QStringList;
class QLineEdit;
class QPushButton;

namespace PixDbBrowser
{

  class WriteObjectDialog : public QDialog
  {
    
    Q_OBJECT
  public:
    WriteObjectDialog(QStringList revInfo, QWidget* parent=0);
    std::string getPendingTag();

  private:
    QLineEdit *pendingTag;
    QPushButton *browseButton,*okButton,*cancelButton;
    
  };

}

#endif
