#include "Browser.h"
#include "BrowserHandler.h"
#include "StartLogo.h"
#include "DbConnection.h"
#include "ConnectivityView.h"
#include "ObjectListView.h"
#include "ObjectView.h"

#include <SelectPanel.h>

#include <QGridLayout>
#include <QSplitter>
#include <QGroupBox>
#include <QMenuBar>
#include <QMenu>
#include <QTreeWidget>
#include <QIcon>
#include <QTimer>
#include <QStringList>
#include <QVBoxLayout>

#include <iostream>
#include <unistd.h>

using namespace PixDbBrowser;

Browser::Browser(BrowserHandler &handler) : QWidget(0), m_handler(handler) {
  m_mainLayout=new QGridLayout(this);
  m_mainLayout->setRowMinimumHeight(0,20);
  m_mainLayout->setRowMinimumHeight(1,500);
  m_mainLayout->setRowStretch(1,20);
  m_mainLayout->setContentsMargins(5,5,5,5);	

  // set up menus
  QMenuBar *menuBar = new QMenuBar(this);
  m_mainLayout->addWidget(menuBar);

  QMenu *fileMenu = menuBar->addMenu("File");
  fileMenu->addAction("Change partition",this,SLOT(changePartition()));
  fileMenu->addAction("Change tags",this,SLOT(changeTagsPrep()));
  fileMenu->addSeparator();
  fileMenu->addAction("Exit",this,SLOT(close()));

  // split up main panel
  m_hSplitter=new QSplitter(Qt::Horizontal,this);
  m_mainLayout->addWidget(m_hSplitter,1,0);
  m_vSplitter=new QSplitter(Qt::Vertical,m_hSplitter);

  // left part of panel
  QGroupBox* connGroup=new QGroupBox();
  connGroup->setTitle("Connectivity tree");
  m_connTree = new ConnectivityView(connGroup, m_handler);
  m_vSplitter->addWidget(connGroup);
  QGroupBox* listGroup=new QGroupBox();
  listGroup->setTitle("List of objects");
  m_objList = new ObjectListView(listGroup, m_handler);
  m_vSplitter->addWidget(listGroup);
  m_vSplitter->adjustSize();

  // right part of panel
  // dummy
  QTabWidget *tabs = new QTabWidget(m_hSplitter);
  m_cfgViewer = new ObjectView(0, m_handler);
  tabs->addTab((QWidget*)m_cfgViewer, "Object details");
//   tabs->addTab(new QWidget(), "Occurences");
//   tabs->addTab(new QWidget(), "Modifications");
//   tabs->addTab(new QWidget(), "Tag loader");
  tabs->setMinimumWidth(700);
  m_hSplitter->adjustSize();
  // import ObjectView menu to menu bar
  menuBar->addMenu(m_cfgViewer->m_actMenu);

  connect(m_cfgViewer, SIGNAL(tagsChanged()), this, SLOT(changeTagsPrep()) );
  connect(m_connTree, SIGNAL(typeSelected(QString)), m_objList, SLOT(selectTab(QString)));
  connect(m_objList, SIGNAL(dbObjSelected(QStringList)), m_cfgViewer, SLOT(updateView(QStringList)));

  setWindowIcon(QIcon(":/icon_main.png"));
  setWindowTitle("PixDbBrowser");
  resize(1000,750);

  m_timer=new QTimer(this);
  m_timer->setSingleShot(true);

  m_sl = new StartLogo();
  m_sl->hide();
  m_singleTagNoSel = false;

  QApplication::processEvents();
  if(m_handler.getPartName()=="") changePartition();
  else {
    m_singleTagNoSel = true;
    setWindowTitle(("PixDbBrowser - Partition "+m_handler.getPartName()).c_str());
    connect(m_timer, SIGNAL( timeout() ), this, SLOT( changeTagsPrep() ) );
    m_timer->start(400);
  }
}

void Browser::changePartition(){
  std::vector< std::vector<std::string> > list(1);
  m_handler.getPartList(list[0]);
  std::vector<std::string> labels, selection;
  labels.push_back("Choose partition: ");

  SelectPanel panel(this, labels, list);
  if(panel.exec()==QDialog::Accepted){
    panel.getSelection(selection);
    m_handler.setPartName(selection.at(0));
    setWindowTitle(("PixDbBrowser - Partition "+m_handler.getPartName()).c_str());
    changeTagsPrep();
  }
}
void Browser::changeTagsPrep(){
  disconnect(m_timer, SIGNAL( timeout() ), this, SLOT( changeTagsPrep() ) );
  m_sl->setText("Loading tags...");
  m_sl->setMax(5);
  m_sl->setCurrent(0);
  m_sl->updateProgressBar();
  m_sl->setWindowFlags(m_sl->windowFlags() | Qt::WindowStaysOnTopHint);;
  m_sl->show();
  connect(m_timer,SIGNAL(timeout()),this,SLOT(changeTags()));
  m_timer->start(250);
}
void Browser::changeTags(){
  disconnect(m_timer, SIGNAL( timeout() ), this, SLOT( changeTags() ) );
  //disconnect(m_connTree, SIGNAL(typeSelected(QString)), m_objList, SLOT(selectTab(QString)));
  //disconnect(m_objList, SIGNAL(dbObjSelected(QStringList)), m_cfgViewer, SLOT(updateView(QStringList)));
  std::vector<std::string> idList;
  std::map< std::string, std::vector<std::string> > connList, cfgList,modCfgList;
  m_handler.getTagsList(idList, connList, cfgList, modCfgList);

  m_sl->setCurrent(1);
  m_sl->updateProgressBar();
  QApplication::processEvents();

  bool proceed = false;
  if(idList.size()==1 && cfgList.find(idList.at(0))!=cfgList.end() && modCfgList.find(idList.at(0))!=modCfgList.end() &&
     connList.find(idList.at(0))!=connList.end() && connList[idList.at(0)].size()==1 &&
     cfgList[idList.at(0)].size()==1 && modCfgList[idList.at(0)].size()==1 && m_singleTagNoSel){
    m_handler.setTagNames(idList.at(0), connList[idList.at(0)].at(0), cfgList[idList.at(0)].at(0), modCfgList[idList.at(0)].at(0));
    m_singleTagNoSel = false;
    proceed = true;
  } else {
    std::vector<std::string> labels, selection;
    labels.push_back("Choose ID Tag: ");
    labels.push_back("Choose Conn. Tag: ");
    labels.push_back("Choose Cfg. Tag: ");
    labels.push_back("Choose Module Cfg. Tag: ");
    std::map<std::string, std::vector< std::vector<std::string> > > otherBoxItems;
    for(std::vector<std::string>::iterator it=idList.begin(); it!=idList.end(); it++){
      if(connList.find(*it)!=connList.end() && cfgList.find(*it)!=cfgList.end() && modCfgList.find(*it)!=modCfgList.end()){
	std::vector< std::vector<std::string> > tmpVec;
	tmpVec.push_back(connList[*it]);
	tmpVec.push_back(cfgList[*it]);
	tmpVec.push_back(modCfgList[*it]);
	otherBoxItems.insert(std::make_pair(*it, tmpVec));
      }
    }
    m_sl->setWindowFlags(m_sl->windowFlags() & ~Qt::WindowStaysOnTopHint);;
    m_sl->hide();
    SelectPanel panel(this, labels, idList, otherBoxItems);
    if(panel.exec()==QDialog::Accepted){
      panel.getSelection(selection);
      m_handler.setTagNames(selection.at(0), selection.at(1), selection.at(2), selection.at(3));
      proceed = true;
    }
    m_singleTagNoSel = false;
  }
  if(proceed) loadPartition();
}
void Browser::loadPartition(){
  m_sl->setText("Loading connectivity list...");
  m_sl->setCurrent(2);
  m_sl->updateProgressBar();
  QApplication::processEvents();
  m_handler.loadConn();

  m_sl->setText("Filling object list...");
  m_sl->setCurrent(3);
  m_sl->updateProgressBar();
  QApplication::processEvents();
  m_objList->updateContent();

  m_sl->setText("Filling connectivity list...");
  m_sl->setCurrent(2);
  m_sl->updateProgressBar();
  QApplication::processEvents();
  m_connTree->updateContent();

  m_vSplitter->adjustSize();

  m_sl->setText("Done!");
  m_sl->setCurrent(4);
  m_sl->updateProgressBar();
  QApplication::processEvents();

  usleep(300);
  m_sl->setWindowFlags(m_sl->windowFlags() & ~Qt::WindowStaysOnTopHint);
  m_sl->hide();
}
