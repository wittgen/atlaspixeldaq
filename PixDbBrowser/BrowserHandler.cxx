#include "BrowserHandler.h"

#include "DbConnection.h"
#include "Exception.h"

#include <ipc/partition.h>
#include <string>

using namespace PixDbBrowser;

BrowserHandler::BrowserHandler(int& argnum,char* args[]) : QApplication(argnum,args), m_dbConn(0)
{
  for(int i = 1 ; i < argnum ; ++i) {
    if( std::string(args[i]) == "-p") {
      if( i+1 <= argnum ) m_partitionName = std::string(args[i+1]);
      else {
        std::cerr << "Error: you didn't specify the partition name." << std::endl;
        this->exit(1);
      }
    }
  }
  std::vector<std::string> partList;
  std::vector<std::string>::iterator it;
  getPartList(partList);

  // We need the partition list in case the argument provided doesn't match an active partition
  if(m_partitionName!="") {
    for (it=partList.begin();it!=partList.end();++it)
      if(*it == m_partitionName) { break; } // The partition provided is active
  } else {
    for (it=partList.begin();it!=partList.end();++it){
      if(m_partitionName=="") m_partitionName = *it;
      else { m_partitionName = ""; break; } // We have more than one choice ; let the user pick!
    }
  }

  if(m_partitionName != "") std::cout << "Partition name: " << m_partitionName << std::endl;

}
void BrowserHandler::getPartList(std::vector<std::string> &list){
  std::list<IPCPartition> partList;
  IPCPartition::getPartitions(partList);
  std::list<IPCPartition>::iterator it;
  for (it=partList.begin();it!=partList.end();++it){
    if(it->name().compare(0,3, "Pix")==0)
      list.push_back(it->name());
  }
  return;
}
void BrowserHandler::getTagsList(std::vector<std::string> &idList, std::map< std::string, std::vector<std::string> > &connMap,
				 std::map<std::string, std::vector<std::string> > &cfgMap,
				 std::map<std::string, std::vector<std::string> > &modCfgMap){
  if(m_partitionName!=""){
    delete m_dbConn; m_dbConn = 0;
    try{
      m_dbConn=new DbConnection(m_partitionName, std::string("PixelDbServer"),std::string("PixelRunParams"));
      m_dbConn->connect();
      m_dbConn->loadDbContent();

      // get ID tag
      std::vector<std::string> tmp_idList;
      const std::vector<std::string>& domainList=m_dbConn->getDomainList();
      
      for (unsigned int i=0;i<domainList.size();i++) {
	if(domainList.at(i).substr(0,14)=="Configuration-")
	  tmp_idList.push_back(domainList.at(i).substr(14,domainList.at(i).length()));
	if(domainList.at(i).substr(0,13)=="Connectivity-") 	
	  tmp_idList.push_back(domainList.at(i).substr(13,domainList.at(i).length()));
      }
      
      std::vector<std::string> tmp_idList2=tmp_idList;
      for (unsigned int i=0;i<tmp_idList.size();i++) {
	for (unsigned int j=0;j<tmp_idList2.size();j++) {
	  if(tmp_idList2.at(j)==tmp_idList.at(i) && j>i){
	    idList.push_back(tmp_idList2.at(j));
	    // get conn. tags for each ID tag
	    std::vector<std::string> connList=m_dbConn->getTagList("Connectivity-"+tmp_idList2.at(j));
	    connMap.insert(std::make_pair(tmp_idList2.at(j), connList));
	    // get cfg. tags for each ID tag
	    std::vector<std::string> rawConfList=m_dbConn->getTagList("Configuration-"+tmp_idList2.at(j));
	    std::vector<std::string> confList, modConfList;
	    for(std::vector<std::string>::iterator it=rawConfList.begin(); it!=rawConfList.end(); it++){
	      bool foundMod=false, foundNonMod=false;
	      const std::vector<std::pair<std::string,unsigned int> >& types=m_dbConn->getTypes(domainList.at(i), *it);
	      for (unsigned int k=0;k<types.size();k++){
		if(types.at(k).first=="PixModule")  foundMod = true;
		if(types.at(k).first!="PixModule")  foundNonMod = true;
		if(foundMod && foundNonMod) break; // all covered, no need to keep looping
	      }
	      if(foundMod)
		modConfList.push_back(*it);
	      if(foundNonMod)
		confList.push_back(*it);
	    }
	    modCfgMap.insert(std::make_pair(tmp_idList2.at(j), modConfList));
	    cfgMap.insert(std::make_pair(tmp_idList2.at(j), confList));
	  }
	}		  
      }
    }
    catch (Exception e) {
      std::cerr << "BrowserHandler::getTagsList : caught exception " << e.message() << std::endl;
    } catch(std::exception& s){
      std::cerr << "BrowserHandler::getTagsList : caught std-lib exception " << s.what() << std::endl;
    } catch(...){
      std::cerr << "BrowserHandler::getTagsList : caught Unknown exception "<< std::endl;
    }
  } else
    std::cerr << "BrowserHandler::getTagsList called with empty partition name" << std::endl;
}
void BrowserHandler::loadConn(){
  if(m_idTag=="" || m_connTag=="" || m_cfgTag=="" || m_modCfgTag==""){
    std::cerr << "BrowserHandler::loadConn called with empty tag name" << std::endl;
    return;
  }
  try{
    if(m_dbConn==0){
      m_dbConn=new DbConnection(m_partitionName, std::string("PixelDbServer"),std::string("PixelRunParams"));
      m_dbConn->connect();
      m_dbConn->loadDbContent();
    }
    m_dbConn->connectPixConn(m_idTag, m_connTag, m_cfgTag,m_modCfgTag);
  }
  catch (Exception e) {
    std::cerr << "BrowserHandler::loadConn : caught exception " << e.message() << std::endl;
  } catch(...){
    std::cerr << "BrowserHandler::loadConn : caught Unknown exception "<< std::endl;
  }
}
const std::vector<unsigned int>& BrowserHandler::getRevisionList(std::string domainName,std::string tag,std::string objName){
  static std::vector<unsigned int> emptyList;
  if(m_idTag=="" || m_connTag=="" || m_cfgTag=="" || m_modCfgTag==""){
    std::cerr << "BrowserHandler::getRevisionList called with empty tag names" << std::endl;
    return emptyList;
  }
  if(m_dbConn==0){
    std::cerr << "BrowserHandler::getRevisionList called without existing DbConnection" << std::endl;
    return emptyList;
  }
  try{
    return m_dbConn->getRevisionList(domainName,tag,objName);
  } catch (Exception e) {
    std::cerr << "BrowserHandler::loadConn : caught exception " << e.message() << std::endl;
  } catch(...){
    std::cerr << "BrowserHandler::loadConn : caught Unknown exception "<< std::endl;
  }
  return emptyList;
}

