#ifndef _CfgViewer_h_
#define _CfgViewer_h_

#include <QFrame>

class configViewer;
class QVBoxLayout;

namespace PixLib
{
  class Config;
}
namespace PixDbBrowser
{
  class CfgViewer : public QFrame {
    
    Q_OBJECT
      
  public:
    CfgViewer(QWidget *parent);

  public slots:
    void update(PixLib::Config& cfg);

  private:
    configViewer *m_baseViewer;
    QVBoxLayout * m_layout;
  };
}
#endif // _CfgViewer_h_
