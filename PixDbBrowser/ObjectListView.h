#ifndef _ObjectListView_h_
#define _ObjectListView_h_

#include <QFrame>

class QTabWidget;
class QListWidgetItem;
class QStringList;

namespace PixDbBrowser
{
  class BrowserHandler;

  class ObjectListView : public QFrame {

    Q_OBJECT

  public:
    ObjectListView(QWidget *parent, BrowserHandler &handler);
    ~ObjectListView(){};

  public slots:
    void updateContent();
    void selectTab(QString name);
    void itemSelected(QListWidgetItem*, QListWidgetItem*);

  signals:
    void dbObjSelected(QStringList);

  private:
    QTabWidget *m_tabs;
    BrowserHandler &m_handler;
  };
}
#endif // _ObjectListView_h_

