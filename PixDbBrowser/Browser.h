#ifndef _Browser_h_
#define _Browser_h_

#include <QWidget>

class QSplitter;
class QGridLayout;
class QTimer;
class QTreeWidget;
class QStringList;

namespace PixDbBrowser
{

  class StartLogo;
  class BrowserHandler;
  class ConnectivityView;
  class ObjectListView;
  class ObjectView;
  
  class Browser : public QWidget
  {
    Q_OBJECT
      
  public:
    Browser() = delete;
    Browser(BrowserHandler &handler);
    ~Browser() = default;
    
  public slots:
    void changePartition();
    void changeTagsPrep();
    void changeTags();
    void loadPartition();

  private:
    QGridLayout *m_mainLayout;
    QSplitter *m_hSplitter, *m_vSplitter;
    BrowserHandler &m_handler;
    StartLogo *m_sl;
    bool m_singleTagNoSel;
    QTimer *m_timer;
    ConnectivityView *m_connTree;
    ObjectListView *m_objList;
    ObjectView *m_cfgViewer;
  };

}

#endif // _Browser_h_
