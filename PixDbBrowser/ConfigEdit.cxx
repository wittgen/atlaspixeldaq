#include "BrowserHandler.h"
#include "ConfigEdit.h"
#include "DbConnection.h"
#include "WriteObjectDialog.h"

#include <OptionsFrame.h>
#include "ConfigEditI3.h"
#include "ConfigEditI4.h"

#include <Config/Config.h>
#include <Config/ConfGroup.h>
#include <Config/ConfObj.h>
#include <PixConnectivity/PixConnectivity.h>
#include <PixModule/PixModule.h>

#include <QStringList>
#include <QString>
#include <QVBoxLayout>
#include <QLabel>
#include <QDateTime>
#include <QPushButton>
#include <QIcon>

using namespace PixDbBrowser;

ConfigEdit::ConfigEdit(PixLib::Config &config, QStringList objPars, QWidget *parent) : 
  QDialog(parent), m_objPars(objPars), m_config(config){
  QVBoxLayout *mainLayout = new QVBoxLayout(this);

  QHBoxLayout *lbLayout = new QHBoxLayout( 0 );
  QGridLayout *labelGrid = new QGridLayout();
  QLabel* selObjLbl=new QLabel(objPars.at(kObjLabel),this);
  selObjLbl->setFont(QFont("Arial",12,QFont::Bold));
  labelGrid->addWidget(selObjLbl,0,1,2,2);
  QLabel* objTypeLbl=new QLabel(objPars.at(kObjType),this);
  QLabel* domainLbl=new QLabel(objPars.at(kDomain),this);
  QLabel* tagLbl=new QLabel(objPars.at(kTag),this);
  QLabel* revLbl=new QLabel(objPars.at(kRev),this);
  QLabel* ptagLbl=new QLabel(objPars.at(kPenTag),this);
  QLabel* l1=new QLabel("Object type:",this);
  labelGrid->addWidget(l1,2,1);
  labelGrid->addWidget(objTypeLbl,2,3);
  QLabel* l2=new QLabel("Domain:",this);
  labelGrid->addWidget(l2,3,1);
  labelGrid->addWidget(domainLbl,3,3);
  QLabel* l3=new QLabel("Tag:",this);
  labelGrid->addWidget(l3,4,1);
  labelGrid->addWidget(tagLbl,4,3);
  QLabel* l5=new QLabel("Revision:",this);
  labelGrid->addWidget(l5,6,1);
  labelGrid->addWidget(revLbl,6,3);	
  QLabel* l6=new QLabel("Pending tag:",this);
  labelGrid->addWidget(l6,7,1);
  labelGrid->addWidget(ptagLbl,7,3);	
  lbLayout->addLayout(labelGrid);

  QSpacerItem *vspacer = new QSpacerItem( 0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum );
  lbLayout->addSpacerItem(vspacer);

  QVBoxLayout *blayout = new QVBoxLayout( 0 );
  QSpacerItem *bspacer = new QSpacerItem( 0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding );
  blayout->addSpacerItem(bspacer);
  QPushButton *okb = new QPushButton("Write to DB", this);
  blayout->addWidget(okb);
  connect(okb, SIGNAL(pressed()), this, SLOT(writeToDb()));
  QPushButton *cab = new QPushButton("Cancel", this);
  blayout->addWidget(cab);
  connect(cab, SIGNAL(pressed()), this, SLOT(reject()));
  lbLayout->addLayout(blayout);

  QVBoxLayout *vlayout = new QVBoxLayout( 0 );
  vlayout->addLayout(lbLayout);

  QLabel* lcfgedt=new QLabel("Revision content",this);
  lcfgedt->setFont(QFont("Arial",10,QFont::Bold));
  vlayout->addWidget(lcfgedt);

  m_edt=0;
  m_edtI3 = 0;
  m_edtI4 = 0;
  m_pm = 0;

  if(objPars.at(kObjType)=="PixModule" && config["general"].name()!= "__TrashConfGroup__" && 
     config["general"]["FE_Flavour"].name()!="__TrashConfObj__" && config["general"]["MCC_Flavour"].name()!="__TrashConfObj__"){
    int nFEs=0;
    for(int chip=0;chip<(int)config.subConfigSize();chip++){
      PixLib::Config &feconf = config.subConfig(chip);
      PixLib::Config &globreg = feconf.subConfig("GlobalRegister_0/GlobalRegister");
      if(globreg.name()!="__TrashConfig__" && globreg["GlobalRegister"].name()!="__TrashConfGroup__") nFEs++;
    }
    int mccf = ((PixLib::ConfList&)config["general"]["MCC_Flavour"]).getValue();
    int fef = ((PixLib::ConfList&)config["general"]["FE_Flavour"]).getValue();
    m_pm = new PixLib::PixModule("Base", "Base", "Base", mccf, fef, nFEs);
    m_pm->config() = config;
    if(((PixLib::ConfList&)config["general"]["FE_Flavour"]).sValue()=="FE_I1" ||
       ((PixLib::ConfList&)config["general"]["FE_Flavour"]).sValue()=="FE_I2") {
      m_edtI3 = new ConfigEditI3(m_pm->config(), this);
      vlayout->addWidget(m_edtI3);
    } else {
      m_edtI4 = new ConfigEditI4(m_pm->config(), this);
      vlayout->addWidget(m_edtI4);
    }
  } else {
    m_edt = new optionsFrame(config, this, true);
    vlayout->addWidget(m_edt);
  }

  mainLayout->addLayout(vlayout);

  setWindowTitle("Object Config Editor");
  setWindowIcon(QIcon(":/icon_main.png"));
}
void ConfigEdit::writeToDb(){
  if(m_edt!=0)
    m_edt->save(); // this writew back changes to Config object in DB
  else if(m_edtI3!=0){
    m_edtI3->StoreCfg();
    m_config = m_pm->config(); // this writew back changes to Config object in DB
  } else if(m_edtI4!=0) {
    m_edtI4->storeCfg();
    m_config = m_pm->config(); // this writes back changes to Config object in DB
  } else
    return; // should never happen
  delete m_pm; // no longer needed
  WriteObjectDialog wod(m_objPars, this);
  if(wod.exec()==QDialog::Accepted){
    m_pendingTag = wod.getPendingTag();
    accept();
  }
}
