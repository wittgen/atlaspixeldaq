#ifndef _DbConnection_h_
#define _DbConnection_h_

#include <map>
#include <vector>
#include <string>

#include "PixDbServer/PixDbDomain.h"

class IPCPartition;
namespace PixLib{
  class PixDbServerInterface;
  class	PixConnectivity;
  class PixISManager;
  class Config;
  class PixModule;
  class PixModuleGroup;
  class TimPixTrigController;
  class PixDisable;
  class PixRunConfig;
}

namespace PixDbBrowser
{

  class Object;
  
  struct ModifiedObject
  {
    std::string domain,tag,type,name;
    unsigned int revision;
  };
  
  class DbConnection
  {
    
  public:
    
    DbConnection(std::string partition,std::string dbServer,std::string isServer);
    ~DbConnection();
    
    void connect();
    void connectPixConn();
    void connectPixConn(std::string idTag,std::string connTag,std::string cfgTag,std::string cfgModTag);
    
    void loadDbContent();
    
    void EnableRem();
    std::vector<std::string> getIdTagList();
    
    const std::vector<std::string>& getDomainList();
    const std::vector<std::string>& getTagList(std::string domName);
    const std::vector<std::string>& getObjectList(std::string domainName,std::string tag);
    const std::vector<std::string>& getObjectList(std::string domainName,std::string tag,std::string type);
    const std::vector<std::pair<std::string,unsigned int> >& getTypes(std::string domain,std::string tag);
    const std::vector<unsigned int>& getRevisionList(std::string domainName,std::string tag,std::string objName);	
    const std::string getPendingTag(std::string domainName,std::string tag,std::string objName,unsigned int revision);
    
    const std::string getPartitionName();
    
    PixLib::PixDbDomain::rev_content getRevision(std::string domain,std::string tag,std::string obj,unsigned int revId);
    
    const std::string getPendingTag(std::string domain,std::string tag,std::string type,std::string obj,unsigned int revId);
    void cloneTag(std::string domain,std::string oldTag,std::string tag,std::string pendTag);
    void removeTag(std::string domain,std::string tag);
    void removePendingTag(std::string domain,std::string pendTag,std::string tag);
    void setPendingTag(std::string domain,std::string tag,std::string object,std::string pendTag,unsigned int revision);
    void setPendingTagAll(std::string domain,std::string tag,std::string pendTag,unsigned int revision);
    void setPendingTagAllTags(std::string domain,std::string pendTag,unsigned int revision);
    void renamePendingTag(std::string domain,std::string tag,std::string type,std::string oldPendTag,std::string newPendTag);
    void writeObject(std::string domain,std::string tag,std::string type,std::string object,PixLib::PixDbDomain::rev_content& rev,unsigned int revision,bool expert);
    void writeObject(std::string domain,std::string tag,std::string type,std::string object, std::string pendTag);
    void deleteTagList(std::vector<std::pair<std::string,std::string> >& tagList);
    
    std::vector<std::string>* getLoadList();
    std::vector<std::string>* getSaveList();
    unsigned int getLoadListLen();
    
    Object* getObjectFromLoadList(std::string id);
    Object* getObjectFromSaveList(std::string id);

    PixLib::Config& getObjectConfig(std::string domainName,std::string tag,std::string objName,std::string objType, unsigned int rev);
    
    void restoreLoadList(std::string id);
    void cleanLoadList(std::string id);
    void restoreSaveList(std::string id);
    void cleanSaveList(std::string id);
    void updateSaveList();
    
    void addLoadList(Object* obj,unsigned int revision);

    
    PixLib::PixDbServerInterface* getDbServer() {return this->dbServer;}
    PixLib::PixConnectivity* getPixConnectivity() {return this->pixConn;} 
	
    const std::vector<ModifiedObject>& getModifiedObjects(std::string domain,std::string tag,std::string type,unsigned int timestamp);	

    PixLib::Config& getConfig(){return *m_cfg;};
    
  private:
    
    std::string partitionName;
    std::string dbServerName;
    std::string isServerName;
    
    IPCPartition* partition;
    PixLib::PixConnectivity* pixConn;
    PixLib::PixDbServerInterface* dbServer;
    PixLib::PixISManager* isManager;
    PixLib::PixModule *m_pm;
    PixLib::PixModuleGroup *m_pmg;
    PixLib::TimPixTrigController *m_tim;
    PixLib::PixDisable *m_pdis;
    PixLib::PixRunConfig *m_prc;
    PixLib::Config *m_cfg;
    
    std::vector<std::string>* domainList;
    std::map<std::string,std::vector<std::string> >* tagList;
    std::vector<std::string>* objList;
    
    std::vector<std::string>* objTypeList;
    std::vector<std::pair<std::string,std::string> >* objInfoList;
    std::map<std::string, std::vector<std::string> >* tags;
    std::vector<unsigned int>* revList;
    std::vector<std::pair<std::string,unsigned int> >* typeList;
    std::vector<ModifiedObject>* modObjects;
    
};

}

#endif

