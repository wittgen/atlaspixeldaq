#ifndef _Object_h_
#define _Object_h_

#include <vector>
#include <string>

namespace PixDbBrowser
{

class Object
{
 public:
  
  const std::vector<unsigned int>& getRevisionList() {return *revList;}
  unsigned int getLastRevision() {return lastRev;}	
  std::string getType() {return type;}
  std::string getTag() {return tag;}
  std::string getName() {return name;}
  std::string getDomain() {return domain;}
  
  void setType(std::string type_arg) {type=type_arg;}
  void setTag(std::string tag_arg) {tag=tag_arg;}
  void setName(std::string name_arg) {name=name_arg;}
  void setDomain(std::string domain_arg) {domain=domain_arg;}
  void setLastRevision(unsigned int lastRev_arg) {lastRev=lastRev_arg;}
  void setRevisionList(std::vector<unsigned int>* revList_arg) {revList=revList_arg;}
  
  
 private:
  
  std::vector<unsigned int>* revList;
  unsigned int lastRev;
  std::string name,type,domain,tag;
	
	
};

}

#endif

