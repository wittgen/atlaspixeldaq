#include "ConnectivityView.h"
#include "DbConnection.h"
#include "BrowserHandler.h"

#include <QVBoxLayout>
#include <QTreeWidget>
#include <QGroupBox>

using namespace PixDbBrowser;

ConnectivityView::ConnectivityView(QWidget *parent, BrowserHandler &handler) : QFrame(parent), m_handler(handler) {
  QVBoxLayout *layout = new QVBoxLayout(this);
  m_tree = new QTreeWidget(this);
  m_tree->setColumnCount(3);
  QStringList hList;
  hList << "Tag";
  hList << "Type";
  hList << "Entries";
  m_tree->setHeaderLabels(hList);
  layout->addWidget(m_tree);
  QGroupBox *gb = dynamic_cast<QGroupBox*>(parent);
  if(gb!=0) gb->setLayout(layout);
  setMinimumWidth(200);
  // forward selection signal to outside panels
  connect(m_tree, SIGNAL( itemClicked(QTreeWidgetItem*, int) ), this, SLOT(procItemSel(QTreeWidgetItem*, int)));
}
void ConnectivityView::updateContent(){
  //disconnect(m_tree, SIGNAL( itemClicked(QTreeWidgetItem*, int) ), this, SLOT(procItemSel(QTreeWidgetItem*, int)));

  DbConnection &dbConn = m_handler.getDbConn();
  m_tree->clear();
  m_tree->setSortingEnabled(false);
  /* org. code, but will not work: have to select tags first (as is done now) and 
     can list only them in the tree view
  const std::vector<std::string>& domainList=dbConn.getDomainList();
  for(unsigned int i=0; i<domainList.size(); i++){
    QTreeWidgetItem *domIt = new QTreeWidgetItem(m_tree);
    domIt->setText(0, domainList.at(i).c_str());
    domIt->setText(1, "Domain");
    domIt->setExpanded(true);
    const std::vector<std::string>& tagList=dbConn.getTagList(domainList.at(i).c_str());
    for (unsigned int j=0;j<tagList.size();j++){
  */

  QTreeWidgetItem *defSelIt=0;

  std::string idname, connname, cfgname, modcfgname;
  m_handler.getTagNames(idname, connname, cfgname, modcfgname);
  for(unsigned int i=0;i<2;i++){
    std::string domName = "Configuration-"+idname;
    if(i) domName = "Connectivity-"+idname;
    QTreeWidgetItem *domIt = new QTreeWidgetItem(m_tree);
    domIt->setText(0, domName.c_str());
    domIt->setText(1, "Domain");
    domIt->setExpanded(true);
    std::vector<std::string> tagNames;
    if(i){
      tagNames.push_back(connname);
    } else {
      tagNames.push_back(cfgname);
      if(cfgname!=modcfgname) tagNames.push_back(modcfgname);
    }
    for(size_t k1=0;k1<tagNames.size();k1++){
      QTreeWidgetItem *tagIt = new QTreeWidgetItem(domIt);
      tagIt->setText(0,QString(tagNames.at(k1).c_str()));
      tagIt->setText(1,QString("Tag"));
      tagIt->setExpanded(true);//false);
      const std::vector<std::pair<std::string,unsigned int> >& types=dbConn.getTypes(domName,tagNames.at(k1));
      for (size_t k2=0;k2<types.size();k2++){
	QTreeWidgetItem* objIt=new QTreeWidgetItem(tagIt);
	objIt->setText(0,QString(types.at(k2).first.c_str()));
	objIt->setText(1,QString("Object type"));
	objIt->setText(2,QString::number(types.at(k2).second));
	objIt->setTextAlignment(2, Qt::AlignRight);
	if(defSelIt==0) defSelIt=objIt;
      }
    }
  }
  for(int i=0;i<m_tree->columnCount();i++)
    m_tree->resizeColumnToContents(i);
  adjustSize();

  if(defSelIt!=0){
    defSelIt->setSelected(true);
    procItemSel(defSelIt, 0);
  }
}
void ConnectivityView::procItemSel(QTreeWidgetItem *item, int col){
  if(col==0 && item!=0 && item->parent()!=0 && item->parent()->parent()!=0) // type-level item
    emit typeSelected(item->text(0));
}
