#include "ObjectView.h"
#include "DbConnection.h"
#include "BrowserHandler.h"
#include "SimpleTree.h"
#include "CfgViewer.h"
#include "ConfigEdit.h"
#include <SetPanel.h>

#include <Config/Config.h>
#include <PixDbServer/PixDbDomain.h>

#include <QStringList>
#include <QString>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QApplication>
#include <QLabel>
#include <QTreeWidget>
#include <QDateTime>
#include <QMenu>
#include <QMessageBox>

using namespace PixDbBrowser;

ObjectView::ObjectView(QWidget *parent, BrowserHandler &handler) : QFrame(parent), m_handler(handler) {
  m_layout = new QVBoxLayout(this);
  m_grid = new QGridLayout();

  QGridLayout *labelGrid = new QGridLayout();
  m_selObjLbl=new QLabel("N/A",this);
  m_selObjLbl->setFont(QFont("Arial",12,QFont::Bold));
  labelGrid->addWidget(m_selObjLbl,0,1,2,2);
  m_objTypeLbl=new QLabel("---",this);
  m_numRevLbl=new QLabel("---",this);
  m_lastRevLbl=new QLabel("---",this);
  m_tagLbl=new QLabel("---",this);
  m_domainLbl=new QLabel("---",this);
  QLabel* l1=new QLabel("Object type:",this);
  labelGrid->addWidget(l1,2,1);
  labelGrid->addWidget(m_objTypeLbl,2,3);
  QLabel* l2=new QLabel("Domain:",this);
  labelGrid->addWidget(l2,3,1);
  labelGrid->addWidget(m_domainLbl,3,3);
  QLabel* l3=new QLabel("Tag:",this);
  labelGrid->addWidget(l3,4,1);
  labelGrid->addWidget(m_tagLbl,4,3);
  QLabel* l4=new QLabel("Revisions:",this);
  labelGrid->addWidget(l4,5,1);
  labelGrid->addWidget(m_numRevLbl,5,3);
  QLabel* l5=new QLabel("Last revision:",this);
  labelGrid->addWidget(l5,6,1);
  labelGrid->addWidget(m_lastRevLbl,6,3);	
  m_grid->addLayout(labelGrid,0,0);

  QStringList hList;
  hList << "Revision";
  hList << "Pending tag";
  m_revList = new SimpleTree(this, "Revisions", hList);
  m_grid->addWidget(m_revList, 1, 0);
  m_revList->m_tree->setContextMenuPolicy(Qt::CustomContextMenu);

  hList.clear();
  hList << "Link name";
  hList << "Object";
  hList << "Type";
  hList << "Revision";
  m_upLinkList = new SimpleTree(this, "Uplinks", hList);
  m_grid->addWidget(m_upLinkList, 0, 1);
  m_downLinkList = new SimpleTree(this, "Downlinks", hList);
  m_downLinkList->setMinimumWidth(400);
  m_grid->addWidget(m_downLinkList, 1, 1);

  m_cfgViewer = new CfgViewer(this);
  m_grid->addWidget(m_cfgViewer, 2, 0, 1, 3);

  m_layout->addLayout(m_grid);

  m_actMenu = new QMenu("Actions");
  m_actAddTag = m_actMenu->addAction("Clone tag",this,SLOT(cloneTag()));
  m_actAddTag->setEnabled(false);
  m_actRemTag = m_actMenu->addAction("Remove tag",this,SLOT(removeTag()));				
  m_actRemTag->setEnabled(false);
  m_actMenu->addSeparator();
  m_actAddAllPTag = m_actMenu->addAction("Add pending tag (all obj.)",this,SLOT(addPendingTagAll()));
  m_actAddAllPTag->setEnabled(false);
  m_actAddPTag = m_actMenu->addAction("Add pending tag (this obj.)",this,SLOT(addPendingTagObj()));
  m_actAddPTag->setEnabled(false); //enable only if object is selected
  m_actRemPTag = m_actMenu->addAction("Remove pending tag",this,SLOT(removePendingTag()));
  m_actRemPTag->setEnabled(false); //enable only if object is selected
  m_actRenPTag = m_actMenu->addAction("Rename pending tag...",this,SLOT(renamePendingTag()));
  m_actRenPTag->setEnabled(false); //enable only if object is selected
  m_actMenu->addSeparator();
  m_actDump = m_actMenu->addAction("Dump revision",this,SLOT(currObjectDump()));
  m_actEdit= m_actMenu->addAction("Object editor",this,SLOT(currObjectEdit()));
  m_actDump->setEnabled(false); //enable only if object is selected
  m_actEdit->setEnabled(false); //enable only if object is selected

  m_revInfo.reserve(kMaxNrev);

}
void ObjectView::updateView(QStringList objData){
  if(objData.size()==kMaxNrev){
    std::string domain, tag, type, objName;
    domain = objData.at(kDomain).toLatin1().data();
    tag = objData.at(kTag).toLatin1().data();
    type = objData.at(kObjType).toLatin1().data();
    objName = objData.at(kObjLabel).toLatin1().data();

    QApplication::setOverrideCursor(Qt::WaitCursor);

    disconnect(m_revList->m_tree, SIGNAL(itemClicked(QTreeWidgetItem*, int)), this, SLOT(readObjectData(QTreeWidgetItem*, int)));
    disconnect(m_revList->m_tree, SIGNAL(customContextMenuRequested (const QPoint &)), this, SLOT(showMenu(const QPoint &)));

    std::vector<unsigned int> myRevList;
    try{
      const std::vector<unsigned int> &dbRevList=m_handler.getRevisionList(domain,tag,objName);
      for(unsigned int i=0; i<dbRevList.size();i++)
	myRevList.push_back(dbRevList[i]);
    }catch(...){
      std::cerr << "ObjectView::updateView : unknown exception when calling DbConnection::getRevisionList()" << std::endl;
      return;
    }

    QString lastRevTime;
    std::vector< std::vector<QString> > revisions;
    for(unsigned int i=0;i<myRevList.size();i++){
      try{
	std::vector<QString> rev_ptag;
	lastRevTime = QDateTime::fromTime_t(myRevList.at(i)).toString("dd.MM.yyyy hh:mm:ss");
	rev_ptag.push_back(lastRevTime);
	rev_ptag.push_back(QString(m_handler.getDbConn().getPendingTag(domain,tag,objName,myRevList.at(i)).c_str()));
	revisions.push_back(rev_ptag);
      }catch(...){
	std::cerr << "ObjectView::updateView : unknown exception while processing revision info" << std::endl;
	return;
      }
    }
    m_domainLbl->setText(objData.at(kDomain));
    m_tagLbl->setText(objData.at(kTag));
    m_selObjLbl->setText(objData.at(kObjLabel));
    m_objTypeLbl->setText(objData.at(kObjType));
    m_numRevLbl->setText(QString::number(myRevList.size()));
    m_lastRevLbl->setText(lastRevTime);
    m_revList->updateList(revisions);

    connect(m_revList->m_tree, SIGNAL(itemClicked(QTreeWidgetItem*, int)), this, SLOT(readObjectData(QTreeWidgetItem*, int)));
    connect(m_revList->m_tree, SIGNAL(customContextMenuRequested (const QPoint &)), this, SLOT(showMenu(const QPoint &)));
    if(revisions.size()>0){
      QStringList objPars;
      objPars.reserve(kMaxNrev);
      objPars.insert(kObjLabel, m_selObjLbl->text());
      objPars.insert(kObjType, m_objTypeLbl->text());
      objPars.insert(kDomain, m_domainLbl->text());
      objPars.insert(kTag, m_tagLbl->text());
      objPars.insert(kRev,revisions.at(revisions.size()-1).at(0));
      objPars.insert(kPenTag,revisions.at(revisions.size()-1).at(1));
      try{
	readObjectData(objPars);
      }catch(...){
	std::cerr << "ObjectView::updateView : unknown exception from readObjectData" << std::endl;
	return;
      }
    }

    QApplication::restoreOverrideCursor();
  }
}
void ObjectView::readObjectData(QTreeWidgetItem *item, int){
  if(item!=0){
    QStringList objPars;
    objPars.reserve(kMaxNrev);
    objPars.insert(kObjLabel, m_selObjLbl->text());
    objPars.insert(kObjType, m_objTypeLbl->text());
    objPars.insert(kDomain, m_domainLbl->text());
    objPars.insert(kTag, m_tagLbl->text());
    objPars.insert(kRev,item->text(0));
    objPars.insert(kPenTag,item->text(1));
    readObjectData(objPars);
  }
}
void ObjectView::readObjectData(QStringList revInfo){
  if(revInfo.size()!=kMaxNrev){
    std::cerr << "ObjectView::readObjectData : wrong no, of list items: " << revInfo.size() << std::endl;
    return;
  }

  m_revInfo = revInfo;

  std::string type, objName, domain, tag, penTag;
  objName = revInfo.at(kObjLabel).toLatin1().data();
  type = revInfo.at(kObjType).toLatin1().data();
  domain = revInfo.at(kDomain).toLatin1().data();
  tag = revInfo.at(kTag).toLatin1().data();
  penTag = revInfo.at(kPenTag).toLatin1().data();
  unsigned int rev = QDateTime::fromString(revInfo.at(kRev), "dd.MM.yyyy hh:mm:ss").toTime_t();

  m_actAddTag->setEnabled(true);
  m_actRemTag->setEnabled(m_handler.getDbConn().getTagList(domain).size()>1);
  m_actAddAllPTag->setEnabled(true);
  m_actAddPTag->setEnabled(true);
  m_actRemPTag->setEnabled(m_numRevLbl->text().toUInt()>1);//need at least one remaining pending tag!
  m_actRenPTag->setEnabled(true);
  m_actDump->setEnabled(true);
  m_actEdit->setEnabled(true);

  // read configuration
  QApplication::setOverrideCursor(Qt::WaitCursor);
  try{
    PixLib::Config &cfg = m_handler.getDbConn().getObjectConfig(domain, tag, objName, type, rev);
    m_cfgViewer->update(cfg);
    
    // read DB link info
    PixLib::PixDbDomain::rev_content revCont = m_handler.getDbConn().getRevision(domain,tag,objName,rev);
    std::vector<PixLib::PixDbDomain::link_object> tmpList;
    std::vector< std::vector<QString> > links;
    
    tmpList=revCont.upLink_content;
    for (unsigned int i=0;i<tmpList.size();i++){
      std::vector<QString> tmpval;
      PixLib::PixDbDomain::link_object tmpData=tmpList.at(i);
      tmpval.push_back(tmpData.linkName.c_str());
      tmpval.push_back(tmpData.objName.c_str());
      tmpval.push_back(tmpData.objType.c_str());
      tmpval.push_back(QDateTime::fromTime_t(tmpData.revision).toString("dd.MM.yyyy hh:mm:ss"));
      links.push_back(tmpval);
    }
    m_upLinkList->updateList(links);
    
    tmpList=revCont.downLink_content;
    links.clear();
    for (unsigned int i=0;i<tmpList.size();i++){
      std::vector<QString> tmpval;
      PixLib::PixDbDomain::link_object tmpData=tmpList.at(i);
      tmpval.push_back(tmpData.linkName.c_str());
      tmpval.push_back(tmpData.objName.c_str());
      tmpval.push_back(tmpData.objType.c_str());
      tmpval.push_back(QDateTime::fromTime_t(tmpData.revision).toString("dd.MM.yyyy hh:mm:ss"));
      links.push_back(tmpval);
    }
    m_downLinkList->updateList(links);
  }catch(...){
    std::cerr << "ObjectView::readObjectData : unknown exception while processing PixLib::Config" << std::endl;
    QApplication::restoreOverrideCursor();
    return;
  }
  
  QApplication::restoreOverrideCursor();
}
void ObjectView::currObjectEdit(){
  if(m_revInfo.at(kObjLabel)=="N/A") return;
  ConfigEdit edt(m_handler.getDbConn().getConfig(), m_revInfo, this);
  if(edt.exec()==QDialog::Accepted) {
    auto pendingTag = edt.getPendingTag();
    m_handler.getDbConn().writeObject(m_revInfo.at(kDomain).toLatin1().data(), m_revInfo.at(kTag).toLatin1().data(), m_revInfo.at(kObjType).toLatin1().data(),
				      m_revInfo.at(kObjLabel).toLatin1().data(), pendingTag);
    updateView(m_revInfo);
  }
}
void ObjectView::currObjectDump(){
  if(m_selObjLbl->text()=="N/A") return;
  m_handler.getDbConn().getConfig().dump(std::cout);
}
void ObjectView::showMenu(const QPoint &) {
  QTreeWidgetItem *item = m_revList->m_tree->currentItem();
  if(item!=0) m_actMenu->exec(QCursor::pos());
}
// still to be implemented
void ObjectView::cloneTag(){
  std::string domain=m_revInfo.at(kDomain).toLatin1().data();
  std::string currentTag=m_revInfo.at(kTag).toLatin1().data();
  std::string currentPendTag=m_revInfo.at(kPenTag).toLatin1().data();
  SetPanel getval(this, "Clone tag \""+m_revInfo.at(kTag)+"\" to new tag:", "");
  if(getval.exec()==QDialog::Accepted){
    std::string newTag = getval.getText().toLatin1().data();
    m_handler.getDbConn().cloneTag(domain,currentTag,newTag,currentPendTag);
    m_handler.getDbConn().loadDbContent();
    //updateView(m_revInfo);
    emit tagsChanged();
  }
}
void ObjectView::removeTag() {
  std::string domain=m_revInfo.at(kDomain).toLatin1().data();
  std::string currentTag=m_revInfo.at(kTag).toLatin1().data();
  if (m_handler.getDbConn().getTagList(domain).size()>1) {
    if (QMessageBox::question(this,"Remove tag","Remove tag \""+ m_revInfo.at(kTag) +"\"?",QMessageBox::Yes,QMessageBox::No)
	==QMessageBox::Yes) {
      m_handler.getDbConn().removeTag(domain,currentTag);
      m_handler.getDbConn().loadDbContent();
      //updateView(m_revInfo);
      emit tagsChanged();
    }
  }
}
void ObjectView::addPendingTag(bool all){
  std::string domain=m_revInfo.at(kDomain).toLatin1().data();
  std::string currentTag=m_revInfo.at(kTag).toLatin1().data();
  std::string currentObjName=m_revInfo.at(kObjLabel).toLatin1().data();

  SetPanel getval(this, "Add pending tag to tag \""+m_revInfo.at(kTag)+"\":", "");
  if(getval.exec()==QDialog::Accepted){
    std::string newPenTag = getval.getText().toLatin1().data();
    if(all)
      m_handler.getDbConn().setPendingTagAll(domain,currentTag,newPenTag,(unsigned int)time(0));
    else
      m_handler.getDbConn().setPendingTag(domain,currentTag,currentObjName,newPenTag,(unsigned int)time(0));;
    m_handler.getDbConn().loadDbContent();
    updateView(m_revInfo);
  }
}
void ObjectView::removePendingTag(){
  std::string domain=m_revInfo.at(kDomain).toLatin1().data();
  std::string currentTag=m_revInfo.at(kTag).toLatin1().data();
  std::string currentPendTag=m_revInfo.at(kPenTag).toLatin1().data();
  std::string currentObjName=m_revInfo.at(kObjLabel).toLatin1().data();
  
  if(m_numRevLbl->text().toUInt()<2){ //need at least one remaining pending tag! 
    QMessageBox::information(this,"Remove pending tag","You cannot remove the last remaining pending tag of this tag!",0);
    return;
  }

  if(QMessageBox::question(this,"Remove pending tag","Remove pending tag \""+m_revInfo.at(kPenTag)+"\"?",QMessageBox::Yes,QMessageBox::No)
     ==QMessageBox::Yes){
    m_handler.getDbConn().removePendingTag(domain,currentPendTag,currentTag);
    m_handler.getDbConn().loadDbContent();
    updateView(m_revInfo);
  }

  return;
}
void ObjectView::renamePendingTag(){
   std::string domain=m_revInfo.at(kDomain).toLatin1().data();
   std::string currentTag=m_revInfo.at(kTag).toLatin1().data();
   std::string currentPendTag=m_revInfo.at(kPenTag).toLatin1().data();
   std::string currentObjType=m_revInfo.at(kObjType).toLatin1().data();

   SetPanel getval(this, "Rename pending tag \""+m_revInfo.at(kPenTag)+"\" to:", m_revInfo.at(kPenTag)+"_new");
   if(getval.exec()==QDialog::Accepted){
     std::string newPenTag = getval.getText().toLatin1().data();
     m_handler.getDbConn().renamePendingTag(domain, currentTag, currentObjType, currentPendTag, newPenTag);
     m_handler.getDbConn().loadDbContent();
     updateView(m_revInfo);
   }

   return;
}
