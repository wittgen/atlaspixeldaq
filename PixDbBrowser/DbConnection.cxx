#include "DbConnection.h"
#include "Exception.h"
#include "Object.h"

#include <ipc/core.h>

#include "PixDbInterface/PixDbInterface.h"
#include "PixDbServer/PixDbDomain.h"
#include "PixDbServer/PixDbServerInterface.h"
#include "PixUtilities/PixISManager.h"

#include <Config/Config.h>
#include <Config/ConfGroup.h>
#include <PixConnectivity/PixConnectivity.h>
#include <PixConnectivity/PartitionConnectivity.h>
#include <PixConnectivity/RodCrateConnectivity.h>
#include <PixConnectivity/SbcConnectivity.h>
#include <PixConnectivity/RodBocConnectivity.h>
#include <PixConnectivity/TimConnectivity.h>
#include <PixConnectivity/OBConnectivity.h>
#include <PixConnectivity/Pp0Connectivity.h>
#include <PixConnectivity/ModuleConnectivity.h>
#include <PixConnectivity/RosConnectivity.h>
#include <PixModuleGroup/PixModuleGroup.h>
#include <PixController/PixController.h>
#include <PixController/PixRunConfig.h>
#include <PixBoc/PixBoc.h>
#include <PixModule/PixModule.h>
#include <PixTrigController/TimPixTrigController.h>
#include <PixConnectivity/PixDisable.h>

#include <oks/kernel.h>
#include <oks/object.h>
#include <oks/class.h>
#include <oks/index.h>
#include <oks/attribute.h>
#include <oks/relationship.h>

#include <iostream>
#include <time.h>
#include <ctime>

using namespace PixDbBrowser;
using namespace PixLib;

DbConnection::DbConnection(std::string partition_arg, std::string dbServer_arg,std::string isServer_arg) : partitionName(partition_arg),dbServerName(dbServer_arg),isServerName(isServer_arg)
{
  domainList=new std::vector<std::string>;
  tagList=new std::map<std::string, std::vector<std::string> >;
  objList=new std::vector<std::string>;
  
  objInfoList=new std::vector<std::pair<std::string,std::string> >;
  typeList=new std::vector<std::pair<std::string,unsigned int> >;
  revList=new std::vector<unsigned int>;
  modObjects=new std::vector<ModifiedObject>;
  isManager=0;
  partition=0;
  dbServer=0;
  pixConn=0;
  m_pmg = 0;
  m_pm  = 0;
  m_tim = 0;
  m_pdis= 0;
  m_prc = 0;
  m_cfg=0;
}

DbConnection::~DbConnection()
{
  delete domainList;
  delete tagList;
  delete objList;
  delete objInfoList;
  delete typeList;
  delete revList;
  delete modObjects;
  delete isManager;
  delete partition;
  delete dbServer;
  delete pixConn;
  delete m_pmg;
  delete m_pm;
  delete m_tim;
  delete m_pdis;
  delete m_prc;
}

void DbConnection::loadDbContent()
{
  domainList->clear();
  tagList->clear();    
  objInfoList->clear();
    
  // This is reserved to the partition loaders; don't use in stand-alone apps
  dbServer->listDomainRem(*domainList); // copy DomainList informaton from dbServer info the List: domainList of dbConn
    
  for (unsigned int i=0;i<domainList->size();i++)
    {
      std::vector<std::string> tmpList;
      dbServer->listTagsRem(domainList->at(i),tmpList);
      tagList->insert(std::make_pair(domainList->at(i),tmpList));
    }
}

void DbConnection::EnableRem()
{
        // This is reserved to the partition loaders; don't use in stand-alone apps
	//dbServer->setReadEnableRem(true);
}

const std::vector<std::string>& DbConnection::getDomainList()
{
	return *domainList;
}

const std::vector<std::string>& DbConnection::getTagList(std::string domName)
{
  std::map<std::string, std::vector<std::string> >::iterator it=tagList->find(domName);
	return it->second;
}

const std::vector<std::string>& DbConnection::getObjectList(std::string domainName,std::string tag)
{
	objList->clear();
	dbServer->listObjectsRem(domainName,tag,*objList);
	return *objList;
}

const std::vector<std::string>& DbConnection::getObjectList(std::string domainName,std::string tag,std::string type)
{
    objList->clear();
    std::vector<std::string> tmp_objList;
    // replacing back the "XXXPixController" to read its objects
    if(type=="PixController"){
      std::vector<std::string> tmpStrList;
      std::vector<unsigned int> tmpCount;
      dbServer->listTypes(domainName,tag,tmpStrList,tmpCount);
      for (unsigned int i=0;i<tmpStrList.size();i++) {
	// checking for all "XXXPixController"
	if(tmpStrList.at(i).find("PixController")!=string::npos){
	  dbServer->listObjectsRem(domainName,tag,tmpStrList.at(i),tmp_objList);
	  objList->insert(objList->end(),tmp_objList.begin(),tmp_objList.end());
	  tmp_objList.clear();
	}
      }
    } else if(type=="PixBoc"){
      std::vector<std::string> tmpStrList;
      std::vector<unsigned int> tmpCount;
      dbServer->listTypes(domainName,tag,tmpStrList,tmpCount);
      for (unsigned int i=0;i<tmpStrList.size();i++) {
	// checking for all "XXXBoc"
	if(tmpStrList.at(i).find("Boc")!=string::npos){
	  dbServer->listObjectsRem(domainName,tag,tmpStrList.at(i),tmp_objList);
	  objList->insert(objList->end(),tmp_objList.begin(),tmp_objList.end());
	  tmp_objList.clear();
	}
      }
    }
    else
      dbServer->listObjectsRem(domainName,tag,type,*objList);

    return *objList;
}


const std::vector<std::pair<std::string,unsigned int> >& DbConnection::getTypes(std::string domainName,std::string tag)
{
  bool haveCtrl=false, haveBoc=false;
  typeList->clear();
  std::vector<std::string> tmpStrList;
  std::vector<unsigned int> tmpCount;
  dbServer->listTypes(domainName,tag,tmpStrList,tmpCount);
  
  // To combine different types of PixController
  std::pair<std::string,unsigned int> PixCtrl_tmpPair;
  PixCtrl_tmpPair.first="PixController";
  PixCtrl_tmpPair.second=0;
  // To combine different types of PixBoc
  std::pair<std::string,unsigned int> PixBoc_tmpPair;
  PixBoc_tmpPair.first="PixBoc"; //added by MJK
  PixBoc_tmpPair.second=0;
  
  
  for (unsigned int i=0;i<tmpStrList.size();i++) {

    std::pair<std::string,unsigned int> tmpPair;
    
    // ignore sub-types
    if(getObjectList(domainName, tag, tmpStrList.at(i)).at(0).find("|")!=std::string::npos) continue;

    if(tmpStrList.at(i).find("PixController")!=string::npos){ // renaming "XXXPixController" to "PixController" in dbConn
      haveCtrl = true;
      PixCtrl_tmpPair.second +=tmpCount.at(i);
    } else if(tmpStrList.at(i).find("Boc")!=string::npos){ // renaming "XXXBoc" to "PixBoc" in dbConn
      haveBoc = true;
      PixBoc_tmpPair.second +=tmpCount.at(i);
    } else {                                               // normal treatment for everything else
      tmpPair.first=tmpStrList.at(i);
      tmpPair.second =tmpCount.at(i);
      typeList->push_back(tmpPair);
    }
  }

  // To add PixController or PixBoc type in the correct domain
  if(haveCtrl) typeList->push_back(PixCtrl_tmpPair);
  if(haveBoc) typeList->push_back(PixBoc_tmpPair);

  return *typeList; 
}

const std::vector<unsigned int>& DbConnection::getRevisionList(std::string domainName,std::string tag,std::string objName)
{
  revList->clear();
  dbServer->listRevisionsRem(domainName,tag,objName,*revList);
  return *revList;
}

const std::string DbConnection::getPendingTag(std::string domainName,std::string tag,std::string objName,unsigned int revision)
{
  std::string ret;
  dbServer->getPending(ret,domainName,tag,objName,revision);
  return ret;
}

const std::string DbConnection::getPartitionName()
{
  return partitionName;
}

PixLib::Config& DbConnection::getObjectConfig(std::string domain,std::string tag,std::string objName,std::string type, unsigned int rev){

  static Config cfg("empty");
  if(cfg.size()==0) cfg.addGroup("empty");
  m_cfg = &cfg;

  if(domain.find("Connectivity-")!=std::string::npos){
    std::string topName = objName;
    std::size_t found = topName.find("/");
    if(found!=std::string::npos) topName = topName.substr(0,found);
    if(type=="PARTITION" && pixConn->parts.find(objName)!=pixConn->parts.end())
      m_cfg = &(pixConn->parts[objName]->config());
    if(type=="ROD_CRATE" && pixConn->crates.find(objName)!=pixConn->crates.end())
      m_cfg = &(pixConn->crates[objName]->config());
    if(type=="SBC" && pixConn->sbcs.find(objName)!=pixConn->sbcs.end())
      m_cfg = &(pixConn->sbcs[objName]->config());
    if(type=="TIM" && pixConn->tims.find(objName)!=pixConn->tims.end())
      m_cfg = &(pixConn->tims[objName]->config());
    if(type=="ROD_BOC" && pixConn->rods.find(objName)!=pixConn->rods.end())
      m_cfg = &(pixConn->rods[objName]->config());
    if(type=="OPTOBOARD" && pixConn->obs.find(objName)!=pixConn->obs.end())
      m_cfg = &(pixConn->obs[objName]->config());
    if(type=="PP0" && pixConn->pp0s.find(objName)!=pixConn->pp0s.end())
      m_cfg = &(pixConn->pp0s[objName]->config());
    if(type=="MODULE" && pixConn->mods.find(objName)!=pixConn->mods.end())
      m_cfg = &(pixConn->mods[objName]->config());
    if(type=="ROS" && pixConn->roses.find(objName)!=pixConn->roses.end())
      m_cfg = &(pixConn->roses[objName]->config());
    if(type=="ROBIN" && pixConn->robins.find(objName)!=pixConn->robins.end())
      m_cfg = &(pixConn->robins[objName]->config());
    if(type=="ROL" && pixConn->rods.find(topName)!=pixConn->rods.end() && pixConn->rods[topName]->rol()!=0)
      m_cfg = &(pixConn->rods[topName]->rol()->config());
    if(type=="ROD_BOC_LINKMAP" && pixConn->rods.find(topName)!=pixConn->rods.end() && pixConn->rods[topName]->linkMap()!=0)
	m_cfg = &(pixConn->rods[topName]->linkMap()->config());
    if(type=="OB_LINKMAP" && pixConn->obs.find(topName)!=pixConn->obs.end())
      m_cfg = &(pixConn->obs[topName]->linkMap()->config());
  } else{
    pixConn->setCfgRev(rev);
    if(type=="PixModule") pixConn->setModCfgRev(rev);
    pixConn->loadConn();

    if(type=="PixModule"){
      // creating PixModule - no need to start from scratch if existing object can be recycled
      if((m_pm!=0 && (m_pm->moduleName()!=objName)) || m_pm==0){
	delete m_pm; m_pm=0;
	ModuleConnectivity *mconn=0;
	for(std::map<std::string, ModuleConnectivity*>::iterator it=pixConn->mods.begin(); it!=pixConn->mods.end(); it++){
	  if(objName==it->second->prodId()){
	    mconn = it->second;
	    break;
	  }
	}
	if(mconn!=0) {
	  PixModule::MCCflavour mccFlv = PixModule::PM_MCC_I2;
	  PixModule::FEflavour  feFlv  = PixModule::PM_FE_I2;
	  int nFe = 16;
	  mconn->getModPars(mccFlv, feFlv, nFe);
	  m_pm = new PixModule(dbServer, (PixModuleGroup *)0, domain, tag, objName, mccFlv, feFlv, nFe);
	  m_cfg = &(m_pm->config());
	} else
	  m_cfg = &cfg;
      } else
	m_cfg = &(m_pm->config());
    }
    else if(type=="PixModuleGroup" || type=="PixController" || type=="PixBoc"){
      try{
	// extract group name if needed
	std::string grpObjName = objName;
	if(type=="PixController"){
	  std::size_t found = objName.find("_ROD");
	  if(found!=std::string::npos)
	    grpObjName = objName.substr(0,found);
	}
	else if(type=="PixBoc"){
	  std::size_t found = objName.find("_BOC");
	  if(found!=std::string::npos)
	    grpObjName = objName.substr(0,found);
	}
	// creating PixModuleGroup - no need to start from scratch if existing object can be recycled
	// create w/o modules to make it faster
	if((m_pmg!=0 && (m_pmg->getName()!=grpObjName)) || m_pmg==0){
	  delete m_pmg; m_pmg=0;
	  PixModuleGroup tmpPmg(grpObjName, UNKNOWN, false);
	  tmpPmg.config().read(dbServer, domain, tag, grpObjName, rev);
	  m_pmg = new PixModuleGroup(grpObjName, tmpPmg.getCtrlType(), tmpPmg.getBocAvailable());
	}
	if(type=="PixModuleGroup") m_cfg = &(m_pmg->config());
	else if(type=="PixController" && m_pmg->getPixController()!=0)
	  m_cfg = &(m_pmg->getPixController()->config());
	else if(type=="PixBoc" && m_pmg->getPixBoc()!=0)
	  m_cfg = m_pmg->getPixBoc()->getConfig();
      }catch(...){
	m_cfg = &cfg;
	m_pmg = 0;
	std::cerr << "DbConnection::getObjectConfig : exception caught while creating PixModuleGroup" << std::endl;
      }
    } else if(type=="TimPixTrigController"){
      try{
	delete m_tim; m_tim=0;
	m_tim = new TimPixTrigController(dbServer,domain, tag, objName);
	m_cfg = m_tim->config();
      }catch(...){
	m_cfg = &cfg;
	m_tim=0;
	std::cerr << "DbConnection::getObjectConfig : exception caught while creating PixTrigController" << std::endl;
      }
    } else if(type=="PixDisable"){
      try{
	delete m_pdis;m_pdis=0;
	m_pdis = new PixDisable(objName);
	m_pdis->readConfig(dbServer, domain, tag, pixConn->getCfgRev());
	m_cfg = &(m_pdis->config());
      }catch(...){
	m_cfg = &cfg;
	m_pdis=0;
	std::cerr << "DbConnection::getObjectConfig : exception caught while creating PixDisable" << std::endl;
      }
    } else if(type=="PixRunConfig"){
      try{
	delete m_prc; m_prc=0;
	m_prc = new PixRunConfig(objName);
	m_prc->readConfig(dbServer, domain, tag, pixConn->getCfgRev());
	m_cfg = &(m_prc->config());
      }catch(...){
	m_cfg = &cfg;
	m_pdis=0;
	std::cerr << "DbConnection::getObjectConfig : exception caught while creating PixRunConfig" << std::endl;
      }
    }
  }
  if(m_cfg->size()==0){
    std::cerr << "Config for \""<<type<<"\" has no groups -> cannot be displayed" << std::endl;
    m_cfg = &cfg;
  } else
    m_cfg->read(dbServer, domain, tag, objName, rev);
  return *m_cfg;
}

PixDbDomain::rev_content DbConnection::getRevision(std::string domain,std::string tag,std::string obj,unsigned int revId)
{
	PixDbDomain::rev_content revCont;
	dbServer->getObject(revCont,domain,tag,obj,revId);
	return revCont;
}

void DbConnection::cloneTag(std::string domain,std::string oldTag,std::string tag,std::string pendTag)
{
	dbServer->cloneTag(domain,oldTag,tag,pendTag);
}

void DbConnection::removeTag(std::string domain,std::string tag)
{
	dbServer->removeTag(domain,tag);
}

void DbConnection::removePendingTag(std::string domain,std::string pendTag,std::string tag)
{
	dbServer->removePendingTag(domain,pendTag,tag);
}

void DbConnection::setPendingTag(std::string domain,std::string tag,std::string object,std::string pendTag,unsigned int revision)
{
	dbServer->setPending(pendTag,domain,tag,object,revision);
}

void DbConnection::setPendingTagAll(std::string domain,std::string tag,std::string pendTag,unsigned int revision)
{
	const std::vector<string>& objects=getObjectList(domain,tag);
	for (unsigned int i=0;i<objects.size();i++)
	{
		const std::vector<unsigned int>& revisions=getRevisionList(domain,tag,objects[i]);
		for (unsigned int j=0;j<revisions.size();j++)
			dbServer->setPending(pendTag,domain,tag,objects[i],revision);
	}
}

void DbConnection::setPendingTagAllTags(std::string domain,std::string pendTag,unsigned int revision)
{
	const std::vector<string>& tag_list=getTagList(domain);
	for (unsigned int t=0;t<tag_list.size();t++)
	{
		const std::vector<string>& objects=getObjectList(domain,tag_list.at(t));
		for (unsigned int i=0;i<objects.size();i++)
		{
			const std::vector<unsigned int>& revisions=getRevisionList(domain,tag_list[t],objects[i]);
			for (unsigned int j=0;j<revisions.size();j++)
				dbServer->setPending(pendTag,domain,tag_list[t],objects[i],revision);
		}
	}
}

void DbConnection::renamePendingTag(std::string domain,std::string tag,std::string type,std::string oldPendTag,std::string newPendTag)
{
	dbServer->renamePendingTag(domain,oldPendTag,newPendTag,tag,type);
}

void DbConnection::writeObject(std::string domain, std::string tag, std::string type, std::string object, std::string pendTag){
  unsigned int my_time = (unsigned int)time(0);
  if(domain.find("Connectivity-")!=std::string::npos){
    if(!(m_cfg!=0 && m_cfg->write(dbServer, domain, tag, object, type, pendTag, my_time)))
      std::cerr << "DbConnection::writeObject : ERROR saving conn. obj. " << type << ", " << object << std::endl;
  } else if(type=="PixModule"){
    if(!(m_pm!=0 && m_pm->writeConfig(dbServer, domain, tag, pendTag, my_time)))
      std::cerr << "DbConnection::writeObject : ERROR saving module " << object << std::endl;
  } else if(type=="PixModuleGroup" || type=="PixController" || type=="PixBoc"){
    // use PixModuleGroup's write all functionality to automatically cover sub-structure
    if(!(m_pmg!=0 && m_pmg->writeConfigAll(dbServer, domain, tag, pendTag, my_time)))
      std::cerr << "DbConnection::writeObject : ERROR saving module group for "<< type <<", " << object << std::endl;
  } else if(type=="TimPixTrigController"){
    if(!(m_tim!=0 && m_tim->writeConfig(dbServer, domain, tag, pendTag, my_time)))
      std::cerr << "DbConnection::writeObject : ERROR saving TIM " << object << std::endl;
  } else if(type=="PixDisable"){
    if(!(m_pdis!=0 && m_pdis->writeConfig(dbServer, domain, tag, pendTag, my_time)))
      std::cerr << "DbConnection::writeObject : ERROR saving Disable " << object << std::endl;
  } else if(type=="PixRunConfig"){
    if(!(m_prc!=0 && m_prc->writeConfig(dbServer, domain, tag, pendTag, my_time)))
      std::cerr << "DbConnection::writeObject : ERROR saving RunConfig " << object << std::endl;
  } else {
  }
}

void DbConnection::writeObject(std::string domain,std::string tag,std::string type,std::string object,PixDbDomain::rev_content& content,unsigned int revision,bool expert)
{
	dbServer->writeObject(content,domain,tag,object,type,revision,expert);
}

std::vector<std::string>* DbConnection::getLoadList()
{
	std::vector<std::string>* ret=new std::vector<std::string>;
	dbServer->listLoadList(*ret);
	return ret;
}

std::vector<std::string>* DbConnection::getSaveList()
{
	std::vector<std::string>* ret=new std::vector<std::string>;
	dbServer->listSaveList(*ret);
	return ret;
}

void DbConnection::addLoadList(Object* obj,unsigned int revision)
{
	PixDbDomain::obj_id objId;
	objId.name=obj->getName();
	objId.tag=obj->getTag();
	objId.domain=obj->getDomain();
	objId.type=obj->getType();
	objId.rev=revision;
	dbServer->addLoadList(objId);
}

Object* DbConnection::getObjectFromLoadList(std::string id)
{
	Object* ret=new Object;
	PixDbDomain::obj_id obj;
	dbServer->getLoadList(id,obj);
	ret->setName(obj.name);
	ret->setTag(obj.tag);
	ret->setType(obj.type);
	ret->setDomain(obj.domain);
	return ret;
}

Object* DbConnection::getObjectFromSaveList(std::string id)
{
	Object* ret=new Object;
	PixDbDomain::obj_id obj;
	dbServer->getSaveList(id,obj);
	ret->setName(obj.name);
	ret->setTag(obj.tag);
	ret->setType(obj.type);
	ret->setDomain(obj.domain);
	return ret;
}

void DbConnection::restoreLoadList(std::string id)
{
	dbServer->restoreLoadList(id);
}

void DbConnection::cleanLoadList(std::string id)
{
	dbServer->cleanLoadList(id);
}

void DbConnection::restoreSaveList(std::string id)
{
	dbServer->restoreSaveList(id);
}

void DbConnection::cleanSaveList(std::string id)
{
	dbServer->cleanSaveList(id);
}

void DbConnection::updateSaveList()
{
	dbServer->updateSaveList();
}

unsigned int DbConnection::getLoadListLen()
{
	return dbServer->getLoadListLen();
}


void DbConnection::deleteTagList(std::vector<std::pair<std::string,std::string> >& tag_list)
{
	for (unsigned int i=0;i<tag_list.size();i++)
		dbServer->removeTag(tag_list[i].first,tag_list[i].second);
}

const std::vector<ModifiedObject>& DbConnection::getModifiedObjects(std::string domain,std::string tag,std::string type,unsigned int timestamp)
{
	modObjects->clear();
	
	const std::vector<std::string>& objects=getObjectList(domain,tag,type);	
	
	for (unsigned int i=0;i<objects.size();i++)
	{
		const std::vector<unsigned int>& revisions=getRevisionList(domain,tag,objects[i]);
		for (unsigned int j=0;j<revisions.size();j++)
			if (revisions[j]>timestamp)
			{
				ModifiedObject tmpMod;
				tmpMod.revision=revisions[j];
				tmpMod.domain=domain;
				tmpMod.tag=tag;
				tmpMod.type=type;
				tmpMod.name=objects[i];
				modObjects->push_back(tmpMod);
			}
	}
	
	return *modObjects;
}

std::vector<std::string> DbConnection::getIdTagList()
{
	return pixConn->listConnTagsOI();
}


void DbConnection::connect() 
{
	try 
	{
		partition = new IPCPartition(partitionName.c_str());
	}
	catch(...)
	{
		throw Exception("Error while connecting to IPC partition!",Exception::Fatal);
	}
	bool ready=false;
	int nTry=0;
	
	do
	{
		sleep(1);
		dbServer=new PixDbServerInterface(partition,dbServerName.c_str(),PixDbServerInterface::CLIENT, ready);
	
    		if(!ready)
    			delete dbServer;
    		else
    			break;
		nTry++;
	} 
	while(nTry<5);  
	if(!ready)
		throw (Exception("Error while connecting to DbServer!",Exception::Fatal));

	try
	{
		isManager = new PixISManager(partition, isServerName.c_str());
	}  
	catch(PixISManagerExc& e)
	{ 
		throw (Exception(std::string("Error from PixISManager!"),Exception::Fatal));
	}
	catch(...)
	{ 
		throw (Exception(std::string("Error while creating ISManager fro IsServer ")+isServerName,Exception::Fatal));

	}
	
	
	
	
	
}

void DbConnection::connectPixConn()
{
	try
	{
		pixConn = new PixConnectivity("Base","Base","Base","Base","Base");
	}
	catch (...)
	{
		throw (Exception(std::string("Error while connecting to PixConnectivity!"),Exception::Fatal));
	}
}
void DbConnection::connectPixConn(std::string idTag,std::string connTag,std::string cfgTag,std::string cfgModTag)
{
  	try
	{

        //pixConn = new PixConnectivity(connTag, connTag, connTag, cfgTag, idTag, cfgModTag);
        pixConn = new PixConnectivity(dbServer,connTag, connTag, connTag, cfgTag, idTag, cfgModTag);

        //qDebug() <<"(DbConnection).rods size= "<< pixConn->rods.size();
	}
	catch (...)
	{
		throw (Exception(std::string("Error while connecting to PixConnectivity!"),Exception::Fatal));
	}

}
