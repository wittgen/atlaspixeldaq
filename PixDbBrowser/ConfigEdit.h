#ifndef _ConfigEdit_h_
#define _ConfigEdit_h_

#include <QDialog>

class QString;
class QStringList;
class QWidget;
class optionsFrame;
class ConfigEditI3;
class ConfigEditI4;

namespace PixLib{
  class Config;
  class PixModule;
}
namespace PixDbBrowser
{

  //  class BrowserHandler;

  class ConfigEdit : public QDialog {
    
    Q_OBJECT
      
  public:
    ConfigEdit(PixLib::Config &config, QStringList objPars, QWidget *parent=0);
    ~ConfigEdit(){};
    std::string getPendingTag(){return m_pendingTag;};

  public slots:
    void writeToDb();

  signals:

  private:
    QStringList m_objPars;
    optionsFrame *m_edt;
    ConfigEditI3 *m_edtI3;
    ConfigEditI4 *m_edtI4;
    PixLib::Config &m_config;
    PixLib::PixModule *m_pm;
    std::string m_pendingTag;
  };
}

#endif // _ConfigEdit_h_
