#ifndef _Exception_h_
#define _Exception_h_

namespace PixDbBrowser
{



class Exception
{

 public:
  
  enum ErrorType {Fatal=0,Error=1};
  
  Exception(std::string msg_arg, ErrorType type_arg) : msg(msg_arg), type(type_arg) {}
    
    ErrorType errorType() {return type;}
    std::string message() {return msg;}
    
 private:
    
    std::string msg;
    ErrorType type;
};

}

#endif

