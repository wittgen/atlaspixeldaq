#ifndef _SimpleTree_h_
#define _SimpleTree_h_

#include <QFrame>
#include <vector>

class QString;
class QStringList;
class QWidget;
class QTreeWidget;
class QTreeWidgetItem;
class QVBoxLayout;

namespace PixDbBrowser
{
  class BrowserHandler;
  class ObjectView;

  class SimpleTree : public QFrame {
    
    Q_OBJECT
      
  friend class ObjectView;

  public:
    SimpleTree(QWidget *parent, QString title, QStringList headers);
    ~SimpleTree(){};

  public slots:
    void updateList(std::vector< std::vector<QString> >);
    void processSelection(QTreeWidgetItem * current, QTreeWidgetItem * previous);

  signals:
    void itemSelected(QStringList);

  protected:
    QTreeWidget *m_tree;
  };
}

#endif // _SimpleTree_h_
