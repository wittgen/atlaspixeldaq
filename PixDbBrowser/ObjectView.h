#ifndef _ObjectView_h_
#define _ObjectView_h_

#include <QFrame>

class QStringList;
class QWidget;
class QVBoxLayout;
class QGridLayout;
class QLabel;
class QMenu;
class QTreeWidgetItem;
class QPoint;
class QAction;

namespace PixLib
{
  class Config;
}
namespace PixDbBrowser
{
  class Browser;
  class BrowserHandler;
  class SimpleTree;
  class CfgViewer;

  class ObjectView : public QFrame {
    Q_OBJECT
      
  friend class Browser;

  public:
    ObjectView(QWidget *parent, BrowserHandler &handler);
    ~ObjectView(){};

  public slots:
    void updateView(QStringList slist);
    void showMenu(const QPoint &);
    void readObjectData(QStringList revInfo);
    void readObjectData(QTreeWidgetItem *item, int);
    void currObjectEdit();
    void currObjectDump();
    void cloneTag();
    void removeTag();				
    void addPendingTag(bool all);
    void addPendingTagAll(){addPendingTag(true);};
    void addPendingTagObj(){addPendingTag(false);};
    void removePendingTag();
    void renamePendingTag();

  signals:
    void tagsChanged();

  protected:
    QMenu *m_actMenu;

  private:
    QVBoxLayout *m_layout;
    QGridLayout *m_grid;
    PixLib::Config *m_cfg;
    CfgViewer *m_cfgViewer;
    BrowserHandler &m_handler;
    QLabel *m_selObjLbl;
    QLabel *m_objTypeLbl;
    QLabel *m_numRevLbl;
    QLabel *m_lastRevLbl;
    QLabel *m_tagLbl;
    QLabel *m_domainLbl;
    SimpleTree *m_revList, *m_upLinkList, *m_downLinkList;
    QAction *m_actAddTag, *m_actRemTag, *m_actAddAllPTag, *m_actAddPTag, *m_actRemPTag, *m_actRenPTag, *m_actDump, *m_actEdit;
    QStringList m_revInfo;
  };
}
#endif // _ObjectView_h_
