#ifndef _ConnectivityView_h_
#define _ConnectivityView_h_

#include <QFrame>

class QTreeWidget;
class QTreeWidgetItem;
class QString;

namespace PixDbBrowser
{
  class BrowserHandler;

  class ConnectivityView : public QFrame {

    Q_OBJECT

  public:
    ConnectivityView(QWidget *parent, BrowserHandler &handler);
    ~ConnectivityView(){};

  public slots:
    void updateContent();
    void procItemSel(QTreeWidgetItem*, int);

  signals:
    void typeSelected(QString name);

  private:
    QTreeWidget *m_tree;
    BrowserHandler &m_handler;
  };
}
#endif // _ConnectivityView_h_
