#ifndef _StartLogo_h_
#define _StartLogo_h_

#include <QWidget>

class QProgressBar;
class QFrame;
class QLabel;
class QSize;
class QGridLayout;

namespace PixDbBrowser
{

class StartLogo : public QWidget
{
  Q_OBJECT
    
 public:
	
  StartLogo();
  ~StartLogo(){};
  
  void setMax(int max_arg) { max = max_arg; }
  void setCurrent(int current_arg) { current = current_arg; }
  void updateProgressBar();
  void setText(QString text_arg) { text = text_arg; }
  
 private:
  
  int max,current;
  
  QProgressBar* progress;
  
  QFrame* logoFrame;
  
  QFrame* frame2;
  QLabel* label;
  QString text;
  
  QSize size;
  
  QGridLayout* layout;
	
};

}

#endif

