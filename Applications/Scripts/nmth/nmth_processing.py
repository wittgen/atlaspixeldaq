import os
import sys
import argparse
from ROOT import PyConfig
PyConfig.IgnoreCommandLineOptions = True #prevents hijacking of command line arguments
from ROOT import TFile, TCanvas, TH2F, TDirectory, TIter, gDirectory, TObject

parser = argparse.ArgumentParser(description='Noise Mask Trough Histogrammer Processing')

parser.add_argument("inputfile", type=str, help="Input root file")
parser.add_argument("output", type=str, help="Output directory for results.")
parser.add_argument("-t", "--result_type", type=str, choices=["occupancy", "averageNoise", "noiseMask"], default="noiseMask", help="Result type")
parser.add_argument("-l", "--limit", type=float, default=1e-6, help="Noise level limit for noiseMask. Mandatory for noiseMask.")
parser.add_argument("-v", "--verbose", help="Verbose", action="store_true")

args = parser.parse_args()

if (args.result_type == "noiseMask"):
    if not args.limit:
        print("Limit argument is mandatory for noiseMask!")
        print("Please provide a limit (-l).")
        sys.exit()


def get_averageNoise(histo, trigger):
    xBins = histo.GetNbinsX()
    yBins = histo.GetNbinsY()
    averageNoise = TH2F("", "", xBins, 0, xBins, yBins, 0, yBins)
    for i in range(xBins):
        for j in range(yBins):
            averageNoise.SetBinContent(i+1, j+1, (histo.GetBinContent(i+1,j+1)/float(trigger)))
    averageNoise.SetName(histo.GetName())
    averageNoise.SetTitle(histo.GetTitle())
    averageNoise.SetXTitle("Column")
    averageNoise.SetYTitle("Row")
    return averageNoise


def generate_noiseMask(histo, threshold):
    maskedPixel = 0
    xBins = histo.GetNbinsX()
    yBins = histo.GetNbinsY()
    noiseMask = TH2F("", "", xBins, 0, xBins, yBins, 0, yBins);
    for i in range(xBins):
        for j in range(yBins):
      	     if(histo.GetBinContent(i+1,j+1) >= (threshold)):
                noiseMask.SetBinContent(i+1, j+1, 1)
		maskedPixel += 1
      		#else:
      		#	noiseMask.SetBinContent(i+1, j+1, 0)
    moduleName = histo.GetName()
    noiseMask.SetName(moduleName)
    noiseMask.SetTitle(histo.GetTitle())
    noiseMask.SetXTitle("Column")
    noiseMask.SetYTitle("Row")
    return [noiseMask, moduleName, maskedPixel]


def write_to_root(output_file, histos):
    print("")
    print("Save histos in %s." %(output_file))
    out = TFile(output_file, "recreate")
    for histo in histos:
	nd = histo.GetName()
        out.mkdir(nd)
        out.cd(nd)
        histo.Write()


def nmth_histos(input_file, output_directory, histo_type, limit):
    histos = []                     #create array for all output histos
    inputfile = TFile(input_file)     #reading from oh file
    run_name = input_file.split('/')[-1].replace('.root','')[4:]
    run_number = run_name[1:]
    print('Run: %s' % run_number)
    trigger_name = ''
    trigger = 0                    #create variable for number of triggers

    inputfile.cd(run_name)

    list_RODs = TIter(gDirectory.GetListOfKeys())
    for obj_ROD in list_RODs:
        current_ROD = obj_ROD.GetName()
        if args.verbose:
            print("Found ROD: %s" % current_ROD)
        inputfile.cd("%s/%s" % (run_name, current_ROD))
        list_modules = TIter(gDirectory.GetListOfKeys())
        for obj_module in list_modules:
            current_module = obj_module.GetName()
            if args.verbose:
                print("Found module: %s" % current_module)
            inputfile.cd("%s/%s/%s/OCCUPANCY" % (run_name, current_ROD, current_module))
            list_triggers = TIter(gDirectory.GetListOfKeys())
            for obj_trigger in list_triggers:
                trigger_name = obj_trigger.GetName()
                trigger = trigger_name[1:]
                inputfile.cd("%s/%s/%s/OCCUPANCY/%s" % (run_name, current_ROD, current_module, trigger_name))
                list_histos = TIter(gDirectory.GetListOfKeys())
                for obj_histo in list_histos:
                    current_histo = obj_histo.ReadObj()
                    current_histo.SetName(current_module)
                    new_title = "%s:%s - %s" %(histo_type, current_module, trigger_name)
                    current_histo.SetTitle(new_title)
                    current_histo.SetXTitle("Column")
                    current_histo.SetYTitle("Row")
                    histos.append(current_histo)

    if (len(histos) == 0):
        print("No histo found!")
        sys.exit(1)                #exit if no histos in histos array

    output_name = ""
    output_file = ""
    histos_tmp = []

    if (histo_type == "occupancy"):
        output_name = "%s_%s_%s.root" %(histo_type, run_name, trigger_name)
        output_file = output_directory + output_name

    elif (histo_type == "averageNoise"):
        output_name = "%s_%s_%s.root" %(histo_type, run_name, trigger_name)
        output_file = output_directory + output_name
        for histo in histos:
            new_title = "%s:%s" %(histo_type, histo.GetName())
            histo.SetTitle(new_title)
            histo = get_averageNoise(histo, trigger)
            histos_tmp.append(histo)

    else: #noiseMask
        output_name = "%s_%s_%s_%s.root" %(histo_type, run_name, trigger_name, limit)
        output_file = output_directory + output_name
        if args.verbose:
            print("")
            print("-------------------------------------------")
            print("Number of masked pixels:")
        total_masked_pixel = 0
        warning_container = []
        for histo in histos:
            new_title = "%s:%s" %(histo_type, histo.GetName())
            histo.SetTitle(new_title)
            histo = get_averageNoise(histo, trigger)
            noiseMaskResults = generate_noiseMask(histo, limit)
            histo = noiseMaskResults[0]
            histos_tmp.append(histo)
            if args.verbose:
                print("%s: %i" %(noiseMaskResults[1],noiseMaskResults[2]))
            total_masked_pixel += noiseMaskResults[2]
            if noiseMaskResults[2] >= 5:
                warning_container.append([noiseMaskResults[1],noiseMaskResults[2]])

        if warning_container:
            print("-------------------------------------------")
            print("WARNING")
            print("Have a look on following module(s)!")
            for i in warning_container:
                print("%s (%i)" %(i[0],i[1]))
            print("-------------------------------------------")
            print("Total number of modules: %i" %(len(histos)))
            print("Total number of masked pixel: %s" %(total_masked_pixel))
            print("-------------------------------------------")


    if histos_tmp:
        histos = histos_tmp
        histos_tmp = []
    write_to_root(output_file, histos)

#main
print("Create %s histos for %s" %(args.result_type, args.inputfile))
nmth_histos(args.inputfile, args.output, args.result_type, args.limit)
