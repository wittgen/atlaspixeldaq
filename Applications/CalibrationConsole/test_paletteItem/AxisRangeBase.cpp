#include "AxisRangeBase.h"

/*
 *  Constructs a AxisRangeBase as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  true to construct a modal dialog.
 */
AxisRangeBase::AxisRangeBase(QWidget* parent)
    : QDialog(parent)
{
    setupUi(this);

}

/*
 *  Destroys the object and frees any allocated resources
 */
AxisRangeBase::~AxisRangeBase()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void AxisRangeBase::languageChange()
{
    retranslateUi(this);
}

