#ifndef AXISRANGEBASE_H
#define AXISRANGEBASE_H

#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QDialog>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_AxisRangeBase
{
public:
    QVBoxLayout *vboxLayout;
    QGridLayout *gridLayout;
    QLabel *textLabel1;
    QLineEdit *m_minEntry;
    QLabel *textLabel2;
    QLineEdit *m_maxEntry;
    QHBoxLayout *hboxLayout;
    QPushButton *m_cancelButton;
    QPushButton *m_acceptButton;

    void setupUi(QDialog *AxisRangeBase)
    {
        if (AxisRangeBase->objectName().isEmpty())
            AxisRangeBase->setObjectName(QString::fromUtf8("AxisRangeBase"));
        AxisRangeBase->resize(237, 161);
        vboxLayout = new QVBoxLayout(AxisRangeBase);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        textLabel1 = new QLabel(AxisRangeBase);
        textLabel1->setObjectName(QString::fromUtf8("textLabel1"));
        textLabel1->setWordWrap(false);

        gridLayout->addWidget(textLabel1, 0, 0, 1, 1);

        m_minEntry = new QLineEdit(AxisRangeBase);
        m_minEntry->setObjectName(QString::fromUtf8("m_minEntry"));

        gridLayout->addWidget(m_minEntry, 0, 1, 1, 1);

        textLabel2 = new QLabel(AxisRangeBase);
        textLabel2->setObjectName(QString::fromUtf8("textLabel2"));
        textLabel2->setWordWrap(false);

        gridLayout->addWidget(textLabel2, 1, 0, 1, 1);

        m_maxEntry = new QLineEdit(AxisRangeBase);
        m_maxEntry->setObjectName(QString::fromUtf8("m_maxEntry"));

        gridLayout->addWidget(m_maxEntry, 1, 1, 1, 1);


        vboxLayout->addLayout(gridLayout);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        m_cancelButton = new QPushButton(AxisRangeBase);
        m_cancelButton->setObjectName(QString::fromUtf8("m_cancelButton"));

        hboxLayout->addWidget(m_cancelButton);

        m_acceptButton = new QPushButton(AxisRangeBase);
        m_acceptButton->setObjectName(QString::fromUtf8("m_acceptButton"));
        m_acceptButton->setDefault(true);

        hboxLayout->addWidget(m_acceptButton);


        vboxLayout->addLayout(hboxLayout);


        retranslateUi(AxisRangeBase);
        QObject::connect(m_cancelButton, SIGNAL(clicked()), AxisRangeBase, SLOT(reject()));
        QObject::connect(m_acceptButton, SIGNAL(clicked()), AxisRangeBase, SLOT(accept()));

        QMetaObject::connectSlotsByName(AxisRangeBase);
    } // setupUi

    void retranslateUi(QDialog *AxisRangeBase)
    {
        AxisRangeBase->setWindowTitle(QApplication::translate("AxisRangeBase", "Form1", 0, QApplication::UnicodeUTF8));
        textLabel1->setText(QApplication::translate("AxisRangeBase", "Minimum :", 0, QApplication::UnicodeUTF8));
        textLabel2->setText(QApplication::translate("AxisRangeBase", "Maximum :", 0, QApplication::UnicodeUTF8));
        m_cancelButton->setText(QApplication::translate("AxisRangeBase", "Cancel", 0, QApplication::UnicodeUTF8));
        m_acceptButton->setText(QApplication::translate("AxisRangeBase", "Ok", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class AxisRangeBase: public Ui_AxisRangeBase {};
} // namespace Ui

QT_END_NAMESPACE

class AxisRangeBase : public QDialog, public Ui::AxisRangeBase
{
    Q_OBJECT

public:
    AxisRangeBase(QWidget* parent = 0);
    ~AxisRangeBase();

protected slots:
    virtual void languageChange();

};

#endif // AXISRANGEBASE_H
