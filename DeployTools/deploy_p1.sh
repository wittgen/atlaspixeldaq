#!/bin/bash

if [ ${0##*/} != ${BASH_SOURCE[0]##*/} ]; then 
    echo "ERROR: do not run deploy script a source."
    echo
    return 1
fi



# defaults - may be changed below
BUILD_DIR="/det/pix/daq/current-tdaq9/x86_64-centos7-gcc8-opt"
SETUP="/det/pix/scripts/Default-pixel.sh"


usage () {
  echo
  echo "$(basename $BASH_SOURCE) [-c] [-p] [-s] [-d BUILD_DIR]"
  echo
  
  echo "Compiles and deploys pixel SW in P1 as atpixdaq. Normally, no arguments needed. Uses the TDAQ release defined in Default-pixel.sh."
 
  echo -e "\t-c    Run CMAKE step only"
  echo -e "\t-s    Use TDAQ configured in the current evironment (depends on \$CMTRELEASE)"
  echo -e "\t-d    Build directory. Default ${BUILD_DIR}"
  echo -e "\t-p    Rebuild PPC SW only"
} 

USEDEFAULT=1

# resolve options
while [[ $# -gt 0 ]]; do 
   key="$1"
   case $key in
         -h|--help)
            usage
            exit
            ;;
         -d)
            [[ -z $2 ]] && usage && exit 1
            BUILD_DIR=$(cd $2; pwd)  # abspath
            shift
            shift
            ;;
         -s)
            unset USEDEFAULT
            shift
            ;;
         -c)
            RUNCMAKE=1
            shift
            ;;
         -p)
            BUILDPPC=1
            shift
            ;;
   esac
done

set --
      
# Which environment
if [[ ! -z $USEDEFAULT ]]; then
   echo "Using Default-pixel.sh"

fi

   
[[ -z $CMTRELEASE ]] && echo "\$CMTRELEASE not set"  && usage && exit 1
   
ver=${CMTRELEASE/tdaq-}
SETUP=/det/tdaq/scripts/setup_devel_${ver}.sh


# Run cmake step only
if [[ ! -z $RUNCMAKE ]]; then 
   ROOTDIR=`dirname $BUILD_DIR`

   ssh atpixdaq@pc-atlas-pub-08  -t "source $SETUP &&  hostname && cd $ROOTDIR && cmake_config ${CMTCONFIG} -- -DTDAQ_SYSTEM_INCLUDE=ON -DBUILD_PPC=ON && chmod g+w -R -f ${BUILD_DIR}"
   exit 0
fi

# Run PPC compilation only
if [[ ! -z $BUILDPPC ]]; then  
   ssh atpixdaq@pc-atlas-pub-08 -t "source $SETUP && make -C ${BUILD_DIR}/PPC_build_dir install -j16"
   exit 0
fi



ssh atpixdaq@pc-atlas-pub-08 -t "source $SETUP && make -C ${BUILD_DIR} install -j 16"

