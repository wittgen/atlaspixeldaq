#!/usr/bin/env python

import argparse, shutil, os, getpass
scriptDir=os.path.dirname(os.path.realpath(__file__))
def load_cached_values():
  cache_dict = {}
  try:
    fn = open(os.environ["HOME"]+"/.oks_deploy", "r")
    for line in fn:
      var = line.split()
      cache_dict[var[0]] = var[1]
    fn.close()
  except:
    return cache_dict
  return cache_dict

def save_cached_values(version, target, daqdir):
  fn = open(os.environ["HOME"]+"/.oks_deploy", "w")
  fn.write("VERSION "+version+"\n")
  fn.write("TARGET "+target+"\n")
  fn.write("DAQDIR "+daqdir)
  fn.close()

def replace_username(filename, username):
  fin = open(filename, "rt")
  data = fin.read()
  data = data.replace('<PARSE_USERNAME>', username)
  fin.close()
  fin = open(filename, "wt")
  fin.write(data)
  fin.close()

def replace_daqdir(filename, daqdir):
  fin = open(filename, "rt")
  data = fin.read()
  data = data.replace('<PARSE_DAQDIR>', daqdir)
  fin.close()
  fin = open(filename, "wt")
  fin.write(data)
  fin.close()

def replace_cmtrelease(filename, cmtrelease):
  fin = open(filename, "rt")
  data = fin.read()
  data = data.replace('<PARSE_CMTRELEASE>', cmtrelease)
  fin.close()
  fin = open(filename, "wt")
  fin.write(data)
  fin.close()

def _logpath(path, names):
  print('Deploying %s' % path)
  return []   # nothing will be ignored

if __name__ == "__main__":
  parser = argparse.ArgumentParser(description='Deploy OKS to shared space')
  parser.add_argument('-v', '--version', help='Version to deploy')
  parser.add_argument('-t', '--target', help='Destination target directory')
  parser.add_argument('-u', '--user', help='Username')
  parser.add_argument('-d', '--daqdir', help='Directory where the DAQ software is deployed')
  
  args = parser.parse_args()
  user_home_dir = os.path.expanduser("~")
  cached_vals = load_cached_values()
  #print(cached_vals)
  
  if not args.user:
    username_host = getpass.getuser()
    username_prompt = input('Username ['+username_host+']: ')
    if not username_prompt:
      user = username_host
    else:
      user = username_prompt
  else:
    user = args.user
  
  if not args.version:
    if 'VERSION' in cached_vals:
      version_cache = cached_vals['VERSION']
    else:
      version_cache = "SR1"
    version_prompt = input('Version ['+version_cache+"]: ")
    if not version_prompt:
      version = version_cache
    else:
      version = version_prompt
  else:
    version = args.version
  
  if not args.target:
    if 'TARGET' in cached_vals:
      target_cache = cached_vals['TARGET']
    else:
      target_cache = os.path.realpath(user_home_dir+"/daq")
    target_prompt = input('Target path ['+target_cache+"]: ")
    if not target_prompt:
      target = target_cache
    else:
      target = target_prompt
  else:
    target = args.target
  
  if not args.daqdir:
    if 'DAQDIR' in cached_vals:
      daqdir_cache = cached_vals['DAQDIR']
    else:
      daqdir_cache = os.path.realpath(os.path.expanduser("~")+"/daq")
    daqdir_prompt = input('Daq directory path ['+daqdir_cache+"]: ")
    if not daqdir_prompt:
      daqdir = daqdir_cache
    else:
      daqdir = daqdir_prompt
  else:
    daqdir = args.daqdir
  
  target = os.path.realpath(target.replace("~",user_home_dir))
  daqdir = os.path.realpath(daqdir.replace("~",user_home_dir))
  cmtrelease=os.environ["CMTRELEASE"]

  if daqdir[-1] is "/":
  	daqdir = daqdir[:-1]
  
  if target[-1] is not "/":
    target = target+"/"
  
  baseDir = scriptDir+"/../PixRCD/PixRCD-00-01-00/PixRCDConfiguration/"
  oksDir = baseDir+version
  oksDest = target+version
  
  save_cached_values(version, target, daqdir)

  print("\n----------------------------------------------------------------")
  print("Deploying OKS (%s) for user `%s` into:" % (version, user))
  print(" ......... %s" % target)
  print("DAQ dir set to:")
  print(" ......... %s\n" % daqdir)
  
  if os.path.exists(oksDest):
    shutil.rmtree(oksDest)
  shutil.copytree(oksDir, oksDest, ignore=_logpath)

  replace_daqdir(oksDest+"/partitions/PixelEnv.data.xml", daqdir)
  replace_username(oksDest+"/partitions/PartitionAlti.data.xml", user)
  replace_username(oksDest+"/partitions/PixelEnv.data.xml", user)
  replace_username(oksDest+"/partitions/Partition_Pixel.data.xml", user)
  replace_username(oksDest+"/segments/Segment_PixelInfrastructure.data.xml", user)
  replace_username(oksDest+"/partitions/SR1-DAQSlice.data.xml", user)
  replace_username(oksDest+"/partitions/PIX-GNAM.data.xml", user)
  replace_cmtrelease(oksDest+"/partitions/Partition_Pixel.data.xml", cmtrelease)
  replace_cmtrelease(oksDest+"/partitions/PartitionAlti.data.xml", cmtrelease)
  replace_cmtrelease(oksDest+"/partitions/PixelEnv.data.xml", cmtrelease)
  replace_cmtrelease(oksDest+"/partitions/SR1-DAQSlice.data.xml", cmtrelease)
  replace_cmtrelease(oksDest+"/partitions/PIX-GNAM.data.xml", cmtrelease)

  print("\nWriting setup_everything.sh")
  fn = open(target+"setup_everything.sh", "w")
  fn.write('#!/bin/bash\n')
  fn.write('source '+daqdir+'/installed/setup.sh\n')
  if(version=='localhost'):
    fn.write('export TDAQ_IPC_INIT_REF="file:/tmp/ipc_root.ref"\n')
  else:
    fn.write('export TDAQ_IPC_INIT_REF="file:/tbed/tdaq/sw/ipc/'+cmtrelease+'/ipc_root.ref"\n')
  fn.write('alias start_infr="setup_daq -newgui -al PixelInfr_'+user+' -d '+oksDest+'/partitions/Partition_Pixel.data.xml"\n')
  fn.write('alias start_daqslice="setup_daq -newgui -al PIXSR1-DAQSlice_'+user+' -d '+oksDest+'/partitions/SR1-DAQSlice.data.xml"\n')
  fn.write('alias start_alti="setup_daq -newgui -al PixAlti_'+user+' -d '+oksDest+'/partitions/PartitionAlti.data.xml"\n')
  fn.write('alias start_gnam="setup_daq -newgui -al PIX-GNAM_'+user+' -d '+oksDest+'/partitions/PIX-GNAM.data.xml"\n')
  fn.write('alias start_TagManager="TagManager --dbPart PixelInfr_'+user+'"\n')
  fn.write('alias start_rodmon="RodStatusMonitorApp -p PixelInfr_'+user+'"\n')
  fn.write('\n')

  print("Appending environment to setup_everything.sh")
  if(version=='localhost'):
    env_file = open(scriptDir+"/Environment_sqlite.txt", "r")
  else:
    env_file = open(scriptDir+"/Environment.txt", "r")

  for line in env_file:
    fn.write(line)
  env_file.close()

  fn.close()


  print("----------------------------------------------------------------\n")
