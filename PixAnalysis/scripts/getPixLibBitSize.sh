#!/bin/sh
pix_lib_name=libPixLib.so

has_pix_lib() {
    case $2 in
    (32|64)
       file $1 2>&1 | grep -q " $2-bit" 
       ;;
    (*)
       false
       ;;
    esac
}

if test -n "$PIX_LIB" && test -d $PIX_LIB; then
  pix_lib_name=${PIX_LIB}/libPixLib.so
  if test -e $pix_lib_name; then
     if has_pix_lib $pix_lib_name 32; then
        echo -m32
     elif has_pix_lib $pix_lib_name 64; then
        echo ""
     else
      echo "ERROR: Failed to determine the bit-size of the PixLib : $pix_lib_name" >&2
      false
    fi
  else
     echo "ERROR: Did not find a PixLib in $PIX_LIB" >&2
     false
  fi
else
  echo "ERROR: The environment variable PIX_LIB does not point to a valid directory." >&2
  false
fi

