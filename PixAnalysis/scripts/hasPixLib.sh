#!/bin/sh
if test -n "$PIX_LIB" && test -d $PIX_LIB; then
  if test -e $PIX_LIB/libPixLib.so; then
    case $2 in
    (32|64)
       file $PIX_LIB/libPixLib.so 2>&1 | grep -q " $2-bit" 
       ;;
    (*)
       false
       ;;
    esac
  else
     echo "ERROR: Did not find a PixLib in $PIX_LIB" >&2
     false
  fi
else
  echo "ERROR: The environment variable PIX_LIB does not point to a valid directory." >&2
  false
fi
