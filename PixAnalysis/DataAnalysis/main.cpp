#ifdef HAVE_QTGSI    
#  include "TQRootApplication.h"
#  include "TQApplication.h"
#  include <TEnv.h>
#  include <TSystem.h>
#elif defined(HAVE_QTROOT)
#  include <qapplication.h>
#  include <TQtWidget.h>
#else
#  include <QRootApplication.h>
#endif

#include <qapplication.h>
#include <BaseException.h>
#include <RCCVmeInterface.h>
#include "mainwin.h"
//#include "TopWin.h"
#include "DAEngine.h"

#include <iostream>
#include <sstream>
#include <exception>
#include <memory>
#include <cassert>
int main( int argc, char ** argv )
{
  char *ho = argv[1];
  if(argc<=2)//GUI mode
    {
#ifdef HAVE_QTGSI
      auto_ptr<TQApplication> root_app(new TQApplication("DataAnalysis",&argc,argv));
      auto_ptr<TQRootApplication> app(new TQRootApplication( argc, argv, 0));
      //      std::cout << gEnv->GetValue("Rint.Logon", (char*)0) << std::endl;
      //      const char *logon = gEnv->GetValue("Rint.Logon", (char*)0);
      //      if (logon) {
      //        char *mac = gSystem->Which(TROOT::GetMacroPath(), logon, kReadPermission);
      //        if (mac) {
      //          root_app->ProcessFile(logon);
      //        }
      //        delete [] mac;
      //      }
#elif defined(HAVE_QTROOT)
      auto_ptr<QApplication> app(new QApplication(argc,argv));
#else
       auto_ptr<QRootApplication> app(new QRootApplication( argc, argv, 0,true));
#endif
      assert ( app.get() );
      //TopWin *top = new TopWin(NULL,"MAmain",0,true, &a);
      // MainWin w; 
      DAEngine eng(true);
      MainWin w(&eng, app.get());
      app->connect( app.get(), SIGNAL( lastWindowClosed() ), app.get(), SLOT( quit() ) );
    
      if( app->argc()>1)// load cut file - must be arg. #1
      	eng.loadCuts(ho);//a.argv()[1]);
     
      w.show();
      int retval = -1;
      std::stringstream msg;
      try{
	retval = app->exec();
      } catch (SctPixelRod::VmeException& v) {
	msg << "VME-exception ";
	msg << v;
      } catch(SctPixelRod::BaseException& b){
	msg << "Base exception ";
	msg << b;
      } catch(std::exception& s){
	msg << "Std-lib exception ";
	msg << s.what();
      } catch(...){
	msg << "Unknown exception ";
      }
      if(msg.str()!="")
	std::cerr << msg.str() << " not caught during execution of main window." << std::endl;
      return retval;
    }
  else if(argc>2)//batch mode
    {
      QApplication a( argc, argv, false);
    
      // QListViewItem *lv = new QListViewItem();
      // MainWin w;//= new MainWin();
      //w.show();
      DAEngine eng(false);
      a.connect( &a, SIGNAL( lastWindowClosed() ), &a, SLOT( quit() ) );

      
      for ( int i = 2; i < a.argc(); i++ )  // a.argc() == argc
      	{
      	  eng.loadFile(a.argv()[i]);
      	}
      eng.loadCuts(a.argv()[1]);
      eng.startAnalyse("All Tests");
    }
}
