#ifndef _ConfObjUtil_h_
#define _ConfObjUtil_h_

#include <Config/Config.h>
#include <string>
#include <vector>
#include <assert.h>

#include "pixa_exception.h"

#ifndef TrimVal_t_h
#define TrimVal_t_h

namespace PixLib {
  typedef unsigned short TrimVal_t;
}

#endif
 
#ifndef TrimVal_t_h
#define TrimVal_t_h

namespace PixLib {
  typedef unsigned short TrimVal_t;
}

#endif
 
template <class T>
inline PixLib::ConfObj::types confTypeID();

template<> inline  PixLib::ConfObj::types  confTypeID<int>()           {return PixLib::ConfObj::INT;}
template<> inline  PixLib::ConfObj::types  confTypeID<unsigned int>()  {return PixLib::ConfObj::INT;}
template<> inline  PixLib::ConfObj::types  confTypeID<short>()         {return PixLib::ConfObj::INT;}
template<> inline  PixLib::ConfObj::types  confTypeID<unsigned short>(){return PixLib::ConfObj::INT;}
template<> inline  PixLib::ConfObj::types  confTypeID<char>()          {return PixLib::ConfObj::INT;}
template<> inline  PixLib::ConfObj::types  confTypeID<unsigned char>() {return PixLib::ConfObj::INT;}

//#ifdef _CPP_VECTOR
#if 1

// vector handling

template <class T> const std::vector<T> &vectorVal(const PixLib::ConfObj &a);
template <class T> std::vector<T> &vectorVal(PixLib::ConfObj &a);

template<> inline  PixLib::ConfObj::types  confTypeID<std::vector<int> >()          {return PixLib::ConfObj::VECTOR;}
template<> inline  PixLib::ConfObj::types  confTypeID<std::vector<unsigned int> >() {return PixLib::ConfObj::VECTOR;}
template<> inline  PixLib::ConfObj::types  confTypeID<std::vector<float> >()        {return PixLib::ConfObj::VECTOR;}

template <class T>
inline PixLib::ConfVector::subtypes vectorTypeID();

template<> inline  PixLib::ConfVector::subtypes  vectorTypeID<int>()            {return PixLib::ConfVector::V_INT;}
template<> inline  PixLib::ConfVector::subtypes  vectorTypeID<unsigned int>()   {return PixLib::ConfVector::V_UINT;}
template<> inline  PixLib::ConfVector::subtypes  vectorTypeID<float>()   {return PixLib::ConfVector::V_FLOAT;}

template <class T> const std::vector<T> &getVectorVal(const PixLib::ConfVector &vector);
template <class T> std::vector<T> &getVectorVal(PixLib::ConfVector &vector);

template <> inline       std::vector<int> &getVectorVal<int>(PixLib::ConfVector &a_vector)       { return a_vector.valueVInt();}
template <> inline const std::vector<int> &getVectorVal<int>(const PixLib::ConfVector &a_vector) { return const_cast<PixLib::ConfVector &>(a_vector).valueVInt();}

template <> inline      std::vector<unsigned int> &getVectorVal<unsigned int>(PixLib::ConfVector &a_vector)       { return a_vector.valueVUint(); }
template <> inline const std::vector<unsigned int> &getVectorVal<unsigned int>(const PixLib::ConfVector &a_vector) 
{ return const_cast<PixLib::ConfVector &>(a_vector).valueVUint(); }

template <> inline      std::vector<float> &getVectorVal<float>(PixLib::ConfVector &a_vector)        { return a_vector.valueVFloat(); }
template <> inline const std::vector<float> &getVectorVal<float>(const PixLib::ConfVector &a_vector)  { return const_cast<PixLib::ConfVector &>(a_vector).valueVFloat(); }




// read only
template <class T>
inline const std::vector<T> &vectorVal(const PixLib::ConfObj &a) {
  assert(const_cast<PixLib::ConfObj &>(a).type()==confTypeID<std::vector<T> >());
  const PixLib::ConfVector &a_vector=static_cast<const PixLib::ConfVector &>(a);
  assert(a_vector.subtype()== vectorTypeID<T>() );
  return getVectorVal<T>(a_vector);
}

// read / write 
template <class T>
inline std::vector<T> &vectorVal(PixLib::ConfObj &a) {
  assert(const_cast<PixLib::ConfObj &>(a).type()==confTypeID<std::vector<T> >());
  PixLib::ConfVector &a_vector=static_cast<PixLib::ConfVector &>(a);
  assert(a_vector.subtype() == vectorTypeID<T>() );
  return getVectorVal<T>(a_vector);
}

// matrix handling

template <class T>
inline PixLib::ConfMatrix::subtypes matrixTypeID();

template<> inline  PixLib::ConfMatrix::subtypes  matrixTypeID<PixLib::TrimVal_t>()            {return PixLib::ConfMatrix::M_U16;}
template<> inline  PixLib::ConfMatrix::subtypes  matrixTypeID<bool>()                      {return PixLib::ConfMatrix::M_U1;}

template <class T> T confMatrixVal(const PixLib::ConfMatrix &a_matrix, unsigned int col, unsigned int row);
template <class T> T confMatrixVal(const PixLib::ConfObj &a_matrix_obj, unsigned int col, unsigned int row);

template <class T> void setConfMatrixVal(PixLib::ConfMatrix &a_matrix, unsigned int col, unsigned int row, T val);
template <class T> void setConfMatrixVal(PixLib::ConfObj &a_matrix_obj, unsigned int col, unsigned int row, T val);


template <class T> const PixLib::ConfMatrix &confMatrix(const PixLib::ConfObj &a_matrix_obj);
template <class T> PixLib::ConfMatrix &confMatrix(PixLib::ConfObj &a_matrix_obj);

//template <class T> std::vector< std::vector<T> > &getMatrixVal(PixLib::ConfMatrix &a_matrix);
//template <class T> const std::vector< std::vector<T> > &getMatrixVal(const PixLib::ConfMatrix &a_matrix);

//template <> inline std::vector< std::vector<PixLib::TrimVal_t> > &getMatrixVal<PixLib::TrimVal_t>(PixLib::ConfMatrix &a_matrix) { return a_matrix.valueU16().get(); }
//template <> inline const std::vector< std::vector<PixLib::TrimVal_t> > &getMatrixVal<PixLib::TrimVal_t>(const PixLib::ConfMatrix &a_matrix) 
//{ return const_cast<PixLib::ConfMatrix &>(a_matrix).valueU16().get(); }


template <> inline PixLib::TrimVal_t confMatrixVal<PixLib::TrimVal_t>(const PixLib::ConfMatrix &a_matrix, unsigned int col, unsigned int row) 
{ return static_cast<PixLib::TrimVal_t>(const_cast<PixLib::ConfMatrix &>(a_matrix).valueU16().get(col,row)); }

template <> inline bool confMatrixVal<bool>(const PixLib::ConfMatrix &a_matrix, unsigned int col, unsigned int row) 
{ return static_cast<bool>(const_cast<PixLib::ConfMatrix &>(a_matrix).valueU1().get(col,row)); }

template <class T> inline T confMatrixVal(const PixLib::ConfObj &a_matrix_obj, unsigned int col, unsigned int row) {
   PixLib::ConfMatrix &a_matrix=confMatrix<T>(a_matrix_obj);
   return confMatrixVal<T>(a_matrix, col, row);
}

template <class T> inline PixLib::ConfMatrix &confMatrix(PixLib::ConfObj &a_matrix_obj) {
   assert ( a_matrix_obj.type() == PixLib::ConfObj::MATRIX );
   PixLib::ConfMatrix &a_matrix=static_cast<PixLib::ConfMatrix &>(a_matrix_obj);
   assert(a_matrix.subtype() == matrixTypeID<T>() );
   return a_matrix;
}

template <> inline void setConfMatrixVal<PixLib::TrimVal_t>(PixLib::ConfMatrix &a_matrix, unsigned int col, unsigned int row, PixLib::TrimVal_t val) 
{ a_matrix.valueU16().set(col,row,val); }

template <> inline void setConfMatrixVal<bool>(PixLib::ConfMatrix &a_matrix, unsigned int col, unsigned int row, bool val) 
{ a_matrix.valueU1().set(col,row,val); }

template <class T> inline void setConfMatrixVal(PixLib::ConfObj &a_matrix_obj, unsigned int col, unsigned int row, T val) {
   PixLib::ConfMatrix &a_matrix=confMatrix<T>(a_matrix_obj);
   setConfMatrixVal<T>(a_matrix, col, row,val);
}

template <class T> inline const PixLib::ConfMatrix &confMatrix(const PixLib::ConfObj &a_matrix_obj) {
  return confMatrix<T>(const_cast<PixLib::ConfObj &>(a_matrix_obj));
}



// template <> inline  std::vector< std::vector<bool> > &getMatrixVal<bool>(PixLib::ConfMatrix &a_matrix) { return a_matrix.valueU1().get(); }
// template <> inline  const std::vector< std::vector<bool> > &getMatrixVal<bool>(const PixLib::ConfMatrix &a_matrix) 
// { return const_cast<PixLib::ConfMatrix &>(a_matrix).valueU1().get(); }

// template <class T> inline std::vector< std::vector<T> > &confMatrix(PixLib::ConfObj &a, unsigned int n_col, unsigned int n_row ) 
// {
//   assert ( a.type() == PixLib::ConfObj::MATRIX );
//   PixLib::ConfMatrix &a_matrix=static_cast<PixLib::ConfMatrix &>(a);
//   assert(a_matrix.subtype() == matrixTypeID<T>() );
//   std::vector<std::vector<T> > &vec_val = getMatrixVal<T>(a_matrix);
//   assert( vec_val.size() == n_col);
//   if (n_col>0) assert( vec_val.back().size() == n_row);
//   return vec_val;
// }

// template <class T> inline const std::vector< std::vector<T> > &confMatrix(const PixLib::ConfObj &a, unsigned int n_col, unsigned int n_row ) 
// {
//   assert ( const_cast<PixLib::ConfObj &>(a).type() == PixLib::ConfObj::MATRIX );
//   PixLib::ConfMatrix &a_matrix=static_cast<const PixLib::ConfMatrix &>(a);
//   assert(const_cast<PixLib::ConfMatrix>(a_matrix).subtype() == matrixTypeID<T>() );
//   std::vector<std::vector<T> > &vec_val = getMatrixVal<T>(a_matrix);
//   assert( vec_val.size() == n_col);
//   if (n_col>0) assert( vec_val.back().size() == n_row);
//   return vec_val;
// }

#endif

// simple type handling

template<> inline  PixLib::ConfObj::types  confTypeID<float>()         {return PixLib::ConfObj::FLOAT;}
template<> inline  PixLib::ConfObj::types  confTypeID<std::string>()   {return PixLib::ConfObj::STRING;}
template<> inline  PixLib::ConfObj::types  confTypeID<bool>()          {return PixLib::ConfObj::BOOL;}

template <class T>
inline PixLib::ConfInt::subtypes intTypeID();

template<> inline  PixLib::ConfInt::subtypes  intTypeID<int>()            {return PixLib::ConfInt::S32;}
template<> inline  PixLib::ConfInt::subtypes  intTypeID<unsigned int>()   {return PixLib::ConfInt::U32;}
template<> inline  PixLib::ConfInt::subtypes  intTypeID<short>()          {return PixLib::ConfInt::S16;}
template<> inline  PixLib::ConfInt::subtypes  intTypeID<unsigned short>() {return PixLib::ConfInt::U16;}
template<> inline  PixLib::ConfInt::subtypes  intTypeID<char>()           {return PixLib::ConfInt::S8;}
template<> inline  PixLib::ConfInt::subtypes  intTypeID<unsigned char>()  {return PixLib::ConfInt::U8;}

template <class T>
inline const T &getIntVal(const PixLib::ConfInt &a);

template<> inline const int            &getIntVal( const PixLib::ConfInt &a ) { return const_cast<PixLib::ConfInt &>(a).valueS32();}
template<> inline const unsigned int   &getIntVal( const PixLib::ConfInt &a ) { return const_cast<PixLib::ConfInt &>(a).valueU32();}
template<> inline const short          &getIntVal( const PixLib::ConfInt &a ) { return const_cast<PixLib::ConfInt &>(a).valueS16();}
template<> inline const unsigned short &getIntVal( const PixLib::ConfInt &a ) { return const_cast<PixLib::ConfInt &>(a).valueU16();}
template<> inline const char           &getIntVal( const PixLib::ConfInt &a ) { return const_cast<PixLib::ConfInt &>(a).valueS8(); }
template<> inline const unsigned char  &getIntVal( const PixLib::ConfInt &a ) { return const_cast<PixLib::ConfInt &>(a).valueU8(); }

template <class T>
inline T &getIntVal(PixLib::ConfInt &a);
template<> inline int            &getIntVal( PixLib::ConfInt &a ) { return const_cast<PixLib::ConfInt &>(a).valueS32();}
template<> inline unsigned int   &getIntVal( PixLib::ConfInt &a ) { return const_cast<PixLib::ConfInt &>(a).valueU32();}
template<> inline short          &getIntVal( PixLib::ConfInt &a ) { return const_cast<PixLib::ConfInt &>(a).valueS16();}
template<> inline unsigned short &getIntVal( PixLib::ConfInt &a ) { return const_cast<PixLib::ConfInt &>(a).valueU16();}
template<> inline char           &getIntVal( PixLib::ConfInt &a ) { return const_cast<PixLib::ConfInt &>(a).valueS8(); }
template<> inline unsigned char  &getIntVal( PixLib::ConfInt &a ) { return const_cast<PixLib::ConfInt &>(a).valueU8(); }

template <class T>
inline const T &getVal(const PixLib::ConfObj &a);

template <class T>
inline T &getVal(PixLib::ConfObj &a);

template<> inline const float &getVal<float>( const PixLib::ConfObj &a ) { return const_cast< PixLib::ConfFloat &>(static_cast<const PixLib::ConfFloat &>(a)).value(); }
template<> inline float &getVal<float>( PixLib::ConfObj &a ) { return static_cast<PixLib::ConfFloat &>(a).value(); }

template<> inline const bool &getVal<bool>( const PixLib::ConfObj &a ) { return const_cast< PixLib::ConfBool &>(static_cast<const PixLib::ConfBool &>(a)).value(); }
template<> inline bool &getVal<bool>( PixLib::ConfObj &a ) { return static_cast<PixLib::ConfBool &>(a).value(); }

template<> inline const std::string &getVal<std::string>( const PixLib::ConfObj &a )
{ return const_cast< PixLib::ConfString &>(static_cast<const PixLib::ConfString &>(a)).value(); }
template<> inline std::string &getVal<std::string>( PixLib::ConfObj &a )
{ return static_cast<PixLib::ConfString &>(a).value(); }


// read only
template <class T>
inline const T &intVal(const PixLib::ConfObj &a) {
  assert(const_cast<PixLib::ConfObj &>(a).type()==confTypeID<T>());
  const PixLib::ConfInt &a_int=static_cast<const PixLib::ConfInt &>(a);
  assert(const_cast<PixLib::ConfInt &>(a_int).subtype()== intTypeID<T>() );
  return getIntVal<T>(a_int);
}

// read / write 
template <class T>
inline T &intVal(PixLib::ConfObj &a) {
  PixLib::ConfInt *a_int=dynamic_cast<PixLib::ConfInt *>(&a);
  assert( a_int );
  if ( a_int->subtype() != intTypeID<T>() ) {
    throw PixA::ConfigTypeMismatch();
  }
  return getIntVal<T>(*a_int);
}



template <class T>
const T &confVal(const PixLib::ConfObj &a) {
  assert( const_cast<PixLib::ConfObj &>(a).type()==confTypeID<T>() );
  return getVal<T>(a);
}

template <class T>
T &confVal(PixLib::ConfObj &a) {
  assert( a.type()==confTypeID<T>() );
  return getVal<T>(a);
}

template<> inline int            &confVal<int>(PixLib::ConfObj &a) { return intVal<int>(a); }
template<> inline unsigned int   &confVal<unsigned int>(PixLib::ConfObj &a) { return intVal<unsigned int>(a); }
template<> inline short          &confVal<short>(PixLib::ConfObj &a) { return intVal<short>(a); }
template<> inline unsigned short &confVal<unsigned short>(PixLib::ConfObj &a) { return intVal<unsigned short>(a); }
template<> inline char           &confVal<char>(PixLib::ConfObj &a) { return intVal<char>(a); }
template<> inline unsigned char  &confVal<unsigned char>(PixLib::ConfObj &a) { return intVal<unsigned char>(a); }

template<> inline const int            &confVal<int>(const PixLib::ConfObj &a) { return intVal<int>(a); }
template<> inline const unsigned int   &confVal<unsigned int>(const PixLib::ConfObj &a) { return intVal<unsigned int>(a); }
template<> inline const short          &confVal<short>(const PixLib::ConfObj &a) { return intVal<short>(a); }
template<> inline const unsigned short &confVal<unsigned short>(const PixLib::ConfObj &a) { return intVal<unsigned short>(a); }
template<> inline const char           &confVal<char>(const PixLib::ConfObj &a) { return intVal<char>(a); }
template<> inline const unsigned char  &confVal<unsigned char>(const PixLib::ConfObj &a) { return intVal<unsigned char>(a); }

template<> inline std::vector<int>           &confVal< std::vector<int> >(PixLib::ConfObj &a)          { return vectorVal<int>(a); }
template<> inline std::vector<unsigned int>  &confVal< std::vector<unsigned int> >(PixLib::ConfObj &a) { return vectorVal<unsigned int>(a); }
template<> inline std::vector<float>         &confVal< std::vector<float> >(PixLib::ConfObj &a)        { return vectorVal<float>(a); }

template<> inline const std::vector<int>           &confVal< std::vector<int> >(const PixLib::ConfObj &a)          { return vectorVal<int>(a); }
template<> inline const std::vector<unsigned int>  &confVal< std::vector<unsigned int> >(const PixLib::ConfObj &a) { return vectorVal<unsigned int>(a); }
template<> inline const std::vector<float>         &confVal< std::vector<float> >(const PixLib::ConfObj &a)        { return vectorVal<float>(a); }

inline bool isList(const PixLib::ConfObj &a) {return const_cast<PixLib::ConfObj &>(a).type()==PixLib::ConfObj::LIST;}

#endif
