#include  "PixConfigWrapper.h"
#include  "PixConfGrpWrapper.h"

namespace PixA {

  IConfGrp &PixConfigWrapper::operator[](const std::string &name)  {
    PixLib::ConfGroup &conf_grp=(*m_config)[name];
    if (conf_grp.name() == s_trashConfGrp) {
      std::stringstream message;
      message << "FATAL [PixConfigWrapper::subConfig] No ConfGrp of name = " << name
	      << " in config " << m_config->name() << "."  << std::endl;
      throw ConfigException(message.str());
    }
    return *(new PixConfGrpWrapper(*this, conf_grp));
  }


  std::string PixConfigWrapper::s_trashConfig("__TrashConfig__");
  std::string PixConfigWrapper::s_trashConfGrp("__TrashConfGroup__");

#ifdef DEBUG
  ObjList PixConfigWrapper::s_objList;
#endif
}



