#ifndef _DbRef_h_
#define _DbRef_h_

#include <memory>
#include "PixDbAccessor.h"

namespace PixA {

  //  class PixDbAccessor;
  class DbHandle;
  class DbManager;

  class DbRef
  {
    friend class DbHandle;
  protected:
    DbRef(std::shared_ptr<PixDbAccessor> accessor) : m_accessor(accessor) { m_accessor->accessStart();}

  public:
    ~DbRef()  { m_accessor->accessEnd();}

    PixLib::dbRecordIterator findRecord(const std::string &name) { return m_accessor->findRecord(name); }
    PixLib::dbRecordIterator recordBegin() { return m_accessor->recordBegin(); }
    PixLib::dbRecordIterator recordEnd() { return m_accessor->recordEnd(); }

    PixLib::dbFieldIterator findField(const std::string &name) { return m_accessor->findField(name); }
    PixLib::dbFieldIterator fieldBegin() { return m_accessor->fieldBegin(); }
    PixLib::dbFieldIterator fieldEnd() { return m_accessor->fieldEnd(); }

    DbHandle access(PixLib::dbRecordIterator &record_iterator);

    bool operator==(const DbRef &a_ref) const { return m_accessor == a_ref.m_accessor;}

    std::shared_ptr<PixLib::DbRecord> record() { return m_accessor->record(); }

    std::shared_ptr<PixLib::DbRecord> addRecord(const std::string &class_name, const std::string &slot_name) { return m_accessor->addRecord(class_name, slot_name); }

    std::string name() const {m_accessor->prepareDbUsage(); return m_accessor->name();}
    std::string pathName() const {m_accessor->prepareDbUsage(); return m_accessor->pathName();}
    std::string className() const {m_accessor->prepareDbUsage(); return m_accessor->className();}

    bool empty() const {m_accessor->prepareDbUsage(); return m_accessor->empty();}
  private:
    std::shared_ptr<PixDbAccessor> m_accessor;
  };

  class DbHandle {
    friend class DbManager;
    friend class PixDbHistoAccessor;
    friend class DbRef;
  protected:
    DbHandle(PixDbAccessor *accessor) : m_accessor(accessor) {}

    /** Only to be used by the db manager.
     */
      const PixDbAccessor &accessor() const {//std::cout<<"passing by here"<<std::endl;
          return *(m_accessor.get());}

  public:
    DbHandle(const DbHandle &a_handle) : m_accessor(a_handle.m_accessor) { }
    DbHandle()  {}

    DbHandle &operator=(const DbHandle &a_handle) { m_accessor= a_handle.m_accessor; return *this; }

    bool operator==(const DbHandle &a_handle) const { return m_accessor == a_handle.m_accessor;}

    operator bool() const {return m_accessor.get() != NULL;}

    const DbRef ref() const {
      return DbRef(m_accessor);
    }

    DbRef ref()  {
      return DbRef(m_accessor);
    }

    DbHandle writeHandle() {
      return DbHandle(new PixDbAccessor(*(m_accessor.get()),false));
    }

  private:
    std::shared_ptr<PixDbAccessor> m_accessor;
  };

  inline DbHandle DbRef::access(PixLib::dbRecordIterator &record_iterator) {
    return DbHandle(new PixDbAccessor(*(m_accessor.get()), record_iterator) );
  }

}

#endif
