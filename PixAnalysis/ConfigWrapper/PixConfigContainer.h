#ifndef _PixConfigContainer_h_
#define _PixConfigContainer_h_

#include "IConfigContainer.h"

namespace PixA {
  class PixConfigContainer : public IConfigContainer
  {
  public:
    PixConfigContainer(ConfigRef &module_config, ConfigRef &)
     ~PixConfigContainer() {}

    const ConfRef &getModuleConfig() const;

    const ConfRef &getBocConfig() const ;
    // virtual const PixLib::Config &getFullBocConfig() const = 0;

    const ConfRef &getRodConfig() const;
    //    virtual const PixLib::Config &getFullRidConfig() const = 0;

  };
}

#endif
