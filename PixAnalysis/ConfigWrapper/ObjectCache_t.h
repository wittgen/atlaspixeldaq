#ifndef _PixA_ObjectCache_t_h_
#define _PixA_ObjectCache_t_h_

#include "Connectivity.h"
#include <map>
#include <set>
#include <string>
//#include <iostream>
#include <climits>

namespace PixA {

  template <class T>
  class UseStat_t {
  public:
    UseStat_t(const T &a, short time) : m_obj(a), m_time(time) {}
    UseStat_t(const UseStat_t &a) : m_obj(a.m_obj), m_time(a.m_time) {}

    T &obj(short time) { m_time=time; return m_obj;}
    const T &obj(short time) const {m_time=time; return m_obj;}

    short time() const { return m_time; }
    bool flipTime() { if (m_time<0) return false; m_time = m_time - SHRT_MAX; return true;}

  private:
    T m_obj;
    mutable short m_time;
  };

  /** Helper class to cache hardware configuration data structures e.g. PixModule, PixBoc.
   * The implementations are mostly in ObjectCache_t_impl.h.
   */
  template <class T>
  class ObjectCache_t
  {
  public:
    ObjectCache_t(unsigned short max_objects = s_nOjectsMax) : m_nObjects(0), m_nObjectsMax(max_objects),m_time(0) {}

    void setMaxObjects(unsigned n_objects_max ) { m_nObjectsMax = n_objects_max; }

    void add(const std::string &name, const Revision_t &revision, const T &a);

    T *object(const std::string &name, const Revision_t &revision) {
      return _object(name,revision);
    }

    const T *object(const std::string &name, const Revision_t &revision) const {
      return const_cast<const T*>( const_cast<ObjectCache_t<T> *>(this)->_object(name,revision) );
    }

    void cleanup(bool execute);

    void aggressiveCleanup();

    unsigned int size() { return m_nObjects; }

  private:
    T *_object(const std::string &name, const Revision_t &revision);

    bool flipTime() const { return m_time == SHRT_MAX; };

    void flipObjTime();

    void incTime()  { if (flipTime()) { flipObjTime(); m_time=0;} else { m_time++;} };

    short currentTime() const { return m_time; }

    bool tooOld(short obj_time) const { return m_time - obj_time > static_cast<short>(m_nObjectsMax); }

    typedef std::map<Revision_t, UseStat_t<T> >     RevisionList_t;
    typedef std::map<std::string, RevisionList_t>   ObjectList_t;

    unsigned short m_nObjects;
    unsigned short m_nObjectsMax;
    static const unsigned short s_nOjectsMax = 100;

    short        m_time;
    ObjectList_t m_objectList;
  };

}

#endif
