#include "DbManager.h"
#include "RootDbOpenHelper.h"
#include "CoralDbOpenHelper.h"

#include "Connectivity.h"

#include <PixModule/PixModule.h>

#include <PixBoc/PixBoc.h>
#include <PixTrigController/TimPixTrigController.h>

#include <ConfigRootIO/ConfigStoreFactory.h>


#include "CoralDBRevisionIndex.h"


#include "Regex_t.h"

// umask
// mkdir

//*************************************

namespace PixA {

  DbWrapper *DbManager::getDbWrapper(std::string full_name, std::string name) {
      
    //std::cout<<"names are: "<<full_name<<" "<<name<<std::endl;
    std::string wildcd ="_#FLAV";
    std::string FEflav;
    std::size_t start_pos = 0;
//      std::cout<<"e qui ti trovo"<<std::endl;
      if(name=="") FEflav="";
      else if(name=="_I4"){FEflav="_I4";}
      else{
    // test: replace wild card with something temporarily useful
            
//*****pixconnobjconfdb

//          std::cout<<"name is: " << name <<std::endl;
          std::string IDname=name, flname="";
          int pos = name.find("_flavour");
          int mccFlv_1 = PixLib::PixModule::PM_MCC_I2;
          int feFlv_1 = PixLib::PixModule::PM_FE_I2;
//          std::cout<<"mccFlv: "<<mccFlv_1<<std::endl;
//          std::cout<<"feFlv: "<<feFlv_1<<std::endl;
          int nFe_1=16;
          if(pos!=(int)std::string::npos){
              IDname = name.substr(0,pos);
              flname = name.substr(pos+9, name.length()-pos-9);
              int flpos=-1, oldpos=0;
              for(int i=0;i<3;i++){
                  std::stringstream ab;
                  oldpos = flpos+1;
                  flpos = flname.find("-", oldpos);
                  if(flpos==(int)std::string::npos) flpos = flname.length();
                  ab << flname.substr(oldpos,flpos-oldpos);
                  switch(i){
                      case 0:
                          ab >> mccFlv_1;
                          break;
                      case 1:
                          ab >> feFlv_1;
                          break;
                      case 2:
                          ab >> nFe_1;
                          break;
                  }
              }
          }
          
//****************
          
//***PixModulePart
          
          if(mccFlv_1==PixLib::PixModule::PM_MCC_I2 && feFlv_1==PixLib::PixModule::PM_FE_I2){
	    //std::cout << "Module " << IDname << " is of type FE-I3/MCC "<< std::endl;
              FEflav="";
          }
          else if(mccFlv_1==PixLib::PixModule::PM_NO_MCC && (feFlv_1&PixLib::PixModule::PM_FE_I4)!=0){
	    //std::cout << "Module " << IDname << " is of type FE-I4 "<< feFlv_1 << ", " << nFe_1 <<std::endl;
              FEflav="_I4";
          }
          else     std::cerr << " Inconsistent or non-existing MCC/FE types for module " << IDname <<  std::endl;

          
//****************
          
      }
    while( (start_pos = full_name.find(wildcd,start_pos)) != std::string::npos ){
      full_name.replace(start_pos,wildcd.length(),FEflav);
      start_pos += FEflav.length();
    }
    //std::cout<<"full name: "<<full_name<<std::endl;
    DbList_t::iterator db_iter = m_dbList.find(full_name);
    if (db_iter == m_dbList.end()) {
      std::string::size_type pos = full_name.rfind(".");
      DbWrapper *db_wrapper=NULL;
      if (pos != std::string::npos && full_name.compare(pos,full_name.size()-pos, ".root")==0) {
	  db_wrapper = new DbWrapper(new RootDbOpenHelper(full_name));
      }
      else if (pos != std::string::npos && full_name.compare(pos,full_name.size()-pos, ".sql")==0) {
	if (full_name.compare(0,12,"sqlite_file:")!=0) {
	  std::stringstream message;
	  message << "FATAL [DbManager::getDbWrapper] SQLite file \"" << full_name << "\"given but not preceded by \"sqlite_file:\".";
	  throw PixLib::PixDBException(message.str());
	}
	db_wrapper = new DbWrapper(new CoralDbOpenHelper(full_name));
      }
      else if (pos == std::string::npos || full_name.compare(pos,full_name.size()-pos, ".sql")==0) {
	db_wrapper = new DbWrapper(new CoralDbOpenHelper(full_name));
      }
      if (!db_wrapper) {
	std::stringstream message;
	message << "FATAL [DbManager::getDbWrapper] Failed to open db " << full_name << ".";
	throw PixA::ConfigException(message.str());
      }
      std::pair<DbList_t::iterator,bool> ret = m_dbList.insert(std::make_pair(full_name, db_wrapper));
      if (!ret.second) {
	std::stringstream message;
	message << "FATAL [DbManager::getDbWrapper] Failed to insert db " << full_name << " into internal db list (Internal error).";
	throw PixA::ConfigException(message.str());
      }
      return ret.first->second;
    }
    return db_iter->second;
  }

  void DbManager::destroy(DbWrapper *db) {
    for(DbList_t::iterator db_iter = m_dbList.begin();
	db_iter != m_dbList.end();
	db_iter++) {
      if (db_iter->second == db) {
	delete db_iter->second;
	m_dbList.erase(db_iter);
	return;
      }
    }

    std::cerr << "WARNING [DbWrapper::destroy] Not in list. Destroy anyway." << std::endl;
    delete db;
  }


  ConfigHandle DbManager::wrapConfigRecord(PixLib::PixDbInterface *db, PixLib::dbRecordIterator &record_iter) {
    return *(new OnDemandConfig(*(new DbWrapper(db)),record_iter ));
  }

  ConfigHandle DbManager::config(const std::string &db_name, const std::vector<std::string> &record_hierarchy) {
    return *(new OnDemandConfig(*getDbWrapper(db_name),record_hierarchy));
  }

  ConfigHandle DbManager::config(const std::string &db_name, const std::string &top_level_record_name) {
    return *(new OnDemandConfig(*getDbWrapper(db_name),top_level_record_name));
  }

  ConfigHandle DbManager::config(const DbHandle &handle) {
    return *(new OnDemandConfig(handle.accessor()));
  }

  PixDbAccessor *DbManager::db(const std::string &db_name, const std::vector<std::string> &record_hierarchy, bool read_only) {
    return new PixDbAccessor(*getDbWrapper(db_name), record_hierarchy,read_only);
  }

  PixDbAccessor *DbManager::db(const std::string &db_name, const std::string &top_level_record_name, bool read_only) {
    return new PixDbAccessor(*getDbWrapper(db_name), top_level_record_name,read_only);
  }

  PixDbAccessor *DbManager::db(const std::string &db_name, bool read_only) {
    return new PixDbAccessor(*getDbWrapper(db_name),read_only);
  }
    PixDbAccessor *DbManager::db_1(const std::string &db_name,std::string string, bool read_only) {
        return new PixDbAccessor(*getDbWrapper(db_name,string),read_only);
    }

  DbHandle DbManager::handle(const std::string &db_name, const std::vector<std::string> &record_hierarchy, bool read_only) {
    return DbHandle(db(db_name,record_hierarchy,read_only));
  }

  DbHandle DbManager::handle(const std::string &db_name, const std::string &top_level_record_name, bool read_only) {
    return DbHandle(db(db_name,top_level_record_name,read_only));
  }

  DbHandle DbManager::handle(const std::string &db_name, bool read_only) {
    return DbHandle(db(db_name,read_only));
  }
    DbHandle DbManager::handle_1(const std::string &db_name,std::string string,bool read_only) {
        return DbHandle(db_1(db_name,string,read_only));
    }

  DbManager::~DbManager() {
    for(DbList_t::iterator db_iter=m_dbList.begin();
	db_iter!=m_dbList.end();
	db_iter++) {
      assert (db_iter->second->use_count() == 0);
      delete db_iter->second;
    }
  }

  bool DbManager::cleanup() {
    for(DbList_t::iterator db_iter=m_dbList.begin();
	db_iter!=m_dbList.end();
	db_iter++) {
      std::cout << "INFO [DbManager::cleanup] " << db_iter->first << " has " 
		<< db_iter->second->use_count() << " users and "
		<< db_iter->second->accessors() << " accessors."
		<< std::endl;
      if (db_iter->second->accessors() == 0 && db_iter->second->use_count() == 0) {
	std::cout << "INFO [DbManager::cleanup] delete " << db_iter->first << "." << std::endl;
	delete db_iter->second;
	db_iter->second=NULL;
	m_dbList.erase(db_iter);
      }
    }
    return !m_dbList.empty();
  }

  void DbManager::deepCleanup()  {
      if (s_instance) {
	s_instance->cleanup();
	if (s_instance->m_dbList.empty()) {
	  delete s_instance;
	  s_instance=NULL;
	}
      }
    }


  void DbManager::showStat() {
    unsigned int counter=0;
    for(DbList_t::iterator db_iter=m_dbList.begin();
	db_iter!=m_dbList.end();
	db_iter++, counter++) {
      std::cout << std::setw(3) << counter 
		<< " : use count = " << std::setw(3) << db_iter->second->use_count() 
		<< " : users = " << std::setw(3) << db_iter->second->m_dbUsers
		<< " : accessors = " << std::setw(3) << db_iter->second->m_nAccessors;
      if (db_iter->second->m_db.first && db_iter->second->m_db.second) {
	std::cout << db_iter->second->m_db.second->getDecName();
      }
      std::cout << std::endl;
    }
  }

  void DbManager::stat() {
    instance()->showStat();
  }

  DbManager *DbManager::s_instance;

}
