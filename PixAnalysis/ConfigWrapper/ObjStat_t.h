#ifndef _ObjStat_t_h_
#define _ObjStat_t_h_

#include <string>
#include <set>

/** Auxilliary class to debug/monitor object creation and destruction.
*/
class ObjStat_t
{
 public:
  ObjStat_t(const std::string &
#ifdef DEBUG
	    name
#endif
	    )
#ifdef DEBUG
    : m_name(name), m_nCtor(0), m_nDtor(0),m_n(0) 
#endif
    {
#ifdef DEBUG
    s_objStatList.insert(this);
#endif
  }

  ~ObjStat_t() {
#ifdef DEBUG
    dump(); s_objStatList.erase(this);
#endif
  }

  void ctor() {
#ifdef DEBUG
    m_nCtor++; m_n++;
#endif
}
  void dtor() { 
#ifdef DEBUG
    m_nDtor++; m_n--;
#endif
  }

  bool clean() const {
#ifdef DEBUG
    return m_n==0;
#else
    return true;
#endif
  }

  void dump() const
#ifdef DEBUG
    ;
#else
    {}
#endif

  static void dumpAll()
#ifdef DEBUG
    ;
#else
    {}
#endif

 private:

#ifdef DEBUG
  std::string m_name;
  unsigned m_nCtor;
  unsigned m_nDtor;
  unsigned m_n;

  static std::set<ObjStat_t *> s_objStatList;
#endif
};

#endif
