#ifndef _PixA_ConnectivityUtil_h_
#define _PixA_ConnectivityUtil_h_

#include "Connectivity.h"
#include <PixConnectivity/RodBocConnectivity.h>
#include <PixConnectivity/ModuleConnectivity.h>
#include <PixConnectivity/Pp0Connectivity.h>
#include <PixConnectivity/OBConnectivity.h>
#include <PixConnectivity/PartitionConnectivity.h>
#include <stdexcept>

namespace PixA {

  inline const std::string &connectivityName(const PixLib::GenericConnectivityObject *conn) {
    if (!conn) {
      throw std::runtime_error("No connectivity object");
    }
    return const_cast<PixLib::GenericConnectivityObject *>(conn)->name();
  }

  inline const PixLib::RodBocConnectivity *rodBoc(const PixLib::ModuleConnectivity *module_conn) {
    PixLib::ModuleConnectivity *temp= const_cast<PixLib::ModuleConnectivity*>(module_conn);
    if (temp->pp0() && temp->pp0()->ob() && temp->pp0()->ob()->rodBoc()) { 
      return temp->pp0()->ob()->rodBoc();
    }
    else {
      return NULL;
    }
  }

  inline PixLib::RodBocConnectivity *rodBoc(PixLib::ModuleConnectivity *module_conn)
  {
    if (module_conn->pp0() && module_conn->pp0()->ob() && module_conn->pp0()->ob()->rodBoc()){
      return module_conn->pp0()->ob()->rodBoc();
    }
    else {
      return NULL;
    }
  }

  inline const PixLib::RodBocConnectivity *rodBoc(const PixLib::Pp0Connectivity *pp0_conn) {
    PixLib::Pp0Connectivity *temp= const_cast<PixLib::Pp0Connectivity*>(pp0_conn);
    if (temp->ob() && temp->ob()->rodBoc()) { 
      return temp->ob()->rodBoc();
    }
    else {
      return NULL;
    }
  }

  inline PixLib::RodBocConnectivity *rodBoc(PixLib::Pp0Connectivity *pp0_conn) {
    if (pp0_conn->ob() && pp0_conn->ob()->rodBoc()) {
      return pp0_conn->ob()->rodBoc();
    }
    else {
      return NULL;
    }
  }

  inline PixLib::RodCrateConnectivity *crate(PixLib::RodBocConnectivity *rod_conn)
  {
    if (rod_conn) {
      return rod_conn->crate();
    }
    else {
      return NULL;
    }
  }

  inline const PixLib::RodCrateConnectivity *crate(const PixLib::RodBocConnectivity *rod_conn) {
    if (rod_conn) {
      return const_cast<PixLib::RodBocConnectivity *>(rod_conn)->crate();
    }
    else {
      return NULL;
    }
  }

  inline PixLib::PartitionConnectivity *partition(PixLib::RodCrateConnectivity *crate_conn)
  {
    if (crate_conn) {
      return crate_conn->partition();
    }
    else {
      return NULL;
    }
  }

  inline const PixLib::PartitionConnectivity *partition(const PixLib::RodCrateConnectivity *crate_conn) {
    if (crate_conn) {
      return const_cast<PixLib::RodCrateConnectivity *>(crate_conn)->partition();
    }
    else {
      return NULL;
    }
  }

  inline PixLib::PartitionConnectivity *partition(PixLib::RodBocConnectivity *rod_conn)
  {
    if (rod_conn) {
      return partition( crate( rod_conn) );
    }
    else {
      return NULL;
    }
  }

  inline const PixLib::PartitionConnectivity *partition(const PixLib::RodBocConnectivity *rod_conn)
  {
    if (rod_conn) {
      return partition( crate(rod_conn) );
    }
    else {
      return NULL;
    }
  }


  inline const PixLib::Pp0Connectivity *pp0(const PixLib::ModuleConnectivity *module_conn) {
    PixLib::ModuleConnectivity *temp= const_cast<PixLib::ModuleConnectivity*>(module_conn);
    return temp->pp0();
  }

  inline PixLib::Pp0Connectivity *pp0(PixLib::ModuleConnectivity *module_conn)
  {
    return module_conn->pp0();
  }

  inline ModuleList::const_iterator moduleIter(const PixLib::ModuleConnectivity *module_conn) {
    PixLib::ModuleConnectivity *temp= const_cast<PixLib::ModuleConnectivity*>(module_conn);
    if (!temp->pp0()) {
      throw std::runtime_error("Failed to get pp0 connectivity from module connectivity");
    }
    ModuleList list(*(temp->pp0()));
    ModuleList::const_iterator end=list.end();
    for( ModuleList::const_iterator module_iter = list.begin();
	 module_iter != end;
	 ++module_iter) {
      if (*module_iter == module_conn)
      return module_iter;
    }
    return end;
  }


  inline const std::string &connectivityName(Pp0List::const_iterator pp0_iter) {
    return const_cast<PixLib::Pp0Connectivity*>(*pp0_iter)->name();
  }

  inline const std::string &connectivityName(Pp0List::iterator pp0_iter) {
    return (*pp0_iter)->name();
  }

  //  inline const std::string &connectivityName(const PixLib::Pp0Connectivity *pp0_conn) {
  //    return const_cast<PixLib::Pp0Connectivity*>(pp0_conn)->name();
  //  }

  inline const std::string &connectivityName(const ModuleList &module_list) {
    return module_list.location();
  }

  
  inline const std::string &coolingLoopName( const PixLib::Pp0Connectivity *pp0_conn ) {
    return const_cast<PixLib::Pp0Connectivity *>(pp0_conn)->coolingLoop();
  }

  inline const std::string &coolingLoopName( Pp0List::iterator pp0_iter ) {
    return (*pp0_iter)->coolingLoop();
  }

  inline const std::string &coolingLoopName(Pp0List::const_iterator pp0_iter) {
    return const_cast<PixLib::Pp0Connectivity*>(*pp0_iter)->coolingLoop();
  }



  inline const std::string &connectivityName(CrateList::const_iterator crate_iter) {
    return const_cast<PixLib::RodCrateConnectivity*>(*crate_iter)->name();
  }

  inline const std::string &connectivityName(CrateList::iterator crate_iter) {
    return (*crate_iter)->name();
  }

  //  inline const std::string &connectivityName(const PixLib::RodCrateConnectivity *crate_conn) {
  //    return const_cast<PixLib::RodCrateConnectivity*>(crate_conn)->name();
  //  }

  inline const std::string &connectivityName(const RodList &rod_list) {
    return rod_list.location();
  }



  inline const std::string &connectivityName(RodList::const_iterator rod_iter) {
    return const_cast<PixLib::RodBocConnectivity*>(*rod_iter)->name();
  }

  inline const std::string &connectivityName(RodList::iterator rod_iter) {
    return (*rod_iter)->name();
  }

  //inline const std::string &connectivityName(const PixLib::RodBocConnectivity *rod) {
  //    return const_cast<PixLib::RodBocConnectivity *>(rod)->name();
  //  }

  inline const std::string &connectivityName(const Pp0List &pp0_list) {
    return pp0_list.location();
  }


  inline const std::string &connectivityName(ModuleList::const_iterator module_iter) {
    return const_cast<PixLib::ModuleConnectivity*>(*module_iter)->name();
  }

  inline const std::string &connectivityName(ModuleList::iterator module_iter) {
    return (*module_iter)->name();
  }

  //  inline const std::string &connectivityName(const PixLib::ModuleConnectivity *module) {
  //    return const_cast<PixLib::ModuleConnectivity *>(module)->name();
  //  }


  inline const std::string &productionName(ModuleList::const_iterator module_iter) {
    return const_cast<PixLib::ModuleConnectivity*>(*module_iter)->prodId();
  }

  inline const std::string &productionName(ModuleList::iterator module_iter) {
    return (*module_iter)->prodId();
  }

  inline const std::string &productionName(const PixLib::ModuleConnectivity *module) {
    return const_cast<PixLib::ModuleConnectivity *>(module)->prodId();
  }


  inline const PixLib::RodBocConnectivity *connectedRod(const PixLib::ModuleConnectivity *module) {
    if (!module ) return NULL;
    PixLib::Pp0Connectivity *pp0=const_cast<PixLib::ModuleConnectivity *>(module)->pp0();
    if (!pp0 ) return NULL;
    PixLib::OBConnectivity *ob=pp0->ob();
    if (!ob ) return NULL;
    return ob->rodBoc();
  }

  // inline const std::string &connectivityName(const PixLib::PartitionConnectivity *partition_conn) {
  //    return const_cast<PixLib::PartitionConnectivity *>(partition_conn)->name();
  //  }

  inline unsigned int modId(ModuleList::const_iterator module_iter) {
    return (*module_iter)->modId;
  }

  inline unsigned int modId(const PixLib::ModuleConnectivity *module_conn) {
    return const_cast<PixLib::ModuleConnectivity *>(module_conn)->modId;
  }

  inline unsigned int groupId(ModuleList::const_iterator module_iter) {
    return (*module_iter)->groupId;
  }

  inline unsigned int groupId(const PixLib::ModuleConnectivity *module_conn) {
    return const_cast<PixLib::ModuleConnectivity *>(module_conn)->groupId;
  }

  inline unsigned int pp0Slot(const ModuleList::const_iterator &module_iter) {
    return static_cast<unsigned int>(const_cast<PixLib::ModuleConnectivity *>(*module_iter)->pp0Slot());
  }

  inline const std::string &partitionName(CrateList::const_iterator crate_iter) {
    return const_cast<PixLib::RodCrateConnectivity*>(*crate_iter)->partition()->name();
  }

  inline const std::string &partitionName(CrateList::iterator crate_iter) {
    return (*crate_iter)->partition()->name();
  }  

  inline const std::string &partitionName(const PixLib::RodCrateConnectivity *crate_conn) {
    return const_cast<PixLib::RodCrateConnectivity *>(crate_conn )->partition()->name();
  }

  /** Get the TX plugin number the PP0 is connected to.
   * @return 0-3 for TX plugins A-D.
   */
  inline unsigned int txPlugin(const Pp0List::const_iterator &pp0_iter) {
    return static_cast<unsigned int>( const_cast<PixLib::OBConnectivity *>(const_cast<PixLib::Pp0Connectivity *>(*pp0_iter)->ob())->bocTxSlot());
  }

  /** Get the TX plugin number the PP0 is connected to.
   * @return 0-3 for TX plugins A-D.
   */
  inline unsigned int txPlugin(const PixLib::Pp0Connectivity *pp0_conn) {
    return static_cast<unsigned int>( const_cast<PixLib::OBConnectivity *>(const_cast<PixLib::Pp0Connectivity *>(pp0_conn)->ob())->bocTxSlot());
  }

  enum EDataLink { DTO1=0, DTO2=1 };

  /** Get the RX plugin number the PP0 is connected to.
   * @return 0-3 for RX plugins A-D.
   */
  inline unsigned int rxPlugin(const Pp0List::const_iterator &pp0_iter, EDataLink link) {
    return static_cast<unsigned int>( const_cast<PixLib::OBConnectivity *>(const_cast<PixLib::Pp0Connectivity *>(*pp0_iter)->ob())->bocRxSlot(link));
  }

  /** Get the RX plugin number the PP0 is connected to.
   * @return 0-3 for RX plugins A-D.
   */
  inline unsigned int rxPlugin(const PixLib::Pp0Connectivity *pp0_conn, EDataLink link) {
    return static_cast<unsigned int>( const_cast<PixLib::OBConnectivity *>(const_cast<PixLib::Pp0Connectivity *>(pp0_conn)->ob())->bocRxSlot(link));
  }


  /** Get the TX plugin number the module is connected to.
   * @return 0-3 for TX plugins A-D.
   */
  inline unsigned int txPlugin(const ModuleList::const_iterator &module_iter) {
    return static_cast<unsigned int>( const_cast<PixLib::ModuleConnectivity *>(*module_iter)->bocLinkTx())/10;
  }

  /** Get the RX plugin number the module is connected to.
   * @return 0-3 for TX plugins A-D.
   */
  inline unsigned int rxPlugin(const ModuleList::const_iterator &module_iter, EDataLink link) {
    return static_cast<unsigned int>( const_cast<PixLib::ModuleConnectivity *>(*module_iter)->bocLinkRx(link)/10);
  }

  /** Get the TX channel number the module is connected to.
   * @return 0-3 for TX plugins A-D.
   */
  inline unsigned int txChannel(const ModuleList::const_iterator &module_iter) {
    return static_cast<unsigned int>( const_cast<PixLib::ModuleConnectivity *>(*module_iter)->bocLinkTx())%10;
  }

  /** Get the RX plugin number the module is connected to.
   * @return 0-3 for TX plugins A-D.
   */
  inline unsigned int rxChannel(const ModuleList::const_iterator &module_iter, EDataLink link) {
    return static_cast<unsigned int>( const_cast<PixLib::ModuleConnectivity *>(*module_iter)->bocLinkRx(link)%10);
  }

  class ModulePluginConnection;

  ModulePluginConnection txConnection(const ModuleList::const_iterator &module_iter);
  ModulePluginConnection rxConnection(const ModuleList::const_iterator &module_iter, EDataLink link);

  class ModulePluginConnection
  {
    friend ModulePluginConnection txConnection(const ModuleList::const_iterator &module_iter);
    friend ModulePluginConnection rxConnection(const ModuleList::const_iterator &module_iter);
    friend ModulePluginConnection rxConnection(const ModuleList::const_iterator &module_iter, EDataLink link);

    friend ModulePluginConnection txConnection(const PixLib::ModuleConnectivity *module_conn);
    friend ModulePluginConnection rxConnection(const PixLib::ModuleConnectivity *module_conn);
    friend ModulePluginConnection rxConnection(const PixLib::ModuleConnectivity *module_conn, EDataLink link);

  protected:
    ModulePluginConnection(unsigned int plugin_and_channel) : m_pluginAndChannel(plugin_and_channel) {};
  public:
    bool isValid() const         { return m_pluginAndChannel<40;}
    
    unsigned int plugin() const  { return m_pluginAndChannel/10;}
    unsigned int channel() const { return m_pluginAndChannel%10;}
  private:
    unsigned int m_pluginAndChannel;
  };

  /** Get the TX channel number the module is connected to.
   * @return 0-3 for TX plugins A-D.
   */
  inline ModulePluginConnection txConnection(const ModuleList::const_iterator &module_iter) {
    return ModulePluginConnection(static_cast<unsigned int>( const_cast<PixLib::ModuleConnectivity *>(*module_iter)->bocLinkTx()));
  }

  /** Get the RX plugin number the module is connected to.
   * @return 0-3 for TX plugins A-D.
   */
  inline ModulePluginConnection rxConnection(const ModuleList::const_iterator &module_iter, EDataLink link) {
    return ModulePluginConnection(static_cast<unsigned int>( const_cast<PixLib::ModuleConnectivity *>(*module_iter)->bocLinkRx(link)));
  }


  /** Get the TX channel number the module is connected to.
   * @return 0-3 for TX plugins A-D.
   */
  inline ModulePluginConnection txConnection(const PixLib::ModuleConnectivity *module_conn) {
    return ModulePluginConnection(static_cast<unsigned int>( const_cast<PixLib::ModuleConnectivity *>(module_conn)->bocLinkTx()));
  }

  /** Get the RX plugin number the module is connected to.
   * @return 0-3 for TX plugins A-D.
   */
  inline ModulePluginConnection rxConnection(const PixLib::ModuleConnectivity *module_conn, EDataLink link) {
    return ModulePluginConnection(static_cast<unsigned int>( const_cast<PixLib::ModuleConnectivity *>(module_conn)->bocLinkRx(link)));
  }


#ifdef _PIXLIB_TIMCONNOBJ
  inline const PixLib::TimConnectivity *responsibleTim(const PixLib::RodCrateConnectivity *rod_crate) {
    return const_cast<PixLib::RodCrateConnectivity *>(rod_crate)->tim();
  }

  inline const PixLib::TimConnectivity *responsibleTim(const PixLib::RodBocConnectivity *rod_boc) {
    const PixLib::RodCrateConnectivity *crate = PixA::crate(rod_boc);
    if (crate) {
      return const_cast<PixLib::RodCrateConnectivity *>(crate)->tim();
    }
    else {
      return static_cast<PixLib::TimConnectivity *>(NULL);
    }
  }

#endif

#ifdef _PIXLIB_ROSCONNOBJ
  inline const PixLib::RobinConnectivity *robinConn(const PixLib::RodBocConnectivity *rod_boc) {
    if (rod_boc) {
      if (const_cast<PixLib::RodBocConnectivity *>(rod_boc)->rol()) {
	return const_cast<PixLib::RodBocConnectivity *>(rod_boc)->rol()->robin();
      }
    }
    return NULL;
  }

  inline const PixLib::RosConnectivity *rosConn(const PixLib::RodBocConnectivity *rod_boc) {
    const PixLib::RobinConnectivity *robin = robinConn(rod_boc);
    if (robin) {
      return const_cast<PixLib::RobinConnectivity *>(robin)->ros();
    }
    return NULL;
  }

  inline const PixLib::RosConnectivity *rosConn(const PixLib::RobinConnectivity *robin) {
    if (robin) {
      return const_cast<PixLib::RobinConnectivity *>(robin)->ros();
    }
    return NULL;
  }

#endif


}

#endif
