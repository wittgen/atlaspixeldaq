#ifndef _PixA_Regex_t_h_
#define _PixA_Regex_t_h_

#include <regex.h>
#include <string>

namespace PixA {
  class Regex_t {
  public:
    Regex_t(const std::string &regex_string) {
      regcomp(&m_regex, regex_string.c_str(), REG_EXTENDED);
    }

    bool match(const std::string &text) const {
      return regexec(const_cast<regex_t *>(&m_regex),text.c_str(),0,NULL,0) == 0;
    }

    ~Regex_t() {
      regfree(&m_regex);
    }

  private:
    regex_t m_regex;
  };
}

#endif
