#include "ConfGrpRefUtil.h"
#include "ConnectivityManager.h"
#include <createDbServer.h>

#include <iomanip>

class IPCPartition;

namespace PixLib {
  class PixDbServerInterface;
}

int main(int argc, char ** argv)
{
  if (argc<3) {
    std::cout << "Usage: test_Disable.cxx <IDtag> <ConnTag> <CfgTag> <ScanID>" << std::endl;
    return -1;
  }
  
  IPCCore::init(argc, argv);

  std::shared_ptr<IPCPartition> db_server_partition;
  db_server_partition = std::make_shared<IPCPartition>("Partition_PixelInfrastructure_lmasetti");
  if (db_server_partition->isValid()) {
    std::cout << "Found partition, trying to use DbServer " << db_server_partition.get() << std::endl;
    std::shared_ptr<PixLib::PixDbServerInterface> db_server( PixA::createDbServer(*db_server_partition, "PixelDbServer"));
    std::cout << "Looking for DbServer PixelDbServer on the partition: " << db_server.get() << std::endl;
    if (db_server.get()) {
      PixA::ConnectivityManager::useDbServer( db_server );
    }
  }

  PixA::ConnectivityRef conn(PixA::ConnectivityManager::getConnectivity(argv[1], argv[2], argv[2], argv[2], argv[3],0));
  std::stringstream disable_name;
  disable_name << "Disable-S" << std::setw(9) << std::setfill('0') << argv[4];
  std::cout << disable_name.str() << std::endl;
  std::shared_ptr<const PixLib::PixDisable> mydisable( conn.getDisable(disable_name.str()) );
  std::shared_ptr<const PixLib::PixDisable> yourdisable( conn.getDisable(disable_name.str()) );
}
