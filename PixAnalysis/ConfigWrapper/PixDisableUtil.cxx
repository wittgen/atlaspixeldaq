#include "PixDisableUtil.h"

namespace PixA {

  DisabledListRoot createPp0DisabledList( const PixA::ConnectivityRef &conn, const DisabledListRoot &orig_root_disabled_list, const std::string &rod_name)
  {
    if (orig_root_disabled_list.isEnableMap()) {
      //       const EnableMap_t &enable_map = orig_root_disabled_list.enableMap();
      //       assert(enable_map);
      //       EnableMap_t::const_iterator  rod_iter = enable_map.find(rod_name);
      //       EnableMap_t sub_enable_map;
      //       if (rod_iter != enable_map.end()) {
      // 	sub_enable_map.insert(std::make_pair(rod_name,rod_iter->second));
      //       }

      return DisabledListRoot( orig_root_disabled_list.enableMap(), rod_name );
    }

    DisabledListRoot root_disabled_list(orig_root_disabled_list);
    const PixLib::RodBocConnectivity *rod_conn = conn.findRod(rod_name);
    if (!rod_conn)  {
      std::stringstream message;
      message << "ROD " << rod_name << " not found in connectivity.";
      throw ConfigException(message.str());
    }

    const PixA::DisabledList rod_disabled_list( PixA::rodDisabledList( root_disabled_list, rod_conn) );
    if (!PixA::enabled( rod_disabled_list, rod_conn )) {
      std::stringstream message;
      message << "ROD " << rod_name << " not enabled.";
      throw ConfigException(message.str());
    }

    // get the list of disabled pp0s
    DisabledList pp0_disabled_list = pp0DisabledList(rod_disabled_list, rod_conn);
    PixA::Pp0List pp0_list = conn.pp0s(rod_name);
    PixA::Pp0List::const_iterator pp0_list_begin=pp0_list.begin();
    PixA::Pp0List::const_iterator pp0_list_end=pp0_list.end();

    std::shared_ptr<PixLib::PixDisable> rod_pix_disable(new PixLib::PixDisable(rod_name));
    bool something_disabled=false;
    for (PixA::Pp0List::const_iterator pp0_iter=pp0_list_begin;
	 pp0_iter != pp0_list_end;
	 ++pp0_iter) {

      if (!PixA::enabled( pp0_disabled_list, pp0_iter )) {
	rod_pix_disable->disable(PixA::connectivityName(pp0_iter) );
	something_disabled=true;
	continue;
      }

      const PixA::DisabledList module_disabled_list( moduleDisabledList(pp0_disabled_list, pp0_iter) );

      PixA::ModuleList::const_iterator module_list_begin=modulesBegin(pp0_iter);
      PixA::ModuleList::const_iterator module_list_end=modulesEnd(pp0_iter);

      for (PixA::ModuleList::const_iterator module_iter=module_list_begin;
	   module_iter != module_list_end;
	   ++module_iter) {

	if (!PixA::enabled( module_disabled_list, module_iter )) {
	  rod_pix_disable->disable(PixA::connectivityName(module_iter) );
	  something_disabled=true;
	}

      }

    }
    if (!something_disabled) {
      rod_pix_disable.reset();
    }

    return DisabledListRoot( rod_pix_disable, DisabledList::kPp0List);
  }

  EnableMap_t createEnableMap(const std::string &pix_disable_string)
  {
    EnableMap_t an_enable_map;
    ModuleEnableMap_t  *a_module_enable_map=NULL;
    std::string::size_type start_pos=0;
    for(;start_pos+1<pix_disable_string.size() ;) {
      std::string::size_type pos =  pix_disable_string.find(":",start_pos+1);
      if (pos == std::string::npos) break;
      if (pos+1<pix_disable_string.size() && pix_disable_string[pos+1]==':') {
	a_module_enable_map=&(an_enable_map[ pix_disable_string.substr(start_pos+1, pos-start_pos-1) ]);
	pos++;
      }
      else {
	if (!a_module_enable_map) {
	  a_module_enable_map=&(an_enable_map[""]);
	}
	(*a_module_enable_map).insert(pix_disable_string.substr(start_pos+1, pos-start_pos-1));
      }
      start_pos=pos;
    }

//     for (EnableMap_t::const_iterator rod_iter = an_enable_map.begin();
// 	 rod_iter != an_enable_map.end();
// 	 ++rod_iter) {
//       std::cout << rod_iter->first << " : ";
//       for (ModuleEnableMap_t::const_iterator module_iter = rod_iter->second.begin();
// 	   module_iter != rod_iter->second.end();
// 	   ++module_iter) {
// 	std::cout << *module_iter << " ";
//       }
//       std::cout << std::endl;
//     }
    return an_enable_map;
  }

}
