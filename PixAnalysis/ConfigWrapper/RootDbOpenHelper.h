#ifndef _RootDbOpenHelper_h_
#define _RootDbOpenHelper_h_

#include <memory>
#include <sstream>
#include <cassert>
#include <fstream>
#include <pixa_exception.h>

#include <DbWrapper.h>

#include <RootDb/RootDb.h>


namespace PixA {

  class RootDbOpenHelper : public IDbOpenHelper
  {
  public:
    RootDbOpenHelper(const std::string &name) : IDbOpenHelper(true),m_name(name) {}

    //  virtual ~RootDbOpenHelper() {}
    std::pair<PixLib::PixDbInterface *, PixLib::DbRecord *> open() const  {
      std::pair<PixLib::PixDbInterface *, PixLib::DbRecord *> ret;
      ret.first = NULL;
      ret.second = NULL;
//       std::cout << "INFO [RootDbOpenHelper::ctor] open "
//       		<< m_name
//       		<< std::endl;

      try {
    std::unique_ptr<PixLib::PixDbInterface> db(new PixLib::RootDb(m_name,"READ"));
    std::unique_ptr<PixLib::DbRecord> record (db->readRootRecord());
	ret.first=db.release();
	ret.second=record.release();
      }
      catch (std::exception &err) {
	throw PixA::ConfigException(err.what());
      }
      catch (...) {
	std::stringstream message;
	message << "ERROR [RootDbOpenHelper::open] Failed to open file " << m_name << " or read the root record." << std::endl;
	throw PixA::ConfigException(message.str());
      }
      return ret;
    }

    std::pair<PixLib::PixDbInterface *, PixLib::DbRecord *> openForWriting() const  {
      std::pair<PixLib::PixDbInterface *, PixLib::DbRecord *> ret;
      ret.first = NULL;
      ret.second = NULL;

//       std::cout << "INFO [RootDbOpenHelper::ctor] open for writing "
//       		<< m_name
// 		<< std::endl;

      std::ifstream in(m_name.c_str());

      try {
	std::string mode (((in) ? "UPDATE" : "NEW"));
    std::unique_ptr<PixLib::PixDbInterface> db( new PixLib::RootDb(m_name,mode) );
    std::unique_ptr<PixLib::DbRecord> record (db->readRootRecord());
	ret.first=db.release();
	ret.second=record.release();
      }
      catch (std::exception &err) {
	throw PixA::ConfigException(err.what());
      }
      catch (...) {
	std::stringstream message;
	message << "ERROR [RootDbOpenHelper::open] Failed to open file " << m_name << " or read the root record." << std::endl;
	throw PixA::ConfigException(message.str());
      }
      return ret;
    }


  private:
    std::string              m_name;
  };

}
#endif
