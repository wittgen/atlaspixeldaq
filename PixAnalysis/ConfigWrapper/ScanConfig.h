#ifndef _ScanConfig_h_
#define _ScanConfig_h_

#include "ConfigHandle.h"
#include "Connectivity.h"

namespace PixA {

  /** Container object which contains the module names and handles to its config, the scan config and the boc config.
   */
  class ScanConfig
  {
  public:
    ScanConfig(const std::string &prod_name,
	       const std::string &conn_name,
	       const ConfigHandle &mod_config,
	       const ConfigHandle &pix_scan_config,
	       const ConfigHandle &boc_config,
	       unsigned int scan_number)
      : m_prodName(prod_name),
	m_connName(conn_name),
	m_scanNumber(scan_number)
	{
	  m_config[kModConfig]=mod_config;
	  m_config[kPixScanConfig]=pix_scan_config;
	  m_config[kBocConfig]=boc_config;
	  //	  m_bocLinkRx[kDTO1]=m_bocLinkRx[kDTO2]=-1;
	}

    ScanConfig(const std::string &prod_name,
	       const std::string &conn_name,
	       const ConfigHandle &mod_config,
	       const ConfigHandle &pix_scan_config,
	       const ConfigHandle &boc_config,
	       unsigned int scan_number,
	       const ConnectivityRef &conn,
	       const ModuleList::const_iterator &mod_iterator)
      : m_prodName(prod_name),
	m_connName(conn_name),
	m_scanNumber(scan_number),
	m_conn(conn),
	m_modConnIter(mod_iterator)
	{
	  m_config[kModConfig]=mod_config;
	  m_config[kPixScanConfig]=pix_scan_config;
	  m_config[kBocConfig]=boc_config;
	  //	  m_bocLinkRx[kDTO1]=m_bocLinkRx[kDTO2]=-1;
	}

    ScanConfig(const ConfigHandle &pix_scan_config,
	       const ConfigHandle &boc_config,
	       unsigned int scan_number,
	       const ConnectivityRef &conn)
      : m_scanNumber(scan_number),
	m_conn(conn)
	{
	  m_config[kPixScanConfig]=pix_scan_config;
	  m_config[kBocConfig]=boc_config;
	  //	  m_bocLinkRx[kDTO1]=m_bocLinkRx[kDTO2]=-1;
	}

    const ConfigHandle &modConfigHandle()     const {return m_config[kModConfig];}
    const ConfigHandle &bocConfigHandle()     const {return m_config[kBocConfig];}
    const ConfigHandle &pixScanConfigHandle() const {return m_config[kPixScanConfig];}


    const std::string &prodName() const    {return m_prodName;}
    const std::string &connName() const    {return m_connName;}

    enum EConfigType {kModConfig, kPixScanConfig, kBocConfig, kNConfig};

    bool hasConfig(unsigned int a_config) const         {if (a_config>=kNConfig) return false; return m_config[a_config];}
    //    ConfigRef config(unsigned int a_config)             {return m_config[a_config].ref();}
    ConfigRef config(unsigned int a_config) const {return m_config[a_config].ref();}

    bool hasModConfig() const              {return m_config[kModConfig];}
    //ConfigRef modConfig()                  {return m_config[kModConfig].ref();}
    const ConfigRef modConfig() const      {return m_config[kModConfig].ref();}

    bool hasPixScanConfig() const          {return m_config[kPixScanConfig];}
    //ConfigRef pixScanConfig()              {return m_config[kPixScanConfig].ref();}
    const ConfigRef pixScanConfig() const  {return m_config[kPixScanConfig].ref();}

    bool hasBocConfig() const              {return m_config[kBocConfig];}
    // ConfigRef bocConfig()                  {return m_config[kBocConfig].ref();}
    const ConfigRef bocConfig() const      {return m_config[kBocConfig].ref();}

    enum ERxLink {kDTO1,kDTO2 };

    /** Return the boc link Rx.
     * Sometimes the channels are swapped. The swapping is not corrected.
     */
    int bocLinkRx(ERxLink link) const;

    /** Return the boc link Rx in the correct order.
     * Sometimes the channels are swapped. This is indicated by a parameter
     * in the scan config.
     */
    int inOrderBocLinkRx(ERxLink link) const;


    //    void setBocLinRx(ERxLink link, int value) { m_bocLinkRx[link] = value;}


    /** Return the serial scan number.
     */
    unsigned int scanNumber() const { return m_scanNumber; }


    /** Return connectivity. 
     * The connectivity reference may not be valid
     */
    const ConnectivityRef &connectivity() const {return m_conn;}

  private:
    std::string  m_prodName;
    std::string  m_connName;
    ConfigHandle m_config[kNConfig];

//    ConfigHandle m_modConfig;
//    ConfigHandle m_pixScanConfig;
//    ConfigHandle m_bocConfig;

    //int m_bocLinkRx[2];

    unsigned int m_scanNumber;

    ConnectivityRef m_conn;
    ModuleList::const_iterator m_modConnIter;

    static int s_dtoLinkMap[2];
  };


  /** Reference to the shared scan config object.
   */
  class ScanConfigRef
  {
  protected:
    ScanConfigRef(ScanConfig *scan_config) : m_scanConfig(scan_config) {}

  public:
    ScanConfigRef(const ScanConfigRef &scan_config_ref) : m_scanConfig(scan_config_ref.m_scanConfig) {}

    const ConfigHandle &modConfigHandle()     const {return m_scanConfig->modConfigHandle();}
    const ConfigHandle &bocConfigHandle()     const {return m_scanConfig->bocConfigHandle();}
    const ConfigHandle &pixScanConfigHandle() const {return m_scanConfig->pixScanConfigHandle();}

    /** Get the production name of a module.
     */
    const std::string &prodName() const    {return m_scanConfig->prodName();}

    /** Get the connectivity name of a module.
     */
    const std::string &connName() const    {return m_scanConfig->connName();}


    /** True if the scan config contains a module config.
     */
    bool hasModConfig() const              {return m_scanConfig->hasModConfig();}

    /** Get the module configuration.
     */
    ConfigRef modConfig()              { assert(hasModConfig()); return m_scanConfig->modConfig();}

    /** Get the module configuration (read only).
     */
    const ConfigRef modConfig() const  { assert(hasModConfig()); return m_scanConfig->modConfig();}


    /** Verify that the given config exists.
     */
    bool hasConfig(unsigned int a_config) const         { return m_scanConfig->hasConfig(a_config);}

    /** Get the config of the given type.
     */
    ConfigRef config(unsigned int a_config)             { assert(hasConfig(a_config)); return m_scanConfig->config(a_config);}

    /** Get the config of the given type (read only).
	*/
    const ConfigRef config(unsigned int a_config) const { assert(hasConfig(a_config)); return m_scanConfig->config(a_config);}


    /** True if the scan config contains a pix scan config.
     */
    bool hasPixScanConfig() const              {return m_scanConfig->hasPixScanConfig();}

    /** Get the scan config.
     */
    ConfigRef pixScanConfig()             { assert(hasPixScanConfig()); return m_scanConfig->pixScanConfig();}

    /** Get the scan config (read only).
     */
    const ConfigRef pixScanConfig() const { assert(hasPixScanConfig()); return m_scanConfig->pixScanConfig();}


    /** True if the scan config contains a BOC config.
     */
    bool hasBocConfig() const              {return m_scanConfig->hasBocConfig();}

    /** Get the boc config.
     */
    ConfigRef bocConfig()              { assert(hasBocConfig()); return m_scanConfig->bocConfig();}

    /** Get the boc config (read only).
     */
    const ConfigRef bocConfig() const  { assert(hasBocConfig()); return m_scanConfig->bocConfig();}

    /** Get the Rx link for the given channel.
     * @param link the chanel kDTO1 or kDTO2 for B-boards otherwise kDTO1.
     * Sometimes the channels are swapped. The swapping is not corrected.
     */
    int bocLinkRx(PixA::ScanConfig::ERxLink link) const {
      return m_scanConfig->bocLinkRx(link);
    }

    /** Get the Rx link for the given channel in the correct order.
     * @param link the chanel kDTO1 or kDTO2 for B-boards otherwise kDTO1.
     * Sometimes the channels are swapped. This is indicated by a parameter
     * in the scan config. If a swap is observed, the ordering is corrected.
     */
    int inOrderBocLinkRx(PixA::ScanConfig::ERxLink link) const {
      return m_scanConfig->inOrderBocLinkRx(link);
    }


    /** Return connectivity. 
     * The connectivity reference may not be valid
     */
    const ConnectivityRef &connectivity() const {return m_scanConfig->connectivity();}


    /** Get the serial scan number.
     */
    unsigned int scanNumber() const {
      return m_scanConfig->scanNumber();
    }


    /** create a scan config object.
     */
    static ScanConfigRef makeScanConfig(const std::string &prod_name, 
					const std::string &conn_name, 
					const ConfigHandle &mod_config, 
					const ConfigHandle &pix_scan_config, 
					const ConfigHandle &boc_config,
					unsigned int scan_number) {
      return ScanConfigRef( new ScanConfig(prod_name, conn_name, mod_config,pix_scan_config,boc_config,scan_number) );
    }

    /** create a scan config object with boc rx links.
     * @todo rather add a connectivity reference to the scan config.
     */
    static ScanConfigRef makeScanConfig(const std::string &prod_name, 
					const std::string &conn_name, 
					const ConfigHandle &mod_config, 
					const ConfigHandle &pix_scan_config, 
					const ConfigHandle &boc_config,
					unsigned int scan_number,
					const ConnectivityRef &conn, const ModuleList::const_iterator &mod_iterator) {
      return ScanConfigRef( new ScanConfig(prod_name, conn_name, mod_config,pix_scan_config,boc_config, scan_number, conn, mod_iterator) );
    }

    /** create a scan config object without valid module identifier
     */
    static ScanConfigRef makeScanConfig(const ConfigHandle &pix_scan_config, 
					const ConfigHandle &boc_config,
					unsigned int scan_number,
					const ConnectivityRef &conn) {
      return ScanConfigRef( new ScanConfig(pix_scan_config,boc_config, scan_number, conn) );
    }


  private:
    std::shared_ptr<ScanConfig> m_scanConfig;
  };

}

#endif
