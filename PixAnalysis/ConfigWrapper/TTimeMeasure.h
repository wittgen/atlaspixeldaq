// TTimeMeasure:
//   

#ifndef _TTimeMeasure_H_
#define _TTimeMeasure_H_

#include <sys/time.h>
#include <unistd.h>
#include <float.h>
#include <math.h>
#ifndef MAKEDICT
#if ( __GNUC__ < 3)
#include <iostream.h>
#else
#include <iostream>
using namespace std;
#endif
#endif

class TTimeMeasure
{
protected:
  struct timeval fStart;
  struct timeval fEnd;
  struct timeval fCheckPoint;
  
  double fDiffMin;
  double fDiffMax;
  double fDiffSum;
  double fDiffSum2;
  int fNDiffs;

public:
  TTimeMeasure() {Start();};
    ~TTimeMeasure() {};
  void Start() {
    fNDiffs=0;
    fDiffMin=DBL_MAX;
    fDiffMax=0.;
    fDiffSum=0.;
    fDiffSum2=0.;
    gettimeofday(&fStart,0);
    fCheckPoint=fStart;
  }
  void Stop() {
    gettimeofday(&fEnd,0);
  }
  void Skip() {
    gettimeofday(&fCheckPoint,0);
  }
  void ResetCheckPoint() {
    gettimeofday(&fCheckPoint,0);
  };
  void Stamp() {
    gettimeofday(&fEnd,0);
    double diff=ElapsedTime(fCheckPoint,fEnd);
    if (diff>0) {
      fNDiffs++;
      fDiffSum+=diff;
      fDiffSum2+=diff*diff;
      if (diff > fDiffMax) fDiffMax=diff;
      if (diff < fDiffMin) fDiffMin=diff;
      fCheckPoint=fEnd;
    }
  }

  void Show() const {
    cout << "total time=" << ElapsedTime(fStart,fEnd);
    if (fNDiffs>1) {
      
      cout << "\t sum of differences=" << fDiffSum
	   << "\t n=" << fNDiffs
	   << ": " << fDiffMin << " < "
	   << fDiffSum/fNDiffs << "+-" << sqrt((fDiffSum2-fDiffSum*fDiffSum/fNDiffs)/(fNDiffs-1))
	   << " < " << fDiffMax;
    }
    cout << endl;
  };
  void ShowFullTimes() const {
    cout << "total time=" << ElapsedTime(fStart,fEnd)
	 << "\t" << fStart.tv_sec << ":" << fStart.tv_usec
	 << " - " << fEnd.tv_sec  << ":" << fEnd.tv_usec
	 << endl;
  };

  static double ElapsedTime(const struct timeval &start, const struct timeval &end) {
    long sec=end.tv_sec-start.tv_sec;
    long usec=end.tv_usec-start.tv_usec;

    return ((double) sec ) +  1e-6 * usec;
  };

};

#endif
