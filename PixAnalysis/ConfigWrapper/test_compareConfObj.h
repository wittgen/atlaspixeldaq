#ifndef _test_comapreConfObj_h_
#define _test_comapreConfObj_h_
#include <string>
namespace PixLib {
 class ConfObj;
 class ConfGroup;
 class Config;
}

std::string stripGroupName(const std::string &full_name);

// template <class T> bool operator==(PixLib::ConfObj &a, PixLib::ConfObj &b);
bool operator==(PixLib::ConfObj &a, PixLib::ConfObj &b);

inline bool operator!=(PixLib::ConfObj &a, PixLib::ConfObj &b) {
  return !(a==b);
}


// compare conf groups
bool operator==(PixLib::ConfGroup &a, PixLib::ConfGroup &b);
inline bool operator!=(PixLib::ConfGroup &a, PixLib::ConfGroup &b)
{
  return !(a==b);
}


bool operator==(PixLib::Config &a, PixLib::Config &b);

inline bool operator!=(PixLib::Config &a, PixLib::Config &b) {
  return !(a==b);
}

#endif
