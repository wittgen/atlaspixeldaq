#include "ConnObjConfigDb.h"
#include "ConnectivityManager.h"
#include "Regex_t.h"

#include "createDbServer.h"
#include "PixConnObjConfigDb.h"
#include <ConfigRootIO/ConfigStore.h>
#include <ConfigRootIO/ConfigStoreFactory.h>

#include <PixModule/PixModule.h>
#include <PixBoc/PixBoc.h>
#include <PixTrigController/PixTrigController.h>

#include "test_compareConfObj.h"
#include <iomanip>

void dumpConfig(PixLib::Config &config, std::string margin)
{
  if (!config.m_group.empty()) {
    std::cout << "INFO [dumpConfig] " << margin << " groups of " << config.name() << " / " << config.type() << " ; " << config.rev() << " : " << std::endl;
  for (std::vector<PixLib::ConfGroup*>::iterator group_iter = config.m_group.begin();
       group_iter != config.m_group.end();
       ++group_iter) {
    std::cout << "INFO [dumpConfig] " << margin << " group : [" << config.name() << "][" << (*group_iter)->name() << "]" << std::endl;
  }
  }

  if (!config.m_config.empty()) {
    std::cout << "INFO [dumpConfig] " << margin << " sub configs of " << config.name() << " / " << config.type() << " ; " << config.rev() << " : " << std::endl;
  for (std::map<std::string, PixLib::Config*>::iterator sub_config_iter = config.m_config.begin();
       sub_config_iter != config.m_config.end();
       ++sub_config_iter) {
    std::cout << "INFO [dumpConfig] "<< margin << " sub configs : " << sub_config_iter->first << std::endl;
  }
  for (std::map<std::string, PixLib::Config*>::iterator sub_config_iter = config.m_config.begin();
       sub_config_iter != config.m_config.end();
       ++sub_config_iter) {
    dumpConfig(*(sub_config_iter->second), margin+"  ");
  }
  }
}

void dumpConfig(PixLib::Config &config) {
  dumpConfig(config,"");
}

class IPCInstance
{

 private:
  IPCInstance(int argc, char **argv, const std::string &partition_name) {
    IPCCore::init(argc, argv);
    m_ipcPartition=std::shared_ptr<IPCPartition>(new IPCPartition(partition_name.c_str()));

    if (!m_ipcPartition->isValid()) {
      std::stringstream message;
      message << "ERROR [IPCInstance] Not a valid partition " << partition_name << std::endl;
      throw std::runtime_error(message.str());
    }
  }
 public:

  static IPCInstance *instance(int argc, char **argv, const std::string &partition_name) {
    if (!s_instance) {
      s_instance = new IPCInstance(argc,argv,partition_name);
    }
    return s_instance;
  }

  IPCPartition &partition() { return *m_ipcPartition;}
  
 private:
  std::shared_ptr<IPCPartition> m_ipcPartition;
  static IPCInstance *s_instance;

};

IPCInstance *IPCInstance::s_instance = NULL;


std::shared_ptr<PixLib::PixDbServerInterface> dbServer(const std::string &db_server_partition_name,
		 const std::string &db_server_name,
		 int argc,
		 char **argv)
{
  int temp_argc=0;
  char *temp_argv[1]={NULL};
  IPCInstance *ipc_instance = IPCInstance::instance(temp_argc,temp_argv, db_server_partition_name);

  std::shared_ptr<PixLib::PixDbServerInterface> db_server( PixA::createDbServer(ipc_instance->partition(),db_server_name) );
  if (db_server.get()) {
    PixA::ConnectivityManager::useDbServer(db_server);
  }
  return db_server;
}


void printTags(const std::string &label, const std::vector<std::string> &tags)
{
  if (!label.empty()) {
    std::cout << label << std::endl;
  }
  for ( std::vector<std::string>::const_iterator tag_iter = tags.begin();
	tag_iter != tags.end();
	++tag_iter) {
    std::cout << *tag_iter << std::endl;
  }
}

std::string makeTimeString(time_t a_time)
{
  std::string a_time_string = ctime(&a_time);
  std::string::size_type ret_pos = a_time_string.find("\r");
  std::string::size_type new_line_pos = a_time_string.find("\n");
  if (new_line_pos == std::string::npos || (new_line_pos>ret_pos && ret_pos != std::string::npos)) {
    new_line_pos =ret_pos;
  }
  if (new_line_pos != std::string::npos ) {
    a_time_string.erase(new_line_pos, a_time_string.size()-new_line_pos);
  }
  return a_time_string;
}

void printRevisions(const std::string &label, const std::set<PixA::Revision_t> &revisions, unsigned int verbose=0)
{
  if (!label.empty()) {
    std::cout << label << std::endl;
  }
  for ( std::set<PixA::Revision_t>::const_iterator rev_iter = revisions.begin();
	rev_iter != revisions.end();
	++rev_iter) {
    std::cout << *rev_iter;
    if (verbose>0) {
      std::cout << " : " << makeTimeString(*rev_iter);
    }
    std::cout << std::endl;
  }
}


std::map<std::string, PixA::CfgLocation_t::EBackEnd> s_backEnd;

PixA::CfgLocation_t::EBackEnd parseBackEnd( const std::string &back_end_name) {
  if (s_backEnd.empty()) {
    s_backEnd.insert(std::make_pair(std::string("DbServer"),PixA::CfgLocation_t::kDbServer));
    s_backEnd.insert(std::make_pair(std::string("dbserver"),PixA::CfgLocation_t::kDbServer));
    s_backEnd.insert(std::make_pair(std::string("db-server"),PixA::CfgLocation_t::kDbServer));
    s_backEnd.insert(std::make_pair(std::string("DBSERVER"),PixA::CfgLocation_t::kDbServer));
    s_backEnd.insert(std::make_pair(std::string("DB-SERVER"),PixA::CfgLocation_t::kDbServer));
    s_backEnd.insert(std::make_pair(std::string("ConfigRootIO"),PixA::CfgLocation_t::kConfigRootIO));
    s_backEnd.insert(std::make_pair(std::string("RootNew"),PixA::CfgLocation_t::kConfigRootIO));
    s_backEnd.insert(std::make_pair(std::string("file"),PixA::CfgLocation_t::kConfigRootIO));
    s_backEnd.insert(std::make_pair(std::string("FILE"),PixA::CfgLocation_t::kConfigRootIO));
  }
  std::map<std::string, PixA::CfgLocation_t::EBackEnd>::const_iterator back_end_iter = s_backEnd.find(back_end_name);
  if (back_end_iter == s_backEnd.end()) {
    std::stringstream message;
    message << "FATAL [parseBackEnd] Unknown back-end : " << back_end_name;
    std::cout << message.str() << std::endl;
    throw std::runtime_error(message.str());
  }
  return back_end_iter->second;
}

std::map<std::string, PixA::IConnObjConfigDbManager::EObjectType> s_typeMap;

PixA::IConnObjConfigDbManager::EObjectType parseObjType(const std::string &obj_type) {

  if (obj_type.empty()) return PixA::IConnObjConfigDbManager::kNObjectTypes;

  if (s_typeMap.empty()) {
    s_typeMap.insert(std::make_pair(std::string("MODULE"),PixA::IConnObjConfigDbManager::kModuleObjects));
    s_typeMap.insert(std::make_pair(std::string("module"),PixA::IConnObjConfigDbManager::kModuleObjects));
    s_typeMap.insert(std::make_pair(std::string("Module"),PixA::IConnObjConfigDbManager::kModuleObjects));
    s_typeMap.insert(std::make_pair(std::string("PixModule"),PixA::IConnObjConfigDbManager::kModuleObjects));

    s_typeMap.insert(std::make_pair(std::string("PixModuleGroup"),PixA::IConnObjConfigDbManager::kModuleGroupObjects));
    s_typeMap.insert(std::make_pair(std::string("ROD"),PixA::IConnObjConfigDbManager::kModuleGroupObjects));
    s_typeMap.insert(std::make_pair(std::string("rod"),PixA::IConnObjConfigDbManager::kModuleGroupObjects));

    s_typeMap.insert(std::make_pair(std::string("PixTrigController"),PixA::IConnObjConfigDbManager::kTrigControllerObjects));
    s_typeMap.insert(std::make_pair(std::string("TIM"),PixA::IConnObjConfigDbManager::kTrigControllerObjects));
    s_typeMap.insert(std::make_pair(std::string("tim"),PixA::IConnObjConfigDbManager::kTrigControllerObjects));

    s_typeMap.insert(std::make_pair(std::string("PixDisable"),PixA::IConnObjConfigDbManager::kDisableObjects));
    s_typeMap.insert(std::make_pair(std::string("DISABLE"),PixA::IConnObjConfigDbManager::kDisableObjects));
    s_typeMap.insert(std::make_pair(std::string("disable"),PixA::IConnObjConfigDbManager::kDisableObjects));
  }

  std::map<std::string, PixA::IConnObjConfigDbManager::EObjectType>::const_iterator type_iter = s_typeMap.find(obj_type);
  if (type_iter == s_typeMap.end()) {
    std::stringstream message;
    message << "ERROR [parseObjType] Unknown type : " << obj_type << ". Possible values are : module, rod, tim, disable + other spellings ." << std::endl;
    throw std::runtime_error(message.str());
  }
  return type_iter->second;
}

enum EConnType {kModuleConn, kRODConn,kBOCConn,kControllerConn, kTimConn, kDisableConn, kNConnTypes};
std::vector<std::pair<PixA::Regex_t *, EConnType> > s_connTypePatterns;

void init() {
  if (s_connTypePatterns.empty()) {

    s_connTypePatterns.push_back(std::make_pair(new PixA::Regex_t("L[012]_B[[:digit:]]+_S[12]_[AC][67]_M[1-6][AC]"),kModuleConn));
    s_connTypePatterns.push_back(std::make_pair(new PixA::Regex_t("L[012]_B[[:digit:]]+_S[12]_[AC][7]_M0"),kModuleConn));
    s_connTypePatterns.push_back(std::make_pair(new PixA::Regex_t("D[123][AC]_B[[:digit:]]+_S[12]_M[1-6]"),kModuleConn));

    s_connTypePatterns.push_back(std::make_pair(new PixA::Regex_t("M5[[:digit:]]+"),kModuleConn));

    s_connTypePatterns.push_back(std::make_pair(new PixA::Regex_t("ROD_[BCDL][1234]_S[[:digit:]]+"),kRODConn));
    s_connTypePatterns.push_back(std::make_pair(new PixA::Regex_t("ROD_C[[:digit:]]_S[[:digit:]]+"),kRODConn));

    s_connTypePatterns.push_back(std::make_pair(new PixA::Regex_t("ROD_[BCDL][1234]_S[[:digit:]]+_BOC"),kBOCConn));
    s_connTypePatterns.push_back(std::make_pair(new PixA::Regex_t("ROD_C[[:digit:]]_S[[:digit:]]+_BOC"),kBOCConn));

    s_connTypePatterns.push_back(std::make_pair(new PixA::Regex_t("ROD_[BCDL][1234]_S[[:digit:]]+_ROD"),kControllerConn));
    s_connTypePatterns.push_back(std::make_pair(new PixA::Regex_t("ROD_C[[:digit:]]_S[[:digit:]]+_ROD"),kControllerConn));

    s_connTypePatterns.push_back(std::make_pair(new PixA::Regex_t("TIM_[BCDL][1234]"),kTimConn));
    s_connTypePatterns.push_back(std::make_pair(new PixA::Regex_t("TIM_C[[:digit:]]"),kTimConn));

  }
}

void readCfg(const std::pair<std::string, EConnType> &conn_type,
	     const std::string &result_file_name,
	     const std::string &id_name,
	     const std::string &cfg_tag_name,
	     PixA::Revision_t conn_rev,
	     const PixLib::Config *ref,
	     int verbose)
{
  try {

    std::shared_ptr<PixLib::PixModule>        module_ptr;
    std::shared_ptr<PixLib::PixModuleGroup>   module_grp_ptr;
    std::shared_ptr<PixLib::PixDisable>       disable_ptr;
    PixLib::Config *config_ptr=NULL;

    switch (conn_type.second) {
    case kRODConn:
    case kBOCConn:
    case kControllerConn: {
      module_grp_ptr = std::shared_ptr<PixLib::PixModuleGroup>(new PixLib::PixModuleGroup(conn_type.first));

      switch (conn_type.second) {
      case kRODConn: {
	config_ptr = &(module_grp_ptr->config());
	break;
      }
      case kBOCConn: {
	if (module_grp_ptr->getPixBoc()) {
	  config_ptr = module_grp_ptr->getPixBoc()->getConfig();
	}
	break;
      }
      case kControllerConn: {
	if (module_grp_ptr->getPixController()) {
	  config_ptr = &(module_grp_ptr->getPixController()->config());
	}
	break;
      }
      default: {
	break;
      }
      }
	  
      break;
    }
    case kModuleConn:
      module_ptr = std::shared_ptr<PixLib::PixModule>(new PixLib::PixModule(NULL,NULL, "", "", conn_type.first));
      config_ptr = &(module_ptr->config());
      config_ptr->m_confName = conn_type.first;
      break;
    case kTimConn:
      break;
    case kDisableConn:
      disable_ptr = std::shared_ptr<PixLib::PixDisable>(new PixLib::PixDisable(conn_type.first));
      config_ptr = &(disable_ptr->config());
      break;
    default:
      std::cout << "ERROR [readCfg] Unhandled config type " << conn_type.first << "(" << conn_type.second << ")" <<std::endl;
      break;
    }

    if (config_ptr) {
      std::shared_ptr<RootConf::IConfigStore> store( RootConf::ConfigStoreFactory::configStore());
      store->read(*(config_ptr), result_file_name,cfg_tag_name,conn_rev);
      if (verbose>0) {
	std::cout << "INFO [readCfg] Read  " << conn_type.first << "(" << conn_type.second << ") " 
		  << id_name << ":" << cfg_tag_name << ";" << conn_rev
		  <<std::endl;
      }
      if (verbose>5 || (verbose>2 && !ref)) {
	config_ptr->dump(std::cout);
      }

    }
    if (ref) {
      std::cout << "INFO [readCfg] Compare config for  " << conn_type.first << "(" << conn_type.second << ") " 
		<< id_name << ":" << cfg_tag_name << ";" << conn_rev
		<<std::endl;
      std::string old_name = config_ptr->m_confName;
      const_cast<PixLib::Config&>(*config_ptr).m_confName = ref->m_confName;
      if (const_cast<PixLib::Config&>(*ref)==const_cast<PixLib::Config&>(*config_ptr)) {
	std::cout << "INFO [readCfg] revisions   " << conn_type.first << "(" << conn_type.second << ") " 
		  << id_name << ":" << cfg_tag_name << ";" << conn_rev
		  << " agree." 
		  <<std::endl;
      }
      else {
	if (verbose==4) {
	  std::cout << "INFO [readCfg] Config " << conn_type.first << " from " << result_file_name << "(" << cfg_tag_name << ";" << conn_rev << ")" << std::endl;
	  config_ptr->dump(std::cout);
	}
	if (verbose==4) {
	  std::cout << "INFO [readCfg] Reference Config " << conn_type.first  << "(" << cfg_tag_name << ";" << conn_rev << ")" << std::endl;
	  const_cast<PixLib::Config*>(ref)->dump(std::cout);
	}
	std::stringstream message;
	message << "ERROR [readCfg] Configurations for " << conn_type.first  <<   " "  << id_name << ":?;" << conn_rev << " does not agree with contents of file "
		<< result_file_name << std::endl;
	throw std::runtime_error(message.str());
      }
      const_cast<PixLib::Config&>(*config_ptr).m_confName = old_name;

    }
  }
  catch (PixA::BaseException &err) {
    std::cout << "ERROR [readCfg] Caught exception : " <<  err.getDescriptor() << std::endl;
  }
  catch (std::exception &err) {
    std::cout << "ERROR [readCfg] Caught exception : " <<  err.what() << std::endl;
  }
  catch (...) {
    std::cout << "ERROR [readCfg] Caught unknown exception." << std::endl;
  }

}

std::pair<std::string, EConnType> getConnType(const std::string &conn_name) {

  init();

  EConnType conn_type = kDisableConn;
  std::string conn_name_base(conn_name);
  for(std::vector<std::pair<PixA::Regex_t *,EConnType> >::const_iterator pattern_iter = s_connTypePatterns.begin();
      pattern_iter != s_connTypePatterns.end();
      ++pattern_iter) {
    if (pattern_iter->first->match(conn_name)) {
      conn_type = pattern_iter->second;
      break;
    }
  }
  if (conn_type==kBOCConn) {
    if (conn_name.size()>4 && conn_name.compare(conn_name.size()-4,4,"_BOC")==0) {
      conn_name_base=conn_name.substr(0,conn_name.size()-4);
    }
  }
  else if (conn_type == kControllerConn) {
    if (conn_name.size()>4 && conn_name.compare(conn_name.size()-4,4,"_ROD")==0) {
      conn_name_base=conn_name.substr(0,conn_name.size()-4);
    }
  }
  return std::make_pair(conn_name_base, conn_type);
}

time_t parseTime(const std::string &time_str)
{
  if (time_str.empty() || time_str=="now" || time_str=="NOW") {
    return time(0);
  }

  if (time_str[0]=='-') {
    char *end_ptr=0;
    long value = strtol(time_str.c_str(),&end_ptr,10);
    time_t now=time(0);
    switch( *end_ptr) {
    case 'w': {
      return now + value*3600*24*7;
    }
    case 'd': {
      return now + value*3600*24;
    }
     case 'h': {
      return now + value*3600;
    }
    case 's':
    default: {
      return now + value;
    }
    }
  }
  else {
    const char *start_time = time_str.c_str();
    bool time_is_given_in_utc = false;
    if (time_str.compare(0,3, "utc")==0 || time_str.compare(0,3, "UTC")==0) {
      time_is_given_in_utc=true;
      start_time+=3;
      while (isspace(*start_time)) start_time++;
    }

    struct ::tm time_struct;
    // supported formats
    // Fri Mar 14 20:47:52 2008
    // Mar 14 20:47:52 2008
    // 2008-04-07 06:47:16
    std::vector<std::string> time_formats;
    //    time_formats.push_back("%Ea %Eb %d %H:%M:$S %Y");
    //    time_formats.push_back("%Eb %d %H:%M:$S %Y");
    time_formats.push_back("%a %b %d %H:%M:%S %Y");
    time_formats.push_back("%a %b %d %H:%M:%S %Y");
    //    time_formats.push_back("%Eb %d %H:%M:%S %Y");
    time_formats.push_back("%b %d %H:%M:%S %Y");
    //    time_formats.push_back("%Y-%m-%d %H:%M:%S");
    time_formats.push_back("%Y-%m-%d %H:%M:%S");
    time_formats.push_back("%Y-%m-%d %H:%M");
    time_formats.push_back("%Y-%m-%d");
    time_formats.push_back("%u");
    for (std::vector<std::string>::const_iterator format_iter = time_formats.begin();
	 format_iter != time_formats.end();
	 format_iter++) {
      if (*format_iter=="%u") {
	char *end_ptr=NULL;
	long utc_time;
	utc_time = strtol(start_time,&end_ptr,10);
	if (!end_ptr || !(*end_ptr) || isspace(*end_ptr)) {
	  return static_cast<time_t>(utc_time);
	}
      }
      else {
      char *end_ptr = strptime(start_time, format_iter->c_str(),&time_struct);
      //      std::cout << *format_iter << static_cast<void *>(end_ptr) << " " << static_cast<unsigned long>(end_ptr - start_time) << std::endl;
      if (end_ptr) {
	//        std::cout << " last_char = \"" << *end_ptr << "\" " << static_cast<unsigned int>(*end_ptr) << std::endl;
	while (isspace(*end_ptr)) end_ptr++;
	if (*end_ptr=='\0' || ((strncmp(end_ptr,"UTC",3)==0 || strncmp(end_ptr,"utc",3)==0) && *(end_ptr+3)=='\0')) {
	  if (*end_ptr) {
	    time_is_given_in_utc=true;
	  }
	  time_t utc_time = mktime(&time_struct);
	  //	  std::cout << " input = " << time_str << " -> "<< utc_time << std::endl;
	  if (time_is_given_in_utc) {
	    struct ::tm gm_time_struct;
	    gmtime_r( &utc_time , &gm_time_struct);
	    gm_time_struct.tm_isdst=0;
	    utc_time = mktime(&gm_time_struct);
	    //	    std::cout << " utc = " << asctime(&gm_time_struct) << " -> "<< utc_time << std::endl;
	  }
	  return utc_time;
	}
      }
      }
    }
  }
  std::stringstream message;
  message << "Unsupported time format : " << time_str << ".";
  throw std::runtime_error(message.str());

}

std::string timeString(const ::time_t &a_time, bool time_only=false, bool week_day=false) {
  struct tm the_time;
  localtime_r(&a_time,&the_time);
  std::stringstream time_string;
  time_string	<< std::setfill('0');

  static char wday_name[7][4] = {
                      "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
                  };

  if (!time_only) {
    if (week_day) {
      time_string << wday_name[the_time.tm_wday];
    }
    time_string << std::setw(4) << the_time.tm_year+1900
		<< "-" << std::setw(2) << the_time.tm_mon+1
		<< "-" << std::setw(2) << the_time.tm_mday
		<< " ";
  }
  time_string << std::setw(2) << the_time.tm_hour
	      << ":" << std::setw(2) << the_time.tm_min
	      << ":" << std::setw(2) << the_time.tm_sec;
  return time_string.str();
}

class ConnData_t
{
public:
  ConnData_t(const std::string &conn_name, PixA::Revision_t rev, const std::string &file_name="")
    : m_connName(conn_name),
      m_fileName(file_name),
      m_rev(rev)
  {}

  void setFileName(const std::string &file_name) { m_fileName=file_name;}
  void setRevision(PixA::Revision_t rev) { m_rev=rev; }

  const std::string &connName() const { return m_connName; }
  const std::string &fileName() const { return m_fileName; }
  PixA::Revision_t revision() const   { return m_rev; }

private:
  std::string      m_connName;
  std::string      m_fileName;
  PixA::Revision_t m_rev;
};

int main(int argc, char **argv)
{

  std::string id_name;
  std::string cfg_tag_name;
  std::string dest_cfg_tag_name="test";
  std::vector< ConnData_t > conn_name;
  PixA::Revision_t rev=0;
  bool error=false;
  //  bool db_server_tags=false;
  //  bool show_disabled_objects=false;
  unsigned int verbose=0;
  std::string obj_type_name;
  std::string result_file_name;
  bool list_tags=false;
  bool scan_revisions=false;
  bool compare_new=false;
  time_t start_time = 0;
  time_t end_time=UINT_MAX;
  bool randomise=false;

  std::shared_ptr<PixLib::PixDbServerInterface> db_server;
  time_t current_time = time(0);
  rev = current_time;
  for (int arg_i=1; arg_i<argc; arg_i++) {

    if (strcmp(argv[arg_i],"--db-server")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-' ) {

      try {
	std::string db_server_partition_name = argv[++arg_i];
	std::string db_server_name = ( (arg_i+1<argc && argv[arg_i+1][0]!='-') ? argv[++arg_i] : "PixelDbServer");
	db_server = dbServer(db_server_partition_name, db_server_name, argc, argv);
      }
      catch (std::runtime_error &err) {
	std::cout << "FATAL [main] caught exception (std::exception) : " << err.what() << std::endl;
	return -1;
      }

    }
    else if (strcmp(argv[arg_i],"-i")==0 && arg_i+1<argc) {
      id_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-n")==0 && arg_i+1<argc) {
      ConnData_t a_conn_name(argv[++arg_i],rev);
      if (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	a_conn_name.setRevision( atoi(argv[++arg_i]) );
	if (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	  a_conn_name.setFileName( argv[++arg_i] );
	}
      }
      conn_name.push_back( a_conn_name );

    }
    else if (strcmp(argv[arg_i],"-t")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      cfg_tag_name = argv[++arg_i];
      if (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	rev=atoi(argv[++arg_i]);
      }
    }
    else if (strcmp(argv[arg_i],"-v")==0 || strcmp(argv[arg_i],"--verbose")==0) {
      if (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	verbose=atoi(argv[++arg_i]);
      }
      else {
	verbose++;
      }
    }
    else if (strcmp(argv[arg_i],"-l")==0 || strcmp(argv[arg_i],"--list-tags")==0) {
      list_tags=true;
    }
    else if ((strcmp(argv[arg_i],"-b")==0 || strcmp(argv[arg_i],"--start-revision")==0  || strcmp(argv[arg_i],"--begin-revision")==0) && arg_i+1<argc) {
      start_time=parseTime(argv[++arg_i]);
    }
    else if ((strcmp(argv[arg_i],"-e")==0 || strcmp(argv[arg_i],"--end-revision")==0) && arg_i+1<argc ) {
      end_time=parseTime(argv[++arg_i]);
    }
    else if (strcmp(argv[arg_i],"-s")==0 || strcmp(argv[arg_i],"--scan-revisions")==0) {
      scan_revisions=true;
    }
    else if (strcmp(argv[arg_i],"-c")==0 || strcmp(argv[arg_i],"--compare")==0) {
      compare_new=true;
    }
    else if (strcmp(argv[arg_i],"-f")==0  && arg_i+1<argc) {
      result_file_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-r")==0) {
      randomise=true;
    }
    else {
      std::cout << "INFO [" << argv[0] << ":main] Unhandled argument : " << argv[arg_i] << "." << std::endl;
      error=true;
    }
  }

  if (scan_revisions) {
    PixA::ConnObjConfigDb cfg_db(PixA::PixConnObjConfigDbManager::createInstance( db_server));

    std::vector<std::string> id_tags;
    if (id_name.empty()) {
      cfg_db.getIdTags(id_tags);
    }
    else {
      id_tags.push_back(id_name);
    }

    std::map<std::string, std::vector<std::string> > tag_list;
    for (std::vector<std::string>::iterator id_iter = id_tags.begin();
	 id_iter != id_tags.end();
	 ++id_iter) {
      if (id_iter->size()>4 && id_iter->compare(id_iter->size()-4,4,"-CFG")==0) {
	*id_iter = id_iter->substr(0,id_iter->size()-4);
      }
      cfg_db.getTags(*id_iter, tag_list[*id_iter]);
      if (verbose>4) {
	std::cout << "INFO ["<<argv[0]<< ":main] tags for " << *id_iter << " : " << tag_list.size() << " -> " << tag_list[ *id_iter].size() << std::endl;
      }
    }

    for(std::vector< ConnData_t >::const_iterator conn_iter = conn_name.begin();
	conn_iter != conn_name.end();
	++conn_iter) {

      std::pair<std::string, EConnType> conn_type = getConnType(conn_iter->connName());
      std::map<std::string, std::pair< std::map<std::string, unsigned int>, unsigned int>  > revision_summary;


      for(std::map<std::string, std::vector< std::string > >::const_iterator id_iter = tag_list.begin();
	  id_iter != tag_list.end();
	  ++id_iter) {

	if (verbose>4) {
	  std::cout << "INFO ["<<argv[0]<< ":main] revisions for " << id_iter->first << " : "  << std::endl;
	}

	std::map<unsigned int, std::string > revision_list;
	revision_summary[id_iter->first].second=0;
	for (std::vector<std::string>::const_iterator tag_iter = id_iter->second.begin();
	     tag_iter != id_iter->second.end();
	     ++tag_iter) {
	  
	  std::set<PixA::Revision_t> conn_revisions;
	  cfg_db.getRevisions(id_iter->first, *tag_iter, conn_type.first,conn_revisions);
	  revision_summary[id_iter->first].first[*tag_iter]=conn_revisions.size();
	  revision_summary[id_iter->first].second += conn_revisions.size();

	  if (verbose>4) {
	    std::cout << "INFO ["<<argv[0]<< ":main] revisions for " << id_iter->first << " : " << *tag_iter << "  = " << conn_revisions.size() << std::endl;
	  }

	}
      }
      std::cout << "INFO ["<<argv[0]<< ":main] Revision summary for  " << conn_iter->connName() << " : " << std::endl;
      for (std::map<std::string, std::pair< std::map<std::string, unsigned int>, unsigned int>  >::const_iterator id_iter =  revision_summary.begin();
	   id_iter != revision_summary.end();
	   ++id_iter) {
	std::cout << "INFO ["<<argv[0]<< ":main] " << std::setw(30) << id_iter->first << " : " << id_iter->second.second << std::endl;
	for (std::map<std::string, unsigned int>::const_iterator tag_iter =  id_iter->second.first.begin();
	     tag_iter != id_iter->second.first.end();
	     ++tag_iter) {
	  std::cout << "INFO ["<<argv[0]<< ":main]             " << std::setw(30) << tag_iter->first << " : " << tag_iter->second << std::endl;
	}
      }
      std::cout << std::endl;

      std::cout << "INFO ["<<argv[0]<< ":main] Revision summary for  " << conn_iter->connName() << " : " << std::endl;
      for (std::map<std::string, std::pair< std::map<std::string, unsigned int>, unsigned int>  >::const_iterator id_iter =  revision_summary.begin();
	   id_iter != revision_summary.end();
	   ++id_iter) {
	std::cout << "INFO ["<<argv[0]<< ":main] " << std::setw(30) << id_iter->first << " : " << id_iter->second.second << std::endl;
      }

      std::cout << std::endl;
    }

    return -1;
  }
  else if (id_name.empty()) {
    PixA::ConnObjConfigDb cfg_db(PixA::PixConnObjConfigDbManager::createInstance( db_server));
    std::vector<std::string> id_tags;
    cfg_db.getIdTags(id_tags);
    printTags( " ID tags :", id_tags );

  }
  else if (list_tags) {
    if (!result_file_name.empty() && !conn_name.empty() ) {

      for(std::vector< ConnData_t >::const_iterator conn_iter = conn_name.begin();
	  conn_iter != conn_name.end();
	  ++conn_iter) {

	std::pair<std::string, EConnType> conn_type = getConnType(conn_iter->connName());

	try {

	  PixLib::Config temp(std::string(conn_type.first));
	  std::vector<RootConf::Revision_t> file_revisions;
	  std::cout << "INFO [main:" << argv[0] <<"] revisions for " << temp.name() << " in " << cfg_tag_name << " :" << std::endl;
	  std::shared_ptr<RootConf::IConfigStore> store( RootConf::ConfigStoreFactory::configStore());
	  store->getRevisions(temp,result_file_name,"all",file_revisions);
	  for (std::vector<RootConf::Revision_t>::const_iterator rev_iter = file_revisions.begin();
	       rev_iter != file_revisions.end();
	       rev_iter++) {
	    time_t rev_time(*rev_iter);
	    std::cout << *rev_iter  << "  |  " << ctime(&rev_time) << std::endl;
	  }
	}
	catch (PixA::BaseException &err) {
	  std::cout << "ERROR ["<<argv[0]<< ":main] Caught exception : " <<  err.getDescriptor() << std::endl;
	}
	catch (std::exception &err) {
	  std::cout << "ERROR ["<<argv[0]<< ":main] Caught exception : " <<  err.what() << std::endl;
	}
	catch (...) {
	  std::cout << "ERROR ["<<argv[0]<< ":main] Caught unknown exception." << std::endl;
	}

      }

    }
    else {
    PixA::ConnObjConfigDb cfg_db(PixA::PixConnObjConfigDbManager::createInstance( db_server));

    std::vector<std::string> tags;
    cfg_db.getTags(id_name, tags);
    printTags( std::string(" Tags in ") + id_name + " : ", tags );
    }
    return -1;
  }

  if (error || id_name.empty() || conn_name.empty() || result_file_name.empty()) {
    std::cout << "USAGE " << argv[0] << "[--db-server partition [name] ] -i id-tag  -f output-file-name -n conn-name [-n conn-name2 [...] ]   [-v [-v [...] ] ] "
	      << " [-l/--list-tags] [-s/--scan-revisions] [-t tag [rev] ] [-c/--compare]" << std::endl
	      << " [-b/--start-time start-time] [-e/--end-time end-time]" << std::endl
	      << std::endl
	      << "\t-i id-tag\t\tId tag to copy into root tree" << std::endl
	      << "\t-n conn-name\tname of the object to be copied: connectivity name of a ROD or TIM,"
	      << "\t\t\t\t\tproduction name of a module, name of a disable. Can be used multiple times." << std::endl
	      << "\t-f file-name\tnName of the root file in which the ROOT tree will be stored." << std::endl
	      << std::endl
	      << "\t-b/--start-time\t\tonly consider revisions starting from this date."<<std::endl
	      << "\t\t\t\tformat: now, -1h, -1d,-1w (one hour, day, week ago); YYYY-MM-DD [hh:mm:ss]" << std::endl
	      << "\t-e/--end-time\t\tOnly consider revisions after this date." << std::endl
	      << std::endl
	      << "-l\t\t\tto only list revisions or tags" << std::endl
	      << "-s\t\t\tScan the given id tag and count the revisions per tag." << std::endl
	      << "-c\t\t\tCompare a revision which the one stored in the root tree." << std::endl
	      << "-v [n]\t\tIncrease verbosity or set verbosity to the given level." <<std::endl
	      << std::endl;

    return -1;
  }

  if (start_time!=0) {
    std::cout << "INFO [" <<argv[0] <<":main] Only consider revisions which are at least as recent as :" << timeString(start_time,false,true) << std::endl;
  }
  assert(sizeof(time_t)==sizeof(unsigned int));
  if (end_time!=static_cast<time_t>(UINT_MAX)) {
    std::cout << "INFO [" <<argv[0] <<":main] Only consider revisions which are older than :" << timeString(end_time,false,true) << std::endl;
  }
  //  PixA::ConnObjConfigDb::useDbServer( db_server );

  PixA::ConnObjConfigDb cfg_db(PixA::PixConnObjConfigDbManager::createInstance( db_server));


  if (verbose%5==0) {
    std::cout << "INFO ["<<argv[0]<<":main] " << timeString(::time(0)) <<" Get tags " << std::endl;
  }

  std::vector<std::string> tag_list;
  cfg_db.getTags(id_name, tag_list);

  unsigned int error_count=0;

  for(std::vector< ConnData_t >::const_iterator conn_iter = conn_name.begin();
      conn_iter != conn_name.end();
      ++conn_iter) {

    if (verbose%5==0) {
      std::cout << "INFO ["<<argv[0]<<":main] " << timeString(::time(0)) <<" Process " << conn_iter->connName() << std::endl;
    }

    std::string a_file_name(result_file_name);
    if (!conn_iter->fileName().empty()) {
      a_file_name = conn_iter->fileName();
    }
    std::pair<std::string, EConnType> conn_type = getConnType(conn_iter->connName());

    if (   ((conn_iter->revision()>0) && (!cfg_tag_name.empty()))
	   || (rev>0 && !cfg_tag_name.empty())) {

      PixA::Revision_t conn_rev = (conn_iter->revision()>0 ? conn_iter->revision() : rev);
      if (verbose>1) {
	std::cout << "INFO ["<<argv[0]<<":main] " << conn_type.first << " get " 
		  << id_name << ":" << cfg_tag_name << ";" << conn_rev
		  << std::endl;
      }

      if (verbose%5==0) {
	std::cout << "INFO ["<<argv[0]<<":main] " << timeString(::time(0)) <<" Read config " << conn_type.first
		  << " " << id_name << "all;" << conn_rev << " from " << a_file_name << std::endl;
      }

      readCfg(conn_type, a_file_name, id_name,"all", conn_rev, NULL, verbose);

    }
    else {

      if (verbose>2) {
	std::cout << "INFO ["<<argv[0]<<":main] get all revisions for " << conn_type.first << "." << std::endl;
      }

      if (verbose%5==0) {
	std::cout << "INFO ["<<argv[0]<<":main] " << timeString(::time(0)) <<" Get all revisions for " << conn_type.first
		  << " in " << id_name << "." << std::endl;
      }


      std::map<unsigned int, std::string > revision_list;
      for (std::vector<std::string>::const_iterator tag_iter = tag_list.begin();
	   tag_iter != tag_list.end();
	   ++tag_iter) {

	if (verbose>3) {
	  std::cout << "INFO ["<<argv[0]<<":main] get revisions for " << conn_type.first << " " << id_name << ":"
		    << *tag_iter << "." << std::endl;
	}

	if (verbose%5==0) {
	  std::cout << "INFO ["<<argv[0]<<":main] " << timeString(::time(0)) <<" Get revisions for " << conn_type.first
		    << id_name << ":" << *tag_iter << "." << std::endl;
	}

	std::set<PixA::Revision_t> conn_revisions;
	cfg_db.getRevisions(id_name, *tag_iter, conn_type.first,conn_revisions);
	if (verbose>4) {
	  std::cout << "INFO ["<<argv[0]<<":main] get found  "<< conn_revisions.size() << " for "  << conn_type.first << " " << id_name << ":"
		    << *tag_iter << "." << std::endl;
	}

	for(std::set<PixA::Revision_t>::const_iterator rev_iter = conn_revisions.begin();
	    rev_iter != conn_revisions.end();
	    ++rev_iter) {
	  if (*rev_iter>=static_cast<PixA::Revision_t>(start_time) && *rev_iter<static_cast<PixA::Revision_t>(end_time)) {
	    revision_list.insert(std::make_pair(*rev_iter, *tag_iter));
	  }
	}
      }

      if (verbose%5==0) {
	std::cout << "INFO ["<<argv[0]<<":main] " << timeString(::time(0)) <<" Now read and write all " << revision_list.size()
		  << " revision(s) for " << conn_type.first << "." << std::endl;
      }

      std::vector< std::pair<PixA::Revision_t, std::string> > permuted_list;
      std::vector< unsigned int > index_list;
      permuted_list.resize(revision_list.size());
      index_list.reserve(revision_list.size());
      unsigned int index=0;

      for(unsigned int an_index=0; an_index<revision_list.size(); an_index++) {
	index_list.push_back( an_index);
      }

      for(std::map<PixA::Revision_t, std::string>::const_iterator rev_iter = revision_list.begin();
	  rev_iter != revision_list.end();
	  ++rev_iter, ++index) {

	if (randomise) {
	  assert(index_list.size()>0);
	  unsigned int the_index_index=rand()%index_list.size();
	  std::cout << "INFO ["<< argv[0] << ":main] " << the_index_index << " " << index_list.size() << std::endl;
	  
	  unsigned int counter=0;
	  std::vector<unsigned int>::iterator index_iter;
	  for (index_iter =  index_list.begin(); 
	       counter < the_index_index && index_iter != index_list.end();
	       ++index_iter,++counter ) {}

	  assert(index_iter != index_list.end());
	  index=*index_iter;
	  index_list.erase(index_iter);
	}

	permuted_list[index]=*rev_iter;
      }

      for(std::vector<std::pair<PixA::Revision_t, std::string> >::const_iterator rev_iter = permuted_list.begin();
	  rev_iter != permuted_list.end();
	  ++rev_iter) {
	try {
	  if (verbose>1) {
	    std::cout << "INFO ["<<argv[0]<<":main] get " << conn_type.first << " " << id_name << ":" << rev_iter->second << ";" << rev_iter->first << std::endl;
	  }

	  if (verbose%5==0) {
	    std::cout << "INFO ["<<argv[0]<<":main] " << timeString(::time(0)) <<" Get tag "
		      << id_name << ":" << rev_iter->second << ";" << rev_iter->first << "." << std::endl;
	  }

	  PixA::ConnObjConfigDbTagRef a_tag( cfg_db.getTag(id_name, rev_iter->second, rev_iter->first) );
	  std::shared_ptr<PixLib::PixModule>         module_ptr;
	  std::shared_ptr<PixLib::PixModuleGroup>    module_grp_ptr;
	  std::shared_ptr<PixLib::PixTrigController> tim_ptr;
	  std::shared_ptr<const PixLib::PixDisable>        disable_ptr;
	  std::vector< std::pair< std::string, PixLib::Config *> > config_list;

	  if (verbose%5==0) {
	    std::cout << "INFO ["<<argv[0]<<":main] " << timeString(::time(0)) <<" Create configured object  "
		      << conn_type.first << " using configuration " 
		      << id_name << ":" << rev_iter->second << ";" << rev_iter->first << "." << std::endl;
	  }

	  switch (conn_type.second) {
	  case kRODConn:
	  case kBOCConn:
	  case kControllerConn:
	    module_grp_ptr=std::shared_ptr<PixLib::PixModuleGroup>(a_tag.createConfiguredModuleGroup(conn_type.first));
	    if (module_grp_ptr.get()) {
	      if (conn_type.second == kRODConn) {
		config_list.push_back( std::make_pair(conn_type.first,&(module_grp_ptr->config())) );
	      }
	      if (conn_type.second != kControllerConn) {
		config_list.push_back( std::make_pair(conn_type.first+"_BOC",module_grp_ptr->getPixBoc()->getConfig()) );
	      }
	      if (conn_type.second != kBOCConn) {
		config_list.push_back( std::make_pair(conn_type.first+"_ROD",&(module_grp_ptr->getPixController()->config())) );
	      }
	    }
	    break;
	  case kModuleConn:
	    module_ptr=std::shared_ptr<PixLib::PixModule>(a_tag.createConfiguredModule(conn_type.first));
	    config_list.push_back( std::make_pair(conn_type.first,&(module_ptr->config())) ) ;
	    break;
	  case kTimConn:
	    tim_ptr=std::shared_ptr<PixLib::PixTrigController>(a_tag.createConfiguredTim(conn_type.first));
	    config_list.push_back( std::make_pair(conn_type.first,tim_ptr->config()) ) ;
	    break;
	  case kDisableConn:
	    disable_ptr=a_tag.disablePtr(conn_type.first);
	    config_list.push_back( std::make_pair(conn_type.first,&(const_cast<PixLib::PixDisable &>(*disable_ptr).config())) ) ;
	    break;
	  default:
	    std::cout << "ERROR ["<<argv[0]<< ":main] Unhandled config type " << conn_type.first << "(" << conn_type.second << ")" <<std::endl;
	    break;
	  }
	  for (std::vector<std::pair< std::string, PixLib::Config *> >::iterator config_iter = config_list.begin();
	       config_iter != config_list.end();
	       ++config_iter) {

	    if (config_iter->second) {
	      if (verbose>3) {
		std::cout << "INFO ["<<argv[0]<<":main] read " << conn_type.first << " " << id_name << ":" << rev_iter->second << ";" << rev_iter->first 
			  << std::endl;
	      }

	      if (!compare_new) {

		if (verbose%5==0) {
		  std::cout << "INFO ["<<argv[0]<<":main] " << timeString(::time(0)) <<" Write  configuration "
			    << id_name << ":" << rev_iter->second << ";" << rev_iter->first 
			    << " of " << conn_type.first << "  to " << a_file_name 
			    << "." << std::endl;
		}

		if (verbose>3) {
		  std::cout << "INFO ["<<argv[0]<<":main] write " << conn_type.first << " " << id_name << ":" << rev_iter->second << ";" << rev_iter->first 
			    << " to " << a_file_name
			    << std::endl;
		}

		std::shared_ptr<RootConf::IConfigStore> store( RootConf::ConfigStoreFactory::configStore());
		//		const std::string old_name = config_iter->second->m_confName;
		//		config_iter->second->m_confName = config_iter->first;
		store->write(*(config_iter->second), a_file_name,"all",rev_iter->first);
		//		config_iter->second->m_confName = old_name;

		if (verbose>2) {
		  std::cout << "INFO ["<<argv[0]<<":main] wrote " << conn_type.first << " " << id_name << ":" << rev_iter->second << ";" << rev_iter->first 
			    << " to " << a_file_name
			    << std::endl;
		}
	      }
	      else if (compare_new) {
		if (verbose>3) {
		  std::cout << "INFO ["<<argv[0]<<":main] compare " << conn_type.first << " " << id_name << ":" << rev_iter->second << ";" << rev_iter->first 
			    << " with revision from " << a_file_name
			    << std::endl;
		}
		readCfg(conn_type, a_file_name, id_name,"all", rev_iter->first, config_iter->second, verbose);
		if (verbose>3) {
		  std::cout << "INFO ["<<argv[0]<<":main] Revisions " << conn_type.first << " " << id_name << ":" << rev_iter->second << ";" << rev_iter->first 
			    << " and from " << a_file_name
			    << " agree."
			    << std::endl;
		}
	      }
	    }
	    else {
	      std::cout << "ERROR ["<<argv[0]<< ":main] No configuration for " <<  conn_type.first
			<< " " << id_name << ":" << rev_iter->second << ";" << rev_iter->first << std::endl;
	    }
	  }
      
	}
	catch (PixA::BaseException &err) {
	  std::cout << "ERROR ["<<argv[0]<< ":main] Caught exception : " <<  err.getDescriptor() << std::endl;
	  error_count++;
	}
	catch (std::exception &err) {
	  std::cout << "ERROR ["<<argv[0]<< ":main] Caught exception : " <<  err.what() << std::endl;
	  error_count++;
	}
	catch (...) {
	  std::cout << "ERROR ["<<argv[0]<< ":main] Caught unknown exception." << std::endl;
	  error_count++;
	}
      }

      if (verbose%5==0) {
	std::cout << "INFO ["<<argv[0]<<":main] " << timeString(::time(0)) <<" list all revisions in " << a_file_name
		  << " for " << conn_type.first 
		  << "." << std::endl;
      }

      if (verbose>3) {
	PixLib::Config temp(std::string(conn_type.first)+"_0");
	std::vector<RootConf::Revision_t> file_revisions;
	std::cout << "INFO [main:" << argv[0] <<"] revisions for " << temp.name() << " in " << cfg_tag_name << " :" << std::endl;
	std::shared_ptr<RootConf::IConfigStore> store( RootConf::ConfigStoreFactory::configStore());
	store->getRevisions(temp,a_file_name,"all",file_revisions);
	for (std::vector<RootConf::Revision_t>::const_iterator rev_iter = file_revisions.begin();
	     rev_iter != file_revisions.end();
	     rev_iter++) {
	  time_t rev_time(*rev_iter);
	  std::cout << *rev_iter  << "  |  " << ctime(&rev_time) << std::endl;
	}
      }

      if (verbose%5==0) {
	std::cout << "INFO ["<<argv[0]<<":main] " << timeString(::time(0)) <<" Done with " << conn_type.first 
		  << " errors = " << error_count
		  << "." << std::endl;
      }

    
    }
  }

  return 0;
}
