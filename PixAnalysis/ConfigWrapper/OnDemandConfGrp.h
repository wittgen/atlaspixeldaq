#ifndef _OnDemandConfGrp_h_
#define _OnDemandConfGrp_h_

#include <string>
#include <sstream>
#include <map>
#include <vector>
#include <stdexcept>
#include <iostream>

#include <Config/ConfObj.h>
#include "IConfGrp.h"
#include "PixDbAccessor.h"
#include "DbWrapper.h"
#include "BlockWiseStorage.h"
#include "pixa_exception.h"

#include <Config/ConfObj.h>
#include <Config/ConfMask.h>
#include "OnDemandConfig.h"

#include "ConfGrpContainer.h"

namespace PixA {

/** Class to create on demand configuration objects from PixDb fields.
 * The configuration objects will be cached. Thus for each object the
 * file will be accessed once.
*/
class OnDemandConfGrp : public IConfGrp, public PixDbAccessor
{
  friend class OnDemandConfig;

    template <class T>
    class ConfigPtr {
    public:
      ConfigPtr(T *ptr) : m_ptr(ptr) {
	m_ptr->IConfigBase::useNode();
      }
      ConfigPtr(ConfigPtr &a) : m_ptr(a.m_ptr) {
	m_ptr->IConfigBase::useNode();
      }
      ~ConfigPtr()
      {
	m_ptr->IConfigBase::doneWithNode();
      }

      ConfigPtr &operator = (ConfigPtr &b) {
	if (b->m_ptr != m_ptr) {
	  m_ptr->IConfigBase::doneWithNode();
	  m_ptr = b->m_ptr;
	  m_ptr->IConfigBase::useNode();
	}
	return this;
      }

      T &operator *() { return *m_ptr; }
      const T &operator *() const { return *m_ptr; }

      T *operator->() { return  m_ptr; }
      const T *operator->() const { return  m_ptr; }

    private:
      T *m_ptr;
    };


protected:
  typedef std::map<std::string, PixLib::ConfObj * >   ConfObjList_t;

  //  OnDemandConfGrp(ConfigRoot *root, DbWrapper &db,) : IConfGrp(root), PixDbAccessor(db) { }
  OnDemandConfGrp(OnDemandConfig &config, const std::string &name)
    : IConfGrp(config),
      PixDbAccessor(config),
      m_confGroupContainer(new ConfGrpContainer(*(config.m_configContainer),name)),
      m_groupName(name)
    { s_stat.ctor();
    updateConfGroupName();
    }

 public:
  ~OnDemandConfGrp() {
    //std::cout << "INFO [OnDemandConfGrp::dtor] delete interface for conf group = " << m_confGroupContainer->m_confGroup->m_groupName << std::endl;
    // the conf objects are deleted by the destructor of the physical conf obj
    s_stat.dtor();
  };

  /** Create a configuration object of the given name from the PixDb field if the object has not been creted yet.
   * The objects are only partially created i.e. default values , the comments and the visibility flag
   * are missing. Moreover, Integer vectors with 18*160 elements will always be treated as matrices of 
   * unsigned short. So, if there happen to be integer vectors of exactly this size, they cannot be 
   * accessed correctly.
   */
  PixLib::ConfObj &operator[](const std::string &name) ;

  void use()  { PixDbAccessor::accessStart(); IConfigBase::useNode(); }
  void done() { PixDbAccessor::accessEnd();   IConfigBase::doneWithNode(); };

  void createAll();

 private:

  template <class T> T &store(const T &);
  template <class T> T &storage() const;
  template <class T> T &storage(unsigned int n_cols, unsigned int n_rows, unsigned int n_def_val, unsigned int n_max_val) const;
  template <class T> void returnStorage( T &unused_elm) const;

  /** Load an object and add it to the conf obj list.
   */
  //  template <class T> PixLib::ConfObj &getMatrix(const std::string &name, const PixLib::dbFieldIterator &field) const;
  template <class T> PixLib::ConfObj &get(const std::string &name, const PixLib::dbFieldIterator &field) const;
  template <class T> PixLib::ConfObj *createConfObj(const std::string &name, T &a) const;
  template <class T>           PixLib::ConfObj *createConfMatrix(const std::string &name, std::vector<T> &a) const;
  template <class T, class T2> PixLib::ConfObj *createConfMatrix(const std::string &name, std::vector<T2> &a) const;
  template <class T, class T2> PixLib::ConfObj *createConfMatrixUnpack(const std::string &name, std::vector<T2> &a) const;

  PixLib::ConfObj &getConfObj(const std::string &name, PixLib::DbField *a_field, const  PixLib::dbFieldIterator &field) const;

  void updateConfGroupName() {
    if (m_confGroupContainer->m_confGroup->name().empty() && isDbInUse()) {
      m_confGroupContainer->updateConfGroupName(name());
    }
  }

  ConfigPtr<ConfGrpContainer> m_confGroupContainer;
  std::string     m_groupName;

  static ObjStat_t s_stat;
};

}
#endif
