#ifndef _ObjList_h_
#define _ObjList_h_

#include "pixa_exception.h"
#include <sstream>
#include <iomanip>
#include <iostream>
#include <map>

namespace PixA {

  /** Helper class to debug memory management
   */
  class ObjList {
  public:
    ObjList() {};
    ~ObjList() {stat();};

    void add(void *a) {
      unsigned long addr=reinterpret_cast<unsigned long>(a);
      std::map<unsigned long, int>::iterator iter = s_alive.find(addr);
      if (iter != s_alive.end()) {
	if (iter->second==0) {
	  iter->second++;
	}
	else {
	  std::stringstream message;
	  message << "FATAL [ObjList::add] Object " << std::hex << addr << std::dec << " exists already (counter=" << iter->second << ") . How can this be.";
	  throw ConfigException(message.str());
	}
      }
      else {
	s_alive[addr]=1;
      }
  //    stat();
    }

    void remove(void *a) {
      unsigned long addr=reinterpret_cast<unsigned long>(a);
      int &counter = s_alive[addr];
      if (counter == 0) {
	std::stringstream message;
	message << "FATAL [ObjList::remove] Object " << std::hex << addr << std::dec << " does not exist.";
	throw ConfigException(message.str());
      }
      counter--;
//      stat();
    }

    void stat() {
      for(std::map<unsigned long, int>::const_iterator iter=s_alive.begin();
	  iter!=s_alive.end();
	  iter++) {
	if (iter->second>0) {
	  std::cout << "INFO [ObjList::stat] Object " << std::hex << iter->first << std::dec << " still alive (counter = " << iter->second << ")." << std::endl;
	}
	else if (iter->second<0) {
	  std::cout << "INFO [ObjList::stat] Object " << std::hex << iter->first << std::dec  << " killed more than once. counter = "  << iter->second << std::endl;
	}
      }
    }
  private:
    std::map<unsigned long, int> s_alive;
  };

}
#endif
