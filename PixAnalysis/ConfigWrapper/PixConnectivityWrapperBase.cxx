#include "PixConnectivityWrapperBase.h"
#include "RootDbOpenHelper.h"
#include <PixModule/PixModule.h>
#include <CoralDB/CoralDB.h>

//#include <RelationalAccess/AuthenticationServiceException.h>


namespace PixA {


  PixConnectivityWrapperBase::~PixConnectivityWrapperBase() {
    delete m_connectivity;
  }

  class CoralConnDB {
  public:
    CoralConnDB(const std::string &connection_string)  {
      std::string::size_type ext_pos = connection_string.rfind('.');
      if (ext_pos  != std::string::npos) {
	std::string ext(connection_string,ext_pos+1, connection_string.size()-ext_pos);
	if (ext == "root") {
	  throw PixLib::PixDBException("FATAL [CoralConnDb] RootDb is not a supported as connectivity database.");
	}
      }
      m_db = std::make_shared<CoralDB::CoralDB>(connection_string, coral::ReadOnly, coral::Error, CoralDB::CoralDB::AUTOCOMMIT);
    }

    void getObjectTags(std::vector<std::string> &object_tag_out) {
      object_tag_out.clear();
      if (m_db.get()) {
      CoralDB::IdTagList idTags;
      m_db->getExistingObjectDictionaryTags(idTags);
      object_tag_out.reserve(idTags.size());
      for (CoralDB::IdTagList::const_iterator iter = idTags.begin();
	   iter != idTags.end();
	   iter++) {
	object_tag_out.push_back(iter->tag());
      }
      }
    }

    enum ETag {kConnectivity, kAlias, kPayload, kNTags};

    void getConnectivityTags(const std::string id_tag, std::vector<std::string> &tags_out) const { getTags(kConnectivity, id_tag, tags_out); }
    void getAliasTags(const std::string id_tag, std::vector<std::string> &tags_out) const { getTags(kAlias, id_tag, tags_out); }
    void getDataTags(const std::string id_tag, std::vector<std::string> &tags_out) const { getTags(kPayload, id_tag, tags_out); }

  protected:
    typedef void (CoralDB::CoralDB:: * GetTagsFuncPtr_t)(CoralDB::TagList&, const string&) const;

    void getTags(ETag tag, const std::string &id_tag, std::vector<std::string> &tags_out  ) const {
      assert( tag < kNTags );
      tags_out.clear();
      if (m_db.get()) {
      CoralDB::TagList tags;
      GetTagsFuncPtr_t func[kNTags]={
	&CoralDB::CoralDB::getExistingConnectivityTags,
	&CoralDB::CoralDB::getExistingAliasTags,
	&CoralDB::CoralDB::getExistingDataTags
      };
      ((*m_db).*(func[tag]))(tags, id_tag);
      tags_out.reserve(tags.size());
      for (CoralDB::TagList::const_iterator iter = tags.begin();
	   iter != tags.end();
	   iter++) {
	tags_out.push_back(iter->tag());
      }
      }
    }

  private:
    std::shared_ptr< CoralDB::CoralDB > m_db;
  };

  void PixConnectivityWrapperBase::getObjectTags(std::vector<std::string> *object_tag_out) 
  {
    if (object_tag_out) {
      std::string connection_string = getenv("PIX_CONN_DB");
      CoralConnDB db(connection_string);
      db.getObjectTags(*object_tag_out);
    }
  }

  void PixConnectivityWrapperBase::getConfigurationObjectTags(std::vector<std::string> *object_tag_out) 
  {
    if (object_tag_out) {
      std::string connection_string = getenv("PIX_CFG_DB");
      CoralConnDB db(connection_string);
      db.getObjectTags(*object_tag_out);
    }
  }

  void PixConnectivityWrapperBase::getConnectivityTags(const std::string &id_name,
						   std::vector<std::string> *conn_tag_out,
						   std::vector<std::string> *alias_tag_out,
						   std::vector<std::string> *payload_tag_out,
						   std::vector<std::string> *configuration_tag_out)
  {
    CoralConnDB db(getenv("PIX_CONN_DB"));
    //    std::shared_ptr<PixLib::PixConnectivity> conn(new PixLib::PixConnectivity("Base","Base","Base","Base",id_name));
    if (conn_tag_out) {
      db.getConnectivityTags(id_name,*conn_tag_out);
    }
    if (alias_tag_out) {
      db.getAliasTags(id_name,*alias_tag_out);
    }
    if (payload_tag_out) {
      db.getDataTags(id_name,*payload_tag_out);
    }
    if (configuration_tag_out) {
      CoralConnDB cfg_db(getenv("PIX_CFG_DB"));
      cfg_db.getConnectivityTags(id_name,*configuration_tag_out);
    }
  }


  /** Find the connectivity object of the given module.
   */
  const PixLib::ModuleConnectivity *PixConnectivityWrapperBase::findModule(const ModuleLocationBase &module_location) const
  {
    std::map<std::string,PixLib::ModuleConnectivity *>::const_iterator module_iter = m_connectivity->mods.find(module_location);
    if (module_iter == m_connectivity->mods.end()) return NULL;
    return module_iter->second;
  }

  const PixLib::Pp0Connectivity *PixConnectivityWrapperBase::findPp0(const Pp0LocationBase &pp0_location) const
  {
    std::map<std::string,PixLib::Pp0Connectivity *>::const_iterator pp0_iter = m_connectivity->pp0s.find(pp0_location);
    if (pp0_iter == m_connectivity->pp0s.end()) return NULL;
    return pp0_iter->second;
  }

  /** Find the connectivity object of the given ROD.
   */
  const PixLib::RodBocConnectivity *PixConnectivityWrapperBase::findRod(const RodLocationBase &rod_location) const
  {
    std::map<std::string,PixLib::RodBocConnectivity *>::const_iterator rod_iter = m_connectivity->rods.find(rod_location);
    if (rod_iter == m_connectivity->rods.end()) return NULL;
    return rod_iter->second;
  }

  /** Find the connectivity object of the given Crate.
   */
  const PixLib::RodCrateConnectivity *PixConnectivityWrapperBase::findCrate(const CrateLocationBase &crate_location) const
  {
    std::map<std::string,PixLib::RodCrateConnectivity *>::const_iterator crate_iter = m_connectivity->crates.find(crate_location);
    if (crate_iter == m_connectivity->crates.end()) return NULL;
    return crate_iter->second;
  }

  void PixConnectivityWrapperBase::writeDisable(PixLib::PixDisable &disable) {
    disable.writeConfig(const_cast<PixLib::PixConnectivity *>(m_connectivity));
  }


}
