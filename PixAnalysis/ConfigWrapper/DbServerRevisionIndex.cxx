#include "DbServerRevisionIndex.h"
#include <PixDbServer/PixDbServerInterface.h>

#define DEBUG_TRACE(a) {  }

namespace PixA {

  std::string DbServerRevisionIndex::s_cfgDomainHeader("Configuration-");

  const char *DbServerRevisionIndex::s_typeName[IRevisionIndex::kNTypes] = {
    "PixModule",
    "PixModuleGroup",
    "TimPixTrigController",
    "PixDisable"
  };

  DbServerRevisionIndex::DbServerRevisionIndex(const std::shared_ptr<PixLib::PixDbServerInterface> &db_server)
    : m_dbServer(db_server) 
  {}

  DbServerRevisionIndex::~DbServerRevisionIndex() {}

  void DbServerRevisionIndex::getIdTags(std::vector<std::string> &id_tag_out) {
    if (m_dbServer) {
      std::vector<std::string> temp;
      try {
	m_dbServer->listDomainRem(temp);
	for (std::vector<std::string>::const_iterator tag_iter = temp.begin();
	     tag_iter != temp.end();
	     ++tag_iter) {

	  if (tag_iter->compare(0,cfgDomainHeader().size(),cfgDomainHeader())==0) {
	    if (tag_iter->size()>cfgDomainHeader().size()+1 && (*tag_iter)[ cfgDomainHeader().size() ]=='-') {
	      id_tag_out.push_back(tag_iter->substr(cfgDomainHeader().size()+1,
						    tag_iter->size()-(cfgDomainHeader().size()+1)));
	    }
	  }
	}

      }
      catch( ... ) {
	// @todo selectively catch ?
      }
    }

  }

  void DbServerRevisionIndex::getTags(const std::string &id_tag, std::vector<std::string> &tag_out) {
    if (m_dbServer) {
      try {
	m_dbServer->listTagsRem(makeCfgDomainName(id_tag), tag_out);
      }
      catch(...) {
	// @todo selectively catch ?
      }
    }
  }

  void DbServerRevisionIndex::getRevisions(const std::string &id_tag, const std::string &cfg_tag, const std::string &conn_name, std::set<Revision_t> &revisions_out) {

    if (m_dbServer) {
      std::vector<unsigned int> temp;
      try {
	m_dbServer-> listRevisionsRem(makeCfgDomainName(id_tag), cfg_tag, conn_name, temp);
	for (std::vector<unsigned int>::const_iterator rev_iter = temp.begin();
	     rev_iter != temp.end();
	     ++rev_iter) {
	  revisions_out.insert(*rev_iter);
	}
      }
      catch(...) {
	// @todo selectively catch ?
      }
    }

  }

  void DbServerRevisionIndex::getNames(const std::string &id_tag, const std::string &cfg_tag, EObjectType type, std::set<std::string> &names) {
    if (m_dbServer && type<kNTypes) {
      std::vector<std::string> temp_names;
      m_dbServer-> listObjectsRem(makeCfgDomainName(id_tag), cfg_tag, s_typeName[type], temp_names);     
      for (std::vector<std::string>::const_iterator name_iter = temp_names.begin();
	   name_iter !=temp_names.end();
	   ++name_iter) {
	names.insert(*name_iter);
      }
    }
  }


  /**
   * @param current_upper_revision_limit after scanning there cannot appear new revisions below this boundary.
   */
  void DbServerRevisionIndex::processRevision(const std::string &id_tag,
					      const std::string &cfg_tag,
					      Revision_t target_revision,
					      Revision_t lower_revision_bound,
					      Revision_t current_upper_revision_limit,
					      IRevisionProcessor &processor) {

    if (m_dbServer.get()) {
      try {
	std::vector<std::string> obj_types;
	// all types which make up a module
	obj_types.push_back("PixModule");
	// all types which make up a ROD
	obj_types.push_back("PixModuleGroup");
	obj_types.push_back("RodPixController");
	obj_types.push_back("PixBoc");
	obj_types.push_back("PixDisable");

	std::vector<std::string> objects;
	std::vector<Revision_t> revisions;

	std::map< std::string, std::pair<Revision_t,Revision_t> > range;

	for (std::vector<std::string>::const_iterator type_iter = obj_types.begin();
	     type_iter != obj_types.end();
	     ++type_iter) {

	  objects.clear();
	  m_dbServer->listObjectsRem( makeCfgDomainName(id_tag), cfg_tag, *type_iter, objects);
	  for (std::vector<std::string>::const_iterator obj_iter = objects.begin();
	       obj_iter != objects.end();
	       ++obj_iter) {

	    // remove ROD and BOC trailer from object name
	    std::string obj_name;
	    std::string::size_type pos = obj_iter->find("_BOC");
	    if (pos == std::string::npos || pos+4 != obj_iter->size()) {
	      pos = obj_iter->find("_ROD");
	    }
	    if (pos != std::string::npos && pos+4 == obj_iter->size()) {
	      obj_name =obj_iter->substr(0,pos);
	    }

	    // debug 
	    // if (*obj_iter != "M513098") continue;

	    revisions.clear();
	    m_dbServer->listRevisionsRem( makeCfgDomainName(id_tag), cfg_tag, *obj_iter, revisions );
	    if (!revisions.empty()) {

	      //	    RevisionList_t &revision_list= object_list[ *obj_iter ];
	      std::pair<Revision_t, Revision_t> a_range;
	      a_range.first = 0;
	      a_range.second = REV_MAX;

	      for (std::vector<Revision_t>::const_iterator revision_iter = revisions.begin();
		   revision_iter != revisions.end();
		   ++revision_iter) {

		if (*revision_iter>=lower_revision_bound && *revision_iter < current_upper_revision_limit) {
		  if (*revision_iter <= target_revision) {
		    if (a_range.first< *revision_iter ) {
		      a_range.first = *revision_iter;
		    }
		  }
		  else if (a_range.second> *revision_iter) {
		    a_range.second = *revision_iter;
		  }
		}
	      }
	      if (a_range.first>0) {

		DEBUG_TRACE( std::cout << "INFO [DbServerRevisionIndex::processRevision] " << *obj_iter << " : "
			     <<  a_range.first  << " - "
			     << ( a_range.second <= current_upper_revision_limit ?  a_range.second : REV_MAX)
			     << std::endl )

		processor.process(*obj_iter,
				  "",
				  a_range.first,
				  a_range.second,
				  0);

	      }
	    }
	  }
	}
      }
      catch(...) {
	std::cout << "ERROR [PixConnObjConfigDbManager::updateRevisions] Caught exveption." << std::endl;
      }
    }

  }


}
