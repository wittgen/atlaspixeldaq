#ifndef _pixa_exception_h_
#define _pixa_exception_h_

#include <BaseException.h>
namespace PixA {

  typedef SctPixelRod::BaseException BaseException;

  class  ConfigException : public BaseException
  {
  public:
    ConfigException(const std::string &name) : BaseException(name) {};

  };

  class  ConfigTypeMismatch : public ConfigException
  {
  public:
    ConfigTypeMismatch() : ConfigException("Config value does not mismatch type.") {};

  };


}
#endif
