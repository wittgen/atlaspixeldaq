#include "ConfigContainer.h"
#include "ConfGrpContainer.h"


namespace PixA {

  ObjStat_t ConfigContainer::s_stat("ConfigContainer");

  ConfigContainer::~ConfigContainer() {
    //    std::cout <<"INFO [ConfigContainer::dtor] delete on demand config for config \"" 
    //    	      << (m_config ? m_config->m_confName : std::string("NULL")) << "\" / \""
    //    	      << (m_config ? m_config->m_confType : std::string("NULL")) << "\"."
    //    	      << std::endl;

    // clean up ConfCroups
    for(ConfGrpList_t::iterator group_iter=m_group.begin();
	group_iter != m_group.end();
	group_iter++) {
      delete group_iter->second;
    }

    // cleanup sub configs
    for(ConfigList_t::iterator config_iter=m_subConfigs.begin();
	config_iter != m_subConfigs.end();
	config_iter++) {
      delete config_iter->second;
    }

    if (isRoot()) {
      //      std::cout << "INFO [ConfigContainer::dtor] " << static_cast<void *>(this) << "(config=" 
      //      		<< static_cast<void *>(m_config) << ") delete real config object ." << std::endl;
      delete m_config;
    }

    s_stat.dtor();
  }

  /** Return a reference to the config group of the given name.
   * If the config group does not yet exist in the cache. A new
   * config group interface is created. The latter will open the
   * database if the database is not yet opened.
   */
  IConfGrp &ConfigContainer::operator[](const std::string &name)  
  {
    ConfGrpList_t::iterator conf_group_iter = m_group.find( name );
    if (conf_group_iter == m_group.end()) {

      std::string message;
      message = "FATAL [ConfigContainer::operator[]] No config group named ";
      message += name;
      message +=" in ";
      message += m_config->m_confName;
      message += ".";

      throw ConfigException(message);
    }
    return *(conf_group_iter->second);
  }

  /** Return a reference to the sub config of the given name.
   * If the sub config does not yet exist in the cache. A new
   * config group interface is created. The latter will open the
   * database if the database is not yet opened.
   */
  IConfig &ConfigContainer::subConfig(const std::string &name)  {
    ConfigList_t::iterator config_iter = m_subConfigs.find( name );
    if (config_iter == m_subConfigs.end()) {

      std::string message;
      message = "FATAL [ConfigContainer::operator[]] No sub config named ";
      message += name;
      message +=" in ";
      message += m_config->m_confName;
      message += ".";

      throw ConfigException(message);
    }
    return *(config_iter->second);
  }

  IConfig &ConfigContainer::createAll() {
    assert(m_isComplete);
    return *this;
  }


}
