#ifndef _OnDemandConfig_h_
#define _OnDemandConfig_h_

#include <map>
#include <string>

#include "PixDbAccessor.h"
#include "IConfig.h"
#include "IConfGrp.h"
#include "DbWrapper.h"
#include <Config/Config.h>
#include "ConfigContainer.h"

#include "ObjStat_t.h"

namespace PixA {

  class OnDemandConfGrp;
  class RootDbConfigHelper;

  class OnDemandConfig : public IConfig, public PixDbAccessor
  {
    friend class PixConnectivityWrapper;
    friend class DbManager;
    friend class DbConfigHandle;
    friend class OnDemandConfGrp;
    friend class RootDbConfigHelper;

    template <class T>
    class ConfigPtr {
    public:
      ConfigPtr(T *ptr) : m_ptr(ptr) {
	m_ptr->IConfigBase::useNode();
      }
      ConfigPtr(ConfigPtr &a) : m_ptr(a.m_ptr) {
	m_ptr->IConfigBase::useNode();
      }
      ~ConfigPtr()
      {
	m_ptr->IConfigBase::doneWithNode();
      }

      ConfigPtr &operator = (ConfigPtr &b) {
	if (b->m_ptr != m_ptr) {
	  m_ptr->IConfigBase::doneWithNode();
	  m_ptr = b->m_ptr;
	  m_ptr->IConfigBase::useNode();
	}
	return this;
      }

      T &operator *() { return *m_ptr; }
      T *operator->() { return  m_ptr; }

    private:
      T *m_ptr;
    };


  protected:
    typedef std::map<std::string, OnDemandConfGrp * > ConfGrpList_t;
    typedef std::map<std::string, OnDemandConfig * > ConfigList_t;

    //  OnDemandConfig(ConfigRoot *root, DbWrapper &db) : IConfig(root), PixDbAccessor(db),m_config(root()->config()) { s_stat.ctor();}

    /** Bootstrap a config hierarchy.
     */
    OnDemandConfig(const PixDbAccessor &accessor) : PixDbAccessor(accessor), m_configContainer(new ConfigContainer)
      {//std::cout<<"starting Ondemandconfig"<<std::endl;
          s_stat.ctor();
	updateConfigName(); 
//         std::cout <<"INFO [OnDemandConfig::ctor(const PixDbAccessor &)] new config \""  << m_config->m_confName << "\" / \""
// 		  << m_config->m_confType << "\""
// 		  << std::endl;
          //std::cout<<"ending Ondemandconfig"<<std::endl;
      }

    /** Bootstrap a config hierarchy.
     */
    OnDemandConfig(DbWrapper &db, const std::vector<std::string> &record_hierarchy) 
      : PixDbAccessor(db, record_hierarchy), m_configContainer(new ConfigContainer)  { s_stat.ctor();  
//         std::cout <<"INFO [OnDemandConfig::ctor(DbWrapper &, const std::vector<std::string> &)] new config \""  
// 		  << m_config->m_confName << "\" / \""
// 		  << m_config->m_confType << "\""
// 		  << " from records \"";
// 	for (std::vector<std::string>::const_iterator iter=record_hierarchy.begin();
// 	     iter != record_hierarchy.end();
// 	     iter++) {
// 	  std::cout << *iter << " / ";
// 	}
// 	std::cout << "\"" << std::endl;
    }

    /** Bootstrap a config hierarchy.
     */
    OnDemandConfig(DbWrapper &db, const std::string &top_level_record_name) 
      : PixDbAccessor(db, top_level_record_name),m_configContainer(new ConfigContainer(top_level_record_name, className()) )
      { s_stat.ctor();
//         std::cout <<"INFO [OnDemandConfig::ctor(DbWrapper &, const std::string &)] new config \""
// 		  << m_config->m_confName << "\" / \""
// 		  << m_config->m_confType << "\""
// 		  << " from record \"" << top_level_record_name << "\"."
// 		  << std::endl;
      }

    /** Bootstrap a config hierarchy.
     */
    OnDemandConfig(DbWrapper &db, PixLib::dbRecordIterator &record_iter) 
      : PixDbAccessor(db, record_iter), m_configContainer(new ConfigContainer(name(),className()) ) {s_stat.ctor();
//         std::cout <<"INFO [OnDemandConfig::ctor(DbWrapper &, PixLib::dbRecordIterator &)] new config \""  
// 		  << m_config->m_confName << "\" / \""
// 		  << m_config->m_confType << "\""
// 		  << std::endl;
    }

    // /** Bootstrap a config hierarchy.
    //  * ConfigRoot may not use the this object, since the object is not yet fully created.
    //  */
    // OnDemandConfig(DbWrapper &db, const std::string &record_name) : PixDbAccessor(db, record_name) {}

    /** Create a sub config specified by the name and the record iterator from an existing config object.
     * @param config a reference of the config which contains the subconfig specified by the name end the iterator
     * @param record_iter a valid record iterator which points to the record of the given name
     * @param name the name of the record
     * the record_iter and name must much. Otherwise the result will be undefined.
     */
    OnDemandConfig(OnDemandConfig &config, PixLib::dbRecordIterator &record_iter, const std::string &name)
      : IConfig(config), PixDbAccessor(config, record_iter, name), m_configContainer(new ConfigContainer(*(config.m_configContainer),name, className() ) ){s_stat.ctor();
      }

//     /** Add and return the real config group object.
//      */
//     PixLib::ConfGroup &addGroup(const std::string &name) {
//       return m_configContainer->addGroup(this, name);
//     }


  public:
    /** Delete all subConfigs and 
     */
    ~OnDemandConfig();

    /** Return a reference to the config group of the given name.
     * If the config group does not yet exist in the cache. A new
     * config group interface is created. The latter will open the
     * database if the database is not yet opened.
     */
    IConfGrp &operator[](const std::string &name) ;

    /** Return a reference to the sub config of the given name.
     * If the sub config does not yet exist in the cache. A new
     * config group interface is created. The latter will open the
     * database if the database is not yet opened.
     */
    IConfig &subConfig(const std::string &name) ;

    /** Create the whole config and return a reference to it.
     */
    PixLib::Config &config() {createAll(); return *(m_configContainer->m_config);}

    void use()  { PixDbAccessor::accessStart(); IConfigBase::useNode(); }
    void done() { PixDbAccessor::accessEnd();   IConfigBase::doneWithNode(); };

    IConfig &createAll();

  private:
    void updateConfigName() {
      if (m_configContainer->m_config->m_confName.empty() && isDbInUse()) {
	m_configContainer->updateConfigName( name(), className() );
      }
    }

    ConfGrpList_t   m_group;
    ConfigList_t    m_subConfigs;
    ConfigPtr<ConfigContainer>  m_configContainer;
    static ObjStat_t s_stat;
  };


}

#endif


