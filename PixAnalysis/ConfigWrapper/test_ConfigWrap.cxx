#include "DbManager.h"
#include <RootDb/RootDb.h>
#include "ConfGrpRefUtil.h"
#include "Connectivity.h"
#include "ConnectivityManager.h"

int main(int argc, char ** argv)
{
  if (argc<=1) return -1;

  std::shared_ptr<PixLib::PixDbInterface> db(new PixLib::RootDb(argv[1],"READ"));
  std::shared_ptr<PixLib::DbRecord> root_record(db->readRootRecord());
  if (argc<=2) {
    for (PixLib::dbRecordIterator iter = root_record->recordBegin(); iter != root_record->recordEnd(); iter++) {
      std::shared_ptr<PixLib::DbRecord> a_record(*(db->DbProcess(iter, PixLib::PixDb::DBREAD)));
      if(a_record->getClassName()==("PixScanResult")){
	std::cout << a_record->getName() << std::endl;
      }
    }
    return -1;
  }

  PixLib::dbRecordIterator iter1 = root_record->findRecord(argv[2]);
  if (iter1 == root_record->recordEnd()) return -1;

  std::shared_ptr<PixLib::DbRecord> result_record(*(db->DbProcess(iter1, PixLib::PixDb::DBREAD)));

  try { 
  for (PixLib::dbRecordIterator iter2 = result_record->recordBegin(); iter2 != result_record->recordEnd(); iter2++) {
    std::shared_ptr<PixLib::DbRecord> group_record(*(db->DbProcess(iter2, PixLib::PixDb::DBREAD)));
    if(group_record->getClassName()==("PixModuleGroup")){
      std::cout << group_record->getName() << std::endl;

      for (PixLib::dbRecordIterator group_iter = group_record->recordBegin(); group_iter != group_record->recordEnd(); group_iter++) {
	std::shared_ptr<PixLib::DbRecord> a_record(*(db->DbProcess(group_iter, PixLib::PixDb::DBREAD)));
	if(a_record->getClassName()==("PixScanData")){
	  std::cout << a_record->getName() << std::endl;
	  
	  PixLib::dbRecordIterator scan_info_iter = a_record->findRecord("ScanInfo");
	  if (scan_info_iter != a_record->recordEnd()) {
	    
	    PixA::ConfigHandle scan_info_handle(PixA::DbManager::wrapConfigRecord(db.get(),scan_info_iter));
	    PixA::ConfigRef  scan_info( scan_info_handle.ref() );
	    std::cout << " conn  = " << confVal<std::string>(scan_info["Tags"]["Conn"]) 
		      << " alias = " << confVal<std::string>(scan_info["Tags"]["Alias"]) 
		      << " data  = " << confVal<std::string>(scan_info["Tags"]["Data"]) 
		      << " cfg   = " << confVal<std::string>(scan_info["Tags"]["Cfg"]) 
		      << " cfg Rev.  = " << confVal<unsigned int>(scan_info["Tags"]["CfgRev"]) 
		      << std::endl;


	      PixA::ConnectivityRef conn(PixA::ConnectivityManager::getConnectivity( confVal<std::string>(scan_info["Tags"]["Conn"]),
										     confVal<std::string>(scan_info["Tags"]["Alias"]),
										     confVal<std::string>(scan_info["Tags"]["Data"]),
										     confVal<std::string>(scan_info["Tags"]["Cfg"]), 
										     confVal<unsigned int>(scan_info["Tags"]["CfgRev"]) ));

	      //conn will wrap a pointer to IConnectivity
	      // IConnectivity will contain a PixConnectivity and will cache config objects.
	      // The connectivity will be a shared property. If there are no more users, the 
	      // connectivity will be deleted eventually.

	      const PixA::Pp0List &pp0s=conn.pp0s(group_record->getName());
	      // read only
	      for (PixA::Pp0List::const_iterator pp0_iter=pp0s.begin();
		   pp0_iter != pp0s.end(); // may be slightly inefficient unless the compiler is smart enough.
		   ++pp0_iter) {

		// it may be more efficient to call modulesBegin and modulesEnd only once:
		PixA::ModuleList::const_iterator modules_begin=PixA::modulesBegin(pp0_iter);
		PixA::ModuleList::const_iterator modules_end=PixA::modulesEnd(pp0_iter);

		for (PixA::ModuleList::const_iterator module_iter=modules_begin;
		     module_iter != modules_end;
		     ++module_iter) {

		  // Do not consider modules without histograms
		  std::cout << " INFO [main] Process module  " << const_cast<PixLib::ModuleConnectivity *>(*module_iter)->name() << "." << std::endl;

		  PixA::ConfigHandle module_conf_handle( conn.modConf(module_iter) );
		  PixA::ConfigRef module_conf( module_conf_handle.ref() );
		  if (confVal<std::string>(module_conf["geometry"]["Type"]) != std::string("unkown")) {
		    int ass_SN = confVal<int>(module_conf["geometry"]["staveID"]);
		    std::cout << " INFO [main] " << const_cast<PixLib::ModuleConnectivity *>(*module_iter)->name() << " on stave = " << ass_SN << "." << std::endl;
		  }

		  std::cout << " INFO [main] " << const_cast<PixLib::ModuleConnectivity *>(*module_iter)->name() << " prod id = " 
			    << const_cast<PixLib::ModuleConnectivity *>(*module_iter)->prodId() 
			    << "." << std::endl;
		}
	      }
	  }
	}
      }
    }
  }
  }
  catch (PixA::ConfigException &err) {
    std::cout << "FATAL [main] exception : " << err << std::endl;
  }

}
