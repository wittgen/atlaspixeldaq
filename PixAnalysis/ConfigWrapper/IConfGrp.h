#ifndef _IConfGrp_h_
#define _IConfGrp_h_

#include "IConfig.h"
#include "pixa_exception.h"
#include <string>

namespace PixLib {
  class ConfObj;
}

namespace PixA {

  class IConfGrp : public IConfigBase
  {
  protected:
    IConfGrp(ConfigRoot &root) : IConfigBase(root) {}
    IConfGrp(IConfGrp &base) : IConfigBase(base) {}
    IConfGrp(IConfig &base) : IConfigBase(base) {}

  public:
    virtual ~IConfGrp() {}

    virtual void use()  = 0;
    virtual void done() = 0;

    virtual PixLib::ConfObj &operator[](const std::string &name)  = 0;

    virtual void createAll() = 0;
    //  virtual ConfObj &operator[](const std::string &name) throw (std::runtime_error) = 0;
  };

}

#endif
