/* Dear emacs, this is -*-c++-*- */
#ifndef _PixA_PixConfigWrapperTopLevel_h_
#define _PixA_PixConfigWrapperTopLevel_h_

#include "PixConfigWrapper.h"

namespace PixA {

  template <class T>
  class PixConfigWrapperTopLevel : public PixConfigWrapper
  {
  public:
    PixConfigWrapperTopLevel(const std::shared_ptr<T> &obj, PixLib::Config &config) : PixConfigWrapper(config), m_obj(obj) {}

    PixConfigWrapperTopLevel(const std::shared_ptr<T> &obj) : PixConfigWrapper(obj->config()), m_obj(obj) {}
    ~PixConfigWrapperTopLevel() {}

  private:
    std::shared_ptr<T> m_obj; /**< Pointer to the object which provides th config to keep the object alive 
				     while the config is still being used.*/
  };

}
#endif
