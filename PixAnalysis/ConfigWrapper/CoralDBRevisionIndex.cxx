#include "CoralDBRevisionIndex.h"
#include <CoralDB/Connection.h>
#include "pixa_exception.h"
#include <CoralBase/Exception.h>

#include "Regex_t.h"

#include <memory>
#include <cassert>

#define DEBUG_TRACE(a) { }

namespace PixA {


  typedef std::shared_ptr<CoralDB::CoralDB> CoralDBInstance;

  const char *CoralDBRevisionIndex::s_typeName[kNTypes]={
    "MODULE_CFG",
    "RODBOC_CFG",
    "TIM_CFG",
    "DISABLE_CFG"
  };
  

  class CoralDBTransactionGuard
  {
  public:

    CoralDBTransactionGuard( CoralDB::CoralDB *db) 
      : m_db(db),
	m_success(false)
    { assert(m_db.get()); m_db->transactionStart(); }

    ~CoralDBTransactionGuard() {
      if (m_success) {
	m_db->transactionCommit();
      }
      else {
	m_db->transactionRollBack();
      }
    }

    CoralDB::CoralDB *operator->() { return m_db.get(); }

    void success() {
      m_success=true;
    }

  private:
    std::shared_ptr<CoralDB::CoralDB> m_db;
    bool m_success;
  };

  std::vector<Regex_t *> CoralDBRevisionIndex::s_patternList;
  std::map<std::string, std::string> CoralDBRevisionIndex::s_typeTranslation;

  std::string CoralDBRevisionIndex::s_cfgIdTagTrailer("-CFG");
  std::string CoralDBRevisionIndex::s_cfgTypeTail("_CFG");
  std::string CoralDBRevisionIndex::s_rootId("ROOT");
  std::string CoralDBRevisionIndex::s_dstSlotName("UP");

  // for debugging
  std::string CoralDBmakeTimeString(time_t a_time)
  {
    std::string a_time_string = ctime(&a_time);
    std::string::size_type ret_pos = a_time_string.find("\r");
    std::string::size_type new_line_pos = a_time_string.find("\n");
    if (new_line_pos == std::string::npos || (new_line_pos>ret_pos && ret_pos != std::string::npos)) {
      new_line_pos =ret_pos;
    }
    if (new_line_pos != std::string::npos ) {
      a_time_string.erase(new_line_pos, a_time_string.size()-new_line_pos);
    }
    return a_time_string;
  }


  CoralDBRevisionIndex::CoralDBRevisionIndex(const std::string &connection_string) : m_connectionString(connection_string), m_cacheUpdateTime(0) 
  {
    if (s_patternList.empty()) {
      s_patternList.push_back(new Regex_t("ROD_.*"));
      s_patternList.push_back(new Regex_t("M5[[:digit:]]+"));
      s_patternList.push_back(new Regex_t("[BCDF][[:digit:]]+"));
      s_patternList.push_back(new Regex_t("TIM_.*"));
      s_patternList.push_back(new Regex_t("Disable-.*"));
    }
    if (s_typeTranslation.empty()) {
      s_typeTranslation.insert(std::make_pair("PixModule","MODULE"));
      s_typeTranslation.insert(std::make_pair("PixModuleGroup","RODBOC"));
      s_typeTranslation.insert(std::make_pair("PixTim","TIM"));
      s_typeTranslation.insert(std::make_pair("PixDisable","DISABLE"));
    }
  }

  CoralDBRevisionIndex::~CoralDBRevisionIndex() {}

  CoralDB::CoralDB *CoralDBRevisionIndex::createCoralDbInstance(const std::string &id_tag, const std::string &cfg_tag, bool read_only) {
    std::unique_ptr<CoralDB::CoralDB> coral_db(new CoralDB::CoralDB(m_connectionString,
								  (read_only ? coral::ReadOnly : coral::Update), 
								  coral::Warning,
								  CoralDB::CoralDB::MANUAL_COMMIT));
    if (!id_tag.empty()) {
      coral_db->setObjectDictionaryTag( makeCfgIdTag(id_tag) );
    }
    if (!cfg_tag.empty()) {
      coral_db->setConnectivityTag( cfg_tag );
    }
    return coral_db.release();
  }

  void CoralDBRevisionIndex::getIdTags(std::vector<std::string> &id_tag_out) {

    std::string error_message;
    try {
      CoralDBInstance db( coralDbInstance() );

      CoralDB::IdTagList id_tags;
      db->getExistingObjectDictionaryTags(id_tags);
      id_tag_out.reserve(id_tags.size()+id_tag_out.size());
      for (CoralDB::IdTagList::const_iterator iter = id_tags.begin();
	   iter != id_tags.end();
	   iter++) {
	if (iter->tag().size()>4 && iter->tag().compare(iter->tag().size()-cfgIdTagTrailer().size(),
							cfgIdTagTrailer().size(),
							cfgIdTagTrailer())==0) {
	  id_tag_out.push_back( iter->tag() );
	}
      }

    }
    catch ( coral::Exception& e ) {
      error_message = "CORAL exception : ";
      error_message += e.what();
    }
    catch ( std::exception& e ) {
      error_message = "Std exception : ";
      error_message += e.what();
    }
    catch ( ... ) {
      error_message = "Unknown exception.";
    }
    if (!error_message.empty()) {
      throw PixA::ConfigException(std::string("ERROR [CoralDBRevisionIndex::listIdTags] CoralDb exception : ")+error_message);
    }

  }

  void CoralDBRevisionIndex::getTags(const std::string &id_tag, std::vector<std::string> &tag_out) {
    std::string error_message;
    try {
      //      CoralDB::CoralDB db(m_cfgIndexDb, coral::ReadOnly, coral::Warning, CoralDB::CoralDB::AUTOCOMMIT);
      CoralDBInstance db( coralDbInstance() );

      CoralDB::TagList tags;
      db->getExistingConnectivityTags(tags,makeCfgIdTag(id_tag));
      tag_out.reserve(tags.size()+tag_out.size());
      for (CoralDB::TagList::const_iterator iter = tags.begin();
	   iter != tags.end();
	   iter++) {
	tag_out.push_back(iter->tag());
      }
    }
    catch ( coral::Exception& e ) {
      error_message = "CORAL exception : ";
      error_message += e.what();
    }
    catch ( std::exception& e ) {
      error_message = "Std exception : ";
      error_message += e.what();
    }
    catch ( ... ) {
      error_message = "Unknown exception.";
    }
    if (!error_message.empty()) {
      throw PixA::ConfigException(std::string("ERROR [CoralDBRevisionIndex::listIdTags] CoralDb exception : ")+error_message);
    }

  }

  void CoralDBRevisionIndex::getRevisions(const std::string &id_tag,
					  const std::string &cfg_tag,
					  const std::string &conn_name,
					  std::set<Revision_t> &revisions_out)
  {
    //      CoralDB::CoralDB db(m_cfgIndexDb, coral::ReadOnly, coral::Warning, CoralDB::CoralDB::AUTOCOMMIT);
    CoralDBInstance db( coralDbInstance( id_tag, cfg_tag ) );

    CoralDB::ConnectionTableMap  conn_table;
    std::vector<CoralDB::Connection> revisions = db->findAllConnections(conn_name,true);

    for(std::vector<CoralDB::Connection>::const_iterator rev_iter = revisions.begin();
	rev_iter != revisions.end();
	rev_iter++) {

      try {

	//	if (isConnObject(rev_iter->fromId())) {
	  ExtRevision_t ext_revision = extractRevision( rev_iter->fromId(), rev_iter->fromSlot() );
	  revisions_out.insert(ext_revision.revision() );
	  //	}
      }
      catch (PixA::ConfigException &) {
      }
    }
  }

  void CoralDBRevisionIndex::getNames(const std::string &id_tag, const std::string &cfg_tag, EObjectType type, std::set<std::string> &names) {

    // Getting the objects names of a given cfg_tag is unsustainably slow.
    // Getting the all objects of a given id tag is much faster (although more data gets returned (?))
    // So to arrive at an usable solution.
    // the object tag is only updated every 60min or the id tag changes.

    ::time_t current_time(::time(0));
    if (id_tag != m_cachedIdTag || current_time - m_cacheUpdateTime  > 60*60) {
    CoralDBInstance db( coralDbInstance( id_tag, cfg_tag ) );
    //    db->setMsgLevel(coral::Verbose);
    CoralDB::CompoundTag compound_tag( makeCfgIdTag(id_tag) ,"" /* should only get the objects of the given cfg_tag but that is very slow*/, "","");

    m_cacheUpdateTime=0;
    db->getObjectDictionary(m_objectDictionaryCache, compound_tag);
    m_cacheUpdateTime=current_time;
    m_cachedIdTag=id_tag;
    }

    for(CoralDB::ObjectDictionaryMap::const_iterator dictionary_iter = m_objectDictionaryCache.begin();
	dictionary_iter != m_objectDictionaryCache.end();
	++dictionary_iter) {
      if (dictionary_iter->second == s_typeName[type]) {
	names.insert( dictionary_iter->first );
      }
    }
    
  }


  ExtRevision_t CoralDBRevisionIndex::extractRevision( const std::string &id_name, const std::string &file_name) {
    std::string::size_type pos = file_name.rfind("_");
    if (   pos == std::string::npos
	   || pos < id_name.size()+1
	   || pos+1>file_name.size()
	   || file_name.compare(pos-id_name.size(),id_name.size(),id_name)!=0
	   || !isdigit(file_name[pos+1])) {
      throw ConfigException("File name does not contain revision number!");
    }
    return ExtRevision_t(atoi(&(file_name.substr(pos+3,10).c_str()[0])), atoi(&(file_name.substr(pos+13,3).c_str()[0])));
  }


  bool CoralDBRevisionIndex::isConnObject(const std::string &id_name) {
    for (std::vector<Regex_t*>::const_iterator pattern_iter = s_patternList.begin();
	 pattern_iter != s_patternList.end();
	 ++pattern_iter) {
      if ((*pattern_iter)->match( id_name )) {
	return true;
      }
    }
    return false;
  }


  class LowerBoundRevision_t
  {
  public:
    LowerBoundRevision_t() : m_revision(0) {}

    void setPath(const std::string &path) { m_path = path; }
    void setRevision(const ExtRevision_t &revision) { m_revision = revision; }

    Revision_t revision() const { return m_revision.revision(); }
    const ExtRevision_t &extRevision() const { return m_revision; }
    unsigned short representation() const { return m_revision.representation(); }
    const std::string &path() const { return m_path; }
  private:
    ExtRevision_t m_revision;
    std::string m_path;
  };

  /**
   * @param current_upper_revision_limit after scanning there cannot appear new revisions below this boundary.
   */
  void CoralDBRevisionIndex::processRevision(const std::string &id_tag,
					     const std::string &cfg_tag,
					     Revision_t target_revision,
					     Revision_t lower_revision_bound,
					     Revision_t current_upper_revision_limit,
					     IRevisionProcessor &processor) {

    CoralDBInstance db( coralDbInstance(id_tag, cfg_tag) );
    //    CoralDB::CoralDB db(m_cfgIndexDb, coral::ReadOnly, coral::Warning, CoralDB::CoralDB::AUTOCOMMIT);

    CoralDB::ConnectionTableMap  conn_table;
    db->getConnectionTable(conn_table);

    // if no valid bounds are given just take the revisions of the precedent and following weeks.
    // if (lower_revision_bound==0) {
    //   Revision_t delta = 24*5*3600;

    //   if (target_revision>delta) {
    // 	lower_revision_bound = target_revision-delta;
    //   }
    //   if (target_revision+delta <current_upper_revision_limit) {
    // 	current_upper_revision_limit=target_revision+delta;
    //   }

    // }

    //     Revision_t current_time = time(0);
    //     Revision_t min_revision=target_revision-3600/4 /* *24*5 */;
    //     Revision_t max_revision=target_revision+3600/4 /* *24*5 */;
    //     if (max_revision>current_time) {
    //       max_revision = current_time;
    //    }

    //    Revision_t hard_cut_min_revision=target_revision-3600/2+20*60 /* *24*5 */;
    //    Revision_t hard_cut_max_revision=target_revision+3600/2+20*60 /* *24*5 */;
    //    Revision_t hard_cut_min_revision=0 /* *24*5 */;
    //    Revision_t hard_cut_max_revision=current_upper_revision_limit /* *24*5 */;

    std::map< std::string, std::pair< std::pair<LowerBoundRevision_t,LowerBoundRevision_t>, std::map<ExtRevision_t, std::string> > > range_map;

    DEBUG_TRACE( std::cout << "INFO [CoralDBRevisionIndex::processRevision] target_revision = " << target_revision << " limits:  " 
		 << lower_revision_bound << " - " << current_upper_revision_limit
			       << std::endl )

    //@todo is it useful to cache a revision range ?
    for(CoralDB::ConnectionTableMap::const_iterator conn_iter = conn_table.begin();
	conn_iter != conn_table.end();
	conn_iter++) {

      // debug
      // if (conn_iter->fromId() != "HVOFF"
      // 	  && conn_iter->fromId() != "LOCKEDOFF" 
      // 	  && conn_iter->fromId() != "BADDIGITAL"
      // 	  && conn_iter->fromId() != "HARDWARE_PROBLEMS" ) continue;
      // if (conn_iter->fromId() != "M510929") continue;
      DEBUG_TRACE( std::cout << "INFO [CoralDBRevisionIndex::processRevision] " << conn_iter->fromId() << " : " << conn_iter->fromSlot() << std::endl )

      try {

	//	if (isConnObject(conn_iter->fromId())) {
	  ExtRevision_t ext_revision = extractRevision( conn_iter->fromId(), conn_iter->fromSlot() );
	  Revision_t revision = ext_revision.revision();

	  DEBUG_TRACE( std::cout << "INFO [CoralDBRevisionIndex::processRevision] " << conn_iter->fromId() << " is conn obj." 
		       << lower_revision_bound << " < " << revision << " < " << current_upper_revision_limit
		       << " (" << CoralDBmakeTimeString(revision) << ")"
		       << std::endl )

	    //	  if (revision < current_upper_revision_limit && revision>=lower_revision_bound || a_range) {
	    {

	    std::pair< std::pair<LowerBoundRevision_t,LowerBoundRevision_t>, std::map<ExtRevision_t, std::string> > &conn_list = range_map[ conn_iter->fromId() ];
	    std::pair<LowerBoundRevision_t,LowerBoundRevision_t> &a_range = conn_list.first;
	    if (revision >= lower_revision_bound && revision<current_upper_revision_limit) {
	      conn_list.second.insert(std::make_pair(ext_revision, conn_iter->fromSlot()));
	    }

	    if (revision <= target_revision) {
	      if (revision>=a_range.first.revision() ) {
		if (revision > a_range.first.revision() || a_range.first.representation()<ext_revision.representation()) {

		  DEBUG_TRACE( std::cout << "INFO [CoralDBRevisionIndex::processRevision] updated " << conn_iter->fromId() << " : " 
			       << ext_revision.revision() << ";" << ext_revision.representation()
			       << std::endl )

		  a_range.first.setRevision(ext_revision);
		  a_range.first.setPath( conn_iter->fromSlot() );
		}
	      }
	    }
	    else {

	      if (   (a_range.second.revision()>current_upper_revision_limit)
		  && (revision<=a_range.second.revision() || a_range.second.revision()==0)) {

		  DEBUG_TRACE( std::cout << "INFO [CoralDBRevisionIndex::processRevision] updated " << conn_iter->fromId() << " : " 
			       << ext_revision.revision() << ";" << ext_revision.representation()
			       << std::endl )

		  a_range.second.setRevision(ext_revision);
	      }
	    }
	    //	  break;
	  }
	  //	}

      }
      catch( PixA::ConfigException &) {
	// catch errors which may occure in "extractRevision" for e.g. the ROOT object
      }

    }

    for (auto range_iter = range_map.begin(); range_iter != range_map.end(); ++range_iter) {

	const std::pair<LowerBoundRevision_t,LowerBoundRevision_t> &range = range_iter->second.first;
	std::map<ExtRevision_t,std::string> &revision_list=range_iter->second.second;

	DEBUG_TRACE( std::cout << "INFO [CoralDBRevisionIndex::processRevision] " << range_iter->first << " : "
		     <<  range.first.revision()  << " - "
		     << ( range.second.revision() <= current_upper_revision_limit ?  range.second.revision() : REV_MAX)
		     << " -> " << range.first.path() << " ; " << range.first.representation()
		     << std::endl )

      if (range_iter->second.first.first.revision()>0 ) {
	revision_list.insert(std::make_pair(range.first.revision(),range.first.path()));
	revision_list.insert(std::make_pair((range.second.revision()>0? range.second.revision() : REV_MAX),""));


	std::map<ExtRevision_t,std::string>::const_iterator first_iter = revision_list.begin();
	for (std::map<ExtRevision_t,std::string>::const_iterator rev_iter = first_iter;
	     rev_iter != revision_list.end();
	     ++rev_iter ) {

	  if (first_iter != rev_iter) {
	    if (first_iter->first.revision() < rev_iter->first.revision()) {
	      // ignore old representations

	      DEBUG_TRACE( std::cout << "INFO [CoralDBRevisionIndex::processRevision] " << range_iter->first << " : "
			   <<  first_iter->first.revision()  << " - "
			   << ( rev_iter->first.revision() <= current_upper_revision_limit ?  rev_iter->first.revision() : REV_MAX)
			   << " -> " << first_iter->second << " ; " << first_iter->first.representation()
			   << std::endl )

		processor.process(range_iter->first,
				  first_iter->second,
				  first_iter->first.revision(),
				  ( rev_iter->first.revision() <= current_upper_revision_limit && rev_iter->first.revision()>0  
				    ?  rev_iter->first.revision() : REV_MAX),
				  first_iter->first.representation());
	    }
	  }
	  first_iter = rev_iter;
	}
      }
    }

  }


  bool CoralDBRevisionIndex::addRevision(const std::string &id_tag,
					 const std::string &cfg_tag,
					 const std::string &type_name,
					 const std::string &name,
					 bool is_in_coral,
					 Revision_t /*revision*/,
					 const std::string &data) {
    try {
      CoralDBTransactionGuard  coral_transaction( coralDbTransaction(id_tag, cfg_tag) );

      if(!is_in_coral) {

	std::map<std::string,std::string>::const_iterator type_iter = s_typeTranslation.find(type_name);
	if (type_iter ==s_typeTranslation.end()) {
	  std::stringstream message;
	  message << "FATAL [CoralDBRevisionIndex::addRevision] Unknown type " << type_name << " for " << name
		  << ". Cannot add unknown types to object dictionary."
		  << std::endl;
	  throw PixA::ConfigException(message.str());
	}

	try {
	  coral_transaction->checkAndInsertObject(name, type_name+s_cfgTypeTail);
	}
	catch ( std::runtime_error &err ) {
	  std::cerr  << "ERROR [CoralDBRevisionIndex::addRevision] Exception while trying to insert object " << name 
		     << " : " << err.what()
		     << std::endl;
	}
      }
      coral_transaction->insertConnection(s_rootId, name, name, s_dstSlotName );

      try {
	coral_transaction->checkAndInsertObject(data, name);
      }
      catch ( std::runtime_error &err ) {
	std::stringstream message;
	message << "FATAL [CoralDBRevisionIndex::addRevision] Exception while trying to add revision " << data
		<< " for " << name  << " : " << err.what()
		<< std::endl;
	throw PixA::ConfigException(message.str());
      }

      coral_transaction->insertConnection(name, data, data, s_dstSlotName );
      coral_transaction.success();
      return true;
    }
    catch(...) {
    }
    return false;
  }



}
