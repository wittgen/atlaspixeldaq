/* Dear emacs, this is -*-c++-*- */
#ifndef _PixA_PixConnObjConfigDb_h_
#define _PixA_PixConnObjConfigDb_h_

#include "SharedConnObjConfigDbManager.h"
#include "ObjectCache_t.h"

namespace PixLib {
  class PixDbServerInterface;
}

namespace CoralDB {
  class CoralDB;
}
#define DEBUG_TRACE(a) { }

namespace PixA {

  namespace Aux {
    class Regex_t;
  }

  template <class T1, class T2>
  inline void  print(std::map<T1,T2> &a_map, typename std::map<T1,T2>::iterator a_iter, const T1 &value) {
    std::cout << "INFO [print] lower bound for " << value;
    if (a_iter != a_map.end()) {
      std::cout << " lower bound = " << a_iter->first;
    }
    else {
      std::cout << " END";
    }
    std::cout << std::endl;
  }

  template <class T1, class T2>
  inline typename std::map<T1,T2>::iterator lower_element(std::map<T1,T2> &a_map, const T1 &value) {
    typename std::map<T1,T2>::iterator a_iter = a_map.lower_bound(value);
    // debug
    DEBUG_TRACE( print(a_map, a_iter, value) )
    if (a_iter == a_map.end() || a_iter->first > value) {
      if ( a_iter != a_map.begin() )  {
	--a_iter;
	// debug
	DEBUG_TRACE( print(a_map, a_iter, value) )
	if (a_iter->first <= value) {
	  return a_iter;
	}
      }
      return a_map.end();
    }
    return a_iter;
  }

  template <class T1, class T2>
  inline typename std::map<T1,T2>::const_iterator lower_element(const std::map<T1,T2> &a_map, const T1 &value) {
    typename std::map<T1,T2>::const_iterator a_iter = lower_element(const_cast<std::map<T1,T2>&>(a_map),value);
    return a_iter;
  }


  class CfgLocation_t
  {
  public:
    enum EBackEnd {kUnknown, kDbServer, kFile, kConfigRootIO, kRootDb, kNBackEnds };

    CfgLocation_t(const std::string &file_name)
      : m_usedCounter(0),
	m_validTill(0),
	m_fileName(file_name),
	m_representation(0),
	m_backEnd( (!file_name.empty() ? kFile : kUnknown) ),
	m_validityIsCutOff(true)
    {}

    CfgLocation_t(Revision_t valid_till, bool validity_is_cut_off, const std::string &file_name, bool on_db_server)
      : m_usedCounter(0),
	m_validTill(valid_till),
	m_fileName(file_name),
	m_representation(0),
	m_backEnd( (on_db_server ? kDbServer : (!file_name.empty() ? kFile : kUnknown) )),
	m_validityIsCutOff(validity_is_cut_off)
    {}

    void setRepresentation( unsigned short representation) { m_representation = representation; }
    unsigned short representation() const { return m_representation; }
    unsigned short nextRepresentation() const { return (isTemporaryCfg() ? m_representation : m_representation+1); }
    void setValidity(Revision_t valid_till, bool is_cut_off) { m_validTill=valid_till; m_validityIsCutOff=is_cut_off; }
    Revision_t validTill()         const { return m_validTill; }
    bool isValidityCutOff()        const { return m_validityIsCutOff; }
    void setValidityCutOff(bool is_cut_off) { m_validityIsCutOff=is_cut_off; }

    void setFileName( const std::string &file_name) { 
      if (m_fileName != file_name ) {
	m_fileName=file_name;
	if (!m_fileName.empty() && m_backEnd!=kDbServer) {
	  m_backEnd=kFile;
	}
	else if (m_backEnd!=kDbServer) {
	  m_backEnd = kUnknown;
	}
      }
    }

    const std::string &fileName()  const { return m_fileName; }
    EBackEnd backEnd()             const { return m_backEnd;}
    bool onDbServer()              const { return m_backEnd == kDbServer; };
    bool isTemporaryCfg()          const { return m_fileName.empty(); }
    bool isFileCfg()               const { return m_backEnd>=kFile && m_backEnd<=kRootDb; }

    void setOnDbServer(bool on_db_server) { if (on_db_server) { m_backEnd = kDbServer;} else if (!m_fileName.empty()) { m_backEnd=kFile; } else { m_backEnd = kUnknown;}  }
    void setRootDb(bool is_root_db) { m_backEnd = (!is_root_db ? kConfigRootIO : kRootDb); }

    void use()                      { m_usedCounter++; }
    void resetUseCounter()          { m_usedCounter=0; }
    unsigned int useCounter() const { return m_usedCounter; }

    void determineFileType(const std::string &cfg_db_path) {
      if (m_backEnd==kFile) {
	m_backEnd = (configRootIOFile(cfg_db_path + m_fileName) ? kConfigRootIO : kRootDb);
      }
    }

  private:
    bool configRootIOFile(const std::string &file_name);

    unsigned int      m_usedCounter;
    Revision_t        m_validTill;
    std::string m_fileName;
    unsigned short    m_representation;
    EBackEnd          m_backEnd;
    bool              m_validityIsCutOff;
  };

  typedef std::map<Revision_t, CfgLocation_t> RevisionList_t;
  typedef std::map<std::string, RevisionList_t> ObjectList_t;

  class PixConnObjConfigDbTag;
  class PixConnObjConfigDbManager;

  class TagPayload_t {
    friend class PixConnObjConfigDbTag;
    friend class PixConnObjConfigDbManager;
  public:
    TagPayload_t(const std::string &id_tag, const std::string &tag) 
      : m_idTag(id_tag),
	m_cfgTag(tag),
	m_used(0)
    {}

    template <class T> ObjectCache_t<T> &cache() { return _cache<T>(); }
    template <class T> const ObjectCache_t<T> &cache() const { return const_cast<TagPayload_t *>(this)->_cache<T>(); }

    const ObjectList_t &objectIndex() const { return m_objectIndex; }

    const CfgLocation_t *cfgLocation(const std::string &obj_name, Revision_t revision) const {
      return const_cast<TagPayload_t *>(this)->_cfgLocation(obj_name, revision);
    }

    CfgLocation_t *cfgLocation(const std::string &obj_name, Revision_t revision)  {
      return _cfgLocation(obj_name, revision);
    }

    const std::string &idTag() const { return m_idTag; }
    const std::string &tag()   const { return m_cfgTag; } 

  protected:
    ObjectList_t &objectIndex() { return m_objectIndex; }

    CfgLocation_t *cfgLocationExactMatch(const std::string &obj_name, Revision_t revision) {
      if (obj_name.empty()) {
	throw PixA::ConfigException("FATAL [PixConnObjConfigDbManager::cfgLocation] Empty object name.");
      }
      ObjectList_t::iterator object_iter = m_objectIndex.find(obj_name);

      if (object_iter == m_objectIndex.end()) {
	// 	std::string message("FATAL [PixConnObjConfigDbManager::cfgLocation] Nothing known about ");
	// 	message += obj_name;
	// 	message += ".";
	// 	throw PixA::ConfigException(message);
	return NULL;
      }
      RevisionList_t::iterator revision_iter = object_iter->second.find(revision);
      if (revision_iter != object_iter->second.end()) {
	return &revision_iter->second;
      }
      return NULL;
    }

    CfgLocation_t *_cfgLocation(const std::string &obj_name, Revision_t revision) {

      if (obj_name.empty()) {
	throw PixA::ConfigException("FATAL [PixConnObjConfigDbManager::cfgLocation] Empty object name.");
      }
      ObjectList_t::iterator object_iter = m_objectIndex.find(obj_name);

      if (object_iter == m_objectIndex.end()) {
	// 	std::string message("FATAL [PixConnObjConfigDbManager::cfgLocation] Nothing known about ");
	// 	message += obj_name;
	// 	message += ".";
	// 	throw PixA::ConfigException(message);
	return NULL;
      }
      RevisionList_t::iterator revision_iter = lower_element(object_iter->second, revision);
      if (revision_iter != object_iter->second.end()) {
	if (     revision_iter->second.validTill()>  revision
	    || ( revision_iter->second.validTill()>= revision && revision_iter->second.isValidityCutOff()) ) {
	  return &revision_iter->second;
	}
      }
      return NULL;
    }

    void dumpRevisions() const;
    void dumpRevisions(const std::string &name) const;

    unsigned int used() const { return m_used; }
    void incrementUsed() { ++m_used; }
    void decrementUsed() { --m_used; }

  private:
    template <class T> ObjectCache_t<T> &_cache();

    ObjectList_t m_objectIndex;
    std::string m_idTag;
    std::string m_cfgTag;

    unsigned int m_used;
    ObjectCache_t<std::shared_ptr<PixLib::PixDisable> >     m_disableCache;
    ObjectCache_t<std::shared_ptr<PixLib::PixModule> >      m_moduleCache;
    ObjectCache_t<std::shared_ptr<PixLib::PixModuleGroup> > m_moduleGroupCache;
    ObjectCache_t<std::shared_ptr<PixLib::PixTrigController> >         m_timCache;

  };

  template <> 
  inline ObjectCache_t<std::shared_ptr<PixLib::PixModule> > &TagPayload_t::_cache<std::shared_ptr<PixLib::PixModule> >()
  { return m_moduleCache; }

  template <> 
  inline ObjectCache_t<std::shared_ptr<PixLib::PixDisable> > &TagPayload_t::_cache<std::shared_ptr<PixLib::PixDisable> >()
  { return m_disableCache; }

  template <>
  inline ObjectCache_t<std::shared_ptr<PixLib::PixModuleGroup> > &TagPayload_t::_cache<std::shared_ptr<PixLib::PixModuleGroup> >()
  { return m_moduleGroupCache; }

  template <> 
  inline ObjectCache_t<std::shared_ptr<PixLib::PixTrigController> > &TagPayload_t::_cache<std::shared_ptr<PixLib::PixTrigController> >()
  { return m_timCache; }


  class IConfigHelper {
  public:
    virtual ~IConfigHelper() {};

    virtual void readConfig(PixLib::Config &config,
			    TagPayload_t &tag_payload,
			    const std::string &name,
			    Revision_t revision,
			    const CfgLocation_t &cfg_location)  = 0;

    virtual bool writeConfig( const std::string &name,
			      const std::string &type_name,
			      const std::vector< std::pair< std::string, const PixLib::Config*> > &config,
			      TagPayload_t &tag_payload,
			      Revision_t revision,
			      CfgLocation_t &cfg_location,
			      const std::string &pending_tag) = 0;

    virtual bool writeConfig(const PixLib::PixModuleGroup &pix_module_group,
			     TagPayload_t &tag_payload,
			     Revision_t revision,
			     CfgLocation_t &cfg_location,
			     const std::string &pending_tag) = 0;

    virtual ConfigHandle modConf(TagPayload_t &tag_payload,
				 const std::string &name,
				 Revision_t revision,
				 const CfgLocation_t &cfg_location)  = 0;

    virtual ConfigHandle bocConf(TagPayload_t &tag_payload,
				const std::string &name,
				Revision_t revision,
				const CfgLocation_t &cfg_location)  = 0;

    virtual ConfigHandle rodConf(TagPayload_t &tag_payload,
				const std::string &name,
				Revision_t revision,
				const CfgLocation_t &cfg_location)  = 0;

    virtual ConfigHandle controllerConf(TagPayload_t &tag_payload,
					const std::string &name,
					Revision_t revision,
					const CfgLocation_t &cfg_location)  = 0;


    virtual bool isSuitable(CfgLocation_t &cfg_location) const = 0;
  };

  typedef ConfigHandle (IConfigHelper::*ConfigHandleFunc_t)(TagPayload_t &tag_payload,
							    const std::string &name,
							    Revision_t revision,
							    const CfgLocation_t &cfg_location);

  class PixConnObjConfigDbManager;

  class PixConnObjConfigDbTag : public IConnObjConfigDbTag
  {
  public:
    PixConnObjConfigDbTag(PixConnObjConfigDbManager &manager, TagPayload_t &tag_payload, Revision_t revision);
    ~PixConnObjConfigDbTag();

  protected:

    void configure(PixLib::Config &config);

    ConfigHandle  moduleConfigHandle(const std::string &name);
    ConfigHandle  moduleGroupConfigHandle(const std::string &name);
    ConfigHandle  bocConfigHandle(const std::string &name);
    ConfigHandle  rodControllerConfigHandle(const std::string &name);
    ConfigHandle  timConfigHandle(const std::string &name);

    ConfigHandle  disableConfigHandle(const std::string &name);

    /**
     * WARNING: nothing guarantees the lifetime of the object!
     */
    const PixLib::Config &moduleConfig(const std::string &name);

    /**
     * WARNING: nothing guarantees the lifetime of the object!
     */
    const PixLib::Config &moduleGroupConfig(const std::string &name);

    /**
     * WARNING: nothing guarantees the lifetime of the object!
     */
    const PixLib::Config &bocConfig(const std::string &name);

    /**
     * WARNING: nothing guarantees the lifetime of the object!
     */
    const PixLib::Config &rodControllerConfig(const std::string &name);

    /**
     * WARNING: nothing guarantees the lifetime of the object!
     */
    const PixLib::Config &timConfig(const std::string &name);

    /**
     * WARNING: nothing guarantees the lifetime of the object!
     */
    const PixLib::PixModule      &configuredModule(const std::string &name);

    /**
     * WARNING: nothing guarantees the lifetime of the object!
     */
    const PixLib::PixModuleGroup &configuredModuleGroup(const std::string &name);

    /**
     * WARNING: nothing guarantees the lifetime of the object!
     */
    const PixLib::PixBoc         &configuredBoc(const std::string &name);

    /**
     * WARNING: nothing guarantees the lifetime of the object!
     */
    const PixLib::PixController  &configuredRodController(const std::string &name);

    /**
     * WARNING: nothing guarantees the lifetime of the object!
     */
    const PixLib::PixTrigController &configuredTim(const std::string &name);


    std::shared_ptr<const PixLib::PixDisable> disablePtr(const std::string &name);

    std::shared_ptr<const PixLib::PixModule> configuredModulePtr(const std::string &name);

    std::shared_ptr<const PixLib::PixModuleGroup> configuredModuleGroupPtr(const std::string &name);

    std::shared_ptr<const PixLib::PixTrigController> configuredTimPtr(const std::string &name);

    // retrieve PixController ID to create correct version of PixModuleGroup - check if there's a neater way
    int getCtrlId(const std::string &name);

    /**
     * The object is owned by the caller, who has the responsibility about cleaning up !
     */
    PixLib::PixModule      *createConfiguredModule(const std::string &name);
    PixLib::PixModuleGroup *createConfiguredModuleGroup(const std::string &name);
    PixLib::PixTrigController *createConfiguredTim(const std::string &name);

    Revision_t storeConfig( const std::string &name,
			    const std::string &type_name,
			    const PixLib::Config &config,
			    Revision_t revision=0,
			    const std::string &pending_tag=s_tmpPendingTag,
			    bool cache=false);

    Revision_t storeConfig( const PixLib::Config &config, Revision_t revision=0, const std::string &pending_tag=s_tmpPendingTag, bool cache=false);

    Revision_t storeConfig( const PixLib::PixModule &obj, Revision_t revision=0, const std::string &pending_tag=s_tmpPendingTag, bool cache=false);
    Revision_t storeConfig( const PixLib::PixModuleGroup &obj, Revision_t revision=0, const std::string &pending_tag=s_tmpPendingTag, bool cache=false);

    Revision_t revision() const { return m_revision; }
    const std::string &idTag() const { return m_tagPayload->idTag(); }
    const std::string &tag() const { return m_tagPayload->tag(); }

    void getObjectList(std::set<std::string> &object_names_out, const std::string &name_filter);

  public:
    static bool isValid(Revision_t revision) { return revision!=s_maxRevision && revision-1!=s_maxRevision; };

    bool isTemporaryCfg(const std::string &name) { return cfgLocation(name).isTemporaryCfg(); }

  private:
    Revision_t storeConfig(const std::string &name,
			   const std::string &type_name,
			   const std::vector<std::pair<std::string, const PixLib::Config *> > &config_list,
			   Revision_t revision,
			   const std::string &pending_tag,
			   bool cache);

    template <class T_CompoundObj_t> const std::shared_ptr<const T_CompoundObj_t> configuredObject(const std::string &name);
    template <class T_CompoundObj_t, class T_Obj_t> const T_Obj_t *configuredChildObject(const std::string &name);
    template <class T_CompoundObj_t, class T_Obj_t> const PixLib::Config &config(const std::string &name);
    ConfigHandle configHandle(ConfigHandleFunc_t config_handle_func, const std::string &name);
    template <class T_CompoundObj_t, class T_Obj_t> ConfigHandle handleFromCachedObject(const std::string &name);

    CfgLocation_t &cfgLocation(const std::string &name);

    PixConnObjConfigDbManager *m_dbManager;

    TagPayload_t *m_tagPayload;
    Revision_t    m_revision;

    static const unsigned int s_maxRevision = UINT_MAX;
    static const std::string s_tmpPendingTag;
  };


  class ConfigHelperWithManagerLink : public IConfigHelper
  {
  public:
    ConfigHelperWithManagerLink(PixConnObjConfigDbManager &manager)
      : m_manager(&manager) {}

    template <class T> T *createObject(const std::string &name);

  protected:

    void registerRevision(TagPayload_t &tag_payload,
			  const std::string &type_name,
			  const std::string &name,
			  Revision_t revision,
			  CfgLocation_t &cfg_location);

    PixConnObjConfigDbManager *m_manager;
  };

  class RevisionInserter;

  class PixConnObjConfigDbManager : public SharedConnObjConfigDbManager {
    friend class PixConnObjConfigDbTag;
    friend class ConfigHelperWithManagerLink;
    friend class RevisionInserter;
  public:

    PixConnObjConfigDbManager(const std::shared_ptr<PixLib::PixDbServerInterface> &db_server,
			      IRevisionIndex *revision_index,
			      CfgLocation_t::EBackEnd store_back_end = CfgLocation_t::kDbServer);

    ~PixConnObjConfigDbManager();

    void getIdTags(std::vector<std::string> &id_tag_out);

    void getTags(const std::string &id_tag, std::vector<std::string> &tag_out);

    void getRevisions(const std::string &id_tag, const std::string &cfg_tag, const std::string &conn_name, std::set<Revision_t> &revisions_out);

    void getNames(const std::string &id_tag, const std::string &cfg_tag, EObjectType type, std::set<std::string> &names);

    /** Return a helper object to configure arbitrary object are retrieve arbitrary configurations from a particular tag.
     */
    ConnObjConfigDbTagRef getTag(const std::string &id_tag, const std::string &cfg_tag, Revision_t revision);

    /** Clone the given tag.
     */
    bool cloneTag(const std::string &id_tag,
		  const std::string &src_tag,
		  const std::string &dest_tag,
		  const std::string &pending_tag="_Tmp");

    void dumpRevisions() const;

    void dumpRevisions(const std::string &name) const;


    static void resetInstance( ) {
      setInstance(NULL);
    }

    static SharedConnObjConfigDbManager *createInstance( const std::shared_ptr<PixLib::PixDbServerInterface> &db_server,
							 CfgLocation_t::EBackEnd store_back_end = CfgLocation_t::kDbServer) {
      PixConnObjConfigDbManager *instance = pixConnObjConfigDbManagerInstance();
      if (!instance || instance->m_storeBackEnd != store_back_end) {
	setInstance( new PixConnObjConfigDbManager(db_server, createRevisionIndex(), store_back_end ) );
      }
      return currentInstance();
    }

    static SharedConnObjConfigDbManager *instance()
    { 
      if (!hasInstance()) {
	// if no instance has been created yet an instance without db server support will be created
	return createInstance(std::shared_ptr<PixLib::PixDbServerInterface>());
      } 
      else {
	return currentInstance();
      }
    }

    static PixConnObjConfigDbManager *pixConnObjConfigDbManagerInstance() {
      if (hasInstance()) {
	SharedConnObjConfigDbManager *manager = instance();
	PixConnObjConfigDbManager *pix_conn_obj_config_db_manager = dynamic_cast<PixConnObjConfigDbManager *>(manager);
	return pix_conn_obj_config_db_manager;
      }
      else {
	return NULL;
      }
    }

    // debug
    static void dumpRevisionsOfInstance() {
      PixConnObjConfigDbManager *pix_conn_obj_config_db_manager = pixConnObjConfigDbManagerInstance();
      if (pix_conn_obj_config_db_manager) {
	pix_conn_obj_config_db_manager->dumpRevisions(); 
      }
    }

    // debug
    static void dumpRevisionsOfInstance(const std::string &name) { 
      PixConnObjConfigDbManager *pix_conn_obj_config_db_manager = pixConnObjConfigDbManagerInstance();
      if (pix_conn_obj_config_db_manager) {
	pix_conn_obj_config_db_manager->dumpRevisions(name); 
      }
    }

//     static std::string &cfgDomainHeader() { return s_cfgDomainHeader; }

//     static std::string &cfgIdTagTrailer() { return s_cfgIdTagTrailer; }

//     static std::string makeCfgDomainName(const std::string &id_tag) { return PixConnObjConfigDbManager::cfgDomainHeader() + id_tag; }

//     static std::string makeCfgIdTag(const std::string &id_tag) { return id_tag+PixConnObjConfigDbManager::cfgIdTagTrailer(); }

  protected:

    static IRevisionIndex *createRevisionIndex();

    std::string stripDefaultPath(const std::string &file_name);

    IConfigHelper &configHelper(CfgLocation_t &cfg_location) {

      assert( m_configHelper.size() == CfgLocation_t::kNBackEnds );
      if (cfg_location.backEnd()==CfgLocation_t::kFile) {
	assert( m_configHelper[CfgLocation_t::kConfigRootIO] );
	assert( m_configHelper[CfgLocation_t::kRootDb] );
	if (m_configHelper[CfgLocation_t::kConfigRootIO]->isSuitable(cfg_location)) {
	  cfg_location.setRootDb(false);
	}
	else if (m_configHelper[CfgLocation_t::kRootDb]->isSuitable(cfg_location)) {
	  cfg_location.setRootDb(true);
	}
      }
      if (cfg_location.backEnd()>=CfgLocation_t::kNBackEnds || !m_configHelper[cfg_location.backEnd()]) {
	std::stringstream message;
	message << "FATAL [PixConnObjConfigDbManager::configHelper] Not a valid back-end " << cfg_location.backEnd() << ".";

	throw PixA::ConfigException( message.str() );
      }
      assert( m_configHelper[cfg_location.backEnd()] );
      return *(m_configHelper[cfg_location.backEnd()]);
    }

    IConfigHelper *configStoreHelper() {
      return m_configHelper[m_storeBackEnd];
    }

    std::shared_ptr<PixLib::PixDbServerInterface> dbServer() { return m_dbServer; }

    bool updateRevisions(const std::string &id_tag, const std::string &cfg_tag, ObjectList_t &object_list, Revision_t desired_revision);

    CfgLocation_t &cfgLocation(TagPayload_t &tag_payload, const std::string &name, Revision_t revision);

    template <class T> T *createObject(const std::string &name);

    std::pair<std::string,unsigned short> fileName(TagPayload_t &tag_payload, const std::string &name, Revision_t revision);

    void registerRevision(TagPayload_t &tag_payload,
			  const std::string &type_name,
			  const std::string &name,
			  Revision_t revision,
			  CfgLocation_t &cfg_location);

  private:

    std::shared_ptr<PixLib::PixDbServerInterface> m_dbServer;
    std::unique_ptr<IRevisionIndex>                   m_revisionIndex;
    std::unique_ptr<IRevisionIndex>                   m_dbServerRevisionIndex;

    static std::vector<std::string> s_defaultCfgPath;
    //    static std::string s_cfgDomainHeader;

    // id_tag, cfg_tag, name, revision


    typedef std::map<std::string, TagPayload_t> CfgTagList_t;
    typedef std::map<std::string, CfgTagList_t> IdTagList_t;

//     RevisionList_t::iterator insertRevision(RevisionList_t &revision_list,
// 					    const ExtRevision_t &revision,
// 					    Revision_t min_revision,
// 					    Revision_t max_revision,
// 					    const std::string &path_name,
// 					    bool on_db_server);

    void getTags(const std::string &id_tag, std::vector<std::string> &tags_out  ) const;

    TagPayload_t &tagList(const std::string &id_tag, const std::string &cfg_tag) {

      if (id_tag.empty() ||  cfg_tag.empty()) {
	throw PixA::ConfigException("FATAL [PixConnObjConfigDbManager::tagList] Empty id or config tag name.");
      }

      CfgTagList_t &tag_list = m_revisionList[id_tag];
      CfgTagList_t::iterator tag_iter = tag_list.find(cfg_tag);
      if (tag_iter == tag_list.end()) {
	std::pair<CfgTagList_t::iterator, bool> ret = tag_list.insert(std::make_pair(cfg_tag,TagPayload_t(id_tag,cfg_tag)));
	if (!ret.second) {
	  std::string message ("FATAL [PixConnObjConfigDbManager::tagList] Failed to add tag ");
	  message += cfg_tag;
	  message += ".";

	  throw PixA::ConfigException(message);
	}
	tag_iter = ret.first;
      }

      return tag_iter->second;

    }

    IdTagList_t  m_revisionList;

    //    template <class T_CompoundObj_t, class T_Obj_t> IConfig *objectConfig(TagPayload_t &tag_payload, const std::string &name, Revision_t revision);

    template <class T_CompoundObj_t> T_CompoundObj_t *createConfiguredObject(TagPayload_t &tag_payload,
									     const std::string &name,
									     Revision_t revision);

    template <class T_CompoundObj_t> std::shared_ptr<const T_CompoundObj_t> cacheObject(TagPayload_t &tag_payload,
											  const std::string &name,
											  Revision_t revision);

    void configure(PixLib::Config &config,
		   TagPayload_t &tag_payload,
		   const std::string &name,
		   Revision_t revision);

      template <class T_CompoundObj_t> ConfigHandle configHandle(TagPayload_t &tag_payload,
								 const std::string &name,
								 Revision_t revision);



    std::vector< IConfigHelper *> m_configHelper;
    std::string                   m_cfgDbPath;

    CfgLocation_t::EBackEnd  m_storeBackEnd;
  };

  inline CfgLocation_t &PixConnObjConfigDbTag::cfgLocation(const std::string &name) {
    return m_dbManager->cfgLocation(*m_tagPayload,name, m_revision);
  }

  //     for (unsigned int pass_i=0; ; pass_i++) {

  //       CfgLocation_t *cfg_location = m_tagPayload->cfgLocation(name, m_revision);

  //       std::cout << "INFO [PixConnObjConfigDbTag::cfgLocation] " << pass_i << ": name = " << name << " revision = "  << m_revision
  // 		<< static_cast<void *>(cfg_location);
  //       if (cfg_location) {
  // 	std::cout << " valid till = " << (cfg_location->isValidityCutOff() ? "~" : "" ) << cfg_location->validTill();
  //       }
  //       std::cout << std::endl;

  //       if (cfg_location) {
  // 	std::cout << "INFO [PixConnObjConfigDbTag::cfgLocation] name = " << name << " revision = "  << m_revision << " ["
  // 		  << static_cast<void *>(cfg_location) << "] "
  // 		  << " back-end = " << static_cast<int>( cfg_location->backEnd() )
  // 	      << std::endl;
  // 	return *cfg_location;
  //       }

  //       if (pass_i == 1 || !m_dbManager->updateRevisions(m_tagPayload->idTag(), m_tagPayload->tag(), m_tagPayload->objectIndex(),  m_revision)) {
  // 	break;
  //       }
  //     }

  //     std::stringstream message;
  //     message << "FATAL [PixConnObjConfigDbTag::cfgLocation] No configuration for "
  // 	    << name << " revision " << m_revision << ".";
  //     throw ConfigException(message.str());
  //   }

  template <class T> 
  inline T *ConfigHelperWithManagerLink::createObject(const std::string &name) {
    return m_manager->createObject<T>(name);
  }

  inline void ConfigHelperWithManagerLink::registerRevision(TagPayload_t &tag_payload,
							    const std::string &type_name,
							    const std::string &name,
							    Revision_t revision,
							    CfgLocation_t &cfg_location) {
      m_manager->registerRevision(tag_payload, type_name, name, revision, cfg_location);
  }


}
#undef DEBUG_TRACE
#endif
