#include "ConnObjConfigDb.h"
#include "PixConnObjConfigDb.h"

namespace PixA {


  SharedConnObjConfigDbManager *ConnObjConfigDb::create(const std::shared_ptr<PixLib::PixDbServerInterface> &db_server, bool persistent_storage) {
    return PixConnObjConfigDbManager::createInstance(db_server, (persistent_storage ?
								   CfgLocation_t::kConfigRootIO
								 : CfgLocation_t::kDbServer));
  }

  ConnObjConfigDb ConnObjConfigDb::sharedInstance() {
    if (!SharedConnObjConfigDbManager::hasInstance()) {
      return ConnObjConfigDb();
    }
    else {
      return ConnObjConfigDb(SharedConnObjConfigDbManager::instance());
    }
  }


}
