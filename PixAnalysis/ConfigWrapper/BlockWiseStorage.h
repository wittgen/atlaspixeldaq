#ifndef _BlockWiseStorage_hh_
#define _BlockWiseStorage_hh_

#include <list>
#include <vector>
#include <utility>
#include <iostream>

namespace PixA {

template <class T>
class BlockWiseStorage
{
  typedef std::vector< T > Block_t;
  typedef std::pair< unsigned int, Block_t > ListElement_t;
  typedef std::list<ListElement_t > List_t;

public:
  BlockWiseStorage(unsigned int chunk_size=64) : m_chunkSize(chunk_size) {}

  T *alloc() {
    if (m_blocks.empty() || m_blocks.back().first >= m_blocks.back().second.size()) {
      m_blocks.push_back(std::make_pair<unsigned int, Block_t >(0 , Block_t() ) );
      m_blocks.back().second.resize(m_chunkSize);
    }

    Block_t &a=m_blocks.back().second;
    T *ptr= &( a[ m_blocks.back().first ] );
    m_blocks.back().first++;
    return ptr;
  }

  void giveBack(T *ptr) {
    dump();
    assert( !m_blocks.empty() );
    Block_t &a=m_blocks.back().second;
    assert( m_blocks.back().first > 0);
    if ( ptr == &( a[ m_blocks.back().first ] ) ) {
      m_blocks.back().first--;
    }
    dump();
  }

  T *store(const T &value) {
    T *ptr=alloc();
    *ptr=value;
    return ptr;
  }

  void dump() {
    unsigned int i=0;
    for (typename List_t::const_iterator block_iter=m_blocks.begin();
	 block_iter != m_blocks.end();
	 block_iter++, i++) {
      std::cout << i << " : size=" << block_iter->second.size() << " used=" << block_iter->first << std::endl;
    }
  }

private:
  unsigned int m_chunkSize;
  List_t m_blocks;
};

}
#endif
