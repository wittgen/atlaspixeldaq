#ifndef _IDbWrapper_h_
#define _IDbWrapper_h_

#include <cassert>
#include <utility>
#include <vector>
#include <sstream>
#include <stdexcept>
#include <map>
#include <memory>

#include <PixDbInterface/PixDbInterface.h>
#include "ObjStat_t.h"

namespace PixLib {
  class PixDbInterface;
//  class DbRecord;
}

namespace PixA {

  template<class T> inline void do_not_deleter(T * /* x */) {
//    std::cout << "INFO [do_not_deleter] Do not delete " << static_cast<void *>(x) << std::endl;
  }

  class DbManager;

  class IDbOpenHelper {
  public:
    IDbOpenHelper(bool must_always_close) : m_mustAlwaysClose(must_always_close) {}
    virtual ~IDbOpenHelper() {}

    virtual std::pair<PixLib::PixDbInterface *, PixLib::DbRecord *> open() const = 0;

    virtual std::pair<PixLib::PixDbInterface *, PixLib::DbRecord *>  openForWriting() const = 0;

    bool mustAlwaysClose() const {return true /*m_mustAlwaysClose*/;}

    /** Return true if records must be deleted by the user.
     * ATTENTION: RootDb does not allow the same file to be opened twice, and requires the user to delete the records and fields
     *            CoralDb on the otherhand does not crash on writing if a file is opened twice, so the cached records may be rendered
     *            invalid (so maybe one should also close coraldb connections). Moreover, PixDbCoralDb manages memory internally,
     *            i.e. records and fields must not be deleted by the user. In summary, "must always close" and "cleanup" are both
     *            true for RootDb and both false for CoralDb, so for the time being there is only one flag.
     */
    bool haveToCleanUp() const { return m_mustAlwaysClose;}

    //    IDbOpenHelper *subRecordHelper(const std::string &sub_record) const = 0;
  private:
    bool m_mustAlwaysClose;
  };

  class DbWrapper
  {
    friend class DbManager;
    friend class DbConfigHandle;
    friend class PixDbAccessor;
  protected:

    /** Wrap around an existing pix db interface which will not be deleted.
     * This is not very save, since nothing ensures that the pixdb interface stays 
     * alive during the lifetime of the DbWrapper.
     * So, use with care.
     */
    DbWrapper(PixLib::PixDbInterface *db);

  public:
    DbWrapper(IDbOpenHelper *a) 
      : m_creator(a),m_dbUsers(0), m_nAccessors(0),m_isOpen(false),m_cleanup(false) 
      {m_db.first=NULL;assert(m_creator); m_cleanup=m_creator->haveToCleanUp(); s_stat.ctor(); }

    ~DbWrapper();

    void use(bool read_only=true) {
      if (read_only) {
      if (m_dbUsers==0) {
	  openDb();
	}
      }
      else {
	openDbForWriting();
      }

      assert(m_db.first && m_db.second.get());
      m_dbUsers++;
      //      std::cout << "INFO [DbWrapper::use] use" << static_cast<void *>(this) 
      //		<< " db = " << static_cast<void *>(m_db.first) 
      //                << " (users=" << m_dbUsers <<")" << std::endl;
    }

    /** use the database record to which the iterator is pointing to.
     * @param record_iter a valid record iterator which points to the record in question.
     * if recrod_iter is invalid the behaviour is undefined.
     */
    std::shared_ptr<PixLib::DbRecord> use(PixLib::dbRecordIterator &record_iter, bool read_only=true);

    void done() {
      assert(m_nAccessors > 0);
      assert(m_dbUsers >0 );
      if (--m_dbUsers==0) { closeDb(false) ; }
    }


    PixLib::PixDbInterface *db()  { assert(m_dbUsers>0 && m_db.first); return m_db.first;}
    std::shared_ptr<PixLib::DbRecord> root()      { assert(m_dbUsers>0 && m_db.first && m_db.second.get()); return m_db.second;}

    std::shared_ptr<PixLib::DbRecord> record(const std::vector<std::string> &name);

    std::shared_ptr<PixLib::DbRecord> recordByName(const std::string &dec_name) {
      assert(m_dbUsers>0 && m_db.first ); 
      PixLib::DbRecord *a_record =( m_db.first->DbFindRecordByName(dec_name) );
      std::shared_ptr<PixLib::DbRecord> temp;
      if (haveToCleanUp()) {
	temp=std::shared_ptr<PixLib::DbRecord>( a_record);
      }
      else {
	temp=std::shared_ptr<PixLib::DbRecord>( a_record, do_not_deleter<PixLib::DbRecord>);
      }
      return temp;
    }

    std::shared_ptr<PixLib::DbRecord> addRecord(std::shared_ptr<PixLib::DbRecord> &parent, const std::string &class_name, const std::string &name) {
      m_db.first->transactionStart();
      std::shared_ptr<PixLib::DbRecord> temp;
      if (haveToCleanUp()) {
	temp=std::shared_ptr<PixLib::DbRecord>( parent->addRecord(class_name,name));
      }
      else {
	temp=std::shared_ptr<PixLib::DbRecord>( parent->addRecord(class_name,name), do_not_deleter<PixLib::DbRecord>);
      }
      m_db.first->transactionCommit();
      return temp;
    }

    bool haveToCleanUp() const {
      return m_cleanup;
    }

    unsigned int use_count() const {
      return m_dbUsers;
    }

    unsigned int accessors() const {
      return m_nAccessors;
    }

  protected:
    void incAccessorCount() { m_nAccessors++;}
    void decAccessorCount() { assert(m_nAccessors>0); if (--m_nAccessors==0) { destroy(); } }

    bool canRecreate() const { return m_creator != NULL; }

    IDbOpenHelper           *m_creator;
    std::pair<PixLib::PixDbInterface *, std::shared_ptr<PixLib::DbRecord> > m_db;
    unsigned int             m_dbUsers;
    unsigned int             m_nAccessors;
    bool                     m_isOpen;
    bool                     m_cleanup;
    bool                     m_isWritable;

  private:
    /** Destroy this db wrapper.
     * Should only be called by @ref done.
     */
    void destroy();

    /** Open the PixDbInterface.
     */
    void openDb();

    /** Open the PixDbInterface in update mode.
     */
    void openDbForWriting();

    /** Close the PixDbInterface.
     * @param force if set to false the database is only closed if the back-end requires to be always closed.
     * The PixDbInterface can be opened again.
     */
    void closeDb(bool force);

#ifdef DEBUG
  protected:
    static void openFile() {
      s_nFiles++;
      s_nOpenCalls++;
    }

    static void closeFile() {
      s_nFiles--;
      s_nCloseCalls++;
    }

    static void keepRecord(const std::shared_ptr<PixLib::DbRecord> &record);

  public:
    static void stat();

  private:

    static unsigned int s_nFiles;
    static unsigned int s_nOpenCalls;
    static unsigned int s_nCloseCalls;
    static unsigned int s_nRecordsCreated;
    static std::map< unsigned long , std::shared_ptr<PixLib::DbRecord> > s_records;
#endif
  private:
    static ObjStat_t s_stat;
  };

}
#endif
