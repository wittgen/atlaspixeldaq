#ifndef _PixA_PixDisableUtil_h_
#define _PixA_PixDisableUtil_h_

#include "ConnectivityUtil.h"
#include <memory>
#include <PixConnectivity/PixDisable.h>
#include <PixConnectivity/PartitionConnectivity.h>
#include "pixa_exception.h"

namespace PixA {

  typedef std::set<std::string>  ModuleEnableMap_t;
  typedef std::map<std::string, ModuleEnableMap_t >  EnableMapContainer_t;

  class EnableMap_t {
  public:
    typedef EnableMapContainer_t::const_iterator const_iterator;
    typedef EnableMapContainer_t::iterator iterator;

    EnableMap_t() : m_map(new EnableMapContainer_t) {}

    EnableMapContainer_t::iterator begin() {
      assert(m_map.get());
      return m_map->begin();
    }

    EnableMapContainer_t::iterator end() {
      assert(m_map.get());
      return m_map->end();
    }

    EnableMapContainer_t::iterator find(const std::string &rod_name) {
      assert(m_map.get());
      return m_map->find(rod_name);
    }

    EnableMapContainer_t::const_iterator begin() const {
      assert(m_map.get());
      return m_map->begin();
    }

    EnableMapContainer_t::const_iterator end() const {
      assert(m_map.get());
      return m_map->end();
    }

    EnableMapContainer_t::size_type size() const {
      assert(m_map.get());
      return m_map->size();
    }

    EnableMapContainer_t::const_iterator find(const std::string &rod_name) const {
      assert(m_map.get());
      return m_map->find(rod_name);
    }
    
    ModuleEnableMap_t &operator[](const std::string &rod_name) {
      assert(m_map.get());
      return (*m_map)[rod_name];
    }

    const ModuleEnableMap_t &operator[](const std::string &rod_name) const {
      assert(m_map.get());
      return (*m_map)[rod_name];
    }

    ModuleEnableMap_t &operator[](const char *rod_name) {
      assert(m_map.get());
      return (*m_map)[std::string(rod_name)];
    }

    const ModuleEnableMap_t &operator[](const char *rod_name) const {
      assert(m_map.get());
      return (*m_map)[std::string(rod_name)];
    }

    std::pair< iterator, bool> insert(const std::pair<std::string, ModuleEnableMap_t> &element) {
      return m_map->insert(element);
    }

    bool isValid() const {
      return m_map.get() && !m_map->empty();
    }
    operator bool() const {
      return isValid();
    }

  private:
    std::shared_ptr<EnableMapContainer_t> m_map;
  };


  class DisabledListRoot;
  DisabledListRoot createPp0DisabledList( const PixA::ConnectivityRef &conn, const DisabledListRoot &root_disabled_list, const std::string &rod_name);
  // usage
  // std::shared_ptr<PixLib::PixDisable> pix_disable(new PixDisable);
  // PixA::ConnectivityRef conn ...
  // RootDisabledList root_disabled_list( conn, pix_disable.get() );
  // PixA::DisabledList crate_disabled_list( crateDisabeledList(root_disabled_list) );
  // if (PixA::enabled(crate_disabled_list, crate_iter or PixLib::RodCrateConnectivity*)) ...
  //   PixA::DisabledList rod_disabled_list( rodDisabeledList(crate_disabled_list, rod_iter or PixLib::RodBocConnectivity*) );
  //
  //    or
  //
  // PixA::DisabledList crate_disabled_list( crateDisabeledList(root_disabled_list) );
  // PixA::DisabledList rod_disabled_list( rodDisabeledList(crate_disabled_list, rod_iter or PixLib::RodBocConnectivity*) );
  // if (rod_disable_list.parentDisabled())


  class DisabledList {
    friend class DisabledListRoot;
  public:
    enum EDisableListType {kCrateList, kRodList, kPp0List, kModuleList};
  protected:
    DisabledList(const PixLib::PixDisable *pix_disable)
      : m_list(NULL),
	m_type(kCrateList)
    {
      if (pix_disable != NULL) {
	const PixLib::ConfObj &obj = const_cast<PixLib::PixDisable *>(pix_disable)->config()["Disabled"]["Objects"];
	assert( const_cast<PixLib::ConfObj &>(obj).type() == PixLib::ConfObj::STRVECT);
	const PixLib::ConfStrVect &str_vec = static_cast<const PixLib::ConfStrVect&>( obj );
	m_list = &( const_cast<PixLib::ConfStrVect &>(str_vec).value());
      }
    }

    DisabledList(const PixLib::PixDisable *pix_disable, EDisableListType type)
      : m_list(NULL),
	m_type(type)
    {
      if (pix_disable != NULL) {
	const PixLib::ConfObj &obj = const_cast<PixLib::PixDisable *>(pix_disable)->config()["Disabled"]["Objects"];
	assert( const_cast<PixLib::ConfObj &>(obj).type() == PixLib::ConfObj::STRVECT);
	const PixLib::ConfStrVect &str_vec = static_cast<const PixLib::ConfStrVect&>( obj );
	m_list = &( const_cast<PixLib::ConfStrVect &>(str_vec).value());
      }
    }

    DisabledList(const EnableMap_t &enable_map, EDisableListType type)
      : m_list(NULL),
	m_type(type),
	m_enableMap(enable_map)
    { if (m_enableMap.isValid()) m_rodMapIter = m_enableMap.end(); }

    DisabledList(const EnableMap_t &enable_map, const std::string &rod_name)
      : m_list(NULL),
	m_type(kPp0List),
        m_enableMap(enable_map)
    { if (m_enableMap.isValid()) m_rodMapIter = m_enableMap.find(rod_name); }

    DisabledList(const DisabledList &a, const std::string &rod_name) 
      : m_list(a.m_list),
	m_type(kPp0List),
	m_enableMap(a.m_enableMap)
    { if (m_enableMap.isValid()) m_rodMapIter = m_enableMap.find(rod_name); }

  public:
    DisabledList() : m_list(NULL), m_type(kCrateList) {}

    //    DisabledList(const DisabledList &a) : m_list(a.m_list), m_type(a.m_type), m_enableMap(a.m_enableMap), m_rodMapIter(a.m_rodMapIter) {}

    DisabledList getParentList() const {
      DisabledList temp(*this);
      switch (m_type) {
      case kPp0List:
	if (m_enableMap.isValid()) {
	  temp.m_rodMapIter=temp.m_enableMap.end();
	}
      case kRodList:
      case kModuleList:
	// --temp.m_type;
	temp.m_type = static_cast<EDisableListType>(static_cast<unsigned short>(temp.m_type)-1);
	break;
      case kCrateList:
	break;
      }
      return temp;
    }

    DisabledList getChildList() const {
      assert(!isEnableMap() || (m_type != kRodList && m_type<kModuleList));
      DisabledList temp(*this);
      // ++temp.m_type;
      temp.m_type = static_cast<EDisableListType>(static_cast<unsigned short>(temp.m_type)+1);
      return temp;
    }

    DisabledList getChildList(const std::string &conn_name) const {
      assert(m_type == kRodList  && isEnableMap());
      return DisabledList(*this, conn_name);
    }

    bool enabled(const std::string &conn_name) const {
      if (isEnableMap()) {
	return enableMapEnabled(conn_name);
      }
      else {
	return pixDisableEnabled(conn_name);
      }
    }

    bool isValid() const {
      return m_list != NULL;
    }

    //     inline int size() const { 
    //       if(isValid()) 
    // 	return m_list->size(); 
    //       else return -1; 
    //     }

    bool isEnableMap() const {
      return m_enableMap.isValid() && m_list==NULL;
    }

    EDisableListType type() const { return m_type; }

    const EnableMap_t &enableMap() const { assert(isEnableMap()); return m_enableMap; }

  protected:

    bool pixDisableEnabled(const std::string &conn_name) const {
      if (!isValid()) return true;
      if (find(m_list->begin(),m_list->end(),conn_name) == m_list->end()) return true;
      return false;
    }

    bool enableMapEnabled(const std::string &conn_name) const {
      assert(m_enableMap.isValid());
      switch(m_type) {
      case kRodList:
	return m_enableMap.find(conn_name) != m_enableMap.end();
      case kModuleList:
	return m_rodMapIter != m_enableMap.end() && m_rodMapIter->second.find(conn_name) != m_rodMapIter->second.end();
      case kCrateList:
      case kPp0List:
	// crates and Pp0s cannot be individually enabled.
	break;
      }
      return true; 
    }

  private:

    std::vector<std::string> *m_list;
    EDisableListType  m_type;
    EnableMap_t m_enableMap;
    EnableMapContainer_t::const_iterator m_rodMapIter;
  };

  class DisabledListRoot : public DisabledList {

    friend DisabledListRoot createPp0DisabledList( const PixA::ConnectivityRef &conn, const DisabledListRoot &root_disabled_list, const std::string &rod_name);
  protected:
    DisabledListRoot(const std::shared_ptr<const PixLib::PixDisable> &pix_disable, DisabledListRoot::EDisableListType type) 
      : DisabledList(pix_disable.get(), type),
	m_pixDisable(pix_disable)
    {}

  public:
    /** An empty crate disable list.*/
    DisabledListRoot() {}

    DisabledListRoot(const DisabledListRoot &a) 
      : DisabledList(a),
      m_pixDisable(a.m_pixDisable)
      {  }

    /** Use the disable list given by the pix disable.*/
    DisabledListRoot(const std::shared_ptr<const PixLib::PixDisable> &pix_disable) 
      : DisabledList(pix_disable.get()),
	m_pixDisable(pix_disable)
    { }

    DisabledListRoot(const EnableMap_t &enable_map)
      : DisabledList(enable_map,kCrateList)
    { }

    DisabledListRoot(const EnableMap_t &enable_map, const std::string &rod_name)
      : DisabledList(enable_map, rod_name)
    {  }

  private:
    std::shared_ptr<const PixLib::PixDisable> m_pixDisable;
  };


  //  inline DisabledList disabledlist( const DisabledList &disabled_list) {
  //    return disabled_list;
  //  }

  // conn object is active :
  //  * readout enable == true
  //  * active         == true
  //  * not in given disable list

  inline bool enabled( const DisabledList &module_disabled_list, const PixLib::ModuleConnectivity *module_conn) {
    assert( module_disabled_list.type() == DisabledList::kModuleList );
    if (!module_conn || !module_conn->enableReadout ) return false;
    if ( !(module_disabled_list.enabled(connectivityName(module_conn)))) return false;
    return true;
  }

  inline bool enabled( const DisabledList &module_disabled_list, const ModuleList::const_iterator module_iter) {
    assert( module_disabled_list.type() == DisabledList::kModuleList );
    return enabled( module_disabled_list, *module_iter );
  }

  inline DisabledList moduleDisabledList( const DisabledList &pp0_disabled_list, const PixLib::Pp0Connectivity */*pp0_conn*/) {
    assert( pp0_disabled_list.type() == DisabledList::kPp0List) ;
    return pp0_disabled_list.getChildList();
  }

  inline DisabledList moduleDisabledList( const DisabledList &pp0_disabled_list, const Pp0List::const_iterator pp0_iter) {
    return moduleDisabledList( pp0_disabled_list, *pp0_iter );
  }

  inline bool enabled( const DisabledList &pp0_disabled_list, const PixLib::Pp0Connectivity *pp0_conn) {
    assert( pp0_disabled_list.type() ==DisabledList::kPp0List );
    if (!pp0_conn) return false;
    if (!(pp0_disabled_list.enabled(connectivityName(pp0_conn)))) return false;
    return true;
  }

  inline bool enabled( const DisabledList &pp0_disabled_list, const Pp0List::const_iterator pp0_iter) {
    assert( pp0_disabled_list.type() ==DisabledList::kPp0List );
    return enabled( pp0_disabled_list, *pp0_iter );
  }

  inline DisabledList pp0DisabledList( const DisabledList &rod_disabled_list, const PixLib::RodBocConnectivity *rod_conn) {
    assert( rod_disabled_list.type() ==DisabledList::kRodList );
    return (rod_disabled_list.isEnableMap() ?
	      rod_disabled_list.getChildList( PixA::connectivityName(rod_conn) ) 
	    : rod_disabled_list.getChildList());
  }

  inline DisabledList pp0DisabledList( const DisabledList &rod_disabled_list, const RodList::const_iterator rod_iter) {
    return pp0DisabledList( rod_disabled_list, *rod_iter );
  }


  inline bool enabled( const DisabledList &rod_disabled_list, const PixLib::RodBocConnectivity *rod_conn) {
    assert( rod_disabled_list.type() ==DisabledList::kRodList );
    if (!rod_conn || !rod_conn->enableReadout) return false;
    if (!(rod_disabled_list.enabled(connectivityName(rod_conn)))) return false;
    return true;
  }

  inline bool enabled( const DisabledList &rod_disabled_list, const RodList::const_iterator rod_iter) {
    assert( rod_disabled_list.type() ==DisabledList::kRodList );
    return enabled( rod_disabled_list, *rod_iter );
  }

  inline DisabledList rodDisabledList( const DisabledList &crate_disabled_list, const PixLib::RodCrateConnectivity */*crate_conn*/) {
    assert( crate_disabled_list.type() ==DisabledList::kCrateList );
    return crate_disabled_list.getChildList();
  }

  inline DisabledList rodDisabledList( const DisabledList &crate_disabled_list, const CrateList::const_iterator crate_iter) {
    return rodDisabledList( crate_disabled_list, *crate_iter );
  }

#ifdef _PIXLIB_TIMCONNOBJ
  inline bool timEnabled( const DisabledList &crate_disabled_list, const CrateList::const_iterator crate_iter) {
    assert( crate_disabled_list.type() ==DisabledList::kRodList );
    const PixLib::TimConnectivity *tim_conn = PixA::responsibleTim(*crate_iter);
    if (tim_conn) {
      if ( crate_disabled_list.enabled(connectivityName(tim_conn)) ) return true;
    }
    return false;
  }
#endif

  // this list is needed to find out whether a ROD is enabled or not
  inline DisabledList rodDisabledList( const DisabledListRoot &disabled_list_root, const PixLib::RodBocConnectivity *rod_conn)
  {
    assert( disabled_list_root.type() ==DisabledList::kCrateList );
    const PixLib::RodCrateConnectivity *crate_conn = NULL;
    if (rod_conn)  {
      crate_conn = PixA::crate(rod_conn);
    }
    if (!crate_conn) {
      std::stringstream message;
      message << "No crate assigned to ROD " << PixA::connectivityName(rod_conn) << ".";
      throw PixA::ConfigException(message.str());
    }
    return rodDisabledList(disabled_list_root, crate_conn);
  }

  inline bool enabled( const DisabledList &crate_disabled_list, const PixLib::RodCrateConnectivity *crate_conn) {
    assert( crate_disabled_list.type() ==DisabledList::kCrateList );
    if (!crate_conn || !crate_conn->enableReadout) return false;
    if (!(crate_disabled_list.enabled(connectivityName(crate_conn)))) return false;
    if (!(crate_disabled_list.getParentList().enabled(PixA::partitionName(crate_conn) )) ) return false;
    return true;
  }

  inline bool enabled( const DisabledList &crate_disabled_list, const CrateList::const_iterator crate_iter) {
    assert( crate_disabled_list.type() ==DisabledList::kCrateList );
    return enabled( crate_disabled_list, *crate_iter );
  }

  inline DisabledList crateDisabledList( const DisabledList &disabled_list) {
    assert( disabled_list.type() <= DisabledList::kCrateList );
    return disabled_list;
  }

  EnableMap_t createEnableMap(const std::string &pix_disable_string);

}

#endif
