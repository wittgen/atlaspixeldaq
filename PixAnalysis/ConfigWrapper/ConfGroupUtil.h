#ifndef _ConfGroupUtil_h_
#define _ConfGroupUtil_h_

#include <Config/ConfGroup.h>
#include <Config/Config.h>
//#include <Config/ConfObj.h>
class ConfObj;
#include <sstream>

inline const PixLib::ConfObj &confElm(const PixLib::ConfGroup &conf_group, const std::string &name, unsigned int index) {
  std::stringstream full_name;
  full_name << name << index;
  return const_cast<PixLib::ConfGroup &>(conf_group)[full_name.str()];
}

inline const PixLib::Config &subConfig(const PixLib::Config &config, const std::string &name, unsigned int index) {
  std::stringstream full_name;
  full_name << name << index;
  return const_cast<PixLib::Config &>(config).subConfig(full_name.str());
}


inline const PixLib::ConfGroup &confGroup(const PixLib::Config &config, const std::string &name) {
  return const_cast<PixLib::Config &>(config)[name];
}

bool updateConfList(PixLib::Config &dest, const PixLib::Config &src);
bool updateConfList(PixLib::ConfGroup &dest, const PixLib::ConfGroup &src);

#endif
