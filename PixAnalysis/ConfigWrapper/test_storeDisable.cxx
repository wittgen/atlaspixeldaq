#include "ConnObjConfigDb.h"

#include "ConnectivityManager.h"

#include "createDbServer.h"
#include "PixConnObjConfigDb.h"

// #include <Config/Config.h>
//#include <Config/ConfGroup.h>

#include "DbManager.h"
//#include <RootDb/RootDb.h>
#include "ConfGrpRefUtil.h"

std::map<std::string, PixA::CfgLocation_t::EBackEnd> s_backEnd;

class IPCInstance
{

 private:
  IPCInstance(int argc, char **argv, const std::string &partition_name) {
    IPCCore::init(argc, argv);
    m_ipcPartition=std::make_shared<IPCPartition>(partition_name.c_str());

    if (!m_ipcPartition->isValid()) {
      std::stringstream message;
      message << "ERROR [IPCInstance] Not a valid partition " << partition_name << std::endl;
      throw std::runtime_error(message.str());
    }
  }
 public:

  static IPCInstance *instance(int argc, char **argv, const std::string &partition_name) {
    if (!s_instance) {
      s_instance = new IPCInstance(argc,argv,partition_name);
    }
    return s_instance;
  }

  IPCPartition &partition() { return *m_ipcPartition;}
  
 private:
  std::shared_ptr<IPCPartition> m_ipcPartition;
  static IPCInstance *s_instance;

};

IPCInstance *IPCInstance::s_instance = NULL;


std::shared_ptr<PixLib::PixDbServerInterface> dbServer(const std::string &db_server_partition_name,
		 const std::string &db_server_name,
		 int argc,
		 char **argv)
{
  int temp_argc=0;
  char *temp_argv[1]={NULL};
  IPCInstance *ipc_instance = IPCInstance::instance(temp_argc,temp_argv, db_server_partition_name);

  std::shared_ptr<PixLib::PixDbServerInterface> db_server( PixA::createDbServer(ipc_instance->partition(),db_server_name) );
  if (db_server.get()) {
    PixA::ConnectivityManager::useDbServer(db_server);
  }
  return db_server;
}

PixA::CfgLocation_t::EBackEnd parseBackEnd( const std::string &back_end_name) {
  if (s_backEnd.empty()) {
    s_backEnd.insert(std::make_pair(std::string("DbServer"),PixA::CfgLocation_t::kDbServer));
    s_backEnd.insert(std::make_pair(std::string("dbserver"),PixA::CfgLocation_t::kDbServer));
    s_backEnd.insert(std::make_pair(std::string("db-server"),PixA::CfgLocation_t::kDbServer));
    s_backEnd.insert(std::make_pair(std::string("DBSERVER"),PixA::CfgLocation_t::kDbServer));
    s_backEnd.insert(std::make_pair(std::string("DB-SERVER"),PixA::CfgLocation_t::kDbServer));
    s_backEnd.insert(std::make_pair(std::string("ConfigRootIO"),PixA::CfgLocation_t::kConfigRootIO));
    s_backEnd.insert(std::make_pair(std::string("RootNew"),PixA::CfgLocation_t::kConfigRootIO));
    s_backEnd.insert(std::make_pair(std::string("file"),PixA::CfgLocation_t::kConfigRootIO));
    s_backEnd.insert(std::make_pair(std::string("FILE"),PixA::CfgLocation_t::kConfigRootIO));
  }
  std::map<std::string, PixA::CfgLocation_t::EBackEnd>::const_iterator back_end_iter = s_backEnd.find(back_end_name);
  if (back_end_iter == s_backEnd.end()) {
    std::stringstream message;
    message << "FATAL [parseBackEnd] Unknown back-end : " << back_end_name;
    std::cout << message.str() << std::endl;
    throw std::runtime_error(message.str());
  }
  return back_end_iter->second;
}

int main(int argc, char **argv)
{

  std::string id_name="CT";
  std::string cfg_tag_name;
  PixA::Revision_t rev=0;
  bool error=false;
  bool simulation=false;
  unsigned int verbose=1;
  unsigned int revision_offset=0;
  std::vector<std::string> file_name_list;

  PixA::CfgLocation_t::EBackEnd store_back_end=PixA::CfgLocation_t::kDbServer;

  std::shared_ptr<PixLib::PixDbServerInterface> db_server;
  time_t current_time = time(0);
  rev = current_time;
  for (int arg_i=1; arg_i<argc; arg_i++) {

    if (strcmp(argv[arg_i],"--db-server")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-' ) {

      try {
	std::string db_server_partition_name = argv[++arg_i];
	std::string db_server_name = ( (arg_i+1<argc && argv[arg_i+1][0]!='-') ? argv[++arg_i] : "PixelDbServer");
	db_server = dbServer(db_server_partition_name, db_server_name, argc, argv);
      }
      catch (std::runtime_error &err) {
	std::cout << "FATAL [main] caught exception (std::exception) : " << err.what() << std::endl;
	return -1;
      }

    }
    else if (strcmp(argv[arg_i],"-i")==0 && arg_i+1<argc) {
      id_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-t")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      cfg_tag_name = argv[++arg_i];
      if (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	rev=atoi(argv[++arg_i]);
      }
    }
    else if (strcmp(argv[arg_i],"--store-back-end")==0 && arg_i+1<argc) {
      try {
	store_back_end = parseBackEnd( argv[++arg_i]);
	//	store=true;
      }
      catch(std::exception &) {
	std::cout << "INFO [" << argv[0] << ":main] invalid back-end : " << argv[arg_i] << "." << std::endl;
      }
    }
    else if (strcmp(argv[arg_i],"-f")==0 && arg_i+1<argc) {
      file_name_list.push_back(argv[++arg_i]);
    }
    else if (strcmp(argv[arg_i],"-r")==0 && arg_i+1<argc) {
      revision_offset = atoi(argv[++arg_i]);
    }
    else if (strcmp(argv[arg_i],"-v")==0 || strcmp(argv[arg_i],"--verbose")==0) {
      verbose++;
    }
    else if (strcmp(argv[arg_i],"-s")==0) {
      simulation=true;
    }
    else {
      std::cout << "INFO [" << argv[0] << ":main] Unhandled argument : " << argv[arg_i] << "." << std::endl;
      error=true;
    }
  }
  if (file_name_list.empty() || id_name.empty() || cfg_tag_name.empty()) {
    error=true;
  }
  if (error) {
    std::cout << "USAGE " << argv[0] << "--db-server partition [name] -i id-tag -t tag [rev] -f disable-file-name [-f ...] [-v [-v [...] ] ]" << std::endl
	      << "\t [--dest-tag tag [rev] ]" << std::endl;
    return -1;
  }

  unsigned int n_errors=0;
  for (std::vector<std::string>::const_iterator file_iter = file_name_list.begin();
       file_iter != file_name_list.end();
       ++file_iter) {

    try {
    std::string::size_type pos = file_iter->rfind(".");
    if (pos == std::string::npos || pos<4) {
      std::cout << "ERROR [main:" << argv[0] << "] File name " << *file_iter << " without extension. Doubt that it is the file name of a PixDisable." << std::endl;
      ++n_errors;
      continue;
    }
    pos-=4;
    std::string::size_type end_pos =pos+1;
    while (pos>0 && isdigit((*file_iter)[pos])) --pos;
    if ((*file_iter)[pos]!='_') {
      std::cout << "ERROR [main:" << argv[0] << "] Expected the revision number to be separated by '_' in  " << *file_iter << " ." << std::endl;
      ++n_errors;
      continue;
    }
    if (!isdigit((*file_iter)[pos])) ++pos;

    if (!isdigit((*file_iter)[pos])) {
      std::cout << "ERROR [main:" << argv[0] << "] File name " << *file_iter << " without revision extension. Doubt that it is the file name of a PixDisable." << std::endl;
      ++n_errors;
      continue;
    }
    unsigned int disable_rev = static_cast<unsigned int>( atoi((&((file_iter->substr(pos,end_pos-pos))[0]))) );
    if (disable_rev==0) {
      std::cout << "ERROR [main:" << argv[0] << "] Failed to extract revision from file name " << *file_iter << "." << std::endl;
      ++n_errors;
      continue;
    }
    std::string::size_type start_pos = file_iter->rfind("/",pos);
    if (start_pos == std::string::npos) start_pos =0;
    else {
      ++start_pos;
    }
    if (start_pos+1>=pos) {
      std::cout << "ERROR [main:" << argv[0] << "] Failed to extract disable name from file name " << *file_iter << "." << std::endl;
      continue;
    }
    std::string disable_name = file_iter->substr(start_pos, pos-start_pos-1);

    std::vector<std::string> record_hierarchy;
    record_hierarchy.push_back(disable_name);

    PixA::DbHandle  db_handle = PixA::DbManager::dbHandle(*file_iter,record_hierarchy);
    PixA::DbRef db_ref(db_handle.ref());

    std::shared_ptr<PixLib::PixDisable> disable_obj(new PixLib::PixDisable(disable_name));
    disable_obj->config().read(db_ref.record().get());

//     PixA::ConfigHandle disable_handle(PixA::DbManager::wrapConfigRecord(db.get(),disable_iter));
//     PixA::ConfigRef  disable_ref( disable_handle.ref() );
//     PixA::ConfigHandle full_disable_handle( disable_handle.ref().createFullCopy() );
//     PixA::ConfigRef  full_disable_ref(  full_disable_handle.ref() );
    if (verbose>3) {
      const_cast<PixLib::Config &>(disable_obj->config()).dump(std::cout);
    }

    PixA::ConnObjConfigDb cfg_db(PixA::PixConnObjConfigDbManager::createInstance( db_server, store_back_end));

    PixA::ConnObjConfigDbTagRef a_tag( cfg_db.getTag(id_name, cfg_tag_name, rev) );
    if (verbose>0) {
      std::cout << "INFO [main:" << argv[0] << "] Store disable " << disable_name << " of file " << *file_iter << " in DbServer tag="
		<< id_name << ":" << cfg_tag_name << ";" << disable_rev+revision_offset
		<< "." << std::endl;
    }
    if (!simulation) {
      a_tag.storeConfig(disable_obj->config(), disable_rev+revision_offset, "disableLoader_Tmp", false );
    }
    else {
      std::cout << "INFO [main:" << argv[0] << "] Only simulated writing of  " << disable_name << " of file " << *file_iter << " in DbServer tag="
		<< id_name << ":" << cfg_tag_name << ";" << disable_rev+revision_offset
		<< "." << std::endl;
    }
    }
    catch(PixA::BaseException &err) {
      std::cout << "ERROR [main:" << argv[0] << "] Caught exception while processing  " << *file_iter << " : " << err.getDescriptor() << "." << std::endl;
      ++n_errors;
    }
    catch(std::exception &err) {
      std::cout << "ERROR [main:" << argv[0] << "] Caught exception while processing  " << *file_iter << " : " << err.what() << "." << std::endl;
      ++n_errors;
    }
  }
  if (n_errors>0) {
    std::cout << "ERROR [main:" << argv[0] << "] " << n_errors << " occurred while processing the " << file_name_list.size() << " file(s)." << std::endl;
  }
  return 0;
}

