#ifndef _PixA_setDbServer_h_
#define _PixA_setDbServer_h_

#include <memory>

class IPCPartition;
namespace PixLib {
  class PixDbServerInterface;
}

namespace PixA {

  std::shared_ptr<PixLib::PixDbServerInterface> createDbServer(IPCPartition &db_server_partition, const std::string &db_server_name);

}

#endif
