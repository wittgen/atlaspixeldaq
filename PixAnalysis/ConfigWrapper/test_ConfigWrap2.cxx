#include "DbManager.h"
#include <RootDb/RootDb.h>
#include "ConfGrpRefUtil.h"
#include "Connectivity.h"
#include <PixModule/PixModule.h>

std::string stripGroupName(const std::string &full_name) {
  std::string::size_type pos=full_name.find("_");
  if (pos!=std::string::npos) {
    return std::string(full_name,pos+1,full_name.size()-pos-1);
  }
  return full_name;
}

template <class T> bool operator==(PixLib::ConfObj &a, PixLib::ConfObj &b);

template <class T> bool compare(PixLib::ConfObj &a, PixLib::ConfObj &b);

template<> inline bool compare<int>(PixLib::ConfObj &a, PixLib::ConfObj &b) {
  ConfInt &a_int=reinterpret_cast<ConfInt&>(a);
  ConfInt &b_int=reinterpret_cast<ConfInt&>(b);
  if (a_int.subtype() != b_int.subtype()) {
    std::cout << "INFO [compare<int>(PixLib::ConfObj &a, PixLib::ConfObj &b)] sub types differ for \"" << a_int.name() << "\" / " << a_int.type()
	      << " : " << a_int.subtype() << " != " << b_int.subtype() << std::endl;
  }
  if (a_int.value() != b_int.value()) {
    std::cout << "INFO [compare<int>(PixLib::ConfObj &a, PixLib::ConfObj &b)] values differ for \"" << a_int.name() << "\" / " << a_int.type()
	      << " : " << a_int.value() << " != " << b_int.value() << std::endl;
  }
  return ( a_int.value() == b_int.value() && a_int.subtype() == b_int.subtype());
}


template<> inline bool compare<float>(PixLib::ConfObj &a, PixLib::ConfObj &b) {
  ConfFloat &a_float=reinterpret_cast<ConfFloat&>(a);
  ConfFloat &b_float=reinterpret_cast<ConfFloat&>(b);
  return ( a_float.value() == b_float.value());
}


template<> inline bool compare<std::string>(PixLib::ConfObj &a, PixLib::ConfObj &b) {
  ConfString &a_string=reinterpret_cast<ConfString&>(a);
  ConfString &b_string=reinterpret_cast<ConfString&>(b);
  return ( a_string.value() == b_string.value());
}

template<> inline bool compare<bool>(PixLib::ConfObj &a, PixLib::ConfObj &b) {
  ConfBool &a_bool=reinterpret_cast<ConfBool&>(a);
  ConfBool &b_bool=reinterpret_cast<ConfBool&>(b);
  return ( a_bool.value() == b_bool.value());
}

bool compareVector(PixLib::ConfObj &a, PixLib::ConfObj &b) {
  ConfVector &a_vector=reinterpret_cast<ConfVector&>(a);
  ConfVector &b_vector=reinterpret_cast<ConfVector&>(b);

  bool ret=true;
  if (a_vector.subtype() != b_vector.subtype()) {
    std::cout << "INFO [compareVector(PixLib::ConfObj &a, PixLib::ConfObj &b)] sub types differ for \"" << a_vector.name() << "\" / " << a_vector.type()
	      << " : " << a_vector.subtype() << " != " << b_vector.subtype() << std::endl;
    ret &=false;
  }

  switch (a_vector.subtype()) {
  case PixLib::ConfVector::V_INT: {
    ret &= (a_vector.valueVInt() == b_vector.valueVInt());
    break;
  }
  case PixLib::ConfVector::V_UINT: {
    ret &= (a_vector.valueVUint() == b_vector.valueVUint());
    break;
  }
  case PixLib::ConfVector::V_FLOAT: {
    ret &= (a_vector.valueVFloat() == b_vector.valueVFloat());
    break;
  }
  default: {
    return false;
  }
  }
  return ret;
  //  return false;
}

template <class T> bool operator==(const std::vector<std::vector<T> > &a, const std::vector<std::vector<T> > &b)
{
  
  if  ( a.size() != b.size() ) {
    std::cout << "ERROR [operator==(const std::vector<std::vector<T> > &a, const std::vector<std::vector<T> > &b)] sizes differ : "
	      << a.size() << " != " << b.size() << std::endl;
    return false;
  }
  typename std::vector< std::vector<T> >::const_iterator b_iter = b.begin();
  bool ret=true;
  unsigned int col_i=0;
  for (typename std::vector< std::vector<T> >::const_iterator a_iter = a.begin();
       a_iter != a.end();
       a_iter++, b_iter++,col_i++) {
      assert ( b_iter != b.end() );
      //      assert (a_iter->size() == b_iter->size());
      if  ( a_iter->size() != b_iter->size() ) {
	std::cout << "ERROR [operator==(const std::vector<std::vector<T> > &a, const std::vector<std::vector<T> > &b)] sizes differ in column "
		  << col_i << " / " << a.size() << " : " 
		  << a_iter->size() << " != " << b_iter->size() << std::endl;
      }
  }
  return ret;
}

bool compareMatrix(PixLib::ConfObj &a, PixLib::ConfObj &b) {
  ConfMatrix &a_matrix=reinterpret_cast<ConfMatrix&>(a);
  ConfMatrix &b_matrix=reinterpret_cast<ConfMatrix&>(b);

  bool ret=true;
  if (a_matrix.subtype() != b_matrix.subtype()) {
    std::cout << "INFO [compareMatrix(PixLib::ConfObj &a, PixLib::ConfObj &b)] sub types differ for \"" << a_matrix.name() << "\" / " << a_matrix.type()
	      << " : " << a_matrix.subtype() << " != " << b_matrix.subtype() << std::endl;
    ret &=false;
    return false;
  }


  switch (a_matrix.subtype()) {
  case PixLib::ConfMatrix::M_U1: {
    std::vector<bool> a_vec;
    std::vector<bool> b_vec;
    assert ( a_matrix.valueU1().ncol() == b_matrix.valueU1().ncol() );
    assert ( a_matrix.valueU1().nrow() == b_matrix.valueU1().nrow() );
    a_matrix.valueU1().get(a_vec);
    b_matrix.valueU1().get(b_vec);
      
    //    a_matrix.valueU1().get(a_vec);
    //    b_matrix.valueU1().get(b_vec);
    ret &= ( a_vec == b_vec );
    break;
  }
  case PixLib::ConfMatrix::M_U16: {
    std::vector<TrimVal_t> a_vec;
    std::vector<TrimVal_t> b_vec;
    assert ( a_matrix.valueU16().ncol() == b_matrix.valueU16().ncol() );
    assert ( a_matrix.valueU16().nrow() == b_matrix.valueU16().nrow() );
    a_matrix.valueU16().get(a_vec);
    b_matrix.valueU16().get(b_vec);
    ret &= ( a_vec == b_vec );
    break;
  }
  default: {
    return false;
  }
  }
  return ret;
}





bool operator==(PixLib::ConfObj &a, PixLib::ConfObj &b) {
  if (a.type()==PixLib::ConfObj::STRING &&  b.type()==PixLib::ConfObj::LIST) {
    //    std::cout << "WARNING [operator==(PixLib::ConfObj &, PixLib::ConfObj &)] list / int  : \"" 
    //	      << a.name() << "\" / " << a.type() << " != \"" 
    //	      << b.name() << "\" / " << b.type() << "." << std::endl;
    PixLib::ConfString &a_string = static_cast<PixLib::ConfString &>(a);
    PixLib::ConfList &b_list     = static_cast<PixLib::ConfList &>(b);
    return a_string.value() == b_list.sValue();
  }
  else if (b.type()==PixLib::ConfObj::STRING &&  a.type()==PixLib::ConfObj::LIST) {
    //    std::cout << "WARNING [operator==(PixLib::ConfObj &, PixLib::ConfObj &)] list / int  : \"" 
    //	      << a.name() << "\" / " << a.type() << " != \"" 
    //	      << b.name() << "\" / " << b.type() << "." << std::endl;
    PixLib::ConfString &b_string = static_cast<PixLib::ConfString &>(b);
    PixLib::ConfList &a_list     = static_cast<PixLib::ConfList &>(a);
    return b_string.value() == a_list.sValue();
  }
  else if (b.type()==PixLib::ConfObj::STRING &&  a.type()==PixLib::ConfObj::LIST) {
    std::cout << "WARNING [operator==(PixLib::ConfObj &, PixLib::ConfObj &)] list / int  : \"" 
	      << a.name() << "\" / " << a.type() << " != \"" 
	      << b.name() << "\" / " << b.type() << "." << std::endl;
  }
  else if ( a.type()  != b.type()) {
    std::cout << "ERROR [operator==(PixLib::ConfObj &, PixLib::ConfObj &)] type mismatch : \"" 
	      << a.name() << "\" / " << a.type() << " != \"" 
	      << b.name() << "\" / " << b.type() << "." << std::endl;
    return false;
  }

  bool ret=true;
  if ( a.name()  != b.name()) {
    std::cout << "ERROR [operator==(PixLib::ConfObj &, PixLib::ConfObj &)] name mismatch : " << a.name() << " != " << b.name() << "." << std::endl;
    ret = false;
  }
  

  switch (a.type()) {
  case PixLib::ConfObj::LIST:
  case PixLib::ConfObj::INT: {
    ret &= compare<int>(a,b);
    break;
  }
  case PixLib::ConfObj::FLOAT: {
    ret &= compare<float>(a,b);
    break;
  }
  case PixLib::ConfObj::BOOL: {
    ret &=compare<bool>(a,b);
    break;
  }
  case PixLib::ConfObj::STRING: {
    ret &= compare<std::string>(a,b);
    break;
  }
  case PixLib::ConfObj::VECTOR: {
    ret &= compareVector(a,b);
    break;
  }
  case PixLib::ConfObj::MATRIX: {
    ret &= compareMatrix(a,b);
    break;
  }
  default: {
    std::cout << "ERROR [operator==(PixLib::ConfObj &a, PixLib::ConfObj &b)] trying to compare unsupported type : "
	      << a.name() << " / " << a.type() << std::endl;
    return false;
  }
    
  }
  return ret;
}

inline bool operator!=(PixLib::ConfObj &a, PixLib::ConfObj &b) {
  return !(a==b);
}


bool operator==(PixLib::ConfGroup &a, PixLib::ConfGroup &b)
{
  bool ret=true;
  if (a.size() != b.size()) {
    std::cout << "ERROR [operator==(PixLib::ConfGroup &a, PixLib::ConfGroup &b)] conf groups " << a.name() << " and " << b.name() << " have different sizes " << a.size() << " != " << b.size() << std::endl;
    ret&=false;
  }
  if (a.name() != b.name()) {
    std::cout << "ERROR [operator==(PixLib::ConfGroup &a, PixLib::ConfGroup &b)] conf groups have different names " << a.name() << " != " << b.name() << std::endl;
    ret&=false;
  }
  for (size_t i=0; i<a.size(); i++) {
    PixLib::ConfObj &bo=b[stripGroupName(a[i].name())];
    if (a[i] != bo) {
      std::cout << "ERROR [operator==(PixLib::ConfObj &a, PixLib::ConfObj &b)] conf objects \"" << a[i].name() << "\" / " << a[i].type() << " of group " << a.name() << " differ " << std::endl;
      ret &= false;
    }
  }
  return ret;
}

bool operator!=(PixLib::ConfGroup &a, PixLib::ConfGroup &b)
{
  return !(a==b);
}

bool operator==(PixLib::Config &a, PixLib::Config &b) ;

inline bool operator!=(PixLib::Config &a, PixLib::Config &b) {
  return !(a==b);
}


bool operator==(PixLib::Config &a, PixLib::Config &b) 
{
  bool ret=true;
  if (a.size() != b.size()) {
    std::cout << "ERROR [operator==(PixLib::Config &a, PixLib::Config &b)] configs " << a.name() << " and " << b.name() << " have different sizes " << a.size() << " != " << b.size() << std::endl;
    ret&=false;
  }
  if (a.name() != b.name()) {
    std::cout << "ERROR [operator==(PixLib::Config &a, PixLib::Config &b)] configs have different names " << a.name() << " != " << b.name() << std::endl;
    //    return false;
    ret&=false;
  }
  if (a.type() != b.type()) {
    std::cout << "INFO [operator==(PixLib::Config &a, PixLib::Config &b)] configs " << a.name() << " have different types " << a.type() << " != " << b.type() << std::endl;
    ret&=false;
  }
  for (unsigned int i=0; i<a.size(); i++) {
    PixLib::ConfGroup &bg=b[a[i].name()];
    if (a[i] != bg) {
      std::cout << "INFO [operator==(PixLib::Config &a, PixLib::Config &b)] conf groups " << a[i].name() << " of config " << a.name() << " differ " << std::endl;
      ret &= false;
    }
  }
  if (a.subConfigSize() != b.subConfigSize()) {
    std::cout << "INFO [operator==(PixLib::Config &a, PixLib::Config &b)] configs have different sizes " << a.size() << " != " << b.size() << std::endl;
    ret &= false;
  }
  for (unsigned int i=0; i<a.subConfigSize(); i++) {
    PixLib::Config &bs=b.subConfig(a.subConfig(i).name());
    if (a.subConfig(i) != bs) {
      std::cout << "INFO [operator==(PixLib::Config &a, PixLib::Config &b)] conf groups " << a.subConfig(i).name() << " of config " << a.name() << " differ " << std::endl;
      ret &= false;
    }
  }
  return ret;
}

void read_vec(PixLib::DbRecord *root_record, PixLib::dbFieldIterator &field_iter)
  {
    std::vector<bool> temp;
    PixLib::dbFieldIterator temp_field_iter (root_record->getDb()->DbProcess(field_iter,PixLib::PixDb::DBREAD,temp));
    //    field_iter.releaseMem();
    temp_field_iter.releaseMem();
    std::cout << "INFO [main] got vector of " << temp.size() << " elements." << std::endl;
  }

int main(int argc, char ** argv)
{
  if (argc<3) return -1;

  for (int loop=0; loop<3; loop++) {
  std::shared_ptr<PixLib::PixDbInterface> db(new PixLib::RootDb(argv[1],"READ"));
  std::shared_ptr<PixLib::DbRecord> root_record(db->readRootRecord());

  unsigned int arg_i=2;
  PixLib::dbRecordIterator iter;
  while (arg_i<unsigned(argc)) {
    iter = root_record->findRecord(argv[arg_i]);
    if (iter == root_record->recordEnd()) {
      std::cout << "INFO [main] record = " << argv[arg_i] << " does not exist." << std::endl;
      return -1;
    }
    if ((arg_i+1)==unsigned(argc)) break;

    root_record = std::shared_ptr<PixLib::DbRecord>(*root_record->getDb()->DbProcess(iter, PixLib::PixDb::DBREAD));
    std::cout << "INFO [main] record = " << root_record->getName();
    arg_i++;
  }

  std::cout << "INFO [main] wrap record " << argv[arg_i] << "." << std::endl;
  {
    std::cout << " root_record " << static_cast<void *>(root_record.get()) << std::endl;
    if (iter == root_record->recordEnd()) {
      std::cout << "INFO [main] record = " << argv[arg_i] << " does not exist." << std::endl;
      return -1;
    }
    
    std::shared_ptr<PixLib::DbRecord> b_record(*(db->DbProcess(iter, PixLib::PixDb::DBREAD)));
    root_record = b_record;
    std::cout << "INFO [main] now in  " <<  root_record->getName() << "." << std::endl;
    std::vector<std::string> rec_list;
    rec_list.push_back("PixFe_0");
    rec_list.push_back("PixelRegister_0");
    for (std::vector<std::string>::const_iterator name_iter=rec_list.begin();
	 name_iter != rec_list.end();
	 name_iter++) {
      iter = root_record->findRecord(*name_iter);
      if (iter == root_record->recordEnd()) {
	std::cout << "INFO [main] record = " << *name_iter << " does not exist." << std::endl;
	return -1;
      }

      std::shared_ptr<PixLib::DbRecord> a_record(*(db->DbProcess(iter, PixLib::PixDb::DBREAD)));
      std::cout << "INFO [main] record = " << a_record->getName() << std::endl;
      root_record = a_record;
    }

    //    std::shared_ptr<PixLib::DbRecord> a_record(*(db->DbProcess(iter, PixLib::PixDb::DBREAD)));
    std::string field_name ("PixelRegister_ENABLE");
    PixLib::dbFieldIterator field_iter = root_record->findField( field_name );
    std::cout << "INFO [main] search field " << field_name << "." << std::endl;
    if (field_iter != root_record->fieldEnd()) {
      std::cout << "INFO [main] found field " << field_name << "." << std::endl;
      //std::shared_ptr<PixLib::DbField> the_field ( *(root_record->getDb()->DbProcess(field_iter,PixLib::PixDb::DBREAD)) );
      //std::cout << "INFO [main] read field " << the_field->getName() << "." << std::endl;
      read_vec(root_record.get(), field_iter);
    }
  }
  }
    return -1;
  unsigned int arg_i=2;
  std::shared_ptr<PixLib::PixDbInterface> db(new PixLib::RootDb(argv[1],"READ"));
  std::shared_ptr<PixLib::DbRecord> root_record(db->readRootRecord());
  PixLib::dbRecordIterator iter;

  try {
    PixA::ConfigHandle conf_handle=PixA::DbManager::wrapConfigRecord(db.get(),iter);
    PixA::ConfigRef conf_ref( conf_handle.ref() );
    const PixLib::Config &a_const_config  = conf_ref.config();
    PixLib::Config &a_config  = const_cast<PixLib::Config &>(a_const_config);
    //const PixLib::ConfObj &a_const_dummy = conf_ref.subConfig("PixFe_8").subConfig("PixelRegister_0")["PixelRegister"]["ENABLE"];
    //PixLib::ConfObj &a_dummy = const_cast<PixLib::ConfObj &>(a_const_dummy);

    std::shared_ptr<PixLib::DbRecord> a_record(*root_record->getDb()->DbProcess(iter, PixLib::PixDb::DBREAD));
    PixLib::PixModule dummy(a_record.get(),NULL,argv[arg_i]);
    //    PixLib::Config b_config(argv[arg_i]);
    PixLib::Config &b_config=dummy.config();
    
    std::cout << " record = " << a_record->getDecName() << std::endl;
    //    b_config.read(a_record.get());

    // write config a
    {
      std::shared_ptr<PixLib::PixDbInterface> db_outa(new PixLib::RootDb("/tmp/test_a.root","RECREATE"));
      std::shared_ptr<PixLib::DbRecord> root_record_outa(db_outa->readRootRecord());
      std::shared_ptr<PixLib::DbRecord> config_record_outa(root_record_outa->addRecord(a_config.type(), a_config.name()));
      a_config.write(config_record_outa.get());
    }

    // write config b
    {
      std::shared_ptr<PixLib::PixDbInterface> db_outb(new PixLib::RootDb("/tmp/test_b.root","RECREATE"));
      std::shared_ptr<PixLib::DbRecord> root_record_outb(db_outb->readRootRecord());
      std::shared_ptr<PixLib::DbRecord> config_record_outb(root_record_outb->addRecord(b_config.type(), b_config.name()));
      b_config.write(config_record_outb.get());
    }

    if (b_config != a_config) {
      std::cout << "FATAL [main] configs differ." << std::endl;
      return -1;
    }
    b_config.dump(std::cout);  
    a_config.dump(std::cout);
  }
  catch (PixA::ConfigException &err) {
    std::cout << "FATAL [main] exception : " << err << std::endl;
    return -1;
  }
  return 0;
}
