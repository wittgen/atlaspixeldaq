/* Dear emacs, this is -*-c++-*- */
#ifndef _PixA_RxLinkInfo_t_h_
#define _PixA_RxLinkInfo_t_h_

#include "ConfigHandle.h"
#include "Connectivity.h"

namespace PixA {

  /** Auxillary class to extract the RX link information from the connectivity or the module config.
   */
  class RxLinkInfo_t
  {
  public:
    RxLinkInfo_t(const std::string &module_name,
		 const PixA::ConnectivityRef &connectivity,
		 const PixA::ConfigHandle &scan_config,
		 const PixA::ConfigHandle &module_config)
      : m_scanConfigHandle(scan_config),
	m_moduleConfigHandle(module_config),
	m_moduleConnectivity(NULL)
    {
      if (connectivity) {
	m_moduleConnectivity = connectivity.findModule(module_name);
      }
    }

    RxLinkInfo_t(const ModuleList::const_iterator &mod_iterator,
		 const PixA::ConfigHandle &scan_config)
      : m_scanConfigHandle(scan_config),
	m_moduleConnectivity( *mod_iterator )
    {}

    enum ERxLink {kDTO1,kDTO2 };

    /** Get the nominal RX  DTO1 and DTO2 links.
     * For some scans the DTO1 and DTO2 links were swapped. This swapping is not taken into account.
     * @sa inOrderBocLinkRx.
     */
    int bocLinkRx(ERxLink link) const;

    /** Exploit the alt link flag of the scan config to correctly order DTO1 and DTO2.
     * If the automatic swapping is not desired the method  @ref bocLinkRx could be used instead.
     */
    int inOrderBocLinkRx(ERxLink link) const;

  private:
    PixA::ConfigHandle    m_scanConfigHandle;
    PixA::ConfigHandle    m_moduleConfigHandle;

    const PixLib::ModuleConnectivity *m_moduleConnectivity;

    static int s_dtoLinkMap[2];
  };

}
#endif
