#include <Config/ConfObj.h>
#include <Config/ConfGroup.h>
#include <Config/Config.h>

#include "test_configSize.h"
#include <iomanip>

ConfSize_t get_size_base(PixLib::ConfObj &a, bool show) {
  ConfSize_t obj_size(0,sizeof(a)+(a.name().size()+a.comment().size())*sizeof(char) );
  if (show) {
      std::cout << "INFO [get_size_base(PixLib::ConfObj &,bool)] the ConfObj " << std::setw(30) << a.name() << " acounts for " << std::setw(9) << obj_size.dataSize() 
		<< " bytes of data and " << std::setw(9) << obj_size.textSize() << " bytes of text ." << std::endl;
  }
  return obj_size;
}

ConfSize_t get_size_int(PixLib::ConfObj &a, bool show) {
  ConfSize_t obj_size = get_size_base(a,false);
    if (show) {
      std::cout << "INFO [get_size_list(PixLib::ConfObj &,bool)] the ConfObj " << std::setw(30) << a.name() << " acounts for " << std::setw(9) << obj_size.dataSize() 
		<< " bytes of data and " << std::setw(9) << obj_size.textSize() << " bytes of text ." << std::endl;
    }
  obj_size += ConfSize_t(0,sizeof(PixLib::ConfInt) - sizeof(PixLib::ConfObj));
  PixLib::ConfInt *a_int=dynamic_cast<PixLib::ConfInt *>(&a);
  if (a_int) {
    unsigned long text_length=0;
    unsigned long data_length=0;
    switch(a_int->subtype()) {
    case PixLib::ConfInt::S32:
      data_length += sizeof(a_int->valueS32());
      break;
    case PixLib::ConfInt::U32:
      data_length += sizeof(a_int->valueU32());
      break;
    case PixLib::ConfInt::S16:
      data_length += sizeof(a_int->valueS16());
      break;
    case PixLib::ConfInt::U16:
      data_length += sizeof(a_int->valueU16());
      break;
    case PixLib::ConfInt::S8:
      data_length += sizeof(a_int->valueS8());
      break;
    case PixLib::ConfInt::U8:
      data_length += sizeof(a_int->valueU8());
      break;
    default: throw std::runtime_error("Invalid Config Subtype");
    }

    obj_size += ConfSize_t(data_length,text_length);
    if (show) {
      std::cout << "INFO [get_size_list(PixLib::ConfObj &,bool)] the ConfInt " << std::setw(30) << a.name() << " acounts for " << std::setw(9) << obj_size.dataSize() 
		<< " bytes of data and " << std::setw(9) << obj_size.textSize() << " bytes of text ." << std::endl;
    }
  }
  else {
    std::cout << "ERROR [get_size_list(PixLib::ConfObj &,bool)] not of expected type PixLib::ConfInt  : "
	      << a.name() << " / " << a.type() << std::endl;
  }
  return obj_size;
}

ConfSize_t get_size_list(PixLib::ConfObj &a, bool show) {
  ConfSize_t obj_size;
  PixLib::ConfList *a_list=dynamic_cast<PixLib::ConfList *>(&a);
  if (a_list) {
    obj_size += get_size_int(a,false);
    unsigned long text_length=0;
    const std::map<std::string, int> &symb=a_list->symbols();
    for (std::map<std::string, int>::const_iterator symb_iter=symb.begin();
	 symb_iter != symb.end();
	 symb_iter++) {
      text_length += symb_iter->first.size()+1 * sizeof(char);
      text_length += sizeof(symb_iter->second);
    }
    obj_size += ConfSize_t(0,text_length+sizeof(*a_list)-sizeof(PixLib::ConfInt));
    if (show) {
      std::cout << "INFO [get_size_list(PixLib::ConfObj &,bool)] the ConfList " << std::setw(30) << a.name() << " acounts for " << std::setw(9) << obj_size.dataSize() 
		<< " bytes of data and " << std::setw(9) << obj_size.textSize() << " bytes of text ." << std::endl;
    }
  }
  else {
    std::cout << "ERROR [get_size_list(PixLib::ConfObj &,bool)] not of expected type PixLib::ConfList  : "
	      << a.name() << " / " << a.type() << std::endl;
  }
  return obj_size;
}

ConfSize_t get_size_float(PixLib::ConfObj &a, bool show) {
  ConfSize_t obj_size = get_size_base(a,false);
  obj_size += ConfSize_t(0,sizeof(PixLib::ConfFloat) - sizeof(PixLib::ConfObj));
  PixLib::ConfFloat *a_float=dynamic_cast<PixLib::ConfFloat *>(&a);
  if (a_float) {
    unsigned long text_length=0;
    unsigned long data_length=0;
    data_length += sizeof(a_float->value());

    obj_size += ConfSize_t(data_length,text_length);
    if (show) {
      std::cout << "INFO [get_size_float(PixLib::ConfObj &,bool)] the ConfFloat " << std::setw(30) << a.name() << " acounts for " << std::setw(9) << obj_size.dataSize() 
		<< " bytes of data and " << std::setw(9) << obj_size.textSize() << " bytes of text ." << std::endl;
    }
  }
  else {
    std::cout << "ERROR [get_size_float(PixLib::ConfObj &,bool)] not of expected type PixLib::ConfFloat  : "
	      << a.name() << " / " << a.type() << std::endl;
  }
  return obj_size;
}

ConfSize_t get_size_bool(PixLib::ConfObj &a, bool show) {
  ConfSize_t obj_size = get_size_base(a,false);
  obj_size += ConfSize_t(0,sizeof(PixLib::ConfBool) - sizeof(PixLib::ConfObj));
  PixLib::ConfBool *a_bool=dynamic_cast<PixLib::ConfBool *>(&a);
  if (a_bool) {
    unsigned long text_length=0;
    unsigned long data_length=0;
    data_length += sizeof(a_bool->value());
    text_length += a_bool->m_yes.size() + a_bool->m_no.size();
    obj_size += ConfSize_t(data_length,text_length);
    if (show) {
      std::cout << "INFO [get_size_bool(PixLib::ConfObj &,bool)] the ConfBool " << std::setw(30) << a.name() << " acounts for " << std::setw(9) << obj_size.dataSize() 
		<< " bytes of data and " << std::setw(9) << obj_size.textSize() << " bytes of text ." << std::endl;
    }
  }
  else {
    std::cout << "ERROR [get_size_bool(PixLib::ConfObj &,bool)] not of expected type PixLib::ConfBool  : "
	      << a.name() << " / " << a.type() << std::endl;
  }
  return obj_size;
}

ConfSize_t get_size_string(PixLib::ConfObj &a, bool show) {
  ConfSize_t obj_size = get_size_base(a,false);
  obj_size += ConfSize_t(0,sizeof(PixLib::ConfString) - sizeof(PixLib::ConfObj));
  PixLib::ConfString *a_string=dynamic_cast<PixLib::ConfString *>(&a);
  if (a_string) {
    unsigned long text_length=0;
    unsigned long data_length=0;
    data_length += sizeof(a_string->value())+a_string->value().size();
    text_length += a_string->m_defval.size();
    obj_size += ConfSize_t(data_length,text_length);
    if (show) {
      std::cout << "INFO [get_size_string(PixLib::ConfObj &,string)] the ConfString " << std::setw(30) << a.name() << " acounts for " << std::setw(9) << obj_size.dataSize() 
		<< " bytes of data and " << std::setw(9) << obj_size.textSize() << " bytes of text ." << std::endl;
    }
  }
  else {
    std::cout << "ERROR [get_size_string(PixLib::ConfObj &,string)] not of expected type PixLib::ConfString  : "
	      << a.name() << " / " << a.type() << std::endl;
  }
  return obj_size;
}

ConfSize_t get_size_link(PixLib::ConfObj &a, bool show) {
  ConfSize_t obj_size = get_size_base(a,false);
  obj_size += ConfSize_t(0,sizeof(PixLib::ConfLink) - sizeof(PixLib::ConfObj));
  PixLib::ConfLink *a_link=dynamic_cast<PixLib::ConfLink *>(&a);
  if (a_link) {
    unsigned long text_length=0;
    unsigned long data_length=0;
    data_length += sizeof(a_link->value())+a_link->value().size();
    text_length += a_link->m_defval.size() + a_link->m_remnam.size();
    obj_size += ConfSize_t(data_length,text_length);
    if (show) {
      std::cout << "INFO [get_size_link(PixLib::ConfObj &,link)] the ConfLink " << std::setw(30) << a.name() << " acounts for " << std::setw(9) << obj_size.dataSize() 
		<< " bytes of data and " << std::setw(9) << obj_size.textSize() << " bytes of text ." << std::endl;
    }
  }
  else {
    std::cout << "ERROR [get_size_link(PixLib::ConfObj &,link)] not of expected type PixLib::ConfLink  : "
	      << a.name() << " / " << a.type() << std::endl;
  }
  return obj_size;
}

template <class T>
unsigned long calc_vector_size(const std::vector<T> &a) {
  unsigned long size = sizeof(a) + a.size() * sizeof(T);
  return size;
}

ConfSize_t get_size_vector(PixLib::ConfObj &a, bool show) {
  ConfSize_t obj_size = get_size_base(a,false);
  obj_size += ConfSize_t(0,sizeof(PixLib::ConfVector) - sizeof(PixLib::ConfObj));
  PixLib::ConfVector *a_vector=dynamic_cast<PixLib::ConfVector *>(&a);
  if (a_vector) {
    unsigned long text_length=0;
    unsigned long data_length=0;
    switch (a_vector->subtype()) {
    case PixLib::ConfVector::V_INT: {
      data_length += calc_vector_size<int>(a_vector->valueVInt());
      break;
    }
    case PixLib::ConfVector::V_UINT: {
      data_length += calc_vector_size<unsigned int>(a_vector->valueVUint());
      break;
    }
    case PixLib::ConfVector::V_FLOAT: {
      data_length += calc_vector_size<float>(a_vector->valueVFloat());
      break;
    }
    default: throw std::runtime_error("Invalid Config Subtype");
    }
    text_length += calc_vector_size<int>(a_vector->m_defInt) + calc_vector_size<unsigned int>(a_vector->m_defUint) + calc_vector_size<float>(a_vector->m_defFloat);
    obj_size += ConfSize_t(data_length,text_length);
    if (show) {
      std::cout << "INFO [get_size_vector(PixLib::ConfObj &,vector)] the ConfVector " << std::setw(30) << a.name() << " acounts for " << std::setw(9) << obj_size.dataSize() 
		<< " bytes of data and " << std::setw(9) << obj_size.textSize() << " bytes of text ." << std::endl;
    }
  }
  else {
    std::cout << "ERROR [get_size_vector(PixLib::ConfObj &,vector)] not of expected type PixLib::ConfVector  : "
	      << a.name() << " / " << a.type() << std::endl;
  }
  return obj_size;
}

//template <class T>
//unsigned long calc_matrix_size(PixLib::ConfMask<T> &a) {
//  unsigned long size = sizeof(a);
//  
//  for (typename std::vector< std::vector<T> >::const_iterator iter=a.get().begin();
//       iter != a.get().end();
//       iter++) {
//    size += iter->size() * sizeof(T) + sizeof(*iter);
//  }
//  return size;
//}

//template <>
//unsigned long calc_matrix_size<bool>(PixLib::ConfMask<bool> &a) {
//  unsigned long size = sizeof(a);
//  for (std::vector< std::vector<bool> >::const_iterator iter=a.get().begin();
//       iter != a.get().end();
//       iter++) {
//    size += (iter->size()+7) / 8 + sizeof(*iter);
//  }
//  return size;
//}


ConfSize_t get_size_matrix(PixLib::ConfObj &a, bool show) {
  ConfSize_t obj_size = get_size_base(a,false);
  obj_size += ConfSize_t(0,sizeof(PixLib::ConfMatrix) - sizeof(PixLib::ConfObj));
  PixLib::ConfMatrix *a_matrix=dynamic_cast<PixLib::ConfMatrix *>(&a);
  if (a_matrix) {
    unsigned long text_length=0;
    unsigned long data_length=0;
    switch (a_matrix->subtype()) {
    case PixLib::ConfMatrix::M_U1: {
 //     data_length += calc_matrix_size<bool>(a_matrix->valueU1());
      break;
    }
    case PixLib::ConfMatrix::M_U16: {
   //   data_length += calc_matrix_size<PixLib::TrimVal_t>(a_matrix->valueU16());
      break;
    }
    default: throw std::runtime_error("Invalid Config Subtype");
    }
  //  text_length += calc_matrix_size<PixLib::TrimVal_t>(a_matrix->m_defU16) + calc_matrix_size<bool>(a_matrix->m_defU1);
    obj_size += ConfSize_t(data_length,text_length);
    if (show) {
      std::cout << "INFO [get_size_matrix(PixLib::ConfObj &,matrix)] the ConfMatrix " << std::setw(30) << a.name() << " acounts for " << std::setw(9) << obj_size.dataSize() 
		<< " bytes of data and " << std::setw(9) << obj_size.textSize() << " bytes of text ." << std::endl;
    }
  }
  else {
    std::cout << "ERROR [get_size_matrix(PixLib::ConfObj &,matrix)] not of expected type PixLib::ConfMatrix  : "
	      << a.name() << " / " << a.type() << std::endl;
  }
  return obj_size;
}


ConfSize_t get_size(PixLib::ConfObj &a, bool show)
{
  ConfSize_t obj_size;
  switch (a.type()) {
  case PixLib::ConfObj::LIST: {
    obj_size = get_size_list(a,show);
    break;
  }
  case PixLib::ConfObj::INT: {
    obj_size = get_size_int(a,show);
    break;
  }
  case PixLib::ConfObj::FLOAT: {
    obj_size = get_size_float(a,show);
    break;
  }
  case PixLib::ConfObj::BOOL: {
    obj_size = get_size_bool(a,show);
    break;
  }
  case PixLib::ConfObj::STRING: {
    obj_size = get_size_string(a,show);
    break;
  }
  case PixLib::ConfObj::LINK: {
    obj_size = get_size_link(a,show);
    break;
  }
  case PixLib::ConfObj::VECTOR: {
    obj_size = get_size_vector(a,show);
    break;
  }
  case PixLib::ConfObj::MATRIX: {
    obj_size = get_size_matrix(a,show);
    break;
  }
  default: {
    std::cout << "ERROR [get_size(PixLib::ConfObj &,bool)] unsupported type : "
	      << a.name() << " / " << a.type() << std::endl;
    return obj_size;
  }
  }
  if (show) {
  std::cout << "INFO [get_size(PixLib::ConfObj &,bool)] the config object " << std::setw(30) << a.name() << " acounts for " << std::setw(9) << obj_size.dataSize() 
	    << " bytes of data and " << std::setw(9) << obj_size.textSize() << " bytes of text ." << std::endl;
  }
  return obj_size;
}

ConfSize_t get_size(PixLib::ConfGroup &a, bool show)
{
  ConfSize_t group_sum;
  for (size_t i=0; i<a.size(); i++) {
    PixLib::ConfObj &ao=a[i];
    group_sum += get_size(ao,show);
  }
  if (show) {
  std::cout << "INFO [get_size(PixLib::ConfObj &,bool)] the config group " << std::setw(30) << a.name() << " acounts for " << std::setw(9) << group_sum.dataSize() 
	    << " bytes of data and " << std::setw(9) << group_sum.textSize() << " bytes of text ." << std::endl;
  }

  return group_sum;
}

ConfSize_t get_size(PixLib::Config &a, bool show)
{
  ConfSize_t group_sum;
  for (unsigned int i=0; i<a.size(); i++) {
    PixLib::ConfGroup &ag=a[i];
    group_sum += get_size(ag, show);
  }
  if (show) {
  std::cout << "INFO [get_size(PixLib::Config &,bool)] the config groups of config " << std::setw(30) << a.name() << " acounts for " << std::setw(9) << group_sum.dataSize() 
	    << " bytes of data and " << std::setw(9) << group_sum.textSize() << " bytes of text ." << std::endl;
  }

  ConfSize_t subconfig_sum;
  for (unsigned int i=0; i<a.subConfigSize(); i++) {
    PixLib::Config &as=a.subConfig(i);
    subconfig_sum += get_size(as,show);
  }

  ConfSize_t sum(group_sum+subconfig_sum);
  if (show) {
  std::cout << "INFO [get_size(PixLib::Config &,bool)] the sub configs config " << std::setw(30) << a.name() << " acounts for " << std::setw(9) << subconfig_sum.dataSize() 
	    << " bytes of data and " << std::setw(9) << subconfig_sum.textSize() << " bytes of text ." << std::endl;

  std::cout << "INFO [get_size(PixLib::Config &,bool)] In total the config " << std::setw(30) << a.name() << " acounts for " << std::setw(9) << sum.dataSize() 
	    << " bytes of data and " << std::setw(9) << sum.textSize() << "bytes of text ." << std::endl;
  }
  return sum;
}
