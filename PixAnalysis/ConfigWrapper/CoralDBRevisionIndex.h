/* Dear emacs, this is -*-c++-*- */
#ifndef _PixA_CoralDBRevisionIndex_h_
#define _PixA_CoralDBRevisionIndex_h_

#include "IRevisionIndex.h"
#include <CoralDB/CoralDB.h>

#include <vector>
#include <map>

#include <time.h>

namespace CoralDB {
  class CoralDB;
}

namespace PixA {

  class Regex_t;

  class ExtRevision_t {
  public:
    ExtRevision_t( Revision_t revision, unsigned short representation=0)
      : m_revision(revision),
	m_representation(representation)
    {}

    Revision_t revision()           const { return m_revision; }
    unsigned short representation() const { return m_representation; }

    void setRepresentation( unsigned short representation) { m_representation = representation; }

  private:
    Revision_t m_revision;
    unsigned short m_representation;
  };

  inline bool operator<(const ExtRevision_t &a, const ExtRevision_t &b) {
    return a.revision() < b.revision()  || (a.revision()==b.revision() && a.representation()< b.representation());
  }


  class CoralDBRevisionIndex : public IRevisionIndex
  {
  public:
    CoralDBRevisionIndex(const std::string &connection_string);

    ~CoralDBRevisionIndex();

    void getIdTags(std::vector<std::string> &id_tag_out);

    void getTags(const std::string &id_tag, std::vector<std::string> &tag_out);

    void getRevisions(const std::string &id_tag, const std::string &cfg_tag, const std::string &conn_name, std::set<Revision_t> &revisions_out);

    void getNames(const std::string &id_tag, const std::string &cfg_tag, EObjectType type, std::set<std::string> &names);

    /**
     * @param current_upper_revision_limit after scanning there cannot appear new revisions below this boundary.
     */
    void processRevision(const std::string &id_tag,
			 const std::string &cfg_tag,
			 Revision_t target_revision,
			 Revision_t lower_revision_bound,
			 Revision_t current_upper_revision_limit,
			 IRevisionProcessor &processor);

    bool addRevision(const std::string &id_tag,
		     const std::string &cfg_tag,
		     const std::string &type_name,
		     const std::string &name,
		     bool is_in_coral,
		     Revision_t /*revision*/,
		     const std::string &data);
    
  protected:

    static std::string makeCfgIdTag(const std::string &id_tag) { return id_tag+CoralDBRevisionIndex::cfgIdTagTrailer(); }
    static const std::string &cfgIdTagTrailer() { return s_cfgIdTagTrailer; }

    ExtRevision_t extractRevision( const std::string &id_name, const std::string &file_name);

    bool isConnObject(const std::string &id_name);

    CoralDB::CoralDB *createCoralDbInstance(const std::string &id_tag, const std::string &cfg_tag, bool read_only);

    CoralDB::CoralDB *coralDbInstance() { return createCoralDbInstance("", "", true);}

    CoralDB::CoralDB *coralDbInstance(const std::string &id_tag, const std::string &cfg_tag, bool read_only=true) 
    { return createCoralDbInstance(id_tag, cfg_tag, read_only); }

    CoralDB::CoralDB *coralDbTransaction(const std::string &id_tag, const std::string &cfg_tag) { return createCoralDbInstance(id_tag, cfg_tag, false); }


  private:

    const std::string m_connectionString;
//    std::auto_ptr<CoralDB::CoralDB> m_db;

    static std::vector<Regex_t *> s_patternList;
    static std::map<std::string, std::string> s_typeTranslation;
    

    time_t                       m_cacheUpdateTime;
    std::string                  m_cachedIdTag;
    CoralDB::ObjectDictionaryMap m_objectDictionaryCache;

    static std::string s_cfgIdTagTrailer;
    static std::string s_cfgTypeTail;
    static std::string s_rootId;
    static std::string s_dstSlotName;
    static const char *s_typeName[kNTypes];
  };

}
#endif
