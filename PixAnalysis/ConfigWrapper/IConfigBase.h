#ifndef _IConfigBase_h_
#define _IConfigBase_h_

#include "ConfigRoot.h"
#include <cassert>
#include <iostream>

namespace PixA {

  class IConfigBase
  {
  public:
    IConfigBase(ConfigRoot &root) : m_root(&root)  /*debug:*/, m_subUsers(0) {debug_init();}
    IConfigBase(IConfigBase &config) : m_root(config.m_root)  /*debug:*/, m_subUsers(0) {debug_init();}
    IConfigBase(IConfig *parent) : m_root(new ConfigRoot(parent)), m_subUsers(0) {debug_init();}

    //debug:
    ~IConfigBase() { assert(m_subUsers==0); debug_dtor();}

    void useNode()      { m_root->use(); /*debug:*/ m_subUsers++; }
    void doneWithNode() { /*debug:*/ assert(m_subUsers>0); m_subUsers--; m_root->done();  }

    /** debug method.
     */
    bool isRoot(const IConfig * const derived_class) const {return m_root && m_root->isRoot(derived_class);}

    /** debug method.
     */
    unsigned int use_count() const {return m_subUsers;}

    /** debug method.
     */
    unsigned int root_use_count() const {return m_root->use_count();}

  protected:
    ConfigRoot *root() {return m_root;}

  private:
    void debug_init() { }//std::cout << "INFO [IConfigBase::ctor] " << static_cast<void *>(this) << " root = " << static_cast<void *>(m_root) << std::endl;
    void debug_dtor() { }//std::cout << "INFO [IConfigBase::dtor] " << static_cast<void *>(this) << " root = " << static_cast<void *>(m_root) << std::endl;

    ConfigRoot  *m_root;
    unsigned int m_subUsers; /* : debug */
  };

}

#endif
