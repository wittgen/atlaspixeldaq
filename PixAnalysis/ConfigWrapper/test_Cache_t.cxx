#include "ObjectCache_t.h"
#include <sstream>

int main(int argc, char **argv)
{
  unsigned int cache_size=100;

  for (int arg_i=1; arg_i < argc; arg_i++) {
    if (strcmp(argv[arg_i],"-c")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      cache_size = atoi(argv[++arg_i]);
    }
    else {
      std::cerr << "unhandled argument : " << argv[arg_i] << std::endl;
      return -1;
    }
  }

  PixA::ObjectCache_t<int> cache(cache_size);

  for (unsigned int rev_i=0; rev_i<10; rev_i++) {

    for (unsigned int i=0; i<10; i++) {
      std::stringstream name;
      name << "obj_" << i;
      if (!cache.object(name.str(), rev_i)) {
	cache.add(name.str(),rev_i,static_cast<int>(i));
      }
      std::cout << "INFO [main] rev = " << rev_i << " i = " << i  << " : cache size = " << cache.size() << std::endl;
    }

    for (unsigned int i=0; i<10; i++) {
      std::stringstream name;
      name << "obj_" << i;
      if ((i%2)==0 || (i%4)==1 && (rev_i%2)==0) {
	cache.object(name.str(), rev_i);
      }
    }

  }

}
