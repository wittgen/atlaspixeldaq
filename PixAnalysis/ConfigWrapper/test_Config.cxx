#include "ConnectivityManager.h"
#include "ConfObjUtil.h"
#include "ConfGrpRefUtil.h"

int main()
{
  unsigned int n_passes = 3;
  unsigned int n_loops_no_cleanup = 3;
  unsigned int n_config_loops = 3;
    //    PixA::ConnectivityRef conn(PixA::getConnectivity("A","P","C","Cfg",1));
  for (unsigned int pass=0; pass<n_passes; pass++) {
  try {
    for (unsigned int i=0; i< n_loops_no_cleanup ; i++) {
  std::string tag("BiStave_4");
  PixA::ConnectivityRef conn(PixA::ConnectivityManager::getConnectivity(tag,tag,tag,"BiStave",0));

    //conn will wrap a pointer to IConnectivity
    // IConnectivity will contain a PixConnectivity and will cache config objects.
    // The connectivity will be a shared property. If there are no more users, the 
    // connectivity will be deleted eventually.

  for (unsigned int j=0; j<n_config_loops; j++) {
    const PixA::Pp0List &pp0s=conn.pp0s("ROD_C1_S11");
    // read only
    for (PixA::Pp0List::const_iterator pp0_iter=pp0s.begin();
	 pp0_iter != pp0s.end(); // may be slightly inefficient unless the compiler is smart enough.
	 ++pp0_iter) {

      // it may be more efficient to call modulesBegin and modulesEnd only once:
      PixA::ModuleList::const_iterator modules_begin=PixA::modulesBegin(pp0_iter);
      PixA::ModuleList::const_iterator modules_end=PixA::modulesEnd(pp0_iter);

      for (PixA::ModuleList::const_iterator module_iter=modules_begin;
 	   module_iter != modules_end;
 	   ++module_iter) {
	PixA::ConfigHandle module_config_handle( conn.modConf(module_iter) );
	PixA::ConfigRef module_config( module_config_handle.ref() );

	PixA::ConfigRef full_module_config ( module_config.createFullCopy() );
	PixA::ConfigHandle full_config_handle( full_module_config);
 	//const PixLib::Config &config = full_module_config.config();
	// 	const_cast<PixLib::Config &>(config).dump(std::cout);
      }



      // make shure that conn does not go out of scope
      // because then the Config object boc_config is referring to will be deleted
      const PixA::ConfigRef boc_config(conn.bocConf(pp0_iter).ref());
      // to ensure that the object stays alive do:
      // and keep the boc_config_handle alive
      const PixA::ConfigHandle boc_config_handle(conn.bocConf(pp0_iter));
      const PixA::ConfigRef boc_config_save(boc_config_handle.ref());


      //rx_config will wrap a pointer to a IConfig (shared object)
      // this will eventually open a data file which will call the use mathod from IDbWrapper
      // once the ConfigRef object is deleted the IConfig interface call the donw method from IDbWrapper
      // if a data file was opened.

      for (unsigned int rx_i=0; rx_i<4; rx_i++) {
	const PixA::ConfigRef tx_config(subConfig(boc_config,"PixTx",rx_i));
	const PixA::ConfGrpRef tx_config_bpm(tx_config["Bpm"]);
	const PixA::ConfigRef rx_config(subConfig(boc_config,"PixRx",rx_i));
	{
	  const PixA::ConfGrpRef rx_config_general(rx_config["General"]);
	  // get config["general"]["DataDelay1"];
	  //      unsigned int fibre_i=2;
	  for (unsigned int fibre_i=2; fibre_i<10 ;fibre_i++) {
	    std::cout << " DataDelay "<<fibre_i<< "  = " << confVal<int>(confElm(rx_config_general,"DataDelay",fibre_i)) << std::endl;
	  }

	  //	  const PixA::ConfGrpRef tx_config_general(rx_config["General"]);
	  // get config["general"]["DataDelay1"];
	  //      unsigned int fibre_i=2;
	  for (unsigned int fibre_i=2; fibre_i<10 ;fibre_i++) {
	    std::cout << " MarkSpace "<<fibre_i<< " = " << confVal<int>(confElm(tx_config_bpm,"MarkSpace",fibre_i)) << std::endl;
	  }
	}
	{
	  const PixA::ConfGrpRef rx_config_opt(rx_config["Opt"]);
	  for (unsigned int fibre_i=2; fibre_i<10 ;fibre_i++) {
	    std::cout << " Threshold "<<fibre_i<< "  = " << confVal<int>(confElm(rx_config_opt,"RxThreshold",fibre_i)) << std::endl;
	  }
	}
      }
    }
    }
    }
  }
  catch (PixA::ConfigException &err) {
    std::cout << "FATAL [main] caught exception " << err << std::endl;
  }
  catch (std::exception &err) {
    std::cout << "FATAL [main] caught exception " << err.what() << std::endl;
  }
  PixA::ConnectivityManager::instance()->cleanup();
#ifdef DEBUG
  PixA::DbWrapper::stat();
#endif
  }
}
