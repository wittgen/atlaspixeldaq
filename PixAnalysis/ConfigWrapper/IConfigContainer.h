#ifndef _IConfigContainer_h_
#define _IConfigContainer_h_

namespace PixA {
  class IConfigContainer
  {
  public:
    virtual ~IConfigContainer() {}

    virtual const ConfRef &getModuleConfig() const = 0;
    virtual const PixLib::Config &getFullModuleConfig() const = 0;

    virtual const ConfRef &getBocConfig() const = 0;
    virtual const PixLib::Config &getFullBocConfig() const = 0;

    virtual const ConfRef &getRodConfig() const = 0;
    virtual const PixLib::Config &getFullRidConfig() const = 0;

  };
}

#endif
