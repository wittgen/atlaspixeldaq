#ifndef _PixA_ObjectCache_t_impl_h_
#define _PixA_ObjectCache_t_impl_h_

#include "ObjectCache_t.h"

namespace PixA {


  template <class T>
  void ObjectCache_t<T>::add(const std::string &name, const Revision_t &revision, const T &a) 
  {

    incTime();

    if (m_nObjects>m_nObjectsMax) {
      if (m_nObjects>m_nObjectsMax+m_nObjectsMax*3/10) {
	//	  std::cout << "INFO [ObjectCache_t::add] cleanup more aggressively." << std::endl;
	this->aggressiveCleanup();
      }
      else{
	//	  std::cout << "INFO [ObjectCache_t::add] cleanup." << std::endl;
	cleanup(true);
      }
    }
    int count=0;
    RevisionList_t &revision_list = m_objectList[name];
    std::pair<typename RevisionList_t::iterator , bool> ret;
    ret = revision_list.insert( std::make_pair<Revision_t, UseStat_t<T> >((Revision_t)revision, UseStat_t<T>(a, currentTime() )) );
    typename RevisionList_t::iterator i;
    for(i=revision_list.begin(); i!=revision_list.end();++i){
        if(i->first == revision){
            count++;
        }
    }
    if (!ret.second && count==0) {
      std::stringstream message;
      message << "ERROR [ObjectCache_t::add] Failed to add object " << name << " / " << revision << " to cache.";
      throw ConfigException(message.str() );
    }
    m_nObjects++;
  }

  template <class T>
  void ObjectCache_t<T>::cleanup(bool execute) {

    for (typename ObjectList_t::iterator obj_iter = m_objectList.begin();
	 obj_iter != m_objectList.end();
	 obj_iter++) {

      bool reached_end = obj_iter->second.empty();
      for (typename RevisionList_t::iterator rev_iter = obj_iter->second.begin();
	   !reached_end;
	   ) {

	typename RevisionList_t::iterator current_rev_iter = rev_iter++;
	reached_end = ( rev_iter ==obj_iter->second.end() );

	if ( tooOld(current_rev_iter->second.time()) && execute) {
	  //	    std::cout << "INFO [ObjectCache_t::cleanup] erase " << obj_iter->first << " / " << current_rev_iter->first << "." << std::endl;
	  obj_iter->second.erase(current_rev_iter);
	  m_nObjects--;
	}
      }
    }
  }

  template <class T>
  void ObjectCache_t<T>::aggressiveCleanup() {
    if (m_nObjects <= m_nObjectsMax) return;
    unsigned n_candidates = m_nObjects - m_nObjectsMax;

    std::set< short > candidates;
    for (typename ObjectList_t::iterator obj_iter = m_objectList.begin();
	 obj_iter != m_objectList.end();
	 obj_iter++) {

      for (typename RevisionList_t::iterator rev_iter = obj_iter->second.begin();
	   rev_iter != obj_iter->second.end();
	   ) {

	typename RevisionList_t::iterator current_rev_iter = rev_iter++;
	if (candidates.size() <= n_candidates  || *(candidates.begin()) > current_rev_iter->second.time()) {
	  if (candidates.size() >= n_candidates) {
	    candidates.erase(candidates.begin());
	  }
	  candidates.insert(current_rev_iter->second.time() );
	}
      }
    }

    if (!candidates.empty()) {
      unsigned int short threshold = *(candidates.rbegin());

      //	std::cout << "INFO [ObjectCache_t::aggressiveCleanup] delete all objects with a short lifetime than " << threshold << "." << std::endl;

      bool reached_last_obj = m_objectList.empty();
      for (typename ObjectList_t::iterator obj_iter = m_objectList.begin();
	   !reached_last_obj;
	   ) {
	typename ObjectList_t::iterator current_obj_iter = obj_iter++;
	reached_last_obj = (obj_iter == m_objectList.end());
	bool reached_last_rev = current_obj_iter->second.empty();
	for (typename RevisionList_t::iterator rev_iter = current_obj_iter->second.begin();
	     !reached_last_rev;
	     ) {
	  typename RevisionList_t::iterator current_rev_iter = rev_iter++;
	  reached_last_rev = (rev_iter == current_obj_iter->second.end() );
	  if (current_rev_iter->first<=threshold) {

	    if (current_obj_iter->second.size()==1) {
	      //		std::cout << "INFO [ObjectCache_t::cleanup] erase " << current_obj_iter->first << " entirely." << std::endl;
	      m_nObjects -= current_obj_iter->second.size();
	      m_objectList.erase( current_obj_iter );
	      break;
	    }
	    else {
	      //		std::cout << "INFO [ObjectCache_t::cleanup] erase " << current_obj_iter->first << " / " << current_rev_iter->first << "." << std::endl;
	      current_obj_iter->second.erase(current_rev_iter);
	      m_nObjects--;
	    }
	  }
	}
      }
    }
  }

  template <class T>
  T *ObjectCache_t<T>::_object(const std::string &name, const Revision_t &revision)
  {

    incTime();

    typename ObjectList_t::iterator obj_iter = m_objectList.find(name);
    if (obj_iter != m_objectList.end()) {

      typename RevisionList_t::iterator rev_iter = obj_iter->second.find(revision);
      if ( rev_iter != obj_iter->second.end()) {

	return &(rev_iter->second.obj(currentTime()));
      }
    }
    return NULL;
  }

  template <class T>
  void ObjectCache_t<T>::flipObjTime() {
    //    std::cout << "INFO [ObjectCache_t::flipObjTime] " << std::endl;
    bool reached_last_obj = m_objectList.empty();
    for (typename ObjectList_t::iterator obj_iter = m_objectList.begin();
	 !reached_last_obj;
	 ) {
      typename ObjectList_t::iterator current_obj_iter = obj_iter++;
      reached_last_obj = (obj_iter == m_objectList.end());

      bool reached_last_rev = current_obj_iter->second.empty();
      for (typename RevisionList_t::iterator rev_iter = current_obj_iter->second.begin();
	   !reached_last_rev;
	   ) {
	typename RevisionList_t::iterator current_rev_iter = rev_iter++;
	reached_last_rev = (rev_iter == current_obj_iter->second.end() );

	// remove too old objects
	if ( !(current_rev_iter->second.flipTime()) ) {
	  if (current_obj_iter->second.size()==1) {
	    //	      std::cout << "INFO [ObjectCache_t::cleanup] erase " << current_obj_iter->first << " entirely." << std::endl;
	    m_nObjects-= current_obj_iter->second.size();
	    m_objectList.erase(current_obj_iter);
	  }
	  else {
	    //	      std::cout << "INFO [ObjectCache_t::cleanup] erase " << current_obj_iter->first << " / " << current_rev_iter->first << "." << std::endl;
	    current_obj_iter->second.erase(current_rev_iter);
	    m_nObjects--;
	  }
	}
      }
    }
  }

}

#endif
