#include "Locations.h"
#include <iomanip>

namespace PixA {

  const char Pp0Location::s_sideName[2]={'A','C'};

  Pp0Location::Pp0Location (ELocation location, 
	       unsigned char layer_nr,
	       unsigned char bistave_nr,
	       unsigned char bistave_part_nr) 
  {
    assert(bistave_part_nr == 1 || bistave_part_nr==2);
    assert(layer_nr>=1 && layer_nr<=3);
    assert(bistave_nr<52);

    std::stringstream pp0_name;
    if (location>=kLA) {
      pp0_name << "L" << layer_nr;
    }
    else {
      pp0_name << "D" << layer_nr << s_sideName[location];
    }
    pp0_name << "_B" << std::setw(2) << std::setfill('0') << bistave_nr
	     << "_S" << bistave_part_nr
	     << "_" << s_sideName[location-kLA]
	     << (bistave_part_nr==1 ? '7' : '6');
    Pp0LocationBase::operator=(pp0_name.str());
  }


  ModuleLocation::ModuleLocation (ELocation location, 
				  unsigned char layer_nr,
				  unsigned char bistave_nr,
				  unsigned char bistave_part_nr,
				  unsigned char module_nr ) 
    : Pp0Location(location,layer_nr,bistave_nr,bistave_part_nr) 
  {

    assert(module_nr<=6);

    std::stringstream module_name;
    module_name << "_M" << module_nr << s_sideName[location-kLA];

    Pp0Location::operator+=(module_name.str());
  }

}
