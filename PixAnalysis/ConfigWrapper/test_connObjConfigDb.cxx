#include "ConnObjConfigDb.h"

#include "ConnectivityManager.h"

#include "createDbServer.h"
#include "PixConnObjConfigDb.h"

#include <PixBoc/PixBoc.h>

#include <ConfigRootIO/RootConfException.h>
#include <iomanip>

void dumpConfig(PixLib::Config &config, std::string margin)
{
  if (!config.m_group.empty()) {
    std::cout << "INFO [dumpConfig] " << margin << " groups of " << config.name() << " / " << config.type() << " ; " << config.rev() << " : " << std::endl;
  for (std::vector<PixLib::ConfGroup*>::iterator group_iter = config.m_group.begin();
       group_iter != config.m_group.end();
       ++group_iter) {
    std::cout << "INFO [dumpConfig] " << margin << " group : [" << config.name() << "][" << (*group_iter)->name() << "]" << std::endl;
  }
  }

  if (!config.m_config.empty()) {
    std::cout << "INFO [dumpConfig] " << margin << " sub configs of " << config.name() << " / " << config.type() << " ; " << config.rev() << " : " << std::endl;
  for (std::map<std::string, PixLib::Config*>::iterator sub_config_iter = config.m_config.begin();
       sub_config_iter != config.m_config.end();
       ++sub_config_iter) {
    std::cout << "INFO [dumpConfig] "<< margin << " sub configs : " << sub_config_iter->first << std::endl;
  }
  for (std::map<std::string, PixLib::Config*>::iterator sub_config_iter = config.m_config.begin();
       sub_config_iter != config.m_config.end();
       ++sub_config_iter) {
    dumpConfig(*(sub_config_iter->second), margin+"  ");
  }
  }
}

void dumpConfig(PixLib::Config &config) {
  dumpConfig(config,"");
}

class IPCInstance
{

 private:
  IPCInstance(int argc, char **argv, const std::string &partition_name) {
    IPCCore::init(argc, argv);
    m_ipcPartition=std::make_shared<IPCPartition>(partition_name.c_str());

    if (!m_ipcPartition->isValid()) {
      std::stringstream message;
      message << "ERROR [IPCInstance] Not a valid partition " << partition_name << std::endl;
      throw std::runtime_error(message.str());
    }
  }
 public:

  static IPCInstance *instance(int argc, char **argv, const std::string &partition_name) {
    if (!s_instance) {
      s_instance = new IPCInstance(argc,argv,partition_name);
    }
    return s_instance;
  }

  IPCPartition &partition() { return *m_ipcPartition;}
  
 private:
  std::shared_ptr<IPCPartition> m_ipcPartition;
  static IPCInstance *s_instance;

};

IPCInstance *IPCInstance::s_instance = NULL;


std::shared_ptr<PixLib::PixDbServerInterface> dbServer(const std::string &db_server_partition_name,
		 const std::string &db_server_name,
		 int argc,
		 char **argv)
{
  int temp_argc=0;
  char *temp_argv[1]={NULL};
  IPCInstance *ipc_instance = IPCInstance::instance(temp_argc,temp_argv, db_server_partition_name);

  std::shared_ptr<PixLib::PixDbServerInterface> db_server( PixA::createDbServer(ipc_instance->partition(),db_server_name) );
  if (db_server.get()) {
    PixA::ConnectivityManager::useDbServer(db_server);
  }
  return db_server;
}


void printTags(const std::string &label, const std::vector<std::string> &tags)
{
  if (!label.empty()) {
    std::cout << label << std::endl;
  }
  for ( std::vector<std::string>::const_iterator tag_iter = tags.begin();
	tag_iter != tags.end();
	++tag_iter) {
    std::cout << *tag_iter << std::endl;
  }
}

std::string makeTimeString(time_t a_time)
{
  std::string a_time_string = ctime(&a_time);
  std::string::size_type ret_pos = a_time_string.find("\r");
  std::string::size_type new_line_pos = a_time_string.find("\n");
  if (new_line_pos == std::string::npos || (new_line_pos>ret_pos && ret_pos != std::string::npos)) {
    new_line_pos =ret_pos;
  }
  if (new_line_pos != std::string::npos ) {
    a_time_string.erase(new_line_pos, a_time_string.size()-new_line_pos);
  }
  return a_time_string;
}

void printRevisions(const std::string &label, const std::set<PixA::Revision_t> &revisions, unsigned int verbose=0)
{
  if (!label.empty()) {
    std::cout << label << std::endl;
  }
  for ( std::set<PixA::Revision_t>::const_iterator rev_iter = revisions.begin();
	rev_iter != revisions.end();
	++rev_iter) {
    std::cout << *rev_iter;
    if (verbose>0) {
      std::cout << " : " << makeTimeString(*rev_iter);
    }
    std::cout << std::endl;
  }
}


std::map<std::string, PixA::CfgLocation_t::EBackEnd> s_backEnd;

PixA::CfgLocation_t::EBackEnd parseBackEnd( const std::string &back_end_name) {
  if (s_backEnd.empty()) {
    s_backEnd.insert(std::make_pair(std::string("DbServer"),PixA::CfgLocation_t::kDbServer));
    s_backEnd.insert(std::make_pair(std::string("dbserver"),PixA::CfgLocation_t::kDbServer));
    s_backEnd.insert(std::make_pair(std::string("db-server"),PixA::CfgLocation_t::kDbServer));
    s_backEnd.insert(std::make_pair(std::string("DBSERVER"),PixA::CfgLocation_t::kDbServer));
    s_backEnd.insert(std::make_pair(std::string("DB-SERVER"),PixA::CfgLocation_t::kDbServer));
    s_backEnd.insert(std::make_pair(std::string("ConfigRootIO"),PixA::CfgLocation_t::kConfigRootIO));
    s_backEnd.insert(std::make_pair(std::string("RootNew"),PixA::CfgLocation_t::kConfigRootIO));
    s_backEnd.insert(std::make_pair(std::string("file"),PixA::CfgLocation_t::kConfigRootIO));
    s_backEnd.insert(std::make_pair(std::string("FILE"),PixA::CfgLocation_t::kConfigRootIO));
  }
  std::map<std::string, PixA::CfgLocation_t::EBackEnd>::const_iterator back_end_iter = s_backEnd.find(back_end_name);
  if (back_end_iter == s_backEnd.end()) {
    std::stringstream message;
    message << "FATAL [parseBackEnd] Unknown back-end : " << back_end_name;
    std::cout << message.str() << std::endl;
    throw std::runtime_error(message.str());
  }
  return back_end_iter->second;
}

std::map<std::string, PixA::IConnObjConfigDbManager::EObjectType> s_typeMap;

PixA::IConnObjConfigDbManager::EObjectType parseObjType(const std::string &obj_type) {

  if (obj_type.empty()) return PixA::IConnObjConfigDbManager::kNObjectTypes;

  if (s_typeMap.empty()) {
    s_typeMap.insert(std::make_pair(std::string("MODULE"),PixA::IConnObjConfigDbManager::kModuleObjects));
    s_typeMap.insert(std::make_pair(std::string("module"),PixA::IConnObjConfigDbManager::kModuleObjects));
    s_typeMap.insert(std::make_pair(std::string("Module"),PixA::IConnObjConfigDbManager::kModuleObjects));
    s_typeMap.insert(std::make_pair(std::string("PixModule"),PixA::IConnObjConfigDbManager::kModuleObjects));

    s_typeMap.insert(std::make_pair(std::string("PixModuleGroup"),PixA::IConnObjConfigDbManager::kModuleGroupObjects));
    s_typeMap.insert(std::make_pair(std::string("ROD"),PixA::IConnObjConfigDbManager::kModuleGroupObjects));
    s_typeMap.insert(std::make_pair(std::string("rod"),PixA::IConnObjConfigDbManager::kModuleGroupObjects));

    s_typeMap.insert(std::make_pair(std::string("PixTrigController"),PixA::IConnObjConfigDbManager::kTrigControllerObjects));
    s_typeMap.insert(std::make_pair(std::string("TIM"),PixA::IConnObjConfigDbManager::kTrigControllerObjects));
    s_typeMap.insert(std::make_pair(std::string("tim"),PixA::IConnObjConfigDbManager::kTrigControllerObjects));

    s_typeMap.insert(std::make_pair(std::string("PixDisable"),PixA::IConnObjConfigDbManager::kDisableObjects));
    s_typeMap.insert(std::make_pair(std::string("DISABLE"),PixA::IConnObjConfigDbManager::kDisableObjects));
    s_typeMap.insert(std::make_pair(std::string("disable"),PixA::IConnObjConfigDbManager::kDisableObjects));
  }

  std::map<std::string, PixA::IConnObjConfigDbManager::EObjectType>::const_iterator type_iter = s_typeMap.find(obj_type);
  if (type_iter == s_typeMap.end()) {
    std::stringstream message;
    message << "ERROR [parseObjType] Unknown type : " << obj_type << ". Possible values are : module, rod, tim, disable + other spellings ." << std::endl;
    throw std::runtime_error(message.str());
  }
  return type_iter->second;
}

class Time_t {
public:
  Time_t(::time_t seconds, long int nano_second)
  { m_spec.tv_sec=seconds;
    m_spec.tv_nsec=nano_second; }

  struct timespec &spec() { return m_spec; }

  void setTime(double new_time) {
    if (new_time>0) {
      m_spec.tv_sec = static_cast<time_t>( floor(new_time) );
      m_spec.tv_nsec = static_cast<long int>((new_time - floor(new_time))*1e9);
    }
  }

  operator bool() const {
    return m_spec.tv_nsec>0 || m_spec.tv_sec>0;
  }

  void sleep() {
    if (m_spec.tv_nsec>0 || m_spec.tv_sec>0) {
      std::cout << "INFO [Time_t::sleep] sleep " << m_spec.tv_sec << "." << std::setfill('0') << std::setw(9) << m_spec.tv_nsec << std::endl;
      nanosleep(&m_spec, NULL);
    }
  }

private:
  struct timespec m_spec;
};


int main(int argc, char **argv)
{

  std::string id_name="CT";
  std::string cfg_tag_name;
  std::string dest_cfg_tag_name="test";
  std::vector< std::pair<std::string, PixA::Revision_t> > conn_name;
  PixA::Revision_t rev1=0;
  PixA::Revision_t dest_rev=0;
  bool error=false;
  //  bool db_server_tags=false;
  //  bool show_disabled_objects=false;
  bool list_id_tags = false;
  bool list_tags = false;
  bool revision_list_only=false;
  bool store=false;
  unsigned int verbose=0;
  std::string obj_type_name;
  unsigned int n_loops=1;
  Time_t loop_wait(0,0);
  Time_t conn_wait(0,0); 
  Time_t init_wait(0,0); 

  PixA::CfgLocation_t::EBackEnd store_back_end=PixA::CfgLocation_t::kDbServer;

  std::shared_ptr<PixLib::PixDbServerInterface> db_server;
  time_t current_time = time(0);
  rev1 = current_time;
  for (int arg_i=1; arg_i<argc; arg_i++) {

    if (strcmp(argv[arg_i],"--db-server")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-' ) {

      try {
	std::string db_server_partition_name = argv[++arg_i];
	std::string db_server_name = ( (arg_i+1<argc && argv[arg_i+1][0]!='-') ? argv[++arg_i] : "PixelDbServer");
	db_server = dbServer(db_server_partition_name, db_server_name, argc, argv);
      }
      catch (std::runtime_error &err) {
	std::cout << "FATAL [main] caught exception (std::exception) : " << err.what() << std::endl;
	return -1;
      }

    }
    else if (strcmp(argv[arg_i],"-i")==0 && arg_i+1<argc) {
      id_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-n")==0 && arg_i+1<argc) {
      std::pair<std::string,PixA::Revision_t> a_conn_name(std::make_pair(argv[++arg_i],rev1));
      if (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	a_conn_name.second = atoi(argv[++arg_i]);
      }
      conn_name.push_back( a_conn_name );
    }
    else if (strcmp(argv[arg_i],"-t")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      cfg_tag_name = argv[++arg_i];
      if (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	rev1=atoi(argv[++arg_i]);
      }
    }
    else if (strcmp(argv[arg_i],"--dest-tag")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      dest_cfg_tag_name = argv[++arg_i];
      if (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	dest_rev=atoi(argv[++arg_i]);
      }
      //      store=true;
    }
    else if (strcmp(argv[arg_i],"-l")==0 || strcmp(argv[arg_i],"--revision-list")==0) {
      revision_list_only=true;
    }
    else if (strcmp(argv[arg_i],"--obj-list")==0  && arg_i+1<argc) {
      obj_type_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"--loop")==0  && arg_i+1<argc) {
      n_loops = atoi(argv[++arg_i]);
    }
    else if (strcmp(argv[arg_i],"--wait")==0  && arg_i+1<argc) {
      loop_wait.setTime( strtod(argv[++arg_i],NULL) );
      if(arg_i+1<argc) {
	conn_wait.setTime( strtod(argv[++arg_i],NULL) );
	if(arg_i+1<argc) {
	  init_wait.setTime( strtod(argv[++arg_i],NULL) );
	}
      }
    }
    else if (strcmp(argv[arg_i],"--store-back-end")==0 && arg_i+1<argc) {
      try {
	store_back_end = parseBackEnd( argv[++arg_i]);
	//	store=true;
      }
      catch(std::exception &) {
	std::cout << "INFO [" << argv[0] << ":main] invalid back-end : " << argv[arg_i] << "." << std::endl;
      }
    }
    else if (strcmp(argv[arg_i],"-s")==0 || strcmp(argv[arg_i],"--store")==0) {
      store=true;
    }
    else if (strcmp(argv[arg_i],"-v")==0 || strcmp(argv[arg_i],"--verbose")==0) {
      verbose++;
    }
    else {
      std::cout << "INFO [" << argv[0] << ":main] Unhandled argument : " << argv[arg_i] << "." << std::endl;
      error=true;
    }
  }

  if (error) {
    std::cout << "USAGE " << argv[0] << "--db-server partition [name] -i id-tag -t tag [rev] [--revision-list] [-n conn-name [rev] ] ... [-v [-v [...] ] ]" << std::endl
	      << "\t [--dest-tag tag [rev] ] [--store-back-end dbserver/file] [--obj-list type-name]" << std::endl;
    return -1;
  }
  if (conn_name.empty() && obj_type_name.empty()) {
    if (id_name.empty()) {
      list_id_tags =true;
    }
    else {
      list_tags = true;
    }
  }

  //  PixA::ConnObjConfigDb::useDbServer( db_server );

  PixA::ConnObjConfigDb cfg_db(PixA::PixConnObjConfigDbManager::createInstance( db_server, store_back_end));

  if (list_id_tags && id_name.empty()) {
    std::vector<std::string> id_tags;
    cfg_db.getIdTags(id_tags);
    printTags( "ID Tags : ", id_tags );
  }
  
  if (list_tags && !id_name.empty()) {
    std::vector<std::string> tags;
    cfg_db.getTags(id_name, tags);
    std::string label("Tags for ");
    label += id_name;
    label += " : ";
    
    printTags( label , tags);
  }

  if (!obj_type_name.empty() && !id_name.empty() && !cfg_tag_name.empty()) {
    std::set<std::string> names;
    std::cout << "INFO [main] list objects in " << id_name << ":" << cfg_tag_name << " of type "
	      << parseObjType(obj_type_name) << std::endl;
    cfg_db.getNames(id_name, cfg_tag_name, parseObjType(obj_type_name), names);
    for(std::set<std::string>::const_iterator name_iter = names.begin();
	    name_iter != names.end();
	++name_iter) {
      std::cout << *name_iter << std::endl;
    }
  }

  try {
  if (!id_name.empty()  && !cfg_tag_name.empty()) {
    if (store) {
      cfg_db.cloneTag(id_name, cfg_tag_name, dest_cfg_tag_name,"_Tmp");
    }

    PixA::ConnObjConfigDbTagRef new_tag( (store ? cfg_db.getTag(id_name, dest_cfg_tag_name, 0) : cfg_db.getTag(id_name, cfg_tag_name,rev1) ));
    init_wait.sleep();
    
    for (unsigned int loop_i=0; loop_i<n_loops; ++loop_i) {
      if (loop_i>0) {
	loop_wait.sleep();
      }

    for (std::vector< std::pair< std::string, PixA::Revision_t> >::const_iterator conn_iter = conn_name.begin();
	 conn_iter != conn_name.end();
	 ++conn_iter) {

	if (conn_iter != conn_name.begin()) {
	  conn_wait.sleep();
	}

      PixA::ConnObjConfigDbTagRef a_tag( cfg_db.getTag(id_name, cfg_tag_name, conn_iter->second) );

      if (revision_list_only) {
	std::set<PixA::Revision_t> revisions;
	cfg_db.getRevisions(id_name, cfg_tag_name, conn_iter->first, revisions);
	std::string label("revisions for ");
	label += conn_iter->first;
	label += " in ";
	label += cfg_tag_name;
	label += " : ";
	printRevisions(label,revisions, verbose);
      } 
      else {
	if (verbose>0) {
	  std::cout << "INFO ["<<argv[0] << ":main] Get configuration for " << conn_iter->first << "." << std::endl;
	}
	if (conn_iter->first.find("ROD_")==0) {
	  std::shared_ptr<const PixLib::PixModuleGroup> obj( a_tag.configuredModuleGroupPtr(conn_iter->first));
	  if (obj.get()) {
	    if (verbose>0) {
	      std::cout << "INFO ["<<argv[0] << ":main] Got configuration for " << conn_iter->first
			<< (a_tag.isTemporaryCfg(conn_iter->first) ? " (temproary) " : "")
			<< "." << std::endl;
	    }
	    if (verbose>3) {
	      const_cast<PixLib::PixModuleGroup &>(*obj).config().dump(std::cout);
	      if (const_cast<PixLib::PixModuleGroup &>(*obj).getPixBoc() && const_cast<PixLib::PixModuleGroup &>(*obj).getPixBoc()->getConfig()) {
	      const_cast<PixLib::PixModuleGroup &>(*obj).getPixBoc()->getConfig()->dump(std::cout);
	      }
	      if (const_cast<PixLib::PixModuleGroup &>(*obj).getPixController() ) {
		const_cast<PixLib::PixModuleGroup &>(*obj).getPixController()->config().dump(std::cout);
	      }
	    }
	    else if (verbose>2) {
	      dumpConfig(const_cast<PixLib::PixModuleGroup &>(*obj).config());
	      if (const_cast<PixLib::PixModuleGroup &>(*obj).getPixBoc() && const_cast<PixLib::PixModuleGroup &>(*obj).getPixBoc()->getConfig()) {
		dumpConfig(*(const_cast<PixLib::PixModuleGroup &>(*obj).getPixBoc()->getConfig()));
	      }
	      if (const_cast<PixLib::PixModuleGroup &>(*obj).getPixController() ) {
		dumpConfig(const_cast<PixLib::PixModuleGroup &>(*obj).getPixController()->config());
	      }
	    }
	    PixA::Revision_t rev2 = (dest_rev> 0 ? dest_rev : time(0) );
	    if (store) {
	      if (verbose>0) {
		std::cout << "INFO ["<<argv[0] << ":main] store new revision " << rev2 << " for " << conn_iter->first 
			  << "." << std::endl;
	      }
	      new_tag.storeConfig(*obj, rev2, "S00001_Tmp", false /* do not cache this config.*/);
	      if (verbose>1) {
		PixA::PixConnObjConfigDbManager::dumpRevisionsOfInstance();
	      }
	    }
	  }

	}
	else {
	  PixA::ConfigHandle config_handle( (conn_iter->first.find("ROD_")==0 ? 
					     a_tag.bocConfigHandle(conn_iter->first) : 
					     (conn_iter->first.find("Disable")==0 ?  
					      a_tag.disableConfigHandle(conn_iter->first) :
					      a_tag.moduleConfigHandle(conn_iter->first)))
					    );
	  if (config_handle) {
	    PixA::ConfigRef config_ref(config_handle.ref());
	    if (verbose>0) {
	      std::cout << "INFO ["<<argv[0] << ":main] Got configuration for " << conn_iter->first 
			<< (a_tag.isTemporaryCfg(conn_iter->first) ? " (temporary) " : "")
			<< "." << std::endl;
	    }
	    if (verbose>3) {
	      const_cast<PixLib::Config &>(config_ref.config()).dump(std::cout);
	    }
	    else if (verbose>2) {
	      dumpConfig(const_cast<PixLib::Config &>(config_ref.config()));
	    }
	    PixA::Revision_t rev3 = (dest_rev> 0 ? dest_rev : time(0) );
	    if (store) {
	      if (verbose>0) {
		std::cout << "INFO ["<<argv[0] << ":main] store new revision " << rev3 << " for " << conn_iter->first << "." << std::endl;
	      }
	      new_tag.storeConfig(config_ref.config(), rev3, "S00001_Tmp", false /* do not cache this config.*/);
	      if (verbose>1) {
		PixA::PixConnObjConfigDbManager::dumpRevisionsOfInstance();
	      }
	    }
	  }
	  else {
	    std::cout << "ERROR ["<<argv[0]<< ":main] No configuration for " <<  conn_iter->first << "."<<std::endl;
	  }
	}
      }
    }
    }
    }
  }
  catch (PixLib::ConfigException &err) {
    std::cerr << "ERROR [main] Caught exception : " << err.getDescriptor() << std::endl;   
  }
  return 0;
}
