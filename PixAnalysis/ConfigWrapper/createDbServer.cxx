#include <PixDbServer/PixDbServerInterface.h>

namespace PixA {

  std::shared_ptr<PixLib::PixDbServerInterface> createDbServer(IPCPartition &db_server_partition, const std::string &db_server_name)
  {
    if (!db_server_name.empty()) {
      if (db_server_partition.isValid()) {
	try {
	  bool ready=false;
	  std::shared_ptr<PixLib::PixDbServerInterface> db_server( new PixLib::PixDbServerInterface(&db_server_partition,
												      db_server_name,
												      PixLib::PixDbServerInterface::CLIENT,
												      ready) );

	  if (ready) {
	    db_server->ready();
	    std::cout << "INFO [setDbServer] db server  " << db_server_name << " in partition " << db_server_partition.name() << " is ready." << std::endl;
	    ready=true;
	    //	  PixA::ConnectivityManager::useDbServer(db_server);
	    return db_server;
	  }

	}
	catch (CORBA::TRANSIENT& err) {
	}
	catch (CORBA::COMM_FAILURE &err) {
	}
	catch (std::exception &err) {
	}

	std::cout << "INFO [setDbServer] Db server " << db_server_name << " of partition " << db_server_partition.name()  << " is not ready." << std::endl;
      }
      else {
	std::cout << "INFO [setDbServer] No valid partition given." << std::endl;
      }
    }
    return std::shared_ptr<PixLib::PixDbServerInterface>();
  }

}
