/* Dear emacs, this is -*-c++-*- */
#ifndef _PixA_IRevisionIndex_h_
#define _PixA_IRevisionIndex_h_

#include <vector>
#include <set>
#include <string>
#include <climits>

namespace PixA {

#ifndef _PixA_Connectivity_h_
  typedef unsigned int Revision_t;
#endif

  static const Revision_t REV_MAX = UINT_MAX;

  class IRevisionProcessor
  {
  public:
    virtual ~IRevisionProcessor() {}
    /**
     * @param till the validity range will be set to REV_MAX in case the limit is currently not known.
     * if till is set to REV_MAX the revision is at least valid until the current_upper_revision_limit
     */
    virtual void process(const std::string &name, const std::string &data, Revision_t since, Revision_t till, unsigned short representation) = 0;

  };

  class IRevisionIndex
  {
  public:

    enum EObjectType {kModule, kModuleGroup, kTrigController, kDisable, kNTypes};

    virtual ~IRevisionIndex() {}

    virtual void getIdTags(std::vector<std::string> &id_tag_out) = 0;

    virtual void getTags(const std::string &id_tag, std::vector<std::string> &tag_out) = 0;

    virtual void getRevisions(const std::string &id_tag, const std::string &cfg_tag, const std::string &conn_name, std::set<Revision_t> &revisions_out) = 0;

    virtual void getNames(const std::string &id_tag, const std::string &cfg_tag, EObjectType type, std::set<std::string> &names) = 0;


    /**
     * @param current_upper_revision_limit after scanning there cannot appear new revisions below this boundary.
     */
    virtual void processRevision(const std::string &id_tag,
				 const std::string &cfg_tag,
				 Revision_t target_revision,
				 Revision_t lower_revision_bound,
				 Revision_t current_upper_revision_limit,
				 IRevisionProcessor &processor) = 0;

    /**
     * @return true on success.
     */
    virtual bool addRevision(const std::string &id_tag,
			     const std::string &cfg_tag,
			     const std::string &type_name,
			     const std::string &name,
			     bool is_in_coral,
			     Revision_t /*revision*/,
			     const std::string &data) = 0;



  };

}
#endif
