#include "OnDemandConfig.h"
#include "OnDemandConfGrp.h"

namespace PixA {

  ObjStat_t OnDemandConfig::s_stat("OnDemandConfig");

  OnDemandConfig::~OnDemandConfig() {
    if (isRoot()) {
      std::cout <<"DEBUG [OnDemandConfig::dtor] Destructor OnDemandConfig (path: \""  << pathName() << "\")\n";
    }

    // clean up ConfCroups
    for(ConfGrpList_t::iterator group_iter=m_group.begin();
	group_iter != m_group.end();
	group_iter++) {
      delete group_iter->second;
    }

    // cleanup sub configs
    for(ConfigList_t::iterator config_iter=m_subConfigs.begin();
	config_iter != m_subConfigs.end();
	config_iter++) {
      delete config_iter->second;
    }
//      std::cout<<"quit from ::dtor"<<std::endl;
    s_stat.dtor();
//      std::cout<<"quit from ::dtor 2"<<std::endl;
  }

  IConfGrp &OnDemandConfig::operator[](const std::string &name)  {
    ConfGrpList_t::iterator group_iter = m_group.find(name);
//     std::cout << "Poi siamo qui 1"<< std::endl;
    if (group_iter == m_group.end()) {
      OnDemandConfGrp *conf_grp = new OnDemandConfGrp( *this , name );

//       std::cout << "INFO [OnDemandConfig::operator[]] got conf group object " << name
// 		<< std::endl;
      updateConfigName();

      std::pair<ConfGrpList_t::iterator, bool> ret = m_group.insert(std::make_pair(name,conf_grp));
  //      std::cout << "Poi siamo qui 1_0"<< std::endl;

      if (!ret.second) {
	std::stringstream message;
	message << "FATAL [OnDemandConfGrp::operator[]] Failed to insert the new config group of name " << name
		<< " of record " << pathName() << " into the cache."  << std::endl;
	throw ConfigException(message.str());
      }
      return *(ret.first->second);
    }
    return *(group_iter->second);
  }

  IConfig &OnDemandConfig::createAll() {
 //     std::cout << "Poi siamo qui 2"<< std::endl;
    if (m_configContainer->m_isComplete) return *m_configContainer;

    try {
    //    bool was_empty = m_group.empty();
    std::vector<std::string>  field_list;
    fillListOfFields(field_list,false);

    std::set<std::string> processed;
    for(std::vector<std::string>::iterator field_iter=field_list.begin();
	field_iter != field_list.end();
	field_iter++) {
      std::string field_name(*field_iter);
      std::string::size_type start_pos = field_name.rfind("/");
      if (start_pos != std::string::npos && start_pos<field_name.size()) {
	// field_name.erase(0, pos+1);
	//	std::cout << "INFO [OnDemandConfig::createAll] create conf group from field : " << std::string(field_name,start_pos+1) << std::endl;
	std::string::size_type pos = field_name.find("_",start_pos+1);
	if (pos != std::string::npos && pos<field_name.size()) {
	  field_name=std::string(field_name,start_pos+1,pos-start_pos-1);
	  //	  std::cout << "INFO [OnDemandConfig::createAll] create conf group from field : " << field_name << std::endl;
	  if (processed.find(field_name) == processed.end()) {
//          std::cout << "Poi siamo qui 2a"<< std::endl;
	    operator[](field_name).createAll();
//          std::cout << "Poi siamo qui 2b"<< std::endl;
	    processed.insert(field_name);
	  }
	}
      }
    }

    if (className()!="PixScanData" && className()!="PixScan") {

      // The Scan config does not contain sub configs (still the case ?)
      // but directories for the histograms.
      // The latter should not be interpreted as a sub config. Since the 
      // histograms of old files cannot be easily identified. It is
      // assumed that the scan config does not contain sub configs.

      std::shared_ptr<PixLib::DbRecord> record_auto;
      for ( PixLib::dbRecordIterator record_iter = recordBegin();
	    record_iter != recordEnd();
	    record_iter++ ) {
	PixLib::DbRecord *record = (*db()->DbProcess(record_iter, PixLib::PixDb::DBREAD));
	if (haveToCleanUp()) {
	  record_auto = std::shared_ptr<PixLib::DbRecord>(record); 
	}
//          std::cout << "Poi siamo qui 2c"<< std::endl;
	subConfig(record->getName()).createAll();
//          std::cout << "Poi siamo qui 2d"<< std::endl;
      }
    }
    m_configContainer->m_isComplete=true;
    }
    catch(PixLib::PixDBException &err) {
      std::stringstream message;
      message << "FATAL [OnDemandConfig::createAll] Failed to load full config : "
      << err.message;
      throw ConfigException(message.str());
    }
 //     std::cout << "Poi siamo qui 2_0"<< std::endl;
    return *m_configContainer;
  }

  IConfig &OnDemandConfig::subConfig(const std::string &name)  {
//      std::cout << "Poi siamo qui 3"<< std::endl;
    ConfigList_t::iterator config_iter = m_subConfigs.find(name);
    //    std::cout << "INFO [OnDemandConfig::subConfig] " << static_cast<void *>(m_config) <<" get sub config " << name
    //	      << " from config \"" << m_config->m_confName << "\" / \"" << m_config->m_confType << "\""
    //	      << std::endl;
    std::string::size_type class_name_pos = name.find("/");
    std::string reduced_name( class_name_pos == std::string::npos ? name : name.substr(0,class_name_pos) );

    if (config_iter == m_subConfigs.end()) {
      try {
	PixLib::dbRecordIterator a_record_iter = findRecord(reduced_name);
	if (a_record_iter == recordEnd() ) {
	  a_record_iter = findRecord(name);
	}
	if (a_record_iter == recordEnd() ) {
	  std::stringstream message;
	  message << "FATAL [OnDemandConfig::subConfig] No sub Config of name = " << name
		  << " in record " << pathName() << "."  << std::endl;
	  throw ConfigException(message.str());
	}
	updateConfigName();
	OnDemandConfig *config = new OnDemandConfig( *this , a_record_iter, name );
	std::pair<ConfigList_t::iterator, bool> ret = m_subConfigs.insert(std::make_pair(name,config));
	if (!ret.second) {
	  std::stringstream message;
	  message << "FATAL [OnDemandConfig::subConfig] Failed to insert the new config of name " << name
		  << " of record " << pathName() << " into the cache."  << std::endl;
	  throw ConfigException(message.str());
	}
	return *(ret.first->second);
      }
      catch(PixLib::PixDBException &err) {
	  std::stringstream message;
	  message << "FATAL [OnDemandConfig::subConfig] Failed to get  " << name
		  << " of record " << pathName() << " : "
		  << err.message;
	  throw ConfigException(message.str());
      }
    }
    return *(config_iter->second);
  }

}
