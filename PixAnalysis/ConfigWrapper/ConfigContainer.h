#ifndef _PixA_ConfigContainer_h_
#define _PixA_ConfigContainer_h_

#include <map>
#include <string>

#include "PixDbAccessor.h"
#include "IConfig.h"
#include "IConfGrp.h"
#include "DbWrapper.h"
#include <Config/Config.h>

#include "ObjStat_t.h"

namespace PixA {


  class OnDemandConfGrp;
  class OnDemandConfig;
  class ConfGrpContainer;

  class ConfigContainer : public IConfig
  {
    friend class PixConnectivityWrapper;
    friend class DbManager;
    friend class DbConfigHandle;
    friend class OnDemandConfGrp;
    friend class OnDemandConfig;
    friend class ConfGrpContainer;

  protected:
    typedef std::map<std::string, ConfGrpContainer * > ConfGrpList_t;
    typedef std::map<std::string, ConfigContainer * > ConfigList_t;

    /** Bootstrap a config hierarchy.
     */
    ConfigContainer() : m_config(new PixLib::Config("")), m_isComplete(false) 
      { //std::cout<<"si, sono chiamato 1"<<std::endl;
          s_stat.ctor();
      }

    /** Bootstrap a config hierarchy.
     */
    ConfigContainer(const std::string &name, const std::string &class_name)
      : m_config(NULL), m_isComplete(false) {
         // std::cout<<"si, sono chiamato 2"<<std::endl;
          m_config = new PixLib::Config(name,class_name);
          s_stat.ctor();
    }

    /** Create a sub config specified by the name and the record iterator from an existing config object.
     * @param config a reference of the config which contains the subconfig specified by the name end the iterator
     * @param record_iter a valid record iterator which points to the record of the given name
     * @param name the name of the record
     * the record_iter and name must much. Otherwise the result will be undefined.
     */
    ConfigContainer(ConfigContainer &config, const std::string &name, const std::string &class_name)
      : IConfig(config), m_config(NULL), m_isComplete(false) {
         // std::cout<<"si, sono chiamato 3"<<std::endl;
	unsigned int index=config.m_config->subConfigSize();
	config.m_config->addConfig(name,class_name);

	if (config.m_config->subConfigSize() != index+1  ) {

	  std::cout << "INFO [ConfigContainer::ctor(ConfigContainer &, PixLib::dbRecordIterator &, const std::string &)] sub configs : ";
	  for (ConfigList_t::const_iterator iter=m_subConfigs.begin();
	       iter != m_subConfigs.end();
	       iter++) {
	    std::cout << iter->first << " ";
	  }
	  std::cout <<  std::endl;

	  std::cout << "INFO [ConfigContainer::ctor(ConfigContainer &, PixLib::dbRecordIterator &, const std::string &)] sub configs in real config object : ";
	  for (std::map<std::string, PixLib::Config *>::const_iterator iter=config.m_config->m_config.begin();
	       iter != config.m_config->m_config.end();
	       iter++) {
	    std::cout << iter->first << " " ;
	  }
	  std::cout <<  std::endl;

	}

	std::map<std::string, PixLib::Config *>::iterator sub_conf_iter = config.m_config->m_config.find(name);
	if (sub_conf_iter == config.m_config->m_config.end()) {
	  std::stringstream message;
	  message << "FATAL [ConfigContainer::ctor] Failed to add sub config name = " << name
		  << " to " << config.m_config->m_confName << "."  << std::endl;
	  throw ConfigException(message.str());
	}
  //std::cout<<"si, sono chiamato 4"<<std::endl;
	config.addSubConfig(*this,name);
	m_config = sub_conf_iter->second;
	s_stat.ctor();
      }

    /** Add and return the real config group object.
     */
    PixLib::ConfGroup &addGroup(ConfGrpContainer &group_container, const std::string &name) {
    //    std::cout<<"si, sono chiamato 5"<<std::endl;
      std::pair<ConfGrpList_t::iterator, bool> ret = m_group.insert( std::make_pair(name, &group_container) );
      if (!ret.second) {
	std::stringstream message;
	message << "FATAL [ConfigContainer::ctor] Failed to add group name = " << name
		<< " to " << m_config->m_confName << ".";
	throw ConfigException(message.str());
      }
      unsigned int index=m_config->size();
      m_config->addGroup(name);
      assert(index+1 == static_cast<unsigned int>(m_config->size()));
      PixLib::ConfGroup &new_group = (*m_config)[index];
      assert( new_group.name() != "__TrashConfGroup__");
      return new_group;
    }

    void addSubConfig(ConfigContainer &container, const std::string &name) {
     //   std::cout<<"si, sono chiamato 6"<<std::endl;
      std::pair<ConfigList_t::iterator, bool> ret = m_subConfigs.insert(std::make_pair(name,&container));
      if (!ret.second) {
	std::string message;
	message = "FATAL [ConfigContainer::ctor] Failed to add sub config name = ";
	message += name;
	message +=" to ";
	message += m_config->m_confName;
	message += ".";

	throw ConfigException(message);
      }
    }

  public:
    /** Delete all subConfigs and 
     */
    ~ConfigContainer();

    /** Return a reference to the config group of the given name.
     * If the config group does not yet exist in the cache. A new
     * config group interface is created. The latter will open the
     * database if the database is not yet opened.
     */
    IConfGrp &operator[](const std::string &name) ;

    /** Return a reference to the sub config of the given name.
     * If the sub config does not yet exist in the cache. A new
     * config group interface is created. The latter will open the
     * database if the database is not yet opened.
     */
    IConfig &subConfig(const std::string &name) ;

    /** Create the whole config and return a reference to it.
     */
      
    PixLib::Config &config() {
     //   std::cout<<"si, lo chiamo io createall()"<<std::endl;
        createAll();
     // std::cout<<"si, l'ho chiamato io createall()"<<std::endl;
 return *m_config;}

    void use()  { IConfigBase::useNode(); }
    void done() { IConfigBase::doneWithNode(); }

    IConfig &createAll(); // { assert( m_isComplete); return *this; }
    
  protected:
    void updateConfigName(const std::string &name, const std::string &class_name) {
      m_config->name(name);
      m_config->type(class_name);
    }

    ConfGrpList_t   m_group;
    ConfigList_t    m_subConfigs;
    PixLib::Config *m_config;
    bool            m_isComplete;
    static ObjStat_t s_stat;
  };


}

#endif


