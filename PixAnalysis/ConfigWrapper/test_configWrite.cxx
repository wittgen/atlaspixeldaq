#include "DbManager.h"
#include <RootDb/RootDb.h>
#include "ConfGrpRefUtil.h"
#include "Connectivity.h"
#include "ConnectivityManager.h"
#include "ConnectivityUtil.h"
#include <PixModule/PixModule.h>
#include "test_compareConfObj.h"
#include "test_configSize.h"

#ifdef HAVE_MEMSTAT
#  include <PixModule/memstat.h>
#endif

int main(int argc, char ** argv)
{
  unsigned int tag_arg_index[4];
  unsigned int module_name_index=0;
  if (argc==3 || argc==4) {
    for (unsigned int tag_i=0; tag_i<3; tag_i++) {
      tag_arg_index[tag_i]=1;
    }
    tag_arg_index[3]=2;
    if (argc==4) {
      module_name_index=3;
    }
  }
  else if (argc>=5 && argc<=6) {
    for (unsigned int tag_i=0; tag_i<4; tag_i++) {
      tag_arg_index[tag_i]=tag_i+1;
    }
    if (argc==6) {
      module_name_index=5;
    }
  }
  else {
    exit(-1);
  }
  
  try {
    PixA::ConnectivityRef conn(PixA::ConnectivityManager::getConnectivity( argv[tag_arg_index[0]],
									   argv[tag_arg_index[1]],
									   argv[tag_arg_index[2]],
									   argv[tag_arg_index[3]],
									   /*time(NULL)-3600*24*6*/ 0));
    PixA::ConnectivityRef conn_ref;
    bool mod_read=false;
    bool mod_write=false;
    bool read_ref=false;

    if (module_name_index>0 ) {
      if (strcmp(argv[module_name_index],"-w")==0) {
	mod_write = true;
      }
      else if (strcmp(argv[module_name_index],"-r")==0) {
	mod_read=true;
      }
      else if (strcmp(argv[module_name_index],"-ro")==0) {
	mod_read=true;
	read_ref=true;
      }
    }

    if (!mod_write || read_ref) {

      struct tm a_time;

      a_time.tm_sec=0;
      a_time.tm_min=0;
      a_time.tm_hour=0;
      a_time.tm_mday=30;
      a_time.tm_mon=4;
      a_time.tm_year=2007-1900;
      a_time.tm_wday=0;
      a_time.tm_wday=0;
      a_time.tm_isdst=0;
      time_t time_today=time(NULL);
      time_t ref_time = mktime(&a_time);
      
      std::cout << "today = " << time_today << "(" << ctime(&time_today) << ") ref = " 
		<< ref_time << "(" << ctime(&ref_time) << ")" << std::endl;

      conn_ref = (PixA::ConnectivityManager::getConnectivity( argv[tag_arg_index[0]],
							      argv[tag_arg_index[1]],
							      argv[tag_arg_index[2]],
							      argv[tag_arg_index[3]],
							      ref_time));
    }


    if (module_name_index>0 && strlen(argv[module_name_index])>0 && !mod_read && !mod_write) {

      std::string module_name;
      if(argv[module_name_index][0]=='*') {
	module_name=std::string(&(argv[module_name_index][1]));
	mod_write=true;
      }
      else {
	module_name=std::string((argv[module_name_index]));
      }
      PixA::ConfigRef mod_conf_ref=conn.modConf(module_name).ref();
#ifdef HAVE_MEMSTAT
      std::cout << "before full config." << std::endl;
      memstat();
#endif
      //@todo the returned object should in principle be const
      PixLib::Config &full_config = const_cast<PixLib::Config &>(mod_conf_ref.config());
      get_size(full_config,true);
 #ifdef HAVE_MEMSTAT
      std::cout << "after full config." << std::endl;
      memstat();
#endif

      PixA::ConfigHandle module_config_handle( conn.modConf(module_name) );
      PixA::ConfigRef module_config( module_config_handle.ref() );

      PixA::ConfigRef full_module_config ( module_config.createFullCopy() );
      PixA::ConfigHandle full_config_handle( full_module_config);
      PixLib::Config &conf=const_cast<PixLib::Config &>(full_module_config.config());
      get_size(conf,true);
      if (!mod_write) {
	if (conn_ref) {
	  PixA::ConfigHandle module_config_ref_handle( conn_ref.modConf(module_name) );
	  PixA::ConfigRef module_config_ref( module_config_ref_handle.ref() );
	  
	  PixA::ConfigRef full_module_config_ref ( module_config_ref.createFullCopy() );
	  PixA::ConfigHandle full_config_handle_ref( full_module_config_ref);

	  const PixLib::Config &orig_conf_ref=full_module_config_ref.config();
	  std::shared_ptr<PixLib::PixModule> a_module( new PixLib::PixModule(NULL,NULL, "trick", "", module_name) );
	  a_module->config() = orig_conf_ref;
	  PixLib::Config &conf_ref = a_module->config();

	  confVal<int>(conf_ref.subConfig("PixMcc_0")["Registers"]["CSR_OutputPattern"])=0xa5a5a5a5;
	  setConfMatrixVal<TrimVal_t>(conf_ref.subConfig("PixFe_0").subConfig("Trim_0/Trim")["Trim"]["FDAC"],0,0,255);
	  // 	  std::cout << conf.subConfig("PixFe_0").name() << std::endl;
	  // 	  std::cout << conf.subConfig("PixFe_0").subConfig("Trim_0/Trim").name() << std::endl;
	  // 	  std::cout << conf.subConfig("PixFe_0").subConfig("Trim_0/Trim")["Trim"].name() << std::endl;
	  // 	  std::cout << conf.subConfig("PixFe_0").subConfig("Trim_0/Trim")["Trim"]["FDAC"].name() << std::endl;
// 	  std::cout << " conf = " << static_cast<int>(confMatrix<TrimVal_t>(conf.subConfig("PixFe_0").subConfig("Trim_0/Trim")["Trim"]["FDAC"],18,160)[0][0])
// 		    << " conf_ref = " << static_cast<int>(confMatrix<TrimVal_t>(conf_ref.subConfig("PixFe_0").subConfig("Trim_0/Trim")["Trim"]["FDAC"],18,160)[0][0])
// 		    << std::endl;
	  if (conf != conf_ref) {
	    conf.dump(std::cout);
	    conf_ref.dump(std::cout);
	  }
	  else {
	    std::cout << module_name << "old and new config agree."
		      << std::endl;
	  }
	}
	else {
	  std::cout << "ERROR [main] cannot compare " << module_name << " because there is no reference connectivity."
		      << std::endl;
	}
      }

      std::cout << module_name << " :: PixMcc_0/Registers.CSR_OutputPattern = " 
		<< confVal<int>(conf.subConfig("PixMcc_0")["Registers"]["CSR_OutputPattern"])
		<< std::endl;
      if (mod_write) {
	confVal<int>(conf.subConfig("PixMcc_0")["Registers"]["CSR_OutputPattern"])=0xa5a5a5a5;
	setConfMatrixVal<TrimVal_t>(conf.subConfig("PixFe_0").subConfig("Trim_0/Trim")["Trim"]["FDAC"],0,0,255);
	// @todo implement writing of configurations
	assert( false );
	//	conn.writeModConf(module_name);
      }
    }
    else {
      if (read_ref) {
	conn = conn_ref;
      }
    PixA::CrateList::const_iterator begin_crate=conn.crates().begin();
    PixA::CrateList::const_iterator end_crate=conn.crates().end();

     unsigned long init_vm_size=0;
#ifdef MEMSTAT
    init_vm_size=vmsize(false);
#endif    
    unsigned long sum2=0;
    unsigned long last_vm_size=init_vm_size;
    unsigned long n_modules=0;
    unsigned int max_n_mods=100;
    for (PixA::CrateList::const_iterator crate_iter = begin_crate;
	 crate_iter != end_crate;
	 ++crate_iter) {
      std::cout << PixA::connectivityName(crate_iter) << std::endl;
      PixA::RodList::const_iterator begin_rod = rodBegin(crate_iter);
      PixA::RodList::const_iterator end_rod = rodEnd(crate_iter);
      if (n_modules>max_n_mods ) break;
      for (PixA::RodList::const_iterator rod_iter = begin_rod;
	   rod_iter != end_rod;
	   ++rod_iter) {
	std::cout << "  " << PixA::connectivityName(rod_iter) << std::endl;
      if (n_modules>max_n_mods) break;
      
      PixA::Pp0List::const_iterator begin_pp0 = PixA::pp0Begin(rod_iter);
      PixA::Pp0List::const_iterator end_pp0 = PixA::pp0End(rod_iter);
      for (PixA::Pp0List::const_iterator pp0_iter = begin_pp0;
	   pp0_iter != end_pp0;
	   ++pp0_iter) {
	if (n_modules>max_n_mods) break;
	  
	  std::cout << "    " << PixA::connectivityName(pp0_iter)  << std::endl;
	  PixA::ModuleList::const_iterator begin_mod = PixA::modulesBegin(pp0_iter);
	  PixA::ModuleList::const_iterator end_mod = PixA::modulesEnd(pp0_iter);
	  for (PixA::ModuleList::const_iterator mod_iter = begin_mod;
	       mod_iter != end_mod;
	       ++mod_iter) {
	    if (n_modules>max_n_mods) break;
	    std::cout << "      "<< PixA::connectivityName(mod_iter)  << std::endl;

	    if (mod_read || mod_write) {
	      //	      PixLib::Config &conf=conn.fullModuleConfig( PixA::connectivityName(mod_iter));
	      //PixLib::Config &conf=conn.fullModuleConfig( PixA::connectivityName(mod_iter));

	      PixA::ConfigRef mod_conf_ref=conn.modConf(mod_iter).ref();
#ifdef HAVE_MEMSTAT
      //std::cout << "before full config." << std::endl;
	      memstat();
	      //	      vmsize();
#endif
	      //PixLib::Config &conf = const_cast<PixLib::Config &>(mod_conf_ref.config());
#ifdef HAVE_MEMSTAT
      //	      std::cout << "after full config." << std::endl;
	      memstat();
#endif
	      unsigned long new_vm_size=0;
#ifdef HAVE_MEMSTAT
              new_vm_size=vmsize(false);
#endif
	      unsigned long diff_vm_size = new_vm_size - last_vm_size;
	      sum2+=diff_vm_size * diff_vm_size;
	      n_modules++;
	      last_vm_size = new_vm_size;
	      std::cout << n_modules << " : " << last_vm_size << "  ::  " << static_cast<double>(diff_vm_size) / 1024.  << std::endl;

	      //	      confVal<int>(conf.subConfig("PixMcc_0")["Registers"]["CSR_OutputPattern"])=0xa5a5a5a5;
	      //	      std::cout << PixA::connectivityName(mod_iter) << " :: PixMcc_0/Registers.CSR_OutputPattern = " 
	      //			<< confVal<int>(conf.subConfig("PixMcc_0")["Registers"]["CSR_OutputPattern"])
	      //			<< std::endl;
	      if (mod_write) {
		//@todo implement writing of configurations
		assert(false);
		//		conn.writeModConf( PixA::connectivityName(mod_iter) );
	      }
	    }
	  }
	}
      }
    }
    std::cout << " n_modules = " << n_modules << std::endl;
    std::cout << " initial vm size = " << init_vm_size << std::endl;
    std::cout << " final vm size =   " << last_vm_size << std::endl;
    if (n_modules>1) {
      unsigned long vm_growth=(last_vm_size-init_vm_size)/n_modules;
      std::cout << " vm growth / mod =   " << vm_growth <<  " +-" << sqrt(sum2-(last_vm_size-init_vm_size)*vm_growth)/(n_modules-1) << std::endl;
    }

    }
  }
  catch (PixA::ConfigException &err) {
    std::cout << "FATAL [main] exception : " << err << std::endl;
    return -1;
  }
  catch (std::exception &err) {
    std::cout << "FATAL [main] std exception : " << err.what() << std::endl;
    return -1;
  }
  return 0;
}
