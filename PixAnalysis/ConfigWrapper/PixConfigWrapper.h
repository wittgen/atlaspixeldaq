#ifndef _PixConfigWrapper_h_
#define _PixConfigWrapper_h_

#include "pixa_exception.h"
#include <string>
#include <Config/Config.h>
#include "IConfig.h"
#include <sstream>
#include "ConfigRef.h"
#include "ConfigHandle.h"


#ifdef DEBUG
#  include "ObjList.h"
#endif


namespace PixA {
  class IConfGrp;

  class PixConfigWrapper : public IConfig
  {
  public:
    PixConfigWrapper(PixLib::Config &config) : m_config(&config) 
      {  
#     ifdef DEBUG	
	s_objList.add(this);  
#     endif
      }

  protected:
    PixConfigWrapper(PixConfigWrapper &parent_config, PixLib::Config &sub_config) : IConfig(parent_config), m_config( &sub_config ) 
      {  
#     ifdef DEBUG	
	s_objList.add(this);  
#     endif
      }

  public:

    virtual ~PixConfigWrapper() 
      { 
#     ifdef DEBUG
	s_objList.remove(this); 
#     endif
      }

    void use()  {IConfigBase::useNode();}
    void done() {IConfigBase::doneWithNode(); if (IConfigBase::use_count()==0) delete this; }

    //  virtual const IConfGrp &operator[](const std::string &name) throw (ConfigException) const = 0;
    IConfGrp &operator[](const std::string &name) ;

    //  virtual const IConfig &subConfig(const std::string &name) const throw (ConfigException) = 0;
    IConfig &subConfig(const std::string &name)  {
      PixLib::Config &sub_config=m_config->subConfig(name);
      if (sub_config.name() == s_trashConfig) {
	std::stringstream message;
	message << "FATAL [PixConfigWrapper::subConfig] No sub Config of name = " << name
		<< " in config " << m_config->name() << "."  << std::endl;
	throw ConfigException(message.str());
      }
      return *(new PixConfigWrapper(*this, sub_config));
    }

    static ConfigHandle wrap(PixLib::Config &config) {
      return *(new PixConfigWrapper(config));
    }

    /** Create the whole config and return a reference to it.
     */
    PixLib::Config &config() {
      return *m_config;
    }

    IConfig &createAll() { return *this; }

  private:
    PixLib::Config *m_config;
    static std::string s_trashConfig;
    static std::string s_trashConfGrp;

#ifdef DEBUG 
  static ObjList s_objList;
 public: 
  static void stat() {s_objList.stat();}
#endif

  };

}

#endif
