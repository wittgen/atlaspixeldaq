#include "ConfGroupUtil.h"

bool updateConfList(PixLib::ConfGroup &dest, const PixLib::ConfGroup &src) {
  //bool ret=false;
  //  std::cout << "INFO [updateConfList] " << dest.name() << std::endl;
  for (unsigned int i = 0; i < dest.size(); i++) {
    if (dest[i].type()==PixLib::ConfObj::LIST) {
      //      std::cout << "INFO [updateConfList] " << dest.m_param[i]->name() << " / " << dest.m_param[i]->type() << std::endl;
      for (unsigned int j = 0; j< src.size(); j++) {
	if (dest[i].name() == src[j].name()) {
	  //	  std::cout << "INFO [updateConfList] types : " << dest.m_param[i]->type() << " =?= " << src.m_param[j]->type() << std::endl;
	  const PixLib::ConfInt *src_conf_int = dynamic_cast<const PixLib::ConfInt *>(&src[j]);
	  if (src_conf_int) {
	    PixLib::ConfInt *dest_conf_int = dynamic_cast<PixLib::ConfInt *>(&dest[i]);
	    if (dest_conf_int) {
	      dest_conf_int->setValue( const_cast<PixLib::ConfInt *>(src_conf_int)->getValue());
	      //ret = true;
	    }
	  }
	  else {
	    const PixLib::ConfString *src_conf_string = dynamic_cast<const PixLib::ConfString *>(&src[j]);
	    if (src_conf_string) {
	      PixLib::ConfList *dest_conf_list = dynamic_cast<PixLib::ConfList *>(&dest[i]);
	      if (dest_conf_list) {
		std::map<std::string, int>::const_iterator symbol_iter = dest_conf_list->symbols().find( const_cast<PixLib::ConfString  *>(src_conf_string)->value() );
		if (symbol_iter != dest_conf_list->symbols().end()) {
		  dest_conf_list->setValue( symbol_iter->second );
		 // ret = true;
		}
	      }
	    }
	  }
	}
      }
    }
  }
  return true;
}

bool updateConfList(PixLib::Config &dest, const PixLib::Config &src)
{
  bool ret = false;
  for (unsigned int i = 0; i < dest.m_group.size(); i++) {
    for (unsigned int j = 0; j< src.m_group.size(); j++) {
      if (dest.m_group[i]->name() == src.m_group[j]->name()) {
	ret |= updateConfList(*(dest.m_group[i]), *(src.m_group[j]));
      }
    }
  }
  for ( std::map<std::string, PixLib::Config*>::iterator dest_iter = dest.m_config.begin();
	dest_iter  != dest.m_config.end();
	++dest_iter) {
    std::map<std::string, PixLib::Config*>::const_iterator src_iter = src.m_config.find(dest_iter->first);
    if (src_iter != src.m_config.end()) {
      ret |= updateConfList(*(dest_iter->second), *(src_iter->second));
    }
  }
  return ret;
}
