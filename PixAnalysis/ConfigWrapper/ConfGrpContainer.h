#ifndef _PixA_ConfGrpContainer_h_
#define _PixA_ConfGrpContainer_h_

#include <string>
#include <sstream>
#include <map>
#include <vector>
#include <stdexcept>
#include <iostream>

#include <Config/Config.h>
#include "IConfGrp.h"
#include "PixDbAccessor.h"
#include "DbWrapper.h"
#include "BlockWiseStorage.h"
#include "pixa_exception.h"

#include <Config/ConfObj.h>
#include <Config/ConfMask.h>
#include "ConfigContainer.h"

#include "ObjStat_t.h"

#ifndef TrimVal_t_h
#define TrimVal_t_h

namespace PixLib {
  typedef unsigned short TrimVal_t;
}

#endif

namespace PixA {

  class OnDemandConfGrp;
  class ConfigContainer;

  template <class T> class PixelMask : public PixLib::ConfMask<T> 
  { 
  public:
    static const unsigned int s_nCols = 18;
    static const unsigned int s_nRows = 160;

    static T maxVal();
    static T defVal();

    PixelMask<T>(const PixelMask<T> &a) : PixLib::ConfMask<T>(a) { 
      /*std::cout << "INFO [PixelMask::ctor] copy PixelMask " << static_cast<const void *>(&a) 
								     << " new = " << static_cast<void *>(this) << std::endl;*/
    }

    PixelMask<T>() : PixLib::ConfMask<T>( s_nCols,s_nRows,PixelMask<T>::maxVal(), PixelMask<T>::defVal()) {
      // std::cout << "INFO [PixelMask::ctor] new PixelMask " << static_cast<void *>(this) << " max = " << PixelMask<T>::maxVal()<< std::endl;
    }
    ~PixelMask<T>() {
      // std::cout << "INFO [PixelMask::dtor] delete PixelMask " << static_cast<void *>(this) << std::endl;
    }

  };

/** Class to create on demand configuration objects from PixDb fields.
 * The configuration objects will be cached. Thus for each object the
 * file will be accessed once.
*/
  class ConfGrpContainer : public IConfGrp
{
  friend class ConfigContainer;
  friend class OnDemandConfGrp;

protected:
  typedef std::map<std::string, PixLib::ConfObj * >   ConfObjList_t;

  //  ConfGrpContainer(ConfigRoot *root, DbWrapper &db,) : IConfGrp(root), PixDbAccessor(db) { }
  ConfGrpContainer(ConfigContainer &config, const std::string &name) : IConfGrp(config), m_isComplete(false)
    { s_stat.ctor();
    m_confGroup=&( config.addGroup(*this, name) );
    //    updateConfGroupName();
    }

 public:
  ~ConfGrpContainer() {
    //std::cout << "INFO [ConfGrpContainer::dtor] delete interface for conf group = " << m_confGroup->m_groupName << std::endl;
    // the conf objects are deleted by the destructor of the physical conf obj
    s_stat.dtor();
  };

  /** Create a configuration object of the given name from the PixDb field if the object has not been creted yet.
   * The objects are only partially created i.e. default values , the comments and the visibility flag
   * are missing. Moreover, Integer vectors with 18*160 elements will always be treated as matrices of 
   * unsigned short. So, if there happen to be integer vectors of exactly this size, they cannot be 
   * accessed correctly.
   */
  PixLib::ConfObj &operator[](const std::string &name)  {
    ConfObjList_t::const_iterator obj_iter = m_obj.find(name);
    if (obj_iter == m_obj.end()) {
	std::string message;
	message="FATAL [ConfGrpContainer::operator[]] No ConfObj of name ";
	message += name;
	message += " for group ";
	message += m_confGroup->name();
	message += ".";
	throw ConfigException(message);
    }
    return  *(obj_iter->second);

  }

  void use()  { IConfigBase::useNode(); }
  void done() { IConfigBase::doneWithNode(); }

  void createAll() { assert(m_isComplete); }

 protected:
  mutable BlockWiseStorage< int >                          m_intStorage;
  mutable BlockWiseStorage< unsigned char >                m_ucharStorage;
  mutable BlockWiseStorage< float >                        m_floatStorage;
  mutable BlockWiseStorage< std::string >                  m_stringStorage;
  mutable BlockWiseStorage< std::vector<bool> >            m_boolVectorStorage;
  mutable BlockWiseStorage< std::vector<int> >             m_intVectorStorage;
  mutable BlockWiseStorage< std::vector<unsigned int> >    m_uintVectorStorage;
  mutable BlockWiseStorage< std::vector<float> >           m_floatVectorStorage;
  // dbfield which are int or bool vectors of 18x160 elements are considered to be matrices:
  mutable BlockWiseStorage< PixelMask<bool> >              m_boolMatrixStorage;
  mutable BlockWiseStorage< PixelMask<PixLib::TrimVal_t> >    m_ushortMatrixStorage;

  ConfObjList_t   m_obj;
  bool            m_isComplete;

  void updateConfGroupName(const std::string &nameArg) {
    m_confGroup->name(nameArg);
  }

  PixLib::ConfGroup *m_confGroup;
  static ObjStat_t s_stat;
};

}
#endif
