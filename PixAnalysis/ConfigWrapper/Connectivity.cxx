#include "Connectivity.h"

namespace PixA {

  void ConnectivityRef::exceptionDoesNotExist(const std::string &location) const {
    std::string message("ERROR [ConnectivityRef] Connectivity object ");
    message += location;
    message += " does not exist.";
    throw ConfigException(message);
  }

  void ConnectivityRef::exceptionTimDoesNotExist(const std::string &location) const {
    std::string message("ERROR [ConnectivityRef] No tim for crate ");
    message += location;
    message += ".";
    throw ConfigException(message);
  }

  void ConnectivityRef::exceptionIllegalIterator(const std::string &iterator_type) const {
    std::string message("ERROR [ConnectivityRef] Illegal ");
    message += iterator_type;
    message += " iterator.";
    throw ConfigException(message);
  }

  std::shared_ptr<const PixLib::PixDisable> ConnectivityRef::getDisable(std::string disableName) const {
    try {
      return m_configDb.disablePtr(disableName);
    }
    catch (PixA::ConfigException &) {

    }

    // now try again with 0 as revision
    try {
      ConnObjConfigDbTagRef config_db( ConnObjConfigDb::sharedInstance().getTag(cfgIdTag(), cfgTag(), 0) );
      return config_db.disablePtr(disableName);
    }
    catch (PixA::ConfigException &err) {
      std::cerr << "FATAL [ConnectivityRef::getDisable] Failed to get disable " << disableName << " in tag " << cfgIdTag() << "/" << cfgTag()
		<< ". The following exception occurred : " << err.getDescriptor()
		<< std::endl;
    }
    return std::shared_ptr<const PixLib::PixDisable>();
  }

  // -- Get config
  // Get Module config

  ConfigHandle ConnectivityRef::modConf(const ModuleLocationBase &module_location) const {
    const PixLib::ModuleConnectivity *module_conn = findModule(module_location);
//    std::cout<<"location to connectivity "<<module_location<<std::endl;
    if (!module_conn) {
      exceptionDoesNotExist(module_location);
    }
    PixLib::EnumMCCflavour::MCCflavour mccFlv;
    PixLib::EnumFEflavour::FEflavour feFlv;
    int nFe;
//      std::cout<<"mcc: "<<(int)mccFlv<<" feflv: "<<(int)feFlv<<" fe num: "<<nFe<<std::endl;
    const_cast<PixLib::ModuleConnectivity*>(module_conn)->getModPars(mccFlv, feFlv, nFe);
//      std::cout<<"mcc: "<<(int)mccFlv<<" feflv: "<<(int)feFlv<<" fe num: "<<nFe<<std::endl;
    std::string name = const_cast<PixLib::ModuleConnectivity*>(module_conn)->prodId();
    std::stringstream a;
    a << "_flavour_" << ((int)mccFlv) << "-" << ((int)feFlv) << "-" << nFe;
    name += a.str();
//       std::cout<<"beccato 4"<<std::endl;
//      std::cout<<"name in connectivity is:"<<name<<std::endl;
    return m_moduleConfigDb.moduleConfigHandle( name );
  }

    
//********maria elena added
     std::string ConnectivityRef::modConf_name(const ModuleLocationBase &module_location) const {
        const PixLib::ModuleConnectivity *module_conn = findModule(module_location);
//        std::cout<<"location to connectivity "<<module_location<<std::endl;
        if (!module_conn) {
            exceptionDoesNotExist(module_location);
        }
        PixLib::EnumMCCflavour::MCCflavour mccFlv;
        PixLib::EnumFEflavour::FEflavour feFlv;
        int nFe;
//        std::cout<<"mcc: "<<(int)mccFlv<<" feflv: "<<(int)feFlv<<" fe num: "<<nFe<<std::endl;
        const_cast<PixLib::ModuleConnectivity*>(module_conn)->getModPars(mccFlv, feFlv, nFe);
//        std::cout<<"mcc: "<<(int)mccFlv<<" feflv: "<<(int)feFlv<<" fe num: "<<nFe<<std::endl;
        std::string name = const_cast<PixLib::ModuleConnectivity*>(module_conn)->prodId();
        std::stringstream a;
        a << "_flavour_" << ((int)mccFlv) << "-" << ((int)feFlv) << "-" << nFe;
        name += a.str();
 //       std::cout<<"name is: "<<name<<std::endl;
        return name;
    }
    std::string ConnectivityRef::modConf_name(ModuleList::const_iterator &module_iter) const {
        PixLib::EnumMCCflavour::MCCflavour mccFlv;
        PixLib::EnumFEflavour::FEflavour feFlv;
        int nFe;
//        std::cout<<"mcc: "<<(int)mccFlv<<" feflv: "<<(int)feFlv<<" fe num: "<<nFe<<std::endl;
        const_cast<PixLib::ModuleConnectivity*>(*module_iter)->getModPars(mccFlv, feFlv, nFe);
//        std::cout<<"mcc: "<<(int)mccFlv<<" feflv: "<<(int)feFlv<<" fe num: "<<nFe<<std::endl;
        std::string name = const_cast<PixLib::ModuleConnectivity*>(*module_iter)->prodId();
        std::stringstream a;
        a << "_flavour_" << ((int)mccFlv) << "-" << ((int)feFlv) << "-" << nFe;
        name += a.str();
//        std::cout<<"name is: "<<name<<std::endl;
        return name;
    }

//*************************************************
    
  ConfigHandle ConnectivityRef::modConf(ModuleList::const_iterator &module_iter) const {
    PixLib::EnumMCCflavour::MCCflavour mccFlv;
    PixLib::EnumFEflavour::FEflavour feFlv;
    int nFe;
    const_cast<PixLib::ModuleConnectivity*>(*module_iter)->getModPars(mccFlv, feFlv, nFe);
    std::string name = const_cast<PixLib::ModuleConnectivity*>(*module_iter)->prodId();
    std::stringstream a;
    a << "_flavour_" << ((int)mccFlv) << "-" << ((int)feFlv) << "-" << nFe;
    name += a.str();
//       std::cout<<"beccato 5"<<std::endl;
    return m_moduleConfigDb.moduleConfigHandle( name );
  }

  // Get BOC config

  ConfigHandle ConnectivityRef::bocConf(Pp0List::const_iterator &pp0_iter) const {
    return m_configDb.bocConfigHandle( rodLocation( pp0_iter ) );
  }

  ConfigHandle ConnectivityRef::bocConf(const Pp0LocationBase &pp0_location) const {
    const PixLib::Pp0Connectivity *pp0_conn = findPp0(pp0_location);
    if (!pp0_conn) {
      exceptionDoesNotExist(pp0_location);
    }
    return m_configDb.bocConfigHandle( rodLocation(pp0_conn) );
  }

  ConfigHandle ConnectivityRef::bocConf(RodList::const_iterator &rod_iter) const {
    return m_configDb.bocConfigHandle( rodLocation( rod_iter ) );
  }

  ConfigHandle ConnectivityRef::bocConfFromRodLocation(const std::string &rod_location) const {
    return m_configDb.bocConfigHandle( rod_location );
  }


  // ROD controller config

  ConfigHandle ConnectivityRef::rodConf(Pp0List::const_iterator &pp0_iter) const {
    return m_configDb.rodControllerConfigHandle( rodLocation(pp0_iter) );
  }

  ConfigHandle ConnectivityRef::rodConf(const Pp0LocationBase &pp0_location) const {
    const PixLib::Pp0Connectivity *pp0_conn = findPp0(pp0_location);
    if (!pp0_conn) {
      exceptionDoesNotExist(pp0_location);
    }

    return m_configDb.rodControllerConfigHandle( rodLocation(pp0_conn) );
  }

  ConfigHandle ConnectivityRef::rodConf(RodList::const_iterator &rod_iter) const {
    return m_configDb.rodControllerConfigHandle( rodLocation(rod_iter) );
  }
  
  ConfigHandle ConnectivityRef::rodConfFromRodLocation(const std::string &rod_location) const {
    return m_configDb.rodControllerConfigHandle( rod_location );
  }

  // Pix Module Group config
  ConfigHandle ConnectivityRef::modGroupConf(RodList::const_iterator &rod_iter) const {
    return m_configDb.moduleGroupConfigHandle( rodLocation(rod_iter) );
  }
  
  ConfigHandle ConnectivityRef::modGroupConf(const std::string &rod_location) const {
    return m_configDb.moduleGroupConfigHandle( rod_location );
  }

#ifdef _PIXLIB_TIMCONNOBJ

  // Tim config
  ConfigHandle ConnectivityRef::timConf(CrateList::const_iterator &crate_iter) const {
    const PixLib::RodCrateConnectivity *crate_conn = *crate_iter;
    if (!crate_conn) {
      exceptionIllegalIterator("crate");
    }
    const PixLib::TimConnectivity *tim_conn = const_cast<PixLib::RodCrateConnectivity *>(crate_conn)->tim();
    if (!tim_conn) {
      exceptionTimDoesNotExist(const_cast<PixLib::RodCrateConnectivity *>(crate_conn)->name());
    }
    return m_configDb.timConfigHandle( const_cast<PixLib::TimConnectivity *>(tim_conn)->name() );
  }
  
  ConfigHandle ConnectivityRef::timConf(const std::string &crate_location) const {
    const PixLib::RodCrateConnectivity *crate_conn = findCrate(crate_location);
    if (!crate_conn) {
      exceptionDoesNotExist(crate_location);
    }
    const PixLib::TimConnectivity *tim_conn = const_cast<PixLib::RodCrateConnectivity *>(crate_conn)->tim();
    if (!tim_conn) {
      exceptionTimDoesNotExist(crate_location);
    }
    return m_configDb.timConfigHandle( const_cast<PixLib::TimConnectivity *>(tim_conn)->name() );
  }

  ConfigHandle ConnectivityRef::timConfFromTimLocation(const std::string &tim_location) const {
    return m_configDb.timConfigHandle( tim_location );
  }
#endif
}
