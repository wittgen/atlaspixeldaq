#include <Config/Config.h>

#include "PixConfigWrapper.h"
#ifdef DEBUG
#  include "PixConfGrpWrapper.h"
#endif
//#include "ConfGroupUtil.h"
#include "ConfObjUtil.h"

#include "DbRef.h"
#include "DbManager.h"

class baseTest {
public:
  baseTest(const std::string &name) : m_config(new PixLib::Config(name)) {
    m_config->addGroup("General");
    (*m_config)["General"].addInt("Eins",m_eins,(name=="B" ? 5 : 1),"Eins",true);
    (*m_config)["General"].addFloat("Zwei",m_zwei,(name=="B" ? 6. : 2.),"Zwei",true);
    (*m_config)["General"].addString("Drei",m_drei,(name=="B" ? std::string("DREI") : std::string("drei")),"Drei",true);

    (*m_config).addGroup("Opt");
    (*m_config)["Opt"].addInt("Eins",m_einsOpt,(name=="B" ? -5 : -1),"Optional eins",true);
    (*m_config)["Opt"].addFloat("Zwei",m_zweiOpt,(name=="B" ? -6. : -2.),"Optional zwei",true);
    (*m_config)["Opt"].addString("Drei",m_dreiOpt,(name=="B" ? std::string("-DREI") : std::string("-drei")),"Optional drei",true);

    m_config->reset();
  }

  PixLib::Config &config() {return *m_config;}

private:
  int         m_eins;
  float       m_zwei;
  std::string m_drei;

  int         m_einsOpt;
  float       m_zweiOpt;
  std::string m_dreiOpt;

  PixLib::Config *m_config;
};

class test {
public:
  test() : m_a("A"), m_b("B"),m_config("test") {
    m_config.addConfig(&m_a.config());
    m_config.addConfig(&m_b.config());

    m_config.addGroup("General");
    m_config["General"].addInt("Global",m_global,0,"Global",true);
    m_config.reset();
  }
  
  PixLib::Config &config() {return m_config;}
private:
  int m_global;
  baseTest m_a;
  baseTest m_b;
  PixLib::Config m_config;
};


int main(int argc, char **argv)
{
  std::string file_name = "/tmp/test.root";
  std::string record_name = "test";
  if (argc>1) {
    file_name = argv[1];
  }
  if (argc>2) {
    record_name = argv[2];
  }

  try {
  test a;
  a.config().dump(std::cout);

  PixA::ConfigHandle config_handle(PixA::PixConfigWrapper::wrap(a.config()));
  std::cout << "Start using the config." << std::endl;
  {
  PixA::ConfigRef config(config_handle.ref());
  PixA::ConfigRef sub_config_a(config.subConfig("A"));
  PixA::ConfigRef sub_config_b(config.subConfig("B"));

  std::map<std::string, PixA::ConfigRef> config_list;
  config_list.insert(std::make_pair(std::string("A"),sub_config_a));
  config_list.insert(std::make_pair(std::string("B"),sub_config_b));

  for (std::map<std::string, PixA::ConfigRef>::const_iterator iter=config_list.begin();
       iter!=config_list.end();
       iter++) {
    PixA::ConfigRef sub_config=iter->second;

    std::cout << iter->first << " general eins =" << confVal<int>(sub_config["General"]["Eins"]) << std::endl;
    std::cout << iter->first << " general zwei =" << confVal<float>(sub_config["General"]["Zwei"]) << std::endl;
    std::cout << iter->first << " general drei =" << confVal<std::string>(sub_config["General"]["Drei"]) << std::endl;

    assert( confVal<int>(sub_config["General"]["Eins"]) == (iter->first=="B" ? 5: 1));
    assert( confVal<float>(sub_config["General"]["Zwei"]) == (iter->first=="B" ? 6. : 2.));
    assert( confVal<std::string>(sub_config["General"]["Drei"]) == (iter->first=="B" ? std::string("DREI") : std::string("drei")));
    
    std::cout << iter->first << " opt eins =" << confVal<int>(sub_config["Opt"]["Eins"]) << std::endl;
    std::cout << iter->first << " opt zwei =" << confVal<float>(sub_config["Opt"]["Zwei"]) << std::endl;
    std::cout << iter->first << " opt drei =" << confVal<std::string>(sub_config["Opt"]["Drei"]) << std::endl;

    assert( confVal<int>(sub_config["Opt"]["Eins"]) == (iter->first=="B" ? -5: -1));
    assert( confVal<float>(sub_config["Opt"]["Zwei"]) == (iter->first=="B" ? -6. : -2.));
    assert( confVal<std::string>(sub_config["Opt"]["Drei"]) == (iter->first=="B" ? std::string("-DREI") : std::string("-drei")));
  }
  std::cout << "Done with config." << std::endl;

  // write config;
  std::cout << "Now write config." << std::endl;
  {
  PixA::DbRef db( PixA::DbManager::dbHandle(file_name).writeHandle().ref() );

  PixLib::dbRecordIterator serial_number_record = db.findRecord(record_name);
  
  if (serial_number_record != db.recordEnd()) {
    std::string message("Cannot add info record. Record ");
    message += record_name;
    message += " exists already.";
    throw PixA::ConfigException(message);
  }
  else {
    
    a.config().write( db.addRecord( record_name, record_name).get() );
  }
  }
  std::cout << "Wrote config." << std::endl;

  }
  }
  catch (PixA::ConfigException &err) {
    std::cout << "INFO [test_PixConfig] caught exception : " << err << std::endl;
  }

  // now read back the config
  
  try 
{
    PixA::ConfigHandle a_handle;
    {
      std::cout << "Read back config." << std::endl;
      PixA::DbRef top_db( PixA::DbManager::dbHandle(file_name).ref() );

      PixLib::dbRecordIterator record = top_db.findRecord(record_name);

      assert (record != top_db.recordEnd());

      PixA::ConfigHandle test_handle(PixA::DbManager::configHandle(top_db.access(record)));
      PixA::ConfigRef test_ref(test_handle.ref());

      PixLib::Config &config = const_cast<PixLib::Config &>(test_ref.config());
      config.dump(std::cout);

      PixA::ConfigRef full_ref(test_ref.createFullCopy());
      a_handle = PixA::ConfigHandle(full_ref);
      std::cout << "done reading back. Now closing everything." << std::endl;
    }

# ifdef DEBUG
  PixA::PixConfigWrapper::stat();
  PixA::PixConfGrpWrapper::stat();
# endif

    {
      std::cout << "Check full copy." << std::endl;
      PixA::ConfigRef test_ref(a_handle.ref());
      PixLib::Config &config = const_cast<PixLib::Config &>(test_ref.config());
      config.dump(std::cout);
      std::cout << "full copy check done." << std::endl;
    }

  }
  catch (PixA::ConfigException &err) {
    std::cout << "INFO [test_PixConfig] caught exception : " << err << std::endl;
  }

# ifdef DEBUG
  PixA::PixConfigWrapper::stat();
  PixA::PixConfGrpWrapper::stat();
# endif
  //  PixA::PixWrapper
}
