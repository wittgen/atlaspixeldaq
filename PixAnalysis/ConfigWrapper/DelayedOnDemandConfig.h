#ifndef _DelayedOnDemandConfig_h_
#define _DelayedOnDemandConfig_h_

#include "IConfig.h"
#include <Connectivity.h>
#include "PixConnectivityWrapper.h"

#include <memory>

namespace PixA {

  class PixConnectivityWrapper;

  class DelayedOnDemandConfig : public IConfig
  {
  public:
    DelayedOnDemandConfig(PixConnectivityWrapper &connectivity,
			  Revision_t revision,
			  PixConnectivityWrapper::EConfigType type,
			  const std::string &connectivity_name,
			  const std::string &production_name)
      : m_pixConnectivity(&connectivity),
	m_revision(revision),
	m_type(type),
	m_name(connectivity_name),
	m_prodName(production_name),
	m_config(NULL)
    {
      std::cout << "INFO [DelayedOnDemandConfig::ctor] delay config for " << m_name << "(" << m_prodName << ")" ;
      switch (m_type) {
      case PixConnectivityWrapper::kRodConf:
	std::cout << " ROD ";
	break;
      case PixConnectivityWrapper::kBocConf:
	std::cout << " BOC ";
	break;
      case PixConnectivityWrapper::kModuleConf:
	std::cout << " Module ";
	break;
      default:
	std::cout << " unknown type ";
	break;
      }
      std::cout << " revision = " << m_revision << "." << std::endl;
    }

    ~DelayedOnDemandConfig() {
      std::cout << "INFO [DelayedOnDemandConfig::dtor] ";
      if (!m_config) {
	std::cout << " on demand config for " << m_name << "(" << m_prodName << ")" 
		  << " revision = " << m_revision << "was not created." << std::endl;
      }
      else {
	std::cout << " delete on demand config for " << m_name << "(" << m_prodName << ")" 
		  << " revision = " << m_revision << "." << std::endl;
	m_config->IConfigBase::doneWithNode();
	m_config = NULL;
      }
    }

    void use() {
      std::cout << "INFO [DelayedOnDemandConfig::createConfigIfNeeded] want to use  config for " << m_name << "(" << m_prodName << ")" 
		<< " revision = " << m_revision << "." << std::endl;
      createConfigIfNeeded();
      m_config->use();
    }

    void done() {
      assert( m_config );
      std::cout << "INFO [DelayedOnDemandConfig::createConfigIfNeeded] finished usage of config " << m_name << "(" << m_prodName << ")" 
		<< " revision = " << m_revision << "." << std::endl;
      createConfigIfNeeded();
      m_config->done();
    }

    IConfGrp &operator[](const std::string &name) {
      createConfigIfNeeded();
      return m_config->operator[](name);
    }

    //  virtual const IConfig &subConfig(const std::string &name) const throw (ConfigException) = 0;
    IConfig &subConfig(const std::string &name)  {
      createConfigIfNeeded();
      return m_config->subConfig(name);
    }

    IConfig &createAll() {
      createConfigIfNeeded();
      return m_config->createAll();
    }

    /** Create the whole config and return a reference to it.
     */
    PixLib::Config &config() {
      createConfigIfNeeded();
      return m_config->config();
    }


  protected:

    void createConfigIfNeeded() {
      if (!m_config) {
	std::cout << "INFO [DelayedOnDemandConfig::createConfigIfNeeded] create on demand config for " << m_name << "(" << m_prodName << ")" ;
	switch (m_type) {
	case PixConnectivityWrapper::kRodConf:
	  std::cout << " ROD ";
	  break;
	case PixConnectivityWrapper::kBocConf:
	  std::cout << " BOC ";
	  break;
	case PixConnectivityWrapper::kModuleConf:
	  std::cout << " Module ";
	  break;
	default:
	  std::cout << " unknown type ";
	  break;
	}
	
	std::cout << " revision = " << m_revision << "." << std::endl;
	m_config = m_pixConnectivity->configFromConnName(m_type,m_name, m_prodName, m_revision);
	m_config->IConfigBase::useNode();
      }
      assert( m_config );
    }

  private:
    PixConnectivityWrapper *m_pixConnectivity;
    Revision_t              m_revision;
    PixConnectivityWrapper::EConfigType m_type;
    std::string             m_name;
    std::string             m_prodName;

    IConfig  *m_config;
  };

}

#endif
