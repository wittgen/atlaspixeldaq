/* Dear emacs, this is -*-c++-*- */
#ifndef _PixA_ConnObjConfigDb_h_
#define _PixA_ConnObjConfigDb_h_

#include "SharedConnObjConfigDbManager.h"
#include <cassert>
#include <exception>

namespace PixA {

  /** Interface to the connectivity object config DB.
   */
  class ConnObjConfigDb {
  public:
    ConnObjConfigDb(const ConnObjConfigDb &a) : m_manager(a.m_manager) {
      m_manager->incrementUsed();
    }

    ConnObjConfigDb(const std::shared_ptr<PixLib::PixDbServerInterface> &db_server = std::shared_ptr<PixLib::PixDbServerInterface>(),
		    bool persistent_storage = false)
      : m_manager(NULL)
    {
      m_manager = create(db_server, persistent_storage);
      assert(m_manager);
      m_manager->incrementUsed();
    }

    ConnObjConfigDb(SharedConnObjConfigDbManager *manager)
      : m_manager(manager)
    {
      if (!m_manager) {
	throw std::runtime_error("FATAL [ConnObjConfigDb::ctor] Invalid shared manager instance given.");
      }
      m_manager->incrementUsed();
    }

    ~ConnObjConfigDb() {
      m_manager->decrementUsed();
      m_manager->tryDestruction();
    }

    /** Get all ID tags.
     */
    void getIdTags(std::vector<std::string> &id_tag_out) {
      m_manager->getIdTags(id_tag_out);
    }

    /** Get all configurations tags for the given ID tag.
     */
    void getTags(const std::string &id_tag, std::vector<std::string> &tag_out) {
      m_manager->getTags(id_tag,tag_out);
    }

    /** Get all revisions for the given object.
     */
    void getRevisions(const std::string &id_tag, const std::string &cfg_tag, const std::string &conn_name, std::set<Revision_t> &revisions_out) {
      m_manager->getRevisions(id_tag, cfg_tag, conn_name, revisions_out);
    }

    /** Get all revisions for the given object.
     */
    void getNames(const std::string &id_tag, const std::string &cfg_tag, IConnObjConfigDbManager::EObjectType type, std::set<std::string> &names) {
      m_manager->getNames(id_tag, cfg_tag, type, names);
    }


    /** Return a helper object to configure arbitrary object are retrieve arbitrary configurations from a particular tag.
     */
    ConnObjConfigDbTagRef getTag(const std::string &id_tag, const std::string &cfg_tag, Revision_t revision) {
      return m_manager->getTag(id_tag, cfg_tag, revision);
    }

    /** Clone the given tag.
     */
    bool cloneTag(const std::string &id_tag,
		  const std::string &src_tag,
		  const std::string &dest_tag,
		  const std::string &pending_tag="_Tmp") {
      return m_manager->cloneTag(id_tag, src_tag, dest_tag, pending_tag);
    }

    static ConnObjConfigDb sharedInstance();

  private:

    static SharedConnObjConfigDbManager *create(const std::shared_ptr<PixLib::PixDbServerInterface> &db_server,
						bool persistent_storage);
    SharedConnObjConfigDbManager *m_manager;
  };

}
#endif
