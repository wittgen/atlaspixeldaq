#include <Config/Config.h>
#include <Config/ConfGroup.h>
#include <Config/ConfObj.h>
#include <Config/ConfMaskI.h>
#include <map>
#include <vector>

#include "test_configSize.h"

class base {
public:
  virtual ~base() {}
  virtual PixLib::Config &config()  = 0;
};

class testInt : public base {
public:
  testInt() : m_config(new PixLib::Config("testInt")) {
    m_config->addGroup("INT1");
    (*m_config)["INT1"].addInt("val1",m_eins,1,"com1",true);
    m_config->reset();
  }

  PixLib::Config &config() {return *m_config;}

private:
  int         m_eins;
  PixLib::Config *m_config;
};

class testChar : public base {
public:
  testChar() : m_config(new PixLib::Config("testChar")) {
    m_config->addGroup("CHR1");
    (*m_config)["CHR1"].addInt("val1",m_eins,1,"com1",true);
    m_config->reset();
  }

  PixLib::Config &config() {return *m_config;}

private:
  char        m_eins;
  PixLib::Config *m_config;
};

class testFloat : public base {
public:
  testFloat() : m_config(new PixLib::Config("testFloat")) {
    m_config->addGroup("FLT1");
    (*m_config)["FLT1"].addFloat("val1",m_eins,1,"com1",true);
    m_config->reset();
  }

  PixLib::Config &config() {return *m_config;}

private:
  float        m_eins;
  PixLib::Config *m_config;
};

class testList : public base {
public:
  testList() : m_config(new PixLib::Config("testList")) {
    m_config->addGroup("LIST");
    std::map<std::string, int> symb;
    symb["sel1"]=1;
    symb["sel2"]=2;
    symb["sel3"]=3;
    symb["sel4"]=4;
    (*m_config)["LIST"].addList("list",m_eins,1,symb,"com1",true);
    m_config->reset();
  }

  PixLib::Config &config() {return *m_config;}

private:
  unsigned short m_eins;
  PixLib::Config *m_config;
};

class testString : public base {
public:
  testString() : m_config(new PixLib::Config("testString")) {
    m_config->addGroup("STRI");
    (*m_config)["STRI"].addString("stri",m_eins,"stri","com1",true);
    m_config->reset();
  }

  PixLib::Config &config() {return *m_config;}

private:
  std::string m_eins;
  PixLib::Config *m_config;
};

class testVector : public base {
public:
  testVector() : m_config(new PixLib::Config("testVector")) {
    m_config->addGroup("VECT");
    std::vector<float> def;
    def.push_back(1.);
    def.push_back(2.);
    def.push_back(3.);
    def.push_back(4.);
    (*m_config)["VECT"].addVector("vect",m_eins,def,"com1",true);
    m_config->reset();
  }

  PixLib::Config &config() {return *m_config;}

private:
  std::vector<float> m_eins;
  PixLib::Config *m_config;
};

class testMatrix1 : public base {
public:
  testMatrix1() : m_config(new PixLib::Config("testIntMatrix")) {
    m_config->addGroup("MATR");
    PixLib::ConfMask<PixLib::TrimVal_t> def(2,8,32);
    for (unsigned int i=0; i<2; i++) {
      for (unsigned int j=0; j<8; j++) {
	def.set(i,j,( (i+j)%2 ? 32 : 0));
      }
    }
    (*m_config)["MATR"].addMatrix("matr",m_eins,def,"com1",true);
    m_config->reset();
  }

  PixLib::Config &config() {return *m_config;}

private:
  PixLib::ConfMask<PixLib::TrimVal_t> m_eins;
  PixLib::Config *m_config;
};

class testMatrix2 : public base {
public:
  testMatrix2() : m_config(new PixLib::Config("testBoolMatrix")) {
    m_config->addGroup("MATR");
    PixLib::ConfMask<bool> def(2,64,false);
    for (unsigned int i=0; i<2; i++) {
      for (unsigned int j=0; j<64; j++) {
	def.set(i,j,( (i+j)%2 ? true : false));
      }
    }
    (*m_config)["MATR"].addMatrix("matr",m_eins,def,"com1",true);
    m_config->reset();
  }

  PixLib::Config &config() {return *m_config;}

private:
  PixLib::ConfMask<bool> m_eins;
  PixLib::Config *m_config;
};


int main()
{
  std::vector<base *> list;
  list.push_back(new testInt);
  list.push_back(new testChar);
  list.push_back(new testFloat);
  list.push_back(new testList);
  list.push_back(new testString);
  list.push_back(new testVector);
  list.push_back(new testMatrix1);
  list.push_back(new testMatrix2);
  for (std::vector<base *>::iterator iter=list.begin(); 
       iter != list.end();
       iter++ ) {
    (*iter)->config().dump(std::cout);
    get_size((*iter)->config(),true);
    delete *iter;
    *iter=NULL;
  }
  return 0;
}
