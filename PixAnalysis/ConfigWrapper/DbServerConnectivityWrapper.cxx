#include "DbServerConnectivityWrapper.h"
#include <PixModule/PixModule.h>
#include <PixBoc/PixBoc.h>
#include "PixConfigWrapperTopLevel.h"
#include "ConnectivityUtil.h"

//#include <RelationalAccess/AuthenticationServiceException.h>


namespace PixA {

  std::string DbServerConnectivityWrapper::s_connectivityDomainName("Connectivity-");
  std::string DbServerConnectivityWrapper::s_configurationDomainName("Configuration-");


  DbServerConnectivityWrapper::DbServerConnectivityWrapper(const std::shared_ptr<PixLib::PixDbServerInterface> &db_server,
							   const std::string &object_tag,
							   const std::string &tagC,
							   const std::string &tagP,
							   const std::string &tagA,
							   const std::string &tagCF,
							   const std::string &tagModCF)
    : m_dbServer(db_server)
  {
    std::unique_ptr<PixLib::PixConnectivity> a_conn;
    for (unsigned int retry=0; ; retry++) {
      try {
    a_conn = std::make_unique<PixLib::PixConnectivity>(db_server.get(), tagC,tagP,tagA, tagCF, object_tag, tagModCF);
      }
      //	catch (coral::AuthenticationServiceException &err) {
      catch (std::exception &err) {
	if (retry>=5) {
	  std::stringstream message;
	  message << "FATAL [DbServerConnectivityWrapper::ctor] Failed to create connectivty data base for tags "
		  << object_tag << ":"
		  << tagC <<", "
		  << tagP <<", "
		  << tagA <<"; "
		  << tagCF;
	  if (!tagModCF.empty() && tagModCF != tagCF ) {
	    message << ", " << tagModCF;
	  }
	  message << "       with error : " << err.what();

	  throw PixA::ConfigException(message.str());
	}
	// sleep 300ms
	usleep(300000);
	continue;
      }
      m_connectivity = a_conn.release();
      break;
    }
    //      std::cout << "INFO [DbServerConnectivityWrapper::ctor] got connectivity " << static_cast<void *>(m_connectivity) << std::endl;
    if (m_connectivity) {
      m_connectivity->loadConn();
    }
  }


  DbServerConnectivityWrapper::~DbServerConnectivityWrapper() {
  }


  std::shared_ptr<PixLib::PixDisable> DbServerConnectivityWrapper::getDisable(std::string disableName, Revision_t revision) {
    return std::shared_ptr<PixLib::PixDisable>();
  }

  void DbServerConnectivityWrapper::writeDisable(const std::shared_ptr<PixLib::PixDisable> &disable, bool cache) {

    if ( m_dbServer.get() ) {
      std::string ptag("S");
      std::string::size_type pos = disable->name().rfind("_");
      if (pos != std::string::npos) {
	ptag += disable->name().substr(pos+1,disable->name().size()-pos-1);
      }

      disable->writeConfig(m_dbServer.get(), "Configuration-"+tag(kId), tag(kConfig), ptag);
    }
    else {
      disable->writeConfig(m_connectivity);
    }

  }


//   Revision_t DbServerConnectivityWrapper::writeModConf(const PixLib::ModuleConnectivity *mod_conn, Revision_t revision) const {
    
//     if (revision != 0) {
//       // cannot change old configurations
//       // First, a new tag has to be created from the old revision 
//       // Then the revision can be set to the head i.e. it can be set to zero.
//       // Finally, the configuration can be saved.
//       //      return revision;
//     }



  void DbServerConnectivityWrapper::getObjectTagsFromDbServer(PixLib::PixDbServerInterface &db_server,
							 std::vector<std::string> *object_tags)
  {
    if (!object_tags) return;

    std::vector<std::string> domlist;
    db_server.listDomainRem(domlist);
    //std::vector<std::string>::iterator domain_iter;

    for(std::vector<std::string>::iterator domain_iter=domlist.begin();
	domain_iter!=domlist.end();
	domain_iter++) {

      if ((*domain_iter).compare(0, s_connectivityDomainName.size(), s_connectivityDomainName) == 0) {
	object_tags->push_back((*domain_iter).substr(s_connectivityDomainName.size(),
						     (*domain_iter).size() - s_connectivityDomainName.size()));
      }

    }

  }

  void DbServerConnectivityWrapper::getConfigurationObjectTagsFromDbServer(PixLib::PixDbServerInterface &db_server,
								      std::vector<std::string> *object_tag_out)
  {
    if (!object_tag_out) return;

    std::vector<std::string> domlist;
    db_server.listDomainRem(domlist);

    for(std::vector<std::string>::iterator domain_iter=domlist.begin();
	domain_iter!=domlist.end();
	domain_iter++) {

      if ((*domain_iter).compare(0, s_configurationDomainName.size(), s_configurationDomainName) == 0) {
	object_tag_out->push_back((*domain_iter).substr(s_configurationDomainName.size(),
						     (*domain_iter).size() - s_configurationDomainName.size()));
      }

    }

  }

  void DbServerConnectivityWrapper::getConnectivityTagsFromDbServer(PixLib::PixDbServerInterface &db_server,
							       const std::string &id_name,
							       std::vector<std::string> *conn_tag_out,
							       std::vector<std::string> *alias_tag_out,
							       std::vector<std::string> *payload_tag_out,
							       std::vector<std::string> *configuration_tag_out)
  {
    std::vector<std::string> *out_ptr[3]={conn_tag_out, alias_tag_out, payload_tag_out};
    std::vector<std::string> *ref=NULL;
    for (unsigned int tag_i=0; tag_i<4; tag_i++) {
      if (out_ptr[tag_i]) {
	if (ref==0) {
	  db_server.listTagsRem(s_connectivityDomainName+id_name, *out_ptr[tag_i]);
	  ref = out_ptr[tag_i];
	}
	else {
	  *out_ptr[tag_i] = *ref;
	}
      }
    }
    if (configuration_tag_out) {
      db_server.listTagsRem(s_configurationDomainName+id_name, *configuration_tag_out);
    }

  }


  template <> inline std::shared_ptr<PixLib::PixModule> DbServerConnectivityWrapper::createObject<PixLib::PixModule>(const std::string &name) {
    // To load an arbitrary revision, first the object is constructed with a default configuration.
    // then the correct revision is loaded manually

    // must first load chip flavours from connectivity to create correct module structure
    PixLib::EnumMCCflavour::MCCflavour mccFlv = PixLib::EnumMCCflavour::PM_MCC_I2;
    PixLib::EnumFEflavour::FEflavour feFlv = PixLib::EnumFEflavour::PM_FE_I2;
    int nFe=16;
    if(m_connectivity->mods.find(name)!=m_connectivity->mods.end())
      m_connectivity->mods[name]->getModPars(mccFlv, feFlv, nFe);
    // Trick, to create the object with the default configuration, the module is constructed with an invalid domain name
    std::shared_ptr<PixLib::PixModule> the_module(new PixLib::PixModule(m_dbServer.get(),NULL, "trick", m_connectivity->getModCfgTag(), name, mccFlv, feFlv, nFe));
    return the_module;
  }

  template <> inline std::shared_ptr<PixLib::PixModuleGroup> DbServerConnectivityWrapper::createObject<PixLib::PixModuleGroup>(const std::string &name) {
    // now we can create the boc
    std::shared_ptr<PixLib::PixModuleGroup> the_pix_module_group(new PixLib::PixModuleGroup(name));
    return the_pix_module_group;
  }

  template <class T_CompoundObj_t, class T_Obj_t> PixLib::Config &config(const std::shared_ptr<T_CompoundObj_t> &obj);

  template <> inline PixLib::Config &config<PixLib::PixModuleGroup, PixLib::PixBoc>(const std::shared_ptr<PixLib::PixModuleGroup> &obj) {
    assert(obj->getPixBoc() && obj->getPixBoc()->getConfig());
    return *(obj->getPixBoc()->getConfig());
  }

  template <> inline PixLib::Config &config<PixLib::PixModuleGroup, PixLib::PixModuleGroup>(const std::shared_ptr<PixLib::PixModuleGroup> &obj) {
    return obj->config();
  }

  template <> inline PixLib::Config &config<PixLib::PixModule, PixLib::PixModule>(const std::shared_ptr<PixLib::PixModule> &obj) {
    return obj->config();
  }


  template <class T_CompoundObj_t, class T_Obj_t>
  void readConfig(const std::shared_ptr<T_CompoundObj_t> &obj,
		  PixLib::PixDbServerInterface *db_server,
		  const std::string &domain_name,
		  const std::string &tag,
		  const std::string &name,
		  Revision_t revision)
  {
    config<T_CompoundObj_t,T_Obj_t>(obj).read(db_server,domain_name, tag, name, revision);
  }

  template <>
  void  readConfig<PixLib::PixModuleGroup, PixLib::PixBoc> (const std::shared_ptr<PixLib::PixModuleGroup> &obj,
							    PixLib::PixDbServerInterface *db_server,
							    const std::string &domain_name,
							    const std::string &tag,
							    const std::string &name,
							    Revision_t revision)
  {
    config<PixLib::PixModuleGroup,PixLib::PixModuleGroup>(obj).read(db_server,domain_name, tag, name, revision);
    config<PixLib::PixModuleGroup,PixLib::PixBoc>(obj).read(db_server,domain_name, tag, name, revision);
  }

  template <>
  void readConfig<PixLib::PixModuleGroup, PixLib::PixModuleGroup>(const std::shared_ptr<PixLib::PixModuleGroup> &obj,
								  PixLib::PixDbServerInterface *db_server,
								  const std::string &domain_name,
								  const std::string &tag,
								  const std::string &name,
								  Revision_t revision)
  {
    readConfig<PixLib::PixModuleGroup, PixLib::PixBoc>(obj,db_server, domain_name, tag, name, revision);
  }



  template <class T_CompoundObj_t, class T_Obj_t> IConfig *DbServerConnectivityWrapper::objectConfig(const std::string &name, Revision_t revision) {

    // If the revision is zero the most recent revision is requested, so the revision is set to the maximum
    if (revision==0) revision--;

    std::shared_ptr<T_CompoundObj_t> obj;
    std::shared_ptr<T_CompoundObj_t> *obj_ptr( cache<std::shared_ptr<T_CompoundObj_t> >().object(name,revision) );

    if (!obj_ptr ) {
      // first create an object with a default configuraitno, since the object cannot be constructed with a certain revision
      obj = createObject<T_CompoundObj_t>(name);
      // then the configuration is loaded manually
      readConfig<T_CompoundObj_t,T_Obj_t>(obj, m_dbServer.get(),configurationDomainName(), m_connectivity->getModCfgTag(), name, revision);

      // cache the object
      cache<std::shared_ptr<T_CompoundObj_t> >().add(name,(const Revision_t)revision,obj);
    }
    else {
      obj = *obj_ptr;
    }

    return new PixConfigWrapperTopLevel<T_CompoundObj_t>( obj, config<T_CompoundObj_t,T_Obj_t>(obj) );
  }


  ConfigHandle DbServerConnectivityWrapper::modConfHandle(const PixLib::ModuleConnectivity *mod, Revision_t revision) {
    return *objectConfig<PixLib::PixModule,PixLib::PixModule>(productionName(mod),revision );
  }

  ConfigHandle DbServerConnectivityWrapper::bocConfHandle(const PixLib::RodBocConnectivity *rod_boc, Revision_t revision){
    return *objectConfig<PixLib::PixModuleGroup,PixLib::PixBoc>(connectivityName(rod_boc),revision );
  }

  ConfigHandle DbServerConnectivityWrapper::rodConfHandle(const PixLib::RodBocConnectivity *rod_boc, Revision_t revision) {
    return *objectConfig<PixLib::PixModuleGroup,PixLib::PixModuleGroup>(connectivityName(rod_boc),revision );
  }

  // instantiate some member functions
  template IConfig *DbServerConnectivityWrapper::objectConfig<PixLib::PixModule,PixLib::PixModule>(const std::string &name, Revision_t revision);
  template IConfig *DbServerConnectivityWrapper::objectConfig<PixLib::PixModuleGroup,PixLib::PixModuleGroup>(const std::string &name, Revision_t revision);
  template IConfig *DbServerConnectivityWrapper::objectConfig<PixLib::PixModuleGroup,PixLib::PixBoc>(const std::string &name, Revision_t revision);

}
