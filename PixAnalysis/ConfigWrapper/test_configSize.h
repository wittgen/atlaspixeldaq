#ifndef _test_configSize_h_
#define _test_configSize_h_

#ifndef TrimVal_t_h
#define TrimVal_t_h

namespace PixLib {
  typedef unsigned short TrimVal_t;
}

#endif

class ConfSize_t
{
public:
  ConfSize_t() : m_dataSize(0), m_textSize(0) {};
  ConfSize_t(unsigned long data, unsigned long text) : m_dataSize(data), m_textSize(text) { };

  ConfSize_t &operator+=(const ConfSize_t &a) {m_dataSize+=a.m_dataSize; m_textSize += a.m_textSize; return *this;}
  ConfSize_t &operator-=(const ConfSize_t &a) {m_dataSize-=a.m_dataSize; m_textSize -= a.m_textSize; return *this;}

  unsigned long dataSize() const {return m_dataSize;}

  unsigned long textSize() const {return m_textSize;}

  void dump(const std::string &a="") const {
    std::cout << a << " : data size = " << m_dataSize << " text size = " << m_textSize << std::endl;
  }

private:
  unsigned long m_dataSize;
  unsigned long m_textSize;

};

inline ConfSize_t operator+(const ConfSize_t &a, const ConfSize_t &b) {
  ConfSize_t temp(a);
  temp+=b;
  return temp;
}

inline ConfSize_t operator-(const ConfSize_t &a, const ConfSize_t &b) {
  ConfSize_t temp(a);
  temp-=b;
  return temp;
}

namespace PixLib {
  class ConfObj;
  class ConfGroup;
  class Config;
}

ConfSize_t get_size(PixLib::ConfObj &a, bool show);
ConfSize_t get_size(PixLib::ConfGroup &a, bool show);
ConfSize_t get_size(PixLib::Config &a, bool show=true);

#endif
