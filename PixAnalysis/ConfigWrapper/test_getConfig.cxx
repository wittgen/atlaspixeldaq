#include "ConnectivityManager.h"
#include "ConfObjUtil.h"
#include "ConfGrpRefUtil.h"
#include "ConnectivityUtil.h"

#include <TFile.h>
#include <TTree.h>

struct ModStats
{
  float caplo[16];
  float caphi[16];
  float v0[16];
  float v1[16];
  float v2[16];
  float v3[16];
};

int main()
{
  using std::cout;
  using std::endl;
  using std::string;
  TFile * f = new TFile("SomeCalibConts.root", "recreate");
  TTree * t = new TTree("t", "Many Calib Numbers");
  ModStats * stats = new ModStats;

  unsigned int n_passes = 1;      
  for (unsigned int pass=0; pass<n_passes; pass++) {
    try {
      
      std::string conntag("TOOTHPIX-2008-V7");
      std::string configtag("TOOTHPIX-BOC");
      
      PixA::ConnectivityRef conn(PixA::ConnectivityManager::getConnectivity(conntag,conntag,conntag,configtag,0));
      
      //conn will wrap a pointer to IConnectivity
      // IConnectivity will contain a PixConnectivity and will cache config objects.
      // The connectivity will be a shared property. If there are no more users, the 
      // connectivity will be deleted eventually.
      string listOfRODs[7] = {"ROD_C0_S11", "ROD_C0_S15", "ROD_C0_S20", "ROD_C1_S12",
			      "ROD_C1_S17", "ROD_C1_S21", "ROD_C1_S5"};
      for (int rodNum=0; rodNum<7; rodNum++)
	{
	  cout<<"     AMB doing ROD: "<<listOfRODs[rodNum]<<endl;
	  const PixA::Pp0List &pp0s=conn.pp0s(listOfRODs[rodNum]);
	  // read only
	  for (PixA::Pp0List::const_iterator pp0_iter=pp0s.begin();
	       pp0_iter != pp0s.end(); // may be slightly inefficient unless the compiler is smart enough.
	       ++pp0_iter) {
	    

	    // it may be more efficient to call modulesBegin and modulesEnd only once:
	    PixA::ModuleList::const_iterator modules_begin=PixA::modulesBegin(pp0_iter);
	    PixA::ModuleList::const_iterator modules_end=PixA::modulesEnd(pp0_iter);

	    for (PixA::ModuleList::const_iterator module_iter=modules_begin;
		 module_iter != modules_end;
		 ++module_iter) {
	      //const PixLib::Config &config = conn.fullModuleConfig(module_iter);
	      //const_cast<PixLib::Config &>(config).dump(std::cout);
	      
	      std::cout << "     AMB Module: " << PixA::connectivityName(module_iter) << std::endl;
	      t->Branch((PixA::connectivityName(module_iter)).c_str(), &stats->caplo, "caplo[16]/F:caphi[16]:v0[16]:v1[16]:v2[16]:v3[16]");
	      PixA::ConfigHandle module_conf_handle( conn.modConf(module_iter) );
	      PixA::ConfigRef module_config_ref( module_conf_handle.ref() );
	      
	      std::vector<std::vector<float> > chargeArray;
	      
	      for (int fe=0; fe!=16; ++fe)
		{
		  std::vector<float> dummy(0);
		  chargeArray.push_back(dummy);
		  
		  char fe_name[50];
		  sprintf(fe_name,"PixFe_%i",fe);
		  //std::cout << "Getting charge for fe " << std::string(fe_name) << std::endl;
		  PixA::ConfigRef fe_config_ref = module_config_ref.subConfig(std::string(fe_name));
		  PixLib::Config& fe_config = const_cast<PixLib::Config &>(fe_config_ref.config());
		  std::string cnamelo = "CInjLo";
		  std::string cnamehi = "CInjHi";
		  // should not used the casts below, but rather the aux functions from ConfObjUtil.h
		  float cInjlo = ((PixLib::ConfFloat&)fe_config["Misc"][cnamelo]).value();
		  float cInjhi = ((PixLib::ConfFloat&)fe_config["Misc"][cnamehi]).value();
		  float vcal_a = ((PixLib::ConfFloat&)fe_config["Misc"]["VcalGradient3"]).value();
		  float vcal_b = ((PixLib::ConfFloat&)fe_config["Misc"]["VcalGradient2"]).value();
		  float vcal_c = ((PixLib::ConfFloat&)fe_config["Misc"]["VcalGradient1"]).value();
		  float vcal_d = ((PixLib::ConfFloat&)fe_config["Misc"]["VcalGradient0"]).value();
		  
		  stats->caplo[fe] = cInjlo;
		  stats->caphi[fe] = cInjhi;
		  stats->v0[fe] = vcal_d;
		  stats->v1[fe] = vcal_c;
		  stats->v2[fe] = vcal_b;
		  stats->v3[fe] = vcal_a;
		  
		  /*
		    std::cout << "cInjlo " << cInjlo << std::endl;
		    std::cout << "cInjhi " << cInjhi << std::endl;	      
		    std::cout << "vcal_a " << vcal_a << std::endl;
		    std::cout << "vcal_b " << vcal_b << std::endl;
		    std::cout << "vcal_c " << vcal_c << std::endl;
		    std::cout << "vcal_d " << vcal_d << std::endl;
		  */      
		  

		    int NvcalSteps = 201;
		    for (int step=0; step!=NvcalSteps; ++step)
		      {
			float v = (float)step; 
			// convert vcal to charge
			float q = (((vcal_a*v + vcal_b)*v + vcal_c)*v + vcal_d)*cInjlo/0.160218;
			chargeArray[fe].push_back(q);
		      }
		    
		}
	      t->Fill();
	    }
	  }
	}	
    }
    catch (PixA::ConfigException &err) {
      std::cout << "FATAL [main] caught exception " << err << std::endl;
    }
    catch (std::exception &err) {
      std::cout << "FATAL [main] caught exception " << err.what() << std::endl;
    }
    PixA::ConnectivityManager::instance()->cleanup();
#ifdef DEBUG
    PixA::DbWrapper::stat();
#endif
  }
  t->Print();
  f->Write();
}
