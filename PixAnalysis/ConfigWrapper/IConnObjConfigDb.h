/* Dear emacs, this is -*-c++-*- */
#ifndef _PixA_IConnObjConfigDb_h_
#define _PixA_IConnObjConfigDb_h_

#include "ConfigHandle.h"

#include "IRevisionIndex.h"
#include <memory>

namespace PixLib {
  class PixModule;
  class PixBoc;
  class PixModuleGroup;
  class PixController;
  class RodPixController;
  class PixTrigController;

  class PixDisable;
}

namespace PixA {

//   /** Interface to the module and ROD configuration data base e.g. DbServer, files.
//    */
//   class IConnObjConfigDb
//   {
//   public:
//     virtual ~IConnObjConfigDb() {}

//     virtual ConfigHandle configHandle(const std::string &type, const std::string &name, const std::string &cfg_tag, Revision_t revision) = 0;
//     virtual PixLib::Config &config(const std::string &type, const std::string &name, const std::string &cfg_tag, Revision_t revision) = 0;

//     virtual void tagList(const std::string &type, std::vector<std::string> &tag_list_out) = 0;
//     virtual void revisionList(const std::string &type, const std::string &name, const std::string &cfg_tag, std::vector<std::string> &tag_list_out) = 0;

//     virtual void configure(PixLib::Config &config, const std::string &cfg_tag, Revision_t revision) = 0;

//     virtual void storeConfig( const PixLib::Config &config, const std::string &cfg_tag, Revision_t revision, const std::string &pending_tag="Generic_Tmp") = 0;
//   };

  class IConnObjConfigDbTag
  {
  public:
    virtual ~IConnObjConfigDbTag() {}

    virtual void configure(/*const std::string &name,*/ PixLib::Config &config) = 0;

    virtual ConfigHandle  moduleConfigHandle(const std::string &name) = 0;
    virtual ConfigHandle  moduleGroupConfigHandle(const std::string &name) = 0;
    virtual ConfigHandle  bocConfigHandle(const std::string &name) = 0;
    virtual ConfigHandle  rodControllerConfigHandle(const std::string &name) = 0;
    virtual ConfigHandle  timConfigHandle(const std::string &name) = 0;
    virtual ConfigHandle  disableConfigHandle(const std::string &name) = 0;

    virtual const PixLib::Config  &moduleConfig(const std::string &name) = 0;
    virtual const PixLib::Config  &moduleGroupConfig(const std::string &name) = 0;
    virtual const PixLib::Config  &bocConfig(const std::string &name) = 0;
    virtual const PixLib::Config  &rodControllerConfig(const std::string &name) = 0;
    virtual const PixLib::Config  &timConfig(const std::string &name) = 0;

    virtual const PixLib::PixModule         &configuredModule(const std::string &name) = 0;
    virtual const PixLib::PixModuleGroup    &configuredModuleGroup(const std::string &name) = 0;
    virtual const PixLib::PixBoc            &configuredBoc(const std::string &name) = 0;
    virtual const PixLib::PixTrigController &configuredTim(const std::string &name) = 0;
    // not very usefule since the PixController is kind of an abstract class
    //    virtual const PixLib::PixController  &configuredRodController(const std::string &name, const std::string &cfg_tag, Revision_t revision) = 0;

    virtual std::shared_ptr<const PixLib::PixDisable>        disablePtr(const std::string &name) = 0;
    virtual std::shared_ptr<const PixLib::PixModule>         configuredModulePtr(const std::string &name) = 0;
    virtual std::shared_ptr<const PixLib::PixModuleGroup>    configuredModuleGroupPtr(const std::string &name) =0;
    virtual std::shared_ptr<const PixLib::PixTrigController> configuredTimPtr(const std::string &name) =0;


    virtual PixLib::PixModule         *createConfiguredModule(const std::string &name) = 0;
    virtual PixLib::PixModuleGroup    *createConfiguredModuleGroup(const std::string &name) = 0;
    virtual PixLib::PixTrigController *createConfiguredTim(const std::string &name) = 0;

    virtual Revision_t storeConfig( const PixLib::Config &config, Revision_t revision=0, const std::string &pending_tag="Generic_Tmp", bool cache=false) = 0;

    virtual Revision_t storeConfig( const PixLib::PixModule &obj, Revision_t revision=0, const std::string &pending_tag="_Tmp", bool cache=false) = 0;
    virtual Revision_t storeConfig( const PixLib::PixModuleGroup &obj, Revision_t revision=0, const std::string &pending_tag="_Tmp", bool cache=false) = 0;
    //    virtual Revision_t storeConfig( const PixLib::PixTrigController &obj, Revision_t revision=0, const std::string &pending_tag="_Tmp", bool cache=false) = 0;

    virtual Revision_t revision() const = 0;
    virtual const std::string &idTag() const  = 0;
    virtual const std::string &tag() const = 0;

    virtual bool isTemporaryCfg(const std::string &name) =0 ;
  };

  /** Helper class to get configurations or configure objects using a specific db tag.
   * The db tag is set at construction time.
   */
  class ConnObjConfigDbTagRef
  {
  public:

    /** Produces an invalid config db tag which must not be used.
     */
    ConnObjConfigDbTagRef()  {}

    ConnObjConfigDbTagRef(IConnObjConfigDbTag *ptr) : m_configDbTag(ptr) {}

    ConnObjConfigDbTagRef(const std::shared_ptr<IConnObjConfigDbTag> &shared_ptr) : m_configDbTag(shared_ptr) {}


    void configure(/*const std::string &name, */ PixLib::Config &config) {
      m_configDbTag->configure(/*name,*/config);
    }


    /** Return a module config a config handle .
     * This will not directly load the configuration but delay the downloading until the
     * handle is used e.g. PixA::ConfigRef a_ref = a_handle.ref();
     */
    ConfigHandle  moduleConfigHandle(const std::string &name) {
      return m_configDbTag->moduleConfigHandle(name);
    }
    ConfigHandle  moduleGroupConfigHandle(const std::string &name) {
      return m_configDbTag->moduleGroupConfigHandle(name);
    }

    ConfigHandle  bocConfigHandle(const std::string &name) {
      return m_configDbTag->bocConfigHandle(name);
    }

    ConfigHandle  rodControllerConfigHandle(const std::string &name) {
      return m_configDbTag->rodControllerConfigHandle(name);
    }

    ConfigHandle  disableConfigHandle(const std::string &name) {
      return m_configDbTag->disableConfigHandle(name);
    }

    ConfigHandle  timConfigHandle(const std::string &name) {
      return m_configDbTag->timConfigHandle(name);
    }


    const PixLib::Config      &moduleConfig(const std::string &name) {
      return m_configDbTag->moduleConfig(name);    
    }

    const PixLib::Config &moduleGroupConfig(const std::string &name) {
      return m_configDbTag->moduleGroupConfig(name);    
    }

    const PixLib::Config         &bocConfig(const std::string &name) {
      return m_configDbTag->bocConfig(name);    
    }

    const PixLib::Config  &rodControllerConfig(const std::string &name) {
      return m_configDbTag->rodControllerConfig(name);    
    }

    const PixLib::Config  &timConfig(const std::string &name) {
      return m_configDbTag->timConfig(name);    
    }


    /** Return a configured pix module.
     * @todo separate off the confiugration data and move it to a light PixModuleData_t class. In particular
     *    strip off all hardware controlling functions.
     */
    const PixLib::PixModule      &configuredModule(const std::string &name) {
      return m_configDbTag->configuredModule(name);
    }

    /** Return a configured pix module group.
     * @todo separate off the confiugration data and move to light PixModuleGroupData_t class. In particular
     *    strip off all hardware controlling functions.
     */
    const PixLib::PixModuleGroup &configuredModuleGroup(const std::string &name)  {
      return m_configDbTag->configuredModuleGroup(name);
    }

    /** Return a configured pix module group.
     * @todo separate off the confiugration data and move to light PixModuleGroupData_t class. In particular
     *    strip off all hardware controlling functions.
     */
    const PixLib::PixTrigController &configuredTim(const std::string &name)  {
      return m_configDbTag->configuredTim(name);    
    }


    /** Return a shared pointer to a disable object.
     * Will eventually go away at some point.
     */
    std::shared_ptr<const PixLib::PixDisable> disablePtr(const std::string &name) {
      return m_configDbTag->disablePtr(name);
    }

    /** Return a shared pointer to a configured PixModule.
     */
    std::shared_ptr<const PixLib::PixModule> configuredModulePtr(const std::string &name) {
      return m_configDbTag->configuredModulePtr(name);
    }

    /** Return a shared pointer to a configured PixModuleGroup.
     */
    std::shared_ptr<const PixLib::PixModuleGroup> configuredModuleGroupPtr(const std::string &name) {
      return m_configDbTag->configuredModuleGroupPtr(name);
    }

    /** Return a shared pointer to a configured PixModuleGroup.
     */
    std::shared_ptr<const PixLib::PixTrigController> configuredTimPtr(const std::string &name) {
      return m_configDbTag->configuredTimPtr(name);
    }

    /** Create a new module object and configure it.
     * @param name the name of the module.
     * @return a new and configured pix module which belongs to the caller.
     */
    PixLib::PixModule         *createConfiguredModule(const std::string &name) {
      return m_configDbTag->createConfiguredModule(name);
    }

    /** Create a new module group object and configure it.
     * @param name the name of the module group.
     * @return a new and configured pix module group which belongs to the caller.
     */
    PixLib::PixModuleGroup    *createConfiguredModuleGroup(const std::string &name) {
      return m_configDbTag->createConfiguredModuleGroup(name);
    }

    /** Create a new tim object and configure it.
     * @param name the name of the tim.
     * @return a new and configured tim which belongs to the caller.
     */
    PixLib::PixTrigController *createConfiguredTim(const std::string &name)  {
      return m_configDbTag->createConfiguredTim(name);
    }

    /** Store a new revision of the given object.
     * The name and type of the object is defined by the config object.
     */
    Revision_t storeConfig( const PixLib::Config &config, Revision_t revision=0, const std::string &pending_tag="_Tmp", bool cache=false) {
      return m_configDbTag->storeConfig(config, revision, pending_tag, cache);
    }

    /** Store a new revision of the given pix module.
     * @todo use PixLib::PixModuleData_t once it exists.
     */
    Revision_t storeConfig( const PixLib::PixModule &obj, Revision_t revision=0, const std::string &pending_tag="_Tmp", bool cache=false) {
      return m_configDbTag->storeConfig(obj, revision, pending_tag, cache);
    }

    /** Store a new revision of the given pix module group.
     * @todo use PixLib::PixModuleData_t once it exists.
     */
    Revision_t storeConfig( const PixLib::PixModuleGroup &obj, Revision_t revision=0, const std::string &pending_tag="_Tmp", bool cache=false) {
      return m_configDbTag->storeConfig(obj, revision, pending_tag, cache);
    }


    Revision_t revision() const      { return m_configDbTag->revision(); }
    const std::string &idTag() const { return m_configDbTag->idTag(); }
    const std::string &tag() const   { return m_configDbTag->tag(); }

    bool isTemporaryCfg(const std::string &name) { return m_configDbTag->isTemporaryCfg(name); }

    /** Check whether the config db tag is valid.
     */
    operator bool() const { return m_configDbTag.get() != NULL; }

    bool operator==(const ConnObjConfigDbTagRef &a) const { return revision()==a.revision() && tag()==a.tag() && idTag() == a.idTag(); }

  private:
    std::shared_ptr<IConnObjConfigDbTag> m_configDbTag;
  };

  /** Central interface to the connectivity object configration database.
   */
  class IConnObjConfigDbManager {
  public:
    enum EObjectType {kModuleObjects, kModuleGroupObjects, kTrigControllerObjects, kDisableObjects, kNObjectTypes};

    virtual ~IConnObjConfigDbManager() {}

    virtual void getIdTags(std::vector<std::string> &id_tag_out) = 0;

    virtual void getTags(const std::string &id_tag, std::vector<std::string> &tag_out) = 0;

    virtual void getRevisions(const std::string &id_tag, const std::string &cfg_tag, const std::string &conn_name, std::set<Revision_t> &revisions_out) = 0;

    virtual void getNames(const std::string &id_tag, const std::string &cfg_tag, EObjectType type, std::set<std::string> &names) = 0;

    /** Return a helper object to configure arbitrary object are retrieve arbitrary configurations from a particular tag.
     */
    virtual ConnObjConfigDbTagRef getTag(const std::string &id_tag, const std::string &cfg_tag, Revision_t revision) = 0;

    /** Clone the given tag.
     */
    virtual bool cloneTag(const std::string &id_tag,
			  const std::string &src_tag,
			  const std::string &dest_tag,
			  const std::string &pending_tag="_Tmp") = 0;

  };

}
#endif
