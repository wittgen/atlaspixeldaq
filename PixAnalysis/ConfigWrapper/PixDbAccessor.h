#ifndef _PixDbAccessor_h_
#define _PixDbAccessor_h_

#include <map>
#include <string>
#include <vector>
#include <PixDbInterface/PixDbInterface.h>
#include "DbWrapper.h"
#include "pixa_exception.h"
#include <memory>

namespace PixA {

  class OnDemandConfig;
  class OnDemandConfGrp;
  class DbRef;

  /** Access the database and increas user counter if the database is opend.
   *
   */
  class PixDbAccessor
  {
    friend class OnDemandConfig;
    friend class OnDemandConfGrp;
    friend class DbManager;
    friend class DbRef;
    friend class DbHandle;
    friend class PixConnectivityWrapper;

  protected:

    PixDbAccessor(DbWrapper &db, bool read_only=true) 
      : m_use(0), m_usedDb(false),m_readOnly(read_only),m_db(&db) 
    {
      m_db->incAccessorCount(); 
#ifdef DEBUG
      debug_init("ctor(DbWrapper &)");
#endif
    }

    PixDbAccessor(DbWrapper &db, const std::vector<std::string> &record_hierarchy, bool read_only=true) 
      : m_use(0), m_usedDb(false),m_readOnly(read_only),m_db(&db) 
    {
      m_db->incAccessorCount(); 
      m_recordHierarchy = record_hierarchy;
#ifdef DEBUG
      debug_init("ctor(DbWrapper &, const std::vector<std::string> &)");
#endif
    }

    PixDbAccessor(DbWrapper &db, const std::string &top_level_record_name, bool read_only=true)
      : m_use(false), m_usedDb(false),m_readOnly(read_only),m_db(&db)
    {
      m_db->incAccessorCount(); 
      m_recordHierarchy.push_back(top_level_record_name);
#ifdef DEBUG
      debug_init("ctor(DbWrapper &, const std::string &)");
#endif
    }


    PixDbAccessor(const PixDbAccessor &accesor, bool read_only=true)
      : m_use(false), m_usedDb(false),m_readOnly(read_only),m_db(accesor.m_db)
    {
      m_db->incAccessorCount();

      if (accesor.m_usedDb) {

	// special treatment for wrapped configs ( @sa DbManager::wrapConfigRecord )
	if (! m_db->canRecreate()) { 
	  m_record=accesor.m_record;
	  m_decName=m_record->getDecName();

	  m_db->use(m_readOnly);
	  m_usedDb = true;
//	  std::cout << "INFO [PixDbAccessor::ctor] Keep open Db of record : " << (m_record.get() ? m_record->getDecName() : NULL)<< "." << std::endl;
	}
      }

      //      if (accesor.m_record.get()) {
      //	m_db->use(m_readOnly);
      //	m_record=accesor.m_record;
      //	m_usedDb=true;
      //      }

      m_recordHierarchy = accesor.m_recordHierarchy;
#ifdef DEBUG
      debug_init("ctor(const PixDbAccessor &)"); 
#endif
    }

    PixDbAccessor(const PixDbAccessor &accesor, PixLib::dbRecordIterator &record_iter, const std::string &name, bool read_only=true)
      : m_use(false), m_usedDb(true),m_readOnly(read_only),m_db(accesor.m_db)
    {
      m_db->incAccessorCount(); 
      m_recordHierarchy = accesor.m_recordHierarchy;
      m_recordHierarchy.push_back(name);
      m_record = m_db->use(record_iter, m_readOnly);
      m_decName=m_record->getDecName();
#ifdef DEBUG
      debug_init("ctor(const PixDbAccessor &, PixLib::dbRecordIterator &, const std::string &)");
#endif
    }

    PixDbAccessor(const PixDbAccessor &accesor, PixLib::dbRecordIterator &record_iter, bool read_only=true)
      : m_use(false), m_usedDb(true),m_readOnly(read_only),m_db(accesor.m_db)
    {
      m_db->incAccessorCount(); 
      m_recordHierarchy = accesor.m_recordHierarchy;
      m_record = m_db->use(record_iter, m_readOnly);
      m_decName=m_record->getDecName();
      m_recordHierarchy.push_back(m_record->getName());
#ifdef DEBUG
      debug_init("ctor(const PixDbAccessor &, PixLib::dbRecordIterator &)");
#endif

    }


    PixDbAccessor(DbWrapper &db, PixLib::dbRecordIterator &record_iter, bool read_only=true)
      : m_use(false), m_usedDb(true),m_readOnly(read_only),m_db(&db)
    {
      m_db->incAccessorCount(); 
      m_record = m_db->use(record_iter, m_readOnly);
      m_decName=m_record->getDecName();
#ifdef DEBUG
      debug_init("ctor(DbWrapper &, PixLib::dbRecordIterator &)");
#endif
    }


  public:
    ~PixDbAccessor() { 
#ifdef DEBUG
      debug_init("dtor");
#endif
      if (m_usedDb) { m_db->done(); } 
      m_db->decAccessorCount(); 
    };

    /** Must be called before accessing the database.
     * must be called before @ref findRecord, @ref recordBegin, @ref recordEnd, @ref findField, @ref fieldEnd
     */
    void accessStart() {
      m_use++;
      //      std::cout << "INFO [PixDbAccessor::accessStart] " << static_cast<void *>(this)  << " users = " << m_use << std::endl;
#ifdef DEBUG
      debug_init("accessStart()");
#endif
      //std::cout<<"passing by here"<<std::endl;
    }

    /** Must be called when access is over to allow the access of the same file by a different object/user.
     */
    void accessEnd() {
      assert(m_use>0);
      m_use--;
      //      std::cout << "INFO [PixDbAccessor::accessEnd] " << static_cast<void *>(this) <<" renaining users = " << m_use << std::endl;
#ifdef DEBUG
      debug_init("accessEnd()");
#endif

      if (m_use==0) {
	if (m_usedDb) {
	  m_db->done();
	  m_usedDb=false;
//	  std::cout << "INFO [PixDbAccessor::accessEnd] delete record " << static_cast<void*>(m_record.get()) << std::endl;
	  m_record.reset();
	}
      }
    }

    /** Return true if the database is already in use.
     */
    bool isDbInUse() const {
      return m_usedDb;
    }

  protected:

    /** Open the database and descend to the correct record, if not yet done.
     * The database will be opend once. 
     */
    void prepareDbUsage() {
      if (!m_usedDb) {
	assert(m_record.get()==NULL);
	m_db->use(m_readOnly);
	m_usedDb=true;

	if (!m_decName.empty()) {
	  m_record = m_db->recordByName(m_decName);
	}
	else if (m_recordHierarchy.empty()) {
	  m_record = m_db->root();
	}
	else {
	  m_record = m_db->record(m_recordHierarchy);
	}
      }
    }

    /** Return the record of the accessor.
     * @return a shared pointer the record of the accesor.
     * It is save to call this method any time.
     */
    std::shared_ptr<PixLib::DbRecord> record() {
      prepareDbUsage();
      return m_record;
    }


    /** Find a recrod of the given name.
     * @param the name of the record.
     * @return a record iterator which still needs to be processed or the record end iterator in case of failure.
     * It is save to call this method any time.
     */
    PixLib::dbRecordIterator findRecord(const std::string &name) {
      prepareDbUsage();
      return m_record->findRecord(name);
    }


    /** Get an iterator which points at the first record (begin record iterator).
     * It is save to call this method any time.
     */
    PixLib::dbRecordIterator recordBegin() {
      prepareDbUsage();
      return m_record->recordBegin();
    }


    /** Get an iterator which points after the last record (end record iterator).
     * The result of this method will be undefined if @prepareDbUsage
     * has not been called yet. The latter will be called by @ref findRecord
     * or @ref findField. In other words, it is save to call this
     * method after findRecord has been called.
     */
    PixLib::dbRecordIterator recordEnd() const {
      return m_record->recordEnd();
    }

    /** Add a new record.
     */
    std::shared_ptr<PixLib::DbRecord> addRecord(const std::string &class_name, const std::string &slot_name) ;

    PixLib::dbFieldIterator findField(const std::string &name) {
      prepareDbUsage();
      return m_record->findField(name);
    }

    PixLib::dbFieldIterator fieldBegin() {
      prepareDbUsage();
      return m_record->fieldBegin();
    }

    /** Get the names of all fields (debuggin purpose.
     * Only works with RootDb files.
     */
    void fillListOfFields(std::vector<std::string> &field_list, bool format=true) ;


    /** Get an iterator which points after the last field (end field iterator).
     * The result of this method will be undefined if @prepareDbUsage
     * has not been called yet. The latter will be called by @ref findRecord
     * or @ref find field. In other words, it is save to call this
     * method after findField has been called.
     */
    PixLib::dbFieldIterator fieldEnd() const {
      return m_record->fieldEnd();
    }

    std::string pathName() const {
      if (m_usedDb) {
	return m_record->getDecName();
      }
      else {
	if (!m_decName.empty()) {
	  return m_decName;
	}
	std::string name;
	name += ":";
	for (std::vector<std::string>::const_iterator iter=m_recordHierarchy.begin();
	     iter!=m_recordHierarchy.end();
	     iter++) {
	  name +="/";
	  name += *iter;
	}
	return name;
      }
    }

    /** Return the name of the embraced record.
     * Will only work after @ref prepareDbUsage has been called.
     */
    std::string name() const {
      assert (m_usedDb);
      return m_record->getName();
    }

    /** Return the class name of the embraced record.
     * Will only work after @ref prepareDbUsage has been called.
     */
    std::string className() const {
      assert (m_usedDb);
      return m_record->getClassName();
    }

    /** Return true if the record contains neither fields nor records.
     */
    bool empty() const {
      return m_record->recordBegin() == m_record->recordEnd() && m_record->fieldBegin() == m_record->fieldEnd();
    }


    PixLib::PixDbInterface *db() const {
      assert(m_usedDb && m_record.get());
      return m_db->db();
    }

    bool haveToCleanUp() const { return m_db->haveToCleanUp();}

  private:

#ifdef DEBUG
    void debug_init(const std::string &method) {
      std::cout << "INFO [PixDbAccessor::" << method
		<<"] " << static_cast<void *>(this) 
		<< " users = "  << m_use <<  " db = " << static_cast<void *>(m_db)  << " in use " << m_usedDb 
		<< " record = " << static_cast<void *>(m_record.get())
		<< " name = "   << m_decName
		<< " hierarchy = ";
       for(std::vector<std::string>::const_iterator iter=m_recordHierarchy.begin();
       	  iter!=m_recordHierarchy.end();
       	  iter++) {
        std::cout << *iter << "/";
       }
       std::cout << std::endl;
    }
#endif

    unsigned int  m_use;
    bool          m_usedDb;
    bool          m_readOnly;
    DbWrapper    *m_db;
    std::shared_ptr<PixLib::DbRecord>    m_record;
    std::vector<std::string> m_recordHierarchy;
    std::string  m_decName;
  };

}
#endif
