#include <RootDb/RootDb.h>
#include <PixController/PixScan.h>
#include "TTree.h"

#include "DbWrapper.h"
#include "RootDbOpenHelper.h"
#include "DbManager.h"
#include "ConfObjUtil.h"

class StatTree_t {
public:
  StatTree_t(const std::string &root_file) : m_file(root_file.c_str(),"RECREATE") {
     m_scanName.reserve(128);
     m_tree = new TTree("ScanTimeProfiling","Scan Timing Statistics");
     m_tree->Branch("nRods",&m_nRods,"nRods/i");
     m_scanNameBranch = m_tree->Branch("scanName",static_cast<void *>( const_cast< char *>(m_scanName.c_str())),"m_scanName/C");
     m_tree->Branch("scanType",&m_scanType,"m_scanType/i");
     m_tree->Branch("nModules",&m_nModules,"nModules[nRods]/i");
     m_tree->Branch("scanningTime",&m_scanningTime,"scanningTime[nRods]/i");
     m_tree->Branch("histWritingTime",&m_histWritingTime,"histWritingTime[nRods]/i");
     m_tree->Branch("configWritingTime",&m_configWritingTime,"configWritingTime[nRods]/i");
     m_tree->Branch("totalTime",&m_totalTime,"totalTime[nRods]/i");
  }

  ~StatTree_t() {
    m_tree->Write(m_tree->GetName(),TObject::kOverwrite);
    //    delete m_tree;
    m_file.Close();
  }

  void fill() {
    m_file.cd();
    m_scanName.c_str();
    if (m_scanNameBranch->GetAddress() != m_scanName.c_str()) {
      m_scanNameBranch->SetAddress(static_cast<void *>( const_cast< char *>( m_scanName.c_str()) ));
    }
    m_tree->Fill();
  }

  static const unsigned int MAX_RODS = 10;
  
  TFile    m_file;
  TTree    *m_tree;
  TBranch *m_scanNameBranch;
  std::string m_scanName;
  unsigned int m_nRods;
  unsigned int m_scanType;
  unsigned int m_nModules[MAX_RODS];
  unsigned int m_scanningTime[MAX_RODS];
  unsigned int m_histWritingTime[MAX_RODS];
  unsigned int m_configWritingTime[MAX_RODS];
  unsigned int m_totalTime[MAX_RODS];
};

int main(int argc, char **argv)
{
  StatTree_t *result=NULL;

  PixLib::PixScan *ps=new PixLib::PixScan;

  for (int arg_i =1; arg_i<argc; arg_i++) {
    if (strcmp(argv[arg_i],"-o")==0) {
      delete result;
      if (++arg_i>argc) break;
      result=new StatTree_t(argv[arg_i]);
    }
    if (!result) {
      result=new StatTree_t("/tmp/ResultTimingAnalysis.root");
    }
    std::cout << "INFO [main] open " << argv[arg_i] << std::endl;
    PixA::DbWrapper wrapper(new PixA::RootDbOpenHelper(argv[arg_i]));
    wrapper.use();
    {
      std::shared_ptr<PixLib::DbRecord> root ( wrapper.root() );

      for (PixLib::dbRecordIterator record_iter = root->recordBegin();
	   record_iter != root->recordEnd();
	   record_iter++) {

	std::shared_ptr<PixLib::DbRecord> label_record ( *(wrapper.db()->DbProcess(record_iter, PixLib::PixDb::DBREAD) ));
	if (label_record->getClassName() == "PixScanResult") {
	  std::string label = label_record->getName();
	  result->m_scanName = label;

	  std::string::size_type pos = label.find(" ");
	  if (pos != std::string::npos ) {
	    label.erase(pos, label.size()-pos);
	  }
	  std::map<std::string, int> &types= ps->getScanTypes();
	  std::map<std::string, int>::const_iterator type_iter = types.find(label);
	  result->m_scanType= (type_iter!=types.end() ? type_iter->second : types.size());
	  std::cout << result->m_scanName << " - > " << label << " : " << result->m_scanType <<  std::endl ;

	  // loop over RODs
	  result->m_nRods=0;
	  for (PixLib::dbRecordIterator group_iter = label_record->recordBegin();
	       group_iter != label_record->recordEnd();
	       group_iter++) {

	    std::shared_ptr<PixLib::DbRecord> group_record ( *(wrapper.db()->DbProcess(group_iter, PixLib::PixDb::DBREAD) ));
	    if (group_record->getClassName() == "PixModuleGroup") {
	      result->m_nRods++;

	      PixLib::dbRecordIterator scan_iter = group_record->findRecord("Data_Scancfg");
	      if (scan_iter != group_record->recordEnd()) {
		try {
		std::shared_ptr<PixLib::DbRecord> scan_record ( *(wrapper.db()->DbProcess(scan_iter, PixLib::PixDb::DBREAD) ));
		PixLib::dbRecordIterator scan_info = scan_record->findRecord("ScanInfo");

		PixA::ConfigHandle scan_info_handle( PixA::DbManager::wrapConfigRecord(wrapper.db(), scan_info) );
		PixA::ConfigRef scan_info_config ( scan_info_handle.ref());
		PixA::ConfGrpRef time=scan_info_config["Time"];
		std::cout << "start = " << confVal<std::string>(time["ScanStart"]) << std::endl
			  << "end = " << confVal<std::string>(time["ScanEnd"]) << std::endl
			  << "write start = " << confVal<std::string>(time["ScanHwriteStart"]) << std::endl
			  << "write end = " << confVal<std::string>(time["ScanHwriteEnd"]) << std::endl
			  << std::endl;
		}
		catch (PixA::ConfigException &err) {
		  std::cout << "main: " << err << std::endl;
		}
	      }
		

	    }
	  }


	  result->fill();
	}
      }
    }
    wrapper.done();
  }
  delete result;
}
