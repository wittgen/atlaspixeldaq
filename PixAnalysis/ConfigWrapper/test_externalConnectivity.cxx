#include "ConnectivityManager.h"
#include "ConfObjUtil.h"
#include "ConfGrpRefUtil.h"
#include <BaseException.h>
#include <stdexcept>
#include "DbWrapper.h"

/** Override access restrictions
 */
class PublicConnectivityManager : public PixA::ConnectivityManager
{
public:
  ConnectivityRef giveConnectivity(PixLib::PixConnectivity *conn) {
    PixA::ConnectivityManager::giveConnectivity(conn);
  }

  PixLib::PixConnectivity *takeConnectivity(ConnectivityRef &conn) {
    PixA::ConnectivityManager::takeConnectivity(conn);
  }

  void announceModuleConfiguration(const ConnectivityRef &conn, PixLib::PixModule *module) {
    PixA::ConnectivityManager::announceModuleConfiguration(conn,module);
  }

  void canDeleteModule(const ConnectivityRef &conn, PixLib::PixModule *module) {
    PixA::ConnectivityManager::canDeleteModule(conn,module);
  }

}

int main()
{
  unsigned int n_passes = 3;
  unsigned int n_loops_no_cleanup = 3;
  unsigned int n_config_loops = 3;
    //    PixA::ConnectivityRef conn(PixA::getConnectivity("A","P","C","Cfg",1));
  for (unsigned int pass=0; pass<n_passes; pass++) {
  try {
    for (unsigned int i=0; i< n_loops_no_cleanup ; i++) {

  std::string tag("BiStave_4");


  std::shared_ptr<PixLib::PixConnectivity> external_conn= new PixLib::PixConnectivity(tag,tag,tag,"BiStave");
  PublicConnectivityManager *manager= static_cast<PublicConnectivityManager *>(PixA::ConnectivityManager->instance());
  std::map<std::string, PixLib::RodBocConnectivity *> rod_iter = external_conn->rodBocs().begin();
  std::shared_ptr<PixLib::PixModuleGroup> a_rod;
  std::string rod_name;

  if (rod_iter != external_conn->rodBocs().end()) {
    rod_name = rod_iter->first;
    std::cout << "INFO [main] Create PixModuleGroup: " << rod_name << std::endl;
    a_rod = std::shared_ptr<PixLib::PixModuleGroup>(new PixLib::PixModuleGroup(external_conn.get(),rod_iter->first));
  }
  PixA::ConnectivityRef conn_ref = Pmanager->giveConnectivity(external_conn.release());
  for (unsigned int module_i=0; module_i<9; module_i++) {
    if (external_conn->
  }

  PixA::ConnectivityRef conn(PixA::ConnectivityManager::getConnectivity(tag,tag,tag,"BiStave",0));

    //conn will wrap a pointer to IConnectivity
    // IConnectivity will contain a PixConnectivity and will cache config objects.
    // The connectivity will be a shared property. If there are no more users, the 
    // connectivity will be deleted eventually.

  for (unsigned int j=0; j<n_config_loops; j++) {
    const PixA::Pp0List &pp0s=conn.pp0s(rod_name);
    // read only
    for (PixA::Pp0List::const_iterator pp0_iter=pp0s.begin();
	 pp0_iter != pp0s.end(); // may be slightly inefficient unless the compiler is smart enough.
	 pp0_iter++) {

      // it may be more efficient to call modulesBegin and modulesEnd only once:
      PixA::ModuleList::const_iterator modules_begin=PixA::modulesBegin(pp0_iter);
      PixA::ModuleList::const_iterator modules_end=PixA::modulesEnd(pp0_iter);

      for (PixA::ModuleList::const_iterator module_iter=modules_begin;
 	   module_iter != modules_end;
 	   module_iter++) {
 	const PixLib::Config &config = conn.fullModuleConfig(module_iter);
	// 	const_cast<PixLib::Config &>(config).dump(std::cout);
      }



      const PixA::ConfigRef boc_config(conn.bocConf(pp0_iter));

      //rx_config will wrap a pointer to a IConfig (shared object)
      // this will eventually open a data file which will call the use mathod from IDbWrapper
      // once the ConfigRef object is deleted the IConfig interface call the donw method from IDbWrapper
      // if a data file was opened.

      for (unsigned int rx_i=0; rx_i<4; rx_i++) {
	const PixA::ConfigRef tx_config(subConfig(boc_config,"PixTx",rx_i));
	const PixA::ConfGrpRef tx_config_bpm(tx_config["Bpm"]);
	const PixA::ConfigRef rx_config(subConfig(boc_config,"PixRx",rx_i));
	{
	  const PixA::ConfGrpRef rx_config_general(rx_config["General"]);
	  // get config["general"]["DataDelay1"];
	  //      unsigned int fibre_i=2;
	  for (unsigned int fibre_i=2; fibre_i<10 ;fibre_i++) {
	    std::cout << " DataDelay "<<fibre_i<< "  = " << confVal<int>(confElm(rx_config_general,"DataDelay",fibre_i)) << std::endl;
	  }

	  //	  const PixA::ConfGrpRef tx_config_general(rx_config["General"]);
	  // get config["general"]["DataDelay1"];
	  //      unsigned int fibre_i=2;
	  for (unsigned int fibre_i=2; fibre_i<10 ;fibre_i++) {
	    std::cout << " MarkSpace "<<fibre_i<< " = " << confVal<int>(confElm(tx_config_bpm,"MarkSpace",fibre_i)) << std::endl;
	  }
	}
	{
	  const PixA::ConfGrpRef rx_config_opt(rx_config["Opt"]);
	  for (unsigned int fibre_i=2; fibre_i<10 ;fibre_i++) {
	    std::cout << " Threshold "<<fibre_i<< "  = " << confVal<int>(confElm(rx_config_opt,"RxThreshold",fibre_i)) << std::endl;
	  }
	}
      }
    }

    

    external_conn = std::shared_ptr<PixLib::PixConnectivity>( manager->takeConnectivity(conn_ref) );
    external_conn.reset();
    }
  
    }
  }
  catch (SctPixelRod::BaseException &err) {
    std::cout << "FATAL [main] caught exception " << err << std::endl;
  }
  catch (std::exception &err) {
    std::cout << "FATAL [main] caught exception " << err.what() << std::endl;
  }
  PixA::ConnectivityManager::instance()->cleanup();
#ifdef DEBUG
  PixA::DbWrapper::stat();
#endif
  }
}
