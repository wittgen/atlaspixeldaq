//#include <ipc/partition.h>
//#include <ipc/core.h>

#include <iostream>
#include <iomanip>
#include <memory>


#include "pixa_exception.h"
#include "Connectivity.h"
#include "ConnectivityManager.h"
#include "ConnectivityUtil.h"
#include "PixDisableUtil.h"

#include "PixConnectivityWrapper.h"
#include <createDbServer.h>

#include "TTimeMeasure.h"

class IPCInstance
{

 private:
  IPCInstance(int argc, char **argv, const std::string &partition_name) {
    IPCCore::init(argc, argv);
    m_ipcPartition=std::make_shared<IPCPartition>(partition_name.c_str());

    if (!m_ipcPartition->isValid()) {
      std::stringstream message;
      message << "ERROR [IPCInstance] Not a valid partition " << partition_name << std::endl;
      throw std::runtime_error(message.str());
    }
  }
 public:

  static IPCInstance *instance(int argc, char **argv, const std::string &partition_name) {
    if (!s_instance) {
      s_instance = new IPCInstance(argc,argv,partition_name);
    }
    return s_instance;
  }

  IPCPartition &partition() { return *m_ipcPartition;}
  
 private:
  std::shared_ptr<IPCPartition> m_ipcPartition;
  static IPCInstance *s_instance;

};

IPCInstance *IPCInstance::s_instance = NULL;

class MyPixDbServerInterface : public PixLib::PixDbServerInterface
{
public:
  MyPixDbServerInterface(IPCPartition *ipcPartition, std::string ipcName,  PixLib::PixDbServerInterface::serviceType serviceTypeArg,  bool &ready)
    : PixLib::PixDbServerInterface(ipcPartition, ipcName, serviceTypeArg, ready) {
    std::cout << "INFO [MyPixDbServerInterface] new instance " << static_cast<void *>(this) << std::endl;
  }

  ~MyPixDbServerInterface()
  {
    std::cout << "INFO [MyPixDbServerInterface] destruct instance " << static_cast<void *>(this) << std::endl;
  }

};

bool setDbServer(const std::string &db_server_partition_name,
		 const std::string &db_server_name,
		 int argc,
		 char **argv)
{
  int temp_argc=0;
  char *temp_argv[1]={NULL};
  IPCInstance *ipc_instance = IPCInstance::instance(temp_argc,temp_argv, db_server_partition_name);

  std::shared_ptr<PixLib::PixDbServerInterface> db_server( PixA::createDbServer(ipc_instance->partition(),db_server_name) );
  if (db_server.get()) {
    PixA::ConnectivityManager::useDbServer(db_server);
    return true;
  }
  return false;
}

std::string enableString( bool enabled ) {
  if (enabled) return "ENABLED ";
  else return         "disabled";
}

typedef int LinkArray_t[4][8];

std::string s_modeNames[3]={std::string("40"),std::string("80a"),std::string("80b")};

const LinkArray_t *linkMap(const PixLib::RodBocConnectivity *rod_boc, unsigned int mode_i)
{
  PixLib::RodBocLinkMap *link_map = const_cast<PixLib::RodBocConnectivity *>(rod_boc)->linkMap();
  switch( mode_i) {
  case 0:
    return &link_map->rodRx40;
  case 1:
    return &link_map->rodRx80a;
  case 2:
    return &link_map->rodRx80a;
  default:
    return NULL;
  }
}

PixA::DisabledListRoot createRootDisabledList( const std::shared_ptr<const PixLib::PixDisable> &pix_disable, const std::string &enable_string)
{
  if (enable_string.empty()) {
    return PixA::DisabledListRoot(pix_disable);
  }
  else {
    return PixA::DisabledListRoot( PixA::createEnableMap(enable_string) );
  }
}

int main(int argc, char **argv)
{
  std::string id_name="CT";
  std::string tag_name;
  std::string cfg_tag_name;
  std::string mod_cfg_tag_name;

  std::string enable_string;

  //bool show_links = false;
  bool db_server_tags=false;
  bool show_disabled_objects=false;
  int verbose=1;

  std::vector<std::string> deactivate_list;
  std::vector<std::string> disable_readout_list;
  std::vector<std::string> rod_names;

  std::string load_disable_name;
  
  std::shared_ptr<PixLib::PixDisable> pix_disable;
  std::shared_ptr<const PixLib::PixDisable> the_pix_disable;

  for (int arg_i=1; arg_i<argc; arg_i++) {

    if (strcmp(argv[arg_i],"--db-server")==0 && arg_i+2<argc && argv[arg_i+1][0]!='-' && argv[arg_i+2][0]!='-') {

      try {
	std::string db_server_partition_name = argv[++arg_i];
	std::string db_server_name = argv[++arg_i];
	db_server_tags = setDbServer(db_server_partition_name, db_server_name, argc, argv);
      }
      catch (std::runtime_error &err) {
	std::cout << "FATAL [main] caught exception (std::exception) : " << err.what() << std::endl;
	return -1;
      }

    }
    else if ((strcmp(argv[arg_i],"--deactivate")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      deactivate_list.push_back(argv[++arg_i]);
    }
    else if ((strcmp(argv[arg_i],"--disable-readout")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      disable_readout_list.push_back(argv[++arg_i]);
    }
    else if ((strcmp(argv[arg_i],"-d")==0 || strcmp(argv[arg_i],"--disable")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      if (!pix_disable.get()) {
	pix_disable = std::make_shared<PixLib::PixDisable>("test");
      }
      pix_disable->disable( argv[++arg_i]);
    }
    else if ((strcmp(argv[arg_i],"-r")==0 || strcmp(argv[arg_i],"--rod")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      rod_names.push_back( argv[++arg_i] );
    }
    else if (strcmp(argv[arg_i],"-a")==0 ) {
      show_disabled_objects = true;
    }
    else if (strcmp(argv[arg_i],"-v")==0 || strcmp(argv[arg_i],"--verbose")==0) {
      verbose++;
    }
    else if (strcmp(argv[arg_i],"-q")==0 || strcmp(argv[arg_i],"--quiet")==0) {
      if (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	verbose=atoi(argv[++arg_i]);
      }
      else {
	verbose++;
      }
    }
    else if (strcmp(argv[arg_i],"--load-disable")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      load_disable_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"--enable-list")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      std::ifstream in(argv[++arg_i]);
      while (in) {
	std::string a_string;
	in >> a_string ;
	enable_string += a_string;
      }
    }
    else if (strcmp(argv[arg_i],"-ot")==0 || strcmp(argv[arg_i],"-o")==0) {
      PixA::ConnectivityRef conn;
      try {
      std::vector<std::string> object_tags;
      std::cout << "INFO [" << argv[0] << ":main] get object tags." << std::endl;
      PixA::ConnectivityManager::instance()->getObjectTags(&object_tags, db_server_tags);
      std::cout << "----------- Object tags " << std::endl;
      for (std::vector<std::string>::iterator it = object_tags.begin(); it!=object_tags.end(); it++) {
	std::cout << *it << std::endl;
      }

      std::cout << "INFO [" << argv[0] << ":main] get configuration object tags." << std::endl;
      PixA::ConnectivityManager::instance()->getConfigurationObjectTags(&object_tags, db_server_tags);
      std::cout << "----------- Configuration Object tags " << std::endl;
      for (std::vector<std::string>::iterator it = object_tags.begin(); it!=object_tags.end(); it++) {
	std::cout << *it << std::endl;
      }
      }
      catch (PixA::ConfigException &err) {
	std::cout << "FATAL [main] caught exception (ConfigException) : " << err.getDescriptor() << std::endl;
	return -1;
      }
      catch (PixA::BaseException &err) {
	std::cout << "FATAL [main] caught exception (BaseException) : " << err.getDescriptor() << std::endl;
	return -1;
      }
      catch (std::exception &err) {
	std::cout << "FATAL [main] caught exception (std::exception) : " << err.what() << std::endl;
	return -1;
      }
      catch (...) {
	std::cout << "FATAL [main] unhandled exception. "  << std::endl;
	return -1;
      }
      return 0;
    }
    else if (strcmp(argv[arg_i],"-lt")==0 || strcmp(argv[arg_i],"-l")==0) {
      PixA::ConnectivityRef conn;
      try {
      std::vector<std::string> conn_tags;
      std::vector<std::string> alias_tags;
      std::vector<std::string> payload_tags;
      std::vector<std::string> cfg_tags;
      std::cout << " manager instance = " << static_cast<void *>(PixA::ConnectivityManager::instance()) << std::endl ;
      PixA::ConnectivityManager::instance()->getConnectivityTags(id_name, &conn_tags, &alias_tags, &payload_tags, &cfg_tags,db_server_tags);

      std::cout << "----------- Connectivity tags " << std::endl;
      for (std::vector<std::string>::iterator it = conn_tags.begin(); it!=conn_tags.end(); it++) {
	std::cout << *it << std::endl;
      }
      std::cout << "----------- Alias tags " << std::endl;
      for (std::vector<std::string>::iterator it = alias_tags.begin(); it!=alias_tags.end(); it++) {
	std::cout << *it << std::endl;
      }
      std::cout << "----------- Payload tags " << std::endl;
      for (std::vector<std::string>::iterator it = payload_tags.begin(); it!=payload_tags.end(); it++) {
	std::cout << *it << std::endl;
      }
      std::cout << "----------- Configuration tags " << std::endl;
      for (std::vector<std::string>::iterator it = cfg_tags.begin(); it!=cfg_tags.end(); it++) {
	std::cout << *it << std::endl;
      }
      }
      catch (PixA::ConfigException &err) {
	std::cout << "FATAL [main] caught exception (ConfigException) : " << err.getDescriptor() << std::endl;
	return -1;
      }
      catch (PixA::BaseException &err) {
	std::cout << "FATAL [main] caught exception (BaseException) : " << err.getDescriptor() << std::endl;
	return -1;
      }
      catch (std::exception &err) {
	std::cout << "FATAL [main] caught exception (std::exception) : " << err.what() << std::endl;
	return -1;
      }
      catch (...) {
	std::cout << "FATAL [main] unhandled exception. "  << std::endl;
	return -1;
      }
      return -1;
    }
    else if (strcmp(argv[arg_i],"-i")==0 && arg_i+1<argc) {
      id_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-x")==0 && arg_i+1<argc) {
      //show_links = true;
    }
    else if (strcmp(argv[arg_i],"-t")==0 && arg_i+2<argc && argv[arg_i+2][0]!='-' && argv[arg_i+1][0]!='-') {
      tag_name = argv[++arg_i];
      cfg_tag_name = argv[++arg_i];
      if (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	mod_cfg_tag_name = argv[++arg_i];
      }
    }
    else {
      std::cout << "ERROR ["<< argv[0] << ":main] unhandled argument " << arg_i << " : " << argv[arg_i]<< "." << std::endl;
    }
  }

  if (tag_name.empty() || cfg_tag_name.empty()) {
      std::cout << "usage: " << argv[0] << "[-i ID-tag] -t tag cfg-tag / -l / -ot [--db-server partition name] [-d disabled-conn-obj]" << std::endl;
      return -1;
  }
  if (mod_cfg_tag_name.empty()) {
    mod_cfg_tag_name = cfg_tag_name;
  }

  PixA::ConnectivityRef conn;
  try {

      std::vector<std::string> conn_tags;
      std::vector<std::string> alias_tags;
      std::vector<std::string> payload_tags;
      std::vector<std::string> cfg_tags;
      std::cout << " manager instance = " << static_cast<void *>(PixA::ConnectivityManager::instance()) << std::endl ;
      PixA::ConnectivityManager::instance()->getConnectivityTags(id_name, &conn_tags, &alias_tags, &payload_tags, &cfg_tags,db_server_tags);

      std::cout << "----------- Connectivity tags " << std::endl;
      for (std::vector<std::string>::iterator it = conn_tags.begin(); it!=conn_tags.end(); it++) {
	std::cout << *it << std::endl;
      }

    conn=PixA::ConnectivityManager::getConnectivity( id_name, tag_name, tag_name, tag_name, cfg_tag_name, mod_cfg_tag_name, static_cast<PixA::Revision_t>(0) );
  }
  catch (PixA::ConfigException &err) {
    std::cout << "FATAL [main] caught exception (ConfigException) : " << err.getDescriptor() << std::endl;
    return -1;
  }
  catch (PixA::BaseException &err) {
    std::cout << "FATAL [main] caught exception (BaseException) : " << err.getDescriptor() << std::endl;
    return -1;
  }
  catch (std::exception &err) {
    std::cout << "FATAL [main] caught exception (std::exception) : " << err.what() << std::endl;
    return -1;
  }
  catch (...) {
    std::cout << "FATAL [main] unhandled exception. "  << std::endl;
    return -1;
  }

  if (!load_disable_name.empty() && conn) {
    the_pix_disable = conn.getDisable(load_disable_name);
  }
  else {
    the_pix_disable = pix_disable;
  }

  for (std::vector<std::string>::const_iterator disable_iter = disable_readout_list.begin();
       disable_iter != disable_readout_list.end();
       disable_iter++) {

    PixLib::ModuleConnectivity *module_conn = const_cast< PixLib::ModuleConnectivity * >( conn.findModule(*disable_iter) );
    if (module_conn) {
      module_conn->enableReadout = false;
    }
    else {
      PixLib::RodBocConnectivity *rod_conn = const_cast<PixLib::RodBocConnectivity *>( conn.findRod(*disable_iter) );
      if (rod_conn) {
	rod_conn->enableReadout = false;
      }
      else {
	PixLib::RodCrateConnectivity *crate_conn = const_cast<PixLib::RodCrateConnectivity *>( conn.findCrate(*disable_iter) );
	if (crate_conn) {
	  crate_conn->enableReadout = false;
	}
      }
    }
  }

  for (std::vector<std::string>::const_iterator disable_iter = deactivate_list.begin();
       disable_iter != deactivate_list.end();
       disable_iter++) {
    PixLib::ModuleConnectivity *module_conn = const_cast< PixLib::ModuleConnectivity * >( conn.findModule(*disable_iter) );
    if (module_conn) {
      module_conn->active() = false;
    }
    else {
      PixLib::Pp0Connectivity *pp0_conn = const_cast< PixLib::Pp0Connectivity * >( conn.findPp0(*disable_iter) );
      if (pp0_conn) {
	pp0_conn->active() = false;
      }
      else {
	PixLib::RodBocConnectivity *rod_conn = const_cast<PixLib::RodBocConnectivity *>( conn.findRod(*disable_iter) );
	if (rod_conn) {
	  rod_conn->active() = false;
	}
	else {
	  PixLib::RodCrateConnectivity *crate_conn = const_cast<PixLib::RodCrateConnectivity *>(conn.findCrate(*disable_iter));
	  if (crate_conn) {
	    crate_conn->active() = false;
	  }
	}
      }
    }
  }
  
  TTimeMeasure timer;
  timer.Start();
  for (unsigned int loop=0; loop<10; loop++) {
  PixA::DisabledListRoot disabled_list_root( createRootDisabledList( the_pix_disable, enable_string) );
  //  const PixA::DisabledList disabled_list( PixA::disableList( disabled_list_root) );
  const LinkArray_t *last_ptr[3]={NULL,NULL,NULL};

  for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
       crate_iter != conn.crates().end();
       ++crate_iter) {

    bool crate_enabled = PixA::enabled( disabled_list_root, crate_iter );

    std::cout << "Crate : " << PixA::connectivityName(crate_iter)  << " " << enableString(crate_enabled) << std::endl;
    if (!crate_enabled && !show_disabled_objects) continue;

    const PixA::DisabledList rod_disabled_list( PixA::rodDisabledList( disabled_list_root, crate_iter) );

    PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
    PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
    for (PixA::RodList::const_iterator rod_iter = rod_begin;
	 rod_iter != rod_end;
	 ++rod_iter) {

      if (!rod_names.empty()) {
	if ( std::find(rod_names.begin(), rod_names.end(), PixA::connectivityName(rod_iter) ) == rod_names.end()) continue;
      }

      // for testing get a rod disable list from the root list 
      const PixA::DisabledList rod_disabled_list_2( PixA::rodDisabledList( disabled_list_root, *rod_iter ) );

      bool rod_enabled = PixA::enabled( rod_disabled_list, rod_iter );
      bool rod_enabled_2 = PixA::enabled( rod_disabled_list_2, rod_iter );

      std::cout << "ROD : " << PixA::connectivityName(rod_iter)  << " " << enableString(rod_enabled);
      if (verbose>1) {
	const PixLib::TimConnectivity *tim_conn = PixA::responsibleTim(*rod_iter);
	if (tim_conn) {
	  std::cout << " -> " << PixA::connectivityName(tim_conn);
	  if (!PixA::timEnabled( rod_disabled_list, crate_iter )) {
	    std::cout << " (disabled)";
	  }
	}
      }
      std::cout << std::endl;
      if (verbose>2) {
	std::cout << "ROD/2 : " << PixA::connectivityName(rod_iter)  << " " << enableString(rod_enabled_2) << std::endl;
	std::cout << "\tcrate " << PixA::connectivityName( PixA::crate(*rod_iter) )  << " " << enableString(rod_enabled) << std::endl;
	std::cout << "\tpartition " << PixA::connectivityName( PixA::partition( *rod_iter) )  << " " << enableString(rod_enabled) << std::endl;
	std::cout << "\tpartition " << PixA::connectivityName( PixA::partition( PixA::crate(*rod_iter)) )  << " " << enableString(rod_enabled) << std::endl;
      }
      if (verbose>3) {
	for (unsigned int mode_i=0; mode_i<3; mode_i++) {
	  const LinkArray_t *ptr = linkMap(*rod_iter, mode_i);
	  if (ptr) {
	    const LinkArray_t &links = *ptr;
	    bool non_empty[5];
	    for(unsigned int i=0; i<5; i++) {
	      non_empty[i]=false;
	    }
	    for (unsigned int plugin_i=0; plugin_i<4; plugin_i++) {
	      for (unsigned int channel_i=0; channel_i<8; channel_i++) {
		if (links[plugin_i][channel_i]>=0) {
		  non_empty[plugin_i]=true;
		  non_empty[4]=true;
		  break;
		}
	      }
	    }
	    if (non_empty[4]) {
	      for (unsigned int plugin_i=0; plugin_i<4; plugin_i++) {
		if (non_empty[plugin_i]) {
		  std::cout << std::setw(3) << s_modeNames[mode_i] << " RX " << static_cast<char>('A'+plugin_i) << " : ";
		  for (unsigned int channel_i=0; channel_i<8; channel_i++) {
		    std::cout << std::setw(5) << links[plugin_i][channel_i]/10 << " " << links[plugin_i][channel_i]%10;
		  }
		  std::cout << std::endl;
		}
	      }
	    }

	  }
	}
      }
      else {
	unsigned int agree_counter=0;
	for (unsigned int mode_i=0; mode_i<3; mode_i++) {
	  const LinkArray_t *ptr = linkMap(*rod_iter, mode_i);
	  if (ptr) {
	    if (last_ptr[mode_i]) {
	      for (unsigned int plugin_i=0; plugin_i<4; plugin_i++) {

		bool differ=false;
		for (unsigned int channel_i=0; channel_i<8; channel_i++) {
		  if ((*ptr)[plugin_i][channel_i] != (*last_ptr[mode_i])[plugin_i][channel_i]) {
		    differ=true;
		  }
		  if (differ) {
		    std::cout  << PixA::connectivityName(rod_iter) << "  link map " << s_modeNames[mode_i]  << " RX " << static_cast<char>('A'+plugin_i) << " differs," <<std::endl;
		  }
		  else {
		    agree_counter++;
		  }
		}
	      }
	    }

	    last_ptr[mode_i]=ptr;
	  }
	}
	if (agree_counter>0) {
	  std::cout << PixA::connectivityName(rod_iter) << " " << agree_counter << " link map(s) agree with last ROD " <<std::endl;
	}
      }


      if (!rod_enabled && !show_disabled_objects) continue;

      const PixA::DisabledList pp0_disabled_list( PixA::pp0DisabledList( rod_disabled_list, rod_iter) );
      PixA::DisabledListRoot root_dummy;
      const PixA::DisabledListRoot root_pp0_disabled_list( PixA::createPp0DisabledList( conn, disabled_list_root, PixA::connectivityName(rod_iter)) );
      root_dummy = root_pp0_disabled_list;

      PixA::Pp0List::const_iterator pp0_begin = PixA::pp0Begin(rod_iter);
      PixA::Pp0List::const_iterator pp0_end = PixA::pp0End(rod_iter);
      for (PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
	   pp0_iter != pp0_end;
	   ++pp0_iter) {

	bool pp0_enabled = PixA::enabled( pp0_disabled_list, pp0_iter );
	bool pp0_enabled_2 = PixA::enabled( root_pp0_disabled_list, pp0_iter );

	std::cout << " PP0 : " << PixA::connectivityName(pp0_iter)   << " cl=" << PixA::coolingLoopName(pp0_iter)
		  << " " << enableString(pp0_enabled)
		  << " ==  " << enableString(pp0_enabled_2) << " == " << root_pp0_disabled_list.enabled( PixA::connectivityName(pp0_iter) )
		  << " connected to TX " << PixA::txPlugin(pp0_iter);

	unsigned int rx_plugin_dto1 = PixA::rxPlugin(pp0_iter, PixA::DTO1);
	unsigned int rx_plugin_dto2 = PixA::rxPlugin(pp0_iter, PixA::DTO2);
	if (rx_plugin_dto1<4) {
	  std::cout << ", DTO1 RX " << PixA::rxPlugin(pp0_iter, PixA::DTO1);
	}
	if (rx_plugin_dto2<4) {
	  std::cout << ", DTO2 RX " << PixA::rxPlugin(pp0_iter, PixA::DTO2);
	}
	std::cout << std::endl;

	if (!pp0_enabled && !show_disabled_objects) continue;

	const PixA::DisabledList module_disabled_list( PixA::moduleDisabledList( pp0_disabled_list, pp0_iter) );
	const PixA::DisabledList module_disabled_list_2( PixA::moduleDisabledList( root_pp0_disabled_list, pp0_iter) );


	PixA::ModuleList::const_iterator module_begin = PixA::modulesBegin(pp0_iter);
	PixA::ModuleList::const_iterator module_end = PixA::modulesEnd(pp0_iter);
	for (PixA::ModuleList::const_iterator module_iter = module_begin;
	     module_iter != module_end;
	     ++module_iter) {

	  bool module_enabled = PixA::enabled( module_disabled_list, module_iter );

	  PixA::ModulePluginConnection tx_connection = PixA::txConnection(module_iter);
	  PixA::ModulePluginConnection rx_connection_dto1 = PixA::rxConnection(module_iter, PixA::DTO1);
	  PixA::ModulePluginConnection rx_connection_dto2 = PixA::rxConnection(module_iter, PixA::DTO2);

	  std::cout << " Module : " << PixA::connectivityName(module_iter);
	  if (verbose>0) {
	    std::cout << " (" << PixA::productionName(module_iter) << ") ";
	  }
	  std::cout << " " << enableString(module_enabled)
		    << " " << enableString( PixA::enabled( module_disabled_list_2, module_iter ) )
		    << " PP0 slot = " << PixA::pp0Slot(module_iter)
		    << " connected to TX " <<  tx_connection.plugin() << "/" << tx_connection.channel();
	  
	  if (rx_connection_dto1.isValid()) {
	    std::cout << ", DTO1 RX " <<  rx_connection_dto1.plugin() << "/" << rx_connection_dto1.channel();
	  }

	  if (rx_connection_dto2.isValid()) {
	    std::cout << ", DTO2 RX " <<  rx_connection_dto2.plugin() << "/" << rx_connection_dto2.channel();
	  }
	  std::cout << std::endl;

	}
      }
    }
  }
  timer.Stamp();
  }
  timer.Show();
  return 0;
}
