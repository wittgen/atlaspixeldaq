#ifndef _PixA_ConfigHandle_h_
#define _PixA_ConfigHandle_h_

#include <memory>
#include <cassert>

#include "OnDemandConfig.h"
#include "DbWrapper.h"
#include "ConfigRef.h"

namespace PixA {

  class MutableConfigRef;
  class ConfigRef;

  /** Handle to a config object.
   * References to config objects @ref MutableConfigRef may open and leave open RootDb
   * files. To avoid conflicts, with other components, whicht try to access the
   * same files, the references should only be kept for the periods access to the
   * config objects really is needed. On the other hand, opening RootDb files is
   * a slow operation, thus one should not release the config references too early.
   */
  class ConfigHandle {
  protected:

  public:

    ConfigHandle(IConfig &config) : m_config(&config) { /*prevent deletion*/ m_config->IConfigBase::useNode(); }

    /** Copy constructor.
     * Copy a handle.
     */
    ConfigHandle(const ConfigHandle &handle) : m_config(handle.m_config) { if (m_config) m_config->IConfigBase::useNode();}


    /** Get A handle from a reference.
     */
    ConfigHandle(const ConfigRef &ref) : m_config(ref.m_config) { assert(ref.m_config); m_config->IConfigBase::useNode();}


    /** Assignement operator.
     * Copy a handle.
     */
    ConfigHandle &operator=(const ConfigHandle &handle) { 
      if (m_config) m_config->IConfigBase::doneWithNode(); 
      m_config = handle.m_config; 
      if (m_config) m_config->IConfigBase::useNode(); 
      return *this; 
    }

    ~ConfigHandle() {
      if (m_config) {
	m_config->IConfigBase::doneWithNode();
      }
      /*DEBUG: */ m_config=NULL;
    }

    /** Create an invalid handle.
     */
    ConfigHandle() : m_config(NULL) {}

    /** Return true if the handle is valid.
     */
    bool isValid() const {
      return m_config != NULL;
    }

    /** Return true if the handle is valid.
     */
    operator bool() const {return isValid();}

    /** Return true if the two config handles point to the same config interface.
     */
    bool operator==(const ConfigHandle &a) const {return m_config == a.m_config; }

    /** Return true if the config handle points to the given config interface.
     */
    bool operator==(const IConfig * const a) const {return a == m_config; }

    /** Get a reference to the config object (read only).
     * Using the reference may open a RootDb file, which will stay open
     * until the reference object is destroyed.
     */
    ConfigRef ref() const  {
      return ConfigRef(*m_config);
    }

    /** Get the total number of users of any branch of the config.
     */
    unsigned int use_count() const { return m_config->root_use_count();}

  protected:
    IConfig *m_config;
  };

  /** Handle to a config object.
   * References to config objects @ref MutableConfigRef may open and leave open RootDb
   * files. To avoid conflicts, with other components, whicht try to access the
   * same files, the references should only be kept for the periods access to the
   * config objects really is needed. On the other hand, opening RootDb files is
   * a slow operation, thus one should not release the config references too early.
   */
  class MutableConfigHandle : public ConfigHandle {
  public:

    MutableConfigHandle(IConfig &config) : ConfigHandle(config) {  }

    /** Copy constructor.
     * Copy a handle.
     */
    MutableConfigHandle(const MutableConfigHandle &handle) : ConfigHandle(handle) { }

    /** Get A handle from a reference.
     */
    MutableConfigHandle(const MutableConfigRef &ref) : ConfigHandle(ref) { }


    /** Assignement operator.
     * Copy a handle.
     */
    MutableConfigHandle &operator=(const MutableConfigHandle &handle) { 
      ConfigHandle::operator=(handle);
      return *this;
    }

    /** Create an invalid handle.
     */
    MutableConfigHandle()  {}

    /** Get a reference to the config object.
     * Using the reference may open a RootDb file, which will stay open
     * until the reference object is destroyed.
     */
    MutableConfigRef ref()  {
      return MutableConfigRef(*m_config);
    }

  private:
  };


}

#endif
