#include "DbWrapper.h"
#include "DbManager.h"

namespace PixA {

  ObjStat_t DbWrapper::s_stat("DbWrapper");

  DbWrapper::DbWrapper(PixLib::PixDbInterface *db) : m_creator(NULL), m_dbUsers(1),m_nAccessors(0),m_isOpen(true),m_cleanup(true),m_isWritable(false) {

    // The database is already open that is why m_dbUsers is set to one 
    // The accessor has not been created yet -> m_nAccessors(0)

    m_db.first=db;
    if (haveToCleanUp()) {
    m_db.second=std::shared_ptr<PixLib::DbRecord>(db->readRootRecord());
    }
    else {
      m_db.second=std::shared_ptr<PixLib::DbRecord>(db->readRootRecord(),do_not_deleter<PixLib::DbRecord>);
    }
    s_stat.ctor();
  }

  DbWrapper::~DbWrapper() 
  {
    s_stat.dtor();
    if (m_creator) {
      //      std::cout << "INFO [DbWrapper::dtor] wrapper " << static_cast<void *>(this)  << " : delete database and root record "
      //      		<< ( m_db.second ? m_db.second->getDecName() : "NULL")
      //		<< " (remaining users=" << m_dbUsers <<", accessors=" << m_nAccessors << ")."
      //      		<< std::endl;
      // If the DbWrapper has been created around an existing db record then 
      // it should bot been deleted
      if (m_dbUsers != 1) {
	if (m_isOpen) {
	assert ( m_dbUsers==0 );
	  closeDb(true);
	}
      }
      assert ( m_nAccessors==0 );
      delete m_creator;
      m_creator=NULL;
      m_dbUsers =0;
    }
  }



  void DbWrapper::closeDb(bool force) 
  {
    //    std::cout << "INFO [DbWrapper::close] " << static_cast<void *>(this) 
    //	      << " db = " << static_cast<void *>(m_db.first) 
    //	      << " (remaining users=" << m_dbUsers <<", accessors=" << m_nAccessors << ")." << std::endl;
    assert (m_isOpen);
    assert (m_creator);
    if (force || m_creator->mustAlwaysClose()) {
#   ifdef DEBUG
    if (m_db.first) {
      closeFile();
    }
#   endif
      m_db.second.reset();

    delete m_db.first;
    m_db.first=NULL;
      m_isOpen=false;
      m_isWritable=false;

      //std::cout << "INFO [DbWrapper::close] Closed db file." << std::endl;
    }
  }

  void DbWrapper::openDb() 
  {
    //	std::cout << "INFO [DbWrapper::use] open database and get root record "
    //		  << std::endl;
    if (!m_isOpen) {
      assert(m_db.first == NULL && m_db.second.get()==NULL);
#     ifdef DEBUG
      openFile();
#     endif
      assert(m_creator);
      std::pair<PixLib::PixDbInterface *, PixLib::DbRecord *> ret = m_creator->open();
      m_db.first = ret.first;
      if (haveToCleanUp()) {
	m_db.second = std::shared_ptr<PixLib::DbRecord>(ret.second);
      }
      else {
	m_db.second = std::shared_ptr<PixLib::DbRecord>(ret.second, do_not_deleter<PixLib::DbRecord>);
      }
      m_isOpen=true;
#     ifdef DEBUG
        keepRecord(m_db.second);
#     endif
    }
  }

  void DbWrapper::openDbForWriting() {
    if (m_dbUsers!=0 && !m_isWritable) {
      std::stringstream message;
      message << "FATAL [RootDbHelper::useForWriting] Database already opened in READONLY mode.";
      throw PixLib::PixDBException(message.str());
    }

    if (m_isOpen) {
      closeDb(true);
    }

    assert(m_db.first == NULL && m_db.second.get()==NULL);

#   ifdef DEBUG
    openFile();
#   endif

    assert(m_creator);
    std::pair<PixLib::PixDbInterface *, PixLib::DbRecord *> ret = m_creator->openForWriting();
    m_db.first = ret.first;
    if (haveToCleanUp()) {
      m_db.second = std::shared_ptr<PixLib::DbRecord>(ret.second);
    }
    else {
      m_db.second = std::shared_ptr<PixLib::DbRecord>(ret.second, do_not_deleter<PixLib::DbRecord>);
  }
    m_isOpen=true;
    m_isWritable=true;

#   ifdef DEBUG
    keepRecord(m_db.second);
#   endif
    
  }


  void DbWrapper::destroy() 
  {
//     std::cout << "INFO [DbWrapper::destroy] done with" << static_cast<void *>(this) 
// 	      << " db = " << static_cast<void *>(m_db.first) 
// 	      << " (remaining users=" << m_dbUsers <<", accessors=" << m_nAccessors << ") will be deleted." << std::endl;
    DbManager::instance()->destroy(this);
  }

  std::shared_ptr<PixLib::DbRecord> DbWrapper::record(const std::vector<std::string> &name) {
    std::shared_ptr<PixLib::DbRecord> root_record=root();
    std::shared_ptr<PixLib::DbRecord> master_root_record=root_record;
    //    std::cout << "INFO [DbWrapper::record] Search record in " << root_record->getDecName() << std::endl;
    std::shared_ptr<PixLib::DbRecord> copy;
    for (std::vector<std::string>::const_iterator record_iter=name.begin();
	 record_iter!=name.end();
	 record_iter++) {

      assert ( root_record.get() );

      //      std::cout << "INFO [DbWrapper::record] Search for  " << *record_iter << " in " << root_record->getDecName() << std::endl;

      PixLib::dbRecordIterator child=root_record->findRecord(*record_iter);
      if  ( child  == root_record->recordEnd() ) {
	std::stringstream message;
	message << "FATAL [RootDbHelper::open] Did not find record " << *record_iter << " in " << root_record->getName() << " of " << master_root_record->getDecName() << ".";
	throw PixLib::PixDBException(message.str());
      }

      PixLib::DbRecord *temp_record = *root_record->getDb()->DbProcess(child, PixLib::PixDb::DBREAD);
      if (haveToCleanUp()) {
	root_record = std::shared_ptr<PixLib::DbRecord>(temp_record);
      }
      else {
	root_record = std::shared_ptr<PixLib::DbRecord>(temp_record, do_not_deleter<PixLib::DbRecord>);
      }

    }
    //    std::cout << "INFO [DbWrapper::record] new record "  << static_cast<void *>(root_record.get()) << " " << root_record->getDecName() << std::endl;
    return root_record;
  }

  std::shared_ptr<PixLib::DbRecord> DbWrapper::use(PixLib::dbRecordIterator &record_iter, bool read_only) {
    //      std::cout << "INFO [DbWrapper::use(PixLib::dbRecordIterator &)] will use" << static_cast<void *>(this) 
    //                << " db = " << static_cast<void *>(m_db.first) 
    //      	  << " (prior users=" << m_dbUsers <<")" << std::endl;
    
    // there should be at least one use which handed the reocrd iterator
    // otherwise it is a bug in the program
    assert (m_dbUsers!=0 && m_db.first && m_db.second.get());
    
    if (!read_only && !m_isWritable) {
      std::stringstream message;
      message << "FATAL [RootDbHelper::useForWriting] Database opened in READONLY mode cannot open database for for writing.";
      throw PixLib::PixDBException(message.str());
    }
    
    //      m_db.second=*( m_db.first->DbProcess(record_iter, PixLib::PixDb::DBREAD) );
    
    // the database should be open and there should be a root record
    //      assert(m_db.first && m_db.second);
    m_dbUsers++;
    
    PixLib::DbRecord *a_record=*(m_db.first->DbProcess(record_iter, PixLib::PixDb::DBREAD));
    std::shared_ptr<PixLib::DbRecord> temp;
    if (haveToCleanUp()) {
      temp=std::shared_ptr<PixLib::DbRecord>( a_record);
    }
    else {
      temp=std::shared_ptr<PixLib::DbRecord>( a_record, do_not_deleter<PixLib::DbRecord>);
    }
#   ifdef DEBUG
    keepRecord(temp);
#   endif
    return temp;
  }
  
#ifdef DEBUG
  void DbWrapper::keepRecord(const std::shared_ptr<PixLib::DbRecord> &record) {
    unsigned long addr = reinterpret_cast<unsigned long>(record.get());
    if (s_records.find(addr)!=s_records.end()) return;
    s_nRecordsCreated++;
    s_records.insert(std::make_pair(addr, record));
  }

  void DbWrapper::stat() {
    std::cout << "INFO [DbWrapper::stat] open = " << s_nFiles << "  open calls = " << s_nOpenCalls << " close calls = " << s_nCloseCalls << std::endl;
    unsigned int n_uniq=0;
    for (std::map<unsigned long, std::shared_ptr<PixLib::DbRecord> >::const_iterator iter=s_records.begin();
	 iter!=s_records.end();
	 iter++) {
      if (!iter->second.unique()) {
	std::cout << "INFO [DbWrapper::stat] record " << static_cast<void *>(iter->second.get()) << " still has " << iter->second.use_count() 
		  << " users. Should only have one." << std::endl;
      }
      else {
	n_uniq++;
      }
    }
    std::cout << "INFO [DbWrapper::stat]  " << s_nRecordsCreated << "  records created, " << n_uniq << " uniq records." << std::endl;
  }

  unsigned int DbWrapper::s_nFiles=0;
  unsigned int DbWrapper::s_nOpenCalls=0;
  unsigned int DbWrapper::s_nCloseCalls=0;
  unsigned int DbWrapper::s_nRecordsCreated=0;
  std::map< unsigned long , std::shared_ptr<PixLib::DbRecord> > DbWrapper::s_records;
#endif

}
