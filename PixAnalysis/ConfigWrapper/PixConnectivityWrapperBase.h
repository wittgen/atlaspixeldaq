#ifndef _PixConnectivityWrapperBase_h_
#define _PixConnectivityWrapperBase_h_

#include <sstream>
#include <string>
#include <map>
#include <iostream>
#include <cassert>

#include <PixConnectivity/PixConnectivity.h>
#include <PixConnectivity/RodBocConnectivity.h>
#include <PixConnectivity/OBConnectivity.h>
#include <PixConnectivity/Pp0Connectivity.h>
#include <PixConnectivity/ModuleConnectivity.h>
#include <PixConnectivity/PixDisable.h>

#include "Connectivity.h"
#include "ConfigHandle.h"

#include <memory>

namespace PixLib {
  class PixModule;
  class PixDisable;
}

namespace PixA {

  //  typedef std::shared_ptr<PixConnectivityConfigHandle> PixConnectivityConfigHandlePtr;

  class PixConnectivityWrapperBase : public IConnectivity
  {
    friend class ConnectivityManager;
  protected:

    PixConnectivityWrapperBase() : m_connectivity(NULL) {};

    PixConnectivityWrapperBase(PixLib::PixConnectivity *conn) : m_connectivity(conn) {};

    ~PixConnectivityWrapperBase();

  public:

    /** Return the list of crates (read only).
     */
    const PixA::CrateList crates() const  {
      return CrateList(m_connectivity->crates);
    }

    /** Return the list of crates.
     */
    PixA::CrateList crates()  {
      return CrateList(m_connectivity->crates);
    }

    /** Return the list of RODs of the given crate (read only).
     */
    const PixA::RodList rods(const PixA::CrateLocationBase &crate) const  {
      CrateList::CrateMap_t::const_iterator rod_iter = m_connectivity->crates.find(crate);
      if (rod_iter == m_connectivity->crates.end() || !rod_iter->second) {
	std::stringstream message;
	message << "ERROR [PixConnectivityWrapperBase::rods] No cate " << crate << ".";
	throw PixA::ConfigException(message.str());
      }
      return PixA::RodList(const_cast<PixLib::RodCrateConnectivity &>(*(rod_iter->second)));
    }

    /** Return the list of RODs of the given crate.
     */
    PixA::RodList rods(const PixA::CrateLocationBase &crate)  {
      CrateList::CrateMap_t::const_iterator rod_iter = m_connectivity->crates.find(crate);
      if (rod_iter == m_connectivity->crates.end() || !rod_iter->second) {
	std::stringstream message;
	message << "ERROR [PixConnectivityWrapperBase::rods] No cate " << crate << ".";
	throw PixA::ConfigException(message.str());
      }
      return PixA::RodList(*(rod_iter->second));
    }

    /** Get the number of Pp0s which are connected to a ROD.
     * @param rod_location the crate name and rod slot.
     */
    const Pp0List pp0s(const RodLocationBase &rod_location) const  {
      std::map<std::string, PixLib::RodBocConnectivity*>::const_iterator rod_iter = m_connectivity->rods.find(rod_location);
      if ( rod_iter == m_connectivity->rods.end() ) {
	std::stringstream message;
	message << "ERROR [PixConnectivityWrapperBase::pp0s] No rod " << rod_location << ".";

	throw PixA::ConfigException(message.str());
      }
      return Pp0List(*(rod_iter->second));
    }

    /** Get the number of modules which are connected to a Pp0.
     */
    const ModuleList modules(const Pp0LocationBase &pp0_location) const  {
      std::map<std::string, PixLib::Pp0Connectivity*>::const_iterator pp0_iter = m_connectivity->pp0s.find(pp0_location);
      if ( pp0_iter == m_connectivity->pp0s.end() ) {
	std::stringstream message;
	message << "ERROR [PixConnectivityWrapperBase::modules] No pp0 " << pp0_location << ".";
	throw PixA::ConfigException(message.str());
      }
      return ModuleList(*(pp0_iter->second));
    }

    /** Get the number of modules which are connected to a Pp0.
     */
    ModuleList modules(Pp0List::iterator &iter) {
      return ModuleList(const_cast<PixLib::Pp0Connectivity &>(**iter));
    }

    /** Get the number of modules which are connected to a Pp0.
     */
    const ModuleList modules(const Pp0List::const_iterator &iter) const {
      return ModuleList(const_cast<PixLib::Pp0Connectivity &>(**iter));
    }

    /** Find the connectivity object of the given module.
     */
    const PixLib::ModuleConnectivity *findModule(const ModuleLocationBase &module_location) const;

    /** Find the connectivity object of the given pp0.
     */
    const PixLib::Pp0Connectivity *findPp0(const Pp0LocationBase &pp0_location) const;

    /** Find the connectivity object of the given ROD.
     */
    const PixLib::RodBocConnectivity *findRod(const RodLocationBase &rod_location) const;

    /** Find the connectivity object of the given Crate.
     */
    const PixLib::RodCrateConnectivity *findCrate(const CrateLocationBase &crate_location) const;

    void writeDisable(PixLib::PixDisable &disable);

    std::string tag(EConnTag tag) const {
      switch (tag) {
      case kId:
	return m_connectivity->getConnTagOI();
      case kIdCFG: {
	std::string object_cfg_tag( m_connectivity->getConnTagOI() );
	object_cfg_tag += "-CFG";
	return object_cfg_tag;
      }
      case kConn:
	return m_connectivity->getConnTagC();
      case kAlias:
	return m_connectivity->getConnTagA();
      case kPayload:
	return m_connectivity->getConnTagP();
      case kConfig:
	return m_connectivity->getCfgTag();
      case kModConfig:
	return m_connectivity->getModCfgTag();
      }
      return std::string();
    }

  static void getObjectTags(std::vector<std::string> *object_tags);

  static void getConfigurationObjectTags(std::vector<std::string> *object_tag_out);

  static void getConnectivityTags(const std::string &id_name,
				  std::vector<std::string> *conn_tag_out,
				  std::vector<std::string> *alias_tag_out,
				  std::vector<std::string> *payload_tag_out,
				  std::vector<std::string> *configuration_tag_out);


  protected:
    enum EConfigType {kRodConf, kBocConf, kModuleConf, kNConfTypes};

  protected:
    PixLib::ModuleConnectivity *moduleConn(const ModuleLocationBase &module_location) const {
      std::map<std::string, PixLib::ModuleConnectivity*>::const_iterator mod_iter = m_connectivity->mods.find(module_location);
      if ( mod_iter == m_connectivity->mods.end() ) {
	std::stringstream message;
	message << "ERROR [PixConnectivityWrapperBase::modConf] No module " << module_location << ".";
	throw PixA::ConfigException(message.str());
      }
      return mod_iter->second;
    }

    const PixLib::RodBocConnectivity *rodBocConnectivity(const Pp0LocationBase &pp0_location) const {
      std::map<std::string, PixLib::Pp0Connectivity*>::const_iterator pp0_iter = m_connectivity->pp0s.find(pp0_location);
      if ( pp0_iter == m_connectivity->pp0s.end() ) {
	std::stringstream message;
	message << "ERROR [PixConnectivityWrapperBase::rodBocConnectivity] No pp0 " << pp0_location << ".";
	throw PixA::ConfigException(message.str());
      }
      return rodBocConnectivity(pp0_iter->second);
    }

    PixLib::RodBocConnectivity *rodBocConnectivity(Pp0List::iterator &pp0_iter) {
      return const_cast<PixLib::RodBocConnectivity *>(rodBocConnectivity(*pp0_iter));
    }

    const PixLib::RodBocConnectivity *rodBocConnectivity(const Pp0List::const_iterator &pp0_iter) const {
      return rodBocConnectivity(const_cast<PixLib::Pp0Connectivity *>(*pp0_iter));
    }

    const PixLib::RodBocConnectivity *rodBocConnectivity(const PixLib::Pp0Connectivity *pp0_conn) const {
      assert(const_cast<PixLib::Pp0Connectivity *>(pp0_conn)->ob() && const_cast<PixLib::Pp0Connectivity *>(pp0_conn)->ob()->rodBoc());
      PixLib::RodBocConnectivity *rod_boc_conn(const_cast<PixLib::Pp0Connectivity *>(pp0_conn)->ob()->rodBoc());
      assert(rod_boc_conn);
      return rod_boc_conn;
    }

    PixLib::PixConnectivity *m_connectivity;
  };

}

#endif
