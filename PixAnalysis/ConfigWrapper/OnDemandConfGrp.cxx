#include "OnDemandConfGrp.h"

namespace PixA {

// template specialisations
// storage
  template <> inline bool           &OnDemandConfGrp::store<bool>(const bool &val)
  {////std::cout<<"vediamo se sei qui A"<<std::endl;
      return *reinterpret_cast<bool *>(m_confGroupContainer->m_ucharStorage.store(val));}

  template <> inline int            &OnDemandConfGrp::store<int>(const int &val)
  {////std::cout<<"vediamo se sei qui B"<<std::endl;
      return *(m_confGroupContainer->m_intStorage.store(val));}

  template <> inline unsigned int   &OnDemandConfGrp::store<unsigned int>(const unsigned int &val)
  {//std::cout<<"vediamo se sei qui C"<<std::endl;
      return *reinterpret_cast<unsigned int *>(m_confGroupContainer->m_intStorage.store(static_cast<const int &>(val))); }

  template <> inline short          &OnDemandConfGrp::store<short>(const short &val)
  {//std::cout<<"vediamo se sei qui D"<<std::endl;
      return *reinterpret_cast<short *>(m_confGroupContainer->m_intStorage.store(static_cast<const int &>(val))); }

  template <> inline unsigned short &OnDemandConfGrp::store<unsigned short>(const unsigned short &val) 
  {//std::cout<<"vediamo se sei qui E"<<std::endl;
      return *reinterpret_cast<unsigned short *>(m_confGroupContainer->m_intStorage.store(static_cast<const int &>(val))); }

  template <> inline char           &OnDemandConfGrp::store<char>(const char &val)
  {//std::cout<<"vediamo se sei qui F"<<std::endl;
      return *reinterpret_cast<char *>(m_confGroupContainer->m_intStorage.store(static_cast<const int &>(val))); }

  template <> inline unsigned char  &OnDemandConfGrp::store<unsigned char>(const unsigned char &val)
  {//std::cout<<"vediamo se sei qui G"<<std::endl;
      return *reinterpret_cast<unsigned char *>(m_confGroupContainer->m_ucharStorage.store(val)); }

  template <> inline float          &OnDemandConfGrp::store<float>(const float &val)
  {//std::cout<<"vediamo se sei qui H"<<std::endl;
      return *(m_confGroupContainer->m_floatStorage.store(val)); }

  template <> inline std::string    &OnDemandConfGrp::store<std::string>(const std::string &val)
  {//std::cout<<"vediamo se sei qui II"<<std::endl;
      return *(m_confGroupContainer->m_stringStorage.store(val));}

  template <> inline bool           &OnDemandConfGrp::storage<bool>()          const
   {//std::cout<<"vediamo se sei qui J"<<std::endl;
       return *reinterpret_cast<bool *>(m_confGroupContainer->m_ucharStorage.alloc()); }

  template <> inline int            &OnDemandConfGrp::storage<int>()           const
  {//std::cout<<"vediamo se sei qui K"<<std::endl;
      return *m_confGroupContainer->m_intStorage.alloc();}

  template <> inline unsigned int   &OnDemandConfGrp::storage<unsigned int>()  const
  {//std::cout<<"vediamo se sei qui L"<<std::endl;
      return *reinterpret_cast<unsigned int *>(m_confGroupContainer->m_intStorage.alloc());}

  template <> inline short          &OnDemandConfGrp::storage<short>()         const
  {//std::cout<<"vediamo se sei qui M"<<std::endl;
      return *reinterpret_cast<short *>(m_confGroupContainer->m_intStorage.alloc());}

  template <> inline unsigned short &OnDemandConfGrp::storage<unsigned short>()const
  {//std::cout<<"vediamo se sei qui N"<<std::endl;
      return *reinterpret_cast<unsigned short *>(m_confGroupContainer->m_intStorage.alloc());}

  template <> inline char           &OnDemandConfGrp::storage<char>()          const
  {//std::cout<<"vediamo se sei qui O"<<std::endl;
      return *reinterpret_cast<char *>(m_confGroupContainer->m_intStorage.alloc());}

  template <> inline unsigned char  &OnDemandConfGrp::storage<unsigned char>() const
  {//std::cout<<"vediamo se sei qui P"<<std::endl;
      return *reinterpret_cast<unsigned char *>(m_confGroupContainer->m_intStorage.alloc());}

  template <> inline float          &OnDemandConfGrp::storage<float>()         const
  {//std::cout<<"vediamo se sei qui Q"<<std::endl;
      return *(m_confGroupContainer->m_floatStorage.alloc()); }

  template <> inline std::string    &OnDemandConfGrp::storage<std::string>()   const
  {//std::cout<<"vediamo se sei qui R"<<std::endl;
      return *(m_confGroupContainer->m_stringStorage.alloc()); }

  template <> inline std::vector<float>   &OnDemandConfGrp::storage<std::vector<float> >() const
  {//std::cout<<"vediamo se sei qui S"<<std::endl;
      return *(m_confGroupContainer->m_floatVectorStorage.alloc()); }

  template <> inline std::vector<unsigned int>   &OnDemandConfGrp::storage<std::vector<unsigned int> >() const
  {//std::cout<<"vediamo se sei qui T"<<std::endl;
      return *(m_confGroupContainer->m_uintVectorStorage.alloc()); }

  template <> inline std::vector<int>   &OnDemandConfGrp::storage<std::vector<int> >() const
  {//std::cout<<"vediamo se sei qui U"<<std::endl;
      return *(m_confGroupContainer->m_intVectorStorage.alloc()); }

  template <> void OnDemandConfGrp::returnStorage<std::vector<int> >(std::vector<int> &unused_elm) const
  {//std::cout<<"vediamo se sei qui V"<<std::endl;
      m_confGroupContainer->m_intVectorStorage.giveBack(&unused_elm); }

  template <> inline std::vector<bool>   &OnDemandConfGrp::storage<std::vector<bool> >() const
  {//std::cout<<"vediamo se sei qui W"<<std::endl;
      return *(m_confGroupContainer->m_boolVectorStorage.alloc()); }

  template <> void OnDemandConfGrp::returnStorage<std::vector<bool> >(std::vector<bool> &unused_elm) const
  {//std::cout<<"vediamo se sei qui X"<<std::endl;
      m_confGroupContainer->m_boolVectorStorage.giveBack(&unused_elm); }

  template <> inline PixLib::ConfMask<bool>   &OnDemandConfGrp::storage<PixLib::ConfMask<bool> >() const
  {//std::cout<<"vediamo se sei qui Y"<<std::endl;
      PixelMask<bool> *a=m_confGroupContainer->m_boolMatrixStorage.alloc(); return *a;}

  template <> inline PixLib::ConfMask<PixLib::TrimVal_t>   &OnDemandConfGrp::storage<PixLib::ConfMask<PixLib::TrimVal_t> >() const
  {//std::cout<<"vediamo se sei qui Z"<<std::endl;
      PixelMask<PixLib::TrimVal_t> *a=m_confGroupContainer->m_ushortMatrixStorage.alloc(); return *a;}

// template specialisations 
// creation of ConfObj

template <> inline PixLib::ConfObj *OnDemandConfGrp::createConfObj<int>(const std::string &name, int &a) const {
  //std::cout<<"vediamo se sei qui A1"<<std::endl;
    return new PixLib::ConfInt(name, a,a,"",false);
}

template <> inline PixLib::ConfObj *OnDemandConfGrp::createConfObj<unsigned int>(const std::string &name, unsigned int &a) const {
  //std::cout<<"vediamo se sei qui B1"<<std::endl;
    return new PixLib::ConfInt(name, a,a,"",false);
}

template <> inline PixLib::ConfObj *OnDemandConfGrp::createConfObj<short>(const std::string &name, short &a) const {
  //std::cout<<"vediamo se sei qui C1"<<std::endl;
    return new PixLib::ConfInt(name, a,a,"",false);
}

 template <> inline PixLib::ConfObj *OnDemandConfGrp::createConfObj<unsigned short>(const std::string &name, unsigned short &a) const {
  //std::cout<<"vediamo se sei qui D1"<<std::endl;
     return new PixLib::ConfInt(name, a,a,"",false);
}

 template <> inline PixLib::ConfObj *OnDemandConfGrp::createConfObj<char>(const std::string &name, char &a) const {
  //std::cout<<"vediamo se sei qui E1"<<std::endl;
     return new PixLib::ConfInt(name, a,a,"",false);
}

 template <> inline PixLib::ConfObj *OnDemandConfGrp::createConfObj<unsigned char>(const std::string &name, unsigned char &a) const {
  //std::cout<<"vediamo se sei qui F1"<<std::endl;
     return new PixLib::ConfInt(name, a,a,"",false);
}

 template <> inline PixLib::ConfObj *OnDemandConfGrp::createConfObj<float>(const std::string &name, float &a) const {
  //std::cout<<"vediamo se sei qui G1"<<std::endl;
     return new PixLib::ConfFloat(name, a,a,"",false);
}

 template <> inline PixLib::ConfObj *OnDemandConfGrp::createConfObj<bool>(const std::string &name, bool &a) const {
  //std::cout<<"vediamo se sei qui H1"<<std::endl;
     return new PixLib::ConfBool(name, a,a,"",false);
}

 template <> inline PixLib::ConfObj *OnDemandConfGrp::createConfObj<std::string>(const std::string &name, std::string &a) const {
  //std::cout<<"vediamo se sei qui II 1"<<std::endl;
     return new PixLib::ConfString(name, a,a,"",false);
}

 template <> inline PixLib::ConfObj *OnDemandConfGrp::createConfObj<std::vector<float> >(const std::string &name, std::vector<float> &a) const {
  //std::cout<<"vediamo se sei qui J1"<<std::endl;
     return new PixLib::ConfVector(name, a,std::vector<float>(),"",false);
}


 template <> inline PixLib::ConfObj *OnDemandConfGrp::createConfObj<std::vector<int> >(const std::string &name, std::vector<int> &a) const {
     //std::cout<<"vediamo se sei qui K1"<<std::endl;
   if (a.size() == 18 * 160 ) {
     if (sizeof(PixLib::TrimVal_t) == sizeof(int)) {
       // is this really a matrix ?
       return createConfMatrix< PixLib::TrimVal_t > (name,a);
     }
     else {
       // is this really a matrix ?
       return createConfMatrix< PixLib::TrimVal_t, int > (name,a);
     }
   }
   else if (a.size() == 18 * 160 / 4 ) {
     // is this really a matrix ?
     return createConfMatrixUnpack< PixLib::TrimVal_t, int > (name,a);
   }
   else {
     // is this really an int vector ?
     return new PixLib::ConfVector(name, a,std::vector<int>(),"",false);
   }
 }

 template <> inline PixLib::ConfObj *OnDemandConfGrp::createConfObj<std::vector<bool> >(const std::string &name, std::vector<bool> &a) const {
     //std::cout<<"vediamo se sei qui L1"<<std::endl;
   return createConfMatrix< bool, bool> (name,a);
 }

  // template <> inline PixLib::ConfObj *OnDemandConfGrp::createConfObj< std::vector<unsigned int> >(const std::string &name, std::vector<unsigned int> &a) const {
  //  return new PixLib::ConfVector(name, a,std::vector<unsigned int>(),"",false);
  //}

  //  template <class T> T defVal();

  template <> inline bool PixelMask<bool>::maxVal() {return true;}
  template <> inline bool PixelMask<bool>::defVal() {return false;}

  //  template <> inline unsigned short maxVal<unsigned short>() {return USHRT_MAX;}
  // trim values:
  template <> inline unsigned short PixelMask<PixLib::TrimVal_t>::maxVal() {return 0xff;}
  template <> inline unsigned short PixelMask<PixLib::TrimVal_t>::defVal() {return 0;}

//   template <class T> unsigned int bits();

//   template <> unsigned int bits<bool>() {
//     return 1;
//   }

//   template <> unsigned int bits<unsigned short>() {
//     return 8;
//   }


  template <class T> PixLib::ConfObj *OnDemandConfGrp::createConfMatrix(const std::string &name, std::vector<T> &a) const {
      //std::cout<<"vediamo se sei qui M1"<<std::endl;
    assert( a.size()==18*160 );
    PixLib::ConfMask<T> &a_mask = storage<PixLib::ConfMask<T> >();// (18,160, maxVal<T>(),defVal<T>());
    a_mask.set(const_cast< std::vector<T> &>(a) );

    return new PixLib::ConfMatrix(name, a_mask ,PixelMask<T>::maxVal(),"",false);
  }

  template <class T, class T2> PixLib::ConfObj *OnDemandConfGrp::createConfMatrix(const std::string &name, std::vector<T2> &a) const {
      //std::cout<<"vediamo se sei qui N1"<<std::endl;
    assert( a.size()==18*160 );
    PixLib::ConfMask<T> &a_mask = storage<PixLib::ConfMask<T> >();// (18,160, maxVal<T>(),defVal<T>());

    std::vector<T> temp_vec;
    temp_vec.reserve(a.size());
    for (typename std::vector<T2>::iterator src_iter = a.begin();
	 src_iter != a.end();
	 src_iter++) {

      temp_vec.push_back(*src_iter);

    }

    a_mask.set(temp_vec);
    return new PixLib::ConfMatrix(name, a_mask ,PixelMask<T>::defVal(),"",false);
  }

  template <class T, class T2> PixLib::ConfObj *OnDemandConfGrp::createConfMatrixUnpack(const std::string &name, std::vector<T2> &a) const {
      //std::cout<<"vediamo se sei qui O1"<<std::endl;
    assert( a.size()==18*160/4 );
    PixLib::ConfMask<T> &a_mask = storage<PixLib::ConfMask<T> >();// (18,160, maxVal<T>(),defVal<T>());
    std::vector<T> temp_vec;
    temp_vec.reserve(a.size()*4);
    for (typename std::vector<T2>::iterator src_iter = a.begin();
	 src_iter != a.end();
	 src_iter++) {

      unsigned int value = *src_iter;
      for (int shift_i=0; shift_i<4; shift_i++) {
	temp_vec.push_back( value & 0xff );
	value >>=  8;
      }

    }
    a_mask.set(temp_vec);
    return new PixLib::ConfMatrix(name, a_mask ,PixelMask<T>::defVal(),"",false);
  }


template  <class T> inline PixLib::ConfObj &OnDemandConfGrp::get(const std::string &obj_name, const PixLib::dbFieldIterator &field) const
{//std::cout<<"vediamo se sei qui P1"<<std::endl;
  T &a=storage<T>();
  PixLib::dbFieldIterator fi(db()->DbProcess(field,PixLib::PixDb::DBREAD,a));
  // to release or not to release ? : 
  fi.releaseMem();
  return *createConfObj(obj_name,a);
}

// template  <class T> inline PixLib::ConfObj &OnDemandConfGrp::getMatrix(const std::string &obj_name, const PixLib::dbFieldIterator &field) const
// {
//   PixLib::ConfMask<T> &a=storage<PixLib::ConfMask<T> >();
//   PixLib::dbFieldIterator fi(db()->DbProcess(field,PixLib::PixDb::DBREAD,a));
//   // to release or not to release ? : 
//   fi.releaseMem();
//   return *createConfObj(obj_name,a);
// }


  void OnDemandConfGrp::createAll() {
      //std::cout<<"vediamo se sei qui Q1"<<std::endl;
    if (m_confGroupContainer->m_isComplete) return;

    try {
    bool was_empty=m_confGroupContainer->m_obj.empty();

    //    std::shared_ptr<PixLib::DbField> the_field_auto;
    for ( PixLib::dbFieldIterator field_iter = fieldBegin();
	  field_iter != fieldEnd();
	  field_iter++ ) {

      PixLib::DbField *the_field(*(db()->DbProcess(field_iter,PixLib::PixDb::DBREAD))) ;
      //      if (haveToCleanUp()) {
      //	the_field_auto = std::shared_ptr<PixLib::DbField>(the_field);
      //      }

      std::string field_name(the_field->getName());
      std::string::size_type pos = field_name.find("_");
      if (pos != std::string::npos && pos<field_name.size() && field_name.compare(0,pos, m_confGroupContainer->m_confGroup->name())==0) {
	field_name.erase(0,pos+1);
	if (was_empty || m_confGroupContainer->m_obj.find(field_name) == m_confGroupContainer->m_obj.end()) {
	  //	  std::cout << "INFO [OnDemandConfGrp::createAll] create field \"" << field_name << "\" for group \"" <<  m_confGroupContainer->m_confGroup->m_groupName << "\"." << std::endl;
	  std::pair<ConfObjList_t::iterator,bool> ret;
	  try {
 //         std::cout<<"io lo chiamo 1"<<std::endl;
	    ret=m_confGroupContainer->m_obj.insert(std::make_pair(field_name,&getConfObj(field_name,
								   the_field,
													   field_iter)));
	  }
	  catch (ConfigException &err) {
	    std::cerr << "ERROR [OnDemandConfGrp::createAll] Caught exception : " << err << std::endl;
	    continue;
	  }

	  if (!ret.second) {
	    std::stringstream message;
	    message << "FATAL [OnDemandConfGrp::operator[]] Failed to insert the new config group of name " << field_name
		    << " of record " << pathName() << " into the cache."  << std::endl;
	    throw ConfigException(message.str());
	  }
        //std::cout<<"vediamo se sei qui 1"<<std::endl;
	  PixLib::ConfObj *temp_obj = ret.first->second;
        //std::cout<<"vediamo se sei qui 2"<<std::endl;
	  temp_obj->m_name=m_confGroupContainer->m_confGroup->name() + "_" + temp_obj->m_name;
        //std::cout<<"vediamo se sei qui 3"<<std::endl;
	  m_confGroupContainer->m_confGroup->push_back(temp_obj);
        //std::cout<<"vediamo se sei qui 4"<<std::endl;
	}
      }
    }
        //std::cout<<"vediamo se sei qui 5"<<std::endl;
    m_confGroupContainer->m_isComplete=true;
        //std::cout<<"vediamo se sei qui 6"<<std::endl;
    }
    catch(PixLib::PixDBException &err) {
      std::stringstream message;
      message << "FATAL [OnDemandConfGrp::createAll] Failed to load full ConfGroup : "
	      << err.message;
      throw ConfigException(message.str());
    }
  }

  PixLib::ConfObj &OnDemandConfGrp::operator[](const std::string &name)   {
      //std::cout<<"vediamo se sei qui R1"<<std::endl;
    ConfObjList_t::const_iterator obj_iter = m_confGroupContainer->m_obj.find(name);
    PixLib::ConfObj *temp_obj;
    if (obj_iter == m_confGroupContainer->m_obj.end()) {
      std::string temp;
      if (!m_groupName.empty()) {
	temp+=m_groupName;
	temp+="_";
      }
      temp += name;
      try {
      PixLib::dbFieldIterator field = findField( temp );

      updateConfGroupName();
      if (field == fieldEnd()) {
	std::vector<std::string> field_list;
	fillListOfFields(field_list);
	std::stringstream message;
	message << "FATAL [OnDemandConfGrp::operator[]] No ConfObj of name = " << name
		<< " for group " << m_groupName << " in record " << m_record->getName() << ". The following fields are available : ";
	for (std::vector<std::string>::const_iterator iter=field_list.begin();
	     iter!=field_list.end();
	     iter++) {
	  if (iter!=field_list.begin()) message << ", ";
	  message << *iter;
	}
	message << "."  << std::endl;
	throw ConfigException(message.str());
      }

      PixLib::DbField *the_field =  *(db()->DbProcess(field,PixLib::PixDb::DBREAD)) ;
 //         std::cout<<"io lo chiamo 2"<<std::endl;
      std::pair<ConfObjList_t::iterator,bool> ret=m_confGroupContainer->m_obj.insert(std::make_pair(name, &getConfObj(name,the_field,field)));
      if (!ret.second) {
	std::stringstream message;
	message << "FATAL [OnDemandConfGrp::operator[]] Failed to insert the new config group of name " << name
		<< " of record " << pathName() << " into the cache."  << std::endl;
	throw ConfigException(message.str());
      }
      temp_obj = ret.first->second;
      temp_obj->m_name=m_confGroupContainer->m_confGroup->name() + "_" + temp_obj->m_name;
      //      temp_obj->m_name += "_";
      //temp_obj->m_name += name;
      m_confGroupContainer->m_confGroup->push_back(temp_obj);
      }
      catch(PixLib::PixDBException &err) {
	std::stringstream message;
	message << "FATAL [OnDemandConfGrp::createAll] Failed to retrive ConfObj " << name << " : "
		<< err.message;
	throw ConfigException(message.str());
      }

    }
    else {
      temp_obj = obj_iter->second;
    }

    //    std::cout << "INFO [OnDemandConfGrp::operator[]] got ";
    //    temp->dump(std::cout);
    //    std::cout << std::endl;
    return *temp_obj;
  }

  PixLib::ConfObj &OnDemandConfGrp::getConfObj(const std::string &name, PixLib::DbField *a_field, const  PixLib::dbFieldIterator &field) const 
  {//std::cout<<"vediamo se sei qui S1"<<std::endl;
    //    std::shared_ptr<PixLib::DbField> the_field ( *(db()->DbProcess(field,PixLib::PixDb::DBREAD)) );
    std::shared_ptr<PixLib::DbField> the_field_auto;

    PixLib::DbField *the_field( a_field );
    if (haveToCleanUp()) {
      the_field_auto = std::shared_ptr<PixLib::DbField>(the_field);
    }

    switch ( (the_field)->getDataType()) {
    case PixLib::PixDb::DBBOOL:
      return get<bool>(name, field);
    case PixLib::PixDb::DBVECTORBOOL: {
      //      std::stringstream message;
      //      // 18 * 160 entries -> matrix ?
      //      message << "FATAL [OnDemandConfGroup::add] No support for vectors of bools. Vectors of bools are used for matrices. Matrices cannot be handled (name = " 
      //	      << (*field)->getName()
      //	      << " in record " << m_record->getName() << ")."  << std::endl;
      //      throw std::runtime_error(message.str());
      PixLib::ConfObj &a=get< std::vector<bool> >(name, field);

      // this should always be a matrix 
      // there should not be an ambiguity
      if  ( a.type() != PixLib::ConfObj::MATRIX ) {
	std::cout << "WARNING [OnDemandConfGroup::add] Expected from fields of type DBVECTORBOOL a matrix ( field " << name
		  << " of record " << m_record->getName() << ")."  << std::endl;
      }
      //      if (a.type()== PixLib::ConfObj::MATRIX) {
      //	std::cout << "INFO [OnDemandConfGroup::add] Created a matrix from field " << name
      //		  << " of record " << m_record->getName() << "."  << std::endl;
      //	a.dump(std::cout);
      //      }

      return a;
    }
    case PixLib::PixDb::DBINT:
      return get<int>(name, field);
    case PixLib::PixDb::DBVECTORINT: {
      // 18 * 160 entries -> matrix ?
      // std::cout << "WARNING [OnDemandConfGroup::add] vector of ints could be vectors or matrices (field " << (*field)->getName()
      //		<< " of record " << m_record->getName() << ")."  << std::endl;
      PixLib::ConfObj &a=get<std::vector<int> >(name, field);
      if (a.type()==PixLib::ConfObj::MATRIX) {
	if (name!="FDAC" && name != "TDAC") {
	  std::cout << "WARNING [OnDemandConfGroup::add] Ambiguous field type. Created a matrix from field " << name
		    << " of record " << m_record->getName() << "."  << std::endl;
	}
	//a.dump(std::cout);
      }
      else if (a.type()==PixLib::ConfObj::VECTOR) {
	std::cout << "WARNING [OnDemandConfGroup::add] Ambiguous field type. Created an int vector from field " << name
		  << " of record " << m_record->getName() << "."  << std::endl;
      }
      else {
	std::cout << "WARNING [OnDemandConfGroup::add] Ambiguous field type. Created a object of type " << a.type() << " from field " << name
		  << " of record " << m_record->getName() << "."  << std::endl;
      }
      return a;
    }
    case PixLib::PixDb::DBULINT:
      return get<unsigned int>(name, field);
    case PixLib::PixDb::DBFLOAT:
      return get<float >(name, field);
    case PixLib::PixDb::DBVECTORFLOAT:
      return get<std::vector<float> >(name, field);
    case PixLib::PixDb::DBDOUBLE: {
      std::stringstream message;
      message << "FATAL [OnDemandConfGroup::add] No suppprt for doubles (fields " << name
	      << " in record " << m_record->getName() << "). Doubles are not expected in configuration files."  << std::endl;
      throw ConfigException(message.str());
    }
    case PixLib::PixDb::DBVECTORDOUBLE: {
      std::stringstream message;
      message << "FATAL [OnDemandConfGroup::add] No suppprt for vectors of doubles ( field " << name
	      << " in record " << m_record->getName() << "). Vectors of doubles are not expected in configuration files."  << std::endl;
      throw ConfigException(message.str());
    }
    case PixLib::PixDb::DBHISTO: {
      std::stringstream message;
      message << "FATAL [OnDemandConfGroup::add] No suppprt for histograms ( field " << name
	      << " in record " << m_record->getName() << "). Histograms are not expected in configuration files."  << std::endl;
      throw ConfigException(message.str());
    }
    case PixLib::PixDb::DBSTRING:
      return get<std::string>(name, field);
    default: {
      std::stringstream message;
      message << "FATAL [OnDemandConfGroup::add] invalid type for name = " << name
	      << " in record " << m_record->getName() << "."  << std::endl;
      throw ConfigException(message.str());
      //    case DBEMPTY:
    }
    }
  }

  ObjStat_t OnDemandConfGrp::s_stat("OnDemandConfGrp");

}
