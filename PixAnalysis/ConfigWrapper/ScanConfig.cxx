#include "ScanConfig.h"
#include "ConfObjUtil.h"

namespace PixA {

  int ScanConfig::s_dtoLinkMap[2]={1,2};

  int ScanConfig::bocLinkRx(ERxLink link) const     {
      assert(link<=kDTO2); 
      if (m_conn && hasModConfig()) {

	return const_cast<PixLib::ModuleConnectivity *>(*m_modConnIter)->bocLinkRx(s_dtoLinkMap[link]);
      }
      else {
	if (!hasModConfig()) {
	  throw PixA::ConfigException("FATAL [ScanConfig::bocLinkRx] Neiter are there RX link information from connectivity database, "
				      "nor is there a module configuration." );
	}
	const PixA::ConfGrpRef &general_config = modConfig()["general"];
	return confVal<int>(( link==kDTO1 ? general_config["BocOutputLink1"] : general_config["BocOutputLink2"]));
      }
  }

  int ScanConfig::inOrderBocLinkRx(ERxLink link) const     {

    bool altlink=false;

    if (hasPixScanConfig()) {
      try {
	ConfGrpRef general_config = pixScanConfig()["general"];
	altlink = confVal<unsigned int>(general_config["useAltModLinks"]);
      }
      catch (...){
	std::cout <<"INFO [ScanConfig::inOrderBocLinkRx] useAltModLinks not saved. Assuming there is no swap"<< std::endl;
      }
    }

    if (altlink) {
      return bocLinkRx((link==kDTO2 ? kDTO2 : kDTO1));
    }
    else {
      return bocLinkRx(link);
    }
  }
}

