#include "test_compareConfObj.h"
#include <Config/ConfObj.h>
#include <Config/ConfGroup.h>
#include <Config/Config.h>

#ifndef TrimVal_t_h
#define TrimVal_t_h

namespace PixLib {
  typedef unsigned short TrimVal_t;
}

#endif

using namespace PixLib;

std::string stripGroupName(const std::string &full_name) {
  std::string::size_type pos=full_name.find("_");
  if (pos!=std::string::npos) {
    return std::string(full_name,pos+1,full_name.size()-pos-1);
  }
  return full_name;
}

//template <class T> bool operator==(PixLib::ConfObj &a, PixLib::ConfObj &b);

template <class T> bool compare(PixLib::ConfObj &a, PixLib::ConfObj &b);

template<> inline bool compare<int>(PixLib::ConfObj &a, PixLib::ConfObj &b) {
  ConfInt &a_int=reinterpret_cast<ConfInt&>(a);
  ConfInt &b_int=reinterpret_cast<ConfInt&>(b);
  if (a_int.subtype() != b_int.subtype()) {
    std::cout << "INFO [compare<int>(PixLib::ConfObj &a, PixLib::ConfObj &b)] sub types differ for \"" << a_int.name() << "\" / " << a_int.type()
	      << " : " << a_int.subtype() << " != " << b_int.subtype() << std::endl;
  }
  if (a_int.value() != b_int.value()) {
    std::cout << "INFO [compare<int>(PixLib::ConfObj &a, PixLib::ConfObj &b)] values differ for \"" << a_int.name() << "\" / " << a_int.type()
	      << " : " << a_int.value() << " != " << b_int.value() << std::endl;
  }
  return ( a_int.value() == b_int.value() && a_int.subtype() == b_int.subtype());
}


template<> inline bool compare<float>(PixLib::ConfObj &a, PixLib::ConfObj &b) {
  ConfFloat &a_float=reinterpret_cast<ConfFloat&>(a);
  ConfFloat &b_float=reinterpret_cast<ConfFloat&>(b);
  return ( a_float.value() == b_float.value());
}


template<> inline bool compare<std::string>(PixLib::ConfObj &a, PixLib::ConfObj &b) {
  ConfString &a_string=reinterpret_cast<ConfString&>(a);
  ConfString &b_string=reinterpret_cast<ConfString&>(b);
  return ( a_string.value() == b_string.value());
}

template<> inline bool compare<bool>(PixLib::ConfObj &a, PixLib::ConfObj &b) {
  ConfBool &a_bool=reinterpret_cast<ConfBool&>(a);
  ConfBool &b_bool=reinterpret_cast<ConfBool&>(b);
  return ( a_bool.value() == b_bool.value());
}

bool compareVector(PixLib::ConfObj &a, PixLib::ConfObj &b) {
  ConfVector &a_vector=reinterpret_cast<ConfVector&>(a);
  ConfVector &b_vector=reinterpret_cast<ConfVector&>(b);

  bool ret=true;
  if (a_vector.subtype() != b_vector.subtype()) {
    std::cout << "INFO [compareVector(PixLib::ConfObj &a, PixLib::ConfObj &b)] sub types differ for \"" << a_vector.name() << "\" / " << a_vector.type()
	      << " : " << a_vector.subtype() << " != " << b_vector.subtype() << std::endl;
    ret &=false;
  }

  switch (a_vector.subtype()) {
  case PixLib::ConfVector::V_INT: {
    ret &= (a_vector.valueVInt() == b_vector.valueVInt());
    break;
  }
  case PixLib::ConfVector::V_UINT: {
    ret &= (a_vector.valueVUint() == b_vector.valueVUint());
    break;
  }
  case PixLib::ConfVector::V_FLOAT: {
    ret &= (a_vector.valueVFloat() == b_vector.valueVFloat());
    break;
  }
  default: {
    return false;
  }
  }
  return ret;
  //  return false;
}

bool compareStringVector(PixLib::ConfObj &a, PixLib::ConfObj &b) {
  PixLib::ConfStrVect &a_vector=reinterpret_cast<PixLib::ConfStrVect&>(a);
  PixLib::ConfStrVect &b_vector=reinterpret_cast<PixLib::ConfStrVect&>(b);
  return a_vector.value() == b_vector.value();
}


template <class T> bool operator==(const std::vector<std::vector<T> > &a, const std::vector<std::vector<T> > &b)
{
  
  if  ( a.size() != b.size() ) {
    std::cout << "ERROR [operator==(const std::vector<std::vector<T> > &a, const std::vector<std::vector<T> > &b)] sizes differ : "
	      << a.size() << " != " << b.size() << std::endl;
    return false;
  }
  typename std::vector< std::vector<T> >::const_iterator b_iter = b.begin();
  bool ret=true;
  unsigned int col_i=0;
  for (typename std::vector< std::vector<T> >::const_iterator a_iter = a.begin();
       a_iter != a.end();
       a_iter++, b_iter++,col_i++) {
      assert ( b_iter != b.end() );
      //      assert (a_iter->size() == b_iter->size());
      if  ( a_iter->size() != b_iter->size() ) {
	std::cout << "ERROR [operator==(const std::vector<std::vector<T> > &a, const std::vector<std::vector<T> > &b)] sizes differ in column "
		  << col_i << " / " << a.size() << " : " 
		  << a_iter->size() << " != " << b_iter->size() << std::endl;
	ret &=false;
	break;
      }
      typename std::vector<T>::const_iterator b_b_iter = b_iter->begin();
      unsigned int row_i=0;
      for (typename std::vector<T>::const_iterator a_a_iter = a_iter->begin();
	   a_a_iter != a_iter->end();
	   a_a_iter++, b_b_iter++,row_i++) {

	assert ( b_b_iter != b_iter->end() );
	if (*a_a_iter != *b_b_iter) {
	  std::cout << "ERROR [operator==(const std::vector<std::vector<T> > &a, const std::vector<std::vector<T> > &b)] elements differ in column "
		    << col_i << " row " << row_i << " : " 
		    << *a_a_iter << " != " << *b_b_iter << std::endl;
	  ret &=false;
	  break;
	}
      }
  }
  return ret;
}

bool compareMatrix(PixLib::ConfObj &a, PixLib::ConfObj &b) {
  ConfMatrix &a_matrix=reinterpret_cast<ConfMatrix&>(a);
  ConfMatrix &b_matrix=reinterpret_cast<ConfMatrix&>(b);

  bool ret=true;
  if (a_matrix.subtype() != b_matrix.subtype()) {
    std::cout << "INFO [compareMatrix(PixLib::ConfObj &a, PixLib::ConfObj &b)] sub types differ for \"" << a_matrix.name() << "\" / " << a_matrix.type()
	      << " : " << a_matrix.subtype() << " != " << b_matrix.subtype() << std::endl;
    ret &=false;
    return false;
  }


  switch (a_matrix.subtype()) {
  case PixLib::ConfMatrix::M_U1: {
    std::vector<bool> a_vec;
    std::vector<bool> b_vec;
    assert ( a_matrix.valueU1().ncol() == b_matrix.valueU1().ncol() );
    assert ( a_matrix.valueU1().nrow() == b_matrix.valueU1().nrow() );
    a_matrix.valueU1().get(a_vec);
    b_matrix.valueU1().get(b_vec);
      
    //    a_matrix.valueU1().get(a_vec);
    //    b_matrix.valueU1().get(b_vec);
    ret &= ( a_vec == b_vec );
    break;
  }
  case PixLib::ConfMatrix::M_U16: {
    std::vector<PixLib::TrimVal_t> a_vec;
    std::vector<PixLib::TrimVal_t> b_vec;
    assert ( a_matrix.valueU16().ncol() == b_matrix.valueU16().ncol() );
    assert ( a_matrix.valueU16().nrow() == b_matrix.valueU16().nrow() );
    a_matrix.valueU16().get(a_vec);
    b_matrix.valueU16().get(b_vec);
    ret &= ( a_vec == b_vec );
    break;
  }
  default: {
    return false;
  }
  }
  return ret;
}



bool operator==(PixLib::ConfObj &a, PixLib::ConfObj &b) {
  if (a.type()==PixLib::ConfObj::STRING &&  b.type()==PixLib::ConfObj::LIST) {
    //    std::cout << "WARNING [operator==(PixLib::ConfObj &, PixLib::ConfObj &)] list / int  : \"" 
    //	      << a.name() << "\" / " << a.type() << " != \"" 
    //	      << b.name() << "\" / " << b.type() << "." << std::endl;
    PixLib::ConfString &a_string = static_cast<PixLib::ConfString &>(a);
    PixLib::ConfList &b_list     = static_cast<PixLib::ConfList &>(b);
    return a_string.value() == b_list.sValue();
  }
  else if (b.type()==PixLib::ConfObj::STRING &&  a.type()==PixLib::ConfObj::LIST) {
    //    std::cout << "WARNING [operator==(PixLib::ConfObj &, PixLib::ConfObj &)] list / int  : \"" 
    //	      << a.name() << "\" / " << a.type() << " != \"" 
    //	      << b.name() << "\" / " << b.type() << "." << std::endl;
    PixLib::ConfString &b_string = static_cast<PixLib::ConfString &>(b);
    PixLib::ConfList &a_list     = static_cast<PixLib::ConfList &>(a);
    return b_string.value() == a_list.sValue();
  }
  else if (b.type()==PixLib::ConfObj::STRING &&  a.type()==PixLib::ConfObj::LIST) {
    std::cout << "WARNING [operator==(PixLib::ConfObj &, PixLib::ConfObj &)] list / int  : \"" 
	      << a.name() << "\" / " << a.type() << " != \"" 
	      << b.name() << "\" / " << b.type() << "." << std::endl;
  }
  else if ( a.type()  != b.type()) {
    std::cout << "ERROR [operator==(PixLib::ConfObj &, PixLib::ConfObj &)] type mismatch : \"" 
	      << a.name() << "\" / " << a.type() << " != \"" 
	      << b.name() << "\" / " << b.type() << "." << std::endl;
    return false;
  }

  bool ret=true;
  if ( a.name()  != b.name()) {
    std::cout << "ERROR [operator==(PixLib::ConfObj &, PixLib::ConfObj &)] name mismatch : " << a.name() << " != " << b.name() << "." << std::endl;
    ret = false;
  }
  

  switch (a.type()) {
  case PixLib::ConfObj::LIST:
  case PixLib::ConfObj::INT: {
    ret &= compare<int>(a,b);
    break;
  }
  case PixLib::ConfObj::FLOAT: {
    ret &= compare<float>(a,b);
    break;
  }
  case PixLib::ConfObj::BOOL: {
    ret &=compare<bool>(a,b);
    break;
  }
  case PixLib::ConfObj::STRING: {
    ret &= compare<std::string>(a,b);
    break;
  }
  case PixLib::ConfObj::VECTOR: {
    ret &= compareVector(a,b);
    break;
  }
  case PixLib::ConfObj::STRVECT: {
    ret &= compareStringVector(a,b);
    break;
  }
  case PixLib::ConfObj::MATRIX: {
    ret &= compareMatrix(a,b);
    break;
  }
  default: {
    std::cout << "ERROR [operator==(PixLib::ConfObj &a, PixLib::ConfObj &b)] trying to compare unsupported type : "
	      << a.name() << " / " << a.type() << std::endl;
    return false;
  }
    
  }
  return ret;
}

bool operator==(PixLib::ConfGroup &a, PixLib::ConfGroup &b)
{
  bool ret=true;
  if (a.size() != b.size()) {
    std::cout << "ERROR [operator==(PixLib::ConfGroup &a, PixLib::ConfGroup &b)] conf groups " << a.name() << " and " << b.name() << " have different sizes " << a.size() << " != " << b.size() << std::endl;
    ret&=false;
  }
  if (a.name() != b.name()) {
    std::cout << "ERROR [operator==(PixLib::ConfGroup &a, PixLib::ConfGroup &b)] conf groups have different names " << a.name() << " != " << b.name() << std::endl;
    ret&=false;
  }
  for (size_t i=0; i<a.size(); i++) {
    PixLib::ConfObj &bo=b[stripGroupName(a[i].name())];
    if (a[i] != bo) {
      std::cout << "ERROR [operator==(PixLib::ConfObj &a, PixLib::ConfObj &b)] conf objects \"" << a[i].name() << "\" / " << a[i].type() << " of group " << a.name() << " differ " << std::endl;
      ret &= false;
    }
  }
  return ret;
}

bool operator==(PixLib::Config &a, PixLib::Config &b) 
{
  bool ret=true;
  if (a.size() != b.size()) {
    std::cout << "ERROR [operator==(PixLib::Config &a, PixLib::Config &b)] configs " << a.name() << " and " << b.name() << " have different sizes " << a.size() << " != " << b.size() << std::endl;
    ret&=false;
  }
  if (a.name() != b.name()) {
    std::cout << "ERROR [operator==(PixLib::Config &a, PixLib::Config &b)] configs have different names " << a.name() << " != " << b.name() << std::endl;
    //    return false;
    ret&=false;
  }
  if (a.type() != b.type()) {
    std::cout << "INFO [operator==(PixLib::Config &a, PixLib::Config &b)] configs " << a.name() << " have different types " << a.type() << " != " << b.type() << std::endl;
    ret&=false;
  }
  for (unsigned int i=0; i<a.size(); i++) {
    PixLib::ConfGroup &bg=b[a[i].name()];
    if (a[i] != bg) {
      std::cout << "INFO [operator==(PixLib::Config &a, PixLib::Config &b)] conf groups " << a[i].name() << " of config " << a.name() << " differ " << std::endl;
      ret &= false;
    }
  }
  if (a.subConfigSize() != b.subConfigSize()) {
    std::cout << "INFO [operator==(PixLib::Config &a, PixLib::Config &b)] configs have different sizes " << a.size() << " != " << b.size() << std::endl;
    ret &= false;
  }
  for (unsigned int i=0; i<a.subConfigSize(); i++) {
    PixLib::Config &as=a.subConfig(i);
    PixLib::Config *bs=&b.subConfig(as.name());
    if (bs->name()=="__TrashConfig__") {
      bs=&b.subConfig(as.name()+"/"+as.type());
    }
    if (bs->name() != as.name()) {
      std::cout << "INFO [operator==(PixLib::Config &a, PixLib::Config &b)] did not get correct sub config for  " << as.name() << ", but got " 
		<< bs->name() << ". Available sub configs : " << std::endl;
      unsigned int j=0;
      for (std::map<std::string, Config*>::const_iterator iter = b.m_config.begin();
	   iter != b.m_config.end(); 
	   iter++, j++) {
	std::cout << iter->first;
	if (j+1<b.subConfigSize()) {
	  std::cout << ", ";
	}
      }
      std::cout << std::endl;
      ret &= false;
    }
    else if (as != *bs) {
      std::cout << "INFO [operator==(PixLib::Config &a, PixLib::Config &b)] conf groups " << a.subConfig(i).name() << " of config " << a.name() << " differ " << std::endl;
      ret &= false;
    }
  }
  return ret;
}
