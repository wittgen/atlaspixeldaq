#ifndef _PixA_Connectivity_h_
#define _PixA_Connectivity_h_

#include <cassert>
#include <iostream>
#include <PixConnectivity/ModuleConnectivity.h>
#include <PixConnectivity/Pp0Connectivity.h>
#include <PixConnectivity/OBConnectivity.h>
#include <PixConnectivity/RodBocConnectivity.h>
#include <PixConnectivity/RodCrateConnectivity.h>
#include <PixConnectivity/TimConnectivity.h>
#include <PixConnectivity/PixDisable.h>
//#ifdef _PIXLIB_PARTCONNOBJ
//#  include <PixConnectivity/PartitionConnectivity.h>
//#endif
#include <map>
#include <string>
#include "Locations.h"

#include "ConnectivityBase.h"
#include "pixa_exception.h"
#include "ConfigHandle.h"

namespace PixA {

  //typedef  std::map<std::string, PixLib::ModuleConnectivity *> ModuleList;

  /** Type specifying the revision of a configuration.
   */
  typedef unsigned int Revision_t;   // need to define this before including ConnObjConfigDb

}

#include "ConnObjConfigDb.h"

namespace PixLib {
  class PixDisable;
}

namespace PixA {

  //typedef  std::map<std::string, PixLib::ModuleConnectivity *> ModuleList;

  //typedef  std::map<std::string, PixLib::Pp0Connectivity *>    PP0List;
  class IConnectivity;
  class ModuleList;
  class Pp0List;
  class RodList;
  class CrateList;

  /** Wrapper around the modules of a PP0.
   * The wrapper provides iterators to ierate over all modules of the PP0.
   */
  class ModuleList {

    friend class IConnectivity;
    friend class Pp0List;
    friend class PixConnectivityWrapperBase;

  protected:

    static const unsigned int s_nModulesMax = 9;

  public:
    ModuleList(PixLib::Pp0Connectivity &pp0) : m_pp0(&pp0) {}

    /** A const iterator pointing at the connectivity of one Module
     */
    class const_iterator {
      friend class ModuleList;
    protected:
      const_iterator(const PixLib::Pp0Connectivity *pp0, int slot) : m_pp0(pp0), m_slot(slot) {};
    public:
      const_iterator() : m_pp0(NULL), m_slot(0) {};
      const_iterator &operator++() {do { m_slot++; } while (m_slot<s_nModulesMax && const_cast< PixLib::Pp0Connectivity *>(m_pp0)->modules(m_slot)==0);return *this;}

      bool operator==(const const_iterator &a) const { return (m_pp0==a.m_pp0 && m_slot==a.m_slot); }
      bool operator!=(const const_iterator &a) const { return (m_pp0!=a.m_pp0 || m_slot!=a.m_slot); }

      const PixLib::ModuleConnectivity *operator*() const {
	return const_cast<PixLib::Pp0Connectivity *>(m_pp0)->modules(m_slot);
      }
    protected:
      const PixLib::Pp0Connectivity *m_pp0;
      unsigned int      m_slot;
    };


    /** An iterator pointing at the connectivity of one Module
     */
    class iterator : public const_iterator {
      friend class ModuleList;
    protected:
      iterator(PixLib::Pp0Connectivity *pp0, int slot) : const_iterator(pp0,slot)  {}
    public:
      iterator() {};

      iterator &operator++() {
	const_iterator::operator++();
	return *this;
      }

      PixLib::ModuleConnectivity *operator*() {
	return const_cast<PixLib::Pp0Connectivity *>(m_pp0)->modules(m_slot);
      }

    };

    iterator begin() { 
      unsigned int slot=1;
      while (slot<s_nModulesMax && const_cast<PixLib::Pp0Connectivity *>(m_pp0)->modules(slot)==0) {
	slot++;
      }
      return iterator(m_pp0,slot);
    }
    iterator end()   { return iterator(m_pp0,s_nModulesMax);}

    const_iterator begin() const { 
      unsigned int slot=1;
      while (slot<s_nModulesMax && const_cast<PixLib::Pp0Connectivity *>(m_pp0)->modules(slot)==0) {
	slot++;
      }
      return const_iterator(m_pp0,slot);
    }
    const_iterator end()   const { return const_iterator(m_pp0,s_nModulesMax);}

    bool empty() const {
      for (unsigned int slot_i=0; slot_i<s_nModulesMax; slot_i++) {
	if (m_pp0->modules(slot_i)) return false;
      }
      return true;
    }

    unsigned int size() const {
      unsigned int n=0;
      for (unsigned int slot_i=0; slot_i<s_nModulesMax; slot_i++) {
	if (m_pp0->modules(slot_i)) n++;
      }
      return n;
    }

    iterator find(const ModuleLocationBase &module_location) {
      for (unsigned int slot_i=0; slot_i<s_nModulesMax; slot_i++) {
	PixLib::ModuleConnectivity *mod_conn=m_pp0->modules(slot_i);
	if (mod_conn && mod_conn->name()==module_location) return iterator(m_pp0,slot_i);
      }
      return end();
    }

    const_iterator find(const ModuleLocationBase &module_location) const {
      for (unsigned int slot_i=0; slot_i<s_nModulesMax; slot_i++) {
	const PixLib::ModuleConnectivity *mod_conn=const_cast<PixLib::Pp0Connectivity *>(m_pp0)->modules(slot_i);
	if (mod_conn && const_cast<PixLib::ModuleConnectivity *>(mod_conn)->name()==module_location) return const_iterator(m_pp0,slot_i);
      }
      return end();
    }

    bool hasParent() const;

    Pp0List parent();

    const Pp0List parent() const;

    const Pp0LocationBase &location() const { return const_cast<PixLib::Pp0Connectivity *>(m_pp0)->name(); }

  private:
    PixLib::Pp0Connectivity *m_pp0;
  };


  /** Wrapper arround the PP0s of a RodBocConnectivity
   * The wrapper provides iterators to iterator over all PP0
   */
  class Pp0List {
    friend class IConnectivity;
    friend class PixConnectivityWrapperBase;
  protected:
    static const unsigned int s_nObsMax = 4;
  public:

    /** Const iterator which points to one PP0.
     */
    class const_iterator
    {
      friend class Pp0List;
    protected:
      const_iterator(const PixLib::RodBocConnectivity &conn, unsigned int slot) 
	: m_rodBoc(&conn), m_slot(slot){}

    public:
      const_iterator(const const_iterator &a) : m_rodBoc(a.m_rodBoc), m_slot(a.m_slot) {}

      const_iterator &operator++() {
	do { m_slot++; 
	} while (m_slot<s_nObsMax
				&& (const_cast<PixLib::RodBocConnectivity *>(m_rodBoc)->obs(m_slot)==0
				    || const_cast<PixLib::RodBocConnectivity *>(m_rodBoc)->obs(m_slot)->pp0()==0));
	return *this;
      }

      bool operator==(const const_iterator &a) const { return (m_rodBoc==a.m_rodBoc && m_slot==a.m_slot); }
      bool operator!=(const const_iterator &a) const { return (m_rodBoc!=a.m_rodBoc || m_slot!=a.m_slot);}

      const PixLib::Pp0Connectivity *operator*() const {
	return const_cast<PixLib::RodBocConnectivity *>(m_rodBoc)->obs(m_slot)->pp0();
      }

      unsigned short slot() const { return m_slot; }

    protected:
      const PixLib::RodBocConnectivity *m_rodBoc;
      unsigned short      m_slot;
    };

    /** Iterator which points to one PP0.
     */
    class iterator : public const_iterator {
      friend class Pp0List;
    protected:
      iterator(PixLib::RodBocConnectivity &conn, unsigned int slot) : const_iterator(const_cast<PixLib::RodBocConnectivity &>(conn),slot) {}

    public:
      iterator(const iterator &a) : const_iterator(a) {}

      iterator &operator++() {
	const_iterator::operator++();
	return *this;
      }

      PixLib::Pp0Connectivity *operator*() {
	return const_cast<PixLib::RodBocConnectivity *>(m_rodBoc)->obs(m_slot)->pp0();
      }
    };


  protected:

  public:
    /** Constructor which should only be used by the connectivity interface
     */
    Pp0List(PixLib::RodBocConnectivity &conn) : m_rodBoc(&conn) {};

    /** Get an iterator which points at the first PP0.
     */
    iterator begin() {
      unsigned int slot=0;
      while (slot<s_nObsMax && (   const_cast<PixLib::RodBocConnectivity *>(m_rodBoc)->obs(slot)==0 
				|| const_cast<PixLib::RodBocConnectivity *>(m_rodBoc)->obs(slot)->pp0()==0)) {
	slot++;
      }
      //std::cout << "INFO [Pp0List::begin] " << const_cast<PixLib::RodBocConnectivity *>(m_rodBoc) << " slot = " << slot << std::endl;
      return iterator(*m_rodBoc,slot);
    }

    /** Get an iterator which points behind the last PP0.
     * ... i.e. points to an invalid location.
     */
    iterator end() {
      return iterator(*m_rodBoc,s_nObsMax);
    }

    /** Get a const iterator which points at the first PP0.
     */
    const_iterator begin() const {
      unsigned int slot=0;
      while (slot<s_nObsMax && (   const_cast<PixLib::RodBocConnectivity *>(m_rodBoc)->obs(slot)==0 
				|| const_cast<PixLib::RodBocConnectivity *>(m_rodBoc)->obs(slot)->pp0()==0)) {
	slot++;
      }
      //std::cout << "INFO [Pp0List::begin] " << const_cast<PixLib::RodBocConnectivity *>(m_rodBoc) << " slot = " << slot << std::endl;
      return const_iterator(*m_rodBoc,slot);
    }

    /** Get a const iterator which points behind the last PP0.
     */
    const_iterator end() const {
      return const_iterator(*m_rodBoc,s_nObsMax);
    }

    /** Returns true if the PP0 list does not contain PP0s.
     */
    bool empty() const {
      for (unsigned int i=0; i<s_nObsMax; i++) {
	if (const_cast<PixLib::RodBocConnectivity *>(m_rodBoc)->obs(i)!=0) return false;
      }
      return true;
    }

    /** Return the number of PP0s. 
     */
    unsigned int size() const {
      unsigned int n=0;
      for (unsigned int i=0; i<s_nObsMax; i++) {
	if (   const_cast<PixLib::RodBocConnectivity *>(m_rodBoc)->obs(i)!=0
	    && const_cast<PixLib::RodBocConnectivity *>(m_rodBoc)->obs(i)->pp0()!=0 ) n++;
      }
      return n;
    }

    /** Search for the given PP0.
     * @return iterator pointing to the PP0 in question or @ref end.
     */
    iterator find(const Pp0LocationBase &location) {
      for (unsigned int i=0; i<s_nObsMax; i++) {
	PixLib::RodBocConnectivity *casted_rod_boc  = const_cast<PixLib::RodBocConnectivity *>(m_rodBoc);
	if (casted_rod_boc->obs(i)!=0 && casted_rod_boc->obs(i)->pp0() && casted_rod_boc->obs(i)->pp0()->name()==location) {
	  return iterator(*m_rodBoc,i);
	}
      }
      return end();
    }

    /** Search for the given PP0 (read only).
     * @return iterator pointing to the PP0 in question or @ref end.
     */
    const_iterator find(const Pp0LocationBase &location) const{
      for (unsigned int i=0; i<s_nObsMax; i++) {
	if (m_rodBoc->obs(i)!=0 && m_rodBoc->obs(i)->pp0()->name()==location) {
	  return const_iterator(*m_rodBoc,i);
	}
      }
      return end();
    }

    bool hasParent() const;

    RodList parent();

    const RodList parent() const;

    const RodLocationBase &location() const { return const_cast<PixLib::RodBocConnectivity *>(m_rodBoc)->name(); }

  private:
    PixLib::RodBocConnectivity *m_rodBoc;
  };


  /** Wrapper arround the RODs of a RodCrateConnectivity
   * The wrapper provides iterators to iterator over all RODs
   */
  class RodList {
    friend class IConnectivity;
    friend class PixConnectivityWrapperBase;
  public:

    typedef std::map<int, PixLib::RodBocConnectivity*> RodCrateMap_t;

    /** Const iterator which points to one PP0.
     */
    class const_iterator
    {
      friend class RodList;
    protected:
      const_iterator(const RodCrateMap_t::const_iterator &rod_iter) : m_rodIter(rod_iter){}

    public:
      const_iterator(const const_iterator &a) : m_rodIter(a.m_rodIter) {}

      const_iterator &operator++() {
	m_rodIter++;
	return *this;
      }

      bool operator==(const const_iterator &a) const { return (m_rodIter == a.m_rodIter); }
      bool operator!=(const const_iterator &a) const { return (m_rodIter != a.m_rodIter); }

      const PixLib::RodBocConnectivity *operator*() const {
	return const_cast<PixLib::RodBocConnectivity *>(m_rodIter->second);
      }

      unsigned short slot() const { return m_rodIter->first; }

    protected:
      RodCrateMap_t::const_iterator m_rodIter;
    };

    /** Iterator which points to one PP0.
     */
    class iterator : public const_iterator {
      friend class RodList;
    protected:
      iterator(const RodCrateMap_t::const_iterator &rod_iter) : const_iterator(rod_iter) {}

    public:
      iterator(const iterator &a) : const_iterator(a) {}

      iterator &operator++() {
	const_iterator::operator++();
	return *this;
      }

      PixLib::RodBocConnectivity *operator*() {
	return const_cast<PixLib::RodBocConnectivity *>(m_rodIter->second);
      }
    };


  protected:
    
  public:
    /** Constructor which should only be used by the connectivity interface
     */
    RodList(PixLib::RodCrateConnectivity &conn) : m_rodCrate(&conn) {};

    /** Get an iterator which points at the first ROD.
     */
    iterator begin() {
      return iterator(RodCrateMap_t::const_iterator(m_rodCrate->rodBocs().begin()));
    }

    /** Get an iterator which points behind the last ROD.
     * ... i.e. points to an invalid location.
     */
    const_iterator end() const {
      return const_iterator(RodCrateMap_t::iterator(m_rodCrate->rodBocs().end()));
    }

    /** Get an iterator which points behind the last ROD.
     * ... i.e. points to an invalid location.
     */
    iterator end() {
      return iterator(m_rodCrate->rodBocs().end());
    }


    /** Get a const iterator which points at the first ROD.
     */
    const_iterator begin() const {
      return const_iterator(RodCrateMap_t::iterator(m_rodCrate->rodBocs().begin()));
    }

    /** Returns true if the ROD list does not contain RODs.
     */
    bool empty() const {
      return m_rodCrate->rodBocs().empty();
    }

    /** Return the number of RODs. 
     */
    unsigned int size() const {
      return m_rodCrate->rodBocs().size();
    }

    /** Search for the given ROD.
     * @return iterator pointing to the ROD in question or @ref end.
     */
    iterator find(const RodLocationBase &location) {
      for(RodCrateMap_t::iterator rod_iter=m_rodCrate->rodBocs().begin();
	  rod_iter!=m_rodCrate->rodBocs().end();
	  rod_iter++) {
	if (const_cast<PixLib::RodBocConnectivity *>(rod_iter->second)->name() == location){
	  return iterator(rod_iter);
	}
      }
      return end();
    }

    /** Search for the given ROD.
     * @return iterator pointing to the ROD in question or @ref end.
     */
    const_iterator find(const RodLocationBase &location) const {
      return const_iterator(const_cast<RodList*>(this)->find(location));
    }

    bool hasParent() const;

    CrateList parent();

    const CrateList parent() const;

    const RodLocationBase &location() const { return const_cast<PixLib::RodCrateConnectivity *>(m_rodCrate)->name(); }


  private:
    PixLib::RodCrateConnectivity *m_rodCrate;
  };

  /** Wrapper arround the RODs of a RodCrateConnectivity
   * The wrapper provides iterators to iterator over all RODs
   */
  class CrateList {
    friend class IConnectivity;
    friend class PixConnectivityWrapperBase;
    friend class RodList;
  public:

    typedef std::map<std::string, PixLib::RodCrateConnectivity*> CrateMap_t;

    /** Const iterator which points to one PP0.
     */
    class const_iterator
    {
      friend class CrateList;
    protected:
      const_iterator(const CrateMap_t::const_iterator &crate_iter) : m_crateIter(crate_iter){}

    public:
      const_iterator(const const_iterator &a) : m_crateIter(a.m_crateIter) {}

      const_iterator &operator++() {
	m_crateIter++;
	return *this;
      }

      bool operator==(const const_iterator &a) const { return (m_crateIter == a.m_crateIter); }
      bool operator!=(const const_iterator &a) const { return (m_crateIter != a.m_crateIter); }

      const PixLib::RodCrateConnectivity *operator*() const {
	return const_cast<PixLib::RodCrateConnectivity *>(m_crateIter->second);
      }

      std::string crateName() const {return m_crateIter->first;}

    protected:
      CrateMap_t::const_iterator m_crateIter;
    };

    /** Iterator which points to one PP0.
     */
    class iterator : public const_iterator {
      friend class CrateList;
    protected:
      iterator(const CrateMap_t::const_iterator &crate_iter) : const_iterator(crate_iter) {}

    public:
      iterator(const iterator &a) : const_iterator(a) {}

      iterator &operator++() {
	const_iterator::operator++();
	return *this;
      }

      PixLib::RodCrateConnectivity *operator*() {
	return const_cast<PixLib::RodCrateConnectivity *>(m_crateIter->second);
      }
    };


  protected:
    
    /** Constructor which should only be used by the connectivity interface
     */
    CrateList(CrateMap_t &crates) : m_crates(&crates) {};

  public:
    /** Get an iterator which points at the first ROD.
     */
    iterator begin() {
      return iterator(CrateMap_t::const_iterator(m_crates->begin()));
    }

    /** Get an iterator which points behind the last ROD.
     * ... i.e. points to an invalid location.
     */
    const_iterator end() const {
      return const_iterator(CrateMap_t::iterator(m_crates->end()));
    }

    /** Get an iterator which points behind the last ROD.
     * ... i.e. points to an invalid location.
     */
    iterator end() {
      return iterator(m_crates->end());
    }


    /** Get a const iterator which points at the first ROD.
     */
    const_iterator begin() const {
      return const_iterator(CrateMap_t::iterator(m_crates->begin()));
    }

    /** Returns true if the ROD list does not contain RODs.
     */
    bool empty() const {
      return m_crates->empty();
    }

    /** Return the number of RODs. 
     */
    unsigned int size() const {
      return m_crates->size();
    }

    /** Search for the given ROD.
     * @return iterator pointing to the ROD in question or @ref end.
     */
    iterator find(const CrateLocationBase &location) {
      return iterator(m_crates->find(location));
    }

    /** Search for the given ROD.
     * @return iterator pointing to the ROD in question or @ref end.
     */
    const_iterator find(const CrateLocationBase &location) const {
      return const_iterator(m_crates->find(location));
    }

  private:
    CrateMap_t *m_crates;
  };


  //  typedef  std::map<std::string, PixLib::RodBocConnectivity *>    RODList_t;

  class ConnectivityManager;
  class ConnectivityRef;

  class IConnectivity : public ConnectivityBase
  {
    friend class ConnectivityRef;
    friend class ConnectivityManager;
  protected:
    /** Get the list of Pp0s which are connected to a ROD.
     * @param rod_location the crate name and rod slot.
     */
    virtual const Pp0List pp0s(const RodLocationBase &rod_location) const = 0;

    /** Get the list of RODs which are connected to a crate  (read only).
     */
    virtual const RodList rods(const CrateLocationBase &crate_location) const =0;

    /** Get the list of RODs which are connected to a crate.
     */
    virtual RodList rods(const CrateLocationBase &crate_location)  =0;

    /** Get the list of crates (read only).
     */
    virtual const CrateList crates() const  =0;

    /** Get the list of crates.
     */
    virtual CrateList crates()  =0;

    /** Get the list of modules which are connected to a Pp0 (read only).
     */
    virtual const ModuleList modules(const Pp0LocationBase &pp0_location) const  =0;

    /** Get the number of modules which are connected to a Pp0.
     */
    virtual const ModuleList modules(const Pp0List::const_iterator &iter) const =0;

    /** Find the connectivity object of the given module.
     */
    virtual const PixLib::ModuleConnectivity *findModule(const ModuleLocationBase &module_location) const = 0;

    /** Find the connectivity object of the given pp0.
     */
    virtual const PixLib::Pp0Connectivity *findPp0(const Pp0LocationBase &pp0_location) const = 0;

    /** Find the connectivity object of the given ROD.
     */
    virtual const PixLib::RodBocConnectivity *findRod(const RodLocationBase &rod_location) const = 0;

    /** Find the connectivity object of the given crate.
     */
    virtual const PixLib::RodCrateConnectivity *findCrate(const CrateLocationBase &crate_location) const = 0;

    /** Get a handle for the on demand loader of the module configuration.
     * Does not allow to iterate over the values of the configuration or configuration group.
     */
    // now done by configdb: virtual ConfigHandle modConf(const ModuleLocationBase &module_location, Revision_t rev) =0;

    /** Get a handle for the on demand loader of the module configuration.
     * Does not allow to iterate over the values of the configuration or configuration group.
     */
    // now done by configdb: virtual const ConfigHandle modConf(const ModuleLocationBase &module_location, Revision_t rev) const =0;


    /** Get a handle for the on demand loader of the module configuration.
     * Does not allow to iterate over the values of the configuration or configuration group.
     */
    // now done by configdb: virtual ConfigHandle modConf(ModuleList::iterator &module_iter, Revision_t rev) =0;

    /** Get a handle for the on demand loader of the module configuration.
     * Does not allow to iterate over the values of the configuration or configuration group.
     */
    // now done by configdb: virtual const ConfigHandle modConf(const ModuleList::const_iterator &module_iter, Revision_t rev) const =0;


    /** Write the current module configuration.
     */
    //    virtual Revision_t writeModConf(const ModuleList::const_iterator &module_iter, Revision_t rev) const = 0;
    
    /** Write the current module configuration.
     */
    //    virtual Revision_t writeModConf(const ModuleLocationBase &module_location, Revision_t rev) const = 0;


    //@todo the BOC config should be accessed with the rod name etc. not the pp0 name.
    // now done by configdb: virtual ConfigHandle bocConf(Pp0List::iterator &pp0_iter, Revision_t rev) =0;

    // now done by configdb: virtual const ConfigHandle bocConf(const Pp0List::const_iterator &pp0_iter, Revision_t rev) const =0;


    // now done by configdb: virtual ConfigHandle bocConf(const Pp0LocationBase &pp0_location, Revision_t rev) =0;

    // now done by configdb: virtual const ConfigHandle bocConf(const Pp0LocationBase &pp0_location, Revision_t rev) const =0;


    // now done by configdb: virtual ConfigHandle rodConf(Pp0List::iterator &pp0_iter, Revision_t rev) =0;

    // now done by configdb: virtual const ConfigHandle rodConf(const Pp0List::const_iterator &pp0_iter, Revision_t rev) const =0;


    // now done by configdb: virtual ConfigHandle rodConf(const Pp0LocationBase &pp0_location, Revision_t rev) =0;

    // now done by configdb: virtual const ConfigHandle rodConf(const Pp0LocationBase &pp0_location, Revision_t rev) const =0;


    // now done by configdb: virtual std::shared_ptr<PixLib::PixDisable> getDisable(std::string disableName, Revision_t revision) =0;

    //    virtual void writeDisableToDbServer(PixLib::PixDisable &disable, std::string pendTag) =0;

    // now done by configdb: virtual void writeDisable(PixLib::PixDisable &disable) =0;

  public:
    enum EConnTag {kId, kIdCFG, kConn,kAlias,kPayload, kConfig, kModConfig};
  protected:
    virtual std::string tag(EConnTag tag) const = 0;
    // RODLocation and Pp0Location have the same type
    //    virtual PixLib::Config &rodConf(const RODLocationBase &rod_location) =0;

    ///    virtual const PixLib::Config &rodConf(const RODLocationBase &rod_location) const =0;

  };

//   class ConnectivityManager;

//   class ConnectivityPtr {
//     friend class ConnectivityManager;
//   protected:
//     ConnectivityPtr(IConnectivity *ptr) : m_connectivity(ptr) {m_connectivity->use();}
//   public:
//     ConnectivityPtr() : m_connectivity(NULL) {}
//     ConnectivityPtr(const ConnectivityPtr &ref) : m_connectivity(ref.m_connectivity){ m_connectivity->use();}
//     ~ConnectivityPtr() { if (m_connectivity) m_connectivity->done();}

//     ConnectivityPtr &operator=(ConnectivityPtr &a) {
//       if (m_connectivity) {
// 	m_connectivity->done();
// 	m_connectivity=NULL;
//       }
//       if (a) {
// 	m_connectivity=a.m_connectivity;
// 	m_connectivity->use();
//       }
//       return *this;
//     }
//     bool isValid() const {return m_connectivity != NULL;}

//     operator bool () const {return m_connectivity != NULL;}

//     IConnectivity *operator->() {return m_connectivity;}
//     const IConnectivity *operator->() const {return m_connectivity;}

//     IConnectivity &operator*() {return *m_connectivity;}
//     const IConnectivity &operator*() const {return *m_connectivity;}

//   private:
//     IConnectivity *m_connectivity;
//   };

  /** Wrapper around the connectivity interface.
   * The wrapper takes care of the book keeping.
   */
  class ConnectivityRef 
  {
    friend class ConnectivityManager;
    friend class PixConnectivityWrapperBase;
  protected:
    ConnectivityRef(IConnectivity *ptr, const ConnObjConfigDbTagRef &config_db)
      : m_connectivity(ptr),
	m_configDb(config_db),
	m_moduleConfigDb(config_db)
    {
      if (m_connectivity) m_connectivity->use();
    }

    ConnectivityRef(IConnectivity *ptr,
		    const ConnObjConfigDbTagRef &config_db,
		    const ConnObjConfigDbTagRef &module_config_db)
      : m_connectivity(ptr),
	m_configDb(config_db),
	m_moduleConfigDb(module_config_db)
    {
      if (m_connectivity) m_connectivity->use();
    }

  public:
    ConnectivityRef() : m_connectivity(NULL) {}

    ConnectivityRef(const ConnectivityRef &ref)
      : m_connectivity(ref.m_connectivity),
	m_configDb(ref.m_configDb),
	m_moduleConfigDb(ref.m_moduleConfigDb)
    { if (m_connectivity)  m_connectivity->use(); }

    ~ConnectivityRef() { if (m_connectivity) m_connectivity->done();}

    operator bool() const {
      return m_connectivity != NULL && (m_configDb);
    }

    void reset() {
      if (m_connectivity) {
	m_connectivity->done();
      }
      m_connectivity=NULL;
      m_configDb=ConnObjConfigDbTagRef();
      m_moduleConfigDb = m_configDb;
    }

    /** Return true if the this and the given object both reference the same connectivity interface.
     */
    bool haveSameConnectivity(const ConnectivityRef &a) const {
      return m_connectivity == a.m_connectivity;
    }

    /** Return true if the this and the given object both reference the same connectivity interface.
     */
    bool haveSameConfiguration(const ConnectivityRef &a) const {
      return m_configDb == a.m_configDb && m_moduleConfigDb == a.m_moduleConfigDb;
    }

    bool areIdentical(const ConnectivityRef &a) const {
      return operator==(a);
    }

    bool operator==(const ConnectivityRef &a) const {
      return haveSameConfiguration(a) && haveSameConnectivity(a);
    }

    /** Return true if the this and the given object both reference the same connectivity interface.
     */
    bool refersTo(const IConnectivity *ptr) const {
      return m_connectivity == ptr;
    }


    ConnectivityRef &operator=(const ConnectivityRef &a) {
      if ( haveSameConnectivity(a) ) {
	m_configDb=a.m_configDb;
	m_moduleConfigDb=a.m_moduleConfigDb;
	return *this;
      }
      reset();
      if (a) {
	m_connectivity=a.m_connectivity;
	m_connectivity->use();
	m_configDb=a.m_configDb;
	m_moduleConfigDb=a.m_moduleConfigDb;
      }
      return *this;
    }

  public:

    /** Get the list of Pp0s which are connected to a ROD.
     * @param rod_location the crate name and rod slot.
     */
    const Pp0List pp0s(const RodLocationBase &rod_location) const  {
      return m_connectivity->pp0s(rod_location);
    }

    /** Get the list of RODs which are connected to a crate.
     */
    const RodList rods(const CrateLocationBase &crate_location) const  {
      return m_connectivity->rods(crate_location);
    }

    /** Get the list of crates.
     */
    const CrateList crates() const  {
      return m_connectivity->crates();
    }

    /** Get the list of modules which are connected to a Pp0 (read only).
     */
    const ModuleList modules(const Pp0LocationBase &pp0_location) const  {
      return m_connectivity->modules(pp0_location);
    }

    /** Get the list of modules which are connected to a Pp0.
     */
    ModuleList modules(Pp0List::const_iterator &iter) {
      return m_connectivity->modules(iter);
    }

    /** Get the list of modules which are connected to a Pp0 (read only)
     */
    const ModuleList modules(const Pp0List::const_iterator &iter) const {
      return m_connectivity->modules(iter);
    }


    /** Find the connectivity object of the given module.
     */
    const PixLib::ModuleConnectivity *findModule(const ModuleLocationBase &module_location) const {
      return m_connectivity->findModule(module_location);
    }

    /** Find the connectivity object of the given PP0.
     */
    const PixLib::Pp0Connectivity *findPp0(const Pp0LocationBase &pp0_location) const {
      return m_connectivity->findPp0(pp0_location);
    }


    /** Find the connectivity object of the given ROD.
     */
    const PixLib::RodBocConnectivity *findRod(const RodLocationBase &rod_location) const {
      return m_connectivity->findRod(rod_location);
    }


    /** Find the connectivity object of the given crate.
     */
    const PixLib::RodCrateConnectivity *findCrate(const CrateLocationBase &crate_location) const {
      return m_connectivity->findCrate(crate_location);
    }


    /** Get a handle for the on demand loader of the module configuration.
     * Will not allow to acces matrices like FDAC and TDAC values.
     * Does not allow to iterate over the values of the configuration or configuration group.
     */

    ConfigHandle modConf(const ModuleLocationBase &module_location) const;
    std::string modConf_name(const ModuleLocationBase &module_location) const;//******maria elena added
    std::string modConf_name(ModuleList::const_iterator &module_iter) const;//******maria elena added

    /** Get a handle for the on demand loader of the module configuration.
     * Will not allow to acces matrices like FDAC and TDAC values.
     * Does not allow to iterate over the values of the configuration or configuration group.
     */
    ConfigHandle modConf(ModuleList::const_iterator &module_iter) const;

    // get BOC configuration
    ConfigHandle bocConf(Pp0List::const_iterator &pp0_iter) const;

    ConfigHandle bocConf(RodList::const_iterator &rod_iter) const;

    ConfigHandle bocConf(const Pp0LocationBase &pp0_location) const;

    ConfigHandle bocConfFromRodLocation(const std::string &rod_location) const;


    // get ROD controller configuration
    ConfigHandle rodConf(RodList::const_iterator &rod_iter) const;

    ConfigHandle rodConf(Pp0List::const_iterator &pp0_iter) const;

    ConfigHandle rodConf(const Pp0LocationBase &pp0_location) const;

    ConfigHandle rodConfFromRodLocation(const std::string &rod_location) const;


    // get ROD configuration
    ConfigHandle modGroupConf(RodList::const_iterator &rod_iter) const;

    ConfigHandle modGroupConf(const std::string &rod_location) const;


    // get TIM configuration
    ConfigHandle timConf(CrateList::const_iterator &crate_iter) const;

    ConfigHandle timConf(const std::string &crate_location) const;

    ConfigHandle timConfFromTimLocation(const std::string &tim_location) const;

    /** Get a disable for the given configuration tag, id tag and the revision.
     * To make this method work also for the disable used for the scan,
     * this method tries to get the disable first with the given revision if that
     * fails it uses 0 as revision.
     */
    std::shared_ptr<const PixLib::PixDisable> getDisable(std::string disableName) const;

    std::string tag(IConnectivity::EConnTag a_tag) const {
      switch (a_tag) {
      case PixA::IConnectivity::kConfig:
	return cfgTag();
      case PixA::IConnectivity::kModConfig:
	return modCfgTag();
      default:
	return m_connectivity->tag(a_tag);
      }
    }

    Revision_t revision() const {
      return m_configDb.revision();
    }

    const std::string &cfgIdTag() const {
      return m_configDb.idTag();
    }

    const std::string &cfgTag() const {
      return m_configDb.tag();
    }

    const std::string &modCfgIdTag() const {
      return m_moduleConfigDb.idTag();
    }

    const std::string &modCfgTag() const {
      return m_moduleConfigDb.tag();
    }

    bool idTagsAgree() const {
      return  tag(PixA::IConnectivity::kId) == cfgIdTag() && modCfgIdTag() == cfgIdTag();
    }

    bool cfgTagsAgree() const {
      return  idTagsAgree() && modCfgTag()==cfgTag();
    }

    ConnObjConfigDbTagRef configDb() const       { return m_configDb; }
    ConnObjConfigDbTagRef moduleConfigDb() const { return m_moduleConfigDb; }
      
  private:

    void exceptionDoesNotExist(const std::string &location) const  ;
    void exceptionTimDoesNotExist(const std::string &location) const  ;
    void exceptionIllegalIterator(const std::string &iterator_type) const  ;

    IConnectivity        *m_connectivity;
    // Revision_t            m_revision;
    mutable ConnObjConfigDbTagRef m_configDb;
    mutable ConnObjConfigDbTagRef m_moduleConfigDb;
  };



  // some convenience functions:
  //  inline const ModuleLocationBase &location(const ModuleList::iterator &iter) {
  //    return const_cast<PixLib::ModuleConnectivity *>(*iter)->name();
  //  }

  inline const ModuleLocationBase &location(const ModuleList::const_iterator &iter) {
    return const_cast<PixLib::ModuleConnectivity *>(*iter)->name();
  }

  inline const Pp0LocationBase &location(Pp0List::const_iterator &iter) {
    return const_cast<PixLib::Pp0Connectivity *>(*iter)->name();
  }

  inline const std::string &rodLocation(const PixLib::Pp0Connectivity *pp0_conn) {
    PixLib::Pp0Connectivity *casted_pp0_conn = const_cast<PixLib::Pp0Connectivity *>(pp0_conn);
    assert( pp0_conn && casted_pp0_conn->ob() && casted_pp0_conn->ob()->rodBoc() );
    return casted_pp0_conn->ob()->rodBoc()->name();
  }

  inline const std::string &rodLocation(Pp0List::const_iterator &iter) {
    const PixLib::Pp0Connectivity *pp0_conn = *iter;
    return rodLocation(pp0_conn);
  }

  inline const std::string &rodLocation(const PixLib::RodBocConnectivity *rod_conn) {
    assert( rod_conn );
    return const_cast<PixLib::RodBocConnectivity *>(rod_conn)->name();
  }

  inline const std::string &rodLocation(RodList::const_iterator &iter) {
    const PixLib::RodBocConnectivity *rod_conn = *iter;
    return rodLocation(rod_conn);
  }


  //  inline const Pp0LocationBase &location(const Pp0List::const_iterator &iter) {
  //    return const_cast<PixLib::Pp0Connectivity *>(*iter)->name();
  //  }

  inline RodList::iterator rodBegin(CrateList::iterator &iter) {
    return RodList(**iter).begin();
  }

  inline RodList::iterator rodEnd(CrateList::iterator &iter) {
    return RodList(**iter).end();
  }

  inline RodList::const_iterator rodBegin(CrateList::const_iterator &iter) {
    return RodList(const_cast<PixLib::RodCrateConnectivity &>(**iter)).begin();
  }

  inline RodList::const_iterator rodEnd(CrateList::const_iterator &iter) {
    return RodList(const_cast<PixLib::RodCrateConnectivity &>(**iter)).end();
  }

  inline Pp0List::iterator pp0Begin(RodList::iterator &iter) {
    return Pp0List(**iter).begin();
  }

  inline Pp0List::iterator pp0End(RodList::iterator &iter) {
    return Pp0List(**iter).end();
  }

  inline Pp0List::const_iterator pp0Begin(RodList::const_iterator &iter) {
    return Pp0List(const_cast<PixLib::RodBocConnectivity &>(**iter)).begin();
  }

  inline Pp0List::const_iterator pp0End(RodList::const_iterator &iter) {
    return Pp0List(const_cast<PixLib::RodBocConnectivity &>(**iter)).end();
  }

  inline ModuleList::iterator modulesBegin(Pp0List::iterator &iter) {
    return ModuleList(**iter).begin();
  }

  inline ModuleList::iterator modulesEnd(Pp0List::iterator &iter) {
    return ModuleList(**iter).end();
  }

  inline ModuleList::const_iterator modulesBegin(Pp0List::const_iterator &iter) {
    ModuleList module_list(const_cast<PixLib::Pp0Connectivity &>(**iter));
    return const_cast<const ModuleList &>(module_list).begin();
  }

  inline ModuleList::const_iterator modulesEnd(Pp0List::const_iterator &iter) {
    ModuleList module_list(const_cast<PixLib::Pp0Connectivity &>(**iter));
    return const_cast<const ModuleList &>(module_list).end();
  }


  inline bool ModuleList::hasParent() const {
    return m_pp0 && m_pp0->ob() && m_pp0->ob()->rodBoc();
  }

  inline Pp0List ModuleList::parent() {
    assert(m_pp0);
    assert(m_pp0->ob());
    assert(m_pp0->ob()->rodBoc());
    return *(m_pp0->ob()->rodBoc());
  }

  inline const Pp0List ModuleList::parent() const {
    return const_cast<ModuleList *>(this)->parent();
  }



  inline bool Pp0List::hasParent() const {
    return m_rodBoc && m_rodBoc->crate();
  }

  inline RodList Pp0List::parent() {
    assert(m_rodBoc);
    assert(m_rodBoc->crate());
    return (*m_rodBoc->crate());
  }

  inline const RodList Pp0List::parent() const {
    return const_cast<Pp0List *>(this)->parent();
  }

  inline bool RodList::hasParent() const {
    return m_rodCrate && m_rodCrate->partition();
  }

#ifdef _PIXLIB_PARTCONNOBJ
  inline CrateList RodList::parent() {
    assert(m_rodCrate);
    assert(m_rodCrate->partition());
    return m_rodCrate->partition()->rodCrates();
  }

  inline const CrateList RodList::parent() const {
    return const_cast<RodList *>(this)->parent();
  }
#endif

}

#endif
