#ifndef PixA_SharedConnObjConfigDbManager_h_
#define PixA_SharedConnObjConfigDbManager_h_

#include "IConnObjConfigDb.h"
#include <exception>

namespace PixA {

  class SharedConnObjConfigDbManager : public IConnObjConfigDbManager
  {

  public:
    SharedConnObjConfigDbManager() : m_used(0) {}

    unsigned int used() const { return m_used; }
    void incrementUsed() { ++m_used; }
    void decrementUsed() { --m_used; }

    void tryDestruction() {
      if (m_used==0) {
	if (s_instance == this) {
	  s_instance=NULL;
	}
	delete this;
      }
    }

    static void resetInstance() {
      if (s_instance) {
	s_instance->decrementUsed();
	s_instance->tryDestruction();
	s_instance =NULL;
      }
    }

    static void setInstance( SharedConnObjConfigDbManager *a_conn_obj_config_db_manager) { 
      resetInstance();
      s_instance = a_conn_obj_config_db_manager; 
      if (s_instance) {
	s_instance->incrementUsed();
      }
    }
    static bool hasInstance() {
      return s_instance != NULL;
    }

    static SharedConnObjConfigDbManager *instance() { 
      if (!s_instance) {
	throw std::runtime_error("FATAL [SharedConnObjConfigDbManager::instance] No instance.");
      }
      return s_instance; 
    };

  protected:
    static SharedConnObjConfigDbManager *currentInstance() { return s_instance; };
    
    ~SharedConnObjConfigDbManager() {}
  private:

    unsigned int m_used;

    static SharedConnObjConfigDbManager *s_instance;
  };

}
#endif
