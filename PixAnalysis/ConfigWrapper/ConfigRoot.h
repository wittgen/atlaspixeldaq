#ifndef _ConfigRoot_h_
#define _ConfigRoot_h_

#include <cassert>
#include <iostream>
#include "ObjStat_t.h"

namespace PixA {

  class IConfig;
  class IConfGrp;
  class IConfigBase;

  /** The root object of a config hierarchy.
   * The root object is responsible for bookkeeping of the usage of 
   * any of its childs.
   */
  class ConfigRoot
  {
    friend class IConfig;
    friend class IConfGrp;
    friend class IConfigBase;

  protected:

    /** Bootstrap the config hierarchy.
     * The constructor only may store the address of the given config interface, because
     * the interface only will be partially constructed since it needs this ConfigRoot object.
     */
    ConfigRoot(IConfig * a_config) : m_users(0), m_config(a_config) { s_stat.ctor(); }

    ~ConfigRoot();

    /** Tell the config root object, that one child is used.
     */
    void use() {
      m_users++;
      //      std::cout << "INFO [ConfigBase::use] config interface = " <<  static_cast<void *>(m_config) <<"  users = " << m_users << std::endl;
    }

    /** Tell the config root that the one child which was used is not needed any longer.
     * If there are no more users the config interface will be deleted.
     */
    void done(); // Implementation is in IConfig.h

    /** Debug method.
     */
    bool isRoot(const IConfig * const a_config) const {
      return a_config == m_config;
    }

    /** Debug method.
     */
    unsigned int use_count() const {
      return m_users;
    }

  private:
    unsigned int  m_users;
    IConfig      *m_config;
    static ObjStat_t s_stat;
  };



}

#endif

