#ifndef _DbServerConnectivityWrapper_h_
#define _DbServerConnectivityWrapper_h_

#include <sstream>
#include <string>
#include <map>
#include <iostream>
#include <cassert>

#include <PixConnectivity/PixConnectivity.h>
#include <PixConnectivity/RodBocConnectivity.h>
#include <PixConnectivity/OBConnectivity.h>
#include <PixConnectivity/Pp0Connectivity.h>
#include <PixConnectivity/ModuleConnectivity.h>
#include <PixConnectivity/PixDisable.h>
#include <PixDbServer/PixDbServerInterface.h>

#include "IConfig.h"
#include "PixConnectivityWrapperBase.h"
#include "ConfigHandle.h"

#include <memory>
#include "ObjectCache_t.h"

namespace PixLib {
  class PixModule;
  class PixModuleGroup;
  class PixDbServerInterface;
  class PixDisable;
}

namespace PixA {


  //  typedef std::shared_ptr<PixConnectivityConfigHandle> PixConnectivityConfigHandlePtr;
  class ConnectivityManager;

  class DbServerConnectivityWrapper : public PixConnectivityWrapperBase
  {
    friend class ConnectivityManager;
  protected:
    
    /** Create a connectivity through the db server interface.
     * @todo split PixConnectivity in a base class and derived classes the db server based connectivity 
     *  handling and one for the rootdb base connectivity handling.
     */
    DbServerConnectivityWrapper(const std::shared_ptr<PixLib::PixDbServerInterface> &db_server,
				const std::string &object_tag,
				const std::string &tagC,
				const std::string &tagP,
				const std::string &tagA,
				const std::string &tagCF,
				const std::string &tagModCF);

    ~DbServerConnectivityWrapper();

  public:


    ConfigHandle modConf(const ModuleLocationBase &module_location, Revision_t revision) {
      return modConfHandle(module_location,revision);
    }

    const ConfigHandle modConf(const ModuleLocationBase &module_location, Revision_t revision) const {
      return const_cast<DbServerConnectivityWrapper *>(this)->modConfHandle(module_location,revision);
    }

    ConfigHandle modConf(ModuleList::iterator &module_iter, Revision_t revision) {
      return modConfHandle(module_iter,revision);
    }

    const ConfigHandle modConf(const ModuleList::const_iterator &module_iter, Revision_t revision) const {
      return const_cast<DbServerConnectivityWrapper *>(this)->modConfHandle(module_iter,revision);
    }

//     Revision_t writeModConf(const ModuleList::const_iterator &module_iter, Revision_t revision) const {
//       return writeModConf(*module_iter,revision);
//     }

//     Revision_t writeModConf(const ModuleLocationBase &location, Revision_t revision) const {
//       return writeModConf(moduleConn(location),revision);
//     }

    ConfigHandle bocConf(Pp0List::iterator &pp0_iter, Revision_t revision) {
      const PixLib::RodBocConnectivity *rod_boc_conn=rodBocConnectivity(pp0_iter);
      return bocConfHandle(rod_boc_conn,revision);
    }

    const ConfigHandle bocConf(const Pp0List::const_iterator &pp0_iter, Revision_t revision) const {
      const PixLib::RodBocConnectivity *rod_boc_conn=rodBocConnectivity(pp0_iter);
      return const_cast<DbServerConnectivityWrapper *>(this)->bocConfHandle(rod_boc_conn,revision);
    }

    ConfigHandle bocConf(const Pp0LocationBase &pp0_location, Revision_t revision) {
      const PixLib::RodBocConnectivity *rod_boc_conn=rodBocConnectivity(pp0_location);
      return bocConfHandle(rod_boc_conn,revision);
    }

    const ConfigHandle bocConf(const Pp0LocationBase &pp0_location, Revision_t revision) const  {
      const PixLib::RodBocConnectivity *rod_boc_conn=rodBocConnectivity(pp0_location);
      return const_cast<DbServerConnectivityWrapper *>(this)->bocConfHandle(rod_boc_conn,revision);
    }


    ConfigHandle rodConf(Pp0List::iterator &pp0_iter, Revision_t revision) {
      const PixLib::RodBocConnectivity *rod_boc_conn=rodBocConnectivity(pp0_iter);
      return rodConfHandle(rod_boc_conn,revision);
    }

    const ConfigHandle rodConf(const Pp0List::const_iterator &pp0_iter, Revision_t revision) const {
      const PixLib::RodBocConnectivity *rod_boc_conn=rodBocConnectivity(pp0_iter);
      return const_cast<DbServerConnectivityWrapper *>(this)->rodConfHandle(rod_boc_conn,revision);
    }

    ConfigHandle rodConf(const Pp0LocationBase &pp0_location, Revision_t revision) {
      const PixLib::RodBocConnectivity *rod_boc_conn=rodBocConnectivity(pp0_location);
      return rodConfHandle(rod_boc_conn,revision);
    }

    const ConfigHandle rodConf(const Pp0LocationBase &pp0_location, Revision_t revision) const {
      const PixLib::RodBocConnectivity *rod_boc_conn=rodBocConnectivity(pp0_location);
      return const_cast<DbServerConnectivityWrapper *>(this)->rodConfHandle(rod_boc_conn,revision);
    }

    std::shared_ptr<PixLib::PixDisable> getDisable(std::string disableName, Revision_t revision);

    void writeDisable(const std::shared_ptr<PixLib::PixDisable> &disable, bool cache);


  static void getObjectTagsFromDbServer(PixLib::PixDbServerInterface &db_server,
					std::vector<std::string> *object_tags);

  static void getConfigurationObjectTagsFromDbServer(PixLib::PixDbServerInterface &db_server,
						     std::vector<std::string> *object_tag_out);

  static void getConnectivityTagsFromDbServer(PixLib::PixDbServerInterface &db_server,
					      const std::string &id_name,
					      std::vector<std::string> *conn_tag_out,
					      std::vector<std::string> *alias_tag_out,
					      std::vector<std::string> *payload_tag_out,
					      std::vector<std::string> *configuration_tag_out);

    static const std::string &connectivityDomainName() { return s_connectivityDomainName; }

  protected:

    IConfig *configFromConnName(EConfigType type, const std::string &connectivity_name, const std::string &production_name, Revision_t revision) {
      //      if (m_dbServer.get()) {
	// When the db server is used then the connectivity cannot be used 
      std::stringstream message;
      message << "FATAL [DbServerConnectivityWrapper::modConf] When the db server is used module, BOC configurations etc. cannot be accessed.";
      throw PixA::ConfigException(message.str());
      //  }
    //       else {
    // 	return config(type, (production_name.empty() ? connectivity_name : production_name), 
    // 		      (revision > 0 ? 
    // 		       const_cast<PixLib::PixConnectivity *>(m_connectivity)->getCfgName(connectivity_name,revision)
    // 		       : const_cast<PixLib::PixConnectivity *>(m_connectivity)->getCfgName(connectivity_name )),
    // 		      revision);
    //       }
    }


  private:

    ConfigHandle modConfHandle(const ModuleLocationBase &module_location, Revision_t revision) {
      return modConfHandle( moduleConn(module_location), revision );
    }

//    const ConfigHandle modConfHandle(const ModuleLocationBase &module_location, Revision_t revision) const {
//      return modConfHandle( moduleConn(module_location), revision );
//    }

    ConfigHandle modConfHandle(const ModuleList::const_iterator &module_iter, Revision_t revision){
      return modConfHandle(*module_iter,revision);
    }

//    const ConfigHandle modConfHandle(const ModuleList::const_iterator &module_iter, Revision_t revision) const {
//      return modConfHandle(const_cast<PixLib::ModuleConnectivity *>(*module_iter), revision);
//    }

    ConfigHandle modConfHandle(const PixLib::ModuleConnectivity *mod, Revision_t revision);

    ConfigHandle bocConfHandle(const PixLib::RodBocConnectivity *rod_boc, Revision_t revision);

    ConfigHandle rodConfHandle(const PixLib::RodBocConnectivity *rod_boc, Revision_t revision);


    //    Revision_t writeModConf(const PixLib::ModuleConnectivity *mod_conn, Revision_t revision) const;



    std::string configurationDomainName() const {
      return s_configurationDomainName + m_connectivity->getConnTagOI();
    }

    std::shared_ptr<PixLib::PixDbServerInterface> m_dbServer;

    template <class T> ObjectCache_t<T> &cache();
    template <class T> std::shared_ptr<T> createObject(const std::string &name);
    template <class T_CompoundObj_t, class T_Obj_t> IConfig *objectConfig(const std::string &name, Revision_t revision);

    ObjectCache_t<std::shared_ptr<PixLib::PixModule> >    m_moduleCache;
    ObjectCache_t<std::shared_ptr<PixLib::PixModuleGroup> >       m_rodCache;

    static std::string s_connectivityDomainName;
    static std::string s_configurationDomainName;
  };

  template <> inline ObjectCache_t<std::shared_ptr<PixLib::PixModule> > &
    DbServerConnectivityWrapper::cache<std::shared_ptr<PixLib::PixModule> >() { return m_moduleCache; }
  template <> inline ObjectCache_t<std::shared_ptr<PixLib::PixModuleGroup> > &
    DbServerConnectivityWrapper::cache<std::shared_ptr<PixLib::PixModuleGroup> >()    { return m_rodCache; }

}

#endif
