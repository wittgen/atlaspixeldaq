#ifndef _ConfGrpRef_h_
#define _ConfGrpRef_h_

#include "IConfGrp.h"

namespace PixLib {
  class ConfObj;
}

namespace PixA {

  class ConfigRef;
  class MutbaleConfigRef;

  class ConfGrpRef
  {
    friend class ConfigRef;
    friend class MutableConfigRef;
  protected:
    //    ConfGrpRef(IConfGrp *conf_group) : m_confGrp(conf_group) { m_confGrp->use(); }
    ConfGrpRef(IConfGrp &conf_group) : m_confGrp(&conf_group) { m_confGrp->use(); }
  public:
    ConfGrpRef(const ConfGrpRef &conf_group) : m_confGrp(conf_group.m_confGrp) {m_confGrp->use(); }

    virtual ~ConfGrpRef() { m_confGrp->done();}

    ConfGrpRef &operator=(ConfGrpRef &a) {
      m_confGrp->done();
      m_confGrp=a.m_confGrp;
      m_confGrp->use();
      return *this;
    }

    virtual const PixLib::ConfObj &operator[](const std::string &name) const  { return (*(const_cast<IConfGrp *>(m_confGrp)))[name];}
    virtual PixLib::ConfObj &operator[](const std::string &name)  { return const_cast<PixLib::ConfObj &>((*m_confGrp)[name]);}

  private:
    IConfGrp *m_confGrp;
  };

}

#endif
