#include "ConfigRoot.h"

namespace PixA {

  ObjStat_t ConfigRoot::s_stat("ConfigRoot");

  ConfigRoot::~ConfigRoot() {
    assert(m_users==0);
    s_stat.dtor();
  }
}

