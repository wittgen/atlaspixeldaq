#ifndef _PixConfGrpWrapper_h_
#define _PixConfGrpWrapper_h_

#include <memory>
#include <string>
#include <sstream>
#include <map>
#include <vector>
#include <stdexcept>

#include <Config/ConfObj.h>
#include <Config/ConfGroup.h>
#include "IConfGrp.h"
#include "PixDbAccessor.h"
#include "DbWrapper.h"
#include "BlockWiseStorage.h"
#include "pixa_exception.h"

#include "PixConfigWrapper.h"

#ifdef DEBUG
#  include "ObjList.h"
#endif
namespace PixA {

class PixConfGrpWrapper : public IConfGrp
{
  friend class PixConfigWrapper;

protected:

  PixConfGrpWrapper(PixConfigWrapper &parent_config, PixLib::ConfGroup &conf_grp) : IConfGrp(parent_config), m_confGrp(&conf_grp) 
    { 
#   ifdef DEBUG 
      s_objList.add(this);
#   endif
    }

 public:
  ~PixConfGrpWrapper() 
    {
#   ifdef DEBUG 
      s_objList.remove(this); 
#   endif
    }

  PixLib::ConfObj &operator[](const std::string &name) {
    PixLib::ConfObj &obj=(*m_confGrp)[name];
    if (obj.type()==PixLib::ConfObj::VOID) {
	std::stringstream message;
	message << "FATAL [PixConfGrpWrapper::operator[]] No config object of name = " << name
		<< " in ConfGrp " << m_confGrp->name() << "."  << std::endl;
	throw ConfigException(message.str());
    }
    return obj;
  }

  void use()  { IConfigBase::useNode(); }
  void done() {IConfigBase::doneWithNode(); if (IConfigBase::use_count()==0) delete this; }

  void createAll() {}
    
 private:

  PixLib::ConfGroup   *m_confGrp;

#ifdef DEBUG 
  static ObjList s_objList;
 public: 
  static void stat() {s_objList.stat();}
#endif
};

}
#endif
