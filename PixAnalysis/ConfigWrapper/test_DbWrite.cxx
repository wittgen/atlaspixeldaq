#include <Config/Config.h>
#include <PixDbInterface/PixDBException.h>

#include <pixa_exception.h>

#include "DbManager.h"

class baseTest {
public:
  baseTest(const std::string &name) : m_config(new PixLib::Config(name)) {
    m_config->addGroup("General");
    (*m_config)["General"].addInt("Eins",m_eins,(name=="B" ? 5 : 1),"Eins",true);
    (*m_config)["General"].addFloat("Zwei",m_zwei,(name=="B" ? 6. : 2.),"Zwei",true);
    (*m_config)["General"].addString("Drei",m_drei,(name=="B" ? std::string("DREI") : std::string("drei")),"Drei",true);

    (*m_config).addGroup("Opt");
    (*m_config)["Opt"].addInt("Eins",m_einsOpt,(name=="B" ? -5 : -1),"Optional eins",true);
    (*m_config)["Opt"].addFloat("Zwei",m_zweiOpt,(name=="B" ? -6. : -2.),"Optional zwei",true);
    (*m_config)["Opt"].addString("Drei",m_dreiOpt,(name=="B" ? std::string("-DREI") : std::string("-drei")),"Optional drei",true);
    m_config->reset();
  }

  PixLib::Config &config() {return *m_config;}

private:
  int         m_eins;
  float       m_zwei;
  std::string m_drei;

  int         m_einsOpt;
  float       m_zweiOpt;
  std::string m_dreiOpt;

  PixLib::Config *m_config;
};


int main(int argc, char **argv)
{
  if (argc<=1)  return -1;

  std::string file_name=argv[1];

  try {
    baseTest a("A");
    baseTest b("B");
    PixA::DbHandle a_db_handle = PixA::DbManager::dbHandle(file_name,false);

    if (a_db_handle) {
      a.config().write( a_db_handle.ref().addRecord("baseTest","A").get() );
    }
    else {
      std::cout << "ERROR [main] Invalid handle for file  : " << file_name << std::endl;
      return -1;
    }
    PixA::DbHandle b_db_handle = PixA::DbManager::dbHandle(file_name);
    bool exception = false;
    try {
      b.config().write( b_db_handle.ref().addRecord("baseTest","B").get() );
    }
    catch (PixLib::PixDBException &err) {
      std::cout << "INFO [main] Caught expected PixDBExcpetion : " << err << std::endl;
      exception=true;
    }
    if (!exception) {
      std::cout << "ERROR [main] Writing to a file which is supposed to be open in read-only mode, did not cause an exception." << std::endl;
      return -1;
    }

    b.config().write( b_db_handle.writeHandle().ref().addRecord("baseTest","B").get() );

    {
      PixA::DbRef a_ref=a_db_handle.ref();
      PixLib::dbRecordIterator record_iter = a_ref.findRecord("B");
      if ( record_iter == a_ref.recordEnd()) {
	std::cout << "ERROR [main] Could not find the record of baseTest / B." << std::endl;
	return -1;
      }
    }

    {
      PixA::DbRef b_ref=b_db_handle.ref();
      PixLib::dbRecordIterator record_iter = b_ref.findRecord("A");
      if (record_iter == b_ref.recordEnd()) {
	std::cout << "ERROR [main] Could not find the record of baseTest / A." << std::endl;
	return -1;
      }
    }
    
  }
  catch (PixA::ConfigException &err) {
    std::cout << "ERROR [main] Caught ConfigExcpetion : " << err << std::endl;
    return -1;
  }
  catch (PixLib::PixDBException &err) {
    std::cout << "ERROR [main] Caught PixDBExcpetion : " << err << std::endl;
    return -1;
  }
  catch (std::exception &err) {
    std::cout << "ERROR [main] Caught exception : " << err.what() << std::endl;
    return -1;
  }
  return 0;

}
