#ifndef _ConnectivityManager_h_
#define _ConnectivityManager_h_

#include <string>
#include <map>

#include "Connectivity.h"

#include <memory>

class STControlEngine;
class STPixModuleGroup;

namespace PixLib {
   class PixDbServerInterface;
}

namespace PixA {

  /** Class to obtain a limited interface to the pix connectivity.
   * The created connectivity interfaces are only deleted if cleanup
   * is called from time to time.
   */
  class ConnectivityManager
  {
    friend class STControlEngine;
    friend class STPixModuleGroup;

  private:
    ConnectivityManager();

    IConnectivity *connectivity(const std::string &object_tag,
				const std::string &tagC,
				const std::string &tagP,
				const std::string &tagA,
				const std::string &tagCF,
				const std::string &tagModCF);
  public:

    /** Get a reference to a connectivity interface for the given set of tags and for the given revision of the configurations.
     */
    ConnectivityRef connectivity(const std::string &tagC,
				 const std::string &tagP,
				 const std::string &tagA,
				 const std::string &tagCF,
				 Revision_t revision) {
      return ConnectivityRef(connectivity("CT",tagC,tagP,tagA,"Base", s_empty),
			     connObjConfigDb().getTag("CT",tagCF,revision));
    }

    /** Get a reference to a connectivity interface for the given set of tags and for the given revision of the configurations.
     */
    ConnectivityRef connectivity(const std::string &object_tag,
				 const std::string &tagC,
				 const std::string &tagP,
				 const std::string &tagA,
				 const std::string &tagCF,
				 Revision_t revision) {
      return ConnectivityRef(connectivity(object_tag,tagC,tagP,tagA,"Base", s_empty), 
			     connObjConfigDb().getTag(object_tag,tagCF,revision));
    }

    /** Get a reference to a connectivity interface for the given set of tags and for the given revision of the configurations.
     */
    ConnectivityRef connectivity(const std::string &object_tag,
				 const std::string &tagC,
				 const std::string &tagP,
				 const std::string &tagA,
				 const std::string &tagCF,
				 const std::string &tagModCF,
				 Revision_t revision) {
      return ConnectivityRef(connectivity(object_tag, tagC,tagP,tagA,"Base", s_empty ), 
			     connObjConfigDb().getTag(object_tag,tagCF,revision),
			     connObjConfigDb().getTag(object_tag,tagModCF,revision));
    }


    /** Get lists of available connectivity object tags.
     * @param object_tags vector which will be filled with the connectiivity object tags;
     */
    void getObjectTags(std::vector<std::string> *object_tags, bool db_server_only=false);

    /** Get lists of available configuration object tags.
     * @param object_tags_out vector which will be filled with the configuration object tags;
     */
    void getConfigurationObjectTags(std::vector<std::string> *object_tags, bool db_server_only=false);

    /** Get lists of available connectivity tags.
     * @param conn_tag_out vector which will be filled with the connectiivity tags;
     * @param alias_tag_out vector which will be filled with the alias tags;
     * @param payload_tag_out vector which will be filled with the payload tags;
     * @param configuration_tag_out vector which will be filled with the configuration tags;
     */
    void getConnectivityTags(const std::string &id_name,
			     std::vector<std::string> *conn_tag_out,
			     std::vector<std::string> *alias_tag_out,
			     std::vector<std::string> *payload_tag_out,
			     std::vector<std::string> *configuration_tag_out,
			     bool db_server_only=false);


  protected:
    /** Hand over the management of the connectivity to the ConnectivityManager.
     * The returned reference must be kept until the connectivity is not used anymore.
     * Otherwise the connectivity may be deleted by the Connectivity Manager.
     * A connectivity handed to the connectivity manager must not be modified by the
     * caller.
     */
    ConnectivityRef giveConnectivity(PixLib::PixConnectivity *conn);

    /** Take the owenership of a connectivity back from the Connectivity Manager.
     * If the request failed a NULL pointer is returned.
     */
    PixLib::PixConnectivity *takeConnectivity(ConnectivityRef &conn);

    /** Give the connectivity Manager the permission of offer module configurations. 
     * The module configurations are still owned by the caller. The caller must 
     * not delete the module but use @ref canDeleteModule.
     */
    void announceModuleConfiguration(const ConnectivityRef &conn, PixLib::PixModule *module);

    /** Give the connectivity manger the ownership over the given module.
     * If the given module is not in use it will be deleted.
     */
    void canDeleteModule(const ConnectivityRef &conn, PixLib::PixModule *module);

    void setDbServer(const std::shared_ptr<PixLib::PixDbServerInterface> &db_server) {
      m_dbServer=db_server;
    }

    void setConnObjConfigDb(const ConnObjConfigDb &conn_obj_config_db) {
      m_connObjConfigDb=conn_obj_config_db;
    }

  public:

    ~ConnectivityManager();

    /** Convenience method to get a connectivity interface for the given tag and revision.
     */
    static ConnectivityRef getConnectivity(const std::string &object_tag,
					   const std::string &tagC,
					   const std::string &tagP,
					   const std::string &tagA,
					   const std::string &tagCF,
					   Revision_t revision)
    {
      return instance()->connectivity(object_tag, tagC,tagP,tagA,tagCF, revision);
    }

    /** Convenience method to get a connectivity interface for the given tag and revision.
     */
    static ConnectivityRef getConnectivity(const std::string &tagC,
					   const std::string &tagP,
					   const std::string &tagA,
					   const std::string &tagCF,
					   Revision_t revision) {
      return instance()->connectivity(tagC,tagP,tagA,tagCF,revision);
    }

    /** Convenience method to get a connectivity interface for the given tag and revision.
     */
    static ConnectivityRef getConnectivity(const std::string &object_tag,
					   const std::string &tagC,
					   const std::string &tagP,
					   const std::string &tagA,
					   const std::string &tagCF,
					   const std::string &tagModCF,
					   Revision_t revision) {
      if (tagCF==tagModCF || tagModCF.empty()) {
	return instance()->connectivity(object_tag, tagC,tagP,tagA,tagCF,revision);
      }
      else {
	return instance()->connectivity(object_tag, tagC,tagP,tagA,tagCF,tagModCF, revision);
      }
    }

    static void useDbServer(std::shared_ptr<PixLib::PixDbServerInterface> &db_server);

    /** Get the only instance of the connectivity manager.
     */
    static ConnectivityManager *instance()  {
      if (!s_instance) s_instance = new ConnectivityManager;
      return s_instance;
    }

    /** Delete unused resources of the managed connectivity databases.
     * Resources are : the pixconnectivity and configurations.
     */
    bool cleanup();

    /** Get the only instance of the connectivity manager.
     */
    static void deepCleanup()  {
      if (s_instance) {
	instance()->cleanup();
	if (instance()->m_connectivityList.empty()) {
	  delete s_instance;
	  s_instance=NULL;
	}
      }
    }


  private:
    ConnObjConfigDb &connObjConfigDb() { return m_connObjConfigDb; }

    typedef std::map< std::string, IConnectivity * > ConnectivityList_t;

    ConnectivityList_t m_connectivityList;
    std::shared_ptr<PixLib::PixDbServerInterface> m_dbServer;
    ConnObjConfigDb                                 m_connObjConfigDb;

    static const std::string s_empty;
    static ConnectivityManager *s_instance;
  };

}

#endif
