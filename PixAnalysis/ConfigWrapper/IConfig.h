#ifndef _IConfig_h_
#define _IConfig_h_

#include "IConfigBase.h"
#include "pixa_exception.h"
#include <string>

namespace PixLib {
  class Config;
}

namespace PixA {

  class IConfGrp;

  class IConfig : public IConfigBase
  {
  protected:
    IConfig(ConfigRoot &root) : IConfigBase(root) { debug(true);}
    IConfig(IConfig &config) : IConfigBase(config) {debug(true);}
    IConfig() : IConfigBase(this) {debug(true);}

  public:

    virtual ~IConfig() {debug(false);}

    virtual void use()  = 0;
    virtual void done() = 0;

    /** debug method.
     */
    bool isRoot() const {return IConfigBase::isRoot(this);}


    //  virtual const IConfGrp &operator[](const std::string &name) throw (ConfigException) const = 0;
    virtual IConfGrp &operator[](const std::string &name)  = 0;
    //  virtual const IConfig &subConfig(const std::string &name) const throw (ConfigException) = 0;
    virtual IConfig &subConfig(const std::string &name)  = 0;

    virtual IConfig &createAll()  = 0;

    /** Create the whole config and return a reference to it.
     */
    virtual PixLib::Config &config() = 0;

    void debug(bool) {
      //      std::cout << "INFO [OnDemandConfig::" << (init ? "ctor" : "dtor" ) << "] "
      //		<< (init ? "construct" : "delete") 
      //		<< " OnDemandConfig "  << static_cast<void *>(this) << std::endl;
    }

  };

  inline  void ConfigRoot::done() {
    assert(m_users>0 && m_users < 10000);
    if (--m_users == 0) {
//      std::cout << "INFO [ConfigBase::done] will delete config interface = " <<  static_cast<void *>(m_config) <<"  users = " << m_users << std::endl;
      delete m_config;
      delete this;
    }
//    else {
//      std::cout << "INFO [ConfigBase::done] config interface = " <<  static_cast<void *>(m_config) <<"  users = " << m_users << std::endl;
//    }
  }

}

#endif
