#include "PixConnectivityWrapper.h"
#include "RootDbOpenHelper.h"
#include <PixModule/PixModule.h>
#include "DbManager.h"
#include "DelayedOnDemandConfig.h"

//#include <RelationalAccess/AuthenticationServiceException.h>


#include "ObjectCache_t_impl.h"


namespace PixA {

    const char *PixConnectivityWrapper::s_disableTypeName="DISABLE";
    const char *PixConnectivityWrapper::s_typeNames[PixConnectivityWrapper::kNConfTypes]={
         "ROD",
         "BOC", 
         "MODULE"
    };

    PixConnectivityWrapper::PixConnectivityWrapper(const std::string &object_tag,
						   const std::string &tagC,
						   const std::string &tagP,
						   const std::string &tagA,
						   const std::string &tagCF,
						   const std::string &tagModCF)
    {
      adjustCache(); 
      std::unique_ptr<PixLib::PixConnectivity> a_conn;
      for (unsigned int retry=0; ; retry++) {
	try {
// 	  std::cout << "INFO [PixConnectivityWrapper::ctor] open connectivty data base for tags "
// 		    << tagC <<", " 
// 		    << tagP <<", "
// 		    << tagA <<", "
// 		    << tagCF 
// 		    << " attempt " << retry 
// 		    << std::endl;
	  a_conn = std::make_unique<PixLib::PixConnectivity>(tagC,tagP,tagA, tagCF, object_tag, tagModCF);
	}
	//	catch (coral::AuthenticationServiceException &err) {
	catch (std::exception &err) {
	  if (retry>=5) {
	    std::stringstream message;
	    message << "FATAL [PixConnectivityWrapper::ctor] Failed to create connectivty data base for tags "
		    << object_tag << ":"
		    << tagC <<", "
		    << tagP <<", "
		    << tagA <<"; "
		    << tagCF;
	    if (!tagModCF.empty() && tagModCF != tagCF ) {
	      message << ", " << tagModCF;
	    }
	    message << "       with error : " << err.what();

	    throw PixA::ConfigException(message.str());
	  }
	  // sleep 300ms
	  usleep(300000);
	  continue;
	}
	m_connectivity = a_conn.release();
	break;
      }
      //      std::cout << "INFO [PixConnectivityWrapper::ctor] got connectivity " << static_cast<void *>(m_connectivity) << std::endl;
      if (m_connectivity) {
	m_connectivity->loadConn();
	
// 	std::vector< std::pair<std::string,std::map<std::string, void*> *> > map_list;

// 	map_list.push_back(std::make_pair(std::string("parts"),reinterpret_cast<std::map<std::string,void *> *>(&m_connectivity->parts)));
// 	map_list.push_back(std::make_pair(std::string("crates"),reinterpret_cast<std::map<std::string,void *> *>(&m_connectivity->crates)));
// 	map_list.push_back(std::make_pair(std::string("rods"),reinterpret_cast<std::map<std::string,void *> *>(&m_connectivity->rods)));
// 	map_list.push_back(std::make_pair(std::string("tims"),reinterpret_cast<std::map<std::string,void *> *>(&m_connectivity->tims)));
// 	map_list.push_back(std::make_pair(std::string("sbcs"),reinterpret_cast<std::map<std::string,void *> *>(&m_connectivity->sbcs)));
// 	map_list.push_back(std::make_pair(std::string("obs"),reinterpret_cast<std::map<std::string,void *> *>(&m_connectivity->obs)));
// 	map_list.push_back(std::make_pair(std::string("pp0s"),reinterpret_cast<std::map<std::string,void *> *>(&m_connectivity->pp0s)));
// 	map_list.push_back(std::make_pair(std::string("mods"),reinterpret_cast<std::map<std::string,void *> *>(&m_connectivity->mods)));
// 	map_list.push_back(std::make_pair(std::string("obmaps"),reinterpret_cast<std::map<std::string,void *> *>(&m_connectivity->obmaps)));
// 	map_list.push_back(std::make_pair(std::string("bocmaps"),reinterpret_cast<std::map<std::string,void *> *>(&m_connectivity->bocmaps)));

// 	for (std::vector< std::pair<std::string,std::map<std::string, void *> *> >::const_iterator iter=map_list.begin();
// 	     iter!=map_list.end();
// 	     iter++) {
// 	  std::cout << iter->first << " elements = " << iter->second->size() << std::endl;
// 	  unsigned int i=0;
// 	  for (std::map<std::string, void *>::const_iterator elm_iter=iter->second->begin();
// 	       elm_iter!= iter->second->end();
// 	       elm_iter++, i++) {
// 	    std::cout << i << " : " << elm_iter->first << std::endl;
// 	  }
// 	  std::cout << std::endl;
// 	}

// 	std::cout << " with rods : " << std::endl;
//       	for (std::map<std::string, PixLib::RodBocConnectivity*>::const_iterator rod_iter = m_connectivity->rods.begin();
// 	     rod_iter != m_connectivity->rods.end();
// 	     rod_iter++) {
// 	  std::cout << rod_iter->first << std::endl;
// 	}
      }

    }

  void PixConnectivityWrapper::adjustCache() {
    for (unsigned int conf_type_i=0; conf_type_i<kNConfTypes; conf_type_i++) {
      m_configList[conf_type_i].setMaxObjects(100);
    }
    m_disableList.setMaxObjects(100);
  }


  DbWrapper *PixConnectivityWrapper::db(std::string full_name) const {
      std::cout<<"pixconnwrapper getwrap"<<std::endl;
    return DbManager::instance()->getDbWrapper(full_name);
  }

  ConfigHandle PixConnectivityWrapper::delayedConfig(Revision_t revision,
						     PixConnectivityWrapper::EConfigType type,
						     const std::string &connectivity_name,
						     const std::string &production_name) {
    return *(new DelayedOnDemandConfig(*this,revision,type,connectivity_name,production_name));
  }


  class ConfigExtractor : public ConfigHandle
  {
  public:
    ConfigExtractor(const ConfigHandle &a_handle) : ConfigHandle(a_handle) {}
    IConfig *configInterface() {return m_config;}
  };

  IConfig *PixConnectivityWrapper::config(EConfigType type, const std::string &name, const std::string &folder_name, Revision_t revision) {
    unsigned int cfg_revision;
    unsigned int cfg_rep; //?
    std::string cfg_name;

    if (revision==0) {
	revision--;
    }

    assert( type < kNConfTypes );
    const_cast<PixLib::PixConnectivity *>(m_connectivity)->getCfgName(name, revision, cfg_name, cfg_revision, cfg_rep, "" /*s_typeNames[type]*/ );

    {
      ConfigHandle *config_handle_ptr = m_configList[type].object(name, cfg_revision);
      // search in cache;
      if (config_handle_ptr) {
	return ConfigExtractor(*config_handle_ptr).configInterface();
      }
    }

    {
      std::string::size_type pos=cfg_name.rfind('.');
      if (pos != std::string::npos) {
	if (cfg_name.compare(pos,pos+4,".sql")==0) {
	  cfg_name="sqlite_file:"+cfg_name;
	}
      }
    }

    DbWrapper *db_wrapper = db(cfg_name);
    std::vector<std::string> record_hierarchy;
    std::string record_name( folder_name.empty() ? name : folder_name);
    record_hierarchy.push_back(record_name);
    switch (type) {
    case kRodConf:
      record_hierarchy.push_back(record_name+"_ROD");
      break;
    case kBocConf:
      record_hierarchy.push_back(record_name+"_BOC");
      break;
    default:
      break;
    }

    //     std::cout << "INFO [PixConnectivityWrapper::modConf] record hierarchy for " << name << "(" << link_name << ")" ;
    //     for (std::vector<std::string>::const_iterator record_iter = record_hierarchy.begin();
    // 	 record_iter != record_hierarchy.end();
    // 	 record_iter++) {
    //       std::cout << *record_iter << " / ";
    //     }
    //     std::cout << std::endl;

    try {
        std::cout<<"si sono io"<<std::endl;
      IConfig *a_config = new OnDemandConfig(*db_wrapper,record_hierarchy);
      m_configList[type].add(name, cfg_revision, ConfigHandle(*a_config ));

      return a_config;
    }
    catch (PixLib::PixDBException &err) {
      throw PixA::ConfigException(err.getDescriptor());
    }
  }

  PixConnectivityWrapper::~PixConnectivityWrapper() {}


  /** Original version: works only without DbServer. Paolo will write a method to get revision both with and without DbServer. 
      For the moment we do it the other way around, that is stupid, but should not crash (Lucia, 24.04.08) */
  // Unfortunately this new version basically renders the caching useless. The revision is basically a timestamp
  // which is not related to the creation time of the object. So basically which each query a new revision will
  // be asked for. In order to make the caching work one has to get the "creation time" of the object. Up to
  // now the only possibility was to extract the creation time from the file name.
  // For the time being only those disable objects which are created per scan are queried, so the revision time
  // will be always the same (start time of the scan).

  //   std::shared_ptr<PixLib::PixDisable> PixConnectivityWrapper::getDisable(std::string disableName, Revision_t revision) {
  //     std::string disable_cfg_name = (revision > 0 ? 
  // 				    const_cast<PixLib::PixConnectivity *>(m_connectivity)->getCfgName(disableName,revision,"DISABLE")
  // 				    : const_cast<PixLib::PixConnectivity *>(m_connectivity)->getCfgName(disableName,"DISABLE") );

  //     //    std::cout << "Disable file name: " << disable_cfg_name << std::endl;
  
  //     std::string::size_type pos=disable_cfg_name.rfind('.');
  //     if (pos != std::string::npos) {
  //       if (disable_cfg_name.compare(pos,pos+4,".sql")==0) {
  // 	disable_cfg_name ="sqlite_file:" + disable_cfg_name;
  //       }
  //     }
  
  
  //     std::shared_ptr<PixLib::PixDisable> a_pix_disable;
  //     Revision_t revision_id;
  //     if (!disable_cfg_name.empty()) {
  //       revision_id = extractRevision(disable_cfg_name);
  //     }
  //     else {
  //       revision_id=1;
  //     }
  
  //     //    std::cout << "Revision: " << revision_id << std::endl;
  
  //     DisableList_t::const_iterator disable_iter = m_disableList.find(disableName);
  //     if (disable_iter != m_disableList.end()) {
  //       //      std::cout << "Found disable object" << std::endl;
  //       DisableRevisionList_t::const_iterator rev_iter = disable_iter->second.find(revision_id);
  //       if (rev_iter != disable_iter->second.end()) {
  // 	//	std::cout << "Found revision" << std::endl;
  // 	return rev_iter->second.obj();
  //       }
  //       //      std::cout << "Revision missing" << std::endl;
  //     }
  //     //    std::cout << "Object missing" << std::endl;
  
  //     // if a pix disable does not exist, store a NULL pointer
  //     if (!disable_cfg_name.empty()) {
  //       a_pix_disable = std::shared_ptr<PixLib::PixDisable>(m_connectivity->getDisable(disableName));
  //     }
  
  //     DisableRevisionList_t &dis_rev_list = m_disableList[disableName];
  //     std::pair<DisableRevisionList_t::const_iterator, bool> ret = 
  //       dis_rev_list.insert(std::make_pair(revision_id, a_pix_disable));
  
  //     if(!ret.second) {
  //       std::stringstream message;
  //       message << "FATAL [PixConnectivityWrapper::getDisable] Failed to insert disable " 
  // 	      << disableName 
  // 	      << " v" << revision_id << " into internal disable list (internal error).";
  //       throw PixA::ConfigException(message.str());
  //     }
  //     else {
  //       //      std::cout << "Inserted " << disableName << " v" << revision_id << " into internal disable list" << std::endl;
  //     }
  //     return ret.first->second.obj();
  //   }

  /** Modified (temporary) version to be used also with DbServer, see comment on original version */

  std::shared_ptr<PixLib::PixDisable> PixConnectivityWrapper::getDisable(std::string disableName, Revision_t revision) {

    unsigned int cfg_revision;
    unsigned int cfg_rep; //?
    std::string cfg_name;
    if (revision==0) {
	revision--;
    }
    const_cast<PixLib::PixConnectivity *>(m_connectivity)->getCfgName(disableName, revision, cfg_name, cfg_revision, cfg_rep, s_disableTypeName );

    {
      std::shared_ptr<PixLib::PixDisable> *disable_ptr = m_disableList.object(disableName, cfg_revision);
      // search in cache;
      if (!disable_ptr) {
	return *disable_ptr;
      }

    }

    {
      std::string::size_type pos=cfg_name.rfind('.');
      if (pos != std::string::npos) {
	if (cfg_name.compare(pos,pos+4,".sql")==0) {
	  cfg_name="sqlite_file:"+cfg_name;
	}
      }
    }

    DbWrapper *db_wrapper = db(cfg_name);

    try {
      std::vector<std::string> record_hierarchy;
      record_hierarchy.push_back(disableName);
      std::shared_ptr<PixLib::DbRecord> obj_record( db_wrapper->record(record_hierarchy) );
      if (!obj_record.get()) {
	std::stringstream message;
	message << "FATAL [PixConnectivityWrapper::getDisable] No configuration in Db for " << disableName << std::endl;
	throw PixA::ConfigException(message.str() );
      }
      else {
	std::shared_ptr<PixLib::PixDisable> disable(new PixDisable(disableName));
	disable->readConfig(obj_record.get());
	m_disableList.add(disableName, cfg_revision, disable);
	return disable;
      }

    }
    catch (PixLib::PixDBException &err) {
      throw PixA::ConfigException(err.getDescriptor());
    }

    return std::shared_ptr<PixLib::PixDisable>();
  }

  void PixConnectivityWrapper::writeDisable(PixLib::PixDisable &disable) {
    disable.writeConfig(const_cast<PixLib::PixConnectivity *>(m_connectivity));
  }

//   void PixConnectivityWrapper::writeDisableToDbServer(PixLib::PixDisable &disable, std::string pendTag) {
//     std::string used_pendTag;
//     if (pendTag == "") { // default is permanent object
//       used_pendTag = "ConnectivityWrapper";
//     }
//     else {
//       used_pendTag = pendTag;
//     }
//     if (m_dbServer.get() != NULL) {
//       disable.writeConfig(m_dbServer.get(), "Configuration-"+tag(kId), tag(kConfig), used_pendTag);
//     }
//     else {
//       std::stringstream message;
//       message << "FATAL [PixConnectivityWrapper::writeDisableToDbServer] DbServer not available ";
//       throw PixA::ConfigException(message.str());
//     }
//   }

//   Revision_t PixConnectivityWrapper::writeModConf(const PixLib::ModuleConnectivity *mod_conn, Revision_t revision) const {

//     if (revision != 0) {
//       // cannot change old configurations
//       // First, a new tag has to be created from the old revision 
//       // Then the revision can be set to the head i.e. it can be set to zero.
//       // Finally, the configuration can be saved.
//       //      return revision;
//     }

//     std::string module_cfg_name = (revision > 0 ? 
// 				     const_cast<PixLib::PixConnectivity *>(m_connectivity)->getCfgName(const_cast<PixLib::ModuleConnectivity *>(mod_conn)->name(),revision)
// 				   : const_cast<PixLib::PixConnectivity *>(m_connectivity)->getCfgName(const_cast<PixLib::ModuleConnectivity *>(mod_conn)->name()) );

//     {
//       std::string::size_type pos=module_cfg_name.rfind('.');
//       if (pos != std::string::npos) {
// 	if (module_cfg_name.compare(pos,pos+4,".sql")==0) {
// 	  module_cfg_name ="sqlite_file:" + module_cfg_name;
// 	}
//       }
//     }


//     Revision_t revision_id=extractRevision(module_cfg_name);

//     ModuleRevisionList_t::const_iterator rev_iter = mod_rev_list_iter->second.find(revision_id);
//     if (rev_iter == mod_rev_list_iter->second.end()) {
//       return revision;
//     }

//     PixLib::DbRecord *dest_record = m_connectivity->addCfg(const_cast<PixLib::ModuleConnectivity *>(mod_conn)->name());
//     if (!dest_record) {
//       std::stringstream message;
//       message << "FATAL [PixConnectivityWrapper::writeConfig] Failed to create new record for  module " 
// 	      << const_cast<PixLib::ModuleConnectivity *>(mod_conn)->prodId() << ".";
//       throw PixA::ConfigException(message.str());
//     }
//     rev_iter->second.first->saveConfig(dest_record);

//     std::string new_name=dest_record->getDecName();
//     std::string::size_type pos = new_name.rfind(":");
//     if (pos != std::string::npos) {
//       new_name.erase(pos);
//     }
//     Revision_t new_revision_id=time(NULL);

//     return new_revision_id;
//   }
  Revision_t PixConnectivityWrapper::extractRevision(const std::string &name) {
    if (name.empty()) {
      std::stringstream message;
      message << "FATAL [PixConnectivityWrapper::extractRevision] Config file name is empty. " 
	      "Looks like an error in the connectivity database.";
      throw PixA::ConfigException(message.str());
    }
    std::string::size_type pos = name.rfind('.');
    if (pos !=std::string::npos) {
      if (pos==0 || !isdigit(name[pos-1])) {
	std::stringstream message;
	message << "FATAL [PixConnectivityWrapper::extractRevision] Expected a file name of format xxxx_[unix time]000.[ext], but got" << name;
	throw PixA::ConfigException(message.str());
      }
      for (unsigned int counter=0;pos>0 && counter<3; pos--, counter++);

      std::string::size_type start_pos = name.rfind('_',pos);
      if (start_pos !=std::string::npos) {
	return std::atoi( std::string(name,start_pos+1,pos-start_pos-1).c_str() );
      }
    }
    std::stringstream message;
    message << "FATAL [PixConnectivityWrapper::extractRevision] Failed to extract revision from file name \"" << name 
	    << "\". Expected something like \"[...]_01234567890.aaaa\".";
    throw PixA::ConfigException(message.str());
  }


  template class ObjectCache_t<ConfigHandle>;

}
