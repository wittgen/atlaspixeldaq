#include "RxLinkInfo_t.h"
#include "ConfObjUtil.h"

namespace PixA {

  int RxLinkInfo_t::s_dtoLinkMap[2]={1,2};

  int RxLinkInfo_t::bocLinkRx(ERxLink link) const     {
      assert(link<=kDTO2);
      if (m_moduleConnectivity) {

	return const_cast<PixLib::ModuleConnectivity *>(m_moduleConnectivity)->bocLinkRx(s_dtoLinkMap[link]);
      }
      else {
	if (!m_moduleConfigHandle) {
	  throw PixA::ConfigException("FATAL [RxLinkInfo_t::bocLinkRx] Neiter are there RX link information from connectivity database, "
				      "nor is there a module configuration." );
	}
	PixA::ConfigRef module_config=m_moduleConfigHandle.ref();
	const PixA::ConfGrpRef &general_config = module_config["general"];
	return confVal<int>(( link==kDTO1 ? general_config["BocOutputLink1"] : general_config["BocOutputLink2"]));
      }
  }

  int RxLinkInfo_t::inOrderBocLinkRx(ERxLink link) const     {

    bool altlink=false;

    if (m_scanConfigHandle) {
      PixA::ConfigRef scan_config=m_scanConfigHandle.ref();
      try {
	ConfGrpRef general_config = scan_config["general"];
	altlink = confVal<unsigned int>(general_config["useAltModLinks"]);
      }
      catch (...){
	std::cout <<"INFO [RxLinkInfo_t::::inOrderBocLinkRx] useAltModLinks not saved. Assuming there is no swap"<< std::endl;
      }
    }

    if (altlink) {
      return bocLinkRx((link==kDTO2 ? kDTO2 : kDTO1));
    }
    else {
      return bocLinkRx(link);
    }
  }
}
