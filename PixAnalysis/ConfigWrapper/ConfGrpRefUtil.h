#ifndef _ConfGrpRefUtil_h_
#define _ConfGrpRefUtil_h_

#include <sstream>

#include "ConfObjUtil.h"
#include "ConfigRef.h"

namespace PixA {

  inline const PixLib::ConfObj &confElm(const ConfGrpRef &conf_group, const std::string &name, unsigned int index) {
    std::stringstream full_name;
    full_name << name << index;
    return conf_group[full_name.str()];
  }

  inline const ConfigRef subConfig(const PixA::ConfigRef &config, const std::string &name, unsigned int index) {
    std::stringstream full_name;
    full_name << name << index;
    return config.subConfig(full_name.str());
  }

}

#endif
