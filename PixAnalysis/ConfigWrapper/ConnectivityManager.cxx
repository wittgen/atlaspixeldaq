#include "ConnectivityManager.h"
#include "PixConnectivityWrapper.h"
#include "DbServerConnectivityWrapper.h"

namespace PixA {

  ConnectivityManager *ConnectivityManager::s_instance =0;

  const std::string ConnectivityManager::s_empty;

  std::string makeTagId(const std::string &object_tag,
			const std::string &tagC,
			const std::string &tagP,
			const std::string &tagA,
			const std::string &tagCF,
			const std::string &tagModCF)
  {
    std::string compound_tag(object_tag+":"+tagC);
    if (tagP != tagC || tagA != tagC ) {
      compound_tag += ",";
      compound_tag += tagA;
      compound_tag += ",";
      compound_tag += tagP;
    }
    if (tagCF != tagC || (tagModCF != tagC || tagModCF.empty())) {
      compound_tag += ";";
      compound_tag += tagCF;
      if (tagModCF != tagCF && !tagModCF.empty()) {
	compound_tag += ",";
	compound_tag += tagModCF;
      }
    }

    return compound_tag;
  }

  ConnectivityManager::ConnectivityManager() : m_connObjConfigDb(ConnObjConfigDb::sharedInstance()) {}

  IConnectivity *ConnectivityManager::connectivity(const std::string &object_tag,
						   const std::string &tagC,
						   const std::string &tagP,
						   const std::string &tagA,
						   const std::string &tagCF,
						   const std::string &tagModCF) {

    //    std::cout <<  << std::endl;

    std::string compound_tag=makeTagId(object_tag,tagC,tagP,tagA,tagCF, tagModCF);

    std::cout << "INFO [ConnectivityManager::connectivity] instance " << static_cast<void *>(this) << " search for \"" 
	      << compound_tag  << "\" in :"<< std::endl;

    ConnectivityList_t::iterator conn_iter = m_connectivityList.find(compound_tag);
    for (ConnectivityList_t::iterator iter=m_connectivityList.begin();
	 iter != m_connectivityList.end();
	 iter++) {
      std::cout << "   \"" << iter->first << "\"" << (iter->first == compound_tag ? " *" : "") << (conn_iter == iter ? " <-" : "") << std::endl;
    }

    if ( conn_iter == m_connectivityList.end() ) {
      std::pair<ConnectivityList_t::iterator,bool> ret;
      ret.first = m_connectivityList.end();

      std::cout << "INFO [ConnectivityManager::connectivity] create new Connectivity for \""
	      << compound_tag  << "."<< std::endl;

      PixConnectivityWrapperBase *conn_ptr=NULL;

      // first try the db server
      std::cout << "INFO [ConnectivityManager::connectivity] db server instance = " << static_cast<void *>(m_dbServer.get()) 
		<< " manager = " << static_cast<void *>(this) 
		<< std::endl;
      if (m_dbServer.get()) {

	try {
	  std::string domain_name ( DbServerConnectivityWrapper::connectivityDomainName() );
	  domain_name += object_tag;


	  std::vector<std::string> taglist;
	  m_dbServer->listTagsRem(domain_name,taglist);
	  //	  std::cout << " --- " << domain_name << std::endl;
	  // 	  for(std::vector<std::string>::const_iterator tag_iter = taglist.begin();
	  // 	      tag_iter != taglist.end();
	  // 	      tag_iter++) {
	  // 	    std::cout << *tag_iter << std::endl;
	  // 	  }

	  std::vector<std::string>::const_iterator tag_iter = std::find(taglist.begin(),taglist.end(),tagC);

	  if (tag_iter != taglist.end()) {
	    conn_ptr = new DbServerConnectivityWrapper(m_dbServer, object_tag, tagC,tagP,tagA,tagCF, tagModCF);
	  }

	}
	catch (CORBA::TRANSIENT& err) {
	}
	catch (CORBA::COMM_FAILURE &err) {
	}
	catch (std::exception &err) {
	  std::cerr << "INFORMATION [ConnectivityManager::ctor] Caught standard exception while trying to create a connectivity through the PixDbServer : "
		    << err.what() << std::endl;
	}
	catch (...) {
	}
	if (!conn_ptr) {
	  std::cerr << "INFORMATION [ConnectivityManager::ctor] Failed to create connectivity through the PixDbServer will now try ORACLE." 
		    << std::endl;
	}
      }

      if (!conn_ptr ) {
	conn_ptr = new PixConnectivityWrapper(object_tag, tagC,tagP,tagA,tagCF, tagModCF);
      }

      ret =m_connectivityList.insert(std::make_pair(compound_tag,conn_ptr));

      if (!ret.second) {
	delete conn_ptr;
	std::stringstream message;
	message << "FATAL [ConnectivityManager::connectivity] Failed to add new connectivity data base to internal list (internal error).";
	throw ConfigException(message.str());
      }

      conn_iter=ret.first;
    }

    return conn_iter->second;
  }

  ConnectivityManager::~ConnectivityManager() {
    std::cout << "INFO [ConnectivityManager::connectivity] dtor " << static_cast<void *>(this) << std::endl;
    assert( cleanup() );
  }

  bool ConnectivityManager::cleanup() 
  {
    bool ret = true;
    for (ConnectivityList_t::iterator conn_iter=m_connectivityList.begin();
	 conn_iter != m_connectivityList.end();
	 conn_iter++) {

      std::cout << "INFO [ConnectivityManager::cleanup] database for " << conn_iter->first << " has " << conn_iter->second->use_count() << " users. "<< std::endl;
      if ( conn_iter->second->use_count() == 0 ) {
	std::cout << "INFO [ConnectivityManager::cleanup] Cleanup connectivity database for " << conn_iter->first << std::endl;
	delete conn_iter->second;
	m_connectivityList.erase(conn_iter);
      }
    }
    std::cout << "INFO [ConnectivityManager::cleanup] " << m_connectivityList.size() << " databases left." << " List is " 
	      << (m_connectivityList.empty() ? " empty " : " filled" ) << "." << std::endl;

    return ret;
  }

  void ConnectivityManager::getConnectivityTags(const std::string &id_name,
						std::vector<std::string> *conn_tag_out,
						std::vector<std::string> *alias_tag_out,
						std::vector<std::string> *payload_tag_out,
						std::vector<std::string> *configuration_tag_out,
						bool db_server_only) {

    if (db_server_only) {
      if (m_dbServer.get()) {
	DbServerConnectivityWrapper::getConnectivityTagsFromDbServer(*m_dbServer,id_name,conn_tag_out,alias_tag_out,payload_tag_out,configuration_tag_out);
      }
      // @todo clear tag lists?
      //       else {
      // 	if (conn_tag_out) conn_tag_out->clear();
      // 	if (alias_tag_out) alias_tag_out->clear();
      // 	if (payload_tag_out) payload_tag_out->clear();
      // 	if (configuration_tag_out) configuration_tag_out->clear();
      //       }
    }
    else {
      PixConnectivityWrapper::getConnectivityTags(id_name,conn_tag_out,alias_tag_out,payload_tag_out,configuration_tag_out);
    }

  }

  void ConnectivityManager::getObjectTags(std::vector<std::string> *object_tags, bool db_server_only)
  {
    if (db_server_only) {
      if (m_dbServer.get()) {
	DbServerConnectivityWrapper::getObjectTagsFromDbServer(*m_dbServer, object_tags);
      }
      // @todo clear tag lists?
      //       else {
      //if (object_tags) object_tags->clear();
      //}
    }
    else {
      PixConnectivityWrapper::getObjectTags(object_tags);
    }
  }

  void ConnectivityManager::getConfigurationObjectTags(std::vector<std::string> *object_tags, bool db_server_only) {
    if (db_server_only) {
      if (m_dbServer.get()) {
	DbServerConnectivityWrapper::getConfigurationObjectTagsFromDbServer(*m_dbServer, object_tags);
      }
    }
    else {
      // @todo clear tag lists?
      //       else {
      //if (object_tags) object_tags->clear();
      //}
      PixConnectivityWrapper::getConfigurationObjectTags(object_tags);
    }
  }

  void ConnectivityManager::useDbServer(std::shared_ptr<PixLib::PixDbServerInterface> &db_server) {
    instance()->setDbServer(db_server);
    instance()->setConnObjConfigDb( ConnObjConfigDb(db_server) );
  }

}
