#ifndef _CoralDbOpenHelper_h_
#define _CoralDbOpenHelper_h_

#include <PixDbCoralDB/PixDbCoralDB.h>
#include <memory>
#include <sstream>
#include <cassert>

#include <DbWrapper.h>


namespace PixA {

  class CoralDbOpenHelper : public IDbOpenHelper
  {
  public:
    CoralDbOpenHelper(const std::string &name) : IDbOpenHelper(false),m_name(name) {
      if (m_name.compare(0,strlen("sqlite_file:"),"sqlite_file:")==0) {
	m_isSQLite = true;
      }
      else {
	m_isSQLite = false;
      }

    }

    //  virtual ~CoralDbOpenHelper() {}
    std::pair<PixLib::PixDbInterface *, PixLib::DbRecord *> open() const  {
      std::pair<PixLib::PixDbInterface *, PixLib::DbRecord *> ret;
      ret.first = NULL;
      ret.second = NULL;
      //      std::cout << "INFO [CoralDbOpenHelper::ctor] open "
      //		<< m_name
      //		<< std::endl;

      std::unique_ptr<PixLib::PixDbInterface> db(new PixLib::PixDbCoralDB(m_name,PixLib::PixDbCoralDB::READONLY,"",PixLib::PixDbCompoundTag()));

      // PixDbCoral owns the records unlike RootDb
      PixLib::DbRecord *record (db->readRootRecord());
      ret.first=db.release();
      ret.second=record;
      return ret;
    }

    std::pair<PixLib::PixDbInterface *, PixLib::DbRecord *> openForWriting() const  {
      std::pair<PixLib::PixDbInterface *, PixLib::DbRecord *> ret;
      ret.first = NULL;
      ret.second = NULL;
      //      std::cout << "INFO [CoralDbOpenHelper::ctor] open for writing "
      //		<< m_name
      //		<< std::endl;
      PixLib::PixDbCoralDB::AccessMode mode=PixLib::PixDbCoralDB::UPDATE;
      if (m_isSQLite) {
	//unsigned int pos=0;
	//if (m_name.compare(0,12,"sqlite_file:")==0) pos=12;
	std::ifstream in(&(m_name.c_str()[12]));
	mode = (((in) ? PixLib::PixDbCoralDB::UPDATE : PixLib::PixDbCoralDB::NEW));
      }
      else {
	std::string message ("ERROR [CoralDbOpenHelper::openForWriting] Opening ");
	message += m_name ;
	message += " in write mode. " ;
	message += "Opening CORAL back-ends in write mode is only supported for SQLITE files. ";
	throw PixLib::PixDBException(message);
      }
      std::unique_ptr<PixLib::PixDbInterface> db(new PixLib::PixDbCoralDB(m_name,mode,AutoCommit(false),"ROOT",PixDbCompoundTag("CT","Base", "Base", "Base")));

      // PixDbCoral owns the records unlike RootDb
      PixLib::DbRecord *record (db->readRootRecord());
      ret.first=db.release();
      ret.second=record;
      return ret;
    }

  private:
    std::string              m_name;
    bool                     m_isSQLite;
  };

}
#endif
