#ifndef _Location_h_
#define _Location_h_

#include <string>
#include <cassert>
#include <sstream>


namespace PixA {

typedef std::string Pp0LocationBase;

class Pp0Location : public Pp0LocationBase {
 public:

  enum ELocation {kDA, kDC, kLA, kLC, kNLocations};

 Pp0Location(const std::string &name) : Pp0LocationBase(name) {};
 Pp0Location(const char *name) : Pp0LocationBase(name) {};
  
  Pp0Location (ELocation location, 
	       unsigned char layer_nr,
	       unsigned char bistave_nr,
	       unsigned char bistave_part_nr);
  
protected:
  static const char s_sideName[2];
};
  
  //typedef std::string ModuleLocationBase;
typedef Pp0LocationBase ModuleLocationBase;

class ModuleLocation : public Pp0Location {
 public:
 ModuleLocation(const std::string &name) : Pp0Location(name) {};
 ModuleLocation(const char *name) : Pp0Location(name) {};

//   ModuleLocation (unsigned short module_nr ) {
//     std::stringstream name;
//     name << "M"
// 	 << module_nr;
//     std::string::operator=(name.str());
//   }

  ModuleLocation (ELocation location, 
		  unsigned char layer_nr,
		  unsigned char bistave_nr,
		  unsigned char bistave_part_nr,
		  unsigned char module_nr );
};

typedef std::string RODLocationBase;
// typedef RODLocationBase RODLocation;

typedef std::string RodLocationBase;
class RodLocation : public RodLocationBase {
 public:
  static const unsigned short s_nCratesMax = 9;
  static const unsigned short s_nSlotsMax = 23;

  RodLocation(const std::string &rod_conn_name) : RodLocationBase(rod_conn_name) {};
  
  RodLocation(unsigned short crate_nr, unsigned short slot_nr) {
    assert(crate_nr<s_nCratesMax && slot_nr < s_nSlotsMax);
    std::stringstream rod_name;
    rod_name << "ROD_CRATE_C" << crate_nr << "_S" << slot_nr;
    operator=(rod_name.str());
  }

};

typedef std::string CrateLocationBase;


}
#endif
