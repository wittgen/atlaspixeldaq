/* Dear emacs, this is -*-c++-*- */
#ifndef _PixA_DbServerRevisionIndex_h_
#define _PixA_DbServerRevisionIndex_h_

#include "IRevisionIndex.h"
#include <memory>

namespace PixLib {
  class PixDbServerInterface;
}

namespace PixA {


  class DbServerRevisionIndex : public IRevisionIndex
  {
  public:
    DbServerRevisionIndex(const std::shared_ptr<PixLib::PixDbServerInterface> &db_server);

    ~DbServerRevisionIndex();

    void getIdTags(std::vector<std::string> &id_tag_out);

    void getTags(const std::string &id_tag, std::vector<std::string> &tag_out);

    void getRevisions(const std::string &id_tag, const std::string &cfg_tag, const std::string &conn_name, std::set<Revision_t> &revisions_out);

    void getNames(const std::string &id_tag, const std::string &cfg_tag, EObjectType type, std::set<std::string> &names);

    /**
     * @param current_upper_revision_limit after scanning there cannot appear new revisions below this boundary.
     */
    void processRevision(const std::string &id_tag,
			 const std::string &cfg_tag,
			 Revision_t target_revision,
			 Revision_t lower_revision_bound,
			 Revision_t current_upper_revision_limit,
			 IRevisionProcessor &processor);

    /** Nothing to be done.
     * ... but storing the configuration which is done by the IConfigHelper.
     */
    bool addRevision(const std::string &/*id_tag*/,
		     const std::string &/*cfg_tag*/,
		     const std::string &/*type_name*/,
		     const std::string &/*name*/,
		     bool /*is_in_coral*/,
		     Revision_t /*revision*/,
		     const std::string &/*data*/) { return true; }

    
    static std::string &cfgDomainHeader() { return s_cfgDomainHeader; }
    static std::string makeCfgDomainName(const std::string &id_tag) { return cfgDomainHeader() + id_tag; }

  private:
    std::shared_ptr<PixLib::PixDbServerInterface> m_dbServer;

    static std::string s_cfgDomainHeader;
    static const char *s_typeName[IRevisionIndex::kNTypes];

  };

}
#endif
