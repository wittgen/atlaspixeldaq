#ifndef _PixConnectivityWrapper_h_
#define _PixConnectivityWrapper_h_

#include <sstream>
#include <string>
#include <map>
#include <iostream>
#include <cassert>

#include <PixConnectivity/PixConnectivity.h>
#include <PixConnectivity/RodBocConnectivity.h>
#include <PixConnectivity/OBConnectivity.h>
#include <PixConnectivity/Pp0Connectivity.h>
#include <PixConnectivity/ModuleConnectivity.h>
#include <PixConnectivity/PixDisable.h>

#include "IConfig.h"
#include "Connectivity.h"
#include "ConfigHandle.h"

#include "PixConnectivityWrapperBase.h"

#include <memory>
#include "ObjectCache_t.h"

namespace PixLib {
  class PixDisable;
}

namespace PixA {

  class DbWrapper;
  class DelayedOnDemandConfig;


  class PixConnectivityWrapper : public PixConnectivityWrapperBase
  {
    friend class ConnectivityManager;
    friend class DelayedOnDemandConfig;

    // the bool should be set to true if the PixModule is owned by the PixConnectivityWrapper otherwise it is set
    // to false.
    typedef ObjectCache_t<ConfigHandle>                             ConfigList_t;
    typedef ObjectCache_t<std::shared_ptr<PixLib::PixDisable> >   DisableList_t;

  protected:
    PixConnectivityWrapper(PixLib::PixConnectivity *conn) 
      : PixConnectivityWrapperBase(conn)
    { adjustCache(); }

    /** Create a connectivity through the db server interface.
     * @todo split PixConnectivity in a base class and derived classes the db server based connectivity 
     *  handling and one for the rootdb base connectivity handling.
     */

    PixConnectivityWrapper(const std::string &object_tag,
			   const std::string &tagC,
			   const std::string &tagP,
			   const std::string &tagA,
			   const std::string &tagCF,
			   const std::string &tagModCF);

    ~PixConnectivityWrapper();

  public:

    ConfigHandle modConf(const ModuleLocationBase &module_location, Revision_t revision) {
      return modConfHandle(module_location,revision);
    }

    const ConfigHandle modConf(const ModuleLocationBase &module_location, Revision_t revision) const {
      return const_cast<PixConnectivityWrapper *>(this)->modConfHandle(module_location,revision);
    }

    ConfigHandle modConf(ModuleList::iterator &module_iter, Revision_t revision) {
      return modConfHandle(module_iter,revision);
    }

    const ConfigHandle modConf(const ModuleList::const_iterator &module_iter, Revision_t revision) const {
      return const_cast<PixConnectivityWrapper *>(this)->modConfHandle(module_iter,revision);
    }

    Revision_t writeModConf(const ModuleList::const_iterator &module_iter, Revision_t revision) const {
      return writeModConf(*module_iter,revision);
    }

    Revision_t writeModConf(const ModuleLocationBase &location, Revision_t revision) const {
      return writeModConf(moduleConn(location),revision);
    }

    ConfigHandle bocConf(Pp0List::iterator &pp0_iter, Revision_t revision) {
      const PixLib::RodBocConnectivity *rod_boc_conn=rodBocConnectivity(pp0_iter);
      return bocConfHandle(rod_boc_conn,revision);
    }

    const ConfigHandle bocConf(const Pp0List::const_iterator &pp0_iter, Revision_t revision) const {
      const PixLib::RodBocConnectivity *rod_boc_conn=rodBocConnectivity(pp0_iter);
      return const_cast<PixConnectivityWrapper *>(this)->bocConfHandle(rod_boc_conn,revision);
    }


    ConfigHandle bocConf(const Pp0LocationBase &pp0_location, Revision_t revision) {
      const PixLib::RodBocConnectivity *rod_boc_conn=rodBocConnectivity(pp0_location);
      return bocConfHandle(rod_boc_conn,revision);
    }

    const ConfigHandle bocConf(const Pp0LocationBase &pp0_location, Revision_t revision) const  {
      const PixLib::RodBocConnectivity *rod_boc_conn=rodBocConnectivity(pp0_location);
      return const_cast<PixConnectivityWrapper *>(this)->bocConfHandle(rod_boc_conn,revision);
    }

    ConfigHandle rodConf(Pp0List::iterator &pp0_iter, Revision_t revision) {
      const PixLib::RodBocConnectivity *rod_boc_conn=rodBocConnectivity(pp0_iter);
      return rodConfHandle(rod_boc_conn,revision);
    }

    const ConfigHandle rodConf(const Pp0List::const_iterator &pp0_iter, Revision_t revision) const {
      const PixLib::RodBocConnectivity *rod_boc_conn=rodBocConnectivity(pp0_iter);
      return const_cast<PixConnectivityWrapper *>(this)->rodConfHandle(rod_boc_conn,revision);
    }

    ConfigHandle rodConf(const Pp0LocationBase &pp0_location, Revision_t revision) {
      const PixLib::RodBocConnectivity *rod_boc_conn=rodBocConnectivity(pp0_location);
      return rodConfHandle(rod_boc_conn,revision);
    }

    const ConfigHandle rodConf(const Pp0LocationBase &pp0_location, Revision_t revision) const {
      const PixLib::RodBocConnectivity *rod_boc_conn=rodBocConnectivity(pp0_location);
      return const_cast<PixConnectivityWrapper *>(this)->rodConfHandle(rod_boc_conn,revision);
    }

    std::shared_ptr<PixLib::PixDisable> getDisable(std::string disableName, Revision_t revision);

    void writeDisable(PixLib::PixDisable &disable);

    // RODLocation and Pp0Location have the same type
    //    virtual PixLib::Config &rodConf(const RODLocationBase &rod_location) =0;

    ///    virtual const PixLib::Config &rodConf(const RODLocationBase &rod_location) const =0;
    std::string tag(EConnTag tag) const {
      switch (tag) {
      case kId:
	return m_connectivity->getConnTagOI();
      case kIdCFG: {
	std::string object_cfg_tag( m_connectivity->getConnTagOI() );
	object_cfg_tag += "-CFG";
	return object_cfg_tag;
      }
      case kConn:
	return m_connectivity->getConnTagC();
      case kAlias:
	return m_connectivity->getConnTagA();
      case kPayload:
	return m_connectivity->getConnTagP();
      case kConfig:
	return m_connectivity->getCfgTag();
      case kModConfig:
	return m_connectivity->getModCfgTag();
      }
      return std::string();
    }

  protected:

    IConfig *configFromConnName(EConfigType type, const std::string &connectivity_name, const std::string &production_name, Revision_t revision) {
      return config(type, connectivity_name, (production_name.empty() ? connectivity_name : production_name), revision);
    }


  private:

    ConfigHandle modConfHandle(const ModuleLocationBase &module_location, Revision_t revision) {
      return modConfHandle( moduleConn(module_location), revision );
    }

    ConfigHandle modConfHandle(const ModuleList::const_iterator &module_iter, Revision_t revision){
      return modConfHandle(*module_iter,revision);
    }

    ConfigHandle modConfHandle(const PixLib::ModuleConnectivity *mod, Revision_t revision) {
      return delayedConfig(revision,
			   kModuleConf,
			   const_cast<PixLib::ModuleConnectivity *>(mod)->name(),
			   const_cast<PixLib::ModuleConnectivity *>(mod)->prodId());
    }


    const PixLib::RodBocConnectivity *rodBocConnectivity(const Pp0LocationBase &pp0_location) const {
      std::map<std::string, PixLib::Pp0Connectivity*>::const_iterator pp0_iter = m_connectivity->pp0s.find(pp0_location);
      if ( pp0_iter == m_connectivity->pp0s.end() ) {
	std::stringstream message;
	message << "ERROR [PixConnectivityWrapper::rodBocConnectivity] No pp0 " << pp0_location << ".";
	throw PixA::ConfigException(message.str());
      }
      return rodBocConnectivity(pp0_iter->second);
    }

    PixLib::RodBocConnectivity *rodBocConnectivity(Pp0List::iterator &pp0_iter) {
      return const_cast<PixLib::RodBocConnectivity *>(rodBocConnectivity(*pp0_iter));
    }

    const PixLib::RodBocConnectivity *rodBocConnectivity(const Pp0List::const_iterator &pp0_iter) const {
      return rodBocConnectivity(const_cast<PixLib::Pp0Connectivity *>(*pp0_iter));
    }

    const PixLib::RodBocConnectivity *rodBocConnectivity(const PixLib::Pp0Connectivity *pp0_conn) const {
      assert(const_cast<PixLib::Pp0Connectivity *>(pp0_conn)->ob() && const_cast<PixLib::Pp0Connectivity *>(pp0_conn)->ob()->rodBoc());
      PixLib::RodBocConnectivity *rod_boc_conn(const_cast<PixLib::Pp0Connectivity *>(pp0_conn)->ob()->rodBoc());
      assert(rod_boc_conn);
      return rod_boc_conn;
    }


    ConfigHandle bocConfHandle(const PixLib::RodBocConnectivity *rod_boc, Revision_t revision) {
      return delayedConfig(revision,
			   kBocConf,
			   const_cast<PixLib::RodBocConnectivity *>(rod_boc)->name(),
			   "");
    }

    ConfigHandle rodConfHandle(const PixLib::RodBocConnectivity *rod_boc, Revision_t revision) {
      return delayedConfig(revision,
			   kRodConf,
			   const_cast<PixLib::RodBocConnectivity *>(rod_boc)->name(),
			   "");
    }

    Revision_t writeModConf(const PixLib::ModuleConnectivity *mod_conn, Revision_t revision) const;

    DbWrapper *db(std::string full_name) const;

    ConfigHandle delayedConfig(Revision_t revision,
			       PixConnectivityWrapper::EConfigType type,
			       const std::string &connectivity_name,
			       const std::string &production_name);

    IConfig *config(EConfigType type, const std::string &name, const std::string &folder_name, Revision_t revision) ;

    static Revision_t extractRevision(const std::string &name);

    void adjustCache();

    mutable ConfigList_t m_configList[kNConfTypes];
    mutable DisableList_t m_disableList;

    static const char *s_typeNames[kNConfTypes];
    static const char *s_disableTypeName;
  };

}

#endif
