#include "PixDbAccessor.h"
#include <RootDb/RootDb.h>

namespace PixA {
  void PixDbAccessor::fillListOfFields(std::vector<std::string> &field_list, bool format)
  {
    field_list.clear();
    prepareDbUsage();
    PixLib::RootDbRecord *root_db_record=dynamic_cast<PixLib::RootDbRecord*>(m_record.get());
    if (root_db_record) {
      field_list = root_db_record->fieldNameList();
      if (format) {

      for(std::vector<std::string>::iterator field_iter=field_list.begin();
	  field_iter != field_list.end();
	  field_iter++) {
	std::string::size_type slash_pos = field_iter->rfind("/");
	if (slash_pos != std::string::npos) {
	  field_iter->erase(0,slash_pos+1);
	  std::string::size_type pos = field_iter->find("_");
	  std::string new_name ("[\"");
	  new_name += std::string(*field_iter,0,pos);
	  new_name +="\"][\"";
	  std::string::size_type end_pos = field_iter->rfind(";");
	  new_name += std::string(*field_iter,pos+1,end_pos);
	  new_name +="\"]";
	  *field_iter = new_name;
	}
      }
      }
    }
    else {
      
      for(PixLib::dbFieldIterator field_iter=fieldBegin();
	  field_iter != fieldEnd();
	  field_iter++) {
      
	PixLib::dbFieldIterator proc_field_iter =  db()->DbProcess(field_iter,PixLib::PixDb::DBREAD);
	//	std::cout <<  (*proc_field_iter)->getName() << std::endl;
	field_list.push_back((*proc_field_iter)->getName());

	if (format) {
	  
	  std::string::size_type slash_pos = field_list.back().rfind("/");
	  if (slash_pos != std::string::npos) {
	    field_list.back().erase(0,slash_pos+1);
	    std::string::size_type pos = field_list.back().find("_");
	    std::string new_name ("[\"");
	    new_name += std::string(field_list.back(),0,pos);
	    new_name +="\"][\"";
	    std::string::size_type end_pos = field_list.back().rfind(";");
	    new_name += std::string(field_list.back(),pos+1,end_pos);
	    new_name +="\"]";
	    field_list.back() = new_name;
	  }
	}

	proc_field_iter.releaseMem();
      }
      //      throw ConfigException("ERROR [PixDbAccessor::fillListOfFields] Not a RootDb interface.");
    }
  }

  /** Add a new record.
   */
  std::shared_ptr<PixLib::DbRecord> PixDbAccessor::addRecord(const std::string &class_name, const std::string &slot_name) 
  { 
    if (m_readOnly) {
      std::string message ("ERROR [PixA::PixDbAccessor::addRecord] Database is opened read only. Cannot add record " );
      message += class_name;
      message += " / ";
      message += slot_name;
      throw PixLib::PixDBException(message);
    }
    prepareDbUsage();
    return m_db->addRecord(m_record,class_name, slot_name); 
  }

}
