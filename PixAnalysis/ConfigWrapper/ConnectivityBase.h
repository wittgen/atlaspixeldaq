#ifndef _ConnectivityBase_h_
#define _ConnectivityBase_h_

#include <cassert>

namespace PixA {

  class ConnectivityBase
  {
  protected:
    ConnectivityBase() : m_use(0) {}
  public:
    virtual ~ConnectivityBase() { assert(m_use==0); }

    void use() {m_use++;}
    void done() {assert(m_use>0);  m_use--; }

    unsigned int use_count() const {return m_use;}
  private:
    unsigned int m_use;
  };

}

#endif
