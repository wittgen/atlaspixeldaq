#ifndef _DbManager_h_
#define _DbManager_h_

#include <string>
#include <map>
#include <vector>
#include "ConfigHandle.h"
#include "DbRef.h"

namespace PixLib {
  class PixDbInterface;
  class dbRecordIterator;
}

namespace PixA {

  class PixDbAccessor;
  class DbWrapper;
  class PixConnectivityWrapper;
  class RootDbConfigHelper;

  class DbManager
  {
    friend class PixConnectivityWrapper;
    friend class DbWrapper;
    friend class RootDbConfigHelper;

    typedef std::map<std::string, DbWrapper *> DbList_t;
  protected:
    DbManager() {}

    DbWrapper *getDbWrapper(std::string full_name,const std::string name = "");

    void destroy(DbWrapper *db);

  public:

    PixDbAccessor *db(const std::string &db_name, const std::vector<std::string> &record_hierarchy, bool read_only=true);
    PixDbAccessor *db(const std::string &db_name, const std::string &top_level_record_name, bool read_only=true);
    PixDbAccessor *db(const std::string &db_name, bool read_only=true);
      PixDbAccessor *db_1(const std::string &db_name, std::string string="", bool read_only=true);

    DbHandle handle(const std::string &db_name, const std::vector<std::string> &record_hierarchy, bool read_only=true);
    DbHandle handle(const std::string &db_name, const std::string &top_level_record_name, bool read_only=true);
    DbHandle handle(const std::string &db_name, bool read_only=true);
      DbHandle handle_1(const std::string &db_name, std::string string="", bool read_only=true);

    ConfigHandle config(const std::string &db_name, const std::vector<std::string> &record_hierarchy);

    ConfigHandle config(const std::string &db_name, const std::string &top_level_record_name) ;

    ConfigHandle config(const DbHandle &handle);

    static ConfigHandle wrapConfigRecord(PixLib::PixDbInterface *db, PixLib::dbRecordIterator &record_iter);

    ~DbManager();

    static PixDbAccessor *getDb(const std::string &db_name, const std::vector<std::string> &record_hierarchy, bool read_only=true) {
      return instance()->db(db_name,record_hierarchy, read_only);
    }

    static PixDbAccessor *getDb(const std::string &db_name, const std::string &top_level_record_name, bool read_only=true) {
      return instance()->db(db_name,top_level_record_name, read_only);
    }

    static PixDbAccessor *getDb(const std::string &db_name, bool read_only=true) {
      return instance()->db(db_name, read_only);
    }


    static ConfigHandle configHandle(const DbHandle &handle) {
      return instance()->config(handle);
    }

    static DbHandle dbHandle(const std::string &db_name, const std::vector<std::string> &record_hierarchy, bool read_only=true) {
      return instance()->handle(db_name,record_hierarchy, read_only);
    }

    static DbHandle dbHandle(const std::string &db_name, const std::string &top_level_record_name, bool read_only=true) {
      return instance()->handle(db_name,top_level_record_name, read_only);
    }

    static DbHandle dbHandle(const std::string &db_name, bool read_only=true) {
      return instance()->handle(db_name, read_only);
    }

      static DbHandle dbHandle_1(const std::string &db_name, std::string string="", bool read_only=true) {
          return instance()->handle_1(db_name,string, read_only);
      }
      

    static void stat();

    static DbManager *instance()  {
      if (!s_instance) s_instance = new DbManager;
      return s_instance;
    }

    bool cleanup();

    static void deepCleanup();

  private:
    // debug:
    void showStat();

    DbList_t m_dbList;
    static DbManager *s_instance;
  };

}

#endif
