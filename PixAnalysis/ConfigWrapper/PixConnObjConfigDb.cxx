#include "PixConnObjConfigDb.h"
#include <TFile.h>
#include <TKey.h>

#include <PixBoc/PixBoc.h>
#include <PixModule/PixModule.h>
#include <PixTrigController/TimPixTrigController.h>

#include <ConfigRootIO/ConfigStoreFactory.h>
#include <ConfigRootIO/ConfigStore.h>

#include "PixConfigWrapperTopLevel.h"

#include "CoralDBRevisionIndex.h"
#include "DbServerRevisionIndex.h"

//for RootDbConfigHelper
#include "DbManager.h"

#include "Regex_t.h"

// umask
// mkdir
#include <sys/stat.h>

#define DEBUG_TRACE(a) { }

namespace PixA {

  bool CfgLocation_t::configRootIOFile(const std::string &file_name) {
    TFile a_file(file_name.c_str());
    if (!a_file.IsOpen()) {
      std::string::size_type name_start = file_name.rfind("_");
      if (name_start != std::string::npos && name_start>1) {
	std::string comb_file_name(file_name.substr(0,name_start));
	comb_file_name+= ".root";
	std::cout << "INFO [CfgLocation_t::configRootIOFile] Does the revision archive " << comb_file_name << " exist ? "<< std::endl;
	TFile comb_file(comb_file_name.c_str());
	if (comb_file.IsOpen()) {
	  m_fileName.erase(m_fileName.size()+name_start-file_name.size(),m_fileName.size());
	  m_fileName += ".root";
	  std::cout << "INFO [CfgLocation_t::configRootIOFile] Yes. Okay use it " << m_fileName << "."<< std::endl;
	  return true;
	}
      }
      return false;
    }
    TObject *object = a_file.GetListOfKeys()->FindObject("rootRecord");
    if (object) {
      DEBUG_TRACE( std::cout << "INFO [CfgLocation_t::configRootIOFile] "<<file_name <<"is old file." <<std::endl )
      assert( object->InheritsFrom(TKey::Class()) ); 
      if ( strcmp(static_cast<TKey *>(object)->GetClassName(),"PixLib::RootDbRecord")==0) return false;
    }
    DEBUG_TRACE( std::cout << "INFO [CfgLocation_t::configRootIOFile] "<<file_name <<"is new file." <<std::endl )
    return true;
  }

  // for debugging
  std::string makeTimeString(time_t a_time)
  {
    std::string a_time_string = ctime(&a_time);
    std::string::size_type ret_pos = a_time_string.find("\r");
    std::string::size_type new_line_pos = a_time_string.find("\n");
    if (new_line_pos == std::string::npos || (new_line_pos>ret_pos && ret_pos != std::string::npos)) {
      new_line_pos =ret_pos;
    }
    if (new_line_pos != std::string::npos ) {
      a_time_string.erase(new_line_pos, a_time_string.size()-new_line_pos);
    }
    return a_time_string;
  }


  bool matchesConfigName(const PixLib::Config &config, const std::string &name) {
    const std::string temp = config.name();
    std::string::size_type pos = temp.size();
    while (pos>0 && isdigit(temp[--pos]));
    if (pos+1 >= temp.size() || temp[pos]!='_') {
      pos=temp.size();
    }
    return temp.compare(0,pos,name)==0;
  }

  std::string  configName(const PixLib::Config &config) {
    const std::string temp = config.name();
    std::string::size_type pos = temp.size();
    while (pos>0 && isdigit(temp[--pos]));
    if (pos+1 >= temp.size() || temp[pos]!='_') {
      pos=temp.size();
    }
    return temp.substr(0,pos);
  }


  const std::string PixConnObjConfigDbTag::s_tmpPendingTag("_Tmp");

  PixConnObjConfigDbTag::PixConnObjConfigDbTag(PixConnObjConfigDbManager &manager,
					 TagPayload_t &tag_payload,
					 Revision_t revision)
    : m_dbManager(&manager),
      m_tagPayload(&tag_payload),
      m_revision(revision)
  { m_tagPayload->incrementUsed(); m_dbManager->incrementUsed(); }

  PixConnObjConfigDbTag::~PixConnObjConfigDbTag() 
  {
    m_tagPayload->decrementUsed();
    m_dbManager->decrementUsed();
    m_dbManager->tryDestruction();
  }


  void PixConnObjConfigDbTag::configure(PixLib::Config &config) {
    std::string name = configName(config);
    CfgLocation_t &cfg_location = cfgLocation( name );
    IConfigHelper &config_helper = m_dbManager->configHelper(cfg_location);
    config_helper.readConfig( config, *m_tagPayload, name , m_revision, cfg_location);
  }




  // helper templates

  template <class T_CompoundObj_t, class T_Obj_t>
  inline PixLib::Config &config(T_CompoundObj_t *obj) {
    return obj->config();
  }


  template <>
  inline PixLib::Config &config<PixLib::PixModuleGroup, PixLib::PixBoc>(PixLib::PixModuleGroup *obj) {
    if (!obj->getPixBoc()) {
      std::stringstream message;
      message << "FATAL [config<PixLib::PixModuleGroup, PixLib::PixBoc>] No PixBoc.";
      throw PixA::ConfigException( message.str() );
    }
    return *(obj->getPixBoc()->getConfig());
  }

  template <>
  inline PixLib::Config &config<PixLib::PixModuleGroup, PixLib::PixController>(PixLib::PixModuleGroup *obj) {
    if (!obj->getPixController()) {
      std::stringstream message;
      message << "FATAL [config<PixLib::PixModuleGroup, PixLib::PixController>] No PixController.";
      throw PixA::ConfigException( message.str() );
    }
    return (obj->getPixController()->config());
  }

  template <>
  inline PixLib::Config &config<PixLib::PixTrigController, PixLib::PixTrigController>(PixLib::PixTrigController *obj) {
    PixLib::Config *config = obj->config();
    if (!config) {
      std::stringstream message;
      message << "FATAL [config<PixLib::PixTrigController, PixLib::PixTrigController>] No config.";
      throw PixA::ConfigException( message.str() );
    }
    return *config;
  }

  template <class T_CompoundObj_t, class T_Obj_t>
  inline const PixLib::Config &config(const T_CompoundObj_t *obj) {
    return config<T_CompoundObj_t,T_Obj_t>( const_cast<T_CompoundObj_t *>(obj) );
  }


  template <class T_CompoundObj_t, class T_Obj_t>
  inline T_Obj_t *childObject(T_CompoundObj_t *obj) {
    return obj;
  }


  template <>
  inline PixLib::PixBoc *childObject<PixLib::PixModuleGroup,PixLib::PixBoc>(PixLib::PixModuleGroup *obj) {
    if (!obj) return static_cast<PixLib::PixBoc *>(NULL);
    return obj->getPixBoc();
  }

  template <> PixLib::PixController *childObject<PixLib::PixModuleGroup,PixLib::PixController>(PixLib::PixModuleGroup *obj) {
    if (!obj) return static_cast<PixLib::PixController *>(NULL);
    return obj->getPixController();
  }

  template <class T_CompoundObj_t, class T_Obj_t>
  inline const T_Obj_t *childObject(const T_CompoundObj_t *obj) {
    return childObject<T_CompoundObj_t,T_Obj_t>( const_cast<T_CompoundObj_t *>(obj) );
  }


  template <class T_CompoundObj_t, class T_Obj_t>
  ConfigHandle PixConnObjConfigDbTag::handleFromCachedObject(const std::string &name) {
    std::shared_ptr<T_CompoundObj_t> *cache_ptr = m_tagPayload->cache< std::shared_ptr<T_CompoundObj_t> >().object(name,m_revision);
    if (cache_ptr) {
      std::shared_ptr<const T_CompoundObj_t> obj =  *cache_ptr;
      PixLib::Config &a_config = const_cast<PixLib::Config &>( PixA::config<T_CompoundObj_t,T_Obj_t>(obj.get()) );
      return *(new PixConfigWrapperTopLevel<const T_CompoundObj_t>( obj, a_config ));
    }
    else {
      return ConfigHandle();
    }
  }

  // configured Objects

  template <class T_CompoundObj_t>
  inline const std::shared_ptr<const T_CompoundObj_t> PixConnObjConfigDbTag::configuredObject(const std::string &name) {
    return m_dbManager->cacheObject<T_CompoundObj_t>(*m_tagPayload, name, m_revision);
  }

  template <class T_CompoundObj_t, class T_Obj_t>
  inline const T_Obj_t *PixConnObjConfigDbTag::configuredChildObject(const std::string &name) {
    return childObject<T_CompoundObj_t, T_Obj_t>( configuredObject<T_CompoundObj_t>(name).get() );
  }

  std::shared_ptr<const PixLib::PixModule> PixConnObjConfigDbTag::configuredModulePtr(const std::string &name) {
    return configuredObject<PixLib::PixModule>(name);
  }

  std::shared_ptr<const PixLib::PixDisable> PixConnObjConfigDbTag::disablePtr(const std::string &name) {
    return configuredObject<PixLib::PixDisable>(name);
  }

  std::shared_ptr<const PixLib::PixModuleGroup> PixConnObjConfigDbTag::configuredModuleGroupPtr(const std::string &name) {
    return configuredObject<PixLib::PixModuleGroup>(name);
  }

  std::shared_ptr<const PixLib::PixTrigController> PixConnObjConfigDbTag::configuredTimPtr(const std::string &name) {
    return configuredObject<PixLib::PixTrigController>(name);
  }


  const PixLib::PixModule      &PixConnObjConfigDbTag::configuredModule(const std::string &name) {
    return *configuredChildObject<PixLib::PixModule,PixLib::PixModule>(name);
  }

  const PixLib::PixModuleGroup &PixConnObjConfigDbTag::configuredModuleGroup(const std::string &name) {
    return *configuredChildObject<PixLib::PixModuleGroup,PixLib::PixModuleGroup>(name);
  }

  const PixLib::PixTrigController &PixConnObjConfigDbTag::configuredTim(const std::string &name) {
    return *configuredChildObject<PixLib::PixTrigController,PixLib::PixTrigController>(name);
  }

  const PixLib::PixBoc         &PixConnObjConfigDbTag::configuredBoc(const std::string &name) {
    return *configuredChildObject<PixLib::PixModuleGroup,PixLib::PixBoc>(name);
  }

  const PixLib::PixController  &PixConnObjConfigDbTag::configuredRodController(const std::string &name) {
    return *configuredChildObject<PixLib::PixModuleGroup,PixLib::PixController>(name);
  }


  // Config

  template <class T_CompoundObj_t, class T_Obj_t>
  inline const PixLib::Config &PixConnObjConfigDbTag::config(const std::string &name) {
    return PixA::config<T_CompoundObj_t,T_Obj_t>( m_dbManager->cacheObject<T_CompoundObj_t>(*m_tagPayload, name, m_revision).get() ); 
  }


  const PixLib::Config &PixConnObjConfigDbTag::moduleConfig(const std::string &name) {
    return config<PixLib::PixModule,PixLib::PixModule>( name );
  }

  const PixLib::Config &PixConnObjConfigDbTag::moduleGroupConfig(const std::string &name) {
    return config<PixLib::PixModuleGroup,PixLib::PixModuleGroup>( name );
  }

  const PixLib::Config &PixConnObjConfigDbTag::bocConfig(const std::string &name) {
    return config<PixLib::PixModuleGroup,PixLib::PixBoc>( name );
  }

  const PixLib::Config &PixConnObjConfigDbTag::rodControllerConfig(const std::string &name) {
    return config<PixLib::PixModuleGroup,PixLib::PixController>( name );
  }

  const PixLib::Config &PixConnObjConfigDbTag::timConfig(const std::string &name) {
    return config<PixLib::PixTrigController,PixLib::PixTrigController>( name );
  }

  // helper for config hadle

  int PixConnObjConfigDbTag::getCtrlId(const std::string &name){
    CfgLocation_t &cfg_location = cfgLocation(name);
    IConfigHelper &config_helper = m_dbManager->configHelper(cfg_location);
    PixLib::PixModuleGroup *grp = new PixLib::PixModuleGroup(name);
    PixLib::Config &cfg = grp->config();
    config_helper.readConfig(cfg, *m_tagPayload, name, m_revision, cfg_location);
    int ctrlId = ((PixLib::ConfList&)cfg["general"]["ctrlType"]).getValue();
    // group was only used to read ctrl. falvour, delete
    delete grp;
    return ctrlId;
  }

  // Config handle

  ConfigHandle PixConnObjConfigDbTag::configHandle(ConfigHandleFunc_t config_handle_func, const std::string &name) {
    std::string IDname=name;
    int pos = name.find("_flavour");
    if(pos!=(int)std::string::npos) IDname = name.substr(0,pos);
    CfgLocation_t &cfg_location = cfgLocation(IDname);
    IConfigHelper &config_helper = m_dbManager->configHelper(cfg_location);
    return (config_helper.*config_handle_func)(*m_tagPayload, name, m_revision, cfg_location);
  }

  ConfigHandle  PixConnObjConfigDbTag::moduleConfigHandle(const std::string &name) {
//      std::cout<<"beccatooooooooooooo"<<std::endl;
    std::string IDname=name;
    int pos = name.find("_flavour");
    if(pos!=(int)std::string::npos) IDname = name.substr(0,pos);
//      std::cout<<"beccatooooooooooooo 1"<<std::endl;
    ConfigHandle handle_from_cache( handleFromCachedObject<PixLib::PixModule,PixLib::PixModule>(IDname) );
//      std::cout<<"beccatooooooooooooo 2"<<std::endl;
    if (handle_from_cache) {
      return handle_from_cache;
    }
    else {
//        std::cout<<"beccatooooooooooooo 3"<<std::endl;

      return configHandle( &IConfigHelper::modConf, name);
    }
  }

  ConfigHandle  PixConnObjConfigDbTag::moduleGroupConfigHandle(const std::string &name) {
    ConfigHandle handle_from_cache( handleFromCachedObject<PixLib::PixModuleGroup,PixLib::PixModuleGroup>(name) );
    if (handle_from_cache) {
      return handle_from_cache;
    }
    else {
      // get PixController flavour from grp. config.
      std::string decName = name;
      std::stringstream a;
      a << getCtrlId(name);
//      decName += "_flavour_"+a.str();
//         std::cout<<"beccato 1"<<std::endl;
      // get correct cfg.
      return configHandle( &IConfigHelper::rodConf, decName);
    }
  }

  ConfigHandle  PixConnObjConfigDbTag::bocConfigHandle(const std::string &name) {
    ConfigHandle handle_from_cache( handleFromCachedObject<PixLib::PixModuleGroup,PixLib::PixBoc>(name) );
    if (handle_from_cache) {
      return handle_from_cache;
    }
    else {
      // get PixController flavour from grp. config.
      std::string decName = name;
      std::stringstream a;
      a << getCtrlId(name);
//        std::cout<<"beccato 2"<<std::endl;
//      decName += "_flavour_"+a.str();
      // get correct cfg.
      return configHandle( &IConfigHelper::bocConf, decName);
    }
  }

  ConfigHandle  PixConnObjConfigDbTag::rodControllerConfigHandle(const std::string &name) {
    ConfigHandle handle_from_cache( handleFromCachedObject<PixLib::PixModuleGroup,PixLib::PixController>(name) );
    if (handle_from_cache) {
      return handle_from_cache;
    }
    else {
      // get PixController flavour from grp. config.
      std::string decName = name;
      std::stringstream a;
      a << getCtrlId(name);
//      decName += "_flavour_"+a.str();
//         std::cout<<"beccato 3"<<std::endl;
      // get correct cfg.
      return configHandle( &IConfigHelper::controllerConf, decName);
    }
  }

  ConfigHandle  PixConnObjConfigDbTag::timConfigHandle(const std::string &name) {
    ConfigHandle handle_from_cache( handleFromCachedObject<PixLib::PixTrigController,PixLib::PixTrigController>(name) );
    if (handle_from_cache) {
      return handle_from_cache;
    }
    else {
      std::shared_ptr<const PixLib::PixTrigController> obj(configuredTimPtr(name));
      PixLib::Config &a_config = PixA::config<PixLib::PixTrigController, PixLib::PixTrigController> (const_cast<PixLib::PixTrigController *>(obj.get()));

      return *(new PixConfigWrapperTopLevel<const PixLib::PixTrigController>( obj,a_config));
    }
  }


  ConfigHandle  PixConnObjConfigDbTag::disableConfigHandle(const std::string &name) {
    std::shared_ptr<const PixLib::PixDisable> obj(disablePtr(name));
    PixLib::Config &a_config = PixA::config<PixLib::PixDisable, PixLib::PixDisable> (const_cast<PixLib::PixDisable *>(obj.get()));
    return *(new PixConfigWrapperTopLevel<const PixLib::PixDisable>( obj, a_config));
  }

  PixLib::PixModule      *PixConnObjConfigDbTag::createConfiguredModule(const std::string &name) {
    return m_dbManager->createConfiguredObject<PixLib::PixModule>(*m_tagPayload, name, m_revision);
  }

  PixLib::PixModuleGroup *PixConnObjConfigDbTag::createConfiguredModuleGroup(const std::string &name) {
    return m_dbManager->createConfiguredObject<PixLib::PixModuleGroup>(*m_tagPayload, name, m_revision);
  }

  PixLib::PixTrigController *PixConnObjConfigDbTag::createConfiguredTim(const std::string &name) {
    return m_dbManager->createConfiguredObject<PixLib::PixTrigController>(*m_tagPayload, name, m_revision);
  }


  void dumpCfgLocation(const std::string &id_tag, const std::string &tag, Revision_t revision, const std::string &name, CfgLocation_t *cfg_location);

  class RevisionInserter : public IRevisionProcessor
  {
  public:
//     RevisionInserter(PixConnObjConfigDbManager &manager, ObjectList_t &object_list)
//       : m_manager(&manager),
// 	m_objectList(&object_list),
// 	m_onDbServer(false),
// 	m_lowerBound(0),
// 	m_upperBound(REV_MAX)
//     {

//     }

    RevisionInserter(PixConnObjConfigDbManager &manager, ObjectList_t &object_list, Revision_t lower_revision_list_bound, Revision_t upper_revision_list_bound)
      : m_manager(&manager),
	m_objectList(&object_list),
	m_lowerBound(lower_revision_list_bound),
	m_upperBound(upper_revision_list_bound),
	m_onDbServer(false)
    {
      assert(m_lowerBound<m_upperBound);
    }

    void process(const std::string &name, const std::string &data, Revision_t since, Revision_t till, unsigned short representation) {
	RevisionList_t &revision_list= (*m_objectList)[ name ];
	DEBUG_TRACE( std::cout << "INFO [RevisionInserter::process] insert revision for " << name << " : " << since << " - " << till << " : " 
		     << data  << " ; " << representation << std::endl )
	bool till_is_cut_off=false;
	if (till==0 || till==REV_MAX) {
	  till=m_upperBound;
	  till_is_cut_off=true;
	}

	RevisionList_t::iterator closest_rev = lower_element(revision_list, since );
	RevisionList_t::iterator the_revision = closest_rev;
	if (closest_rev != revision_list.end()) {
	  DEBUG_TRACE( std::cout << "INFO [RevisionInserter::process] closest revision for " << name << " : " << since << " - " << till << " : "
		       << " is " << closest_rev->first << " -"<<  closest_rev->second.validTill() << std::endl )

	  if ( closest_rev->first < since && closest_rev->second.validTill() > since/* || closest_rev->second.validTill()>m_lowerBound*/) {
	    closest_rev->second.setValidity(since, false);
	  }
	  closest_rev++;
	}
	else {
	  closest_rev = revision_list.begin();
	  // @todo in principle closes_rev must now be larger than since.
	  while (closest_rev!= revision_list.end() && closest_rev->first<since) closest_rev++;
	}

	if (closest_rev != revision_list.end()) {

	  DEBUG_TRACE( std::cout << "INFO [RevisionInserter::process] next revision for " << name << " : " << since << " - " << till << " : "
		       << " is " << closest_rev->first << " -"<<  closest_rev->second.validTill() << std::endl )

	  if ( closest_rev->first < till) {
	    till = closest_rev->first;
	    till_is_cut_off=false;
	  }
	}

	if (the_revision == revision_list.end() || the_revision->first != since) {
	  std::pair<RevisionList_t::iterator, bool> 
	    ret = revision_list.insert(std::make_pair(since,CfgLocation_t(till,
									  till_is_cut_off,
									  (data.empty() ? data : m_manager->stripDefaultPath(data)),
									  m_onDbServer)));

	  DEBUG_TRACE( if (ret.second) {
	    dumpCfgLocation("?","?",since, name, &(ret.first->second));
	  })
	  assert( ret.first != revision_list.end() );
	  ret.first->second.setRepresentation( representation );
	}
	else {
	  if (m_onDbServer) {
	    the_revision->second.setOnDbServer(true);
	  }
	  if (!data.empty() || the_revision->second.fileName().empty()) {
	    the_revision->second.setFileName( (data.empty() ? data : m_manager->stripDefaultPath(data)) );
	  }
	  if (the_revision->second.isValidityCutOff()) {
	    if (till > the_revision->second.validTill()) {
	      the_revision->second.setValidity(till, till_is_cut_off );
	    }
	  }
	  else {
	    if (till < the_revision->second.validTill()) {
	      the_revision->second.setValidity(till, till_is_cut_off );
	    }
	  }
	}
    }

    void setOnDbServer(bool on_db_server) { m_onDbServer = on_db_server; }

  private:
    PixConnObjConfigDbManager *m_manager;
    ObjectList_t              *m_objectList;
    Revision_t             m_lowerBound;
    Revision_t             m_upperBound;
    bool                       m_onDbServer;
  };


  Revision_t PixConnObjConfigDbTag::storeConfig( const std::string &name,
						 const std::string &type_name,
						 const std::vector< std::pair< std::string, const PixLib::Config *> > &config_list,
						 Revision_t revision,
						 const std::string &pending_tag,
						 bool cache) {
    bool has_configs=false;
    if (name.empty()) {
      throw PixA::ConfigException( "FATAL [PixConnObjConfigDbTag::storeConfig] Cannot store unnamed config." );
    }
    for (std::vector< std::pair< std::string, const PixLib::Config *> >::const_iterator iter = config_list.begin();
	 iter != config_list.end();
	 ++iter) {
      if (iter->second) {
	if (iter->first.empty()) {
	  throw PixA::ConfigException( "FATAL [PixConnObjConfigDbTag::storeConfig] Cannot store unnamed config." );
	}
	has_configs=true;
      }
    }

    if (!has_configs ) {
      throw PixA::ConfigException( "FATAL [PixConnObjConfigDbTag::storeConfig] Config list is empty." );
    }

    if (revision==REV_MAX || revision==0) {
      revision=time(0);
    }
    IConfigHelper *config_store_helper = m_dbManager->configStoreHelper();
    if (!config_store_helper) {
      throw PixA::ConfigException( "FATAL [PixConnObjConfigDbTag::storeConfig] No back-end to store configs." );
    }

    {
      RevisionInserter revision_inserter(*m_dbManager, m_tagPayload->objectIndex(),revision,REV_MAX);
      revision_inserter.process(name, "", revision, REV_MAX,0);
    }

    // after insertion there should now be a cfglocation for this revision 
    CfgLocation_t *cfg_location = m_tagPayload->cfgLocation(name,revision);
    if (!cfg_location) {
      std::string message_name("FATAL [PixConnObjConfigDbTag::storeConfig] Failed to add entry for ");
      message_name += name;
      message_name += ".";
      throw PixA::ConfigException( message_name );
    }
    if (!config_store_helper->writeConfig(name, type_name, config_list, *m_tagPayload, revision, *cfg_location, pending_tag )) {
      revision=0;
    }


    //     const_cast<PixLib::Config &>(config).write(m_dbManager->dbServer().get(),
    // 					       PixConnObjConfigDbManager::makeCfgDomainName(m_tagPayload->idTag()),
    // 					       m_tagPayload->tag(),
    // 					       name,
    // 					       config.type(),
    // 					       pending_tag,
    // 					       revision);
    if (cache) {
      // createobject 
      // copy config;
    }
    return revision;
  }

  Revision_t PixConnObjConfigDbTag::storeConfig( const PixLib::Config &config, Revision_t revision, const std::string &pending_tag, bool cache) {
    std::vector< std::pair< std::string, const PixLib::Config *> > config_list;
    std::string name = configName(config);
    config_list.push_back( std::make_pair(name, &config) );
    return storeConfig(name, config_list.front().second->type(), config_list, revision,pending_tag, cache);
  }

  Revision_t PixConnObjConfigDbTag::storeConfig( const std::string &name,
						 const std::string &type,
						 const PixLib::Config &config,
						 Revision_t revision,
						 const std::string &pending_tag,
						 bool cache) {
    std::vector< std::pair< std::string, const PixLib::Config *> > config_list;
    config_list.push_back( std::make_pair(name, &config) );
    return storeConfig(name, type, config_list, revision,pending_tag,cache);
  }

  Revision_t PixConnObjConfigDbTag::storeConfig( const PixLib::PixModule &obj, Revision_t revision, const std::string &pending_tag, bool cache) {
    //    throw std::runtime_error("FATAL [PixConnObjConfigDbTag::storeConfig(PixLib::PixModule...)] Not implemented.");

    return storeConfig( const_cast<PixLib::PixModule &>(obj).moduleName(),
			 "PixModule",
			 PixA::config<PixLib::PixModule,PixLib::PixModule>( const_cast<PixLib::PixModule *>(&obj) ),
			 revision,
			 pending_tag,
			 cache);

    //     const PixLib::Config &config = PixA::config<PixLib::PixModule,PixLib::PixModule>( const_cast<PixLib::PixModule *>(&obj) );
    //     std::vector<const PixLib::Config *> config_list;
    //     config_list.push_back( &config);
    //     std::string name = configName(config);
    //     return storeConfig(config_list, "PixModule", revision, pending_tag, name, cache);
  }

  std::vector< std::pair< std::string, const PixLib::Config *> > makeConfigList(const PixLib::PixModuleGroup &obj);

  Revision_t PixConnObjConfigDbTag::storeConfig( const PixLib::PixModuleGroup &obj, Revision_t revision, const std::string &pending_tag, bool cache) {

    // alternative :
    //    throw std::runtime_error("FATAL [PixConnObjConfigDbTag::storeConfig(PixLib::PixModuleGroup...)] Not implemented.");
    //     std::vector< std::pair< std::string, const PixLib::Config *> > config_list = makeConfigList(obj);
    //     return storeConfig(const_cast<PixLib::PixModuleGroup&>(obj).getName(), "PixModuleGroup", config_list, revision, pending_tag, cache);

    // @todo should avoid duplication of the cfg_location stuff :
    std::string name = const_cast<PixLib::PixModuleGroup &>(obj).getName();

    if (name.empty()) {
      throw PixA::ConfigException( "FATAL [PixConnObjConfigDbTag::storeConfig] Cannot store unnamed config." );
    }

    if (revision==REV_MAX || revision==0) {
      revision=time(0);
    }

    IConfigHelper *config_store_helper = m_dbManager->configStoreHelper();
    if (!config_store_helper) {
      throw PixA::ConfigException( "FATAL [PixConnObjConfigDbTag::storeConfig] No back-end to store configs." );
    }

    {
      RevisionInserter revision_inserter(*m_dbManager, m_tagPayload->objectIndex(),revision,REV_MAX);
      revision_inserter.process(name, "", revision, REV_MAX,0);
    }

    // after insertion there should now be a cfglocation for this revision 
    CfgLocation_t *cfg_location = m_tagPayload->cfgLocation(name,revision);
    if (!cfg_location) {
      std::string message_name("FATAL [PixConnObjConfigDbTag::storeConfig] Failed to add entry for ");
      message_name += name;
      message_name += ".";
      throw PixA::ConfigException( message_name );
    }
    if (!config_store_helper->writeConfig(obj, *m_tagPayload, revision, *cfg_location, pending_tag )) {
      revision=0;
    }

    if (cache) {
      // createobject
      // copy config;
    }
    return revision;

  }


  void PixConnObjConfigDbTag::getObjectList(std::set<std::string> &object_names_out, const std::string &name_filter) {
    if (m_tagPayload->objectIndex().empty()) {
      m_dbManager->updateRevisions(m_tagPayload->idTag(), m_tagPayload->tag(), m_tagPayload->objectIndex(), 0) ;
    }
    const ObjectList_t obj_list = m_tagPayload->objectIndex();
    Regex_t pattern(name_filter);
    for ( ObjectList_t::const_iterator obj_iter  = obj_list.begin();
	  obj_iter != obj_list.end();
	  ++obj_iter) {
      if (name_filter.empty() ||  pattern.match(obj_iter->first)) {
	object_names_out.insert(obj_iter->first);
      }
    }

  }


  std::vector<std::string> PixConnObjConfigDbManager::s_defaultCfgPath;
  //  std::string PixConnObjConfigDbManager::s_cfgDomainHeader("Configuration-");

  template <class T_CompoundObj_t, class T_Obj_t> PixLib::Config &config(const std::shared_ptr<T_CompoundObj_t> &obj) {
    return obj->config();
  }

  template <> inline PixLib::Config &config<PixLib::PixModuleGroup, PixLib::PixBoc>(const std::shared_ptr<PixLib::PixModuleGroup> &obj) {
    assert(obj->getPixBoc() && obj->getPixBoc()->getConfig());
    return *(obj->getPixBoc()->getConfig());
  }

  //   template <> inline PixLib::Config &config<PixLib::PixModuleGroup, PixLib::RodPixController>(const std::shared_ptr<PixLib::PixModuleGroup> &obj) {
  //     assert(obj->getPixController() );
  //     return (obj->getPixController()->config());
  //   }

  template <> inline PixLib::Config &config<PixLib::PixModuleGroup, PixLib::PixController>(const std::shared_ptr<PixLib::PixModuleGroup> &obj) {
    assert(obj->getPixController() );
    return (obj->getPixController()->config());
  }

  //   template <> inline PixLib::Config &config<PixLib::PixModuleGroup, PixLib::PixModuleGroup>(const std::shared_ptr<PixLib::PixModuleGroup> &obj) {
  //     return obj->config();
  //   }

  //   template <> inline PixLib::Config &config<PixLib::PixModule, PixLib::PixModule>(const std::shared_ptr<PixLib::PixModule> &obj) {
  //     return obj->config();
  //   }
  std::vector< std::pair< std::string, const PixLib::Config *> > makeConfigList(const PixLib::PixModuleGroup &const_obj)
  {
    PixLib::PixModuleGroup &obj=const_cast<PixLib::PixModuleGroup &>(const_obj);
    const PixLib::Config &config = PixA::config<PixLib::PixModuleGroup,PixLib::PixModuleGroup>(const_cast<PixLib::PixModuleGroup *>(&obj));
    std::vector< std::pair< std::string, const PixLib::Config *> >config_list;
    config_list.push_back(std::make_pair(const_cast<PixLib::PixModuleGroup&>(obj).getName(), &config));
    {
      const PixLib::Config &sub_config = PixA::config<PixLib::PixModuleGroup,PixLib::PixBoc>(const_cast<PixLib::PixModuleGroup *>(&obj));
      config_list.push_back(std::make_pair(const_cast<PixLib::PixModuleGroup &>(obj).getPixBoc()->getCtrlName(),&sub_config) );
    }
    {
      const PixLib::Config &sub_config = PixA::config<PixLib::PixModuleGroup,PixLib::PixController>(const_cast<PixLib::PixModuleGroup *>(&obj));
      config_list.push_back(std::make_pair(const_cast<PixLib::PixModuleGroup &>(obj).getPixController()->getCtrlName(),&sub_config) );
    }

    return config_list;
  }

  /** Helper class to read configurations from the Db Server
   */
  class DbServerConfigHelper : public ConfigHelperWithManagerLink
  {
  public:
    DbServerConfigHelper(PixConnObjConfigDbManager &manager, const std::shared_ptr<PixLib::PixDbServerInterface> &db_server)
      : ConfigHelperWithManagerLink(manager),
	m_dbServer(db_server)
    {
      if (!m_dbServer) {
	throw std::runtime_error("FATAL [DbServerConfigHelper::ctor] Tried to construct db-server config helper without valid db-server.");
      }
    }

    void readConfig(PixLib::Config &config,
    		    TagPayload_t &tag_payload,
    		    const std::string &name,
    		    Revision_t revision,
    		    const CfgLocation_t &cfg_location) {
      _readConfig(config, tag_payload, name, revision);
    }

    bool writeConfig( const std::string &name,
		      const std::string &/*type_name*/,
		      const std::vector< std::pair<std::string, const PixLib::Config *> > &config_list,
		      TagPayload_t &tag_payload,
		      Revision_t revision,
		      CfgLocation_t &cfg_location,
		      const std::string &pending_tag) {

      assert(revision>0 && revision < UINT_MAX );
      bool ret=true;
      for (std::vector< std::pair<std::string , const PixLib::Config *> >::const_iterator config_iter=config_list.begin();
	   config_iter != config_list.end();
	   ++config_iter) {
	assert( config_iter->second);
	ret &= const_cast<PixLib::Config *>(config_iter->second)->write(m_dbServer.get(),
									DbServerRevisionIndex::makeCfgDomainName(tag_payload.idTag()),
									tag_payload.tag(),
									config_iter->first,
									const_cast<PixLib::Config *>(config_iter->second)->type(),
									pending_tag,
									revision);
      }
      cfg_location.setOnDbServer(true);
      return ret;
    }

    bool writeConfig( const PixLib::PixModuleGroup &pix_module_group,
		      TagPayload_t &tag_payload,
		      Revision_t revision,
		      CfgLocation_t &cfg_location,
		      const std::string &pending_tag) {
      
      bool ret =const_cast<PixLib::PixModuleGroup&>(pix_module_group).writeConfigAll(m_dbServer.get(),
									       DbServerRevisionIndex::makeCfgDomainName(tag_payload.idTag()),
									       tag_payload.tag(),
									       pending_tag,
									       revision);
      if (ret) {
	cfg_location.setOnDbServer(true);
      }
      return ret;
    }


    ConfigHandle modConf(TagPayload_t &tag_payload,
    			 const std::string &name,
    			 Revision_t revision,
    			 const CfgLocation_t &cfg_location) {
      return config<PixLib::PixModule,PixLib::PixModule>( tag_payload, name, revision, cfg_location);
    }

    ConfigHandle bocConf(TagPayload_t &tag_payload,
			 const std::string &name,
			 Revision_t revision,
			 const CfgLocation_t &cfg_location) {

      return config<PixLib::PixModuleGroup,PixLib::PixBoc>( tag_payload, name, revision, cfg_location);

    }

    ConfigHandle rodConf(TagPayload_t &tag_payload,
			 const std::string &name,
			 Revision_t revision,
			 const CfgLocation_t &cfg_location) {
      return config<PixLib::PixModuleGroup,PixLib::PixModuleGroup>( tag_payload, name, revision, cfg_location);
    }

    ConfigHandle controllerConf(TagPayload_t &tag_payload,
				const std::string &name,
				Revision_t revision,
				const CfgLocation_t &cfg_location) {
      return config<PixLib::PixModuleGroup,PixLib::PixController>( tag_payload, name, revision, cfg_location);
    }

    bool isSuitable(CfgLocation_t &cfg_location) const {
      return cfg_location.onDbServer();
    }


  private:

    void _readConfig(PixLib::Config &config,
    		    const TagPayload_t &tag_payload,
    		    const std::string &name,
    		    Revision_t revision) {
      if (!PixConnObjConfigDbTag::isValid(revision)) {
	revision = UINT_MAX;
      }
      DEBUG_TRACE( std::cout << "INFO [DbServerConfigHelper::_readConfig] read "
		   << " domain = "   << DbServerRevisionIndex::makeCfgDomainName(tag_payload.idTag())
		   << " tag = "      << tag_payload.tag()
		   << " name = "     << name
		<< " revision = " << revision
		   << std::endl )
      config.read(m_dbServer.get(),DbServerRevisionIndex::makeCfgDomainName(tag_payload.idTag()), tag_payload.tag(), name, revision);
    }

    template <class T_CompoundObj_t, class T_Obj_t>
    void readConfig(const std::shared_ptr<T_CompoundObj_t> &obj,
		    const TagPayload_t &tag_payload,
		    const std::string &name,
		    Revision_t revision)
    {
      _readConfig( PixA::config<T_CompoundObj_t,T_Obj_t>(obj), tag_payload, name, revision);
    }

    void readPixModuleGroupConfig(const std::shared_ptr<PixLib::PixModuleGroup> &obj,
				  const TagPayload_t &tag_payload,
				  const std::string &name,
				  Revision_t revision) {
      if (!PixConnObjConfigDbTag::isValid(revision)) {
	revision = UINT_MAX;
      }
      assert( obj->getName() == name );
      obj->readConfig(m_dbServer.get(),DbServerRevisionIndex::makeCfgDomainName(tag_payload.idTag()), tag_payload.tag(), revision);

      // _readConfig( PixA::config<PixLib::PixModuleGroup,PixLib::PixModuleGroup>(obj), tag_payload , name, revision);
      // _readConfig( PixA::config<PixLib::PixModuleGroup,PixLib::PixController>(obj), tag_payload, name+"_ROD", revision);
      // _readConfig( PixA::config<PixLib::PixModuleGroup,PixLib::PixBoc>(obj), tag_payload, name+"_BOC", revision);
    }


    template <class T_CompoundObj_t, class T_Obj_t> 
    ConfigHandle config(TagPayload_t &tag_payload,
			const std::string &name,
			Revision_t revision,
			const CfgLocation_t &cfg_location) {
 //       std::cout<<"name here 2_0: "<<name<<std::endl;
      std::string IDname=name;
      int pos = name.find("_flavour");
      if(pos!=(int)std::string::npos) IDname = name.substr(0,pos);
      std::shared_ptr<T_CompoundObj_t> obj( this->createObject<T_CompoundObj_t>(name) );
 //             std::cout<<"name here 2: "<<name<<std::endl;
      readConfig<T_CompoundObj_t, T_Obj_t>(obj, tag_payload, IDname, revision);
      tag_payload.cache<std::shared_ptr<T_CompoundObj_t> >().add(IDname,revision,obj);
 //       std::cout<<"name here 2_1: "<<name<<std::endl;
      return *(new PixConfigWrapperTopLevel<const T_CompoundObj_t>( obj, PixA::config<T_CompoundObj_t,T_Obj_t>(obj) ));
    }

    std::shared_ptr<PixLib::PixDbServerInterface> m_dbServer;
    static const std::string s_domainName;
  };



  const std::string DbServerConfigHelper::s_domainName("Configuration-");

  template <>
  inline void DbServerConfigHelper::readConfig<PixLib::PixModuleGroup, PixLib::PixController> (const std::shared_ptr<PixLib::PixModuleGroup> &obj,
											       const TagPayload_t &tag_payload,
											       const std::string &name,
											       Revision_t revision)
  { readPixModuleGroupConfig(obj, tag_payload, name, revision); }

  template <>
  inline void DbServerConfigHelper::readConfig<PixLib::PixModuleGroup, PixLib::PixBoc> (const std::shared_ptr<PixLib::PixModuleGroup> &obj,
											const TagPayload_t &tag_payload,
											const std::string &name,
											Revision_t revision)
  { readPixModuleGroupConfig(obj, tag_payload, name, revision); }

  template <>
  inline void DbServerConfigHelper::readConfig<PixLib::PixModuleGroup, PixLib::PixModuleGroup>(const std::shared_ptr<PixLib::PixModuleGroup> &obj,
											       const TagPayload_t &tag_payload,
											       const std::string &name,
											       Revision_t revision)
  { readPixModuleGroupConfig(obj, tag_payload, name, revision); }



  class HelperWithFileBackEnd
  {
  public:
    HelperWithFileBackEnd(const std::string &cfg_db_path)
      : m_cfgDbPath(cfg_db_path)
    {}

    static std::pair<std::string,unsigned short> fileName(CfgLocation_t  &cfg_location, const std::string &name, Revision_t revision) {
      std::stringstream file_name;
      file_name << name << "/" << name << "_"
		<< std::setw(12) << std::setfill('0') << revision;

      file_name << std::setw(3) <<  cfg_location.nextRepresentation();
      return std::make_pair<std::string, unsigned short>(file_name.str(), cfg_location.nextRepresentation());
    }

    void createDir(const std::string &name) {
      mode_t m = umask(0000);
      std::string dirn = m_cfgDbPath + "/" + name;
      mkdir(dirn.c_str(), 0777);
      umask(m);
    }

    const std::string cfgDbPath() const { return m_cfgDbPath; }
  private:
    std::string m_cfgDbPath;
  };

  /** Helper class to read configurations from the Db Server
   */
  class ConfigRootIOConfigHelper :  public ConfigHelperWithManagerLink, protected HelperWithFileBackEnd
  {
  public:
    ConfigRootIOConfigHelper(PixConnObjConfigDbManager &manager,
			     const std::string &cfg_db_path)
      : ConfigHelperWithManagerLink(manager),
	HelperWithFileBackEnd(cfg_db_path)
    { DEBUG_TRACE( std::cout << "INFO [RootDbConfigHelper::ctor] use db in " << cfg_db_path << "." << std::endl ) }

    void readConfig(PixLib::Config &config,
    		    TagPayload_t &tag_payload,
    		    const std::string &name,
    		    Revision_t revision,
    		    const CfgLocation_t &cfg_location) {
      _readConfig(config, tag_payload, name, revision, cfg_location);
    }

    bool writeConfig( const std::string &name,
		      const std::string &type_name,
		      const std::vector< std::pair<std::string, const PixLib::Config *> > &config_list,
		      TagPayload_t &tag_payload,
		      Revision_t revision,
		      CfgLocation_t &cfg_location,
		      const std::string &/*pending_tag*/) {
      bool ret=true;
      assert(revision>0 && revision < UINT_MAX );
      //      assert( matchesConfigName(config, name+name_ext) );

      std::pair<std::string,unsigned short> file_name = fileName(cfg_location, name, revision);
      if (!file_name.first.empty()) {
	file_name.first += ".root";

	createDir(name);
	for (std::vector< std::pair<std::string, const PixLib::Config *> >::const_iterator config_iter = config_list.begin();
	     config_iter != config_list.end();
	     ++config_iter) {
	  assert( config_iter->second );
	  std::shared_ptr<RootConf::IConfigStore> a_store(RootConf::ConfigStoreFactory::configStore());

	  // @todo extend ConfigRootIO interface to allow to pass a custom name for the time being the config name is temporily changed. This may cause problems in
	  // a multi threaded environment.

	  PixLib::Config *config = const_cast<PixLib::Config *>(config_iter->second);
	  std::string old_config_name = config->name();
	  config->name(config_iter->first );
	  a_store->write(*const_cast<PixLib::Config *>(config_iter->second), cfgDbPath() + file_name.first, s_inFileTag, revision);
	  config->name(old_config_name);
	  ret &=true;
	}
	cfg_location.setFileName(cfgDbPath() + file_name.first);
	cfg_location.setRepresentation(file_name.second);

	// have to register the file including the db path prefix
	registerRevision(tag_payload, type_name, name, revision,cfg_location);
	if (!cfg_location.fileName().empty()) {
	  // but in the cfg_location the name is stored without the prefix
	  cfg_location.setFileName(file_name.first);
	}
      }

      return ret;
    }

    bool writeConfig( const PixLib::PixModuleGroup &obj,
		      TagPayload_t &tag_payload,
		      Revision_t revision,
		      CfgLocation_t &cfg_location,
		      const std::string &pending_tag) {

      std::vector< std::pair< std::string, const PixLib::Config *> > config_list = makeConfigList(obj);
      writeConfig(const_cast<PixLib::PixModuleGroup&>(obj).getName(), "PixModuleGroup", config_list, tag_payload, revision, cfg_location, pending_tag);
      return true;
    }


    ConfigHandle modConf(TagPayload_t &tag_payload,
    			 const std::string &name,
    			 Revision_t revision,
    			 const CfgLocation_t &cfg_location) {
      return config<PixLib::PixModule,PixLib::PixModule>( tag_payload, name, revision, cfg_location);
    }

    ConfigHandle bocConf(TagPayload_t &tag_payload,
			 const std::string &name,
			 Revision_t revision,
			 const CfgLocation_t &cfg_location) {

      return config<PixLib::PixModuleGroup,PixLib::PixBoc>( tag_payload, name, revision, cfg_location);

    }

    ConfigHandle rodConf(TagPayload_t &tag_payload,
			 const std::string &name,
			 Revision_t revision,
			 const CfgLocation_t &cfg_location) {
      return config<PixLib::PixModuleGroup,PixLib::PixModuleGroup>( tag_payload, name, revision, cfg_location);
    }

    ConfigHandle controllerConf(TagPayload_t &tag_payload,
				const std::string &name,
				Revision_t revision,
				const CfgLocation_t &cfg_location) {
      return config<PixLib::PixModuleGroup,PixLib::PixController>( tag_payload, name, revision, cfg_location);
    }

    bool isSuitable(CfgLocation_t &cfg_location) const  {
      if (cfg_location.isTemporaryCfg()) return false;
      cfg_location.determineFileType(cfgDbPath() );
      return (cfg_location.backEnd()==CfgLocation_t::kConfigRootIO);
    }


  private:

    void _readConfig(PixLib::Config &config,
		     TagPayload_t &tag_payload,
		     const std::string &name,
		     Revision_t revision,
		     const CfgLocation_t &cfg_location) {
      if (!PixConnObjConfigDbTag::isValid(revision)) {
	revision = UINT_MAX;
      }
//      assert( matchesConfigName(config, name) );
      //      m_store->read(config, m_cfgDbPath + cfg_location.fileName(),tag_payload.tag(),revision);
      std::shared_ptr<RootConf::IConfigStore> a_store(RootConf::ConfigStoreFactory::configStore());
      try {
        a_store->read(config, cfgDbPath() + cfg_location.fileName(),s_inFileTag,revision);
      }
      catch(RootConf::RevisionNotFound &) {
      std::string old_name=config.m_confName;
      try {
        config.m_confName = name;
	a_store->read(config, cfgDbPath() + cfg_location.fileName(),s_inFileTag,revision);       
      }
      catch(RootConf::RevisionNotFound &) {
	std::cout << "INFO [ConfigRootIOConfigHelper::_readConfig] Trie to search revision " << revision << " for " << name << " in "
		  << cfgDbPath() + cfg_location.fileName() << " / all" <<std::endl;
	a_store->read(config, cfgDbPath() + cfg_location.fileName(),"all",revision);
      }
      config.m_confName=old_name;
    }
    }

    template <class T_CompoundObj_t, class T_Obj_t>
    void readConfig(const std::shared_ptr<T_CompoundObj_t> &obj,
		    TagPayload_t &tag_payload,
		    const std::string &name,
		    Revision_t revision,
		    const CfgLocation_t &cfg_location)
    {
      _readConfig( PixA::config<T_CompoundObj_t,T_Obj_t>(obj), tag_payload, name, revision, cfg_location);
    }

    void readPixModuleGroupConfig(const std::shared_ptr<PixLib::PixModuleGroup> &obj,
				  TagPayload_t &tag_payload,
				  const std::string &name,
				  Revision_t revision,
				  const CfgLocation_t &cfg_location) {
      _readConfig( PixA::config<PixLib::PixModuleGroup,PixLib::PixModuleGroup>(obj), tag_payload, name, revision, cfg_location);
      _readConfig( PixA::config<PixLib::PixModuleGroup,PixLib::PixController>(obj), tag_payload, name+"_ROD", revision, cfg_location);
      _readConfig( PixA::config<PixLib::PixModuleGroup,PixLib::PixBoc>(obj), tag_payload, name+"_BOC", revision, cfg_location);
    }


    template <class T_CompoundObj_t, class T_Obj_t> 
    ConfigHandle config(TagPayload_t &tag_payload,
			const std::string &name,
			Revision_t revision,
			const CfgLocation_t &cfg_location) {

      std::shared_ptr<T_CompoundObj_t> obj( this->createObject<T_CompoundObj_t>(name) );
 //             std::cout<<"name here 3: "<<name<<std::endl;
      readConfig<T_CompoundObj_t, T_Obj_t>(obj, tag_payload, name, revision, cfg_location);
      tag_payload.cache<std::shared_ptr<T_CompoundObj_t> >().add(name,revision,obj);

      return *(new PixConfigWrapperTopLevel<const T_CompoundObj_t>( obj, PixA::config<T_CompoundObj_t,T_Obj_t>(obj) ));
    }

    //    std::shared_ptr<RootConf::IConfigStore> m_store;
    static const std::string s_domainName;
    static const std::string s_inFileTag;
  };

  const std::string ConfigRootIOConfigHelper::s_inFileTag("Def");

  template <>
  inline void ConfigRootIOConfigHelper::readConfig<PixLib::PixModuleGroup, PixLib::PixController> (const std::shared_ptr<PixLib::PixModuleGroup> &obj,
												   TagPayload_t &tag_payload,
												   const std::string &name,
												   Revision_t revision,
												   const CfgLocation_t &cfg_location)
  { readPixModuleGroupConfig(obj, tag_payload, name, revision, cfg_location); }

  template <>
  inline void ConfigRootIOConfigHelper::readConfig<PixLib::PixModuleGroup, PixLib::PixBoc> (const std::shared_ptr<PixLib::PixModuleGroup> &obj,
											    TagPayload_t &tag_payload,
											    const std::string &name,
											    Revision_t revision,
											    const CfgLocation_t &cfg_location)
  { readPixModuleGroupConfig(obj, tag_payload, name, revision, cfg_location); }

  template <>
  inline void ConfigRootIOConfigHelper::readConfig<PixLib::PixModuleGroup, PixLib::PixModuleGroup>(const std::shared_ptr<PixLib::PixModuleGroup> &obj,
												   TagPayload_t &tag_payload,
												   const std::string &name,
												   Revision_t revision,
												   const CfgLocation_t &cfg_location)
  { readPixModuleGroupConfig(obj, tag_payload, name, revision, cfg_location); }



  class RootDbConfigHelper : public IConfigHelper, protected HelperWithFileBackEnd
  {
  public:
    RootDbConfigHelper(const std::string &cfg_db_path)
      : HelperWithFileBackEnd(cfg_db_path) { DEBUG_TRACE( std::cout << "INFO [RootDbConfigHelper::ctor] use db in " << cfgDbPath() << "." << std::endl ) }

    void readConfig(PixLib::Config &config,
		    TagPayload_t &tag_payload,
		    const std::string &name,
		    Revision_t revision,
		    const CfgLocation_t &cfg_location) {

	std::vector<std::string> record_hierarchy;
	std::string config_name(name);
	std::cout << "INFO [RootDbConfigHelper::readConfig] config type is " << config.type() << std::endl;
	if (config.type() == "PixBoc") {
	  std::string::size_type end_pos = name.size();
	  if (name.size()>4 && name.compare(name.size()-4,4,"_BOC")==0) {
	    end_pos = name.size()-4;
	  }
	  record_hierarchy.push_back(name.substr(0,end_pos));
	  if (end_pos == name.size()) {
	    config_name += "_BOC";
	  }
	}
	else if (config.type() == "PixController" || config.type()== "RodPixController") {

	  std::string::size_type end_pos = name.size();
	  if (name.size()>4 && name.compare(name.size()-4,4,"_ROD")==0) {
	    end_pos = name.size()-4;
	  }
	  record_hierarchy.push_back(name.substr(0,end_pos));
	  if (end_pos == name.size()) {
	    config_name += "_ROD";
	  }
	}

	record_hierarchy.push_back(config_name);
	DbHandle  db_handle = DbManager::dbHandle(cfgDbPath() + cfg_location.fileName(),record_hierarchy);
	DbRef db_ref(db_handle.ref());
	config.read(db_ref.record().get());
    }

    bool writeConfig( const std::string &/*name*/,
		      const std::string &/*type_name*/,
		      const std::vector< std::pair< std::string, const PixLib::Config *> >&/*config_list*/,
		      TagPayload_t &/*tag_payload*/,
		      Revision_t /*revision*/,
		      CfgLocation_t &/*cfg_location*/,
		      const std::string &/*pending_tag*/) {

	throw PixA::ConfigException( "FATAL [RootDbConfigHelper::writeConfig] Writing to RootDb files is not supported.");
	return false;
    }

    bool writeConfig( const PixLib::PixModuleGroup &obj,
		      TagPayload_t &tag_payload,
		      Revision_t revision,
		      CfgLocation_t &cfg_location,
		      const std::string &pending_tag) {

      std::vector< std::pair< std::string, const PixLib::Config *> >config_list = makeConfigList(obj);
      return writeConfig(const_cast<PixLib::PixModuleGroup&>(obj).getName(), "PixModuleGroup", config_list, tag_payload, revision, cfg_location, pending_tag);
    }

    ConfigHandle modConf(TagPayload_t &tag_payload,
    			 const std::string &name,
    			 Revision_t revision,
    			 const CfgLocation_t &cfg_location) {
      return createOnDemandConfig(tag_payload, name, revision, cfg_location,"");
    }

    ConfigHandle bocConf(TagPayload_t &tag_payload,
			 const std::string &name,
			 Revision_t revision,
			 const CfgLocation_t &cfg_location) {
      return createOnDemandConfig(tag_payload, name, revision, cfg_location,"_BOC");
    }

    ConfigHandle rodConf(TagPayload_t &tag_payload,
			 const std::string &name,
			 Revision_t revision,
			 const CfgLocation_t &cfg_location) {
      return createOnDemandConfig(tag_payload, name, revision, cfg_location,"");
    }

    ConfigHandle controllerConf(TagPayload_t &tag_payload,
				const std::string &name,
				Revision_t revision,
				const CfgLocation_t &cfg_location) {
      return createOnDemandConfig(tag_payload, name, revision, cfg_location,"_ROD");
    }


    bool isSuitable(CfgLocation_t &cfg_location) const {
      if (cfg_location.isTemporaryCfg()) return false;
      cfg_location.determineFileType(cfgDbPath());
      return (cfg_location.backEnd()==CfgLocation_t::kRootDb);
    }


    ConfigHandle createOnDemandConfig(const TagPayload_t &/*tag_payload*/,
				      const std::string &name,
				      Revision_t revision,
				      const CfgLocation_t &cfg_location,
				      const std::string &name_extension)
    {//std::cout<<"get db wrapper in conn"<<std::endl;

      try {

	DEBUG_TRACE( std::cout << "INFO [RootDbConfigHelper::createOnDemandConfig] file = " << cfgDbPath() + cfg_location.fileName() << "." << std::endl )
	DbWrapper *db_wrapper = DbManager::instance()->getDbWrapper(cfgDbPath() + cfg_location.fileName());
	std::vector<std::string> record_hierarchy;
	std::string record_name( name );

	record_hierarchy.push_back(record_name);
	if (!name_extension.empty())
	  record_hierarchy.push_back(record_name+name_extension);
	//      record_hierarchy.push_back(record_name+"_ROD");
	//      record_hierarchy.push_back(record_name+"_BOC");
        //   std::cout<<"si sono io 3"<<std::endl;
	return *(new OnDemandConfig(*db_wrapper,record_hierarchy));
      }
      catch (PixLib::PixDBException &err) {
	throw PixA::ConfigException(err.getDescriptor());
      }
    }

  };

  IRevisionIndex *PixConnObjConfigDbManager::createRevisionIndex() {
    IRevisionIndex *revision_index=NULL;
    {
      const char *pix_cfg_index_db = getenv("PIX_CFG_DB");
      if (!pix_cfg_index_db || strlen(pix_cfg_index_db)==0) {
	throw PixA::ConfigException( "FATAL [PixConnObjConfigDbManager::ctor] Connection string for configuration index DB is undefined"
				     " Define environment variable PIX_CFG_DB.");
      }
      revision_index = new     CoralDBRevisionIndex(pix_cfg_index_db);
    }

    return revision_index;
  }


  PixConnObjConfigDbManager::PixConnObjConfigDbManager(const std::shared_ptr<PixLib::PixDbServerInterface> &db_server,
						       IRevisionIndex *revision_index,
						       CfgLocation_t::EBackEnd store_back_end)
    : m_dbServer(db_server),
      m_revisionIndex(revision_index),
      m_dbServerRevisionIndex((db_server.get() ? new DbServerRevisionIndex(db_server) : static_cast<DbServerRevisionIndex *>(NULL)) ),
      m_storeBackEnd(store_back_end)
  {

    if (s_defaultCfgPath.empty()) {
      //      s_defaultCfgPath.push_back("/data1/db/cfg");
      //      s_defaultCfgPath.push_back("/daq/db/cfg");
      s_defaultCfgPath.push_back("*/db/cfg/");
    }

    std::string cfg_db_path;
    {
      const char * pix_cfg_pref = getenv("PIX_CFG_PREF");
      if (!pix_cfg_pref || strlen(pix_cfg_pref)==0) {
	throw PixA::ConfigException( "FATAL [PixConnObjConfigDbManager::ctor] Do not know location of pix configuration db."
				     " Environment variable PIX_CFG_PREF is undefined.");
      }
      cfg_db_path=pix_cfg_pref;
      if (cfg_db_path.size()>1 && cfg_db_path[cfg_db_path.size()-1]!='/') {
	cfg_db_path+='/';
      }
    }
    m_cfgDbPath = cfg_db_path;

    m_configHelper.resize(CfgLocation_t::kNBackEnds, NULL);
    if (db_server) {
      m_configHelper[CfgLocation_t::kDbServer]=new DbServerConfigHelper( *this, db_server);
    }
    else if (store_back_end == CfgLocation_t::kDbServer) {
      store_back_end=CfgLocation_t::kConfigRootIO;
    }
    m_configHelper[CfgLocation_t::kRootDb]=new RootDbConfigHelper(cfg_db_path);
    m_configHelper[CfgLocation_t::kConfigRootIO]=new ConfigRootIOConfigHelper(*this, cfg_db_path);

    if (!m_configHelper[m_storeBackEnd]) {
      m_storeBackEnd = CfgLocation_t::kUnknown;
      DEBUG_TRACE( std::cout << "WARNING [PixConnObjConfigDbManager::ctor] Selected back-end for storing configs is not available." << std::endl )
    }
  }

  PixConnObjConfigDbManager::~PixConnObjConfigDbManager() {

    DEBUG_TRACE( dumpRevisions() )
    
    for(std::vector<IConfigHelper *>::iterator config_helper_iter = m_configHelper.begin();
	config_helper_iter != m_configHelper.end();
	++config_helper_iter) {
      delete *config_helper_iter;
      *config_helper_iter = NULL;
    }

  }

  void PixConnObjConfigDbManager::getIdTags(std::vector<std::string> &id_tag_out) {

    try {
      if (m_dbServerRevisionIndex.get()) m_dbServerRevisionIndex->getIdTags(id_tag_out);
    }
    catch(...) {
    }

    try {
      m_revisionIndex->getIdTags(id_tag_out);
    }
    catch(...) {
    }

    sort(id_tag_out.begin(),id_tag_out.end());
  }

  void PixConnObjConfigDbManager::getTags(const std::string &id_tag, std::vector<std::string> &tag_out) {

    try {
      if (m_dbServerRevisionIndex.get()) m_dbServerRevisionIndex->getTags(id_tag,tag_out);
    }
    catch (...) {
    }

    try {
      m_revisionIndex->getTags(id_tag,tag_out);
    }
    catch (...) {
    }

    sort(tag_out.begin(),tag_out.end());
  }

  void PixConnObjConfigDbManager::getRevisions(const std::string &id_tag, const std::string &cfg_tag, const std::string &conn_name, std::set<Revision_t> &revisions_out) {
    try {
      if (m_dbServerRevisionIndex.get()) m_dbServerRevisionIndex->getRevisions(id_tag,cfg_tag, conn_name, revisions_out);
    }
    catch(...) {
    }

    try {
      m_revisionIndex->getRevisions(id_tag,cfg_tag, conn_name, revisions_out);
    }
    catch(...) {
    }
  }

  void PixConnObjConfigDbManager::getNames(const std::string &id_tag, const std::string &cfg_tag, EObjectType type, std::set<std::string> &names)
  {
    assert( static_cast<unsigned short>(kNObjectTypes) == static_cast<unsigned short>(IRevisionIndex::kNTypes));
    IRevisionIndex::EObjectType object_type = static_cast<IRevisionIndex::EObjectType>(type);
    try {
      if (m_dbServerRevisionIndex.get()) m_dbServerRevisionIndex->getNames(id_tag,cfg_tag, object_type, names);
    }
    catch(...) {
    }

    const char *coral_revisions=getenv("PIXA_ONLY_DBSERVER_REVISIONS");
    if (!coral_revisions || (strlen(coral_revisions)>0 && strcmp(coral_revisions,"yes")!=0)) {
    try {
      m_revisionIndex->getNames(id_tag,cfg_tag, object_type, names);
    }
    catch(...) {
    }
    }
  }

  
  std::string PixConnObjConfigDbManager::stripDefaultPath(const std::string &file_name) {

    for ( std::vector<std::string>::const_iterator default_cfg_path_iter = s_defaultCfgPath.begin();
	  default_cfg_path_iter != s_defaultCfgPath.end();
	  ++default_cfg_path_iter) {
      std::string::size_type pos;
      if (default_cfg_path_iter->size()>0 && (*default_cfg_path_iter)[0]=='*') {
	pos = file_name.find( &((*default_cfg_path_iter)[1]) );
	if (pos == std::string::npos)  continue;

	pos += default_cfg_path_iter->size()-1;
      }
      else if ( file_name.compare(0,default_cfg_path_iter->size(), *default_cfg_path_iter )==0) {
	pos= default_cfg_path_iter->size();
      }
      else {
	continue;
      }
      const char *file_name_ptr=file_name.c_str();
      while (file_name_ptr[pos]=='/') pos++;
      return file_name.substr(pos,file_name.size()-pos);

    }

    if ( file_name.compare(0,m_cfgDbPath.size(), m_cfgDbPath )==0) {
      std::string::size_type pos= m_cfgDbPath.size();
      const char *file_name_ptr=file_name.c_str();
      while (file_name_ptr[pos]=='/') pos++;
      return file_name.substr(pos,file_name.size()-pos);
    }
    return file_name;
  }

  ConnObjConfigDbTagRef PixConnObjConfigDbManager::getTag(const std::string &id_tag, const std::string &cfg_tag, Revision_t revision) {
    if (!PixConnObjConfigDbTag::isValid(revision)) revision=time(0);
    DEBUG_TRACE( std::cout << "INFO [PixConnObjConfigDbManager::getTag] " << id_tag << "/" << cfg_tag << ";" << revision << "." << std::endl )
    std::unique_ptr<PixConnObjConfigDbTag> config_tag(new PixConnObjConfigDbTag(*this, tagList(id_tag,cfg_tag),revision));
    return ConnObjConfigDbTagRef(config_tag.release());
  }


  bool PixConnObjConfigDbManager::updateRevisions(const std::string &id_tag, const std::string &cfg_tag, ObjectList_t &object_list, Revision_t target_revision)
  {
    DEBUG_TRACE( std::cout << "INFO [PixConnObjConfigDbManager::updateRevisions] " << id_tag << "/" << cfg_tag << " target :" << target_revision << "." << std::endl )
    Revision_t lower_bound = target_revision - 3600*24*7; // just consider revisions one week older
    Revision_t upper_bound = target_revision + 3600*24*7; // and one week younger (at most the current time)
    Revision_t current_upper_revision_limit=time(0);  // just consider revisions one week newer or the current time
    if (current_upper_revision_limit <upper_bound) {
      upper_bound=current_upper_revision_limit;
    }
    RevisionInserter inserter(*this, object_list, lower_bound, upper_bound);

    try {
      m_revisionIndex->processRevision(id_tag, cfg_tag, target_revision, lower_bound, upper_bound, inserter);
    }
    catch (...) {
    }

    if (m_dbServerRevisionIndex.get()) {
      inserter.setOnDbServer(true);
      // @todo could try to get the highest revions of all conn objects which is equal to or below the target revision
      try {
	m_dbServerRevisionIndex->processRevision(id_tag, cfg_tag, target_revision, 0, current_upper_revision_limit, inserter);
      }
      catch(...) {
      }
    }

    DEBUG_TRACE( dumpRevisions())
    return true;
  }

  
  const char *s_backEndName[]={"Unknown", "DbServer", "File", "File:ConfigRootIO", "File:RootDb" };
  std::string backEndName(CfgLocation_t::EBackEnd back_end) {
    if (back_end<CfgLocation_t::kNBackEnds) {
      return s_backEndName[back_end];
    }
    else {
      std::stringstream name;
      name << s_backEndName[0] << "(" << static_cast<int>(back_end) << ") ";
      return name.str();
    }
  }

  CfgLocation_t &PixConnObjConfigDbManager::cfgLocation(TagPayload_t &tag_payload, const std::string &name, Revision_t revision)
  {
    for (unsigned int pass_i=0; pass_i<2; pass_i++) {
      CfgLocation_t *cfg_location = tag_payload.cfgLocation(name,revision);

      DEBUG_TRACE( std::cout << "INFO [PixConnObjConfigDbManager::cfgLocation] name = " << name << " revision = "  << revision 
		   << " (" << makeTimeString(revision) << ") "
		   << " [" << static_cast<void *>(cfg_location) << "] " ;
		   if (cfg_location) {
		     std::cout << " valid till = " << (cfg_location->isValidityCutOff() ? "~" : "" ) << cfg_location->validTill();
		   }
		   std::cout << std::endl )

      if (cfg_location) {
	/* DEBUG_TRACE(*/ std::cout << "INFO [PixConnObjConfigDbManager::cfgLocation] name = " << name 
	             << " tag = " << tag_payload.idTag() << " : " << tag_payload.tag()
                     << " revision = "  << revision << " (" << makeTimeString(revision) << ") " << " ["
		     << static_cast<void *>(cfg_location) << "] "
		     << " back-end = " << backEndName( cfg_location->backEnd() )
		     << " file=" << cfg_location->fileName()
		     << std::endl; /*)*/
	//std::cout<<"returning cfg location"<<std::endl;
	return *cfg_location;
      }
    //  std::cout<<"I am not here"<<std::endl;
      if (pass_i == 1 || ! updateRevisions(tag_payload.idTag(), tag_payload.tag(), tag_payload.objectIndex(), revision)) {
	//std::cout<<"passing by here"<<std::endl;
	break;
      }
    }

    std::stringstream message;
    message << "FATAL [PixConnObjConfigDbManager::cfgLocation] No configuration for "
	    << name << " revision " << revision << ".";
    DEBUG_TRACE( std::cout << message.str() << std::endl )
    throw ConfigException(message.str());
   // std::cout<<"passing by here 1"<<std::endl;
  }

  // @todo should go into back-end
  std::pair<std::string,unsigned short> PixConnObjConfigDbManager::fileName(TagPayload_t &tag_payload, const std::string &name, Revision_t revision) {
    std::stringstream file_name;
    file_name << name << "/" << name << "_"
	      << std::setw(12) << std::setfill('0') << revision;

    try {
     // std::cout<<"passing by here 2"<<std::endl;
      //std::cout<<"file name at this point is: "<<file_name<<std::endl;
      CfgLocation_t *cfg_location = tag_payload.cfgLocationExactMatch(name,revision);
      if(cfg_location) {

	cfg_location->setRepresentation(cfg_location->representation()+1);
	file_name << std::setw(3) <<  cfg_location->representation();
	return std::make_pair<std::string, unsigned short>(file_name.str(), cfg_location->representation());
      }
    }
    catch(...) {
    }

    file_name << std::setw(3) << 0;
    //std::cout<<"passing by here 3"<<std::endl;
    return std::make_pair<std::string, unsigned short>(file_name.str(), 0);
  }

  // @todo should go into revision index
  void  PixConnObjConfigDbManager::registerRevision(TagPayload_t &tag_payload,
						    const std::string &type_name,
						    const std::string &name,
						    Revision_t revision,
						    CfgLocation_t &cfg_location) {
    std::string error_message;
    try {
      ObjectList_t object_list = tag_payload.objectIndex();
      RevisionList_t &revision_list= object_list[ name ];

      // mimick structure produced by PixDbInterface for compatibility
      bool in_index=false;
      for (RevisionList_t::iterator all_rev_iter = revision_list.begin();
	   all_rev_iter != revision_list.end();
	   ++all_rev_iter) {
	if (!all_rev_iter->second.isTemporaryCfg()) {
	  in_index = true;
	  break;
	}
      }
      if (m_revisionIndex->addRevision(tag_payload.idTag(),tag_payload.tag(),type_name, name, in_index,revision, cfg_location.fileName())) {
	// success
	return;
      }

    }
    catch(std::exception &err) {
      error_message =err.what();
    }
    catch(...) {
    }

    cfg_location.setFileName(""); // will set back-end to unknown in case the config is not on the db server

    std::stringstream message;
    message << "FATAL [PixConnObjConfigDbManager::cfgLocation] Failed to write configuration for "
	    << name << " revision " << revision << ".";
    if (!error_message.empty()) {
      message << " Error : " << error_message << std::endl;
    }
    throw ConfigException(message.str());
  }

  // @todo to revision index ?
  bool PixConnObjConfigDbManager::cloneTag(const std::string &id_tag,
					   const std::string &src_tag,
					   const std::string &dest_tag,
					   const std::string &pending_tag) {
    if (!dbServer()) return false;
    try {
      return dbServer()->cloneTag(DbServerRevisionIndex::makeCfgDomainName(id_tag), src_tag,dest_tag, pending_tag);
    }
    catch ( ... ) {
    }
    return false;
  }

  template <> inline PixLib::PixModule *PixConnObjConfigDbManager::createObject<PixLib::PixModule>(const std::string &name) {
    // To load an arbitrary revision, first the object is constructed with a default configuration.
    // then the correct revision is loaded manually
      
    // Trick, to create the object with the default configuration, the module is constructed with an invalid domain name
    //    return new PixLib::PixModule(m_dbServer.get(),NULL, "trick", "", name);

    // retrieve flavour information from manipulated prod. ID
  //  std::cout<<"name is: " << name <<std::endl;
    std::string IDname=name, flname="";
    int pos = name.find("_flavour");
    int mccFlv = PixLib::PixModule::PM_MCC_I2;
    int feFlv = PixLib::PixModule::PM_FE_I2;
   // std::cout<<"mccFlv: "<<mccFlv<<std::endl;
  //  std::cout<<"feFlv: "<<feFlv<<std::endl;
    int nFe=16;
    if(pos!=(int)std::string::npos){
      IDname = name.substr(0,pos);
      flname = name.substr(pos+9, name.length()-pos-9);
      int flpos=-1, oldpos=0;
      for(int i=0;i<3;i++){
	std::stringstream a;
	oldpos = flpos+1;
	flpos = flname.find("-", oldpos);
	if(flpos==(int)std::string::npos) flpos = flname.length();
	a << flname.substr(oldpos,flpos-oldpos);
	switch(i){
	case 0:
	  a >> mccFlv;
	  break;
	case 1:
	  a >> feFlv;
	  break;
	case 2:
	  a >> nFe;
	  break;
	}
      }
    }
    std::cerr << "INFO [PixLib::PixModule] Create PixModule which will be configured later, Ignore the following errors about : "
	      << "default configuration, and problem in reading configuration. " << std::endl;
    //std::cout<<"yes here I am giving information"<<std::endl;
    std::cout<<IDname<<" "<<mccFlv<<" "<<feFlv<<" "<<nFe<<std::endl;
    return new PixLib::PixModule("", "", IDname, mccFlv, feFlv, nFe);
  }

  template <> inline PixLib::PixDisable *PixConnObjConfigDbManager::createObject<PixLib::PixDisable>(const std::string &name) {
    return new PixLib::PixDisable( name);
  }

  template <> inline PixLib::PixModuleGroup *PixConnObjConfigDbManager::createObject<PixLib::PixModuleGroup>(const std::string &name) {
    // retrieve ROD flavour information from manipulated name
    std::string IDname=name;
    PixLib::CtrlType ctt = PixLib::ROD;
    int pos = name.find("_flavour");
    int temp;
    if(pos!=(int)std::string::npos){
      IDname = name.substr(0,pos);
      std::stringstream a;
      a << name.substr(pos+9, name.length()-pos-9);
      a >> temp;
      ctt=static_cast<PixLib::CtrlType>(temp);
    }
    // now we can create the module group
    return new PixLib::PixModuleGroup(IDname, ctt);
  }

  template <> inline PixLib::PixTrigController *PixConnObjConfigDbManager::createObject<PixLib::PixTrigController>(const std::string &name) {
    // now we can create the boc
    return new PixLib::TimPixTrigController(name);
  }


  template <class T_CompoundObj_t> void readConfig(T_CompoundObj_t &obj,
						    IConfigHelper &config_helper,
						    TagPayload_t &tag_payload,
						    const std::string &name,
						    Revision_t revision,
						    CfgLocation_t &cfg_location){
    config_helper.readConfig( config<T_CompoundObj_t,T_CompoundObj_t>(&obj), tag_payload, name, revision, cfg_location);
  }

  template <> void readConfig<PixLib::PixModuleGroup>(PixLib::PixModuleGroup &obj,
						      IConfigHelper &config_helper,
						      TagPayload_t &tag_payload,
						      const std::string &name,
						      Revision_t revision,
						      CfgLocation_t &cfg_location) {
    config_helper.readConfig( obj.config(), tag_payload, name, revision, cfg_location);
    if (obj.getPixController()) {
      config_helper.readConfig( obj.getPixController()->config(), tag_payload, name+"_ROD", revision, cfg_location);
    }
    if (obj.getPixBoc() && obj.getPixBoc()->getConfig()) {
      config_helper.readConfig( *(obj.getPixBoc()->getConfig()), tag_payload, name+"_BOC", revision, cfg_location);
    }
  }

  
  void dumpCfgLocation(const std::string &id_tag, const std::string &tag, Revision_t revision, const std::string &name, CfgLocation_t *cfg_location)
  {
    static const char *back_end_names[CfgLocation_t::kNBackEnds]={"Unknown", "DbServer", "File", "ConfigRootIO", "RootDb" };

    std::cout << "INFO [dumpCfgLocation] " << name << " " << id_tag << "/" << tag << ";" << revision << " : ";
    if (cfg_location) {

	CfgLocation_t::EBackEnd back_end = (cfg_location->backEnd() < CfgLocation_t::kNBackEnds ? cfg_location->backEnd() : CfgLocation_t::kUnknown);
	std::cout << "\t <" << revision << " - "
		  << (cfg_location->isValidityCutOff() ? "~" : "")
		  << cfg_location->validTill() 
		  << " ( <" << makeTimeString( revision ) << " - " << makeTimeString( cfg_location->validTill() ) << " ) "
		  << " : "
		  << " back-end="  << back_end_names[back_end]
		  << " file=" << cfg_location->fileName()
		  << " used=" << cfg_location->useCounter();
	if (cfg_location->onDbServer()) {
	  std::cout << " on-db";
	}
	if (cfg_location->isTemporaryCfg()) {
	  std::cout << " temporary";
	}
    }
    else {
      std::cout << " <unknown>" << std::endl;
    }
    std::cout << std::endl;
  }



  template <class T_CompoundObj_t>
  inline T_CompoundObj_t *PixConnObjConfigDbManager::createConfiguredObject(TagPayload_t &tag_payload,
									    const std::string &name,
									    Revision_t revision)
  {
    std::unique_ptr<T_CompoundObj_t> obj( createObject<T_CompoundObj_t>(name) );
   //   std::cout<<"name here 1: "<<name<<std::endl;
    // then the configuration is loaded manually
    CfgLocation_t &cfg_location=this->cfgLocation(tag_payload, name, revision);
    IConfigHelper &config_helper = configHelper(cfg_location);
    readConfig<T_CompoundObj_t>(*obj, config_helper, tag_payload, name, revision, cfg_location);
    return obj.release();
  }

  



  void PixConnObjConfigDbManager::configure(PixLib::Config &config,
					    TagPayload_t &tag_payload,
					    const std::string &name,
					    Revision_t revision)
  {

    CfgLocation_t &cfg_location=cfgLocation(tag_payload, name, revision);
    IConfigHelper &config_helper = configHelper(cfg_location);
    config_helper.readConfig( config, tag_payload, name, revision, cfg_location);
  }

  template <class T_CompoundObj_t> std::shared_ptr<const T_CompoundObj_t> PixConnObjConfigDbManager::cacheObject(TagPayload_t &tag_payload,
														   const std::string &name,
														   Revision_t revision) {

    // If the revision is zero the most recent revision is requested, so the revision is set to the maximum
    if (revision==0) revision--;

    std::shared_ptr<const T_CompoundObj_t> obj;
    std::shared_ptr<T_CompoundObj_t> *obj_ptr( tag_payload.cache<std::shared_ptr<T_CompoundObj_t> >().object(name,revision) );

    if (!obj_ptr ) {
      // first create an object with a default configuraitno, since the object cannot be constructed with a certain revision
      std::shared_ptr<T_CompoundObj_t> an_obj( createConfiguredObject<T_CompoundObj_t>(tag_payload, name, revision) );

      // cache the object
      tag_payload.cache<std::shared_ptr<T_CompoundObj_t> >().add(name,revision,an_obj);
      obj = an_obj;
    }
    else {
      obj = *obj_ptr;
    }
    return obj;
  }



  void TagPayload_t::dumpRevisions() const {
    static const char *back_end_names[CfgLocation_t::kNBackEnds]={"Unknown", "DbServer", "File", "ConfigRootIO", "RootDb" };

    for (ObjectList_t::const_iterator obj_iter = m_objectIndex.begin();
	 obj_iter != m_objectIndex.end();
	 ++obj_iter) {
      std::cout << "- " << obj_iter->first << std::endl;
      for (RevisionList_t::const_iterator rev_iter = obj_iter->second.begin();
	   rev_iter != obj_iter->second.end();
	   ++rev_iter) {
	CfgLocation_t::EBackEnd back_end = (rev_iter->second.backEnd() < CfgLocation_t::kNBackEnds ? rev_iter->second.backEnd() : CfgLocation_t::kUnknown);
	std::cout << "\t" << rev_iter->first << " - " 
		  << (rev_iter->second.isValidityCutOff() ? "~" : "")
		  << rev_iter->second.validTill() 
		  << " ( " << makeTimeString( rev_iter->first ) << " - " << makeTimeString( rev_iter->second.validTill() ) << " ) "
		  << " : "
		  << " back-end="  << back_end_names[back_end]
		  << " file=" << rev_iter->second.fileName()
		  << " used=" << rev_iter->second.useCounter();
	if (rev_iter->second.onDbServer()) {
	  std::cout << " on-db";
	}
	if (rev_iter->second.isTemporaryCfg()) {
	  std::cout << " temporary";
	}
	std::cout << std::endl;
      }
    }
  }

  void TagPayload_t::dumpRevisions(const std::string &name) const {
    static const char *back_end_names[CfgLocation_t::kNBackEnds]={"Unknown", "DbServer", "File", "ConfigRootIO", "RootDb" };

    ObjectList_t::const_iterator obj_iter = m_objectIndex.find(name);
    if (obj_iter != m_objectIndex.end() ) {
      std::cout << "- " << obj_iter->first << std::endl;
      for (RevisionList_t::const_iterator rev_iter = obj_iter->second.begin();
	   rev_iter != obj_iter->second.end();
	   ++rev_iter) {
	CfgLocation_t::EBackEnd back_end = (rev_iter->second.backEnd() < CfgLocation_t::kNBackEnds ? rev_iter->second.backEnd() : CfgLocation_t::kUnknown);
	std::cout << "\t" << rev_iter->first << " - " 
		  << (rev_iter->second.isValidityCutOff() ? "~" : "")
		  << rev_iter->second.validTill() 
		  << " ( " << makeTimeString( rev_iter->first ) << " - " << makeTimeString( rev_iter->second.validTill() ) << " ) "
		  << " : "
		  << " back-end="  << back_end_names[back_end]
		  << " file=" << rev_iter->second.fileName()
		  << " used=" << rev_iter->second.useCounter();
	if (rev_iter->second.onDbServer()) {
	  std::cout << " on-db";
	}
	if (rev_iter->second.isTemporaryCfg()) {
	  std::cout << " temporary";
	}
	std::cout << std::endl;
      }
    }
  }

  void PixConnObjConfigDbManager::dumpRevisions() const {
    std::cout << "INFO [PixConnObjConfigDbManager::dumpRevisionList] Dump revisions :" << std::endl;
    for (IdTagList_t::const_iterator id_tag_iter = m_revisionList.begin();
	 id_tag_iter != m_revisionList.end();
	 ++id_tag_iter) {
      std::cout << "== " << id_tag_iter->first << std::endl;
      for (CfgTagList_t::const_iterator cfg_tag_iter = id_tag_iter->second.begin();
	   cfg_tag_iter != id_tag_iter->second.end();
	   ++cfg_tag_iter) {
	std::cout << "-- " << cfg_tag_iter->first << std::endl;
	cfg_tag_iter->second.dumpRevisions();
      }
    }
  }

  void PixConnObjConfigDbManager::dumpRevisions(const std::string &name) const {
    std::cout << "INFO [PixConnObjConfigDbManager::dumpRevisionList] Dump revisions :" << std::endl;
    for (IdTagList_t::const_iterator id_tag_iter = m_revisionList.begin();
	 id_tag_iter != m_revisionList.end();
	 ++id_tag_iter) {
      std::cout << "== " << id_tag_iter->first << std::endl;
      for (CfgTagList_t::const_iterator cfg_tag_iter = id_tag_iter->second.begin();
	   cfg_tag_iter != id_tag_iter->second.end();
	   ++cfg_tag_iter) {
	std::cout << "-- " << cfg_tag_iter->first << std::endl;
	cfg_tag_iter->second.dumpRevisions(name);
      }
    }
  }

  template <class T_CompoundObj_t>
  inline ConfigHandle PixConnObjConfigDbManager::configHandle(TagPayload_t &tag_payload,
							      const std::string &name,
							      Revision_t revision)
  {
    std::shared_ptr<T_CompoundObj_t> obj( cacheObject<T_CompoundObj_t>(tag_payload,name,revision) );
    return new PixConfigWrapperTopLevel<const T_CompoundObj_t>( obj, config<T_CompoundObj_t>(obj.get()) );
  }

  // instantiation
  template std::shared_ptr<const PixLib::PixModuleGroup> PixConnObjConfigDbManager::cacheObject<PixLib::PixModuleGroup>(TagPayload_t &tag_payload,
															  const std::string &name,
															  Revision_t revision);

  template std::shared_ptr<const PixLib::PixModule>      PixConnObjConfigDbManager::cacheObject<PixLib::PixModule>(TagPayload_t &tag_payload,
														     const std::string &name,
														     Revision_t revision);

  template std::shared_ptr<const PixLib::PixDisable>     PixConnObjConfigDbManager::cacheObject<PixLib::PixDisable>(TagPayload_t &tag_payload,
														       const std::string &name,
														       Revision_t revision);

  template std::shared_ptr<const PixLib::PixTrigController> PixConnObjConfigDbManager::cacheObject<PixLib::PixTrigController>(TagPayload_t &tag_payload,
																const std::string &name,
																Revision_t revision);

}

#include "ObjectCache_t_impl.h"

namespace PixA {

  // instantiate object cache objects :
  template class ObjectCache_t<std::shared_ptr<PixLib::PixModule> >;
  template class ObjectCache_t<std::shared_ptr<PixLib::PixModuleGroup> >;
  template class ObjectCache_t<std::shared_ptr<PixLib::PixTrigController> >;
  template class ObjectCache_t<std::shared_ptr<PixLib::PixDisable> >;

}
