

#ifdef DEBUG
std::set<ObjStat_t *> ObjStat_t::s_objStatList;

void ObjStat_t::dumpAll() {
  for (std::set<ObjStat_t *>::const_iterator iter = s_objStatList.begin();
       iter != s_objStatList.end();
       iter++) {
    (*iter)->dump();
  }
}

void ObjStat_t::dump() const {
  std::cout << "INFO [ObjStat::dump] Statistics for " << m_name << " ctors = " << m_nCtor << "dtors = " << m_nDtor << "  alive = " << m_n << std::endl;
}
#endif
