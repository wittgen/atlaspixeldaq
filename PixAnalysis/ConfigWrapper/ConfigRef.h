#ifndef _PixA_ConstConfigRef_h_
#define _PixA_ConstConfigRef_h_

#include "IConfig.h"
#include "ConfGrpRef.h"

namespace PixLib {
  class Config;
}

namespace PixA {

  class PixConnectivityWrapper;

  class ConfigHandle;
  class ConstConfigHandle;
  class MutableConfigRef;

  class ConfigRef
  {
    //    friend class IConfig;
    //    friend class PixConnectivityWrapper;
    //    friend class PixConfigWrapper;
    //    friend class DbManager;
    friend class ConfigHandle;
    friend class MutableConfigRef;

  protected:
    //    ConfigRef(IConfig *config) : m_config(config) { m_config->use();}
    ConfigRef(IConfig &config) : m_config(&config) { m_config->use();}
  public:
    ConfigRef(const ConfigRef &config) : m_config(config.m_config) { m_config->use();}
    ConfigRef(const MutableConfigRef &config);

    ~ConfigRef() {
      m_config->done();
    }

    ConfigRef &operator=(ConfigRef &a) {
      m_config->done();
      m_config=a.m_config;
      m_config->use();
      return *this;
    }

    const ConfGrpRef operator[](const std::string &name) const   {
      // for the time being the IConfig does only provide non-const methods
      return ConfGrpRef((const_cast<IConfig &>( *m_config))[name]);
    }

    const ConfigRef subConfig(const std::string &name) const  {
      // for the time being the IConfig does only provide non-const methods
      return ConfigRef((const_cast<IConfig&>(*m_config)).subConfig(name));
    }


    ConfigRef createFullCopy() {
      return m_config->createAll();
    }


    /** Create the whole config and return a reference to it.
     */
    const PixLib::Config &config() const {
      return const_cast<IConfig *>(m_config)->config();
    }

  protected:
    MutableConfigRef mutableConfigRef();

    IConfig *m_config;
  };


  class MutableConfigRef : public ConfigRef
  {
    friend class MutableConfigHandle;
    friend class ConfigHandle;
    friend class ConfigRef;

  protected:
    MutableConfigRef(IConfig &config) : ConfigRef(config) { }

  public:
    MutableConfigRef(const MutableConfigRef &config) : ConfigRef(config) { }

    MutableConfigRef &operator=(MutableConfigRef &a) {
      ConfigRef::operator=(a);
      return *this;
    }

    MutableConfigRef &operator=(ConfigRef &a) {
      ConfigRef::operator=(a);
      return *this;
    }

    MutableConfigRef createFullCopy()  {
      ConfigRef full_copy( ConfigRef::createFullCopy() );
      return full_copy.mutableConfigRef();
    }

    ConfGrpRef operator[](const std::string &name)  {
      return ConfGrpRef((*m_config)[name]);
    }

    MutableConfigRef subConfig(const std::string &name)  {
      // for the time being the IConfig does only provide non-const methods
      return MutableConfigRef((const_cast<IConfig&>(*m_config)).subConfig(name));
    }

    /** Create the whole config and return a reference to it.
     */
    PixLib::Config &config() {
      return m_config->config();
    }

  private:
    //     /** Inihibit non const accessors
    //      */
    //     ConfGrpRef operator[](const std::string &name) throw (ConfigException) { assert(false); return ConfigRef::operator[](name); }

    //     /** Inihibit non const accessors
    //      */
    //     ConfigRef subConfig(const std::string &name) throw (ConfigException) { assert(false); return ConfigRef::subConfig(name);}

  };

  inline ConfigRef::ConfigRef(const MutableConfigRef &config) : m_config(config.m_config) { m_config->use();}
  inline MutableConfigRef ConfigRef::mutableConfigRef()  { return MutableConfigRef(*m_config); }

}

#endif
