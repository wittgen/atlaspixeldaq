#include "Sector.h"
#include "ModuleLabel.h"

#include <qbrush.h> 
#include <qpen.h>
#include <qpainter.h> 
#include <qwidget.h>

#include <cmath>

Sector::Sector(QWidget* parent, const char* name, int i , QString label)
  : QObject(parent, name), m_index(i), m_label(label)
{
  if(m_label=="")
    m_label = "Sector "+QString::number(i+1);
  for(int j=0;j<6;j++){
    m_modules[j] = new ModuleLabelDisk(parent, "mod"+QString::number(j+1)+"s"+QString::number(i+1), j);
    connect(this,SIGNAL(mouseClick(double,double)), m_modules[j], SLOT(extClick(double,double)));
    //connect(m_modules[j], SIGNAL(statusUpdated(QPaintEvent *)), parent,SLOT(paintEvent(QPaintEvent *)));
  }
}
void Sector::paint()
{
  double main_phi = 45*(double)(m_index-4);

  QPainter pa((QWidget*)parent());
  pa.setPen(QPen(Qt::black,1));
  pa.setBrush(QBrush(Qt::lightGray));
  pa.translate(((QWidget*)parent())->width()/2,((QWidget*)parent())->height()/2);
  pa.drawPie(-275,-275,550,550,-2-(int)(main_phi*16),-710);
  QPainter pc((QWidget*)parent());
  pc.setPen(QPen(Qt::black,2));
  pc.translate(((QWidget*)parent())->width()/2,((QWidget*)parent())->height()/2);
  pc.rotate(22.5+main_phi);
  QFont f;
  f.setBold(true);
  pc.setFont(f);
  pc.drawText(80,5,m_label);
  // draw modules
  for(int j=0;j<6;j++){
    QPainter p((QWidget*)parent());
    p.translate(((QWidget*)parent())->width()/2,((QWidget*)parent())->height()/2);
    p.rotate(main_phi);
    m_modules[j]->paint(p);
  }
}
void Sector::loadModule(QString mName, int pos)
{
  if(pos>=0 && pos<6)
    m_modules[pos]->setLabel(mName);
}
void Sector::extEvent ( QMouseEvent * me )
{
  // turn into normal variables: (0,0) is at centree of panel, y is pointing up
  double myX = (double)me->x()-((QWidget*)parent())->width()/2;
  double myY = ((QWidget*)parent())->height()/2-(double)me->y();
  double phi = 0, rad = sqrt(myX*myX+myY*myY);
  const double pi=3.141592654;
  if(myX!=0) phi = atan(myY/myX);
  else phi = pi/2;
  if(phi<0) phi = pi+phi;
  if(myY<0) phi += pi;
  double phi_max = 2.5*pi-45./180.*pi*(double)(m_index-2);
  double phi_min = 2.5*pi-45./180.*pi*(double)(m_index-1);
  if(phi_max>2*pi) {
    phi_max = phi_max-2*pi;
    phi_min = phi_min-2*pi;
  }
  if(phi>phi_min && phi<phi_max && rad>150 && rad<260){
    emit mouseClick(pi/4-phi+phi_min, rad);
  }

  return;
}
ModuleLabel* Sector::getModule(int i)
{
  if(i>=0 && i<6) return m_modules[i];
  else            return 0;
}
