#ifndef MODULEMAP_H
#define MODULEMAP_H

#include <qpushbutton.h>
#include <qwidget.h>
#include <vector>
#include <map>
#include <string>

class QApplication;
class ModuleLabel;
class QLabel;
class QString;

class Pp0Button : public QPushButton {
 Q_OBJECT

 public:
  Pp0Button( const QString & text, QWidget * parent, const char * name = 0 );
  ~Pp0Button(){};

  void addLabel(ModuleLabel *label){m_labels.push_back(label);};
  void mouseReleaseEvent ( QMouseEvent *);

 signals:
  void clickedPp0(std::vector<ModuleLabel*>, bool);

  std::vector<ModuleLabel*> m_labels;
};

class ModuleMap : public QWidget {

 Q_OBJECT

 public:

   enum assyType{stave, sector, disk, layer0, layer1, halfshell2, userdef};

  ModuleMap(assyType in_assyType, const char *in_label, QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = Qt::WType_Dialog, bool withButtons=false);
  ModuleMap(int items, int modules, const char *in_label, QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = Qt::WType_Dialog, bool withButtons=false);
  ~ModuleMap(){};

  const char* getLabel();
  ModuleLabel* loadModule(void *module, const char *name);
  ModuleLabel* loadModule(const char *name, int staveID, int modID, const char *staveName=0);
  void setup(QString, bool);

 public slots:
  void updateStatus();
  void reset(bool res_all=true);
  void updateStatus(std::map<std::string,std::string>);
  void repaint(){paintEvent(0);};
  void exec();

 protected slots:
  void paintEvent ( QPaintEvent * );
  void mouseDoubleClickEvent ( QMouseEvent * e );

 private:
  bool m_modal;
  int m_nItems, m_nMods;
  QLabel *m_topLabel;
  std::map<std::string,ModuleLabel*> m_modLabels;
 protected:
  assyType m_assyType;
 signals:
  void clickedPp0(std::vector<ModuleLabel*>, bool);
};

#endif // MODULEMAP_H
