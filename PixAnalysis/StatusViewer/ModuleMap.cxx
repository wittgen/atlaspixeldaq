#include "ModuleMap.h"
#include "ModuleLabel.h"
#include "Sector.h"

#include <qpixmap.h>
#include <qimage.h>
#include <QGridLayout>
#include <qvariant.h>
#include <qlayout.h>
#include <qstring.h>
#include <qapplication.h>
#include <qbrush.h> 
#include <qpen.h>
#include <qpainter.h> 

Pp0Button::Pp0Button( const QString & text, QWidget * parent, const char * name ) : QPushButton(text, parent, name)
{
}
void Pp0Button::mouseReleaseEvent ( QMouseEvent *e)
{
  QPushButton::mouseReleaseEvent(e);
  for(std::vector<ModuleLabel*>::iterator it = m_labels.begin(); it!=m_labels.end(); it++){
    (*it)->sendClickSig(isOn());
  }
}

ModuleMap::ModuleMap(int items, int modules, const char *in_label, QWidget* parent, const char* name, bool modal, WFlags fl, bool withButtons)
  : QWidget(parent, name, fl), m_modal(modal), m_nItems(items), m_nMods(modules), m_assyType(userdef)
{
  if ( !name )
    setName( "ModuleMapBase" );
  setCaption("Module map");
  setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)3, (QSizePolicy::SizeType)3, 0, 0, sizePolicy().hasHeightForWidth() ) );
  resize( QSize(465, 77).expandedTo(minimumSizeHint()) );
  
  m_topLabel = new QLabel(in_label, this, "topLabel");
  
  QString iLabel = "Item ";
  setup(iLabel, withButtons);
}
ModuleMap::ModuleMap(assyType in_assyType, const char *in_label, QWidget* parent, const char* name, bool modal, WFlags fl, bool withButtons)
  : QWidget(parent, name, fl), m_modal(modal), m_assyType(in_assyType)
{
  if ( !name )
    setName( "ModuleMapBase" );
  setCaption("Module map");
  setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)3, (QSizePolicy::SizeType)3, 0, 0, sizePolicy().hasHeightForWidth() ) );
  resize( QSize(465, 77).expandedTo(minimumSizeHint()) );
  
  m_topLabel = new QLabel(in_label, this, "topLabel");
  
  //  QString assyType = in_assyType;
  QString iLabel = "Stave ";
  if(in_assyType==disk){
    m_nMods=6;
    m_nItems=8;
    iLabel = "Sector ";
  } else if(in_assyType==sector){
    m_nMods=6;
    m_nItems=1;
    iLabel = "Sector ";
  } else if(in_assyType==stave){
    m_nMods=13;
    m_nItems=1;
  } else{
    m_nMods=13;
    if(in_assyType==layer0)
      m_nItems=22;
    else if(in_assyType==layer1)
      m_nItems=38;
    else if(in_assyType==halfshell2)
      m_nItems=26;
  }

  if(in_assyType==disk){
    Pp0Button *bt[8];
    if(withButtons){
      QVBoxLayout*Form1Layout = new QVBoxLayout( this, 11, 6, "Form1Layout"); 
      
      QVBoxLayout*layout10 = new QVBoxLayout( 0, 0, 6, "layout10"); 
      QSpacerItem*spacer9 = new QSpacerItem( 21, 41, QSizePolicy::Minimum, QSizePolicy::Fixed );
      layout10->addItem( spacer9 );
      
      QHBoxLayout*layout1 = new QHBoxLayout( 0, 0, 6, "layout1"); 
      QSpacerItem*spacer2 = new QSpacerItem( 101, 31, QSizePolicy::Expanding, QSizePolicy::Minimum );
      layout1->addItem( spacer2 );
      
      bt[1] = new Pp0Button( "ena",  this, "pushButton1" );
      layout1->addWidget( bt[1] );
      QSpacerItem*spacer1 = new QSpacerItem( 150, 41, QSizePolicy::Maximum, QSizePolicy::Minimum );
      layout1->addItem( spacer1 );
      
      bt[2] = new Pp0Button( "ena",  this, "pushButton2" );
      layout1->addWidget( bt[2] );
      QSpacerItem*spacer3 = new QSpacerItem( 101, 41, QSizePolicy::Expanding, QSizePolicy::Minimum );
      layout1->addItem( spacer3 );
      layout10->addLayout( layout1 );
      QSpacerItem*spacer7 = new QSpacerItem( 21, 100, QSizePolicy::Minimum, QSizePolicy::Expanding );
      layout10->addItem( spacer7 );
      
      QHBoxLayout*layout2 = new QHBoxLayout( 0, 0, 6, "layout2"); 
      QSpacerItem*spacer2_2 = new QSpacerItem( 150, 31, QSizePolicy::Maximum, QSizePolicy::Minimum );
      layout2->addItem( spacer2_2 );
      
      bt[0] = new Pp0Button( "ena",  this, "pushButton1_2" );
      layout2->addWidget( bt[0] );
      QSpacerItem*spacer1_2 = new QSpacerItem( 250, 41, QSizePolicy::MinimumExpanding, QSizePolicy::Minimum );
      layout2->addItem( spacer1_2 );
      
      bt[3] = new Pp0Button( "ena",  this, "pushButton2_2" );
      layout2->addWidget( bt[3] );
      QSpacerItem*spacer3_2 = new QSpacerItem( 150, 41, QSizePolicy::Maximum, QSizePolicy::Minimum );
      layout2->addItem( spacer3_2 );
      layout10->addLayout( layout2 );
      QSpacerItem*spacer8 = new QSpacerItem( 31, 90, QSizePolicy::Minimum, QSizePolicy::Expanding );
      layout10->addItem( spacer8 );
      
      QHBoxLayout*layout2_2 = new QHBoxLayout( 0, 0, 6, "layout2_2"); 
      QSpacerItem*spacer2_2_2 = new QSpacerItem( 150, 31, QSizePolicy::Maximum, QSizePolicy::Minimum );
      layout2_2->addItem( spacer2_2_2 );
      
      bt[7] = new Pp0Button( "ena",  this, "pushButton1_2_2" );
      layout2_2->addWidget( bt[7] );
      QSpacerItem*spacer1_2_2 = new QSpacerItem( 250, 41, QSizePolicy::MinimumExpanding, QSizePolicy::Minimum );
      layout2_2->addItem( spacer1_2_2 );
      
      bt[4] = new Pp0Button( "ena",  this, "pushButton2_2_2" );
      layout2_2->addWidget( bt[4] );
      QSpacerItem*spacer3_2_2 = new QSpacerItem( 150, 41, QSizePolicy::Maximum, QSizePolicy::Minimum );
      layout2_2->addItem( spacer3_2_2 );
      layout10->addLayout( layout2_2 );
      QSpacerItem*spacer8_2 = new QSpacerItem( 31, 89, QSizePolicy::Minimum, QSizePolicy::Expanding );
      layout10->addItem( spacer8_2 );
      
      QHBoxLayout*layout1_2 = new QHBoxLayout( 0, 0, 6, "layout1_2"); 
      QSpacerItem*spacer2_3 = new QSpacerItem( 101, 31, QSizePolicy::Expanding, QSizePolicy::Minimum );
      layout1_2->addItem( spacer2_3 );
      
      bt[6] = new Pp0Button( "ena",  this, "pushButton1_3" );
      layout1_2->addWidget( bt[6] );
      QSpacerItem*spacer1_3 = new QSpacerItem( 150, 41, QSizePolicy::Maximum, QSizePolicy::Minimum );
      layout1_2->addItem( spacer1_3 );
      
      bt[5] = new Pp0Button( "ena",  this, "pushButton2_3" );
      layout1_2->addWidget( bt[5] );
      QSpacerItem*spacer3_3 = new QSpacerItem( 101, 41, QSizePolicy::Expanding, QSizePolicy::Minimum );
      layout1_2->addItem( spacer3_3 );
      layout10->addLayout( layout1_2 );
      QSpacerItem*spacer9_2 = new QSpacerItem( 21, 41, QSizePolicy::Minimum, QSizePolicy::Fixed );
      layout10->addItem( spacer9_2 );
      Form1Layout->addLayout( layout10 );
      for(int k=0;k<8;k++){
	bt[k]->setMaximumSize( QSize( 40, 32767 ) );
	bt[k]->setToggleButton( true );
	bt[k]->setOn( true );
	bt[k]->setText( tr( "ena" ) );
      }
    }
    setMinimumSize(550,550);
    resize(600,600);
    for(int i=0;i<8;i++){
      Sector *sec = new Sector(this,"stv"+QString::number(i+1), i);
      for(int j=0;j<6&&withButtons;j++)
	bt[i]->addLabel(sec->getModule(j));
    }
  } else
    setup(iLabel, withButtons);
}
void ModuleMap::setup(QString iLabel, bool withButtons)
{
  QVBoxLayout *ModuleMapBaseLayout = new QVBoxLayout( this, 11, 6, "ModuleMapBaseLayout"); 

  QVBoxLayout *ModuleMapSubLayout = new QVBoxLayout( 0,0, 6, "ModuleMapSubLayout"); 
  ModuleMapSubLayout->addWidget( m_topLabel);
  
  QGrid *moduleMap = new QGrid( m_nItems+1, Qt::Vertical, this, "moduleMap" );
  moduleMap->setSpacing(3);
  moduleMap->setMargin(2);
  
  // buttons left
  Pp0Button *bt[m_nItems];
  if(withButtons){
    new QLabel("", moduleMap, "dummyl");
    for(int j=1;j<=m_nItems;j++){
      QString name = "btl"+QString::number(j);
      bt[j] = new Pp0Button("ena",  moduleMap, name);
      bt[j]->setToggleButton(true);
      bt[j]->setOn( true );
      bt[j]->setMaximumHeight(15);
      bt[j]->setMaximumWidth(40);
    }
  }

  QLabel *labels;
  QString stvLabels[13]={"M6C", "M5C", "M4C", "M3C", "M2C", "M1C" , "M0", 
    "M1A", "M2A", "M3A", "M4A", "M5A", "M6A"};
  std::vector< std::vector<ModuleLabel*> > rbmls;
  for(int j=0;j<=m_nItems;j++){
    std::vector<ModuleLabel*> tmpVec;
    rbmls.push_back(tmpVec);
  }
  for(int i=0;i<=m_nMods;i++){
    for(int j=0;j<=m_nItems;j++){
      if(j==0){
	if(i==0){
	  labels = new QLabel("", moduleMap, "dummy");
	} else{
	  QString ltxt = "Position "+QString::number(i);
	  if(m_nMods==13){ // use stave labels
	    ltxt = stvLabels[i-1];
	  }
	  QString name = "head"+QString::number(i);
	  labels = new QLabel(ltxt, moduleMap, name);
	  labels->setBackgroundColor(Qt::white);
	}
      } else{
	if(i==0){
	  QString ltxt = iLabel+QString::number(j);
	  QString name = "stv"+QString::number(j);
	  labels = new QLabel(ltxt, moduleMap, name);
	  labels->setBackgroundColor(Qt::white);
	} else{
	  QString ltxt = "Module "+QString::number(i);
	  QString name = "mod"+QString::number(i)+"s"+QString::number(j);
	  labels = new ModuleLabelBarrel(ltxt, moduleMap, name);
	  if(withButtons && j>0){
	    if((i==7 && ((j+1)/2)*2==(j+1)) || i<7)
	      bt[j]->addLabel((ModuleLabel*)labels);
	    else
	      rbmls[j].push_back((ModuleLabel*)labels);
	  }
	}
      }
      labels->setMaximumHeight(17);
    }
  }
  
  // buttons right
  if(withButtons){
    new QLabel("", moduleMap, "dummyr");
    for(int j=1;j<=m_nItems;j++){
      QString name = "btr"+QString::number(j);
      Pp0Button *bt = new Pp0Button("ena",  moduleMap, name);
      bt->setToggleButton(true);
      bt->setMaximumHeight(15);
      bt->setMaximumWidth(40);
      bt->setOn( true );
      for(unsigned int k=0;k<rbmls[j].size(); k++)
	bt->addLabel(rbmls[j][k]);
    }
  }

  ModuleMapSubLayout->addWidget( moduleMap );

  ModuleMapBaseLayout->addLayout(ModuleMapSubLayout);
}
ModuleLabel* ModuleMap::loadModule(void* mod, const char *name)
{
  if(m_modLabels[name]==0) return 0;
  m_modLabels[name]->loadModule(mod);
  m_modLabels[name]->updateStatus("unknown");
  //  if(m_assyType==disk)
  //    paintEvent(0);
  return m_modLabels[name];
}
ModuleLabel* ModuleMap::loadModule(const char *name, int staveID, int modID, const char *staveName)
{
  if(staveID>=m_nItems || modID>=m_nMods) return 0;
  QString cname = "mod"+QString::number(modID+1)+"s"+QString::number(staveID+1);
  ModuleLabel *ml = dynamic_cast<ModuleLabel*>(child(cname));
  if(ml==0) return 0;
  ml->setText(name);
  ml->updateStatus("unknown");
  m_modLabels.insert(std::make_pair(name,ml));
  // also label stave/sector correctly if requested
  if(staveName!=0){
    if(m_assyType==disk){
      cname = "stv"+QString::number(staveID+1);
      Sector *l =  dynamic_cast<Sector*>(child(cname));
      if(l!=0)
	l->setLabel(QString(staveName));
    } else{
      cname = "stv"+QString::number(staveID+1);
      QLabel *l =  dynamic_cast<QLabel*>(child(cname));
      if(l!=0)
	l->setText(staveName);
    }
  }
  //  if(m_assyType==disk)
  //    paintEvent(0);
  return ml;
}
void ModuleMap::updateStatus()
{
  for(std::map<std::string,ModuleLabel*>::iterator it = m_modLabels.begin(); it!= m_modLabels.end(); it++)
    it->second->updateStatus();
  if(m_assyType==disk && isVisible())
    paintEvent(0);
}
void ModuleMap::updateStatus(std::map<std::string,std::string> modList)
{
  for(std::map<std::string,std::string>::iterator it = modList.begin(); it!= modList.end(); it++)
    if(m_modLabels[it->first]!=0)
      m_modLabels[it->first]->updateStatus(it->second.c_str());
  if(m_assyType==disk && isVisible())
    paintEvent(0);
}
const char* ModuleMap::getLabel()
{
  return m_topLabel->text().latin1();
}
void ModuleMap::reset(bool res_all)
{
  m_modLabels.clear();
  QString iLabel = "Stave ";
  if(m_assyType==sector || m_assyType==disk)
    iLabel = "Sector ";
  else if(m_assyType==userdef)
     iLabel = "Item ";
  for(int i=0;i<=m_nMods;i++){
    for(int j=1;j<=m_nItems;j++){
      if(i==0){
	QString ltxt = iLabel+QString::number(j);
	QString name = "stv"+QString::number(j);
	if(res_all){
	  if(m_assyType==disk){
	    Sector *sec = dynamic_cast<Sector*>(child(name));
	    if(sec!=0)
	      sec->setLabel(ltxt);
	  } else{
	    QLabel *label = dynamic_cast<ModuleLabel*>(child(name));
	    if(label!=0) label->setText(ltxt);
	  }
	}
      } else{
	QString ltxt = "Module "+QString::number(i);
	QString name = "mod"+QString::number(i)+"s"+QString::number(j);
	ModuleLabel *label = dynamic_cast<ModuleLabel*>(child(name));
	if(label!=0){
	  label->loadModule(0);
	  label->setText(ltxt);
	  label->setBackgroundColor(Qt::darkGray);
	}
      }    
    }
  }
  if(m_assyType==disk && isVisible())
    paintEvent(0);
}
void ModuleMap::paintEvent ( QPaintEvent * )
{
  if(m_assyType==disk){
    for(int i=0;i<8;i++){
      Sector *sec = dynamic_cast<Sector*>(child("stv"+QString::number(i+1)));
      if(sec!=0)
	sec->paint();
    }
    QPainter pb(this);
    pb.setPen(QPen(Qt::black,1));
    pb.setBrush(QBrush(Qt::lightGray));
    pb.translate(width()/2,height()/2);
    pb.drawEllipse(-70,-70,140,140);
  }
}
void ModuleMap::mouseDoubleClickEvent ( QMouseEvent * e )
{
  if(m_assyType==disk){
    for(int i=0;i<8;i++)
      ((Sector*)child("stv"+QString::number(i+1)))->extEvent(e);
  }
}
void ModuleMap::exec()
{
  if(m_modal) ;
  show();
}
