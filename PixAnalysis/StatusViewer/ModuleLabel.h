#ifndef MODULELABEL_H
#define MODULELABEL_H

#include <qlabel.h>

class ModuleLabel : public QLabel {

    Q_OBJECT

 public:
  ModuleLabel(const QString & text, QWidget* parent = 0, const char* name = 0);
  virtual ~ModuleLabel(){};

  virtual void loadModule(void*){};
  virtual void updateStatus(){};
  virtual void updateStatus(const char *){};

  void sendClickSig(bool act);

 signals:
  void moduleDoubleClick(void*);
  void moduleAction(void*, bool);

 protected:
  void *m_module;
};

class ModuleLabelDisk : public ModuleLabel{

  Q_OBJECT

 public:
  ModuleLabelDisk(QWidget* parent = 0, const char* name = 0, int i=0);
  ~ModuleLabelDisk(){};
  void setLabel(QString name);
  void loadModule(void* mod){m_module = mod;updateStatus();};
  void updateStatus();
  void updateStatus(const char *pass);

 public slots:
  void paint(QPainter &p);
  void extClick(double,double);

 signals:
  void statusUpdated(QPaintEvent *);

 private:
  int m_index;

};

class ModuleLabelBarrel : public ModuleLabel {

    Q_OBJECT

 public:
  ModuleLabelBarrel(const QString & text, QWidget * parent, const char * name = 0);
  ~ModuleLabelBarrel(){};
  void mouseDoubleClickEvent( QMouseEvent *e );

  void loadModule(void* mod){m_module = mod;updateStatus();};
  void updateStatus();
  void updateStatus(const char *pass);

};

#endif // MODULELABEL_H
