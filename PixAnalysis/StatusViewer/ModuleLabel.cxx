#include "ModuleLabel.h"

#include <qcolor.h>
#include <qevent.h> 
#include <qmessagebox.h>
#include <qbrush.h> 
#include <qpen.h>
#include <qpainter.h> 

ModuleLabel::ModuleLabel(const QString & text, QWidget * parent, const char * name)
  : QLabel(text, parent), m_module(0)
{
}
void ModuleLabel::sendClickSig(bool act)
{
  emit moduleAction((void*)m_module, act);
}

ModuleLabelDisk::ModuleLabelDisk(QWidget* parent, const char* name, int i)
  : ModuleLabel("Module "+QString::number(i+1),parent, name), m_index(i)
{
  this->hide();
  setAutoFillBackground(true);
  //setBackgroundColor(Qt::darkGray);
  QPalette pal = palette();
  pal.setColor(QPalette::Window, QColor(Qt::darkGrey));
  setPalette(pal);
}
void ModuleLabelDisk::paint(QPainter &p)
{
  int xoff = 150;
  double phi = 6.+13.*(double)m_index;
  if(m_index>2){
    xoff=160;
    phi = 12.5+13.*(double)(5-m_index);
  }
  p.rotate(phi);
  p.fillRect(xoff,10,100,-20,QBrush(backgroundColor()));
  p.drawRect(xoff,10,100,-20);
  QFont f;
  f.setBold(true);
  p.setFont(f);
  p.drawText(xoff+10,5,text());
}
void ModuleLabelDisk::extClick(double phi, double rad)
{
  const double pi=3.141592654;
  double xoff = 150.;
  double phi_min = 3./180.*pi+13./180*pi*(double)m_index;
  double phi_max = 9./180.*pi+13./180*pi*(double)m_index;
  if(m_index>2){
    xoff=160;
    phi_min = 9.5/180.*pi+13./180*pi*(double)(5-m_index);
    phi_max = 15.5/180.*pi+13./180*pi*(double)(5-m_index);
  }
  if(phi>phi_min && phi<phi_max && rad>xoff && rad<(xoff+100))
    emit moduleDoubleClick((void*)m_module);
}
void ModuleLabelDisk::setLabel(QString name)
{
  setText(name);
  setBackgroundColor(Qt::cyan);
}
void ModuleLabelDisk::updateStatus()
{
  if(m_module==0) return;
  setBackgroundColor(Qt::cyan);
  //QPaintEvent *e=0;
  //emit statusUpdated(e);
}
void ModuleLabelDisk::updateStatus(const char *in_pass)
{
  setBackgroundColor(Qt::cyan);
  QString pass = in_pass;
  if(pass=="passed")
    setBackgroundColor(Qt::green);
  else if(pass=="failed")
    setBackgroundColor(Qt::red);
  else if(pass=="attention")
    setBackgroundColor(Qt::yellow);
  //QPaintEvent *e=0;
  //emit statusUpdated(e);
}

ModuleLabelBarrel::ModuleLabelBarrel(const QString & text, QWidget * parent, const char * name)
  : ModuleLabel(text, parent, name)
{
  setBackgroundColor(Qt::darkGray);
}
void ModuleLabelBarrel::mouseDoubleClickEvent( QMouseEvent * )
{
  emit moduleDoubleClick((void*)m_module);
}
void ModuleLabelBarrel::updateStatus()
{
  if(m_module==0) return;

  setBackgroundColor(Qt::cyan);

//   QString pass = m_module->GetPassed();
//   if(pass=="passed")
//     setBackgroundColor(Qt::green);
//   else if(pass=="failed")
//     setBackgroundColor(Qt::red);
}
void ModuleLabelBarrel::updateStatus(const char *in_pass)
{
  setBackgroundColor(Qt::cyan);

  QString pass = in_pass;
  if(pass=="passed")
    setBackgroundColor(Qt::green);
  else if(pass=="failed")
    setBackgroundColor(Qt::red);
  else if(pass=="attention")
    setBackgroundColor(Qt::yellow);
}
