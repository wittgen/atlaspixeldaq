#ifndef SECTOR_H
#define SECTOR_H

#include <qobject.h>
#include <QMouseEvent>

class ModuleLabelDisk;
class ModuleLabel;

class Sector : public QObject{

  Q_OBJECT

 public:
  Sector(QWidget* parent = 0, const char* name = 0, int i=0, QString label="");
  ~Sector(){};
  void setLabel(QString sName){m_label=sName;};
  void loadModule(QString mName, int pos);
  ModuleLabel* getModule(int);

 public slots:
  void paint();
  void extEvent ( QMouseEvent * e );

 signals:
  void mouseClick(double, double);

 private:
  int m_index;
  QString m_label;
  ModuleLabelDisk *m_modules[6];
};

#endif // SECTOR_H
