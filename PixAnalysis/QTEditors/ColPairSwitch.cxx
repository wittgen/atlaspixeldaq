#include <QPushButton>
#include <QApplication>

#include "ColPairSwitch.h"

using namespace std;

ColourButton::ColourButton(const QString &text, QWidget *parent) : QPushButton(text, parent){
  setMaximumSize(QSize(22, 22));
  setAutoFillBackground(false);
  setStyleSheet("background-color: rgb(255, 0, 0);");
  setCheckable(true);
  setAutoRepeat(false);
  setAutoExclusive(false);
  setDefault(false);
  setFlat(false);
  QObject::connect(this, SIGNAL(clicked()),  this, SLOT(setState()));
}
void ColourButton::setState(){
  if(isChecked())
    setStyleSheet("background-color: rgb(0, 255, 0);");
  else
    setStyleSheet("background-color: rgb(255, 0, 0);");
}

ColPairSwitch::ColPairSwitch(QWidget* parent, int nButtons)
  : QDialog(parent), m_nButtons(nButtons){

  setupUi();
  QObject::connect(allOnButton, SIGNAL(clicked()),  this, SLOT(allOn()));
  QObject::connect(allOffButton, SIGNAL(clicked()), this, SLOT(allOff()));

  for(int i=0;i<m_nButtons;i++){
    m_buttons.push_back(new ColourButton(QString::number(i+1), this));
    m_buttons[i]->setObjectName("PushButton_"+QString::number(i+1));
    hboxLayout->addWidget(m_buttons[i]);
  }
}

void ColPairSwitch::setupUi(){
  if (objectName().isEmpty())
    setObjectName(QString::fromUtf8("ColPairSwitch"));
  resize(272, 143);
  vboxLayout = new QVBoxLayout(this);
  vboxLayout->setSpacing(6);
  vboxLayout->setContentsMargins(11, 11, 11, 11);
  vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
  vboxLayout1 = new QVBoxLayout();
  vboxLayout1->setSpacing(6);
  vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
  hboxLayout = new QHBoxLayout();
  hboxLayout->setSpacing(6);
  hboxLayout->setContentsMargins(0, 0, 0, 0);
  hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
  
  vboxLayout1->addLayout(hboxLayout);
  
  hboxLayout1 = new QHBoxLayout();
  hboxLayout1->setSpacing(6);
  hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
  allOnButton = new QPushButton(this);
  allOnButton->setObjectName(QString::fromUtf8("allOnButton"));
  
  hboxLayout1->addWidget(allOnButton);
  
  Spacer1_2 = new QSpacerItem(45, 16, QSizePolicy::Expanding, QSizePolicy::Minimum);
  
  hboxLayout1->addItem(Spacer1_2);
  
  allOffButton = new QPushButton(this);
  allOffButton->setObjectName(QString::fromUtf8("allOffButton"));
  
  hboxLayout1->addWidget(allOffButton);
  
  
  vboxLayout1->addLayout(hboxLayout1);
  
  hboxLayout2 = new QHBoxLayout();
  hboxLayout2->setSpacing(6);
  hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
  PushButton4 = new QPushButton(this);
  PushButton4->setObjectName(QString::fromUtf8("PushButton4"));
  
  hboxLayout2->addWidget(PushButton4);
  
  Spacer1 = new QSpacerItem(45, 16, QSizePolicy::Expanding, QSizePolicy::Minimum);
  
  hboxLayout2->addItem(Spacer1);
  
  PushButton5 = new QPushButton(this);
  PushButton5->setObjectName(QString::fromUtf8("PushButton5"));
  
  hboxLayout2->addWidget(PushButton5);
  
  
  vboxLayout1->addLayout(hboxLayout2);
  
  
  vboxLayout->addLayout(vboxLayout1);
  
  
  retranslateUi();
  QObject::connect(PushButton5, SIGNAL(clicked()), this, SLOT(reject()));
  QObject::connect(PushButton4, SIGNAL(clicked()), this, SLOT(accept()));
  
  QMetaObject::connectSlotsByName(this);
}
void ColPairSwitch::retranslateUi(){
  setWindowTitle(QApplication::translate("ColPairSwitch", "Column Pair Switch", 0));
  allOnButton->setText(QApplication::translate("ColPairSwitch", "all ON", 0));
  allOffButton->setText(QApplication::translate("ColPairSwitch", "all OFF", 0));
  PushButton4->setText(QApplication::translate("ColPairSwitch", "OK", 0));
  PushButton5->setText(QApplication::translate("ColPairSwitch", "Cancel", 0));
}

void ColPairSwitch::allOff(){
  for(int i=0;i<m_nButtons;i++){
    m_buttons[i]->setChecked(false);
    m_buttons[i]->setState();
  }
}
void ColPairSwitch::allOn(){
  for(int i=0;i<m_nButtons;i++){
    m_buttons[i]->setChecked(true);
    m_buttons[i]->setState();
  }
}
