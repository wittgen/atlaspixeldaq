#include "SetPanel.h"

#include <QApplication>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QSpinBox>
#include <QVBoxLayout>

SetPanel::SetPanel(QWidget* parent, PanelType type, QString labelTxt) : QDialog(parent) {
  setupUi(type, labelTxt, "", 0);
}
SetPanel::SetPanel(QWidget* parent, QString labelTxt, QString editTxt) : QDialog(parent) {
  setupUi(SetPanel::txtval, labelTxt, editTxt, 0);
}
SetPanel::SetPanel(QWidget* parent, QString labelTxt, int editVal) : QDialog(parent) {
  setupUi(SetPanel::intval, labelTxt, "", editVal);
}

void SetPanel::setupUi(PanelType type, QString labelTxt, QString valTxt, int valInt)  {
  

  if (objectName().isEmpty())
    setObjectName(QString::fromUtf8("SetPanel"));

  QHBoxLayout *mainLayout = new QHBoxLayout(this);

  TextLabel = new QLabel(this);
  TextLabel->setObjectName(QString::fromUtf8("TextLabel"));
  TextLabel->setGeometry(QRect(10, 10, 92, 26));
  TextLabel->setWordWrap(false);
  mainLayout->addWidget(TextLabel);
  if(type==intval){
    SpinBox = new QSpinBox(this);
    SpinBox->setObjectName(QString::fromUtf8("SpinBox"));
    SpinBox->setGeometry(QRect(120, 10, 70, 21));
    SpinBox->setMaximum(99999999);
    SpinBox->setMinimum(-99999999);
    SpinBox->setValue(valInt);
    mainLayout->addWidget(SpinBox);
  } else
    SpinBox=0;
  if(type==txtval){
    LineEdit = new QLineEdit(this);
    LineEdit->setObjectName(QString::fromUtf8("LineEdit"));
    LineEdit->setGeometry(QRect(120, 10, 70, 22));
    LineEdit->setText(valTxt);
    mainLayout->addWidget(LineEdit);
  } else
    LineEdit=0;
  
  OKButton = new QPushButton(this);
  OKButton->setObjectName(QString::fromUtf8("OKButton"));
  OKButton->setGeometry(QRect(200, 10, 80, 26));
  mainLayout->addWidget(OKButton);
  CancelButton = new QPushButton(this);
  CancelButton->setObjectName(QString::fromUtf8("CancelButton"));
  CancelButton->setGeometry(QRect(290, 10, 80, 26));
  mainLayout->addWidget(CancelButton);

  resize(384, 49);

  retranslateUi(labelTxt);
  QObject::connect(OKButton, SIGNAL(clicked()), this, SLOT(accept()));
  QObject::connect(CancelButton, SIGNAL(clicked()), this, SLOT(reject()));
  
  QMetaObject::connectSlotsByName(this);
}

void SetPanel::retranslateUi(QString labelTxt)  {
  this->setWindowTitle(QApplication::translate("SetPanel", "Set To ...", 0));
  OKButton->setText(QApplication::translate("SetPanel", "OK", 0));
  CancelButton->setText(QApplication::translate("SetPanel", "Cancel", 0));
  TextLabel->setText(QApplication::translate("SetPanel",labelTxt.toLatin1().data() , 0));
}

QString SetPanel::getText(){
  static QString notxt="ERROR";
  if(LineEdit!=0) return LineEdit->text();
  else            return notxt;
}
int SetPanel::getInt(){
  if(SpinBox!=0) return SpinBox->value();
  else           return -999;
}
void SetPanel::setIntLimits(int min, int max){
  if(SpinBox!=0){
    SpinBox->setMinimum(min);
    SpinBox->setMaximum(max);
  }
}
