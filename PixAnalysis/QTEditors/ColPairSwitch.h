#if !defined(COLPAIRSWITCH_H)
#define COLPAIRSWITCH_H

#include <QDialog>
#include <QHBoxLayout>
#include <QPushButton>
#include <QSpacerItem>
#include <QVBoxLayout>
#include <QPushButton>

#include <vector>

class ColourButton : public QPushButton {
 Q_OBJECT

 public:

  ColourButton(const QString &text, QWidget *parent=0);
  ~ColourButton(){};

 public slots:
  void setState();

};

class ColPairSwitch : public QDialog {

 Q_OBJECT

 public:
  ColPairSwitch(QWidget* parent = 0, int nButtons=9);
  ~ColPairSwitch(){};

 public slots:
  void allOff();
  void allOn();

 public:
  std::vector<ColourButton*>& getButtons(){return m_buttons;}

 private:
  int m_nButtons;
  std::vector<ColourButton*> m_buttons;
  QVBoxLayout *vboxLayout;
  QVBoxLayout *vboxLayout1;
  QHBoxLayout *hboxLayout;
  QHBoxLayout *hboxLayout1;
  QPushButton *allOnButton;
  QSpacerItem *Spacer1_2;
  QPushButton *allOffButton;
  QHBoxLayout *hboxLayout2;
  QPushButton *PushButton4;
  QSpacerItem *Spacer1;
  QPushButton *PushButton5;

  void setupUi();
  void retranslateUi();

};

#endif
