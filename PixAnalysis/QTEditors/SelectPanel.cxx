#include "SelectPanel.h"

#include <QPushButton>
#include <QComboBox>
#include <QLabel>
#include <QHBoxLayout>

SelectPanel::SelectPanel(QWidget *parent, std::vector<std::string> labels, std::vector< std::vector<std::string> > boxItems) : QDialog(parent) {
  setupUi(labels);
  for(unsigned int i = 0; i<boxItems.size(); i++){
    if(i<boxes.size()){
      for(unsigned int j = 0; j<boxItems.at(i).size(); j++)
	boxes.at(i)->addItem(boxItems.at(i).at(j).c_str());
    }
  }
}
SelectPanel::SelectPanel(QWidget *parent, std::vector<std::string> labels, std::vector<std::string> firstBoxItems,
			 std::map<std::string, std::vector< std::vector<std::string> > > otherBoxItems)
  : QDialog(parent), m_otherBoxItems(otherBoxItems) {
  setupUi(labels);
  if(boxes.size()>0){
    for(unsigned int j = 0; j<firstBoxItems.size(); j++)
      boxes.at(0)->addItem(firstBoxItems.at(j).c_str());
  }
  connect(boxes.at(0), SIGNAL(currentIndexChanged(int)), this, SLOT(setOtherBoxes(int)));
  setOtherBoxes(0);
}
void SelectPanel::setupUi(std::vector<std::string> labels){
  QVBoxLayout *mainLayout=new QVBoxLayout(this);
  unsigned int nitems = labels.size();
  // add label - combo box line for each vector entry
  for(unsigned int i=0; i<nitems; i++){
    QHBoxLayout *boxLayout=new QHBoxLayout();
    QLabel *blab = new QLabel(labels.at(i).c_str(), this);
    boxLayout->addWidget(blab);
    QSpacerItem *boxsp = new QSpacerItem(5,5);
    boxLayout->addItem(boxsp);
    QComboBox *box = new QComboBox(this);
    boxes.push_back(box);
    boxLayout->addWidget(box);
    mainLayout->addLayout(boxLayout);
  }

  // add OK / Cancel buttons
  QHBoxLayout *butLayout=new QHBoxLayout();
  QPushButton *okbut = new QPushButton("OK", this);
  butLayout->addWidget(okbut);
  QSpacerItem *butsp = new QSpacerItem(5,5);
  butLayout->addItem(butsp);
  QPushButton *cabut = new QPushButton("Cancel", this);
  butLayout->addWidget(cabut);
  mainLayout->addLayout(butLayout);
  connect(okbut, SIGNAL(pressed()), this, SLOT(accept()));
  connect(cabut, SIGNAL(pressed()), this, SLOT(reject()));

  resize(400,80+nitems*60);

}
void SelectPanel::getSelection(std::vector<std::string> &selectedList){
  selectedList.clear();
  for (std::vector<QComboBox*>::iterator it=boxes.begin(); it!=boxes.end();it++){
    selectedList.push_back((*it)->currentText().toLatin1().data());
  }
  return;
}
void SelectPanel::setOtherBoxes(int index){
  if(boxes.size()>0){
    std::string idtag = boxes.at(0)->itemText(index).toLatin1().data();
    if(m_otherBoxItems.find(idtag)!=m_otherBoxItems.end()){
      std::vector< std::vector<std::string> > tmpVec = m_otherBoxItems[idtag];
      for(std::vector< std::vector<std::string> >::iterator it=tmpVec.begin(); it!=tmpVec.end();it++){
	for(unsigned int i = 0; i<tmpVec.size(); i++){
	  if((i+1)<boxes.size()){
	    boxes.at(i+1)->clear();
	    for(unsigned int j = 0; j<tmpVec.at(i).size(); j++)
	      boxes.at(i+1)->addItem(tmpVec.at(i).at(j).c_str());
	  }
	}
      }
    }
  }
}
