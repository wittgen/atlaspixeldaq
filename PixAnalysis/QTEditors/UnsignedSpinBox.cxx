#include <QLineEdit>
#include "UnsignedSpinBox.h"

UnsignedSpinBox::UnsignedSpinBox(QWidget* parent)
  : QSpinBox(parent)
{
  setBase(16);
  m_value = 0;
  lineEdit()->setValidator(0);
}

UnsignedSpinBox::~UnsignedSpinBox() {}

void UnsignedSpinBox::setBase(int base)
{
  m_base=base;
  QString prefix;
  switch(base){
  case 10:
  default:
    prefix="";
    break;
  case 16:
    prefix="0x";
    break;
  case 2:
    prefix="(b)";
    break;
  }
  setPrefix(prefix);
}

void UnsignedSpinBox::setUMinimum(unsigned long umin) {
  setMinimum(UtoI(umin));
}
void UnsignedSpinBox::setUMaximum(unsigned long umax) {
  setMaximum(UtoI(umax));
  setMinimumSize((40+fontMetrics().size(Qt::TextSingleLine, "0x"+textFromValue(UtoI(umax))).width()), 0);
}
int UnsignedSpinBox::base() const
{
  return m_base;
}

unsigned long UnsignedSpinBox::UValue() {
  m_value = ItoU(value());
  return m_value;
}

void UnsignedSpinBox::setUValue(unsigned long val){
  m_value = val;
  setValue(UtoI(m_value));
}

QString UnsignedSpinBox::textFromValue(int value) const
{
  return QString::number(ItoU(value), base()).toUpper();
}

int UnsignedSpinBox::valueFromText(const QString& /*text*/) const
{
  bool ok;
  return UtoI(cleanText().toULong(&ok, base()));
}
QValidator::State UnsignedSpinBox::validate(QString &input, int &pos) const {
  QString copy(cleanText());
  pos -= copy.size() - copy.trimmed().size();
  copy = copy.trimmed();
  if (copy.isEmpty())
    return QValidator::Intermediate;
  input = prefix() + copy.toUpper();
  bool okay;
  copy.toUInt(&okay, base());
  return okay?QValidator::Acceptable:QValidator::Invalid;
}
