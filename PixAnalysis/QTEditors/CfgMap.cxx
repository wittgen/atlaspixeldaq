#include "CfgMap.h"

#include <QMouseEvent>

CfgMap::CfgMap(uint **dat, uint maxval, uint ncol, uint nrow, bool flipped, QWidget* parent, Qt::WindowFlags fl, 
	       const char *gname, const char *mname, int chip, uint sx, uint sy)
  : QDialog(parent, fl), m_extData(dat), m_maxval(maxval), m_ncol(ncol), m_nrow(nrow), m_chip(chip), m_flipped(flipped) {

  setupUi();
  QObject::connect(CancelButton, SIGNAL(clicked()), this, SLOT(reject()));
  QObject::connect(AcceptButton, SIGNAL(clicked()), this, SLOT(save()));

  m_gname="";
  m_mname="";
  if(gname!=0) m_gname = gname;
  if(mname!=0) m_mname = mname;

  m_scaleX = sx;
  m_scaleY = sy;

  rowVal->setMaximum(m_nrow-1);
  colVal->setMaximum(m_ncol-1);

  colLimitLow->setMaximum(m_ncol-1);
  rowLimitLow->setMaximum(m_nrow-1);
  colLimitHigh->setMaximum(m_ncol-1);
  rowLimitHigh->setMaximum(m_nrow-1);


  pixelSetVal->setMaximum(m_maxval);

  delete TheMap;
  TheMap = new CfgPixMap(this);
  TheMap->setObjectName(QString::fromUtf8("TheMap"));
  TheMap->setScaledContents(true);
  TheMap->setWordWrap(false);
  //  QObject::connect(TheMap, SIGNAL(doubleMapClick(uint,uint)), this, SLOT(doubleMapClick(uint,uint)));
  QObject::connect(TheMap, SIGNAL(singleMapClick(uint,uint)), this, SLOT(singleMapClicked(uint,uint)));
  QObject::connect(colVal, SIGNAL(valueChanged(int)), this, SLOT(readPixVal()));
  QObject::connect(rowVal, SIGNAL(valueChanged(int)), this, SLOT(readPixVal()));
  QObject::connect(SetButton, SIGNAL(clicked()), this, SLOT(setPixVal()));

  QObject::connect(SetROIButton, SIGNAL(clicked()), this, SLOT(setROI()));

  QObject::connect(setColButton, SIGNAL(clicked()), this, SLOT(setColVal()));
  QObject::connect(setRowButton, SIGNAL(clicked()), this, SLOT(setRowVal()));
  QObject::connect(setGangedButton, SIGNAL(clicked()), this, SLOT(setGangVal()));
  QObject::connect(setIntGangedButton, SIGNAL(clicked()), this, SLOT(setIntgdVal()));
  QObject::connect(setLongButton, SIGNAL(clicked()), this, SLOT(setLongVal()));  
  QObject::connect(setAllButton, SIGNAL(clicked()), this, SLOT(setAllVal()));  

  mapVerticalLayout->addWidget(TheMap);
  
  m_img = new QImage((int)(m_ncol*m_scaleX),(int)(m_nrow*m_scaleY),QImage::Format_RGB32);

  m_data = new uint*[m_ncol];
  for(uint i=0;i<m_ncol; i++){
    m_data[i] = new uint[m_nrow];
    for(uint j=0;j<m_nrow; j++){
      m_data[i][j] = dat[i][m_flipped?(m_nrow-1-j):j];
      for(uint pi=0;pi<m_scaleX;pi++){
	for(uint pj=0;pj<m_scaleY;pj++){
		mapValueToPixelColor(m_scaleX*i+pi,m_scaleY*j+pj,(double) m_data[i][j]);
	}
      }
    }
  }
  TheMap->setPixmap(QPixmap::fromImage(*m_img));
  readPixVal();

}
CfgMap::~CfgMap(){
  for(uint i=0;i<m_ncol; i++)
    delete[] m_data[i];
  delete[] m_data;
  delete m_img;
}
void CfgMap::setupUi() {
  if (objectName().isEmpty())
    setObjectName(QString::fromUtf8("CfgMap"));
  resize(682, 575);
  horizontalLayout_15 = new QHBoxLayout(this);
  horizontalLayout_15->setSpacing(6);
  horizontalLayout_15->setContentsMargins(11, 11, 11, 11);
  horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
  verticalLayout_8 = new QVBoxLayout();
  verticalLayout_8->setSpacing(6);
  verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
  horizontalLayout_14 = new QHBoxLayout();
  horizontalLayout_14->setSpacing(6);
  horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
  verticalLayout_3 = new QVBoxLayout();
  verticalLayout_3->setSpacing(6);
  verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
  verticalLayout = new QVBoxLayout();
  verticalLayout->setSpacing(6);
  verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
  horizontalLayout = new QHBoxLayout();
  horizontalLayout->setSpacing(6);
  horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
  pixelLabel = new QLabel(this);
  pixelLabel->setObjectName(QString::fromUtf8("pixelLabel"));
  
  horizontalLayout->addWidget(pixelLabel);
  
  horizontalSpacer_2 = new QSpacerItem(28, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
  
  horizontalLayout->addItem(horizontalSpacer_2);
  
  pixelVal = new QLabel(this);
  pixelVal->setObjectName(QString::fromUtf8("pixelVal"));
  
  horizontalLayout->addWidget(pixelVal);
  
  verticalLayout->addLayout(horizontalLayout);
  
  horizontalLayout_12 = new QHBoxLayout();
  horizontalLayout_12->setSpacing(6);
  horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
  colLabel = new QLabel(this);
  colLabel->setObjectName(QString::fromUtf8("colLabel"));
  
  horizontalLayout_12->addWidget(colLabel);
  
  horizontalSpacer_6 = new QSpacerItem(28, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
  
  horizontalLayout_12->addItem(horizontalSpacer_6);
  
  colVal = new QSpinBox(this);
  colVal->setObjectName(QString::fromUtf8("colVal"));
  colVal->setMinimumSize(QSize(80, 0));
  colVal->setMaximum(255);
  
  horizontalLayout_12->addWidget(colVal);
  
  
  verticalLayout->addLayout(horizontalLayout_12);
  
  horizontalLayout_13 = new QHBoxLayout();
  horizontalLayout_13->setSpacing(6);
  horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
  rowLabel = new QLabel(this);
  rowLabel->setObjectName(QString::fromUtf8("rowLabel"));
  
  horizontalLayout_13->addWidget(rowLabel);
  
  horizontalSpacer_13 = new QSpacerItem(28, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
  
  horizontalLayout_13->addItem(horizontalSpacer_13);
  
  rowVal = new QSpinBox(this);
  rowVal->setObjectName(QString::fromUtf8("rowVal"));
  rowVal->setMinimumSize(QSize(80, 0));
  rowVal->setMaximum(255);
  
  horizontalLayout_13->addWidget(rowVal);
  
  
  verticalLayout->addLayout(horizontalLayout_13);
  
  
  verticalLayout_3->addLayout(verticalLayout);
  
  verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);
  
  verticalLayout_3->addItem(verticalSpacer);
  
  line = new QFrame(this);
  line->setObjectName(QString::fromUtf8("line"));
  line->setFrameShape(QFrame::HLine);
  line->setFrameShadow(QFrame::Sunken);
  
  verticalLayout_3->addWidget(line);
  
  verticalLayout_7 = new QVBoxLayout();
  verticalLayout_7->setSpacing(6);
  verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
  verticalLayout_2 = new QVBoxLayout();
  verticalLayout_2->setSpacing(6);
  verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
  horizontalLayout_3 = new QHBoxLayout();
  horizontalLayout_3->setSpacing(6);
  horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
  SetButton = new QPushButton(this);
  SetButton->setObjectName(QString::fromUtf8("SetButton"));
  
  horizontalLayout_3->addWidget(SetButton);
  
  horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
  
  horizontalLayout_3->addItem(horizontalSpacer_5);
  
  label = new QLabel(this);
  label->setObjectName(QString::fromUtf8("label"));
  
  horizontalLayout_3->addWidget(label);
  
  pixelSetVal = new QSpinBox(this);
  pixelSetVal->setObjectName(QString::fromUtf8("pixelSetVal"));
  pixelSetVal->setMaximum(255);
  
  horizontalLayout_3->addWidget(pixelSetVal);
  
  
  verticalLayout_2->addLayout(horizontalLayout_3);
  
  horizontalLayout_6 = new QHBoxLayout();
  horizontalLayout_6->setSpacing(6);
  horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
  setColButton = new QPushButton(this);
  setColButton->setObjectName(QString::fromUtf8("setColButton"));
  
  horizontalLayout_6->addWidget(setColButton);
  
  horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
  
  horizontalLayout_6->addItem(horizontalSpacer_7);
  
  setRowButton = new QPushButton(this);
  setRowButton->setObjectName(QString::fromUtf8("setRowButton"));
  
  horizontalLayout_6->addWidget(setRowButton);
  
  verticalLayout_2->addLayout(horizontalLayout_6);
  
  horizontalLayout_7 = new QHBoxLayout();
  horizontalLayout_7->setSpacing(6);
  horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
  setGangedButton = new QPushButton(this);
  setGangedButton->setObjectName(QString::fromUtf8("setGangedButton"));
  
  horizontalLayout_7->addWidget(setGangedButton);
  
  horizontalSpacer_15 = new QSpacerItem(30, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);
  
  horizontalLayout_7->addItem(horizontalSpacer_15);
  
  setIntGangedButton = new QPushButton(this);
  setIntGangedButton->setObjectName(QString::fromUtf8("setIntGangedButton"));
  
  horizontalLayout_7->addWidget(setIntGangedButton);
  
  horizontalSpacer_16 = new QSpacerItem(30, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);
  
  horizontalLayout_7->addItem(horizontalSpacer_16);
  
  setLongButton = new QPushButton(this);
  setLongButton->setObjectName(QString::fromUtf8("setLongButton"));
  
  horizontalLayout_7->addWidget(setLongButton);
  
  horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
  
  horizontalLayout_7->addItem(horizontalSpacer_8);
  
  
  verticalLayout_2->addLayout(horizontalLayout_7);
  
  horizontalLayout_10 = new QHBoxLayout();
  horizontalLayout_10->setSpacing(6);
  horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
  setAllButton = new QPushButton(this);
  setAllButton->setObjectName(QString::fromUtf8("setAllButton"));
  
  horizontalLayout_10->addWidget(setAllButton);
  
  horizontalSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
  
  horizontalLayout_10->addItem(horizontalSpacer_11);
  
  
  verticalLayout_2->addLayout(horizontalLayout_10);
  
  
  verticalLayout_7->addLayout(verticalLayout_2);
  
  verticalSpacer_4 = new QSpacerItem(20, 30, QSizePolicy::Minimum, QSizePolicy::Fixed);
  
  verticalLayout_7->addItem(verticalSpacer_4);
  
  verticalLayout_6 = new QVBoxLayout();
  verticalLayout_6->setSpacing(6);
  verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
  horizontalLayout_5 = new QHBoxLayout();
  horizontalLayout_5->setSpacing(6);
  horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
  SetROIButton = new QPushButton(this);
  SetROIButton->setObjectName(QString::fromUtf8("SetROIButton"));
  
  horizontalLayout_5->addWidget(SetROIButton);
  
  horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
  
  horizontalLayout_5->addItem(horizontalSpacer_4);
  
  
  verticalLayout_6->addLayout(horizontalLayout_5);
  
  horizontalLayout_4 = new QHBoxLayout();
  horizontalLayout_4->setSpacing(6);
  horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
  verticalLayout_4 = new QVBoxLayout();
  verticalLayout_4->setSpacing(6);
  verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
  pixelLabel_2 = new QLabel(this);
  pixelLabel_2->setObjectName(QString::fromUtf8("pixelLabel_2"));
  
  verticalLayout_4->addWidget(pixelLabel_2);
  
  colLabel_2 = new QLabel(this);
  colLabel_2->setObjectName(QString::fromUtf8("colLabel_2"));
  
  verticalLayout_4->addWidget(colLabel_2);
  
  rowLabel_2 = new QLabel(this);
  rowLabel_2->setObjectName(QString::fromUtf8("rowLabel_2"));
  
  verticalLayout_4->addWidget(rowLabel_2);
  
  rowLabel_3 = new QLabel(this);
  rowLabel_3->setObjectName(QString::fromUtf8("rowLabel_3"));
  
  verticalLayout_4->addWidget(rowLabel_3);
  
  
  horizontalLayout_4->addLayout(verticalLayout_4);
  
  horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
  
  horizontalLayout_4->addItem(horizontalSpacer_3);
  
  verticalLayout_5 = new QVBoxLayout();
  verticalLayout_5->setSpacing(6);
  verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
  colLimitLow = new QSpinBox(this);
  colLimitLow->setObjectName(QString::fromUtf8("colLimitLow"));
  
  verticalLayout_5->addWidget(colLimitLow);
  
  rowLimitLow = new QSpinBox(this);
  rowLimitLow->setObjectName(QString::fromUtf8("rowLimitLow"));
  
  verticalLayout_5->addWidget(rowLimitLow);
  
  colLimitHigh = new QSpinBox(this);
  colLimitHigh->setObjectName(QString::fromUtf8("colLimitHigh"));
  
  verticalLayout_5->addWidget(colLimitHigh);
  
  rowLimitHigh = new QSpinBox(this);
  rowLimitHigh->setObjectName(QString::fromUtf8("rowLimitHigh"));
  
  verticalLayout_5->addWidget(rowLimitHigh);
  
  
  horizontalLayout_4->addLayout(verticalLayout_5);
  
  
  verticalLayout_6->addLayout(horizontalLayout_4);
  
  
  verticalLayout_7->addLayout(verticalLayout_6);
  
  
  verticalLayout_3->addLayout(verticalLayout_7);
  
  verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

  verticalLayout_3->addItem(verticalSpacer_3);
  
  
  horizontalLayout_14->addLayout(verticalLayout_3);
  
  mapVerticalLayout = new QVBoxLayout();
  mapVerticalLayout->setSpacing(6);
  mapVerticalLayout->setObjectName(QString::fromUtf8("mapVerticalLayout"));
  TheMap = new QLabel(this);
  TheMap->setObjectName(QString::fromUtf8("TheMap"));
  QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
  sizePolicy.setHorizontalStretch(0);
  sizePolicy.setVerticalStretch(0);
  sizePolicy.setHeightForWidth(TheMap->sizePolicy().hasHeightForWidth());
  TheMap->setSizePolicy(sizePolicy);
  TheMap->setFrameShape(QFrame::Box);
  TheMap->setScaledContents(true);
  
  mapVerticalLayout->addWidget(TheMap);
  
  verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);
  
  mapVerticalLayout->addItem(verticalSpacer_2);
  
  
  horizontalLayout_14->addLayout(mapVerticalLayout);
  
  
  verticalLayout_8->addLayout(horizontalLayout_14);
  
  horizontalLayout_2 = new QHBoxLayout();
  horizontalLayout_2->setSpacing(6);
  horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
  horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
  
  horizontalLayout_2->addItem(horizontalSpacer);
  
  AcceptButton = new QPushButton(this);
  AcceptButton->setObjectName(QString::fromUtf8("AcceptButton"));
  
  horizontalLayout_2->addWidget(AcceptButton);
  
  spacer24 = new QSpacerItem(20, 18, QSizePolicy::Fixed, QSizePolicy::Minimum);
  
  horizontalLayout_2->addItem(spacer24);
  
  CancelButton = new QPushButton(this);
  CancelButton->setObjectName(QString::fromUtf8("CancelButton"));
  
  horizontalLayout_2->addWidget(CancelButton);
  
  
  verticalLayout_8->addLayout(horizontalLayout_2);
  
  
  horizontalLayout_15->addLayout(verticalLayout_8);
  
  
  retranslateUi();
  
  QMetaObject::connectSlotsByName(this);
}

void CfgMap::retranslateUi() {
  setWindowTitle(QApplication::translate("CfgMap", "Config Map", 0));
  pixelLabel->setText(QApplication::translate("CfgMap", "Pixel value:    ", 0));
#ifndef QT_NO_TOOLTIP
  pixelVal->setToolTip(QApplication::translate("CfgMap", "value currently set for selected pixel", 0));
#endif // QT_NO_TOOLTIP
  pixelVal->setText(QApplication::translate("CfgMap", "  0", 0));
  colLabel->setText(QApplication::translate("CfgMap", "Column: ", 0));
#ifndef QT_NO_TOOLTIP
  colVal->setToolTip(QApplication::translate("CfgMap", "column of selected pixel", 0));
#endif // QT_NO_TOOLTIP
  rowLabel->setText(QApplication::translate("CfgMap", "Row: ", 0));
#ifndef QT_NO_TOOLTIP
  rowVal->setToolTip(QApplication::translate("CfgMap", "row of selected pixel", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
  SetButton->setToolTip(QApplication::translate("CfgMap", "set pixel identified by row/column as above to value given to the right", 0));
#endif // QT_NO_TOOLTIP
  SetButton->setText(QApplication::translate("CfgMap", "Set selected pixel", 0));
  label->setText(QApplication::translate("CfgMap", "to value", 0));
#ifndef QT_NO_TOOLTIP
  pixelSetVal->setToolTip(QApplication::translate("CfgMap", "value that currently selected pixel is set to when button \"Set selected pixel\" is pushed", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
  setColButton->setToolTip(QApplication::translate("CfgMap", "set all pixels identified by column as above to specified value", 0));
#endif // QT_NO_TOOLTIP
  setColButton->setText(QApplication::translate("CfgMap", "Set entire col", 0));
#ifndef QT_NO_TOOLTIP
  setRowButton->setToolTip(QApplication::translate("CfgMap", "set all pixels identified by row as above to specified value", 0));
#endif // QT_NO_TOOLTIP
  setRowButton->setText(QApplication::translate("CfgMap", "Set entire row", 0));
#ifndef QT_NO_TOOLTIP
  setGangedButton->setToolTip(QApplication::translate("CfgMap", "set all ganged pixels (row=153,155,157,159) to specified value", 0));
#endif // QT_NO_TOOLTIP
  setGangedButton->setText(QApplication::translate("CfgMap", "Set ganged pixels", 0));
#ifndef QT_NO_TOOLTIP
  setIntGangedButton->setToolTip(QApplication::translate("CfgMap", "set all ganged pixels (row=154,156,158) to specified value", 0));
#endif // QT_NO_TOOLTIP
  setIntGangedButton->setText(QApplication::translate("CfgMap", "Set inter-g'ed pixels", 0));
#ifndef QT_NO_TOOLTIP
  setLongButton->setToolTip(QApplication::translate("CfgMap", "set all ganged pixels (col=0,17) to specified value", 0));
#endif // QT_NO_TOOLTIP
  setLongButton->setText(QApplication::translate("CfgMap", "Set long pixels", 0));
#ifndef QT_NO_TOOLTIP
  setAllButton->setToolTip(QApplication::translate("CfgMap", "set all pixels to specified value", 0));
#endif // QT_NO_TOOLTIP
  setAllButton->setText(QApplication::translate("CfgMap", "Set all pixels", 0));
  SetROIButton->setText(QApplication::translate("CfgMap", "ROI", 0));
  pixelLabel_2->setText(QApplication::translate("CfgMap", "ROI col low:", 0));
  colLabel_2->setText(QApplication::translate("CfgMap", "ROI row low:", 0));
  rowLabel_2->setText(QApplication::translate("CfgMap", "ROI col high:", 0));
  rowLabel_3->setText(QApplication::translate("CfgMap", "ROI row high:", 0));
  TheMap->setText(QString());
  AcceptButton->setText(QApplication::translate("CfgMap", "Save", 0));
  CancelButton->setText(QApplication::translate("CfgMap", "Cancel", 0));
}

void CfgMap::save(){
  for(uint i=0;i<m_ncol; i++){
    for(uint j=0;j<m_nrow; j++){
      m_extData[i][j] = m_data[i][m_flipped?(m_nrow-1-j):j];
    }
  }
  accept();
}
void CfgMap::readPixVal(){
  int row = m_flipped?rowVal->value():(m_nrow-1-rowVal->value());
  pixelVal->setText(QString::number(m_data[colVal->value()][row]));
}
void CfgMap::setPixVal(){
  int row = m_flipped?rowVal->value():(m_nrow-1-rowVal->value());
  m_data[colVal->value()][row] = (uint)pixelSetVal->value();
  // refresh image
  for(uint pi=0;pi<m_scaleX;pi++){
    for(uint pj=0;pj<m_scaleY;pj++){
    	mapValueToPixelColor(m_scaleX*colVal->value()+pi,m_scaleY*row+pj,(double)pixelSetVal->value());
    }
  }
  TheMap->setPixmap(QPixmap::fromImage(*m_img));
  readPixVal();
}


void CfgMap::setROI(){

// set all pixels to 1
	  for(unsigned int k=0;k<m_ncol;k++){
	    for(unsigned int i =0;i<m_nrow;i++){
	      m_data[k][i] = 1;
	      // refresh image
	      for(uint pi=0;pi<m_scaleX;pi++){
		  for(uint pj=0;pj<m_scaleY;pj++){
			  mapValueToPixelColor(m_scaleX*k+pi,m_scaleY*i+pj,(double)m_maxval);
		}
	      }
	    }
	  }
	  TheMap->setPixmap(QPixmap::fromImage(*m_img));
	  readPixVal();

// set selected rectangle (region of interest) of pixels to 0
	  for (uint ROIi = (uint)colLimitLow->value(); ROIi<(uint)colLimitHigh->value(); ROIi++){
		  for (uint ROIj = (uint)rowLimitLow->value(); ROIj<(uint)rowLimitHigh->value(); ROIj++){
		    int row = m_flipped?ROIj:(m_nrow-1-ROIj);
		    m_data[ROIi][row] = 0;

		  // refresh image
    	  for(uint pi=0;pi<m_scaleX;pi++){
    		  for(uint pj=0;pj<m_scaleY;pj++){
    			  mapValueToPixelColor(m_scaleX*ROIi+pi,m_scaleY*row+pj,(double)pixelSetVal->value());
    		  	  	  	  	  	  	  	  	  }
		  	  	  	  	  	  	  	  	  }
		  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	}
	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	 }
	  TheMap->setPixmap(QPixmap::fromImage(*m_img));
	  readPixVal();
}


void CfgMap::setColVal(){
  for(unsigned int i =0;i<m_nrow;i++){
    m_data[colVal->value()][i] = (uint)pixelSetVal->value();

    // refresh image
    for(uint pi=0;pi<m_scaleX;pi++){
      for(uint pj=0;pj<m_scaleY;pj++){
    	  mapValueToPixelColor(m_scaleX*colVal->value()+pi,m_scaleY*i+pj,(double)pixelSetVal->value());
      }
    }
  }
  TheMap->setPixmap(QPixmap::fromImage(*m_img));
  readPixVal();
}
void CfgMap::setRowVal(){
  for(unsigned int i =0;i<m_ncol;i++){
    int row = m_flipped?rowVal->value():(m_nrow-1-rowVal->value());
    m_data[i][row] = (uint)pixelSetVal->value();
    // refresh image
    for(uint pi=0;pi<m_scaleX;pi++){
      for(uint pj=0;pj<m_scaleY;pj++){
    	  mapValueToPixelColor(m_scaleX*i+pi,m_scaleY*row+pj,(double)pixelSetVal->value());
      }
    }
  }
  TheMap->setPixmap(QPixmap::fromImage(*m_img));
  readPixVal();
}
void CfgMap::setGangVal(){
  const unsigned int ngrow=4;
  unsigned int grow[ngrow]={153,155,157,159};
  for(unsigned int k=0;k<ngrow;k++){
    for(unsigned int i =0;i<m_ncol;i++){
      int row = m_flipped?grow[k]:(m_nrow-1-grow[k]);
      m_data[i][row] = (uint)pixelSetVal->value();
      // refresh image
      for(uint pi=0;pi<m_scaleX;pi++){
	for(uint pj=0;pj<m_scaleY;pj++){
		mapValueToPixelColor(m_scaleX*i+pi,m_scaleY*row+pj,(double)pixelSetVal->value());
	}
      }
    }
  }
  TheMap->setPixmap(QPixmap::fromImage(*m_img));
}
void CfgMap::setIntgdVal(){
  const unsigned int nigrow=3;
  unsigned int igrow[nigrow]={154,156,158};
  for(unsigned int k=0;k<nigrow;k++){
    for(unsigned int i =0;i<m_ncol;i++){
      int row = m_flipped?igrow[k]:(m_nrow-1-igrow[k]);
     m_data[i][row] = (uint)pixelSetVal->value();
      // refresh image
      for(uint pi=0;pi<m_scaleX;pi++){
	for(uint pj=0;pj<m_scaleY;pj++){
		mapValueToPixelColor(m_scaleX*i+pi,m_scaleY*row+pj,(double)pixelSetVal->value());
	}
      }
    }
  }
  TheMap->setPixmap(QPixmap::fromImage(*m_img));
  readPixVal();
}
void CfgMap::setLongVal(){
  const unsigned int nlcol=2;
  unsigned int lcol[nlcol]={0,17};
  for(unsigned int k=0;k<nlcol;k++){
    for(unsigned int i =0;i<m_nrow;i++){
      m_data[lcol[k]][i] = (uint)pixelSetVal->value();
      // refresh image
      for(uint pi=0;pi<m_scaleX;pi++){
	for(uint pj=0;pj<m_scaleY;pj++){
		mapValueToPixelColor(m_scaleX*lcol[k]+pi,m_scaleY*i+pj,(double)pixelSetVal->value());
	}
      }
    }
  }
  TheMap->setPixmap(QPixmap::fromImage(*m_img));
  readPixVal();
}
void CfgMap::setAllVal(){
  for(unsigned int k=0;k<m_ncol;k++){
    for(unsigned int i =0;i<m_nrow;i++){
      m_data[k][i] = (uint)pixelSetVal->value();

      // refresh image
      for(uint pi=0;pi<m_scaleX;pi++){
	for(uint pj=0;pj<m_scaleY;pj++){
		mapValueToPixelColor(m_scaleX*k+pi,m_scaleY*i+pj,(double)pixelSetVal->value());
	}
      }
    }
  }
  TheMap->setPixmap(QPixmap::fromImage(*m_img));
  readPixVal();
}
void CfgMap::singleMapClicked(uint x, uint y){
  //  printf("release at %d %d\n",x/m_scaleX, y/m_scaleY);
  colVal->setValue(x/m_scaleX);
  rowVal->setValue(m_flipped?(y/m_scaleY):(m_nrow-y/m_scaleY-1));
  readPixVal();
}
void CfgMap::mapValueToPixelColor(unsigned int pX, unsigned int pY, double pValue)
{
	double pRedValue = 0;
	double pGreenValue = 0;
	double pBlueValue = 0;

	if(m_maxval > 128){	//only use new mapping for higher range to prevent confusion
		if(pValue<(double)m_maxval/3.){
			pRedValue = 255. / m_maxval * pValue * 3.;
			pGreenValue = 0;
			pBlueValue = 0;
		}
		if(pValue>(double)m_maxval/3. && pValue<2.*(double)m_maxval/3.){
			pRedValue = 0;
			pGreenValue = 0;
			pBlueValue = 255. / m_maxval * pValue * 3.;
		}
		if(pValue>2.*(double)m_maxval/3.){
			pRedValue = 0;
			pGreenValue = 255. / m_maxval * pValue * 3.;
			pBlueValue = 0;
		}
	}
	else
		pRedValue = 255. / m_maxval * pValue;
	m_img->setPixel(pX, pY,qRgb((int)pRedValue,(int)pGreenValue,(int)pBlueValue));
}

void CfgMap::setFei4Style(){
  setGangedButton->hide();
  setIntGangedButton->hide();
}

// the actual pixel map
CfgPixMap::CfgPixMap( QWidget * parent) : QLabel(parent){
}
void CfgPixMap::mouseDoubleClickEvent (QMouseEvent *event){
  emit doubleMapClick(event->x(), event->y());
}
void CfgPixMap::mouseReleaseEvent (QMouseEvent *event){
  emit singleMapClick(event->x(), event->y());
}
