#include <Config/Config.h>
#include <ConfigWrapper/ConfObjUtil.h>
#include <Config/ConfGroup.h>
#include <QLabel>
#include <QLineEdit>
#include <QCheckBox>
#include <QComboBox>
#include <QSpinBox>
#include <QHBoxLayout>

#include "WidgetConfObjMap.h"
#include "UnsignedSpinBox.h"

using namespace PixLib;

PixLib::ConfObj& findObject(std::string combName, PixLib::Config &a_config){
    std::string grpN, objN;
    grpN = combName;
    objN = combName;
    std::string::size_type pos = grpN.find("_");
    if(pos!=std::string::npos){
      grpN.erase(pos,grpN.length()-pos);
      objN.erase(0,pos+1);
    }
    PixLib::ConfObj &obj = a_config[grpN][objN];
    if(obj.name()=="__TrashConfObj__"){
      // link and alias don't follow groupname_objname convention -> search all groups
      for (unsigned int i=0; i<(unsigned int)a_config.size(); i++) {
	ConfGroup &grp = a_config[i];
	for (unsigned int j=0; j<(unsigned int)grp.size(); j++) {
	  PixLib::ConfObj &alt_obj = grp[j];
	  if((alt_obj.type() == PixLib::ConfObj::ALIAS || alt_obj.type() == PixLib::ConfObj::LINK)
	     && alt_obj.name()==combName)
	    return alt_obj;
	}
      }
      std::cerr << "findObject : object for group " << grpN << ", name " << objN << " is trash, alt. method without result" << std::endl;
    }

    return obj;
}
WidgetConfObjMap::WidgetConfObjMap(PixLib::Config &config) : m_config(&config) {
  m_handles.clear();
}

WidgetConfObjMap::WidgetConfObjMap(const WidgetConfObjMap &b) : m_config(const_cast<WidgetConfObjMap &>(b).m_config) {
  m_handles = const_cast<WidgetConfObjMap &>(b).m_handles;
}

WidgetConfObjMap::~WidgetConfObjMap(){
  m_handles.clear();
}
WidgetConfObjMap& WidgetConfObjMap::operator=(const WidgetConfObjMap &b) {
  m_handles = const_cast<WidgetConfObjMap &>(b).m_handles;
  m_config  = const_cast<WidgetConfObjMap &>(b).m_config;
  return *this;
}
void WidgetConfObjMap::save(PixLib::Config &a_config) {
  // save to cfg
  for(std::map<std::string,QWidget*>::iterator it=m_handles.begin(); it!=m_handles.end(); it++){
    PixLib::ConfObj &obj = findObject(it->first, a_config);
    switch(obj.type()){
    case PixLib::ConfObj::ALIAS: 
      ((PixLib::ConfAlias&) obj).m_value = ((QLineEdit*)it->second)->text().toLatin1().data();
      break;
    case PixLib::ConfObj::LINK:
      ((PixLib::ConfLink&) obj).m_value = ((QLineEdit*)it->second)->text().toLatin1().data();
      break;
    case PixLib::ConfObj::STRING:
      ((PixLib::ConfString&) obj).m_value = ((QLineEdit*)it->second)->text().toLatin1().data();
      break;
    case PixLib::ConfObj::BOOL:
      ((PixLib::ConfBool&) obj).m_value = ((QCheckBox*)it->second)->isChecked();
      break;
    case PixLib::ConfObj::INT:
      switch (obj.subtype()) {
      case PixLib::ConfInt::U32:
	confVal<unsigned int>(static_cast<PixLib::ConfInt&>(obj))=static_cast<UnsignedSpinBox*>(it->second)->UValue();
	break;
      case PixLib::ConfInt::S32:
	confVal<int>(static_cast<PixLib::ConfInt&>(obj))=static_cast<QSpinBox*>(it->second)->value();
	break;
      case PixLib::ConfInt::U16:
	confVal<unsigned short>(static_cast<PixLib::ConfInt&>(obj))=static_cast<unsigned short>(static_cast<UnsignedSpinBox*>(it->second)->UValue());
	break;
      case PixLib::ConfInt::S16:
	confVal<short>(static_cast<PixLib::ConfInt&>(obj))=static_cast<short>(static_cast<QSpinBox*>(it->second)->value());
	break;
      case PixLib::ConfInt::U8:
	confVal<unsigned char>(static_cast<PixLib::ConfInt&>(obj))=static_cast<unsigned char>(static_cast<UnsignedSpinBox*>(it->second)->UValue());
	break;
      case PixLib::ConfInt::S8:
	confVal<char>(static_cast<PixLib::ConfInt&>(obj))=static_cast<char>(static_cast<QSpinBox*>(it->second)->value());
	break;
          default: throw ConfigExc("Invalid Config Subtype");
      }
      break;
    case PixLib::ConfObj::LIST:{
      int read_val = (int) ((PixLib::ConfList&)obj).m_symbols[((QComboBox*)it->second)->currentText().toLatin1().data()];
      switch( obj.subtype() ){
      case PixLib::ConfList::S32:
	*((int *)((PixLib::ConfList&)obj).m_value) = (int) read_val;
	break;
      case PixLib::ConfList::U32:
	*((unsigned int *)((PixLib::ConfList&)obj).m_value) = (unsigned int) read_val;
	break;
      case PixLib::ConfList::S16:
	*((short int *)((PixLib::ConfList&)obj).m_value) = (short int) read_val;
	break;
      case PixLib::ConfList::U16:
	*((unsigned short int *)((PixLib::ConfList&)obj).m_value) = (unsigned short int) read_val;
	break;
      case PixLib::ConfList::S8:
	*((char *)((PixLib::ConfList&)obj).m_value) = (char) read_val;
	break;
      case PixLib::ConfList::U8:
	*((unsigned char *)((PixLib::ConfList&)obj).m_value) = (unsigned char) read_val;
	break;
      default:
	break;
      }
      break;}
    case PixLib::ConfObj::FLOAT:
      ((PixLib::ConfFloat&) obj).m_value = ((QDoubleSpinBox*)it->second)->value();
      break;
    case PixLib::ConfObj::STRVECT:{
      std::vector<std::string> &tmpVec = ((PixLib::ConfStrVect&)obj).value();
	tmpVec.clear();
	for(int i=0;i<((ConfTable*)it->second)->rowCount(); i++)
	  tmpVec.push_back(std::string(((ConfTable*)it->second)->item(i,0)->text().toLatin1().data()));
	break;}
    case PixLib::ConfObj::VECTOR:
      if(obj.subtype()==PixLib::ConfVector::V_INT){
	std::vector<int> &tmpVec = ((PixLib::ConfVector&)obj).valueVInt();
	tmpVec.clear();
	for(int i=0;i<((ConfTable*)it->second)->rowCount(); i++)
	  tmpVec.push_back(((ConfTable*)it->second)->item(i,0)->text().toInt());
	
      }
      else if(obj.subtype()==PixLib::ConfVector::V_UINT){
	std::vector<unsigned int> &tmpVec = ((PixLib::ConfVector&)obj).valueVUint();
	tmpVec.clear();
	for(int i=0;i<((ConfTable*)it->second)->rowCount(); i++)
	  tmpVec.push_back(((ConfTable*)it->second)->item(i,0)->text().toUInt());
	
      }
      else if(obj.subtype()==PixLib::ConfVector::V_FLOAT){
	std::vector<float> &tmpVec = ((PixLib::ConfVector&)obj).valueVFloat();
	tmpVec.clear();
	for(int i=0;i<((ConfTable*)it->second)->rowCount(); i++){
	  tmpVec.push_back(((ConfTable*)it->second)->item(i,0)->text().toFloat());
	}	
      }
      break;
    case PixLib::ConfObj::MATRIX:{
      PixLib::ConfMatrix &mapobj = (PixLib::ConfMatrix&)obj;
      if(mapobj.subtype()==PixLib::ConfMatrix::M_U16){
	PixLib::ConfMask<unsigned short int> &mmask = *((PixLib::ConfMask<unsigned short int> *)mapobj.m_value);
	for(int i=0;i<((ConfTable*)it->second)->rowCount(); i++){
	  for(int j=0;j<((ConfTable*)it->second)->columnCount(); j++){
	    mmask.set(j,i, ((ConfTable*)it->second)->item(i,0)->text().toUInt());
	  }
	}
      }else if(mapobj.subtype()==PixLib::ConfMatrix::M_U1){
	PixLib::ConfMask<bool> &mmask = *((PixLib::ConfMask<bool> *)mapobj.m_value);
	for(int i=0;i<((ConfTable*)it->second)->rowCount(); i++){
	  for(int j=0;j<((ConfTable*)it->second)->columnCount(); j++){
	    mmask.set(j,i, (bool)((ConfTable*)it->second)->item(i,0)->text().toUInt());
	  }
	}
      }
      break;}
    default:
      std::cerr << "Can't process object " << obj.name() << " received for handle " << it->first << std::endl;
      break;
    }
  }
}
void WidgetConfObjMap::update(PixLib::Config &a_config){
  // save to cfg
  for(std::map<std::string,QWidget*>::iterator it=m_handles.begin(); it!=m_handles.end(); it++){
    PixLib::ConfObj &obj = findObject(it->first, a_config);
    /*
    std::string grpN, objN;
    grpN = it->first;
    objN = it->first;
    std::string::size_type pos = grpN.find("_");
    if(pos!=std::string::npos){
      grpN.erase(pos,grpN.length()-pos);
      objN.erase(0,pos+1);
    }
    PixLib::ConfObj &obj = a_config[grpN][objN];
    */
    update(obj);
  }
}
void WidgetConfObjMap::update(PixLib::ConfObj &obj){
  std::map<std::string,QWidget*>::iterator it = m_handles.find(obj.name());
  if(it==m_handles.end()){
    std::cerr << "WidgetConfObjMap::update() : can't find widget for object " << obj.name() << std::endl;
    return;
  }
  QWidget *tmpWgt=it->second;
  switch(obj.type()){
  case PixLib::ConfObj::ALIAS:
    ((QLineEdit*)tmpWgt)->setText( ((PixLib::ConfAlias&) obj).value().c_str());
    break;
  case PixLib::ConfObj::LINK:
    ((QLineEdit*)tmpWgt)->setText( ((PixLib::ConfLink&) obj).value().c_str());
    break;
  case PixLib::ConfObj::STRING:
    ((QLineEdit*)tmpWgt)->setText( ((PixLib::ConfString&) obj).value().c_str());
    break;
  case PixLib::ConfObj::BOOL:
    ((QCheckBox*)tmpWgt)->setChecked( ((PixLib::ConfBool&) obj).value() ) ;
    break;
  case PixLib::ConfObj::FLOAT:{
    ((QDoubleSpinBox*)tmpWgt)->setValue(((PixLib::ConfFloat&)obj).value());
    break;}
  case PixLib::ConfObj::INT:
    if (obj.subtype() == PixLib::ConfInt::U32 || obj.subtype() == PixLib::ConfInt::U16 ||
	    obj.subtype() == PixLib::ConfInt::U8) {
      ((UnsignedSpinBox*)tmpWgt)->setUValue(((PixLib::ConfInt&)obj).value());
    }
    else {
      ((QSpinBox*)tmpWgt)->setValue(((PixLib::ConfInt&)obj).value());
    }
    break;
  case PixLib::ConfObj::LIST:{
    QComboBox *cbhandle=(QComboBox*)tmpWgt;
    int currID=-1;
//     std::map<std::string, int>::const_iterator mapIT;
//     for(mapIT = ((PixLib::ConfList&)obj).m_symbols.begin(); mapIT != ((PixLib::ConfList&)obj).m_symbols.end();mapIT++){
//       if(mapIT->first==((PixLib::ConfList&)obj).sValue()){
// 	currID = cbhandle->count()-1;
// 	break;
//       }
//     }
    for(int i=0;i<cbhandle->count();i++){
      if(((PixLib::ConfList&)obj).sValue()==std::string(cbhandle->itemText(i).toLatin1().data())){
	currID=i;
	break;
      }
    }
    if(currID>=0) cbhandle->setCurrentIndex(currID);
    break;}
  case PixLib::ConfObj::STRVECT:
  case PixLib::ConfObj::VECTOR:{
    QSpinBox *sb = (QSpinBox*)m_handles[obj.name()+"_sb1"];
    ConfTable *table = (ConfTable *) tmpWgt;
    table->clear();
    unsigned int vsize=0;
    void *vecPtr = 0;
    if(obj.type()==PixLib::ConfObj::STRVECT){
      std::vector<std::string> &tmpVec = ((ConfStrVect&)obj).value();
      vecPtr = (void*)&tmpVec;
      vsize = tmpVec.size();
    }
    else if(obj.subtype()==ConfVector::V_INT){
      std::vector<int> &tmpVec = ((ConfVector&)obj).valueVInt();
      vecPtr = (void*)&tmpVec;
      vsize = tmpVec.size();
    }
    else if(obj.subtype()==ConfVector::V_UINT){
      std::vector<unsigned int> &tmpVec = ((ConfVector&)obj).valueVUint();
      vecPtr = (void*)&tmpVec;
      vsize = tmpVec.size();
    }
    else if(obj.subtype()==ConfVector::V_FLOAT){
      std::vector<float> &tmpVec = ((ConfVector&)obj).valueVFloat();
      vecPtr = (void*)&tmpVec;
      vsize = tmpVec.size();
    }
    table->setRowCount(vsize);
    sb->setValue(vsize);
    table->setColumnCount(1);
    for(unsigned int j=0;j<vsize; j++){
      QString sval;
      if(obj.type()==PixLib::ConfObj::STRVECT)
	sval = QString(((std::vector<std::string>*)vecPtr)->at(j).c_str());
      else if(obj.subtype()==ConfVector::V_INT)
	sval = QString::number(((std::vector<int>*)vecPtr)->at(j));
      else if(obj.subtype()==ConfVector::V_UINT)
	sval = QString::number(((std::vector<unsigned int>*)vecPtr)->at(j));
      else if(obj.subtype()==ConfVector::V_FLOAT)
	sval = QString::number(((std::vector<float>*)vecPtr)->at(j),'g');
      QTableWidgetItem *titem=new QTableWidgetItem(sval);
      table->setItem(j,0, titem);
    }
    break;}
  case ConfObj::MATRIX:{
    QSpinBox *sbr = (QSpinBox*)m_handles[obj.name()+"_sb1"];
    QSpinBox *sbc = (QSpinBox*)m_handles[obj.name()+"_sb2"];
    ConfTable *table = (ConfTable *) tmpWgt;
    table->clear();
    ConfMatrix &mapobj = (ConfMatrix&)obj;
    if(mapobj.subtype()==ConfMatrix::M_U16){
      ConfMask<unsigned short int> &mmask = *((ConfMask<unsigned short int> *)mapobj.m_value);
      unsigned int nCol = mmask.get().size();
      unsigned int nRow = mmask.get().front().size();
      table->setRowCount(nRow);
      sbr->setValue(nRow);
      table->setColumnCount(nCol);
      sbc->setValue(nCol);
      for(unsigned int j=0;j<nRow; j++){
	for(unsigned int k=0;k<nCol; k++){
	  QTableWidgetItem *titem = new QTableWidgetItem(QString::number(mmask.get(k,j)));
	  table->setItem(j,k,titem);
	}
      }
    } else if(mapobj.subtype()==ConfMatrix::M_U1){
      ConfMask<bool> &mmask = *((ConfMask<bool> *)mapobj.m_value);
      unsigned int nCol = mmask.get().size();
      unsigned int nRow = mmask.get().front().size();
      table->setRowCount(nRow);
      sbr->setValue(nRow);
      table->setColumnCount(nCol);
      sbc->setValue(nCol);
      for(unsigned int j=0;j<nRow; j++){
	for(unsigned int k=0;k<nCol; k++){
	  QTableWidgetItem *titem = new QTableWidgetItem(QString::number(mmask.get(k,j)));
	  table->setItem(j,k,titem);
	}
      }
    }
    break;}
  default:
    std::cerr << "WidgetConfObjMap::update : don't know how to process object " << obj.name() << std::endl;
    break;
  }
  tmpWgt->repaint();
}
QWidget* WidgetConfObjMap::createWidget(QWidget *parent, QBoxLayout *h_layout, PixLib::ConfObj &obj, int base){
  QWidget *tmpWgt=0;
  std::vector<QWidget*> extraWgt;
  switch(obj.type()){
  case PixLib::ConfObj::ALIAS: 
  case PixLib::ConfObj::LINK: {
    QFrame *frame = new QFrame(parent);
    QVBoxLayout *frameLayout = new QVBoxLayout( frame );
    tmpWgt = (QWidget*) new QLineEdit(frame);
    frameLayout->addWidget(tmpWgt);
    frame->setFrameShape( QFrame::Box );
    frame->setFrameShadow( QFrame::Plain );
    if (h_layout) h_layout->addWidget(frame);
    break;
  }
  case PixLib::ConfObj::STRING:{
    QFrame *frame = new QFrame(parent);
    QVBoxLayout *frameLayout = new QVBoxLayout( frame );
    tmpWgt = (QWidget*) new QLineEdit(frame);
    frameLayout->addWidget(tmpWgt);
    frame->setFrameShape( QFrame::Box );
    frame->setFrameShadow( QFrame::Sunken );
    if((int)obj.name().find("passw")!=(int)std::string::npos)
      ((QLineEdit*)tmpWgt)->setEchoMode(QLineEdit::Password);
    if (h_layout) h_layout->addWidget(frame);
    break;}
  case PixLib::ConfObj::BOOL:{
    tmpWgt = (QWidget*) new QCheckBox("On/Off",parent);
    if (h_layout) h_layout->addWidget(tmpWgt);
    break;}
  case PixLib::ConfObj::INT:{
    if (obj.subtype() == PixLib::ConfInt::U32 || obj.subtype() == PixLib::ConfInt::U16 ||
	  obj.subtype() == PixLib::ConfInt::U8) {
      tmpWgt = (QWidget*) new UnsignedSpinBox(parent);
      ((UnsignedSpinBox*)tmpWgt)->setBase(base);
      ((UnsignedSpinBox*)tmpWgt)->setUMaximum(UINT_MAX);
      ((UnsignedSpinBox*)tmpWgt)->setUMinimum(0);
    }
    else {
      tmpWgt = (QWidget*) new QSpinBox(parent);
      ((QSpinBox*)tmpWgt)->setMaximum(INT_MAX);
      ((QSpinBox*)tmpWgt)->setMinimum(-INT_MAX);
    }
    if (h_layout) h_layout->addWidget(tmpWgt);
    break;}
  case PixLib::ConfObj::FLOAT:{
    tmpWgt = (QWidget*) new QDoubleSpinBox(parent);
    ((QDoubleSpinBox*)tmpWgt)->setMaximum(9999999);
    ((QDoubleSpinBox*)tmpWgt)->setMinimum(-9999999);
    ((QDoubleSpinBox*)tmpWgt)->setValue(((PixLib::ConfFloat&)obj).value());
    if (h_layout) h_layout->addWidget(tmpWgt);
    break;}
  case PixLib::ConfObj::LIST:{
    QComboBox *cbhandle = new QComboBox(parent);
    tmpWgt = (QWidget*)cbhandle; 
    std::map<std::string, int>::const_iterator mapIT;
    for(mapIT = ((PixLib::ConfList&)obj).m_symbols.begin(); mapIT != ((PixLib::ConfList&)obj).m_symbols.end();mapIT++){
      cbhandle->addItem(mapIT->first.c_str());
    }
    if (h_layout) h_layout->addWidget(tmpWgt);
    break;}
  case PixLib::ConfObj::STRVECT:
  case PixLib::ConfObj::VECTOR:{
    QHBoxLayout *hlayout = new QHBoxLayout( 0 );
    QVBoxLayout *vlayout = new QVBoxLayout( 0 );
    ConfTable *table = new ConfTable( parent );
    tmpWgt = (QWidget*)table;
    hlayout->addWidget(table);
    QSpinBox *sb = new QSpinBox( parent );
    sb->setMaximum(99999);
    vlayout->addWidget(sb);
    QSpacerItem *spc = new QSpacerItem(5,5,QSizePolicy::Fixed,QSizePolicy::Expanding);
    vlayout->addSpacerItem(spc);
    hlayout->addLayout(vlayout);
    extraWgt.push_back((QWidget*)sb);
    if (h_layout) h_layout->addLayout(hlayout);
    break;}
  case PixLib::ConfObj::MATRIX:{
    QHBoxLayout *hlayout = new QHBoxLayout( 0 );
    QVBoxLayout *vlayout = new QVBoxLayout( 0 );
    ConfTable *table = new ConfTable( parent );
    tmpWgt = (QWidget*)table;
    hlayout->addWidget(table);
    QSpinBox *sbr = new QSpinBox( parent );
    sbr->setMaximum(99999);
    vlayout->addWidget(sbr);
    QSpinBox *sbc = new QSpinBox( parent );
    sbc->setMaximum(99999);
    vlayout->addWidget(sbc);
    QSpacerItem *spc = new QSpacerItem(5,5,QSizePolicy::Fixed,QSizePolicy::Expanding);
    vlayout->addSpacerItem(spc);
    hlayout->addLayout(vlayout);
    extraWgt.push_back((QWidget*)sbr);
    extraWgt.push_back((QWidget*)sbc);
    if (h_layout) h_layout->addLayout(hlayout);
    break;}
  default:
    tmpWgt = (QWidget*) new QLabel("unhandled PixLib::ConfObj type "+QString::number(obj.type()), parent);
    if (h_layout) h_layout->addWidget(tmpWgt);
  }
  connect(obj, tmpWgt, extraWgt);
  update(obj);
  return tmpWgt;
}
void WidgetConfObjMap::createLine(QWidget *parent, QBoxLayout *layout, PixLib::ConfObj &obj, int base){
  if(obj.visible() && obj.type() != PixLib::ConfObj::VOID) {
    QHBoxLayout *h_layout = new QHBoxLayout( );
    QString lname = obj.comment().c_str();//obj.name().c_str();

    if (obj.type() == PixLib::ConfObj::LINK) {
      lname += " (link)";
    }
    if (obj.type() == PixLib::ConfObj::ALIAS) {
      lname += " (alias)";
    }

    QLabel *label = new QLabel(lname);
    h_layout->addWidget(label);
    QSpacerItem *spacer = new QSpacerItem( 41, 21, QSizePolicy::Expanding, QSizePolicy::Minimum );
    h_layout->addItem( spacer );

//     QWidget *tmpWgt= 
    createWidget(parent, h_layout,obj,base);
//     if(tmpWgt!=0) {
//       h_layout->addWidget(tmpWgt);
//     }

    layout->addLayout(h_layout);

  }
}
void WidgetConfObjMap::connect(PixLib::ConfObj &obj, QWidget *widget){
  std::vector<QWidget*> extraWgt;
  connect(obj, widget, extraWgt);
}
void WidgetConfObjMap::connect(PixLib::ConfObj &obj, QWidget *widget, std::vector<QWidget*> extraWgt){
  switch(obj.type()){
  case PixLib::ConfObj::ALIAS: 
  case PixLib::ConfObj::LINK: 
  case PixLib::ConfObj::STRING:{
    QLineEdit *tmpWgt = dynamic_cast<QLineEdit *>(widget);
    assert (tmpWgt);
    break;}
  case PixLib::ConfObj::BOOL:{
    QCheckBox *tmpWgt = dynamic_cast<QCheckBox *>(widget);
    assert (tmpWgt);
    break;}
  case PixLib::ConfObj::INT:{
    if (obj.subtype() == PixLib::ConfInt::U32 || obj.subtype() == PixLib::ConfInt::U16 ||
	obj.subtype() == PixLib::ConfInt::U8) {
      UnsignedSpinBox *tmpWgt = dynamic_cast<UnsignedSpinBox *>(widget);
      assert (tmpWgt);
    } else {
      QSpinBox *tmpWgt = dynamic_cast<QSpinBox *>(widget);
      assert (tmpWgt);
    }
    break; }
  case PixLib::ConfObj::FLOAT:{
    QDoubleSpinBox *tmpWgt = dynamic_cast<QDoubleSpinBox *>(widget);
    assert (tmpWgt);
    break;}
  case PixLib::ConfObj::LIST:{
    QComboBox *tmpWgt = dynamic_cast<QComboBox *>(widget);
    assert (tmpWgt);
    break;
  }
  case PixLib::ConfObj::STRVECT:
  case PixLib::ConfObj::VECTOR:{
    ConfTable *tmpWgt = dynamic_cast<ConfTable*>(widget);
    assert (tmpWgt);
    QSpinBox *sb = (extraWgt.size()>0)?dynamic_cast<QSpinBox*>(extraWgt.at(0)):0;
    assert (sb);
    sb->setObjectName((obj.name()+"_sb1").c_str());
    m_handles.insert(std::make_pair((obj.name()+"_sb1").c_str(), sb));
    QObject::connect(sb, SIGNAL(valueChanged(int)), tmpWgt, SLOT(setTableSize(int)));
    break;}
  case PixLib::ConfObj::MATRIX:{
    ConfTable *tmpWgt = dynamic_cast<ConfTable*>(widget);
    assert (tmpWgt);
    QSpinBox *sbr = (extraWgt.size()>0)?dynamic_cast<QSpinBox*>(extraWgt.at(0)):0;
    QSpinBox *sbc = (extraWgt.size()>1)?dynamic_cast<QSpinBox*>(extraWgt.at(1)):0;
    assert (sbr);
    assert (sbc);
    sbr->setObjectName((obj.name()+"_sb1").c_str());
    sbc->setObjectName((obj.name()+"_sb2").c_str());
    m_handles.insert(std::make_pair((obj.name()+"_sb1").c_str(),sbr));
    m_handles.insert(std::make_pair((obj.name()+"_sb2").c_str(),sbc));
    QObject::connect(sbr, SIGNAL(valueChanged(int)), tmpWgt, SLOT(setTableSize(int)));
    QObject::connect(sbc, SIGNAL(valueChanged(int)), tmpWgt, SLOT(setTableSize(int)));
    break;}
  default:
    break;
  }
  m_handles.insert(std::make_pair(obj.name().c_str(),widget));
  update(obj);
}
ConfTable::ConfTable(QWidget * parent) : QTableWidget(parent) {}
ConfTable::ConfTable(int rows, int columns, QWidget * parent) : QTableWidget(rows, columns, parent) {}
void ConfTable::setTableSize(int n){
  QSpinBox *sb = dynamic_cast<QSpinBox*>(sender());
  if(sb!=0 && (sb->objectName().right(4)=="_sb1" || sb->objectName().right(4)=="_sb2")){
    if(sb->objectName().right(4)=="_sb1"){
      int oldNrow = rowCount();
      for(int i=n;i<oldNrow;i++)
	removeRow(i);
      for(int i=oldNrow;i<n;i++){
	insertRow(i);
	setItem(i,0, new QTableWidgetItem(QString("0")));
      }
    }
    if(sb->objectName().right(4)=="_sb2"){
      int oldNcol = columnCount();
      for(int i=n;i<oldNcol;i++)
	removeColumn(i);
      for(int i=oldNcol;i<n;i++){
	insertColumn(i);
	setItem(i,0, new QTableWidgetItem(QString("0")));
      }
    }
  }
}
