#ifndef _SelectPanel_h_
#define _SelectPanel_h_

#include <vector>
#include <map>
#include <string>
#include <QDialog>

class QWidget;
class QComboBox;

class SelectPanel : public QDialog {
  Q_OBJECT

 public:

  SelectPanel(QWidget *parent, std::vector<std::string> labels, std::vector< std::vector<std::string> > boxItems);
  SelectPanel(QWidget *parent, std::vector<std::string> labels, std::vector<std::string> firstBoxItems,
	      std::map<std::string, std::vector< std::vector<std::string> > > otherBoxItems);
  ~SelectPanel(){};

  void getSelection(std::vector<std::string>&);

 public slots:
  void setOtherBoxes(int);

 private:
  std::vector<QComboBox*> boxes;
  std::map<std::string, std::vector< std::vector<std::string> > > m_otherBoxItems;
  void setupUi(std::vector<std::string>);

};

#endif // _SelectPanel_h_
