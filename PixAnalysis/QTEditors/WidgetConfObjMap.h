#ifndef _WidgetConfObjMap_h_
#define _WidgetConfObjMap_h_

#include <map>
#include <string>
#include <QWidget>
#include <QTableWidget>

namespace PixLib {
  class ConfObj;
  class Config;
}
class QBoxLayout;

class ConfTable : public QTableWidget {
  Q_OBJECT
 public:
  ConfTable(QWidget * parent = 0);
  ConfTable(int rows, int columns, QWidget * parent = 0);
  ~ConfTable(){};

 public slots:
  void setTableSize(int n);

};


/** Class to create widgets for given config objects. 
 * copied from OptionsPanel.
 */
class WidgetConfObjMap
{
public:
  WidgetConfObjMap(PixLib::Config &config);

  /** constructor to make STL happy.
   * Since the WidgetConfObjMap contains a reference to the config object 
   * which will be modified by save, "const WidgetConfObjMap" for the
   * argument cannot be achieved.
   */
  WidgetConfObjMap(const WidgetConfObjMap &b);

  ~WidgetConfObjMap();

  WidgetConfObjMap& operator=(const WidgetConfObjMap &b);

  /** Copy  widgets values back to config class.
   */
  void save() {save(*m_config);};

  /** Copy  widgets values to a config class.
   */
  void save(PixLib::Config &dest);

  /** Update widgets from the given config.
   */
  void update(PixLib::Config &a_config);
  void update(PixLib::ConfObj &obj);
  void update(){update(*m_config);};

  /** Create a widget for the given conf object.
   * @param h_layout can be set to NULL.
   * The conf object must be part of the config class which was given upon 
   * construction.
   * The method does not protect agains objects of  type ConfObj::VOID.
   */
  QWidget *createWidget(QWidget *parent, QBoxLayout *h_layout, PixLib::ConfObj &obj, int base=10);

  QWidget *createWidget(QWidget *parent, PixLib::ConfObj &obj, int base=10) {
    return createWidget(parent,NULL,obj,base);
  }

  /** Create a widget for the given object together with a label.
   * Objects of type ConfObj::VOID will be ignored.
   */
  void createLine(QWidget *parent, QBoxLayout *layout, PixLib::ConfObj &obj, int base=10);

  /** Get a reference to the config class.
   */
  PixLib::Config &config() {return *m_config;}

  /** Connect an existing widget with a config object.
   * The methods asserts (using assert) that the widget type exactly matches
   * the expected type for the given ConfObj. If the widget and the type
   * does not match the program will be aborted. 
   */
  void connect(PixLib::ConfObj &obj, QWidget *widget);
  void connect(PixLib::ConfObj &obj, QWidget *widget, std::vector<QWidget*> extraWgt);
  /** Get a reference to the config class (read only).
   */
  const PixLib::Config &config() const {return *m_config;}

private:
  std::map<std::string,QWidget*> m_handles;
  PixLib::Config *m_config;

};

#endif
