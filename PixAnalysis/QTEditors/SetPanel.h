#ifndef SETPANEL_H
#define SETPANEL_H

#include <QDialog>

class QLineEdit;
class QPushButton;
class QLabel;
class QSpinBox;

class SetPanel : public QDialog {

 Q_OBJECT

   enum PanelType{intval, txtval};

public:
 SetPanel(QWidget* parent = 0, PanelType type=SetPanel::intval, QString labelTxt="Fill in here");
 SetPanel(QWidget* parent = 0, QString labelTxt="Fill in here", QString editTxt="new text");
 SetPanel(QWidget* parent = 0, QString labelTxt="Fill in here", int editVal=0);
  ~SetPanel(){};

  QString getText();
  int getInt();
  void setIntLimits(int min, int max);
  
 private:
  void setupUi(PanelType type, QString labelTxt, QString valTxt, int valInt);
  void retranslateUi(QString labelTxt);

  QPushButton *OKButton;
  QPushButton *CancelButton;
  QSpinBox *SpinBox;
  QLineEdit *LineEdit;
  QLabel *TextLabel;

};
#endif // SETPANEL_H
