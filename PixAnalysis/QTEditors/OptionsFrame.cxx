#include "OptionsFrame.h"

#include <Config/Config.h>

#include <QLabel>
#include <QScrollArea>
#include <QGroupBox>
#include <QVBoxLayout>

using namespace PixLib;

optionsFrame::optionsFrame( PixLib::Config &in_cfg, QWidget* parent, bool subGrps, const std::string *only_this_conf_grp,
			    const std::vector<std::string> *exclude_pattern) 
  : QFrame (parent), 
    m_config(in_cfg)
{

  QVBoxLayout *BaseLayout = new QVBoxLayout( this );
  
  m_widgetMap.push_back(WidgetConfObjMap( m_config ));// remember main cfg.
  WidgetConfObjMap &objMap = m_widgetMap.at(0);
  m_tabwidget=0;
  if (only_this_conf_grp && !only_this_conf_grp->empty()) {
    QFrame *a_frame = new QFrame( this );
    unsigned int grp_i=0;
    for (; grp_i<(unsigned int)m_config.size(); grp_i++) {
      ConfGroup &grp = m_config[grp_i];
      // debug:
      //	std::cout << grp.name() << " =?= " << *only_this_conf_grp << std::endl;
      if (grp.name() == *only_this_conf_grp) {
	bool empty =  fillTab(a_frame, grp, exclude_pattern, objMap);
	if (empty) grp_i = m_config.size();
	break;
      }
    }
    if (grp_i>=(unsigned int)m_config.size()) {
      QLabel * a_label= new QLabel( a_frame );
      a_label->setText( "<p align=\"center\">No options for this phase </p>" );
      
      QVBoxLayout *frame_layout = new QVBoxLayout( a_frame );
      frame_layout->addWidget(a_label);
    }
    BaseLayout->addWidget(a_frame);
  }
  else {
    m_tabwidget = new QTabWidget( this );
    BaseLayout->addWidget(m_tabwidget);
    for (unsigned int i=0; i<(unsigned int)m_config.size(); i++) {
      ConfGroup &grp = m_config[i];
      QWidget *TabPage = new QWidget( m_tabwidget );
      fillTab(TabPage, grp, exclude_pattern, objMap);
      m_tabwidget->addTab( TabPage,  grp.name().c_str());
    }
  }

  if(subGrps)
    loadSubConf(m_config,exclude_pattern);
  
  resize( QSize(450, 250).expandedTo(minimumSizeHint()) );

  for(std::vector<WidgetConfObjMap>::iterator it=m_widgetMap.begin(); it!=m_widgetMap.end(); it++)
    it->update();

}
optionsFrame::~optionsFrame(){
  m_widgetMap.clear();
}

void optionsFrame::loadSubConf(PixLib::Config &inConf, const std::vector<std::string> *exclude_pattern)
{
  assert (m_tabwidget);
  for(unsigned int j=0;j<inConf.subConfigSize();j++){
    m_widgetMap.push_back(WidgetConfObjMap( inConf.subConfig(j) ));// remember main cfg.
    WidgetConfObjMap &objMap = m_widgetMap.at(m_widgetMap.size()-1);
    for (unsigned int i=0; i<(unsigned int)(inConf.subConfig(j)).size(); i++) {
      ConfGroup &grp = (inConf.subConfig(j))[i];
      QWidget *TabPage = new QWidget( m_tabwidget );
      fillTab(TabPage, grp, exclude_pattern, objMap);
      m_tabwidget->addTab( TabPage,(inConf.subConfig(j).name()+"-"+grp.name()).c_str());
    }
    loadSubConf(inConf.subConfig(j),exclude_pattern);
  }
}



bool optionsFrame::fillTab(QWidget *tab, PixLib::ConfGroup &cgrp, const std::vector<std::string> *exclude_pattern, 
			   WidgetConfObjMap &objMap){
  std::string::size_type name_pos=cgrp.name().size()+1;
  if (cgrp.size()>10) {
    unsigned int count=0;
    if (exclude_pattern) {
      for (size_t i=0; i<cgrp.size(); i++) {
	ConfObj &obj = cgrp[i];
	
	if(obj.visible() && obj.type() != PixLib::ConfObj::VOID){
	  std::string obj_name(obj.name(),name_pos);
	  bool exclude=false;
	  for (std::vector<std::string>::const_iterator pattern_iter=exclude_pattern->begin();
	       pattern_iter!=exclude_pattern->end();
	       pattern_iter++) {
	    if (obj_name.find(*pattern_iter)==0) {
	      exclude=true;
	      break;
	    }
	  }
	  if (!exclude) {
	    count++;
	  }
	}
      }
    }
    else {
      count = cgrp.size();
    }
    if (count> 10) {
      return fillScrollView(tab,cgrp,exclude_pattern, objMap);
    }
  }
  QVBoxLayout *layout1 = new QVBoxLayout( tab );
  QSpacerItem *spacer_top = new QSpacerItem( 0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding );
  layout1->addItem( spacer_top );

  bool empty=true;
  for (size_t i=0; i<cgrp.size(); i++) {
    ConfObj &obj = cgrp[i];
    
    if(obj.visible() && obj.type() != PixLib::ConfObj::VOID){
      bool exclude=false;
      if (exclude_pattern) {
	std::string obj_name(obj.name(),name_pos);
	for (std::vector<std::string>::const_iterator pattern_iter=exclude_pattern->begin();
	     pattern_iter!=exclude_pattern->end();
	     pattern_iter++) {
//debug:
//	  std::cout << obj_name << "=?=" << *pattern_iter << std::endl;
	  if (obj_name.find(*pattern_iter)==0) {
	    exclude=true;
	    break;
	  }
	}
      }
      if (!exclude) {
	int base = 10;
 	if (obj.type() == PixLib::ConfObj::INT){
 	  std::string obj_name(obj.name(),name_pos);
 	  if(obj_name.find("linkMap") != std::string::npos) base=16;
 	}
	objMap.createLine(tab,layout1,obj,base);
	empty=false;
      }
    }
  }
  QSpacerItem *spacer_bottom = new QSpacerItem( 0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding );
  layout1->addItem( spacer_bottom );

  return empty;
}

bool optionsFrame::fillScrollView(QWidget *tab, PixLib::ConfGroup &cgrp, const std::vector<std::string> *exclude_pattern, 
			   WidgetConfObjMap &objMap){

  QScrollArea* sv = new QScrollArea(tab);//,(std::string("view_")+cgrp.name()).c_str(),0);
  //sv->setWidgetResizable(true);//setResizePolicy(QScrollArea::AutoOneFit);

  QVBoxLayout* sv_layout=new QVBoxLayout(tab);
  sv_layout->addWidget(sv);

  QGroupBox *config_group= new QGroupBox( sv->viewport() );
  QHBoxLayout *config_group_layout = new QHBoxLayout( config_group );
  config_group_layout->setAlignment( Qt::AlignTop );
  //config_group->setColumnLayout(0, Qt::Vertical );
  config_group_layout->setSpacing( 4 );
  config_group_layout->setMargin( 11 );
  //config_group->setFrameShape(QGroupBox::NoFrame);
  config_group->setTitle("");

  QHBoxLayout *column_layout= new QHBoxLayout( 0 );
  QVBoxLayout *label_layout = new QVBoxLayout( 0 );
  QVBoxLayout *object_layout = new QVBoxLayout( 0 );


  std::string::size_type name_pos=cgrp.name().size()+1;
  bool empty=true;
  for (size_t i=0; i<cgrp.size(); i++) {
    ConfObj &obj = cgrp[i];

    if(obj.visible() && obj.type() != PixLib::ConfObj::VOID){
      bool exclude=false;
      if (exclude_pattern) {
	std::string obj_name(obj.name(),name_pos);
	for (std::vector<std::string>::const_iterator pattern_iter=exclude_pattern->begin();
	     pattern_iter!=exclude_pattern->end();
	     pattern_iter++) {
//debug:
//	  std::cout << obj_name << "=?=" << *pattern_iter << std::endl;
	  if (obj_name.find(*pattern_iter)==0) {
	    exclude=true;
	    break;
	  }
	}
      }
      if (!exclude) {
	int base = 10;
 	if (obj.type() == PixLib::ConfObj::INT){
 	  std::string obj_name(obj.name(),name_pos);
 	  if(obj_name.find("linkMap") != std::string::npos) base=16;
 	}

	QString lname = obj.comment().c_str();//obj.name().c_str();

	if (obj.type() == PixLib::ConfObj::LINK) {
	  lname += " (link)";
	}
	if (obj.type() == PixLib::ConfObj::ALIAS) {
	  lname += " (alias)";
	}

	QLabel *label = new QLabel(lname,config_group);
	label_layout->addWidget(label);

	objMap.createWidget(config_group,object_layout,obj,base);

	empty=false;
      }
    }
  }
  column_layout->addLayout(label_layout);
  QSpacerItem *sep = new QSpacerItem( 20, 20, QSizePolicy::MinimumExpanding, QSizePolicy::Minimum );
  column_layout->addItem( sep );
  column_layout->addLayout(object_layout);
  config_group_layout->addLayout(column_layout);
  sv->setWidget(config_group);
  sv->viewport()->adjustSize();

  return empty;
}

void optionsFrame::save(){
  // save to cfg
  //std::cout << "Save options : " << std::endl;
  for(std::vector<WidgetConfObjMap>::iterator it=m_widgetMap.begin(); it!=m_widgetMap.end(); it++)
    it->save();
  // m_config.dump(std::cout);
}
