#ifndef CFGMAP_H
#define CFGMAP_H

#include <QDialog>
#include <QLabel>
#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QDialog>
#include <QFrame>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QPushButton>
#include <QSpacerItem>
#include <QSpinBox>
#include <QVBoxLayout>

class QMouseEvent;
class QImage;
class QString;

class CfgPixMap : public QLabel
{
 Q_OBJECT

 public:
  CfgPixMap( QWidget * parent = 0);
  ~CfgPixMap(){};

 public slots:
  void mouseDoubleClickEvent (QMouseEvent *);
  void mouseReleaseEvent (QMouseEvent *);

 signals:
  void singleMapClick(uint x, uint y);
  void doubleMapClick(uint x, uint y);
};

class CfgMap : public QDialog
{

 Q_OBJECT

 public:
  CfgMap(uint **data, uint maxval, uint ncol, uint nrow, bool flipped=false, QWidget* parent = 0, Qt::WindowFlags fl = 0 , 
	 const char *gname=0, const char *mname=0, int chip=0, uint sx=20, uint sy=4);
  ~CfgMap();

  void setFei4Style();
  uint **m_extData;

 public slots:
  void singleMapClicked(uint x, uint y);
  void readPixVal();
  void setPixVal();
  void setROI();
  void setColVal();
  void setRowVal();
  void setGangVal();
  void setIntgdVal();
  void setLongVal();
  void setAllVal();
  void save();
  uint nRow(){return m_nrow;};
  uint nCol(){return m_ncol;};

 private:

  void mapValueToPixelColor(unsigned int pX, unsigned int pY, double pValue);	//translates a value to a pixel color
  void setupUi();
  void retranslateUi();

  uint **m_data, m_maxval, m_ncol, m_nrow, m_scaleX, m_scaleY;
  int m_chip;
  QString m_gname, m_mname;
  QImage *m_img;
  bool m_flipped;

  QHBoxLayout *horizontalLayout_15;
  QVBoxLayout *verticalLayout_8;
  QHBoxLayout *horizontalLayout_14;
  QVBoxLayout *verticalLayout_3;
  QVBoxLayout *verticalLayout;
  QHBoxLayout *horizontalLayout;
  QLabel *pixelLabel;
  QSpacerItem *horizontalSpacer_2;
  QLabel *pixelVal;
  QHBoxLayout *horizontalLayout_12;
  QLabel *colLabel;
  QSpacerItem *horizontalSpacer_6;
  QSpinBox *colVal;
  QHBoxLayout *horizontalLayout_13;
  QLabel *rowLabel;
  QSpacerItem *horizontalSpacer_13;
  QSpinBox *rowVal;
  QSpacerItem *verticalSpacer;
  QFrame *line;
  QVBoxLayout *verticalLayout_7;
  QVBoxLayout *verticalLayout_2;
  QHBoxLayout *horizontalLayout_3;
  QPushButton *SetButton;
  QSpacerItem *horizontalSpacer_5;
  QLabel *label;
  QSpinBox *pixelSetVal;
  QHBoxLayout *horizontalLayout_6;
  QPushButton *setColButton;
  QSpacerItem *horizontalSpacer_7;
  QPushButton *setRowButton;
  QHBoxLayout *horizontalLayout_7;
  QSpacerItem *horizontalSpacer_15;
  QPushButton *setGangedButton;
  QPushButton *setIntGangedButton;
  QSpacerItem *horizontalSpacer_16;
  QPushButton *setLongButton;
  QSpacerItem *horizontalSpacer_8;
  QHBoxLayout *horizontalLayout_10;
  QPushButton *setAllButton;
  QSpacerItem *horizontalSpacer_11;
  QSpacerItem *verticalSpacer_4;
  QVBoxLayout *verticalLayout_6;
  QHBoxLayout *horizontalLayout_5;
  QPushButton *SetROIButton;
  QSpacerItem *horizontalSpacer_4;
  QHBoxLayout *horizontalLayout_4;
  QVBoxLayout *verticalLayout_4;
  QLabel *pixelLabel_2;
  QLabel *colLabel_2;
  QLabel *rowLabel_2;
  QLabel *rowLabel_3;
  QSpacerItem *horizontalSpacer_3;
  QVBoxLayout *verticalLayout_5;
  QSpinBox *colLimitLow;
  QSpinBox *rowLimitLow;
  QSpinBox *colLimitHigh;
  QSpinBox *rowLimitHigh;
  QSpacerItem *verticalSpacer_3;
  QVBoxLayout *mapVerticalLayout;
  QLabel *TheMap;
  QSpacerItem *verticalSpacer_2;
  QHBoxLayout *horizontalLayout_2;
  QSpacerItem *horizontalSpacer;
  QPushButton *AcceptButton;
  QSpacerItem *spacer24;
  QPushButton *CancelButton;

};
#endif // CFGMAP_H
