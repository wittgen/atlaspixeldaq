/**
 * @file UnsignedSpinBox.h
 * @brief adopt uspinbox.h from qt3 addons for qt4
 * @author Tayfun Ince <tayfun.ince@cern.ch>
 * @date 2014/11/10
 */

#ifndef _UnsignedSpinBox_h_
#define _UnsignedSpinBox_h_

#include <QSpinBox>
#include <QString>

class UnsignedSpinBox : public QSpinBox
{
  Q_OBJECT

 public:
  UnsignedSpinBox(QWidget* parent = 0);
  virtual ~UnsignedSpinBox();

  void setBase(int base);
  int base() const;

  inline int UtoI(unsigned long U) const {return U - 2147483648;}
  inline unsigned long ItoU(int I) const {return I + 2147483648;}

  inline unsigned long UMinimum() const {return ItoU(minimum());}
  inline unsigned long UMaximum() const {return ItoU(maximum());}

  void setUMinimum(unsigned long umin);
  void setUMaximum(unsigned long umax);

  unsigned long UValue();
  virtual void setUValue(unsigned long);

 protected:
  int valueFromText(const QString& text) const;
  QString textFromValue(int value) const;
  QValidator::State validate(QString &input, int &pos) const;

 private:
  int m_base;
  unsigned long m_value;
};

#endif
