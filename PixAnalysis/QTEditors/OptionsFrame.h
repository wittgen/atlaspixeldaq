#ifndef OPTIONSFRAME_H
#define OPTIONSFRAME_H

#include <QFrame>
#include <vector>
#include <string>
#include "QTEditors/WidgetConfObjMap.h"

class QTabWidget;
class QWidget;
namespace PixLib{
 class Config;
 class ConfGroup;
}

class optionsFrame : public QFrame {

  Q_OBJECT

 public:
  optionsFrame( PixLib::Config &in_cfg, QWidget* parent = 0, bool subGrps = false, 
		const std::string *only_this_conf_grp=0, const std::vector<std::string> *exclude_pattern=0);
  ~optionsFrame();
  void loadSubConf(PixLib::Config &inConf, const std::vector<std::string> *exclude_pattern); 
  bool fillTab(QWidget *, PixLib::ConfGroup &, const std::vector<std::string> *exclude_pattern, WidgetConfObjMap &objMap);
  bool fillScrollView(QWidget *tab, PixLib::ConfGroup &cgrp, const std::vector<std::string> *exclude_pattern, WidgetConfObjMap &objMap);



  QTabWidget *getTabWidget(){return m_tabwidget;}

 public slots:
  void save();

 private:
  QTabWidget *m_tabwidget;
  PixLib::Config &m_config;

  std::vector<WidgetConfObjMap> m_widgetMap;
};

#endif //  OPTIONSFRAME_H
