#ifndef _VisualisationList_h_
#define _VisualisationList_h_

#include "IVisualiser.h"
#include <DataContainer/HistoInfo_t.h>

#include <cassert>

namespace PixA {

  /** Provide a list of simple visualsisers
   */
  class VisualisationList : public IVisualiser
  {
  public:
    VisualisationList(IVisualiser &a)  {
      push_back(a);
    } 

    ~VisualisationList() { for(std::vector<std::pair<IVisualiser *,int> >::iterator vis_iter = m_visualiserList.begin();
			       vis_iter!=m_visualiserList.end();
			       vis_iter++) { if (vis_iter->second==0) { delete vis_iter->first;} vis_iter->first = NULL; } }

    /** Get the name of the visualiser.
     */
    const std::string &name() const {
      assert (!m_visualiserList.empty());
      return (m_visualiserList.begin()->first)->name();
    }

    /** Get the number of options or zero.
     * On option is a variatio of the default visualisation.
     * A visualiser may or may not offer options.
     */
    unsigned int nOptions() const { return m_visualiserList.size(); }

    /** Get the name of visualisation option.
     * @param option the index of the option (must be smaller than the value returned by @nOptions.).
     * @return the name of the option.
     */
    const std::string &optionName(unsigned int option) const {
      assert (option < m_visualiserList.size());
      return m_visualiserList[option].first->optionName(m_visualiserList[option].second);
    }

    /** Returns true, if the visualiser can optionally display only one front-end.
     */
    bool canVisualiseOneFrontEnd() const {
      for(std::vector<std::pair<IVisualiser *, int> >::const_iterator vis_iter = m_visualiserList.begin();
	  vis_iter!=m_visualiserList.end();
	  vis_iter++) {
	if ((vis_iter->first)->canVisualiseOneFrontEnd()) {
	  return true;
	}
      }
      return false;
    }

    /** Returns the maximum number of modules the visualiser can display simultaneously.
     */
    unsigned int canVisualiseNModules() const {
      unsigned int can_visualise_n_modules=static_cast<unsigned int>(-1);
      for(std::vector<std::pair<IVisualiser *, int> >::const_iterator vis_iter = m_visualiserList.begin();
	  vis_iter!=m_visualiserList.end();
	  vis_iter++) {
	if ((vis_iter->first)->canVisualiseNModules()< can_visualise_n_modules ) {
	  can_visualise_n_modules = (vis_iter->first)->canVisualiseNModules();
	}
      }
      if (can_visualise_n_modules==static_cast<unsigned int>(-1)) {
	can_visualise_n_modules=0;
      }
      return can_visualise_n_modules;
    }

    /** Fill the histogram info structure.
     * The structure contains the index ranges for each dimension.
     * @sa HistIno_t
     */
    void fillIndexArray(IScanDataProvider &data_provider, const std::vector<std::string> &module_names, HistoInfo_t &info) const {
      info.reset();
      for(std::vector< std::pair<IVisualiser *, int> >::const_iterator vis_iter = m_visualiserList.begin();
	  vis_iter!=m_visualiserList.end();
	  vis_iter++) {
	HistoInfo_t a_info;
	(vis_iter->first)->fillIndexArray(data_provider, module_names, a_info);
	info.makeUnion(a_info);
      }
    }

    /** Perform the default visualisation.
     */
    void visualise(IScanDataProvider &data_provider, const std::vector<std::string> &module_names, const Index_t &index, int front_end) {
      assert (!m_visualiserList.empty());
      (m_visualiserList.begin()->first)->visualise(data_provider, module_names,m_visualiserList.begin()->second, index, front_end);
    }

    /** Perform the chosen visualisation option. 
     */
    void visualise(IScanDataProvider &data_provider, const std::vector<std::string> &module_names, int option, const Index_t &index, int front_end) {
      assert ( static_cast<unsigned int>(option) < m_visualiserList.size());
      m_visualiserList[option].first->visualise(data_provider, module_names, m_visualiserList[option].second, index,front_end);
    }

    void push_back(IVisualiser &a) {
      for (unsigned int option_i=0; option_i<a.nOptions(); option_i++) {
	m_visualiserList.push_back(std::make_pair<IVisualiser *, int>(&a, static_cast<int>(option_i)) );
      }
    }

private:
  std::vector< std::pair<IVisualiser *, int> > m_visualiserList;
};

}
#endif
  
