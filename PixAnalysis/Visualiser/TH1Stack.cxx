#include <TH1Stack.h>

TH1Stack::TH1Stack(const TH1Stack &a)
  : TH1F(a)
{
  for (std::vector<TH1 *>::const_iterator h1_iter = a.m_histoList.begin();
       h1_iter != a.m_histoList.end();
       h1_iter++) {
    m_histoList.push_back(static_cast<TH1 *>((*h1_iter)->Clone()));
  }
}

TH1Stack::~TH1Stack()
{
  clear();
}

void TH1Stack::clear()
{
  for (std::vector<TH1 *>::const_iterator h1_iter = m_histoList.begin();
       h1_iter != m_histoList.end();
       h1_iter++) {
    delete *h1_iter;
  }
  // histograms are hopefully deleted by root
  m_histoList.clear();
}


void TH1Stack::Copy(TObject *dest) const
{
  if (!dest->InheritsFrom(TH1F::Class())) return;
  TH1F::Copy(*dest);

  if (!dest->InheritsFrom(TH1Stack::Class())) return;

  TH1Stack *dest_stack(static_cast<TH1Stack *>(dest));
  dest_stack->m_histoList.clear();

  for (std::vector<TH1 *>::const_iterator h1_iter = m_histoList.begin();
       h1_iter != m_histoList.end();
       h1_iter++) {
    dest_stack->m_histoList.push_back(static_cast<TH1 *>((*h1_iter)->Clone()));
  }
}

TH1 *TH1Stack::NewH1(const char *name, const char *title)
{
  TH1 *h1=new TH1F(name, title, GetNbinsX(), GetXaxis()->GetXmin(), GetXaxis()->GetXmax());
  h1->SetDirectory(0);
  h1->SetBit(kCanDelete);
  m_histoList.push_back(h1);
  return h1;
}

void TH1Stack::DeleteBottomH1() {
  if (m_histoList.empty()) return;
  delete m_histoList.back();
  m_histoList.pop_back();
}

void  TH1Stack::Draw(Option_t* option)
{

  if (m_histoList.empty()) return;

  TH1F::Draw(option);

  std::vector<TH1 *>::const_iterator h1_iter = m_histoList.begin();
  if (h1_iter != m_histoList.end()) {
    std::string option_string(option);
    if (option_string.find("same") == std::string::npos && option_string.find("SAME") == std::string::npos) {
      option_string += "same";
    }
    for (;
	 h1_iter != m_histoList.end();
	 h1_iter++) {
      (*h1_iter)->Draw(option_string.c_str());
    }
  }
  m_histoList.clear();
}
