#include "SummaryHistoVisualiser.h"
#include <DataContainer/HistoUtil.h>
#include <TCanvas.h>
#include <TROOT.h>
#include <TH2.h>
#include <TH1.h>
#include <memory>
#include <Histo/Histo.h>

#include "VisualisationFactory.h"


namespace PixA {

  std::string SummaryHistoVisualiser::s_name("Show Histograms");

  void SummaryHistoVisualiser::fillIndexArray(HistInfo_t &info) const {
    HistoRef histo(m_histo.ref());
    histo.numberOfHistos(m_histoName,info);
  }

  void SummaryHistoVisualiser::visualise(int , int index[4], int ) 
  {
    HistoRef histo(m_histo.ref());
    std::shared_ptr<PixLib::Histo> a_histo ( histo.histo(m_histoName,index));
    if (a_histo.get()) {
      PixLib::SummaryHisto summary;
      summary.readHisto(a_histo.get());
      summary->dump(std::cout);
    }
  }

  SummaryHistoVisualisationKit::SummaryHistoVisualisationKit(bool can_be_option)  {
    VisualisationFactory::registerKit("/HistoSummary$",&a_SummaryHistoVisualisationKit_instance);
  }

  IVisualiser *SummaryHistoVisualisationKit::visualiser(const std::string &histo_name, const std::vector<std::string> &/*folder_hierarchy*/,  
						       const PixA::HistoHandle &histo) const {
    return new SummaryHistoVisualiser(histo_name,histo);
  }
  
  SummaryHistoVisualisationKit a_SummaryHistoVisualisationKit_instance;

}
