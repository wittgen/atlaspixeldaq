#include "SCurvePixelVisualiser.h"
#include <DataContainer/HistoUtil.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <Histo/Histo.h>
#include <DataContainer/GenericHistogramAccess.h>
#include <ConfigWrapper/ConfObjUtil.h>

#include "VisualisationFactory.h"


namespace PixA {

  std::string SCurvePixelVisualiser::s_name("Show S-Curve");

  void SCurvePixelVisualiser::fillIndexArray(IScanDataProvider &data_provider,
					const std::vector<std::string> &module_names,
					HistoInfo_t &info) const {
    assert(module_names.size()==1);


    data_provider.numberOfHistos(module_names[0], m_histoName,info);

    m_histoInfo = info; // create a local copy
    Index_t local_index;
    local_index.reserve(4);
    for (unsigned int i=0; i<4; i++) {
      local_index.push_back(0);
    }

    // get histo and set the pixel dimensions accordingly... 
    const Histo *a_histo ( data_provider.histogram( module_names[0], m_histoName,local_index));
    if (!a_histo) std::cout << "ERROR [SCurvePixelVisualiser::fillIndexArray] histogram could not be retrieved   check the indizes" << std::endl;
    
    unsigned int n_columns = PixA::nColumns(a_histo);
    unsigned int n_rows = PixA::nRows(a_histo);


    // modify the original index (to be seen in the spin boxes) 
    // this is then used to be able to loop through the pixels
    info.setMaxIndex(0, n_columns);
    info.setMaxIndex(1, n_rows);
    // create a local copy
    m_histoInfo.setMaxIndex(0, n_columns);
    m_histoInfo.setMaxIndex(1, n_rows);

  }

  void SCurvePixelVisualiser::visualise(IScanDataProvider &data_provider, const std::vector<std::string> &module_names, int /*option*/, const Index_t &histo_index, int front_end )
  {
    assert( module_names.size()==1 );

    std::vector<float> vcal_arr;
    std::vector<float> occ_arr;
    vcal_arr.reserve(m_histoInfo.maxIndex(0));
    occ_arr.reserve(m_histoInfo.maxIndex(0));

     // @todo  generalize for other loop positions  (for example TOT calibration)
    unsigned int the_vcal_loop_index=0;    // assume that vcal ist the first  loop variable
    PixA::ConfigHandle scan_config_handle = data_provider.scanConfig();

    if (!scan_config_handle) return; 
    PixA::ConfigRef pix_scan_config = scan_config_handle.ref();
    const PixA::ConfGrpRef &loop_config(pix_scan_config["loops"]);

    try {
      std::stringstream loop_idx;
      loop_idx << the_vcal_loop_index;
      
        
      // check if the vcal loop has uniform step size 
      bool uniform = confVal<bool>(loop_config[std::string("loopVarUniformLoop_")+loop_idx.str()]);
      // fill vcal array
      if (uniform)  {
	int   steps   =confVal<int>(loop_config[std::string("loopVarNStepsLoop_")+loop_idx.str()]);
	float min_val = confVal<float>(loop_config[std::string("loopVarMinLoop_")+loop_idx.str()]);
	float max_val = confVal<float>(loop_config[std::string("loopVarMaxLoop_")+loop_idx.str()]);
	float bin_width=max_val-min_val;

	if (steps>1) {
	  bin_width /= (steps-1);
	}
	else {
	  if (min_val==max_val) bin_width=0.5 ;
	}
	float x=min_val;
	for (int step_i=0; step_i<steps; step_i++) {
	  vcal_arr.push_back(x);
	  x += bin_width;
	}
      }
      else {
	vcal_arr =  confVal<std::vector<float> >(loop_config[std::string("loopVarValuesLoop_")+loop_idx.str()]);
      }

      PixA::ConfGrpRef general_config( pix_scan_config["general"] );
    }
    catch (PixA::ConfigException &) {
      return;
    }

    {
      // eventually m_histoInfo will not be set
      // maybe rather use vcal_arr.size() which should be equal to the number of loop steps
      Index_t local_index;
      local_index.reserve(histo_index.size());


      //  decide which index to use for looping through pixels: 
      //  if loops 0 and 1 are abused don't take these for the local_index which is used for getting the histograms
      //  or take the occupancy loop index for looping 
      for (unsigned int i=0; i<histo_index.size(); i++) {
	//	local_index.push_back(histo_index[i]);
	local_index.push_back(0);
      }

 
      // try to guess the loop index which corresponds to the vcal loop
      // @todo should use scan parameters
      unsigned int occ_loop_i=0;
      for (unsigned int index_i=0; index_i<3; index_i++) {
	if (m_histoInfo.maxIndex(index_i) == vcal_arr.size()) {
	   occ_loop_i=index_i;
	   break;
        }
      }


      for (unsigned int loop_i=0; loop_i<m_histoInfo.maxIndex(occ_loop_i); loop_i++) {
	local_index[occ_loop_i]=loop_i;
	const Histo *a_histo ( data_provider.histogram( module_names[0], m_histoName,local_index));
	if (!a_histo) std::cout << "ERROR [SCurvePixelVisualiser::visualise] histogram could not be retrieved   check the indizes" << std::endl;
	
	int col_i = histo_index[0];
	int row_i = histo_index[1];
	
	// a_histo is a Root histogram, bins starting from 1
	row_i++;
	col_i++;
	float value  = binContent(a_histo,col_i,row_i);
	occ_arr.push_back(value);
      }
    }
    
    
    if (occ_arr.size() != vcal_arr.size()) {
      std::cout << "ERROR [SCurvePixelVisualiser::visualise] vcal steps and number of histograms do not match : " << occ_arr.size() << " != " << vcal_arr.size()
		<< "." << std::endl;
      return;
    }
    
    if (occ_arr.size()>0) {
      TCanvas *a_canvas=canvas();
      a_canvas->cd();

      TGraph *graph=new TGraph(occ_arr.size(), &(vcal_arr[0]), &(occ_arr[0]));

      graph->SetMarkerStyle(20);
      graph->SetBit(kCanDelete);
      graph->Draw("ALP");

      a_canvas->Modified();
      a_canvas->Update();
    }
  }


  IVisualiser *SCurveVisualisationKit::visualiser(const std::string &histo_name,
						  const std::vector<std::string> &/*folder_hierarchy*/) const
  {
    return new SCurvePixelVisualiser(histo_name);
  }

  SCurveVisualisationKit a_SCurveVisualisationKit_instance;

  bool SCurveVisualisationKit::init() {
    //@todo currently the SCurve histogram visualiser does not handle multiple modules
    //     but the the visualisation kit collector is not smart enough to return visualiser
    //     which handle the number of modules. 
    //     so the SCurve visualiser is disabled for the time being.
    //     
    // PixA::VisualisationFactory::registerKit("(/|^)OCCUPANCY$",&a_SCurveVisualisationKit_instance);
    //    PixA::VisualisationFactory::registerKit("\\(/\\|^\\)OCCUPANCY$",&a_SCurveVisualisationKit_instance);
    return true;
  }

  bool SCurveVisualisationKit::s_instantiated = SCurveVisualisationKit::init();
}
