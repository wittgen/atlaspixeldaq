#include "VisualisationFactory.h"
#include "VisualisationFactoryImpl.h"
namespace PixA {

  VisualisationFactory::~VisualisationFactory() {
  }

  VisualisationFactory *VisualisationFactory::createInstance() {
    return new VisualisationFactoryImpl;
  }

  VisualisationFactory *VisualisationFactory::s_instance=NULL;

}
