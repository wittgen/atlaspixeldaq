#include "PixScanModuleMapVisualiser.h"
#include <DataContainer/HistoUtil.h>
#include <TCanvas.h>
#include <TROOT.h>
#include <TH2.h>

#include "IScanDataProvider.h"
#include "VisualisationFactory.h"
#include "VisualisationUtil.h"
#include <DataContainer/HistoExtraInfoList.h>

namespace PixA {

  std::string PixScanModuleMapVisualiser::s_name("Show Histogram and Projection");

  void PixScanModuleMapVisualiser::fillIndexArray(IScanDataProvider &data_provider, const std::vector<std::string> &module_names, HistoInfo_t &info) const 
  {
    info.reset();
    if(module_names.empty() ) return;

    for (std::vector< std::string >::const_iterator module_iter=module_names.begin();
	 module_iter != module_names.end();
	 module_iter++) {

      HistoInfo_t an_info;
      data_provider.numberOfHistos(*module_iter, m_histoName, an_info);
      info.makeUnion(an_info);

    }
  }

  /** Return the name of the module.
   */
  std::string PixScanModuleMapVisualiser::getModuleTitle(IScanDataProvider &data_provider, const std::string &module_name)
  {
    const std::string &prod_name = data_provider.prodName(module_name);

    if (!prod_name.empty() && prod_name != module_name) {
      std::string module_title;

      module_title = module_name;
      module_title += " (";
      module_title += prod_name;
      module_title += ")";
      return module_title;
    }
    else {
      return module_name;
    }
  }

  TH2 *PixScanModuleMapVisualiser::get2DHisto(IScanDataProvider &data_provider, const std::string &module_name, const Index_t &index, int front_end) 
  {
    const Histo *a_histo = data_provider.histogram(module_name, m_histoName, index);

    TObject *obj = chipMap<TH2,Histo>(a_histo,front_end);
    if (obj && obj->InheritsFrom(TH2::Class())) {

      TH2 *h2=static_cast<TH2 *>(obj);

      if (h2->GetXaxis() && h2->GetYaxis()) {
	h2->GetXaxis()->SetTitle("Module Column");
	h2->GetYaxis()->SetTitle("Module Row");
      }
      if (h2->GetZaxis()) {
	const PixA::HistoExtraInfo_t &extra_info(PixA::HistoExtraInfoList::histoExtraInfo(m_histoName));
	h2->GetZaxis()->SetTitle(extra_info.axis(PixA::HistoExtraInfo_t::kZaxis).title().c_str());
      }
      return h2;
    }
    delete obj;
    return NULL;
  }

}
