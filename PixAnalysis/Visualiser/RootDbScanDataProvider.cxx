#include "RootDbScanDataProvider.h"

namespace PixA {

  ConnectivityRef RootDbScanDataProvider::s_invalidConnectivty;
  std::string RootDbScanDataProvider::s_emptyString;

  RootDbScanDataProvider::RootDbScanDataProvider() {}
  RootDbScanDataProvider::~RootDbScanDataProvider() {

    for(std::vector<Histo *>::iterator histo_iter = m_cleanupHistos.begin();
	histo_iter != m_cleanupHistos.end();
	histo_iter++) {
      delete *histo_iter;
      *histo_iter = NULL;
    }
    m_cleanupHistos.clear();
  }

  std::pair<HistoList_t *,unsigned int> RootDbScanDataProvider::createEmptyHistoList(const std::string &label_name)
  {
    std::map< std::string, unsigned int >::const_iterator label_iter = m_labelMap.find(label_name);
    if (label_iter == m_labelMap.end()) {
      unsigned int new_id = m_results.size();
      m_results.push_back(HistoList_t());

      std::pair<std::map< std::string, unsigned int >::iterator, bool> ret
	= m_labelMap.insert(std::make_pair(label_name, new_id));
      if (!ret.second) {
	std::string message;
	message += "INFO [RootDbScanDataProvider::createEmptyHistoList] Failed to create new histogram list for label ";
	message += label_name;
	message += ".";
	throw std::runtime_error(message);
      }
      label_iter= ret.first;
      m_currentLabel= label_iter->second;
    }
    assert( label_iter->second < m_results.size() );

    return std::make_pair<HistoList_t *, unsigned int>( &(m_results[label_iter->second]), (unsigned int)label_iter->second );
  }

}
