#ifndef _TH1Stack_h_
#define _TH1Stack_h_

#include <TH1.h>
#include <vector>

class TH1Stack : public TH1F
{
public:
  TH1Stack(const char *name, const char *title, int nbins, float min, float max) : TH1F(name,title,nbins, min,max){}

  TH1Stack(const TH1Stack &a);

  ~TH1Stack();

  TObject *Clone() const {
    return new TH1Stack(*this);
  }

  void Copy(TObject *dest) const;

  void clear();

  TH1 *NewH1(const char *name, const char *title);

  void DeleteBottomH1();

  void  Draw(Option_t* option);

private:
  std::vector<TH1 *> m_histoList;
  using TH1F::Clone;
  using TH1F::Copy;
};

#endif
