#include "IScanDataProvider.h"
#include "PixScanHistoVisualiser.h"
#include <DataContainer/HistoUtil.h>
#include <TCanvas.h>
#include <TH2.h>
#include <DataContainer/GenericHistogramAccess.h>
#include <TText.h>
#include <TStyle.h>
#include <Histo/Histo.h>

#include "VisualisationFactory.h"
#include "VisualisationUtil.h"

namespace PixA {

  std::string PixScanHistoVisualiser::s_name("Show Raw Histograms");

  void PixScanHistoVisualiser::fillIndexArray(IScanDataProvider &data_provider, const std::vector<std::string> &module_names, HistoInfo_t &info) const {
    info.reset();
    if(module_names.empty() ) return;

    for (std::vector< std::string >::const_iterator module_iter=module_names.begin();
	 module_iter != module_names.end();
	 module_iter++) {

      HistoInfo_t an_info;
      data_provider.numberOfHistos(*module_iter, m_histoName, an_info);
      info.makeUnion(an_info);

    }
    m_info = info;
  }

//   void PixScanHistoVisualiser::makeHistoName(const PixA::ScanConfigRef &scan_config, PixA::HistoRef &histo, std::vector<std::string> &histo_name) const
//   {
//     std::pair<EModuleNameingConvention, unsigned int> ret =
//       makePixScanHistoName(m_moduleNaming, m_modID, scan_config, histo, m_histoName,histo_name);
//     m_moduleNaming=ret.first;
//     m_modID=ret.second;
//   }


  template <class T_Histo_t> TObject *convertToObj(T_Histo_t *histo);

  inline TObject *convertToObj(const PixLib::Histo *histo) {
    return transform( histo );
  }

  inline TObject *convertToObj(const TH1 *histo) {
    return histo->Clone() ;
  }


  void PixScanHistoVisualiser::visualise(IScanDataProvider &data_provider,
					 const std::vector<std::string> &module_names, 
					 int , 
					 const Index_t &index,
					 int front_end)
  {
    if (module_names.empty() ) return;

    PlotCanvas_t plot_canvas=PlotCanvas_t::canvas(module_names.size());

    //    int old_opt_title = gStyle->GetOptTitle();
    gStyle->SetOptTitle(0);

    std::vector< const Histo * > histo_list;
    histo_list.reserve(module_names.size());

    std::string sub_title;

    //    try {
    {
      initPlot();
      // prepare
      for (std::vector< std::string >::const_iterator module_iter=module_names.begin();
	   module_iter != module_names.end();
	   module_iter++) {

	try {
	  // the histos are owned by the data_provider
	  const Histo *a_histo ( data_provider.histogram(*module_iter, m_histoName, index) );
	  histo_list.push_back(a_histo);
	  if ( a_histo ) {

	    prepare(data_provider, *module_iter, histo_list.back() , (m_canVisualiseOneFe ? front_end : -1));
	  }
	}
	catch (PixLib::PixDBException &err) {
	  std::cout << "ERROR [PixScanHistoVisualiser::visualise] Caught exception : " <<  err << std::endl;
	  histo_list.push_back(NULL);
	}

      }

      assert (histo_list.size() == module_names.size() );

      std::vector< const Histo * >::iterator histo_ptr_iter = histo_list.begin();
      unsigned int pad_i=0;
      bool first=true;
      for (std::vector< std::string >::const_iterator module_iter=module_names.begin();
	   module_iter != module_names.end();
	   module_iter++, pad_i++, histo_ptr_iter++) {

	TObject *obj=NULL;

	if (!(*histo_ptr_iter) ) continue;

	// create plotable object
	obj=transform( data_provider, *module_iter, *histo_ptr_iter,index, (m_canVisualiseOneFe ? front_end : -1));
	if (obj == *histo_ptr_iter || !obj) {
	  obj=convertToObj( *histo_ptr_iter );
	}

	if (obj) {
	  obj->SetBit(kCanDelete);
	  if (obj->InheritsFrom(TH1::Class())) {
	    static_cast<TH1 *>(obj)->SetDirectory(0);
	  }

	  if (first) {
	    // make plot title
	    plot_canvas.setTitle(makeTitle(data_provider, obj->GetTitle()));

	    if (module_names.size()>1) {
	      std::string name = *module_iter;
	      std::string::size_type pos = name.find("_M");
	      if (pos!=std::string::npos) {
		name.erase(pos);
		plot_canvas.setName(name);
	      }
	    }
	    else {
	      plot_canvas.setName( *module_iter );
	    }

	    plot_canvas.setId(data_provider.scanNumber());

	    if (sub_title.empty()) {
	      sub_title = subTitle(data_provider);
	      if (sub_title.empty()) {

		// mark loops which run along a histogram axes
		markUsedLoops(obj,data_provider.scanConfig());
		// could add loop variables whic
		sub_title=makeSubTitle(data_provider, index);
	      }
	    }
	    if (!sub_title.empty()) {
	      plot_canvas.setSubTitle(sub_title);
	    }

	    first=false;
	  }

	  if (module_names.size()==1) {
	    plot_canvas.plot()->cd();
	  }
	  else {
	    plot_canvas.plot()->cd(pad_i+1);
	  }

	  if (obj->InheritsFrom(TNamed::Class())) {
	    TNamed *named=static_cast<TNamed*>(obj);
	    std::string name = named->GetName();
	    if (name.find( *module_iter ) == std::string::npos) {
	      name += '_';
	      name += *module_iter;
	      named->SetName(name.c_str());
	    }
	  }

	  if (obj->InheritsFrom(TH2::Class())) {
	    TH2 *h2=static_cast<TH2*>(obj);
	    h2->SetStats(0);
	    h2->SetMinimum(0);
	    h2->SetMaximum(h2->GetMaximum());
	    obj->Draw("colz");
	  }
	  else if (obj->InheritsFrom(TH1::Class())) {
	    TH1 *h1=static_cast<TH1*>(obj);
	    h1->SetStats(0);
	    if (h1->GetYaxis()) {
	      if (module_names.size()==1) {
		h1->GetYaxis()->SetTitleOffset(1.1);
	      }
	    }
	    h1->Draw();
	  }
	  else {
	    obj->Draw();
	  }
	  std::stringstream mod_name;
	  const std::string &prod_name( data_provider.prodName(*module_iter) );

	  if (prod_name.empty() || prod_name == *module_iter) {
	    mod_name << *module_iter;
	  }
	  else {
	    mod_name << *module_iter << " (" << prod_name << ")";
	  }
	  TText *a_text=new TText(0.15,0.95,mod_name.str().c_str());
	  a_text->SetTextSize(a_text->GetTextSize()*1.2);
	  a_text->SetNDC(kTRUE);
	  a_text->SetBit(kCanDelete);
	  a_text->Draw();
	  enhancePlot( (module_names.size()==1 ? plot_canvas.plot() : plot_canvas.plot()->GetPad(pad_i+1)), data_provider, *module_iter, index, obj );
	}
	if (plot_canvas.plot()->GetPad(pad_i+1)) {
	  plot_canvas.plot()->GetPad(pad_i+1)->Modified();
	}
      }
    }
    plot_canvas.update();
    //gStyle->SetOptTitle(old_opt_title);
  }

  void PixScanHistoVisualiser::visualiseLoop(IScanDataProvider &data_provider,
					     const std::vector<std::string> &module_names,
					     unsigned int loop,
					     const Index_t &orig_index,
					     int front_end)
  {
    if (module_names.empty() ) return;

    if (loop >= orig_index.size() || loop >= HistoInfo_t::maxLevels() ) {
      visualise(data_provider,module_names,0,orig_index,front_end);
      return;
    }

    unsigned int n_loops = ( loop+1<orig_index.size() ? m_info.maxIndex(loop) : m_info.maxHistoIndex() );
    unsigned int start_loop = ( loop<orig_index.size() ? 0 : m_info.minHistoIndex() );

    if (n_loops<=start_loop) {
      visualise(data_provider,module_names,0,orig_index,front_end);
      return;
    }

    PlotCanvas_t plot_canvas=PlotCanvas_t::canvas(n_loops-start_loop, module_names.size());

    //    int old_opt_title = gStyle->GetOptTitle();
    gStyle->SetOptTitle(0);


    std::vector< std::vector<const Histo *> > histo_list;
    histo_list.reserve(module_names.size());

    std::string sub_title;

    //    try {
    {
      initPlot();


      // prepare
      for (std::vector< std::string >::const_iterator module_iter=module_names.begin();
	   module_iter != module_names.end();
	   module_iter++) {

	histo_list.push_back(std::vector<const Histo *>());
	Index_t clone_index(orig_index);
	for (unsigned int loop_i=start_loop; loop_i< n_loops; loop_i++) {
	  clone_index[loop]=loop_i;
	  try {

	    // the histos are owned by the data_provider
	    const Histo *a_histo ( data_provider.histogram(*module_iter, m_histoName, clone_index) );
	    histo_list.back().push_back(a_histo);
	    if ( a_histo ) {

	      prepare(data_provider, *module_iter, histo_list.back().back() , (m_canVisualiseOneFe ? front_end : -1));
	    }
	  }
	  catch (PixLib::PixDBException &err) {
	    std::cout << "ERROR [PixScanHistoVisualiser::visualise] Caught exception : " <<  err << std::endl;
	    histo_list.back().push_back(NULL);
	  }
	}

      }

      assert (histo_list.size() == module_names.size() );

      std::vector< std::vector<const Histo *> >::iterator loop_histo_list_ptr_iter = histo_list.begin();
      unsigned int row_i=0;
      unsigned int first_row_i=0;

      bool first=true;
      for (std::vector< std::string >::const_iterator module_iter=module_names.begin();
	   module_iter != module_names.end();
	   module_iter++, row_i++, loop_histo_list_ptr_iter++) {

	assert( loop_histo_list_ptr_iter != histo_list.end() );
	std::vector<const Histo *>::iterator histo_ptr_iter = loop_histo_list_ptr_iter->begin();

	unsigned int col_i=0;
	Index_t clone_index(orig_index);
	for (unsigned int loop_i=start_loop; loop_i< n_loops; loop_i++, histo_ptr_iter++ , col_i++) {
	  clone_index[loop]=loop_i;
	  assert( histo_ptr_iter != loop_histo_list_ptr_iter->end() );
	  TObject *obj=NULL;

	  if (!(*histo_ptr_iter) ) continue;

	  // create plotable object
	  obj=transform( data_provider, *module_iter, *histo_ptr_iter,clone_index, (m_canVisualiseOneFe ? front_end : -1));
	  if (obj == *histo_ptr_iter || !obj) {
	    obj=convertToObj( *histo_ptr_iter );
	  }

	  if (obj) {
	    obj->SetBit(kCanDelete);
	    if (obj->InheritsFrom(TH1::Class())) {
	      static_cast<TH1 *>(obj)->SetDirectory(0);
	    }

	    if (first) {
	      first_row_i=row_i;
	      // make plot title
	      plot_canvas.setTitle(makeTitle(data_provider, obj->GetTitle()));

	      if (module_names.size()>1) {
		std::string name = *module_iter;
		std::string::size_type pos = name.find("_M");
		if (pos!=std::string::npos) {
		  name.erase(pos);
		  plot_canvas.setName(name);
		}
	      }
	      else {
		plot_canvas.setName( *module_iter );
	      }

	      plot_canvas.setId(data_provider.scanNumber());

	      markUsedLoops(obj,data_provider.scanConfig());
	    }

	    unsigned int pad_i=0;
	    TVirtualPad *a_pad;
	    if (module_names.size()==1 && n_loops-start_loop==1) {
	      a_pad = plot_canvas.plot();
	    }
	    else {
	      pad_i=col_i+row_i*(n_loops-start_loop)+1;
	      a_pad = plot_canvas.plot()->GetPad(pad_i);
	    }
	    if (a_pad) {
	      a_pad->cd();
	    }

	    if (obj->InheritsFrom(TNamed::Class())) {
	      TNamed *named=static_cast<TNamed*>(obj);
	      std::string name = named->GetName();
	      if (name.find( *module_iter ) == std::string::npos) {
		name += '_';
		name += *module_iter;
		named->SetName(name.c_str());
	      }
	    }
	    if (obj->InheritsFrom(TH2::Class())) {
	      TH2 *h2=static_cast<TH2*>(obj);
	      h2->SetStats(0);
	      h2->SetMinimum(0);
	      h2->SetMaximum(h2->GetMaximum());
	      obj->Draw("colz");
	    }
	    else if (obj->InheritsFrom(TH1::Class())) {
	      TH1 *h1=static_cast<TH1*>(obj);
	      h1->SetStats(0);
	      if (h1->GetYaxis()) {
		if (module_names.size()==1) {
		  h1->GetYaxis()->SetTitleOffset(1.1);
		}
	      }
	      h1->Draw();
	    }
	    else {
	      obj->Draw();
	    }
	    std::stringstream mod_name;
	    const std::string &prod_name( data_provider.prodName(*module_iter) );

	    if (prod_name.empty() || prod_name == *module_iter) {
	      mod_name << *module_iter;
	    }
	    else {
	      mod_name << *module_iter << " (" << prod_name << ")";
	    }

	    unsigned int n_objs = n_loops-start_loop;
	    if (n_objs > module_names.size()) n_objs =module_names.size();
	    if (n_objs>7) n_objs=7;

	    float text_scale=(1+(.6*(n_objs-1)/7))*1.2;
	    {
	      TText *a_text=new TText(0.15,0.95,mod_name.str().c_str());
	      a_text->SetTextSize(a_text->GetTextSize()*text_scale);
	      a_text->SetNDC(kTRUE);
	      a_text->SetBit(kCanDelete);
	      a_text->Draw();
	    }
	    enhancePlot( a_pad, data_provider, *module_iter, clone_index, obj );


	    // need to make a copy of the loops used array
	    std::vector<bool> used_loops_clone = m_loopUsed;

	    if (row_i==first_row_i) {
	      std::string loop_text = makeLoopText(data_provider, loop, loop_i);
	      if (!loop_text.empty()) {
		if (a_pad) {
		  a_pad->cd();
		}
		TText *a_text=new TText(0.90,0.15,loop_text.c_str());
		a_text->SetTextSize(a_text->GetTextSize()*text_scale);
		a_text->SetNDC(kTRUE);
		a_text->SetTextAlign(31);
		a_text->SetTextColor(4);
		a_text->SetBit(kCanDelete);
		a_text->Draw();
	      }
	    }


	    if (first) {

	      if (sub_title.empty()) {
		// mark loops which run along a histogram axes
		// could add loop variables whic
		std::vector<bool> used_loops_clone2 = m_loopUsed;

		sub_title = subTitle(data_provider);
		if (sub_title.empty()) {

		  sub_title=makeSubTitle(data_provider, clone_index);
		}

		for (unsigned int i=0; i<3; i++) {
		  if (m_loopUsed[i] && !used_loops_clone2[i]) {
		    used_loops_clone[i]=m_loopUsed[i];
		  }
		}
	      }
	      if (!sub_title.empty()) {
		plot_canvas.setSubTitle(sub_title);
	      }

	      first=false;
	    }
	    // recover used loops for next sub plot
	    m_loopUsed = used_loops_clone;

	    if (a_pad) {
	      a_pad->Modified();
	    }
	  }

	}
      }
    }
    plot_canvas.update();
    //gStyle->SetOptTitle(old_opt_title);
  }


  TObject *PixScanHistoVisualiser::transform(IScanDataProvider &data_provider,
					     const std::string &module_name,
					     const Histo *histo,
					     const Index_t &index,
					     unsigned int front_end)
  {
    if (front_end<16) {
      return PixA::chipMap<TH2,Histo>(histo,front_end);
    }
    else {
      if (histo && PixA::is2D(histo)) {
	PixA::ConfigHandle scan_config = data_provider.scanConfig();
	if (scan_config) {
	  TObject *obj=createHisto(*histo, m_histoName, scan_config.ref(),index);
	  if (obj->InheritsFrom(TH2::Class())) {
	    TH2 *h2=static_cast<TH2 *>(obj);
	    if (h2->GetXaxis()) h2->GetXaxis()->SetTitleOffset(1.);
	    if (h2->GetYaxis()) h2->GetYaxis()->SetTitleOffset(1.3);
	    if (h2->GetZaxis()) h2->GetZaxis()->SetTitleOffset(1.);
	  }
	  return obj;
	}
      }
      return PixA::transform(histo);
    }
  }

  class AxisNameAccessor_t 
  {
  public:
    AxisNameAccessor_t() : m_nAxes(0),m_histo(NULL) {}
    AxisNameAccessor_t(TH2 *h2) : m_nAxes(2), m_histo(h2) {}
    AxisNameAccessor_t(TH1 *h1) : m_nAxes(1), m_histo(h1) {}

    unsigned int nAxes() const { return m_nAxes & 0xff; }

    const char *axisName(unsigned int axis_i) const {
      return (axis_i==0 ? m_histo->GetXaxis()->GetName() : m_histo->GetYaxis()->GetName() );
    }

    bool used(unsigned int axis_i) const { return m_nAxes & (1<<(8+axis_i)); }

    void setUsed(unsigned int axis_i) { m_nAxes |=(1<<(8+axis_i)); }

  private:
    unsigned int  m_nAxes;
    //bool m_used[2];
    TH1          *m_histo;
  };

  void PixScanHistoVisualiser::markUsedLoops(TObject *obj, const PixA::ConfigHandle &scan_config) 
  {

    for (unsigned int loop_i=0; loop_i<3; loop_i++) {
      m_loopUsed[loop_i]=false;
    }

    if (scan_config) {
      AxisNameAccessor_t axis_names;

      if (obj->InheritsFrom(TH2::Class())) {
	axis_names=AxisNameAccessor_t(static_cast<TH2 *>(obj));

      }
      else if (obj->InheritsFrom(TH1::Class())) {
	axis_names=AxisNameAccessor_t(static_cast<TH1 *>(obj));
      }
      if (axis_names.nAxes()>0) {
	try {
	  PixA::ConfigRef scan_config_ref( scan_config.ref() );
	  const PixA::ConfGrpRef &loop_config(scan_config_ref["loops"]);

	  for (unsigned int loop_i=0; loop_i<3; loop_i++) {

	    std::stringstream loop_idx;
	    loop_idx << loop_i;

	    if (!confVal<bool>(loop_config[std::string("activeLoop_")+loop_idx.str()])) continue;

	    std::string param_name = confVal<std::string>(loop_config[std::string("paramLoop_")+loop_idx.str()]);

	    for (unsigned int axis_i=0; axis_i< axis_names.nAxes(); axis_i++) {

	      if ( !axis_names.used(axis_i) && param_name.compare(axis_names.axisName(axis_i))==0) {
		m_loopUsed[loop_i]=true;
		axis_names.setUsed(axis_i);
		std::cout << "INFO [PixScanHistoVisualiser::markUsedLoops] loop " << loop_i << ", " << param_name
			  << " used for axis "
			  << axis_i << std::endl;
	      }
	    }
	  }
	}
	catch(...) {
	}
      }
    }
  }

  std::string PixScanHistoVisualiser::makeTitle(IScanDataProvider &/*data_provider*/, const std::string &histo_title)
  {
    std::stringstream the_title;
    std::string old_title(histo_title);
    std::string::size_type pos = old_title.find(" mod ");
    if (pos != std::string::npos) {
      old_title.erase(pos);
    }
    the_title << m_histoName << " / " << old_title;
    return the_title.str();
  }

  std::string PixScanHistoVisualiser::makeSubTitle(IScanDataProvider &data_provider, const Index_t &index)
  {
    assert( HistoInfo_t::s_nMaxLevels == 3+1);
    std::stringstream sub_title;
    PixA::ConfigHandle scan_config_handle(data_provider.scanConfig());
    if (!scan_config_handle) return "";


    try {
      //bool is_loop_used[3];
     // for (unsigned int loop_i=0; loop_i<3; loop_i++) {
	//is_loop_used[loop_i]=isLoopUsed(loop_i);
     // }

      for (unsigned int index_i=0; index_i+1< index.size(); index_i++) {
	sub_title << makeLoopText( data_provider, index_i, index[index_i]);
      }
    }
    catch(ConfigException &err) {
      std::cout << "INFO [PixScanHistoVisualiser::makeSubTitle] Caught exception : " << err
		<< std::endl;
    }
    catch(...) {
    }
    return sub_title.str();
  }


  std::string PixScanHistoVisualiser::makeLoopText( IScanDataProvider &data_provider,
						    unsigned int index_i,
						    unsigned int elm_i)
  {
    std::stringstream sub_title;
    PixA::ConfigHandle scan_config_handle(data_provider.scanConfig());
    if (!scan_config_handle) return "";

    PixA::ConfigRef scan_config( scan_config_handle.ref() );
    const PixA::ConfGrpRef &loop_config(scan_config["loops"]);

    for (unsigned int loop_i=3; loop_i-->0;) {
      //	if (loop_i+1 < index.size())
      if (!isLoopUsed(loop_i)) {
	std::stringstream loop_idx;
	loop_idx << loop_i;

	if (!confVal<bool>(loop_config[std::string("activeLoop_")+loop_idx.str()])) continue;

	std::string param_name = confVal<std::string>(loop_config[std::string("paramLoop_")+loop_idx.str()]);
	bool really_uniform = confVal<bool>(loop_config[std::string("loopVarUniformLoop_")+loop_idx.str()]);

	std::cout << "INFO [PixScanHistoVisualiser::makeSubTitle] loop " << loop_i << ", " << param_name
		  << " used for index "<< index_i << "?"
		  << std::endl;

	bool uniform = really_uniform;
	float a_value;
	if (!uniform) {
	  const std::vector<float> &values ( confVal<std::vector<float> >(loop_config[std::string("loopVarValuesLoop_")+loop_idx.str()]));
	  // verify that the number of steps is consistent with the number of sub histo levels:
	  if (values.size() != m_info.maxIndex(index_i))  continue;
	  m_loopUsed[loop_i]=true;
	  if (static_cast<unsigned int>(elm_i) < values.size()) {
	    a_value=values[ elm_i ];
	  }
	  else {
	    uniform=true;
	  }
	}
	if (uniform) {
	  int   steps   =confVal<int>(loop_config[std::string("loopVarNStepsLoop_")+loop_idx.str()]);

	  // verify that the number of steps is consistent with the number of sub histo levels:
	  if (static_cast<unsigned int>(steps) != m_info.maxIndex(index_i))  continue;
	  m_loopUsed[loop_i]=true;

	  float min_val = confVal<float>(loop_config[std::string("loopVarMinLoop_")+loop_idx.str()]);
	  float max_val = confVal<float>(loop_config[std::string("loopVarMaxLoop_")+loop_idx.str()]);
	  if (steps<=1) {
	    a_value = min_val + (max_val - min_val) * elm_i;
	  }
	  else {
	    a_value = min_val + (max_val - min_val)/(steps-1) * elm_i;
	  }
	}
	if (sub_title.str().size()>0) {
	  sub_title << ", ";
	}
	const VarInfo_t &var_info = varInfo(param_name);
	if (var_info.hasVarName() ) {
	  sub_title << var_info.varName();
	}
	else {
	  sub_title << param_name;
	}
	sub_title << " = ";
	if (uniform != really_uniform) {
	  sub_title << "~";
	}
	if (var_info.hasConversionFactor()) {
	  a_value *= var_info.conversionFactor();
	}
	sub_title << a_value << " " << var_info.unitName();
	break;
      }
    }
    return sub_title.str();
  }

  unsigned int PixScanHistoVisualiser::searchIndex(IScanDataProvider &data_provider, const std::string &var_name)
  {
    PixA::ConfigHandle scan_config_handle(data_provider.scanConfig());
    if (!scan_config_handle) return m_info.maxLevels();

    PixA::ConfigRef scan_config( scan_config_handle.ref() );
    const PixA::ConfGrpRef &loop_config(scan_config["loops"]);

    for (unsigned int loop_i=3; loop_i-->0;) {
      //	if (loop_i+1 < index.size())
      //      if (!isLoopUsed(loop_i)) 
      {
	std::stringstream loop_idx;
	loop_idx << loop_i;

	if (!confVal<bool>(loop_config[std::string("activeLoop_")+loop_idx.str()])) continue;

	std::string param_name = confVal<std::string>(loop_config[std::string("paramLoop_")+loop_idx.str()]);
	if (param_name == var_name) {

	  bool really_uniform = confVal<bool>(loop_config[std::string("loopVarUniformLoop_")+loop_idx.str()]);


	  bool uniform = really_uniform;
	  unsigned int n_steps=0;
	  if (!uniform) {
	    const std::vector<float> &values ( confVal<std::vector<float> >(loop_config[std::string("loopVarValuesLoop_")+loop_idx.str()]));
	    n_steps = values.size();
	  }
	  else {
	    n_steps   =confVal<int>(loop_config[std::string("loopVarNStepsLoop_")+loop_idx.str()]);
	  }
	  for (unsigned int index_i=0; index_i<4; index_i++) {

	    std::cout << "INFO [PixScanHistoVisualiser::makeSubTitle] loop " << loop_i << ", " << param_name
		      << " used for index "<< index_i << "?" << m_info.maxIndex(index_i) << " =?= " << n_steps
		      << std::endl;

	    if (m_info.maxIndex(index_i)<=0) {
	      if (m_info.maxHistoIndex()==n_steps) {
		return m_info.maxLevels()-1;
	      }
	      break;
	    }

	    if (m_info.maxIndex(index_i)==n_steps) { 
	      std::cout << "INFO [PixScanHistoVisualiser::makeSubTitle] loop " << loop_i << ", " << param_name
			<< " used for index "<< index_i << "."
			<< std::endl;
	      return index_i;
	    }
	  }
	}
      }
    }
    return m_info.maxLevels();
  }

  std::map<std::string, PixScanHistoVisualiser::VarInfo_t> PixScanHistoVisualiser::s_varList;

  const PixScanHistoVisualiser::VarInfo_t &PixScanHistoVisualiser::varInfo(const std::string &param_name) 
  {
    if (s_varList.empty()) {
      s_varList["OB_VISET"] = VarInfo_t("mV",1.,"VIset");
      s_varList["OB_RX_DELAY"] = VarInfo_t("ns",1.,"RX delay");
    }
    std::map<std::string, VarInfo_t>::const_iterator unit_iter = s_varList.find(param_name);

    if (unit_iter==s_varList.end()) {
      return s_varList[""];
    }
    else {
      return unit_iter->second;
    }
  }

  // dummy implementation for the virtual function
  void PixScanHistoVisualiser::enhancePlot(TVirtualPad */*plot_pad*/,
					   IScanDataProvider & /*data_provider*/,
					   const std::string &module_name,
					   const Index_t & /*index*/,
					   TObject */*obj*/ ) {}


  PixScanHistoVisualisationKit::PixScanHistoVisualisationKit() {
    VisualisationFactory::registerKit("$",this);
  }

  IVisualiser *PixScanHistoVisualisationKit::visualiser(const std::string &histo_name,
							const std::vector<std::string> &/*folder_hierarchy*/) const
  {
    return new PixScanHistoVisualiser(histo_name,false);
  }

  PixScanHistoVisualisationKit a_PixScanHistoVisualisationKit_instance;

}
