#ifndef _SimpleVisualiser_h_
#define _SimpleVisualiser_h_

#include "IVisualiser.h"
#include <cassert>

namespace PixA  {

  /** The interface of a simple histogram visualisation class.
   * The mehtods @ref name, @ref visualise and @ref fillIndexArray need to be implemented
   * The simple visualiser provides dummy implementatinos for some methods for cases where
   * a visualiser does not offer options and does not know anything about front-ends.
   */
  class ISimpleVisualiser : public IVisualiser
  {
  public:

    /** The simple visualiser does not offer options.
     */
    unsigned int nOptions() const {return 1;};

    /** The simple visualiser does not offer options.
     * This method needs to be implemented nevertheless.
     */
    const std::string &optionName(unsigned int /*option*/) const { return name();};

    /** The simple visualiser does not know anything about front-ends.
     */
    bool canVisualiseOneFrontEnd() const { return false;}

    /** The simple visualiser does not know anything about front-ends.
     * Returns the maximum number of modules the visualiser can display simultaneously.
     */
    unsigned int canVisualiseNModules() const { return 1;}

  };
}


#endif
