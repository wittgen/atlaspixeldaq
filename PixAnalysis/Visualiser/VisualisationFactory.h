#ifndef _VisualisationFactory_h_
#define _VisualisationFactory_h_

#include <string>
#include <vector>
#include <utility>

namespace PixA {

  class IVisualiser;
  class IVisualisationKit;
  class VisualisationKitSubSet_t;
  class ScanConfigRef;
  class HistoHandle;
  class ConnectivityRef;

  /** Factory class to create visualisation objects.
   * The static methods are indented for object creation.
   */
  class VisualisationFactory
  {
  protected:

    virtual ~VisualisationFactory();

    virtual IVisualiser *_visualiser(const VisualisationKitSubSet_t &kits,
				     const std::string &histo_name,
				     const std::vector<std::string> &folder_hierarchy) = 0;

    virtual void _registerKit(const std::string &pattern, IVisualisationKit *kit)  = 0 ;

    virtual void _getKits(const std::string &histo_name, const std::vector<std::string> &folder_hierarchy,  VisualisationKitSubSet_t &kits ) = 0;

    virtual void _getKits(const std::string &histo_name, VisualisationKitSubSet_t &kits ) = 0;

  public:
    static VisualisationFactory *instance() { if (!s_instance) s_instance = createInstance(); return s_instance;}

    /** Create a visualiser for the given histo.
     */
    static IVisualiser *visualiser(const VisualisationKitSubSet_t &kits,
				   const std::string &histo_name, 
				   const std::vector<std::string> &folder_hierarchy) {
      return instance()->_visualiser(kits, histo_name,folder_hierarchy);
    }


    /** Get a list of kits to create visualisers which match the histogram and folder names.
     */
    static void getKits(const std::string &histo_name, const std::vector<std::string> &folder_hierarchy,  VisualisationKitSubSet_t &kits ) {
      return instance()->_getKits(histo_name,folder_hierarchy, kits);
    }

    /** Get a list of kits to create visualisers which match the histogram and folder names.
     */
    static void getKits(const std::string &full_histo_name, VisualisationKitSubSet_t &kits ) {
      return instance()->_getKits(full_histo_name, kits);
    }


    /** Register a new kit for histograms which match the given regular expression.
     * @param pattern a regular expression to match the histogram including folder names.
     * @param kit a pointer to the kit which could be used for all matching histograms
     * The pattern can match the full histogram path name: [top leve]/.../[subfolder]/[histo name].
     * To exactly match the histo name, but not to restrict the folder,  use : /[histo_name]$.
     */
    static void registerKit(const std::string &pattern, IVisualisationKit *kit) {
      return instance()->_registerKit(pattern, kit);
    }

  private:
    static VisualisationFactory *createInstance();

    static VisualisationFactory *s_instance;
  };

}

#endif
