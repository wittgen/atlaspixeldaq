/* Dear emacs, this is -*-c++-*- */
#ifndef _IVisualiser_h_
#define _IVisualiser_h_

#include <string>
#include <vector>

class HistoInfo_t;

namespace PixA  {

  typedef std::vector<unsigned int> Index_t;
  class IScanDataProvider;
 
  /** The interface of a histogram visualisation class.
   */
  class IVisualiser
  {
  public:
    virtual ~IVisualiser() {};

    /** Get the name of the visualiser.
     */
    virtual const std::string &name() const = 0;

    /** Get the number of options (there is at least one).
     * On option is a variatio of the default visualisation.
     * A visualiser may or may not offer options. Option zero 
     * referrs to the default option.
     */
    virtual unsigned int nOptions() const = 0;

    /** Get the name of visualisation option.
     * @param option the index of the option (must be smaller than the value returned by @nOptions.).
     * @return the name of the option.
     */
    virtual const std::string &optionName(unsigned int option) const = 0;

    /** Returns true, if the visualiser can optionally display only one front-end.
     */
    virtual bool canVisualiseOneFrontEnd() const = 0;

    /** Returns the maximum number of modules the visualiser can display simultaneously.
     */
    virtual unsigned int canVisualiseNModules() const = 0;

    /** Fill the histogram info structure.
     * The structure contains the index ranges for each dimension.
     * @sa HistoInfo_t
     */
    virtual void fillIndexArray(IScanDataProvider &data_provider, const std::vector<std::string> &module_names, HistoInfo_t &info) const = 0;

    /** Perform the default visualisation for the given front_end and the given index set.
     * @param index the index set to specify the histogram in the multi dimensional histogram collection.
     * @param front_end the number of the front_end (0-15) which should be visualised or -1 for all front-ends
     * The front-end parameter will be ignored if canVisualiseOneFrontEnd returns false.
     */
    void visualise(IScanDataProvider &data_provider, const std::vector<std::string> &module_names, const Index_t &index, int front_end) {
      visualise(data_provider, module_names, 0,index,front_end);
    }

    /** Perform the chosen visualisation for the given front_end and the given index set.
     * @param option the visualisation option (0 referrs to the default option).
     * @param index the index set to specify the histogram in the multi dimensional histogram collection.
     * @param front_end the number of the front_end (0-15) which should be visualised or -1 for all front-ends
     * The front-end parameter will be ignored if canVisualiseOneFrontEnd returns false.
     */
    virtual void visualise(IScanDataProvider &data_provider, const std::vector<std::string> &module_names, int option, const Index_t &index, int front_end) = 0;
  };
}
#endif
