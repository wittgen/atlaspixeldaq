#include "VisualisationUtil.h"
#include <TCanvas.h>
#include <TText.h>
#include <TStyle.h>
#include <TROOT.h>

#include <sstream>

#include <TH1.h>

namespace PixA {

  unsigned int PlotCanvas_t::s_canvasCounter=0;
  unsigned int PlotCanvas_t::s_maxCanvasCounter=0;

  bool PlotCanvas_t::s_initialised=PlotCanvas_t::initROOT();

  bool PlotCanvas_t::initROOT() {
    TH1::AddDirectory(false);
    return true;
  }

  PlotCanvas_t::PlotCanvas_t(unsigned int n_objects, unsigned int n_y)
  {
    std::stringstream canvas_name;
    canvas_name << "pixcan";
    std::stringstream canvas_title;
    canvas_title << "Pixel Module Analysis Canvas";
    if (s_canvasCounter>0) {
      canvas_name << "_" << s_canvasCounter;
      canvas_title << " [" << s_canvasCounter << "]";
    }

    TCanvas *a_canvas = static_cast<TCanvas*>(gROOT->GetListOfCanvases()->FindObject(canvas_name.str().c_str()));
    if (!a_canvas) {
      a_canvas = new TCanvas(canvas_name.str().c_str(),canvas_title.str().c_str(), 10,10,700,850);
    }
    else {
      a_canvas->Clear();
      a_canvas->Show();
    }

    if (a_canvas) {
      a_canvas->cd();

      m_titlePad = new TPad("ptxt","Text pad",0,.92,1,1);
      m_titlePad->SetFillStyle(1001);
      m_titlePad->SetFillColor(18);
      m_titlePad->Draw();

      a_canvas->cd();
      m_plotPad = new TPad("pplt","Plot pad",0,0,1,.92);
      m_plotPad->Draw();
      if (n_objects>1) {
	if (n_y>0) {
	  m_plotPad->Divide(n_objects, n_y);
	}
	else {
	  m_plotPad->Divide(2,(n_objects+1)/2);
	}
      }
      else if (n_y>0) {
	m_plotPad->Divide(1,n_y);
      }
      adjustPads();
    }
  }
  
  void PlotCanvas_t::adjustPads()
  {
    unsigned int start_pad=(m_plotPad->GetPad(1) ? 1 : 0);
    float left_margin=(m_plotPad->GetPad(1) ? .115 : .14);
    for (unsigned int pad_i=start_pad; m_plotPad->GetPad(pad_i); pad_i++) {
      TVirtualPad *a_pad = m_plotPad->GetPad(pad_i);
      if (a_pad) {
	a_pad->SetBottomMargin(.14);
	a_pad->SetTopMargin(.06);
	a_pad->SetLeftMargin(left_margin);
	a_pad->SetRightMargin(.095);
      }
    }
  }

  void PlotCanvas_t::setTitle(const std::string &title)
  {
    m_titlePad->cd();
    TText *a_text=new TText(0.05,.64,title.c_str());
    a_text->SetTextSize(.35);
    a_text->SetNDC(kTRUE);
    a_text->SetBit(kCanDelete);
    a_text->Draw();
  }

  void PlotCanvas_t::setName(const std::string &name)
  {
    if (name.empty()) return;
    m_titlePad->cd();
    TText *a_text=new TText(0.05,.35,name.c_str());
    a_text->SetTextSize(.25);
    a_text->SetNDC(kTRUE);
    a_text->SetBit(kCanDelete);
    a_text->Draw();

  }

  void PlotCanvas_t::setId(unsigned int id)
  {
    if (id>0) {
      std::stringstream id_string;
      id_string << "Scan Nr. " << id;

      m_titlePad->cd();
      TText *a_text=new TText(0.95,0.35,id_string.str().c_str());
      a_text->SetTextSize(.25);
      a_text->SetTextAlign(31);
      a_text->SetNDC(kTRUE);
      a_text->SetBit(kCanDelete);
      a_text->Draw();
    }
  }

  void PlotCanvas_t::setSubTitle(const std::string &sub_title)
  {
    if (sub_title.empty()) return;
    m_titlePad->cd();
    TText *a_text=new TText(0.05,0.1,sub_title.c_str());
    a_text->SetTextSize(.25);
    a_text->SetTextColor(4);
    a_text->SetNDC(kTRUE);
    a_text->SetBit(kCanDelete);
    a_text->Draw();

  }

  class Rgb_t {
  public:
    Rgb_t(float r, float g, float b) : m_r(r), m_g(g), m_b(b) {}

    const float &r() const { return m_r; }
    const float &g() const { return m_g; }
    const float &b() const { return m_b; }

  private:
    float m_r;
    float m_g;
    float m_b;
  };

  void PlotCanvas_t::setPalette(EPaletteType type)
  {
    // copied from ModuleAnalysis/RootStuff.cpp
    // (c) Joern Grosse-Knetter

    Int_t i;
    Float_t colfrac;
    TColor *color;
    int    palette[100];// 2D-map colour palette black-red-yellow
    switch(type){
    case kMABlackRedYellow:
      // black-red-yellow
      // start with black-to-red
      for(i=0;i<60;i++){
	colfrac = (Float_t)i/60;
	if(! gROOT->GetColor(201+i)){
	  color = new TColor (201+i,colfrac,0,0,"");
	}else{
	  color = gROOT->GetColor(201+i);
	  color->SetRGB(colfrac,0,0);
	}
	palette[i]=201+i;
      }
      // red-to-yellow now
      for(i=0;i<40;i++){
	colfrac = (Float_t)i/40;
	if(! gROOT->GetColor(261+i)){
	  color = new TColor (261+i,1,colfrac,0,"");
	}else{
	  color = gROOT->GetColor(261+i);
	  color->SetRGB(1,colfrac,0);
	}
	palette[i+60]=261+i;
      }
      gStyle->SetPalette(100,palette); 
      break;
    case kBlackRed:
      // black-red, *no* yellow
      for(i=0;i<100;i++){
	colfrac = (Float_t)i/100;
	if(! gROOT->GetColor(201+i)){
	  color = new TColor (201+i,colfrac,0,0,"");
	}else{
	  color = gROOT->GetColor(201+i);
	  color->SetRGB(colfrac,0,0);
	}
	palette[i]=201+i;
      }
      gStyle->SetPalette(100,palette); 
      break;
    case kRootRainbow:
      // rainbow
      gStyle->SetPalette(1); 
      break;
    case kTurboDAQ:
      // black-blue-purple-red-orange-yellow-white (TurboDAQ style)
      for(i=0;i<20;i++){ // black to blue
	colfrac = (Float_t)i/20;
	if(! gROOT->GetColor(201+i)){
	  color = new TColor (201+i,0,0,colfrac,"");
	}else{
	  color = gROOT->GetColor(201+i);
	  color->SetRGB(0,0,colfrac);
	}
	palette[i]=201+i;
      }
      for(i=0;i<20;i++){ // blue to purple
	colfrac = 0.8*(Float_t)i/20;
	if(! gROOT->GetColor(221+i)){
	  color = new TColor (221+i,colfrac,0,1,"");
	}else{
	  color = gROOT->GetColor(221+i);
	  color->SetRGB(colfrac,0,1);
	}
	palette[20+i]=221+i;
      }
      for(i=0;i<20;i++){ // purple to red
	colfrac = (Float_t)i/20;
	if(! gROOT->GetColor(241+i)){
	  color = new TColor (241+i,0.8+0.2*colfrac,0,1-colfrac,"");
	}else{
	  color = gROOT->GetColor(241+i);
	  color->SetRGB(0.8+0.2*colfrac,0,1-colfrac);
	}
	palette[40+i]=241+i;
      }
      for(i=0;i<25;i++){ // red to orange to yellow
	colfrac = (Float_t)i/25;
	if(! gROOT->GetColor(261+i)){
	  color = new TColor (261+i,1,colfrac,0,"");
	}else{
	  color = gROOT->GetColor(261+i);
	  color->SetRGB(1,colfrac,0);
	}
	palette[60+i]=261+i;
      }
      for(i=0;i<15;i++){ // yellow to white
	colfrac = (Float_t)i/15;
	if(! gROOT->GetColor(286+i)){
	  color = new TColor (286+i,1,1,colfrac,"");
	}else{
	  color = gROOT->GetColor(286+i);
	  color->SetRGB(1,1,colfrac);
	}
	palette[80+i]=286+i;
      }
      gStyle->SetPalette(100,palette); 
      break;
    case kBlackWhite:
      // black-white
      for(i=0;i<90;i++){
	colfrac = (Float_t)i/100;
	if(! gROOT->GetColor(201+i)){
	  color = new TColor (201+i,colfrac,colfrac,colfrac,"");
	}else{
	  color = gROOT->GetColor(201+i);
	  color->SetRGB(colfrac,colfrac,colfrac);
	}
	palette[i]=201+i;
      }
      gStyle->SetPalette(90,palette); 
      break;
    default:
      break;
    }

    // define some shades of yellow, magenta and blue
    std::vector< std::pair<int, Rgb_t > > colour_list;
    colour_list.push_back( std::make_pair(191,Rgb_t(0.90225, 0.89075, 0.72975 ) ) );  // faint dirty yellow 
    colour_list.push_back( std::make_pair(106,Rgb_t(0.7, 0, 0.7)));                   // dark magenta
    colour_list.push_back( std::make_pair(109, Rgb_t(0.171244, 0.151098, 0.674902))); // dark blue
    for ( std::vector< std::pair<int, Rgb_t > >::const_iterator colour_iter = colour_list.begin();
	  colour_iter != colour_list.end();
	  colour_iter++) {
      TColor *color2 = gROOT->GetColor(colour_iter->first);
      if(!color2){
	new TColor (colour_iter->first,colour_iter->second.r(), colour_iter->second.g(), colour_iter->second.b());
      }
      else{
	color2->SetRGB(colour_iter->second.r(), colour_iter->second.g(), colour_iter->second.b());
      }
    }
  }

}
