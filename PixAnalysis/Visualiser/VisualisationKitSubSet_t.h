#ifndef _VisualisationKitSubSet_h_
#define _VisualisationKitSubSet_h_

#include <vector>

namespace PixA {

  class VisualisationFactoryImpl;
  class IVisualisationKit;

  typedef std::vector<IVisualisationKit *> KitSubSetList_t;

  class VisualisationKitSubSet_t {
    friend class VisualisationFactoryImpl;
  public:
    void clear()       { m_kitList.clear(); }
    bool empty() const { return m_kitList.empty(); }
    bool fallBackOnly() const;
    bool aboveThreshold(unsigned int threshold_priority) const;

  protected:
    KitSubSetList_t &list();
    void push_back(IVisualisationKit *a_kit) { m_kitList.push_back(a_kit);}
    IVisualisationKit *back() { return m_kitList.back();}
    void pop_back() { return m_kitList.pop_back();}


//    KitSubSetList_t::iterator begin()        {return m_kitList.begin();}
//    KitSubSetList_t::iterator end()          {return m_kitList.end();}
    KitSubSetList_t::const_iterator begin() const       {return m_kitList.begin();}
    KitSubSetList_t::const_iterator end()   const       {return m_kitList.end();}
  private:
    KitSubSetList_t m_kitList;
  };

}

#endif
