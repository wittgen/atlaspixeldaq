#include "HistoVisualiser.h"
#include <DataContainer/HistoUtil.h>
#include <TCanvas.h>
#include <TROOT.h>
#include <TH2.h>
#include <Histo/Histo.h>
#include <IScanDataProvider.h>

#include "VisualisationFactory.h"
#include "VisualisationUtil.h"


namespace PixA {

  template <class Histo_t> TH1 *convertToTH1(Histo_t *histo);

  inline TH1 *convertToTH1(const PixLib::Histo *histo) {
    std::unique_ptr<TObject> obj ( transform( histo ) );
    if ( obj.get() && obj->InheritsFrom(TH1::Class())) {
      return static_cast<TH1 *>(obj.release());
    }
    return NULL;
  }

  inline TH1 *convertToTH1(const TH1 *histo) {
    return static_cast<TH1 *>( histo ? histo->Clone() : NULL );
  }

  std::string HistoVisualiser::s_name("Show Raw Histograms");

  void HistoVisualiser::fillIndexArray(IScanDataProvider &data_provider, const std::vector<std::string> &module_names,  HistoInfo_t &info) const {
    assert( module_names.size() == 1 );
    data_provider.numberOfHistos(module_names[0],m_histoName,info);
  }

  void HistoVisualiser::visualise(IScanDataProvider &data_provider, const std::vector<std::string> &module_names, int , const Index_t &index, int ) 
  {
    assert( module_names.size() == 1 );
    TObject *obj=NULL;
    {
      obj=convertToTH1( data_provider.histogram(module_names[0],m_histoName,index) );
    }

    if (obj) {
      PlotCanvas_t plot_canvas=PlotCanvas_t::canvas( 1);
      plot_canvas.setTitle(obj->GetTitle());
      plot_canvas.setId(data_provider.scanNumber());

      plot_canvas.plot()->cd();
      obj->SetBit(kCanDelete);
      if (obj->InheritsFrom(TH2::Class())) {
	obj->Draw("colz");
      }
      else if (obj->InheritsFrom(TH1::Class())) {
	obj->Draw();
      }
      else {
	obj->Draw();
      }
      plot_canvas.update();
    }
  }

  TCanvas *HistoVisualiser::canvas() {
    TCanvas *a_canvas = static_cast<TCanvas*>(gROOT->GetListOfCanvases()->FindObject("pixcan"));
    if (!a_canvas) {
      a_canvas = new TCanvas("pixcan","Pixel Module Analysis Canvas",10,10,700,850);
    }
    else {
      a_canvas->Clear();
      a_canvas->Show();
    }
    a_canvas->cd();
    return a_canvas;
  }

  HistoVisualisationKit::HistoVisualisationKit(bool can_be_option) : m_canBeOption(can_be_option) {
  }

  IVisualiser *HistoVisualisationKit::visualiser(const std::string &histo_name,
						 const std::vector<std::string> &/*folder_hierarchy*/) const 
  {
    return new HistoVisualiser(histo_name);
  }
  
  HistoVisualisationKit a_HistoVisualisationKitFallBack_instance(false);
  HistoVisualisationKit a_HistoVisualisationKit_instance(true);

  bool HistoVisualisationKit::init() {
    VisualisationFactory::registerKit("$",&a_HistoVisualisationKitFallBack_instance);
    VisualisationFactory::registerKit("/HistoSummary$",&a_HistoVisualisationKit_instance);
    VisualisationFactory::registerKit("/InlinkOutlink$",&a_HistoVisualisationKit_instance);//cla
    VisualisationFactory::registerKit("/PowerMeasurement$",&a_HistoVisualisationKit_instance);//cla
    return true;
  }

  bool HistoVisualisationKit::s_instantiated = HistoVisualisationKit::init();


  // EXAMPLE:
//   class Phase1Visualiser : public ISimpleVisualiser {
//   public:

//     Phase1Visualiser(const std::string &histo_name, const PixA::HistoHandle &handle) : m_histoName(histo_name), m_histo(handle) {}
//     /** Get the name of the visualiser.
//      */
//     const std::string &name() const { return s_name;}

//     /** Returns true, if the visualiser can optionally display only one front-end.
//      */
//     bool canVisualiseOneFrontEnd() const {return false;}

//     /** Fill the histogram info structure.
//      * The structure contains the index ranges for each dimension.
//      * @sa HistoInfo_t
//      */
//     void fillIndexArray(HistoInfo_t &info) const;

//     /** Perform the chosen visualisation for the given front_end and the given index set.
//      * @param option the visualisation option.
//      * @param index the index set to specify the histogram in the multi dimensional histogram collection.
//      * @param front_end the number of the front_end (0-15) which should be visualised or -1 for all front-ends
//      * The front-end parameter will be ignored if canVisualiseOneFrontEnd returns false.
//      */
//     void visualise(int , int index[4], int front_end);

//   private:

//     TCanvas *canvas();

//     static std::string s_name;
//     std::string m_histoName;
//     HistoHandle m_histo;
//   };

//   std::string Phase1Visualiser::s_name("Show Phase1 Summary");

//   void Phase1Visualiser::fillIndexArray(HistoInfo_t &info) const {
//     info.reset();
//     info.setMaxHistoIndex(1);
//     info.setMinHistoIndex(0);
//     //    HistoRef histo(m_histo.ref());
//     //    histo.numberOfHistos(m_histoName,info);
//   }

//   void Phase1Visualiser::visualise(int , int index[4], int ) 
//   {
//     HistoRef histo(m_histo.ref());
//     int my_index[4]={-1,-1,-1,0};
//     TCanvas *a_canvas=canvas();
//     a_canvas->cd();
//     a_canvas->Divide(2,4);
//     for (unsigned int i=0; i<7; i++) {
//       my_index[3]=i;
//       std::shared_ptr<PixLib::Histo> a_histo ( histo.histo(m_histoName,my_index));
//       if (a_histo.get()) {
// 	TObject *obj = transform(a_histo.get());
// 	if (obj) {
// 	  a_canvas->cd(i+1);
// 	  obj->SetBit(kCanDelete);
// 	  if (obj->InheritsFrom(TH2::Class())) {
// 	    obj->Draw("colz");
// 	  }
// 	  else  {
// 	      obj->Draw();
// 	  }
// 	}
//       }
//     }
//     a_canvas->Modified();
//     a_canvas->Update();
//   }

//   TCanvas *Phase1Visualiser::canvas() {
//     TCanvas *a_canvas = static_cast<TCanvas*>(gROOT->GetListOfCanvases()->FindObject("pixcan"));
//     if (!a_canvas) {
//       a_canvas = new TCanvas("pixcan","Pixel Module Analysis Canvas",10,10,700,850);
//     }
//     else {
//       a_canvas->Clear();
//       a_canvas->Show();
//     }
//     a_canvas->cd();
//     return a_canvas;
//   }

//   class Phase1VisualisationKit : public IVisualisationKit 
//   {
//   public:
//     Phase1VisualisationKit();

//     unsigned int priority() const {return  IVisualisationKit::normalPriority+1;}

//     /** Create a visualiser for the given inputs or return NULL.
//      */
//     IVisualiser *visualiser(const std::string &histo_name, const std::vector<std::string> &/*folder_hierarchy*/,
// 				    const PixA::HistoHandle &histo) const;

//   };

//   Phase1VisualisationKit::Phase1VisualisationKit() {
//     VisualisationFactory::registerKit("/Phase_1/HistoSummary$",this);
//   }

//   IVisualiser *Phase1VisualisationKit::visualiser(const std::string &histo_name, const std::vector<std::string> &/*folder_hierarchy*/,  
// 						       const PixA::HistoHandle &histo) const {
//     return new Phase1Visualiser(histo_name,histo);
//   }

//   Phase1VisualisationKit a_Phase1VisualisationKit_instance;
// 
}
