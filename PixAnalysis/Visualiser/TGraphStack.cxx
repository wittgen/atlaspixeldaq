#include <TGraphStack.h>
#include <TH1.h>
#include <iostream>

TGraphStack::TGraphStack(const TGraphStack &a)
  : TNamed(a)
{
  for (std::vector<TGraph *>::const_iterator graph_iter = a.m_graphList.begin();
       graph_iter != a.m_graphList.end();
       graph_iter++) {
    m_graphList.push_back(static_cast<TGraph *>((*graph_iter)->Clone()));
  }
}

TGraphStack::~TGraphStack()
{
  clear();
}

void TGraphStack::clear()
{
  for (std::vector<TGraph *>::const_iterator graph_iter = m_graphList.begin();
       graph_iter != m_graphList.end();
       graph_iter++) {
    delete *graph_iter;
  }
  m_graphList.clear();
}


void TGraphStack::Copy(TObject &dest) const
{
  if (!dest.InheritsFrom(TGraphStack::Class())) return;

  TNamed::Copy(dest);

  TGraphStack *dest_stack = static_cast<TGraphStack *>(&dest);
  dest_stack->clear();
  for (std::vector<TGraph *>::const_iterator graph_iter = m_graphList.begin();
       graph_iter != m_graphList.end();
       graph_iter++) {
    dest_stack->m_graphList.push_back(static_cast<TGraph *>((*graph_iter)->Clone()));
  }
}

void TGraphStack::DeleteBottomGraph() {
  if (m_graphList.empty()) return;
  delete m_graphList.back();
  m_graphList.pop_back();
}

void  TGraphStack::Draw(Option_t* option)
{

  if (m_graphList.empty()) return;

  //  TGraph::Draw(option);
  std::string option_str(option);
  std::string::size_type pos=option_str.find("A");
  if (pos==std::string::npos) {
    pos=option_str.find("a");
  }

  (*m_graphList.rbegin())->SetMinimum(m_min);
  (*m_graphList.rbegin())->SetMaximum(m_max);
  bool first=true;
  for (std::vector<TGraph *>::reverse_iterator graph_iter = m_graphList.rbegin();
       graph_iter != m_graphList.rend();
       graph_iter++) {
    //    std::cout << "INFO [TGraphStack::Draw] option = " << option_str << std::endl;
    (*graph_iter)->Draw(option_str.c_str());
    if (first) {
      if ((*graph_iter)->GetHistogram()) {
	if (!m_axisTitle[0].empty() && (*graph_iter)->GetHistogram()->GetXaxis()) {
	  (*graph_iter)->GetHistogram()->GetXaxis()->SetTitle(m_axisTitle[0].c_str());
	}
	if (!m_axisTitle[1].empty() && (*graph_iter)->GetHistogram()->GetYaxis()) {
	  (*graph_iter)->GetHistogram()->GetYaxis()->SetTitle(m_axisTitle[1].c_str());
	}
      }
      if (pos != std::string::npos) {
	option_str.erase(pos,1);
	pos=std::string::npos;
      }
      first=false;
    }

  }
  m_graphList.clear();

  // If the object is not used anymore by the caller 
  // we delete it.
  if (TestBit(kCanDelete)) {
    delete this;
  }
}
