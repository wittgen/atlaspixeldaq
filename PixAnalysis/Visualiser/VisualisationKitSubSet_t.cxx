#include "VisualisationKitSubSet_t.h"
#include "IVisualisationKit.h"

namespace PixA {

  bool VisualisationKitSubSet_t::fallBackOnly() const
  {
    return !aboveThreshold(IVisualisationKit::fallBackOnly);
  }

  bool VisualisationKitSubSet_t::aboveThreshold(unsigned int threshold_priority) const
  {
    for (KitSubSetList_t::const_iterator kit_iter = begin();
	 kit_iter != end();
	 ++kit_iter) {
      if ((*kit_iter)->priority()>threshold_priority) return true;
    }
    return false;
  }

}
