#ifndef _ModuleMapVisualiser_h_
#define _ModuleMapVisualiser_h_

#include "ISimpleVisualiser.h"

class TH1;
class TH2;
class TCanvas;
class TObject;
class TVirtualPad;

namespace PixA {

  class HistoBinning;

  /** Visualise the a 2D map, a 1D projection and a graph of all the pixels of a module or chip.
   * The class is abstract. In particular the derived class has to provide the 2d histogram
   */
  class ModuleMapVisualiser : public IVisualiser {
  public:

    ModuleMapVisualiser(const std::string &title)
      : m_title(title)
      {}

    /** Returns true, if the visualiser can optionally display only one front-end.
     */
    bool canVisualiseOneFrontEnd() const {return true;}

    /** The simple visualiser does not offer options.
     */
    unsigned int nOptions() const {return 4;}

    /** The simple visualiser does not offer options.
     * This method needs to be implemented nevertheless.
     */
    const std::string &optionName(unsigned int option) const { assert(option < 4); return s_optionName[option];}


    /** The simple visualiser does not know anything about front-ends.
     * Returns the maximum number of modules the visualiser can display simultaneously.
     */
    unsigned int canVisualiseNModules() const { return 8;}


    /** Perform the chosen visualisation for the given front_end and the given index set.
     * @param option the visualisation option.
     * @param index the index set to specify the histogram in the multi dimensional histogram collection.
     * @param front_end the number of the front_end (0-15) which should be visualised or -1 for all front-ends
     * The front-end parameter will be ignored if canVisualiseOneFrontEnd returns false.
     */
    void visualise(IScanDataProvider &data_provider, const std::vector<std::string> &module_names, int option, const Index_t &index, int front_end);

  protected:
    /** function which will be called before starting to plot.
     */
    virtual void initPlot(IScanDataProvider &/*data_provider*/, TVirtualPad* /*parent*/) {}

    /** function which could be used by derived classes to extend the plot.
     * like overlaying the optimum boc parameters.
     */
    virtual void enhancePlot(IScanDataProvider &/*data_provider*/, const std::vector<std::string> &/*module_names*/, TVirtualPad* /*plot_pad*/) {}

    /** Get the 2d histogram of the chip / module.
     */
    virtual TH2 *get2DHisto(IScanDataProvider &data_provider, const std::string &module_name, const Index_t &index, int front_end) = 0;

    /** Enahnce the map.
     */
    virtual void enhanceMap(IScanDataProvider &/*data_provider*/, const std::string &/*module_name*/, TVirtualPad* /*plot_pad*/, TH2* /* h2 */) {};


    /** Get the 2d histogram of the chip / module.
     */
    virtual TH1 *getProjection(IScanDataProvider &data_provider, const std::string &module_name, TH2* h2, int option);

    /** Enhance the visualisation of the projection.
     * Could be used to add fits.
     * The default function sets the y-axis to log scale.
     */
    virtual void enhanceProjection(IScanDataProvider &data_provider, const std::string &module_name, TVirtualPad* plot_pad, TH1* /* h1 */);


    /** Get a 1D graph of the pixels.
     */
    virtual TObject *getPixelGraph(IScanDataProvider &data_provider, const std::string &module_name, TH2 *h2, int option);

    /** Enhance the visualisation of the projection.
     * Could be used to add fits.
     */
    virtual void enhancePixelGraph(IScanDataProvider &/*data_provider*/, const std::string &/*module_name*/, TVirtualPad* /*plot_pad*/, TObject* /* graph */) {};


    /** Function which will be called at the end of the plot
     */
    virtual void finish(IScanDataProvider &/*data_provider*/, const std::vector<std::string> &/*module_names*/, TVirtualPad* /*parent*/) {};

  protected:

    /** Get the binning for the 1d histogram.
     */
    static PixA::HistoBinning getBinning(TH2 *h2, unsigned int mask_stages, unsigned int total_mask_stages);


    /** Get the projection of given 1D histogram
     */
    static TH1 *getProjection(TH2 *h2,
			      const PixA::HistoBinning &binning,
			      unsigned int mask_stages,
			      unsigned int total_mask_stages, 
			      int option);


    /** Get a list of graphs of normal, long, ganged, inter ganged, scanned, not scanned etc. pixels.
     */
    static TObject *getPixelGraph(TH2 *h2, unsigned int mask_stages, unsigned int total_mask_stages, int option);


    /** Show the module name on the given pad.
     */
    virtual void showModuleName(const std::string &module_name, TVirtualPad *a_pad) const;
    
    
    /** Extract pp0 name from name list.
     */
    static std::string pp0Name(const std::vector<std::string> &name_list);

  private:

    std::string   m_title;
    std::string   m_objectName;
    //unsigned int  m_nObjects;
    static std::string s_optionName[4];
  };


}

#endif
