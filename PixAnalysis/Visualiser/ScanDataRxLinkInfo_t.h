/* Dear emacs, this is -*-c++-*- */
#ifndef _PixA_ScanDataRxLinkInfo_t_h_
#define _PixA_ScanDataRxLinkInfo_t_h_

#include <ConfigWrapper/RxLinkInfo_t.h>
#include "IScanDataProvider.h"

namespace PixA {

  class ScanDataRxLinkInfo_t : public RxLinkInfo_t
  {
  public:
    ScanDataRxLinkInfo_t(IScanDataProvider &data_provider, const std::string &module_name)
      : RxLinkInfo_t(module_name,
		     data_provider.connectivity(),
		     data_provider.scanConfig(),
		     data_provider.moduleConfig(module_name) )
    {}

  };

}
#endif
