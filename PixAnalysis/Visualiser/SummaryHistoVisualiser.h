#ifndef _SummaryHistoVisualiser_h_
#define _SummaryHistoVisualiser_h_

#include "ISimpleVisualiser.h"
#include "IVisualisationKit.h"
#include <DataContainer/HistoRef.h>

class TCanvas;

namespace PixA {

  class SummaryHistoVisualiser : public ISimpleVisualiser {
  public:

    SummaryHistoVisualiser(const std::string &histo_name, const PixA::HistoHandle &handle) : m_histoName(histo_name), m_histo(handle) {}
    /** Get the name of the visualiser.
     */
    const std::string &name() const { return s_name;}

    /** Returns true, if the visualiser can optionally display only one front-end.
     */
    bool canVisualiseOneFrontEnd() const {return false;}

    /** Fill the histogram info structure.
     * The structure contains the index ranges for each dimension.
     * @sa HistInfo_t
     */
    void fillIndexArray(HistInfo_t &info) const;

    /** Perform the chosen visualisation for the given front_end and the given index set.
     * @param option the visualisation option.
     * @param index the index set to specify the histogram in the multi dimensional histogram collection.
     * @param front_end the number of the front_end (0-15) which should be visualised or -1 for all front-ends
     * The front-end parameter will be ignored if canVisualiseOneFrontEnd returns false.
     */
    void visualise(int , int index[4], int front_end);

  private:

    static std::string s_name;
    std::string m_histoName;
    HistoHandle m_histo;
  };

  class SummaryHistoVisualisationKit : public IVisualisationKit 
  {
  public:
    SummaryHistoVisualisationKit();

    unsigned int priority() const {return IVisualisationKit::normalPriority + 2;}

    /** Create a visualiser for the given inputs or return NULL.
     */
    IVisualiser *visualiser(const std::string &histo_name, const std::vector<std::string> &/*folder_hierarchy*/,
				    const PixA::HistoHandle &histo) const;

  };

}

#endif
