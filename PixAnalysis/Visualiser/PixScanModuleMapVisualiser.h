#ifndef _PixScanModuleMapVisualiser_h_
#define _PixScanModuleMapVisualiser_h_

#include "ModuleMapVisualiser.h"
#include <DataContainer/HistoRef.h>
#include <ConfigWrapper/ScanConfig.h>
#include <ConfigWrapper/ConfObjUtil.h>


class TCanvas;
class TObject;
class TVirtualPad;

namespace PixA {

  /** Visualise the a 2D map, a 1D projection and a graph of all the pixels of a module or chip.
   */
  class PixScanModuleMapVisualiser : public ModuleMapVisualiser {
  public:

    PixScanModuleMapVisualiser(const std::string &histo_name)
      : ModuleMapVisualiser(histo_name),
	m_histoName(histo_name)
      {}

    /** Get the name of the visualiser.
     */
    const std::string &name() const { return s_name;}

    /** Fill the histogram info structure.
     * The structure contains the index ranges for each dimension.
     * @sa HistoInfo_t
     */
    void fillIndexArray(IScanDataProvider &data_provider, const std::vector<std::string> &module_names, HistoInfo_t &info) const;

    /** Utility function to produce a title for modules.
     */
    static std::string getModuleTitle(IScanDataProvider &data_provider, const std::string &module_name);

  protected:

    /** Get the 2d histogram of the chip / module.
     */
    TH2 *get2DHisto(IScanDataProvider &data_provider, const std::string &module_name, const Index_t &index, int front_end);

    std::string m_histoName;

    static std::string s_name;
  };

}

#endif
