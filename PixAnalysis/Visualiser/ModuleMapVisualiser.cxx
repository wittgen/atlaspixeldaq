#include <DataContainer/HistoUtil.h>
#include <TCanvas.h>
#include <TH2.h>
#include <TText.h>
#include <TStyle.h>
#include <Histo/Histo.h>
#include "TH1Stack.h"
#include "TGraphStack.h"
#include "VisualisationUtil.h"
#include "IScanDataProvider.h"

#include "VisualisationFactory.h"
#include "ModuleMapVisualiser.h"
#include <DataContainer/Average_t.h>

#include "DataContainer/GenericHistogramAccess.h"

using namespace PixLib;
namespace PixA {

  std::string ModuleMapVisualiser::s_optionName[4]={"Show 1D or all", "Show 1D Projection", "Show Map","Show Graph"};

  void ModuleMapVisualiser::visualise(IScanDataProvider &data_provider, const std::vector<std::string> &module_names, int option, const Index_t &index, int front_end)
  {
    //std::cout << "INFO [ModuleMapVisualiser::visualise] front-end = " << front_end << std::endl;
    // visualiser only workds for single modules.
    //    assert(module_names.size() == 1);

    if (module_names.empty() ) return;
    PlotCanvas_t plot_canvas=PlotCanvas_t::canvas( (module_names.size()==1) ? 0 : module_names.size() );
    if (module_names.size()==1) {
      gStyle->SetOptTitle(0);
      if (option==0) {
	plot_canvas.plot()->Divide(1,3);
      }
      else {
	plot_canvas.plot()->Divide(1,1);
      }
      plot_canvas.adjustPads();
    }
    else {
      gStyle->SetOptTitle(0);
    }

    plot_canvas.setTitle(m_title);
    if (module_names.size()==1) {
      plot_canvas.setName(module_names.front() );
    }
    else {
      plot_canvas.setName( pp0Name(module_names) );
    }
    plot_canvas.setId( data_provider.scanNumber() );

    //    int old_opt_title = gStyle->GetOptTitle();


    //    try {
    initPlot(data_provider, plot_canvas.plot()->GetMother());
      
    unsigned int module_i=0;
    for (std::vector< std::string >::const_iterator module_iter=module_names.begin();
	 module_iter != module_names.end();
	 module_iter++, module_i++) {

      unsigned int pad_i=module_i;
      std::unique_ptr<TH2> a_h2(get2DHisto(data_provider, *module_iter, index, front_end));
      TH2 *h2 = a_h2.get();

      if ( ((module_names.size()==1 && option==0) || option==2) && (plot_canvas.plot()->GetPad(pad_i))  ) {
	pad_i++;
	plot_canvas.plot()->cd(pad_i);

	if (a_h2.get()) {
	  h2->SetBit(kCanDelete);
	  h2->SetDirectory(0);
	  h2->SetStats(0);
	  a_h2.release();
	  h2->Draw("colz");
	}
	enhanceMap(data_provider, *module_iter, plot_canvas.plot()->GetPad(pad_i),h2);
	if (module_names.size()>0) showModuleName(*module_iter, plot_canvas.plot()->GetPad(pad_i));
	plot_canvas.plot()->GetPad(pad_i)->Modified();
      }
      if ( (option==0 || option==1) && (plot_canvas.plot()->GetPad(pad_i))  ) {
	pad_i++;
	TH1 *h1=getProjection(data_provider, *module_iter, h2, option);
	plot_canvas.plot()->cd(pad_i);
	if (h1) {
	  h1->SetBit(kCanDelete);
	  h1->SetDirectory(0);
	  h1->SetStats(1);
	  h1->Draw();//Copy();
	}
	enhanceProjection(data_provider, *module_iter, plot_canvas.plot()->GetPad(pad_i),h1);
	if (module_names.size()>0) showModuleName(*module_iter, plot_canvas.plot()->GetPad(pad_i));
	plot_canvas.plot()->GetPad(pad_i)->Modified();
      }

      if ( ((module_names.size()==1 && option==0) || option==3) && (plot_canvas.plot()->GetPad(pad_i))  ) {
	pad_i++;

	TObject *object=getPixelGraph(data_provider, *module_iter, h2, option);
	plot_canvas.plot()->cd(pad_i);
	if (object) {
	  object->SetBit(kCanDelete);
	  //	if (object->InheritsFrom(TH1::Class())) {
	  //	  static_cast<TH1 *>(object)->SetDirectory(0);
	  //	}
	  object->Draw("AP");
	}
	enhancePixelGraph(data_provider, *module_iter, plot_canvas.plot()->GetPad(pad_i),object);
	if (module_names.size()>0) showModuleName(*module_iter, plot_canvas.plot()->GetPad(pad_i));
	plot_canvas.plot()->GetPad(pad_i)->Modified();
	
      }

    }
    finish(data_provider, module_names, plot_canvas.plot()->GetMother());
    plot_canvas.update();
  }

  TH1 *ModuleMapVisualiser::getProjection(IScanDataProvider &data_provider, const std::string &module_name, TH2 *h2,int option)
  {
 
    PixA::HistoBinning binning(getBinning(h2,1,1));
    if (binning.min()<binning.max() && binning.nBins()>0) {
      if (binning.nBins()==1) {
        double width=binning.max()-binning.min();
        if (width<=0) width=1;
        binning=PixA::HistoBinning(3,binning.min()-width,binning.min()+2*width);
      }
    //    std::cout << "INFO [ModuleMapVisualiser::getProjection] binning : " << binning.min() << " -  " << binning.max() << " / " << binning.nBins() << std::endl;
    return getProjection(h2,binning, 1,1,option);
    }
    else {
       return NULL;
    }
  }


  PixA::HistoBinning ModuleMapVisualiser::getBinning(TH2 *h2, unsigned int mask_stages, unsigned int total_mask_stages) 
  {
    return PixA::guessHistoBinningForMapProjection(h2,
						   -1,
						   PixA::PixelTypeMask_t::all(),
						   PixA::NoReference(),
						   mask_stages, 
						   total_mask_stages);
  }

  TH1 *ModuleMapVisualiser::getProjection(TH2 *h2,
					  const PixA::HistoBinning &binning,
					  unsigned int mask_stages,
					  unsigned int total_mask_stages,
					  int option)
  {
    if (!h2) return NULL;
    //    PixA::HistoBinning binning=getBinning(h2,mask_stages, total_mask_stages);
    std::vector<unsigned short> types;
    std::vector<std::string> name_ext;
    std::vector<std::string> title_ext;
    std::vector<unsigned short> color;
    std::vector<unsigned short> dash_type;

    types.push_back(PixA::PixelTypeMask_t::all());
    name_ext.push_back("");
    title_ext.push_back("");
    color.push_back(5);
    dash_type.push_back(3466);


    PixGeometry geo(nRows(h2),nColumns(h2));  

    if (geo.pixType()==PixGeometry::FEI2_CHIP ||  geo.pixType()==PixGeometry::FEI2_MODULE) {
      if (option==0) {
	// Claus' grouping :
	types.push_back(PixA::PixelTypeMask_t::kInterGangedPixel | PixA::PixelTypeMask_t::kLongInterGangedPixel | PixA::PixelTypeMask_t::kLongPixel
			| PixA::PixelTypeMask_t::kGangedPixel | PixA::PixelTypeMask_t::kLongGangedPixel);
	name_ext.push_back("_special");
	title_ext.push_back("(long,inter-ganged)");
	color.push_back(6);
	dash_type.push_back(3456);
	
	types.push_back(PixA::PixelTypeMask_t::kGangedPixel | PixA::PixelTypeMask_t::kLongGangedPixel);
	name_ext.push_back("_ganged");
	title_ext.push_back("(ganged)");
	color.push_back(4);
	dash_type.push_back(3466);
      }
      else {
	types.push_back(PixA::PixelTypeMask_t::kLongPixel
			| PixA::PixelTypeMask_t::kGangedPixel | PixA::PixelTypeMask_t::kInterGangedPixel 
			| PixA::PixelTypeMask_t::kLongGangedPixel | PixA::PixelTypeMask_t::kLongInterGangedPixel);
	title_ext.push_back("(inter-ganged)");
	name_ext.push_back("_interGanged");
	color.push_back(191);
	dash_type.push_back(3456);
	
	types.push_back(PixA::PixelTypeMask_t::kLongPixel
			| PixA::PixelTypeMask_t::kGangedPixel 
			| PixA::PixelTypeMask_t::kLongGangedPixel | PixA::PixelTypeMask_t::kLongInterGangedPixel);
	title_ext.push_back("(long)");
	name_ext.push_back("_long");
	color.push_back(6);
	dash_type.push_back(3456);
	
	
	types.push_back(PixA::PixelTypeMask_t::kLongGangedPixel | PixA::PixelTypeMask_t::kLongInterGangedPixel | PixA::PixelTypeMask_t::kGangedPixel);
	title_ext.push_back("(long inter-ganged)");
	name_ext.push_back("longInterGanged");
	color.push_back(106);
	dash_type.push_back(3466);
	
	types.push_back(PixA::PixelTypeMask_t::kGangedPixel
			| PixA::PixelTypeMask_t::kLongGangedPixel);
	title_ext.push_back("(ganged)");
	name_ext.push_back("_ganged");
	color.push_back(4);
	dash_type.push_back(3456);
	
	
	types.push_back(PixA::PixelTypeMask_t::kLongGangedPixel);
	title_ext.push_back("(long ganged)");
	name_ext.push_back("_longGanged");
	color.push_back(109);
	dash_type.push_back(3456);
      }
    }else if(geo.pixType()==PixGeometry::FEI4_CHIP ||  geo.pixType()==PixGeometry::FEI4_MODULE) {
      //if (option==0 || option==1) { // FE-I4 only has normal and long, nothing to group...
	types.push_back(PixA::PixelTypeMask_t::kLongPixel);
	title_ext.push_back("(long)");
	name_ext.push_back("_long");
	color.push_back(6);
	dash_type.push_back(3456);
	//}
    }
    assert(types.size() == title_ext.size() );
    assert(types.size() == name_ext.size() );
    assert(types.size() == color.size() );
    assert(types.size() == dash_type.size() );

    TH1Stack *h1_stack=new TH1Stack(h2->GetName(),h2->GetTitle(),
				    binning.nBins(), binning.min(), binning.max());

    //int chip=0;
    //if (h2->GetNbinsX()==PixelCoord_t::s_nCol && h2->GetNbinsY()==PixelCoord_t::s_nRow) {
    if(geo.pixType()==PixGeometry::FEI4_CHIP ||  geo.pixType()==PixGeometry::FEI2_CHIP){
      //chip=0;
    }
    //else if (h2->GetNbinsX()==PixelCoord_t::s_nCol * PixelCoord_t::s_nChip/2 && h2->GetNbinsY()==PixelCoord_t::s_nRow *2) {
    else if(geo.pixType()==PixGeometry::FEI4_MODULE || geo.pixType()==PixGeometry::FEI2_MODULE) {
      //chip=-1;
    }
    else {
      //chip=-2;
    }


    TH1 *h1_unset=NULL;

    for (unsigned int type_i=0; type_i<types.size(); type_i++) {

      std::string full_histo_name( h2->GetName() );
      full_histo_name += name_ext[type_i];
      std::string full_histo_title( h2->GetTitle() );
      full_histo_title += " ";
      full_histo_title += title_ext[type_i];
      
      TH1 *h1;
      if (type_i==0) {
	h1=h1_stack;
      }
      else {
 	if (h1_unset) {
 	  h1=h1_unset;
 	  //	  h1->Reset();
 	  h1->SetName(full_histo_name.c_str());
 	  h1->SetTitle(full_histo_title.c_str());
 	  h1_unset=NULL;
 	}
 	else {
	  h1=h1_stack->NewH1(full_histo_name.c_str(),full_histo_title.c_str());
	}
      }
      if (mask_stages == total_mask_stages) {
      	h1_unset=h1;
      }
      else {
	full_histo_name += "_unScanned";
	full_histo_title += " (not scanned)";
	if (!h1_unset) {
	  h1_unset=h1_stack->NewH1(full_histo_name.c_str(),full_histo_title.c_str());
	}
	else {
	  h1_unset->SetName(full_histo_name.c_str());
	  h1_unset->SetTitle(full_histo_title.c_str());
	}
      }
      
      PixA::fillHistoFromMap(h1,
			     h1_unset,
			     h2,
			     -1,
			     types[type_i],
			     PixA::NoReference(),
			     mask_stages,
			     total_mask_stages);

      if (h1->GetXaxis() && h2->GetZaxis()) {
	h1->GetXaxis()->SetTitle(h2->GetZaxis()->GetTitle());
      }
      if (h1->GetYaxis()) {
	h1->GetYaxis()->SetTitle("Number of Pixel");
      }

      h1->SetFillStyle(1001);
      h1->SetFillColor(color[type_i]);
      if (h1_unset!=h1) {
	if (h1_unset->GetEntries()>0.) {
	  h1_unset->SetFillStyle(dash_type[type_i]);
	  h1_unset->SetFillColor(color[type_i]);
	  h1_unset=NULL;
	}
      }
      else {
	h1_unset=NULL;
      }
    }
    if (h1_unset) {
      h1_stack->DeleteBottomH1();
    }
    return h1_stack;
  }

  void ModuleMapVisualiser::enhanceProjection(IScanDataProvider &, const std::string &, TVirtualPad* plot_pad, TH1* /* h1 */) {
    if (plot_pad) {
      plot_pad->SetLogy(1);
    }
  }

  TObject *ModuleMapVisualiser::getPixelGraph(IScanDataProvider &, const std::string &, TH2 *h2, int option) 
  {
    return getPixelGraph(h2,1,1,option);
  }


  TObject *ModuleMapVisualiser::getPixelGraph(TH2 *h2, unsigned int mask_stages, unsigned int total_mask_stages, int option) 
  {
    if (!h2) return nullptr;
//     unsigned short types[3]={PixA::PixelTypeMask_t::all()
//                              & ~(PixA::PixelTypeMask_t::kGangedPixel | PixA::PixelTypeMask_t::kInterGangedPixel
//                                  |  PixA::PixelTypeMask_t::kLongGangedPixel | PixA::PixelTypeMask_t::kLongInterGangedPixel),
//                              PixA::PixelTypeMask_t::kGangedPixel | PixA::PixelTypeMask_t::kInterGangedPixel,
// 			     PixA::PixelTypeMask_t::kLongGangedPixel | PixA::PixelTypeMask_t::kLongInterGangedPixel};

//     const char *name_ext[3]={"","_ganged","_longGanged"};
//     const char *title_ext[3]={"","(inter-,ganged)","(long inter-, ganged)"};


    std::vector<unsigned short> types;
    std::vector<std::string> name_ext;
    std::vector<std::string> title_ext;
    std::vector<unsigned short> color;
    std::vector<unsigned short> dash_type;
    std::vector< std::pair<unsigned short,unsigned short> > marker_style;

    types.push_back(PixA::PixelTypeMask_t::all()
		      & ~(  PixA::PixelTypeMask_t::kLongPixel
			  | PixA::PixelTypeMask_t::kGangedPixel | PixA::PixelTypeMask_t::kInterGangedPixel
			  | PixA::PixelTypeMask_t::kLongGangedPixel | PixA::PixelTypeMask_t::kLongInterGangedPixel));
    name_ext.push_back("");
    title_ext.push_back("");
    color.push_back(1);
    dash_type.push_back(3466);
    marker_style.push_back( std::make_pair<unsigned short, unsigned short>(1,6) );

    PixGeometry geo(nRows(h2),nColumns(h2));
    if (geo.pixType()==PixGeometry::FEI2_CHIP || geo.pixType()==PixGeometry::FEI2_MODULE) {
      if (option==0) {
	// Claus' grouping :
	types.push_back(PixA::PixelTypeMask_t::kInterGangedPixel | PixA::PixelTypeMask_t::kLongInterGangedPixel | PixA::PixelTypeMask_t::kLongPixel);
	name_ext.push_back("_special");
	title_ext.push_back("(long,inter-ganged)");
	color.push_back(6);
	dash_type.push_back(3456);
	marker_style.push_back( std::make_pair<unsigned short, unsigned short>(6,6) );
	
	types.push_back(PixA::PixelTypeMask_t::kGangedPixel | PixA::PixelTypeMask_t::kLongGangedPixel);
	name_ext.push_back("_ganged");
	title_ext.push_back("(ganged)");
	color.push_back(4);
	dash_type.push_back(3466);
	marker_style.push_back( std::make_pair<unsigned short, unsigned short>(6,6) );
	// JGK: inconsistent definition - copied version from ModuleMapVisualiser::getProjection and removed duplication
	//    unsigned short color[3]={5,6,4};
	//    unsigned short dash_type[3]={3466,3456,3465};
	/*
	types.push_back(PixA::PixelTypeMask_t::kGangedPixel | PixA::PixelTypeMask_t::kInterGangedPixel);
	name_ext.push_back("_ganged");
	title_ext.push_back("(inter-,ganged)");
	color.push_back(6);
	dash_type.push_back(3456);
	marker_style.push_back( std::make_pair<unsigned short, unsigned short>(6,6) );
	
	types.push_back(PixA::PixelTypeMask_t::kLongGangedPixel | PixA::PixelTypeMask_t::kLongInterGangedPixel);
	name_ext.push_back("_longGanged");
	title_ext.push_back("(long inter-, ganged)");
	color.push_back(4);
	dash_type.push_back(3466);
	marker_style.push_back( std::make_pair<unsigned short, unsigned short>(6,6) );
	*/
      }
      else {
	
	types.push_back(PixA::PixelTypeMask_t::kInterGangedPixel);
	title_ext.push_back("(inter-ganged)");
	name_ext.push_back("_interGanged");
	color.push_back(191);
	dash_type.push_back(3456);
	marker_style.push_back( std::make_pair<unsigned short, unsigned short>(6,6) );
	
	types.push_back(PixA::PixelTypeMask_t::kLongPixel);
	title_ext.push_back("(long)");
	name_ext.push_back("_long");
	color.push_back(6);
	dash_type.push_back(3456);
	marker_style.push_back( std::make_pair<unsigned short, unsigned short>(6,6) );
	
	types.push_back(PixA::PixelTypeMask_t::kLongInterGangedPixel);
	title_ext.push_back("(long inter-ganged)");
	name_ext.push_back("longInterGanged");
	color.push_back(106);
	dash_type.push_back(3466);
	marker_style.push_back( std::make_pair<unsigned short, unsigned short>(6,6) );
	
	
	types.push_back(PixA::PixelTypeMask_t::kGangedPixel);
	title_ext.push_back("(ganged)");
	name_ext.push_back("_ganged");
	color.push_back(4);
	dash_type.push_back(3456);
	marker_style.push_back( std::make_pair<unsigned short, unsigned short>(6,6) );
	
	
	types.push_back(PixA::PixelTypeMask_t::kLongGangedPixel);
	title_ext.push_back("(long ganged)");
	name_ext.push_back("_longGanged");
	color.push_back(109);
	dash_type.push_back(3456);
	marker_style.push_back( std::make_pair<unsigned short, unsigned short>(6,6) );
      }
    }else if (geo.pixType()==PixGeometry::FEI4_CHIP ||  geo.pixType()==PixGeometry::FEI4_MODULE) {
      //if (option==0 || option==3) {// FE-I4 only has normal and long, nothing to group...
	types.push_back(PixA::PixelTypeMask_t::kLongPixel);
	name_ext.push_back("_special");
	title_ext.push_back("(long)");
	color.push_back(6);
	dash_type.push_back(3456);
	marker_style.push_back( std::make_pair<unsigned short, unsigned short>(6,6) );
	//}
    }
    assert(types.size() == title_ext.size() );
    assert(types.size() == name_ext.size() );
    assert(types.size() == color.size() );
    assert(types.size() == dash_type.size() );
    assert(types.size() == marker_style.size() );


    //    unsigned short color[3]={1,6,4};
    //    unsigned short marker_style[3][2]={{1,6},{6,6},{6,6}};

    TGraphStack *graph_stack=new TGraphStack(h2->GetName(), h2->GetTitle());
    if (geo.pixType()==PixGeometry::FEI4_CHIP || geo.pixType()==PixGeometry::FEI2_CHIP){
      //h2->GetNbinsX()==PixelCoord_t::s_nCol && h2->GetNbinsY()==PixelCoord_t::s_nRow) {
      std::stringstream title;
      title << "Row + Column #times "<< geo.nChipRow();//PixelCoord_t::s_nRow;
      graph_stack->SetXaxisTitle(title.str().c_str());
    }
    else{// if (h2->GetNbinsX()==PixelCoord_t::s_nCol*PixelCoord_t::s_nChip/2 && h2->GetNbinsY()==PixelCoord_t::s_nRow*2) {
      std::stringstream title;
      title << "Row + Column #times "<< geo.nChipRow() << " + Chip #times " << geo.nChipPixel();
      //title << "Row + Column #times "<< PixelCoord_t::s_nRow << " + Chip #times " << PixelCoord_t::s_nPix;
      graph_stack->SetXaxisTitle(title.str().c_str());
    }
    if (h2->GetZaxis()) {
      graph_stack->SetYaxisTitle(h2->GetZaxis()->GetTitle());
    }

    Average_t av_total;
    for (unsigned int type_i=0; type_i<types.size(); type_i++) {

      std::string full_graph_name( h2->GetName() );
      full_graph_name += name_ext[type_i];
      std::string full_graph_title( h2->GetTitle() );
      full_graph_title += " ";
      full_graph_title += title_ext[type_i];

      Average_t av;
      std::pair<TGraph *, TGraph* > pixel_graphs = PixA::pixelGraph(h2,
								    -1,
								    types[type_i],
								    &av,
								    PixA::NoReference(),
								    mask_stages,
								    total_mask_stages);
      av_total.add(av);

      if (pixel_graphs.first) {
	pixel_graphs.first->SetMarkerColor(color[type_i]);
	pixel_graphs.first->SetMarkerStyle( (pixel_graphs.first->GetN() > PixelCoord_t::s_nPix ? marker_style[type_i].first : marker_style[type_i].second) );
	pixel_graphs.first->SetLineColor(color[type_i]);
	pixel_graphs.first->SetName(full_graph_name.c_str());
	pixel_graphs.first->SetTitle(full_graph_name.c_str());
	graph_stack->AddGraph(pixel_graphs.first);
      }

      if (pixel_graphs.second) {
	full_graph_name += "_unScanned";
	full_graph_title += " (not scanned)";

	pixel_graphs.second->SetMarkerColor(2);
	pixel_graphs.second->SetMarkerStyle(6);
	pixel_graphs.second->SetLineColor(2);
	pixel_graphs.second->SetName(full_graph_name.c_str());
	pixel_graphs.second->SetTitle(full_graph_name.c_str());
	graph_stack->AddGraph(pixel_graphs.second);
      }
    }
    av_total.calculate();
    float range = av_total.max()  - av_total.min();
    if (range<=0.) {
	range=av_total.max()*.2;
        if (range<=0.) {
	   range=1;
        }
    }
    //    std::cout << "INFO [getPixelGraph] range = " << av_total.min() << " - " << av_total.max() << " -> " << range << std::endl;
    if (range>0) {
      graph_stack->SetMinimum(av_total.min()-range*.05);
      graph_stack->SetMaximum(av_total.max()+range*.05);
    }
    return graph_stack;
  }

  std::string ModuleMapVisualiser::pp0Name(const std::vector<std::string> &name_list) 
  {
    if (name_list.size()==0) return "";
    std::string pp0_name;
    for (std::vector<std::string>::const_iterator module_iter = name_list.begin();
	 module_iter != name_list.end();
	 module_iter++) {
      std::string::size_type pos = module_iter->rfind("_");
      if (   pos == std::string::npos) {
	pp0_name="";
	break;
      }
      if (module_iter->length() - pos>4 || module_iter->length() - pos<3) {
	pp0_name="";
	break;
      }
      if ((*module_iter)[pos+1]!='M') {
	pp0_name="";
	break;
      }
      if ((!pp0_name.empty() && module_iter->compare(0,pos,pp0_name)!=0)) {
	pp0_name="";
	break;
      }

      if (   pos == std::string::npos
          || module_iter->length() - pos>4 || module_iter->length() - pos<3
          || (*module_iter)[pos+1]!='M'
	  || (!pp0_name.empty() && module_iter->compare(0,pos,pp0_name)!=0)) {
	pp0_name="";
	break;
      }
      else if (pp0_name.empty()) {
	pp0_name=std::string(*module_iter,0,pos);
      }
    }
    if (pp0_name.empty()) {
      for (std::vector<std::string>::const_iterator module_iter = name_list.begin();
	   module_iter != name_list.end();
	   module_iter++) {
	pp0_name += *module_iter;
	pp0_name +=", ";
      }
      if (pp0_name.empty()) {
	pp0_name.erase(pp0_name.size()-2,2);
      }
    }
    return pp0_name;
  }

  void ModuleMapVisualiser::showModuleName(const std::string &module_name, TVirtualPad *a_pad) const
  {
    if (!module_name.empty() && a_pad) {
      TText *a_text=new TText(a_pad->GetLeftMargin()+.03,1.-a_pad->GetTopMargin()-.02,module_name.c_str());
      a_text->SetNDC(kTRUE);
      a_text->SetTextSize(a_text->GetTextSize()*1.2);
      a_text->SetTextAlign(13);
      a_pad->cd();
      a_text->Draw();
    }
  }

}
