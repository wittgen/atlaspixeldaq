#include <ConfigWrapper/Regex_t.h>
#include <string>
#include <iostream>
#include <cerrno>

int main(int argc, char **argv)
{
  std::string a_name("/CONNECTIVITY_TEST - 13/Phase_0/HistoSummary");
  std::string a_pattern("/HistoSummary$");
  if (argc>1) {
    a_name = argv[1];
  }
  if (argc>2) {
    a_pattern = argv[2];
  }
  
 PixA::Regex_t *pattern=new PixA::Regex_t(a_pattern);
  std::cout << " current error = " << errno << std::endl;
  std::cout << "match " << a_name << " matches " << a_pattern << " ? " << pattern->match(a_name) << "." << std::endl;
  
}
