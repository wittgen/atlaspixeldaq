#ifndef _VisualisationUtil_h_
#define _VisualisationUtil_h_

#include <TVirtualPad.h>
//class TVirtualPad;
#include <string>

namespace PixA {
  class PlotCanvas_t
  {
  public:
    PlotCanvas_t(unsigned int n_objects, unsigned int n_y=0);

    TVirtualPad *title() { return m_titlePad;}
    TVirtualPad *plot() { return m_plotPad;}

    /** Set and draw the canvas title.
     */
    void setTitle(const std::string &title);

    /** Set and draw the name of object which is displayed on the canvas.
     * The name is displayed below the title.
     */
    void setName(const std::string &name);

    /** Set and draw the Id.
     * The Id could be the scan number
     */
    void setId(unsigned int id);

    /** Set and draw the name of object which is displayed on the canvas.
     * The name is displayed below the title.
     */
    void setSubTitle(const std::string &sub_title);

    /** Get, clear and divide a canvas.
     */
    static PlotCanvas_t canvas(unsigned int n_objects, unsigned int n_y=0) {   return PlotCanvas_t (n_objects, n_y); }

    /** Adjust margins.
     */
    void adjustPads();

    /** Redraw the canvas.
     */
    void update() {
      m_plotPad->Modified();
      m_titlePad->Modified();
      m_titlePad->GetMother()->Modified();
      m_titlePad->GetMother()->Update();
    }

    enum EPaletteType {kMABlackRedYellow, kBlackRed, kRootRainbow, kTurboDAQ, kBlackWhite };
    static void setPalette(EPaletteType type);

    static void incCanvas() { s_maxCanvasCounter++; s_canvasCounter=s_maxCanvasCounter;}
    static void setCounter(unsigned int counter) { s_canvasCounter = counter; }
    static unsigned int count() {return s_canvasCounter; }
  private:
    TVirtualPad *m_plotPad;
    TVirtualPad *m_titlePad;
    static unsigned int s_canvasCounter;
    static unsigned int s_maxCanvasCounter;
    static bool         initROOT();
    static bool         s_initialised;
  };

}
#endif
