#ifndef _PixScanHistoVisualiser_h_
#define _PixScanHistoVisualiser_h_

#include "ISimpleVisualiser.h"
#include "IVisualisationKit.h"
#include <DataContainer/HistoRef.h>
#include <ConfigWrapper/ScanConfig.h>
#include <ConfigWrapper/ConfObjUtil.h>

#include <DataContainer/MakePixScanHistoName.h>
#include <vector>

class TCanvas;
class TObject;
class TVirtualPad;
class TH1;

namespace PixA {

  typedef TH1 Histo;

  class PixScanHistoVisualiser : public ISimpleVisualiser {
  public:

    PixScanHistoVisualiser(const std::string &histo_name,
			   bool can_visualise_one_fe)
      : m_histoName(histo_name),
	m_canVisualiseOneFe(can_visualise_one_fe)
      { m_loopUsed.resize(3);}

    /** Get the name of the visualiser.
     */
    const std::string &name() const { return s_name;}

    /** Returns true, if the visualiser can optionally display only one front-end.
     */
    bool canVisualiseOneFrontEnd() const {return m_canVisualiseOneFe;}

    /** Returns the maximum number of modules the visualiser can display simultaneously.
     */
    unsigned int canVisualiseNModules() const { return 8; }

    /** Fill the histogram info structure.
     * The structure contains the index ranges for each dimension.
     * @sa HistoInfo_t
     */
    void fillIndexArray(IScanDataProvider &data_provider, const std::vector<std::string> &module_names, HistoInfo_t &info) const;

    /** Perform the chosen visualisation for the given front_end and the given index set.
     * @param option the visualisation option.
     * @param index the index set to specify the histogram in the multi dimensional histogram collection.
     * @param front_end the number of the front_end (0-15) which should be visualised or -1 for all front-ends
     * The front-end parameter will be ignored if canVisualiseOneFrontEnd returns false.
     */
    void visualise(IScanDataProvider &data_provider, const std::vector<std::string> &module_names, int , const Index_t &index, int front_end);


    /** Visualise the histograms of an entire loop.
     */
    void visualiseLoop(IScanDataProvider &data_provider,
		       const std::vector<std::string> &module_names,
		       unsigned int loop,
		       const Index_t &index,
		       int front_end);

  protected:
    /** function which will be called before starting to plot.
     */
    virtual void initPlot() {}

    /** function which could be used by derived classes to extend the plot.
     * @param plot_pad the pad
     * @param scan_config the configuration associated to the pad
     * @param object the object which was plotted (most likely a TH2 or TH1.)
     * like overlaying the optimum boc parameters.
     */
    virtual void enhancePlot(TVirtualPad *plot_pad, IScanDataProvider &data_provider, const std::string &module_name, const Index_t &index, TObject *obj );

    /** Could be overloaded to make custom titles.
     */
    virtual std::string makeTitle(IScanDataProvider &data_provider, const std::string &histo_title);

    /** Will be called for each histogram before actually plotting anything.
     */
    virtual void prepare(IScanDataProvider & /*data_provider*/,
                         const std::string &/*module_name*/, 
	                 const Histo * /* histo */,
                         unsigned int /* front_end */) {}

    /** Could be overloaded to create MATH2F instead of TH2.
     * @return a new ROOT TObject or the same hisotgram (in case Histo is a ROOT histogram).
     */
    virtual TObject *transform(IScanDataProvider &data_provider,
			       const std::string &module_name,
			       const Histo *histo,
			       const Index_t &index,
			       unsigned int front_end);

    /** Produce the text which is written for each loop on top of the histogram column
     * @param index_i the index for the index array (loop or level index).
     * @param elm_i the value index[index_i] or the index at the given level.
     * @return a text characterising the value at the given level.
     */
    virtual std::string makeLoopText( IScanDataProvider &data_provider, unsigned int index_i, unsigned int elm_i);

    /** Search for the level index which matches the given var_name.
     */
    unsigned int searchIndex(IScanDataProvider &data_provider, const std::string &var_name);

    /** Return the histogram name.
     */
    const std::string &histoName() const {return m_histoName;}

    /** Return a sub-title for the canvas.
     */
    virtual std::string subTitle(IScanDataProvider & /*data_provider*/) const { return "";}

    /** Make a sub title from the loop variables.
     */
    std::string makeSubTitle(IScanDataProvider &data_provider, const Index_t &index);

    /** Return true if the given loop is "used" by a histogram axis.
     */
    bool isLoopUsed(unsigned int loop_i) const { assert(loop_i<m_loopUsed.size()); return m_loopUsed[loop_i]; }

    /** Get the information about the dimensionality of the current histogram array.
     */
    const HistoInfo_t &info() const { return m_info;}

  private:
    //    void makeHistoName(const PixA::ScanConfigRef &scan_config, PixA::HistoRef &histo, std::vector<std::string> &histo_name) const;

    /** Mark all loops which are used for histogram axes.
     */
    void markUsedLoops(TObject *obj, const PixA::ConfigHandle &scan_config) ;

    std::string m_histoName;
    bool m_canVisualiseOneFe;
    static std::string s_name;

    std::vector<bool> m_loopUsed; /**< Set to true for all loops which are used for a histogram axis.*/
    mutable HistoInfo_t m_info; /**< caches the number of histos. */

    /** Auxillary class which holds additional information about a pix scan loop variable.
     */
    class VarInfo_t
    {
    public:
      VarInfo_t() : m_conversionFactor(1.) {}

      VarInfo_t(const std::string &unit_name, float conversion_factor=1.)
	: m_unitName(unit_name), m_conversionFactor(conversion_factor) {}

      VarInfo_t(const std::string &unit_name, float conversion_factor, const std::string &var_name)
	: m_unitName(unit_name), m_varName(var_name), m_conversionFactor(conversion_factor) {}

      const std::string &unitName() const {return m_unitName;}

      bool  hasVarName() const {return !m_varName.empty();}
      const std::string &varName() const {return m_varName;}

      bool hasConversionFactor() const {return m_conversionFactor!=1.;}
      const float &conversionFactor() const {return m_conversionFactor;}

    private:
      std::string m_unitName;
      std::string m_varName;
      float       m_conversionFactor;
    };

    /** Get additional information about a parmeter.
     */
    static const VarInfo_t &varInfo(const std::string &param_name);

    static std::map<std::string, VarInfo_t> s_varList;
  };



  class PixScanHistoVisualisationKit : public IVisualisationKit 
  {
  public:
    PixScanHistoVisualisationKit();

    unsigned int priority() const {return IVisualisationKit::fallBackOnly;}

    /** Create a visualiser for the given inputs or return NULL.
     */
    virtual IVisualiser *visualiser(const std::string &/*histo_name*/,
				    const std::vector<std::string> &/*folder_hierarchy*/) const;

  };


}

#endif
