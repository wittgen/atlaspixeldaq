/* Dear emacs, this is -*-c++-*- */
#ifndef _PixA_IScanDataProvider_h_
#define _PixA_IScanDataProvider_h_

#include <ConfigWrapper/ConfigHandle.h>
#include <vector>
#include "Histo.h"
class HistoInfo_t;

namespace PixA {


  typedef unsigned int SerialNumber_t;
  typedef std::vector<unsigned int> Index_t;

  class ConnectivityRef;

  /** Helper class which provides scan results, parameters and important configurations.
   * The helper class will provide access to the pix scan histograms, the scan configuration,
   * the module and the BOC configurations.
   * The data provider are intended for giving access to the histograms to the visualiser.
   */
  class IScanDataProvider
  {
  public:

    virtual ~IScanDataProvider() {}

    /** Return the serial number of the currently provided scan data.
     */
    virtual SerialNumber_t scanNumber() const = 0;

    /** Return the connectivity used for the current scan.
     * The connectivity may be invalid.
     */
    virtual const PixA::ConnectivityRef &connectivity() = 0;

    /** Get the dimensions of the pix scan histogram of the given name for the specified module.
     * @param connectivity_name the connectivity name ( or production name in case of very old result files) of the module.
     * @param histo_name the name of the histogram e.g. RAW_DATA_DIFF2, OCCUPANCY, etc. .
     * @param info will be filled with the minimum and maximum histogram indices.
     */
    virtual void numberOfHistos(const std::string &connectivity_name,
				const std::string &histo_name,
				HistoInfo_t &info) = 0;

    /** Get the pix scan histogram of the given name for the specified module.
     * @param connectivity_name the connectivity name ( or production name in case of very old result files) of the module.
     * @param histo_name the name of the histogram e.g. RAW_DATA_DIFF2, OCCUPANCY, etc. .
     * @param index the indices which specify the location in the pix scan histogram tree.
     * @return pointer to a histogram which is owned by the data provider.
     * If the histogram could live longer than the data provider than the histogram needs to be copied.
     */
    virtual const Histo *histogram(const std::string &connectivity_name,
				   const std::string &histo_name,
				   const Index_t &index) = 0;

    /** Get the production name of the given module.
     * @param connectivity_name the connectivity name of the module.
     * @return the production name of the module.
     * For very old data files the name given as connectivity name may actually be 
     * the production name.
     */
    virtual const std::string &prodName(const std::string &connectivity_name) = 0;

    /** Get the parameters of the scan.
     * @return  handle for the scan configuration.
     * NOTE: the returned handle may not be valid.
     */
    virtual const PixA::ConfigHandle scanConfig() = 0;

    virtual const PixA::ConfigHandle scanConfig(std::string &fe_tag) = 0;

    /** Returns the module configuration for the specified module.
     * @param connectivity_name the connectivity name ( or production name in case of very old result files) of the module.
     * @return handle for the module configuration.
     * NOTE: the returned handle may not be valid.
     */
    virtual const PixA::ConfigHandle moduleConfig(const std::string &connectivity_name) = 0;

    /** Returns the configuration of the BOC to which the specified module is attached.
     * @param connectivity_name the connectivity name ( or production name in case of very old result files) of the module.
     * @return handle for the BOC configuration.
     * NOTE: the returned handle may not be valid.
     */
    virtual const PixA::ConfigHandle bocConfig(const std::string &connectivity_name) = 0;

    /** Returns the configuration of the ROD to which the specified module is attached.
     * @param connectivity_name the connectivity name ( or production name in case of very old result files) of the module.
     * @return handle for the BOC configuration.
     * NOTE: the returned handle may not be valid.
     */
    virtual const PixA::ConfigHandle rodConfig(const std::string &connectivity_name) = 0;

    /** Returns the configuration of the PixModuleGroupe to which the specified module is attached.
     * @param connectivity_name the connectivity name ( or production name in case of very old result files) of the module.
     * @return handle for the BOC configuration.
     * NOTE: the returned handle may not be valid.
     */
    virtual const PixA::ConfigHandle moduleGroupConfig(const std::string &connectivity_name) = 0;

    /** Returns the configuration of the tim which is contained in the crate to which the given module is attached.
     * @param connectivity_name the connectivity name ( or production name in case of very old result files) of the module.
     * @return handle for the BOC configuration.
     * NOTE: the returned handle may not be valid.
     */
    virtual const PixA::ConfigHandle timConfig(const std::string &connectivity_name) = 0;

    // @todo needed ?
    //    virtual PixA::ConfigHandle rodConfig(const std::string &connectivity_name) = 0;
  };

}
#endif
