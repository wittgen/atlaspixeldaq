#ifndef _TGraphStack_h_
#define _TGraphStack_h_

#include <TGraph.h>
#include <vector>
#include <string>

/** Container for several Graphs.
 * This object can be used to draw several graphs at once.
 * The graph stack does not add itself to the canvas
 * Thus if the calling code relies on the canvas to
 * delete the objects, the graph stack would not be deleted.
 */
class TGraphStack : public TNamed
{
public:
  TGraphStack(const char *name, const char *title) : TNamed(name,title) {};

  TGraphStack(const TGraphStack &a);

  ~TGraphStack();

  TObject *Clone() const {
    return new TGraphStack(*this);
  }

  void Copy(TObject &dest) const;

  /** Add a graph to the graph stack.
   * The graph which is added last is drawn first, thus it is overpainted by the priorily added graphs.
   */
  void AddGraph(TGraph *a_graph) {
    m_graphList.push_back(a_graph);
  }

  /** Set the minimum value of the first drawn graph
   */
  void SetMinimum(float min_val) { m_min = min_val; }
  
  /** Set the maximum value of the first drawn graph
   */
  void SetMaximum(float max_val) { m_max = max_val; }

  void DeleteBottomGraph();

  void SetXaxisTitle(const char *axis_title) {m_axisTitle[0]=axis_title;}

  void SetYaxisTitle(const char *axis_title) {m_axisTitle[1]=axis_title;}

  /** Draw all the graphs in the list and clear the list.
   * If the bit kCanDelete is set, this object will be
   * deleted after all the graphs are drawn.
   */
  void  Draw(Option_t* option);

private:
  void clear();
  float m_min;
  float m_max;
  std::string m_axisTitle[2];
  std::vector<TGraph *> m_graphList;
  using TNamed::Clone;
};

#endif
