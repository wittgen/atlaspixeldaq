#ifndef _VisualisationFactoryImpl_h_
#define _VisualisationFactoryImpl_h_

#include <VisualisationFactory.h>

#include <ConfigWrapper/Regex_t.h>

namespace PixA {

  typedef std::vector< std::pair < Regex_t *, IVisualisationKit *> > KitList_t;

  class VisualisationFactoryImpl : public  VisualisationFactory
  {
    friend class VisualisationFactory;
  protected:
    VisualisationFactoryImpl();

    IVisualiser *_visualiser(const VisualisationKitSubSet_t &kits,
			     const std::string &histo_name,
			     const std::vector<std::string> &folder_hierarchy);

    void _registerKit(const std::string &pattern, IVisualisationKit *kit);

    void _getKits(const std::string &histo_name, const std::vector<std::string> &folder_hierarchy,  VisualisationKitSubSet_t &kits );

    void _getKits(const std::string &histo_name,  VisualisationKitSubSet_t &kits );

  private:
    static std::string makeString(const std::string &histo_name, const std::vector<std::string> &folder_hierarchy);
    IVisualiser *createVisualisationList(std::vector<std::pair< unsigned int, IVisualiser *> > &visualiser_list);

     KitList_t m_visualisationKitList;

    static VisualisationFactory *s_instance;
  };

}

#endif
