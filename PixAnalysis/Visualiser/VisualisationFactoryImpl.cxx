#include "VisualisationFactoryImpl.h"
#include "VisualisationKitSubSet_t.h"
#include "IVisualisationKit.h"
#include "IVisualiser.h"
#include "VisualisationList.h"
#include <ConfigWrapper/Connectivity.h>


namespace PixA {

  VisualisationFactoryImpl::VisualisationFactoryImpl() {}

  IVisualiser *VisualisationFactoryImpl::_visualiser(const VisualisationKitSubSet_t &kits,
						     const std::string &histo_name,
						     const std::vector<std::string> &folder_hierarchy)

  {
    std::vector<std::pair<unsigned int, IVisualiser *> > visualiser_list;
    for (KitSubSetList_t::const_iterator kit_iter=kits.begin();
	 kit_iter != kits.end();
	 kit_iter++) {
	IVisualiser *a_visualiser = (*kit_iter)->visualiser(histo_name, folder_hierarchy);
	if (a_visualiser) {
	  if (   !visualiser_list.empty() ) {
	    if (   visualiser_list.back().first<=IVisualisationKit::fallBackOnlyMax
	        && (*kit_iter)->priority()>visualiser_list.back().first) {
	      delete visualiser_list.back().second;
	      //	    visualiser_list.back().second = NULL;
	      visualiser_list.pop_back();
	    }
	    else if ((*kit_iter)->priority() <= IVisualisationKit::fallBackOnlyMax) {
	      delete a_visualiser;
	      continue;
	    }
	  }
	  visualiser_list.push_back(std::make_pair((*kit_iter)->priority(), a_visualiser));
	}
    }
    return createVisualisationList(visualiser_list);
  }


  void VisualisationFactoryImpl::_getKits(const std::string &full_histo_name,  VisualisationKitSubSet_t &kits ) {
    kits.clear();
    unsigned int counter =0 ;
    for (KitList_t::const_iterator kit_iter=m_visualisationKitList.begin();
	 kit_iter != m_visualisationKitList.end();
	 kit_iter++, counter++) {
      //     std::cout << counter << " : " << histo_name;
      if (kit_iter->first->match(full_histo_name)) {
	kits.push_back(kit_iter->second);
      }
      //     else {
      //	std::cout << " -----  " << std::endl;
      //      }
    }
  }

  void VisualisationFactoryImpl::_getKits(const std::string &histo_name, const std::vector<std::string> &folder_hierarchy,  VisualisationKitSubSet_t &kits ) {
    std::string full_histo_name = makeString(histo_name,folder_hierarchy);
    _getKits(full_histo_name , kits);
  }

  IVisualiser *VisualisationFactoryImpl::createVisualisationList(std::vector<std::pair< unsigned int, IVisualiser *> > &visualiser_list)
  {
    //    if (visualiser_list.size()>1) {
    //      for (std::vector<std::pair<unsigned int, IVisualiser *> >::iterator vis_iter=visualiser_list.begin() ; vis_iter != visualiser_list.end(); vis_iter++) {
    //	if (vis_iter->first<=IVisualisationKit::fallBackOnlyMax) {
    //	  visualiser_list.erase(vis_iter);
    //	}
    //      }
    //    }
    
    if (visualiser_list.size()>1) {
      std::vector<std::pair<unsigned int, IVisualiser *> >::iterator priority_iter = visualiser_list.end();
      unsigned int priority = 0 ;

      for (std::vector<std::pair<unsigned int, IVisualiser *> >::iterator vis_iter=visualiser_list.begin() ; vis_iter != visualiser_list.end(); vis_iter++) {

	// This list will not support visualiser which provide options on there own.
	//	assert ( vis_iter->second->nOptions() == 1);

	if (vis_iter->first>priority || priority_iter == visualiser_list.end()) {
	  priority_iter = vis_iter;
	  priority = vis_iter->first;
	}
      }

      ;

      VisualisationList *the_visualiser=new VisualisationList( *(priority_iter->second) );
      for (std::vector<std::pair<unsigned int,IVisualiser *> >::iterator vis_iter=visualiser_list.begin();
	   vis_iter != visualiser_list.end(); 
	   vis_iter++) {
	if (vis_iter != priority_iter) {
	  the_visualiser->push_back( *(vis_iter->second) );
	}
      }
      return the_visualiser;
    }
    else if (!visualiser_list.empty()) {
      return visualiser_list.begin()->second;
    }
    else {
      return NULL;
    }
  }

  std::string VisualisationFactoryImpl::makeString(const std::string &histo_name, const std::vector<std::string> &folder_hierarchy) {
    std::string a_string;
    for(std::vector<std::string>::const_iterator folder_iter= folder_hierarchy.begin();
	folder_iter != folder_hierarchy.end();
	folder_iter++) {
      a_string += '/';
      a_string += *folder_iter;
    }
    a_string += '/';
    a_string += histo_name;
    return a_string;
  }

  void VisualisationFactoryImpl::_registerKit(const std::string &pattern, IVisualisationKit *kit) {
    assert(kit);
    std::cout << "DEBUG [VisualisationFactory::registerKit] kit " << static_cast<void *>(kit) << " for pattern " << pattern
	      << " with priority " << kit->priority() << "." << std::endl;
    Regex_t *a_ptr=new Regex_t(pattern);
    m_visualisationKitList.push_back(std::make_pair(a_ptr, kit));
  }

}
