#ifndef _HistoVisualiser_h_
#define _HistoVisualiser_h_

#include "ISimpleVisualiser.h"
#include "IVisualisationKit.h"
#include <DataContainer/HistoRef.h>

class TCanvas;

namespace PixA {

  class HistoVisualiser : public ISimpleVisualiser {
  public:

    HistoVisualiser(const std::string &histo_name) : m_histoName(histo_name) {}
    /** Get the name of the visualiser.
     */
    const std::string &name() const { return s_name;}

    /** Returns true, if the visualiser can optionally display only one front-end.
     */
    bool canVisualiseOneFrontEnd() const {return false;}

    /** Returns the maximum number of modules the visualiser can display simultaneously.
     */
    unsigned int canVisualiseNModules() const { return 1; }


    /** Fill the histogram info structure.
     * The structure contains the index ranges for each dimension.
     * @sa HistoInfo_t
     */
    void fillIndexArray(IScanDataProvider &data_provider, const std::vector<std::string> &module_names, HistoInfo_t &info) const;

    /** Perform the chosen visualisation for the given front_end and the given index set.
     * @param option the visualisation option.
     * @param index the index set to specify the histogram in the multi dimensional histogram collection.
     * @param front_end the number of the front_end (0-15) which should be visualised or -1 for all front-ends
     * The front-end parameter will be ignored if canVisualiseOneFrontEnd returns false.
     */
    void visualise(IScanDataProvider &data_provider, const std::vector<std::string> &module_names, int , const Index_t &index, int front_end);

  protected:

    TCanvas *canvas();

    static std::string s_name;
    std::string m_histoName;
  };

  class HistoVisualisationKit : public IVisualisationKit 
  {
  public:
    HistoVisualisationKit(bool can_be_option);

    unsigned int priority() const {return (m_canBeOption ? IVisualisationKit::normalPriority : IVisualisationKit::fallBackOnly);}

    /** Create a visualiser for the given inputs or return NULL.
     */
    IVisualiser *visualiser(const std::string &histo_name, 
			    const std::vector<std::string> &/*folder_hierarchy*/) const;

  private:
    bool m_canBeOption;
    static bool init();
    static bool s_instantiated;
  };

}

#endif
