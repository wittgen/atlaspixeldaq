#ifndef _PixA_Histo_h_
#define _PixA_Histo_h_
class TH1;

# define _PIXA_HISTO_IS_ROOT_TH1
namespace PixA {
  typedef TH1 Histo;
}

#ifdef _PIXA_HISTO_IS_ROOT_TH1

# ifdef ROOT_TH1
#  include <DataContainer/HistoUtil.h>
#  include <Histo/Histo.h>
#  include <memory>
namespace PixA {
  inline Histo *convert(PixLib::Histo *a_histo) {
    std::unique_ptr<PixLib::Histo> a_histo_ptr(a_histo);
    if (a_histo_ptr.get()) {
      std::unique_ptr<TObject> obj( PixA::transform(a_histo_ptr.get()) );
      if (obj->InheritsFrom(TH1::Class())) {
	return static_cast< TH1 *>(obj.release());
      }
    }
    return NULL;
  }
}
# endif
#else
namespace PixA {
  inline Histo *convert(PixLib::Histo *a_histo) {
    return a_histo;
  }
}
#endif
#endif
