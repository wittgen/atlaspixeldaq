#ifndef _IVisualisationKit_h_
#define _IVisualisationKit_h_

#include <vector>
#include <string>


namespace PixA {

  class ScanConfigRef;
  class HistoHandle;
  class ConnectivityRef;

  class IVisualiser;

  /** Auxillary class to create visualiser.
   * A kit should imlement at least one of the three methods @ref visualiser.
   */
  class IVisualisationKit {
  public:
    virtual ~IVisualisationKit() {}

    /** Return the priority of the visualiser.
     * If several visualisers exists for one input set, the visualiser with highest priority 
     * will be used by default.
     * A priority of zero indicates that the visualiser is only used in case no other visualiser
     * exists.
     */
    virtual unsigned int priority() const  = 0;

    /** Create a visualiser for the given inputs or return NULL.
     */
    virtual IVisualiser *visualiser(const std::string &/*histo_name*/,
				    const std::vector<std::string> &/*folder_hierarchy*/) const { return NULL;};


    enum EPriorityType { fallBackOnly    =   0,
			 fallBackOnlyMax =  16,
			 lowPriority     =  32,
			 normalPriority  =  64,
			 highPriority    = 127 };

  };
}

#endif
