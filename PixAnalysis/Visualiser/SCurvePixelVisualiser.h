#ifndef _SCurvePixelVisualiser_h_
#define _SCurvePixelVisualiser_h_

#include "HistoVisualiser.h"
#include <ConfigWrapper/ScanConfig.h>
#include "IVisualisationKit.h"
#include <DataContainer/HistoRef.h>
#include <DataContainer/HistoInfo_t.h>
#include <IScanDataProvider.h>

class TCanvas;

namespace PixA {

  class SCurvePixelVisualiser : public HistoVisualiser {
  public:

    //    SCurvePixelVisualiser(const std::string &histo_name);
      //     : HistoVisualiser(histo_name), 
      //	m_scanConfig(scan_config) {}


    SCurvePixelVisualiser(const std::string &histo_name) : HistoVisualiser(histo_name) {} 

    /** Get the name of the visualiser.
     */
    const std::string &name() const { return s_name;}


    /** Function abused to manipulate the (unsused) indizes that can be used then to loop through pixels.
     */
    void fillIndexArray(IScanDataProvider &data_provider,
			const std::vector<std::string> &module_names,
			HistoInfo_t &info) const;

    /** Returns true, if the visualiser can optionally display only one front-end.
     */
    bool canVisualiseOneFrontEnd() const {return true;}


    /** Returns the maximum number of modules the visualiser can display simultaneously.
     */
    unsigned int canVisualiseNModules() const { return 1;} 

    /** Perform the chosen visualisation for the given front_end and the given index set.
     * @param option the visualisation option.
     * @param index the index set to specify the histogram in the multi dimensional histogram collection.
     * @param front_end the number of the front_end (0-15) which should be visualised or -1 for all front-ends
     * The front-end parameter will be ignored if canVisualiseOneFrontEnd returns false.
     */
    void visualise(IScanDataProvider &data_provider, const std::vector<std::string> &module_names, int option, const Index_t &histo_index, int front_end);

  private:
    mutable HistoInfo_t   m_histoInfo;
    static std::string s_name;
  };

  class SCurveVisualisationKit : public IVisualisationKit 
  {
  public:

    unsigned int priority() const {return IVisualisationKit::normalPriority-1;}

    /** Create a visualiser for the given inputs or return NULL.
     */
    IVisualiser *visualiser(const std::string &histo_name,
			    const std::vector<std::string> &/*folder_hierarchy*/) const;

  private:
    static bool init();
    static bool s_instantiated;
  };

}

#endif
