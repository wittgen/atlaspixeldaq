/* Dear emacs, this is -*-c++-*- */
#ifndef _PixA_RootDbScanDataProvider_h_
#define _PixA_RootDbScanDataProvider_h_

#include "IScanDataProvider.h"
#include <ConfigWrapper/Connectivity.h>
#include <DataContainer/HistoList_t.h>
#include <vector>

namespace PixA {

  /** Simple scan data provider which reads the histograms from RootDb files.
   * This provider is intended to be used by ModuleAnalysis to give histogram
   * and configuration data access to the visualiser.
   */
  class RootDbScanDataProvider : public IScanDataProvider
  {
  public:
    RootDbScanDataProvider();
    ~RootDbScanDataProvider();

    SerialNumber_t scanNumber() const {
      if ( m_currentLabel < m_results.size() && m_results[m_currentLabel].begin() != m_results[m_currentLabel].end() ) {

	const PixScanHistoElmPtr_t histo_elm = m_results[m_currentLabel].begin()->second;
	if (histo_elm) {
	  return histo_elm->scanConfig().scanNumber();
	}
      }
      return 0;
    }

    /** Return the connectivity used for the current scan.
     * The connectivity may be invalid.
     */
    const PixA::ConnectivityRef &connectivity() {
      if ( m_currentLabel < m_results.size() && m_results[m_currentLabel].begin() != m_results[m_currentLabel].end() ) {

	const PixScanHistoElmPtr_t histo_elm = m_results[m_currentLabel].begin()->second;
	if (histo_elm) {
	  return histo_elm->scanConfig().connectivity();
	}
      }
      return s_invalidConnectivty;
    }

    void numberOfHistos(const std::string &connectivity_name,
			const std::string &histo_name,
			HistoInfo_t &info) 
    {
      info.reset();

      if (m_currentLabel>=m_results.size()) return;

      m_results[m_currentLabel].numberOfHistograms(connectivity_name, histo_name, info);
    }

    const Histo *histogram(const std::string &connectivity_name,
			   const std::string &histo_name,
			   const Index_t &index)
    {

      if (m_currentLabel>=m_results.size()) return NULL;

      Histo *a_histo = m_results[m_currentLabel].loadHistogram<PixA::Histo>(connectivity_name, histo_name, index);
      if (a_histo) {
	m_cleanupHistos.push_back(a_histo);
      }
      return a_histo;
    }

    const std::string  &prodName(const std::string &connectivity_name)
    {
      if (m_currentLabel< m_results.size()) {
	const PixScanHistoElmPtr_t histo_elm = m_results[m_currentLabel].moduleHistoList(connectivity_name);
	if (histo_elm) {
	  return histo_elm->scanConfig().prodName();
	}
      }
      return s_emptyString;
    }

    const PixA::ConfigHandle scanConfig() 
    {
      if ( m_currentLabel < m_results.size() && m_results[m_currentLabel].begin() != m_results[m_currentLabel].end() ) {
	
	const PixScanHistoElmPtr_t histo_elm = m_results[m_currentLabel].begin()->second;
	if (histo_elm) {
	  
	  return histo_elm->scanConfig().pixScanConfigHandle();
	}
      }
      return PixA::ConfigHandle();
    }

    const PixA::ConfigHandle scanConfig(std::string fe_tag) {
      // To be put the necessary code here by Yosuke
      return PixA::ConfigHandle();
    }

    const PixA::ConfigHandle moduleConfig(const std::string &connectivity_name) 
    {
      if (m_currentLabel< m_results.size()) {
	const PixScanHistoElmPtr_t histo_elm = m_results[m_currentLabel].moduleHistoList(connectivity_name);
	if (histo_elm) {
	  return histo_elm->scanConfig().modConfigHandle();
	}
      }
      return PixA::ConfigHandle();
    }

    const PixA::ConfigHandle bocConfig(const std::string &connectivity_name)
    {
      if (m_currentLabel< m_results.size()) {
	const PixScanHistoElmPtr_t histo_elm = m_results[m_currentLabel].moduleHistoList(connectivity_name);
	if (histo_elm) {
	  return histo_elm->scanConfig().bocConfigHandle();
	}
      }
      return PixA::ConfigHandle();
    }

    //@todo if needed need to implement this
    const PixA::ConfigHandle rodConfig(const std::string &connectivity_name) {
      assert(false);
      return PixA::ConfigHandle();
    }

    //@todo if needed need to implement this
    const PixA::ConfigHandle moduleGroupConfig(const std::string &connectivity_name) {
      assert(false);
      return PixA::ConfigHandle();
    }

    //@todo if needed need to implement this
    const PixA::ConfigHandle timConfig(const std::string &connectivity_name) {
      assert(false);
      return PixA::ConfigHandle();
    }

//     PixA::ConfigHandle rodConfig(const std::string &connectivity_name) 
//     {
//       return PixA::ConfigHandle();
//     }

    static unsigned int invalidLabel()   {
      return s_invalid;
    }

    /** Change the scan results to be accessed in the future.
     * @param label_name the label name of the scan result.
     */
    unsigned int setLabel(const std::string &label_name)
    {
      std::map< std::string, unsigned int >::const_iterator label_iter = m_labelMap.find(label_name);
      if (label_iter == m_labelMap.end()) {
	m_currentLabel = s_invalid;
      }
      else {
	m_currentLabel = label_iter->second;
      }
      return m_currentLabel;
    }

    /** Change the scan results to be accessed in the future.
     * @param label_id the id which got assigned to a label.
     */
    void setLabelId(unsigned int label_id)
    {
      assert (label_id < m_results.size());
      m_currentLabel = label_id;
    }

    /** Return true if the label id is valid.
     */
    bool isValidLabelId(unsigned int label_id) const {
      return label_id != s_invalid && label_id < m_results.size();
    }

    /** Get a new histogram list for the given label.
     * @param label_name the name of the label which defines the scan.
     * The histo list needs to be filled e.g. by the @ref ScanResultStreamer
     */
    std::pair<HistoList_t *,unsigned int> createEmptyHistoList(const std::string &);

  protected:
    static const unsigned int s_invalid = static_cast<unsigned int>(-1);


  private:
    unsigned int m_currentLabel;

    std::map< std::string, unsigned int > m_labelMap;
    std::vector< HistoList_t > m_results;

    static std::string s_emptyString;

    static PixA::ConnectivityRef s_invalidConnectivty;
    std::vector< Histo *> m_cleanupHistos; /**< With the data provider all histograms which have been served to clients will be deleted. */
    using IScanDataProvider::scanConfig;
  };

}
#endif
