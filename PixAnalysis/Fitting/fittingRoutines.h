#ifndef _FITTINGROUTINES_H
#define _FITTINGROUTINES_H

#define NCHIPS 16
#define NCOLS 18
#define NROWS 160
#define NPIXELS (NCHIPS*NCOLS*NROWS)
#define MAXBINS 256
#define NPMAX 4

typedef struct { 
	float par[NPMAX];
} FitPars;

typedef struct {
	unsigned int pixelId, tPixel, tModule, iterations;
	FitPars s;
	float chi2;
} PixelFit;

typedef struct {
  float *x, *y, *w, *ye;
	int n;
} FitData;

#if 0
/* jsv. in a perfect world */
typedef struct {
	FitPars s;
	float chi2;
} PixelFit;
#endif

typedef struct {
	float dt, tBegin, tFinal;
} ManualSeek;

typedef struct {
  int npars;
  float deltaPar[NPMAX]; /* user provides initial deltaMu and deltaSigma */
  float epsilonPar[NPMAX]; /* user provides convergence criterion */
  float minPar[NPMAX]; /* par values below minPar are rounded to this value */
  float concavitySmallness; /* determines smallness criterion for 2nd derivative */
  int nIters, maxIters, converge, convergePar[NPMAX];
  int ndf; /* number of degrees of freedom */
  float chi2; /* final result for chi2 */
  float chi2CutOff; /* if we get chi2 greater than this value, abandon calculation */
  float cutOffPar[NPMAX]; /* if we get values greater than these, abandon */
  void * curve; /* additional information about the curve */
  /* options */
  int extraIter; /* 1 = after convergence, iterate once more for good measure */
  float (*findMinSigma)(FitData *pfd, FitPars *ps, void *pFit);
  float (*findMinMu)(FitData *pfd, FitPars *ps, void *pFit);
  float (*fxn)(FitData *pfd,FitPars *s);
  float (*chiSquared)(FitData *pfd,FitPars *s);
  int (*parGuess)(FitData *pfd,FitPars *sParams,void *vpfit);
  int (*fitSCurve)(FitData *pfd, void *vpfit);
  ManualSeek manualSeekMu, manualSeekSigma;
} Fit;

typedef struct {
	float x, y;
 } Point;

/* function declarations */
void setSigma(FitPars *ps,float sigma);
float errf_ext(float x, FitPars *psp);
float *log_ext(float x, FitPars *psp);
float abs_ext(float x);
int sGuess(FitData *pfd, FitPars *pSParams, void *vpfit);
int totGuess(FitData *pfd, FitPars *Params, void *vpfit);
float nextAbscissa(Point *pt);
float parabolicAbscissa(Point *pt);
int fitSCurveAim(FitData *pfd, void *vpfit);
int fitSCurveCrawl(FitData *pfd, void *vpfit);
int fitSCurveParabolicAlgorithm(FitData *pfd, void *vpfit);
int fitSCurveBruteForce(FitData *pfd, void *vpfit);
int defaultFit(Fit *pfit);
PixelFit *fitModule(void *histBase,int nBins);
float chiSquared(FitData *pfd,FitPars *s);
float logLikelihood(FitData *pfd, FitPars *s);
float logLikelihoodToT(FitData *pfd, FitPars *s);
int extractGoodData(FitData *pfdi,FitData *pfdo);
float manualFindMinSigma(FitData *pfd, FitPars *ps, void *pFit);
float manualFindMinMu(FitData *pfd, FitPars *ps, void *pFit);

extern float data_weight[];
extern float data_errf[];
extern float inverse_lut_interval;
extern float inverse_weight_lut_interval;

#endif
