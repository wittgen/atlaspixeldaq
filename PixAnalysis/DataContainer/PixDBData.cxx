#include "PixDBData.h"

#include "FitClass.h"

#include <TGraphErrors.h>
#include <TF1.h>

#include <RootDb/RootDb.h>

#ifndef _WINDOWS
#include <PixModule/PixModule.h>
#include <PixBoc/PixBoc.h>
#include <RCCVmeInterface.h>
#endif

#include <ConfigWrapper/ConfObjUtil.h>
#include "LevelInfo_t.h"

#define NDBG_PRNT 0

//using namespace PixLib;



// temporary
double TOTFunc(double *x, double *par){
  double denom = par[3]+x[0]*par[4]+par[5]*x[0]*x[0]+par[6]*x[0]*x[0]*x[0]  + par[2];
  if(denom!=0)
    return par[0]+par[1]/denom;
  else
    return 0;
}

PixDBData::PixDBData(const char *name, const char *path, const char *modName_in, const char *conn_name)
  : PixelDataContainer(name,path)
{
  init(modName_in,conn_name);
}

PixDBData::PixDBData(const char *name, const char *path, const char *modName_in, const char *conn_name, PixA::ConnectivityRef &conn)
  : PixelDataContainer(name,path), m_conn(conn)
{
  init(modName_in,conn_name);
}

void PixDBData::init(const char *modName_in, const char *conn_name)
{
  if(modName_in)
    m_modName = modName_in;
  if (conn_name) {
    m_connName = conn_name;
  }

  m_graph = 0;
  m_1dHisto = 0;
  m_2dHisto = 0;
  m_Dbfile = NULL;
  m_pm = 0;
  m_pb = 0;
  m_fitClass = 0;
  m_oldChiHisto = 0;

  // get module ID
  try{
      std::unique_ptr<PixLib::DbRecord> dataRec ( openDbFile(0) );
      if(dataRec.get()!=0){
	PixLib::dbFieldIterator fit = dataRec->findField("general_ModuleId");
	if(fit != dataRec->fieldEnd()) {
	  PixLib::dbFieldIterator fit2(m_Dbfile->DbProcess(fit,PixLib::PixDb::DBREAD, m_modID));
	  fit2.releaseMem();
	}
	else
	  m_modID = -1;
      }
      else {
	m_modID = -1;
      }
  }catch(...){
    m_modID = -1;
  }
  closeFiles();

  if (m_modID==-1 && m_conn) {
 //     std::cout << "I call connectivity 1" << std::endl;
    PixA::ConfigHandle mod_conf_handle = m_conn.modConf(m_connName);
    if (mod_conf_handle) {
      PixA::ConfigRef mod_conf = mod_conf_handle.ref();
      //      const PixLib::ConfObj &obj = mod_conf["general"]["ModuleId"];
      m_modID=confVal<int>(mod_conf["general"]["ModuleId"]);
      std::cout << "m_modID: " << m_modID << std::endl;
    }
//    PixA::ConfigRef mod_conf = m_conn.modConf(m_connName).ref();
//    m_modID=confVal<int>(mod_conf["general"]["ModuleId"]);
  }
}
PixDBData::~PixDBData()
{
  delete m_1dHisto;
  delete m_2dHisto;
  closeFiles();
  delete m_pm;
  delete m_pb;
  delete m_fitClass;
  delete m_oldChiHisto;
}
int PixDBData::getScanLevel()
{
  int nloops;
  Config &conf = getScanConfig();
  for(nloops=0;nloops<3;nloops++){
    std::stringstream lnum;
    lnum << nloops;
    if(conf["loops"]["activeLoop_"+lnum.str()].name()!="__TrashConfObj__" && 
       !((ConfBool&)conf["loops"]["activeLoop_"+lnum.str()]).value())
      break;
  }
  return nloops;
}
int PixDBData::getNevents()
{
  int nevts = -1;
  Config &conf = getScanConfig();
  if(conf["general"]["repetitions"].name()!="__TrashConfObj__")
    nevts = ((ConfInt&)conf["general"]["repetitions"]).getValue();
  return nevts;
}
int PixDBData::getScanSteps(int loopLevel)
{
  Config &conf = getScanConfig();
  std::stringstream lnum;
  lnum << loopLevel;
  if(conf["loops"]["loopVarNStepsLoop_"+lnum.str()].name()!="__TrashConfObj__")
    return ((ConfInt&)conf["loops"]["loopVarNStepsLoop_"+lnum.str()]).getValue();
  else
    return -1;
}
int PixDBData::getScanStart(int loopLevel)
{
  Config &conf = getScanConfig();
  std::stringstream lnum;
  lnum << loopLevel;
  if(conf["loops"]["loopVarMinLoop_"+lnum.str()].name()!="__TrashConfObj__")
    return (int)((ConfFloat&)conf["loops"]["loopVarMinLoop_"+lnum.str()]).value();
  else
    return -1;
}
int PixDBData::getScanStop(int loopLevel)
{
  Config &conf = getScanConfig();
  std::stringstream lnum;
  lnum << loopLevel;

  bool valFree = false;

  if(conf["loops"]["loopVarValuesFreeLoop_"+lnum.str()].name()!="__TrashConfObj__")
    valFree = ((ConfBool&)conf["loops"]["loopVarValuesFreeLoop_"+lnum.str()]).value();

  if(valFree){
    int steps = getScanSteps(loopLevel);
    int start = getScanStart(loopLevel);
    return start+start/abs(start)*steps;
  } else {
    if(conf["loops"]["loopVarMaxLoop_"+lnum.str()].name()!="__TrashConfObj__")
      return (int)((ConfFloat&)conf["loops"]["loopVarMaxLoop_"+lnum.str()]).value();
    else
      return -1;
  }
}
std::string PixDBData::getScanPar(int loopLevel)
{
  static std::string retStrg="unknow par";
  Config &conf = getScanConfig();
  std::stringstream lnum;
  lnum << loopLevel;
  if(conf["loops"]["paramLoop_"+lnum.str()].name()!="__TrashConfObj__")
    return ((ConfList&)conf["loops"]["paramLoop_"+lnum.str()]).sValue();
  else
    return retStrg;
}
bool PixDBData::haveHistoType(PixLib::PixScan::HistogramType type)
{
  if(type>=PixLib::PixScan::MAX_HISTO_TYPES) return false;
  bool retval = false;
  Config &conf = getScanConfig();
  std::string name=getHistoName(type);
  if(name=="none") return false;
  if(conf["histograms"]["histogramFilled"+name].name()!="__TrashConfObj__" && 
     conf["histograms"]["histogramKept"+name].name()!="__TrashConfObj__") {
    retval = ((ConfBool&)conf["histograms"]["histogramFilled"+name]).value() &&
      ((ConfBool&)conf["histograms"]["histogramKept"+name]).value();
  }
  return retval;
}
std::string PixDBData::getHistoName(PixLib::PixScan::HistogramType type)
{
  PixLib::PixScan *ps = (m_ps.get() ?  m_ps.get() : new PixScan());
  std::string name="none";
  for(std::map<std::string, int>::iterator it = ps->getHistoTypes().begin();
      it!=ps->getHistoTypes().end(); it++){
    if(it->second==type){
      name = it->first;
      break;
    }
  }
  if (m_ps.get() != ps ) 
     delete ps;
  return name;
}

TH2F* PixDBData::GetMap(int chip, int in_type, int scanpt)
{
  // clean up old temp. histo first
  if(m_2dHisto!=0) m_2dHisto->Delete();
  m_2dHisto=0;
  if(m_1dHisto!=0) m_1dHisto->Delete();
  m_1dHisto=0;

  PixLib::PixScan::HistogramType type = (PixLib::PixScan::HistogramType) in_type;

  if(type<0 || type>=PixLib::PixScan::MAX_HISTO_TYPES)
    return 0;

  // get basic info about scan
  PixLib::Config &conf = getScanConfig();
  std::string lname = "loopVarValuesLoop_0";
  int id[4] = {0,0,0,0}, incrid=0;
  if(scanpt>=0)
    id[0] = scanpt;
  if(type==PixLib::PixScan::SCURVE_MEAN || type==PixLib::PixScan::SCURVE_SIGMA || type==PixLib::PixScan::SCURVE_CHI2){
    id[0] = -1;
    if(scanpt>=0)
      id[1] = scanpt;
    incrid=1;
    lname = "loopVarValuesLoop_1";
  }
  std::vector<float> &scanval = ((ConfVector&)conf["loops"][lname]).valueVFloat();
  int nsteps=scanval.size();
  if(getScanLevel()==0) nsteps=1;
  if(nsteps<1) nsteps=1;

  int nmin=0, nmax=nsteps;
  if(scanpt>=0){
    nmin = scanpt;
    nmax = scanpt+1;
  }

  // get histogram for module
  try{
    std::unique_ptr<PixLib::DbRecord> dataRec ;
      dataRec = std::unique_ptr<PixLib::DbRecord>( openDbFile(true) );

    for(int nsc=nmin;nsc<nmax;nsc++){
      std::unique_ptr<PixLib::Histo> h;
      try{
	  h = std::unique_ptr<PixLib::Histo> ( readHisto(dataRec.get(),type,id) );
      }catch(...){
	h.reset();
      }
      id[incrid]++;
      // some sanity checks
      if(h.get()!=0){
	if(h->nDim()!=2){
	  closeFiles();
	  return 0;
	}
	// turn PixLib::Histo into ROOT TH2F
	if(m_2dHisto==0){
	  gROOT->cd();
	  m_2dHisto = new TH2F(h->name().c_str(),h->title().c_str(),
			       h->nBin(0),h->min(0), h->max(0),h->nBin(1),h->min(1), h->max(1));
	}
	int i,j;
	for (i=0; i<h->nBin(0); i++) {
	  for (j=0; j<h->nBin(1); j++) {
	    float cont = m_2dHisto->GetBinContent(i+1,j+1) + (float)(*h)(i,j);
	    m_2dHisto->SetBinContent(i+1,j+1,cont);
	  }
	}
      }
    }
    closeFiles();
  }catch(...){
    closeFiles();
    return 0;
  }

  return getChipMap(chip);
}
TGraphErrors* PixDBData::GetScanHi(int chip, int col, int row, int in_type, int scanpt)
{
  delete m_graph; m_graph=0;

  PixLib::PixScan::HistogramType type = (PixLib::PixScan::HistogramType) in_type;
  if(type<0 || type>=PixLib::PixScan::MAX_HISTO_TYPES)
    return 0;

  if(getScanLevel()==0) return 0; // can only display scan data

  PixLib::Config &conf = getScanConfig();

  std::string lname = "loopVarValuesLoop_0";
  int id[4] = {0,0,0,0}, incrid=0;
  if(scanpt>=0) id[0] = scanpt;
  if(type==PixLib::PixScan::SCURVE_MEAN || type==PixLib::PixScan::SCURVE_SIGMA || type==PixLib::PixScan::SCURVE_CHI2){
    id[0] = -1;
    incrid=1;
    lname = "loopVarValuesLoop_1";
  }
  std::vector<float> &scanval = ((ConfVector&)conf["loops"][lname]).valueVFloat();

  double nevts=(double)((ConfInt&)conf["general"]["repetitions"]).getValue();

  double *xpts, *ypts, *xepts, *yepts;
  xpts  = new double[scanval.size()];
  ypts  = new double[scanval.size()];
  xepts = new double[scanval.size()];
  yepts = new double[scanval.size()];
  int npts=0;
  // get histogram for module
  try{
    std::unique_ptr<PixLib::DbRecord> dataRec;
      dataRec = std::unique_ptr<PixLib::DbRecord>( openDbFile(true) );

    for(int nsc=0;nsc<(int)scanval.size();nsc++){
      xpts[npts]  = (double)scanval[nsc];
      xepts[npts] = 1e-4*(double)scanval[nsc];
      std::unique_ptr<PixLib::Histo> h;
      try{
	  h = std::unique_ptr<PixLib::Histo>(readHisto(dataRec.get(),type,id));
      }catch(...){
	h.reset();
      }
      if(h.get()!=0){
	float x,y;
	PixXY(chip,col,row,&x,&y);
	ypts[npts]  = (double)(*h)((int)x,(int)y);
	switch(type){
	case PixLib::PixScan::OCCUPANCY: // binomial error
	  if(nevts>0)
	    yepts[npts] = ypts[npts]/nevts*(nevts-ypts[npts]);
	  else
	    yepts[npts] = ypts[npts];
	  if(yepts[npts]>0)       
	    yepts[npts] = sqrt(yepts[npts]);
	  break;
	default: // don't know what to do - have no error
	  yepts[npts] = 1e-4*ypts[npts];
	}
	npts++;
      }
      id[incrid]++;
    }
  }catch(...){
  }
  closeFiles();
  if(npts>0){
    m_graph = new TGraphErrors(npts, xpts, ypts, xepts, yepts);
    m_graph->GetXaxis()->SetTitle("Scan parameter");
    m_graph->SetMarkerStyle(20);
    m_graph->SetMarkerSize(.6f);
  }
  delete xpts;
  delete ypts;
  delete xepts;
  delete yepts;

  return m_graph;
}
PixLib::Config& PixDBData::getScanConfig()
{

  if (!m_ps.get()) {
  m_ps = std::make_unique<PixLib::PixScan>();
  try{
      std::unique_ptr<PixLib::DbRecord> dataRec ( openDbFile(true) );
      if(dataRec.get() ){
	m_ps->readConfig(dataRec.get());
      }
      closeDbFile();
  }catch(...){
    closeFiles();
  }
  }
  return m_ps->config();
}

PixLib::Config& PixDBData::getModConfig()
{
  static PixLib::Config cfg("blabla");
  PixLib::Config *retcfg = &cfg;

  try{
    delete m_pm;
      std::unique_ptr<PixLib::DbRecord> dataRec ( openDbFile(0) );
      if(dataRec.get()!=0){
	m_pm = new PixLib::PixModule(dataRec.get(), 0, m_modName);
	retcfg = &( m_pm->config());
      }
      closeDbFile();
      if(dataRec.get()==0 && m_conn && !m_connName.empty()) {
	std::cout << "INFO [PixDBData::getModConfig] get module config for " << m_connName << " from connectivty." << std::endl;
	if (m_fullModuleConfig.empty()) {
    //    std::cout << "I call connectivity 2" << std::endl;
	  m_fullModuleConfig.push_back(m_conn.modConf(m_connName).ref().createFullCopy());
	  m_fullModuleConfigHandle=m_fullModuleConfig.back();
	}
	//@todo thish method really should return a const PixLib::Config
	return const_cast<PixLib::Config &>(m_fullModuleConfig.back().config());
      }
  }catch(...){
    closeFiles();
  }
  return *retcfg;
}
PixLib::Config& PixDBData::getBocConfig()
{
  static PixLib::Config cfg("blabla");
  PixLib::Config *retcfg = &cfg;
  try{
    delete m_pb;
    m_pb=0;
    SctPixelRod::RCCVmeInterface *vme = 0;
    PixLib::PixModuleGroup *grp = 0;
      std::unique_ptr<PixLib::DbRecord> dataInq ( openDbFile(2) );
      if(dataInq.get()!=0){
        //Deprecated constructor removed (was not working, anyhow..) 
	//grp = new PixLib::PixModuleGroup(dataInq->getDb());
	//m_pb = new PixLib::PixBoc(*grp,dataInq.get());
	//retcfg = m_pb->getConfig();
      }
      closeDbFile();
    delete grp;
    delete vme;
  }catch(...){
    closeFiles();
  }
  return *retcfg;
}

void PixDBData::closeDbFile() {
  delete m_Dbfile; m_Dbfile=NULL;
}


PixLib::DbRecord* PixDBData::openDbFile(int type, bool write)
{
  closeDbFile(); // closes prev. opened file if still open

  // get file and scan names from stored strings
  std::string fname = m_fullpath;
  fname.erase(fname.length()-1,1);
  std::string scanName = m_fullpath;
  scanName.erase(scanName.length()-1,1);
  int i = fname.find_last_of("/");
  if(i!=(int)std::string::npos){
    fname.erase(i-1,fname.length()-i+1);
    scanName.erase(0,i+1);
  }

  if(fname.substr(fname.length()-5,5)==".root"){
    TFile f(fname.c_str());
    std::unique_ptr<TNamed> nobj ( (TNamed*) f.Get("rootRecord") );
    std::string clName = "none";
    if(nobj.get()!=0) clName = nobj->ClassName();
    f.Close();
    if(clName=="PixLib::RootDbRecord"){
      if(write)
	m_Dbfile = new PixLib::RootDb(fname,"UPDATE");
      else
	m_Dbfile = new PixLib::RootDb(fname,"READ");
    } else{
      m_Dbfile = 0;
      return 0;
    }
  } else{
    m_Dbfile = 0;
    return 0;
  }


  {
  std::unique_ptr<PixLib::DbRecord> retInq;
  PixLib::dbRecordIterator ri;

  std::unique_ptr<PixLib::DbRecord> root (m_Dbfile->readRootRecord());

  std::string name = scanName;

  ri = root->findRecord(name+"/PixScanResult");
  if(ri == root->recordEnd()) return 0;
  std::unique_ptr<PixLib::DbRecord> scanInq ( *(m_Dbfile->DbProcess(ri, PixLib::PixDb::DBREAD)));
  name = m_pname;
  ri = scanInq->findRecord(name+"/PixModuleGroup");
  if(ri == scanInq->recordEnd()) return 0;
  std::unique_ptr<PixLib::DbRecord> grpInq( *(m_Dbfile->DbProcess(ri, PixLib::PixDb::DBREAD)) );
  
  ri=grpInq->recordEnd();
  if(type==1){
    name = "Data_Scancfg/PixScanData";
    ri = grpInq->findRecord(name);
  }
  else if(type==2) {
    name = "OpticalBoc/PixBoc";
    ri = grpInq->findRecord(name);
  }
  else {
    if (!m_connName.empty()) {
      name = m_connName+"/PixModule";
      ri = grpInq->findRecord(name);
    }

    if(ri==grpInq->recordEnd()){
      name = m_modName+"/PixModule";
      ri = grpInq->findRecord(name);
    }
  }
  if(ri==grpInq->recordEnd()){
    if(type==0){ // modules might have been saved in a global folder
      name = m_pname;
      std::vector<std::string *> name_list;
      if (!m_connName.empty()) name_list.push_back(&m_connName);
      name_list.push_back(&m_modName);
      ri = root->findRecord(name+"/PixModules");
      if(ri!=root->recordEnd()){
	ri = m_Dbfile->DbProcess(ri, PixLib::PixDb::DBREAD);
	std::unique_ptr<PixLib::DbRecord> ri_content(*ri);

	for (std::vector<std::string *>::const_iterator iter=name_list.begin();
	     iter!=name_list.end();
	     iter++) {

	  PixLib::dbRecordIterator ri2 = (*ri)->findRecord((**iter)+"/PixModule");
	  if(ri2!=(*ri)->recordEnd()) {
	    retInq = std::unique_ptr<PixLib::DbRecord>(*(m_Dbfile->DbProcess(ri2, PixLib::PixDb::DBREAD)));
	    break;
	  }
	}
      }
    }
  }else
    retInq = std::unique_ptr<PixLib::DbRecord>(*(m_Dbfile->DbProcess(ri, PixLib::PixDb::DBREAD)));
  return retInq.release();
  }
}

std::string makeRecordName(const std::string &name, const std::string &class_name, bool old ) 
{
  return ( old ? name + "/" + name : name + "/" + class_name );
}

PixLib::Histo* PixDBData::readHisto(PixLib::DbRecord* dbi, 
				    PixLib::PixScan::HistogramType type, const int id[4] ) 
{
  PixLib::Histo *h = 0;
  if(dbi==0) return 0;
  if(m_modID<0) return 0; // initial problems, don't even try to do anything
  std::unique_ptr<PixLib::DbRecord> currRec;
  std::string histo_name=getHistoName(type);

  if(histo_name!="none"){

    std::string name(histo_name);
    bool old=false;
    name += "/PixScan_Histo";
    PixLib::dbRecordIterator ri  = dbi->findRecord(name);
    if (ri == dbi->recordEnd()) {
      name = histo_name + "/" + histo_name;
      ri  = dbi->findRecord(name);
      old=true;
    }

    if (ri != dbi->recordEnd()) {
      currRec = std::unique_ptr<PixLib::DbRecord>(*(dbi->getDb()->DbProcess(ri, PixLib::PixDb::DBREAD)));
      std::stringstream inqname;
      ri =currRec->recordEnd();
      if (!m_connName.empty()) {
	ri = currRec->findRecord(makeRecordName(m_connName,"PixScanHisto",old));
      }
      if (ri == currRec->recordEnd()) {
	std::stringstream a_name;
	a_name << "Mod" << m_modID;
	ri = currRec->findRecord(makeRecordName(a_name.str(),"PixScanHisto",old));
	if (old) {
	  inqname << a_name.str() << "_";
	}
      }
      
      if (ri != currRec->recordEnd()) {
	currRec = std::unique_ptr<PixLib::DbRecord>(*(dbi->getDb()->DbProcess(ri, PixLib::PixDb::DBREAD)));
	
	std::stringstream fieldname;
	int lvl;
	for(lvl=2;lvl>=0 && id[lvl]>=0 ;lvl--){
	  switch(lvl){
	  default:
	  case 0:
	    inqname << "C";
	    break;
	  case 1:
	    inqname << "B";
	    break;
	  case 2:
	    inqname << "A";
	    break;
	  }
	  inqname << id[lvl];
	  ri  = currRec->findRecord(makeRecordName(inqname.str(),"PixScanHisto",old));
	  if (ri == currRec->recordEnd()) break;
	  currRec = std::unique_ptr<PixLib::DbRecord>(*(dbi->getDb()->DbProcess(ri, PixLib::PixDb::DBREAD)));
	  inqname << "_";
	}
      }
      if (ri!=currRec->recordEnd()) {
	// then currRec contains the correct record
	
	inqname << id[3]; // what does "_0" mean??
	PixLib::dbFieldIterator fit = currRec->findField(inqname.str());
	if (fit != currRec->fieldEnd()) {  // histos on this level
	  //	  fit = dbi->getDb()->DbProcess(fit,PixLib::PixDb::DBREAD);
	  //	  fit.releaseMem();
	  h = new Histo();
	  fit = dbi->getDb()->DbProcess(fit, PixLib::PixDb::DBREAD, *h);
	  fit.releaseMem();
	}
      }
    }
  }
  return h;
}

bool PixDBData::numberOfHistos(PixLib::PixScan::HistogramType type, HistoInfo_t &info ) {
  bool ret=false;
  info.reset();

    try {
      std::unique_ptr< PixLib::DbRecord> dataRec ( openDbFile(1) );
      ret = numberOfHistos(dataRec.get(),type,info);
    }
    catch (...) {}
    closeDbFile();
  return ret;
}

class LevelInfo_t : public LevelInfoBase_t
{
public:
  LevelInfo_t(const std::string &name, PixLib::DbRecord *record) : LevelInfoBase_t(name) ,m_levelRecord(record) {}
  LevelInfo_t(const std::string &name, unsigned int level_i, PixLib::DbRecord *record) : LevelInfoBase_t(name,level_i),m_levelRecord(record) {}

  PixLib::DbRecord *record()           {return m_levelRecord.get(); }
  std::shared_ptr<PixLib::DbRecord> m_levelRecord;
};

bool PixDBData::numberOfHistos(PixLib::DbRecord* dbi, 
			       PixLib::PixScan::HistogramType type, HistoInfo_t &info ) {

  // info.reset();
  unsigned int min_histo[4]={0,0,0,0};
  unsigned int max_histo[4]={0,0,0,0};


  if(dbi==0) return 0;
  if(m_modID<0) return 0; // initial problems, don't even try to do anything

  std::string histo_name=getHistoName(type);
  //  std::cout << "INFO [PixDBData::numberOfHistos] search histograms of type " << histo_name << std::endl;
  if(histo_name!="none"){

    std::string name(histo_name);
    bool old=false;
    name += "/PixScan_Histo";
    PixLib::dbRecordIterator ri  = dbi->findRecord(name);
    if (ri == dbi->recordEnd()) {
      name = histo_name + "/" + histo_name;
      ri  = dbi->findRecord(name);
      old=true;
    }

    std::unique_ptr<PixLib::DbRecord> currRec;

    if (ri != dbi->recordEnd()) {
      currRec = std::unique_ptr<PixLib::DbRecord> (*(dbi->getDb()->DbProcess(ri , PixLib::PixDb::DBREAD)));

      std::stringstream inqname;
      if (!m_connName.empty()) {
	ri = currRec->findRecord(makeRecordName(m_connName,"PixScanHisto",old));
      }
      if (ri == currRec->recordEnd()) {
	std::stringstream a_name;
	a_name << "Mod" << m_modID;
	ri = currRec->findRecord(makeRecordName(a_name.str(),"PixScanHisto",old));
	if (old) {
	  inqname << a_name.str() << "_";
	}
      }

      if (ri != currRec->recordEnd()) {


	std::vector<LevelInfo_t> level_stack;
	level_stack.reserve(4);
	level_stack.push_back(LevelInfo_t(inqname.str(), *(dbi->getDb()->DbProcess(ri, PixLib::PixDb::DBREAD))) );

	while (!level_stack.empty()) {

// 	  std::cout << "INFO [PixDBData::numberOfHistos] search on level "  << level_stack.back().level()
// 		    << " in " << level_stack.back().record()->getName()
// 		    << std::endl;

	  if (level_stack.back().index() == 0) {
	    //search histograms
	    // the only possibility is to load all fields and analyse the 
	    // record type to gel all the histograms
	    // this is slow

	    // so it is assumed that the lowest histogram index is smaller than :
	    const unsigned int n_histos = 4;
	    min_histo[level_stack.back().level()]=0;
	    max_histo[level_stack.back().level()]=0;
	    info.setMinHistoIndex(n_histos);
	    for (unsigned int hist_i=0; ; hist_i++) {
	      std::stringstream hist_name;
	      hist_name << level_stack.back().name() << hist_i;

	      PixLib::dbFieldIterator fit = level_stack.back().record()->findField(hist_name.str());
	      //	      std::cout << "INFO [PixDBData::numberOfHistos] search histogram " << hist_name.str() << " of type " << histo_name  << std::endl;
	      if (fit==level_stack.back().record()->fieldEnd()) {
		;
		if (hist_i<n_histos && max_histo[ level_stack.back().level() ]==0 ) {
		  min_histo[ level_stack.back().level() ]++;
		}
		else break;
	      }

	      max_histo[ level_stack.back().level() ]++;
	    }
	    if (min_histo[ level_stack.back().level() ] == max_histo[ level_stack.back().level() ]) {
	      min_histo[ level_stack.back().level() ]=0;
	      max_histo[ level_stack.back().level() ]=0;
	    }
	    unsigned int min_histo_temp= (min_histo[ level_stack.back().level() ]  < max_histo[ level_stack.back().level() ] ? 
					  min_histo[ level_stack.back().level() ]
					  : max_histo[ level_stack.back().level()] );

	    if (min_histo_temp < info.minHistoIndex() )
	      info.setMinHistoIndex(min_histo_temp);

	    if (max_histo[ level_stack.back().level() ] > info.maxHistoIndex()) 
	      info.setMaxHistoIndex( max_histo[ level_stack.back().level() ]);
	  }

	  if (level_stack.back().level()>=LevelInfo_t::maxLevel()) {
	    level_stack.pop_back();
	    continue;
	  }

	  std::string a_name ( level_stack.back().name() );
	  a_name += LevelInfo_t::levelName( level_stack.back().level() ) ;

	  for (;;) {

	    std::stringstream sub_level_name;
	    sub_level_name << a_name << level_stack.back().index();

// 	    std::cout << "INFO [PixDBData::numberOfHistos] search on level "  << level_stack.back().level()
// 		      << " for sub level " << sub_level_name.str() 
// 		      << " reocrd name = " << makeRecordName(sub_level_name.str(),"PixScanHisto",old)
// 		      << std::endl;

	    PixLib::dbRecordIterator ri_a = level_stack.back().record()->findRecord(makeRecordName(sub_level_name.str(),"PixScanHisto",old));
	    level_stack.back().index()++;

	    if (ri_a == level_stack.back().record()->recordEnd()) {
	      level_stack.pop_back();
	      break;
	    }

	    // there should not be histograms if there are sub levels.
	    assert ( max_histo[ level_stack.back().level() ] == 0);

	    unsigned int inv_level_index= LevelInfo_t::maxLevel()-1 - level_stack.back().level();

	    if (info.maxIndex(inv_level_index) < level_stack.back().index() )
		info.setMaxIndex(inv_level_index,  level_stack.back().index());

	    level_stack.push_back(LevelInfo_t(sub_level_name.str(),level_stack.back().level()+1, *( dbi->getDb()->DbProcess(ri_a, PixLib::PixDb::DBREAD) )) );
	    break;
	  }

	}
      }
    }
  }
//   std::cout << "INFO [PixDBData::numberOfHistos] result : ";
//   for (unsigned int i =0; i< LevelInfo_t::maxLevel(); i++) {
//     std::cout << info.maxIndex(i) << ", ";
//   }
//   std::cout << info.minHistoIndex() << " - " << info.maxHistoIndex()
// 	    << std::endl;
  return info.minHistoIndex() <  info.maxHistoIndex();
}


PixLib::Histo* PixDBData::getGenericPixLibHisto(int type, int scanpt, int scan_level)
{
  if(type<0)
    return 0;
  if (type>PixLib::PixScan::MAX_HISTO_TYPES) return 0;

  int ftype = (int)type - (int) PixLib::PixScan::MAX_HISTO_TYPES;
  PixLib::Histo *fh=0;
  if(ftype>=0 && (fh=getParHisto(ftype))!=0)
    return fh;
  else if (type>=PixLib::PixScan::MAX_HISTO_TYPES)
    return 0;

  // get basic info about scan
  int id[4];
  int scanl = scan_level+1;
  if(scanl<0)
    scanl = getScanLevel();

  // get histogram for module

  std::unique_ptr<PixLib::DbRecord> dataRec;
    dataRec = std::unique_ptr<PixLib::DbRecord>( openDbFile(1) );

  std::unique_ptr<PixLib::Histo> h;

  id[3]=0;
  for(int nlvl=2;nlvl>=0; nlvl--){
    for(int k=0;k<3;k++){
      if(k<nlvl)
	id[k] = -1;
      else if (k==nlvl){
	if((scanl-1)==nlvl && scanpt>=0)
	  id[k] = scanpt;
	else
	  id[k] = 0;
      } else
	id[k] = 0;
    }
    try{
	h = std::unique_ptr<PixLib::Histo>( readHisto(dataRec.get(),static_cast<PixLib::PixScan::HistogramType>(type),id) );
    }catch(...){
      h.reset();
    }
    if(h.get()!=0) break; // found something, process now
  }
  if(h.get()==0 && scan_level<0){ // might be end-of scan result, try somewhere else
    for(int nlvl=2;nlvl>=0; nlvl--){
      for(int k=0;k<3;k++){
	if(k<nlvl)
	  id[k] = -1;
	else
	  id[k] = 0;
      }
      try{
	  h = std::unique_ptr<PixLib::Histo>( readHisto(dataRec.get(), static_cast<PixLib::PixScan::HistogramType>(type), id) );
      }catch(...){
	h.reset();
      }
      if(h.get()!=0) break; // found something, process now
    }
  }
  closeFiles();
  return h.release();
}

PixLib::Histo* PixDBData::getGenericPixLibHisto(PixLib::PixScan::HistogramType type, const int idx[4])
{
  if(type<0)
    return 0;

  int ftype = (int)type - (int) PixLib::PixScan::MAX_HISTO_TYPES;
  PixLib::Histo *fh=0;
  if(ftype>=0 && (fh=getParHisto(ftype))!=0)
    return fh;
  else if (type>=PixLib::PixScan::MAX_HISTO_TYPES)
    return 0;


  std::unique_ptr<PixLib::Histo> h;
  // get histogram for module

  std::unique_ptr<PixLib::DbRecord> dataRec ;
    dataRec = std::unique_ptr<PixLib::DbRecord> ( openDbFile(1) );

  try{
      h = std::unique_ptr<PixLib::Histo>( readHisto(dataRec.get(),type,idx) );
  }catch(...){
    h.reset();
  }
  closeFiles();
  return h.release();
}

TObject* PixDBData::getGenericHisto(PixLib::PixScan::HistogramType type, 
				    int chip, int col, int row, int scanpt, int scan_level)
{
  // clean up old temp. histo first
  if(m_2dHisto!=0) m_2dHisto->Delete();
  m_2dHisto=0;
  if(m_1dHisto!=0) m_1dHisto->Delete();
  m_1dHisto=0;

  std::unique_ptr<PixLib::Histo> h ( getGenericPixLibHisto(type,scanpt, scan_level) );
  if(h.get()==0) return 0;

  gROOT->cd(); // make sure histo is created in memory, not file
  if(h->nDim()==1){
    m_1dHisto = new TH1F(h->name().c_str(),h->title().c_str(),
			 h->nBin(0),h->min(0), h->max(0));
    int i;
    for (i=0; i<h->nBin(0); i++) {
      m_1dHisto->SetBinContent(i+1,(float)(*h)(i));
    }
    return (TObject*) m_1dHisto;
  } else if (h->nDim()==2){
    m_2dHisto = new TH2F(h->name().c_str(),h->title().c_str(),
			 h->nBin(0),h->min(0), h->max(0),h->nBin(1),h->min(1), h->max(1));
    int i,j;
    for (i=0; i<h->nBin(0); i++) {
      for (j=0; j<h->nBin(1); j++) {
	m_2dHisto->SetBinContent(i+1,j+1,(float)(*h)(i,j));
      }
    }
    return (TObject*) m_2dHisto;
  } else
    return 0;
}

TObject* PixDBData::getGenericHisto(PixLib::PixScan::HistogramType type, 
				    int chip, int col, int row, const int idx[4])
{
  // clean up old temp. histo first
  if(m_2dHisto!=0) m_2dHisto->Delete();
  m_2dHisto=0;
  if(m_1dHisto!=0) m_1dHisto->Delete();
  m_1dHisto=0;

  std::unique_ptr<PixLib::Histo> h ( getGenericPixLibHisto(type,idx) );
  if(!h.get()) return 0;

  gROOT->cd(); // make sure histo is created in memory, not file
  if(h->nDim()==1){
    m_1dHisto = new TH1F(h->name().c_str(),h->title().c_str(),
			 h->nBin(0),h->min(0), h->max(0));
    int i;
    for (i=0; i<h->nBin(0); i++) {
      m_1dHisto->SetBinContent(i+1,(float)(*h)(i));
    }
    return (TObject*) m_1dHisto;
  } else if (h->nDim()==2){
    m_2dHisto = new TH2F(h->name().c_str(),h->title().c_str(),
			 h->nBin(0),h->min(0), h->max(0),h->nBin(1),h->min(1), h->max(1));
    int i,j;
    for (i=0; i<h->nBin(0); i++) {
      for (j=0; j<h->nBin(1); j++) {
	m_2dHisto->SetBinContent(i+1,j+1,(float)(*h)(i,j));
      }
    }
    return (TObject*) m_2dHisto;
  } else
    return 0;
}

TH2F* PixDBData::getDACMap(int in_chip, const char *in_type)
{
  // clean up old temp. histo first
  if(m_2dHisto!=0) m_2dHisto->Delete();
  m_2dHisto=0;
  if(m_1dHisto!=0) m_1dHisto->Delete();
  m_1dHisto=0;

  if(in_type==0) return 0;
  std::string type = in_type;
  if(type!="TDAC" && type!="FDAC") return 0; // no other map programmed

  PixLib::Config &conf = getModConfig();

  int binx, biny, chip, col, row, chip_min=0, chip_max=16, nrows=320, ncols=144;
  if(in_chip>=0){
    nrows=160;
    ncols = 18;
    chip_min = in_chip;
    chip_max = in_chip+1;
  }

  std::string title = "Map of "+type;
  if(in_chip>=0){
    std::stringstream cs;
    cs << in_chip;
    title += " chip ";
    title += cs.str();
  }
  m_2dHisto = new TH2F("tmpdac",title.c_str(), ncols, -0.5, 0.5+(float)ncols,
			 nrows, -0.5, 0.5+(float)nrows);

  
  for(chip=chip_min; chip<chip_max; chip++){
    std::stringstream a;
    a << chip;
    Config &subcfg1 = conf.subConfig("PixFe_"+a.str());
    Config *subcfg=0;
    for(unsigned int i=0; i<subcfg1.subConfigSize(); i++){
      if(subcfg1.subConfig(i).name()=="Trim_0"){
	subcfg = &(subcfg1.subConfig(i));
	break;
      }
    }
    if(subcfg==0) return 0;
    PixLib::ConfMatrix &matrix = (PixLib::ConfMatrix &) (*subcfg)["Trim"][type];
    const PixLib::ConfMask<TrimVal_t> &mask = matrix.valueU16();
    //tc.dump(std::cout);
    for(col=0;col<18; col++){
      for(row=0;row<160;row++){
	if(in_chip>=0){
	  binx = col+1;
	  biny = row+1;
	} else
	  PixCCRtoBins(chip, col, row, binx, biny);
	m_2dHisto->SetBinContent(binx,biny,static_cast<float>( const_cast<PixLib::ConfMask<TrimVal_t> &>(mask).get(col,row)) );
      }
    }
  }

  return m_2dHisto;
}
TH2F* PixDBData::getChipMap(int chip){
  if(chip<0) // full module, we're done
    return m_2dHisto;
  if(m_2dHisto==0) // nothing to do
    return 0;
  else{  // have to extract chip part from module map
    char hname[100], htitle[200];
    sprintf(hname,"%s%d",m_2dHisto->GetName(),chip);
    sprintf(htitle,"%s chip %d",m_2dHisto->GetTitle(),chip);
    gROOT->cd();
    TH2F *retmap = new TH2F(hname,htitle,NCOL,-0.5,NCOL-0.5,NROW,-0.5,NROW-0.5);
    for(int i=0;i<NCOL;i++){
      for(int j=0;j<NROW;j++){
	float xval, yval;
        PixXY(chip,i,j,&xval,&yval);
        retmap->Fill(i,j,(float) 
		     m_2dHisto->GetBinContent(m_2dHisto->GetBin(1+(int)xval,1+(int)yval)));
      }
    }
    delete m_2dHisto;
    m_2dHisto = retmap;
    return m_2dHisto;
  }
}
void PixDBData::fitHisto(int fittype, int chip, float chicut, float xmin, float xmax, float fracErr, bool useNdof)
{
  std::stringstream a1;
  a1 << chip;
  if(chip<-1 || chip>15)
    throw SctPixelRod::BaseException("PixDBData::FitHisto : Chip-ID must be between -1 and 15, but is "+a1.str()+".");

  int k, minChip=0, maxChip=16, npar = m_fitClass->getNPar(fittype)+4*(int)m_fitClass->hasVCAL(fittype);
  if(chip>-1){
    minChip = chip;
    maxChip = chip+1;
  }
  double *x, *y, *xerr, *yerr, *pars;
  bool *pfix;
  x    = new double[m_varValues.size()];
  y    = new double[m_varValues.size()];
  xerr = new double[m_varValues.size()];
  yerr = new double[m_varValues.size()];
  pars = new double[npar];
  pfix = new bool[npar];
  for(k=0;k<(int)m_varValues.size();k++){
    x[k]    = (double)m_varValues[k];
    xerr[k] = 1e-5*(double)m_varValues[k];
  }
  
  double ymax = 0;
  bool Clow = true;
  // check if binomial errors are needed, and if so get #injections
  Config &scfg = getScanConfig();
  if(scfg["fe"]["chargeInjCapHigh"].name()!="__TrashConfObj__")
    Clow = !scfg["fe"]["chargeInjCapHigh"].valueBool();
  if(fracErr==0 && m_errHistos.size()==0){
    if(scfg["general"]["repetitions"].name()!="__TrashConfObj__")
      ymax = (double) scfg["general"]["repetitions"].valueInt();
  }

  int count = 0;
  for(int c=minChip;c<maxChip;c++){
    
    if(m_fitClass->hasVCAL(fittype)){
      // set VCAL calibration parameters
      std::stringstream a2;
      a2 << c;
      Config &mcfg = getModConfig().subConfig("PixFe_"+a2.str());
      if(mcfg["Misc"].name()!="__TrashConfObj__"){
	std::string cname = "CInjLo";
	if(!Clow) cname = "CInjHi";
	if(mcfg["Misc"][cname].name()=="__TrashConfObj__")
	  throw SctPixelRod::BaseException("PixDBData::FitHisto : Can't find "+cname+" in config of FE "+a2.str());
	float cap = ((ConfFloat&)mcfg["Misc"][cname]).value();
	cap /= 0.160218; // conversion C -> e
	for(int j=0;j<4;j++){
	  std::stringstream b;
	  b << j;
	  if(mcfg["Misc"]["VcalGradient"+b.str()].name()=="__TrashConfObj__")
	    throw SctPixelRod::BaseException("PixDBData::FitHisto : Can't find VcalGradient"+b.str()+" in config of FE "+a2.str());
	  pars[m_fitClass->getNPar(fittype)+j] = cap*(((ConfFloat&)mcfg["Misc"]["VcalGradient"+b.str()]).value());
	  pfix[m_fitClass->getNPar(fittype)+j] = true; // never change VCAL parameters in fit
	}
      }
    }
    for(int row=0;row<160;row++){
      for(int col=0;col<18;col++){
	// re-load non-VCAL parameters
	for(k=0;k<(int)m_parInit.size();k++){
	  pars[k] = m_parInit[k];
	  pfix[k] = m_parFixed[k];
	}
	// fill y-point for this pixel
	float xval, yval;
	PixXY(c,col,row,&xval,&yval);
	for(k=0;k<(int)m_varValues.size();k++){
	  y[k]    = (*(m_fitHistos[k]))((int)xval, (int)yval);
	  if(m_errHistos.size()!=0){
	    yerr[k] = (*(m_errHistos[k]))((int)xval, (int)yval);
	  }else if(fracErr)
	    yerr[k] = (double)fracErr*(*(m_fitHistos[k]))((int)xval, (int)yval);
	  else // binomial
	    yerr[k] = y[k]/ymax*(ymax-y[k]);
	}
	// run fit
	float chi;
	if(m_oldChiHisto==0 || (m_oldChiHisto!=0 && (*m_oldChiHisto)((int)xval, (int)yval) >= chicut)){
	  if(count<NDBG_PRNT){
	    printf("Fitting %d %d %d\n",chip, col, row);
	    if(m_oldChiHisto!=0)
	      printf("old chi2: %f\n",(*m_oldChiHisto)((int)xval, (int)yval));
	  }
	  chi = (float)m_fitClass->runFit((int)m_varValues.size(), x, y, xerr, yerr, pars, pfix, fittype, 
					  (double)xmin, (double)xmax, false);//(count<NDBG_PRNT));
	  if(count<NDBG_PRNT){
	    if(m_oldChiHisto!=0)
	      printf("new chi2: %f\n",chi);
	  }
	  count++;
	  for(int i=0;i<(int)m_fitClass->getNPar(fittype);i++){
	    if(chi<chicut)
	      (m_parHistos[i])->set((int)xval, (int)yval, pars[i]);
	    else
	      (m_parHistos[i])->set((int)xval, (int)yval, 0);
	  }
	  // convert chi2 to chi2/ndf if requested
	  if(((int)(m_varValues.size())-npar-1)>0 && useNdof)
	    chi  /= (float)((int)(m_varValues.size()-npar)-1);
	  (m_parHistos[m_fitClass->getNPar(fittype)])->set((int)xval, (int)yval, (double)chi);
	} else if(m_oldChiHisto!=0 && (*m_oldChiHisto)((int)xval, (int)yval) < chicut)
	  (m_parHistos[m_fitClass->getNPar(fittype)])->set((int)xval, (int)yval, -999);
      }
    }
  }

  delete[] x;
  delete[] y;
  delete[] xerr;
  delete[] yerr;
  delete[] pars;
  delete[] pfix;
}
void PixDBData::initFit(PixLib::PixScan::HistogramType type, int loop_level, float *pars, bool *pars_fixed, 
			int fittype, PixLib::PixScan::HistogramType errType, PixLib::PixScan::HistogramType chiType, 
			bool doCalib)
{
  clearFit(true);

  if(m_fitClass == 0)
    m_fitClass = new FitClass();

  int npars = m_fitClass->getNPar(fittype);
  if(npars==0)
    throw SctPixelRod::BaseException("PixDBData::initFit : Selected fit function is currently NOT supported.");

  if(pars==0)
    throw SctPixelRod::BaseException("PixDBData::initFit : Initial parameter settings must be provided during fit initialisation.");


  // initialise parameters
  for(int i=0;i<npars;i++){
    m_parInit.push_back(pars[i]);
    m_parFixed.push_back(pars_fixed[i]);
    std::stringstream b;
    b<< i;
    m_parHistos.push_back(new Histo("par"+b.str(),m_fitClass->getFuncName(fittype)+" par. "+b.str(),144,-0.5,143.5,
				    320,-0.5,319.5));
  }
  m_parHistos.push_back(new Histo("CHI2",m_fitClass->getFuncName(fittype)+" chi2",144,-0.5,143.5,320,-0.5,319.5));
  if(m_fitClass->hasVCAL(fittype) && !doCalib){
    m_parInit.push_back(0);
    m_parInit.push_back(1);
    m_parInit.push_back(0);
    m_parInit.push_back(0);
    for(int j=0;j<4;j++)
      m_parFixed.push_back(true);
  }

  // get scan points
  std::stringstream a;
  a << loop_level;
  if(getScanLevel()<=loop_level)
    throw SctPixelRod::BaseException("PixDBData::initFit : Requested loop level "+a.str()+" was not used in this scan.");
  Config &scfg = getScanConfig();
  std::string name = "loopVarValuesLoop_";
  name += a.str();
  m_varValues.clear();
  if(scfg["loops"][name].name()!="__TrashConfObj__"){
    ConfObj &obj = scfg["loops"][name];
    switch( obj.subtype() ){
    case ConfVector::V_INT:{
      std::vector<int> &tmpVec = ((ConfVector&)obj).valueVInt();
      for(std::vector<int>::iterator IT = tmpVec.begin(); IT!=tmpVec.end(); IT++)
	m_varValues.push_back((float)(*IT));
      break;}
    case ConfVector::V_UINT:{
      std::vector<unsigned int> &tmpVec = ((ConfVector&)obj).valueVUint();
      for(std::vector<unsigned int>::iterator IT = tmpVec.begin(); IT!=tmpVec.end(); IT++)
	m_varValues.push_back((float)(*IT));
      break;}
    case ConfVector::V_FLOAT:{
      std::vector<float> &tmpVec = ((ConfVector&)obj).valueVFloat();
      for(std::vector<float>::iterator IT = tmpVec.begin(); IT!=tmpVec.end(); IT++)
	m_varValues.push_back((float)(*IT));
      break;}
    default:
      throw SctPixelRod::BaseException("PixDBData::initFit : vector holding scan var. values is of unknown type.");
      break;
    }      
  } else
    throw SctPixelRod::BaseException("PixDBData::initFit : can't find config for requested loop.");
  
  // get all histograms
  for(int i=0;i<(int)m_varValues.size(); i++){
    Histo *ho = getGenericPixLibHisto(type, i, loop_level);
    if(ho==0)
      throw SctPixelRod::BaseException("PixDBData::initFit : one of the nec. data histos was not found.");
    else
      m_fitHistos.push_back(new Histo(*ho));
  }
  if(errType<PixLib::PixScan::MAX_HISTO_TYPES){
    for(int i=0;i<(int)m_varValues.size(); i++){
      Histo *ho = getGenericPixLibHisto(errType, i, loop_level);
      if(ho==0)
	throw SctPixelRod::BaseException("PixDBData::initFit : one of the nec. error histos was not found.");
      else
	m_errHistos.push_back(new Histo(*ho));
    }
  }
  if(chiType<PixLib::PixScan::MAX_HISTO_TYPES){
    Histo *ho = getGenericPixLibHisto(chiType, 0, loop_level+1);
    if(ho==0){
      //      printf("PixDBData::initFit : chi2 histo %d was not found on level %d.\n", (int)chiType, loop_level+1);
      m_oldChiHisto = 0;
    }else
      m_oldChiHisto = new Histo(*ho);
  }

}
void PixDBData::clearFit(bool clearParHisto)
{
  for(std::vector<PixLib::Histo*>::iterator it = m_fitHistos.begin(); it!=m_fitHistos.end(); it++){
    delete (*it); (*it)=0;
  }
  m_fitHistos.clear();
  for(std::vector<PixLib::Histo*>::iterator it = m_errHistos.begin(); it!=m_errHistos.end(); it++){
    delete (*it); (*it)=0;
  }
  m_errHistos.clear();
  if(clearParHisto){
    for(std::vector<PixLib::Histo*>::iterator it2 = m_parHistos.begin(); it2!=m_parHistos.end(); it2++){
      delete (*it2); (*it2)=0;
    }
    m_parHistos.clear();
  }
  delete m_fitClass; m_fitClass=0;
  delete m_oldChiHisto; m_oldChiHisto=0;
  m_parInit.clear();
  m_varValues.clear();
}

PixLib::Histo* PixDBData::getParHisto(int ipar)
{
  if(ipar<0 || ipar>=(int)m_parHistos.size()) return 0;

  return m_parHistos[ipar];
}
TH2F* PixDBData::getParHistoR(int ipar)
{
  Histo *h = getParHisto(ipar);
  if(h==0) return 0;
  // turn PixLib::Histo into ROOT TH2F
  delete m_2dHisto;
  gROOT->cd();
  m_2dHisto = new TH2F(h->name().c_str(),h->title().c_str(),
		       h->nBin(0),h->min(0), h->max(0),h->nBin(1),h->min(1), h->max(1));

  int i,j;
  for (i=0; i<h->nBin(0); i++) {
    for (j=0; j<h->nBin(1); j++) {
      float cont = m_2dHisto->GetBinContent(i+1,j+1) + (float)(*h)(i,j);
      m_2dHisto->SetBinContent(i+1,j+1,cont);
    }
  }
  return m_2dHisto;
}
PixLib::DbRecord* PixDBData::getResultsRecord()
{
  DbRecord *scandatRec = openDbFile(1,true);    //this should get us the PixScanData Record
  DbRecord *retRec = getRecord(scandatRec, "RESULTS","AnalysisResults");
  delete scandatRec;
  return retRec;
}

PixLib::DbRecord* PixDBData::getRecord(PixLib::DbRecord *parent, std::string dname, std::string name)
{
  std::unique_ptr<PixLib::DbRecord> retRec;
  dbRecordIterator ri;
  ri = parent->findRecord(dname+"/"+name);
  if(ri!=parent->recordEnd())
    retRec = std::unique_ptr<PixLib::DbRecord> ( *(parent->getDb()->DbProcess(ri, PixLib::PixDb::DBREAD)));
  else{
    retRec = std::unique_ptr<PixLib::DbRecord> ( parent->getDb()->makeRecord(name, dname) );
    retRec = std::unique_ptr<PixLib::DbRecord> (*(parent->getDb()->DbProcess(parent->pushRecord(retRec.get()), PixLib::PixDb::DBREAD)));
  }
  return retRec.release();
}

void PixDBData::writePixLibHisto(PixLib::Histo *in_hi, PixLib::PixScan::HistogramType type, int scanpt, int scan_level)
{
  std::string rname, hname = getHistoName(type);
  std::string levLabel[4]={"A","B","C",""};

  if(in_hi==0) return;

  // get config and remember for later -> is stored in m_ps
  getScanConfig();

  PixLib::DbRecord *scandatRec=0, *topRec=0, *subRec[4]={0,0,0,0};
    scandatRec = openDbFile(1,true);    //this should get us the PixScanData Inquire

    // create record structure for requested level
    topRec = getRecord(scandatRec, hname, hname);
    std::stringstream a;
    a << m_modID;
    hname = "Mod"+a.str();
    subRec[0] = getRecord(topRec, hname, hname);
    for(int i=0;i<(3-scan_level);i++){
      hname += "_"+levLabel[i]+"0";
      subRec[i+1] = getRecord(subRec[i], hname, hname);
    }
    // overwrite/create field for histo
    hname += "_0";
    PixLib::dbFieldIterator fit = subRec[3-scan_level]->findField(hname);
    std::unique_ptr<PixLib::DbField> hfield;
    if(fit!=subRec[3-scan_level]->fieldEnd())
      hfield = std::unique_ptr<PixLib::DbField> ( *(m_Dbfile->DbProcess(fit, PixLib::PixDb::DBREAD)));
    else
      hfield = std::unique_ptr<PixLib::DbField> ( m_Dbfile->makeField(hname) );
    m_Dbfile->DbProcess(hfield.get(),PixLib::PixDb::DBCOMMIT,*in_hi);
    if(fit==subRec[3-scan_level]->fieldEnd())
      subRec[3-scan_level]->pushField(hfield.release());

  // add this histo to config as filled and kept
  Config &conf = m_ps->config();
  hname = getHistoName(type);
  if(conf["histograms"]["histogramFilled"+hname].name()!="__TrashConfObj__")
	  ((ConfBool&)conf["histograms"]["histogramFilled"+hname]).m_value = true;
  if(conf["histograms"]["histogramKept"+hname].name()!="__TrashConfObj__")
	  ((ConfBool&)conf["histograms"]["histogramKept"+hname]).m_value = true;
  m_ps->writeConfig(scandatRec); // writes to file
  // close file in write mode, open again in read-only
  openDbFile(1);
  for(int i=0;i<4;i++) delete subRec[i];
  delete topRec;
  delete scandatRec;
}
int PixDBData::getPixScanID(int stdScanID)
{
  switch (stdScanID){
  case  1:
    return (int)PixScan::DIGITAL_TEST;
  case  6:
    return (int)PixScan::THRESHOLD_SCAN;
  case  8:
    return (int)PixScan::CROSSTALK_SCAN;
  case  9:
    return (int)PixScan::TOT_CALIB;
  case 12:
    return (int)PixScan::TIMEWALK_MEASURE;
  case 22:
    return (int)PixScan::INTIME_THRESH_SCAN;
  case 36:
    return (int)PixScan::T0_SCAN;
  }
  return -1;
}

PixLib::Histo* PixDBData::getGenericPixLibHisto(int type, const int scanpt[4]) {
  if (type>=PixLib::PixScan::MAX_HISTO_TYPES) return 0;
  return getGenericPixLibHisto(static_cast<PixLib::PixScan::HistogramType>(type),scanpt);
}

bool PixDBData::haveHistoType(int type)
{
  return haveHistoType((PixLib::PixScan::HistogramType) type);
}
