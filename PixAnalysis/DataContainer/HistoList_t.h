// Dear emacs, this is -*-c++-*-
#ifndef _PixA_HistoList_t_h_
#define _PixA_HistoList_t_h_

#include <vector>
#include <map>
#include "HistoRef.h"
#include "MakePixScanHistoName.h"
#include <ConfigWrapper/ScanConfig.h>
#include <DataContainer/HistoUtil.h>
#include <memory>

#include <sstream>

#ifdef _PIXA_HISTO_IS_ROOT_TH1
#  include <TH1.h>
#  include <Histo/Histo.h>
#endif

namespace PixA {

  typedef std::vector<unsigned int> Index_t;

  class PixScanHistoElm_t
  {
  public:

    PixScanHistoElm_t(const PixA::ScanConfigRef  &scan_config, PixA::EModuleNameingConvention naming_scheme = PixA::kUnknownModuleNaming)
      : m_scanConfig(scan_config),
	m_namingScheme(naming_scheme),
	m_moduleId(s_invalidModuleId)
    {
      std::cout << "INFO [PixCon::PixScanHistoElm_t::ctor] " << static_cast<void *>(this) << std::endl;
    }


    ~PixScanHistoElm_t() {
      std::cout << "INFO [PixCon::PixScanHistoElm_t::dtor] " << static_cast<void *>(this) << std::endl;
    }


    void addHisto(unsigned int histo_id, const PixA::HistoHandle &histo_handle)
    {
      // verify that the id is reasonable
      assert (histo_id < m_histoHandle.size()+1000);
      std::cout << "INFO [PixCon::PixScanHistoElm_t::addHisto] " << static_cast<void *>(this) << " histo id = " << histo_id 
		<< ", old list size = " << m_histoHandle.size() << std::endl;
      if (histo_id>=m_histoHandle.size()) {
	m_histoHandle.resize(histo_id+1);
      }
      else {
	if (m_histoHandle[histo_id]) {
	  std::stringstream message;
	  message << "FATAL [PixScanHistoElm_t::addHisto] histogram " << histo_id << "does exist already.";
	  throw std::runtime_error(message.str());
	}
      }
      m_histoHandle[histo_id]=histo_handle;
    }

    template <class THisto_t> THisto_t *loadHistogram(unsigned int histo_id, const std::string &histo_name, const Index_t &index)
    {
      PixLib::Histo *a_histo_ptr = _loadHistogram(histo_id, histo_name, index);
      if (a_histo_ptr) {
	return transform<THisto_t>(a_histo_ptr);
      }
      else {
	return NULL;
      }
    }

    template <class THisto_t> static THisto_t *transform(PixLib::Histo *a_histo_ptr);

    void numberOfHistograms(unsigned int histo_id, const std::string &histo_name, HistoInfo_t &info) 
    {
      std::vector<std::string> full_histo_name;
      PixA::HistoRef histo = histoRef( histo_id, histo_name, full_histo_name);
      if ( histo ) {
	histo.numberOfHistos(full_histo_name,info);
      }
      else {
	info.reset();
      }
    }

    PixA::EModuleNameingConvention moduleNameingScheme() const {return m_namingScheme;}
    void setModuleNameingScheme(PixA::EModuleNameingConvention naming_scheme, unsigned int module_id) { m_namingScheme = naming_scheme; m_moduleId = module_id; }
    unsigned int moduleId() const { return m_moduleId; }

    PixA::ScanConfigRef  &scanConfig() { return m_scanConfig; }
    PixA::HistoHandle &histoHandle(unsigned int index)   { assert(index <m_histoHandle.size());  return m_histoHandle[index]; }

    unsigned nHistos() const { return m_histoHandle.size(); }

protected:
    PixA::HistoRef histoRef(unsigned int histo_id, const std::string &histo_name, std::vector<std::string> &full_histo_name)
    {
      if (histo_id < nHistos()) {
	PixA::HistoHandle histo_handle(histoHandle(histo_id));
	if (histo_handle) {
	  PixA::HistoRef histo( histo_handle.ref());
	  std::pair<PixA::EModuleNameingConvention, unsigned int> ret =
	    PixA::makePixScanHistoName( moduleNameingScheme(),
					moduleId(),
					scanConfig(),
					histo,
					stripPath(histo_name),
					full_histo_name );
	  setModuleNameingScheme(ret.first,ret.second);
	  return histo;
	}
      }
      return PixA::HistoHandle().ref();
    }

    static std::string stripPath(const std::string &histo_name);

    PixLib::Histo *_loadHistogram(unsigned int histo_id, const std::string &histo_name, const Index_t &index)
    {
      std::vector<std::string> full_histo_name;
      PixA::HistoRef histo = histoRef( histo_id, histo_name, full_histo_name);
      if ( histo ) {
	//	int index_arr[4];
	//	unsigned int counter_i=0;
// 	for (;counter_i<4 && counter_i < index.size(); counter_i++) {
// 	  index_arr[counter_i]=index[counter_i];
// 	}
// 	for (;counter_i<4; counter_i++) {
// 	  index_arr[counter_i]=-1;
// 	}
	PixLib::Histo *a_histo_ptr = histo.histo(full_histo_name, index);
	return a_histo_ptr;
      }
      return NULL;
    }


  private:
    std::vector<PixA::HistoHandle>    m_histoHandle;
    PixA::ScanConfigRef  m_scanConfig;
    PixA::EModuleNameingConvention m_namingScheme;
    unsigned int m_moduleId;
    unsigned int m_counter;

    static const unsigned int s_invalidModuleId = static_cast<unsigned int >(-1);
  };

  //  typedef std::vector< PixScanHistoElm_t > HistoIndex_t;
  typedef std::shared_ptr<PixScanHistoElm_t> PixScanHistoElmPtr_t;
  typedef std::map< std::string , PixScanHistoElmPtr_t > ModuleIndex_t;

  class HistoList_t {
  public:
    HistoList_t() {}

    unsigned int histoId(const std::string &histo_name) const {
      unsigned int histo_id = findHistoName(histo_name);
      if (histo_id < m_histoMap.size()) {
	return histo_id;
      }
      return invalidHisto();
    }

    void addHisto(const std::string &histo_name,
		  const PixA::ScanConfigRef &scan_config,
		  const std::string &module_name,
		  const PixA::HistoHandle &histo_handle);

    void addPlainHisto(const std::string &histo_name,
		       const PixA::ScanConfigRef &scan_config,
		       const std::string &module_name,
		       const PixA::HistoHandle &histo_handle);

    unsigned int createHistoId(const std::string &histo_name);

    static unsigned int invalidHisto() { return s_invalidHisto; }

    PixScanHistoElmPtr_t createModuleHistoList(const std::string &module_name, const PixA::ScanConfigRef &scan_config);

    PixScanHistoElmPtr_t moduleHistoList(const std::string &module_name) {
      ModuleIndex_t::iterator module_iter = m_moduleIndex.find(module_name);
      if (module_iter != m_moduleIndex.end() ) {
	return module_iter->second;
      }
      return PixScanHistoElmPtr_t();
    }

    const PixScanHistoElmPtr_t moduleHistoList(const std::string &module_name) const {
      ModuleIndex_t::const_iterator module_iter = m_moduleIndex.find(module_name);
      if (module_iter != m_moduleIndex.end() ) {
	return module_iter->second;
      }
      return PixScanHistoElmPtr_t();
    }

    template <class THisto_t> THisto_t *loadHistogram(const std::string &module_name, const std::string &histo_name, const Index_t &index)
    {
      PixScanHistoElmPtr_t histo_list_ptr = moduleHistoList(module_name);
      if (histo_list_ptr.get()) {
	return histo_list_ptr->loadHistogram<THisto_t>(histoId(histo_name),histo_name, index);
      }
      return NULL;
    }

    void numberOfHistograms(const std::string &module_name, const std::string &histo_name, HistoInfo_t &info) 
    {
      PixScanHistoElmPtr_t histo_list_ptr = moduleHistoList(module_name);
      if (histo_list_ptr.get()) {
	histo_list_ptr->numberOfHistograms(histoId(histo_name),histo_name, info);
      }
      else {
	info.reset();
      }
    }


    std::vector< std::string > &nameList()             { return m_histoMap; }
    const std::vector< std::string > &nameList() const { return m_histoMap; }

    ModuleIndex_t::const_iterator begin() const        { return m_moduleIndex.begin(); }
    ModuleIndex_t::const_iterator end()  const         { return m_moduleIndex.end(); }

    ModuleIndex_t::size_type size() const              { return m_moduleIndex.size(); }

  protected:
    unsigned int findHistoName(const std::string &histo_name) const {
      unsigned int id_i;
      for (id_i=0; id_i < m_histoMap.size(); id_i++) {
	if (m_histoMap[id_i] == histo_name) break;;
      }
      return id_i;
    }

    static const unsigned int s_invalidHisto = static_cast<unsigned int>(-1);
    std::vector< std::string >           m_histoMap;
    ModuleIndex_t                        m_moduleIndex;
  };

  template <> inline PixLib::Histo *PixScanHistoElm_t::transform<PixLib::Histo>(PixLib::Histo *a_histo_ptr)
  {
    return a_histo_ptr;
  }

#ifdef ROOT_TH1
  template <> inline TH1 *PixScanHistoElm_t::transform<TH1>(PixLib::Histo *a_histo_ptr)
  {
    std::unique_ptr<PixLib::Histo> safe_histo_ptr(a_histo_ptr);
    std::unique_ptr<TObject> obj( PixA::transform(safe_histo_ptr.get()) );
    if (obj->InheritsFrom(TH1::Class())) {
      ::TH1 *h1=static_cast< TH1 *>(obj.get());
      obj.release();
      return h1;
    }
    return NULL;
  }
#endif

}

#endif
