#include "ScanResultStreamer.h"
#include "IScanResultListener.h"
#include "PixDbDataContainer.h"
#include "HistoRef.h"
#include "HistoInfo_t.h"
#include "MakePixScanHistoName.h"
#include <iostream>
#include <PixController/PixScan.h>

#include <ConfigWrapper/ConfObjUtil.h>
#include <ConfigWrapper/ConfigHandle.h>

#include <cstdlib>

#include <time.h>
 
#ifndef TrimVal_t_h
#define TrimVal_t_h

namespace PixLib {
  typedef unsigned short TrimVal_t;
}

#endif


void help(const char *arg0)
{
  std::cout << arg0 << " [-i] [-l] file_name [label_name...] [file_name [label_name ...]]." <<std::endl;
}

class ScanResultSpy : public PixA::IScanResultListener 
{
public:

  void newFile(const std::string& file_name) {
    std::cout << "INFO [ScanResultSpy::newFile]  file = " << file_name << std::endl;
  }

  void newLabel(const std::string &name, const PixA::ConfigHandle &label_config) {
    std::string comment;
    std::string time_stamp;
    unsigned int scan_number=0;
    if (label_config) {
      PixA::ConfigRef label_config_ref = label_config.ref();
     try {
       comment = confVal<std::string>(label_config_ref[""]["Comment"]);
       scan_number = confVal<unsigned int>(label_config_ref[""]["ScanNumber"]);
       time_stamp = confVal<std::string>(label_config_ref[""]["TimeStamp"]);
       
     }
     catch (PixA::ConfigException &err)
     {
        // catch exceptions which are thrown if a field does not exist e.g. ScanNumber
     }
    }
    std::cout << "INFO [ScanResultSpy::newLabel] ";
    if (scan_number>0) {
      std::cout << scan_number;
    }
    std::cout << " : \"" << name << "\"";
    if (!time_stamp.empty()) {
      std::cout << "  - " << time_stamp;
    }
    if (!comment.empty()) {
      std::cout << "  :: " << comment << "";
    }
    std::cout << std::endl;
  }

  /** Called whenever the pix module group changes.
   */
  void newPixModuleGroup(const std::string &name) {
    std::cout << "INFO [ScanResultSpy::newPixModuleGroup] pix module group=\"" << name << "\"" << std::endl;
  }

  /** Called whenever the pix module group changes.
   */
  void finishPixModuleGroup() {
    std::cout << "INFO [ScanResultSpy::finishPixModuleGroup] done with pix module group." << std::endl;
  }

  /** Called whenever the Pp0 changes.
   * newPp0 will never be called without newPixModuleGroup being called.
   */
  void newPp0(const std::string &name) {
    std::cout << "INFO [ScanResultSpy::newPp0] PP0=\"" << name << "\"" << std::endl;
  }

  void finishPp0() {
    std::cout << "INFO [ScanResultSpy::finishPp0] done with PP0." << std::endl;
  }


  /** Called whenever the result folder changes
   * @param folder_list the names of the folder hierarchy
   */
  void newFolder(const std::vector<std::string> &folder_hierarchy) {
    std::cout << "INFO [ScanResultSpy::newFolder] path=\"";
    for (std::vector<std::string>::const_iterator folder_iter = folder_hierarchy.begin();
	 folder_iter!=folder_hierarchy.end();
	 folder_iter++) {
      std::cout << *folder_iter << "/";
    }
    std::cout << "\"" << std::endl;
  }

  /** Called when a new scan info record is found
   * @param scan_info handle for the scan info config object.
   */
  void newScanInfo(const PixA::ConfigHandle &scan_info_handle) {
    try {
      PixA::ConfigRef  scan_info( scan_info_handle.ref() );

      {
      PixA::ConfGrpRef tags(scan_info["Tags"]);
      std::string tag_conn  = confVal<std::string>(tags["Conn"]);
      std::string tag_alias = confVal<std::string>(tags["Alias"]);
      std::string tag_data  = confVal<std::string>(tags["Data"]);
      std::string tag_cfg   = confVal<std::string>(tags["Cfg"]);
      unsigned int revision;
      try {
	revision = confVal<unsigned int>(tags["CfgRev"]);
      }
      catch (PixA::ConfigTypeMismatch &) {
	revision = static_cast< unsigned int>( confVal<int>(tags["CfgRev"]) );
      }

      std::cout << "INFO [ScanResultSpy::newScanInfo] tags: conn  = " << tag_conn 
		<< " alias = " << tag_alias
		<< " data  = " << tag_data
		<< " cfg   = " << tag_cfg
		<< " cfg Rev.  = " << revision
		<< std::endl;
      }

      {
      PixA::ConfGrpRef logs(scan_info["Logs"]);
      std::string log_comment  = confVal<std::string>(logs["Comment"]);
      std::string log_scan_type = confVal<std::string>(logs["ScanType"]);
      unsigned int  log_scan_id;
      try {
	log_scan_id = confVal<unsigned int>(logs["ScanId"]);
      }
      catch (PixA::ConfigTypeMismatch &) {
	log_scan_id = static_cast< unsigned int>( confVal<int>(logs["ScanId"]) );
      }

      std::cout << "INFO [ScanResultSpy::newScanInfo] logs: id  = " << log_scan_id
		<< " type = " << log_scan_type
		<< " comment  = " << log_comment
		<< std::endl;
      }

      {
      PixA::ConfGrpRef time(scan_info["Time"]);
      std::string start_time_string  = confVal<std::string>(time["ScanStart"]);
      std::string end_time_string  = confVal<std::string>(time["ScanEnd"]);

      struct tm start_tm;
      strptime(start_time_string.c_str(),"%Y-%m-%d %H:%M:%S", &start_tm);

      struct tm end_tm;
      strptime(end_time_string.c_str(),"%Y-%m-%d %H:%M:%S", &end_tm);
      time_t start_time = mktime(&start_tm);
      time_t end_time = mktime(&end_tm);

      std::cout << "INFO [ScanResultSpy::newScanInfo] time: start  = " << start_time_string
		<< " end = " << end_time_string
		<< " duration = " << end_time - start_time
		<< std::endl;
      }

    }
    catch(...) {
    }

  }


  /** Does intentionally nothing.
   */
  void newPixScan(const PixA::ConfigHandle &pix_scan_config) {
    std::cout << "INFO [ScanResultSpy::newPixScan] New scan configuration." << std::endl;
  }


  //  /** Called whenever the scan changes
  //   * @param folder_list the names of the folder hierarchy
  //   */
  //   void newScan(const ScanConfigRef &scan_config) = 0;
  
  /** Called for each pix scan histo.
   */
  void newPixScanHisto(const std::string &histo_name,
		       const PixA::ScanConfigRef &scan_config, 
		       const PixA::HistoHandle &histo_handle) {
    PixA::HistoHandle a_histo_handle(histo_handle);
    PixA::PixDbDataContainer container(scan_config,
				       histo_handle);
    int histo_type = getHistoType(histo_name);


    std::cout << "INFO [ScanResultSpy::newPixScanHisto] module=";
    if (!scan_config.connName().empty()) {
      std::cout << scan_config.connName() << "(" << scan_config.prodName() << ")";
    }
    else {
      std::cout << scan_config.prodName();
    }
    std::cout << " histo=\"" << histo_name << "\"" << std::endl;
    PixA::HistoRef histo(a_histo_handle.ref());
    HistoInfo_t info;
    //    histo.numberOfHistos(histo_name, info);
    container.numberOfHistos(histo_type,info);
    std::cout << "idx:";
    for (int i=0; i<3; i++) {
      std::cout << info.maxIndex(i) << (i<2 ? "," : "");
    }
    std::cout << " n histos=" << info.minHistoIndex() << " - " << info.maxHistoIndex() << std::endl;

    PixA::Index_t index;
    for (int i=0; i<3; i++) {
      if (info.maxIndex(i)<=0) break;
      index.push_back(rand()%info.maxIndex(i));
    }
    index.push_back(info.minHistoIndex() + rand()%(info.maxHistoIndex() - info.minHistoIndex()));
    std::cout << "INFO [ScanResultSpy::newPixScanHisto] get histo : ";
    for (unsigned int i=0; i<index.size(); i++) {
      std::cout << index[i] << " ";
    }
    std::cout << std::endl;

    std::vector<std::string> full_histo_name;
    PixA::makePixScanHistoName(PixA::kUnknownModuleNaming,static_cast<unsigned int>(-1),scan_config,histo,histo_name,full_histo_name);

    histo.histo(full_histo_name,index);

    for (unsigned int loop_i=0; loop_i<3; loop_i++) {
      std::cout << loop_i << " : " << container.getScanPar(loop_i) << " :: "
		<< container.getScanStart(loop_i) << " - " << container.getScanStop(loop_i) << " / " << container.getScanSteps(loop_i) 
		<< std::endl;
    }
    try {
      std::cout << " events = " << container.getNevents() << std::endl;
      std::cout << " Boc link RX " 
		<< scan_config.inOrderBocLinkRx(PixA::ScanConfig::kDTO1) 
		<< " " << scan_config.inOrderBocLinkRx(PixA::ScanConfig::kDTO2) << std::endl;
    }
    catch (PixA::ConfigException &err) {
      std::cout << "WARNING [ScanResultSpy::newPixScanHisto] Caught exception : " << err << "." << std::endl;
    }
  }
 
    /** Called for each normal histo.
     */
  void newHisto(const std::string &name, const PixA::HistoHandle &histo_handle, const std::string &rod_name, const PixA::ConfigHandle &scan_config) {
    PixA::HistoHandle a_histo_handle(histo_handle);
    std::cout << "INFO [ScanResultSpy::newHisto] module=";
    std::cout << " histo=\"" << name << "\"" << std::endl;
    PixA::HistoRef histo(a_histo_handle.ref());
    HistoInfo_t info;
    histo.numberOfHistos(name, info);
    std::cout << "idx:";
    for (int i=0; i<3; i++) {
      std::cout << info.maxIndex(i) << (i<2 ? "," : "");
    }
    std::cout << " n histos=" << info.minHistoIndex() << " - " << info.maxHistoIndex() << std::endl;
  }

  void finish() {
    std::cout << "INFO [ScanResultSpy::finish] all done." << std::endl;
  }

  void reportProblem(const std::string &message) {
    std::cout << "INFO [ScanResultSpy::reportProblem] : " << message << std::endl;
  }

private:
  static void initHistoNameTypeList();
  static int getHistoType(const std::string &histo_name);
  static std::map< std::string, int > s_histoNameToType;
};

std::map< std::string, int> ScanResultSpy::s_histoNameToType;

int ScanResultSpy::getHistoType(const std::string &histo_name)
{
  if (s_histoNameToType.empty()) initHistoNameTypeList();
  std::map< std::string, int >::const_iterator iter = s_histoNameToType.find(histo_name);
  if (iter != s_histoNameToType.end()) {
    return iter->second;
  }
  return -1;
}


void ScanResultSpy::initHistoNameTypeList() {
  //  if (s_histoTypeToName.empty()) {
  s_histoNameToType.clear();
  std::unique_ptr<PixLib::PixScan> ps(new PixLib::PixScan());
  s_histoNameToType = ps->getHistoTypes();
  //  }
}


int main(int argc, char **argv) 
{
  if (argc<2 ) {
    help((argc>0 ? argv[0] : "?"));
    return -1;
  }

  int arg_i=1;
  bool ignore_conn=false;
  bool list_files=false;
  while (arg_i<argc && argv[arg_i] && argv[arg_i][0]=='-') {
    if (strcmp(argv[arg_i],"-i")==0) {
      ignore_conn=true;
    }
    else if (strcmp(argv[arg_i],"-l")==0) {
      list_files=true;
    }
    else {
      help((argc>0 ? argv[0] : "?"));
      return -1;
    }
    arg_i++;
  }
  if (arg_i>=argc) {
    help((argc>0 ? argv[0] : "?"));
    return -1;
  }
  try {

    for (; arg_i<argc; arg_i++) {
      //  if (stream_all) {
      std::string file_name;
      try {
	bool labels_follow=false;
	bool first=true;
	std::vector<std::string> label;
	if (!labels_follow) {
	  for(; arg_i<argc; arg_i++) {
	    std::string a_name=argv[arg_i];
	    if (a_name.find(".root")==std::string::npos) {
	      label.push_back(a_name);
	    }
	    else {
	      if (label.empty() && first) {
		labels_follow=true;
	      }
	      file_name = a_name;
	      break;
	    }
	  }
	  first=false;
	}

	if (!file_name.empty()) {
	  PixA::ScanResultStreamer streamer(file_name);
	  ScanResultSpy listener;
	  streamer.setListener( listener );
	  if (ignore_conn) {
	    streamer.setIgnoreConnAndCfg(true);
	  }
	  if (labels_follow) {
	    for(; arg_i<argc; arg_i++) {
	      std::string a_name=argv[arg_i];
	      if (a_name.find(".root")==std::string::npos) {
		label.push_back(a_name);
	      }
	      else {
		break;
	      }
	    }
	  }
	  if (list_files) {
	    streamer.readLabels();
	  }
	  else {
	    if (label.empty()) {
	      streamer.read();
	    }
	    else {
	      for (std::vector<std::string>::const_iterator label_iter = label.begin();
		   label_iter != label.end();
		   label_iter++) {
		streamer.readLabel(*label_iter);
	      }
	    }
	  }
	}
      }
      catch (PixA::ConfigException &err) {
	std::cout << "FATAL [main] " << file_name << " : caught exception (ConfigException) : " << err.getDescriptor() << std::endl;
      }
      catch (PixA::BaseException &err) {
	std::cout << "FATAL [main] " << file_name << " : caught exception (BaseException) : " << err.getDescriptor() << std::endl;
      }
      catch (std::exception &err) {
	std::cout << "FATAL [main] " << file_name << " : caught exception (std::exception) : " << err.what() << std::endl;
      }
      catch (...) {
	std::cout << "FATAL [main] " << file_name << " : unhandled exception. "  << std::endl;
      }
    }
    //   else {
    //     PixA::ScanResultStreamer streamer(argv[arg_i++]);
    //     if (ignore_conn) {
    //       streamer.setIgnoreConnAndCfg(true);
    //     }
    //     streamer.setListener(new ScanResultSpy);
    //     if (arg_i==argc) {
    //       streamer.readLabels();
    //     }
    //     else {
    //       for (; arg_i<argc; arg_i++) {
    // 	streamer.readLabel(argv[arg_i]);
    //       }
    //     }
    //   }
  }
  catch (PixA::ConfigException &err) {
    std::cout << "FATAL [main] caught exception (ConfigException) : " << err.getDescriptor() << std::endl;
  }
  catch (PixA::BaseException &err) {
    std::cout << "FATAL [main] caught exception (BaseException) : " << err.getDescriptor() << std::endl;
  }
  catch (std::exception &err) {
    std::cout << "FATAL [main] caught exception (std::exception) : " << err.what() << std::endl;
  }
  catch (...) {
    std::cout << "FATAL [main] unhandled exception. "  << std::endl;
    throw;
  }


}
