#ifndef _LevelInfoBase_t
#define _LevelInfoBase_t

#include <string>

class LevelInfo_t;
class LevelInfoOld_t;

/** Auxilliary class to walk throught the hierarchy of PixScanHistos.
 * used by @ref PixDbHistoAccessor::numberOfHistos,  RootDBHistoAccessor::numberOfHistos
 */
class LevelInfoBase_t
{
//  friend class LevelInfo_t;
//  friend class LevelInfoOld_t;
//protected:
 public:
  //  LevelInfo_t() : m_level(0),m_index(0) {};
  //  LevelInfo_t(level LevelInfo_t &a) : m_name(a.m_name), m_level(a.m_level),m_index(a.m_),m_levelRecord(record) { m_name += '_';};
  LevelInfoBase_t(const std::string &name) : m_name(name), m_level(0),m_index(0)
  { if (!m_name.empty()) m_name += '_';};
  LevelInfoBase_t(const std::string &name, unsigned int level_i) : m_name(name), m_level(level_i),m_index(0)
  { if (!m_name.empty()) m_name += '_';};

  const std::string &name() const {return m_name;}
  std::string &name()             {return m_name;}

  unsigned int level()  const          {return m_level;}
  const char *levelName()  const       {return s_levelName[m_level];}

  static const unsigned int maxLevel() {return s_maxLevel;};
  static const char *levelName(unsigned int level)    {return s_levelName[level];}

  const unsigned int &index() const    {return m_index;};
  unsigned int &index()                {return m_index;};

  std::string  m_name;
  unsigned int m_level;
  unsigned int m_index;

  static const unsigned s_maxLevel = 3;
  static const char *s_levelName[4];
};

#endif
