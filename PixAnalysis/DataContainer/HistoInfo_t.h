#ifndef _HistoInfo_t_h_
#define _HistoInfo_t_h_

#include <vector>

namespace PixA {
  /** An index array to access histograms in a pix scan histogram tree.
   */
  typedef std::vector<unsigned int> Index_t;
}

/** Class which tells the dimensions of each level of a PixScanHisto histogram tree.
* A pix scan histo is a histogram tree. Here it is expected that only the
* lowest level of the tree contains histograms.
*/
class HistoInfo_t
{
 public:

  /** Reset to a histogram tree without branches and histograms.
   */
  void reset() {
    for (unsigned int i=0; i<4; i++) {m_maxIndex[i]=0;} 
    m_minHistoIndex = 0;
  }

  /** Set the minimum index of the histograms in the lowest level.
   */
  void setMinHistoIndex(unsigned int min_histo_index) {m_minHistoIndex = min_histo_index;}

  /** Set the maximum index (one above the maximum allowed index) of histograms in the lowest level.
   */
  void setMaxHistoIndex(unsigned int max_histo_index) {m_maxIndex[3]   = max_histo_index;}

  /** Get the lowest index for which there are histograms in the histogram tree (read write). 
   */
  unsigned int &minHistoIndex()             { return m_minHistoIndex ;}

  /** Get the lowest index for which there are histograms in the histogram tree. 
   */
  const unsigned int &minHistoIndex() const { return m_minHistoIndex ;}

  /** Get one above the highes index for which there are histograms in the histogram tree. 
   */
  const unsigned int &maxHistoIndex() const { return m_maxIndex[3]   ;}

  /** Get one above the highes index for which there are histograms in the histogram tree (read write). 
   */
  unsigned int &maxHistoIndex()             { return m_maxIndex[3]   ;}


  /** Combine two HistoInfo_t objects such that the ranges of both HistoInfo_t objects are contained.
   */
  void makeUnion(const HistoInfo_t &a_info) {
    if (m_minHistoIndex>=m_maxIndex[3] || m_minHistoIndex>a_info.m_minHistoIndex) {
      m_minHistoIndex=a_info.m_minHistoIndex;
    }
    for (unsigned int i=0; i<4; i++) {
      if (m_maxIndex[i]<a_info.m_maxIndex[i]) m_maxIndex[i]=a_info.m_maxIndex[i];
    }
  }

  /** Set the maximum index (one above) of the given level.
   * There are no branches at this live with this or an higher index in the histogram tree.
   */
  void setMaxIndex(unsigned int level, unsigned int max_histo_index) {m_maxIndex[level]   = max_histo_index;}

  /** Create an index array which accesses the histogram with all indices set to the lowest value.
   */
  void makeDefaultIndexList(PixA::Index_t &index) const {
    index.clear();
    for (unsigned int i=0; i<3; i++) {
      if (m_maxIndex[i]>0) {
	index.push_back(0);
      }
    }
    if (m_minHistoIndex < m_maxIndex[3]) {
      index.push_back(m_minHistoIndex);
    }
  }

  /** Get the maximum index (one above) of the given level.
   * There are no branches at this live with this or an higher index in the histogram tree.
   */
  const unsigned int &maxIndex(unsigned int level) const {return m_maxIndex[level];}

  /** Get the maximum index (one above) of the given level (read write).
   * There are no branches at this live with this or an higher index in the histogram tree.
   */
  unsigned int &maxIndex(unsigned int level)             {return m_maxIndex[level];}

  /** Get the maximum number of leveles including the lowest level which contains the histograms.
   * There are no branches at this live with this or an higher index in the histogram tree.
   */
  static unsigned int maxLevels() { return s_nMaxLevels; }

  static const unsigned int s_nMaxLevels = 4;

  unsigned int m_maxIndex[4];
  unsigned int m_minHistoIndex;
};



#endif
