#ifndef _PixDbDataContainer_h_
#define _PixDbDataContainer_h_

#include <TKey.h>
#include <TSystem.h>
#include <PixController/PixScan.h>
#include <string>
#include <vector>

#include "PixelDataContainer.h"
#include "ConfigWrapper/Connectivity.h"
#include "ConfigWrapper/ConfigHandle.h"
#include "ConfigWrapper/ScanConfig.h"

#include "HistoInfo_t.h"
#include "HistoRef.h"

class FitClass;
class TH2F;
class TH1F;
class TObject;
class TGrahpErrors;
namespace PixLib{
  class PixDbInterface;
  class DbRecord;
  class Config;
  class Histo;
  class PixModule;
}


/*! This class provides easy access to scan data
 *  such as configuration information or histograms
 * @authors Joern Grosse-Knetter <joern.grosse-knetter@uni-bonn.de>
 */

namespace PixA {

  class PixDbDataContainer : public PixelDataContainer {

  private:
    /** Common initialisation.
     * should only be called by constructors.
     */
    void init();
  public:
    PixDbDataContainer(const PixA::ScanConfigRef &scan_config,
		       const PixA::HistoHandle &histo);

    ~PixDbDataContainer();


    /*! returns the scan level,
     * i.e. 0 for no scan, 1 for 1D, 2 for 2D scan etc
     */
    int getScanLevel();
    /*! returns number of events, i.e. number of injections 
     *  followed by LVL1 trigger(s)
     */
    int getNevents();
    /*! returns number of scan steps on requested loop level
     */
    int getScanSteps(int loopLevel);
    /*! returns scan start value on requested loop level
     */
    int getScanStart(int loopLevel);
    /*! returns scan start value on requested loop level
     */
    int getScanStop (int loopLevel);
    /*! returns the name of the scanned variable
     */
    std::string getScanPar(int loopLevel);

    /*! Returns a TH2F-map of PixScan::HistogramType "type"
     *  if chip>0 than the map for only this chip is returned;
     *  in case of several histograms for the various scan points,
     *  this is "guesssed"     <br>
     *  BEWARE: the object returned is temporary and will be deleted
     *          by most calls to other PixDBData functions; use the
     *          resp. copy constructor to retain a permanent object
     */
    TH2F* GetMap(int chip, int type){return GetMap(chip,type,-1);};
    /*! Returns a TH2F-map of PixScan::HistogramType "type"
     *  if chip>0 than the map for only this chip is returned;
     *  in case of several histograms for the various scan points,
     *  this is specified by the scanot argument   <br>
     *  BEWARE: the object returned is temporary and will be deleted
     *          by most calls to other PixDBData functions; use the
     *          resp. copy constructor to retain a permanent object
     */
    TH2F* GetMap(int chip, int type, int scanpt);
    /*! reduce module-wide current map to chip-map
     *  NB: map stored in memory is replaced!
     */
    TH2F* getChipMap(int chip);
    /*! Returns a TH2F-map of type "type" from a 2D scan
     *  (e.g. TDAC) for the given scan point
     *  if chip>0 than the map for only this chip is returned    <br>
     *  BEWARE: the object returned is temporary and will be deleted
     *          by most calls to other PixDBData functions; use the
     *          resp. copy constructor to retain a permanent object
     */
    TH2F* Get2DMap(int chip, int type, int scanpt){return GetMap(chip,type,scanpt);};
    /*! Returns a TH2F-map of TDAC or FDAC values as in the
     *  module config
     *  if chip>0 than the map for only this chip is returned    <br>
     *  BEWARE: the object returned is temporary and will be deleted
     *          by most calls to other PixDBData functions; use the
     *          resp. copy constructor to retain a permanent object
     */
    TH2F* getDACMap(int chip, const char *type);
    /*! Returns a graph containing hits vs scan variable
     *  for a 1D scan, or threshold, noise or chi^2 vs outer var.
     *  for a 2D scan    <br>
     *  BEWARE: the object returned is temporary and will be deleted
     *          by most calls to other PixDBData functions; use the
     *          resp. copy constructor to retain a permanent object
     */
    TGraphErrors* GetScanHi(int chip, int col, int row, int type, int scanpt=-1);
    /*! Returns a PixLib::Histo object of the requested type    <br>
     *  BEWARE: the object returned is temporary and will be deleted
     *          by most calls to other PixDBData functions; use the
     *          Histo copy constructor to retain a permanent Histo object
     */
    PixLib::Histo* getGenericPixLibHisto(int type, int scanpt=-1,
					 int scan_level=-1);

    /*! Returns a PixLib::Histo object of the requested type    <br>
     *  BEWARE: the object returned is temporary and will be deleted
     *          by most calls to other PixDBData functions; use the
     *          Histo copy constructor to retain a permanent Histo object
     */
    PixLib::Histo* getGenericPixLibHisto(PixLib::PixScan::HistogramType type, const int scanpt[4]);

    PixLib::Histo* getGenericPixLibHisto(int type, const int scanpt[4]);


    /*! returns true if a certain histo type was filled and
     *  kept during data taking
     */
    bool haveHistoType(int type);
    bool haveHistoType(PixLib::PixScan::HistogramType type);
    /*! returns the name of a PixLib histogram type
     */
    static const std::string &getHistoName(PixLib::PixScan::HistogramType type);

    /*! Returns a reference to the PixScan config    <br>
     *  BEWARE: the object returned is temporary and will be deleted
     *          by most calls to other PixDBData functions; use the
     *          Config copy constructor to retain a permanent object
     */
    PixLib::Config& getScanConfig();
    /*! Returns a reference to the PixModule config    <br>
     *  BEWARE: the object returned is temporary and will be deleted
     *          by most calls to other PixDBData functions; use the
     *          Config copy constructor to retain a permanent object
     */
    PixLib::Config& getModConfig();
    /*! Returns a reference to the PixBoc config    <br>
     *  BEWARE: the object returned is temporary and will be deleted
     *          by most calls to other PixDBData functions; use the
     *          Config copy constructor to retain a permanent object
     */
    PixLib::Config& getBocConfig();
    /*! either returns the results inquire or makes it if it is not there
     */

    const PixA::ConfigHandle modConfig() const {return m_scanConfig.modConfigHandle(); };
    PixA::ConfigHandle modConfig() {return m_scanConfig.modConfigHandle(); };

    const PixA::ConfigHandle bocConfig() const {return m_scanConfig.bocConfigHandle(); };
    PixA::ConfigHandle bocConfig() {return m_scanConfig.bocConfigHandle(); };

    const PixA::ConfigHandle scanConfig() const {return m_scanConfig.pixScanConfigHandle(); };
    PixA::ConfigHandle scanConfig() {return m_scanConfig.pixScanConfigHandle();};

    /*! either returns the results record or makes it if it is not there
     */
    PixLib::DbRecord* getResultsRecord();

    /*! either returns a DbRecord of specified name or makes it if it is not there
     */
    PixLib::DbRecord* getRecord(PixLib::DbRecord *parent, std::string dname, std::string name);
    /*! initialise fitting
     */
    void initFit(PixLib::PixScan::HistogramType type, int loop_level, float *pars, bool *pars_fixed, int fittype=1, 
		 PixLib::PixScan::HistogramType errType=PixLib::PixScan::MAX_HISTO_TYPES,
		 PixLib::PixScan::HistogramType chiType=PixLib::PixScan::MAX_HISTO_TYPES, bool doCalib=true);
    /*! clear fit remnants; bool argument: true -> clear also histos, otherwise only the rest
     */
    void clearFit(bool);
    /*! Fit specified histogram versus variable scanned in specified loop
     */
    void fitHisto(int fittype=1, int chip=-1, float chicut=20,float xmin=0, float xmax=0, 
		  float fracErr=0, bool useNdof=true);

    // save histo - not finalised yet
    void writePixLibHisto(PixLib::Histo*, PixLib::PixScan::HistogramType type, int scanpt, int scan_level);

    static int getPixScanID(int stdScanID);

    // temporary: retrieve one of the current fit parameter histos
    PixLib::Histo* getParHisto(int ipar);
    TH2F* getParHistoR(int ipar);


    /*! Count the number of histograms per level.
     * @param type one of the supported histogram types.
     * @param idx_out will be filled with the maximum number of histograms per level (0-2) or -1 if the level does not exist.
     */
    bool numberOfHistos(int type, HistoInfo_t &info ) {
      if (type >= PixLib::PixScan::MAX_HISTO_TYPES ) { 
	info.reset();
	//      for(unsigned int i=0; i<4; i++) {idx_out[i]=0;}
	return false;
      }
      return numberOfHistos(static_cast<PixLib::PixScan::HistogramType>(type),info);
    }

    /*! Count the number of histograms per level.
     * @param type one of the supported histogram types.
     * @param idx_out will be filled with the maximum number of histograms per level (0-3) or -1 if the level does not exist.
     */
    bool numberOfHistos(PixLib::PixScan::HistogramType type, HistoInfo_t &info ) ;


    /*! Count the number of histograms per level.
     * @param dbi the RootDb database record for the initial directory.
     * @param type one of the supported histogram types.
     * @param idx_out will be filled with the maximum number of histograms per level (0-3) or -1 if the level does not exist.
     */
    bool numberOfHistos(PixLib::DbRecord* dbi, 
			PixLib::PixScan::HistogramType type, HistoInfo_t &info ) ;

    /*! Returns the ToT spectrum e.g. for a source scan
     *  for a given pixel; if calibration is provided
     *  the ToT is calibrated into electrons
     * Always returns zero. 
     * @todo implement method properly?
     */
    TH1F* GetToTDist(int, int, int, bool, PixelDataContainer *) {return 0;}

  private:

    /*! Close all database files.
     * To avoid conflicts with other applications which want to access the same files like STControl,
     * all database files need to be closed before leaving the method.
     */

    // temporary variables
    TH2F *m_2dHisto;
    TH1F *m_1dHisto;
    TGraphErrors *m_graph;
    std::unique_ptr<PixLib::PixScan> m_ps;
    PixLib::PixModule *m_pm;
    PixLib::PixBoc *m_pb;
    // derived info about this module/scan
    int m_modID;

    // temporary storage for fitting routine
    std::vector<PixLib::Histo*> m_fitHistos, m_errHistos, m_parHistos;
    PixLib::Histo *m_oldChiHisto;
    std::vector<float> m_parInit, m_varValues;
    std::vector<bool> m_parFixed;
    FitClass *m_fitClass;
    // private helper functions

  private:

//     static template <class T> defaultLoopValue();

//     template <class T> T getLoopValueSafe(const std::string &name, int loop) {
//       T par = defaultLoopValue<T>();
//       ConfigRef scan_conf = scanConfig().ref();

//       std::stringstream full_name;
//       full_name << name << loopLevel;
//       try {
// 	par = confVal<T>(scan_conf["loops"][full_name.str()]);
//       }
//       catch (...) {
//       }

//       return par;
//     }

    PixLib::Histo* readHisto(PixLib::PixScan::HistogramType type, const int id[4] );

    void makeHistoName(PixLib::PixScan::HistogramType type, std::vector<std::string> &histo_name);

    PixA::ScanConfigRef m_scanConfig;
    PixA::HistoHandle   m_histo;

    enum EModuleNameingConvention {kUnknownModuleNaming,kNewModuleNaming, kOldModuleNaming } m_moduleNaming;
    static void initHistoTypeNameList();
    static std::map< PixLib::PixScan::HistogramType , std::string> s_histoTypeToName;
    static std::string                s_unknownTypeName;
  };

}
#endif // PIXDBDATA_H
