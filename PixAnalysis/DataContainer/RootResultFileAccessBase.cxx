#include "RootResultFileAccessBase.h"
#include "ConfigWrapper/Regex_t.h"

#include <TFile.h>
#include <TKey.h>
#include <TH1.h>

#include <fstream>
#include <sstream>
#include <iomanip>
#include <cassert>
#include <climits>

namespace PixA {

  const char *RootResultFileAccessBase::s_pixScanStoragePathEnv = "PIXSCAN_STORAGE_PATH";
  const char RootResultFileAccessBase::s_serialNumberTypeId[kNSerialNumberTypes]={'S','A'};

  std::vector< Regex_t* > RootResultFileAccessBase::s_modulePattern;
  std::vector< Regex_t* > RootResultFileAccessBase::s_rodPattern;

  void RootResultFileAccessBase::init()
  {
    if (s_modulePattern.empty()) {
      s_modulePattern.reserve(7);
      s_modulePattern.push_back(new Regex_t("L[012]_B[0-9][0-9]_S[12]_[AC][67]_M[1-6][AC]"));
      s_modulePattern.push_back(new Regex_t("L[012]_B[0-9][0-9]_S[12]_[AC][7]_M0"));
      s_modulePattern.push_back(new Regex_t("LX_B[0-9][0-9]_S[12]_[AC][67]_M[1-6][AC]"));
      s_modulePattern.push_back(new Regex_t("LX_B[0-9][0-9]_S[12]_[AC][7]_M0"));
      s_modulePattern.push_back(new Regex_t("D[123][AC]_B[0-9][0-9]_S[12]_M[1-6]"));
      s_modulePattern.push_back(new Regex_t("LI_S[0-9][0-9]_[AC]_M[1-4]_[AC][1-8]"));
      s_modulePattern.push_back(new Regex_t("LI_S[0-9][0-9]_[AC]_[13][24]_M[1-4]_[AC]([1-9]|1[0-2])"));
    }
    if (s_rodPattern.empty()) {
      s_rodPattern.reserve(1);
      s_rodPattern.push_back(new Regex_t("ROD_[IBCDL][0-4]_S([5-9]|1[0-9]|2[0-1])"));
    }
  }

  class ContainsHistograms : public IHistogramDirectoryVerifier 
  {
  public:
    ContainsHistograms(bool verbose) : m_verbose(verbose) {}
    
    bool containsHistograms(EConnItemType type, const std::string &conn_name, TDirectory *histo_directory) {
      return RootResultFileAccessBase::containsHistograms( histo_directory, m_verbose);
    }
  private:
    bool m_verbose;
  };


  std::string RootResultFileAccessBase::fileName(const std::string &file_name, const std::string &alternate_file_name)
  {

    // try first the file name given in the meta data, then the alternate file name. Finally the
    // the path defined by the environment variable PIXSCAN_STORAGE_PATH + the base file name given
    // by the meta data.
    for (unsigned int attempt_i=0; attempt_i<3; ++attempt_i) {
      std::string a_file_name;
      switch (attempt_i) {
      case 0:
	a_file_name += file_name;
	break;
      case 1:
	a_file_name += alternate_file_name;
	break;
      case 2: {
	char *scan_data_path =getenv(s_pixScanStoragePathEnv);
	if (scan_data_path && strlen(scan_data_path)>0) {
	  a_file_name = scan_data_path;

	  if (a_file_name.empty()) break; // paranoia check

	  if (a_file_name[file_name.size()-1]!='/') {
	    if (a_file_name.size()>5 && a_file_name.compare(a_file_name.size()-5,5,".root")==0) {
	      std::string::size_type pos = a_file_name.rfind("/");
	      if (pos!=std::string::npos) {
		a_file_name.erase(pos+1,file_name.size()-pos-1);
	      }
	    }
	    else {
	      a_file_name+='/';
	    }
	  }
	}
	const std::string &the_file_name = file_name;
	std::string::size_type pos = the_file_name.rfind("/");
	if (pos==std::string::npos) {
	  pos=0;
	}
	else {
	  pos++;
	}
	std::string::size_type end_pos = the_file_name.find(".root::",pos);
	if (end_pos==std::string::npos) {
	  end_pos=the_file_name.size();
	}
	else {
	  end_pos+=5;
	}

	a_file_name += the_file_name.substr(pos,end_pos-pos);
	break;
      }
      }

      std::string::size_type end_pos = a_file_name.find(".root::");
      if (end_pos!=std::string::npos) {
	a_file_name.erase(end_pos+5, a_file_name.size()-end_pos-5);
      }
      if (a_file_name.empty()) continue;

      std::ifstream test_in(a_file_name.c_str());
      if (test_in) {
	return a_file_name;
      }
    }
    return "";
  }

  std::pair<TFile *, TDirectory *> RootResultFileAccessBase::open(const std::string &file_name,
								  ESerialNumberType serial_number_type, 
								  SerialNumber_t serial_number)
  {
    assert( serial_number_type < kNSerialNumberTypes);

    std::unique_ptr<TFile> a_file(new TFile(file_name.c_str()));
    std::stringstream folder_name;
    folder_name << s_serialNumberTypeId[serial_number_type] << std::setw(9) << std::setfill('0') << serial_number;

    if (!a_file.get()) {
      std::string message("FATAL [CAN::RootResultFileAccessBase::open] Failed to open file ");
      message += file_name ;
      message += " does not contain directory for scan ";
      message += folder_name.str();
      message += ".";
      throw NoResultFile(message);
    }

    try {
      TDirectory *a_directory = enter(a_file.get(), folder_name.str());
      std::pair<TFile *, TDirectory *> ret;
      ret.first = a_file.release();
      ret.second = a_directory;
      return ret;
    }
    catch (MissingResultDirectory &err) {
      std::string message("FATAL [RootResultFileAccessBase::open] File name ");
      message += file_name ;
      message += " does not contain directory for scan ";
      message += folder_name.str();
      message += ".";
      throw NoResultFile(message);
    }

  }

  TDirectory *RootResultFileAccessBase::enter(TDirectory *a_directory, const std::string &dir_name)
  {

    TObject *obj = NULL;

    if (a_directory) {
      obj = a_directory->Get(dir_name.c_str() );
    }

    if (!obj || !obj->InheritsFrom(TDirectory::Class())) {
      std::string message("FATAL [CAN::RootResultFileAccessBase::enter] File name ");
      message += a_directory->GetFile()->GetName();
      message += " does not contain directory ";
      message += dir_name;
      message += ".";
      throw MissingResultDirectory(message);
    }

    TDirectory *sub_directory = static_cast<TDirectory *>(obj);
    sub_directory->cd();
    return sub_directory;

  }

  TDirectory *RootResultFileAccessBase::enter(TDirectory *scan_directory,
					      const std::string &rod_name,
					      const std::string &module_name,
					      const std::string &histo_name)
  {
    if (!scan_directory || histo_name.empty()) return NULL;
    TDirectory *sub_dir = scan_directory;
    if (!rod_name.empty()) {
      sub_dir = enter(sub_dir, rod_name);
      if (!sub_dir) return NULL;
    }

    if (!module_name.empty()) {
      sub_dir = enter(sub_dir, module_name);
      if (!sub_dir) return NULL;
    }

    sub_dir = enter(sub_dir, histo_name);
    return sub_dir;
  }

  class DirStackElement_t {
  public:

    DirStackElement_t(TList *list)
      : m_listIter(list),
	m_type(IHistogramDirectoryVerifier::kNConnItemTypes)
    { assert(list); }

    DirStackElement_t(const DirStackElement_t &parent, TList *list) 
      : m_listIter(list),
	m_fullHistogramName(parent.m_fullHistogramName),
	m_type(parent.m_type),
	m_connName(parent.m_connName)
    { assert(list); }

    const std::string &name() const { return m_fullHistogramName; }

    void extendName(const std::string &dir_name) {
      if (!m_fullHistogramName.empty()) {
	m_fullHistogramName += '/';
      }
      m_fullHistogramName += dir_name;
    }

    void setConnName(IHistogramDirectoryVerifier::EConnItemType conn_type, const std::string &conn_name) {
      m_type = conn_type;
      m_connName = conn_name;
    }

    IHistogramDirectoryVerifier::EConnItemType connItemType() const { return m_type;}

    const std::string &connName() const { return m_connName;}

    TObject *Next() { return m_listIter.Next(); }

  private:
    TListIter   m_listIter;
    std::string m_fullHistogramName;
    IHistogramDirectoryVerifier::EConnItemType m_type;
    std::string m_connName;
  };


  void RootResultFileAccessBase::histogramList(TDirectory *scan_directory,
					       std::set<std::string> &rod_histogram_list_out,
					       std::set<std::string> &module_histogram_list_out,
					       bool verbose)
  {
    ContainsHistograms contains_histogram(verbose);
    histogramList(scan_directory, rod_histogram_list_out, module_histogram_list_out, &contains_histogram, verbose);
  }


  void RootResultFileAccessBase::histogramList(TDirectory *scan_directory,
					       std::set<std::string> &rod_histogram_list_out,
					       std::set<std::string> &module_histogram_list_out,
					       IHistogramDirectoryVerifier *verifier,
					       bool verbose)
  {
    assert( verifier );
    RootResultFileAccessBase::init();

    std::vector< DirStackElement_t > directory_stack;
    directory_stack.reserve(5);

    directory_stack.push_back( DirStackElement_t( scan_directory->GetListOfKeys()  ) );
    const std::vector< Regex_t *> *pattern_lists[2] = {&s_rodPattern, &s_modulePattern};
    std::vector< std::set< std::string > *> a_histogram_list;
    a_histogram_list.push_back(&rod_histogram_list_out);
    a_histogram_list.push_back(&module_histogram_list_out);

    while (!directory_stack.empty()) {
      TObject *key_obj = 0;
      bool could_be_lowest_level=false;
      TDirectory *mother_dir=NULL;
      while ( (key_obj = directory_stack.back().Next())) {
	assert( key_obj->InheritsFrom(TKey::Class()));
	TKey *a_key = static_cast<TKey *>(key_obj);

	if (verbose) {
	  std::cout << "INFO [RootResultFileAccessBase::addFile] name = " << a_key->GetName() << " class = " << a_key->GetClassName() << std::endl;
	}

	if (inheritsFrom( a_key->GetClassName(),TDirectory::Class(), verbose)) {
	  could_be_lowest_level=false;
	  std::string dir_name = a_key->GetName();
	  bool is_pix_scan_histo;
	  if (   dir_name.size()>1
	      && dir_name.size()<7 /* just allow a reasonable number of levels, to not confuse a pix scan histo with e.g. an analysis or scan.*/
	      && ((dir_name[0]>='A' && dir_name[0]<='Z') || isdigit(dir_name[0]) ) ) {

	    is_pix_scan_histo=true;
	    if (directory_stack.back().connItemType()>=IHistogramDirectoryVerifier::kNConnItemTypes) {
	      if (verbose) {
		std::cout << "WARNING [RootResultFileAccessBase::requestHistogramList] PixScanHist "
			  << directory_stack.back().name() << " not assigned to a connectivity object." <<std::endl;
	      }
	    }
	    else {

	      for (unsigned int pos=1; pos< dir_name.size(); ++pos) {
		if (!isdigit(dir_name[pos])) {
		  is_pix_scan_histo=false;
		  break;
		}
	      }

	      // this is a pix scan histo directory 
	      // so there is no need to investigate this directory any further
	      if (is_pix_scan_histo) {
		if (directory_stack.back().name().empty() ) {
		  std::cout << "WARNING [RootResultFileAccessBase::requestHistogramList] histogram without name." <<std::endl;
		  directory_stack.back().extendName("");
		}

		std::unique_ptr<TObject> dir_obj( a_key->ReadObj() );
		if (dir_obj.get()) {
		  assert( dir_obj->InheritsFrom(TDirectory::Class()));
		  TDirectory *a_dir = static_cast<TDirectory *>(dir_obj.release());
		  if (verifier->containsHistograms(directory_stack.back().connItemType(), directory_stack.back().connName(), a_dir)) {
		    assert( static_cast<unsigned int>(directory_stack.back().connItemType()) < a_histogram_list.size() );

		    // @todo set histogram status

		    if (verbose) {
		      std::cout << "INFO [RootResultFileAccessBase::requestHistogramList] add histogram " << directory_stack.back().name() 
				<< " to item " << directory_stack.back().connItemType() << "."<< std::endl;
		    }
		    a_histogram_list[directory_stack.back().connItemType()]->insert( directory_stack.back().name() );
		  }
		}
		break;
	      }
	    }

	  }
	  else {
	    
	    is_pix_scan_histo=false;
	  }

	  bool is_conn_name=false;
	  if (directory_stack.size() <= 2) {
	    // chech whether the directory name is a connectivity name
	    assert(directory_stack.size()>0); // paranoia check
	    const std::vector< Regex_t* > *a_pattern_list = pattern_lists[directory_stack.size()-1];
	    for (std::vector< Regex_t *>::const_iterator pattern_iter = a_pattern_list->begin();
		 pattern_iter != a_pattern_list->end();
		 ++pattern_iter) {
	      if ((*pattern_iter)->match(dir_name)) {
		is_conn_name = true;
		break;
	      }
	    }
	  }

	  if (verbose) {
	    std::cout << "INFO [RootResultFileAccessBase::addFile] name = " << a_key->GetName() << " class = " << a_key->GetClassName() 
		      << (is_pix_scan_histo ? " is pix scan histo " : "" ) << (is_conn_name ? " is conn name " : "" ) << std::endl;
	  }

	  if (!is_pix_scan_histo) {
	    // the pix scan histo levels are ignored.
	    std::unique_ptr<TObject> dir_obj( a_key->ReadObj() );
	    if (dir_obj.get()) {
	      assert( dir_obj->InheritsFrom(TDirectory::Class()));
	      TDirectory *a_dir = static_cast<TDirectory *>(dir_obj.release() );
	      directory_stack.push_back( DirStackElement_t( directory_stack.back(), a_dir->GetListOfKeys() ));
	      if (!is_conn_name) {
		directory_stack.back().extendName(dir_name);
	      }
	      else {
		directory_stack.back().setConnName( (directory_stack.size()==3 ?
						       IHistogramDirectoryVerifier::kModuleItem
						     : IHistogramDirectoryVerifier::kRodItem ),
						    dir_name);
	      }

	      if (verbose) {
		std::cout << "INFO [RootResultFileAccessBase::addFile] name = " << a_key->GetName() << " class = " << a_key->GetClassName() 
			  << (is_pix_scan_histo ? " is pix scan histo " : "" ) << (is_conn_name ? " is conn name " : "" )
			  << " conn_type = " << directory_stack.back().connItemType() << " stack size = " << directory_stack.size()
			  << " current dir name " << directory_stack.back().name()
			  << std::endl;
	      }
	    }
	  }
	}
	else if (inheritsFrom( a_key->GetClassName(),TH1::Class(), verbose)) {
	  std::string dir_name = a_key->GetName();
	  std::string::size_type dir_pos = dir_name.rfind('/');
	  std::string::size_type start_pos=dir_name.size();
	  if (dir_pos != std::string::npos && dir_pos+1 < dir_name.size()) {
	    start_pos=dir_pos+1;
	  }
	  unsigned int pos;
	  for (pos=start_pos+1; pos< dir_name.size() && isdigit(dir_name[pos]); ++pos);
	  if (pos < dir_name.size()) {
	    if (dir_name[pos]==':') {
	      could_be_lowest_level=true;
	      mother_dir = a_key->GetMotherDir();
	      if (verbose) {
		std::cout << "INFO [RootResultFileAccessBase::addFile] could be histogram of lowest level " << a_key->GetName() 
			  << " class = " << a_key->GetClassName() 
			  << std::endl;
	      }
	    }
	  }
	}
      }
      if (could_be_lowest_level && directory_stack.back().connItemType()<IHistogramDirectoryVerifier::kNConnItemTypes) {
	if (verbose) {
	  std::cout << "INFO [RootResultFileAccessBase::addFile] shallow histogram directory hierarchy "
		    << directory_stack.back().name()
		    << std::endl;
	}

	a_histogram_list[directory_stack.back().connItemType()]->insert( directory_stack.back().name() );
	if (mother_dir) {
	  verifier->containsHistograms(directory_stack.back().connItemType(), directory_stack.back().connName(), mother_dir);
	}
      }
      directory_stack.pop_back();
    }
  }

  void RootResultFileAccessBase::numberOfHistograms(TDirectory *a_directory, HistoInfo_t &histo_info, bool verbose)
  {
    histo_info.reset();
    if (!a_directory) return;

    std::vector< std::pair<unsigned int,unsigned int> > index_extrema;
    index_extrema.reserve(4);
    std::vector< TListIter > directory_stack;
    directory_stack.push_back( TListIter(a_directory->GetListOfKeys()) );
    bool first_histo=true;

    while (!directory_stack.empty()) {
      TObject *key_obj;
      while ( (key_obj = directory_stack.back().Next())) {
	assert( key_obj->InheritsFrom(TKey::Class()));
	TKey *a_key = static_cast<TKey *>(key_obj);
	std::string dir_name ( a_key->GetName() );
	if (verbose) {
	  std::cout << "INFO [RootResultFileAccessBase::numberOfHistograms] name = " << dir_name << " class = " << a_key->GetClassName() << std::endl;
	}
	bool is_histogram=false;
	std::string::size_type start_pos=0;

	if ( !inheritsFrom(a_key->GetClassName(),TDirectory::Class(), verbose)) {
	  if (!inheritsFrom(a_key->GetClassName(), TH1::Class(), verbose)) {
	    std::cout << "WARNING [RootResultFileAccessBase::numberOfHistograms] Only expected directories and histograms in the PixScanHisto directory " 
		      << a_directory->GetName()
		      << "." << std::endl;
	    continue;
	  }
	  else {
	    std::string::size_type dir_pos = dir_name.rfind('/');
	    if (dir_pos != std::string::npos && dir_pos+1 < dir_name.size()) {
	      start_pos=dir_pos+1;
	    }
	    is_histogram=true;
	  }
	}

	bool is_pix_scan_histo;

	if (
	       (   (!is_histogram && dir_name.size()>1 && dir_name[0]>='A' && dir_name[0]<='Z' && dir_name.size()<7 && start_pos==0
		 /* : just allow a reasonable number of levels, to not confuse a pix scan histo with e.g. an analysis or scan.*/)
		|| ( is_histogram && dir_name.size()>start_pos+1 && isdigit(dir_name[start_pos])) ) ) {

	  is_pix_scan_histo=true;
	  for (unsigned int pos=start_pos+1; pos< dir_name.size(); ++pos) {
	    if (!isdigit(dir_name[pos])) {
	      if (!is_histogram || dir_name[pos]!=':') {
		is_pix_scan_histo=false;
	      }
	      break;
	    }
	  }
	  if (verbose) {
	    std::cout << "INFO [RootResultFileAccessBase::numberOfHistograms] name = " << dir_name << " class = " << a_key->GetClassName()
		      << (is_pix_scan_histo ? " is pix scan histo" : "")<< std::endl;
	  }

	  if (is_pix_scan_histo) {
	    assert(directory_stack.size()>0); // paranoia_check
	    if (   (dir_name[start_pos]==(char)('A'+directory_stack.size()-1))
		   || ( (directory_stack.size()==index_extrema.size() || ( first_histo  && directory_stack.size()==index_extrema.size()+1))  && isdigit(dir_name[start_pos]))) {
	      if ( dir_name[start_pos]==(char)('A'+directory_stack.size()-1 )) {
		start_pos++;
	      }
	      else {
		first_histo=false;
	      }

	      unsigned int index=atoi(&((dir_name.c_str())[start_pos]));
	      if (directory_stack.size()>index_extrema.size()) {
		index_extrema.resize(directory_stack.size(), std::make_pair(UINT_MAX,0));
	      }
	      if (index+1 > index_extrema[directory_stack.size()-1].second) {
		index_extrema[directory_stack.size()-1].second=index+1;
	      }
	      if (index < index_extrema[directory_stack.size()-1].first) {
		index_extrema[directory_stack.size()-1].first=index;
	      }

	      if (verbose) {
		std::cout << "INFO [RootResultFileAccessBase::numberOfHistograms] name = " << &(dir_name.c_str()[start_pos]) << " class = " << a_key->GetClassName()
			  << " -> " << directory_stack.size()-1 << " : " << index_extrema[directory_stack.size()-1].first
			  << " - " << index_extrema[directory_stack.size()-1].second << std::endl;
	      }

	    }
	    else {
	      std::cout << "WARNING [RootResultFileAccessBase::numberOfHistograms] invalid subdirectory name in " << a_directory->GetName() 
			<< "." << std::endl;
	    }
	  }
	}
	else {
	  is_pix_scan_histo=false;
	}

	if (!is_pix_scan_histo) {
	  std::cout << "WARNING [RootResultFileAccessBase::numberOfHistograms] directory " << a_directory->GetName() 
		    << " contains other than pix scan histos." << std::endl;
	  continue;
	}

	if (!is_histogram) {
	  std::unique_ptr<TObject> dir_obj( a_key->ReadObj() );
	  if (dir_obj.get()) {
	    assert( dir_obj->InheritsFrom(TDirectory::Class()));
	    TDirectory *a_dir = static_cast<TDirectory *>(dir_obj.release());
	    directory_stack.push_back( TListIter(a_dir->GetListOfKeys()) );
	  }
	}
      }
      directory_stack.pop_back();
    }

    for (std::vector<std::pair<unsigned int, unsigned int> >::const_iterator index_iter = index_extrema.begin();
	 index_iter != index_extrema.end();
	 ++index_iter) {
      if (index_extrema.end() - index_iter > 1) {
	if (index_iter->first != 0) {
	  std::cout << "WARNING [RootResultFileAccessBase::numberOfHistograms] Lowest index of the PixScanHisto " 
		    << a_directory->GetName()
		    << " does not start at zero : " << index_iter->first << " - " << index_iter->second
		    << "." << std::endl;
	}

	if (verbose) {
	  std::cout << "INFO [RootResultFileAccessBase::numberOfHistograms] set index "
		    << index_iter-index_extrema.begin() << " to " << index_iter->second
		    << "." << std::endl;
	}
	histo_info.setMaxIndex(index_iter-index_extrema.begin(), index_iter->second);
      }
      else {

	if (index_iter->first >index_iter->second ) {
	  std::cout << "WARNING [RootResultFileAccessBase::numberOfHistograms] minimum histo index is larger than the maximum histo index for the PixScanHisto " 
		    << a_directory->GetName()
		    << " : " << index_iter->first << " - " << index_iter->second
		    << "." << std::endl;
	}

	histo_info.setMaxHistoIndex(index_iter->second);
	histo_info.setMinHistoIndex(index_iter->first);
      }

      if (verbose) {
	std::cout << "INFO [RootResultFileAccessBase::numberOfHistograms] ";
	std::cout << index_iter->first << " - " << index_iter->second << " " << std::endl;
      }
    }

  }

  bool RootResultFileAccessBase::containsHistograms(TDirectory *a_directory, bool verbose)
  {
    if (!a_directory) return false;

    std::vector< TListIter > directory_stack;
    directory_stack.push_back( TListIter(a_directory->GetListOfKeys()) );

    while (!directory_stack.empty()) {
      TObject *key_obj = 0;
      while ( (key_obj = directory_stack.back().Next())) {
	assert( key_obj->InheritsFrom(TKey::Class()));
	TKey *a_key = static_cast<TKey *>(key_obj);

	if (inheritsFrom(a_key->GetClassName(),TDirectory::Class(), verbose)) {

	  std::unique_ptr<TObject> dir_obj( a_key->ReadObj() );
	  if (dir_obj.get()) {
	    assert( dir_obj->InheritsFrom(TDirectory::Class()));
	    TDirectory *a_dir = static_cast<TDirectory *>(dir_obj.release());
	    directory_stack.push_back( TListIter(a_dir->GetListOfKeys()) );
	  }
	}
	else {
	  if ( verbose) {
	    std::cout << "INFO  [RootResultFileAccessBase::containsHistograms] name = " << a_key->GetName() << " class = " << a_key->GetClassName() << std::endl;
	  }
	  if ( inheritsFrom(a_key->GetClassName(),TH1::Class(), verbose)
		 /* allow for other stuff ? || inheritsFrom(inheritsFrom(a_key->GetClassName(), TGraph::Class(), verbose*/) {
	    return true;
	  }
	  else if (verbose) {
	    std::cout << "WARNING  [RootResultFileAccessBase::containsHistograms] Unknown class " << a_key->GetClassName() 
		      << " in " << a_directory->GetName();
	    if (a_directory->GetFile()) {
	      std::cout << " of " << a_directory->GetFile()->GetName();
	    }
	    std::cout << "." << std::endl;
	  }

	}
      }
      directory_stack.pop_back();
    }
    return false;
  }

  TH1 *RootResultFileAccessBase::histogram(TDirectory *histo_dir,
					   const Index_t &index,
					   bool verbose)
  {
    TDirectory *a_dir=histo_dir;
    char dir_letter='A';
    for (unsigned int index_i=0; index_i+1<index.size(); index_i++) {
      std::stringstream folder_name;
      folder_name << dir_letter++ << index[index_i];
      if (verbose) {
	std::cout << "INFO [RootResultFileAccessBase::requestHistogramList] enter " << folder_name.str()
		  << " of " <<a_dir->GetName()
		  << "." << std::endl;
      }
      a_dir = enter(a_dir, folder_name.str().c_str());
      if (!a_dir) {
	return NULL;
      }
    }

    TListIter list_iter( a_dir->GetListOfKeys() );

    TObject *key_obj;
    while ( (key_obj = list_iter.Next())) {
      assert( key_obj->InheritsFrom(TKey::Class()));
      TKey *a_key = static_cast<TKey *>(key_obj);

      if (inheritsFrom( a_key->GetClassName(),TH1::Class(), verbose )) {

	std::string histo_name(a_key->GetName());
	std::string::size_type start_pos=0;
	{
	  std::string::size_type dir_pos = histo_name.rfind('/');
	  if (dir_pos != std::string::npos && dir_pos+1 < histo_name.size()) {
	    start_pos=dir_pos+1;
	  }
	}

	std::string::size_type pos = histo_name.find(":", start_pos);
	if (pos == std::string::npos) {
	  pos=histo_name.size();
	}
	const char *ptr=&(histo_name.c_str()[start_pos]);
	if (static_cast<unsigned int>(atoi(ptr))==index.back()) {
	  const char *end_ptr=&(histo_name.c_str()[pos]);
	  while (ptr != end_ptr && isdigit(*ptr)) ptr++;
	  if (ptr==end_ptr) {
	    std::unique_ptr<TObject> obj( a_key->ReadObj() );
	    if (obj.get() && obj->InheritsFrom(TH1::Class())) {
	      static_cast<TH1 &>( *obj ).SetDirectory(0);
	      return static_cast<TH1 *>( obj.release());
	    }
	  }
	}
      }
    }

    if (verbose) {
      std::cout << "WARNING [RootResultFileAccessBase::requestHistogramList] PixScanHist "
		<< " No histogram ";
      for (unsigned int index_i=0; index_i<index.size(); index_i++) {
	std::cout << index_i;
	if (index_i+1<index.size()) 
	  std::cout << ", ";
      }
      std::cout << " named " << histo_dir->GetName() <<  "." << std::endl;
    }
    return NULL;
  }


}
