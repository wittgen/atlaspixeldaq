#ifndef _PixelUtil_h_
#define _PixelUtil_h_

namespace PixA {

  class PixelTypeMask_t {
  public:
    enum EPixelType { kNoPixel=0, kNormalPixel=1, kLongPixel=2, kGangedPixel=4,  kLongGangedPixel=8, kInterGangedPixel=16, kLongInterGangedPixel=32 };

    PixelTypeMask_t() : m_mask(0) {}

    PixelTypeMask_t(unsigned short a_mask ) : m_mask(a_mask) {}

    PixelTypeMask_t &setUseNormal()          { m_mask |= kNormalPixel; return *this; }
    PixelTypeMask_t &setUseGanged()          { m_mask |= kGangedPixel; return *this; }
    PixelTypeMask_t &setUseLong()            { m_mask |= kLongPixel; return *this; }
    PixelTypeMask_t &setUseInterGanged()     { m_mask |= kInterGangedPixel; return *this; }
    PixelTypeMask_t &setUseLongGanged()      { m_mask |= kLongGangedPixel; return *this; }
    PixelTypeMask_t &setUseLongInterGanged() { m_mask |= kLongInterGangedPixel; return *this; }

    bool useNormal() const             { return m_mask | kNormalPixel;}
    bool useLong() const               { return m_mask | kLongPixel;}
    bool useGanged() const             { return m_mask | kGangedPixel;}
    bool useInterGanged() const        { return m_mask | kInterGangedPixel;}
    bool useLongInterGanged() const    { return m_mask | kLongInterGangedPixel;}

    static unsigned short all() {return kNormalPixel | kLongPixel | kGangedPixel  |  kLongGangedPixel | kInterGangedPixel  | kLongInterGangedPixel; }

    unsigned short mask() const {return m_mask;}
    operator unsigned short() const {return m_mask;}

    /** Returns the pixel type.
     * copied from PixelDataContainer
     *
     * @authors Joern Grosse-Knetter <joern.grosse-knetter@uni-bonn.de>
     * 2007-12-04, bugfix so that type() also returns kLongPixel, Sara Strandberg
     */
    static unsigned short type(int col, int row, bool isFEI4) {
      if(isFEI4) return type_FEI4(col, row);
      else       return type_FEI3(col, row);
    }

    static unsigned short type_FEI3(int col, int row) {
      if (( col > 0) && ( col < 17)){
	if (row>=152) {
	  if (row>=152 && (row&1) ==0) {
	    //   if (row == 152 || row == 154 || row == 156 || row == 158)
	    return kInterGangedPixel; // inter-ganged pixel
	  }
	  else {
	    // else if (row == 153 || row == 155 || row == 157 || row == 159)
	    return kGangedPixel; // ganged pixel
	  }
	}
	else {
	  return kNormalPixel; // normal pixel
	}

      } 
      else {

	if (row>=152) {
	  if (row>=152 && (row&1) ==0) {
	    //if (row == 152 || row == 154 || row == 156 || row == 158)
	    return kLongInterGangedPixel; // long inter-ganged pixel
	  }
	  else {
	  // if (row == 153 || row == 155 || row == 157 || row == 159)
	    return kLongGangedPixel; // long ganged pixel
	  }
	}
	else {
	  return kLongPixel; // long
	}
      }
    }

    static unsigned short type_FEI4(int col, int row) {
      if (( col > 0) && ( col < 79)){
	  return kNormalPixel; // normal pixel
      } 
      else {
	return kLongPixel; // long
      }
    }
    
  private:
    unsigned short m_mask;
  };

  /** Translate between chip and module coordinates.
   * copied from PixelDataContainer
   *
   * @authors Joern Grosse-Knetter <joern.grosse-knetter@uni-bonn.de>
   */
  class PixelCoord_t
  {
  public:
    static const int s_nChip = 16;
    static const int s_nPix  = 2880;
    static const int s_nSpecialPix  = 448;
    static const int s_nCol  = 18;
    static const int s_nRow  = 160;

    PixelCoord_t(int xval, int yval) {
      if(yval<s_nRow){ // chips 0-7
	m_row  = yval;
	m_col  = xval%s_nCol;
	m_chip = xval/s_nCol;
      } else{
	m_row  = 2*s_nRow-1- yval;
	m_col  = s_nCol-1  - xval%s_nCol;
	m_chip = s_nChip-1 - xval/s_nCol;
      }
    }

    PixelCoord_t(int chip, int col, int row) : m_chip(chip), m_col(col), m_row(row) {}

    int chip() const {return m_chip;}
    int col()  const {return m_col;}
    int row()  const {return m_row;}

    int pixel() const { return chip()*s_nPix+row()+s_nRow*col(); }

    int x() const {

      if(m_chip<s_nChip/2) {
	return m_chip*s_nCol+m_col;
      }
      else {
	return (s_nCol-1-m_col + s_nCol*(s_nChip-1-m_chip));
      }

    }

    int y() const {
      if (m_chip<s_nChip/2) {
	return m_row;
      }
      else {
	return 2*s_nRow-1-m_row;
      }
    }

  private:
    int m_chip;
    int m_col;
    int m_row;
  };

}
#endif
