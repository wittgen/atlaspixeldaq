#include "RootResultFileAccessBase.h"
#include "HistoInfo_t.h"
#include "PixelUtil.h"

#include <TFile.h>
#include <TKey.h>
#include <TDirectory.h>
#include <TList.h>

#include <vector>
#include <map>
#include <set>
#include <iostream>
#include <string>
#include <memory>
#include <cmath>
#include <iomanip>
#include <sstream>

#include <memory>

#include <TH1.h>
#include <TH2.h>

class IDirectoryTreeListener
{
public:
  virtual ~IDirectoryTreeListener() {}

  virtual bool shallEnter(TKey *child_directory_key, const std::vector<std::string> &path) = 0 ;
  
};

class DirectoryTreeReader
{
public:
  DirectoryTreeReader(IDirectoryTreeListener &listener, bool verbose=false) : m_listener(&listener), m_verbose(verbose) {}
  void setListener(IDirectoryTreeListener &listener) { m_listener=&listener; }
  void read(TDirectory *directory);

private:
  IDirectoryTreeListener *m_listener;
  bool m_verbose;
};


class DirStackElement_t {
public:
  
  DirStackElement_t(TList *list)
    : m_listIter(list)
  { assert(list); }

  DirStackElement_t(const DirStackElement_t &parent, TList *list) 
    : m_listIter(list),
      m_path(parent.m_path)
  { assert(list); }

  std::string name() const { if (m_path.empty()) return ""; else return m_path.back(); }
  const std::vector<std::string> &path() const { return m_path; }

  void extendName(const std::string &dir_name) {
    m_path.push_back(dir_name);
  }
  
  TObject *Next() { return m_listIter.Next(); }

private:
  TListIter   m_listIter;
  std::vector<std::string> m_path;
};


void DirectoryTreeReader::read(TDirectory *parent_dir)
{
  if (!m_listener) return;
    std::vector< DirStackElement_t > directory_stack;
    directory_stack.reserve(5);

    directory_stack.push_back( DirStackElement_t( parent_dir->GetListOfKeys()  ) );

    while (!directory_stack.empty()) {
      TObject *key_obj;
      while ( (key_obj = directory_stack.back().Next())) {
	assert( key_obj->InheritsFrom(TKey::Class()));
	TKey *a_key = static_cast<TKey *>(key_obj);

	if (m_verbose) {
	  std::cout << "INFO [RootResultFileAccessBase::addFile] name = " << a_key->GetName() << " class = " << a_key->GetClassName() << std::endl;
	}

	if (PixA::RootResultFileAccessBase::inheritsFrom( a_key->GetClassName(),TDirectory::Class(), m_verbose)) {
	  if (m_listener->shallEnter(a_key, directory_stack.back().path())) {

	    // the pix scan histo levels are ignored.
	    std::unique_ptr<TObject> dir_obj( a_key->ReadObj() );
	    if (dir_obj.get()) {
	      assert( dir_obj->InheritsFrom(TDirectory::Class()));
	      TDirectory *a_dir = static_cast<TDirectory *>(dir_obj.release() );
	      directory_stack.push_back( DirStackElement_t( directory_stack.back(), a_dir->GetListOfKeys() ));

	      directory_stack.back().extendName(a_key->GetName());

	      if (m_verbose) {
		std::cout << "INFO [RootResultFileAccessBase::addFile] name = " << a_key->GetName() << " class = " << a_key->GetClassName() 
			  << " stack size = " << directory_stack.size()
			  << " current dir name " << directory_stack.back().name()
			  << std::endl;
	      }
	    }
	  }
	}
      }
      directory_stack.pop_back();
    }
  }

class BatchAnalyser : public IDirectoryTreeListener
{
public:
  bool shallEnter(TKey *child_directory_key, const std::vector<std::string> &path) ;
  virtual void analyse(unsigned int serial_number, const std::string &rod_name, const std::string &module_name, TDirectory *conn_dir) = 0;

protected:

  void storeHisto(const std::string &rod_name, const std::string &conn_name, const std::string &histogram_name, unsigned int serial_number, TDirectory *current_file) {
    if (current_file && !rod_name.empty() && !histogram_name.empty() && serial_number>0) {
      m_histoList[rod_name][conn_name][histogram_name][serial_number]=current_file;
    }
  }

//   const std::map<unsigned int, std::shared_ptr<TH1> > histo(const std::string &conn_name, const std::string &histogram_name) const {
//     std::map< std::string, std::map< std::string, std::map<unsigned int , std::shared_ptr<TH1> > > >::const_iterator conn_iter = m_histoList.find(conn_name);
//     if (conn_iter == m_histoList.end()) return s_emptyHistoList;
//     std::map< std::string, std::map<unsigned int , std::shared_ptr<TH1> > >::const_iterator histo_iter = conn_iter->second.find(conn_name);
//     if (histo_iter == conn_iter->second.end()) return s_emptyHistoList;
//     return histo_iter->second;
//   }

  const std::map< std::string, std::map< std::string, std::map< std::string, std::map<unsigned int , TDirectory * > > > > &histoList() const { return m_histoList; }

public:
  bool check() {
    unsigned int counter=0;
    for (std::map< std::string, std::map< std::string, std::map< std::string, std::map<unsigned int , TDirectory *> > > >::const_iterator rod_iter = histoList().begin();
	 rod_iter != histoList().end();
	 ++rod_iter) {
    for (std::map< std::string, std::map< std::string, std::map<unsigned int , TDirectory * > > >::const_iterator conn_iter = rod_iter->second.begin();
	 conn_iter != rod_iter->second.end();
	 ++conn_iter) {
      for (std::map< std::string, std::map<unsigned int , TDirectory * > >::const_iterator histo_iter = conn_iter->second.begin();
	   histo_iter != conn_iter->second.end();
	   ++histo_iter) {
	for (std::map<unsigned int , TDirectory * >::const_iterator serial_iter = histo_iter->second.begin();
	     serial_iter != histo_iter->second.end();
	     ++serial_iter) {
	  counter++;
	}
      }
    }
    }
    return counter;
  }


private:
  std::map< std::string, std::map< std::string, std::map< std::string, std::map<unsigned int , TDirectory * > > > > m_histoList;
  static std::map<unsigned int , TDirectory * > s_emptyHistoList;
};

  std::map<unsigned int , TDirectory * > BatchAnalyser::s_emptyHistoList;

bool BatchAnalyser::shallEnter(TKey *a_key, const std::vector<std::string> &path) {

  if (path.size()==2) {
    std::unique_ptr<TObject> dir_obj( a_key->ReadObj() );
    if (dir_obj.get()) {
      assert( dir_obj->InheritsFrom(TDirectory::Class()));
      TDirectory *a_dir = static_cast<TDirectory *>(dir_obj.release() );
      unsigned int serial_number = 0;
      if (path[0].size()>0 && path[0][0]=='S' && isdigit(path[0][1])) {
	serial_number = atoi(&(path[0].c_str()[1]));
      }
      else {
	std::cout << "WARNING [BatchAnalyser::shallEnter] Path is not a serial number " << path[0] << "." << std::endl;
      }
      std::string module_name  = a_key->GetName();
      if (path[1].empty() || module_name.empty()) {
	std::cout << "WARNING [BatchAnalyser::shallEnter] ROD (\"" << path[1]  << "\") or (\"" << module_name<< "\")." << std::endl;
      }
      analyse(serial_number, path[1], module_name, a_dir);
    }
  }

  return path.size()<2;
}

class ThresholdAnalyser : public BatchAnalyser
{
public:
  enum EHistos {kThreshold, kDeltaThreshold, kNoise, kDeltaNoise, kChiSquare, kDeltaChiSquare, kThrNoise, kNHistos };

  ThresholdAnalyser()
    : m_chi2Min(0.02),
      m_goodFitMin(10000),
      m_invertIgnore(false)
  {
    m_histoNames.push_back(s_scurveMeanName);
    m_histoNames.push_back(s_scurveSigmaName);
    m_histoNames.push_back(s_scurveChiName);

    for (std::vector<std::string>::const_iterator histo_type_iter = m_histoNames.begin();
	 histo_type_iter != m_histoNames.end();
	 ++histo_type_iter) {
      m_cuts.push_back( std::vector< std::pair<float, float> >());
      m_mapName.push_back( std::vector<std::string>());
    }

    m_cuts.push_back( std::vector< std::pair<float, float> >());
    m_mapName.push_back( std::vector<std::string>());

    //    m_cuts[0].push_back(std::make_pair<float,float>(4850.,5800.) );
    //    m_mapName[0].push_back( "funny_bump" );

    m_cuts[0].push_back(std::make_pair<float,float>(3500.,3800.) );
    m_mapName[0].push_back( "low_thr_bump" );

    m_cuts[0].push_back(std::make_pair<float,float>(0,3000.) );
    m_mapName[0].push_back( "low_thr" );

    m_cuts[0].push_back(std::make_pair<float,float>(4500.,10000) );
    m_mapName[0].push_back( "high_thr" );


    m_cuts[1].push_back(std::make_pair<float,float>(460,660.) );
    m_mapName[1].push_back( "noise_bump_normal" );

    m_cuts[1].push_back(std::make_pair<float,float>(1,140) );
    m_mapName[1].push_back( "disconnected" );

    m_cuts[1].push_back(std::make_pair<float,float>(280,10000) );
    m_mapName[1].push_back( "hv_off" );

    m_cuts[2].push_back(std::make_pair<float,float>(-FLT_MAX,0.05) );
    m_mapName[2].push_back( "too_good_fit" );

    m_cuts[2].push_back(std::make_pair<float,float>(5.3,9.3) );
    m_mapName[2].push_back( "chi2_bump" );

    m_cuts[3].push_back(std::make_pair<float,float>(0,8) );
    m_mapName[3].push_back( "low_significance" );

    m_cuts[3].push_back(std::make_pair<float,float>(8,10) );
    m_mapName[3].push_back( "mid_significance" );
  }

  void setIgnore(const std::set<std::string> &ignore_names) { m_ignore=ignore_names; }
  void setInvertIgnore(bool invert=true) {m_invertIgnore=invert; }

  void analyse(unsigned int serial_number, const std::string &rod_name, const std::string &module_name, TDirectory *conn_dir) {
    if (!m_ignore.empty()) {
      if (m_invertIgnore) {
        if (m_ignore.find(module_name) == m_ignore.end() && m_ignore.find(rod_name) == m_ignore.end()) return;
      }
      else {
        if (m_ignore.find(module_name) != m_ignore.end() || m_ignore.find(rod_name) != m_ignore.end()) return;
      }
    }
    std::vector<std::string>::const_iterator histo_name_iter = m_histoNames.begin();
    if (histo_name_iter == m_histoNames.end()) return;

    HistoInfo_t info;
    std::vector<TDirectory *> histo_dir_list;
    try {
    TDirectory *a_dir = PixA::RootResultFileAccessBase::enter(conn_dir,*histo_name_iter);
    if (!a_dir) {
      std::cout << "ERROR [BatchAnalyser::analyse] No histogram " << *histo_name_iter << " for " << serial_number << ", "
		<< rod_name << ", " << module_name << "." << std::endl;
      return;
    }


    PixA::RootResultFileAccessBase::numberOfHistograms(a_dir, info, false);
    if (info.maxHistoIndex() - info.minHistoIndex()==0) {
      std::cout << "ERROR [BatchAnalyser::analyse] Empty histogram directory " << *histo_name_iter << " for " << serial_number << ", "
		<< rod_name << ", " << module_name << "." << std::endl;
      return;
    }

    unsigned int n_histos=info.maxHistoIndex() - info.minHistoIndex();
    for(unsigned int loop_i=0; loop_i<3; loop_i++) {
      if (info.maxIndex(loop_i)<=0) {
	break;
      }
      n_histos *= info.maxIndex(loop_i);
    }
    if (n_histos != 1) {
      std::cout << "ERROR [BatchAnalyser::analyse] More than one histogram in histogram directory " << s_scurveMeanName << " for " << serial_number << ", "
		<< rod_name << ", " << module_name << "." << std::endl;
      return;
    }
    PixA::Index_t index;
    info.makeDefaultIndexList(index);

    //    std::vector<std::shared_ptr<TH1> > h1;
    std::vector< TDirectory *> histo_dirs;
    for (;
	 histo_name_iter != m_histoNames.end();
	 ++histo_name_iter) {
      if (!a_dir) {
	a_dir = PixA::RootResultFileAccessBase::enter(conn_dir,*histo_name_iter);
	if (!a_dir) {
	  std::cout << "ERROR [BatchAnalyser::analyse] No histogram " << *histo_name_iter << " for " << serial_number << ", "
		    << rod_name <<  ", " << module_name << "." << std::endl;
	  continue;
	}
	PixA::RootResultFileAccessBase::numberOfHistograms(a_dir, info, false);
      }

      unsigned int n_histos=info.maxHistoIndex() - info.minHistoIndex();
      for(unsigned int loop_i=0; loop_i<3; loop_i++) {
	if (info.maxIndex(loop_i)<=0) {
	  break;
	}
	n_histos *= info.maxIndex(loop_i);
      }
      if (n_histos != 1) {
	std::cout << "ERROR [BatchAnalyser::analyse] More than one histogram in histogram directory " << s_scurveMeanName << " for " << serial_number << ", "
		  << rod_name << ", " << module_name << "." << std::endl;
	return;
      }

      //       TH1 *a_h1 = PixA::RootResultFileAccessBase::histogram(a_dir, index, false);
      //       if (!a_h1) {
      // 	std::cout << "ERROR [BatchAnalyser::analyse] More than one histogram in histogram directory " << s_scurveMeanName << " for " << serial_number << ", "
      // 		  << rod_name << ", " << module_name << "." << std::endl;
      // 	break;
      //       }
      //       a_h1->SetDirectory(NULL);

      histo_dirs.push_back(a_dir);
      a_dir =NULL;
    }

    if (histo_dirs.size() == m_histoNames.size()) {
      histo_name_iter = m_histoNames.begin();
      std::cout << "INFO [BatchAnalyser::analyse] Store " << histo_dirs.size() << " for " << serial_number << " , "
		<< rod_name << ", " << module_name << "." << std::endl;
      std::vector<TDirectory *>::const_iterator dir_iter = histo_dirs.begin();
      for(std::vector< std::string >::const_iterator histo_name_iter = m_histoNames.begin();
	  histo_name_iter != m_histoNames.end();
	  ++histo_name_iter, ++dir_iter) {
	assert(dir_iter  != histo_dirs.end());
	storeHisto( rod_name, module_name, *histo_name_iter, serial_number, *dir_iter);
      }
    }
    else {
      std::cout << "ERROR [BatchAnalyser::analyse] Did not get all histograms " << histo_dirs.size() << " != " 
		<<  m_histoNames.size() << " for  " << serial_number << ", "
		<< rod_name << ", " << module_name << "." << std::endl;
    }
    }
    catch (PixA::MissingResultDirectory &err) {
      std::cout << "ERROR [BatchAnalyser::analyse] Caught exception : " << err.getDescriptor() << std::endl;
      if (conn_dir) {
	conn_dir->ls();
      }
    }
    //    check();
  }

  void analyseAccumulated();
  
  bool save(const std::string &output_name);

private:
  float m_chi2Min;
  unsigned int m_goodFitMin;

  std::set<std::string> m_ignore;
  std::vector< std::vector< TH1 * > > m_resultH1;
  std::vector< std::vector< TH2 * > > m_resultH2;
  std::vector< std::vector< TH1 * > > m_resultH2file;
  std::vector< std::vector< TH1 * > > m_resultH2module;
  std::vector< std::vector< TH1 * > > m_resultH2fe;
  std::vector< std::vector<std::string> > m_mapName;
  std::vector< std::vector< std::pair<float, float> > > m_cuts;
  std::vector<std::string> m_histoNames;
  static const std::string s_scurveMeanName;
  static const std::string s_scurveSigmaName;
  static const std::string s_scurveChiName;
  
  bool m_invertIgnore;
};


void ThresholdAnalyser::analyseAccumulated() {
  
//   std::cout << "INFO [BatchAnalyser::analyseAccumulated] check start (0)"  << std::endl;
//   check();
//   std::cout << "INFO [BatchAnalyser::analyseAccumulated] check done (0)"  << std::endl;

  std::map<unsigned short, unsigned short> pixel_type_map;
  std::vector<std::string> type_name_extension;

  pixel_type_map[PixA::PixelTypeMask_t::kNormalPixel]=0;
  type_name_extension.push_back(std::string(""));
  pixel_type_map[PixA::PixelTypeMask_t::kLongPixel]=1;
  type_name_extension.push_back(std::string("_long"));
  pixel_type_map[PixA::PixelTypeMask_t::kGangedPixel]=2;
  type_name_extension.push_back(std::string("_ganged"));
  pixel_type_map[PixA::PixelTypeMask_t::kLongGangedPixel]=3;
  type_name_extension.push_back(std::string("_long_ganged"));
  pixel_type_map[PixA::PixelTypeMask_t::kInterGangedPixel]=4;
  type_name_extension.push_back(std::string("_inter_ganged"));
  pixel_type_map[PixA::PixelTypeMask_t::kLongInterGangedPixel]=5;
  type_name_extension.push_back(std::string("_long_inter_ganged"));

  std::map<unsigned int, unsigned int> serial_number_map;
  {
    unsigned int n_modules=0;
    for (std::map< std::string, std::map< std::string, std::map< std::string, std::map<unsigned int , TDirectory * > > > >::const_iterator 
	   rod_iter = histoList().begin();
	 rod_iter != histoList().end();
	 ++rod_iter) {

      for (std::map< std::string, std::map< std::string, std::map<unsigned int , TDirectory * > > >::const_iterator conn_iter = rod_iter->second.begin();
	   conn_iter != rod_iter->second.end();
	   ++conn_iter) {
	n_modules++;
	for (std::map< std::string, std::map<unsigned int , TDirectory * > >::const_iterator histo_iter = conn_iter->second.begin();
	     histo_iter != conn_iter->second.end();
	     ++histo_iter) {

	  for (std::map<unsigned int , TDirectory * >::const_iterator file_iter = histo_iter->second.begin();
	       file_iter != histo_iter->second.end();
	       ++file_iter) {
	
	    std::map<unsigned int, unsigned int>::iterator serial_iter = serial_number_map.find(file_iter->first);
	    if (serial_iter == serial_number_map.end()) {
	      serial_number_map.insert(std::make_pair(file_iter->first, serial_number_map.size()));
	    }
	  }
	}
      }
    }

    m_resultH2.reserve(m_mapName.size());
    m_resultH2file.reserve(m_mapName.size());
    m_resultH2module.reserve(m_mapName.size());
    m_resultH2fe.reserve(m_mapName.size());
    unsigned int files = serial_number_map.size();
    for (unsigned int h1_i=0; h1_i<m_mapName.size(); h1_i++) {

      m_resultH2.push_back( std::vector<TH2 *>() );
      m_resultH2file.push_back( std::vector<TH1 *>() );
      m_resultH2module.push_back( std::vector<TH1 *>() );
      m_resultH2fe.push_back( std::vector<TH1 *>() );
      for (std::vector<std::string>::const_iterator map_name_iter = m_mapName[h1_i].begin();
	   map_name_iter != m_mapName[h1_i].end();
	   ++map_name_iter) {

	m_resultH2.back().push_back(new TH2F(map_name_iter->c_str(),map_name_iter->c_str() ,18*8,-0.5,18*8-.5, 160*2, -.5, 160*2-.5));

	m_resultH2file.back().push_back(new TH1F(((*map_name_iter)+"_file").c_str(),((*map_name_iter)+"_file").c_str() ,files, -0.5, files-.5));
	TH1 *h1=m_resultH2file.back().back();
	if (h1->GetXaxis()) {
          unsigned int counter=0;
	  for (std::map<unsigned int, unsigned int>::iterator serial_iter=serial_number_map.begin();
	       serial_iter != serial_number_map.end();
	       ++serial_iter) {
	    // chose more sensible mapping of serial numbers to bins
            serial_iter->second=counter++;
	    unsigned int bin_i =h1->GetXaxis()->FindBin(serial_iter->second);
	    if (bin_i>0 && bin_i<=h1->GetNbinsX()) {
	      std::stringstream bin_name;
	      bin_name << 'S' << std::setfill('0') << std::setw(9) << serial_iter->first;
	      h1->GetXaxis()->SetBinLabel(bin_i,bin_name.str().c_str());
	    }
	  }
	}
	m_resultH2module.back().push_back(new TH1F(((*map_name_iter)+"_modules").c_str(),((*map_name_iter)+"_modules").c_str() ,n_modules, -0.5, n_modules-.5));
	m_resultH2fe.back().push_back(new TH1F(((*map_name_iter)+"_fe").c_str(),((*map_name_iter)+"_fe").c_str() ,16, -0.5, 15+.5));

      }
    }
  }
  m_resultH1.resize(kNHistos);
  for (unsigned int h1_i=0; h1_i<m_resultH1.size(); h1_i++) {
    m_resultH1[h1_i].reserve(6);
  }
  std::string delta_name ="delta_";
  //  if (false) {
  for (unsigned int pixel_type_i=0; pixel_type_i<6; pixel_type_i++) {
//     std::cout << "INFO [BatchAnalyser::analyseAccumulated] check start (0)" << pixel_type_i << std::endl;
//     check();
//     std::cout << "INFO [BatchAnalyser::analyseAccumulated] check done (0)" << pixel_type_i << std::endl;
    assert( type_name_extension.size()>pixel_type_i);
    std::string name;
    name = "THRESHOLD";
    name += type_name_extension[pixel_type_i];
    assert(m_resultH1.size()>=kNHistos);

    m_resultH1[kThreshold].push_back(new TH1D(name.c_str(), name.c_str(), 500, 0.,10000.));
    m_resultH1[kThreshold].back()->SetDirectory(NULL);
    name += "__delta";
    m_resultH1[kDeltaThreshold].push_back(new TH1D(name.c_str(), name.c_str(), 100, -200.,200.));
    m_resultH1[kDeltaThreshold].back()->SetDirectory(NULL);

//     std::cout << "INFO [BatchAnalyser::analyseAccumulated] check start (0t)" << pixel_type_i << std::endl;
//     check();
//     std::cout << "INFO [BatchAnalyser::analyseAccumulated] check done (0t)" << pixel_type_i << std::endl;

    name = "NOISE";
    name += type_name_extension[pixel_type_i];
    m_resultH1[kNoise].push_back(new TH1D(name.c_str(), name.c_str(), 500, 0.,2000.));
    m_resultH1[kNoise].back()->SetDirectory(NULL);
    name += "__delta";
    m_resultH1[kDeltaNoise].push_back(new TH1D(name.c_str(), name.c_str(), 1000, -200.,200.));
    m_resultH1[kDeltaNoise].back()->SetDirectory(NULL);

//     std::cout << "INFO [BatchAnalyser::analyseAccumulated] check start (0n)" << pixel_type_i << std::endl;
//     check();
//     std::cout << "INFO [BatchAnalyser::analyseAccumulated] check done (0n)" << pixel_type_i << std::endl;

    name = "CHI2";
    name += type_name_extension[pixel_type_i];
    m_resultH1[kChiSquare].push_back(new TH1D(name.c_str(), name.c_str(), 1000, 0.,25.));
    m_resultH1[kChiSquare].back()->SetDirectory(NULL);
    name += "__delta";
    m_resultH1[kDeltaChiSquare].push_back(new TH1D(name.c_str(), name.c_str(), 100, -200.,200.));
    m_resultH1[kDeltaChiSquare].back()->SetDirectory(NULL);

    name = "THR_OVER_NOISE";
    name += type_name_extension[pixel_type_i];
    m_resultH1[kThrNoise].push_back(new TH1D(name.c_str(), name.c_str(), 500, 0.,100.));
    m_resultH1[kThrNoise].back()->SetDirectory(NULL);

//     std::cout << "INFO [BatchAnalyser::analyseAccumulated] check start (0c)" << pixel_type_i << std::endl;
//     check();
//     std::cout << "INFO [BatchAnalyser::analyseAccumulated] check done (0c)" << pixel_type_i << std::endl;
  }
  //  }

//   std::cout << "INFO [BatchAnalyser::analyseAccumulated] check start (1)"  << std::endl;
//   check();
//   std::cout << "INFO [BatchAnalyser::analyseAccumulated] check done (1)"  << std::endl;

  std::vector< std::vector< std::vector< std::vector<double> > > > stat;
  for (std::map< std::string, std::map< std::string, std::map< std::string, std::map<unsigned int , TDirectory * > > > >::const_iterator 
	 rod_iter = histoList().begin();
       rod_iter != histoList().end();
       ++rod_iter) {

  for (std::map< std::string, std::map< std::string, std::map<unsigned int , TDirectory * > > >::const_iterator conn_iter = rod_iter->second.begin();
       conn_iter != rod_iter->second.end();
       ++conn_iter) {
    stat.push_back( std::vector< std::vector< std::vector<double> > >() );
    stat.back().resize(4);
    for (unsigned int j=0; j<4; j++) {
      stat.back()[j].resize(rod_iter->second.size());
      stat.back()[j].resize(m_histoNames.size());
      for (unsigned int i=0; i<m_histoNames.size(); i++) {
	stat.back()[j][i].resize(6,0.);
      }
    }
  }
  }
//   std::cout << "INFO [BatchAnalyser::analyseAccumulated] check start (2)"  << std::endl;
//   check();
//   std::cout << "INFO [BatchAnalyser::analyseAccumulated] check done (2)"  << std::endl;
  unsigned int n_modules=0;
  unsigned int module_i=0;
  for (std::map< std::string, std::map< std::string, std::map< std::string, std::map<unsigned int , TDirectory * > > > >::const_iterator 
	 rod_iter = histoList().begin();
       rod_iter != histoList().end();
       ++rod_iter) {

  std::cout << "INFO new ROD  : " << rod_iter->first << " with " << rod_iter->second.size() << " module(s)."<<std::endl;

  for (std::map< std::string, std::map< std::string, std::map<unsigned int , TDirectory * > > >::const_iterator conn_iter = rod_iter->second.begin();
       conn_iter != rod_iter->second.end();
       ++conn_iter, ++module_i) {
    n_modules++;

    unsigned int module_serial_number=0;
    {
    std::map< std::string, std::map<unsigned int , TDirectory * > >::const_iterator histo_iter = conn_iter->second.find(s_scurveChiName);
    if (histo_iter == conn_iter->second.end() || histo_iter->second.empty()) {
      std::cout << "ERROR [BatchAnalyser::analyse] No chi2 histogram for : " << conn_iter->first << " <-" << rod_iter->first << std::endl;
      continue;
    }

    // take the last scan with enough good fits.
    for (std::map<unsigned int , TDirectory * >::const_reverse_iterator serial_iter = histo_iter->second.rbegin();
	 serial_iter != histo_iter->second.rend();
	 ++serial_iter) {

      HistoInfo_t info;
      PixA::RootResultFileAccessBase::numberOfHistograms(serial_iter->second, info, false);
      PixA::Index_t index;
      info.makeDefaultIndexList(index);
      std::unique_ptr<TH1> a_h1_ref( PixA::RootResultFileAccessBase::histogram(serial_iter->second, index, false));
      if (!a_h1_ref.get()) {
	std::cout << "ERROR [BatchAnalyser::analyse] More than one histogram in histogram directory " << histo_iter->first << " for " << serial_iter->first << ", "
		  << rod_iter->first << ", " << conn_iter->first << "." << std::endl;
	break;
      }
      a_h1_ref->SetDirectory(NULL);

      if (a_h1_ref->InheritsFrom(TH2::Class())) {
	unsigned int n_undefined_pixelTypes=0;
	TH2 *h2=static_cast<TH2 *>(a_h1_ref.get());
	unsigned int good_fits=0;
	for (unsigned int binx_i=1; binx_i<=static_cast<unsigned int>(h2->GetNbinsX()); binx_i++) {
	  for (unsigned int biny_i=1; biny_i<=static_cast<unsigned int>(h2->GetNbinsY()); biny_i++) {
	    double bin_content = h2->GetBinContent(h2->GetBin(binx_i, biny_i) );
	    if (bin_content>m_chi2Min) {
	      if (++good_fits>m_goodFitMin) {
		break;
	      }
	    }
	  }
	}
	if (good_fits>m_goodFitMin) {
	  module_serial_number = serial_iter->first;
	}
      }
    }

    if (module_serial_number==0) { 
      std::cout << "ERROR [BatchAnalyser::analyse] No good fit for : " << conn_iter->first << " <-" << rod_iter->first << std::endl;
      continue;
    }
    }
    //    std::cout << "INFO new conn name : " << conn_iter->first <<std::endl;
    unsigned int result_h1_index=0;
    for (std::vector<std::string>::const_iterator histo_name_iter = m_histoNames.begin();
	 histo_name_iter != m_histoNames.end();
	 ++histo_name_iter, ++result_h1_index) {

      //      std::cout << "INFO new hist name :" << (histo_name_iter - m_histoNames.begin()) << " : " << *histo_name_iter <<std::endl;

    std::map< std::string, std::map<unsigned int , TDirectory * > >::const_iterator histo_iter = conn_iter->second.find(*histo_name_iter);
    if (histo_iter == conn_iter->second.end() || histo_iter->second.empty()) {
      std::cout << "ERROR [BatchAnalyser::analyseAccumulated] No histograms " << *histo_name_iter << " for " << conn_iter->first << "." << std::endl;
      continue;
    }
    assert( result_h1_index < m_cuts.size() );



    std::map<unsigned int , TDirectory * >::const_iterator serial_iter = histo_iter->second.find(module_serial_number);
    if (serial_iter ==histo_iter->second.end()) {
      std::cout << "ERROR [BatchAnalyser::analyse] No histogram " << *histo_name_iter << " for  " 
		<< conn_iter->first << " (" << rod_iter->first << ") in " << module_serial_number << ", although, the chi2 was fine." << std::endl;
      continue;
    }

    assert (serial_iter->second);

    unsigned int serial_number_id=serial_number_map.size();
    {
      std::map<unsigned int, unsigned int>::iterator serial_map_iter = serial_number_map.find(serial_iter->first);
      if (serial_map_iter != serial_number_map.end()) {
	serial_number_id = serial_map_iter->second;
      }
    }

    HistoInfo_t info;
    PixA::RootResultFileAccessBase::numberOfHistograms(serial_iter->second, info, false);
    PixA::Index_t index;
    info.makeDefaultIndexList(index);
    std::unique_ptr<TH1> a_h1_thr;
    std::uniqye_ptr<TH1> a_h1_ref( PixA::RootResultFileAccessBase::histogram(serial_iter->second, index, false));
    if (!a_h1_ref.get()) {
      std::cout << "ERROR [BatchAnalyser::analyse] More than one histogram in histogram directory " << histo_iter->first << " for " << serial_iter->first << ", "
		<< rod_iter->first << ", " << conn_iter->first << "." << std::endl;
      break;
    }
    a_h1_ref->SetDirectory(NULL);

    if (*histo_name_iter== s_scurveSigmaName) {
      std::map< std::string, std::map<unsigned int , TDirectory * > >::const_iterator thr_histo_iter = conn_iter->second.find(s_scurveMeanName);
      if (thr_histo_iter != conn_iter->second.end()) {
	std::map<unsigned int , TDirectory * >::const_iterator thr_serial_iter = thr_histo_iter->second.find(serial_iter->first);
	if (thr_serial_iter != thr_histo_iter->second.end()) {
	  a_h1_thr = std::unique_ptr<TH1>( PixA::RootResultFileAccessBase::histogram(thr_serial_iter->second, index, false));
	}
      }
      if (!a_h1_thr.get()) {
	std::cout << "WARNING [BatchAnalyser::analyse] No threshold mean for " << serial_iter->first << ", "
		  << rod_iter->first << ", " << conn_iter->first << "." << std::endl;
      }
    }


    if (a_h1_ref->InheritsFrom(TH2::Class())) {
      unsigned int n_undefined_pixelTypes=0;
      TH2 *h2=static_cast<TH2 *>(a_h1_ref.get());
      TH2 *h2_thr=NULL;
      if (a_h1_thr.get() && a_h1_thr->InheritsFrom(TH2::Class())) {
	h2_thr=static_cast<TH2 *>(a_h1_thr.get());
      }
      unsigned int histo_i = histo_name_iter-m_histoNames.begin();
      assert( stat.size()> module_i && histo_i < stat[module_i][0].size() && stat[module_i][0][histo_i].size()>=6);
      std::vector<double> &pixelCounter = stat[module_i][0][histo_i];
      std::vector<double> &emptyPixelCounter = stat[module_i][1][histo_i];
      std::vector<double> &sum = stat[module_i][2][histo_i];
      std::vector<double> &sum2 = stat[module_i][3][histo_i];
      for (unsigned int binx_i=1; binx_i<=static_cast<unsigned int>(h2->GetNbinsX()); binx_i++) {
	for (unsigned int biny_i=1; biny_i<=static_cast<unsigned int>(h2->GetNbinsY()); biny_i++) {
	  PixA::PixelCoord_t coord(binx_i-1, biny_i-1);
	  unsigned short type=PixA::PixelTypeMask_t::type( coord.col(), coord.row());
	  std::map<unsigned short, unsigned short>::const_iterator pixel_type_iter = pixel_type_map.find(type);
	  if (pixel_type_iter != pixel_type_map.end()) {
	    double bin_content = h2->GetBinContent(h2->GetBin(binx_i, biny_i) );
	    for(std::vector<std::pair<float,float> >::const_iterator cut_iter = m_cuts[result_h1_index].begin();
		cut_iter != m_cuts[result_h1_index].end();
		++cut_iter) {
	      if (bin_content>= cut_iter->first && bin_content< cut_iter->second) {
		unsigned int cut_index = cut_iter - m_cuts[result_h1_index].begin();
		m_resultH2[result_h1_index][cut_index]->Fill( binx_i-1, biny_i-1);
		m_resultH2file[result_h1_index][cut_index]->Fill( serial_number_id );
		m_resultH2module[result_h1_index][cut_index]->Fill( module_i );
		m_resultH2fe[result_h1_index][cut_index]->Fill( coord.chip() );
	      }
	    }
	    assert(result_h1_index*2< kNHistos && m_resultH1[result_h1_index*2].size()>=pixel_type_iter->second);
	    m_resultH1[result_h1_index*2][pixel_type_iter->second]->Fill( bin_content);
	    if (bin_content>0 && h2_thr) {
	      double thr_bin_content = h2_thr->GetBinContent(h2->GetBin(binx_i, biny_i) );
	      double significance = thr_bin_content/bin_content;
	      unsigned int thr_over_noise_index=3;
	      for(std::vector<std::pair<float,float> >::const_iterator cut_iter = m_cuts[thr_over_noise_index].begin();
		  cut_iter != m_cuts[thr_over_noise_index].end();
		  ++cut_iter) {
		if (significance>= cut_iter->first && significance< cut_iter->second) {
		  unsigned int cut_index = cut_iter - m_cuts[thr_over_noise_index].begin();
		  m_resultH2[thr_over_noise_index][cut_index]->Fill( binx_i-1, biny_i-1);
		  m_resultH2file[thr_over_noise_index][cut_index]->Fill( serial_number_id );
		  m_resultH2module[thr_over_noise_index][cut_index]->Fill( module_i );
		  m_resultH2fe[thr_over_noise_index][cut_index]->Fill( coord.chip() );
		}
	      }
	      m_resultH1[kThrNoise][pixel_type_iter->second]->Fill( significance);
	    }
	    if ( bin_content>0) {
	      sum[pixel_type_iter->second]+= bin_content;
	      sum2[pixel_type_iter->second]+= bin_content*bin_content;
	      pixelCounter[pixel_type_iter->second] += 1;
	    }
	    else {
	      emptyPixelCounter[pixel_type_iter->second] += 1;
	    }
	  }
	  else {
	    n_undefined_pixelTypes++;
	  }
	}
      }
      if(n_undefined_pixelTypes>0) {
	std::cout << "ERROR [BatchAnalyser::analyseAccumulated] Histogram " << *histo_name_iter << " is empty for " << conn_iter->first << "." << std::endl;
      }
      //      std::cout << "INFO [BatchAnalyser::analyseAccumulated] " << conn_iter->first << " : " << *histo_name_iter << " / " << serial_iter->first << std::endl;
      //       for (unsigned int i=0; i<6; i++) {
      // 	if (pixelCounter[i]>0) {
      // 	  std::cout << "\t" << i << ":" << std::setw(14) << sum[i]/pixelCounter[i] 
      // 		    << "+-" << std::setw(14)  << sqrt((sum2[i]-sum[i]*sum[i]/pixelCounter[i])/(pixelCounter[i]-1)) 
      // 		    << " " << std::setw(6) << pixelCounter[i] << " / " << std::setw(6) << emptyPixelCounter[i] 
      // 		    << std::endl;
      // 	}
      //       }
      //      std::cout << std::endl;


      {
      TH2 *h2_ref=h2;
      for (std::map<unsigned int , TDirectory * >::const_iterator serial_iter = histo_iter->second.begin();
	   serial_iter != histo_iter->second.end();
	   ++serial_iter) {

	std::uniqye_ptr<TH1> a_h1( PixA::RootResultFileAccessBase::histogram(serial_iter->second, index, false));
	if (!a_h1.get()) {
	  std::cout << "ERROR [BatchAnalyser::analyse] More than one histogram in histogram directory " << histo_iter->first << " for " << serial_iter->first << ", "
		    << rod_iter->first << ", " << conn_iter->first << "." << std::endl;
	  continue;
	}
	a_h1->SetDirectory(NULL);

	if (a_h1->InheritsFrom(TH2::Class())) {
	  h2=static_cast<TH2 *>(a_h1.get());

	  if(    h2->GetNbinsX() == h2_ref->GetNbinsX() 
	     || h2->GetNbinsY() == h2_ref->GetNbinsY() ) {

	    unsigned int n_undefined_pixelTypes=0;

	    for (unsigned int binx_i=1; binx_i<=static_cast<unsigned int>(h2->GetNbinsX()); binx_i++) {
	      for (unsigned int biny_i=1; biny_i<=static_cast<unsigned int>(h2->GetNbinsY()); biny_i++) {

		PixA::PixelCoord_t coord(binx_i-1, biny_i-1);
		unsigned short type=PixA::PixelTypeMask_t::type( coord.col(), coord.row());
		std::map<unsigned short, unsigned short>::const_iterator pixel_type_iter = pixel_type_map.find(type);
		if (pixel_type_iter != pixel_type_map.end()) {
		  unsigned int bin_i = static_cast<unsigned int>( h2->GetBin(binx_i, biny_i) );
		  double content=h2_ref->GetBinContent(bin_i) - h2->GetBinContent(bin_i);
		  m_resultH1[result_h1_index*2+1][pixel_type_iter->second]->Fill( content);
		}
		else {
		  n_undefined_pixelTypes++;
		}

	      }
	    }
	    if(n_undefined_pixelTypes>0) {
	      std::cout << "ERROR [BatchAnalyser::analyseAccumulated] Histogram " << serial_iter->first <<  " / " 
			<< *histo_name_iter << " is empty for " << conn_iter->first << "." << std::endl;
	    }

	  }
	  else {
	    std::cout << "ERROR [BatchAnalyser::analyseAccumulated] Histogram " << serial_iter->first <<  " / " 
		      << *histo_name_iter << " has different binning as reference " << conn_iter->first << "." << std::endl;
	  }
	}
	else {
	  std::cout << "ERROR [BatchAnalyser::analyseAccumulated] Histogram " << serial_iter->first <<  " / " 
		    << *histo_name_iter << " is not a 2D histogram for " << conn_iter->first << "." << std::endl;
	}

      }
      }
    }
    else {
      std::cout << "ERROR [BatchAnalyser::analyseAccumulated] Histogram " << *histo_name_iter << " is not a 2D histogram for " << conn_iter->first << "." << std::endl;
    }
  }

  }

  }

  std::vector< std::vector< std::vector< double > > > comb_stat;
  comb_stat.resize(4);
  for (unsigned int j=0; j<4; j++) {
    comb_stat[j].resize(m_histoNames.size());
    for (unsigned int i=0; i<m_histoNames.size(); i++) {
      comb_stat[j][i].resize(6,0.);
    }
  }

  std::map< float, std::string > av_noise;
  {    
  unsigned int module_i=0;
  for (std::map< std::string, std::map< std::string, std::map< std::string, std::map<unsigned int , TDirectory * > > > >::const_iterator
	 rod_iter = histoList().begin();
       rod_iter != histoList().end();
       ++rod_iter) {

    for (std::map< std::string, std::map< std::string, std::map<unsigned int , TDirectory * > > >::const_iterator conn_iter = rod_iter->second.begin();
	 conn_iter != rod_iter->second.end();
	 ++conn_iter, ++module_i) {

      for (unsigned int stat_j=0; stat_j<4; stat_j++) {
	for (unsigned int histo_i=0; histo_i<m_histoNames.size(); histo_i++) {
	  for (unsigned int pixel_type_i=0; pixel_type_i<6; pixel_type_i++) {
	    comb_stat[stat_j][histo_i][pixel_type_i]+=stat[module_i][stat_j][histo_i][pixel_type_i];
	  }
	}
      }
      unsigned int sum_stat_j=2;
      unsigned int n_stat_j=0;
      unsigned int histo_i=1; // noise
      unsigned int pixel_type_i=0; // normal
      assert(stat[module_i][sum_stat_j].size()>histo_i);

      if (stat[module_i][n_stat_j][histo_i][0]>0) {
	double module_av_noise = stat[module_i][sum_stat_j][histo_i][pixel_type_i]/stat[module_i][n_stat_j][histo_i][pixel_type_i];
	av_noise.insert(std::make_pair(static_cast<float>(module_av_noise),conn_iter->first));
      }
    }
  }
  }

  std::cout << "INFO [BatchAnalyser::analyseAccumulated] Noisy modules max. 10 / " << n_modules << " : " << std::endl;
  unsigned int counter=0;
  for (std::map<float, std::string>::reverse_iterator noisy_modules = av_noise.rbegin();
       noisy_modules != av_noise.rend() && counter<10;
       ++noisy_modules, ++counter) {
    std::cout << "\t" << std::setw(20) << noisy_modules->second << " : " << noisy_modules->first << std::endl;
  }

  std::cout << "INFO [BatchAnalyser::analyseAccumulated] Results from " << n_modules << " :: " << std::endl;
  for(unsigned int histo_i=0; histo_i<m_histoNames.size();histo_i++) {

    const std::vector<double> &pixelCounter = comb_stat[0][histo_i];
    const std::vector<double> &emptyPixelCounter = comb_stat[1][histo_i];
    const std::vector<double> &sum = comb_stat[2][histo_i];
    const std::vector<double> &sum2 = comb_stat[3][histo_i];

    std::cout << "INFO [BatchAnalyser::analyseAccumulated] " << m_histoNames[histo_i] << " : " << std::endl;
    for (unsigned int i=0; i<6; i++) {
      if (pixelCounter[i]>0) {
	std::cout << "\t" << i << ":" << std::setw(14) << sum[i]/pixelCounter[i] 
		  << "+-" << std::setw(14)  << sqrt((sum2[i]-sum[i]*sum[i]/pixelCounter[i])/(pixelCounter[i]-1)) 
		  << " " << std::setw(9) << pixelCounter[i] << " / " << std::setw(9) << emptyPixelCounter[i] 
		  << std::endl;
      }
    }
  }
  std::cout << std::endl;

  {
    std::cout << "INFO [BatchAnalyser::analyseAccumulated] Module index : " << std::endl;
    unsigned int n_modules=0;
    for (std::map< std::string, std::map< std::string, std::map< std::string, std::map<unsigned int , TDirectory * > > > >::const_iterator 
	   rod_iter = histoList().begin();
	 rod_iter != histoList().end();
	 ++rod_iter) {

      for (std::map< std::string, std::map< std::string, std::map<unsigned int , TDirectory * > > >::const_iterator conn_iter = rod_iter->second.begin();
	   conn_iter != rod_iter->second.end();
	   ++conn_iter) {
	std::cout << "INFO [BatchAnalyser::analyseAccumulated] " << std::setw(4) << n_modules << " : " << std::setw(30) << conn_iter->first
		  << " <- " << rod_iter->first << " : " << std::endl;
	n_modules++;
      }
    }
  }

  {
    std::cout << "INFO [BatchAnalyser::analyseAccumulated] Scan index : " << std::endl;
    for (std::map<unsigned int, unsigned int>::iterator serial_iter = serial_number_map.begin();
	 serial_iter != serial_number_map.end();
	 ++serial_iter) {
	std::cout << "INFO [BatchAnalyser::analyseAccumulated] " << std::setw(10) << serial_iter->second << " : " << std::setw(4) << serial_iter->first
		  << std::endl;
    }
  }



}

bool ThresholdAnalyser::save(const std::string &output_name) {
  std::unique_ptr<TFile> file(TFile::Open(output_name.c_str(), "UPDATE") );
  if (!file.get() || !file->IsOpen()) {
    file=std::unique_ptr<TFile>(TFile::Open(output_name.c_str(), "CREATE") );
  }
  if (!file->IsWritable()) {
    std::cout << "ERROR [BatchAnalyser::save] Failed to open file " << output_name << " for writing." << std::endl;
    return false;
  }

  std::vector< std::vector< std::vector< TH1 *> > *> h1_vector_list;
  h1_vector_list.push_back( &m_resultH1 );
  h1_vector_list.push_back( &m_resultH2file );
  h1_vector_list.push_back( &m_resultH2module );
  h1_vector_list.push_back( &m_resultH2fe );

  for (std::vector< std::vector< std::vector< TH1 *> > *>::iterator h1_list_iter =  h1_vector_list.begin();
       h1_list_iter != h1_vector_list.end();
       ++h1_list_iter) {
    if (*h1_list_iter) {
      for (std::vector< std::vector< TH1 * > >::iterator histo_iter = (*h1_list_iter)->begin();
	   histo_iter != (*h1_list_iter)->end();
	   ++histo_iter) {

      for (std::vector< TH1 * >::iterator pixel_type_iter = histo_iter->begin();
	   pixel_type_iter != histo_iter->end();
	   ++pixel_type_iter) {
	(*pixel_type_iter)->Write();
	(*pixel_type_iter)->SetDirectory(0);
	delete  (*pixel_type_iter);
	(*pixel_type_iter) = NULL;
      }
    }
    }
  }

  for (std::vector< std::vector< TH2 * > >::iterator histo_iter = m_resultH2.begin();
       histo_iter != m_resultH2.end();
       ++histo_iter) {

    for (std::vector< TH2 * >::iterator type_iter = histo_iter->begin();
	 type_iter != histo_iter->end();
	 ++type_iter) {
      (*type_iter)->Write();
      (*type_iter)->SetDirectory(0);
      delete  (*type_iter);
      (*type_iter) = NULL;
    }
  }
  std::cout << "INFO [ThresholdAnalyser] All done." << std::endl;

  return true;
}


const std::string ThresholdAnalyser::s_scurveMeanName("SCURVE_MEAN");
const std::string ThresholdAnalyser::s_scurveSigmaName("SCURVE_SIGMA");
const std::string ThresholdAnalyser::s_scurveChiName("SCURVE_CHI2");


int main(int argc, char **argv)
{
  std::string output_file_name="/tmp/test.root";
  std::vector<std::string> file_names;
  std::set<std::string> ignore_names;
  unsigned int verbose=1;
  bool invert_ignore=false;

  for (int arg_i=1; arg_i<argc; arg_i++) {

    if (strcmp(argv[arg_i],"-f")==0 && arg_i+1 < argc && argv[arg_i+1][0]!='-') {
      while (arg_i+1 < argc && argv[arg_i+1][0]!='-') {
	file_names.push_back(argv[++arg_i]);
      }
    }
    else if (strcmp(argv[arg_i],"--ignore")==0 && arg_i+1 < argc && argv[arg_i+1][0]!='-') {
      while (arg_i+1 < argc && argv[arg_i+1][0]!='-') {
	ignore_names.insert(argv[++arg_i]);
      }
    }
    else if (strcmp(argv[arg_i],"-o")==0 && arg_i+1 < argc && argv[arg_i+1][0]!='-') {
      output_file_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-v")==0 ) {
      verbose++;
    }
    else if (strcmp(argv[arg_i],"--invert-ignore")==0) {
     invert_ignore=true;
    }
    else if (strcmp(argv[arg_i],"-q")==0 ) {
      verbose=0;
    }
    else {
      file_names.push_back(argv[arg_i]);
    }

  }
  
  if (!file_names.empty()) {
    ThresholdAnalyser analyser;
    if (!analyser.save(output_file_name)) { // to ensure the output file is valid;
      return -1;
    }
    analyser.setIgnore(ignore_names);
    if (invert_ignore) {
      analyser.setInvertIgnore(invert_ignore);
    }
    DirectoryTreeReader reader(analyser, verbose>1);
    std::vector<std::shared_ptr<TFile> > open_files;
    for (std::vector<std::string>::const_iterator file_iter = file_names.begin();
	 file_iter != file_names.end();
	 ++file_iter) {
      if (verbose>0) {
	std::cout << "INFO ["<<argv[0] << ":main] process file "  << *file_iter << "." << std::endl;
      }
      std::shared_ptr<TFile> file(TFile::Open(file_iter->c_str()) );
      if (file.get()) {
	open_files.push_back( std::shared_ptr<TFile>(file.release()) );
	reader.read( open_files.back().get() );
      }

//     if (verbose>0) {
//       std::cout << "INFO ["<<argv[0] << ":main] check start." << std::endl;
//       analyser.check();
//       std::cout << "INFO ["<<argv[0] << ":main] done." << std::endl;
//     }
      

    }
    if (verbose>0) {
      std::cout << "INFO ["<<argv[0] << ":main] analyse accumulated histograms." << std::endl;
    }
//     if (verbose>0) {
//       std::cout << "INFO ["<<argv[0] << ":main] check start." << std::endl;
//       analyser.check();
//       std::cout << "INFO ["<<argv[0] << ":main] done." << std::endl;
//     }

    analyser.analyseAccumulated();
    analyser.save(output_file_name);
    

  }
  return 0;
}
