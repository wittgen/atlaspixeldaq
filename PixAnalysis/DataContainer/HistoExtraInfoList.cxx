#include "HistoExtraInfoList.h"

namespace PixA {

  HistoExtraInfoList::HistoExtraInfoList() {

    {
      HistoExtraInfo_t &extra_info = m_extraInfo["RAW_DATA_DIFF_2"];
      extra_info.axis(HistoExtraInfo_t::kXaxis)/*.setTitle("RX Delay (ns)")*/.addScanVariable("BOC_RX_DELAY");
      extra_info.axis(HistoExtraInfo_t::kYaxis)/*.setTitle("RX Threshold")*/.addScanVariable("BOC_RX_THR");
      extra_info.axis(HistoExtraInfo_t::kZaxis).setTitle("Transmission Errors");
    }

    {
      HistoExtraInfo_t &extra_info = m_extraInfo["RAW_DATA_DIFF_1"];
      extra_info.axis(HistoExtraInfo_t::kXaxis).setTitle("Pattern");
      extra_info.axis(HistoExtraInfo_t::kYaxis)/*.setTitle("RX Threshold")*/.addScanVariable("BOC_RX_DELAY").addScanVariable("BOC_RX_THR").addScanVariable("BOC_TX_MS");
    }

    HistoAxisInfo_t::addDefaultVarTitle("BOC_RX_DELAY","RX Delay (ns)");
    HistoAxisInfo_t::addDefaultVarTitle("BOC_RX_THR","RX Threshold");
    HistoAxisInfo_t::addDefaultVarTitle("BOC_TX_MS","TX MSR");

    {
      HistoExtraInfo_t &extra_info = m_extraInfo["RAW_DATA_0"];
      extra_info.axis(HistoExtraInfo_t::kXaxis).setTitle("Pattern");
      extra_info.axis(HistoExtraInfo_t::kYaxis)/*.setTitle("RX Threshold")*/.addScanVariable("BOC_RX_DELAY").addScanVariable("BOC_RX_THR");
    }

    {
      HistoExtraInfo_t &extra_info = m_extraInfo["RAW_DATA_1"];
      extra_info.axis(HistoExtraInfo_t::kXaxis).setTitle("Pattern");
      extra_info.axis(HistoExtraInfo_t::kYaxis)/*.setTitle("RX Threshold")*/.addScanVariable("BOC_RX_DELAY").addScanVariable("BOC_RX_THR").addScanVariable("BOC_TX_MS");
    }

    {
      HistoExtraInfo_t &extra_info = m_extraInfo["RAW_DATA_REF"];
      extra_info.axis(HistoExtraInfo_t::kXaxis).setTitle("Pattern");
      extra_info.axis(HistoExtraInfo_t::kYaxis);
    }

    {
      HistoExtraInfo_t &extra_info = m_extraInfo["OCCUPANCY"];
      extra_info.axis(HistoExtraInfo_t::kZaxis).setTitle("Events");
    }

    {
      HistoExtraInfo_t &extra_info = m_extraInfo["SCURVE_MEAN"];
      extra_info.axis(HistoExtraInfo_t::kZaxis).setTitle("Threshold (e^{-})");
    }

    {
      HistoExtraInfo_t &extra_info = m_extraInfo["SCURVE_SIGMA"];
      extra_info.axis(HistoExtraInfo_t::kZaxis).setTitle("Noise (e^{-})");
    }

    {
      HistoExtraInfo_t &extra_info = m_extraInfo["SCURVE_CHI2"];
      extra_info.axis(HistoExtraInfo_t::kZaxis).setTitle("#chi^{2}/n.d.f");
    }

    {
      HistoExtraInfo_t &extra_info = m_extraInfo["TIMEWALK"];
      extra_info.axis(HistoExtraInfo_t::kZaxis).setTitle("strobe delay (ns)");
    }
  }

  HistoExtraInfoList *HistoExtraInfoList::s_instance=NULL;

  std::map<std::string, std::string> HistoAxisInfo_t::s_varTitleList;


}
