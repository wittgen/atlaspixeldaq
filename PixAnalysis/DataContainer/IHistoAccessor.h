#ifndef _IHistoAccessor_h_
#define _IHistoAccessor_h_

#include <vector>
#include <string>
#include "HistoInfo_t.h"

namespace PixLib {
  class Histo;
}

namespace PixA {

  class HistoRef;

  class IHistoAccessor
  {
    friend class HistoRef;
  public:
    virtual ~IHistoAccessor() {}
  protected:

    /** Called before an access is started.
     */
    virtual void accessStart() =0;

    /** Called onve everything is done.
     */
    virtual void accessEnd() =0;


    /** Count the number of histograms which exist for the given name.
     * @param name the name of the histogram collection.
     * @param info will be filled with the maximum number of histograms per level (0-2) or -1 and the number of histograms at deepest level.
     * The default method will create a vector filled with the one element and will call the 
     * the method which takes a folder hierarchy.
     */
    virtual bool numberOfHistos(const std::string &name, HistoInfo_t &info ) {
      std::vector<std::string> temp;
      temp.push_back(name);
      return numberOfHistos(temp,info);
    }

    /** Return true if the histogramm of the given name exists.
     */
    virtual bool haveHisto(const std::string &name) {
      std::vector<std::string> temp;
      temp.push_back(name);
      return haveHisto(temp);
    }

    /** Returns the requested PixLib::Histo of the histogram collection of the given name.
     * @return a pointer to a newly created histogramm or NULL.
     * The caller is responsible for deleting the hisogram.
     */
    virtual PixLib::Histo* histo(const std::string &name, const Index_t &index) {
      std::vector<std::string> temp;
      temp.push_back(name);
      return histo(temp,index);
    }

    /** Count the number of histograms which exist for the given name.
     * @param folder_hierarchy list of folder names which will lead to the desired histogram collection.
     * @param info will be filled with the maximum number of histograms per level (0-2) or -1 and the number of histograms at deepest level.
     */
    virtual bool numberOfHistos(const std::vector<std::string> &folder_hierarchy, HistoInfo_t &info ) =0;

    /** Return true if the histogramm exists at the given location.
     * @param folder_hierarchy list of folder names which will lead to the desired histogram collection.
     */
    virtual bool haveHisto(const std::vector<std::string> &/*folder_hierarchy*/) {return false;};


    /** Returns the requested PixLib::Histo of the histogram collection of the given name.
     * @return a pointer to a newly created histogramm or NULL.
     * The caller is responsible for deleting the hisogram.
     */
    virtual PixLib::Histo* histo(const std::vector<std::string> &name, const Index_t &index) = 0;

  };

}
#endif
