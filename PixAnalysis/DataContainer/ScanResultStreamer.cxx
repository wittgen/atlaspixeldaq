#include "ScanResultStreamer.h"
#include "IScanResultListener.h"

#include <ConfigWrapper/ConfObjUtil.h>
#include <ConfigWrapper/DbManager.h>
#include <ConfigWrapper/ConnectivityManager.h>
#include <PixController/PixScan.h>

#include "PixDbHistoAccessor.h"
#include "ConfigWrapper/Regex_t.h"
#include <TKey.h>


#include "DFThreads/DFMutex.h"

namespace PixA {

  void ScanResultStreamer::initModuleNamePattern()
  {
    if (m_modulePattern.empty()) {
      m_modulePattern.reserve(7);
      m_modulePattern.push_back(new Regex_t("L[012]_B[0-9][0-9]_S[12]_[AC][67]_M[1-6][AC]"));
      m_modulePattern.push_back(new Regex_t("L[012]_B[0-9][0-9]_S[12]_[AC][7]_M0"));
      m_modulePattern.push_back(new Regex_t("LX_B[0-9][0-9]_S[12]_[AC][67]_M[1-6][AC]"));
      m_modulePattern.push_back(new Regex_t("LX_B[0-9][0-9]_S[12]_[AC][7]_M0"));
      m_modulePattern.push_back(new Regex_t("D[123][AC]_B[0-9][0-9]_S[12]_M[1-6]"));
      m_modulePattern.push_back(new Regex_t("LI_S[0-9][0-9]_[AC]_M[1-4]_[AC][1-8]"));
      m_modulePattern.push_back(new Regex_t("LI_S[0-9][0-9]_[AC]_[13][24]_M[1-4]_[AC]([1-9]|1[0-2])"));
    }
  }


  ScanResultStreamer::ScanResultStreamer(const std::string &orig_file_name, const std::string &label_name) 
    : m_fileName(orig_file_name),
      m_rootHandle(DbManager::dbHandle(orig_file_name)),
      m_ignoreConnAndCfg(false)
  {

    // Try to check integrity of the file
    std::string::size_type pos = orig_file_name.rfind(".");
    if (pos != std::string::npos) {
      if (orig_file_name.compare(pos,orig_file_name.size()-pos,".root")==0) {
	std::unique_ptr<TFile> a_file(new TFile(orig_file_name.c_str()));
	if (!a_file->IsOpen() && !a_file->IsZombie()) {
	  std::string message("ERROR [ScanResultStreamer::ScanResultStreamer] Failed to open ROOT file : ");
	  message += orig_file_name;
	  message += ".";
	  throw ConfigException(message);
	}
	TList *a_list = a_file->GetListOfKeys();
	if (!a_list) {
	  std::string message("ERROR [ScanResultStreamer::ScanResultStreamer] ROOT file : ");
	  message += orig_file_name;
	  message += " is empty.";
	  throw ConfigException(message);
	}
	TListIter list_iter( a_list );
	TObject *obj=NULL;
	while ( (obj = list_iter.Next()) ) {
	  if (obj->InheritsFrom(TKey::Class())) {
	    TKey *a_key =static_cast<TKey *>(obj);
	    if (strcmp(a_key->GetClassName(),"TDirectory")==0 || strcmp(a_key->GetClassName(),"TDirectoryFile")==0) {
	      break;
	    }
	  }
	}
	if (!obj) {
	  std::string message("ERROR [ScanResultStreamer::ScanResultStreamer] ROOT file : ");
	  message += orig_file_name;
	  message += " does not contain subdirectories. So, it cannot contain scan data or is corrupt.";
	  throw ConfigException(message);
	}
      }
    }


    std::unique_ptr<TFile> a_file(new TFile(m_fileName.c_str()));
    std::cout << "INFO [ScanResultStreamer::ctor] Recover file " << m_fileName << "." << std::endl;
    int keys = a_file->Recover();
    if (keys==0) {
      throw PixA::ConfigException(std::string("ERROR [ScanResultStreamer::ctor] ROOT file seems ")+m_fileName+" to be corrupt");
    }
    std::cout << "INFO [ScanResultStreamer::ctor] Recovered file " << m_fileName << "." << std::endl;
    setLabel(label_name);
  }

  ScanResultStreamer::ScanResultStreamer(DbHandle &root_handle, const std::string &label_name)
    : m_rootHandle(root_handle),
      m_ignoreConnAndCfg(false)
  {
    DbRef db_ref( root_handle.ref() );
    m_fileName=db_ref.pathName();
      
    setLabel(label_name);
  }

  ScanResultStreamer::~ScanResultStreamer()
  {
    for (std::vector< Regex_t *>::iterator pattern_iter = m_modulePattern.begin();
	 pattern_iter != m_modulePattern.end();
	 pattern_iter++) {
      delete *pattern_iter;
    }
    m_modulePattern.clear();
  }

  void ScanResultStreamer::setLabel(const std::string &label_name) {
    if (label_name.empty()) {
      m_labelHandle=DbHandle();
    }
    else {
      DbRef root(m_rootHandle.ref());
      PixLib::dbRecordIterator iter = root.findRecord(label_name);
      if (iter == root.recordEnd()) {
	std::stringstream message;
	message << "ERROR [ScanResultStreamer::ScanResultStreamer] No label " << label_name << " in database.";
	throw PixLib::PixDBException(message.str());
      }
      m_labelHandle=root.access(iter);
    }
  }

  void ScanResultStreamer::readLabel(const std::string &label_name, const std::string &mname)
  {
    if (!m_listener) {
      std::cerr << "WARNING [ScanResultStreamer::read] No listener. So, won't do anything.";
      return;
    }
    setLabel(label_name);
    if (m_labelHandle) {
      if (m_first) {
	m_listener->newFile(m_fileName);
	m_first=false;
      }
      readCurrentLabel(mname);
      m_listener->finish();
    }
  }

  void ScanResultStreamer::read(const std::string &mname) {
    if (!m_listener) {
      std::cerr << "WARNING [ScanResultStreamer::read] No listener. So, won't do anything.";
      return;
    }
    try {
    if (m_labelHandle) {
      if (m_first) {
	m_listener->newFile(m_fileName);
      }
      readCurrentLabel(mname);
    }
    else {
      m_listener->newFile(m_fileName);
      m_first=false;
      DbRef root(m_rootHandle.ref());
      for (PixLib::dbRecordIterator label_iter = root.recordBegin();
	   label_iter != root.recordEnd();
	   label_iter++) {
	m_labelHandle=root.access(label_iter);
	try {
	  readCurrentLabel(mname);
	}
	catch(PixA::BaseException &err) {
	  std::string label_name("(unknwon)");
	  if (m_labelHandle) {
	    DbRef label_record(m_labelHandle.ref());
	    label_name = label_record.name();
	  }

	  std::cerr << "ERROR [ScanResultStreamer::read] Caught exception : \"" << err.getDescriptor() << "\", while processing label " << label_name << "." << std::endl ;
	}
      }
    }
    }
    catch (DFMutex::NotLockedByThread &err) {
      std::cerr << "FATAL [ScanResultStreamer::read] Caught exception : \"" << err.what() << "\", while processing file " << mname << "." << std::endl ;
    }

    m_listener->finish();
  }


  void ScanResultStreamer::readLabels() {
    if (!m_listener) {
      std::cerr << "WARNING [ScanResultStreamer::read] No listener. So, won't do anything.";
      return;
    }

    m_listener->newFile(m_fileName);
    DbRef root(m_rootHandle.ref());
    for (PixLib::dbRecordIterator label_iter = root.recordBegin();
	 label_iter != root.recordEnd();
	 label_iter++) {
      DbHandle label_handle(root.access(label_iter));
      DbRef label_record(label_handle.ref());
      if(label_record.className()=="PixScanResult") {
	ConfigHandle label_fake_config = DbManager::configHandle(label_handle);
	m_listener->newLabel(label_record.name(), label_fake_config);
      }
    }
    m_listener->finish();
  }

  class StackElement_t {
  public:
    StackElement_t(const PixA::DbHandle &folder_handle, unsigned int folder_hierarchy_index, const PixA::ConfigHandle &scan_info) 
      : m_folderHandle(folder_handle), m_hierarchyIndex(folder_hierarchy_index), m_scanInfo(scan_info) {}

    StackElement_t(const PixA::DbHandle &folder_handle, unsigned int folder_hierarchy_index) 
      : m_folderHandle(folder_handle), m_hierarchyIndex(folder_hierarchy_index) {}

    PixA::DbHandle &folder()             { return m_folderHandle;}
    const PixA::DbHandle &folder() const { return m_folderHandle;}

    unsigned int hierarchyIndex() const  { return m_hierarchyIndex;}

    PixA::ConfigHandle &scanInfo()             { return m_scanInfo;}
    const PixA::ConfigHandle &scanInfo() const { return m_scanInfo;}

  private:
    PixA::DbHandle     m_folderHandle;
    unsigned int m_hierarchyIndex;
    PixA::ConfigHandle m_scanInfo;
  };

  void ScanResultStreamer::readCurrentLabel(const std::string &mname) {

    if (!m_listener) {
      std::cerr << "WARNING [ScanResultStreamer::readCurrentLabel] No listener. So, won't do anything." << std::endl;
      return;
    }
    if (!m_labelHandle) {
      std::cerr << "ERROR [ScanResultStreamer::readCurrentLabel] No label selected." << std::endl;
      return;
    }

    DbRef label_record(m_labelHandle.ref());
    if(label_record.className()!="PixScanResult") return;

    ConfigHandle label_fake_config = DbManager::configHandle(m_labelHandle);
    m_listener->newLabel(label_record.name(), label_fake_config);

    unsigned int scan_number=0;
    PixA::ConfigRef label_config_ref(label_fake_config.ref());
    try {
      scan_number = confVal<unsigned int>(label_config_ref[""]["ScanNumber"]);
    }
    catch(...) {
    }

    std::string modNam="", grpNam="";
    PixLib::dbRecordIterator bStart = label_record.recordBegin();
    PixLib::dbRecordIterator bEnd = label_record.recordEnd();


    // if a particular module name was given ....
    if(!mname.empty()) modNam = mname;
    if(!modNam.empty()){
      grpNam = modNam;
      std::string::size_type pos = modNam.find("/");
      if(pos!=std::string::npos){
	modNam.erase(0,pos+1);
	grpNam.erase(pos, grpNam.length()-pos);
      }
      bStart = label_record.findRecord(grpNam+"/PixModuleGroup");
      if(bStart!=bEnd){
	bEnd = bStart;
	bEnd++;
      }
    }


    // loop over all or the selected rods
    for(PixLib::dbRecordIterator baseIter = bStart; baseIter!=bEnd; baseIter++){

      DbHandle rod_handle( label_record.access(baseIter) );
      DbRef rod_record( rod_handle.ref() );

      if(rod_record.className()==("PixModuleGroup")){
	std::string rod_name = rod_record.name();

	m_configList.clear();
	m_globalConfigList.clear();

	m_listener->newPixModuleGroup(rod_name);

	DbHandle scan_data_handle;
	PixA::ConfigHandle boc_config;

	{
	unsigned int record_i=0;
	try {
	for(PixLib::dbRecordIterator record_iter = rod_record.recordBegin(); 
	    record_iter != rod_record.recordEnd(); 
	    record_iter++){

	  DbHandle tmp_handle( rod_record.access(record_iter) );
	  DbRef tmp_record( tmp_handle.ref() );


	  std::string class_name( tmp_record.className() );

	  //	  std::cout << "INFO [ScanResultStreamer::readCurrentLabel]  " << rod_name << " records  : " << tmp_record.name() << " / " << class_name << std::endl;

	  if(class_name=="PixScanData" || class_name=="PixScan"){
	    scan_data_handle = tmp_handle;
	  }
	  else if(class_name=="PixModule") {
	    m_configList.insert(std::make_pair(tmp_record.name(),PixA::DbManager::configHandle(tmp_handle)));
	  }
	  else if(class_name=="PixBoc") {
	    boc_config = DbManager::configHandle(tmp_handle);
	  }
	}
	}
	catch (PixLib::PixDBException &err) {
	  std::cerr << "ERROR [ScanResultStreamer::readCurrentLabel] Failed to process record " << record_i << " of " << rod_record.name() << " : " << std::endl
		    << err.getDescriptor() << std::endl;
	}
	}

	// new pix scan
	if(scan_data_handle){
	  DbRef scan_data(scan_data_handle.ref());
	  /* debug */ PixLib::dbRecordIterator debug_boc_config_record_iter = rod_record.findRecord("OpticalBoc");
	  if (!boc_config) {
	    // then search for global boc config
	    DbRef root_record(m_rootHandle.ref());
	    PixLib::dbRecordIterator global_rod_record_iter = root_record.findRecord(rod_name);
	    if (global_rod_record_iter != root_record.recordEnd()) {
	      DbHandle global_rod_handle(root_record.access(global_rod_record_iter));
	      DbRef global_rod_record(global_rod_handle.ref());
	      PixLib::dbRecordIterator boc_config_record_iter = global_rod_record.findRecord("OpticalBoc/PixBoc");
	      if (boc_config_record_iter != global_rod_record.recordEnd()) {
		boc_config = DbManager::configHandle(global_rod_record.access(boc_config_record_iter));
	      }
	    }
	  }

	  std::vector< StackElement_t > folder_stack;

	  // initial folder is the module group folder
	  std::vector<std::string> folder_hierarchy;
	  folder_stack.push_back( StackElement_t(scan_data_handle, folder_hierarchy.size()) );
	  while (!folder_stack.empty()) {
	    // processing folders
	    DbHandle folder_handle = folder_stack.back().folder();
	    unsigned hierarchy_index = folder_stack.back().hierarchyIndex();
	    ConfigHandle scan_info_1 = folder_stack.back().scanInfo();

	    std::vector<std::string>::iterator pos=folder_hierarchy.begin();
	    for (unsigned int i=0; i< hierarchy_index; i++) {
	      assert(pos != folder_hierarchy.end() );
	      pos++;
	    }
	    folder_hierarchy.erase(pos,folder_hierarchy.end());
	    DbRef folder(folder_handle.ref());
	    folder_stack.pop_back();

	    //	    std::cout << "INFO [ScanResultStreamer::readCurrentLabel] folder = " << folder.name() << std::endl;

	    if (folder_handle == scan_data_handle) {
	      //	      m_listener->newLabel(folder.name());
	    }
	    else {
	      folder_hierarchy.push_back(folder.name());
	      m_listener->newFolder(folder_hierarchy);
	    }

	    //	    std::cout << "INFO [ScanResultStreamer::readCurrentLabel] processing " << folder.pathName() << " class = " << folder.className()
	    //		      << std::endl;

	    std::vector< DbHandle > pix_scan_histo_list;
	    std::vector< DbHandle > histo_list;

	    // get a scan info if existing
	    PixLib::dbRecordIterator scan_info_iter_1 = scan_data.findRecord("ScanInfo");
	    if (scan_info_iter_1 != scan_data.recordEnd()) {
	      try {
		DbHandle scan_info_handle ( scan_data.access(scan_info_iter_1) );
		scan_info_1 = DbManager::configHandle(scan_info_handle);
		m_listener->newScanInfo(scan_info_1);
	      }
	      catch(...) {
	      }
	    }


	    //	    std::cout << "INFO [ScanResultStreamer::readCurrentLabel] folder " << folder.name()
	    //		      << " contains " <<  " : "
	    //		      << std::endl;
	    folder = folder_handle.ref();
	    // analyse sub records
	    unsigned int record_i=0;
	    try {
	    for(PixLib::dbRecordIterator rec_iter = folder.recordBegin(); 
		rec_iter != folder.recordEnd();
		rec_iter++,record_i++) {
	      DbHandle a_record_handle(folder.access(rec_iter));
	      DbRef a_record(a_record_handle.ref());
	      //	      std::cout << "INFO [ScanResultStreamer::readCurrentLabel]  "
	      //			<< " processing record  = " << a_record.name()
	      //			<< " / class = " << a_record.className()
	      //			<< std::endl;
	      std::string class_name( a_record.className() );


	      if(class_name=="PixModule") {
		// a module config
		//		std::cout << "INFO [ScanResultStreamer::readCurrentLabel]  "
		//			  << " add local module config " << a_record.name()
		//			  << std::endl;

		m_configList.insert(std::make_pair(a_record.name(),PixA::DbManager::configHandle(a_record_handle)));
	      }
	      //	      else if(a_record->getClassName()=="PixScanHisto") {
	      //		std::vector<std::string>::const_iterator hist_type_name_iter=find(a_record->getClassName(), s_histoTypeList.begin(),s_histoTypeList.end());
	      //		if (hist_type_name_iter != s_histoTypeList.end()) {
	      //		  pix_scan_histo_list.push_back(a_record.release());
	      //		}
	      //		else {
	      //		  histo_list.push_back(a_record.release());
	      //		}
	      //	      }
	      else if(class_name=="Histo") {
		// a custom histogram
		if (!a_record.empty()) {
		histo_list.push_back(a_record_handle);
	      }
	      }
	      else if(class_name=="PixScan_Histo") {
		    // a custom pix scan histogram
		if (!a_record.empty()) {
		  pix_scan_histo_list.push_back(a_record_handle);
		}
	      }
	      else if(class_name=="PixScan_Info") {
		// do nothing
	      }
	      else {
		if (s_histoTypeList.empty()) initHistoTypes();
		std::set<std::string>::const_iterator hist_type_name_iter=s_histoTypeList.find(class_name);
		if (hist_type_name_iter != s_histoTypeList.end()) {
		  // standard pix scan histo new and old fashioned
		  if (!a_record.empty()) {
		  pix_scan_histo_list.push_back(a_record_handle);
		  }
		}
		else {
		  if(class_name=="PixScanHisto") {
		    if (!a_record.empty()) {
		    histo_list.push_back(a_record_handle);
		  }
		  }
		  else {
		    // a folder
		    //		    std::cout << "INFO [ScanResultStreamer::readCurrentLabel]  "
		    //			      << " add folder " << a_record.name() << " to stack."
		    //			      << std::endl;
		    
		    folder_stack.push_back( StackElement_t( a_record_handle, folder_hierarchy.size(), scan_info_1 ) );
		  }
		}
	      }

	    }
	    }
	    catch (PixLib::PixDBException &err) {
	      std::cerr << "ERROR [ScanResultStreamer::readCurrentLabel] Failed to process record " << record_i << " of " << folder.name() << " : " << std::endl
			<< err.getDescriptor() << std::endl;
	    }
	    if (!pix_scan_histo_list.empty()) {

	      m_conn.reset();

	      if (!m_ignoreConnAndCfg) {
		// Can we get a connectivity ?
		PixLib::dbRecordIterator scan_info_iter_2 = scan_data.findRecord("ScanInfo");
		if (scan_info_iter_2 != scan_data.recordEnd()) {
		  try {
		    DbHandle scan_info_handle ( scan_data.access(scan_info_iter_2) );
		    ConfigHandle scan_info_2( DbManager::configHandle(scan_info_handle) );
		    m_conn = connectivity(scan_info_2);

		  }
		  catch(...) {
		  }

		}
	      }

	      // No local module config folder ? 
	      if (!m_ignoreConnAndCfg) {

		if (m_configList.empty() && m_globalConfigList.empty()) {

		  // Then search for global module config folder
		  if (m_globalConfigList.empty()) {
		    if (!createGlobaleConfigList(m_rootHandle, rod_name, mname)) {
		      std::cout << "INFO [ScanResultStreamer::read] No local or global module configuration. Try to get configurations from the configuration db." << std::endl;
		      // or try connectivity ?
		      if (!createConfigListFromConnectivity(rod_name)) {
			m_listener->reportProblem("No configurations in file, no connectivity.");

			throw PixA::BaseException("ERROR [ScanResultStreamer::read] Did not find local or global module configurations."
						  "Also did not get module configuration from connectivity database." );
		      }
		    }
		  }

		}
	      }

	      // // exit if no config
	      // //	      if (!config_handle_list.empty() || !global_config_handle_list.empty()) {

	      // now process pix scan histos

	      // get list of modules with histograms
	      std::set<std::string> module_names;
	      for ( std::vector< DbHandle >::iterator  histo_iter = pix_scan_histo_list.begin();
		    histo_iter != pix_scan_histo_list.end();
		    histo_iter++) {

		DbRef histo_record(histo_iter->ref());

		//		std::cout << "INFO [ScanResultStreamer::readCurrentLabel]  "
		//			  << " process histos " << histo_record.name() << "."
		//			  << std::endl;

		for (PixLib::dbRecordIterator module_iter = histo_record.recordBegin();
		     module_iter != histo_record.recordEnd();
		     module_iter++) {
		  DbHandle module_handle(histo_record.access(module_iter));
		  DbRef module_record(module_handle.ref());

		  std::string module_name = module_record.name();
		  if (module_record.className() == "PixScanHisto" || (module_name.size() == 4 && module_name.compare(0,3,"Mod"))) {
		    //		    std::cout << "INFO [ScanResultStreamer::readCurrentLabel] add module " << module_name << std::endl;
		    module_names.insert(module_name);
		  }
		}
	      }

	      PixA::ConfigHandle  pix_scan_config;
	      if (folder.fieldBegin() != folder.fieldEnd() ) {
		pix_scan_config = PixA::DbManager::configHandle(folder_handle);
		if (pix_scan_config) {
		  m_listener->newPixScan(pix_scan_config);
		}
	      }

	      if (m_ignoreConnAndCfg) {
		processModulesFromModuleList(module_names, folder_handle, pix_scan_histo_list, pix_scan_config, boc_config, scan_number);
	      }
	      else {
		if (m_conn) {
		  processModulesFromConnectivity(module_names, folder_handle, pix_scan_histo_list, pix_scan_config, boc_config,rod_name, scan_number);
		}
		else {
		  processModulesFromConfigList(module_names, folder_handle, pix_scan_histo_list, pix_scan_config, boc_config, scan_number);
		}
	      }
	    }

	    if (!histo_list.empty()) {
	      PixA::HistoHandle histo_handle( PixDbHistoAccessor::histoHandle( folder_handle ) );

	      for ( std::vector< DbHandle >::iterator  histo_iter = histo_list.begin();
		    histo_iter != histo_list.end();
		    histo_iter++) {
		DbRef histo_record(histo_iter->ref());
		std::string histo_name = histo_record.name();

		m_listener->newHisto( histo_name, histo_handle, rod_name, scan_info_1);
	      }
	    }

	  }
	}

	m_listener->finishPixModuleGroup();

      }
    }
  }

  bool ScanResultStreamer::createGlobaleConfigList(DbHandle &root_handle, const std::string &rod_name, const std::string &module_name_arg) {
    //    std::cout << "INFO [ScanResultStreamer::createGlobaleConfigList] search for global module config for ROD " << rod_name << "."
    //    	      << std::endl;

    DbRef root_record(root_handle.ref());
    PixLib::dbRecordIterator ri = root_record.findRecord(rod_name+"/PixModules");
    if (ri != root_record.recordEnd()) {
      DbHandle global_mod_config_handle( root_record.access(ri) );
      DbRef global_mod_config(global_mod_config_handle.ref());

      PixLib::dbRecordIterator mod_end = global_mod_config.recordEnd();
      PixLib::dbRecordIterator mod_start = mod_end;
      if (!module_name_arg.empty()){
	// only consider the module of the given name
	mod_start = global_mod_config.findRecord(module_name_arg+"/PixModule");
	if (mod_start!=mod_end) {
	  mod_end = mod_start;
	  mod_end++;
	}
      }
      else {
	mod_start = global_mod_config.recordBegin();
      }

      for (PixLib::dbRecordIterator mod_conf_iter = mod_start;
	   mod_conf_iter!= mod_end;
	   mod_conf_iter++) {
	DbHandle a_mod_conf_handle(global_mod_config.access( mod_conf_iter) );
	DbRef a_mod_conf(a_mod_conf_handle.ref());
	std::string module_name = a_mod_conf.name();
	//	std::cout << "INFO [ScanResultStreamer::createGlobaleConfigList]  "
	//		  << " add global module config " << module_name
	//		  << std::endl;
	m_globalConfigList.insert(std::make_pair(module_name, PixA::DbManager::configHandle(a_mod_conf_handle) ));
      }
    }

    return !m_globalConfigList.empty();
  }

  bool ScanResultStreamer::createConfigListFromConnectivity(const std::string &rod_name) {
    try {
      if (m_conn) {
	//	std::cout << "INFO [ScanResultStreamer::createConfigListFromConnectivity] Try connectivity to get module configurations." << std::endl;
	//conn will wrap a pointer to IConnectivity
	// IConnectivity will contain a PixConnectivity and will cache config objects.
	// The connectivity will be a shared property. If there are no more users, the 
	// connectivity will be deleted eventually.
	const PixA::Pp0List &pp0s=m_conn.pp0s(rod_name);
	// read only
	for (PixA::Pp0List::const_iterator pp0_iter=pp0s.begin();
	     pp0_iter != pp0s.end(); // may be slightly inefficient unless the compiler is smart enough.
	     ++pp0_iter) {

	  // it may be more efficient to call modulesBegin and modulesEnd only once:
	  PixA::ModuleList::const_iterator modules_begin=PixA::modulesBegin(pp0_iter);
	  PixA::ModuleList::const_iterator modules_end=PixA::modulesEnd(pp0_iter);
	  for (PixA::ModuleList::const_iterator module_iter=modules_begin;
	       module_iter != modules_end;
	       ++module_iter) {
	    m_globalConfigList.insert(std::make_pair(const_cast<PixLib::ModuleConnectivity*>(*module_iter)->prodId(),
						     m_conn.modConf(module_iter)) );

	  }
	}
      }
    }
    catch(...) {
    }
    return !m_globalConfigList.empty();
  }

  bool ScanResultStreamer::processModulesFromConnectivity(const std::set<std::string> &module_names,
							  const DbHandle &histo_folder_handle,
							  const std::vector< DbHandle > &pix_scan_histo_list,
							  const PixA::ConfigHandle &pix_scan_config,
							  const PixA::ConfigHandle &boc_config,
							  const std::string &rod_name,
							  unsigned int scan_number)
  {
    assert (m_conn);

    // if there is a scan info section then use connectivity information

    //conn will wrap a pointer to IConnectivity
    // IConnectivity will contain a PixConnectivity and will cache config objects.
    // The connectivity will be a shared property. If there are no more users, the 
    // connectivity will be deleted eventually.
    const PixA::Pp0List &pp0s=m_conn.pp0s(rod_name);
    // read only
    for (PixA::Pp0List::const_iterator pp0_iter=pp0s.begin();
	 pp0_iter != pp0s.end(); // may be slightly inefficient unless the compiler is smart enough.
	 ++pp0_iter) {
      m_listener->newPp0( const_cast<PixLib::Pp0Connectivity*>(*pp0_iter)->name());
	// it may be more efficient to call modulesBegin and modulesEnd only once:
      PixA::ModuleList::const_iterator modules_begin=PixA::modulesBegin(pp0_iter);
      PixA::ModuleList::const_iterator modules_end=PixA::modulesEnd(pp0_iter);
      for (PixA::ModuleList::const_iterator module_iter=modules_begin;
	   module_iter != modules_end;
	   ++module_iter) {

	// first try connectivity name 
	std::set<std::string>::const_iterator a_mod = module_names.find(const_cast<PixLib::ModuleConnectivity *>(*module_iter)->name());
	if (a_mod == module_names.end()) { 
	  //  then the old name
	  // If there are connectivity tags then the new pix scan format should be in use which names the modules by the full connectiivty name
	  std::stringstream a_mod_name;
	  a_mod_name << "Mod" << (*module_iter)->modId;
	  a_mod = module_names.find(a_mod_name.str());
	  std::cerr << "INFO [ScanResultStreamer::processModulesFromConnectivity] Try old name  " << a_mod_name.str() << std::endl;
	}
	if (a_mod != module_names.end()) { 

	  try {
	  ScanConfigRef scan_config(ScanConfigRef::makeScanConfig(const_cast<PixLib::ModuleConnectivity *>(*module_iter)->prodId(),
								  const_cast<PixLib::ModuleConnectivity *>(*module_iter)->name(), 
								  modConf(const_cast<PixLib::ModuleConnectivity *>(*module_iter)->prodId()), 
								  pix_scan_config, 
								  (boc_config ? boc_config : m_conn.bocConf(pp0_iter) ), 
								  scan_number,
								  m_conn,
								  module_iter));

	  PixA::HistoHandle histo_handle( PixDbHistoAccessor::histoHandle( histo_folder_handle ) );

	  for (std::vector< DbHandle >::const_iterator hist_iter = pix_scan_histo_list.begin();
		 hist_iter != pix_scan_histo_list.end();
		 hist_iter++) {
	    //PixA::HistoHandle histo_handle( PixDbHistoAccessor::histoHandle( *hist_iter ) );
	      DbRef histo_record(hist_iter->ref());
	      std::string histo_name = histo_record.name();
	      m_listener->newPixScanHisto( histo_name, 
					   scan_config, 
					   histo_handle);
	  }
	  }
	  catch (PixA::ConfigException &err) {
	    std::cout << err << std::endl;
	  }
	}
	else {
	  std::cerr << "INFO [ScanResultStreamer::processModulesFromConnectivity] Nothing for module " 
		    << const_cast<PixLib::ModuleConnectivity *>(*module_iter)->name() 
		    << "(" << const_cast<PixLib::ModuleConnectivity *>(*module_iter)->prodId() << ")" << std::endl;
	}
      }
      m_listener->finishPp0();

    }
    return true;
  }

  bool ScanResultStreamer::processModulesFromConfigList(const std::set<std::string> &module_names,
							const DbHandle &histo_folder_handle,
							const std::vector< DbHandle > &pix_scan_histo_list,
							const PixA::ConfigHandle &pix_scan_config,
							const PixA::ConfigHandle &boc_config,
							unsigned int scan_number)
  {
    const std::map<std::string, PixA::ConfigHandle> *config_list=(!m_configList.empty() ? &m_configList : &m_globalConfigList);
    if (config_list->empty()) return false;
    // if there is a scan info section then use connectivity information

    // dirty trick to keep the RootDb file open 
    ConfigRef first_mod_conf( config_list->begin()->second.ref() );

    std::multimap<int, std::pair< std::string, std::map<std::string, PixA::ConfigHandle>::const_iterator > > ordered_module_list;

    // order modules wrt. to stave id
    for (std::map<std::string, PixA::ConfigHandle>::const_iterator module_conf_iter = config_list->begin();
	 module_conf_iter != config_list->end();
	 module_conf_iter++) {
      ConfigRef mod_conf ( module_conf_iter->second.ref() );
      int ass_SN=-1;
      std::string mod_conn_name;
      try {
	std::string ass_type = confVal<std::string>(mod_conf["geometry"]["Type"]);
	if (ass_type != "unknown") {
	  ass_SN=confVal<int>(mod_conf["geometry"]["staveID"]);
	}
	mod_conn_name = confVal<std::string>(mod_conf["geometry"]["connName"]);
      }
      catch(...) {
      }
      ordered_module_list.insert(std::make_pair(ass_SN,std::make_pair(mod_conn_name,module_conf_iter)));
    }

    // now process modules in increasing stave id order.
    int last_stave_id = -1;
    for ( std::multimap<int, std::pair< std::string, std::map<std::string, PixA::ConfigHandle>::const_iterator > >::const_iterator
	    ordered_iter = ordered_module_list.begin();
	  ordered_iter != ordered_module_list.end();
	  ordered_iter++) {

      if (ordered_iter->first>0 && ordered_iter->first != last_stave_id) {
	// stave id is not a Pp0, but we do not have anything better
	std::stringstream stave_label;
	stave_label << ordered_iter->first;
	m_listener->newPp0(stave_label.str());
	last_stave_id = ordered_iter->first;
      }

      const std::map<std::string, PixA::ConfigHandle>::const_iterator &mod_conf_iter = ordered_iter->second.second;
      const std::string &mod_conn_name = ordered_iter->second.first;

      ScanConfigRef scan_config(ScanConfigRef::makeScanConfig( mod_conf_iter->first , mod_conn_name, mod_conf_iter->second, pix_scan_config, boc_config, scan_number ));

      PixA::HistoHandle histo_handle( PixDbHistoAccessor::histoHandle( histo_folder_handle ) );
      for (std::vector< DbHandle >::const_iterator hist_iter = pix_scan_histo_list.begin();
	   hist_iter != pix_scan_histo_list.end();
	   hist_iter++) {
	// PixA::HistoHandle histo_handle( PixDbHistoAccessor::histoHandle( *hist_iter ) );
	DbRef histo_record(hist_iter->ref());
	std::string histo_name = histo_record.name();

	m_listener->newPixScanHisto( histo_name,  scan_config, histo_handle);
      }
      m_listener->finishPp0();
    }

    return true;
  }

  bool ScanResultStreamer::processModulesFromModuleList(const std::set<std::string> &module_names,
							const DbHandle &histo_folder_handle,
							const std::vector< DbHandle > &pix_scan_histo_list,
							const PixA::ConfigHandle &pix_scan_config,
							const PixA::ConfigHandle &boc_config,
							unsigned int scan_number)
  {
    unsigned int n_errors=0;
    unsigned int n_modules=0;

    // Since the module names are sorted in the set,
    // The modules of each pp0 are contained in one subset.
    std::string pp0_name;
    for (std::set<std::string>::const_iterator module_iter = module_names.begin();
	 module_iter != module_names.end();
	 module_iter++) {

      bool is_module = false;
      for (std::vector< Regex_t *>::const_iterator pattern_iter = m_modulePattern.begin();
	   pattern_iter != m_modulePattern.end();
	   pattern_iter++) {
	if ((*pattern_iter)->match( *module_iter )) {
	  is_module=true;
	  break ;
	}
      }

      if (is_module) {
	bool isIBLDBM = (module_iter->substr(0,2).compare("LI") == 0) ? true : false;
	std::string a_pp0_name = (isIBLDBM) ? module_iter->substr(0,8) : module_iter->substr(0,module_iter->rfind("_M"));
 	if (a_pp0_name != pp0_name) {
	  if (!pp0_name.empty()) {
	    m_listener->finishPp0();
	  }
	  pp0_name = a_pp0_name;
	  m_listener->newPp0(pp0_name);
	}

	ScanConfigRef scan_config(ScanConfigRef::makeScanConfig( *module_iter , *module_iter, PixA::ConfigHandle(), pix_scan_config, boc_config, scan_number ));

	for ( std::vector< DbHandle >::const_iterator  histo_iter = pix_scan_histo_list.begin();
	      histo_iter != pix_scan_histo_list.end();
	      histo_iter++) {

	  PixA::HistoHandle histo_handle( PixDbHistoAccessor::histoHandle( histo_folder_handle ) );
	  DbRef histo_record(histo_iter->ref());
	  histo_record.recordBegin();
	  if (histo_record.findRecord(*module_iter) != histo_record.recordEnd()) {
	    std::string histo_name = histo_record.name();
	    n_modules++;
	    m_listener->newPixScanHisto( histo_name,  scan_config, histo_handle);
	  }
	}
      }
      else {
	n_errors++;
	std::cout << "WARNING [ScanResultStreamer::read] Name of folder with pix scan histos does not seem to be a module name : " 
		  << *module_iter << "."
		  << std::endl;
      }
    }
    return n_modules>0 || n_errors==0;
  }


  std::set<std::string> ScanResultStreamer::s_histoTypeList;

  void ScanResultStreamer::initHistoTypes() {
    if (s_histoTypeList.empty()) {
      std::unique_ptr<PixLib::PixScan> ps(new PixLib::PixScan);
      std::map<std::string, int> types = ps->getHistoTypes();
      for (std::map<std::string, int>::const_iterator type_iter = types.begin();
	   type_iter != types.end();
	   type_iter++) {
	s_histoTypeList.insert(type_iter->first);
      }
    }
  }

  PixA::ConfigHandle ScanResultStreamer::modConf(const std::string &module_prod_name) {
    PixA::ConfigHandle handle;
    if (!m_configList.empty()) {
      std::map<std::string, PixA::ConfigHandle>::iterator module_conf_iter = m_configList.find(module_prod_name);
      if (module_conf_iter != m_configList.end()) {
	handle = module_conf_iter->second;
      }
    }
    else if (!m_globalConfigList.empty()) {
      std::map<std::string, PixA::ConfigHandle>::iterator module_conf_iter = m_globalConfigList.find(module_prod_name);
      if (module_conf_iter != m_globalConfigList.end()) {
	handle = module_conf_iter->second;
      }
    }
    else if (m_conn) {
      try {
          std::cout<<"e se fossi io"<<std::endl;
	handle = m_conn.modConf(module_prod_name);
      }
      catch(PixA::ConfigException &) {
      }
    }

    if (!handle) {
      std::stringstream message;
      message << "ERROR [ScanResultStreamer::modConf] No module configuration for module " << module_prod_name << ".";
      throw PixA::ConfigException(message.str());
    }
    return handle;
  }

  PixA::ConnectivityRef ScanResultStreamer::connectivity(const ConfigHandle &scan_info_handle)
  {
    PixA::ConnectivityRef m_conn;
    if (scan_info_handle) {
      try {
	PixA::ConfigRef  scan_info( scan_info_handle.ref() );

	PixA::ConfGrpRef tags(scan_info["Tags"]);
	std::string tag_conn  = confVal<std::string>(tags["Conn"]);
	std::string tag_alias = confVal<std::string>(tags["Alias"]);
	std::string tag_data  = confVal<std::string>(tags["Data"]);
	std::string tag_cfg   = confVal<std::string>(tags["Cfg"]);
	unsigned int revision;
	try {
	  revision = confVal<unsigned int>(tags["CfgRev"]);
	}
	catch (PixA::ConfigTypeMismatch &) {
	  revision = static_cast< unsigned int>( confVal<int>(tags["CfgRev"]) );
	}
	std::string tag_id = "";
	try {
	  tag_id    = confVal<std::string>(tags["ObjID"]);
	  m_conn = PixA::ConnectivityManager::getConnectivity( tag_id, tag_conn, tag_alias, tag_data,tag_cfg, revision);
	}
	catch(...) {
	  m_conn = PixA::ConnectivityManager::getConnectivity( tag_conn, tag_alias, tag_data,tag_cfg, revision);
	}
	std::cout << " ID = " << tag_id 
		  << " conn  = " << tag_conn 
		  << " alias = " << tag_alias
		  << " data  = " << tag_data
		  << " cfg   = " << tag_cfg
		  << " cfg Rev.  = " << revision
		  << std::endl;
      }
      catch(...) {
      }
    }
    return m_conn;
  }
}
