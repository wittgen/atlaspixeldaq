#ifndef _NopScanResultListener_h_
#define _NopScanResultListener_h_

#include <IScanResultListener.h>

namespace PixA {

  /** ScanResultListener which implements all the member virtual functions but does not do anything.
   * Useful for derived listeners which only want to implement a very small subset.
   */
  class NopScanResultListener : public PixA::IScanResultListener
  {

  public:

    void newFile( const std::string &/*name*/) {}

    /** Does  intentionally nothing.
     */
    void newLabel(const std::string &, const PixA::ConfigHandle &) {}

    /** Does intentionally nothing.
     */
    void newFolder(const std::vector<std::string> &) {}

    /** Does intentionally nothing.
     */
    void newScanInfo(const ConfigHandle &) {}

    /** Does intentionally nothing.
     */
    void newPixScan(const ConfigHandle &) {}


    /** Does intentionally nothing.
     */
    void newPixScanHisto(const std::string   &,
			 const PixA::ScanConfigRef &,
			 const PixA::HistoHandle   &) {}

    /** Does intentionally nothing.
     */
    void newHisto(const std::string &, const PixA::HistoHandle &, const std::string &, const ConfigHandle &) {}

    /** Does intentionally nothing.
     */
    void newPixModuleGroup(const std::string &) {}
  
    /** Will be called after the processing of a pix module group.
     * Does intentionally nothing.
     */
    void finishPixModuleGroup() {}


    /** Does intentionally nothing.
     */
    void newPp0(const std::string &) {}


    /** Called after the last module of a PP0 has been processed.
     */
    void finishPp0() {}


    /** Called at the end.
     */
    void finish() {}

    /** Called in case problems occure.
     */
    void reportProblem(const std::string &) {}

  };

}
#endif
