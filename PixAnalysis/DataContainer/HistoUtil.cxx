#include "HistoUtil.h"
#include <TH1.h>
#include <TH2.h>
#include <TGraph.h>
#include <Histo/Histo.h>
#include <sstream>
#include <set>

#include "GenericHistogramAccess.h"
#include "Average_t.h"
#include <ConfigWrapper/ConfigRef.h>
#include <ConfigWrapper/ConfObjUtil.h>

#include "HistoAxisInfo_t.h"
#include "HistoExtraInfo_t.h"
#include "HistoExtraInfoList.h"

using namespace PixLib;
namespace PixA {

   const int PixelCoord_t::s_nPix;
   const int PixelCoord_t::s_nSpecialPix;
  template <>
  TObject *transform<TH1>(const TH1 *h) {
    if (!h) return NULL;
    return h->Clone();
  }

  template <>
  inline TObject *transform<TH2>(const TH2 *h) {
    return transform<TH1>(h);
  }

  template <>
  TObject *transform<PixLib::Histo>(const PixLib::Histo *h)
  {

    if (!h) return NULL;

    //    gROOT->cd(); // make sure histo is created in memory, not file
    if(h->nDim()==1){

      TH1 *h1 = new TH1F(h->name().c_str(),h->title().c_str(),
			 h->nBin(0),h->min(0), h->max(0));
      h1->SetDirectory(0);
      int i;
      for (i=0; i<h->nBin(0); i++) {
	h1->SetBinContent(i+1,static_cast<float>( (*h)(i) ) );
      }
      return h1;

    }
    else if (h->nDim()==2){

      TH2 *h2 = new TH2F(h->name().c_str(),h->title().c_str(),
			 h->nBin(0),h->min(0), h->max(0),h->nBin(1),h->min(1), h->max(1));
      h2->SetDirectory(0);

      int i,j;
      float a_min=FLT_MAX;
      float a_max=-FLT_MAX;
      for (i=0; i<h->nBin(0); i++) {
	for (j=0; j<h->nBin(1); j++) {
	  h2->SetBinContent(i+1,j+1,static_cast<float>( (*h)(i,j) ) );
	  if ((*h)(i,j) < a_min ) a_min = (*h)(i,j);
	  if ((*h)(i,j) > a_max ) a_max = (*h)(i,j);
	}
      }
      if (a_min==a_max) {
	if (a_min==0.) {
	  h2->SetMinimum(-1.);
	  h2->SetMaximum(1.);
	}
	else {
	  h2->SetMinimum(a_min*(1-1e-3));
	  h2->SetMaximum(a_min*(1+1e-3));
	}
      }
      return h2;

    }
    else {
      return NULL;
    }
  }

  template <class T_Histo_dest_, class T_Histo_src_>
  T_Histo_dest_* chipMap(const T_Histo_src_ *h2, int chip)
  {
    if (!h2 || !is2D(h2)) return NULL;

    if(chip<0) // full module, we're done
      return static_cast<TH2 *>(transform(h2));

    PixGeometry geo(nRows(h2),nColumns(h2));
    assert(geo.pixType()!=PixGeometry::INVALID_MODULE); 

    std::stringstream hname;
    std::stringstream htitle;

    hname << histoName(h2) << chip;
    htitle << histoTitle(h2) << " chip " << chip;
    int nRow=geo.nChipRow();
    int nCol=geo.nChipCol();

    T_Histo_dest_ *dest_h2= createHisto<T_Histo_dest_>(hname.str().c_str(),htitle.str().c_str(),
						       nCol,-0.5,nCol-0.5,
						       nRow,-0.5,nRow-0.5);

    float a_min=FLT_MAX;
    float a_max=-FLT_MAX;
    for(int col_i=0; col_i<nCol; col_i++){
      for(int row_i=0; row_i<nRow; row_i++){

	PixCoord pixel=geo.coord(chip,col_i,row_i);
	float value  = binContent(h2,startColumn(h2)+pixel.x(), startRow(h2)+pixel.y());
	setBinContent(dest_h2,startColumn(dest_h2)+col_i, startColumn(dest_h2)+row_i,value);
	if (value < a_min ) a_min = value;
	if (value > a_max ) a_max = value;

	//	PixelCoord_t pixel_dest(0,col_i,row_i);
	//setBinContent(dest_h2,startColumn(dest_h2)+pixel_dest.x(), startRow(dest_h2)+pixel_dest.y(),value);
	//        retmap->Fill(i,j,(float) static_cast<float>( (*h)(pixel.x(),pixel.y()) ));
      }
    }
    if (a_min==a_max) {
      if (a_min==0.) {
	setMinMax(dest_h2,static_cast<float>(-1.),static_cast<float>(1.));
      }
      else {
	setMinMax(dest_h2,static_cast<float>(a_min*(1-1e-3)),static_cast<float>(a_min*(1+1e-3)));
      }
    }
    
    return dest_h2;
  }


  template <class T_Histo_, class T_Reference_>
  void fillHistoFromMap(TH1 *dest,
			TH1 *dest_non_scanned,
			const T_Histo_ *src,
			int chip,
			PixelTypeMask_t pixel_types,
			T_Reference_ reference,
			unsigned int mask_steps,
			unsigned int total_mask_steps,
			unsigned int start_mask_step)
  {
    if (!dest) return;
    if (!dest_non_scanned) dest_non_scanned=dest;
    if (!src || !is2D(src)) return;


    unsigned int start_chip;
    unsigned int end_chip;
    PixGeometry geo(nRows(src),nColumns(src)); 

    if(geo.pixType()==PixGeometry::INVALID_MODULE) return; 

    if (  geo.pixType()==PixGeometry::FEI2_CHIP || geo.pixType()==PixGeometry::FEI4_CHIP) {
      start_chip = 0;
      end_chip = 1;
    }
    else { // should be  geo.pixType()==PixGeometry::FEI2_MODULE || geo.pixType()==PixGeometry::FEI4_MODULE
      if (chip>=0) {
	start_chip = chip;
	end_chip   = chip+1;
      }
      else {
	start_chip = 0;
	end_chip   = geo.nChip();
      }
    }

    if(total_mask_steps==0 || mask_steps ==0){
      total_mask_steps=1;
      mask_steps =1;
    }
      unsigned int n_parts = geo.nChipPixel()/total_mask_steps;
      
//       std::cout << "INFO [fillHistoFromMap] chips: " << start_chip << " - " << end_chip
// 	        << " with " << nRows(src) << "x" << nColumns(src) << " pixels;"
// 		<< "pixel type = " << pixel_types << ", parts = "  << n_parts
// 		<< " stages = " << start_mask_step << " - " << start_mask_step + mask_steps << " / " << total_mask_steps 
// 		<<"; chip geo.: " << geo.nChip() << ", " << geo.nChipCol() << ", " << geo.nChipRow()
// 		<< std::endl;
	for (unsigned int chip_i=start_chip; chip_i<end_chip; chip_i++) {
	  for (unsigned int part_i=0; part_i< n_parts; part_i++) {
	    
	    unsigned int scan_start_mask_step_part= start_mask_step + part_i*total_mask_steps;
	    unsigned int scan_end_mask_step_part  = scan_start_mask_step_part + mask_steps;
	    
	    unsigned int start_mask_step_part = part_i*total_mask_steps;
	    unsigned int end_mask_step_part   = start_mask_step_part + total_mask_steps;
	    
	    for (unsigned int mask_step_i=start_mask_step_part;
		 mask_step_i<end_mask_step_part;
		 mask_step_i++) {
	      unsigned int col_i = mask_step_i / geo.nChipRow(); 
	      unsigned int row_i = mask_step_i % geo.nChipRow(); 
	      
	      if (col_i%2==1) {
		//row_i=nRows(src) - 1 - row_i;
		row_i=geo.nChipRow() - 1 - row_i;
	      }
	      if (pixel_types & PixelTypeMask_t::type(col_i,row_i, (geo.pixType()==PixGeometry::FEI4_CHIP||geo.pixType()==PixGeometry::FEI4_MODULE))) {
		PixCoord pixel = geo.coord(chip_i,col_i,row_i);
		float value = binContent(src,startColumn(src)+pixel.x(),startRow(src)+pixel.y());
		if (isnan(value)) {
		  std::cout << "ERROR [fillHistoFromMap] Not a number for bin " << pixel.x() << ", " << pixel.y() << " of "
			    << histoName(src)
			    << std::endl;
		}
		else {
		  if (mask_step_i<scan_start_mask_step_part || mask_step_i>=scan_end_mask_step_part) {
		    if (reference.isSet(value)) {
		      dest_non_scanned->Fill(reference.transformOff(value));
		    }
		  }
		  else {
		    //if(col_i == 40 && (row_i>10 && row_i<30)) dest->Fill(reference.transformOn(value));
		    dest->Fill(reference.transformOn(value));
		  }
		}
	      }
	    }
	  }
	}
  }
  
  template <class T_Histo_, class T_Reference_>
  HistoBinning guessHistoBinningForMapProjection(const T_Histo_ *src,
						 int chip,
						 PixelTypeMask_t pixel_types,
						 T_Reference_ reference,
						 unsigned int mask_steps,
						 unsigned int total_mask_steps,
						 unsigned int start_mask_step)
  {
    if (!src || !is2D(src)) return HistoBinning(0,0,0);

    unsigned int start_chip;
    unsigned int end_chip;
    PixGeometry geo(nRows(src),nColumns(src));
    if(geo.pixType()==PixGeometry::INVALID_MODULE){
      std::cout << "ERROR [guessHistoBinningForMapProjection] Expected histogramm for one module or one front-end, got "<<
	nRows(src)<<"x"<<nColumns(src)<<" pixels." << std::endl;
      return HistoBinning(0,0,0);
    }

    int g_nChip = geo.nChip();
    int g_nPix  = geo.nChipPixel();

    if (  geo.pixType()==PixGeometry::FEI2_CHIP ||  geo.pixType()==PixGeometry::FEI4_CHIP) {
      start_chip = 0;
      end_chip = 1;
    }
    else{ // should be  geo.pixType()==PixGeometry::FEI2_MODULE || geo.pixType()==PixGeometry::FEI4_MODULE
      if (chip>=0 && chip <g_nChip) {
	start_chip = chip;
	end_chip   = chip+1;
      }
      else {
	start_chip = 0;
	end_chip   = g_nChip;
      }
    }
    Average_t av;
    //int g_nCol  = geo.nChipCol();
    int g_nRow  = geo.nChipRow();

    if(total_mask_steps==0 || mask_steps ==0){
      total_mask_steps=1;
      mask_steps =1;
    }

    unsigned int n_parts = g_nPix/total_mask_steps;
    for (unsigned int chip_i=start_chip; chip_i<end_chip; chip_i++) {
      for (unsigned int part_i=0; part_i< n_parts; part_i++) {
	
	unsigned int scan_start_mask_step_part= start_mask_step + part_i*total_mask_steps;
	unsigned int scan_end_mask_step_part  = scan_start_mask_step_part + mask_steps;
	
	unsigned int start_mask_step_part = part_i*total_mask_steps;
	unsigned int end_mask_step_part   = start_mask_step_part + total_mask_steps;
	
	for (unsigned int mask_step_i=start_mask_step_part;
	     mask_step_i<end_mask_step_part;
	     mask_step_i++) {
	  
	  unsigned int col_i = mask_step_i / g_nRow;
	  unsigned int row_i = mask_step_i % g_nRow;
	  
	  if (col_i%2==1) {
	    row_i=g_nRow - 1 - row_i;
	  }
	  
	  if (pixel_types & PixelTypeMask_t::type(col_i,row_i, (geo.pixType()==PixGeometry::FEI4_CHIP||geo.pixType()==PixGeometry::FEI4_MODULE))) {
	    //PixelCoord_t pixel(chip_i,col_i,row_i);
	    PixCoord pixel = geo.coord(chip_i,col_i,row_i);
	    float value = binContent(src,startColumn(src)+pixel.x(),startRow(src)+pixel.y());
	    
	    if (mask_step_i<scan_start_mask_step_part || mask_step_i>=scan_end_mask_step_part) {
	      
	      if (!reference.isSet(value)) continue; // if the bin has not been touched it should be equal to zero 0.
	      
	      value = reference.transformOff(value);
	    }
	    else {
	      value = reference.transformOn(value);
	    }
	    av.add(value);
	  }
	}
      }
    }

    if (av.n()>1 && av.min()<=av.max()) {
      if (av.max()==av.min()) {
	float max_val=av.max();
	float min_val=av.min();
	if (av.min()==0) {
	  max_val=.5;
	  min_val=-.5;
	}
	else {
	  float range = av.max() *.01;
	  min_val -= range;
	  max_val += range;
	}
	return HistoBinning(1,min_val,max_val);
      }
      else {
	av.calculate();
	//	double sigma=sqrt((sum2-sum*sum/n_pixel)/(n_pixel-1));
	//      double mean=sum/n_pixel;
	unsigned int n_bins;
	if (av.rms() > (av.max()-av.min())*1e-3) {
	  n_bins=static_cast<unsigned int>(3*(av.max()-av.min())/av.rms());
	  if (n_bins>300) n_bins=300;
	  else if (n_bins<100) n_bins=100;
	}
	else {
	  n_bins=300;
	}

	float width=0.5*(av.max()-av.min())/n_bins;
	return HistoBinning(n_bins,av.min()-width,av.max()+width);
      }
    }
    return HistoBinning(0,0,0);
  }


  template <class T_Histo_, class T_Reference_>
  std::pair<TGraph *,TGraph *> pixelGraph(const T_Histo_ *chip_map,
					  int chip,
					  PixelTypeMask_t pixel_types,
					  Average_t *average,
					  T_Reference_ reference,
					  unsigned int mask_steps,
					  unsigned int total_mask_steps,
					  unsigned int start_mask_step)
  {
    std::pair<TGraph *,TGraph *> ret = std::make_pair<TGraph *,TGraph *>(nullptr,nullptr);
    if (!is2D(chip_map)) return ret;

    Average_t temp_av;

    if (!chip_map || nColumns(chip_map)<=0 || nRows(chip_map)<=0) return ret;

    unsigned n_pixel; // =nColumns(chip_map)*nRows(chip_map);
    unsigned int start_chip;
    unsigned int end_chip;

    PixGeometry geo(nRows(chip_map),nColumns(chip_map));

    if(geo.pixType()==PixGeometry::INVALID_MODULE){
      std::cerr << "ERROR [pixelGraph] Expected 2D histogram containing a module or front-end map." << std::endl;
      return ret;
    }
    int g_nChip = geo.nChip();
    int g_nPix  = geo.nChipPixel();

    n_pixel = g_nPix;

    if (  geo.pixType()==PixGeometry::FEI2_CHIP ||  geo.pixType()==PixGeometry::FEI4_CHIP) {
      start_chip=0;
      end_chip=1;
    }else{ // should be  geo.pixType()==PixGeometry::FEI2_MODULE || geo.pixType()==PixGeometry::FEI4_MODULE
      if (chip>=0) {
	start_chip = chip;
	end_chip   = chip+1;
      }
      else {
	start_chip = 0;
	end_chip   = g_nChip;
	n_pixel *= g_nChip;
      }
    }

    std::vector<float> x_arr[2];
    std::vector<float> y_arr[2];
    for (unsigned int i=0; i<2;i++) {
      x_arr[i].reserve(n_pixel);
      y_arr[i].reserve(n_pixel);
    }

      //int g_nCol  = geo.nChipCol();
      int g_nRow  = geo.nChipRow();
      
      
      //std::cout<<g_nRow<<std::endl;
      
//       std::cout << "INFO [pixelGraph] chips: " << start_chip << " - " << end_chip
// 	        << " with " << nRows(chip_map) << "x" << nColumns(chip_map) << " pixels;"
// 		<< "pixel type = " << pixel_types << ", parts = "  << n_parts
// 		<< " stages = " << start_mask_step << " - " << start_mask_step + mask_steps << " / " << total_mask_steps 
// 		<< std::endl;

    std::cout<<"Mask steps "<<total_mask_steps<<" "<<mask_steps<<std::endl;
    if(total_mask_steps==0 || mask_steps ==0){
      total_mask_steps=1;
      mask_steps =1;
    }
    
     unsigned int n_parts = g_nPix/total_mask_steps;
      for (unsigned int chip_i=start_chip; chip_i<end_chip; chip_i++) {
	for (unsigned int part_i=0; part_i< n_parts; part_i++) {
	  
	  unsigned int scan_start_mask_step_part= start_mask_step + part_i*total_mask_steps;
	  unsigned int scan_end_mask_step_part  = scan_start_mask_step_part + mask_steps;
	  
	  unsigned int start_mask_step_part = part_i*total_mask_steps;
	  unsigned int end_mask_step_part   = start_mask_step_part + total_mask_steps;

	  for (unsigned int mask_step_i=start_mask_step_part;
	       mask_step_i<end_mask_step_part;
	       mask_step_i++) {

	    unsigned int col_i = mask_step_i / g_nRow;
	    unsigned int row_i = mask_step_i % g_nRow;
	    
	    if (col_i%2==1) {
	      row_i=g_nRow - 1 - row_i;
	    }
	    if (pixel_types & PixelTypeMask_t::type(col_i,row_i, (geo.pixType()==PixGeometry::FEI4_CHIP||geo.pixType()==PixGeometry::FEI4_MODULE))) {
	      PixCoord pixel = geo.coord(chip_i,col_i,row_i);
	      unsigned int pixel_i = pixel.pixel();
	      float value = binContent(chip_map,startColumn(chip_map)+pixel.x(),startRow(chip_map)+pixel.y());
	      unsigned int type_i;
	      if (mask_step_i<scan_start_mask_step_part || mask_step_i>=scan_end_mask_step_part) {
		
		if (!reference.isSet(value)) continue; // if the bin has not been touched it should be equal to zero 0.
		
		value = reference.transformOff(value);
		type_i=1;
	      }
	      else {
		value = reference.transformOn(value);
		type_i=0;
	      }
	      
	      if (pixel_i>=0 && pixel_i<(unsigned int)(g_nPix * g_nChip) && !isnan(value)) {
		y_arr[type_i].push_back(value);
		x_arr[type_i].push_back(pixel_i);
	      }

	      temp_av.add(value);
	    }
	  }
	}
      }
      //}
    temp_av.calculate();

    if (average) {
      *average = temp_av;
    }

    for (unsigned int type_i=0; type_i<2; type_i++) {
      if (x_arr[type_i].size()<=0) continue;
      TGraph *a_pixel_graph=new TGraph(x_arr[type_i].size(), &(x_arr[type_i][0]), &(y_arr[type_i][0]));
      if (a_pixel_graph) {
	if (temp_av.min()<temp_av.max() && temp_av.min()>=0) {
	  a_pixel_graph->SetMaximum(temp_av.max()*1.2);
	  if (temp_av.min()==0.) {
	    a_pixel_graph->SetMinimum(temp_av.min());
	  }
	  else {
	    a_pixel_graph->SetMinimum(temp_av.min()*.8);
	  }
	}
	else {
	  a_pixel_graph->SetMaximum( (temp_av.max()>0 ? temp_av.max()*1.2 : temp_av.max()*.8 ) );
	  a_pixel_graph->SetMinimum( temp_av.min() * 1.2 );
	}

	std::stringstream name;
	name << histoName(chip_map) << "_graph";
	if (type_i!=0) {
	  name << "_notScanned";
	}
	a_pixel_graph->SetName(name.str().c_str());

	std::stringstream title;
	title << histoTitle(chip_map);
	if (type_i!=0) {
	  title << " (not scanned) ";
	}
	a_pixel_graph->SetTitle(title.str().c_str());
      }
      //      std::cout << "INFO [pixelGraph] new graph " << a_pixel_graph->GetName() << " size= " << a_pixel_graph->GetN() << std::endl;
      if (type_i==0) ret.first = a_pixel_graph;
      else ret.second = a_pixel_graph;
    }
    return ret;
  }

  // getDACMap must be checked by using FEI3 module and FEI4 double chip module. Takubo
  template <class T_Histo_>
  T_Histo_* getDACMap(int chip, const std::string &dac_name, PixA::ConfigRef &mod_config)
  {
    if(dac_name!="TDAC" && dac_name!="FDAC") return NULL; // no other map programmed
    int nChipsMax = 0;
    for(unsigned int chip_i=0; chip_i<50; chip_i++){
      std::stringstream chip_nr;
      chip_nr << chip_i;
      try { 
	mod_config.subConfig("PixFe_"+chip_nr.str()); 
	nChipsMax++; 
      } catch(...){ 
	break; 
      }  
    } 
    std::string FEflavour;
    try{
      FEflavour = confVal<std::string>(mod_config.subConfig("PixFe_0")["ClassInfo"]["ClassName"]); 
    }catch(...) {
      FEflavour="PixFeI2";
    }
  
    PixLib::PixGeometry::pixelType ptype=PixLib::PixGeometry::INVALID_MODULE;
    if(FEflavour=="PixFeI2" || FEflavour=="PixFeI1") {
      if(nChipsMax==1) 
	ptype = PixLib::PixGeometry::FEI2_CHIP; 
      else
	ptype = PixLib::PixGeometry::FEI2_MODULE; 
    } else if(FEflavour=="PixFeI4A" || FEflavour=="PixFeI4B"){ 
      if(nChipsMax==1) 
	ptype = PixLib::PixGeometry::FEI4_CHIP; 
      else 
	ptype = PixLib::PixGeometry::FEI4_MODULE; 
    } 
    PixLib::PixGeometry pg(ptype);  
    
    unsigned int nrows = pg.nRow();
    unsigned int ncols = pg.nCol();
    unsigned int nFErows = pg.nChipRow();
    unsigned int nFEcols = pg.nChipCol();
    unsigned int chip_start = 0;
    unsigned int chip_end = nChipsMax;

    if(chip>=0){
      chip_start=chip;
      chip_end=chip+1;
      nrows = nFErows;
      ncols = nFEcols;
    }
    
    std::stringstream title;
    title << dac_name << " Map";

    std::stringstream name;
    name << dac_name << "_Map";

    if(chip>=0){
      title << ", FE"<< chip;
      name << "_" << chip;
    }
    T_Histo_ *h2=createHisto<T_Histo_>(name.str().c_str(),title.str().c_str(), 
				       ncols,-.5, ncols-.5, 
				       nrows, -.5, nrows - .5 );
    for(unsigned int chip_i=chip_start; chip_i<chip_end; chip_i++){
      std::stringstream chip_nr;
      chip_nr << chip_i;
      try {
	PixA::ConfigRef fe_config( mod_config.subConfig("PixFe_"+chip_nr.str()).subConfig("Trim_0/Trim"));
	const PixLib::ConfMatrix &a_matrix = confMatrix<PixLib::TrimVal_t>(fe_config["Trim"][dac_name]);
	for(unsigned int col_i=0; col_i<nFEcols; col_i++){
	  for(unsigned int row_i=0; row_i<nFErows; row_i++){
	    PixCoord pixel = pg.coord(((chip>=0) ? 0 : chip_i),col_i,row_i);
	    setBinContent(h2,startColumn(h2)+pixel.x(), startRow(h2)+pixel.y(), confMatrixVal<PixLib::TrimVal_t>(a_matrix,col_i,row_i));
	  }
	}
      }
      catch (PixA::ConfigException &err) {
	std::cout << "FATAL [getDACMap] caught config exception :" << err << std::endl;
      }
      catch(...) {
      }
    }

    return h2;

  }

  /*
  template <class T_Histo_>
  T_Histo_* getDACMap(int chip, const std::string &dac_name, PixA::ConfigRef &mod_config)
  {
    if (chip>16) return NULL;
    if(dac_name!="TDAC" && dac_name!="FDAC") return NULL; // no other map programmed
    
    unsigned int nrows = PixelCoord_t::s_nRow;
    unsigned int ncols = PixelCoord_t::s_nCol;
    unsigned int chip_start;
    unsigned int chip_end;
    if(chip>=0){
      chip_start=chip;
      chip_end=chip+1;
    }
    else {
      nrows *= 2;
      ncols *= PixelCoord_t::s_nChip/2;

      chip_start=0;
      chip_end=PixelCoord_t::s_nChip;
    }

    std::stringstream title;
    title << dac_name << " Map";

    std::stringstream name;
    name << dac_name << "_Map";

    if(chip>=0){
      title << ", FE"<< chip;
      name << "_" << chip;
    }

    T_Histo_ *h2=createHisto<T_Histo_>(name.str().c_str(),title.str().c_str(), 
				       ncols,-.5, ncols-.5, 
				       nrows, -.5, nrows - .5 );

    for(unsigned int chip_i=chip_start; chip_i<chip_end; chip_i++){
      std::stringstream chip_nr;
      chip_nr << chip_i;
      try {
	PixA::ConfigRef fe_config( mod_config.subConfig("PixFe_"+chip_nr.str()).subConfig("Trim_0/Trim"));
	const PixLib::ConfMatrix &a_matrix = confMatrix<PixLib::TrimVal_t>(fe_config["Trim"][dac_name]);

	for(unsigned int col_i=0; col_i<static_cast<unsigned int>(PixelCoord_t::s_nCol); col_i++){
	  for(unsigned int row_i=0; row_i<static_cast<unsigned int>(PixelCoord_t::s_nRow); row_i++){
	    PixelCoord_t pixel((chip>=0 ? 0 : chip_i),col_i,row_i);
	    setBinContent(h2,startColumn(h2)+pixel.x(), startRow(h2)+pixel.y(), confMatrixVal<PixLib::TrimVal_t>(a_matrix,col_i,row_i));
	  }
	}
      }
      catch (PixA::ConfigException &err) {
	std::cout << "FATAL [getDACMap] caught config exception :" << err << std::endl;
      }
      catch(...) {
      }
    }

    return h2;
  }
*/

  // instantiation of some templates for common template parameters
  // -- fillHistoFromMap
  template void fillHistoFromMap<TH2,NoReference>(TH1 *dest,
						  TH1 *dest_non_scanned,
						  const TH2 *src,
						  int chip,
						  PixelTypeMask_t pixel_types,
						  NoReference a,
						  unsigned int mask_steps,
						  unsigned int total_mask_steps,
						  unsigned int start_mask_step);

  template void fillHistoFromMap<TH2,SimpleReference>(TH1 *dest,
						      TH1 *dest_non_scanned,
						      const TH2 *src,
						      int chip,
						      PixelTypeMask_t pixel_types,
						      SimpleReference a,
						      unsigned int mask_steps,
						      unsigned int total_mask_steps,
						      unsigned int start_mask_step);

  template <> void fillHistoFromMap<TH1,NoReference>(TH1 *dest,
						     TH1 *dest_non_scanned,
						     const TH1 *src,
						     int chip,
						     PixelTypeMask_t pixel_types,
						     NoReference a,
						     unsigned int mask_steps,
						     unsigned int total_mask_steps,
						     unsigned int start_mask_step)
  {
    assert(!src || src->InheritsFrom(TH2::Class()));
    fillHistoFromMap<TH2,NoReference>(dest,dest_non_scanned,static_cast<const TH2 *>(src),chip,pixel_types, a, mask_steps, total_mask_steps, start_mask_step);
  }
/*
  template void fillHistoFromMap<TH1,NoReference>(TH1 *dest,
						  TH1 *dest_non_scanned,
						  const TH1 *src,
						  int chip,
						  PixelTypeMask_t pixel_types,
						  NoReference a,
						  unsigned int mask_steps,
						  unsigned int total_mask_steps,
						  unsigned int start_mask_step);

*/
  template <> void fillHistoFromMap<TH1,SimpleReference>(TH1 *dest,
						      TH1 *dest_non_scanned,
						      const TH1 *src,
						      int chip,
						      PixelTypeMask_t pixel_types,
						      SimpleReference a,
						      unsigned int mask_steps,
						      unsigned int total_mask_steps,
						      unsigned int start_mask_step)
  {
    assert(!src || src->InheritsFrom(TH2::Class()));
    fillHistoFromMap<TH2,SimpleReference>(dest,dest_non_scanned, static_cast<const TH2 *>(src),chip,pixel_types, a, mask_steps, total_mask_steps, start_mask_step);
  }
/*
  template void fillHistoFromMap<TH1,SimpleReference>(TH1 *dest,
						      TH1 *dest_non_scanned,
						      const TH1 *src,
						      int chip,
						      PixelTypeMask_t pixel_types,
						      SimpleReference a,
						      unsigned int mask_steps,
						      unsigned int total_mask_steps,
						      unsigned int start_mask_step);


i*/
  // instantiation of some templates for common template parameters
  // -- fillHistoFromMap

  template void fillHistoFromMap(TH1 *dest,
				 TH1 *dest_non_scanned,
				 const PixLib::Histo *src,
				 int chip,
				 PixelTypeMask_t pixel_types,
				 NoReference a,
				 unsigned int mask_steps,
				 unsigned int total_mask_steps,
				 unsigned int start_mask_step);

  template void fillHistoFromMap(TH1 *dest,
				 TH1 *dest_non_scanned,
				 const PixLib::Histo *src,
				 int chip,
				 PixelTypeMask_t pixel_types,
				 SimpleReference a,
				 unsigned int mask_steps,
				 unsigned int total_mask_steps,
				 unsigned int start_mask_step);


  // instantiation of some templates for common template parameters
  // -- guessHistoBinningForMapProjection
  template
  HistoBinning guessHistoBinningForMapProjection<TH2,NoReference>(const TH2 *src,
								  int chip,
								  PixelTypeMask_t pixel_types,
								  NoReference reference,
								  unsigned int mask_steps,
								  unsigned int total_mask_steps,
								  unsigned int start_mask_step);

  template
  HistoBinning guessHistoBinningForMapProjection<TH2,SimpleReference>(const TH2 *src,
								  int chip,
								  PixelTypeMask_t pixel_types,
								  SimpleReference reference,
								  unsigned int mask_steps,
								  unsigned int total_mask_steps,
								  unsigned int start_mask_step);

  template <>
  HistoBinning guessHistoBinningForMapProjection<TH1,NoReference>(const TH1 *src,
								  int chip,
								  PixelTypeMask_t pixel_types,
								  NoReference reference,
								  unsigned int mask_steps,
								  unsigned int total_mask_steps,
								  unsigned int start_mask_step)
  {
    assert(!src || src->InheritsFrom(TH2::Class()));
    return guessHistoBinningForMapProjection<TH2,NoReference>(static_cast<const TH2 *>(src),chip, pixel_types, reference, mask_steps, total_mask_steps, start_mask_step);
  }
/*
  template
  HistoBinning guessHistoBinningForMapProjection<TH1,NoReference>(const TH1 *src,
								  int chip,
								  PixelTypeMask_t pixel_types,
								  NoReference reference,
								  unsigned int mask_steps,
								  unsigned int total_mask_steps,
								  unsigned int start_mask_step);
*/
  template <>
  HistoBinning guessHistoBinningForMapProjection<TH1,SimpleReference>(const TH1 *src,
								      int chip,
								      PixelTypeMask_t pixel_types,
								      SimpleReference reference,
								      unsigned int mask_steps,
								      unsigned int total_mask_steps,
								      unsigned int start_mask_step)
  {
    assert(!src || src->InheritsFrom(TH2::Class()));
    return guessHistoBinningForMapProjection<TH2,SimpleReference>(static_cast<const TH2 *>(src),chip, pixel_types, reference, mask_steps, total_mask_steps, start_mask_step);
  }
/*
  template
  HistoBinning guessHistoBinningForMapProjection<TH1,SimpleReference>(const TH1 *src,
								  int chip,
								  PixelTypeMask_t pixel_types,
								  SimpleReference reference,
								  unsigned int mask_steps,
								  unsigned int total_mask_steps,
								  unsigned int start_mask_step);
*/
  template
  HistoBinning guessHistoBinningForMapProjection<PixLib::Histo,NoReference>(const PixLib::Histo *src,
								  int chip,
								  PixelTypeMask_t pixel_types,
								  NoReference reference,
								  unsigned int mask_steps,
								  unsigned int total_mask_steps,
								  unsigned int start_mask_step);

  template
  HistoBinning guessHistoBinningForMapProjection<PixLib::Histo,SimpleReference>(const PixLib::Histo *src,
										int chip,
										PixelTypeMask_t pixel_types,
										SimpleReference reference,
										unsigned int mask_steps,
										unsigned int total_mask_steps,
										unsigned int start_mask_step);

  // instantiation of some templates for common template parameters
  // -- pixelGraph

  template
  std::pair<TGraph *,TGraph *> pixelGraph<TH2,NoReference>(const TH2 *chip_map,
							   int chip,
							   PixelTypeMask_t pixel_types,
							   Average_t *average,
							   NoReference reference,
							   unsigned int mask_steps,
							   unsigned int total_mask_steps,
							   unsigned int start_mask_step);

  template
  std::pair<TGraph *,TGraph *> pixelGraph<TH2,SimpleReference>(const TH2 *chip_map,
							       int chip,
							       PixelTypeMask_t pixel_types,
							       Average_t *average,
							       SimpleReference reference,
							       unsigned int mask_steps,
							       unsigned int total_mask_steps,
							       unsigned int start_mask_step);

  template <>
  std::pair<TGraph *,TGraph *> pixelGraph<TH1,NoReference>(const TH1 *chip_map,
							   int chip,
							   PixelTypeMask_t pixel_types,
							   Average_t *average,
							   NoReference reference,
							   unsigned int mask_steps,
							   unsigned int total_mask_steps,
							   unsigned int start_mask_step)
  {
    assert(!chip_map || chip_map->InheritsFrom(TH2::Class()));
    return pixelGraph<TH2,NoReference>(static_cast<const TH2 *>(chip_map), chip,pixel_types, average,reference,mask_steps, total_mask_steps, start_mask_step);
  }
/*
  template
  std::pair<TGraph *,TGraph *> pixelGraph<TH1,NoReference>(const TH1 *chip_map,
							   int chip,
							   PixelTypeMask_t pixel_types,
							   Average_t *average,
							   NoReference reference,
							   unsigned int mask_steps,
							   unsigned int total_mask_steps,
							   unsigned int start_mask_step);
*/
  template <>
  std::pair<TGraph *,TGraph *> pixelGraph<TH1,SimpleReference>(const TH1 *chip_map,
							       int chip,
							       PixelTypeMask_t pixel_types,
							       Average_t *average,
							       SimpleReference reference,
							       unsigned int mask_steps,
							       unsigned int total_mask_steps,
							       unsigned int start_mask_step)
  {
    assert(!chip_map || chip_map->InheritsFrom(TH2::Class()));
    return pixelGraph<TH2,SimpleReference>(static_cast<const TH2 *>(chip_map), chip,pixel_types, average,reference,mask_steps, total_mask_steps, start_mask_step);
  }
/*
  template
  std::pair<TGraph *,TGraph *> pixelGraph<TH1,SimpleReference>(const TH1 *chip_map,
							       int chip,
							       PixelTypeMask_t pixel_types,
							       Average_t *average,
							       SimpleReference reference,
							       unsigned int mask_steps,
							       unsigned int total_mask_steps,
							       unsigned int start_mask_step);
*/
  template
  std::pair<TGraph *,TGraph *> pixelGraph<PixLib::Histo,NoReference>(const PixLib::Histo *chip_map,
								     int chip,
								     PixelTypeMask_t pixel_types,
								     Average_t *average,
								     NoReference reference,
								     unsigned int mask_steps,
								     unsigned int total_mask_steps,
								     unsigned int start_mask_step);

  template
  std::pair<TGraph *,TGraph *> pixelGraph<PixLib::Histo,SimpleReference>(const PixLib::Histo *chip_map,
									 int chip,
									 PixelTypeMask_t pixel_types,
									 Average_t *average,
									 SimpleReference reference,
									 unsigned int mask_steps,
									 unsigned int total_mask_steps,
									 unsigned int start_mask_step);

  // instantiation of some templates for common template parameters
  // -- dac, chip maps, transformation to ROOT TH1, TH2

  template
  PixLib::Histo* getDACMap<PixLib::Histo>(int chip, const std::string &dac_name, PixA::ConfigRef &mod_config);

  template
  TH2* getDACMap<TH2>(int chip, const std::string &dac_name, PixA::ConfigRef &mod_config);

  template <>
  TH1* getDACMap<TH1>(int chip, const std::string &dac_name, PixA::ConfigRef &mod_config)
  {
    return getDACMap<TH2>(chip,dac_name,mod_config);
  }
/*
  template
  TH1* getDACMap<TH1>(int chip, const std::string &dac_name, PixA::ConfigRef &mod_config);
*/

  template
  TH2* chipMap<TH2,PixLib::Histo>(const PixLib::Histo *h2, int chip);

  template
  TH2* chipMap<TH2,TH2>(const TH2 *h2, int chip);

  template <>
  TH2* chipMap<TH2,TH1>(const TH1 *h2, int chip) {
    assert(!h2 || h2->InheritsFrom(TH2::Class()));
    return chipMap<TH2,TH2>(static_cast<const TH2 *>(h2),chip);
  }

/*
  template
  TObject *transform<PixLib::Histo>(const PixLib::Histo *h);
  //needed by chipMap<TH2,TH2>(...)

  template
  TObject *transform<TH2>(const TH2 *h);

  template
  TObject *transform<TH1>(const TH1 *h);
*/


  template <class T_Histo_t>
  const std::pair<int , int > findAxisVar(const T_Histo_t &histo, const std::string &histo_name, const PixA::ConfigRef &scan_config, const Index_t &index) {
    const PixA::ConfGrpRef &loop_config(scan_config["loops"]);
    //    std::cout << "INFO [PixA::findAxisVar] get extra info for histo " << histo_name << std::endl;
    //    std::string::size_type pos = histo_name.rfind("/");
    //    if (pos != std::string::npos && ++pos>histo_name.size()) pos=std::string::npos;
    // ((pos==std::string::npos ? histo_name : std::string(histo_name,pos,histo_name.size()-pos))
    const HistoExtraInfo_t &extra_info(HistoExtraInfoList::histoExtraInfo(histo_name));

    // only x and y axis are considered
    // the z axis cannot depend on a loop index
    unsigned int start_axis=0; 
    unsigned int end_axis=2;

    if (!is2D(&histo)) {
      end_axis=1;
    }

    int loop_var[2]={-1,-1};

    for (unsigned int loop_i=0; loop_i<3; loop_i++) {
      //      if (loop_i+1 >= index.size() ) {
      {
	std::stringstream loop_idx;
	loop_idx << loop_i;

	// if the index for the given loop index is non zero
	// then there is an entire histogram per this loop index and the loop index 
	// cannot be used at the same time as an index for an axis.
	if (!confVal<bool>(loop_config[std::string("activeLoop_")+loop_idx.str()])) continue;

	const std::string &param_name = confVal<std::string>(loop_config[std::string("paramLoop_")+loop_idx.str()]);
	std::cout << "INFO [PixA::findAxisVar] " << loop_i << " : " << param_name << std::endl;
	for (unsigned int axis_i=start_axis; axis_i<end_axis; axis_i++) {
	  if (extra_info.axis(static_cast<HistoExtraInfo_t::EAxis>(axis_i)).hasVar(param_name)) {
	    if (loop_var[axis_i]<0) {
	      // only change the association of loop variable and axis if it has not been defined yet.

	      // verify that the number of bins corresponds with the loop var steps
	      bool uniform = confVal<bool>(loop_config[std::string("loopVarUniformLoop_")+loop_idx.str()]);

	      if (uniform)  {
		unsigned int   steps   =confVal<int>(loop_config[std::string("loopVarNStepsLoop_")+loop_idx.str()]);
		std::cout << "INFO [PixA::findAxisVar] " << loop_i << " : " << param_name
			  << " : matches axis " << axis_i << " ? " 
			  << steps << " =?= " << nBins(&histo,axis_i)
			  << std::endl;
		if (nBins(&histo,axis_i) != static_cast<unsigned int>(steps) ) continue;
	      }
	      else {
		const std::vector<float> &
		  values ( confVal<std::vector<float> >(loop_config[std::string("loopVarValuesLoop_")+loop_idx.str()]));
		std::cout << "INFO [PixA::findAxisVar] " << loop_i << " : " << param_name
			  << " : matches axis " << axis_i << " ? " 
			  << values.size() << " =?= " << nBins(&histo,axis_i)
			  << std::endl;
		if ( nBins(&histo,axis_i) != values.size()) continue;
	      }
	      
	      loop_var[axis_i]=loop_i;
	    }
	  }
	}
      }
    }

    return std::make_pair(loop_var[0],loop_var[1]);
  }

  template <class T_Histo_t>
  TH2 *createHistoResortBins(const T_Histo_t &histo, const PixA::ConfigRef &scan_config, const std::pair<int , int> &axis_loop_var);

  template <class T_Histo_t>
  TH2 *createHisto(const T_Histo_t &histo, const std::string &histo_name, const PixA::ConfigRef &scan_config, const std::pair<int , int> &axis_loop_var)
  {
    if (!is2D(&histo)) return NULL;

    const PixA::ConfGrpRef &loop_config(scan_config["loops"]);
    const HistoExtraInfo_t &extra_info(HistoExtraInfoList::histoExtraInfo(histo_name));

    std::vector<float> bins[2]; // container for non uniform binning
    HistoBinning binning[2];          // container for uniform binng
    bool uniform_binning=true;
    int axis_loop_var_temp[2]={axis_loop_var.first,axis_loop_var.second};

    TH2 *dest_h2=NULL;

    for (unsigned int axis_i=0; axis_i<2; axis_i++) {
      binning[axis_i].setBinning(nBins(&histo,axis_i),axisMin(&histo,axis_i),axisMax(&histo,axis_i));
      if (axis_loop_var_temp[axis_i]>=0) {
	std::stringstream loop_idx;
	loop_idx << axis_loop_var_temp[axis_i];

	// verify that the number of bins corresponds with the loop var steps
	bool uniform = confVal<bool>(loop_config[std::string("loopVarUniformLoop_")+loop_idx.str()]);

	if (uniform)  {
	  int   steps   =confVal<int>(loop_config[std::string("loopVarNStepsLoop_")+loop_idx.str()]);
	  // the matching of steps and bins should have been verified before hand
	  assert( nBins(&histo,axis_i) == static_cast<unsigned int>(steps) );
	  float min_val = confVal<float>(loop_config[std::string("loopVarMinLoop_")+loop_idx.str()]);
	  float max_val = confVal<float>(loop_config[std::string("loopVarMaxLoop_")+loop_idx.str()]);
	  float bin_width=max_val-min_val;
	  if (steps>1) {
	    // assume that the min and max value indicate the value at the centre of the bin
	    bin_width /= (steps-1);
	    bin_width *= .5;
	  }
	  else {
	    if (min_val==max_val) bin_width=0.5 ;
	  }

	  binning[axis_i].setBinning(steps,min_val-bin_width, max_val+bin_width);
	}
	else {
	  uniform_binning=false;
	  const std::vector<float> &
	    values ( confVal<std::vector<float> >(loop_config[std::string("loopVarValuesLoop_")+loop_idx.str()]));

	  // the matching of steps and bins should have been verified before hand
	  assert (static_cast<unsigned int>(nBins(&histo,axis_i)) == values.size());

	  if (values.size()==0) continue;

	  if (values.size()==1) {
	    binning[axis_i].setBinning(1,values[0]-.5,values[1]+.5);
	  }
	  else {
	    std::vector<float>::const_iterator value_iter = values.begin(); 
	    float last_value  = *value_iter;
	    value_iter++;
	    float new_value  = *value_iter;

	    float bin_width_1 = new_value - last_value;
	    bool need_to_sort_values=true;
	    if  (bin_width_1 > 0.) {
	      need_to_sort_values=false;
	      bin_width_1*=.5;
	      float bin_start=last_value-bin_width_1;
	      float bin_end=last_value+bin_width_1;
	      bins[axis_i].push_back(bin_start);
	      for (;;) {
		last_value = new_value;
		bins[axis_i].push_back(bin_end);

		value_iter++;
		if (value_iter==values.end()) break;
		bin_start = bin_end;

		new_value= *value_iter;
		float bin_width_2 = new_value - last_value;
		if  (bin_width_2 <= 0.) {
		  need_to_sort_values = true;
		  bins[axis_i].clear();
		  break;
		}
		bin_width_2 *= .5;
		bin_end = last_value+bin_width_2;
	      }
	    }
	    if (need_to_sort_values) {
	      dest_h2=createHistoResortBins(histo,scan_config, axis_loop_var);
	      break;
	    }
	  }
	}
      }
    }

    if (!dest_h2) {
      if (uniform_binning) {
	//	std::cout << "INFO [HistoUtil.cxx:createHisto] binning :" 
	//		  << binning[0].min() << " - " << binning[0].max() << " / " << binning[0].nBins()<< ", "
	//		  << binning[1].min() << " - " << binning[1].max() << " / " << binning[1].nBins()
	//		  << std::endl;
	dest_h2=new TH2F(histoName(&histo), histoTitle(&histo), 
			 binning[0].nBins(),binning[0].min(),binning[0].max(),
			 binning[1].nBins(),binning[1].min(),binning[1].max());
	if (!dest_h2) return NULL;
	dest_h2->SetDirectory(0);
      }
      else {
	for (unsigned int axis_i=0; axis_i<2; axis_i++) {
	  if ( bins[axis_i].empty()) {
	    float bin_width=binning[axis_i].max()-binning[axis_i].min();
	    if (bin_width==0) {
	      bin_width=0.5;
	    }
	    float bin_start=binning[axis_i].min();

	    for (unsigned int bin_i=0; bin_i < binning[axis_i].nBins(); bin_i++) {
	      bins[axis_i].push_back(bin_start);
	      bin_start+= bin_width;
	    }
	    bins[axis_i].push_back( binning[axis_i].max() );
	  }
	  assert( bins[axis_i].size()>1);
	}
	dest_h2=new TH2F(histoName(&histo), histoTitle(&histo), bins[0].size()-1, &(bins[0][0]),bins[1].size()-1, &(bins[1][0]));
	if (!dest_h2) return NULL;
	dest_h2->SetDirectory(0);
      }

      float a_min=FLT_MAX;
      float a_max=-FLT_MAX;
      for (unsigned int i=0; i<nBins(&histo,0); i++) {
	for (unsigned int j=0; j<nBins(&histo,1); j++) {
	  float src_bin_content = binContent(&histo,startColumn(&histo)+i, startRow(&histo)+j );
	  dest_h2->SetBinContent(i+startColumn(dest_h2),j+startRow(dest_h2), src_bin_content) ;
	  if (src_bin_content < a_min ) a_min = src_bin_content ;
	  if (src_bin_content > a_max ) a_max = src_bin_content ;
	}
      }
      if (a_min==a_max) {
	if (a_min==0.) {
	  dest_h2->SetMinimum(-1.);
	  dest_h2->SetMaximum(1.);
	}
	else {
	  dest_h2->SetMinimum(a_min*(1-1e-3));
	  dest_h2->SetMaximum(a_min*(1+1e-3));
	}
      }
    }
    if (dest_h2) {
      const std::string *var_name[2]={NULL,NULL};
      for (unsigned int axis_i=0; axis_i<2; axis_i++) {
	if (axis_loop_var_temp[axis_i]>=0) {
	  std::stringstream loop_param_name;
	  loop_param_name << "paramLoop_" << axis_loop_var_temp[axis_i];
	  var_name[axis_i] = &(confVal<std::string>(loop_config[loop_param_name.str()]));
	}
      }

      if (dest_h2->GetXaxis()) {
	if (var_name[0]) {
	  dest_h2->GetXaxis()->SetName(var_name[0]->c_str());
	  dest_h2->GetXaxis()->SetTitle(extra_info.axis(HistoExtraInfo_t::kXaxis).title(*(var_name[0])).c_str());
	}
	else {
	  dest_h2->GetXaxis()->SetTitle(extra_info.axis(HistoExtraInfo_t::kXaxis).title().c_str());
	}
      }
      if (dest_h2->GetYaxis()) {
	if (var_name[1]) {
	  dest_h2->GetYaxis()->SetName(var_name[1]->c_str());
	  dest_h2->GetYaxis()->SetTitle(extra_info.axis(HistoExtraInfo_t::kYaxis).title(*(var_name[1])).c_str());
	}
	else {
	  dest_h2->GetYaxis()->SetTitle(extra_info.axis(HistoExtraInfo_t::kYaxis).title().c_str());
	}
      }
      if (dest_h2->GetZaxis()) dest_h2->GetZaxis()->SetTitle(extra_info.axis(HistoExtraInfo_t::kZaxis).title().c_str());
    }
    return dest_h2;
  }
  
  template <class T_Histo_t>
  TH2 *createHistoResortBins(const T_Histo_t &histo, const PixA::ConfigRef &scan_config, const std::pair<int , int> &axis_loop_var)
  {
    if (!is2D(&histo)) return NULL;

    const PixA::ConfGrpRef &loop_config(scan_config["loops"]);
    //const HistoExtraInfo_t &extra_info(HistoExtraInfoList::histoExtraInfo(const_cast<PixLib::Histo &>(histo).name()));

    std::vector<float> bins[2]; // container for non uniform binning
    const std::vector<float> *bin_ptr[2]={&bins[0],&bins[1]};
    int axis_loop_var_temp[2]={axis_loop_var.first,axis_loop_var.second};

    for (unsigned int axis_i=0; axis_i<2; axis_i++) {
      float min_val=axisMin(&histo,axis_i);
      float max_val=axisMax(&histo,axis_i);
      int steps=nBins(&histo,axis_i);

      if (axis_loop_var_temp[axis_i]>=0) {
	std::stringstream loop_idx;
	loop_idx << axis_loop_var_temp[axis_i];

	// verify that the number of bins corresponds with the loop var steps
	bool uniform = confVal<bool>(loop_config[std::string("loopVarUniformLoop_")+loop_idx.str()]);

	if (!uniform) {
	  const std::vector<float> &
	    values ( confVal<std::vector<float> >(loop_config[std::string("loopVarValuesLoop_")+loop_idx.str()]));

	  bin_ptr[axis_i]=&values;
	  // the matching of steps and bins should have been verified before hand
	  assert ( nBins(&histo,axis_i) == values.size());

	  if (values.size()==0) {
	    bins[axis_i].push_back(-.5);
	    bins[axis_i].push_back(+.5);
	  }
	  else if (values.size()==1) {
	    bins[axis_i].push_back(values[0]-.5);
	    bins[axis_i].push_back(values[0]+.5);
	  }
	  else {
	    std::set<float> sorted_values;
	    {
	      for (std::vector<float>::const_iterator value_iter=values.begin();
		   value_iter != values.end();
		   value_iter++) {
		sorted_values.insert(*value_iter);
	      }
	    }

	    std::set<float>::const_iterator value_iter = sorted_values.begin(); 
	    float last_value  = *value_iter;
	    value_iter++;
	    float new_value  = *value_iter;

	    float bin_width_1 = (new_value - last_value)*.5;

	    float bin_start=last_value-bin_width_1;
	    float bin_end=last_value+bin_width_1;
	    bins[axis_i].push_back(bin_start);

	    for (;;) {
	      last_value = new_value;
	      bins[axis_i].push_back(bin_end);

	      value_iter++;
	      if (value_iter==sorted_values.end()) break;
	      bin_start = bin_end;

	      new_value= *value_iter;
	      float bin_width_2 = (new_value - last_value)*.5;
	      bin_end = last_value+bin_width_2;
	    }
	  }
	}
	else {
	  steps   =confVal<int>(loop_config[std::string("loopVarNStepsLoop_")+loop_idx.str()]);
	  // the matching of steps and bins should have been verified before hand
	  assert( nBins(&histo,axis_i) == static_cast<unsigned int>(steps) );
	  min_val = confVal<float>(loop_config[std::string("loopVarMinLoop_")+loop_idx.str()]);
	  max_val = confVal<float>(loop_config[std::string("loopVarMaxLoop_")+loop_idx.str()]);
	}
      }
      if (bins[axis_i].empty()) {
	float bin_width=max_val-min_val;
	if (steps>1) {
	  // assume that the min and max value indicate the value at the centre of the bin
	  bin_width /= (steps-1);
	}
	else {
	  if (min_val==max_val) {
	    min_val -= .5;
	    max_val += .5;
	    bin_width=1.;
	  }
	}

	float bin_start=min_val;

	for (unsigned int bin_i=0; bin_i < static_cast<unsigned int>(steps); bin_i++) {
	  bins[axis_i].push_back(bin_start);
	  bin_start+= bin_width;
	}
	bins[axis_i].push_back( max_val);
      }
    }

    TH2 *dest_h2;

    dest_h2=new TH2F(histoName(&histo), histoTitle(&histo), bins[0].size()-1, &(bins[0][0]),bins[1].size()-1, &(bins[1][0]));
    if (!dest_h2) return NULL;
    dest_h2->SetDirectory(0);

    for (unsigned int axis_i=0; axis_i<2; axis_i++) {
      assert( static_cast<unsigned int>(nBins(&histo,axis_i)) <= bin_ptr[axis_i]->size() );
    }

    for (unsigned int i=0; i<nBins(&histo,0); i++) {
      unsigned int dest_bin_i = dest_h2->GetXaxis()->FindBin( (*bin_ptr[0])[i]);
      for (unsigned int j=0; j<nBins(&histo,1); j++) {
	unsigned int dest_bin_j = dest_h2->GetXaxis()->FindBin( (*bin_ptr[1])[j]);
	dest_h2->SetBinContent(dest_h2->GetBin(dest_bin_i,dest_bin_j), binContent(&histo,startColumn(&histo)+i,startRow(&histo)+j) ) ;
      }
    }
    return dest_h2;
  }

  template <class T_Histo_t>
  TH2 *createHisto(const T_Histo_t &histo, const std::string &histo_name, const PixA::ConfigRef &scan_config, const Index_t &index ) {
    const std::pair<int , int > axis_var = findAxisVar(histo,histo_name, scan_config, index);
    return createHisto(histo,histo_name,scan_config,axis_var);
  }

  // instantiations
  template
  const std::pair<int , int >
  findAxisVar<PixLib::Histo>(const PixLib::Histo &histo, const std::string &histo_name, const PixA::ConfigRef &scan_config, const Index_t &index);

  template
  TH2 *createHisto<PixLib::Histo>(const PixLib::Histo &histo, const std::string &histo_name, const PixA::ConfigRef &scan_config, const std::pair<int , int> &axis_loop_var);
  template 
  TH2 *createHisto<PixLib::Histo>(const PixLib::Histo &histo, const std::string &histo_name, const PixA::ConfigRef &scan_config, const Index_t &index );

  template
  const std::pair<int , int >
  findAxisVar<TH2>(const TH2 &histo, const std::string &histo_name, const PixA::ConfigRef &scan_config, const Index_t &index);

  template
  TH2 *createHisto<TH2>(const TH2 &histo, const std::string &histo_name, const PixA::ConfigRef &scan_config, const std::pair<int , int> &axis_loop_var);
  template
  TH2 *createHisto<TH2>(const TH2 &histo, const std::string &histo_name, const PixA::ConfigRef &scan_config, const Index_t &index );

  template <>
  const std::pair<int , int >
  findAxisVar<TH1>(const TH1 &histo, const std::string &histo_name, const PixA::ConfigRef &scan_config, const Index_t &index) {
    assert(histo.InheritsFrom(TH2::Class()));
    return findAxisVar<TH2>(static_cast<const TH2 &>(histo), histo_name, scan_config, index);
  }

  template <>
  TH2 *createHisto<TH1>(const TH1 &histo, const std::string &histo_name, const PixA::ConfigRef &scan_config, const std::pair<int , int> &axis_loop_var) {
    assert(histo.InheritsFrom(TH2::Class()));
    return createHisto<TH2>(static_cast<const TH2 &>(histo), histo_name, scan_config, axis_loop_var);
  }

  template  <>
  TH2 *createHisto<TH1>(const TH1 &histo, const std::string &histo_name, const PixA::ConfigRef &scan_config, const Index_t &index ) {
    assert(histo.InheritsFrom(TH2::Class()));
    return createHisto<TH2>(static_cast<const TH2 &>(histo), histo_name, scan_config, index);
  }
/*
  template
  const std::pair<int , int >
  findAxisVar<TH1>(const TH1 &histo, const std::string &histo_name, const PixA::ConfigRef &scan_config, const Index_t &index);

  template
  TH2 *createHisto<TH1>(const TH1 &histo, const std::string &histo_name, const PixA::ConfigRef &scan_config, const std::pair<int , int> &axis_loop_var);
  template
  TH2 *createHisto<TH1>(const TH1 &histo, const std::string &histo_name, const PixA::ConfigRef &scan_config, const Index_t &index );
*/
}
