#ifndef PIXELDATACONTAINER_H
#define PIXELDATACONTAINER_H

#define NCHIP          16
#define NPIX         2880
#define NCOL           18
#define NROW          160

#include <string>
#include <memory>
#include <vector>
#include <cassert>

class TH1F;
class TH2F;
class TGraphErrors;

namespace PixLib {
  class Config;
  class Histo;
}

#include <ConfigWrapper/ConfigHandle.h>

class HistoInfo_t;


/*! This is a abstract class combining needs from both the ModuleAnalysis 
 *  <a href="http://sctpixel.home.cern.ch/sctpixel/RodDaq/PixelDoxy/ModuleAnalysis/html/classDatSet.html">DatSet</a>
 *  and the PixLib::PixScan-oriented PixDBData classes.
 * 
 * @authors Joern Grosse-Knetter <joern.grosse-knetter@uni-bonn.de>
 */

class PixelDataContainer {

 public:
  PixelDataContainer(const char *name=0, const char *path=0);
  virtual ~PixelDataContainer(){};

  /*! get the name of the data item
   */
  const char* GetName(){return m_dtname.c_str();};
  /*! get the original file name (as from TurboDAQ, ROOT etc)
   *
   */
  const char* GetPath(){return m_pname.c_str();};
  /*! get the original file path
   *
   */
  const char* GetFullPath(){return m_fullpath.c_str();};
  /*! change the name (ie that referring to the tree-view
   *  label) of the data item; path (ie the org. file name
   *  remains unchaned
   */
  void ChangeName(const char* newname){m_dtname = newname;};

  /*! map coordinates: convert chip, column, row into x and y coordinate
   */
  static void PixXY(int chip, int col, int row, float *xval, float *yval);
  /*! scan histogram index: convert chip, column, row into index
   */
  static int  PixIndex(int chip, int col, int row){return chip*NPIX+row+NROW*col;}
  /*! scan histogram index: convert x and y coordinate into index
   */
  static int  PixXYtoInd(int xval, int yval);
  /*! map coordinates: convert x and y coordinates into chip, column, row
   */
  static void PixXYtoCCR(int xval, int yval, int *chip, int *col, int *row);
  /*! map coordinates: convert x and y coordinates into x and y bin index
   */
  static void PixXYtoBins(float xval, float yval, int &binx, int &biny);
  /*! map coordinates: convert chip, column, row into x and y bin index
   */
  static void PixCCRtoBins(int chip, int col, int row,int &binx, int &biny);
  /*! distinguish normal 50x400um, long and ganged pixels (index)
   * 0 normal
   * 1 long
   * 2 ganged
   * 3 long ganged
   * 5 inter-ganged
   */
  static int  Pixel_Type(int col, int row);
  /*! distinguish normal 50x400um, long and ganged pixels (name)
   */
  static const char * Pixel_TypeName(int type);

  /*! returns the scan level,
   * i.e. 0 for no scan, 1 for 1D, 2 for 2D scan etc
   */
  virtual int getScanLevel(){return -1;};
  /*! returns true if a certain histo type was filled and
   *  kept during data taking
   */

  /*! Count the number of histograms per level.
   * @param type one of the supported histogram types.
   * @param idx_out will be filled with the maximum number of histograms per level (0-2) or -1 if the level does not exist.
   */
  virtual bool numberOfHistos(int type, HistoInfo_t &info ) =0;

  virtual bool haveHistoType(int){return false;};
  /*! returns number of events, i.e. number of injections 
   *  followed by LVL1 trigger(s)
   */

  /*! Returns a reference to the PixScan config    <br>
   *  BEWARE: the object returned is temporary and will be deleted
   *          by most calls to other PixDBData functions; use the
   *          Config copy constructor to retain a permanent object
   * @deprecated
   */
  virtual PixLib::Config &getScanConfig() = 0;

  /*! Returns a reference to the PixScan config (read only)   <br>
   * @deprecated
   */
  virtual PixLib::Config &getScanConfig() const { return const_cast<PixelDataContainer *>(this)->getScanConfig(); }

  /*! Returns a reference to the PixModule config    <br>
   *  BEWARE: the object returned is temporary and will be deleted
   *          by most calls to other PixDBData functions; use the
   *          Config copy constructor to retain a permanent object
   * @deprecated
   */
  virtual PixLib::Config& getModConfig() = 0;

  /**  Returns a reference to the PixModule config (read only)    <br>
   * @deprecated
   */
  virtual PixLib::Config &getModConfig() const { return const_cast<PixelDataContainer *>(this)->getModConfig(); }

  /**  Returns a reference to the PixModule config (read only)    <br>
   * The object the reference is referring to is a shared entity.
   * It may feature on demand loading and may not support matrices
   * which are for example used for the TDAC and FDAC values.
   * The interface also does not yet support to iterate over the 
   * config or config groups. It may be faster if only a small
   * number of elements is accessed.
   */
//  virtual const PixA::ConfigRef getModConfigRef() const;

  // new methods to access the configs 
  // for the time being they return invalid configs
  virtual const PixA::ConfigHandle modConfig() const {return PixA::ConfigHandle(); };
  virtual PixA::ConfigHandle modConfig() {return PixA::ConfigHandle(); };

  virtual const PixA::ConfigHandle bocConfig() const {return PixA::ConfigHandle(); };
  virtual PixA::ConfigHandle bocConfig() {return PixA::ConfigHandle(); };

  virtual const PixA::ConfigHandle scanConfig() const {return PixA::ConfigHandle(); };
  virtual PixA::ConfigHandle scanConfig() {return PixA::ConfigHandle();};

  /*! Returns a PixLib::Histo object of the requested type    <br>
   *  BEWARE: the object returned is temporary and will be deleted
   *          by most calls to other PixDBData functions; use the
   *          Histo copy constructor to retain a permanent Histo object
   */
  virtual PixLib::Histo* getGenericPixLibHisto(int type, const int scanpt[4]) = 0;

  /*! Returns a PixLib::Histo object of the requested type    <br>
   *  BEWARE: the object returned is temporary and will be deleted
   *          by most calls to other PixDBData functions; use the
   *          Histo copy constructor to retain a permanent Histo object
   */
  virtual PixLib::Histo* getGenericPixLibHisto(int type, int scanpt=-1, int scan_level=-1) = 0;

  virtual int getNevents(){return -1;};
  /*! returns number of scan steps on requested loop level
   */
  virtual int getScanSteps(int){return -1;};
  /*! returns scan start value on requested loop level
   */
  virtual int getScanStart(int){return -1;};
  /*! returns scan start value on requested loop level
   */
  virtual int getScanStop (int){return -1;};

  /*! Returns a TH2F-map of type "type" (see FileTypes.h)
   *  if chip>0 than the map for only this chip is returned
   */
  virtual TH2F* GetMap(int, int){return 0;};
  /*! Returns a TH2F-map of type "type" from a 2D scan
   *  (e.g. TDAC) for the given scan point
   *  if chip>0 than the map for only this chip is returned
   */
  virtual TH2F* Get2DMap(int, int, int){return 0;};
  /*! returns a TH2F map of "type"
   * versus inner+outer scan parameter (e.g. #hits vs
   * TDAC and VCAL) or "type"-distribution vs
   * inner scan parameter (e.g. LVL1 vs MCC strobe delay)
   */
  virtual TH2F* GetScanMap(int, int, int, int){return 0;};
  /*! Returns a graph containing hits vs scan variable
   *  for a 1D scan, or threshold, noise or chi^2 vs outer var.
   *  for a 2D scan
   */
  virtual TGraphErrors* GetScanHi(int, int, int, int a=38, int b=-1) = 0;
  /*! Returns a graph containing ToT vs scan variable (1D only)
   */
  virtual TGraphErrors* GetToTHi(int, int, int){return 0;};
  /*! Returns the ToT spectrum e.g. for a source scan
   *  as a summary for a chip (chip==-1 is the module summary)
   */
  virtual TH1F* GetSpecHi(int){return 0;};

  /*! Returns the ToT spectrum e.g. for a source scan
   *  for a given pixel; if calibration is provided
   *  the ToT is calibrated into electrons
   */
  virtual TH1F* GetToTDist(int, int, int, bool nofit=false, PixelDataContainer *ToTcal=0)  = 0;

 protected:
  std::string m_dtname, m_pname, m_fullpath;

};


#endif // PIXELDATACONTAINER_H
