#include "ScanResultStreamer.h"
#include "ResultFileIndexer.h"
#include "PixDbDataContainer.h"
#include "HistoRef.h"
#include "HistoInfo_t.h"
#include "MakePixScanHistoName.h"
#include <iostream>
#include <PixController/PixScan.h>

#include <ConfigWrapper/ConfObjUtil.h>
#include <ConfigWrapper/ConfigHandle.h>

#include <cstdlib>
#include <vector>

#include <time.h>
 
#ifndef TrimVal_t_h
#define TrimVal_t_h

namespace PixLib {
  typedef unsigned short TrimVal_t;
}

#endif


void help(const char *arg0)
{
  std::cout << arg0 << " [-i] [-q] [-e [-e matching-folder-expression]] [-l] file_name [label_name...] [file_name [label_name ...]]." <<std::endl;
}


class TestResultFileIndexer : public PixA::ResultFileIndexer
{
public:
  enum EMeasurementType {kScan, kAnalysis};

  TestResultFileIndexer(bool create_connectivity_if_needed) 
    : PixA::ResultFileIndexer(create_connectivity_if_needed),
      m_scanSerialNumber(0),
      m_analysisSerialNumber(0),
      m_verbose(true)
  {}

  void setVerbose(bool verbose) { m_verbose=true; }

  void addPixScanConfig( PixA::SerialNumber_t scan_serial_number,
			 const std::string &folder_name,
			 PixA::ConfigHandle &/*pix_scan_config*/) {
    if (m_verbose) {
      std::cout << "INFO [TestResultFileIndexer::addPixScanConfig] New pix scan config for S" << scan_serial_number << " : " << folder_name << "." << std::endl;
    }
  }


  PixA::SerialNumber_t allocateScanSerialNumber(const std::string &file_name, const PixA::ConfigHandle &/*scan_config*/, bool matches_pattern) {
    PixA::SerialNumber_t a_serial_number = static_cast<PixA::SerialNumber_t>(currentScanNumber());
    if (a_serial_number==0) {
      a_serial_number = ++m_scanSerialNumber;
    }
    if (matches_pattern) {
      createData(kScan, a_serial_number, file_name);
    }
    else if (m_verbose) {
      std::cout << "INFO [TestResultFileIndexer::allocateScanSerialNumber] S" << a_serial_number << ": " << file_name << " does not match the patterns." << std::endl;
    }

    return a_serial_number;
  }


  /** Will be called if a new serial number is needed.
     * Can be used to also write analysis meta data.
     */
  PixA::SerialNumber_t allocateAnalysisSerialNumber(PixA::SerialNumber_t scan_serial_number, bool matches_pattern) {
    ++m_analysisSerialNumber;
    if (matches_pattern) {
      const ScanData_t &scan_data=data(kScan,scan_serial_number);

      createData(kAnalysis, m_analysisSerialNumber, scan_data.fileName() );
    }
    else if (m_verbose) {
      std::cout << "INFO [TestResultFileIndexer::allocateScanSerialNumber] S" << m_analysisSerialNumber << " does not match the patterns." << std::endl;
    }

    return m_analysisSerialNumber;
  }


  /** Called for plain histograms assigned to an analysis.
   */
  void addAnalysisHisto(PixA::SerialNumber_t analysis_serial_number,
			const std::string &rod_name,
			const std::string &folder_name, 
			const std::string &histo_name,
			const PixA::HistoHandle &histo_handle) {
    if (m_verbose) {
      std::cout << "INFO [TestResultFileIndexer::addAnalysisHisto] A" << std::setw(9) << std::setfill('0') << analysis_serial_number
		<< " ROD= " << std::setfill(' ') << std::setw(strlen("RDO_C1_S12")) << rod_name
		<< " : " << folder_name << " / " << histo_name
		<< std::endl;
    }
    data(kAnalysis,analysis_serial_number).addRodHisto(rod_name, histo_handle);
  }

  /** Called for plain histograms assigned to a scan.
   */
  void addScanHisto(PixA::SerialNumber_t scan_serial_number,
		    const std::string &rod_name,
		    const PixA::ScanConfigRef &scan_config,
		    const std::string &folder_name,
		    const std::string &histo_name,
		    const PixA::HistoHandle &histo_handle) {
    if (m_verbose) {
      std::cout << "INFO [TestResultFileIndexer::addScanHisto] S" << std::setw(9) << std::setfill('0') << scan_serial_number
		<< " ROD= " << std::setfill(' ') << std::setw(strlen("RDO_C1_S12")) << rod_name
		<< " : " << folder_name << " / " << histo_name
		<< std::endl;
    }
    data(kScan,scan_serial_number).addRodHisto(rod_name, histo_handle);
  }

  /** Called for pix scan histograms assigned to a scan.
   */
  void addScanHisto(PixA::SerialNumber_t scan_serial_number,
		    const std::string &rod_name,
		    const std::string &module_name,
		    const PixA::ScanConfigRef &scan_config,
		    const std::string &folder_name,
		    const std::string &histo_name,
		    const PixA::HistoHandle &histo_handle) {
    if (m_verbose) {
      std::cout << "INFO [TestResultFileIndexer::addScanHisto] S" << std::setw(9) << std::setfill('0') << scan_serial_number
		<< " ROD= " << std::setfill(' ') << std::setw(strlen("RDO_C1_S12")) << rod_name
		<< " Module= " << std::setfill(' ') << std::setw(20) << module_name
		<< " : " << folder_name << " / " << histo_name
		<< std::endl;
    }
    data(kScan,scan_serial_number).addModuleHisto(module_name, histo_handle);
  }

  void showResults() const {
    for (unsigned int type_i=0; type_i<=kAnalysis; type_i++) {
      for (std::map<PixA::SerialNumber_t, ScanData_t>::const_iterator data_iter = m_data[type_i].begin();
	   data_iter != m_data[type_i].end();
	   data_iter++) {
	std::cout << (type_i==kAnalysis ? 'A' : 'S') << std::setw(9) << std::setfill('0') << data_iter->first << " : " << std::setfill(' ') 
		  <<  (data_iter->second.isUsed() ? "Used " : "Empty" )
		  <<  (data_iter->second.hasProblems() ? "Problems" : "Okay   " )
		  << " : "<< data_iter->second.fileName() << std::endl;
      }
      std::cout << std::endl;
    }
  }

private:
  unsigned int m_scanSerialNumber;
  unsigned int m_analysisSerialNumber;

  class ScanData_t {
  public:
    enum EHistoListType {kModuleHistoList, kRodHistoList };
    ScanData_t(const std::string &file_name) : m_fileName(file_name) {}

    void addModuleHisto( const std::string &module_name, const PixA::HistoHandle &handle ) {
      if (handle) {
	m_histoList[kModuleHistoList][module_name]++;
      }
    }

    void addRodHisto( const std::string &rod_name, const PixA::HistoHandle &handle ) {
      if (handle) {
	m_histoList[kRodHistoList][rod_name]++;
      }
    }

    void verify() {

      //@todo use connectivity and add all RODs / Modules for which there are no histograms ?
      for (unsigned int type_i=0; type_i<=kModuleHistoList; type_i++) {
	unsigned int n_histos=0;
	for (std::map<std::string, unsigned>::const_iterator module_iter = m_histoList[type_i].begin();
	     module_iter != m_histoList[type_i].end();
	     module_iter++) {
	  if (module_iter->second > n_histos) n_histos = module_iter->second;
	}
	for (std::map<std::string, unsigned>::const_iterator module_iter = m_histoList[type_i].begin();
	     module_iter != m_histoList[type_i].end();
	   module_iter++) {
	  if (module_iter->second != n_histos) {
	    m_problems.insert(module_iter->first);
	  }
	}
      }

    }

    bool isUsed() const { return !m_histoList[0].empty() || !m_histoList[1].empty(); }
    bool hasProblems() const { return m_problems.size()>0; }

    std::set<std::string>::const_iterator beginProblems() const { return m_problems.begin(); }
    std::set<std::string>::const_iterator endProblems()   const { return m_problems.end(); }

    const std::string &fileName() const { return m_fileName; }

  private:
    std::string m_fileName;
    //    PixA::Connectivity m_conn;
    std::map<std::string,unsigned int> m_histoList[2];
    std::set<std::string> m_problems;
  };

  ScanData_t &data(EMeasurementType type, PixA::SerialNumber_t serial_number) {
    assert(type <=kAnalysis);
    std::map<PixA::SerialNumber_t, ScanData_t>::iterator data_iter = m_data[type].find(serial_number);
    assert (data_iter != m_data[type].end());
    return data_iter->second;
  };

  ScanData_t &createData(EMeasurementType type, PixA::SerialNumber_t serial_number, const std::string &file_name)
  {
    assert(type <=kAnalysis);
    std::pair< std::map<PixA::SerialNumber_t, ScanData_t>::iterator, bool> ret = m_data[type].insert(std::make_pair(serial_number, ScanData_t(file_name)));
    assert ( ret.second );

    return ret.first->second;
  };


  std::map<PixA::SerialNumber_t, ScanData_t> m_data[2];
  bool m_verbose;
};


int main(int argc, char **argv)
{
  if (argc<2 ) {
    help((argc>0 ? argv[0] : "?"));
    return -1;
  }

  int arg_i=1;
  bool ignore_conn=false;
  bool list_files=false;
  bool quiet=false;
  std::vector<std::string> pattern;

  while (arg_i<argc && argv[arg_i] && argv[arg_i][0]=='-') {
    if (strcmp(argv[arg_i],"-i")==0) {
      ignore_conn=true;
    }
    else if (strcmp(argv[arg_i],"-l")==0) {
      list_files=true;
    }
    else if (strcmp(argv[arg_i],"-e")==0  && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      ++arg_i;
      pattern.push_back(argv[arg_i]);
    }
    else if (strcmp(argv[arg_i],"-q")==0) {
      quiet=true;
    }
    else {
      help((argc>0 ? argv[0] : "?"));
      return -1;
    }
    ++arg_i;
  }
  if (arg_i>=argc) {
    help((argc>0 ? argv[0] : "?"));
    return -1;
  }
  try {

    TestResultFileIndexer indexer(false);
    if (quiet) indexer.setVerbose(false);

    // to test whether protect  and strip work
    //    std::cout << " protect = " << indexer.protectFileName("hallo:/test::all::a.root") << std::endl;
    //    std::cout << " strip = " << indexer.stripFileName("hallo/test:all:a.root::label-/folder/phase1","label-") << std::endl;

    for (std::vector<std::string>::const_iterator pattern_iter = pattern.begin();
	 pattern_iter != pattern.end();
	 ++pattern_iter) {
      std::cout << "INFO [main] only consider histograms of the directories which match \"" << *pattern_iter << "\"" << std::endl;
      indexer.addPattern(*pattern_iter);
    }

    for (; arg_i<argc; arg_i++) {
      //  if (stream_all) {
      std::string file_name;
      try {
	bool labels_follow=false;
	bool first=true;
	std::vector<std::string> label;
	if (!labels_follow) {
	  for(; arg_i<argc; arg_i++) {
	    std::string a_name=argv[arg_i];
	    if (a_name.find(".root")==std::string::npos) {
	      label.push_back(a_name);
	    }
	    else {
	      if (label.empty() && first) {
		labels_follow=true;
	      }
	      file_name = a_name;
	      break;
	    }
	  }
	  first=false;
	}

	if (!file_name.empty()) {
	  PixA::ScanResultStreamer streamer(file_name);
	  
	  indexer.setVerbose(true);
	  streamer.setListener(indexer);
	  if (ignore_conn) {
	    streamer.setIgnoreConnAndCfg(true);
	  }
	  if (labels_follow) {
	    for(; arg_i<argc; arg_i++) {
	      std::string a_name=argv[arg_i];
	      if (a_name.find(".root")==std::string::npos) {
		label.push_back(a_name);
	      }
	      else {
		break;
	      }
	    }
	  }
	  if (list_files) {
	    streamer.readLabels();
	  }
	  else {
	    if (label.empty()) {
	      streamer.read();
	    }
	    else {
	      for (std::vector<std::string>::const_iterator label_iter = label.begin();
		   label_iter != label.end();
		   label_iter++) {
		streamer.readLabel(*label_iter);
	      }
	    }
	  }
	}
      }
      catch (PixA::ConfigException &err) {
	std::cout << "FATAL [main] " << file_name << " : caught exception (ConfigException) : " << err.getDescriptor() << std::endl;
      }
      catch (PixA::BaseException &err) {
	std::cout << "FATAL [main] " << file_name << " : caught exception (BaseException) : " << err.getDescriptor() << std::endl;
      }
      catch (std::exception &err) {
	std::cout << "FATAL [main] " << file_name << " : caught exception (std::exception) : " << err.what() << std::endl;
      }
      catch (...) {
	std::cout << "FATAL [main] " << file_name << " : unhandled exception. "  << std::endl;
      }
    }
    //   else {
    //     PixA::ScanResultStreamer streamer(argv[arg_i++]);
    //     if (ignore_conn) {
    //       streamer.setIgnoreConnAndCfg(true);
    //     }
    //     streamer.setListener(new ScanResultSpy);
    //     if (arg_i==argc) {
    //       streamer.readLabels();
    //     }
    //     else {
    //       for (; arg_i<argc; arg_i++) {
    // 	streamer.readLabel(argv[arg_i]);
    //       }
    //     }
    //   }
    indexer.showResults();
  }
  catch (PixA::ConfigException &err) {
    std::cout << "FATAL [main] caught exception (ConfigException) : " << err.getDescriptor() << std::endl;
  }
  catch (PixA::BaseException &err) {
    std::cout << "FATAL [main] caught exception (BaseException) : " << err.getDescriptor() << std::endl;
  }
  catch (std::exception &err) {
    std::cout << "FATAL [main] caught exception (std::exception) : " << err.what() << std::endl;
  }
  catch (...) {
    std::cout << "FATAL [main] unhandled exception. "  << std::endl;
    throw;
  }


}
