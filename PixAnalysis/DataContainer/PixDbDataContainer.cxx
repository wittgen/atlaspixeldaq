#include "HistoInfo_t.h"
#include "PixDbDataContainer.h"

#include "FitClass.h"

#include <TGraphErrors.h>
#include <TF1.h>

#include <RootDb/RootDb.h>

#ifndef _WINDOWS
#include <PixModule/PixModule.h>
#include <PixBoc/PixBoc.h>
#endif

#include <ConfigWrapper/ConfObjUtil.h>
#include "HistoUtil.h"

#define NDBG_PRNT 0

//using namespace PixLib;

namespace PixA {

// temporary
double TOTFunc(double *x, double *par){
  double denom = par[3]+x[0]*par[4]+par[5]*x[0]*x[0]+par[6]*x[0]*x[0]*x[0]  + par[2];
  if(denom!=0)
    return par[0]+par[1]/denom;
  else
    return 0;
}



PixDbDataContainer::PixDbDataContainer(const PixA::ScanConfigRef &scan_config,
				       const PixA::HistoHandle &histo)
  : m_scanConfig(scan_config),
    m_histo(histo)
{ init(); }


void PixDbDataContainer::init()
{
  m_graph = 0;
  m_1dHisto = 0;
  m_2dHisto = 0;
  m_pm = 0;
  m_pb = 0;
  m_fitClass = 0;
  m_oldChiHisto = 0;
  m_modID=-1;
  m_moduleNaming=kUnknownModuleNaming;

  //  m_modID=confVal<int>(mod_conf.ref()["general"]["ModuleId"]);

}

PixDbDataContainer::~PixDbDataContainer()
{
  delete m_1dHisto;
  delete m_2dHisto;
  delete m_pm;
  delete m_pb;
  delete m_fitClass;
  delete m_oldChiHisto;
}

int PixDbDataContainer::getScanLevel()
{
  int nloops=0;
  if (m_scanConfig.hasPixScanConfig()) {
  ConfigRef scan_conf = m_scanConfig.pixScanConfig();
  try {
    for(nloops=0;nloops<3;nloops++){
      std::stringstream lnum;
      lnum << nloops;
      if(!confVal<bool>(scan_conf["loops"]["activeLoop_"+lnum.str()]))
	break;
    }
  }
  catch (PixA::ConfigException &err) {
    std::cerr << "ERROR [PixDbDataContainer::getScanLevel] exception: " << err << std::endl;
  }
  catch (...) {
  }
  }
  return nloops;
}

int PixDbDataContainer::getNevents()
{
  int nevts = -1;
  if (m_scanConfig.hasPixScanConfig()) {
  ConfigRef scan_conf = m_scanConfig.pixScanConfig();
  try {
    nevts = confVal<int>(scan_conf["general"]["repetitions"]);
  }
  catch (PixA::ConfigException &err) {
    std::cerr << "ERROR [PixDbDataContainer::getNevents] exception: " << err << std::endl;
  }
  catch (...) {
  }
  }
  return nevts;
}


int PixDbDataContainer::getScanSteps(int loopLevel)
{
  int scan_steps = -1;
  if (m_scanConfig.hasPixScanConfig()) {
  ConfigRef scan_conf = m_scanConfig.pixScanConfig();

  std::stringstream lnum;
  lnum << loopLevel;
  try {
    scan_steps = confVal<int>(scan_conf["loops"]["loopVarNStepsLoop_"+lnum.str()]);
  }
  catch (PixA::ConfigException &err) {
    std::cerr << "ERROR [PixDbDataContainer::getScanSteps] exception: " << err << std::endl;
  }
  catch (...) {
  }
  }
  return scan_steps;
}



int PixDbDataContainer::getScanStart(int loopLevel)
{
  //@todo why does this function return an int ?
  float scan_start = -1;
  if (m_scanConfig.hasPixScanConfig()) {
  ConfigRef scan_conf = m_scanConfig.pixScanConfig();

  std::stringstream lnum;
  lnum << loopLevel;
  try {
    scan_start = confVal<float>(scan_conf["loops"]["loopVarMinLoop_"+lnum.str()]);
  }
  catch (PixA::ConfigException &err) {
    std::cerr << "ERROR [PixDbDataContainer::getScanStart] exception: " << err << std::endl;
  }
  catch (...) {
  }
  }
  return static_cast<int>(scan_start);
}


int PixDbDataContainer::getScanStop(int loopLevel)
{
  // @todo why does it return an int
  std::stringstream lnum;
  lnum << loopLevel;

  //  bool valFree = false;
  if (m_scanConfig.hasPixScanConfig()) {
  ConfigRef scan_conf = m_scanConfig.pixScanConfig();
  try {
    if (confVal<bool>(scan_conf["loops"]["loopVarValuesFreeLoop_"+lnum.str()])) {
      int scan_steps = confVal<int>(scan_conf["loops"]["loopVarNStepsLoop_"+lnum.str()]);
      float scan_start = confVal<float>(scan_conf["loops"]["loopVarMinLoop_"+lnum.str()]);

      return static_cast<int>( scan_start+scan_start/abs(scan_start)*scan_steps );

    }
    else {
      return  static_cast<int>( confVal<float>(scan_conf["loops"]["loopVarMaxLoop_"+lnum.str()]) );
    }
  }
  catch (PixA::ConfigException &err) {
    std::cerr << "ERROR [PixDbDataContainer::getScanStop] exception: " << err << std::endl;
  }
  catch (...) {
  }
  }
  return -1;
}

std::string PixDbDataContainer::getScanPar(int loopLevel)
{
  static std::string ret_string="unknow par";
  if (m_scanConfig.hasPixScanConfig()) {
  try {
    ConfigRef scan_conf = m_scanConfig.pixScanConfig();
    std::stringstream lnum;
    lnum << loopLevel;
    ret_string = confVal<std::string>(scan_conf["loops"]["paramLoop_"+lnum.str()]);
  }
  catch (PixA::ConfigException &err) {
    std::cerr << "ERROR [PixDbDataContainer::getScanPar] exception: " << err << std::endl;
  }
  catch (...) {
  }
  }
  return ret_string;
}

bool PixDbDataContainer::haveHistoType(PixLib::PixScan::HistogramType type)
{
  
  if(type>=PixLib::PixScan::MAX_HISTO_TYPES) return false;
  std::string name=getHistoName(type);
  if(name=="none") return false;
  if (m_scanConfig.hasPixScanConfig()) {
  try {
    ConfigRef scan_conf = m_scanConfig.pixScanConfig();

    if (   confVal<bool>(scan_conf["histograms"]["histogramFilled"+name])
	&& confVal<bool>(scan_conf["histograms"]["histogramKept"+name])) {
      return true;
    }
  }
  catch (PixA::ConfigException &err) {
    std::cerr << "ERROR [PixDbDataContainer::haveHistoType] exception: " << err << std::endl;
  }
  catch (...) {
  }
  }
  return false;
}

std::map< PixLib::PixScan::HistogramType , std::string> PixDbDataContainer::s_histoTypeToName;
std::string PixDbDataContainer::s_unknownTypeName="none";

void PixDbDataContainer::initHistoTypeNameList() {
  //  if (s_histoTypeToName.empty()) {
  s_histoTypeToName.clear();
  std::unique_ptr<PixLib::PixScan> ps(new PixScan());
  for(std::map<std::string, int>::iterator it = ps->getHistoTypes().begin();
      it != ps->getHistoTypes().end(); 
      it++){
    s_histoTypeToName.insert(std::make_pair(static_cast<PixLib::PixScan::HistogramType>(it->second),it->first));
  }
  //  }
}

const std::string &PixDbDataContainer::getHistoName(PixLib::PixScan::HistogramType type)
{
  if (s_histoTypeToName.empty()) initHistoTypeNameList();
  std::map< PixLib::PixScan::HistogramType , std::string>::const_iterator iter = s_histoTypeToName.find(type);
  if (iter != s_histoTypeToName.end()) {
    return iter->second;
  }
  return s_unknownTypeName;
}

TH2F* PixDbDataContainer::GetMap(int chip, int in_type, int scanpt)
{
  // clean up old temp. histo first
  if(m_2dHisto!=0) m_2dHisto->Delete();
  m_2dHisto=0;
  if(m_1dHisto!=0) m_1dHisto->Delete();
  m_1dHisto=0;

  PixLib::PixScan::HistogramType type = (PixLib::PixScan::HistogramType) in_type;

  if(type<0 || type>=PixLib::PixScan::MAX_HISTO_TYPES)
    return 0;
  if (!m_scanConfig.hasPixScanConfig()) return NULL;
  // get basic info about scan
  //PixLib::Config &conf = getScanConfig();
  ConfigRef scan_conf = m_scanConfig.pixScanConfig();

  std::string lname = "loopVarValuesLoop_0";
  int id[4] = {0,0,0,0}, incrid=0;
  if(scanpt>=0)
    id[0] = scanpt;
  if(type==PixLib::PixScan::SCURVE_MEAN || type==PixLib::PixScan::SCURVE_SIGMA || type==PixLib::PixScan::SCURVE_CHI2){
    id[0] = -1;
    if(scanpt>=0)
      id[1] = scanpt;
    incrid=1;
    lname = "loopVarValuesLoop_1";
  }

  const std::vector<float> &scanval = confVal< std::vector<float> >(scan_conf["loops"][lname]);
  int nsteps=scanval.size();
  if(getScanLevel()==0) nsteps=1;
  if(nsteps<1) nsteps=1;

  int nmin=0, nmax=nsteps;
  if(scanpt>=0){
    nmin = scanpt;
    nmax = scanpt+1;
  }

  // get histogram for module
  try{

    // dirty trick to keep the file open
    HistoRef histo( m_histo.ref() );

    for(int nsc=nmin;nsc<nmax;nsc++){
      std::unique_ptr<PixLib::Histo> h( readHisto(type,id) );

      id[incrid]++;
      // some sanity checks
      if(h.get()!=0){
	if(h->nDim()!=2){
	  return 0;
	}
	// turn PixLib::Histo into ROOT TH2F
	if(m_2dHisto==0){
	  gROOT->cd();
	  m_2dHisto = new TH2F(h->name().c_str(),h->title().c_str(),
			       h->nBin(0),h->min(0), h->max(0),h->nBin(1),h->min(1), h->max(1));
	}
	int i,j;
	for (i=0; i<h->nBin(0); i++) {
	  for (j=0; j<h->nBin(1); j++) {
	    float cont = m_2dHisto->GetBinContent(i+1,j+1) + (float)(*h)(i,j);
	    m_2dHisto->SetBinContent(i+1,j+1,cont);
	  }
	}
      }
    }
  }
  catch(PixA::ConfigException &err ) {
    err.what(std::cerr); std::cerr << std::endl;
    return 0;
  }
  catch(PixLib::PixDBException &err ) {
    err.what(std::cerr); std::cerr << std::endl;
    return 0;
  }
  catch(...) {
    return 0;
  }

  return getChipMap(chip);
}

TGraphErrors* PixDbDataContainer::GetScanHi(int chip, int col, int row, int in_type, int scanpt)
{
  delete m_graph; m_graph=0;

  PixLib::PixScan::HistogramType type = (PixLib::PixScan::HistogramType) in_type;
  if(type<0 || type>=PixLib::PixScan::MAX_HISTO_TYPES)
    return 0;

  if(getScanLevel()==0) return 0; // can only display scan data

  PixLib::Config &conf = getScanConfig();

  std::string lname = "loopVarValuesLoop_0";
  int id[4] = {0,0,0,0}, incrid=0;
  if(scanpt>=0) id[0] = scanpt;
  if(type==PixLib::PixScan::SCURVE_MEAN || type==PixLib::PixScan::SCURVE_SIGMA || type==PixLib::PixScan::SCURVE_CHI2){
    id[0] = -1;
    incrid=1;
    lname = "loopVarValuesLoop_1";
  }
  //  std::vector<float> &scanval = ((ConfVector&)conf["loops"][lname]).valueVFloat();
  std::vector<float> &scanval = confVal<std::vector<float> >( conf["loops"][lname]);

  //  double nevts=(double)((ConfInt&)conf["general"]["repetitions"]).getValue();
  double nevts=/*static_cast<double>*/ (confVal<int>(conf["general"]["repetitions"]));

  double *xpts, *ypts, *xepts, *yepts;
  xpts  = new double[scanval.size()];
  ypts  = new double[scanval.size()];
  xepts = new double[scanval.size()];
  yepts = new double[scanval.size()];
  int npts=0;
  // get histogram for module
  try{

    // dirty trick to keep the file open
    HistoRef histo=m_histo.ref();

    for(int nsc=0;nsc<(int)scanval.size();nsc++){
      xpts[npts]  = (double)scanval[nsc];
      xepts[npts] = 1e-4*(double)scanval[nsc];
      std::unique_ptr<PixLib::Histo> h(readHisto(type,id));
      if(h.get()!=0){
	float x,y;
	PixXY(chip,col,row,&x,&y);
	ypts[npts]  = (double)(*h)((int)x,(int)y);
	switch(type){
	case PixLib::PixScan::OCCUPANCY: // binomial error
	  if(nevts>0)
	    yepts[npts] = ypts[npts]/nevts*(nevts-ypts[npts]);
	  else
	    yepts[npts] = ypts[npts];
	  if(yepts[npts]>0)       
	    yepts[npts] = sqrt(yepts[npts]);
	  break;
	default: // don't know what to do - have no error
	  yepts[npts] = 1e-4*ypts[npts];
	}
	npts++;
      }
      id[incrid]++;
    }
  }catch(...){
  }
  if(npts>0){
    m_graph = new TGraphErrors(npts, xpts, ypts, xepts, yepts);
    m_graph->GetXaxis()->SetTitle("Scan parameter");
    m_graph->SetMarkerStyle(20);
    m_graph->SetMarkerSize(.6f);
  }
  delete xpts;
  delete ypts;
  delete xepts;
  delete yepts;

  return m_graph;
}

PixLib::Config& PixDbDataContainer::getScanConfig()
{
  return m_ps->config();
}

PixLib::Config& PixDbDataContainer::getModConfig()
{
  static PixLib::Config cfg("blabla");
  PixLib::Config *retcfg = &cfg;
  return *retcfg;
}
PixLib::Config& PixDbDataContainer::getBocConfig()
{
  static PixLib::Config cfg("blabla");
  PixLib::Config *retcfg = &cfg;
  return *retcfg;
}

void PixDbDataContainer::makeHistoName(PixLib::PixScan::HistogramType type, std::vector<std::string> &histo_name)
{
  histo_name.push_back(getHistoName(type));
  if (m_moduleNaming!= kOldModuleNaming) {
    histo_name.push_back(m_scanConfig.connName());
    if (m_moduleNaming == kUnknownModuleNaming) {
      HistoRef histo_ref(m_histo.ref());
      if (histo_ref.haveHisto(histo_name)) {
	m_moduleNaming=kNewModuleNaming;
      }
      else {
	histo_name.pop_back();
      }
    }
  }

  if (histo_name.size()==1) {
    if (m_modID<0) {
      if (m_scanConfig.hasModConfig()) {
	try {
	  m_modID=confVal<int>(m_scanConfig.modConfig()["general"]["ModuleId"]);
	}
	catch (PixA::ConfigException &err) {
	  std::cerr << "ERROR [PixDbDataContainer::makeHistoName] exception: " << err << std::endl;
	}
	catch(...) {
	}
      }
    }
    std::stringstream a_name;
    a_name << "Mod" << m_modID;
    histo_name.push_back(a_name.str());
    if (m_moduleNaming == kUnknownModuleNaming) {
      HistoRef histo_ref(m_histo.ref());
      if (histo_ref.haveHisto(histo_name)) {
	m_moduleNaming=kOldModuleNaming;
      }
    }
  }
}

PixLib::Histo* PixDbDataContainer::readHisto(PixLib::PixScan::HistogramType type, const int id[4] ) 
{
  std::vector<std::string> histo_name;
  HistoRef histo_ref(m_histo.ref());
  makeHistoName(type,histo_name);
  return histo_ref.histo(histo_name,id);
}

bool PixDbDataContainer::numberOfHistos(PixLib::PixScan::HistogramType type, HistoInfo_t &info ) {
  std::vector<std::string> histo_name;
  HistoRef histo_ref(m_histo.ref());
  makeHistoName(type,histo_name);
  return histo_ref.numberOfHistos(histo_name,info);
}

PixLib::Histo* PixDbDataContainer::getGenericPixLibHisto(int type, int scanpt, int scan_level)
{
  if(type<0)
    return 0;
  if (type>PixLib::PixScan::MAX_HISTO_TYPES) return 0;

  int ftype = (int)type - (int) PixLib::PixScan::MAX_HISTO_TYPES;
  PixLib::Histo *fh=0;
  if(ftype>=0 && (fh=getParHisto(ftype))!=0)
    return fh;
  else if (type>=PixLib::PixScan::MAX_HISTO_TYPES)
    return 0;

  // get basic info about scan
  int id[4];
  int scanl = scan_level+1;
  if(scanl<0)
    scanl = getScanLevel();

  // get histogram for module

  std::unique_ptr<PixLib::Histo> h;

  id[3]=0;
  for(int nlvl=2;nlvl>=0; nlvl--){
    for(int k=0;k<3;k++){
      if(k<nlvl)
	id[k] = -1;
      else if (k==nlvl){
	if((scanl-1)==nlvl && scanpt>=0)
	  id[k] = scanpt;
	else
	  id[k] = 0;
      } else
	id[k] = 0;
    }
    try{
      h = std::unique_ptr<PixLib::Histo>( readHisto(static_cast<PixLib::PixScan::HistogramType>(type),id));
    }
    catch(...) {
      h.reset();
    }
    if(h.get()!=0) break; // found something, process now
  }

  if(h.get()==0 && scan_level<0){ // might be end-of scan result, try somewhere else
    for(int nlvl=2;nlvl>=0; nlvl--){
      for(int k=0;k<3;k++){
	if(k<nlvl)
	  id[k] = -1;
	else
	  id[k] = 0;
      }
      try {
	h = std::unique_ptr<PixLib::Histo>( readHisto(static_cast<PixLib::PixScan::HistogramType>(type),id) );
      }
      catch(...){
	h.reset();
      }
      if(h.get()!=0) break; // found something, process now
    }
  }
  return h.release();
}

PixLib::Histo* PixDbDataContainer::getGenericPixLibHisto(PixLib::PixScan::HistogramType type, const int idx[4])
{
  if(type<0)
    return 0;

  int ftype = (int)type - (int) PixLib::PixScan::MAX_HISTO_TYPES;
  PixLib::Histo *fh=0;
  if(ftype>=0 && (fh=getParHisto(ftype))!=0)
    return fh;
  else if (type>=PixLib::PixScan::MAX_HISTO_TYPES)
    return 0;

  std::unique_ptr<PixLib::Histo> h;
  // get histogram for module
  try{
    h = std::unique_ptr<PixLib::Histo>( readHisto(type,idx) );
  }catch(...){
    h.reset();
  }
  return h.release();
}

TH2F* PixDbDataContainer::getDACMap(int in_chip, const char *in_type)
{
  // clean up old temp. histo first
  //  if(m_2dHisto!=0) m_2dHisto->Delete();
  //  m_2dHisto=0;
  //  if(m_1dHisto!=0) m_1dHisto->Delete();
  //  m_1dHisto=0;

  std::string dac_name(in_type);
  if (!m_scanConfig.hasModConfig()) return NULL;

  PixA::ConfigRef mod_config ( m_scanConfig.modConfig() );
  TH2 *h2 = PixA::getDACMap<TH2>(in_chip,dac_name,mod_config);
  if (!h2) return NULL;

  h2->SetBit(kCanDelete);
  assert ( h2->InheritsFrom(TH2F::Class()));
  return static_cast<TH2F *>(h2);
}

TH2F* PixDbDataContainer::getChipMap(int chip){
//   if(chip<0) // full module, we're done
//     return m_2dHisto;
//   if(m_2dHisto==0) // nothing to do
//     return 0;
//   else{  // have to extract chip part from module map
//     char hname[100], htitle[200];
//     sprintf(hname,"%s%d",m_2dHisto->GetName(),chip);
//     sprintf(htitle,"%s chip %d",m_2dHisto->GetTitle(),chip);
//     gROOT->cd();
//     TH2F *retmap = new TH2F(hname,htitle,NCOL,-0.5,NCOL-0.5,NROW,-0.5,NROW-0.5);
//     for(int i=0;i<NCOL;i++){
//       for(int j=0;j<NROW;j++){
// 	float xval, yval;
//         PixXY(chip,i,j,&xval,&yval);
//         retmap->Fill(i,j,(float) 
// 		     m_2dHisto->GetBinContent(m_2dHisto->GetBin(1+(int)xval,1+(int)yval)));
//       }
//     }
//     delete m_2dHisto;
//     m_2dHisto = retmap;
//     return m_2dHisto;
//   }
  TH2 *h2=chipMap<TH2,TH2>(m_2dHisto,chip);
  if (!h2) return NULL;

  assert (h2->InheritsFrom(TH2F::Class()) );
  h2->SetBit(kCanDelete);
  return static_cast<TH2F*>(h2);
}
void PixDbDataContainer::fitHisto(int fittype, int chip, float chicut, float xmin, float xmax, float fracErr, bool useNdof)
{
  std::stringstream ss;
  ss << chip;
  if(chip<-1 || chip>15)
    throw SctPixelRod::BaseException("PixDbDataContainer::FitHisto : Chip-ID must be between -1 and 15, but is "+ss.str()+".");

  int k, minChip=0, maxChip=16, npar = m_fitClass->getNPar(fittype)+4*(int)m_fitClass->hasVCAL(fittype);
  if(chip>-1){
    minChip = chip;
    maxChip = chip+1;
  }
  double *x, *y, *xerr, *yerr, *pars;
  bool *pfix;
  x    = new double[m_varValues.size()];
  y    = new double[m_varValues.size()];
  xerr = new double[m_varValues.size()];
  yerr = new double[m_varValues.size()];
  pars = new double[npar];
  pfix = new bool[npar];
  for(k=0;k<(int)m_varValues.size();k++){
    x[k]    = (double)m_varValues[k];
    xerr[k] = 1e-5*(double)m_varValues[k];
  }
  
  double ymax = 0;
  bool Clow = true;
  // check if binomial errors are needed, and if so get #injections
  Config &scfg = getScanConfig();
  if(scfg["fe"]["chargeInjCapHigh"].name()!="__TrashConfObj__")
    Clow = !scfg["fe"]["chargeInjCapHigh"].valueBool();
  if(fracErr==0 && m_errHistos.size()==0){
    if(scfg["general"]["repetitions"].name()!="__TrashConfObj__")
      ymax = (double) scfg["general"]["repetitions"].valueInt();
  }

  int count = 0;
  for(int c=minChip;c<maxChip;c++){
    
    if(m_fitClass->hasVCAL(fittype)){
      // set VCAL calibration parameters
      std::stringstream a;
      a << c;
      Config &mcfg = getModConfig().subConfig("PixFe_"+a.str());
      if(mcfg["Misc"].name()!="__TrashConfObj__"){
	std::string cname = "CInjLo";
	if(!Clow) cname = "CInjHi";
	if(mcfg["Misc"][cname].name()=="__TrashConfObj__")
	  throw SctPixelRod::BaseException("PixDbDataContainer::FitHisto : Can't find "+cname+" in config of FE "+a.str());
	float cap = ((ConfFloat&)mcfg["Misc"][cname]).value();
	cap /= 0.160218; // conversion C -> e
	for(int j=0;j<4;j++){
	  std::stringstream b;
	  b << j;
	  if(mcfg["Misc"]["VcalGradient"+b.str()].name()=="__TrashConfObj__")
	    throw SctPixelRod::BaseException("PixDbDataContainer::FitHisto : Can't find VcalGradient"+b.str()+" in config of FE "+a.str());
	  pars[m_fitClass->getNPar(fittype)+j] = cap*(((ConfFloat&)mcfg["Misc"]["VcalGradient"+b.str()]).value());
	  pfix[m_fitClass->getNPar(fittype)+j] = true; // never change VCAL parameters in fit
	}
      }
    }
    for(int row=0;row<160;row++){
      for(int col=0;col<18;col++){
	// re-load non-VCAL parameters
	for(k=0;k<(int)m_parInit.size();k++){
	  pars[k] = m_parInit[k];
	  pfix[k] = m_parFixed[k];
	}
	// fill y-point for this pixel
	float xval, yval;
	PixXY(c,col,row,&xval,&yval);
	for(k=0;k<(int)m_varValues.size();k++){
	  y[k]    = (*(m_fitHistos[k]))((int)xval, (int)yval);
	  if(m_errHistos.size()!=0){
	    yerr[k] = (*(m_errHistos[k]))((int)xval, (int)yval);
	  }else if(fracErr)
	    yerr[k] = (double)fracErr*(*(m_fitHistos[k]))((int)xval, (int)yval);
	  else // binomial
	    yerr[k] = y[k]/ymax*(ymax-y[k]);
	}
	// run fit
	float chi;
	if(m_oldChiHisto==0 || (m_oldChiHisto!=0 && (*m_oldChiHisto)((int)xval, (int)yval) >= chicut)){
	  if(count<NDBG_PRNT){
	    printf("Fitting %d %d %d\n",chip, col, row);
	    if(m_oldChiHisto!=0)
	      printf("old chi2: %f\n",(*m_oldChiHisto)((int)xval, (int)yval));
	  }
	  chi = (float)m_fitClass->runFit((int)m_varValues.size(), x, y, xerr, yerr, pars, pfix, fittype, 
					  (double)xmin, (double)xmax, false);//(count<NDBG_PRNT));
	  if(count<NDBG_PRNT){
	    if(m_oldChiHisto!=0)
	      printf("new chi2: %f\n",chi);
	  }
	  count++;
	  for(int i=0;i<(int)m_fitClass->getNPar(fittype);i++){
	    if(chi<chicut)
	      (m_parHistos[i])->set((int)xval, (int)yval, pars[i]);
	    else
	      (m_parHistos[i])->set((int)xval, (int)yval, 0);
	  }
	  // convert chi2 to chi2/ndf if requested
	  if(((int)(m_varValues.size())-npar-1)>0 && useNdof)
	    chi  /= (float)((int)(m_varValues.size()-npar)-1);
	  (m_parHistos[m_fitClass->getNPar(fittype)])->set((int)xval, (int)yval, (double)chi);
	} else if(m_oldChiHisto!=0 && (*m_oldChiHisto)((int)xval, (int)yval) < chicut)
	  (m_parHistos[m_fitClass->getNPar(fittype)])->set((int)xval, (int)yval, -999);
      }
    }
  }

  delete[] x;
  delete[] y;
  delete[] xerr;
  delete[] yerr;
  delete[] pars;
  delete[] pfix;
}
void PixDbDataContainer::initFit(PixLib::PixScan::HistogramType type, int loop_level, float *pars, bool *pars_fixed, 
			int fittype, PixLib::PixScan::HistogramType errType, PixLib::PixScan::HistogramType chiType, 
			bool doCalib)
{
  clearFit(true);

  if(m_fitClass == 0)
    m_fitClass = new FitClass();

  int npars = m_fitClass->getNPar(fittype);
  if(npars==0)
    throw SctPixelRod::BaseException("PixDbDataContainer::initFit : Selected fit function is currently NOT supported.");

  if(pars==0)
    throw SctPixelRod::BaseException("PixDbDataContainer::initFit : Initial parameter settings must be provided during fit initialisation.");


  // initialise parameters
  for(int i=0;i<npars;i++){
    m_parInit.push_back(pars[i]);
    m_parFixed.push_back(pars_fixed[i]);
    std::stringstream b;
    b<< i;
    m_parHistos.push_back(new Histo("par"+b.str(),m_fitClass->getFuncName(fittype)+" par. "+b.str(),144,-0.5,143.5,
				    320,-0.5,319.5));
  }
  m_parHistos.push_back(new Histo("CHI2",m_fitClass->getFuncName(fittype)+" chi2",144,-0.5,143.5,320,-0.5,319.5));
  if(m_fitClass->hasVCAL(fittype) && !doCalib){
    m_parInit.push_back(0);
    m_parInit.push_back(1);
    m_parInit.push_back(0);
    m_parInit.push_back(0);
    for(int j=0;j<4;j++)
      m_parFixed.push_back(true);
  }

  // get scan points
  std::stringstream a;
  a << loop_level;
  if(getScanLevel()<=loop_level)
    throw SctPixelRod::BaseException("PixDbDataContainer::initFit : Requested loop level "+a.str()+" was not used in this scan.");
  Config &scfg = getScanConfig();
  std::string name = "loopVarValuesLoop_";
  name += a.str();
  m_varValues.clear();
  if(scfg["loops"][name].name()!="__TrashConfObj__"){
    ConfObj &obj = scfg["loops"][name];
    switch( obj.subtype() ){
    case ConfVector::V_INT:{
      std::vector<int> &tmpVec = ((ConfVector&)obj).valueVInt();
      for(std::vector<int>::iterator IT = tmpVec.begin(); IT!=tmpVec.end(); IT++)
	m_varValues.push_back((float)(*IT));
      break;}
    case ConfVector::V_UINT:{
      std::vector<unsigned int> &tmpVec = ((ConfVector&)obj).valueVUint();
      for(std::vector<unsigned int>::iterator IT = tmpVec.begin(); IT!=tmpVec.end(); IT++)
	m_varValues.push_back((float)(*IT));
      break;}
    case ConfVector::V_FLOAT:{
      std::vector<float> &tmpVec = ((ConfVector&)obj).valueVFloat();
      for(std::vector<float>::iterator IT = tmpVec.begin(); IT!=tmpVec.end(); IT++)
	m_varValues.push_back((float)(*IT));
      break;}
    default:
      throw SctPixelRod::BaseException("PixDbDataContainer::initFit : vector holding scan var. values is of unknown type.");
      break;
    }      
  } else
    throw SctPixelRod::BaseException("PixDbDataContainer::initFit : can't find config for requested loop.");
  
  // get all histograms
  for(int i=0;i<(int)m_varValues.size(); i++){
    Histo *ho = getGenericPixLibHisto(type, i, loop_level);
    if(ho==0)
      throw SctPixelRod::BaseException("PixDbDataContainer::initFit : one of the nec. data histos was not found.");
    else
      m_fitHistos.push_back(new Histo(*ho));
  }
  if(errType<PixLib::PixScan::MAX_HISTO_TYPES){
    for(int i=0;i<(int)m_varValues.size(); i++){
      Histo *ho = getGenericPixLibHisto(errType, i, loop_level);
      if(ho==0)
	throw SctPixelRod::BaseException("PixDbDataContainer::initFit : one of the nec. error histos was not found.");
      else
	m_errHistos.push_back(new Histo(*ho));
    }
  }
  if(chiType<PixLib::PixScan::MAX_HISTO_TYPES){
    Histo *ho = getGenericPixLibHisto(chiType, 0, loop_level+1);
    if(ho==0){
      //      printf("PixDbDataContainer::initFit : chi2 histo %d was not found on level %d.\n", (int)chiType, loop_level+1);
      m_oldChiHisto = 0;
    }else
      m_oldChiHisto = new Histo(*ho);
  }

}
void PixDbDataContainer::clearFit(bool clearParHisto)
{
  for(std::vector<PixLib::Histo*>::iterator it = m_fitHistos.begin(); it!=m_fitHistos.end(); it++){
    delete (*it); (*it)=0;
  }
  m_fitHistos.clear();
  for(std::vector<PixLib::Histo*>::iterator it = m_errHistos.begin(); it!=m_errHistos.end(); it++){
    delete (*it); (*it)=0;
  }
  m_errHistos.clear();
  if(clearParHisto){
    for(std::vector<PixLib::Histo*>::iterator it2 = m_parHistos.begin(); it2!=m_parHistos.end(); it2++){
      delete (*it2); (*it2)=0;
    }
    m_parHistos.clear();
  }
  delete m_fitClass; m_fitClass=0;
  delete m_oldChiHisto; m_oldChiHisto=0;
  m_parInit.clear();
  m_varValues.clear();
}

PixLib::Histo* PixDbDataContainer::getParHisto(int ipar)
{
  if(ipar<0 || ipar>=(int)m_parHistos.size()) return 0;

  return m_parHistos[ipar];
}
TH2F* PixDbDataContainer::getParHistoR(int ipar)
{
  Histo *h = getParHisto(ipar);
  if(h==0) return 0;
  // turn PixLib::Histo into ROOT TH2F
  delete m_2dHisto;
  gROOT->cd();
  m_2dHisto = new TH2F(h->name().c_str(),h->title().c_str(),
		       h->nBin(0),h->min(0), h->max(0),h->nBin(1),h->min(1), h->max(1));

  int i,j;
  for (i=0; i<h->nBin(0); i++) {
    for (j=0; j<h->nBin(1); j++) {
      float cont = m_2dHisto->GetBinContent(i+1,j+1) + (float)(*h)(i,j);
      m_2dHisto->SetBinContent(i+1,j+1,cont);
    }
  }
  return m_2dHisto;
}

//PixLib::DbRecord* PixDbDataContainer::getResultsRecord()
//{
//  DbRecord *scandatRec = openDbFile(1,true);    //this should get us the PixScanData Record
//  DbRecord *retRec = getRecord(scandatRec, "RESULTS","AnalysisResults");
//  delete scandatRec;
//  return retRec;
//}

// PixLib::DbRecord* PixDbDataContainer::getRecord(PixLib::DbRecord *parent, std::string dname, std::string name)
// {
//   std::shared_ptr<PixLib::DbRecord> retRec;
//   dbRecordIterator ri;
//   ri = parent->findRecord(dname+"/"+name);
//   if(ri!=parent->recordEnd())
//     retRec = std::shared_ptr<PixLib::DbRecord> ( *(parent->getDb()->DbProcess(ri, PixLib::PixDb::DBREAD)));
//   else{
//     retRec = std::shared_ptr<PixLib::DbRecord> ( parent->getDb()->makeRecord(name, dname) );
//     retRec = std::shared_ptr<PixLib::DbRecord> (*(parent->getDb()->DbProcess(parent->pushRecord(retRec.get()), PixLib::PixDb::DBREAD)));
//   }
//   return retRec.release();
// }

// void PixDbDataContainer::writePixLibHisto(PixLib::Histo *in_hi, PixLib::PixScan::HistogramType type, int scanpt, int scan_level)
// {
//   std::string rname, hname = getHistoName(type);
//   std::string levLabel[4]={"A","B","C",""};

//   if(in_hi==0) return;

//   // get config and remember for later -> is stored in m_ps
//   getScanConfig();

//   PixLib::DBInquire *scandatInq=0, *topInq=0, *subInq;
//   PixLib::DbRecord *scandatRec=0, *topRec=0, *subRec[4]={0,0,0,0};
//   if(m_isOld){
//     scandatInq = openDBFile(1,true);    //this should get us the PixScanData Inquire
    
//     // create inquire structure for requested level
//     topInq = getInquire(scandatInq, hname, hname);
//     std::stringstream a;
//     a << m_modID;
//     hname = "Mod"+a.str();
//     subInq = getInquire(topInq, hname, hname);
//     for(int i=0;i<(3-scan_level);i++){
//       hname += "_"+levLabel[i]+"0";
//       subInq = getInquire(subInq, hname, hname);
//     }
    
//     // overwrite/create field for histo
//     hname += "_0";
//     PixLib::fieldIterator fit = subInq->findField(hname);
//     PixLib::DBField *hfield;
//     if(fit!=subInq->fieldEnd())
//       hfield = (*fit);
//     else
//       hfield = m_DBfile->makeField(hname);
//     m_DBfile->DBProcess(hfield,COMMIT,*in_hi);
//     if(fit==subInq->fieldEnd())
//       subInq->pushField(hfield);
//     m_DBfile->DBProcess(subInq,COMMITREPLACE);
//   } else{
//     scandatRec = openDbFile(1,true);    //this should get us the PixScanData Inquire

//     // create record structure for requested level
//     topRec = getRecord(scandatRec, hname, hname);
//     std::stringstream a;
//     a << m_modID;
//     hname = "Mod"+a.str();
//     subRec[0] = getRecord(topRec, hname, hname);
//     for(int i=0;i<(3-scan_level);i++){
//       hname += "_"+levLabel[i]+"0";
//       subRec[i+1] = getRecord(subRec[i], hname, hname);
//     }
//     // overwrite/create field for histo
//     hname += "_0";
//     PixLib::dbFieldIterator fit = subRec[3-scan_level]->findField(hname);
//     std::shared_ptr<PixLib::DbField> hfield;
//     if(fit!=subRec[3-scan_level]->fieldEnd())
//       hfield = std::shared_ptr<PixLib::DbField> ( *(m_Dbfile->DbProcess(fit, PixLib::PixDb::DBREAD)));
//     else
//       hfield = std::shared_ptr<PixLib::DbField> ( m_Dbfile->makeField(hname) );
//     m_Dbfile->DbProcess(hfield.get(),PixLib::PixDb::DBCOMMIT,*in_hi);
//     if(fit==subRec[3-scan_level]->fieldEnd())
//       subRec[3-scan_level]->pushField(hfield.release());
//   }

//   // add this histo to config as filled and kept
//   Config &conf = m_ps->config();
//   hname = getHistoName(type);
//   if(conf["histograms"]["histogramFilled"+hname].name()!="__TrashConfObj__")
//     ((ConfBool&)conf["histograms"]["histogramFilled"+hname]).m_value = true;
//   if(conf["histograms"]["histogramKept"+hname].name()!="__TrashConfObj__")
//     ((ConfBool&)conf["histograms"]["histogramKept"+hname]).m_value = true;
//   if(m_isOld){
//     m_ps->writeConfig(scandatInq); // writes to file
//     // close file in write mode, open again in read-only
//     openDBFile(1);
//   } else{
//     m_ps->writeConfig(scandatRec); // writes to file
//     // close file in write mode, open again in read-only
//     openDbFile(1);
//     for(int i=0;i<4;i++) delete subRec[i];
//     delete topRec;
//     delete scandatRec;
//   }
// }

int PixDbDataContainer::getPixScanID(int stdScanID)
{
  switch (stdScanID){
  case  1:
    return (int)PixScan::DIGITAL_TEST;
  case  6:
    return (int)PixScan::THRESHOLD_SCAN;
  case  8:
    return (int)PixScan::CROSSTALK_SCAN;
  case  9:
    return (int)PixScan::TOT_CALIB;
  case 12:
    return (int)PixScan::TIMEWALK_MEASURE;
  case 22:
    return (int)PixScan::INTIME_THRESH_SCAN;
  case 36:
    return (int)PixScan::T0_SCAN;
  }
  return -1;
}

PixLib::Histo* PixDbDataContainer::getGenericPixLibHisto(int type, const int scanpt[4]) {
  if (type>=PixLib::PixScan::MAX_HISTO_TYPES) return 0;
  return getGenericPixLibHisto(static_cast<PixLib::PixScan::HistogramType>(type),scanpt);
}

bool PixDbDataContainer::haveHistoType(int type)
{
  return haveHistoType((PixLib::PixScan::HistogramType) type);
}

}
