#ifndef _IScanResultListener_h_
#define _IScanResultListener_h_

#include <string>
#include <vector>
#include <ConfigWrapper/ScanConfig.h>

namespace PixA {

  class HistoHandle;

  class IScanResultListener
  {
  public:
    virtual ~IScanResultListener() {}

    virtual void newFile( const std::string &name) = 0;

    /** Called for each scan result label.
     * @param name the label name
     * @param label_config a fake config which allows to query the comment, the scan time.
     * The fields can be access like:
     *<pre>
     * #include <ConfigWrapper/ConfObjUtil.h>
     * #include <ConfigWrapper/ConfigHandle.h>
     * // ...
     * std::string comment;
     * std::string time_stamp;
     * unsigned int scan_number=0;
     * if (label_config) {
     *    PixA::ConfigRef label_config_ref = label_config.ref();
     *    try {
     *      comment     = confVal<std::string >( label_config_ref[""]["Comment"]    );
     *      scan_number = confVal<unsigned int>( label_config_ref[""]["ScanNumber"] );
     *      time_stamp  = confVal<std::string >( label_config_ref[""]["TimeStamp"]  );
     *    }
     *    catch (PixA::ConfigException &err)
     *    {
     *      // catch exceptions which are thrown if a field does not exist e.g. ScanNumber
     *    }
     * }
     *</pre>
     */
    virtual void newLabel(const std::string &name, const ConfigHandle &label_config) = 0;

    /** Called whenever the pix module group changes.
     */
    virtual void newPixModuleGroup(const std::string &name) = 0;

    /** Called after processing a pix module group.
     */
    virtual void finishPixModuleGroup() = 0;

    /** Called whenever the Pp0 changes.
     * newPp0 will never be called without newPixModuleGroup being called.
     */
    virtual void newPp0(const std::string &name) = 0;

    /** Called after the last module of a PP0 has been processed.
     */
    virtual void finishPp0() = 0;

    /** Called whenever the result folder changes
     * @param folder_list the names of the folder hierarchy.
     */
    virtual void newFolder(const std::vector<std::string> &folder_hierarchy) = 0;

    /** Called when a new scan info record is found
     * @param scan_info handle for the scan info config object.
     */
    virtual void newScanInfo(const ConfigHandle &scan_info) = 0;

    //  /** Called whenever the scan changes
    //   * @param folder_list the names of the folder hierarchy
    //   */
    //  virtual void newScan(const ScanConfigRef &scan_config) = 0;


    /** Called when a new pix scan config is found.
     * @param pix_scan_config handle for the pix scan config object.
     */
    virtual void newPixScan(const ConfigHandle &pix_scan_config) = 0;


    /** Called for each pix scan histo.
     * @param histo_name the name of the histogram (collection).
     * @param scan_config the module connectivity and production name and a collection of config handles : scam config, module config, boc config.
     * @param db_acccess a database handle for the folder which contains the histogram of the given name.
     */
    virtual void newPixScanHisto(const std::string &histo_name,
				 const ScanConfigRef &scan_config, 
				 const HistoHandle &db_access) = 0;

    /** Called for each normal histo.
     * @param name the name of the histogram (collection).
     * @param histo a database handle for the folder which contains the histogram of the given name.
     * @param pix_module_group_name the connectivity name of the ROD.
     * @param scan_info a handle for the scan info config (may be invalid).
     */
    virtual void newHisto(const std::string &name, 
			  const HistoHandle &histo, 
			  const std::string &pix_module_group_name, 
			  const ConfigHandle &scan_info) = 0;

    /** Called at the end.
     */
    virtual void finish() = 0;

    /** Report a problem to the listener.
     */
    virtual void reportProblem(const std::string &message) = 0;
  };

}
#endif
