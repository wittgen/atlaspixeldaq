#ifndef _HistoRef_h_
#define _HistoRef_h_

#include "IHistoAccessor.h"
#include <memory>

namespace PixA {

  class HistoHandle;

  class HistoRef
  {
    friend class HistoHandle;

  protected:
    HistoRef(const std::shared_ptr<IHistoAccessor> &histo) : m_histo(histo) { init(); }

    void init() {
      if (m_histo.get())
	m_histo->accessStart();
    }

  public:
    HistoRef(const HistoRef &ref) : m_histo(ref.m_histo) { init(); }

    HistoRef &operator=(const HistoRef &ref) { 
      if (m_histo.get()) { m_histo->accessEnd();}
      m_histo=ref.m_histo;

      init();
      return *this;
    }

    ~HistoRef() {
      if (m_histo.get()) {
	m_histo->accessEnd();
      }
    }

    /** Return true if the reference is valid.
     */
    operator bool() const {
      return m_histo.get() != NULL;
    }

    /** Count the number of histograms in the collection of the given name.
     * @param name the name of the histogram collection.
     * @param info will be filled with the maximum number of histograms per level (0-2) or -1 and the number of histograms at deepest level.
     */
    bool numberOfHistos(const std::string &name, HistoInfo_t &info ) {
      return m_histo->numberOfHistos(name,info);
    }

    /** Return true if the histogramm of the given name exists.
     */
    bool haveHisto(const std::string &name) {
      return m_histo->haveHisto(name);
    }

    /** Returns the requested PixLib::Histo of the histogram collection of the given name.
     * @return a pointer to a newly created histogramm or NULL.
     * The caller is responsible for deleting the hisogram.
     */
    PixLib::Histo* histo(const std::string &name, const Index_t &index) {
      return m_histo->histo(name,index);
    }

    /** Returns the requested PixLib::Histo of the histogram collection of the given name (deprecated).
     * @return a pointer to a newly created histogramm or NULL.
     * The caller is responsible for deleting the hisogram.
     * NOTE: deprecated.
     */
    PixLib::Histo* histo(const std::string &name, const int old_index[4]) {
      Index_t index;
      makeIndex(index,old_index);
      return m_histo->histo(name,index);
    }

    /** Count the number of histograms in the collection at the given location.
     * @param folder_hierarchy list of folder names which will lead to the desired histogram collection.
     * @param info will be filled with the maximum number of histograms per level (0-2) or -1 and the number of histograms at deepest level.
     */
    bool numberOfHistos(const std::vector<std::string> &folder_hierarchy, HistoInfo_t &info ) {
      return m_histo->numberOfHistos(folder_hierarchy,info);
    }

    /** Return true if the histogramm exists at the given location.
     * @param folder_hierarchy list of folder names which will lead to the desired histogram collection.
     */
    bool haveHisto(const std::vector<std::string> &folder_hierarchy) {
      return m_histo->haveHisto(folder_hierarchy);
    }


    /** Returns the requested PixLib::Histo of the histogram collection of the given name.
     * @return a pointer to a newly created histogramm or NULL.
     * The caller is responsible for deleting the hisogram.
     */
    PixLib::Histo* histo(const std::vector<std::string> &folder_hierarchy, const Index_t &index) {
      return m_histo->histo(folder_hierarchy,index);
    }

    /** Returns the requested PixLib::Histo of the histogram collection of the given name (deprectated).
     * @return a pointer to a newly created histogramm or NULL.
     * The caller is responsible for deleting the hisogram.
     * NOTE: deprecated
     */
    PixLib::Histo* histo(const std::vector<std::string> &folder_hierarchy, const int old_index[4]) {
      Index_t index;
      makeIndex(index,old_index);
      return m_histo->histo(folder_hierarchy,index);
    }

  private:
    void makeIndex(Index_t &index, const int old_index[4])
    {
      index.clear();
      index.reserve(4);
      for (unsigned int i=0; i<3; i++) {
	if (old_index[i]>=0) {
	  index.push_back(static_cast<unsigned int>(old_index[i]));
	}
      }
      if (old_index[3]>=0) {
	index.push_back(static_cast<unsigned int>(old_index[3]));
      }
    }

    std::shared_ptr<IHistoAccessor> m_histo;
  };

  /** A handle for histograms
   * A histogram handle can be used to create a reference @ref HistoRef,
   * which can be used to access or count histograms.
   */
  class HistoHandle
  {
    //  protected:
  public:
    HistoHandle(IHistoAccessor *histo) : m_histo(std::shared_ptr<IHistoAccessor>(histo)) {}
  public:
    /** Create an invalid histogram handle
     * A histogram handle is not necessarily valid. So one has to verify before usage 
     * whether a histogram handle is valid.
     */
    HistoHandle()  {}

    HistoHandle(const HistoHandle &a) : m_histo(a.m_histo) {};

    HistoHandle &operator=(const HistoHandle &a) { m_histo = a.m_histo; return *this;};

    HistoRef ref() { 
      return HistoRef(m_histo);
    }

    const HistoRef ref() const { 
      return HistoRef(m_histo);
    }

    /** Return true if the reference is valid.
     */
    operator bool() const {
      return m_histo.get() != NULL;
    }

  private:
    std::shared_ptr<IHistoAccessor> m_histo;
  };

}

#endif
