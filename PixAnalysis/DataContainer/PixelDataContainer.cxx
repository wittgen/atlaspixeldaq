#include "PixelDataContainer.h"

PixelDataContainer::PixelDataContainer(const char *name, const char *path)
{
  // name on the display and file path
  if(name!=0)
    m_dtname = name;
  else
    m_dtname = "";
  if(path!=0)
    m_pname  = path;
  else
    m_pname  = "";
  m_fullpath = m_pname;

  // split file name into bare name and rest of path
  int i = m_pname.find_last_of("/");
  if(i>0){
    m_pname.erase(0,i+1);
    m_fullpath.erase(i+1,m_fullpath.length()-i);
  }

}
void PixelDataContainer::PixXY(int chip, int col, int row, float *xval, float *yval){

  if(chip<NCHIP/2){
    *xval = (float)(chip*NCOL+col);
    *yval = (float)row;
  } else{
    *xval = (float)(NCOL-1-col + NCOL*(NCHIP-1-chip));
    *yval = (float)(2*NROW-1-row);
  }

  return;
}
int PixelDataContainer::PixXYtoInd(int xval, int yval){
  int row, col, chip;
  PixXYtoCCR(xval,yval,&chip,&col,&row);
  return PixIndex(chip, col, row);
}
void PixelDataContainer::PixXYtoCCR(int xval, int yval, int *chip, int *col, int *row){
  if(yval<NROW){ // chips 0-7
    *row  = yval;
    *col  = xval%NCOL;
    *chip = xval/NCOL;
  } else{
    *row  = 2*NROW-1- yval;
    *col  = NCOL-1  - xval%NCOL;
    *chip = NCHIP-1 - xval/NCOL;
  }
  return;
}
void  PixelDataContainer::PixXYtoBins(float xval, float yval, int &binx, int &biny)
{// added by Aldo returns the binx and biny that corresponds to
 // the xval and yval

  binx = (int)xval + 1;
  biny = (int)yval + 1;
  
  return;
}
void  PixelDataContainer::PixCCRtoBins(int chip, int col, int row,int &binx, int &biny){
  float xval, yval;
  PixXY(chip,col,row,&xval,&yval);
  binx = (int)xval + 1;
  biny = (int)yval + 1;
  
  return;
}
int PixelDataContainer::Pixel_Type(int col, int row)
{

  if (( col > 0) && ( col < 17)){
    if (row == 152 || row == 154 || row == 156 || row == 158)
      return 5; // inter-ganged pixel
    else if (row == 153 || row == 155 || row == 157 || row == 159)
      return 2; // ganged pixel
    else
      return 0; // normal pixel
  } else {
    if (row == 152 || row == 154 || row == 156 || row == 158)
      return 6; // long inter-ganged pixel
    else if (row == 153 || row == 155 || row == 157 || row == 159)
      return 3; // long ganged pixel
    else
      return 1; // long
  }
}
const char* PixelDataContainer::Pixel_TypeName(int type)
{// returns the name of the pixel type
  switch(type){
  case 0: //normal
    return "NORMAL";
  case 1: // long
    return "LONG";
  case 2: // ganged
    return "GANGED";
  case 3: // long ganged
    return "LONG GANGED";
  case 4: //all
    return "ALL";
  case 5: // inter-ganged
    return "INTER-GANGED";
  case 6: // long inter-ganged
    return "LONG INTER-GANGED";
  default: //error
    return "Not Implemented";
  }
}
