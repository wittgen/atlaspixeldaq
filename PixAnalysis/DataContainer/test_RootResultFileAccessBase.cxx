#include "RootResultFileAccessBase.h"
#include <iostream>
#include <memory>

#include <TH1.h>
#include <TH2.h>
#include <TFile.h>

#include "HistoInfo_t.h"

int main(int argc, char **argv)
{
  std::vector<unsigned int> serial_number;
  std::vector<std::string> conn_names;
  std::string rod_name;
  std::string file_name;
  bool verbose=false;
  bool error = false;
  for (int arg_i=1; arg_i<argc; arg_i++) {
    if (strcmp(argv[arg_i],"-s")==0 && arg_i+1 < argc && isdigit(argv[arg_i+1][0])) {
      serial_number.push_back(atoi(argv[++arg_i]));
    }
    else if (strcmp(argv[arg_i],"-r")==0 && arg_i+1 < argc && isalpha(argv[arg_i+1][0])) {
      rod_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-m")==0 && arg_i+1 < argc && isalpha(argv[arg_i+1][0])) {
      conn_names.push_back(argv[++arg_i]);
    }
    else if (strcmp(argv[arg_i],"-f")==0 && arg_i+1 < argc) {
      file_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-v")==0) {
      verbose = true;
    }
    else {
      std::cout << "ERROR [" << argv[0] << ":main] Unexpected argument " << arg_i << " : " << argv[arg_i] << std::endl;
      error = true;
    }
  }
  if (error) {
    std::cout << "usage: " << argv[0] << " -f \"input root file\" -s serial-number -r rod-name [-m module-name ...]  " << std::endl
	      << std::endl
	      << " -m can be repeated as often as needed. So can the serial number,"
	      << std::endl;
    return -1;
  }

  try {

    std::vector<std::string> histogram_list;
    std::vector<unsigned int> conn_item_types;
    std::vector<std::string> conn_item_type_names;
    conn_item_types.push_back(0);
    conn_item_type_names.push_back("ROD");
    conn_item_types.push_back(1);
    conn_item_type_names.push_back("Module");

    for (std::vector<unsigned int>::const_iterator serial_number_iter = serial_number.begin();
	 serial_number_iter != serial_number.end();
	 ++serial_number_iter ) {

      std::pair<TFile *, TDirectory *> file = PixA::RootResultFileAccessBase::open(file_name,
										   PixA::RootResultFileAccessBase::kScanNumber,
										   *serial_number_iter);

      std::shared_ptr<TFile>  file_ptr(file.first);

      if (!file.first || !file.second) {
	std::cout << "FATAL [" << argv[0] << "main] Failed to open file " << file_name << " for scan " << *serial_number_iter << "." << std::endl;
	continue;
      }

      // get histogram lists for RODs and modules
      std::vector< std::set<std::string> > histogram_list;
      histogram_list.resize(2);
      PixA::RootResultFileAccessBase::histogramList( file.second, histogram_list[0],histogram_list[1],verbose);

      std::vector<std::string>::const_iterator conn_item_type_name_iter = conn_item_type_names.begin();

      for (std::vector<unsigned int>::const_iterator conn_type_iter = conn_item_types.begin();
	   conn_type_iter != conn_item_types.end();
	   ++conn_type_iter, ++conn_item_type_name_iter) {

	// paranoia check
	assert( conn_item_type_name_iter != conn_item_type_names.end() );
	assert( *conn_type_iter < histogram_list.size() );
	std::cout << *conn_item_type_name_iter <<  " hisotgrams : " << std::endl;
	unsigned int counter_i=0;
	for (std::set<std::string>::const_iterator histo_iter = histogram_list[*conn_type_iter].begin();
	     histo_iter != histogram_list[*conn_type_iter].end();
	     ++histo_iter, ++counter_i) {
	  std::cout << counter_i << " : " << *histo_iter << std::endl;
	}

	for (std::vector<std::string>::const_iterator conn_name_iter = conn_names.begin();
	     conn_name_iter != conn_names.end();
	     conn_name_iter++) {
	  counter_i=0;
	  for (std::set<std::string>::const_iterator histo_iter = histogram_list[*conn_type_iter].begin();
	     histo_iter != histogram_list[*conn_type_iter].end();
	     ++histo_iter, ++counter_i) {

	    // get the number of histograms which exist for a particular module
	    HistoInfo_t histo_info;
	    TDirectory *histo_dir = PixA::RootResultFileAccessBase::enter( file.second,rod_name, ( *conn_type_iter==0 ? std::string("") : *conn_name_iter), *histo_iter   );
	    PixA::RootResultFileAccessBase::numberOfHistograms(histo_dir, histo_info, verbose);

	    // dump the number of histograms to stdout
	    std::cout << *conn_name_iter << " :: " << counter_i << " : " << *histo_iter << " : ";
	    for (unsigned int j=0; j<3; j++) {
	      std::cout << histo_info.maxIndex(j) << ", ";
	    }
	    std::cout << histo_info.minHistoIndex() <<  " - " << histo_info.maxHistoIndex() << std::endl;


	    // invent a histogram index to access one histogram
	    PixA::Index_t index;
	    histo_info.makeDefaultIndexList(index);
	    if (index.size()>0) {
	    for (unsigned int i=0; i<index.size()-1; i++) {
	      if (histo_info.maxIndex(i)>0) {
		index[i]=histo_info.maxIndex(i)-1;
	      }
	    }
	    if (histo_info.maxHistoIndex()>0) {
	      index.back()=histo_info.maxHistoIndex()-1;
	    }

	    std::shared_ptr<TH1> h1( PixA::RootResultFileAccessBase::histogram(histo_dir, index, verbose) );
	    if (h1.get()) {
	      std::cout << " INFO [main " << argv[0] << "] histo " << h1->GetName() << " : "
			<< h1->GetXaxis()->GetXmin() << " - " << h1->GetXaxis()->GetXmax() << " / " << h1->GetNbinsX();
	      if (h1->InheritsFrom(TH2::Class())) {
		TH2 *h2=static_cast<TH2 *>(h1.get());
		std::cout << "; " << h2->GetYaxis()->GetXmin() << " - " << h2->GetYaxis()->GetXmax() << " / " << h1->GetNbinsY();
	      }
	      std::cout << std::endl;

	    }
	    }
	  }
	}

      }

    }
    return 0;
  }
  catch(std::exception &err) {
    std::cout << "FATAL [main" << argv[0] << "] caught exception : "  << err.what() << std::endl;
  }
  catch(...) {
    std::cout << "FATAL [main" << argv[0] << "] caught unknown exception." << std::endl;
  }
  return -1;
}
