#ifndef _HistoAxisInfo_t_h_
#define _HistoAxisInfo_t_h_

#include <string>
#include <map>
#include <vector>
#include <algorithm>


namespace PixA {

/** Class which provides axis title names for histograms.
 * The class also can be used to find out whether a certain histogram axis, may depend on
 * a scan variable (@ref hasVar)
 */
class HistoAxisInfo_t
{
public:
  HistoAxisInfo_t() {};
  HistoAxisInfo_t(const std::string &title) : m_title(title) {};

  /** Tell the class that the histogram axis may be the given variable.
   */
  HistoAxisInfo_t &addScanVariable(const std::string &name) { m_varNameList.push_back(name); return *this;}

  /** Set the axis title.
   */
  HistoAxisInfo_t &setTitle(const std::string &title)       { m_title=title; return *this;}

  /** Get the axis title.
   */
  const std::string &title() const {return m_title;}

  /** Return true if the axis may depend on the given scan variable.
   */
  bool hasVar(const std::string &var_name) const {
    std::vector<std::string>::const_iterator var_iter = find(m_varNameList.begin(),m_varNameList.end(),var_name);
    return var_iter != m_varNameList.end();
  }

  /** Return a title assuming that the axis depends on the given variable.
   * If no dependence was defined or no default title exists for the given variable,
   * the default title for this axis is return.
   */
  const std::string &title(const std::string &var_name) const {
    std::vector<std::string>::const_iterator var_iter = find(m_varNameList.begin(),m_varNameList.end(),var_name);
    if (var_iter != m_varNameList.end()) {
      std::map<std::string, std::string>::const_iterator var_title_iter = s_varTitleList.find(var_name);
      if (var_title_iter != s_varTitleList.end()) {
	return var_title_iter->second;
      }
    }
    return m_title;
  }

  /** Set the default title for the given variable.
   */
  static void addDefaultVarTitle(const std::string &var_name, const std::string &var_title) {
    s_varTitleList.insert(std::make_pair(var_name, var_title) );
  }

private:
  std::string m_title;
  std::vector< std::string > m_varNameList;

  static std::map<std::string, std::string> s_varTitleList;
};

}
#endif
