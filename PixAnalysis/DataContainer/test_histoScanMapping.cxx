#include "ScanResultStreamer.h"
#include "IScanResultListener.h"
#include "PixDbDataContainer.h"
#include "HistoRef.h"
#include "HistoInfo_t.h"
#include "MakePixScanHistoName.h"
#include <iostream>
#include <iomanip>
#include <PixController/PixScan.h>

#include <ConfigWrapper/ConfObjUtil.h>
#include <ConfigWrapper/ConfigHandle.h>

#include <map>
#include <vector>
//#include <multimap>

#include <cstdlib>
 
#ifndef TrimVal_t_h
#define TrimVal_t_h

namespace PixLib {
  typedef unsigned short TrimVal_t;
}

#endif


void help(const char *arg0)
{
  std::cout << arg0 << " file_name [label_name]" <<std::endl;
}

class ScanResultSpy : public PixA::IScanResultListener 
{
public:
  ScanResultSpy() : m_serialNumber(0) {};
  void newFile(const std::string&file_name) {
    std::cout << "INFO [ScanResultSpy::newFile]  file = " << file_name << std::endl;
  }

  void newLabel(const std::string &name, const PixA::ConfigHandle &label_config) {
    std::string comment;
    std::string time_stamp;
    unsigned int scan_number=0;
    if (label_config) {
      PixA::ConfigRef label_config_ref = label_config.ref();
     try {
       comment = confVal<std::string>(label_config_ref[""]["Comment"]);
       scan_number = confVal<unsigned int>(label_config_ref[""]["ScanNumber"]);
       time_stamp = confVal<std::string>(label_config_ref[""]["TimeStamp"]);
     }
     catch (PixA::ConfigException &err)
     {
        // catch exceptions which are thrown if a field does not exist e.g. ScanNumber
     }
    }
    std::cout << "INFO [ScanResultSpy::newLabel] ";
    if (scan_number>0) {
      std::cout << scan_number;
    }
    std::cout << " : \"" << name << "\"";
    if (!time_stamp.empty()) {
      std::cout << "  - " << time_stamp;
    }
    if (!comment.empty()) {
      std::cout << "  :: " << comment << "";
    }
    std::cout << std::endl;
    m_label = name;
    m_scanInfoScanNumber = scan_number;
    m_currentROD="";
    m_currentFolder="";
    if (m_serialNumber==0 && scan_number>0) {
      m_serialNumber=scan_number;
    }
    else {
      m_serialNumber=1;
    }
    m_folderToSerialNumber.clear();
    m_multiMap.clear();
    m_tempHistoList.clear();
    m_folderHierarchy.clear();
    m_nPixScanHistos=0;
    m_nHistos=0;
  }

  void newScanInfo(const PixA::ConfigHandle&) {}

  /** Called whenever the pix module group changes.
   */
  void newPixModuleGroup(const std::string &name) {
    std::cout << "INFO [ScanResultSpy::newPixModuleGroup] pix module group=\"" << name << "\"" << std::endl;
    m_currentROD = name;
  }


  /** Does nothing ?.
   */
  void newPixScan(const PixA::ConfigHandle &pix_scan_config) {}

  /** Called whenever the pix module group changes.
   */
  void finishPixModuleGroup() {
    std::stringstream header;
    header << std::setw(5) << std::setfill('0') << m_scanInfoScanNumber << m_label <<  ":" << m_currentROD + "// ";

    std::cout << "INFO [ScanResultSpy::finishPixModuleGroup] " << header.str() << " done with pix module group. pix scan histos = " << m_nPixScanHistos 
	      << " histos = " << m_nHistos << " serial numbers = " << m_folderToSerialNumber.size() << std::endl;
    if (m_folderToSerialNumber.size()>0) {
      std::cout << "INFO [ScanResultSpy::finishPixModuleGroup] " << header.str() << " done with pix module group. Gollected the following serial numbers :" << std::endl;
      for (std::map<std::string, unsigned int>::const_iterator serial_number_iter = m_folderToSerialNumber.begin();
	   serial_number_iter != m_folderToSerialNumber.end();
	   serial_number_iter++) {
	std::cout << "INFO [ScanResultSpy::finishPixModuleGroup] " << header.str() << serial_number_iter->first << " -> " << serial_number_iter->second << std::endl;
      }

      for (std::map<std::string, std::vector<std::pair<std::string, unsigned int> > >::iterator histo_iter = m_tempHistoList.begin();
	   histo_iter != m_tempHistoList.end();
	   histo_iter++) {
	std::map<std::string, unsigned int>::const_iterator serial_number_iter = m_folderToSerialNumber.find(histo_iter->first);
	if (serial_number_iter != m_folderToSerialNumber.end()) {
	  for(std::vector< std::pair<std::string,unsigned int> >::iterator iter=histo_iter->second.begin();
	      iter != histo_iter->second.end();
	      iter++) {
	    iter->second = serial_number_iter->second;
	  }
	}
	else {

	  unsigned int associated_histos=0;
	  for(std::vector< std::pair<std::string,unsigned int> >::iterator iter=histo_iter->second.begin();
	      iter != histo_iter->second.end();
	      iter++) {
	    std::map<std::string, unsigned int>::const_iterator histo_serial_number_iter = m_folderToSerialNumber.find(histo_iter->first+iter->first+"/");
	    if (histo_serial_number_iter != m_folderToSerialNumber.end()) {
	      iter->second = histo_serial_number_iter->second;
	      associated_histos++;
	    }
	    else {
	      iter->second = 0;
	    }
	  }

	  if (associated_histos != histo_iter->second.size()) {

	    std::string path = histo_iter->first;
	    for(;;) {
	      std::string::size_type pos = path.rfind("/");
	      if (pos == std::string::npos || pos == 0) {
		break;
	      }
	      path.erase(pos,path.size()-pos);
	      std::cout << "INFO [ScanResultSpy::finishPixModuleGroup] " << header.str() << "  try " << path << std::endl;
	      std::multimap<std::string, unsigned int>::const_iterator serial_number_lower_iter = m_multiMap.lower_bound(path);
	      if (serial_number_lower_iter != m_multiMap.end()) {
		std::cout << "INFO [ScanResultSpy::finishPixModuleGroup] " << header.str() << "  ok  " << path << " matches" << std::endl;
		std::multimap<std::string, unsigned int>::const_iterator serial_number_upper_iter = m_multiMap.upper_bound(path);
		if (serial_number_upper_iter != m_multiMap.end() && serial_number_upper_iter != serial_number_lower_iter) {
		  unsigned int candidate_counter=0;
		  for (std::multimap<std::string, unsigned int>::const_iterator multi_ser_iter = serial_number_lower_iter;
		       multi_ser_iter != serial_number_upper_iter;
		       multi_ser_iter++) {
		    std::cout << "INFO [ScanResultSpy::finishPixModuleGroup] " << header.str() << "  candidate  " << multi_ser_iter->second << std::endl;
		    candidate_counter++;
		  }

		  if (candidate_counter>1) {
		    std::cout << "WARNING [ScanResultSpy::finishPixModuleGroup] " << header.str()
			      << candidate_counter <<" candidates for "
			      << histo_iter->first
			      << " : " ;
		  }
		  for(std::vector< std::pair<std::string,unsigned int> >::iterator iter=histo_iter->second.begin();
		      iter != histo_iter->second.end();
		      iter++) {
		    if (iter->second == 0) {
		      // take the largest serial number
		      // assuming that the scans are connectect, it is likely that a histogram results from a comparison
                      // of scans.
		      for (std::multimap<std::string, unsigned int>::const_iterator multi_ser_iter = serial_number_lower_iter;
			   multi_ser_iter != serial_number_upper_iter;
			   multi_ser_iter++) {
			if (multi_ser_iter->second>iter->second) {
			  iter->second = multi_ser_iter->second;
			}
		      }
		      if (candidate_counter>1) {
			std::cout << " histo=\"" << iter->first << "\"" << std::endl;
		      }
		    }
		  }
		  if (candidate_counter>1) {
		    std::cout << ". Candidates : ";

		    for (std::multimap<std::string, unsigned int>::const_iterator multi_ser_iter = serial_number_lower_iter;
			 multi_ser_iter != serial_number_upper_iter;
			 multi_ser_iter++) {
		      std::cout << multi_ser_iter->second << " ";
		    }
		    std::cout << std::endl;
		  }
		}
	      }
	    }
	  }
	}

	for(std::vector< std::pair<std::string, unsigned int> >::const_iterator iter=histo_iter->second.begin();
	    iter != histo_iter->second.end();
	    iter++) {

	  if (iter->second >0 ) {
	    std::cout << "INFO [ScanResultSpy::finishPixModuleGroup] " << header.str() 
		      << histo_iter->first << "  : " << iter->first
		      << " -> " << iter->second << std::endl;
	  }
	  else {
	    std::cout << " INFO [ScanResultSpy::finishPixModuleGroup] " << header.str() 
		      << histo_iter->first << "  : " << iter->first
		      << " -> " << iter->second << std::endl;
	  }
	}
      }


    }
    else {
      if (m_nPixScanHistos != 0) {
	std::cout << "FATAL [ScanResultSpy::finishPixModuleGroup] " << header.str() << "No serial numbers but pix scan histos." << std::endl;
	assert(m_nPixScanHistos==0);
      }
      for (std::map<std::string, std::vector<std::pair<std::string, unsigned int> > >::iterator histo_iter = m_tempHistoList.begin();
	   histo_iter != m_tempHistoList.end();
	   histo_iter++) {

	std::pair<std::map<std::string, unsigned int>::const_iterator, bool>
	  ret = m_folderToSerialNumber.insert(std::make_pair(histo_iter->first, m_serialNumber++));

	if (!ret.second) {
	  throw std::runtime_error("FATAL [ScanResultSpy::finishPixModuleGroup] Failed to add serial number to list");
	}

	for(std::vector< std::pair<std::string,unsigned int> >::iterator iter=histo_iter->second.begin();
	    iter != histo_iter->second.end();
	    iter++) {
	  iter->second = ret.first->second;
	}
      }
    }

    std::map<unsigned int, std::string> serial_numbers;
    for(std::map<std::string, unsigned int>::const_iterator serial_number_iter=m_folderToSerialNumber.begin();
	serial_number_iter!=m_folderToSerialNumber.end();
	serial_number_iter++) {
      serial_numbers.insert(std::make_pair(serial_number_iter->second, serial_number_iter->first) );
    }

    for(std::map<unsigned int, std::string>::const_iterator serial_number_iter=serial_numbers.begin();
	serial_number_iter!=serial_numbers.end();
	serial_number_iter++) {
      std::cout << "INFO [ScanResultSpy::finishPixModuleGroup] " << header.str() << " association " << serial_number_iter->first << " -> "
		<< serial_number_iter->second << " : " << std::endl;
      for (std::map< std::string, std::vector< std::pair<std::string, unsigned int> > >::const_iterator histo_iter = m_tempHistoList.begin();
	   histo_iter != m_tempHistoList.end();
	   histo_iter++) {
	for (std::vector<std::pair<std::string, unsigned int> >::const_iterator iter = histo_iter->second.begin();
	     iter != histo_iter->second.end();
	     iter++) {
	  if (iter->second == serial_number_iter->first) {
	    std::cout << "INFO [ScanResultSpy::finishPixModuleGroup] " << header.str() << " histo association " 
		      << serial_number_iter->first << " -> " << histo_iter->first << " / "
		      << iter->first << std::endl;
	  }
	}
      }
    }

    m_tempHistoList.clear();
    m_nHistos=0;
    m_nPixScanHistos=0;
  }

  /** Called whenever the Pp0 changes.
   * newPp0 will never be called without newPixModuleGroup being called.
   */
  void newPp0(const std::string &name) {
    //    std::cout << "INFO [ScanResultSpy::newPp0] PP0=\"" << name << "\"" << std::endl;
  }

  void finishPp0() {
    //    std::cout << "INFO [ScanResultSpy::finishPp0] done with PP0." << std::endl;
  }


  /** Called whenever the result folder changes
   * @param folder_list the names of the folder hierarchy
   */
  void newFolder(const std::vector<std::string> &folder_hierarchy) {
    std::cout << "INFO [ScanResultSpy::newFolder] path=\"";
    m_currentFolder="";
    m_folderHierarchy.clear();
    for (std::vector<std::string>::const_iterator folder_iter = folder_hierarchy.begin();
	 folder_iter!=folder_hierarchy.end();
	 folder_iter++) {
      if (*folder_iter != "Scans" && *folder_iter != "Histos") {
	std::cout << *folder_iter << "/";
	m_currentFolder += *folder_iter + "/";
	m_folderHierarchy.push_back(*folder_iter);
      }
    }
    std::cout << "\"" << std::endl;
  }


  //  /** Called whenever the scan changes
  //   * @param folder_list the names of the folder hierarchy
  //   */
  //   void newScan(const ScanConfigRef &scan_config) = 0;

  /** Called for each pix scan histo.
   */
  void newPixScanHisto(const std::string &histo_name,
		       const PixA::ScanConfigRef &scan_config, 
		       const PixA::HistoHandle &histo_handle) {

    std::map<std::string, unsigned int>::const_iterator serial_number_iter = m_folderToSerialNumber.find(m_currentFolder);
    if (serial_number_iter == m_folderToSerialNumber.end()) {
      std::pair<std::map<std::string, unsigned int>::const_iterator, bool>
	ret = m_folderToSerialNumber.insert(std::make_pair(m_currentFolder, m_serialNumber++));
      if (!ret.second) {
	throw std::runtime_error("FATAL [ScanResultSpy::newPixScanHisto] Failed to add serial number to list");
      }
      serial_number_iter = ret.first;

      std::cout << "INFO [ScanResultSpy::newPixScanHisto] new serial number " <<  serial_number_iter->second
		<< " for  " << m_currentFolder << "."
		<< std::endl;

      std::string path;
      for (std::vector<std::string>::const_iterator folder_iter = m_folderHierarchy.begin();
	   folder_iter != m_folderHierarchy.end();
	   folder_iter++) {
	path += *folder_iter ;
	m_multiMap.insert(std::make_pair(path, serial_number_iter->second));

	std::cout << "INFO [ScanResultSpy::newPixScanHisto] add " << path << " to " << serial_number_iter->second
		  << std::endl;

	path += "/";
      }
    }

    m_nPixScanHistos++;

    PixA::HistoHandle a_histo_handle(histo_handle);
    PixA::PixDbDataContainer container(scan_config,
				       histo_handle);
    int histo_type = getHistoType(histo_name);


    std::cout << "INFO [ScanResultSpy::newPixScanHisto] "
	      << m_label << " " << m_scanInfoScanNumber << " : " << m_currentROD << "//" <<  m_currentFolder 
	      << " module=";
    if (!scan_config.connName().empty()) {
      std::cout << scan_config.connName() << "(" << scan_config.prodName() << ")";
    }
    else {
      std::cout << scan_config.prodName();
    }
    std::cout << " histo=\"" << histo_name << "\"" << std::endl;
  }
 
    /** Called for each normal histo.
     */
  void newHisto(const std::string &name, const PixA::HistoHandle &histo_handle, const std::string &rod_name, const PixA::ConfigHandle &scan_config) {
    PixA::HistoHandle a_histo_handle(histo_handle);
    std::cout << "INFO [ScanResultSpy::newHisto] "
	      << m_label << " " << m_scanInfoScanNumber << " : " << m_currentROD << "//" << m_currentFolder
	      << "ROD=" << rod_name;
    std::cout << " histo=\"" << name << "\"" << std::endl;
    m_tempHistoList[m_currentFolder].push_back(std::make_pair(name,0) );
    m_nHistos++;
  }

  void finish() {
    std::cout << "INFO [ScanResultSpy::finish] all done.";
  }

  void reportProblem(const std::string &message) {
    std::cout << "INFO [ScanResultSpy::reportProblem] : " << message << std::endl;
  }

private:
  std::string m_label;
  std::string m_currentROD;
  unsigned int m_scanInfoScanNumber;
  std::string m_currentFolder;

  unsigned int m_serialNumber;
  std::vector<std::string> m_folderHierarchy;
  std::map<std::string, unsigned int> m_folderToSerialNumber;
  std::multimap<std::string, unsigned int> m_multiMap;
  std::map<std::string, std::vector< std::pair<std::string, unsigned int> > > m_tempHistoList;

  unsigned int m_nHistos;
  unsigned int m_nPixScanHistos;

  static void initHistoNameTypeList();
  static int getHistoType(const std::string &histo_name);
  static std::map< std::string, int > s_histoNameToType;
};

std::map< std::string, int> ScanResultSpy::s_histoNameToType;

int ScanResultSpy::getHistoType(const std::string &histo_name)
{
  if (s_histoNameToType.empty()) initHistoNameTypeList();
  std::map< std::string, int >::const_iterator iter = s_histoNameToType.find(histo_name);
  if (iter != s_histoNameToType.end()) {
    return iter->second;
  }
  return -1;
}


void ScanResultSpy::initHistoNameTypeList() {
  //  if (s_histoTypeToName.empty()) {
  s_histoNameToType.clear();
  std::unique_ptr<PixLib::PixScan> ps(new PixLib::PixScan());
  s_histoNameToType = ps->getHistoTypes();
  //  }
}


int main(int argc, char **argv) 
{
  if (argc<2 || argc>3) {
    help((argc>0 ? argv[0] : "?"));
    return -1;
  }
  
  try {
  PixA::ScanResultStreamer streamer(argv[1]);
  ScanResultSpy listener;
  streamer.setListener(listener);
  if (argc==2) {
    streamer.read();
  }
  else {
    for (int arg_i=2; arg_i<argc; arg_i++) {
      streamer.readLabel(argv[arg_i]);
    }
  }
  }
  catch (PixA::ConfigException &err) {
    std::cout << "FATAL [main] caught exception (ConfigException) : " << err.getDescriptor() << std::endl;
  }
  catch (PixA::BaseException &err) {
    std::cout << "FATAL [main] caught exception (BaseException) : " << err.getDescriptor() << std::endl;
  }
  catch (std::exception &err) {
    std::cout << "FATAL [main] caught exception (std::exception) : " << err.what() << std::endl;
  }
  catch (...) {
    std::cout << "FATAL [main] unhandled exception. "  << std::endl;
    throw;
  }


}
