#ifndef _HistoExtraInfoList_h_
#define _HistoExtraInfoList_h_

#include <map>
#include <string>

#include "HistoExtraInfo_t.h"

namespace PixA {


class HistoExtraInfoList {
 protected:
  HistoExtraInfoList();

  const HistoExtraInfo_t &_histoExtraInfo(const std::string &name) const {
    // only consider the histogram name without the path name
    std::string::size_type pos = name.rfind("/");
    if (pos != std::string::npos && ++pos>name.size()) pos=std::string::npos;
    std::map<std::string,HistoExtraInfo_t>::const_iterator info_iter = m_extraInfo.find( (pos==std::string::npos ?
											  name :
											  std::string(name,pos,name.size()-pos)) );
    if (info_iter == m_extraInfo.end()) {
      return m_extraInfo[""];
    }
    else return info_iter->second;
  }

  void _addHistoExtraInfo(const std::string &name, const HistoExtraInfo_t &extra_info) {
    // only consider the histogram name without the path name
    std::string::size_type pos = name.rfind("/");
    if (pos != std::string::npos && ++pos>name.size()) pos=std::string::npos;
    m_extraInfo[ (pos==std::string::npos ?name : std::string(name,pos,name.size()-pos)) ]=extra_info;
  }

  static HistoExtraInfoList &instance() {
    if (!s_instance) {
      s_instance=new HistoExtraInfoList;
    }
    return *s_instance;
  }

 public:

  static const HistoExtraInfo_t &histoExtraInfo(const std::string &name) {
    return instance()._histoExtraInfo(name);
  }

  static void addHistoExtraInfo(const std::string &name, const HistoExtraInfo_t &extra_info) {
    instance()._addHistoExtraInfo(name,extra_info);
  }

 private:
  static HistoExtraInfoList *s_instance;

  mutable std::map<std::string, HistoExtraInfo_t> m_extraInfo;
};

}
#endif
