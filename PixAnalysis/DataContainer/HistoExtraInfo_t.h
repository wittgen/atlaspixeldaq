#ifndef _HistoExtraInfo_t_h_
#define _HistoExtraInfo_t_h_

#include "HistoAxisInfo_t.h"

namespace PixA {

  class HistoExtraInfo_t
  {
  public:
    HistoExtraInfo_t() {};

    enum EAxis {kXaxis, kYaxis, kZaxis};

    HistoAxisInfo_t &axis(EAxis axis) {return m_axis[axis];}

    const HistoAxisInfo_t &axis(EAxis axis) const {return m_axis[axis];}

  private:
    std::string m_title;
    HistoAxisInfo_t m_axis[3];
  };
}

#endif
