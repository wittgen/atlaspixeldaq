#ifndef _PixA_MakePixScanHistoName_h_
#define _PixA_MakePixScanHistoName_h_

#include <vector>
#include <string>

namespace PixA {

  class HistoRef;
  class ScanConfigRef;

  enum EModuleNameingConvention {kUnknownModuleNaming,kNewModuleNaming, kOldModuleNaming, kPlainNaming };

  std::pair<EModuleNameingConvention, unsigned int> 
    makePixScanHistoName(EModuleNameingConvention module_naming,
			 unsigned int mod_id,
			 const PixA::ScanConfigRef &scan_config,
			 PixA::HistoRef &histo,
			 const std::string &histo_name,
			 std::vector<std::string> &full_histo_name);

}
#endif
