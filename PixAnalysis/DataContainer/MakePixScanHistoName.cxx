#include "MakePixScanHistoName.h"

#include <ConfigWrapper/ScanConfig.h>
#include "HistoRef.h"

#include <ConfigWrapper/ConfObjUtil.h>

namespace PixA {

  std::pair<EModuleNameingConvention, unsigned int>
    makePixScanHistoName(EModuleNameingConvention module_naming,
			 unsigned int mod_id,
			 const PixA::ScanConfigRef &scan_config,
			 PixA::HistoRef &histo,
			 const std::string &histo_name,
			 std::vector<std::string> &full_histo_name)
  {
    full_histo_name.push_back(histo_name);
    if (module_naming==kPlainNaming) return std::make_pair(module_naming, static_cast<unsigned int>(-1));

    if (module_naming!= kOldModuleNaming) {
      full_histo_name.push_back(scan_config.connName());
      if (module_naming == kUnknownModuleNaming) {

	if (histo.haveHisto(full_histo_name)) {
	  module_naming=kNewModuleNaming;
	}
	else {
	  full_histo_name.pop_back();
	}
      }
    }
    if (full_histo_name.size()==1) {
      if (mod_id==static_cast<unsigned int>(-1)) {
	if (scan_config.hasModConfig()) {
	  try {
	    mod_id=confVal<int>(scan_config.modConfig()["general"]["ModuleId"]);
	  }catch(...) {
	  }
	}
	else {
	  std::cout << "WARNING [makePixScanHistoName] Tried to fall back to old histogram naming scheme but did not get a module Id for : " << scan_config.connName()
		    << ". Assume plain naming scheme." << std::endl; 
	  return std::make_pair(module_naming, static_cast<unsigned int>(-1));
	}
      }
      std::stringstream a_name;
      a_name << "Mod" << mod_id;
      full_histo_name.push_back(a_name.str());
      if (module_naming == kUnknownModuleNaming) {
	if (histo.haveHisto(full_histo_name)) {
	  module_naming=kOldModuleNaming;
	}
      }
    }
    return std::make_pair(module_naming, mod_id);
  }

}
