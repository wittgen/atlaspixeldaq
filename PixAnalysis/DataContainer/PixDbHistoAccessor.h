#ifndef _PixDbHistoAccessor_h_
#define _PixDbHistoAccessor_h_

#include "IHistoAccessor.h"
#include <ConfigWrapper/PixDbAccessor.h>
#include <ConfigWrapper/ObjStat_t.h>
#include <ConfigWrapper/DbRef.h>
#include "HistoRef.h"

namespace PixLib {
  class DbRecord;
  class Histo;
}

namespace PixA {

  class HistoHandle;

  class PixDbHistoAccessor : public IHistoAccessor, public PixDbAccessor
  {
    friend class HistoAccessorRef;

  protected:
    PixDbHistoAccessor(const PixA::PixDbAccessor &accessor) : PixDbAccessor(accessor) { s_stat.ctor();}
    ~PixDbHistoAccessor() { s_stat.dtor();}

    void accessStart() {
      PixDbAccessor::accessStart();
    }

    /** Called onve everything is done.
     */
    void accessEnd() {
      PixDbAccessor::accessEnd();
    }

    // Use default implementation which will call the vector version
    // bool numberOfHistos(const std::string &name, HistoInfo_t &info ) ;

    // Use default implementation which will call the vector version
    // bool haveHisto(const std::string &name);

    // Use default implementation which will call the vector version
    // PixLib::Histo* getGenericPixLibHisto(const std::string &name, const int scanpt[4]);

    /** Count the number of histograms which exist for the given name.
     * @param folder_hierarchy list of folder names which will lead to the desired histogram collection.
     * @param info will be filled with the maximum number of histograms per level (0-2) or -1 and the number of histograms at deepest level.
     */
    bool numberOfHistos(const std::vector<std::string> &folder_hierarchy, HistoInfo_t &info );

    /** Return true if the histogramm exists at the given location.
     * @param folder_hierarchy list of folder names which will lead to the desired histogram collection.
     */
    bool haveHisto(const std::vector<std::string> &folder_hierarchy);


    /** Returns the requested PixLib::Histo of the histogram collection of the given name.
     * @return a pointer to a newly created histogramm or NULL.
     * The caller is responsible for deleting the hisogram.
     */
    PixLib::Histo* histo(const std::vector<std::string> &name, const Index_t &index);

  public:
    static HistoHandle histoHandle( const DbHandle &handle);

  private:
    bool numberOfHistos(PixLib::DbRecord* record, const std::string &histo_name, HistoInfo_t &info );
    PixLib::Histo* histo(PixLib::DbRecord* record, const std::string &histo_name, const Index_t &index ) ;
    using IHistoAccessor::numberOfHistos;
    using IHistoAccessor::histo;

    PixLib::DbRecord *getRecord(const std::vector<std::string> &name);

    static ObjStat_t s_stat;
  };

  inline HistoHandle PixDbHistoAccessor::histoHandle( const DbHandle &handle) {
    return HistoHandle(new PixDbHistoAccessor(handle.accessor()) );
  }

}
#endif
