#include "IScanResultListener.h"
#include "ResultFileIndexer.h"
#include <ConfigWrapper/ConfObjUtil.h>
#include <ConfigWrapper/ConnectivityUtil.h>
#include "ScanResultStreamer.h"
#include "ConfigWrapper/Regex_t.h"
#include <iomanip>

namespace PixA {


  ResultFileIndexer::ResultFileIndexer(bool create_connectivity_if_needed)
    : /*m_tempHistoList(NULL)*/
    m_currentSerialNumber(0),
    m_currentScanNumber(0),
    m_createConnectivity(create_connectivity_if_needed),
    m_verbose(false)
  {
    m_ignoreFolderList.push_back( "Scans" );
    m_ignoreFolderList.push_back( "Histos" );
  }

  ResultFileIndexer::~ResultFileIndexer() {
    if (m_verbose) {
    for (std::map< std::string, SerialNumber_t >::const_iterator scan_iter = m_labelToSerialNumberMap.begin();
	 scan_iter != m_labelToSerialNumberMap.end();
	 scan_iter++) {
      std::cout << "INFO [ResultFileIndexer::dtor] S"  << std::setw(9) << std::setfill('0') << scan_iter->second  << " <- " << scan_iter->first << "." <<std::endl;
    }
    }

  }

  void ResultFileIndexer::addPattern( const std::string &pattern)
  { 
     if (!pattern.empty()) {
     m_folderPattern.push_back(new Regex_t(pattern));
     }
  }

  bool ResultFileIndexer::matchesPattern(const std::string &path_name) const
  {
    std::cout << "INFO [ResultFileIndexer::matchesPattern] path = " << path_name << std::endl;
    if (m_folderPattern.empty()) {
      return true;
    }
    else {
      for(std::vector<Regex_t *>::const_iterator iter=m_folderPattern.begin();
	  iter != m_folderPattern.end();
	  ++iter) {
	if ((*iter)->match(path_name)) {
	  std::cout << "INFO [ResultFileIndexer::matchesPattern] path = " << path_name << " matches." << std::endl;
	  return true;
	}
      }
    }
    return false;
  }

  void ResultFileIndexer::newFile(const std::string &name) {
    std::string::size_type ext_pos = name.rfind(".root");
    if (ext_pos==std::string::npos) {
      ext_pos = name.rfind(".sql");
    }
    if (ext_pos!=std::string::npos) {
      std::string::size_type start_pos = name.rfind("_ROD_",ext_pos);
      if (start_pos != std::string::npos) {
	const char *ptr = &(name[start_pos+strlen("_ROD_")]);
	if (*ptr=='C' || *ptr=='B' || *ptr=='L' || *ptr=='D' || *ptr=='I') {
	  ptr++;
	unsigned int n_digits=0;
	while ( isdigit(*ptr) ) {
	  ptr++;
	  n_digits++;
	}
	if (n_digits>0 && strncmp(ptr,"_S",2)==0) {
	  n_digits=0;
	  ptr+=2;
	  while ( isdigit(*ptr) ) {
	    ptr++;
	    n_digits++;
	  }
	  if (n_digits>0) {
	    m_currentFile = name.substr(0, start_pos);
	    m_currentFile += name.substr(ext_pos,name.size()-ext_pos);
	    if (m_verbose) {
	      std::cout << "INFO [ResultFileIndexer::newFile] file = " << m_currentFile << std::endl;
	    }
	    return;
	  }
	}
	//	std::string rod_name( name.substr(name,start_pos,pos-start_pos));
	//	if (m_rodPattern.matches(rod_name)) {
	}
      }
    }
    m_currentFile=name;
  }

  void ResultFileIndexer::newLabel(const std::string &name, const PixA::ConfigHandle &label_config)
  {
    m_currentLabel=name;
    std::string comment;
    std::string time_stamp;
    m_currentScanNumber = 0;
    if (label_config) {
      PixA::ConfigRef label_config_ref = label_config.ref();
     try {
       comment = confVal<std::string>(label_config_ref[""]["Comment"]);
       m_currentScanNumber = confVal<unsigned int>(label_config_ref[""]["ScanNumber"]);
       time_stamp = confVal<std::string>(label_config_ref[""]["TimeStamp"]);

     }
     catch (PixA::ConfigException &err)
     {
        // catch exceptions which are thrown if a field does not exist e.g. ScanNumber
     }
    }
    m_currentComment = comment;

    std::string::size_type pos = name.find(" - ");
    if (pos != std::string::npos ) {
      m_currentTypeName = name.substr(0,pos);
      m_currentLabelExtension=name.substr(pos+3,name.size()-pos-3);
    }
    else {
      m_currentTypeName =name;
      m_currentLabelExtension = "";
    }
    // remove all cached properties which are assigned to a serial number from memory
    m_currentSerialNumber=0;
    m_pixScanConfig.clear();

    if (m_verbose ){
    std::cout << "INFO [ResultFileIndexer::newLabel] ";
    if (m_currentScanNumber>0) {
      std::cout << m_currentScanNumber;
    }
      std::cout << " : \"" << m_currentTypeName << "\"";
      if (!m_currentLabelExtension.empty()) {
	std::cout << " \"" << m_currentLabelExtension << "\"";
      }
    if (!time_stamp.empty()) {
      std::cout << "  - " << time_stamp;
    }
    if (!comment.empty()) {
	std::cout << "  :: " << m_currentComment << "";
    }
    std::cout << std::endl;
    }

    std::vector<std::string> temp;
    newFolder(temp);
  }


  void ResultFileIndexer::newScanInfo(const ConfigHandle &scan_info)
  {
    m_currentScanInfo=scan_info;
    m_scanInfoFolder = currentFolder();
  }

  void ResultFileIndexer::newPixScan(const ConfigHandle &pix_scan_config) {
    m_pixScanConfig[ currentFolder()]=std::make_pair(pix_scan_config,false);
  }

  void ResultFileIndexer::usePixScanConfig(SerialNumber_t serial_number, const std::string &folder_name) {
    std::string temp_folder_name = folder_name;
    std::string::size_type slash_pos = folder_name.size();
    if (folder_name.size()>0 && folder_name[folder_name.size()-1]=='/') {
      slash_pos = folder_name.size()-1;
    }

    std::string::size_type file_name_pos = folder_name.find("::");
    if (file_name_pos != std::string::npos && file_name_pos+2 < temp_folder_name.size()) {
      temp_folder_name.erase(0,file_name_pos+2);
    }
    for(;slash_pos>0;) {
      std::map<std::string, std::pair<PixA::ConfigHandle, bool> >::iterator pix_scan_config_iter = m_pixScanConfig.find(temp_folder_name);
      if ( pix_scan_config_iter != m_pixScanConfig.end()) {
	if (!pix_scan_config_iter->second.second) {
	  addPixScanConfig( serial_number,  pix_scan_config_iter->first, pix_scan_config_iter->second.first);
	}
	pix_scan_config_iter->second.second=true;
	return;
      }
      slash_pos = temp_folder_name.rfind("/",slash_pos-1);
      if (slash_pos == std::string::npos) break;

      temp_folder_name.erase(slash_pos,temp_folder_name.size()-slash_pos);
    }
    std::cout << "INFO [ResultFileIndexer::usePixScanConfig] No pix scan config for folder : "
	      << folder_name << "." << std::endl;
  }

  // #include <ScanInfo_t.hh>
  // #include <IMetaDataService.hh>

  // @todo should go somewhere else
//   InfoPtr_t<ScanInfo_t> ResultFileIndexer::makeScanInfo()
//   {
//     if (!m_currentScanInfo) return InfoPtr_t<ScanInfo_t>();

//     try {
//       PixA::ConfigRef  scan_info( m_currentScanInfo.ref() );

//       PixA::ConfGrpRef time(scan_info["Time"]);
//       m_startTimeString  = confVal<std::string>(time["ScanStart"]);
//       std::string end_time_string  = confVal<std::string>(time["ScanEnd"]);

//       struct tm start_tm;
//       strptime(m_startTimeString.c_str(),"%Y-%m-%d %H:%M:%S", &start_tm);

//       struct tm end_tm;
//       strptime(end_time_string.c_str(),"%Y-%m-%d %H:%M:%S", &end_tm);
//       time_t start_time = mktime(&start_tm);
//       time_t end_time = mktime(&end_tm);
//       m_duration = end_time - start_time;

//       if (m_verbose) {
// 	std::cout << "INFO [ScanResultSpy::newScanInfo] time: start  = " << m_startTimeString
// 		  << " end = " << end_time_string
// 		  << " duration = " << end_time - start_time
// 		<< std::endl;
//       }

//       InfoPtr_t<ScanInfo_t> scan_meta_data(new ScanInfo_t);
//       scan_meta_data->setScanStart(start_time);
//       scan_meta_data->setScanEnd(end_time);
//       scan_meta_data->setScanType(m_currentTypeName);
//       //      if (!m_currentComment.empty()) {
//       std::string a_comment;
//       if (!m_currentLabelExtension.empty()) {
// 	a_comment += m_currentLabelExtension;
// 	a_comment += std::endl;
//       }
//       a_comment += m_currentComment;
//       scan_meta_data->setComment(a_comment);
//       //      }

//       PixA::ConfGrpRef tags(scan_info["Tags"]);
//       std::string tag_conn  = confVal<std::string>(tags["Conn"]);
//       std::string tag_alias = confVal<std::string>(tags["Alias"]);
//       std::string tag_data  = confVal<std::string>(tags["Data"]);
//       std::string tag_cfg   = confVal<std::string>(tags["Cfg"]);
//       unsigned int revision;
//       try {
// 	revision = confVal<unsigned int>(tags["CfgRev"]);
//       }
//       catch (PixA::ConfigTypeMismatch &) {
// 	revision = static_cast< unsigned int>( confVal<int>(tags["CfgRev"]) );
//       }

//       if (m_verbose) {
// 	std::cout << "INFO [ScanResultSpy::newScanInfo] Connectivity tags : "
// 		  << " conn  = " << tag_conn 
// 		  << " alias = " << tag_alias
// 		  << " data  = " << tag_data
// 		  << " cfg   = " << tag_cfg
// 		  << " cfg Rev.  = " << revision
// 		  << std::endl;
//       }

//       scan_meta_data->setIdTag("CT");
//       scan_meta_data->setConnTag(tag_conn);
//       scan_meta_data->setDataTag(tag_data);
//       scan_meta_data->setAliasTag(tag_alias);
//       scan_meta_data->setCfgTag(tag_cfg);
//       scan_meta_data->setCfgRev(revision);

//       return scan_meta_data;
//     }
//     catch(...) {
//       std::cerr << "ERROR [ResultFileIndexer::makeScanInfo] Failed to set connectivity tags in scan meta data." << std::endl;
//     }
//     return InfoPtr_t<ScanInfo_t>();
//   }

  /** Called whenever the pix module group changes.
   */
  void ResultFileIndexer::newPixModuleGroup(const std::string &name) {
    m_currentRodName = name;
    if (m_verbose) {
    std::cout << "INFO [ResultFileIndexer::newPixModuleGroup] pix module group=\"" << name << "\"" << std::endl;
  }
  }

  /** Called whenever the pix module group changes.
   */
  void ResultFileIndexer::finishPixModuleGroup() {
    if (m_verbose) {
      std::cout << "INFO [ResultFileIndexer::finishPixModuleGroup] done with pix module group." << std::endl;
    }
    for( std::map< std::string, std::vector<HistoContainer_t> >::iterator folder_iter = m_histoList.begin();
	 folder_iter != m_histoList.end();
	 ++folder_iter) {
      bool matches_pattern = matchesPattern(folder_iter->first + "::" + currentPixModuleGroupName());

      if (!matches_pattern) continue;

      SerialNumber_t scan_serial_number = findSerialNumber( folder_iter->first );

      // if there is no scan assigned to this folder or if the analysis assigned to this scan has
      // the invalid serial number zero, then consider this "analysis histogram as a "scan" histogram.

      if (scan_serial_number == 0) {
	// add this histogram as a new scan.
	if (m_currentScanInfo) {
	  scan_serial_number = _getScanSerialNumber( folder_iter->first, PixA::ConfigHandle() /* invalid scan config */, matches_pattern );
	}
      }

      if (scan_serial_number>0) {
	usePixScanConfig(scan_serial_number,folder_iter->first);

	for (std::vector< HistoContainer_t >::iterator histo_iter = folder_iter->second.begin();
	     histo_iter != folder_iter->second.end();
	     ++histo_iter) {
	  addScanHisto( scan_serial_number,
			currentPixModuleGroupName(),
			scanConfig( scan_serial_number, PixA::ConfigHandle(), ""),
			stripFileName(folder_iter->first, currentLabel()),
			histo_iter->name(),
			histo_iter->histoHandle() );
	}
      }
      else {
	std::cout << "ERROR [ResultFileIndexer::finishPixModuleGroup] Did not get a serial number for " << folder_iter->first << ". Not adding :" << std::endl;
	for (std::vector< HistoContainer_t >::iterator histo_iter = folder_iter->second.begin();
	     histo_iter != folder_iter->second.end();
	     ++histo_iter) {
	  std::cout << "\t" << histo_iter->name()  <<std::endl;
	}
      }
    }

    m_histoList.clear();
    m_currentRodName = "";
  }

  /** Called whenever the Pp0 changes.
   * newPp0 will never be called without newPixModuleGroup being called.
   */
  void ResultFileIndexer::newPp0(const std::string &name) {
    if (m_verbose) {
    std::cout << "INFO [ResultFileIndexer::newPp0] PP0=\"" << name << "\"" << std::endl;
  }
  }

  void ResultFileIndexer::finishPp0() {
    if (m_verbose) {
    std::cout << "INFO [ResultFileIndexer::finishPp0] done with PP0." << std::endl;
  }
  }


  /** Called whenever the result folder changes
   * @param folder_list the names of the folder hierarchy
   */
  void ResultFileIndexer::newFolder(const std::vector<std::string> &folder_hierarchy)
  {
    if (m_verbose) {
      std::cout << "INFO [ResultFileIndexer::newFolder] path=\"";
    }
    m_fullPath = protectFileName(m_currentFile);
    m_fullPath += "::";
    m_currentFolder = m_currentLabel;
    m_currentFolder += "/";
    for (std::vector<std::string>::const_iterator folder_iter = folder_hierarchy.begin();
	 folder_iter!=folder_hierarchy.end();
	 folder_iter++) {

      std::vector<std::string>::const_iterator ignore_folder_iter = m_ignoreFolderList.begin();
      for (;
	   ignore_folder_iter != m_ignoreFolderList.end();
	   ignore_folder_iter++) {
	if ( *ignore_folder_iter == *folder_iter ) break;
      }

      if ( ignore_folder_iter == m_ignoreFolderList.end() ) {
	m_currentFolder += *folder_iter;
	m_currentFolder += '/';
	if (m_verbose) {
	  std::cout << *folder_iter << "/";
	}
      }

    }
    if (m_verbose) {
      std::cout << "\"" << std::endl;
    }
    // a serial number is assigned to a lable
    // all sub folder will be assigned to the same serial number
    //    m_currentSerialNumber=0;
    m_fullPath += m_currentFolder;
    m_matches = matchesPattern(m_fullPath+"::"+currentPixModuleGroupName());
  }


  //  /** Called whenever the scan changes
  //   * @param folder_list the names of the folder hierarchy
  //   */
  //   void newScan(const ScanConfigRef &scan_config) = 0;

  /** Called for each pix scan histo.
   */
  void ResultFileIndexer::newPixScanHisto(const std::string &histo_name,
					  const PixA::ScanConfigRef &scan_config,
					  const PixA::HistoHandle &histo_handle) {
    if (m_currentSerialNumber == 0 ) {
      if (!getScanSerialNumber( scan_config.pixScanConfigHandle() )) return;
    }

    // do not consider histograms in folders which do not match the given patterns
    if (!currentFolderMatchesPattern()) return;

    PixA::HistoHandle a_histo_handle(histo_handle);

    if (m_verbose) {
    std::cout << "INFO [ResultFileIndexer::newPixScanHisto] module=";
    if (!scan_config.connName().empty()) {
      std::cout << scan_config.connName() << "(" << scan_config.prodName() << ")";
    }
    else {
      std::cout << scan_config.prodName();
    }
    std::cout << " histo=\"" << histo_name << "\"" << std::endl;
  }

    //    HistoList_t &a_histo_list = getHistoList();
    usePixScanConfig(m_currentSerialNumber, currentFolder());

    addScanHisto( m_currentSerialNumber,
		  currentPixModuleGroupName(),
		  scan_config.connName(),
		  ( !m_createConnectivity || scan_config.connectivity() ? 
		      scan_config 
		    : scanConfig(m_currentSerialNumber, scan_config.pixScanConfigHandle(), scan_config.connName())),
		  currentFolder() ,
		  histo_name,
		  histo_handle );

    //    m_tempHistoList->addHisto(m_currentFolder + histo_name,scan_config, scan_config.connName(), histo_handle);
  }

  void ResultFileIndexer::newHisto(const std::string &name, const PixA::HistoHandle &histo_handle, 
		const std::string &
#ifndef NDEBUG
		rod_name
#endif
		, const PixA::ConfigHandle &/*scan_config*/) 
  {
    //      std::cout << "INFO [CalibrationDataResultFileIndexer::newHisto]  " << currentFolder()  << " " << name <<  " for ROD " << rod_name << std::endl;
    assert( rod_name == m_currentRodName );
    if (histo_handle) {
      m_histoList[m_fullPath].push_back( HistoContainer_t(name, histo_handle) );
    }
    else {
	std::cout << "WARNING [CalibrationDataResultFileIndexer::newHisto]  " << currentFolder()  << " " << name <<  " for ROD " << rod_name
		  << " not a valid histogram." << std::endl;
    }
  }

  void ResultFileIndexer::finish() {
    if (m_verbose) {
    std::cout << "INFO [ResultFileIndexer::finish] all done." << std::endl;
  }
  }

  SerialNumber_t ResultFileIndexer::_getScanSerialNumber(const std::string &folder_name, const PixA::ConfigHandle &pix_scan_config, bool matches_pattern)
  {

    std::string label = getLabelFromFolder(folder_name);
    std::map< std::string, SerialNumber_t >::iterator label_iter = m_labelToSerialNumberMap.find( label  );
    if (label_iter == m_labelToSerialNumberMap.end()) {
      SerialNumber_t serial_number = allocateScanSerialNumber( getSerialNumberFileIdFromFolder( folder_name ), pix_scan_config, matches_pattern);
      if (matches_pattern) {
	if (m_verbose) {
	  std::cout << "INFO [ResultFileIndexer::_getScanSerialNumber] map " << label << " to S" << serial_number << std::endl;
	}
	std::pair< std::map< std::string, SerialNumber_t >::iterator, bool> ret = m_labelToSerialNumberMap.insert(std::make_pair( label, serial_number));
	if (!ret.second) {
	  std::cerr <<"ERROR [ResultFileIndexer::_getScanSerialNumber] failed to insert mapping : " << currentLabel() << " -> S" << serial_number << std::endl;
	}
	label_iter = ret.first;
      }
      else {
	return 0;
      }
    }
    usePixScanConfig( label_iter->second, folder_name );
    return label_iter->second;
  }

//   /** Get a histo list for the current folder.
//    */
//   HistoList_t &ResultFileIndexer::_getHistoList()
//   {
//     assert( m_currentSerialNumber>0 );
//     const std::string &rod_name = m_currentRodName;
//     assert( !rod_name.empty() );

//     std::map< std::string, HistoList_t> &rod_list = m_scanList[m_currentSerialNumber];
//     std::map< std::string, HistoList_t>::iterator rod_iter = rod_list.find( rod_name );
//     if (rod_iter == rod_list.end()) {
//       std::pair< std::map<std::string, HistoList_t>::iterator, bool > 
// 	ret = rod_list.insert( std::make_pair(rod_name, HistoList_t()) );
//       rod_iter = ret.first;
//     }
//     return rod_iter->second;
//   }

  ScanConfigRef ResultFileIndexer::scanConfig( SerialNumber_t scan_serial_number, const ConfigHandle &pix_scan_config, const std::string &module_name )
  {
    ConnectivityRef conn;
    ConfigHandle boc_config;
    if (m_createConnectivity) {
      conn = ScanResultStreamer::connectivity(m_currentScanInfo);
      if (conn) {
	PixA::Pp0List pp0_list=conn.pp0s( currentPixModuleGroupName() );
	PixA::Pp0List::const_iterator pp0_begin = pp0_list.begin();
	PixA::Pp0List::const_iterator pp0_end = pp0_list.end();
	for(PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
	    pp0_iter != pp0_end;
	    ++pp0_iter) {
	  
	  boc_config = conn.bocConf(pp0_iter);
	  if (boc_config) break;
	}
      }
      else {
	if (m_createConnectivity) {
	  std::cerr << "ERROR [ResultFileIndexer::scanConfgi] No connectiviity for scan S" << scan_serial_number << std::endl;
	}
      }
    }
    if (module_name.empty()) {
      return ScanConfigRef::makeScanConfig( pix_scan_config, 
					    boc_config,
					    scan_serial_number,
					    conn);
    }
    else {
      const PixLib::ModuleConnectivity *module_conn=NULL;
      if (conn) {
	module_conn = conn.findModule( module_name);
      }
      if (module_conn) {
 //         std::cout<<"a giocare sono io"<<std::endl;
	return ScanConfigRef::makeScanConfig( const_cast<PixLib::ModuleConnectivity *>( module_conn)->prodId(),
					      module_name,
					      conn.modConf( module_name),
					      pix_scan_config,
					      boc_config,
					      scan_serial_number,
					      conn,
					      PixA::moduleIter( module_conn ) );
      }
      else {
	// scan config without module name ?
	return ScanConfigRef::makeScanConfig( "",
					      module_name,
					      PixA::ConfigHandle(),
					      pix_scan_config,
					      boc_config,
					      scan_serial_number);
      }
    }
  }

  std::string ResultFileIndexer::stripFileName(const std::string &full_name, const std::string &label) {
    std::string::size_type pos = full_name.find("::");
    if (pos == std::string::npos) {
      pos=0;
    }
    else {
      pos+=2;
    }
    if (!label.empty() && full_name.compare(pos, label.size(),label)==0) {
      pos += label.size();
    }
    if (pos>0) {
      return full_name.substr(pos,full_name.size()-pos);
    }
    else {
    return full_name;
  }
  }

  std::string ResultFileIndexer::protectFileName(const std::string &full_name) {
    std::string ret;
    std::string::size_type last_pos=0;
    for(;;) {
      std::string::size_type pos = full_name.find("::",last_pos);
      if (pos ==std::string::npos) break;
      ret += full_name.substr(last_pos,pos-last_pos);
      ret += "\\:\\:";
      last_pos = pos+2;
    }
    if (ret.empty()) {
      return full_name;
    }
    else {
      ret += full_name.substr(last_pos, full_name.size()-last_pos);
      return ret;
    }
  }

  PixA::ConnectivityRef ResultFileIndexer::connectivity(const ConfigHandle &scan_info)
  {
    return ScanResultStreamer::connectivity(scan_info);
  }

  std::string ResultFileIndexer::getLabelFromFolder(const std::string &folder_name)
  {
    std::string::size_type start_pos = folder_name.find("::");
    if (start_pos==std::string::npos || start_pos+2>=folder_name.size()) {
      start_pos = 0;
    }
    else {
      start_pos+=2;
    }
    std::string::size_type pos = folder_name.find("/", start_pos);
    if (pos != std::string::npos) {
      return folder_name.substr(start_pos,pos-start_pos);
    }
    else {
      return folder_name;
    }
  }

  std::string ResultFileIndexer::getSerialNumberFileIdFromFolder(const std::string &folder_name)
  {
    std::string::size_type start_pos = folder_name.find("::");
    if (start_pos==std::string::npos || start_pos+2>=folder_name.size()) {
      start_pos = 0;
    }
    else {
      start_pos+=2;
    }
    std::string::size_type pos = folder_name.find("/", start_pos);
    if (pos != std::string::npos) {
      return folder_name.substr(0,pos);
    }
    else {
      return folder_name;
    }
  }

}
