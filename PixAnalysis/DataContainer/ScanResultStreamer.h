#ifndef _ScanResultStreamer_h_
#define _ScanResultStreamer_h_

#include <memory>
#include <set>
#include <map>
#include <vector>

#include <ConfigWrapper/ConfigHandle.h>
#include <ConfigWrapper/Connectivity.h>
#include <ConfigWrapper/DbRef.h>

namespace PixLib {
  class DbRecord;
  class PixDbInterface;
}

namespace PixA {

  class Regex_t;
  class IScanResultListener;

  class ScanResultStreamer {
  public:
    ScanResultStreamer(const std::string &orig_file_name, const std::string &label_name="");

    ScanResultStreamer(DbHandle &root_handle, const std::string &label_name="");

    ~ScanResultStreamer();

    /** Process all labels of the  the file.
     */
    void read(const std::string &mname="");

    /** Process one label of the file.
     */
    void readLabel(const std::string &label_name, const std::string &mname="");

    /** Only read the labels but do not process the lables.
     */
    void readLabels();

    /** Get a connectivity for the given scanInfo.
     */
    static PixA::ConnectivityRef connectivity(const ConfigHandle &scan_info);

    /** Set the ignore-connectivity-and-config flag.
     * If this flag is set to true, the stream will not attempt to create a connectivity nor
     * will it search for module configurations.
     */
    void setIgnoreConnAndCfg(bool ignore) {
      if (ignore) {
	initModuleNamePattern();
      }
      m_ignoreConnAndCfg=ignore;
    }

  private:
    void setLabel(const std::string &label_name);

    void readCurrentLabel(const std::string &mname="");
  public:

    /** Set a listener which is called for each part of the result. 
     * The listener is owned by the user who is responsible for
     * keeping the listener alive until the scan result streamer
     * has finished and to clean up afterwards.
     */
    void setListener(IScanResultListener &listener) { m_first=false; m_listener=&listener;}

  protected:

    bool createGlobaleConfigList(DbHandle &root_record, const std::string &rod_name, const std::string &module_name);

    bool createConfigListFromConnectivity(const std::string &rod_name);

    bool processModulesFromConnectivity(const std::set<std::string> &module_names,  
					const DbHandle &histo_folder_handle,
					const std::vector< DbHandle > &pix_scan_histo_list,
					const PixA::ConfigHandle &pix_scan_config,
					const PixA::ConfigHandle &boc_config,
					const std::string &rod_name,
					unsigned int scan_number);

    bool processModulesFromConfigList(const std::set<std::string> &module_names,  
				      const DbHandle &histo_folder_handle,
				      const std::vector< DbHandle > &pix_scan_histo_list,
				      const PixA::ConfigHandle &pix_scan_config,
				      const PixA::ConfigHandle &boc_config,
				      unsigned int scan_number);

    /** Process modules only from the list of folders which contain pix scan histos.
     * This method only makes sense in conjunction with @ref m_ignoreConnAndCfg set to true.
     * In that particular case, this folder list is the only mean to know what 
     * modules could exist.
     */
    bool processModulesFromModuleList(const std::set<std::string> &module_names,
				      const DbHandle &histo_folder_handle,
				      const std::vector< DbHandle > &pix_scan_histo_list,
				      const PixA::ConfigHandle &pix_scan_config,
				      const PixA::ConfigHandle &boc_config,
				      unsigned int scan_number);

    PixA::ConfigHandle modConf(const std::string &module_prod_name);

    std::string m_fileName;
    DbHandle m_rootHandle;
    DbHandle m_labelHandle;
    DbHandle m_groupHandle;

    PixA::ConnectivityRef m_conn;

    std::map<std::string, PixA::ConfigHandle> m_configList;
    std::map<std::string, PixA::ConfigHandle> m_globalConfigList;

    IScanResultListener                      *m_listener;

    static std::set<std::string> s_histoTypeList;
    static void initHistoTypes();

    void initModuleNamePattern();

    std::vector< Regex_t *>          m_modulePattern;
    bool m_ignoreConnAndCfg;
    bool m_first;
  };

}
#endif
