#ifndef _GenericHistogramAccess_h_
#define _GenericHistogramAccess_h_

#ifdef _PIXA_HISTO_IS_ROOT_TH1
#  include <TH1.h>
#  include <TH2.h>
#endif
#ifdef ROOT_TH2
#  include <TROOT.h>
#endif
#include "Histo/Histo.h"
#include "PixFe/PixGeometry.h"
using namespace PixLib;
namespace PixA {

  // generic histogram access functions
  template <class T> T *createHisto(const char *name,
				    const char *title,
				    unsigned int n_columns,float start_column, float end_column,
				    unsigned int n_rows,   float start_row,    float end_row);

  template <class T> float binContent(const T *, unsigned int col_i, unsigned int row_i);
  template <class T> void setBinContent(T *, unsigned int col_i, unsigned int row_i, float value);
  template <class T> bool is2D(const T *);
  template <class T> unsigned int nBins(const T *, unsigned int axis);
  template <class T> unsigned int nColumns(const T *);
  template <class T> unsigned int nRows(const T *);
  template <class T> unsigned int startRow(const T *);
  template <class T> unsigned int endRow(const T *);
  template <class T> unsigned int startColumn(const T *);
  template <class T> unsigned int endColumn(const T *);
  template <class T> double axisMin(const T *, unsigned int axis);
  template <class T> double axisMax(const T *, unsigned int axis);
  template <class T> const char *histoName(const T *);
  template <class T> const char *histoTitle(const T *);

  template <class T> void setMinMax(T *, float a_min, float a_max);

  // ----
  // implementations for PixLib histo

# ifdef PIXLIB_HISTO
  template <> inline PixLib::Histo *createHisto<PixLib::Histo>(const char *name,
							const char *title,
							unsigned int n_columns,float start_column, float end_column,
							unsigned int n_rows,   float start_row,    float end_row) {
    return new PixLib::Histo(name,title,n_columns,start_column, end_column, n_rows, start_row, end_row);
  }

  template <> inline bool is2D<PixLib::Histo>(const PixLib::Histo *h2) {
    return const_cast<PixLib::Histo *>(h2)->nDim()==2;
  }

  template <> inline void setBinContent<PixLib::Histo>(PixLib::Histo *h2, unsigned int col_i, unsigned int row_i, float value) {
    h2->set(col_i,row_i,value);
  }

  template <> inline float binContent<PixLib::Histo>(const PixLib::Histo *h2, unsigned int col_i, unsigned int row_i) {
    return (*const_cast<PixLib::Histo *>(h2))(col_i,row_i);
  }

  template <> inline unsigned int nColumns<PixLib::Histo>(const PixLib::Histo *h2) {
    return const_cast<PixLib::Histo *>(h2)->nBin(0);
  }

  template <> inline unsigned int nRows<PixLib::Histo>(const PixLib::Histo *h2) {
    return const_cast<PixLib::Histo *>(h2)->nBin(1);
  }

  template <> inline unsigned int nBins<PixLib::Histo>(const PixLib::Histo *h2, unsigned int axis) {
    assert( axis<=2 );
    return const_cast<PixLib::Histo *>(h2)->nBin(axis);
  }

  template <> inline double axisMin<PixLib::Histo>(const PixLib::Histo *h2, unsigned int axis) {
    assert( axis<=2 );
    return const_cast<PixLib::Histo *>(h2)->min(axis);
  }

  template <> inline double axisMax<PixLib::Histo>(const PixLib::Histo *h2, unsigned int axis) {
    assert( axis<=2 );
    return const_cast<PixLib::Histo *>(h2)->max(axis);
  }

  template <> inline unsigned int startRow<PixLib::Histo>(const PixLib::Histo */*h2*/) {
    return 0;
  }

  template <> inline unsigned int endRow<PixLib::Histo>(const PixLib::Histo *h2) {
    return const_cast<PixLib::Histo *>(h2)->nBin(0);
  }

  template <> inline unsigned int startColumn<PixLib::Histo>(const PixLib::Histo */*h2*/) {
    return 0;
  }

  template <> inline unsigned int endColumn<PixLib::Histo>(const PixLib::Histo *h2) {
    return const_cast<PixLib::Histo *>(h2)->nBin(0);
  }

  template <> inline const char *histoName<PixLib::Histo>(const PixLib::Histo *h2) {
    return const_cast<PixLib::Histo *>(h2)->name().c_str();
  }

  template <> inline const char *histoTitle<PixLib::Histo>(const PixLib::Histo *h2) {
    return const_cast<PixLib::Histo *>(h2)->title().c_str();
  }

  


# endif
  // implementations for pixlib histos.
  //___ 


  // ---
  // implementations for ROOT TH2

# ifdef ROOT_TH2
 template <> inline TH2 *createHisto<TH2>(const char *name,
				    const char *title,
				    unsigned int n_columns,float start_column, float end_column,
				    unsigned int n_rows,   float start_row,    float end_row) {
    gROOT->cd();
    TH2 *a_h2=new TH2F(name,title,n_columns,start_column, end_column, n_rows, start_row, end_row);
    a_h2->SetDirectory(0);
    return a_h2;
  }

  template <> inline bool is2D<TH2>(const TH2 *h2) {
    return static_cast<const TObject *>(h2)->InheritsFrom(TH2::Class());
  }

  template <> inline void setBinContent<TH2>(TH2 *h2, unsigned int col_i, unsigned int row_i, float value) {
    h2->SetBinContent(h2->GetBin(col_i,row_i),value);
  }

  template <> inline float binContent<TH2>(const TH2 *h2, unsigned int col_i, unsigned int row_i) {
    return h2->GetBinContent(h2->GetBin(col_i,row_i));
  }

  template <> inline unsigned int nColumns<TH2>(const TH2 *h2) {
    return static_cast<unsigned int>(h2->GetNbinsX());
  }

  template <> inline unsigned int nRows<TH2>(const TH2 *h2) {
    return static_cast<unsigned int>(h2->GetNbinsY());
  }

  template <> inline unsigned int nBins<TH2>(const TH2 *h2, unsigned int axis) {
    return (axis==0 ? nColumns(h2) : nRows(h2));
  }

  template <> inline double axisMin<TH2>(const TH2 *h2, unsigned int axis) {
    return (axis==0 ? h2->GetXaxis()->GetXmin() : h2->GetYaxis()->GetXmin());
  }

  template <> inline double axisMax<TH2>(const TH2 *h2, unsigned int axis) {
    return (axis==0 ? h2->GetXaxis()->GetXmax() : h2->GetYaxis()->GetXmax());
  }

  template <> inline unsigned int startRow<TH2>(const TH2 */*h2*/) {
    return 1;
  }

  template <> inline unsigned int endRow<TH2>(const TH2 *h2) {
    return static_cast<unsigned int>(h2->GetNbinsY()+1);
  }

  template <> inline unsigned int startColumn<TH2>(const TH2 */*h2*/) {
    return 1;
  }

  template <> inline unsigned int endColumn<TH2>(const TH2 *h2) {
    return static_cast<unsigned int>(h2->GetNbinsY()+1);
  }

  template <> inline const char *histoName<TH2>(const TH2 *h2) {
    return h2->GetName();
  }

  template <> inline const char *histoTitle<TH2>(const TH2 *h2) {
    return h2->GetTitle();
  }

  template <> inline void setMinMax<TH2>(TH2 *h2, float a_min, float a_max) {
    h2->SetMinimum(a_min);
    h2->SetMaximum(a_max);
  }

  template <> inline void setMinMax<PixLib::Histo>(PixLib::Histo *, float, float) {
  }

  // implementations for ROOT TH2
  //____
# endif

  // ---
  // implementations for ROOT TH1

# ifdef ROOT_TH1
  template <> inline bool is2D<TH1>(const TH1 *h1) {
    return static_cast<const TObject *>(h1)->InheritsFrom(TH2::Class());
  }

  template <> inline void setBinContent<TH1>(TH1 *h2, unsigned int col_i, unsigned int row_i, float value) {
    assert( h2->InheritsFrom(TH2::Class()));
    static_cast<TH2 *>(h2)->SetBinContent(static_cast<TH2 *>(h2)->GetBin(col_i,row_i),value);
  }

  template <> inline float binContent<TH1>(const TH1 *h2, unsigned int col_i, unsigned int row_i) {
    assert( h2->InheritsFrom(TH2::Class()));
    return static_cast<const TH2 *>(h2)->GetBinContent(static_cast<const TH2 *>(h2)->GetBin(col_i,row_i));
  }

  template <> inline unsigned int nColumns<TH1>(const TH1 *h2) {
    assert( h2->InheritsFrom(TH2::Class()));
    return static_cast<unsigned int>(static_cast<const TH2 *>(h2)->GetNbinsX());
  }

  template <> inline unsigned int nRows<TH1>(const TH1 *h2) {
    assert( h2->InheritsFrom(TH2::Class()));
    return static_cast<unsigned int>(static_cast<const TH2 *>(h2)->GetNbinsY());
  }

  template <> inline unsigned int nBins<TH1>(const TH1 *h1, unsigned int axis) {
    if (axis==0) {
      return nColumns(static_cast<const TH2 *>(h1));
    }
    else {
      assert( h1->InheritsFrom(TH2::Class()));
      return nRows(static_cast<const TH2 *>(h1));
    }
  }

  template <> inline double axisMin<TH1>(const TH1 *h1, unsigned int axis) {
    if (axis==0) {
      return h1->GetXaxis()->GetXmin();
    }
    else {
      assert( h1->InheritsFrom(TH2::Class()));
      return static_cast<const TH2 *>(h1)->GetYaxis()->GetXmin();
    }
  }

  template <> inline double axisMax<TH1>(const TH1 *h1, unsigned int axis) {
    if (axis==0) {
      return h1->GetXaxis()->GetXmax();
    }
    else {
      assert( h1->InheritsFrom(TH2::Class()));
      return static_cast<const TH2 *>(h1)->GetYaxis()->GetXmax();
    }
  }

  template <> inline unsigned int startRow<TH1>(const TH1 */*h2*/) {
    return 1;
  }

  template <> inline unsigned int endRow<TH1>(const TH1 *h2) {
    assert( h2->InheritsFrom(TH2::Class()));
    return static_cast<unsigned int>(static_cast<const TH2 *>(h2)->GetNbinsY()+1);
  }

  template <> inline unsigned int startColumn<TH1>(const TH1 */*h2*/) {
    return 1;
  }

  template <> inline unsigned int endColumn<TH1>(const TH1 *h1) {
    //    assert( h2->InheritsFrom(TH2::Class()));
    //return static_cast<unsigned int>(static_cast<const TH2 *>(h2)->GetNbinsY()+1);
    return static_cast<unsigned int>(h1->GetNbinsY()+1);
  }

  template <> inline const char *histoName<TH1>(const TH1 *h1) {
    return h1->GetName();
  }

  template <> inline const char *histoTitle<TH1>(const TH1 *h1) {
    return h1->GetTitle();
  }

  template <> inline void setMinMax<TH1>(TH1 *h1, float a_min, float a_max) {
    h1->SetMinimum(a_min);
    h1->SetMaximum(a_max);
  }

# endif

  // implementations for ROOT TH1
  //____

  /* guess module type from histogram */
 template <class T> class PixGeometry moduleType(const T *i) {
    unsigned int nCol=nColumns(i);
    unsigned int nRow=nRows(i);
    PixGeometry fei2_module(PixGeometry::FEI2_MODULE);
    PixGeometry fei2_chip(PixGeometry::FEI2_CHIP);
    PixGeometry fei4_module(PixGeometry::FEI4_MODULE);
    PixGeometry fei4_chip(PixGeometry::FEI4_CHIP);
    PixGeometry invalid(PixGeometry::INVALID_MODULE);
    if(nCol==fei2_module.nCol() && nRow==fei2_module.nRow()) return fei2_module; 
    if(nCol==fei2_chip.nChipCol() && nRow==fei2_chip.nChipRow()) return fei2_chip;  
    if(nCol==fei4_module.nCol() && nRow==fei4_module.nRow()) return fei4_module;
    if(nCol==fei4_module.nChipCol() && nRow==fei4_module.nChipRow()) return  fei4_chip;
    return invalid;
  }

}

#endif
