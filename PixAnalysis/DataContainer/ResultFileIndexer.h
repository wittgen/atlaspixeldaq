#include "IScanResultListener.h"
#include "HistoRef.h"
#include <cstdlib>

namespace PixA {

  class Regex_t;

  typedef unsigned int SerialNumber_t;

  class HistoList_t;

  /**
   * Serial number assignment strategie
   *  - on new ScanInfo : create meta data 
   *    + connectivity tags
   *    + comment from label
   *    + file name
   *    + time stamps
   *  - on newHist
   *    + add pair<full_path_name, histogram >
   *  - on newPixScanHisto
   *    + get serial number using file_name + full_path_name.
   *    + map file_name + all higher level path names to this serial number (upto  label)
   *    + if new serial number (optionally?) write scan meta data, (optionally) set status of all RODs to UNKNOWN
   *    + (optionally) publish histograms in OH
   *    + (optionally) set ROD status to running.
   *  - on finishPixModuleGroup
   *    + if have histos
   *      . search for scan serial number using path_name maybe strip last level
   *      . not found but have scan info : get serial number, opt. write scan meta data, opt. set status of all RODs 
   *      . found : get analysis serial number for scan, opt. write analysis meta data, opt. set analysis status of all RODs.
   *      . publish histogram in OH
   *      . opt. set ROD analysis status to "DONE"
   *      . clear histo list
   *     + if have serial number opt. set ROD status to "DONE"
   *  - on finishLabel
   *    + opt. set status of all RODs which have unknwon STATUS to "ABORTED."
   *    + opt. set analysis status of all RODs which have unknwon STATUS to "ABORTED."
   *    + opt. set global scan status and analysis status.
   *    + clear and remove analysis and serial number container (contain ROD status maps)
   *    + clear folder fragment to serial number assignment map
   */
  class ResultFileIndexer : public PixA::IScanResultListener
  {
  public:

    ResultFileIndexer(bool create_connectivity_if_needed);

    ~ResultFileIndexer();

    void setVerbose(bool verbose) { m_verbose=verbose; }

    /** Only consider histograms which are in a folder which matches the given patterns.
     * @param pattern regular expression to match the folder path.
     * This function can be called more than once for multiple patterns.
     */
    void addPattern( const std::string &pattern);

    static std::string stripFileName(const std::string &full_name, const std::string &label);

    static std::string protectFileName(const std::string &full_name);

  protected:

    /** Will be called if a new serial number is needed.
     * Can be used to also write scan meta data.
     */
    virtual SerialNumber_t allocateScanSerialNumber(const std::string &file_name, const PixA::ConfigHandle &pix_scan_config, bool matches_pattern) = 0;


    /** Will be called if a new serial number is needed.
     * @param scan_serial_number the serial number of the scan to which this analysis is assigned.
     * Can be used to also write analysis meta data.
     */
    virtual SerialNumber_t allocateAnalysisSerialNumber(SerialNumber_t scan_serial_number, bool matches_pattern) = 0;


    /** Add a pix scan config for the given folder to the given scan serial number.
     * @param scan_serial_number the serial number of the scan in question
     * @param folder_name the folder to which this pix scan config is assigned.
     * @param pix_scan_config a handle to a pix scan config.
     */
    virtual void addPixScanConfig( SerialNumber_t scan_serial_number,
				   const std::string &folder_name,
				   PixA::ConfigHandle &pix_scan_config) = 0;


    /** Called for plain histograms assigned to an analysis.
     */
    virtual void addAnalysisHisto(SerialNumber_t analysis_serial_number,
				  const std::string &rod_name,
				  const std::string &folder_name, 
				  const std::string &histo_name,
				  const PixA::HistoHandle &histo_handle) = 0;


    /** Called for plain histograms assigned to a scan.
     */
    virtual void addScanHisto(SerialNumber_t scan_serial_number,
			      const std::string &rod_name,
			      const PixA::ScanConfigRef &scan_config,
			      const std::string &folder_name,
			      const std::string &histo_name,
			      const PixA::HistoHandle &histo_handle) = 0;

    /** Called for pix scan histograms assigned to a scan.
     */
    virtual void addScanHisto(SerialNumber_t scan_serial_number,
			      const std::string &rod_name,
			      const std::string &module_name,
			      const PixA::ScanConfigRef &scan_config,
			      const std::string &folder_name,
			      const std::string &histo_name,
			      const PixA::HistoHandle &histo_handle) = 0;

    void newFile( const std::string &name);

    void newLabel(const std::string &name, const PixA::ConfigHandle &label_config);

    /** Called whenever the pix module group changes.
     */
    void newPixModuleGroup(const std::string &name);

    /** Called whenever the pix module group changes.
     */
    void finishPixModuleGroup();

//     /** Called when a new scan info record is found
//      * @param scan_info handle for the scan info config object.
//      */
//     virtual void newScanInfo(const ConfigHandle &scan_info) = 0;

    /** Called whenever the Pp0 changes.
     * newPp0 will never be called without newPixModuleGroup being called.
     */
    void newPp0(const std::string &name);

    void finishPp0();

    /** Called whenever the result folder changes
     * @param folder_list the names of the folder hierarchy
     */
    void newFolder(const std::vector<std::string> &folder_hierarchy);

    /** Called when a new scan info record is found
     * @param scan_info handle for the scan info config object.
     */
    void newScanInfo(const ConfigHandle &scan_info);

    /** Called when a new pix scan config is found.
     * @param pix_scan_config handle for the pix scan config object.
     */
    void newPixScan(const ConfigHandle &pix_scan_config);

    //  /** Called whenever the scan changes
    //   * @param folder_list the names of the folder hierarchy
    //   */
    //   void newScan(const ScanConfigRef &scan_config) = 0;
  
    /** Called for each pix scan histo.
     */
    void newPixScanHisto(const std::string &histo_name,
			 const PixA::ScanConfigRef &scan_config, 
			 const PixA::HistoHandle &histo_handle);
 
    /** Called for each normal histo.
     * @param name the name of the histogram (collection).
     * @param histo a database handle for the folder which contains the histogram of the given name.
     * @param pix_module_group_name the connectivity name of the ROD.
     * @param scan_info a handle for the scan info config (may be invalid).
     */
    void newHisto(const std::string &, const PixA::HistoHandle &, const std::string &, const PixA::ConfigHandle &);

    void finish();


    /** Do nothing.
     */
    void reportProblem(const std::string &) {}


  protected:

    //    void setHistoList(HistoList_t  *new_histo_list)  { m_tempHistoList = new_histo_list; }

    ConfigHandle &currentScanInfo()                  { return m_currentScanInfo; }

    const std::string &currentTypeName() const       { return m_currentTypeName; }

    const std::string &currentLabel() const        { return m_currentLabel; }

    const unsigned int currentScanNumber() const   { return m_currentScanNumber; }

    const std::string &currentLabelExtension() const { return m_currentLabelExtension; }

    const std::string &currentComment() const      { return m_currentComment; }

    const std::string &currentFolder() const       { return m_currentFolder; }

    const std::string &scanInfoFolder() const       { return m_scanInfoFolder; }

    const std::string &currentPixModuleGroupName() const { return m_currentRodName; }

    bool matchesPattern(const std::string &path_name) const;

    bool currentFolderMatchesPattern() const { return m_matches ; }

    //    HistoList_t *currentHistoList()                  { return m_tempHistoList; }
    //    const HistoList_t  *currentHistoList() const     { return m_tempHistoList; }

    /** Get a scan serial number for the current folder.
     * If no serial number has been assigned yet, a new serial number will be 
     * allocated and the current folder will be assigned to it.
     * @sa _getScanSerialNumber
     */
    SerialNumber_t getScanSerialNumber(const PixA::ConfigHandle &pix_scan_config)
    {
      if (m_currentSerialNumber==0) {
	m_currentSerialNumber = _getScanSerialNumber( m_fullPath, pix_scan_config, currentFolderMatchesPattern() );
      }
      return m_currentSerialNumber;
    }

//     /** Get a histo list for the current folder.
//      * @sa _getHistoList
//      */
//     HistoList_t &getHistoList() {
//       if (m_tempHistoList) return m_tempHistoList;
//       return _getHistoList();
//     }

    /** Return the scan serial number which is assigned to the given folder or zero.
     * @param folder_name the name of the folder.
     * @return the assigned serial number or zero.
     */
    SerialNumber_t findSerialNumber( const std::string &folder_name ) const
    {
      std::string label = getLabelFromFolder(folder_name);
      std::map<std::string,SerialNumber_t>::const_iterator label_to_serial_number_iter = m_labelToSerialNumberMap.find(label);
      if (label_to_serial_number_iter != m_labelToSerialNumberMap.end()) {
	return label_to_serial_number_iter->second;
      }
      return 0;
    }

    /** Get a connectivity for the given scanInfo.
     */
    static PixA::ConnectivityRef connectivity(const ConfigHandle &scan_info);
    
  private:
    /** Get a serial number for the given folder.
     * If no serial number has been assigned yet, a new serial number will be 
     * allocated and the current folder will be assigned to it.
     */
    SerialNumber_t _getScanSerialNumber( const std::string &folder_name, const PixA::ConfigHandle &pix_scan_config, bool matches_pattern);

    /** Mark a pix scan config associated to the given folder as used.
     */
    void usePixScanConfig(SerialNumber_t serial_number, const std::string &folder_name);

    static std::string getLabelFromFolder(const std::string &folder_name);

    /** Get the part of the full path name which is assigned to a serial number.
     */
    static std::string getSerialNumberFileIdFromFolder(const std::string &folder_name);

//     /** Get a histo list for the current folder.
//      */
//     HistoList_t &_getHistoList();

    /** Create a scan config for a module or the current ROD if no module name is given.
     * @param scan_serial_number the serial number added to the scan config
     * @param pix_scan_config a handle for the scan configuration (may be invalid).
     * @param module_name the name of the module or an empty string.
     */
    ScanConfigRef scanConfig( SerialNumber_t scan_serial_number, const ConfigHandle &pix_scan_config, const std::string &module_name );

    std::vector<std::string> m_ignoreFolderList; /**< List of folder names which should be skipped when building up the full path name.*/

    //    HistoList_t    *m_tempHistoList;

    std::string     m_currentTypeName;
    std::string     m_currentLabel;
    std::string     m_currentLabelExtension;
    std::string     m_currentComment;
    std::string     m_currentRodName;
    std::string     m_currentFile;
    std::string     m_fullPath;
    std::string     m_currentFolder;
    SerialNumber_t  m_currentSerialNumber;
    unsigned int    m_currentScanNumber;
    ConfigHandle    m_currentScanInfo;
    std::string     m_scanInfoFolder;

    class HistoContainer_t
    {
    public:
      HistoContainer_t(const std::string &histo_name, const PixA::HistoHandle &handle)
	: m_histoName(histo_name),
	  m_histoHandle(handle)
      {}

      PixA::HistoHandle histoHandle() const { return m_histoHandle; }
      const std::string &name() const       { return m_histoName; }

    private:
      std::string       m_histoName;
      PixA::HistoHandle m_histoHandle;
    };

    std::map< std::string, SerialNumber_t >                           m_labelToSerialNumberMap;
    std::map<std::string, std::pair< PixA::ConfigHandle, bool > >     m_pixScanConfig;
    std::map< std::string, std::vector<HistoContainer_t> > m_histoList;
    std::vector< Regex_t *>          m_folderPattern;
    bool m_matches;

    bool m_createConnectivity;
    bool m_verbose;
  };

}
