#ifndef _HistoUtil_h_
#define _HistoUtil_h_

#include "PixelUtil.h"
#include <utility>
#include <string>
#include <vector>
#include <iostream>
class TObject;
class TH2;
class TH1;
class TGraph;

namespace PixLib {
  class Histo;
}

namespace PixA {

  typedef std::vector<unsigned int> Index_t;

  class ConfigRef;
  class Average_t;

  /** Creates a new ROOT histogram (1d or 2d) in memory and copies the elements.
   * The caller is responsible for cleaning (both histograms).
   * The source could be a ROOT or PixLib Histo. If the source is a ROOT
   * histogram the source object will be cloned.
   */
  template <class T_Histo_>
  TObject *transform(const T_Histo_ *h);

  /** Creates a new histogram (2d) in memory containing the pixel map of the given chip.
   * @param h pointer to a PixLib or ROOT histogram containing the full module matrix 2880*16 pixel.
   * @param chip the select front-end (0-15) or -1 for the whole matrix.
   * @return a ROOT histogram
   * The caller is responsible for deleting (both histograms).
   * The template is only instantiated for TH2 as a histogram destination type.
   * The input can be a PixLib or ROOT histogram.
   */
  template <class T_Histo_dest_, class T_Histo_src_>
  T_Histo_dest_* chipMap(const T_Histo_src_ *h2, int chip);
  //  TH2* chipMap(PixLib::Histo *h, int chip);

  /** Simple transformation which negates the values of pixel which were not scanned.
   * There are transformation for pixel which were scanned @ref transformOn and 
   * for pixel which were not scanned @ref transformOff.
   */
  class NoReference
  {
  public:
    /** return true if the not scanned pixel was set i.e. differs from the initial value which is zero. 
     */
    bool  isSet(const float value) const {return value!=0;}

    /** transformation for the values of scanned pixel
     */
    float transformOn(const float value) const {return value;}

    /** transformation for the values of not scanned pixel which were set nevertheless.
     */
    float transformOff(const float value) const {return -value;}
  };

  /** Simple transformation if a reference value us subtracted from the pixel value.
   * There are transformation for pixel which were scanned @ref transformOn and 
   * for pixel which were not scanned @ref transformOff.
   */
  class SimpleReference
  {
  public:
    SimpleReference(const float reference) : m_ref(reference) {};

    /** return true if the not scanned pixel was set i.e. differs from the initial value which is zero. 
     */
    bool  isSet(const float value) const {return value!=0;}

    /** transformation for the values of scanned pixel
     */
    float transformOn(const float value) const {return value-m_ref;}

    /** transformation for the values of not scanned pixel which were set nevertheless.
     */
    float transformOff(const float value) const {return value;}
  private:
    float m_ref;
  };


  /** Fill the bin contents for the 2D map into the given 1D histogram.
   * @param dest 1D histogram to be filled with the values of all pixels (or only scanned pixels if dest_non_scanned is not NULL)
   * @param dest_non_scanned 1D histogram or NULL to be filled with the values of all non scanned pixels.
   * @param src 2d module map histogram containing a value for each pixel.
   * @param chip the front-end number or -1 to restrict the projection to one front-end.
   * @param pixel_types to select which pixels i.e. normal, long. ganged, inter-ganged, should be considered.
   * @param a reference value to be subtracted from the value of scanned pixels.
   * @param mask_steps the number of effected mask stages.
   * @param total_mask_steps the totla number of mask_stages needed to scan the full matrix.
   * @param start_mask_step the initial mask stage.
   *
   * The template parameters T_Histo_ can be TH2 or PixLib::Histo, T_Reference can be NoReference or SimpleReference
   */
  template <class T_Histo_, class T_Reference_>
  void fillHistoFromMap(TH1 *dest,
			TH1 *dest_non_scanned,
			const T_Histo_ *src,
			int chip,
			PixelTypeMask_t pixel_types,
			T_Reference_ a,
			unsigned int mask_steps=1,
			unsigned int total_mask_steps=1,
			unsigned int start_mask_step=0);


  class HistoBinning
  {
  public:
    HistoBinning() : m_nBins(0), m_min(0),m_max(0) {}
    HistoBinning(unsigned int n_bins, float min_value, float max_value) : m_nBins(n_bins), m_min(min_value),m_max(max_value) {}

    unsigned int nBins() const {return m_nBins;}
    float        min()   const {return m_min;}
    float        max()   const {return m_max;}

    void setBinning(unsigned int n_bins, float min_value, float max_value) { m_nBins=n_bins; m_min=min_value; m_max=max_value;}

    unsigned int m_nBins;
    float        m_min;
    float        m_max;
  };

  /** Guess a good binning .
   * The template parameters T_Histo_ can be TH2 or PixLib::Histo, T_Reference can be NoReference or SimpleReference
   */
  template <class T_Histo_, class T_Reference_>
  HistoBinning guessHistoBinningForMapProjection(const T_Histo_ *src,
						 int chip,
						 PixelTypeMask_t pixel_types,
						 T_Reference_ a,
						 unsigned int mask_steps=1,
						 unsigned int total_mask_steps=1,
						 unsigned int start_mask_step=0);

  /** Return a graph of all scanned and all unscanned pixels . 
   * @param chip_map the pixel map of one front-end or the whole module.
   * @param chip the front-end number (0-15) or -1 for all front-ends.
   * @param pixel_types the pixel types which should be considered
   * @param average  a pointer to an average structure which will be filled with the min, max, mean and rms of all the pixels if average is not NULL.
   * @param reference a reference transformation for scanned and not scanned pixel
   * @param mask_steps the number of mask_steps
   * @param total_mask_steps the total number of mask_steps
   * @param start_mask_step the index of the first  mask_steps
   * @return the pointer to the graph of scanned and not scanned pixels or NULL.
   *
   * The template parameters T_Histo_ can be TH2 or PixLib::Histo, T_Reference can be NoReference or SimpleReference
   * The parameter to select a particular front-end is only exploited if the histogram contains a whole module.
   */
  template <class T_Histo_, class T_Reference_>
  std::pair<TGraph *,TGraph *> pixelGraph(const T_Histo_ *chip_map,
					  int chip,
					  PixelTypeMask_t pixel_types,
					  Average_t *average,
					  T_Reference_ reference,
					  unsigned int mask_steps=1,
					  unsigned int total_mask_steps=1,
					  unsigned int start_mask_step=0);

  /** Create a 2D histogram and fill with the DAC values.
   * @param chip the front-end number (0-15) or -1 for all front-ends.
   * @param dac_name the name of the DAC which can be FDAC or TDAC.
   * @param config a handle of the module config
   */
  template <class T_Histo_>
  T_Histo_* getDACMap(int chip, const std::string &dac_name, ConfigRef &mod_config);


  /** Create ROOT histogramm from PixLib or ROOT histogram with corrected binning and titled axes.
   * T_Histo_t can be PixLib::Histo or a ROOT TH2
   */
  template <class T_Histo_t>
  TH2 *createHisto(const T_Histo_t &histo, const std::string &histo_name, const PixA::ConfigRef &scan_config, const Index_t &index );

  /** Find the variables of an axis.
   */
  template <class T_Histo_t>
  const std::pair<int , int > findAxisVar(const T_Histo_t &histo, const std::string &histo_name, const PixA::ConfigRef &scan_config, const Index_t &index);
    
}

#endif
