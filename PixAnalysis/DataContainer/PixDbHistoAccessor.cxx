#include <PixDbInterface/PixDbInterface.h>
#include "LevelInfo_t.h"
//#include <ConfigWrapper/ObjStat_t.h>
//#include <memory>
#include "PixDbHistoAccessor.h"
#include <Histo/Histo.h>

namespace PixA {

  ObjStat_t PixDbHistoAccessor::s_stat("PixDbHistoAccessor");

  class LevelInfo_t : public LevelInfoBase_t
  {
    friend class PixDbHistoAccessor;
  protected:
    LevelInfo_t(const std::string &name, PixLib::DbRecord *record) : LevelInfoBase_t(name) ,m_levelRecord(record) {}
    LevelInfo_t(const std::string &name, unsigned int level_i, PixLib::DbRecord *record) : LevelInfoBase_t(name,level_i),m_levelRecord(record) {}

    PixLib::DbRecord *record()           {return m_levelRecord.get(); }
    std::shared_ptr<PixLib::DbRecord> m_levelRecord;
  };

  void infoMessage(const std::string &label, const std::string &message_head, const std::vector<std::string> &folder_hierarchy)
  {
    std::cout << "INFO [" << label <<"] " << message_head << " Folder : ";
    for (std::vector<std::string>::const_iterator name_iter = folder_hierarchy.begin();
	 name_iter != folder_hierarchy.end();
	 name_iter++) {
      std::cout << *name_iter << " / ";
    }
    std::cout << std::endl;
  }

  void infoMessage(const std::string &label, const std::string &message_head, const HistoInfo_t &info)
  {
    std::cout << "INFO [" << label <<"] " << message_head << " dimensions : ";
    for (unsigned int i=0; i<3; i++) {
      std::cout << info.maxIndex(i) << " ";
    }
    std::cout << info.minHistoIndex() << "-" << info.maxHistoIndex();
    std::cout << std::endl;
  }

  void infoMessage(const std::string &label, const std::string &message_head, const Index_t &index)
  {
    std::cout << "INFO [" << label <<"] " << message_head << " dimensions : ";
    for (Index_t::const_iterator index_iter = index.begin();
	 index_iter != index.end();
	 index_iter++) {
      std::cout << *index_iter << " ";
    }
    std::cout << std::endl;
  }

  void infoMessage(const std::string &label, const std::string &message_head, const std::string &name)
  {
    std::cout << "INFO [" << label <<"] " << message_head << " name  : " << name;
    std::cout << std::endl;
  }


  bool PixDbHistoAccessor::numberOfHistos(const std::vector<std::string> &folder_hierarchy, HistoInfo_t &info )
  {
    infoMessage("PixDbHistoAccessor::numberOfHistos","",folder_hierarchy);
    //    std::shared_ptr<PixLib::DbRecord> a_record_auto;
    PixLib::DbRecord *a_record = getRecord(folder_hierarchy);
    bool ret = numberOfHistos(a_record,folder_hierarchy.back(), info);
    
    return ret;
  }

  bool PixDbHistoAccessor::haveHisto(const std::vector<std::string> &folder_hierarchy) 
  {
    bool ret=true;
    try {
      PixLib::DbRecord *a_record = getRecord(folder_hierarchy);
      if (haveToCleanUp()) {
	delete a_record;
      }
    }
    catch (PixLib::PixDBException &err) {
      ret = false;
    }
    return ret;
  }

  PixLib::Histo* PixDbHistoAccessor::histo(const std::vector<std::string> &folder_hierarchy, const Index_t &index)
  {
    infoMessage("PixDbHistoAccessor::histo","",folder_hierarchy);
    infoMessage("PixDbHistoAccessor::histo","",index);
    PixLib::DbRecord *a_record(getRecord(folder_hierarchy));
    return histo(a_record,folder_hierarchy.back(), index);
  }

  PixLib::DbRecord *PixDbHistoAccessor::getRecord(const std::vector<std::string> &name) {
    if (name.empty()) return NULL;

    std::vector<std::string>::const_iterator folder_iter = name.begin();
    PixLib::dbRecordIterator record_iter = findRecord(*folder_iter);
    //    std::cout << "INFO [PixDbHistoAccessor::getRecord] search record " <<  *folder_iter << " in  " << pathName() << std::endl;
    // recordEnd must be called after findRecord!
    PixLib::dbRecordIterator record_end = recordEnd();
    std::unique_ptr<PixLib::DbRecord> record_auto;
    PixLib::DbRecord *record=nullptr;

    // the last name is processed by number of histos or get histo
    std::string record_path;
    while (record_iter != record_end ) {
      record = ( *(db()->DbProcess(record_iter, PixLib::PixDb::DBREAD)) );
      if (haveToCleanUp()) {
	record_auto = std::unique_ptr<PixLib::DbRecord>(record);
      }
      if (++folder_iter == name.end()) break;

      //      std::cout << "INFO [PixDbHistoAccessor::getRecord] got record " <<  record->getName() << std::endl;
      record_path = record->getDecName();
      record_end = record->recordEnd();
      record_iter = record->findRecord(*folder_iter);
      //      std::cout << "INFO [PixDbHistoAccessor::getRecord] search record " <<  *folder_iter << " in  " << record_path << std::endl;
    }

    if (record_iter == record_end) {
      std::stringstream message;
      message << "FATAL [PixDbHistoAccessor::getRecord] no " ;
      if (folder_iter == name.end()) {
	message << " histogram ";
      }
      message << "record " << *folder_iter;
      if (record) {
	message << " in " << record_path << std::endl;
      }
      message << ".";
      std::cout << message.str() << std::endl;
      record_auto.reset();
      throw PixLib::PixDBException(message.str());
    }
    record_auto.release();
    return record;
  }

  std::string makeRecordName(const std::string &name, const std::string &class_name, bool old ) 
  {
    return ( old ? name + "/" + name : name + "/" + class_name );
  }

  bool PixDbHistoAccessor::numberOfHistos(PixLib::DbRecord* dbi, const std::string &histo_name, HistoInfo_t &info ) 
  {
    info.reset();
    unsigned int min_histo[4]={0,0,0,0};
    unsigned int max_histo[4]={0,0,0,0};


    if(dbi==0) return 0;
    //    if(m_modID<0) return 0; // initial problems, don't even try to do anything

    //  std::cout << "INFO [PixDBData::numberOfHistos] search histograms of type " << histo_name << std::endl;
    if(!histo_name.empty() && histo_name!="none"){

      //std::string hName(histo_name);
      bool old=false;
      if (dbi->getClassName() != "PixScan_Histo" && dbi->getClassName() != "PixScanHisto") {
	old=true;
      }
      //      name += "/PixScan_Histo";
      //      PixLib::dbRecordIterator ri  = dbi->findRecord(name);
      //      if (ri == dbi->recordEnd()) {
      //	name = histo_name + "/" + histo_name;
      //	ri  = dbi->findRecord(name);
      //	old=true;
      //      }

      //      if (ri != dbi->recordEnd()) {
      {
	{
	  PixLib::PixDbInterface *the_db = dbi->getDb();

	  std::stringstream inqname;
	  if (old) {
	    inqname << histo_name;
	  }
	  
	  std::vector<LevelInfo_t> level_stack;
	  level_stack.reserve(4);
	  level_stack.push_back(LevelInfo_t(inqname.str(), dbi) );

	  while (!level_stack.empty()) {

	    // 	  std::cout << "INFO [PixDBData::numberOfHistos] search on level "  << level_stack.back().level()
	    // 		    << " in " << level_stack.back().record()->getName()
	    // 		    << std::endl;

	    if (level_stack.back().index() == 0) {
	      //search histograms
	      // the only possibility is to load all fields and analyse the 
	      // record type to gel all the histograms
	      // this is slow

	      // so it is assumed that the lowest histogram index is smaller than :
	      min_histo[level_stack.back().level()]=0;
	      max_histo[level_stack.back().level()]=0;

	      PixLib::dbFieldIterator field_end = level_stack.back().record()->fieldEnd();
	      std::unique_ptr<PixLib::DbField> a_field_auto;
	      for(PixLib::dbFieldIterator field_iter  =  level_stack.back().record()->fieldBegin();
		  field_iter != field_end;
		  field_iter++) {
		PixLib::DbField *a_field (*(level_stack.back().record()->getDb()->DbProcess(field_iter, PixLib::PixDb::DBREAD)));
		if (haveToCleanUp()) {
		  a_field_auto = std::unique_ptr<PixLib::DbField>(a_field);
		}
		std::string fName(a_field->getName());
		//std::cout << "INFO [PixDbHistoAccessor::numberOfHistos] scrutinise field " << name << std::endl;
		if (fName.size()>level_stack.back().name().size()) {
		  if (isdigit(fName.c_str()[level_stack.back().name().size()])) {
		    unsigned int h_nr=atoi( &( fName.c_str()[level_stack.back().name().size()] ) );
		    //		    std::cout << "INFO [PixDbHistoAccessor::numberOfHistos] id =  " << h_nr << std::endl;
		    if (h_nr< min_histo[level_stack.back().level()] || min_histo[level_stack.back().level()]==max_histo[level_stack.back().level()]) {
		      min_histo[level_stack.back().level()]=h_nr;
		    }
		    if (h_nr>= max_histo[level_stack.back().level()] || max_histo[level_stack.back().level()]==0) {
		      max_histo[level_stack.back().level()]=h_nr+1;
		    }
		  }
		}
	      }
	      //std::cout << "INFO [PixDbHistoAccessor::numberOfHistos] range " <<  min_histo[ level_stack.back().level()] << " - " 
	      //	<< max_histo[ level_stack.back().level()]<< std::endl;

	      if (min_histo[ level_stack.back().level()] < info.minHistoIndex() || info.minHistoIndex()==info.maxHistoIndex())
		info.setMinHistoIndex(min_histo[ level_stack.back().level()] );

	      if (max_histo[ level_stack.back().level() ] > info.maxHistoIndex()) 
		info.setMaxHistoIndex( max_histo[ level_stack.back().level() ]);
	    }

	    if (level_stack.back().level()>=LevelInfo_t::maxLevel()) {
	      level_stack.pop_back();
	      continue;
	    }

	    std::string a_name ( level_stack.back().name() );
	    a_name += LevelInfo_t::levelName( level_stack.back().level() ) ;

	    for (;;) {

	      std::stringstream sub_level_name;
	      sub_level_name << a_name << level_stack.back().index();

// 	      std::cout << "INFO [PixDBData::numberOfHistos] search on level "  << level_stack.back().level()
// 			<< " for sub level " << sub_level_name.str() 
// 			<< " reocrd name = " << makeRecordName(sub_level_name.str(),"PixScanHisto",old)
// 			<< " in recrod " << level_stack.back().record()->getDecName() 
// 			<< std::endl;

	      PixLib::dbRecordIterator ri_a = level_stack.back().record()->findRecord(makeRecordName(sub_level_name.str(),"PixScanHisto",old));
	      level_stack.back().index()++;

	      if (ri_a == level_stack.back().record()->recordEnd()) {
		level_stack.pop_back();
		break;
	      }

	      // there should not be histograms if there are sub levels.
	      assert ( max_histo[ level_stack.back().level() ] == 0);

	      //	      unsigned int inv_level_index= LevelInfo_t::maxLevel()-1 - level_stack.back().level();
	      unsigned int inv_level_index= level_stack.back().level();

	      if (info.maxIndex(inv_level_index) < level_stack.back().index() )
		info.setMaxIndex(inv_level_index,  level_stack.back().index());

	      level_stack.push_back(LevelInfo_t(sub_level_name.str(),level_stack.back().level()+1, *( the_db->DbProcess(ri_a, PixLib::PixDb::DBREAD) )) );
	      break;
	    }

	  }
	}
      }
    }
    //   std::cout << "INFO [PixDBData::numberOfHistos] result : ";
    //   for (unsigned int i =0; i< LevelInfo_t::maxLevel(); i++) {
    //     std::cout << info.maxIndex(i) << ", ";
    //   }
    //   std::cout << info.minHistoIndex() << " - " << info.maxHistoIndex()
    // 	    << std::endl;
    return info.minHistoIndex() <  info.maxHistoIndex();
  }

  PixLib::Histo* PixDbHistoAccessor::histo(PixLib::DbRecord* dbi, const std::string &histo_name, const Index_t &index ) 
  {
    PixLib::Histo *h = 0;
    if(dbi==0 && index.size()>0) return 0;
    std::unique_ptr<PixLib::DbRecord> currRec_auto;
    PixLib::DbRecord *currRec;

    if(!histo_name.empty() && histo_name!="none"){

      std::string name(histo_name);

      bool old=false;
      if (dbi->getClassName() != "PixScan_Histo" && dbi->getClassName() != "PixScanHisto") {
	old=true;
      }
//       name += "/PixScan_Histo";
//       PixLib::dbRecordIterator ri  = dbi->findRecord(name);
//       if (ri == dbi->recordEnd()) {
// 	name = histo_name + "/" + histo_name;
// 	ri  = dbi->findRecord(name);
// 	old=true;
//       }

//      if (ri != dbi->recordEnd()) 
      {
	currRec = dbi; /*std::shared_ptr<PixLib::DbRecord>(*(dbi->getDb()->DbProcess(ri, PixLib::PixDb::DBREAD)));*/
	if (haveToCleanUp()) {
	  currRec_auto = std::unique_ptr<PixLib::DbRecord>(currRec); /*std::shared_ptr<PixLib::DbRecord>(*(dbi->getDb()->DbProcess(ri, PixLib::PixDb::DBREAD)));*/
	}
	PixLib::PixDbInterface *the_db = currRec->getDb();
	  std::stringstream inqname;
	  if (old) {
	    inqname << histo_name << "_";
	  }

	  PixLib::dbRecordIterator ri=currRec->recordEnd();
	  bool has_record=true;
	  std::stringstream fieldname;
	  unsigned int lvl;
	  for(lvl=0;lvl<index.size()-1 ;lvl++){
	    switch(lvl){
	    default:
	    case 0:
	      inqname << "A";
	      break;
	    case 1:
	      inqname << "B";
	      break;
	    case 2:
	      inqname << "C";
	      break;
	    }
	    inqname << index[lvl];
	    ri  = currRec->findRecord(makeRecordName(inqname.str(),"PixScanHisto",old));
	    if (ri == currRec->recordEnd()) {
	      has_record=false;
	      break;
	    }
	    currRec = (*(the_db->DbProcess(ri, PixLib::PixDb::DBREAD)));
	    if (haveToCleanUp()) {
	      currRec_auto = std::unique_ptr<PixLib::DbRecord>(currRec);
	    }
	    inqname << "_";
	  }
	  infoMessage("PixDbHistoAccessor::histo","",inqname.str());
	if (has_record) {
	  // then currRec contains the correct record

	  // ensured at entry :
	  // assert( index.size()>0 );
	  // so not needed.
	  std::cout << "INFO [PixDbHistoAccessor::histo] index : ";
	  for (unsigned int i=0; i<index.size(); i++) {
	    std::cout << index[i] << ", ";
	  }
	  std::cout << " name =  " << currRec->getDecName() << std::endl;
	  inqname << index[index.size()-1]; // what does "_0" mean??
	  PixLib::dbFieldIterator fit = currRec->findField(inqname.str());
	  if (fit != currRec->fieldEnd()) {  // histos on this level
	    //	  fit = dbi->getDb()->DbProcess(fit,PixLib::PixDb::DBREAD);
	    //	  fit.releaseMem();
	    h = new PixLib::Histo();
	    fit = the_db->DbProcess(fit, PixLib::PixDb::DBREAD, *h);
	    fit.releaseMem();
	  }
	}
      }
    }
    
    return h;
  }

}
