#ifndef _Average_t_h_
#define _Average_t_h_

#include <cmath>

namespace PixA {
  /** Provide mean and rms value of a set of values.
   */
  class AverageSimple_t
  {
  public:
    AverageSimple_t() : m_n(0), m_sum(0),m_sum2(0) {}

    /** Consider the given value for the average, rms, min and max.
     * If this function is called after calculate the result is undefined.
     */
    void add(float val) {
      m_n++;
      m_sum+=val;
      m_sum2+=val*val;
    }

    /** Add an other average object.
     * If this function is called after calculate the result is undefined.
     */
    void add(AverageSimple_t &a) {
      m_n+=a.m_n;
      m_sum+=a.m_sum;
      m_sum2+=a.m_sum2;
    }

    //   /** Remove the given value from the mean and rms.
    //    * This function can be used to remove a value which has been added previously by add.
    //    * Note, this will not change the min and max value.
    //    */
    //   void sub(float val) {
    //     m_n--;
    //     m_sum-=val;
    //     m_sum2-=val*val;
    //   }

    /** Calculateulate mean and rms.
     * After this call @ref mean and @ref rms are useful.
     */
    void calculate() {
      if (m_n>1) {
	double mean=m_sum/m_n;
	m_rms=sqrt((m_sum2-m_sum*mean)/(m_n-1));
	m_mean = mean;
      }
    }

    /** Return the sum of the values.
     * Invalid after @ref calculate has bean called
     */
    double sum() const
    {
      return m_sum;
    }

    /** Return the sum of the values squared.
     * Invalid after @ref calculate has bean called
     */
    double sum2() const
    {
      return m_sum2;
    }

    /** Return the number of values.
     */
    unsigned int n() const 
    {
      return m_n;
    }

    /** Return the mean value.
     * Result only valid after @ref calculate has been called.
     */
    double mean() const {
      return m_mean;
    }

    /** Return the rms value.
     * Result only valid after @ref calculate has been called.
     */
    double rms() const {
      return m_rms;
    }

    unsigned int m_n;
    union {
      double m_mean;
      double m_sum;
    };

    union {
      double m_rms;
      double m_sum2;
    };

  };


  /** Provide min, max, mean and rms value of a set of values.
   */
  class Average_t : public AverageSimple_t
  {
  public:
    Average_t() : m_min(FLT_MAX),m_max(-FLT_MAX) {}

    /** Consider the given value for the average, rms, min and max.
     * If this function is called after calculate the result is undefined.
     */
    void add(float val) {
      AverageSimple_t::add(val);
      if (val>m_max) m_max=val;
      if (val<m_min) m_min=val;
    }

    /** Add an other average object.
     * If this function is called after calculate the result is undefined.
     */
    void add(Average_t &a) {
      AverageSimple_t::add(a);
      if (a.m_max>m_max) m_max=a.m_max;
      if (a.m_min<m_min) m_min=a.m_min;
    }


    /** Return the minimum value;
     */
    float min() const {
      return m_min;
    }

    /** Return the maximum value;
     */
    float max() const {
      return m_max;
    }

    float m_min;
    float m_max;

  };

}
#endif
