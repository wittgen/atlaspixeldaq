#ifndef _PixA_RootResultFileAccessBase_h_
#define _PixA_RootResultFileAccessBase_h_

#include <iostream>
#include <vector>
#include <set>

#include "HistoInfo_t.h"

#include "TROOT.h"
#include "TClass.h"

#include <ConfigWrapper/pixa_exception.h>

class TFile;
class TDirectory;
class TH1;

namespace PixA {

  class Regex_t;

  class MissingResultDirectory : public BaseException {
  public:
    MissingResultDirectory(const std::string &message) : BaseException(message) {}
  };

  class NoResultFile : public MissingResultDirectory {
  public:
    NoResultFile(const std::string &message) : MissingResultDirectory(message) {}
  };

  /** Class which determines whether a directory is a filled pix scan histogram directory.
   * The class will be called for each top level pix scan histogram directory. The
   * method contains histogram should return true if the given directory looks
   * like a valid,non-empty pix scan histogram directory.
   */
  class IHistogramDirectoryVerifier {
  public:
    enum EConnItemType {kRodItem, kModuleItem, kNConnItemTypes };
    virtual ~IHistogramDirectoryVerifier() {}
    virtual bool containsHistograms(EConnItemType type, const std::string &conn_name, TDirectory *histo_directory) = 0;
  };

  /** Class to facilitate the acces of the new Root files.
   */
  class RootResultFileAccessBase
  {
  public:

    typedef unsigned int SerialNumber_t;
    enum ESerialNumberType {kScanNumber, kAnalysisNumber, kNSerialNumberTypes};

    /** Verify whether the given class is a derivative of the given base class.
     * @param class_name the name of the derived class
     * @param a_class the base class
     * @param verbose will become more verbose if set to true.
     */
    static bool inheritsFrom(const char *class_name, const TClass *a_class, bool verbose) {
      TClass *given_class = gROOT->GetClass(class_name);
      if (given_class) {
	if (given_class->InheritsFrom(a_class)) {
	  return true;
	}
      }
      else {
	if (verbose) {
	  std::cout << "WARNING [RootFileHistogramProvider::inheritsFrom] class "  << class_name << " does not exist." << std::endl;
	}
      }
      return false;
    }

    /** Determine the name of the result file.
     * @param file_name the first file name candidate
     * @param alternate_file_name the second file name candidate
     * Verifies the existence of the file_name. First it will start with file_name,
     * then with alternate_file_name. If neither file_name nor alternate_file_name
     * exists, the path is stripped off and replaced by the path name of the 
     * file name template (or pathe name) which is stored in the environment variable
     * defined in @ref s_pixScanStoragePathEnv.
     */
    static std::string fileName(const std::string &file_name, const std::string &alternate_file_name);

    /** Open the given file and enter the directory for the given scan.
     * @param file_name the name of the file
     * @param serial_number_type the type of the serial number i.e scan or analysis.
     * @param scan_serial_number the serial number of the scan.
     */
    static std::pair<TFile *, TDirectory *> open(const std::string &file_name,
						 ESerialNumberType serial_number_type,
						 SerialNumber_t scan_serial_number);

    /** Enter the given sub directory.
     * @param a_directory the parent directory
     * @param dir_name the name of the sub-directory.
     */
    static TDirectory *enter(TDirectory *a_directory, const std::string &dir_name);


    /** Enter the directory for a given ROD, module and histogram.
     * @param scan_directory the driectory of a scan.
     * @param rod_name the connectivity name of the ROD.
     * @param module_name the connectivity name of the module or an empty string.
     * @param histo_name the name of the histogram e.g. SCURVE_MEAN, OCCUPANCY etc. .
     */
    static TDirectory *enter(TDirectory *scan_directory,
			     const std::string &rod_name,
			     const std::string &module_name,
			     const std::string &histo_name);

    /** Get a the list of histograms which exist in the given scan directory for RODs and modules.
     * @param scan_directory the directory of a scan
     * @param rod_histogram_list_out the list of histograms which exist per ROD.
     * @param module_histogram_list_out the list of histograms which exist per module.
     * @param verbose be more verbose when set to true.
     */
    static void histogramList(TDirectory *scan_directory,
			      std::set<std::string> &rod_histogram_list_out,
			      std::set<std::string> &module_histogram_list_out,
			      bool verbose);

    /** Get a the list of histograms which exist in the given scan directory for RODs and modules.
     * @param scan_directory the directory of a scan
     * @param rod_histogram_list_out the list of histograms which exist per ROD.
     * @param module_histogram_list_out the list of histograms which exist per module.
     * @param listener a helper method which can be used to count the histograms per connectivity object.
     * @param verbose be more verbose when set to true.
     * 
     */
    static void histogramList(TDirectory *scan_directory,
			      std::set<std::string> &rod_histogram_list_out,
			      std::set<std::string> &module_histogram_list_out,
			      IHistogramDirectoryVerifier *verifier,
			      bool verbose);

    /** Count the number of histograms in the given subdirectory.
     * @param histo_dir the top level directory of a pix scan hist which contains the scan loop subdirectories e.g. Ai/Bj/../n:.. .
     * @param histo_info will be filled with the number of histograms.
     * @param verbose be more verbose when set to true.
     */
    static void numberOfHistograms(TDirectory *a_directory, HistoInfo_t &histo_info, bool verbose);

    /** Return true if the given directory contains in at least one subdirectory a histogram.
     * @param a_directory the parent directory.
     * @param verbose be more verbose.
     */
    static bool containsHistograms(TDirectory *a_directory, bool verbose);

    /** Get the histogram with the given index from the given directory.
     * @param histo_dir the top level directory of a pix scan hist which contains the scan loop subdirectories e.g. Ai/Bj/../n:.. .
     * @param index the index of the histogram in the pix scan histo multi dimensional histogram array.
     * @param verbose be more verbose if set to true.
     */
    static TH1 *histogram(TDirectory *histo_dir,
			  const Index_t &index,
			  bool verbose);
 
 private:
    /** Method which defines the connectivity name patterns.
     */
    static void init();

    static const char *s_pixScanStoragePathEnv;
    static const char  s_serialNumberTypeId[kNSerialNumberTypes];
    static std::vector< Regex_t* > s_modulePattern;
    static std::vector< Regex_t* > s_rodPattern;

  };

}
#endif
