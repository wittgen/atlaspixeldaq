#ifndef _HistInfo_t_h_
#define _HistInfo_t_h_

class HistInfo_t
{
 public:
  void reset() {
    for (unsigned int i=0; i<4; i++) {m_maxIndex[i]=0;} 
    m_minHistoIndex = 0;
  }


  void setMinHistoIndex(unsigned int min_histo_index) {m_minHistoIndex = min_histo_index;}
  void setMaxHistoIndex(unsigned int max_histo_index) {m_maxIndex[3]   = max_histo_index;}

  unsigned int &minHistoIndex()             { return m_minHistoIndex ;}
  const unsigned int &minHistoIndex() const { return m_minHistoIndex ;}

  const unsigned int &maxHistoIndex() const { return m_maxIndex[3]   ;}
  unsigned int &maxHistoIndex()             { return m_maxIndex[3]   ;}


  void makeUnion(const HistInfo_t &a_info) {
    if (m_minHistoIndex>=m_maxIndex[3] || m_minHistoIndex>a_info.m_minHistoIndex) {
      m_minHistoIndex=a_info.m_minHistoIndex;
    }
    for (unsigned int i=0; i<4; i++) {
      if (m_maxIndex[i]<a_info.m_maxIndex[i]) m_maxIndex[i]=a_info.m_maxIndex[i];
    }
  }

  void setMaxIndex(unsigned int level, unsigned int max_histo_index) {m_maxIndex[level]   = max_histo_index;}

  const unsigned int &maxIndex(unsigned int level) const {return m_maxIndex[level];}
  unsigned int &maxIndex(unsigned int level)             {return m_maxIndex[level];}

  unsigned int m_maxIndex[4];
  unsigned int m_minHistoIndex;
};


#endif
