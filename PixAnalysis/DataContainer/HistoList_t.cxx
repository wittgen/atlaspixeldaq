#include <memory>



#include "HistoList_t.h"

namespace PixA {

  void HistoList_t::addHisto(const std::string &histo_name, const PixA::ScanConfigRef &scan_config, const std::string &module_name, const PixA::HistoHandle &histo_handle) {
    unsigned int id = createHistoId(histo_name);
    std::cout << "INFO [HistoList_t::addHisto] Add " << histo_name << " , id = " << id << ", for module " << module_name << std::endl;
    PixScanHistoElmPtr_t histo_list = createModuleHistoList(module_name,scan_config);
    if (histo_list.get()) {
      histo_list->addHisto(id, histo_handle);
    }
    else {
      throw std::runtime_error("Failed to add histo to list." );
    }
  }

  void HistoList_t::addPlainHisto(const std::string &histo_name,
				  const PixA::ScanConfigRef &scan_config,
				  const std::string &module_name,
				  const PixA::HistoHandle &histo_handle) {
    unsigned int id = createHistoId(histo_name);
    std::cout << "INFO [HistoList_t::addHisto] Add " << histo_name << " , id = " << id << ", for module " << module_name << std::endl;
    PixScanHistoElmPtr_t histo_list = createModuleHistoList(module_name,scan_config);
    if (histo_list.get()) {
      histo_list->addHisto(id, histo_handle);
      histo_list->setModuleNameingScheme(PixA::kPlainNaming,static_cast<unsigned int>(-1));
    }
    else {
      throw std::runtime_error("Failed to add histo to list." );
    }
  }


  unsigned int HistoList_t::createHistoId(const std::string &histo_name)  {
    unsigned int histo_id = findHistoName(histo_name);
    if (histo_id >= m_histoMap.size()) {
      histo_id = m_histoMap.size();
      m_histoMap.push_back(histo_name);
    }
    return histo_id;
  }

  PixScanHistoElmPtr_t HistoList_t::createModuleHistoList(const std::string &module_name, const PixA::ScanConfigRef &scan_config) {
    ModuleIndex_t::iterator module_iter = m_moduleIndex.find(module_name);
    if (module_iter == m_moduleIndex.end()) {
      std::pair<ModuleIndex_t::iterator, bool> ret = m_moduleIndex.insert(make_pair(module_name, std::make_shared<PixScanHistoElm_t>(scan_config)));
      if (!ret.second) {
	throw std::runtime_error("Failed to add module to list." );
      }
      module_iter = ret.first;
    }
    assert( module_iter->second );
    //      if (!module_iter->second) {
    //	module_iter->second = PixScanHistoElmPtr_t(new PixScanHistoElm_t(scan_config));
    //      }
    return module_iter->second;
  }


  std::string PixScanHistoElm_t::stripPath(const std::string &histo_name)
  {
    std::string::size_type pos = histo_name.rfind("/");
    if (pos != std::string::npos && pos+1 < histo_name.size()) {
      return std::string(histo_name,pos+1,histo_name.size()-pos-1);
    }
    else {
      return histo_name;
    }
  }

}

