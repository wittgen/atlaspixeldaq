/********************************************************************************
** Form generated from reading UI file 'LinkPanelBase.ui'
**
** Created by: Qt User Interface Compiler version 4.8.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LINKPANELBASE_H
#define UI_LINKPANELBASE_H

#include <Qt3Support/Q3ButtonGroup>
#include <Qt3Support/Q3Header>
#include <Qt3Support/Q3ListView>
#include <Qt3Support/Q3MimeSourceFactory>
#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QRadioButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_LinkPanelBase
{
public:
    QVBoxLayout *vboxLayout;
    QLabel *m_titleLable;
    Q3ButtonGroup *buttonGroup2;
    QVBoxLayout *vboxLayout1;
    QHBoxLayout *hboxLayout;
    QLabel *textLabel2;
    QRadioButton *m_groupModuleButton;
    QRadioButton *m_groupDspButton;
    QRadioButton *m_groupLinkButton;
    QRadioButton *m_groupPluginButton;
    QSpacerItem *spacer2;
    Q3ListView *m_linkView;

    void setupUi(QDialog *LinkPanelBase)
    {
        if (LinkPanelBase->objectName().isEmpty())
            LinkPanelBase->setObjectName(QString::fromUtf8("LinkPanelBase"));
        LinkPanelBase->resize(765, 480);
        vboxLayout = new QVBoxLayout(LinkPanelBase);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        m_titleLable = new QLabel(LinkPanelBase);
        m_titleLable->setObjectName(QString::fromUtf8("m_titleLable"));
        QFont font;
        font.setPointSize(14);
        font.setBold(true);
        m_titleLable->setFont(font);
        m_titleLable->setMargin(10);
        m_titleLable->setWordWrap(false);

        vboxLayout->addWidget(m_titleLable);

        buttonGroup2 = new Q3ButtonGroup(LinkPanelBase);
        buttonGroup2->setObjectName(QString::fromUtf8("buttonGroup2"));
        buttonGroup2->setFrameShadow(Q3GroupBox::Plain);
        buttonGroup2->setLineWidth(0);
        buttonGroup2->setFlat(true);
        buttonGroup2->setColumnLayout(0, Qt::Vertical);
        buttonGroup2->layout()->setSpacing(6);
        buttonGroup2->layout()->setContentsMargins(11, 11, 11, 11);
        vboxLayout1 = new QVBoxLayout();
        QBoxLayout *boxlayout = qobject_cast<QBoxLayout *>(buttonGroup2->layout());
        if (boxlayout)
            boxlayout->addLayout(vboxLayout1);
        vboxLayout1->setAlignment(Qt::AlignTop);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        textLabel2 = new QLabel(buttonGroup2);
        textLabel2->setObjectName(QString::fromUtf8("textLabel2"));
        textLabel2->setWordWrap(false);

        hboxLayout->addWidget(textLabel2);

        m_groupModuleButton = new QRadioButton(buttonGroup2);
        m_groupModuleButton->setObjectName(QString::fromUtf8("m_groupModuleButton"));
        buttonGroup2->insert(m_groupModuleButton, -1);

        hboxLayout->addWidget(m_groupModuleButton);

        m_groupDspButton = new QRadioButton(buttonGroup2);
        m_groupDspButton->setObjectName(QString::fromUtf8("m_groupDspButton"));
        buttonGroup2->insert(m_groupDspButton, -1);

        hboxLayout->addWidget(m_groupDspButton);

        m_groupLinkButton = new QRadioButton(buttonGroup2);
        m_groupLinkButton->setObjectName(QString::fromUtf8("m_groupLinkButton"));
        m_groupLinkButton->setChecked(true);
        buttonGroup2->insert(m_groupLinkButton, -1);

        hboxLayout->addWidget(m_groupLinkButton);

        m_groupPluginButton = new QRadioButton(buttonGroup2);
        m_groupPluginButton->setObjectName(QString::fromUtf8("m_groupPluginButton"));
        buttonGroup2->insert(m_groupPluginButton, -1);

        hboxLayout->addWidget(m_groupPluginButton);

        spacer2 = new QSpacerItem(360, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacer2);


        vboxLayout1->addLayout(hboxLayout);


        vboxLayout->addWidget(buttonGroup2);

        m_linkView = new Q3ListView(LinkPanelBase);
        m_linkView->addColumn(QApplication::translate("LinkPanelBase", "Name", 0, QApplication::UnicodeUTF8));
        m_linkView->header()->setClickEnabled(true, m_linkView->header()->count() - 1);
        m_linkView->header()->setResizeEnabled(true, m_linkView->header()->count() - 1);
        m_linkView->addColumn(QApplication::translate("LinkPanelBase", "ModID", 0, QApplication::UnicodeUTF8));
        m_linkView->header()->setClickEnabled(true, m_linkView->header()->count() - 1);
        m_linkView->header()->setResizeEnabled(true, m_linkView->header()->count() - 1);
        m_linkView->addColumn(QApplication::translate("LinkPanelBase", "DTO", 0, QApplication::UnicodeUTF8));
        m_linkView->header()->setClickEnabled(true, m_linkView->header()->count() - 1);
        m_linkView->header()->setResizeEnabled(true, m_linkView->header()->count() - 1);
        m_linkView->addColumn(QApplication::translate("LinkPanelBase", "Plugin", 0, QApplication::UnicodeUTF8));
        m_linkView->header()->setClickEnabled(true, m_linkView->header()->count() - 1);
        m_linkView->header()->setResizeEnabled(true, m_linkView->header()->count() - 1);
        m_linkView->addColumn(QApplication::translate("LinkPanelBase", "Fibre", 0, QApplication::UnicodeUTF8));
        m_linkView->header()->setClickEnabled(true, m_linkView->header()->count() - 1);
        m_linkView->header()->setResizeEnabled(true, m_linkView->header()->count() - 1);
        m_linkView->addColumn(QApplication::translate("LinkPanelBase", "Outlink", 0, QApplication::UnicodeUTF8));
        m_linkView->header()->setClickEnabled(true, m_linkView->header()->count() - 1);
        m_linkView->header()->setResizeEnabled(true, m_linkView->header()->count() - 1);
        m_linkView->addColumn(QApplication::translate("LinkPanelBase", "DSP", 0, QApplication::UnicodeUTF8));
        m_linkView->header()->setClickEnabled(true, m_linkView->header()->count() - 1);
        m_linkView->header()->setResizeEnabled(true, m_linkView->header()->count() - 1);
        m_linkView->setObjectName(QString::fromUtf8("m_linkView"));

        vboxLayout->addWidget(m_linkView);


        retranslateUi(LinkPanelBase);
        QObject::connect(m_groupLinkButton, SIGNAL(toggled(bool)), LinkPanelBase, SLOT(orderByLink(bool)));
        QObject::connect(m_groupPluginButton, SIGNAL(toggled(bool)), LinkPanelBase, SLOT(orderByPlugin(bool)));
        QObject::connect(m_groupModuleButton, SIGNAL(toggled(bool)), LinkPanelBase, SLOT(orderByModule(bool)));
        QObject::connect(m_groupDspButton, SIGNAL(toggled(bool)), LinkPanelBase, SLOT(orderByDsp(bool)));

        QMetaObject::connectSlotsByName(LinkPanelBase);
    } // setupUi

    void retranslateUi(QDialog *LinkPanelBase)
    {
        LinkPanelBase->setWindowTitle(QApplication::translate("LinkPanelBase", "Link Panel", 0, QApplication::UnicodeUTF8));
        m_titleLable->setText(QApplication::translate("LinkPanelBase", "<ROD>", 0, QApplication::UnicodeUTF8));
        buttonGroup2->setTitle(QString());
        textLabel2->setText(QApplication::translate("LinkPanelBase", "Group by :", 0, QApplication::UnicodeUTF8));
        m_groupModuleButton->setText(QApplication::translate("LinkPanelBase", "Module", 0, QApplication::UnicodeUTF8));
        m_groupDspButton->setText(QApplication::translate("LinkPanelBase", "DSP", 0, QApplication::UnicodeUTF8));
        m_groupLinkButton->setText(QApplication::translate("LinkPanelBase", "Link", 0, QApplication::UnicodeUTF8));
        m_groupPluginButton->setText(QApplication::translate("LinkPanelBase", "Plugin", 0, QApplication::UnicodeUTF8));
        m_linkView->header()->setLabel(0, QApplication::translate("LinkPanelBase", "Name", 0, QApplication::UnicodeUTF8));
        m_linkView->header()->setLabel(1, QApplication::translate("LinkPanelBase", "ModID", 0, QApplication::UnicodeUTF8));
        m_linkView->header()->setLabel(2, QApplication::translate("LinkPanelBase", "DTO", 0, QApplication::UnicodeUTF8));
        m_linkView->header()->setLabel(3, QApplication::translate("LinkPanelBase", "Plugin", 0, QApplication::UnicodeUTF8));
        m_linkView->header()->setLabel(4, QApplication::translate("LinkPanelBase", "Fibre", 0, QApplication::UnicodeUTF8));
        m_linkView->header()->setLabel(5, QApplication::translate("LinkPanelBase", "Outlink", 0, QApplication::UnicodeUTF8));
        m_linkView->header()->setLabel(6, QApplication::translate("LinkPanelBase", "DSP", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class LinkPanelBase: public Ui_LinkPanelBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LINKPANELBASE_H
