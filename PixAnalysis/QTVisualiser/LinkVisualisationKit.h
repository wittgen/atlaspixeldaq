#ifndef _LinkVisualisationKit_h_
#define _LinkVisualisationKit_h_

#include <Visualiser/IVisualisationKit.h>

#include <string>
#include <map>
#include <iostream>

class QWidget;

void registerLinkVisualiser(QWidget *top_win, unsigned int a_priority, bool only_if_lower_priorotiy_kits=true);

#endif
