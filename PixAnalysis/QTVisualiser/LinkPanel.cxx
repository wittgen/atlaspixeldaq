#include "LinkPanel.h"
//#include <ConfigWrapper/Connectivity.h>
//#include <ConfigWrapper/PixDisableUtil.h>

#include <QTreeWidget>
//#include <QPainter>
#include <iomanip>

namespace PixA {
  /** execute a function for all enabled modules.
   * The function object UnaryFunction_tmpl must contain a method:
   * <verb>
   *     void operator()( const PixA::ModuleList::const_iterator &module_iter) ;
   * </verb>
   */
  template <class UnaryFunction_templ>
  inline void for_each_module(const PixA::DisabledList module_disabled_list,
		       PixA::ModuleList::const_iterator module_list_begin,
		       PixA::ModuleList::const_iterator module_list_end,
		       UnaryFunction_templ &a_function) {

    for (PixA::ModuleList::const_iterator module_iter=module_list_begin;
	 module_iter != module_list_end;
	 ++module_iter) {

      if (!PixA::enabled( module_disabled_list, module_iter )) continue;
      a_function( module_iter );
    }
  }

  /** execute a function for all enabled modules.
   * The function object UnaryFunction_tmpl must contain a method:
   * <verb>
   *     void operator()( const PixA::ModuleList::const_iterator &module_iter) ;
   * </verb>
   */
  template<class UnaryFunction_templ>
  inline void for_each_module(const PixA::DisabledList pp0_disabled_list,
		       PixA::Pp0List::const_iterator pp0_list_begin,
		       PixA::Pp0List::const_iterator pp0_list_end,
		       UnaryFunction_templ &a_function) {

    for (PixA::Pp0List::const_iterator pp0_iter=pp0_list_begin;
	 pp0_iter != pp0_list_end;
	 ++pp0_iter) {

      if (!PixA::enabled( pp0_disabled_list, pp0_iter )) continue;
      const PixA::DisabledList module_disabled_list( moduleDisabledList(pp0_disabled_list, pp0_iter) );

      PixA::ModuleList::const_iterator module_list_begin=modulesBegin(pp0_iter);
      PixA::ModuleList::const_iterator module_list_end=modulesEnd(pp0_iter);

      for_each_module(module_disabled_list, module_list_begin, module_list_end, a_function);

    }
  }



  /** Execute a function for all enabled PP0s.
   * The function object UnaryFunction_tmpl must contain a method:
   * <verb>
   *     void operator()( const PixA::Pp0List::const_iterator &pp0_iter) ;
   * </verb>
   */
  template<class UnaryFunction_templ>
  inline void for_each_pp0(const PixA::DisabledList pp0_disabled_list,
			   PixA::Pp0List::const_iterator pp0_list_begin,
			   PixA::Pp0List::const_iterator pp0_list_end,
			   UnaryFunction_templ &a_function) {

    for (PixA::Pp0List::const_iterator pp0_iter=pp0_list_begin;
	 pp0_iter != pp0_list_end;
	 ++pp0_iter) {

      if (!PixA::enabled( pp0_disabled_list, pp0_iter )) continue;
      a_function(pp0_iter);
    }
  }


  /** execute a function for all enabled modules.
   * The function object UnaryFunction_tmpl must contain a method:
   * <verb>
   *     void operator()( const PixA::ModuleList::const_iterator &module_iter) ;
   * </verb>
   */
  template<class UnaryFnction_templ>
  inline void for_each_module(const PixA::ConnectivityRef &conn,
			      const PixA::RODLocationBase &rod_location,
			      const PixA::DisabledList &pp0_disabled_list,
			      UnaryFnction_templ &a_function) {
    assert(conn);
    const PixA::Pp0List pp0_list( conn.pp0s(rod_location) );
    PixA::Pp0List::const_iterator pp0_list_begin=pp0_list.begin();
    PixA::Pp0List::const_iterator pp0_list_end=pp0_list.end();

    for_each_module(pp0_disabled_list, pp0_list_begin, pp0_list_end,a_function);
  }

  /** Execute a function for all enabled PP0s.
   * The function object UnaryFunction_tmpl must contain a method:
   * <verb>
   *     void operator()( const PixA::Pp0List::const_iterator &pp0_iter) ;
   * </verb>
   */
  template<class UnaryFnction_templ>
  inline void for_each_pp0(const PixA::ConnectivityRef &conn,
			   const PixA::RODLocationBase &rod_location,
			   const PixA::DisabledList &pp0_disabled_list,
			   UnaryFnction_templ &a_function) {

      const PixA::Pp0List pp0_list( conn.pp0s(rod_location) );
      PixA::Pp0List::const_iterator pp0_list_begin=pp0_list.begin();
      PixA::Pp0List::const_iterator pp0_list_end=pp0_list.end();

      for_each_pp0(pp0_disabled_list, pp0_list_begin, pp0_list_end,a_function);
  }
}

LinkPanel::LinkPanel(const PixA::ConnectivityRef &conn,
		     const PixA::DisabledListRoot &root_disable_list,
		     const std::string &rod_name,
		     QWidget* parent)
: QDialog(parent),
    m_rodName(rod_name),
    m_conn(conn),
    m_disabledList( PixA::createPp0DisabledList(m_conn, root_disable_list, rod_name) )
{
    setupUi(this );
  std::string title("Link Map : ");
  title += m_rodName;
  m_titleLable->setText(title.c_str() );
  if (m_groupLinkButton->isChecked()) {
    orderByLink(true);
  }
  else if (m_groupPluginButton->isChecked()) {
    orderByPlugin(true);
  }
  else if (m_groupModuleButton->isChecked()) {
    orderByModule(true);
  }

  m_linkView->adjustSize();
  adjustSize();
}

LinkPanel::~LinkPanel() {}


class LinkOrder  : std::unary_function< PixA::ModuleList::const_iterator, void>
{
public:
  enum ELinkType {k1A, k1B, k2A, k2B, kTx};

  typedef std::pair<std::pair<int,ELinkType>, const PixLib::ModuleConnectivity *> LinkElement_t;
  typedef std::vector<LinkElement_t> LinkList_t;
  typedef std::map<int, LinkList_t> LinkMap_t;

  void operator()( const PixA::ModuleList::const_iterator &module_iter)  {
    addLink(const_cast<PixLib::ModuleConnectivity *>(*module_iter)->outLink1A(), k1A, module_iter);
    addLink(const_cast<PixLib::ModuleConnectivity *>(*module_iter)->outLink1B(), k1B, module_iter);
    addLink(const_cast<PixLib::ModuleConnectivity *>(*module_iter)->outLink2A(), k2A, module_iter);
    addLink(const_cast<PixLib::ModuleConnectivity *>(*module_iter)->outLink2B(), k2B, module_iter);
  }

  LinkMap_t::const_iterator begin() const {return m_list.begin(); }
  LinkMap_t::const_iterator end()   const {return m_list.end(); }

private:
  void addLink(int link, ELinkType type, const PixA::ModuleList::const_iterator &module_iter) {
    std::cout << "INFO [LinkOrder::addLink] " << link << " " << static_cast<unsigned int>(type) << " " << PixA::connectivityName(module_iter) << std::endl;
    if (link>=0) {
      m_list[link].push_back(std::make_pair(std::make_pair(link,type), *module_iter));
    }
  }
  LinkMap_t m_list;
};


class DspOrder  : std::unary_function< PixA::ModuleList::const_iterator, void>
{
public:
  typedef std::map<unsigned int, LinkOrder::LinkList_t> DspMap_t;

  void operator()( const PixA::ModuleList::const_iterator &module_iter)  {
    addLink(const_cast<PixLib::ModuleConnectivity *>(*module_iter)->outLink1A(), LinkOrder::k1A, module_iter);
    addLink(const_cast<PixLib::ModuleConnectivity *>(*module_iter)->outLink1B(), LinkOrder::k1B, module_iter);
    addLink(const_cast<PixLib::ModuleConnectivity *>(*module_iter)->outLink2A(), LinkOrder::k2A, module_iter);
    addLink(const_cast<PixLib::ModuleConnectivity *>(*module_iter)->outLink2B(), LinkOrder::k2B, module_iter);
  }

  DspMap_t::const_iterator begin() const {return m_list.begin(); }
  DspMap_t::const_iterator end()   const {return m_list.end(); }

private:
  void addLink(int link, LinkOrder::ELinkType type, const PixA::ModuleList::const_iterator &module_iter) {
    std::cout << "INFO [LinkOrder::addLink] " << link << " " << static_cast<unsigned int>(type) << " " << PixA::connectivityName(module_iter) << std::endl;
    if (link>=0) {
      m_list[PixA::groupId(module_iter)].push_back(std::make_pair(std::make_pair(link,type), *module_iter));
    }
  }
  DspMap_t m_list;
};


class ModuleOrder  : std::unary_function< PixA::ModuleList::const_iterator, void>
{
public:
  typedef std::map<unsigned int, LinkOrder::LinkList_t> ModuleMap_t;

  void operator()( const PixA::ModuleList::const_iterator &module_iter)  {
    addLink(const_cast<PixLib::ModuleConnectivity *>(*module_iter)->outLink1A(), LinkOrder::k1A, module_iter);
    addLink(const_cast<PixLib::ModuleConnectivity *>(*module_iter)->outLink1B(), LinkOrder::k1B, module_iter);
    addLink(const_cast<PixLib::ModuleConnectivity *>(*module_iter)->outLink2A(), LinkOrder::k2A, module_iter);
    addLink(const_cast<PixLib::ModuleConnectivity *>(*module_iter)->outLink2B(), LinkOrder::k2B, module_iter);
    addLink(0/*ignored*/, LinkOrder::kTx, module_iter);
  }

  ModuleMap_t::const_iterator begin() const {return m_list.begin(); }
  ModuleMap_t::const_iterator end()   const {return m_list.end(); }

private:
  void addLink(int link, LinkOrder::ELinkType type, const PixA::ModuleList::const_iterator &module_iter) {
    if (link>=0) {
      m_list[PixA::modId(module_iter)].push_back(std::make_pair(std::make_pair(link,type), *module_iter));
    }
  }
  ModuleMap_t m_list;
};


  class LinkItem : public QTreeWidgetItem
  {
  public:
    LinkItem(QTreeWidget *parent, QTreeWidgetItem *last_item, unsigned int child_counter)
      : QTreeWidgetItem(parent,last_item),
	m_even( (child_counter & 1)==0)
    {}
    LinkItem(QTreeWidget *parent, unsigned int child_counter)
      : QTreeWidgetItem(parent),
	m_even( (child_counter & 1)==0)
    {}
    LinkItem(QTreeWidgetItem *parent, QTreeWidgetItem *last_item, unsigned int child_counter)
      : QTreeWidgetItem(parent,last_item),
	m_even( (child_counter & 1)==0)
    {}
    LinkItem(QTreeWidgetItem *parent, unsigned int child_counter)
      : QTreeWidgetItem(parent),
	m_even( (child_counter & 1)==0)
    {}

    void setChildIndex(unsigned int child_counter) {
      m_even=((child_counter & 1)==0);
    }

  protected:
    /* apparently not used and needs removal of QColorGroup towards pure usage of QPainter for QT4
    void paintCell ( QPainter * painter, const QColorGroup & colour_group, int column, int width, int align )
    {
      painter->save();
      if ( isSelected() ) {
        QColorGroup new_colour_group(colour_group);
	new_colour_group.setColor(QColorGroup::Base, new_colour_group.highlight() );
	new_colour_group.setColor(QColorGroup::Text, new_colour_group.highlightedText() );
        Q3ListViewItem::paintCell(painter, new_colour_group, column, width, align);
      }
      else if (m_even) {
        QColorGroup new_colour_group(colour_group);
	new_colour_group.setColor(QColorGroup::Base, s_background );
        Q3ListViewItem::paintCell(painter, new_colour_group, column, width, align);
      }
      else {
        Q3ListViewItem::paintCell(painter, colour_group, column, width, align);
      }
      painter->restore();
    }
      */
  private:
    bool  m_even;
    static QColor s_background;
  };

  QColor LinkItem::s_background=QColor(static_cast<unsigned int>(255*.93),
				       static_cast<unsigned int>(255*.93),
				       static_cast<unsigned int>(255*.93));


class AddLinkItems : std::unary_function< LinkOrder::LinkList_t, void>
{
public:
  enum EColumn {kFormater, kDsp, kModule, kProdName, kModuleId, kPlugin, kNColumns};

  AddLinkItems( QTreeWidget *list_view, QTreeWidgetItem *parent, EColumn first_column)
  : m_listView(list_view),
    m_parent(parent),
    m_lastItem(NULL),
    m_firstColumn(first_column),
    m_nColumns(0),
    m_elements(0)
  {
    std::cout << "INFO [AddLinkItems::ctor] " << std::endl;
    assert(m_listView);
    setupColumnMap(first_column);

    setColumnTitle(kFormater, "Fmt:Link");
    setColumnTitle(kDsp, "DSP");

    setColumnTitle(kModule, "Module");
    setColumnTitle(kProdName, "Prod. name");
    setColumnTitle(kModuleId, "Module ID");

    setColumnTitle(kPlugin, "Plugin");
    //    setColumnTitle(kPluginChannel, "Channel");
    if (m_firstColumn != kFormater) {
      setColumnTitle(m_firstColumn, "Name");
    }
    //    m_listView->setItemMargin(3);
    m_listView->setSortingEnabled(true);
    m_listView->sortItems(0, Qt::AscendingOrder);
  }
  AddLinkItems( AddLinkItems &a)
    : m_listView(a.m_listView),
      m_parent(a.m_parent),
      m_lastItem(a.m_lastItem),
      m_firstColumn(a.m_firstColumn),
      m_nColumns(a.m_nColumns),
      m_elements(a.m_nColumns),
      m_column(a.m_column)
  {
    std::cout << "INFO [AddLinkItems::ctor] copy" << std::endl;
  }

  void setParent(QTreeWidgetItem *parent) { m_parent = parent; };

  void operator()( const LinkOrder::LinkElement_t &link_def)  {

    QTreeWidgetItem *new_item = createItem();
    std::cout << "INFO [AddLinkItems::operator()] ENTRY elements = " << m_elements << std::endl;
    std::stringstream link;
    link << std::setw(2) << (link_def.first.first / 0xf) << ":" << std::setfill('0')  << std::setw(2) << (link_def.first.first % 0xf);
    setColumnText(new_item, kFormater, link.str().c_str());
    if (m_firstColumn == kModule) {
      std::stringstream module_name;
      module_name << PixA::modId(link_def.second) << ": " << PixA::connectivityName(link_def.second).c_str() << " (" << PixA::productionName(link_def.second).c_str() << ")";
      setColumnText(new_item, kModule, PixA::connectivityName(link_def.second).c_str());
    }
    else {
      setColumnText(new_item, kModule, PixA::connectivityName(link_def.second).c_str());
      setColumnText(new_item, kProdName, PixA::productionName(link_def.second).c_str());
      setColumnText(new_item, kModuleId, QString::number(PixA::modId(link_def.second)).toLatin1().data());
    }


    PixA::ModulePluginConnection tx_connection = PixA::txConnection(link_def.second);
    PixA::ModulePluginConnection rx_connection_dto1 = PixA::rxConnection(link_def.second, PixA::DTO1);
    PixA::ModulePluginConnection rx_connection_dto2 = PixA::rxConnection(link_def.second, PixA::DTO2);
    PixA::ModulePluginConnection connection = tx_connection;

    std::stringstream plugin_name;
    if (link_def.first.second != LinkOrder::kTx) {
      setColumnText(new_item, kDsp, QString::number(PixA::groupId(link_def.second)).toLatin1().data());
      if (link_def.first.second == LinkOrder::k1A || link_def.first.second == LinkOrder::k1B) {
	connection = rx_connection_dto2; //correct ?
      }
      else {
	connection = rx_connection_dto1; //correct ?
      }
      plugin_name << "Rx";
    }
    else {
      plugin_name << "Tx";
    }
    plugin_name << static_cast<char>('A'+connection.plugin() ) << ":" << connection.channel();
    setColumnText(new_item, kPlugin, plugin_name.str().c_str() );

    m_listView->resizeColumnToContents(m_column[kModule]);

    std::cout << "INFO [AddLinkItems::operator()] EXIT elements = " << m_elements << std::endl;
    m_lastItem =new_item;
  }

  QTreeWidgetItem *createItem() {
    return createItem(m_parent);
  }

protected:

  void setColumnText(QTreeWidgetItem *item, EColumn col_id, const char *text) {
    assert( col_id < kNColumns);
    if ( m_listView->columnCount()>0 && m_column[col_id] < static_cast<unsigned int>(m_listView->columnCount()) ) {
      item->setText(m_column[col_id],text);
    }
  }

  void setColumnTitle(EColumn col_id, const char  *text) {
    assert( col_id < kNColumns);
    if ( m_listView->columnCount()>0 && m_column[col_id] < static_cast<unsigned int>(m_listView->columnCount()) ) {
      m_listView->headerItem()->setText(m_column[col_id],text);
      if (col_id != kModule /*&& col_id != kPlugin*/ && col_id != m_firstColumn) {
	m_listView->setColumnWidth( m_column[col_id], m_listView->fontMetrics().width(m_listView->headerItem()->text(m_column[col_id]) )+10 );
      }
      else {
	m_listView->resizeColumnToContents(m_column[col_id]);
      }
    }
  }

public:

  QTreeWidgetItem *createItem(QTreeWidgetItem *parent) {
    QTreeWidgetItem *new_item;
    std::cout << "INFO [AddLinkItems::createItem] counter = " << m_elements << std::endl;
    if (parent) {
      if (m_lastItem) {
	new_item = new LinkItem(parent, m_lastItem,m_elements);
      }
      else {
	new_item = new LinkItem(parent, m_elements);
      }
    }
    else {
      if (m_lastItem) {
	new_item = new LinkItem(m_listView, m_lastItem,m_elements);
      }
      else {
	new_item = new LinkItem(m_listView, m_elements);
      }
    }
    m_elements++;
    return new_item;
  }

protected:
  void setupColumnMap(EColumn first_column) {
    unsigned int mode_i=0;
    if (first_column==kFormater)  {
      m_nColumns=kNColumns;
      m_firstColumn=kFormater;
      mode_i=1;
    }
    else if (first_column==kModule)  {
      m_nColumns=kNColumns-3;
      m_firstColumn=kModule;
      mode_i=0;
    }
    else {
      m_nColumns=kNColumns;
      m_firstColumn=kPlugin;
      mode_i=2;
    }

    unsigned int col_i=0;

    unsigned short perm[3][3]={
      {0,1,2},
      {1,2,0},
      {2,1,0}
    };

    m_column.resize(kNColumns);
    for (col_i=0; col_i< m_column.size(); col_i++) {
      m_column[col_i]=kNColumns;
    }
    col_i=0;
    for (unsigned int pass_i=0; pass_i<3; pass_i++) {
      switch (perm[mode_i][pass_i]) {
      case 0:
	if (first_column != kModule) {
	  m_column[kModule]=col_i++;
	  m_column[kProdName]=col_i++;
	  m_column[kModuleId]=col_i++;
	}
	break;
      case 1:
	m_column[kFormater]=col_i++;
	m_column[kDsp]=col_i++;
	break;
      case 2:
	//	if (first_column != kPlugin) {
	m_column[kPlugin]=col_i++;
	  //	}
	//	m_column[kPluginChannel]=col_i++;
	break;
      }
    }

//     while (static_cast<unsigned int>(m_listView->columnCount())<m_nColumns)  {
//       m_listView->addColumn("");
//     }
//     while (static_cast<unsigned int>(m_listView->columnCount())>m_nColumns)  {
//       m_listView->removeColumn(m_listView->columnCount()-1);
//     }
    m_listView->setColumnCount(m_nColumns);
  }

private:
  QTreeWidget      *m_listView;
  QTreeWidgetItem  *m_parent;
  QTreeWidgetItem  *m_lastItem;
  EColumn         m_firstColumn;
  unsigned int    m_nColumns;
 // unsigned int    m_padding;
  unsigned int    m_elements;
  std::vector<unsigned int> m_column;
};

class AddLinkItemsContainer : std::unary_function< LinkOrder::LinkList_t, void>
{
public:

  AddLinkItemsContainer( QTreeWidget *list_view, QTreeWidgetItem *parent, AddLinkItems::EColumn first_column)
    : m_obj(new AddLinkItems(list_view, parent,first_column))
  {}

  void setParent(QTreeWidgetItem *parent) { m_obj->setParent(parent); };

  void operator()( const LinkOrder::LinkElement_t &link_def)  {
    (*m_obj)(link_def);
  }

  QTreeWidgetItem *createItem() {
    return m_obj->createItem();
  }

protected:

public:

  QTreeWidgetItem *createItem(QTreeWidgetItem *parent) {
    return m_obj->createItem(parent);
  }

private:
  std::shared_ptr<AddLinkItems> m_obj;
};



class AddLinkOrderedItems : std::unary_function< LinkOrder::LinkMap_t, void>
{
public:
  AddLinkOrderedItems( QTreeWidget *list_view, QTreeWidgetItem *parent, AddLinkItems::EColumn first_column)
    : m_addItems(list_view, parent, first_column)
  {}

  AddLinkOrderedItems( QTreeWidget *list_view)
    : m_addItems(list_view, NULL, AddLinkItems::kFormater)
  {}

  void operator()( const std::pair< int, LinkOrder::LinkList_t  > &link_def)  {
    for_each( link_def.second.begin(), link_def.second.end(), m_addItems);
  }

  QTreeWidgetItem *createItem(QTreeWidgetItem *parent) {
    return m_addItems.createItem(parent);
  }

  void setParent(QTreeWidgetItem *parent) {
    return m_addItems.setParent(parent);
  }

private:
  AddLinkItemsContainer    m_addItems;
};

class AddDspOrderedItems : std::unary_function< DspOrder::DspMap_t, void>
{
public:
  AddDspOrderedItems( QTreeWidget *list_view)
    : m_addItems(list_view, NULL, AddLinkItems::kFormater)
  {}

  void operator()( const std::pair< unsigned int, LinkOrder::LinkList_t  > &link_def)  {

    QTreeWidgetItem *parent = createItem(NULL);
    std::stringstream dsp_name;
    dsp_name << "DSP " << link_def.first;
    parent->setText(0, dsp_name.str().c_str());
    parent->setExpanded(true);
    m_addItems.setParent(parent);

    for_each( link_def.second.begin(), link_def.second.end(), m_addItems);
  }

  QTreeWidgetItem *createItem(QTreeWidgetItem *parent) {
    return m_addItems.createItem(parent);
  }

  void setParent(QTreeWidgetItem *parent) {
    return m_addItems.setParent(parent);
  }

private:
  AddLinkItemsContainer    m_addItems;
};

class AddModuleOrderedItems : std::unary_function< ModuleOrder::ModuleMap_t, void>
{
public:
  AddModuleOrderedItems( QTreeWidget *list_view)
    : m_addItems(list_view, NULL, AddLinkItems::kModule)
  {}

  void operator()( const std::pair< unsigned int, LinkOrder::LinkList_t  > &link_def)  {
    if (!link_def.second.empty()) {

      QTreeWidgetItem *parent = createItem(NULL);
      std::stringstream module_name;
      module_name << link_def.first << " : "
		  << PixA::connectivityName( link_def.second.begin()->second)
		  << " (" << PixA::productionName( link_def.second.begin()->second)
		  << ")";
      parent->setText(0, module_name.str().c_str());
      parent->setExpanded(true);
      m_addItems.setParent(parent);

      for_each( link_def.second.begin(), link_def.second.end(), m_addItems);
    }
  }

  QTreeWidgetItem *createItem(QTreeWidgetItem *parent) {
    return m_addItems.createItem(parent);
  }

  void setParent(QTreeWidgetItem *parent) {
    return m_addItems.setParent(parent);
  }

private:
  AddLinkItemsContainer    m_addItems;
};


class PluginOrder  : std::unary_function< PixA::ModuleList::const_iterator, void>
{
public:
  //  enum ELinkType {k1A, k1B, k2A, k2B, kTX};

  //  typedef std::vector <std::pair<std::pair<int,LinkOrder::ELinkType>, const PixLib::ModuleConnectivity *> > LinkList_t;
  typedef std::map<unsigned int, LinkOrder::LinkList_t >    ChannelList_t;
  typedef std::map<unsigned int, ChannelList_t > PluginList_t;

  void operator()( const PixA::ModuleList::const_iterator &module_iter)  {
    addLink(const_cast<PixLib::ModuleConnectivity *>(*module_iter)->outLink1A(), LinkOrder::k1A, module_iter);
    addLink(const_cast<PixLib::ModuleConnectivity *>(*module_iter)->outLink1B(), LinkOrder::k1B, module_iter);
    addLink(const_cast<PixLib::ModuleConnectivity *>(*module_iter)->outLink2A(), LinkOrder::k2A, module_iter);
    addLink(const_cast<PixLib::ModuleConnectivity *>(*module_iter)->outLink2B(), LinkOrder::k2B, module_iter);
    addLink(0/*ignored*/, LinkOrder::kTx, module_iter);
  }

  PluginList_t::const_iterator begin() const {return m_list.begin(); }
  PluginList_t::const_iterator end()   const {return m_list.end(); }

  static bool isTx(unsigned int plugin_id) {
    return plugin_id<4;
  }

  static unsigned int plugin(unsigned int plugin_id) {
    return plugin_id%4;
  }

private:
  void addLink(int link, LinkOrder::ELinkType type, const PixA::ModuleList::const_iterator &module_iter) {
    if (type == LinkOrder::kTx) {
      PixA::ModulePluginConnection tx_connection = PixA::txConnection(module_iter);
      std::cout << "INFO [LinkOrder::addLink] " << link << " " << static_cast<unsigned int>(type) << " " << PixA::connectivityName(module_iter) << std::endl;
      if (tx_connection.isValid()) {
	m_list[tx_connection.plugin()][tx_connection.channel()].push_back(std::make_pair(std::make_pair(link,type), *module_iter));
      }
    }
    else {
      if (link>=0) {
	PixA::ModulePluginConnection rx_connection = PixA::rxConnection(module_iter, (type == LinkOrder::k1A || type==LinkOrder::k1B ? PixA::DTO2 : PixA::DTO1));
	std::cout << "INFO [LinkOrder::addLink] " << link << " " << static_cast<unsigned int>(type) << " " << PixA::connectivityName(module_iter) << std::endl;
	if (rx_connection.isValid()) {
	  m_list[rx_connection.plugin()+4][rx_connection.channel()].push_back(std::make_pair(std::make_pair(link,type), *module_iter));
	}
      }
    }
  }
  PluginList_t m_list;
};




class AddPluginOrderedItems : std::unary_function< PluginOrder::PluginList_t, void>
{
protected:

  class AddPluginChannelItems : std::unary_function< PluginOrder::ChannelList_t, void>
  {
  public:
    AddPluginChannelItems( QTreeWidget *list_view)
      : m_addItems(list_view,NULL, AddLinkItems::kPlugin)
    {}

    void operator()( const std::pair< unsigned int, LinkOrder::LinkList_t > &link_def)  {
      for_each( link_def.second.begin(), link_def.second.end(), m_addItems);
    }

    QTreeWidgetItem *createItem(QTreeWidgetItem *parent) {
      return m_addItems.createItem(parent);
    }
    void setParent(QTreeWidgetItem *parent) { m_addItems.setParent(parent); }
  private:

    AddLinkItemsContainer    m_addItems;
  };

public:

  AddPluginOrderedItems( QTreeWidget *list_view)
    : m_addItems(list_view)
  {}

  void operator()( const std::pair< unsigned int, PluginOrder::ChannelList_t > &link_def)  {
    QTreeWidgetItem *parent = createItem(NULL);
    QString plugin_name( (PluginOrder::isTx(link_def.first) ? "Tx" : "Rx"));
    plugin_name += ('A'+ PluginOrder::plugin(link_def.first));
    parent->setText(0, plugin_name);
    parent->setExpanded(true);
    m_addItems.setParent(parent);

    std::for_each( link_def.second.begin(), link_def.second.end(), m_addItems);
  }

private:
    QTreeWidgetItem *createItem(QTreeWidgetItem *parent) {
      return m_addItems.createItem(parent);
    }

  AddPluginChannelItems m_addItems;
};

void LinkPanel::orderByModule(bool checked)
{
  if (!checked) return;
  if (!m_conn) return;
  m_linkView->clear();
  try {
    PixA::Pp0List pp0_list( m_conn.pp0s(m_rodName));
    ModuleOrder module_order;
    PixA::for_each_module(m_disabledList, pp0_list.begin(),pp0_list.end(), module_order);

    AddModuleOrderedItems add_module_items(m_linkView);
    std::for_each(module_order.begin(), module_order.end(), add_module_items);

  }
  catch(PixA::BaseException &err) {
    std::cerr << "ERROR [LinkPanel::orderByModule] Caught exception : " << err.getDescriptor() << std::endl;
  }
  catch(std::exception &err) {
    std::cerr << "ERROR [LinkPanel::orderByModule] Caught std exception : " << err.what() << std::endl;
  }
  catch(...) {
    std::cerr << "ERROR [LinkPanel::orderByModule] Caught unknown exception." << std::endl;
  }
}

void LinkPanel::orderByDsp(bool checked)
{
  if (!checked) return;
  if (!m_conn) return;
  m_linkView->clear();
  try {
    PixA::Pp0List pp0_list( m_conn.pp0s(m_rodName));
    DspOrder dsp_order;
    PixA::for_each_module(m_disabledList, pp0_list.begin(),pp0_list.end(), dsp_order);

    AddDspOrderedItems add_dsp_items(m_linkView);
    std::for_each(dsp_order.begin(), dsp_order.end(), add_dsp_items);

  }
  catch(PixA::BaseException &err) {
    std::cerr << "ERROR [LinkPanel::orderByModule] Caught exception : " << err.getDescriptor() << std::endl;
  }
  catch(std::exception &err) {
    std::cerr << "ERROR [LinkPanel::orderByModule] Caught std exception : " << err.what() << std::endl;
  }
  catch(...) {
    std::cerr << "ERROR [LinkPanel::orderByModule] Caught unknown exception." << std::endl;
  }
}

void LinkPanel::orderByLink(bool checked) {
  if (!checked) return;
  if (!m_conn) return;
  m_linkView->clear();
  try {
    PixA::Pp0List pp0_list( m_conn.pp0s(m_rodName));
    LinkOrder link_order;
    PixA::for_each_module(m_disabledList, pp0_list.begin(),pp0_list.end(), link_order);

    AddLinkOrderedItems add_link_items(m_linkView);
    std::for_each(link_order.begin(), link_order.end(), add_link_items);

  }
  catch(PixA::BaseException &err) {
    std::cerr << "ERROR [LinkPanel::orderByLink] Caught exception : " << err.getDescriptor() << std::endl;
  }
  catch(std::exception &err) {
    std::cerr << "ERROR [LinkPanel::orderByLink] Caught std exception : " << err.what() << std::endl;
  }
  catch(...) {
    std::cerr << "ERROR [LinkPanel::orderByLink] Caught unknown exception." << std::endl;
  }

}

void LinkPanel::orderByPlugin(bool checked) {
  if (!checked) return;
  if (!m_conn) return;
  m_linkView->clear();
  try {
    PixA::Pp0List pp0_list( m_conn.pp0s(m_rodName));
    PluginOrder plugin_order;
    PixA::for_each_module(m_disabledList, pp0_list.begin(),pp0_list.end(), plugin_order);

    AddPluginOrderedItems add_plugin_ordered_items(m_linkView);
    std::for_each(plugin_order.begin(), plugin_order.end(), add_plugin_ordered_items);

  }
  catch(PixA::BaseException &err) {
    std::cerr << "ERROR [LinkPanel::orderByPlugin] Caught exception : " << err.getDescriptor() << std::endl;
  }
  catch(std::exception &err) {
    std::cerr << "ERROR [LinkPanel::orderByPlugin] Caught std exception : " << err.what() << std::endl;
  }
  catch(...) {
    std::cerr << "ERROR [LinkPanel::orderByPlugin] Caught unknown exception." << std::endl;
  }

  
}

