
#include "SummaryHistoFormInlinkOutlink.h"
#include "SummaryHistoVisualiser.h"
#include <Histo/Histo.h>

//#include <list>
#include <sstream>
#include <iomanip>
#include <q3table.h>
#include <qlabel.h>
//Added by qt3to4:
#include <QCloseEvent>

#ifdef _PIXA_HISTO_IS_ROOT_TH1
#  include <TH1.h>
#endif
#include <DataContainer/GenericHistogramAccess.h>

SummaryHistoFormInlinkOutlink::SummaryHistoFormInlinkOutlink(SummaryHistoVisualiser *visualiser, const PixA::Histo *histo, QWidget* parent, const char* name, bool modal, Qt::WFlags fl  )
  //: SummaryHistoFormBase(parent,name,modal,fl) , m_visualiser(visualiser)
:QDialog(parent),m_visualiser(visualiser)

{
setupUi(this);
  if (!histo || !PixA::is2D(histo) || PixA::nColumns(histo)==0 || PixA::nRows(histo)<33) {
    m_moduleVarTable->hide();
    m_pp0VarTable->hide();
    m_globalVarTable->hide();
    return;
  }

  //look for noisy channels
  std::list<unsigned int> noisyOutlinks;
  for(unsigned int histo_colum=0; histo_colum<32 ; histo_colum++) {
    unsigned int counts(0);
    for(unsigned int histo_row=0; histo_row < 46; histo_row++) {
      double res = PixA::binContent(histo, PixA::startColumn(histo)+histo_row, PixA::startRow(histo)+histo_colum)
	          /PixA::binContent(histo, PixA::startColumn(histo)+histo_row, PixA::startRow(histo)+32);
      if (res>0.3) counts++;      
    }
    if(counts>=3) noisyOutlinks.push_back(histo_colum);
  }

  ///////////////
  //m_moduleVarTable is used for inlinkOutlink map
  //
  m_title->setText(name);

  m_moduleVarTable->setNumCols(2);
  m_moduleVarTable->horizontalHeader()->setLabel(0,"Inlink");
  m_moduleVarTable->horizontalHeader()->setLabel(1,"Outlink");

  m_moduleVarTable->setNumRows(0);
  for(unsigned int histo_row=0; histo_row < 46; histo_row++) {
    std::stringstream temp;
    int new_row(-1);
    for(unsigned int histo_colum=0; histo_colum<32 ; histo_colum++) {
      if(std::find(noisyOutlinks.begin(),noisyOutlinks.end(),histo_colum)!=noisyOutlinks.end()) continue; //if this outlink is noisy discard it

      double res =  PixA::binContent(histo, PixA::startColumn(histo)+histo_row, PixA::startRow(histo)+histo_colum)
	          / PixA::binContent(histo,PixA::startColumn(histo)+histo_row,PixA::startRow(histo)+32);
      if (res>0.3) {
	if(new_row==-1) { //if first outlink found for current inlink
	  new_row = m_moduleVarTable->numRows();	    
	  m_moduleVarTable->insertRows(new_row,1);
	  m_moduleVarTable->verticalHeader()->setLabel(new_row, "");
	  std::stringstream inlink_str;
	  inlink_str << histo_row;
	 m_moduleVarTable->setText( new_row, 0, QString::fromStdString(inlink_str.str() )); //print the Inlink
	}
	temp << formatOutlink(histo_colum) << " ";
      }
    }
    if(new_row!=-1) m_moduleVarTable->setText( new_row, 1, QString::fromStdString(temp.str() ));
  }	  
  ////////////////////
  //m_pp0VarTable is not used 
  delete m_pp0VarTable;
  delete m_globalVarTable;
}


void SummaryHistoFormInlinkOutlink::closeEvent ( QCloseEvent * ) {
  if (m_visualiser) {
    m_visualiser->closeDialog();
  }
}

const std::string SummaryHistoFormInlinkOutlink::formatOutlink(const unsigned int histo_colum) {
  std::stringstream temp;
  int formater(histo_colum/4);
  int line(histo_colum%4);
  temp << formater << ":" << line;  
  //std::setw(6) << ::std::fixed << std::setprecision(3) << value << ::std::setprecision( 6 );
  return temp.str();
}
