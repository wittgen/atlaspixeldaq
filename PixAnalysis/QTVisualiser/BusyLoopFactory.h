#ifndef _PixA_BusyLoopFactory_h_
#define _PixA_BusyLoopFactory_h_

#include <string>
#include <climits>

class QApplication;
class QDialog;

namespace PixA {
  
  class IBusyLoop
  {
  public:
    virtual ~IBusyLoop() {}

    /** Change the text displayed in the dialog if there is one.
     */
    virtual void changeMessage(const std::string &message) = 0;

  };

  class IBusyLoopFactory {
  public:
    virtual ~IBusyLoopFactory() {};
    virtual IBusyLoop *busyLoop(const std::string &message,
				QDialog* dialog = 0,
				unsigned int dialog_after_time = 2500,
				unsigned int update_time = 100) =0;
  };

  class BusyLoopFactory
  {
  public:
    static IBusyLoop *busyLoop(const std::string &message,
			       QDialog* dialog,
			       unsigned int dialog_after_time = 2500,
			       unsigned int update_time = 100) {
      return instance()->busyLoop(message, dialog, dialog_after_time, update_time);
    }

    static IBusyLoop *busyLoop(const std::string &message, unsigned int update_time = 100) {
      return instance()->busyLoop(message, static_cast<QDialog *>(NULL), INT_MAX, update_time);
    }

    static void setInstance(IBusyLoopFactory *new_factory) {
      delete s_instance;
      s_instance = new_factory;
    }
  private:
    static IBusyLoopFactory *createInstance();

    static IBusyLoopFactory *instance() {
      if (!s_instance) {
	setInstance( createInstance() );
      }
      return s_instance;
    }

    static IBusyLoopFactory *s_instance;
  };
}

#endif
