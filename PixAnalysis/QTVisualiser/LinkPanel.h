// Dear emacs, this is -*-c++-*-
#ifndef _LinkPanel_h_
#define _LinkPanel_h_

#include <ui_LinkPanelBase.h>
#include <ConfigWrapper/Connectivity.h>
#include <ConfigWrapper/PixDisableUtil.h>

class LinkPanel : public QDialog, public Ui_LinkPanelBase
{
 Q_OBJECT
public:
  LinkPanel(const PixA::ConnectivityRef &conn,
	    const PixA::DisabledListRoot &root_disable_list,
	    const std::string &rod_name,
	    QWidget* parent = 0);

  ~LinkPanel();

public slots:
  void orderByLink(bool);
  void orderByPlugin(bool);
  void orderByModule(bool);
  void orderByDsp(bool);

private:
  const std::string      m_rodName;
  PixA::ConnectivityRef  m_conn;
  PixA::DisabledListRoot m_disabledList;
};

#endif
