#ifndef CONFIGVIEWER_H

#include <QDialog>
#include <QPushButton>
#include <QCloseEvent>
#include <ui_ConfigViewerBase.h>

#include <ConfigWrapper/ConfigHandle.h>

// special table view for any PixLib config
class QTableWidget;
class QTreeWidgetItem;

namespace PixLib{
 class Config;
 class ConfGroup;
 class ConfMatrix;
}

class QTablePushButton : public QPushButton {

  Q_OBJECT
 public:
  QTablePushButton(PixLib::ConfMatrix &confObj, QWidget * parent, const char * name = 0 );
  ~QTablePushButton();

 public slots:
  void showContent();

 private:
  PixLib::ConfMatrix &m_obj;
};

class matrixViewer : public QDialog {

  Q_OBJECT
 public:
  matrixViewer(PixLib::ConfMatrix &in_cfg, QWidget* parent = 0);
  ~matrixViewer();

 public slots:

 private:
  PixLib::ConfMatrix &m_config;

};

class configViewer : public QDialog, public Ui_ConfigViewerBase {

  Q_OBJECT
 public:
  configViewer( const std::string &title, const PixLib::Config &in_cfg, QWidget* parent = 0);
  configViewer( const std::string &title, const PixLib::Config &in_cfg, PixA::ConfigHandle config_handle, QWidget* parent = 0);

  ~configViewer();

 public slots:
  void fillTab(QWidget *tab, const PixLib::ConfGroup &cgrp);
  void fillConfGroupView(QTreeWidgetItem*,int);
 protected:
  void closeEvent(QCloseEvent * event );
  void updateList();
  QTreeWidgetItem *loadSubConf(QTreeWidgetItem *parent, const PixLib::Config &sub_config);

 private:
  const PixLib::Config &m_config;
  PixA::ConfigHandle m_configHandle;
  QTableWidget     *m_confObjTable;

};

#endif // CONFIGVIEWER_H 
