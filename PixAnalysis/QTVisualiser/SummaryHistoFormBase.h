/********************************************************************************
** Form generated from reading UI file 'SummaryHistoFormBase.ui'
**
** Created by: Qt User Interface Compiler version 4.8.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SUMMARYHISTOFORMBASE_H
#define UI_SUMMARYHISTOFORMBASE_H

#include <Qt3Support/Q3Header>
#include <Qt3Support/Q3MimeSourceFactory>
#include <Qt3Support/Q3Table>
#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QSpacerItem>
#include <QtGui/QSplitter>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SummaryHistoFormBase
{
public:
    QVBoxLayout *vboxLayout;
    QLabel *m_title;
    QSplitter *m_modPp0Splitter;
    QWidget *layout4;
    QVBoxLayout *vboxLayout1;
    QHBoxLayout *hboxLayout;
    QLabel *textLabel1_2;
    QSpacerItem *spacer2;
    Q3Table *m_moduleVarTable;
    QWidget *layout11;
    QVBoxLayout *vboxLayout2;
    QHBoxLayout *hboxLayout1;
    QLabel *textLabel1;
    QSpacerItem *spacer1;
    Q3Table *m_pp0VarTable;
    QHBoxLayout *hboxLayout2;
    QLabel *textLabel1_3;
    QSpacerItem *spacer1_2;
    Q3Table *m_globalVarTable;

    void setupUi(QDialog *SummaryHistoFormBase)
    {
        if (SummaryHistoFormBase->objectName().isEmpty())
            SummaryHistoFormBase->setObjectName(QString::fromUtf8("SummaryHistoFormBase"));
        SummaryHistoFormBase->resize(600, 480);
        vboxLayout = new QVBoxLayout(SummaryHistoFormBase);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        m_title = new QLabel(SummaryHistoFormBase);
        m_title->setObjectName(QString::fromUtf8("m_title"));
        QFont font;
        font.setPointSize(14);
        font.setBold(true);
        m_title->setFont(font);
        m_title->setMargin(0);
        m_title->setAlignment(Qt::AlignVCenter|Qt::AlignLeft);
        m_title->setIndent(16);
        m_title->setWordWrap(false);

        vboxLayout->addWidget(m_title);

        m_modPp0Splitter = new QSplitter(SummaryHistoFormBase);
        m_modPp0Splitter->setObjectName(QString::fromUtf8("m_modPp0Splitter"));
        m_modPp0Splitter->setOrientation(Qt::Vertical);
        layout4 = new QWidget(m_modPp0Splitter);
        layout4->setObjectName(QString::fromUtf8("layout4"));
        vboxLayout1 = new QVBoxLayout(layout4);
        vboxLayout1->setSpacing(6);
        vboxLayout1->setContentsMargins(11, 11, 11, 11);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        vboxLayout1->setContentsMargins(0, 0, 0, 0);
        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        textLabel1_2 = new QLabel(layout4);
        textLabel1_2->setObjectName(QString::fromUtf8("textLabel1_2"));
        textLabel1_2->setWordWrap(false);

        hboxLayout->addWidget(textLabel1_2);

        spacer2 = new QSpacerItem(291, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacer2);


        vboxLayout1->addLayout(hboxLayout);

        m_moduleVarTable = new Q3Table(layout4);
        m_moduleVarTable->setObjectName(QString::fromUtf8("m_moduleVarTable"));
        m_moduleVarTable->setNumRows(0);
        m_moduleVarTable->setNumCols(0);

        vboxLayout1->addWidget(m_moduleVarTable);

        m_modPp0Splitter->addWidget(layout4);
        layout11 = new QWidget(m_modPp0Splitter);
        layout11->setObjectName(QString::fromUtf8("layout11"));
        vboxLayout2 = new QVBoxLayout(layout11);
        vboxLayout2->setSpacing(6);
        vboxLayout2->setContentsMargins(11, 11, 11, 11);
        vboxLayout2->setObjectName(QString::fromUtf8("vboxLayout2"));
        vboxLayout2->setContentsMargins(0, 0, 0, 0);
        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setSpacing(6);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        textLabel1 = new QLabel(layout11);
        textLabel1->setObjectName(QString::fromUtf8("textLabel1"));
        textLabel1->setWordWrap(false);

        hboxLayout1->addWidget(textLabel1);

        spacer1 = new QSpacerItem(473, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout1->addItem(spacer1);


        vboxLayout2->addLayout(hboxLayout1);

        m_pp0VarTable = new Q3Table(layout11);
        m_pp0VarTable->setObjectName(QString::fromUtf8("m_pp0VarTable"));
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(7), static_cast<QSizePolicy::Policy>(7));
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(m_pp0VarTable->sizePolicy().hasHeightForWidth());
        m_pp0VarTable->setSizePolicy(sizePolicy);
        m_pp0VarTable->setNumRows(0);
        m_pp0VarTable->setNumCols(0);

        vboxLayout2->addWidget(m_pp0VarTable);

        hboxLayout2 = new QHBoxLayout();
        hboxLayout2->setSpacing(6);
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        textLabel1_3 = new QLabel(layout11);
        textLabel1_3->setObjectName(QString::fromUtf8("textLabel1_3"));
        QSizePolicy sizePolicy1(static_cast<QSizePolicy::Policy>(5), static_cast<QSizePolicy::Policy>(1));
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(textLabel1_3->sizePolicy().hasHeightForWidth());
        textLabel1_3->setSizePolicy(sizePolicy1);
        textLabel1_3->setWordWrap(false);

        hboxLayout2->addWidget(textLabel1_3);

        spacer1_2 = new QSpacerItem(461, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout2->addItem(spacer1_2);


        vboxLayout2->addLayout(hboxLayout2);

        m_globalVarTable = new Q3Table(layout11);
        m_globalVarTable->setObjectName(QString::fromUtf8("m_globalVarTable"));
        QSizePolicy sizePolicy2(static_cast<QSizePolicy::Policy>(7), static_cast<QSizePolicy::Policy>(5));
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(m_globalVarTable->sizePolicy().hasHeightForWidth());
        m_globalVarTable->setSizePolicy(sizePolicy2);
        m_globalVarTable->setNumRows(0);
        m_globalVarTable->setNumCols(0);

        vboxLayout2->addWidget(m_globalVarTable);

        m_modPp0Splitter->addWidget(layout11);

        vboxLayout->addWidget(m_modPp0Splitter);


        retranslateUi(SummaryHistoFormBase);

        QMetaObject::connectSlotsByName(SummaryHistoFormBase);
    } // setupUi

    void retranslateUi(QDialog *SummaryHistoFormBase)
    {
        SummaryHistoFormBase->setWindowTitle(QApplication::translate("SummaryHistoFormBase", "Form1", 0, QApplication::UnicodeUTF8));
        m_title->setText(QApplication::translate("SummaryHistoFormBase", "Title", 0, QApplication::UnicodeUTF8));
        textLabel1_2->setText(QApplication::translate("SummaryHistoFormBase", "Module Variables :", 0, QApplication::UnicodeUTF8));
        textLabel1->setText(QApplication::translate("SummaryHistoFormBase", "PP0 Variables :", 0, QApplication::UnicodeUTF8));
        textLabel1_3->setText(QApplication::translate("SummaryHistoFormBase", "Global Variables :", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class SummaryHistoFormBase: public Ui_SummaryHistoFormBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SUMMARYHISTOFORMBASE_H
