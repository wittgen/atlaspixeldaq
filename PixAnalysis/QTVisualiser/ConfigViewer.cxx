#include "ConfigViewer.h"
#include <Histo.h>
#include <Config/ConfGroup.h>
#include <QLayout>
#include <QTableWidget>
#include <QComboBox>
#include <QTreeWidget>

using namespace PixLib;

configViewer::configViewer(const std::string &title, const PixLib::Config &in_cfg, QWidget* parent)
  : QDialog(parent),
    m_config(in_cfg),
    m_confObjTable(NULL)
{
  setupUi(this );
  setWindowTitle(title.c_str());
  m_titleLabel->setText(title.c_str() );

  updateList();

  resize( QSize(700, 400).expandedTo(minimumSizeHint()) );
  connect(m_confGroupList, SIGNAL(itemClicked(QTreeWidgetItem *, int)), this, SLOT(fillConfGroupView(QTreeWidgetItem*,int)));

}

configViewer::configViewer(const std::string &title, const PixLib::Config &in_cfg, PixA::ConfigHandle config_handle, QWidget* parent)
  : QDialog(parent),
    m_config(in_cfg),
    m_configHandle(config_handle),
    m_confObjTable(NULL)
{
  setupUi(this );
  setWindowTitle(title.c_str());
  m_titleLabel->setText(title.c_str() );
  m_confGroupLabel->setText(const_cast<PixLib::Config &>(in_cfg).name().c_str());
  m_confGroupList->setSortingEnabled(true);
  updateList();

  resize( QSize(700, 400).expandedTo(minimumSizeHint()) );
  connect(m_confGroupList, SIGNAL(itemClicked(QTreeWidgetItem *, int)), this, SLOT(fillConfGroupView(QTreeWidgetItem*,int)));
}

configViewer::~configViewer(){
}

void configViewer::closeEvent(QCloseEvent * event )
{
  QDialog::closeEvent(event);
  delete this;
}

void configViewer::updateList()
{
  QTreeWidgetItem *config_item=new QTreeWidgetItem(m_confGroupList);
  config_item->setText(0, const_cast<PixLib::Config &>(m_config).name().c_str());

  QTreeWidgetItem *first_item = loadSubConf(config_item, m_config);
  fillConfGroupView(first_item,0);

  m_confGroupList->resize(m_confGroupList->sizeHint());
  /*
  {
    QSize a_size(m_confGroupList->sizeHint());
    {
      QFontMetrics fm(m_confGroupList->font());
      int text_width(fm.width("M"));
      a_size.setWidth(a_size.width()+m_confGroupList->margin()*2+m_confGroupList->itemMargin()+text_width*2);
    }
    if (a_size.width() < m_confGroupList->contentsWidth()+ m_confGroupList->margin()*2) {
      a_size.setWidth( m_confGroupList->contentsWidth() + m_confGroupList->margin()*2);
    }
    m_confGroupList->resize( a_size );
    adjustSize();
    }*/
  m_confGroupList->sortItems(0, Qt::AscendingOrder);
}

class ConfGroupItem  : public QTreeWidgetItem
{
public:
  ConfGroupItem(QTreeWidgetItem *list_view_item, const std::string &name, const PixLib::ConfGroup &group)
    : QTreeWidgetItem(list_view_item),
      m_group(&group)
  {setText(0, name.c_str());}

  ConfGroupItem(QTreeWidget *list_view, const std::string &name, const PixLib::ConfGroup &group)
    : QTreeWidgetItem(list_view),
      m_group(&group)
  {setText(0, name.c_str());}

  const PixLib::ConfGroup &group() const { return *m_group;}

private:
  const PixLib::ConfGroup *m_group;
};

void addName( std::map< std::pair<std::string,int>, unsigned int> &order, std::string a_name, unsigned int index)
{
  std::string::size_type pos = a_name.find_first_of("0123456789");
  int number=0;
  if (pos != std::string::npos) {
    char *start = &(a_name[pos]);
    char *end=NULL;
      number=strtol(start,&end,10);
      if (end) {
	a_name.erase(pos,static_cast<std::string::size_type>(end-start));
	a_name.insert(pos,"0");
      }
      else {
	number=0;
      }
  }
  order.insert(std::make_pair(std::make_pair(a_name,number),index));
}

QTreeWidgetItem *configViewer::loadSubConf(QTreeWidgetItem *parent, const PixLib::Config &a_config)
{
  QTreeWidgetItem *return_item=nullptr;
  parent->setExpanded(true);
  std::map< std::pair<std::string,int>, unsigned int> order;
  for(unsigned int j=0;j<a_config.subConfigSize();j++){
    addName(order, const_cast<PixLib::Config &>( a_config.subConfig(j) ).name(),j);
  }
  for(std::map< std::pair<std::string,int>, unsigned int>::reverse_iterator order_iter = order.rbegin();
      order_iter != order.rend();
      ++order_iter) {
    unsigned int j=order_iter->second;
    const PixLib::Config &sub_config = a_config.subConfig(j);
    //    unsigned int offset = const_cast<PixLib::Config&>(a_config).size();
    QTreeWidgetItem *sub_config_item=new QTreeWidgetItem(parent);
    sub_config_item->setText(0, const_cast<PixLib::Config &>(sub_config).name().c_str());
    return_item = loadSubConf(sub_config_item, sub_config);
  }

  for (unsigned int i=0; i<static_cast<unsigned int>( const_cast<PixLib::Config&>(a_config).size() ); i++) {
    const ConfGroup &grp = const_cast<PixLib::Config &>(a_config)[i];
    QTreeWidgetItem *conf_group_item=new ConfGroupItem(parent, const_cast<PixLib::ConfGroup &>(grp).name(), grp);
    return_item = conf_group_item;
  }
  return return_item;
}

void configViewer::fillConfGroupView(QTreeWidgetItem *item, int) {
  ConfGroupItem *conf_group_item=dynamic_cast<ConfGroupItem *>(item);
  if (conf_group_item) {
    std::string new_title="";
    QTreeWidgetItem *parent = item;
    while (parent) {
      if (new_title.empty()) {
	new_title = parent->text(0).toLatin1().data();
      }
      else {
	new_title = std::string(parent->text(0).toLatin1().data()) + " / " + new_title;
      }
      parent = parent->parent();
    }
    m_confGroupLabel->setText(new_title.c_str() );
    fillTab(m_confGroupView, conf_group_item->group());
  }
}

enum EColumnTypes {kColName, kColType,kColValue, kColComment};
void configViewer::fillTab(QWidget *tab, const ConfGroup &cgrp){

  PixLib::ConfGroup& pcgrp = const_cast<PixLib::ConfGroup&>(cgrp);

  QVBoxLayout *layout1 = dynamic_cast<QVBoxLayout *>(tab->layout());
  if (!layout1) layout1 = new QVBoxLayout( tab );

  delete m_confObjTable;
  m_confObjTable=new QTableWidget(pcgrp.size(), 4, tab);
  QStringList hlist;
  hlist << "Name";
  hlist << "Type";
  hlist << "Value";
  hlist << "Comment";
  m_confObjTable->setHorizontalHeaderLabels(hlist);
  m_confObjTable->setColumnWidth(kColName,150);
  m_confObjTable->setColumnWidth(kColComment,250);
  layout1->addWidget(m_confObjTable);

  for (size_t i=0; i<pcgrp.size(); i++) {
    //@todo obj should be const 
    ConfObj &obj = pcgrp[i];
    //    QHBoxLayout *layout2 = new QHBoxLayout( 0, 0, 6, "layout"+QString::number(i)); 
    QString ocmt  = obj.comment().c_str();
    QString oname = obj.name().c_str();
    int pos=oname.indexOf("_");
    if(pos>=0)  oname.remove(0,pos+1);
    QString typeStr, valStr;
    switch( obj.type() ) {
    case ConfObj::INT : {
      typeStr = "INT"; 
      int ival=0;
      switch( obj.subtype() ){
      case ConfInt::S32:
	ival = (int)((ConfInt&)obj).valueS32();
	break;
      case ConfInt::U32:
	ival = (int)((ConfInt&)obj).valueU32();
	break;
      case ConfInt::S16:
	ival = (int)((ConfInt&)obj).valueS16();
	break;
      case ConfInt::U16:
	ival = (int)((ConfInt&)obj).valueU16();
	break;
      case ConfInt::S8:
	ival = (int)((ConfInt&)obj).valueS8();
	break;
      case ConfInt::U8:
	ival = (int)((ConfInt&)obj).valueU8();
	break;
      default: ;
      }
      valStr  = QString::number(ival);
      break;}
    case ConfObj::FLOAT : 
      typeStr = "FLOAT";
      valStr  = QString::number(((ConfFloat&)obj).value(),'f',3);
      break;
    case ConfObj::LIST : 
      typeStr = "LIST"; 
      valStr  = ((ConfList&)obj).sValue().c_str();
      break;
    case ConfObj::BOOL : 
      typeStr = "BOOL";
      valStr  = ((ConfBool&)obj).value()?"true":"false";
      break;
    case ConfObj::STRING : 
      typeStr = "STRING";
      valStr  = ((ConfString&)obj).value().c_str();
      break;
    case ConfObj::LINK:
      typeStr = "LINK";
      valStr  = ((ConfLink&)obj).value().c_str();
      break;
    case ConfObj::ALIAS:
      typeStr = "ALIAS";
      valStr  = ((ConfLink&)obj).value().c_str();
      break;
    case ConfObj::VOID : 
      typeStr = "VOID"; 
      valStr  = "void";
      break;
    default: 
      typeStr = "Unrecognized";
      valStr  = "???";
    }
    //INT, FLOAT, VECTOR, STRVECT, MATRIX, LIST, BOOL, STRING, LINK, ALIAS, VOID
    QTableWidgetItem *newTabItem;
    newTabItem = new QTableWidgetItem(oname);
    m_confObjTable->setItem(i, kColName, newTabItem);
    newTabItem = new QTableWidgetItem(ocmt);
    m_confObjTable->setItem(i, kColComment, newTabItem);
    if(obj.type() == ConfObj::MATRIX) {
      QTablePushButton *tb = new QTablePushButton( (ConfMatrix&)obj, this );
      tb->setText("Show");
      newTabItem = new QTableWidgetItem("MATRIX");
      m_confObjTable->setItem(i, kColType, newTabItem);
      m_confObjTable->setCellWidget(i, kColValue, tb);
    } else if ( obj.type() == ConfObj::VECTOR) {
      QStringList stringList;
      switch( obj.subtype() ){
      case ConfVector::V_INT:{
	std::vector<int> &tmpVec = ((ConfVector&)obj).valueVInt();
	for(std::vector<int>::iterator IT = tmpVec.begin(); IT!=tmpVec.end(); IT++)
	  stringList << QString::number(*IT);
	break;}
      case ConfVector::V_UINT:{
	std::vector<unsigned int> &tmpVec = ((ConfVector&)obj).valueVUint();
	for(std::vector<unsigned int>::iterator IT = tmpVec.begin(); IT!=tmpVec.end(); IT++)
	  stringList << QString::number(*IT);
	break;}
      case ConfVector::V_FLOAT:{
	std::vector<float> &tmpVec = ((ConfVector&)obj).valueVFloat();
	for(std::vector<float>::iterator IT = tmpVec.begin(); IT!=tmpVec.end(); IT++)
	  stringList << QString::number(*IT);
	break;}
      default:
	break;
      }
      newTabItem = new QTableWidgetItem("VECTOR");
      m_confObjTable->setItem(i, kColType, newTabItem);
      QComboBox *comboItem = new QComboBox ( m_confObjTable );
      comboItem->addItems( stringList );
      if(stringList.count()>0) comboItem->setCurrentIndex( 0 );
      m_confObjTable->setCellWidget(i, kColValue, comboItem);
    } else if ( obj.type() == ConfObj::STRVECT) {
      QStringList stringList;
      std::vector<std::string> &tmpVec = ((ConfStrVect&)obj).value();
      for(std::vector<std::string>::iterator IT = tmpVec.begin(); IT!=tmpVec.end(); IT++)
	stringList << QString(IT->c_str());
      newTabItem = new QTableWidgetItem("STRVECT");
      m_confObjTable->setItem(i, kColType, newTabItem);
      QComboBox *comboItem = new QComboBox ( m_confObjTable );
      comboItem->addItems( stringList );
      if(stringList.count()>0) comboItem->setCurrentIndex( 0 );
      m_confObjTable->setCellWidget(i, kColValue, comboItem);
    } else {
      newTabItem = new QTableWidgetItem(typeStr);
      m_confObjTable->setItem(i, kColType, newTabItem);
      newTabItem = new QTableWidgetItem(valStr);
      m_confObjTable->setItem(i, kColValue, newTabItem);
    }
  }

  m_confObjTable->resizeRowsToContents();
  m_confObjTable->resizeColumnsToContents();
  m_confObjTable->adjustSize();
  m_confObjTable->show();
}
QTablePushButton::QTablePushButton( PixLib::ConfMatrix &confObj, QWidget * parent, const char * /*name*/ )
  : QPushButton(parent), m_obj(confObj)
{
  connect(this, SIGNAL(clicked()), this, SLOT(showContent()));
}
QTablePushButton::~QTablePushButton()
{
}
void QTablePushButton::showContent()
{
  matrixViewer *mv = new matrixViewer(m_obj, this);
  mv->exec();
  delete mv;
}
matrixViewer::matrixViewer(PixLib::ConfMatrix &in_cfg, QWidget* parent)
  : QDialog(parent), m_config(in_cfg)
{
  setWindowTitle("ConfMatrix viewer");
  QVBoxLayout *BaseLayout = new QVBoxLayout( this );
  QHBoxLayout *layout1 = new QHBoxLayout( );
  layout1->setSpacing(6);
  QVBoxLayout *layout2 = new QVBoxLayout( );
  layout2->setSpacing(6);
  QTableWidget *table = new QTableWidget( this );
  layout2->addWidget(table);

  QSpacerItem *spacer1 = new QSpacerItem( 41, 21, QSizePolicy::Expanding, QSizePolicy::Minimum );
  layout1->addItem( spacer1 );
  QPushButton *closeB = new QPushButton( this );
  layout1->addWidget(closeB);
  closeB->setText("Close");
  QSpacerItem *spacer2 = new QSpacerItem( 41, 21, QSizePolicy::Expanding, QSizePolicy::Minimum );
  layout1->addItem( spacer2 );

  layout2->addLayout( layout1 );
  BaseLayout->addLayout( layout2 );

  resize( QSize(700, 400).expandedTo(minimumSizeHint()) );
  connect( closeB, SIGNAL( clicked() ), this, SLOT( close() ) );

  // file table
  int sizec, sizer=0;
  switch(m_config.subtype()){
  case ConfMatrix::M_U16:{
    std::vector< std::vector<unsigned short int> > mask = ((PixLib::ConfMask<unsigned short int> &) m_config.valueU16()).get();
    sizec = mask.size();
    if(sizec>0)
      sizer = mask[0].size();
    table->setColumnCount(sizec);
    table->setRowCount(sizer);
    for(int i=0;i<sizec;i++){
      table->setColumnWidth(i,50);
      for(int j=0;j<sizer;j++){
	QTableWidgetItem *newTabItem = new QTableWidgetItem(QString::number(mask[i][j]));
	table->setItem(j, i, newTabItem);
      }
    }
    break;}
  case ConfMatrix::M_U1:{
    std::vector< std::vector<bool> > mask = ((PixLib::ConfMask<bool> &) m_config.valueU1()).get();
    sizec = mask.size();
    if(sizec>0)
      sizer = mask[0].size();
    table->setColumnCount(sizec);
    table->setRowCount(sizer);
    for(int i=0;i<sizec;i++){
      table->setColumnWidth(i,50);
      for(int j=0;j<sizer;j++){
	QTableWidgetItem *newTabItem = new QTableWidgetItem(mask[i][j]?"ON":"OFF");
	table->setItem(j, i, newTabItem);
      }
    }
    break;}
  default:
    return;
  }
  table->resizeRowsToContents();
  table->resizeColumnsToContents();
}
matrixViewer::~matrixViewer()
{
}

