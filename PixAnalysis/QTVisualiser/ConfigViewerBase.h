/********************************************************************************
** Form generated from reading UI file 'ConfigViewerBase.ui'
**
** Created by: Qt User Interface Compiler version 4.8.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONFIGVIEWERBASE_H
#define UI_CONFIGVIEWERBASE_H

#include <Qt3Support/Q3Frame>
#include <Qt3Support/Q3Header>
#include <Qt3Support/Q3ListView>
#include <Qt3Support/Q3MimeSourceFactory>
#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QSplitter>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ConfigViewerBase
{
public:
    QVBoxLayout *vboxLayout;
    QLabel *m_titleLabel;
    QSplitter *splitter1;
    Q3ListView *m_confGroupList;
    QWidget *layout5;
    QVBoxLayout *vboxLayout1;
    QLabel *m_confGroupLabel;
    Q3Frame *m_confGroupView;
    QVBoxLayout *vboxLayout2;
    QHBoxLayout *hboxLayout;
    QSpacerItem *spacer1;
    QPushButton *m_closeButton;
    QSpacerItem *spacer2;

    void setupUi(QDialog *ConfigViewerBase)
    {
        if (ConfigViewerBase->objectName().isEmpty())
            ConfigViewerBase->setObjectName(QString::fromUtf8("ConfigViewerBase"));
        ConfigViewerBase->resize(561, 448);
        vboxLayout = new QVBoxLayout(ConfigViewerBase);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        m_titleLabel = new QLabel(ConfigViewerBase);
        m_titleLabel->setObjectName(QString::fromUtf8("m_titleLabel"));
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(3), static_cast<QSizePolicy::Policy>(1));
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(m_titleLabel->sizePolicy().hasHeightForWidth());
        m_titleLabel->setSizePolicy(sizePolicy);
        QFont font;
        font.setPointSize(14);
        m_titleLabel->setFont(font);
        m_titleLabel->setIndent(14);
        m_titleLabel->setWordWrap(false);

        vboxLayout->addWidget(m_titleLabel);

        splitter1 = new QSplitter(ConfigViewerBase);
        splitter1->setObjectName(QString::fromUtf8("splitter1"));
        QSizePolicy sizePolicy1(static_cast<QSizePolicy::Policy>(7), static_cast<QSizePolicy::Policy>(7));
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(splitter1->sizePolicy().hasHeightForWidth());
        splitter1->setSizePolicy(sizePolicy1);
        splitter1->setOrientation(Qt::Horizontal);
        m_confGroupList = new Q3ListView(splitter1);
        m_confGroupList->addColumn(QApplication::translate("ConfigViewerBase", "Name", 0, QApplication::UnicodeUTF8));
        m_confGroupList->header()->setClickEnabled(false, m_confGroupList->header()->count() - 1);
        m_confGroupList->header()->setResizeEnabled(false, m_confGroupList->header()->count() - 1);
        m_confGroupList->setObjectName(QString::fromUtf8("m_confGroupList"));
        QSizePolicy sizePolicy2(static_cast<QSizePolicy::Policy>(1), static_cast<QSizePolicy::Policy>(7));
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(m_confGroupList->sizePolicy().hasHeightForWidth());
        m_confGroupList->setSizePolicy(sizePolicy2);
        m_confGroupList->setMinimumSize(QSize(150, 0));
        m_confGroupList->setResizePolicy(Q3ScrollView::Manual);
        m_confGroupList->setHScrollBarMode(Q3ScrollView::Auto);
        m_confGroupList->setResizeMode(Q3ListView::NoColumn);
        splitter1->addWidget(m_confGroupList);
        layout5 = new QWidget(splitter1);
        layout5->setObjectName(QString::fromUtf8("layout5"));
        vboxLayout1 = new QVBoxLayout(layout5);
        vboxLayout1->setSpacing(6);
        vboxLayout1->setContentsMargins(11, 11, 11, 11);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        vboxLayout1->setContentsMargins(0, 0, 0, 0);
        m_confGroupLabel = new QLabel(layout5);
        m_confGroupLabel->setObjectName(QString::fromUtf8("m_confGroupLabel"));
        m_confGroupLabel->setWordWrap(false);

        vboxLayout1->addWidget(m_confGroupLabel);

        m_confGroupView = new Q3Frame(layout5);
        m_confGroupView->setObjectName(QString::fromUtf8("m_confGroupView"));
        sizePolicy1.setHeightForWidth(m_confGroupView->sizePolicy().hasHeightForWidth());
        m_confGroupView->setSizePolicy(sizePolicy1);
        m_confGroupView->setFrameShape(QFrame::NoFrame);
        m_confGroupView->setFrameShadow(QFrame::Plain);
        vboxLayout2 = new QVBoxLayout(m_confGroupView);
        vboxLayout2->setSpacing(6);
        vboxLayout2->setContentsMargins(0, 0, 0, 0);
        vboxLayout2->setObjectName(QString::fromUtf8("vboxLayout2"));

        vboxLayout1->addWidget(m_confGroupView);

        splitter1->addWidget(layout5);

        vboxLayout->addWidget(splitter1);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        spacer1 = new QSpacerItem(101, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacer1);

        m_closeButton = new QPushButton(ConfigViewerBase);
        m_closeButton->setObjectName(QString::fromUtf8("m_closeButton"));

        hboxLayout->addWidget(m_closeButton);

        spacer2 = new QSpacerItem(181, 21, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacer2);


        vboxLayout->addLayout(hboxLayout);


        retranslateUi(ConfigViewerBase);
        QObject::connect(m_confGroupList, SIGNAL(clicked(Q3ListViewItem*)), ConfigViewerBase, SLOT(fillConfGroupView(Q3ListViewItem*)));
        QObject::connect(m_closeButton, SIGNAL(clicked()), ConfigViewerBase, SLOT(close()));

        QMetaObject::connectSlotsByName(ConfigViewerBase);
    } // setupUi

    void retranslateUi(QDialog *ConfigViewerBase)
    {
        ConfigViewerBase->setWindowTitle(QApplication::translate("ConfigViewerBase", "Form1", 0, QApplication::UnicodeUTF8));
        m_titleLabel->setText(QApplication::translate("ConfigViewerBase", "<b>ConfigName</b>", 0, QApplication::UnicodeUTF8));
        m_confGroupList->header()->setLabel(0, QApplication::translate("ConfigViewerBase", "Name", 0, QApplication::UnicodeUTF8));
        m_confGroupLabel->setText(QApplication::translate("ConfigViewerBase", "ConfGroup", 0, QApplication::UnicodeUTF8));
        m_closeButton->setText(QApplication::translate("ConfigViewerBase", "Close", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class ConfigViewerBase: public Ui_ConfigViewerBase {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONFIGVIEWERBASE_H
