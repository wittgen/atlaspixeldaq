#ifndef _MaskableTableItem_h_

#include <q3table.h>

class MaskableTableItem : public Q3TableItem
{
public:
  MaskableTableItem( Q3Table * table, const char *text, bool enabled=true)
    : Q3TableItem ( table, Q3TableItem::Never, text ), m_enabled(enabled) { }

  void paint ( QPainter * p, const QColorGroup & cg, const QRect & cr, bool selected );
 protected:
  bool    m_enabled;
  static const QColor s_statusColor[2];
};

#endif
