#include <StatusTableItem.h>

#include <qfont.h>
#include <qpainter.h>

const char *StatusTableItem::s_statusName[kNStati] = {
  "Passed",
  "Failed",
  "Unknown"
};

const QColor StatusTableItem::s_statusColor[kNStati][2] = {
  { QColor(200,255,200), QColor(0,255,0)  },
  { QColor(255,200,200), QColor(255,0,0)  },
  { QColor(255,255,0),   QColor(255,255,0)}
};

void StatusTableItem::paint ( QPainter * p, const QColorGroup & /*cg*/, const QRect & /*cr*/, bool selected ) {
  QFont a_font(p->font());
  if (selected) {
    a_font.setBold(true);
   p->setFont(a_font);
  }
  else {
    a_font.setBold(false);
    p->setFont(a_font);
  }
  p->setBrush( s_statusColor[m_status][m_enabled] );
  p->drawRect( table()->cellRect(row(), col()) );
  p->drawText (table()->cellRect(row(), col()), Qt::AlignBottom | Qt::AlignHCenter , s_statusName[m_status]);
}
