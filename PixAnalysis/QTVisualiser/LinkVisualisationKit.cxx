#include "ConfigVisualisationKit.h"
#include "LinkVisualisationKit.h"
#include <ConfigWrapper/ScanConfig.h>
#include <Visualiser/ISimpleVisualiser.h>
#include <Visualiser/IScanDataProvider.h>
#include "ConfigViewer.h"
#include <DataContainer/HistoInfo_t.h>

#include "LinkPanel.h"

#include <Visualiser/VisualisationFactory.h>

class LinkVisualiser : public PixA::ISimpleVisualiser 
{
public:

  LinkVisualiser(QWidget *top_win)
    : m_topWin(top_win) /*m_dialog(NULL), */
  {  }

  ~LinkVisualiser() {
    //    delete m_dialog;
  }
  /** Get the name of the visualiser.
   */
  const std::string &name() const { return s_name; }

  /** Returns true, if the visualiser can optionally display only one front-end.
   */
  bool canVisualiseOneFrontEnd() const {return false;}

  /** Fill the histogram info structure.
   * The structure contains the index ranges for each dimension.
   * @sa HistoInfo_t
   */
  void fillIndexArray(PixA::IScanDataProvider&, const std::vector<std::string >&, HistoInfo_t &info) const {
    info.reset();
    info.setMinHistoIndex(0);
    info.setMaxHistoIndex(1);
  }

  /** Perform the chosen visualisation for the given front_end and the given index set.
   * @param option the visualisation option.
   * @param index the index set to specify the histogram in the multi dimensional histogram collection.
   * @param front_end the number of the front_end (0-15) which should be visualised or -1 for all front-ends
   * The front-end parameter will be ignored if canVisualiseOneFrontEnd returns false.
   */
  void visualise(PixA::IScanDataProvider &data_provider, const std::vector<std::string > &module_names, int, const PixA::Index_t & /* index */, int /* front_end */) {
    try {
    assert( module_names.size() == 1);
    QDialog *a_dialog=NULL;

//     std::string dialog_name = "LinkMap_";
//     dialog_name+=module_names.front();
    std::string a_title("LinkMap : ");
    a_title += module_names.front();
    PixA::ConnectivityRef conn (data_provider.connectivity());
    if (conn) {
      //@todo get disable from scan data provider
      PixA::DisabledListRoot empty_disabled_list;
      a_dialog=new LinkPanel(conn , empty_disabled_list, module_names.front(), m_topWin);//, dialog_name.c_str(),false);
    }
    else {
      std::cerr << "ERROR [LinkVisualiser::visualise] No connectivity. Cannot show link map." << std::endl;
    }

    if (a_dialog) {
      a_dialog->show();
    }
    }
    catch(PixA::BaseException &err) {
      std::cerr << "ERROR [LinkVisualiser::visualise] Caught base exception : " << err.getDescriptor() << std::endl;
    }
    catch( ... ) {
      std::cerr << "ERROR [LinkVisualiser::visualise] Caught unknown exception." << std::endl;
    }
  }

  void closeDialog() {
  }

private:
  QWidget              *m_topWin;
  static std::string    s_name;
};


std::string LinkVisualiser::s_name("Show Link Map");


/** Auxillary class to create the summary histo visualiser.
 */
class LinkVisualisationKit : public PixA::IVisualisationKit
{
 public:
  LinkVisualisationKit(QWidget *top_win, unsigned int a_priority)
  : m_priority(a_priority),
    m_topWin(top_win)
  {}

  unsigned int priority() const { return m_priority; }

  /** Create a visualiser for the given inputs or return NULL.
   */
  PixA::IVisualiser *visualiser(const std::string &/*histo_name*/,
				const std::vector<std::string> &/*folder_hierarchy*/) const {
    return new LinkVisualiser(m_topWin);
  }

  static PixA::IVisualisationKit *createKit(QWidget *top_win, unsigned int a_priority) {
    return new LinkVisualisationKit(top_win, a_priority);
  }


 private:
  unsigned int m_priority;
  QWidget *m_topWin;
};


#include <Visualiser/VisualisationKitSubSet_t.h>


void registerLinkVisualiser(QWidget *top_win, unsigned int a_priority, bool only_if_lower_priorotiy_kits) {
  if (a_priority==0) only_if_lower_priorotiy_kits=true;
  if ( !only_if_lower_priorotiy_kits || !hasKit("Link_map", a_priority-1)) {
    PixA::VisualisationFactory::registerKit("^/Link_map$",         LinkVisualisationKit::createKit(top_win, a_priority) );
  }
}
