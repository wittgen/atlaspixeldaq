#include "BusyLoopFactory.h"

#include <qapplication.h>
#include <iostream>

namespace PixA {

  class DummyBusyLoop : public IBusyLoop
  {
  public:
    DummyBusyLoop()  { QApplication::setOverrideCursor( Qt::WaitCursor ); }
    ~DummyBusyLoop() { QApplication::restoreOverrideCursor(); }

    void changeMessage(const std::string &/*message*/) {}

  };

  class DummyBusyLoopFactory : public IBusyLoopFactory {
  public:
    IBusyLoop *busyLoop(const std::string &message,
			QDialog*,
			unsigned int /*dialog_after_time */,
			unsigned int /*update_time */) {
      std::cout << "INFO [BusyLoopFactory::busyLoop] " << message << std::endl;
      return new DummyBusyLoop;
    }

  };

  IBusyLoopFactory *BusyLoopFactory::createInstance() {
    return new DummyBusyLoopFactory;
  }

  IBusyLoopFactory *BusyLoopFactory::s_instance=NULL;

}
