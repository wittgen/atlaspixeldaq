#ifndef _SummaryHistoVisualiser_h_
#define _SummaryHistoVisualiser_h_

#include <Visualiser/ISimpleVisualiser.h>
#include <Visualiser/IVisualisationKit.h>
#include <DataContainer/HistoRef.h>
#include <ConfigWrapper/Connectivity.h>

//class SummaryHistoForm;
class Ui_SummaryHistoFormBase;

class SummaryHistoVisualiser : public PixA::ISimpleVisualiser {
  friend class SummaryHistoForm;
 public:

  SummaryHistoVisualiser(const std::string &histo_name)
    : m_dialog(NULL), m_histoName(histo_name) {}

  ~SummaryHistoVisualiser();
  /** Get the name of the visualiser.
   */
  const std::string &name() const { return s_name;}

  /** Returns true, if the visualiser can optionally display only one front-end.
   */
  bool canVisualiseOneFrontEnd() const {return false;}

  /** Returns the maximum number of modules the visualiser can display simultaneously.
   */
  unsigned int canVisualiseNModules() const { return 1; }

  /** Fill the histogram info structure.
   * The structure contains the index ranges for each dimension.
   * @sa HistoInfo_t
   */
  void fillIndexArray(PixA::IScanDataProvider &data_provider, const std::vector<std::string> &module_names, HistoInfo_t &info) const;

  /** Perform the chosen visualisation for the given front_end and the given index set.
   * @param option the visualisation option.
   * @param index the index set to specify the histogram in the multi dimensional histogram collection.
   * @param front_end the number of the front_end (0-15) which should be visualised or -1 for all front-ends
   * The front-end parameter will be ignored if canVisualiseOneFrontEnd returns false.
   */
  void visualise(PixA::IScanDataProvider &data_provider, const std::vector<std::string> &module_names, int /*option*/, const PixA::Index_t &index, int front_end);

  void closeDialog();

 protected:

 private:
  std::string            m_currentConnName;
  Ui_SummaryHistoFormBase * m_dialog;
  static std::string    s_name;
  std::string           m_histoName;
};

/** Auxillary class to create the summary histo visualiser.
 */
class SummaryHistoVisualisationKit : public PixA::IVisualisationKit
{
 public:
  SummaryHistoVisualisationKit();

  unsigned int priority() const {return PixA::IVisualisationKit::normalPriority + 2;}

  /** Create a visualiser for the given inputs or return NULL.
   */
  PixA::IVisualiser *visualiser(const std::string &histo_name,
				const std::vector<std::string> &/*folder_hierarchy*/) const;

};

#endif
