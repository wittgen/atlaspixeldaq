#ifndef _ConfigVisualisationKit_h_
#define _ConfigVisualisationKit_h_

//#include <Visualiser/ISimpleVisualiser.h>
#include <Visualiser/IVisualisationKit.h>
//#include <DataContainer/ScanConfig.h>
//#include <ConfigWrapper/Connectivity.h>

#include <string>
#include <map>
#include <iostream>

class QWidget;

bool hasKit(const std::string &name, unsigned int min_priority);
void registerConfigVisualiser(QWidget *top_win, unsigned int a_priority, bool only_if_lower_priorotiy_kits=true);

#endif
