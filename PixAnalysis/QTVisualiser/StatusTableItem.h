#ifndef _StatusTableItem_h_

#include <q3table.h>

class StatusTableItem : public Q3TableItem
{
public:
  enum EStatus {kPassed, kFailed, kUnknown, kNStati };

  StatusTableItem( Q3Table * table, EStatus status, bool enabled=true)
    : Q3TableItem ( table, Q3TableItem::Never, "" ), m_status(status), m_enabled(enabled) { if (m_status>=kNStati) m_status=kUnknown;}

  void paint ( QPainter * p, const QColorGroup & cg, const QRect & cr, bool selected );
 protected:
  EStatus m_status;
  bool    m_enabled;
  static const char *s_statusName[kNStati];
  static const QColor s_statusColor[kNStati][2];
};

#endif
