#ifndef _SummaryHistoForm_h_
#define _SummaryHistoForm_h_

#include "ui_SummaryHistoFormBase.h"
#include <memory>
#include <string>
#include <Visualiser/IScanDataProvider.h>
//Added by qt3to4:
#include <QCloseEvent>

class QCloseEvent;

class SummaryHistoVisualiser;

class SummaryHistoForm : public QDialog, public Ui_SummaryHistoFormBase
{
public:

  SummaryHistoForm(SummaryHistoVisualiser *visualiser,
		   const PixA::ConnectivityRef &connectivity,
		   const std::string &rod_name,
		   const PixA::Histo *histo,
		   QWidget* parent = 0,
		   const char* name = 0,
		   bool modal = FALSE,
		   Qt::WFlags fl = 0 );

  //  ~SummaryHistoForm();

private:
  void closeEvent ( QCloseEvent * e );

  enum EVarType {ModVar,Pp0Var, GlobalVar, NVarTypes};

  class VarInfo_t {
  public:
    VarInfo_t() : m_index(0), m_varType(NVarTypes) {}

    /** Constructor for status vars.
     */
    VarInfo_t(const std::string &name, unsigned int index, EVarType var_type=ModVar)
      : m_name(name), m_index(index), m_varType(var_type),m_statusVar(true) {}

    /** Constructor for measurements.
     */
    VarInfo_t(const std::string &name, const std::string unit, unsigned int index, EVarType var_type=ModVar) 
      : m_name(name), m_index(index), m_varType(var_type),m_unit(unit),m_statusVar(false) {}

    const std::string &name() const   {return m_name;}
    const std::string &unit() const   {return m_unit;}
    const unsigned int index() const  {return m_index;}
    const EVarType varType() const    {return m_varType;}
    operator bool() const             {return m_varType < NVarTypes;}
    bool isStatus() const             {return m_statusVar; } 

  private:
    std::string  m_name;
    unsigned int m_index;
    EVarType     m_varType;
    std::string  m_unit;
    bool         m_statusVar;
  };

  enum EBinType {BinVarType,BinTag, BinTime,BinPp0Mask, BinModMask,BinFeMask,NSpecialBins};
  //  enum ELogVarType {V_dd, V_dda, V_hv, I_d, I_a, I_hv, V_iset, V_vdc, V_pin, I_iset, I_vdc, I_pin, T_mod, T_opto, status_mod, status_opto, status_global};
  enum ELogVarType {V_dd, V_dda, V_hv, I_d, I_a, I_hv, V_iset, V_vdc, V_pin, I_iset, I_vdc, I_pin, T_mod, T_opto, status_mod, status_opto, status_global,
		    I_pin_mod, iolink_ratio1, iolink_ratio2, iolink_drx};
  enum LogTag {Initial, Final, Sub1, Sub2, Sub3, Sub4, Sub5, Sub6, CheckIviset, CheckDI, CheckAI, CheckHVI};
  enum LogStatus {Passed, Failed, Unknown};

  const std::string &makeStatusEntry(const double &value);
  static std::string makeTimeEntry(const double &value);
  static std::string makeMaskEntry(const double &value);
  //  static std::string makeEntry(ELogVarType type,  const double &value);
  std::string makeEntry(std::map<int,VarInfo_t>::const_iterator a_type_iter,  const double &value);

  std::map<int, VarInfo_t > m_logVarType;
  std::map<int, std::string > m_logStatus;
  std::map<int, std::string > m_logTag;

  SummaryHistoVisualiser *m_visualiser;
};

#endif
