
#include "SummaryHistoForm.h"
#include "SummaryHistoVisualiser.h"
#include <Histo/Histo.h>

#include <PixUtilities/PixTime.h>

#include <sstream>
#include <iomanip>
#include <q3table.h>
#include <qlabel.h>
//Added by qt3to4:
#include <QCloseEvent>

#include "StatusTableItem.h"
#include "MaskableTableItem.h"

#ifdef _PIXA_HISTO_IS_ROOT_TH1
#  include <TH1.h>
#endif
#include <DataContainer/GenericHistogramAccess.h>


SummaryHistoForm::SummaryHistoForm(SummaryHistoVisualiser *visualiser,
				   const PixA::ConnectivityRef &connectivity,
				   const std::string &rod_name,
				   const PixA::Histo *histo,
				   QWidget* parent,
				   const char* name,
				   bool modal,
				   Qt::WFlags fl  )
//  : SummaryHistoFormBase(parent,name,modal,fl) ,
: QDialog(parent),  
  m_visualiser(visualiser)
{
    setupUi(this );
  if (!histo || !PixA::is2D(histo) || PixA::nRows(histo)==0 || PixA::nColumns(histo)< NSpecialBins) {
    m_moduleVarTable->hide();
    m_pp0VarTable->hide();
    m_globalVarTable->hide();
  }
  else {
    m_title->setText(name);
    unsigned int index=1;
    unsigned int pp0_index=1;
    m_logVarType[status_global] = VarInfo_t("status",0,GlobalVar);
    m_logVarType[status_mod] = VarInfo_t("status mod",index++);
    m_logVarType[V_dd] = VarInfo_t("V_dd","V",index++);
    m_logVarType[V_dda] = VarInfo_t("V_dda","V",index++);
    m_logVarType[V_hv] = VarInfo_t("V_hv","V", index++);
    m_logVarType[I_d] = VarInfo_t("I_d","A",index++);
    m_logVarType[I_a] = VarInfo_t("I_a","A",index++);
    m_logVarType[I_hv] = VarInfo_t("I_hv","A",index++);
    m_logVarType[T_mod] = VarInfo_t("T_mod","C",index++);
    m_logVarType[I_pin_mod] = VarInfo_t("I_pin_mod","A",index++);
    m_logVarType[iolink_ratio1] = VarInfo_t("iolink_ratio1","",index++);
    m_logVarType[iolink_ratio2] = VarInfo_t("iolink_ratio2","",index++);
    m_logVarType[iolink_drx] = VarInfo_t("iolink_drx","muA",index++);

    m_logVarType[status_opto] = VarInfo_t("status opto",pp0_index++,Pp0Var);
    m_logVarType[V_iset] = VarInfo_t("V_iset","V",pp0_index++,Pp0Var);
    m_logVarType[V_vdc] = VarInfo_t("V_vdc","V",pp0_index++,Pp0Var);
    m_logVarType[V_pin] = VarInfo_t( "V_pin","V",pp0_index++,Pp0Var);
    m_logVarType[I_iset] = VarInfo_t("I_iset","A",pp0_index++,Pp0Var);
    m_logVarType[I_vdc] = VarInfo_t("I_vdc","A",pp0_index++,Pp0Var);
    m_logVarType[I_pin] = VarInfo_t("I_pin","A",pp0_index++,Pp0Var);
    m_logVarType[T_opto] = VarInfo_t("T_opto","C",pp0_index++,Pp0Var);

    m_logStatus[Passed] = "Passed";
    m_logStatus[Failed] = "Failed";
    m_logStatus[Unknown] = "Unknown";

    m_logTag[Initial] = "Initial";
    m_logTag[Final] = "Final";
    m_logTag[Sub1] = "Sub1";
    m_logTag[Sub2] = "Sub2";
    m_logTag[Sub3] = "Sub3";
    m_logTag[Sub4] = "Sub4";
    m_logTag[Sub5] = "Sub5";
    m_logTag[Sub6] = "Sub6";
    m_logTag[CheckIviset] = "CheckIviset";
    m_logTag[CheckDI] = "CheckDI";
    m_logTag[CheckAI] = "CheckAI";
    m_logTag[CheckHVI] = "CheckHVI";

    std::map<unsigned int, unsigned int> mod_map;
    std::map<unsigned int, unsigned int> pp0_map;
    unsigned int all_mods=0;
    unsigned int all_pp0s=0;
    for (unsigned int row_i=0; row_i<static_cast<unsigned int>( PixA::nRows(histo)); row_i++) {

      unsigned int mod_mask = static_cast<unsigned int>( PixA::binContent(histo,PixA::startColumn(histo)+BinModMask,PixA::startRow(histo)+row_i) );
     all_mods |= mod_mask;

     unsigned int pp0_mask = static_cast<unsigned int>( PixA::binContent(histo,PixA::startColumn(histo)+BinPp0Mask,PixA::startRow(histo)+row_i) );
     all_pp0s |= pp0_mask;
    }

    all_pp0s=0xf;
    all_mods=0xffffffff;

    // set column headers for Module var table
    unsigned int n_global_mod_columns=0;

    //      unsigned int mod_mask = static_cast<unsigned int>( PixA::binContent(histo,PixA::startColumn(histo)+BinModMask,PixA::startRow(histo)+row_i) );

    // find out which modules to display
    n_global_mod_columns=3;
    m_moduleVarTable->setNumCols(n_global_mod_columns);
    m_moduleVarTable->horizontalHeader()->setLabel(0,"Tag");
    m_moduleVarTable->horizontalHeader()->setLabel(1,"Time");
    m_moduleVarTable->horizontalHeader()->setLabel(2,"FE mask");

    // JFA: Initial loop to check if SummaryHisto is for Inlink
    bool isInLink = false;
    for (unsigned int row_i=0; row_i< PixA::nRows(histo); row_i++) {

       std::map<int, VarInfo_t >::const_iterator 
         type_iter =  m_logVarType.find(static_cast<ELogVarType>( static_cast<unsigned int>( PixA::binContent(histo,
													      PixA::startColumn(histo)+BinVarType,
													      PixA::startRow(histo)+row_i))) );
       if (type_iter != m_logVarType.end() && type_iter->first == I_pin_mod) {
         isInLink = true;
	 break;
       }
    }

    if (connectivity && !isInLink) {

      // if the connectivity exists:
      unsigned int counter=0;

      const PixA::Pp0List &pp0s=connectivity.pp0s(rod_name);
      for (PixA::Pp0List::const_iterator pp0_iter=pp0s.begin();
	   pp0_iter != pp0s.end(); // may be slightly inefficient unless the compiler is smart enough.
	   ++pp0_iter) {
	// it may be more efficient to call modulesBegin and modulesEnd only once:
	PixA::ModuleList::const_iterator modules_begin=PixA::modulesBegin(pp0_iter);
	PixA::ModuleList::const_iterator modules_end=PixA::modulesEnd(pp0_iter);
	for (PixA::ModuleList::const_iterator module_iter=modules_begin;
	     module_iter != modules_end;
	     ++module_iter) {
	  mod_map.insert(std::make_pair((*module_iter)->modId,counter));
	  m_moduleVarTable->insertColumns(m_moduleVarTable->numCols());
	 m_moduleVarTable->horizontalHeader()->setLabel(counter+n_global_mod_columns,QString::fromStdString( const_cast<PixLib::ModuleConnectivity *>(*module_iter)->name()));
	  counter++;

	}
      }
    }
    else {
      // if there is no connectivity
      unsigned int counter=0;
      for (unsigned int mod_i=0; mod_i<32; mod_i++) {
	if ( (all_mods & (1<<mod_i)) ) {
	  mod_map.insert(std::make_pair(mod_i, counter));
	  counter++;
	}
      }

      for (std::map<unsigned int, unsigned int>::const_iterator mod_iter=mod_map.begin();
	   mod_iter != mod_map.end();
	   mod_iter++) {
	std::stringstream mod_name;
	mod_name << "Mod" << mod_iter->first;
	m_moduleVarTable->insertColumns(m_moduleVarTable->numCols());
	m_moduleVarTable->horizontalHeader()->setLabel(mod_iter->second+n_global_mod_columns, mod_name.str().c_str());
      }
    }

    unsigned int n_global_pp0_columns=2;
    m_pp0VarTable->setNumCols(n_global_pp0_columns);
   m_pp0VarTable->horizontalHeader()->setLabel(0,"Tag");
    m_pp0VarTable->horizontalHeader()->setLabel(1,"Time");


    unsigned int n_global_columns=3;
    m_globalVarTable->setNumCols(n_global_columns);
    m_globalVarTable->horizontalHeader()->setLabel(0,"Tag");
   m_globalVarTable->horizontalHeader()->setLabel(1,"Time");
    m_globalVarTable->horizontalHeader()->setLabel(2,"Status");
    m_globalVarTable->hide();

    if (connectivity) {
      const PixA::Pp0List &pp0s=connectivity.pp0s(rod_name);
      unsigned int counter=0;
      for (PixA::Pp0List::const_iterator pp0_iter=pp0s.begin();
	   pp0_iter != pp0s.end(); // may be slightly inefficient unless the compiler is smart enough.
	   ++pp0_iter) {
	pp0_map.insert(std::make_pair(pp0_iter.slot(), counter));
	m_pp0VarTable->insertColumns(m_pp0VarTable->numCols());
	m_pp0VarTable->horizontalHeader()->setLabel(counter+n_global_pp0_columns, QString::fromStdString(const_cast<PixLib::Pp0Connectivity *>(*pp0_iter)->name()));
	counter++;
      }
    }
    else {
      // set column headers for Pp0 var table
      unsigned int n_global_pp0_columns=0;
      {
	unsigned int counter=0;

	for (unsigned int pp0_i=0; pp0_i<4; pp0_i++) {
	  if ( (all_pp0s & (1<<pp0_i)) ) {
	    pp0_map.insert(std::make_pair(pp0_i, counter));
	    counter++;
	  }
	}

	for (std::map<unsigned int, unsigned int>::const_iterator pp0_iter=pp0_map.begin();
	     pp0_iter != pp0_map.end();
	     pp0_iter++) {
	  std::stringstream pp0_name;
	  pp0_name << "Pp0" << pp0_iter->first;
	  m_pp0VarTable->insertColumns(m_pp0VarTable->numCols());
	  m_pp0VarTable->horizontalHeader()->setLabel(pp0_iter->second+n_global_pp0_columns, pp0_name.str().c_str());
	}

      }
    }

    m_moduleVarTable->setNumRows(0);
    m_pp0VarTable->setNumRows(0);
    for (unsigned int row_i=0; row_i< PixA::nRows(histo); row_i++) {

	std::map<int, VarInfo_t >::const_iterator 
	  type_iter =  m_logVarType.find(static_cast<ELogVarType>( static_cast<unsigned int>( PixA::binContent(histo,
													       PixA::startColumn(histo)+BinVarType,
													       PixA::startRow(histo)+row_i))) );

      if (type_iter != m_logVarType.end() && type_iter->second.varType()==ModVar) {

	unsigned int mod_mask = static_cast<unsigned int>( PixA::binContent(histo,PixA::startColumn(histo)+BinModMask,PixA::startRow(histo)+row_i) );

	int new_row_i = m_moduleVarTable->numRows();
        m_moduleVarTable->insertRows(new_row_i,1);
        m_moduleVarTable->verticalHeader()->setLabel(new_row_i, QString::fromStdString(type_iter->second.name()));

        std::map<int, std::string >::const_iterator
	  tag_iter = m_logTag.find( static_cast<ELogVarType>( static_cast<unsigned int>( PixA::binContent(histo,
													  PixA::startColumn(histo)+BinTag,
													  PixA::startRow(histo)+row_i)) ));

        unsigned int col_i=0;
        if (tag_iter != m_logTag.end()) {
	  m_moduleVarTable->setText( new_row_i, col_i++ , tag_iter->second.c_str() );
	}
	else {
	  m_moduleVarTable->setText( new_row_i, col_i++ , "" );
	  std::cout << "WARNING [SummaryHistoForm::ctor] Unknown tag = " << BinTag << " in row " << new_row_i << std::endl;
	}

    //    m_moduleVarTable->setText( new_row_i, col_i++, makeTimeEntry( PixA::binContent(histo,PixA::startColumn(histo)+BinTime,PixA::startRow(histo)+row_i)) );
        m_moduleVarTable->setText( new_row_i, col_i++, QString::fromStdString(makeMaskEntry( PixA::binContent(histo,PixA::startColumn(histo)+BinFeMask,PixA::startRow(histo)+row_i))) );

	for (unsigned int mod_i=0; mod_i<32;mod_i++) {
	  std::map<unsigned int, unsigned int>::const_iterator mod_iter = mod_map.find(mod_i);
	  if (mod_iter != mod_map.end()) {
	    unsigned int mod_column=mod_iter->second+n_global_mod_columns;
	    if (type_iter->second.isStatus() ) {
	      m_moduleVarTable->setItem(new_row_i, mod_column,
					new StatusTableItem(m_moduleVarTable, 
							    static_cast<StatusTableItem::EStatus>(static_cast<unsigned short>( PixA::binContent(histo,
																		PixA::startColumn(histo)+NSpecialBins+mod_i,
																		PixA::startRow(histo)+row_i))),
							    mod_mask & (1<<mod_i)));
	    }
	    else {
	      m_moduleVarTable->setItem( new_row_i, mod_column, new MaskableTableItem(m_moduleVarTable,
										      makeEntry( type_iter, PixA::binContent(histo,
															     PixA::startColumn(histo)+NSpecialBins+mod_i,
															     PixA::startRow(histo)+row_i)).c_str(),
										   mod_mask & (1<<mod_i))) ;
	    }
	  }
	}

      }
      else if (type_iter != m_logVarType.end() && type_iter->second.varType()==Pp0Var) {

	unsigned int pp0_mask = static_cast<unsigned int>( PixA::binContent(histo,PixA::startColumn(histo)+BinPp0Mask,PixA::startRow(histo)+row_i) );

	int new_row_i = m_pp0VarTable->numRows();
        m_pp0VarTable->insertRows(new_row_i,1);
       m_pp0VarTable->verticalHeader()->setLabel(new_row_i, QString::fromStdString(type_iter->second.name()));
        std::map<int, std::string >::const_iterator tag_iter = m_logTag.find( static_cast<unsigned int>( PixA::binContent(histo,
															  PixA::startColumn(histo)+BinTag,
															  PixA::startRow(histo)+row_i) ) );
        unsigned int col_i=0;
        if (tag_iter != m_logTag.end()) {
	  m_pp0VarTable->setText( new_row_i, col_i++ , tag_iter->second.c_str() );
	}
	else {
	  m_pp0VarTable->setText( new_row_i, col_i++ , "" );
	  std::cout << "WARNING [SummaryHistoForm::ctor] Unknown tag = " << BinTag << " in row " << new_row_i << std::endl;
	}

        m_pp0VarTable->setText( new_row_i, col_i++, QString::fromStdString(makeTimeEntry( PixA::binContent(histo,PixA::startColumn(histo)+BinTime,PixA::startRow(histo)+row_i)) ));
	 m_pp0VarTable->setText( new_row_i, col_i++, QString::fromStdString(makeMaskEntry( PixA::binContent(histo,PixA::startColumn(histo)+BinFeMask,PixA::startRow(histo)+row_i))) );

	//      unsigned int pp0_mask = static_cast<unsigned int>( PixA::binContent(histo,PixA::startColumn(histo)+BinPp0Mask,PixA::startRow(histo)+row_i) );
        for (unsigned int pp0_i=0; pp0_i<4;pp0_i++) {
	  std::map<unsigned int, unsigned int>::const_iterator pp0_iter = pp0_map.find(pp0_i);
	  if (pp0_iter != pp0_map.end()) {
	    unsigned int pp0_column = pp0_iter->second + n_global_pp0_columns;
	    if (type_iter->second.isStatus() ) {
	        m_pp0VarTable->setItem(new_row_i, pp0_column,
				       new StatusTableItem(m_pp0VarTable, 
							   static_cast<StatusTableItem::EStatus>(static_cast<unsigned short>( PixA::binContent(histo,
																	       PixA::startColumn(histo)+NSpecialBins+pp0_i,
																	       PixA::startRow(histo)+row_i) )),
							   pp0_mask & (1<<pp0_i)));
	    }
	    else {
	      m_pp0VarTable->setItem( new_row_i, pp0_column, new MaskableTableItem(m_pp0VarTable,
										   makeEntry( type_iter, PixA::binContent(histo,
															  PixA::startColumn(histo)+NSpecialBins+pp0_i,
															  PixA::startRow(histo)+row_i)).c_str(),
										pp0_mask & (1<<pp0_i)));
	    }
	  }
	}

      }
      else if (type_iter != m_logVarType.end() && type_iter->second.varType()==GlobalVar) {
	if (m_globalVarTable->isHidden()) {
	  m_globalVarTable->show();
	}
	int new_row_i = m_globalVarTable->numRows();
        m_globalVarTable->insertRows(new_row_i,1);
        m_globalVarTable->verticalHeader()->setLabel(new_row_i, "Global" );
        unsigned int col_i=0;
	//        m_moduleVarTable->setText( new_row_i, col_i++, "global" );
	//        m_moduleVarTable->setText( new_row_i, col_i++, makeTimeEntry( PixA::binContent(histo,PixA::startColumn(histo)+BinTime,PixA::startRow(histo)+row_i)) );
	//        m_moduleVarTable->setText( new_row_i, col_i++, makeMaskEntry( PixA::binContent(histo,PixA::startColumn(histo)+BinFeMask,PixA::startRow(histo)+row_i)) );
	
	//        m_moduleVarTable->setText( new_row_i, col_i++, makeStatusEntry( PixA::binContent(histo,PixA::startColumn(histo)+NSpecialBins,PixA::startRow(histo)+row_i)) );
	
        m_globalVarTable->setText( new_row_i, col_i++, "" ); //empty
        m_globalVarTable->setText( new_row_i, col_i++,QString::fromStdString( makeTimeEntry( PixA::binContent(histo,PixA::startColumn(histo)+BinTime,PixA::startRow(histo)+row_i))) );
	//        m_globalVarTable->setText( new_row_i, col_i++, makeMaskEntry( PixA::binContent(histo,PixA::startColumn(histo)+BinFeMask,PixA::startRow(histo)+row_i)) );
	m_globalVarTable->setItem(new_row_i, col_i++,
				  new StatusTableItem(m_globalVarTable,
						      static_cast<StatusTableItem::EStatus>(static_cast<unsigned short>( PixA::binContent(histo,
																	  PixA::startColumn(histo)+NSpecialBins,
																	  PixA::startRow(histo)+row_i))) ));
	//        m_globalVarTable->setText( new_row_i, col_i++, makeStatusEntry( PixA::binContent(histo,PixA::startColumn(histo)+NSpecialBins,PixA::startRow(histo)+row_i)) );
	
      }
    }
  }
  for (int col_i=0; col_i< m_moduleVarTable->numCols(); col_i++) {
    m_moduleVarTable->adjustColumn(col_i);
  }
  for (int col_i=0; col_i< m_pp0VarTable->numCols(); col_i++) {
    m_pp0VarTable->adjustColumn(col_i);
  }
}

void SummaryHistoForm::closeEvent ( QCloseEvent * ) {
  if (m_visualiser) {
    m_visualiser->closeDialog();
  }
}


const std::string &SummaryHistoForm::makeStatusEntry(const double &value) {
  std::map<int, std::string >::const_iterator status_iter = m_logStatus.find( static_cast<int>(value) );
  if (status_iter != m_logStatus.end()) {
    return status_iter->second;
  }
  return m_logStatus[Unknown];
}

std::string SummaryHistoForm::makeTimeEntry(const double &value) {
  return PixLib::TimeToStr(static_cast<unsigned int>(value) );
}

std::string SummaryHistoForm::makeMaskEntry(const double &value) {
  std::stringstream temp;
  temp << std::hex << static_cast<unsigned int>(value) << std::dec;
  return temp.str();
}

std::string SummaryHistoForm::makeEntry(std::map<int,VarInfo_t>::const_iterator a_type_iter,  const double &value) {
  //  ELogVarType type = static_cast<ELogVarType>( a_type_iter->first );
  if (!a_type_iter->second.isStatus()) {
    std::stringstream temp;
    temp << std::setw(6) << ::std::fixed << std::setprecision(3) << value << ::std::setprecision( 6 );
    if (!a_type_iter->second.unit().empty()) {
      temp << a_type_iter->second.unit();
    }
    return temp.str();
  }
  
  return makeStatusEntry(value);
}
