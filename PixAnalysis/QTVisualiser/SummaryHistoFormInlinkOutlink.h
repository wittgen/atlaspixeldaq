#ifndef _SummaryHistoFormInlinkOutlink_h_
#define _SummaryHistoFormInlinkOutlink_h_

#include "SummaryHistoFormBase.h"
#include <memory>
#include <string>
#include <Visualiser/IScanDataProvider.h>
//Added by qt3to4:
#include <QCloseEvent>

class QCloseEvent;

class SummaryHistoVisualiser;

class SummaryHistoFormInlinkOutlink : public QDialog, public Ui_SummaryHistoFormBase
{
public:

  SummaryHistoFormInlinkOutlink(SummaryHistoVisualiser *visualiser, const PixA::Histo *histo, QWidget* parent = 0, const char* name = 0, bool modal = FALSE, Qt::WFlags fl = 0 );
  //  ~SummaryHistoForm();

private:
  void closeEvent ( QCloseEvent * e );
  void LookForNoisyChannels( const PixA::Histo *histo );

  inline const std::string formatOutlink(const unsigned int outlink);

  SummaryHistoVisualiser *m_visualiser;
  std::list<unsigned int> noisyOutlinks;
};

#endif
