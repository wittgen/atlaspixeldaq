#include <MaskableTableItem.h>

#include <qfont.h>
#include <qpainter.h>


const QColor MaskableTableItem::s_statusColor[2] = {
 QColor(200,200,200), QColor(0,0,0)  
};

void MaskableTableItem::paint ( QPainter * p, const QColorGroup & /*cg*/, const QRect & /*cr*/, bool selected ) {
  QFont a_font(p->font());
  if (selected) {
    a_font.setBold(true);
   p->setFont(a_font);
  }
  else {
    a_font.setBold(false);
    p->setFont(a_font);
  }
  p->setPen( s_statusColor[m_enabled] );
  p->drawText (table()->cellRect(row(), col()), Qt::AlignBottom | Qt::AlignHCenter , text());
}
