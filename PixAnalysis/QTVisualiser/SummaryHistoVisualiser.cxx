#include "SummaryHistoVisualiser.h"
#include <DataContainer/HistoUtil.h>
#include <TCanvas.h>
#include <TROOT.h>
#include <TH2.h>
#include <TH1.h>
#include <memory>
#include <Histo/Histo.h>

#include <Visualiser/VisualisationFactory.h>
#include <Visualiser/IScanDataProvider.h>
#include "SummaryHistoFormBase.h"
#include "SummaryHistoForm.h"
#include "SummaryHistoFormInlinkOutlink.h"
#include "SummaryHistoFormPowerMeasurement.h"

#include <qdialog.h>
#include <sstream>

std::string SummaryHistoVisualiser::s_name("Show Table");

SummaryHistoVisualiser::~SummaryHistoVisualiser() {
  delete m_dialog;
}

void SummaryHistoVisualiser::fillIndexArray(PixA::IScanDataProvider &data_provider, const std::vector<std::string> &module_names, HistoInfo_t &info) const {
  assert( module_names.size() == 1);
  data_provider.numberOfHistos(module_names.front(), m_histoName,info);
}

void SummaryHistoVisualiser::visualise(PixA::IScanDataProvider &data_provider, const std::vector<std::string> &module_names, int /*option*/, const PixA::Index_t &index, int /*front_end*/)
{
  assert( module_names.size() == 1);
  const PixA::Histo *a_histo = data_provider.histogram( module_names.front(), m_histoName, index);
  if (a_histo) {
    //   PixLib::SummaryHisto summary;
    if (m_dialog && m_currentConnName != module_names.front() ) {
      closeDialog();
    }
    if (!m_dialog) {
      m_currentConnName = module_names.front();
      std::stringstream a_name;
      a_name << module_names[0] << " : " << m_histoName;

      if(std::strstr("InlinkOutlink",m_histoName.c_str())) //cla
	//	m_dialog=new SummaryHistoFormInlinkOutlink(this, a_histo.release(), NULL, a_name.str().c_str(),FALSE );
	m_dialog=new SummaryHistoFormPowerMeasurement(this, a_histo, NULL, a_name.str().c_str(),FALSE ); //temp
      else {
	if(std::strstr("PowerMesurments",m_histoName.c_str())) {
	  m_dialog=new SummaryHistoFormPowerMeasurement(this, a_histo, NULL, a_name.str().c_str(),FALSE );
	}
	else {
	  m_dialog=new SummaryHistoForm(this, data_provider.connectivity(), module_names.front(), a_histo, NULL, a_name.str().c_str(),FALSE );
	}
      }
    }
   // m_dialog->show();
  }
}

void SummaryHistoVisualiser::closeDialog()
{
  delete m_dialog;
  m_dialog=NULL;
}

SummaryHistoVisualisationKit::SummaryHistoVisualisationKit()  {
  PixA::VisualisationFactory::registerKit("/HistoSummary$",this);
  PixA::VisualisationFactory::registerKit("/InlinkOutlink$",this); //cla
  PixA::VisualisationFactory::registerKit("/PowerMesurements$",this); //cla
}

PixA::IVisualiser *SummaryHistoVisualisationKit::visualiser(const std::string &histo_name, 
							    const std::vector<std::string> &/*folder_hierarchy*/) const {
  return new SummaryHistoVisualiser(histo_name);
}

SummaryHistoVisualisationKit a_SummaryHistoVisualisationKit_instance;
