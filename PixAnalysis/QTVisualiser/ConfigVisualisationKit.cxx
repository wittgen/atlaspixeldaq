#include "ConfigVisualisationKit.h"
#include <ConfigWrapper/ScanConfig.h>
#include <Visualiser/ISimpleVisualiser.h>
#include <Visualiser/IScanDataProvider.h>
#include "ConfigViewer.h"
#include <DataContainer/HistoInfo_t.h>
#include <ConfigWrapper/ConnectivityUtil.h>

#include "BusyLoopFactory.h"

#include <Visualiser/VisualisationFactory.h>
#include <iomanip>

class IConfigAccessor
{
public:
  virtual ~IConfigAccessor() {}
  virtual const PixA::ConfigHandle configHandle(PixA::IScanDataProvider &data_provider, const std::string &object_name) const = 0 ;
  virtual std::string title(PixA::IScanDataProvider &data_provider, const std::string &object_name) const = 0;
  virtual const std::string &configName() const = 0 ;
};

std::string serialNumberTrailer(PixA::IScanDataProvider &data_provider) {
  std::stringstream a_trailer;
  a_trailer << " (S" << std::setfill('0') << std::setw(9) << data_provider.scanNumber() << ")";
  return a_trailer.str();
}

class ScanConfigAccessor : public IConfigAccessor
{
public:
  const PixA::ConfigHandle configHandle(PixA::IScanDataProvider &data_provider, const std::string &object_name) const {
    return data_provider.scanConfig((std::string&)object_name);
  }

  std::string title(PixA::IScanDataProvider &data_provider, const std::string &/*object_name*/) const {
    return s_configTitle + serialNumberTrailer(data_provider) ;
  }

  const std::string &configName() const { return s_configTitle; }

private:
  static const std::string s_configTitle;
};

const std::string ScanConfigAccessor::s_configTitle("Scan Configuration");


class ModuleConfigAccessor : public IConfigAccessor
{
public:
  const PixA::ConfigHandle configHandle(PixA::IScanDataProvider &data_provider, const std::string &object_name) const {
    return data_provider.moduleConfig(object_name);
  }

  std::string title(PixA::IScanDataProvider &data_provider, const std::string &object_name) const {
    return s_configTitle + object_name  + serialNumberTrailer(data_provider);
  }

  const std::string &configName() const { return s_configTitle; }

private:
  static const std::string s_configTitle;
};

const std::string ModuleConfigAccessor::s_configTitle("Module Configuration");


class RodConfigAccessorBase : public IConfigAccessor
{
protected:

  std::string rodName(PixA::IScanDataProvider &data_provider, const std::string &object_name) const {
    if (object_name==m_lastObjectName) {
      return m_lastRodName;
    }
    m_lastObjectName = object_name;

    PixA::ConnectivityRef conn( data_provider.connectivity() );
    if (conn) {
      const PixLib::RodBocConnectivity *rod_conn = PixA::connectedRod( conn.findModule( object_name ));
      if (!rod_conn) {
	rod_conn = conn.findRod( object_name );
      }
      if (rod_conn) {
	m_lastRodName = PixA::connectivityName(rod_conn);
	return m_lastRodName;
      }
    }
    m_lastRodName.clear();
    return m_lastRodName;
  }

  std::string rodTitle(const std::string &prefix, PixA::IScanDataProvider &data_provider, const std::string &object_name) const {
    std::string rod_name = rodName(data_provider, object_name);
    if (rod_name.empty()) {
      rod_name = "<unknown>";
    }
    return prefix + " : " + rod_name +  serialNumberTrailer(data_provider);
  }

private:
  mutable std::string m_lastObjectName;
  mutable std::string m_lastRodName;
};


class ModuleGroupConfigAccessor : public RodConfigAccessorBase
{
public:

  const PixA::ConfigHandle configHandle(PixA::IScanDataProvider &data_provider, const std::string &object_name) const {
    std::string rod_name = rodName(data_provider, object_name);
    return data_provider.moduleGroupConfig(rod_name);
  }

  std::string title(PixA::IScanDataProvider &data_provider, const std::string &object_name) const {
    return  rodTitle(s_configTitle, data_provider, object_name) ;
  }

  const std::string &configName() const { return s_configTitle; }

private:
  static const std::string s_configTitle;
};

const std::string ModuleGroupConfigAccessor::s_configTitle("Module group configuration");

class RodControllerConfigAccessor : public RodConfigAccessorBase
{
public:

  const PixA::ConfigHandle configHandle(PixA::IScanDataProvider &data_provider, const std::string &object_name) const {
    std::string rod_name = rodName(data_provider, object_name);
    return data_provider.rodConfig(rod_name);
  }

  std::string title(PixA::IScanDataProvider &data_provider, const std::string &object_name) const {
    return  rodTitle(s_configTitle, data_provider, object_name) ;
  }

  const std::string &configName() const { return s_configTitle; }

private:
  static const std::string s_configTitle;
};

const std::string RodControllerConfigAccessor::s_configTitle("ROD configuration");


class BocConfigAccessor : public RodConfigAccessorBase
{
public:

  const PixA::ConfigHandle configHandle(PixA::IScanDataProvider &data_provider, const std::string &object_name) const {
    std::string rod_name = rodName(data_provider, object_name);
    return data_provider.bocConfig(rod_name);
  }

  std::string title(PixA::IScanDataProvider &data_provider, const std::string &object_name) const {
    return  rodTitle(s_configTitle, data_provider, object_name) ;
  }

  const std::string &configName() const { return s_configTitle; }

private:
  static const std::string s_configTitle;
};

const std::string BocConfigAccessor::s_configTitle("BOC configuration");


class TimControllerConfigAccessor : public RodConfigAccessorBase
{
public:

  const PixA::ConfigHandle configHandle(PixA::IScanDataProvider &data_provider, const std::string &object_name) const {
    std::string tim_name = timName(data_provider, object_name);
    return data_provider.timConfig(tim_name);
  }

  std::string title(PixA::IScanDataProvider &data_provider, const std::string &object_name) const {
    return  timTitle(s_configTitle, data_provider, object_name) ;
  }

  const std::string &configName() const { return s_configTitle; }

protected:

  std::string timTitle(const std::string &prefix, PixA::IScanDataProvider &data_provider, const std::string &object_name) const {
    std::string tim_name = timName(data_provider, object_name);
    if (tim_name.empty()) {
      tim_name = "<unknown>";
    }
    return prefix + " : " + tim_name + serialNumberTrailer(data_provider);
  }

  std::string timName(PixA::IScanDataProvider &data_provider, const std::string &object_name) const {

    if (object_name == m_lastObjectName) {
      return m_lastTimName;
    }
    m_lastObjectName = object_name;

    PixA::ConnectivityRef conn( data_provider.connectivity() );
    if (conn) {
      const PixLib::ModuleConnectivity *module_conn = conn.findModule( object_name );
      const PixLib::RodBocConnectivity *rod_conn = NULL;
      if (module_conn) {
	rod_conn = PixA::connectedRod( module_conn );
      }
      else {
	rod_conn = conn.findRod( object_name );
      }
      if (rod_conn) {
	const PixLib::RodCrateConnectivity *crate_conn = PixA::crate(rod_conn);
	if (crate_conn) {
	  const PixLib::TimConnectivity *tim_conn = const_cast<PixLib::RodCrateConnectivity *>(crate_conn)->tim();
	  if (tim_conn) {
	    m_lastTimName = const_cast<PixLib::TimConnectivity *>(tim_conn)->name();
	    return m_lastTimName;
	  }
	}
      }
      else {
	// assume the given object name is the name of the tim
	m_lastTimName = object_name;
	return m_lastTimName;
      }
    }
    m_lastTimName.clear();
    return m_lastTimName;
  }

private:
  mutable std::string m_lastObjectName;
  mutable std::string m_lastTimName;

  static const std::string s_configTitle;
};

const std::string TimControllerConfigAccessor::s_configTitle("TIM configuration");

class ConfigVisualiser : public PixA::ISimpleVisualiser 
{
public:

  ConfigVisualiser(QWidget *top_win, IConfigAccessor &config_accessor)
    : m_topWin(top_win), /*m_dialog(NULL), */
      m_configAccessor(&config_accessor)
  { assert(m_configAccessor); }

  ~ConfigVisualiser() {
    //    delete m_dialog;
  }
  /** Get the name of the visualiser.
   */
  const std::string &name() const { return m_configAccessor->configName(); }

  /** Returns true, if the visualiser can optionally display only one front-end.
   */
  bool canVisualiseOneFrontEnd() const {return false;}

  /** Fill the histogram info structure.
   * The structure contains the index ranges for each dimension.
   * @sa HistoInfo_t
   */
  void fillIndexArray(PixA::IScanDataProvider&, const std::vector<std::string >&, HistoInfo_t &info) const {
    info.reset();
    info.setMinHistoIndex(0);
    info.setMaxHistoIndex(1);
  }

  /** Perform the chosen visualisation for the given front_end and the given index set.
   * @param option the visualisation option.
   * @param index the index set to specify the histogram in the multi dimensional histogram collection.
   * @param front_end the number of the front_end (0-15) which should be visualised or -1 for all front-ends
   * The front-end parameter will be ignored if canVisualiseOneFrontEnd returns false.
   */
  void visualise(PixA::IScanDataProvider &data_provider, const std::vector<std::string > &module_names, int, const PixA::Index_t & index, int /* front_end */) {
    try {
      PixA::Index_t mindex;
      if(index.size()>0) mindex = index;
      else{
        for(unsigned i=0;i<module_names.size();i++) mindex.push_back(i);
      }

      for(PixA::Index_t::iterator mit=mindex.begin(); mit!=mindex.end(); mit++){
        if((*mit)<module_names.size()){
          QDialog *a_dialog=NULL;
          
          PixA::ConfigHandle a_config_handle = m_configAccessor->configHandle(data_provider, module_names[(*mit)]);
          std::string a_title = m_configAccessor->title(data_provider, module_names[(*mit)]);
          
          if (a_config_handle) {
            
            std::cout << "DEBUG [ConfigVisualiser] prepare configuration for " <<  module_names[(*mit)] << "." << std::endl;
            PixA::ConfigRef config_ref ( a_config_handle.ref() );
            const PixLib::Config *a_config = NULL;
            {
              std::stringstream message;
              message << " Get " <<  m_configAccessor->configName()  << " for " << module_names[(*mit)] << " .... ";
              std::shared_ptr<PixA::IBusyLoop> busy_loop(PixA::BusyLoopFactory::busyLoop(message.str()));
              a_config = &(a_config_handle.ref().config());
            }
//             std::string dialog_name = m_configAccessor->configName();
            a_title+=" - ";
            a_title+=module_names[(*mit)];
            a_dialog=new configViewer(a_title, *a_config, a_config_handle, m_topWin);//, dialog_name.c_str(),false);
          }
          if (a_dialog) {
            a_dialog->show();
          }
        }
      }
    }
    catch(PixA::BaseException &err) {
      std::cout << "ERROR [ConfigVisualiser::visualise] Caught base exception : " << err.getDescriptor() << std::endl;
    }
    catch( ... ) {
      std::cout << "ERROR [ConfigVisualiser::visualise] Caught unknown exception." << std::endl;
    }
  }

  void closeDialog() {
    //    delete m_dialog;
    //    m_dialog=NULL;
  }

private:
  QWidget              *m_topWin;
  IConfigAccessor      *m_configAccessor;
  //  configViewer         *m_dialog;
};



/** Auxillary class to create the summary histo visualiser.
 */
class ConfigVisualisationKit : public PixA::IVisualisationKit
{
 public:
  ConfigVisualisationKit(QWidget *top_win, IConfigAccessor *config_accessor, unsigned int a_priority)
  : m_priority(a_priority),
    m_topWin(top_win),
    m_configAccessor(config_accessor)
  {}

  unsigned int priority() const { return m_priority; }

  /** Create a visualiser for the given inputs or return NULL.
   */
  PixA::IVisualiser *visualiser(const std::string &/*histo_name*/,
				const std::vector<std::string> &/*folder_hierarchy*/) const {
    return new ConfigVisualiser(m_topWin, *(m_configAccessor.get()));
  }

  static PixA::IVisualisationKit *createKit(QWidget *top_win, IConfigAccessor *config_accessor, unsigned int a_priority) {
    return new ConfigVisualisationKit(top_win, config_accessor, a_priority);
  }


 private:
  unsigned int m_priority;
  QWidget *m_topWin;
  std::shared_ptr<IConfigAccessor> m_configAccessor;
};


#include <Visualiser/VisualisationKitSubSet_t.h>

bool hasKit(const std::string &name, unsigned int min_priority)
{
    std::vector<std::string>       empty_folder_list;
    PixA::VisualisationKitSubSet_t config_display_kits;
    PixA::VisualisationFactory::getKits(name, empty_folder_list, config_display_kits);

    return (!config_display_kits.empty() && config_display_kits.aboveThreshold(min_priority));
}



void registerConfigVisualiser(QWidget *top_win, unsigned int a_priority, bool only_if_lower_priorotiy_kits) {
  if (a_priority==0) only_if_lower_priorotiy_kits=true;
  if ( !only_if_lower_priorotiy_kits || !hasKit("Scan_cfg", a_priority-1)) {
    PixA::VisualisationFactory::registerKit("^/Scan_cfg$",        ConfigVisualisationKit::createKit(top_win, new ScanConfigAccessor,          a_priority) );
  }
  if ( !only_if_lower_priorotiy_kits || !hasKit("Module_cfg", a_priority-1)) {
    PixA::VisualisationFactory::registerKit("^/Module_cfg$",      ConfigVisualisationKit::createKit(top_win, new ModuleConfigAccessor,        a_priority) );
  }
  if ( !only_if_lower_priorotiy_kits || !hasKit("ModuleGroup_cfg", a_priority-1)) {
    PixA::VisualisationFactory::registerKit("^/ModuleGroup_cfg$", ConfigVisualisationKit::createKit(top_win, new ModuleGroupConfigAccessor,   a_priority) );
  }
  if ( !only_if_lower_priorotiy_kits || !hasKit("Boc_cfg", a_priority-1)) {
    PixA::VisualisationFactory::registerKit("^/Boc_cfg$",         ConfigVisualisationKit::createKit(top_win, new BocConfigAccessor,           a_priority) );
  }
  if ( !only_if_lower_priorotiy_kits || !hasKit("Rod_cfg", a_priority-1)) {
    PixA::VisualisationFactory::registerKit("^/Rod_cfg$",         ConfigVisualisationKit::createKit(top_win, new RodControllerConfigAccessor, a_priority) );
  }
  if ( !only_if_lower_priorotiy_kits || !hasKit("Tim_cfg", a_priority-1)) {
    PixA::VisualisationFactory::registerKit("^/Tim_cfg$",         ConfigVisualisationKit::createKit(top_win, new TimControllerConfigAccessor, a_priority) );
  }
}
