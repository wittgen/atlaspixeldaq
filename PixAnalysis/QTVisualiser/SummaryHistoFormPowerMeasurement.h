#ifndef _SummaryHistoFormPowerMeasurement_h_
#define _SummaryHistoFormPowerMeasurement_h_

#include "SummaryHistoFormBase.h"
#include <memory>
#include <string>

#include <Visualiser/IScanDataProvider.h>
//Added by qt3to4:
#include <QCloseEvent>

class QCloseEvent;

class SummaryHistoVisualiser;

class SummaryHistoFormPowerMeasurement : public QDialog, public Ui_SummaryHistoFormBase
{
public:

  SummaryHistoFormPowerMeasurement(SummaryHistoVisualiser *visualiser, const PixA::Histo *histo, QWidget* parent = 0, const char* name = 0, bool modal = FALSE, Qt::WFlags fl = 0 );
  //  ~SummaryHistoForm();

private:
  void closeEvent ( QCloseEvent * e );
  void LookForNoisyChannels(const PixA::Histo *histo);

  bool FoundInlink(const PixA::Histo *histo, unsigned int inlink);
  std::string FormatEntry(const PixA::Histo *histo, unsigned int inlink, unsigned int colum);

  SummaryHistoVisualiser *m_visualiser;
  std::list<unsigned int> noisyOutlinks;
};

#endif
