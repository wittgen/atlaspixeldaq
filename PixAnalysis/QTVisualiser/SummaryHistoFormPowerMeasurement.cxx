#include "SummaryHistoFormPowerMeasurement.h"
#include "SummaryHistoVisualiser.h"
#include <Histo/Histo.h>

#include <sstream>
#include <iomanip>
#include <q3table.h>
#include <qsplitter.h>
#include <qlabel.h>
//Added by qt3to4:
#include <QCloseEvent>

#ifdef _PIXA_HISTO_IS_ROOT_TH1
#  include <TH1.h>
#endif
#include <DataContainer/GenericHistogramAccess.h>

SummaryHistoFormPowerMeasurement::SummaryHistoFormPowerMeasurement(SummaryHistoVisualiser *visualiser, const PixA::Histo *histo, QWidget* parent, const char* name, bool modal, Qt::WFlags fl  )
 // : SummaryHistoFormBase(parent,name,modal,fl) 
: QDialog(parent),
 m_visualiser(visualiser)

{
setupUi(this);
  if (!histo || !PixA::is2D(histo) || PixA::nColumns(histo)<32 || PixA::nRows(histo)<32) {
    m_moduleVarTable->hide();
    m_pp0VarTable->hide();
    m_globalVarTable->hide();
    return;
  }

  LookForNoisyChannels(histo);

  ////////////////////
  //m_moduleVarTable is used for power measurement
  //set horizontal labels
  m_title->setText(name);
  m_moduleVarTable->setNumCols(7);
  m_moduleVarTable->horizontalHeader()->setLabel(0,"Inlink");
  m_moduleVarTable->horizontalHeader()->setLabel(1, "V_iset");
  m_moduleVarTable->horizontalHeader()->setLabel(2, "I_iset");
  m_moduleVarTable->horizontalHeader()->setLabel(3, "V_vdc");
  m_moduleVarTable->horizontalHeader()->setLabel(4, "I_vdc");
  m_moduleVarTable->horizontalHeader()->setLabel(5, "V_pin");
  m_moduleVarTable->horizontalHeader()->setLabel(6, "I_pin");
  //set verticcal labels
  m_moduleVarTable->setNumRows(0);
  unsigned int row(0);
  for(int inlink=0; inlink<32; inlink++) {
    if( FoundInlink(histo, inlink) ) {
      row = m_moduleVarTable->numRows();

      m_moduleVarTable->insertRows( row, 1);
      m_moduleVarTable->verticalHeader()->setLabel(row, "" );
      std::stringstream temp;
      temp << inlink;

     m_moduleVarTable->setText( row, 0, QString::fromStdString(temp.str()) );
       m_moduleVarTable->setText(row, 1,QString::fromStdString(FormatEntry(histo,inlink,48+inlink*7)) ); //V_iset
      m_moduleVarTable->setText( row, 2, QString::fromStdString(FormatEntry(histo,inlink,49+inlink*7)) ); //I_iset
      m_moduleVarTable->setText( row, 3, QString::fromStdString(FormatEntry(histo,inlink,46+inlink*7) )); //V_vdc
      m_moduleVarTable->setText( row, 4, QString::fromStdString(FormatEntry(histo,inlink,47+inlink*7) )); //I_vdc
      m_moduleVarTable->setText( row, 5, QString::fromStdString(FormatEntry(histo,inlink,50+inlink*7)) ); //V_pin
      m_moduleVarTable->setText( row, 6, QString::fromStdString(FormatEntry(histo,inlink,51+inlink*7) )); //I_pin 
    }
  }
  //m_pp0VarTable not used
  delete m_pp0VarTable;
}


void SummaryHistoFormPowerMeasurement::closeEvent ( QCloseEvent * ) {
  if (m_visualiser) {
    m_visualiser->closeDialog();
  }
}

bool SummaryHistoFormPowerMeasurement::FoundInlink(const PixA::Histo *histo, unsigned int inlink) {
  if ( PixA::nRows(histo)<33 || PixA::nColumns(histo)<=inlink ) return false;

  for(unsigned int colum=0; colum<32; colum++) {
    if(std::find(noisyOutlinks.begin(),noisyOutlinks.end(),colum)!=noisyOutlinks.end()) continue; //if this outlink is noisy discard it
    if(   PixA::binContent(histo,PixA::startColumn(histo)+inlink,PixA::startRow(histo)+colum)
	/ PixA::binContent(histo,PixA::startColumn(histo)+inlink,PixA::startRow(histo)+32) > 0.3 ) return true;
  }
  return false;
}

std::string SummaryHistoFormPowerMeasurement::FormatEntry(const PixA::Histo *histo, unsigned int inlink, unsigned int column) {
  assert( inlink < PixA::nColumns(histo) && column < PixA::nRows(histo) );
  std::stringstream temp;
  // the histograms are organised : x == column, y==row
  // invert the sense of rows and columns
  temp << PixA::binContent(histo,PixA::startColumn(histo)+inlink,PixA::startRow(histo)+column);
  return temp.str();
}

void SummaryHistoFormPowerMeasurement::LookForNoisyChannels(const PixA::Histo *histo) {

  assert( PixA::nColumns(histo) >= 46 && PixA::nRows(histo) >=33  );

  for(unsigned int histo_colum=0; histo_colum<32 ; histo_colum++) {
    unsigned int counts(0);
    for(unsigned int histo_row=0; histo_row < 46; histo_row++) {
      double res =   PixA::binContent(histo,PixA::startColumn(histo)+histo_row,PixA::startRow(histo)+histo_colum)
	           / PixA::binContent(histo,PixA::startColumn(histo)+histo_row,PixA::startRow(histo)+32);
      if (res>0.3) counts++;
    }
    if(counts>=3) noisyOutlinks.push_back(histo_colum);
  }
}
