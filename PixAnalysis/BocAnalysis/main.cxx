#include <boc2root.h>
#include <iostream>

void help()
{
    std::cout << "\nUsage: ./boc2root [options] <datafile> <dir>"<<endl;
    std::cout << " <datafile>         Input RootDB file from STcontrol"<<endl;
    std::cout << " <dir>              Output directory"<<endl;
    std::cout << " By default it runs over all scans. You can select specific scans with the options below."<<endl;
    std::cout << "Options:"<<endl;
    std::cout << " -h, --help         This help"<<endl;
    std::cout << " -f, --fast         Run fast scans"<<endl;
    std::cout << " -s, --slow         Run slow scans"<<endl;
    std::cout << " -2, --2Dslow       Run 2D slow scans"<<endl;
    std::cout << " -t, --thr          Run threshold scans"<<endl;
    std::cout << " -v, --validation   Run validation scans"<<endl;
    std::cout << " -p, --power        Run power scans\n"<<endl;
}


int main(int argc, char **argv) {

  /// First one is input file, the second is output path.
  std::vector<std::string> file_paths;
  /// Run only specific kinds of scan
  bool only[10] = {0,0,0,0,0,0,0,0,0,0};
  bool anything_set = false;
  for (int iarg = 1; iarg < argc; ++iarg) {
    std::string arg = argv[iarg];
    if (arg == "--fast" || arg == "-f") {
      only[0] = true;
      anything_set = true;
      cout<<" Will display fast scans"<<endl;
    } else if (arg == "--slow" || arg == "-s") {
      only[2] = true;
      anything_set = true;
      cout<<" Will display slow scans"<<endl;
    } else if (arg == "--2Dslow" || arg == "-2") {
      only[1] = true;
      only[5] = true;
      anything_set = true;
      cout<<" Will display 2D slow scans"<<endl;
    } else if (arg == "--thr" || arg == "-t") {
      only[3] = true;
      anything_set = true;
      cout<<" Will display threshold scans"<<endl;
    } else if (arg == "--validation" || arg == "-v") {
      only[4] = true;
      anything_set = true;
      cout<<" Will display validation scans"<<endl;
    } else if (arg == "--power" || arg == "-p") {
      only[6] = true;
      anything_set = true;
      cout<<" Will display power scans"<<endl;
    } else if (arg == "--help" || arg == "-h") {
      help();
      return 0;
    } else {
      file_paths.push_back(arg);
    }
  }

  if (file_paths.size() != 2) {
    help();
    return -1;
  }

  string filelist_name = file_paths[0]; 
  string dirname = file_paths[1];

  if (!anything_set)
    for (int i=0; i<10; ++i) only[i] = true ;

  // Get all the data taken
  Boc2root data(filelist_name.c_str(), dirname.c_str(),only);
  


  return 0;
  
}


