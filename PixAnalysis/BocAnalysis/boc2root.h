/********************************************************************
                  newboc2root - take the results of a boc scan and 
                             dumps the DIFF_2 histos as TH2F in a root file
                     -----------------------
                         MD - 14.11.2006
    begin                : Tue, 3 Jan 2006
    stolen from          : Jens Weingarten
    email                : jens.weingarten@cern.ch

********************************************************************/

#ifndef BOC2ROOT_H
#define BOC2ROOT_H

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <list>

// #include <fstream.h>            //WP

#include <ConnectivityManager.h>
#include <ConfObjUtil.h>
#include <ConfGrpRefUtil.h>
#include <DbManager.h>

#include "PixController/PixScan.h"
#include "Histo/Histo.h"
#include "PixConfDBInterface/PixConfDBInterface.h"
#include "PixConfDBInterface/RootDB.h"
#include <RootDb/RootDb.h>
#include <Config/Config.h>
#include <Config/ConfObj.h>
#include "DataContainer/PixDBData.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLine.h"
#include "DataContainer/ScanResultStreamer.h"
#include "DataContainer/IScanResultListener.h" 
#include "DataContainer/PixDbDataContainer.h" 
#include "DataContainer/HistoRef.h"
#include "DataContainer/HistoInfo_t.h"



struct Module_PP0 {
  std::string slot;
  int position;
  std::string module;
  std::string stave;
  std::string connName;
  int outlink;
  int inlink;
  std::string TX;
  std::string RX;
  std::string RXthreshold;
  std::string RXdelay;
  std::string threshold;
  std::string delay;
} ;

struct ScanSpecs{
  
  int Bins;
  float min, max, minpos, maxpos; 
  float BinWidth;
  bool pacman, LoopUsed;
  string ParamLoop;
};

struct Scan {
  std::string slot;
  std::string module;
  std::string stave;
  int    xbins;
  int    ybins;
  int    ymin;
  int    ymax;
  int    yopt;
  double thr_value_yopt;
  int    delay;
  int    ysumopt;
  double thr_value_ysumopt;
  int    delaysum ;
  int    A;
  double single_square;
  double AA;
  int    max_window;
  double max_window_size;
  double viset;
  double topto;
} ;

//char htmlname[200];



class Boc2root{         // : public PixDBData {

 public:
  //  Boc2root(const char *path=0);
  Boc2root(const char *fname, const char *dirname, const bool (&only)[10] );
//  ~Boc2root();

//  bool write_to_file(const char*config_file, std::string RX, std::string RXthreshold, int thrvalue, std::string RXdelay, int delaysum);
  bool write_to_file(const char*config_file, std::string opto_name, int best_viset);
  bool FindData(const char *fname, const char* type, const char *dirname, const bool (&only)[10]);

 

  vector<Scan> Get_Scan_vector();
  vector<string> Get_Opto_Names();
  
  //! Accessors
  void set_nChannel(int n) {m_nChannel = n;};
  int get_nChannel() const {return m_nChannel;};

  float Ne_mean, Ne_var, TotAvg_mean, TotAvg_var;
  PixDBData *tempObj;
  PixDBData **Obj_array;
  TH2F *rootHisto;
  PixLib::Histo **Histo_array;
  int m_foundObjects;
  PixConfDBInterface *m_RootDB;
  PixConfDBInterface *m_RootDB_cfg;
  PixDbInterface *myDb;
  TH2F* hist[1000];
  int m_Nmods;
  TFile *file_out;

  vector<Scan> v_scan;
  char test[200];

  TH2F *rootHisto2[1000] ;
 
 private:
  ScanSpecs Dim[3];
  string outSpeed;
  int m_nChannel;
  PixLib::Histo** histArray;

  int m_NOISE_MAX;
  int m_NOISE_HVOFF_MAX;
  int m_THRESHOLD_MAX;
  int m_THRESHOLD_MIN;
  int m_THRESHOLD_DISPERSION_MAX;
  int m_THRESHOLD_DISPERSION_MIN;
  int m_NOISE_DIFF_MIN;
  int m_CHI2_MAX;
  int m_GOOD_FIT_MIN;
  int m_GOOD_FIT_HVOFF_MIN;

};



#endif //BOC2ROOT_H
