/********************************************************************
                  newboc2root - take the results of a boc scan and 
                             dumps the DIFF_2 histos as TH2F in a root file
                     -----------------------
                         MD - 14.11.2006
    begin                : Tue, 3 Jan 2006
    stolen from          : Jens Weingarten
    email                : jens.weingarten@cern.ch

********************************************************************/

#ifndef SCANRESULTSPY_H
#define SCANRESULTSPY_H

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <list>

// #include <fstream.h>            //WP

#include <ConnectivityManager.h>
#include <ConfObjUtil.h>
#include <ConfGrpRefUtil.h>
#include <DbManager.h>

#include "PixController/PixScan.h"
#include "Histo/Histo.h"
#include <RootDb/RootDb.h>
#include <Config/Config.h>
#include <Config/ConfObj.h>
#include "DataContainer/PixDBData.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TColor.h"
#include "TLatex.h"
#include "TLine.h"
#include "DataContainer/ScanResultStreamer.h"
#include "DataContainer/IScanResultListener.h" 
#include "DataContainer/PixDbDataContainer.h" 
#include "DataContainer/HistoRef.h"
#include "DataContainer/HistoInfo_t.h"

class ScanResultSpy : public PixA::IScanResultListener {

  //  typedef std::map<std::string, PixA::PixDbDataContainer *> DataContainerList_t;
  //  typedef std::map<std::string, int> PixScanHistoTypeList_t;
  typedef std::map<std::string, int> HistoNameToType_t;
  vector <Histo> v_histo;
  vector <Histo> v_historef;
  vector <Histo> v_historaw;
  vector <string> v_histname;
  vector <string> v_histnameraw;
  vector <string> v_connname;
  vector <string> v_connnameraw;
  vector <string> v_modulename;
  vector <string> v_modulenameraw;
  vector <int> v_inLink;
  vector <int> v_outLink;
  vector <int> v_scansteps0;
  vector <int> v_scansteps1;
  vector <int> v_scansteps2;
  vector <int> v_scanmin0;
  vector <int> v_scanmin1;
  vector <int> v_scanmin2;
  vector <int> v_scanmax0;
  vector <int> v_scanmax1;
  vector <int> v_scanmax2;
  vector <int> v_nhist;
  vector <int> v_nlink;
  vector <int> v_linkmin;
  vector <int> v_linkmax;
  vector <int> v_scanlevel;
  vector <string> v_scanpar0;
  vector <string> v_scanpar1;
  vector <string> v_scanpar2;
  vector <string> v_speed;
  
  string myfold;
  bool blayer;
  //  vector <string> webname;

  string s_htmlname;
  FILE* fHTML;
  FILE* fHTMLNEW;
  TFile* ff;

  std::string dirn;
  std::string fname;
  std::string idname;
  std::string s_scanname;
  std::string pp0name;
  std::string rodname;
 public:
  ScanResultSpy();
  void newFile(const std::string&) {}
  void newLabel(const std::string &name, const PixA::ConfigHandle &label_config);
  void newPixModuleGroup(const std::string &name);
  void finishPixModuleGroup();
  void newPp0(const std::string &name);
  void finishPp0();
  void newFolder(const std::vector<std::string> &folder_hierarchy);

  /** Does intentionally nothing.
   */
  void newScanInfo(const PixA::ConfigHandle &) {}

  /** Does intentionally nothing. Really?
   */
  void newPixScan(const PixA::ConfigHandle &pix_scan_config) {};


  void newPixScanHisto(const std::string &histo_name,
                       const PixA::ScanConfigRef &scan_config,
		       const PixA::HistoHandle &histo_handle);
  void newHisto(const std::string &name, const PixA::HistoHandle &histo_handle, const std::string &rod_name, const PixA::ConfigHandle &scan_config);
  void finish();

  /** Does intentionally nothing.
   */
  void reportProblem(const std::string &) { }
  void setName(std::string scanname, std::string finname,const char *dirname);
  int FindBestViset(const int opt);
  int FindBestThreshold(int iviset,int imodule,int ilink);
  int FindBestDelay(int iviset,int imodule,int ilink,int thropt);
  bool FindSTO();

  //  static std::map< std::string, int > ScanResultSpy::s_histoNameToType;
  //  std::map< std::string, int > ScanResultSpy::s_histoNameToType;
  //  static PixScanHistoTypeList_t s_pixScanHistoTypeList;
  static HistoNameToType_t s_histoNameToType;
  
  //  void set_name(ofstream *fhtml) {f_htmlname = (*fhtml);};
 
 private:
  //  static void initHistoNameTypeList(); 
  //  static int getHistoType(const std::string &histo_name); 
  //   static void initHistoNameTypeList(){ */
/*     s_histoNameToType.clear(); */
/*     std::auto_ptr<PixLib::PixScan> ps(new PixLib::PixScan()); */
/*     s_histoNameToType = ps->getHistoTypes(); */
/*   }; */
/*   static int getHistoType(const std::string &histo_name){ */
/*     if (s_histoNameToType.empty()) initHistoNameTypeList(); */
/*     std::map< std::string, int >::const_iterator iter = s_histoNameToType.find(histo_name); */
/*     if (iter != s_histoNameToType.end()) { */
/*       return iter->second; */
/*     } */
/*     return -1; */
/*   }; */
  //  static PixScanHistoTypeList_t s_pixScanHistoTypeList;

  //  static std::map< std::string, int > ScanResultSpy::s_histoNameToType;
};

#endif //NEWBOC2ROOT_H
