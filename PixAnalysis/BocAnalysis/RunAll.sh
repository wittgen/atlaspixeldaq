#!/usr/bin/env bash
# Script to run boc2root on multiple files
# Useful for making summary pages
# Author: Yann Coadou (Yann.Coadou@cern.ch)
# Created: 7 May 2007
#######################################################################

usage(){
    echo "Syntax: $0 [options] <input dir> <output dir>"
    echo "  <input dir>:  directory containing RootDB files from STcontrol. Typically:"
    echo "                 /home/pixeldaq/Data/Data-C78-ISP"
    echo "                Could also be comma-separated list of files:"
    echo "                 file1.root,file2.root"
    echo "  <output dir>: directory for boc2root output html pages. Typically:"
    echo "                 /tdaqfiles/PixelPackage/Phase2/<some dir>"
    echo " options:"
    echo "  -h, --help         This help"
    echo "  -d, --debug        Show parameters"
    echo "  -n, --no-summary   Do not produce the html summary pages"
    exit 1
}

debug(){
    echo "outdir    = $dir"
    echo "indir     = $indir"
    echo "summary   = $summary"
    exit 1
}


# Read command line options
args=`getopt -l "help,debug,no-summary" "h,d,n" "$@"`
[ $? != 0 ] && usage

debug=0
summary=1
set -- $args
while [ ! -z "$1" ]
do
  #echo $1
  case "$1" in
    -h | --help) usage;;
    -d | --debug) debug=1;;
    -n | --no-summary) summary=0;;
    #--input) shift; Input=`echo "$1" | tr -d "'"`;;
    --) shift; # get all arguments without name
	[ $# -ne 2 ] && usage
	indir=`echo "$1" | tr -d "'"`
	dir=`echo "$2" | tr -d "'"`
        break;;
    *) echo "Unknown option $1"; usage; break;;
  esac
  shift
done

[ $debug -eq 1 ] && debug


mkdir -p $dir

if [ ! -w $dir ]; then
    echo Error: No write access to $dir
    echo " First fix write permissions for $USER in this directory."
    exit 1
fi

# Directory or file list input
if [ $indir != ${indir%.root} ]; then
    filelist=`echo $indir | tr , ' '`
else
    filelist="$indir/*root"
fi

# Compile the latest version:
make boc2root
if [ $? -ne 0 ]; then
    echo "Error: boc2root could not compile."
    exit 1
fi

ifile=0
nproblems=0
nfiles=`ls $filelist |grep -v File_|wc -l`
for file in `ls $filelist |grep -v File_`
do
    ifile=$((ifile+1))
    LOG=`basename $file`
    echo -n "$ifile/${nfiles##* } ${LOG} "
    ./boc2root $file $dir >& $dir/${LOG/.root/.log}
    error=$?
    [ `tail -1 $dir/${LOG/.root/.log} | grep END > /dev/null; echo $?` -ne 0 ] && error="222"
    echo $error
    [ $error -ne 0 ] && nproblems=$((nproblems+1))
done

echo
echo Processed $nfiles files. $nproblems files had serious problems.


# Run the other script to only produce summary pages
echo
if [ $summary -eq 1 ]; then
    echo Making summary html pages
    ./pp0_summary.sh $dir
else
    echo Not producing summary html pages
fi

exit 0
