/********************************************************************
                  boc2root - take the results of a boc scan and 
                             dumps the DIFF_2 histos as TH2F in a root file
                     -----------------------
                         MD - 14.11.2006
    begin                : Tue, 3 Jan 2006
    stolen from          : Jens Weingarten
    analysis from        : Daniel Dobos
    email                : jens.weingarten@cern.ch

********************************************************************/

#include "ScanResultSpy.h"
#include <iomanip>
#include <unistd.h>

//ofstream f_html;

string NoSpaces(string s)
{
  while(s.find(" ") != string::npos)
    {
      s.replace(s.find(" "), 1, "_");
    }
  return s;
}

string NoSlashes(string s)
{
  while(s.find("/") != string::npos)
    {
      s.replace(s.find("/"), 1, "_");
    }
  return s;
}

void ColourScale(int type){
  Int_t i;
  Float_t colfrac;
  TColor *color;
  int    palette[100];// 2D-map colour palette black-red-yellow
  gStyle->SetCanvasColor(10);
  gStyle->SetOptStat(0);
  switch(type){
  case 0:
    // black-red-yellow
    // start with black-to-red
    for(i=0;i<60;i++){
      colfrac = (Float_t)i/60;
      if(! gROOT->GetColor(201+i)){
        color = new TColor (201+i,colfrac,0,0,"");
      }else{
        color = gROOT->GetColor(201+i);
        color->SetRGB(colfrac,0,0);
      }
      palette[i]=201+i;
    }
    // red-to-yellow now
    for(i=0;i<40;i++){
      colfrac = (Float_t)i/40;
      if(! gROOT->GetColor(261+i)){
        color = new TColor (261+i,1,colfrac,0,"");
      }else{
        color = gROOT->GetColor(261+i);
        color->SetRGB(1,colfrac,0);
      }
      palette[i+60]=261+i;
    }
    gStyle->SetPalette(100,palette); 
    break;
  case 1:
    // black-red, *no* yellow
    for(i=0;i<100;i++){
      colfrac = (Float_t)i/100;
      if(! gROOT->GetColor(201+i)){
        color = new TColor (201+i,colfrac,0,0,"");
      }else{
        color = gROOT->GetColor(201+i);
        color->SetRGB(colfrac,0,0);
      }
      palette[i]=201+i;
    }
    gStyle->SetPalette(100,palette); 
    break;
  case 2:
    // rainbow
    gStyle->SetPalette(1); 
    break;
  case 3:
    // black-blue-purple-red-orange-yellow-white (TurboDAQ style)
    for(i=0;i<20;i++){ // black to blue
      colfrac = (Float_t)i/20;
      if(! gROOT->GetColor(201+i)){
        color = new TColor (201+i,0,0,colfrac,"");
      }else{
        color = gROOT->GetColor(201+i);
        color->SetRGB(0,0,colfrac);
      }
      palette[i]=201+i;
    }
    for(i=0;i<20;i++){ // blue to purple
      colfrac = 0.8*(Float_t)i/20;
      if(! gROOT->GetColor(221+i)){
        color = new TColor (221+i,colfrac,0,1,"");
      }else{
        color = gROOT->GetColor(221+i);
        color->SetRGB(colfrac,0,1);
      }
      palette[20+i]=221+i;
    }
    for(i=0;i<20;i++){ // purple to red
      colfrac = (Float_t)i/20;
      if(! gROOT->GetColor(241+i)){
        color = new TColor (241+i,0.8+0.2*colfrac,0,1-colfrac,"");
      }else{
        color = gROOT->GetColor(241+i);
        color->SetRGB(0.8+0.2*colfrac,0,1-colfrac);
      }
      palette[40+i]=241+i;
    }
    for(i=0;i<25;i++){ // red to orange to yellow
      colfrac = (Float_t)i/25;
      if(! gROOT->GetColor(261+i)){
        color = new TColor (261+i,1,colfrac,0,"");
      }else{
        color = gROOT->GetColor(261+i);
        color->SetRGB(1,colfrac,0);
      }
      palette[60+i]=261+i;
    }
    for(i=0;i<15;i++){ // yellow to white
      colfrac = (Float_t)i/15;
      if(! gROOT->GetColor(286+i)){
        color = new TColor (286+i,1,1,colfrac,"");
      }else{
        color = gROOT->GetColor(286+i);
        color->SetRGB(1,1,colfrac);
      }
      palette[80+i]=286+i;
    }
    gStyle->SetPalette(100,palette); 
    break;
  case 4:
    // black-white
    for(i=0;i<90;i++){
      colfrac = (Float_t)i/100;
      if(! gROOT->GetColor(201+i)){
        color = new TColor (201+i,colfrac,colfrac,colfrac,"");
      }else{
        color = gROOT->GetColor(201+i);
        color->SetRGB(colfrac,colfrac,colfrac);
      }
      palette[i]=201+i;
    }
    gStyle->SetPalette(90,palette); 
    break;
  case 5:
    // red-yellow, *no* yellow
    for(i=0;i<100;i++){
      colfrac = (Float_t)i/100;
      if(! gROOT->GetColor(201+i)){
        color = new TColor (201+i,1,colfrac,0,"");
      }else{
        color = gROOT->GetColor(201+i);
        color->SetRGB(1,colfrac,0);
      }
      palette[i]=201+i;
    }
    gStyle->SetPalette(100,palette); 
    break;
  default:
    break;
  }
}
int ScanResultSpy::FindBestViset(const int opt)
{

  // ---------------------------------------------------
  // opt=0: use maximum error free region
  //    =1: use maximum error free region but make sure that it is not the minimum or maximum
  //    =2: use maximum error free region but make sure that it is not the minimum or maximum
  //        and that adjacent bins are at least 50% as good as the chosen bin
  //    =3: new algorithm from Jens: use maximum delay width
  //    =4: new algorithm from Jens: use maximum delay width but make sure that it is not the minimum or maximum

  int areamax=0;
  int widthmax=0;
  int ibestviset=0;
  int ibestvisetw=0;
  const int nH=v_nhist.at(0);
  std::vector<int> areamin; areamin.resize(nH);
  std::vector<int> widthmin; widthmin.resize(nH);
  //  memset(areamin,99,sizeof(int)*nH);
  //  memset(areamin,99,sizeof(int)*nH);
  
  for (int ih=0;ih<v_nhist.at(0);ih++){
    areamin[ih]=999;
    widthmin[ih]=999;
    for (uint im=0;im<v_modulename.size();im++){
      for (int il=0;il<v_nlink.at(im);il++){
	int area=0;
	int index=(im*v_nhist.at(im)+ih)*v_nlink.at(im)+il;
	int maxwidth=0;
	for(int ithr = 0; ithr<v_histo.at(index).nBin(0); ithr++) {
	  int width=0;
	  for(int idelay = 0; idelay<v_histo.at(index).nBin(1); idelay++) {
	    int val=int((v_histo.at(index))(idelay,ithr));
	    if (val<1) {
	      width++;
	      area++;
	    }
	  }
	  if (width>maxwidth) maxwidth=width;
	}
	if (maxwidth<widthmin[ih]) widthmin[ih]=maxwidth;
	if (area<areamin[ih]) areamin[ih]=area;
      }
    }
    if (areamin[ih]>areamax) {
      ibestviset=ih;
      areamax=areamin[ih];
    }
    if (widthmin[ih]>widthmax) {
      ibestvisetw=ih;
      widthmax=widthmin[ih];
    }
  }

  if (opt==1){
    if (ibestviset==0) ibestviset+=1;
    if (ibestviset==v_nhist.at(0)-1) ibestviset-=1;
  } else if (opt==2){
    if (ibestviset==0) ibestviset+=1;
    if (ibestviset==v_nhist.at(0)-1) ibestviset-=1;
    if (2*areamin[ibestviset-1]<=areamin[ibestviset]) {
      cout << "FindBestViset: (alg 2) danger: close to falling left edge : "<<areamin[ibestviset-1] <<
	" , " << areamin[ibestviset]<<endl;
      cout << "FindBestViset: (alg 2) tune to slightly higher value" <<endl;
      ibestviset+=1;
    }
    if (2*areamin[ibestviset+1]<=areamin[ibestviset]) {
      cout << "FindBestViset: (alg 2) danger: close to falling right edge : "<<areamin[ibestviset+1] <<
	" , " << areamin[ibestviset]<<endl;
      cout << "FindBestViset: (alg 2) tune to slightly lower value"<<endl;
      ibestviset-=1;
    }
  } else if (opt==3){
    ibestviset=ibestvisetw;
  } else if (opt==4){
    ibestviset=ibestvisetw;
    if (ibestviset==0) ibestviset+=1;
    if (ibestviset==v_nhist.at(0)-1) ibestviset-=1;
  }
  

  if (v_nhist.at(0)>1) {
    return ibestviset;
  } else {
    return 0;
  }
}


int ScanResultSpy::FindBestDelay(int iviset,int imodule,int ilink,int thropt)
{

  double delaymaxr=0;
  double delaymaxl=0;
  int delaybestr=0;
  int delaybestl=0;

  int NDelay = v_histo.at(0).nBin(0);

  std::vector<int> DelayVal;  DelayVal.resize(NDelay);
    
  int bw=1;
  if (v_scansteps0.at(0)>1) bw=(v_scanmax0.at(0)-v_scanmin0.at(0))/(v_scansteps0.at(0)-1);
  for (int i=0;i<NDelay;i++) {
    DelayVal[i]=(int)(v_scanmin0.at(0)+i*bw);
  }

  int index=(imodule*v_nhist.at(imodule)+iviset)*v_nlink.at(imodule)+ilink;
  for(int idelay = 0; idelay<NDelay; idelay++) {
    int val=int((v_histo.at(index))(idelay,thropt));
    int neighbr=idelay+1;
    if (idelay==NDelay-1) neighbr=0; 
    int neighbl=idelay-1;
    if (idelay==0) neighbl=NDelay-1; 
    // this is one edge of the error region=> need to make sure the right side neighbour is 0
    if (val>delaymaxr && v_histo.at(index)(neighbr,thropt)<=0.5) {
      delaymaxr=val;
      delaybestr=idelay;          
    } 
    if (val>delaymaxl && v_histo.at(index)(neighbl,thropt)<=0.5) {
      delaymaxl=val;
      delaybestl=idelay;          
    } 
  }
  int delayWidth=DelayVal[delaybestl]-DelayVal[delaybestr];
  if (delayWidth<=0) delayWidth=25+delayWidth;
  int delaybest=DelayVal[delaybestr]+2;
  if (delayWidth<5) {
    delaybest = -1;
  } else if (delayWidth==5) {
    delaybest=delaybestr+2*25/NDelay;
  } else {
    delaybest=DelayVal[delaybestr]+1+(int)(0.2*delayWidth);
  } 
  if (delaybest>=NDelay) delaybest-=NDelay;
  return delaybest;
}

int ScanResultSpy::FindBestThreshold(int iviset,int imodule,int ilink)
{

  int thropt=-1;
  int thrmin=999;
  int thrmax=-1;
  int thrsumopt=-1;
  int nbinwidth=0;
  int xwidthmin=999;
  int xwidthsummin=999;

  int index=(imodule*v_nhist.at(imodule)+iviset)*v_nlink.at(imodule)+ilink;
  for(int ithr = 0; ithr<v_histo.at(index).nBin(1); ithr++) {
    int xwidth=0;
    int xwidthsum=0;
    for(int idelay = 0; idelay<v_histo.at(index).nBin(0); idelay++) {
      double val=(v_histo.at(index))(idelay,ithr);
      if (val>0.5)  xwidth++;
      xwidthsum+=(int)val;
      if (val<1 && ithr<thrmin) thrmin=ithr;
      if (val<1 && ithr>thrmax) thrmax=ithr;
      if (xwidth<xwidthmin) {
	xwidthmin=xwidth; 
	thropt=ithr;
	nbinwidth++; //count the number of bins with the same width
	if (xwidthsum<xwidthsummin) {
	  xwidthsummin=xwidthsum;
	  thrsumopt=ithr;
	}
      }
    }
  }

  if (nbinwidth>1 && thrsumopt>-1 && thrsumopt<v_histo.at(0).nBin(1)) thropt=thrsumopt;
  thropt+=(int) ((float)((thrmax-thropt)-(thropt-thrmin))/2);
  return thropt;
}


ScanResultSpy::ScanResultSpy()
{
  blayer=false;
  cout << "new ScanResultSpy"<<endl;
  return;
}

void ScanResultSpy::setName(std::string scanname, std::string finname,const char *dirname){

  fname = finname;
  idname = finname;
  idname.erase(finname.find_last_of("."),finname.length());
  idname.erase(0,finname.find_last_of("/")+1);
  dirn=string(dirname);
  if (dirn.substr(dirn.length()-1,dirn.length())=="/") {
    dirn.erase(dirn.length()-1,dirn.length());
  }

  s_scanname = scanname;

  return;
}
  
void ScanResultSpy::newLabel(const std::string &name, const PixA::ConfigHandle &label_config) {

  //  v_sumhisto.clear();

  //  webname.clear();
  std::string comment;
  std::string time_stamp;
  unsigned int scan_number=0;
  if (label_config) {
    PixA::ConfigRef label_config_ref = label_config.ref();
    try {
      comment = confVal<std::string>(label_config_ref[""]["Comment"]);
      scan_number = confVal<unsigned int>(label_config_ref[""]["ScanNumber"]);
      time_stamp = confVal<std::string>(label_config_ref[""]["TimeStamp"]);
      
    }
    catch (PixA::ConfigException &err)
      {
        // catch exceptions which are thrown if a field does not exist e.g. ScanNumber
      }
  }
  std::cout << "INFO [ScanResultSpy::newLabel] ";
  if (scan_number>0) {
    std::cout << scan_number;
  }
  std::cout << " : " << name << "\"";
  if (!time_stamp.empty()) {
    std::cout << "  - " << time_stamp;
  }
  if (!comment.empty()) {
    std::cout << "  :: " << comment << "";
  }
  std::cout << std::endl;
}

/** Called whenever the pix module group changes.
 */
void ScanResultSpy::newPixModuleGroup(const std::string &name) {



  rodname=name;
  //  v_sumhisto.clear();
  std::cout << "INFO [ScanResultSpy::newPixModuleGroup] pix module group=\"" << name << "\"" << std::endl;

}

void ScanResultSpy::finishPixModuleGroup() {
  blayer=false;
  std::cout << "INFO [ScanResultSpy::finishPixModuleGroup] done with pix module group." << std::endl;
  
}

/** Called whenever the Pp0 changes.
 * newPp0 will never be called without newPixModuleGroup being called.
 */
void ScanResultSpy::newPp0(const std::string &name) {
  blayer=false;
  ColourScale(0);
  std::cout << "INFO [ScanResultSpy::newPp0] PP0 =\"" << name << "\"" << std::endl;
  std::string rootname=name+"_"+s_scanname+"_"+idname+".root";
  std::string s_rootname = NoSpaces(rootname);
  //  ff = new TFile(s_rootname.c_str(),"RECREATE");

  //  vector <Histo> histovec;
  v_histo.clear();
  v_historef.clear();
  v_modulename.clear();
  v_connname.clear();
  v_histname.clear();
  v_inLink.clear();
  v_outLink.clear();
  v_scansteps0.clear();
  v_scansteps1.clear();
  v_scansteps2.clear();
  v_scanmin0.clear();
  v_scanmin1.clear();
  v_scanmin2.clear();
  v_scanmax0.clear();
  v_scanmax1.clear();
  v_scanmax2.clear();
  v_scanpar0.clear();
  v_scanpar1.clear();
  v_scanpar2.clear();
  v_scanlevel.clear();
  v_nhist.clear();
  v_nlink.clear();
  //  rownoerr.clear();

  pp0name=name;
  
}

void ScanResultSpy::finishPp0() {
  std::cout << "INFO [ScanResultSpy::finishPp0] PP0 =\"" << pp0name << "\"" << std::endl;

  if (v_histo.size()<=0 || v_nhist.size()<=0) {
    std::cout << "INFO [ScanResultSpy::finishPp0] found no histograms => return" << std::endl;
    return;
  }

  if (v_histo.size()<v_modulename.size()*v_nhist.at(0)) {
    cout << "ScanResultSpy: Problem with Histogram counting: either missing some or mistake => return"<<endl;
    cout << "NHist = "<<v_histo.size() << " NModule = " << v_modulename.size()<< "  Hist/Module = " << v_nhist.at(0)<<endl;
    return;
  } 

//   std::string hname1=v_histname.at(0);
//   std::string hname2=NULL;
//   for (int i = 0 ; i < v_modulename.size(); i++) {
//     cout << i << "  " << v_histname.at(i)<<endl;
//     if (v_histname.at(i)!=hname1) {
//       cout << "two different histogram types!"<<endl;
//       hname2=v_histname.at(i);
//     }
//     if (v_histname.at(i)!=hname1 && v_histname.at(i)!=hname2) {
//       cout << "3 different histogram types! code not ready yet => return"<<endl;
//     }
//   }
  
  //  std::string s_html=dirn+"/"+rodname + "-PP0_"+pp0name+"--"+s_scanname+"-"+hname1+".html";
  char slevel[1]; 
  sprintf(slevel,"%i",v_scanlevel.at(0));
  //  std::string sl=(string) slevel;

  std::string s_html=dirn+"/"+rodname + "-PP0_"+pp0name+"--"+idname+"_"+s_scanname;
  if (myfold != "") s_html += "-"+myfold;
  s_html += ".html";
  std::string s_htmlnew = NoSpaces(s_html);
  fHTMLNEW= fopen(s_htmlnew.c_str(),"wt");

  if (s_scanname.find("BOC_RX_DELAY_SCAN")!= string::npos) {
    cout << "got slow threshold scan "<<s_scanname.c_str()<<endl;
    cout << "axis label: " <<v_scanpar0.at(0).c_str()<<endl;
    if (v_scanpar0.at(0).find("BOC_RX_THR")!=string::npos) cout << "success !!"<<endl;
  }

  if (s_scanname.find("BocSlowThreshold")!= string::npos 
      || s_scanname.find("BOC_THR_DEL_SHORT")!= string::npos
      || s_scanname.find("BOC_RX_THR_SCAN")!= string::npos
      || (s_scanname.find("BOC_RX_DELAY_SCAN")!= string::npos && v_scanpar0.at(0).find("BOC_RX_THR")!=string::npos)
      || myfold.find("BocSlowThreshold")!= string::npos){
      //      || myfold.c_str()=="BocSlowThreshold") {
    cout << "got slow threshold scan "<<s_scanname.c_str()<<endl;
  //  if (s_scanname.substr(0,17)=="BOC_THR_DEL_SHORT" || myfold.c_str()=="BocSlowThreshold") {
    bool sto=FindSTO();
  } 
//   FILE* fHTML2=NULL;
//   if (hname2!=NULL){
//     s_html=dirn+"/"+rodname + "-PP0_"+pp0name+"--"+s_scanname+"-"+hname2+".html";
//     s_htmlnew = NoSpaces(s_html);
//     fHTML2= fopen(s_htmlnew.c_str(),"wt");
//   }
  
  if (fHTMLNEW == 0) {
    cout<<" Error: cannot write into "<<s_htmlnew<<". Check permissions."<<endl;
    exit(1);
  }
  cout << "New WEBPAGE: http://atlsr1.cern.ch" << s_htmlnew.c_str()<<endl;



//   fprintf(fHTMLNEW,"<html>\n");
//   fprintf(fHTMLNEW,"<head><title>Connectivity Test: Scan results from ROD %s and PP0 %s  </title></head>\n",rodname.c_str(),pp0name.c_str());
//   fprintf(fHTMLNEW,"<body>\n");
//   fprintf(fHTMLNEW,"<center><h2> Connectivity Test: Scan results from ROD %s and PP0 %s  </h2></center><br>\n",rodname.c_str(),pp0name.c_str());
//   fprintf(fHTMLNEW,"<hr>\n");
//   if (hname2!=NULL){
//     fprintf(fHTMLNEW,"<html>\n");
//   fprintf(fHTMLNEW,"<head><title>Connectivity Test: Scan results from ROD %s and PP0 %s  </title></head>\n",rodname.c_str(),pp0name.c_str());
//   fprintf(fHTMLNEW,"<body>\n");
//   fprintf(fHTMLNEW,"<center><h2> Connectivity Test: Scan results from ROD %s and PP0 %s  </h2></center><br>\n",rodname.c_str(),pp0name.c_str());
//   fprintf(fHTMLNEW,"<hr>\n");
//   }

  const int m_NOISE_MAX = 250;
  //  int m_NOISE_HVOFF_MAX = 500;
  const int m_THRESHOLD_MAX = 4800;
  const int m_THRESHOLD_MIN = 3800;
  const int m_THRESHOLD_DISPERSION_MAX = 250;
  const int m_THRESHOLD_DISPERSION_MIN = 50;
  //  int m_NOISE_DIFF_MIN = 100;
  //  int m_CHI2_MAX = 5;
  //  int m_GOOD_FIT_MIN = 100;
  //  int m_GOOD_FIT_HVOFF_MIN = 100;

  // now plot these histograms

  // BH: here I made the assumption that all modules have the same scan loop

  const int NDelay=v_scansteps0.at(0);
  const int NThreshold=v_scansteps1.at(0);
  const int NViset=v_scansteps2.at(0);
 
  std::vector<int> DelayVal; DelayVal.resize(NDelay);
  std::vector<int> ThresholdVal; ThresholdVal.resize(NThreshold);
  std::vector<int> VisetVal; VisetVal.resize(NViset);
    
  int bw=0;
  for (int i=0;i<NDelay;i++) {
    if (v_scansteps0.at(0)>1) bw=(v_scanmax0.at(0)-v_scanmin0.at(0))/(v_scansteps0.at(0)-1);
    DelayVal[i]=(int)(v_scanmin0.at(0)+i*bw);
  }
  for (int i=0;i<NThreshold;i++) {
    if (v_scansteps1.at(0)>1) bw=(v_scanmax1.at(0)-v_scanmin1.at(0))/(v_scansteps1.at(0)-1);
    ThresholdVal[i]=(int)(v_scanmin1.at(0)+i*bw);
  }
  for (int i=0;i<NViset;i++) {
    if (v_scansteps2.at(0)>1) bw=(v_scanmax2.at(0)-v_scanmin2.at(0))/(v_scansteps2.at(0)-1);
    VisetVal[i]=(int) (v_scanmin2.at(0)+i*bw);
  }    

  int iBestViset=-1;
  int BestViset=-1;
  int iBestViset1=-1;
  int iBestViset2=-1;
  int iBestViset3=-1;
  int iBestViset4=-1;
  if (v_histname.at(0)=="RAW_DATA_DIFF_2"){
    iBestViset=FindBestViset(0);
    if (iBestViset>-1) BestViset=VisetVal[iBestViset];
    iBestViset1=FindBestViset(1);
    iBestViset2=FindBestViset(2);
    iBestViset3=FindBestViset(3);
    iBestViset4=FindBestViset(4);
    cout << "output of five VIset tuning algorithms:" <<endl;
    cout << "Alg 0: " << VisetVal[iBestViset]<<endl;
    cout << "Alg 1: " << VisetVal[iBestViset1]<<endl;
    cout << "Alg 2: " << VisetVal[iBestViset2]<<endl;
    cout << "Alg 3: " << VisetVal[iBestViset3]<<endl;
    cout << "Alg 4: " << VisetVal[iBestViset4]<<endl;
  }


  TH1F* thrsumhist[5];
  for (int i=0;i<5;i++) {
    char name[200];
    sprintf(name,"ThrSummary_%i_%s_%s-%s",i,pp0name.c_str(),s_scanname.c_str(),myfold.c_str());
    thrsumhist[i]=new TH1F(name,name,7,-0.5,6.5);
  }

  const int nMod=v_modulename.size();
  std::vector<TH2F*> occhist; occhist.resize(nMod);
  TH2F *occhistref;

  const int nLink=v_nlink.at(0);
  int ntemp=nMod;
  if (NViset>0) ntemp*=NViset;
  if (nLink>0) ntemp*=nLink;
//   const int nHist=ntemp;
//   TH1F* stohist[nHist];
//   for (int i=0;i<nHist;i++) {
//     char name[100];
//     sprintf(name,"sto_%i",i);
//     stohist[i]=new TH1F(name,name,32,48,1583);
//   }
  
  vector <int> bestThreshold;
  vector <int> bestDelay;
  vector <bool> modPassPh3Thr;
  vector <bool> modPassPh3Noise;

  fprintf(fHTMLNEW,"<h3>SCAN: %s  ( Speed = %s )</h3><br><br>\n",s_scanname.c_str(),v_speed.at(0).c_str());
  
  for (uint im=0;im<v_modulename.size();im++){
    if (v_histname.at(im)=="RAW_DATA_DIFF_2"){
      for (int il=0;il<v_nlink.at(im);il++){
	int bestThr=FindBestThreshold(iBestViset,im,il);
	int bestDel=FindBestDelay(iBestViset,im,il,bestThr);
	bestThreshold.push_back(ThresholdVal[bestThr]);
	bestDelay.push_back(bestDel);
	int stepsize=0;
	if (v_scansteps2[im]>0) stepsize=(v_scanmax2[im]-v_scanmin2[im])/v_scansteps2[im];
	fprintf(fHTMLNEW,"Module: %s, Link %i <br>\n",v_connname.at(im).c_str(),il);
	TCanvas *c = new TCanvas("name","title",650*v_nhist.at(im),650);
	c->Divide(v_nhist.at(im),1);
	ColourScale(0);

	for (int ih=0;ih<v_nhist.at(im);ih++){ // loop over Viset
	  int index=(im*v_nhist.at(im)+ih)*v_nlink[im]+il;
	  char hname[200];
	  sprintf(hname,"%s_%s_%s_VIset%i_Link%i",v_connname.at(im).c_str(),v_modulename.at(im).c_str(),v_histname.at(im).c_str(),VisetVal[ih],il);
	  //	  TH2F* hist=new TH2F(hname,hname,v_histo.at(index).nBin(0),v_histo.at(index).min(0),v_histo.at(index).max(0),
	  //			      v_histo.at(index).nBin(1),v_histo.at(index).min(1),v_histo.at(index).max(1));
	  TH2F* hist=new TH2F(hname,hname,v_histo.at(index).nBin(0),v_scanmin0[im],v_scanmax0[im],
			      v_histo.at(index).nBin(1),v_scanmin1[im],v_scanmax1[im]);
	  for (int ix=0;ix<v_histo.at(index).nBin(0);ix++) {
	    for (int iy=0;iy<v_histo.at(index).nBin(1);iy++) {
	      float val=v_histo.at(index)(ix,iy);	  
	      hist->SetBinContent(ix+1,iy+1,val);
	    }
	  }

//                            TLatex l;
//                             l.SetTextSize(0.1);
//                             l.SetTextColor(3);                  // green
//                             l.DrawLatex(good_delay-0.73,good_yopt-0.2,"X");

	  c->cd(ih+1);
	  hist->Draw("colz");
	}

	char gifname[200];
	sprintf(gifname,"%s/%s_%s_%s_%s_%s_Link%i.png",dirn.c_str(),v_connname.at(im).c_str(),v_modulename.at(im).c_str(),v_histname.at(im).c_str(),idname.c_str(),s_scanname.c_str(),il);
	c->SaveAs(gifname);
	sprintf(gifname,"./%s_%s_%s_%s_%s_Link%i.png",v_connname.at(im).c_str(),v_modulename.at(im).c_str(),v_histname.at(im).c_str(),idname.c_str(),s_scanname.c_str(),il);
	fprintf(fHTMLNEW,"<a href=\"%s\"> <img src=\"%s\" HEIGHT=\"200\" BORDER=0 alt=\"\"</a><br>\n",gifname,gifname);
      }
    }
    // SLOW BOC SCAN
    if (v_histname.at(im)=="RAW_DATA_REF"){
      
    }
    // SLOW BOC SCAN

    if (v_histname.at(im)=="RAW_DATA_1"){
      cout << "now plot the RAW_DATA_1"<<endl;
      int off=0;
      if (v_nlink.at(im)==1) off=2;
      if (v_nhist.at(im)>1) {
	cout << "Boc2Root: code not yet ready for viset loop of slow scan"<<endl;
      }
      for (int il=0;il<v_nlink.at(im);il++){
	int index=im;
	if (blayer) {
	  index=(int) 3*(im/2) +off+il;
	}
	int stepsize=1;
	if (v_scansteps2[im]>0) stepsize=(v_scanmax2[im]-v_scanmin2[im])/v_scansteps2[im];
	fprintf(fHTMLNEW,"Module: %s, Link %i <br>\n",v_connname.at(im).c_str(),il);
	TCanvas *c = new TCanvas("name","title",650*v_nhist.at(im),650);
	c->Divide(v_nhist.at(im),1);
	ColourScale(0);
	//	for (int ih=0;ih<v_nhist.at(im);ih++){ // loop over Viset
	  //	  int index=(im*v_nhist.at(im)+ih)*v_nlink[im]+il;
	  //	  int viset=v_scanmin2[im]+ih*stepsize;
	  char hname[200];
	  sprintf(hname,"rawhist_%s_%s_%s_%s_Link%i",v_connname.at(im).c_str(),v_modulename.at(im).c_str(),v_histname.at(im).c_str(),s_scanname.c_str(),il);
	  TH2F* hist=new TH2F(hname,hname,v_histo.at(index).nBin(0),v_histo.at(index).min(0),v_histo.at(index).max(0),
 			      v_histo.at(index).nBin(1),v_scanmin0[im],v_scanmax0[im]);
// 			      v_histo.at(index).nBin(1),v_histo.at(index).min(1),v_histo.at(index).max(1));
	  for (int ix=0;ix<v_histo.at(index).nBin(0);ix++) {
	    for (int iy=0;iy<v_histo.at(index).nBin(1);iy++) {
	      float val=v_histo.at(index)(ix,iy);	  
	      hist->SetBinContent(ix+1,iy+1,val);
	    }
	  }
	  hist->GetXaxis()->SetTitle("Time");
	  hist->GetYaxis()->SetTitleOffset(1.3);
	  hist->GetYaxis()->SetTitle(v_scanpar0.at(im).c_str());
	  //	  hist->SetAxisRange(0,150,"X");
	  c->cd(1);
	  hist->Draw("COL");
	  //	}
	char gifname[200];
	sprintf(gifname,"%s/raw_%s_%s_%s_%s_%s_Link%i.gif",dirn.c_str(),v_connname.at(im).c_str(),v_modulename.at(im).c_str(),v_histname.at(im).c_str(),idname.c_str(),s_scanname.c_str(),il);
	cout << "gifname = "<<gifname<<endl;
	c->SaveAs(gifname);
	sprintf(gifname,"./raw_%s_%s_%s_%s_%s_Link%i.gif",v_connname.at(im).c_str(),v_modulename.at(im).c_str(),v_histname.at(im).c_str(),idname.c_str(),s_scanname.c_str(),il);
	fprintf(fHTMLNEW,"<a href=\"%s\"> <img src=\"%s\" HEIGHT=\"200\" BORDER=0 alt=\"\"</a><br>\n",gifname,gifname);
      }
    }


    // THRESHOLD SCAN
    if (v_histname.at(im)=="SCURVE_MEAN" || v_histname.at(im)=="SCURVE_SIGMA"
	|| v_histname.at(im)=="SCURVE_CHI2"){
      
      float EX=0, EX2=0;
      int NB=0;
      char lastc=v_connname[im].at(v_connname[im].size()-1);
      if (isalpha(lastc)) lastc=v_connname[im].at(v_connname[im].size()-2);
      int mid = atoi(&lastc)+1;

      if (v_histname.at(im)=="SCURVE_MEAN"){
	for (int ix=0;ix<v_histo.at(im).nBin(0);ix++) {
	  for (int iy=0;iy<v_histo.at(im).nBin(1);iy++) {
	    EX+=v_histo.at(im)(ix,iy);
	    EX2+=v_histo.at(im)(ix,iy)*v_histo.at(im)(ix,iy);
	    if (v_histo.at(im)(ix,iy)>0) NB++;
	  }
      }
	
	float SCURVE_MEAN_MEAN = 0;
	float SCURVE_MEAN_RMS = 0;
	
	if (NB>0) {
	  SCURVE_MEAN_MEAN= EX /(float) NB;
	  SCURVE_MEAN_RMS = sqrt(EX2 /(float) NB - SCURVE_MEAN_MEAN*SCURVE_MEAN_MEAN);
	}

	thrsumhist[0]->SetBinContent(mid,  SCURVE_MEAN_MEAN);
	thrsumhist[1]->SetBinContent(mid,  SCURVE_MEAN_RMS);
	if (SCURVE_MEAN_MEAN > m_THRESHOLD_MAX || SCURVE_MEAN_MEAN < m_THRESHOLD_MIN ||
	    SCURVE_MEAN_RMS > m_THRESHOLD_DISPERSION_MAX || SCURVE_MEAN_RMS < m_THRESHOLD_DISPERSION_MIN){
	  modPassPh3Thr.push_back(false);
	} else{
	  modPassPh3Thr.push_back(true);
	}
	    
      } else if (v_histname.at(im)=="SCURVE_SIGMA") {
	EX=0;EX2=0;NB=0;
	for (int ix=0;ix<v_histo.at(im).nBin(0);ix++) {
	  for (int iy=0;iy<v_histo.at(im).nBin(1);iy++) {
	    EX+=v_histo.at(im)(ix,iy);
	    EX2+=v_histo.at(im)(ix,iy)*v_histo.at(im)(ix,iy);
	  if (v_histo.at(im)(ix,iy)>0) NB++;
	  }
	}
	
	float SCURVE_SIGMA_MEAN = 0;
	float SCURVE_SIGMA_RMS = 0;
	if(NB>0){
	  SCURVE_SIGMA_MEAN = EX / (float)NB;
	  SCURVE_SIGMA_RMS = sqrt(EX2 / (float)NB - SCURVE_SIGMA_MEAN*SCURVE_SIGMA_MEAN);
	}
	thrsumhist[2]->SetBinContent(mid, SCURVE_SIGMA_MEAN);
	thrsumhist[3]->SetBinContent(mid, SCURVE_SIGMA_RMS);
	if (SCURVE_SIGMA_MEAN>m_NOISE_MAX) {
	  modPassPh3Noise.push_back(false);
	} else{
	  modPassPh3Noise.push_back(true);
	}
      } else if (v_histname.at(im)=="SCURVE_CHI2"){
	int N_GOOD_FIT = 0;
	for (int ix=0;ix<v_histo.at(im).nBin(0);ix++) {
	  for (int iy=0;iy<v_histo.at(im).nBin(1);iy++) {
	    float chi2=v_histo.at(im)(ix,iy);
	    if (chi2!=0 && chi2<5) N_GOOD_FIT++;
	  }
	}
	thrsumhist[4]->SetBinContent(mid,N_GOOD_FIT);
      }
      
    }
    // OCCUPANCY plot from Digital scan
    if (v_histname.at(im)=="OCCUPANCY"){
//       TCanvas *c = new TCanvas("name","title",650*v_modulename[im].size(),650);
      
//       c->Divide(v_modulename[im].size(),1);
//       ColourScale(0);
      char hname[200];
      sprintf(hname,"DigScan_%s_Mod%s_Scan%s",v_connname[im].c_str(),v_modulename[im].c_str(),s_scanname.c_str());
      occhist[im]=new TH2F(hname,hname,v_histo.at(im).nBin(0),v_histo.at(im).min(0),v_histo.at(im).max(0),
			     v_histo.at(im).nBin(1),v_histo.at(im).min(1),v_histo.at(im).max(1));
      for (int ix=0;ix<v_histo.at(im).nBin(0);ix++) {
	for (int iy=0;iy<v_histo.at(im).nBin(1);iy++) {
	  float val=v_histo.at(im)(ix,iy);
	  occhist[im]->SetBinContent(ix+1,iy+1,val);
	}
      }
      if (im == 0) { // Fill ref histogram (3 steps, 1 module)
	occhistref=new TH2F("digref","digref",v_histo.at(im).nBin(0),
			    v_histo.at(im).min(0),v_histo.at(im).max(0),
			    v_histo.at(im).nBin(1),v_histo.at(im).min(1),
			    v_histo.at(im).max(1));
	for (int ix=0;ix<18;++ix) {
	  for (int iy=0;iy<160;iy+=32) {
	    if (ix%2 == 0) {
	      occhistref->SetBinContent(ix+1,iy+1,200);
	      occhistref->SetBinContent(ix+1,iy+2,200);
	      occhistref->SetBinContent(ix+1,iy+3,200);
	    } else {
	      occhistref->SetBinContent(ix+1,iy+32,200);
	      occhistref->SetBinContent(ix+1,iy+31,200);
	      occhistref->SetBinContent(ix+1,iy+30,200);
	    }
	  }
	}
	occhistref->SetAxisRange(0,17,"X");
	occhistref->SetAxisRange(0,160,"Y");
      } // end ref histo
      occhist[im]->SetAxisRange(0,17,"X");
      occhist[im]->SetAxisRange(0,160,"Y");
      //      c->cd(im+1);
      //      occhist.Draw("colz");
      //      }
//       char gifname[200];
//       sprintf(gifname,"%s/DigScan_%s_%s_Sum.png",dirn.c_str(),v_connname.at(im).c_str(),v_modulename.at(im).c_str());
//       c->SaveAs(gifname);
//       sprintf(gifname,"./DigScan_%s_%s_Sum.png",dirn.c_str(),v_connname.at(im).c_str(),v_modulename.at(im).c_str());
//       fprintf(fHTMLNEW,"<a href=\"%s\"> <img src=\"%s\" HEIGHT=\"200\" BORDER=0 alt=\"\"</a><br>\n",gifname,gifname);
      
    }
    
  }
  
  // now summarise the result of the tuning
  if (v_histname.at(0)=="RAW_DATA_DIFF_2"){
    fprintf(fHTMLNEW,"Found %i values of VIset that were scanned: <br>\n",v_nhist.at(0));
    if (v_nhist.at(0)>1) {
      fprintf(fHTMLNEW,"<font size =\"+1\" color=\"green\">Tuned Value for VIset: %i</font><br>\n",BestViset);
      fprintf(fHTMLNEW,"Alternative Value for VIset (Alg 1): %i<br>\n",VisetVal[iBestViset1]);
      fprintf(fHTMLNEW,"Alternative Value for VIset (Alg 2): %i<br>\n",VisetVal[iBestViset2]);
      fprintf(fHTMLNEW,"Alternative Value for VIset (Alg 3): %i<br>\n",VisetVal[iBestViset3]);
      fprintf(fHTMLNEW,"Alternative Value for VIset (Alg 4): %i<br>\n",VisetVal[iBestViset4]);
    } else {
      fprintf(fHTMLNEW,"<font size =\"+1\" color=\"green\">No scan over VIset => no tuned value</font><br>\n");
    }
    fprintf(fHTMLNEW,"<font size =\"+1\">Best RX threshold and RX delay values (%s): </font><br>\n",pp0name.c_str());
    for (uint im=0;im<v_modulename.size();im++){
      for (int il=0;il<v_nlink.at(im);il++){
	// first sort out outlink
	int rxindex[4] = {0,3,4,7};
	char rxindexc[4] = {'A','B','C','D'};
	int index = im*v_nlink.at(im)+il;
	int stream=(rxindex[v_outLink[index]/10]*12+2)+v_outLink[index]%10;
	if (v_histname.at(im)=="RAW_DATA_DIFF_2"){
	
	  fprintf(fHTMLNEW,"<font size=\"+1\"> Module %s, link %i, Outlink=RX %c:%i (stream %i) : RX-delay = %i (%x), RX-trhreshold = %i (%x) </font> <br>\n",
		  v_connname.at(im).c_str(),il,
		  rxindexc[v_outLink[index]/10],v_outLink[index]%10,stream,
		  (int)bestDelay.at(index),(int)bestDelay.at(index),
		  (int)bestThreshold.at(index),(int)bestThreshold.at(index));
	  printf("Module %s, link %i, Outlink=RX %c:%i (stream %i) : RX-delay = %i (%x), RX-trhreshold = %i (%x)\n",
		 v_connname.at(im).c_str(),il,
		 rxindexc[v_outLink[index]/10],v_outLink[index]%10,stream,
		 (int)bestDelay.at(index),(int)bestDelay.at(index),
		 (int)bestThreshold.at(index),(int)bestThreshold.at(index));
	}
      }
    }
  } else if (v_histname.at(0)=="SCURVE_MEAN" || v_histname.at(0)=="SCURVE_SIGMA"
	     || v_histname.at(0)=="SCURVE_CHI2"){
    TCanvas *c = new TCanvas("name","title",650*5,650);
    c->Divide(5,1);
    thrsumhist[0]->GetYaxis()->SetTitle("Threshold");
    thrsumhist[1]->GetYaxis()->SetTitle("Threshold dispersion");
    thrsumhist[2]->GetYaxis()->SetTitle("Noise");
    thrsumhist[3]->GetYaxis()->SetTitle("Noise dispersion");
    thrsumhist[4]->GetYaxis()->SetTitle("Number of fits with 0<chi2<5");
    for (int i=0;i<5;i++) {
      c->cd(i+1);
      thrsumhist[i]->GetYaxis()->SetTitleOffset(1.5);
      thrsumhist[i]->GetXaxis()->SetTitle("Module #");
      thrsumhist[i]->SetFillColor(4);
      thrsumhist[i]->Draw("hist");
    }
    char gifname[200];
    sprintf(gifname,"%s/ThresholdScan_%s_%s_%s.png",dirn.c_str(),pp0name.c_str(),idname.c_str(),s_scanname.c_str());
    c->SaveAs(gifname);
    sprintf(gifname,"./ThresholdScan_%s_%s_%s.png",pp0name.c_str(),idname.c_str(),s_scanname.c_str());
    fprintf(fHTMLNEW,"<a href=\"%s\"> <img src=\"%s\" HEIGHT=\"200\" BORDER=0 alt=\"\"</a><br>\n",gifname,gifname);
    for (uint im=0;im<modPassPh3Noise.size();im++) {
      if (modPassPh3Noise[im]){
	fprintf(fHTMLNEW,"<font color=\"green\"> Module %s passed noise test!</font><br>\n",v_connname[im*3+1].c_str());
      } else {
	fprintf(fHTMLNEW,"<font color=\"red\"> Module %s failed noise test assuming HV was on! Is HV off maybe?<br></font>\n",v_connname[im*3+1].c_str());
      }
    }
    fprintf(fHTMLNEW,"<br>\n");
    for (uint im=0;im<modPassPh3Thr.size();im++) {
      if (modPassPh3Thr[im]){
	fprintf(fHTMLNEW,"<font color=\"green\"> Module %s passed threshold test!</font><br>\n",v_connname[im*3+1].c_str());
      } else {
	fprintf(fHTMLNEW,"<font color=\"red\"> Module %s failed threshold test assuming HV was on! Is HV off maybe?<br></font>\n",v_connname[im*3+1].c_str());
      }
    }
  } else if (v_histname.at(0)=="OCCUPANCY"){
    // DIGITAL SCAN
    if (nMod<1) return;
    TCanvas *c = new TCanvas("name","title",650*nMod,650);
    c->Divide(nMod,1);
    for (int im=0;im<nMod;im++){
      c->cd(im+1);
      occhist[im]->Draw("colz");
    }
    char gifname[200];
    sprintf(gifname,"%s/DigScan_%s_%s_%s.png",dirn.c_str(),pp0name.c_str(),idname.c_str(),s_scanname.c_str());
    c->SaveAs(gifname);
    sprintf(gifname,"./DigScan_%s_%s_%s.png",pp0name.c_str(),idname.c_str(),s_scanname.c_str());
    fprintf(fHTMLNEW,"Digital Scan <br>\n");
    fprintf(fHTMLNEW,"<a href=\"%s\"> <img src=\"%s\" HEIGHT=\"200\" BORDER=0 alt=\"\"</a><br>\n",gifname,gifname);
    c->Clear();
    c->Divide(nMod,1);
    for (uint im=0;im<v_modulename.size();im++){
      occhist[im]->SetMaximum(201);
      occhist[im]->SetMinimum(199);
      c->cd(im+1);
      occhist[im]->Draw("colz");
    }
    fprintf(fHTMLNEW,"Digital Scan Zoom<br>\n");
    sprintf(gifname,"%s/DigScan_%s_%s_%s_zoom.png",dirn.c_str(),pp0name.c_str(),idname.c_str(),s_scanname.c_str());
    c->SaveAs(gifname);
    sprintf(gifname,"./DigScan_%s_%s_%s_zoom.png",pp0name.c_str(),idname.c_str(),s_scanname.c_str());
    fprintf(fHTMLNEW,"<a href=\"%s\"> <img src=\"%s\" HEIGHT=\"200\" BORDER=0 alt=\"\"</a><br>\n",gifname,gifname);

    c->Clear();
    c->Divide(nMod,1);
    ColourScale(2);
    for (uint im=0;im<v_modulename.size();im++){
      occhist[im]->Add(occhistref,-1.);
      occhist[im]->SetMaximum(1);
      occhist[im]->SetMinimum(-1);
      c->cd(im+1);
      occhist[im]->Draw("colz");
    }
    fprintf(fHTMLNEW,"Digital Scan Difference with reference<br>\n");
    sprintf(gifname,"%s/DigScan_%s_%s_%s_diff.png",dirn.c_str(),pp0name.c_str(),idname.c_str(),s_scanname.c_str());
    c->SaveAs(gifname);
    sprintf(gifname,"./DigScan_%s_%s_%s_diff.png",pp0name.c_str(),idname.c_str(),s_scanname.c_str());
    fprintf(fHTMLNEW,"<a href=\"%s\"> <img src=\"%s\" HEIGHT=\"200\" BORDER=0 alt=\"\"</a><br>\n",gifname,gifname);
  }
  
  for (unsigned int im=0;im<v_histnameraw.size();im++){
    if (v_histnameraw.at(im)=="RAW_DATA_1"){
      int stepsize=1;
      int index=im;
      if (v_scansteps2[im]>0) stepsize=(v_scanmax2[im]-v_scanmin2[im])/v_scansteps2[im];
      char hname[200];
      sprintf(hname,"rawhist_%s_%s_%s_%s",v_connnameraw.at(im).c_str(),v_modulenameraw.at(im).c_str(),v_histnameraw.at(im).c_str(),s_scanname.c_str());

      TH2F* histr=new TH2F(hname,hname,v_historaw.at(index).nBin(0),
			   v_historaw.at(index).min(0),v_historaw.at(index).max(0),
			   v_historaw.at(index).nBin(1),v_scanmin0[im],v_scanmax0[im]);

      for (int ix=0;ix<v_historaw.at(index).nBin(0);ix++) {
	for (int iy=0;iy<v_historaw.at(index).nBin(1);iy++) {
	  float val=v_historaw.at(index)(ix,iy);	  
	  histr->SetBinContent(ix+1,iy+1,val);
	}
      }

      histr->GetXaxis()->SetTitle("Time");
      histr->GetYaxis()->SetTitle(v_scanpar0.at(im).c_str());

//       TCanvas *ccc;
//       ccc->cd(1);
//       histr->Draw("col");
    }
  }
  
  // close webpage
  fprintf(fHTMLNEW,"<hr>\n");
  fprintf(fHTMLNEW,"Extracted from %s\n",fname.c_str());
  fprintf(fHTMLNEW,"<hr>\n");
  fprintf(fHTMLNEW,"Last updated: <SCRIPT type=\"text/JavaScript\" language=\"JavaScript\">\n");
  fprintf(fHTMLNEW,"<!-- \n");
  fprintf(fHTMLNEW,"testdate = new Date(document.lastModified);  testdate = testdate.toLocaleString(); document.writeln(testdate); \n");
  fprintf(fHTMLNEW,"-->\n");
  fprintf(fHTMLNEW,"</SCRIPT></body></html>\n");
  fclose(fHTMLNEW); 
  std::cout << "INFO [ScanResultSpy::finishPp0] done with PP0."<<std::endl<<std::endl;
  std::cout << "----------------------------------------------------------------"<<std::endl;
  std::cout << "| WEBPAGE location: http://atlsr1.cern.ch" <<s_htmlnew.c_str()<<std::endl;
  std::cout << "----------------------------------------------------------------"<<std::endl<<std::endl;
  //  ff->Write();
  //  ff->Close();
}


bool ScanResultSpy::FindSTO(){
  std::cout << "----------------------------------------------------------------"<<std::endl;
  std::cout << "| Have a slow threshold scan => check for slow turnon : " << s_scanname.c_str()<<std::endl;
  std::cout << "----------------------------------------------------------------"<<std::endl;

  
  vector <Histo> rawdatadiff;
  vector <Histo> rawdataref;
  vector <Histo> rawdata;
  vector <string> cname;

  if (v_nlink.at(0)==2 && v_nlink.at(1)==1) {
    blayer=true;
  } else {
    blayer=false;
  }
  cout << "Start reading histograms "<< v_nlink.size()<<endl;
  if (!blayer) {
    for (uint i=0;i<v_histname.size();i++) {
      //      cout << "BH debug: 1  "<<i<< "  " << v_nlink.at(i)<<"  " <<v_modulename.at(i)<<endl;
      //      if (v_nlink.at(i)>1) {
      //	cout << "FindSTO Error: this should never happen"<<endl;
      //	continue;
      //      } else {
      if (1==1){ // BH
	Histo hix= v_histo.at(i);
	if (v_histname.at(i)=="RAW_DATA_1"){
	  rawdata.push_back(hix);
	  cname.push_back(v_connname.at(i).c_str());
	} else if (v_histname.at(i)=="RAW_DATA_REF"){
	  rawdataref.push_back(hix);
	} 
      }
    }
    for (uint i=0;i<v_histnameraw.size();i++) {
      Histo hix= v_historaw.at(i);
      if (v_histnameraw.at(i)=="RAW_DATA_DIFF_1"){
	rawdatadiff.push_back(hix);
      } 
    }
    if (rawdata.size() != rawdataref.size()) {
      cout << "Number of histograms does not match (not B-layer): " << rawdata.size() << "  " << rawdataref.size()<<endl;
      return false;
    }
  } else {
    cout << "BH debug: 0 (b layer) "<< v_histname.size()<< "  " << v_histo.size()<<endl;
    for (uint i=0;i<v_histname.size();i++) {
      cout << "BH debug: 1  "<<i<< "  " << v_nlink.at(i)<<"  " <<v_modulename.at(i)<<"  " <<v_connname.at(i)<<endl;
      int off=0;
      if (v_nlink.at(i)==1) off=2;
      for (int j=0;j<v_nlink.at(i);j++) {
	int index=(int) 3*(i/2) +off+j;
	Histo hix= v_histo.at(index);
	if (off==0){
	  rawdata.push_back(hix);
	  cname.push_back(v_connname.at(i).c_str());
	} else if (off==2){
	  rawdataref.push_back(hix);
	} 
      }
    }
    for (uint i=0;i<v_histnameraw.size();i++) {
      Histo hix= v_historaw.at(i);
      if (v_histnameraw.at(i)=="RAW_DATA_DIFF_1"){
	rawdatadiff.push_back(hix);
      } 
    }
    if (rawdatadiff.size() != 2*rawdataref.size()) {
      cout << "Number of histograms does not match (B-layer): " << rawdata.size() << "  " << 2*rawdataref.size()<<endl;
      return false;
    }
  }
  cout << "End of reading histograms"<<endl;
  const int nHist=rawdata.size();

  cout << "Number of histograms:  RAW_DATA_1  = "<<nHist<< "  RAW_DATA_REF = " <<rawdataref.size()<< "   " << rawdata.size()<<endl;



  std::vector<TH1F*> last1; last1.resize(nHist);
  std::vector<TH1F*> last10; last10.resize(nHist);
  std::vector<TH1F*> zeroflip; zeroflip.resize(nHist);
  std::vector<TH1F*> oneflip; oneflip.resize(nHist);
  int j=0;
  vector <bool> havehist;
  for (int i=0;i<nHist;i++) {
    int maxrow=rawdata.at(i).nBin(1)-1;
    int rownoerr=-1;
    for (int irow=0;irow<rawdata.at(i).nBin(1);irow++){
      int nerr=0;
      for (int icol=1;icol<rawdata.at(i).nBin(0);icol++){
	int rawddiff=0;
	if (rawdataref.at(0)(icol-1,0)==1) rawddiff=(int)(10-rawdata.at(i)(icol,irow));
	else rawddiff=(int) rawdata.at(i)(icol,irow);
	
	if (rawddiff>0) nerr++;

	//	if (icol<50) cout << irow << ","<<icol<< " : " <<nerr << "  " << rawddiff << "  " << rawdata.at(i)(icol,irow) << "  " << rawdataref.at(0)(icol-1,0)<<endl;
      }
      if (nerr<=0 && irow>rownoerr){
	// last row without errors
	rownoerr=irow;
      }
      //      cout << irow << " : " << nerr<< endl;
    }


    if (rownoerr>=maxrow){
      //      cout << "no upper error band in slow thresold scan => cannot do analysis for module: "<<v_modulename.at(i)<<endl;
      fprintf(fHTMLNEW,"%s: no STO analysis possible since no upper error band (not displayed)<br>\n",cname.at(i).c_str());
      havehist.push_back(false);
      continue;
    } else if (rownoerr<0) {
      //      cout << "no error free region in slow thresold scan => cannot do analysis for module: "<<v_modulename.at(i)<<endl;
      fprintf(fHTMLNEW,"%s: no STO analysis possible since no error free region (not displayed)<br>\n",cname.at(i).c_str());
      havehist.push_back(false);
      continue;
//     } else if (rownoerr+1>=maxrow){
//       cout << "error free region too close to histogram boundary => cannot do analysis for module: "<<v_modulename.at(i)<<endl;
//       fprintf(fHTMLNEW,"%s: no STO analysis possible since error free region too close to histogram boundary<br>\n",cname.at(i).c_str());
//       havehist.push_back(false);
//       continue;
    } else {
      //      cout << "Doing Slow Turnon Analysis for module "<< v_modulename.at(i)<< "  " << cname.at(i).c_str()<<endl;
      havehist.push_back(true);
    }

    j++;
    int rowmaxerr=999;

    std::vector<int> nzeroflip; nzeroflip.resize(rawdata.at(i).nBin(1)+1);
    std::vector<int> noneflip; noneflip.resize(rawdata.at(i).nBin(1)+1);
    std::vector<int> maxzeroflip; maxzeroflip.resize(rawdata.at(i).nBin(1)+1);
    std::vector<int> icolmax; icolmax.resize(rawdata.at(i).nBin(1)+1);
    std::vector<int> icollast1; icollast1.resize(rawdata.at(i).nBin(1)+1); 
    std::vector<int> icollast10; icollast10.resize(rawdata.at(i).nBin(1)+1);
    for (int irow=0;irow<rawdata.at(i).nBin(1);irow++){
      nzeroflip[irow]=0;
      noneflip[irow]=0;
      maxzeroflip[irow]=0;
      icolmax[irow]=0;
      icollast1[irow]=0;
      icollast10[irow]=0;
    }


    // calculate minimal threshold where we get >90% errors
    for (int irow=rownoerr;irow<rawdata.at(i).nBin(1);irow++){

      int nbit=0;
      int nerr=0;
      bool firstone=false;
      float sumdata=0;
      for (int icol=1;icol<rawdata.at(i).nBin(0);icol++){
	//	if (icol>1100) printf("%3.0f, %3.0f, %3.0f, %i \n",rawdataref.at(i)(icol-1,0),rawdatadiff.at(i)(icol,irow),rawdata.at(i)(icol,irow),icol);
	//
	//	if (icol<100 ) printf("xbit %i %i : %3.0f, %3.0f, %3.0f \n",icol,irow,rawdataref.at(i/2)(icol-1,0),rawdatadiff.at(i)(icol,irow),rawdata.at(i)(icol,irow));
	//	if (icol>1200 && icol<1224) printf("xbit %i %i : %3.0f, %3.0f, %3.0f \n",icol,irow,rawdataref.at(i/2)(icol-1,0),rawdatadiff.at(i)(icol,irow),rawdata.at(i)(icol,irow));
	
	sumdata+=rawdata.at(i)(icol,irow);
	int rawddiff=0;
	if (rawdataref.at(0)(icol-1,0)==1) rawddiff=(int)(10-rawdata.at(i)(icol,irow));
	else rawddiff=(int) rawdata.at(i)(icol,irow);
	if (rawdataref.at(0)(icol-1,0)>0) {
	  nbit++;
	  if(rawdata.at(i)(icol,irow)>0) {
	    nerr++;
	  }	  
	}
	if (rawdataref.at(0)(icol-1,0)==0){
	  if(rawdata.at(i)(icol,irow)>0) {
	    nzeroflip[irow]+=rawdata.at(i)(icol,irow);
	    if (rawdata.at(i)(icol,irow)>maxzeroflip[irow]) maxzeroflip[irow]=rawdata.at(i)(icol,irow);
	  }
	}
	if (rawdataref.at(0)(icol-1,0)>0){
	  if(rawdata.at(i)(icol,irow)<10) {
	    noneflip[irow]+=(int)(10-rawdata.at(i)(icol,irow));
	  }
	}
	if (icol<1224) {
	  if (rawdata.at(i)(icol,irow)>0 && icol>icollast1[irow]) {
	    icollast1[irow]=icol;
	  }
	  if (rawdata.at(i)(icol,irow)==10 && icol>icollast10[irow]) {
	    icollast10[irow]=icol;
	  }
	  if (rawdataref.at(0)(icol-1,0)>0&& icol>icolmax[irow]) icolmax[irow]=icol;
	}

      }

      if ((float)nerr>=0.9*(float)nbit && irow<rowmaxerr) {
	rowmaxerr=irow;
      }
    }

    for (int irow=rownoerr;irow<=rowmaxerr;irow++) {
      if (nzeroflip[irow]>0) {
	//	fprintf(fHTMLNEW,"%s: ERROR: Zero flipped to One %i  times in row %i (noerr row = %i, maxzeroflip = %i) => cannot do STO analysis <br> \n",cname.at(i).c_str(),nzeroflip[irow],irow,rownoerr,maxzeroflip[irow]);
	continue;
      }
    }
    

    char name[200];
    sprintf(name,"last1_%s_%s_%s",v_modulename.at(i).c_str(),cname.at(i).c_str(),s_scanname.c_str());
    last1[i] = new TH1F(name,name,rawdata.at(i).nBin(1),-0.5,rawdata.at(i).nBin(1)+0.5);
    sprintf(name,"last10_%s_%s_%s",v_modulename.at(i).c_str(),cname.at(i).c_str(),s_scanname.c_str());
    last10[i] = new TH1F(name,name,rawdata.at(i).nBin(1),-0.5,rawdata.at(i).nBin(1)+0.5);
    sprintf(name,"zeroflip_%s_%s_%s",v_modulename.at(i).c_str(),cname.at(i).c_str(),s_scanname.c_str());
    zeroflip[i] = new TH1F(name,name,rawdata.at(i).nBin(1),-0.5,rawdata.at(i).nBin(1)+0.5);
    sprintf(name,"oneflip_%s_%s_%s",v_modulename.at(i).c_str(),cname.at(i).c_str(),s_scanname.c_str());
    oneflip[i] = new TH1F(name,name,rawdata.at(i).nBin(1),-0.5,rawdata.at(i).nBin(1)+0.5);
    last1[i]->GetXaxis()->SetTitle("row number");
    last10[i]->GetXaxis()->SetTitle("row number");
    zeroflip[i]->GetXaxis()->SetTitle("row number");
    oneflip[i]->GetXaxis()->SetTitle("row number");
    last1[i]->GetYaxis()->SetTitle("Minimum number of bits missed");
    last10[i]->GetYaxis()->SetTitle("Maximum number of bits missed");
    zeroflip[i]->GetYaxis()->SetTitle("Number of flips of 0 to 1");
    oneflip[i]->GetYaxis()->SetTitle("Number of flips of 1 to 0");
    for (int irow=0;irow<=rawdata.at(i).nBin(1);irow++) {
      if (nzeroflip[irow]>0) {
	//	fprintf(fHTMLNEW,"%s: ERROR: Zero flipped to One %i  times in row %i, last1 = %i, last 10 = %i <br> \n",cname.at(i).c_str(),nzeroflip[irow],irow,icolmax[irow]-icollast1[irow],icolmax[irow]-icollast10[irow]);
	last1[i]->SetBinContent(irow,icolmax[irow]-icollast1[irow]);
	last10[i]->SetBinContent(irow,icolmax[irow]-icollast10[irow]);
	zeroflip[i]->SetBinContent(irow,nzeroflip[irow]);
      }
      oneflip[i]->SetBinContent(irow,noneflip[irow]);
    }
  }

  cout << "loop 1 ended"<<endl;

  int nplot=0;
  float max=0;
  float min=0;
  for (int i=0;i<havehist.size();i++) {
    if (havehist.at(i)) nplot++;
    else continue;
  }

  cout << "Nhistos: " << havehist.size()<<endl;

  max+=0.5;

  if (nplot<1) return false;

  cout << "FindSTO: now make the plots of STO "<<nplot <<endl;

  TCanvas*c = new TCanvas("name","title",650*nplot,650*4);
  c->Divide(nplot,4);
  vector <int> stoobs;
  vector <float> vsto;
  vector <float> lostbins;
  int ic=0;
  for (int i=0;i<nHist;i++){
    if (!havehist.at(i)) {
      stoobs.push_back(0);
      vsto.push_back(0);
      continue;
    }
    ic++;
    float valsto=0;
    for (int ib=0; ib<last1[i]->GetNbinsX();ib++) {
      if (last1[i]->GetBinContent(ib)>0) valsto++;
    }
    vsto.push_back(valsto);
    if (valsto>2) stoobs.push_back(4);
    else if (valsto>1) stoobs.push_back(3);
    else if (valsto>0 && last1[i]->GetMaximum()>100) stoobs.push_back(2);
    else stoobs.push_back(0);
    //    hst[i]->Rebin(10);
    //    hst[i]->Scale(1./10.);
    //    hst[i]->SetMinimum(min);
    //    hst[i]->SetMaximum(max);
    last1[i]->SetFillColor(33);
    last10[i]->SetFillColor(48);
    zeroflip[i]->SetFillColor(7);
    oneflip[i]->SetFillColor(8);
    last1[i]->GetYaxis()->SetTitleOffset(1.3);
    last10[i]->GetYaxis()->SetTitleOffset(1.3);
    zeroflip[i]->GetYaxis()->SetTitleOffset(1.3);
    oneflip[i]->GetYaxis()->SetTitleOffset(1.3);
    c->cd(ic);
    last1[i]->Draw("hist");
    c->cd(ic+nplot);
    last10[i]->Draw("hist");
    c->cd(ic+nplot*2);
    zeroflip[i]->Draw("hist");
    c->cd(ic+nplot*3);
    oneflip[i]->Draw("hist");
  }
  std::string gifname=dirn+"/SlowTurnOnAnalysis-"+rodname+"_"+s_scanname+"_"+v_connname.at(0)+".png";
  gifname=NoSpaces(gifname);
  c->SaveAs(gifname.c_str());
  fprintf(fHTMLNEW,"<hr>\n");
  fprintf(fHTMLNEW,"<h3>Slow turnon Analysis for scan %s</h3><br>\n",s_scanname.c_str());
  gifname="./SlowTurnOnAnalysis-"+rodname+"_"+s_scanname+"_"+v_connname.at(0)+".png";
  gifname=NoSpaces(gifname);
  fprintf(fHTMLNEW,"<a href=\"%s\"> <img src=\"%s\" HEIGHT=\"600\" BORDER=0 alt=\"\"</a><br>\n",gifname.c_str(),gifname.c_str());
  for (int i=0;i<nHist;i++){
    if (!havehist.at(i)) {
      fprintf(fHTMLNEW,"%s: no STO analysis possible since no upper error band or no error free region (not displayed)<br>\n",cname.at(i).c_str());
      printf("%s: no STO analysis possible since no upper error band or no error free region (not displayed)<br>\n",cname.at(i).c_str());
    } else {
      if (stoobs.at(i)<2) {
	fprintf(fHTMLNEW,"%s: <font color=\"green\"> no signs of STO found (nbin = %4.0f, # of lost bits = %4.0f)</font><br>\n",cname.at(i).c_str(),vsto.at(i),last1[i]->GetMaximum());
      } else if (stoobs.at(i)==2) {
	fprintf(fHTMLNEW,"%s: <font color=\"red\"> Mild slow turnon found (nbin = %4.0f, # of lost bins = %4.0f) </font><br>\n",cname.at(i).c_str(),vsto.at(i),last1[i]->GetMaximum());
      } else {
	fprintf(fHTMLNEW,"%s: <font color=\"red\"> Slow turnon found (nbin = %4.0f, # of lost bins = %4.0f) </font><br>\n",cname.at(i).c_str(),vsto.at(i),last1[i]->GetMaximum());
      }
    }
  }

  fprintf(fHTMLNEW,"<hr>\n");
  fprintf(fHTMLNEW,"end of STO<br>\n");
  return true;
}

/** Called whenever the result folder changes
 * @param folder_list the names of the folder hierarchy
 */
  void ScanResultSpy::newFolder(const std::vector<std::string> &folder_hierarchy) {

  std::cout << "INFO [ScanResultSpy::newFolder] path=\"";
  for (std::vector<std::string>::const_iterator folder_iter = folder_hierarchy.begin();
       folder_iter!=folder_hierarchy.end();
       folder_iter++) {
    std::cout << *folder_iter << "/";
    myfold=(*folder_iter);
  }
  std::cout << "\"" << std::endl;
}

//ScanResultSpy::PixScanHistoTypeList_t ScanResultSpy::s_pixScanHistoTypeList;
//bool ScanResultSpy::s_pixScanHistoTypeListInitialised = ScanResultSpy::initPixScanHistoTypeList();


//  /** Called whenever the scan changes
//   * @param folder_list the names of the folder hierarchy
//   */
//   void newScan(const ScanConfigRef &scan_config) = 0;

/** Called for each pix scan histo.
 */
void ScanResultSpy::newPixScanHisto(const std::string &histo_name,
				    const PixA::ScanConfigRef &scan_config,
				    const PixA::HistoHandle &histo_handle) {


  std::cout << "INFO [ScanResultSpy::newPixScanHisto] Histo =\"" << histo_name << "\"" << std::endl;

  // ignore the RAW_DATA_0 histograms right away
  //  if (histo_name=="RAW_DATA_0" || histo_name=="RAW_DATA_1") return;

  if (histo_name=="RAW_DATA_0" || histo_name=="RAW_DATA_DIFF_1") return;
  if (histo_name=="RAW_DATA_DIFF_1") return;
  PixA::HistoHandle a_histo_handle(histo_handle);
  PixA::PixDbDataContainer container(scan_config,
				     histo_handle);
  //  int histo_type = getHistoType(histo_name);

  HistoNameToType_t s_histoNameToType;
  std::shared_ptr<PixLib::PixScan> ps(new PixLib::PixScan());
  s_histoNameToType = ps->getHistoTypes();

  //  initHistoNameTypeList();
  std::map< std::string, int >::const_iterator iter = s_histoNameToType.find(histo_name);
  int histo_type =-1;
  if (iter != s_histoNameToType.end()) {
    histo_type= iter->second;
  }
  if (histo_type<0) cout << "INFO [ScanResultSpy::newPixScanHisto] problem! no histo_type found"<<endl;

  std::cout << "INFO [ScanResultSpy::newPixScanHisto] module=";
  if (!scan_config.connName().empty()) {
    std::cout << scan_config.connName() << "(" << scan_config.prodName() << ")";
  }
  else {
    std::cout << scan_config.prodName();
  }
  std::cout << " histo=\"" << histo_name << "\"" << std::endl;

  int outl[2];
  outl[0]  = scan_config.inOrderBocLinkRx(PixA::ScanConfig::kDTO1);
  outl[1]  = scan_config.inOrderBocLinkRx(PixA::ScanConfig::kDTO2);
  cout << "LINK: " << outl[0] << ", "<<outl[1]<<endl;
  bool altlink=false;
  if (scan_config.inOrderBocLinkRx(PixA::ScanConfig::kDTO1)==scan_config.bocLinkRx(PixA::ScanConfig::kDTO1)) altlink=false;
  // YC: for non-B layer, only one link is valid
  if (outl[0]<0) outl[0]=outl[1];
  if (outl[1]<0) outl[1]=outl[0];
  // YC: fix by hand when order seems wrong
  //     (info not available in file)
  if (!altlink && outl[0]>outl[1]) {
    int tmp = outl[0];
    outl[0] = outl[1];
    outl[1] = tmp;
  }

  PixA::HistoRef histo(a_histo_handle.ref());
  HistoInfo_t info;

  if (container.getScanSteps(0)<0) {std::cout << "No loop in Scan =>Return"<<endl; return;}

  container.numberOfHistos(histo_type,info);
  if (container.getScanSteps(0)<0) {std::cout << "No loop in Scan =>Return"<<endl; return;}



  std::string speed = confVal<std::string>(scan_config.pixScanConfig()["mcc"]["mccBandwidth"]);

  if (histo_name=="RAW_DATA_0" || histo_name=="RAW_DATA_DIFF_1"){

    if (!scan_config.connName().empty()) {
      v_connnameraw.push_back(scan_config.connName());
      v_modulenameraw.push_back(scan_config.prodName());
    } else {
      v_modulenameraw.push_back(scan_config.prodName());
    }
  } else {
    if (!scan_config.connName().empty()) {
      v_connname.push_back(scan_config.connName());
      v_modulename.push_back(scan_config.prodName());
    }
    else {
      v_modulename.push_back(scan_config.prodName());
    }
    
    for (uint i=info.minHistoIndex();i<info.maxHistoIndex();i++){
      v_outLink.push_back(outl[i]);
    }

    v_speed.push_back(speed);
    std::cout << "INFO [ScanResultSpy::newPixScanHisto] speed = "<<speed.c_str()<<endl;
    v_scansteps0.push_back(container.getScanSteps(0));
    v_scansteps1.push_back(container.getScanSteps(1));
    v_scansteps2.push_back(container.getScanSteps(2));
    v_scanmin0.push_back(container.getScanStart(0));
    v_scanmin1.push_back(container.getScanStart(1));
    v_scanmin2.push_back(container.getScanStart(2));
    v_scanmax0.push_back(container.getScanStop(0));
    v_scanmax1.push_back(container.getScanStop(1));
    v_scanmax2.push_back(container.getScanStop(2));
    v_scanpar0.push_back(container.getScanPar(0));
    v_scanpar1.push_back(container.getScanPar(1));
    v_scanpar2.push_back(container.getScanPar(2));
    v_scanlevel.push_back(container.getScanLevel());
    
    v_nhist.push_back(info.maxIndex(2));
    v_nlink.push_back(info.maxHistoIndex()-info.minHistoIndex());
    v_linkmax.push_back(info.maxHistoIndex());
    v_linkmin.push_back(info.minHistoIndex());
  }
  if (histo_name=="RAW_DATA_DIFF_2") {
    for (uint iz=0;iz<info.maxIndex(2);iz++){
      for (uint ih=info.minHistoIndex();ih<info.maxHistoIndex();ih++) {
	int idx_in[4]={-1,-1,iz,ih};
	Histo hix= Histo(*(container.getGenericPixLibHisto(histo_type,idx_in)));
	v_histo.push_back(hix);
      }
    }
  } else if (histo_name=="OCCUPANCY"   || histo_name=="SCURVE_CHI2" ||
	     histo_name=="SCURVE_MEAN" || histo_name=="SCURVE_SIGMA" ||
	     histo_name=="FMTC_LINKMAP") {
    Histo hix= Histo(*(container.getGenericPixLibHisto(histo_type,0,2)));
    v_histo.push_back(hix);
  } else if (histo_name=="RAW_DATA_1"){
    for (uint iz=0;iz<info.maxIndex(2);iz++){
      for (uint ih=info.minHistoIndex();ih<info.maxHistoIndex();ih++) {
	int idx_in[4]={-1,0,iz,ih};
	Histo hix= Histo(*(container.getGenericPixLibHisto(histo_type,idx_in)));
	v_histo.push_back(hix);
      }
    }
  } else if (histo_name=="RAW_DATA_REF"){
    //    for (uint iz=0;iz<info.maxIndex(2);iz++){
    for (uint ih=info.minHistoIndex();ih<info.maxHistoIndex();ih++) {
      int idx_in[4]={-1,-1,-1,ih};
      Histo hix= Histo(*(container.getGenericPixLibHisto(histo_type,idx_in)));
      v_historef.push_back(hix);
      v_histo.push_back(hix);
    }
    //RW
  } else if (histo_name=="RAW_DATA_DIFF_1"){
    //    for (uint iz=0;iz<info.maxIndex(2);iz++){
    for (uint ih=info.minHistoIndex();ih<info.maxHistoIndex();ih++) {
      int idx_in[4]={-1,0,0,ih};
      Histo hix= Histo(*(container.getGenericPixLibHisto(histo_type,idx_in)));
      v_historaw.push_back(hix);
    }
  } 
  //RW
  if (histo_name=="RAW_DATA_DIFF_1") {
    v_histnameraw.push_back(histo_name);
  } else {
    v_histname.push_back(histo_name);
  }
  cout << "NewPixScanHisto: analysed "<<  v_histo.size()<<" histograms for "<<v_modulename.size()<<" modules" <<endl;
  cout << "NewPixScanHisto: analysed "<<  v_histnameraw.size()<<" raw histograms for "<<v_modulename.size()<<" modules" <<endl;

}

/** Called for each normal histo.
 */
void ScanResultSpy::newHisto(const std::string &name, const PixA::HistoHandle &histo_handle, const std::string &rod_name, const PixA::ConfigHandle &scan_config) {
  PixA::HistoHandle a_histo_handle(histo_handle);
  std::cout << "INFO [ScanResultSpy::newHisto] module=";
  std::cout << " histo=\"" << name << "\"" << std::endl;
  PixA::HistoRef historef(a_histo_handle.ref());
  HistoInfo_t info;
  historef.numberOfHistos(name, info);
  std::cout << "idx:";
  for (int i=0; i<3; i++) {
    std::cout << info.maxIndex(i) << (i<2 ? "," : "");
  }

  const std::string nn="0";//"ThresholdScanResults";

  ColourScale(0);

  vector <Histo> v_sumhisto;

  // this histogram seems to contain no useful information
  if (name=="HistoSummary") return;
  for (uint ih=info.minHistoIndex();ih<info.maxHistoIndex();ih++) {
    if ((name=="PowerMesurements" || name=="PowerMeasurements") && ih==1 || ih==2) continue;
    if (name=="BocFast80" && ih>1 && ih<6) continue; 
    if (name=="InlinkOutlink" && ih>0 && ih<3)  continue; 
    // here I somehow have to get the summary histograms of each phase
    int idx_in[4]={-1,-1,-1,ih};
    bool hh=historef.haveHisto(name);
    if (!hh) std::cout << "Error in newHisto: histogram "<< name.c_str()<<" not found"<<endl;
    Histo hix= Histo(*(historef.histo(name,idx_in)));
    v_sumhisto.push_back(hix);
  }

  // now make the webpage and the plots

  const int nhist=v_sumhisto.size();

  if (nhist<1) return;

  //  char slevel[1]; sprintf(slevel,"%i",v_scanlevel.at(0));
  
  std::string s_html=dirn+"/"+rodname + "-Summary" +"--"+s_scanname+"-"+name+".html";
  std::string s_htmlnew = NoSpaces(s_html);

  fHTML= fopen(s_htmlnew.c_str(),"wt");
  if (fHTML == 0) {
    cout<<" Error: cannot write into "<<s_htmlnew<<". Check permissions."<<endl;
    exit(1);
  }
  cout << "New WEBPAGE: " << s_htmlnew.c_str()<<endl;
  fprintf(fHTML,"<html>\n");
  fprintf(fHTML,"<head><title>Connectivity Test: Summary Results from %s and histogram %s </title></head>\n",rodname.c_str(),name.c_str());
  fprintf(fHTML,"<body>\n");
  fprintf(fHTML,"<center><h2> Connectivity Test: Summary Results from %s and histogram %s </h2></center><br>\n",rodname.c_str(),name.c_str());
  fprintf(fHTML,"<hr>\n");

  int nDim=v_sumhisto.at(0).nDim();
  //  TH1F* phasehist[nhist];
  cout << "Number of dimensions: " << nDim<<endl;
  
  
  if (nDim==1) {
    TCanvas* c1; 
    for (uint i=0;i<v_sumhisto.size();i++){
      char cname[50];
      sprintf(cname,"name%i",i%3);
      if (i%3==0) {
	c1 = new TCanvas(cname,"title",650*3,650);
	c1->Divide(3,1);
      }
      c1->cd(i%3+1);
      TH1F* hist = new TH1F(v_sumhisto.at(i).name().c_str(),v_sumhisto.at(i).title().c_str(),
			    v_sumhisto.at(i).nBin(0),v_sumhisto.at(i).min(0),v_sumhisto.at(i).max(0));
      for (int ix=0;ix<v_sumhisto.at(i).nBin(0);ix++) {
	float val=v_sumhisto.at(i)(ix);
	hist->SetBinContent(ix+1,val);
      }
      hist->SetFillColor(4);
      hist->Draw();
      if (i%3-2==0 || i+1==v_sumhisto.size()) {
	char txt[2];
	sprintf(txt,"%i",i);
	std::string gifname=dirn+"/"+rodname+"_"+idname+"_"+s_scanname+"_"+name+"_"+txt+".png";
	gifname=NoSpaces(gifname);
	c1->SaveAs(gifname.c_str());
	gifname="./"+rodname+"_"+idname+"_"+s_scanname+"_"+name+"_"+txt+".png";
	//	sprintf(gifname,"./SumHist_%s_%s_%s_%i.gif",rodname.c_str(),s_scanname.c_str(),name.c_str(),i);
	gifname=NoSpaces(gifname);
	fprintf(fHTML,"<a href=\"%s\"> <img src=\"%s\" HEIGHT=\"200\" BORDER=0 alt=\"\"</a><br>\n",gifname.c_str(),gifname.c_str());
      }
    }
  } else if (nDim==2) {
    TCanvas* c1 = new TCanvas("name1","title",650*v_sumhisto.size(),650);
    c1->Divide(v_sumhisto.size(),1);
    //    cout << "name2d "<<name.c_str()<<endl;
    if (name=="PowerMesurements" || name=="PowerMeasurements") {
      std::string foutname=dirn+"/"+rodname + "--"+s_scanname+".txt";
      std::string foname=NoSpaces(foutname);
      ofstream out(foname.c_str());
      out << "OL===Fmt0=Fmt1=Fmt2=Fmt3=Fmt4=Fmt5=Fmt6=Fmt7===";
      out << "Irx0==Irx3==Irx4==Irx7==Drx0==Drx1==Drx2==Drx3==";
      out << "Viset0=Iiset0=Viset1=Iiset1=Viset2=Iiset2=Viset3=Viset3=";
      out << endl;
      
      for (uint i=0;i<v_sumhisto.size();i++){
	for (unsigned int ol=0; ol<48; ol++) {
	  if (ol==0 || ((ol%12)>1 && (ol%12)<10)) {
	    // Dump on screen
	    out << std::dec << std::setw(4) << ol << " ";
	    for (int iy=0; iy<32; iy++) {
	      int val = (int)(0.5 + 10.0*v_sumhisto.at(i)(ol,iy)/v_sumhisto.at(i)(ol,32));
	      if (val == 0) {
		out << "-";
	      } else if (val == 10) {
		out << "*";
	      } else {
		out << std::setw(1) << val;
	    }
	      if (iy%4 == 3) out << " ";
	    }
	    out << " ";
	    out << std::setw(5) << (int)(1000*v_sumhisto.at(i)(ol, 33+0)) << " ";
	    out << std::setw(5) << (int)(1000*v_sumhisto.at(i)(ol, 33+3)) << " ";
	    out << std::setw(5) << (int)(1000*v_sumhisto.at(i)(ol, 33+4)) << " ";
	    out << std::setw(5) << (int)(1000*v_sumhisto.at(i)(ol, 33+7)) << " "; 
	    out << std::setw(5) << (int)(1000*(v_sumhisto.at(i)(ol, 33+0)-v_sumhisto.at(i)(0, 33+0))) << " ";
	    out << std::setw(5) << (int)(1000*(v_sumhisto.at(i)(ol, 33+3)-v_sumhisto.at(i)(0, 33+3))) << " ";
	    out << std::setw(5) << (int)(1000*(v_sumhisto.at(i)(ol, 33+4)-v_sumhisto.at(i)(0, 33+4))) << " ";
	    out << std::setw(5) << (int)(1000*(v_sumhisto.at(i)(ol, 33+7)-v_sumhisto.at(i)(0, 33+7))) << " ";
	    out << std::fixed << std::setprecision(4) << v_sumhisto.at(i)(ol, 48+0) << " ";
	    out << std::fixed << std::setprecision(4) << v_sumhisto.at(i)(ol, 49+0) << " ";
	    out << std::fixed << std::setprecision(4) << v_sumhisto.at(i)(ol, 48+7) << " ";
	    out << std::fixed << std::setprecision(4) << v_sumhisto.at(i)(ol, 49+7) << " ";
	    out << std::fixed << std::setprecision(4) << v_sumhisto.at(i)(ol, 48+14) << " ";
	    out << std::fixed << std::setprecision(4) << v_sumhisto.at(i)(ol, 49+14) << " ";
	    out << std::fixed << std::setprecision(4) << v_sumhisto.at(i)(ol, 48+21) << " ";
	    out << std::fixed << std::setprecision(4) << v_sumhisto.at(i)(ol, 49+21) << " ";
	    out << std::scientific << std::endl;
	  }
	}
      } 
      out.close();
      std::string ftemp="./"+rodname +"--"+s_scanname+".txt";
      std::string fnamelink=NoSpaces(ftemp);
      fprintf(fHTML,"<font size=\"+1\"> <bf>Power Measurements:</bf> <a href=\"%s \"> ascii file containing results</a><br></font>\n",fnamelink.c_str());
    }
  }

  fprintf(fHTML,"<hr>\n");
  fprintf(fHTML,"Extracted from %s\n",fname.c_str());
  fprintf(fHTML,"<hr>\n");
  fprintf(fHTML,"Last updated: <SCRIPT type=\"text/JavaScript\" language=\"JavaScript\">\n");
  fprintf(fHTML,"<!-- \n");
  fprintf(fHTML,"testdate = new Date(document.lastModified);  testdate = testdate.toLocaleString(); document.writeln(testdate); \n");
  fprintf(fHTML,"-->\n");
  fprintf(fHTML,"</SCRIPT></body></html>\n");
  fclose(fHTML); 
//   cout << s_scanname.c_str()<< "  " << rodname.c_str()<<endl;

  cout << "end of newHisto"<<endl;
}

void ScanResultSpy::finish() {

  // get sumary histograms of phases
//   if (v_sumhisto.size()<=0){
//     std::cout << "INFO [ScanResultSpy::finish] found no histograms => return" << std::endl;
//     return;
//   } else {
//     std::cout << "INFO [ScanResultSpy::finish] found "<<  v_sumhisto.size()<< " histograms"<<std::endl;
//   }

//   TH1F* phase1hist[10];
//   TH1F* phase2hist[10];
//   TH1F* phase3hist[10];
//   TH1F* phase4hist[10];
  
//   for (int i=0;i<v_sumhisto.size();i++){
//     cout << v_sumhisto.at(i).name()<<endl;
//   }
  
//   std::cout << "* webpages that were produced *"<<endl;
//   std::cout << "-------------------------------"<<endl;
//   for (uint i=0;i<webname.size();i++) {
//     std::cout << webname.at(i).c_str()<<endl;
//   }
//   std::cout << "-------------------------------"<<endl;
}
