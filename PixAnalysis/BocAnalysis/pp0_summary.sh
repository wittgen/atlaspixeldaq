#!/usr/bin/env bash
# Script to produce a summary page per pp0 after having run boc2root
# on scan results
# It just collects html files to build a summary page
# Author: Yann Coadou (Yann.Coadou@cern.ch)
# Created: 7 May 2007
###########################################################################


if [ -z $1 ]; then
    echo "Syntax: $0 <dirname>"
    echo " where <dirname> is typically like /tdaqfiles/PixelPackage/Phase2/<some dir>"
    exit 1
fi

dir=$1

sqpname=`basename $dir`

# Check if the output directory is valid
if [ ! -d $dir ]; then
    echo Error: Not a valid directory: $dir
    if [ -d /tdaqfiles/PixelPackage/Phase2/$dir ]; then
	read -n 1 -s -p " Did you mean /tdaqfiles/PixelPackage/Phase2/$dir ? " answer
	echo
	# accept y, Y, return
	if [ x$answer = "xY" -o x$answer = "xy" -o x$answer = "x" ]; then
	    dir=/tdaqfiles/PixelPackage/Phase2/$dir
	else
	    echo; echo Check your directory.
	    exit 1
	fi
    else
	exit 1
    fi
fi

if [ ! -w $dir ]; then
    echo Error: No write access to $dir
    echo " First fix write permissions for $USER in this directory."
    exit 1
fi


# Produce summary page per ROOT file
####################################
Not_used(){
outhtml=${dir}/Summary-per-file-${sqpname}.html

cat > $outhtml <<EOF
<html>
<head><title> BOC scans per file for $sqpname </title></head>
<body>
<center><h2> BOC scans per file for $sqpname </h2></center><br>
<hr>
<ul>
EOF

npp0=0
for file in `ls ${dir}/*.html | grep -v -e "--" -e "index" -e "/Summary"`
do
    html=`basename $file`
    echo " <li> <a href=\"$html\">${html/.html/} </a>" >> $outhtml
    npp0=$((npp0+1))
done

echo Found $npp0 ROOT files.

cat >> $outhtml <<EOF
</ul>
<hr>
 Found $npp0 ROOT files.
<hr>
Last updated: <SCRIPT type="text/JavaScript" language="JavaScript">
<!--
testdate = new Date(document.lastModified);  testdate = testdate.toLocaleString(); document.writeln(testdate);
-->
</SCRIPT> by $USER
</body>
</html>
EOF


echo Summarized in http://atlsr1.cern.ch$outhtml
}



# Get the html files to summarize and split them by pp0
#######################################################
npp0=0
nsummaries=0
prev_pp0=""
for file in `ls ${dir}/*.html | grep -e "--"`
do
    html=`basename $file`
    root=`echo ${html/.html/} | awk -F "--" '{print $2}'`
    pp0=`echo $html | awk -F "--" '{print $1}'`
    if [ x$pp0 != x$prev_pp0 ]; then
	if [ `echo $pp0 |grep PP0 >& /dev/null; echo $?` -eq 0 ]; then
	    npp0=$((npp0+1))
	else
	    nsummaries=$((nsummaries+1))
	fi
	if [ x$prev_pp0 != "x" ]; then
#	    echo "  Finishing"
cat >>${dir}/Summary-${prev_pp0}.html <<EOF
</ul>
<hr>
Last updated: <SCRIPT type="text/JavaScript" language="JavaScript">
<!--
testdate = new Date(document.lastModified);  testdate = testdate.toLocaleString(); document.writeln(testdate);
-->
</SCRIPT> by $USER
</body>
</html>
EOF
	fi

#	echo new $pp0
cat >${dir}/Summary-${pp0}.html <<EOF
<html>
<head><title> BOC scans for $sqpname, $pp0 </title></head>
<body>
<center><h2> BOC scans for $sqpname, $pp0 </h2></center><br>
<hr>
<ul>
 <li> <a href="$html">$root </a>
EOF
	prev_pp0=$pp0
    else
	echo " <li> <a href=\"$html\">$root </a>" >> ${dir}/Summary-${pp0}.html
#	echo " filling"
    fi
done

if [ x$prev_pp0 != "x" ]; then
#    echo "  Finishing"
cat >>${dir}/Summary-${pp0}.html <<EOF
</ul>
<hr>
Last updated: <SCRIPT type="text/JavaScript" language="JavaScript">
<!--
testdate = new Date(document.lastModified);  testdate = testdate.toLocaleString(); document.writeln(testdate);
-->
</SCRIPT> by $USER
</body>
</html>
EOF
fi

echo Found $npp0 pp0s and $nsummaries ROD summaries.


# Produce summary page
outhtml=${dir}/Summary-per-pp0-${sqpname}.html

cat > $outhtml <<EOF
<html>
<head><title> BOC scans per pp0 for $sqpname </title></head>
<body>
<center><h2> BOC scans per pp0 for $sqpname </h2></center><br>
<hr>
<ul>
EOF

for file in `ls ${dir}/Summary-ROD_*.html`
do
    html=`basename $file`
    pp0=`echo ${html/.html/} | awk -F "Summary-" '{print $2}'`
    echo " <li> <a href=\"$html\">$pp0 </a>" >> $outhtml
done

cat >> $outhtml <<EOF
</ul>
<hr>
 Found $npp0 pp0s and $nsummaries ROD summaries.
<hr>
Last updated: <SCRIPT type="text/JavaScript" language="JavaScript">
<!--
testdate = new Date(document.lastModified);  testdate = testdate.toLocaleString(); document.writeln(testdate);
-->
</SCRIPT> by $USER
</body>
</html>
EOF


echo Summarized in http://atlsr1.cern.ch$outhtml

exit 0
