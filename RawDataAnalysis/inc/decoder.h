#ifndef __decoder_h__
#define __decoder_h__

#define N_EFB_ERROR_BITS 32
enum {
  ERRORTYPE_BCID=0,
  ERRORTYPE_L1ID,
  ERRORTYPE_MODULE_TIMEOUT,
  ERRORTYPE_DATAINCORRECT,
  ERRORTYPE_BUFFEROVERFLOW,
  ERRORTYPE_ATLAS0,
  ERRORTYPE_ATLAS1,
  ERRORTYPE_ATLAS2,
  ERRORTYPE_ATLAS3,
  ERRORTYPE_ATLAS4,
  ERRORTYPE_ATLAS5,
  ERRORTYPE_ATLAS6,
  ERRORTYPE_ATLAS7,
  ERRORTYPE_ATLAS8,
  ERRORTYPE_ATLAS9,
  ERRORTYPE_ATLAS10,
  ERRORTYPE_ALMOSTFULL,
  ERRORTYPE_OVERFLOW,
  ERRORTYPE_HEADER,
  ERRORTYPE_SYNCHBIT,
  ERRORTYPE_INVROWCOL,
  ERRORTYPE_MCCSKIP,
  ERRORTYPE_FE_ERROR_EOC,
  ERRORTYPE_FE_ERROR_HAMMING,
  ERRORTYPE_FE_ERROR_REGPARITY,
  ERRORTYPE_FE_ERROR_HITPARITY,
  ERRORTYPE_FE_ERROR_BITFLIP,
  ERRORTYPE_MCC_ERROR_HITOVERFLOW,
  ERRORTYPE_MCC_ERROR_EOEOVERFLOW,
  ERRORTYPE_MCC_ERROR_L1FEFAIL,
  ERRORTYPE_MCC_ERROR_BCIDFAIL,
  ERRORTYPE_MCC_ERROR_L1GLOABLFAIL,
  ERRORTYPE_DATALOSS,
  ERRORTYPE_LAST
};

char errorstring[ERRORTYPE_LAST][255] = {
  "ERRORTYPE_BCID",
  "ERRORTYPE_L1ID",
  "ERRORTYPE_MODULE_TIMEOUT",
  "ERRORTYPE_DATAINCORRECT",
  "ERRORTYPE_BUFFEROVERFLOW",
  "ERRORTYPE_ATLAS0",
  "ERRORTYPE_ATLAS1",
  "ERRORTYPE_ATLAS2",
  "ERRORTYPE_ATLAS3",
  "ERRORTYPE_ATLAS4",
  "ERRORTYPE_ATLAS5",
  "ERRORTYPE_ATLAS6",
  "ERRORTYPE_ATLAS7",
  "ERRORTYPE_ATLAS8",
  "ERRORTYPE_ATLAS9",
  "ERRORTYPE_ATLAS10",
  "ERRORTYPE_ALMOSTFULL",
  "ERRORTYPE_OVERFLOW",
  "ERRORTYPE_HEADER",
  "ERRORTYPE_SYNCHBIT",
  "ERRORTYPE_INVROWCOL",
  "ERRORTYPE_MCCSKIP",
  "ERRORTYPE_FE_ERROR_EOC",
  "ERRORTYPE_FE_ERROR_HAMMING",
  "ERRORTYPE_FE_ERROR_REGPARITY",
  "ERRORTYPE_FE_ERROR_HITPARITY",
  "ERRORTYPE_FE_ERROR_BITFLIP",
  "ERRORTYPE_MCC_ERROR_HITOVERFLOW",
  "ERRORTYPE_MCC_ERROR_EOEOVERFLOW",
  "ERRORTYPE_MCC_ERROR_L1FEFAIL",
  "ERRORTYPE_MCC_ERROR_BCIDFAIL",
  "ERRORTYPE_MCC_ERROR_L1GLOABLFAIL",
  "ERRORTYPE_DATALOSS"
};

#define mBOF       0xb0f00000
#define mEOF       0xe0f00000
#define mRODHEAD   0xee1234ee
#define mROBHEAD   0xdd1234dd
#define mROSHEAD   0xcc1234cc
#define mSUBDHEAD  0xbb1234bb
#define mEVENTHEAD 0xaa1234aa
#define mSTRANGEHEAD 0x12340000

struct rod_header {
  unsigned int formatversion;
  unsigned int sourceID;
  unsigned int RunNR;
  unsigned int L1ID;
  unsigned int BCID;
  unsigned int L1Ttype;
  unsigned int detectorEtype;
};
typedef struct rod_header rod_header;

struct event_trailer {
  unsigned int errorflag;
  unsigned int errorcount;
  unsigned int numstatus;
  unsigned int wordcount;
  unsigned int statusposition;
  unsigned int checksum;
};
typedef struct event_trailer event_trailer;

struct empty_rod_fragment {
  rod_header rodheaderemptyEF;
  unsigned int blap[4];
};
typedef struct empty_rod_fragment empty_rod_fragment;

struct generic_header {
  unsigned int fragment_size;
  unsigned int header_size;
  unsigned int formatversion;
  unsigned int sourceID;
  unsigned int num_status;
};
typedef struct generic_header generic_header;

struct full_event_header {
  unsigned int fragment_size;
  unsigned int header_size;
  unsigned int formatversion;
  unsigned int sourceID;
  unsigned int num_status;
  unsigned int status0;     //suppose there is only one
  unsigned int checkSumType;
  unsigned int bunchCrossingTimeSec;
  unsigned int bunchCrossingTimeNanoSec;
  unsigned int global_eventID;
  unsigned int RunType;
  unsigned int RunNR;
  unsigned int LumiBlockNR;
  unsigned int ExtL1ID;
  unsigned int BCID;
  unsigned int L1TriggerTtype;
  unsigned int numOfL1TriggerInfoWords; //suppose to be 0
  unsigned int numOfL2TriggerInfoWords; //suppose to be 0
  unsigned int numOfEFTriggerInfoWords; //suppose to be 0
  unsigned int numOfStreamTagsWords;    //suppose to be 0
};
typedef struct full_event_header full_event_header;

struct ros_header {
  unsigned int fragment_size;
  unsigned int header_size;
  unsigned int formatversion;
  unsigned int sourceID;
  unsigned int num_status;
  unsigned int status0;     //suppose there is only one
  unsigned int wordcount;
  unsigned int RunNR;
  unsigned int ExtL1ID;
  unsigned int RosBcid;
};
typedef struct ros_header ros_header;

struct rob_status_elements {
  unsigned int error_bits;
  unsigned int rodFragCRC;
  unsigned int checkSum;
};

typedef struct rob_status_elements rob_status_elements;

#define CACHESIZE 16

char* decode_FEerror(int byte);
void decode_trailer(event_trailer trailer, int rodfragmentcount,
		    unsigned int eventnum, unsigned int eventtime,
		    unsigned int ecrnum,
		    unsigned int rodbcid, unsigned int lvl1id,
		    unsigned int rodid, bool verbose = false);
void byte_to_bitstring(int byte, char* outchar, int len);
char* decode_MCCerror(int byte);
char* decode_MCCerror(int byte);
char* decode_ebits(int byte);
char* decode_ebits_trail(int byte);
char* decode_generic(int twobytes);
char* decode_rob_specific(int twobytes);
void decode_data(unsigned int word, unsigned int rob_id, int event);
event_trailer reorder_cache(unsigned int cache[CACHESIZE], int count);

#endif /* __decoder_h__ */
