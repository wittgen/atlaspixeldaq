
/* routine to decode a pixel event captured on the DSP */
/* required argument is the input file                 */
/* written by A. Korn LBNL: Andreas.Korn@cern.ch       */
/* higher order headers, decode of rob specific elements*/
/* added and adapted (not backwards compatible)        */
/* to event format 4.0 by Iskander Ibragimov           */
/* $Id: */

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include "decoder.h"

int theEveBcid = 0;
int theRodBcid = 0;
int theModBcid = 0;
int theROBID = 0;
int glink = 0;
int grod = 0;

void print_event_header( full_event_header &header ) {
  printf("%-20s : %u\n", "ExtL1ID", header.ExtL1ID);
  printf("%-20s : %u\n", "BCID", header.BCID);
}

void print_ros_header( ros_header &header ) {
  printf("%-20s : %u\n", "fragment_size", header.fragment_size);
  printf("%-20s : %u\n", "header_size",   header.header_size);
  printf("%-20s : %u\n", "formatversion", header.formatversion);
  printf("%-20s : %u\n", "sourceID",      header.sourceID);
  printf("%-20s : %u\n", "num_status",    header.num_status);
  printf("%-20s : %u\n", "status0",       header.status0);
  printf("%-20s : %u\n", "wordcount",     header.wordcount);
  printf("%-20s : %u\n", "RunNR",         header.RunNR);
  printf("%-20s : %u\n", "ExtL1D",        header.ExtL1ID);
  printf("%-20s : %u\n", "RosBcid",       header.RosBcid);
}

using namespace std;

int main(int argc, char **argv) {
  unsigned int word;
  full_event_header eve_head;
  ros_header ros_head;
  
  if( argc < 3 ) {
    cout << "Usage: ./skim_pixel input_file output_file [pixel,ibl]" << endl;
    return 1;
  }
  
  ifstream fin( argv[1], ios::binary );
  
  if( !fin.is_open() ) {
    cout << "File is not open!" << endl;
    return 1;
  }
  
  ofstream fout( argv[2], ios::out | ios::binary );
  
  string option = "";
  if( argc >3 )  option = argv[3];
  transform(option.begin(), option.end(), option.begin(), ::tolower);
  
  int a;
  int size_eve = 0;
  int size_eve_head = 0;
  int size_sum_pixel_ros = 0;
  int n = 0;
  int event_count = 0;
  
  unsigned int buf[100000];
  
  fin.read( (char*) &word, sizeof( word ) );
  while( !fin.eof() ) {
    
    // Begin of event (aka pre-event header)
    if( (word&0xffff0000) == mSTRANGEHEAD ) {
      fout.write( (char*) &word, sizeof( word ) );
      
      fin.read( (char*) &word, sizeof( word ) );
      fout.write( (char*) &word, sizeof( word ) );
      int size = word - 2;
      fin.read   ( (char*) &buf, size*sizeof(word) );
      fout.write ( (char*) &buf, size*sizeof(word) );
      
      fin.read( (char*) &word, sizeof( word ) );
      continue;
    }
      
    // scan to the Event header tag (aa1234aa)
    else if( word == mEVENTHEAD ) {
      if( event_count%100==0 ) cout << "processing number of events " << event_count << endl;
      
      // Need to write out mEVENTHEAD (aa1234aa) at the beginning of the loop
      fout.write( (char*) &word, sizeof( word ) );
      
      fin.read   ( (char*) &eve_head, sizeof( eve_head ) );
      fout.write ( (char*) &eve_head, sizeof( eve_head) );
      
      size_eve = eve_head.fragment_size;
      size_eve_head = eve_head.header_size;
      
      // header tail part
      int tail_word = (size_eve_head-sizeof(eve_head)/sizeof(word)-1);
      
      fin.read   ( (char*) buf, tail_word*sizeof(word) );
      fout.write ( (char*) buf, tail_word*sizeof(word) );
      
      event_count++;
      
      
      // scan to the ROB header tag (dd1234dd)
      // should loop only once.
      int nn=0;
      while( word != mROBHEAD ) {
	fin.read   ( (char*) &word, sizeof( word ) );
	nn++;
      }
      
      while ( word == mROBHEAD ) {
	
	// read the ROB header
	fin.read   ( (char*) &ros_head, sizeof( ros_head ) );
	
	// skim only pixel or ibl ros fragment.
	// Pixel source IDs are 0x0011XXXX, 0x0012XXXX, 0x0013XXXX.
	// IBL   source ID  is  0x0014XXXX.
	
	bool write_ros_flag = false;
	if( option == "pixel" ) {
	  if( (ros_head.sourceID & 0xffff0000)>>16 == 0x11 ||
	      (ros_head.sourceID & 0xffff0000)>>16 == 0x12 ||
	      (ros_head.sourceID & 0xffff0000)>>16 == 0x13 )    write_ros_flag = true;
	  
	} else if( option == "ibl" ) {
	  if( (ros_head.sourceID & 0xffff0000)>>16 == 0x14  )   write_ros_flag = true;
	}
	
	// write out the ROS header
	if( write_ros_flag ) {
	  fout.write ( (char*) &word, sizeof(word) );
	  fout.write ( (char*) &ros_head, sizeof( ros_head ) );
	}
	
	// read the rest of the ROS fragment i.e. the contents of the data
	int size_ros = ros_head.fragment_size;
	int size_buf = (size_ros - sizeof(ros_head)/sizeof(word) - 1) * sizeof(word);
	fin.read   ( (char*) buf, size_buf );
	
	// write out the ROS fragment stored in the buffer
	if ( write_ros_flag ) fout.write ( (char*) buf, size_buf );
	
	// read the next word (should be the next ROB header except the end of event
	fin.read   ( (char*) &word, sizeof(word) );
	
      }
      
      if( (word&0xffff0000) != mSTRANGEHEAD ) {
	cout << "warning: " << fin.tellg() << " " << hex << showbase << word << endl;
      }
      
      
      continue;
    }
    
    else {
      fin.read( (char*) &word, sizeof(word) );
    }
    
  } // end of file stream while loop
  
  cout << "-----------------------------------------------------------" << endl;
  cout << "skimmed " << event_count << "event." << endl;
  
  fin.close();
  fout.close();
  
  return 0;
}












