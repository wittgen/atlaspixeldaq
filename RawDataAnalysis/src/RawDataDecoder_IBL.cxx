
/* routine to decode a pixel event captured on the DSP */
/* required argument is the input file                 */
/* written by A. Korn LBNL: Andreas.Korn@cern.ch       */
/* higher order headers, decode of rob specific elements*/
/* added and adapted (not backwards compatible)        */
/* to event format 4.0 by Iskander Ibragimov           */
/* $Id: */

#include <stdio.h>
#include <string.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TFile.h>
#include <set>
#include <iostream>
#include <string>

char string[256];

Int_t theEveBcid = 0;
Int_t theRodBcid = 0;
Int_t theModBcid = 0;
Int_t theROBID = 0;
Int_t glink = 0;
Int_t grod = 0;
TH1F *hmod_bcid = 0;
TH1F *hrod_bcid = 0;
TH1F *heve_bcid = 0;
TH1F *hdiff_eve_rod_bcid = 0;
TH1F *hdiff_rod_mod_bcid = 0;
TH1F *hdiff_eve_mod_bcid = 0;
TH2F *hdiff_eve_rod_bcid_by_rod = 0;
TH2F *hdiff_eve_rod_bcid_by_rob = 0;
TH2F *hdiff_rod_mod_bcid_by_mod = 0;
TH2F *hdiff_rod_mod_bcid_by_rod = 0;

TH1F *hhit_tot = 0;

TH1F *herrortype = 0;

std::set<Int_t> modid;
std::set<Int_t> rodid;
std::set<Int_t> robid;

#define N_EFB_ERROR_BITS 32
enum {
  ERRORTYPE_BCID=0,
  ERRORTYPE_L1ID,
  ERRORTYPE_MODULE_TIMEOUT,
  ERRORTYPE_DATAINCORRECT,
  ERRORTYPE_BUFFEROVERFLOW,
  ERRORTYPE_ATLAS0,
  ERRORTYPE_ATLAS1,
  ERRORTYPE_ATLAS2,
  ERRORTYPE_ATLAS3,
  ERRORTYPE_ATLAS4,
  ERRORTYPE_ATLAS5,
  ERRORTYPE_ATLAS6,
  ERRORTYPE_ATLAS7,
  ERRORTYPE_ATLAS8,
  ERRORTYPE_ATLAS9,
  ERRORTYPE_ATLAS10,
  ERRORTYPE_ALMOSTFULL,
  ERRORTYPE_OVERFLOW,
  ERRORTYPE_HEADER,
  ERRORTYPE_SYNCHBIT,
  ERRORTYPE_INVROWCOL,
  ERRORTYPE_MCCSKIP,
  ERRORTYPE_FE_ERROR_EOC,
  ERRORTYPE_FE_ERROR_HAMMING,
  ERRORTYPE_FE_ERROR_REGPARITY,
  ERRORTYPE_FE_ERROR_HITPARITY,
  ERRORTYPE_FE_ERROR_BITFLIP,
  ERRORTYPE_MCC_ERROR_HITOVERFLOW,
  ERRORTYPE_MCC_ERROR_EOEOVERFLOW,
  ERRORTYPE_MCC_ERROR_L1FEFAIL,
  ERRORTYPE_MCC_ERROR_BCIDFAIL,
  ERRORTYPE_MCC_ERROR_L1GLOABLFAIL,
  ERRORTYPE_DATALOSS,
  ERRORTYPE_LAST
};

char errorstring[ERRORTYPE_LAST][255] = {
  "ERRORTYPE_BCID",
  "ERRORTYPE_L1ID",
  "ERRORTYPE_MODULE_TIMEOUT",
  "ERRORTYPE_DATAINCORRECT",
  "ERRORTYPE_BUFFEROVERFLOW",
  "ERRORTYPE_ATLAS0",
  "ERRORTYPE_ATLAS1",
  "ERRORTYPE_ATLAS2",
  "ERRORTYPE_ATLAS3",
  "ERRORTYPE_ATLAS4",
  "ERRORTYPE_ATLAS5",
  "ERRORTYPE_ATLAS6",
  "ERRORTYPE_ATLAS7",
  "ERRORTYPE_ATLAS8",
  "ERRORTYPE_ATLAS9",
  "ERRORTYPE_ATLAS10",
  "ERRORTYPE_ALMOSTFULL",
  "ERRORTYPE_OVERFLOW",
  "ERRORTYPE_HEADER",
  "ERRORTYPE_SYNCHBIT",
  "ERRORTYPE_INVROWCOL",
  "ERRORTYPE_MCCSKIP",
  "ERRORTYPE_FE_ERROR_EOC",
  "ERRORTYPE_FE_ERROR_HAMMING",
  "ERRORTYPE_FE_ERROR_REGPARITY",
  "ERRORTYPE_FE_ERROR_HITPARITY",
  "ERRORTYPE_FE_ERROR_BITFLIP",
  "ERRORTYPE_MCC_ERROR_HITOVERFLOW",
  "ERRORTYPE_MCC_ERROR_EOEOVERFLOW",
  "ERRORTYPE_MCC_ERROR_L1FEFAIL",
  "ERRORTYPE_MCC_ERROR_BCIDFAIL",
  "ERRORTYPE_MCC_ERROR_L1GLOABLFAIL",
  "ERRORTYPE_DATALOSS"
};

#define mBOF       0xb0f00000
#define mEOF       0xe0f00000
#define mRODHEAD   0xee1234ee
#define mROBHEAD   0xdd1234dd
#define mROSHEAD   0xcc1234cc
#define mSUBDHEAD  0xbb1234bb
#define mEVENTHEAD 0xaa1234aa
#define mSTRANGEHEAD 0x12340000

#define rHEAD      0x2
#define rTRAIL     0x4
#define rERROR     0x1
#define rHIT       0x8
#define rHIT_COND  0xa

struct rod_header {
  unsigned int formatversion;
  unsigned int sourceID;
  unsigned int RunNR;
  unsigned int L1ID;
  unsigned int BCID;
  unsigned int L1Ttype;
  unsigned int detectorEtype;
};
typedef struct rod_header rod_header;

struct event_trailer {
  unsigned int errorflag;
  unsigned int errorcount;
  unsigned int numstatus;
  unsigned int wordcount;
  unsigned int statusposition;
  unsigned int checksum;
};
typedef struct event_trailer event_trailer;

struct empty_rod_fragment {
  rod_header rodheaderemptyEF;
  unsigned int blap[4];
};
typedef struct empty_rod_fragment empty_rod_fragment;

struct generic_header {
  unsigned int fragment_size;
  unsigned int header_size;
  unsigned int formatversion;
  unsigned int sourceID;
  unsigned int num_status;
};
typedef struct generic_header generic_header;

struct full_event_header {
  unsigned int fragment_size;
  unsigned int header_size;
  unsigned int formatversion;
  unsigned int sourceID;
  unsigned int num_status;
  unsigned int status0;     //suppose there is only one
  unsigned int checkSumType;
  unsigned int bunchCrossingTimeSec;
  unsigned int bunchCrossingTimeNanoSec;
  unsigned int global_eventID;
  unsigned int RunType;
  unsigned int RunNR;
  unsigned int LumiBlockNR;
  unsigned int ExtL1ID;
  unsigned int BCID;
  unsigned int L1TriggerTtype;
  unsigned int numOfL1TriggerInfoWords; //suppose to be 0
  unsigned int numOfL2TriggerInfoWords; //suppose to be 0
  unsigned int numOfEFTriggerInfoWords; //suppose to be 0
  unsigned int numOfStreamTagsWords;    //suppose to be 0
};
typedef struct full_event_header full_event_header;

struct ros_header {
  unsigned int fragment_size;
  unsigned int header_size;
  unsigned int formatversion;
  unsigned int sourceID;
  unsigned int num_status;
  unsigned int status0;     //suppose there is only one
  unsigned int wordcount;
  unsigned int RunNR;
  unsigned int ExtL1ID;
  unsigned int RosBcid;
};
typedef struct ros_header ros_header;

struct rob_status_elements {
  unsigned int error_bits;
  unsigned int rodFragCRC;
  unsigned int checkSum;
};

typedef struct rob_status_elements rob_status_elements;

char* decode_FEerror(int byte){
  static char FEERROR[256];
  int len = 0;
  
  printf("decode_FEerror: %x : ", byte);
  if(byte & 1) printf("EOCOVERFLOW|");
  len = strlen(FEERROR);
  if(byte & 2) printf("HAMMINGCODE|");
  len = strlen(FEERROR);
  if(byte & 4) printf("REGPARITY|");
  len = strlen(FEERROR);
  if(byte & 8) printf("HITPARITY|");
  
  return FEERROR;
}

void decode_trailer(event_trailer trailer, int rodfragmentcount, unsigned int eventnum, unsigned int eventtime, unsigned int ecrnum, unsigned int rodbcid, unsigned int lvl1id, unsigned int rodid, bool verbose = false){
  int i, elements;
  unsigned int  *temp;
  
  if(2==trailer.numstatus && 1==trailer.statusposition){
    printf("</module_hits>\n");
    printf("<Trailer\n errorflag ='%x' \n",trailer.errorflag);
    for(i = 0;i<32;i++){
      if(trailer.errorflag & (1<<i)) {
	printf("%u\t%u\t%u\t%u\t%u\t0x%x\tTrailer errorflag %s\n", eventnum, eventtime, ecrnum, rodbcid, rodid, lvl1id, errorstring[i]);
	herrortype->Fill( errorstring[i], 1 );
      }
    }
    
    printf("  errorcount ='%u' \n",trailer.errorcount & 0xffff);
    printf("  errorcount ='%x' \n",trailer.errorcount);
    if(trailer.errorcount & 1 << 16) printf("  TIM_clock_ERROR = 'true' \n");
    if(trailer.errorcount & 1 << 17) printf("  BOC_clock_ERROR = 'true' \n");
    if(trailer.errorcount & 1 << 18) printf("  ROL_testblock_ACTIVE = 'true' \n");
    printf("  num_status_should_be_2 = '%x' \n",trailer.numstatus);
    printf("  wordcount = '%u' ",trailer.wordcount);
    printf("  status_pos_should_be_1 = '%u' \n",trailer.statusposition);
    printf("/>\n\n");
    printf("</rob>\n\n");
  }else{
    
    temp = (unsigned int*)&trailer;
    printf("</module_hits>\n");
    printf("  <rod_trailer errorflag = 0x%08x, errorcount = 0x%08x, numstat = 0x%08x, wordcount = 0x%08x, statuspos = 0x%08x, checksum = 0x%08x>/>\n",
	   trailer.errorflag, trailer.errorcount, trailer.numstatus, trailer.wordcount, trailer.statusposition, trailer.checksum);
    printf("</rob>\n");
  }
}


void byte_to_bitstring(int byte, char* outchar, int len){
  char bitchar[2] = {'0','1'};
  int i;
  for(i=0; i<len;i++){
    outchar[len-1-i] = bitchar[(1 & (byte>>i))];
  }
  outchar[len] = 0;
}


char* decode_MCCerror(int byte){
  static char MCCERROR[256];
  int len = 0;
  
  sprintf(MCCERROR,"");
  if(byte & 1) sprintf(MCCERROR,"HITOVERFLOW|");
  len = strlen(MCCERROR);
  if(byte & 2) sprintf(&MCCERROR[len],"EOEOVERFLOW|"); 
  len = strlen(MCCERROR);
  if(byte & 4) sprintf(&MCCERROR[len],"L1CHKFAILFE|"); //L1ID check inside the corresponding receiver for FE failed
  len = strlen(MCCERROR);
  if(byte & 8) sprintf(&MCCERROR[len],"BCIDCHKFAIL|"); //BCID check btw. EoE words belonging to diff FE's failed
  len = strlen(MCCERROR);
  if(byte & 16) sprintf(&MCCERROR[len],"L1CHKFAILGLOBAL|"); //L1ID check btw. EoE words belonging to diff FE's failed
  
  return MCCERROR;
}

char* decode_ebits(int byte){
  static char MERROR[256];
  int len = 0;
  
  sprintf(MERROR,"");
  if(byte & 2) sprintf(MERROR,"b");
  len = strlen(MERROR);
  if(byte & 4) sprintf(&MERROR[len],"l");
  len = strlen(MERROR);
  if(byte & 8) sprintf(&MERROR[len],"t");
  len = strlen(MERROR);
  if(byte & 16) sprintf(&MERROR[len],"P");
  
  return MERROR;
}

char* decode_ebits_trail(int byte){
  static char MERROR[256];
  int len = 0;
  
  sprintf(MERROR,"");
  if(byte & 4) sprintf(&MERROR[len],"of");
  len = strlen(MERROR);
  if(byte & 8) sprintf(&MERROR[len],"htl");
  len = strlen(MERROR);
  if(byte & 16) sprintf(&MERROR[len],"tb");
  
  return MERROR;
}


char* decode_generic(int twobytes){
  static char GENERROR[256];
  int len = 0;
  sprintf(GENERROR,"");
  if(twobytes & 1) sprintf(GENERROR,"BCIDCHKFAIL!");
  len = strlen(GENERROR);
  if(twobytes & 2) sprintf(&GENERROR[len],"L1CHKFAIL!"); 
  len = strlen(GENERROR);
  if(twobytes & 4) sprintf(&GENERROR[len],"TIMEOUT!"); 
  len = strlen(GENERROR);
  if(twobytes & 8) sprintf(&GENERROR[len],"DATAINCORRECT!"); 
  len = strlen(GENERROR);
  if(twobytes & 16) sprintf(&GENERROR[len],"BUFFEROVERFLOW! ");
  return GENERROR;
}

char* decode_rob_specific(int twobytes){ // wrong, see specific
  static char GENERROR[256];
  int len = 0;
  sprintf(GENERROR,"");
  if(twobytes & 1) sprintf(GENERROR,"BCIDCHKFAIL!");
  len = strlen(GENERROR);
  if((twobytes>>1) & 1 ) sprintf(&GENERROR[len],"L1CHKFAIL!"); 
  len = strlen(GENERROR);
  if((twobytes>>2) & 1) sprintf(&GENERROR[len],"TIMEOUT!"); 
  len = strlen(GENERROR);
  if((twobytes>>3) & 1) sprintf(&GENERROR[len],"DATAINCORRECT!");  
  len = strlen(GENERROR);
  if((twobytes>>4) & 1) sprintf(&GENERROR[len],"BUFFEROVERFLOW! ");
  len = strlen(GENERROR);
  if((twobytes>>5) & 1) sprintf(&GENERROR[len],"BUFFEROVERFLOW! ");
  len = strlen(GENERROR);
  if((twobytes>>6) & 1) sprintf(&GENERROR[len],"BUFFEROVERFLOW! ");
  len = strlen(GENERROR);
  if((twobytes>>13) & 1) sprintf(&GENERROR[len],"NO EF for requested L1ID - generate an empty EF");
  return GENERROR;
}

void decode_data(unsigned int word, unsigned int rob_id, int event){
  char *mbytes;
  int i, mask;
  int FE, FEerror;
  unsigned int inword;
  
  static unsigned int oldrob_id  = 0;
  static unsigned int old_event  = 0;
  static unsigned int nhits  = 0;
  static unsigned int nlinks = 0;
  static unsigned int link_errors = 0;
  static unsigned int link   = 0;
  static unsigned int bcid = 0;
  static unsigned int l1id = 0;
  static unsigned int link_list[255];
  static unsigned int fei4b_flag = 0;
  static unsigned int number_of_no_data = 0;
  unsigned int tot = 0;
  unsigned int column = 0;
  unsigned int row = 0;
  static bool inmodule = false;
  
  std::string str = "";
  //str += Form("word = 0x%x", word);
  
  if((rob_id!=oldrob_id)||(event!=old_event)){
    str += Form(" <ROB_Summary ROB = '%x' links = '%d'>",oldrob_id, nlinks);
    for(i=0;i<nlinks;i++)str += Form(" == %x", link_list[i]);
    str += Form("</ ROB_Summary>\n\n");
    str += Form("<module_hits>\n");
    nhits  = 0;
    nlinks = 0;
    oldrob_id  = rob_id;
    old_event  = event;
  }
  
  inword = word;
  mbytes = (char*)&inword;
  //str += Form(", mbytes: %d %d %d %d\n", mbytes[0], mbytes[1], mbytes[2], mbytes[3]);
  
  // mask first four bits
  mask = (word & 0xF0000000) >> 28;
  //printf("mask = 0x%x\n", mask);
  
  if (1 /*number_of_no_data < 5*/){ //(sizeof(event_trailer)/sizeof()-1)) {
    if( mask == rHEAD ) {
      if((0==mbytes[0])&&(0==mbytes[2])&&(0x40==mbytes[1])){
	printf("Time out \n");
      }else{
	inmodule = true;
	
	link = mbytes[3] & 0x7;
	l1id = (mbytes[1] & 0x7c)>>2;
	bcid = mbytes[0]&0xff + ((mbytes[1] & 0x3)<<8);
	fei4b_flag = (mbytes[1] & 0x80)>>3;
	str += Form("  <module_head link = '%x' L1ID = '%u' BCID = '%u', FEI4B = '%u' />\n",
		    link, l1id, bcid, fei4b_flag);
	
	hmod_bcid->Fill( bcid );
	hdiff_rod_mod_bcid->Fill( (int)theRodBcid - (int)bcid );
	hdiff_eve_mod_bcid->Fill( (int)(theEveBcid&0xff) - (int)(bcid&0xff) );
	
	modid.insert( grod*100 + link );
	Int_t index = std::distance( modid.begin(), modid.find( grod*100+link ) );
	hdiff_rod_mod_bcid_by_mod->Fill( index, theRodBcid - bcid );

	Int_t index_rod = std::distance( rodid.begin(), rodid.find( grod ) );
	hdiff_rod_mod_bcid_by_rod->Fill( index_rod, theRodBcid - bcid, 1 );
	
	/*
	link = mbytes[2] & 0xff;
	str += Form("  <module link = '%x' L1ID = '%d' BCID = '%d'",
		    link, mbytes[1] & 0xff, mbytes[0]& 0xff);
	if( glink != link ) {
	  glink = (int)link;
	  
	  theModBcid = (int)(mbytes[0]& 0xff);
	  hmod_bcid->Fill( theModBcid );
	  hdiff_rod_mod_bcid->Fill( theRodBcid - theModBcid );
	  
	  modid.insert( grod*100 + glink );
	  Int_t index = std::distance( modid.begin(), modid.find( grod*100+glink ) );
	  hdiff_rod_mod_bcid_by_mod->Fill( index, theRodBcid - theModBcid, 1 );
	  
	  Int_t index_rod = std::distance( rodid.begin(), rodid.find( grod ) );
	  hdiff_rod_mod_bcid_by_rod->Fill( index_rod, theRodBcid - theModBcid, 1 );
	}
	
	nhits  = 0;
	link   = mbytes[2] & 0xff; 
	if(nlinks<255)link_list[nlinks] = link;
	*/
	
	nlinks++;
      }
      number_of_no_data =0;
      
    } else if ( mask == rTRAIL ) {
      link = (int)(mbytes[3] & 0x7);
      str += Form("  <module_trailer ROB = '%x' link ='%x' #hits = '%u' errors_header ='%s' errors_trailer = '%s' />\n",
		  rob_id, link, nhits, decode_ebits(link_errors), decode_ebits_trail(mbytes[3] & 0x1E));     
      str += Form("</module_hits>\n");
      number_of_no_data =0;
      inmodule = false;
      
    } else if( mask == rHIT || mask == rHIT+1) {
      
      link = (unsigned int)(mbytes[3] & 0x7);
      tot = (unsigned int)(mbytes[2]&0xff)>>4;
      column = (int)((mbytes[1] & 0xfe)>>1);
      row = (unsigned int)(mbytes[0]&0xff) + (unsigned int)((mbytes[1]&0x1)<<8);
      str += Form("    <hit link = '%u' Row = '%3u' Column ='%2u' TOT ='%2u' word = '0x%8x' />\n",
		  link, row, column, tot, word);
      
      hhit_tot->Fill( tot );
      nhits++;
      number_of_no_data =0;

    } else if( mask == rERROR ) {
      if ( 1 /*inmodule*/ ) {
	link    = (int)((mbytes[3] & 0xe) >>1) ;
	int service_code = (int)((mbytes[2] & 0x7e) >>1);
	int service_code_counter = (int)((mbytes[1]&0xff)<<8) + (int)(mbytes[0]&0xff);
	str += Form("  <error ROB ='%x' link ='%d' service_code = '0x%x' service_code_counter = '0x%x' />\n",rob_id, link, service_code, service_code_counter);
	number_of_no_data =0;
      }
      number_of_no_data++;
      
    } else {
      printf("WARNING: exception!! : 0x%x word = 0x%x\n", mask, word);
      number_of_no_data++;
      
    }
  }
  else number_of_no_data = 0;
  
  if( nhits==0 && inmodule ) {
    printf("%s", str.c_str());
  } else if( nhits>0 ) {
    printf("%s", str.c_str());
  }
}

#define CACHESIZE 16

event_trailer reorder_cache(unsigned int cache[CACHESIZE], int count){
  static unsigned int temp[CACHESIZE];
  event_trailer *trailer;
  
  int start = count - (sizeof(event_trailer)/sizeof(unsigned int)) - 1;
  for(int i = start;i<=count;i++)temp[i-start]=cache[i & 0xf];
  trailer = (event_trailer *)temp;
  
  return (*trailer);
}



int main(int argc, char **argv) {
  int len, chip, col, row, status, tot, count, i;
  int rodfragmentcount;
  unsigned int word;
  unsigned int temp;
  unsigned int cache[CACHESIZE];
  unsigned int rob_list[32];
  rod_header rdheader;
  generic_header rob_header, subd_header;
  full_event_header fullE_header;
  ros_header rosE_header;
  rob_status_elements rob_status;
  FILE *inputhandle;
  char mbytes[4];
  bool RODflag;
  
  int event = 0;
  int current_rob = 0;
  int current_rod = 0;
  int current_bcid = 0;
  int current_lvl1id = 0;
  int num_rob_summary   = 0;
  int num_links_summary = 0;
  int num_hits_summary  = 0;
  int ecrnum = 0;
  int timeref = 0;
  int eventtime = 0;
  bool verbose = true;
  
  hmod_bcid = new TH1F("hmod_bcid", ";BCID(Module);# Entries", 256, -0.5, 255.5);
  hrod_bcid = new TH1F("hrod_bcid", ";BCID(ROD);# Entries", 4096, -0.5, 4095.5);
  heve_bcid = new TH1F("heve_bcid", ";BCID(Event);# Entries", 4096, -0.5, 4095.5);
  hdiff_rod_mod_bcid = new TH1F("hdiff_rod_mod_bcid", ";BCID(ROD) - BCID(Module);# Entries", 8192, -4096.5, 4095.5);
  hdiff_eve_rod_bcid = new TH1F("hdiff_eve_rod_bcid", ";BCID(Event) - BCID(ROD);# Entries", 8192, -4096.5, 4095.5);
  hdiff_eve_mod_bcid = new TH1F("hdiff_eve_mod_bcid", ";BCID(Event) - BCID(Module);# Entries", 512, -256.5, 255.5);
  hdiff_eve_rod_bcid_by_rod = new TH2F("hdiff_eve_rod_bcid_by_rod", ";ROD index;BCID diff (eve-rod)", 132, 0, 132, 256, -128.5, 127.5);
  hdiff_eve_rod_bcid_by_rob = new TH2F("hdiff_eve_rod_bcid_by_rob", ";ROB index;BCID diff (eve-rod)", 132, 0, 132, 256, -128.5, 127.5);
  hdiff_rod_mod_bcid_by_mod = new TH2F("hdiff_rod_mod_bcid_by_mod", ";Module index;BCID diff (rod-mod)", 1744, 0, 1744, 256+30, -128.5, 127.5+30);
  hdiff_rod_mod_bcid_by_rod = new TH2F("hdiff_rod_mod_bcid_by_rod", ";ROD index;BCID diff (rod-mod)", 132, 0, 132, 256+30, -128.5, 127.5+30);
  
  hhit_tot = new TH1F("hhit_tot", ";ToT;Entries", 256, 0, 256);
  
  herrortype = new TH1F("herrortype", ";Error Type;# Entries", 32, 0, 32);
  
  if(argc < 2){
    printf("usage: argv[1] is filename.\n");
    return 1;
  }else{
    inputhandle  = fopen(argv[1], "r");
  }
  
  printf("<?xml version = '1.0' encoding='ASCII'?>\n");
  printf("<!DOCTYPE rosdata (event) [\n");
  printf("<!ELEMENT event (event_head, rob) >\n");
  printf("<!ELEMENT event_head EMPTY >\n");
  printf("<!ELEMENT rob (rob_head, rod_head, ROB_Summary, module_hits) >\n");
  printf("<!ELEMENT rob_head EMPTY >\n");
  printf("<!ELEMENT rod_head EMPTY >\n");
  printf("<!ELEMENT ROB_Summary EMPTY >\n");
  printf("<!ELEMENT module_hits (module, error) >\n");
  printf("<!ELEMENT module (hit) >\n");
  printf("<!ELEMENT error (#PCDATA) >\n");
  printf("<!ATTLIST event_summary num_rob CDATA #REQUIRED >\n");
  printf("<!ATTLIST event_head hader CDATA word_size CDATA Source_ID CDATA Event_time_sec DCATA Exact_event_time_nsec CDATA>\n");
  printf("]>\n\n\n");
  printf("<rosdata>\n");
  
  for(count=0;count<CACHESIZE;count++)cache[count] = 0;
  count = 0;
  rodfragmentcount = 0;
  RODflag = false;
  status = 0;
  bool pixelpart = false;
  empty_rod_fragment emptyfragment;
  bool emptyEF;
  unsigned int sub_detector;
  while(0!=(status = (fread(&word, 1, 4, inputhandle)))){
    //printf("while: word = 0x%08x\n",word);
    //    if((mBOF == (word & 0xFFFF0000))){
    //      if(0==(status = (fread(&word, 1, 4, inputhandle)))) break;
    
    cache[count++ & 0xf] = word;
    if(RODflag)rodfragmentcount++;
    switch(word){
    case mRODHEAD:
      if (pixelpart) {
	if (emptyEF) {
	  if(0==(status = (fread(&emptyfragment, 1, sizeof(empty_rod_fragment), inputhandle)))) break;
	  if (verbose) printf("Empty ROD EF generated ...\n");
	  emptyEF = false;
	  continue;
	}
	if(RODflag && verbose) decode_trailer(reorder_cache(cache, count), rodfragmentcount, event, eventtime, ecrnum, current_bcid, current_lvl1id, current_rod);
	if (verbose) {
	  //printf("==================================================\n");
	  printf("<rod_head \n");
	}
	if(0==(status = (fread(&word, 1, 4, inputhandle)))) break;
	if (verbose) { printf("  words_size = '%u'\n", word);}
	if(0==(status = (fread(&rdheader, 1, sizeof(rod_header), inputhandle)))) break;
	if (verbose) {
	  printf("  Format_version 0x%x \n", rdheader.formatversion);
	  printf("  RODSourceID 0x%x \n", rdheader.sourceID);
	  printf("  RunNR = '%u' \n", rdheader.RunNR);
	  printf("  Level1_ID = '%u' \n", rdheader.L1ID);
	  printf("  ROD_ECR_part_of_Level1_ID = '%u' \n", (rdheader.L1ID & 0xff000000) >> 24);
	  printf("  Level1_ID_low = '%u' \n", rdheader.L1ID & 0xf);
	  printf("  BCID = '%u' \n", rdheader.BCID);
	  printf("  BC_low_ID = '%u' \n", rdheader.BCID & 0xff);
	  printf("  Level1_type = '%u' \n", rdheader.L1Ttype);
	  printf("  Detector_type = '0x%x'\n  ROD_type = '0x%x' \n", rdheader.detectorEtype,(rdheader.detectorEtype>>16) & 0xff);
	  printf("/>\n\n");
	  
	  rodid.insert( (int)rdheader.sourceID );
	  grod = (int)rdheader.sourceID;
	  Int_t index = (Int_t)( std::distance(rodid.begin(), rodid.find( rdheader.sourceID ) ) );
	  Int_t index_rob = (Int_t)( std::distance(robid.begin(), robid.find( theROBID ) ) );
	  
	  theRodBcid = (int)rdheader.BCID;
	  hrod_bcid->Fill( theRodBcid );
	  hdiff_eve_rod_bcid->Fill( theEveBcid - theRodBcid );
	  hdiff_eve_rod_bcid_by_rod->Fill( index, theEveBcid - theRodBcid, 1 );
	  hdiff_eve_rod_bcid_by_rob->Fill( index_rob, theEveBcid - theRodBcid, 1 );
	}
	RODflag = true;
	rodfragmentcount = 0;
	current_rod = rdheader.sourceID;
	current_bcid = rdheader.BCID;
	ecrnum = (rdheader.L1ID & 0xff000000) >> 24;
	current_lvl1id =  rdheader.L1ID & 0xf;
	//if (current_rod!=current_rob) printf("  ID MISMATCH btw ROB %x and ROD %x \n", current_rob, current_rod);
	pixelpart = false;
      }
      break;
    case mROBHEAD:
      if(RODflag && verbose)decode_trailer(reorder_cache(cache, count),rodfragmentcount, event, eventtime, ecrnum, current_bcid, current_lvl1id, current_rod);
      if (verbose) {
	printf("<!-------------------------------------------------------->\n");
	printf("<rob>\n");
	printf("<rob_head\n %x ", word);
      }
      if(0==(status = (fread(&rob_header, 1, sizeof(generic_header), inputhandle)))) break;
      if (verbose) printf(" words_size = '%u'\n", rob_header.fragment_size);
      sub_detector = rob_header.sourceID >> 16;
      /*
      if ((sub_detector >>4) == 1) { pixelpart = true; std::cout << "In pixel part! " << std::hex << subd_header.sourceID <<std::endl;}
      else { pixelpart = false; std::cout << "Not in pixel part! " << std::hex << subd_header.sourceID <<std::endl;}
      */
      pixelpart = true;
      
      if (verbose) {
	printf("  Format_version = '0x%x' \n", rob_header.formatversion);
	printf("  ROBSource_ID = '0x%x' \n", rob_header.sourceID);
	theROBID = (Int_t)rob_header.sourceID;
	robid.insert( theROBID );
      }
      if(0==(status = (fread(&rob_status, 1, sizeof(rob_status_elements), inputhandle)))) break;
      if (((rob_status.error_bits & 0xffff0000) >> 29) & 1) emptyEF=true;
      if (verbose) {
	printf("  ROBIN_generic_error_bits = '%s' \n", decode_generic(rob_status.error_bits & 0x0000ffff));
	printf("  ROBIN_specific_error_bits = '%s' \n", decode_rob_specific((rob_status.error_bits & 0xffff0000) >> 16));
	printf("/>\n");
      }
      //printf("  CRC of the ROD fragment %u \n", rob_status.rodFragCRC); //on the ROL just before the given ROB fragment was requested 
      RODflag = false;
      rodfragmentcount = 0;
      current_rob = rob_header.sourceID;
      if(num_rob_summary<32)rob_list[num_rob_summary] = rob_header.sourceID;
      num_rob_summary++;
      break;
    case mEVENTHEAD:
      if(RODflag && verbose)decode_trailer(reorder_cache(cache, count),rodfragmentcount, event, eventtime, ecrnum, current_bcid, current_lvl1id, current_rod);
      //verbose = (event == 1942);
      if (verbose) {
	
	printf("<event>\n");
	printf("<event_summary num_rob = '%u' >\n  <ROBS_found>", num_rob_summary);
	for(i=0;i<num_rob_summary;i++)printf(" == %x", rob_list[i]);
	printf("</ROBS_found>\n</event_summary>\n");
	
	printf("<!-------------------------------------------------------->\n");
	printf("<event_head\n  header = '0x%x'\n", word);
      }
      if(0==(status = (fread(&fullE_header, 1, sizeof(full_event_header), inputhandle)))) break;
      if (verbose) {
	printf("  word_size = '%u' \n", fullE_header.fragment_size);
	//printf("  Format version 0x%x \n", fullE_header.formatversion);
	printf("  Source_ID = '0x%x' \n", fullE_header.sourceID);
	printf("  Event_time_sec = '%u' \n", fullE_header.bunchCrossingTimeSec);
	printf("  Exact_event_time_nsec = '%u' \n", fullE_header.bunchCrossingTimeNanoSec);
	printf("  Run_number '%u' \n", fullE_header.RunNR);
	printf("  Run_type '0x%u' \n", fullE_header.RunType);
	printf("  Extended_LvL1ID = '%u' \n", fullE_header.ExtL1ID);
	printf("  ECR_part_of_Level1_ID = '%u'\n", (fullE_header.ExtL1ID & 0xff000000) >> 24);
	printf("  Event_BCID = '%u' \n", fullE_header.BCID);
	printf("  EVENT_status_element_specific = '%u' \n", (fullE_header.status0 & 0xffff0000)>>16);
	printf("  EVENT_status_element_generic = '%u' \n", fullE_header.status0 & 0x0000ffff);
	printf("/>\n");
	
	theEveBcid = (int)fullE_header.BCID;
	heve_bcid->Fill( theEveBcid );
      }
      if (event == 0) timeref = fullE_header.bunchCrossingTimeSec;
      eventtime = fullE_header.bunchCrossingTimeSec-timeref;
      RODflag = false;
      rodfragmentcount = 0;
      num_rob_summary = 0;
      event++;
      //verbose = (event == 1943);
      break;
    default:
      if(mSTRANGEHEAD == (word & 0xFFFF0000)){
	if(RODflag && verbose)decode_trailer(reorder_cache(cache, count),rodfragmentcount, event, eventtime, ecrnum, current_bcid, current_lvl1id, current_rod);
	if (verbose) {
	  if( event>0 ) printf("</event>\n\n\n\n");
	  //if( event >=3 ) break;
	  printf("==================================================\n ");
	  printf("STRANGEHEADER %x \n", word);
	}
	RODflag = false;
	rodfragmentcount = 0;
      }else if((mEOF == (word & 0xFFFF0000))){
	//if (verbose) printf("==================================================\n ");
      }else{
	if(RODflag &&verbose)decode_data(word,current_rob, event);
      }
      break;
      
    }
    
    if( event >= 100 ) break;
  }
  fclose(inputhandle);
  if (verbose) {
    printf("==================================================\n ");
    printf("PROCESSED %x events\n", event);
    
    printf("</rosdata>\n");
  }
  
  TFile *ofile = new TFile("out.root", "recreate");
  hmod_bcid->Write();
  hrod_bcid->Write();
  heve_bcid->Write();
  hdiff_rod_mod_bcid->Write();
  hdiff_eve_rod_bcid->Write();
  hdiff_eve_mod_bcid->Write();
  
  hdiff_eve_rod_bcid_by_rod->Write();
  hdiff_eve_rod_bcid_by_rob->Write();
  
  hdiff_rod_mod_bcid_by_mod->Write();
  hdiff_rod_mod_bcid_by_rod->Write();
  
  hhit_tot->Write();
  
  herrortype->Write();
  ofile->Close();
  
  return 0;
}












