# Current release
   - !505 Removal of RAW_PATTERN
# tag-09-02-01_v04 2021/04/08

   - !502 Skip resetFPGA at the start of scan, update GNAM submodule to newer version 
   - !500 Setup DDC IS in IBL controller
   - !499 Moving from SerialPort to Fei3Proxy, due to empty histograms seen in the PIT
   - !498 Moving IBL optoScan from SerialPort to single Tx channels
   - !497 Fix latency bug 
   - !496 Removal of IS variables dependency 
   - !495 Console in user mode
   - !494 Bug reco at ECR
   - !493 Add PPC ALWAYS_BUILD external project option
   - !489 Scan/tuning updates
      - Implementation of generic DSP scan in FitServer 
      - Add of GDAC, IF, TDAC and FDAC scan steps variables on the PPC
      - Removal of LVL1 histos and FEbyFe scans in PixScan (kept in console)
      - Adding new fitMethods to PixFitFitter
      - New tuning procesing added: GDAC_TUNE_ITERATED, TDAC_TUNE_ITERATED
      - Clean-up in PixModuleGroup for the scanning part
      - Error is displayed in PixelInfr if scan results cannot be saved on disk
      - Bugfix: electing TuningPointsFile from Console
      - Bugfix: Console crash while displaying ToT histograms, TDAC_T histo visualisation
   - !490 Added script to dump rod connectivity to json 
   - !491 Updates from P1. Fix using alternative link for scans, minor cleanup of infr startup scripts, add ECR to rodpyutils
   - !484 CLOB prefetching in the connectivity loader 
   - !486 Ibl scan speedup, remove unnecesary memory copy
   - !483 Removal of old RootDB interface
   - !482 Ibl opto tune, representation change of phases for IBL on DB, setReadoutSpeed in DataTakingConfig
   - !481 Cosmetic changes to console and removal of lvl1histobinned option 

 # tag-09-02-01_v03 2021/01/20

   - !467 Removal of unused scan variables from the scan class 
   - !474 T0 scan, proper handling of strobe delays/strobe delay ranges in calibrationConsole/PPC, code duplication in scan analyses removed
   - !465 Initial commits for ConfMon 
   - !473 New Alti segments, remove LTP segments, minor oks cleanup  

# tag-09-02-01_v02 2020/12/11
 
   - !470 Compiler and repository fixes 
   - !468 Standalone scan executable
   - !471 DumpDisable and GetDefTags added to CMakeLists 
   - !463 Moved variables used by FitServer from ROOT file to IS and cleaned up supported mask steps
   - !464 Added moveMask to the Fei3Proxy, removal of strobePixelLaches for the mask step staging
   - !462 Moving signal handler variables to std::atomic in Applications under PixLib
   - !460 AutoRecover and AutoDisable cmd cleanup and adding help
   - !461 Resolve IP from ROD hostname in HCP 
   - !459 Use module proxy to set masksteps instead serial port 
   - !457 Reworked the queue structure in the PixFit to use unique_ptrs 
   - !456 Concurrency fixes in PixFitNet 
    
# tag-09-02-01_v01 2020/10/18

   - !451 Add GNAM2. SR1: GNAM2 in local data taking or standalone. P1: in local data taking and ATLAS (not in this commit) 
   - !455 Remove GNAM1 from local P1 segments
   - !454 P1 local segment & deployment script updates
   - !453 Adding half clock return and MCC reset to remove IO delay 
   - !450 upgrade to tdaq-09-02-01
   - !450 Removed RCE scan presets, commented out some unused function, fixed ConfObj constness bug 
   - !448 Enabling Tx before sending configuration in FE-I3 (already done in FE-I4)
   - !447 Refactoring of Scans and cleanup in PixModuleGroup
   - !446 Cleanup of PPCPixController and BarrelPixController
   - !445 Histogram cleanup  and fixes for the configuration handling (including PixCtrl minor changes)
   - !444 Scan emu & new boc emu
      - New BOC emulator methods with realistic occupancy
      - Implemented the use of the BOC emulator during scans:
   - !443 Fei3 pixel readback fix 
   - !441 Bugfixes in the newly implemented FE register implementation and refactoring of ChangeCfg 
   - !437 Using the global maps for the register name to register mapping in the PixLib FE objects 
   - !438 Introucing Catch for unit testing, first test cases for PixLib
      - Reimplemented register length feature for Fei3 and Fei4
      - Reincluded ChangeCfg executable
      - Bumped BocLib version to newest version
   - !401 
      - Strongly typed enums for the FEI3 and FEI4
      - Removed unused Fe configs
      - Register length no longer stored (follow-up needed for proper implementation)
      - HCP to get FEI4 cfg (pixel and global) from the ROD
      - Temporarily removed ChangeCfg executable from compilation
      
# tag-09-01-00 2020/08/31

   - !433 Update to tdaq-09-01-00, add ZPostInstall for P1
   - !434 Skip sendConfig in emulator mode 
      - Avoid misleading error (IDLE check in IBL) in the DAQSlice/PixelInfr
      - Speed up configure, since sent of the configuration is skipped
      - Added ERROR in case PIXEL_UP/PIXEL_DOWN is issued in emulator mode
   - !419 
      - cleanup up OKS file generation
      - cleanup PixModule class
      - add variant submodule in preparation of DB changes (ConfGroup, ConfigIO, ConfObj, etc.)
      - contains fixed from strict_aliasing branch
      - cleanup of unused code
   - !428
      - Changes to the FEI3 readback HCP
   - !427 
      - Possible to disable/recover several RODs/modules in parallel
      - Fixing some bugs in the autodisable logic
      
# tag-09-00-02 2020/07/29

   - !426 - SR1, Fix ethernet interface names in oks 
   - !425 - reenable reloadCfg for scans to write configuration
   - !424    
      - reinntroduce sending bocconfigure, do not hash when sending MccBandwidth register
      - add DbServer_monitor
   - !423 Updates from pit
      - publishAvgOccupancy from RCD
      - cleanup of daqslice oks
      -fix rod publishing for Histogramming
   - !422 Adding S-curve sigma and chi2 to output of GDAC tunings
   
# tag-09-00-01 2020/07/09

   - !410
      - Add possibility to run scans in parallel
      - Bugfix publishing FMTSTAT in controllers
   - !416 
      - Fix race condition at deallocation in the RCD (#54 )
      - Removal of PixMessages::publishMessages, using ERS_HERE instead
      - autoDisable removed from RCD::probe and moved to a separated thread
      - Improving autodisable in PixModuleGroup
      - Removal of system calls
      - New PixStoplessRemoval and PixRCController classes
      - Fixing mutex lock/unlock in PixActions
      - New Applications/binaries: PixelMTSReceiver and simpleVmeUart
      - ChangeCfg added to the CMakeList
      - Signal Handler added to all the applications (some modifications needed in some classes)
      - New ipc command to issue RECOVER (meant for Rod Recovery)
      - Few other minor fixes
   - !417 Compile calibration analysis plugins, serveral code improvements.
   - !414 Update boc submodule path
   - !405 Resetting of FPGAs at startScan execution 
   - !403 Fixing getenv calls in Console  
   - !412 Moving to single FitServer for entire SR1
   - !410 Putting ROOT in MT mode for the FitServer to get rid of race conditions when deleting ROOT objects
   - !409 Update CalibrationConsole/CalibrationDataView/MetaDataList.cxx
   - !408 Update CalibrationConsole/Visualiser/DACMapVisualisationKit.cxx
   - !406  WIP: Devel cleanup cmake
   - !402 Crate3 segments added in test-bed
   - !395 Fixing call of AutoDisOn and AutoRecover from Console
   - !400 Update of SR1 segments for test-bed

#  tag-09-00-00 2020/05/18

   - !399 Devel cleanup pit segments
   - !398 master linux -> devel_cleanup
   - !379 Merge master to master_linux
   - !397 Devel cleanup pit fixes
   - !396 Devel cleanup feature ppc
   - !394 Fixing autodisable, moving scripts away form PixLib/Examples, Added AutoDisOn...
   - !393 Devel cleanup pitupdates
   - !392 Devel cleanup submodule

#  tag-02-08-06 2020/05/05

   - !386 Bug fix for IblCppMon, FMTSTAT not properly displayed while QS<QS_MON.  
   - !385 Error is now displayed when TIM lost configuration, to be tested. 
   - !383 Fix for IBL emulator (busy when one FE is disabled) 
   - !379 Adapt Reconfig@ECR for linux, reduce verbosity of hcp
   - !381 Fix bug introduced in !366. Set rx mask in PPCppRodPixController

#  tag-02-08-06 2020/05/05

   - !366 Reconfig@ECR - reworked Fei4, new Fei3 for master registers
   - !359 Noise through histogrammer
   - !380(L) Fix IBL ip and updates from pit

#  tag-02-08-05 2020/04/22

   - !370 Release update changelog
   - !369 Merging changes in the PIT
     - Adding local PIT changes to IblCppMonitoringTask
     - Using alternative channels in 2 new problematic optical links
     - Changing logic to set the Tx/Rx delays and phases, now is done in setReadoutSpeed to avoid to be sent multiple times
     - Changes in PixBoc, problem found while creating a new ROD configuration object for IBL, ChangeCfg changed accordingly
   - !364 Adding forgotten line to DataTakingConfig. Bug-fix, the number of hits in emulator.
   - !362 add publishing of HT_0, HT_1 for IBL
   - !360 SR1 UpdateConn for IBL rods C3_S11 and C3_S18 
   - !358 Fix for IBL emulator (getting busy while single FE disabled)
   - !357 Migration to SYSTEST-V7 connectivity after OB replacement in C1_S17
   - !348 Adding PLL reset VME command 
   - !337 Removal of old PixControllers and associated functionality
   - !349 BugFix on PixModuleGroup, the RX delays and thresholds were not set
   - !345 Slimming the FE cfg hashing HCP
   - !344 Caching of registers for FEI4 scans
    
#  tag-02-08-04 2019/12/18

   - !343 Deletion of fitservers histos
   - !341 AbortScan HCP fixed 
      - to work for both pixel and IBL
   - !339 Vme uart fixes - Adding DbServer to speed up the loading of the connectivity
   - !340 Minor fixes
      - Removing the PlsrDelay from the Fei4GlobalCfg hash
      - Removed that during digital injection the threshold is set to high for Fei4
   - !337 Caching of global registers for PIX scans
   - !337 Removal of old PixRodControllers and associated functionality
   - !342 Vme uart fixes in the pit
   - !340 Minor fixes
      - Removed high threshold in scans for FEI4 during digital injections
      - Removed PlsrDelay from FEI4 global register hash
   - !315 VME UART reader 

#  tag-02-08-03 2019/11/29

   - !334 Abort scan fixed
   - !330 Final implementation of FE hashing
   - !327 update from pit
      - Reduce timout for GADC scan
      - Disable FE hashing temporairly
      - IBL master/slave ip update in PI1
   - !318 First version of FE hashing
   - !313 GADC for temperature

