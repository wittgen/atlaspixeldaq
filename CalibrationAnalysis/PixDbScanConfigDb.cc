#include "PixDbScanConfigDb.hh"
#include "ScanConfigDbInstance.hh"
#include "PixDbGlobalMutex.hh"
#include <ConfigWrapper/ConfigHandle.h>

namespace CAN {

  PixDbScanConfigDb::PixDbScanConfigDb() {
    Lock globalLock(PixDbGlobalMutex::mutex());
    try {
      m_configDb=std::unique_ptr<IConfigDb>( ScanConfigDbInstance::create() );
    }
    catch ( CAN::MissingConfig &err) {
      //      throw MRSException("CFGDB_no_scan_conf_db", MRS_FATAL, "CAN::PixDbScanConfigDb::ctor", err.getDescriptor() );
      throw MRSException(PixLib::PixMessages::FATAL, "CAN::PixDbScanConfigDb::ctor", err.getDescriptor() );
    }
  }

  PixDbScanConfigDb::~PixDbScanConfigDb() {}

  PixA::ConfigRef PixDbScanConfigDb::config(const std::string &name, const std::string &tag, unsigned int revision) {
    Lock globalLock(PixDbGlobalMutex::mutex());
    try {
      PixA::ConfigHandle handle = m_configDb->config( name,tag,revision);
      return handle.ref().createFullCopy();
    }
    catch ( CAN::MissingConfig &err) {
      //      throw MRSException("CFGDB_no_scan_conf_db", MRS_FATAL, "CAN::PixDbScanConfigDb::ctor", err.getDescriptor() );
      throw MRSException(PixLib::PixMessages::FATAL, "CAN::PixDbScanConfigDb::ctor", err.getDescriptor() );
    }
  }
    
    PixA::ConfigRef PixDbScanConfigDb::config_1(const std::string &name, const std::string &tag, unsigned int revision,std::string string ) {
        Lock globalLock(PixDbGlobalMutex::mutex());
        try {
            PixA::ConfigHandle handle = m_configDb->config_1( name,tag,revision, string);
            return handle.ref().createFullCopy();
        }
        catch ( CAN::MissingConfig &err) {
	  //throw MRSException("CFGDB_no_scan_conf_db", MRS_FATAL, "CAN::PixDbScanConfigDb::ctor",
          //                     err.getDescriptor() );
	  throw MRSException(PixLib::PixMessages::FATAL, "CAN::PixDbScanConfigDb::ctor", err.getDescriptor() );
        }
    }

}
