#include <iostream>
#include <string>
#include <vector>
#include <memory>

#include "DirIter.hh"

#include <cstring>

int main(int argc, const char **argv)
{
  std::string extension;
  std::vector<std::string> path_names;
  bool list_dirs=false;

  bool error=false;

  for (int arg_i=1; arg_i<argc; arg_i++) {
    if (strcmp(argv[arg_i],"-d")==0) {
      list_dirs=true;
    }
    else if (strcmp(argv[arg_i],"-e")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      extension=argv[++arg_i];
    }
    else if (argv[arg_i][0]!='-') {
      path_names.push_back(argv[arg_i]);
    }
    else {
      std::cerr << "ERROR Unknown argument : " << argv[arg_i] << std::endl;
      error =true;
    }
  }

  if (error) {
    std::cout << "USAGE " << argv[0] << " [-l] [-e extension] path ..." << std::endl;
    return -1;
  }

  for(std::vector<std::string>::const_iterator path_iter = path_names.begin();
      path_iter != path_names.end();
      path_iter++) {

    {
      CAN::FileInfo info(*path_iter);
      std::cout << " --- list : " << *path_iter << "( exists" << info.exists() << ", dir=" << info.isDir() << ")" <<std::endl;
    }

    std::auto_ptr<CAN::IDirIter> dir_iter;
    if (!list_dirs) {
      dir_iter  = std::auto_ptr<CAN::IDirIter>( CAN::DirLister::fileIterator(*path_iter, extension) );
    }
    else {
      dir_iter  = std::auto_ptr<CAN::IDirIter>( CAN::DirLister::directoryIterator(*path_iter) );
    }
    while (dir_iter->next()) {
      CAN::FileInfo info(dir_iter->pathName());

      std::cout << dir_iter->name() << "( exists" << info.exists() << ", dir=" << info.isDir() << ")" <<std::endl;
    }
  }
  return 0;
}
