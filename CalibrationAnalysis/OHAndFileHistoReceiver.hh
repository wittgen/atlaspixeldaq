#ifndef _CAN_OHAndFileHistoReceiver_h_
#define _CAN_OHAndFileHistoReceiver_h_

#include "IHistoReceiver.hh"

namespace CAN {
  class OHAndFileHistoReceiver : public IHistoReceiver
  {
  public:
    OHAndFileHistoReceiver(IHistoReceiver *first_attemp, IHistoReceiver *second_attemp)
      : m_firstHistoReceiver(first_attemp),
	m_secondHistoReceiver(second_attemp)
    {}

    unsigned int getHistograms(SerialNumber_t scan_serial_number,
			       const std::string &rod_name,
			       const std::string &conn_name,
			       const std::string &histo_name,
			       TopLevelHistoArray &array) {
      unsigned int n_histograms =0;

      IHistoReceiver *attempts[2]={m_firstHistoReceiver.get(), m_secondHistoReceiver.get() };

      for (unsigned int attempt_i=0; attempt_i<2 && n_histograms==0; attempt_i++) {
	try {
	  n_histograms = attempts[attempt_i]->getHistograms(scan_serial_number, rod_name, conn_name, histo_name, array);
	}
	catch (...) {
	}
      }
      return n_histograms;
    }

    std::vector<std::string>  availableHistograms(SerialNumber_t scan_serial_number,
						   const std::string &rod_name,
						   const std::string &conn_name){
      std::vector<std::string> histoNameList;
      IHistoReceiver *attempts[2]={m_firstHistoReceiver.get(), m_secondHistoReceiver.get() };
      
      for (unsigned int attempt_i=0; attempt_i<2 && histoNameList.size() == 0; attempt_i++) {
	
	try {
	  histoNameList = attempts[attempt_i]->availableHistograms(scan_serial_number, rod_name, conn_name);
	}
	catch (...) {
	}
	
      }
      
      return histoNameList;
    }


  private:
    std::unique_ptr<IHistoReceiver> m_firstHistoReceiver;
    std::unique_ptr<IHistoReceiver> m_secondHistoReceiver;
  };

}
#endif
