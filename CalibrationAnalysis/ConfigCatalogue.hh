#ifndef _CAN_ConfigCatalogue_h_
#define _CAN_ConfigCatalogue_h_

#include <string>
#include <cassert>
#include <memory>

// same definitions are done by seal and omni

#ifdef HAVE_NAMESPACES
#  undef HAVE_NAMESPACES
#endif
#ifdef HAVE_BOOL
#  undef HAVE_BOOL
#endif
#ifdef HAVE_DYNAMIC_CAST
#  undef HAVE_DYNAMIC_CAST
#endif

#ifdef CORAL_ONE
#  include <SealKernel/Context.h>
#endif

#include <RelationalAccess/ISessionProxy.h>

#include <ConfigWrapper/pixa_exception.h>
#include "ConfigCatalogueListener.hh"
#include "ConfigDescription.hh"

namespace CAN {

  /** Exception which gets thrown if a missing module, boc or scan config is accessed.
   */
  typedef PixA::ConfigException ConfigException;

  /** Exception which gets thrown if a config does not exist
   */
  class MissingConfigDescription : public ConfigException
  {
  public:
    MissingConfigDescription(const std::string &message) : ConfigException(message) {}
  };

  class MissingConfigKey : public ConfigException
  {
  public:
    MissingConfigKey(const std::string &message) : ConfigException(message){}
  };
  
  class CorruptDescription : public ConfigException
  {
  public:
    CorruptDescription(const std::string &message) : ConfigException(message) {}
  };

  class ConfigCatalogue {

  private:
    std::string m_connString;
    std::unique_ptr<coral::ISessionProxy> m_session;

    bool m_verbose;

    static const char *s_configKeyTable;
    static const char *s_configNameTable;
    static const char *s_configCatalogTable;
    static const char *s_configCommentTable;
    enum EColumns {kKey,
		   kName, kNameFk,                 // name -> foreign key
		   kClassName, kCategory, kLevel,  // type description table
		   kCommentColumnTagName, kCommentColumnRevision, kCommentColumnDate,kCommentColumnCommentText,  // config comment table
		   kNColumns, kNKeyColumns=kName, kNNameColumns=kClassName, kNDescriptionColumns=kCommentColumnTagName };
    static const char *s_columnNames[kNColumns];

    void transactionStartReadOnly();
    void transactionStartUpdate();
    void transactionCommit();

#ifdef CORAL_ONE
    void init(seal::Context &context, const std::string &connection_string, bool update);
#else
    void init(const std::string &connection_string, bool update);
#endif
    template <class T> bool updateField(const std::string &name, const std::string &field_name, T new_value);

    /** Get a new foreign key for a new config name.
     */
    long long updateKey();

  public:

    ConfigCatalogue(const std::string &connection_string, bool verbose=false, bool update=true);
    ~ConfigCatalogue();

    void printTables();
    void printTableDesc();
    void printTableContent();
    void createTables();
    void createAuxTables();
    void dropTables();

    void addConfigDescription(const std::string &name,
			      const std::string &class_name,
			      const std::string &category_name,
			      ConfigDescription::EConfigLevel level=ConfigDescription::kNormal);

    bool updateCategory(const std::string &name, std::string new_category) {
      return updateField<std::string>(name,s_columnNames[kCategory], new_category);
    }

    /** Add a comment to the particular configuration.
     * @param type_name the name of the configuration
     * @param tag_name a particular configuration tag or an empty string.
     * @param revision id of a particular revision or zero.
     * @param comment the comment to be added.
     */
    void addComment(const std::string &type_name, const std::string &tag_name, Revision_t revision, const std::string &comment);

    ConfigDescription getDescription(const std::string &name);

    void getDescription(const std::string &class_name, IDescriptionListener &listener, ConfigDescription::EConfigLevel max_level=ConfigDescription::kNormal);

    /** Get the comment for a particular data set.
     * @param type_name the name of configuration.
     * @param tag_name the name of a particular tag or an empty string.
     * @param revision a particular revision or zero.
     * @param listener a helper class which will be called for each matching comment.
     * @param do not return the comments for particular tags or revisions in case the tag or revision is unspecified but just the general comments.
     */
    void getComments(const std::string &type_name,
		     const std::string &tag_name,
		     Revision_t revision,
		     ICommentListener &listener,
		     bool top_level_only=false);

    /** Get the key associated to the given name.
     */
    long long getKey(const std::string &name);

    void VerboseOutput(bool verbose) {m_verbose = verbose;};

  };

} //namespace CAN

#endif
