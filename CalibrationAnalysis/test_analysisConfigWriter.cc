#include <ConfigWrapper/DbManager.h>
#include <ConfigWrapper/DbRef.h>
#include <ConfigWrapper/ConfObjUtil.h>
#include <sstream>
#include <memory>

#include "AnalysisFactory.hh"
#include "AnalysisInfo_t.hh"
#include "IAnalysisObject.hh"
#include "IAnalysisPerROD.hh"
#include "IClassification.hh"
#include "IPostProcessing.hh"

#include "IConfigDb.hh"
#include "DummyConfigDb.hh"
#include "PixDbConfigDb.hh"
#include "PixDbObjectConfigDb.hh"
#include "ObjectConfigDbInstance.hh"
#include <ConfigWrapper/PixConfigWrapper.h>

#include "exception.hh"
#include <iostream>

#include <time.h>
#include <unistd.h>

int main(int argc, char **argv)
{
  std::string conf_db_path;
  char * ctmp = getenv("CAN_CFG_PATH");
  ctmp == NULL ? conf_db_path = "" : conf_db_path = ctmp;

  std::string name[1];
  CAN::Revision_t rev[1]={0};
  std::string tag[1];

  bool read_only=false;

  for (int arg_i=1; arg_i<argc; arg_i++) {
    std::cout << argc << std::endl;
    std::cout << arg_i << " " << argv[arg_i] << std::endl;
    if (strcmp(argv[arg_i],"-d")==0 && arg_i+2<argc && argv[arg_i+1][0]!='-') {
      conf_db_path=argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-r")==0) {
      read_only=true;
    }
    else if (strcmp(argv[arg_i],"-a")==0 && arg_i+3<argc && argv[arg_i+1][0]!='-' && argv[arg_i+2][0]!='-') {
      name[0] = argv[++arg_i];
      tag[0] = argv[++arg_i];
      std::cout << " revision = " << argv[arg_i+1] << std::endl;
      rev[0] = atoi(argv[++arg_i]);
      std::cout << " ->  " << rev[0] << std::endl;
    }
    else {
      std::cerr << "ERROR ["<< argv[0] << ":main] unhandled argument " << arg_i << " : " << argv[arg_i]<< "." << std::endl;
    }
  }
  //  if (rev[0]==0) {
  //    rev[0] = time(NULL);
  //  }

  if (conf_db_path.empty() || name[0].empty() || tag[0].empty() || rev[0]==0) {
    std::cerr << "ERROR  ["<< argv[0] << ":main] usage :" << std::endl
	      << argv[0] << " -d conf-db-path -a name tag revision [-r]" << std::endl
	      << "-d conf-db-path       the path were to store the RootDb configuration files." << std::endl
	      << "-a name tag revision  the analysis name, the tag and the revision of the configuration." << std::endl;
    return -1;
  }
  if (conf_db_path.empty() || conf_db_path[conf_db_path.size()-1]!='/') {
    conf_db_path+='/';
  }

  // first create the analysis object
  CAN::ObjectInfo_t obj_info(name[0],tag[0],rev[0]);

  std::cout << "INFO [" << argv[0] << ":main] create analysis object : " 
	    << obj_info.name() << "/"
	    << obj_info.tag() << "/"
	    << obj_info.revision()
	    << std::endl;

  try {
    std::auto_ptr<CAN::IObjectConfigDb> db( (read_only ?
				       static_cast<CAN::IObjectConfigDb *>(new CAN::PixDbObjectConfigDb)
				       : static_cast<CAN::IObjectConfigDb *>(new CAN::DummyConfigDb  )));
    std::auto_ptr<CAN::IAnalysisObject> obj(CAN::AnalysisFactory::get()->create(0,obj_info, db.get()));
    if (!obj.get()) {
      std::cerr << "ERROR [" << argv[0] << ":main] Failed to create object."
		<< std::endl;
      return -1;
    }

    if (read_only) {
      obj->config().dump(std::cout);
      return 0;
    }

    {
    std::auto_ptr<CAN::IConfigDb> config_db( new CAN::PixDbConfigDb(conf_db_path));

    std::string class_name = "IAnalysisObject";
    if (dynamic_cast<CAN::IAnalysisPerROD *>(obj.get())) {
      class_name = CAN::ObjectConfigDbInstance::objectClassName(CAN::ObjectConfigDbInstance::kAnalysis);
    }
    else if (dynamic_cast<CAN::IClassification *>(obj.get())) {
      class_name = CAN::ObjectConfigDbInstance::objectClassName(CAN::ObjectConfigDbInstance::kClassification);
    }
    else if (dynamic_cast<CAN::IPostProcessing *>(obj.get())) {
      class_name = CAN::ObjectConfigDbInstance::objectClassName(CAN::ObjectConfigDbInstance::kPostProcessing);
    }
    else {
      std::cout << "ERROR object not of type : IAnalysisPerROD, IClassification, IPostProcessing." << std::endl;
    }

    PixA::ConfigHandle config_handle( PixA::PixConfigWrapper::wrap(obj->config()) );
    config_db->addConfig(class_name, obj_info.name(), obj_info.tag(), PixA::ConfigHandle(config_handle.ref().createFullCopy()) );

    std::cout << " --- created configuration for : " << obj_info.name() << " obj_info.tag() " << " / " << time(NULL) << std::endl;
    obj->config().dump(std::cout);
    }

    std::cout << "INFO [main:" << argv[0] << "] No error detected." << std::endl;

  }
  catch( CAN::MRSException &err) {
    std::cerr << "ERROR [" << argv[0] << ":main] Caught exception : "
	      << err.getDescriptor()
	      << std::endl;
    return -1;
  }
  catch( std::exception &err) {
    std::cerr << "ERROR [" << argv[0] << ":main] Caught exception : "
	      << err.what()
	      << std::endl;
    return -1;
  }
  catch( ... ) {
    std::cerr << "ERROR [" << argv[0] << ":main] Unknown exception . "
	      << std::endl;
    return -1;
  }

  return 0;
}
