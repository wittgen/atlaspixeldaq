#ifndef _CAN_ArchivingUnit_hh_
#define _CAN_ArchivingUnit_hh_

//#include <mrs/message.h>
#include "Flag.hh"
#include "Lock.hh"
#include "RingBuffer_t.hh"
#include "CanThread.hh"

#include "IArchivingUnit.hh"
#include "IResultStore.hh"
#include "LogLevel.hh"
#include <Common.hh>
#include "IsDictionary.hh"

namespace CAN {

  class IsDictionary;
  class ExtendedAnalysisResultList_t;

  class ArchivingUnit  : public IArchivingUnit, private CanThread
  {
  public:
    ArchivingUnit(//IPCPartition &mrs_partition,
		  IsDictionary &dictionary,
		  std::vector<IResultStore *> archiver,
		  ELogLevel log_level);

    ~ArchivingUnit();

    void setLogLevel(ELogLevel log_level) {
      m_logLevel=log_level;
    }

    void queueForArchiving(SerialNumber_t analysis_serial_number,
			   const std::string &rod_name,
			   AnalysisStatus::EStatus final_status,
			   const std::string &is_name,
			   ExtendedAnalysisResultList_t *analysis_results);

    bool isArchiving() {
      Lock lock(m_writeLock);
      return m_buffer.hasElement() | m_archiving;
    }

    void reset() { 
      m_reset=true;
      m_newResults.setFlag(); 
    } 


    void shutdownWait() {
      CanThread::shutdownWait();
    }

    void initiateShutdown() {
      CanThread::initiateShutdown();
    }
    
  protected:


    void wakeupForShutdown() {
      m_newResults.setFlag();
      m_freeSlots.setFlag();
    }

    void main();

    void archiveElements();

    void clearBuffer();

    static const std::string &componentName() {return s_componentName; }

    void setIsStatus(const std::string &is_name, AnalysisStatus::EStatus final_status);

  private:

    class ResultInfo_t {
    public:
      ResultInfo_t() 
	: m_serialNumber(0),
	  m_status(AnalysisStatus::kUnknown),
	  m_result(NULL)
      {}

      ResultInfo_t(SerialNumber_t analysis_serial_number,
		   const std::string &rod_name,
		   AnalysisStatus::EStatus final_status,
		   const std::string &is_name,
		   ExtendedAnalysisResultList_t *result)
	: m_serialNumber(analysis_serial_number),
	  m_rodName(rod_name),
	  m_status(final_status),
	  m_isName(is_name),
	  m_result(result)
      {}

      SerialNumber_t serialNumber() const    { return m_serialNumber; }
      const std::string &rodName() const     { return m_rodName; }
      AnalysisStatus::EStatus status() const { return m_status; }
      const std::string &isName() const      { return m_isName; }
      ExtendedAnalysisResultList_t *result() const { return m_result; } 

      operator bool() const { return m_serialNumber>0; }

    private:
    
      SerialNumber_t  m_serialNumber;
      std::string m_rodName;
      AnalysisStatus::EStatus m_status;
      std::string m_isName;
      ExtendedAnalysisResultList_t *m_result;
    };

    class Cleanup
    {
    public:

      void operator()(ResultInfo_t &a) {
	a=s_empty;
      }

    private:
      static ResultInfo_t s_empty;
    };


    typedef RingBuffer_t< ResultInfo_t, ResultInfo_t&, Cleanup > AnalysisResultRingBuffer_t;

    Mutex m_writeLock;
    AnalysisResultRingBuffer_t m_buffer;
    
    //    std::auto_ptr<MRSStream> m_out;
    IsDictionary *m_isDictionary;
    std::vector< IResultStore * > m_archiver;

    Flag m_freeSlots;
    Flag m_newResults;

    ELogLevel m_logLevel;

    bool m_archiving;
    bool m_reset;

    static std::string s_componentName;
  };

}

#endif
