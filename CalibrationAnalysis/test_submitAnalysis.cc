#include "MetaDataService.hh"
#include <PixMetaException.hh>

#include "AnalysisInfo_t.hh"

#include <ipc/partition.h>
#include <ipc/core.h>
//#include <SerialNumberServer.hh>
//#include <SerialNumberServer_ref.hh>

#include "Scheduler_ref.hh"

#include "exception.hh"

int main(int argc, char **argv)
{
  std::string partition_name;
  std::string name[3];
  CAN::Revision_t rev[3];
  std::string tag[3];
  CAN::ESerialNumberType serial_number_type=CAN::kScanNumber;

  CAN::SerialNumber_t serial_number(0);
  std::map<std::string, std::pair<CAN::ESerialNumberType, CAN::SerialNumber_t> > named_sources;
  bool error=false;
  for (int arg_i=1; arg_i<argc; arg_i++) {
    if (strcmp(argv[arg_i],"-p")==0 && arg_i+1<argc) {
      partition_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-h")==0 || strcmp(argv[arg_i],"--help")==0) {
      error=true;
    }
    else if (strcmp(argv[arg_i],"-a")==0 && arg_i+3<argc && argv[arg_i+1][0]!='-' && argv[arg_i+2][0]!='-') {
      name[0] = argv[++arg_i];
      tag[0] = argv[++arg_i];
      rev[0] = atoi(argv[++arg_i]);
    }
    else if (strcmp(argv[arg_i],"-c")==0 && arg_i+3<argc && argv[arg_i+1][0]!='-' && argv[arg_i+2][0]!='-') {
      name[1] = argv[++arg_i];
      tag[1] = argv[++arg_i];
      rev[1] = atoi(argv[++arg_i]);
    }
    else if (strcmp(argv[arg_i],"-x")==0 && arg_i+3<argc && argv[arg_i+1][0]!='-' && argv[arg_i+2][0]!='-') {
      name[2] = argv[++arg_i];
      tag[2] = argv[++arg_i];
      rev[2] = atoi(argv[++arg_i]);
    }
    else if (strcmp(argv[arg_i],"-s")==0 && arg_i+1<argc) {
      ++arg_i;
      if (   isdigit(argv[arg_i][0] ) 
	  || (strlen(argv[arg_i])>1 && isdigit((argv[arg_i][1])) && (argv[arg_i][0]=='S'||argv[arg_i][0]=='A')) ) {
	const char *number_ptr = argv[arg_i];
	if (*number_ptr=='S') {
	  serial_number_type=CAN::kScanNumber;
	  number_ptr++;
	}
	else if (*number_ptr=='A') {
	  serial_number_type=CAN::kAnalysisNumber;
	  number_ptr++;
	}
	serial_number = atoi(number_ptr);
      }
      else {
	const char *number_ptr = argv[arg_i];
	while (*number_ptr && *number_ptr!=':') number_ptr++;
	if (!*number_ptr) {
	  std::cerr << "ERROR ["<< argv[0] << ":main] expected e.g. number like 1001, S01001, A01001, or named source e.g. hvon:S01001 after -s but got : " << argv[arg_i]
		    << "." << std::endl;
	  error = true;
	}
	else {
	  const char *name_end = number_ptr;
	  std::string name(argv[arg_i],0,name_end-argv[arg_i]);
	  number_ptr++;
	  CAN::ESerialNumberType a_type = CAN::kScanNumber;
	  if (*number_ptr=='S') {
	    a_type=CAN::kScanNumber;
	    number_ptr++;
	  }
	  else if (*number_ptr=='A') {
	    a_type=CAN::kAnalysisNumber;
	    number_ptr++;
	  }
	  const char *number_ptr_start = number_ptr;
	  while (isdigit(*number_ptr)) number_ptr++;
	  if (*number_ptr || number_ptr_start == number_ptr) {
	    std::cerr << "ERROR ["<< argv[0] << ":main] expected e.g. number like 1001, S01001, A01001,  after the name" 
		      << name << " but got : " << name_end+1
		      << "." << std::endl;
	    error = true;
	  }
	  else {
	    CAN::SerialNumber_t a_serial_number = atoi(number_ptr_start);
	    if (a_serial_number==0) {
	      std::cerr << "ERROR ["<< argv[0] << ":main] Serial number is zero : " << argv[arg_i]
			<< "." << std::endl;
	      error = true;
	    }
	    else {
	      if (serial_number == 0) {
		serial_number = a_serial_number;
	      }
	      //CLA
	      std::pair<CAN::ESerialNumberType, CAN::SerialNumber_t> pairToInsert = std::make_pair(a_type, a_serial_number);
	      for(std::map<std::string, std::pair<CAN::ESerialNumberType, CAN::SerialNumber_t> >::const_iterator source_iter = named_sources.begin();
		  source_iter != named_sources.end(); source_iter++) {
		if(pairToInsert==source_iter->second) {
		  std::cerr << "ERROR   A scanID con only be added once!  "  << std::endl;
		  error = true;
		}
	      }
	      if(!error) //CLA
		named_sources.insert(std::make_pair(name,pairToInsert));
	    }
	  }
	}
      }
    }
    else {
      std::cerr << "ERROR ["<< argv[0] << ":main] unhandled argument " << arg_i << " : " << argv[arg_i]<< "." << std::endl;
      error = true;
    }
  }

  if(serial_number==0) {
    std::cout << "WARNING  The scanID was not set correctly, check number of input parameters." << std::endl;
    error=true;
  }
  
  std::vector<CAN::ObjectInfo_t> obj_list;
  for (unsigned int obj_i=0; obj_i<3; obj_i++) {
    if (name[obj_i].empty() || tag[obj_i].empty()) {
      error=true;
      break;
    }
    else {
      obj_list.push_back(CAN::ObjectInfo_t(name[obj_i],tag[obj_i],rev[obj_i]));
    }
  }

  if (error) {
    std::cerr << "usage: " << argv[0] << "-p partition -a analysis tag rev -c classification tag rev -x post-processing tag rev -s serial-number." << std::endl
	      << std::endl
	      << "\t the serial number can be :" << std::endl
	      << "\t * a number e.g. 1001," << std::endl
	      << "\t * a scan or analysis serial number e.g. S01001, A01001 etc. ," << std::endl
	      << "\t * a named scan or analysis serial number e.g. hvon:S01001, old:A01001 etc. ," << std::endl;
    return -1;
  }

  assert(obj_list.size()==3);

  CAN::SerialNumber_t scan_serial_number(0);
  if (serial_number_type==CAN::kAnalysisNumber) {
    // get the serial number of the referenced scan
    try {
      CAN::InfoPtr_t<CAN::AnalysisInfo_t> an_analysis_info = CAN::MetaDataService::instance()->getAnalysisInfo(serial_number) ;
      scan_serial_number =  an_analysis_info->scanSerialNumber();
    }
    catch (CAN::PixMetaException &err) {
      std::cerr << "ERROR [" << argv[0] << ":main] Caught exception : "<< err.what()  << std::endl;
      return EXIT_FAILURE;
    }
    catch (std::exception &err) {
      std::cerr << "ERROR [" << argv[0] << ":main] Caught exception : "<< err.what()  << std::endl;
      return EXIT_FAILURE;
    }
    catch (...) {
      std::cerr << "ERROR [" << argv[0] << ":main] Caught unknwon exception"  << std::endl;
      return EXIT_FAILURE;
    }
  }

  CAN::InfoPtr_t<CAN::AnalysisInfo_t> analysis_info(new CAN::AnalysisInfo_t(obj_list[0],
									    obj_list[1],
									    obj_list[2], 
									    (serial_number_type==CAN::kScanNumber ? serial_number : scan_serial_number),
									    (serial_number_type==CAN::kAnalysisNumber ? serial_number : 0),
									    CAN::kDebug,
									    "",
									    time(0)));

  for(std::map<std::string, std::pair<CAN::ESerialNumberType, CAN::SerialNumber_t> >::const_iterator source_iter = named_sources.begin();
      source_iter != named_sources.end();
      source_iter++) {
    analysis_info->addSource(source_iter->first, source_iter->second.first, source_iter->second.second);
    std::cout << "adding source " << source_iter->first << " with serial number " << source_iter->second.second << std::endl;
  }

  if (!analysis_info->check()) {
    analysis_info->Print();

    std::cerr << "ERROR [" << argv[0] << ":main] The analyis meta data failed the check. Are there named sources"
      " and is the master serial number part of the source list?"  << std::endl;
    return EXIT_FAILURE;
  }

  // START IPC

  // Start IPCCore
  IPCCore::init(argc, argv);

  // Create IPCPartition
  std::cout << "INFO [" << argv[0] << ":main] Get serial number from partition " << partition_name << std::endl;
  std::auto_ptr<IPCPartition> ipcPartition( new IPCPartition(partition_name.c_str()) );
  if (!ipcPartition->isValid()) {
    std::cerr << "ERROR [" << argv[0] << ":main] Not a valid partition " << partition_name << std::endl;
    return EXIT_FAILURE;
  }

  analysis_info->Print();

  bool scheduler_comm_error=false;
  try {

    CAN::Scheduler_ref manitu (*ipcPartition); // Should throw an exception if the scheduler does not exist in the 
                                               // given partition

    std::cout << "INFO [" << argv[0] << ":main] submit analysis : " << std::endl << "      "
	      << analysis_info->objectInfo(CAN::kAlg).name() << "/"
	      << analysis_info->objectInfo(CAN::kAlg).tag() << "/"
	      << analysis_info->objectInfo(CAN::kAlg).revision() << std::endl << "      "
	      << analysis_info->objectInfo(CAN::kClassification).name() << "/"
	      << analysis_info->objectInfo(CAN::kClassification).tag() << "/"
	      << analysis_info->objectInfo(CAN::kClassification).revision() << std::endl << "      "
	      << analysis_info->objectInfo(CAN::kPostProcessing).name() << "/"
	      << analysis_info->objectInfo(CAN::kPostProcessing).tag() << "/"
	      << analysis_info->objectInfo(CAN::kPostProcessing).revision() << std::endl << "      "
	      << " analysis of "
	      << analysis_info->scanSerialNumber()
	      << std::endl;

    CORBA::ULong jobs_waiting_for_data;
    CORBA::ULong jobs_waiting;
    CORBA::ULong jobs_running;
    CORBA::ULong empty_slots;

    manitu.getQueueStatus(jobs_waiting_for_data, jobs_waiting, jobs_running, empty_slots);

    if (jobs_running+empty_slots<=0) {
      std::cerr << "ERROR [" << argv[0] << ":main] Scheduler does not seem to offer any processing slots."  << std::endl;
    }
    else {
    CAN::AnalysisInfoData_t submit_analysis_data;
    CAN::copyAnalysisInfo(*analysis_info, submit_analysis_data);

      CAN::SerialNumber_t analysis_serial_number = CAN::MetaDataService::instance()->newSerialNumber(CAN::kAnalysisNumber);    
      CAN::MetaDataService::instance()->addInfo(analysis_serial_number, analysis_info);
      std::cerr << "INFO [" << argv[0] << ":main] Job got analysis serial number " << analysis_serial_number << "." << std::endl;

      manitu.submitAnalysis(analysis_serial_number, submit_analysis_data);
    }

  }
  catch (CAN::MRSException &err) {
    std::cerr << "ERROR [" << argv[0] << ":main] Caught exception " << err.getDescriptor() << std::endl;
    return EXIT_FAILURE;
  }
  catch (CORBA::TRANSIENT& err) {
    scheduler_comm_error=true;
  }
  catch (CORBA::COMM_FAILURE &err) {
    scheduler_comm_error=true;
  }
  catch (std::exception &err) {
    std::cerr << "ERROR [" << argv[0] << ":main] Caught standard exception " << err.what() << std::endl;
    return EXIT_FAILURE;
  }
  catch (...) {
    scheduler_comm_error=true;
  }

  if (scheduler_comm_error) {
    std::cerr << "ERROR [" << argv[0] << ":main] Failed to communicate with scheduler in partition " << partition_name << std::endl;
    return EXIT_FAILURE;
  }

  return 0;
}
