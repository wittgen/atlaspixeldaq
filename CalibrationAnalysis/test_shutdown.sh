#!/bin/sh

partition=test

# test -x $MATER/rat || (exit -1
# test -x $MATER/manitu || exit -1
test -n "$MATER" || ( echo "ERROR [$0] Environment variable MATER not set." >&2; false) || exit -1
test -x $MATER/kill_manitu || ( echo "ERROR [$0] kill_manitu not found. Check Your environment." >&2; false) || exit -1

export PATH=$MATER:$PATH
if (which ipc_ls >/dev/null 2>/dev/null && ipc_ls -p test | grep -q ${partition}_Manitu); then
  echo "INFO [$0] Send shutdown command."
  kill_manitu ${partition} &
  sleep 10
else
  echo "INFO [$0] No scheduler listed in the partition ${partition}."
fi

for i in ros01 control2 control4; do
  echo "INFO [$0] kill manitu and rats on $i"
  ssh -x $i killall manitu rat &
done

for i in ros01 control2 control4; do
  echo "INFO [$0] check for running manitu and rats on $i"
  ssh -x $i sh -c "ps aux | grep manitu\|rat" &
done

wait
echo "done."


