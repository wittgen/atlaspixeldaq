#ifndef _CAN_IAnalysisDataServer_hh_
#define _CAN_IAnalysisDataServer_hh_

#include <string>
#include <ConfigWrapper/PixDisableUtil.h>


namespace PixA {
  class ConnectivityRef;
}

namespace CAN {

  class IDataServer;

  /** Class which encapsulates a set of DataServers for one analysis and one ROD (allowing analyses which handle several scans).
   *  All Histogramms for one scan and ROD are handled by a dedicated DataServer.
   *  Since the connectivity has to be the same for all the scans of one analysis 
   *  it is handled here and not in the scan specific DataServers. 
   */
  class IAnalysisDataServer {
  public:
    virtual ~IAnalysisDataServer() {}

    /** All DataServers for one analysis (and ROD) are only saved here (not in MasterDataServer), to be accessed from the analyses
     *  thus they are deleted in the destructor (they are created by the MasterDataServer)
     */
    virtual void addDataServer(const std::string &scanName, IDataServer* dataServer) =0;

    // methods to retrieve a specific ScanDataServer
    virtual IDataServer* getDataServer(const std::string &scanName) = 0;

    virtual IDataServer* getDataServer() = 0; //default version for analysis with only one scan, return the first DataServer in the list

    // methods to retrieve the analysis specific connectivity (same for all scans)
    virtual const PixA::ConnectivityRef &connectivity() = 0;

    /**
     *  Check if given pp0 was enabled for all scans 
     */
    virtual bool enabled(PixA::Pp0List::const_iterator pp0) =0;

    /**
     *  Returns list of modules which where disbaled in at least one of the scans 
     */
    virtual PixA::DisabledList& getCombinedModuleDisabledList(const PixA::Pp0LocationBase &pp0) =0;  

    virtual void createCombinedModuleDisabledList(const PixA::RODLocationBase &rod_location) =0;
  };

}

#endif
