#include "Flag.hh"
#include <omnithread.h>
#include <string>
#include <iostream>
#include <deque>
#include <vector>
#include <map>

#include <utility>
#include <memory>

#include <unistd.h>

class B : public omni_thread 
{
public:
  B(const std::string &name) : m_name(name),m_abort(false),m_errors(0),m_counter(0) {
    start_undetached();
  }

  ~B() {
    std::cout << "INFO [B::dtor] destruct " << m_name  << "." << std::endl;
  }
  CAN::Flag &readyFlag() {return m_readyFlag;}

  const CAN::Flag &exitFlag() {return m_exitFlag;}

  void abort() {
    m_abort=true;
  }

  void shutdown() {
    abort();
    m_readyFlag.setFlag();
    m_exitFlag.wait();
    while (state()!=omni_thread::STATE_TERMINATED) {
      usleep(1000);
    }
  }

  void addElm(const std::string &thread, int elm) {
    CAN::Lock lock(m_queueLock);
    m_deque.push_back(std::make_pair(thread,elm));
    m_readyFlag.setFlag();
  }
  
  unsigned int nQueued() const {
    return m_deque.size();
  }

  const std::string &name() const {
    return m_name;
  }

  unsigned int errors() const { return m_errors;}

  unsigned int counter() const { return m_counter;}


private:
  void *run_undetached(void *) {
    while (!m_abort) {
      m_readyFlag.wait(m_abort);
      
      while (!m_deque.empty() && !m_abort) {
	std::pair<std::string,int> elm;
	{
	  CAN::Lock lock(m_queueLock);
	  elm = m_deque.front();
	  m_deque.pop_front();
	}
	std::map<std::string , int>::iterator iter=m_lastElm.find(elm.first);
	if (iter==m_lastElm.end()) {
	  iter=m_lastElm.insert(std::make_pair<std::string,int>((std::string)elm.first,-1)).first;
	}
	if (iter!=m_lastElm.end()) {
	  if (iter->second+1 != elm.second) {
	    m_errors++;
	  }
	  iter->second=elm.second;
	}
	else {
	  m_errors++;
	}
	m_counter++;
	//	std::cout << "INFO [B::run_detached] got from " << elm.first  << " " << elm.second << std::endl;
	//	usleep(1*1000);
      }
    }

    m_exitFlag.setFlag();
    return NULL;
  }

  CAN::Mutex m_queueLock;
  std::deque< std::pair<std::string,int> > m_deque;

  std::string m_name;
  bool m_abort;
  CAN::Flag m_readyFlag;
  CAN::Flag m_exitFlag;
  std::map<std::string, int> m_lastElm;
  unsigned int m_errors;
  unsigned int m_counter;
};

class A : public omni_thread
{
public:
  A(const std::string &name, B &b) : m_name(name), m_abort(false), m_b(&b),m_counter(0) {
    start_undetached();
  }

  ~A() {
    std::cout << "INFO [A::dtor] destruct " << m_name  << "." << std::endl;
  }

  void abort() {
    m_abort=true;
  }

  void shutdown() {
    m_abort=true;
    usleep(10);
    m_exitFlag.wait();
    while (state()!=omni_thread::STATE_TERMINATED) {
      usleep(1000);
    }
  }

  const std::string &name() const {
    return m_name;
  }

private:
  void *run_undetached(void *) {
    while (!m_abort) {
      m_b->addElm(m_name,m_counter++);
      usleep(1);
    }
    m_exitFlag.setFlag();
    return NULL;
  }

  std::string m_name;
  bool m_abort;
  B *m_b;
  CAN::Flag m_exitFlag;
  int m_counter;
  

};

int main() {
  std::cout << "INFO [main] start." << std::endl;
  B master("master");
  std::vector<std::shared_ptr<A> > list_a;
  list_a.push_back(std::shared_ptr<A>(new A("A1",master)));
  list_a.push_back(std::shared_ptr<A>(new A("A2",master)));
  list_a.push_back(std::shared_ptr<A>(new A("A3",master)));
  list_a.push_back(std::shared_ptr<A>(new A("A4",master)));
  list_a.push_back(std::shared_ptr<A>(new A("A5",master)));

  sleep(5);
  std::cout << "INFO [main] stop." << std::endl;
  for (std::vector<std::shared_ptr<A> >::iterator thread_iter=list_a.begin();
       thread_iter != list_a.end();
       thread_iter++) {
    (*thread_iter)->abort();
  }
  std::cout << "INFO [main] sent abort." << std::endl;
  for (std::vector<std::shared_ptr<A> >::iterator thread_iter=list_a.begin();
       thread_iter != list_a.end();
       thread_iter++) {
    if ((*thread_iter)->state() == omni_thread::STATE_RUNNING) {
      std::cout << "INFO [main] " << (*thread_iter)->name() << " is still running." << std::endl;
    }
  }
  std::cout << "INFO [main] sent shutdown." << std::endl;
  for (std::vector<std::shared_ptr<A> >::iterator thread_iter=list_a.begin();
       thread_iter != list_a.end();
       thread_iter++) {
    (*thread_iter)->shutdown();
  }
  for (std::vector<std::shared_ptr<A> >::iterator thread_iter=list_a.begin();
       thread_iter != list_a.end();
       thread_iter++) {
    if ((*thread_iter)->state() == omni_thread::STATE_RUNNING) {
      std::cout << "ERROR [main] " << (*thread_iter)->name() << " is still running. Should have aborted." << std::endl;
    }
  }
  list_a.clear();

  std::cout << "INFO [main] shutdown master." << std::endl;
  master.shutdown();
  std::cout << "INFO [main] still " << master.nQueued() << " elements in the queue. Processed " << master.counter() << "." << std::endl;

  if (master.state()==omni_thread::STATE_RUNNING) {
    std::cout << "ERROR [main] still running but I am already done. That is bad." << std::endl;
  }
  else {
    if (master.errors()!=0 ) {
      std::cout << "ERROR [main] " << master.errors() << " detected." << std::endl;
    }
    else {
      std::cout << "INFO [main] done." << std::endl;
    }
  }
  return 0;
}
