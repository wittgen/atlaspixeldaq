#ifndef _CAN_IsDictionary_hh_
#define _CAN_IsDictionary_hh_

#include <is/infodictionary.h>
#include "Lock.hh"

namespace CAN {

  class IsDictionary
  {
  public:
    IsDictionary(IPCPartition &is_partition) : m_dictionary(is_partition) {}

    ISInfoDictionary &dictionary() { return m_dictionary; }
    Mutex &mutex()                 { return m_dictionaryMutex; };

  private:
    Mutex             m_dictionaryMutex;
    ISInfoDictionary  m_dictionary;
  };

}

#endif
