#include "MasterDataServerCT.hh"

#include "sendMessage.hh"
#include "PixUtilities/PixMessages.h"
#include <iomanip>
#include <string>
#include <sstream>
#include "ExecutionUnit.hh"
#include "DataServerCT.hh"

#include "ScanConfigDb.hh"

#include <ipc/partition.h>
#include <is/inforeceiver.h>
#include <is/infoiterator.h>
#include <is/callbackinfo.h>
#include <is/exceptions.h>

#include "IHistoReceiver.hh"


namespace CAN {


  ISInfoString MasterDataServerCT::ScanData_t::s_test;

  void MasterDataServerCT::ScanData_t::statusChanged(ISCallbackInfo *info)
  {
    if( info->type() == s_test.type() ) {

      std::string rod_name( ScanIsStatus::extractRodName( info->name() ) );

      if (! rod_name.empty() ) {

	  std::map< PixA::RodLocationBase, std::shared_ptr<RodData_t> >::iterator rod_iter = findRod(rod_name);
	  if (rod_iter  != endRod() ) {

	    info->value( rod_iter->second->isScanStatusString() );

	    if ( rod_iter->second->updateScanStatus() ) {
	      master()->dataAvailable();
	    }
	  }
	}
      }
    }


  //  MasterDataServerCT::MasterDataServerCT(IPCPartition &mrs_partition, std::string path, std::string file)
  MasterDataServerCT::MasterDataServerCT(std::string path, std::string file)
    : m_shutdown(false),
      m_executionUnit(NULL),
      //m_out(new MRSStream(mrs_partition)),
      //      m_isListener(scan_partition),
      //m_scanPartition(&scan_partition),
      //m_isServerName(scan_is_server_name),
      //m_histoReceiver(histoReciever),
      m_path(path),
      m_file(file)
  {    
    //assert( m_histoReceiver.get() );
  }


  std::shared_ptr<IDataServer> MasterDataServerCT::createDataServer(SerialNumber_t analysis_serial_number,
								      const PixA::RodLocationBase &rod_conn_name,
								      SerialNumber_t scan_serial_number,
								      const InfoPtr_t<PixLib::ScanInfo_t> &scan_info,
								      const SerialNumber_t src_analysis_serial_number,
								      PixA::ConnectivityRef &conn)
  {
    //std::shared_ptr<IDataServer> data_server(new DataServer(scan_serial_number, scan_info, src_analysis_serial_number, conn)); //conn,rod_conn_name, *this;
    DataServerCT *dataServer(new DataServerCT( conn, rod_conn_name, this ));

    dataServer->setInputDir( m_path ); 
    dataServer->addFile( m_file );

    return std::shared_ptr<IDataServer>( dataServer );
  }

  void MasterDataServerCT::subscribe(SerialNumber_t scan_serial_number)
  {
  }

  bool MasterDataServerCT::RodData_t::updateScanStatus()
  {
    ScanIsStatus::EStatus old_status = m_scanStatus;

    m_scanStatus = ScanIsStatus::status( m_infoScanStatusString.getValue() );
    return  (old_status < ScanIsStatus::kSuccess && m_scanStatus >= ScanIsStatus::kSuccess);
  }

    
  void MasterDataServerCT::unsubscribe(SerialNumber_t scan_serial_number)
  {
    std::stringstream is_status_pattern;
    is_status_pattern << "SCT_" << scan_serial_number << "/.*" << "/" << "STATUS";

    isReceiver().unsubscribe(m_isServerName,ScanData_t::s_test.type() && is_status_pattern.str() );
  }


  void MasterDataServerCT::start(IExecutionUnit &execution_unit)
  {
    m_executionUnit=&execution_unit;
    //m_isListener.listen();
    start_undetached();
  }

  void *MasterDataServerCT::run_undetached(void *) 
  {
      assert(m_executionUnit);

      //      sendMessage(*m_out,"DATASRV_term",MRS_INFORMATION, "MasterDataServerCT::run_detached", "..");
      ers::info(PixLib::pix::daq (ERS_HERE, "MasterDataServerCT::run_detached", ".."));

      m_exit.setFlag();
      return NULL;
  }

  void MasterDataServerCT::addDataServer(SerialNumber_t analysis_serial_number,
					 const PixA::RodLocationBase &rod_conn_name,
					 const std::shared_ptr<IDataServer>  &data_server )
  {
    // only a data server which was created by the master should be added to the master
    // and the master will create IDataServer of type DataServer
    // @sa createDataServer
    DataServerCT *casted_data_server = dynamic_cast<DataServerCT *>(data_server.get() );
    assert(data_server);
    casted_data_server->closeRequests();

    
    // TO BE IMPLEMENTED CORRECTLY !!!!!!!!!!!!!!!
    m_executionUnit->dataAvailable(serial_number_iter->first, rod_iter->first); //when read

    std::stringstream message;
    message << "Got all data  for A" << std::setw(9) << std::setfill('0') << serial_number_iter->first << rod_iter->first
	    << ".";
    //    sendMessage(*m_out,"DATASRV_data_avail",MRS_INFORMATION, "MasterDataServerCT::run_detached", message.str());
    ers::info(PixLib::pix::daq (ERS_HERE,"MasterDataServerCT::run_detached", message.str()));

    m_dataRequest.setFlag();
  }

}
