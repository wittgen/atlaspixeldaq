#include <memory>
#include "ScanResultListener.hh"

std::map< std::string, int> ScanResultListener::s_mhistoNameToType;


int ScanResultListener::getHistoType(const std::string &histo_name)
{
  if (s_mhistoNameToType.empty()) initHistoNameTypeList();
  std::map< std::string, int >::const_iterator iter = s_mhistoNameToType.find(histo_name);
  if (iter != s_mhistoNameToType.end()) {
    return iter->second;
  }
  return -1;
}


void ScanResultListener::initHistoNameTypeList() {
  //  if (s_histoTypeToName.empty()) {
  s_mhistoNameToType.clear();
  std::unique_ptr<PixLib::PixScan> ps(new PixLib::PixScan());
  s_mhistoNameToType = ps->getHistoTypes();
  //  }
}


void ScanResultListener::newHisto(const std::string &name, const PixA::HistoHandle &histo_handle, const std::string &rod_name, const PixA::ConfigHandle &scan_config) {
  PixA::HistoHandle a_histo_handle(histo_handle);
  Cout("newHisto] module="); //??
  Cout(" histo=\""+name+"\"");
  PixA::HistoRef histo(a_histo_handle.ref());
  HistoInfo_t info;
  histo.numberOfHistos(name, info);
  if(debugLevel<=INFO) std::cout << "idx:";
  std::stringstream out;
  for (int i=0; i<3; i++) {
    out << info.maxIndex(i) << (i<2 ? "," : "");
  }
  out << " n histos=" << info.minHistoIndex() << " - " << info.maxHistoIndex();
  Cout(out.str());

  //CLA   need to collect also theese !!
}

void ScanResultListener::newPixScanHisto(const std::string &histo_name, const PixA::ScanConfigRef &scan_config, const PixA::HistoHandle &histo_handle) {
  //  if(m_found_pp0) m_found_lables.push_back(histo_name);
  int histo_type = getHistoType(histo_name);

  m_conn = scan_config.connectivity();

  PixA::HistoHandle * a_histo_handle = new PixA::HistoHandle(histo_handle);
  PixA::PixDbDataContainer container(scan_config, histo_handle);

  Cout("newPixScanHisto] module=",false);
  std::stringstream modName;
  if (!scan_config.connName().empty()) {
    modName << scan_config.connName() << "(" << scan_config.prodName() << ")";
  }
  else {
    modName << scan_config.prodName();
  }
  modName << " histo=\"" << histo_name << "\"";
  Cout(modName.str());
  PixA::HistoRef histo(a_histo_handle->ref());
  HistoInfo_t info;
  //    histo.numberOfHistos(histo_name, info); //CLA: NO this works only for Histos not for PixScanHistos !
  container.numberOfHistos(histo_type,info);
  std::stringstream out;
  out << "idx:";
  for (int i=0; i<3; i++) {
    out << info.maxIndex(i) << (i<2 ? "," : "");
  }
  out << " n histos=" << info.minHistoIndex() << " - " << info.maxHistoIndex();
  Cout(out.str());

  std::string speed = confVal<std::string>(scan_config.pixScanConfig()["mcc"]["mccBandwidth"]);
  Cout(" SPEED= "+speed);
  
  std::stringstream out2;
  for (unsigned int loop_i=0; loop_i<3; loop_i++) {
    out2 << loop_i << " : " << container.getScanPar(loop_i) << " :: "
	 << container.getScanStart(loop_i) << " - " << container.getScanStop(loop_i) << " / " << container.getScanSteps(loop_i) << "   ";     
  }
  Cout(out2.str());
  
  //std::cout << " Boc link RX " << scan_config.inOrderBocLinkRx(0) << " " << scan_config.inOrderBocLinkRx(1) << std::endl; //gives error

//   std::map< int, std::list<PixA::HistoHandle*> >::iterator hlit = m_foundHistos.find(histo_type);
//   if( hlit == m_foundHistos.end() ) {    
//     std::list<PixA::HistoHandle*> *hlist = new std::list<PixA::HistoHandle*>;
//     m_foundHistos[histo_type]= *hlist;
//     std::list<std::string> *mlist = new std::list<std::string>;
//     m_foundModuleNames[histo_type]= *mlist;
//     Cout("starting new list of type "+histo_type);
//   }
//   m_foundHistos[histo_type].push_back( a_histo_handle );
//   m_scanConfigRefs[histo_type].push_back( new PixA::ScanConfigRef(scan_config));
//   m_foundModuleNames[histo_type].push_back( modName.str() );
//   if(find(m_pp0Names.begin(), m_pp0Names.end(), m_currentpp0Name)==m_pp0Names.end()) {
//     m_pp0Names.push_back( m_currentpp0Name );
//     std::cout << ">>>>>>>>>>>>> adding pp0 name " << m_currentpp0Name << std::endl;
//   }

  m_histos.push_back( *new MHisto(a_histo_handle,m_currentpp0Name,modName.str(),histo_type,new PixA::ScanConfigRef(scan_config)));
}

/*
  void ModTreeBuilder::newPixScanHisto(const std::string   &histo_name,
				       const PixA::ScanConfigRef &scan_config,
				       const PixA::HistoHandle   &histo_handle)
{

  //  if (m_matchingKits.empty() || m_lastHistoName != histo_name) {
  //    VisualisationFactory::getKits(histo_name, m_folderList, m_matchingKits);
  //    m_lastHistoName = histo_name;
  //  }

  PixScanHistoTypeList_t::const_iterator histo_type_iter = s_pixScanHistoTypeList.find(histo_name);
  if (histo_type_iter == s_pixScanHistoTypeList.end()) {
    std::stringstream message;
    message << "FATAL [ModTreeBuilder::newPixScanHisto] Not a known pix scan histo type \"" << histo_name << "\".";

    std::cout << message.str();
    for (PixScanHistoTypeList_t::const_iterator iter=s_pixScanHistoTypeList.begin();
	 iter!=s_pixScanHistoTypeList.end();
	 iter++) {
      std::cout << iter->first << std::endl;
    }

    throw PixLib::PixDBException(message.str());
  }

  DataContainerList_t::iterator container_iter  = m_dataContainer.find(scan_config.prodName());
  if (container_iter == m_dataContainer.end()) {
    std::pair<DataContainerList_t::iterator,bool>
      ret = m_dataContainer.insert(std::make_pair(scan_config.prodName(),
						  new PixA::PixDbDataContainer(scan_config,
									       histo_handle)));

    if (!ret.second) {
      std::stringstream message;
      message << "FATAL [ModTreeBuilder::newPixScanHisto] Failed to store data container for module " << scan_config.prodName() << ".";
      throw PixLib::PixDBException(message.str());
    }
    container_iter = ret.first;
  }

  assert( m_currentGroup );

  if (!m_currentPp0) {
    try {
      PixA::ConfigRef mod_config = scan_config.modConfig();
      int stave_id = confVal<int>(mod_config["geometry"]["staveID"]);
      std::stringstream pp0_name;
      pp0_name << stave_id;
      ModItem *stave_item=find(m_currentGroup, pp0_name.str());
      if (!stave_item) {
	stave_item = new ModItem(m_currentGroup, QString::number(stave_id), 0, STVITEM);
	stave_item->setOpen(true);
	m_currentPp0 = stave_item;
      }
    }
    catch (...) {
    }
  }

  PixA::IVisualiser *a_visualiser = NULL;
  if (m_currentPp0) {

    // if there is a PP0 (or stave) then the histograms of all modules are assambled in a list
    
    Pp0HistoList_t::iterator histo_list = m_pp0HistoList.find(histo_name);
    if ( histo_list == m_pp0HistoList.end()) {
      std::pair<Pp0HistoList_t::iterator,bool> ret = m_pp0HistoList.insert(std::make_pair(histo_name,Pp0HistoInfo_t()));
      if (!ret.second) {
	std::cerr << "ERROR [ModTreeBuilder::pixScanHisto] failed to create histo list for PP0 " << m_currentPp0->text(0).latin1() << "." << std::endl;
      }
      else {
	PixA::VisualisationFactory::getKits(histo_name, m_folderList, ret.first->second.visualisationKits());
      }
      histo_list = ret.first;
    }
    if ( histo_list != m_pp0HistoList.end()) {
      histo_list->second.add(scan_config,histo_handle); //CLA here

      a_visualiser = PixA::VisualisationFactory::visualiser(histo_list->second.visualisationKits(),
 							    histo_list->first,
 							    m_folderList,
 							    scan_config,
 							    histo_handle);

    }
  }
  else {

    // if there is no PP0 (or stave) then the kits have to be search for each module
    PixA::VisualisationKitSubSet_t a_kit_set;
    PixA::VisualisationFactory::getKits(histo_name, m_folderList, a_kit_set);
    a_visualiser = PixA::VisualisationFactory::visualiser(a_kit_set,
							  histo_name,
							  m_folderList,
							  scan_config,
							  histo_handle);
  }



  //    std::string conn_name  = confVal<std::string>(mod_config["geometry"]["connName"]);
  std::stringstream full_name;
  if (!scan_config.connName().empty()) {
    full_name << scan_config.connName() << " (" << scan_config.prodName() << ")";
  }
  else {
    full_name << scan_config.prodName();
  }

  ModItem *parent_item = ( m_currentPp0 ? m_currentPp0  : m_currentGroup);
  ModItem *mod_item=find(parent_item, full_name.str());
  if (!mod_item) {
    mod_item = new ModItem(parent_item,full_name.str().c_str(), 0, MODITEM);
    // mod_item->setOpen(true);
  }

  std::pair<ModItem *,bool> ret = createFolderItems(mod_item);
  mod_item = ret.first;

  if (ret.second) {
    ModItem *plotit = new ModItem(mod_item,  "Display config", container_iter->second,NONE);
    ModItem *subplt;

    if (scan_config.hasPixScanConfig()) {
      PixA::IVisualiser *cfg_visualiser = PixA::VisualisationFactory::visualiser(m_configDisplayKits,
										 "Scan_cfg",
										 m_folderList,
										 scan_config,
										 histo_handle);
      if (cfg_visualiser) new VisualisableItem(plotit, "Display Scan Cfg.", cfg_visualiser);
    }

    if (scan_config.hasModConfig()) {
      PixA::IVisualiser *cfg_visualiser = PixA::VisualisationFactory::visualiser(m_configDisplayKits,
										 "Module_cfg",
										 m_folderList,
										 scan_config,
										 histo_handle);
      if (cfg_visualiser) new VisualisableItem(plotit, "Display Module Cfg.", cfg_visualiser);
    }

    if (scan_config.hasBocConfig()) {
      PixA::IVisualiser *cfg_visualiser = PixA::VisualisationFactory::visualiser(m_configDisplayKits,
										 "Boc_cfg",
										 m_folderList,
										 scan_config,
										 histo_handle);
      if (cfg_visualiser) new VisualisableItem(plotit, "Display BOC Cfg.", cfg_visualiser);
    }

    //    subplt = new ModItem(plotit, "Display scan cfg", container_iter->second,PSCONFIG);
    //    subplt = new ModItem(plotit, "Display module cfg", container_iter->second,PMCONFIG);
    //    subplt = new ModItem(plotit, "Display BOC cfg", container_iter->second,PBCONFIG);

    PixA::IVisualiser *tdac_map_visualiser = PixA::VisualisationFactory::visualiser(m_DACMapKits,
									      "TDAC",
									      m_folderList,
									      scan_config,
									      histo_handle);

    PixA::IVisualiser *fdac_map_visualiser = PixA::VisualisationFactory::visualiser(m_DACMapKits,
									      "FDAC",
									      m_folderList,
									      scan_config,
									      histo_handle);

    if (tdac_map_visualiser) {
      new VisualisableItem(plotit, "Plot TDACs", tdac_map_visualiser);
    }
    else {
      subplt = new ModItem(plotit, "Plot TDACs", container_iter->second,TDACLOG);
    }

    if (fdac_map_visualiser) {
      new VisualisableItem(plotit, "Plot FDACs", fdac_map_visualiser);
    }
    else {
      subplt = new ModItem(plotit, "Plot FDACs", container_iter->second,FDACLOG);
    }
  }
  if (a_visualiser) {
    new VisualisableItem( mod_item , std::string("Plot "+histo_name).c_str(), a_visualiser );
  }
  else {
    new ModItem(mod_item,std::string("Plot "+histo_name).c_str(),container_iter->second,(PIXDBTYPES+histo_type_iter->second));
  }

}
*/


void ScanResultListener::newFolder(const std::vector<std::string> &folder_hierarchy) {
  std::stringstream out;
  out << "newFolder] path=\"";
  for (std::vector<std::string>::const_iterator folder_iter = folder_hierarchy.begin();
       folder_iter!=folder_hierarchy.end();
       folder_iter++) {
    out << *folder_iter << "/";
  }
  out << "\"";
  Cout(out.str());
}

void ScanResultListener::newLabel(const std::string &name, const PixA::ConfigHandle &label_config) {
  //  if(m_found_pp0) m_found_lables.push_back(name); //does not get called by streamer
  std::string comment;
  std::string time_stamp;
  unsigned int scan_number=0;

  m_conn = PixA::ConnectivityRef();

  if (label_config) {
    PixA::ConfigRef label_config_ref = label_config.ref();
    try {
      comment = confVal<std::string>(label_config_ref[""]["Comment"]);
      scan_number = confVal<unsigned int>(label_config_ref[""]["ScanNumber"]);
      time_stamp = confVal<std::string>(label_config_ref[""]["TimeStamp"]);
      
    }
    catch (PixA::ConfigException &err)
      {
        // catch exceptions which are thrown if a field does not exist e.g. ScanNumber
     }
  }
  std::stringstream out;
  out << "newLabel] ";
  if (scan_number>0) {
    out << scan_number;
  }
  out << " : \"" << name << "\"";
  if (!time_stamp.empty()) {
    out << "  - " << time_stamp;
  }
  if (!comment.empty()) {
    out << "  :: " << comment << "";
  }
  Cout(out.str());
}
