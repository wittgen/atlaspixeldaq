#include "DummyDataServer.hh"

#include "ExecutionUnit.hh"

namespace CAN {

  void DummyDataServer::thatIsAll() { m_gotAllRequests=true; m_master->setDataRequest(); m_master=nullptr;}

  DummyDataServer::~DummyDataServer() {
    std::cout << "INFO [DummyDataServer::dtor] destructed data server for ROD " << m_rodName << "." << std::endl;
  }

  DummyMasterDataServer::DummyMasterDataServer()//IPCPartition &mrs_partition) 
    : m_shutdown(false),
      m_executionUnit(nullptr)
      //      m_out(new MRSStream(mrs_partition))

  {}

  void DummyMasterDataServer::start(IExecutionUnit &execution_unit)
  {
    m_executionUnit=&execution_unit;
    start_undetached();
  }


  void *DummyMasterDataServer::run_undetached(void *) 
  {
      assert(m_executionUnit);

//       sendMessage(*m_out,"DATASRV_start",MRS_INFORMATION, "DummyDataServer::run_detached", "Start data retrieval.");
      ers::info(PixLib::pix::daq (ERS_HERE, "DummyDataServer::run_detached", "Start data retrieval."));

      std::map<SerialNumber_t, DataServerList_t >::iterator serial_number_iter;
      {
	Lock lock( m_dataServerListMutex );
	serial_number_iter = m_dataServerList.begin();
      }

      for (;!m_shutdown;) {

	if (m_dataServerList.empty()) {
// 	  sendMessage(*m_out,"DATASRV_doozing",MRS_INFORMATION, "DummyDataServer::run_detached", "Nothing to do.");
	  ers::info(PixLib::pix::daq (ERS_HERE, "DummyDataServer::run_detached", "Nothing to do."));
	  m_dataRequest.wait(m_shutdown);
	  if (m_shutdown) break;

	}

	DataServerList_t::iterator rod_iter;
	DataServerList_t::iterator rod_iter_end;
	{
	  Lock lock( m_dataServerListMutex );

	  serial_number_iter = m_dataServerList.begin();
	  while (serial_number_iter != m_dataServerList.end() && serial_number_iter->second.empty() ) {
	    std::map<SerialNumber_t, DataServerList_t >::iterator first_iter=serial_number_iter;
	    while (serial_number_iter != m_dataServerList.end() && serial_number_iter->second.empty() ) {
	      serial_number_iter++;
	    }
	    if (serial_number_iter != first_iter) {
	      m_dataServerList.erase(first_iter,serial_number_iter);
	    }
	    serial_number_iter = m_dataServerList.begin();
	  }

	  if (serial_number_iter == m_dataServerList.end() ) continue;

	  rod_iter_end=serial_number_iter->second.end();
	  for (rod_iter = serial_number_iter->second.begin();
	       rod_iter != rod_iter_end;
	       rod_iter++) {
	    break;
	    //	    if (rod_iter->second->gotAllRequests()) break;
	  }
	}

	if (rod_iter != rod_iter_end) {
	  // request histograms

	  // when done
	  m_executionUnit->dataAvailable(serial_number_iter->first, rod_iter->first);

	  std::stringstream message;
	  message << "Got all data  for A" << std::setw(9) << std::setfill('0') << serial_number_iter->first << " " << rod_iter->first
		  << ".";
// 	  sendMessage(*m_out,"DATASRV_data_avail",MRS_INFORMATION, "DummyDataServer::run_detached", message.str());
	  ers::info(PixLib::pix::daq (ERS_HERE, "DummyDataServer::run_detached", message.str()));

	  {
	    Lock lock( m_dataServerListMutex );
	    serial_number_iter->second.erase(rod_iter);
	    if (serial_number_iter->second.empty()) {
	      m_dataServerList.erase(serial_number_iter);
	    }
	  }

	}

      }

//       sendMessage(*m_out,"DATASRV_term",MRS_INFORMATION, "DummyDataServer::run_detached", "Stopped data retrieval.");
      ers::info(PixLib::pix::daq (ERS_HERE, "DummyDataServer::run_detached", "Stopped data retrieval."));

      m_exit.setFlag();
      return NULL;
  }

  std::shared_ptr<IAnalysisDataServer> DummyMasterDataServer::createAnalysisDataServer(SerialNumber_t /*analysis_serial_number*/,
											 const PixA::RodLocationBase &rod_conn_name,
											 SerialNumbers_t serial_number_list,
											 const InfoPtr_t<PixLib::ScanInfo_t> &/*scan_info*/,
											 const SerialNumber_t /*src_analysis_serial_number*/,
											 PixA::ConnectivityRef &conn,
											 const std::vector<PixA::DisabledListRoot> &disabled_lists)
    {
      Lock lock( m_dataServerListMutex );
      std::shared_ptr<IAnalysisDataServer> data_server(new AnalysisDataServer(conn));
      std::vector< PixA::DisabledListRoot >::const_iterator disabled_list_iter = disabled_lists.begin();

      for(SerialNumbers_t::const_iterator serial_number_iter=serial_number_list.begin();
	  serial_number_iter != serial_number_list.end();
	  ++serial_number_iter, ++disabled_list_iter) {

	assert( disabled_list_iter != disabled_lists.end());
	if (serial_number_iter->second.first == kScanNumber) {
	  DummyDataServer *scan_data_server = new DummyDataServer(rod_conn_name, *this);
	  scan_data_server->setDisabledList(  PixA::createPp0DisabledList( conn,
									   *disabled_list_iter,
									   rod_conn_name) );
	  data_server->addDataServer(serial_number_iter->first, scan_data_server);
	}
	// else {
	// @todo for the time beeing analysis serial numbers are not supported
	// }

      }
      return data_server;
    }

}
