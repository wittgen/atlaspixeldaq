#ifndef _CAN_AnalysisDataServer_hh_
#define _CAN_AnalysisDataServer_hh_

#include <string>
#include <map>
#include "IAnalysisDataServer.hh"
#include "IMetaDataService.hh" //for InfoPtr_t 
#include <ConfigWrapper/Connectivity.h> //for PixA::ConnectivityRef 



namespace CAN {

  class IDataServer;

  /** Class which encapsulates a set of DataServers for one analysis and one ROD (allowing analyses which handle several scans).
   *  All Histogramms for one scan and ROD are handled by a dedicated DataServer.
   *  Since the connectivity has to be the same for all the scans of one analysis 
   *  it is handled here and not in the scan specific DataServers. 
   */
  class AnalysisDataServer : public IAnalysisDataServer {
  public:
    AnalysisDataServer(PixA::ConnectivityRef &conn);

//     AnalysisDataServer(std::map<std::string,SerialNumber_t> scan_serial_numbers,
// 		       const InfoPtr_t<PixLib::ScanInfo_t> &scan_info,
// 		       const SerialNumber_t src_analysis_serial_number,
// 		       PixA::ConnectivityRef &conn);

    ~AnalysisDataServer();

    /** All DataServers for one analysis (and ROD) are only saved here (not in MasterDataServer), to be accessed from the analyses
     *  thus they are deleted in the destructor (they are created by the MasterDataServer)
     */
    void addDataServer(const std::string &scanName, IDataServer* dataServer);

    // methods to retrieve a specific ScanDataServer
    IDataServer* getDataServer(const std::string &scanName);

    IDataServer* getDataServer(); //default version for analysis with only one scan, return the first DataServer in the list
    
    // methods to retrieve the analysis specific connectivity (same for all scans)
    const PixA::ConnectivityRef &connectivity();

    IDataServer* NextDataServer();

    /**
     *  Check if given pp0 was enabled for all scans 
     */
    bool enabled(PixA::Pp0List::const_iterator pp0);

    /**
     *  Returns list of modules which where disbaled in at least one of the scans 
     */
    void createCombinedModuleDisabledList(const PixA::RODLocationBase &rod_location);

    inline PixA::DisabledList& getCombinedModuleDisabledList(const PixA::Pp0LocationBase &pp0) { return m_moduleDisabledLists[pp0]; };  

    void childHasData() {
      m_childsWithData++;
    }

    bool hasAllData() const {
      return m_childsWithData == m_DataServers.size();
    }

  private:
    std::map<std::string,IDataServer*> m_DataServers;
    unsigned int                       m_childsWithData; 

    const PixA::ConnectivityRef         m_conn;
    std::map<std::string,IDataServer*>::const_iterator currentDataServer;
    std::map<const PixA::Pp0LocationBase,PixA::DisabledList>  m_moduleDisabledLists; //at the moment there is only a "(enabled && enabled) -> enabled" type of list 
    // do we need "(enabled || enabled) -> enabled" ?
  };
  
}

#endif
