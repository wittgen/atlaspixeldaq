#include "Scheduler_impl.hh"
#include "Worker.hh"
#include <ConfigWrapper/ConnectivityManager.h>
#include <ConfigWrapper/ConnectivityUtil.h>
#include <ConfigWrapper/ConfObjUtil.h>
#include <ConfigWrapper/PixDisableUtil.h>
//#include <mrs/message.h>
#include "exception.hh"
#include "sendMessage.hh"

#include "PixDbGlobalMutex.hh"

#include "MetaDataService.hh"
#include "IPCServerInstance.hh"

#include "isUtil.hh"

namespace CAN {

  std::string Scheduler_impl::s_componentName="SCHED";

  //  Scheduler_impl::Scheduler_impl(IPCPartition &ipc_partition, IPCPartition &mrs_partition, const std::string &is_server_name)
  Scheduler_impl::Scheduler_impl(IPCPartition &ipc_partition, const std::string &is_server_name)
    : IPCNamedObject<POA_CAN::Scheduler,ipc::multi_thread>(ipc_partition,s_schedulerName),
      //      m_out(new MRSStream(mrs_partition)),
      m_submissionProcessor( new SubmissionProcessor(*this) ),
      //      m_analysisMonitor(ipc_partition, mrs_partition, is_server_name, *this),
      m_analysisMonitor(ipc_partition, is_server_name, *this),
      m_maxConcurrentSubmissions(4),
      m_maxSubmissionQueueLength(10),
      m_retrySubmissionTime(20 /*sec*/),
      m_isServerName(is_server_name),
      m_logLevel(kInfoLog)
  {
    publish();
    std::stringstream message;
    message << "Scheduler " << s_schedulerName  << " in partition " << ipc_partition.name() <<  " is running.";
    {
      //Lock mrs_lock(m_mrsMutex);
      //sendMessage(*m_out,"SCHED_up",s_componentName,MRS_INFORMATION, "CAN::Scheduler_impl::ctor", message.str() );
      ers::info(PixLib::pix::daq (ERS_HERE,"CAN::Scheduler_impl::ctor", message.str() ));
    }
    IPCServerInstance::run();
  }

  void Scheduler_impl::submitAnalysis(SerialNumber_t analysis_serial_number, const AnalysisInfoData_t &analysis_info)
  {

    {
      Lock lock(m_submissionQueueMutex);

      if (m_submissionRequests.size() > m_maxSubmissionQueueLength) {
	std::stringstream message;
	message << "Too many submissions in queue. Already " << m_submissionRequests.size() << " submissions are queued. Please try again later.";
	{
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"SCHED_busy",s_componentName,MRS_FATAL, "CAN::Scheduler_impl::submitAnalysis", message.str() );
	  ers::fatal(PixLib::pix::daq (ERS_HERE,"CAN::Scheduler_impl::submitAnalysis", message.str() ));
	}
	return;
      }

      m_submissionRequests.push_back( SubmissionRequest_t(analysis_serial_number, analysis_info) );
      m_submissionProcessor->newRequest();
    }
  }

  void Scheduler_impl::processSubmissionRequests()
  {
    {
      for(;;) {
	SubmissionRequest_t *current_request;
	{
	  Lock lock(m_submissionQueueMutex);
	  if (m_submissionRequests.empty() ) return;

	  if ( m_analysisMonitor.nJobs()> m_maxConcurrentSubmissions) {

	    // if there are too many jobs beeing processed 
	    // wait.

	    m_submissionProcessor->wait(m_retrySubmissionTime);
	    return;
	  }
	  else {
	    if (m_nWorker==0) {
	      // if there are no worker 
	      // wait 10 sec and try again.
	      m_submissionProcessor->wait(10);
	      return;
	    }
	    // disable timer 
	    m_submissionProcessor->wait(0);
	  }
	  current_request=&(m_submissionRequests.front());
	}

	if (!current_request->abort()) {
	  processSubmissionRequest( *current_request );
	}
	else {
	  current_request->resetAbort();
	  m_analysisMonitor.setGlobalStatusToAborted(current_request->analysisSerialNumber());
	}

	{
	  Lock lock(m_submissionQueueMutex);

	  if (current_request->abort()) {
	    sendAbortAnalysis(current_request->analysisSerialNumber());
	    m_analysisMonitor.markForAbortion(current_request->analysisSerialNumber());
	  }

	  m_submissionRequests.pop_front();
	}

      }
    }
  }

  void Scheduler_impl::processSubmissionRequest(SubmissionRequest_t &submission_request)
  {
    SerialNumber_t analysis_serial_number=submission_request.analysisSerialNumber();
    const AnalysisInfo_t &converted_analysis_info = submission_request.analysisInfo();

    if (analysis_serial_number<=0 && m_logLevel>=kErrorLog) {
      std::stringstream message;
      message << "Invalid analysis serial : " << analysis_serial_number << ".";
//       Lock mrs_lock(m_mrsMutex);
//       sendMessage(*m_out,"SCHED_invalid_param",s_componentName,MRS_ERROR, "CAN::Scheduler_impl::submitAnalysis", message.str() );
      ers::error(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::submitAnalysis", message.str() ));
    }

    try {

      if (m_herd.size()==0) {
	if (m_logLevel>=kErrorLog) {
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"SCHED_no_worker",s_componentName,MRS_ERROR, "CAN::Scheduler_impl::ctor",
	  ers::error(PixLib::pix::daq (ERS_HERE,"CAN::Scheduler_impl::ctor",
		      "No worker is registered. Cannot do anything. Please start at least one worker" ));
	}
	return;
      }

      std::pair<unsigned int, unsigned> slots = workerSlots();

      if (slots.first==0) {
	if (m_logLevel>=kErrorLog) {
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"SCHED_no_worker",s_componentName,MRS_ERROR, "CAN::Scheduler_impl::ctor",
	  ers::error(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::ctor",
		      "No worker offers processing slots. Cannot do anything. Please check whether the workers are still alive." ));
	}
	return;
      }

      //      AnalysisInfo_t converted_analysis_info;
      //      copyAnalysisInfo(analysis_info,converted_analysis_info);

      // get scan info of all scans referenced by the analyses and compare connectivity tags
      InfoPtr_t<PixLib::ScanInfo_t> scan_info = getScanInfoAndCompareConnTags(analysis_serial_number, converted_analysis_info);
      if (!scan_info) {
	return;
      }

      {
	std::stringstream message;
	message << "Query connectivity : "
		<< scan_info->connTag();
	if (scan_info->aliasTag() != scan_info->connTag() && scan_info->dataTag() != scan_info->connTag() ) {
	  message << " " << scan_info->aliasTag() << " " << scan_info->dataTag();
	}
	message << " " << scan_info->cfgTag() << " / " << scan_info->cfgRev();
	if (scan_info->cfgTag() != scan_info->modCfgTag()) {
	  message << " " << scan_info->modCfgTag() << " / " << scan_info->modCfgRev();
	}

	if (m_logLevel>=kInfoLog) {
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"SCHED_query_conn",s_componentName,MRS_DIAGNOSTIC, "CAN::Scheduler_impl::submitAnalysis", message.str() );
	  ers::debug(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::submitAnalysis", message.str() ));
	}
      }
      PixA::ConnectivityRef conn;

      // only the DisabledListRoot will keep the PixDisable alive.
      PixA::DisabledListRoot disabled_list_root;

      {
	Lock lock(m_connMutex);
        Lock globalLock(PixDbGlobalMutex::mutex());
	conn = PixA::ConnectivityManager::getConnectivity( scan_info->idTag(),
                                                           scan_info->connTag(),
							   scan_info->aliasTag(),
							   scan_info->dataTag(),
							   scan_info->cfgTag(),
							   scan_info->modCfgTag(),
							   scan_info->cfgRev());
	if (conn && scan_info->pixDisableString().empty()) {
	  std::stringstream disableName;
	  disableName << "Disable-S" << std::setw(9) << std::setfill('0') << converted_analysis_info.scanSerialNumber();
	  disabled_list_root = PixA::DisabledListRoot( conn.getDisable(disableName.str()) );
	}
	else {
	  if (m_logLevel>=kDiagnostics5Log) {
	    std::stringstream message;
	    message << "Take enable information from meta data (legnth=" << scan_info->pixDisableString().size() <<  ") for "
		    << makeScanSerialNumberString(converted_analysis_info.scanSerialNumber()) << ".";
// 	    Lock mrs_lock(m_mrsMutex);
// 	    sendMessage(*m_out,"SCHED_scan_disable",s_componentName,MRS_DIAGNOSTIC, "CAN::Scheduler_impl::submitAnalysis", message.str() );
	    ers::debug(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::submitAnalysis", message.str() ));
	  }
	  disabled_list_root = PixA::createEnableMap(scan_info->pixDisableString());
	}

      }

      if (!conn) {
	std::stringstream message;
	message << "Failed to load connectivity for tags : " << scan_info->connTag();
	if (scan_info->aliasTag() != scan_info->connTag() && scan_info->dataTag() != scan_info->connTag() ) {
	  message << " " << scan_info->aliasTag() << " " << scan_info->dataTag();
	}
	message << " " << scan_info->cfgTag() << " / " << scan_info->cfgRev() << ".";
	if (scan_info->cfgTag() != scan_info->modCfgTag()) {
	  message << " " << scan_info->modCfgTag() << " / " << scan_info->modCfgRev();
	}

	//	throw MRSException("SCHED_connectivity_load_failed", MRS_FATAL, "CAN::Scheduler_impl::submitAnalysis",message.str());
	throw MRSException(PixLib::PixMessages::FATAL, "CAN::Scheduler_impl::submitAnalysis",message.str());

      }


      // count enabled RODs and estimate the expected load
      std::vector< std::pair<unsigned int, std::string> > enabled_rods;
      PixA::CrateList::const_iterator begin_crate=conn.crates().begin();
      PixA::CrateList::const_iterator end_crate=conn.crates().end();

      {
	std::stringstream message;
	message << " All RODs : ";
	for (PixA::CrateList::const_iterator crate_iter = begin_crate;
	     crate_iter != end_crate;
	     ++crate_iter) {

	  if (!PixA::enabled( disabled_list_root, crate_iter )) continue;

	  const PixA::DisabledList rod_disabled_list( PixA::rodDisabledList( disabled_list_root, crate_iter) );

	  //	std::cout << const_cast<PixLib::RodCrateConnectivity *>(*crate_iter)->name() << std::endl;
	  PixA::RodList::const_iterator begin_rod = PixA::rodBegin(crate_iter);
	  PixA::RodList::const_iterator end_rod = PixA::rodEnd(crate_iter);
	  for (PixA::RodList::const_iterator rod_iter = begin_rod;
	       rod_iter != end_rod;
	       ++rod_iter) {

	    std::string temp(PixA::connectivityName(rod_iter) );
	    // debug: only consider ROD_B1_* rods
	    //if (temp.find("ROD_B1")==std::string::npos) continue;

	    if (!PixA::enabled( rod_disabled_list, rod_iter )) continue;

	    const PixA::DisabledList pp0_disabled_list( PixA::pp0DisabledList( rod_disabled_list, rod_iter) );

	    unsigned int n_modules=0;

	    PixA::Pp0List::const_iterator begin_pp0 = PixA::pp0Begin(rod_iter);
	    PixA::Pp0List::const_iterator end_pp0 = PixA::pp0End(rod_iter);
	    for (PixA::Pp0List::const_iterator pp0_iter = begin_pp0;
		 pp0_iter != end_pp0;
		 ++pp0_iter) {

	      if (!PixA::enabled( pp0_disabled_list, pp0_iter )) continue;

	      const PixA::DisabledList module_disabled_list(PixA::moduleDisabledList( pp0_disabled_list, pp0_iter) );

	      PixA::ModuleList::const_iterator begin_module = PixA::modulesBegin(pp0_iter);
	      PixA::ModuleList::const_iterator end_module = PixA::modulesEnd(pp0_iter);
	      for (PixA::ModuleList::const_iterator module_iter = begin_module;
		   module_iter != end_module;
		   ++module_iter) {

		if (PixA::enabled( module_disabled_list, module_iter )) {
		  n_modules++;
		}

	      }
	    }
	    if (n_modules>0) {
	      // assume that the load (second ) just depends on the number of modules.
	      enabled_rods.push_back( std::make_pair( n_modules, PixA::connectivityName(rod_iter) ) );
	      message << PixA::connectivityName(rod_iter) << " ";

	    }
	  }
	}
	message << ".";
	if (m_logLevel>=kDiagnosticsLog) {
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"SCHED_rod_list",s_componentName,MRS_DIAGNOSTIC, "CAN::Scheduler_impl::submitAnalysis", message.str() );
	  ers::debug(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::submitAnalysis", message.str() ));
	}
      }

      if (enabled_rods.size() > 0 ) {


	// The serial number is supplied by the user. And also the analysis meta data has to be written by the user
	//
	//        SerialNumber_t analysis_serial_number = CAN::MetaDataService::instance()->newSerialNumber(CAN::kAnalysisNumber);
	//        InfoPtr_t<AnalysisInfo_t> temp_info(new AnalysisInfo_t);
	//        copyAnalysisInfo(analysis_info,*temp_info);
	//        CAN::MetaDataService::instance()->addInfo(analysis_serial_number,temp_info);

	if (!m_analysisMonitor.haveJobList(analysis_serial_number)) {


	  std::shared_ptr<AnalysisJobList_t> job_list(new AnalysisJobList_t(converted_analysis_info,
									      m_analysisMonitor,
									      AnalysisStatus::makeIsHeader("",analysis_serial_number)));

	  // publish global status
	  {
	    std::string is_global_status_name( AnalysisStatus::makeRegexOrVarName( AnalysisStatus::makeIsHeader(m_isServerName, analysis_serial_number),
										   AnalysisStatus::globalVarName()));
	    Lock dictionary_lock(m_analysisMonitor.mutex());
	    ISType test;
	    setIsValue(m_analysisMonitor.dictionary(),test, is_global_status_name, job_list->isGlobalStatus());
	  }

	  spreadJobs(analysis_serial_number, job_list, enabled_rods);
	}
	else {
	  std::stringstream message;
	  message << "There are already jobs running tagged by the serial number " << makeAnalysisSerialNumberString(analysis_serial_number)
		  << ". Apparently the serial number is not new." << std::endl;
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"SCHED_config_except",MRS_FATAL,"CAN::Scheduler_impl::submitAnalysis",message.str());
	  ers::fatal(PixLib::pix::daq (ERS_HERE,"CAN::Scheduler_impl::submitAnalysis",message.str()));
	}
      }
    }
    catch (CAN::MRSException &err) {
      //      Lock mrs_lock(m_mrsMutex);
      err.sendMessage();//*m_out,"CAN::Scheduler_impl::submitAnalysis");
    }
    catch (PixA::ConfigException &err) {
      std::stringstream message;
      message << "Caught exception :  " << err << "." << std::endl;
//       Lock mrs_lock(m_mrsMutex);
//       sendMessage(*m_out,"SCHED_config_except",MRS_FATAL,"CAN::Scheduler_impl::submitAnalysis",message.str());
      ers::fatal(PixLib::pix::daq (ERS_HERE,"CAN::Scheduler_impl::submitAnalysis",message.str()));
    }
    catch (PixLib::PixDBException &err) {
      std::stringstream message;
      message << "Caught exception :  " << err << "." << std::endl;
//       Lock mrs_lock(m_mrsMutex);
//       sendMessage(*m_out,"SCHED_db_except",MRS_FATAL,"CAN::Scheduler_impl::submitAnalysis",message.str());
      ers::fatal(PixLib::pix::daq (ERS_HERE,"CAN::Scheduler_impl::submitAnalysis",message.str()));
    }
    catch (std::exception &err) {
      std::stringstream message;
      message << "Caught exception :  " << err.what() << "." << std::endl;
//       Lock mrs_lock(m_mrsMutex);
//       sendMessage(*m_out,"SCHED_std_except",s_componentName,MRS_FATAL,"CAN::Scheduler_impl::submitAnalysis",message.str());
      ers::fatal(PixLib::pix::daq (ERS_HERE,"CAN::Scheduler_impl::submitAnalysis",message.str()));
    }
    catch (...) {
//       Lock mrs_lock(m_mrsMutex);
//       sendMessage(*m_out,"SCHED_unknown_except",s_componentName,MRS_FATAL,"CAN::Scheduler_impl::submitAnalysis","Unknown exception occured.");
      ers::fatal(PixLib::pix::daq (ERS_HERE,"CAN::Scheduler_impl::submitAnalysis","Unknown exception occured."));
    }

  }

  InfoPtr_t<PixLib::ScanInfo_t> Scheduler_impl::getScanInfoAndCompareConnTags(SerialNumber_t analysis_serial_number, const AnalysisInfo_t &the_analysis_info)
  {
    // get scan info of all scans referenced by the analyses and compare connectivity tags

    Lock lock(m_metaDataMutex);

    InfoPtr_t<PixLib::ScanInfo_t> scan_info;
    InfoPtr_t<PixLib::ScanInfo_t> empty_scan_info;
    std::set<SerialNumber_t> all_scan_serial_numbers;

    for (std::map<std::string, std::pair<ESerialNumberType, SerialNumber_t> >::const_iterator source_iter = the_analysis_info.beginSource();
	 source_iter != the_analysis_info.endSource();
	 source_iter++) {

      if (source_iter->second.first == kScanNumber) {
	all_scan_serial_numbers.insert(source_iter->second.second);
      }
      else if (source_iter->second.first == kAnalysisNumber) {
	InfoPtr_t<CAN::AnalysisInfo_t> an_analysis_info = MetaDataService::instance()->getAnalysisInfo(source_iter->second.second) ;
	all_scan_serial_numbers.insert( an_analysis_info->scanSerialNumber());
      }
      else {
	if (m_logLevel>=kErrorLog) {
	  std::stringstream message;
	  message << "Source serial number is neither a scan nor an analysis " << makeAnalysisSerialNumberString(analysis_serial_number) << "." << std::endl;

// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"SCHED_bad_analysis_info",s_componentName,MRS_ERROR, "CAN::Scheduler_impl::getScanInfoAndCompareConnTags", message.str() );
	  ers::error(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::getScanInfoAndCompareConnTags", message.str() ));
	}
	return empty_scan_info;
      }
    }

    for (std::set<SerialNumber_t>::const_iterator scan_iter = all_scan_serial_numbers.begin();
	 scan_iter != all_scan_serial_numbers.end();
	 scan_iter++) {

      InfoPtr_t<PixLib::ScanInfo_t> temp_scan_info = MetaDataService::instance()->getScanInfo( *scan_iter ) ;
      if (!temp_scan_info) {
	if (m_logLevel>=kErrorLog) {
	  std::stringstream message;
	  message << "Did not get scan meta data for  a scan " << makeScanSerialNumberString( *scan_iter ) << "." << std::endl;
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"SCHED_missing_scan_info",s_componentName,MRS_ERROR, "CAN::Scheduler_impl::getScanInfoAndCompareConnTags", message.str() );
	  ers::error(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::getScanInfoAndCompareConnTags", message.str() ));
	}
	return empty_scan_info;
      }
      else if (scan_info) {

	if (     scan_info->idTag()    != temp_scan_info->idTag()
		 ||  scan_info->connTag()  != temp_scan_info->connTag()
		 ||  scan_info->aliasTag() != temp_scan_info->aliasTag()
		 ||  scan_info->dataTag()  != temp_scan_info->dataTag()) {
	  if (m_logLevel>=kErrorLog) {
	    std::stringstream message;
	    message << "Connectivity tags of the scans " << makeScanSerialNumberString( *(all_scan_serial_numbers.begin()) )
		    << " and " << makeScanSerialNumberString( *scan_iter )
		    << " differ." << std::endl;
// 	    Lock mrs_lock(m_mrsMutex);
// 	    sendMessage(*m_out,"SCHED_conntag_mismatch",s_componentName,MRS_ERROR, "CAN::Scheduler_impl::getScanInfoAndCompareConnTags", message.str() );
	    ers::error(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::getScanInfoAndCompareConnTags", message.str() ));
	  }
	  return empty_scan_info;
	}
      }
      else {
	scan_info = temp_scan_info;
      }

    }
    return scan_info;
  }


  std::pair<unsigned int, unsigned> Scheduler_impl::workerSlots()
  {
    unsigned int n_free_slots=0;
    unsigned int n_slots=0;
    {
      Lock register_lock(m_herdMutex);
      for (std::vector< std::shared_ptr<WorkerInfo_t > >::iterator worker_iter = m_herd.begin();
	   worker_iter != m_herd.end();
	   worker_iter++) {

	if ((*worker_iter)->toBeRemoved()) continue;

	n_slots +=  (*worker_iter)->nSlots();
	n_free_slots +=  (*worker_iter)->nUnusedSlots();
      }
    }

    if (n_slots==0) {
      if (m_logLevel>=kErrorLog) {
	std::stringstream message;
	message << "The " << m_herd.size()
		<< " workers do not offer any slots. Cannot do any processing. Please start at least one worker with at least one processing slot.";
// 	Lock mrs_lock(m_mrsMutex);
// 	sendMessage(*m_out,"SCHED_no_worker",s_componentName,MRS_ERROR, "CAN::Scheduler_impl::ctor", message.str() );
	ers::error(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::ctor", message.str() ));
      }
    }
    return std::make_pair(n_slots, n_free_slots);
  }

  bool Scheduler_impl::spreadJobs(SerialNumber_t analysis_serial_number,
				  const std::shared_ptr<AnalysisJobList_t> &job_list,
				  const std::vector< std::pair<unsigned int, std::string > > &rod_list)
  {
    AnalysisInfoData_t analysis_info_data;
    copyAnalysisInfo(job_list->info(), analysis_info_data);
    return spreadJobs(analysis_serial_number, job_list, analysis_info_data, rod_list);
  }

  //  void Scheduler_impl::spreadJobs(SerialNumber_t analysis_serial_number, const AnalysisInfo_t &analysis_info, const std::vector<std::string> &rod_list) {
  //    AnalysisInfoData_t analysis_info_data;
  //  //    copyAnalysisInfo(analysis_info, analysis_info_data);
  //    spreadJobs(analysis_serial_number, analysis_info_data, rod_list);
  //  }
  bool Scheduler_impl::spreadJobs(SerialNumber_t analysis_serial_number,
				  const std::shared_ptr<AnalysisJobList_t> &job_list,
				  const AnalysisInfoData_t &analysis_info_data,
				  const std::vector< std::pair<unsigned int, std::string > > &rod_list)
  {
    // first remove all worker which are marked for removal
    // this also updates the preferred worker lists
    removeWorker();

    // should never happen :
    if (rod_list.size() == 0 ) return true;

    bool submission_okay = false;
    try {


      std::multimap< unsigned int, std::pair< std::string, bool >, std::greater<unsigned int> > temp_rod_list;
      // determine the expected new load to be distributed
      // and sort jobs according to the expected load
      unsigned int total_new_load = 0;
      unsigned int max_single_load = 0;
      unsigned int av_single_load = UINT_MAX;

      for(std::vector< std::pair< unsigned int, std::string>  >::const_iterator rod_iter = rod_list.begin();
	  rod_iter != rod_list.end();
	  rod_iter++) {

	// paranoia check
	// should not be necessary
	if (rod_iter->first==0) {
	  if (m_logLevel>=kFatalLog) {
	    std::stringstream message;
	    message << "Expected load for ROD " << rod_iter->second << " is zero for analysis "
		    << makeAnalysisSerialNumberString(analysis_serial_number) << ". Logic error.";
// 	    Lock mrs_lock(m_mrsMutex);
// 	    sendMessage(*m_out,"SCHED_std_except",s_componentName,MRS_FATAL,"CAN::Scheduler_impl::spreadJobs",message.str());
	    ers::error(PixLib::pix::daq (ERS_HERE,"CAN::Scheduler_impl::spreadJobs",message.str()));
	  }
	  continue;
	}

	if (rod_iter->first > max_single_load) {
	  max_single_load = rod_iter->first;
	}
	if (rod_iter->first < av_single_load) {
	  av_single_load = rod_iter->first;
	}
	total_new_load += rod_iter->first;
	temp_rod_list.insert(std::make_pair( rod_iter->first, std::make_pair(rod_iter->second,false) ) );
      }
      //      av_single_load = total_new_load / rod_list.size();

      Lock worker_lock(m_herdMutex);

      // dump out the distribution and expected load :
      if (m_logLevel>=kDiagnosticsLog && m_herd.size()>0) {
	std::stringstream message;
	message << " Current loads : ";
	for (std::vector< std::shared_ptr<WorkerInfo_t > >::iterator worker_iter = m_herd.begin();
	     worker_iter != m_herd.end();
	     worker_iter++) {
	  message << (*worker_iter)->name() << ":" << (*worker_iter)->load();
	  if (m_herd.end() - worker_iter>1) {
	    message << ", ";
	  }
	}
	message << ".";
// 	Lock mrs_lock(m_mrsMutex);
// 	sendMessage(*m_out,"SCHED_spread",s_componentName,MRS_DIAGNOSTIC,"CAN::Scheduler_impl::spreadJobs",message.str());
	ers::debug(PixLib::pix::daq (ERS_HERE,"CAN::Scheduler_impl::spreadJobs",message.str()));
      }


      // determine average load
      unsigned int av_load=0;
      unsigned int n_slots=0;
      for (std::vector< std::shared_ptr<WorkerInfo_t > >::iterator worker_iter = m_herd.begin();
	   worker_iter != m_herd.end();
	   worker_iter++) {

	if ((*worker_iter)->toBeRemoved()) continue;
	n_slots += (*worker_iter)->nSlots();
	av_load += (*worker_iter)->load();

      }

      if (n_slots==0) {
	if (m_logLevel>=kFatalLog) {
	  std::stringstream message;
	  message << "No worker or worker do not offer slots. Cannot process analysis submission " << makeAnalysisSerialNumberString(analysis_serial_number) << ".";
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"SCHED_std_except",s_componentName,MRS_FATAL,"CAN::Scheduler_impl::spreadJobs",message.str());
	  ers::fatal(PixLib::pix::daq (ERS_HERE,"CAN::Scheduler_impl::spreadJobs",message.str()));
	}

	return false;
      }

      av_load += total_new_load;
      {

	// exclude overloaded worker whose load is too high to take additional jobs
	bool overloaded_worker = true;
	unsigned int temp_n_slots = n_slots;
	while ( overloaded_worker == true && temp_n_slots > 0) {
	  n_slots = temp_n_slots;
	  overloaded_worker = false;
	  unsigned int temp_av_load = av_load / n_slots;
	  temp_n_slots = 0;
	  for (std::vector< std::shared_ptr<WorkerInfo_t > >::iterator worker_iter = m_herd.begin();
	       worker_iter != m_herd.end();
	       worker_iter++) {
	    if ((*worker_iter)->toBeRemoved() || (*worker_iter)->nSlots() == 0 ) continue;

	    if ((*worker_iter)->load() > (temp_av_load + max_single_load) * (*worker_iter)->nSlots())  {
	      overloaded_worker = true;
	    }
	    else {
	      temp_n_slots += (*worker_iter)->nSlots();
	    }
	  }
	  if (temp_n_slots==n_slots) break;
	}
	av_load /= n_slots; // use as "upper" threshold

	if (m_logLevel>=kDiagnosticsLog) {
	  std::stringstream message;
	  message << "load threshold  = " << av_load + max_single_load << " slots = " << n_slots << ",  max single load = " << max_single_load
		  << " average single load = " << av_single_load
		  << " in analysis "
		  << makeAnalysisSerialNumberString(analysis_serial_number) << ".";
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"SCHED_spread",s_componentName,MRS_DIAGNOSTIC,"CAN::Scheduler_impl::spreadJobs",message.str());
	  ers::debug(PixLib::pix::daq (ERS_HERE,"CAN::Scheduler_impl::spreadJobs",message.str()));
	}
      }

      std::map< std::shared_ptr<WorkerInfo_t> , std::vector< std::pair<unsigned int, std::string >  > > worker_list;
      for (std::vector< std::shared_ptr<WorkerInfo_t > >::iterator worker_iter = m_herd.begin();
	   worker_iter != m_herd.end();
	   worker_iter++) {

	if ((*worker_iter)->toBeRemoved()) continue;

	worker_list.insert(std::make_pair(*worker_iter, std::vector< std::pair< unsigned int, std::string>  >() ) );
      }

      std::vector< std::map< std::string, std::vector< std::shared_ptr<WorkerInfo_t > > >::iterator > preferred_worker_list;
      preferred_worker_list.resize(rod_list.size(), m_preferredWorker.end());

      std::string temp_empty;

      // first assign jobs for RODs which have exactly one preferred worker, then two (maximum)
      for( unsigned int n_preferred_worker = 0; n_preferred_worker < 2; n_preferred_worker++) {

	for(std::map< unsigned int, std::pair<std::string, bool> >::iterator rod_iter = temp_rod_list.begin();
	    rod_iter != temp_rod_list.end();
	    rod_iter++) {

	  // rod assigned to a worker ?
	  if (rod_iter->second.second ) continue;

	  //	      unsigned int rod_i = rod_iter-current_rod_list_ptr->begin();

	  std::map< std::string, std::vector< std::shared_ptr<WorkerInfo_t > > >::iterator
	    preferred_worker_list_iter = m_preferredWorker.find( rod_iter->second.first );

	  if ( preferred_worker_list_iter != m_preferredWorker.end() && preferred_worker_list_iter->second.size()>0) {
	    //	      preferred_worker_list[ rod_i ] =  preferred_worker_iter->second;
	    if (preferred_worker_list_iter->second.size()==n_preferred_worker) {

	      for(std::vector< std::shared_ptr<WorkerInfo_t > >::const_iterator preferred_worker_iter= preferred_worker_list_iter->second.begin();
		  preferred_worker_iter != preferred_worker_list_iter->second.end();
		  preferred_worker_iter++) {

		// assume that the total load can be split "equally" over all slots of the worker.
		// Allow a worker to exceed the average load by the maximum load produced by a single
		// job
		if (   ((*preferred_worker_iter)->load() + rod_iter->first )
		       <= ((av_load+max_single_load) * (*preferred_worker_iter)->nSlots()) ) {

		  std::map< std::shared_ptr<WorkerInfo_t> , std::vector< std::pair<unsigned int, std::string>  > >::iterator
		    worker_iter = worker_list.find( *preferred_worker_iter  );

		  if (worker_iter == worker_list.end()) {
		    // In principle, this should never happen.
		    // The preferred worker lists and the list of existing worker shold be
		    // sycnhronised.

		    // remove this worker from the preferred worker list
		    // and assign later.
		    preferred_worker_list_iter->second.clear();

		    // 		    std::stringstream message;
		    // 		    message << "Preferred worker for ROD " << rod_iter->second.first << " while submitting analysis "
		    // 			    << makeAnalysisSerialNumberString(analysis_serial_number) << ".";
		    // 		    Lock mrs_lock(m_mrsMutex);
		    // 		    sendMessage(*m_out,"SCHED_bug",s_componentName,MRS_FATAL,"CAN::Scheduler_impl::spreadJobs",message.str());
		  }
		  else {
		    // assign ROD to worker
		    worker_iter->first->giveLoad( rod_iter->first );
		    worker_iter->second.push_back( std::make_pair(rod_iter->first, rod_iter->second.first) );
		    rod_iter->second.second = true;

		    // 		    std::stringstream message;
		    // 		    message << "Assign job for ROD " << rod_iter->second.first << " to preferred worker " <<  (worker_iter->first)->name()
		    // 			    << " in analysis "
		    // 			    << makeAnalysisSerialNumberString(analysis_serial_number) << ".";
		    // 		    Lock mrs_lock(m_mrsMutex);
		    // 		    sendMessage(*m_out,"SCHED_spread",s_componentName,MRS_DIAGNOSTIC,"CAN::Scheduler_impl::spreadJobs",message.str());

		  }
		}
		else if (m_logLevel>=kDiagnosticsLog) {
		  std::stringstream message;
		  message << "Cannot assign job for ROD " << rod_iter->second.first << " to preferred worker since the load exceeds target value : "
			  << ( (*preferred_worker_iter)->load() + rod_iter->first )
			  << " !<=  " << ( (av_load+max_single_load) * (*preferred_worker_iter)->nSlots() )
			  << " in " << makeAnalysisSerialNumberString(analysis_serial_number) << ".";
// 		  Lock mrs_lock(m_mrsMutex);
// 		  sendMessage(*m_out,"SCHED_spread",s_componentName,MRS_DIAGNOSTIC,"CAN::Scheduler_impl::spreadJobs",message.str());
		  ers::debug(PixLib::pix::daq (ERS_HERE,"CAN::Scheduler_impl::spreadJobs",message.str()));
		}
	      }
	    }
	  }
	}

      }

      // now randomely distribute
      {
	std::map< std::shared_ptr<WorkerInfo_t> , std::vector< std::pair<unsigned int, std::string >  > >::iterator worker_iter = worker_list.begin();

	for(std::map< unsigned int, std::pair<std::string, bool> >::iterator rod_iter = temp_rod_list.begin();
	    rod_iter != temp_rod_list.end();
	    rod_iter++) {

	  // ROD already assigned to a worker ?
	  if (rod_iter->second.second) continue;

	  // find a worker whose load is still tolarable :

	  std::map< std::shared_ptr<WorkerInfo_t> , std::vector< std::pair<unsigned int, std::string>  > >::iterator start_worker_iter = worker_iter;
	  std::map< std::shared_ptr<WorkerInfo_t> , std::vector< std::pair<unsigned int, std::string>  > >::iterator min_worker_iter = worker_iter;
	  unsigned int min_load = (worker_iter->first)->load();

	  while ( ( (worker_iter->first)->load() + rod_iter->first )
		  >= ((av_load /* +av_single_load */ ) * (worker_iter->first)->nSlots()  ) ) {
	    if (++worker_iter == worker_list.end()) {
	      worker_iter = worker_list.begin();
	    }
	    if (worker_iter == start_worker_iter) {
	      break;
	    }
	    if ((worker_iter->first)->load() < min_load) {
	      min_load = (worker_iter->first)->load();
	      min_worker_iter = worker_iter;
	    }
	  }

// 	  {
// 	    std::stringstream message;
// 	    message << " Assign  " << rod_iter->second.first << "(" << rod_iter->first << ") to "
// 		    << (worker_iter->first)->name() << "(" << (worker_iter->first)->load() << ")  av_load = " << av_load 
// 		    << ", min_load = " << min_load << "." << std::endl;

// 	    Lock mrs_lock(m_mrsMutex);
// 	    sendMessage(*m_out,"SCHED_spread",s_componentName,MRS_DIAGNOSTIC,"CAN::Scheduler_impl::spreadJobs",message.str());
// 	  }


	  if (worker_iter == start_worker_iter ) {
	    assert( min_worker_iter != worker_list.end());
	    worker_iter = min_worker_iter;
	  }
	  (worker_iter->first)->giveLoad( rod_iter->first );
	  worker_iter->second.push_back( std::make_pair(rod_iter->first, rod_iter->second.first) );

	  // add the worker to the list of preferred worker of the ROD :
	  std::map< std::string, std::vector< std::shared_ptr<WorkerInfo_t > > >::iterator
	    preferred_worker = m_preferredWorker.find( rod_iter->second.first );

	  if (preferred_worker== m_preferredWorker.end()) {

	    std::pair<std::map< std::string, std::vector< std::shared_ptr<WorkerInfo_t > > >::iterator,bool>
	      ret = m_preferredWorker.insert(std::make_pair( rod_iter->second.first, std::vector< std::shared_ptr<WorkerInfo_t > >()) );
	    if (ret.second) {
	      ret.first->second.push_back( worker_iter->first );
	    }
	  }
	  else {
	    // limit the number of preferred worker to 2
	    if (preferred_worker->second.size()>1) {
	      preferred_worker->second.erase(preferred_worker->second.begin());
	    }
	    preferred_worker->second.push_back(worker_iter->first);
	  }
	  rod_iter->second.second = true;

	  if (++worker_iter == worker_list.end()) {
	    worker_iter = worker_list.begin();
	  }

	}
      }

      // dump out the distribution and expected load :
      if (m_logLevel>=kDiagnosticsLog) {
	std::stringstream message;
	message << " loads : ";
	for (std::vector< std::shared_ptr<WorkerInfo_t > >::iterator worker_iter = m_herd.begin();
	     worker_iter != m_herd.end();
	     worker_iter++) {
	  message << (*worker_iter)->name() << ":" << (*worker_iter)->load();
	  if (m_herd.end() - worker_iter>1) {
	    message << ", ";
	  }
	}
	message << ".";
// 	Lock mrs_lock(m_mrsMutex);
// 	sendMessage(*m_out,"SCHED_spread",s_componentName,MRS_DIAGNOSTIC,"CAN::Scheduler_impl::spreadJobs",message.str());
	ers::debug(PixLib::pix::daq (ERS_HERE,"CAN::Scheduler_impl::spreadJobs",message.str()));
      }

      for (std::map< std::shared_ptr<WorkerInfo_t> , std::vector< std::pair<unsigned int, std::string>  > >::iterator worker_iter = worker_list.begin();
	   worker_iter != worker_list.end();
	   worker_iter++) {

	{
	  std::stringstream message;
	  message << " assigned to worker " << (worker_iter->first)->name() << ":";
	  for (std::vector< std::pair<unsigned int, std::string > >::const_iterator rod_iter = worker_iter->second.begin();
	       rod_iter != worker_iter->second.end();
	       rod_iter++) {

	    message << rod_iter->second << "(" << rod_iter->first << ")";
	    if (worker_iter->second.end() - rod_iter>1) {
	      message << ", ";
	    }
	  }
	  if (m_logLevel>=kDiagnosticsLog) {
// 	    Lock mrs_lock(m_mrsMutex);
// 	    sendMessage(*m_out,"SCHED_spread",s_componentName,MRS_DIAGNOSTIC,"CAN::Scheduler_impl::spreadJobs",message.str());
	    ers::debug(PixLib::pix::daq (ERS_HERE,"CAN::Scheduler_impl::spreadJobs",message.str()));
	  }
	}
      }


      {
	// check whether this is the current submission and whether it should be aborted
	Lock lock(m_submissionQueueMutex);
	if (!m_submissionRequests.empty() 
	    && m_submissionRequests.front().analysisSerialNumber() == analysis_serial_number
	    && m_submissionRequests.front().abort()) {
	  // remove the abort mark 
	  // such that the abort is not send again
	  m_submissionRequests.front().resetAbort();

	  m_analysisMonitor.setGlobalStatusToAborted(analysis_serial_number);


	  if (m_logLevel>=kInfoLog) {
	    std::stringstream message;
	    message << "Aborted analysis " << makeAnalysisSerialNumberString(analysis_serial_number) << ".";
// 	    Lock mrs_lock(m_mrsMutex);
// 	    sendMessage(*m_out,"SCHED_spreadJobs",s_componentName,MRS_INFORMATION,"CAN::Scheduler_impl::spreadJobs",message.str());
	    ers::info(PixLib::pix::daq (ERS_HERE,"CAN::Scheduler_impl::spreadJobs",message.str()));
	  }
	  return true;
	}
      }

      bool job_submission_okay=true;

      unsigned int n_jobletts = 0;
      unsigned int n_jobletts_total = 0;
      unsigned int n_worker = 0;
      for (std::map< std::shared_ptr<WorkerInfo_t> , std::vector< std::pair<unsigned int, std::string >  > >::iterator worker_iter = worker_list.begin();
	   worker_iter != worker_list.end();
	   worker_iter++) {
	if (!submitJobs(analysis_serial_number,
			job_list,
			analysis_info_data,
			worker_iter->first,
			worker_iter->second)) {
	  job_submission_okay =false;
	}
	else {
	  n_jobletts += worker_iter->second.size();
	  n_worker++;
	}
	n_jobletts_total += worker_iter->second.size();
      }

      if (!m_analysisMonitor.addJobList(analysis_serial_number, job_list)) {
	std::stringstream message;
	message << "Failed to add analysis " << makeAnalysisSerialNumberString(analysis_serial_number)
		<< " to internal queue of scheduler. Possible reasons : out of memory or serial number is not unique.";

	if (m_logLevel>=kFatalLog) {
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"SCHED_Queue_Failure",s_componentName,MRS_FATAL, "CAN::Scheduler_impl::submitAnalysis", message.str() );
	  ers::fatal(PixLib::pix::daq (ERS_HERE,"CAN::Scheduler_impl::submitAnalysis", message.str() ));
	}
	throw std::runtime_error("out of memory.");
      }

      if (m_logLevel>=kInfoLog) {
	std::stringstream message;
	message << "Submitted successfully analysis " << makeAnalysisSerialNumberString(analysis_serial_number) << " of  "
		<< n_jobletts << " / " << n_jobletts_total << " RODs to "
		<< n_worker <<" / " << worker_list.size() << " worker.";
// 	Lock mrs_lock(m_mrsMutex);
// 	sendMessage(*m_out,"SCHED_spreadJobs",s_componentName,MRS_INFORMATION,"CAN::Scheduler_impl::spreadJobs",message.str());
	ers::info(PixLib::pix::daq (ERS_HERE,"CAN::Scheduler_impl::spreadJobs",message.str()));
      }

      submission_okay = job_submission_okay;
    }
    catch (CAN::MRSException &err) {
      //      Lock mrs_lock(m_mrsMutex);
      err.sendMessage();//*m_out,"CAN::Scheduler_impl::spreadJobs");
    }
    catch (std::exception &err) {
      std::stringstream message;
      message << "Caught exception :  " << err.what() << "." << std::endl;
//       Lock mrs_lock(m_mrsMutex);
//       sendMessage(*m_out,"SCHED_std_except",s_componentName,MRS_FATAL,"CAN::Scheduler_impl::spreadJobs",message.str());
      ers::fatal(PixLib::pix::daq (ERS_HERE,"CAN::Scheduler_impl::spreadJobs",message.str()));
    }
    catch (...) {
//       Lock mrs_lock(m_mrsMutex);
//       sendMessage(*m_out,"SCHED_unknown_except",s_componentName,MRS_FATAL,"CAN::Scheduler_impl::spreadJobs","Unknown exception occured.");
      ers::fatal(PixLib::pix::daq (ERS_HERE,"CAN::Scheduler_impl::spreadJobs","Unknown exception occured."));
    }
    return submission_okay;
  }

  bool Scheduler_impl::submitJobs( SerialNumber_t analysis_serial_number,
				   const std::shared_ptr<AnalysisJobList_t> &job_list,
				   const AnalysisInfoData_t &analysis_info_data,
				   std::shared_ptr<WorkerInfo_t> the_worker,
				   const std::vector< std::pair<unsigned int, std::string > > &rod_list)
  {

    {
      Lock queue_lock(m_analysisMonitor.jobQueueMutex());
      job_list->addJobs(AnalysisStatus::makeIsHeader(m_isServerName,analysis_serial_number), the_worker ,rod_list);
    }

    bool submission_okay = false;
    try {
      StringVec rod_name_list;
      rod_name_list.length(rod_list.size());
      for (unsigned int rod_i=0; rod_i<rod_list.size(); rod_i++) {
	rod_name_list[rod_i] =  CORBA::string_dup( rod_list[rod_i].second.c_str() );
      }
      the_worker->worker()->submitAnalysis(analysis_serial_number, analysis_info_data, rod_name_list);
      if (m_logLevel>=kDiagnosticsLog) {
	std::stringstream message;
	message << "Submitted jobs for RODs : ";

	for (std::vector< std::pair<unsigned int, std::string> >::const_iterator rod_iter = rod_list.begin();
	     rod_iter != rod_list.end();
	     rod_iter++) {
	  message << " " << rod_iter->second;
	}
	message << " to worker " << the_worker->name() << " (" << makeAnalysisSerialNumberString(analysis_serial_number) << ".";
// 	Lock mrs_lock(m_mrsMutex);
// 	sendMessage(*m_out,"SCHED_submission",s_componentName,MRS_DIAGNOSTIC, "CAN::Scheduler_impl::submitAnalysis", message.str() );
	ers::debug(PixLib::pix::daq (ERS_HERE,"CAN::Scheduler_impl::submitAnalysis", message.str() ));
      }
      submission_okay = true;
    }
    catch (CORBA::TRANSIENT& err) {
    }
    catch (CORBA::COMM_FAILURE &err) {
    }
    catch (...) {
      if (m_logLevel>=kErrorLog) {
	std::stringstream message;
	message << "Caught unhandled exception while submitting to " << the_worker->name() << " for " << makeAnalysisSerialNumberString(analysis_serial_number) << ".";
// 	Lock mrs_lock(m_mrsMutex);
// 	sendMessage(*m_out,"SCHED_submission",s_componentName,MRS_ERROR, "CAN::Scheduler_impl::submitAnalysis", message.str() );
	ers::error(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::submitAnalysis", message.str() ));
      }
    }
    if (!submission_okay) {
      if (m_logLevel>=kErrorLog) {
	std::stringstream message;
	message << "CORBA error while submitting analysis for RODs : ";
	for (std::vector< std::pair<unsigned int, std::string> >::const_iterator rod_iter = rod_list.begin();
	   rod_iter != rod_list.end();
	     rod_iter++) {
	  message << " " << rod_iter->second;
	}
	message << " to worker " << the_worker->name() << " for "  << makeAnalysisSerialNumberString(analysis_serial_number) << ". Will reschedule analysis jobs.";
	{
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"SCHED_submission",s_componentName,MRS_ERROR, "CAN::Scheduler_impl::submitJobs", message.str() );
	  ers::error(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::submitJobs", message.str() ));
	}
      }
      the_worker->markForRemoval();


      // mark all failed jobs for resubmission
      {
	//	Lock queue_lock(m_analysisMonitor.jobQueueMutex());
	for (std::vector< std::pair<unsigned int, std::string > >::const_iterator rod_iter = rod_list.begin();
	     rod_iter != rod_list.end();
	     rod_iter++) {
	  AnalysisJobList_t::iterator job_iter =  job_list->find(rod_iter->second);
	  if (job_iter == job_list->end()) {
	    if (m_logLevel>=kFatalLog) {
	      std::stringstream message;
	      message << "Cannot mark job for ROD "<< rod_iter->first << " for resubmission since it is not in the job list (analysis "
		      << makeAnalysisSerialNumberString(analysis_serial_number) << ").";
// 	      Lock mrs_lock(m_mrsMutex);
// 	      sendMessage(*m_out,"SCHED_submission",s_componentName,MRS_FATAL, "CAN::Scheduler_impl::submitJobs", message.str() );
	      ers::fatal(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::submitJobs", message.str() ));
	    }

	  }
	  else {
	    job_iter->second.markForResubmission();
	  }
	}
      }
    }
    return submission_okay;
  }

  /** Query the status of all queues.
   *  This call may ask the worker nodes for the current status, thus it could block.
   */
  void Scheduler_impl::getQueueStatus(CORBA::ULong &jobs_waiting_for_data, CORBA::ULong &jobs_waiting, CORBA::ULong  &jobs_running, CORBA::ULong  &empty_slots)
  {
    checkWorkerAndRemoveDeadOnes(jobs_waiting_for_data, jobs_waiting, jobs_running, empty_slots, 0);

    // also kind of consider the unprocessed submission requests.
    jobs_waiting_for_data += m_submissionRequests.size();
  }


  /** Query the status of all queues.
   *  This call may ask the worker nodes for the current status, thus it could block.
   */
  void Scheduler_impl::checkWorkerAndRemoveDeadOnes(CORBA::ULong &jobs_waiting_for_data,
						    CORBA::ULong &jobs_waiting,
						    CORBA::ULong &jobs_running,
						    CORBA::ULong &empty_slots,
						    unsigned int remove_after_n_missing_responses )
  {
    jobs_waiting_for_data=0;
    jobs_waiting=0;
    jobs_running=0;
    empty_slots=0;
    unsigned int dead_worker=0;
    std::string dead_worker_names;
    bool remove_worker=false;
    unsigned int n_worker=0;
    {
      Lock worker_lock(m_herdMutex);
      for (std::vector< std::shared_ptr<WorkerInfo_t > >::iterator worker_iter = m_herd.begin();
	   worker_iter != m_herd.end();
	   worker_iter++) {

	if ((*worker_iter)->toBeRemoved()) {

	  // this worker already got the restartsignal
	  // so it just should be restarted.
	  remove_worker=true;
	  continue;
	}

	try {
	  CORBA::ULong jobs_waiting_for_data_on_worker;
	  CORBA::ULong jobs_waiting_on_worker;
	  CORBA::ULong jobs_running_on_worker;
	  CORBA::ULong jobs_executed_on_worker;
	  CORBA::ULong empty_slots_on_worker;

	  (*worker_iter)->worker()->getQueueStatus(jobs_waiting_for_data_on_worker,
						   jobs_waiting_on_worker,
						   jobs_running_on_worker,
						   jobs_executed_on_worker,
						   empty_slots_on_worker);

	  (*worker_iter)->setQueueStatus(static_cast<unsigned int>(jobs_waiting_for_data_on_worker),
					 static_cast<unsigned int>(jobs_waiting_on_worker),
					 static_cast<unsigned int>(jobs_running_on_worker),
					 static_cast<unsigned int>(jobs_executed_on_worker),
					 static_cast<unsigned int>(empty_slots_on_worker));

	    if (m_logLevel>kDiagnostics5Log) {
	      std::stringstream message;
	      message << "Worker " << (*worker_iter)->name() << " : waiting " << jobs_waiting_for_data_on_worker
		      << " ready=" << jobs_waiting_on_worker
		      << " running=" << jobs_running_on_worker
		      << " executed=" << jobs_executed_on_worker
		      << " empty=" << empty_slots_on_worker
		      << " -> hanging since " << (::time(0)-(*worker_iter)->hangingSince()) << "s.";
// 	      Lock mrs_lock(m_mrsMutex);
// 	      sendMessage(*m_out,"SCHED_worker_status",s_componentName, MRS_DIAGNOSTIC, "CAN::Scheduler_impl::getQueueStatus",message.str());
	      ers::debug(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::getQueueStatus",message.str()));
	    }

	  jobs_waiting_for_data += jobs_waiting_for_data_on_worker;
	  jobs_waiting          += jobs_waiting_on_worker;
	  jobs_running          += jobs_running_on_worker;
	  empty_slots           += empty_slots_on_worker;
	  
	  if (empty_slots_on_worker+jobs_running_on_worker>0) {
	    n_worker++;
	  }

	  if ((*worker_iter)->missingResponses()>0) {

	    if (m_logLevel>=kInfoLog) {
	      std::stringstream message;
	      message << "Worker " << (*worker_iter)->name() << " is back again.";
// 	      Lock mrs_lock(m_mrsMutex);
// 	      sendMessage(*m_out,"SCHED_worker_reappeared",s_componentName, MRS_INFORMATION, "CAN::Scheduler_impl::getQueueStatus",message.str());
	      ers::info(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::getQueueStatus",message.str()));
	    }
	    (*worker_iter)->resetMissingResponse();
	  }

	}
	catch(...) {

	  if ((*worker_iter)->missingResponses()==0 &&  m_logLevel>=kErrorLog) {
	    std::stringstream message;
	    message << "Failed to communicate with Worker " << (*worker_iter)->name() << ". Presumably, it is dead.";
// 	    Lock mrs_lock(m_mrsMutex);
// 	    sendMessage(*m_out,"SCHED_worker_dead",s_componentName, MRS_ERROR, "CAN::Scheduler_impl::getQueueStatus",message.str());
	    ers::error(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::getQueueStatus",message.str()));
	  }

	  if (remove_after_n_missing_responses>0 || (*worker_iter)->missingResponses()==0) {

	    (*worker_iter)->incMissingResponse();
	    // mark unresponsive worker for removal
	    if ( remove_after_n_missing_responses>=0
		 && (*worker_iter)->missingResponses() > remove_after_n_missing_responses
		 /*&& (*worker_iter)->nFreeSlots() == (*worker_iter)->nSlots()*/) {
	      if (!dead_worker_names.empty()) {
		dead_worker_names += ", ";
	      }
	      dead_worker_names += (*worker_iter)->name();
	      (*worker_iter)->markForRemoval();
	      remove_worker=true;
	    }

// 	    std::stringstream message;
// 	    message << "Worker " << (*worker_iter)->name() << " did not respond " << (*worker_iter)->missingResponses() << " times.";
// 	    Lock mrs_lock(m_mrsMutex);
// 	    sendMessage(*m_out,"SCHED_worker_dead",s_componentName, MRS_DIAGNOSTIC, "CAN::Scheduler_impl::getQueueStatus",message.str());

	  }

	  dead_worker++;
	}

      }

    }
    if (remove_worker) {

      if (m_logLevel>=kWarnLog) {
	std::stringstream message;
	message << "Will remove the worker : "<<  dead_worker_names << ".";
	{
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"SCHED_worker_dead",s_componentName, MRS_WARNING, "CAN::Scheduler_impl::getQueueStatus",message.str());
	  ers::warning(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::getQueueStatus",message.str()));
	}
      }

      removeWorker();
    }
    if (m_nWorker>n_worker) {
      m_nWorker=n_worker;
      m_submissionProcessor->newRequest();
    }
    m_nWorker=n_worker;
  }

  void Scheduler_impl::removeWorker()
  {
    Lock worker_lock(m_herdMutex);
    std::vector< std::shared_ptr<WorkerInfo_t > > new_herd;
    for (std::vector< std::shared_ptr<WorkerInfo_t > >::iterator worker_iter = m_herd.begin();
	 worker_iter != m_herd.end();
	 worker_iter++) {

      // remove all worker which are marked for removal
      if ( !(*worker_iter)->toBeRemoved()) {
	new_herd.push_back( *worker_iter );
      }
      else {

	if (m_logLevel>=kInfoLog) {
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"SCHED_restart_worker",s_componentName,MRS_INFORMATION, "CAN::Scheduler_impl::getQueueStatus",
	  ers::info(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::getQueueStatus",
		      "Signal worker to restart and remove from list."));
	}

	try {
	  (*worker_iter)->worker()->restart();
	}
	catch (...) {
	}

      }
    }
    m_herd = new_herd;

    // update the list of preferred worker
    for (std::map< std::string, std::vector< std::shared_ptr<WorkerInfo_t > > >::iterator rod_iter = m_preferredWorker.begin();
	 rod_iter != m_preferredWorker.end();
	 rod_iter++) {

      bool recreate_preferred_worker_list = false;
      for (std::vector< std::shared_ptr<WorkerInfo_t > >::iterator worker_iter = rod_iter->second.begin();
	   worker_iter != rod_iter->second.end();
	   worker_iter++) {
	if ( (*worker_iter)->toBeRemoved()) {
	  if (rod_iter->second.size()==1) {
	    rod_iter->second.clear();
	    break;
	  }
	  else {
	    recreate_preferred_worker_list = true;
	  }
	}
      }

      if (recreate_preferred_worker_list) {
	std::vector< std::shared_ptr<WorkerInfo_t > > new_preferred_worker_list;
	for (std::vector< std::shared_ptr<WorkerInfo_t > >::iterator worker_iter = rod_iter->second.begin();
	     worker_iter != rod_iter->second.end();
	     worker_iter++) {
	  if ( !(*worker_iter)->toBeRemoved()) {
	    new_preferred_worker_list.push_back( *worker_iter );
	  }
	}
	rod_iter->second = new_preferred_worker_list;
      }
    }

  }


  /** Register a worker node with the scheduler
   * @param obj an object of type CAN::Worker
   */
  unsigned short Scheduler_impl::registerWorker( CORBA::Object_ptr obj , const char *identity, CORBA::ULong n_slots) {
    Worker_var a_worker = Worker::_narrow(obj);
    if (!CORBA::is_nil (a_worker) ) {
      if (n_slots>0) {

	if (m_logLevel>=kInfoLog) {
	  std::stringstream message;
	  message << "Registered worker " << identity << "(" << std::hex << static_cast<void *>(a_worker.in()) << std::dec << ") with " << n_slots << " processing slots.";
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"SCHED_new_worker",s_componentName,MRS_INFORMATION, "CAN::Scheduler_impl::registerWorker", message.str());
	  ers::info(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::registerWorker", message.str()));
	}

	std::shared_ptr<WorkerInfo_t> new_worker(new WorkerInfo_t(a_worker._retn(), identity,n_slots));
	{
	  Lock lock(m_herdMutex);
	  if (new_worker->name().size()==0) {
	    std::stringstream new_name;
	    new_name << m_herd.size();
	    new_worker->setName(new_name.str());
	  }
	  m_herd.push_back(new_worker);
	  m_nWorker++;
	  m_submissionProcessor->newRequest();
	}
      }
    }
    else if (m_logLevel>=kErrorLog) {
//       Lock mrs_lock(m_mrsMutex);
//       sendMessage(*m_out,"SCHED_wrong_worker_obj",s_componentName,MRS_ERROR, "CAN::Scheduler_impl::registerWorker",
      ers::error(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::registerWorker",
		  "Tried to register non existing object or an object which is not of type Worker."));
    }
    return m_logLevel;
  }

  /** Deregister a worker node with the scheduler
   * @param obj an object of type CAN::Worker
   */
  void Scheduler_impl::deregisterWorker( CORBA::Object_ptr obj) {
    Worker_ptr a_worker;
    try {
      a_worker = Worker::_narrow(obj);
    }
    catch (...) {
      if (m_logLevel>=kWarnLog) {
	std::stringstream message;
	message << "Object given for deregistration does not seem to refer to a valid workds : " << std::hex << static_cast<void *>(obj) << std::dec << ".";
// 	Lock mrs_lock(m_mrsMutex);
// 	sendMessage(*m_out,"SCHED_deregister_worker_failed",s_componentName, MRS_WARNING, "CAN::Scheduler_impl::deregisterWorker", message.str());
	ers::warning(PixLib::pix::daq (ERS_HERE,"CAN::Scheduler_impl::deregisterWorker", message.str()));
      }
      return;
    }

    CORBA::release(obj);
    if (!CORBA::is_nil (a_worker) ) {

      if (m_logLevel>=kDiagnosticsLog) {
      	std::stringstream message;
	message << "Try to Deregistered worker " << std::hex << static_cast<void *>(a_worker) << std::dec << ".";
// 	Lock mrs_lock(m_mrsMutex);
// 	sendMessage(*m_out,"SCHED_try_deregister_worker",s_componentName, MRS_DIAGNOSTIC, "CAN::Scheduler_impl::deregisterWorker", message.str());
	ers::error(PixLib::pix::daq (ERS_HERE,"CAN::Scheduler_impl::deregisterWorker", message.str()));
      }

      Lock lock(m_herdMutex);
      for (std::vector< std::shared_ptr<WorkerInfo_t > >::iterator worker_iter = m_herd.begin();
	   worker_iter != m_herd.end();
	   worker_iter++) {

	if ( a_worker->_is_equivalent((*worker_iter)->worker())) {
	  std::stringstream message;
	  message << "Deregistered worker " << (*worker_iter)->name() << ". ";

	  // @todo have tp reschedule all associated jobs
	  m_herd.erase(worker_iter);
	  if (m_nWorker>0) --m_nWorker;
	  if (m_logLevel>=kInfoLog) {
// 	    Lock mrs_lock(m_mrsMutex);
// 	    sendMessage(*m_out,"SCHED_deregistered_worker",s_componentName, MRS_INFORMATION, "CAN::Scheduler_impl::deregisterWorker",
	    ers::info(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::deregisterWorker",
			message.str()));
	  }

	  break;
	}

      }

    }
    else if (m_logLevel>=kErrorLog) {
//       Lock mrs_lock(m_mrsMutex);
//       sendMessage(*m_out,"SCHED_wrong_worker_obj",s_componentName,MRS_ERROR, "CAN::Scheduler_impl::deregisterWorker",
      ers::error(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::deregisterWorker",
		  "Tried to deregister non existing object or an object not of type Worker."));
    }
  }

  /** Remove a worker from the internal list
   * @param a_worker pointer to a CAN::Worker
   */
  void Scheduler_impl::removeWorker( Worker_ptr a_worker)
  {

    if (m_logLevel>=kDiagnosticsLog) {
      std::stringstream message;
      message << "Try to Deregistered worker " << std::hex << static_cast<void *>(a_worker) << std::dec << ".";
//       Lock mrs_lock(m_mrsMutex);
//       sendMessage(*m_out,"SCHED_try_deregister_worker",s_componentName, MRS_DIAGNOSTIC, "CAN::Scheduler_impl::deregisterWorker", message.str());
      ers::debug(PixLib::pix::daq (ERS_HERE,"CAN::Scheduler_impl::deregisterWorker", message.str()));
    }

    Lock lock(m_herdMutex);
    for (std::vector< std::shared_ptr<WorkerInfo_t > >::iterator worker_iter = m_herd.begin();
	 worker_iter != m_herd.end();
	 worker_iter++) {

      if ( a_worker == (*worker_iter)->worker()) {
	std::stringstream message;
	message << "Deregistered worker " << (*worker_iter)->name() << ". ";

	// @todo have tp reschedule all associated jobs
	m_herd.erase(worker_iter);

	if (m_logLevel>=kInfoLog) {
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"SCHED_deregistered_worker",s_componentName, MRS_INFORMATION, "CAN::Scheduler_impl::deregisterWorker",
	  ers::info(PixLib::pix::daq (ERS_HERE,"CAN::Scheduler_impl::deregisterWorker",
		      message.str()));
	}

	break;
      }

    }
  }


  bool Scheduler_impl::sendAbortAnalysis(SerialNumber_t analysis_serial_number)
  {
    std::shared_ptr<AnalysisJobList_t> analysis_job_list = m_analysisMonitor.searchJobList(analysis_serial_number);
    if (!analysis_job_list.get()) return false;

    //    // build the rod name list and call the abort function of the worker

    // abort the given analysis on all workers on which jobs are registerd.
    // Call abortRequested on all those jobs, to indicate the monitoring process that
    // the jobs do not need to terminate successfully.
    StringVec rod_name_list;
    Lock worker_lock(m_herdMutex);
    for (std::vector< std::shared_ptr<WorkerInfo_t > >::iterator worker_iter = m_herd.begin();
	 worker_iter != m_herd.end();
	 worker_iter++) {

      //rod_name_list.length(analysis_jobs->second.size());
      unsigned int n_rods=0;
      for (AnalysisJobList_t::iterator job_iter = analysis_job_list->begin();
	   job_iter != analysis_job_list->end();
	   job_iter++) {
	// @todo verify that this comparison really works !
	//  alternatively (*worker_iter).get() ==  job_iter->second.get()
	if (!job_iter->second.markedForResubmission()) {

	  if (!job_iter->second.hasWorker()) {
	    std::stringstream message;
	    message << "Job for " << job_iter->first << " of " << makeAnalysisSerialNumberString(analysis_serial_number) 
		    << " is not assigned to any worker but also not marked for resubmission.";
	    if (m_logLevel>=kWarnLog) {
// 	      Lock mrs_lock(m_mrsMutex);
// 	      sendMessage(*m_out,"SCHED_bug",s_componentName, MRS_WARNING, "CAN::Scheduler_impl::abortAnalysis",message.str());
	      ers::warning(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::abortAnalysis",message.str()));
	    }
	    continue;
	  }

	  if ( (*worker_iter)->worker()->_is_equivalent(job_iter->second.worker()) ) {
	    n_rods++;
	    job_iter->second.abortRequested();
	    //	  rod_name_list[n_rods++]=CORBA::string_dup( job_iter->first );
	  }
	}
      }
      //      rod_name_list.length(n_rods);
      if (n_rods>0) {
	try {
	  std::stringstream message;
	  message << "Send abort of " << analysis_serial_number << " to worker " << (*worker_iter)->name() << ".";
	  if (m_logLevel>=kDiagnosticsLog) {
// 	    Lock mrs_lock(m_mrsMutex);
// 	    sendMessage(*m_out,"SCHED_abortreq",s_componentName, MRS_DIAGNOSTIC, "CAN::Scheduler_impl::abortAnalysis",message.str());
	    ers::debug(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::abortAnalysis",message.str()));
	  }
	  (*worker_iter)->worker()->abortAnalysis(analysis_serial_number, false);
	}
	catch(...) {
	  if (m_logLevel>=kDiagnosticsLog) {
	    std::stringstream message;
	    message << "Failed to abort analysis " << analysis_serial_number << " of worker " << (*worker_iter)->name() << ". Presumably the worker is dead.";
// 	    Lock mrs_lock(m_mrsMutex);
// 	    sendMessage(*m_out,"SCHED_worker_dead",s_componentName, MRS_ERROR, "CAN::Scheduler_impl::abortAnalysis",message.str());
	    ers::error(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::abortAnalysis",message.str()));
	  }
	}
      }
    }
    return true;
  }


  class IsAnalysisSubmissionRequest
  {
  public:

    IsAnalysisSubmissionRequest(SerialNumber_t analysis_serial_number) 
      : m_serialNumber(analysis_serial_number) 
    {}

    bool operator()(SubmissionRequest_t &a) {
      return a.analysisSerialNumber() == m_serialNumber;
    }

  private:
    SerialNumber_t m_serialNumber;
  };

  void Scheduler_impl::abortAnalysis(SerialNumber_t analysis_serial_number)
  {
    if (!sendAbortAnalysis(analysis_serial_number)) {
      // if not yet monitored the analysis could still be in the submission request queue
      Lock lock(m_submissionQueueMutex);
      std::deque< SubmissionRequest_t >::iterator submission_request = find_if( m_submissionRequests.begin(),
										m_submissionRequests.end(),
										IsAnalysisSubmissionRequest(analysis_serial_number));
      if (submission_request != m_submissionRequests.end()) {
	submission_request->setAbort();
      }
      return;
    }

    m_analysisMonitor.markForAbortion(analysis_serial_number);
  }


  void Scheduler_impl::shutdown() {
    {
      Lock lock(m_herdMutex);
      for (std::vector< std::shared_ptr<WorkerInfo_t > >::iterator worker_iter = m_herd.begin();
	   worker_iter != m_herd.end();
	   worker_iter++) {
	std::stringstream message1;
	message1 << "Shutdown worker " << std::hex << static_cast<void *>((*worker_iter)->worker()) << ".";
	if (m_logLevel>=kDiagnosticsLog) {
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"SCHED_shutdown_worker",s_componentName,MRS_DIAGNOSTIC, "CAN::Scheduler_impl::shutdown",message1.str());
	  ers::debug(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::shutdown",message1.str()));
	}
	try {
	  (*worker_iter)->worker()->shutdown();
	}
	catch (std::exception &err ) {
	  if (m_logLevel>=kFatalLog) {
	    std::stringstream message2;
	    message2 << " Failed to shutdown worker. Reason : " << err.what();
// 	    Lock mrs_lock(m_mrsMutex);
// 	    sendMessage(*m_out,"SCHED_shutdown_worker_failed",s_componentName,MRS_FATAL, "CAN::Scheduler_impl::shutdown",message2.str());
	    ers::fatal(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::shutdown",message2.str()));
	  }
	}
	catch (...) {
	  if (m_logLevel>=kFatalLog) {
// 	    Lock mrs_lock(m_mrsMutex);
// 	    sendMessage(*m_out,"SCHED_shutdown_worker_failed",s_componentName,MRS_FATAL, "CAN::Scheduler_impl::shutdown", "Failed to shutdown worker. Reason unknown.");
	    ers::fatal(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::shutdown", "Failed to shutdown worker. Reason unknown."));
	  }
	}
      }
      m_herd.clear();
    }

    if (m_logLevel>=kInfoLog) {
//       Lock mrs_lock(m_mrsMutex);
//       sendMessage(*m_out,"SCHED_shutdown",s_componentName,MRS_INFORMATION, "CAN::Worker_impl::shutdown", "Shutdown scheduler." );
      ers::info(PixLib::pix::daq (ERS_HERE, "CAN::Worker_impl::shutdown", "Shutdown scheduler." ));
    }
    m_analysisMonitor.abort();

    IPCServerInstance::stop();
  }

  void Scheduler_impl::pingWorker() {
    {
      if (m_logLevel>=kDiagnosticsLog) {
// 	Lock mrs_lock(m_mrsMutex);
// 	sendMessage(*m_out,"SCHED_worker_alive",s_componentName,MRS_DIAGNOSTIC, "CAN::Scheduler_impl::pingWorker","Check vitality of all workers.");
	ers::debug(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::pingWorker","Check vitality of all workers."));
      }
      Lock lock(m_herdMutex);
      for (std::vector< std::shared_ptr<WorkerInfo_t > >::iterator worker_iter = m_herd.begin();
	   worker_iter != m_herd.end();
	   worker_iter++) {
	bool alive=false;
	try {
	  alive = (*worker_iter)->worker()->alive();
	}
	catch(...) {
	}

	if (alive) {
	  std::stringstream message;
	  message << "Worker " << (*worker_iter)->name() << " still alive.";
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"SCHED_worker_alive",s_componentName, MRS_DIAGNOSTIC, "CAN::Scheduler_impl::pingWorker",message.str());
	  ers::debug(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::pingWorker",message.str()));
	}
	else {
	  std::stringstream message;
	  message << "Worker " << (*worker_iter)->name() << " does not respond.";
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"SCHED_worker_dead",s_componentName, MRS_ERROR, "CAN::Scheduler_impl::pingWorker",message.str());
	  ers::error(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::pingWorker",message.str()));
	}
      }
    }
  }

  void Scheduler_impl::reset() {
    Lock worker_lock(m_herdMutex);
    for (std::vector< std::shared_ptr<WorkerInfo_t > >::iterator worker_iter = m_herd.begin();
	 worker_iter != m_herd.end();
	 worker_iter++) {
      try {
	(*worker_iter)->worker()->reset();
      }
      catch(...) {
	if (m_logLevel>=kErrorLog) {
	  std::stringstream message;
	  message << "Failed to reset worker " << (*worker_iter)->name() << ".";
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"SCHED_worker_dead",s_componentName, MRS_ERROR, "CAN::Scheduler_impl::pingWorker",message.str());
	  ers::error(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::pingWorker",message.str()));
	}
      }
    }
    m_analysisMonitor.reset();
  }

  void Scheduler_impl::restartWorker() {
    m_nWorker=0;
    Lock worker_lock(m_herdMutex);
    for (std::vector< std::shared_ptr<WorkerInfo_t > >::iterator worker_iter = m_herd.begin();
	 worker_iter != m_herd.end();
	 worker_iter++) {
      try {
	(*worker_iter)->worker()->restart();
      }
      catch(...) {
	if (m_logLevel>=kErrorLog) {
	  std::stringstream message;
	  message << "Failed to reset worker " << (*worker_iter)->name() << ".";
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"SCHED_worker_dead",s_componentName, MRS_ERROR, "CAN::Scheduler_impl::pingWorker",message.str());
	  ers::error(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::pingWorker",message.str()));
	}
      }
    }
  }

  void Scheduler_impl::testWorker(unsigned int remove_after_n_missing_responses)
  {
    // test whether workers are still responsive and update internal memory of the
    // queue status

    CORBA::ULong jobs_waiting_for_data_unused;
    CORBA::ULong jobs_waiting_unused;
    CORBA::ULong jobs_running_unused;
    CORBA::ULong empty_slots_unused;

    checkWorkerAndRemoveDeadOnes(jobs_waiting_for_data_unused, jobs_waiting_unused, jobs_running_unused, empty_slots_unused, remove_after_n_missing_responses);

  }

  SubmissionRequest_t::SubmissionRequest_t(SerialNumber_t analysis_serial_number, const AnalysisInfoData_t &analysis_info)
    : m_analysisSerialNumber(analysis_serial_number),
      m_abort(false)
  {
    copyAnalysisInfo(analysis_info,m_analysisInfo);
  }

  void *Scheduler_impl::SubmissionProcessor::run_undetached(void *arg) {
    while (!m_shutdown) {
      if (m_scheduler->nSubmissionRequests()==0) {
	m_hasRequests.wait(m_shutdown);
      }
      else if (m_wait>0) {
	m_hasRequests.timedwait(m_wait, m_shutdown);
      }
      if (m_shutdown) break;
      m_scheduler->processSubmissionRequests();
    }
    m_exit.setFlag();
    return NULL;
  }

  void Scheduler_impl::SubmissionProcessor::shutdownWait() {
    m_shutdown=true;
    m_hasRequests.setFlag();

    if (state()!=omni_thread::STATE_TERMINATED) {
      m_exit.wait();
      while (state()!=omni_thread::STATE_TERMINATED) {
	usleep(1000);
      }
    }
  }

  void Scheduler_impl::setLogLevel(unsigned short level) {
    m_logLevel=static_cast<ELogLevel>(level);
    m_analysisMonitor.setLogLevel(m_logLevel);

    Lock worker_lock(m_herdMutex);
    for (std::vector< std::shared_ptr<WorkerInfo_t > >::iterator worker_iter = m_herd.begin();
	 worker_iter != m_herd.end();
	 worker_iter++) {
      try {
	(*worker_iter)->worker()->setLogLevel(level);
      }
      catch(...) {
	if (m_logLevel>=kErrorLog) {
	  std::stringstream message;
	  message << "Failed to conntact worker " << (*worker_iter)->name() << " to change log level.";
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"SCHED_worker_dead",s_componentName, MRS_ERROR, "CAN::Scheduler_impl::setLogLevel",message.str());
	  ers::error(PixLib::pix::daq (ERS_HERE, "CAN::Scheduler_impl::setLogLevel",message.str()));
	}
      }
    }
    
  }

}
