#ifndef _CAN_IAnalysisPerROD_hh_
#define _CAN_IAnalysisPerROD_hh_

#include <string>
#include <ConfigWrapper/Locations.h>
#include "Common.hh"
#include "IAnalysisObject.hh"


namespace CAN {

  class IAnalysisDataServer;
  class ExtendedAnalysisResultList_t;
  class AnalysisPerPP0;
  class AnalysisPerROD;

  /** Very base interface of the analysis class.
   */
  class IAnalysisBase : public IAnalysisObject
  {
  public:
    friend class AnalysisPerPP0;
    friend class AnalysisPerROD;

    /** Return the name of the analysis.
     */
    virtual const std::string &name() const = 0;

    /** A unique identifier of an analysis. Could for example be the scan id.
     * This identifier is used to localise histograms.
     */
    virtual SerialNumber_t serialNumber() const = 0;

    /** If called the analysis should abort. 
     * This method will be called from a different thread, so here 
     * only a flag should be set.
     * @sa DefaultAnalysisBase
     */
    virtual void requestAbort() = 0;

  protected:
    virtual bool abortRequested() const = 0;

  };


  /** The interface of analysis which analyse data per rod.
   */
  class IAnalysisPerROD : public IAnalysisBase
  {
  public:

    /** can be used to ask server to prefetch histograms in the background.
     * All histograms etc. which are used by an anlysis need to be requested
     * All requests have to be made in this method.
     */
    virtual void requestHistograms(IAnalysisDataServer *data_server,
				   const PixA::RODLocationBase &rod_location) = 0;

    /** Analyse the requested histograms.
     * Once all requested histograms etc. are available in the data server the execution
     * unit will call the analyse method
     * @sa ExecutionUnit.
     */    
    virtual void analyse(IAnalysisDataServer *data_server,
			 const PixA::RODLocationBase &rod_location,
			 ExtendedAnalysisResultList_t &analysis_results) = 0;
  };
}

#endif
