#ifndef _CAN_for_each_hh_
#define _CAN_for_each_hh_

#include <ConfigWrapper/PixDisableUtil.h>
#include <ConfigWrapper/Connectivity.h>
#include <string>

namespace CAN {

  /** execute a function for all enabled modules.
   * The function object UnaryFunction_tmpl must contain a method:
   * <verb>
   *     void operator()( const PixA::ModuleList::const_iterator &module_iter) ;
   * </verb>
   */
  template <class UnaryFunction_templ>
  inline void for_each_module(const PixA::DisabledList module_disabled_list,
		       PixA::ModuleList::const_iterator module_list_begin,
		       PixA::ModuleList::const_iterator module_list_end,
		       UnaryFunction_templ &a_function) {

    for (PixA::ModuleList::const_iterator module_iter=module_list_begin;
	 module_iter != module_list_end;
	 ++module_iter) {

      if (!PixA::enabled( module_disabled_list, module_iter )) continue;
      a_function( module_iter );
    }
  }

  /** execute a function for all enabled modules.
   * The function object UnaryFunction_tmpl must contain a method:
   * <verb>
   *     void operator()( const PixA::ModuleList::const_iterator &module_iter) ;
   * </verb>
   */
  template<class UnaryFunction_templ>
  inline void for_each_module(const PixA::DisabledList pp0_disabled_list,
		       PixA::Pp0List::const_iterator pp0_list_begin,
		       PixA::Pp0List::const_iterator pp0_list_end,
		       UnaryFunction_templ &a_function) {

    for (PixA::Pp0List::const_iterator pp0_iter=pp0_list_begin;
	 pp0_iter != pp0_list_end;
	 ++pp0_iter) {

      if (!PixA::enabled( pp0_disabled_list, pp0_iter )) continue;
      const PixA::DisabledList module_disabled_list( moduleDisabledList(pp0_disabled_list, pp0_iter) );

      PixA::ModuleList::const_iterator module_list_begin=modulesBegin(pp0_iter);
      PixA::ModuleList::const_iterator module_list_end=modulesEnd(pp0_iter);

      for_each_module(module_disabled_list, module_list_begin, module_list_end, a_function);

    }
  }



  /** Execute a function for all enabled PP0s.
   * The function object UnaryFunction_tmpl must contain a method:
   * <verb>
   *     void operator()( const PixA::Pp0List::const_iterator &pp0_iter) ;
   * </verb>
   */
  template<class UnaryFunction_templ>
  inline void for_each_pp0(const PixA::DisabledList pp0_disabled_list,
			   PixA::Pp0List::const_iterator pp0_list_begin,
			   PixA::Pp0List::const_iterator pp0_list_end,
			   UnaryFunction_templ &a_function) {

    for (PixA::Pp0List::const_iterator pp0_iter=pp0_list_begin;
	 pp0_iter != pp0_list_end;
	 ++pp0_iter) {

      if (!PixA::enabled( pp0_disabled_list, pp0_iter )) continue;
      a_function(pp0_iter);
    }
  }


  /** execute a function for all enabled modules.
   * The function object UnaryFunction_tmpl must contain a method:
   * <verb>
   *     void operator()( const PixA::ModuleList::const_iterator &module_iter) ;
   * </verb>
   */
  template<class UnaryFnction_templ>
  inline void for_each_module(const PixA::ConnectivityRef &conn,
			      const PixA::RODLocationBase &rod_location,
			      const PixA::DisabledList &pp0_disabled_list,
			      UnaryFnction_templ &a_function) {
    assert(conn);
    const PixA::Pp0List pp0_list( conn.pp0s(rod_location) );
    PixA::Pp0List::const_iterator pp0_list_begin=pp0_list.begin();
    PixA::Pp0List::const_iterator pp0_list_end=pp0_list.end();

    for_each_module(pp0_disabled_list, pp0_list_begin, pp0_list_end,a_function);
  }

  /** Execute a function for all enabled PP0s.
   * The function object UnaryFunction_tmpl must contain a method:
   * <verb>
   *     void operator()( const PixA::Pp0List::const_iterator &pp0_iter) ;
   * </verb>
   */
  template<class UnaryFnction_templ>
  inline void for_each_pp0(const PixA::ConnectivityRef &conn,
			   const PixA::RODLocationBase &rod_location,
			   const PixA::DisabledList &pp0_disabled_list,
			   UnaryFnction_templ &a_function) {

      const PixA::Pp0List pp0_list( conn.pp0s(rod_location) );
      PixA::Pp0List::const_iterator pp0_list_begin=pp0_list.begin();
      PixA::Pp0List::const_iterator pp0_list_end=pp0_list.end();

      for_each_pp0(pp0_disabled_list, pp0_list_begin, pp0_list_end,a_function);
  }

#  ifdef _CAN_ExtendedAnalysisResultList_t_hh_
  /** Base class for a simple per module analysis
   * The derived class needs to implement a function 
   *     void operator()( const PixA::ModuleList::const_iterator &module_iter);
   * which then calls calls setModuleStatus.
   */
  class ModuleAnalysisBase : public std::unary_function<const PixA::ModuleList::const_iterator &, void>
  {
  public:
    ModuleAnalysisBase(const PixA::RODLocationBase &rod_location, ExtendedAnalysisResultList_t &analysis_results)
      : m_rodLocation(rod_location),
	m_analysisResults(&analysis_results),
        m_combinedStatus(true),
	m_hasModules(false)
    {}

    ~ModuleAnalysisBase() { setCombinedStatus(); }

    // void operator()( const PixA::ModuleList::const_iterator &module_iter);

  protected:


    void setModuleStatus(const std::string &module_name, bool status) {
      m_analysisResults->addValue("Status",module_name, status);
      m_combinedStatus &= status;
      m_hasModules=true;
    }

    void setModuleStatus(const PixA::ModuleList::const_iterator &module_iter, bool status) {
      setModuleStatus(PixA::connectivityName(module_iter), status);
    }


    ExtendedAnalysisResultList_t &resultList() { return *m_analysisResults;}
    const PixA::RODLocationBase &rodLocation() const { return m_rodLocation;}

  private:

    void setCombinedStatus() {
      bool combined_result = m_combinedStatus & m_hasModules;
      m_analysisResults->addValue("Status",m_rodLocation, combined_result);
    }

    const PixA::RODLocationBase   m_rodLocation;
    ExtendedAnalysisResultList_t *m_analysisResults;

    bool m_combinedStatus;
    bool m_hasModules;
  };
#  endif

#  ifdef _CAN_IDataServer_hh_

#  include <algorithm>

  /** Helper class to request module configurations.
   */
  class RequestModuleConfig : std::unary_function< PixA::ModuleList::const_iterator, void>
  {
  public:
    RequestModuleConfig(IDataServer *data_server)
      : m_dataServer(data_server)
    {}

    void operator()( const PixA::ModuleList::const_iterator &module_iter)  {
      m_dataServer->requestModuleConfig( PixA::connectivityName( module_iter ) );
    }

  private:
    IDataServer *m_dataServer;
  };

#    ifdef _PixA_ConnectivityUtil_h_


  class RequestHistograms : std::unary_function< PixA::ModuleList::const_iterator, void>
  {
  protected:

    class RequestHistogramsForModule : public std::unary_function<const std::string &, void>
    {
    public:
      RequestHistogramsForModule(IDataServer *data_server,
			const std::string &module_name)
	: m_dataServer(data_server),
	  m_moduleName(module_name)
      {}

      void operator()(const std::string &histo_name) {
	std::cout << "INFO [RequestHistograms::RequestHistogramsForModule::operaotr(histo_iter)] request histogram " << histo_name
		  << " for module " << m_moduleName << "." << std::endl;
	m_dataServer->requestHistogram(histo_name, m_moduleName );
      }
    private:
      IDataServer *m_dataServer;
      const std::string m_moduleName;
    };

  public:
    RequestHistograms(IDataServer *data_server,
		      bool request_module_config=false)
      : m_dataServer(data_server),
	m_requestModuleConfig(request_module_config)
    {}

    RequestHistograms(IDataServer *data_server,
		      const std::vector<std::string> &histograms,
		      bool request_module_config=false)
      : m_dataServer(data_server),
	m_histogramList(histograms),
	m_requestModuleConfig(request_module_config)
    {}

    RequestHistograms(IDataServer *data_server,
		      const std::string &histogram,
		      bool request_module_config=false)
      : m_dataServer(data_server),
	m_requestModuleConfig(request_module_config)
    { m_histogramList.push_back(histogram);}

    void addHistogram(const std::string &histogram_name) {
      m_histogramList.push_back(histogram_name);
    }

    void operator()( const PixA::ModuleList::const_iterator &module_iter)  {
      for_each(m_histogramList.begin(), m_histogramList.end(), RequestHistogramsForModule(m_dataServer, PixA::connectivityName( module_iter )));
      if (m_requestModuleConfig) {
	std::cout << "INFO [RequestHistograms::RequestHistograms::operaotr(histo_iter)] request module config for " << PixA::connectivityName( module_iter ) << "." << std::endl;
	m_dataServer->requestModuleConfig(PixA::connectivityName( module_iter ));
      }
    }

  private:
    IDataServer *m_dataServer;
    std::vector<std::string> m_histogramList;
    bool m_requestModuleConfig;
  };

#    endif
#  endif
}

#endif
