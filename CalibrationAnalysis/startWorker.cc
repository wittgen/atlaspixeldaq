#include <ipc/partition.h>
//#include <ipc/server.h>
#include <ipc/core.h>
#include "Worker_impl.hh"

//#include <mrs/message.h>
#include "exception.hh"
//#include "sendMessage.hh"

#include "ConfigWrapper/ConnectivityManager.h"
#include "ConfigWrapper/DbManager.h"
#include "MetaDataService.hh"

//#include <omniORB4/internal/giopStream.h>

#include <memory>

#include "daemonise.hh"

#include "MasterDataServerCT.hh"
#include "Common.hh"


int main(int argc, char **argv)
{
  // GET COMMAND LINE


  std::string ipcPartitionName = argv[1];
  //  std::string mrsPartitionName;
  std::string ident;
  const char *obj_id = getenv("TDAQ_APPLICATION_OBJECT_ID");
  if (obj_id!=0) {
    ident=obj_id;
  }
  else {
    std::string hostname(1024,'\0');
    gethostname(&(hostname[0]),hostname.size());
    hostname.erase(strlen(hostname.c_str()));
    ident="rat_";
    ident+=hostname;
    std::cout << "TDAQ_APPLICATION_OBJECT_ID: " << ident << std::endl;
    setenv("TDAQ_APPLICATION_OBJECT_ID",ident.c_str(),0);
  }

  unsigned int n_slots=1;
  unsigned int n_part=0;
  bool do_daemonise=false;
  std::string path,file;

  //  std::string *partitition_arr[2]={&ipcPartitionName,&mrsPartitionName};
  bool error=false;
  for (int arg_i=1; arg_i<argc; arg_i++) {
    if (strcmp(argv[arg_i],"-n")==0 && arg_i+1<argc) {
      n_slots = atoi(argv[++arg_i]);
    }
    else if (strcmp(argv[arg_i],"-d")==0) {
      do_daemonise=true;
    }
    else if (strcmp(argv[arg_i],"-i")==0 && arg_i+1<argc) {
      ident = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-p")==0 && arg_i+1<argc) {
      path = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-f")==0 && arg_i+1<argc) {
      file = argv[++arg_i];
    }
    else {
      if (n_part<2) {
	//	*(partitition_arr[n_part++])=argv[arg_i];
      }
      else {
	error=true;
      }
    }
  }
  if (n_part<1 || error) {
    std::cout << "USAGE: rat [-n #] \"partition name\" [partition name]" << std::endl;
    return 1;
  }

  std::cout << "INFO [" << argv[0] << ":main] Starting worker " << ident << " on " << ipcPartitionName << std::endl;
  //	    << ". Messages will go to MRS on partition " << (!mrsPartitionName.empty() ? mrsPartitionName : ipcPartitionName) << std::endl;

  if (do_daemonise) {

    int ret = daemonise();
    if (ret != 0 ) { 
      std::cout << "error while daeminzing" << std::endl;
      return ret;
    }
  }

  // START IPC
  // Start IPCCore
  IPCCore::init(argc, argv);

  // Create IPCPartition
  std::cout << "Getting partition " << ipcPartitionName << std::endl;
  std::auto_ptr<IPCPartition> ipcPartition (new IPCPartition(ipcPartitionName.c_str()));
//   std::auto_ptr<IPCPartition> mrsPartition;
//   if (!mrsPartitionName.empty()) {
//     mrsPartition = std::auto_ptr<IPCPartition>(new IPCPartition(mrsPartitionName.c_str()));
//   }

  
  try {
    //    IPCPartition *mrs_partition = &(mrsPartition.get() ? *(mrsPartition.get()) : *(ipcPartition.get()));
    //    CAN::MasterDataServerCT * masterDataServer(new CAN::MasterDataServerCT(*mrs_partition,path,file));
    CAN::MasterDataServerCT * masterDataServer(new CAN::MasterDataServerCT(path,file));
    CAN::SerialNumber_t analysis_serial_number(1111111);
    const PixA::RodLocationBase rod_conn_name("");
    PixA::ConnectivityRef conn;
    /// masterDataServer->createDataServerCT( analysis_serial_number, rod_conn_name, conn, path, file); //is done in ExecutionUnit !!! 
    
    std::cout << "before " << std::endl;
    IPCPartition *hh = ipcPartition.get();
    std::cout << "after " << std::endl;

    //    CAN::Worker_impl *worker(new CAN::Worker_impl( *hh, *mrs_partition, masterDataServer, ident, n_slots));
    CAN::Worker_impl *worker(new CAN::Worker_impl( *hh, masterDataServer, ident, n_slots));
  }
  catch (CAN::MRSException &err) {
    //MRSStream out((mrsPartition.get() ? *(mrsPartition.get()) : *(ipcPartition.get())));
    err.sendMessage();//out, std::string(argv[0])+":main" );
    std::cerr << "ERROR [" << argv[0] << ":main] Caught exception " << err.getDescriptor() << std::endl;
  }
  catch (std::exception &err) {
    std::cerr << "ERROR [" << argv[0] << ":main] Caught standard exception " << err.what() << std::endl;
  }

  PixA::ConnectivityManager::deepCleanup();
  CAN::MetaDataService::deepCleanup();
  PixA::DbManager::deepCleanup();
//   catch (omni::giopStream::CommFailure &err) {
//     MRSStream out((mrsPartition.get() ? *(mrsPartition.get()) : *(ipcPartition.get())));
//     std::stringstream message;
//     message << "Internal CORBA communication error " << err.minor() << " in " << err.filename() << ":" << err.linenumber() << ".";
//     CAN::sendMessage(out,"SCHEDALIVE",MRS_INFORMATION, "rat.cc:main", message.str());
//   }

  return 0;
}
