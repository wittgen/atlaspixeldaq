#include "PixScanHistoReceiver.hh"
#include "HistoArray.hh"

namespace CAN {

  void PixScanHistoReceiver::receive( OHRootHistogram & h )
  {
    std::string histogram_name( h.histogram->GetName() );
    std::string::size_type pos = ( histogram_name.size() < m_pixScanHistogramName.size() ? std::string::npos : histogram_name.find(m_pixScanHistogramName) );

    // the histogram name should contain the pix scan histogram name
    // plus the array indices plus the histogram name:
    // [pix_scan_histo_name]/(Ax/)(Bx/)(Cx/)x:name
    if ( pos == std::string::npos || histogram_name.size() <= pos+m_pixScanHistogramName.size() ) {
      std::cout << "ERROR [PixScanHistoReceiver::receive] Histogram does not start with the expected pix scan histo name." << std::endl;
      return;
    }

    PixA::Index_t index;
    pos += m_pixScanHistogramName.size();
    std::string::size_type end_pos=histogram_name.find(":",pos);
    if (end_pos == std::string::npos) {
      std::cout << "ERROR [PixScanHistoReceiver::receive] No end marker which separates the original histogram name." << std::endl;
      return;
    }

    bool reached_end=false;

    char *ptr_a = &(histogram_name[pos]);
    const char *ptr_end=&(histogram_name[end_pos]);

    // the levels are separated by '/'
    char sep='/';
    char level_marker='A';
    while (ptr_a < ptr_end) {

      // Although not excluded, up to now there havn't been any pix scan histos with more than 3 levels.
      if (index.size()>4) {
	std::cout << "ERROR [PixScanHistoReceiver::receive] Too many indices." << std::endl;
      }

      // Each pix scan histo index starts with a capital letter from A to C.
      if (*ptr_a == level_marker) {
	level_marker++;
	ptr_a++;
      }
      else {
	reached_end=true;
	// the original histogram name is separated by ':'
	sep=':';
      }
      char *ptr_b=ptr_a;
      int an_index = strtol(ptr_a, &ptr_b,10 );

      if (ptr_b-ptr_a==0 || ptr_b > ptr_end || *ptr_b!=sep) {
	std::cout << "ERROR [PixScanHistoReceiver::receive] Position of next index indicator out of range or incorrect separator." << std::endl;
	return;
      }
      if (an_index<0) {
	std::cout << "ERROR [PixScanHistoReceiver::receive] Invalid index." << std::endl;
	return;
      }
      if (*ptr_b==sep) ptr_b++;
      ptr_a=ptr_b;

      index.push_back( static_cast<unsigned int>(an_index) );
    }

    if (!reached_end) {
      std::cout << "ERROR [PixScanHistoReceiver::receive] Last index is a pix scan histogram index not a histogram index." << std::endl;
      return;
    }

    m_histoArray->addHisto(index,h.histogram.release());
  }

  void PixScanHistoReceiver::receive( std::vector<OHRootHistogram*> & h_array)
  {
    for (std::vector<OHRootHistogram*>::iterator histo_iter=h_array.begin();
	 histo_iter != h_array.end();
	 histo_iter++) {
      receive(*(*histo_iter));
    }
  }

}
