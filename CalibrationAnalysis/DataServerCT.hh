#ifndef _CAN_DataServerCT_hh_
#define _CAN_DataServerCT_hh_

#include "IDataServer.hh"
#include "exception.hh"
//#include <mrs/message.h>

//CLA:
#include <ConfigWrapper/ScanConfig.h>
#include <DataContainer/HistoRef.h>
#include "ConfigWrapper/DbManager.h"
#include "MetaDataService.hh"
#include "ScanInfo_t.hh"
#include <map>
#include <deque>
#include <vector>
#include "Lock.hh"
#include "Flag.hh"
#include "ScanResultListener.hh"

//
#include <TH2F.h>

namespace CAN {

  class MasterDataServerCT;

  class StackElement_t {
  public:
    StackElement_t(const PixA::DbHandle &folder_handle, unsigned int folder_hierarchy_index, const PixA::ConfigHandle &scan_info) 
      : m_folderHandle(folder_handle), m_hierarchyIndex(folder_hierarchy_index), m_scanInfo(scan_info) {}

    StackElement_t(const PixA::DbHandle &folder_handle, unsigned int folder_hierarchy_index) 
      : m_folderHandle(folder_handle), m_hierarchyIndex(folder_hierarchy_index) {}
    
    PixA::DbHandle &folder()             { return m_folderHandle;}
    const PixA::DbHandle &folder() const { return m_folderHandle;}

    unsigned int hierarchyIndex() const  { return m_hierarchyIndex;}

    PixA::ConfigHandle &scanInfo()             { return m_scanInfo;}
    const PixA::ConfigHandle &scanInfo() const { return m_scanInfo;}

  private:
    PixA::DbHandle     m_folderHandle;
    unsigned int m_hierarchyIndex;
    PixA::ConfigHandle m_scanInfo;
  };
  
  /** Server of histograms, configurations and connecitivity  from the Connectivity Test (CT).
   * The server will automatically provide the connectivity and the scan configuration.
   * Histograms and module or boc configurations etc. need to be requested before they
   * can be retrieved from the server.
   */
  class DataServerCT : public IDataServer
  {
    // General DataServer Interface:
  public:
    //DataServerCT(PixA::ConnectivityRef &conn, const std::string &rod_name, MasterDataServer *master); 
    DataServerCT(PixA::ConnectivityRef &conn, const PixA::RodLocationBase &rod_name, MasterDataServerCT *master); 

    DataServerCT() {}; //only to be used by runAnalysis

    ~DataServerCT();

    void requestHistogram(const std::string &,const std::string &); //scan-type and pp0-name

    void requestUIntAnalysisResult(const std::string &tag_name,
    				   Revision_t &revision,
    				   const std::string &conn_name,
    				   const std::string &variable_name);

    void requestBoolAnalysisResult(const std::string &tag_name,
    				   Revision_t &revision,
    				   const std::string &conn_name,
    				   const std::string &variable_name);

    void requestAvAnalysisResult(const std::string &tag_name,
					   Revision_t &revision,
					   const std::string &conn_name,
					   const std::string &variable_name);

    void requestModuleGroupConfig() {};

    void requestScanConfig() {};

    void requestBocConfig();

    void requestModuleConfig(const std::string &);

    void requestModuleConfigObject(const std::string &,
    				   const std::string &,
    				   const std::string &);

    void requestModuleConfigObject(const std::string &,
    				   const std::vector<std::string> &,
    				   const std::string &);

    void thatIsAll();

    //CLA
    void sendToHistoServer(std::string &name, PixLib::Histo &histo);
    void getFromHistoServer(std::string &name, PixLib::Histo &histo);

    void numberOfHistograms(const std::string&, const std::string&, HistoInfo_t&) const { assert(false); }
    const Histo* getHistogram(const std::string&, const std::string&, const PixA::Index_t&) const { assert(false); }

    /* 
     * Get next histogram which matches current selection, NULL if no more
    */
    PixLib::Histo *getHistogram(const std::string &histogram_type, const std::string &conn_name) const { return NULL; };
//     TH2F* getRootHistogram( const std::string &histogram_type, const std::string &conn_name);  
//     TH1F* getRoot1DHistogram( const std::string &histogram_type, const std::string &conn_name);  
    //    std::list<scanHisto_t> DataServerCT::getHistogramList(const std::string histogram_type); //the list is for one pp0, one entry in the list (scanHisto_t) has all histos from one module      
    std::list<PixA::HistoHandle*> getHistograms(const std::string &scanType,const std::string &pp0name) { return listener->GetHistos(scanType,pp0name); };  //needed type etc given in requestHistogram 
    std::list<std::string> getModuleNames(const std::string &scanType,const std::string &pp0name) { return listener->GetModuleNames(scanType,pp0name); };  

    std::list<std::string> getpp0Names(); 

    unsigned int getUIntAnalysisResult(const std::string &conn_name,
				       const std::string &variable_name) const;


    unsigned int getBoolAnalysisResult(const std::string &conn_name,
				       const std::string &variable_name) const;

    const AverageResult_t &getAvAnalysisResult(const std::string &conn_name,
					       const std::string &variable_name) const;

    const PixLib::Config &getScanConfig() const;

    std::list<PixA::ScanConfigRef*> getScanConfigRefs(const std::string &scanType,const std::string &pp0name) const; //CLA

    const PixLib::Config &getBocConfig() const;
    
    const PixLib::Config &getModuleConfig(const std::string &module_conn_name) const;

    const PixLib::ConfigObj &getModuleConfigObject(const std::string &module_conn_name,
						   const std::vector<std::string> &sub_config_and_group_name,
						   const std::string &config_object_name) const;

    const PixLib::ConfigObj &getModuleConfigObject(const std::string &module_conn_name,
						   const std::string &config_group_name,
						   const std::string &config_object_name) const;
    
    const std::string &rodConnName() const {return m_rodName;}

    bool gotAllRequests() const {return m_gotAllRequests; }

    // Functions from Streamer:
  public:
    //    ScanResultStreamer(const std::string &orig_file_name, const std::string &label_name="");
    void setInputDir(const std::string path) { m_path=path; }; 
    void addFile(const std::string file_name);
    void addPath(const std::string path);
    std::string GetFileName() { return m_files.front(); };

  private:
    PixLib::Histo* Convert2PixLibHisto(const PixA::DbHandle lasthisto, const std::string histogram_type);

    void getFilesInPath(const std::string &path); 

    void GetHistoLists(std::vector<PixA::DbHandle> &pix_scan_histo_list, std::vector<PixA::DbHandle> &histo_list, PixA::DbRef folder, std::vector<StackElement_t> &folder_stack);
    bool GetScanData(PixA::DbHandle &scan_data_handle, PixA::DbRef rod_record);
    // CLA new:
    void GetRecordLists(std::vector<PixA::DbHandle> &mlist, PixA::DbRef folder,const std::string selectClassName);
    void CreateNewHistoList(const std::string histogram_type);
    void GetHistoMap(std::map<std::string,PixLib::Histo> *histoMap);

    HistoInfo_t GetHistoInfo(const std::string &name,const PixA::HistoHandle &histo_handle);
    void AppendFolders( const std::string file_name, std::map<std::string,PixLib::Histo> *histoMap);
    void AppendFilesInPath(const std::string path); 
  public:
    void SelectModule(const std::string module_name);
    void SelectScanLabel(const std::string scan_label);
  private: //variables
    std::vector<std::string> m_paths;
    std::string m_path;
    std::vector<std::string> m_files;    
    std::vector<PixA::DbHandle> m_histoList;
    std::vector<PixA::DbHandle> m_scanHistoList;

  private:
    PixA::ConnectivityRef m_connectivity;
    //std::string           m_rodName;
    const PixA::RodLocationBase m_rodName;
    MasterDataServerCT      *m_master;
    bool                  m_gotAllRequests;
    PixA::ScanResultStreamer * streamer;
    ScanResultListener * listener;
    //    PixLib::Histo* Ref2Histo(PixA::HistoRef historef, const std::string histogram_type,std::vector<histoAxis_t> axis);

    /* //uncoment-start
  public:
    // Process all labels of all files.
    //
    //    void read(const std::string &mname="");

    // Process one label.
    //
    void readLabel(const std::string &label_name, const std::string &mname="");

    // Only read the labels but do not process the lables.
    //
    //    void readLabels();

    // Get a connectivity for the given scanInfo.
    //
    static PixA::ConnectivityRef ScanInfo2Connectivity(const PixA::ConfigHandle &scan_info);

    void setLabel(const std::string &label_name);

    void readCurrentLabel(const std::string &mname="");

    bool createGlobaleConfigList(PixA::DbHandle &root_record, const std::string &rod_name, const std::string &module_name);

    bool createConfigListFromConnectivity(const std::string &rod_name);

    bool processModulesFromConnectivity(const std::set<std::string> &module_names,  
					const PixA::DbHandle &histo_folder_handle,
					const std::vector<PixA::DbHandle> &pix_scan_histo_list,
					const PixA::ConfigHandle &pix_scan_config,
					const PixA::ConfigHandle &boc_config,
					const std::string &rod_name);

    bool processModulesFromConfigList(const std::set<std::string> &module_names,  
				      const PixA::DbHandle &histo_folder_handle,
				      const std::vector<PixA::DbHandle> &pix_scan_histo_list,
				      const PixA::ConfigHandle &pix_scan_config,
				      const PixA::ConfigHandle &boc_config);

    PixA::ConfigHandle modConf(const std::string &module_prod_name);

    std::string m_fileName;
    std::vector<PixA::DbHandle> m_rootHandles;
    PixA::DbHandle m_labelHandle;
    PixA::DbHandle m_groupHandle;

    PixA::ConnectivityRef m_conn;

    std::map<std::string, PixA::ConfigHandle> m_configList;
    std::map<std::string, PixA::ConfigHandle> m_globalConfigList;

    static std::set<std::string> s_histoTypeList;
    static void initHistoTypes();    

    // Private Functions: (from Listener)
  private:
    void newLabel(const std::string &name, const PixA::ConfigHandle &label_config);
    void newPixModuleGroup(const std::string &name);
    void newPp0(const std::string &name);
    void finishPp0();
    void newFolder(const std::vector<std::string> &folder_hierarchy);
    ////
    void newPixScanHisto(const std::string &histo_name,
			 const PixA::ScanConfigRef &scan_config,
			 const PixA::HistoHandle &histo_handle);
    void newHisto(const std::string &name, const PixA::HistoHandle &histo_handle, 
		  const std::string &rod_name, const PixA::ConfigHandle &scan_config);

  private:
    int HistoName2Type(const std::string &histo_name);
    void PrintSpeedAndModule(const PixA::ScanConfigRef &scan_config); 
    typedef std::multimap<int, std::pair< std::string, std::map<std::string, PixA::ConfigHandle::const_iterator> > ModuleList_t;
    ModuleList_t GetModuleList(const std::map<std::string, PixA::ConfigHandle> *config_list);
    std::set<std::string> GetModuleNames(std::vector<PixA::DbHandle> pix_scan_histo_list);
    void GetScanDataAndBocConfig(PixA::DbHandle &scan_data_handle, PixA::ConfigHandle &boc_config, PixA::DbRef rod_record);
    */
  };
}
#endif
