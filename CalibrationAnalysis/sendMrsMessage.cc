#include <ipc/partition.h>
#include <ipc/core.h>

//#include <mrs/message.h>
#include "exception.hh"

int main(int argc, char **argv)
{
  // GET COMMAND LINE


//   std::string mrsPartitionName;
  std::string message_id;
  std::string severity;
  std::string message;
  std::string method_name;

  bool error=false;
  for (int arg_i=1; arg_i<argc; arg_i++) {
//     if (strcmp(argv[arg_i],"-m")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
//       mrsPartitionName = argv[++arg_i];
//     }
//    else 
    if (strcmp(argv[arg_i],"-i")==0 && arg_i+1<argc) {
      message_id = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-t")==0 && arg_i+1<argc) {
      message = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-s")==0 && arg_i+1<argc) {
      severity = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-f")==0 && arg_i+1<argc) {
      method_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-h")==0) {
      error=true;
      break;
    }
    else {
      error=true;
    }
  }
  //  std::map<std::string, CAN::MRSSeverity> severity_map;
  std::map<std::string, PixLib::PixMessages::MESSAGE_TYPE> severity_map;
  severity_map["INFORMATION"]=PixLib::PixMessages::INFORMATION;
  severity_map["WARNING"]=PixLib::PixMessages::WARNING;
  severity_map["ERROR"]=PixLib::PixMessages::ERROR;
  severity_map["FATAL"]=PixLib::PixMessages::FATAL;
  severity_map["DIAGNOSTIC"]=PixLib::PixMessages::DIAGNOSTIC;

  std::map<std::string, PixLib::PixMessages::MESSAGE_TYPE>::const_iterator severity_map_iter = severity_map.find(severity);

  //  if (message.empty() || message_id.empty() || mrsPartitionName.empty() || severity_map_iter == severity_map.end() || error) {
  if (message.empty() || message_id.empty() || severity_map_iter == severity_map.end() || error) {
    std::cout << "USAGE: mrsMessage [-h] -m mrs-partition -i msg-id -s severity -f function-name -t text" << std::endl
	      << std::endl
	      << "\t-m mrs-partition\tSet the mrs partition name. If -m is omitted the second name will be used." << std::endl
	      << "\t-s severity\t\tSet the message severity: INFORMATION, WARNING, ERROR, FATAL." << std::endl
	      << "\t-t text\t\t\tSet the message text.." << std::endl
	      << "\t-i message-id\t\tSet the message id." << std::endl
	      << "\t-f method-name\t\tSet the function or module name which triggered this message." << std::endl
	      << std::endl
	      << "\t-h\t\t\tShow this help."
	      << std::endl
	      << "The ipc partition has to be specified. The other arguments are optional. Other arguments will be ignored."
	      << std::endl;
    return EXIT_FAILURE;
  }

//   if (mrsPartitionName.empty()) {
//     std::cerr << "ERROR [" << argv[0] << ":main] No partition name given for  MRS."  << std::endl;
//     return EXIT_FAILURE;
//   }
  
  std::cout << "INFO [" << argv[0] << ":main] Send messages to ERS"  << std::endl;

  // START IPC
  // Start IPCCore
  IPCCore::init(argc, argv);

//   IPCPartition mrsPartition(mrsPartitionName.c_str());
//   if (!mrsPartition.isValid()) {
//     std::cerr << "ERROR [" << argv[0] << ":main] Not a valid partition \"" << mrsPartitionName << "\" for MRS." << std::endl;
//     return EXIT_FAILURE;
//   }

//   MRSStream out(mrsPartition);
//   CAN::sendMessage(out, message_id, severity_map_iter->second, method_name, message);
  PixLib::PixMessages msg;
  msg.publishMessage(severity_map_iter->second, method_name, message);

  return 0;
}
