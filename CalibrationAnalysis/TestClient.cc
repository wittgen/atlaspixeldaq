#include <ipc/partition.h>
#include <ipc/core.h>
#include "TestServer.hh"
#include "Common.hh"

#include <vector>
#include <iostream>
#include <iomanip>
#include <stdexcept>

#include <time.h>

int main(int argc, char **argv) {

  // GET COMMAND LINE

  // Check arguments
  if(argc<2) {
    std::cout << "USAGE: test_SerialNumberServer \"partition name\"" << std::endl;
    return 1;
  }

  std::string ipcPartitionName = argv[1];

  // START IPC

  // Start IPCCore
  IPCCore::init(argc, argv);

  // Create IPCPartition
  std::cout << "Getting partition " << ipcPartitionName << std::endl;
  IPCPartition *ipcPartition = new IPCPartition(ipcPartitionName.c_str());

  try {

    std::string name(ipcPartition->name()+"_TestServer");

    unsigned int n_counter = 500000;

    std::string           ana_name = "TestAnalysis";
    CAN::ESerialNumberType ana_src_type = CAN::kScanNumber;
    CAN::SerialNumber_t   ana_src_number= 1;
    std::string           ana_tag = "base";
    CAN::Revision_t       ana_revision = time(NULL);

    CAN::TestServer_var test_server = ipcPartition->lookup<CAN::TestServer>(name);
    for(unsigned int pass_i=0; pass_i<2; pass_i++) {

    {
      unsigned int start_time = time(NULL);
      test_server->resetCounter();
      for (unsigned int counter_i=0; counter_i < n_counter; counter_i++) {
	CORBA::String_var a_name = CORBA::string_dup(ana_name.c_str());
	CORBA::String_var a_tag = CORBA::string_dup(ana_tag.c_str());
	
	test_server->test1(a_name,ana_src_type, ana_src_number, a_tag, ana_revision);
      }

      unsigned int a_counter = test_server->getCounter();
      unsigned int end_time = time(NULL);
      double time_diff = end_time - start_time;
      if (a_counter>0) {
	time_diff/=a_counter;
      }
      std::cout << "INFO [test1] time per call = " << time_diff << " counter = " << a_counter <<std::endl;
    }

    {
      unsigned int start_time = time(NULL);
      test_server->resetCounter();
      for (unsigned int counter_i=0; counter_i < n_counter; counter_i++) {
	CORBA::String_var a_name = CORBA::string_dup(ana_name.c_str());
	CORBA::String_var a_tag = CORBA::string_dup(ana_tag.c_str());
	CAN::AnalysisInfoData_t ana_data;
	ana_data.m_analysisName = a_name;
	ana_data.m_srcSerialNumberType = ana_src_type;
	ana_data.m_srcSerialNumber = ana_src_number;
	ana_data.m_tag = a_tag;
	ana_data.m_revision = ana_revision;

	test_server->test2(ana_data);
      }

      unsigned int a_counter = test_server->getCounter();
      unsigned int end_time = time(NULL);
      double time_diff = end_time - start_time;
      if (a_counter>0) {
	time_diff/=a_counter;
      }
      std::cout << "INFO [test2] time per call = " << time_diff << " counter = " << a_counter <<std::endl;
    }
    }
      
  }
  catch (std::runtime_error &err) {
    std::cerr << err.what() << std::endl;
  }
}
