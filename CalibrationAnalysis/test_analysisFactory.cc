#include "AnalysisFactory.hh"
#include "AnalysisInfo_t.hh"
#include "IObjectConfigDb.hh"
#include "DummyConfigDb.hh"
#include "IAnalysisObject.hh"
#include "IClassification.hh"
#include "DummyDataServer.hh"
#include "ExtendedAnalysisResultList_t.hh"


#include "exception.hh"


int main(int argc, char **argv)
{
  std::string name;
  std::string tag;
  CAN::Revision_t rev=0;
  CAN::SerialNumber_t serial_number=0;
  unsigned int wait_and_reload=0;

  for (int arg_i=1; arg_i<argc; arg_i++) {
    if ((strcmp(argv[arg_i],"-o")==0 || strcmp(argv[arg_i],"-a")==0) && arg_i+3<argc && argv[arg_i+1][0]!='-' && argv[arg_i+2][0]!='-') {
      name = argv[++arg_i];
      tag = argv[++arg_i];
      rev = atoi(argv[++arg_i]);
    }
    else if (strcmp(argv[arg_i],"-s")==0 && arg_i+1<argc) {
      serial_number = atoi(argv[++arg_i]);
    }
    else if (strcmp(argv[arg_i],"-w")==0 && arg_i+1<argc) {
      wait_and_reload = atoi(argv[++arg_i]);
    }
    else {
      std::cerr << "ERROR ["<< argv[0] << ":main] unhandled argument " << arg_i << " : " << argv[arg_i]<< "." << std::endl;
    }
  }
  
  if (name.empty() || tag.empty()) {
    std::cerr << "usage: " << argv[0] << "-o object_name tag rev -s serial-number." << std::endl;
    return -1;
  }
  CAN::ObjectInfo_t obj_info(name,tag,rev);


  std::cout << "INFO [" << argv[0] << ":main] create analysis object : " 
	    << obj_info.name() << "/"
	    << obj_info.tag() << "/"
	    << obj_info.revision() << " with serial number "
	    << serial_number
	    << std::endl;

  try {
    std::auto_ptr<CAN::IObjectConfigDb> db(new CAN::DummyConfigDb);
    {
      // test analysis object factory
      std::auto_ptr<CAN::IAnalysisObject> obj(CAN::AnalysisFactory::get()->create(serial_number,obj_info, db.get()));
      if (!obj.get()) {
        std::cerr << "ERROR [" << argv[0] << ":main] Failed to create object."
	  	  << std::endl;
        return -1;
      }
      else {
        CAN::ExtendedAnalysisResultList_t results;
        {
          // execute a classification algorithm with dummy inputs
          CAN::IClassification *alg=dynamic_cast<CAN::IClassification *>(obj.get());
          if (alg) {
	    try {

	      // @todo hack : will break horribly for a classification which requires a valid data server
	      // the data server has at least to provide a correct connectivity.
	      CAN::DummyMasterDataServer *invalid_master=NULL;
	      CAN::DummyDataServer *invalid_data_server= new CAN::DummyDataServer("", *invalid_master);
	      PixA::ConnectivityRef invalid_conn;
	      CAN::AnalysisDataServer invalid_analysis_data_server(invalid_conn);
	      invalid_analysis_data_server.addDataServer("",invalid_data_server);
              alg->classify(&invalid_analysis_data_server, "ROD_C0_S1",results);
              std::cout << "INFO [" << argv[0] << ":main] Invoked IClassification::classify of " <<  obj_info.name() << "." << std::endl;
            }
	    catch (...) {
	      std::cerr << "ERROR [" << argv[0] << ":main] Caught exception while executing classification." << std::endl;
	      return -1;
            }
          }
        }
      }
      
    }

    // test unloading of plugins
    if (wait_and_reload>0)
    { 
      CAN::AnalysisFactory::unloadAll();
      std::cout << "kill -SIGSTOP " << getpid() << std::endl;
      std::cout << "Change the plugin and recompile." << std::endl;
      sleep(wait_and_reload);
      {
        std::auto_ptr<CAN::IAnalysisObject> obj(CAN::AnalysisFactory::get()->create(serial_number,obj_info, db.get()));
        if (!obj.get()) {
        std::cerr << "ERROR [" << argv[0] << ":main] Failed to create object."
                  << std::endl;
        }
      }
    }
  }
  catch( CAN::MRSException &err) {
    std::cerr << "ERROR [" << argv[0] << ":main] Caught exception : "
	      << err.getDescriptor()
	      << std::endl;
    return -1;
  }
  catch( std::exception &err) {
    std::cerr << "ERROR [" << argv[0] << ":main] Caught exception : "
	      << err.what()
	      << std::endl;
    return -1;
  }
}
