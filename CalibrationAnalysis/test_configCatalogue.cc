#include <iostream>
#include <cstdlib>
#include <CoralBase/Exception.h>

#include "ConfigCatalogue.hh"
#include "ConfigCatalogueListener.hh"

#include <time.h>

  class CommentListener : public CAN::ICommentListener {
  public:
    void newComment(const std::string &type_name, const std::string &tag_name, CAN::Revision_t revision, CAN::time_t date, const std::string &comment) {
      ::time_t std_time = static_cast<time_t>(date);
      std::cout << "INFO [CommentListener::newComment] name = " << type_name << " tag = " << tag_name << " / "<<revision << " : " 
		<< ctime(&std_time)
		<< comment << std::endl;
    }
  };

  class DescriptionListener : public CAN::IDescriptionListener {
  public:
    void newDescription(const std::string &type_name, const CAN::ConfigDescription &desc) {
      std::cout << "INFO [CommentListener::newDescription]"
		<< " name  = " << type_name
		<< " class = " << desc.className()
		<< " cat.  = " << desc.category()
		<< " level = " << desc.levelName()
		<< std::endl;     
    }
  };


int main(int argc, char **argv)
{
  std::string connection_string;
  std::string name;
  std::string class_name;
  std::string category;
  std::string tag_name;
  unsigned int revision=0;
  bool general_comments_only = false;
  std::string comment;
  unsigned int max_level=0;

  enum ECommand {kAddComment, kUpdateCategory, kAddDescription, kShowDescription, kShowClass, kShowComments} command=kShowDescription;
  CAN::ConfigDescription::EConfigLevel level=CAN::ConfigDescription::kNormal;
  bool error=false;

  int arg_i=1;
  for(arg_i=1; arg_i<argc; arg_i++) {
    if (strcmp(argv[arg_i],"-d")==0 && arg_i+1< argc && argv[arg_i+1][0]!='-') {
      connection_string = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-l")==0 && arg_i+1 < argc) {
      command=kShowDescription;
      if (argv[arg_i+1][0]=='-') {
	error=true;
	break;
      }
      name=argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-c")==0 && arg_i+1 < argc) {
      command=kShowClass;
      if (argv[arg_i+1][0]=='-') {
	error=true;
	break;
      }
      class_name=argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-u")==0 && arg_i+2 < argc) {
      command=kUpdateCategory;
      if (argv[arg_i+1][0]=='-') {
	error=true;
	break;
      }
      name=argv[++arg_i];
      if (argv[arg_i+1][0]=='-') {
	error=true;
	break;
      }
      category=argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-t")==0 && arg_i+2 < argc) {
      command=kAddComment;
      if (argv[arg_i+1][0]=='-') {
	error=true;
	break;
      }
      name=argv[++arg_i];
      if (argv[arg_i+1][0]=='-') {
	error=true;
	break;
      }
      if (arg_i+2<argc && argv[arg_i+2][0]!='-') {
	tag_name=argv[++arg_i];
	if (arg_i+2<argc && argv[arg_i+2][0]!='-') {
	  revision=atoi(argv[++arg_i]);
	}
      }
      comment=argv[++arg_i];
    }
    else if ((strcmp(argv[arg_i],"-x")==0 || strcmp(argv[arg_i],"-X")==0) && arg_i+1 < argc) {
      if (strcmp(argv[arg_i],"-X")==0) {
	general_comments_only=true;
      }
      command=kShowComments;
      if (argv[arg_i+1][0]=='-') {
	error=true;
	break;
      }
      name=argv[++arg_i];
      if (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	tag_name=argv[++arg_i];
	if (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	  revision=atoi(argv[++arg_i]);
	}
      }
    }
    else if (strcmp(argv[arg_i],"-s")==0 && arg_i+4 < argc) {
      command=kAddDescription;
      if (argv[arg_i+1][0]=='-') {
	error=true;
	break;
      }
      name=argv[++arg_i];
      if (argv[arg_i+1][0]=='-') {
	error=true;
	break;
      }
      class_name=argv[++arg_i];
      if (argv[arg_i+1][0]=='-') {
	error=true;
	break;
      }
      category=argv[++arg_i];
      if (argv[arg_i+1][0]=='-') {
	error=true;
	break;
      }
      ++arg_i;
      unsigned int level_i=static_cast<CAN::ConfigDescription::EConfigLevel>(0);
      for (; level_i<=CAN::ConfigDescription::kNLevels; level_i++) {
	if (strcmp(CAN::ConfigDescription::levelName(static_cast<CAN::ConfigDescription::EConfigLevel>(level_i)),argv[arg_i])==0) {
	  break;
	}
      }
      if (level_i > CAN::ConfigDescription::kNLevels) {
	error=true;
	break;
      }
      level=static_cast<CAN::ConfigDescription::EConfigLevel>(level_i);
    }
    else if ((strcmp(argv[arg_i],"-L")==0|| strcmp(argv[arg_i],"--max-level")==0) && arg_i+1 < argc) {
      ++arg_i;
      unsigned int level_i=static_cast<CAN::ConfigDescription::EConfigLevel>(0);
      for (; level_i<=CAN::ConfigDescription::kNLevels; level_i++) {
	if (strcmp(CAN::ConfigDescription::levelName(static_cast<CAN::ConfigDescription::EConfigLevel>(level_i)),argv[arg_i])==0) {
	  break;
	}
      }
      if (level_i > CAN::ConfigDescription::kNLevels) {
	error=true;
	break;
      }
      max_level=static_cast<CAN::ConfigDescription::EConfigLevel>(level_i);
    }
  }
  if (error) {
    std::cout << "Error at argument " << arg_i << " " << argv[arg_i] << std::endl;
    std::cout << "USAGE -d connection-string [-s name class-name caregory level=[Normal,Expert,Debug] ] [-l name ] [-t name [tag] [revision] comment]" 
	      << "\t [-x/-X name [tag] [revision]] [-u name category ] [-L/--max-level Normal,Expert,Debug ] [-c class-name]" << std::endl;
    return -1;
  }

  try {
    CAN::ConfigCatalogue catalogue(connection_string,/*verbose*/true,/*update*/true);
    switch (command) {
    case kAddComment:
      catalogue.addComment(name,tag_name, revision, comment);
      break;
    case kUpdateCategory: 
      catalogue.updateCategory(name,category);
      break;
    case kAddDescription:
      catalogue.addConfigDescription(name,class_name, category, level);
      break;
    case kShowDescription: {
      CAN::ConfigDescription desc = catalogue.getDescription(name);
      std::cout << " name  = " << name
		<< " class = " << desc.className()
		<< " cat.  = " << desc.category()
		<< " level = " << desc.levelName()
		<< std::endl;
      break;
    }
    case kShowClass: {
      DescriptionListener listener;
      catalogue.getDescription(class_name, listener, static_cast<CAN::ConfigDescription::EConfigLevel>(max_level));
      break;
    }
    case kShowComments: {
      CommentListener listener;
      catalogue.getComments(name,tag_name,revision, listener, general_comments_only);
      break;
    }
    }
  }
  catch (CAN::MissingConfigDescription &err) {
    std::cout << " Caught MissingConfigDescription : " << err.getDescriptor() << "." << std::endl;
    return -1;
  }
  catch (PixA::ConfigException &err) {
    std::cout << " Caught ConfigException : " << err.getDescriptor() << "." << std::endl;
    return -1;
  }
  catch (coral::Exception &err) {
    std::cout << " Caught CORAL Exception : " << err.what() << "." << std::endl;
    return -1;
  }
  catch (std::exception &err) {
    std::cout << " Caught std exception : " << err.what() << "." << std::endl;
    return -1;
  }
  catch (...) {
    std::cout << " Caught unknown exception." << std::endl;
    return -1;
  }
  return 0;
}
