#ifndef _CAN_RestartHelper_hh_
#define _CAN_RestartHelper_hh_

#include "CanThread.hh"

//#include <mrs/message.h>
#include "sendMessage.hh"
#include <memory>
#include "PixUtilities/PixMessages.h"


namespace CAN {
  class Worker_impl;

  /** Helper thread which will restart the worker.
   */
  class RestartThread : public CanThread
  {
  public:
    RestartThread()//IPCPartition &mrs_partition)
      : m_worker(NULL),
	//m_out(new MRSStream(mrs_partition)),
	m_msg(new PixLib::PixMessages()),
	m_kill(false)
    {}

    RestartThread(Worker_impl &worker)//,
    //		  IPCPartition &mrs_partition)
      : m_worker(&worker),
	//	m_out(new MRSStream(mrs_partition)),
	m_kill(false)
    { start(); }

    void setWorker(Worker_impl &worker) {
      m_worker=&worker;
      if (state()!=omni_thread::STATE_RUNNING) {     
	start();
      }
    }

    void kill() {
      m_kill=true;
      m_restart.setFlag();
    }

    void restart() {
      m_restart.setFlag();
    }

    void done() {
      m_workerIsDone.setFlag();
    }

  protected:

    void wakeupForShutdown() {
      m_restart.setFlag();
    }

    void main();

    void triggerReset() {}


  private:
    Worker_impl              *m_worker;
    //    std::auto_ptr<MRSStream>  m_out;
    std::unique_ptr<PixLib::PixMessages> m_msg;
    Flag m_restart;
    Flag m_workerIsDone ;
    bool m_kill;
  };

}

#endif 
