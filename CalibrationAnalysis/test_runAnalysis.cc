#include <ipc/partition.h>
#include <ipc/core.h>
#include <PixConnectivity/PartitionConnectivity.h>

#include "exception.hh"
#include <ConfigWrapper/pixa_exception.h>

#include <ConfigWrapper/ConnectivityManager.h>
#include <ConfigWrapper/DbManager.h>
#include <MetaDataService.hh>
#include "PixDbGlobalMutex.hh"
#include "Lock.hh"
#include "PixDbObjectConfigDb.hh"
#include "AnalysisFactory.hh"
#include "AnalysisPerX.hh"

#include "DummyDataServer.hh"
#include "HistoReceiver.hh"
#include "RootFileHistoReceiver.hh"
#include "OHAndFileHistoReceiver.hh"
#include "DirectRetrievalDataServer.hh"
#include <ScanInfo_t.hh>

#include <ExtendedAnalysisResultList_t.hh>

#include <ConfigWrapper/ConnectivityManager.h>
#include <ConfigWrapper/ConnectivityUtil.h>
#include <ConfigWrapper/createDbServer.h>


#include <PixConnectivity/PixConnectivity.h>
//#include <omniORB4/internal/giopStream.h>

#include <PixModule/PixModule.h>
#include <PixModuleGroup/PixModuleGroup.h>
#include <PixBoc/PixBoc.h>
#include <PixController/PixController.h>

#include <memory>
#include <vector>
#include <algorithm>
#include <cctype>

#include "daemonise.hh"

const std::string s_onDbTag="OnDB";

class DbServerReadLock {
public:
  DbServerReadLock( PixLib::PixDbServerInterface *db_server ) : m_dbServer(db_server) {
    m_dbServer->setReadEnable(false);
  }
  ~DbServerReadLock() {
    m_dbServer->setReadEnable(true);
  }
private:
  PixLib::PixDbServerInterface *m_dbServer;
};

enum ECacheWhat {kCacheBOCCfg=1, kCacheModuleCfg=2, kCacheDisable=4 };
bool cacheConnectivity(PixLib::PixDbServerInterface *db_server,
		       CAN::SerialNumber_t scan_serial_number,
		       const CAN::InfoPtr_t<PixLib::ScanInfo_t> &scan_info,
		       unsigned short cache_mask,
		       std::vector<std::string> &rod_name)
{
  if (db_server && scan_info->connTag()==scan_info->aliasTag() && scan_info->connTag()==scan_info->dataTag()) {

    std::string id_tag = scan_info->idTag();
    if (id_tag.empty()) {
      id_tag = "CT";
    }
    std::string domain_name = "Connectivity-"+id_tag;
    std::string config_domain_name = "Configuration-"+id_tag;

    // check whether the db server has the connectivity tag
    std::vector<std::string> tags;
    tags.clear();

    if (!db_server->ipc_getReadEnable()) {
      db_server->setReadEnable(true);
    }

    db_server->listTagsRem(domain_name, tags);
    std::cout << "INFO [cacheConnectivity] tags in " << domain_name << " : ";
    for (std::vector<std::string>::const_iterator tag_iter = tags.begin();
	 tag_iter != tags.end();
	 ++tag_iter) {
      std::cout << *tag_iter << " ";
    }
    std::cout << std::endl;

    std::vector<std::string>::const_iterator tag_iter = find(tags.begin(),tags.end(),scan_info->connTag());
    {
      std::auto_ptr<PixLib::PixConnectivity> pix_conn;
    if (tag_iter == tags.end()) {
      // no, the db server does not have the conn tag.
      // create a pix connectivity for the given tag and upload it to the db server
      pix_conn = std::auto_ptr<PixLib::PixConnectivity>( new PixLib::PixConnectivity( scan_info->connTag(),
										      scan_info->aliasTag(),
										      scan_info->dataTag(),
										      scan_info->cfgTag(),
										      scan_info->idTag(),
										      scan_info->modCfgTag()));
      pix_conn->loadConn();

      DbServerReadLock lock(db_server);
      pix_conn->dumpInDb( db_server );

    }

    if (cache_mask & (kCacheDisable)) {
      std::string config_domain_name = "Configuration-"+id_tag;
      std::vector<unsigned int> revisions;

      std::stringstream obj_name;
      obj_name << "Disable-S" << std::setfill('0') << std::setw(9) << scan_serial_number;
      db_server->listRevisionsRem(config_domain_name, scan_info->cfgTag(),obj_name.str(), revisions);
      if (revisions.empty()) {

	std::shared_ptr<const PixLib::PixDisable> pix_disable;
	{
	CAN::Lock globalLock(CAN::PixDbGlobalMutex::mutex());
	PixA::ConnObjConfigDbTagRef config_db( PixA::ConnObjConfigDb::sharedInstance().getTag(scan_info->idTag(),scan_info->cfgTag(),0) );
	//PixA::ConnObjConfigDbTagRef config_db( conn.configDb() );
	pix_disable = std::shared_ptr<const PixLib::PixDisable>( config_db.disablePtr(obj_name.str())  );
	}

// 	if (!pix_conn.get()) {
// 	  pix_conn = std::auto_ptr<PixLib::PixConnectivity>( new PixLib::PixConnectivity( scan_info->connTag(),
// 											  scan_info->aliasTag(),
// 											  scan_info->dataTag(),
// 											  scan_info->cfgTag(),
// 											  scan_info->idTag(),
// 											  scan_info-> modCfgTag()));
// 	}
// 	std::auto_ptr<PixLib::PixDisable> pix_disable( pix_conn->getDisable(obj_name.str()) );
	if (pix_disable.get()) {
	  std::stringstream pending_tag;
	  pending_tag << "S" << std::setfill('0') << std::setw(9) << scan_serial_number << "_Tmp";
	  unsigned int revision_inout=static_cast<unsigned int>(-1);
	  const_cast<PixLib::PixDisable &>(*pix_disable).config().write(db_server,
								       config_domain_name,
								       scan_info->cfgTag(),
								       obj_name.str(),
									const_cast<PixLib::PixDisable &>(*pix_disable).config().type(),
								       pending_tag.str(),
								       revision_inout);
	}
      }
    }
    }


    if ((cache_mask & (kCacheBOCCfg | kCacheModuleCfg))) {
      // now check whether the db server also has the configuration tag.
      std::string config_domain_name = "Configuration-"+id_tag;
      tags.clear();
      db_server->listTagsRem(domain_name, tags);
      tag_iter = find(tags.begin(),tags.end(),scan_info->cfgTag());

      std::vector<std::string>::const_iterator mod_tag_iter = tag_iter;
      if (scan_info->modCfgTag() != scan_info->cfgTag()) {
	mod_tag_iter = find(tags.begin(),tags.end(),scan_info->modCfgTag());
      }

      if ((tag_iter == tags.end() && (cache_mask & kCacheBOCCfg)) || (mod_tag_iter == tags.end() && (cache_mask & kCacheModuleCfg))) {
	bool load_boc_config=false;
	if (tag_iter == tags.end() && (cache_mask & kCacheBOCCfg)) {
	  load_boc_config = true;
	}

	bool load_mod_config=(cache_mask & kCacheModuleCfg);

	if (mod_tag_iter != tags.end()) {
	  load_mod_config = false;
	}
	//@todo need to check cfg tag
	// since the connectivity cannot be given to the connectivity manager (should that be possible)
	// the connectivity has to be retrieved again.
	CAN::Lock globalLock(CAN::PixDbGlobalMutex::mutex());
	unsigned int cfg_revision = ( scan_info->cfgRev() > 0 ? scan_info->cfgRev() : scan_info->scanStart() );
	PixA::ConnectivityRef conn =PixA::ConnectivityManager::getConnectivity( scan_info->idTag(),
							  scan_info->connTag(),
							  scan_info->aliasTag(),
							  scan_info->dataTag(),
							  scan_info->cfgTag(),
							  scan_info->modCfgTag(),
							  cfg_revision);
	PixA::ConnObjConfigDbTagRef config_db( conn.configDb() );

	DbServerReadLock lock(db_server);

	for (std::vector<std::string>::const_iterator rod_iter = rod_name.begin();
	     rod_iter != rod_name.end();
	     ++rod_iter) {

	  if (load_boc_config) {
	    try {
	      std::vector<unsigned int> revisions;
	      //	      db_server->listRevisionsRem(config_domain_name, scan_info->cfgTag(),*rod_iter, revisions);
	      if (revisions.empty()) {
		std::shared_ptr<const PixLib::PixModuleGroup> pix_module_group( config_db.configuredModuleGroupPtr(*rod_iter )  );

		// 	      PixA::ConfigHandle boc_config_handle( conn.bocConf(pp0_iter) );
		// 	      PixA::ConfigRef boc_config_ref(boc_config_handle.ref());
		// 	      PixA::ConfigRef boc_config( boc_config_ref.createFullCopy() );
		unsigned int rev  = scan_info->cfgRev();
		if (rev==0) {
		  rev=scan_info->scanStart();
		}
		const_cast<PixLib::PixModuleGroup*>(pix_module_group.get())->writeConfigAll(db_server,
											    config_domain_name,
											    scan_info->cfgTag(),
											    s_onDbTag,
											    rev); // should use revision of config not scan
	      }
	    }
	    catch(...) {
	    }
	  }

	  PixA::Pp0List pp0_list=conn.pp0s(*rod_iter);

	  PixA::Pp0List::const_iterator pp0_begin = pp0_list.begin();
	  PixA::Pp0List::const_iterator pp0_end = pp0_list.end();
	  for(PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
	      pp0_iter != pp0_end;
	      ++pp0_iter) {

	    if (load_mod_config) {
	      PixA::ModuleList::const_iterator module_begin = PixA::modulesBegin(pp0_iter);
	      PixA::ModuleList::const_iterator module_end = PixA::modulesEnd(pp0_iter);
	      for(PixA::ModuleList::const_iterator module_iter = module_begin;
		  module_iter != module_end;
		  ++module_iter) {

		std::vector<unsigned int> revisions;
		//		db_server->listRevisionsRem(config_domain_name, scan_info->cfgTag(),PixA::productionName( *module_iter) , revisions);
		if (revisions.empty()) {

		  PixA::ConfigHandle module_config_handle( conn.modConf(module_iter) );
		  PixA::ConfigRef module_config_ref(module_config_handle.ref());
		  PixA::ConfigRef module_config( module_config_ref.createFullCopy() );
		  unsigned int rev = scan_info->cfgRev();
		  if (rev==0) {
		    rev=scan_info->scanStart();
		  }
		  const_cast<PixLib::Config &>(module_config.config()).write(db_server,
									     config_domain_name,
									     scan_info->modCfgTag(),
									     const_cast<PixLib::Config &>(module_config.config()).name(),
									     const_cast<PixLib::Config &>(module_config.config()).type(),
									     s_onDbTag,
									     rev);  // should use revision of config not scan
		}
	      }
	    }
	  }
	}

	// @todo cache module / boc config
      }
    }
  }
  return true;
}

std::string enableString( bool enabled ) {
  if (enabled) return "ENABLED ";
  else return         "disabled";
}

void listConnectivity( PixA::ConnectivityRef &conn, PixA::DisabledListRoot &disabled_list_root, bool show_disabled_objects=false)
{
  //  const PixA::DisabledList disabled_list( PixA::disableList( disabled_list_root) );

  for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
       crate_iter != conn.crates().end();
       ++crate_iter) {

    bool crate_enabled = PixA::enabled( disabled_list_root, crate_iter );

    std::cout << "Crate : " << PixA::connectivityName(crate_iter)  << " " << enableString(crate_enabled) << std::endl;
    if (!crate_enabled && !show_disabled_objects) continue;

    const PixA::DisabledList rod_disabled_list( PixA::rodDisabledList( disabled_list_root, crate_iter) );

    PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
    PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
    for (PixA::RodList::const_iterator rod_iter = rod_begin;
	 rod_iter != rod_end;
	 ++rod_iter) {

      bool rod_enabled = PixA::enabled( rod_disabled_list, rod_iter );

      std::cout << "ROD : " << PixA::connectivityName(rod_iter)  << " " << enableString(rod_enabled) << std::endl;

      if (!rod_enabled && !show_disabled_objects) continue;

      const PixA::DisabledList pp0_disabled_list( PixA::pp0DisabledList( rod_disabled_list, rod_iter) );
      const PixA::DisabledListRoot root_pp0_disabled_list( PixA::createPp0DisabledList( conn, disabled_list_root, PixA::connectivityName(rod_iter)) );

      PixA::Pp0List::const_iterator pp0_begin = PixA::pp0Begin(rod_iter);
      PixA::Pp0List::const_iterator pp0_end = PixA::pp0End(rod_iter);
      for (PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
	   pp0_iter != pp0_end;
	   ++pp0_iter) {

	bool pp0_enabled = PixA::enabled( pp0_disabled_list, pp0_iter );

	std::cout << " PP0 : " << PixA::connectivityName(pp0_iter)  
		  << " " << enableString(pp0_enabled)
		  << " connected to TX " << PixA::txPlugin(pp0_iter);

	unsigned int rx_plugin_dto1 = PixA::rxPlugin(pp0_iter, PixA::DTO1);
	unsigned int rx_plugin_dto2 = PixA::rxPlugin(pp0_iter, PixA::DTO2);
	if (rx_plugin_dto1<4) {
	  std::cout << ", DTO1 RX " << PixA::rxPlugin(pp0_iter, PixA::DTO1);
	}
	if (rx_plugin_dto2<4) {
	  std::cout << ", DTO2 RX " << PixA::rxPlugin(pp0_iter, PixA::DTO2);
	}
	std::cout << std::endl;

	if (!pp0_enabled && !show_disabled_objects) continue;

	const PixA::DisabledList module_disabled_list( PixA::moduleDisabledList( pp0_disabled_list, pp0_iter) );

	PixA::ModuleList::const_iterator module_begin = PixA::modulesBegin(pp0_iter);
	PixA::ModuleList::const_iterator module_end = PixA::modulesEnd(pp0_iter);
	for (PixA::ModuleList::const_iterator module_iter = module_begin;
	     module_iter != module_end;
	     ++module_iter) {

	  bool module_enabled = PixA::enabled( module_disabled_list, module_iter );

	  PixA::ModulePluginConnection tx_connection = PixA::txConnection(module_iter);
	  PixA::ModulePluginConnection rx_connection_dto1 = PixA::rxConnection(module_iter, PixA::DTO1);
	  PixA::ModulePluginConnection rx_connection_dto2 = PixA::rxConnection(module_iter, PixA::DTO2);

	  std::cout << " Module : " << PixA::connectivityName(module_iter)
		    << " " << enableString(module_enabled)
		    << " PP0 slot = " << PixA::pp0Slot(module_iter)
		    << " connected to TX " <<  tx_connection.plugin() << "/" << tx_connection.channel()
		    << std::endl;
	}
      }
    }
  }
}

template <class T>
std::string formatAnalysisResult(const T &result);

template <class T>
inline std::string formatAnalysisResult(const T &result)
{
  std::stringstream buffer;
  buffer << result;
  return buffer.str();
}

template <>
inline std::string formatAnalysisResult<CAN::AverageResult_t>(const CAN::AverageResult_t &result)
{
  std::stringstream buffer;
  buffer << result.value(CAN::AverageResult_t::kLower99) << " < "
	 << result.value(CAN::AverageResult_t::kMean) << "+-"
	 << result.value(CAN::AverageResult_t::kRMS) << " < "
	 << result.value(CAN::AverageResult_t::kUpper99) << " / "
	 << result.n();
  return buffer.str();
}

template <>
inline std::string formatAnalysisResult<PixelMap_t>(const PixelMap_t &result)
{
  std::stringstream buffer;
  buffer << result.size();
  return buffer.str();
}


template <class T>
inline void dumpAnalysisResults(const std::string &rod_name,
				typename std::map< std::string, std::map<std::string, T> >::const_iterator begin,
				typename std::map< std::string, std::map<std::string, T> >::const_iterator end)
{
  unsigned int length=0;
  unsigned int conn_length=0;
  for (typename std::map< std::string, std::map<std::string, T> >::const_iterator variable_iter = begin;
       variable_iter != end;
       ++variable_iter) {
    if (variable_iter->first.size()>length) {
      length = variable_iter->first.size();
    }
    for (typename std::map<std::string, T>::const_iterator conn_iter = variable_iter->second.begin();
	 conn_iter != variable_iter->second.end();
	 ++conn_iter) {
      if (conn_iter->first.size()>conn_length) {
	conn_length = conn_iter->first.size();
      }
    }
  }
  for (typename std::map< std::string, std::map<std::string, T> >::const_iterator variable_iter = begin;
       variable_iter != end;
       ++variable_iter) {
    for (typename std::map<std::string, T>::const_iterator conn_iter = variable_iter->second.begin();
	 conn_iter != variable_iter->second.end();
	 ++conn_iter) {
      std::cout << "INFO [dumpAnalysisResults] " << rod_name << " :: " << std::setw(conn_length) << conn_iter->first << " " << std::setw(length+1) << variable_iter->first << " : "
		<< formatAnalysisResult(conn_iter->second)
		<< std::endl;
    }
  }
}

void dumpAnalysisResults(const std::string &rod_name, CAN::ExtendedAnalysisResultList_t &analysis_results, bool dump_config=false)
{
  std::cout << "INFO [dumpAnalysisResults] " << rod_name << ":" << std::endl;
  dumpAnalysisResults<bool>(rod_name, analysis_results.begin<bool>(), analysis_results.end<bool>());
  dumpAnalysisResults<unsigned int>(rod_name, analysis_results.begin<unsigned int>(), analysis_results.end<unsigned int>());
  dumpAnalysisResults<float>(rod_name, analysis_results.begin<float>(), analysis_results.end<float>());
  dumpAnalysisResults<CAN::AverageResult_t>(rod_name, analysis_results.begin<CAN::AverageResult_t>(), analysis_results.end<CAN::AverageResult_t>());
  dumpAnalysisResults<PixelMap_t>(rod_name, analysis_results.begin<PixelMap_t>(), analysis_results.end<PixelMap_t>());

  if (dump_config) {
    std::cout << "INFO [dumpAnalysisResults] Dump configuraiton of PixModuleGroup of " << rod_name << " : " << std::endl;
    std::shared_ptr<PixLib::PixModuleGroupData_t > pix_module_group = analysis_results.modifiedPixModuleGroupConfig();
    if (pix_module_group.get()) {
      pix_module_group->config().dump(std::cout);
      if (pix_module_group->getPixBoc() && pix_module_group->getPixBoc()->getConfig()) {
	pix_module_group->getPixBoc()->getConfig()->dump(std::cout);
      }
      if (pix_module_group->getPixController() ) {
	pix_module_group->getPixController()->config().dump(std::cout);
      }
    }

    for(std::vector< std::shared_ptr<PixLib::PixModuleData_t >  >::iterator module_iter = analysis_results.beginModifiedModuleConfig();
	module_iter != analysis_results.endModifiedModuleConfig();
	++module_iter) {
      assert( *module_iter );
      (*module_iter)->config().dump(std::cout);
    }
  }

}

int lower_case ( int c )
{
  return tolower ( c );
}

std::map<std::string,CAN::ELogLevel> s_logLevelNames;

CAN::ELogLevel parseLogLevel(const char *log_level_string)
{
  if (strlen(log_level_string)<1) {
      throw std::runtime_error("Empty log level name.");
  }

  if (isdigit(log_level_string[0])) {
    return static_cast<CAN::ELogLevel>(atoi(log_level_string));
  }
  else {
    if (s_logLevelNames.empty()) {
      s_logLevelNames["silent"]=CAN::kSilentLog;
      s_logLevelNames["quiet"]=CAN::kSilentLog;
      s_logLevelNames["fatal"]=CAN::kFatalLog;
      s_logLevelNames["error"]=CAN::kErrorLog;
      s_logLevelNames["warn"]=CAN::kWarnLog;
      s_logLevelNames["warning"]=CAN::kWarnLog;
      s_logLevelNames["slowinfo"]=CAN::kSlowInfoLog;
      s_logLevelNames["slowinformation"]=CAN::kSlowInfoLog;
      s_logLevelNames["info"]=CAN::kInfoLog;
      s_logLevelNames["information"]=CAN::kInfoLog;
      s_logLevelNames["debug"]=CAN::kDiagnosticsLog;
      s_logLevelNames["debug1"]=CAN::kDiagnostics1Log;
      s_logLevelNames["debug2"]=CAN::kDiagnostics2Log;
      s_logLevelNames["debug3"]=CAN::kDiagnostics3Log;
      s_logLevelNames["debug4"]=CAN::kDiagnostics4Log;
      s_logLevelNames["debug5"]=CAN::kDiagnostics5Log;
      s_logLevelNames["debug6"]=CAN::kAllDiagnosticsLog;
      s_logLevelNames["diagnostics"]=CAN::kDiagnosticsLog;
      s_logLevelNames["diagnostics1"]=CAN::kDiagnostics1Log;
      s_logLevelNames["diagnostics2"]=CAN::kDiagnostics2Log;
      s_logLevelNames["diagnostics3"]=CAN::kDiagnostics3Log;
      s_logLevelNames["diagnostics4"]=CAN::kDiagnostics4Log;
      s_logLevelNames["diagnostics5"]=CAN::kDiagnostics5Log;
      s_logLevelNames["diagnostics6"]=CAN::kAllDiagnosticsLog;
    }
    
    std::string level_name(log_level_string);
    transform ( level_name.begin(), level_name.end(), level_name.begin(), lower_case );
    
    std::map<std::string,CAN::ELogLevel>::const_iterator log_iter = s_logLevelNames.find(level_name);
    if (log_iter ==s_logLevelNames.end()) {
      std::stringstream message;
      message << "Not a known log level name : " << level_name << ".";
      throw std::runtime_error(message.str());
    }
    return log_iter->second;
  }

}

int main(int argc, char **argv)
{
  // GET COMMAND LINE


  std::string partition_name;
  std::string oh_server_name;
  std::string db_server_partition_name;
  std::string db_server_name;

  std::string name[3];
  CAN::Revision_t rev[3];
  std::string tag[3];
  CAN::ESerialNumberType serial_number_type=CAN::kScanNumber;

  CAN::SerialNumber_t analysis_serial_number(0);

  CAN::SerialNumber_t serial_number(0);
  CAN::SerialNumber_t scan_serial_number(0);
  std::map<std::string, std::pair<CAN::ESerialNumberType, CAN::SerialNumber_t> > named_sources;

  std::vector<std::string> rod_list;

  unsigned int cache_mask=0;

  bool use_db_server_cache=false;
  bool list_conn_only=false;
  unsigned int verbose=1;
  CAN::ELogLevel log_level=CAN::kInfoLog;

  bool error=false;
  for (int arg_i=1; arg_i<argc; arg_i++) {
    if (   (strcmp(argv[arg_i],"-p")==0  || strcmp(argv[arg_i],"--partition")==0  || strcmp(argv[arg_i],"--oh-partition")==0)
	&& arg_i+1<argc && argv[arg_i+1][0]!='-') {
      partition_name = argv[++arg_i];
    }
    else if ( (strcmp(argv[arg_i],"--oh-server-name")==0 || strcmp(argv[arg_i],"--oh-name")==0 || strcmp(argv[arg_i],"-o")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      oh_server_name = argv[++arg_i];
    }
    else if ( (strcmp(argv[arg_i],"-D")==0
	       || strcmp(argv[arg_i],"--db-server-name")==0)
	      && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      db_server_name = argv[++arg_i];
      use_db_server_cache=true;
    }
    else if (strcmp(argv[arg_i],"--cache")==0) {
      use_db_server_cache=true;
    }
    else if (strcmp(argv[arg_i],"--cache-modules")==0) {
      cache_mask |= kCacheModuleCfg;
    }
    else if (strcmp(argv[arg_i],"--cache-rod")==0) {
      cache_mask |= kCacheBOCCfg;
    }
    else if ( strcmp(argv[arg_i],"--db-server-partition")==0
	      && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      db_server_partition_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-h")==0) {
      error=true;
      break;
    }
    else if (strcmp(argv[arg_i],"-a")==0 && arg_i+3<argc && argv[arg_i+1][0]!='-' && argv[arg_i+2][0]!='-') {
      name[0] = argv[++arg_i];
      tag[0] = argv[++arg_i];
      rev[0] = atoi(argv[++arg_i]);
    }
    else if (strcmp(argv[arg_i],"-c")==0 && arg_i+3<argc && argv[arg_i+1][0]!='-' && argv[arg_i+2][0]!='-') {
      name[1] = argv[++arg_i];
      tag[1] = argv[++arg_i];
      rev[1] = atoi(argv[++arg_i]);
    }
    else if (strcmp(argv[arg_i],"-x")==0 && arg_i+3<argc && argv[arg_i+1][0]!='-' && argv[arg_i+2][0]!='-') {
      name[2] = argv[++arg_i];
      tag[2] = argv[++arg_i];
      rev[2] = atoi(argv[++arg_i]);
    }
    else if ((strcmp(argv[arg_i],"-l")==0 || strcmp(argv[arg_i],"-L")==0 || strcmp(argv[arg_i],"--list-conn")==0) && (arg_i+1>=argc || argv[arg_i+1][0]=='-')) {
      list_conn_only=true;
    }
    else if ((strcmp(argv[arg_i],"-l")==0  || strcmp(argv[arg_i],"--log-level")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      ++arg_i;
      log_level = parseLogLevel(argv[arg_i]);
      std::cout << "INFO [main:"<<argv[0] <<"] new log level =" << log_level << std::endl;
    }
    else if (strcmp(argv[arg_i],"-s")==0 && arg_i+1<argc) {
      ++arg_i;
      if (   isdigit(argv[arg_i][0] ) 
	  || (strlen(argv[arg_i])>1 && isdigit((argv[arg_i][1])) && (argv[arg_i][0]=='S'||argv[arg_i][0]=='A')) ) {
	const char *number_ptr = argv[arg_i];
	if (*number_ptr=='S') {
	  serial_number_type=CAN::kScanNumber;
	  number_ptr++;
	}
	else if (*number_ptr=='A') {
	  serial_number_type=CAN::kAnalysisNumber;
	  number_ptr++;
	}
	serial_number = atoi(number_ptr);
      }
      else {
	const char *number_ptr = argv[arg_i];
	while (*number_ptr && *number_ptr!=':') number_ptr++;
	if (!*number_ptr) {
	  std::cerr << "ERROR ["<< argv[0] << ":main] expected e.g. number like 1001, S01001, A01001, or named source e.g. hvon:S01001 after -s but got : " << argv[arg_i]
		    << "." << std::endl;
	  error = true;
	}
	else {
	  const char *name_end = number_ptr;
	  std::string name(argv[arg_i],0,name_end-argv[arg_i]);
	  number_ptr++;
	  CAN::ESerialNumberType a_type = CAN::kScanNumber;
	  if (*number_ptr=='S') {
	    a_type=CAN::kScanNumber;
	    number_ptr++;
	  }
	  else if (*number_ptr=='A') {
	    a_type=CAN::kAnalysisNumber;
	    number_ptr++;
	  }
	  const char *number_ptr_start = number_ptr;
	  while (isdigit(*number_ptr)) number_ptr++;
	  if (*number_ptr || number_ptr_start == number_ptr) {
	    std::cerr << "ERROR ["<< argv[0] << ":main] expected e.g. number like 1001, S01001, A01001,  after the name" 
		      << name << " but got : " << name_end+1
		      << "." << std::endl;
	    error = true;
	  }
	  else {
	    CAN::SerialNumber_t a_serial_number = atoi(number_ptr_start);
	    if (a_serial_number==0) {
	      std::cerr << "ERROR ["<< argv[0] << ":main] Serial number is zero : " << argv[arg_i]
			<< "." << std::endl;
	      error = true;
	    }
	    else {
	      if (serial_number == 0) {
		serial_number = a_serial_number;
	      }
	      //CLA
	      std::pair<CAN::ESerialNumberType, CAN::SerialNumber_t> pairToInsert = std::make_pair(a_type, a_serial_number);
	      for(std::map<std::string, std::pair<CAN::ESerialNumberType, CAN::SerialNumber_t> >::const_iterator source_iter = named_sources.begin();
		  source_iter != named_sources.end(); source_iter++) {
		if(pairToInsert==source_iter->second) {
		  std::cerr << "ERROR   A scanID con only be added once!  "  << std::endl;
		  error = true;
		}
	      }
	      if(!error) //CLA
		named_sources.insert(std::make_pair(name,pairToInsert));
	    }
	  }
	}
      }
    }
    else if (strcmp(argv[arg_i],"-r")==0 && arg_i+1<argc) {
      rod_list.push_back(argv[++arg_i]);
    }
    else if ((strcmp(argv[arg_i],"-v")==0 || strcmp(argv[arg_i],"--verbose")==0)) {
      log_level=static_cast<CAN::ELogLevel>(static_cast<unsigned short>(log_level)+1);
    }
    else if ((strcmp(argv[arg_i],"-q")==0 || strcmp(argv[arg_i],"--quiet")==0)) {
      log_level=CAN::kFatalLog;
    }
    else {
      std::cerr << "ERROR ["<< argv[0] << ":main] unhandled argument " << arg_i << " : " << argv[arg_i]<< "." << std::endl;
      error = true;
    }
  }

  if(serial_number==0) {
    std::cout << "WARNING  The scanID was not set correctly, check number of input parameters." << std::endl;
    error=true;
  }

  std::vector<CAN::ObjectInfo_t> obj_list;
  for (unsigned int obj_i=0; obj_i<3; obj_i++) {
    if (obj_i==0 & (name[obj_i].empty() || tag[obj_i].empty())) {
      error=true;
      break;
    }
    else {
      obj_list.push_back(CAN::ObjectInfo_t(name[obj_i],tag[obj_i],rev[obj_i]));
    }
  }

  if (error) {
    std::cout << "USAGE: test_runAnalysis [-h] -p/--partition] \"partition name\" [-o/--oh-name] \"oh server name\" \\" << std::endl
	      << "\t[-D/--db-server-name DbServer-name] [--cache] [-q] [-L] [-l log-level] \\" << std::endl
	      << "\t-a analysis tag rev -c classification tag rev -x post-processing tag rev -s serial-number \\" << std::endl
              << "\t-r ROD-name -r ..." << std::endl
	      << std::endl
	      << "\t-p partition\t\t\t\tSet name of the ipc partition." << std::endl
	      << std::endl
	      << "\t-o/--oh-name oh server name\t\tSet name of the oh server (mandatory)." << std::endl
	      << "\t--oh-partition oh partition name\tName of oh partition if different from ipc partition (optional)." << std::endl
	      << std::endl
	      << "\t-D/--db-server-name\t\t\tthe name of the DB server." << std::endl
	      << "\t--db-server-partition\t\t\tthe partition for the db-server" << std::endl
	      << "\t--cache\t\t\t\t\tcache the connectivity on the db-server" << std::endl
	      << std::endl
	      << "\t-q/--quiet\t\t\t\tbe less verbose." << std::endl
	      << "\t-l/--log-levl level\t\tChange log level : quiet, fatal, error, warn, info,debug, debug1,...debug6." << std::endl
	      << "\t-L/--list-conn\t\t\t\tlist the crates, rods, pp0s and modules in the connectivity." << std::endl
	      << "\t-h\t\t\t\t\tShow this help." << std::endl
	      << std::endl
	      << "The ipc partition has to be specified. The other arguments are optional."
	      << std::endl;
    return EXIT_FAILURE;
  }


  try {

    // START IPC
    // Start IPCCore
    IPCCore::init(argc, argv);


    std::auto_ptr<IPCPartition> oh_partition;
    std::auto_ptr<IPCPartition> separate_db_server_partition;
    std::shared_ptr<PixLib::PixDbServerInterface> db_server;

    std::auto_ptr<CAN::IObjectConfigDb> object_config_db(new CAN::PixDbObjectConfigDb);

    IPCPartition *db_server_partition = NULL;
    if (!partition_name.empty()) {
      oh_partition = std::auto_ptr<IPCPartition>(new IPCPartition(partition_name.c_str()));
    }
    if (!db_server_partition_name.empty() && partition_name != db_server_partition_name) {
      separate_db_server_partition = std::auto_ptr<IPCPartition>(new IPCPartition(db_server_partition_name.c_str()));
      db_server_partition = separate_db_server_partition.get();
    }
    else {
      db_server_partition = oh_partition.get();
    }

    if (db_server_partition) {
      if (!db_server_name.empty()) {
	std::cout << "INFO ["<<argv[0]<<":main] Search for the Pixel db server." << std::endl;
	std::map< std::string, ipc::PixDbServerInterface_var > objects;
	db_server_partition->getObjects<ipc::PixDbServerInterface>( objects );
	if (objects.begin() != objects.end()) {
	  db_server_name = objects.begin()->first;
	  std::cout << "INFO [main" << argv[0] << "] found a db server named = "  << db_server_name << std::endl;
	}
	if (!db_server_name.empty()) {
	  db_server=std::shared_ptr<PixLib::PixDbServerInterface>( PixA::createDbServer(*db_server_partition,
											  db_server_name) );
	  if (db_server.get()) {
	    PixA::ConnectivityManager::useDbServer( db_server );
	    std::cout << "INFO [main" << argv[0] << "] will use db server from partition " << db_server_partition->name() << " named " << db_server_name << std::endl;
	  }
	}
      }
    }
    if (use_db_server_cache && !db_server.get()) {
      std::cout << "INFO [main" << argv[0] << "] No db server to cache the connectivity." << std::endl;
    }

    std::auto_ptr<CAN::IHistoReceiver> histo_receiver;

    std::auto_ptr<CAN::IHistoReceiver> file_receiver( new CAN::RootFileHistoReceiver);
    if (oh_partition.get() && !oh_server_name.empty()) {
      std::auto_ptr<CAN::IHistoReceiver> oh_receiver(  new CAN::HistoReceiver(*oh_partition.get(), oh_server_name) );
      histo_receiver = std::auto_ptr<CAN::IHistoReceiver> ( new CAN::OHAndFileHistoReceiver(oh_receiver.release(), file_receiver.release() ) );
    }
    else {
      histo_receiver = file_receiver;
    }


    // create the analysis info object to just to make it more easy to reuse code
    CAN::InfoPtr_t<CAN::AnalysisInfo_t> analysis_info(new CAN::AnalysisInfo_t(obj_list[0],
									      obj_list[1],
									      obj_list[2],
									      (serial_number_type==CAN::kScanNumber ? serial_number : scan_serial_number),
									      (serial_number_type==CAN::kAnalysisNumber ? serial_number : 0),
									      CAN::kDebug,
									      "",
									      time(0)));

    for(std::map<std::string, std::pair<CAN::ESerialNumberType, CAN::SerialNumber_t> >::const_iterator source_iter = named_sources.begin();
	source_iter != named_sources.end();
	source_iter++) {
      analysis_info->addSource(source_iter->first, source_iter->second.first, source_iter->second.second);
      std::cout << "adding source " << source_iter->first << " with serial number " << source_iter->second.second << std::endl;
    }

    if (!analysis_info->check()) {
      analysis_info->Print();

      std::cerr << "ERROR [" << argv[0] << ":main] The analyis meta data failed the check. Are there named sources"
	" and is the master serial number part of the source list?"  << std::endl;
      return EXIT_FAILURE;
    }

    CAN::InfoPtr_t<PixLib::ScanInfo_t> scan_info;
    scan_info = CAN::InfoPtr_t<PixLib::ScanInfo_t>( CAN::MetaDataService::instance()->getScanInfo(analysis_info->scanSerialNumber()) );
    scan_info->Print();
    if (scan_info->pixDisableString().empty()) {
      cache_mask |= kCacheDisable;
    }

    // Also need to load the disable lists before the analyses request histograms

    // for simplicity a vector is constructed which has the same ordering as the list of source serial numbers
    // Each element of this vector which corresponds to a scan (unlike an analysis), gets filled with
    // a disabled root list.

    std::vector< PixA::DisabledListRoot > disabled_lists;
    disabled_lists.reserve( analysis_info->scanSerialNumbers().size() );

    PixA::ConnectivityRef conn;
    {

      // Get the connectivity  from the FIRST scan
      // the scheduler does some verification whether the connectivities of the different sources match.
      {
	std::stringstream message;
	message << "Query connectivity : "
		<< scan_info->connTag();
	if (scan_info->aliasTag() != scan_info->connTag() && scan_info->dataTag() != scan_info->connTag() ) {
	  message << " " << scan_info->aliasTag() << " " << scan_info->dataTag();
	}
	message << " " << scan_info->cfgTag() << " / " << scan_info->cfgRev();
	if (scan_info->cfgTag() != scan_info->modCfgTag()) {
	  message << " " << scan_info->modCfgTag() << " / " << scan_info->modCfgRev();
	}

	std::cerr << "INFO [" << argv[0] << ":main] " << message.str()  << std::endl;
      }

      {
      cacheConnectivity((use_db_server_cache ? db_server.get() : NULL),  analysis_info->scanSerialNumber(), scan_info, cache_mask , rod_list);

      CAN::Lock globalLock(CAN::PixDbGlobalMutex::mutex());

      unsigned int cfg_rev = ( scan_info->cfgRev()>0 ? scan_info->cfgRev() : scan_info->scanStart());
      conn =PixA::ConnectivityManager::getConnectivity( scan_info->idTag(),
							scan_info->connTag(),
							scan_info->aliasTag(),
							scan_info->dataTag(),
							scan_info->cfgTag(),
							scan_info->modCfgTag(),
							cfg_rev);
      }
      if (conn) {

	{

	  for (std::map<std::string, std::pair<CAN::ESerialNumberType, CAN::SerialNumber_t> >::const_iterator source_iter = analysis_info->beginSource();
	       source_iter != analysis_info->endSource();
	       ++source_iter) {

	    disabled_lists.push_back( PixA::DisabledListRoot() );
	    if (source_iter->second.first == CAN::kScanNumber) {
	      if (analysis_info->scanSerialNumber()==source_iter->second.second && !scan_info->pixDisableString().empty()) {
		disabled_lists.back() =  PixA::createEnableMap( scan_info->pixDisableString() );
	      }
	      else {
		std::string enable_string;

		if (analysis_info->scanSerialNumber()!=source_iter->second.second) {
		  CAN::InfoPtr_t<PixLib::ScanInfo_t> a_scan_info;
		  a_scan_info = CAN::InfoPtr_t<PixLib::ScanInfo_t>( CAN::MetaDataService::instance()->getScanInfo(source_iter->second.second  ) );
		  enable_string = a_scan_info->pixDisableString();
		}
		if (!enable_string.empty()) {
		  disabled_lists.back() =  PixA::createEnableMap(enable_string);
		}
		else {
	      try {
		std::stringstream disable_name;
		disable_name << "Disable-S" << std::setw(9) << std::setfill('0') << source_iter->second.second;
		disabled_lists.back() = conn.getDisable(disable_name.str());
	      }
	      catch(...) {
		std::stringstream message;
		message << "Failed to get scan disable list for " << CAN::makeScanSerialNumberString(source_iter->second.second) << ".";
		std::cerr << "ERROR [" << argv[0] << ":main] " << message.str()  << std::endl;
		return -1;
	      }
		}
	      }
	    }
	  }
	}
      }

    }
    if (!conn) {
      std::cerr << "ERROR [" << argv[0] << ":main] Did not get a valid connectivity." << std::endl;
      return -1;
    }

    if (list_conn_only) {
      PixA::DisabledListRoot disabled_list_root;

      if (!disabled_lists.empty()) {
	// @todo combine the disabled lists also for listing
	// for the time  being just pick the first disable
	disabled_list_root = disabled_lists.front();
      }

      listConnectivity(conn, disabled_list_root);
    }

    // create a per-ROD-analysis object
    // EObjectType types[kNObjectTypes]={kAlg,kClassification, kPostProcessing};
    std::auto_ptr<CAN::IAnalysisObject> obj[CAN::kNObjectTypes];
    for (unsigned int obj_i=0; obj_i<CAN::kNObjectTypes; obj_i++) {
      if (!name[obj_i].empty()) {
	const CAN::ObjectInfo_t &object_info( analysis_info->objectInfo(static_cast<CAN::EObjectType>(obj_i)) );
	if (!object_info.name().empty()) {
	  obj[obj_i] =  std::auto_ptr<CAN::IAnalysisObject>(CAN::AnalysisFactory::get()->create(analysis_serial_number,
												object_info,
												object_config_db.get()));
	  if (obj[obj_i].get()) {
	    obj[obj_i]->setLogLevel(log_level);
	  }
	}
      }
    }

    CAN::IAnalysisPerROD    *rod_analysis = dynamic_cast<CAN::IAnalysisPerROD *>(obj[CAN::kAlg].get());

    if (!rod_analysis) {
      CAN::IAnalysisPerPP0    *pp0_analysis = dynamic_cast<CAN::IAnalysisPerPP0 *>(obj[CAN::kAlg].get());
      if (!pp0_analysis) {
	CAN::IAnalysisPerModule *mod_analysis = dynamic_cast<CAN::IAnalysisPerModule *>(obj[CAN::kAlg].get());
	if (!mod_analysis) {

	  std::string message(" The analysis " );
	  message += analysis_info->name(CAN::kAlg) ;
	  message += " is not of type IAnalysisPerROD, IAnalysisPerPP0, IAnalysisPerModule .";
	  std::cerr << "ERROR [" << argv[0] << ":main] " << message  << std::endl;
	  return -1;
	}
	pp0_analysis=new CAN::AnalysisPerPP0(*mod_analysis);
      }
      rod_analysis=new CAN::AnalysisPerROD(*pp0_analysis);
    }

    if (log_level>=CAN::kDiagnostics5Log && rod_analysis) {
      const CAN::ObjectInfo_t &object_info( analysis_info->objectInfo(static_cast<CAN::EObjectType>(CAN::kAlg)) );
      std::cout << "INFO [" << argv[0] << "] created " << object_info.name() << " " <<object_info.tag()
		<< ";" << object_info.revision() << " : " << obj[CAN::kAlg]->config().name()<< std::endl;
    }

    CAN::IClassification    *classification = dynamic_cast<CAN::IClassification *>(obj[CAN::kClassification].get());
    if (!classification && !name[1].empty()) {
      std::string message(" No classification object given or is not of type IClassification (\"" );
      message += analysis_info->name(CAN::kClassification) ;
      message += "\"), No Classification will be executed.";
      std::cerr << "WARNING [" << argv[0] << ":main] " << message  << std::endl;
    }
    else if (log_level>=CAN::kDiagnostics5Log) {
      const CAN::ObjectInfo_t &object_info( analysis_info->objectInfo(static_cast<CAN::EObjectType>(CAN::kClassification)) );
      std::cout << "INFO [" << argv[0] << "] created " << object_info.name() << " " <<object_info.tag()
		<< ";" << object_info.revision() << " : " << obj[CAN::kClassification]->config().name()<< std::endl;
    }

    CAN::IPostProcessing    *post_processing = dynamic_cast<CAN::IPostProcessing *>(obj[CAN::kPostProcessing].get());
    if (!post_processing  && !name[2].empty()) {
      std::string message(" No post-processing object given or is not of type IPostProcessing (\"" );
      message += analysis_info->name(CAN::kPostProcessing) ;
      message += "\"), No PostProcessing will not be executed.";
      std::cerr << "WARNING [" << argv[0] << ":main] " << message  << std::endl;
    }
    else if (log_level>=CAN::kDiagnostics5Log) {
      const CAN::ObjectInfo_t &object_info( analysis_info->objectInfo(static_cast<CAN::EObjectType>(CAN::kPostProcessing)) );
      std::cout << "INFO [" << argv[0] << "] created " << object_info.name() << " " <<object_info.tag()
		<< ";" << object_info.revision() << " : " << obj[CAN::kPostProcessing]->config().name()<< std::endl;
    }

    for (std::vector<std::string>::const_iterator rod_iter = rod_list.begin();
	 rod_iter != rod_list.end();
	 ++rod_iter) {

      PixA::Pp0List pp0_list = conn.pp0s( *rod_iter );
      PixA::RodList rod_list = pp0_list.parent();
      PixA::CrateList crate_list = rod_list.parent();
      PixA::CrateList::const_iterator the_crate_iter = crate_list.find(rod_list.location());
      if (the_crate_iter == crate_list.end()) {
	std::cerr << "ERROR [" << argv[0] << ":main] Crate " << rod_list.location() << " of ROD " << *rod_iter << " does not exist." << std::endl;
	continue;
      }

      PixA::RodList::const_iterator the_rod_iter = rod_list.find(pp0_list.location());
      if (the_rod_iter == rod_list.end()) {
	std::cerr << "ERROR [" << argv[0] << ":main] Rod " << pp0_list.location() << " does not exist in " << rod_list.location() << std::endl;
	continue;
      }

      bool enabled=true;
      std::auto_ptr<CAN::IAnalysisDataServer> data_server(new CAN::AnalysisDataServer(conn));
      std::vector< PixA::DisabledListRoot >::const_iterator disabled_list_iter = disabled_lists.begin();
      for (std::map<std::string, std::pair<CAN::ESerialNumberType, CAN::SerialNumber_t> >::const_iterator source_iter = analysis_info->beginSource();
	   source_iter != analysis_info->endSource();
	   ++source_iter, ++disabled_list_iter) {

	bool crate_enabled = PixA::enabled( *disabled_list_iter, the_crate_iter );
	if (!crate_enabled) {
	  std::cerr << "ERROR [" << argv[0] << ":main] Crate " << PixA::connectivityName( the_crate_iter ) << " disabled. Skipping " << *rod_iter << std::endl;
	  enabled=false;
	  break;
	}

	const PixA::DisabledList rod_disabled_list( PixA::rodDisabledList( *disabled_list_iter, the_crate_iter) );

	bool rod_enabled = PixA::enabled( rod_disabled_list, the_rod_iter );
	if (!rod_enabled) {
	  std::cerr << "ERROR [" << argv[0] << ":main] ROD " << PixA::connectivityName( the_rod_iter ) << " disabled. Skipping " << *rod_iter << std::endl;
	  enabled = false;
	  break;
	}

	if (source_iter->second.first == CAN::kScanNumber) {
	  assert( disabled_list_iter != disabled_lists.end());

	  CAN::InfoPtr_t<PixLib::ScanInfo_t> a_scan_info;
	  if (source_iter->second.second == scan_serial_number) {
	    a_scan_info = scan_info;
	  }
	  else {
	    a_scan_info = CAN::InfoPtr_t<PixLib::ScanInfo_t>( CAN::MetaDataService::instance()->getScanInfo(source_iter->second.second));
	  }
	  std::auto_ptr<CAN::DirectRetrievalDataServer>
	    a_scan_data_server( new CAN::DirectRetrievalDataServer(*histo_receiver,
								   source_iter->second.second,
								   *rod_iter,
								   conn,
								   scan_info) );

	  a_scan_data_server->setDisabledList( PixA::createPp0DisabledList( conn,
									    *disabled_list_iter,
									    *rod_iter) );

	  data_server->addDataServer(source_iter->first, a_scan_data_server.release() );
	}
      }

      if (!enabled) continue;

      if (log_level>=CAN::kInfoLog) {
	std::cout << "INFO "<<argv[0] <<":main] request histograms for " << *rod_iter << std::endl;
      }
      rod_analysis->requestHistograms(data_server.get(), *rod_iter);

      if (log_level>=CAN::kInfoLog) {
	std::cout << "INFO "<<argv[0] <<":main] analyse " << *rod_iter << std::endl;
      }
      CAN::ExtendedAnalysisResultList_t results;
      rod_analysis->analyse(data_server.get(), *rod_iter, results);
      bool perform_post_processing=(post_processing!=NULL);

      if (log_level>=CAN::kInfoLog) {
	if (log_level>=CAN::kDiagnosticsLog) {
	  dumpAnalysisResults( *rod_iter, results);
	}

	if (log_level>=CAN::kDiagnosticsLog) {
	  std::cout << "INFO "<<argv[0] <<":main] classify results for " << *rod_iter << std::endl;
	}
	
	bool status = classification->classify(data_server.get(), *rod_iter, results);
	if (log_level>=CAN::kInfoLog) {
	  std::cout << "INFO "<<argv[0] <<":main] Classification of results for " << *rod_iter << " : " << (status ? " Passed" : "Failed") << std::endl;
	}
	perform_post_processing &= status;

      }

      if (perform_post_processing) {
	if (log_level>=CAN::kDiagnosticsLog) {
	  dumpAnalysisResults( *rod_iter, results);
	}

	if (log_level>=CAN::kDiagnosticsLog) {
	  std::cout << "INFO "<<argv[0] <<":main] post-processing for " << *rod_iter << std::endl;
	}
	post_processing->requestData(data_server.get(), *rod_iter);
	post_processing->process(data_server.get(), *rod_iter, results);

	if (log_level>=CAN::kInfoLog) {
	  std::cout << "INFO "<<argv[0] <<":main] Finished post-processing for " << *rod_iter << "." << std::endl;
	}

      }

      if (log_level>=CAN::kDiagnosticsLog) {
	dumpAnalysisResults( *rod_iter, results);
      }

      if (log_level>=CAN::kInfoLog) {
	std::cout << "INFO "<<argv[0] <<":main] done with " << *rod_iter << std::endl;
      }

      // do something with the analysis results ?
    }

  }
  catch (PixA::ConfigException &err) {
     std::cerr << "ERROR [" << argv[0] << ":main] Caught config exception " << err.getDescriptor() << std::endl;
  }
  catch( CAN::MRSException &err) {
    std::cerr << "ERROR [" << argv[0] << ":main] Caught exception : "
	      << err.getDescriptor()
	      << std::endl;
    return -1;
  }
  catch (std::exception &err) {
    std::cerr << "ERROR [" << argv[0] << ":main] Caught standard exception " << err.what() << std::endl;
  }

  PixA::ConnectivityManager::deepCleanup();
  CAN::MetaDataService::deepCleanup();
  PixA::DbManager::deepCleanup();

  return 0;
}
