#ifndef _analysisOutputBOC_h_
#define _analysisOutputBOC_h_

#include "analysisOutput_Base.h"


class ResultBOC : public AnalysisResult { //this is for one scan and one module
 public:
  ResultBOC() :AnalysisResult() {};
  ~ResultBOC() {};

  float bestViset;
  float optimalThreshold;
  float optimalDelay;

 public:
  //ClassDef(ResultBOC,1) //not 0 -> activate I/O  ; not needed at the moemnt, put in if needed!
};

#endif
