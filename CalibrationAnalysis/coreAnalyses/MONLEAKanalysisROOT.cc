#ifndef _MONLEAKanalysisROOT_CC_
#define _MONLEAKanalysisROOT_CC_

#include "MONLEAKanalysisROOT.h"
#include <PixDbGlobalMutex.hh>
#include <Lock.hh>
#include <iostream>
#include <sstream>
#include "TMath.h"

MONLEAKanalysisROOT::MONLEAKanalysisROOT(): _Ncol(0), _Nrow(0), _Nfe(0), _ChipType(PixGeometry::INVALID_MODULE)
{
  std::cout << "MONLEAKanalysis:  constructed." << std::endl;
} 


MONLEAKanalysisROOT::~MONLEAKanalysisROOT()
{
  //WriteResults();
}

void MONLEAKanalysisROOT::WriteResults()
{
  TFile f("resultsMONLEAK.root","update");
  //f.WriteTObject(m_result.get(),"scanHistos","SingleKey"); //as one array
  f.Close();  
}

void MONLEAKanalysisROOT::Initialize()
{
  AnalysisInputMONLEAK* hmonleak = (AnalysisInputMONLEAK*) m_scan->At(0);
  if(!hmonleak || !hmonleak->histo_monleak) return; 

  PixGeometry geo(PixA::nRows(hmonleak->histo_monleak),PixA::nColumns(hmonleak->histo_monleak)); 
  _ChipType = geo.pixType();

  _Nrow = helper.getNrow(_ChipType);
  _Ncol = helper.getNcol(_ChipType);
  _Nfe = helper.getNfrontEnd(_ChipType); // # of FEs in one module

  // -- no cuts yet...
}  

void MONLEAKanalysisROOT::Calculate() 
{
 
  std::cout <<  m_scan->GetEntries() << std::endl;
  std::cout << "**************************************" << std::endl;
  std::cout << "MONLEAKanalysis::Calculate  starting" << std::endl;
  m_scan->R__FOR_EACH(AnalysisInputMONLEAK,PrintInfo)(); 
  std::cout << "MONLEAKanalysis: These were the module names." << std::endl;  


  // Loop over modules
  for(Int_t i=0; i<m_scan->GetEntries(); i++) { 

    float LeakageCurrent;    
    float MonLeakCurrent;
    float FeedbackCurrent = 1./8.;  // -- feedback current value for Preset conf (IF=IFTrim=1, FDACS=0)
    //    float aver_current_FE[16];
    std::vector<float> aver_current_FE (_Nfe, 0.);
    
    float iLeakTotal = 0.; float iLeakL0 = 0.;  float iLeakL1 = 0.;
    float iLeakL2 = 0.;  float iLeakDisk = 0.;
    
    float iLeak_FEmax=0.; float iLeak_FEmin=999999.;   

    std::cout << "Looping over scan entries" << std::endl;

    ResultMONLEAK* module_result = new ResultMONLEAK();
   
    module_result->pp0Name = ((AnalysisInputMONLEAK*)m_scan->At(i))->pp0Name;
    std::cout << "pp0 name = " << module_result->pp0Name << std::endl;
    module_result->moduleName = ((AnalysisInputMONLEAK*)m_scan->At(i))->moduleName;
    std::cout << "module name = " << module_result->moduleName << std::endl;
    // module_result->MakePixelMap = create_Pixelmap; 
    
    std::cout << "Accessing histograms from scan" << std::endl;
    const TH2* h_monleak = ((AnalysisInputMONLEAK*)m_scan->At(i))->histo_monleak;
        
    if (h_monleak==NULL){
      std::cout << "MONLEAKanalysis: WARNING  no histos for module " << module_result->moduleName << " -> skipping." << std::endl;
      module_result->passed=false;
      m_result->Add( module_result );
      continue;
    }
    module_result->passed=true;
    //    std::cout << "------> Analysing module: " << module_result->moduleName << std::endl; 
        
    //get FE disable info
    FE_disable = ((AnalysisInputMONLEAK*)m_scan->At(i))->fe_disable;
    
    TString mod_name = module_result->moduleName;
    std::stringstream m (std::stringstream::out);
    m << mod_name;
    std::string mod_name_str = m.str();
        
    // -- Get module location
    bool isL0 = false;    if (mod_name_str.compare(0,2,"L0")==0) { isL0 = true; }
    bool isL1 = false;    if (mod_name_str.compare(0,2,"L1")==0) { isL1 = true; }  
    bool isL2 = false;    if (mod_name_str.compare(0,2,"L2")==0) { isL2 = true; }
    bool isDisk = false;  if (mod_name_str.compare(0,1,"D")==0)  { isDisk = true; }
    
    //std::cout << "isL0? " << isL0 << " isL1? " << isL1 << " isL2? " << isL2 << " isDisk? " << isDisk << std::endl;
    
    std::shared_ptr<TH2F> iLeakMap(new TH2F("Leakage Current Map "+mod_name,"Map "+mod_name,helper.Ncols,-0.5,helper.Ncols-0.5,helper.Nrows,-0.5,helper.Nrows-0.5));
    //    int nmax = 1024; // -- default
    int nmax = 1360;
    std::shared_ptr<TH1F> iLeak(new TH1F("Leakage Current "+mod_name,"Current "+mod_name,nmax,0,0.125*nmax));
    std::shared_ptr<TH1F> iLeak_FE(new TH1F("Leakage Current/FE "+mod_name,"Current/FE"+mod_name,16,-0.5,15.5));
    
    // -- Loop over pixels (histogram entries)
    //for(int col=0; col<helper.Ncols; col++) {
    //for(int row=0; row<helper.Nrows; row++) {
    for(int col=0; col<_Ncol; col++) {
      for(int row=0; row<_Nrow; row++) {
	
 	int FE = -1;
	if(_ChipType == PixGeometry::FEI2_CHIP || _ChipType == PixGeometry::FEI2_MODULE){
	  if (row < 160)
	    FE = col/18;
	  else
	    FE = 15 - col/18;
	  if (FE_disable[FE] == 1)
	    continue;
	}else if(_ChipType == PixGeometry::FEI4_CHIP || _ChipType == PixGeometry::FEI4_MODULE){
	  FE = col/80;
	}

	MonLeakCurrent = h_monleak->GetBinContent(col+1,row+1);        // -- get monleak current
	// max current = 1023 dacs, if scan fails pixels have curr=1279 dacs 

	//	if (MonLeakCurrent>1024.)    continue;
                   
	  MonLeakCurrent = 0.125*MonLeakCurrent;                     // -- DAC to nA conversion
	  LeakageCurrent = MonLeakCurrent - 2.*FeedbackCurrent;      // -- get Leakage current
	  if (LeakageCurrent<0.)  LeakageCurrent = 0.;
	  
	  // -- all together
	  iLeakMap->Fill(col,row,LeakageCurrent);
	  iLeak->Fill(LeakageCurrent);
	  
	  // -- Total leakage current in the module
	  iLeakTotal += LeakageCurrent; 
	  
	  if (isL0)        iLeakL0 = iLeakL0 + LeakageCurrent;
	  else if (isL1)   iLeakL1 = iLeakL1 + LeakageCurrent;
	  else if (isL2)   iLeakL2 = iLeakL2 + LeakageCurrent;
	  else if (isDisk) iLeakDisk = iLeakDisk + LeakageCurrent;
	  
	  // 	  std::cout << LeakageCurrent << std::endl;
	  // 	  std::cout << LeakageCurrent/46080. << std::endl;
	  // 	  std::cout << iLeakL0 << std::endl;

	  // -- FE's
	  if(_ChipType == PixGeometry::FEI2_CHIP || _ChipType == PixGeometry::FEI2_MODULE){
	    aver_current_FE[FE] = aver_current_FE[FE] + LeakageCurrent/2880.;
	  }else if(_ChipType == PixGeometry::FEI4_CHIP || _ChipType == PixGeometry::FEI4_MODULE){
	    aver_current_FE[FE] = aver_current_FE[FE] + LeakageCurrent/26880.;
	  }
	  std::cout << "FE number " << FE << ", average current " << aver_current_FE[FE] << std::endl;
	  //	}
      }
    }// end pixel-loop

    //    for (int FE=0; FE<16;++FE){
    for (int FE=0; FE<_Nfe;++FE){
      // std::cout << "For module... " << std::endl;
      std::cout << "FE number " << FE << ", average current " << aver_current_FE[FE] << std::endl;
      iLeak_FE->SetBinContent(FE+1,aver_current_FE[FE]);
     
      if (aver_current_FE[FE]>iLeak_FEmax)
	iLeak_FEmax = aver_current_FE[FE];
      
      if (aver_current_FE[FE]<iLeak_FEmin)
	iLeak_FEmin = aver_current_FE[FE];
    }


    // -- save results
    std::cout << "[MONLEAKanalysis] Saving results" << std::endl;
    
    module_result->iLeak_FE = iLeak_FE;
    module_result->iLeak_all = iLeakTotal/46080.;
    module_result->iLeakMap = iLeakMap;
    module_result->iLeak = iLeak;
    module_result->iLeakTotal = iLeakTotal;
    module_result->iLeak_FEmax = iLeak_FEmax;
    module_result->iLeak_FEmin = iLeak_FEmin;
    module_result->iLeak_FE0 = aver_current_FE[0];
    module_result->iLeak_FE1 = aver_current_FE[1];
    if(_ChipType == PixGeometry::FEI2_CHIP || _ChipType == PixGeometry::FEI2_MODULE){
      module_result->iLeak_FE2 = aver_current_FE[2];
      module_result->iLeak_FE3 = aver_current_FE[3];
      module_result->iLeak_FE4 = aver_current_FE[4];
      module_result->iLeak_FE5 = aver_current_FE[5];
      module_result->iLeak_FE6 = aver_current_FE[6];
      module_result->iLeak_FE7 = aver_current_FE[7];
      module_result->iLeak_FE8 = aver_current_FE[8];
      module_result->iLeak_FE9 = aver_current_FE[9];
      module_result->iLeak_FE10 = aver_current_FE[10];
      module_result->iLeak_FE11 = aver_current_FE[11];
      module_result->iLeak_FE12 = aver_current_FE[12];
      module_result->iLeak_FE13 = aver_current_FE[13];
      module_result->iLeak_FE14 = aver_current_FE[14];
      module_result->iLeak_FE15 = aver_current_FE[15];
    }
    //    iLeakL0 = iLeakL0/(float)helper.Ncols*helper.Nrows;
    //     iLeakL1 = iLeakL1/(float)helper.Ncols*helper.Nrows;
    //     iLeakL2 = iLeakL2/(float)helper.Ncols*helper.Nrows;
    //     iLeakDisk = iLeakDisk/(float)helper.Ncols*helper.Nrows;
    
    std::cout << iLeakL0 << std::endl;
    
    if (isL0)       {         module_result->iLeak_L0 = iLeakL0/46080.;   }
    else if (isL1)  {         module_result->iLeak_L1 = iLeakL1/46080.;   }
    else if (isL2)  {         module_result->iLeak_L2 = iLeakL2/46080.;   }
    else if (isDisk){         module_result->iLeak_disks = iLeakDisk/46080.;}
    
    //std::cout << "Adding result to vector" << std::endl;
    m_result->Add( module_result );  
    
  } // -- end module loop
  
  std::cout << "  after calculate  results size: " << m_result->GetEntries() << std::endl;
  std::cout << "MONLEAKanalysis: analysis finished." << std::endl;

} // -- end calculate


//Not being used!
void MONLEAKanalysisROOT::AdditionalCalculations() 
{
  //
}

void MONLEAKanalysisROOT::findBadPixels(ResultMONLEAK* module, int ientry, std::vector<unsigned short int> fe_disable) 
{
  //
}


void MONLEAKanalysisROOT::QualifyResults() 
{
  // bool allPassed(true);
  //   for(Int_t i=0; i<m_result->GetEntries(); i++) { 
  //     ResultMONLEAK* module_result = (ResultMONLEAK*)m_result->At(i); 
  //     if(!module_result->passed) { 
  //       allPassed=false;
  //       break;
  //     }
  //   }  
  //   if(allPassed)
  //     m_status=kPassed;
  //   else 
  //     m_status=kFailed;
  
  //   std::cout << "MONLEAKanalysis::QualifyResults  Total result is :" << ( (allPassed)? "OK":"FAILED" ) << std::endl;
  //   std::cout << "*************************************" << std::endl;
}


void MONLEAKanalysisROOT::CheckForKnownProblems() //setting of failure bits
{
 
}




#endif

