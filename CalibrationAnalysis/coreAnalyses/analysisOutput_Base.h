#ifndef _analysisOutputBase_h_
#define _analysisOutputBase_h_

#include <TString.h>
#include <TH2F.h>


class AnalysisResult : public TObject  { //this is for one scan and one module
 public:
  AnalysisResult() :moduleName(""),passed(false),tests(0) {};
  virtual ~AnalysisResult() {};

  TString rodName;
  TString pp0Name; 
  TString moduleName; 

  bool passed;
  UInt_t tests;

 public:
  //ClassDef(AnalysisResult,1) //not 0 -> activate I/O  ; not needed at the moemnt, put in if needed!
};

#endif
