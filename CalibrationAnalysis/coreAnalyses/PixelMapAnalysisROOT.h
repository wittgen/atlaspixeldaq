#ifndef _PixelMapanalysisROOT_h_
#define _PixelMapanalysisROOT_h_ _

#include "ROOTanalysisBase.h"
#include "analysisInput_PixelMap.h"
#include "analysisOutput_PixelMap.h"
#include <iostream>
#include <TFile.h>
#include <TString.h>



class PixelMapAnalysisROOT : public ROOTanalysisBase 
{
 public:
  PixelMapAnalysisROOT();
  ~PixelMapAnalysisROOT();
    
  void Initialize();

  void Calculate(); //internal calculations

  void QualifyResults(); //apply cuts from analysis config

  void CheckForKnownProblems(); //setting of failure bits


 private:
  void WriteResults();
  int  CountPixelsWithBit(const PixelMap_t& pixelMap, int i, int selectType=-1); // How many pixels have bit i set
  void AddPixelMap(ResultPixelMap* module_result, PixelMap_t * pixelmap, int bitToSet); 
};


#endif

