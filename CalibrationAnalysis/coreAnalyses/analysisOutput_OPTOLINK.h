#ifndef _analysisOutputBOC_h_
#define _analysisOutputBOC_h_

#include "analysisOutput_Base.h"


class ResultOPTOLINK : public AnalysisResult { //this is for one scan and one module
 public:
  ResultOPTOLINK() {
      passed = false;
      optimalPhase_LINK1=255;
      optimalPhase_LINK2=255;
      passed_LINK1 = false;
      passed_LINK2 = false;
  };
  ~ResultOPTOLINK() {};
  
  float bestViset;
  float optimalThreshold_Link1;
  float optimalThreshold_Link2;
  float optimalDelay_Link1;
  float optimalDelay_Link2;
  float configThreshold_Link1;
  float configThreshold_Link2;
  float configDelay_Link1;
  float configDelay_Link2;
  float configViSet_Link1;
  float configViSet_Link2;
  float commonSpots;
  float minErrors_Link1;
  float minErrors_Link2;
  float minErrors;
  bool visetOk;
  bool visetLow;
  bool goodThr_Link1;
  bool goodThr_Link2;
  bool goodDel_Link1;
  bool goodDel_Link2;
  bool goodTuning_Link1;
  bool goodTuning_Link2;
  bool useNewTuning;
  //IBL
  unsigned int optimalPhase_LINK1;
  unsigned int optimalPhase_LINK2;
  bool passed_LINK1;
  bool passed_LINK2;

 public:
  //ClassDef(ResultOPTOLINK,1) //not 0 -> activate I/O  ; not needed at the moemnt, put in if needed!
};

#endif
