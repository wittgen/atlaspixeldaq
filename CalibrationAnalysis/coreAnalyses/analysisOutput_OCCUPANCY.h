#ifndef _analysisOutputOCCUPANCY_h_
#define _analysisOutputOCCUPANCY_h_

#include "analysisOutput_Base.h"
#include "PixelCoralClientUtils/PixelMap.h"

class ResultOCCUPANCY : public AnalysisResult { //this is for one scan and one module
 public:
  ResultOCCUPANCY(){
      
  };
  ~ResultOCCUPANCY() {};

  bool Passed_FirstMaskstageTest;
  float PixExtraHits;
  float PixMissingHits;
  float MaxMissingHits;
  float MaxExtraHits;
  float MissingHits;
  float ExtraHits;
  float MissingHitsMS1;
  float PixMissingHitsMS1;
  float MissingHitsMSnot1;
  float PixMissingHitsMSnot1;
  float ExtraHitsMS1;
  float PixExtraHitsMS1;
  float ExtraHitsMSnot1;
  float PixExtraHitsMSnot1;
  float DeadPixel;
//*********** new variable added [maria elena] to count the sum of dead + badmissing + badextra pixels
  float BadPixel;
//**********************
  float OverThreshold; // will be used only for analog scans around threshold
  float SeenPercentage; // will be used only for analog scans around threshold
  bool DeadFE;
  bool HVopen;
  bool MakePixelMap; // to make sure the analysis only creates maps if selected

  PixelMap_t deadPixelMap;

 public:
  //ClassDef(ResultOCCUPANCY,1) //not 0 -> activate I/O  ; not needed at the moemnt, put in if needed!
};

#endif
