#ifndef _ROOTanalysisHelper_h_
#define _ROOTanalysisHelper_h_ _

#include <iostream>
#include <memory>
#include <TFile.h>
#include <TH2F.h>
#include <cmath>
#include "math.h"
#include "DataContainer/HistoUtil.h"
#include "PixFe/PixGeometry.h"
#include "DataContainer/GenericHistogramAccess.h"


class ROOTanalysisHelper 
{
 public:
  ROOTanalysisHelper();
  ROOTanalysisHelper(int nClasses, bool ifIntime);
  ~ROOTanalysisHelper() {};

  //Test if a given fraction of histogram enries is below/above some cut:  
  bool FulfillMinCut(Float_t value, Float_t cutValue); 
  bool FulfillMaxCut(Float_t value, Float_t cutValue);
  bool FulfillMinMaxCut(Float_t value, Float_t cutMinValue, Float_t cutMaxValue);
  bool FulfillMinCut(TH1F * histo, Float_t cutValue, Float_t fraction=0.95); 
  bool FulfillMaxCut(TH1F * histo, Float_t cutValue, Float_t fraction=0.95); 
  
  float PercentFailedMaxCut(TH1F * histo, Float_t cutValue, Float_t fraction=0.95); 
  float PercentFailedMinCut(TH1F * histo, Float_t cutValue, Float_t fraction=0.95); 

  float PercentFailedMaxCut(std::shared_ptr<TH1F> histo, Float_t cutValue); 
  float PercentFailedMinCut(std::shared_ptr<TH1F> histo, Float_t cutValue); 

  //Convert a 2D per pixel Histo  to a 1D histo of the entries
  std::shared_ptr<TH1F> IntegrateHisto(const TH2* histo, Int_t bin_min, Int_t bin_max, Int_t pixclass=-1, Int_t FE=-1, Int_t zeroBin=0); //FE=-1 -> integrate over all FrontEnds 

//**************************************[maria elena] added this function
std::shared_ptr<TH1F> IntegrateHisto(const TH2* histo, Int_t bin_min, Int_t bin_max, Int_t pixclass, Int_t FE, Int_t zeroBin, int chip_type, int mask, int _Ntotmasks,std::vector<std::vector<unsigned short int> > cp_enable, std::vector<unsigned short int> fe_disable); //FE=-1 -> integrate over
//********************************************************************
    
  //Convert a 2D per pixel Histo  to a 1D histo of the entries by applying cuts on a different histogram
  std::shared_ptr<TH1F> IntegrateHisto(const TH2* anaHisto, Int_t bin_min, Int_t bin_max, const TH2* cutHisto, Int_t cut_min, Int_t cut_max, Int_t pixclass, Int_t FE, Int_t zeroBin=0);
  //Set singel bits in a 4Byte variable
  void SetBit(UInt_t &var, Int_t bit); 

  bool HasBit(const UInt_t &var, Int_t bit); 


  //Dump TObjects to dumpedInfo.root (e.g. 1D or 2D histos) 
  void Dump(TObject* histo);     

  Float_t GetVariance(TH1F &histo);

  int getFrontEndStartCol(int fe);
  int getFrontEndStartCol(int fe, int chip_type);
  int getFrontEndStopCol(int fe);
  int getFrontEndStopCol(int fe, int chip_type);
  int getFrontEndStartRow(int fe);
  int getFrontEndStartRow(int fe, int chip_type);
  int getFrontEndStopRow(int fe);
  int getFrontEndStopRow(int fe, int chip_type);

  int getNrow(int chip_type);
  int getNcol(int chip_type);
  int getNfrontEnd(int chip_type);
  int getNcp(int chip_type);
  int getNpixel(int chip_type);

//*********************** [maria elena] added this function
  int getNpixelClasses(int chip_type);
//************************************
    
  int moduleCol(int fe, int col);
  int moduleCol(int fe, int col, int chip_type);
  int moduleRow(int fe, int row);
  int moduleRow(int fe, int row, int chip_type);
  int frontEnd(int col, int row); 
  int frontEnd(int col, int row, int chip_type); 
  int columnPair(int moduleCol, int moduleRow); //0..8
  int columnPair(int moduleCol, int moduleRow, int chip_type); //0..8
  bool isMasked(int col, int row, int mask); 
  bool isMasked(int col, int row, int mask, int _Ntotmasks, int chip_type);//[maria elena] modified
  unsigned short pixelType(int col, int row); 
  unsigned short pixelType(int col, int row, int chip_type); 
  int pixelTypeInt(int col, int row); 
  int pixelTypeInt(int col, int row, int chip_type); 
  std::string getPixelTypeString(int pixeltype); //input: 0 to 5
  std::string getPixelClassString(int pixeltype, int nclasses = 3); //input: 1 to 3
//*********************** [maria elena] added this function
  std::string getPixelClassString(int pixeltype, int chip_type, int nclasses);
//*******************************************************
  unsigned short pixelClass(int col, int row, int nclasses = 3); 
//*********************** [maria elena] added this function
  unsigned short pixelClass(int col, int row, int chip_type, int nclasses);
//*******************************************************

  Float_t lowestXpercent(const float percentCut, TH1F* h); 
  Float_t highestXpercent(const float percentCut, TH1F* h); 

  //enum EPixelType { kNoPixel=0, kNormalPixel=1, kLongPixel=2, kGangedPixel=4,  kLongGangedPixel=8, kInterGangedPixel=16, kLongInterGangedPixel=32 };
  int NpixelTypes;
  int NpixelClasses;
  int Nrows;
  int Ncols;
  int NfrontEnds;
 
 private: 
  bool intimeAnalysis;
};


#endif

