#ifndef _analysisInputT0_h_
#define _analysisInputT0_h_

#include "analysisInput_Base.h"


class AnalysisInputT0 : public AnalysisInput { //this is for one scan and one module
 public:
  //AnalysisInputT0() :AnalysisInput(), histo_threshold_1(NULL), histo_threshold_2(NULL), nPixel(0), nMaskStages(0) {};
  AnalysisInputT0() :AnalysisInput() {};
  ~AnalysisInputT0() {};
  std::vector<std::vector<const TH2*> > histo; 
  
  Int_t nMaskStages, NTotmasks, nDelRangeSteps, nDelSteps;
  Int_t Nmasks;
  Int_t Nevents;
  Int_t Nmasksdone;
  Int_t _ChipFeFlav;
  std::vector<Float_t> delRange_array;
  std::vector<Float_t> delStep_array;
  Float_t minStrobeDelay, maxStrobeDelay;
  TString moduleName; 
  TString pp0Name;


  //Helper functions
  void PrintInfo() {
    std::cout << "AnalysisInputT0 for pp0 " << pp0Name.Data() << ", module: " << moduleName.Data() << std::endl;
  };

};

#endif
