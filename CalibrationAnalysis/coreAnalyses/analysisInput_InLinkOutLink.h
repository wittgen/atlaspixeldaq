// Jun 27 2008 Shih-Chieh Hsu
//             Input Class for InLinkOutLink analysis 
//
#ifndef _analysisInputInLinkOutLink_h_
#define _analysisInputInLinkOutLink_h_

#include "analysisInput_Base.h"


class AnalysisInputInLinkOutLink : public AnalysisInput { //this is for one scan and one module
 public:
  AnalysisInputInLinkOutLink() :AnalysisInput(), MBs(-9) {};
  ~AnalysisInputInLinkOutLink() {};

  //Rod as base unit 
  TString  rodName; 

  // histogram
  const TH2* histo_InLinkOutLink;

  Int_t MBs;     // [40,80,160 MHz]
  Int_t nPixel;  // depending on mask steps
  std::vector<histoAxis_t> axis; //the axis of the histo (the same for all histos) of type histoAxis_t 

  //InLink/OutLink MCC speed is 40 MHz
  Int_t moduleId;
  Int_t inLink;
  Int_t outLink1A;
  Int_t outLink2A;

  //Helper functions
  void PrintInfo() {
    std::cout << "AnalysisInputInLinkOutLink for module: " << moduleName.Data() << std::endl;
  };

 public:
  //ClassDef(AnalysisInputInLinkOutLink,1) //not 0 -> activate I/O 
};

#endif
