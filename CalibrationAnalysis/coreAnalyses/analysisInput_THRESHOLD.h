#ifndef _analysisInputTHRESHOLD_h_
#define _analysisInputTHRESHOLD_h_

#include "analysisInput_Base.h"
#include <vector>

class AnalysisInputTHRESHOLD : public AnalysisInput { //this is for one scan and one module
 public:
  AnalysisInputTHRESHOLD() :AnalysisInput(), histo_chi2(NULL), histo_mean(NULL), histo_sigma(NULL), MBs(-9),nPixel(0) {}

  ~AnalysisInputTHRESHOLD() {};

  const TH2* histo_chi2;  // pixle maps
  const TH2* histo_mean;  
  const TH2* histo_sigma;  
  //  std::vector<histoAxis_t> axis; //the axis of the histo (the same for all histos) 


  Int_t MBs;    /////   [40,80,160 MHz]
  Int_t nPixel, nMaskStages, nTotMaskStages ;  //depending on mask steps
  std::vector<histoAxis_t> axis; //the axis of the histo (the same for all histos) of type histoAxis_t 

  //Helper functions
  void PrintInfo() {
    std::cout << "AnalysisInputTHRESHOLD for module: " << moduleName.Data() << "  MBit/s=" << MBs << "  HistoEntries:"
	      << entriesSafe(histo_chi2) << " " << entriesSafe(histo_mean) << " " << entriesSafe(histo_sigma) << std::endl;
  };

 public:
  //ClassDef(AnalysisInputTHRESHOLD,1) //not 0 -> activate I/O 
};

#endif
