#ifndef _analysisInputBOND_h_
#define _analysisInputBOND_h_

#include "analysisInput_Base.h"
#include <iostream>
#include <vector>
#include <list>
#include <TH2F.h>
#include <TString.h>
#include <TList.h>


class AnalysisInputBOND : public AnalysisInput { //this is for one analysis and one module (may contain histos from different scans)
 public:
  AnalysisInputBOND() :AnalysisInput(), MBs(-9) {};
  ~AnalysisInputBOND() {};

  const TH2* histo_sigma_HVon;  
  const TH2* histo_sigma_HVoff;  
  //  std::vector<histoAxis_t> axis; //the axis of the histo (the same for all histos) 


  Int_t MBs;    /////   [40,80,160 MHz]
  Int_t nPixel;  //depending on mask steps

  std::vector<histoAxis_t> axis; //the axis of the histo (the same for all histos) of type histoAxis_t 
    float zaehler;
    float nenner;

  //Helper functions
  void PrintInfo() {
    std::cout << "AnalysisInputBOND for module: " << moduleName.Data() << "  MBit/s=" << MBs << "  HistoEntries: " << histo_sigma_HVon->GetEntries() << "(HVon), " << histo_sigma_HVoff->GetEntries() << "(HVoff)" << std::endl;
  };

 public:
  //ClassDef(AnalysisInputBOND,1) //not 0 -> activate I/O 
};

#endif
