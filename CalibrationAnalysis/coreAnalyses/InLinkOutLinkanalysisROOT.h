// author:2008-06-27 Shih-Chieh Hsu
//        header file of the core of inlink outlink analysis

#ifndef _InLinkOutLinkanalysisROOT_h_
#define _InLinkOutLinkanalysisROOT_h_ _

#include "ROOTanalysisBase.h"
#include "analysisInput_InLinkOutLink.h"
#include "analysisOutput_InLinkOutLink.h"
#include <iostream>
#include <TFile.h>
#include <TString.h>



class InLinkOutLinkanalysisROOT : public ROOTanalysisBase 
{

 public:
  InLinkOutLinkanalysisROOT();
  ~InLinkOutLinkanalysisROOT();
 
  void Initialize();

  void Calculate(); //internal calculations

  void QualifyResults(); //apply cuts from analysis config

  void CheckForKnownProblems(){};

  //The cuts are directly accessible from the AutomaticAnalysis
  Float_t cut_drx_min; //cut on  difference of RX pin current with/without turnning on Viset, L0 requirement is factor of 2 larger 
  Float_t cut_ratio_bit_min;  //cut on ratio bit min (single value)

  Int_t   m_percentCut; // used for lower and upper bin value calculations

 private:
  void WriteResults();

};


#endif
