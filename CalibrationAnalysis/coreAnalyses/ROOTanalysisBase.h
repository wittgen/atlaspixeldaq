#ifndef _ROOTanalysisBase_h_
#define _ROOTanalysisBase_h_ _

#include <TFile.h>
#include <TString.h>
#include <iostream>
#include "analysisInput_Base.h"
#include "ROOTanalysisHelper.h"
#include "PixDbGlobalMutex.hh"
#include "Lock.hh"

#include <LogLevel.hh>

class ROOTanalysisBase  
{
 public:
 ROOTanalysisBase(): m_logLevel(CAN::kDiagnostics1Log), m_status(kWaiting), m_result(new TObjArray()) { m_result->SetOwner();}
;
  virtual ~ROOTanalysisBase() {
/*     m_result->Delete(); */
/*     delete m_result; */
    CAN::Lock globalLock(CAN::PixDbGlobalMutex::mutex());
    m_result.reset();
  };

  enum Status_t { kWaiting, kRunning, kPassed, kFailed, kPartPassed, kPartFailed }; //kPartPassed designates partly missing input data but the given is ok, kPartFailed indicates unsufficient input data 


  void Reset() {
    CAN::Lock globalLock(CAN::PixDbGlobalMutex::mutex());
    m_result->Delete(); //before starting analysis over new pp0 delete old results
    std::cout << "  after reset   results size: " << m_result->GetEntries() << std::endl; //temp
  }

  void Analyse(AnalysisInput_t scan) { //not to be overwritten!
    m_scan=scan; //this is a pointer
    m_status=kRunning;
    Initialize();
    Calculate();
    CheckForKnownProblems();
    QualifyResults(); //has to set status to either passed or failed
  }

 protected:
  virtual void Initialize()=0;

  virtual void Calculate()=0; //internal calculations

  virtual void QualifyResults()=0; //apply cuts from analysis config

  virtual void CheckForKnownProblems()=0; //setting of failure bits
  //                                        the falure bits are located in the analysis results, they exist per module

 public:
  Status_t GetStatus() { return m_status; };

  AnalysisResult_t GetResult() { return m_result; };

  void SetLogLevel(CAN::ELogLevel level) { m_logLevel=level; };

  void WriteRootFile( TString fileName, const AnalysisInput_t analysisInput, std::string pp0Name) {
    TFile f("_"+fileName,"UPDATE");  
    //    analysisInput->Write("scanHistos",1); //wirte the list as one
    //scan->Write(); //write each scanHistoTHRESHOLD_t seperately
    f.WriteTObject(analysisInput.get(),"scanHistos_"+TString(pp0Name),"SingleKey"); //as one array
    f.Close();  
    std::cout << "Writing of analysisInput file  _" << fileName.Data() << " was succesful. Added " << "scanHistos_"+TString(pp0Name) << std::endl;
  }

 protected:
  CAN::ELogLevel m_logLevel;  
  Status_t m_status;
  AnalysisInput_t m_scan;
  AnalysisResult_t m_result;
  ROOTanalysisHelper helper;
};


#endif

