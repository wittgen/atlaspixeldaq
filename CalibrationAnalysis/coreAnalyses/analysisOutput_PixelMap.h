#ifndef _analysisOutputPixelMap_h_
#define _analysisOutputPixelMap_h_

#include "analysisOutput_Base.h"
#include "PixelCoralClientUtils/PixelMap.h"



class ResultPixelMap : public AnalysisResult { //this is for one scan and one module
 public:
  ResultPixelMap() :infoUsed(0)
    {};
  ~ResultPixelMap() {};


  UInt_t infoUsed;
  unsigned int numBadPixels[6];    //differentially for the six pixel types 
  unsigned int numBadPixels_total; // total number
  PixelMap_t finalPixelMap; //  <---

 public:
  //ClassDef(ResultPixelMap,1) //not 0 -> activate I/O  ; not needed at the moemnt, put in if needed!
};

#endif
