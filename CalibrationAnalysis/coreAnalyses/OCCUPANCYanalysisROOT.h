#ifndef _OCCUPANCYanalysisROOT_h_
#define _OCCUPANCYanalysisROOT_h_ 


#include "ROOTanalysisBase.h"
#include "analysisInput_OCCUPANCY.h"
#include "analysisOutput_OCCUPANCY.h"
#include "TH2I.h"
#include "ROOTanalysisHelper.h"

class OCCUPANCYanalysisROOT  : public ROOTanalysisBase
{
 public:
  OCCUPANCYanalysisROOT();
  ~OCCUPANCYanalysisROOT();


 private:
  void Initialize();

  void Calculate(); //internal calculations

  float AdditionalCalculations(int module); // more calculations for analog scans
  
  void QualifyResults(); //apply cuts from analysis config

  void CheckForKnownProblems();

  void Print();

 public:
  Float_t max_bad_pixel;
  Float_t max_extra_hits_allowed;
  Float_t min_charge;
  Float_t max_missing_hits_MS1;
  bool apply_cut_on_MS1;
  Float_t DeadFE_percentage;
//********* new variable added [maria elena] to count the sum of dead + badmissing + badextra pixels
  Float_t Max_Dead_Pixel;
//*********************
  bool create_Pixelmap;
 
 private: 

  //variables

  bool allModulesPassed;
  bool Passed;

  // Algorithm parameters
  // --------------------

  int _Nmasks;
  int _Nevents;
  int _Nmasksdone;
    
  int _Ncol;
  int _Nrow;

  //int _ChipType;
    
int _ChipFeFlav;

  // # of column and row must be optimized by Yosuke. 
  int _pp0pattern[288][400];
  int _pattern[288][400];
 
  bool _digitalInjection;
  
  int _charge;

  std::vector<unsigned short int> _fe_disable;
std::vector<std::vector<unsigned short int> > _cp_enable;

};


#endif

