#ifndef _analysisOutputBASICTHR_h_
#define _analysisOutputBASICTHR_h_

#include <TString.h>
#include <TH2F.h>
#include "analysisOutput_Base.h"
#include "PixelCoralClientUtils/PixelMap.h"


class ResultBASICTHR : public AnalysisResult { //this is for one scan and one module
 public:
  ResultBASICTHR() {
    passed = 0;
    tests = 0;
    average_threshold = average_noise = RMS_threshold = RMS_noise = 0.;
    failed_fit = 0;
    passed_average_noise_max = passed_average_noise_min = passed_average_threshold_min = passed_average_threshold_max = passed_RMS_threshold_max = passed_RMS_noise_max = passed_failed_fit_max = 0;
  };
  ~ResultBASICTHR() {};

  Float_t average_threshold; //all pixels together without failed fits
  Float_t average_noise;
  Float_t RMS_threshold;
  Float_t RMS_noise;

  unsigned int failed_fit;

  bool passed_average_noise_max;
  bool passed_average_noise_min;
  bool passed_average_threshold_min;
  bool passed_average_threshold_max; 
  bool passed_RMS_threshold_max;
  bool passed_RMS_noise_max;
  bool passed_failed_fit_max;

  TH1F hInt_threshold; 
  TH1F hInt_noise;

  TH2F failed_fit_histo;
  PixelMap_t failed_fit_map;

 public:
  //ClassDef(ResultBASICTHR,1) //not 0 -> activate I/O  ; not needed at the moemnt, put in if needed!
};

#endif
