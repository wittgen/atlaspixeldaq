#ifndef _analysisOutputINTIMETHR_h_
#define _analysisOutputINTIMETHR_h_

#include <TString.h>
#include <TH2F.h>
#include "analysisOutput_Base.h"
#include "PixelCoralClientUtils/PixelMap.h"

// One object per scan per module
class ResultINTIMETHR : public AnalysisResult
{
public:
  ResultINTIMETHR() : AnalysisResult(),
    passed_all_fe_noise(false),
    passed_all_fe_threshold_mean(false),
    passed_all_fe_threshold_RMS(false),
    passed_all_fe_bad_pixel(false),
    numBadPixels(0),
    numDisconnectedPixels(0),
    numGoodFEs(0)
    {
      // Module variables
      passed_module_threshold_mean[0]=passed_module_threshold_mean[1]=passed_module_threshold_mean[2]=false;
      passed_module_threshold_RMS[0]=passed_module_threshold_RMS[1]=passed_module_threshold_RMS[2]=false;
      passed_module_noise[0]=passed_module_noise[1]=passed_module_noise[2]=passed_module_noise[3]=false;
      for(Int_t pixtype=0; pixtype<3; pixtype++)	average_threshold[pixtype]=0;
      for(Int_t pixtype=0; pixtype<3; pixtype++)	RMS_threshold[pixtype]=0;
      for(Int_t pixtype=0; pixtype<4; pixtype++)	average_noise[pixtype]=0;
      for(Int_t pixtype=0; pixtype<4; pixtype++)	RMS_noise[pixtype]=0;
      
      // FE variables
      for(Int_t FE=0; FE<16; FE++)
	{
	  for(Int_t pixtype=0; pixtype<3; pixtype++)	average_threshold_FE[FE][pixtype]=0;
	  
	  passed_FE_noise_min[FE]=false;
	  passed_FE_noise_max[FE]=false;
	  passed_FE_threshold_min[FE]=false;
	  passed_FE_threshold_max[FE]=false;
	  passed_FE_threshold_mean_min[FE]=false;
	  passed_FE_threshold_mean_max[FE]=false;
	  passed_FE_threshold_RMS_max[FE]=false;
	  passed_FE_cuts[FE]=false;
	}
    };
  ~ResultINTIMETHR()	{};
  
  // Pixel map
  bool	MakePixelMap;	// analysis creates map if true
  PixelMap_t	badPixelMap;
  
  // Error type
  bool	passed_all_fe_noise;
  bool	passed_all_fe_threshold_mean;
  bool	passed_all_fe_threshold_RMS;
  bool	passed_all_fe_bad_pixel;
  
  bool passed_module_threshold_mean[3];
  bool passed_module_threshold_RMS[3];
  bool passed_module_noise[4];
 
  // Module variables
  Float_t	average_threshold[3];	// for 3 classes of pixel types
  Float_t	RMS_threshold[3];
  Float_t	average_noise[4];		// for 4 classes of pixel types
  Float_t	RMS_noise[4];
  
  unsigned int numBadPixels;
  unsigned int numDisconnectedPixels;
  unsigned int numGoodFEs;
  
  // FE variables for COOL
  Float_t	average_threshold_FE[16][3]; //for 3 classes of pixel types
  
  bool	passed_FE_noise_min[16];
  bool	passed_FE_noise_max[16]; 
  bool	passed_FE_threshold_min[16];
  bool	passed_FE_threshold_max[16];
  bool	passed_FE_threshold_mean_min[16];
  bool	passed_FE_threshold_mean_max[16];
  bool	passed_FE_threshold_RMS_max[16];
  bool	passed_FE_cuts[16];

public:
	// ClassDef(ResultINTIMETHR, 1)	// not 0 -> activate I/O; not needed at the moemnt, put in if needed!
};

#endif
