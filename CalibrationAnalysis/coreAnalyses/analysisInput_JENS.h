#ifndef _analysisInputJENS_h_
#define _analysisInputJENS_h_

#include "analysisInput_Base.h"
#include <vector>
#include <TH2F.h>
#include <TString.h>
#include <iostream>

class AnalysisInputJENS : public AnalysisInput { //this is for one scan and one module
 public:
  AnalysisInputJENS() :AnalysisInput(), MBs(-9) {};
  ~AnalysisInputJENS() {};

  std::vector<const TH2*> histo_Link1; //several histos for scan over Viset 
  std::vector<const TH2*> histo_Link2; //several histos for scan over Viset
  std::vector<histoAxis_t> axis; //the axis of the histo (the same for all histos) of type histoAxis_t 

  Int_t MBs; //40,80,160 Mbits/s
  Int_t thr_Link1, thr_Link2, del_Link1, del_Link2;
  Int_t nPixel;  //depending on mask steps


  //Helper functions
  void PrintInfo() {
    std::cout << "AnalysisInputJENS for pp0 " << pp0Name.Data() << ", module: " << moduleName.Data() << "  MBit/s=" << MBs << "  HistoEntries:" << std::endl;
    
    std::cout << "vector Link1 " << histo_Link1.size() << std::endl;
    std::cout << "vector Link2 " << histo_Link2.size() << std::endl;
    std::cout << "vector axis " <<  axis.size() << std::endl;

    std::vector<const TH2*>::const_iterator iter = histo_Link1.begin();
    for(;iter!=histo_Link1.end(); iter++) {
      //std::cout << "histos link1 " << iter->GetEntries() << std::endl;
    }

    for(unsigned int i=0; i<axis.size(); i++)
      std::cout << "axis["<<i<<"].nSteps " <<  axis[i].nSteps << std::endl;

  };

  //ClassDef(AnalysisInputJENS,1) 
};

#endif
