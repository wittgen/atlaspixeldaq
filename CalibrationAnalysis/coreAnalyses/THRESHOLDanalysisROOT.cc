#ifndef _THRESHOLDanalysisROOT_CC_
#define _THRESHOLDanalysisROOT_CC_

#include "THRESHOLDanalysisROOT.h"
#include <PixDbGlobalMutex.hh>
#include <Lock.hh>
#include <iostream>
#include "TMath.h"
#include "PixFe/PixGeometry.h" //********[maria elena] added 
#include "DataContainer/GenericHistogramAccess.h"  //********[maria elena] added 

THRESHOLDanalysisROOT::THRESHOLDanalysisROOT()
{
  std::cout << "THRESHOLDanalysis:  constructed." << std::endl;
} 


THRESHOLDanalysisROOT::~THRESHOLDanalysisROOT()
{
  //WriteResults();
}

void THRESHOLDanalysisROOT::WriteResults()
{
  TFile f("resultsTHRESHOLD.root","update");
  f.WriteTObject(m_result.get(),"scanHistos","SingleKey"); //as one array
  f.Close();  
}

void THRESHOLDanalysisROOT::Initialize()
{
  //! Don't hard code here. Take values from analysis configuration, defaults in AutomaticAnalysis.

  if(cut_noise_max[0]==0 && cut_thr_min==0  && cut_thr_max==0 && cut_thr_RMS_max==0) {
    std::cout << "WARNING   Apparent problem in reading analysis config, all cuts are zero.  -> Setting default values." << std::endl;

    cut_noise_max[0]=400; //on the sigma of the threshold distribution 
    cut_noise_max[1]=420;
    cut_noise_max[2]=450;
    cut_noise_max_percent[0]=90; //90%
    cut_noise_max_percent[1]=90; //90%
    cut_noise_max_percent[2]=90; //90%
    cut_thr_min=3000;
    cut_thr_min_percent=95;
    cut_thr_max=5500; //was 5000
    cut_thr_max_percent=95;
    cut_thr_RMS_max=400; //200 //on the sigma of the means distribution
    cut_thr_mean_min=3200; 
    cut_thr_mean_max=5300;
    range_thr_mean_min=1000;
    range_thr_mean_max=6000;
    cut_thresholdOverNoise_min=10;
    cut_num_bad_pixels_per_FE=200;
      
  }
}  

void THRESHOLDanalysisROOT::Calculate() 
{
  std::cout << "**************************************" << std::endl;
  std::cout << "THRESHOLDanalysis::Calculate  starting" << std::endl;
  m_scan->R__FOR_EACH(AnalysisInputTHRESHOLD,PrintInfo)(); 
  std::cout << "THRESHOLDanalysis: These were the module names." << std::endl;  

  // Loop over modules
  for(Int_t i=0; i<m_scan->GetEntries(); i++) { 
    ResultTHRESHOLD* module_result = new ResultTHRESHOLD();
    module_result->pp0Name = ((AnalysisInputTHRESHOLD*)m_scan->At(i))->pp0Name;
    module_result->moduleName = ((AnalysisInputTHRESHOLD*)m_scan->At(i))->moduleName;
    module_result->MakePixelMap = create_Pixelmap; 

    if((   !((AnalysisInputTHRESHOLD*)m_scan->At(i))->histo_chi2
       ||   !((AnalysisInputTHRESHOLD*)m_scan->At(i))->histo_mean
       ||   !((AnalysisInputTHRESHOLD*)m_scan->At(i))->histo_sigma)
       || 
          (((AnalysisInputTHRESHOLD*)m_scan->At(i))->histo_chi2->GetEntries()==0 
       || ((AnalysisInputTHRESHOLD*)m_scan->At(i))->histo_mean->GetEntries()==0
       || ((AnalysisInputTHRESHOLD*)m_scan->At(i))->histo_sigma->GetEntries()==0)) {
      std::cout << "THRESHOLDanalysis: WARNING  no histos for module " << module_result->moduleName << " -> skipping." << std::endl;
      module_result->passed=false;
      m_result->Add( module_result );
      continue;
    }
    
    //*******************************
    PixGeometry geom(PixA::nRows(((AnalysisInputTHRESHOLD*)m_scan->At(i))->histo_mean),PixA::nColumns(((AnalysisInputTHRESHOLD*)m_scan->At(i))->histo_mean));
    int chipType = geom.pixType();
      
   //int Nrow = helper.getNrow(chipType);
   //int Ncol = helper.getNcol(chipType);
   int Nfe=helper.getNfrontEnd(chipType);
   //int Ncp=helper.getNcp(chipType); // # of the enable column pairs
    //***********************************************

      //get FE disable info
      FE_disable = ((AnalysisInputTHRESHOLD*)m_scan->At(i))->fe_disable;
      CP_enable = ((AnalysisInputTHRESHOLD*)m_scan->At(i))->cp_enable;
      
    std::cout << "Analysing module: " << module_result->moduleName << std::endl; 
    // all 'passed' result variables are initialized as 'false' in analysisOutput_THRESHOLD.h

    // noise
    if(!((AnalysisInputTHRESHOLD*)m_scan->At(i))->histo_sigma || ((AnalysisInputTHRESHOLD*)m_scan->At(i))->histo_sigma->GetEntries()==0)
      std::cout << "No noise histogram." << std::endl; 
    else {
      std::cout << "-- Calculating noise values over module --" << std::endl; 

      // Loop over classes of pixel types
      for (int pixtype=0; pixtype<helper.getNpixelClasses(chipType); pixtype++) {
          
//**************************[maria elena] modified getPixelClassString function
	std::cout << "Now looking at pixel class: " << helper.getPixelClassString(pixtype+1,chipType,helper.getNpixelClasses(chipType)) << std::endl; //intimeAnalysis = false; default.
          	
    std::cout << "INFO [THRESHOLDanalysis::Calculate] histo_sigma = " << static_cast<const void *>(((AnalysisInputTHRESHOLD*)m_scan->At(i))->histo_sigma) << std::endl;
	std::shared_ptr<TH1F> histo_sigma = helper.IntegrateHisto( ((AnalysisInputTHRESHOLD*)m_scan->At(i))->histo_sigma, 1,  2000   ,pixtype+1,-1,0,chipType,((AnalysisInputTHRESHOLD*)m_scan->At(i))->nMaskStages,((AnalysisInputTHRESHOLD*)m_scan->At(i))->nTotMaskStages,CP_enable,FE_disable);
	module_result->average_noise[pixtype] = histo_sigma->GetMean();
	module_result->RMS_noise[pixtype] = histo_sigma->GetRMS();
      } //end loop over pixel types
    }

    // threshold
    if(!((AnalysisInputTHRESHOLD*)m_scan->At(i))->histo_mean || ((AnalysisInputTHRESHOLD*)m_scan->At(i))->histo_mean->GetEntries()==0) 
      std::cout << "No threshold histogram." << std::endl; 
    else {
      std::cout << "-- Calculating noise values over module --" << std::endl; 

      std::shared_ptr<TH1F> histo_mean = helper.IntegrateHisto( ((AnalysisInputTHRESHOLD*)m_scan->At(i))->histo_mean, 1, 10000,-1,-1,0,chipType,((AnalysisInputTHRESHOLD*)m_scan->At(i))->nMaskStages,((AnalysisInputTHRESHOLD*)m_scan->At(i))->nTotMaskStages,CP_enable,FE_disable);
      std::shared_ptr<TH1F> histo_mean_restricted  = helper.IntegrateHisto( ((AnalysisInputTHRESHOLD*)m_scan->At(i))->histo_mean, range_thr_mean_min, range_thr_mean_max,-1,-1,0,chipType,((AnalysisInputTHRESHOLD*)m_scan->At(i))->nMaskStages,((AnalysisInputTHRESHOLD*)m_scan->At(i))->nTotMaskStages,CP_enable,FE_disable);
      std::cout << "cutting on threshold_RMS: " << std::endl;
      if( helper.FulfillMaxCut( histo_mean_restricted->GetRMS(), cut_thr_RMS_max) )  module_result->passed_module_threshold_RMS=true; 

      module_result->average_threshold = histo_mean_restricted->GetMean();
      module_result->RMS_threshold = histo_mean_restricted->GetRMS();
    }

    // threshold/noise
    if(!((AnalysisInputTHRESHOLD*)m_scan->At(i))->histo_sigma 
       || !((AnalysisInputTHRESHOLD*)m_scan->At(i))->histo_mean
       || ((AnalysisInputTHRESHOLD*)m_scan->At(i))->histo_sigma->GetEntries()==0
       || ((AnalysisInputTHRESHOLD*)m_scan->At(i))->histo_mean->GetEntries()==0) {
      std::cout << "Skipping thresholdOverNoise calculation due to missing histogram!!" << std::endl; 
    }
    else {
      std::shared_ptr<TH2> histo_thresholdOverNoise;
      {
	CAN::Lock lock( CAN::PixDbGlobalMutex::mutex() );
	histo_thresholdOverNoise =std::shared_ptr<TH2> ( (TH2*)((AnalysisInputTHRESHOLD*)m_scan->At(i))->histo_mean->Clone() );
      }
      histo_thresholdOverNoise.get()->Divide(((AnalysisInputTHRESHOLD*)m_scan->At(i))->histo_sigma);
      module_result->average_thresholdOverNoise = histo_thresholdOverNoise.get()->GetMean();
    }

      
      //calculate bad pixel maps and number of bad pixels
    findBadPixels(module_result, i, FE_disable, CP_enable);
      
    //loop over FE
    for(int FE=0; FE<Nfe; FE++) {
      	if(FE_disable.size() <= 0) continue;
      //check for disabled FE
      if ( FE_disable[FE] == 1){
	std::cout << "THRESHOLDanalysis : FE" << FE <<" on module " << module_result->moduleName << " is disabled!" << std::endl;
	module_result->passed_FE_noise_max[FE]=true; //set disabled FEs to pass
	module_result->passed_FE_threshold_min[FE]=true;
	module_result->passed_FE_threshold_max[FE]=true; 
	module_result->passed_FE_threshold_RMS_max[FE]=true;
	module_result->passed_FE_threshold_mean_min[FE]=true; 
	module_result->passed_FE_threshold_mean_max[FE]=true;
	
	for (int pixtype=0; pixtype<helper.getNpixelClasses(chipType); pixtype++){ //set all values to 0.
	  module_result->average_noise_FE[FE][pixtype]=0;
	  module_result->average_threshold_FE[FE][pixtype]=0; 
	  module_result->RMS_threshold_FE[FE][pixtype]=0; 
	}
	continue; //skip to next FE
      }
        
      //FE noise
      if(!((AnalysisInputTHRESHOLD*)m_scan->At(i))->histo_sigma || ((AnalysisInputTHRESHOLD*)m_scan->At(i))->histo_sigma->GetEntries()==0)
	std::cout << "No noise histogram." << std::endl; 	
      else {
	std::cout << "-- cutting on FE noise --" << std::endl;
	module_result->passed_FE_noise_max[FE]=true;

	//loop over pixel types
	for (int pixtype=0; pixtype<helper.getNpixelClasses(chipType); pixtype++) {
	  std::shared_ptr<TH1F> histo_fe_sigma = helper.IntegrateHisto( ((AnalysisInputTHRESHOLD*)m_scan->At(i))->histo_sigma, 1,  2000, pixtype+1, FE,0,chipType, ((AnalysisInputTHRESHOLD*)m_scan->At(i))->nMaskStages,((AnalysisInputTHRESHOLD*)m_scan->At(i))->nTotMaskStages,CP_enable,FE_disable);
	  
	  // if no entries in restricted range, then fe fails
	  if ( !histo_fe_sigma || histo_fe_sigma->GetEntries()==0 )
	    module_result->passed_FE_noise_max[FE]=false;
	  else{
	    if( !helper.FulfillMaxCut( histo_fe_sigma.get(), cut_noise_max[pixtype], cut_noise_max_percent[pixtype]/Float_t(100)) )
	      module_result->passed_FE_noise_max[FE]=false; 	    
	    module_result->average_noise_FE[FE][pixtype] = histo_fe_sigma->GetMean();  
	  }
	}
      }

      //FE threshold
      if(!((AnalysisInputTHRESHOLD*)m_scan->At(i))->histo_mean || ((AnalysisInputTHRESHOLD*)m_scan->At(i))->histo_mean->GetEntries()==0) 
	std::cout << "No threshold histo" << std::endl;
      else {
	std::cout << "-- cutting on FE threshold --" << std::endl; 	
	std::shared_ptr<TH1F> histo_fe_mean = helper.IntegrateHisto(((AnalysisInputTHRESHOLD*)m_scan->At(i))->histo_mean, 1, 10000, -1, FE,0,chipType, ((AnalysisInputTHRESHOLD*)m_scan->At(i))->nMaskStages,((AnalysisInputTHRESHOLD*)m_scan->At(i))->nTotMaskStages,CP_enable,FE_disable);
	std::shared_ptr<TH1F> histo_fe_mean_restricted1 = helper.IntegrateHisto(((AnalysisInputTHRESHOLD*)m_scan->At(i))->histo_mean, range_thr_mean_min, range_thr_mean_max, -1, FE,0,chipType, ((AnalysisInputTHRESHOLD*)m_scan->At(i))->nMaskStages,((AnalysisInputTHRESHOLD*)m_scan->At(i))->nTotMaskStages,CP_enable,FE_disable);
	
	// if no entries in restricted range, then fe fails
	if ( !histo_fe_mean_restricted1 || histo_fe_mean_restricted1->GetEntries()==0){
	  module_result->passed_FE_threshold_min[FE]=false;
	  module_result->passed_FE_threshold_max[FE]=false; 
	  module_result->passed_FE_threshold_RMS_max[FE]=false;
	  module_result->passed_FE_threshold_mean_min[FE]=false;
	  module_result->passed_FE_threshold_mean_max[FE]=false;
	}
	else {
	  std::cout << "cutting on threshold_min for FE" << FE << std::endl;
	  if( helper.FulfillMinCut( histo_fe_mean.get(), cut_thr_min, cut_thr_min_percent/Float_t(100)) )   module_result->passed_FE_threshold_min[FE]=true; 
	  
	  std::cout << "cutting on threshold_max for FE"<< FE << std::endl;
	  if( helper.FulfillMaxCut( histo_fe_mean.get(), cut_thr_max, cut_thr_max_percent/Float_t(100)) )   module_result->passed_FE_threshold_max[FE]=true; 
	  
	  std::cout << "cutting on threshold RMS for FE" << FE << std::endl;
	  if ( helper.FulfillMaxCut( histo_fe_mean_restricted1->GetRMS(), cut_thr_RMS_max) )   module_result->passed_FE_threshold_RMS_max[FE]=true;
	  
	  std::cout << "cutting on threshold-mean_min for FE"<< FE << std::endl;
	  if( helper.FulfillMinCut( histo_fe_mean_restricted1->GetMean(), cut_thr_mean_min) )   module_result->passed_FE_threshold_mean_min[FE]=true; 
	  
	  std::cout << "cutting on threshold-mean_max for FE"<< FE << std::endl;
	  if( helper.FulfillMaxCut( histo_fe_mean_restricted1->GetMean(), cut_thr_mean_max) )   module_result->passed_FE_threshold_mean_max[FE]=true;
	         	  
	  //loop over pixel classes
	  for (int pixtype=0; pixtype<helper.getNpixelClasses(chipType); pixtype++) {
	    std::shared_ptr<TH1F> histo_fe_mean_restricted2 = helper.IntegrateHisto(((AnalysisInputTHRESHOLD*)m_scan->At(i))->histo_mean, range_thr_mean_min, range_thr_mean_max, pixtype+1, FE,0,chipType, ((AnalysisInputTHRESHOLD*)m_scan->At(i))->nMaskStages,((AnalysisInputTHRESHOLD*)m_scan->At(i))->nTotMaskStages,CP_enable,FE_disable);
	    module_result->average_threshold_FE[FE][pixtype] = histo_fe_mean_restricted2->GetMean();
	    module_result->RMS_threshold_FE[FE][pixtype] = histo_fe_mean_restricted2->GetRMS();
	  }//end loop over pixel classes
	}
      }      
    }//end loop over FE   
    

    //final FE pass/fail status
    module_result->passed_all_fe_bad_pixel=true;
    module_result->passed_all_fe_noise=true;
    module_result->passed_all_fe_threshold=true;
    module_result->passed_all_fe_threshold_RMS=true;
    //loop over fes
    for(int FE=0; FE<Nfe; FE++) {
      module_result->passed_FE_cuts[FE]=true;
      //fe noise
      if ( !module_result->passed_FE_noise_max[FE] ){
	module_result->passed_all_fe_noise=false;
	module_result->passed_FE_cuts[FE]=false;
      }
      //fe threshold
      if ( !module_result->passed_FE_threshold_min[FE] ||
	   !module_result->passed_FE_threshold_max[FE] ||
	   !module_result->passed_FE_threshold_mean_min[FE] ||
	   !module_result->passed_FE_threshold_mean_max[FE] ){
	module_result->passed_all_fe_threshold=false;
	module_result->passed_FE_cuts[FE]=false;
      }
      //fe threshold rms
      if ( !module_result->passed_FE_threshold_RMS_max[FE] ){
	module_result->passed_all_fe_threshold_RMS=false;
      	module_result->passed_FE_cuts[FE]=false;
      }
      //fe number of bad pixels
      if ( num_bad_pixels[FE] > cut_num_bad_pixels_per_FE ){
	module_result->passed_all_fe_bad_pixel=false;
	module_result->passed_FE_cuts[FE]=false;
      }
      // "good" FE is enabled and passes all cuts
      if ( FE_disable[FE]==0 && module_result->passed_FE_cuts[FE]==true) module_result->numGoodFEs++;
    }

    // final module pass/fail status
    module_result->passed = ( module_result->passed_all_fe_bad_pixel &&
			      module_result->passed_module_threshold_RMS &&
			      module_result->passed_all_fe_threshold &&
			      module_result->passed_all_fe_threshold_RMS &&
			      module_result->passed_all_fe_noise );

    std::cout << "> Result for module " << module_result->moduleName << " is: " << ( (module_result->passed)?  "OK":"FAILED" ) << std::endl;
    m_result->Add( module_result );
  }//end loop over modules

  std::cout << "  after calculate  results size: " << m_result->GetEntries() << std::endl;
  std::cout << "THRESHOLDanalysis: analysis finished." << std::endl;
}


//Not being used!
void THRESHOLDanalysisROOT::AdditionalCalculations() 
{
  //
}

void THRESHOLDanalysisROOT::findBadPixels(ResultTHRESHOLD* module, int ientry, std::vector<unsigned short int> fe_disable, std::vector<std::vector<unsigned short int> > cp_enable)
{
    
//*******************************
PixGeometry geom(PixA::nRows(((AnalysisInputTHRESHOLD*)m_scan->At(ientry))->histo_mean),PixA::nColumns(((AnalysisInputTHRESHOLD*)m_scan->At(ientry))->histo_mean));
int chipType = geom.pixType();
int Nrow = helper.getNrow(chipType);
int Ncol = helper.getNcol(chipType);
int Nfe=helper.getNfrontEnd(chipType);
//***********************************************

  const TH2* threshold = ((AnalysisInputTHRESHOLD*)m_scan->At(ientry))->histo_mean;
  const TH2* noise = ((AnalysisInputTHRESHOLD*)m_scan->At(ientry))->histo_sigma;
  const TH2* chi2 = ((AnalysisInputTHRESHOLD*)m_scan->At(ientry))->histo_chi2;

  std::cout << "THRESHOLDanalysisROOT::doPixelMaps is starting" << std::endl;
  if (threshold == NULL && noise == NULL && chi2 == NULL) {
    std::cout << "No histograms, will not create any pixel map" << std::endl;
    return;
  }
  //initialize number of bad pixels on module and FEs to 0
  for (int fe=0; fe<Nfe; fe++) num_bad_pixels[fe]=0;
  module->numBadPixels=0;
  for(int col=1; col<=Ncol; col++) {
    for(int row=1; row<=Nrow; row++) {
      const std::pair<int,int> index = std::make_pair(col-1,row-1);    
//************      if( (threshold->GetBinContent(col,row)==0 && noise->GetBinContent(col,row)==0 && chi2->GetBinContent(col,row)==0)|| threshold->GetBinContent(col,row) < 0 ) {[maria elena]
        if( (threshold->GetBinContent(col,row)==0 && noise->GetBinContent(col,row)==0)|| threshold->GetBinContent(col,row) < 0 ) {
//*************************
	helper.SetBit(module->badPixelMap[index], 13); // 13 = "threshold not fitted"
	
	if (cp_enable[helper.frontEnd(col-1,row-1,chipType)][helper.columnPair(col-1,row-1,chipType)] && !fe_disable[helper.frontEnd(col-1,row-1,chipType)] && helper.isMasked(col-1, row-1,((AnalysisInputTHRESHOLD*)m_scan->At(ientry))->nMaskStages,((AnalysisInputTHRESHOLD*)m_scan->At(ientry))->nTotMaskStages,chipType)) { //only count bad pixels on enabled FEs and enabled masks
	  module->numBadPixels++; 
	  num_bad_pixels[helper.frontEnd(col-1,row-1,chipType)]++;
	}
      }   
      else if(noise->GetBinContent(col,row)!=0 && threshold->GetBinContent(col,row)/noise->GetBinContent(col,row) < cut_thresholdOverNoise_min)
	  helper.SetBit(module->badPixelMap[index], 18); // 18 = "high noise/threshold"
    }
  }
    //***********************
    std::cout<<"number of bad pixels is: "<< module->numBadPixels <<std::endl;
    //*************************************
      }


void THRESHOLDanalysisROOT::QualifyResults() 
{
  bool allPassed(true);
  for(Int_t i=0; i<m_result->GetEntries(); i++) { 
    ResultTHRESHOLD* module_result = (ResultTHRESHOLD*)m_result->At(i); 
    if(!module_result->passed) { 
      allPassed=false;
      break;
    }
  }  
  if(allPassed)
    m_status=kPassed;
  else 
    m_status=kFailed;

  std::cout << "THRESHOLDanalysis::QualifyResults  Total result is :" << ( (allPassed)? "OK":"FAILED" ) << std::endl;
  std::cout << "*************************************" << std::endl;
}


void THRESHOLDanalysisROOT::CheckForKnownProblems() //setting of failure bits
{
  for(Int_t i=0; i<m_result->GetEntries(); i++) { 
    ResultTHRESHOLD* module_result = (ResultTHRESHOLD*)m_result->At(i); 
    ///
    CheckForZeroMean(module_result);
  }
}


void THRESHOLDanalysisROOT::CheckForZeroMean(ResultTHRESHOLD* module_result) //setting of failure bits, average_threshold has to be calculated in Calculate() before!
{
  if(module_result->average_threshold==0) {
    helper.SetBit(module_result->tests,1); //test 1 failed
    module_result->passed=false;
  }
}

#endif

