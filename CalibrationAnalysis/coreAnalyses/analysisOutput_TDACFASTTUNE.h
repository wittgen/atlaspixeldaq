#ifndef _analysisOutputTDACFASTTUNE_h_
#define _analysisOutputTDACFASTTUNE_h_

#include "analysisOutput_Base.h"

class ResultTDACFASTTUNE : public AnalysisResult { //this is for one scan and one module
 public:
  ResultTDACFASTTUNE(){
      
  };
  ~ResultTDACFASTTUNE() {};

    float MeanPercentage; 
    float RMSPercentage;
    float Chi2;
    float MeanTDAC;
    bool passedMean;
    bool passedRMS;
    bool passedChi2;
    bool passedTDACs;
    std::vector<float> vec_extraHits; 


 public:
  //ClassDef(ResultTDACFASTTUNE,1) //not 0 -> activate I/O  ; not needed at the moemnt, put in if needed!
};

#endif
