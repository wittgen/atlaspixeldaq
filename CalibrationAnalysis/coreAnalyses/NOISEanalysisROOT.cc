#include "NOISEanalysisROOT.h"
#include <iostream>
#include <fstream>
#include "TFile.h"
#include "TCanvas.h"
#include "TH2.h"
#include "DataContainer/HistoUtil.h"
#include "PixFe/PixGeometry.h"
#include "DataContainer/GenericHistogramAccess.h"
using namespace PixLib;




NOISEanalysisROOT::NOISEanalysisROOT(): _Nmasks(0), _Nevents(0), _Ncol(0), _Nrow(0), _ChipType(PixGeometry::INVALID_MODULE)
{
  std::cout << std::endl;
  std::cout << "-------------------------------" << std::endl;
  std::cout << "   NOISE Analysis Algorithm   " << std::endl;
  std::cout << "-------------------------------" << std::endl;
  std::cout << std::endl;

  // this analysis can handle analog scans

}



NOISEanalysisROOT::~NOISEanalysisROOT()
{
  //
}






void NOISEanalysisROOT::Initialize()
{
  if(!m_scan) {
    std::cout << std::endl;
    std::cout << "NOISEanalysisROOT::Initialize ERROR: input data pointer is null" << std::endl;
    std::cout << std::endl;
    return;
  }

  AnalysisInputNOISE* occupancy = (AnalysisInputNOISE*) m_scan->At(0);
  if(!occupancy || !occupancy->histo) return; 
  _Nmasks  = occupancy->Nmasks;
  _Nevents = occupancy->Nevents;
_Nmasksdone = occupancy->Nmasksdone;

  PixGeometry geo(PixA::nRows(occupancy->histo),PixA::nColumns(occupancy->histo)); 
  _ChipType = geo.pixType();

  _Nrow = helper.getNrow(_ChipType);//occupancy->histo->GetNbinsY();
  _Ncol = helper.getNcol(_ChipType);//occupancy->histo->GetNbinsX();


  // fill pattern
  //-----------------------
  int nPixels = 0;  

  std::cout << "Number of masks   = " << _Nmasks << std::endl;
  std::cout << "Number of masks performed = " << _Nmasksdone << std::endl;
  std::cout << "Number of Events  = " << _Nevents << std::endl;
  std::cout << "Number of modules = " << m_scan->GetEntries() << std::endl;
  std::cout << std::endl;
  std::cout << "Number of active pixels = " << nPixels << std::endl;
  std::cout << std::endl;
    
} 
  
  
 

void NOISEanalysisROOT::Calculate()
{
  //TFile f("test.root","recreate");

  std::cout<<"NOISEanalysisROOT::Calculate()::started!"<<std::endl;
    
  int Nfe=helper.getNfrontEnd(_ChipType); // # of FEs in one module
  //int Ncp=helper.getNcp(_ChipType); // # of the enable column pairs
  //int Npix=helper.getNpixel(_ChipType); // # of pixel in one FE

  // initializing, might not be necessary  
  Passed = false;

  std::cout << "OCCUPANCY entries: " << m_scan->GetEntries()  << std::endl;
  
    int all_num_modules=8;
    if(_ChipType == PixGeometry::FEI2_CHIP || _ChipType == PixGeometry::FEI2_MODULE){all_num_modules=7;}
    else if(_ChipType == PixGeometry::FEI4_CHIP || _ChipType == PixGeometry::FEI4_MODULE){all_num_modules=8;}
    
    if(m_scan->GetEntries() == 0) return;
    if(m_scan->GetEntries()>all_num_modules) {
    std::cout << "ERROR, pp0 with " << m_scan->GetEntries() << " modules" << std::endl;
    //return;
  }

  if(m_logLevel>=CAN::kDiagnostics1Log) std::cout << "Number of Modules = " << m_scan->GetEntries() << std::endl;

  for(int i = 0; i < m_scan->GetEntries(); i++) {
    AnalysisInputNOISE* occupancy = (AnalysisInputNOISE*) m_scan->At(i);
    ResultNOISE* module_result = new ResultNOISE();

    // if(m_logLevel>=CAN::kDiagnostics2Log) 
      std::cout << " Module #" << i+1 << std::endl;

      // get fe & cp disable information for each module
      _fe_disable = occupancy->fe_disable;
      _cp_enable = occupancy->cp_enable;
      
      // some variables 
      float PixNoisy = 0;

      
      if(!occupancy->histo || occupancy->histo->GetEntries()==0)
      {
	std::cout<<"NOISEanalysis: WARNING no histogram for module "<<occupancy->moduleName<<", skipping this one."<<std::endl;
	module_result->passed=false;
	m_result->Add(module_result);
	continue;
      }


      int expected=0;
      int observed=0;

    for(int fe = 0; fe < Nfe; fe++){
        if(_fe_disable[fe] == 1){continue;}
        for(int col = helper.getFrontEndStartCol(fe, _ChipType); col <= helper.getFrontEndStopCol(fe, _ChipType); col++){
            for(int row = helper.getFrontEndStartRow(fe, _ChipType); row <= helper.getFrontEndStopRow(fe, _ChipType); row++){
                
                int cp= helper.columnPair(col, row, _ChipType);
                if(!helper.isMasked(col,row,_Nmasksdone,_Nmasks,_ChipType) || _cp_enable[fe][cp] ==0){continue;}

              observed = (int) occupancy->histo->GetBinContent(col+1,row+1);

                if(m_logLevel>=CAN::kDiagnostics3Log && i==0) std::cout << col+1 << ", " << row+1 << "    " << expected << " - " << observed << "; " << std::endl;

                if(_Nevents!=0){
              if((double)(observed/_Nevents)>pow(10,-max_noise_allowed))
              {
                  PixNoisy++;
              }
                          
              if(m_logLevel>=CAN::kDiagnostics4Log) {
                  if((double)(observed/_Nevents)>pow(10,-max_noise_allowed))
                      std::cout <<  col+1 << ", " << row+1 << "    " << expected << " - " << observed << "; " << std::endl;
              }
                }
                
                else{
                    if(observed>0)
                    {
                        PixNoisy++;
                    }
                    
                    if(m_logLevel>=CAN::kDiagnostics4Log) {
                        if(observed>0)
                            std::cout <<  col+1 << ", " << row+1 << "    " << expected << " - " << observed << "; " << std::endl;
                    }
                    
                }
          }
      }
  }
    
      // if(m_logLevel>=CAN::kDiagnostics1Log) {
      std::cout << "   Number of noisy pixels = " << PixNoisy << std::endl;
      
      

    module_result->pp0Name = ((AnalysisInputNOISE*)m_scan->At(i))->pp0Name;
    module_result->moduleName = ((AnalysisInputNOISE*)m_scan->At(i))->moduleName;
    module_result->PixNoisy = PixNoisy;
    module_result->MakePixelMap = create_Pixelmap;
      

      // check for dead FE only if we have a high number of dead pixel

      if(PixNoisy<max_noisy_pixel) Passed=true;

    module_result->passed = Passed;
    std::cout<<"Result for module "<<module_result->moduleName<<" is: "<<((module_result->passed)? "OK":"FAILED")<<std::endl;
    m_result->Add(module_result);
  } // modules


  if(m_logLevel>=CAN::kDiagnostics1Log) this->Print();
   //f.Close();
        
} 
 


 
void NOISEanalysisROOT::QualifyResults()
{
  allModulesPassed = true;
  for(int i = 0; i<m_result->GetEntries(); i++)
    {
      ResultNOISE* module_result = (ResultNOISE*)m_result->At(i);
      if(!module_result->passed)
	{
	  allModulesPassed = false;
	  break;
	}
    }
  

  std::cout<<"NOISEanalysisROOT::QualifyResults():  "<<(allModulesPassed? "OK":"FAILED")<<std::endl;
  
  if(allModulesPassed) m_status= kPassed;
  else  m_status=kFailed;
  
}


void NOISEanalysisROOT::CheckForKnownProblems() //setting of failure bits
{

}


void NOISEanalysisROOT::Print()
{
  std::cout << std::endl;
  std::cout << "-------------------------------------" << std::endl;
  std::cout << "NOISE Scan Analysis Results: " << std::endl;
  std::cout << "-------------------------------------" << std::endl;
  std::cout << std::endl;

  std::cout << "-------------------------------------" << std::endl;
  std::cout << std::endl << std::endl;
}
