#ifndef _ANALOGanalysisROOT_h_
#define _ANALOGanalysisROOT_h_ 

#include "ROOTanalysisBase.h"
#include "analysisInput_ANALOG.h"
#include "analysisOutput_ANALOG.h"
#include "ROOTanalysisHelper.h"



class ANALOGanalysisROOT  : public ROOTanalysisBase
{
 public:
  ANALOGanalysisROOT();
  ~ANALOGanalysisROOT();

 private: //methods
  void Initialize();

  void Calculate(); //internal calculations

  void QualifyResults(); //apply cuts from analysis config

  void CheckForKnownProblems(); //setting of failure bits

 
 private: 
  void WriteResults();
  void AdditionalCalculations();
  void GetErrorMap(const TH2* histo, Int_t events, ResultANALOG*);

 public:
  Float_t cut_current_max;

 public:
  Float_t variance_modules;  
  TH1F hInt; //summing over all modules of the scan
  Int_t events;
};


#endif

