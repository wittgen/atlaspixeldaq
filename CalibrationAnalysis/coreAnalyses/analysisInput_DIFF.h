#ifndef _analysisInputDIFF_h_
#define _analysisInputDIFF_h_

#include "analysisInput_Base.h"
#include <map>


class AnalysisInputDIFF : public AnalysisInput { //this is for one scan and one module
 public:
  AnalysisInputDIFF() :AnalysisInput() {};
  ~AnalysisInputDIFF() {};
  std::map< std::string, std::pair<std::shared_ptr<TH1F>,std::shared_ptr<TH1F> > > histos_1d;
  std::map< std::string, std::pair<std::shared_ptr<TH2F>,std::shared_ptr<TH2F> > > histos_2d;

  Int_t nPixel, nMaskStages;  //depending on mask steps
  TString moduleName; 
  TString pp0Name;

  //Helper functions
  void PrintInfo() {
  };

};

#endif
