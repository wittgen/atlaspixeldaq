#ifndef _TotBothCapAnalysisROOT_h_
#define _TotBothCapAnalysisROOT_h_ 

#include "TGraphErrors.h"
#include "TF1.h"

#include "ROOTanalysisBase.h"
#include "analysisInput_TotBothCap.h"
#include "analysisOutput_TotBothCap.h"
#include "ROOTanalysisHelper.h"
#include <map>

class MeanSigmaDeriver;
class MeanSigmaWithoutOutliers;

class TotBothCapAnalysisROOT  : public ROOTanalysisBase
{
 public:
  
  TotBothCapAnalysisROOT();
  ~TotBothCapAnalysisROOT();
  
  
  // Algorithm parameters
  // --------------------

  float n_pixeltypes;
  
  // end algorithm paramaters

  bool allPassed;
    
  std::map<std::string,int> map_tot_fitfuncs;
  
  struct ModuleData
  {
    // data that goes per module
    AnalysisInputTotBothCap * module_input;
    ResultTotBothCap * module_result;
    std::string module_name;
    // maybe at some point there will be an uncertainty on the injected charge. so far it is just set to zero.
    std::vector<std::vector<float> > charge_clow;
    std::vector<std::vector<float> > charge_chigh_full;
    std::vector<std::vector<float> > charge_chigh_lim;
    std::vector<std::vector<int> > n_masked_pixels;
    std::vector<std::vector<int> > n_healthy_pixels;
    std::vector<std::vector<std::shared_ptr<TF1> > > tot_fit_result;
    std::vector<std::vector<std::shared_ptr<TF1> > > disp_fit_result; 
  };    
  
 public:
//   bool isHealthyPixel(std::vector<float> tot, std::vector<float> sigma);
//   bool isNoisyPixel(std::vector<float> tot, std::vector<float> sigma, std::vector<MeanSigmaDeriver> ms_tot, std::vector<MeanSigmaDeriver> ms_sigma);
  
 private: //methods
  void Initialize();

  void Calculate(); //internal calculations

  void QualifyResults(); //apply cuts from analysis config

  void CheckForKnownProblems(); //setting of failure bits

  void InitializeModuleResults(std::shared_ptr<TotBothCapAnalysisROOT::ModuleData> module_data);
  void FillDataVectors(std::shared_ptr<TotBothCapAnalysisROOT::ModuleData> module_data);
  std::shared_ptr<TF1> PerformTotFitOnAverage(int fe, int pixtype, /*int n_healthy_pixels,*/
						std::vector<std::vector<float> > charge,
						std::vector<std::vector<float> > mean_tot, std::vector<std::vector<float> > mean_tot_err);
  std::shared_ptr<TF1> PerformDispersionFit(int fe, int pixtype, int n_healthy_pixels, std::vector<float> mean_tot, std::vector<float> mean_tot_err, std::vector<float> disp, std::vector<float> disp_err);
 
  void SavePerPixelClosureResults(int fe, int pixtype, std::shared_ptr<TotBothCapAnalysisROOT::ModuleData> module_data, std::shared_ptr<TF1> fit, std::vector<float> v_charge, std::vector<float> v_tot, std::vector<float> v_tot_err);
  void SaveTOTClosureResults(int fe, int pixtype, std::shared_ptr<TotBothCapAnalysisROOT::ModuleData> module_data, std::vector<float> v_mean_tot, std::vector<float> v_mean_tot_err);
  void SaveDispClosureResults(int fe, int pixtype, std::shared_ptr<TotBothCapAnalysisROOT::ModuleData> module_data, std::vector<float> v_mean_tot, std::vector<float> v_mean_tot_err, std::vector<float> v_disp, std::vector<float> v_disp_err);
  void SaveResults(std::shared_ptr<TotBothCapAnalysisROOT::ModuleData> module_data);

  std::vector<float> removePoint(std::vector<float> v_full, int point);
  
  TF1 * createTotFitFunction(const char* name);
//   std::shared_ptr<TF1> createTotInvFitFunction(char* name);
//   std::shared_ptr<TF1> createDispFitFunction(char* name);
//   TF1* createTotFitFunctionPtr(char* name);
//   TF1* createDispFitFunctionPtr(char* name);

  static double TotFunc(double *x, double *par);

  int FitBetterAndReport(TF1 * fitFunc, TGraphErrors * g,
			 float p0 = 500.0, float p1 = -1000.0, float p2 = 150000.0);

private:

  ROOTanalysisHelper* helper;
  
};


#endif

