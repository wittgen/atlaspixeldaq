#ifndef _T0analysisROOT_CC_
#define _T0analysisROOT_CC_

#include "T0analysisROOT.h"
#include <iostream>
#include "TMath.h"
#include "TF1.h"


T0analysisROOT::T0analysisROOT(): _Ncol(0), _Nrow(0)
{
  std::cout << "T0analysis:  constructed." << std::endl;
} 


T0analysisROOT::~T0analysisROOT()
{
  //WriteResults();
}

void T0analysisROOT::WriteResults()
{
  TFile f("resultsT0.root","update");
  f.WriteTObject(m_result.get(),"scanHistos","SingleKey"); //as one array
  f.Close();  
}

void T0analysisROOT::Initialize()
{
  AnalysisInputT0* module_data_pointer = (AnalysisInputT0*)m_scan->At(0);
  
  _Nmasks  = module_data_pointer->Nmasks;
  _Nevents = module_data_pointer->Nevents;
  _Nmasksdone = module_data_pointer->Nmasksdone;
  _ChipFeFlav= module_data_pointer->_ChipFeFlav;

  _Nrow = helper.getNrow(_ChipFeFlav);//occupancy->histo->GetNbinsY();
  _Ncol = helper.getNcol(_ChipFeFlav);//occupancy->histo->GetNbinsX();

}

void T0analysisROOT::Calculate() 
{
  std::cout << "**************************************" << std::endl;
  std::cout << "T0analysis::Calculate  starting" << std::endl;

  if (m_scan->GetEntries() == 0) {
    std::cout << "No modules to run on. Exiting." << std::endl;
    return;
  }
 
  if (((AnalysisInputT0*)m_scan->At(0))->nDelRangeSteps <= 0) {
    std::cout << "Number of delay range steps is 0. Exiting..." << std::endl;
    return;
  }
  if (((AnalysisInputT0*)m_scan->At(0))->nMaskStages <= 0) {
    std::cout << "Number of maskstages is 0. Exiting..." << std::endl;
    return;
  }

  //int Nfe=helper.getNfrontEnd(_ChipFeFlav); // # of FEs in one module
  //int Ncp=helper.getNcp(_ChipFeFlav); // # of the enable column pairs
  //int Npix=helper.getNpixel(_ChipFeFlav); // # of pixel in one FE

  m_scan->R__FOR_EACH(AnalysisInputT0,PrintInfo)(); 

  // Loop over modules
  for(Int_t i=0; i<m_scan->GetEntries(); i++) {

    AnalysisInputT0* module_data_pointer = (AnalysisInputT0*)m_scan->At(i);

    _Nmasks=module_data_pointer->nMaskStages;
    _NTotmasks=module_data_pointer->NTotmasks;

    ResultT0* module_result = new ResultT0();
    module_result->pp0Name = module_data_pointer->pp0Name;
    module_result->moduleName = module_data_pointer->moduleName;
    std::cout << "Analysing module: " << module_result->moduleName << std::endl;

    if (module_data_pointer->histo.empty() ) {
      std::cout << "Totally missing histograms..." << std::endl;
      continue;
    }

    module_result->passed = false;
    std::array<int,2> best_diff ={min_del_diff,min_del_diff};

    //std::cout<<"NCol "<<(int)_Ncol<<" NRow "<<(int)_Nrow<<std::endl;

    int delRS = module_data_pointer->nDelRangeSteps;
    if(delRS <=0)delRS = 1;//In case there is only one delay step

    int maxChip=1;//Granularity is MCC for FEI3 and FE for FE-I4

    for (int dr=0; dr<delRS; dr++){ // loop over delay range steps
      std::array<double,2> max ={0,0};
      //Granularity is MCC for FEI3 (1 chip) and FE for FE-I4 (2 Chips)
      std::array <std::array<double,64>,2> occArray;

      if(_ChipFeFlav == PixGeometry::FEI4_MODULE)maxChip=2;
      //std::cout<<"FeFlav "<< _ChipFeFlav<<" maxChip "<<maxChip<<std::endl;
      for (int ds=0; ds<module_data_pointer->nDelSteps; ds++) { // loop over delay steps
        std::array<int,2> n_scanned ={0,0};
        std::array<double,2> occ ={0,0};
        for (int col=0; col<_Ncol; col++) {
	  for (int row=0; row<_Nrow; row++) {
	    int chipIndex=0;
	     if(_ChipFeFlav == PixGeometry::FEI4_MODULE)chipIndex=helper.frontEnd(col,row, _ChipFeFlav);
            if ( module_data_pointer->fe_disable[helper.frontEnd(col,row,_ChipFeFlav)] == 1) continue; //do following only for enabled FEs
            if (!module_data_pointer->cp_enable[helper.frontEnd(col,row,_ChipFeFlav)][helper.columnPair(col,row,_ChipFeFlav)] ) continue;
            if (!helper.isMasked(col,row,_Nmasks,_NTotmasks,_ChipFeFlav)) continue; //do following only for scanned pixels
              n_scanned[chipIndex]++;
              occ [chipIndex] += ((module_data_pointer->histo)[dr][ds])->GetBinContent(col+1,row+1);
          }
        }

        //std::cout<<n_scanned<<" "<<occ<<std::endl;
          for (int chipIndex =0;chipIndex<maxChip;chipIndex++){
            if (n_scanned [chipIndex] == 0) continue;
            occ[chipIndex] /= (double)n_scanned[chipIndex];
            if (occ[chipIndex] > max[chipIndex])max[chipIndex] = occ[chipIndex];
            occArray[chipIndex][ds] = occ[chipIndex];
            //std::cout<<ds<<" "<<occ[chipIndex]<<std::endl;
          }
      }

      for (int chipIndex =0;chipIndex<maxChip;chipIndex++){

        //std::cout<<chipIndex<<" MaxOcc "<<max[chipIndex]<<" limit "<<(double)_Nevents*pixel_occ/100.<<" "<<module_data_pointer->delStep_array.size()<<std::endl;

        if(max[chipIndex] < (double)_Nevents*pixel_occ/100.) continue;//Skip if maximum is below the threshold
        if(occArray[chipIndex][0] > (max[chipIndex]*rising_edge_occ/100.) ) continue;//Skip if first point is above rising edge threshold

        int risingEdge =-1;
        int fallingEdge=-1;
        int delayStart=-1;

          for (int s=0;s<module_data_pointer->nDelSteps;s++ ){

            if(occArray[chipIndex][s]>=(max[chipIndex]*rising_edge_occ/100.) && risingEdge==-1 )risingEdge = module_data_pointer->delStep_array.at(s);//Rising edge detection
            if(risingEdge>-1 && occArray [chipIndex][s]<= (max[chipIndex]*falling_edge_occ/100.) && fallingEdge ==-1 )fallingEdge= module_data_pointer->delStep_array.at(s);//falling edge detection
            if(occArray[chipIndex][s]>=(max[chipIndex]*T0_occ/100.) && delayStart ==-1)delayStart = module_data_pointer->delStep_array.at(s);//Occupancy start detection
          }

        int diff = fallingEdge-risingEdge;

        //std::cout<<chipIndex<<" "<<"Rising edge "<<risingEdge<<" falling edge "<<fallingEdge<<" T0 "<<delayStart<<" diff "<<diff<<std::endl;

          if(diff>best_diff[chipIndex]){//check best diff (fallingEdge -risingEdge)) for delay range
            best_diff[chipIndex] = diff;
            module_result->best_delay_range[chipIndex] = module_data_pointer->delRange_array.at(dr);
            module_result->conv[chipIndex] = 24.95/(double)diff;//ns aasumes 1BC = 24.95 ns
            module_result->rising_edge[chipIndex] = risingEdge;
            module_result->falling_edge[chipIndex] = fallingEdge;
            module_result->T0[chipIndex] = delayStart + (int) (delay_offset/module_result->conv[chipIndex]);
            module_result->passedFE[chipIndex] = true;
          }
        }
    }

    for (int chipIndex =0;chipIndex<maxChip;chipIndex++){
      std::cout<<module_result->moduleName<<" "<<chipIndex<<" Best Range "<<module_result->best_delay_range[chipIndex]<<" Rising edge "<<module_result->rising_edge[chipIndex]<<" falling edge "<<module_result->falling_edge[chipIndex]<<" T0 "<<module_result->T0[chipIndex]<<std::endl;
      if(chipIndex==0)module_result->passed = module_result->passedFE[chipIndex];
      else module_result->passed &= module_result->passedFE[chipIndex];
    }

    std::cout << "Result for module " << module_result->moduleName << " is: " << ( (module_result->passed)?  "OK":"FAILED" ) << std::endl;
    //module_result->passed = false;
    m_result->Add( module_result );
  }

  std::cout << "  after calculate  results size: " << m_result->GetEntries() << std::endl;

}

void T0analysisROOT::QualifyResults() 
{
  std::cout<<"T0analysisROOT::QualifyResults"<<std::endl;
  bool allPassed =  true;
  for(Int_t i=0; i<m_result->GetEntries(); i++) {
    ResultT0* module_result = (ResultT0*)m_result->At(i); 
    if(!module_result->passed) {
      allPassed=false;
      break;
    }
  }
  if(allPassed)
    m_status=kPassed;
  else 
    m_status=kFailed;

  std::cout << "T0analysis::QualifyResults  Total result is :" << ( (allPassed)? "OK":"FAILED" ) << std::endl;
  std::cout << "*************************************" << std::endl;
}

#endif

