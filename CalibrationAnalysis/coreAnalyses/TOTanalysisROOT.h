#ifndef _TOTanalysisROOT_h_
#define _TOTanalysisROOT_h_ 

#include "ROOTanalysisBase.h"
#include "analysisInput_TOT.h"
#include "analysisOutput_TOT.h"
#include "ROOTanalysisHelper.h"
#include <map>

class MeanSigmaDeriver;
class MeanSigmaWithoutOutliers;

class TOTanalysisROOT  : public ROOTanalysisBase
{
 public:
  
 TOTanalysisROOT();
  ~TOTanalysisROOT();
  
  
  // Algorithm parameters
  // --------------------
  
  float min_tot_fit_closure;
  float max_tot_fit_closure;
  float max_frac_bad_tot_fit_closure;
  float min_corr;
  float min_disp_fit_closure;
  float max_disp_fit_closure;
  float max_frac_bad_disp_fit_closure;
  float max_frac_bad_pixels;
  float max_n_bad_pixels;

  float min_tot_mip;
  float max_tot_mip;
  float min_disp_mip;
  float max_disp_mip;
    
  float pred_mip;

  float n_pixeltypes;
  bool fit_each_pixel;
  bool dump_data;
  bool debug;
  bool fit_disp_vs_charge;
  std::string tot_fitfunc;

  int npars_tot_fit_max;
  int npars_disp_fit_max;

  int npars_tot_fit;
  int npars_disp_fit;
  
  // end algorithm paramaters

  bool allPassed;
    
  std::map<std::string,int> map_tot_fitfuncs;
  
  struct ModuleData {
    // data that goes per module
    AnalysisInputTOT* module_input;
    ResultTOT* module_result;
    std::string module_name;
    // maybe at some point there will be an uncertainty on the injected charge. so far it is just set to zero.
    std::vector<std::vector<float> > charge;
    std::vector<std::vector<float> > charge_err;
    std::vector<std::vector<int> > n_masked_pixels;
    std::vector<std::vector<int> > n_healthy_pixels;
    std::vector<std::vector<std::shared_ptr<TF1> > > tot_fit_result;
    std::vector<std::vector<std::shared_ptr<TF1> > > disp_fit_result; 
 };    
  
 public:
  bool isHealthyPixel(std::vector<float> tot, std::vector<float> sigma);
  bool isNoisyPixel(std::vector<float> tot, std::vector<float> sigma, std::vector<MeanSigmaDeriver> ms_tot, std::vector<MeanSigmaDeriver> ms_sigma);
  
 private: //methods
  void Initialize();

  void Calculate(); //internal calculations

  void QualifyResults(); //apply cuts from analysis config

  void CheckForKnownProblems(); //setting of failure bits

  void InitializeModuleResults(std::shared_ptr<TOTanalysisROOT::ModuleData> module_data);
  void FillDataVectors(std::shared_ptr<TOTanalysisROOT::ModuleData> module_data);
  std::shared_ptr<TF1> PerformTotFitPerPixel(int col, int row, std::vector<float> charge, std::vector<float> charge_err, std::vector<float> tot, std::vector<float> tot_err);
  std::shared_ptr<TF1> PerformTotFitOnAverage(int fe, int pixtype, int n_healthy_pixels, std::vector<float> charge, std::vector<float> charge_err, std::vector<float> mean_tot, std::vector<float> mean_tot_err);
  std::shared_ptr<TF1> PerformDispersionFit(int fe, int pixtype, int n_healthy_pixels, std::vector<float> mean_tot, std::vector<float> mean_tot_err, std::vector<float> disp, std::vector<float> disp_err);
 
  void SavePerPixelClosureResults(int fe, int pixtype, std::shared_ptr<TOTanalysisROOT::ModuleData> module_data, std::shared_ptr<TF1> fit, std::vector<float> v_charge, std::vector<float> v_tot, std::vector<float> v_tot_err);
  void SaveTOTClosureResults(int fe, int pixtype, std::shared_ptr<TOTanalysisROOT::ModuleData> module_data, std::vector<float> v_mean_tot, std::vector<float> v_mean_tot_err);
  void SaveDispClosureResults(int fe, int pixtype, std::shared_ptr<TOTanalysisROOT::ModuleData> module_data, std::vector<float> v_mean_tot, std::vector<float> v_mean_tot_err, std::vector<float> v_disp, std::vector<float> v_disp_err);
  void SaveResults(std::shared_ptr<TOTanalysisROOT::ModuleData> module_data);

  std::vector<float> removePoint(std::vector<float> v_full, int point);

  std::shared_ptr<TF1> createTotFitFunction(const char* name);
  std::shared_ptr<TF1> createTotInvFitFunction(const char* name);
  std::shared_ptr<TF1> createDispFitFunction(const char* name);
  TF1* createTotFitFunctionPtr(const char* name);
  TF1* createDispFitFunctionPtr(const char* name);

  static double TOTFunc(double *x, double *par);
  static double TOTFuncPol(double *x, double *par);
  static double TOTFuncPol0(double *x, double *par);
  static double TOTFuncPol2(double *x, double *par);
  static double TOTFuncPol3(double *x, double *par);
  static double TOTFuncJens(double *x, double *par);
  static double TOTFuncDetlef(double *x, double *par);
  static double TOTFuncChebyshevSecond(double *x, double *par);
  static double TOTFuncChebyshevThird(double *x, double *par);
  static double TOTFuncIskander(double *x, double *par);

  static double TOTFuncInv(double *y, double *par);
  static double TOTFuncInvPol(double *y, double *par);
  static double TOTFuncInvPol0(double *y, double *par);
  static double TOTFuncInvPol2(double *y, double *par);
  static double TOTFuncInvPol3(double *y, double *par);
  static double TOTFuncInvJens(double *y, double *par);
  static double TOTFuncInvDetlef(double *y, double *par);
  static double TOTFuncInvChebyshevSecond(double *y, double *par);
  static double TOTFuncInvChebyshevThird(double *y, double *par);
  static double TOTFuncInvIskander(double *y, double *par);

 private:

  ROOTanalysisHelper* helper;

  int _Ncol;
  int _Nrow;
  int _Nfe;
  int _ChipType;
    std::vector<unsigned short int> _fe_disable;
    std::vector<std::vector<unsigned short int> > _cp_enable;

};

class MeanSigmaDeriver {
  
 public:
  
  MeanSigmaDeriver() 
    : m_sum(0), 
    m_sum2(0), 
    m_nentries(0) 
      {
      }
    ~MeanSigmaDeriver() {}
    void AddValue(float x) {
      m_sum += x;
      m_sum2 += x*x;
      ++m_nentries;
    }
    void RemoveValue(double x) {
      std::cout << "m_sum before " << m_sum << std::endl;
      std::cout << "m_sum2 before " << m_sum2 << std::endl;
      std::cout << "m_nentries before " << m_nentries << std::endl;
      m_sum -= x;
      m_sum2 -= x*x;
      --m_nentries;
      std::cout << "m_sum after " << m_sum << std::endl;
      std::cout << "m_sum2 after " << m_sum2 << std::endl;
      std::cout << "m_nentries after " << m_nentries << std::endl;
    }
    double GetMean() {
      double mean = 0.;
      if (m_nentries > 0.) mean = m_sum/m_nentries;
      return mean;
    }
    double GetMeanError() {
      double mean_err = 0.;
      if (m_nentries > 0.) mean_err = this->GetSigma()/sqrt(m_nentries);
      return mean_err;
    }
   double GetSigma() {
      double sigma = 0.;
      if (m_nentries > 1) sigma = (m_sum2 - m_sum*m_sum/m_nentries)/m_nentries;
      if (sigma < 0.) sigma = 0.; // this will only happen if numerical problems, set sigma to zero before sqrt()
      sigma = sqrt(sigma);
      return sigma;
    }
   double GetSigmaError() {
     double sigma_err = 0.;
     if (m_nentries > 0.) sigma_err = (this->GetSigma()*this->GetSigma())/(2.*m_nentries);
     if (sigma_err < 0.) sigma_err = 0.; // this should never happen, but just in case set sigma to zero before sqrt()
     sigma_err = sqrt(sigma_err);
     return sigma_err;
   }
   int GetEntries() {
     return (int) m_nentries;
   }
   
 private:
    double m_sum;
    double m_sum2;
    double m_nentries;
};

class MeanSigmaWithoutOutliers {
  
 public:
  
  MeanSigmaWithoutOutliers(const std::vector<float> &v_tot, const std::vector<float> &v_sigma)
    :
    m_ms_tot_raw(),
    m_ms_tot_trimmed(),
    m_ms_sigma_raw(),
    m_ms_sigma_trimmed(),
    m_n_trimmed_pixels(0)
      {
	int n_tot = v_tot.size();
	int n_sigma = v_sigma.size();
	if (n_tot == n_sigma) { // if this is not true I don't know which pixel is which
	  for (int i=0; i!=n_tot; ++i) {
	    m_ms_tot_raw.AddValue(v_tot[i]);
	    m_ms_sigma_raw.AddValue(v_sigma[i]);
	  }
	  float mpv_tot = GetRawMostProbableValue(v_tot,1280,0.2);
	  float mpv_sigma = GetRawMostProbableValue(v_sigma,40,0.2);
	  float rms_tot = m_ms_tot_raw.GetSigma();
	  float rms_sigma = m_ms_sigma_raw.GetSigma();
	  for (int i=0; i!=n_tot; ++i) {
	    if (isTrimmed(v_tot[i],mpv_tot,rms_tot,2.) && isTrimmed(v_sigma[i],mpv_sigma,rms_sigma,2.)) {
	      m_ms_tot_trimmed.AddValue(v_tot[i]);
	      m_ms_sigma_trimmed.AddValue(v_sigma[i]);
	      ++m_n_trimmed_pixels;
	      m_trimmed.push_back(1);
	    }
	    else {
	      m_trimmed.push_back(0);
	    }
	  }
	}
      }
    
    ~MeanSigmaWithoutOutliers() {}
    
 /*    float GetMean() {return m_ms_tot_trimmed.GetMean();} */
/*     float GetMeanError() {return m_ms_tot_trimmed.GetMeanError()*1.136;}  // correcting back to the true rms in case of perfect gauss */
/*     float GetSigma() {return m_ms_sigma_trimmed.GetMean();} */
/*     float GetSigmaError() {return m_ms_sigma_trimmed.GetMeanError()*1.136;}  // correcting back to the true rms in case of perfect gauss */
/*     float GetDispersion() {return m_ms_tot_trimmed.GetSigma()*1.136;} // correcting back to the true rms in case of perfect gauss */
/*     float GetDispersionError() {return m_ms_tot_trimmed.GetSigmaError()*1.136;} // correcting back to the true rms in case of perfect gauss */
/*     int GetNumberOfTrimmedPixels() {return m_n_trimmed_pixels;} */
/*     bool isTrimmedPixel(int pix) {bool mtrimmed = (m_trimmed[pix] != 0); return mtrimmed;} */

    float GetMean() {return m_ms_tot_trimmed.GetMean();}
    float GetMeanError() {return m_ms_tot_trimmed.GetMeanError();}
    float GetSigma() {return m_ms_sigma_trimmed.GetMean();}
    float GetSigmaError() {return m_ms_sigma_trimmed.GetMeanError();}
    float GetDispersion() {return m_ms_tot_trimmed.GetSigma();}
    float GetDispersionError() {return m_ms_tot_trimmed.GetSigmaError();}
    int GetNumberOfTrimmedPixels() {return m_n_trimmed_pixels;}
    bool isTrimmedPixel(int pix) {bool mtrimmed = (m_trimmed[pix] != 0); return mtrimmed;}
    
 private:
    
    MeanSigmaDeriver m_ms_tot_raw;
    MeanSigmaDeriver m_ms_tot_trimmed;
    MeanSigmaDeriver m_ms_sigma_raw;
    MeanSigmaDeriver m_ms_sigma_trimmed;
    int m_n_trimmed_pixels;
    std::vector<unsigned short int> m_trimmed;
    
    bool isTrimmed(float x, float mpv, float rms, float n_std_cutoff) {
      bool is_trimmed = false;
      // n_std_cutoff = number of standard deviations from MPV we start tossing data
      if(x > mpv-n_std_cutoff*rms && x <= mpv+n_std_cutoff*rms) is_trimmed = true;
      return is_trimmed;
    }
    float GetRawMostProbableValue(const std::vector<float> &v, const int nbins, const float binwidth) {
      // you cannot change the number of bins and the binwidth independently!
      // range always has to be 0-256!
      std::vector<float> number(nbins,0.), lower_bound(nbins,0.), upper_bound(nbins,0.), bin_average(nbins,0.);
      for(int bin=0; bin!=nbins; ++bin) {
	number[bin] = 0.;
	lower_bound[bin] =  float(bin)*binwidth - 0.5*binwidth;
	upper_bound[bin] = float(bin+1)*binwidth - 0.5*binwidth; 
	bin_average[bin] = (lower_bound[bin] + upper_bound[bin])/2.;
      }
      int n = v.size();
      for(int data_index=0; data_index!=n; ++data_index) {
	for(int bin_index=0; bin_index!=nbins; ++bin_index) {
	  if(v[data_index] >= lower_bound[bin_index] && v[data_index] < upper_bound[bin_index]) {
	    number[bin_index] += 1.;
	  }
	}
      }
      float highest_bin = number[0];
      float most_probable_value = bin_average[0];
      for(int bin_index=0; bin_index!=nbins; ++bin_index) {
	if(number[bin_index] > highest_bin) {
	  highest_bin = number[bin_index];
	  most_probable_value = bin_average[bin_index];      
	}
      }
      return most_probable_value;
    }

};

#endif

