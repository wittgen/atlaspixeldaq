#ifndef _analysisOutputBOND_h_
#define _analysisOutputBOND_h_

#include "analysisOutput_Base.h"
#include "PixelCoralClientUtils/PixelMap.h"


class ResultBOND : public AnalysisResult { //this is for one scan and one module
 public:
  ResultBOND() :AnalysisResult() {};
  ~ResultBOND() {};


  TH1F hInt_delta; //delta of noise for HVon/HVoff for all pixles 

  unsigned int numBadPixels; //[6]: for 6 different pixel types ?
  PixelMap_t badPixels;
  TH2F badPixelmap;

 public:
  //ClassDef(ResultBOND,1) //not 0 -> activate I/O  ; not needed at the moemnt, put in if needed!
};

#endif
