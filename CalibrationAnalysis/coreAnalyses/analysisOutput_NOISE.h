#ifndef _analysisOutputNOISE_h_
#define _analysisOutputNOISE_h_

#include "analysisOutput_Base.h"
#include "PixelCoralClientUtils/PixelMap.h"

class ResultNOISE : public AnalysisResult { //this is for one scan and one module
 public:
  ResultNOISE(){
      
  };
  ~ResultNOISE() {};

  float PixNoisy;
  bool MakePixelMap; // to make sure the analysis only creates maps if selected

  PixelMap_t noisyPixelMap;

 public:
};

#endif
