#ifndef _BUMPanalysisROOT_h_
#define _BUMPanalysisROOT_h_ _

#include "ROOTanalysisBase.h"
#include "analysisInput_BUMP.h"
#include "analysisOutput_BUMP.h"
#include <iostream>
#include <TFile.h>
#include <TString.h>


#include "../Lock.hh"
#include "../Flag.hh"



const int NROWS = 320;
const int NCOLS = 144;

class BUMPanalysisROOT : public ROOTanalysisBase 
{
 public:
  BUMPanalysisROOT();
  ~BUMPanalysisROOT();

  // cuts
  float m_longNoiseCut;
  float m_normalNoiseCut;
  float m_minHitsAnalog;
  float m_minHitsXtalk;
//********************************
//bool m_is_merged;
//*******************************************************
    
  void Initialize();

  void Calculate(); //internal calculations

  void QualifyResults(); //apply cuts from analysis config

  void CheckForKnownProblems() {};

 private:
  void WriteResults();

  int _Nrow;
  int _Ncol;
  int _ChipType;
    std::vector<unsigned short int> FE_disable;
    std::vector<std::vector<unsigned short int> > _cp_enable;
    unsigned int _Nmasks;
    unsigned int _Nevents;
    unsigned int _Nmask_stage;

};
#endif
  
