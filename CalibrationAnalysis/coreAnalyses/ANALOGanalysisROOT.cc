#include "ANALOGanalysisROOT.h"
#include <iostream>
#include <fstream>
#include "TFile.h"



ANALOGanalysisROOT::ANALOGanalysisROOT()
{
  std::cout << "ANALOGanalysis:  constructed." << std::endl;
}


ANALOGanalysisROOT::~ANALOGanalysisROOT()
{
  WriteResults();
}

void ANALOGanalysisROOT::WriteResults()
{
  TFile f("resultsANALOG.root","update");
  f.WriteTObject(m_result.get(),"scanHistos","SingleKey"); //as one array
  f.Close();  
}

void ANALOGanalysisROOT::Initialize()
{
  //! Don't hard code here. Take values from analysis configuration, defaults in AutomaticAnalysis.
  //cut_current_max=400;
}


void ANALOGanalysisROOT::Calculate() 
{
  std::cout << "**************************************" << std::endl;
  std::cout << "ANALOGanalysis::Calculate  starting" << std::endl;
  m_scan->R__FOR_EACH(AnalysisInputANALOG,PrintInfo)(); 
  std::cout << "ANALOGanalysis: These were the module names." << std::endl;  

  hInt = *new TH1F("hIntAllModules","Currents for all modules",300,-200,100); 

  for(Int_t i=0; i<m_scan->GetEntries(); i++) { 
    ResultANALOG* module_result = new ResultANALOG();
    module_result->moduleName = ((AnalysisInputANALOG*)m_scan->At(i))->moduleName;

    //determine how many events were scanned (should get this from scan config)
    //cout << "#entries in input histo: " << (((AnalysisInputANALOG*)m_scan->At(i))->histo).GetEntries() << std::endl; 
    if(((AnalysisInputANALOG*)m_scan->At(i))->histo->GetEntries()==0) {
      std::cout << "ANALOGanalysis: WARNING  no histos for module " << module_result->moduleName << " -> skipping." << std::endl;
      module_result->passed=false;
      m_result->Add( module_result );
      continue;
    }
    std::cout << "Analysing module: " << module_result->moduleName << std::endl; 
    module_result->passed=true;

    // Loop over pixel types
    for (int pixtype=0; pixtype<helper.NpixelTypes; pixtype++) {
      std::cout << "Now looking at pixel type: " << helper.getPixelTypeString(pixtype) << std::endl;

      std::shared_ptr<TH1F> histo = helper.IntegrateHisto( ((AnalysisInputANALOG*)m_scan->At(i))->histo, 1, 301, -1, -1);
      events = histo->GetMaximumBin();
      std::cout << "#events in Analog scan: " << events << std::endl; 
      std::shared_ptr<TH1F> histo2 = helper.IntegrateHisto( ((AnalysisInputANALOG*)m_scan->At(i))->histo, -200, 100, -1, -1, events); //deviation from expected events
      module_result->hInt = *histo2.get(); //make a copy
      hInt.Add(histo2.get());
      
      std::cout << "cutting on currrent_max: ";
      if( !helper.FulfillMaxCut( histo2.get(), cut_current_max) ) 
	module_result->passed=false; 
        //helper.SetBit(module_result->passed,1); 
      
      module_result->average[pixtype] = histo2->GetMean();
    }
    std::cout << "Result for module " << module_result->moduleName << " is: " << ( (module_result->passed)?  "OK":"FAILED" ) << std::endl;
    m_result->Add( module_result );
  }

  //AdditionalCalculations();

  std::cout << "ANALOGanalysis: analysis finished." << std::endl;
}

  
void ANALOGanalysisROOT::AdditionalCalculations() 
{
  TH1F means_modules("MeansModules","Means for different modules",m_scan->GetEntries(),0,m_scan->GetEntries()); 

  for(Int_t i=0; i<m_scan->GetEntries(); i++) { 
    ResultANALOG* module_result = new ResultANALOG();
    module_result->moduleName = ((AnalysisInputANALOG*)m_scan->At(i))->moduleName;

    if((((AnalysisInputANALOG*)m_scan->At(i))->histo)->GetEntries()==0) continue;

    std::shared_ptr<TH1F> histo = helper.IntegrateHisto( ((AnalysisInputANALOG*)m_scan->At(i))->histo, 1, 301, -1, -1);
    events = histo->GetMaximumBin();
    std::cout << "#events in Analog scan: " << events << std::endl; 
    std::shared_ptr<TH1F> histo2 = helper.IntegrateHisto( ((AnalysisInputANALOG*)m_scan->At(i))->histo, -200, 100, -1, -1, events); //deviation from expected events

    GetErrorMap( ((AnalysisInputANALOG*)m_scan->At(i))->histo, events, module_result);

    for(int FE=1; FE<=16; FE++) {
      std::shared_ptr<TH1F> histo3 = helper.IntegrateHisto( ((AnalysisInputANALOG*)m_scan->At(i))->histo, -200, 100, -1,  FE, events);
      TH1F htemp = *histo3.get(); //make a copy
      module_result->hInt_FEs.push_back(htemp);

      std::cout << " mean for FE " << FE << ": " << histo3->GetMean() << std::endl;
      module_result->AvPerFE.Fill(FE-1, histo3->GetMean() );
      module_result->RMSPerFE.Fill(FE-1, histo3->GetRMS() );
    }
    module_result->variance_mean_FEs = helper.GetVariance(module_result->AvPerFE);
    module_result->variance_RMS_FEs = helper.GetVariance(module_result->RMSPerFE);

    means_modules.Fill(i,histo->GetMean());

    m_result->Add( module_result );
   
    //helper.Dump( histo );
  }
  variance_modules = helper.GetVariance(means_modules);

  std::cout << "ANALOGanalysisROOT:: End of AdditionalCalculations()" << std::endl;
} 


void ANALOGanalysisROOT::QualifyResults()
{
  bool allPassed(true);
  for(Int_t i=0; i<m_result->GetEntries(); i++) { 
    ResultANALOG* module_result = (ResultANALOG*)m_result->At(i); 
    if(!module_result->passed) { 
      allPassed=false;
      break;
    }
  }  
  if(allPassed)
    m_status=kPassed;
  else 
    m_status=kFailed;

  std::cout << "ANALOGanalysis::QualifyResults  Total result is :" << ( (allPassed)? "OK":"FAILED" ) << std::endl;
  std::cout << "*************************************" << std::endl;
}

void ANALOGanalysisROOT::CheckForKnownProblems()
{

}


void ANALOGanalysisROOT::GetErrorMap(const TH2* histo, Int_t events_arg, ResultANALOG* module_result)
{
  int cols=helper.Ncols;  //   /8;
  int rows=helper.Nrows;  //   /2;

  module_result->hErrorMap         =  TH2F("erorPixelMap","pixel map of error pixels",cols,0,cols,rows,0,rows);
  module_result->hErrorMap_plus1   =  TH2F("erorPixelMap_p1","pixel map of +1 error pixels",cols,0,cols,rows,0,rows);
  module_result->hErrorMap_minus1  =  TH2F("erorPixelMap_m1","pixel map of -1 error pixels",cols,0,cols,rows,0,rows);
  module_result->hErrorMap_minus19 =  TH2F("erorPixelMap_m19","pixel map of -19 error pixels",cols,0,cols,rows,0,rows);
  
  for(int x=1; x<=cols; x++) {
    for(int y=1; y<=rows; y++) {

      if(histo->GetBinContent(x,y)!=0 && histo->GetBinContent(x,y)!=events_arg) {  //look for pixel counts which are not 0(within scna pattern), but dont show expected number of counts 
	int diff(histo->GetBinContent(x,y)-events_arg);
	module_result->hErrorMap.Fill( x,y, diff ); 
	std::cout << "filling error map " << diff <<  std::endl;
	if(diff == +1) 	module_result->hErrorMap_plus1  .Fill( x,y, diff );
	if(diff == -1)  module_result->hErrorMap_minus1 .Fill( x,y, diff );
	if(diff == -19) module_result->hErrorMap_minus19.Fill( x,y, diff );
      }
    }
  }
}
