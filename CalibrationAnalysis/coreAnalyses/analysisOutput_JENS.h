#ifndef _analysisOutputBOC_h_
#define _analysisOutputBOC_h_

#include "analysisOutput_Base.h"


class ResultJENS : public AnalysisResult { //this is for one scan and one module
 public:
  ResultJENS() :AnalysisResult() {
      passed = 0;
  };
  ~ResultJENS() {};

  float idealMSR0, idealMSR1;
  float minval0, minval1;

 public:
  //ClassDef(ResultJENS,1) //not 0 -> activate I/O  ; not needed at the moemnt, put in if needed!
};

#endif
