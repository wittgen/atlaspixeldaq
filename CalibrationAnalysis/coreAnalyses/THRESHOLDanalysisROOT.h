#ifndef _THRESHOLDanalysisROOT_h_
#define _THRESHOLDanalysisROOT_h_ _

#include "ROOTanalysisBase.h"
#include "analysisInput_THRESHOLD.h"
#include "analysisOutput_THRESHOLD.h"
#include <iostream>
#include <TFile.h>
#include <TString.h>



class THRESHOLDanalysisROOT : public ROOTanalysisBase 
{
 public:
  THRESHOLDanalysisROOT();
  ~THRESHOLDanalysisROOT();
    
  void Initialize();

  void Calculate(); //internal calculations

  void QualifyResults(); //apply cuts from analysis config

  void CheckForKnownProblems(); //setting of failure bits


  //The cuts are directly accessible from the AutomaticAnalysis
  Float_t cut_noise_max[3]; 
  Int_t   cut_noise_max_percent[3];
  Float_t cut_thr_min; 
  Int_t   cut_thr_min_percent;
  Float_t cut_thr_max;
  Int_t   cut_thr_max_percent;
  Float_t cut_thr_mean_min; 
  Float_t cut_thr_mean_max;
  Float_t cut_thr_RMS_max;
  Int_t   range_thr_mean_min;
  Int_t   range_thr_mean_max;
  Float_t cut_thresholdOverNoise_min;
  Int_t   cut_num_bad_pixels_per_FE;

  bool create_Pixelmap;
  
 private:
  void WriteResults();
  void AdditionalCalculations();
  //tests
  void CheckForZeroMean(ResultTHRESHOLD* module_result);
  void findBadPixels(ResultTHRESHOLD* module, int ientry, std::vector<unsigned short int> fe_disable, std::vector<std::vector<unsigned short int> > cp_enable);

  std::vector<unsigned short int> FE_disable;
  std::vector<std::vector<unsigned short int> > CP_enable;

  Int_t num_bad_pixels[16];
};


#endif

