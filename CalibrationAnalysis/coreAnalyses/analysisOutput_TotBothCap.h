#ifndef _analysisOutputTotBothCap_h_
#define _analysisOutputTotBothCap_h_

#include <TString.h>
#include <TH2F.h>
#include "analysisOutput_Base.h"


class ResultTotBothCap : public AnalysisResult // this stores the results for one scan and one module
{
 public:
  ResultTotBothCap():AnalysisResult() {};
  ~ResultTotBothCap() {};
  
  std::vector<std::vector<std::vector<Float_t> > > fe_pixtype_tot;
  std::vector<std::vector<std::vector<Float_t> > > fe_pixtype_tot_err;  
  
};

#endif
