#ifndef _analysisOutputANNA_h_
#define _analysisOutputANNA_h_

#include "analysisOutput_Base.h"

class ResultANNA : public AnalysisResult { //this is for one scan and one module
 public:
  ResultANNA(){
      passed = 0;
  };
  ~ResultANNA() {};

  float PixExtraHits;
  float PixMissingHits;
  float MaxMissingHits;
  float MaxExtraHits;
  bool ModStatus;

 public:
  //ClassDef(ResultANNA,1) //not 0 -> activate I/O  ; not needed at the moemnt, put in if needed!
};

#endif
