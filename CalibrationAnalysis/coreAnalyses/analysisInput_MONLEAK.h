#ifndef _analysisInputMONLEAK_h_
#define _analysisInputMONLEAK_h_

#include "analysisInput_Base.h"
#include <vector>

class AnalysisInputMONLEAK : public AnalysisInput { //this is for one scan and one module
 public:
  AnalysisInputMONLEAK() :AnalysisInput(), histo_monleak(NULL) {}

  ~AnalysisInputMONLEAK() {};

  const TH2* histo_monleak; 


  std::vector<unsigned short int> fe_disable;

  //Helper functions
  void PrintInfo() {
  };

 public:
  //ClassDef(AnalysisInputMONLEAK,1) //not 0 -> activate I/O 
};

#endif
