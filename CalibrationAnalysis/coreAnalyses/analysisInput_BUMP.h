#ifndef _analysisInputBUMP_h_
#define _analysisInputBUMP_h_

#include "analysisInput_Base.h"
#include <vector>


class AnalysisInputBUMP : public AnalysisInput { //this is for one scan and one module
 public:
  AnalysisInputBUMP() :AnalysisInput() {};
  ~AnalysisInputBUMP() {};

  const TH2* histo_sigma; // noise distribution from threshold scan  
  const TH2* histo_analog; // analog scan for this module
  const TH2* histo_xtalk;// xtalk scan for this module
    const TH2* histo_analog_merg; // analog scan no low charge
    Int_t Nmasks;
    Int_t Nevents;
    Int_t NTotmasks;

  TString moduleName; 
  TString pp0Name;

  //Helper functions
  void PrintInfo() {
  };

};

#endif
