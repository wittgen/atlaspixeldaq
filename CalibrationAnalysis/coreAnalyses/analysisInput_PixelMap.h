#ifndef _analysisInputPixelMap_h_
#define _analysisInputPixelMap_h_

#include "analysisInput_Base.h"
#include "PixelCoralClientUtils/PixelMap.h"


class AnalysisInputPixelMap : public AnalysisInput { //this is for one scan and one module
 public:
  AnalysisInputPixelMap() : 
    use_analog(false),
    use_digital(false),
    use_threshold(false),
    use_bond(false),
    use_tot(false),
    use_crosstalk(false),
    use_calibrationMask(false),
    use_datatakingMask(false)
    {};
  ~AnalysisInputPixelMap() {};


  // Input are results from other analyses:
  PixelMap_t pm_analog;
  PixelMap_t pm_digital;
  PixelMap_t pm_threshold;
  PixelMap_t pm_bond;
  PixelMap_t pm_tot;
  PixelMap_t pm_crosstalk;
  //
  PixelMap_t pm_calibrationMask;
  PixelMap_t pm_datatakingMask;

  bool use_analog;
  bool use_digital;
  bool use_threshold;
  bool use_bond;
  bool use_tot;
  bool use_crosstalk;
  bool use_calibrationMask;
  bool use_datatakingMask;

  //Helper functions
  void PrintInfo() {
    std::cout << "AnalysisInputPixelMap for module: " << moduleName.Data() << std::endl;
  };

 public:
  //ClassDef(AnalysisInputPixelMap,1) //not 0 -> activate I/O 
};

#endif
