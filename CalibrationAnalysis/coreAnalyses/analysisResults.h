#ifndef _analysisResults_h_
#define _analysisResults_h_


/** CLA:
 *  Container for pixel maps
 *  For a value definition of the status see: https://twiki.cern.ch/twiki/bin/view/Atlas/PixelCOOLoffline#Dead_and_special_pixels_map
 */
class Pixel_t
{
 public:
  Pixel_t(const unsigned int row, const unsigned int column, const unsigned int status)
    : m_row(row), m_column(column), m_status(status)
    {}
  unsigned int Row()    { return m_row; }; 
  unsigned int Column() { return m_column; }; 
  unsigned int Status() { return m_status; };
 private:
  unsigned int m_row; 
  unsigned int m_column; 
  unsigned int m_status;
};
typedef std::vector< Pixel_t >  PixelMap_t; 


#endif
