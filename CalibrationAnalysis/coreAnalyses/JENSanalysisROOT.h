#ifndef _JENSanalysisROOT_h_
#define _JENSanalysisROOT_h_ 

#include "ROOTanalysisBase.h"
#include "analysisInput_JENS.h"
#include "analysisOutput_JENS.h"


class JENSanalysisROOT  : public ROOTanalysisBase
{
 public:
  JENSanalysisROOT();
  ~JENSanalysisROOT();

  std::string m_filename;

 private: //methods

  void Initialize();

  void Calculate(); //internal calculations

  void QualifyResults(); //apply cuts from analysis config

  void CheckForKnownProblems(); //setting of failure bits
  
  
  int FindBestMSR();
  void Print();	  
  
  
 private: //variables  
  int _NMSR; 
  std::vector<int> _MSRVal;
  

  // Algorithm parameters
  // --------------------
  
  int _MSR[7][2];
  int Minval[7][2];

};


#endif

