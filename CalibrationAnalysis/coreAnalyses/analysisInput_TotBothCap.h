#ifndef _analysisInputTotBothCap_h_
#define _analysisInputTotBothCap_h_

#include "analysisInput_Base.h"
#include <vector>
#include <TH1F.h>
#include <TH2F.h>
#include <TString.h>
#include <iostream>

class AnalysisInputTotBothCap : public AnalysisInput  //this is for one scan and one module
{
 public:
  
  AnalysisInputTotBothCap() :AnalysisInput() {};
  ~AnalysisInputTotBothCap() {};
  
  TString moduleName;
  TString pp0Name;
  //TString connName;
  
  std::vector<const TH2*> histo_mean_clow; 
  std::vector<const TH2*> histo_sigma_clow; 
  std::vector<std::vector<float> > charge_clow; // array of injected charges per frontend
  std::vector<const TH2*> histo_mean_chigh_full; 
  std::vector<const TH2*> histo_sigma_chigh_full; 
  std::vector<std::vector<float> > charge_chigh_full; // array of injected charges per frontend
  std::vector<const TH2*> histo_mean_chigh_lim; 
  std::vector<const TH2*> histo_sigma_chigh_lim; 
  std::vector<std::vector<float> > charge_chigh_lim; // array of injected charges per frontend
  int n_maskstages_clow;
  int n_events_clow;
  int n_vcalsteps_clow;
  int n_maskstages_chigh_full;
  int n_events_chigh_full;
  int n_vcalsteps_chigh_full;
  int n_maskstages_chigh_lim;
  int n_events_chigh_lim;
  int n_vcalsteps_chigh_lim;

  // Helper functions
  void PrintInfo()
    {
      std::cout << "AnalysisInputTotBothCap for pp0 " << pp0Name.Data() << ", module: " << moduleName.Data() << std::endl;
    };
  //ClassDef(AnalysisInputTotBothCap,1);
};

#endif
