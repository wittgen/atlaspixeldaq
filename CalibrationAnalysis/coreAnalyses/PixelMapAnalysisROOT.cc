#ifndef _PixelMapAnalysisROOT_CC_
#define _PixelMapAnalysisROOT_CC_

#include "PixelMapAnalysisROOT.h"
#include <iostream>
#include "TMath.h"

PixelMapAnalysisROOT::PixelMapAnalysisROOT()
{
  std::cout << "PixelMapAnalysis:  constructed." << std::endl;
} 


PixelMapAnalysisROOT::~PixelMapAnalysisROOT()
{
  //WriteResults();
}

void PixelMapAnalysisROOT::WriteResults()
{
  TFile f("resultsPixelMap.root","update");
  f.WriteTObject(m_result.get(),"AnalysisInput","SingleKey"); //as one array
  f.Close();  
}

void PixelMapAnalysisROOT::Initialize()
{
  //! Don't hard code here. Take values from analysis configuration, defaults in AutomaticAnalysis.

//   if(cut  ==0) {
//     std::cout << "WARNING   Apparent problem in reading analysis config, all cuts are zero.  -> Setting default values." << std::endl;

//   }
}  

void PixelMapAnalysisROOT::Calculate() 
{
  std::cout << "**************************************" << std::endl;
  std::cout << "PixelMapAnalysis::Calculate  starting" << std::endl;
  m_scan->R__FOR_EACH(AnalysisInputPixelMap,PrintInfo)(); 
  std::cout << "PixelMapAnalysis: These were the module names." << std::endl;  

  // Loop over modules
  for(Int_t i=0; i<m_scan->GetEntries(); i++) { 
    ResultPixelMap* module_result = new ResultPixelMap();
    module_result->pp0Name = ((AnalysisInputPixelMap*)m_scan->At(i))->pp0Name;
    module_result->moduleName = ((AnalysisInputPixelMap*)m_scan->At(i))->moduleName;

    std::cout << "Analysing module: " << module_result->moduleName << std::endl; 
    module_result->passed=true;

//   PixelMap_t pm_calibrationMask;
//   PixelMap_t pm_datatakingMask;

    // Combine pixel maps
    AddPixelMap( module_result, &((AnalysisInputPixelMap*)m_scan->At(i))->pm_analog,          13); 
    AddPixelMap( module_result, &((AnalysisInputPixelMap*)m_scan->At(i))->pm_digital,          8); 
    AddPixelMap( module_result, &((AnalysisInputPixelMap*)m_scan->At(i))->pm_threshold,       17); 
    AddPixelMap( module_result, &((AnalysisInputPixelMap*)m_scan->At(i))->pm_bond,             9); 
    AddPixelMap( module_result, &((AnalysisInputPixelMap*)m_scan->At(i))->pm_tot,             14); 
    AddPixelMap( module_result, &((AnalysisInputPixelMap*)m_scan->At(i))->pm_crosstalk,       15); 

    // Set overall useability bit 0
    for(PixelMap_t::iterator iter=module_result->finalPixelMap.begin(); iter != module_result->finalPixelMap.end(); ++iter) {
      if( helper.HasBit(iter->second, 0) ||
	  helper.HasBit(iter->second, 1) || 
	  helper.HasBit(iter->second, 8) || 
	  helper.HasBit(iter->second, 9) || 
	  helper.HasBit(iter->second,13) || 
	  helper.HasBit(iter->second,14) || 
	  helper.HasBit(iter->second,16) ) 
	helper.SetBit( iter->second, 0 );
    }

    // Get number of bad pixels for different pixel types
    for (int pixtype=0; pixtype<helper.NpixelTypes; pixtype++) {
      std::cout << "Now looking at pixel type: " << helper.getPixelTypeString(pixtype) << std::endl;
      module_result->numBadPixels[pixtype] = CountPixelsWithBit( module_result->finalPixelMap, 0, pixtype);     
    }
    module_result->numBadPixels_total = CountPixelsWithBit( module_result->finalPixelMap, 0); 

    // Record which information was used 
    if(((AnalysisInputPixelMap*)m_scan->At(i))->use_analog)    helper.SetBit( module_result->infoUsed, 1);
    if(((AnalysisInputPixelMap*)m_scan->At(i))->use_digital)   helper.SetBit( module_result->infoUsed, 2);
    if(((AnalysisInputPixelMap*)m_scan->At(i))->use_threshold) helper.SetBit( module_result->infoUsed, 3);
    if(((AnalysisInputPixelMap*)m_scan->At(i))->use_bond)      helper.SetBit( module_result->infoUsed, 4);
    if(((AnalysisInputPixelMap*)m_scan->At(i))->use_tot)       helper.SetBit( module_result->infoUsed, 5);
  }

  std::cout << "PixelMapAnalysis: analysis finished." << std::endl;
}


void PixelMapAnalysisROOT::AddPixelMap(ResultPixelMap* module_result, PixelMap_t * pixelmap, int bitToSet) 
{
  for(PixelMap_t::iterator iter=pixelmap->begin(); iter!=pixelmap->end(); ++iter) {
    if(iter->second!=0) {
      const std::pair<int,int>& index(iter->first);
      if( module_result->finalPixelMap.find(index) == module_result->finalPixelMap.end() )  
	module_result->finalPixelMap[index]=0;
      helper.SetBit( module_result->finalPixelMap[index], bitToSet );
    }
  }
}

void PixelMapAnalysisROOT::QualifyResults() 
{
  bool allPassed(true);
  for(Int_t i=0; i<m_result->GetEntries(); i++) { 
    ResultPixelMap* module_result = (ResultPixelMap*)m_result->At(i); 
    if(!module_result->passed) { 
      allPassed=false;
      break;
    }
  }  
  if(allPassed)
    m_status=kPassed;
  else 
    m_status=kFailed;

  std::cout << "PixelMapAnalysis::QualifyResults  Total result is :" << ( (allPassed)? "OK":"FAILED" ) << std::endl;
  std::cout << "*************************************" << std::endl;
}


void PixelMapAnalysisROOT::CheckForKnownProblems() //setting of failure bits
{
  for(Int_t i=0; i<m_result->GetEntries(); i++) { 
    //    ResultPixelMap* module_result = (ResultPixelMap*)m_scan->At(i); 
    ///
    //CheckForZeroMean(module_result);
  }
}


int PixelMapAnalysisROOT::CountPixelsWithBit(const PixelMap_t& pixelmap, int bit, int selectType)
{
  int count(0);
  for(PixelMap_t::const_iterator iter=pixelmap.begin(); iter!=pixelmap.end(); ++iter) {
    int type = helper.pixelType( iter->first.first -1, iter->first.second -1 ); //index position start counting at 1,1 !
    if(selectType!=-1 && type!=selectType) continue;
    if(helper.HasBit(iter->second,bit)) count++;
  }
  return count;
}

#endif

