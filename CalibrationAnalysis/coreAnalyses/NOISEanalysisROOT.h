#ifndef _NOISEanalysisROOT_h_
#define _NOISEanalysisROOT_h_ 

#include "ROOTanalysisBase.h"
#include "analysisInput_NOISE.h"
#include "analysisOutput_NOISE.h"
#include "TH2I.h"
#include "ROOTanalysisHelper.h"

class NOISEanalysisROOT  : public ROOTanalysisBase
{
 public:
  NOISEanalysisROOT();
  ~NOISEanalysisROOT();


 private:
  void Initialize();

  void Calculate(); //internal calculations
  
  void QualifyResults(); //apply cuts from analysis config

  void CheckForKnownProblems();

  void Print();

 public:
  Float_t max_noisy_pixel;
  Int_t max_noise_allowed;
  bool create_Pixelmap;
 
 private: 

  //variables

  bool allModulesPassed;
  bool Passed;

  // Algorithm parameters
  // --------------------

  int _Nmasks;
  int _Nevents;
  int _Nmasksdone;
    
  int _Ncol;
  int _Nrow;

  int _ChipType;

  // # of column and row must be optimized by Yosuke. 
  //int _pp0pattern[150][400];
  //int _pattern[150][400];
 
  std::vector<unsigned short int> _fe_disable;
std::vector<std::vector<unsigned short int> > _cp_enable;

};


#endif

