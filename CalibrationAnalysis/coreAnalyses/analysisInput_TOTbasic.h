#ifndef _analysisInputTOTbasic_h_
#define _analysisInputTOTbasic_h_

#include "analysisInput_Base.h"
#include <vector>
#include <TH2F.h>
#include <TString.h>
#include <iostream>

class AnalysisInputTOTbasic : public AnalysisInput { //this is for one scan and one module
 public:
  AnalysisInputTOTbasic() :AnalysisInput(),histoMean(0),histoSigma(0),moduleName(""),MBs(-1),
                           Nmasks(0),Nevents(0),scanType(-1),digitalInjection(false) {};
  ~AnalysisInputTOTbasic() {};

  const TH2* histoMean;
  const TH2* histoSigma; 
  std::vector<histoAxis_t> axis; //the axis of the histo (the same for all histos) of type histoAxis_t 
  
  TString moduleName;
  Int_t MBs; //40,80,160 Mbits/s
  TString pp0Name;
  Int_t Nmasks;
  Int_t Nevents;
  Int_t NTotmasks;
  Int_t scanType;
  Bool_t digitalInjection;

  ////////////////////////////////
  ///////Analysis Results
 /*  UInt_t passed; */
/*   UInt_t failure; */
/*   Float_t average; */

  //Helper functions
  void PrintInfo() {
    std::cout << "AnalysisInputTOTbasic for pp0 " << pp0Name.Data() << ", module: " << moduleName.Data() << "  MBit/s=" << MBs << std::endl;
  };
  
};

#endif
