#include "TOTanalysisROOT.h"
#include <iostream>
#include <fstream>
#include "TGraphErrors.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TMath.h"
#include <PixDbGlobalMutex.hh>
#include <Lock.hh>



TOTanalysisROOT::TOTanalysisROOT(): _Ncol(0), _Nrow(0), _Nfe(0), _ChipType(PixGeometry::INVALID_MODULE)
{
  std::cout << std::endl;
  std::cout << "----------------------------" << std::endl;
  std::cout << "   TOT Analysis Algorithm   " << std::endl;
  std::cout << "----------------------------" << std::endl;
  std::cout << std::endl;

  map_tot_fitfuncs.insert(std::make_pair("TOTFunc",3));
  map_tot_fitfuncs.insert(std::make_pair("TOTFuncChebyshevSecond",3));
  map_tot_fitfuncs.insert(std::make_pair("TOTFuncChebyshevThird",4));
  map_tot_fitfuncs.insert(std::make_pair("TOTFuncPol2",3));
  map_tot_fitfuncs.insert(std::make_pair("TOTFuncPol3",4));
  map_tot_fitfuncs.insert(std::make_pair("TOTFuncJens",3));
  map_tot_fitfuncs.insert(std::make_pair("TOTFuncDetlef",3));
  map_tot_fitfuncs.insert(std::make_pair("TOTFuncIskander",3));

  npars_tot_fit_max = 4;
  npars_disp_fit_max = 2;
 
}

TOTanalysisROOT::~TOTanalysisROOT()
{
  if (debug) std::cout << "In TOTanalysisROOT destructor" << std::endl;
}

void TOTanalysisROOT::Initialize()
{
  AnalysisInputTOT* mintot  = (AnalysisInputTOT*)m_scan->At(0);
  if(!mintot || !mintot->histo_mean[0]) return; 

  PixGeometry geo(PixA::nRows(mintot->histo_mean[0]),PixA::nColumns(mintot->histo_mean[0])); 
  _ChipType = geo.pixType();

  _Nrow = helper->getNrow(_ChipType);
  _Ncol = helper->getNcol(_ChipType);
  _Nfe = helper->getNfrontEnd(_ChipType);

  std::cout << "Running with the following parameter settings:" << std::endl;
  
  std::cout << "min_tot_fit_closure " << min_tot_fit_closure << std::endl;
  std::cout << "max_tot_fit_closure " << max_tot_fit_closure << std::endl;
  std::cout << "max_frac_bad_tot_fit_closure " << max_frac_bad_tot_fit_closure << std::endl;
  std::cout << "min_corr " << min_corr << std::endl;
  std::cout << "min_disp_fit_closure " << min_disp_fit_closure << std::endl;
  std::cout << "max_disp_fit_closure " << max_disp_fit_closure << std::endl;
  std::cout << "max_frac_bad_disp_fit_closure " << max_frac_bad_disp_fit_closure << std::endl;
  std::cout << "pred_mip" << pred_mip << std::endl;

    
  std::cout << "max_frac_bad_pixels " << max_frac_bad_pixels << std::endl;
  std::cout << "max_n_bad_pixels " << max_n_bad_pixels << std::endl;
  
  std::cout << "min_tot_mip " << min_tot_mip << std::endl;
  std::cout << "max_tot_mip " << max_tot_mip << std::endl;
  std::cout << "min_disp_mip " << min_disp_mip << std::endl;
  std::cout << "max_disp_mip " << max_disp_mip << std::endl;

  std::cout << "n_pixeltypes " << n_pixeltypes << std::endl;
  std::cout << "fit_each_pixel " << fit_each_pixel << std::endl;
  std::cout << "dump_data " << dump_data << std::endl;
  std::cout << "tot_fitfunc " << tot_fitfunc << std::endl;
  std::cout << "debug output " << debug << std::endl;

  helper = new ROOTanalysisHelper();
  
  npars_tot_fit = map_tot_fitfuncs[tot_fitfunc];
  npars_disp_fit = 2;
 
  std::cout << "npars_tot_fit " << npars_tot_fit << std::endl;
  std::cout << "npars_disp_fit " << npars_disp_fit << std::endl;


}

void TOTanalysisROOT::Calculate() {
  if (debug) std::cout << "In TOTanalysisROOT::Calculate()" << std::endl;

  // Do some sanity chacks on the data we got
  if (m_scan->GetEntries() == 0) {
    std::cout << "No modules to run on. Exiting." << std::endl;
    return;
  }
  // get some information about the scan (same for every module)
  if (((AnalysisInputTOT*)m_scan->At(0))->n_vcalsteps <= 0) {
    std::cout << "Number of Vcal steps is 0. Exiting!" << std::endl;
    return;
  }
  if (((AnalysisInputTOT*)m_scan->At(0))->n_maskstages <= 0) {
    std::cout << "Number of maskstages is 0. Exiting!" << std::endl;
    return;
  }
  if (((AnalysisInputTOT*)m_scan->At(0))->n_events <= 0) {
    std::cout << "Number of events is 0. Exiting!" << std::endl;
    return;
  }

  // Print some info
  // if (debug) m_scan->R__FOR_EACH(AnalysisInputTOT,PrintInfo)(); // crashes
  if (debug) std::cout << "these were the module names." << std::endl;  
  if (debug) std::cout << "Running on " << ((AnalysisInputTOT*)m_scan->At(0))->n_events << " events" << std::endl;
  if (debug) std::cout << "Running with " << ((AnalysisInputTOT*)m_scan->At(0))->n_maskstages << " maskstages" << std::endl;
  if (debug) std::cout << "Running with " << ((AnalysisInputTOT*)m_scan->At(0))->n_vcalsteps << " vcalsteps " << std::endl;

  // start looping over modules
  allPassed=true;
  for (Int_t i=0; i<m_scan->GetEntries(); i++) { // loop over modules on pp0
    std::shared_ptr<TOTanalysisROOT::ModuleData> module_data(new TOTanalysisROOT::ModuleData());
    module_data->module_input = (AnalysisInputTOT*)m_scan->At(i);
    module_data->module_result = new ResultTOT();
    module_data->module_result->pp0Name = module_data->module_input->pp0Name; // have to set this, used by the base class
    module_data->module_result->moduleName = module_data->module_input->moduleName; // have to set this, used by the base class
    module_data->module_name = module_data->module_input->moduleName; // looks useless, but now I have a std::string instead of a stupid TString
    module_data->charge = module_data->module_input->charge; // this is just plain copying, to have this vector on same level as the charge_err one
      _fe_disable = ((AnalysisInputTOT*)m_scan->At(i))->fe_disable;
      _cp_enable = ((AnalysisInputTOT*)m_scan->At(i))->cp_enable;

    if (module_data->charge.size() == 0) {
      std::cout << "Charge array has 0 size (was the module config read correctly?). Exiting!" << std::endl;
      return;
    }
    //module_data->charge_err.resize(helper->NfrontEnds,std::vector<float>(module_data->module_input->n_vcalsteps,0.)); // so far the charge error is always zero, maybe this will change in the future
    module_data->charge_err.resize(_Nfe,std::vector<float>(module_data->module_input->n_vcalsteps,0.)); // so far the charge error is always zero, maybe this will change in the future
    // sanity check on data
    if (module_data->module_input->histo_mean.size() != (unsigned int) module_data->module_input->n_vcalsteps || module_data->module_input->histo_sigma.size() != (unsigned int) module_data->module_input->n_vcalsteps ||
	module_data->module_input->histo_mean.size() == 0 || module_data->module_input->histo_sigma.size() == 0) {
      std::cout << "Missing histograms for module " << module_data->module_name << ". Skipping!" << std::endl;
      continue;
    }
    InitializeModuleResults(module_data);

    std::cout << "Processing module " << module_data->module_name << std::endl;

    // VECTORS TO STORE NUMBER OF OK PIXELS
    //module_data->n_masked_pixels.resize(helper->NfrontEnds, std::vector<int>(int(n_pixeltypes),0));
    //    module_data->n_healthy_pixels.resize(helper->NfrontEnds, std::vector<int>(int(n_pixeltypes),0));
    module_data->n_masked_pixels.resize(_Nfe, std::vector<int>(int(n_pixeltypes),0));
    module_data->n_healthy_pixels.resize(_Nfe, std::vector<int>(int(n_pixeltypes),0));
      

    // VECTORS TO STORE FIT RESULTS    
    //module_data->tot_fit_result.resize(helper->NfrontEnds, std::vector<std::shared_ptr<TF1> >(int(n_pixeltypes),std::shared_ptr<TF1>((TF1*) NULL)));
    //module_data->disp_fit_result.resize(helper->NfrontEnds, std::vector<std::shared_ptr<TF1> >(int(n_pixeltypes),std::shared_ptr<TF1>((TF1*) NULL)));
    module_data->tot_fit_result.resize(_Nfe, std::vector<std::shared_ptr<TF1> >(int(n_pixeltypes),std::shared_ptr<TF1>((TF1*) NULL)));
    module_data->disp_fit_result.resize(_Nfe, std::vector<std::shared_ptr<TF1> >(int(n_pixeltypes),std::shared_ptr<TF1>((TF1*) NULL)));


    //for (int fe=0; fe!=helper->NfrontEnds; ++fe) { // loop over front-ends on module
    for (int fe=0; fe!=_Nfe; ++fe) { // loop over front-ends on module
      //std::cout << "Looking at fe " << fe << std::endl;
      /*
      int start_col = helper->getFrontEndStartCol(fe);
      int stop_col = helper->getFrontEndStopCol(fe);
      int start_row = helper->getFrontEndStartRow(fe);
      int stop_row = helper->getFrontEndStopRow(fe);
      */
      int start_col = helper->getFrontEndStartCol(fe, _ChipType);
      int stop_col = helper->getFrontEndStopCol(fe, _ChipType);
      int start_row = helper->getFrontEndStartRow(fe, _ChipType);
      int stop_row = helper->getFrontEndStopRow(fe, _ChipType);
      for (int pixtype=0; pixtype!=int(n_pixeltypes); ++pixtype) { // loop over pixtypes

	// vectors which will be used for fitting
	std::vector<float> tot(module_data->module_input->n_vcalsteps,0.); // for per pixel fit (will be reused for each pixel)
	std::vector<float> tot_err(module_data->module_input->n_vcalsteps,0.); // for per pixel fit (will be reused for each pixel)
	std::vector<float> sigma(module_data->module_input->n_vcalsteps,0.); // for per pixel fit (will be reused for each pixel)
	std::vector<float> sigma_err(module_data->module_input->n_vcalsteps,0.); // for per pixel fit (will be reused for each pixel)
	std::vector<float> average_tot_mean(module_data->module_input->n_vcalsteps,0.); // for average fit
	std::vector<float> average_tot_mean_err(module_data->module_input->n_vcalsteps,0.); // for average fit
	std::vector<float> average_tot_sigma(module_data->module_input->n_vcalsteps,0.); // for average fit
	std::vector<float> average_tot_sigma_err(module_data->module_input->n_vcalsteps,0.); // for average fit
	std::vector<float> rms_tot_mean(module_data->module_input->n_vcalsteps,0.);
	std::vector<float> rms_tot_mean_err(module_data->module_input->n_vcalsteps,0.);
	std::vector<float> disp(module_data->module_input->n_vcalsteps,0.);
	std::vector<float> disp_err(module_data->module_input->n_vcalsteps,0.);
	std::vector <TH1F*> h_tot(module_data->module_input->n_vcalsteps,(TH1F*) NULL);
	std::vector <TH1F*> h_sigma(module_data->module_input->n_vcalsteps,(TH1F*) NULL);
	// for per pixel fit, to derive the average fit parameters
	std::vector<MeanSigmaDeriver> ms_tot_fit(4,MeanSigmaDeriver()); // last value is chi2
	//std::vector<MeanSigmaDeriver> ms_sigma_fit(3,MeanSigmaDeriver()); // last value is chi2
	for (int col=start_col; col!=stop_col+1; ++col) { // loop over columns of front-end
	  for (int row=start_row; row!=stop_row+1; ++row) { // loop over rows of front-end

	    if (!helper->isMasked(col,row,module_data->module_input->n_maskstages,module_data->module_input->nTotMaskStages,_ChipType)) continue; // only do the fit for masked pixels
          if (!_cp_enable[fe][helper->columnPair(col,row,_ChipType)] || _fe_disable[fe]) continue;

	    int pixeltype = -1;
	    if(_ChipType == PixGeometry::FEI2_CHIP || _ChipType == PixGeometry::FEI2_MODULE){
	      if (int(n_pixeltypes) == 6) pixeltype = helper->pixelTypeInt(col,row); // normal, long etc 
	      //if (int(n_pixeltypes) == 6) pixeltype = helper->pixelTypeInt(col,row, _ChipType); // normal, long etc 
	      else if (int(n_pixeltypes) == 3) pixeltype = int(helper->pixelClass(col,row,3))-1; // 3 groups
	      else if (int(n_pixeltypes) == 2) pixeltype = int(helper->pixelClass(col,row,2))-1; // 2 groups
	    }else if(_ChipType == PixGeometry::FEI4_CHIP || _ChipType == PixGeometry::FEI4_MODULE){
	      if (int(n_pixeltypes) == 1) pixeltype = helper->pixelTypeInt(col,row, _ChipType); // normal, long 
	    }
          
if (pixeltype != pixtype) continue; // VERY IMPORTANT, OTHERWISE WILL DOUBLE-COUNT!
          
	    ++(module_data->n_masked_pixels[fe][pixtype]);

	    for (int vcal=0; vcal!=module_data->module_input->n_vcalsteps; ++vcal) { // loop over vcal steps

	      if (module_data->n_masked_pixels[fe][pixtype] == 1) { // this is the first pixel for this fe/pixtype, so create the histgrams to hold raw data
		// create h_tot and h_sigma
		{
		  CAN::Lock lock(CAN::PixDbGlobalMutex::mutex());
		  char str[500];
		  sprintf(str,"h_tot_vcal%i",vcal);
		  h_tot[vcal] = new TH1F(str,str,1024,0.,256.); // holds all TOTs for a front-end and pixeltype

		  //h_tot[vcal]->SetDirectory(0);
		  sprintf(str,"h_sigma_vcal%i",vcal);
		  h_sigma[vcal] = new TH1F(str,str,1024,0.,16.); // holds all sigmas for a front-end and pixeltype
		  //h_sigma[vcal]->SetDirectory(0);
		}
	      }
	      float tot_mean = ((module_data->module_input->histo_mean)[vcal])->GetBinContent(col+1,row+1);
	      float tot_sigma = ((module_data->module_input->histo_sigma)[vcal])->GetBinContent(col+1,row+1);
	      tot[vcal] = tot_mean;
	      tot_err[vcal] = tot_sigma/TMath::Sqrt(float(module_data->module_input->n_events));
	      sigma[vcal] = tot_sigma;
	      sigma_err[vcal] = TMath::Sqrt((tot_sigma*tot_sigma)/(2.*float(module_data->module_input->n_events)));
	    }
	    bool healthy = isHealthyPixel(tot,sigma);
	    if (healthy) {
	      module_data->module_result->h2d_healthy->SetBinContent(col+1,row+1,1.);
	      module_data->n_healthy_pixels[fe][pixtype] += 1;
	      if (fit_each_pixel) { // FIT EACH PIXEL AND AVERAGE FIT PARAMETERS (NOT RECOMMENDED)
		std::shared_ptr<TF1> pix_tot_fit_result = PerformTotFitPerPixel(row,col,module_data->charge[fe],module_data->charge_err[fe],tot,tot_err);
		bool good_tot_fit = false;
		for (int par=0; par!=npars_tot_fit; ++par) {

		  if (pix_tot_fit_result->GetParameter(par) != 0.) good_tot_fit = true; 
		}
		if (good_tot_fit) {
		  for (int par=0; par!=npars_tot_fit; ++par) {

		    ms_tot_fit[par].AddValue(pix_tot_fit_result->GetParameter(par));
		  }
		  ms_tot_fit[3].AddValue(pix_tot_fit_result->GetChisquare());
		}
		if (dump_data) SavePerPixelClosureResults(fe,pixtype,module_data,pix_tot_fit_result,module_data->charge[fe],tot,tot_err);
		pix_tot_fit_result.reset();
	      }
	      // now we must loop over vcal again to fill the histograms for the mean calculation
	      // we need two loops over vcal since we only want to fill h_tot and h_sigma with healthy pixels
	      // and we need to look at all vcal steps to decide if a pixel is healthy
	      for (int vcal=0; vcal!=module_data->module_input->n_vcalsteps; ++vcal) { // loop over vcal steps
		float tot_mean = ((module_data->module_input->histo_mean)[vcal])->GetBinContent(col+1,row+1);
		float tot_sigma = ((module_data->module_input->histo_sigma)[vcal])->GetBinContent(col+1,row+1);
		h_tot[vcal]->Fill(tot_mean);
		h_sigma[vcal]->Fill(tot_sigma);
	      }
	    }
	  }
	}
	if (fit_each_pixel) { // FIT EACH PIXEL AND AVERAGE FIT PARAMETERS (NOT RECOMMENDED)
	  // Store the average of the fit parameters and the chi2
 	  char str[1000];
 	  sprintf(str,"totfu_fe%i_pixtype%i",fe,pixtype);
 	  module_data->tot_fit_result[fe][pixtype] = createTotFitFunction(str);
	  for (int par=0; par!=npars_tot_fit; ++par) {
	    module_data->tot_fit_result[fe][pixtype]->SetParameter(par,ms_tot_fit[par].GetMean());
	    module_data->tot_fit_result[fe][pixtype]->SetParError(par,ms_tot_fit[par].GetMeanError());
	  }
	  module_data->tot_fit_result[fe][pixtype]->SetChisquare(ms_tot_fit[3].GetMean());
	}
	for (int vcal=0; vcal!=module_data->module_input->n_vcalsteps; ++vcal) { // loop over vcal steps
	  if(h_tot[vcal]==NULL || h_sigma[vcal]==NULL)continue;
	  float average_tot = h_tot[vcal]->GetMean();
	  float average_sigma = h_sigma[vcal]->GetMean();
	  float rms_tot = h_tot[vcal]->GetRMS();
	  float rms_sigma = h_sigma[vcal]->GetRMS();
	  // Set axis range to +/- 3 sigma from mean to remove outliers in mean calculation
	  h_tot[vcal]->GetXaxis()->SetRangeUser(average_tot-3.*rms_tot,average_tot+3.*rms_tot);
	  h_sigma[vcal]->GetXaxis()->SetRangeUser(average_sigma-3.*rms_sigma,average_sigma+3.*rms_sigma);
	  average_tot_mean[vcal] = h_tot[vcal]->GetMean();
	  average_tot_mean_err[vcal] = h_tot[vcal]->GetMeanError();
	  average_tot_sigma[vcal] = h_sigma[vcal]->GetMean();
	  average_tot_sigma_err[vcal] = h_sigma[vcal]->GetMeanError();
	  rms_tot_mean[vcal] = h_tot[vcal]->GetRMS();
	  rms_tot_mean_err[vcal] = h_tot[vcal]->GetRMSError();
	  disp[vcal] = TMath::Sqrt(rms_tot_mean[vcal]*rms_tot_mean[vcal] + average_tot_sigma[vcal]*average_tot_sigma[vcal]); // new definition of dispersion (meaning comb. of sigma per pixel and rms over fe)
	  disp_err[vcal] = TMath::Sqrt(rms_tot_mean_err[vcal]*rms_tot_mean_err[vcal] + average_tot_sigma_err[vcal]*average_tot_sigma_err[vcal]);
          CAN::Lock lock(CAN::PixDbGlobalMutex::mutex());
	  delete h_tot[vcal];
	  delete h_sigma[vcal];
	}
	if (!fit_each_pixel) { // FIT USING AVERAGE TOT (RECOMMENDED)
	  // Fit of average ToT vs charge
	  module_data->tot_fit_result[fe][pixtype] = PerformTotFitOnAverage(fe,pixtype,module_data->n_healthy_pixels[fe][pixtype],module_data->charge[fe],module_data->charge_err[fe],average_tot_mean,average_tot_mean_err);
	}
          
          
//*****************************[maria elena] temporary outputs
/*          for(int v=0;v<21;v++){
              if(v==0)std::cout<<"tot={";
              std::cout<<average_tot_mean[v]<<",";
              if(v==20)std::cout<<"}"<<std::endl;
          }
          for(int v=0;v<21;v++){
              if(v==0)std::cout<<"tot_err={";
              std::cout<<average_tot_mean_err[v]<<",";
              if(v==20)std::cout<<"}"<<std::endl;
          }
          for(int v=0;v<21;v++){
              if(v==0)std::cout<<"charge={";
              std::cout<<module_data->charge[fe][v]<<",";
              if(v==20)std::cout<<"}"<<std::endl;
          }
          for(int v=0;v<21;v++){
              if(v==0)std::cout<<"charge_err={";
              std::cout<<module_data->charge_err[fe][v]<<",";
              if(v==20)std::cout<<"}"<<std::endl;
          }*/
//************************************************************
          
          
          
	SaveTOTClosureResults(fe,pixtype,module_data,average_tot_mean,average_tot_mean_err); // need to call it here cause I need the data that goes into the fit to do closure
	// Do Dispersion fit (cannot be done per pixel since it is dispersion over all pixels on fe/pixtype)
	if (fit_disp_vs_charge) {
	  module_data->disp_fit_result[fe][pixtype] = PerformDispersionFit(fe,pixtype,module_data->n_healthy_pixels[fe][pixtype],module_data->charge[fe],module_data->charge_err[fe],disp,disp_err);
	}
          
	else {
	  module_data->disp_fit_result[fe][pixtype] = PerformDispersionFit(fe,pixtype,module_data->n_healthy_pixels[fe][pixtype],average_tot_mean,average_tot_mean_err,disp,disp_err);
	}
          
//*****************************[maria elena] temporary outputs
/*          for(int v=0;v<21;v++){
              if(v==0)std::cout<<"disp={";
              std::cout<<disp[v]<<",";
              if(v==20)std::cout<<"}"<<std::endl;
          }
          for(int v=0;v<21;v++){
              if(v==0)std::cout<<"disp_err={";
              std::cout<<disp_err[v]<<",";
              if(v==20)std::cout<<"}"<<std::endl;
          }
          for(int v=0;v<21;v++){
              if(v==0)std::cout<<"charge={";
              std::cout<<module_data->charge[fe][v]<<",";
              if(v==20)std::cout<<"}"<<std::endl;
          }
          for(int v=0;v<21;v++){
              if(v==0)std::cout<<"charge_err={";
              std::cout<<module_data->charge_err[fe][v]<<",";
              if(v==20)std::cout<<"}"<<std::endl;
          }*/
//************************************************************
          
          
	SaveDispClosureResults(fe,pixtype,module_data,average_tot_mean,average_tot_mean_err,disp,disp_err);
      }
    }
    
    SaveResults(module_data);
    CAN::Lock lock(CAN::PixDbGlobalMutex::mutex());
    //delete module_data;
    module_data.reset();
  }
  if (debug) std::cout << "end of TOT analysis." << std::endl;  
}

void TOTanalysisROOT::InitializeModuleResults(std::shared_ptr<TOTanalysisROOT::ModuleData> module_data) {
  
  
  char str[800];
  
  // INITIALIZE CLOSURE HISTOGRAMS
  
  {
    CAN::Lock lock(CAN::PixDbGlobalMutex::mutex());
    
    // always created:
    // histograms always needed (used internally in pass/fail derivation or sent to the Console for display)
    // if dump_data flag only:    
    // histograms only needed when doing detailed analysis and TOTanalysisROOT is called from the AnalysisFramework
    
    sprintf(str,"h2d_healthy_%s",module_data->module_name.c_str());
    //module_data->module_result->h2d_healthy =  std::shared_ptr<TH2F>(new TH2F(str,str,helper->Ncols,-0.5,float(helper->Ncols)-0.5,helper->Nrows,-0.5,float(helper->Nrows)-0.5));
    module_data->module_result->h2d_healthy =  std::shared_ptr<TH2F>(new TH2F(str,str,_Ncol,-0.5,float(_Ncol)-0.5,_Nrow,-0.5,float(_Nrow)-0.5));
    module_data->module_result->h2d_healthy->SetDirectory(0);
    
    sprintf(str,"h2d_closure_perpixel_mean_tot_fit_%s",module_data->module_name.c_str());
    module_data->module_result->h2d_closure_perpixel_mean_tot_fit_module = std::shared_ptr<TH2F>(new TH2F(str,str,250,0.,50000.,250,0.,50000.));
    module_data->module_result->h2d_closure_perpixel_mean_tot_fit_module->SetDirectory(0);
    
    sprintf(str,"h2d_closure_pergroup_disp_fit_%s",module_data->module_name.c_str());
    module_data->module_result->h2d_closure_pergroup_disp_fit_module = std::shared_ptr<TH2F>(new TH2F(str,str,100,0.,10.,100,0.,10.));
    module_data->module_result->h2d_closure_pergroup_disp_fit_module->SetDirectory(0);
    
    sprintf(str,"h1d_closure_perpixel_mean_tot_fit_%s",module_data->module_name.c_str());
    module_data->module_result->h1d_closure_perpixel_mean_tot_fit_module = std::shared_ptr<TH1F>(new TH1F(str,str,500,-50000.,50000.));
    module_data->module_result->h1d_closure_perpixel_mean_tot_fit_module->SetDirectory(0);
    module_data->module_result->h1d_closure_perpixel_mean_tot_fit_module->GetXaxis()->SetTitle("Injected Charge-Predicted Charge");

    sprintf(str,"h1d_closure_pergroup_disp_fit_%s",module_data->module_name.c_str());
    module_data->module_result->h1d_closure_pergroup_disp_fit_module = std::shared_ptr<TH1F>(new TH1F(str,str,200,-10.,10.));
    module_data->module_result->h1d_closure_pergroup_disp_fit_module->SetDirectory(0);
    module_data->module_result->h1d_closure_pergroup_disp_fit_module->GetXaxis()->SetTitle("Calculated Dispersion-Predicted Dispersion");

    sprintf(str,"h2d_residual_vs_charge_perpixel_mean_tot_fit_%s",module_data->module_name.c_str());
    module_data->module_result->h2d_residual_vs_charge_perpixel_mean_tot_fit_module = std::shared_ptr<TH2F>(new TH2F(str,str,250,0.,50000.,250,-10000.,10000.));
    module_data->module_result->h2d_residual_vs_charge_perpixel_mean_tot_fit_module->SetDirectory(0);
    module_data->module_result->h2d_residual_vs_charge_perpixel_mean_tot_fit_module->GetXaxis()->SetTitle("Injected Charge");
    module_data->module_result->h2d_residual_vs_charge_perpixel_mean_tot_fit_module->GetYaxis()->SetTitle("Injected Charge-Predicted Charge");

    sprintf(str,"h2d_residual_vs_charge_perpixel_tot_fit_%s",module_data->module_name.c_str());
    module_data->module_result->h2d_residual_vs_charge_perpixel_tot_fit_module = std::shared_ptr<TH2F>(new TH2F(str,str,250,0.,50000.,250,-10000.,10000.));
    module_data->module_result->h2d_residual_vs_charge_perpixel_tot_fit_module->SetDirectory(0);
    module_data->module_result->h2d_residual_vs_charge_perpixel_tot_fit_module->GetXaxis()->SetTitle("Injected Charge");
    module_data->module_result->h2d_residual_vs_charge_perpixel_tot_fit_module->GetYaxis()->SetTitle("Injected Charge-Predicted Charge");

    sprintf(str,"h2d_residual_vs_mean_tot_pergroup_disp_fit_%s",module_data->module_name.c_str());
    module_data->module_result->h2d_residual_vs_mean_tot_pergroup_disp_fit_module = std::shared_ptr<TH2F>(new TH2F(str,str,255,0.,255.,250,-1.,1.));
    module_data->module_result->h2d_residual_vs_mean_tot_pergroup_disp_fit_module->SetDirectory(0);
    module_data->module_result->h2d_residual_vs_mean_tot_pergroup_disp_fit_module->GetXaxis()->SetTitle("Mean ToT");
    module_data->module_result->h2d_residual_vs_mean_tot_pergroup_disp_fit_module->GetYaxis()->SetTitle("Calculated Dispersion-Predicted Dispersion");

    if (dump_data) { // only for detailed analysis
      sprintf(str,"h2d_closure_perpixel_tot_fit_%s",module_data->module_name.c_str());
      module_data->module_result->h2d_closure_perpixel_tot_fit_module = std::shared_ptr<TH2F>(new TH2F(str,str,250,0.,50000.,250,0.,50000.));
      module_data->module_result->h2d_closure_perpixel_tot_fit_module->SetDirectory(0);
      
      sprintf(str,"h1d_closure_perpixel_tot_fit_%s",module_data->module_name.c_str());
      module_data->module_result->h1d_closure_perpixel_tot_fit_module = std::shared_ptr<TH1F>(new TH1F(str,str,500,-50000.,50000.));
      module_data->module_result->h1d_closure_perpixel_tot_fit_module->SetDirectory(0);
    }
    
    //for (int fe=0; fe!=helper->NfrontEnds; ++fe) {
    for (int fe=0; fe!=_Nfe; ++fe) {
      module_data->module_result->h2d_closure_perpixel_mean_tot_fit.push_back(std::vector<std::shared_ptr<TH2F> >());
      module_data->module_result->h2d_closure_pergroup_disp_fit.push_back(std::vector<std::shared_ptr<TH2F> >());
      module_data->module_result->h1d_closure_perpixel_mean_tot_fit.push_back(std::vector<std::shared_ptr<TH1F> >());
      module_data->module_result->h1d_closure_pergroup_disp_fit.push_back(std::vector<std::shared_ptr<TH1F> >());
     
      if (dump_data) { // only for detailed analysis
	module_data->module_result->h2d_closure_perpixel_tot_fit.push_back(std::vector<std::shared_ptr<TH2F> >());
	module_data->module_result->h1d_closure_perpixel_tot_fit.push_back(std::vector<std::shared_ptr<TH1F> >());
 
	module_data->module_result->h1d_closure_perpixel_pervcal_mean_tot_fit.push_back(std::vector<std::vector<std::shared_ptr<TH1F> > >());
	module_data->module_result->h1d_closure_pergroup_pervcal_mean_tot_fit.push_back(std::vector<std::vector<std::shared_ptr<TH1F> > >());
	module_data->module_result->h1d_closure_pergroup_pervcal_disp_fit.push_back(std::vector<std::vector<std::shared_ptr<TH1F> > >());
	
	module_data->module_result->h1d_residual_perpixel_pervcal_mean_tot_fit.push_back(std::vector<std::vector<std::shared_ptr<TH1F> > >());
	module_data->module_result->h1d_residual_pergroup_pervcal_mean_tot_fit.push_back(std::vector<std::vector<std::shared_ptr<TH1F> > >());
	module_data->module_result->h1d_residual_pergroup_pervcal_disp_fit.push_back(std::vector<std::vector<std::shared_ptr<TH1F> > >());
	
	module_data->module_result->h1d_pull_pergroup_pervcal_mean_tot_fit.push_back(std::vector<std::vector<std::shared_ptr<TH1F> > >());
	module_data->module_result->h1d_pull_pergroup_pervcal_disp_fit.push_back(std::vector<std::vector<std::shared_ptr<TH1F> > >());
	
	module_data->module_result->h1d_closure_vs_charge_perpixel_mean_tot_fit.push_back(std::vector<std::shared_ptr<TH2F> >());
	module_data->module_result->h1d_closure_vs_charge_pergroup_mean_tot_fit.push_back(std::vector<std::shared_ptr<TH2F> >());
	module_data->module_result->h1d_closure_vs_charge_perpixel_tot_fit.push_back(std::vector<std::shared_ptr<TH2F> >());
	module_data->module_result->h1d_closure_vs_charge_pergroup_disp_fit.push_back(std::vector<std::shared_ptr<TH2F> >());
	module_data->module_result->h1d_closure_vs_mean_tot_pergroup_disp_fit.push_back(std::vector<std::shared_ptr<TH2F> >());
	
	module_data->module_result->h1d_residual_vs_charge_perpixel_mean_tot_fit.push_back(std::vector<std::shared_ptr<TH2F> >());
	module_data->module_result->h1d_residual_vs_charge_pergroup_mean_tot_fit.push_back(std::vector<std::shared_ptr<TH2F> >());
	module_data->module_result->h1d_residual_vs_charge_perpixel_tot_fit.push_back(std::vector<std::shared_ptr<TH2F> >());
	module_data->module_result->h1d_residual_vs_charge_pergroup_disp_fit.push_back(std::vector<std::shared_ptr<TH2F> >());
	module_data->module_result->h1d_residual_vs_mean_tot_pergroup_disp_fit.push_back(std::vector<std::shared_ptr<TH2F> >());
	
	module_data->module_result->h1d_pull_vs_charge_perpixel_mean_tot_fit.push_back(std::vector<std::shared_ptr<TH2F> >());
	module_data->module_result->h1d_pull_vs_charge_pergroup_mean_tot_fit.push_back(std::vector<std::shared_ptr<TH2F> >());
	module_data->module_result->h1d_pull_vs_charge_perpixel_tot_fit.push_back(std::vector<std::shared_ptr<TH2F> >());
	module_data->module_result->h1d_pull_vs_charge_pergroup_disp_fit.push_back(std::vector<std::shared_ptr<TH2F> >());
	module_data->module_result->h1d_pull_vs_mean_tot_pergroup_disp_fit.push_back(std::vector<std::shared_ptr<TH2F> >());
      }

      
      for (int pixtype=0; pixtype!=int(n_pixeltypes); ++pixtype) {
	sprintf(str,"h2d_closure_perpixel_mean_tot_fit_%s_fe%i_pixtype%i",module_data->module_name.c_str(),fe,pixtype);
	module_data->module_result->h2d_closure_perpixel_mean_tot_fit[fe].push_back(std::shared_ptr<TH2F>(new TH2F(str,str,250,0.,50000.,250,0.,50000.)));
	module_data->module_result->h2d_closure_perpixel_mean_tot_fit[fe][pixtype]->SetDirectory(0);
	sprintf(str,"h2d_closure_pergroup_disp_fit_%s_fe%i_pixtype%i",module_data->module_name.c_str(),fe,pixtype);
	module_data->module_result->h2d_closure_pergroup_disp_fit[fe].push_back(std::shared_ptr<TH2F>(new TH2F(str,str,100,0.,10.,100,0.,10.)));
	module_data->module_result->h2d_closure_pergroup_disp_fit[fe][pixtype]->SetDirectory(0);
	sprintf(str,"h1d_closure_perpixel_mean_tot_fit_%s_fe%i_pixtype%i",module_data->module_name.c_str(),fe,pixtype);
	module_data->module_result->h1d_closure_perpixel_mean_tot_fit[fe].push_back(std::shared_ptr<TH1F>(new TH1F(str,str,500,-50000.,50000.)));
	module_data->module_result->h1d_closure_perpixel_mean_tot_fit[fe][pixtype]->SetDirectory(0);
	sprintf(str,"h1d_closure_pergroup_disp_fit_%s_fe%i_pixtype%i",module_data->module_name.c_str(),fe,pixtype);
	module_data->module_result->h1d_closure_pergroup_disp_fit[fe].push_back(std::shared_ptr<TH1F>(new TH1F(str,str,200,-10.,10.)));
	module_data->module_result->h1d_closure_pergroup_disp_fit[fe][pixtype]->SetDirectory(0);

	if (dump_data) { // only for detailed analysis
	  module_data->module_result->h1d_closure_perpixel_pervcal_mean_tot_fit[fe].push_back(std::vector<std::shared_ptr<TH1F> >());
	  module_data->module_result->h1d_closure_pergroup_pervcal_mean_tot_fit[fe].push_back(std::vector<std::shared_ptr<TH1F> >());
	  module_data->module_result->h1d_closure_pergroup_pervcal_disp_fit[fe].push_back(std::vector<std::shared_ptr<TH1F> >());
	  
	  module_data->module_result->h1d_residual_perpixel_pervcal_mean_tot_fit[fe].push_back(std::vector<std::shared_ptr<TH1F> >());
	  module_data->module_result->h1d_residual_pergroup_pervcal_mean_tot_fit[fe].push_back(std::vector<std::shared_ptr<TH1F> >());
	  module_data->module_result->h1d_residual_pergroup_pervcal_disp_fit[fe].push_back(std::vector<std::shared_ptr<TH1F> >());
	  
	  module_data->module_result->h1d_pull_pergroup_pervcal_mean_tot_fit[fe].push_back(std::vector<std::shared_ptr<TH1F> >());
	  module_data->module_result->h1d_pull_pergroup_pervcal_disp_fit[fe].push_back(std::vector<std::shared_ptr<TH1F> >());

	  sprintf(str,"h2d_closure_perpixel_tot_fit_%s_fe%i_pixtype%i",module_data->module_name.c_str(),fe,pixtype);
	  module_data->module_result->h2d_closure_perpixel_tot_fit[fe].push_back(std::shared_ptr<TH2F>(new TH2F(str,str,250,0.,50000.,250,0.,50000.)));
	  module_data->module_result->h2d_closure_perpixel_tot_fit[fe][pixtype]->SetDirectory(0);

	  sprintf(str,"h1d_closure_perpixel_tot_fit_%s_fe%i_pixtype%i",module_data->module_name.c_str(),fe,pixtype);
	  module_data->module_result->h1d_closure_perpixel_tot_fit[fe].push_back(std::shared_ptr<TH1F>(new TH1F(str,str,500,-50000.,50000.)));
	  module_data->module_result->h1d_closure_perpixel_tot_fit[fe][pixtype]->SetDirectory(0);

	  sprintf(str,"h1d_closure_vs_charge_perpixel_mean_tot_fit_%s_fe%i_pixtype%i",module_data->module_name.c_str(),fe,pixtype);
	  module_data->module_result->h1d_closure_vs_charge_perpixel_mean_tot_fit[fe].push_back(std::shared_ptr<TH2F>(new TH2F(str,str,250,0.,50000.,200,-10000.,10000.)));
	  module_data->module_result->h1d_closure_vs_charge_perpixel_mean_tot_fit[fe][pixtype]->SetDirectory(0);
	  sprintf(str,"h1d_closure_vs_charge_pergroup_mean_tot_fit_%s_fe%i_pixtype%i",module_data->module_name.c_str(),fe,pixtype);
	  module_data->module_result->h1d_closure_vs_charge_pergroup_mean_tot_fit[fe].push_back(std::shared_ptr<TH2F>(new TH2F(str,str,250,0.,50000.,200,-10000.,10000.)));
	  module_data->module_result->h1d_closure_vs_charge_pergroup_mean_tot_fit[fe][pixtype]->SetDirectory(0);
	  sprintf(str,"h1d_closure_vs_charge_perpixel_tot_fit_%s_fe%i_pixtype%i",module_data->module_name.c_str(),fe,pixtype);
	  module_data->module_result->h1d_closure_vs_charge_perpixel_tot_fit[fe].push_back(std::shared_ptr<TH2F>(new TH2F(str,str,250,0.,50000.,200,-10000.,10000.)));
	  module_data->module_result->h1d_closure_vs_charge_perpixel_tot_fit[fe][pixtype]->SetDirectory(0);
	  sprintf(str,"h1d_closure_vs_charge_pergroup_disp_fit_%s_fe%i_pixtype%i",module_data->module_name.c_str(),fe,pixtype);
	  module_data->module_result->h1d_closure_vs_charge_pergroup_disp_fit[fe].push_back(std::shared_ptr<TH2F>(new TH2F(str,str,250,0.,50000.,200,-5.,5.)));
	  module_data->module_result->h1d_closure_vs_charge_pergroup_disp_fit[fe][pixtype]->SetDirectory(0);
	  sprintf(str,"h1d_closure_vs_mean_tot_pergroup_disp_fit_%s_fe%i_pixtype%i",module_data->module_name.c_str(),fe,pixtype);
	  module_data->module_result->h1d_closure_vs_mean_tot_pergroup_disp_fit[fe].push_back(std::shared_ptr<TH2F>(new TH2F(str,str,250,0.,100.,200,-5.,5.)));
	  module_data->module_result->h1d_closure_vs_mean_tot_pergroup_disp_fit[fe][pixtype]->SetDirectory(0);
	  
	  sprintf(str,"h1d_residual_vs_charge_perpixel_mean_tot_fit_%s_fe%i_pixtype%i",module_data->module_name.c_str(),fe,pixtype);
	  module_data->module_result->h1d_residual_vs_charge_perpixel_mean_tot_fit[fe].push_back(std::shared_ptr<TH2F>(new TH2F(str,str,250,0.,50000.,200,-10.,10.)));
	  module_data->module_result->h1d_residual_vs_charge_perpixel_mean_tot_fit[fe][pixtype]->SetDirectory(0);
	  sprintf(str,"h1d_residual_vs_charge_pergroup_mean_tot_fit_%s_fe%i_pixtype%i",module_data->module_name.c_str(),fe,pixtype);
	  module_data->module_result->h1d_residual_vs_charge_pergroup_mean_tot_fit[fe].push_back(std::shared_ptr<TH2F>(new TH2F(str,str,250,0.,50000.,200,-10.,10.)));
	  module_data->module_result->h1d_residual_vs_charge_pergroup_mean_tot_fit[fe][pixtype]->SetDirectory(0);
	  sprintf(str,"h1d_residual_vs_charge_perpixel_tot_fit_%s_fe%i_pixtype%i",module_data->module_name.c_str(),fe,pixtype);
	  module_data->module_result->h1d_residual_vs_charge_perpixel_tot_fit[fe].push_back(std::shared_ptr<TH2F>(new TH2F(str,str,250,0.,50000.,200,-10.,10.)));
	  module_data->module_result->h1d_residual_vs_charge_perpixel_tot_fit[fe][pixtype]->SetDirectory(0);
	  sprintf(str,"h1d_residual_vs_charge_pergroup_disp_fit_%s_fe%i_pixtype%i",module_data->module_name.c_str(),fe,pixtype);
	  module_data->module_result->h1d_residual_vs_charge_pergroup_disp_fit[fe].push_back(std::shared_ptr<TH2F>(new TH2F(str,str,250,0.,50000.,200,-5.,5.)));
	  module_data->module_result->h1d_residual_vs_charge_pergroup_disp_fit[fe][pixtype]->SetDirectory(0);
	  sprintf(str,"h1d_residual_vs_mean_tot_pergroup_disp_fit_%s_fe%i_pixtype%i",module_data->module_name.c_str(),fe,pixtype);
	  module_data->module_result->h1d_residual_vs_mean_tot_pergroup_disp_fit[fe].push_back(std::shared_ptr<TH2F>(new TH2F(str,str,250,0.,100.,200,-5.,5.)));
	  module_data->module_result->h1d_residual_vs_mean_tot_pergroup_disp_fit[fe][pixtype]->SetDirectory(0);
	  
	  sprintf(str,"h1d_pull_vs_charge_perpixel_mean_tot_fit_%s_fe%i_pixtype%i",module_data->module_name.c_str(),fe,pixtype);
	  module_data->module_result->h1d_pull_vs_charge_perpixel_mean_tot_fit[fe].push_back(std::shared_ptr<TH2F>(new TH2F(str,str,250,0.,50000.,200,-50.,50.)));
	  module_data->module_result->h1d_pull_vs_charge_perpixel_mean_tot_fit[fe][pixtype]->SetDirectory(0);
	  sprintf(str,"h1d_pull_vs_charge_pergroup_mean_tot_fit_%s_fe%i_pixtype%i",module_data->module_name.c_str(),fe,pixtype);
	  module_data->module_result->h1d_pull_vs_charge_pergroup_mean_tot_fit[fe].push_back(std::shared_ptr<TH2F>(new TH2F(str,str,250,0.,50000.,200,-50.,50.)));
	  module_data->module_result->h1d_pull_vs_charge_pergroup_mean_tot_fit[fe][pixtype]->SetDirectory(0);
	  sprintf(str,"h1d_pull_vs_charge_perpixel_tot_fit_%s_fe%i_pixtype%i",module_data->module_name.c_str(),fe,pixtype);
	  module_data->module_result->h1d_pull_vs_charge_perpixel_tot_fit[fe].push_back(std::shared_ptr<TH2F>(new TH2F(str,str,250,0.,50000.,200,-50.,50.)));
	  module_data->module_result->h1d_pull_vs_charge_perpixel_tot_fit[fe][pixtype]->SetDirectory(0);
	  sprintf(str,"h1d_pull_vs_charge_pergroup_disp_fit_%s_fe%i_pixtype%i",module_data->module_name.c_str(),fe,pixtype);
	  module_data->module_result->h1d_pull_vs_charge_pergroup_disp_fit[fe].push_back(std::shared_ptr<TH2F>(new TH2F(str,str,250,0.,50000.,200,-50.,50.)));
	  module_data->module_result->h1d_pull_vs_charge_pergroup_disp_fit[fe][pixtype]->SetDirectory(0);
	  sprintf(str,"h1d_pull_vs_mean_tot_pergroup_disp_fit_%s_fe%i_pixtype%i",module_data->module_name.c_str(),fe,pixtype);
	  module_data->module_result->h1d_pull_vs_mean_tot_pergroup_disp_fit[fe].push_back(std::shared_ptr<TH2F>(new TH2F(str,str,250,0.,100.,200,-50.,50.)));
	  module_data->module_result->h1d_pull_vs_mean_tot_pergroup_disp_fit[fe][pixtype]->SetDirectory(0);
	  
	  for (int vcal=0; vcal!=module_data->module_input->n_vcalsteps;++vcal) {
	    sprintf(str,"h1d_closure_pergroup_pervcal_mean_tot_fit_%s_fe%i_pixtype%i_vcal%i",module_data->module_name.c_str(),fe,pixtype,vcal);
	    module_data->module_result->h1d_closure_pergroup_pervcal_mean_tot_fit[fe][pixtype].push_back(std::shared_ptr<TH1F>(new TH1F(str,str,500,-5000.,5000.)));
	    module_data->module_result->h1d_closure_pergroup_pervcal_mean_tot_fit[fe][pixtype][vcal]->SetDirectory(0);
	    sprintf(str,"h1d_closure_perpixel_pervcal_mean_tot_fit_%s_fe%i_pixtype%i_vcal%i",module_data->module_name.c_str(),fe,pixtype,vcal);
	    module_data->module_result->h1d_closure_perpixel_pervcal_mean_tot_fit[fe][pixtype].push_back(std::shared_ptr<TH1F>(new TH1F(str,str,500,-50000.,50000.)));
	    module_data->module_result->h1d_closure_perpixel_pervcal_mean_tot_fit[fe][pixtype][vcal]->SetDirectory(0);
	    sprintf(str,"h1d_closure_pergroup_pervcal_disp_fit_%s_fe%i_pixtype%i_vcal%i",module_data->module_name.c_str(),fe,pixtype,vcal);
	    module_data->module_result->h1d_closure_pergroup_pervcal_disp_fit[fe][pixtype].push_back(std::shared_ptr<TH1F>(new TH1F(str,str,500,-10.,10.)));
	    module_data->module_result->h1d_closure_pergroup_pervcal_disp_fit[fe][pixtype][vcal]->SetDirectory(0);
	    sprintf(str,"h1d_residual_perpixel_pervcal_mean_tot_fit_%s_fe%i_pixtype%i_vcal%i",module_data->module_name.c_str(),fe,pixtype,vcal);
	    module_data->module_result->h1d_residual_perpixel_pervcal_mean_tot_fit[fe][pixtype].push_back(std::shared_ptr<TH1F>(new TH1F(str,str,500,-10.,10.)));
	    module_data->module_result->h1d_residual_perpixel_pervcal_mean_tot_fit[fe][pixtype][vcal]->SetDirectory(0);
	    sprintf(str,"h1d_residual_pergroup_pervcal_mean_tot_fit_%s_fe%i_pixtype%i_vcal%i",module_data->module_name.c_str(),fe,pixtype,vcal);
	    module_data->module_result->h1d_residual_pergroup_pervcal_mean_tot_fit[fe][pixtype].push_back(std::shared_ptr<TH1F>(new TH1F(str,str,500,-10.,10.)));
	    module_data->module_result->h1d_residual_pergroup_pervcal_mean_tot_fit[fe][pixtype][vcal]->SetDirectory(0);
	    sprintf(str,"h1d_residual_pergroup_pervcal_disp_fit_%s_fe%i_pixtype%i_vcal%i",module_data->module_name.c_str(),fe,pixtype,vcal);
	    module_data->module_result->h1d_residual_pergroup_pervcal_disp_fit[fe][pixtype].push_back(std::shared_ptr<TH1F>(new TH1F(str,str,500,-10.,10.)));
	    module_data->module_result->h1d_residual_pergroup_pervcal_disp_fit[fe][pixtype][vcal]->SetDirectory(0);
	    sprintf(str,"h1d_pull_pergroup_pervcal_mean_tot_fit_%s_fe%i_pixtype%i_vcal%i",module_data->module_name.c_str(),fe,pixtype,vcal);
	    module_data->module_result->h1d_pull_pergroup_pervcal_mean_tot_fit[fe][pixtype].push_back(std::shared_ptr<TH1F>(new TH1F(str,str,500,-50.,50.)));
	    module_data->module_result->h1d_pull_pergroup_pervcal_mean_tot_fit[fe][pixtype][vcal]->SetDirectory(0);
	    sprintf(str,"h1d_pull_pergroup_pervcal_disp_fit_%s_fe%i_pixtype%i_vcal%i",module_data->module_name.c_str(),fe,pixtype,vcal);
	    module_data->module_result->h1d_pull_pergroup_pervcal_disp_fit[fe][pixtype].push_back(std::shared_ptr<TH1F>(new TH1F(str,str,500,-50.,50.)));
	    module_data->module_result->h1d_pull_pergroup_pervcal_disp_fit[fe][pixtype][vcal]->SetDirectory(0);
	  }
	}
      }
    }
  }
  
}

void  TOTanalysisROOT::SavePerPixelClosureResults(int fe, int pixtype, std::shared_ptr<TOTanalysisROOT::ModuleData> module_data, std::shared_ptr<TF1> fit, std::vector<float> v_charge, std::vector<float> v_tot, std::vector<float> v_tot_err) {

  char str[500];

  sprintf(str,"invtotfu_fe%i_pixtype%i",fe,pixtype);
  std::shared_ptr<TF1> tot_inv_fit_result = createTotInvFitFunction(str);
  for (int par=0; par!=npars_tot_fit; ++par) {
    tot_inv_fit_result->SetParameter(par,fit->GetParameter(par));
  }

  for (int vcal = 0; vcal !=module_data->module_input->n_vcalsteps; ++vcal) { // loop over vcal steps
    
    float charge_inj = v_charge[vcal];
    float tot = v_tot[vcal];
    float tot_err = v_tot_err[vcal];
    float tot_pred =  fit->Eval(charge_inj);
    float charge_pred = tot_inv_fit_result->Eval(tot);
    
    module_data->module_result->h1d_closure_perpixel_tot_fit_module->Fill(charge_inj-charge_pred);
    module_data->module_result->h2d_residual_vs_charge_perpixel_tot_fit_module->Fill(charge_inj,charge_inj-charge_pred);
    module_data->module_result->h2d_closure_perpixel_tot_fit[fe][pixtype]->Fill(charge_inj,charge_pred);
    module_data->module_result->h1d_closure_perpixel_tot_fit[fe][pixtype]->Fill(charge_inj-charge_pred);
    module_data->module_result->h1d_closure_vs_charge_perpixel_tot_fit[fe][pixtype]->Fill(charge_inj,charge_inj-charge_pred);
    module_data->module_result->h1d_residual_vs_charge_perpixel_tot_fit[fe][pixtype]->Fill(charge_inj,tot-tot_pred);
    if (tot_err != 0.) module_data->module_result->h1d_pull_vs_charge_perpixel_tot_fit[fe][pixtype]->Fill(charge_inj,(tot-tot_pred)/tot_err);
    
  }
  
}

void TOTanalysisROOT::SaveTOTClosureResults(int fe, int pixtype, std::shared_ptr<TOTanalysisROOT::ModuleData> module_data, std::vector<float> v_mean_tot, std::vector<float> v_mean_tot_err) {

  if (debug) std::cout << "In SaveTOTSigmaClosureResults" << std::endl;

  char str[500];
  /*  
  int start_col = helper->getFrontEndStartCol(fe);
  int stop_col = helper->getFrontEndStopCol(fe);
  int start_row = helper->getFrontEndStartRow(fe);
  int stop_row = helper->getFrontEndStopRow(fe);
  */
  int start_col = helper->getFrontEndStartCol(fe, _ChipType);
  int stop_col = helper->getFrontEndStopCol(fe, _ChipType);
  int start_row = helper->getFrontEndStartRow(fe, _ChipType);
  int stop_row = helper->getFrontEndStopRow(fe, _ChipType);

  sprintf(str,"invtotfu_fe%i_pixtype%i",fe,pixtype);
  std::shared_ptr<TF1> tot_inv_fit_result = createTotInvFitFunction(str);
  for (int par=0; par!=npars_tot_fit; ++par) {
    tot_inv_fit_result->SetParameter(par,module_data->tot_fit_result[fe][pixtype]->GetParameter(par));
  }
  for (int vcal = 0; vcal !=module_data->module_input->n_vcalsteps; ++vcal) { // loop over vcal steps
    for (int col=start_col; col!=stop_col+1; ++col) { // loop over columns of front-end
      for (int row=start_row; row!=stop_row+1; ++row) { // loop over rows of front-end
	if (!helper->isMasked(col,row,module_data->module_input->n_maskstages,module_data->module_input->nTotMaskStages,_ChipType)) continue; // only do the fit for masked pixels
          if (!_cp_enable[fe][helper->columnPair(col,row,_ChipType)] || _fe_disable[fe]) continue;

	if (module_data->module_result->h2d_healthy->GetBinContent(col+1,row+1) != 1.) continue;
/*	int pixeltype = -1;
	if(_ChipType == PixGeometry::FEI2_CHIP || _ChipType == PixGeometry::FEI2_MODULE){
	  if (int(n_pixeltypes) == 6) pixeltype = helper->pixelTypeInt(col,row); // normal, long etc 
	  else if (int(n_pixeltypes) == 3) pixeltype = int(helper->pixelClass(col,row,3))-1; // 3 groups
	  else if (int(n_pixeltypes) == 2) pixeltype = int(helper->pixelClass(col,row,2))-1; // 3 groups
	}else if(_ChipType == PixGeometry::FEI4_CHIP || _ChipType == PixGeometry::FEI4_MODULE){
	  if (int(n_pixeltypes) == 1) pixeltype = helper->pixelTypeInt(col,row, _ChipType); // normal, long etc 
	} */

    float charge_inj = module_data->charge[fe][vcal];
	float tot = ((module_data->module_input->histo_mean)[vcal])->GetBinContent(col+1,row+1);
	float tot_err = ((module_data->module_input->histo_sigma)[vcal])->GetBinContent(col+1,row+1)/TMath::Sqrt(module_data->module_input->n_events);
	float tot_pred_mean =  module_data->tot_fit_result[fe][pixtype]->Eval(charge_inj);
	float charge_pred_mean = tot_inv_fit_result->Eval(tot);
	module_data->module_result->h2d_closure_perpixel_mean_tot_fit_module->Fill(charge_inj,charge_pred_mean);
	module_data->module_result->h1d_closure_perpixel_mean_tot_fit_module->Fill(charge_inj-charge_pred_mean);
	module_data->module_result->h2d_residual_vs_charge_perpixel_mean_tot_fit_module->Fill(charge_inj,charge_inj-charge_pred_mean);
	module_data->module_result->h2d_closure_perpixel_mean_tot_fit[fe][pixtype]->Fill(charge_inj,charge_pred_mean);
          
	module_data->module_result->h1d_closure_perpixel_mean_tot_fit[fe][pixtype]->Fill(charge_inj-charge_pred_mean);
 
//*******************[maria elena] temporary output
 /*         std::cout<<"disp - pred_disp = "<<charge_inj<<" - "<<charge_pred_mean<<" = "<<charge_inj-charge_pred_mean<<std::endl;*/
//****************************************************
          
          
	if (dump_data) {
	  module_data->module_result->h1d_closure_perpixel_pervcal_mean_tot_fit[fe][pixtype][vcal]->Fill(charge_inj-charge_pred_mean);
	  module_data->module_result->h1d_residual_perpixel_pervcal_mean_tot_fit[fe][pixtype][vcal]->Fill(tot-tot_pred_mean);
	  module_data->module_result->h1d_closure_vs_charge_perpixel_mean_tot_fit[fe][pixtype]->Fill(charge_inj,charge_inj-charge_pred_mean);
	  module_data->module_result->h1d_residual_vs_charge_perpixel_mean_tot_fit[fe][pixtype]->Fill(charge_inj,tot-tot_pred_mean);
	  module_data->module_result->h1d_pull_vs_charge_perpixel_mean_tot_fit[fe][pixtype]->Fill(charge_inj,(tot-tot_pred_mean)/tot_err);
	}
      }
    }
    if (dump_data) {
      float charge_inj = module_data->charge[fe][vcal];
      float mean_tot = v_mean_tot[vcal];
      float mean_tot_err = v_mean_tot_err[vcal];
      float tot_pred_mean = module_data->tot_fit_result[fe][pixtype]->Eval(charge_inj);
      float charge_pred_mean = tot_inv_fit_result->Eval(mean_tot);
      module_data->module_result->h1d_closure_pergroup_pervcal_mean_tot_fit[fe][pixtype][vcal]->Fill(charge_pred_mean-charge_inj);
      module_data->module_result->h1d_residual_pergroup_pervcal_mean_tot_fit[fe][pixtype][vcal]->Fill(mean_tot-tot_pred_mean);
      if (mean_tot_err != 0.) module_data->module_result->h1d_pull_pergroup_pervcal_mean_tot_fit[fe][pixtype][vcal]->Fill((mean_tot-tot_pred_mean)/mean_tot_err);
      module_data->module_result->h1d_closure_vs_charge_pergroup_mean_tot_fit[fe][pixtype]->Fill(charge_inj,charge_inj-charge_pred_mean);
      module_data->module_result->h1d_residual_vs_charge_pergroup_mean_tot_fit[fe][pixtype]->Fill(charge_inj,mean_tot-tot_pred_mean);
      if (mean_tot_err != 0.) module_data->module_result->h1d_pull_vs_charge_pergroup_mean_tot_fit[fe][pixtype]->Fill(charge_inj,(mean_tot-tot_pred_mean)/mean_tot_err);
    }
  }
  CAN::Lock lock(CAN::PixDbGlobalMutex::mutex());
  tot_inv_fit_result.reset();
}

void TOTanalysisROOT::SaveDispClosureResults(int fe, int pixtype, std::shared_ptr<TOTanalysisROOT::ModuleData> module_data, std::vector<float> v_mean_tot, std::vector<float> v_mean_tot_err, std::vector<float> v_disp, std::vector<float> v_disp_err) {

  for (int vcal = 0; vcal !=module_data->module_input->n_vcalsteps; ++vcal) { // loop over vcal steps
    float charge_inj = module_data->charge[fe][vcal];
    float mean_tot = v_mean_tot[vcal];
    float disp = v_disp[vcal];
    float disp_err = v_disp_err[vcal];
    float pred_disp = 0.;
    if (fit_disp_vs_charge) pred_disp = module_data->disp_fit_result[fe][pixtype]->Eval(charge_inj);
    else pred_disp = module_data->disp_fit_result[fe][pixtype]->Eval(mean_tot);
  
    module_data->module_result->h2d_closure_pergroup_disp_fit_module->Fill(disp,pred_disp);
    module_data->module_result->h1d_closure_pergroup_disp_fit_module->Fill(disp-pred_disp);
    module_data->module_result->h2d_residual_vs_mean_tot_pergroup_disp_fit_module->Fill(mean_tot,disp-pred_disp);
    module_data->module_result->h2d_closure_pergroup_disp_fit[fe][pixtype]->Fill(disp,pred_disp);
    module_data->module_result->h1d_closure_pergroup_disp_fit[fe][pixtype]->Fill(disp-pred_disp);
      
//*******************[maria elena] temporary output
/*      std::cout<<"disp - pred_disp = "<<disp<<" - "<<pred_disp<<" = "<<disp-pred_disp<<std::endl;*/
//****************************************************
      
    if (dump_data) {
      module_data->module_result->h1d_closure_pergroup_pervcal_disp_fit[fe][pixtype][vcal]->Fill(disp-pred_disp);
      module_data->module_result->h1d_residual_pergroup_pervcal_disp_fit[fe][pixtype][vcal]->Fill(disp-pred_disp);
      if (disp_err != 0.) module_data->module_result->h1d_pull_pergroup_pervcal_disp_fit[fe][pixtype][vcal]->Fill((disp-pred_disp)/disp_err);
  
      module_data->module_result->h1d_closure_vs_charge_pergroup_disp_fit[fe][pixtype]->Fill(charge_inj,disp-pred_disp);
      module_data->module_result->h1d_residual_vs_charge_pergroup_disp_fit[fe][pixtype]->Fill(charge_inj,disp-pred_disp);
      if (disp_err != 0.) module_data->module_result->h1d_pull_vs_charge_pergroup_disp_fit[fe][pixtype]->Fill(charge_inj,(disp-pred_disp)/disp_err);
      module_data->module_result->h1d_closure_vs_mean_tot_pergroup_disp_fit[fe][pixtype]->Fill(mean_tot,disp-pred_disp);
      module_data->module_result->h1d_residual_vs_mean_tot_pergroup_disp_fit[fe][pixtype]->Fill(mean_tot,disp-pred_disp);
      if (disp_err != 0.) module_data->module_result->h1d_pull_vs_mean_tot_pergroup_disp_fit[fe][pixtype]->Fill(mean_tot,(disp-pred_disp)/disp_err);
    }
  }
  
}

void TOTanalysisROOT::SaveResults(std::shared_ptr<TOTanalysisROOT::ModuleData> module_data) {
  
  if (debug) std::cout << "In SaveResults()" << std::endl;
  
  // calculate the mean and dispersion of each of the three fit parameters - per FE and pixel type
  // this will be part of the pass/fail decision
  //module_data->module_result->Status = false;
  module_data->module_result->passed = true;
  module_data->module_result->good_tuning = false;
  module_data->module_result->good_pixels = true;
  module_data->module_result->good_closure = true;
  module_data->module_result->tot_mip = 0.;
  module_data->module_result->disp_mip = 0.;
  module_data->module_result->frac_bad_tot_fit_closure = 0.; //new
  module_data->module_result->frac_bad_disp_fit_closure = 0.; // new
  module_data->module_result->charge_corr = 0.; //new
  module_data->module_result->n_passed_fe_pixtype = 0.;
  module_data->module_result->fe_pixtype_passed.resize(_Nfe, std::vector<unsigned short int>(int(n_pixeltypes),1));
  module_data->module_result->fe_pixtype_closure_failed_mask.resize(_Nfe, std::vector<UInt_t>(int(n_pixeltypes),0));
  module_data->module_result->fe_pixtype_badpixel_failed_mask.resize(_Nfe, std::vector<UInt_t>(int(n_pixeltypes),0));
  module_data->module_result->fe_pixtype_tot.resize(npars_tot_fit_max,std::vector<std::vector<Float_t> >(_Nfe, std::vector<Float_t>(int(n_pixeltypes),0.)));
  module_data->module_result->fe_pixtype_tot_err.resize(npars_tot_fit_max,std::vector<std::vector<Float_t> >(_Nfe, std::vector<Float_t>(int(n_pixeltypes),0.)));
  module_data->module_result->fe_pixtype_tot_chi2.resize(_Nfe, std::vector<Float_t>(int(n_pixeltypes),0.));
  module_data->module_result->fe_pixtype_disp.resize(npars_disp_fit_max,std::vector<std::vector<Float_t> >(_Nfe, std::vector<Float_t>(int(n_pixeltypes),0.)));
  module_data->module_result->fe_pixtype_disp_err.resize(npars_disp_fit_max,std::vector<std::vector<Float_t> >(_Nfe, std::vector<Float_t>(int(n_pixeltypes),0.)));
  module_data->module_result->fe_pixtype_disp_chi2.resize(_Nfe, std::vector<Float_t>(int(n_pixeltypes),0.));
  module_data->module_result->fe_pixtype_n_bad_pixels.resize(_Nfe, std::vector<UInt_t>(int(n_pixeltypes),0));
  module_data->module_result->fe_pixtype_frac_bad_pixels.resize(_Nfe, std::vector<Float_t>(int(n_pixeltypes),0.));
  module_data->module_result->fe_pixtype_tot_mip.resize(_Nfe, std::vector<Float_t>(int(n_pixeltypes),0.));
  module_data->module_result->fe_pixtype_disp_mip.resize(_Nfe, std::vector<Float_t>(int(n_pixeltypes),0.));
  module_data->module_result->fe_pixtype_charge_corr.resize(_Nfe, std::vector<Float_t>(int(n_pixeltypes),0.));
  module_data->module_result->fe_pixtype_frac_bad_tot_fit_closure.resize(_Nfe, std::vector<Float_t>(int(n_pixeltypes),0.)); 
  module_data->module_result->fe_pixtype_frac_bad_disp_fit_closure.resize(_Nfe, std::vector<Float_t>(int(n_pixeltypes),0.)); 
  /*
    module_data->module_result->fe_pixtype_passed.resize(helper->NfrontEnds, std::vector<unsigned short int>(int(n_pixeltypes),1));
    module_data->module_result->fe_pixtype_closure_failed_mask.resize(helper->NfrontEnds, std::vector<UInt_t>(int(n_pixeltypes),0));
    module_data->module_result->fe_pixtype_badpixel_failed_mask.resize(helper->NfrontEnds, std::vector<UInt_t>(int(n_pixeltypes),0));
    module_data->module_result->fe_pixtype_tot.resize(npars_tot_fit_max,std::vector<std::vector<Float_t> >(helper->NfrontEnds, std::vector<Float_t>(int(n_pixeltypes),0.)));
    module_data->module_result->fe_pixtype_tot_err.resize(npars_tot_fit_max,std::vector<std::vector<Float_t> >(helper->NfrontEnds, std::vector<Float_t>(int(n_pixeltypes),0.)));
    module_data->module_result->fe_pixtype_tot_chi2.resize(helper->NfrontEnds, std::vector<Float_t>(int(n_pixeltypes),0.));
    module_data->module_result->fe_pixtype_disp.resize(npars_disp_fit_max,std::vector<std::vector<Float_t> >(helper->NfrontEnds, std::vector<Float_t>(int(n_pixeltypes),0.)));
    module_data->module_result->fe_pixtype_disp_err.resize(npars_disp_fit_max,std::vector<std::vector<Float_t> >(helper->NfrontEnds, std::vector<Float_t>(int(n_pixeltypes),0.)));
    module_data->module_result->fe_pixtype_disp_chi2.resize(helper->NfrontEnds, std::vector<Float_t>(int(n_pixeltypes),0.));
    module_data->module_result->fe_pixtype_n_bad_pixels.resize(helper->NfrontEnds, std::vector<UInt_t>(int(n_pixeltypes),0));
    module_data->module_result->fe_pixtype_frac_bad_pixels.resize(helper->NfrontEnds, std::vector<Float_t>(int(n_pixeltypes),0.));
    module_data->module_result->fe_pixtype_tot_mip.resize(helper->NfrontEnds, std::vector<Float_t>(int(n_pixeltypes),0.));
    module_data->module_result->fe_pixtype_disp_mip.resize(helper->NfrontEnds, std::vector<Float_t>(int(n_pixeltypes),0.));
    module_data->module_result->fe_pixtype_charge_corr.resize(helper->NfrontEnds, std::vector<Float_t>(int(n_pixeltypes),0.));
    module_data->module_result->fe_pixtype_frac_bad_tot_fit_closure.resize(helper->NfrontEnds, std::vector<Float_t>(int(n_pixeltypes),0.)); 
    module_data->module_result->fe_pixtype_frac_bad_disp_fit_closure.resize(helper->NfrontEnds, std::vector<Float_t>(int(n_pixeltypes),0.)); 
  */
  float sum_tot_mip = 0.;
  float sum_disp_mip = 0.;
  float sum_frac_bad_tot_fit_closure = 0.; //new
  float sum_frac_bad_disp_fit_closure = 0.; // new
  float sum_charge_corr = 0.; //new
  int n_not_disabled_fe_pixtype = 0;
  
  //  for (int fe=0; fe!=helper->NfrontEnds; ++fe) { // loop over FEs
  for (int fe=0; fe!=_Nfe; ++fe) { // loop over FEs
    for (int pixtype=0; pixtype!=int(n_pixeltypes); ++pixtype) { // loop over pixtypes
    // fraction of bad pixels
      if (module_data->n_masked_pixels[fe][pixtype] != 0.) module_data->module_result->fe_pixtype_frac_bad_pixels[fe][pixtype] = (1. - (float(module_data->n_healthy_pixels[fe][pixtype])/float(module_data->n_masked_pixels[fe][pixtype])))*100.;
      // number of bad pixels
      module_data->module_result->fe_pixtype_n_bad_pixels[fe][pixtype] =  module_data->n_masked_pixels[fe][pixtype] - module_data->n_healthy_pixels[fe][pixtype];
      bool disabled_fe = module_data->module_result->fe_pixtype_frac_bad_pixels[fe][pixtype] == 100.;
      // paramaters of tot fit
      for (int par=0; par!=npars_tot_fit; ++par) {
	module_data->module_result->fe_pixtype_tot[par][fe][pixtype] = module_data->tot_fit_result[fe][pixtype]->GetParameter(par);
	module_data->module_result->fe_pixtype_tot_err[par][fe][pixtype] = module_data->tot_fit_result[fe][pixtype]->GetParError(par);
      }
      module_data->module_result->fe_pixtype_tot_chi2[fe][pixtype] = module_data->tot_fit_result[fe][pixtype]->GetChisquare();
      // parameters of disp fit
      for (int par=0; par!=npars_disp_fit; ++par) {
	module_data->module_result->fe_pixtype_disp[par][fe][pixtype] = module_data->disp_fit_result[fe][pixtype]->GetParameter(par);
	module_data->module_result->fe_pixtype_disp_err[par][fe][pixtype] = module_data->disp_fit_result[fe][pixtype]->GetParError(par);
      }
      module_data->module_result->fe_pixtype_disp_chi2[fe][pixtype] = module_data->disp_fit_result[fe][pixtype]->GetChisquare();
      // tot and disp of mip
      float tot_mip = module_data->tot_fit_result[fe][pixtype]->Eval(pred_mip);
      module_data->module_result->fe_pixtype_tot_mip[fe][pixtype] = tot_mip;
      float disp_mip = 0.;
      if (fit_disp_vs_charge) disp_mip = module_data->disp_fit_result[fe][pixtype]->Eval(pred_mip);
      else disp_mip = module_data->disp_fit_result[fe][pixtype]->Eval(module_data->module_result->fe_pixtype_tot_mip[fe][pixtype]);
      module_data->module_result->fe_pixtype_disp_mip[fe][pixtype] = disp_mip;
      // closure test
      module_data->module_result->fe_pixtype_charge_corr[fe][pixtype] = 100.*module_data->module_result->h2d_closure_perpixel_mean_tot_fit[fe][pixtype]->GetCorrelationFactor(1,2);
      module_data->module_result->fe_pixtype_frac_bad_tot_fit_closure[fe][pixtype] = 
	helper->PercentFailedMinCut(module_data->module_result->h1d_closure_perpixel_mean_tot_fit[fe][pixtype],min_tot_fit_closure) + 
	helper->PercentFailedMaxCut(module_data->module_result->h1d_closure_perpixel_mean_tot_fit[fe][pixtype],max_tot_fit_closure);
                
      module_data->module_result->fe_pixtype_frac_bad_disp_fit_closure[fe][pixtype] = 
	helper->PercentFailedMinCut(module_data->module_result->h1d_closure_pergroup_disp_fit[fe][pixtype],min_disp_fit_closure) + 
	helper->PercentFailedMaxCut(module_data->module_result->h1d_closure_pergroup_disp_fit[fe][pixtype],max_disp_fit_closure);

      if (!disabled_fe) {
	sum_tot_mip += module_data->module_result->fe_pixtype_tot_mip[fe][pixtype];
	sum_disp_mip += module_data->module_result->fe_pixtype_disp_mip[fe][pixtype];
	sum_frac_bad_tot_fit_closure += module_data->module_result->fe_pixtype_frac_bad_tot_fit_closure[fe][pixtype];
	sum_frac_bad_disp_fit_closure += module_data->module_result->fe_pixtype_frac_bad_disp_fit_closure[fe][pixtype];
	sum_charge_corr += module_data->module_result->fe_pixtype_charge_corr[fe][pixtype];
	++n_not_disabled_fe_pixtype;
      }
      
      // ** MAKE FINAL CUTS **

      // cuts on fit closure test results
      if (module_data->module_result->fe_pixtype_charge_corr[fe][pixtype] <= min_corr) {
          
helper->SetBit(module_data->module_result->fe_pixtype_closure_failed_mask[fe][pixtype],0);
                   
	if (debug) std::cout << "Failed charge corr cut" << std::endl;
      }
      if (module_data->module_result->fe_pixtype_frac_bad_tot_fit_closure[fe][pixtype] > max_frac_bad_tot_fit_closure) {
          
	helper->SetBit(module_data->module_result->fe_pixtype_closure_failed_mask[fe][pixtype],1);

    if (debug) std::cout << "Failed tot closure cut" << std::endl;
      }
      if (module_data->module_result->fe_pixtype_frac_bad_disp_fit_closure[fe][pixtype] > max_frac_bad_disp_fit_closure) {
          
	helper->SetBit(module_data->module_result->fe_pixtype_closure_failed_mask[fe][pixtype],2);

	if (debug) std::cout << "Failed disp closure cut" << std::endl;
      }
 
      // cuts on fraction of bad pixels
      if (module_data->module_result->fe_pixtype_frac_bad_pixels[fe][pixtype] > max_frac_bad_pixels &&
          module_data->module_result->fe_pixtype_n_bad_pixels[fe][pixtype] > max_n_bad_pixels){
          
helper->SetBit(module_data->module_result->fe_pixtype_badpixel_failed_mask[fe][pixtype],0);
        }

      // check if a fe/pixeltype and module passed all cuts
      if (!disabled_fe) {
	if (module_data->module_result->fe_pixtype_closure_failed_mask[fe][pixtype] != 0) module_data->module_result->good_closure = false;
	if (module_data->module_result->fe_pixtype_badpixel_failed_mask[fe][pixtype] != 0) module_data->module_result->good_pixels = false;
      }
      
      if (module_data->module_result->fe_pixtype_closure_failed_mask[fe][pixtype] != 0 || 
	  module_data->module_result->fe_pixtype_badpixel_failed_mask[fe][pixtype] != 0) {
	module_data->module_result->fe_pixtype_passed[fe][pixtype] = 0;
	if (!disabled_fe) { // to make disabled FEs not fail module
	  module_data->module_result->passed = false;
	  allPassed = false;
	}
      }
      else {
	module_data->module_result->n_passed_fe_pixtype += 1.;
      }
    }
  }
  module_data->module_result->tot_mip = sum_tot_mip/(float(n_not_disabled_fe_pixtype));
  module_data->module_result->disp_mip = sum_disp_mip/(float(n_not_disabled_fe_pixtype));
  module_data->module_result->frac_bad_tot_fit_closure = sum_frac_bad_tot_fit_closure/(float(n_not_disabled_fe_pixtype));
  module_data->module_result->frac_bad_disp_fit_closure = sum_frac_bad_disp_fit_closure/(float(n_not_disabled_fe_pixtype));
  module_data->module_result->charge_corr = sum_charge_corr/(float(n_not_disabled_fe_pixtype));
  if (module_data->module_result->tot_mip < max_tot_mip && module_data->module_result->tot_mip > min_tot_mip &&
      module_data->module_result->disp_mip < max_disp_mip && module_data->module_result->disp_mip > min_disp_mip) module_data->module_result->good_tuning = true;
  m_result->Add(module_data->module_result);

}

void TOTanalysisROOT::QualifyResults()
{
  if (debug) std::cout << "Qualifying results" << std::endl;

  if (allPassed)
    m_status=kPassed;
  else 
    m_status=kFailed;

  if (debug) std::cout << "Overall status is " << m_status << std::endl;

}


void TOTanalysisROOT::CheckForKnownProblems() //setting of failure bits
{
  if (debug) std::cout << "Checking for known problems" << std::endl;
}

std::shared_ptr<TF1> TOTanalysisROOT::PerformTotFitPerPixel(int row, int col, std::vector<float> charge, std::vector<float> charge_err, std::vector<float> tot, std::vector<float> tot_err) {
  
  if (debug) std::cout << "In PerformTotFitPerPixel" << std::endl;

  TF1* m_totfunc = createTotFitFunctionPtr("fitfunc"); // m_totfunc created with new. Have to delete when done.
  int NvcalSteps = charge.size();
  TGraphErrors* g_tot = new TGraphErrors(NvcalSteps,&charge[0],&tot[0],&charge_err[0],&tot_err[0]);
  //TGraphErrors* g_tot = new TGraphErrors(NvcalSteps-1,&charge[1],&tot[1],&charge_err[1],&tot_err[1]);
  int tot_fit_status = -1;
  {
    CAN::Lock lock(CAN::PixDbGlobalMutex::mutex());
    tot_fit_status = g_tot->Fit(m_totfunc,"RQ");
  }
  if (tot_fit_status != 0) { // remove one point at a time to see if converges 
    //std::cout << "Fit did not converge for col" << col << " row" << row << " trying to remove points" << std::endl;
    delete g_tot;
    for (int i=NvcalSteps-1; i!=-1; --i) {
      //std::cout << "Removing point " << i << std::endl;
      std::vector<float> temp_charge = removePoint(charge,i);
      std::vector<float> temp_charge_err = removePoint(charge_err,i);
      std::vector<float> temp_tot = removePoint(tot,i);
      std::vector<float> temp_tot_err = removePoint(tot_err,i);
      g_tot = new TGraphErrors(NvcalSteps-1,&temp_charge[0],&temp_tot[0],&temp_charge_err[0],&temp_tot_err[0]);
      for (int par=0; par!=npars_tot_fit; ++par) {
	m_totfunc->SetParameter(par,0.);
      }
      {
	CAN::Lock lock(CAN::PixDbGlobalMutex::mutex());
	tot_fit_status = g_tot->Fit(m_totfunc,"RQ");
      }
      if (debug) std::cout << "tot fit status " << tot_fit_status << std::endl;
      if (tot_fit_status == 0) break;
    }
  }

  TF1* tmpfunc = g_tot->GetFunction("fitfunc");
  std::shared_ptr<TF1> ret_func;
  char str[100];
  sprintf(str,"totfu_col%i_row%i",col,row);
  if (tmpfunc != NULL) {
    ret_func = std::shared_ptr<TF1>((TF1*) tmpfunc->Clone(str));
  }
  else {
    ret_func = std::shared_ptr<TF1>((TF1*) m_totfunc->Clone(str));
  }
  if (tot_fit_status != 0) {

    for (int par=0; par!=npars_tot_fit; ++par) {
      ret_func->SetParameter(par,0.);
      ret_func->SetParError(par,0.);
    }
    ret_func->SetChisquare(0.);
  }
  
  CAN::Lock lock(CAN::PixDbGlobalMutex::mutex());
  delete m_totfunc;
  delete tmpfunc;
  delete g_tot;
  
  return ret_func;

}

std::shared_ptr<TF1> TOTanalysisROOT::PerformTotFitOnAverage(int fe, int pixtype, int n_healthy_pixels, std::vector<float> charge, std::vector<float> charge_err, std::vector<float> mean_tot, std::vector<float> mean_tot_err) {
  
    
  if (debug) std::cout << "In PerformTotFitOnAverage" << std::endl;

  TF1* m_totfunc = createTotFitFunctionPtr("fitfunc"); // m_totfunc created with new. Have to delete when done.
  int NvcalSteps = charge.size();
  TGraphErrors* g_mean_tot = new TGraphErrors(NvcalSteps,&charge[0],&mean_tot[0],&charge_err[0],&mean_tot_err[0]);
  //TGraphErrors* g_mean_tot = new TGraphErrors(NvcalSteps-1,&charge[1],&mean_tot[1],&charge_err[1],&mean_tot_err[1]);
  int tot_fit_status = -1;
  if (n_healthy_pixels >= 1) {
    {
      CAN::Lock lock(CAN::PixDbGlobalMutex::mutex());
      tot_fit_status = g_mean_tot->Fit(m_totfunc,"RQ");
    }
    if (tot_fit_status != 0) { // remove one point at a time to see if converges 
      std::cout << "Fit did not converge for fe" << fe << " pixtype" << pixtype << " trying to remove points" << std::endl;
      delete g_mean_tot;
      for (int i=NvcalSteps-1; i!=-1; --i) {
	std::cout << "Removing point " << i << std::endl;
	std::vector<float> temp_charge = removePoint(charge,i);
	std::vector<float> temp_charge_err = removePoint(charge_err,i);
	std::vector<float> temp_tot = removePoint(mean_tot,i);
	std::vector<float> temp_tot_err = removePoint(mean_tot_err,i);
	g_mean_tot = new TGraphErrors(NvcalSteps-1,&temp_charge[0],&temp_tot[0],&temp_charge_err[0],&temp_tot_err[0]);
	for (int par=0; par!=npars_tot_fit; ++par) {
	  m_totfunc->SetParameter(par,0.);
	}
	{
	  CAN::Lock lock(CAN::PixDbGlobalMutex::mutex());
	  tot_fit_status = g_mean_tot->Fit(m_totfunc,"RQ");
	}
	if (debug) std::cout << "tot fit status " << tot_fit_status << std::endl;
	if (tot_fit_status == 0) break;
      }
    }
  }

  TF1* tmpfunc = g_mean_tot->GetFunction("fitfunc");
  std::shared_ptr<TF1> ret_func;
  char str[100];
  sprintf(str,"totfu_fe%i_pixtype%i",fe,pixtype);
  if (tmpfunc != NULL) {
    ret_func = std::shared_ptr<TF1>((TF1*) tmpfunc->Clone(str));
  }
  else {
    ret_func = std::shared_ptr<TF1>((TF1*) m_totfunc->Clone(str));
  }
  if (tot_fit_status != 0) {
    for (int par=0; par!=npars_tot_fit; ++par) {
      ret_func->SetParameter(par,0.);
      ret_func->SetParError(par,0.);
    }
    ret_func->SetChisquare(0.);
  }
  
  CAN::Lock lock(CAN::PixDbGlobalMutex::mutex());
  delete m_totfunc;
  delete tmpfunc;
  delete g_mean_tot;
  
  return ret_func;

}

std::shared_ptr<TF1> TOTanalysisROOT::PerformDispersionFit(int fe, int pixtype, int n_healthy_pixels, std::vector<float> mean_tot, std::vector<float> mean_tot_err, std::vector<float> disp, std::vector<float> disp_err) {
  
  if (debug) std::cout << "In PerformDispersion" << std::endl;

  TF1* m_dispfunc = createDispFitFunctionPtr("fitfunc"); // m_dispfunc created with new. Have to delete when done.
  int NvcalSteps = mean_tot.size();
  TGraphErrors* g_disp = new TGraphErrors(NvcalSteps,&mean_tot[0],&disp[0],&mean_tot_err[0],&disp_err[0]);
  //TGraphErrors* g_disp = new TGraphErrors(NvcalSteps-1,&mean_tot[1],&disp[1],&mean_tot_err[1],&disp_err[1]);
  int disp_fit_status = -1;
  if (n_healthy_pixels >= 2) {
    {
      CAN::Lock lock(CAN::PixDbGlobalMutex::mutex());
      disp_fit_status = g_disp->Fit(m_dispfunc,"RQ");
    }
  }
 
  TF1* tmpfunc = g_disp->GetFunction("fitfunc");
  std::shared_ptr<TF1> ret_func;
  char str[100];
  sprintf(str,"dispfu_fe%i_pixtype%i",fe,pixtype);
  if (tmpfunc != NULL) {
    ret_func = std::shared_ptr<TF1>((TF1*) tmpfunc->Clone(str));
  }
  else {
    ret_func = std::shared_ptr<TF1>((TF1*) m_dispfunc->Clone(str));
  }

  if (disp_fit_status != 0) {
    for (int par=0; par!=npars_disp_fit; ++par) {
      ret_func->SetParameter(par,0.);
      ret_func->SetParError(par,0.);
    }
    ret_func->SetChisquare(0.);
  }

  CAN::Lock lock(CAN::PixDbGlobalMutex::mutex());
  delete m_dispfunc;
  delete tmpfunc;
  delete g_disp;
  
  return ret_func;
  
}

bool TOTanalysisROOT::isHealthyPixel(std::vector<float> tot, std::vector<float> sigma) {

  bool healthy = true;
  
  int n_tot = tot.size();
  int n_tot_nonzero = n_tot;
  int n_sigma_nonzero = n_tot;
  float last_tot = 0.; 
  //float last_sigma = 0.; 
  bool tot_monotonic = true;
  //bool sigma_monotonic = true;
  for (int i=0; i!=n_tot; ++i) {
    if (tot[i] == 0.) --n_tot_nonzero;
    if (sigma[i] == 0.) --n_sigma_nonzero;
    if (tot[i] < last_tot) tot_monotonic = false;
    //if (sigma[i] < last_sigma) sigma_monotonic = false;
    last_tot = tot[i];
    //last_sigma = sigma[i];
  }
  
  if (n_tot_nonzero == 0) healthy = false;
  if (n_sigma_nonzero == 0) healthy = false;
  if (!tot_monotonic) healthy = false; // NOTE: THIS CAN ONLY BE APPLIED IF VCAL STEP SIZE IS LARGE ENOUGH
  //if (!sigma_monotonic) healthy = false; // this is almost never true

  return healthy;

}

std::vector<float> TOTanalysisROOT::removePoint(std::vector<float> v_full, int point) {

  std::vector<float> v;
  int Npoints = v_full.size();
  for (int i=0; i!=Npoints; ++i) {
    if (i==point) continue;
    v.push_back(v_full[i]);
  }
  return v;

}

std::shared_ptr<TF1> TOTanalysisROOT::createTotFitFunction(const char* name) {
  
  CAN::Lock lock(CAN::PixDbGlobalMutex::mutex());

  std::shared_ptr<TF1> tot_fit_result = std::shared_ptr<TF1>((TF1*) NULL);
  
  if (tot_fitfunc == "TOTFunc") tot_fit_result = std::shared_ptr<TF1>(new TF1(name,TOTFunc,0.,500000.,npars_tot_fit));
  else if (tot_fitfunc == "TOTFuncChebyshevSecond") tot_fit_result = std::shared_ptr<TF1>(new TF1(name,TOTFuncChebyshevSecond,0.,500000.,npars_tot_fit));
  else if (tot_fitfunc == "TOTFuncChebyshevThird") tot_fit_result = std::shared_ptr<TF1>(new TF1(name,TOTFuncChebyshevThird,0.,500000.,npars_tot_fit));
  else if (tot_fitfunc == "TOTFuncPol2") tot_fit_result = std::shared_ptr<TF1>(new TF1(name,TOTFuncPol2,0.,500000.,npars_tot_fit));
  else if (tot_fitfunc == "TOTFuncPol3") tot_fit_result = std::shared_ptr<TF1>(new TF1(name,TOTFuncPol3,0.,500000.,npars_tot_fit));
  else if (tot_fitfunc == "TOTFuncJens") tot_fit_result = std::shared_ptr<TF1>(new TF1(name,TOTFuncJens,0.,500000.,npars_tot_fit));
  else if (tot_fitfunc == "TOTFuncDetlef") tot_fit_result = std::shared_ptr<TF1>(new TF1(name,TOTFuncDetlef,0.,500000.,npars_tot_fit));
  else if (tot_fitfunc == "TOTFuncIskander") tot_fit_result = std::shared_ptr<TF1>(new TF1(name,TOTFuncIskander,0.,500000.,npars_tot_fit));
  
  return tot_fit_result;

}

std::shared_ptr<TF1> TOTanalysisROOT::createTotInvFitFunction(const char* name) {
  
  CAN::Lock lock(CAN::PixDbGlobalMutex::mutex());
  
  std::shared_ptr<TF1> tot_inv_fit_result = std::shared_ptr<TF1>((TF1*) NULL);
  
  if (tot_fitfunc == "TOTFunc") tot_inv_fit_result = std::shared_ptr<TF1>(new TF1(name,TOTFuncInv,0.,255.,npars_tot_fit));
  else if (tot_fitfunc == "TOTFuncChebyshevSecond") tot_inv_fit_result = std::shared_ptr<TF1>(new TF1(name,TOTFuncInvChebyshevSecond,0.,255.,npars_tot_fit));
  else if (tot_fitfunc == "TOTFuncChebyshevThird") tot_inv_fit_result = std::shared_ptr<TF1>(new TF1(name,TOTFuncInvChebyshevThird,0.,255.,npars_tot_fit));
  else if (tot_fitfunc == "TOTFuncPol2") tot_inv_fit_result = std::shared_ptr<TF1>(new TF1(name,TOTFuncInvPol2,0.,255.,npars_tot_fit));
  else if (tot_fitfunc == "TOTFuncPol3") tot_inv_fit_result = std::shared_ptr<TF1>(new TF1(name,TOTFuncInvPol3,0.,255.,npars_tot_fit));
  else if (tot_fitfunc == "TOTFuncJens") tot_inv_fit_result = std::shared_ptr<TF1>(new TF1(name,TOTFuncInvJens,0.,255.,npars_tot_fit));
  else if (tot_fitfunc == "TOTFuncDetlef") tot_inv_fit_result = std::shared_ptr<TF1>(new TF1(name,TOTFuncInvDetlef,0.,255.,npars_tot_fit));
  else if (tot_fitfunc == "TOTFuncIskander") tot_inv_fit_result = std::shared_ptr<TF1>(new TF1(name,TOTFuncInvIskander,0.,255.,npars_tot_fit));
  
  return tot_inv_fit_result;

}

std::shared_ptr<TF1> TOTanalysisROOT::createDispFitFunction(const char* name) {
  
  CAN::Lock lock(CAN::PixDbGlobalMutex::mutex());
  
  std::shared_ptr<TF1> disp_fit_result = std::shared_ptr<TF1>((TF1*) NULL);
  
  if (fit_disp_vs_charge) {
    disp_fit_result = std::shared_ptr<TF1>(new TF1(name,TOTFuncPol,0.,500000.,npars_disp_fit));
  }
  else {
    disp_fit_result = std::shared_ptr<TF1>(new TF1(name,TOTFuncPol,0.,255.,npars_disp_fit));
  }

  return disp_fit_result;

}


TF1* TOTanalysisROOT::createTotFitFunctionPtr(const char* name) {
  // This function creates a TF1* with new.
  // It is up to the called to delete when done with it.

  CAN::Lock lock(CAN::PixDbGlobalMutex::mutex());

  TF1* tot_fit_result = NULL;

  if (tot_fitfunc == "TOTFunc") tot_fit_result = new TF1(name,TOTFunc,0.,500000.,npars_tot_fit);
  else if (tot_fitfunc == "TOTFuncChebyshevSecond") tot_fit_result = new TF1(name,TOTFuncChebyshevSecond,0.,500000.,npars_tot_fit);
  else if (tot_fitfunc == "TOTFuncChebyshevThird") tot_fit_result = new TF1(name,TOTFuncChebyshevThird,0.,500000.,npars_tot_fit);
  else if (tot_fitfunc == "TOTFuncPol2") tot_fit_result = new TF1(name,TOTFuncPol2,0.,500000.,npars_tot_fit);
  else if (tot_fitfunc == "TOTFuncPol3") tot_fit_result = new TF1(name,TOTFuncPol3,0.,500000.,npars_tot_fit);
  else if (tot_fitfunc == "TOTFuncJens") tot_fit_result = new TF1(name,TOTFuncJens,0.,500000.,npars_tot_fit);
  else if (tot_fitfunc == "TOTFuncDetlef") tot_fit_result = new TF1(name,TOTFuncDetlef,0.,500000.,npars_tot_fit);
  else if (tot_fitfunc == "TOTFuncIskander") tot_fit_result = new TF1(name,TOTFuncIskander,0.,500000.,npars_tot_fit);
  
  return tot_fit_result;

}

TF1* TOTanalysisROOT::createDispFitFunctionPtr(const char* name) {
  // This function creates a TF1* with new.
  // It is up to the called to delete when done with it.

  CAN::Lock lock(CAN::PixDbGlobalMutex::mutex());
  
  TF1* disp_fit_result = NULL;
  if (fit_disp_vs_charge)  disp_fit_result = new TF1(name,TOTFuncPol,0.,500000.,npars_disp_fit);
  else  disp_fit_result = new TF1(name,TOTFuncPol,0.,255.,npars_disp_fit);

  return disp_fit_result;

}

// ***********************************
// ALL THE FIT FUNCTIONS DEFINED BELOW
// ***********************************

double TOTanalysisROOT::TOTFunc(double *x, double *par){
  
 double ret = 9.9e10;
 double num = x[0]+par[1];
 double denom = x[0]+par[2];
 if (denom!=0.) ret = par[0]*(num/denom);

 return ret;

}

double TOTanalysisROOT::TOTFuncPol(double *x, double *par){
  
 double ret = 9.9e10;
 ret = par[0]+par[1]*x[0];
 
 return ret;

}

double TOTanalysisROOT::TOTFuncPol0(double *x, double *par){
  
 double ret = 9.9e10;
 ret = par[0]*x[0];
 
 return ret;

}

double TOTanalysisROOT::TOTFuncPol2(double *x, double *par){
  
  double ret = 9.9e10;
  ret = par[0] + par[1]*x[0] + par[2]*x[0]*x[0];
 
  return ret;

}

double TOTanalysisROOT::TOTFuncPol3(double *x, double *par){
  
 double ret = 9.9e10;
 ret = par[0]+par[1]*x[0]+par[2]*x[0]*x[0]+par[3]*x[0]*x[0]*x[0];
 return ret;

}

double TOTanalysisROOT::TOTFuncDetlef(double *x, double *par){
  
 double ret = 9.9e10;
 double num = x[0]+par[1];
 double denom = x[0]+par[0]*par[2];
 if (denom!=0.) ret = par[0]*(num/denom);

 return ret;

}

double TOTanalysisROOT::TOTFuncChebyshevSecond(double *x, double *par) {

  double q = x[0];
  double qprime = (2./(35000.))*q - (9./7.);

  double ret = 9.9e10;
  ret = par[0] + par[1]*qprime + par[2]*((2*qprime*qprime)-1);

  return ret;

}

double TOTanalysisROOT::TOTFuncChebyshevThird(double *x, double *par) {

double q = x[0];
double qprime = (2./(35000.))*q - (9./7.);

double ret = 9.9e10;
 ret = par[0] + par[1]*qprime + par[2]*((2*qprime*qprime)-1) + par[3]*((4.*qprime*qprime*qprime) - 3.*qprime);

return ret;

}

double TOTanalysisROOT::TOTFuncJens(double* x, double* par) {

  double ret = 9.9e10;
  ret = par[0]*x[0]+(par[1]-par[0]*par[2]) - par[1]*par[2]/x[0];

  return ret;

}

double TOTanalysisROOT::TOTFuncIskander(double *x, double *par){

  double ret = 9.9e10;
  
  ret = par[0] + x[0]*par[1] + pow(x[0],2)*par[2]; 
  
  return ret;

}

double TOTanalysisROOT::TOTFuncInv(double *y, double *par){
  
  double ret = 9.9e10;
  
  if ((y[0]-par[0]) != 0.) ret = (par[0]*par[1]-y[0]*par[2])/(y[0]-par[0]);
  
  return ret;
  
}

double TOTanalysisROOT::TOTFuncInvPol(double *y, double *par){

  double ret = 9.9e10;
  
  if (par[1] != 0.) ret = (y[0]-par[0])/par[1];

  return ret;

}

double TOTanalysisROOT::TOTFuncInvPol0(double *y, double *par){

  double ret = 9.9e10;
  
  if (par[0] != 0.) ret = (y[0])/par[0];

  return ret;

}

double TOTanalysisROOT::TOTFuncInvPol2(double *y, double *par){
  
  double ret = 9.9e10;
  double ret1 = 9.9e10;
  double ret2 = 9.9e10;

  double p = par[1]/par[2];
  double q = (par[0] - y[0])/par[2];

  if (((-p/2.)*(-p/2.) - q) < 0.) return 0.; // to protect from nan by sqrt(neg number)

  ret1 = -p/2. + TMath::Sqrt((-p/2.)*(-p/2.) - q);
  ret2 = -p/2. - TMath::Sqrt((-p/2.)*(-p/2.) - q);
  
  if (p < 0) ret = ret2;
  else ret = ret1;

  return ret;
  
}


double TOTanalysisROOT::TOTFuncInvPol3(double *y, double *par){
  
  double ret = 0.;
  
  return ret;

}

double TOTanalysisROOT::TOTFuncInvDetlef(double *y, double *par){
  
  double ret = 9.9e10;

  if ((y[0]-par[0]) != 0.) ret = (par[0]*par[1]-y[0]*par[0]*par[2])/(y[0]-par[0]);
  
  return ret;

}

double TOTanalysisROOT::TOTFuncInvChebyshevSecond(double *y, double *par) {

  return 0.;

}

double TOTanalysisROOT::TOTFuncInvChebyshevThird(double *y, double *par) {

return 0.;

}

double TOTanalysisROOT::TOTFuncInvJens(double* y, double* par) {

  double ret = 9.9e10;
  double ret1 = 9.9e10;
  double ret2 = 9.9e10;
  
  double p = par[1]/par[0] - par[2] - y[0]/par[0];
  double q = -1.*par[1]*par[2]/par[0];

  if (((-p/2.)*(-p/2.) - q) < 0.) return 0.; // to protect from nan by sqrt(neg number)

  ret1 = -p/2. + TMath::Sqrt((-p/2.)*(-p/2.) - q);
  ret2 = -p/2. - TMath::Sqrt((-p/2.)*(-p/2.) - q);
  
  if (p > 0) ret = ret2;
  else ret = ret1;

  return ret;

}

double TOTanalysisROOT::TOTFuncInvIskander(double *y, double *par){
  
  return 0;

}



