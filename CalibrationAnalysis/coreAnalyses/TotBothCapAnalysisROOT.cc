#include "TotBothCapAnalysisROOT.h"
#include <iostream>
#include <fstream>
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TMath.h"
#include <PixDbGlobalMutex.hh>
#include <Lock.hh>


/*******************************************************************************************
 * Created by Andre Bach, after TOTanalysis code by Sara Strandberg.
 * TotBothCapAnalysisROOT.cc and .h
 * AutomaticTotBothCapAnalysis.cc and .hh
 * with analysisInput_TOT.h and Output
 *
 * NOTE
 *
 * THIS CODE IS NOT FINISHED
 *
 * The extended fit itself is mainly done, but various things should be implemented before
 * this code is actually used. Importantly:
 *  - Validation of its results, to old code and/or to DSP extended fit
 *  - Output of things beside just the result, for validation and double-checking
 *  - General clean up
 *  - Right now it takes exactly three standard TOT_CALIB scans (clow, chigh_full,
 *     chigh_limited), it should be made to take more arbitrary input and deal with it
 *  - It should check data for goodness before using it
 *     in particular, there may be an issue where some pixels report zero ToT for chigh
 *     and large vcal
 * 
 * Contact me at nonnormalizable@gmail.com for questions and complaints.
 *******************************************************************************************/

TotBothCapAnalysisROOT::TotBothCapAnalysisROOT()
{
  std::cout << std::endl;
  std::cout << "----------------------------------" << std::endl;
  std::cout << "   TotBothCapAnalysis Algorithm   " << std::endl;
  std::cout << "----------------------------------" << std::endl;
  std::cout << std::endl;
}

TotBothCapAnalysisROOT::~TotBothCapAnalysisROOT()
{
  std::cout << "In TotBothCapAnalysisROOT destructor" << std::endl; // debug
}

void TotBothCapAnalysisROOT::Initialize()
{
  std::cout << "In TotBothCapAnalysisROOT::Initialize" << std::endl;

  helper = new ROOTanalysisHelper();
   
}

void TotBothCapAnalysisROOT::Calculate()
{   
  std::cout << "In TotBothCapAnalysisROOT::Calculate()" << std::endl; // debug
  
  // Do some sanity chacks on the data we got
  if (m_scan->GetEntries() == 0)
    {
      std::cout << "ERROR No modules to run on. Exiting." << std::endl;
      return;
    }
  // get some information about the scan (same for every module)
  if (((AnalysisInputTotBothCap*)m_scan->At(0))->n_vcalsteps_clow <= 0)
    {
      std::cout << "ERROR Number of Vcal steps is 0. Exiting!" << std::endl;
      return;
    }
  if (((AnalysisInputTotBothCap*)m_scan->At(0))->n_maskstages_clow <= 0)
    {
      std::cout << "ERROR Number of maskstages is 0. Exiting!" << std::endl;
      return;
    }
  if (((AnalysisInputTotBothCap*)m_scan->At(0))->n_events_clow <= 0)
    {
      std::cout << "ERROR Number of events is 0. Exiting!" << std::endl;
      return;
    }

  // Print some info
  m_scan->R__FOR_EACH(AnalysisInputTotBothCap,PrintInfo)(); // debug x5
  std::cout << "those were the module names." << std::endl;  
  std::cout << "Running on " << ((AnalysisInputTotBothCap*)m_scan->At(0))->n_events_clow << " clow events" << std::endl;
  std::cout << "Running with " << ((AnalysisInputTotBothCap*)m_scan->At(0))->n_maskstages_clow << " clow maskstages" << std::endl;
  std::cout << "Running with " << ((AnalysisInputTotBothCap*)m_scan->At(0))->n_vcalsteps_clow << " clow vcalsteps " << std::endl;

  // start looping over modules
  allPassed=true;
  for (Int_t i=0; i<m_scan->GetEntries(); i++) // loop over modules on pp0
    {
      std::shared_ptr<TotBothCapAnalysisROOT::ModuleData> module_data(new TotBothCapAnalysisROOT::ModuleData());
      module_data->module_input = (AnalysisInputTotBothCap*)m_scan->At(i);
      module_data->module_result = new ResultTotBothCap();
      module_data->module_result->pp0Name = module_data->module_input->pp0Name; // have to set this, used by the base class
      module_data->module_result->moduleName = module_data->module_input->moduleName; // have to set this, used by the base class
      module_data->module_name = module_data->module_input->moduleName; // looks useless, but now I have a string instead of a stupid TString
      module_data->charge_clow = module_data->module_input->charge_clow;
      module_data->charge_chigh_full = module_data->module_input->charge_chigh_full;
      module_data->charge_chigh_lim = module_data->module_input->charge_chigh_lim;
      
      if (module_data->charge_clow.size() == 0)
	{
	  std::cout << "ERROR Charge array has 0 size (was the module config read correctly?). Exiting!" << std::endl;
	  return;
	}
	    
      // sanity check on data
      if (module_data->module_input->histo_mean_clow.size() != (unsigned int) module_data->module_input->n_vcalsteps_clow
	  || module_data->module_input->histo_sigma_clow.size() != (unsigned int) module_data->module_input->n_vcalsteps_clow
	  || module_data->module_input->histo_mean_clow.size() == 0
	  || module_data->module_input->histo_sigma_clow.size() == 0)
	{
	  std::cout << "ERROR Missing histograms for module " << module_data->module_name << ". Skipping!" << std::endl;
	  continue;
	}
    
      InitializeModuleResults(module_data);
      
      std::cout << "AMB - Processing module " << module_data->module_name << std::endl;
      
      module_data->n_masked_pixels.resize(helper->NfrontEnds, std::vector<int>(int(n_pixeltypes),0));
      
      // VECTORS TO STORE FIT RESULTS
      module_data->tot_fit_result.resize(helper->NfrontEnds, std::vector<std::shared_ptr<TF1> >(int(n_pixeltypes),std::shared_ptr<TF1>((TF1*) NULL)));
      module_data->disp_fit_result.resize(helper->NfrontEnds, std::vector<std::shared_ptr<TF1> >(int(n_pixeltypes),std::shared_ptr<TF1>((TF1*) NULL)));
      
      for (int fe=0; fe != helper->NfrontEnds; ++fe)
	{
	  int start_col = helper->getFrontEndStartCol(fe);
	  int stop_col = helper->getFrontEndStopCol(fe);
	  int start_row = helper->getFrontEndStartRow(fe);
	  int stop_row = helper->getFrontEndStopRow(fe);
	  for (int pixtype=0; pixtype!=int(n_pixeltypes); ++pixtype)
	    {
	      bool debugBigTime = fe==0 && module_data->module_name=="D1A_B05_S2_M2";
	      if (debugBigTime)
		{
		  std::cout << "AMB Debugging this big time." << std::endl;
		}
	      std::vector <TH1F*> h_tot_clow(module_data->module_input->n_vcalsteps_clow,(TH1F*) NULL);
	      std::vector <TH1F*> h_sigma_clow(module_data->module_input->n_vcalsteps_clow,(TH1F*) NULL);
	      std::vector <TH1F*> h_tot_chigh_full(module_data->module_input->n_vcalsteps_chigh_full,(TH1F*) NULL);
	      std::vector <TH1F*> h_sigma_chigh_full(module_data->module_input->n_vcalsteps_chigh_full,(TH1F*) NULL);
	      std::vector <TH1F*> h_tot_chigh_lim(module_data->module_input->n_vcalsteps_chigh_lim,(TH1F*) NULL);
	      std::vector <TH1F*> h_sigma_chigh_lim(module_data->module_input->n_vcalsteps_chigh_lim,(TH1F*) NULL);

	      std::vector<float> average_tot_mean_clow(module_data->module_input->n_vcalsteps_clow,0.);
	      std::vector<float> average_tot_mean_err_clow(module_data->module_input->n_vcalsteps_clow,0.);
	      std::vector<float> average_tot_mean_chigh_full(module_data->module_input->n_vcalsteps_chigh_full,0.);
	      std::vector<float> average_tot_mean_err_chigh_full(module_data->module_input->n_vcalsteps_chigh_full,0.);
	      std::vector<float> average_tot_mean_chigh_lim(module_data->module_input->n_vcalsteps_chigh_lim,0.);
	      std::vector<float> average_tot_mean_err_chigh_lim(module_data->module_input->n_vcalsteps_chigh_lim,0.);
	      
	      for (int col=start_col; col != stop_col; ++col)
		{
		  for (int row=start_row; row != stop_row; ++row)
		    {
		      // only do the fit for masked pixels
		      if (!helper->isMasked(col,row,module_data->module_input->n_maskstages_clow)) {std::cout<<"AMB not masked!"<<std::endl;continue;};
		      int pixeltype = -1;
		      if (int(n_pixeltypes) == 2)
			pixeltype = int(helper->pixelClass(col,row,2))-1;
		      else
			std::cout << "ERROR Asking for ! 2 pixeltypes, but only 2 is supported." << std::endl;
		      if (pixeltype != pixtype) continue; // only look at pixel type we're looping over
		      ++(module_data->n_masked_pixels[fe][pixtype]);
		      
		      // loop over vcal steps for **clow** scan
		      for (int vcal=0; vcal!=module_data->module_input->n_vcalsteps_clow; ++vcal)
			{
			  // create the histgrams to hold raw data, if first pixel of this fe & type
			  if (module_data->n_masked_pixels[fe][pixtype] == 1)
			    {
			      CAN::Lock lock(CAN::PixDbGlobalMutex::mutex());
			      char str[500];
			      sprintf(str,"h_tot_vcal_clow%i",vcal);
			      h_tot_clow[vcal] = new TH1F(str,str,1024,0.,256.); // holds all TOTs for a front-end and pixeltype
			      sprintf(str,"h_sigma_vcal_clow%i",vcal);
			      h_sigma_clow[vcal] = new TH1F(str,str,1024,0.,16.); // holds all sigmas for a front-end and pixeltype
			    } // end if first pixel of this fe & type
			  float tot_mean_clow = ((module_data->module_input->histo_mean_clow)[vcal])->GetBinContent(col+1,row+1);
			  float tot_sigma_clow = ((module_data->module_input->histo_sigma_clow)[vcal])->GetBinContent(col+1,row+1);
			  if (tot_mean_clow && tot_sigma_clow)
			    {
			      h_tot_clow[vcal]->Fill(tot_mean_clow);
			      h_sigma_clow[vcal]->Fill(tot_sigma_clow);
			    }
			}
		      
		      // loop over vcal steps for **chigh_full** scan
		      for (int vcal=0; vcal!=module_data->module_input->n_vcalsteps_chigh_full; ++vcal)
			{
			  // create the histgrams to hold raw data, if first pixel of this fe & type
			  if (module_data->n_masked_pixels[fe][pixtype] == 1)
			    {
			      CAN::Lock lock(CAN::PixDbGlobalMutex::mutex());
			      char str[500];
			      sprintf(str,"h_tot_vcal_chigh_full%i",vcal);
			      h_tot_chigh_full[vcal] = new TH1F(str,str,1024,0.,256.);
			      sprintf(str,"h_sigma_vcal_chigh_full%i",vcal);
			      h_sigma_chigh_full[vcal] = new TH1F(str,str,1024,0.,16.);
			    } // end if first pixel of this fe & type
			  float tot_mean_chigh_full = ((module_data->module_input->histo_mean_chigh_full)[vcal])->GetBinContent(col+1,row+1);
			  float tot_sigma_chigh_full = ((module_data->module_input->histo_sigma_chigh_full)[vcal])->GetBinContent(col+1,row+1);
			  if (tot_mean_chigh_full && tot_sigma_chigh_full)
			    {
			      h_tot_chigh_full[vcal]->Fill(tot_mean_chigh_full);
			      h_sigma_chigh_full[vcal]->Fill(tot_sigma_chigh_full);
			    }
			}

		      // loop over vcal steps for **chigh_lim** scan
		      for (int vcal=0; vcal!=module_data->module_input->n_vcalsteps_chigh_lim; ++vcal)
			{
			  // create the histgrams to hold raw data, if first pixel of this fe & type
			  if (module_data->n_masked_pixels[fe][pixtype] == 1)
			    {
			      CAN::Lock lock(CAN::PixDbGlobalMutex::mutex());
			      char str[500];
			      sprintf(str,"h_tot_vcal_chigh_lim%i",vcal);
			      h_tot_chigh_lim[vcal] = new TH1F(str,str,1024,0.,256.);
			      sprintf(str,"h_sigma_vcal_chigh_lim%i",vcal);
			      h_sigma_chigh_lim[vcal] = new TH1F(str,str,1024,0.,16.);
			    } // end if first pixel of this fe & type
			  float tot_mean_chigh_lim = ((module_data->module_input->histo_mean_chigh_lim)[vcal])->GetBinContent(col+1,row+1);
			  float tot_sigma_chigh_lim = ((module_data->module_input->histo_sigma_chigh_lim)[vcal])->GetBinContent(col+1,row+1);
			  if (tot_mean_chigh_lim && tot_sigma_chigh_lim)
			    {
			      h_tot_chigh_lim[vcal]->Fill(tot_mean_chigh_lim);
			      h_sigma_chigh_lim[vcal]->Fill(tot_sigma_chigh_lim);
			    }
			}
		    } // row
		} // col
	      for (int vcal=0; vcal!=module_data->module_input->n_vcalsteps_clow; ++vcal)
		{
		  average_tot_mean_clow[vcal] = h_tot_clow[vcal]->GetMean();
		  average_tot_mean_err_clow[vcal] = h_tot_clow[vcal]->GetMeanError();
		  delete h_tot_clow[vcal];
		  delete h_sigma_clow[vcal];
		} // vcal steps, 2nd loop
	      for (int vcal=0; vcal!=module_data->module_input->n_vcalsteps_chigh_full; ++vcal)
		{
		  average_tot_mean_chigh_full[vcal] = h_tot_chigh_full[vcal]->GetMean();
		  average_tot_mean_err_chigh_full[vcal] = h_tot_chigh_full[vcal]->GetMeanError();
		  delete h_tot_chigh_full[vcal];
		  delete h_sigma_chigh_full[vcal];
		} // vcal steps, 2nd loop
	      for (int vcal=0; vcal!=module_data->module_input->n_vcalsteps_chigh_lim; ++vcal)
		{
		  average_tot_mean_chigh_lim[vcal] = h_tot_chigh_lim[vcal]->GetMean();
		  average_tot_mean_err_chigh_lim[vcal] = h_tot_chigh_lim[vcal]->GetMeanError();
		  delete h_tot_chigh_lim[vcal];
		  delete h_sigma_chigh_lim[vcal];
		} // vcal steps, 2nd loop

	      // Do the fit, do the fit...
	      std::vector<std::vector<float> > charge_all;
	      charge_all.push_back(module_data->charge_clow[fe]);
	      charge_all.push_back(module_data->charge_chigh_full[fe]);
	      charge_all.push_back(module_data->charge_chigh_lim[fe]);
	      std::vector<std::vector<float> > mean_tot_all;
	      mean_tot_all.push_back(average_tot_mean_clow);
	      mean_tot_all.push_back(average_tot_mean_chigh_full);
	      mean_tot_all.push_back(average_tot_mean_chigh_lim);
	      std::vector<std::vector<float> > mean_tot_err_all;
	      mean_tot_err_all.push_back(average_tot_mean_err_clow);
	      mean_tot_err_all.push_back(average_tot_mean_err_chigh_full);
	      mean_tot_err_all.push_back(average_tot_mean_err_chigh_lim);
	      module_data->tot_fit_result[fe][pixtype] = PerformTotFitOnAverage(fe, pixtype, charge_all, mean_tot_all, mean_tot_err_all);
	    } // for pixtype loop
	} // for fe loop
      
      SaveResults(module_data);
    } // end loop over modules
  
  std::cout << "end of TOT analysis." << std::endl; // debug
}

std::shared_ptr<TF1> TotBothCapAnalysisROOT::PerformTotFitOnAverage(int fe, int pixtype,
								      std::vector<std::vector<float> > charge,
								      std::vector<std::vector<float> > mean_tot,
								      std::vector<std::vector<float> > mean_tot_err)
{    
  // AMB TODO: Put checks on the data here and/or earlier in the code!!

  /*
   * 1. Fit region A, clow.
   */

  TGraphErrors * g_a = new TGraphErrors(charge[0].size());
  for (unsigned int i=0; i < charge[0].size(); i++)
    {
      g_a->SetPoint(i, charge[0][i], mean_tot[0][i]);
      g_a->SetPointError(i, 0, mean_tot_err[0][i]);
    }
  
  TF1 * totfunc_a = createTotFitFunction("TotFunc_aa");
  FitBetterAndReport(totfunc_a, g_a);

  /*
   * 2. Fit region C, chigh_lim.
   */

  TGraphErrors * g_c = new TGraphErrors(charge[2].size());
  for (unsigned int i=0; i < charge[2].size(); i++)
    {
      g_c->SetPoint(i, charge[2][i], mean_tot[2][i]);
      g_c->SetPointError(i, 0, mean_tot_err[2][i]);
    }
  
  TF1 * totfunc_c = createTotFitFunction("TotFunc_cc");
  FitBetterAndReport(totfunc_c, g_c);
  
  /*
   * 3. Find scale factor.
   */

  float lowTotAt20000 = totfunc_a->Eval(20000);
      
  TF1 * highLimitedFitInverse = new TF1("offlineFitInverse", "([0]*[1]-[2]*x)/(x-[0])", 1, 100);
  for (int i=0; i<3; i++)
    highLimitedFitInverse->SetParameter(i, totfunc_c->GetParameter(i));
  float scaleDenom = highLimitedFitInverse->Eval(lowTotAt20000);
  if (!scaleDenom || scaleDenom < 5000.0 || scaleDenom > 80000.0)
    {
      std::cout << "WARNING Cigh limited scaling failed: inverse function at ToT for 20k elections = " << scaleDenom << " electrons" << std::endl;
      scaleDenom = 20000.0;
    }
  float scaleFactor = 20000.0 / scaleDenom;

  /*
   * 4. Scale region B and fit to A+B.
   */

  for (unsigned int i=0; i < charge[1].size(); i++)
    {
      charge[1][i] = charge[1][i] * scaleFactor;
    }
  
  int a_size = charge[0].size();
  int ab_size = charge[0].size() + charge[1].size();
  TGraphErrors * g_ab = new TGraphErrors(ab_size);
  for (int i=0; i < ab_size; i++)
    {
      if (i < a_size)
	{
	  g_ab->SetPoint(i, charge[0][i], mean_tot[0][i]);
	  g_ab->SetPointError(i, 0, mean_tot_err[0][i]);
	}
      else
	{
	  g_ab->SetPoint(i, charge[1][i-a_size], mean_tot[1][i-a_size]);
	  g_ab->SetPointError(i, 0, mean_tot_err[1][i-a_size]);
	}
    }
  
  TF1 * totfunc_ab = createTotFitFunction("TotFunc_ab");
  FitBetterAndReport(totfunc_ab, g_ab);

  std::shared_ptr<TF1> ret_func = std::shared_ptr<TF1>((TF1*) totfunc_ab->Clone("clone_of_totfunc_ab"));
  delete totfunc_a;
  delete g_a;
  delete totfunc_ab;
  delete g_ab;
  delete totfunc_c;
  delete g_c;

  return ret_func;
}

void TotBothCapAnalysisROOT::InitializeModuleResults(std::shared_ptr<TotBothCapAnalysisROOT::ModuleData> module_data)
{
  std::cout << "AMB In TotBothCapAnalysisROOT::InitializeModuleResults" << std::endl;
    

  {
    CAN::Lock lock(CAN::PixDbGlobalMutex::mutex());
    
  }

}

void TotBothCapAnalysisROOT::SaveTOTClosureResults(int fe, int pixtype, std::shared_ptr<TotBothCapAnalysisROOT::ModuleData> module_data, std::vector<float> v_mean_tot, std::vector<float> v_mean_tot_err)
{
  std::cout << "In SaveTOTSigmaClosureResults" << std::endl; // debug
}

void TotBothCapAnalysisROOT::SaveResults(std::shared_ptr<TotBothCapAnalysisROOT::ModuleData> module_data)
{
  std::cout << "In SaveResults()" << std::endl; // debug
  
  for (int fe=0; fe!=helper->NfrontEnds; ++fe)
    {
      for (int pixtype=0; pixtype!=int(n_pixeltypes); ++pixtype)
	{
	  // paramaters of tot fit
	  for (int par=0; par < 3; ++par)
	    {
	      if (module_data->module_result->fe_pixtype_tot.size()==0) module_data->module_result->fe_pixtype_tot.resize(3);
	      if (module_data->module_result->fe_pixtype_tot[par].size()==0) module_data->module_result->fe_pixtype_tot[par].resize(16);
	      if (module_data->module_result->fe_pixtype_tot[par][fe].size()==0) module_data->module_result->fe_pixtype_tot[par][fe].resize(2);
	      if (module_data->module_result->fe_pixtype_tot_err.size()==0) module_data->module_result->fe_pixtype_tot_err.resize(3);
	      if (module_data->module_result->fe_pixtype_tot_err[par].size()==0) module_data->module_result->fe_pixtype_tot_err[par].resize(16);
	      if (module_data->module_result->fe_pixtype_tot_err[par][fe].size()==0) module_data->module_result->fe_pixtype_tot_err[par][fe].resize(2);
	      module_data->module_result->fe_pixtype_tot[par][fe][pixtype] = module_data->tot_fit_result[fe][pixtype]->GetParameter(par);
	      module_data->module_result->fe_pixtype_tot_err[par][fe][pixtype] = module_data->tot_fit_result[fe][pixtype]->GetParError(par);
	    }
	}
    }
  m_result->Add(module_data->module_result);
}

void TotBothCapAnalysisROOT::QualifyResults()
{
  std::cout << "Qualifying results" << std::endl; // debug

  if (allPassed)
    m_status=kPassed;
  else 
    m_status=kFailed;
  
  std::cout << "Overall status is " << m_status << std::endl; // debug

}


void TotBothCapAnalysisROOT::CheckForKnownProblems() //setting of failure bits
{
  std::cout << "Checking for known problems" << std::endl; // debug
}



// bool TotBothCapAnalysisROOT::isHealthyPixel(vector<float> tot, vector<float> sigma)
// {

//   bool healthy = true;
  
//   int n_tot = tot.size();
//   int n_tot_nonzero = n_tot;
//   int n_sigma_nonzero = n_tot;
//   float last_tot = 0.; 
//   float last_sigma = 0.; 
//   bool tot_monotonic = true;
//   bool sigma_monotonic = true;
//   for (int i=0; i!=n_tot; ++i) {
//     if (tot[i] == 0.) --n_tot_nonzero;
//     if (sigma[i] == 0.) --n_sigma_nonzero;
//     if (tot[i] < last_tot) tot_monotonic = false;
//     if (sigma[i] < last_sigma) sigma_monotonic = false;
//     last_tot = tot[i];
//     last_sigma = sigma[i];
//   }
  
//   if (n_tot_nonzero == 0) healthy = false;
//   if (n_sigma_nonzero == 0) healthy = false;
//   if (!tot_monotonic) healthy = false; // NOTE: THIS CAN ONLY BE APPLIED IF VCAL STEP SIZE IS LARGE ENOUGH
//   //if (!sigma_monotonic) healthy = false; // this is almost never true

//   return healthy;

// }

TF1 * TotBothCapAnalysisROOT::createTotFitFunction(const char* name)
{
  CAN::Lock lock(CAN::PixDbGlobalMutex::mutex());

  TF1 * tot_fit_result = NULL;
  
  tot_fit_result = new TF1(name,TotFunc,0.,500000.,3);
  return tot_fit_result;

}

/******************************************************************************
 * FitBetterAndReport
 * To do a fit with initial parameters to a ToT vs charge plot and warn if fail.
 ******************************************************************************/
int TotBothCapAnalysisROOT::FitBetterAndReport(TF1 * f, TGraphErrors * g,
					       float p0, float p1, float p2)
{
  CAN::Lock lock(CAN::PixDbGlobalMutex::mutex());
  f->SetParameter(0, p0);
  f->SetParameter(1, p1);
  f->SetParameter(2, p2);
  int fitSt = g->Fit(f, "NQ");
  if (f->GetChisquare() / std::max(double(f->GetNDF()), 1.0) > 3000 || fitSt)
    {
      // Crazy cut for NDF because y errors for points are very small:
      // standard deviation for the mean for an average over all pixels.
      std::cout << "WARNING: possible failed fit: Chi^2/NDF = " << f->GetChisquare() << "/" << f->GetNDF() << ", status = " << fitSt << std::endl;
      return 1;
    }
  
  return 0;
}

// ***********************************
// ALL THE FIT FUNCTIONS DEFINED BELOW
// ***********************************

double TotBothCapAnalysisROOT::TotFunc(double * x, double * par)
{
  double ret = 9.9e10;
  double num = x[0]+par[1];
  double denom = x[0]+par[2];
  if (denom!=0.)
    ret = par[0]*(num/denom);
  
  return ret; 
}

// double TotBothCapAnalysisROOT::TotFuncInv(double * y, double * par)
// {
//   double ret = 9.9e10;
  
//   if ((y[0]-par[0]) != 0.)
//     ret = (par[0]*par[1]-y[0]*par[2])/(y[0]-par[0]);
  
//   return ret;
// }




