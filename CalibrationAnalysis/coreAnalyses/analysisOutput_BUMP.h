#ifndef _analysisOutputBUMP_h_
#define _analysisOutputBUMP_h_

#include <TString.h>
#include <TH2F.h>
#include "analysisOutput_Base.h"
#include "PixelCoralClientUtils/PixelMap.h"


class ResultBUMP : public AnalysisResult { //this is for one scan and one module
 public:
  ResultBUMP() {
    nAnalogDead = 0;
    nXtalkDead = 0;
    nDisconnected = 0;
//*********************
nAnalogMerged = 0;
//*****************************
  };
  ~ResultBUMP() {};

  int nAnalogDead;
  int nXtalkDead;
  int nDisconnected;
//*********************
int nAnalogMerged;
//*****************************

  std::shared_ptr<TH2I> disconnectedPixels; 

 public:
  //ClassDef(ResultBUMP,1) //not 0 -> activate I/O  ; not needed at the moemnt, put in if needed!
};

#endif
