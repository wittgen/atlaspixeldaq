#include "BOCanalysisROOT.h"
#include <iostream>
#include <fstream>
#include "TFile.h"



BOCanalysisROOT::BOCanalysisROOT(): _NDelay(0), _NThreshold(0), _NViSet(0)
{
  std::cout << std::endl;
  std::cout << "----------------------------" << std::endl;
  std::cout << "   BOC Analysis Algorithm   " << std::endl;
  std::cout << "----------------------------" << std::endl;
  std::cout << std::endl;
}



BOCanalysisROOT::~BOCanalysisROOT()
{
  //
}



void BOCanalysisROOT::Initialize()
{
  if(!m_scan) {
    std::cout << std::endl;
    std::cout << "BOCanalysisROOT::Initialize ERROR: input data pointer is null" << std::endl;
    std::cout << std::endl;
    return;
  }

  AnalysisInputBOC *scan_iter = (AnalysisInputBOC*) m_scan->First();                         

  if(m_logLevel>=CAN::kDiagnostics3Log) {
    std::cout << "loop names = " << std::endl;
    if(scan_iter->axis.size()>0) 
      std::cout << "  " << scan_iter->axis[0].variableName << std::endl;
    if(scan_iter->axis.size()>1) 
      std::cout << "  " << scan_iter->axis[1].variableName << std::endl;
    if(scan_iter->axis.size()>2) 
      std::cout << "  " << scan_iter->axis[2].variableName << std::endl;
    std::cout << std::endl;
  }


  _NDelay = 0; _DelayVal.clear();
  _NThreshold = 0; _ThresholdVal.clear();
  _NViSet = 0;  _ViSetVal.clear(); 
  

  // Set delay axis
  // -------------------------------
  _NDelay = scan_iter->axis[0].nSteps;
  int width = ( scan_iter->axis[0].lastBin-scan_iter->axis[0].firstBin)/
    ( scan_iter->axis[0].nSteps-1);
  for (int i=0;i<_NDelay;i++) 
    _DelayVal.push_back( (int)(scan_iter->axis[0].firstBin+i*width) );
     

  // Set threshold axis
  // -------------------------------
  _NThreshold = scan_iter->axis[1].nSteps;
  width = ( scan_iter->axis[1].lastBin-scan_iter->axis[1].firstBin)/
    ( scan_iter->axis[1].nSteps-1);
  for (int i=0;i<_NThreshold;i++) 
    _ThresholdVal.push_back( (int)(scan_iter->axis[1].firstBin+i*width) );
  

  // Set ViSet axis
  // -------------------------------
  if(scan_iter->axis.size()>2) {
    _NViSet = scan_iter->axis[2].nSteps;
    if(_NViSet > 1 ) {
      width = ( scan_iter->axis[2].lastBin-scan_iter->axis[2].firstBin)/
	( scan_iter->axis[2].nSteps-1);
      for (int i=0;i<_NViSet;i++) 
	_ViSetVal.push_back( (int)(scan_iter->axis[2].firstBin+i*width) );
    } 
  }


  for(int mod=0; mod<7; mod++) {
    for(int ilink=0; ilink<2; ilink++) {
      _Thr[mod][ilink] = -1;
      _Thr_min[mod][ilink] = -1;
      _Thr_max[mod][ilink] = -1;
      _Delay[mod][ilink] = -1;
      _Delay_redge[mod][ilink] = -1;
      _Delay_ledge[mod][ilink] = -1;
      _Delay_width[mod][ilink] = -1;
    }
  }
  

  if(m_logLevel>=CAN::kDiagnostics3Log) {
    std::cout << std::endl;
    std::cout << "Delay: " << _NDelay << std::endl;
    for(int y=0; y< _NDelay; y++) std::cout << _DelayVal[y] << ", ";
    std::cout << std::endl;
    std::cout << "Threshold: " << _NThreshold << std::endl;
    for(int y=0; y< _NThreshold; y++) std::cout << _ThresholdVal[y] << ", ";
    std::cout << std::endl;
    if(_NViSet > 1 ) {
      std::cout << "ViSet: " << _NViSet << std::endl;
      for(int y=0; y< _NViSet; y++) std::cout << _ViSetVal[y] << ", ";
      std::cout << std::endl;
    }
  }
}


void BOCanalysisROOT::Calculate() 
{  
  std::cout << "**************************************" << std::endl;
  std::cout << "BOCanalysis::Calculate  starting" << std::endl;
  m_scan->R__FOR_EACH(AnalysisInputBOC,PrintInfo)(); 
  std::cout << "BOCanalysis: These were the module names." << std::endl;  

  this->FindBestViSet();
 
  for(int ilink=0; ilink<2; ilink++) {
    this->FindBestThreshold(_ViSet, ilink+1);
    this->FindBestDelay(_ViSet, ilink+1);
  }  
  if(m_logLevel>=CAN::kDiagnostics1Log) this->Print();
} 



/*
void BOCanalysisROOT::Calculate() 
{
  TFile f("test.root","recreate");
  
 
  std::cout << "Number of Modules = " << m_scan->GetEntries() << std::endl;
  for(Int_t i = 0; i < m_scan->GetEntries(); i++) {

     std::cout << " Module #" << i << std::endl;
    AnalysisInputBOC* bocScan = (AnalysisInputBOC*) m_scan->At(i);
 
    char name[10];
    sprintf(name,"scan%i",i);
    bocScan->histo_Link1[0]->SetName(name);
    bocScan->histo_Link1[0]->Write();
  }
  
  f.Close();
}
*/


void BOCanalysisROOT::FindBestViSet()
{

  _ViSet = 0;
  _EFR   = 0;
  
  if(_NViSet < 2) return;
  
   if(m_scan->GetEntries()>7) {
    std::cout << "ERROR, pp0 with " << m_scan->GetEntries() << " modules" << std::endl;
    //return;
  }

  int areamin[10] = {0};
  
  
  //LOOP OVER VISET
  for (int ih=0; ih < _NViSet; ih++){  //Viset loop
    
    areamin[ih] = 999;
    
    //LOOP OVER MODULES 
    for(Int_t i = 0; i < m_scan->GetEntries(); i++) {
      AnalysisInputBOC * scan_iter = (AnalysisInputBOC *) m_scan->At(i); 
      
      int area_link1 = 0;
      int area_link2 = 0;
      
      for(int ithr = 1; ithr <= _NThreshold; ithr++) {
	for(int idelay = 1; idelay <= _NDelay; idelay++) {
	  
	  int val1 = int(scan_iter->histo_Link1[ih]->GetBinContent(idelay,ithr));
	  if (val1<1) area_link1++;
	  
	  if(scan_iter->histo_Link2.size()) {
	    int val2 = int(scan_iter->histo_Link2[ih]->GetBinContent(idelay,ithr));
	    if (val2<1) area_link2++;
	  }
	}
      }
      if (area_link1<areamin[ih]) areamin[ih] = area_link1;
      if(scan_iter->histo_Link2.size()) 
	if (area_link2<areamin[ih]) areamin[ih] = area_link2;
    }
    
    
    if(m_logLevel>=CAN::kDiagnostics3Log)
      std::cout << "Min area for ViSet = " << _ViSetVal[ih] << "  -->  " << areamin[ih] << std::endl;
  }
  
  
  int areamax = 0;
  int ibestviset=0;
  
  for (int ih=0; ih < _NViSet; ih++) {  //Viset loop
    if (areamin[ih] > areamax) {
      areamax = areamin[ih]; 
      ibestviset = ih;
    }
  }
  
  _ViSet = ibestviset;
  _EFR   = areamin[ibestviset];  
}

 


void BOCanalysisROOT::FindBestThreshold(int ih, int ilink)
{
  if(m_scan->GetEntries()>7) {
    std::cout << "ERROR, pp0 with " << m_scan->GetEntries() << " modules" << std::endl;
    //return;
  }

  AnalysisInputBOC *scan_iter1 = (AnalysisInputBOC*) m_scan->First();                        

  if(ilink!=1 && ilink!=2) return;
  if(ilink==1 && scan_iter1->histo_Link1.size()==0) return;
  if(ilink==2 && scan_iter1->histo_Link2.size()==0) return;


  int xwidth[26] = {0};
  int val;
  
  int thropt = -1;
  int thrmin = 9999;
  int thrmax = 0;
  int min    = 9999;


  //LOOP OVER MODULES
  std::cout << "ariel, nmodules = " << m_scan->GetEntries() << std::endl;
  for(Int_t mod = 0; mod < m_scan->GetEntries(); mod++) {
    AnalysisInputBOC * scan_iter2 = (AnalysisInputBOC *) m_scan->At(mod); 
    
    for(int ithr = 1; ithr <= _NThreshold; ithr++) {
      
      xwidth[ithr]=0;
      
      for(int idelay = 1; idelay <= _NDelay; idelay++) {
	
	if(ilink==1)
	  val = int(scan_iter2->histo_Link1[ih]->GetBinContent(idelay,ithr));
	if(ilink==2)
	  val = int(scan_iter2->histo_Link2[ih]->GetBinContent(idelay,ithr));
	
	if (val>0.5)  xwidth[ithr]++;
	
	if (val<1 && ithr < thrmin) thrmin = ithr;
	if (val<1 && ithr > thrmax) thrmax = ithr;
	
      }

    
      if(xwidth[ithr] < min) {
	min = xwidth[ithr];
	thropt = ithr;
      }
    }

    thropt = (int) (thrmin+thrmax)/2;
    
    _Thr[mod][ilink-1] = thropt;
    _Thr_min[mod][ilink-1] = thrmin;
    _Thr_max[mod][ilink-1] = thrmax;
     
  } //loop over modules
  
}



void BOCanalysisROOT::FindBestDelay(int ih, int ilink)
{
  if(m_scan->GetEntries()>7) {
    std::cout << "ERROR, pp0 with " << m_scan->GetEntries() << " modules" << std::endl;
    //return;
  }

  AnalysisInputBOC *scan_iter1 = (AnalysisInputBOC*) m_scan->First();                        
  
  if(ilink!=1 && ilink!=2) return;
  if(ilink==1 && scan_iter1->histo_Link1.size()==0) return;
  if(ilink==2 && scan_iter1->histo_Link2.size()==0) return;

  int val = -1, valR = -1, valL = -1;
  int delaymaxr  = 0;
  int delaymaxl  = 0;
  int delaybestr = 0;
  int delaybestl = 0;
  int delaybest  = 0;
  int dwidth     = 0;

  //LOOP OVER MODULES
  for(Int_t mod = 0; mod < m_scan->GetEntries(); mod++) {
    AnalysisInputBOC * scan_iter2 = (AnalysisInputBOC *) m_scan->At(mod); 
     
    std::cout << "  module #" << mod << std::endl;

    for(int idelay = 1; idelay <= _NDelay; idelay++) {
      
      if(ilink==1)
	val = int(scan_iter2->histo_Link1[ih]->GetBinContent(idelay,_Thr[mod][ilink-1]));
      if(ilink==2)
	val = int(scan_iter2->histo_Link2[ih]->GetBinContent(idelay,_Thr[mod][ilink-1]));
	
      int neighbr = idelay+1;
      if (idelay == _NDelay) neighbr = 1; 

      int neighbl = idelay-1;
      if (idelay==1) neighbl = _NDelay; 

      if(ilink==1) {
	valR = int(scan_iter2->histo_Link1[ih]->GetBinContent(neighbr,_Thr[mod][ilink]));
	valL = int(scan_iter2->histo_Link1[ih]->GetBinContent(neighbl,_Thr[mod][ilink]));
      }

      if(valR <= 0.5) dwidth++;

      // this is one edge of the error region=> need to make sure the right side neighbour is 0
      if (val > delaymaxr && valR <= 0.5) {
	delaymaxr = val;
	delaybestr = idelay;          
      } 
      if (val > delaymaxl && valL <= 0.5) {
	delaymaxl = val;
	delaybestl = idelay;          
      } 
    }

    
    int delayWidth =_DelayVal[delaybestl] - _DelayVal[delaybestr];
    if (delayWidth <= 0) delayWidth = 25 + delayWidth;
    
    if (delayWidth < 5) {
      delaybest = -1;
    } else if (delayWidth == 5) {
      delaybest = delaybestr+2*25/_NDelay;
    } else {
      delaybest = _DelayVal[delaybestr]+1+(int)(0.2*delayWidth);
    } 
    if (delaybest >= _NDelay) delaybest -= _NDelay;
    
    _Delay[mod][ilink-1] = delaybest;
    _Delay_ledge[mod][ilink-1] = _DelayVal[delaybestl];
    _Delay_redge[mod][ilink-1] = _DelayVal[delaybestr];
    _Delay_width[mod][ilink-1] = (_DelayVal[1]-_DelayVal[0]) * _DelayVal[dwidth];
  }
}


void BOCanalysisROOT::QualifyResults() 
{
  m_status=kPassed;
}


void BOCanalysisROOT::CheckForKnownProblems() 
{

}


void BOCanalysisROOT::Print()
{
  std::cout << std::endl;
  std::cout << "-------------------------------------" << std::endl;
  std::cout << "BOC Scan Analysis Results: " << std::endl;
  std::cout << "-------------------------------------" << std::endl;
  std::cout << std::endl;

  if(_NViSet > 1) 
    std::cout << " o ViSet = " << _ViSetVal[_ViSet] << "mV" << "  (minimum EFR = " << _EFR << ")" << std::endl;
  std::cout << " o Threhsold, Delay = " << std::endl;
  for(int mod=0; mod<7; mod++) {
    for(int ilink=0; ilink<2; ilink++) {
      if(_Thr[mod][ilink]!=-1) 
	std::cout << "     Module #" << mod << ", Link #" << ilink+1 << "  ->  " 
	     << _ThresholdVal[_Thr[mod][ilink]] << ", " << _Delay[mod][ilink]
	     << " ns" << std::endl;
    }
  } 
  std::cout << "-------------------------------------" << std::endl;
  std::cout << std::endl << std::endl;
}
