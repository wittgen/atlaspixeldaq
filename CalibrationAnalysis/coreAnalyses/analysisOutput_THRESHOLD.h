#ifndef _analysisOutputTHRESHOLD_h_
#define _analysisOutputTHRESHOLD_h_

#include <TString.h>
#include <TH2F.h>
#include "analysisOutput_Base.h"
#include "PixelCoralClientUtils/PixelMap.h"


class ResultTHRESHOLD : public AnalysisResult { //this is for one scan and one module
 public:
  ResultTHRESHOLD() :AnalysisResult(), 
    passed_all_fe_bad_pixel(false),
    passed_all_fe_noise(false),
    passed_all_fe_threshold(false),
    passed_all_fe_threshold_RMS(false),
    passed_module_threshold_RMS(false), 
    numBadPixels(0), numGoodFEs(0)
    {
      //module values
      average_threshold=0;
      average_noise[0]=average_noise[1]=average_noise[2]=0;
      RMS_threshold=0;
      RMS_noise[0]=RMS_noise[1]=RMS_noise[2]=0;

      //FE values
      for (int FE=0; FE<16; FE++){
	passed_FE_threshold_min[FE]=false;
	passed_FE_threshold_max[FE]=false; 
	passed_FE_threshold_mean_min[FE]=false;
	passed_FE_threshold_mean_max[FE]=false;
	passed_FE_noise_max[FE]=false;
	passed_FE_threshold_RMS_max[FE]=false;
	passed_FE_cuts[FE]=false;

	average_threshold_FE[FE][0]=average_threshold_FE[FE][1]=average_threshold_FE[FE][2]=0 ; 
	average_noise_FE[FE][0]=average_noise_FE[FE][1]=average_noise_FE[FE][2]=0;
	RMS_threshold_FE[FE][0]=RMS_threshold_FE[FE][1]=RMS_threshold_FE[FE][2]=0;
      }
    };
  ~ResultTHRESHOLD() {};

  //pixel maps
  bool MakePixelMap; // to make sure the analysis only creates maps if selected
  PixelMap_t badPixelMap;

  //error type
  bool passed_all_fe_bad_pixel;
  bool passed_all_fe_noise;
  bool passed_all_fe_threshold;
  bool passed_all_fe_threshold_RMS;
  bool passed_module_threshold_RMS;  

  //module values
  Float_t average_threshold; 
  Float_t average_noise[3]; //for 3 classes of pixel types
  Float_t average_thresholdOverNoise;

  Float_t RMS_threshold; 
  Float_t RMS_noise[3]; //for 3 classes of pixel types

  unsigned int numBadPixels;
  unsigned int numGoodFEs;

  //FE values
  Float_t average_threshold_FE[16][3]; //for 3 classes of pixel types 
  Float_t average_noise_FE[16][3];
  Float_t RMS_threshold_FE[16][3]; 

  bool passed_FE_threshold_min[16];
  bool passed_FE_threshold_max[16]; 
  bool passed_FE_threshold_mean_min[16];
  bool passed_FE_threshold_mean_max[16];
  bool passed_FE_noise_max[16];
  bool passed_FE_threshold_RMS_max[16];
  bool passed_FE_cuts[16];

 public:
  //ClassDef(ResultTHRESHOLD,1) //not 0 -> activate I/O  ; not needed at the moemnt, put in if needed!
};

#endif
