#include "DIGITALanalysisROOT.h"
#include <iostream>
#include <fstream>
#include "TFile.h"



DIGITALanalysisROOT::DIGITALanalysisROOT(): _Nmasks(0), _Nevents(0), _Ncol(0), _Nrow(0)
{
  std::cout << std::endl;
  std::cout << "-------------------------------" << std::endl;
  std::cout << "   DIGITAL Analysis Algorithm   " << std::endl;
  std::cout << "-------------------------------" << std::endl;
  std::cout << std::endl;
}



DIGITALanalysisROOT::~DIGITALanalysisROOT()
{
  //
}





void DIGITALanalysisROOT::Initialize()
{
  if(!m_scan) {
    std::cout << std::endl;
    std::cout << "DIGITALanalysisROOT::Initialize ERROR: input data pointer is null" << std::endl;
    std::cout << std::endl;
    return;
  }

  AnalysisInputDIGITAL* occupancy = (AnalysisInputDIGITAL*) m_scan->At(0);
  _Nrow = occupancy->histo->GetNbinsY();
  _Ncol = occupancy->histo->GetNbinsX();
 
  _Nmasks  = occupancy->Nmasks;
  _Nevents = occupancy->Nevents;

  // fill pattern
  //-----------------------
  int nPixels = 0;
  for(int col = 1; col <= _Ncol; col++) {
    for(int row = 1; row <= _Nrow; row++) {
      _pattern[col-1][row-1] = 0;
    }
  }
  

  //odd columns
  for(int h = 1; h <= _Nrow; h = h+32) {
    for(int j = 1; j <= _Ncol; j = j+2) {
      for(int m = 0; m < _Nmasks; m++) {
	
	int col = j;
	int row = h+m;
	
	_pattern[col-1][row-1] = _Nevents;
	nPixels++;

      }  
    }
  }

  //even columns
  for(int h = 32; h <= _Nrow; h = h+32) {
    for(int j = 2; j <= _Ncol; j = j+2) {
      for(int m = 0; m < _Nmasks; m++) {
	
	int col = j;
	int row = h-m;
	if(row<1) row += _Nrow;

	_pattern[col-1][row-1] = _Nevents;
	nPixels++;

      }  
    }
  }
  
  std::cout << "Number of masks   = " << _Nmasks << std::endl;
  std::cout << "Number of Events  = " << _Nevents << std::endl;
  std::cout << "Number of modules = " << m_scan->GetEntries() << std::endl;
  std::cout << std::endl;
  std::cout << "Number of active pixels = " << nPixels << std::endl;
  std::cout << std::endl;

} 
  
  
 

void DIGITALanalysisROOT::Calculate() 
{
  //TFile f("test.root","recreate");
  

  if(m_scan->GetEntries()>7) {
    std::cout << "ERROR, pp0 with " << m_scan->GetEntries() << " modules" << std::endl;
    //return;
  }


  if(m_logLevel>=CAN::kDiagnostics1Log) std::cout << "Number of Modules = " << m_scan->GetEntries() << std::endl;
  for(Int_t i = 0; i < m_scan->GetEntries(); i++) {
    
    if(m_logLevel>=CAN::kDiagnostics2Log) std::cout << " Module #" << i << std::endl;
    AnalysisInputDIGITAL* occupancy = (AnalysisInputDIGITAL*) m_scan->At(i);
    

    int extraHits = 0;
    int missingHits = 0;

    //char name[10];
    //sprintf(name,"scan%i",i);
    //occupancy->histo->SetName(name);
    //occupancy->histo->Write();
   
    for(int col = 1; col <= _Ncol; col++) {
      for(int row = 1; row <= _Nrow; row++) {
	int expected = _pattern[col-1][row-1];
	int observed = occupancy->histo->GetBinContent(col,row);
	int error = observed - expected;
	if(m_logLevel>=CAN::kDiagnostics3Log && i==0) 
	  std::cout << col << ", " << row << "    " << expected << " - " << observed << "; " << error << std::endl;
	if(observed<expected) missingHits -= error;
	if(observed>expected) extraHits   += error;
	
	if(m_logLevel>=CAN::kDiagnostics4Log) {
	  if(observed!=expected)
	    std::cout <<  col << ", " << row << "    " << expected << " - " << observed << "; " << error << std::endl;
	}
      }
    }
      
    if(m_logLevel>=CAN::kDiagnostics1Log) {
      std::cout << "   Number of missing hits = " << missingHits << std::endl;
      std::cout << "   Number of extra hits = " << extraHits << std::endl;
      std::cout << std::endl;
    }
    
  } // modules
  
  if(m_logLevel>=CAN::kDiagnostics1Log) this->Print();

  //f.Close();
} 
 
 
void DIGITALanalysisROOT::QualifyResults() 
{
  if(allModulesPassed)
    m_status=kPassed;
  else 
    m_status=kFailed;
}


void DIGITALanalysisROOT::CheckForKnownProblems() //setting of failure bits
{

}


void DIGITALanalysisROOT::Print()
{
  std::cout << std::endl;
  std::cout << "-------------------------------------" << std::endl;
  std::cout << "DIGITAL Scan Analysis Results: " << std::endl;
  std::cout << "-------------------------------------" << std::endl;
  std::cout << std::endl;

  std::cout << "-------------------------------------" << std::endl;
  std::cout << std::endl << std::endl;
}
