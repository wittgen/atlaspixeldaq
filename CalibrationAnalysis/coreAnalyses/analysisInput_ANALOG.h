#ifndef _analysisInputANALOG_h_
#define _analysisInputANALOG_h_

#include "analysisInput_Base.h"
#include <vector>
#include <TH2F.h>
#include <TString.h>
#include <iostream>
#include <TSystem.h>

class AnalysisInputANALOG : public AnalysisInput { //this is for one scan and one module
 public:
  const TH2* histo; 

  TString moduleName;
  Int_t MBs; //40,80,160 Mbits/s
  TString pp0Name;
  std::vector<histoAxis_t> axis; //the axis of the histo (the same for all histos) of type histoAxis_t 


  //Helper functions
  void PrintInfo() {
    std::cout << "AnalysisInputANALOG for pp0 " << pp0Name.Data() << ", module: " << moduleName.Data() << "  MBit/s=" << MBs << std::endl;
    
    //    std::cout << "vector axis " <<  axis.size() << std::endl;
    std::cout << "histos entries " << histo->GetEntries() << std::endl;
  };

  ////////////////////////////////
  ///////Analysis Results
  UInt_t passed;
  UInt_t failure;
  Float_t average;

  TH2F hErrorMap; 
  TH2F hErrorMap_minus1; 
  TH2F hErrorMap_minus19; 
  TH2F hErrorMap_plus1; 

  TH1F hInt; //integrating over total module
  std::vector<TH1F> hInt_FEs; //integrated current histograms for different FEs   

  TH1F AvPerFE; 
  TH1F RMSPerFE; 
  Float_t variance_mean_FEs;
  Float_t variance_RMS_FEs;

  //ClassDef(AnalysisInputANALOG,3) 
};

#endif
