#ifndef _ANNAanalysisROOT_h_
#define _ANNAanalysisROOT_h_ 

#include "ROOTanalysisBase.h"
#include "analysisInput_ANNA.h"
#include "analysisOutput_ANNA.h"
#include "TH2I.h"

class ANNAanalysisROOT  : public ROOTanalysisBase
{
 public:
  ANNAanalysisROOT();
  ~ANNAanalysisROOT();


 private:
  void Initialize();

  void Calculate(); //internal calculations

  void QualifyResults(); //apply cuts from analysis config

  void CheckForKnownProblems(); //setting of failure bits

  void Print();

 private: //variables

  bool allModulesPassed;
  
  // Algorithm parameters
  // --------------------

  int _Nmasks;
  int _Nevents;

  int _Ncol;
  int _Nrow;

  int _pattern[144][320];

};


#endif

