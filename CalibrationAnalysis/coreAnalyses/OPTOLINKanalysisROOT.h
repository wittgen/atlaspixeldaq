#ifndef _OPTOLINKanalysisROOT_h_
#define _OPTOLINKanalysisROOT_h_ 

#include "ROOTanalysisBase.h"
#include "analysisInput_OPTOLINK.h"
#include "analysisOutput_OPTOLINK.h"


class OPTOLINKanalysisROOT  : public ROOTanalysisBase
{
 public:
  OPTOLINKanalysisROOT();
  ~OPTOLINKanalysisROOT();

//  Int_t m_allowedovershoots;
  Float_t m_upperdistance;
  Float_t m_lowerdistance;
  Float_t m_leftdeviation;
  Float_t m_rightdeviation;
  Int_t m_overshootlimit;
  Int_t m_highcutoff;
  Int_t m_tunedistance;
  bool m_forcedfail;
  Int_t m_serialNumber;

  // common parameter
  Int_t m_visetlowthreshold;

  Float_t m_maxthr;

  Float_t m_fraqdelay;
  Float_t m_fraqthr;
  
 
  Int_t m_maxiter;
  bool m_bf;
  Int_t m_bfsteps;

  enum Startpoint
  {
    LEFTDOWN,
    LEFTUP,
    RIGHTDOWN,
    RIGHTUP,
    FRAQ,
    COM
  };
  int m_sp=FRAQ;

  enum Algotype
  {
    NEW,
    OLD
  };
  int m_at=OLD;

 private: //methods

  void Initialize();

  void Calculate(); //internal calculations

  void QualifyResults(); //apply cuts from analysis config

  void CheckForKnownProblems(); //setting of failure bits
  
  
  int FindBestViSet();
  void FindBestThreshold(int iviset, int ilink);
  void FindBestDelay(int iviset, int ilink);
  bool QualifyTuning(int ih, AnalysisResult* general_result, int mod, float thr_1, float thr_2, float del_1, float del_2);
  bool QualifyTuning(int ih, int mod, float thr_1, float thr_2, float del_1, float del_2);
  int CommonSpotFinder(int ih); //, AnalysisResult* general_result);
  void Print();

  void FindPreferedPoint();//new algorithm

  void FindBestPhases();//IBL
  void getBestPhase(const TH2 * histo, unsigned int &bestPhase, bool &success);

 private: //variables  
  int _NDelay; 
  std::vector<float> _DelayVal;
  
  int _NThreshold; 
  std::vector<float> _ThresholdVal;
  
  int _NViSet;  
  std::vector<int> _ViSetVal;
  

  // Algorithm parameters
  // --------------------
  
  int _ViSet;
  int _EFR;

  float _Thr[7][2];
  float _Thr_min[7][2];
  float _Thr_max[7][2];

  float _Delay[7][2];
  float _Delay_redge[7][2];
  float _Delay_ledge[7][2];
  float _Delay_width[7][2];

  int _ChipFeFlav;

  bool _Passed[8][2];
};


#endif

