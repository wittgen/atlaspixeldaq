#ifndef _analysisInputTDACFASTTUNE_h_
#define _analysisInputTDACFASTTUNE_h_

#include "analysisInput_Base.h"
#include <vector>
#include <TH2F.h>
#include <TString.h>
#include <iostream>

class AnalysisInputTDACFASTTUNE : public AnalysisInput { //this is for one scan and one module
 public:
  AnalysisInputTDACFASTTUNE() :AnalysisInput(), histo_tdac(NULL) {};
  ~AnalysisInputTDACFASTTUNE() {};

  std::vector<const TH2*> histo_occ;
  const TH2* histo_tdac;
  std::vector<histoAxis_t> axis; //the axis of the histo (the same for all histos) of type histoAxis_t 
  
  TString moduleName;
  Int_t MBs; //40,80,160 Mbits/s
  TString pp0Name;
  Int_t Nmasks;
  Int_t Nevents;
  Int_t Ntdacsteps;
  Bool_t digitalInjection;
  Int_t charge;
  


  ////////////////////////////////
  ///////Analysis Results
 /*  UInt_t passed; */
/*   UInt_t failure; */
/*   Float_t average; */


  //Helper functions
  void PrintInfo() {
    std::cout << "AnalysisInputTDACFASTTUNE for pp0 " << pp0Name.Data() << ", module: " << moduleName.Data() << "  MBit/s=" << MBs << std::endl;
  };
  
  //ClassDef(AnalysisInputTDACFASTTUNE,1) 
};
    

#endif
