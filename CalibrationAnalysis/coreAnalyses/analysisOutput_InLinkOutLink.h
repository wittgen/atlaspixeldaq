#ifndef _analysisOutputInLinkOutLink_h_
#define _analysisOutputInLinkOutLink_h_

#include <TString.h>
#include "analysisOutput_Base.h"


class ResultInLinkOutLink : public AnalysisResult { //this is for one scan and one module
 public:
  ResultInLinkOutLink() :AnalysisResult() 
    {
      drx_res=ratiobits1=ratiobits2=0;
      noisy1=noisy2=0;
    };
  ~ResultInLinkOutLink() {};

   Float_t drx_res;
   Float_t ratiobits1;
   Float_t ratiobits2;
   bool   noisy1;
   bool   noisy2;
   
 public:
  //ClassDef(ResultInLinkOutLink,1) //not 0 -> activate I/O  ; not needed at the moemnt, put in if needed!
};

#endif
