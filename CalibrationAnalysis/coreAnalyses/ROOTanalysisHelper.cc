#ifndef _ROOTanalysisHelper_CC_
#define _ROOTanalysisHelper_CC_

#include "ROOTanalysisHelper.h"


ROOTanalysisHelper::ROOTanalysisHelper()
{
  Nrows = 320;
  Ncols = 144;
  NpixelTypes = 6;
  NpixelClasses = 3;
  NfrontEnds = 16;
  intimeAnalysis = false;
} 

ROOTanalysisHelper::ROOTanalysisHelper(int nClasses, bool ifIntime)
{
  Nrows = 320;
  Ncols = 144;
  NpixelTypes = 6;
  NpixelClasses = nClasses;
  NfrontEnds = 16;
  intimeAnalysis = ifIntime; 
}

//***************************[maria elena] added this function
int ROOTanalysisHelper::getNpixelClasses(int chip_type)
{
    return (chip_type == PixGeometry::FEI4_CHIP || chip_type == PixGeometry::FEI4_MODULE) ? 2 : 3;
}

//***************************************

void ROOTanalysisHelper::SetBit(UInt_t &var, Int_t bit) 
{
  var |= (1<<bit);
  //  std::cout << "setting bit " << bit << "  ->" << var << std::endl;
}

bool ROOTanalysisHelper::HasBit(const UInt_t &var, Int_t bit)
{
  return var & (1<<bit); 
}

bool ROOTanalysisHelper::FulfillMinCut(Float_t value, Float_t cutValue) 
{
  std::cout << value << " > " << cutValue;
  if( value > cutValue ) { 
    std::cout << " OK " << std::endl;    
    return true;
  }
  else {
    std::cout << " !FAILED! " << std::endl;
    return false;
  }
}

bool ROOTanalysisHelper::FulfillMaxCut(Float_t value, Float_t cutValue) 
{
  std::cout << value << " < " << cutValue;
  if( value < cutValue ) { 
    std::cout << " OK " << std::endl;    
    return true;
  }
  else {
    std::cout << " !FAILED! " << std::endl;
    return false;
  }
}

bool ROOTanalysisHelper::FulfillMinMaxCut(Float_t value, Float_t cutMinValue, Float_t cutMaxValue) 
{
  std::cout << cutMaxValue<< " < " << value << " > " << cutMinValue;
  if( value > cutMinValue && value < cutMaxValue) { 
    std::cout << " OK " << std::endl;    
    return true;
  }
  else {
    std::cout << " !FAILED! " << std::endl;
    return false;
  }
}


bool ROOTanalysisHelper::FulfillMaxCut(TH1F * histo, Float_t cutValue, Float_t fraction) 
{
  float precentFailed = PercentFailedMaxCut(histo, cutValue, fraction);

  std::cout << (100-precentFailed) << "%  < " << cutValue;   

  bool result( precentFailed < (1-fraction)*100 ); 

  if(result) 
    std::cout << " OK " << std::endl;    
  else
    std::cout << " !FAILED! (required were " << fraction*100 << "%)" << std::endl;
  return result;
}


bool ROOTanalysisHelper::FulfillMinCut(TH1F * histo, Float_t cutValue, Float_t fraction) 
{
  float percentFailed = PercentFailedMinCut( histo, cutValue, fraction);

  std::cout << (100-percentFailed) << "%  > " << cutValue;

  bool result( percentFailed < (1-fraction)*100 );   

  if(result) 
    std::cout << " OK " << std::endl;    
  else
    std::cout << " !FAILED! (required were " << fraction*100 << "%)" << std::endl;
  return result;
}

float ROOTanalysisHelper::PercentFailedMaxCut(TH1F * histo, Float_t cutValue, Float_t fraction)
{
  Float_t tot = histo->Integral();
  if(tot==0) {
    std::cout << "ROOTanalysisHelper: WARNING histo is empty!" << std::endl;
    return false;
  }
  Int_t cutBin(histo->FindBin(cutValue));

  return (1-histo->Integral(1,cutBin)/tot)*100;  //have to start at bin 1, bin 0 is the underflow bin
} 

float ROOTanalysisHelper::PercentFailedMinCut(TH1F * histo, Float_t cutValue, Float_t fraction)
{
  Float_t tot = histo->Integral();
  if(tot==0) {
    std::cout << "ROOTanalysisHelper: WARNING histo is empty!" << std::endl;
    return false;
  }
  Int_t cutBin(histo->FindBin(cutValue));

  return histo->Integral(1,cutBin)/tot*100; //have to start at bin 1, bin 0 is the underflow bin
} 



float ROOTanalysisHelper::PercentFailedMaxCut(std::shared_ptr<TH1F> histo, Float_t cutValue)
{
  Float_t tot = histo->Integral(0,histo->GetNbinsX()+1);
  if(tot==0) {
    std::cout << "ROOTanalysisHelper: WARNING histo is empty!" << std::endl;
    return 100.; // at lease return 100 and not 0 if histogram is empty!!
  }
  Int_t cutBin(histo->FindBin(cutValue));

  return (1-histo->Integral(0,cutBin)/tot)*100;  //this is sara's version, and i _want_ to include overflow and underflow
} 

float ROOTanalysisHelper::PercentFailedMinCut(std::shared_ptr<TH1F> histo, Float_t cutValue)
{
  Float_t tot = histo->Integral(0,histo->GetNbinsX()+1);
  if(tot==0) {
    std::cout << "ROOTanalysisHelper: WARNING histo is empty!" << std::endl;
    return 100.;  // at lease return 100 and not 0 if histogram is empty!!
  }
  Int_t cutBin(histo->FindBin(cutValue));

  return histo->Integral(0,cutBin)/tot*100; //this is sara's version, and i _want_ to include overflow and underflow
} 

void ROOTanalysisHelper::Dump(TObject* histo) 
{
  TFile f("dumpedInfo.root","update");
  histo->Write();
  f.Close();
} 


std::shared_ptr<TH1F> ROOTanalysisHelper::IntegrateHisto(const TH2* histo, Int_t bin_min, Int_t bin_max, Int_t pixclass, Int_t FE, Int_t zeroBin) 
{
  Int_t binsXstart,binsXend,binsYstart,binsYend;
  binsXstart=1;
  binsXend=Ncols;
  binsYstart=1;
  binsYend=Nrows;
  const int nBINS(1000);

//to construct range of result histo automatically
//   Float_t max(0);
//   for(int x=binsXstart; x<binsXend; x++) 
//     for(int y=binsYstart; y<binsYend; y++) 
//       if(histo->GetBinContent(x,y)>max) max=histo->GetBinContent(x,y);  
//  TH1F* h_result = new TH1F(histo->GetTitle(),histo->GetTitle(),50,0,max+1); //default

  TString name(histo->GetTitle());
  name+=pixclass;
  //name+=bin_max; //make different names, incase it is called twice (to prevent ROOT warning)

  //TH1F* h_result = new TH1F(name,histo->GetTitle(),nBINS,bin_min,bin_max); 
  std::shared_ptr<TH1F> h_result(new TH1F(name,histo->GetTitle(),nBINS,bin_min,bin_max));

  for(int x=binsXstart; x<=binsXend; x++) {
    for(int y=binsYstart; y<=binsYend; y++) {
      if(FE!=-1 && frontEnd(x-1,y-1)!=FE) continue; //select specific FE

      //      std::cout << "x: " << x << " y: " << y << "  -> " << histo->GetBinContent(x,y) << std::endl;
      if(pixclass!=-1) {
	// using pxiel classes now instead of pixel types
	int pT=pixelClass(x-1,y-1,NpixelClasses); //starting form 0 !
	if(pT==0) {
	  std::cout << "WARNING: pixel index out of range!" << std::endl;
	  continue;
	} 
	if(pT!=pixclass) continue;
      }
      if(histo->GetBinContent(x,y)!=0 && histo->GetBinContent(x,y)!=zeroBin) //!!
	h_result->Fill( histo->GetBinContent(x,y)-zeroBin );
    }
  }
  if(h_result->Integral(0,nBINS+1)/h_result->Integral()>1.01) {
    std::cout << "ROOTanalysisHelper::IntegrateHisto()  WARNING  large number of under/overflow entries !" << std::endl;
  }
  return h_result;
}

//****************************************[maria elena] added this function

std::shared_ptr<TH1F> ROOTanalysisHelper::IntegrateHisto(const TH2* histo, Int_t bin_min, Int_t bin_max, Int_t pixclass, Int_t FE, Int_t zeroBin, int chip_type, int mask, int _Ntotmasks,std::vector<std::vector<unsigned short int> > cp_enable, std::vector<unsigned short int> fe_disable)
{
    Int_t binsXstart,binsXend,binsYstart,binsYend;
    binsXstart=1;
    binsXend=getNcol(chip_type);
    binsYstart=1;
    binsYend=getNrow(chip_type);
    const int nBINS(1000);
    
    //to construct range of result histo automatically
    //   Float_t max(0);
    //   for(int x=binsXstart; x<binsXend; x++)
    //     for(int y=binsYstart; y<binsYend; y++)
    //       if(histo->GetBinContent(x,y)>max) max=histo->GetBinContent(x,y);
    //  TH1F* h_result = new TH1F(histo->GetTitle(),histo->GetTitle(),50,0,max+1); //default
    
    TString name(histo->GetTitle());
    name+=pixclass;
    //name+=bin_max; //make different names, incase it is called twice (to prevent ROOT warning)
    
    //TH1F* h_result = new TH1F(name,histo->GetTitle(),nBINS,bin_min,bin_max);
    std::shared_ptr<TH1F> h_result(new TH1F(name,histo->GetTitle(),nBINS,bin_min,bin_max));
    
    if(fe_disable[FE]==1) {return h_result;}
    else{
    for(int x=binsXstart; x<=binsXend; x++) {
        for(int y=binsYstart; y<=binsYend; y++) {
            if(FE!=-1 && frontEnd(x-1,y-1,chip_type)!=FE) continue; //select specific FE
            if(! cp_enable[frontEnd(x-1,y-1,chip_type)][columnPair(x-1,y-1,chip_type)] || !isMasked(x-1, y-1,mask,_Ntotmasks,chip_type)) continue;
            
            //      std::cout << "x: " << x << " y: " << y << "  -> " << histo->GetBinContent(x,y) << std::endl;
            if(pixclass!=-1) {
                // using pxiel classes now instead of pixel types
                int pT=pixelClass(x-1,y-1,chip_type,getNpixelClasses(chip_type)); //starting form 0 !
                if(pT==0) {
                    std::cout << "WARNING: pixel index out of range!" << std::endl;
                    continue;
                }
                if(pT!=pixclass) continue;
            }
            if(histo->GetBinContent(x,y)!=0 && histo->GetBinContent(x,y)!=zeroBin) //!!
                h_result->Fill( histo->GetBinContent(x,y)-zeroBin );
        }
    }
    if(h_result->Integral(0,nBINS+1)/h_result->Integral()>1.01) {
        std::cout << "ROOTanalysisHelper::IntegrateHisto()  WARNING  large number of under/overflow entries !" << std::endl;
    }
        return h_result;}
}

//********************************************************************************

std::shared_ptr<TH1F> ROOTanalysisHelper::IntegrateHisto(const TH2* anaHisto, Int_t bin_min, Int_t bin_max, const TH2* cutHisto, Int_t cut_min, Int_t cut_max, Int_t pixclass, Int_t FE, Int_t zeroBin) 
{
  Int_t binsXstart,binsXend,binsYstart,binsYend;
  binsXstart=1;
  binsXend=Ncols;
  binsYstart=1;
  binsYend=Nrows;
  const int nBINS(1000);

  TString name(anaHisto->GetTitle());
  name+=pixclass;
  std::shared_ptr<TH1F> h_result(new TH1F(name,anaHisto->GetTitle(),nBINS,bin_min,bin_max));

  for(int x=binsXstart; x<=binsXend; x++) {
    for(int y=binsYstart; y<=binsYend; y++) {
      if(FE!=-1 && frontEnd(x-1,y-1)!=FE) continue; //select specific FE

      //      std::cout << "x: " << x << " y: " << y << "  -> " << anaHisto->GetBinContent(x,y) << std::endl;
      if(pixclass!=-1) {
	// using pxiel classes now instead of pixel types
	int pT=pixelClass(x-1,y-1,NpixelClasses); //starting form 0 !
	if(pT==0) {
	  std::cout << "WARNING: pixel index out of range!" << std::endl;
	  continue;
	} 
	if(pT!=pixclass) continue;
      }
      if(anaHisto->GetBinContent(x,y)!=0 && anaHisto->GetBinContent(x,y)!=zeroBin && cutHisto->GetBinContent(x,y)>cut_min && cutHisto->GetBinContent(x,y)<cut_max ) //!!
	h_result->Fill( anaHisto->GetBinContent(x,y)-zeroBin );
    }
  }
  if(h_result->Integral(0,nBINS+1)/h_result->Integral()>1.01) {
    std::cout << "ROOTanalysisHelper::IntegrateAnaHisto()  WARNING  large number of under/overflow entries !" << std::endl;
  }
  return h_result;
}

Float_t ROOTanalysisHelper::GetVariance(TH1F &histo)
{
  Float_t sum(0), sum_diff(0);
  Int_t n(histo.GetNbinsX());
  std::cout << "----------n " << n << std::endl;

  for(Int_t i=1; i<=n ;i++) {
    sum += histo.GetBinContent(i);
    std::cout << "== " << histo.GetBinContent(i) << std::endl;
  }
  Float_t mean(sum/n);
  std::cout << "----------mean " << mean << std::endl;

  for(Int_t i=0; i<n ;i++)
    sum_diff += pow( mean - histo.GetBinContent(i) ,2); 

  std::cout << "----------variance " << sqrt(sum_diff/n) << std::endl;
  return sqrt(sum_diff/n)/mean; //divided by mean!
}


int ROOTanalysisHelper::frontEnd(int col, int row) 
{
  int FE = -1;
  if ((int)row/160 == 0)  FE = (int)col/18;
  if ((int)row/160 == 1)  FE = 8 + (int)(143-col)/18;
  return FE;
}

int ROOTanalysisHelper::frontEnd(int col, int row, int chip_type) 
{
  int FE = -1;
  if(chip_type == PixGeometry::FEI2_CHIP || chip_type == PixGeometry::FEI2_MODULE){
    if ((int)row/160 == 0)  FE = (int)col/18;
    else if ((int)row/160 == 1)  FE = 8 + (int)(143-col)/18;
  }else if(chip_type == PixGeometry::FEI4_CHIP || chip_type == PixGeometry::FEI4_MODULE){
    if ((int)col/80 == 0)  FE = 0;
    else if ((int)col/80 == 1)  FE = 1;
  }
  return FE;
}

int ROOTanalysisHelper::columnPair(int moduleCol, int moduleRow)
{
  int cp = -1;
  if ((int)moduleRow/160 == 0)  cp = (int)(moduleCol % 18)/2;
  if ((int)moduleRow/160 == 1)  cp = 8 - (int)(moduleCol % 18)/2;
  return cp;
}

int ROOTanalysisHelper::columnPair(int moduleCol, int moduleRow, int chip_type)
{
  int cp = -1;
  if(chip_type == PixGeometry::FEI2_CHIP || chip_type == PixGeometry::FEI2_MODULE){
    if ((int)moduleRow/160 == 0)  cp = (int)(moduleCol % 18)/2;
    if ((int)moduleRow/160 == 1)  cp = 8 - (int)(moduleCol % 18)/2;
  }
  else if(chip_type == PixGeometry::FEI4_CHIP || chip_type == PixGeometry::FEI4_MODULE){
    if ((int)moduleCol/80 == 0) cp = (int) (moduleCol/2);
    else if ((int)moduleCol/80 == 1) cp = (int) (moduleCol/2-40);
  }
  return cp;  
}


int ROOTanalysisHelper::getFrontEndStartCol(int fe) {
  if (fe > 7) fe = 15-fe;
  int row = fe*18;
  return row;
}

int ROOTanalysisHelper::getFrontEndStartCol(int fe, int chip_type) {
  int col = -1;
  if(chip_type == PixGeometry::FEI2_CHIP || chip_type == PixGeometry::FEI2_MODULE){
    if (fe > 7) fe = 15-fe;
    col = fe*18;
  }
  /*else if(chip_type == PixGeometry::FEI4_CHIP || chip_type == PixGeometry::FEI4_MODULE){
    row = 0;*/
  else if(chip_type == PixGeometry::FEI4_CHIP){
      col = 0;
  }else if(chip_type == PixGeometry::FEI4_MODULE){
      if (fe == 0) col = 0;
      else if (fe == 1) col = 80;
    
  }
  return col;
}

int ROOTanalysisHelper::getFrontEndStopCol(int fe) {
  if (fe > 7) fe = 15-fe;
  int row = (fe+1)*18-1;
  return row;
}

int ROOTanalysisHelper::getFrontEndStopCol(int fe, int chip_type) {
  int col = -1;
  if(chip_type == PixGeometry::FEI2_CHIP || chip_type == PixGeometry::FEI2_MODULE){
    if (fe > 7) fe = 15-fe;
    col = (fe+1)*18-1;
  }/*else if(chip_type == PixGeometry::FEI4_CHIP || chip_type == PixGeometry::FEI4_MODULE){
    row = 336-1;
  }*/
  else if(chip_type == PixGeometry::FEI4_CHIP){
      col = 79;
  }else if(chip_type == PixGeometry::FEI4_MODULE){
      if (fe == 0) col = 79;
      else if (fe == 1) col = 159;
  }
  
  return col;
}

int ROOTanalysisHelper::getFrontEndStartRow(int fe) {
  int col = -1;
  if (fe < 8) col = 0;
  else col = 160;
  return col;
}

int ROOTanalysisHelper::getFrontEndStartRow(int fe, int chip_type) {
  int row = -1;
  if(chip_type == PixGeometry::FEI2_CHIP || chip_type == PixGeometry::FEI2_MODULE){
    if (fe < 8) row = 0;
    else row = 160;
  }/*else if(chip_type == PixGeometry::FEI4_CHIP){
    col = 0; 
  }else if(chip_type == PixGeometry::FEI4_MODULE){
    if (fe == 0) col = 0;
    else if (fe == 1) col = 80;
  }*/
  else if(chip_type == PixGeometry::FEI4_CHIP || chip_type == PixGeometry::FEI4_MODULE){
      row = 0;}
  return row;
}

int ROOTanalysisHelper::getFrontEndStopRow(int fe) {
  int col = -1;
  if (fe < 8) col = 159;
  else col = 319;
  return col;
}

int ROOTanalysisHelper::getFrontEndStopRow(int fe, int chip_type) {
  int row = -1;
  if(chip_type == PixGeometry::FEI2_CHIP || chip_type == PixGeometry::FEI2_MODULE){
    if (fe < 8) row = 159;
    else row = 319;
  }/*else if(chip_type == PixGeometry::FEI4_CHIP){
    col = 79; 
  }else if(chip_type == PixGeometry::FEI4_MODULE){
    if (fe == 0) col = 79;
    else if (fe == 1) col = 159;
  }*/
  else if(chip_type == PixGeometry::FEI4_CHIP || chip_type == PixGeometry::FEI4_MODULE){
      row = 336-1;
  }
  return row;
}

bool ROOTanalysisHelper::isMasked(int col, int row, int mask) {
  bool masked = false;

  int rest_row_even = row%(32);
  int rest_row_odd = (319-row)%(32);
  if (col%2 == 0 && rest_row_even < mask) masked = true;
  if (col%2 == 1 && rest_row_odd < mask) masked = true;

  return masked;
}

bool ROOTanalysisHelper::isMasked(int col, int row, int mask, int _Ntotmasks, int chip_type) {
  bool masked = false;

  if(chip_type == PixGeometry::FEI2_CHIP || chip_type == PixGeometry::FEI2_MODULE){
    int rest_row_even = row%(_Ntotmasks);
    int rest_row_odd = (319-row)%(_Ntotmasks);
    
    if (col%2 == 0 && rest_row_even < mask) masked = true;
    if (col%2 == 1 && rest_row_odd < mask) masked = true;
  }else if(chip_type == PixGeometry::FEI4_CHIP || chip_type == PixGeometry::FEI4_MODULE){
    int rest_row_even = row%(_Ntotmasks);
    int rest_row_odd = (335-row)%(_Ntotmasks);
    
    if (col%2 == 0 && rest_row_even < mask) masked = true;
    if (col%2 == 1 && rest_row_odd < mask) masked = true; //default ones

/*    if (col%2 == 1 && rest_row_even < mask) masked = true; //for SR1
    if (col%2 == 0 && rest_row_odd < mask) masked = true;  //for SR1*/
  }
  return masked;
}

std::string ROOTanalysisHelper::getPixelTypeString(int pixeltype)
{
  switch(pixeltype) {
  case 0:
    return "normal";
  case 1:
    return "long";
  case 2:
    return "ganged";
  case 3:
    return "long_ganged";
  case 4:
    return "interganged";
  case 5:
    return "long_interganged";
  }
  return "unknown";
}

std::string ROOTanalysisHelper::getPixelClassString(int pixeltype, int nclasses)
{
  if (nclasses == 2) {
    switch(pixeltype) {
    case 1:
      return "non_ganged";
    case 2:
      return "ganged";
    }
  } else if (nclasses == 3) {
    if (intimeAnalysis == false) {
      switch(pixeltype) {
      case 1:
	return "normal";
      case 2:
	return "special_non_ganged";
      case 3:
	return "special_ganged";
      }
    } else {
      switch(pixeltype) {
      case 1:
	return "normal_and_ganged";
      case 2:
	return "long";
      case 3:
	return "interganged";
      }
    }
  } else if (nclasses == 4) {
    switch(pixeltype) {
      case 1:
	return "normal";
      case 2:
	return "long";
      case 3:
	return "ganged";
      case 4:
	return "interganged";
    }
  }
  return "unknown";
}

//**************************************[maria elena] added this function
std::string ROOTanalysisHelper::getPixelClassString(int pixeltype, int chip_type, int nclasses)
{
    if(chip_type == PixGeometry::FEI4_CHIP || chip_type == PixGeometry::FEI4_MODULE){
        if(nclasses == 2){
        switch(pixeltype) {
            case 1:
                return "normal";
            case 2:
                return "long";
        }
    }
    }
    
    else{
        
    if (nclasses == 2) {
        switch(pixeltype) {
            case 1:
                return "non_ganged";
            case 2:
                return "ganged";
        }
    } else if (nclasses == 3) {
        if (intimeAnalysis == false) {
            switch(pixeltype) {
                case 1:
                    return "normal";
                case 2:
                    return "special_non_ganged";
                case 3:
                    return "special_ganged";
            }
        } else {
            switch(pixeltype) {
                case 1:
                    return "normal_and_ganged";
                case 2:
                    return "long";
                case 3:
                    return "interganged";
            }
        }
    } else if (nclasses == 4) {
        switch(pixeltype) {
            case 1:
                return "normal";
            case 2:
                return "long";
            case 3:
                return "ganged";
            case 4:
                return "interganged";
        }
    }
    return "unknown";
}
    return "unknown";
}
//*******************************************************

// 6 basic pixel types are classified into 3 classes
unsigned short ROOTanalysisHelper::pixelClass(int col, int row, int nclasses) 
{
  unsigned short pt(pixelType(col, row));
  assert(pt!=0);
  int type((int)log2(pt));
  if (nclasses == 2) {
    if(type==0||type==1||type==4||type==5) return 1; //non_ganged = normal & long & interganged & long_interganged
    if(type==2||type==3) return 2; //ganged = normal_ganged & long_ganged 
  } else if (nclasses == 3) {
    if (intimeAnalysis == false) {
      if(type==0) return 1; //normal
      if(type==1||type==4||type==5) return 2; //special_non_ganged = long & interganged & long_interganged
      if(type==2||type==3) return 3; //special_ganged = normal_ganged & long_ganged 
    } else {
      if(type==0||type==2||type==3) return 1; //normal && ganged
      if(type==1) return 2; //long 
      if(type==4||type==5) return 3; // interganged & long_interganged
    }
  } else if (nclasses == 4) {
    if(type==0) return 1; //normal
    if(type==1) return 2; //long 
    if(type==2||type==3) return 3; //ganged
    if(type==4||type==5) return 4; // interganged & long_interganged
  }
  return 0;
}

// 6 basic pixel types are classified into 3 classes
//*************************************[maria elena] added this function
unsigned short ROOTanalysisHelper::pixelClass(int col, int row, int chip_type, int nclasses)
{
    if(chip_type == PixGeometry::FEI4_CHIP || chip_type == PixGeometry::FEI4_MODULE){
        unsigned short pt(pixelType(col, row, chip_type));
        assert(pt!=0);
        int type((int)log2(pt));
        if (nclasses == 2) {
            if(type==0) return 1; //normal
            if(type==1) return 2; //special_non_ganged = long & interganged & long_interganged
        }
    }
    else{
    unsigned short pt(pixelType(col, row, chip_type));
    assert(pt!=0);
    int type((int)log2(pt));
    if (nclasses == 2) {
        if(type==0||type==1||type==4||type==5) return 1; //non_ganged = normal & long & interganged & long_interganged
        if(type==2||type==3) return 2; //ganged = normal_ganged & long_ganged
    } else if (nclasses == 3) {
        if (intimeAnalysis == false) {
            if(type==0) return 1; //normal
            if(type==1||type==4||type==5) return 2; //special_non_ganged = long & interganged & long_interganged
            if(type==2||type==3) return 3; //special_ganged = normal_ganged & long_ganged
        } else {
            if(type==0||type==2||type==3) return 1; //normal && ganged
            if(type==1) return 2; //long
            if(type==4||type==5) return 3; // interganged & long_interganged
        }
    } else if (nclasses == 4) {
        if(type==0) return 1; //normal
        if(type==1) return 2; //long
        if(type==2||type==3) return 3; //ganged
        if(type==4||type==5) return 4; // interganged & long_interganged
    }
    }
    return 0;
}
//*****************************************************

unsigned short ROOTanalysisHelper::pixelType(int col, int row) {

  // expects a column and row for a module 
  // ie a column between 0 and 143 and a row between 0 and 319  
  // columns and rows start counting from 0!!

  enum EPixelType { kNoPixel=0, kNormalPixel=1, kLongPixel=2, kGangedPixel=4,  kLongGangedPixel=8, kInterGangedPixel=16, kLongInterGangedPixel=32 };

  if (row > 159) row = 319-row; // count rows in FEs 7-15 backwards
  
  row = row%160;
  col = col%18;
  

  if ((col == 0) || (col == 17)){ // first or last column of FE
    if (row>=153) {
      if ((row&1) == 0) {
	return kLongInterGangedPixel; // long inter-ganged pixel
      }
      else {
	return kLongGangedPixel; // long ganged pixel
      }
    }
    else {
      return kLongPixel; // long pixel
    }
  }
  else { // all other columns
    if (row>=153) {
      if ((row&1) == 0) { // even row
	return kInterGangedPixel; // inter-ganged pixel
      }
      else {
	return kGangedPixel; // ganged pixel
      }
    }
    else {
      return kNormalPixel; // normal pixel
    }
  } 
  
}

unsigned short ROOTanalysisHelper::pixelType(int col, int row, int chip_type) {
  // expects a column and row for a module 
  // ie a column between 0 and 143 and a row between 0 and 319  
  // columns and rows start counting from 0!!
  enum EPixelType { kNoPixel=0, kNormalPixel=1, kLongPixel=2, kGangedPixel=4,  kLongGangedPixel=8, kInterGangedPixel=16, kLongInterGangedPixel=32 };

  if(chip_type == PixGeometry::FEI2_CHIP || chip_type == PixGeometry::FEI2_MODULE){ 
    if (row > 159) row = 319-row; // count rows in FEs 7-15 backwards
    
    row = row%160;
    col = col%18;

    if ((col == 0) || (col == 17)){ // first or last column of FE
      if (row>=153) {
	if ((row&1) == 0) {
	  return kLongInterGangedPixel; // long inter-ganged pixel
	}else {
	  return kLongGangedPixel; // long ganged pixel
	}
      } else {
	return kLongPixel; // long pixel
      }
    } else { // all other columns
      if (row>=153) {
	if ((row&1) == 0) { // even row
	  return kInterGangedPixel; // inter-ganged pixel
	} else {
	  return kGangedPixel; // ganged pixel
	}
      } else {
	return kNormalPixel; // normal pixel
      }
    } 
  }else if(chip_type == PixGeometry::FEI4_CHIP || chip_type == PixGeometry::FEI4_MODULE){
    if(col == 0 || col == 79 || col == 80 || col == 159){
      return kLongPixel; // long pixel
    }else{
      return kNormalPixel; // normal pixel
    }
  }
  return kNoPixel;
}

int ROOTanalysisHelper::pixelTypeInt(int col, int row) {

  int pixtype = -1;

  unsigned short pixel_type = pixelType(col,row); // normal, long etc 
  if (pixel_type > 0) {
    pixtype = int(log2(float(pixel_type))); // will give a number between 0 and 5, which can be used as index in vector
  }
  return pixtype;
  
}

int ROOTanalysisHelper::pixelTypeInt(int col, int row, int chip_type) {

  int pixtype = -1;
  
  unsigned short pixel_type = pixelType(col,row, chip_type); // normal, long etc 
  if (pixel_type > 0) {
    pixtype = int(log2(float(pixel_type))); // will give a number between 0 and 5, which can be used as index in vector
  }
  return pixtype;
  
}

int ROOTanalysisHelper::moduleRow(int fe, int row) { // convert row of front-end to row of module
  if (fe > 7) {
    row += 160;
  }
  return row;
}

int ROOTanalysisHelper::moduleRow(int fe, int row, int chip_type) { // convert row of front-end to row of module
  if(chip_type == PixGeometry::FEI2_CHIP){
    if (fe > 7) row += 160;
  }else if(chip_type == PixGeometry::FEI4_CHIP){
    row = 0;
  }else if(chip_type == PixGeometry::FEI4_MODULE){
    row = 0;
  }
  return row;
}

int ROOTanalysisHelper::moduleCol(int fe, int col) { // convert col of front-end to col of module
  if (fe > 7) {
    fe = 15-fe;
    col = 17-col;
  }
  col += fe*18;
  return col;
}

int ROOTanalysisHelper::moduleCol(int fe, int col, int chip_type) { // convert col of front-end to col of module //maria elena modified
  if(chip_type == PixGeometry::FEI2_CHIP){
    if (fe > 7) {
      fe = 15-fe;
      col = 17-col;
    }
    col += fe*18;
  }else if(chip_type == PixGeometry::FEI4_CHIP){
    col = col + 0;
  }else if(chip_type == PixGeometry::FEI4_MODULE){
    if(fe == 0) col = col + 0; 
    else if(fe == 1) col = col+80;
  }
  return col;
}

/**
 *  Returns value for which x per cent of the entries in the histogram are lower than this value.
 *  Input x  as fraction, e.g. x=0.05 (for 5%)
 */
Float_t ROOTanalysisHelper::lowestXpercent(const float percentCut, TH1F* h) 
{
  Float_t tot( h->Integral() );
  Float_t min( h->GetXaxis()->GetXmin() );
  Float_t max( h->GetXaxis()->GetXmax() );
  Float_t step( (max-min)/500 );
  Int_t minBin( h->FindBin(min) );
  Float_t x(min);
  for(; x<max ; x+=step) {
    Int_t cutBin( h->FindBin(x) );
    Float_t fraction( h->Integral(minBin,cutBin)/tot );
    if(fraction >= percentCut) break;
  }
  return x;
}

Float_t ROOTanalysisHelper::highestXpercent(const float percentCut, TH1F* h) 
{
  Float_t tot( h->Integral() );
  Float_t min( h->GetXaxis()->GetXmin() );
  Float_t max( h->GetXaxis()->GetXmax() );
  Float_t step( (max-min)/500 );
  Int_t maxBin( h->FindBin(max) );
  Float_t x(max);
  for(; x>min ; x-=step) {
    Int_t cutBin( h->FindBin(x) );
    Float_t fraction( h->Integral(cutBin,maxBin)/tot );
    if(fraction >= percentCut) break;
  }
  return x;
}

 int ROOTanalysisHelper::getNrow(int chip_type){
  int Nrow=0; // # of row in one moduel
  if(chip_type == PixGeometry::FEI2_MODULE) Nrow = 320;
  else if(chip_type == PixGeometry::FEI2_CHIP) Nrow = 160;
  else if(chip_type == PixGeometry::FEI4_MODULE) Nrow = 336;
  else if(chip_type == PixGeometry::FEI4_CHIP) Nrow = 336;
  return Nrow;
}

 int ROOTanalysisHelper::getNcol(int chip_type){
  int Ncol=0; // # of column in one moduel
  if(chip_type == PixGeometry::FEI2_MODULE) Ncol = 144;
  else if(chip_type == PixGeometry::FEI2_CHIP) Ncol = 18;
  else if(chip_type == PixGeometry::FEI4_CHIP) Ncol = 80;
  else if(chip_type == PixGeometry::FEI4_MODULE) Ncol = 160;
  return Ncol;
}


int ROOTanalysisHelper::getNfrontEnd(int chip_type){
  int Nfe=0; // # of FEs in one module
  if(chip_type == PixGeometry::FEI2_MODULE) Nfe = 16;
  else if(chip_type == PixGeometry::FEI2_CHIP) Nfe = 1;
  else if(chip_type == PixGeometry::FEI4_CHIP) Nfe = 1;
  else if(chip_type == PixGeometry::FEI4_MODULE) Nfe = 2;
  return Nfe;
}

int ROOTanalysisHelper::getNcp(int chip_type){
  int Ncp=0; // # of the enable column pairs
  if(chip_type == PixGeometry::FEI2_MODULE) Ncp = 9;
  else if(chip_type == PixGeometry::FEI2_CHIP) Ncp = 9;
  else if(chip_type == PixGeometry::FEI4_CHIP) Ncp = 40;
  else if(chip_type == PixGeometry::FEI4_MODULE) Ncp = 40;
  return Ncp;
}

int ROOTanalysisHelper::getNpixel(int chip_type){
  int Npix=0; // # of pixel in one FE
  if(chip_type == PixGeometry::FEI2_MODULE) Npix = 2880;
  else if(chip_type == PixGeometry::FEI2_CHIP) Npix = 2880;
  else if(chip_type == PixGeometry::FEI4_CHIP) Npix = 26880;
  else if(chip_type == PixGeometry::FEI4_MODULE) Npix = 26880;
  return Npix;
}

#endif

