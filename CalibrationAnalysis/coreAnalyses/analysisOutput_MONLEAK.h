#ifndef _analysisOutputMONLEAK_h_
#define _analysisOutputMONLEAK_h_

#include <TString.h>
#include <TH2F.h>
#include "analysisOutput_Base.h"
#include "PixelCoralClientUtils/PixelMap.h"


class ResultMONLEAK : public AnalysisResult { //this is for one scan and one module
 public:
  ResultMONLEAK() :AnalysisResult()
    {
      //module values
      iLeak_all=-1;
      iLeak_L0=-1;
      iLeak_L1=-1;
      iLeak_L2=-1;
      iLeak_disks=-1;
      iLeakTotal=0;
      iLeak_FEmax=-1;
      iLeak_FEmin=-1;
      iLeak_FE0=-1;
      iLeak_FE1=-1;
      iLeak_FE2=-1;
      iLeak_FE3=-1;
      iLeak_FE4=-1;
      iLeak_FE5=-1;
      iLeak_FE6=-1;
      iLeak_FE7=-1;
      iLeak_FE8=-1;
      iLeak_FE9=-1;
      iLeak_FE10=-1;
      iLeak_FE11=-1;
      iLeak_FE12=-1;
      iLeak_FE13=-1;
      iLeak_FE14=-1;
      iLeak_FE15=-1;
      //iLeak_FE = -1;
    };
  ~ResultMONLEAK() {};

  //module values
  Float_t iLeak_all;
  Float_t iLeak_L0;
  Float_t iLeak_L1;
  Float_t iLeak_L2;
  Float_t iLeak_disks;
  Float_t iLeakTotal;
  Float_t iLeak_FEmax;
  Float_t iLeak_FEmin;
  Float_t iLeak_FE0;
  Float_t iLeak_FE1;
  Float_t iLeak_FE2;
  Float_t iLeak_FE3;
  Float_t iLeak_FE4;
  Float_t iLeak_FE5;
  Float_t iLeak_FE6;
  Float_t iLeak_FE7;
  Float_t iLeak_FE8;
  Float_t iLeak_FE9;
  Float_t iLeak_FE10;
  Float_t iLeak_FE11;
  Float_t iLeak_FE12;
  Float_t iLeak_FE13;
  Float_t iLeak_FE14;
  Float_t iLeak_FE15;

  std::shared_ptr<TH2F> iLeakMap;
  std::shared_ptr<TH1F> iLeak;
  std::shared_ptr<TH1F> iLeak_FE;

  /*  std::shared_ptr<TH2F> iLeakPixels_L0; */
  /*   std::shared_ptr<TH2F> iLeakPixels_L1; */
  /*   std::shared_ptr<TH2F> iLeakPixels_L2; */
  /*   std::shared_ptr<TH2F> iLeakPixels_disks; */

 public:
  //ClassDef(ResultMONLEAK,1) //not 0 -> activate I/O  ; not needed at the moemnt, put in if needed!
};

#endif
