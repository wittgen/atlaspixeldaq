#include "TDACFASTTUNEanalysisROOT.h"
#include <iostream>
#include <fstream>
#include "TFile.h"
#include "TF1.h"



TDACFASTTUNEanalysisROOT::TDACFASTTUNEanalysisROOT(): _Nevents(0)
{
  std::cout << std::endl;
  std::cout << "-------------------------------" << std::endl;
  std::cout << "   TDACFASTTUNE Analysis Algorithm   " << std::endl;
  std::cout << "-------------------------------" << std::endl;
  std::cout << std::endl;

  // this analysis should verify the output / final histogram of a TDAC_FAST_TUNE



}



TDACFASTTUNEanalysisROOT::~TDACFASTTUNEanalysisROOT()
{
  //
}






void TDACFASTTUNEanalysisROOT::Initialize()
{
  if(!m_scan) {
    std::cout << std::endl;
    std::cout << "TDACFASTTUNEanalysisROOT::Initialize ERROR: input data pointer is null" << std::endl;
    std::cout << std::endl;
    return;
  }

  AnalysisInputTDACFASTTUNE* occupancy = (AnalysisInputTDACFASTTUNE*) m_scan->At(0);
  if(!occupancy || !occupancy->histo_tdac || !occupancy->histo_occ[(occupancy->Ntdacsteps)-1]) return; 
 
  _Nevents = occupancy->Nevents;
  _Ntdacsteps = occupancy->Ntdacsteps;

  
  std::cout << "Number of Events  = " << _Nevents << std::endl;
  std::cout << "Number of modules = " << m_scan->GetEntries() << std::endl;
  std::cout << "Number of steps in TDAC tuning = " << _Ntdacsteps<< std::endl;
} 
  
  
 

void TDACFASTTUNEanalysisROOT::Calculate() 
{
  //TFile f("test.root","recreate");
  
  
  std::cout<<"TDACFASTTUNEanalysisROOT::Calculate()::started!"<<std::endl;
  // initializing, might not be necessary  
  Passed = false;
  
  if(m_scan->GetEntries() == 0) return;
  if(m_scan->GetEntries()>7) {
    std::cout << "ERROR, pp0 with " << m_scan->GetEntries() << " modules" << std::endl;
    //return;
  }

   
  if(m_logLevel>=CAN::kDiagnostics1Log) std::cout << "Number of Modules = " << m_scan->GetEntries() << std::endl;
  for(Int_t i = 0; i < m_scan->GetEntries(); i++) 
    {
    
      // if(m_logLevel>=CAN::kDiagnostics2Log) 
      std::cout << " Module #" << i+1 << std::endl;
      
      AnalysisInputTDACFASTTUNE* occupancy = (AnalysisInputTDACFASTTUNE*) m_scan->At(i);
      ResultTDACFASTTUNE* module_result = new ResultTDACFASTTUNE();
      
      // some variables 
      _MeanPercentage = 100;
      _RMSPercentage = 100;
      _Mean = 0;
      _RMS = 0;
      _chi2 = 100;
      _Pixel_no_hits = 0;
      _Pixel_all_hits = 0;
      _Pixel_more_hits = 0;
      _Mean_tdac_all = 0;
      _allFEok = true;
      // initialize empty vector
      _vec_extraHits.clear();
      

      if(!occupancy->histo_occ[_Ntdacsteps-1] || occupancy->histo_occ[_Ntdacsteps-1]->GetEntries() == 0)
	{
	  std::cout<<"TDACFASTTUNEanalysis: WARNING no histogram for module "<<module_result->moduleName<<", skipping this one."<<std::endl;
	  module_result->passed=false;
	  m_result->Add(module_result);
	 
	  continue;
	}
       if(!occupancy->histo_tdac || occupancy->histo_tdac->GetEntries() == 0)
	{
	  std::cout<<"TDACFASTTUNEanalysis: WARNING no histogram for module "<<module_result->moduleName<<", skipping this one."<<std::endl;
	  module_result->passed=false;
	  m_result->Add(module_result);
	 
	  continue;
	}
      
      module_result->pp0Name = ((AnalysisInputTDACFASTTUNE*)m_scan->At(i))->pp0Name;
      module_result->moduleName = ((AnalysisInputTDACFASTTUNE*)m_scan->At(i))->moduleName;
      
      AdditionalCalculations(i);

      // see if module passes our cuts
      bool passedMean = false;
      bool passedRMS = false;
      bool passedChi2 = false;

      if(_MeanPercentage >= Mean_lowerCut && _MeanPercentage <= Mean_upperCut) passedMean = true;
      if(_RMSPercentage <= RMS_upperCut) passedRMS = true;
      if(_chi2 <= max_chi2) passedChi2 = true;

      if(passedMean && passedRMS && passedChi2 && _allFEok) Passed = true;
      else Passed = false;

      module_result->passed = Passed;
      module_result->passedMean = passedMean;
      module_result->passedRMS = passedRMS;
      module_result->passedChi2 = passedChi2;
      module_result->passedTDACs = _allFEok;
      std::cout<<"Result for module "<<module_result->moduleName<<" is: "<<((module_result->passed)? "OK":"FAILED")<<std::endl;
      
      // give out vector of extra hits
      for(int step = 0; step < _Ntdacsteps; step++){
	module_result->vec_extraHits.push_back(_vec_extraHits[step]);
      }

      module_result->MeanPercentage = _MeanPercentage;
      module_result->RMSPercentage = _RMSPercentage;
      module_result->Chi2 = _chi2;
      module_result->MeanTDAC = _Mean_tdac_all;
      m_result->Add(module_result);
      
    } // modules
  
  if(m_logLevel>=CAN::kDiagnostics1Log) this->Print();

  //f.Close();
} 
 
void TDACFASTTUNEanalysisROOT::AdditionalCalculations(int module)
{
  std::cout<<"TDACFASTTUNEanalysisROOT::AdditionalCalculations: started"<<std::endl;
  
  // checking TDAC values per frontend, loop over frontends
  for(int fe = 0; fe < 16; fe++){
    float mean_tdac = 0;
    float low_percentage = 0;
    float high_percentage = 0;
    bool tdac_ok = false;
    
    // get mean TDAC value per FE
    std::shared_ptr<TH1F> histo_tdac = helper.IntegrateHisto(((AnalysisInputTDACFASTTUNE*)m_scan->At(module))->histo_tdac, 0, 1000, -1, fe);
    mean_tdac = histo_tdac->GetMean();
    
    //get fractions of pixel with TDACs at the edges
    low_percentage = helper.PercentFailedMinCut(histo_tdac, TDAC_toolow);
    high_percentage = helper.PercentFailedMaxCut(histo_tdac, TDAC_toohigh);

    
    // check if the average TDAC values for this FE are ok
    if(mean_tdac > TDACmean_lowerCut && mean_tdac < TDACmean_upperCut && low_percentage < Percentage_lowTDAC && high_percentage < Percentage_highTDAC) tdac_ok = true;
    
    // set status for module
    _allFEok =  tdac_ok && _allFEok;

  } 

  std::cout<<"the TDAC values for all FEs are "<<(_allFEok? "ok":"not ok")<<std::endl;
  
  // get mean TDAC per module to return to console
  std::shared_ptr<TH1F> histo_tdac_all = helper.IntegrateHisto(((AnalysisInputTDACFASTTUNE*)m_scan->At(module))->histo_tdac, 0, 1000, -1, -1);
  _Mean_tdac_all = histo_tdac_all->GetMean();

  // checking occupancies in every tdac step, most calculations only done for last step histogram
  for(int step = 0; step < _Ntdacsteps; step++)
    {
      std::shared_ptr<TH1F> histo_occ = helper.IntegrateHisto(((AnalysisInputTDACFASTTUNE*)m_scan->At(module))->histo_occ[step], 0, 1000, -1, -1);
      
      // check histogram from last step of the tuning
      if(step == _Ntdacsteps-1)
	{
	  _Mean = histo_occ->GetMean();
	  std::cout<<"mean value of seen hits: "<<_Mean<<" for module "<<module<<std::endl;
	  
	  
	  _RMS = histo_occ->GetRMS();
	  std::cout<<"RMS is "<<_RMS<<std::endl;
	  
	  std::cout<<"number of injected events "<<_Nevents<<std::endl;
	  _MeanPercentage = (_Mean/_Nevents)*100;
	  std::cout<<"mean percentage: "<<_MeanPercentage<<std::endl;
	  
	  _RMSPercentage = (_RMS/_Nevents)*100; 
	  std::cout<<"rms percentage: "<<_RMSPercentage<<std::endl;
	  
	  // do some fitting to see whether the distribution has a nice shape or not
	  TF1* fit = new TF1("fit", "gaus", 0, _Nevents);
	  histo_occ->Fit("fit", "0RQ");
	  
	  // chisquare per degree of freedom
	  _chi2 = (fit->GetChisquare())/(_Nevents-1);
	  float fitmean = (fit->GetParameter(1))/(_Nevents)*100;
	  float fitrms = (fit->GetParameter(2))/(_Nevents)*100;
	  std::cout<<"chisquare/dof of gauss fit: "<<_chi2<<" with mean at "<<fitmean<<" percent and a standard deviation of "<<fitrms<<" percent"<<std::endl;
	  
	  // get number of pixel seeing no or all hits
	  _Pixel_no_hits = histo_occ->GetBinContent(histo_occ->FindBin(0));
	  _Pixel_all_hits = histo_occ->GetBinContent(histo_occ->FindBin(_Nevents));
	  
	  std::cout<<"For this module "<<_Pixel_no_hits<<" pixel see no hits and "<<_Pixel_all_hits<<" see all injected hits"<<std::endl;
	}
      // check for extra hits in every step
      _Pixel_more_hits = histo_occ->Integral(histo_occ->FindBin(_Nevents)+1, 1000);
      _vec_extraHits.push_back(_Pixel_more_hits);
    }
}




 
void TDACFASTTUNEanalysisROOT::QualifyResults() 
{
  allModulesPassed = true;
  for(int i = 0; i<m_result->GetEntries(); i++)
    {
      ResultTDACFASTTUNE* module_result = (ResultTDACFASTTUNE*)m_result->At(i);
      if(!module_result->passed)
	{
	  allModulesPassed = false;
	  break;
	}
    }
  

  std::cout<<"TDACFASTTUNEanalysisROOT::QualifyResults():  "<<(allModulesPassed? "OK":"FAILED")<<std::endl;
  
  if(allModulesPassed) m_status= kPassed;
  else  m_status=kFailed;
  
}


void TDACFASTTUNEanalysisROOT::CheckForKnownProblems() //setting of failure bits
{

}


void TDACFASTTUNEanalysisROOT::Print()
{
  std::cout << std::endl;
  std::cout << "-------------------------------------" << std::endl;
  std::cout << "TDACFASTTUNE Scan Analysis Results: " << std::endl;
  std::cout << "-------------------------------------" << std::endl;
  std::cout << std::endl;

  std::cout << "-------------------------------------" << std::endl;
  std::cout << std::endl << std::endl;
}
