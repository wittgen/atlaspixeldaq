#ifndef _InLinkanalysisROOT_h_
#define _InLinkanalysisROOT_h_ _

#include "ROOTanalysisBase.h"
#include "analysisInput_InLink.h"
#include "analysisOutput_InLink.h"
#include <iostream>
#include <TFile.h>
#include <TString.h>



class InLinkanalysisROOT : public ROOTanalysisBase 
{
 public:
  InLinkanalysisROOT();
  ~InLinkanalysisROOT();
    
  void Initialize();

  void Calculate(); //internal calculations

  void QualifyResults(); //apply cuts from analysis config

  void CheckForKnownProblems(); //setting of failure bits

  //The cuts are directly accessible from the AutomaticAnalysis
  Float_t cut_I_pin_mod_min; //cut on min Ipin 
  Float_t cut_I_pin_mod_max; //cut on max Ipin
  Float_t cut_I_pin_Pp0_min; //cut on min Ipin per Pp0 
  Float_t cut_I_pin_Pp0_max; //cut on max Ipin per Pp0

 private:
  void WriteResults();

};


#endif

