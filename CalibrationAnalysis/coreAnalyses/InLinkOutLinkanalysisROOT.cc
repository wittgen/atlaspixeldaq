//  2008-06-27 Shih-Chieh Hsu 
//    Core of inlink outlink analysis. 
//    The analysis is adapted from  Applications/Pixel/PixLib/PixConnectivityTest/PixConnectivityTest_phase2.cxx
//    bool PixConnectivityTest::endInlinkOutlink(bool test)
//
//    Calculate(): 
//     1. Iterate each module, get the corresponding InLink and outLink1A/2A ID 
//     and calculate the bin index in the  scan output, a 2D histogram.
//     2. Retrieve  difference of Rx pin currents with/without VIset for Power measurement of Opto PP0
//     3. Calculate the bit ratio. D board has only one bit ratio but B board has two bits.
//     4. Require pass minimum pin current and bit rato 

#ifndef _InLinkOutLinkanalysisROOT_CC_
#define _InLinkOutLinkanalysisROOT_CC_

#include <iomanip>

#include "InLinkOutLinkanalysisROOT.h"

InLinkOutLinkanalysisROOT::InLinkOutLinkanalysisROOT()
{
  std::cout << "InLinkOutLinkanalysisROOT:  constructed." << std::endl;
} 


InLinkOutLinkanalysisROOT::~InLinkOutLinkanalysisROOT()
{
  //WriteResults();
}

void InLinkOutLinkanalysisROOT::WriteResults()
{
  TFile f("resultsInLinkOutLink.root","update");
  f.WriteTObject(m_result.get(),"scanHistos","SingleKey"); //as one array
  f.Close();  
}

void InLinkOutLinkanalysisROOT::Initialize()
{
  //! Don't hard code here. Take values from analysis configuration, defaults in AutomaticAnalysis.

  if(cut_ratio_bit_min<=0.0){
    std::cout << "WARNING   Apparent problem in reading analysis config, all cuts are zero.  -> Setting default values." << std::endl;
    cut_ratio_bit_min=0.45; //on the ratio of bits
  }
}  

void InLinkOutLinkanalysisROOT::Calculate() 
{
  std::cout << "INFO InLinkOutLinkanalysisROOT: **************************************" << std::endl;
  std::cout << "INFO InLinkOutLinkanalysisROOT: InLinkOutLinkanalysis::Calculate  starting" << std::endl;
  m_scan->R__FOR_EACH(AnalysisInputInLinkOutLink,PrintInfo)(); 


  float drxCut = cut_drx_min;

  // Loop over modules
  for(Int_t i=0; i<m_scan->GetEntries(); i++) {
    ResultInLinkOutLink* module_result = new ResultInLinkOutLink();
    AnalysisInputInLinkOutLink* module_scan = (AnalysisInputInLinkOutLink*) m_scan->At(i);

    module_result->rodName = module_scan->rodName;
    module_result->pp0Name = module_scan->pp0Name;
    module_result->moduleName = module_scan->moduleName;

    const TH2* h_link =  module_scan->histo_InLinkOutLink;
    if( !h_link || (h_link &&  h_link->GetEntries()==0)){
      std::cerr << "ERR InLinkOutLinkanalysisROOT: WARNING  no histos for module " << module_result->moduleName << " -> skipping." << std::endl;
      module_result->passed=false;
      m_result->Add( module_result );
      continue;
    }

    

    // Get drx : difference of RX pin current with/without turnning on Viset
    int inX     = module_scan->inLink +1;
    int fmt     = module_scan->outLink1A/16;//0~7
    int ch      = module_scan->outLink1A%16;
    int inY     = fmt*4 +ch +1;
    int fmt2    = module_scan->outLink2A/16;
    int ch2     = module_scan->outLink2A%16;
    int inY2    = fmt2*4+ch2+1;
    int fmtGroup= fmt/2;//0~3  every two formatters in a group

    int rxinY        = 33+1;
         if(fmtGroup == 0 ) rxinY+=0;
    else if(fmtGroup == 1 ) rxinY+=3;
    else if(fmtGroup == 2 ) rxinY+=4;
    else if(fmtGroup == 3 ) rxinY+=7;

    // daq/RodDaq/RodCrate/BocCard.h defines MONITOR_CHANNEL_TYPE[BOC_MONITOR_CHANNELS=12]
    //  0~7 monitor currents,  8,9  monitor voltage 10,11 monitor temp
    // Applications/Pixel/PixLib/PixController/RodPixController.cxx:RodPixController::fmtCountLinkmap
    //  fills 12 monitor channels to histogram Ybin 34~45
    //  0~7 monitor currents,  8,9  monitor voltage 10,11 monitor temp
    
    //Check Interference
    module_result->noisy1 =false;
    module_result->noisy2 =false;
    for(int idx_inlink=1;idx_inlink<=46;idx_inlink++){
       if(idx_inlink == inX) continue;

       int ol=idx_inlink-1;
       if (ol==0 || ((ol%12)>1 && (ol%12)<10)) {
       
         if(                             h_link->GetBinContent(idx_inlink, inY )!=0) module_result->noisy1=true;
         if(module_scan->outLink2A>=0 && h_link->GetBinContent(idx_inlink, inY2)!=0) module_result->noisy2=true; 
       }//ol
    }
    
    //Get pedestal
    //check formatter counts in the same Rx. 
    int ped_inlink=1;
    double min_delta=999999;
    double min_rxIPin=999999;
    int    num_noisy_link=0;
    int    num_ped=0;
    double sum_ped=0;
    double sum2_ped=0;
    for(int idx_inlink=1;idx_inlink<=46;idx_inlink++){
       //skip the same plugin
       if(idx_inlink!=1 && (idx_inlink/12 == inX/12)) continue;
       int ol=idx_inlink-1;
       if (ol==0 || ((ol%12)>1 && (ol%12)<10)) {
         num_ped++;
         sum_ped+=h_link->GetBinContent(idx_inlink,rxinY);
         sum2_ped+=(h_link->GetBinContent(idx_inlink,rxinY)*h_link->GetBinContent(idx_inlink,rxinY));

         int num_noisy_link_count=0;
         double delta=0;  
         for(int idx_ch = 1; idx_ch<=8;idx_ch++){
           int idx_y = idx_ch+fmtGroup*8;
           if(idx_y==inY) continue;
           if(module_scan->outLink2A>=0 && idx_y==inY2) continue;

           delta+=fabs(h_link->GetBinContent(inX, idx_y)-h_link->GetBinContent(idx_inlink,idx_y));
           if(h_link->GetBinContent(idx_inlink,idx_y)>0) num_noisy_link_count++;
         } 
         if(delta<min_delta){
            min_delta=delta;
            ped_inlink=idx_inlink;
	    min_rxIPin=h_link->GetBinContent(idx_inlink, rxinY);
            num_noisy_link= num_noisy_link_count;
         }
         if(delta==min_delta){
            if(h_link->GetBinContent(idx_inlink, rxinY)<min_rxIPin){
	      ped_inlink=idx_inlink;
	      min_rxIPin=h_link->GetBinContent(idx_inlink, rxinY);
              num_noisy_link= num_noisy_link_count;
	    }
         }
      }//ol
    }

    double drx_res = 0.0;
    drx_res = h_link->GetBinContent(inX, rxinY)-h_link->GetBinContent(ped_inlink, rxinY);

    std::cout << "InLinkOutLinkanalysisROOT: Summary "
         << " "<< module_result->rodName
         << " "<< module_result->pp0Name
         << " "<< module_result->moduleName
         << " (inX,inY,inY2,rxY)=( " << inX<<" , "<<inY<<" , "<< inY2<< " , "<< rxinY <<" ) "
         << " numPed "<< num_ped
         << " ped= " <<  sum_ped/num_ped <<" +- "<< sqrt(fabs(sum2_ped-sum_ped*sum_ped/num_ped)/(num_ped-1))
         << " numNoisyLink= "<< num_noisy_link
         << " dRx( " << ped_inlink <<")= "<< drx_res
         << " dRx( 1 ) = "<< h_link->GetBinContent(inX, rxinY)-h_link->GetBinContent(1, rxinY)
         << " IsNoisy1 " << module_result->noisy1 <<" IsNoisy2 "<< module_result->noisy2
         <<std::endl;

    std::cout << "InLinkOutLinkanalysisROOT: " << module_result->moduleName <<" inX: "<< std::setw(2) <<inX        <<" fmt,ch "<< fmt <<"," << ch<<" ";
    for(int idx_ch=1;idx_ch<=8;idx_ch++) std::cout << std::setw(5)<< h_link->GetBinContent(inX      ,idx_ch+fmtGroup*8)<<" ";
    std::cout << " RxIPin " << h_link->GetBinContent(inX         ,rxinY) 
         << " noisy " << module_result->noisy1 <<" "<< module_result->noisy2
	 <<std::endl;

    std::cout << "InLinkOutLinkanalysisROOT: " << module_result->moduleName <<" ped: "<< std::setw(2) <<ped_inlink <<" fmt,ch "<< fmt <<"," << ch<<" ";
    for(int idx_ch=1;idx_ch<=8;idx_ch++) std::cout << std::setw(5)<<h_link->GetBinContent(ped_inlink,idx_ch+fmtGroup*8)<<" ";
    std::cout << " RxIPin " << h_link->GetBinContent(ped_inlink  ,rxinY)
         << " noisy " << module_result->noisy1 <<" "<< module_result->noisy2
	 <<std::endl;

    std::cout << "InLinkOutLinkanalysisROOT: " << module_result->moduleName <<" org: "<< std::setw(2) << 1         <<" fmt,ch "<< fmt <<"," << ch<<" ";
    for(int idx_ch=1;idx_ch<=8;idx_ch++) std::cout << std::setw(5)<<h_link->GetBinContent(1         ,idx_ch+fmtGroup*8)<<" ";
    std::cout << " RxIPin " << h_link->GetBinContent(1           ,rxinY) 
         << " noisy " << module_result->noisy1 <<" "<< module_result->noisy2
	 <<std::endl;
    

    //for(int ix=1;ix<=h_link->GetNbinsX();ix++){
    //    std::cout <<"ix"<< std::setw(5) << ix<<" ";
    //    for(int iy=1;iy<=32;iy++){
    //      std::cout << std::setw(5)<< h_link->GetBinContent(ix,iy)<<" ";
    //    }
    //    std::cout << std::endl;
    //}
 
    if(m_logLevel>=CAN::kDiagnostics1Log){
       std::cout <<h_link->GetName()<<std::endl;
       std::cout <<"InLinkOutLinkanalysisROOT: " 
                 <<" module " << module_scan->rodName<<" "<<module_scan->moduleName <<"(inlink, outlink1A, outLink2A)=(" 
		 << module_scan->inLink <<","<<module_scan->outLink1A << "," <<module_scan->outLink2A <<") "
                 <<" (in,fmt)=("<< inX <<","<<fmt <<")"
                 <<" (in,hisY)=("<< inX<<","<<inY<<") "
                 <<" binContent="<<h_link->GetBinContent(inX, inY)
                 <<" subtract from "<< h_link->GetBinContent(1, inY)
                 <<std::endl;
    }
    if(m_logLevel>=CAN::kDiagnostics2Log){ 
      std::cout <<"InLinkOutLinkanalysisRoot: Matrix "<<std::endl;
      for(int nn=0;nn<h_link->GetNbinsY();nn++){
        printf("%2d ",nn); 
        for(int mm=0;mm<48;mm++){
         if(mm==0 || ((mm/12)== (module_scan->inLink/12)) )  
            printf("%9.3f ",h_link->GetBinContent(mm+1,nn+1));
        }
        std::cout <<std::endl;
      } 
    }


    module_result->drx_res    = drx_res;
    module_result->passed=true;
    if (module_scan->outLink1A >= 0) {
      // Get ratio
      double ratio_res = h_link->GetBinContent(inX,inY)/h_link->GetBinContent(inX,32+1);
      module_result->ratiobits1 = ratio_res;
      
      if (module_result->ratiobits1 > cut_ratio_bit_min &&
          module_result->drx_res    > drxCut) {
        module_result->passed=true;
      } else {
        module_result->passed=false;
      }
    }
    
    if (module_scan->outLink2A >= 0) {
      // If B-layer: drx cut doubles
      drxCut =  2.0*cut_drx_min;
      double ratio_res = h_link->GetBinContent(inX,inY2)/h_link->GetBinContent(inX,32+1);
      module_result->ratiobits2 = ratio_res; 
      
      if (module_result->ratiobits1 > cut_ratio_bit_min && 
          module_result->ratiobits2 > cut_ratio_bit_min && 
	  module_result->drx_res    > drxCut) {
        module_result->passed=true;
      } else {
        module_result->passed=false;
      }
    }

    std::cout << "INFO InLinkOutLinkanalysisROOT: module " << module_result->rodName <<" "<< module_result->moduleName 
         << " RxPin " <<module_result->drx_res
         << " ratio1 "<<module_result->ratiobits1
         << " ratio2 "<<module_result->ratiobits2
         << " is: " << ( (module_result->passed)?  "OK":"FAILED" ) << std::endl;
    m_result->Add( module_result );

  }// loop each modules


  std::cout << "INFO InLinkOutLinkanalysisROOT: analysis finished." << std::endl;
}


void InLinkOutLinkanalysisROOT::QualifyResults() 
{
  bool allPassed(true);
  for(Int_t i=0; i<m_result->GetEntries(); i++) { 
    ResultInLinkOutLink* module_result = (ResultInLinkOutLink*)m_result->At(i); 
    if(!module_result->passed) { 
      allPassed=false;
      break;
    }
  }  
  if(allPassed)
    m_status=kPassed;
  else 
    m_status=kFailed;

  std::cout << "InLinkOutLinkanalysis::QualifyResults  Total result is :" << ( (allPassed)? "OK":"FAILED" ) << std::endl;
  std::cout << "*************************************" << std::endl;
}


#endif
