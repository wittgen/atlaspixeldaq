#ifndef _BASICTHRanalysisROOT_h_
#define _BASICTHRanalysisROOT_h_ _

#include "ROOTanalysisBase.h"
#include "analysisInput_BASICTHR.h"
#include "analysisOutput_BASICTHR.h"
#include <iostream>
#include <TFile.h>
#include <TString.h>



class BASICTHRanalysisROOT : public ROOTanalysisBase 
{
 public:
  BASICTHRanalysisROOT();
  ~BASICTHRanalysisROOT();
    
  void Initialize();

  void Calculate(); //internal calculations

  void QualifyResults(); //apply cuts from analysis config

  void CheckForKnownProblems() {};


  //The cuts are directly accessible from the AutomaticAnalysis
  Float_t cut_average_threshold_min;
  Float_t cut_average_threshold_max;
  Float_t cut_RMS_threshold_max;
  Float_t cut_average_noise_min;
  Float_t cut_average_noise_max;
  Float_t cut_RMS_noise_max;
  Int_t cut_failed_fit_max;

 private:
  void WriteResults();

  int _Ncol;
  int _Nrow;

  int _ChipType;

    std::vector<unsigned short int> _fe_disable;
    std::vector<std::vector<unsigned short int> > _cp_enable;

};


#endif

