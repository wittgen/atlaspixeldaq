#ifndef _BOCanalysisROOT_h_
#define _BOCanalysisROOT_h_ 

#include "ROOTanalysisBase.h"
#include "analysisInput_BOC.h"


class BOCanalysisROOT  : public ROOTanalysisBase
{
 public:
  BOCanalysisROOT();
  ~BOCanalysisROOT();

 private: //methods

  void Initialize();

  void Calculate(); //internal calculations

  void QualifyResults(); //apply cuts from analysis config

  void CheckForKnownProblems(); //setting of failure bits
  
  
  void FindBestViSet();
  void FindBestThreshold(int iviset, int ilink);
  void FindBestDelay(int iviset, int ilink);
  void Print();	  
  
  
 private: //variables  
  int _NDelay; 
  std::vector<int> _DelayVal;
  
  int _NThreshold; 
  std::vector<int> _ThresholdVal;
  
  int _NViSet;  
  std::vector<int> _ViSetVal;
  

  // Algorithm parameters
  // --------------------

  int _ViSet;
  int _EFR;

  int _Thr[7][2];
  int _Thr_min[7][2];
  int _Thr_max[7][2];

  int _Delay[7][2];
  int _Delay_redge[7][2];
  int _Delay_ledge[7][2];
  int _Delay_width[7][2];
};


#endif

