#include "OPTOLINKanalysisROOT.h"
#include <iostream>
#include <fstream>
#include "TFile.h"



OPTOLINKanalysisROOT::OPTOLINKanalysisROOT(): _NDelay(0), _NThreshold(0), _NViSet(0)
{
  std::cout << std::endl;
  std::cout << "----------------------------" << std::endl;
  std::cout << "   OPTOLINK Analysis Algorithm   " << std::endl;
  std::cout << "----------------------------" << std::endl;
  std::cout << std::endl;
}



OPTOLINKanalysisROOT::~OPTOLINKanalysisROOT()
{
  //
}



void OPTOLINKanalysisROOT::Initialize()
{
  if(!m_scan) {
    std::cout << std::endl;
    std::cout << "OPTOLINKanalysisROOT::Initialize ERROR: input data pointer is null" << std::endl;
    std::cout << std::endl;
    return;
  } else {
    std::cout << "JDDBG: Input data pointer seems to exist..." << std::endl;
  }

  std::cout << "JDDBG: Reached Input handling of OPTOLINKanalysisROOT class" << std::endl;
  
  std::cout << "JDDBG: Number of entries in AnalysisInput: " << m_scan->GetEntries() << std::endl;

  AnalysisInputOPTOLINK *scan_iter = nullptr;

  if (!(scan_iter = (AnalysisInputOPTOLINK*) m_scan->First())) return;

  this->m_serialNumber = scan_iter->m_serialNumber;
  
  _ChipFeFlav= scan_iter->_ChipFeFlav;
  std::cout<<"FE flavour "<<_ChipFeFlav<<std::endl;

//  std::cout << "JDDBG: Got scan iterator, running PrintInfo()" << std::endl;
//  scan_iter->PrintInfo();

//  if(m_logLevel>=CAN::kDiagnostics3Log) {
    std::cout << "JDDBG: Loop names = " ;
    if(scan_iter->axis.size()>0) 
      std::cout << "  " << scan_iter->axis[0].variableName;
    else
      std::cout << "  " << "[none]";
    if(scan_iter->axis.size()>1) 
      std::cout << "  " << scan_iter->axis[1].variableName;
    else
      std::cout << "  " << "[none]";
    if(scan_iter->axis.size()>2) 
      std::cout << "  " << scan_iter->axis[2].variableName;
    else
      std::cout << "  " << "[none]";
    std::cout << std::endl;
//  }



  _NDelay = 0; _DelayVal.clear();
  _NThreshold = 0; _ThresholdVal.clear();
  _NViSet = 0;  _ViSetVal.clear(); 


  // Set delay axis
  // -------------------------------
  _NDelay = scan_iter->axis[0].nSteps;
  float width = ( scan_iter->axis[0].lastBin-scan_iter->axis[0].firstBin)/
    ( scan_iter->axis[0].nSteps - 1.0);
  for (int i=0;i<_NDelay;i++)  
    _DelayVal.push_back( (scan_iter->axis[0].firstBin+i*width) );


  // Set threshold axis
  // -------------------------------
  _NThreshold = scan_iter->axis[1].nSteps;
  width = ( scan_iter->axis[1].lastBin-scan_iter->axis[1].firstBin)/
    ( scan_iter->axis[1].nSteps - 1.0);
  for (int i=0;i<_NThreshold;i++) 
    _ThresholdVal.push_back( (scan_iter->axis[1].firstBin+i*width) );


  // Set ViSet axis
  // -------------------------------
  if(scan_iter->axis.size()>2) {
    _NViSet = scan_iter->axis[2].nSteps;
    if(_NViSet > 1 ) {
      width = ( scan_iter->axis[2].lastBin-scan_iter->axis[2].firstBin)/
	( scan_iter->axis[2].nSteps - 1.0);
      for (int i=0;i<_NViSet;i++) 
	_ViSetVal.push_back( (int)(scan_iter->axis[2].firstBin+i*width) );
    } else {
      if (_NViSet == 1) _ViSetVal.push_back( (int) scan_iter->axis[2].firstBin );
      else _ViSetVal.push_back( (int) 0);
    }
  }


  for(int mod=0; mod<7; mod++) {
    for(int ilink=0; ilink<2; ilink++) {
      _Thr[mod][ilink] = -1;
      _Thr_min[mod][ilink] = -1;
      _Thr_max[mod][ilink] = -1;
      _Delay[mod][ilink] = -1;
      _Delay_redge[mod][ilink] = -1;
      _Delay_ledge[mod][ilink] = -1;
      _Delay_width[mod][ilink] = -1;
    }
  }
  

  if(m_logLevel>=CAN::kDiagnostics3Log) {
    std::cout << std::endl;
    std::cout << "Delay: " << _NDelay << std::endl;
    for(int y=0; y< _NDelay; y++) std::cout << _DelayVal[y] << ", ";
    std::cout << std::endl;
    std::cout << "Threshold: " << _NThreshold << std::endl;
    for(int y=0; y< _NThreshold; y++) std::cout << _ThresholdVal[y] << ", ";
    std::cout << std::endl;
    if(_NViSet > 1 ) {
      std::cout << "ViSet: " << _NViSet << std::endl;
      for(int y=0; y< _NViSet; y++) std::cout << _ViSetVal[y] << ", ";
      std::cout << std::endl;
    }
  }
}


void OPTOLINKanalysisROOT::Calculate() 
{  
  std::cout << "**************************************" << std::endl;
  std::cout << "OPTOLINKanalysis::Calculate  starting" << std::endl;

  if(_ChipFeFlav == PixGeometry::FEI4_MODULE){
    FindBestPhases();
     std::cout << "**************************************" << std::endl;
     std::cout << "OPTOLINKanalysis::Calculate finish" << std::endl;
    return;
  }

  if (m_scan->GetEntries()) {
    m_scan->R__FOR_EACH(AnalysisInputOPTOLINK,PrintInfo)(); 
    std::cout << "OPTOLINKanalysis: These were the module names." << std::endl;  

    int visetOk(0);
    if(m_at==OLD){
       visetOk = this->FindBestViSet();
       for(int ilink=0; ilink<2; ilink++) {
         this->FindBestThreshold(_ViSet, ilink+1);
         this->FindBestDelay(_ViSet, ilink+1);
       }  

       m_status = kPassed;
  
       for(Int_t i=0; i<m_scan->GetEntries(); i++) { 
	 auto module_result = std::make_unique<ResultOPTOLINK> ();
	 module_result->pp0Name = ((AnalysisInputOPTOLINK*)m_scan->At(i))->pp0Name;
	 module_result->moduleName = ((AnalysisInputOPTOLINK*)m_scan->At(i))->moduleName;
	 module_result->configThreshold_Link1 = ((AnalysisInputOPTOLINK*)m_scan->At(i))->thr_Link1;
	 module_result->configThreshold_Link2 = ((AnalysisInputOPTOLINK*)m_scan->At(i))->thr_Link2;
	 module_result->configDelay_Link1 = ((AnalysisInputOPTOLINK*)m_scan->At(i))->del_Link1;
	 module_result->configDelay_Link2 = ((AnalysisInputOPTOLINK*)m_scan->At(i))->del_Link2;
	 module_result->configViSet_Link1 = ((AnalysisInputOPTOLINK*)m_scan->At(i))->viset_Link1;
	 module_result->configViSet_Link2 = ((AnalysisInputOPTOLINK*)m_scan->At(i))->viset_Link2;
      
	 module_result->optimalThreshold_Link1 = _Thr[i][0];
	 module_result->optimalThreshold_Link2 = _Thr[i][1];
	 module_result->visetOk = (visetOk & 1);
	 module_result->visetLow = ((visetOk & 2) >> 1);
	 if (_NViSet>1) 
	   module_result->bestViset = _ViSetVal[_ViSet];
	 else if (_NViSet==1)
	   module_result->bestViset = _ViSetVal[0];
	 else
	   module_result->bestViset = (((AnalysisInputOPTOLINK*)m_scan->At(i))->viset_Link1 * 1000.0); //Assuming config values uns Volt
	 module_result->optimalDelay_Link1 = _Delay[i][0];
	 module_result->optimalDelay_Link2 = _Delay[i][1];
	 this->QualifyTuning(_ViSet, module_result.get(), i, ((AnalysisInputOPTOLINK*)m_scan->At(i))->thr_Link1, 
			     ((AnalysisInputOPTOLINK*)m_scan->At(i))->thr_Link2, 
			     ((AnalysisInputOPTOLINK*)m_scan->At(i))->del_Link1,
			     ((AnalysisInputOPTOLINK*)m_scan->At(i))->del_Link2);
	 module_result->goodTuning_Link1 = (module_result->goodThr_Link1 && module_result->goodDel_Link1);
	 module_result->goodTuning_Link2 = (module_result->goodThr_Link2 && module_result->goodDel_Link2);

	 module_result->passed=((module_result->goodTuning_Link1 && module_result->goodTuning_Link2) && !m_forcedfail);
	 module_result->useNewTuning = this->QualifyTuning(_ViSet, i, _Thr[i][0], _Thr[i][1], _Delay[i][0], _Delay[i][1]);

	 //(!(module_result->passed));
	 module_result->commonSpots = this->CommonSpotFinder(_ViSet);//, module_result);
	 m_result->Add( module_result.release());
       }

    }else if(m_at==NEW){

       this->FindPreferedPoint();


       m_status = kPassed;
  
       for(Int_t i=0; i<m_scan->GetEntries(); i++) { 
	 auto module_result = std::make_unique<ResultOPTOLINK> ();
	 module_result->pp0Name = ((AnalysisInputOPTOLINK*)m_scan->At(i))->pp0Name;
	 module_result->moduleName = ((AnalysisInputOPTOLINK*)m_scan->At(i))->moduleName;
	 module_result->configThreshold_Link1 = ((AnalysisInputOPTOLINK*)m_scan->At(i))->thr_Link1;
	 module_result->configThreshold_Link2 = ((AnalysisInputOPTOLINK*)m_scan->At(i))->thr_Link2;
	 module_result->configDelay_Link1 = ((AnalysisInputOPTOLINK*)m_scan->At(i))->del_Link1;
	 module_result->configDelay_Link2 = ((AnalysisInputOPTOLINK*)m_scan->At(i))->del_Link2;
	 module_result->configViSet_Link1 = ((AnalysisInputOPTOLINK*)m_scan->At(i))->viset_Link1;
	 module_result->configViSet_Link2 = ((AnalysisInputOPTOLINK*)m_scan->At(i))->viset_Link2;
      
	 module_result->optimalThreshold_Link1 = _Thr[i][0];
	 module_result->optimalThreshold_Link2 = _Thr[i][1];
	 module_result->visetOk = true;
	 module_result->visetLow = false;
	 module_result->bestViset = (((AnalysisInputOPTOLINK*)m_scan->At(i))->viset_Link1 * 1000.0); //Assuming config values uns Volt
	 module_result->optimalDelay_Link1 = _Delay[i][0];
	 module_result->optimalDelay_Link2 = _Delay[i][1];
	 
	 module_result->goodTuning_Link1=(_Passed[i][0]);
	 module_result->goodTuning_Link2=(_Passed[i][1]);
	 
	 module_result->passed=((module_result->goodTuning_Link1 && module_result->goodTuning_Link2) && !m_forcedfail);
	 module_result->useNewTuning = module_result->passed;

	 module_result->commonSpots = 0;
	 m_result->Add( module_result.release() );
       }

    }
//  if(m_logLevel>=CAN::kDiagnostics1Log) this->Print();
    
    std::cout << std::endl << "Just for my mostest dearest Josh, Analysis ID: " << this->m_serialNumber << " PP0 " << ((AnalysisInputOPTOLINK*)m_scan->At(0))->moduleName << " Viset " << _ViSetVal[_ViSet] << std::endl;
  } else {
    std::cout << "You stink, which is why I won't calculate anything, go away!" << std::endl;
  }
} 


int OPTOLINKanalysisROOT::FindBestViSet()
{
  _ViSet = 0;
  _EFR   = 0;
  
  std::cout << "JDDBG: You're in FindBestViSet (and thaaaaat close to a working Analysis) ..." << std::endl;
  
  //  int m_allowedovershoots = 2;
  int visetOk = 0;
  
  if(_NViSet < 2) {
    std::cout << "JDDBG: Less than 2 steps in ViSet, namely " << _NViSet << " - Implies I will just check whether I like the BOCscan or not..." << std::endl;
  }
  
  if(m_scan->GetEntries()>7) {
    std::cout << "ERROR, pp0 with " << m_scan->GetEntries() << " modules" << std::endl;
    //return;
  }

  int areamin[128] = {0};
  
  std::vector<int> idealthreshold( (_NViSet > 0 ? _NViSet : 1) );
  std::vector<int> nummodules((_NViSet > 0 ? _NViSet : 1));
  std::vector<int> overshootthreshold((_NViSet > 0 ? _NViSet : 1)); // (..., 0) could initialise here 
  std::vector<int> numhistos((_NViSet > 0 ? _NViSet : 1)); // (..., 0) could initialise here 
  
  AnalysisInputOPTOLINK *scan_iter = (AnalysisInputOPTOLINK *)NULL;
  
  std::cout << "JDDBG: Resetting arrays in FindBestViSet... ";

  for(int ih=0; ih < (_NViSet > 0 ? _NViSet : 1); ih++) {
    idealthreshold[ih] = 0;
    nummodules[ih] = 0;
    overshootthreshold[ih] = 0;
    numhistos[ih] = 0;
  }
  
  std::cout << "[done]" << std::endl;
  
  //LOOP OVER VISET

  std::cout << "JDDBG: Looping over " << (_NViSet > 0 ? _NViSet : 1) << " VIset values and " << m_scan->GetEntries() << " modules" << std::endl;
  for (int ih=0; ih < (_NViSet > 0 ? _NViSet : 1); ih++){  //Viset loop
    
    areamin[ih] = 999;

    overshootthreshold[ih] = 0;
    
    //LOOP OVER MODULES 
    for(Int_t i = 0; i < m_scan->GetEntries(); i++) {
      scan_iter = (AnalysisInputOPTOLINK *) m_scan->At(i); 
      std::cout << "JDDBG: Scan_iter->Maxcount is: " << scan_iter->Maxcount << std::endl;
      std::ofstream outputfile;
//      char filename[200];
      std::string modulename((((AnalysisInputOPTOLINK*)m_scan->At(i))->moduleName).Data());

      int area_link1 = 0;
      int area_link2 = 0;
      
      int thrmin1 = 0, thrmin2 = 0;
      int thrminsum1 = _NDelay * ((scan_iter->Maxcount)*2), thrminsum2 = _NDelay * ((scan_iter->Maxcount)*2);

      std::cout << "JDDBG: Overshoot limit =  " << m_overshootlimit << ", High Cutoff = " << m_highcutoff << std::endl;
      
      std::cout << "JDDBG: Trying to run on module " << i << "... ";
      if(!(scan_iter->histo_Link1.size())) {
        std::cout << std::endl << "JDDBG: Histogram size for Link1 of mod " << i << " is zero, skipping...";
      } else numhistos[ih]++;
      if(!(scan_iter->histo_Link2.size())) {
        std::cout << std::endl << "JDDBG: Histogram size for Link2 of mod " << i << " is zero, skipping...";
      } else numhistos[ih]++; 
      for(int ithr = 1; ithr <= _NThreshold; ithr++) {
        int sum1 = 0, sum2 = 0;								// Variables to contain the sum of counts for one threshold row
	int areastrip1 = 0, areastrip2 = 0; 
	for(int idelay = 1; idelay <= _NDelay; idelay++) {
//          std::cout << "In Threshold step " << ithr << " and Delay step " << idelay << std::endl;
	  
	  if(scan_iter->histo_Link1.size()) {
	    int val1 = int(scan_iter->histo_Link1[ih]->GetBinContent(idelay,ithr));
	    if (val1<1) {
	      area_link1++;
	      areastrip1++;
	    }
	    val1 = (val1 > (scan_iter->Maxcount) ? (scan_iter->Maxcount) : val1);
	    sum1 += val1;									// Summing up all vals in one row to find which Threshold has the lowest sum value
	  }
	  
	  if(scan_iter->histo_Link2.size()) {
	    int val2 = int(scan_iter->histo_Link2[ih]->GetBinContent(idelay,ithr));
	    if (val2<1) {
	      area_link2++;
	      areastrip2++;
	    }
	    val2 = (val2 > (scan_iter->Maxcount) ? (scan_iter->Maxcount) : val2);
	    sum2 += val2;								// Summing up all vals in one row to find which Threshold has the lowest sum value
	  }
	}
	if(scan_iter->histo_Link1.size()) {
	  if (sum1 < thrminsum1) {
//	    std::cout << "JDDBG: Found minimal width: " << sum1 << ", previous was: " << thrminsum1 << " Am at Threshold: " << ithr - 1 << std::endl;
	    thrminsum1 = sum1;
	    thrmin1 = (ithr - 1); // subtraction of one to convert from root histo indices to array indices
	    if (ithr == (_NThreshold - 1) && (sum1 > ((scan_iter->Maxcount)*m_overshootlimit))) {
	      std::cout << "JDDBG: Number of overshoots is: " << (overshootthreshold[ih]) << " being increased by 1, Maxcount*2 was: " << ((scan_iter->Maxcount)*m_overshootlimit) << std::endl;
	      (overshootthreshold[ih])++;
	    }
            if (ithr == (_NThreshold - 1) && (areastrip1 < m_highcutoff)) (overshootthreshold[ih])+= 2;
	  }
	}
        if(scan_iter->histo_Link2.size()) {
	  if (sum2 < thrminsum2) {
//	    std::cout << "JDDBG: Found minimal width: " << sum2 << ", previous was: " << thrminsum2 << " Am at Threshold: " << ithr - 1 << std::endl;
	    thrminsum2 = sum2;
	    thrmin2 = (ithr - 1); // subtraction of one to convert from root histo indices to array indices
	    if (ithr == (_NThreshold - 1) && (sum2 > ((scan_iter->Maxcount)*m_overshootlimit))) {
	      std::cout << "JDDBG: Number of overshoots is: " << (overshootthreshold[ih]) << " being increased by 1, Maxcount*2 was: " << ((scan_iter->Maxcount)*m_overshootlimit) << std::endl;
	      (overshootthreshold[ih])++;
	    }
            if (ithr == (_NThreshold - 1) && (areastrip2 < m_highcutoff)) (overshootthreshold[ih])+= 2;
	  }
	}
      }

      std::cout << "[done]" << std::endl;      
      if(scan_iter->histo_Link1.size()) {
        idealthreshold[ih] += thrmin1;
	if (_NViSet)
	  std::cout << "MOD: Ideal Threshold bin found on Link1 for ViSet " << _ViSetVal[ih] << "mV is " << _ThresholdVal[thrmin1] << std::endl;
	else
          std::cout << "MOD: Ideal Threshold bin found on Link1 is " << _ThresholdVal[thrmin1] << std::endl;
      }
      if(scan_iter->histo_Link2.size()) {
        idealthreshold[ih] += thrmin2;
	if (_NViSet)
	  std::cout << "MOD: Ideal Threshold bin found on Link2 for ViSet " << _ViSetVal[ih] << "mV is " << _ThresholdVal[thrmin2] << std::endl;
	else
          std::cout << "MOD: Ideal Threshold bin found on Link2 is " << _ThresholdVal[thrmin2] << std::endl;
      }

      
      if (thrmin1) nummodules[ih]++;
      if (thrmin2) nummodules[ih]++;
      
      if(scan_iter->histo_Link2.size()) 
        if (area_link1<areamin[ih]) areamin[ih] = area_link1;
      if(scan_iter->histo_Link2.size()) 
	if (area_link2<areamin[ih]) areamin[ih] = area_link2;
    }
    
    
    if (nummodules[ih]) {
      std::cout << "PP0: Ideal Treshold sum is " << idealthreshold[ih] << " for " << nummodules[ih] << " thresholds found tuneable - Average threshold is thus: " << (idealthreshold[ih] / nummodules[ih]) << std::endl;
      if (_NViSet)
        std::cout << "PP0: Ideal average Threshold found for ViSet " << _ViSetVal[ih] << "mV is " << _ThresholdVal[(idealthreshold[ih] / nummodules[ih])] << std::endl;
      else
        std::cout << "PP0: Ideal average Threshold found is " << _ThresholdVal[(idealthreshold[ih] / nummodules[ih])] << std::endl;
    } else {
      if (_NViSet) std::cout << "PP0: Could not find any modules for Viset " << _ViSetVal[ih] << "mV" << std::endl;
    }
    if(m_logLevel>=CAN::kDiagnostics3Log)
      if (_NViSet) std::cout << "Min area for ViSet = " << _ViSetVal[ih] << "  -->  " << areamin[ih] << std::endl;
  }
  
  
  int areamax = 0;
  int ibestviset=0;
  
  int viset_low = 0;
  
  if (_NViSet > 1) {
    for (int viset = 0; viset < _NViSet; viset++) {
      if (nummodules[viset] > 0) {
	idealthreshold[viset] = idealthreshold[viset]/nummodules[viset]; //* NViset / num_modules;
        if (_ThresholdVal[idealthreshold[viset]] < m_visetlowthreshold) viset_low = 1;
	else viset_low = 0;
	if (idealthreshold[viset] < (_NThreshold - 1) && overshootthreshold[viset] <= (numhistos[viset]/5) && idealthreshold[viset] >= idealthreshold[(ibestviset > 0 ? ibestviset : 0)]) {
	  if (overshootthreshold[viset] == 0){
            ibestviset = viset;
	    visetOk = 1;
	  } else {
            if ((((float) nummodules[viset])/((float) overshootthreshold[viset])) >= 2.0) {
	      ibestviset = viset;
              visetOk = 1;
	    } else {
	      std::cout << "JDDBG: Hi Testers - I didn't choose this VIset (" << _ViSetVal[viset] << "mV), cause I didn't like the Ratio of modules over overshoots." << std::endl;
	    }
	  }
	} else {
	  std::cout << "JDDBG: Hi Testers - I didn't choose this VIset (" << _ViSetVal[viset] << "mV), cause I didn't like ";
	  if (!(idealthreshold[viset] < (_NThreshold - 1))) std::cout << "the average threshold being too high, namely " << idealthreshold[viset] << " ";
	  if (!(overshootthreshold[viset] <= (numhistos[viset]/5))) {
            if (!(idealthreshold[viset] < (_NThreshold - 1))) std::cout << "and ";
            std::cout << "the number of overshoots was " << overshootthreshold[viset] << " and meant to not be higher than " << (numhistos[viset]/5) << " ";
	  }
	  if (!(idealthreshold[viset] >= idealthreshold[(ibestviset > 0 ? ibestviset : 0)])) {
            if (!(idealthreshold[viset] < (_NThreshold - 1) && overshootthreshold[viset] <= (numhistos[viset]/5))) std::cout << "and ";
            std::cout << "the average threshold of " << idealthreshold[viset];
	  }
	  std::cout << std::endl;
	}
      } else {
	std::cout << "No modules found for viset " << _ViSetVal[viset] << "mV" << std::endl;
      } 
    }
  } else {
    if (nummodules[0] > 0) {
      idealthreshold[0] = idealthreshold[0]/nummodules[0]; //* NViset / num_modules;
      if (_ThresholdVal[idealthreshold[0]] < m_visetlowthreshold) viset_low = 1;
      if (idealthreshold[0] < (_NThreshold - 1) && overshootthreshold[0] <= (numhistos[0]/5)) {
	if (overshootthreshold[0] == 0){
          ibestviset = 0;
          visetOk = 1;
	} else {
          if ((((float) nummodules[0])/((float) overshootthreshold[0])) >= 2.0) {
	    ibestviset = 0;
	    visetOk = 1;
	  } else {
	    std::cout << "JDDBG: Hi Testers - I don't like this VIset, cause I didn't like the Ratio of modules over overshoots." << std::endl;
	  }
	}
      } else {
	std::cout << "JDDBG: Hi Testers - I don't like this VIset, cause I don't agree with ";
	if (!(idealthreshold[0] < (_NThreshold - 1))) std::cout << "the average threshold being too high, namely " << idealthreshold[0] << " ";
	if (!(overshootthreshold[0] <= (numhistos[0]/5))) {
          if (!(idealthreshold[0] < (_NThreshold - 1))) std::cout << "and ";
          std::cout << "the number of overshoots which is " << overshootthreshold[0] << " and not meant to be higher than " << (numhistos[0]/5) << " ";
	}
	std::cout << std::endl;
      }
    } else {
      std::cout << "No modules found" << std::endl;
    } 
  }

  for (int ih=0; ih < (_NViSet > 0 ? _NViSet : 1); ih++) {  //Viset loop
    if (areamin[ih] > areamax) {
      areamax = areamin[ih]; 
//      ibestviset = ih;
    }
  }
  
  _ViSet = ibestviset;
  _EFR   = areamax;
  
  return ((visetOk & 1) + ((viset_low & 1)<<1));
}

void OPTOLINKanalysisROOT::FindBestThreshold(int ih, int ilink)
{
  std::cout << "JDDBG: You're in FindBestThreshold (we're about to tune to first order) ..." << std::endl;
  
  if(m_scan->GetEntries()==0) return;
  if(m_scan->GetEntries()>7) {
    std::cout << "ERROR, pp0 with " << m_scan->GetEntries() << " modules" << std::endl;
    //return;
  }


  std::cout << "JDDBG: Running Threshold tuning on " << m_scan->GetEntries() << " modules" << std::endl;
  for(Int_t mod = 0; mod < m_scan->GetEntries(); mod++) {
    AnalysisInputOPTOLINK * scan_iter = (AnalysisInputOPTOLINK *) m_scan->At(mod); 

    int thrmin1 = 0, thrmin2 = 0;
    _Thr_min[mod][0] = _Thr_min[mod][1] = -1;
    _Thr_max[mod][0] = _Thr_max[mod][1] = -1;
    int thrminsum1 = _NDelay * (1<<15), thrminsum2 = _NDelay * (1<<15);
    
    //    int valsaround_tuning[2][2] = {{-1, -1}, {-1, -1}};

    std::cout << "JDDBG: Trying to run on module " << mod << "... ";
    if(!(scan_iter->histo_Link1.size())) {
      std::cout << std::endl << "JDDBG: Histogram size for Link1 of mod " << mod << " is zero, skipping...";
    }
    if(!(scan_iter->histo_Link2.size())) {
      std::cout << std::endl << "JDDBG: Histogram size for Link2 of mod " << mod << " is zero, skipping...";
    }
    for(int ithr = 1; ithr <= _NThreshold; ithr++) {
      int sum1 = 0, sum2 = 0;								// Variables to contain the sum of counts for one threshold row
      for(int idelay = 1; idelay <= _NDelay; idelay++) {

	if(scan_iter->histo_Link1.size()) {
	  int val1 = int(scan_iter->histo_Link1[_ViSet]->GetBinContent(idelay,ithr));
          val1 = (val1 > (scan_iter->Maxcount) ? (scan_iter->Maxcount) : val1);
	  if (val1<1 && _Thr_min[mod][0]<0) _Thr_min[mod][0] = ithr - 1;
	  if (val1<1) _Thr_max[mod][0] = ithr - 1;
	  sum1 += val1;									// Summing up all vals in one row to find which Threshold has the lowest sum value
	}

	if(scan_iter->histo_Link2.size()) {
	  int val2 = int(scan_iter->histo_Link2[_ViSet]->GetBinContent(idelay,ithr));
          val2 = (val2 > (scan_iter->Maxcount) ? (scan_iter->Maxcount) : val2);
	  if (val2<1 && _Thr_min[mod][1]<0) _Thr_min[mod][1] = ithr - 1;
	  if (val2<1) _Thr_max[mod][1] = ithr - 1;
	  sum2 += val2;								// Summing up all vals in one row to find which Threshold has the lowest sum value
	}
      }
      if(scan_iter->histo_Link1.size()) {
	if (sum1 < thrminsum1) {
	  thrminsum1 = sum1;
	  thrmin1 = (ithr - 1); // subtraction of one to convert from root histo indices to array indices
	}
      }
      if(scan_iter->histo_Link2.size()) {
	if (sum2 < thrminsum2) {
	  thrminsum2 = sum2;
	  thrmin2 = (ithr - 1); // subtraction of one to convert from root histo indices to array indices
	}
      }
    }
    std::cout << "[done]" << std::endl;

    // switch
    if(scan_iter->histo_Link1.size()) {
      if (((thrmin1 - _Thr_min[mod][0] < m_lowerdistance) || scan_iter->MBs == 80) ) {
// Correcting towards the center of the EFR here...
        std::cout << "Correcting low tuned Threshold or 80MBit/s" << std::endl;
        thrmin1 += (_Thr_min[mod][0] + _Thr_max[mod][0])/2;
	thrmin1 /= 4;
      }
      _Thr[mod][0] = _ThresholdVal[thrmin1];
      std::cout << "MOD: Ideal Threshold found on Link1 is " << _ThresholdVal[thrmin1] << " (Setting " << thrmin1 << ")" << std::endl;
      
    }
    if(scan_iter->histo_Link2.size()) {
      if (((thrmin2 - _Thr_min[mod][1] < m_lowerdistance) || scan_iter->MBs == 80) ) {
// Correcting towards the center of the EFR here...
        std::cout << "Correcting low tuned Threshold or 80MBit/s" << std::endl;
        thrmin2 += (_Thr_min[mod][1] + _Thr_max[mod][1])/2;
	thrmin2 /= 4;
      }
      _Thr[mod][1] = _ThresholdVal[thrmin2];
      std::cout << "MOD: Ideal Threshold found on Link2 is " << _ThresholdVal[thrmin2] << " (Setting " << thrmin2 << ")" << std::endl;
    }
  }
  
/*  _Thr[mod][ilink-1] = thropt;
    _Thr_min[mod][ilink-1] = thrmin;
    _Thr_max[mod][ilink-1] = thrmax;
  */
}



void OPTOLINKanalysisROOT::FindBestDelay(int ih, int ilink)
{
  std::cout << "JDDBG: You're in FindBestDelay (we're nearly there) ..." << std::endl;
  
  if(m_scan->GetEntries()==0) return;
  if(m_scan->GetEntries()>7) {
    std::cout << "ERROR, pp0 with " << m_scan->GetEntries() << " modules" << std::endl;
    //return;
  }

  std::vector<std::vector<int>> projection_x (_NDelay, std::vector<int>{0,0});

  std::cout << "JDDBG: Running Delay tuning on " << m_scan->GetEntries() << " modules" << std::endl;
  for(Int_t mod = 0; mod < m_scan->GetEntries(); mod++) {
    AnalysisInputOPTOLINK * scan_iter = (AnalysisInputOPTOLINK *) m_scan->At(mod); 

    int delmaxsum1 = 0, delmaxsum2 = 0;

    std::cout << "JDDBG: Trying to run on module " << mod << "... ";
    if(!(scan_iter->histo_Link1.size())) {
      std::cout << std::endl << "JDDBG: Histogram size for Link1 of mod " << mod << " is zero, skipping...";
    }
    if(!(scan_iter->histo_Link2.size())) {
      std::cout << std::endl << "JDDBG: Histogram size for Link2 of mod " << mod << " is zero, skipping...";
    }
    for(int idelay = 1; idelay <= _NDelay; idelay++) {
      projection_x[idelay-1][0] = projection_x[idelay-1][1] = 0;
      int sum1 = 0, sum2 = 0;								// Variables to contain the sum of counts for one threshold row
      for(int ithr = 1; ithr <= _NThreshold; ithr++) {
	if(scan_iter->histo_Link1.size()) {
	  int val1 = int(scan_iter->histo_Link1[_ViSet]->GetBinContent(idelay,ithr));
          val1 = (val1 > (scan_iter->Maxcount) ? (scan_iter->Maxcount) : val1);
	  sum1 += val1;									// Summing up all vals in one row to find which Threshold has the lowest sum value
	}

	if(scan_iter->histo_Link2.size()) {
	  int val2 = int(scan_iter->histo_Link2[_ViSet]->GetBinContent(idelay,ithr));
          val2 = (val2 > (scan_iter->Maxcount) ? (scan_iter->Maxcount) : val2);
	  sum2 += val2;								// Summing up all vals in one row to find which Threshold has the lowest sum value
	}
      }
      if (sum1 > delmaxsum1) delmaxsum1 = sum1;
      if (sum2 > delmaxsum2) delmaxsum2 = sum2;
      projection_x[idelay-1][0] = sum1;
      projection_x[idelay-1][1] = sum2;
    }
    std::cout << "[done]" << std::endl;

    int delmax1 = 0, delmax2 = 0;

    for(int idelay = 0; idelay < _NDelay; idelay++) {
      if(scan_iter->histo_Link1.size()) {
	if (projection_x[idelay][0] == delmaxsum1 && projection_x[((idelay + 1) % _NDelay)][0] < delmaxsum1) {
	  delmax1 = idelay;
	}
      }
      if(scan_iter->histo_Link2.size()) {
	if (projection_x[idelay][1] == delmaxsum2 && projection_x[((idelay + 1) % _NDelay)][1] < delmaxsum2) {
	  delmax2 = idelay;
	}
      }
    }
    
    if(scan_iter->histo_Link1.size()) {
      _Delay[mod][0] = fmod(_DelayVal[delmax1] + m_tunedistance, 25);
      if (_Delay[mod][0] < 0) _Delay[mod][0] += 25; 
    }
    if(scan_iter->histo_Link2.size()) {
      _Delay[mod][1] = fmod(_DelayVal[delmax2] + m_tunedistance, 25);
      if (_Delay[mod][1] < 0) _Delay[mod][1] += 25; 
    }

    // Catching large delay difference between link1 and link2
    //    if ((_Delay[mod][0] == 24 && _Delay[mod][1] == 0) || (_Delay[mod][0] == 0 && _Delay[mod][1] == 24)) _Delay[mod][0] = _Delay[mod][1] = 0;
    if (abs(_Delay[mod][0] - _Delay[mod][1]) > 23) _Delay[mod][0] = _Delay[mod][1] = 0;

    if(scan_iter->histo_Link1.size()) {
      std::cout << "MOD: Ideal Delay found on Link1 is " << _DelayVal[delmax1] << " (Setting " << delmax1 << ")" << std::endl;
    }
    if(scan_iter->histo_Link2.size()) {
      std::cout << "MOD: Ideal Delay found on Link2 is " << _DelayVal[delmax2] << " (Setting " << delmax2 << ")" << std::endl;
    }
  }
  
}

bool OPTOLINKanalysisROOT::QualifyTuning(int ih, AnalysisResult* general_result, int mod, float thr_1, float thr_2, float del_1, float del_2)
{
  ResultOPTOLINK* module_result = static_cast<ResultOPTOLINK*>(general_result);
  std::cout << "JDDBG: You're in QualifyTuning (The last one, promised) ..." << std::endl;
  
  int minVal1;
  int minVal2;
  int lowerthresh[2], higherthresh[2];
  int leftdelay[2], rightdelay[2];
  int leftspace[2][2] = {{-2, -2}, {-2, -2}}, rightspace[2][2] = {{-2, -2}, {-2, -2}};
  
  
  std::cout << "JDDBG: Qualifying tuning points on " << m_scan->GetEntries() << " modules" << std::endl;

  AnalysisInputOPTOLINK * scan_iter = (AnalysisInputOPTOLINK *) m_scan->At(mod); 

  std::cout << "JDDBG: Trying to run on module " << mod << "... ";
  if(!(scan_iter->histo_Link1.size())) {
    std::cout << std::endl << "JDDBG: Histogram size for Link1 of mod " << mod << " is zero, skipping...";
  }
  if(!(scan_iter->histo_Link2.size())) {
    std::cout << std::endl << "JDDBG: Histogram size for Link2 of mod " << mod << " is zero, skipping...";
  }
  
  minVal1 = (scan_iter->Maxcount);
  minVal2 = (scan_iter->Maxcount);

  leftdelay[0] = -1;
  leftdelay[1] = -1;
  rightdelay[0] = -1;
  rightdelay[1] = -1;
  lowerthresh[0] = -1;
  lowerthresh[1] = -1;
  higherthresh[0] = -1;
  higherthresh[1] = -1;

  int thrmin[2][2] = {{-1, -1}, {-1, -1}};
  int thrmax[2][2] = {{-1, -1}, {-1, -1}};
  for(int idelay = 0; idelay < _NDelay; idelay++) {
    if (del_1 >= _DelayVal[idelay]) {
      leftdelay[0] = idelay + 1;
      rightdelay[0] = ((idelay + 1) % _NDelay) + 1;
    }
    if (del_2 >= _DelayVal[idelay]) {
      leftdelay[1] = idelay + 1;
      rightdelay[1] = ((idelay + 1) % _NDelay) + 1;
    }
  }

  for(int ithr = 0; ithr < _NThreshold; ithr++) {
    if (thr_1 >= _ThresholdVal[ithr]) {
      lowerthresh[0] = ithr + 1;
      higherthresh[0] = ( (lowerthresh[0] < _NThreshold) ? (lowerthresh[0] + 1) : (_NThreshold) );
    }
    if (thr_2 >= _ThresholdVal[ithr]) {
      lowerthresh[1] = ithr + 1;
      higherthresh[1] = ( (lowerthresh[1] < _NThreshold) ? (lowerthresh[1] + 1) : (_NThreshold) );
    }
  }

// This one deals with wrong accesses, but doesn't make the physical meaning any better - fixed above
//  if (leftdelay[0] == 0) leftdelay[0] = 25;
//  if (leftdelay[1] == 0) leftdelay[1] = 25;

  for(int idelay = 1; idelay <= _NDelay; idelay++) {
    for(int ithr = 1; ithr <= _NThreshold; ithr++) {
      if(scan_iter->histo_Link1.size()) {
	int val1 = int(scan_iter->histo_Link1[_ViSet]->GetBinContent(idelay,ithr));
	if (val1 < minVal1) minVal1 = val1;
        val1 = (val1 > (scan_iter->Maxcount) ? (scan_iter->Maxcount) : val1);
	if (idelay == leftdelay[0] && val1 < 1 && thrmin[0][0] == -1) thrmin[0][0] = ithr - 1;
	if (idelay == rightdelay[0] && val1 < 1 && thrmin[0][1] == -1) thrmin[0][1] = ithr - 1;
	if (idelay == leftdelay[0] && val1 < 1) thrmax[0][0] = ithr - 1;
	if (idelay == rightdelay[0] && val1 < 1) thrmax[0][1] = ithr - 1;
      }

      if(scan_iter->histo_Link2.size()) {
	int val2 = int(scan_iter->histo_Link2[_ViSet]->GetBinContent(idelay,ithr));
	if (val2 < minVal2) minVal2 = val2;
        val2 = (val2 > (scan_iter->Maxcount) ? (scan_iter->Maxcount) : val2);
	if (idelay == leftdelay[1] && val2 < 1 && thrmin[1][0] == -1) thrmin[1][0] = ithr - 1;
	if (idelay == rightdelay[1] && val2 < 1 && thrmin[1][1] == -1) thrmin[1][1] = ithr - 1;
	if (idelay == leftdelay[1] && val2 < 1) thrmax[1][0] = ithr - 1;
	if (idelay == rightdelay[1] && val2 < 1) thrmax[1][1] = ithr - 1;
      }
    }
  }
  
  for(int idelay = 0; idelay < _NDelay; idelay++) {
    for(int ithr = 1; ithr <= _NThreshold; ithr++) {
      if(scan_iter->histo_Link1.size()) {
	int val1 = int(scan_iter->histo_Link1[_ViSet]->GetBinContent(((((leftdelay[0] - 1) + idelay) % _NDelay) + 1), ithr));
        val1 = (val1 > (scan_iter->Maxcount) ? (scan_iter->Maxcount) : val1);
	if (ithr == lowerthresh[0] && val1 > 0.5 && rightspace[0][0] <= -1) rightspace[0][0] = idelay - 1;
	if (ithr == higherthresh[0] && val1 > 0.5 && rightspace[0][1] <= -1) rightspace[0][1] = idelay - 1;
	val1 = int(scan_iter->histo_Link1[_ViSet]->GetBinContent(((((leftdelay[0] + 24) - idelay) % _NDelay) + 1), ithr));
        val1 = (val1 > (scan_iter->Maxcount) ? (scan_iter->Maxcount) : val1);
	if (ithr == lowerthresh[0] && val1 > 0.5 && leftspace[0][0] <= -1) leftspace[0][0] = idelay - 1;
	if (ithr == higherthresh[0] && val1 > 0.5 && leftspace[0][1] <= -1) leftspace[0][1] = idelay - 1;
      }
      if(scan_iter->histo_Link2.size()) {
	int val2 = int(scan_iter->histo_Link2[_ViSet]->GetBinContent(((((leftdelay[1] - 1) + idelay) % _NDelay) + 1), ithr));
        val2 = (val2 > (scan_iter->Maxcount) ? (scan_iter->Maxcount) : val2);
	if (ithr == lowerthresh[1] && val2 > 0.5 && rightspace[1][0] <= -1) rightspace[1][0] = idelay - 1;
	if (ithr == higherthresh[1] && val2 > 0.5 && rightspace[1][1] <= -1) rightspace[1][1] = idelay - 1 ;
	val2 = int(scan_iter->histo_Link2[_ViSet]->GetBinContent(((((leftdelay[1] + 24) - idelay) % _NDelay) + 1), ithr));
        val2 = (val2 > (scan_iter->Maxcount) ? (scan_iter->Maxcount) : val2);
	if (ithr == lowerthresh[1] && val2 > 0.5 && leftspace[1][0] <= -1) leftspace[1][0] = idelay - 1;
	if (ithr == higherthresh[1] && val2 > 0.5 && leftspace[1][1] <= -1) leftspace[1][1] = idelay - 1;
      }
    }
  }
  if (rightspace[0][0] == -2) rightspace[0][0] = 25;
  if (rightspace[0][1] == -2) rightspace[0][1] = 25;
  if (rightspace[1][0] == -2) rightspace[1][0] = 25;
  if (rightspace[1][1] == -2) rightspace[1][1] = 25;
  if (leftspace[0][0] == -2) leftspace[0][0] = 25;
  if (leftspace[0][1] == -2) leftspace[0][1] = 25;
  if (leftspace[1][0] == -2) leftspace[1][0] = 25;
  if (leftspace[1][1] == -2) leftspace[1][1] = 25;
  std::cout << "[done]" << std::endl;

  int avgThrMin[2][2];
  int avgThrMax[2][2];

  for (int i = 0; i < 2; i++) {
    for (int j = 0; j < 2; j++) {
      if (thrmin[i][j] > 0) {
	avgThrMin[i][j] = (_ThresholdVal[thrmin[i][j]] + _ThresholdVal[thrmin[i][j] - 1]) / 2;
      }
      else {
	avgThrMin[i][j] = _ThresholdVal[0];
      }
    }
  }
  for (int i = 0; i < 2; i++) {
    for (int j = 0; j < 2; j++) {
      if (thrmax[i][j] == -1) {
	avgThrMax[i][j] = 0;
      }
      else {
	if (thrmax[i][j] < _NThreshold - 1) {
	  avgThrMax[i][j] = (_ThresholdVal[thrmax[i][j]] + _ThresholdVal[thrmax[i][j] + 1]) / 2;
	}
	else {
	  avgThrMax[i][j] = 999;
	}
      }
    }
  }
  
  std::cout << "Debugging module " << ((AnalysisInputOPTOLINK*)m_scan->At(mod))->moduleName << std::endl;
  std::cout << "Threshold min val 00: " << avgThrMin[0][0] << " with int val " << thrmin[0][0] << " at delay " << leftdelay[0] - 1 << std::endl;
  std::cout << "Threshold min val 01: " << avgThrMin[0][1] << " with int val " << thrmin[0][1] << " at delay " << rightdelay[0] - 1 << std::endl;
  std::cout << "Threshold min val 10: " << avgThrMin[1][0] << " with int val " << thrmin[1][0] << " at delay " << leftdelay[1] - 1 << std::endl;
  std::cout << "Threshold min val 11: " << avgThrMin[1][1] << " with int val " << thrmin[1][1] << " at delay " << rightdelay[1] - 1 << std::endl;
  std::cout << "Threshold max val 00: " << avgThrMax[0][0] << " with int val " << thrmax[0][0] << " at delay " << leftdelay[0] - 1 << std::endl;
  std::cout << "Threshold max val 01: " << avgThrMax[0][1] << " with int val " << thrmax[0][1] << " at delay " << rightdelay[0] - 1 << std::endl;
  std::cout << "Threshold max val 10: " << avgThrMax[1][0] << " with int val " << thrmax[1][0] << " at delay " << leftdelay[1] - 1 << std::endl;
  std::cout << "Threshold max val 11: " << avgThrMax[1][1] << " with int val " << thrmax[1][1] << " at delay " << rightdelay[1] - 1 << std::endl;
  std::cout << " Input tuned threshold 0: " << thr_1 << " delay 0: " << del_1 << std::endl;
  std::cout << " Input tuned threshold 1: " << thr_2 << " delay 0: " << del_2 << std::endl; 

  for(int ithr = 0; ithr < _NThreshold; ithr++) {
    if (thr_1 >= _ThresholdVal[ithr]) {
      lowerthresh[0] = ithr;
      higherthresh[0] = ithr + (ithr + 1 == _NThreshold ? 0 : 1);
    }
    if (thr_2 >= _ThresholdVal[ithr]) {
      lowerthresh[1] = ithr;
      higherthresh[1] = ithr + (ithr + 1 == _NThreshold ? 0 : 1);
    }
  }

  if(scan_iter->histo_Link1.size()) {
    // Judging Link1 Threshold  
    if (thr_1 <= 0) {
      module_result->goodThr_Link1 = true;
    } else {
      if ((avgThrMin[0][0] + m_lowerdistance <= thr_1) &&
	  (avgThrMin[0][1] + m_lowerdistance <= thr_1) &&
	  (avgThrMax[0][0] - m_upperdistance >= thr_1) &&
	  (avgThrMax[0][1] - m_upperdistance >= thr_1)) {
	module_result->goodThr_Link1 = true;
      } else {
	module_result->goodThr_Link1 = false;
      }
    }
        
    // Judging Link1 Delay the new way
    if (del_1 < 0) {
      module_result->goodDel_Link1 = true;
    } else {
      if ((rightspace[0][0] >= m_rightdeviation) && (rightspace[0][1] >= m_rightdeviation) && (leftspace[0][0] >= m_leftdeviation) && (leftspace[0][1] >= m_leftdeviation)) module_result->goodDel_Link1 = true;
      else module_result->goodDel_Link1 = false;
    }
  } else {
    module_result->goodThr_Link1 = true;
    module_result->goodDel_Link1 = true;
  }

  if(scan_iter->histo_Link2.size()) {
    // Judging Link2 Threshold  
    if (thr_2 <= 0) {
      module_result->goodThr_Link2 = true;
    } else {
      if ((avgThrMin[1][0] + m_lowerdistance <= thr_2) &&
	  (avgThrMin[1][1] + m_lowerdistance <= thr_2) &&
	  (avgThrMax[1][0] - m_upperdistance >= thr_2) &&
	  (avgThrMax[1][1] - m_upperdistance >= thr_2)) {
	module_result->goodThr_Link2 = true;
      } else {
	module_result->goodThr_Link2 = false;
      }
    }
    
    // Judging Link1 Delay the new way
    if (del_2 < 0) {
      module_result->goodDel_Link2 = true;
    } else {
      if ((rightspace[1][0] >= m_rightdeviation) && (rightspace[1][1] >= m_rightdeviation) && (leftspace[1][0] >= m_leftdeviation) && (leftspace[1][1] >= m_leftdeviation)) module_result->goodDel_Link2 = true;
      else module_result->goodDel_Link2 = false;
    }
  } else {
    module_result->goodThr_Link2 = true;
    module_result->goodDel_Link2 = true;
  }

  module_result->minErrors_Link1 = minVal1;
  module_result->minErrors_Link2 = minVal2;
  
  module_result->minErrors = (minVal1 < minVal2) ? minVal1 : minVal2;
  return true;

}

bool OPTOLINKanalysisROOT::QualifyTuning(int ih, int mod, float thr_1, float thr_2, float del_1, float del_2)
{
  std::cout << "JDDBG: You're in QualifyTuning (The last one, promised) ..." << std::endl;
  
  bool wellTuned = true;
  int minVal1;
  int minVal2;
  int lowerthresh[2], higherthresh[2];
  int leftdelay[2], rightdelay[2];
  int leftspace[2][2] = {{-2, -2}, {-2, -2}}, rightspace[2][2] = {{-2, -2}, {-2, -2}};
  
  
  std::cout << "JDDBG: Qualifying tuning points on " << m_scan->GetEntries() << " modules" << std::endl;

  AnalysisInputOPTOLINK * scan_iter = (AnalysisInputOPTOLINK *) m_scan->At(mod); 

  std::cout << "JDDBG: Trying to run on module " << mod << "... ";
  if(!(scan_iter->histo_Link1.size())) {
    std::cout << std::endl << "JDDBG: Histogram size for Link1 of mod " << mod << " is zero, skipping...";
  }
  if(!(scan_iter->histo_Link2.size())) {
    std::cout << std::endl << "JDDBG: Histogram size for Link2 of mod " << mod << " is zero, skipping...";
  }
  
  minVal1 = (scan_iter->Maxcount);
  minVal2 = (scan_iter->Maxcount);

  leftdelay[0] = -1;
  leftdelay[1] = -1;
  rightdelay[0] = -1;
  rightdelay[1] = -1;
  lowerthresh[0] = -1;
  lowerthresh[1] = -1;
  higherthresh[0] = -1;
  higherthresh[1] = -1;

  int thrmin[2][2] = {{-1, -1}, {-1, -1}};
  int thrmax[2][2] = {{-1, -1}, {-1, -1}};
  for(int idelay = 0; idelay < _NDelay; idelay++) {
    if (del_1 >= _DelayVal[idelay]) {
      leftdelay[0] = idelay + 1;
      rightdelay[0] = ((idelay + 1) % _NDelay) + 1;
    }
    if (del_2 >= _DelayVal[idelay]) {
      leftdelay[1] = idelay + 1;
      rightdelay[1] = ((idelay + 1) % _NDelay) + 1;
    }
  }

  for(int ithr = 0; ithr < _NThreshold; ithr++) {
    if (thr_1 >= _ThresholdVal[ithr]) {
      lowerthresh[0] = ithr + 1;
      higherthresh[0] = ( (lowerthresh[0] < _NThreshold) ? (lowerthresh[0] + 1) : (_NThreshold) );
    }
    if (thr_2 >= _ThresholdVal[ithr]) {
      lowerthresh[1] = ithr + 1;
      higherthresh[1] = ( (lowerthresh[1] < _NThreshold) ? (lowerthresh[1] + 1) : (_NThreshold) );
    }
  }

// This one deals with wrong accesses, but doesn't make the physical meaning any better - fixed above
//  if (leftdelay[0] == 0) leftdelay[0] = 25;
//  if (leftdelay[1] == 0) leftdelay[1] = 25;

  for(int idelay = 1; idelay <= _NDelay; idelay++) {
    for(int ithr = 1; ithr <= _NThreshold; ithr++) {
      if(scan_iter->histo_Link1.size()) {
	int val1 = int(scan_iter->histo_Link1[_ViSet]->GetBinContent(idelay,ithr));
	if (val1 < minVal1) minVal1 = val1;
        val1 = (val1 > (scan_iter->Maxcount) ? (scan_iter->Maxcount) : val1);
	if (idelay == leftdelay[0] && val1 < 1 && thrmin[0][0] == -1) thrmin[0][0] = ithr - 1;
	if (idelay == rightdelay[0] && val1 < 1 && thrmin[0][1] == -1) thrmin[0][1] = ithr - 1;
	if (idelay == leftdelay[0] && val1 < 1) thrmax[0][0] = ithr - 1;
	if (idelay == rightdelay[0] && val1 < 1) thrmax[0][1] = ithr - 1;
      }

      if(scan_iter->histo_Link2.size()) {
	int val2 = int(scan_iter->histo_Link2[_ViSet]->GetBinContent(idelay,ithr));
	if (val2 < minVal2) minVal2 = val2;
        val2 = (val2 > (scan_iter->Maxcount) ? (scan_iter->Maxcount) : val2);
	if (idelay == leftdelay[1] && val2 < 1 && thrmin[1][0] == -1) thrmin[1][0] = ithr - 1;
	if (idelay == rightdelay[1] && val2 < 1 && thrmin[1][1] == -1) thrmin[1][1] = ithr - 1;
	if (idelay == leftdelay[1] && val2 < 1) thrmax[1][0] = ithr - 1;
	if (idelay == rightdelay[1] && val2 < 1) thrmax[1][1] = ithr - 1;
      }
    }
  }
  
  for(int idelay = 0; idelay < _NDelay; idelay++) {
    for(int ithr = 1; ithr <= _NThreshold; ithr++) {
      if(scan_iter->histo_Link1.size()) {
	int val1 = int(scan_iter->histo_Link1[_ViSet]->GetBinContent(((((leftdelay[0] - 1) + idelay) % _NDelay) + 1), ithr));
        val1 = (val1 > (scan_iter->Maxcount) ? (scan_iter->Maxcount) : val1);
	if (ithr == lowerthresh[0] && val1 > 0.5 && rightspace[0][0] <= -1) rightspace[0][0] = idelay - 1;
	if (ithr == higherthresh[0] && val1 > 0.5 && rightspace[0][1] <= -1) rightspace[0][1] = idelay - 1;
	val1 = int(scan_iter->histo_Link1[_ViSet]->GetBinContent(((((leftdelay[0] + 24) - idelay) % _NDelay) + 1), ithr));
        val1 = (val1 > (scan_iter->Maxcount) ? (scan_iter->Maxcount) : val1);
	if (ithr == lowerthresh[0] && val1 > 0.5 && leftspace[0][0] <= -1) leftspace[0][0] = idelay - 1;
	if (ithr == higherthresh[0] && val1 > 0.5 && leftspace[0][1] <= -1) leftspace[0][1] = idelay - 1;
      }
      if(scan_iter->histo_Link2.size()) {
	int val2 = int(scan_iter->histo_Link2[_ViSet]->GetBinContent(((((leftdelay[1] - 1) + idelay) % _NDelay) + 1), ithr));
        val2 = (val2 > (scan_iter->Maxcount) ? (scan_iter->Maxcount) : val2);
	if (ithr == lowerthresh[1] && val2 > 0.5 && rightspace[1][0] <= -1) rightspace[1][0] = idelay - 1;
	if (ithr == higherthresh[1] && val2 > 0.5 && rightspace[1][1] <= -1) rightspace[1][1] = idelay - 1 ;
	val2 = int(scan_iter->histo_Link2[_ViSet]->GetBinContent(((((leftdelay[1] + 24) - idelay) % _NDelay) + 1), ithr));
        val2 = (val2 > (scan_iter->Maxcount) ? (scan_iter->Maxcount) : val2);
	if (ithr == lowerthresh[1] && val2 > 0.5 && leftspace[1][0] <= -1) leftspace[1][0] = idelay - 1;
	if (ithr == higherthresh[1] && val2 > 0.5 && leftspace[1][1] <= -1) leftspace[1][1] = idelay - 1;
      }
    }
  }
  if (rightspace[0][0] == -2) rightspace[0][0] = 25;
  if (rightspace[0][1] == -2) rightspace[0][1] = 25;
  if (rightspace[1][0] == -2) rightspace[1][0] = 25;
  if (rightspace[1][1] == -2) rightspace[1][1] = 25;
  if (leftspace[0][0] == -2) leftspace[0][0] = 25;
  if (leftspace[0][1] == -2) leftspace[0][1] = 25;
  if (leftspace[1][0] == -2) leftspace[1][0] = 25;
  if (leftspace[1][1] == -2) leftspace[1][1] = 25;
  std::cout << "[done]" << std::endl;

  int avgThrMin[2][2];
  int avgThrMax[2][2];

  for (int i = 0; i < 2; i++) {
    for (int j = 0; j < 2; j++) {
      if (thrmin[i][j] > 0) {
	avgThrMin[i][j] = (_ThresholdVal[thrmin[i][j]] + _ThresholdVal[thrmin[i][j] - 1]) / 2;
      }
      else {
	avgThrMin[i][j] = _ThresholdVal[0];
      }
    }
  }
  for (int i = 0; i < 2; i++) {
    for (int j = 0; j < 2; j++) {
      if (thrmax[i][j] == -1) {
	avgThrMax[i][j] = 0;
      }
      else {
	if (thrmax[i][j] < _NThreshold - 1) {
	  avgThrMax[i][j] = (_ThresholdVal[thrmax[i][j]] + _ThresholdVal[thrmax[i][j] + 1]) / 2;
	}
	else {
	  avgThrMax[i][j] = 999;
	}
      }
    }
  }
  
  std::cout << "Debugging module " << ((AnalysisInputOPTOLINK*)m_scan->At(mod))->moduleName << std::endl;
  std::cout << "Threshold min val 00: " << avgThrMin[0][0] << " with int val " << thrmin[0][0] << " at delay " << leftdelay[0] - 1 << std::endl;
  std::cout << "Threshold min val 01: " << avgThrMin[0][1] << " with int val " << thrmin[0][1] << " at delay " << rightdelay[0] - 1 << std::endl;
  std::cout << "Threshold min val 10: " << avgThrMin[1][0] << " with int val " << thrmin[1][0] << " at delay " << leftdelay[1] - 1 << std::endl;
  std::cout << "Threshold min val 11: " << avgThrMin[1][1] << " with int val " << thrmin[1][1] << " at delay " << rightdelay[1] - 1 << std::endl;
  std::cout << "Threshold max val 00: " << avgThrMax[0][0] << " with int val " << thrmax[0][0] << " at delay " << leftdelay[0] - 1 << std::endl;
  std::cout << "Threshold max val 01: " << avgThrMax[0][1] << " with int val " << thrmax[0][1] << " at delay " << rightdelay[0] - 1 << std::endl;
  std::cout << "Threshold max val 10: " << avgThrMax[1][0] << " with int val " << thrmax[1][0] << " at delay " << leftdelay[1] - 1 << std::endl;
  std::cout << "Threshold max val 11: " << avgThrMax[1][1] << " with int val " << thrmax[1][1] << " at delay " << rightdelay[1] - 1 << std::endl;
  std::cout << " Input tuned threshold 0: " << thr_1 << " delay 0: " << del_1 << std::endl;
  std::cout << " Input tuned threshold 1: " << thr_2 << " delay 0: " << del_2 << std::endl; 

  for(int ithr = 0; ithr < _NThreshold; ithr++) {
    if (thr_1 >= _ThresholdVal[ithr]) {
      lowerthresh[0] = ithr;
      higherthresh[0] = ithr + (ithr + 1 == _NThreshold ? 0 : 1);
    }
    if (thr_2 >= _ThresholdVal[ithr]) {
      lowerthresh[1] = ithr;
      higherthresh[1] = ithr + (ithr + 1 == _NThreshold ? 0 : 1);
    }
  }

  if(scan_iter->histo_Link1.size()) {
    // Judging Link1 Threshold  
    if (thr_1 <= 0) {
      wellTuned = (wellTuned && true);
    } else {
      if ((avgThrMin[0][0] + m_lowerdistance <= thr_1) &&
	  (avgThrMin[0][1] + m_lowerdistance <= thr_1) &&
	  (avgThrMax[0][0] - m_upperdistance >= thr_1) &&
	  (avgThrMax[0][1] - m_upperdistance >= thr_1)) {
        wellTuned = (wellTuned && true);
      } else {
        wellTuned = (wellTuned && false);
      }
    }
        
    // Judging Link1 Delay the new way
    if (del_1 < 0) {
      wellTuned = (wellTuned && true);
    } else {
      if ((rightspace[0][0] >= m_rightdeviation) && (rightspace[0][1] >= m_rightdeviation) && (leftspace[0][0] >= m_leftdeviation) && (leftspace[0][1] >= m_leftdeviation)) wellTuned = (wellTuned && true);
      else wellTuned = (wellTuned && false);

    }
  } else {
      wellTuned = (wellTuned && true);
  }

  if(scan_iter->histo_Link2.size()) {
    // Judging Link2 Threshold  
    if (thr_2 <= 0) {
      wellTuned = (wellTuned && true);
    } else {
      if ((avgThrMin[1][0] + m_lowerdistance <= thr_2) &&
	  (avgThrMin[1][1] + m_lowerdistance <= thr_2) &&
	  (avgThrMax[1][0] - m_upperdistance >= thr_2) &&
	  (avgThrMax[1][1] - m_upperdistance >= thr_2)) {
        wellTuned = (wellTuned && true);
      } else {
        wellTuned = (wellTuned && false);
      }
    }
    
    // Judging Link1 Delay the new way
    if (del_2 < 0) {
      wellTuned = (wellTuned && true);
    } else {
      if ((rightspace[1][0] >= m_rightdeviation) && (rightspace[1][1] >= m_rightdeviation) && (leftspace[1][0] >= m_leftdeviation) && (leftspace[1][1] >= m_leftdeviation)) wellTuned = (wellTuned && true);
      else wellTuned = (wellTuned && false);
    }
  } else {
      wellTuned = (wellTuned && true);
  }

  return wellTuned;

}

int OPTOLINKanalysisROOT::CommonSpotFinder(int ih) //, AnalysisResult* general_result)
{
//  ResultOPTOLINK* module_result = static_cast<ResultOPTOLINK*>(general_result);
  std::cout << "JDDBG: Spotfinder (Fooled you before, but hoping this is gonna be the last function to come into this analyses) ..." << std::endl;
  std::cout << "JDDBG: Searching for single spots \"inside\" the scan off " << m_scan->GetEntries() << " modules" << std::endl;

  int num_spots = 0;
  int sum_spots = 0;
  int num_links = 0;
// The following lines run over points in histograms and, for each module, check whether the point we're actually looking at is a standlone spot in a pattern like this:
//   0
//  010
//   0
// If so, the sum of spots is counted up - yet, the point is not removed from any analyses, but just counted, such that one can check the clobal view for general failures

  
  for(int idelay = 0; idelay < _NDelay; idelay++) {
    for(int ithr = 0; ithr < _NThreshold; ithr++) {
      num_spots = 0;
      num_links = 0;
      for(Int_t mod=0; mod < m_scan->GetEntries(); mod++) {
        AnalysisInputOPTOLINK * scan_iter = (AnalysisInputOPTOLINK *) m_scan->At(mod);
        if(scan_iter->histo_Link1.size()) {
	  num_links++;
	  if (int(scan_iter->histo_Link1[_ViSet]->GetBinContent((idelay % _NDelay) + 1,(ithr % _NThreshold) + 1)) >= 1) {
            if ((int(scan_iter->histo_Link1[_ViSet]->GetBinContent(((idelay + _NDelay - 1) % _NDelay) + 1,(ithr % _NThreshold) + 1)) < 1) &&
	        (int(scan_iter->histo_Link1[_ViSet]->GetBinContent(((idelay + 1) % _NDelay) + 1,(ithr % _NThreshold) + 1)) < 1) &&
	        (ithr == 0 ? true : (int(scan_iter->histo_Link1[_ViSet]->GetBinContent((idelay % _NDelay) + 1,((ithr - 1) % _NThreshold) + 1)) < 1)) &&
	        (ithr == (_NThreshold - 1) ? true : (int(scan_iter->histo_Link1[_ViSet]->GetBinContent((idelay % _NDelay) + 1,((ithr + 1) % _NThreshold) + 1)) < 1))) {
	      num_spots++;
	    }
	  }
        }
        if(scan_iter->histo_Link2.size()) {
	  num_links++;
          if (int(scan_iter->histo_Link2[_ViSet]->GetBinContent((idelay % _NDelay) + 1,(ithr % _NThreshold) + 1)) >= 1) {
            if ((int(scan_iter->histo_Link2[_ViSet]->GetBinContent(((idelay + _NDelay - 1) % _NDelay) + 1,(ithr % _NThreshold) + 1)) < 1) &&
	        (int(scan_iter->histo_Link2[_ViSet]->GetBinContent(((idelay + 1) % _NDelay) + 1,(ithr % _NThreshold) + 1)) < 1) &&
	        (ithr == 0 ? true : (int(scan_iter->histo_Link2[_ViSet]->GetBinContent((idelay % _NDelay) + 1,((ithr - 1) % _NThreshold) + 1)) < 1)) &&
	        (ithr == (_NThreshold - 1) ? true : (int(scan_iter->histo_Link2[_ViSet]->GetBinContent((idelay % _NDelay) + 1,((ithr + 1) % _NThreshold) + 1)) < 1))) {
	      num_spots++;
	    }
	  }
        }
      }
      if (num_spots == num_links) sum_spots++;
    }
  }
  std::cout << "[done] found " << sum_spots << " common spots inside histograms" << std::endl;
  return sum_spots;
}

void OPTOLINKanalysisROOT::QualifyResults() 
{
  m_status=kPassed;
}


void OPTOLINKanalysisROOT::CheckForKnownProblems() 
{
}

void OPTOLINKanalysisROOT::Print()
{
}

void OPTOLINKanalysisROOT::FindBestPhases(){//IBL

  for(int i=0; i<m_scan->GetEntries(); i++) {
    AnalysisInputOPTOLINK * scan_iter = (AnalysisInputOPTOLINK *) m_scan->At(i); 
    auto module_result = std::make_unique<ResultOPTOLINK> ();
    module_result->pp0Name = scan_iter->pp0Name;
    module_result->moduleName = scan_iter->moduleName;

    if(!scan_iter->histo_Link1.empty())
      getBestPhase( scan_iter->histo_Link1[0],module_result->optimalPhase_LINK1,module_result->passed_LINK1);

    if(!scan_iter->histo_Link2.empty())
      getBestPhase( scan_iter->histo_Link2[0],module_result->optimalPhase_LINK2,module_result->passed_LINK2);

    std::cout<<"Module "<<scan_iter->moduleName<<"_LINK1 Best phase "<<module_result->optimalPhase_LINK1<<std::endl;
    std::cout<<"Module "<<scan_iter->moduleName<<"_LINK2 Best phase "<<module_result->optimalPhase_LINK2<<std::endl;

    module_result->passed = module_result->passed_LINK1 && module_result->passed_LINK2;

    m_result->Add(module_result.release());
  }

  std::cout<<"Done finding best phases"<<std::endl;

}

void OPTOLINKanalysisROOT::getBestPhase(const TH2 * histo, unsigned int &bestPhase, bool &success){

  int maxErrors =0;  
  int worstPhase=-1;

    for(unsigned int phase = 0; phase < 4; phase++) {
      int errors = histo->GetBinContent(phase+1,1);
        if(errors > maxErrors ){
          maxErrors = errors;
          worstPhase=phase;
        }
        if (errors == 0){
          bestPhase= phase;
          success=true;
        }
    }
  //Ideally the best phase is the opposite from the worst phase however if it is not the case the phase with 0 errors is selected
  int bP = (worstPhase+2)%4;
  if(histo->GetBinContent(bP+1,1) == 0 )bestPhase = bP;

  std::cout<<"WorstPhase "<<worstPhase<<" BestPhase "<<bestPhase<<std::endl;

}

void OPTOLINKanalysisROOT::FindPreferedPoint()
{

  if(m_scan->GetEntries()==0) return;
  if(m_scan->GetEntries()>7) {
    std::cout << "OPTOLINKanalysisROOT: ERROR, pp0 with " << m_scan->GetEntries() << " modules" << std::endl;
    return;
  }

  _ViSet=0;
  Double_t zero(_DelayVal[0]);
  Double_t twentyfive(2*_DelayVal[_NDelay-1]-_DelayVal[_NDelay-2]);

  for(Int_t mod = 0; mod < m_scan->GetEntries(); mod++) {
    AnalysisInputOPTOLINK * scan_iter = (AnalysisInputOPTOLINK *) m_scan->At(mod); 
    for(int link=0;link<2;link++){
      if(!((link==0&&scan_iter->histo_Link1.size())||(link==1&&scan_iter->histo_Link2.size())))//check if link has histogram
	continue;
      TH2F* h; //histogram
      bool passed(true);
      if(link==0)
	h = (TH2F*)scan_iter->histo_Link1.back();
      else
	h = (TH2F*)scan_iter->histo_Link2.back();

      int startdelay = 0,enddelay = 0,startthr = 0,endthr = 0;  //borders of EFR

      {//set startdelay
	int sum=0;
	std::cout << "OPTOLINKanalysisROOT: Trying to run on module " << mod << " and link "<< link+1<< std::endl;
	bool inEFR(false);
	for(int idelay = 1; idelay <= _NDelay; idelay++) {
	  for(int ithr = 1; ithr <= _NThreshold; ithr++) {
	    int val1 = int(h->GetBinContent(idelay,ithr));
	    if(val1==0){
	      sum+=1;
	    }
	  }
	  
	  if(sum!=0&&idelay==1) {
	    inEFR=true;
	  }else if(sum!=0&&inEFR==false){
	    startdelay=idelay-2+_NDelay;
	    break;
	  }else if(sum==0){
	    inEFR=false;
	  }
	  sum=0;
	}
      }
      
      if(m_sp==RIGHTDOWN||m_sp==RIGHTUP||m_sp==FRAQ){//set enddelay
	int sum=0;
	
	//bool inEFR(false);
	for(int idelay = _NDelay; idelay >= 0; idelay--) {
	  for(int ithr = 1; ithr <= _NThreshold; ithr++) {
	    int val1 = int(h->GetBinContent((startdelay+idelay-1)%_NDelay+1,ithr));
	    if(val1==0){
	      sum+=1;
	    }
	  }
	  if(sum!=0){
	    enddelay=idelay;
	    break;
	  }
	  sum=0;
	}

      }

      if(m_sp==LEFTDOWN||m_sp==RIGHTDOWN||m_sp==FRAQ){//set startthr
	int sum=0;
	for(int ithr = 1; ithr <= _NThreshold; ithr++) {
	  for(int idelay = 1; idelay <= _NDelay; idelay++) {
	    int val1 = int(h->GetBinContent(idelay,ithr));
	    if(val1==0){
	      sum+=1;
	    }
	  }
	  
	  if(sum!=0){
	    startthr=ithr;
	    break;
	  }
	  sum=0;
	}


      }
      if(m_sp==LEFTUP||m_sp==RIGHTUP||m_sp==FRAQ){//set endthr
	int sum=0;
	for(int ithr = _NThreshold; ithr >= 1; ithr--) {
	  for(int idelay = 1; idelay <= _NDelay; idelay++) {
	    int val1 = int(h->GetBinContent(idelay,ithr));
	    if(val1==0){
	      sum+=1;
	    }
	  }
	  
	  if(sum!=0){
	    endthr=ithr;
	    break;
	  }
	  sum=0;
	}
      }
      Int_t minimum = 0;//minimum number of errors considered error free bin

      int sum = 0,sumv = 0,sumd = 0;
      if(m_sp==COM){
	for(int idelay = 1; idelay <= _NDelay; idelay++) {
	  for(int ithr = 1; ithr <= _NThreshold; ithr++) {
	    if(int(h->GetBinContent((idelay-1+startdelay)%_NDelay+1,ithr))<=minimum){
	      sumd+=idelay;
	      sumv+=ithr;
	      sum+=1;
	      //cout << "current delay: "<< (idelay+startdelay)%_NDelay  << "current avr: "<< (sumv/sum+startdelay)%_NDelay << std::endl;
	    }
	  }
	}
      }

      Int_t del = 0;//center of error free area
      Int_t thr = 0;

      if(sum==0)sum=1;
      Bool_t finetune(true);
      Bool_t bf(true);
      Int_t bfsteps(50);

      

      
	  

      Int_t xf(1);//finetune direction
      Int_t yf(1);
      int sp(m_sp);
      Double_t la(_DelayVal[1]-_DelayVal[0]);//bin width
      Double_t ta(_ThresholdVal[1]-_ThresholdVal[0]);
      Double_t sthr((m_upperdistance+ta+m_lowerdistance)/2);//outer area dimensions
      Double_t sdel((m_leftdeviation+la+m_rightdeviation)/2);
      Int_t rt(sthr/ta);//outer area dimensions in bins
      Int_t rd(sdel/la);

      Int_t diffd,difft; //inner rectangle dimensions(bins) of area. Have to be odd numbers for pointsymmetrie of area.
      difft=ceil((ta+abs(m_upperdistance-m_lowerdistance)*1.0)/ta);
      if(difft%2==0)
	difft++;
      diffd=ceil((la+abs(m_leftdeviation-m_rightdeviation)*1.0)/la);
      if(diffd%2==0)
	diffd++;

      
      
      Double_t f(1);//factor on area size
      Double_t delf=_DelayVal[1]-_DelayVal[0];
      Double_t thrf=std::min(m_leftdeviation,m_rightdeviation)/std::min(m_upperdistance,m_lowerdistance)*ta;
      Double_t maxdist((1.0/4+std::min(m_leftdeviation,m_rightdeviation)/delf)*delf);

      switch(m_sp){//set startpoints of algorithm and finetune directions
      case FRAQ:
	finetune=false;
	del = m_fraqdelay*enddelay*1.0+2-(Int_t)(m_leftdeviation-m_rightdeviation)/la/2;
	thr = m_fraqthr*(endthr-startthr)+startthr+(Int_t)(m_upperdistance-m_lowerdistance)/ta/2;
	break;
      case LEFTDOWN:
	xf=-1;
	yf=-1;
	del = sdel/la+2;
	thr = sthr/ta+startthr;
	break;
      case LEFTUP:
	xf=1;
	yf=-1;
	del = sdel/la+2;
	thr = endthr-sthr/ta;
	break;
      case RIGHTDOWN:
	xf=-1;
	yf=1;
	del = enddelay-sdel/la;
	thr = sthr/ta+startthr;
	break;
      case RIGHTUP:
	xf=1;
	yf=1;
	del = enddelay-sdel/la;
	thr = endthr-sthr/ta;
	break;
      case COM:
	f=2;
	del = (Int_t)sumd/sum;
	thr = (Int_t)sumv/sum;
	finetune=false;
	break;
      }


      Bool_t running(true);
      Int_t iterations(0);

      const Double_t mindist(0.005);

      Bool_t rep(false);

      std::vector<std::vector<bool>> mask (_NDelay, std::vector<bool>(_NThreshold, false));
      std::vector<std::vector<bool>> mask2 (_NDelay, std::vector<bool>(_NThreshold, false));

      Int_t fullarea(0);
      std::vector<std::vector<bool>> area ((Int_t)(2*rd*f)+1, std::vector<bool> ( (Int_t)(2*rt*f)+1, false));
      {
	Int_t df(rd*f);
	Int_t tf(rt*f);
	for(int i=0;i<=2*rd*f;i++){  //define area to keep error free
	  for(int j=0;j<=2*rt*f;j++){
	    if(abs(i-df)>rd*f||abs(j-tf)>rt*f)
	      area[i][j]=false;
	    else if(abs(i-df)<=(Int_t)((diffd-1)/2)*f||abs(j-tf)<=(Int_t)((difft-1)/2)*f)
	      fullarea++,area[i][j]=true;
	    else if(pow(delf*(i-(df-(Int_t)((diffd-1)/2*f))),2) + pow(thrf*(j-(tf+(Int_t)((difft-1)/2*f))),2)  <=  pow(f*maxdist,2))
	      fullarea++,area[i][j]=true;
	    else if(pow(delf*(i-(df-(Int_t)((diffd-1)/2*f))),2) + pow(thrf*(j-(tf-(Int_t)((difft-1)/2*f))),2)  <=  pow(f*maxdist,2))
	      fullarea++,area[i][j]=true;
	    else if(pow(delf*(i-(df+(Int_t)((diffd-1)/2*f))),2) + pow(thrf*(j-(tf+(Int_t)((difft-1)/2*f))),2)  <=  pow(f*maxdist,2))
	      fullarea++,area[i][j]=true;
	    else if(pow(delf*(i-(df+(Int_t)((diffd-1)/2*f))),2) + pow(thrf*(j-(tf-(Int_t)((difft-1)/2*f))),2)  <=  pow(f*maxdist,2))
	      fullarea++,area[i][j]=true;
	    else 
	      area[i][j]=false;
	  }
	}
      }
        
      //cout<<"fullarea: "<<fullarea<<endl;
	  
      while(running){
	Int_t sum2=0, sumv2 = 0,sumd2 = 0;//calculate COM in area
	int dmin(std::min(std::max(0,del-(Int_t)(rd*f)),_NDelay));
	int dmax(std::min(std::max(0,del+(Int_t)(rd*f)),_NDelay));
	int tmin(std::min(std::max(0,thr-(Int_t)(rt*f)),(Int_t)(rt*f+m_maxthr/ta)));
	int tmax(std::min(std::max(0,thr+(Int_t)(rt*f)),(Int_t)(rt*f+m_maxthr/ta)));
	for(int idelay = dmin; idelay <= dmax; idelay++) {
	  for(int ithr = tmin; ithr <= tmax; ithr++) {
	    if(area[idelay-dmin][ithr-tmin]||rep){
	      //Double_t dist(sqrt(pow(idelay-del,2)+pow(ithr-thr,2)));
	      Int_t val1;
	      
	      val1 = int(h->GetBinContent((idelay-1+startdelay)%_NDelay+1,ithr));
	      if(val1<=minimum){
		sumd2+=idelay;
		sumv2+=ithr;
		sum2+=1;
	      }
	    }
	  }
	}
	if(sum2==0){
	  std::cout<<"OPTOLINKanalysisROOT: Area in calculation is zero. Algorithm failed!";
	  if(int(h->GetBinContent(((Int_t)del+startdelay+_NDelay)%_NDelay+1,(Int_t)thr))!=0){
	    std::cout<<"  not in EFR!"<<std::endl;
	  }else{
	    std::cout<<"  in EFR"<<std::endl;
	  }
	  passed=false;
	  running=false;
	  break;
	}
	Double_t delw(sumd2*1.0/sum2);
	Double_t thrw(sumv2*1.0/sum2);
	
	if(fullarea==sum2){
	  std::cout<<"OPTOLINKanalysisROOT: converged after "<<iterations<<" iterations. (d="<<delw<<" t="<<thrw<<" error area="<<fullarea-sum2<<")"<<std::endl;
	  running=false;
	    
	  break;
	}
	//save checked positions
	if(!rep)
	  mask[del-1][thr-1]=true;
	else{
	  mask2[del-1][thr-1]=true;
	}
	//move point in direction of COM of EFR in area
	if(!rep){
	  if(del-delw<-mindist)
	    del+=1;
	  else if(del-delw>mindist)
	    del-=1;
	  if(thr-thrw<-mindist)
	    thr+=1;
	  else if(thr-thrw>mindist)
	    thr-=1;
	}else{
	  if(del-delw<-mindist)
	    del+=2;
	  else if(del-delw>mindist)
	    del-=2;
	  if(thr-thrw<-mindist)
	    thr+=2;
	  else if(thr-thrw>mindist)
	    thr-=2;
	}

	    
	if((!rep&&mask[del-1][thr-1])||(rep&&mask2[del-1][thr-1])){
	  if(sp==COM){//=============== MAXDIST ====================
	    if(f<=1.11){
	      //cout<<"Factor got smaller than 1.11"<<endl;
	      //cout<<"try to find point in FRAQ mode"<<endl;
	      sp=FRAQ;
	      f=1;
	    }else{
	      f-=0.1;
	    }
	    //clear masks
	    for(int i=0;i<_NDelay;i++){
	      for(int j=0;j<_NThreshold;j++){
		mask[i][j]=false;
	      }
	    }
	    for(int i=0;i<_NDelay;i++){
	      for(int j=0;j<_NThreshold;j++){
		mask2[i][j]=false;
	      }
	    }
	    fullarea=0;
	    {
	      Int_t df(rd*f);
	      Int_t tf(rt*f);
	      for(int i=0;i<=2*rd*f;i++){  //define area to keep error free
		for(int j=0;j<=2*rt*f;j++){
		  if(abs(i-df)>rd*f||abs(j-tf)>rt*f)
		    area[i][j]=false;
		  else if(abs(i-df)<=(Int_t)((diffd-1)/2)*f||abs(j-tf)<=(Int_t)((difft-1)/2)*f)
		    fullarea++,area[i][j]=true;
		  else if(pow(delf*(i-(df-(Int_t)((diffd-1)/2*f))),2) + pow(thrf*(j-(tf+(Int_t)((difft-1)/2*f))),2)  <=  pow(f*maxdist,2))
		    fullarea++,area[i][j]=true;
		  else if(pow(delf*(i-(df-(Int_t)((diffd-1)/2*f))),2) + pow(thrf*(j-(tf-(Int_t)((difft-1)/2*f))),2)  <=  pow(f*maxdist,2))
		    fullarea++,area[i][j]=true;
		  else if(pow(delf*(i-(df+(Int_t)((diffd-1)/2*f))),2) + pow(thrf*(j-(tf+(Int_t)((difft-1)/2*f))),2)  <=  pow(f*maxdist,2))
		    fullarea++,area[i][j]=true;
		  else if(pow(delf*(i-(df+(Int_t)((diffd-1)/2*f))),2) + pow(thrf*(j-(tf-(Int_t)((difft-1)/2*f))),2)  <=  pow(f*maxdist,2))
		    fullarea++,area[i][j]=true;
		  else 
		    area[i][j]=false;
		}
	      }
	    }
	    //DUMP(fullarea);
	  }else{//====================== NORMAL ===================
	    if(!rep){
	      rep=true;
	      f=2;
	      continue;
	    }else{
	      //cout<<"error area="<<fullarea-sum<<"  (d="<<del<<" t="<<thr<<")"<<endl;
	      std::cout<<"OPTOLINKanalysisROOT: stopping after "<<iterations<<" iterations without minimum distance (d="<<delw<<" t="<<thrw<<") ";
	      std::cout<<"  Algorithm failed! EFR might be to small."<<std::endl;

	      passed=false;
	      running=false;
	      f=1;
	      break;
	    }
	  }
	}else if(rep){
	  rep=false;
	  mask2[del-1][thr-1]=true;
	  f=1;
	}
	  
	iterations++;
	if(iterations>m_maxiter){
	  std::cout<<"OPTOLINKanalysisROOT: maximal number of "<<m_maxiter<<" iterations reached. (d="<<delw<<" t="<<thrw<<" maxdist="<<maxdist<<")";
	  if(int(h->GetBinContent(((Int_t)del+startdelay+_NDelay)%_NDelay+1,(Int_t)thr))!=0){
	    std::cout<<"   not in EFR!";
	  }else{
	    std::cout<<"   in EFR.";
	  }
	  std::cout<<"   Algorithm failed!"<<std::endl;
	  passed=false;
	  running=false;
	}
      }



      if(!passed&&bf){//search in a square around the last point
	int savedel(del);
	int savethr(thr);
	int layer(1);
	int leg(0);
	int x(0);
	int y(0);
	for(int i=0;i<bfsteps;i++){
	  switch(leg){
	  case 0: ++x; if(x  == layer)  ++leg;                break;
	  case 1: ++y; if(y  == layer)  ++leg;                break;
	  case 2: --x; if(-x == layer)  ++leg;                break;
	  case 3: --y; if(-y == layer){ leg = 0; ++layer; }   break;
	  }
	  if(del+x>0&&del+x<_NDelay&&thr+y>0&&thr+y<m_maxthr/ta)
	    if(mask[x+del-1][y+thr-1]==false){
	      mask[x+del-1][y+thr-1]=true;
	      if(h->GetBinContent((x+del-1+startdelay)%_NDelay+1,y+thr)<=minimum){
		Int_t sum3(0);
		int dmin(std::min(std::max(0,del-(Int_t)(rd*f)),_NDelay));
		int dmax(std::min(std::max(0,del+(Int_t)(rd*f)),_NDelay));
		int tmin(std::min(std::max(0,thr-(Int_t)(rt*f)),(Int_t)(rt*f+m_maxthr/ta)));
		int tmax(std::min(std::max(0,thr+(Int_t)(rt*f)),(Int_t)(rt*f+m_maxthr/ta)));
		for(int idelay = dmin; idelay <= dmax; idelay++) {
		  for(int ithr = tmin; ithr <= tmax; ithr++) {
		    if(area[idelay-dmin][ithr-tmin]){
		      Int_t val1 = int(h->GetBinContent((idelay-1+startdelay)%_NDelay+1,ithr));
		      if(val1>minimum)
			goto endloop;
		      else
			sum3++;
		    }
		  }
		}
		if(sum3==fullarea){
		  passed=true;
		  del+=x;
		  thr+=y;
		  break;
		}
	      endloop:;
	      
	      }
	    }
	}
	if(!passed){
	  del=savedel;
	  thr=savethr;
	}

      }
      if(!passed){     //improve point if algorithm failed
	for(int c=0;c<3;c++){
	  Double_t sum4=0,sumv4 = 0,sumd4 = 0;
	  int dmin(std::min(std::max(0,del-(Int_t)(rd*f)),_NDelay));
	  int dmax(std::min(std::max(0,del+(Int_t)(rd*f)),_NDelay));
	  int tmin(std::min(std::max(0,thr-(Int_t)(rt*f)),(Int_t)(rt*f+m_maxthr/ta)));
	  int tmax(std::min(std::max(0,thr+(Int_t)(rt*f)),(Int_t)(rt*f+m_maxthr/ta)));
	  for(int idelay = dmin; idelay <= dmax; idelay++) {
	    for(int ithr = tmin; ithr <= tmax; ithr++) {
	      if(area[idelay-dmin][ithr-tmin]){
		if(int(h->GetBinContent((idelay-1+startdelay)%_NDelay+1,ithr))<=minimum){
		  sumd4+=idelay;
		  sumv4+=ithr;
		  sum4+=1;
		}
	      }
	    }
	  }
	  Double_t delw = 0;
	  Double_t thrw = 0;
	  delw = sumd4*1.0/sum4;
	  thrw = sumv4*1.0/sum4;
	  if(del-delw<-mindist)
	    del+=1;
	  else if(del-delw>mindist)
	    del-=1;
	  if(thr-thrw<-mindist)
	    thr+=1;
	  else if(thr-thrw>mindist)
	    thr-=1;
	}
      }
      if(passed&&finetune){    //fine tune distance (remove gap between algo area and EFR)
	for(int in = 0;in<_NThreshold;in++){
	  thr+=xf;
	  Int_t sum5=0;
	  int dmin(std::min(std::max(0,del-(Int_t)(rd*f)),_NDelay));
	  int dmax(std::min(std::max(0,del+(Int_t)(rd*f)),_NDelay));
	  int tmin(std::min(std::max(0,thr-(Int_t)(rt*f)),(Int_t)(rt*f+m_maxthr/ta)));
	  int tmax(std::min(std::max(0,thr+(Int_t)(rt*f)),(Int_t)(rt*f+m_maxthr/ta)));
	  for(int idelay = dmin; idelay <= dmax; idelay++) {
	    for(int ithr = tmin; ithr <= tmax; ithr++) {
	      if(area[idelay-dmin][ithr-tmin]){
		//Double_t dist(sqrt(pow(idelay-del,2)+pow(ithr-thr,2)));
		Int_t val1;
		val1 = int(h->GetBinContent((idelay-1+startdelay)%_NDelay+1,ithr));
		//cout<<"val"<<val1;
		if(val1<=minimum){
		  sum5+=1;
		  //cout << "sum5="<<sum5 << std::endl;
		}
	      }
	    }
	  }
	  //cout<<"finetune: "<<fullarea<<"  "<<sum<<endl;
	  if(fullarea!=sum){
	    thr-=xf;
	    break;
	  }
	}
	for(int in = 0;in<_NThreshold;in++){
	  del+=yf;
	  Int_t sum6=0;
	  int dmin(std::min(std::max(0,del-(Int_t)(rd*f)),_NDelay));
	  int dmax(std::min(std::max(0,del+(Int_t)(rd*f)),_NDelay));
	  int tmin(std::min(std::max(0,thr-(Int_t)(rt*f)),(Int_t)(rt*f+m_maxthr/ta)));
	  int tmax(std::min(std::max(0,thr+(Int_t)(rt*f)),(Int_t)(rt*f+m_maxthr/ta)));
	  for(int idelay = dmin; idelay <= dmax; idelay++) {
	    for(int ithr = tmin; ithr <= tmax; ithr++) {
	      if(area[idelay-dmin][ithr-tmin]){
		if(int(h->GetBinContent((idelay-1+startdelay)%_NDelay+1,ithr))<=minimum){
		  sum6+=1;
		}
	      }
	    }
	  }
	  if(fullarea!=sum6){
	    del-=yf;
	    break;
	  }
	}
      }
      
      if(passed){
	_Delay[mod][link] =fmod(_DelayVal[(startdelay+del-1+_NDelay)%_NDelay]+(m_leftdeviation-m_rightdeviation)/2+twentyfive,twentyfive);
	_Thr[mod][link] =std::max(_ThresholdVal[0],std::min(_ThresholdVal[_NThreshold-1],_ThresholdVal[thr-1]-(m_upperdistance-m_lowerdistance)/2));
      }else{
	_Delay[mod][link] = _DelayVal[(startdelay+(Int_t)del-1+_NDelay)%_NDelay];
	_Thr[mod][link] = _ThresholdVal[(Int_t)thr-1];
      }
      _Passed[mod][link] = passed;
	
      
    }//link loop
    
    //If the links lie on different sides of the delay axis the closer one to the side (0 or 24) is moved to the side of the other.
    if(scan_iter->histo_Link1.size()&&scan_iter->histo_Link2.size()){
      if (_Delay[mod][0] < 0.25*twentyfive && _Delay[mod][1] >  0.75*twentyfive){
	if(abs(_Delay[mod][0]-zero)<abs(_Delay[mod][1]-twentyfive))
	  _Delay[mod][0]=twentyfive;
	else
	  _Delay[mod][1]=zero;

      }else if(_Delay[mod][1] < 0.25*twentyfive && _Delay[mod][0] >  0.75*twentyfive){
	if(abs(_Delay[mod][0]-twentyfive)<abs(_Delay[mod][1]-zero))
	  _Delay[mod][0]=zero;
	else
	  _Delay[mod][1]=twentyfive;
      }
    }

    if(scan_iter->histo_Link1.size()) {
      std::cout << "MOD: Ideal Delay found on Link1 is " << _Delay[mod][0] << std::endl;
    }
    if(scan_iter->histo_Link2.size()) {
      std::cout << "MOD: Ideal Delay found on Link2 is " << _Delay[mod][1] << std::endl;
    }
    if(scan_iter->histo_Link1.size()) {
      std::cout << "MOD: Ideal Threshold found on Link1 is " << _Thr[mod][0] << std::endl;
    }
    if(scan_iter->histo_Link2.size()) {
      std::cout << "MOD: Ideal Threshold found on Link2 is " << _Thr[mod][1] << std::endl;
    }
  }//module loop
}
