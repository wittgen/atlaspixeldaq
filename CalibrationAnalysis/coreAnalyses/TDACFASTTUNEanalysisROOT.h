#ifndef _TDACFASTTUNEanalysisROOT_h_
#define _TDACFASTTUNEanalysisROOT_h_ 

#include "ROOTanalysisBase.h"
#include "analysisInput_TDACFASTTUNE.h"
#include "analysisOutput_TDACFASTTUNE.h"
#include "TH2I.h"
#include "ROOTanalysisHelper.h"

class TDACFASTTUNEanalysisROOT  : public ROOTanalysisBase
{
 public:
  TDACFASTTUNEanalysisROOT();
  ~TDACFASTTUNEanalysisROOT();


 private:
  void Initialize();

  void Calculate(); //internal calculations

  void AdditionalCalculations(int module); // more calculations for analog scans

  void QualifyResults(); //apply cuts from analysis config

  void CheckForKnownProblems(); //setting of failure bits

  void Print();

 public:

  Float_t TDACmean_lowerCut;
  Float_t TDACmean_upperCut;
  Float_t Percentage_lowTDAC;
  Float_t Percentage_highTDAC;
  Float_t TDAC_toolow;
  Float_t TDAC_toohigh;
  Float_t Mean_lowerCut;
  Float_t Mean_upperCut;
  Float_t RMS_upperCut;
  Float_t Pixel_w_no_hits;
  Float_t Pixel_w_all_hits;
  Float_t max_chi2;

 private: //variables

  bool allModulesPassed;
  bool Passed;

  // Algorithm parameters
  // --------------------

  int _Nevents;
  int _Ntdacsteps;
  
  float _Mean;
  float _RMS;
  float _MeanPercentage;
  float _RMSPercentage;
  float _chi2;
  float _Pixel_no_hits;
  float _Pixel_all_hits;
  float _Pixel_more_hits;
  float _Mean_tdac_all;
  bool _allFEok;
  std::vector<float> _vec_extraHits;

  
  
};


#endif

