#ifndef _analysisOutputANALOG_h_
#define _analysisOutputANALOG_h_

#include <vector>
#include <TH2F.h>
#include <TString.h>


class ResultANALOG : public TObject { //this is for one scan and one module
 public:
  ResultANALOG() :moduleName(""),passed(0),tests(0) {};
  ~ResultANALOG() {};

  TString moduleName;
  UInt_t passed;
  UInt_t tests;

  Float_t average[6]; //for 6 different pixel types

  TH2F hErrorMap; 
  TH2F hErrorMap_minus1; 
  TH2F hErrorMap_minus19; 
  TH2F hErrorMap_plus1; 

  TH1F hInt; //integrating over total module
  std::vector<TH1F> hInt_FEs; //integrated current histograms for different FEs   

  TH1F AvPerFE; 
  TH1F RMSPerFE; 
  Float_t variance_mean_FEs;
  Float_t variance_RMS_FEs;

  //ClassDef(ScanHistoANALOG,1)  // not needed at the moemnt, put in if needed!
};

#endif
