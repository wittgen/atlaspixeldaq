#ifndef _analysisOutputT0_h_
#define _analysisOutputT0_h_

#include "analysisOutput_Base.h"

struct ResultT0 : public AnalysisResult { //this is for one scan and one module

  std::array<float, 2> conv {0, 0};
  std::array<int, 2> T0 {-1, -1};
  std::array<float, 2> rising_edge {-1., -1.};
  std::array<float, 2> falling_edge {-1., -1.};
  std::array<int, 2> best_delay_range {-1, -1};
  std::array<int, 2> failed_scan {0, 0};
  std::array<bool, 2> passedFE {false, false};

};

#endif
