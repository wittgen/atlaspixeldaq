#ifndef _analysisInputANNA_h_
#define _analysisInputANNA_h_

#include "analysisInput_Base.h"
#include <vector>
#include <TH2F.h>
#include <TString.h>
#include <iostream>

class AnalysisInputANNA : public AnalysisInput { //this is for one scan and one module
 public:
  AnalysisInputANNA() :AnalysisInput() {};
  ~AnalysisInputANNA() {};

  const TH2* histo; 
  std::vector<histoAxis_t> axis; //the axis of the histo (the same for all histos) of type histoAxis_t 
  
  TString moduleName;
  Int_t MBs; //40,80,160 Mbits/s
  TString pp0Name;
  Int_t Nmasks;
  Int_t Nevents;
  


  ////////////////////////////////
  ///////Analysis Results
  UInt_t passed;
  UInt_t failure;
  Float_t average;


  //Helper functions
  void PrintInfo() {
    std::cout << "AnalysisInputANNA for pp0 " << pp0Name.Data() << ", module: " << moduleName.Data() << "  MBit/s=" << MBs << std::endl;
  };
  
  //ClassDef(AnalysisInputANNA,1) 
};
    

#endif
