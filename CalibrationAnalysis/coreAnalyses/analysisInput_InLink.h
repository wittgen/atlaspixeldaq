#ifndef _analysisInputInLink_h_
#define _analysisInputInLink_h_

#include "analysisInput_Base.h"


class AnalysisInputInLink : public AnalysisInput { //this is for one scan and one module
 public:
  AnalysisInputInLink() :AnalysisInput(), MBs(-9) {};
  ~AnalysisInputInLink() {};

  //Rod as base unit 
  TString  rodName; 

  // histogram
  const TH2* histo_InLink;
  const TH2* histo_chi2;  // pixle maps
  const TH2* histo_mean;  
  const TH2* histo_sigma;  
  //  std::vector<histoAxis_t> axis; //the axis of the histo (the same for all histos) 


  Int_t MBs;    /////   [40,80,160 MHz]
  Int_t nPixel;  //depending on mask steps
  std::vector<histoAxis_t> axis; //the axis of the histo (the same for all histos) of type histoAxis_t 

  //InLink/OutLink MCC speed is 40 MHz
  Int_t moduleId;
  Int_t inLink;
  Int_t outLink1A;
  Int_t outLink2A;
  Int_t bocLinkTx;

  //Helper functions
  void PrintInfo() {
    std::cout << "AnalysisInputInLink for module: " << moduleName.Data() << "  MBit/s=" << MBs <<std::endl;
    if (histo_InLink) std::cout << "  HistoEntries:" << histo_InLink->GetEntries() << std::endl;
    else std::cout <<" No histogram attach to this module "<<std::endl;
  };

 public:
  //ClassDef(AnalysisInputInLink,1) //not 0 -> activate I/O 
};

#endif
