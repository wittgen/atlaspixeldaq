#ifndef _analysisOutputTOT_h_
#define _analysisOutputTOT_h_

#include <TString.h>
#include <TH2F.h>
#include "analysisOutput_Base.h"


class ResultTOT : public AnalysisResult { // this stores the results for one scan and one module
 public:
  ResultTOT():AnalysisResult() {};
  ~ResultTOT() {};
  
  //std::string module_name;

  //bool Status; // defined in base
  //bool passed; // defined in base
  bool good_tuning; 
  bool good_pixels;
  bool good_closure;

  float tot_mip;
  //float sigma_mip;
  float disp_mip;
  float frac_bad_tot_fit_closure; //new
  float frac_bad_disp_fit_closure; // new
  float charge_corr; //new

   float n_passed_fe_pixtype;

  std::vector<std::vector<unsigned short int> > fe_pixtype_passed;
  
  std::vector<std::vector<UInt_t> > fe_pixtype_closure_failed_mask;
  std::vector<std::vector<UInt_t> > fe_pixtype_badpixel_failed_mask;

  std::vector<std::vector<Float_t> > fe_pixtype_frac_bad_tot_fit_closure; 
  std::vector<std::vector<Float_t> > fe_pixtype_charge_corr; 
  std::vector<std::vector<Float_t> > fe_pixtype_frac_bad_disp_fit_closure; 
  std::vector<std::vector<Float_t> > fe_pixtype_frac_bad_pixels;
  std::vector<std::vector<UInt_t> > fe_pixtype_n_bad_pixels;

  std::vector<std::vector<std::vector<Float_t> > > fe_pixtype_tot;
  std::vector<std::vector<std::vector<Float_t> > > fe_pixtype_tot_err;
  std::vector<std::vector<Float_t> > fe_pixtype_tot_chi2;
  //std::vector<std::vector<std::vector<Float_t> > > fe_pixtype_sigma;
  //std::vector<std::vector<std::vector<Float_t> > > fe_pixtype_sigma_err;
  //std::vector<std::vector<Float_t> > fe_pixtype_sigma_chi2;
  std::vector<std::vector<std::vector<Float_t> > > fe_pixtype_disp;
  std::vector<std::vector<std::vector<Float_t> > > fe_pixtype_disp_err;
  std::vector<std::vector<Float_t> > fe_pixtype_disp_chi2;

  std::vector<std::vector<Float_t> > fe_pixtype_tot_mip;
  //std::vector<std::vector<Float_t> > fe_pixtype_sigma_mip;
  std::vector<std::vector<Float_t> > fe_pixtype_disp_mip;

  std::shared_ptr<TH2F> h2d_healthy;

  std::shared_ptr<TH2F> h2d_closure_perpixel_mean_tot_fit_module;
  std::shared_ptr<TH2F> h2d_closure_perpixel_tot_fit_module;
  std::shared_ptr<TH2F> h2d_closure_pergroup_disp_fit_module;
  std::shared_ptr<TH1F> h1d_closure_perpixel_mean_tot_fit_module;
  std::shared_ptr<TH1F> h1d_closure_perpixel_tot_fit_module;
  std::shared_ptr<TH1F> h1d_closure_pergroup_disp_fit_module;
  std::shared_ptr<TH2F> h2d_residual_vs_charge_perpixel_mean_tot_fit_module;
  std::shared_ptr<TH2F> h2d_residual_vs_charge_perpixel_tot_fit_module;
  std::shared_ptr<TH2F> h2d_residual_vs_mean_tot_pergroup_disp_fit_module;

  std::vector<std::vector<std::shared_ptr<TH2F> > > h2d_closure_perpixel_mean_tot_fit;
  std::vector<std::vector<std::shared_ptr<TH2F> > > h2d_closure_perpixel_tot_fit;
  std::vector<std::vector<std::shared_ptr<TH2F> > > h2d_closure_pergroup_disp_fit;
  std::vector<std::vector<std::shared_ptr<TH1F> > > h1d_closure_perpixel_mean_tot_fit;
  std::vector<std::vector<std::shared_ptr<TH1F> > > h1d_closure_perpixel_tot_fit;
  std::vector<std::vector<std::shared_ptr<TH1F> > > h1d_closure_pergroup_disp_fit;

  std::vector<std::vector<std::shared_ptr<TH2F> > > h1d_closure_vs_charge_perpixel_mean_tot_fit;
  std::vector<std::vector<std::shared_ptr<TH2F> > > h1d_closure_vs_charge_pergroup_mean_tot_fit;
  std::vector<std::vector<std::shared_ptr<TH2F> > > h1d_closure_vs_charge_perpixel_tot_fit;
  std::vector<std::vector<std::shared_ptr<TH2F> > > h1d_closure_vs_charge_pergroup_disp_fit;
  std::vector<std::vector<std::shared_ptr<TH2F> > > h1d_closure_vs_mean_tot_pergroup_disp_fit;

  std::vector<std::vector<std::shared_ptr<TH2F> > > h1d_residual_vs_charge_perpixel_mean_tot_fit;
  std::vector<std::vector<std::shared_ptr<TH2F> > > h1d_residual_vs_charge_pergroup_mean_tot_fit;
  std::vector<std::vector<std::shared_ptr<TH2F> > > h1d_residual_vs_charge_perpixel_tot_fit;
  std::vector<std::vector<std::shared_ptr<TH2F> > > h1d_residual_vs_charge_pergroup_disp_fit;
  std::vector<std::vector<std::shared_ptr<TH2F> > > h1d_residual_vs_mean_tot_pergroup_disp_fit;

  std::vector<std::vector<std::shared_ptr<TH2F> > > h1d_pull_vs_charge_perpixel_mean_tot_fit;
  std::vector<std::vector<std::shared_ptr<TH2F> > > h1d_pull_vs_charge_pergroup_mean_tot_fit;
  std::vector<std::vector<std::shared_ptr<TH2F> > > h1d_pull_vs_charge_perpixel_tot_fit;
  std::vector<std::vector<std::shared_ptr<TH2F> > > h1d_pull_vs_charge_pergroup_disp_fit;
  std::vector<std::vector<std::shared_ptr<TH2F> > > h1d_pull_vs_mean_tot_pergroup_disp_fit;

  std::vector<std::vector<std::vector<std::shared_ptr<TH1F> > > > h1d_closure_perpixel_pervcal_mean_tot_fit;
  std::vector<std::vector<std::vector<std::shared_ptr<TH1F> > > > h1d_closure_pergroup_pervcal_mean_tot_fit;
  std::vector<std::vector<std::vector<std::shared_ptr<TH1F> > > > h1d_closure_pergroup_pervcal_disp_fit;
  std::vector<std::vector<std::vector<std::shared_ptr<TH1F> > > > h1d_residual_perpixel_pervcal_mean_tot_fit;
  std::vector<std::vector<std::vector<std::shared_ptr<TH1F> > > > h1d_residual_pergroup_pervcal_mean_tot_fit;
  std::vector<std::vector<std::vector<std::shared_ptr<TH1F> > > > h1d_residual_pergroup_pervcal_disp_fit;
  std::vector<std::vector<std::vector<std::shared_ptr<TH1F> > > > h1d_pull_pergroup_pervcal_mean_tot_fit;
  std::vector<std::vector<std::vector<std::shared_ptr<TH1F> > > > h1d_pull_pergroup_pervcal_disp_fit;

  //std::vector<std::vector<std::vector<std::shared_ptr<TH2F> > > > h1d_closure_pergroup_pervcal_mean_tot_disp_fit;


  // ClassDef(ResultTOT,1); //not 0 -> activate I/O  ; not needed at the moment, put in if needed!
  
};

#endif
