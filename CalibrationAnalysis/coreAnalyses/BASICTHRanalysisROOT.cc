#ifndef _BASICTHRanalysisROOT_CC_
#define _BASICTHRanalysisROOT_CC_

#include "BASICTHRanalysisROOT.h"
#include <iostream>
#include "TMath.h"

BASICTHRanalysisROOT::BASICTHRanalysisROOT()
{
  std::cout << "BASICTHRanalysis:  constructed." << std::endl;
} 


BASICTHRanalysisROOT::~BASICTHRanalysisROOT()
{
  //WriteResults();
}

void BASICTHRanalysisROOT::WriteResults()
{
  TFile f("resultsBASICTHR.root","update");
  f.WriteTObject(m_result.get(),"scanHistos","SingleKey"); //as one array
  f.Close();  
}

void BASICTHRanalysisROOT::Initialize()
{
  //! Don't hard code here. Take values from analysis configuration, defaults in AutomaticAnalysis.

  if(cut_average_threshold_min == 0 && cut_average_threshold_max == 0 && cut_RMS_threshold_max == 0 && cut_average_noise_min == 0 && cut_average_noise_max == 0 && cut_RMS_noise_max == 0 && cut_failed_fit_max == 0) {
    std::cout << "WARNING   Apparent problem in reading analysis config, all cuts are zero.  -> Setting default values." << std::endl;
    cut_average_threshold_min = 3000;
    cut_average_threshold_max = 5000;
    cut_RMS_threshold_max = 200;
    cut_average_noise_min = 140;
    cut_average_noise_max = 190;
    cut_RMS_noise_max = 8;
    cut_failed_fit_max = 64;
  }

  AnalysisInputBASICTHR* basicthr = (AnalysisInputBASICTHR*)m_scan->At(0);
  if(!basicthr || !basicthr->histo_chi2) return;
  PixGeometry geo(PixA::nRows(basicthr->histo_chi2),PixA::nColumns(basicthr->histo_chi2)); 
  _ChipType = geo.pixType();

  _Nrow = helper.getNrow(_ChipType); 
  _Ncol = helper.getNcol(_ChipType); 
}  

void BASICTHRanalysisROOT::Calculate() 
{
  std::cout << "**************************************" << std::endl;
  std::cout << "BASICTHRanalysis::Calculate  starting" << std::endl;
  m_scan->R__FOR_EACH(AnalysisInputBASICTHR,PrintInfo)(); 
  std::cout << "BASICTHRanalysis: These were the module names." << std::endl;  

    double preval = -1;
    
    int Nfe=helper.getNfrontEnd(_ChipType); // # of FEs in one module
    //int Ncp=helper.getNcp(_ChipType); // # of the enable column pairs
    
  // Loop over modules
  for(Int_t i=0; i<m_scan->GetEntries(); i++) { 
    ResultBASICTHR* module_result = new ResultBASICTHR();
    module_result->pp0Name = ((AnalysisInputBASICTHR*)m_scan->At(i))->pp0Name;
    module_result->moduleName = ((AnalysisInputBASICTHR*)m_scan->At(i))->moduleName;

    if(!((AnalysisInputBASICTHR*)m_scan->At(i))->histo_chi2 || !((AnalysisInputBASICTHR*)m_scan->At(i))->histo_threshold || !((AnalysisInputBASICTHR*)m_scan->At(i))->histo_noise) {
      std::cout << "BASICTHRanalysis: WARNING  not all histos for module " << module_result->moduleName << " -> skipping." << std::endl;
      module_result->passed=false;
      m_result->Add( module_result );
      continue;
    }

    if(((AnalysisInputBASICTHR*)m_scan->At(i))->histo_chi2->GetEntries()==0 && ((AnalysisInputBASICTHR*)m_scan->At(i))->histo_threshold->GetEntries()==0 && ((AnalysisInputBASICTHR*)m_scan->At(i))->histo_noise->GetEntries()==0) {
      std::cout << "BASICTHRanalysis: WARNING  no histos for module " << module_result->moduleName << " -> skipping." << std::endl;
      module_result->passed=false;
      m_result->Add( module_result );
      continue;
    }
    std::cout << "Analysing module: " << module_result->moduleName << std::endl; 
    
    // Fill 1D histos and pixelmap of pixels with failed fit

    module_result->hInt_threshold = *(new TH1F("Threshold_1D", "Threshold distribution for all pixels, no failed fits", 500, 0., 10000.));
    module_result->hInt_noise = *(new TH1F("Noise_1D", "Noise distribution for all pixels, no failed fits", 500, 0., 1000.));
    //module_result->failed_fit_histo = *(new TH2F("FailedFit_map", "Map of pixels with failed fit", helper.Ncols, 0., helper.Ncols, helper.Nrows, 0., helper.Nrows));
    module_result->failed_fit_histo = *(new TH2F("FailedFit_map", "Map of pixels with failed fit", _Ncol, 0., _Ncol, _Nrow, 0., _Nrow));

    //    std::cout << "nmask stage: " << ((AnalysisInputBASICTHR*)m_scan->At(i))->nMaskStages << std::endl;
    //std::cout << "Ncol, Nrows: " << _Ncol << ", " << _Nrow << std::endl;
    //    for (int ix = 1; ix <= helper.Ncols; ix++) {
      if(preval != ((AnalysisInputBASICTHR*)m_scan->At(i))->nMaskStages){
      std::cout << " [maria elena] maskstages :" << ((AnalysisInputBASICTHR*)m_scan->At(i))->nMaskStages << std::endl;
      }
      preval = ((AnalysisInputBASICTHR*)m_scan->At(i))->nMaskStages ;

      // get fe & cp disable information for each module
      _fe_disable = ((AnalysisInputBASICTHR*)m_scan->At(i))->fe_disable;
      _cp_enable = ((AnalysisInputBASICTHR*)m_scan->At(i))->cp_enable;
      
    for (int ix = 1; ix <= _Ncol; ix++) {
      //      for (int iy = 1; iy <= helper.Nrows; iy ++) {
      for (int iy = 1; iy <= _Nrow; iy ++) {
	Float_t threshold = ((AnalysisInputBASICTHR*)m_scan->At(i))->histo_threshold->GetBinContent(ix,iy);
	Float_t noise = ((AnalysisInputBASICTHR*)m_scan->At(i))->histo_noise->GetBinContent(ix,iy);
	//Float_t chi2 = ((AnalysisInputBASICTHR*)m_scan->At(i))->histo_chi2->GetBinContent(ix,iy);
     if(threshold == 0 && noise == 0) {
	  if (helper.isMasked(ix-1,iy-1,((AnalysisInputBASICTHR*)m_scan->At(i))->nMaskStages,((AnalysisInputBASICTHR*)m_scan->At(i))->nTOTMaskStages,  _ChipType)){
          module_result->failed_fit_map[std::make_pair(ix,iy)] = 0x10; // I use bit 4, that should be a spare in the maps
	    module_result->failed_fit_histo.SetBinContent(ix,iy,1);
	  }
	}
	else {
	  module_result->hInt_threshold.Fill(threshold);
	  module_result->hInt_noise.Fill(noise);
	}
      }
    }

//******************************[maria elena]
      // set expected value to 0 if FE is disabled, this should prevent disabled FEs from being counted
      for(int fe = 0; fe < Nfe; fe++){
          if(_fe_disable.size() <= 0) continue;
          if(_fe_disable[fe] == 1){
              std::cout<<"BASICTHRanalysis::Calculate(): FE "<<fe<<"on module "<<((AnalysisInputBASICTHR*)m_scan->At(i))->moduleName<<"is disabled!"<<std::endl;
              for(int col = helper.getFrontEndStartCol(fe, _ChipType); col <= helper.getFrontEndStopCol(fe, _ChipType); col++){
                  for(int row = helper.getFrontEndStartRow(fe, _ChipType); row <= helper.getFrontEndStopRow(fe, _ChipType); row++){
                      Float_t threshold = ((AnalysisInputBASICTHR*)m_scan->At(i))->histo_threshold->GetBinContent(col+1,row+1);
                      Float_t noise = ((AnalysisInputBASICTHR*)m_scan->At(i))->histo_noise->GetBinContent(col+1,row+1);
                      if(threshold == 0 && noise == 0) {
                          if (helper.isMasked(col,row,((AnalysisInputBASICTHR*)m_scan->At(i))->nMaskStages,((AnalysisInputBASICTHR*)m_scan->At(i))->nTOTMaskStages,  _ChipType)){
                              module_result->failed_fit_map[std::make_pair(col+1,row+1)] = 0; // I use bit 4, that should be a spare in the maps
                              int ix= col+1;
                              int iy= row+1;
                      module_result->failed_fit_histo.SetBinContent(ix,iy,0);
                          }
                      }
                      
                  }
              }
          }
          if(_cp_enable.size() <= 0) continue;
         // for(int cp = 0; cp < Ncp; cp++){
          for(int col = helper.getFrontEndStartCol(fe, _ChipType); col <= helper.getFrontEndStopCol(fe, _ChipType); col++){
              for(int row = helper.getFrontEndStartRow(fe, _ChipType); row <= helper.getFrontEndStopRow(fe, _ChipType); row++){
                  int cp= helper.columnPair(col, row, _ChipType);

              if(_cp_enable[fe][cp] == 0){
                  std::cout<<"BASICTHRanalysis::Calculate(): "<<"CP "<< cp<< " in FE "<<fe<<" on module "<<((AnalysisInputBASICTHR*)m_scan->At(i))->moduleName<<" is disabled!"<<std::endl;
                //  for(int row = helper.getFrontEndStartRow(fe, _ChipType); row <= helper.getFrontEndStopRow(fe, _ChipType); row++){
                   //   for(int col=cp*2; col<cp*2+2; cp++){
                          Float_t threshold = ((AnalysisInputBASICTHR*)m_scan->At(i))->histo_threshold->GetBinContent(col+1,row+1);
                          Float_t noise = ((AnalysisInputBASICTHR*)m_scan->At(i))->histo_noise->GetBinContent(col+1,row+1);
                          if(threshold == 0 && noise == 0) {
                              if (helper.isMasked(col,row,((AnalysisInputBASICTHR*)m_scan->At(i))->nMaskStages,((AnalysisInputBASICTHR*)m_scan->At(i))->nTOTMaskStages,  _ChipType)){
                                  module_result->failed_fit_map[std::make_pair(col+1,row+1)] = 0; // I use bit 4, that should be a spare in the maps
                                  int ix= col+1;
                                  int iy= row+1;
                          module_result->failed_fit_histo.SetBinContent(ix,iy,0);
                    //          }
                         }
                      }
                      
                  }
              }
          }

      }

//********************************************************
    // Apply cuts

    // Rescale allowed number of unfitted pixels to scanned mask stages
    module_result->failed_fit = (Int_t)module_result->failed_fit_histo.Integral();
    std::cout << "Module: " << module_result->moduleName << " failed fit: " <<  module_result->failed_fit  << std::endl;
    Float_t fraction = (Float_t)(module_result->failed_fit)/(Float_t)(((AnalysisInputBASICTHR*)m_scan->At(i))->nPixel_abs_frac*_Ncol*_Nrow);

    //Float_t allowed = (Float_t)(cut_failed_fit_max)/(Float_t)(helper.Ncols * helper.Nrows);
    Float_t allowed = (Float_t)(cut_failed_fit_max)/(Float_t)(_Ncol * _Nrow);

    if (fraction > allowed) {
      module_result->passed_failed_fit_max = false;
    }
    else {
      module_result->passed_failed_fit_max = true;
    }
    std::cout << "Module: " << module_result->moduleName << " failed fit: " << fraction << " allowed: " << allowed << ": " << module_result->passed_failed_fit_max << std::endl;  

    // Cut window in average threshold
    module_result->average_threshold = module_result->hInt_threshold.GetMean();
    std::cout << "Module: " << module_result->moduleName << " average threshold: " << module_result->average_threshold << " window: " << cut_average_threshold_min << " - " << cut_average_threshold_max << std::endl;  
      
      //    std::cout << "av thr, av thr max: " << module_result->average_threshold << ", " << cut_average_threshold_max << std::endl;
    if (module_result->average_threshold > cut_average_threshold_max) {
      module_result->passed_average_threshold_max = false;
    }
    else {
      module_result->passed_average_threshold_max = true;
    }
    if (module_result->average_threshold < cut_average_threshold_min) {
      module_result->passed_average_threshold_min = false;
    }
    else {
      module_result->passed_average_threshold_min = true;
    }

    // Cut window in average noise
    module_result->average_noise = module_result->hInt_noise.GetMean();
    std::cout << "Module: " << module_result->moduleName << " average noise: " << module_result->average_noise << " window: " << cut_average_noise_min << " - " << cut_average_noise_max << std::endl;
      
    if (module_result->average_noise > cut_average_noise_max) {
      module_result->passed_average_noise_max = false;
    }
    else {
      module_result->passed_average_noise_max = true;
    }
    if (module_result->average_noise < cut_average_noise_min) {
      
      module_result->passed_average_noise_min = false;
    }
    else {
      module_result->passed_average_noise_min = true;
    }    

    // Upper cut on RMS of threshold distribution
    module_result->RMS_threshold = module_result->hInt_threshold.GetRMS();
    std::cout << "Module: " << module_result->moduleName << " RMS threshold: " << module_result->RMS_threshold << " max: " << cut_RMS_threshold_max << std::endl;
    //    std::cout << "rms thre, rms thre max: " << module_result->RMS_threshold << ", " << cut_RMS_threshold_max << std::endl;
    if (module_result->RMS_threshold > cut_RMS_threshold_max) {
      module_result->passed_RMS_threshold_max = false;
    }
    else {
      module_result->passed_RMS_threshold_max = true;
    }

    //Upper cut on RMS of noise distribution
    module_result->RMS_noise = module_result->hInt_noise.GetRMS();
    std::cout << "Module: " << module_result->moduleName << " RMS noise: " << module_result->RMS_noise << " max: " << cut_RMS_noise_max << std::endl;
    //  std::cout << "rms no, rms no max: " << module_result->RMS_noise << ", " << cut_RMS_noise_max << std::endl; 
    if (module_result->RMS_noise > cut_RMS_noise_max) {
      module_result->passed_RMS_noise_max = false;
    }
    else {
      module_result->passed_RMS_noise_max = true;
    }


    module_result->passed = (module_result->passed_failed_fit_max &&  
			     module_result->passed_average_threshold_min &&
			     module_result->passed_average_threshold_max &&
			     module_result->passed_RMS_threshold_max &&
			     module_result->passed_average_noise_min &&
			     module_result->passed_average_noise_max &&
			     module_result->passed_RMS_noise_max); 
    
    std::cout << "Result for module " << module_result->moduleName << " is: " << ( (module_result->passed)?  "OK":"FAILED" ) << std::endl;
    m_result->Add( module_result );
  }

  std::cout << "  after calculate  results size: " << m_result->GetEntries() << std::endl;

}

void BASICTHRanalysisROOT::QualifyResults() 
{
  bool allPassed(true);
  for(Int_t i=0; i<m_result->GetEntries(); i++) { 
    ResultBASICTHR* module_result = (ResultBASICTHR*)m_result->At(i); 
    if(!module_result->passed) { 
      allPassed=false;
      break;
    }
  }  
  if(allPassed)
    m_status=kPassed;
  else 
    m_status=kFailed;

  std::cout << "BASICTHRanalysis::QualifyResults  Total result is :" << ( (allPassed)? "OK":"FAILED" ) << std::endl;
  std::cout << "*************************************" << std::endl;
}

#endif

