#ifndef _analysisInput_Base_h_
#define _analysisInput_Base_h_

#include <iostream>
#include <vector>
#include <list>
#include <memory>

#include <TObjArray.h>
#include <TString.h>
#include <TH2F.h>

inline Stat_t entriesSafe(const TH1 *h1) {
  if (h1) {
    return h1->GetEntries();
  }
  else {
    return -1.;
  }
}

class AnalysisInput : public TObject { //this is for one scan and one module
 public:
  AnalysisInput() : pp0Name(""), moduleName("") {};
  virtual ~AnalysisInput() {};

  TString pp0Name;
  TString moduleName; 

  std::vector<unsigned short int> fe_disable;
  std::vector<std::vector<unsigned short int> > cp_enable;

  //  ClassDef(AnalysisInput,1)  
};


class histoAxis_t : public TObject {
 public:
  TString variableName;
  float firstBin;
  float lastBin;
  Int_t nSteps;

  //ClassDef(histoAxis_t,1) 
};

typedef std::shared_ptr<TObjArray> AnalysisInput_t;  //bosst classes don't work with ROOT IO 
typedef std::shared_ptr<TObjArray> AnalysisResult_t;

#endif
