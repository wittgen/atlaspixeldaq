#ifndef _analysisOutputTOTbasic_h_
#define _analysisOutputTOTbasic_h_

#include "analysisOutput_Base.h"
#include "TH1F.h"

class ResultTOTbasic : public AnalysisResult { //this is for one scan and one module
 public:
  ResultTOTbasic(){
  };
  ~ResultTOTbasic() {};

  float PixPassing;
  float PixFailingHigh;
  float PixFailingLow;
  float PixFailingSigma;
  float PixGood;
  float PixDead;
  float PctPassing; 
  float PctFailingHigh;
  float PctFailingLow;
  float PctFailingSigma;
  float PctGood;
  float PctDead;
  float TOTperMod;
  float SIGperMod;
  float TOTEperMod;
  float SIGEperMod;

  float TOTfe0, TOTfe1, TOTfe2, TOTfe3, TOTfe4, TOTfe5, TOTfe6, TOTfe7, TOTfe8, TOTfe9, TOTfe10, TOTfe11, TOTfe12, TOTfe13, TOTfe14, TOTfe15;
  float SIGfe0, SIGfe1, SIGfe2, SIGfe3, SIGfe4, SIGfe5, SIGfe6, SIGfe7, SIGfe8, SIGfe9, SIGfe10, SIGfe11, SIGfe12, SIGfe13, SIGfe14, SIGfe15;

  TH1F* TOTperFE;
  TH1F* TOTperPixel;
  TH1F* SIGperFE;
  TH1F* SIGperPixel;

 public:
  //ClassDef(ResultTOTbasic,1) //not 0 -> activate I/O  ; not needed at the moemnt, put in if needed!
};

#endif
