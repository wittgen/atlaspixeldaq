#ifndef _analysisOutputDIFF_h_
#define _analysisOutputDIFF_h_

#include <TString.h>
#include <TH2F.h>
#include "analysisOutput_Base.h"
#include "PixelCoralClientUtils/PixelMap.h"


class ResultDIFF : public AnalysisResult { //this is for one scan and one module
 public:
  ResultDIFF() {
  };
  ~ResultDIFF() {};

  std::map< std::string, std::shared_ptr<TH2F> >  histos_2d_out;
  std::map< std::string, std::shared_ptr<TH1F> >  histos_1d_out;

  //std::map< std::string, TH2F >  histos_2d_out;
  //std::map< std::string, TH1F >  histos_1d_out;
  std::map< std::string, float>  histos_dev_out;

 public:
  //ClassDef(ResultDIFF,1) //not 0 -> activate I/O  ; not needed at the moemnt, put in if needed!
};

#endif
