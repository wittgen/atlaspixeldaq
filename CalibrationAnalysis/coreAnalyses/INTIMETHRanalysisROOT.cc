#ifndef _INTIMETHRanalysisROOT_CC_
#define _INTIMETHRanalysisROOT_CC_

#include "INTIMETHRanalysisROOT.h"
#include <PixDbGlobalMutex.hh>
#include <Lock.hh>
#include <iostream>
#include "TMath.h"

INTIMETHRanalysisROOT::INTIMETHRanalysisROOT(): _Ncol(0), _Nrow(0), _Nfe(0), _ChipType(PixGeometry::INVALID_MODULE)
{
  std::cout << "INTIMETHRanalysis:    INFO INTIMETHRanalysisROOT() constructed" << std::endl;
}

INTIMETHRanalysisROOT::~INTIMETHRanalysisROOT()
{
  // WriteResults();
}

void INTIMETHRanalysisROOT::WriteResults()
{
  std::cout << "INTIMETHRanalysis:    INFO WriteResults() starting" << std::endl;
  TFile f("resultsINTIMETHR.root","update");
  f.WriteTObject(m_result.get(),"scanHistos","SingleKey");	// as one array
  f.Close();  
}

void INTIMETHRanalysisROOT::Initialize()
{
  //! Don't hard code here. Take values from analysis configuration, defaults in AutomaticAnalysis.
  
  std::cout << "INTIMETHRanalysis:    INFO Initialize() starting" << std::endl;

  helperThr = new ROOTanalysisHelper(3, true);	// helper uses 3 pixel classes
  helperNoise = new ROOTanalysisHelper(4, true);	// helper uses 4 pixel classes


  AnalysisInputINTIMETHR* h_mean = (AnalysisInputINTIMETHR*) m_scan->At(0);
  PixGeometry geo(PixA::nRows(h_mean->histo_mean),PixA::nColumns(h_mean->histo_mean)); 
  _ChipType = geo.pixType();
  
  _Nrow = helperThr->getNrow(_ChipType);
  _Ncol = helperThr->getNcol(_ChipType);
  _Nfe = helperThr->getNfrontEnd(_ChipType); // # of FEs in one module

}  

void INTIMETHRanalysisROOT::Calculate() 
{
  std::cout << "INTIMETHRanalysis:    INFO **************************************" << std::endl;
  std::cout << "INTIMETHRanalysis:    INFO Calculate() starting module analysis" << std::endl;
  m_scan->R__FOR_EACH(AnalysisInputINTIMETHR, PrintInfo)();
  std::cout << "INTIMETHRanalysis:    INFO ^^ these were the module names" << std::endl;
  
  // Loop over modules
  for(Int_t i=0; i<m_scan->GetEntries(); i++)
    { 
      ResultINTIMETHR* module_result = new ResultINTIMETHR();
      module_result->pp0Name = ((AnalysisInputINTIMETHR*)m_scan->At(i))->pp0Name;
      module_result->moduleName = ((AnalysisInputINTIMETHR*)m_scan->At(i))->moduleName;
      module_result->MakePixelMap = create_Pixelmap;
      
      if((!((AnalysisInputINTIMETHR*)m_scan->At(i))->histo_chi2 &&
	  !((AnalysisInputINTIMETHR*)m_scan->At(i))->histo_mean &&
	  !((AnalysisInputINTIMETHR*)m_scan->At(i))->histo_sigma)
	 ||
	 (	((AnalysisInputINTIMETHR*)m_scan->At(i))->histo_chi2->GetEntries()==0 &&
		((AnalysisInputINTIMETHR*)m_scan->At(i))->histo_mean->GetEntries()==0 &&
		((AnalysisInputINTIMETHR*)m_scan->At(i))->histo_sigma->GetEntries()==0))
	{
	  std::cout << "INTIMETHRanalysis: WARNING no histograms for module " << module_result->moduleName << "! -> skipping" << std::endl;
	  module_result->passed=false;
	  m_result->Add(module_result);
	  continue;
	}
      std::cout << "INTIMETHRanalysis:    INFO analyzing module " << module_result->moduleName << std::endl;
      // All 'passed' result variables are initialized as 'false' in analysisOutput_INTIMETHR.h
      
      // Calculate module variables: threshold
      if(!((AnalysisInputINTIMETHR*)m_scan->At(i))->histo_mean || ((AnalysisInputINTIMETHR*)m_scan->At(i))->histo_mean->GetEntries()==0)
	std::cout << "INTIMETHRanalysis: WARNING no threshold histogram" << std::endl;
      else
	{
	  std::cout << "INTIMETHRanalysis:    INFO -- Calculating threshold values over module --" << std::endl;
	  
	  // Loop over the 3 classes of pixels, compute average_threshold and RMS_threshold for each case, and try threshold RMS max cut
	  for(Int_t pixtype=0; pixtype<helperThr->NpixelClasses; pixtype++)
	    {
	      std::cout << "Now looking at pixel class: " << helperThr->getPixelClassString(pixtype+1,helperThr->NpixelClasses) << std::endl;
	      // std::shared_ptr<TH1F> histo_mean = helperThr->IntegrateHisto(((AnalysisInputINTIMETHR*)m_scan->At(i))->histo_mean, 1, 10000);
	      std::shared_ptr<TH1F> histo_mean_restricted = helperThr->IntegrateHisto(((AnalysisInputINTIMETHR*)m_scan->At(i))->histo_mean, range_thr_mean_min, range_thr_mean_max, pixtype+1);
	      std::cout << "cutting on threshold_mean and _RMS: " << std::endl;
	      if( helperThr->FulfillMinMaxCut( histo_mean_restricted->GetMean(), cut_thr_mean_min[pixtype], cut_thr_mean_max[pixtype]))  module_result->passed_module_threshold_mean[pixtype]=true; 
	      if( helperThr->FulfillMaxCut( histo_mean_restricted->GetRMS(), cut_thr_RMS_max[pixtype]) )  module_result->passed_module_threshold_RMS[pixtype]=true;	     
 	      module_result->average_threshold[pixtype] = histo_mean_restricted->GetMean();
	      module_result->RMS_threshold[pixtype] = histo_mean_restricted->GetRMS();
	    }
	}
      

      // Calculate module variables: noise
      if(!((AnalysisInputINTIMETHR*)m_scan->At(i))->histo_sigma || ((AnalysisInputINTIMETHR*)m_scan->At(i))->histo_sigma->GetEntries()==0)
	std::cout << "INTIMETHRanalysis: WARNING no noise histogram" << std::endl;
      else
	{
	  std::cout << "INTIMETHRanalysis:    INFO -- Calculating noise values over module --" << std::endl;
	  
	  // Loop over the 4 classes of pixels, and compute average_noise and RMS_noise for each case
	  for(Int_t pixtype=0; pixtype<helperNoise->NpixelClasses; pixtype++)
	    {
	      std::cout << "Now looking at pixel class: " << helperNoise->getPixelClassString(pixtype+1,helperNoise->NpixelClasses) << std::endl;
	      // std::cout << "INTIMETHRanalysis::Calculate(): histo_sigma = " << static_cast<const void *>(((AnalysisInputINTIMETHR*)m_scan->At(i))->histo_sigma) << std::endl;
	      std::shared_ptr<TH1F> histo_sigma = helperNoise->IntegrateHisto(((AnalysisInputINTIMETHR*)m_scan->At(i))->histo_sigma, 1, 2000, pixtype+1);
	      if( helperNoise->FulfillMinMaxCut( histo_sigma->GetMean(), cut_noise_min[pixtype], cut_noise_max[pixtype]))  module_result->passed_module_noise[pixtype]=true;
	      module_result->average_noise[pixtype] = histo_sigma->GetMean();
	      module_result->RMS_noise[pixtype] = histo_sigma->GetRMS();
	    }
	}
      
     
      // Get disabled FE info
      FE_disable = ((AnalysisInputINTIMETHR*)m_scan->At(i))->fe_disable;
      CP_enable = ((AnalysisInputINTIMETHR*)m_scan->At(i))->cp_enable;

      // Find num_bad_pixels(per FE) and module_result->numBadPixels(total)
      std::cout << "INTIMETHRanalysis:    INFO -- Finding bad pixels in enabled FEs --" << std::endl;
      findBadPixels(module_result, i, FE_disable, CP_enable);
      
      // Loop over FE
      //for(Int_t FE=0; FE<16; FE++)
      for(Int_t FE=0; FE<_Nfe; FE++)
	{
	  std::cout << "INTIMETHRanalysis:    INFO analyzing FE" << FE << " on module " << module_result->moduleName << std::endl;
	  // Disabled FEs are skipped!
	  if(FE_disable[FE] == 1)
	    {
	      std::cout << "INTIMETHRanalysis: WARNING this FE is disabled! -> skipping" << std::endl;
	      
	      // Clear all FE variables
	      for(Int_t pixtype=0; pixtype<helperThr->NpixelClasses; pixtype++)	module_result->average_threshold_FE[FE][pixtype]=0;
	     	      
	      // Set disabled FEs to pass
	      module_result->passed_FE_noise_min[FE]=true;
	      module_result->passed_FE_noise_max[FE]=true;
	      module_result->passed_FE_threshold_min[FE]=true;
	      module_result->passed_FE_threshold_max[FE]=true;
	      module_result->passed_FE_threshold_mean_min[FE]=true;
	      module_result->passed_FE_threshold_mean_max[FE]=true;
	      module_result->passed_FE_threshold_RMS_max[FE]=true;
	      module_result->passed_FE_cuts[FE]=true;

	      // Skip to next FE
	      continue;
	    }
	  
	  // Calculate FE variables: noise
	  if(!((AnalysisInputINTIMETHR*)m_scan->At(i))->histo_sigma || ((AnalysisInputINTIMETHR*)m_scan->At(i))->histo_sigma->GetEntries()==0)
	    std::cout << "INTIMETHRanalysis: WARNING no noise histogram" << std::endl;
	  else
	    {
	      module_result->passed_FE_noise_min[FE]=true;
	      module_result->passed_FE_noise_max[FE]=true;
	      
	      // Loop over the 4 classes of pixels, and compute average_noise and RMS_noise for each case
	      for(Int_t pixtype=0; pixtype<helperNoise->NpixelClasses; pixtype++)
		{
		  std::shared_ptr<TH1F> histo_fe_sigma = helperNoise->IntegrateHisto(((AnalysisInputINTIMETHR*)m_scan->At(i))->histo_sigma, 1,  2000, pixtype+1, FE);
		  
		  // If no histogram entries, the FE has failed!
		  if(!histo_fe_sigma || histo_fe_sigma->GetEntries()==0)
		    {
		      std::cout << "INTIMETHRanalysis:  FAILED noise cuts for '" << helperNoise->getPixelClassString(pixtype+1) << "': no noise histogram" << std::endl;
		      module_result->passed_FE_noise_min[FE]=false;
		      module_result->passed_FE_noise_max[FE]=false;
		    }
		  else
		    {
		      if(!helperNoise->FulfillMinCut(histo_fe_sigma.get(), cut_noise_min[pixtype], cut_noise_min_percent[pixtype]/Float_t(100))) module_result->passed_FE_noise_min[FE]=false;
		      if(!helperNoise->FulfillMaxCut(histo_fe_sigma.get(), cut_noise_max[pixtype], cut_noise_max_percent[pixtype]/Float_t(100))) module_result->passed_FE_noise_max[FE]=false;
		    }
		}
	    }
	  
	  // Calculate FE variables: threshold
	  if(!((AnalysisInputINTIMETHR*)m_scan->At(i))->histo_mean || ((AnalysisInputINTIMETHR*)m_scan->At(i))->histo_mean->GetEntries()==0) 
	    std::cout << "INTIMETHRanalysis: WARNING no threshold histogram" << std::endl;
	  else
	    {
	      // Loop over the 3 classes of pixels, compute average_threshold and RMS_threshold for each case
	      for(Int_t pixtype=0; pixtype<helperThr->NpixelClasses; pixtype++)
		{
		  std::shared_ptr<TH1F> histo_fe_mean_cut = helperThr->IntegrateHisto(((AnalysisInputINTIMETHR*)m_scan->At(i))->histo_mean, range_thr_mean_min, range_thr_mean_max, ((AnalysisInputINTIMETHR*)m_scan->At(i))->histo_sigma, cut_noise_min[pixtype], 2000, pixtype+1, FE);
		  std::shared_ptr<TH1F> histo_fe_mean_restricted = helperThr->IntegrateHisto(((AnalysisInputINTIMETHR*)m_scan->At(i))->histo_mean, range_thr_mean_min, range_thr_mean_max, pixtype+1, FE);
		  module_result->passed_FE_threshold_min[FE]=true;
		  module_result->passed_FE_threshold_max[FE]=true; 
		  module_result->passed_FE_threshold_RMS_max[FE]=true;
		  module_result->passed_FE_threshold_mean_min[FE]=true;
		  module_result->passed_FE_threshold_mean_max[FE]=true;
		  // If no entries in restricted range, the FE has failed!
		  if(!histo_fe_mean_restricted || histo_fe_mean_restricted->GetEntries()==0)
		    {
		      std::cout << "INTIMETHRanalysis:  FAILED all threshold cuts for '" << helperThr->getPixelClassString(pixtype+1) << "': no threshold histogram" << std::endl;
		      module_result->passed_FE_threshold_min[FE]=false;
		      module_result->passed_FE_threshold_max[FE]=false;
		      module_result->passed_FE_threshold_mean_min[FE]=false;
		      module_result->passed_FE_threshold_mean_max[FE]=false;
		      module_result->passed_FE_threshold_RMS_max[FE]=false;
		    }
		  else
		    {
		      if(!helperThr->FulfillMinCut(histo_fe_mean_restricted.get(), cut_thr_min[pixtype], cut_thr_min_percent[pixtype]/Float_t(100))) {
			module_result->passed_FE_threshold_min[FE]=false;
			std::cout << "INTIMETHRanalysis:  FAILED threshold min cut for '" << helperThr->getPixelClassString(pixtype+1) << "'" << std::endl;
		      }
		      if(!helperThr->FulfillMaxCut(histo_fe_mean_restricted.get(), cut_thr_max[pixtype], cut_thr_max_percent[pixtype]/Float_t(100))) {
			module_result->passed_FE_threshold_max[FE]=false;
			std::cout << "INTIMETHRanalysis:  FAILED threshold max cut for '" << helperThr->getPixelClassString(pixtype+1) << "'" << std::endl;
		      }
		      
		      if(!helperThr->FulfillMinCut(histo_fe_mean_restricted->GetMean(), cut_thr_mean_min[pixtype])){
			module_result->passed_FE_threshold_mean_min[FE]=false;
			std::cout << "INTIMETHRanalysis:  FAILED threshold mean min cut for '" << helperThr->getPixelClassString(pixtype+1) << "'" << std::endl;
		      }
		      if(!helperThr->FulfillMaxCut(histo_fe_mean_restricted->GetMean(), cut_thr_mean_max[pixtype])) {
			module_result->passed_FE_threshold_mean_max[FE]=false;
			std::cout << "INTIMETHRanalysis:  FAILED threshold mean max cut for '" << helperThr->getPixelClassString(pixtype+1) << "'" << std::endl;
		      }
		      if(!helperThr->FulfillMaxCut(histo_fe_mean_restricted->GetRMS(), cut_thr_RMS_max[pixtype])) {
			module_result->passed_FE_threshold_RMS_max[FE]=false;
			std::cout << "INTIMETHRanalysis:  FAILED threshold RMS max cut for '" << helperThr->getPixelClassString(pixtype+1) << "'" << std::endl;
		      }
		      module_result->average_threshold_FE[FE][pixtype] = histo_fe_mean_cut->GetMean();
		      std::cout << "CALC AVG THRESH " << " FE#" << FE << " TYPE " << pixtype << " w/o cut: " << histo_fe_mean_restricted->GetMean() << " with cut: " << histo_fe_mean_cut->GetMean() << std::endl;
		    }
		}
	    }      
	}
      
      // Final check if FE passed/failed
      module_result->passed_all_fe_noise=true;
      module_result->passed_all_fe_threshold_mean=true;
      module_result->passed_all_fe_threshold_RMS=true;
      module_result->passed_all_fe_bad_pixel=true;
      //for(Int_t FE=0; FE<16; FE++)
      for(Int_t FE=0; FE<_Nfe; FE++)
	{
	  module_result->passed_FE_cuts[FE]=true;
	  // Check FE noise
	  if(!module_result->passed_FE_noise_min[FE] || !module_result->passed_FE_noise_max[FE])
	    {
	      std::cout << "INTIMETHRanalysis:  FAILED noise min/max cut for FE" << FE << std::endl;
	      module_result->passed_all_fe_noise=false;
	      module_result->passed_FE_cuts[FE]=false;
	    }
	  // Check FE threshold mean
	  if ( !module_result->passed_FE_threshold_min[FE] ||
	       !module_result->passed_FE_threshold_max[FE] ||
	       !module_result->passed_FE_threshold_mean_min[FE] ||
	       !module_result->passed_FE_threshold_mean_max[FE] ){
	    std::cout << "INTIMETHRanalysis:  FAILED threshold mean min/max cut for FE" << FE << std::endl;
	    module_result->passed_all_fe_threshold_mean=false;
	    module_result->passed_FE_cuts[FE]=false;
	  }
	  // Check FE threshold RMS
	  if(!module_result->passed_FE_threshold_RMS_max[FE])
	    {
	      std::cout << "INTIMETHRanalysis:  FAILED threshold RMS max cut for FE" << FE << std::endl;
	      module_result->passed_all_fe_threshold_RMS=false;
	      module_result->passed_FE_cuts[FE]=false;
	    }
	  // Check number of bad pixels
	  if(num_bad_pixels[FE] > cut_num_bad_pixels_per_FE)
	    {
	      std::cout << "INTIMETHRanalysis:  FAILED maximum no. of bad pixels cut for FE" << FE << std::endl;
	      module_result->passed_all_fe_bad_pixel=false;
	      module_result->passed_FE_cuts[FE]=false;
	    }
	  // A "good" FE is one which is enabled and passes all cuts
	  if(FE_disable[FE]==0 && module_result->passed_FE_cuts[FE]==true)
	    module_result->numGoodFEs++;
	}
      
      // Final check if module passed/failed
      module_result->passed = (	//module_result->passed_module_threshold_mean &&
				//module_result->passed_module_threshold_RMS &&
		                module_result->numGoodFEs>8);
      
      std::cout << "INTIMETHRanalysis:    INFO module " << module_result->moduleName << " has " << ( (module_result->passed)?  "PASSED!":"FAILED!" ) << std::endl;
      m_result->Add(module_result);
    }
  
  std::cout << "INTIMETHRanalysis:    INFO no. of modules analysed: " << m_result->GetEntries() << std::endl;
  std::cout << "INTIMETHRanalysis:    INFO Calculate() finished" << std::endl;
}


//Not being used!
void INTIMETHRanalysisROOT::AdditionalCalculations() 
{
  //
}

void INTIMETHRanalysisROOT::findBadPixels(ResultINTIMETHR* module, Int_t ientry, std::vector<unsigned short int> fe_disable, std::vector<std::vector<unsigned short int> > cp_enable)
{
  std::cout << "INTIMETHRanalysis:    INFO findBadPixels() starting" << std::endl;
  const TH2* threshold = ((AnalysisInputINTIMETHR*)m_scan->At(ientry))->histo_mean;
  const TH2* noise = ((AnalysisInputINTIMETHR*)m_scan->At(ientry))->histo_sigma;
  const TH2* chi2 = ((AnalysisInputINTIMETHR*)m_scan->At(ientry))->histo_chi2;
  
  if(threshold == NULL && noise == NULL && chi2 == NULL)
    {
      std::cout << "INTIMETHRanalysis: WARNING no histograms; will not create any pixel map" << std::endl;
      return;
    }

  //for(Int_t FE=0; FE<16; FE++)	num_bad_pixels[FE]=0;
  for(Int_t FE=0; FE<_Nfe; FE++)	num_bad_pixels[FE]=0;
  module->numBadPixels=0;
  module->numDisconnectedPixels=0;
  //for(Int_t col=1; col<=helperNoise->Ncols; col++) {
  //for(Int_t row=1; row<=helperNoise->Nrows; row++) {
  for(Int_t col=1; col<=_Ncol; col++) {
    for(Int_t row=1; row<=_Nrow; row++) {
      const std::pair<Int_t, Int_t> index = std::make_pair(col-1, row-1);
      if((threshold->GetBinContent(col, row)==0 && noise->GetBinContent(col, row)==0 && chi2->GetBinContent(col, row)==0) || threshold->GetBinContent(col, row)<0)
	{
	  helperNoise->SetBit(module->badPixelMap[index], 13);	// 13 = "threshold not fitted"
	  
	  // Only count bad pixels on enabled FEs and enabled masks
	  if(!fe_disable[helperNoise->frontEnd(col-1, row-1)] && cp_enable[helperNoise->frontEnd(col-1,row-1)][helperNoise->columnPair(col-1,row-1)] && helperNoise->isMasked(col-1, row-1, ((AnalysisInputINTIMETHR*)m_scan->At(ientry))->nMaskStages))
	    {
	      module->numBadPixels++;
	      num_bad_pixels[helperNoise->frontEnd(col-1, row-1)]++;
	    }
      }
      if (noise->GetBinContent(col, row)>90 && noise->GetBinContent(col, row)<cut_noise_min[helperNoise->pixelClass(col-1,row-1,helperNoise->NpixelClasses)-1]) 
	{
	  module->numDisconnectedPixels++;
	  num_bad_pixels[helperNoise->frontEnd(col-1, row-1)]++;
	}
    }
  }
  std::cout << "INTIMETHRanalysis:    INFO no. of bad pixels in module: "<< module->numBadPixels << std::endl;
  std::cout << "INTIMETHRanalysis:    INFO no. of bad pixels in module: "<< module->numDisconnectedPixels << std::endl;
  std::cout << "INTIMETHRanalysis:    INFO findBadPixels() finished" << std::endl;
}


void INTIMETHRanalysisROOT::QualifyResults() 
{
  std::cout << "INTIMETHRanalysis:    INFO QualifyResults() starting" << std::endl;
  bool allPassed(true);
  Int_t failedModules(0);
  for(Int_t i=0; i<m_result->GetEntries(); i++)
    { 
      ResultINTIMETHR* module_result = (ResultINTIMETHR*)m_result->At(i); 
      if(!module_result->passed)
	{ 
	  if(allPassed) allPassed=false;
	  failedModules++;
	}
    }
  if(allPassed)
    {
      std::cout << "INTIMETHRanalysis:    INFO all modules have PASSED!" << std::endl;
      std::cout << "INTIMETHRanalysis:    INFO results qualified!" << std::endl;
      m_status=kPassed;
    }
  else 
    {
      std::cout << "INTIMETHRanalysis:    INFO " << m_result->GetEntries() - failedModules << " modules PASSED!" << std::endl;
      std::cout << "INTIMETHRanalysis:    INFO " << failedModules << " modules FAILED!" << std::endl;
      std::cout << "INTIMETHRanalysis:    INFO results not qualified!" << std::endl;
      m_status=kFailed;
    }
}

void INTIMETHRanalysisROOT::CheckForKnownProblems() //setting of failure bits
{
  for(Int_t i=0; i<m_result->GetEntries(); i++) { 
    ResultINTIMETHR* module_result = (ResultINTIMETHR*)m_result->At(i); 
    ///
    CheckForZeroMean(module_result);
  }
}


void INTIMETHRanalysisROOT::CheckForZeroMean(ResultINTIMETHR* module_result) //setting of failure bits, average_threshold has to be calculated in Calculate() before!
{
  if(module_result->average_threshold[0]==0) {
    helperThr->SetBit(module_result->tests,1); //test 1 failed
    module_result->passed=false;
  }
}

#endif

