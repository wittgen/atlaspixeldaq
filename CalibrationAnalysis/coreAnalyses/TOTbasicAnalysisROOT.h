#ifndef _TOTbasicAnalysisROOT_h_
#define _TOTbasicAnalysisROOT_h_ 

#include "ROOTanalysisBase.h"
#include "analysisInput_TOTbasic.h"
#include "analysisOutput_TOTbasic.h"
#include "TH2I.h"
#include "ROOTanalysisHelper.h"

class TOTbasicAnalysisROOT  : public ROOTanalysisBase
{
 public:
  TOTbasicAnalysisROOT();
  ~TOTbasicAnalysisROOT();


 private:
  void Initialize();

  void Calculate(); //internal calculations

  float AdditionalCalculations(int module); // more calculations for analog scans

  void QualifyResults(); //apply cuts from analysis config

  void CheckForKnownProblems(); //setting of failure bits

  void Print();

 public:
  Float_t expected_mean;
//  Float_t expected_sigma; [maria elena]
  Float_t expected_sigmaError;
  Float_t expected_meanError;
  Float_t expected_meanFail;
  Float_t expected_sigmaFail;
  Float_t max_failing_pixels;
  bool isAnalogScan;
  bool isFDACTuneScan;
  bool isIFTuneScan;

 private: //variables

  bool allModulesPassed;
  bool Passed;

  // Algorithm parameters
  // --------------------

  unsigned int _Nmasks;
  unsigned int _Nevents;

  unsigned int _Ncol;
  unsigned int _Nrow;
  unsigned int _Npixels;

  unsigned int _Nfe;
  unsigned int _Nmask_stage;
  unsigned int _ChipType;

  bool _digitalInjection;
  int  _scanType;
    
    std::vector<unsigned short int> _fe_disable;
    std::vector<std::vector<unsigned short int> > _cp_enable;

};


#endif

