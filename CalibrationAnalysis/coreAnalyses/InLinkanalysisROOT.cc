#ifndef _InLinkanalysisROOT_CC_
#define _InLinkanalysisROOT_CC_

#include "InLinkanalysisROOT.h"
#include <iostream>
#include "TMath.h"

InLinkanalysisROOT::InLinkanalysisROOT()
{
  std::cout << "InLinkanalysisROOT:  constructed." << std::endl;
} 


InLinkanalysisROOT::~InLinkanalysisROOT()
{
  //WriteResults();
}

void InLinkanalysisROOT::WriteResults()
{
  TFile f("resultsInLink.root","update");
  f.WriteTObject(m_result.get(),"scanHistos","SingleKey"); //as one array
  f.Close();  
}

void InLinkanalysisROOT::Initialize()
{
  //! Don't hard code here. Take values from analysis configuration, defaults in AutomaticAnalysis.

  if(cut_I_pin_mod_min<=0.0){
    std::cout << "WARNING   Apparent problem in reading analysis config, all cuts are zero.  -> Setting default values." << std::endl;
    cut_I_pin_mod_min=450; //on the ratio of bits
  }
}  

void InLinkanalysisROOT::Calculate() 
{
  std::cout << "InLinkanalysisROOT::Calculate  starting *********************************" << std::endl;
  m_scan->R__FOR_EACH(AnalysisInputInLink,PrintInfo)(); 


  // Loop over modules
  for(Int_t i=0; i<m_scan->GetEntries(); i++) {
    ResultInLink* module_result = new ResultInLink();
    AnalysisInputInLink* module_scan = (AnalysisInputInLink*) m_scan->At(i);

    module_result->rodName = module_scan->rodName;
    module_result->pp0Name = module_scan->pp0Name;
    module_result->moduleName = module_scan->moduleName;
    //module_result->moduleId   = module_scan->moduleId;

    const TH2* h_link =  module_scan->histo_InLink;
    if(!h_link || (h_link && h_link->GetEntries()==0)){
      std::cerr << "ERROR InLinkanalysisROOT: WARNING  no histos for module " << module_result->moduleName << " -> skipping." << std::endl;
      module_result->passed=false;
      m_result->Add( module_result );
      continue;
    }
 
    // all 'passed' result variables are initialized as 'false' in analysisOutput_LINK.h

    //get I_pin_mod
    //convert boxLinkTxID to InLinkScanTxID: bocLinkTxID = ob*10+ch but InLink scan TxID= ob*8+ch
    int inX = (module_scan->bocLinkTx/10)*8+(module_scan->bocLinkTx%10)+1; 
    int inY = 3;
    
    double I_pin_mod= h_link->GetBinContent(inX, inY);
    module_result->I_pin_mod = I_pin_mod;
    module_result->passed=true;
    if (I_pin_mod <  cut_I_pin_mod_min || I_pin_mod > cut_I_pin_mod_max) {
        module_result->passed=false;
    }
    /*
    std::cout << ">  module " << module_result->moduleName  
         << " modID " <<  module_scan->moduleId
         << " inLink " << module_scan->inLink
         << " ouLink " << module_scan->outLink1A  
         << " I_pin_mod " << module_result->I_pin_mod 
         << " status " << ( (module_result->passed)?  "OK":"FAILED" ); 
    for(int jj=1;jj< 7; jj++) std::cout <<" "<< h_link->GetBinContent(inX,jj) ; 
    std::cout << std::endl;
    */
    m_result->Add( module_result );
  }// loop each modules


  std::cout << "  after calculate  results size: " << m_result->GetEntries() << std::endl;


  //AdditionalCalculations();

  std::cout << "InLinkanalysisROOT: analysis finished." << std::endl;
}

void InLinkanalysisROOT::QualifyResults() 
{
  std::cout << "InLinkanalysisROOT::QualifyResults: Number of Modules: " << m_result->GetEntries() <<std::endl;

  bool allPassed(true);
  for(Int_t i=0; i<m_result->GetEntries(); i++) { 
    ResultInLink* module_result = (ResultInLink*)m_result->At(i); 
    if(!module_result->passed) { 
      allPassed=false;
      break;
    }
    std::cout <<"InLinkanalysis::QualifyResults " << module_result->moduleName <<" "<<module_result->passed <<" allPassed= "<< allPassed <<std::endl; 
  }  
  if(allPassed)
    m_status=kPassed;
  else 
    m_status=kFailed;

  std::cout << "InLinkanalysisROOT::QualifyResults: final allPassed: " << allPassed <<" status: "<< m_status <<std::endl;
  std::cout << "InLinkanalysisROOT::QualifyResults: Total result is :" << ( (allPassed)? "OK":"FAILED" ) << std::endl;
  std::cout << "*************************************" << std::endl;
}


void InLinkanalysisROOT::CheckForKnownProblems() //setting of failure bits
{

  //for(Int_t i=0; i<m_result->GetEntries(); i++) { 
  //  ResultInLink* module_result = (ResultInLink*)m_result->At(i); 
  //}
}
#endif
