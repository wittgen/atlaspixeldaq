#ifndef _DIFFanalysisROOT_h_
#define _DIFFanalysisROOT_h_ _

#include "ROOTanalysisBase.h"
#include "analysisInput_DIFF.h"
#include "analysisOutput_DIFF.h"
#include <iostream>
#include <TFile.h>
#include <TString.h>


#include "../Lock.hh"
#include "../Flag.hh"




class DIFFanalysisROOT : public ROOTanalysisBase 
{
 public:
  DIFFanalysisROOT();
  ~DIFFanalysisROOT();
    
  void Initialize();

  void Calculate(); //internal calculations

  void QualifyResults(); //apply cuts from analysis config

  void CheckForKnownProblems() {};

 private:
  void WriteResults();
  float getDeviation(TH1* h1, TH1* h2);
  std::unique_ptr<TH1F> RobustRebin(std::shared_ptr<TH1F> valuesHisto ,std::shared_ptr<TH1F> binningHisto);
  std::unique_ptr<TH2F> RobustRebin(std::shared_ptr<TH2F> valuesHisto ,std::shared_ptr<TH2F> binningHisto);

};
#endif
  
