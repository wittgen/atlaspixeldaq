#ifndef _BONDanalysisROOT_CC_
#define _BONDanalysisROOT_CC_

#include "BONDanalysisROOT.h"
#include <iostream>
#include "TMath.h"
#include "PixFe/PixGeometry.h"

BONDanalysisROOT::BONDanalysisROOT()
{
  std::cout << "BONDanalysis:  constructed." << std::endl;
} 


void BONDanalysisROOT::WriteResults()
{
  TFile f("resultsBOND.root","update");
  f.WriteTObject(m_result.get(),"scanHistos","SingleKey"); //as one array
  f.Close();  
}

void BONDanalysisROOT::Initialize()
{
  //! Don't hard code here. Take values from analysis configuration, defaults in AutomaticAnalysis.

  if(cut_delta_min==0) {
    std::cout << "WARNING   Apparent problem in reading analysis config, all cuts are zero.  -> Setting default values." << std::endl;
    cut_delta_min=50; //minimal cut on the deltas of noise for HVon/HVoff 
  }
}  

void BONDanalysisROOT::Calculate() 
{
  std::cout << "**************************************" << std::endl;
  std::cout << "BONDanalysis::Calculate  starting" << std::endl;
  m_scan->R__FOR_EACH(AnalysisInputBOND,PrintInfo)(); 
  std::cout << "BONDanalysis: These were the module names." << std::endl;  

  // Loop over modules
  for(Int_t i=0; i<m_scan->GetEntries(); i++) { 
    ResultBOND* module_result = new ResultBOND();
    module_result->moduleName = ((AnalysisInputBOND*)m_scan->At(i))->moduleName;

      PixGeometry geom(PixA::nRows(((AnalysisInputBOND*)m_scan->At(i))->histo_sigma_HVoff),PixA::nColumns(((AnalysisInputBOND*)m_scan->At(i))->histo_sigma_HVoff));
      chipType = geom.pixType();
      Nrow = helper.getNrow(chipType);
      Ncol = helper.getNcol(chipType);
      FE_disable = ((AnalysisInputBOND*)m_scan->At(i))->fe_disable;
      _cp_enable = ((AnalysisInputBOND*)m_scan->At(i))->cp_enable;
      _Nmasks=((AnalysisInputBOND*)m_scan->At(i))->zaehler;
      _Nmask_stage=((AnalysisInputBOND*)m_scan->At(i))->nenner;

      
    if(((AnalysisInputBOND*)m_scan->At(i))->histo_sigma_HVon->GetEntries()==0 || ((AnalysisInputBOND*)m_scan->At(i))->histo_sigma_HVoff->GetEntries()==0) {
      std::cout << "BONDanalysis: WARNING  Not both noise histograms for module " << module_result->moduleName << " present  -> skipping." << std::endl;
      module_result->passed=false;
      m_result->Add( module_result );
      continue;
    }
    std::cout << "Analysing module: " << module_result->moduleName << std::endl; 
    module_result->passed=true;

    // Loop over pixel types
    //    for (int pixtype=0; pixtype<helper.NpixelTypes; pixtype++) {
    // std::cout << "Now looking at pixel type: " << helper.getPixelTypeString(pixtype) << std::endl;

    std::shared_ptr<TH1F> histo_delta = CheckDeltas( ((AnalysisInputBOND*)m_scan->At(i))->histo_sigma_HVoff, ((AnalysisInputBOND*)m_scan->At(i))->histo_sigma_HVon, module_result );

    helper.Dump(histo_delta.get());

    module_result->hInt_delta = *histo_delta.get();
    
    std::cout << "cutting on delta_noise_min: ";
    if( !helper.FulfillMinCut( histo_delta.get(), cut_delta_min, 0.99) ) //99% should have delta_noise > cut_delta_min
      module_result->passed=false; 
    
    //} //endl loop over pixel types

    std::cout << "Result for module " << module_result->moduleName << " is: " << ( (module_result->passed)?  "OK":"FAILED" ) << std::endl;
    m_result->Add( module_result );
  }
  std::cout << "BONDanalysis: analysis finished." << std::endl;
}


std::shared_ptr<TH1F> BONDanalysisROOT::CheckDeltas(const TH2* HVoff, const TH2* HVon, ResultBOND* module) 
{
  std::shared_ptr<TH2F> badPixelmap(new TH2F("badpixelmap"+module->moduleName,"badpixelmap",Ncol,0,Ncol,Nrow,0,Nrow));
  module->badPixels.clear();

  TString name(HVon->GetTitle());
  name+=HVoff->GetTitle();
  //name+=pixtype;
  std::shared_ptr<TH1F> deltas(new TH1F(name,name,10000,0,10000)); 

  assert((HVoff->GetNbinsX()-HVon->GetNbinsX())==0);
  assert((HVoff->GetNbinsY()-HVon->GetNbinsY())==0);

  for(int x=0; x<=HVon->GetNbinsX(); x++) {
    for(int y=0; y<=HVon->GetNbinsY(); y++) {

        if (!helper.isMasked(x,y,_Nmasks,_Nmask_stage,chipType)){continue;}
                
        if (FE_disable[helper.frontEnd(x,y,chipType)] == 1)
            continue;
        
        if (!_cp_enable[helper.frontEnd(x,y,chipType)][helper.columnPair(x,y,chipType)]) continue;

        
        
      if(HVoff->GetBinContent(x,y)!=0 && HVon->GetBinContent(x,y)!=0) {
	float delta( HVoff->GetBinContent(x,y) -  HVon->GetBinContent(x,y) );
	deltas->Fill( delta );

	if(delta < cut_delta_min) {
	  badPixelmap->Fill( x,y, 1 );
	  module->badPixels[std::make_pair(x,y)]=0; //status 0 = 
	}
	else
	  badPixelmap->Fill( x,y, 0 );
      }
    }
  }
  module->badPixelmap = *badPixelmap.get(); //make a copy
  module->numBadPixels = module->badPixels.size();

  return deltas;
}

void BONDanalysisROOT::QualifyResults() 
{
  bool allPassed(true);
  for(Int_t i=0; i<m_result->GetEntries(); i++) { 
    ResultBOND* module_result = (ResultBOND*)m_result->At(i); 
    if(!module_result->passed) { 
      allPassed=false;
      break;
    }
  }  
  if(allPassed)
    m_status=kPassed;
  else 
    m_status=kFailed;

  std::cout << "BONDanalysis::QualifyResults  Total result is :" << ( (allPassed)? "OK":"FAILED" ) << std::endl;
  std::cout << "*************************************" << std::endl;
}


#endif

