#ifndef _T0analysisROOT_h_
#define _T0analysisROOT_h_ _

#include "ROOTanalysisBase.h"
#include "analysisInput_T0.h"
#include "analysisOutput_T0.h"
#include <iostream>
#include <TFile.h>
#include <TString.h>



class T0analysisROOT : public ROOTanalysisBase 
{
 public:
  T0analysisROOT();
  ~T0analysisROOT();
    
  void Initialize();

  void Calculate(); //internal calculations

  void QualifyResults(); //apply cuts from analysis config

  void CheckForKnownProblems() {};


  //The cuts are directly accessible from the AutomaticAnalysis
  Float_t delay_offset;//ns
  Float_t pixel_occ;
  Float_t rising_edge_occ;
  Float_t falling_edge_occ;
  Float_t T0_occ;
  Int_t min_del_diff;

 private:
  void WriteResults();

  int _Ncol;
  int _Nrow;
  int _NTotmasks;
  int _Nmasks;
  int _Nevents;
  int _Nmasksdone;
  
  int _ChipFeFlav;
};


#endif

