#ifndef _INTIMETHRanalysisROOT_h_
#define _INTIMETHRanalysisROOT_h_ _

#include "ROOTanalysisHelper.h"
#include "ROOTanalysisBase.h"
#include "analysisInput_INTIMETHR.h"
#include "analysisOutput_INTIMETHR.h"
#include <iostream>
#include <TFile.h>
#include <TString.h>


class INTIMETHRanalysisROOT : public ROOTanalysisBase 
{
public:
	INTIMETHRanalysisROOT();
	~INTIMETHRanalysisROOT();

	void Initialize();

	void Calculate(); // internal calculations

	void QualifyResults(); // apply cuts from analysis config

	void CheckForKnownProblems(); // setting of failure bits

	// The cuts are directly accessible from the AutomaticAnalysis
	Float_t	cut_noise_min[4];
	Float_t	cut_noise_max[4];
	Int_t	cut_noise_min_percent[4];
	Int_t	cut_noise_max_percent[4];
	Float_t	cut_thr_min[3];
	Float_t	cut_thr_max[3];
	Int_t	cut_thr_min_percent[3];
	Int_t	cut_thr_max_percent[3];
	Float_t	cut_thr_mean_min[3]; 
	Float_t	cut_thr_mean_max[3];
	Float_t	cut_thr_RMS_max[3];
	Int_t	range_thr_mean_min;
	Int_t	range_thr_mean_max;
	Int_t	cut_num_bad_pixels_per_FE;

	bool create_Pixelmap;

	ROOTanalysisHelper* helperNoise;	// helper uses 4 pixel classes
	ROOTanalysisHelper* helperThr;	// helper uses 3 pixel classes
	// The object "helper" inherited from the parent class will not be used by these children!

private:
	void WriteResults();
	void AdditionalCalculations();

	// Tests
	void CheckForZeroMean(ResultINTIMETHR* module_result);
	void findBadPixels(ResultINTIMETHR* module, Int_t ientry, std::vector<unsigned short int> FE_disable, std::vector<std::vector<unsigned short int> > CP_enable);

	std::vector<unsigned short int> FE_disable;
	std::vector<std::vector<unsigned short int> > CP_enable;
	Int_t	num_bad_pixels[16];

	int _Ncol;
	int _Nrow;
	int _Nfe;
	int _ChipType;
};

#endif

