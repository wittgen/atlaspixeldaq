#ifndef _analysisInputBASICTHR_h_
#define _analysisInputBASICTHR_h_

#include "analysisInput_Base.h"

class AnalysisInputBASICTHR : public AnalysisInput { //this is for one scan and one module
 public:
  AnalysisInputBASICTHR() :AnalysisInput(), histo_chi2(NULL), histo_threshold(NULL), histo_noise(NULL) {};
  ~AnalysisInputBASICTHR() {};
  const TH2* histo_chi2;  // pixel maps
  const TH2* histo_threshold;  
  const TH2* histo_noise;  

  Int_t nPixel_abs_frac, nMaskStages, nTOTMaskStages;  //depending on mask steps
  TString moduleName; 
  TString pp0Name;

  //Helper functions
  void PrintInfo() {
    std::cout << "scanHistoBASICTHR for module: " << moduleName.Data() << "  HistoEntries:" << entriesSafe(histo_chi2) << " " << entriesSafe(histo_threshold) << " " << entriesSafe(histo_noise) << std::endl;
  };

};

#endif
