#ifndef _analysisInputOPTOLINK_h_
#define _analysisInputOPTOLINK_h_

#include "analysisInput_Base.h"
#include <vector>
#include <TH2F.h>
#include <TString.h>
#include <iostream>

class AnalysisInputOPTOLINK : public AnalysisInput { //this is for one scan and one module
 public:
  AnalysisInputOPTOLINK() :AnalysisInput(), MBs(-9) {};
  ~AnalysisInputOPTOLINK() {};

  std::vector<const TH2*> histo_Link1; //several histos for scan over Viset 
  std::vector<const TH2*> histo_Link2; //several histos for scan over Viset
  std::vector<histoAxis_t> axis; //the axis of the histo (the same for all histos) of type histoAxis_t 

  int MBs; //40,80,160 Mbits/s
  int Maxcount;
  float thr_Link1, thr_Link2, del_Link1, del_Link2;
  float viset_Link1, viset_Link2;
  int m_serialNumber;
  int _ChipFeFlav;


  //Helper functions
  void PrintInfo() {
    std::cout << "AnalysisInputOPTOLINK No. " << m_serialNumber << " for pp0 " << pp0Name.Data() << ", module: " << moduleName.Data() << "  MBit/s=" << MBs << "  HistoEntries:" << std::endl;
    
    std::cout << "vector Link1 " << histo_Link1.size() << std::endl;
    std::cout << "vector Link2 " << histo_Link2.size() << std::endl;
    std::cout << "vector axis " <<  axis.size() << std::endl;

    std::vector<const TH2*>::const_iterator iter = histo_Link1.begin();
    for(;iter!=histo_Link1.end(); iter++) {
      //std::cout << "histos link1 " << iter->GetEntries() << std::endl;
    }

    for(unsigned int i=0; i<axis.size(); i++)
      std::cout << "axis["<<i<<"].nSteps " <<  axis[i].nSteps << std::endl;

  };

  //ClassDef(AnalysisInputOPTOLINK,1) 
};

#endif
