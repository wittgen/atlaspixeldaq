#ifndef _DIFFanalysisROOT_CC_
#define _DIFFanalysisROOT_CC_

#include "DIFFanalysisROOT.h"
#include <iostream>
#include "TMath.h"
#include "TCanvas.h"
#include "../Lock.hh"
#include "../PixDbGlobalMutex.hh"


DIFFanalysisROOT::DIFFanalysisROOT()
{
  std::cout << "DIFFanalysis:  constructed." << std::endl;
} 


DIFFanalysisROOT::~DIFFanalysisROOT()
{
  //WriteResults();
}

void DIFFanalysisROOT::WriteResults()
{
  TFile f("resultsDIFF.root","update");
  f.WriteTObject(m_result.get(),"scanHistos","SingleKey"); //as one array
  f.Close();  
}

void DIFFanalysisROOT::Initialize()
{
}  

void DIFFanalysisROOT::Calculate() 
{
  //locking this mutex prevents ROOT from colliding with itself in the Histogram
  // Recievers et al. 
  CAN::Lock lock( CAN::PixDbGlobalMutex::mutex() );
  std::cout << "**************************************" << std::endl;
  std::cout << "DIFFanalysis::Calculate  starting" << std::endl;
  m_scan->R__FOR_EACH(AnalysisInputDIFF,PrintInfo)(); 
  std::cout << "DIFFanalysis: These were the module names." << std::endl;  

  // Loop over modules
  for(Int_t i=0; i<m_scan->GetEntries(); i++) { 
    ResultDIFF* module_result = new ResultDIFF();
    module_result->pp0Name = ((AnalysisInputDIFF*)m_scan->At(i))->pp0Name;
    module_result->moduleName = ((AnalysisInputDIFF*)m_scan->At(i))->moduleName;
    
    std::map< std::string, float > hs_dev;
  
    std::map< std::string, std::pair<std::shared_ptr<TH1F>,std::shared_ptr<TH1F> > >::iterator histos_1d_iter;
    std::map< std::string, std::shared_ptr<TH1F> > hs_1d_diff;
    for (histos_1d_iter  = ((AnalysisInputDIFF*)m_scan->At(i))->histos_1d.begin();
	 histos_1d_iter != ((AnalysisInputDIFF*)m_scan->At(i))->histos_1d.end();
	 histos_1d_iter ++){

      std::shared_ptr<TH1F> rel_diff_1d(new TH1F((*histos_1d_iter->second.first)));
      std::shared_ptr<TH1F> abs_diff_1d(new TH1F((*histos_1d_iter->second.first)));
      std::shared_ptr<TH1F> temp_histo_1d(new TH1F((*histos_1d_iter->second.first)));
      std::unique_ptr<TH1F> h1a;
      std::unique_ptr<TH1F> h1b;
      float h1_dev;
       
      if ((*histos_1d_iter->second.first).GetNbinsX() == (*histos_1d_iter->second.second).GetNbinsX()
	  && (*histos_1d_iter->second.first).GetXaxis()->GetBinCenter(1) ==  (*histos_1d_iter->second.second).GetXaxis()->GetBinCenter(1)
	  && (*histos_1d_iter->second.first).GetXaxis()->GetBinWidth(1)  ==  (*histos_1d_iter->second.second).GetXaxis()->GetBinWidth(1)){
	h1a.reset(new TH1F(*histos_1d_iter->second.first));
	h1b.reset(new TH1F(*histos_1d_iter->second.second));
      }
      else{
	std::cout<<"Be careful!! Incorrect # of bins in these histos, using a smart rebin algorithm"<<std::endl;
	h1a.reset(new TH1F(*histos_1d_iter->second.first));
	h1b = RobustRebin(histos_1d_iter->second.second,histos_1d_iter->second.first);
      }
      
      h1_dev = getDeviation(h1a.get(),h1b.get());
      hs_dev[histos_1d_iter->first + "_dev"] = h1_dev;     
      
      abs_diff_1d->Add(h1a.get(), h1b.get(), 1, -1 );
      abs_diff_1d->SetEntries(1);
      hs_1d_diff[histos_1d_iter->first + "_abs"] = abs_diff_1d;

      rel_diff_1d->Add(h1a.get(), h1b.get(), 1, -1 );
      temp_histo_1d->Add(h1a.get(),h1b.get());
      rel_diff_1d->Divide((&(*temp_histo_1d)));
      rel_diff_1d->SetEntries(1);
      hs_1d_diff[histos_1d_iter->first + "_rel"] = rel_diff_1d;

    }

    std::map< std::string, std::pair<std::shared_ptr<TH2F>,std::shared_ptr<TH2F> > >::iterator histos_2d_iter;
    std::map< std::string, std::shared_ptr<TH2F> > hs_2d_diff;
    for (histos_2d_iter  = ((AnalysisInputDIFF*)m_scan->At(i))->histos_2d.begin();
	 histos_2d_iter != ((AnalysisInputDIFF*)m_scan->At(i))->histos_2d.end();
	 histos_2d_iter ++){
      
      std::shared_ptr<TH2F> rel_diff_2d(new TH2F((*histos_2d_iter->second.first)));
      std::shared_ptr<TH2F> abs_diff_2d(new TH2F((*histos_2d_iter->second.first)));
      std::shared_ptr<TH2F> temp_histo_2d(new TH2F((*histos_2d_iter->second.first)));
      std::unique_ptr<TH2F> h2a;
      std::unique_ptr<TH2F> h2b;
      float h2_dev;

      if ((*histos_2d_iter->second.first).GetNbinsX() == (*histos_2d_iter->second.second).GetNbinsX()
	  && (*histos_2d_iter->second.first).GetXaxis()->GetBinCenter(1) ==  (*histos_2d_iter->second.second).GetXaxis()->GetBinCenter(1)
	  && (*histos_2d_iter->second.first).GetXaxis()->GetBinWidth(1)  ==  (*histos_2d_iter->second.second).GetXaxis()->GetBinWidth(1)
	  && (*histos_2d_iter->second.first).GetYaxis()->GetBinCenter(1) ==  (*histos_2d_iter->second.second).GetYaxis()->GetBinCenter(1)
	  && (*histos_2d_iter->second.first).GetYaxis()->GetBinWidth(1)  ==  (*histos_2d_iter->second.second).GetYaxis()->GetBinWidth(1)){
	h2a.reset(new TH2F(*histos_2d_iter->second.first));
	h2b.reset(new TH2F(*histos_2d_iter->second.second));
      }
      else{
	std::cout<<"Be careful!! Incorrect # of bins in these histos, using a homegrown rebin algorithm"<<std::endl;
	h2a.reset(new TH2F(*histos_2d_iter->second.first));
	h2b = RobustRebin(histos_2d_iter->second.second,histos_2d_iter->second.first);
      }
      
      h2_dev = getDeviation(h2a.get(),h2b.get());
      hs_dev[histos_2d_iter->first + "_dev"] = h2_dev;     

      abs_diff_2d->Add(h2a.get(), h2b.get(), 1, -1 );
      abs_diff_2d->SetEntries(1);
      hs_2d_diff[histos_2d_iter->first + "_abs"] = abs_diff_2d;

      rel_diff_2d->Add(h2a.get(), h2b.get(), 1, -1 );
      temp_histo_2d->Add(h2a.get(),h2b.get());
      rel_diff_2d->Divide((&(*temp_histo_2d)));
      rel_diff_2d->SetEntries(1);
      std::string histo_name = histos_2d_iter->first + "_rel";
      hs_2d_diff[histos_2d_iter->first + "_rel"] = rel_diff_2d;
    }

    if (hs_dev.size() != 0){
      module_result->histos_dev_out = hs_dev;
    }
    
    if (hs_1d_diff.size() != 0){
      module_result->histos_1d_out = hs_1d_diff;
    }
    if (hs_2d_diff.size() != 0){
      module_result->histos_2d_out = hs_2d_diff;
    }

  m_result->Add( module_result );
  }
}
        


void DIFFanalysisROOT::QualifyResults() 
{
  std::cout << "*************************************" << std::endl;
}

float DIFFanalysisROOT::getDeviation(TH1* h1, TH1* h2)
{
  if (h1->GetNbinsX() != h2->GetNbinsX() ||
      h1->GetNbinsY() != h2->GetNbinsY() ||
      h1->GetNbinsZ() != h2->GetNbinsZ() )
    {return -1;}
  int nBins = h1->GetNbinsX() * h1->GetNbinsY() * h1->GetNbinsZ();
  double sumVar = 0;
  float stddev;
  for(int ix = 1; ix <= h1->GetNbinsX(); ix++){
    for(int iy = 1; iy <= h1->GetNbinsY(); iy++){    
      for(int iz = 1; iz <= h1->GetNbinsZ(); iz++){
	float diff = (h1->GetBinContent(ix,iy,iz)-h2->GetBinContent(ix,iy,iz));
	sumVar =sumVar + diff*diff;
      }}}
  stddev = sqrt(sumVar/nBins);
  return stddev;
}
			  
	
std::unique_ptr<TH1F> DIFFanalysisROOT::RobustRebin(std::shared_ptr<TH1F> valuesHisto ,std::shared_ptr<TH1F> binningHisto)
{
  //rebins valuesHisto into binningHisto so they can be added... should be used with caution
  //set the left edge and right edge of the values bin. Remember the true end on the right
  //see if any of these overlap with the overflow/underflow.  If so, increment the overflow/underflow bins
  // and reset the edges so that they don't touch overflow and underflow
  //start at the left edge
  //see if the right edge is in the same binningBin
  //if it isn't, set the right edge to be the binningBin edge
  //increment the bin by the size of the valuesBin*fraction of the valuesBin that is in that binningBin
  //set the left edge to the right edge of the current binningBin (the current right edge)
  //set the right edge back to the true right end
  //increment the binningBin
  //end when the right edge is inside the bin

  std::unique_ptr<TH1F>outputHisto(new TH1F(*binningHisto));
  outputHisto->Reset("ICE");

  int binningBin, valuesBin;
  float valuesValue, valuesCenter, valuesMargin;
  //carry the over and underflow bins over
  outputHisto->AddBinContent(0,valuesHisto->GetBinContent(0));
  outputHisto->AddBinContent(outputHisto->GetNbinsX(),valuesHisto->GetBinContent(valuesHisto->GetNbinsX()));
  for (valuesBin = 1 ; valuesBin <= valuesHisto->GetNbinsX(); valuesBin++){
    //I have the bin number of the values histo
    valuesCenter = valuesHisto->GetBinCenter(valuesBin);
    valuesMargin = valuesHisto->GetBinWidth(valuesBin)/2;
    valuesValue = valuesHisto->GetBinContent(valuesBin);
    int overflowBin = outputHisto->GetNbinsX()+1;
    
    float leftEdge, rightEdge, rightEnd;
    leftEdge = valuesCenter - valuesMargin;
    rightEnd = valuesCenter + valuesMargin;
    rightEdge=rightEnd;
    if (leftEdge < binningHisto->GetBinCenter(1) - binningHisto->GetBinWidth(1)/2){
      outputHisto->AddBinContent(0,((binningHisto->GetBinCenter(1) - binningHisto->GetBinWidth(1)/2) - (valuesCenter - valuesMargin))/(2*valuesMargin));
      leftEdge = binningHisto->GetBinCenter(1) - binningHisto->GetBinWidth(1)/2;
    }
    if (rightEdge > binningHisto->GetBinCenter(overflowBin-1) + binningHisto->GetBinWidth(overflowBin-1)/2){
      outputHisto->AddBinContent(overflowBin,((valuesCenter+valuesMargin)-(binningHisto->GetBinCenter(overflowBin-1)+binningHisto->GetBinWidth(overflowBin-1)/2))/(2*valuesMargin));
      rightEdge =  binningHisto->GetBinCenter(overflowBin-1) + binningHisto->GetBinWidth(overflowBin-1)/2;
      rightEnd = rightEdge;
    }
    bool endLoop = false;
    binningBin = binningHisto->GetXaxis()->FindFixBin(leftEdge);
    while (!endLoop){
      if ( rightEdge > outputHisto->GetBinCenter(binningBin) + outputHisto->GetBinWidth(binningBin)/2)
	rightEdge = outputHisto->GetBinCenter(binningBin) + outputHisto->GetBinWidth(binningBin)/2;
      else
	endLoop = true;
      outputHisto->AddBinContent(binningBin,valuesValue*(rightEdge-leftEdge)/(2*valuesMargin));
      leftEdge=rightEdge;
      rightEdge = rightEnd;
      binningBin++;
    }
  }				 	  
    return outputHisto;
}


std::unique_ptr<TH2F> DIFFanalysisROOT::RobustRebin(std::shared_ptr<TH2F> valuesHisto ,std::shared_ptr<TH2F> binningHisto){

  //this does not handle overflow and underflow at all
  std::unique_ptr<TH2F> outputHisto(new TH2F(*binningHisto));
  outputHisto->Reset("ICE");

  int bbX, bbY, vbX, vbY;
  float valuesValue, vcX, vcY, vmX,vmY;

  int overflowBinX = outputHisto->GetNbinsX()+1;
  int overflowBinY = outputHisto->GetNbinsY()+1;

  for (vbX = 1 ; vbX <= valuesHisto->GetNbinsX(); vbX++){
    for (vbY = 1 ; vbY <=valuesHisto->GetNbinsY(); vbY++){
      
      //I have the bin number of the values histo
      vcX = valuesHisto->GetXaxis()->GetBinCenter(vbX);
      vcY = valuesHisto->GetYaxis()->GetBinCenter(vbY);
      vmX = valuesHisto->GetXaxis()->GetBinWidth(vbX)/2;
      vmY = valuesHisto->GetYaxis()->GetBinWidth(vbY)/2;
      valuesValue = valuesHisto->GetBinContent(vbX,vbY);

      float leftEdge, rightEdge, bottomEdge, topEdge, rightEnd, bottomEnd, topEnd;
      leftEdge = vcX - vmX;
      rightEdge = vcX + vmX;
      bottomEdge = vcY - vmY;
      topEdge = vcY + vmY;
      rightEnd = rightEdge;
      bottomEnd = bottomEdge;
      topEnd = topEdge;

      if (leftEdge < binningHisto->GetXaxis()->GetBinCenter(1) - binningHisto->GetXaxis()->GetBinWidth(1)/2){
	leftEdge = binningHisto->GetXaxis()->GetBinCenter(1) - binningHisto->GetXaxis()->GetBinWidth(1)/2;
      }
      if (bottomEdge < binningHisto->GetYaxis()->GetBinCenter(1) - binningHisto->GetYaxis()->GetBinWidth(1)/2){
	bottomEdge = binningHisto->GetYaxis()->GetBinCenter(1) - binningHisto->GetYaxis()->GetBinWidth(1)/2;
	bottomEnd = bottomEdge;
      }
      if (rightEdge > binningHisto->GetXaxis()->GetBinCenter(overflowBinX-1) + binningHisto->GetXaxis()->GetBinWidth(overflowBinX-1)/2){
	rightEdge = binningHisto->GetXaxis()->GetBinCenter(overflowBinX-1) + binningHisto->GetXaxis()->GetBinWidth(overflowBinX-1)/2;
	rightEnd = rightEdge;
      }
      if (topEdge > binningHisto->GetYaxis()->GetBinCenter(overflowBinY-1) + binningHisto->GetYaxis()->GetBinWidth(overflowBinY-1)/2){
	topEdge = binningHisto->GetYaxis()->GetBinCenter(overflowBinY-1) + binningHisto->GetYaxis()->GetBinWidth(overflowBinY-1)/2;
	topEnd = topEdge;
      }
      
      bool endLoopX = false;
      bbX = binningHisto->GetXaxis()->FindFixBin(leftEdge);
      while (!endLoopX){
	if (rightEdge > outputHisto->GetXaxis()->GetBinCenter(bbX) + outputHisto->GetXaxis()->GetBinWidth(bbX)/2)
	  rightEdge = outputHisto->GetXaxis()->GetBinCenter(bbX) + outputHisto->GetXaxis()->GetBinWidth(bbX)/2;
	else
	  endLoopX = true;
	bool endLoopY=false;
	bbY = binningHisto->GetYaxis()->FindFixBin(bottomEdge);
	while (!endLoopY){
	  if (topEdge > outputHisto->GetYaxis()->GetBinCenter(bbY) + outputHisto->GetYaxis()->GetBinWidth(bbY)/2)
	    topEdge = outputHisto->GetYaxis()->GetBinCenter(bbY) + outputHisto->GetYaxis()->GetBinWidth(bbY)/2;
	  else
	    endLoopY = true;
	  int gbin = outputHisto->GetBin(bbX,bbY);
	  outputHisto->AddBinContent(gbin,valuesValue*(rightEdge-leftEdge)/(2*vmX)*(topEdge-bottomEdge)/(2*vmY));
	  	  
	  bottomEdge=topEdge;
	  topEdge=topEnd;
	  bbY++;
	}
	bottomEdge=bottomEnd;
	leftEdge=rightEdge;
	rightEdge=rightEnd;
	bbX++;
      }
    }
  }
  return outputHisto;

}

#endif

