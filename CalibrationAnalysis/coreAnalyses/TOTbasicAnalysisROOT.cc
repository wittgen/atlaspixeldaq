#include "TOTbasicAnalysisROOT.h"
#include <iostream>
#include <fstream>
#include <cmath>
#include "TFile.h"
//#include "TCanvas.h"



TOTbasicAnalysisROOT::TOTbasicAnalysisROOT(): _Nmasks(0), _Nevents(0), _Ncol(0), _Nrow(0), _Nfe(0), _Nmask_stage(0), _ChipType(PixGeometry::INVALID_MODULE)
{
  std::cout << std::endl;
  std::cout << "-------------------------------" << std::endl;
  std::cout << "   TOTbasic Analysis Algorithm   " << std::endl;
  std::cout << "-------------------------------" << std::endl;
  std::cout << std::endl;
}


TOTbasicAnalysisROOT::~TOTbasicAnalysisROOT()
{
  //
}


void TOTbasicAnalysisROOT::Initialize()
{
  std::cout << "aaaa1" << std::endl;
  if(!m_scan) {
    std::cout << std::endl;
    std::cout << "TOTbasicAnalysisROOT::Initialize ERROR: input data pointer is null! Bailing out..." << std::endl;
    std::cout << std::endl;
    return;
  }

//  std::cout << "TOTbasicAnalysisROOT::Initialize making input" << std::endl;
  AnalysisInputTOTbasic* basic = (AnalysisInputTOTbasic*) m_scan->At(0);
  if(!basic){
    std::cout << "TOTbasicAnalysisROOT::Initialize ERROR: input data are null! Bailing out..." << std::endl;
    return;
  } 
  if (!basic->histoMean){
    std::cout << "TOTbasicAnalysisROOT::Initialize ERROR: input histograms are null! Bailing out..." << std::endl;
    return;
  }

  PixGeometry geo(PixA::nRows(basic->histoMean),PixA::nColumns(basic->histoMean)); 
  _ChipType = geo.pixType();

  _Nrow = basic->histoMean->GetNbinsY();
  _Ncol = basic->histoMean->GetNbinsX();
  _Nmasks  = basic->Nmasks;
  _Nevents = basic->Nevents;
  _digitalInjection = basic->digitalInjection;
  _scanType = basic->scanType;
  _Nfe=helper.getNfrontEnd(_ChipType); // # of FEs in one module  

//*******************************
  /*if(_ChipType == PixGeometry::FEI2_CHIP || _ChipType == PixGeometry::FEI2_MODULE){
    _Nmask_stage = 32;
  }else if(_ChipType == PixGeometry::FEI4_CHIP || _ChipType == PixGeometry::FEI4_MODULE){
    _Nmask_stage = 4; // To be checked by Yosuke  
  }*/ //[maria elena] changed

    _Nmask_stage =  basic->NTotmasks;
//*****************************************************************
    
  if(_digitalInjection){
    std::cout << "TOTbasicAnalysisROOT::Initialize WARNING: You are trying to analyze a digital scan, be careful!" << std::endl;
  }

//  std::cout << "TOTbasicAnalysisROOT::Initialize helper" << std::endl;
  helper = ROOTanalysisHelper();

  _Npixels = _Nmasks*_Ncol*std::floor(float(_Nrow)/_Nmask_stage);

  std::cout<<"Analysed scan is ";
  if (_digitalInjection) std::cout << "DIGITAL";
  else if (_scanType==0) std::cout << "ANALOG";
  else if (_scanType==1) std::cout << "FDAC_TUNE";
  else if (_scanType==2) std::cout << "IF_TUNE";
  std::cout<<std::endl;

  std::cout << "Number of masks   = " << _Nmasks << std::endl;
  std::cout << "Total number of masks   = " << _Nmask_stage << std::endl; 
  std::cout << "Number of Events  = " << _Nevents << std::endl;
  std::cout << "Number of modules = " << m_scan->GetEntries() << std::endl;
  std::cout << "Scan type = " << _scanType << std::endl;
  std::cout << std::endl;
  std::cout << "Number of active pixels = " << _Npixels << " over " << _Ncol << " columns and " << _Nrow << " rows." << std::endl;
  std::cout << std::endl;
  Print();
}  

void TOTbasicAnalysisROOT::Calculate() 
{
  std::cout<<"TOTbasicAnalysisROOT::Calculate()::started!"<<std::endl;
  // initializing, might not be necessary  
  Passed = false;
  
    int all_num_modules=8;
    if(_ChipType == PixGeometry::FEI2_CHIP || _ChipType == PixGeometry::FEI2_MODULE){all_num_modules=7;}
    else if(_ChipType == PixGeometry::FEI4_CHIP || _ChipType == PixGeometry::FEI4_MODULE){all_num_modules=8;}
    
    if(m_scan->GetEntries() == 0) return;
    if(m_scan->GetEntries()>all_num_modules) {

    std::cout << "ERROR, pp0 with " << m_scan->GetEntries() << " modules" << std::endl;
    //return;
  }

  if(m_logLevel>=CAN::kDiagnostics1Log) std::cout << "Number of Modules = " << m_scan->GetEntries() << std::endl;
  char title[80];
  
  for(Int_t i = 0; i < m_scan->GetEntries(); i++) {
    
    // if(m_logLevel>=CAN::kDiagnostics2Log) 
      std::cout << " Module #" << i+1 << std::endl;

      // get fe & cp disable information for each module

    AnalysisInputTOTbasic* totbasic = (AnalysisInputTOTbasic*) m_scan->At(i);
    ResultTOTbasic* module_result = new ResultTOTbasic();

      _fe_disable = totbasic->fe_disable;
      _cp_enable = totbasic->cp_enable;

      
    float TOTfe[16];
    float SIGfe[16];
    float TOTEfe[16];
    float SIGEfe[16];

    unsigned int pixPFE[16];
    //    for (unsigned int j=0;j<16;j++){
    for (unsigned int j=0;j<_Nfe;j++){
      TOTfe[j]=0;
      SIGfe[j]=0;
      TOTEfe[j]=0;
      SIGEfe[j]=0;
      pixPFE[j]=0;
    }

    // some variables 
    unsigned int PixPassing=0;
    unsigned int PixFailingHigh=0;
    unsigned int PixFailingLow=0;
    unsigned int PixFailingSigma=0;
    unsigned int PixDead=0;

    float TOTperMod=0;
    float SIGperMod=0;
    float TOTEperMod=0;
    float SIGEperMod=0;

    sprintf(title,"TOT_M%i",i);
    TH1F* TOTperFE = new TH1F(title,title,20,25,35);
    sprintf(title,"TOT_M%ipixel",i);
    TH1F* TOTperPixel = new TH1F(title,title,20,25,35);
    sprintf(title,"SIG_M%i",i);
    TH1F* SIGperFE = new TH1F(title,title,20,0,4);
    sprintf(title,"SIG_M%ipixel",i);
    TH1F* SIGperPixel = new TH1F(title,title,20,0,4);

    TOTperFE->SetDirectory(0);
    TOTperPixel->SetDirectory(0);
    SIGperFE->SetDirectory(0);
    SIGperPixel->SetDirectory(0);

    unsigned int PixGood=0;

    if (_scanType==-1){
      std::cout<<"TOTbasicAnalysis: WARNING could not determine scan type for moudle "<<module_result->moduleName<<", assuming analog."<<std::endl;
      _scanType=0;
    }

            
    if(!totbasic->histoMean ||
       (!totbasic->histoSigma && _scanType==0) ||
       totbasic->histoMean->GetEntries()==0){
      std::cout<<"TOTbasicAnalysis: WARNING no histogram for module "<<module_result->moduleName<<", skipping this one."<<std::endl;
      module_result->passed=false;
      module_result->pp0Name = ((AnalysisInputTOTbasic*)m_scan->At(i))->pp0Name;
      module_result->moduleName = ((AnalysisInputTOTbasic*)m_scan->At(i))->moduleName;

      module_result->PixPassing=0;
      module_result->PixFailingHigh=0;
      module_result->PixFailingLow=0;
      module_result->PixGood=0;
      module_result->PctGood=0;
      module_result->PctPassing=0;
      module_result->PctFailingHigh=0;
      module_result->PctFailingLow=0;
      module_result->PctFailingSigma=0;
      module_result->PixDead=0;
      module_result->PctDead=0;
      module_result->TOTperMod=TOTperMod;
      module_result->SIGperMod=SIGperMod;
      module_result->TOTEperMod=TOTEperMod;
      module_result->SIGEperMod=SIGEperMod;
        
      module_result->TOTfe0=TOTfe[0];
      module_result->TOTfe1=TOTfe[1];
      if(_ChipType == PixGeometry::FEI2_CHIP || _ChipType == PixGeometry::FEI2_MODULE){
	module_result->TOTfe2=TOTfe[2];
	module_result->TOTfe3=TOTfe[3];
	module_result->TOTfe4=TOTfe[4];
	module_result->TOTfe5=TOTfe[5];
	module_result->TOTfe6=TOTfe[6];
	module_result->TOTfe7=TOTfe[7];
	module_result->TOTfe8=TOTfe[8];
	module_result->TOTfe9=TOTfe[9];
	module_result->TOTfe10=TOTfe[10];
	module_result->TOTfe11=TOTfe[11];
	module_result->TOTfe12=TOTfe[12];
	module_result->TOTfe13=TOTfe[13];
	module_result->TOTfe14=TOTfe[14];
	module_result->TOTfe15=TOTfe[15];
      }

      module_result->SIGfe0=SIGfe[0];
      module_result->SIGfe1=SIGfe[1];
      if(_ChipType == PixGeometry::FEI2_CHIP || _ChipType == PixGeometry::FEI2_MODULE){
	module_result->SIGfe2=SIGfe[2];
	module_result->SIGfe3=SIGfe[3];
	module_result->SIGfe4=SIGfe[4];
	module_result->SIGfe5=SIGfe[5];
	module_result->SIGfe6=SIGfe[6];
	module_result->SIGfe7=SIGfe[7];
	module_result->SIGfe8=SIGfe[8];
	module_result->SIGfe9=SIGfe[9];
	module_result->SIGfe10=SIGfe[10];
	module_result->SIGfe11=SIGfe[11];
	module_result->SIGfe12=SIGfe[12];
	module_result->SIGfe13=SIGfe[13];
	module_result->SIGfe14=SIGfe[14];
	module_result->SIGfe15=SIGfe[15];
      }
      module_result->TOTperFE=TOTperFE;
      module_result->TOTperPixel=TOTperPixel;
      module_result->SIGperFE=SIGperFE;
      module_result->SIGperPixel=SIGperPixel;
      m_result->Add(module_result);
      continue;
    }

    for(unsigned int col = 1; col <= _Ncol; col++) {
      for(unsigned int row = 1; row <= _Nrow; row++) {

        // Deal with mask stages
 /*       if (col%2==0){ // even column
          //if ( ((row-1)%32+1)<=32-_Nmasks) continue;
	  if ( ((row-1)%_Nmask_stage+1)<=_Nmask_stage-_Nmasks) continue;
        } else { // odd column
          //if ( ((row-1)%32+1)>_Nmasks) continue;
	  if ( ((row-1)%_Nmask_stage+1)>_Nmasks) continue;
        }*/

//*********************[maria elena] I would do: (same results)
          if (!helper.isMasked(col-1,row-1,_Nmasks,_Nmask_stage,_ChipType)){
              continue;}
//*************************************************

          
          
	if (!_cp_enable[helper.frontEnd(col-1,row-1,_ChipType)][helper.columnPair(col-1,row-1,_ChipType)] || _fe_disable[helper.frontEnd(col-1,row-1,_ChipType)]) continue;
          
          
        float tot = totbasic->histoMean->GetBinContent(col,row);
        float sigma =0.;
        if (_scanType==0) sigma = totbasic->histoSigma->GetBinContent(col,row); // 1,1 is the starting point, since 0 is underflow
      
          
        if (tot==0){
          PixDead++;
        } else if (tot<expected_mean-expected_meanError){
          PixFailingLow++;
        } else if (tot>expected_mean+expected_meanError){
          PixFailingHigh++;
        } else if (sigma>expected_sigmaError) {
          PixFailingSigma++;
        } else {
          PixPassing++;
        }
          

//        std::cout << "Pixel " << col << ", " << row << " with " << tot << std::endl;

          
          
        if (sigma<expected_sigmaError &&
            tot>expected_mean-expected_meanError &&
            tot<expected_mean+expected_meanError ){
          // Pixel passes cuts, add results to histograms
          TOTEperMod = (TOTEperMod*PixGood+(tot-TOTperMod)*(tot-TOTperMod))/(PixGood+1);
          TOTperMod = (TOTperMod*PixGood + tot)/(PixGood+1);
          SIGEperMod = (SIGEperMod*PixGood+(sigma-SIGperMod)*(sigma-SIGperMod))/(PixGood+1);
          SIGperMod = (SIGperMod*PixGood + sigma)/(PixGood+1);
          PixGood++;

          TOTperPixel->Fill(tot);
          SIGperPixel->Fill(sigma);

          //int fe = helper.frontEnd(col-1,row-1); // helper uses 0,0 as the starting point, we're using 1,1
	  int fe = helper.frontEnd(col-1,row-1, _ChipType); // helper uses 0,0 as the starting point, we're using 1,1

	  if(_ChipType == PixGeometry::FEI2_CHIP || _ChipType == PixGeometry::FEI2_MODULE){
	    if (fe>=0 && fe<16){
	      TOTEfe[fe] = ( TOTEfe[fe]*pixPFE[fe]+(tot-TOTfe[fe])*(tot-TOTfe[fe]))/(pixPFE[fe]+1);
	      TOTfe[fe] = (TOTfe[fe]*pixPFE[fe] + tot)/(pixPFE[fe]+1);
	      SIGEfe[fe] = ( SIGEfe[fe]*pixPFE[fe]+(sigma-SIGfe[fe])*(sigma-SIGfe[fe]))/(pixPFE[fe]+1);
	      SIGfe[fe] = (SIGfe[fe]*pixPFE[fe] + sigma)/(pixPFE[fe]+1);
	      pixPFE[fe]++;
	    }
	  }else if(_ChipType == PixGeometry::FEI4_CHIP){
	    if (fe==0){
	      TOTEfe[fe] = ( TOTEfe[fe]*pixPFE[fe]+(tot-TOTfe[fe])*(tot-TOTfe[fe]))/(pixPFE[fe]+1);
	      TOTfe[fe] = (TOTfe[fe]*pixPFE[fe] + tot)/(pixPFE[fe]+1);
	      SIGEfe[fe] = ( SIGEfe[fe]*pixPFE[fe]+(sigma-SIGfe[fe])*(sigma-SIGfe[fe]))/(pixPFE[fe]+1);
	      SIGfe[fe] = (SIGfe[fe]*pixPFE[fe] + sigma)/(pixPFE[fe]+1);
	      pixPFE[fe]++;
	    }
	  }else if(_ChipType == PixGeometry::FEI4_MODULE){ 
	    if (fe>=0 && fe<2){
	      TOTEfe[fe] = ( TOTEfe[fe]*pixPFE[fe]+(tot-TOTfe[fe])*(tot-TOTfe[fe]))/(pixPFE[fe]+1);
	      TOTfe[fe] = (TOTfe[fe]*pixPFE[fe] + tot)/(pixPFE[fe]+1);
	      SIGEfe[fe] = ( SIGEfe[fe]*pixPFE[fe]+(sigma-SIGfe[fe])*(sigma-SIGfe[fe]))/(pixPFE[fe]+1);
	      SIGfe[fe] = (SIGfe[fe]*pixPFE[fe] + sigma)/(pixPFE[fe]+1);
	      pixPFE[fe]++;
	    }
	  }
        }  

      }
    }

      
      
    //    for (unsigned int fe=0;fe<16;fe++){
    for (unsigned int fe=0;fe<_Nfe;fe++){
    if (_fe_disable[fe]) continue;
        
      TOTperFE->Fill(TOTfe[fe]);
      if (TOTEfe[fe]>=0.) TOTEfe[fe] = sqrt( TOTEfe[fe] );
      else TOTEfe[fe] = sqrt( -TOTEfe[fe] );

      SIGperFE->Fill(SIGfe[fe]);
      if (SIGEfe[fe]>=0.) SIGEfe[fe] = sqrt( SIGEfe[fe] );
      else SIGEfe[fe] = sqrt( -SIGEfe[fe] );

      std::cout << "\tOn FE " << fe << " TOT="<<TOTfe[fe]<<"+/-"<<TOTEfe[fe]<<" and SIG="<<SIGfe[fe]<<"+/-"<<SIGEfe[fe]<<std::endl;
    }
      
 
    float PctPassing=float(PixPassing)/float(_Npixels-PixDead);
    float PctFailingHigh=float(PixFailingHigh)/float(_Npixels-PixDead);
    float PctFailingLow=float(PixFailingLow)/float(_Npixels-PixDead);
    float PctFailingSigma=float(PixFailingSigma)/float(_Npixels-PixDead);
    float PctDead=float(PixDead)/float(_Npixels);
    float PctGood=float(PixGood)/float(_Npixels-PixDead);

    std::cout << "\tNumber of Pixels Passing = " << PixPassing << " or " << PctPassing*100. << "%" << std::endl;
    std::cout << "\tNumber of Pixels Failing High = " << PixFailingHigh << " or " << PctFailingHigh*100. << "%" << std::endl;
    std::cout << "\tNumber of Pixels Failing Low = " <<PixFailingLow << " or " << PctFailingLow*100. << "%" << std::endl;
    std::cout << "\tNumber of Pixels Failing Sigma = " <<PixFailingSigma << " or " << PctFailingSigma*100. << "%" << std::endl;
    std::cout << "\tNumber of Pixels Dead = " << PixDead << " or " << PctDead*100. << "%" << std::endl;
    std::cout << "\tWith " << PixGood << " good pixels of " << _Npixels << std::endl;
    std::cout << "\tThis module had TOT="<<TOTperMod<<"+/-"<<TOTEperMod<<" and SIG="<<SIGperMod<<"+/-"<<SIGEperMod<<std::endl;
    std::cout << std::endl;

    module_result->pp0Name = ((AnalysisInputTOTbasic*)m_scan->At(i))->pp0Name;
    module_result->moduleName = ((AnalysisInputTOTbasic*)m_scan->At(i))->moduleName;

    module_result->PixPassing=PixPassing;
    module_result->PixFailingHigh=PixFailingHigh;
    module_result->PixFailingLow=PixFailingLow;
    module_result->PixFailingSigma=PixFailingSigma;
    module_result->PixGood=PixGood;
    module_result->PctGood=PctGood;
    module_result->PctPassing=PctPassing;
    module_result->PctFailingHigh=PctFailingHigh;
    module_result->PctFailingLow=PctFailingLow;
    module_result->PctFailingSigma=PctFailingSigma;
    module_result->PixDead=PixDead;
    module_result->PctDead=PctDead;
    module_result->TOTperMod=TOTperMod;
    module_result->SIGperMod=SIGperMod;
    module_result->TOTEperMod=TOTEperMod;
    module_result->SIGEperMod=SIGEperMod;
 
    module_result->TOTfe0=TOTfe[0];
    module_result->TOTfe1=TOTfe[1];
    if(_ChipType == PixGeometry::FEI2_CHIP || _ChipType == PixGeometry::FEI2_MODULE){
      module_result->TOTfe2=TOTfe[2];
      module_result->TOTfe3=TOTfe[3];
      module_result->TOTfe4=TOTfe[4];
      module_result->TOTfe5=TOTfe[5];
      module_result->TOTfe6=TOTfe[6];
      module_result->TOTfe7=TOTfe[7];
      module_result->TOTfe8=TOTfe[8];
      module_result->TOTfe9=TOTfe[9];
      module_result->TOTfe10=TOTfe[10];
      module_result->TOTfe11=TOTfe[11];
      module_result->TOTfe12=TOTfe[12];
      module_result->TOTfe13=TOTfe[13];
      module_result->TOTfe14=TOTfe[14];
      module_result->TOTfe15=TOTfe[15];
    }

    module_result->SIGfe0=SIGfe[0];
    module_result->SIGfe1=SIGfe[1];
    if(_ChipType == PixGeometry::FEI2_CHIP || _ChipType == PixGeometry::FEI2_MODULE){
      module_result->SIGfe2=SIGfe[2];
      module_result->SIGfe3=SIGfe[3];
      module_result->SIGfe4=SIGfe[4];
      module_result->SIGfe5=SIGfe[5];
      module_result->SIGfe6=SIGfe[6];
      module_result->SIGfe7=SIGfe[7];
      module_result->SIGfe8=SIGfe[8];
      module_result->SIGfe9=SIGfe[9];
      module_result->SIGfe10=SIGfe[10];
      module_result->SIGfe11=SIGfe[11];
      module_result->SIGfe12=SIGfe[12];
      module_result->SIGfe13=SIGfe[13];
      module_result->SIGfe14=SIGfe[14];
      module_result->SIGfe15=SIGfe[15];
    }
 
    module_result->TOTperFE=TOTperFE;
    module_result->TOTperPixel=TOTperPixel;
    module_result->SIGperFE=SIGperFE;
    module_result->SIGperPixel=SIGperPixel;
    
    // check if all modules are working -> let scan pass or fail
    if(PctPassing < 1.-max_failing_pixels/100 ||
       TOTperMod < expected_mean - expected_meanFail ||
       TOTperMod > expected_mean + expected_meanFail ||
       SIGperMod > expected_sigmaFail )
      {
	Passed = false;
      }
    else Passed = true;

    module_result->passed = Passed;
    std::cout<<"Result for module "<<module_result->moduleName<<" is: "<<((module_result->passed)? "OK":"FAILED")<<std::endl;
    
    m_result->Add(module_result);
      
  } // modules
    
  if(m_logLevel>=CAN::kDiagnostics1Log) this->Print();

} 


float TOTbasicAnalysisROOT::AdditionalCalculations(int module)
{
  std::cout<<"TOTbasicAnalysisROOT::AdditionalCalculations: started, but I'm not sure why."<<std::endl;
  return 0.;
}

 
void TOTbasicAnalysisROOT::QualifyResults() 
{
  std::cout << "aaaa2" << std::endl;

//  cout<<"TOTbasicAnalysisROOT::QualifyResults() starting"<<endl;

  allModulesPassed = true;
  for(int i = 0; i<m_result->GetEntries(); i++)
    {
      ResultTOTbasic* module_result = (ResultTOTbasic*)m_result->At(i);
      if(!module_result->passed)
	{
	  allModulesPassed = false;
	  break;
	}
    }

  std::cout<<"TOTbasicAnalysisROOT::QualifyResults():  "<<(allModulesPassed? "OK":"FAILED")<<std::endl;
  
  if(allModulesPassed) m_status= kPassed;
  else  m_status=kFailed;
  
}


void TOTbasicAnalysisROOT::CheckForKnownProblems() //setting of failure bits
{
  std::cout << "aaaa3" << std::endl;

}

void TOTbasicAnalysisROOT::Print()
{
  std::cout << std::endl;
  std::cout << "-------------------------------------" << std::endl;
  std::cout << "TOTbasic Scan Analysis Results: " << std::endl;
  std::cout << "-------------------------------------" << std::endl;
  std::cout << "Expected mean: " << expected_mean << std::endl;
 // std::cout << "Expected sigma: " << expected_sigma << std::endl; [maria elena]
  std::cout << "Expected mean error: " << expected_meanError << std::endl;
  std::cout << "Expected sigma error: " << expected_sigmaError << std::endl;
  std::cout << "Failing mean error: " << expected_meanFail << std::endl;
  std::cout << "Failing sigma error: " << expected_sigmaFail << std::endl;
  std::cout << "Max pct failing pixels: " << max_failing_pixels << std::endl;
  std::cout << "-------------------------------------" << std::endl;
  std::cout << std::endl << std::endl;
}

