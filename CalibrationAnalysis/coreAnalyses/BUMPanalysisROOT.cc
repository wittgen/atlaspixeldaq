#ifndef _BUMPanalysisROOT_CC_
#define _BUMPanalysisROOT_CC_

#include "BUMPanalysisROOT.h"
#include <iostream>
#include "TMath.h"
#include "TCanvas.h"
#include "../Lock.hh"
#include "../PixDbGlobalMutex.hh"


BUMPanalysisROOT::BUMPanalysisROOT()
{
  std::cout << "BUMPanalysis:  constructed." << std::endl;
} 


BUMPanalysisROOT::~BUMPanalysisROOT()
{
  //WriteResults();
}

void BUMPanalysisROOT::WriteResults()
{
  TFile f("resultsBUMP.root","update");
  //  f.WriteTObject(m_result.get(),"scanHistos","SingleKey"); //as one array
  f.Close();  
}

void BUMPanalysisROOT::Initialize()
{
  m_longNoiseCut = 240;
  m_normalNoiseCut = 160;
  m_minHitsAnalog = 1;
  m_minHitsXtalk = 1;
    
  AnalysisInputBUMP* bump = (AnalysisInputBUMP*) m_scan->At(0);
  if(!bump || !bump->histo_xtalk) return; 

  PixGeometry geo(PixA::nRows(bump->histo_xtalk),PixA::nColumns(bump->histo_xtalk)); 
  _ChipType = geo.pixType();

_Nrow = helper.getNrow(_ChipType); //bump->histo_xtalk->GetNbinsY();
_Ncol = helper.getNcol(_ChipType); //bump->histo_xtalk->GetNbinsX();
_Nmasks  = bump->Nmasks;
_Nevents = bump->Nevents;
_Nmask_stage =  bump->NTotmasks;

}  

void BUMPanalysisROOT::Calculate() 
{
  //locking this mutex prevents ROOT from colliding with itself in the Histogram
  // Recievers et al. 
  CAN::Lock lock( CAN::PixDbGlobalMutex::mutex() );
  std::cout << "**************************************" << std::endl;
  std::cout << "BUMPanalysis::Calculate  starting" << std::endl;
  m_scan->R__FOR_EACH(AnalysisInputBUMP,PrintInfo)(); 
  std::cout << "BUMPanalysis: These were the module names." << std::endl;  

  // Loop over modules
  int missingHistoXtalk = 0, missingHistoAnalog = 0, missingHistoSigma = 0, missingHistoAnalog_merg=0;
  for(Int_t i=0; i<m_scan->GetEntries(); i++) { 
    ResultBUMP* module_result = new ResultBUMP();
    module_result->pp0Name = ((AnalysisInputBUMP*)m_scan->At(i))->pp0Name;
    module_result->moduleName = ((AnalysisInputBUMP*)m_scan->At(i))->moduleName;
      
    const TH2* xtalkHisto = ((AnalysisInputBUMP*)m_scan->At(i))->histo_xtalk;
    const TH2* analogHisto = ((AnalysisInputBUMP*)m_scan->At(i))->histo_analog;
    const TH2* sigmaHisto = ((AnalysisInputBUMP*)m_scan->At(i))->histo_sigma;
//********************************************************
const TH2* analogHisto_merg = ((AnalysisInputBUMP*)m_scan->At(i))->histo_analog_merg;
//********************************************************************************
      

    if (xtalkHisto == NULL)
      missingHistoXtalk++;
    if (analogHisto == NULL)
      missingHistoAnalog++;
    if (sigmaHisto == NULL)
      missingHistoSigma++;
//*********************************************************
      if (/*m_is_merged &&*/ analogHisto_merg == NULL)
      missingHistoAnalog_merg++;
//*********************************************************************************
      
      
      
//************************************[maria elena] would do like this:

 //   if (xtalkHisto == NULL || analogHisto == NULL || sigmaHisto == NULL) // cannot do disconnected bumps search with crosstalk
 //     continue;

      
       if ((xtalkHisto == NULL || analogHisto == NULL || sigmaHisto == NULL) && (_ChipType == PixGeometry::FEI2_CHIP || _ChipType == PixGeometry::FEI2_MODULE)) // cannot do disconnected bumps search
          continue;
       if ((analogHisto == NULL || sigmaHisto == NULL) && (_ChipType == PixGeometry::FEI4_CHIP || _ChipType == PixGeometry::FEI4_MODULE)) // cannot do disconnected bumps search
       continue;
      
      
      if(/*m_is_merged && */analogHisto_merg == NULL) continue;
//******************************************************************
      
      
    TString moduleName = ((AnalysisInputBUMP*)m_scan->At(i))->moduleName;
//    std::shared_ptr<TH2I> disconnectedPixels(new TH2I("Disconnected "+moduleName,"Disconnected "+moduleName,NCOLS,-0.5,NCOLS-0.5,NROWS,-0.5,NROWS-0.5));
//************************************[maria elena] does like this:
    std::shared_ptr<TH2I> disconnectedPixels(new TH2I("Disconnected "+moduleName,"Disconnected "+moduleName,_Ncol,-0.5,_Ncol-0.5,_Nrow,-0.5,_Nrow-0.5));
//******************************************************************
    int nAnalogDead = 0;
    int nXtalkDead = 0;
    int nDisconnected = 0;
    int nMerged = 0;
      
      FE_disable = ((AnalysisInputBUMP*)m_scan->At(i))->fe_disable;
      _cp_enable = ((AnalysisInputBUMP*)m_scan->At(i))->cp_enable;

    //for (int row=0; row<NROWS; row++){
    //for (int col=0; col<NCOLS; col++){
    for (int row=0; row<_Nrow; row++){
      for (int col=0; col<_Ncol; col++){

          if (!helper.isMasked(col,row,_Nmasks,_Nmask_stage,_ChipType)){continue;}

	if (FE_disable[helper.frontEnd(col,row,_ChipType)] == 1)
	  continue;

if (!_cp_enable[helper.frontEnd(col,row,_ChipType)][helper.columnPair(col,row,_ChipType)]) continue;

	float hitsAnalog = analogHisto->GetBinContent(col+1,row+1);
 
//*************************************
float hitsAnalog_merg=0;
//if(m_is_merged)
{
hitsAnalog_merg = analogHisto_merg->GetBinContent(col+1,row+1);}
//********************************************************

//********************************************************************
          float hitsXtalk =0;
if(_ChipType == PixGeometry::FEI2_CHIP || _ChipType == PixGeometry::FEI2_MODULE){
          
	hitsXtalk = xtalkHisto->GetBinContent(col+1,row+1);
          
      }
//**************************************************************************************
          
          
	float noise = sigmaHisto->GetBinContent(col+1,row+1);
	bool isLongPixel = false, isGangedPixel = false;

	if(_ChipType == PixGeometry::FEI2_CHIP || _ChipType == PixGeometry::FEI2_MODULE){
	  if (col%18 == 0 || col%18 == 17)
	    isLongPixel = true;
	  if (row == 153 || row == 155 || row == 157 || row == 159 || row == 160 || row == 162 || row == 164 || row == 166)
	    isGangedPixel = true;
	}else if(_ChipType == PixGeometry::FEI4_CHIP){
	  if (col == 0 || col == _Ncol-1){
	    isLongPixel = true;
	  }
	}else if(_ChipType == PixGeometry::FEI4_MODULE){
	  if (col == 0 || col == _Ncol/2-1 || col == _Ncol/2 || col == _Ncol-1)
	    isLongPixel = true;
	}

	bool isNoisy = false;
	if ( ((isLongPixel || isGangedPixel) && noise>m_longNoiseCut) || (!(isLongPixel || isGangedPixel) && noise>m_normalNoiseCut) )
	  isNoisy = true;
          
	if (hitsAnalog < m_minHitsAnalog)
	  nAnalogDead++;
          
//*******************************
if(/*m_is_merged &&*/ hitsAnalog_merg >= m_minHitsAnalog){
nAnalogDead--;
nMerged++;
}
//**************************************************
          
          
//********************************************************************
if(_ChipType == PixGeometry::FEI2_CHIP || _ChipType == PixGeometry::FEI2_MODULE){

	if (hitsXtalk < m_minHitsXtalk)
	  nXtalkDead++;
          
	if (hitsAnalog >= m_minHitsAnalog && hitsXtalk < m_minHitsXtalk && !isNoisy){
	  nDisconnected++;
	  disconnectedPixels->Fill(col,row,1);
	}
	else if (hitsAnalog >= m_minHitsAnalog && hitsXtalk < m_minHitsXtalk && isNoisy)
	  disconnectedPixels->Fill(col,row,-1);
          
}
          
else if(_ChipType == PixGeometry::FEI4_CHIP || _ChipType == PixGeometry::FEI4_MODULE){
 //if (hitsAnalog >= m_minHitsAnalog && !isNoisy){
 nDisconnected=0;
 disconnectedPixels->Fill(col,row,0);
/* }
 else if (hitsAnalog >= m_minHitsAnalog && isNoisy)
 disconnectedPixels->Fill(col,row,-1);*/

 }
//**************************************************************************************

      }
    }

      

    module_result->nAnalogDead = nAnalogDead;
//if(m_is_merged)
{module_result->nAnalogMerged = nMerged;}
    module_result->nXtalkDead = nXtalkDead;
    module_result->nDisconnected = nDisconnected;
    module_result->disconnectedPixels = disconnectedPixels;
    m_result->Add(module_result);

    std::cout << "===========================================" << std::endl;
    std::cout << " Analysis results "<< std::endl;
    std::cout << "===========================================" << std::endl;
    std::cout << "nAnalogDead       : " << module_result->nAnalogDead << std::endl;
//if(m_is_merged)
{std::cout << "nAnalogMerged       : " << module_result->nAnalogMerged << std::endl;}
    std::cout << "nXtalkDead        : " << module_result->nXtalkDead << std::endl;
    std::cout << "nDisconnected     : " << module_result->nDisconnected << std::endl;
    std::cout << "===========================================" << std::endl;
  }
  std::cout << "**************************************" << std::endl;
  std::cout << "Summary" << std::endl;
  std::cout << "**************************************" << std::endl;
  std::cout << "Total scans without XTalk Histo: " << missingHistoXtalk << std::endl;
  std::cout << "Total scans without Analog Histo: " << missingHistoAnalog << std::endl;
  std::cout << "Total scans without Sigma Histo: " << missingHistoSigma << std::endl;
//if(m_is_merged)
{  std::cout << "Total scans without Analog Merged Histo: " << missingHistoAnalog_merg << std::endl;}
  std::cout << "**************************************" << std::endl;
}


void BUMPanalysisROOT::QualifyResults() 
{
  std::cout << "*************************************" << std::endl;
}

#endif

