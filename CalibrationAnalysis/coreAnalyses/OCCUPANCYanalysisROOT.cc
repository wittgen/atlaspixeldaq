#include "OCCUPANCYanalysisROOT.h"
#include <iostream>
#include <fstream>
#include "TFile.h"
#include "TCanvas.h"
#include "TH2.h"
#include "DataContainer/HistoUtil.h"
#include "PixFe/PixGeometry.h"
#include "DataContainer/GenericHistogramAccess.h"
using namespace PixLib;





OCCUPANCYanalysisROOT::OCCUPANCYanalysisROOT(): _Nmasks(0), _Nevents(0), _Ncol(0), _Nrow(0), _ChipFeFlav(PixGeometry::INVALID_MODULE)
{
  std::cout << std::endl;
  std::cout << "-------------------------------" << std::endl;
  std::cout << "   OCCUPANCY Analysis Algorithm   " << std::endl;
  std::cout << "-------------------------------" << std::endl;
  std::cout << std::endl;

  // this analysis can handle digital and analog scans
  // for digital scans and analog scans with an injected charge above the threshold (this value can be defined by "min_charge" in the analysis configuration) it is expected to see all injected signals.
  // in this case the status of each module is determined depending on the number of extra or missing hits seen by the module.
  // the cut values can be defined in the configuration (max_extra_hits_allowed, max_missing_hits_allowed) and are 0 by default.
  // in case the scan is analog and the injected charge is below or near threshold (< min_charge) the mean value of seen hits is calculated for each module and the percentage of seen hits is given back (SeenPercentage in Console, set to 100 for digital scans & analog scans above threshold).
  // a variable "OverThreshold" is given back depending on the percentage of seen hits - (1) means above threshold, which should be true if >95% hits are seen, (2) means close to threshold and (3) means below threshold, which should be true if <10% hits are seen.




}



OCCUPANCYanalysisROOT::~OCCUPANCYanalysisROOT()
{
  //
}

void OCCUPANCYanalysisROOT::Initialize()
{
  if(!m_scan) {
    std::cout << std::endl;
    std::cout << "OCCUPANCYanalysisROOT::Initialize ERROR: input data pointer is null" << std::endl;
    std::cout << std::endl;
    return;
  }

  AnalysisInputOCCUPANCY* occupancy = (AnalysisInputOCCUPANCY*) m_scan->At(0);
  if(!occupancy || !occupancy->histo) return; 
  _Nmasks  = occupancy->Nmasks;
  _Nevents = occupancy->Nevents;
_Nmasksdone = occupancy->Nmasksdone;
    _ChipFeFlav=occupancy->_ChipFeFlav;

//  PixGeometry geo(PixA::nRows(occupancy->histo),PixA::nColumns(occupancy->histo));
//  _ChipType = geo.pixType();

  _Nrow = helper.getNrow(_ChipFeFlav);//occupancy->histo->GetNbinsY();
  _Ncol = helper.getNcol(_ChipFeFlav);//occupancy->histo->GetNbinsX();

    
    
    std::cout << "Number columns " << _Ncol << std::endl;
    std::cout << "Number rows " << _Nrow << std::endl;
  //  std::cout << "Chiptype Maria Elena " << _ChipFeFlav <<" Histo chip type "<< _ChipType <<  std::endl;
    
  _digitalInjection = occupancy->digitalInjection;
  
  if(!_digitalInjection)
    {
      _charge = occupancy->charge;
    }
  // if digital scan, charge is not used, set to 0
  else _charge = 0;


  // fill pattern
  //-----------------------
  int nPixels = 0;
  for(int col = 1; col <= _Ncol; col++) {
    for(int row = 1; row <= _Nrow; row++) {
      _pp0pattern[col-1][row-1] = 0;
    }
  }
  
  //odd columns
  for(int h = 1; h <= _Nrow; h = h+_Nmasks) {
    for(int j = 1; j <= _Ncol; j = j+2) {
      for(int m = 0; m < _Nmasks; m++) {
	
	int col = j;
	int row = h+m;
	
	_pp0pattern[col-1][row-1] = _Nevents;
	nPixels++;

      }  
    }
  }

  //even columns
  // for(int h = 1; h <= _Nrow; h = h+_Nmasks) might be wrong by yosuke
  for(int h = _Nmasks; h <= _Nrow; h = h+_Nmasks) {
    for(int j = 2; j <= _Ncol; j = j+2) {
      for(int m = 0; m < _Nmasks; m++) {
	
	int col = j;
	int row = h-m;
	if(row<1) row += _Nrow;

	_pp0pattern[col-1][row-1] = _Nevents;
	nPixels++;

      }  
    }
  }  

  std::cout<<"Analysed scan is "<<(_digitalInjection? "DIGITAL":"ANALOG")<<std::endl;
  std::cout << "Number of masks   = " << _Nmasks << std::endl;
  std::cout << "Number of masks performed = " << _Nmasksdone << std::endl;
  std::cout << "Number of Events  = " << _Nevents << std::endl;
  std::cout << "Number of modules = " << m_scan->GetEntries() << std::endl;
  std::cout << std::endl;
  std::cout << "Number of pixels = " << nPixels << std::endl;
  std::cout << std::endl;
    
} 
  
  
 

void OCCUPANCYanalysisROOT::Calculate() 
{
  //TFile f("test.root","recreate");

  std::cout<<"OCCUPANCYanalysisROOT::Calculate()::started!"<<std::endl;
    
  int Nfe=helper.getNfrontEnd(_ChipFeFlav); // # of FEs in one module
  //int Ncp=helper.getNcp(_ChipFeFlav); // # of the enable column pairs
  int Npix=helper.getNpixel(_ChipFeFlav); // # of pixel in one FE

  // initializing, might not be necessary  
  Passed = false;

  std::cout << "OCC entries: " << m_scan->GetEntries()  << std::endl;
    int all_num_modules=8;
    if(_ChipFeFlav == PixGeometry::FEI2_CHIP || _ChipFeFlav == PixGeometry::FEI2_MODULE){all_num_modules=7;}
    else if(_ChipFeFlav == PixGeometry::FEI4_CHIP || _ChipFeFlav == PixGeometry::FEI4_MODULE){all_num_modules=8;}
    
  if(m_scan->GetEntries() == 0) return;
  if(m_scan->GetEntries()>all_num_modules) {
    std::cout << "ERROR, pp0 with " << m_scan->GetEntries() << " modules" << std::endl;
    //return;
  }

  if(m_logLevel>=CAN::kDiagnostics1Log) std::cout << "Number of Modules = " << m_scan->GetEntries() << std::endl;

  for(int i = 0; i < m_scan->GetEntries(); i++) {
    AnalysisInputOCCUPANCY* occupancy = (AnalysisInputOCCUPANCY*) m_scan->At(i);
    ResultOCCUPANCY* module_result = new ResultOCCUPANCY();

    // if(m_logLevel>=CAN::kDiagnostics2Log) 
      std::cout << " Module #" << i+1 << std::endl;

      // get fe & cp disable information for each module
      _fe_disable = occupancy->fe_disable;
      _cp_enable = occupancy->cp_enable;
      // initialize pattern and check for not analyzed pixels
      for(int col = 0; col < _Ncol; col++) {
	for(int row = 0; row < _Nrow; row++) {
	  _pattern[col][row] = _pp0pattern[col][row];
        if(!helper.isMasked(col,row,_Nmasksdone,_Nmasks,_ChipFeFlav)) _pattern[col][row] = 0; //[maria elena]
	}
      }
      // set expected value to 0 if FE is disabled, this should prevent disabled FEs from being counted
      for(int fe = 0; fe < Nfe; fe++){
	if(_fe_disable.size() <= 0) continue;
	if(_fe_disable[fe] == 1){
	  std::cout<<"OCCUPANCYanalysis::Calculate(): FE "<<fe<<"on module "<<occupancy->moduleName<<"is disabled!"<<std::endl;
	  for(int col = helper.getFrontEndStartCol(fe, _ChipFeFlav); col <= helper.getFrontEndStopCol(fe, _ChipFeFlav); col++){
	    for(int row = helper.getFrontEndStartRow(fe, _ChipFeFlav); row <= helper.getFrontEndStopRow(fe, _ChipFeFlav); row++){
	      _pattern[col][row] = 0;
	    }
	  }
	}

	// check for disable cp
	if(_cp_enable.size() <= 0) continue;
	//for(int cp = 0; cp < Ncp; cp++){
          for(int col = helper.getFrontEndStartCol(fe, _ChipFeFlav); col <= helper.getFrontEndStopCol(fe, _ChipFeFlav); col++){
              for(int row = helper.getFrontEndStartRow(fe, _ChipFeFlav); row <= helper.getFrontEndStopRow(fe, _ChipFeFlav); row++){
                  int cp= helper.columnPair(col, row, _ChipFeFlav);         
	  if(_cp_enable[fe][cp] == 0){
	    std::cout<<"OCCUPANCYanalysis::Calculate(): "<<"CP "<< cp<< " in FE "<<fe<<" on module "<<occupancy->moduleName<<" is disabled!"<<std::endl;
	    //for(int row = helper.getFrontEndStartRow(fe, _ChipType); row <= helper.getFrontEndStopRow(fe, _ChipType); row++){
	      _pattern[col][row] = 0;
	    }
	  }
	}
      }

      std::vector<std::vector<int>> devPattern (_Ncol, std::vector<int>( _Nrow, 0) );
      // some variables 
      int extraHits = 0;
      int maxExtraHits = 0;
      int missingHits = 0;
      int maxMissingHits = 0;
      int PixMissingHits = 0;
      int PixExtraHits = 0;
      int PixBadMissing = 0;
      int PixBadExtra = 0;
      int maskMissingHits = 0;
      int maskExtraHits = 0;
      int maskPixExtraHits = 0;
      int maskPixMissingHits = 0;
      int ThrPosition = 0;
      int DeadPixel = 0;
      float SeenPercentage = 100;
      bool Passed_FirstMS = false;
      bool DeadFE = true; // !DeadFE will be returned 
      bool HVopen = false;
      std::vector<int> MaskStageProblems(_Nmasks, 0); // vector to hold number of problematic pixel per mask stage
      
      // an array to hold dead pixel per FE
      std::vector<int> deadPixelpFE (Nfe, 0);
      
      if(!occupancy->histo || occupancy->histo->GetEntries()==0)
      {
	std::cout<<"OCCUPANCYanalysis: WARNING no histogram for module "<<occupancy->moduleName<<", skipping this one."<<std::endl;
	module_result->passed=false;
	m_result->Add(module_result);
	continue;
      }


       // std::cout<<"Ncol: "<<_Ncol<<" Nrow "<<_Nrow<<std::endl;


/*	  int expected = _pattern[col-1][row-1];
	  int observed = (int) occupancy->histo->GetBinContent(col,row);                
	  int error = observed - expected;
	  // fill error pattern
	  devPattern[col-1][row-1] = error;
	  if(m_logLevel>=CAN::kDiagnostics3Log && i==0) std::cout << col << ", " << row << "    " << expected << " - " << observed << "; " << error << std::endl;
	  if(observed<expected)
	    {
	      missingHits -= error;
	      PixMissingHits++;
	      if(abs(error) > maxMissingHits) maxMissingHits = abs(error);
	      // counts the number of pixels with more than 10% deviation from expected value
	      if(abs(error) > 0.1*expected) PixBadMissing++;
	      
	      // count completely dead pixel
	      if(observed == 0) {
		DeadPixel++;
		
		// count number of dead pixel for this FE
		deadPixelpFE[helper.frontEnd(col-1,row-1, _ChipType)]++;
		// if selected, create entry in pixelmap
		const pair<int,int> index = make_pair(col-1,row-1);
		
		if(create_Pixelmap){
		  if(_digitalInjection)  helper.SetBit(module_result->deadPixelMap[index], 8);
		  else if( _charge > min_charge)  helper.SetBit(module_result->deadPixelMap[index], 17);
		}
	      }
	    }
	  if(observed>expected) 
	    {
	      extraHits   += error;
	    PixExtraHits++;
	    if(error > maxExtraHits) maxExtraHits = error;
	    //counts the number of pixels with more than 10% deviation from expected value;
	    if(error > 0.1*expected ) PixBadExtra++;
	    }
	  
	  if(m_logLevel>=CAN::kDiagnostics4Log) {
	    if(observed!=expected)
	      std::cout <<  col << ", " << row << "    " << expected << " - " << observed << "; " << error << std::endl;
	  }
	}
          
      }
*/
      int expected=0;
      int observed=0;
      int error=0;


    for(int fe = 0; fe < Nfe; fe++){
        if(_fe_disable[fe] == 1){continue;}
        for(int col = helper.getFrontEndStartCol(fe, _ChipFeFlav); col <= helper.getFrontEndStopCol(fe, _ChipFeFlav); col++){
            for(int row = helper.getFrontEndStartRow(fe, _ChipFeFlav); row <= helper.getFrontEndStopRow(fe, _ChipFeFlav); row++){
                
                int cp= helper.columnPair(col, row, _ChipFeFlav);
                if(!helper.isMasked(col,row,_Nmasksdone,_Nmasks,_ChipFeFlav) || _cp_enable[fe][cp] ==0){continue;}
               // std::cout<<"col "<<col<<" row "<<row<<std::endl;
              expected = _pattern[col][row];
              observed = (int) occupancy->histo->GetBinContent(col+1,row+1);
              error = observed - expected;
              // fill error pattern
              devPattern[col][row] = error;
              if(m_logLevel>=CAN::kDiagnostics3Log && i==0) std::cout << col+1 << ", " << row+1 << "    " << expected << " - " << observed << "; " << error << std::endl;
              if(error<0 && observed!=0)
              {
                  missingHits -= error;
                  PixMissingHits++;
                  if(abs(error) > maxMissingHits) maxMissingHits = abs(devPattern[col][row]);
                  // counts the number of pixels with more than 10% deviation from expected value
                  if(abs(error) > 0.1*expected) PixBadMissing++;
              }
              // count completely dead pixel
              if(observed == 0 && error<0) {
                  DeadPixel++;

                  // count number of dead pixel for this FE
                  deadPixelpFE[helper.frontEnd(col,row, _ChipFeFlav)]++;
                  // if selected, create entry in pixelmap
                  const std::pair<int,int> index = std::make_pair(col,row);
                  
                  if(create_Pixelmap){
                      if(_digitalInjection)  helper.SetBit(module_result->deadPixelMap[index], 8);
                      else if( _charge > min_charge)  helper.SetBit(module_result->deadPixelMap[index], 17);
                  }

              }
              
              if(error>0 && observed!=0)
              {
                  extraHits   += error;
                  PixExtraHits++;
                  if(error > maxExtraHits) maxExtraHits = error;
                  //counts the number of pixels with more than 10% deviation from expected value;
                  if(error > 0.1*expected ) PixBadExtra++;
              }
              
              if(m_logLevel>=CAN::kDiagnostics4Log) {
                  if(observed!=expected)
                      std::cout <<  col+1 << ", " << row+1 << "    " << expected << " - " << observed << "; " << error << std::endl;
              }

          }
      }
  }
    
      // if(m_logLevel>=CAN::kDiagnostics1Log) {
      std::cout << "   Number of missing hits = " << missingHits << std::endl;
      std::cout << "   Number of Pixels with missing hits = " << PixMissingHits << std::endl;
      std::cout << "   Number of Pixels with missing hits and more than 10% deviation from expected hits = " <<PixBadMissing << std::endl;
      std::cout << "   Maximum of missing hits = " << abs(maxMissingHits) << std::endl;
      std::cout << "   Number of extra hits = " << extraHits << std::endl;
      std::cout << "   Number of Pixels with extra hits = " << PixExtraHits << std::endl;
      std::cout << "   Number of Pixels with extra hits and more than 10% deviation from expected hits = " <<PixBadExtra << std::endl;
      std::cout << "   Maximum of extra hits = " << maxExtraHits << std::endl; 
      std::cout <<"   "<<DeadPixel<<" pixel are presumably dead"<<std::endl;
      std::cout <<std::endl;        
      
      
    // loop over maskstages to get values per maskstage
    for(int m = 0; m < _Nmasks; m++)
      {
	// reinitialize for each maskstage
	maskMissingHits = 0;
	maskExtraHits = 0;
	maskPixMissingHits = 0;
	maskPixExtraHits = 0;
	
	// get the entries corresponding to this maskstage
	// odd columns first
	for(int h = 1; h <= _Nrow; h = h+_Nmasks) {
	  for(int j = 1; j <= _Ncol; j = j+2) {
	    int err = devPattern[j-1][h+m-1];
	    if(err>0) 
	      {
		maskExtraHits+=err;
		maskPixExtraHits++;
	      }
	    if(err<0)
	      {
		maskMissingHits-=err;
		maskPixMissingHits++;
	      }
	    if(err != 0) MaskStageProblems[m]++;
	  }
	}
	// even columns
	for(int h = _Nmasks; h <= _Nrow; h = h+_Nmasks) {
	  for(int j = 2; j <= _Ncol; j = j+2) {
	    
	    int row = h-m;
	    if(row<1) row += _Nrow;
	    int err  = devPattern[j-1][row-1];
	    if(err>0)
	      {
		maskExtraHits+=err;
		maskPixExtraHits++;
	      }
	    if(err<0) 
	      {
		maskMissingHits-=err;
		maskPixMissingHits++;
	      }
	    if(err != 0) MaskStageProblems[m]++;
	  }
	}


	//cout << "Maskstage "<<m+1<< " shows "<<abs(maskMissingHits)<<" missing hits in "<<maskPixMissingHits <<" pixels and "<<maskExtraHits<<" extra hits in "<<maskPixExtraHits<<" pixels." <<endl;
	

	// give out failures in first mask stage
	if(m==0)
	  {
	    module_result->MissingHitsMS1 = abs(maskMissingHits);
	    module_result->PixMissingHitsMS1 = maskPixMissingHits;
	    module_result->ExtraHitsMS1 = maskExtraHits;
	    module_result->PixExtraHitsMS1 = maskPixExtraHits;
	  }
 

	
	
      }

    module_result->pp0Name = ((AnalysisInputOCCUPANCY*)m_scan->At(i))->pp0Name;
    module_result->moduleName = ((AnalysisInputOCCUPANCY*)m_scan->At(i))->moduleName;
    module_result->PixExtraHits = PixExtraHits;
    module_result->PixMissingHits = PixMissingHits;
    module_result->MaxExtraHits = maxExtraHits;
    module_result->MaxMissingHits = maxMissingHits;
    module_result->MissingHits = missingHits;
    module_result->ExtraHits = extraHits;
    module_result->MissingHitsMSnot1 = missingHits - (module_result->MissingHitsMS1);
    module_result->PixMissingHitsMSnot1 = PixMissingHits - (module_result->PixMissingHitsMS1);
    module_result->ExtraHitsMSnot1 = extraHits - (module_result->ExtraHitsMS1);
    module_result->PixExtraHitsMSnot1 = PixExtraHits - (module_result->PixExtraHitsMS1);
    module_result->DeadPixel = DeadPixel;
    module_result->MakePixelMap = create_Pixelmap;
      
//******************************* new variable added [maria elena] to count the sum of dead + badmissing + badextra pixels
    module_result->BadPixel = DeadPixel+PixBadExtra+PixBadMissing;
    std::cout<<"bad Pixels are"<<module_result->BadPixel<<std::endl;
//*******************************************************************************

#if 0
    // check for dead FE only if we have a high number of dead pixel
    if(DeadPixel >= 1000) {
      // loop over FEs
      for(int j = 0; j < Nfe; j++){
	bool fevalue = true;
	if(deadPixelpFE[j] > Npix*( DeadFE_percentage /100)) fevalue = false; 
	DeadFE = DeadFE && fevalue;
      }
    }
      
#endif

      // check for dead FE only if we have a high number of dead pixel
      std::cout<<"the max number of dead pixel allowed is "<<Max_Dead_Pixel<<std::endl;

      if(DeadPixel >= Max_Dead_Pixel) {
          // loop over FEs
          for(int j = 0; j < Nfe; j++){
              bool fevalue = true;
              if(deadPixelpFE[j] > Npix*( DeadFE_percentage /100)) fevalue = false;
              DeadFE = DeadFE && fevalue;
          }
      }      
      
    if(DeadFE == false){
      std::cout<<" one FE is presumably dead"<<std::endl;
      module_result->DeadFE = true;
    }
    else module_result->DeadFE = false;
    
    
    std::cout<<"missing hits "<<missingHits<<", dead pixel: "<<DeadPixel<<std::endl;
    
    // check if something goes wrong in first mask stage & give out result
    //if(_ChipType == PixGeometry::FEI2_CHIP || _ChipType == PixGeometry::FEI2_MODULE){
      if(module_result->MissingHitsMS1 <= (max_missing_hits_MS1/100)*missingHits) Passed_FirstMS = true;
      else{ Passed_FirstMS = false;}

     // check if all modules are working -> let scan pass or fail

      if((PixMissingHits + DeadPixel) <= max_bad_pixel && extraHits <= max_extra_hits_allowed)
	{
	  if(apply_cut_on_MS1 && !Passed_FirstMS){ 
	    Passed = false;
	  }else{
	    Passed = true;
	  }
	}else{
	Passed = false; 
      }
   /* }else if(_ChipType == PixGeometry::FEI4_CHIP || _ChipType == PixGeometry::FEI4_MODULE){
      Passed_FirstMS = true;
      Passed = true;
    }*/
    
    // if the scan is an analog scan with injected charge close to threshold set result to passed
    if(!_digitalInjection && _charge < min_charge)
      {
	Passed = true;
	SeenPercentage = AdditionalCalculations(i);
    std::cout<<"seen percentage"<<SeenPercentage<<std::endl;

	// decide wether the injected charge is below(3), near(2) or above(1) threshold
	if(SeenPercentage>95) ThrPosition = 1;
	else if(SeenPercentage<10) ThrPosition = 3;
	else ThrPosition = 2;
      }
    // if the scan is an analog scan with injected charge above threshold we can use the scan to identify modules with HV open
    else if(!_digitalInjection && _charge >= min_charge) {
      ThrPosition = 1;

      // criteria: (1) more than 5000 problematic pixel, (2) inefficient pixel + dead pixel > pixel with extra hits, (3) more than 5 extra hits, (4) no dead FE, 
      if(!(module_result->DeadFE) &&  PixMissingHits+DeadPixel+PixExtraHits > 5000 && (PixMissingHits + DeadPixel) > PixExtraHits && PixExtraHits > 5 ) 
      {HVopen = true;

      }

    }
      

    module_result->passed = Passed;
    module_result->Passed_FirstMaskstageTest = Passed_FirstMS;
    std::cout<<"Result for module "<<module_result->moduleName<<" is: "<<((module_result->passed)? "OK":"FAILED")<<std::endl;
    module_result->HVopen = HVopen;
    module_result->OverThreshold = ThrPosition;
    module_result->SeenPercentage = SeenPercentage;
    m_result->Add(module_result);
  } // modules


  if(m_logLevel>=CAN::kDiagnostics1Log) this->Print();
   //f.Close();
        
} 
 
float OCCUPANCYanalysisROOT::AdditionalCalculations(int module)
{
  std::cout<<"OCCUPANCYanalysisROOT::AdditionalCalculations: started"<<std::endl;
  
  // histogram with deviation from epected events
  std::shared_ptr<TH1F> histo = helper.IntegrateHisto(((AnalysisInputOCCUPANCY*)m_scan->At(module))->histo, 0, 1000, -1, -1, 0, _ChipFeFlav,_Nmasksdone,_Nmasks,_cp_enable, _fe_disable);
  
  float mean = histo->GetMean();
  std::cout<<"mean value of unseen hits: "<<mean<<" for module "<<module+1<<std::endl;
  
  float rms = histo->GetRMS();
  std::cout<<"RMS is "<<rms<<std::endl;
  
  float percentage = (mean/_Nevents)*100;

  return percentage;

}




 
void OCCUPANCYanalysisROOT::QualifyResults() 
{
  allModulesPassed = true;
  for(int i = 0; i<m_result->GetEntries(); i++)
    {
      ResultOCCUPANCY* module_result = (ResultOCCUPANCY*)m_result->At(i);
      if(!module_result->passed)
	{
	  allModulesPassed = false;
	  break;
	}
    }
  

  std::cout<<"OCCUPANCYanalysisROOT::QualifyResults():  "<<(allModulesPassed? "OK":"FAILED")<<std::endl;
  
  if(allModulesPassed) m_status= kPassed;
  else  m_status=kFailed;
  
}


void OCCUPANCYanalysisROOT::CheckForKnownProblems() //setting of failure bits
{

}


void OCCUPANCYanalysisROOT::Print()
{
  std::cout << std::endl;
  std::cout << "-------------------------------------" << std::endl;
  std::cout << "OCCUPANCY Scan Analysis Results: " << std::endl;
  std::cout << "-------------------------------------" << std::endl;
  std::cout << std::endl;

  std::cout << "-------------------------------------" << std::endl;
  std::cout << std::endl << std::endl;
}
