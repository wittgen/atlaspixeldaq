#ifndef _analysisInputTOT_h_
#define _analysisInputTOT_h_

#include "analysisInput_Base.h"
#include <vector>
#include <TH1F.h>
#include <TH2F.h>
#include <TString.h>
#include <iostream>

class AnalysisInputTOT : public AnalysisInput  { //this is for one scan and one module
 public:
  
  AnalysisInputTOT() :AnalysisInput() {};
  ~AnalysisInputTOT() {};
    
  TString moduleName;
  TString pp0Name;
  //TString connName;
  
  std::vector<const TH2*> histo_mean; 
  std::vector<const TH2*> histo_sigma; 
  std::vector<std::vector<float> > charge; // array of injected charges per frontend
  int n_maskstages;
  int n_events;
  int n_vcalsteps;
  int nTotMaskStages;

  // Helper functions
  void PrintInfo() {
    std::cout << "AnalysisInputTOT for pp0 " << pp0Name.Data() << ", module: " << moduleName.Data() << std::endl;
  };
  //ClassDef(AnalysisInputTOT,1);
};

#endif
