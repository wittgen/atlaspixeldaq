#include "JENSanalysisROOT.h"
#include <iostream>
#include <fstream>
#include "TFile.h"



JENSanalysisROOT::JENSanalysisROOT(): _NMSR(0)
{
  std::cout << std::endl;
  std::cout << "----------------------------" << std::endl;
  std::cout << "   JENS Analysis Algorithm   " << std::endl;
  std::cout << "----------------------------" << std::endl;
  std::cout << std::endl;
}



JENSanalysisROOT::~JENSanalysisROOT()
{
  //
}



void JENSanalysisROOT::Initialize()
{
  if(!m_scan) {
    std::cout << std::endl;
    std::cout << "JENSanalysisROOT::Initialize ERROR: input data pointer is null" << std::endl;
    std::cout << std::endl;
    return;
  } else {
    std::cout << "JDDBG: Input data pointer seems to exist..." << std::endl;
  }

  std::cout << "JDDBG: Reached Input handling of JENSanalysisROOT class" << std::endl;
  
  std::cout << "JDDBG: Number of entries in AnalysisInput: " << m_scan->GetEntries() << std::endl;

  AnalysisInputJENS *scan_iter = NULL;

  if (!(scan_iter = (AnalysisInputJENS*) m_scan->First())) return;

//  std::cout << "JDDBG: Got scan iterator, running PrintInfo()" << std::endl;
//  scan_iter->PrintInfo();

//  if(m_logLevel>=CAN::kDiagnostics3Log) {
    std::cout << "JDDBG: Loop names = " ;
    if(scan_iter->axis.size()>0) 
      std::cout << "  " << scan_iter->axis[0].variableName;
    else
      std::cout << "  " << "[none]";
    if(scan_iter->axis.size()>1) 
      std::cout << "  " << scan_iter->axis[1].variableName;
    else
      std::cout << "  " << "[none]";
    if(scan_iter->axis.size()>2) 
      std::cout << "  " << scan_iter->axis[2].variableName;
    else
      std::cout << "  " << "[none]";
    std::cout << std::endl;
//  }


  _NMSR = 0; _MSRVal.clear();


  // Set delay axis
  // -------------------------------
  _NMSR = scan_iter->axis[0].nSteps;
  int width = ( scan_iter->axis[0].lastBin-scan_iter->axis[0].firstBin)/
    ( scan_iter->axis[0].nSteps-1);
  for (int i=0;i<_NMSR;i++) 
    _MSRVal.push_back( (int)(scan_iter->axis[0].firstBin+i*width) );
}


void JENSanalysisROOT::Calculate() 
{  
  std::cout << "**************************************" << std::endl;
  std::cout << "JENSanalysis::Calculate  starting" << std::endl;

  if (m_scan->GetEntries()) {
    m_scan->R__FOR_EACH(AnalysisInputJENS,PrintInfo)(); 
    std::cout << "JENSanalysis: These were the module names." << std::endl;  

    this->FindBestMSR();
 
  
    for(Int_t i=0; i<m_scan->GetEntries(); i++) { 
      ResultJENS* module_result = new ResultJENS();
      module_result->pp0Name = ((AnalysisInputJENS*)m_scan->At(i))->pp0Name;
      module_result->moduleName = ((AnalysisInputJENS*)m_scan->At(i))->moduleName;
      module_result->idealMSR0 = _MSR[i][0];
      module_result->idealMSR1 = _MSR[i][1];
      module_result->minval0 = Minval[i][0];
      module_result->minval1 = Minval[i][1];
      module_result->passed=true;
      m_result->Add( module_result );
    }
  } else {
    std::cout << "You stink, which is why I won't calculate anything, go away!" << std::endl;
  }
} 


int JENSanalysisROOT::FindBestMSR()
{
  std::cout << "JDDBG: You're in FindBestMSR" << std::endl;
  
  if(m_scan->GetEntries()>7) {
    std::cout << "ERROR, pp0 with " << m_scan->GetEntries() << " modules" << std::endl;
    //return;
  }

  AnalysisInputJENS *scan_iter = (AnalysisInputJENS *)NULL;

  for(int ih=0; ih < (_NMSR > 0 ? _NMSR : 1); ih++) {
    _MSR[ih][0] = _MSR[ih][1] = 0x13;
    Minval[ih][0] = Minval[ih][1] = 10000;
  }
  
  for(Int_t i = 0; i < m_scan->GetEntries(); i++) {
    scan_iter = (AnalysisInputJENS *) m_scan->At(i); 
    std::cout << "JDDBG: Trying to run on module " << i << "... ";
    if(!(scan_iter->histo_Link1.size())) {
      std::cout << std::endl << "JDDBG: Histogram size for Link1 of mod " << i << " is zero, skipping...";
    } 
    if(!(scan_iter->histo_Link2.size())) {
      std::cout << std::endl << "JDDBG: Histogram size for Link2 of mod " << i << " is zero, skipping...";
    } 
    for (int im=1; im<=_NMSR; im++) {
      if(scan_iter->histo_Link1.size()) {
	int val1 = int(scan_iter->histo_Link1[0]->GetBinContent(im));
        std::cout << "At Link1 MSR: " << (im - 1) << " val is " << val1 << std::endl;
	if (val1 < Minval[i][0]) {
	  std::cout << "Minimum Value so far" << std::endl;
	  Minval[i][0] = val1;
	  _MSR[i][0] = (im - 1);
	}
      }

      if(scan_iter->histo_Link2.size()) {
	int val2 = int(scan_iter->histo_Link2[0]->GetBinContent(im));
        std::cout << "At Link2 MSR: " << (im - 1) << " val is " << val2 << std::endl;
	if (val2 < Minval[i][1]) {
	  std::cout << "Minimum Value so far" << std::endl;
	  Minval[i][1] = val2;
	  _MSR[i][1] = (im - 1);
	}
      }
    }
  }
  
  return 1;
}

 



void JENSanalysisROOT::QualifyResults() 
{
  m_status=kPassed;
}


void JENSanalysisROOT::CheckForKnownProblems() 
{

}

void JENSanalysisROOT::Print()
{
/*  std::cout << std::endl;
  std::cout << "-------------------------------------" << std::endl;
  std::cout << "BOC Scan Analysis Results: " << std::endl;
  std::cout << "-------------------------------------" << std::endl;
  std::cout << std::endl;

  if(_NViSet > 1) 
    std::cout << " o ViSet = " << _ViSetVal[_ViSet] << "mV" << "  (minimum EFR = " << _EFR << ")" << std::endl;
  std::cout << " o Threhsold, Delay = " << std::endl;
  for(int mod=0; mod<7; mod++) {
    for(int ilink=0; ilink<2; ilink++) {
      if(_Thr[mod][ilink]!=-1) 
	cout << "     Module #" << mod << ", Link #" << ilink+1 << "  ->  " 
	     << _ThresholdVal[_Thr[mod][ilink]] << ", " << _Delay[mod][ilink]
	     << " ns" << std::endl;
    }
  } 
  std::cout << "-------------------------------------" << std::endl;
  std::cout << std::endl << std::endl;*/
}


