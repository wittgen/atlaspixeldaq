#include "ANNAanalysisROOT.h"
#include <iostream>
#include <fstream>
#include "TFile.h"



ANNAanalysisROOT::ANNAanalysisROOT(): _Nmasks(0), _Nevents(0), _Ncol(0), _Nrow(0)
{
  std::cout << std::endl;
  std::cout << "-------------------------------" << std::endl;
  std::cout << "   ANNA Analysis Algorithm   " << std::endl;
  std::cout << "-------------------------------" << std::endl;
  std::cout << std::endl;
}



ANNAanalysisROOT::~ANNAanalysisROOT()
{
  //
}





void ANNAanalysisROOT::Initialize()
{
  //debugging
  std::cout<<"ANNAanalysisROOT::Initialize()"<<std::endl;
  if(!m_scan) {
    std::cout << std::endl;
    std::cout << "ANNAanalysisROOT::Initialize ERROR: input data pointer is null" << std::endl;
    std::cout << std::endl;
    return;
  }

  AnalysisInputANNA* occupancy = (AnalysisInputANNA*) m_scan->At(0);
  // skip if there are no histograms
  if(!occupancy) return;
  _Nrow = occupancy->histo->GetNbinsY();
  _Ncol = occupancy->histo->GetNbinsX();
 
  _Nmasks  = occupancy->Nmasks;
  _Nevents = occupancy->Nevents;

  // fill pattern
  //-----------------------
  int nPixels = 0;
  for(int col = 1; col <= _Ncol; col++) {
    for(int row = 1; row <= _Nrow; row++) {
      _pattern[col-1][row-1] = 0;
    }
  }
  

  //odd columns
  for(int h = 1; h <= _Nrow; h = h+32) {
    for(int j = 1; j <= _Ncol; j = j+2) {
      for(int m = 0; m < _Nmasks; m++) {
	
	int col = j;
	int row = h+m;
	
	_pattern[col-1][row-1] = _Nevents;
	nPixels++;

      }  
    }
  }

  //even columns
  for(int h = 32; h <= _Nrow; h = h+32) {
    for(int j = 2; j <= _Ncol; j = j+2) {
      for(int m = 0; m < _Nmasks; m++) {
	
	int col = j;
	int row = h-m;
	if(row<1) row += _Nrow;

	_pattern[col-1][row-1] = _Nevents;
	nPixels++;

      }  
    }
  }
  
  std::cout << "Number of masks   = " << _Nmasks << std::endl;
  std::cout << "Number of Events  = " << _Nevents << std::endl;
  std::cout << "Number of modules = " << m_scan->GetEntries() << std::endl;
  std::cout << std::endl;
  std::cout << "Number of active pixels = " << nPixels << std::endl;
  std::cout << std::endl;

} 
  
  
 

void ANNAanalysisROOT::Calculate() 
{
  //TFile f("test.root","recreate");
  
  //debugging during night times ...
  std::cout<<"ANNAanalysisROOT::Calculate()::started!"<<std::endl;

 
  if(m_scan->GetEntries() == 0) return;
  if(m_scan->GetEntries()>7) {
    std::cout << "ERROR, pp0 with " << m_scan->GetEntries() << " modules" << std::endl;
    //return;
  }

   if(m_logLevel>=CAN::kDiagnostics1Log) std::cout << "Number of Modules = " << m_scan->GetEntries() << std::endl;
  for(Int_t i = 0; i < m_scan->GetEntries(); i++) {
    
    // if(m_logLevel>=CAN::kDiagnostics2Log) 
      std::cout << " Module #" << i << std::endl;
            
    AnalysisInputANNA* occupancy = (AnalysisInputANNA*) m_scan->At(i);
   
    
      // an array to hold the deviations, initialize it for each module
    int devPattern[144][320];
    for(int col = 1; col<= _Ncol; col++)
      {
	for(int row = 1; row<= _Nrow; row++)
	  {
	    devPattern[col-1][row-1] = 0;
	  }
      }
    
    // some variables 
    int extraHits = 0;
    int maxExtraHits = 0;
    int missingHits = 0;
    int maxMissingHits = 0;
    int PixMissingHits = 0;
    int PixExtraHits = 0;
    int PixBadMissing = 0;
    int PixBadExtra = 0;
    int maskMissingHits = 0;
    int maskExtraHits = 0;
    int maskPixExtraHits = 0;
    int maskPixMissingHits = 0;

    //char name[10];
    //sprintf(name,"scan%i",i);
    //occupancy->histo->SetName(name);
    //occupancy->histo->Write();
   
    for(int col = 1; col <= _Ncol; col++) {
      for(int row = 1; row <= _Nrow; row++) {
	int expected = _pattern[col-1][row-1];
	int observed = occupancy->histo->GetBinContent(col,row);
	int error = observed - expected;
	// fill error pattern
	devPattern[col-1][row-1] = error;
	if(m_logLevel>=CAN::kDiagnostics3Log && i==0) std::cout << col << ", " << row << "    " << expected << " - " << observed << "; " << error << std::endl;
	if(observed<expected)
	  {
	    missingHits -= error;
	    PixMissingHits++;
	    if(abs(error) > maxMissingHits) maxMissingHits = error;
	    // counts the number of pixels with more than 10% deviation from expected value
	    if(abs(error) > 0.1*expected) PixBadMissing++;
	  }
	if(observed>expected) 
	  {
	    extraHits   += error;
	    PixExtraHits++;
	    if(error > maxExtraHits) maxExtraHits = error;
	    //counts the number of pixels with more than 10% deviation from expected value;
	    if(error > 0.1*expected ) PixBadExtra++;
	  }
	if(m_logLevel>=CAN::kDiagnostics4Log) {
	  if(observed!=expected)
	    std::cout <<  col << ", " << row << "    " << expected << " - " << observed << "; " << error << std::endl;
	}
      }
    }

    // if(m_logLevel>=CAN::kDiagnostics1Log) {
    std::cout << "   Number of missing hits = " << missingHits << std::endl;
    std::cout << "   Number of Pixels with missing hits = " << PixMissingHits << std::endl;
    std::cout << "   Number of Pixels with missing hits and more than 10% deviation from expected hits = " <<PixBadMissing << std::endl;
    std::cout << "   Maximum of missing hits = " << maxMissingHits << std::endl;
    std::cout << "   Number of extra hits = " << extraHits << std::endl;
    std::cout << "   Number of Pixels with extra hits = " << PixExtraHits << std::endl;
    std::cout << "   Number of Pixels with extra hits and more than 10% deviation from expected hits = " <<PixBadExtra << std::endl;
    std::cout << "   Maximum of extra hits = " << maxExtraHits << std::endl; 
    std::cout << std::endl;
    
    // loop over maskstages to get values per maskstage
    for(int m = 0; m < _Nmasks; m++)
      {
	// reinitialize for each maskstage
	maskMissingHits = 0;
	maskExtraHits = 0;
	maskPixMissingHits = 0;
	maskPixExtraHits = 0;
	
	// get the entries corresponding to this maskstage
	// odd columns first
	for(int h = 1; h <= _Nrow; h = h+32) {
	  for(int j = 1; j <= _Ncol; j = j+2) {
	    
	    int err = devPattern[j-1][h+m-1];
	    if(err>0) 
	      {
		maskExtraHits+=err;
		maskPixExtraHits++;
	      }
	    if(err<0)
	      {
		maskMissingHits-=err;
		maskPixMissingHits++;
	      }
	  }
	}
	// even columns
	for(int h = 32; h <= _Nrow; h = h+32) {
	  for(int j = 2; j <= _Ncol; j = j+2) {
	    
	    int row = h-m;
	    if(row<1) row += _Nrow;
	    int err  = devPattern[j-1][row-1];
	    if(err>0)
	      {
		maskExtraHits+=err;
		maskPixExtraHits++;
	      }
	    if(err<0) 
	      {
		maskMissingHits-=err;
		maskPixMissingHits++;
	      }
	  }
	}


	cout << "Maskstage "<<m+1<< " shows "<<abs(maskMissingHits)<<" missing hits in "<<maskPixMissingHits <<" pixels and "<<maskExtraHits<<" extra hits in "<<maskPixExtraHits<<" pixels." <<endl;
	//}
      
      }
    
    ResultANNA* module_result = new ResultANNA();
    module_result->pp0Name = ((AnalysisInputANNA*)m_scan->At(i))->pp0Name;
    module_result->moduleName = ((AnalysisInputANNA*)m_scan->At(i))->moduleName;
    module_result->PixExtraHits = PixExtraHits;
    module_result->PixMissingHits = PixMissingHits;
    module_result->MaxExtraHits = maxExtraHits;
    module_result->MaxMissingHits = maxMissingHits;
    


    m_result->Add(module_result);

    // check if all modules are working -> let scan pass or fail
    if(missingHits == 0 && extraHits == 0) 
      {
	allModulesPassed = true;
	cout<<"ANNAanalysis should have worked for this module"<<endl;
      }
    else allModulesPassed = false;

    module_result->ModStatus = allModulesPassed;
    
  } // modules
  
  if(m_logLevel>=CAN::kDiagnostics1Log) this->Print();

  //f.Close();
} 
 
 
void ANNAanalysisROOT::QualifyResults() 
{
  cout<<"ANNAanalysisROOT::QualifyResults():  "<<allModulesPassed<<endl;
  

  if(allModulesPassed) m_status=kPassed;
  else  m_status=kFailed;
  
}


void ANNAanalysisROOT::CheckForKnownProblems() //setting of failure bits
{

}


void ANNAanalysisROOT::Print()
{
  std::cout << std::endl;
  std::cout << "-------------------------------------" << std::endl;
  std::cout << "ANNA Scan Analysis Results: " << std::endl;
  std::cout << "-------------------------------------" << std::endl;
  std::cout << std::endl;

  std::cout << "-------------------------------------" << std::endl;
  std::cout << std::endl << std::endl;
}
