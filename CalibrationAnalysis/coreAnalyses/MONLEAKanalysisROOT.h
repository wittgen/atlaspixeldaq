#ifndef _MONLEAKanalysisROOT_h_
#define _MONLEAKanalysisROOT_h_ _

#include "ROOTanalysisBase.h"
#include "analysisInput_MONLEAK.h"
#include "analysisOutput_MONLEAK.h"
#include <iostream>
#include <TFile.h>
#include <TString.h>


class MONLEAKanalysisROOT : public ROOTanalysisBase 
{
 public:
  MONLEAKanalysisROOT();
  ~MONLEAKanalysisROOT();
    
  void Initialize();

  void Calculate(); //internal calculations

  void QualifyResults(); //apply cuts from analysis config

  void CheckForKnownProblems(); //setting of failure bits

  
 private:
  void WriteResults();
  void AdditionalCalculations();
  //tests
  //  void CheckForZeroMean(ResultMONLEAK* module_result);
  void findBadPixels(ResultMONLEAK* module, int ientry, std::vector<unsigned short int> FE_disable); 

  std::vector<unsigned short int> FE_disable;
  //  Int_t num_bad_pixels[16];

  int _Ncol;
  int _Nrow;
  int _Nfe;
  int _ChipType;
};


#endif

