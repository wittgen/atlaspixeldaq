#ifndef _BONDanalysisROOT_h_
#define _BONDanalysisROOT_h_ _

#include "ROOTanalysisBase.h"
#include "analysisInput_BOND.h"
#include "analysisOutput_BOND.h"
#include <iostream>
#include <TFile.h>
#include <TString.h>



class BONDanalysisROOT : public ROOTanalysisBase 
{
 public:
  BONDanalysisROOT();
  ~BONDanalysisROOT() = default;
    
  void Initialize();

  void Calculate(); //internal calculations

  void QualifyResults(); //apply cuts from analysis config

  //Pure virtual method from ROOTanalysisBase, needs to be implemented
  void CheckForKnownProblems() {};
  
  //The cuts are directly accessible from the AutomaticAnalysis
  int cut_delta_min; //delta between noise of two thresholds (on/off)

 private:
    std::vector<unsigned short int> FE_disable;
    std::vector<std::vector<unsigned short int> > _cp_enable;
    unsigned int _Nmasks;
    //unsigned int _Nevents;
    unsigned int _Nmask_stage;
    unsigned int chipType;
    unsigned int Ncol;
    unsigned int Nrow;

  void WriteResults();
  std::shared_ptr<TH1F> CheckDeltas(const TH2* HVoff, const TH2* HVon, ResultBOND* module); 
};


#endif

