#ifndef _analysisOutputInLink_h_
#define _analysisOutputInLink_h_

#include <TString.h>
#include <TH2F.h>
#include "analysisOutput_Base.h"
#include "PixelCoralClientUtils/PixelMap.h"


class ResultInLink : public AnalysisResult { //this is for one scan and one module
 public:
  ResultInLink() :AnalysisResult(), 
    passed_I_pin_mod_min(false)
    //upper_threshold(0),lower_threshold(0),
    //passed_noise_max(false), passed_threshold_min(false), passed_threshold_max(false), 
    //passed_threshold_mean_min(false), passed_threshold_mean_max(false),
    //passed_threshold_RMS_max(false),
    //percentFailed_threshold_min(0), percentFailed_threshold_max(0), 
    //numBadPixels(0)
    {
      I_pin_mod=0;
      //average_threshold=0;
      //average_noise[0]=average_noise[1]=average_noise[2]=0;
      //average_chi2=0;

      //RMS_threshold=0;
      //RMS_noise[0]=RMS_noise[1]=RMS_noise[2]=0;
      //RMS_chi2=0;

      //percentFailed_noise_max[0]=percentFailed_noise_max[1]=percentFailed_noise_max[2]=0;
    };
  ~ResultInLink() {};

   Float_t I_pin_mod;
   
//  Float_t average_threshold; 
//  Float_t average_chi2;
//  Float_t average_noise[3]; //for 3 different classes of pixel types

//  Float_t upper_threshold; // lowest vaule of upper x% bin
//  Float_t lower_threshold; //highest value of lower x% bin

//  Float_t RMS_threshold; 
//  Float_t RMS_chi2;
//  Float_t RMS_noise[3]; //for 3 different classes of pixel types

  bool passed_I_pin_mod_min;
//  bool passed_noise_max;  
//  bool passed_threshold_min;
//  bool passed_threshold_max; 
//  bool passed_threshold_mean_min;
//  bool passed_threshold_mean_max; 
//  bool passed_threshold_RMS_max; 

//  Float_t percentFailed_iolink_ratio_bit_min;
//  Float_t percentFailed_noise_max[3];  
//  Float_t percentFailed_threshold_min;
//  Float_t percentFailed_threshold_max; 

  TH1F hInt_chi2; 
  TH1F hInt_mean; 
  TH1F hInt_noise[3]; //for 3 different classes of pixel types

  TH2F pixelmap_normal;
  TH2F pixelmap_long;
  TH2F pixelmap_ganged;
  TH2F pixelmap_interganged;
  TH2F pixelmap_longganged;
  TH2F pixelmap_longinterganged;
  TH2F pixelmap_dead; // <--
  TH2F pixelmap_noisy;

//  unsigned int numBadPixels; //[6]: for 6 different pixel types ?
//  PixelMap_t badPixels;

  /////////////////////
  TH1F AvPerFE_chi2; 
  TH1F AvPerFE_mean; 
  TH1F AvPerFE_noise;

 public:
  //ClassDef(ResultInLink,1) //not 0 -> activate I/O  ; not needed at the moemnt, put in if needed!
};

#endif
