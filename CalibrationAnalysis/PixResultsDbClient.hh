#ifndef _PixResultsDbClient_hh_
#define _PixResultsDbClient_hh_

#include "IPixResultsDbClient.hh"
#include <PixelCoralClientUtils/IPixResultsDbListener.hh>
#include <RelationalAccess/AccessMode.h>

class PixResultsDbClient
{
public:
  static IPixResultsDbClient *createClient(const std::string &connection_string, bool verbose=false, coral::AccessMode access_mode = coral::Update);
  static IPixResultsDbClient *createClient(bool verbose=false, coral::AccessMode access_mode = coral::Update);
};

#endif
