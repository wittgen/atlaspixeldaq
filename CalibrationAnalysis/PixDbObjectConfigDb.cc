#include <memory>

#include "PixDbObjectConfigDb.hh"
#include "ObjectConfigDbInstance.hh"
#include "PixDbGlobalMutex.hh"
#include <ConfigWrapper/DbManager.h>
#include "AnalysisInfo_t.hh"

namespace CAN {

  PixDbObjectConfigDb::PixDbObjectConfigDb() 
  {
    Lock globalLock(PixDbGlobalMutex::mutex());
    try {
      m_configDb=std::unique_ptr<IConfigDb>(ObjectConfigDbInstance::create() );
    }
    catch ( CAN::MissingConfig &err) {
//       throw MRSException("CFGDB_no_can_conf_db", MRS_FATAL, "CAN::PixDbObjectConfigDb::ctor",
// 			 err.getDescriptor() );
      throw MRSException(PixLib::PixMessages::FATAL, "CAN::PixDbObjectConfigDb::ctor", err.getDescriptor() );
    }

  }

    void PixDbObjectConfigDb::readConfig(const ObjectInfo_t &object_info, PixLib::Config &config, std::string string)
  {
     // std::cout<<"non mi chiedermi di avere"<<std::endl;
    Lock globalLock(PixDbGlobalMutex::mutex());
    try {
      config.reset();
      std::string file_name = m_configDb->configFileName(object_info.name(), object_info.tag(), object_info.revision() );

      PixA::DbHandle db_handle = PixA::DbManager::dbHandle_1(file_name,string);
      PixA::DbRef db_ref = db_handle.ref();
      config.read( db_ref.record().get() );

    }
    catch ( CAN::MissingConfig &err) {
//       throw MRSException("CFGDB_no_can_config", MRS_FATAL, "CAN::PixDbObjectConfigDb::readConfig",
// 			 err.getDescriptor() );
      throw MRSException(PixLib::PixMessages::FATAL, "CAN::PixDbObjectConfigDb::readConfig", err.getDescriptor() );
    }
  }

}
