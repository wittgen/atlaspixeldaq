#include "AnalysisRequestProcessor.hh"

//#include <mrs/message.h>
#include "sendMessage.hh"
#include "PixUtilities/PixMessages.h"
#include "exception.hh"

#include "IExecutionUnit.hh"
#include "IsDictionary.hh"
#include "AnalysisFactory.hh"
#include "AnalysisPerX.hh"

#include "MetaDataService.hh"
#include <ConfigWrapper/ConnectivityManager.h>

#include "PixDbObjectConfigDb.hh"
#include "PixDbGlobalMutex.hh"

#include "isUtil.hh"

namespace CAN {

  std::string AnalysisRequestProcessor::s_componentAnaReqProcName( "ANAREQPROC" );

  AnalysisRequestProcessor::AnalysisRequestProcessor(//IPCPartition &mrs_partition,
						     IsDictionary &dictionary,
						     const std::string &can_is_server_name,
						     IExecutionUnit &execution_unit)
    : m_shutdown(false),
      m_db(new PixDbObjectConfigDb),
      //      m_out(new MRSStream(mrs_partition)),
      m_isDictionary(&dictionary),
      m_canIsServerName(can_is_server_name),
      m_executionUnit(&execution_unit),
      m_currentAnalysis(0),
      m_logLevel(kWarnLog),
      m_abortRequest(false)
  {
    start_undetached();
  }

  void AnalysisRequestProcessor::queueRequest(SerialNumber_t    analysis_serial_number,
					      const AnalysisInfoData_t analysis_info,
					      const StringVec &rod_name_list)
  {
    Request_t *new_request = new Request_t(analysis_serial_number, analysis_info);
    new_request->reserve(rod_name_list.length() );

    //    if(false) {

    // is dictionary is shared between threads
    // but is is not thread safe, so locking is needed.
    {
      Lock dictionary_lock(m_isDictionary->mutex());
      ISType test;
      for (std::size_t rod_i=0; rod_i < rod_name_list.length(); rod_i++) {
//          std::cout<<"the iterator is: "<<rod_i<<std::endl;
 	Joblett_t &new_job = new_request->createJob(analysis_serial_number,std::string(rod_name_list[rod_i]),m_canIsServerName);
	// publish initial status
	test=new_job.isStatusType();
	setIsValue(m_isDictionary->dictionary(),test,new_job.isDictionaryName(),new_job.isStatus());
      }
    }

    //  }

    {
      Lock queue_lock(m_queueMutex);
      m_requestQueue.push_back( new_request );
    }
    m_queueFilled.setFlag();
  }

  void *AnalysisRequestProcessor::run_undetached(void *arg)
  {
      std::cout<<"everytime you call me"<<std::endl;
    // thread should not be started with arguments
    assert (arg==NULL);
      std::cout<<"call process request"<<std::endl;
    processRequests();
      std::cout<<"end process request"<<std::endl;
    m_exitProcessor.setFlag();
      std::cout<<"here run undetached for requestprocessor ends"<<std::endl;
    return NULL;
  }

  void AnalysisRequestProcessor::processRequests()
  {
//      std::cout<<"processRequests is starting"<<std::endl;
      
    if (m_logLevel>=kInfoLog) {
//       Lock mrs_lock(m_mrsMutex);
//       sendMessage(*m_out,"ANAREQPROC_init",s_componentAnaReqProcName,MRS_INFORMATION, "AnalysisRequestProcessor::processRequests", "Start request processing.");
      ers::info(PixLib::pix::daq (ERS_HERE, "AnalysisRequestProcessor::processRequests", "Start request processing."));
    }
//std::cout<<"is it called here? 1"<<std::endl;
    for(;!m_shutdown;) {
      if (m_requestQueue.empty()) {
	m_queueFilled.wait(m_shutdown);
	if (m_shutdown) break;
      }
//        std::cout<<"is it called here? 2"<<std::endl;

      std::unique_ptr<Request_t> analysis_request;
      {
	Lock analysis_lock(m_currentAnalysisMutex);
//          std::cout<<"is it called here? 3"<<std::endl;

	Lock queue_lock(m_queueMutex);
	if (m_requestQueue.empty()) continue;
	analysis_request  = std::unique_ptr<Request_t> ( m_requestQueue.front() );
	m_requestQueue.pop_front();
//          std::cout<<"is it called here? 4"<<std::endl;

	m_currentAnalysis = analysis_request->serialNumber();
      }

      bool submission_error=true;
      try {
//          std::cout<<"is it called here? 5"<<std::endl;

	if (m_logLevel>=kInfoLog) {
	  std::stringstream message;
	  message << "Process analysis request A" << std::setw(9) << std::setfill('0') << analysis_request->serialNumber();
	  if (m_logLevel>=kDiagnosticsLog) {
	    message << " for RODs :";
	    for (JoblettList_t::const_iterator rod_iter = analysis_request->beginJoblett();
		 rod_iter != analysis_request->endJoblett();
		 rod_iter++) {
//            std::cout<<"is it called here? 6"<<std::endl;

	      message << " " << rod_iter->first;
	    }
	    message << ".";
	  }
	  else {
	    message << analysis_request->nJobletts() << " ROD(s).";
 //         std::cout<<"is it called here? 7"<<std::endl;

	  }
	  {
// 	    Lock mrs_lock(m_mrsMutex);
// 	    sendMessage(*m_out,"ANAREQPROC_processRequests",s_componentAnaReqProcName,MRS_INFORMATION, "CAN::AnalysisRequestProcessor::processRequests", message.str() );
	    ers::info(PixLib::pix::daq (ERS_HERE, "CAN::AnalysisRequestProcessor::processRequests", message.str() ));
	  }
	}

	if (checkAbort()) abortException( analysis_request->serialNumber() );
//          std::cout<<"is it called here? 8"<<std::endl;


	const AnalysisInfo_t &analysis_info = analysis_request->analysisInfo();
	//analysis_info.Print();
//          std::cout<<"number of the scan is: "<<analysis_info.scanSerialNumber()<<std::endl;
	InfoPtr_t<PixLib::ScanInfo_t> scan_info;
	scan_info = InfoPtr_t<PixLib::ScanInfo_t>( MetaDataService::instance()->getScanInfo(analysis_info.scanSerialNumber()) );

// std::cout<<"random output 1"<<std::endl;
	// Also need to load the disable lists before the analyses request histograms

	// for simplicity a vector is constructed which has the same ordering as the list of source serial numbers
	// Each element of this vector which corresponds to a scan (unlike an analysis), gets filled with
	// a disabled root list.

	std::vector< PixA::DisabledListRoot > disabled_lists;
	disabled_lists.reserve( analysis_info.scanSerialNumbers().size() );
//   std::cout<<"random output 2"<<std::endl;

	PixA::ConnectivityRef conn;
        {
// std::cout<<"random output 3"<<std::endl;
	  // Get the connectivity  from the FIRST scan
	  // the scheduler does some verification whether the connectivities of the different sources match.
	  if (m_logLevel>=kDiagnosticsLog) {
	    std::stringstream message;
	    message << "Query connectivity : "
		    << scan_info->connTag();
	    if (scan_info->aliasTag() != scan_info->connTag() && scan_info->dataTag() != scan_info->connTag() ) {
	      message << " " << scan_info->aliasTag() << " " << scan_info->dataTag();
	    }
	    message << " " << scan_info->cfgTag() << " / " << scan_info->cfgRev();
	    if (scan_info->cfgTag() != scan_info->modCfgTag()) {
	      message << " " << scan_info->modCfgTag() << " / " << scan_info->modCfgRev();
	    }
// 	    Lock mrs_lock(m_mrsMutex);
// 	    sendMessage(*m_out,"ANAREQPROC_processRequests",s_componentAnaReqProcName,MRS_DIAGNOSTIC, "CAN::AnalysisRequestProcessor::processRequests", message.str() );
	    ers::debug(PixLib::pix::daq (ERS_HERE, "CAN::AnalysisRequestProcessor::processRequests", message.str() ));
	  }
            
//            std::cout<<"random output 4"<<std::endl;
     
	  Lock globalLock(PixDbGlobalMutex::mutex());
	  conn =PixA::ConnectivityManager::getConnectivity( scan_info->idTag(),
							    scan_info->connTag(),
							    scan_info->aliasTag(),
							    scan_info->dataTag(),
							    scan_info->cfgTag(),
							    scan_info->modCfgTag(),
							    scan_info->cfgRev());
//            std::cout<<"random output 5"<<std::endl;

	  if (checkAbort()) abortException( analysis_request->serialNumber() );

	  if (conn) {

	    {
//            std::cout<<"random output 6"<<std::endl;

	      for (std::map<std::string, std::pair<ESerialNumberType, SerialNumber_t> >::const_iterator source_iter = analysis_info.beginSource();
		   source_iter != analysis_info.endSource();
		   ++source_iter) {
//              std::cout<<"random output 7"<<std::endl;

		disabled_lists.push_back( PixA::DisabledListRoot() );

		// @todo also get disable information for analyses
		if (source_iter->second.first == kScanNumber) {
//            std::cout<<"random output 8"<<std::endl;

		  InfoPtr_t<PixLib::ScanInfo_t> a_scan_info;
		  if (source_iter->second.second == analysis_info.scanSerialNumber()) {
		    a_scan_info=scan_info;
		  }
		  else {
		    a_scan_info = InfoPtr_t<PixLib::ScanInfo_t>( MetaDataService::instance()->getScanInfo( source_iter->second.second) );
		  }

		  try {
//              std::cout<<"random output 9"<<std::endl;

		    if (conn && a_scan_info->pixDisableString().empty()) {
		      std::stringstream disableName;
		      disableName << "Disable-S" << std::setw(9) << std::setfill('0') << source_iter->second.second;
		      disabled_lists.back() = PixA::DisabledListRoot( conn.getDisable(disableName.str()) );
		    }
		    else {
		      if (m_logLevel>=kDiagnostics5Log) {
			std::stringstream message;
			message << "Take enable information from meta data for " << makeScanSerialNumberString(source_iter->second.second) << ".";
// 			Lock mrs_lock(m_mrsMutex);
// 			sendMessage(*m_out,"ANAREQPROC_scan_disable",s_componentAnaReqProcName, MRS_DIAGNOSTIC,
// 				    "CAN::AnalysisRequestProcessor::processRequests", message.str());
			ers::debug(PixLib::pix::daq (ERS_HERE,
				    "CAN::AnalysisRequestProcessor::processRequests", message.str()));
		      }
		      disabled_lists.back() = PixA::createEnableMap(a_scan_info->pixDisableString());
		    }
//              std::cout<<"random output 10"<<std::endl;
 
		  }
		  catch(...) {
		    if (m_logLevel>=kErrorLog) {
		      std::stringstream message;
		      message << "Failed to get scan disable list for " << makeScanSerialNumberString(source_iter->second.second) << ".";
// 		      Lock mrs_lock(m_mrsMutex);
// 		      sendMessage(*m_out,"ANAREQPROC_no_scan_disable",MRS_ERROR, "CAN::AnalysisRequestProcessor::processRequests", message.str());
		      ers::error(PixLib::pix::daq (ERS_HERE, "CAN::AnalysisRequestProcessor::processRequests", message.str()));
		    }
		  }

		}
	      }
	    }

	  }

        }
//          std::cout<<"random output 11"<<std::endl;
          
	if (checkAbort()) abortException( analysis_request->serialNumber() );

	// create a per-ROD-analysis object
	// EObjectType types[kNObjectTypes]={kAlg,kClassification, kPostProcessing};
          
          int numberofrods=0;
    for (JoblettList_t::const_iterator rod_iter = analysis_request->beginJoblett();
               rod_iter != analysis_request->endJoblett();
               rod_iter++) {numberofrods++;}
          
    std::vector<std::string> stringa(numberofrods);
    int count=0;
          std::unique_ptr<IAnalysisObject> obj[kNObjectTypes];
          for (JoblettList_t::iterator rod_iter = analysis_request->beginJoblett();
               rod_iter != analysis_request->endJoblett();
               rod_iter++) {
              
//              std::cout<<"rod name could be"<< rod_iter->first <<std::endl;
          
          
	
	for (unsigned int obj_i=0; obj_i<kNObjectTypes; obj_i++) {
//	  std::cout<<"e invece sono io"<<std::endl;
        
        
        
//        rod_iter = analysis_request->beginJoblett();
        std::string rod_name = rod_iter->first;
        const PixA::Pp0List &pp0s=conn.pp0s(rod_name);
        PixA::Pp0List::const_iterator pp0_iter=pp0s.begin();
        PixA::ModuleList::const_iterator modules_begin=PixA::modulesBegin(pp0_iter);
        PixA::ModuleList::const_iterator module_iter=modules_begin;
        stringa[count]=conn.modConf_name(module_iter);
        
//        std::cout<<"stringa is:" << stringa <<std::endl;
        
	  obj[obj_i] =  std::unique_ptr<IAnalysisObject>(AnalysisFactory::get()->create(analysis_request->serialNumber(),
										      analysis_request->analysisInfo().objectInfo(static_cast<EObjectType>(obj_i)), 
										      m_db.get(), stringa[count]));
	}
          
//          std::cout<<"processRequests is at the end? 1"<<std::endl;

	IAnalysisPerROD    *rod_analysis = dynamic_cast<IAnalysisPerROD *>(obj[kAlg].get());

	if (!rod_analysis) {
	  IAnalysisPerPP0    *pp0_analysis = dynamic_cast<IAnalysisPerPP0 *>(obj[kAlg].get());
	  if (!pp0_analysis) {
	    IAnalysisPerModule *mod_analysis = dynamic_cast<IAnalysisPerModule *>(obj[kAlg].get());
	    if (!mod_analysis) {

	      std::string message(" The analysis " );
	      message += analysis_info.name(kAlg) ;
	      message += " is not of type IAnalysisPerROD, IAnalysisPerPP0, IAnalysisPerModule .";
	      //	      throw MRSException("WRONGANALYSISBASE", MRS_FATAL, "CAN::AnalysisRequestProcessor::processRequests",message);
	      throw MRSException(PixLib::PixMessages::FATAL, "CAN::AnalysisRequestProcessor::processRequests",message);

	    }
	    pp0_analysis=new AnalysisPerPP0(*mod_analysis);
	  }
	  rod_analysis=new AnalysisPerROD(*pp0_analysis);
	}
          
//          std::cout<<"processRequests is at the end? 2"<<std::endl;


	IClassification    *classification = dynamic_cast<IClassification *>(obj[kClassification].get());
	if (!classification) {
	  std::string message(" The classification object " );
	  message += analysis_info.name(kClassification) ;
	  message += " is not of type IClassification .";
// 	  throw MRSException("WRONGANALYSISBASE", MRS_FATAL, "CAN::AnalysisRequestProcessor::processRequests",message);
	  throw MRSException(PixLib::PixMessages::FATAL, "CAN::AnalysisRequestProcessor::processRequests",message);
	}
          
//          std::cout<<"processRequests is at the end? 3"<<std::endl;


	IPostProcessing    *post_processing = dynamic_cast<IPostProcessing *>(obj[kPostProcessing].get());
	if (!post_processing) {
	  std::string message(" The post-processing object " );
	  message += analysis_info.name(kPostProcessing) ;
	  message += " is not of type IPostProcessing .";
	  //	  throw MRSException("WRONGANALYSISBASE", MRS_FATAL, "CAN::AnalysisRequestProcessor::processRequests",message);
	  throw MRSException(PixLib::PixMessages::FATAL, "CAN::AnalysisRequestProcessor::processRequests",message);
	}
 //         std::cout<<"processRequests is at the end? 4"<<std::endl;

//	{

	  // hand ownership of the analysis objects to the job object list.
	  for (unsigned int obj_i=0; obj_i<kNObjectTypes;obj_i++) {
	    if (obj[obj_i].get()) {
	      obj[obj_i]->setLogLevel(m_logLevel);
	    }
	    obj[obj_i].release();
	  }
        
	  std::shared_ptr<JobObjects_t> objects_sptr(new JobObjects_t(rod_analysis,classification,post_processing));

//	  for (JoblettList_t::iterator joblett_iter = analysis_request->beginJoblett();
//	       joblett_iter != analysis_request->endJoblett();
//	       joblett_iter++) {
          JoblettList_t::iterator joblett_iter =rod_iter;
          joblett_iter->second->setObjects(objects_sptr);
          count++;
	  }

	  if (checkAbort()) abortException( analysis_request->serialNumber() );

          submission_error=false;
std::cout<<"AnalysisRequestProcessor: analysisrequest serial number "<<analysis_request->serialNumber()<<std::endl;
	  m_executionUnit->queueJob(analysis_request->serialNumber(),
				    analysis_info.scanSerialNumbers(),//CLA
				    scan_info,
				    analysis_info.srcAnalysisSerialNumber(),
				    conn,
				    disabled_lists,
				    analysis_request->beginJoblett(),
				    analysis_request->endJoblett());

	  {
	    Lock lock(m_currentAnalysisMutex);
	    if (checkAbort()) {
	      m_executionUnit->abortAnalysis(analysis_request->serialNumber(),true);
	    }
	    m_currentAnalysis=0;
	  }

	  //Lock lock(m_analysisJobListMutex);
	  //const StringVec &rod_name_list = analysis_request->rodNameList(); // CORBA string sequence
// 	  for (std::size_t rod_i=0; rod_i < rod_name_list.length(); rod_i++) {
// 	    std::cout << "now requesting histograms for " << rod_name_list[rod_i] << std::endl;//CLA
// 	    rod_analysis->requestHistograms(m_dataServer, rod_name_list[rod_i]);
// 	    m_analysisJobList[analysis_serial_number].insert(std::make_pair<PixA::RodLocation, ExecutionUnit::Joblett_t >(rod_name_list[rod_i],ExecutionUnit::Joblett_t(objects_sptr)));
// 	    m_executionUnit->queueJob(rod_name_list[rod_i],ExecutionUnit::Joblett_t(objects_sptr));
// 	  }
	//}
          
      }
      catch(PixA::ConfigException &err) {
          std::cout<<"catch1"<<std::endl;
	if (m_logLevel>=kFatalLog) {
	  std::string message(" Caught exception : " );
	  message += err.getDescriptor() ;
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"config_except",s_componentAnaReqProcName,MRS_FATAL, "CAN::AnalysisRequestProcessor::processRequests",message);
	  ers::fatal(PixLib::pix::daq (ERS_HERE, "CAN::AnalysisRequestProcessor::processRequests",message));
	}
      }
      catch(MRSException &err) {
          std::cout<<"catch2"<<std::endl;
	if (m_logLevel>=kFatalLog) {
// 	  Lock mrs_lock(m_mrsMutex);
 	  err.sendMessage();//*m_out, "CAN::AnalysisRequestProcessor::processRequests" );
	}
      }
      catch(std::exception &err) {
          std::cout<<"catch3"<<std::endl;
	if (m_logLevel>=kFatalLog) {
	  std::string message(" Caught exception : " );
	  message += err.what() ;
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"config_except",s_componentAnaReqProcName,MRS_FATAL, "CAN::AnalysisRequestProcessor::processRequests",message);
	  ers::fatal(PixLib::pix::daq (ERS_HERE, "CAN::AnalysisRequestProcessor::processRequests",message));
	}
      } 
      catch(...) {
          std::cout<<"catch4"<<std::endl;
	if (m_logLevel>=kErrorLog) {
	  std::cerr << "ERROR [AnalysisRequestProcessor::processRequests] Unhandled exception caught!"  << std::endl;
	}
      }

      if (submission_error) {
          std::cout<<"catch5"<<std::endl;
	setAbortedAnalysisStatus( analysis_request.get() );
      }

    }
    if (m_logLevel>=kInfoLog) {
        std::cout<<"catch6"<<std::endl;
//       Lock mrs_lock(m_mrsMutex);
//       sendMessage(*m_out,"ANAREQPROC_term",MRS_INFORMATION, s_componentAnaReqProcName,"AnalysisRequestProcessor::processRequests", "Request processing terminated.");
      ers::info(PixLib::pix::daq (ERS_HERE, s_componentAnaReqProcName,"AnalysisRequestProcessor::processRequests", "Request processing terminated."));
    }
      
  }

  void AnalysisRequestProcessor::abortAnalysis(SerialNumber_t    analysis_serial_number)
  {
    std::unique_ptr<Request_t> analysis_request;
    {
      Lock analysis_lock(m_currentAnalysisMutex);
      if (m_currentAnalysis == analysis_serial_number) {
	m_abortRequest=true;
	m_aborted.wait();
      }
      Lock queue_lock(m_queueMutex);
      for (std::deque< Request_t *>::iterator request_iter=m_requestQueue.begin();
	   request_iter != m_requestQueue.end();
	   request_iter++) {
	if ((*request_iter)->serialNumber() == analysis_serial_number) {
	  analysis_request  = std::unique_ptr<Request_t> ( *request_iter );
	  *request_iter=NULL;
	  m_requestQueue.erase(request_iter);
	  break;
	}
      }
    }

    if (analysis_request.get()) {
      setAbortedAnalysisStatus( analysis_request.get() );

      if (m_logLevel>=kInfoLog) {
	std::stringstream message;
	message << "Aborted analysis request for " << analysis_serial_number << ".";
// 	Lock mrs_lock(m_mrsMutex);
// 	sendMessage(*m_out,"ANAREQ_abort",s_componentAnaReqProcName,MRS_INFORMATION, "AnalysisRequestProcessor::abortAnalysis", message.str() );
	ers::info(PixLib::pix::daq (ERS_HERE, "AnalysisRequestProcessor::abortAnalysis", message.str() ));
      }
    }

  }

  void AnalysisRequestProcessor::abortException(SerialNumber_t analysis_serial_number) {
//     throw MRSException("ANAREQ_abort", MRS_INFORMATION, "CAN::AnalysisRequestProcessor::processRequests", 
// 		       std::string("Abort anlysis ")+makeAnalysisSerialNumberString(analysis_serial_number)+".");
    throw MRSException(PixLib::PixMessages::INFORMATION, "CAN::AnalysisRequestProcessor::processRequests", 
		       std::string("Abort anlysis ")+makeAnalysisSerialNumberString(analysis_serial_number)+".");
  }

  void AnalysisRequestProcessor::setAbortedAnalysisStatus( Request_t *analysis_request)
  {
    Lock lock(m_currentAnalysisMutex);
    Lock dictionary_lock(m_isDictionary->mutex());
    for (JoblettList_t::iterator rod_iter = analysis_request->beginJoblett();
	 rod_iter != analysis_request->endJoblett();
	 rod_iter++) {
      rod_iter->second->setAborted();
      rod_iter->second->updateIsStatus();
      updateIsValue(m_isDictionary->dictionary(), rod_iter->second->isDictionaryName(), rod_iter->second->isStatus() );
    }

    if (m_logLevel>=kFatalLog) {
      std::stringstream message;
      message << " Submission failure. for A" << std::setw(9) << std::setfill('0') << analysis_request->serialNumber();
      {
// 	Lock mrs_lock(m_mrsMutex);
// 	sendMessage(*m_out,"submission_error",s_componentAnaReqProcName,MRS_FATAL, "CAN::AnalysisRequestProcessor::processRequests",message.str());
	ers::fatal(PixLib::pix::daq (ERS_HERE, "CAN::AnalysisRequestProcessor::processRequests",message.str()));
      }
    }
  }


  void AnalysisRequestProcessor::reset() 
  {

    {
      Lock queue_lock(m_queueMutex);
      for (std::deque< Request_t *>::iterator request_iter=m_requestQueue.begin();
	   request_iter != m_requestQueue.end();
	   request_iter++) {

	delete *request_iter;
	*request_iter=NULL;
      }
      m_requestQueue.clear();
    }
    //    AnalysisFactory::unloadAll();
    if (m_logLevel>=kInfoLog) {
//       Lock mrs_lock(m_mrsMutex);
//       sendMessage(*m_out,"ANAREQ_reset",s_componentAnaReqProcName,MRS_INFORMATION, "AnalysisRequestProcessor::reset", "Resetted request queue.");
      ers::info(PixLib::pix::daq (ERS_HERE, "AnalysisRequestProcessor::reset", "Resetted request queue."));
    }
  }

  void AnalysisRequestProcessor::shutdownWait() 
  {
    // @todo reset ?
    reset();
    m_shutdown=true;
    m_queueFilled.setFlag();
    if (state()!=omni_thread::STATE_TERMINATED) {
      if (m_logLevel>=kInfoLog) {
// 	Lock mrs_lock(m_mrsMutex);
//         sendMessage(*m_out,"ANAREQ_reset",s_componentAnaReqProcName,MRS_INFORMATION, "AnalysisRequestProcessor::shutdown", "Waiting for processing thread to shut down.");
	ers::info(PixLib::pix::daq (ERS_HERE, "AnalysisRequestProcessor::shutdown", "Waiting for processing thread to shut down."));
      }
      m_exitProcessor.wait();
      while (state()!=omni_thread::STATE_TERMINATED) {
	usleep(1000);
      }
    }
    if (m_logLevel>=kInfoLog) {
//       Lock mrs_lock(m_mrsMutex);
//       sendMessage(*m_out,"ANAREQ_shutdown",s_componentAnaReqProcName,MRS_INFORMATION, "AnalysisRequestProcessor::shutdown", "Shut down.");
      ers::info(PixLib::pix::daq (ERS_HERE, "AnalysisRequestProcessor::shutdown", "Shut down."));
    }
  }
}
