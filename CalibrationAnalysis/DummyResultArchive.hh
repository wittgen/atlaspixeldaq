#ifndef _CAN_DummyResultArchive_hh_
#define _CAN_DummyResultArchive_hh_

#include "IResultStore.hh"
#include "PixelCoralClientUtils/CoralClient.hh"
//#include <mrs/message.h>
#include "Common.hh"

// both seal and omniorb define HAVE_NAMESPACES
#ifdef HAVE_NAMESPACES
# undef HAVE_NAMESPACES
#endif
#include <SealServices/Application.h>

namespace CAN {

  /** Abstract interface of the class which archives the analysis results
   */
  class DummyResultArchive : public IResultStore
  {
  public:

    /** Put the given object into the results store and archive it.
     * @param results the analysis result list object to be transferred to the results store.
     * The ResultsStore will take the ownership over the given analysis result list
     */
    void put(SerialNumber_t id, AnalysisResultList_t *results) {
    }

    void initiateShutdown() {
    }

  };

}

#endif
