#ifndef _CAN_LogLevel_hh_
#define _CAN_LogLevel_hh_

namespace CAN {
  enum ELogLevel {kSilentLog, kFatalLog, kErrorLog, kWarnLog, kSlowInfoLog, kInfoLog, kDiagnosticsLog, 
		  kDiagnostics1Log,kDiagnostics2Log, kDiagnostics3Log, kDiagnostics4Log, kDiagnostics5Log, kAllDiagnosticsLog, 
		  kNLogLevels };
}

#endif 
