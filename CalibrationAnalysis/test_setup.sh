# for sh

#export CAN=$PWD
# export CAN=$PIX_APP_TOP/Pixel/CalibrationAnalysis;

if test -n "$CAN_CALIB_META"; then
  :
else
  export CAN_CALIB_META=${WorkDir}/Applications/Pixel/PixCalibDbCoral
fi
if test -n "$CAN_CALIB_RESULTS"; then
  :
else
  export CAN_CALIB_RESULTS=${WorkDir}/Applications/Pixel/PixResultsDbCoral
fi
export LD_LIBRARY_PATH=${CAN}:${PIX_ANA}/ConfigWrapper:${PIX_ANA}/DataContainer:${CAN_CALIB_META}:${CAN_CALIB_RESULTS}:${CAN}/configWriter:$LD_LIBRARY_PATH
export SEAL_PLUGINS=${CAN}/plugins/test:${CAN}/autoAnalyses:$SEAL_PLUGINS
export LD_LIBRARY_PATH=${CAN}/plugins/test:${CAN}/autoAnalyses:$LD_LIBRARY_PATH
#export PIX_METADATA_DB=${HOME}/CAN_db/CAN_meta/
export PIX_METADATA_DB="sqlite_file:${HOME}/CAN_db/CAN_meta/metadb.sql"
export CAN_CFG_PATH=${HOME}/CAN_db/CAN_cfg/
export PIXSCAN_CFG_PATH=${HOME}/CAN_db/CAN_cfg/
export CAN_CALIB_DB="sqlite_file:/tmp/${USER}/calib/dbcalib.sql"
export CAN_CALIB_DB_PATH=

if test ${CAN_CALIB_DB#oracle:} = ${CAN_CALIB_DB}; then
 CAN_CALIB_DB_PATH=${CAN_CALIB_DB#sqlite_file:};
 CAN_CALIB_DB_PATH=${CAN_CALIB_DB_PATH%/*}
fi

for dir in $PIX_METADATA_DB $CAN_CFG_PATH ${CAN_CALIB_DB_PATH}; do
  if test ${dir#*:} = ${dir}; then
  if test -d $dir; then
    :
  else
    mkdir -p $dir
  fi
  elif test ${dir#sqlite_file:} = ${dir}; then
    :
  else
   dir=${dir#sqlite_file:}
   dir=${dir%/*}
  fi
done

