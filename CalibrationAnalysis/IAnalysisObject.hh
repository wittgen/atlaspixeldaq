#ifndef _CAN_IAnalysisObject_hh_
#define _CAN_IAnalysisObject_hh_

#include "LogLevel.hh"

namespace PixLib {
  class Config;
}

namespace CAN {

  class IAnalysisObject {
  public:
    virtual ~IAnalysisObject() {}
    virtual PixLib::Config &config() = 0;
    virtual void setLogLevel(ELogLevel log_level) = 0;

  };

}

#endif
