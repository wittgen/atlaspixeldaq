#include <ipc/partition.h>
#include <ipc/core.h>
#include "Worker_impl.hh"
#include "IPCServerInstance.hh"

//#include <mrs/message.h>
#include <ConfigWrapper/pixa_exception.h>

#include "ConfigWrapper/ConnectivityManager.h"
#include "ConfigWrapper/DbManager.h"
#include "MetaDataService.hh"
#include "MasterDataServer.hh"
#include "DummyDataServer.hh"
#include "HistoReceiver.hh"
#include "RootFileHistoReceiver.hh"
#include "OHAndFileHistoReceiver.hh"

#include "CoralResultArchive.hh"

#include <ConfigWrapper/createDbServer.h>

#include <csignal>

//#include <omniORB4/internal/giopStream.h>

#include "daemonise.hh"


void signal_handler(int signal) {
    CAN::IPCServerInstance::stop();
}

class DelayResultStore : public CAN::IResultStore
{
public:
  DelayResultStore(unsigned int delay) : m_delay(delay) {}

  void put(CAN::SerialNumber_t, const std::string&, CAN::ExtendedAnalysisResultList_t *) {
    sleep(m_delay);
  }

  void initiateShutdown() {}

private:
  unsigned int m_delay;
};

int main(int argc, char **argv)
{
  // GET COMMAND LINE


  std::string ipcPartitionName;
  //  std::string mrsPartitionName;
  std::string ohPartitionName;
  std::string ohServerName;
  std::string nameServerName="nameServer";
  std::string ohServerBase="pixel_histo_server_PixNameServerInternal";
  std::string scanIsPartitionName;
  std::string scan_is_server_name;
  std::string can_is_server_name;
  std::string db_server_partition_name;
  std::string db_server_name;

  std::unique_ptr<CAN::IResultStore> result_store_delay;

  std::string ident;
  const char *obj_id = getenv("TDAQ_APPLICATION_OBJECT_ID");
  if (obj_id!=0) {
    ident=obj_id;
  }
  else {
    std::string hostname(1024,'\0');
    gethostname(&(hostname[0]),hostname.size());
    hostname.erase(strlen(hostname.c_str()));
    ident="rat_";
    ident+=hostname;
    setenv("TDAQ_APPLICATION_OBJECT_ID",ident.c_str(),0);
  }

  {
    const char *app_name = getenv("TDAQ_APPLICATION_NAME");
    if (!app_name) {
      std::string new_app_name;
      std::string hostname(1024,'\0');
      gethostname(&(hostname[0]),hostname.size());
      hostname.erase(strlen(hostname.c_str()));
      new_app_name="rat_";
      new_app_name+=hostname;
      setenv("TDAQ_APPLICATION_NAME",new_app_name.c_str(),0);
    }
  }

  unsigned int n_slots=1;
  unsigned int n_part=0;
  bool do_daemonise=false;

  bool error=false;
  for (int arg_i=1; arg_i<argc; arg_i++) {
    if (strcmp(argv[arg_i],"-n")==0 && arg_i+1<argc) {
      if (isdigit(argv[arg_i+1][0])) {  // deprecated to specify the number of slots with -n
	n_slots = atoi(argv[++arg_i]);
      }
      else {
	scan_is_server_name = argv[++arg_i];
	if (can_is_server_name.empty()) {
	  can_is_server_name = scan_is_server_name;
	}
      }
    }
    else if (strcmp(argv[arg_i],"-t")==0 && arg_i+1<argc) {
      n_slots = atoi(argv[++arg_i]);
    }
    else if (strcmp(argv[arg_i],"-d")==0) {
      do_daemonise=true;
    }
    else if (strcmp(argv[arg_i],"-p")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      ipcPartitionName = argv[++arg_i];
      n_part++;
//       if (!mrsPartitionName.empty()) {
// 	n_part=2;
//       }
    }
//     else if (strcmp(argv[arg_i],"-m")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
//       mrsPartitionName = argv[++arg_i];
//       if (!ipcPartitionName.empty()) {
// 	n_part++;
//       }
//     }
    else if (strcmp(argv[arg_i],"--oh-partition")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      ohPartitionName = argv[++arg_i];
    }
    else if ( (strcmp(argv[arg_i],"--oh-name-base")==0 || strcmp(argv[arg_i],"-o")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      ohServerBase = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"--name-server")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      nameServerName = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"--oh-server")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      ohServerName = argv[++arg_i];
    }

    else if ( (   strcmp(argv[arg_i],"--scan-is-name")==0 
 	       || strcmp(argv[arg_i],"-s")==0)              /* deprecated*/
	      && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      scan_is_server_name = argv[++arg_i];
    }
    else if ( (strcmp(argv[arg_i],"--can-is-name")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      can_is_server_name = argv[++arg_i];
    }
    else if ( (strcmp(argv[arg_i],"--scan-is-partition")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      scanIsPartitionName = argv[++arg_i];
    }
    else if ( (strcmp(argv[arg_i],"-D")==0
	       || strcmp(argv[arg_i],"--db-server-name")==0)
	      && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      db_server_name = argv[++arg_i];
    }
    else if ( strcmp(argv[arg_i],"--db-server-partition")==0
	      && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      db_server_partition_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-i")==0 && arg_i+1<argc) {
      ident = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"--delay-result-writing")==0 && arg_i+1<argc) {
      unsigned int delay= atoi( argv[++arg_i]);
      if (delay>0) {
	result_store_delay=std::unique_ptr<CAN::IResultStore>(new DelayResultStore(delay));
      }
    }
    else if (strcmp(argv[arg_i],"-h")==0) {
      error=true;
      break;
    }
    else {
      if (n_part>=2) {
	error=true;
      }
    }
  }
  bool dummy_data_server = ohServerBase.empty();
  if (dummy_data_server) {
    std::cout << "INFO [main:] No oh server name given. Use dummy data server." <<std::endl;
  }
  if (can_is_server_name.empty()) {
    if (!scan_is_server_name.empty()) {
      can_is_server_name = scan_is_server_name;
    }
    else {
      can_is_server_name = "CAN";
    }
  }
  if (n_slots ==0 || ipcPartitionName.empty() || (!dummy_data_server && ohServerBase.empty()) || (!dummy_data_server && scan_is_server_name.empty()) || error) {
    std::cout << "USAGE: rat [-n #] [-h] (-p) \"partition name\" [(-m) partition name] -o \"oh server base\" -s \"scan is server name\"" << std::endl
	      << std::endl
	      << "\t-d\t\t\tDaemonise the rat." << std::endl
	      << "\t-p partition\t\tSet name of the ipc partition. The -p is optional." << std::endl
      //	      << "\t-m mrs-partition\tSet the mrs partition name. If -m is omitted the second name will be used." << std::endl
	      << "\t--can-is-name\t\tName of the IS server where the analysis status is published." << std::endl
	      << "\t-n/--scan-is-name is server name\tSet name of the is server used to publish the scan status (mandatory)." << std::endl
	      << "\t--scan-partition partition name\t\tName of partition where the scan status is published if different from ipc partition (optional)." << std::endl
	      << std::endl
	      << "\t--db-server-name/-D\t\t\t\tName of the db server otherwise it will not be used." << std::endl
	      << "\t--db-server-partition/-D\t\t\t\tName of the db server partition if it differes from the default partition." << std::endl
	      << std::endl
	      << "\t-o/--oh-name-base oh server name\t\tSet name of the oh server (mandatory)." << std::endl
	      << "\t--oh-partition oh partition name\tName of oh partition if different from ipc partition (optional)." << std::endl
	      << "\t--oh-server \t\tSet a specific OH server (used eg: for analysis histograms)"<<std::endl
	      << "\t--name-server \t\tSet the name server "<<std::endl
	      << std::endl
	      << "\t-t number of threads\tSet the number of parallel processing threads the worker should offer." << std::endl
	      << std::endl
	      << "\t-h\t\t\tShow this help."
	      << std::endl
	      << "The ipc partition has to be specified. The other arguments are optional. Though, the defaults for the is-server "<<std::endl
	      << "and the server are presumably not correct."<<std::endl
	      << std::endl
	      << "Example ./rat -p PixelInfr -n RunParams -D PixelDbServer --name-server nameServer"
	      << "--oh-server pixel_histo_server_PixNameServerInternal -o pixel_histo_server -t 1" 
	      << std::endl;
    return EXIT_FAILURE;
  }

  std::cout << "INFO [" << argv[0] << ":main] Starting worker " << ident << " on " << ipcPartitionName<< std::endl;
  //	    << ". Messages will go to MRS on partition " << (!mrsPartitionName.empty() ? mrsPartitionName : ipcPartitionName) << std::endl;

  if (do_daemonise) {

    int ret = daemonise();
    if (ret != 0 ) return ret;

  }

  // START IPC
  // Start IPCCore
  IPCCore::init(argc, argv);

  // Create IPCPartition
  std::cout << "Getting partition " << ipcPartitionName << std::endl;
  std::unique_ptr<IPCPartition> ipcPartition (new IPCPartition(ipcPartitionName.c_str()));

  if (!ipcPartition->isValid()) {
    PIX_ERROR(argv[0] << ":main] Not a valid partition " << ipcPartitionName);
    return EXIT_FAILURE;
  }

//   std::auto_ptr<IPCPartition> mrsPartition;
//   std::cout << "Getting MRS " << mrsPartitionName << std::endl;
//   if (!mrsPartitionName.empty()) {
//     mrsPartition = std::auto_ptr<IPCPartition>(new IPCPartition(mrsPartitionName.c_str()));
//     std::cout << "... " << mrsPartitionName << std::endl;
//     if (!mrsPartition->isValid()) {
//       std::cerr << "ERROR [" << argv[0] << ":main] MRS partition " << mrsPartitionName << " is invalid." << std::endl;
//       return EXIT_FAILURE;
//     }
//   }

  std::unique_ptr<IPCPartition> db_server_partition;
  if (!db_server_name.empty()) {
    if (!db_server_partition_name.empty()) {
      db_server_partition = std::make_unique<IPCPartition>(db_server_partition_name.c_str());
      if (!db_server_partition->isValid()) {
	PIX_ERROR(argv[0] << ":main] Not a valid partition " << db_server_partition_name
		  << " for the db server.");
	return EXIT_FAILURE;
      }
    }

    std::shared_ptr<PixLib::PixDbServerInterface> db_server( PixA::createDbServer((db_server_partition.get() ?
										       *db_server_partition
										     : *ipcPartition),
										    db_server_name) );
    if (db_server.get()) {
      PixA::ConnectivityManager::useDbServer( db_server );
    }
    else {
//       MRSStream out((mrsPartition.get() ? *(mrsPartition.get()) : *(ipcPartition.get())));
//       CAN::sendMessage(out,"WORKER_no_db_server","WORKER",MRS_WARNING,"main","Failed to create the db server.");
      ers::warning(PixLib::pix::daq (ERS_HERE,"main","Failed to create the db server."));
    }
  }

  // prevent ROOT from adding histograms to random files
  TH1::AddDirectory(false);

  std::unique_ptr<CAN::IMasterDataServer> master_server;
  std::cout << "Getting master server " << ohPartitionName << std::endl;
  if (!dummy_data_server) {
    std::unique_ptr<IPCPartition> ohPartition;
    if (!ohPartitionName.empty()) {
      ohPartition = std::make_unique<IPCPartition>(ohPartitionName.c_str());
      std::cout << "... " << ohPartitionName << std::endl;
      if (!ohPartition->isValid()) {
        std::cerr << "ERROR [" << argv[0] << ":main] OH partition " << ohPartitionName << " is invalid." << std::endl;
        return EXIT_FAILURE;
      }
    }

    std::unique_ptr<IPCPartition> scanIsPartition;
    std::cout << "Getting scan is server " << scanIsPartitionName << std::endl;
    if (!scanIsPartitionName.empty()) {
      scanIsPartition = std::make_unique<IPCPartition>(scanIsPartitionName.c_str());
      std::cout << "... " << scanIsPartitionName << std::endl;
      if (!scanIsPartition->isValid()) {
        std::cerr << "ERROR [" << argv[0] << ":main] Scan IS partition " << scanIsPartitionName << " is invalid." << std::endl;
        return EXIT_FAILURE;
      }
    }
//  std::cout << "Pit stop 1 " << std::endl;
  std::unique_ptr<CAN::IHistoReceiver> oh_receiver(  new CAN::HistoReceiver((ohPartition.get() ? *(ohPartition.get()) : *(ipcPartition.get())),
								     ohServerBase) );
//std::cout << "Pit stop 2 " << std::endl;
  std::unique_ptr<CAN::IHistoReceiver> file_receiver( new CAN::RootFileHistoReceiver);
//std::cout << "Pit stop 3 " << std::endl;
std::unique_ptr<CAN::IHistoReceiver> histo_receiver( new CAN::OHAndFileHistoReceiver(oh_receiver.release(), file_receiver.release() ) );
//std::cout << "Pit stop 4 " << std::endl;
  master_server = std::unique_ptr<CAN::IMasterDataServer>(new CAN::MasterDataServer((scanIsPartition.get() ? *(scanIsPartition.get()) : *(ipcPartition.get())),
										   scan_is_server_name,
										  histo_receiver.release()));
										  //  (mrsPartition.get() ? *(mrsPartition.get()) : *(ipcPartition.get()))));
  }
  else {
    master_server = std::unique_ptr<CAN::IMasterDataServer>(new CAN::DummyMasterDataServer());//(mrsPartition.get() ? *(mrsPartition.get()) : *(ipcPartition.get())) ));
  }
//std::cout << "Pit stop 5 " << std::endl;
  std::vector<CAN::IResultStore *> result_store_list;

  // debug result archiving unit
  if (result_store_delay.get()) {
    result_store_list.push_back(result_store_delay.release());
  }
//std::cout << "Pit stop 6 " << std::endl;
  // store analysis results in coral
  result_store_list.push_back(new CAN::CoralResultArchive);
//std::cout << "Pit stop 7 " << std::endl;
  // store analysis histograms
  try {
    std::unique_ptr<CAN::HistoResultArchive> histo_result_archive( new CAN::HistoResultArchive );
    histo_result_archive->setServers(ipcPartition.get(), ohServerName,nameServerName);
    histo_result_archive->remakeOHInterface();
    result_store_list.push_back(histo_result_archive.release());
  }
  catch (PixA::BaseException &err) {
    //    MRSStream out((mrsPartition.get() ? *(mrsPartition.get()) : *(ipcPartition.get())));

    std::stringstream message;
    message << "Failed to create histo server interface to store analysis results. Caught exception " << err.getDescriptor();
//    CAN::sendMessage(out,"WORKERFAILED",MRS_FATAL, "rat.cc:main", message.str());
    ers::fatal(PixLib::pix::daq (ERS_HERE, "rat.cc:main", message.str()));
    //std::cerr << "ERROR [" << argv[0] << ":main]  " << message.str() << std::endl;

    return -1;
  }
  catch (...) {
    //    MRSStream out((mrsPartition.get() ? *(mrsPartition.get()) : *(ipcPartition.get())));

    std::stringstream message;
    message << "Failed to create histo server interface to store analysis results. Caught unhandled exception. ";
    //    CAN::sendMessage(out,"WORKERFAILED",MRS_FATAL, "rat.cc:main", message.str());
    ers::fatal(PixLib::pix::daq (ERS_HERE, "rat.cc:main", message.str()));
    throw;
  }
  
  // Install the signal handler
  std::signal(SIGINT   , signal_handler);
  std::signal(SIGQUIT  , signal_handler);
  std::signal(SIGILL   , signal_handler);
  std::signal(SIGTRAP  , signal_handler);
  std::signal(SIGABRT  , signal_handler);
  std::signal(SIGIOT   , signal_handler);
  std::signal(SIGBUS   , signal_handler);
  std::signal(SIGFPE   , signal_handler);
  std::signal(SIGKILL  , signal_handler);
  std::signal(SIGSEGV  , signal_handler);
  std::signal(SIGPIPE  , signal_handler);
  std::signal(SIGTERM  , signal_handler);
  std::signal(SIGSTKFLT, signal_handler);
  std::signal(SIGSYS   , signal_handler);
  
  // special return code which can be exploited by a wrapper script to restart the worker.
  int ret=13;
  try {
    std::unique_ptr<CAN::RestartThread> restart_thread(new CAN::RestartThread());//(mrsPartition.get() ? *(mrsPartition.get()) : *(ipcPartition.get()))));
    /*CAN::Worker_impl *worker =*/ new CAN::Worker_impl (*(ipcPartition.get()),
						  //(mrsPartition.get() ? *(mrsPartition.get()) : *(ipcPartition.get())),
						  can_is_server_name,
						  master_server.release(),
						  result_store_list,
						  ident,
						  n_slots);
						  //, restart_thread.get());

  } catch (PixA::ConfigException &err) {
     PIX_ERROR(argv[0] << ":main] Caught config exception " << err.getDescriptor());
  } catch (CAN::MRSException &err) {
    //MRSStream out((mrsPartition.get() ? *(mrsPartition.get()) : *(ipcPartition.get())));
    err.sendMessage();//out, std::string(argv[0])+":main" );
    PIX_ERROR( argv[0] << ":main] Caught exception " << err.getDescriptor());
  } catch (std::exception &err) {
    PIX_ERROR(argv[0] << ":main] Caught standard exception " << err.what());
  } catch (...) {
    PIX_ERROR(argv[0] << ":main] Unknown exception");
  }

  PIX_LOG("Exiting worker...");
  PixA::ConnectivityManager::deepCleanup();
  CAN::MetaDataService::deepCleanup();
  PixA::DbManager::deepCleanup();

//   catch (omni::giopStream::CommFailure &err) {
//     MRSStream out((mrsPartition.get() ? *(mrsPartition.get()) : *(ipcPartition.get())));
//     std::stringstream message;
//     message << "Internal CORBA communication error " << err.minor() << " in " << err.filename() << ":" << err.linenumber() << ".";
//     CAN::sendMessage(out,"SCHEDALIVE",MRS_INFORMATION, "rat.cc:main", message.str());
//   }

  PIX_LOG("Exiting rat app...");
  return ret;
}
