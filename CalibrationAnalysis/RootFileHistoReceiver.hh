#ifndef _RootFileHistoReceiver_H_
#define _RootFileHistoReceiver_H_

#include "IHistoReceiver.hh"

#include <utility>
#include <map>

#include <memory>

class TFile;
class TDirectory;
class TClass;

namespace PixLib {
  class ScanInfo_t;
}

namespace CAN {

  class RootFileHistoReceiver : public IHistoReceiver
  {
  public:
    RootFileHistoReceiver(bool verbose=false);
    ~RootFileHistoReceiver();

    unsigned int getHistograms(SerialNumber_t scan_serial_number,
			       const std::string &rod_name,
			       const std::string &conn_name,
			       const std::string &histo_name,
			       TopLevelHistoArray &array);

    std::vector<std::string> availableHistograms(SerialNumber_t scan_serial_number,
						  const std::string &rod_name,
						  const std::string &conn_name);

  private:
    bool inheritsFrom(const char *class_name, const TClass *a_class);
    std::string fileName(const PixLib::ScanInfo_t &scan_info);
    std::pair<TFile *, TDirectory *> open(const std::string &file_name, SerialNumber_t scan_serial_number);
    TDirectory *enter(TDirectory *a_directory, const std::string &dir_name);
    unsigned int loadHistograms(TDirectory *a_directory, TopLevelHistoArray &array);

    class FileData_t {
    public:
      FileData_t( const std::shared_ptr<TFile> &a_file, TDirectory *a_directory) : m_file(a_file), m_directory(a_directory), m_used(0) {}
      ~FileData_t();
      void incrementUsed()      { m_used++; }
      void resetUsed()          { m_used=0; }
      unsigned int used() const { return m_used; }
      TDirectory *directory()   { return m_directory; }
    private:
      std::shared_ptr<TFile> m_file;
      TDirectory              *m_directory;
      unsigned int             m_used;
    };

    static const unsigned int s_maxCacheSize = 2;
    std::map< SerialNumber_t, FileData_t > m_cache;
    bool m_verbose;

    static bool s_initialised;
    static bool initROOT();
  };

}
#endif
