#ifndef _PixDbGlobalMutex_hh_
#define _PixDbGlobalMutex_hh_

#include "Lock.hh"

namespace CAN {
   class PixDbGlobalMutex {
   public:
      static Mutex &mutex() { return s_mutex;}
   private:
      static Mutex s_mutex;
   };
}

#endif
