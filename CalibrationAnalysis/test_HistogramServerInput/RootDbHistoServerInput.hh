// Dear emacs, this is -*-c++-*-
#ifndef _CAN_RootDbHistogramInput_hh_
#define _CAN_RootDbHistogramInput_hh_

#include <ipc/partition.h>
#include <Common.hh>
#include <string>

namespace CAN {

  /** Histogram server implementation which servers histograms from CT-files.
   */
  class RootDbHistoServerInput
  {
  public:

    RootDbHistoServerInput(IPCPartition &oh_partition,
			   const std::string &oh_server_name,
			   const std::string &name_server_name,
			   const std::string &scan_is_server_name);

    ~RootDbHistoServerInput();

    SerialNumber_t addFile(const std::string &file_name, SerialNumber_t first_serial_number);

    void dumpHistos(std::string outputFileName, SerialNumber_t first_serial_number); //CLA

  private:
    IPCPartition *m_ohPartition;
    std::string   m_ohServerName;
    std::string   m_nameServerName;
    std::string   m_scanIsServerName;
  };

}


#endif
