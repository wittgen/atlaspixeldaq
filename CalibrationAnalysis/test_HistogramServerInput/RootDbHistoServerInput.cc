#include "RootDbHistoServerInput.hh"
#include <DataContainer/MakePixScanHistoName.h>
#include "DataContainer/HistoUtil.h"
#include "DataContainer/HistoRef.h"
#include <DataContainer/IScanResultListener.h>
#include <DataContainer/ScanResultStreamer.h>
#include <DataContainer/HistoInfo_t.h>
#include <DataContainer/ResultFileIndexer.h>
#include <ConfigWrapper/ConfObjUtil.h>
#include <ConfigWrapper/ConnectivityUtil.h>

#include <PixDbConfigDb.hh>
#include <ScanConfigDbInstance.hh>
#include <PixCoralDbMetaDataService.hh>

#include <oh/OHRawProvider.h>
#include <is/infodictionary.h>
#include <is/infoT.h>

#include <isUtil.hh>
#include <ScanIsStatus.hh>

#include <Histo/Histo.h>
#include <PixHistoServer/PixHistoServerInterface.h>

#include <Common.hh>

#include <cstdlib>
#include <stdexcept>
#include <map>


// for debugging
#include <iomanip>

namespace CAN {

  class MyCommandListener : public OHCommandListener {
  public:
  void command ( const std::string & name, const std::string & cmd ) {
    std::cout << " Command " << cmd << " received for the " << name << " histogram" << std::endl;
  }

  void command ( const std::string & cmd ){
    std::cout << " Command " << cmd << " received for all histograms" << std::endl;
  }
  };


  class HistoServerResultFileInput : public PixA::ResultFileIndexer
  {
  public:

    HistoServerResultFileInput(IPCPartition &oh_partition,
			       const std::string &oh_server_name,
			       const std::string &histo_name_server_name,
			       const std::string &scan_is_server_name,
			       SerialNumber_t first_serial_number)
      : ResultFileIndexer(true),
	m_histoServerInterface(new  PixLib::PixHistoServerInterface(&oh_partition, oh_server_name, histo_name_server_name,"RootDbHistoServerInput")),
	m_isServerName(scan_is_server_name),
	m_scanSerialNumber(first_serial_number),
	m_analysisSerialNumber(0),
	m_moduleNamingScheme(PixA::kUnknownModuleNaming),
	m_dictionary(oh_partition)
    {

    }


    void dumpHistos(std::string outputFileName) //CLA
    {
      bool result(true);
      std::stringstream folderstream;
      folderstream << "/";
      folderstream << "S" << std::setfill('0') << std::setw(9) << currentScanNumber(); //CLA
      folderstream << "/";

      std::cout << "------------ dumpHistos: Trying to write HS-folder: " << folderstream.str() << std::endl;
      
      try {
	result = m_histoServerInterface->writeRootHistoServer( folderstream.str(), outputFileName, true); 
      }
      catch(...) {
	std::cout << "ERROR  cought while trying to write HistoServer histos to file." << std::endl;
      }
      if(!result) {
	std::cout << "WARNING  writing of HistoServer histos NOT successful!" << std::endl;
      }
    }

    /** Will be called if a new serial number is needed.
     * Can be used to also write scan meta data.
     */
    PixA::SerialNumber_t allocateScanSerialNumber(const std::string &file_name, const PixA::ConfigHandle &pix_scan_config, bool matches_pattern) {

      PixA::ConfigHandle scan_info_handle( currentScanInfo() );
      PixA::ConnectivityRef conn( connectivity(scan_info_handle) );
      if (!conn) {
	std::stringstream message;
	message << "ERROR [CalibrationDataResultFileIndexer::allocateScanSerialNumber] No connectivity for  \""
		<< currentLabel() << " / " << currentFolder() << "\"." << std::endl;
	throw PixA::BaseException(message.str());
      }

      SerialNumber_t the_scan_serial_number = metaDataService()->scanSerialNumberForFile(file_name);
      if (the_scan_serial_number==0) {
	if (!pix_scan_config) {
	  std::stringstream message;
	  message << "ERROR [CalibrationDataResultFileIndexer::allocateScanSerialNumber] No scan config \""
		  << currentLabel() << " / " << currentFolder() << "\"." << std::endl;
	  throw PixA::BaseException(message.str());
	}
	unsigned int scan_serial_number = ++m_scanSerialNumber;
	the_scan_serial_number = scan_serial_number;
	time_t start_time=0;
	std::string start_time_string;
	time_t end_time=0;
	unsigned int duration=0;
      try {
	PixA::ConfigRef  scan_info( scan_info_handle.ref() );

	PixA::ConfGrpRef time(scan_info["Time"]);
	  start_time_string  = confVal<std::string>(time["ScanStart"]);
	std::string end_time_string  = confVal<std::string>(time["ScanEnd"]);

	struct tm start_tm;
	  strptime(start_time_string.c_str(),"%Y-%m-%d %H:%M:%S", &start_tm);

	struct tm end_tm;
	strptime(end_time_string.c_str(),"%Y-%m-%d %H:%M:%S", &end_tm);
	start_time = PixCoralDbClient::LocalToGMTTime( mktime(&start_tm) );
	end_time = PixCoralDbClient::LocalToGMTTime( mktime(&end_tm) );
	duration = end_time - start_time;

      }
      catch(...) {
      }

      //	PixA::ConfigRef pix_scan_config_ref( pix_scan_config.ref() );
      //	PixA::ConfigRef full_pix_scan_config( pix_scan_config_ref.createFullCopy() );
      //	ScanConfigDb::instance()->writeConfig( scan_serial_number, full_pix_scan_config );


	CAN::InfoPtr_t<PixLib::ScanInfo_t> scan_info = new PixLib::ScanInfo_t;

	//	if (!currentLabel().empty())
	{
	  std::string scan_type_name = currentLabel();
	  std::string::size_type pos  = scan_type_name.find(" - ");
	  unsigned int n_alpha=0;
	  unsigned int n_digit=0;
	  if (pos != std::string::npos && pos > 8 ) {
	    for (std::string::size_type char_i=0; char_i<pos; char_i++) {
	      if (isalpha(scan_type_name[char_i])) n_alpha++;
	      else if (isdigit(scan_type_name[char_i])) n_digit++;
	    }
	  }
	  if (n_alpha>4 && n_alpha>n_digit) {
	    scan_info->setScanType(scan_type_name.substr(0,pos));
	  }
	  else {
	    scan_info->setScanType(currentLabel());
	  }
	}
	scan_info->setScanTypeTag("TEST");

	scan_info->setScanStart(start_time);
	scan_info->setScanEnd(end_time);
	scan_info->setComment(currentComment());
	scan_info->setQuality(CAN::kNormal);
	scan_info->setFile(file_name);

	  scan_info->setIdTag( conn.tag(PixA::IConnectivity::kId) );
	  scan_info->setConnTag(conn.tag(PixA::IConnectivity::kConn) );
	  scan_info->setDataTag( conn.tag(PixA::IConnectivity::kPayload) );
	  scan_info->setAliasTag( conn.tag(PixA::IConnectivity::kAlias) );
	  scan_info->setCfgTag( conn.tag(PixA::IConnectivity::kConfig) );
	  scan_info->setModCfgTag( conn.tag(PixA::IConnectivity::kModConfig) );
	  scan_info->setCfgRev( conn.revision() );
	  scan_info->setModCfgRev( conn.revision() );

	 try {
	   const char *scan_config_db = getenv("PIXSCAN_CFG_PATH");
	   if (!scan_config_db || strlen(scan_config_db)==0) {
	     std::cerr << "FATAL [CalibrationDataResultFileIndexer::newPixScanHisto] "
	                  "Environment variable PIXSCAN_CFG_PATH is undefined. It defines the location of the scan configuration db, "
		       << std::endl;
	   }
	   else {
	     std::auto_ptr<CAN::PixDbConfigDb> db( new CAN::PixDbConfigDb(scan_config_db));
	     // only add the scan configuration if it does not exist yet
	     try {
	       db->configFileName(scan_info->scanType(), scan_info->scanTypeTag(), scan_info->scanStart());
	     }
	     catch (MissingConfig &err ) {
	       db->addConfig(CAN::ScanConfigDbInstance::scanClassName(),scan_info->scanType(), scan_info->scanTypeTag(), scan_info->scanStart(), pix_scan_config);
	     }
	   }

	 }
	 catch (MissingConfig &err ) {
	   std::cerr << "ERROR [CalibrationDataResultFileIndexer::newPixScanHisto] Caught exception " << err.getDescriptor() << std::endl;
	 }


	try {

	  metaDataService()->addInfo(scan_serial_number,scan_info);
	  std::cout << "INFO [CalibrationDataResultFileIndexer::allocateScanSerialNumber] Added scan meta data :: " << scan_serial_number << " : " 
		    << file_name 
		    << " : " << currentLabel() << " / " << currentFolder() 
		    << "\n\t\t"
		    << scan_info->scanType() << ", "
		    << scan_info->idTag() << " : "
		    << scan_info->connTag() << "; "
		    << scan_info->cfgTag() << " / "
		    << scan_info->cfgRev()
		    << std::endl;



	}
	catch (CAN::MRSException &err) {
	  std::cerr << "ERROR [CalibrationDataResultFileIndexer::newPixScanHisto] Caught exception " << err.getDescriptor() << std::endl;
	}
	catch (std::exception &err) {
	  std::cerr << "ERROR [CalibrationDataResultFileIndexer::newPixScanHisto] Caught standard exception " << err.what() << std::endl;
	}
      }

      if (matches_pattern) {
	// set initial rod scan status to unknown
	PixA::CrateList::const_iterator begin_crate=conn.crates().begin();
	PixA::CrateList::const_iterator end_crate=conn.crates().end();

      std::map<std::string,RodData_t> &a_rod_status_map = m_rodStatus[the_scan_serial_number];
	{
	  for (PixA::CrateList::const_iterator crate_iter = begin_crate;
	       crate_iter != end_crate;
	       ++crate_iter) {
	    //	std::cout << const_cast<PixLib::RodCrateConnectivity *>(*crate_iter)->name() << std::endl;
	    PixA::RodList::const_iterator begin_rod = rodBegin(crate_iter);
	    PixA::RodList::const_iterator end_rod = rodEnd(crate_iter);
	    for (PixA::RodList::const_iterator rod_iter = begin_rod;
		 rod_iter != end_rod;
		 ++rod_iter) {

	    std::pair< std::map<std::string,RodData_t>::iterator, bool> ret = a_rod_status_map.insert( std::make_pair( PixA::connectivityName(*rod_iter), RodData_t()) );
	      if (ret.second) {
	      setRodStatus(the_scan_serial_number, PixA::connectivityName(*rod_iter), ret.first->second.rodStatus());
	      }
	    }
	  }
	}
       }
      std::cout << "allocateScanSerialNumber: " << the_scan_serial_number << std::endl;
      return the_scan_serial_number;
    }


    /** Will be called if a new serial number is needed.
     * Can be used to also write analysis meta data.
     */
    PixA::SerialNumber_t allocateAnalysisSerialNumber(PixA::SerialNumber_t scan_serial_number, bool matches_pattern) {
      return 0; // @todo add support for analysis histograms.

      SerialNumber_t analysis_serial_number = ++m_analysisSerialNumber;

      time_t start_time = 0;
      time_t end_time = 0;
      std::string file_name;

      PixA::ConfigHandle scan_info_handle( currentScanInfo() );
      if (scan_info_handle) {
	try {
	  PixA::ConfigRef  scan_info( scan_info_handle.ref() );

	  PixA::ConfGrpRef time(scan_info["Time"]);
	  std::string start_time_string  = confVal<std::string>(time["ScanStart"]);
	  std::string end_time_string  = confVal<std::string>(time["ScanEnd"]);

	  struct tm start_tm;
	  strptime(start_time_string.c_str(),"%Y-%m-%d %H:%M:%S", &start_tm);

	  struct tm end_tm;
	  strptime(end_time_string.c_str(),"%Y-%m-%d %H:%M:%S", &end_tm);
	  start_time = PixCoralDbClient::LocalToGMTTime( mktime(&start_tm) );
	  end_time = PixCoralDbClient::LocalToGMTTime( mktime(&end_tm) );
	  unsigned int duration = end_time - start_time;

	  std::cout << "INFO [CalibrationDataResultFileIndexer::allocateAnalysisSerialNumber] time: start  = " << start_time_string 
		    << " end = " << end_time_string
		    << " duration = " << duration
		    << std::endl;
	}
	catch(...) {
	}
      }
      else {
	const CAN::InfoPtr_t<PixLib::ScanInfo_t> scan_info ( metaDataService()->getScanInfo(scan_serial_number) );
	if (!scan_info) {
	  std::cout << "ERROR [CalibrationDataResultFileIndexer::allocateAnalysisSerialNumber] No meta data for scan S"
		    << scan_serial_number
		<< std::endl;
	}
	else {
	  start_time = scan_info->scanStart();
	  end_time = scan_info->scanEnd();
	}
	file_name = scan_info->file();
      }

      CAN::InfoPtr_t<CAN::AnalysisInfo_t> analysis_info = new CAN::AnalysisInfo_t(ObjectInfo_t(currentTypeName(),"CT",0),
										  ObjectInfo_t(),
										  ObjectInfo_t(),
										  scan_serial_number,
										  0,
										  CAN::kNormal,
										  currentComment()+"\nlabel="+currentLabel(),
										  start_time,
										  end_time,
										  file_name);

      try {

	metaDataService()->addInfo(scan_serial_number,analysis_info);
	std::cout << "INFO [HistoServerResultFileInput::allocateAnalysisSerialNumber] Added analysis meta data :: " << analysis_serial_number << " : " 
		  << std::endl;
      }
      catch(...) {
	std::cout << "ERROR [HistoServerResultFileInput::allocateAnalysisSerialNumber] Failed to add  analysis meta data :: " << analysis_serial_number << " : " 
		  << std::endl;
      }

      return analysis_serial_number;
    }


    void addPixScanConfig(PixA::SerialNumber_t, const std::string&, PixA::ConfigHandle&) {
      //@todo need to collect the scan configurations and write them all togetherto
      // the scan configuration db.
      // Or better said first verify that the scan config db does not exist yet.
    }

    /** Called for plain histograms assigned to an analysis.
     */
    void addAnalysisHisto(PixA::SerialNumber_t /*analysis_serial_number*/,
			  const std::string &/*rod_name*/,
			  const std::string &/*folder_name*/,
			  const std::string &/*histo_name*/,
			  const PixA::HistoHandle &/*histo_handle*/)
    {
    }


    void addScanHisto(PixA::SerialNumber_t /*scan_serial_number*/,
		      const std::string &/*rod_name*/,
		      const PixA::ScanConfigRef &/*scan_config*/,
		      const std::string &/*folder_name*/,
		      const std::string &/*histo_name*/,
		      const PixA::HistoHandle &/*histo_handle*/) {
    }

    /** Called for pix scan histograms assigned to a scan.
     */
    void addScanHisto(PixA::SerialNumber_t scan_serial_number,
		      const std::string &rod_name,
		      const std::string &module_name,
		      const PixA::ScanConfigRef &scan_config,
		      const std::string &folder_name,
		      const std::string &histo_name,
		      const PixA::HistoHandle &histo_handle) {
      if (!histo_handle) {
	std::cout << "ERROR [CalibrationDataResultFileIndexer::addScanHisto] Invalid histo handle for S" 
		  << scan_serial_number << folder_name << " / " << module_name << "."
		  << std::endl;
	return;
      }

      try {

	PixA::HistoRef histo( histo_handle.ref());
	std::vector<std::string> full_histo_name;
	std::pair<PixA::EModuleNameingConvention, unsigned int> ret =
	  PixA::makePixScanHistoName( m_moduleNamingScheme,
				      static_cast<unsigned int>(-1),
				      scan_config,
				      histo,
				      stripPath(histo_name),
				      full_histo_name );
	m_moduleNamingScheme = ret.first;

	HistoInfo_t info;
	histo.numberOfHistos(full_histo_name,info);
	PixA::Index_t index;
	info.makeDefaultIndexList(index);

	if ( index.size() > 0) {
	  PixA::Index_t min_index(index);

	  RodData_t &a_rod_data = m_rodStatus[scan_serial_number][rod_name];
	  if (a_rod_data.rodStatus() != CAN::ScanIsStatus::name(CAN::ScanIsStatus::kRunning )) {
	    a_rod_data.setRodStatus( CAN::ScanIsStatus::name(CAN::ScanIsStatus::kRunning ) );
	    setRodStatus( scan_serial_number, rod_name, a_rod_data.rodStatus() );
	  }
	  a_rod_data.addModuleHisto(histo_name, histo_handle);

	  // loop over all indices
	  for( ;index[0]<info.maxIndex(0); ) {

	    std::auto_ptr<PixLib::Histo> a_hist( histo.histo(full_histo_name,index) );
	    if (a_hist.get()) {
	      std::string histogram_server_histo_name = makeHistogramServerHistoName(scan_serial_number, rod_name, module_name, histo_name,index);
	      try {
		m_histoServerInterface->sendHisto(histogram_server_histo_name, *(a_hist.get()) );
	      }
	      catch(...) {
		std::cout << "ERROR [CalibrationDataResultFileIndexer::newPixScanHisto] Failed to sent histograms " << histogram_server_histo_name 
			  << " for scan " << scan_serial_number
			  << " of " << currentLabel()  << " : " << currentFolder() 
			  << " to histogram server."
			  << std::endl;
		break;
	      }
	    }

	    // next index;
	    unsigned int level_i=index.size();
	    for (; level_i-->0; ) {
	      if (index[level_i]+1<info.maxIndex(level_i)) {
		index[level_i]++;
		break;
	      }
	      else {
		index[level_i]=min_index[level_i];
	      }
	    }
	    if (level_i>index.size()) break;
	  }

	}
      }
      catch (PixLib::PixDBException &err ) {
	std::cout << "ERROR [CalibrationDataResultFileIndexer::newPixScanHisto] Failed to retrieve the histograms " << histo_name << " for scan " << scan_serial_number
		  << " for " << currentLabel()  << " : " << currentFolder() << std::endl;
      }
    }

    unsigned int nextScanSerialNumber() const { 
      return m_scanSerialNumber+1; 
    }   

    void finishPixModuleGroup() {

      ResultFileIndexer::finishPixModuleGroup();

      for (std::map<SerialNumber_t, std::map< std::string, RodData_t > >::iterator scan_iter=m_rodStatus.begin();
	   scan_iter != m_rodStatus.end();
	   scan_iter++) {
	std::map< std::string, RodData_t >::iterator rod_iter = scan_iter->second.find(currentPixModuleGroupName());

	if (rod_iter == scan_iter->second.end() || rod_iter->second.rodStatus() == CAN::ScanIsStatus::name(CAN::ScanIsStatus::kUnknown)) {
	  setRodStatus(scan_iter->first, currentPixModuleGroupName(), CAN::ScanIsStatus::name(CAN::ScanIsStatus::kAborted ));
      }
	else {
	  if (rod_iter->second.rodStatus() == CAN::ScanIsStatus::name(CAN::ScanIsStatus::kRunning )) {
	    if (rod_iter->second.hasHistograms()) {
	      setRodStatus(scan_iter->first, currentPixModuleGroupName(), CAN::ScanIsStatus::name(CAN::ScanIsStatus::kSuccess ));
	    }
	    else {
	      setRodStatus(scan_iter->first, currentPixModuleGroupName(), CAN::ScanIsStatus::name(CAN::ScanIsStatus::kFailed ));
      }
      }
	}
      }
      std::cout << "INFO [CalibrationDataResultFileIndexer::finishPixModuleGroup]  " << currentPixModuleGroupName() << " ." << std::endl;
    }

    void finish() {
      std::cout << "INFO [CalibrationDataResultFileIndexer::finish] Update scan status of all RODs." << std::endl;
      for(std::map<SerialNumber_t, std::map<std::string, RodData_t> >::const_iterator scan_iter = m_rodStatus.begin();
	  scan_iter != m_rodStatus.end();
	  ++scan_iter) {
	for (std::map<std::string, RodData_t>::const_iterator rod_iter = scan_iter->second.begin();
	     rod_iter != scan_iter->second.end();
	     rod_iter++) {
	  if (rod_iter->second.rodStatus() == CAN::ScanIsStatus::name(CAN::ScanIsStatus::kUnknown )) {
	    setRodStatus(scan_iter->first, rod_iter->first, CAN::ScanIsStatus::name(CAN::ScanIsStatus::kAborted ) );
	  }
	}
      }
    }


  protected:

    void setRodStatus(SerialNumber_t serial_number, const std::string &rod_name, const std::string &status_string) {
      std::stringstream is_scan_status_name;
      is_scan_status_name << m_isServerName << "./" << "S"<< std::setw(9) << std::setfill('0') << serial_number /*<<  should also add the crate name */
			  << '/' << rod_name
			  << '/' << "STATUS";
      std::cout << "INFO [CalibrationDataResultFileIndexer::setRodStatus] publish : name=\"" << is_scan_status_name.str() 
		<< "\" value=\"" << status_string << "\"."
		<< std::endl;

      m_isStatus.setValue(status_string);
      ISType type(m_isStatus.type());
      setIsValue(m_dictionary,type,is_scan_status_name.str(), m_isStatus );
    }


    static std::string stripPath(const std::string &histo_name);
    static std::string makeHistogramServerHistoName(SerialNumber_t scan_serial_number,
						    const std::string &rod_name,
						    const std::string &conn_name,
						    const std::string &histo_name,
						    const PixA::Index_t &index);

  private:
    static PixCoralDbMetaDataService *metaDataService();
    static PixCoralDbMetaDataService *s_instance;

    std::auto_ptr<PixLib::PixHistoServerInterface> m_histoServerInterface;
    std::string                                    m_isServerName;

    class RodData_t {
    public:
      RodData_t() : m_rodStatus(CAN::ScanIsStatus::name(CAN::ScanIsStatus::kUnknown )) {}

      void addModuleHisto(const std::string &histo_name, const PixA::HistoHandle &histo_handle) { m_moduleHistoList.insert(std::make_pair(histo_name, histo_handle));}
      void addHisto(const std::string &histo_name, const PixA::HistoHandle &histo_handle) { m_rodHistoList.insert(std::make_pair(histo_name, histo_handle));}

      bool hasHistograms() const { return !m_moduleHistoList.empty() || !m_rodHistoList.empty(); }

      std::multimap<std::string, PixA::HistoHandle>::const_iterator beginModuleHisto() const
      { return m_moduleHistoList.begin();}

      std::multimap<std::string, PixA::HistoHandle>::const_iterator endModuleHisto() const
      { return m_moduleHistoList.end();}

      std::multimap<std::string, PixA::HistoHandle>::const_iterator beginModuleHisto(const std::string &histo_name) const
      { return m_moduleHistoList.lower_bound(histo_name);}

      std::multimap<std::string, PixA::HistoHandle>::const_iterator endModuleHisto(const std::string &histo_name) const
      { return m_moduleHistoList.upper_bound(histo_name);}

      std::map<std::string, PixA::HistoHandle>::const_iterator beginRodHisto() const
      { return m_rodHistoList.begin();}

      std::map<std::string, PixA::HistoHandle>::const_iterator endRodHisto() const
      { return m_rodHistoList.end();}

      const std::string &rodStatus() const                 { return m_rodStatus; }
      void setRodStatus(const std::string &new_rod_status) { m_rodStatus = new_rod_status; }

    private:
      std::string                                   m_rodStatus;
      std::map<std::string, PixA::HistoHandle>      m_rodHistoList;
      std::multimap<std::string, PixA::HistoHandle> m_moduleHistoList;
    };

    /** Return a connectivity.
     * @todo Return custom connectivity to mock-up data
     */
    const PixA::ConnectivityRef connectivity( const PixA::ConfigHandle &scan_info_handle ) {
      return ResultFileIndexer::connectivity( scan_info_handle );
    }

    std::map<SerialNumber_t, std::map< std::string, RodData_t > > m_rodStatus;

    SerialNumber_t   m_scanSerialNumber;
    SerialNumber_t   m_analysisSerialNumber;

    
    PixA::EModuleNameingConvention m_moduleNamingScheme;

    ISInfoDictionary m_dictionary;
    ISInfoString     m_isStatus;

  };

  std::string HistoServerResultFileInput::stripPath(const std::string &histo_name)
  {
    std::string::size_type pos = histo_name.rfind("/");
    if (pos != std::string::npos && pos+1 < histo_name.size()) {
      return std::string(histo_name,pos+1,histo_name.size()-pos-1);
    }
    else {
      return histo_name;
    }
  }

  std::string HistoServerResultFileInput::makeHistogramServerHistoName(SerialNumber_t scan_serial_number,
								       const std::string &rod_name,
								       const std::string &conn_name,
								       const std::string &histo_name,
								       const PixA::Index_t &index)
  {
    std::stringstream full_name;
    full_name << "/S" << std::setw(9) << std::setfill('0') << scan_serial_number << '/';
    if (!rod_name.empty()) {
      full_name << rod_name <<'/';
    }
    if (!conn_name.empty()) {
      full_name <<  conn_name << '/';
    }
    full_name << histo_name;

    char letter='A';
    for (PixA::Index_t::const_iterator level_iter = index.begin();
	 level_iter!= index.end();
	 level_iter++) {
      full_name << '/';
      if( index.end() - level_iter > 1) {
	full_name << letter++;
      }
      full_name << *level_iter;
    }
    full_name << ':';
    return full_name.str();
  }

  PixCoralDbMetaDataService *HistoServerResultFileInput::s_instance=NULL;

  PixCoralDbMetaDataService *HistoServerResultFileInput::metaDataService() 
  {
    if (!s_instance)
      s_instance = new PixCoralDbMetaDataService;
    return s_instance;
  }


  RootDbHistoServerInput::RootDbHistoServerInput(IPCPartition &oh_partition,
						 const std::string &oh_server_name,
						 const std::string &name_server_name,
						 const std::string &scan_is_server_name)
    : m_ohPartition(&oh_partition),
      m_ohServerName(oh_server_name),
      m_nameServerName(name_server_name),
      m_scanIsServerName(scan_is_server_name)
  { }

  RootDbHistoServerInput::~RootDbHistoServerInput() {}

  SerialNumber_t RootDbHistoServerInput::addFile(const std::string &file_name, SerialNumber_t first_serial_number) {
    PixA::ScanResultStreamer streamer(file_name);
    HistoServerResultFileInput histo_server_input(*m_ohPartition,
										   m_ohServerName,
										   m_nameServerName,
										   m_scanIsServerName,
										   first_serial_number);
    streamer.setListener( histo_server_input );
    try {
      streamer.read();
    }
    catch (PixA::BaseException &err) {
      std::cout << "ERROR [RootDbHistoServerInput::addFile] caught exception while processing file " << file_name 
		<< " :  " <<  err.getDescriptor()
		<< std::endl;
    }


    SerialNumber_t next_scan_serial_number = histo_server_input.nextScanSerialNumber();

    return next_scan_serial_number;
  }


  void RootDbHistoServerInput::dumpHistos(std::string outputFileName, SerialNumber_t first_serial_number) //CLA
  {
    HistoServerResultFileInput histo_server_input(*m_ohPartition,
						  m_ohServerName,
						  m_nameServerName,
						  m_scanIsServerName,
						  first_serial_number);
    histo_server_input.dumpHistos(outputFileName);
  }

}
