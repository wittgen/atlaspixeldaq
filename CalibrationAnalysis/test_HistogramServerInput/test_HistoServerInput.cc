#include <memory>
#include <vector>
#include <iostream>
#include <cstdlib>

#include <ipc/core.h>

#include "RootDbHistoServerInput.hh"

#include <ConfigWrapper/ConnectivityManager.h>
#include <ConfigWrapper/createDbServer.h>

int main( int argc, char ** argv )
{
    std::string partition_name;
    std::string oh_server_name = "PixelHistoServer";
    std::string name_server_name = "HistoNameServer";
    std::string is_server_name = "RunParams";
    std::string db_server_partition_name;
    std::string db_server_name;
    CAN::SerialNumber_t serial_number = 0;
    bool error=false;
    bool dumpNewRootFile(false); //CLA
    std::vector<std::string> result_files;
    std::string outputFileName("newRootFile.root");

    for (int arg_i=1; arg_i<argc; arg_i++) {
      if (strcmp(argv[arg_i],"-p")==0 && arg_i+1<argc) {
	partition_name = argv[++arg_i];
      }
      else if (strcmp(argv[arg_i],"-o")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	oh_server_name = argv[++arg_i];
      }
      else if (strcmp(argv[arg_i],"-n")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	name_server_name = argv[++arg_i];
      }
      else if (strcmp(argv[arg_i],"-s")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	serial_number = atoi(argv[++arg_i]);
      }
      else if (strcmp(argv[arg_i],"--scan-is")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	is_server_name = argv[++arg_i];
      }
      else if ( (strcmp(argv[arg_i],"-D")==0
		 || strcmp(argv[arg_i],"--db-server-name")==0)
		&& arg_i+1<argc && argv[arg_i+1][0]!='-') {
	db_server_name = argv[++arg_i];
      }
      else if ( strcmp(argv[arg_i],"--db-server-partition")==0
		&& arg_i+1<argc && argv[arg_i+1][0]!='-') {
	db_server_partition_name = argv[++arg_i];
      }
      else if (strcmp(argv[arg_i],"-f")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	while (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	  ++arg_i;
	  result_files.push_back(argv[arg_i]);
	  if (!result_files.back().empty() && result_files.back()[0]=='\\') {
	    result_files.back().erase(0,1);
	  }
	}
      }
      else if(strcmp(argv[arg_i],"-dumpNewRootFile")==0 && arg_i<argc) { //CLA
	dumpNewRootFile=true;
      }
      else {
	std::cout << "ERROR ["<< argv[0] << ":main] unhandled argument " << arg_i << " : " << argv[arg_i]<< "."
		  << std::endl;
	error=true;
      }
    }

    if (error || serial_number == 0) {
      std::cout << "usage: " << argv[0] << "-p partition  [-n \"name server name\"] [-o \"oh server name\"] "
		<< " -s \"first serial number\""  
		<< " -f infile.root"
		<< " [-dumpNewRootFile]" 
		<< std::endl
		<< "\t\t\t\t[--scan-is \"scan is server name\"]" << std::endl;
      return -1;
    }

    // Start IPCCore
    IPCCore::init(argc, argv);

    IPCPartition oh_partition(partition_name.c_str());
    if (!oh_partition.isValid()) {
      std::cout << "ERROR [" << argv[0] << ":main] Not a valid partition : " << partition_name << "." << std::endl;
      return -1;
    }

    std::auto_ptr<IPCPartition> db_server_partition;
    if (!db_server_name.empty()) {
      if (!db_server_partition_name.empty()) {
	db_server_partition = std::auto_ptr<IPCPartition>(new IPCPartition(db_server_partition_name.c_str()));
	if (!db_server_partition->isValid()) {
	  std::cerr << "ERROR [" << argv[0] << ":main] Not a valid partition " << db_server_partition_name
		    << " for the db server." << std::endl;
	  return EXIT_FAILURE;
	}
      }

      std::shared_ptr<PixLib::PixDbServerInterface> db_server( PixA::createDbServer((db_server_partition.get() ?
										       *db_server_partition
										       : oh_partition),
										      db_server_name) );
      if (db_server.get()) {
	PixA::ConnectivityManager::useDbServer( db_server );
      }
      else {
	std::cout << "WARNING [main:" << argv[0] << "] Failed to create the db server." << std::endl;
      }
    }

    std::auto_ptr<CAN::RootDbHistoServerInput> histo_input(new CAN::RootDbHistoServerInput(oh_partition,
											   oh_server_name,
											   name_server_name,
											   is_server_name));
    for (std::vector<std::string>::const_iterator file_iter=result_files.begin();
	 file_iter != result_files.end();
	 file_iter++) {
      if (!file_iter->empty()) {

	CAN::SerialNumber_t new_serial_number = histo_input->addFile(*file_iter, serial_number);
	//	if (new_serial_number > serial_number ) {
	// added something
	//	}

	if(dumpNewRootFile) 
	  histo_input->dumpHistos(outputFileName,new_serial_number-1); //CLA:  new_serial_number-1 is the number that was used (in IS)

	serial_number = new_serial_number;
      }
    }

    return 0;
}
