#include <HistoReceiver.hh>
#include <HistoDef.hh>
#include <HistoArray.hh>

#include <ipc/partition.h>
#include <is/exceptions.h>

#include <exception>
#include <exception.hh>

#include <TFile.h>


int main(int argc, char **argv)
{
    std::string partition_name;
    std::string oh_server_name = "test_OH";
    std::string name_server_name = "test_NameServer";
    std::string output_file = "/tmp/test.root";
    std::string histogram_pattern;

    CAN::SerialNumber_t serial_number = 0;
    bool error=false;

    std::vector<std::string> result_files;

    for (int arg_i=1; arg_i<argc; arg_i++) {
      if (strcmp(argv[arg_i],"-p")==0 && arg_i+1<argc) {
	partition_name = argv[++arg_i];
      }
      else if (strcmp(argv[arg_i],"-o")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	oh_server_name = argv[++arg_i];
      }
      else if (strcmp(argv[arg_i],"-n")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	name_server_name = argv[++arg_i];
      }
      else if (strcmp(argv[arg_i],"-s")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	serial_number = atoi(argv[++arg_i]);
      }
      else if (strcmp(argv[arg_i],"-i")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	histogram_pattern = argv[++arg_i];
      }
      else if (strcmp(argv[arg_i],"-d")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	output_file = argv[++arg_i];
      }
      else {
	std::cout << "ERROR ["<< argv[0] << ":main] unhandled argument " << arg_i << " : " << argv[arg_i]<< "." << std::endl;
	error=true;
      }
    }

    if (error || serial_number == 0 || partition_name.empty() || oh_server_name.empty() || output_file.empty() ) {
      std::cout << "usage: " << argv[0] << " -p partition  [-n \"name server name\"] [-o \"oh server name\"] -s \"first serial number\"" << std::endl
		<< "\t\t\t-i \"pix scan histo name (including serial number, ROD etc.)\" -d \"destination file\"" << std::endl;
      return -1;
    }

    try {
      // Start IPCCore
      IPCCore::init(argc, argv);

      std::auto_ptr<IPCPartition> partition (new IPCPartition( partition_name.c_str()));
      if (!partition->isValid()) {
	std::cout << "ERROR [" << argv[0] << "] Partition \"" << partition_name << "\" is not a valid partition." << std::endl;
	return -1;
      }

    CAN::HistoReceiver receiver(*partition, oh_server_name);

    CAN::TopLevelUpdateHistoArray histo_array;
    std::string rod_name;
    std::string conn_name;
    std::string::size_type rod_end=0;
    std::string::size_type conn_name_end=0;

    if (histogram_pattern.compare(0,3,"ROD")==0) {
      rod_end = histogram_pattern.find("/");
      rod_name = std::string(histogram_pattern,0,rod_end);
    }

    if (rod_end != std::string::npos) {
      conn_name_end = histogram_pattern.find("/",rod_end+1);
      if (conn_name_end != std::string::npos) {
	conn_name = std::string(histogram_pattern, rod_end+1, conn_name_end-rod_end-1);
      }
    }

    std::string histogram_name;
    if (conn_name_end != std::string::npos && conn_name_end>0) {
      histogram_name = std::string(histogram_pattern,conn_name_end+1,histogram_pattern.size()-conn_name_end);
    }

    std::cout << "INFO [main] ROD = " << rod_name << " conn=" << conn_name << " histo=" << histogram_name << "." << std::endl;

    receiver.getHistograms(serial_number, rod_name, conn_name, histogram_name, histo_array);

    TFile output(output_file.c_str(),"RECREATE");
    if (histo_array.hasValidInfo()) {
      PixA::Index_t index;

      histo_array.info().makeDefaultIndexList(index);

      if ( index.size() > 0) {
	PixA::Index_t min_index(index);
	// loop over all indices
	for( ;index[0]<histo_array.info().maxIndex(0); ) {
	  TH1 *h1= histo_array.histo(index);
	  //	  if (h1) {
	    //	    std::string full_name(histogram_pattern);
	  //	    full_name += h1->GetName();
	  //	    h1->SetName(full_name.c_str());
	  //	  }
	  output.cd();
	  h1->Write();
	  std::cout << "INFO [" << argv[0] << ":main] write " << h1->GetName() << "." << std::endl;
	  h1->SetDirectory(0);

	  // next index;
	  unsigned int level_i=index.size();
	  for (; level_i-->0; ) {
	    if (index[level_i]+1<histo_array.info().maxIndex(level_i)) {
	      index[level_i]++;
	      break;
	    }
	    else {
	      index[level_i]=min_index[level_i];
	    }
	  }
	  if (level_i>index.size()) break;
	}
      }

    }
    }
    catch(daq::is::Exception &err) {
      std::cout << "ERROR [" << argv[0] << "] An is exception occured :" << err.message() << std::endl;
    }
    catch(CAN::BaseException &err) {
      std::cout << "ERROR [" << argv[0] << "] A base exception occured :" << err.getDescriptor() << std::endl;
    }
    catch(std::exception &err) {
      std::cout << "ERROR [" << argv[0] << "] An standard exception occured :" << err.what() << std::endl;
    }
    catch(...) {
      std::cout << "ERROR [" << argv[0] << "] An unknown exception occured." << std::endl;
    }
    return 0;
}
