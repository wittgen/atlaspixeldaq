#ifndef _ScanResultStreamer_h_
#define _ScanResultStreamer_h_

#include <memory>
#include <set>
#include <map>
#include <vector>

#include <ConfigWrapper/ConfigHandle.h>
#include <ConfigWrapper/Connectivity.h>
#include <ConfigWrapper/DbRef.h>

#include <DataContainer/IScanResultListener.h>
namespace PixLib {
  class DbRecord;
  class PixDbInterface;
}

namespace PixA {

  class ScanResultStreamer {
  public:
    ScanResultStreamer(const std::string &orig_file_name, const std::string &label_name="");

    ScanResultStreamer(DbHandle &root_handle, const std::string &label_name="");

    /** Process all labels of the  the file.
     */
    void read(const std::string &pp0name="");

    /** Process one label of the file.
     */
    void readLabel(const std::string &label_name, const std::string &pp0name="");

    /** Only read the labels but do not process the lables.
     */
    void readLabels();

    /** Get a connectivity for the given scanInfo.
     */
    static PixA::ConnectivityRef connectivity(const ConfigHandle &scan_info);

  private:
    void setLabel(const std::string &label_name);

    void readCurrentLabel(const std::string &mname="");
  public:

    void setListener(IScanResultListener *listener) { m_listener=std::unique_ptr<IScanResultListener>(listener);}

    IScanResultListener *releaseListener()          { return m_listener.release();}

  protected:

    bool createGlobaleConfigList(DbHandle &root_record, const std::string &rod_name, const std::string &module_name);

    bool createConfigListFromConnectivity(const std::string &rod_name);

    bool processModulesFromConnectivity(const std::set<std::string> &module_names,  
					const DbHandle &histo_folder_handle,
					const std::vector< DbHandle > &pix_scan_histo_list,
					const PixA::ConfigHandle &pix_scan_config,
					const PixA::ConfigHandle &boc_config,
					const std::string &rod_name,
					unsigned int scan_number);

    bool processModulesFromConfigList(const std::set<std::string> &module_names,  
				      const DbHandle &histo_folder_handle,
				      const std::vector< DbHandle > &pix_scan_histo_list,
				      const PixA::ConfigHandle &pix_scan_config,
				      const PixA::ConfigHandle &boc_config,
				      unsigned int scan_number);

    PixA::ConfigHandle modConf(const std::string &module_prod_name);

    std::string m_fileName;
    DbHandle m_rootHandle;
    DbHandle m_labelHandle;
    DbHandle m_groupHandle;

    PixA::ConnectivityRef m_conn;

    std::map<std::string, PixA::ConfigHandle> m_configList;
    std::map<std::string, PixA::ConfigHandle> m_globalConfigList;

    std::unique_ptr<IScanResultListener>    m_listener;

    static std::set<std::string> s_histoTypeList;
    static void initHistoTypes();

    std::string m_requested_pp0name;
  };

}
#endif
