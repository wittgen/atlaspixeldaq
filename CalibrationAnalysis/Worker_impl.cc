#include "Worker_impl.hh"
//#include <mrs/message.h>
#include "Scheduler.hh"
#include "Scheduler_impl.hh"
#include "IConfigDb.hh"

#include "IPCServerInstance.hh"

#include "MasterDataServer.hh"
#include "ArchivingUnit.hh"


#include <signal.h>

namespace CAN {

  // @todo the data server should be replaced by a master data server which provides per serial number and rod name a specifig

  Worker_impl::Worker_impl(IPCPartition &ipc_partition,
			   //IPCPartition &mrs_partition,
			   const std::string &can_is_server_name,
			   IMasterDataServer * masterDataServer,
			   std::vector<IResultStore *> result_store_list,
			   const std::string &name,
			   unsigned int n_slots)
			   //,RestartThread *restart_thread)

        : IPCNamedObject<POA_CAN::Worker,ipc::single_thread>(ipc_partition,name),
	  //m_restartThread(restart_thread),
	  //m_out(new MRSStream(mrs_partition)),
	  m_partition(ipc_partition),
	  m_isDictionary(ipc_partition),
	  m_dataServer(masterDataServer),
	  m_executionUnit(//mrs_partition,
			  m_isDictionary,
			  *m_dataServer,
			  //new ArchivingUnit(mrs_partition, m_isDictionary, result_store_list,kInfoLog),
			  new ArchivingUnit(m_isDictionary, result_store_list,kInfoLog),
			  n_slots,
			  *this),
	  //m_analysisRequestProcessor(mrs_partition,m_isDictionary, can_is_server_name, m_executionUnit),
	  m_analysisRequestProcessor(m_isDictionary, can_is_server_name, m_executionUnit),
	  m_ident(ipc_partition.name()+":"+name),
	  m_logLevel(kInfoLog),
	  m_registered(false)
  {
//      std::cout << "worker 1" << std::endl;
    setLogLevel(m_logLevel);
    m_restartThread=nullptr;
    //m_restartThread->setWorker(*this);

    Scheduler_var m_handle;
    bool scheduler_comm_error=false;
    try {
//        std::cout << "worker 2" << std::endl;
      m_handle = ipc_partition.lookup<Scheduler>(s_schedulerName) ;
      if (CORBA::is_nil(m_handle) || m_handle->_non_existent()) {
	scheduler_comm_error=true;
      }
    }
    catch (CORBA::TRANSIENT& err) {
      scheduler_comm_error=true;
    }
    catch (CORBA::COMM_FAILURE &err) {
      scheduler_comm_error=true;
    }
    catch (...) {
      scheduler_comm_error=true;
    }

    if (scheduler_comm_error) {
//std::cout << "worker 3" << std::endl;
      m_analysisRequestProcessor.initiateShutdown();
      m_dataServer->initiateShutdown();
      m_executionUnit.initiateShutdown(true);

      std::stringstream message;
      message << "Did not get a valid CORBA handle for the scheduler " << s_schedulerName << " in partition " << ipc_partition.name() << ".";

      m_analysisRequestProcessor.shutdownWait();
      m_dataServer->shutdownWait();
      m_executionUnit.shutdownWait();
      m_restart=true;
      //throw MRSException("NOSCHED",MRS_FATAL,"CAN::Worker_impl::ctor",message.str());
      throw MRSException(PixLib::PixMessages::FATAL,"CAN::Worker_impl::ctor",message.str());
    }
//std::cout << "worker 4" << std::endl;
    m_logLevel=static_cast<CAN::ELogLevel>(m_handle->registerWorker(_this(), m_ident.c_str(), n_slots));
    setLogLevel(m_logLevel);
    m_registered=true;
    m_dataServer->start(m_executionUnit);

    // the workers are not public
    //    publish();
    IPCServerInstance::run();
  }

//   /** Starts the dummy data server used by the test plugin
//    */
//   Worker_impl::Worker_impl(IPCPartition &ipc_partition,
// 			   IPCPartition &mrs_partition,
// 			   const std::string &name,
// 			   unsigned int n_slots)

//         : IPCNamedObject<POA_CAN::Worker,ipc::single_thread>(ipc_partition,ipc_partition.name()+name),
// 	  m_out(new MRSStream(mrs_partition)),
// 	  m_partition(ipc_partition),
// 	  m_isDictionary(ipc_partition),
// 	  m_dataServer(new DummyMasterDataServer(mrs_partition)),
// 	  m_executionUnit(mrs_partition,m_isDictionary, *m_dataServer, n_slots),
// 	  m_analysisRequestProcessor(mrs_partition,m_isDictionary,m_executionUnit),
// 	  m_ident(ipc_partition.name()+name),
// 	  m_registered(false)

//   {
//     Scheduler_var m_handle(ipc_partition.lookup<Scheduler>(s_schedulerName) );

//     if (CORBA::is_nil(m_handle) || m_handle->_non_existent()) {
//       std::stringstream message;
//       message << "Did not get a valid CORBA handle for the scheduler " << s_schedulerName << " in partition " << ipc_partition.name() << ".";
//       throw MRSException("NOSCHED",MRS_FATAL,"CAN::Worker_impl::ctor",message.str());
//     }

//     m_handle->registerWorker(_this(), m_ident.c_str(), n_slots);
//     m_registered=true;
//     m_dataServer->start(m_executionUnit);
//     IPCServerInstance::run();
//     //    publish();
//   }

  Worker_impl::~Worker_impl()
  {
//      std::cout << "worker 5" << std::endl;
    if (m_registered) {
      bool scheduler_comm_error=false;

      try {
//          std::cout << "worker 6" << std::endl;
	Scheduler_var m_handle(m_partition.lookup<Scheduler>(m_partition.name()+"_Manitu") );
	if (!CORBA::is_nil(m_handle) || !Scheduler::_narrow(m_handle.in())) {
	  m_handle->deregisterWorker(_this());
	  m_registered=false;
 //       std::cout << "worker 7" << std::endl;
	}
      }
      catch (CORBA::TRANSIENT& err) {
	scheduler_comm_error=true;
      }
      catch (CORBA::COMM_FAILURE &err) {
	scheduler_comm_error=true;
      }
      catch(...) {
	scheduler_comm_error=true;
      }

      if (scheduler_comm_error && m_logLevel>=kInfoLog) {
//          std::cout << "worker 8" << std::endl;
	std::stringstream message;
	message << "Did not manage to deregister myself from scheduler "<< s_schedulerName << " in partition " << m_partition.name() << ". Scheduler is not reachable.";
// 	sendMessage(*m_out,"WORKER_dtor","WORKER",MRS_INFORMATION, "CAN::Worker_impl::shutdown", message.str() );
	ers::info(PixLib::pix::daq (ERS_HERE, "CAN::Worker_impl::shutdown", message.str() ));
      }

    }
  }

  void Worker_impl::submitAnalysis(SerialNumber_t    analysis_serial_number,
				   const AnalysisInfoData_t &analysis_info,
				   const StringVec &rod_conn_name_list)
  {
//      std::cout << "worker 9" << std::endl;
    m_analysisRequestProcessor.queueRequest(analysis_serial_number,analysis_info, rod_conn_name_list);
  }

  bool Worker_impl::dequeuAnalysis(SerialNumber_t    analysis_serial_number,
				   const char *rod_conn_name)
  {
//      std::cout << "worker 10" << std::endl;
    return m_executionUnit.dequeuAnalysis(analysis_serial_number,rod_conn_name);
  }

  void Worker_impl::getQueueStatus(CORBA::ULong &jobs_waiting_for_data,
				   CORBA::ULong &jobs_waiting,
				   CORBA::ULong &jobs_running,
				   CORBA::ULong &jobs_exectued,
				   CORBA::ULong &empty_slots)
  {
//      std::cout << "worker 11" << std::endl;
    m_executionUnit.getQueueStatus(jobs_waiting_for_data, jobs_waiting, jobs_running, jobs_exectued, empty_slots);
  }

  /** Abort all pending, waiting, running tasks associated to the given analysis serial  number.
   */
  void Worker_impl::abortAnalysis(SerialNumber_t    analysis_serial_number, bool ignore_results) {
//      std::cout << "worker 12" << std::endl;
    if (m_logLevel>=kDiagnosticsLog) {
//       sendMessage(*m_out,"WORKER_abortAnalysis","WORKER",MRS_DIAGNOSTIC, "CAN::Worker_impl::abortAnalysis", "abort anlysis requests." );
      ers::debug(PixLib::pix::daq (ERS_HERE, "CAN::Worker_impl::abortAnalysis", "abort anlysis requests." ));
    }
    m_analysisRequestProcessor.abortAnalysis(analysis_serial_number);

    if (m_logLevel>=kDiagnosticsLog) {
//       sendMessage(*m_out,"WORKER_abortAnalysis","WORKER",MRS_DIAGNOSTIC, "CAN::Worker_impl::abortAnalysis", "abort data requests." );
      ers::debug(PixLib::pix::daq (ERS_HERE, "CAN::Worker_impl::abortAnalysis", "abort data requests." ));
    }
    m_dataServer->abort(analysis_serial_number);

    if (m_logLevel>=kDiagnosticsLog) {
//       sendMessage(*m_out,"WORKER_abortAnalysis","WORKER",MRS_DIAGNOSTIC, "CAN::Worker_impl::abortAnalysis", "abort analysis execution." );
      ers::debug(PixLib::pix::daq (ERS_HERE,"CAN::Worker_impl::abortAnalysis", "abort analysis execution." ));
    }
    m_executionUnit.abortAnalysis(analysis_serial_number, ignore_results);
  }

  /** Abort analysis of the given serial number and rod name.
   */
  void Worker_impl::abortJob(SerialNumber_t analysis_serial_number, const char *rod_conn_name, bool ignore_results) {
 //     std::cout << "worker 13" << std::endl;
    m_analysisRequestProcessor.abortAnalysis(analysis_serial_number);
    m_executionUnit.abortAnalysis(analysis_serial_number, rod_conn_name, ignore_results);
  }

  void Worker_impl::reset() {
//      std::cout << "worker 14" << std::endl;
    if (m_logLevel>=kDiagnosticsLog) {
//       sendMessage(*m_out,"WORKER_resetANAREQ","WORKER",MRS_DIAGNOSTIC, "CAN::Worker_impl::reset", "Reset analysisRequestProcessor." );
      ers::debug(PixLib::pix::daq (ERS_HERE, "CAN::Worker_impl::reset", "Reset analysisRequestProcessor." ));
    }
    m_analysisRequestProcessor.reset();

    if (m_logLevel>=kDiagnosticsLog) {
//       sendMessage(*m_out,"WORKER_resetDATASRV","WORKER",MRS_DIAGNOSTIC, "CAN::Worker_impl::reset", "Reset data server." );
      ers::debug(PixLib::pix::daq (ERS_HERE, "CAN::Worker_impl::reset", "Reset data server." ));
    }
    m_dataServer->reset();
    
    if (m_logLevel>=kDiagnosticsLog) {
//       sendMessage(*m_out,"WORKER_resetANAEXEC","WORKER",MRS_DIAGNOSTIC, "CAN::Worker_impl::shutdown", "Reset execution unit." );
      ers::debug(PixLib::pix::daq (ERS_HERE, "CAN::Worker_impl::shutdown", "Reset execution unit.") );
    }
    m_executionUnit.reset();
    if (m_logLevel>=kDiagnosticsLog) {
//       sendMessage(*m_out,"WORKER_reset","WORKER",MRS_DIAGNOSTIC, "CAN::Worker_impl::reset", "Worker reset complete." );
      ers::debug(PixLib::pix::daq (ERS_HERE, "CAN::Worker_impl::reset", "Worker reset complete." ));
    }
  }

  void Worker_impl::_shutdown() {
//      std::cout << "worker 15" << std::endl;
    bool scheduler_comm_error=false;

    try {
      Scheduler_var m_handle(m_partition.lookup<Scheduler>(m_partition.name()+"_Manitu") );
      if (!CORBA::is_nil(m_handle)) {
	m_handle->deregisterWorker(_this());
	m_registered=false;
      }
    }
    catch (CORBA::TRANSIENT& err) {
      scheduler_comm_error=true;
    }
    catch (CORBA::COMM_FAILURE &err) {
      scheduler_comm_error=true;
    }
    catch(...) {
      scheduler_comm_error=true;
    }
    if (scheduler_comm_error && m_logLevel>=kWarnLog) {
      std::stringstream message;
      message << "Did not manage to deregister myself from scheduler "<< s_schedulerName << " in partition " << m_partition.name() << ".";
      //       sendMessage(*m_out,"WORKER_shutdown","WORKER",MRS_WARNING, "CAN::Worker_impl::shutdown", message.str() );
      ers::warning(PixLib::pix::daq (ERS_HERE, "CAN::Worker_impl::shutdown", message.str() ));
    }

    m_analysisRequestProcessor.initiateShutdown();
    m_dataServer->initiateShutdown();
    m_executionUnit.initiateShutdown(true);

    m_analysisRequestProcessor.shutdownWait();
    m_dataServer->shutdownWait();
    m_executionUnit.shutdownWait();

    if (scheduler_comm_error && m_logLevel>=kInfoLog) {
      std::stringstream message;
      message << "Shutting down worker " << m_ident << ".";
//       sendMessage(*m_out,"WORKER_shutdown","WORKER",MRS_INFORMATION, "CAN::Worker_impl::shutdown", message.str() );
      ers::info(PixLib::pix::daq (ERS_HERE, "CAN::Worker_impl::shutdown", message.str() ));
    }
    //_destroy();
    //m_restartThread->done();
    //IPCServerInstance::stop();
  }

  void Worker_impl::_restart() {
//      std::cout << "worker 16" << std::endl;
    m_restart=true;
    _shutdown();
  }

  bool Worker_impl::alive()  {
 //     std::cout << "worker 17" << std::endl;
    MasterDataServer *master_data_server = dynamic_cast<MasterDataServer *>(m_dataServer.get());
    if (master_data_server) {
      master_data_server->dump();
    }

    return true;
  }

  void Worker_impl::setLogLevel(unsigned short level) {
//      std::cout << "worker 18" << std::endl;
    m_logLevel=static_cast<ELogLevel>(level);

    m_dataServer->setLogLevel(m_logLevel);
    m_executionUnit.setLogLevel(m_logLevel);
    m_analysisRequestProcessor.setLogLevel(m_logLevel);

    {
      std::stringstream message;
      message << "Log level is changed to " << m_logLevel <<".";
//       sendMessage(*m_out,"WORKER_log","WORKER", MRS_DIAGNOSTIC, "CAN::Worker_impl::setLogLevel",message.str());
      ers::debug(PixLib::pix::daq (ERS_HERE, "CAN::Worker_impl::setLogLevel",message.str()));
    }

  }


  void RestartThread::main() {
    {
//        std::cout << "worker 19" << std::endl;
      std::stringstream message;
      message << "Restart thread waiting "<<".";
//       sendMessage(*m_out,"RestartThread_start","RestartThread", MRS_DIAGNOSTIC, "CAN::RestartThread::run_undetached",message.str());
      ers::debug(PixLib::pix::daq (ERS_HERE, "CAN::RestartThread::run_undetached",message.str()));
    }
    m_restart.wait(shutdownFlag());

    bool kill_worker=false;
    {
      std::stringstream message;
      message << "Restart was triggered "<<".";
//       sendMessage(*m_out,"RestartThread_triggered","RestartThread", MRS_DIAGNOSTIC, "CAN::RestartThread::run_undetached",message.str());
      ers::debug(PixLib::pix::daq (ERS_HERE,"CAN::RestartThread::run_undetached",message.str()));
    }
    if (!shutdownFlag() && m_worker) {
      {
	std::stringstream message;
	message << "Issue restart "<<".";
// 	sendMessage(*m_out,"RestartThread_restart","RestartThread", MRS_DIAGNOSTIC, "CAN::RestartThread::run_undetached",message.str());
	ers::debug(PixLib::pix::daq (ERS_HERE, "CAN::RestartThread::run_undetached",message.str()));
      }
      if (m_kill) {
	m_worker->_shutdown();
      }
      else {
	m_worker->_restart();
      }
      kill_worker=true;
    }
    kill_worker &= !m_workerIsDone.timedwait(20);
    if (kill_worker) {
      {
	std::stringstream message;
	message << "Send SIGTERM "<<".";
// 	sendMessage(*m_out,"RestartThread_terminate","RestartThread", MRS_DIAGNOSTIC, "CAN::RestartThread::run_undetached",message.str());
	ers::debug(PixLib::pix::daq (ERS_HERE, "CAN::RestartThread::run_undetached",message.str()));
      }
      ::kill(getpid(),SIGTERM);
      sleep(20);
      {
	std::stringstream message;
	message << "Send SIGKILL "<<".";
// 	sendMessage(*m_out,"RestartThread_KILL","RestartThread", MRS_DIAGNOSTIC, "CAN::RestartThread::run_undetached",message.str());
	ers::debug(PixLib::pix::daq (ERS_HERE, "CAN::RestartThread::run_undetached",message.str()));
      }
      ::kill(getpid(),SIGKILL);
    }

    {
      std::stringstream message;
      message << "Exit "<<".";
//       sendMessage(*m_out,"RestartThread_exit","RestartThread", MRS_DIAGNOSTIC, "CAN::RestartThread::run_undetached",message.str());
      ers::debug(PixLib::pix::daq (ERS_HERE, "CAN::RestartThread::run_undetached",message.str()));
    }
  }


}
