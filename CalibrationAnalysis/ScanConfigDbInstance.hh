/* Dear emacs, this is -*-c++-*- */
#ifndef _CAN_ScanConfigDbInstance_hh_
#define _CAN_ScanConfigDbInstance_hh_

#include "IConfigDb.hh"

namespace CAN {

  class ScanConfigDbInstance
  {
  public:
    static IConfigDb *create();

    static const char *scanClassName() { return s_scanClassName; }
  private:
    static const char *s_scanClassName;
  };

}
#endif
