#!/bin/sh

top=${CAN%Applications/Pixel/CalibrationAnalysis}
can_src=${top}/Applications/Pixel/CalibrationAnalysis
can_src=${CAN}
pixcon_src=${top}/Applications/Pixel/CalibrationConsole

can_partition=${USER}_test
can_is_name=RunParams
scan_is_name=RunParams

id_tag="CT"
conn_tag="TOOTHPIX-2008-V6"
cfg_tag="TOOTHPIX"

can_tag=TEST

# db_server_flag="--db-server-name PixelDbServer --db-server-partition Partition_PixelInfrastructure"
# db_server_flag="--db-server-name PixelDbServer --db-server-partition ${USER}_test"
db_server_flag=""

rat_flags="-o PixelHistoServer --scan-is-name RunParams"
# rat_flags=" -o \"\" "

simu=echo

level=1
work_dir=""
while test -n "$1";  do
    case $1 in
	(-l) 
	   shift
	   level=$1
	   ;;
	(-d) 
	   shift
	   work_dir=$1
	   ;;
	(-x) 
	   simu=""
	   ;;
    esac
    shift
done

echo "----- start : `date`"

if test -z "$work_dir"; then
  mkdir -p /tmp/${USER} || exit -1
  if test -n "$simu"; then
     work_dir=/tmp/${USER}/can_test.XXXXXXXXXX
  else
     work_dir=$(mktemp -d /tmp/${USER}/can_test.XXXXXXXXXX)
     echo "tmp work dir = ${work_dir}"
  fi
fi
echo " work dir : ${work_dir} "
${simu} mkdir -p ${work_dir} || exit -1

echo " start level : ${level} "



if test -f ${top}/env.sh; then
:
else
  exit -1
fi

if test -d ${can_src}; then
:
else
  exit -1
fi

if test -d ${pixcon_src}; then
:
else
  exit -1
fi

. ${top}/env.sh
. ${CAN}/test_setup.sh;



export CAN_DB_TOP=${work_dir}

export PIX_METADATA_DB=sqlite_file:${CAN_DB_TOP}/meta.sql
export CAN_CALIB_DB=sqlite_file:${CAN_DB_TOP}/dbcalib.sql

export CAN_CFG_PATH=${CAN_DB_TOP}/CAN_cfg
export PIXSCAN_CFG_PATH=${CAN_DB_TOP}/scan_cfg

export PIX_CFG_PREF=/tmp/goetz/test/db
mkdir -p ${PIX_CFG_PREF} || exit -1

for dir in ${CAN_CFG_PATH} ${PIXSCAN_CFG_PATH}; do
  mkdir -p ${dir} || exit -1
done

if test $level -le 1; then
echo " - 1 : create scan parameter db "
(
cd ${pixcon_src} || exit -1
if test -x ./Console/test_createScanParamDb; then
  :
else
 (cd Console || exit -1
  make -f test_Makefile test_createScanParamDb ) || exit -1
fi
if test -x ./Console/test_createScanParamDb; then
  :
else
  exit -1
fi
${simu} ./Console/test_createScanParamDb
)
fi

cd ${can_src} || exit -1

if test $level -le 2; then
echo " - 2 : create parameters of analyses "

# ${simu} ./configWriter/configWriter -a TestAnalysis ${can_tag} 0 --no-gui  -s DataSrv_histogramName "" -s DataSrv_scanName "" \
# -s DataSrv_needScanCfg t -s DataSrv_needModuleCfg t \
# -s test_analysisTime 50 || exit -1

# ${simu} ./configWriter/configWriter -a TestAnalysis TwoScans_${can_tag} 0 --no-gui  -s DataSrv_histogramName "" -s DataSrv_scanName "digital" -s DataSrv_scanName "analog" \
# -s DataSrv_needScanCfg t -s DataSrv_needModuleCfg t \
# -s test_analysisTime 50 || exit -1

#
# @todo upload histograms to the OH server and test also the histogram download

${simu} ./configWriter/configWriter -a TestAnalysis ${can_tag} 0 --no-gui  -s DataSrv_histogramName "" -s test_analysisTime 50 \
-s DataSrv_scanName "" \
-s Results_addFloatResult true \
-s Results_addStateResult true \
-s Results_addAverageResult true \
-s Results_addBoolResult true \
-s Results_addSpecialPixelMapResult true \
|| exit -1

# -s test_allowForMissingHistograms false  -s test_analysisTime 50

${simu} ./configWriter/configWriter -a TestClassification ${can_tag} 0 --no-gui || exit -1
${simu} ./configWriter/configWriter -a TestPostProcessing ${can_tag} 0 --no-gui || exit -1
fi

if test $level -le 3; then
echo " - 3 : fake scan meta data "
${simu} rm -f ${PIX_METADATA_DB#sqlite_file:}
${simu} ./test_scanMetaDataWriter -p ${can_partition} -t ${conn_tag} ${cfg_tag} -n DIGITAL_TEST -r ${work_dir}/result.root -f `find ${PIXSCAN_CFG_PATH}/DIGITAL_TEST -name "*.root" | head -n 1` || exit -1
${simu} ./test_scanMetaDataWriter -p ${can_partition} -t ${conn_tag} ${cfg_tag} -n ANALOG_TEST -r ${work_dir}/result.root -f `find ${PIXSCAN_CFG_PATH}/DIGITAL_TEST -name "*.root" | head -n 1` || exit -1
fi


pid_list=""

if test $level -le 4; then
echo " - 4 : start manitu "
  TDAQ_APPLICATION_NAME=manitu TDAQ_APPLICATION_OBJECT_ID=manitu.$(hostname) ${simu} ./manitu  -p ${can_partition} --can-is-name ${can_is_name} ${db_server_flag} &
pid_list="$! ${pid_list}"
  echo $pid_list
${simu} sleep 3
fi

monitor_string=""

number=1001


echo "       cleanup is "
${simu} ./canIsCleaner -p ${can_partition} -n ${can_is_name} -e ".*A0.*/globalStatus"  || exit -1
${simu} ./canIsCleaner -p ${can_partition} -n ${can_is_name} -e ".*A0.*/status" || exit -1

echo "       setting the scan status to DONE in bg"
 ( for scan_number in 1001 1002; do 
      for rod in ROD_C1_S14 ROD_C1_S5 ROD_C1_S21 ROD_C1_S12 ROD_C1_S17 ROD_C0_S15 ROD_C0_S20 ROD_C1_S17 ROD_C1_S12 ROD_C0_S15  ROD_C0_S11  ROD_C1_S5; do
         ${CAN}/test_setScanStatus -p ${can_partition} -n ${can_is_name} -s ${scan_number} -r ${rod} -t DONE;
      done
   done ) &


${simu} rm ${CAN_CALIB_DB#sqlite_file:}

for pass in 1 2; do

  if test $level -le 5; then
  echo " - 5 : start rat "

  if test $pass = 1; then
    rats=1;
  else
    rats="2"
  fi
 
  for i in `echo $rats`; do
    echo rat.$(hostname).${i} ${simu} ./rat  -p ${can_partition} --can-is-name ${can_is_name} ${rat_flags} ${db_server_flag} -t 2
    echo  ${can_partition} --can-is-name ${can_is_name} ${rat_flags} ${db_server_flag} -t 2 \
    | TDAQ_APPLICATION_NAME=rat.${i} TDAQ_APPLICATION_OBJECT_ID=rat.$(hostname).${i} ${simu} xargs ./rat &
    rat_pid=$!
    pid_list="$rat_pid ${pid_list}"
    if test $i = 2; then
       bad_rat_pid=$rat_pid
    fi
  done
  ${simu} sleep 3
  fi

# echo "wait 20s"
# sleep 20
# echo "continue"

  if test $level -le 6; then
    echo " - 6 : check queues "
    ${simu} ./call_manitu -p ${can_partition} -q
  fi

  if test $level -le 7; then
    echo " - 7 : submit an analysis "

echo "  CALIB DB at pass $pass"
 echo ".tables
select * from CALIB_KEYGEN;" |   sqlite3 ${CAN_CALIB_DB#sqlite_file:}

    if test $pass = 1; then
       submit_jobs="1 2"
    else
       submit_jobs="2 3 4 5 6 7 8 9 10"
       #" 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30"
    fi

    for i in `echo $submit_jobs`; do
      serial_number="-s 1001"
      the_can_tag=${can_tag}
#      if test $i = 2; then
#	  serial_number="-s digital:1001 -s analog:1002"
#	  the_can_tag=TwoScans_${can_tag}
#      fi
      ${simu} ./test_submitAnalysis -p ${can_partition} -a TestAnalysis ${the_can_tag} 0 -c TestClassification ${can_tag} 0 -x TestPostProcessing ${can_tag} 0 `echo ${serial_number}`
      monitor_string=${monitor_string}"-m $number "
      number=$(($number + 1))
      if test $i = 9; then
        ${simu} kill $bad_rat_pid
      fi
    done


    # get queue status
    ${simu} ./call_manitu -p ${can_partition} -q || exit -1


    # monitor submitted analysis and wait until they are done
    result=false;
    if test -n "${monitor_string}"; then
      echo "monitor scans  ${monitor_string} on  ${can_is_name}"
      if (${simu} ./test_isReader -p ${can_partition} -n ${can_is_name} ${monitor_string}); then
          result=true
      else
          result=false;
      fi
    fi
  # get queue status
  #  all queues should be empty, the worker should still be running
  ${simu} ./call_manitu -p ${can_partition} -q || exit -1
  fi

done 
#pass


echo " kill ${bad_rat_pid}  "
${simu} sleep 5
${simu} kill $bad_rat_pid

# list global status
${simu} is_ls -p ${can_partition} -n ${can_is_name} -R ".*A.*/globalStatus" -v

# get queue status again
#  Can not harm, can it ?
${simu} ./call_manitu -p ${can_partition} -q


if test $level -le 8; then
echo " - 8 : shutdown CAN infrastructure "
${simu} ./call_manitu -p ${can_partition} -q
${simu}  ./call_manitu -p ${can_partition} -k
${simu}  sleep 5
fi


echo "   kill the rats and manitu if they are not dead yet."
for i in `echo $pid_list`; do
  ${simu}   kill $i
done


echo "  result = $result"
echo "----- done : `date` "
$result
