#ifndef _CAN_AnalysisMonitor_hh_
#define _CAN_AnalysisMonitor_hh_

#include "Flag.hh"
#include "Lock.hh"

#include "AnalysisJobList_t.hh"

#include <string>

#include <is/infodictionary.h>

#include "IsInfoReceiverThread.hh"
#include "IsDictionary.hh"

class IPCPartition;
//#include <mrs/message.h>

#include "LogLevel.hh"

namespace CAN {

  class Scheduler_impl;

  class AnalysisMonitor : public omni_thread
  {
  private:

  public:
    //    AnalysisMonitor(IPCPartition &ipc_partition, IPCPartition &mrs_partition, const std::string &is_server_name, Scheduler_impl &scheduler);
    AnalysisMonitor(IPCPartition &ipc_partition, const std::string &is_server_name, Scheduler_impl &scheduler);

    IPCPartition &partition()         { return *m_partition; }
    const std::string &isServerName() { return m_isServerName; }

    void setLogLevel(ELogLevel log_level) { m_logLevel = log_level; }

    /** Abort the monitoring task.
     */
    void abort()   { m_abort =true; m_reset=true; setFlag(); }

    //    /** wait until a signal arrives.
    //     */
    void wait()    { m_flag.wait(m_reset); }
    void setFlag() { m_flag.setFlag(); }

    ISInfoReceiver   &isReceiver() { return m_isListener.receiver(); }
    ISInfoDictionary &dictionary() { return m_isDictionary.dictionary(); }
    Mutex   &mutex()               { return m_isDictionary.mutex(); }

    /** Get a job list which is already monitored for the given serial number if it exists.
     * @param analysis_serial_number the analysis serial number
     * @return the job list or an invalid pointer.
     */
    std::shared_ptr<AnalysisJobList_t> searchJobList(SerialNumber_t analysis_serial_number) 
    {
      JobQueue_t::iterator list_iter = m_queuedJobs.find( analysis_serial_number );
      if (list_iter != m_queuedJobs.end()) {
	return list_iter->second;
      }
      return std::shared_ptr<AnalysisJobList_t>();
    }

    /** Add a new job to the list of monitored jobs.
     * @param analysis_serial_number the analysis serial number
     * @param analysis_job_list the job list
     * @return true if the analysis job was added successfully.
     */
    bool addJobList(SerialNumber_t analysis_serial_number, const std::shared_ptr<AnalysisJobList_t> &analysis_job_list) {
      Lock submit_lock(m_jobQueueMutex);
      JobQueue_t::iterator analysis_iter = m_queuedJobs.find(analysis_serial_number);
      if ( analysis_iter == m_queuedJobs.end()) {
	std::pair<JobQueue_t::iterator,bool> ret = m_queuedJobs.insert( std::make_pair(analysis_serial_number,analysis_job_list));
	return ret.second;
      }
      else {
	if (analysis_iter->second->abort() && analysis_iter->second->isPlaceHolder()) {
	  analysis_iter->second = analysis_job_list;
	  analysis_iter->second->setAbort();
	  abortAllRunningJobs(*analysis_iter);
	  return true;
	}
	else {
	  if (analysis_job_list.get() == analysis_iter->second.get()) {
	    // the analysis joblist was already registered.
	    return true;
	  }
	  else {
	    return false;
	  }
	}
      }
    }

    /** Return true if the given analysis is to be aborted
     */
    bool toBeAborted(SerialNumber_t analysis_serial_number) {
      Lock submit_lock(m_jobQueueMutex);
      JobQueue_t::iterator analysis_iter = m_queuedJobs.find(analysis_serial_number);
      if (    analysis_iter != m_queuedJobs.end()
	   && analysis_iter->second->abort()) {
	return true;
      }
      else {
	return false;
      }
    }

    void markForAbortion(SerialNumber_t analysis_serial_number) {
      Lock submit_lock(m_jobQueueMutex);
      JobQueue_t::iterator analysis_iter = m_queuedJobs.find(analysis_serial_number);
      if (    analysis_iter != m_queuedJobs.end()) {
	analysis_iter->second->setAbort();
      }
      //      else {
      //	m_queuedJobs.insert( std::make_pair(analysis_serial_number,AnalysisJobList_t::createAnalysisListForAbortion()));
      //      }
    }

    /** Only set the global status to aborted.
     * Should only be used when an analysis was not submitted yet.
     */
    void setGlobalStatusToAborted(SerialNumber_t analysis_serial_number);

    /** Return true if there is already a joblist for the given serial number
     * @param analysis_serial_number serial number of the analysis
     */
    bool haveJobList(SerialNumber_t analysis_serial_number) {
      Lock submit_lock(m_jobQueueMutex);
      return ( m_queuedJobs.find(analysis_serial_number) != m_queuedJobs.end());
    }


    /** Signal the analysis monitor thread to abort. 
     */
    void shutdown() { abort(); }

    /** Signal the analysis monitor thread to abort and wait until the thread terminated.
     */
    void shutdownWait();

    /** Get The mutex to protect for parallel manipulation of the jobs queues.
     */
    Mutex &jobQueueMutex()  { return m_jobQueueMutex; };

    /** Reset list of monitored jobs.
     */
    void reset();

    /** Set the status of all running jobs of the given analysis to aborted.
     * This methods locks the queue mutex.
     */
    void abortAllRunningJobs(SerialNumber_t analysis_serial_number) {
      Lock lock(jobQueueMutex());

      JobQueue_t::iterator analysis_iter = m_queuedJobs.find(analysis_serial_number);
      if (analysis_iter != m_queuedJobs.end()) {
	 abortAllRunningJobs(*analysis_iter);
      }
    }

    unsigned int nJobs() const {
      return m_queuedJobs.size();
    }

  protected:

    void monitor();

    void *run_undetached(void *arg);

    /** Set the status of all running jobs to aborted.
     * This method does not lock the queue mutex i.e. the
     * calling method must lock it.
     */
    void abortAllRunningJobs();

    /** Set the status of all running jobs of the given analysis to aborted
     * This method does not lock the queue mutex i.e. the
     * calling method must lock it.
     */
    void abortAllRunningJobs(std::pair<const SerialNumber_t, std::shared_ptr<AnalysisJobList_t> > &analysis_job_list);


    /** Look for jobs which do not progress or whose worker is marked for removal
     * @return true if worker are to be removed and jobs need to be resubmitted.
     * This method does not lock the queue mutex i.e. the
     * calling method must lock it.
     */
    bool findCrashedWorker(const time_t &current_time);

    /** Determine the global status of an analysis.
     * @param analysis_status the analysis status of all jobletts of an analysis.
     * @return the new global status
     * The global status is the maximum of the analysis jobletts states of all jobletts with a state
     * below or equal running, where running is the maximum. If all jobletts have a state above running
     * the maximum state is taken, where the minimum is success, then timeout, then failure etc. .
     */
    AnalysisStatus::EStatus determineGlobalStatus(const std::pair<SerialNumber_t, std::shared_ptr<AnalysisJobList_t> > &analysis_status );

  private:

    /** Create a combined status from the given global status and the status of an individual joblett.
     */
    AnalysisStatus::EStatus combinedStatus(AnalysisStatus::EStatus global_status, AnalysisStatus::EStatus a_status)
    {

      // set the global status to the maximum of Submitted, Pending, Waiting, Running
      // or if the status is success, failure, timeout, aborted then set the status to
      // the maximum only if new_global_status is unknown or success, failure, timeout, aborted

      if (a_status>AnalysisStatus::kRunning) {
	if (global_status==AnalysisStatus::kUnknown || global_status>AnalysisStatus::kRunning) {
	  if (a_status > global_status) {

	    // kTimeOut has lower precedence as a failure
	    if (a_status!=AnalysisStatus::kTimeOut ||global_status==AnalysisStatus::kSuccess) {
	      global_status = a_status;
	    }
	  }
	}
      }
      else {
	//	    analysis_finished = false;
	if (a_status>global_status || global_status>AnalysisStatus::kRunning) {
	  global_status=a_status;
	}
      }
      return global_status;
    }

    /** Determin the meta data status from the global analysis status.
     */
    CAN::GlobalStatus metaDataStatus(AnalysisStatus::EStatus global_status) {

      CAN::GlobalStatus global_analysis_status=CAN::kNotSet;
      if (global_status>AnalysisStatus::kRunning) {
	switch (global_status) {
	case AnalysisStatus::kSuccess:
	  global_analysis_status=CAN::kSuccess;
	  break;
	case AnalysisStatus::kFailure:
	  global_analysis_status=CAN::kFailure;
	  break;
	case AnalysisStatus::kTimeOut:
	case AnalysisStatus::kAborted:
	  global_analysis_status=CAN::kAborted;
	  break;
	default:
	  break;
	}
      }
      return global_analysis_status;
    }


    void _reset();

    /** inhibited */
    void start() {}

    typedef std::map< SerialNumber_t, std::shared_ptr<AnalysisJobList_t> > JobQueue_t;
    JobQueue_t m_queuedJobs;
    Mutex m_jobQueueMutex;

    IPCPartition            *m_partition;
    std::string              m_isServerName;
    IsInfoReceiverThread     m_isListener;
    IsDictionary             m_isDictionary;
    Scheduler_impl          *m_scheduler;
//     Mutex                    m_mrsMutex;
//     MRSStream               *m_out;
    unsigned long            m_updateFreq;
    time_t                   m_timeOutSubmissionMax;   /**< Jobs which have the submitted status longer than this number of seconds will trigger a resubmissin.*/
    //    time_t                   m_timeOutSubmissionMin;   /**< All jobs which have the submitted status longer than this number of seconds will be resubmitted if a resubmission got triggered.*/
    time_t                   m_timeOutStart;           /**< If a woirker has a free slot for more than this time, and there are waiting jobs, then the worker is considered to have crashed.*/
    time_t                   m_timeOutRunningAndArchivingMax; /**< Jobs which run for longer than this number of seconds are considered to have failed.*/
    //    time_t                   m_timeOutRunningMin;             /**< Jobs which run for longer than this number of seconds are considered to have failed.*/
    time_t                   m_abortTimeMax;           /**< send abort again if no status change happens within this time span.*/
    time_t                   m_keepJobInfoTime;        /**< Delete a completed job from the job list, if it finished this number of seconds ago.*/

    unsigned int             m_nMissingResponsesMax;   /**< maximum number of responses that may miss from a worker.*/
    unsigned int             m_nResubmissionsMax;      /**< maximum number of attempts to resubmit jobs.*/

    ELogLevel                m_logLevel;

    Flag                     m_flag;
    Flag                     m_exitMonitoring;
    bool                     m_abort;
    bool                     m_reset;

    static const std::string s_componentName;
  };

}
#endif
