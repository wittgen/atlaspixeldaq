#include <MetaDataService.hh>
#include <iostream>
#include <memory>

#include <TH1.h>
#include <TH2.h>

#include <PixelCoralClientUtils/AnalysisResultList_t.hh>
#include <ExtendedAnalysisResultList_t.hh>
#include <PixelCoralClientUtils/CoralClient.hh>

#include <CoralDbClient.hh>

#include <ConfigWrapper/pixa_exception.h>
#include <ConfigWrapper/Connectivity.h>
#include <ConfigWrapper/ConnectivityManager.h>
#include <ConfigWrapper/ConnectivityUtil.h>
#include <ConfigWrapper/PixDisableUtil.h>

#include "sendMessage.hh"

#include "SealKernel/Context.h"

bool isEmpty(CAN::ExtendedAnalysisResultList_t &analysis_results) {
  return analysis_results.begin<bool>()==analysis_results.end<bool>()
    && analysis_results.begin<unsigned int>()==analysis_results.end<unsigned int>()
    && analysis_results.begin<float>()==analysis_results.end<float>()
    && analysis_results.begin<CAN::AverageResult_t>()==analysis_results.end<CAN::AverageResult_t>()
    && analysis_results.begin<PixelMap_t>()==analysis_results.end<PixelMap_t>();
}

int main(int argc, char **argv)
{
  std::vector<CAN::SerialNumber_t> analysis_serial_number;
  std::string src_db_conn_name;
  std::string dest_db_conn_name;
  bool simulation=true;
  bool verbose=false;
  bool error = false;

  bool list_only=false;
  std::vector<std::string> conn_name_list;
  std::string var_name;

  for (int arg_i=1; arg_i<argc; arg_i++) {
    if (strcmp(argv[arg_i],"-a")==0 && arg_i+1 < argc && isdigit(argv[arg_i+1][0])) {
      analysis_serial_number.push_back(atoi(argv[++arg_i]));
    }
    else if ( (strcmp(argv[arg_i],"--src")==0 || strcmp(argv[arg_i],"-s")==0) && arg_i+1 < argc ) {
      src_db_conn_name = argv[++arg_i];
    }
    else if ( (strcmp(argv[arg_i],"--dest")==0 || strcmp(argv[arg_i],"-d")==0) && arg_i+1 < argc) {
      dest_db_conn_name = argv[++arg_i];
    }
    else if ( (strcmp(argv[arg_i],"--execute")==0 || strcmp(argv[arg_i],"-x")==0)) {
      simulation=false;
    }
    else if ( (strcmp(argv[arg_i],"--verbose")==0 || strcmp(argv[arg_i],"-v")==0)) {
      verbose=true;
    }
    else if ( (strcmp(argv[arg_i],"--list")==0 || strcmp(argv[arg_i],"-l")==0)) {
      list_only=true;
    }
    else if ( (strcmp(argv[arg_i],"--conn-name")==0 || strcmp(argv[arg_i],"-c")==0)  && arg_i+1 < argc) {
      conn_name_list.push_back( argv[++arg_i]);
      list_only=true;
    }
    else if ( (strcmp(argv[arg_i],"--var-name")==0 || strcmp(argv[arg_i],"-n")==0)  && arg_i+1 < argc) {
      var_name = argv[++arg_i];
    }
    else {
      std::cout << "ERROR [" << argv[0] << ":main] unexpected argument " << arg_i << " : " << argv[arg_i] << std::endl
		<< std::endl;
      error = true;
    }
  }
  if (list_only && !simulation) {
    std::cout << "ERROR [" << argv[0] << ":main] \"list\" and \"execute\" are incompatible." << std::endl
	      << std::endl;
    error = true;
  }
  if (list_only && !analysis_serial_number.empty()) {
    std::cout << "ERROR [" << argv[0] << ":main] \"list\" and \"-a\" are incompatible." << std::endl
	      << std::endl;
    error = true;
  }
  if (!list_only && !var_name.empty()) {
    std::cout << "ERROR [" << argv[0] << ":main] \"execute\" and \"--var-name/-n\" are incompatible." << std::endl
	      << std::endl;
    error = true;
  }
  if (error) {
    std::cout << "usage: " << argv[0] << " --src/-s src-db-connection --dest/-d dest-db-connection -a serial-number [-a ...] " << std::endl
	      << std::endl
	      << "--src/-s\tsource db connection string e.g. sqlite_file:/home/pixeldaq/calib/dbcalib.sql" << std::endl
	      << "--dest/-d\tdestination db connection string e.g. oracle://ATONR_COOL/ATLAS_COOLONL_PIXEL" << std::endl
	      << "-a\tanalysis serial number (can be used multiple times)." << std::endl
	      << "-v\tverbose." << std::endl
	      << std::endl
	      << " or " << std::endl
	      << std::endl
	      << argv[0] << "--list/-l --src/-s src-db-connection [--conn-name/-c module, ROD, PP0 --conn-name...] [--var-name/-n] " << std::endl;
    return -1;
  }

  try {

    seal::Application app;
    seal::Context *context = app.context();

    //    CAN::PixCoralDbClient meta_data_client(false);
    PixCoralClient src_result_db(src_db_conn_name, context, verbose);
    if (list_only) {
      std::set<CAN::SerialNumber_t> matching_analysis_serial_number;
      src_result_db.listAnalyses(conn_name_list, var_name, matching_analysis_serial_number);
      for(std::set<CAN::SerialNumber_t>::const_iterator analysis_serial_number_iter = matching_analysis_serial_number.begin();
	  analysis_serial_number_iter != matching_analysis_serial_number.end();
	  ++analysis_serial_number_iter) {
	std::cout << *analysis_serial_number_iter << std::endl;
      }
    }
    else {
    PixCoralClient dest_result_db(dest_db_conn_name, context, verbose);

    for(std::vector<CAN::SerialNumber_t>::const_iterator analysis_serial_number_iter = analysis_serial_number.begin();
	analysis_serial_number_iter != analysis_serial_number.end();
	++analysis_serial_number_iter) {

      std::vector<std::string> conn_name_list;
      try {
      CAN::InfoPtr_t<CAN::AnalysisInfo_t> analysis_info;
      {
	analysis_info =  CAN::InfoPtr_t<CAN::AnalysisInfo_t>( CAN::MetaDataService::instance()->getAnalysisInfo(*analysis_serial_number_iter));
      }
      if (!analysis_info) {
	std::cerr << "ERROR ["<<argv[0]<<"] nothing known about " << CAN::makeAnalysisSerialNumberString(*analysis_serial_number_iter) << "." << std::endl;
	continue;
      }

      const CAN::InfoPtr_t<PixLib::ScanInfo_t> scan_info( CAN::MetaDataService::instance()->getScanInfo(analysis_info->scanSerialNumber() ) );

      if (!scan_info) {
	std::cerr << "ERROR ["<<argv[0]<<"] nothing known about the scan " << CAN::makeScanSerialNumberString( analysis_info->scanSerialNumber() )
		  << " which was analysed by " << CAN::makeAnalysisSerialNumberString(*analysis_serial_number_iter) << "." << std::endl;
	continue;
      }

      PixA::ConnectivityRef conn;
      {
	conn =PixA::ConnectivityManager::getConnectivity( scan_info->idTag(),
							  scan_info->connTag(),
							  scan_info->aliasTag(),
							  scan_info->dataTag(),
							  scan_info->cfgTag(),
							  scan_info->modCfgTag(),
							  scan_info->cfgRev());

      }
      if (!conn) {
	std::cerr << "ERROR ["<<argv[0]<<"] Failed to get connectivity for scan " << CAN::makeScanSerialNumberString( analysis_info->scanSerialNumber() ) << "." << std::endl;
	continue;
      }

      PixA::DisabledListRoot disabled_list_root;
      if (conn) {
	std::stringstream disableName;
	disableName << "Disable-S" << std::setw(9) << std::setfill('0') << analysis_info->scanSerialNumber();
	try { 
	  disabled_list_root = PixA::DisabledListRoot( conn.getDisable(disableName.str()) );
	}
	catch(...) {
	  // may not exist
	}
      }

      //  const PixA::DisabledList disabled_list( PixA::disableList( disabled_list_root) );
      bool has_enabled_crates=false;

      for (PixA::CrateList::const_iterator crate_iter = conn.crates().begin();
	   crate_iter != conn.crates().end();
	   ++crate_iter) {

	bool crate_enabled = PixA::enabled( disabled_list_root, crate_iter );

	if (!crate_enabled) continue;

	const PixA::DisabledList rod_disabled_list( PixA::rodDisabledList( disabled_list_root, crate_iter) );

	PixA::RodList::const_iterator rod_begin = PixA::rodBegin(crate_iter);
	PixA::RodList::const_iterator rod_end = PixA::rodEnd(crate_iter);
	bool has_enabled_rods=false;
	for (PixA::RodList::const_iterator rod_iter = rod_begin;
	     rod_iter != rod_end;
	     ++rod_iter) {

	  bool rod_enabled = PixA::enabled( rod_disabled_list, rod_iter );
	  if (!rod_enabled) continue;

	  const PixA::DisabledListRoot pp0_disabled_list( PixA::createPp0DisabledList( conn, disabled_list_root, PixA::connectivityName(rod_iter)) );

	  PixA::Pp0List::const_iterator pp0_begin = PixA::pp0Begin(rod_iter);
	  PixA::Pp0List::const_iterator pp0_end = PixA::pp0End(rod_iter);
	  bool has_enabled_pp0s=false;
	  for (PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
	       pp0_iter != pp0_end;
	       ++pp0_iter) {

	    bool pp0_enabled = PixA::enabled( pp0_disabled_list, pp0_iter );
	    if (!pp0_enabled) continue;

	    const PixA::DisabledList module_disabled_list( PixA::moduleDisabledList( pp0_disabled_list, pp0_iter) );


	    PixA::ModuleList::const_iterator module_begin = PixA::modulesBegin(pp0_iter);
	    PixA::ModuleList::const_iterator module_end = PixA::modulesEnd(pp0_iter);
	    bool has_enabled_modules=false;
	    for (PixA::ModuleList::const_iterator module_iter = module_begin;
		 module_iter != module_end;
		 ++module_iter) {
	      bool module_enabled = PixA::enabled( module_disabled_list, module_iter );
	      if (!module_enabled) continue;

	      conn_name_list.push_back(PixA::connectivityName(module_iter));
	      has_enabled_modules=true;
	    }
	    if (has_enabled_modules) {
	      has_enabled_pp0s=true;
	      conn_name_list.push_back(PixA::connectivityName(pp0_iter));
	    }
	  }
	  if (has_enabled_pp0s) {
	    has_enabled_rods=true;
	    conn_name_list.push_back(PixA::connectivityName(rod_iter));
	  }
	}
	if (has_enabled_rods) {
	  has_enabled_crates=true;
	}
      }

      CAN::ExtendedAnalysisResultList_t dest_analysis_results;
      try {
	dest_analysis_results = dest_result_db.getAnalysisResultsFromDB(*analysis_serial_number_iter, conn_name_list);
      }
      catch ( std::exception &err) {
	if (verbose) {
	  std::cout << "INFO [" << argv[0] << ":main] An exception was thrown when searching for analysis "  
		    << CAN::makeAnalysisSerialNumberString(*analysis_serial_number_iter) 
		    << " in the destination data base " << dest_db_conn_name << " : " << err.what()  
		    << ". So presumably this analysis does not exist yet in that DB."
		    << std::endl;
	}
      }

      if (isEmpty(dest_analysis_results)) {
	try {
	  CAN::ExtendedAnalysisResultList_t src_analysis_results = src_result_db.getAnalysisResultsFromDB(*analysis_serial_number_iter, conn_name_list);
	  if (!isEmpty(src_analysis_results)) {
	    if (verbose || simulation) {
	      std::cout << "INFO [" << argv[0] << ":main] " << (simulation ? "Would copy" : "Copy") << " analysis results of analysis "
			<< CAN::makeAnalysisSerialNumberString(*analysis_serial_number_iter) 
			<< (simulation ? "(simulation)" : "") << std::endl;
	    }
	    if (!simulation) {
	      dest_result_db.fillTables(*analysis_serial_number_iter, &src_analysis_results);
	    }
	  }
	  else  /*if (verbose)*/ {

	    std::cout << "INFO [" << argv[0] << ":main] Result list for analysis "  
		      << CAN::makeAnalysisSerialNumberString(*analysis_serial_number_iter) 
		      << " is empty or does not exist in the source data base " << src_db_conn_name << "."
		      << std::endl;
	  }
	}
	catch(std::exception &err) {
	  std::cout << "ERROR [" << argv[0] << ":main] An exception was thrown when searching for analysis "  
		    << CAN::makeAnalysisSerialNumberString(*analysis_serial_number_iter) 
		    << " in the source data base " << src_db_conn_name << " : " << err.what()  
		    << "."
		    << std::endl;
	}
      }

      }
      catch (PixA::ConfigException &err) {
	std::cout << "ERROR [" << argv[0] << ":main] Caught exception while processing analysis " 
		  << CAN::makeAnalysisSerialNumberString( *analysis_serial_number_iter ) 
		  << " : "  << err.getDescriptor() << std::endl;
      }
      catch (std::exception &err) {
	std::cout << "ERROR [" << argv[0] << ":main] Caught exception while processing analysis " 
		  << CAN::makeAnalysisSerialNumberString( *analysis_serial_number_iter ) 
		  << " : "  << err.what() << std::endl;
      }
    }

    return 0;
    }
  }
  catch(std::exception &err) {
    std::cout << "FATAL [" << argv[0] << ":main] caught exception : "  << err.what() << std::endl;
  }
  catch(...) {
    std::cout << "FATAL [" << argv[0] << ":main] caught unknown exception." << std::endl;
  }
  return -1;
}
