#if [ $1 -eq ""]; then
#    echo "USAGE: setup.sh dir";
#fi 
mydir=${1:-DAQ/Calib-test}
mydir=${mydir/$HOME\//}
olddir=`pwd`
cd
mkdir -p $mydir
cd $mydir
export WorkDir=`pwd`
#daq_setup $mydir
source /daq/slc4/zzzz_daq.sh $mydir
export CAN_CALIB_META=${WorkDir}/Applications/Pixel/PixCalibDbCoral
export CAN_CALIB_RESULTS=${WorkDir}/Applications/Pixel/PixResultsDbCoral
export PIX_ANA=${WorkDir}/Applications/Pixel/PixAnalysis
#export SN_SVC=${WorkDir}/Applications/Pixel/SerialNumberService
export CAN=${WorkDir}/Applications/Pixel/CalibrationAnalysis
export SEAL_PLUGINS=${CAN}/plugins/test:${CAN}/autoAnalyses:${CAN}/coreAnalyses:$SEAL_PLUGINS
export LD_LIBRARY_PATH=${CAN}:${CAN_CALIB_META}:${CAN_CALIB_RESULTS}:${PIX_ANA}/ConfigWrapper:${PIX_ANA}/DataContainer:${CAN}/plugins/test:${CAN}/autoAnalysis:${CAN}/coreAnalysis:$LD_LIBRARY_PATH
export CAN_CFG_PATH=/tmp/$USER/conf
export PIXSCAN_CFG_PATH=/tmp/${USER}/conf
mkdir -p $CAN_CFG_PATH
export PIX_METADATA_DB="sqlite_file:/tmp/${USER}/calib/metadb.sql"
#export PIX_METADATA_DB=/tmp/${USER}/
mkdir -p /tmp/${USER}/calib
export CAN_CALIB_DB="sqlite_file:/tmp/${USER}/calib/dbcalib.sql"

cd $olddir
unset olddir mydir


# Make sure the AFS token is valid
reklog () 
{ 
    tokens | /bin/grep AFS;
    if [ $? -eq 1 ]; then
        echo Renewing AFS ticket ...;
	aklog
	tokens | /bin/grep AFS;
	if [ $? -eq 1 ]; then
	    klog;
	fi
    else
        echo Already authenticated with AFS.;
    fi
}


# Compiles everything.
# Usage: CompileCAN [tag]
#   where tag is head by default, or a specific tag for CalibrationAnalysis
CompileCAN () {
    if [$2 -eq ""]; then
	echo USAGE: CompileCAN CVSUSER tag ;
    else
	tag=$2
	CVSUSER=$1
	export CVSROOT="${CVSUSER}@isscvs.cern.ch:/local/reps/atlaspixeldaq"
	[ "$tag"x = "x" ] || tag="-r $2"
	reklog
	cd ${WorkDir}
	cvs -d ${CVSROOT} co -r Pixel-DAQ RodDaq VmeInterface
	cvs -d ${CVSROOT} co -r PixCAL-0-3 Applications/Pixel/CoralDB Applications/Pixel/PixLib
	cvs -d ${CVSROOT} co Applications/Pixel/PixAnalysis
	cvs -d ${CVSROOT} co Applications/Pixel/PixCalibDbCoral
	cvs -d ${CVSROOT} co -P $tag Applications/Pixel/CalibrationAnalysis

	echo "======= RodDaq/RodCrate ======"
	cd ${WorkDir}/RodDaq/RodCrate
	make
	make inst

	echo "======= VmeInterface ======"
	cd ${WorkDir}/VmeInterface
	make
	make inst

	echo "======= Applications/Pixel/CoralDB ======"
	cd ${WorkDir}/Applications/Pixel/CoralDB
	make

	echo "======= Applications/Pixel/PixLib ======"
	cd ${WorkDir}/Applications/Pixel/PixLib
	make
	make inst
	cd ${WorkDir}/Applications/Pixel/PixLib/Examples
	make HistoServer

	echo "======= $PIX_ANA ======"
	cd $PIX_ANA
	make

	#    echo "======= $SN_SVC ======"
	#    cd $SN_SVC
	#    make

	echo "======= $CAN ======"
	cd ${WorkDir}/Applications/Pixel/PixCalibDbCoral
	make
	cd ${WorkDir}/Applications/Pixel/PixResultsDbCoral
	make
	cd $CAN
	make
    fi
}

