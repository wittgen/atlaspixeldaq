#ifndef _CAN_ExtendedAnalysisResultList_t_hh_
#define _CAN_ExtendedAnalysisResultList_t_hh_

#include <PixelCoralClientUtils/AnalysisResultList_t.hh>
#include <PixelCoralClientUtils/PixResultsException.hh>
#include <map>
#include <vector>
#include <sstream>
#include <PixelCoralClientUtils/PixelMap.h>
#include <TH1.h>

#include <memory>
#include <iostream>

namespace PixLib {

  class PixModuleGroup;
  class PixModule;

  // In principle not the whole PixModule / PixModuleGroup should be exposed since there are many methods which
  // will crash without hardware.
  typedef PixModuleGroup PixModuleGroupData_t;
  typedef PixModule      PixModuleData_t;
}

namespace CAN {

  //  class ExtendedAnalysisResultList_t : public AnalysisResultList_t;

  class AnalysisHistoNotFound : public CAN::PixResultsException
  {
    friend class ExtendedAnalysisResultList_t;
  protected:
    AnalysisHistoNotFound(const std::string &function_name,
					const std::string &message)
      : CAN::PixResultsException("ANAHISTO_NOTEXISTING "+function_name+": "+message)
    {}
  };

  class ExtendedAnalysisResultList_t : public AnalysisResultList_t{
  public:
    ~ExtendedAnalysisResultList_t();
  protected:
    template <class T>
    std::map<std::string, std::map<std::string, std::shared_ptr<T> > > &getPtrMap();

    template <class T>
    const std::map<std::string, std::map<std::string, std::shared_ptr<T> > > &getPtrMap() const
    {return const_cast<ExtendedAnalysisResultList_t*>(this)->getPtrMap<T>();}

    void throwHistoDoesNotExist(const std::string &histo_name, const std::string conn_name) const {
      std::stringstream message;
      message << "No histogram of name " << histo_name << " for connectivity object " << conn_name << ".";
      throw AnalysisHistoNotFound("ExtendedAnalysisResultList_t::histo<T>",message.str());
    }

  public:
    template <class T>
    void addHisto(const std::string &histo_name, const std::string &conn_name, const std::shared_ptr<T> &histo) {
      getPtrMap<T>()[histo_name].insert(std::make_pair<std::string, std::shared_ptr<T> >((std::string)conn_name, (std::shared_ptr<T>)histo));
    }

    template <class T>
    std::shared_ptr<T> histo(const std::string &histo_name, const std::string &conn_name) const {
      typename std::map< std::string, std::map<std::string, T> >::const_iterator iter=getPtrMap<T>().find(histo_name);
      if (iter == getPtrMap<T>().end()) {
	throwHistoDoesNotExist(histo_name,conn_name);
      }
      typename std::map<std::string, T>::const_iterator val_iter=iter->second.find(conn_name);
      if (val_iter == iter->second.end()) {
	throwHistoDoesNotExist(histo_name,conn_name);
      }
      return val_iter->second;
    }

    template <class T>
    bool hasHisto(const std::string &histo_name, const std::string &conn_name) const {
      typename std::map< std::string, std::map<std::string, T> >::const_iterator iter=getPtrMap<T>().find(histo_name);
      if (iter != getPtrMap<T>().end()) {
	typename std::map<std::string, T>::const_iterator histo_iter=iter->second.find(conn_name);
	if (histo_iter != iter->second.end()) {
	  return true;
	}
      }
      return false;
    }

    template <class T>
    typename std::map< std::string, std::map<std::string, std::shared_ptr<T> > >::iterator beginHisto()
      { return getPtrMap<T>().begin(); }

    template <class T>
    typename std::map< std::string, std::map<std::string, std::shared_ptr<T> > >::const_iterator beginHisto() const
      { return getPtrMap<T>().begin(); }

    template <class T>
    typename std::map< std::string, std::map<std::string, std::shared_ptr<T> > >::const_iterator endHisto() const
      { return getPtrMap<T>().end(); }

    //     template <class T>
    //     int size() const
    //       { return getPtrMap<T>().size(); }

    template <class T>
    bool hasHistograms() const
    {
      //      std::cout<<"Checking for a null map pointer "<<&getPtrMap<T>()<<" , "<< getPtrMap<T>().empty()<<std::endl;
      return true;
    }

    void showContents() const;

    /** Add a modified pix module group configuration.
     * The @ref ConfigWriteProcessor would write configurations of the extended result list.
     */
    void addNewPixModuleGroupConfig(std::shared_ptr<PixLib::PixModuleGroupData_t > &module_group) {
      m_moduleGroupConfig = module_group;
    }

    /** Add a modified pix module configuration.
     * The @ref ConfigWriteProcessor would write module configurations of the extended result list.
     */
    void addNewPixModuleConfig(std::shared_ptr<PixLib::PixModuleData_t > &module) {
      if (module.get()) {
	m_moduleConfig.push_back(module);
      }
    }

    /** Return true if ther are pix module group or pix module configurations.
     */
    bool hasModifiedConfigurations() const {
      return m_moduleConfig.size() || m_moduleGroupConfig.get();
    }

    /** Return a modified pix module group configuration or NULL.
     */
    std::shared_ptr<PixLib::PixModuleGroupData_t > modifiedPixModuleGroupConfig()  { return m_moduleGroupConfig; }

    /** Return a begin iterator over all the added modified module configuraitons.
     */
    std::vector< std::shared_ptr<PixLib::PixModuleData_t >  >::iterator        beginModifiedModuleConfig()       { return m_moduleConfig.begin(); }

    /** Return an end iterator over all the added modified module configuraitons.
     */
    std::vector< std::shared_ptr<PixLib::PixModuleData_t >  >::iterator        endModifiedModuleConfig()         { return m_moduleConfig.end(); }

    /** Return a begin iterator over all the added modified module configuraitons (read only).
     */
    std::vector< std::shared_ptr<PixLib::PixModuleData_t >  >::const_iterator  beginModifiedModuleConfig()   const { return m_moduleConfig.end(); }

    /** Return an endn iterator over all the added modified module configuraitons (read only).
     */
    std::vector< std::shared_ptr<PixLib::PixModuleData_t >  >::const_iterator  endModifiedModuleConfig()     const { return m_moduleConfig.end(); }

  private:

    std::map<std::string, std::map<std::string, std::shared_ptr<TH1> > >            m_mapTH1;

    std::vector< std::shared_ptr<PixLib::PixModuleData_t >  >                       m_moduleConfig;
    std::shared_ptr<PixLib::PixModuleGroupData_t >                                  m_moduleGroupConfig;
  };

  template <>
  inline std::map<std::string, std::map<std::string, std::shared_ptr<TH1> > > &ExtendedAnalysisResultList_t::getPtrMap< TH1 >()
    { return ExtendedAnalysisResultList_t::m_mapTH1; }


  // example:
  //
  //    inline void test() {
  //     AnalysisResultList_t a;
  //     a.addValue<unsigned int>("test","ROD_C1_S11",1);
  //     unsigned int a_value = a.value<unsigned int>("test","ROD_C1_S11");
  //
  //     for (std::map<std::string, std::map<std::string, unsigned int > >::const_iterator iter=a.begin<unsigned int>();
  // 	 iter != a.end<unsigned int>();
  // 	 iter ++) {
  //
  //       for (std::map<std::string, unsigned int >::const_iterator val_iter=iter->second.begin();
  // 	   val_iter != iter->second.end();
  // 	   val_iter ++) {
  // 	      unsigned int b=val_iter->second;
  //       }
  //     }
  //   }

}

#endif
