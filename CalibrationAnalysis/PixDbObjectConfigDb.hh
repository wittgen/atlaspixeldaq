#ifndef _CAN_PixDbObjectConfigDb_hh_
#define _CAN_PixDbObjectConfigDb_hh_

#include "IObjectConfigDb.hh"
#include "IConfigDb.hh"

namespace CAN {

  /** Interface for a configuration database.
   */
  class PixDbObjectConfigDb : public IObjectConfigDb
  {
  public:
    PixDbObjectConfigDb();

    void readConfig(const ObjectInfo_t &object_info, PixLib::Config &config,std::string string="");
  private:

    std::unique_ptr<IConfigDb> m_configDb;
  };

}

#endif
