#ifndef _CAN_ConfigCatalogListener_hh_
#define _CAN_ConfigCatalogListener_hh_

#include <string>
// same definitions are done by seal and omni

#ifdef HAVE_NAMESPACES
#  undef HAVE_NAMESPACES
#endif
#ifdef HAVE_BOOL
#  undef HAVE_BOOL
#endif
#ifdef HAVE_DYNAMIC_CAST
#  undef HAVE_DYNAMIC_CAST
#endif

#include <Common.hh>

// same definitions are done by seal and omni

#ifdef HAVE_NAMESPACES
#  undef HAVE_NAMESPACES
#endif
#ifdef HAVE_BOOL
#  undef HAVE_BOOL
#endif
#ifdef HAVE_DYNAMIC_CAST
#  undef HAVE_DYNAMIC_CAST
#endif


namespace CAN {

  class ConfigDescription;

  /** Helper class to pass configuration descriptions to an application.
   */
  class IDescriptionListener {
  public:
    virtual ~IDescriptionListener() {}
    virtual void newDescription(const std::string &type_name, const ConfigDescription &description) = 0;
  };

  class ICommentListener {
  public:
    virtual ~ICommentListener() {}

    /** @param type_name the name of type for which the comment is meant.
     * @param tag_name the name of a tag for which the comment is meant or an empty string if the comment is not specific for the tag.
     * @param revision the specific revision for which the comment is meant or zero if the comment is not revision specific.
     * @param the date of the comment.
     * @param comment the comment.
     */
    virtual void newComment(const std::string &type_name, const std::string &tag_name, Revision_t revision, CAN::time_t date, const std::string &comment) = 0;
  };

}
#endif
