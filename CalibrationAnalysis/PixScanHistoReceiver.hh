#ifndef _CAN_PixScanHistoReceiver_h_
#define _CAN_PixScanHistoReceiver_h_

#include <oh/OHRootReceiver.h>
#include <vector>
#include <string>

namespace CAN {

  class TopLevelHistoArray;

  class PixScanHistoReceiver : public OHRootReceiver {
  public:
    PixScanHistoReceiver( TopLevelHistoArray &histo_array,  std::string pix_scan_histo_name)
      : m_histoArray(&histo_array),
	m_pixScanHistogramName(pix_scan_histo_name)
    { }

    void receive( OHRootHistogram & h );

    void receive( std::vector<OHRootHistogram*> & h_array);

    /** Does nothing intentionally. */
    void receive( OHRootGraph & ) {}

    /** Does nothing intentionally. */
    void receive( OHRootGraph2D & ) {}
  private:
    TopLevelHistoArray *m_histoArray;
    std::string m_pixScanHistogramName;

  };
}
#endif
