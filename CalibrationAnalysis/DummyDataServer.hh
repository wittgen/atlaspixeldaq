#ifndef _CAN_DummyDataServer_hh_
#define _CAN_DummyDataServer_hh_

#include "IMasterDataServer.hh"
#include "IDataServer.hh"
#include "AnalysisDataServer.hh"
#include "IExecutionUnit.hh"
#include "exception.hh"
//#include <mrs/message.h>

#include <ConfigWrapper/Connectivity.h>

#include "MetaDataService.hh"
#include "ScanInfo_t.hh"

#include <map>
#include <deque>

#include "Lock.hh"
#include "Flag.hh"

#include <ConfigWrapper/PixDisableUtil.h>

namespace PixLib{
  class PixMessages;
}

namespace CAN {

  class DummyMasterDataServer;

  /** Server of histograms, configurations and the connecitivity.
   * The server will automatically provide the connectivity and the scan configuration.
   * Histograms and module or boc configurations etc. need to be requested before they
   * can be retrieved from the server.
   */
  class DummyDataServer : public IDataServer
  {

  public:
    friend class DummyMasterDataServer;

    DummyDataServer(const std::string &rod_name, DummyMasterDataServer &master)
      : m_rodName(rod_name),
	m_master(&master),
	m_gotAllRequests(false)
    { }

    ~DummyDataServer();

    void requestHistogram(const std::string &,
    			  const std::string &) {}

    void requestUIntAnalysisResult(const std::string &tag_name,
    				   Revision_t &revision,
    				   const std::string &conn_name,
    				   const std::string &variable_name) {}

    void requestBoolAnalysisResult(const std::string &tag_name,
    				   Revision_t &revision,
    				   const std::string &conn_name,
    				   const std::string &variable_name) {}

    void requestAvAnalysisResult(const std::string &tag_name,
					   Revision_t &revision,
					   const std::string &conn_name,
					   const std::string &variable_name) {}


    void requestScanConfig() {}

    void requestBocConfig() {}

    void requestModuleConfig(const std::string &) {}

    void requestModuleGroupConfig() {}


    //     void requestModuleConfigObject(const std::string &,
    //     				   const std::string &,
    //     				   const std::string &) {}

    //     void requestModuleConfigObject(const std::string &,
    //     				   const std::vector<std::string> &,
    //     				   const std::string &) {}

    void thatIsAll();


    void numberOfHistograms(const std::string&, const std::string&, HistoInfo_t &) const 
    //    { throw MRSException("DATASRV_NOIMPL",MRS_FATAL,"CAN::DummyDataServer::numberOfHistograms","Not implemented.");}
      { throw MRSException(PixLib::PixMessages::FATAL,"CAN::DummyDataServer::numberOfHistograms","Not implemented.");}

    const Histo* getHistogram(const std::string&, const std::string&, const PixA::Index_t&) const 
    //    { throw MRSException("DATASRV_NOIMPL",MRS_FATAL,"CAN::DummyDataServer::getHistogram","Not implemented.");}
      { throw MRSException(PixLib::PixMessages::FATAL,"CAN::DummyDataServer::getHistogram","Not implemented.");}


    PixLib::Histo *getHistogram(const std::string &histogram_type_name,
			const std::string &conn_name) const
    //      { throw MRSException("DATASRV_NOIMPL",MRS_FATAL,"CAN::DummyDataServer::getHistogram","Not implemented.");}
      { throw MRSException(PixLib::PixMessages::FATAL,"CAN::DummyDataServer::getHistogram","Not implemented.");}

    unsigned int getUIntAnalysisResult(const std::string &conn_name,
				       const std::string &variable_name) const
    //      { throw MRSException("DATASRV_NOIMPL",MRS_FATAL,"CAN::DummyDataServer::getUIntAnalysisResult","Not implemented.");}
      { throw MRSException(PixLib::PixMessages::FATAL,"CAN::DummyDataServer::getUIntAnalysisResult","Not implemented.");}

    unsigned int getBoolAnalysisResult(const std::string &conn_name,
				       const std::string &variable_name) const
    //      { throw MRSException("DATASRV_NOIMPL",MRS_FATAL,"CAN::DummyDataServer::getBoolAnalysisResult","Not implemented.");}
      { throw MRSException(PixLib::PixMessages::FATAL,"CAN::DummyDataServer::getBoolAnalysisResult","Not implemented.");}

    const AverageResult_t &getAvAnalysisResult(const std::string &conn_name,
					       const std::string &variable_name) const
    //      { throw MRSException("DATASRV_NOIMPL",MRS_FATAL,"CAN::DummyDataServer::getAvAnalysisResult","Not implemented.");}
      { throw MRSException(PixLib::PixMessages::FATAL,"CAN::DummyDataServer::getAvAnalysisResult","Not implemented.");}


    const PixLib::Config &getScanConfig() const
    //      { throw MRSException("DATASRV_NOIMPL",MRS_FATAL,"CAN::DummyDataServer::getScanConfig","Not implemented.");}
      { throw MRSException(PixLib::PixMessages::FATAL,"CAN::DummyDataServer::getScanConfig","Not implemented.");}

    const PixLib::Config &getBocConfig() const
    //      { throw MRSException("DATASRV_NOIMPL",MRS_FATAL,"CAN::DummyDataServer::getBocConfig","Not implemented.");}
      { throw MRSException(PixLib::PixMessages::FATAL,"CAN::DummyDataServer::getBocConfig","Not implemented.");}

    const PixLib::Config &getModuleConfig(const std::string &module_conn_name) const
    //      { throw MRSException("DATASRV_NOIMPL",MRS_FATAL,"CAN::DummyDataServer::getModuleConfig","Not implemented.");}
      { throw MRSException(PixLib::PixMessages::FATAL,"CAN::DummyDataServer::getModuleConfig","Not implemented.");}


    virtual std::shared_ptr<PixLib::PixModuleData_t> createConfiguredModule(const std::string &) const
    //      { throw MRSException("DATASRV_NOIMPL",MRS_FATAL,"CAN::DummyDataServer::createConfiguredModule","Not implemented.");}
      { throw MRSException(PixLib::PixMessages::FATAL,"CAN::DummyDataServer::createConfiguredModule","Not implemented.");}

    virtual std::shared_ptr<PixLib::PixModuleGroupData_t> createConfiguredModuleGroup() const
    //      { throw MRSException("DATASRV_NOIMPL",MRS_FATAL,"CAN::DummyDataServer::createConfiguredModuleGroup","Not implemented.");}
     {  throw MRSException(PixLib::PixMessages::FATAL,"CAN::DummyDataServer::createConfiguredModuleGroup","Not implemented.");}

//     const PixLib::ConfigObj &getModuleConfigObject(const std::string &module_conn_name,
// 						     const std::vector<std::string> &sub_config_and_group_name,
// 						     const std::string &config_object_name) const
////       { throw MRSException("DATASRV_NOIMPL",MRS_FATAL,"CAN::DummyDataServer::getModuleConfigObject","Not implemented.");}

//     const PixLib::ConfigObj &getModuleConfigObject(const std::string &module_conn_name,
// 						     const std::string &config_group_name,
// 						     const std::string &config_object_name) const
////       { throw MRSException("DATASRV_NOIMPL",MRS_FATAL,"CAN::DummyDataServer::getModuleConfigObject","Not implemented.");}


    bool needModuleConfig() const 
    //    { throw MRSException("DATASRV_NOIMPL",MRS_FATAL,"CAN::DummyDataServer::needModuleConfig","Not implemented.");}
      { throw MRSException(PixLib::PixMessages::FATAL,"CAN::DummyDataServer::needModuleConfig","Not implemented.");}

    const PixA::DisabledList &getPp0DisabledList() const { return m_pp0DisabledList; }

    const std::string &rodConnName() const {return m_rodName;}

    bool gotAllRequests() const {return m_gotAllRequests; }

  protected:
    void setDisabledList(const PixA::DisabledListRoot &pp0_disabled_list_root) { m_pp0DisabledList = pp0_disabled_list_root; }

  private:
    std::string           m_rodName;
    DummyMasterDataServer *m_master;
    bool                  m_gotAllRequests;
    PixA::DisabledListRoot m_pp0DisabledList;
  };


  class DummyMasterDataServer : public IMasterDataServer, public omni_thread
  {
  public:
    typedef std::deque< std::pair< PixA::RodLocationBase, std::shared_ptr<IAnalysisDataServer> > > DataServerList_t;//CLA

    DummyMasterDataServer();//IPCPartition &mrs_partition);

    std::shared_ptr<IAnalysisDataServer> createAnalysisDataServer(SerialNumber_t /*analysis_serial_number*/, //CLA
								    const PixA::RodLocationBase &rod_conn_name,
								    SerialNumbers_t scan_serial_numbers, //CLA
								    const InfoPtr_t<PixLib::ScanInfo_t> &/*scan_info*/,
								    const SerialNumber_t /*src_analysis_serial_number*/,
								    PixA::ConnectivityRef &conn,
								    const std::vector<PixA::DisabledListRoot> &disabled_lists);

    void addAnalysisDataServer(SerialNumber_t analysis_serial_number, //CLA
			       const PixA::RodLocationBase &rod_conn_name,
			       const std::shared_ptr<IAnalysisDataServer>  &data_server)
    {
      DataServerList_t &a_list = m_dataServerList[analysis_serial_number];
      a_list.push_back( std::make_pair(rod_conn_name, data_server));
      m_dataRequest.setFlag();
    }

    void start(IExecutionUnit &execution_unit);

    virtual void abort(SerialNumber_t) { }

    void reset() {}

    void initiateShutdown() { m_shutdown=true; m_dataRequest.setFlag(); }

    void shutdownWait() {
      if (state() == omni_thread::STATE_RUNNING) {
	m_exit.wait();
	while (state()!=omni_thread::STATE_TERMINATED) {
	  usleep(1000);
	}
      }
    }

    void setDataRequest() { m_dataRequest.setFlag(); }

    void setLogLevel(ELogLevel ) {}


  protected:
    void *run_undetached(void *);

  private:
    Mutex m_dataServerListMutex;
    Flag  m_dataRequest;
    Flag  m_exit;
    bool  m_shutdown;

    std::map<SerialNumber_t, DataServerList_t > m_dataServerList;

    std::map<SerialNumber_t, InfoPtr_t<PixLib::ScanInfo_t> > m_metaDataCache;
    IExecutionUnit *m_executionUnit;
    //    std::auto_ptr<MRSStream> m_out;
    
  };

}

#endif
