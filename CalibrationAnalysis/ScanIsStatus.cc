#include "ScanIsStatus.hh"
#include <sstream>
#include <iomanip>

namespace CAN {

  const char *ScanIsStatus::s_allStatusName[9]={
    "UNKNOWN",
    "NONE",
    "INITIALIZING",
    "RUNNING",
    "PROCESSING",
    "DONE",
    "SUCCESS",
    "FAILED",
    "ABORTED"
  };

  const char *ScanIsStatus::s_statusName[kAborted+1]={
    "UNKNOWN",
    "NONE",
    "INITIALIZING",
    "RUNNING",
    "DONE",
    "FAILED",
    "ABORTED"
  };

  const ScanIsStatus::EStatus ScanIsStatus::s_statusMap[9]={
    ScanIsStatus::kUnknown,
    ScanIsStatus::kNone,
    ScanIsStatus::kInitialising,
    ScanIsStatus::kRunning,
    ScanIsStatus::kRunning,
    ScanIsStatus::kSuccess,
    ScanIsStatus::kSuccess,
    ScanIsStatus::kFailed,
    ScanIsStatus::kAborted
  };

  const std::string ScanIsStatus::s_emptyString;

  const std::string ScanIsStatus::s_statusVarName("STATUS");


  std::string ScanIsStatus::extractRodName(const std::string &full_is_var_name)
  {
    std::string::size_type rod_name_end = full_is_var_name.rfind("/");
    if (rod_name_end != std::string::npos && rod_name_end>0) {

      std::string::size_type rod_name_begin = full_is_var_name.rfind("/",rod_name_end-1);
      if (rod_name_begin != std::string::npos) {
	return std::string(full_is_var_name, rod_name_begin+1, rod_name_end-rod_name_begin-1);
      }
    }
    return s_emptyString;
  }

  std::string ScanIsStatus::makeRegex(SerialNumber_t serial_number)
  {
    // If the naming convention changes in IS the pattern for the scan and analysis status has to be changed here.
    std::stringstream pattern;
    pattern << ".*" /* << "/" */ << s_measurementTypeId << std::setw(9) << std::setfill('0') << serial_number
	    << "/.*/" << s_statusVarName;
    return pattern.str();
  }

  std::string ScanIsStatus::makeRegex()
  {
    // If the naming convention changes in IS the pattern for the scan and analysis status has to be changed here.
    std::string temp(".*/");
    temp+=s_measurementTypeId;
    temp+=".*/";
    temp+=s_statusVarName;
    return temp;
  }

  std::string ScanIsStatus::makeIsVarName(const std::string &is_server_name, const std::string &rod_connectivity_name, SerialNumber_t serial_number)
  {
    // If the naming convention changes in IS the pattern for the scan and analysis status has to be changed here.
    std::stringstream pattern;
    if (!is_server_name.empty()) {
      pattern << is_server_name << '.';
    }
    pattern << '/' << s_measurementTypeId << std::setw(9) << std::setfill('0') << serial_number
	    << rod_connectivity_name << s_statusVarName;
    return pattern.str();
  }

}
