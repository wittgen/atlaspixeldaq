#ifndef _CAN_Lock_hh_
#define _CAN_Lock_hh_

#include <omnithread.h>

namespace CAN {

  typedef omni_mutex Mutex;

  typedef omni_condition Condition;

  class Lock {
  public:
    Lock(Mutex &a_mutex) : m_mutex(&a_mutex) {m_mutex->lock();}

    ~Lock()  {m_mutex->unlock();}

  private:
    Mutex *m_mutex;
  };

}

#endif
