#include "daemonise.hh"

#include <sys/stat.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>

#include <iostream>

int daemonise()
{

  std::cout << "INFO [daemonise] Daemonise exec." << std::endl;
   if(getppid()==1) return 0; // already a daemon

   int status=fork();
   if (status<0) return -1;  // fork error
   if (status>0) return 1;             // parent exits 


   //   close all descriptors
   for (unsigned int i=getdtablesize();i-->0;) { 
     close(i);
   }

    status = setsid();

    if (status == -1)
    {
      return -1;
    }

    status = fork();

    switch (status)
    {
    case -1:
      return -1;
    case 0: /* (second) child process */
        break;
    default: /* parent process */
      return 1;
    }

   
  umask(027);                     // set newly created file permissions 
  chdir("/");                     // change running directory

  unsigned int fd=open("/dev/null",O_RDWR); /* open stdin */
  dup(fd); /* stdout */
  dup(fd); /* stderr */
  
  signal(SIGCHLD,SIG_IGN);        // ignore child
  signal(SIGTSTP,SIG_IGN);        // ignore tty signals
  signal(SIGTTOU,SIG_IGN);
  signal(SIGTTIN,SIG_IGN);
  //   signal(SIGHUP,signal_handler); // catch hangup signal
   //   signal(SIGTERM,signal_handler); // catch kill signal 
  return 0;
}
