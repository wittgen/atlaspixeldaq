#ifndef _CAN_WorkerInfo_t_hh_
#define _CAN_WorkerInfo_t_hh_

#include "Worker.hh"
#include <time.h>
#include <string>
#include <cassert>

namespace CAN {

  class WorkerInfo_t {
  private:
    /** inhibited */
    WorkerInfo_t(WorkerInfo_t &a) {}
  public:
    WorkerInfo_t(Worker_ptr worker, const char *name, unsigned int n_slots)
      : m_name(name),
	m_nSlots(n_slots),
	m_nFreeSlots(n_slots),
	m_running(0),
	m_executed(0),
	m_waiting(0),
	m_waitingForData(0),
	m_load(0),
	m_nMissingResponses(0),
	m_worker(worker),
	m_toBeRemoved(false)
    { resetHangingSince(); }

    ~WorkerInfo_t() { CORBA::release(m_worker); }

    const std::string &name()  const       { return m_name; }
    void setName(const std::string &name)  { m_name = name; }

    unsigned int nSlots()     const        { return m_nSlots; }
    unsigned int nFreeSlots() const        { return m_nFreeSlots; }

    void setQueueStatus(unsigned int jobs_waiting_for_data,
			unsigned int jobs_waiting,
			unsigned int jobs_running,
			unsigned int jobs_executed,
			unsigned int empty_slots)
    {
      if (    (jobs_executed!=m_executed)
           || (jobs_executed==m_executed && jobs_running != m_running)
	   || (m_running==0 && m_waiting==0)) {
	resetHangingSince();
      }
      m_nFreeSlots=empty_slots;
      m_running=jobs_running;
      m_executed=jobs_executed;
      m_waiting=jobs_waiting;
      m_waitingForData=jobs_waiting_for_data;
    }


    unsigned int nUnusedSlots() const
    { unsigned int temp_free = m_nFreeSlots; unsigned temp_waiting = m_waiting; return (temp_free > temp_waiting ? temp_free - temp_waiting : 0); }

    void submitJobs(unsigned int n_jobs) { m_waitingForData += n_jobs; }

    //    void setFreeSlots(unsigned int n_free_slots ) {m_nFreeSlots=n_free_slots;}

    Worker_ptr worker()             { return m_worker; }
    const Worker_ptr worker() const { return m_worker; }

    //    void incRunningJobs() { m_running++; assert(m_nFreeSlots>0); m_nFreeSlots--; }
    //    void incWaitingJobs() { m_waiting++; }

    //    void decRunningJobs() { assert(m_running>0); m_running--; m_nFreeSlots++;}
    //    void decWaitingJobs() { assert(m_waiting>0); m_waiting--; }

    //    void decSubmittedJobs() { assert(m_submitted>0); m_submitted--; }

    unsigned int runningJobs() const { return m_running; }
    unsigned int waitingJobs() const { return m_waiting; }

    void resetHangingSince() { m_hangingSince=time(NULL);}
    time_t hangingSince() const { return m_hangingSince; }

    unsigned int missingResponses() const { return m_nMissingResponses; }
    void incMissingResponse()             { m_nMissingResponses++;};
    void resetMissingResponse()           { m_nMissingResponses=0;};

    bool toBeRemoved() const { return m_toBeRemoved; }
    void markForRemoval()    { m_toBeRemoved = true; }

    void giveLoad( unsigned int load) {
      m_load += load;
    }

    void takeLoad( unsigned int load) {
      m_load -= load;
      assert( m_load>=0 ); 
    }

    unsigned int load() const { return m_load; }

  private:
    std::string  m_name;
    unsigned int m_nSlots;
    unsigned int m_nFreeSlots;
    unsigned int m_running;
    unsigned int m_executed;
    unsigned int m_waiting;
    unsigned int m_waitingForData;
    unsigned int m_load;
    time_t       m_hangingSince;
    unsigned int m_nMissingResponses;

    Worker_ptr  m_worker;
    bool m_toBeRemoved;
  };

}
#endif
