#ifndef _CAN_IResultStore_hh_
#define _CAN_IResultStore_hh_

#include "Common.hh"

namespace CAN {

  class ExtendedAnalysisResultList_t;

  /** Abstract interface of the class which archives the analysis results
   */
  class IResultStore
  {
  public:
    virtual ~IResultStore() {}

    /** Put the given object into the results store and archive it.
     * @param analysis_serial_number the serial number of the analysis to which the results belong.
     * @param results the analysis result list object to be transferred to the results store.
     * Note, the owner ship of the results is by the caller.
     */
    virtual void put(SerialNumber_t analysis_serial_number, const std::string& rod_name, ExtendedAnalysisResultList_t *results) = 0 ;

    /** Tell the result store to shutdown.
     */
    virtual void initiateShutdown() = 0;
  };

}

#endif
