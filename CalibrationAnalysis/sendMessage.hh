#ifndef _sendMessage_hh_
#define _sendMessage_hh_

#include <string>
//#include <mrs/message.h>
#include "Common.hh"

namespace CAN {

// #ifndef _CAN_exception_hh_
//   typedef severity MRSSeverity;
// #endif

//   void sendMessage(MRSStream &out,
// 		   const std::string &message_id,
// 		   MRSSeverity a_severity,
// 		   const std::string &function_name,
// 		   const std::string &message);

//   void sendMessage(MRSStream &out,
// 		   const std::string &message_id,
// 		   MRSSeverity a_severity,
// 		   const std::string &function_name,
// 		   const std::string &connectivity_name,
// 		   const std::string &message);

//   void sendMessage(MRSStream &out,
// 		   const std::string &message_id,
// 		   const std::string &module_id,
// 		   MRSSeverity a_severity,
// 		   const std::string &function_name,
// 		   const std::string &message);

//   void sendMessage(MRSStream &out,
// 		   const std::string &message_id,
// 		   const std::string &module_id,
// 		   MRSSeverity a_severity,
// 		   const std::string &function_name,
// 		   const std::string &connectivity_name,
// 		   const std::string &message);

  std::string makeSerialNumberString(char type, SerialNumber_t serial_number);

  inline std::string makeScanSerialNumberString(SerialNumber_t serial_number) {
    return makeSerialNumberString('S',serial_number);
  }

  inline std::string makeAnalysisSerialNumberString(SerialNumber_t serial_number) {
    return makeSerialNumberString('A',serial_number);
  }



}
#endif
