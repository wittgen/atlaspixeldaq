#ifndef _CAN_IWorker_hh_
#define _CAN_IWorker_hh_

namespace CAN {
  class IWorker {
  public:
    virtual ~IWorker() {}
    virtual void triggerRestart() = 0;
  };

}

#endif
