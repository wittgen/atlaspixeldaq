#ifndef _IPostProcessing_hh_
#define _IPostProcessing_hh_

#include "IAnalysisObject.hh"
#include <ConfigWrapper/Locations.h>

namespace CAN {

  class ExtendedAnalysisResultList_t;
  class IAnalysisDataServer;

  /** Interface of objects which perform post-processing of the analysis results.
   * A post processing could for example be used to update module configurations, or
   * produce output for the offline conditions database.
   */
  class IPostProcessing : public IAnalysisObject
  {
  public:

    /** can be used to ask server to prefetch histograms or configuration in the background.
     * All histograms and configurations etc. which are used by the post-processor need to be requested
     * All requests have to be made in this method.
     */
    virtual void requestData(IAnalysisDataServer *data_server,
			     const PixA::RODLocationBase &rod_location) = 0;

    /** Post-process the results.
     * @param rod_location the name of the rod.
     * @param analysis_result the analysis results including the results of the classification step.
     * The post-processing is only performed if the classification, @ref IClassification, returned success.
     */
    virtual void process(IAnalysisDataServer *data_server,
			 const PixA::RODLocationBase &rod_location,
			 ExtendedAnalysisResultList_t &analysis_results) = 0;

  };

}

#endif
