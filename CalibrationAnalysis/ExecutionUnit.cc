#include "ExecutionUnit.hh"

#include "IMasterDataServer.hh"
#include "IAnalysisDataServer.hh"
#include "CoralResultArchive.hh"
#include "IArchivingUnit.hh"
#include "AnalysisFactory.hh"

#include "IsDictionary.hh"
#include "isUtil.hh"

#include "memstat.hh"

#include "IWorker.hh"

namespace CAN {


  std::string ExecutionUnit::s_componentName( "ANAEXEC" );

  ExecutionUnit::JobThread::JobThread(IsDictionary &is_dictionary,
				      ExecutionUnit &execution_unit,
				      const std::shared_ptr<IArchivingUnit> &archiver,
				      //IPCPartition &mrs_partition,
				      ELogLevel log_level)
    : m_joblett(NULL),
      m_analysisSerialNumber(0),
      m_isDictionary(&is_dictionary),
      m_executionUnit(&execution_unit),
      m_resultArchive(archiver),
      //      m_out(new MRSStream(mrs_partition)),
      m_logLevel(log_level),
      m_isUsed(false),
      m_shutdown(false)
  {
    if (m_logLevel>=kInfoLog) {
      std::stringstream message;
      message << " new thread " << static_cast<void *>(this) << ".";
//       sendMessage(*m_out, ExecutionUnit::componentName(), "JOBTHREAD_start",MRS_INFORMATION, "[JobThread::ctor]", message.str());
      ers::info(PixLib::pix::daq (ERS_HERE,  "[JobThread::ctor]", message.str()));
    }
    start_undetached();

  }

  ExecutionUnit::JobThread::~JobThread() {
    if (m_logLevel>=kInfoLog) {
      std::stringstream message;
      message << " destruct thread " << static_cast<void *>(this) << ".";
//       sendMessage(*m_out, ExecutionUnit::componentName(), "JOBTHREAD_term",MRS_INFORMATION, "[JobThread::ctor]", message.str());
      ers::info(PixLib::pix::daq (ERS_HERE,  "[JobThread::ctor]", message.str()));
    }
  }

  void *ExecutionUnit::JobThread::run_undetached(void *arg) {
    assert (arg==NULL);

    for (; !m_shutdown;) {

      m_newJob.wait(m_shutdown);
      if (m_shutdown) break;
      if (m_joblett) {
	// 	{
	// 	  std::stringstream message;
	// 	  message << " run analysis " << m_analysisSerialNumber << " for " << m_rodName << " on " << static_cast<void *>(this) << ".";
	// 	  sendMessage(*m_out, ExecutionUnit::componentName(), "JOBTHREAD_analyse",MRS_DIAGNOSTIC, "[JobThread::run_undetached]", message.str());
	// 	}
	analyse();
	assert(!m_joblett || m_joblett->isDone());
	// 	{
	// 	  std::stringstream message;
	// 	  message << "Done with analysis " << m_analysisSerialNumber << " for ROD " << m_rodName << ".";
	// 	  sendMessage(*m_out, ExecutionUnit::componentName(), "JOBTHREAD_analysis",MRS_DIAGNOSTIC, "ExecutionUnit::JobThread::run_undetached", message.str());
	// 	}
      }

      m_isUsed=false;
      m_executionUnit->joblettFinished();

    }

    m_exit.setFlag();
    if (m_logLevel>=kInfoLog) {
      std::stringstream message;
      message << " exit thread " << static_cast<void *>(this) << ".";
//       sendMessage(*m_out, ExecutionUnit::componentName(), "JOBTHREAD_exit",MRS_INFORMATION, "[JobThread::run_undetached]", message.str());
      ers::info(PixLib::pix::daq (ERS_HERE,  "[JobThread::run_undetached]", message.str()));
    }

    return NULL;
  }

  void ExecutionUnit::JobThread::analyse()
  {
    bool set_final_status=true;

    try {
      m_joblett->analysis()->setLogLevel(m_logLevel);

      if (m_logLevel>=kDiagnostics5Log) {
	std::stringstream message;
	message << "Start Analysis " << m_analysisSerialNumber << " for ROD " << m_rodName << " " << ".";
// 	sendMessage(*m_out, ExecutionUnit::componentName(), "JOBTHREAD_analysis",MRS_DIAGNOSTIC, "ExecutionUnit::JobThread::analyse", message.str());
	ers::debug(PixLib::pix::daq (ERS_HERE,  "ExecutionUnit::JobThread::analyse", message.str()));
      }
      // run analysis
      m_joblett->analysis()->analyse(m_joblett->dataServer().get(), m_rodName,m_joblett->results());
      if (m_logLevel>=kDiagnostics5Log) {
	std::stringstream message;
	message << "Finished Analysis " << m_analysisSerialNumber << " for ROD " << m_rodName << " " << ".";
// 	sendMessage(*m_out, ExecutionUnit::componentName(), "JOBTHREAD_analysis",MRS_DIAGNOSTIC, "ExecutionUnit::JobThread::analyse", message.str());
	ers::debug(PixLib::pix::daq (ERS_HERE, "ExecutionUnit::JobThread::analyse", message.str()));
      }

      if (m_joblett->isAbortRequested()) {
	m_joblett->setAborted();
      }
      else {

	// post-processing ?
	m_joblett->classification()->setLogLevel(m_logLevel);
	if (m_joblett->classification()->classify(m_joblett->dataServer().get(), m_rodName, m_joblett->results())) {

	  // @todo the post processing should be executed by the main thread.
	  //       it is considered to be mostly IO intensive.
	  m_joblett->postProcessing()->setLogLevel(m_logLevel);
	  m_joblett->postProcessing()->process(m_joblett->dataServer().get(),m_rodName,m_joblett->results());

	  if (m_joblett->isAbortRequested()) {

	    if (m_logLevel>=kFatalLog) {
	      std::stringstream message;
	      message << "Analysis " << m_analysisSerialNumber << " for ROD " << m_rodName << " " << " was aborted .";
// 	      sendMessage(*m_out, ExecutionUnit::componentName(), "JOBTHREAD_analysis",MRS_FATAL, "ExecutionUnit::JobThread::analyse", message.str());
	      ers::fatal(PixLib::pix::daq (ERS_HERE, "ExecutionUnit::JobThread::analyse", message.str()));
	    }

	    m_joblett->setAborted();
	  }
	  else {
	    m_joblett->setSuccess();
	    m_joblett->results().addValue<bool>("Classification",m_rodName,true);
	  }

	}
	else {
	  m_joblett->setFailure();
	  m_joblett->results().addValue<bool>("Classification",m_rodName,false);
	}

	if (m_logLevel>=kDiagnostics5Log) {
	  std::stringstream message;
	  message << "Analysis " << m_analysisSerialNumber << " for ROD " << m_rodName << " finished with " << AnalysisStatus::name(m_joblett->status()) 
		  << ". Will now archive results .";
// 	  sendMessage(*m_out, ExecutionUnit::componentName(), "JOBTHREAD_analysis",MRS_DIAGNOSTIC, "ExecutionUnit::JobThread::analyse", message.str());
	  ers::debug(PixLib::pix::daq (ERS_HERE, "ExecutionUnit::JobThread::analyse", message.str()));
	}

	// archive results ?
	if ( !m_joblett->ignoreResults() ) {

	  bool error=true;
	  try {

	    m_resultArchive->queueForArchiving( m_analysisSerialNumber,
						m_rodName,
						m_joblett->status(),
						m_joblett->isDictionaryName(),
						m_joblett->takeResults());
	    set_final_status=false;
	    error=false;
	  }
	  catch ( CAN::MRSException & err ) {
	  }
	  catch (...) {
	  }

	  if (error) {
	    m_joblett->setFailure();
	    if (m_logLevel>=kFatalLog) {
	      std::stringstream message;
	      message << "Caught exception when trying to queue results for archiving (A" << m_analysisSerialNumber << ", ROD " << m_rodName << ").";
// 	      sendMessage(*m_out, ExecutionUnit::componentName(), "JOBTHREAD_analysis",MRS_FATAL, "ExecutionUnit::JobThread::analyse", message.str());
	      ers::fatal(PixLib::pix::daq (ERS_HERE, "ExecutionUnit::JobThread::analyse", message.str()));
	    }
	  }

	}
	else if (m_logLevel>=kInfoLog) {
	  std::stringstream message;
	  message << "Results of analysis " << m_analysisSerialNumber << " for ROD " << m_rodName << " are trashed.";
// 	  sendMessage(*m_out, ExecutionUnit::componentName(), "JOBTHREAD_analysis",MRS_INFORMATION, "ExecutionUnit::JobThread::analyse", message.str());
	 ers::info(PixLib::pix::daq (ERS_HERE,  "ExecutionUnit::JobThread::analyse", message.str()));
	}
      }
    }
    catch ( CAN::MRSException & err) {
      m_joblett->setFailure();
      err.sendMessage();//*m_out,"ExecutionUnit::JobThread::analyse");
    }
    catch (...) {
      m_joblett->setFailure();
      if (m_logLevel>=kDiagnosticsLog) {
	std::cout<<"ERROR [ExecutionUnit::JobThread::analyse] Caught an unhandled exception."<<std::endl;
      }
      if (m_logLevel>=kErrorLog) {
// 	sendMessage(*m_out, ExecutionUnit::componentName(), "JOBTHREAD_analysis",MRS_ERROR, "ExecutionUnit::JobThread::analyse", 
	ers::error(PixLib::pix::daq (ERS_HERE, "ExecutionUnit::JobThread::analyse", 
		    "Unknown exception caught while analysing data!"));
      }
    }

    if (m_logLevel>=kDiagnosticsLog) {
      std::stringstream message;
      message << "Final status of analysis " << m_analysisSerialNumber << " for ROD " << m_rodName << " " << AnalysisStatus::name(m_joblett->status()) << " .";
//       sendMessage(*m_out, ExecutionUnit::componentName(), "JOBTHREAD_analysis",MRS_DIAGNOSTIC, "ExecutionUnit::JobThread::analyse", message.str());
      ers::debug(PixLib::pix::daq (ERS_HERE, "ExecutionUnit::JobThread::analyse", message.str()));
    }

    if (set_final_status) {
      Lock lock(m_isDictionary->mutex());
      try {
	updateIsValue(m_isDictionary->dictionary(), m_joblett->isDictionaryName(), m_joblett->isStatus());
      } catch ( daq::is::Exception & ex ) {
	if (m_logLevel>=kErrorLog) {
// 	  sendMessage(*m_out, ExecutionUnit::componentName(), "JOBTHREAD_analysis",MRS_ERROR, "ExecutionUnit::JobThread::analyse", ex.what());
	 ers::error(PixLib::pix::daq (ERS_HERE, "ExecutionUnit::JobThread::analyse", ex.what()));
	}
      }
    }

    m_joblett->setIsDone();
    m_joblett=NULL;
  }

  ExecutionUnit::ExecutionUnit(//IPCPartition &mrs_partition, 
			       IsDictionary &dictionary, 
			       IMasterDataServer &data_server, 
			       IArchivingUnit *archiver,
			       unsigned int n_slots,
			       IWorker &worker) 
    : //m_out(new MRSStream(mrs_partition)),
      m_isDictionary(&dictionary),
      m_dataServer(&data_server),
      m_resultArchive(archiver),
      m_nJoblettsQueued(0),
      m_nJoblettsReady(0),
      m_nJoblettsExecuted(0),
      m_worker(&worker),
      m_maxIdleVMSize(1024*1024*4 /* 4 GB */),
      m_logLevel(kWarnLog),
      m_shutdown(false),
      m_dataServerIsDown(false)
  {
    assert(m_resultArchive);
    // create the analysis threads
    m_threadList.reserve(n_slots);
    for (unsigned int slot_i=0; slot_i<n_slots; slot_i++) {
      //      m_threadList.push_back(new JobThread(*m_isDictionary, *this, m_resultArchive,mrs_partition, m_logLevel) );
      m_threadList.push_back(new JobThread(*m_isDictionary, *this, m_resultArchive,m_logLevel) );
    }
    start_undetached();
  }

  ExecutionUnit::~ExecutionUnit() {
    for(JobThreadList_t::iterator thread_iter = m_threadList.begin();
	thread_iter != m_threadList.end();
	thread_iter++) {
      (*thread_iter)->initiateShutdown(false);
    }

    for(JobThreadList_t::iterator thread_iter = m_threadList.begin();
	thread_iter != m_threadList.end();
	thread_iter++) {
      (*thread_iter)->shutdownWait();
      delete (*thread_iter);
      (*thread_iter) = NULL;
    }
    if (m_logLevel>=kDiagnosticsLog) {
//       Lock lock(m_mrsMutex);
//       sendMessage(*m_out,"ANAEXEC_dtor",MRS_DIAGNOSTIC, "ExecutionUnit::dtor", "Destructed the analysis threads.");
      ers::debug(PixLib::pix::daq (ERS_HERE, "ExecutionUnit::dtor", "Destructed the analysis threads."));
    }
  }


  void ExecutionUnit::queueJob(const SerialNumber_t analysis_serial_number,
			       const SerialNumbers_t serial_number_list,
			       const InfoPtr_t<PixLib::ScanInfo_t> &scan_info,
			       const SerialNumber_t src_analysis_serial_number,
			       PixA::ConnectivityRef &conn,
			       const std::vector<PixA::DisabledListRoot> &disabled_lists,
			       JoblettList_t::iterator joblett_begin,
			       JoblettList_t::const_iterator joblett_end)
  {

    AnalysisThreadList_t *analysis_thread_list=NULL;

    {

      for (JoblettList_t::iterator joblett_iter = joblett_begin;
	   joblett_iter != joblett_end;
	   joblett_iter++) {

	// create a new data server which first will take requests;
	std::shared_ptr<IAnalysisDataServer>  data_server ( m_dataServer->createAnalysisDataServer(analysis_serial_number,
												     joblett_iter->first,
												     serial_number_list,
												     scan_info,
												     src_analysis_serial_number,
												     conn,
												     disabled_lists) );
	bool error=true;
	try {
	  joblett_iter->second->analysis()->requestHistograms(data_server.get(), joblett_iter->first );
	  joblett_iter->second->postProcessing()->requestData(data_server.get(), joblett_iter->first );
	  joblett_iter->second->setDataServer(data_server);
	  {
	    Lock lock(m_analysisListMutex);
	    if (m_exitProcessing.isFlagged())  return;
	    if (analysis_thread_list == NULL) {
	      analysis_thread_list = &(m_analysisList[analysis_serial_number]);
	    }
	    analysis_thread_list->insert( *joblett_iter );
	  }

	  // then when the data server is completly setup, it is handed to the master which 
	  // executes the requests.

	  // Note: The joblett has be inserted into the analysis list before the "that is all" can be send.
	  // Otherwise the DataServer may have fulfilled the requests before the
	  // joblett is reachable, i.e. the joblett cannot be informed by the data server that 
	  // all the requested data has arrived.

	  joblett_iter->second->setWaiting();
	  {
	    Lock lock(m_isDictionary->mutex());
	    try {
	      updateIsValue(m_isDictionary->dictionary(), joblett_iter->second->isDictionaryName(), joblett_iter->second->isStatus() );
	    }
	    catch ( daq::is::Exception & ex ) {
	      if (m_logLevel>=kErrorLog) {
// 		Lock lock(m_mrsMutex);
// 		sendMessage(*m_out, s_componentName, "ANAEXEC_queueJob",MRS_ERROR, "ExecutionUnit::queueJob", ex.what());
		ers::error(PixLib::pix::daq (ERS_HERE,  "ExecutionUnit::queueJob", ex.what()));
	      }
	    }

	  }

	  m_nJoblettsQueued++;
	  m_dataServer->addAnalysisDataServer(analysis_serial_number, joblett_iter->first, data_server );
	  error=false;
	}
	catch ( CAN::MRSException & err) {
	  if (m_logLevel>=kErrorLog) {
	    //	    Lock lock(m_mrsMutex);
	    err.sendMessage();//*m_out,"ExecutionUnit::queueJob");
	  }
	}
	catch(...) {
	  if (m_logLevel>=kErrorLog) {
	    std::stringstream message;
	    message << "Caught unknown exception while trying to setup the data server (" 
		    << joblett_iter->first
		    << " / "
		    << makeAnalysisSerialNumberString(analysis_serial_number) 
		    << ").";
// 	    Lock lock(m_mrsMutex);
// 	    sendMessage(*m_out, s_componentName, "ANAEXEC_queueJob",MRS_ERROR, "ExecutionUnit::queueJob", message.str());
	    ers::error(PixLib::pix::daq (ERS_HERE,  "ExecutionUnit::queueJob", message.str()));
	  }
	}
	if (error) {
	  joblett_iter->second->setFailure();
	  Lock lock(m_isDictionary->mutex());
	  try {
	    updateIsValue(m_isDictionary->dictionary(), joblett_iter->second->isDictionaryName(), joblett_iter->second->isStatus());
	  } catch ( daq::is::Exception & ex ) {
 	    if (m_logLevel>=kErrorLog) {
// 	      sendMessage(*m_out, ExecutionUnit::componentName(), "JOBTHREAD_analysis",MRS_ERROR, "ExecutionUnit::JobThread::analyse", ex.what());
	    ers::error(PixLib::pix::daq (ERS_HERE,  "ExecutionUnit::JobThread::analyse", ex.what()));
	    }
	  }
	  joblett_iter->second->setIsDone();
	}

      }
    }
  }

  void *ExecutionUnit::run_undetached(void *arg) 
  {
    assert(arg==NULL);
    processExecutionQueue();
    m_exitProcessing.setFlag();
    return NULL;
  }

  bool ExecutionUnit::dataAvailable(SerialNumber_t serial_number, const PixA::RodLocationBase &rod_location) 
  {
    Lock lock_anaysisListMutex(m_analysisListMutex);
    if (!m_exitProcessing.isFlagged()) {

      AnalysisList_t::iterator analysis_iter = m_analysisList.find(serial_number);
      if (analysis_iter != m_analysisList.end()) {

	AnalysisThreadList_t::iterator joblett_iter =  analysis_iter->second.find(rod_location);
	if (joblett_iter !=  analysis_iter->second.end()) {
	  assert( joblett_iter->second->status() < AnalysisStatus::kReady);
	  m_nJoblettsQueued--;
	  m_nJoblettsReady++;
	  joblett_iter->second->setReady();
	  {
	    Lock lock_isDictionaryMutex(m_isDictionary->mutex());
	    try {
	      updateIsValue(m_isDictionary->dictionary(), joblett_iter->second->isDictionaryName(), joblett_iter->second->isStatus() );
	    }
	    catch ( daq::is::Exception & ex ) {
	    }
	  }

	  m_analysisReady.setFlag();
	  return true;
	}
      }
    }
    return false;
  }

  void ExecutionUnit::processExecutionQueue()
  {
    if (m_logLevel>=kInfoLog) {
//       Lock lock(m_mrsMutex);
//       sendMessage(*m_out, s_componentName, "ANAEXEC_start",MRS_INFORMATION, "ExecutionUnit::processExecutionQueue", "Start analysis execution loop.");
      ers::info(PixLib::pix::daq (ERS_HERE, "ExecutionUnit::processExecutionQueue", "Start analysis execution loop."));
    }
    for(;!m_shutdown;) {

      JobThreadList_t::iterator thread_iter = m_threadList.begin();
      for(;!m_shutdown;) {
	{
	  thread_iter = m_threadList.begin();
	  for (;thread_iter != m_threadList.end() && (*thread_iter)->isUsed(); thread_iter++) ;
	  if (thread_iter != m_threadList.end()) {
	    break;
	  }
	}
	m_threadFree.wait(m_shutdown);
      }

      if (!m_shutdown) {

	{
	Lock lock_analysisLockMutex(m_analysisListMutex);
	//bool analysis_ready=false;
	for (AnalysisList_t::iterator analysis_iter = m_analysisList.begin();
	     analysis_iter != m_analysisList.end()  && thread_iter != m_threadList.end() && !m_shutdown;
	     ) {

	  AnalysisList_t::iterator current_analysis_iter = analysis_iter++;
	  for (AnalysisThreadList_t::iterator joblett_iter = current_analysis_iter->second.begin();
	       joblett_iter != current_analysis_iter->second.end() && thread_iter != m_threadList.end()  && !m_shutdown;
	       ) {
	    AnalysisThreadList_t::iterator current_joblett_iter = joblett_iter++;

	    if (current_joblett_iter->second->isReady()) {
	      //analysis_ready=true;
	      if (current_joblett_iter->second->isAbortRequested()) {
		if (m_logLevel>=kDiagnosticsLog) {
		  std::stringstream message;
		  message << " Abort request for  ROD " << current_joblett_iter->first << " of " << current_analysis_iter->first << ".";
		  {
// 		    Lock lock(m_mrsMutex);
// 		    sendMessage(*m_out, s_componentName, "ANAEXEC_abort",MRS_DIAGNOSTIC, "ExecutionUnit::processExecutionQueue", message.str());
		   ers::debug(PixLib::pix::daq (ERS_HERE, "ExecutionUnit::processExecutionQueue", message.str()));
		  }
		}

		current_joblett_iter->second->setAborted();
	      }
	      else {
		// first set status then start the job, because the job modifies the status himself
		current_joblett_iter->second->setRunning();
	      }

	      {
		Lock lock_isDictionaryMutex(m_isDictionary->mutex());

		try {
		  updateIsValue(m_isDictionary->dictionary(), current_joblett_iter->second->isDictionaryName(), current_joblett_iter->second->isStatus() );
		}
		catch ( daq::is::Exception & ex ) {
		  if (m_logLevel>=kErrorLog) {
// 		    Lock lock(m_mrsMutex);
// 		    sendMessage(*m_out, s_componentName, "ANAEXEC_queueJob",MRS_ERROR, "ExecutionUnit::queueJob", ex.what());
		    ers::error(PixLib::pix::daq (ERS_HERE, "ExecutionUnit::queueJob", ex.what()));
		  }
		}

	      }
	      m_nJoblettsReady--;
	      m_nJoblettsExecuted++;

	      if (current_joblett_iter->second->status() != AnalysisStatus::kAborted) {

	      (*thread_iter)->setJob(current_analysis_iter->first, current_joblett_iter->first, current_joblett_iter->second.get());

	      if (m_logLevel>=kInfoLog) {
		std::stringstream message;
		message << " Analyse ROD " << current_joblett_iter->first << " of " << current_analysis_iter->first << ".";
		{
// 		  Lock lock(m_mrsMutex);
// 		  sendMessage(*m_out, s_componentName, "ANAEXEC_start_analysis",MRS_INFORMATION, "ExecutionUnit::processExecutionQueue", message.str());
		  ers::info(PixLib::pix::daq (ERS_HERE, "ExecutionUnit::processExecutionQueue", message.str()));
		}
	      }

	      for ( ; thread_iter != m_threadList.end() && (*thread_iter)->isUsed(); thread_iter++);
	      if (thread_iter == m_threadList.end()) 
		break;
	      }

	    }
	    else if (current_joblett_iter->second->isDone()) {
	      current_analysis_iter->second.erase(current_joblett_iter);
	    }
	  }
	  if (current_analysis_iter->second.empty()) {
	    m_analysisList.erase(current_analysis_iter);
	  }

	}

	}

	if (thread_iter != m_threadList.end() && (*thread_iter)) {

	  bool wait=true;

	  while ((m_nJoblettsQueued==0 && m_nJoblettsReady==0) && m_resultArchive->isArchiving()) {
	    if (m_analysisReady.timedwait(20, m_shutdown)) {
	      wait=false;
	      break;
	    }
	  }

	  if (wait ) {
	    checkWorkerStatus();  
	    m_analysisReady.wait(m_shutdown);
	  }
	}
      }
    }

    if (m_logLevel>=kInfoLog) {
//       Lock lock(m_mrsMutex);
//       sendMessage(*m_out, s_componentName,"ANAEXEC_TERM",MRS_INFORMATION, "ExecutionUnit::processExecutionQueue", "Terminated execution loop.");
      ers::info(PixLib::pix::daq (ERS_HERE,  "ExecutionUnit::processExecutionQueue", "Terminated execution loop."));
    }
  }

  bool ExecutionUnit::checkWorkerStatus()
  {
    Lock lock(m_analysisListMutex);

    if (m_logLevel>=kDiagnostics5Log) {
      //      Lock lock(m_mrsMutex);
      std::stringstream message;
      message << "Status : queued = " << m_nJoblettsQueued << ", ready = " << m_nJoblettsReady << " archiving = " 
	      << ( m_resultArchive->isArchiving() ? "yes" : "no")
	      << " vm size = " << vmsize(false)
 	      << std::endl;
//       sendMessage(*m_out, s_componentName,"ANAEXEC_vmlimit",MRS_DIAGNOSTIC, "ExecutionUnit::checkWorkerStatus", message.str());
	ers::debug(PixLib::pix::daq (ERS_HERE,  "ExecutionUnit::checkWorkerStatus", message.str()));
    }

    if (m_nJoblettsQueued==0 && m_nJoblettsReady==0) {
      bool all_done=true;
      if (!m_analysisList.empty()) {
	for (AnalysisList_t::const_iterator analysis_iter = m_analysisList.begin();
	     analysis_iter != m_analysisList.end();
	     ++analysis_iter) {
	  for (AnalysisThreadList_t::const_iterator thread_iter = analysis_iter->second.begin();
	       thread_iter != analysis_iter->second.end();
	       ++thread_iter) {
	    if (thread_iter->second.get() && !thread_iter->second->isDone()) {
	      all_done=false;
	      break;
	    }
	  }
	}
      }

      if (all_done) {
	unsigned long  current_vm_size = vmsize(false);
	if (current_vm_size > m_maxIdleVMSize) {
	  if (!m_resultArchive->isArchiving()) {
	    if (m_logLevel>=kInfoLog) {
	      std::stringstream message;
	      message << "The worker is idle and the VM size of " << current_vm_size << " exceeds the limit of " << m_maxIdleVMSize << "."
		      << " Will trigger a worker restart.";
// 	      Lock lock(m_mrsMutex);
// 	      sendMessage(*m_out, s_componentName,"ANAEXEC_vmlimit",MRS_INFORMATION, "ExecutionUnit::checkWorkerStatus", message.str());
	      ers::info(PixLib::pix::daq (ERS_HERE, "ExecutionUnit::checkWorkerStatus", message.str()));
	    }
	    m_worker->triggerRestart();
	  }
	  else {
	    return true;
	  }
	}
      }
      else {
	if (m_logLevel>=kDiagnostics5Log) {
	  std::stringstream message;
	  message << "The worker seems idle but the analysis list is not empty." << std::endl;
// 	  sendMessage(*m_out, s_componentName,"ANAEXEC_vmlimit",MRS_DIAGNOSTIC, "ExecutionUnit::checkWorkerStatus", message.str() );
	  ers::debug(PixLib::pix::daq (ERS_HERE, "ExecutionUnit::checkWorkerStatus", message.str()) );
	  
	  message.clear();
	  message << "Entries in the analysis list : " << std::endl;
	  for (AnalysisList_t::const_iterator analysis_iter = m_analysisList.begin();
	       analysis_iter != m_analysisList.end();
	       ++analysis_iter) {

	    message << analysis_iter->first << " : ";
	    for (AnalysisThreadList_t::const_iterator thread_iter = analysis_iter->second.begin();
		 thread_iter != analysis_iter->second.end();
		 ++thread_iter) {
	      message << thread_iter->first << ( thread_iter->second.get() && !thread_iter->second->isDone() ? "*" : "") << ", ";
	    }
	    message << ".";

	  }
// 	  Lock lock(m_mrsMutex);
// 	  sendMessage(*m_out, s_componentName,"ANAEXEC_vmlimit",MRS_DIAGNOSTIC, "ExecutionUnit::checkWorkerStatus", message.str() );
	  ers::debug(PixLib::pix::daq (ERS_HERE, "ExecutionUnit::checkWorkerStatus", message.str() ));
	}
      }
    }
    return  false;
  }

  void ExecutionUnit::getQueueStatus(CORBA::ULong &jobs_waiting_for_data,
				     CORBA::ULong &jobs_waiting,
				     CORBA::ULong &jobs_running,
				     CORBA::ULong &jobs_executed,
				     CORBA::ULong &empty_slots) 
  {
    Lock lock(m_exitProcessing.mutex());
    if (m_exitProcessing.isFlagged())  return;

    empty_slots=0;
    jobs_running=0;
    jobs_executed=m_nJoblettsExecuted;
    jobs_waiting=m_nJoblettsReady;
    jobs_waiting_for_data=m_nJoblettsQueued;

    for (JobThreadList_t::const_iterator thread_iter = m_threadList.begin();
	 thread_iter != m_threadList.end();
	 thread_iter++) {
      if ((*thread_iter)->isUsed()) {
	jobs_running++;
      }
      else {
	empty_slots++;
      }
    }
  }


  bool ExecutionUnit::dequeuAnalysis(SerialNumber_t    analysis_serial_number,
				     const char *rod_conn_name) {

    Lock lock(m_analysisListMutex);
    AnalysisList_t::iterator analysis_iter = m_analysisList.find(analysis_serial_number);
    if (analysis_iter != m_analysisList.end()) {

      for (AnalysisThreadList_t::iterator joblett_iter = analysis_iter->second.begin();
	   joblett_iter != analysis_iter->second.end();
	   joblett_iter++) {

	if (joblett_iter->first == rod_conn_name && !joblett_iter->second->isRunning()) {
	  analysis_iter->second.erase(joblett_iter);
	  return true;
	}
      }
    }
    return false;
  }

  void ExecutionUnit::abortAnalysis(SerialNumber_t    analysis_serial_number, bool ignore_results) {

    {
    Lock lock(m_analysisListMutex);
    AnalysisList_t::iterator analysis_iter = m_analysisList.find(analysis_serial_number);
    if (analysis_iter != m_analysisList.end()) {

      for (AnalysisThreadList_t::iterator joblett_iter = analysis_iter->second.begin();
	   joblett_iter != analysis_iter->second.end();
	   joblett_iter++) {

	if (joblett_iter->second->status()<=AnalysisStatus::kRunning) {

	  if (ignore_results) {
	    joblett_iter->second->requestAbortAndIgnore();
	  }
	  else {
	    joblett_iter->second->requestAbort();
	  }
	}
      }
      //	  m_analysisList.erase(analysis_iter);
    }
    }

    if (m_logLevel>=kInfoLog) {
      std::stringstream message;
      message << "Aborted analysis " << analysis_serial_number << ( ignore_results ? " and ignore results." : ".");
      {
// 	Lock lock(m_mrsMutex);
// 	sendMessage(*m_out,"ANAEXEC_ABORT",s_componentName, MRS_INFORMATION, "ExecutionUnit::abortAnalysis", message.str());
	ers::info(PixLib::pix::daq (ERS_HERE, "ExecutionUnit::abortAnalysis", message.str()));
      }
    }
  }

  void ExecutionUnit::abortAnalysis(SerialNumber_t    analysis_serial_number, const char *rod_conn_name, bool ignore_result) {

    Lock lock(m_analysisListMutex);
    AnalysisList_t::iterator analysis_iter = m_analysisList.find(analysis_serial_number);
    if (analysis_iter != m_analysisList.end()) {

      for (AnalysisThreadList_t::iterator joblett_loop_iter = analysis_iter->second.begin();
	   joblett_loop_iter != analysis_iter->second.end();
	   ) {
	AnalysisThreadList_t::iterator joblett_iter = joblett_loop_iter++;
	if (joblett_iter->first == rod_conn_name && joblett_iter->second->status() <= AnalysisStatus::kRunning) {
	  //	  analysis_iter->second.erase(joblett_iter);
	  if (ignore_result) {
	    joblett_iter->second->requestAbortAndIgnore();
	  }
	  else {
	    joblett_iter->second->requestAbort();
	  }
	}
      }
      //	  m_analysisList.erase(analysis_iter);
    }

    if (m_logLevel>=kInfoLog) {
      std::stringstream message;
      message << "Aborted analysis " << analysis_serial_number << ( ignore_result ? " and ignore results." : ".");
      {
// 	Lock lock(m_mrsMutex);
// 	sendMessage(*m_out,"ANAEXEC_ABORT",s_componentName,MRS_INFORMATION, "ExecutionUnit::abortAnalysis", message.str());
	ers::info(PixLib::pix::daq (ERS_HERE, "ExecutionUnit::abortAnalysis", message.str()));
      }
    }
  }

  void ExecutionUnit::reset() 
  {
    {
      {
      Lock lock(m_analysisListMutex);
      for ( AnalysisList_t::iterator analysis_iter = m_analysisList.begin();
	    analysis_iter != m_analysisList.end();
	    analysis_iter++) {

	for (AnalysisThreadList_t::iterator joblett_iter = analysis_iter->second.begin();
	     joblett_iter != analysis_iter->second.end();
	     joblett_iter++) {

	  if (joblett_iter->second->status()<=AnalysisStatus::kRunning) {
	    joblett_iter->second->requestAbort();
	  }
	}
	//	  m_analysisList.erase(analysis_iter);
      }
      }

      m_resultArchive->reset();

      // wait until all jobs have terminated
      if (m_logLevel>=kDiagnosticsLog) {
// 	Lock mrs_lock(m_mrsMutex);
// 	sendMessage(*m_out,"ANAEXEC_resetWait",s_componentName,MRS_DIAGNOSTIC, "ExecutionUnit::reset", "Wait until all jobs have terminated.");
	ers::debug(PixLib::pix::daq (ERS_HERE, "ExecutionUnit::reset", "Wait until all jobs have terminated."));
      }
      for(;;) {

	bool has_running_jobs=false;
	{
	Lock lock(m_analysisListMutex);
	for ( AnalysisList_t::iterator analysis_iter = m_analysisList.begin();
	      analysis_iter != m_analysisList.end();
	      analysis_iter++) {

	  for (AnalysisThreadList_t::iterator joblett_iter = analysis_iter->second.begin();
	       joblett_iter != analysis_iter->second.end();
	       joblett_iter++) {

	    if (!joblett_iter->second->isDone() && !joblett_iter->second->isReady() && !m_dataServerIsDown) {
	      has_running_jobs=true;
	      if (m_logLevel>=kDiagnostics4Log) {
		std::stringstream message;
		message << "Job still running : A" << analysis_iter->first  << ", " << joblett_iter->first 
			<< " status = " << AnalysisStatus::name( joblett_iter->second->status() )
 			<< "." << std::endl;
// 		Lock lock(m_mrsMutex);
// 		sendMessage(*m_out,"ANAEXEC_clearAnalysisList",s_componentName,MRS_DIAGNOSTIC, "ExecutionUnit::reset", message.str());
		  ers::debug(PixLib::pix::daq (ERS_HERE, "ExecutionUnit::reset", message.str()));
	      }
	      break;
	    }
	  }
	}
	}
	if (!has_running_jobs) break;
	// //	  m_analysisList.erase(analysis_iter);
	m_threadFree.timedwait(1);
      }

      //     // @todo the following will crash. Should this method wait until all jobs have terminated ?

      if (m_logLevel>=kDiagnosticsLog) {
// 	Lock lock(m_mrsMutex);
// 	sendMessage(*m_out,"ANAEXEC_clearAnalysisList",s_componentName,MRS_DIAGNOSTIC, "ExecutionUnit::reset", "Clear the analysis list.");
	ers::debug(PixLib::pix::daq (ERS_HERE, "ExecutionUnit::reset", "Clear the analysis list."));
      }
      m_analysisList.clear();
    }
    m_nJoblettsQueued=0;
    //{
    //  Lock lock(m_mrsMutex);
    //  sendMessage(*m_out,"ANAEXEC_unloadPlugins",s_componentName,MRS_DIAGNOSTIC, "ExecutionUnit::reset", "Unload all plugins.");
    //}

    // AnalysisFactory::unloadAll();
    

    if (m_logLevel>=kInfoLog) {
//       Lock lock(m_mrsMutex);
//       sendMessage(*m_out,"ANAEXEC_reset",s_componentName,MRS_INFORMATION, "ExecutionUnit::reset", "Resetted execution queues and unloaded all plugins.");
      ers::info(PixLib::pix::daq (ERS_HERE, "ExecutionUnit::reset", "Resetted execution queues and unloaded all plugins."));
    }
  }

  void ExecutionUnit::initiateShutdown(bool ignore_result)
  {
    m_shutdown=true;

    for(JobThreadList_t::iterator thread_iter = m_threadList.begin();
	thread_iter != m_threadList.end();
	thread_iter++) {
      (*thread_iter)->initiateShutdown(ignore_result);
    }
    m_resultArchive->initiateShutdown();
  }

  void ExecutionUnit::shutdownWait() {
    // sendMessage(*m_out,"ANAEXEC_ABORT",MRS_INFORMATION, "ExecutionUnit::shutdown", "Shutdown execution processor.");
    // @todo reset ?
    m_resultArchive->initiateShutdown();
    reset();
    m_shutdown=true;
    m_threadFree.setFlag();
    m_analysisReady.setFlag();

    if (state()!=omni_thread::STATE_TERMINATED) {
      if (m_logLevel>=kInfoLog) {
// 	Lock lock(m_mrsMutex);
// 	sendMessage(*m_out,"ANAEXEC_shutdown_wait",s_componentName,MRS_INFORMATION, "ExecutionUnit::shutdown", "Waiting for execution processor thread to shut down.");
	ers::info(PixLib::pix::daq (ERS_HERE, "ExecutionUnit::shutdown", "Waiting for execution processor thread to shut down."));
      }
      m_exitProcessing.wait();
      while (state()!=omni_thread::STATE_TERMINATED) {
	usleep(1000);
      }
    }

    for(JobThreadList_t::iterator thread_iter = m_threadList.begin();
	thread_iter != m_threadList.end();
	thread_iter++) {
      (*thread_iter)->shutdownWait();
      delete *thread_iter;
      *thread_iter = NULL;
    }
    m_threadList.clear();

    m_resultArchive->shutdownWait();
    
    if (m_logLevel>=kInfoLog) {
//       Lock lock(m_mrsMutex);
//       sendMessage(*m_out,"ANAEXEC_shutdown",MRS_INFORMATION, "ExecutionUnit::shutdown", "Shutting down.");
      ers::info(PixLib::pix::daq (ERS_HERE, "ExecutionUnit::shutdown", "Shutting down."));
    }

  }

  void ExecutionUnit::setLogLevel(ELogLevel log_level) {
    m_logLevel = log_level;

    for(JobThreadList_t::iterator thread_iter = m_threadList.begin();
	thread_iter != m_threadList.end();
	thread_iter++) {
      (*thread_iter)->setLogLevel(log_level);
    }
    m_resultArchive->setLogLevel(log_level);
  }

}
