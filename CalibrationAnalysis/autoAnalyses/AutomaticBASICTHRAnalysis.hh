#ifndef _CAN_AutomaticBASICTHRAnalysis_hh_
#define _CAN_AutomaticBASICTHRAnalysis_hh_

#include "AutomaticAnalysisBase.hh"
#include "BASICTHRanalysisROOT.h"
#include "coreAnalyses/analysisInput_BASICTHR.h"
#include "coreAnalyses/analysisOutput_BASICTHR.h"


namespace CAN {

  /** 
   * Analysis for BASICTHR scans, for one pp0 
   */
  class AutomaticBASICTHRAnalysis : public AutomaticAnalysisBase 
  {
  public:
    AutomaticBASICTHRAnalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &);
    ~AutomaticBASICTHRAnalysis() {};

  private:
    void newAnalysisInput() { m_newInput = new AnalysisInputBASICTHR(); };
    void addNewAnalysisInput(AnalysisInput_t analysisInput) { analysisInput->Add(m_newInput); };  
    ////
    void requestScanConfigs  (IAnalysisDataServer *ana_data_server); 
    void requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name);
    void getBasics       (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos); 
    void getScanConfigs  (PixLib::Config&   scan_config);
    void getModuleConfigs(PixLib::Config& module_config);
    void getHistograms   (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos);
    void saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result);
 
  private:
    std::string             m_histo_type_chi2; 
    std::string             m_histo_type_threshold; 
    std::string             m_histo_type_noise; 
    AnalysisInputBASICTHR*  m_newInput;
  };

}
#endif

