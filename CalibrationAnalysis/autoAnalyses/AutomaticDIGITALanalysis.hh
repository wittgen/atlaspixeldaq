#ifndef _CAN_AutomaticDIGITALanalysis_hh_
#define _CAN_AutomaticDIGITALanalysis_hh_

#include "Analysis.hh"
#include "DIGITALanalysisROOT.h"
#include "analysisInput_DIGITAL.h"

#include "DataContainer/HistoRef.h"
#include "DataContainer/HistoInfo_t.h"
#include "DataContainer/HistoUtil.h"
#include "DataContainer/PixDbDataContainer.h"
#include <ConfigWrapper/ConfObjUtil.h>

#include "DataServerCT.hh"
#include "HistoConverter.hh"

namespace CAN {

  /** 
   * Analysis for DIGITAL scans, for one pp0 
   */
  class AutomaticDIGITALanalysis : public IAnalysisPerPP0 
  {
  public:
    AutomaticDIGITALanalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &) : 
      m_serialNumber(analysis_serial_number), m_abortRequest(false), m_name("DIGITALanalysis"), m_config("TestAnalysis") {};
    
    /** Histograms need to be requested from the data server prior to the analysis.
     */
    void requestHistograms(IDataServer *data_server, const PixA::Pp0LocationBase &pp0_location);

    void analyse(IDataServer *data_server, const PixA::Pp0LocationBase &pp0_location, 
		 AnalysisResultList_t &analysis_results);
    

    /////
    PixLib::Config &config() { return m_config; }
    
    const std::string& name() const { return m_name; };
    SerialNumber_t serialNumber() const         { return m_serialNumber;}
    void requestAbort()                         { m_abortRequest=true;}




  protected:
    bool abortRequested() const                 { return m_abortRequest; }
    // non privte to give quick non-virtual access
    SerialNumber_t  m_serialNumber;
    bool            m_abortRequest;
    std::string     m_name;


  private:
    DIGITALanalysisROOT m_analysis;
    std::string m_histo_type; //should be in mother class? No, other analyses may have a number of different ones
    std::string m_pp0_name;

    PixLib::Config m_config;
    
    //helper functions
    AnalysisInput_t ExtractDIGITALdata(std::list<PixA::HistoHandle*> histograms, std::list<PixA::ScanConfigRef *> scan_configs );
    HistoConverter converter;
  };

}
#endif

