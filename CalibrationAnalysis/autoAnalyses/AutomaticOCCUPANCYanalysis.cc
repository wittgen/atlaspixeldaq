#include "AutomaticOCCUPANCYanalysis.hh"
#include "PixFe/PixGeometry.h"
#include "DataContainer/GenericHistogramAccess.h"

namespace CAN {
  
  AutomaticOCCUPANCYanalysis::AutomaticOCCUPANCYanalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &oi)
    : AutomaticAnalysisBase(analysis_serial_number, oi)
  {
    m_name = "OCCUPANCYanalysis";
    m_config = PixLib::Config("OCCUPANCYanalysis");
    m_analysis = unique_ptr<OCCUPANCYanalysisROOT>(new OCCUPANCYanalysisROOT);
    OCCUPANCYanalysisROOT* anptr = static_cast<OCCUPANCYanalysisROOT*>(m_analysis.get());
    
    m_config.addGroup("general");
    PixLib::ConfGroup &group = m_config["general"];
    
    // more variables can be added here
    group.addFloat("max_bad_pixel", anptr->max_bad_pixel, 50, "maximum number of inefficient or dead pixel", true);
    group.addFloat("max_extra_hits_allowed", anptr->max_extra_hits_allowed, 0, "maximum extra hits allowed", true);
    group.addFloat("min_charge", anptr->min_charge, 5000, "minimum charge to expect 100% hits in analog scan", true);
    group.addBool("apply_cut_on_MS1", anptr->apply_cut_on_MS1, false, "choice of applying cut on failures in first maskstage", true);
    group.addFloat("max_missing_hits_MS1", anptr->max_missing_hits_MS1, 10, "maximum percentage of missing hits allowed in first mask stage", true);
    group.addFloat("DeadFE_percentage", anptr->DeadFE_percentage, 95, "Dead FE if this percentage of pixel in FE is dead", true);
//*************** new variable added [maria elena] to count the sum of dead + badmissing + badextra pixels
    group.addFloat("Max_Dead_Pixel", anptr->Max_Dead_Pixel, 1000, "Max number of allowed dead pixel", true);
//************************
    group.addBool("create_Pixelmap", anptr->create_Pixelmap, true, "create a map of dead pixel", true);

    vector<string> occupancy_names;
    m_histo_type = "OCCUPANCY";
    occupancy_names.push_back(m_histo_type);
    m_scan_histo_names[""] = occupancy_names;
    std::cout<<"AutomaticOCCUPANCYanalysis: constructed."<<std::endl;

  }

  void AutomaticOCCUPANCYanalysis::requestScanConfigs(IAnalysisDataServer* ana_data_server)
  {
    ana_data_server->getDataServer()->requestScanConfig();
  }

  void AutomaticOCCUPANCYanalysis::requestModuleConfigs(IAnalysisDataServer* ana_data_server, string module_name)
  {
    ana_data_server->getDataServer()->requestModuleConfig(module_name);
    std::cout<<"AutomaticOCCUPCANYanalysis:: requesting configuration for module "<<module_name<<std::endl;
  }

void AutomaticOCCUPANCYanalysis::getHistograms(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos)
  {
    cout << "AutomaticOCCUPANCYanalysis::getHisto   at start." << endl;
    const TH2* h_occ=0;
    string conn_name;
    if(m_CTdata)
      {
 	DataServerCT* dataServerCT = dynamic_cast<DataServerCT*>(ana_data_server->getDataServer());
 	h_occ = converter.GetTheHisto2D_old(*iterator(dataServerCT->getHistograms(m_histo_type, m_pp0_name).begin(),pos),*iterator(dataServerCT->getScanConfigRefs(m_histo_type, m_pp0_name).begin(),pos), m_histo_type, 0,0,0,0);
	cout<<"CTdata is there"<<endl;
      }
    else
      {
 	conn_name = PixA::connectivityName(module_iter);
 	IDataServer* data_server = ana_data_server->getDataServer();
 	h_occ = converter.GetTheHisto2D(m_histo_type, data_server, conn_name, 0,0,0,0);
      }
    if(h_occ!=NULL)
      {
 	m_newInput->histo = h_occ;
	
      }
    else {
      cout<<"something went wrong, histogram is empty"<<endl;
      char mes[128];
      sprintf(mes, "Histogram for %s not found.", conn_name.c_str());
      throw PixA::ConfigException(mes);
    }
      
    PixGeometry geo(PixA::nRows(h_occ),PixA::nColumns(h_occ));
    m_feflv = geo.pixType();
    m_newInput->_ChipFeFlav = m_feflv;

    cout << "AutomaticOCCUPANCYanalysis::getHisto   at end." << endl;
  }
  
  void AutomaticOCCUPANCYanalysis::getBasics(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos)
  {
    m_newInput->pp0Name = m_pp0_name;
    m_newInput->moduleName = getModuleName(ana_data_server, module_iter, pos);
  }

  void AutomaticOCCUPANCYanalysis::getScanConfigs(PixLib::Config& scan_config)
  {
    cout << "AutomaticOCCUPANCYanalysis::getScanConfigs   at start." << endl;
    m_newInput->Nmasks = (int) converter.getTotalMaskStageSteps(confVal<std::string>(const_cast< PixLib::Config &>(scan_config)["general"]["maskStageTotalSteps"]));
    m_newInput->Nmasksdone = confVal<int>(scan_config["general"]["maskStageSteps"]);
      std::cout<<"Nmasks is:"<< (int) converter.getTotalMaskStageSteps(confVal<std::string>(const_cast< PixLib::Config &>(scan_config)["general"]["maskStageTotalSteps"])) <<std::endl;
    m_newInput->Nevents = confVal<int>(scan_config["general"]["repetitions"]);
      maskStagesMode = confVal<string>(scan_config["general"]["maskStageMode"]);
     digital = confVal<bool>(scan_config["fe"]["digitalInjection"]);
    m_newInput->digitalInjection = digital;
    if(!digital)
      {
    vcal = confVal<int>(scan_config["fe"]["vCal"]);
	float charge = 0;
	if(vcal != 0)
	  {
	    // we have to calculate the injected charge from the vcal setting, calculations should be roughly true, but can be replaced by correct formulas if needed
	    cout<<"analog scan was run using a fixed vcal"<<endl;
          cout << "_ChipFeFlav: " << m_feflv << endl;

          if(m_feflv == PixGeometry::FEI2_CHIP || m_feflv == PixGeometry::FEI2_MODULE){
              bool HighCap = confVal<bool>(scan_config["fe"]["chargeInjCapHigh"]);
              cout << "HighCap: " << HighCap << endl;
	   
	    if(HighCap) charge = 240*vcal;
              else charge = 42*vcal;
                    cout<<"analog scan with charge: "<<charge<<endl;
          }

          m_newInput->charge = charge;
	  }
	else
	  {
	    int NchargeSteps = confVal<int>(scan_config["loops"]["loopVarNStepsLoop_0"]);
	    cout<<"analog scan. charge steps: "<<NchargeSteps<<endl;
	    if(NchargeSteps == 1)
	      {
		charge = confVal<float>(scan_config["loops"]["loopVarMinLoop_0"]);
	      }
	    else
	      {
		cout<<"different charges were injected in the analysed analog scan"<<endl;
		// use the minimal charge to cut on anyways
		charge = confVal<float>(scan_config["loops"]["loopVarMinLoop_0"]);
	      }
	  }
	

      }


    std::string speed = confVal<std::string>(scan_config["mcc"]["mccBandwidth"]);
    if(speed == "SINGLE_40" || speed == "DOUBLE_40") m_newInput->MBs=40;
    else if(speed == "SINGLE_80" || speed == "DOUBLE_80") m_newInput->MBs=80;
    else m_newInput->MBs=-1; // should not happen

    m_newInput->axis = *converter.GetTheAxis(scan_config);
    cout << "AutomaticOCCUPANCYanalysis::getScanConfigs   at end." << endl;
  }
  
  void AutomaticOCCUPANCYanalysis::getModuleConfigs(PixLib::Config& module_config)
  {
      cout << "AutomaticOCCUPANCYanalysis::getModuleConfigs   at start." << endl;
    ROOTanalysisHelper helper;

    getDisabledFECP (module_config, m_feflv , m_newInput->fe_disable, m_newInput->cp_enable);

    int Nfe = helper.getNfrontEnd(m_feflv); // # of FEs in one module

    PixLib::Config &casted_a_config=const_cast<PixLib::Config &>(module_config);
    if (casted_a_config.size()<=0 && casted_a_config.subConfigSize()<=0) return;
    for (int fe=0; fe!=Nfe; ++fe) {
    //std::string maskStagesMode = confVal<string>(scan_config["general"]["maskStageMode"]);
	std::string fe_name = "PixFe_" + std::to_string(fe);
        PixLib::Config& fe_config = module_config.subConfig(fe_name);
        if(fe_config.size() <= 0 && fe_config.subConfigSize() <= 0) continue;
       // bool digital = confVal<bool>(scan_config["fe"]["digitalInjection"]);
        
        if(!digital){
          // int vcal = confVal<int>(scan_config["fe"]["vCal"]);
            if(vcal != 0){
              if(m_feflv == PixGeometry::FEI4_CHIP || m_feflv == PixGeometry::FEI4_MODULE){
                int cInj=0;
                  if (maskStagesMode=="FEI4_ENA_SCAP") {
                    cInj=((PixLib::ConfFloat&)fe_config["Misc"]["CInjLo"]).value();
                  } else if (maskStagesMode=="FEI4_ENA_LCAP"){
                    cInj=((PixLib::ConfFloat&)fe_config["Misc"]["CInjMed"]).value();
                  } else  if (maskStagesMode=="FEI4_ENA_BCAP"){
                    cInj=static_cast<ConfFloat&>(fe_config["Misc"]["CInjHi"]).value();
                  }
                float vcal_a = ((PixLib::ConfFloat&)fe_config["Misc"]["VcalGradient3"]).value();
                float vcal_b = ((PixLib::ConfFloat&)fe_config["Misc"]["VcalGradient2"]).value();
                float vcal_c = ((PixLib::ConfFloat&)fe_config["Misc"]["VcalGradient1"]).value();
                float vcal_d = ((PixLib::ConfFloat&)fe_config["Misc"]["VcalGradient0"]).value();

                float charge = (vcal_a*vcal*vcal*vcal + vcal_b*vcal*vcal + vcal_c*vcal + vcal_d)*cInj/0.160218;
                cout<<"analog scan with charge: "<<charge<<endl;
                m_newInput->charge = charge;
             }
           }
        }
     }
  }
  
  void AutomaticOCCUPANCYanalysis::saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result2)
  {
    ResultOCCUPANCY* module_result = static_cast<ResultOCCUPANCY*>(module_result2);
    unique_ptr<ROOTanalysisHelper> helper(new ROOTanalysisHelper());
    
    
    analysis_results.addValue<bool>("Passed_FirstMaskstageTest", module_result->moduleName.Data(), module_result->Passed_FirstMaskstageTest);
    analysis_results.addValue<float>("PixMissingHits", module_result->moduleName.Data(), module_result->PixMissingHits);
    analysis_results.addValue<float>("PixExtraHits", module_result->moduleName.Data(), module_result->PixExtraHits);
    analysis_results.addValue<float>("MaxMissingHits", module_result->moduleName.Data(), module_result->MaxMissingHits);
    analysis_results.addValue<float>("MaxExtraHits", module_result->moduleName.Data(), module_result->MaxExtraHits);
    analysis_results.addValue<float>("MissingHits", module_result->moduleName.Data(), module_result->MissingHits);
    analysis_results.addValue<float>("ExtraHits", module_result->moduleName.Data(), module_result->ExtraHits);
    analysis_results.addValue<float>("Missing hits in mask stage 1", module_result->moduleName.Data(), module_result->MissingHitsMS1);
    analysis_results.addValue<float>("Pixel with missing hits in mask stage 1", module_result->moduleName.Data(), module_result->PixMissingHitsMS1);
    analysis_results.addValue<float>("Missing Hits in mask stages > 1", module_result->moduleName.Data(), module_result->MissingHitsMSnot1);
    analysis_results.addValue<float>("Pixel with missing hits in mask stages > 1", module_result->moduleName.Data(), module_result->PixMissingHitsMSnot1);
    analysis_results.addValue<float>("Extra hits in mask stage 1", module_result->moduleName.Data(), module_result->ExtraHitsMS1);
    analysis_results.addValue<float>("Pixel with extra hits in mask stage 1", module_result->moduleName.Data(), module_result->PixExtraHitsMS1);
    analysis_results.addValue<float>("Extra Hits in mask stages > 1", module_result->moduleName.Data(), module_result->ExtraHitsMSnot1);
    analysis_results.addValue<float>("Pixel with extra hits in mask stages > 1", module_result->moduleName.Data(), module_result->PixExtraHitsMSnot1);
    analysis_results.addValue<float>("Dead Pixel", module_result->moduleName.Data(), module_result->DeadPixel);
    analysis_results.addValue<bool>("Dead Frontend", module_result->moduleName.Data(), module_result->DeadFE);
    // output only for analog scans
    if(!(m_newInput->digitalInjection)){
      analysis_results.addValue<float>("OverThreshold", module_result->moduleName.Data(), module_result->OverThreshold);
      analysis_results.addValue<float>("SeenPercentage", module_result->moduleName.Data(), module_result->SeenPercentage);
      analysis_results.addValue<bool>("HVopen", module_result->moduleName.Data(), module_result->HVopen);
    }
    if(module_result->MakePixelMap){ // checks if this was enabled in analysis configuration
      if(m_newInput->digitalInjection){
	analysis_results.addValue<PixelMap_t>("Digitally dead Pixel", module_result->moduleName.Data(), module_result->deadPixelMap);
      }
      else
	analysis_results.addValue<PixelMap_t>("Analog dead Pixel", module_result->moduleName.Data(), module_result->deadPixelMap);
    }
  }

}



 
