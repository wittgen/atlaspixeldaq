// 2008-06-27 Shih-Chieh Hsu
// 
// BOC_LINK Scan generates one 2D histogram per ROD. The histogram name is assigned to the first module on the ROD.
// The histogram is cloned and pointed to each module for later analysis. 
// Note that we use 40MHz for In/Outlink scan. 


#include "AutomaticInLinkOutLinkanalysis.hh"
#include <ExtendedAnalysisResultList_t.hh>

namespace CAN {

  AutomaticInLinkOutLinkanalysis::AutomaticInLinkOutLinkanalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &oi)  
    : AutomaticAnalysisBaseROD( analysis_serial_number, oi) 
  {
    m_name="InLinkOutLinkanalysis";
    m_config=PixLib::Config("InLinkOutLinkanalysis");
    m_analysis = unique_ptr<InLinkOutLinkanalysisROOT>(new InLinkOutLinkanalysisROOT);
    
    m_config.addGroup("general");
    PixLib::ConfGroup &group=m_config["general"];

    //initial cuts
    group.addFloat("cut_drx_min iolink", (static_cast<InLinkOutLinkanalysisROOT*>(m_analysis.get()))->cut_drx_min, 0.2, "cut_drx_min", true);
    group.addFloat("cut_ratio_bit_min iolink", (static_cast<InLinkOutLinkanalysisROOT*>(m_analysis.get()))->cut_ratio_bit_min, 0.45, "cut_ratio_bit_min", true);
    
    // histogram name 
    // PixelHistoServer.pixel_provider_ReloadOH./S000004514/ROD_C0_S11/L0_B12_S2_C6_M6C/FMTC_LINKMAP/A0/B0/C0/0:LNKMAP_0_0_0 <2/6/08 03:18:38.399832> <HistogramData<double>>
    vector<string> threshold_names;
    m_histo_type_name = "FMTC_LINKMAP";

    threshold_names.push_back(m_histo_type_name);
    m_scan_histo_name = threshold_names[0]; // For analysis with only one scan, scan name doesn't matter

    std::cout << "AutomaticInLinkOutLinkanalysis:   constructed." << std::endl;

    //initialization
    global_ROD_name ="";
  }


  void AutomaticInLinkOutLinkanalysis::requestScanConfigs(IAnalysisDataServer *ana_data_server) 
  {
    ana_data_server->getDataServer()->requestScanConfig();
  }

  void AutomaticInLinkOutLinkanalysis::requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name) 
  {
    ana_data_server->getDataServer()->requestModuleConfig(module_name);
    std::cout<<"AutomaticInlinkOutLinkanalysis:: requesting configuration for module "<<module_name<<std::endl;
  }

  void AutomaticInLinkOutLinkanalysis::getBasics(IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter) 
  {

    m_newInput->rodName = m_ROD_name;
    m_newInput->pp0Name = m_pp0_name;
    // m_newInput->moduleID   = getModuleId  ( ana_data_server, module_iter, pos);  
    m_newInput->moduleName = const_cast<PixLib::ModuleConnectivity*>(*module_iter)->name();
    m_newInput->moduleId   = const_cast<PixLib::ModuleConnectivity*>(*module_iter)->modId;
    m_newInput->inLink     = const_cast<PixLib::ModuleConnectivity*>(*module_iter)->inLink();


  
    //Note that we use 40MHz to do In/OutLink scan
    m_newInput->outLink1A  = const_cast<PixLib::ModuleConnectivity*>(*module_iter)->outLink1A(PixLib::PixScan::SINGLE_40);
    m_newInput->outLink2A  = const_cast<PixLib::ModuleConnectivity*>(*module_iter)->outLink2A(PixLib::PixScan::SINGLE_40);

    PixLib::Pp0Connectivity *pp0= const_cast<PixLib::ModuleConnectivity*>(*module_iter)->pp0();
    std::string OBType="UnKnown";
    if(pp0 && pp0->ob()){
      OBType= pp0->ob()->linkMapSName();
    }

    cout <<" INFO AutomaticInLinkOutLinkanalysis::getBasics " 
         <<" name "<< m_newInput->moduleName 
         <<" modID " << m_newInput->moduleId
         <<" InLink "<< m_newInput->inLink 
         <<" OutLink1A "<< m_newInput->outLink1A 
         <<" 2A "<< m_newInput->outLink2A
         <<" 1B "<< const_cast<PixLib::ModuleConnectivity*>(*module_iter)->outLink1B(PixLib::PixScan::SINGLE_40) 
         <<" 2B "<< const_cast<PixLib::ModuleConnectivity*>(*module_iter)->outLink2B(PixLib::PixScan::SINGLE_40)
         <<" bocTx " << const_cast<PixLib::ModuleConnectivity*>(*module_iter)->bocLinkTx()
         <<" bocRxDTO " << const_cast<PixLib::ModuleConnectivity*>(*module_iter)->bocLinkRx(2)
         <<" bocRxDTO2 "<< const_cast<PixLib::ModuleConnectivity*>(*module_iter)->bocLinkRx(1) 
         <<" " << OBType    
         <<endl;

  }


  void AutomaticInLinkOutLinkanalysis::getScanConfigs(PixLib::Config&   scan_config) 
  {
    //cout << "AutomaticInLinkOutLinkanalysis::getScanConfigs   at start." << endl;
    string speed = confVal<std::string>(scan_config["mcc"]["mccBandwidth"]);
    float zaehler = confVal<int>(scan_config["general"]["maskStageSteps"]); 
    float nenner = converter.getTotalMaskStageSteps(confVal<std::string>(scan_config["general"]["maskStageTotalSteps"])); 
    if(nenner!=0) m_newInput->nPixel = 92160*int(zaehler/nenner);
    //std::cout << "!!!nPixel " << 92160*zaehler/nenner << "(" << zaehler << "/" << nenner << ")" << std::endl; 
    
    if(speed=="SINGLE_40" || speed=="DOUBLE_40") m_newInput->MBs=40;
    else if(speed=="SINGLE_80" || speed=="DOUBLE_80") m_newInput->MBs=80;
    else m_newInput->MBs=-1; //should not happen
    
    //m_newInput->axis = *converter.GetTheAxis(const_cast< PixLib::Config &>(*scan_config)); //do we need this?
    //cout << "AutomaticInLinkOutLinkanalysis::getScanConfigs   done." << endl;
  }

 
  void AutomaticInLinkOutLinkanalysis::getModuleConfigs(PixLib::Config& module_config) 
  {
  }


  void AutomaticInLinkOutLinkanalysis::getHistograms(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter) 
  {
    cout << "INFO AutomaticInLinkOutLinkanalysis:: hist: " << m_histo_type_name
         << " converter.GetTheHisto2D " <<endl;


    global_ROD_name = m_ROD_name;
    string conn_name = m_ROD_name;
    IDataServer* data_server = ana_data_server->getDataServer();
 
    // PixelHistoServer.pixel_provider_ReloadOH./S000004514/ROD_C0_S11/FMTC_LINKMAP/A0/B0/C0/0:LNKMAP_0_0_0 <2/6/08 03:18:38.399832> <HistogramData<double>> 
    const TH2* h_InLinkOutLink =  converter.GetTheHisto2D( m_histo_type_name, data_server, conn_name, 0,0,0,0);
    if(h_InLinkOutLink){
      global_histo_InLinkOutLink = h_InLinkOutLink;
      m_newInput->histo_InLinkOutLink  = h_InLinkOutLink;

    }else{
      global_histo_InLinkOutLink = NULL;
      m_newInput->histo_InLinkOutLink  = NULL;
      cout <<"ERROR AutomaticInLinkOutLinkanalysis::getHistograms: Can't find histogram: " << m_histo_type_name 
           <<" with conn_name: " << conn_name
           <<" for module: "<<PixA::connectivityName( module_iter )
           <<endl;
    }
    
  }


  void AutomaticInLinkOutLinkanalysis::saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result2) 
  {
    ResultInLinkOutLink* module_result = static_cast<ResultInLinkOutLink*>(module_result2);
    unique_ptr<ROOTanalysisHelper> helper(new ROOTanalysisHelper());
    //
    analysis_results.addValue<float>("RX_IPin_Residual",module_result->moduleName.Data(),module_result->drx_res);
    analysis_results.addValue<float>("hit bits ratio 1A",module_result->moduleName.Data(),module_result->ratiobits1);
    analysis_results.addValue<float>("hit bits ratio 2A",module_result->moduleName.Data(),module_result->ratiobits2);
    analysis_results.addValue<bool> ("noisy 1A"         ,module_result->moduleName.Data(),module_result->noisy1);
    analysis_results.addValue<bool> ("noisy 2A"         ,module_result->moduleName.Data(),module_result->noisy2);
    //
  }
}
