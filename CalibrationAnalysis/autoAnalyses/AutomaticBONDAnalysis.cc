#include "AutomaticBONDAnalysis.hh"
#include "PixFe/PixGeometry.h"
#include "DataContainer/GenericHistogramAccess.h"

namespace CAN {

  AutomaticBONDAnalysis::AutomaticBONDAnalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &oi)  
    : AutomaticAnalysisBase( analysis_serial_number, oi) 
  {
    m_name="BONDanalysis";
    m_config=PixLib::Config("BONDanalysis");
    m_analysis = unique_ptr<BONDanalysisROOT>(new BONDanalysisROOT);

    m_config.addGroup("general");
    PixLib::ConfGroup &group=m_config["general"];
    group.addInt("cut_delta_min", (static_cast<BONDanalysisROOT*>(m_analysis.get()))->cut_delta_min, 50, "delta_min", true);
    //
    group.addString("ScanName1", m_scanNameHVon, "Threshold_HVon", "scanName1", true);
    group.addString("ScanName2", m_scanNameHVoff,"Threshold_HVoff","scanName2", true);

    m_histo_type = "SCURVE_SIGMA";
    vector<string> histo_names;
    histo_names.push_back(m_histo_type); 
    m_scan_histo_names[m_scanNameHVon]  = histo_names; 
    m_scan_histo_names[m_scanNameHVoff] = histo_names; 
  }


  void AutomaticBONDAnalysis::requestScanConfigs(IAnalysisDataServer *ana_data_server) 
  {
    ana_data_server->getDataServer()->requestScanConfig();
  }

  void AutomaticBONDAnalysis::requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name) 
  {
  }

  void AutomaticBONDAnalysis::getBasics(IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos) 
  {
    m_newInput->pp0Name = m_pp0_name;
    m_newInput->moduleName = getModuleName( ana_data_server, module_iter, pos); 
  }
 

  void AutomaticBONDAnalysis::getScanConfigs(PixLib::Config&   scan_config) 
  {
    //float zaehler = confVal<int>(scan_config["general"]["maskStageSteps"]); 
    //float nenner = converter.getTotalMaskStageSteps(confVal<std::string>(scan_config["general"]["maskStageTotalSteps"])); 
  //  if(nenner!=0) m_newInput->nPixel = 92160*int(zaehler/nenner); [maria elena] commented it out
      
  }

 

  void AutomaticBONDAnalysis::getHistograms(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos) 
  {
    const TH2 *h_sigma_HVon, *h_sigma_HVoff;  
    if(m_CTdata) {
      DataServerCT * dataServerCT = dynamic_cast<DataServerCT*>( ana_data_server->getDataServer(m_scanNameHVon) );
      h_sigma_HVon = converter.GetTheHisto2D_old( *iterator(dataServerCT->getHistograms( m_histo_type, m_pp0_name).begin(),pos),  
						  *iterator(dataServerCT->getScanConfigRefs( m_histo_type, m_pp0_name).begin(),pos),  m_histo_type, -1,0,0,0);
      dataServerCT = dynamic_cast<DataServerCT*>( ana_data_server->getDataServer(m_scanNameHVoff) );
      h_sigma_HVoff = converter.GetTheHisto2D_old( *iterator(dataServerCT->getHistograms( m_histo_type, m_pp0_name).begin(),pos),  
						   *iterator(dataServerCT->getScanConfigRefs( m_histo_type, m_pp0_name).begin(),pos),  m_histo_type, -1,0,0,0);
    }
    else {
      string conn_name = PixA::connectivityName( module_iter );  
      h_sigma_HVon = converter.GetTheHisto2D( m_histo_type, ana_data_server->getDataServer(m_scanNameHVon) , conn_name, -1,0,0,0);
      h_sigma_HVoff= converter.GetTheHisto2D( m_histo_type, ana_data_server->getDataServer(m_scanNameHVoff), conn_name, -1,0,0,0);
    }
    if(h_sigma_HVon !=NULL) m_newInput->histo_sigma_HVon = h_sigma_HVon;
    if(h_sigma_HVoff!=NULL) m_newInput->histo_sigma_HVoff= h_sigma_HVoff;
      
      PixGeometry geo(PixA::nRows(h_sigma_HVon),PixA::nColumns(h_sigma_HVon));
      m_feflv = geo.pixType();
      std::cout << "FE flavor: " << m_feflv << std::endl;

  }

    void AutomaticBONDAnalysis::getModuleConfigs(PixLib::Config& module_config)
    {
        std::cout << "getModuleConfigs start" << std::endl;
        getDisabledFECP (module_config, m_feflv , m_newInput->fe_disable, m_newInput->cp_enable);
        std::cout << "getModuleConfigs end" << std::endl;

    }

  void AutomaticBONDAnalysis::saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result2) 
  {
    ResultBOND* module_result = static_cast<ResultBOND*>(module_result2);
    analysis_results.addValue<unsigned int>("number of bad pixels",module_result->moduleName.Data(),module_result->numBadPixels);
  }

}



