#include "AutomaticTOTanalysis.hh"
//#include "ConfigWrapper/ConnectivityUtil.h"
//#include "DataContainer/HistInfo_t.h"


namespace CAN {

  AutomaticTOTanalysis::AutomaticTOTanalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &oi)  
    : AutomaticAnalysisBase( analysis_serial_number, oi) 
  {
    m_name="TOTanalysis";
    m_config=PixLib::Config("TOTanalysis");
    m_analysis = unique_ptr<TOTanalysisROOT>(new TOTanalysisROOT);
    
    m_config.addGroup("general");
    PixLib::ConfGroup &group=m_config["general"];

    group.addFloat("min_tot_fit_closure",(static_cast<TOTanalysisROOT*>(m_analysis.get()))->min_tot_fit_closure, -5000., "min difference allowed between predicted charge and injected charge", true);
    group.addFloat("max_tot_fit_closure",(static_cast<TOTanalysisROOT*>(m_analysis.get()))->max_tot_fit_closure, 5000., "max difference allowed between predicted charge and injected charge", true);
    group.addFloat("max_frac_bad_tot_fit_closure",(static_cast<TOTanalysisROOT*>(m_analysis.get()))->max_frac_bad_tot_fit_closure, 0.5, "max fraction of pixels (times vcal steps) failing the predicted charge - injected charge test (in %)", true);
    group.addFloat("min_corr",(static_cast<TOTanalysisROOT*>(m_analysis.get()))->min_corr, 99.5, "min charge correlation allowed (in %)", true);
    group.addFloat("min_disp_fit_closure",(static_cast<TOTanalysisROOT*>(m_analysis.get()))->min_disp_fit_closure, -0.5, "min difference allowed between calculated and fitted", true);
    group.addFloat("max_disp_fit_closure",(static_cast<TOTanalysisROOT*>(m_analysis.get()))->max_disp_fit_closure, 0.5, "max difference allowed between calculated and fitted", true);
    group.addFloat("max_frac_bad_disp_fit_closure",(static_cast<TOTanalysisROOT*>(m_analysis.get()))->max_frac_bad_disp_fit_closure, 20., "max fraction of vcal steps failing the fitted dispersion - calculated dispersion test (in %)", true);
    group.addFloat("max_frac_bad_pixels",(static_cast<TOTanalysisROOT*>(m_analysis.get()))->max_frac_bad_pixels, 50., "max fraction of bad pixels allowed (in %)", true);
    group.addFloat("max_n_bad_pixels",(static_cast<TOTanalysisROOT*>(m_analysis.get()))->max_n_bad_pixels, 3000., "max number of bad pixels allowed", true);
   
    group.addFloat("min_tot_mip",(static_cast<TOTanalysisROOT*>(m_analysis.get()))->min_tot_mip, 29.5, "min tot for a mip", true);
    group.addFloat("max_tot_mip",(static_cast<TOTanalysisROOT*>(m_analysis.get()))->max_tot_mip, 30.5, "max tot for a mip", true);
    group.addFloat("min_disp_mip",(static_cast<TOTanalysisROOT*>(m_analysis.get()))->min_disp_mip, 0., "min disp for a mip", true);
    group.addFloat("max_disp_mip",(static_cast<TOTanalysisROOT*>(m_analysis.get()))->max_disp_mip, 1., "max disp for a mip", true);
   
    group.addFloat("pred_mip",(static_cast<TOTanalysisROOT*>(m_analysis.get()))->pred_mip, 20000., "predicted electrons from a mip", true);
      
      
    //group.addFloat("n_pixeltypes",(static_cast<TOTanalysisROOT*>(m_analysis.get()))->n_pixeltypes, 2, "number of pixel types", true);
    group.addFloat("n_pixeltypes",(static_cast<TOTanalysisROOT*>(m_analysis.get()))->n_pixeltypes, 1, "number of pixel types", true);
    group.addBool("fit_each_pixel",(static_cast<TOTanalysisROOT*>(m_analysis.get()))->fit_each_pixel,false, "fit each pixel", true);
    group.addString("tot_fitfunc",(static_cast<TOTanalysisROOT*>(m_analysis.get()))->tot_fitfunc,"TOTFunc","fit function", true);
    group.addBool("dump_data",(static_cast<TOTanalysisROOT*>(m_analysis.get()))->dump_data,false, "dump data", true);
    group.addBool("debug",(static_cast<TOTanalysisROOT*>(m_analysis.get()))->debug,false, "debug output", true);
    group.addBool("fit_disp_vs_charge",(static_cast<TOTanalysisROOT*>(m_analysis.get()))->fit_disp_vs_charge,false, "fit dispersion vs charge (not <ToT>)", true);

    vector<string> tot_names;
    m_histo_type_mean = "TOT_MEAN"; //for TOT scans
    m_histo_type_sigma = "TOT_SIGMA"; //for TOT scans
    tot_names.push_back(m_histo_type_mean);  tot_names.push_back(m_histo_type_sigma);
    m_scan_histo_names[""] = tot_names; // For analysis with only one scan, scan name doesn't matter

    std::cout << "AutomaticTOTanalysis:   constructed." << std::endl;
  }


  void AutomaticTOTanalysis::requestScanConfigs(IAnalysisDataServer *ana_data_server) 
  {
    ana_data_server->getDataServer()->requestScanConfig();
  }

  void AutomaticTOTanalysis::requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name) 
  {
    ana_data_server->getDataServer()->requestModuleConfig(module_name);
  }

  void AutomaticTOTanalysis::getBasics(IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos) 
  {
    std::cout << "getBasics" << std::endl;
    m_newInput->pp0Name = m_pp0_name;
    m_newInput->moduleName = getModuleName( ana_data_server, module_iter, pos);
  }


  void AutomaticTOTanalysis::getScanConfigs(PixLib::Config& scan_config) 
  {
    std::cout << "getScanConfigs" << std::endl;
    m_vcal_array.clear();
    int Nevents = confVal<int>(scan_config["general"]["repetitions"]);
    int NmaskStages = confVal<int>(scan_config["general"]["maskStageSteps"]);
    maskStagesMode = confVal<string>(scan_config["general"]["maskStageMode"]);
    float NTotMaskStages = converter.getTotalMaskStageSteps(confVal<std::string>(const_cast< PixLib::Config &>(scan_config)["general"]["maskStageTotalSteps"]));
    std::string loop_var = confVal<string>(scan_config["loops"]["paramLoop_0"]);
    m_NvcalSteps = confVal<int>(scan_config["loops"]["loopVarNStepsLoop_0"]);
    m_chargeInjCapHigh = confVal<bool>(scan_config["fe"]["chargeInjCapHigh"]);

    bool uniform = confVal<bool>(scan_config["loops"]["loopVarUniformLoop_0"]);
    
    if (uniform)  {
      m_NvcalSteps = confVal<int>(scan_config["loops"]["loopVarNStepsLoop_0"]);

      //m_NvcalSteps = 6;
      float min_val = confVal<float>(scan_config["loops"]["loopVarMinLoop_0"]);
      float max_val = confVal<float>(scan_config["loops"]["loopVarMaxLoop_0"]);
      float bin_width = max_val-min_val;
      if (m_NvcalSteps > 1) {
	bin_width /= (m_NvcalSteps-1);
      }
      else {
	if (min_val==max_val) bin_width=0.5 ;
      }
      float vcal = min_val;
      for (int step_i=0; step_i!=m_NvcalSteps; ++step_i) {
	m_vcal_array.push_back(vcal);
	vcal += bin_width;
      }
    }
    else {
      m_vcal_array = confVal<std::vector<float> >(scan_config["loops"][std::string("loopVarValuesLoop_0")]);
    }

    ROOTanalysisHelper helper;
    int Nfe = helper.getNfrontEnd(m_feflv); // # of FEs in one module
    
    m_newInput->charge.clear();
    if (loop_var == "CHARGE") { // looping over charge, so vcal array actually contains charge
      //for (int fe=0; fe!=16; ++fe) {
      for (int fe=0; fe!=Nfe; ++fe) {
	std::vector<float> dummy(0);
	m_newInput->charge.push_back(dummy);
	m_newInput->charge[fe] = m_vcal_array;
      }
    }
    
     
    //     m_vcal_array[0] = 150;
    //     m_vcal_array[1] = 310;
    //     m_vcal_array[2] = 470;
    //     m_vcal_array[3] = 630;
    //     m_vcal_array[4] = 790;
    //     m_vcal_array[5] = 950;
    m_newInput->n_maskstages = NmaskStages;
    m_newInput->n_events = Nevents;
    m_newInput->n_vcalsteps = m_NvcalSteps;
    m_newInput->nTotMaskStages= (int) NTotMaskStages;
  }

 /* void AutomaticTOTanalysis::getModuleConfigs(const std::string module) {
    std::cout << "getModuleConfigs" << std::endl;
    if (m_newInput->charge.size() != 0) return; // I am looping over charge and the array is already filled
    
    std::map<std::string,std::map<std::string,float> > modcfg;
    ifstream ss_modcfg;
    int Nmodcfg = 0;
    std::string str_module;
    std::string str_fe;
    std::string str_CInjLo;
    float float_CInjLo;
    std::string str_CInjHi;
    float float_CInjHi;
    std::string str_VcalGradient0;
    float float_VcalGradient0;
    std::string str_VcalGradient1;
    float float_VcalGradient1;
    std::string str_VcalGradient2;
    float float_VcalGradient2;
    std::string str_VcalGradient3;
    float float_VcalGradient3;
    ss_modcfg.open("/home/sara/modcfg/FeCfgPit.txt");
    while (ss_modcfg >> str_module >> str_fe >>
	   str_CInjLo >> float_CInjLo >>
	   str_CInjHi >> float_CInjHi >>
	   str_VcalGradient0 >> float_VcalGradient0 >>
	   str_VcalGradient1 >> float_VcalGradient1 >>
	   str_VcalGradient2 >> float_VcalGradient2 >>
	   str_VcalGradient3 >> float_VcalGradient3) {
      ++Nmodcfg;
      std::string mod_fe = str_module+"_"+str_fe;
      std::map<std::string,float> dummy;
      dummy.insert(make_pair(str_CInjLo,float_CInjLo));
      dummy.insert(make_pair(str_CInjHi,float_CInjHi));
      dummy.insert(make_pair(str_VcalGradient0,float_VcalGradient0));
      dummy.insert(make_pair(str_VcalGradient1,float_VcalGradient1));
      dummy.insert(make_pair(str_VcalGradient2,float_VcalGradient2));
      dummy.insert(make_pair(str_VcalGradient3,float_VcalGradient3));
      modcfg.insert(make_pair(mod_fe,dummy));
      //std::cout << str_CInjLo << " " << float_CInjLo << std::endl;
    }
    ss_modcfg.close();
    
    std::map<std::string,std::string> modid;
    ifstream ss_modid;
    int Nmodid = 0;
    std::string str_modname;
    std::string str_modid;
    ss_modid.open("/home/sara/daq/Applications/Pixel/AnalysisFramework/data/sethNameDict.txt");
    while (ss_modid >> str_modname >> str_modid) {
      ++Nmodid;
      modid.insert(make_pair(str_modname,str_modid));
      //std::cout << str_modname << " " << str_modid << std::endl;
    }

    std::string stripped_module = module;
    if (module.find("M0") == string::npos && 
	(module.find("L0") != string::npos || module.find("L1") != string::npos || module.find("L2") != string::npos)) stripped_module.erase(stripped_module.end()-1,stripped_module.end());
    //std::cout << stripped_module << std::endl;

    ROOTanalysisHelper helper;
    int Nfe = helper.getNfrontEnd(m_feflv); // # of FEs in one module

    std::vector<std::vector<float> > charge_array;
    //for (int fe=0; fe!=16; ++fe) {
    for (int fe=0; fe!=Nfe; ++fe) {
      std::vector<float> dummy(0);
      charge_array.push_back(dummy);
      char modfe_str[500];
      sprintf(modfe_str,"M%s_PixFe_%i",modid[stripped_module].c_str(),fe);
      std::string modfe = std::string(modfe_str);
      std::string cname = "CInjLo";
      if (m_chargeInjCapHigh) cname = "CInjHi";
      float vcal_a = modcfg[modfe]["VcalGradient3"];
      float vcal_b = modcfg[modfe]["VcalGradient2"];
      float vcal_c = modcfg[modfe]["VcalGradient1"];
      float vcal_d = modcfg[modfe]["VcalGradient0"];
        float cInj;
        if(m_feflv == PixGeometry::FEI2_CHIP || m_feflv == PixGeometry::FEI2_MODULE){cInj = modcfg[modfe][cname];}
        else if(m_feflv == PixGeometry::FEI4_CHIP || m_feflv == PixGeometry::FEI4_MODULE){
            if (maskStagesMode=="FEI4_ENA_SCAP") {
                    cInj=1.9;
            }
            else if(maskStagesMode=="FEI4_ENA_LCAP"){
                    cInj=3.9;
            }
            else if(maskStagesMode=="FEI4_ENA_BCAP"){
                    cInj=5.7;
            }
            }

      for (int step=0; step!=m_NvcalSteps; ++step) {
	float v = m_vcal_array[step];
	float q = (vcal_a*v*v*v + vcal_b*v*v + vcal_c*v + vcal_d)*cInj/0.160218;
	//std::cout << "vcal " << v << " charge " << q << std::endl;
	charge_array[fe].push_back(q);
      }
    }
    m_newInput->charge = charge_array;
    
  }*/
  
  void AutomaticTOTanalysis::getModuleConfigs(PixLib::Config& module_config) 
  {
      std::cout << "getModuleConfigs not by hand" << std::endl;
   // if (m_newInput->charge.size() != 0) return; // I am looping over charge and the array is already filled
 
    PixLib::Config &casted_a_config=const_cast<PixLib::Config &>(module_config);
    if (casted_a_config.size()<=0 && casted_a_config.subConfigSize()<=0) return;
 
    //getModuleConfigs();

    std::vector<std::vector<float> > charge_array;
    
    ROOTanalysisHelper helper;
    
    getDisabledFECP (module_config, m_feflv , m_newInput->fe_disable, m_newInput->cp_enable);
    
    int Nfe = helper.getNfrontEnd(m_feflv); // # of FEs in one module
   
    //for (int fe=0; fe!=16; ++fe) {
    for (int fe=0; fe!=Nfe; ++fe) {
 
      std::vector<float> dummy(0);
      charge_array.push_back(dummy);
      std::string fe_name = "PixFe_" + std::to_string(fe);
      PixLib::Config& fe_config = module_config.subConfig(fe_name);
        if(fe_config.size() <= 0 && fe_config.subConfigSize() <= 0) continue;
      std::string cname = "CInjLo";
      if (m_chargeInjCapHigh) cname = "CInjHi";
        //**********************
        float cInj = 0.;
        if(m_feflv == PixGeometry::FEI2_CHIP || m_feflv == PixGeometry::FEI2_MODULE){cInj = ((PixLib::ConfFloat&)fe_config["Misc"][cname]).value();}
        else if(m_feflv == PixGeometry::FEI4_CHIP || m_feflv == PixGeometry::FEI4_MODULE){
            if (maskStagesMode=="FEI4_ENA_SCAP") {
                
                    cInj=((PixLib::ConfFloat&)fe_config["Misc"]["CInjLo"]).value();
            }
            else if (maskStagesMode=="FEI4_ENA_LCAP"){
                    cInj=((PixLib::ConfFloat&)fe_config["Misc"]["CInjMed"]).value();
            }
            else  if (maskStagesMode=="FEI4_ENA_BCAP"){
                    cInj=((PixLib::ConfFloat&)fe_config["Misc"]["CInjHi"]).value();
            }
}
      //************************
      float vcal_a = ((PixLib::ConfFloat&)fe_config["Misc"]["VcalGradient3"]).value();
      float vcal_b = ((PixLib::ConfFloat&)fe_config["Misc"]["VcalGradient2"]).value();
      float vcal_c = ((PixLib::ConfFloat&)fe_config["Misc"]["VcalGradient1"]).value();
      float vcal_d = ((PixLib::ConfFloat&)fe_config["Misc"]["VcalGradient0"]).value();
      std::cout << "Cap cInj " << cInj << " for fe number " << fe << std::endl;
      for (int step=0; step!=m_NvcalSteps; ++step) {
	float v = m_vcal_array[step];
	float q = (vcal_a*v*v*v + vcal_b*v*v + vcal_c*v + vcal_d)*cInj/0.160218;
	charge_array[fe].push_back(q);
      }       
    }

  if (m_newInput->charge.empty())m_newInput->charge = charge_array;
 
  }
  
  void AutomaticTOTanalysis::getHistograms(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos)
  {
      
      if(m_CTdata) {
          DataServerCT * dataServerCT = dynamic_cast<DataServerCT*>( ana_data_server->getDataServer() );
          PixA::ConfigRef scan_config_ref = (*iterator(dataServerCT->getScanConfigRefs( *m_scan_histo_names.begin()->second.begin(), m_pp0_name).begin(),pos))->pixScanConfig();
          PixLib::Config& scan_config =const_cast<PixLib::Config &>(scan_config_ref.config());
          m_NvcalSteps = confVal<int>(scan_config["loops"]["loopVarNStepsLoop_0"]);
      }
      else {
          PixLib::Config& scan_config = const_cast<PixLib::Config&>(ana_data_server->getDataServer()->getScanConfig());
          m_NvcalSteps = confVal<int>(scan_config["loops"]["loopVarNStepsLoop_0"]);
       }

      
    //if (m_newInput->charge.size() == 0) getModuleConfigs(PixA::connectivityName(module_iter));

    for (int step=0; step!=m_NvcalSteps; ++step) {
      const TH2 *h_mean, *h_sigma;  
      if(m_CTdata) {
 	DataServerCT * dataServerCT = dynamic_cast<DataServerCT*>( ana_data_server->getDataServer() );
 	h_mean  = converter.GetTheHisto2D_old( *iterator(dataServerCT->getHistograms( m_histo_type_mean, m_pp0_name).begin(),pos),  
 					       *iterator(dataServerCT->getScanConfigRefs( m_histo_type_mean, m_pp0_name).begin(),pos),  m_histo_type_mean, 0,0,step,0);
 	h_sigma = converter.GetTheHisto2D_old( *iterator(dataServerCT->getHistograms( m_histo_type_sigma, m_pp0_name).begin(),pos),  
 					       *iterator(dataServerCT->getScanConfigRefs( m_histo_type_sigma, m_pp0_name).begin(),pos),  m_histo_type_sigma, 0,0,step,0);
      } else {
	string conn_name = PixA::connectivityName( module_iter );  
	IDataServer* data_server = ana_data_server->getDataServer();
	h_mean  = converter.GetTheHisto2D( m_histo_type_mean, data_server, conn_name, 0,0,step,0);
	h_sigma = converter.GetTheHisto2D( m_histo_type_sigma,data_server, conn_name, 0,0,step,0);
	//cout << "Fetching histograms for module " << conn_name << endl;
	//std::cout << "h_mean " << h_mean << " h_sigma " << h_sigma << std::endl;
      }
      if(h_mean!=NULL) {
	m_newInput->histo_mean.push_back(h_mean);
      }
      if(h_sigma!=NULL) { 
	m_newInput->histo_sigma.push_back(h_sigma);
      }
      if(h_mean!=NULL && m_feflv == -1){
	PixGeometry geo(PixA::nRows(h_mean),PixA::nColumns(h_mean)); 
	m_feflv = geo.pixType();
	std::cout << "FE flavor: " << m_feflv << std::endl;
      }
    }
    
  }

  void AutomaticTOTanalysis::saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result2) 
  {
    ResultTOT* module_result = static_cast<ResultTOT*>(module_result2);
    unique_ptr<ROOTanalysisHelper> helper(new ROOTanalysisHelper());
    ExtendedAnalysisResultList_t& extended_results = (ExtendedAnalysisResultList_t&)analysis_results;

    int Npars_tot = (static_cast<TOTanalysisROOT*>(m_analysis.get()))->map_tot_fitfuncs[(static_cast<TOTanalysisROOT*>(m_analysis.get()))->tot_fitfunc];
    int Npars_disp = 2;

    char tempstr[50];
    
    std::cout << "**********************************" << std::endl;
    std::cout << "**********************************" << std::endl;
    std::cout << "Results for module " << module_result->moduleName.Data() << ":" << std::endl;
   
    strcpy(tempstr,"passed");
    extended_results.addValue<bool>(tempstr,module_result->moduleName.Data(),module_result->passed);
    std::cout << tempstr << ": " << module_result->passed << std::endl;
    strcpy(tempstr,"good_pixels");
    extended_results.addValue<bool>(tempstr,module_result->moduleName.Data(),module_result->good_pixels);
    std::cout << tempstr << ": " << module_result->good_pixels << std::endl;
    strcpy(tempstr,"good_closure");
    extended_results.addValue<bool>(tempstr,module_result->moduleName.Data(),module_result->good_closure);
    std::cout << tempstr << ": " << module_result->good_closure << std::endl;
    strcpy(tempstr,"good_tuning");
    extended_results.addValue<bool>(tempstr,module_result->moduleName.Data(),module_result->good_tuning);
    std::cout << tempstr << ": " << module_result->good_tuning << std::endl;
    strcpy(tempstr,"tot_mip");
    extended_results.addValue<float>(tempstr,module_result->moduleName.Data(),module_result->tot_mip);
    std::cout << tempstr << ": " << module_result->tot_mip << std::endl;
    strcpy(tempstr,"disp_mip");
    extended_results.addValue<float>(tempstr,module_result->moduleName.Data(),module_result->disp_mip);
    std::cout << tempstr << ": " << module_result->disp_mip << std::endl;
    strcpy(tempstr,"n_passed_fe_pixtype");
    extended_results.addValue<float>(tempstr,module_result->moduleName.Data(),module_result->n_passed_fe_pixtype);
    std::cout << tempstr << ": " << module_result->n_passed_fe_pixtype << std::endl;
    strcpy(tempstr,"frac_bad_tot_fit_closure");
    extended_results.addValue<float>(tempstr,module_result->moduleName.Data(),module_result->frac_bad_tot_fit_closure);
    strcpy(tempstr,"frac_bad_disp_fit_closure");
    extended_results.addValue<float>(tempstr,module_result->moduleName.Data(),module_result->frac_bad_disp_fit_closure);
    strcpy(tempstr,"charge_corr");
    extended_results.addValue<float>(tempstr,module_result->moduleName.Data(),module_result->charge_corr);
    for (int fe=0; fe!=helper->getNfrontEnd(m_feflv); ++fe) {
      for (int pixtype=0; pixtype<int((static_cast<TOTanalysisROOT*>(m_analysis.get()))->n_pixeltypes); pixtype++) {
	std::cout << "**********************************" << std::endl;
	std::string pixtypeStr;
	if(m_feflv == PixGeometry::FEI2_CHIP || m_feflv == PixGeometry::FEI2_MODULE){
	  if (int((static_cast<TOTanalysisROOT*>(m_analysis.get()))->n_pixeltypes) == 6) pixtypeStr = helper->getPixelTypeString(pixtype);
	  else if (int((static_cast<TOTanalysisROOT*>(m_analysis.get()))->n_pixeltypes) == 3) pixtypeStr = helper->getPixelClassString(pixtype+1,3);
	  else if (int((static_cast<TOTanalysisROOT*>(m_analysis.get()))->n_pixeltypes) == 2) pixtypeStr = helper->getPixelClassString(pixtype+1,2);
	}else if(m_feflv == PixGeometry::FEI4_CHIP || m_feflv == PixGeometry::FEI4_MODULE){
	  if (int((static_cast<TOTanalysisROOT*>(m_analysis.get()))->n_pixeltypes) == 1) pixtypeStr = helper->getPixelTypeString(pixtype);
	}
	std::cout << "Results for module " << module_result->moduleName.Data() << " FE " << fe << " " << pixtypeStr << "pixels : " << std::endl;

	sprintf(tempstr,"passed_FE%i_%s_pixels",fe,pixtypeStr.c_str());
	bool fepixtypepassed = (module_result->fe_pixtype_passed[fe][pixtype] != 0);
	extended_results.addValue<bool>(tempstr,module_result->moduleName.Data(),fepixtypepassed);
	std::cout << tempstr << ": " << module_result->fe_pixtype_passed[fe][pixtype] << std::endl;

	sprintf(tempstr,"closure_failed_mask_FE%i_%s_pixels",fe,pixtypeStr.c_str());
	extended_results.addValue<float>(tempstr,module_result->moduleName.Data(), (float) module_result->fe_pixtype_closure_failed_mask[fe][pixtype]); // should be changed to unsigned ints and defined in db at some point
	std::cout << tempstr << ": " << module_result->fe_pixtype_closure_failed_mask[fe][pixtype] << std::endl;

	sprintf(tempstr,"frac_bad_tot_fit_closure_FE%i_%s_pixels",fe,pixtypeStr.c_str());
	extended_results.addValue<float>(tempstr,module_result->moduleName.Data(),module_result->fe_pixtype_frac_bad_tot_fit_closure[fe][pixtype]);
	std::cout << tempstr << ": " << module_result->fe_pixtype_frac_bad_tot_fit_closure[fe][pixtype] << std::endl;

	sprintf(tempstr,"charge_corr_FE%i_%s_pixels",fe,pixtypeStr.c_str());
	extended_results.addValue<float>(tempstr,module_result->moduleName.Data(),module_result->fe_pixtype_charge_corr[fe][pixtype]);
	std::cout << tempstr << ": " << module_result->fe_pixtype_charge_corr[fe][pixtype] << std::endl;

	sprintf(tempstr,"frac_bad_disp_fit_closure_FE%i_%s_pixels",fe,pixtypeStr.c_str());
	extended_results.addValue<float>(tempstr,module_result->moduleName.Data(),module_result->fe_pixtype_frac_bad_disp_fit_closure[fe][pixtype]);
	std::cout << tempstr << ": " << module_result->fe_pixtype_frac_bad_disp_fit_closure[fe][pixtype] << std::endl;

	sprintf(tempstr,"badpixel_failed_mask_FE%i_%s_pixels",fe,pixtypeStr.c_str());
	extended_results.addValue<float>(tempstr,module_result->moduleName.Data(), (float) module_result->fe_pixtype_badpixel_failed_mask[fe][pixtype]); // should be changed to unsigned ints and defined in db at some point
	std::cout << tempstr << ": " << module_result->fe_pixtype_badpixel_failed_mask[fe][pixtype] << std::endl;

	sprintf(tempstr,"frac_bad_pixels_FE%i_%s_pixels",fe,pixtypeStr.c_str());
	extended_results.addValue<float>(tempstr,module_result->moduleName.Data(),module_result->fe_pixtype_frac_bad_pixels[fe][pixtype]);
	std::cout << tempstr << ": " << module_result->fe_pixtype_frac_bad_pixels[fe][pixtype] << std::endl;

	sprintf(tempstr,"n_bad_pixels_FE%i_%s_pixels",fe,pixtypeStr.c_str());
	extended_results.addValue<float>(tempstr,module_result->moduleName.Data(), (float) module_result->fe_pixtype_n_bad_pixels[fe][pixtype]);
	std::cout << tempstr << ": " << module_result->fe_pixtype_n_bad_pixels[fe][pixtype] << std::endl;

	for (int par=0; par!=Npars_tot; ++par) {
	  sprintf(tempstr,"tot_p%i_FE%i_%s_pixels",par,fe,pixtypeStr.c_str());
	  extended_results.addValue<float>(tempstr,module_result->moduleName.Data(),module_result->fe_pixtype_tot[par][fe][pixtype]);
	  std::cout << tempstr << ": " << module_result->fe_pixtype_tot[par][fe][pixtype] << std::endl;

	  sprintf(tempstr,"tot_p%i_err_FE%i_%s_pixels",par,fe,pixtypeStr.c_str());
	  extended_results.addValue<float>(tempstr,module_result->moduleName.Data(),module_result->fe_pixtype_tot_err[par][fe][pixtype]);
	  std::cout << tempstr << ": " << module_result->fe_pixtype_tot_err[par][fe][pixtype] << std::endl;
	}

	sprintf(tempstr,"tot_chi2_FE%i_%s_pixels",fe,pixtypeStr.c_str());
	extended_results.addValue<float>(tempstr,module_result->moduleName.Data(),module_result->fe_pixtype_tot_chi2[fe][pixtype]);
	std::cout << tempstr << ": " << module_result->fe_pixtype_tot_chi2[fe][pixtype] << std::endl;

	for (int par=0; par!=Npars_disp; ++par) {
	  sprintf(tempstr,"disp_p%i_FE%i_%s_pixels",par,fe,pixtypeStr.c_str());
	  extended_results.addValue<float>(tempstr,module_result->moduleName.Data(),module_result->fe_pixtype_disp[par][fe][pixtype]);
	  std::cout << tempstr << ": " << module_result->fe_pixtype_disp[par][fe][pixtype] << std::endl;
	  
	  sprintf(tempstr,"disp_p%i_err_FE%i_%s_pixels",par,fe,pixtypeStr.c_str());
	  extended_results.addValue<float>(tempstr,module_result->moduleName.Data(),module_result->fe_pixtype_disp_err[par][fe][pixtype]);
	  std::cout << tempstr << ": " << module_result->fe_pixtype_disp_err[par][fe][pixtype] << std::endl;
	}

	sprintf(tempstr,"disp_chi2_FE%i_%s_pixels",fe,pixtypeStr.c_str());
	extended_results.addValue<float>(tempstr,module_result->moduleName.Data(),module_result->fe_pixtype_disp_chi2[fe][pixtype]);
	std::cout << tempstr << ": " << module_result->fe_pixtype_disp_chi2[fe][pixtype] << std::endl;

	sprintf(tempstr,"tot_mip_FE%i_%s_pixels",fe,pixtypeStr.c_str());
	extended_results.addValue<float>(tempstr,module_result->moduleName.Data(),module_result->fe_pixtype_tot_mip[fe][pixtype]);
	std::cout << tempstr << ": " << module_result->fe_pixtype_tot_mip[fe][pixtype] << std::endl;

	//sprintf(tempstr,"sigma_mip_FE%i_%s_pixels",fe,pixtypeStr.c_str());
	//extended_results.addValue<float>(tempstr,module_result->moduleName.Data(),module_result->fe_pixtype_sigma_mip[fe][pixtype]);
	//std::cout << tempstr << ": " << module_result->fe_pixtype_sigma_mip[fe][pixtype] << std::endl;

	sprintf(tempstr,"disp_mip_FE%i_%s_pixels",fe,pixtypeStr.c_str());
	extended_results.addValue<float>(tempstr,module_result->moduleName.Data(),module_result->fe_pixtype_disp_mip[fe][pixtype]);
	std::cout << tempstr << ": " << module_result->fe_pixtype_disp_mip[fe][pixtype] << std::endl;

	std::cout << "**********************************" << std::endl;
	std::cout << std::endl;
      }
    }
    
    if ((static_cast<TOTanalysisROOT*>(m_analysis.get()))->fit_each_pixel) extended_results.addHisto<TH1>("h2d_tot_fit_residual_vs_charge",module_result->moduleName.Data(),std::shared_ptr<TH2>(module_result->h2d_residual_vs_charge_perpixel_tot_fit_module));
    else extended_results.addHisto<TH1>("h2d_mean_tot_fit_residual_vs_charge",module_result->moduleName.Data(),std::shared_ptr<TH2>(module_result->h2d_residual_vs_charge_perpixel_mean_tot_fit_module));
    extended_results.addHisto<TH1>("h2d_disp_fit_residual_vs_mean_tot",module_result->moduleName.Data(),std::shared_ptr<TH2>(module_result->h2d_residual_vs_mean_tot_pergroup_disp_fit_module));
    //extended_results.addHisto<TH1>("h2d_mean_tot_fit_closure",module_result->moduleName.Data(),std::shared_ptr<TH2>(module_result->h2d_closure_perpixel_mean_tot_fit_module));
    extended_results.addHisto<TH1>("h1d_mean_tot_fit_closure",module_result->moduleName.Data(),std::shared_ptr<TH1>(module_result->h1d_closure_perpixel_mean_tot_fit_module));
    //extended_results.addHisto<TH1>("h2d_disp_fit_closure",module_result->moduleName.Data(),std::shared_ptr<TH2>(module_result->h2d_closure_pergroup_disp_fit_module));
    extended_results.addHisto<TH1>("h1d_disp_fit_closure",module_result->moduleName.Data(),std::shared_ptr<TH1>(module_result->h1d_closure_pergroup_disp_fit_module));
    extended_results.addHisto<TH1>("h2d_healthy",module_result->moduleName.Data(),std::shared_ptr<TH2>(module_result->h2d_healthy));

    std::cout << "**********************************" << std::endl;
    std::cout << "**********************************" << std::endl;
    std::cout << "These were the results!" << std::endl;
   }

}
