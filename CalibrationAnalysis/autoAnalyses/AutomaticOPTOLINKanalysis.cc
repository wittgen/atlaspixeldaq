#include "AutomaticOPTOLINKanalysis.hh"
//#include "ConfigWrapper/ConnectivityUtil.h"
//#include "DataContainer/HistInfo_t.h"


namespace CAN {

  AutomaticOPTOLINKanalysis::AutomaticOPTOLINKanalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &oi)  
    : AutomaticAnalysisBase( analysis_serial_number, oi) 
  {
    m_name="OPTOLINKanalysis";
    m_config=PixLib::Config("OPTOLINKanalysis");
    m_analysis = unique_ptr<OPTOLINKanalysisROOT>(new OPTOLINKanalysisROOT);
    OPTOLINKanalysisROOT* anptr = static_cast<OPTOLINKanalysisROOT*>(m_analysis.get());
    
    m_config.addGroup("general");
    PixLib::ConfGroup &group=m_config["general"];

    // parameters for General
    group.addFloat("m_maxthr",         anptr->m_maxthr,         100, "Maximum threshold for new algorithm",          true);
    group.addFloat("m_fraqdelay",         anptr->m_fraqdelay,         0.5, "Delay starting point in the EFR as a fraction of the width of the EFR. (E.g. 0=left border;1=right border;0.5=middle)",          true);
    group.addFloat("m_fraqthr",         anptr->m_fraqthr,         0.3, "Threshold starting point in the EFR as a fraction of the hight of the EFR. (E.g. 0=bottom border;1=top border;0.5=middle)",          true);


    std::map<std::string, int> algoList;
    algoList["old algorithm"]  = OPTOLINKanalysisROOT::OLD;
    algoList["new algorithm"] = OPTOLINKanalysisROOT::NEW;
    group.addList("m_at", anptr->m_at, static_cast<int>(OPTOLINKanalysisROOT::OLD), algoList,"What algorithm to use", true);

    std::map<std::string, int> startList;
    startList["fraction of EFR"]  = OPTOLINKanalysisROOT::FRAQ;
    startList["left down"] = OPTOLINKanalysisROOT::LEFTDOWN;
    startList["right down"] = OPTOLINKanalysisROOT::RIGHTDOWN;
    startList["left up"] = OPTOLINKanalysisROOT::LEFTUP;
    startList["right up"] = OPTOLINKanalysisROOT::RIGHTUP;
    startList["max dist to ER"] = OPTOLINKanalysisROOT::COM;
    group.addList("m_sp", anptr->m_sp, static_cast<int>(OPTOLINKanalysisROOT::FRAQ), startList,"Starting point for new algorithm as a fraction or starting in one of the corners of the smallest possible rectangle around the EFR", true);

//    group.addInt("m_allowedovershoots", anptr->m_allowedovershoots, 2, "Maximum allowed overshoots", true);
    group.addFloat("m_upperdistance",         anptr->m_upperdistance,         30, "Minimum distance between highest error free thresh. and tuned thresh",         true);
    group.addFloat("m_lowerdistance",         anptr->m_lowerdistance,         20, "Minimum distance between lowest error free thresh. and tuned thresh",          true);
    group.addFloat("m_leftdistance",          anptr->m_leftdeviation,          2, "Minimum space between an error to the left hand side and tunepoint",           true);
    group.addFloat("m_rightdistance",         anptr->m_rightdeviation,        3, "Minimum space between an error to the right hand side and tunepoint",          true);
    group.addInt("m_overshootlimit",        anptr->m_overshootlimit,         2, "Minimum Overshoot bins defining an overshoot (only old algorithm)",                                 true);
    group.addInt("m_highcutoff",            anptr->m_highcutoff,            10, "Minimum width if maximum threshold is chosen (only old algorithm)",                                 true);
    group.addInt("m_tunedistance",          anptr->m_tunedistance,           7, "Offset between highest vertical error sum and tuned delay (only old algorithm)",                    true);
    group.addBool("m_forcedfail",           anptr->m_forcedfail,          true, "Force analysis to give status failed for config writing",                      true);
    

    // parameters for Common
    group.addInt("m_visetlowthreshold",     anptr->m_visetlowthreshold,    160, "(Common) Minimum average threshold before viset is flagged as low",            true);

    // todo: add variables you want to configure
    group.addInt("m_maxiter",        anptr->m_maxiter,         99999, "Maximum number of iterations per module for new algorithm.",                                 false);
    group.addBool("m_bf",           anptr->m_bf,          true, "Check points in an outward spiral around last point if fast algorithm fails",                      false);
    group.addInt("m_bfsteps",        anptr->m_bfsteps,         50, "Number of points to check after fast algorithm fails",                                 false);
    
    vector<string> boc_names;
    m_histo_type = "RAW_DATA_DIFF_2"; //for BOC scans
    boc_names.push_back(m_histo_type); 
    m_scan_histo_names[""] = boc_names; // For analysis with only one scan, scan name doesn't matter

    std::cout << "AutomaticOPTOLINKanalysis:   constructed." << std::endl;
  }


  void AutomaticOPTOLINKanalysis::requestScanConfigs(IAnalysisDataServer *ana_data_server) 
  {
    ana_data_server->getDataServer()->requestScanConfig();
    ana_data_server->getDataServer()->requestBocConfig();
  }

  void AutomaticOPTOLINKanalysis::requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name) 
  {
    ana_data_server->getDataServer()->requestModuleConfig(module_name);
  }

  void AutomaticOPTOLINKanalysis::getBasics(IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos) 
  {

    IDataServer *scan_data_server = ana_data_server->getDataServer();

    const PixLib::ModuleConnectivity *module_conn = const_cast<PixLib::ModuleConnectivity *>(*module_iter);
    assert( module_conn );

    m_newInput->pp0Name = m_pp0_name;
    m_newInput->moduleName = getModuleName( ana_data_server, module_iter, pos);
    m_newInput->m_serialNumber = this->m_serialNumber;

    if(m_feflv == PixGeometry::FEI4_MODULE)return;

    //Not clear what is used for, skipped for IBL
    int bocLink1 = static_cast<int>( const_cast<PixLib::ModuleConnectivity *>(*module_iter)->bocLinkRx(1));
    int bocLink2 = static_cast<int>( const_cast<PixLib::ModuleConnectivity *>(*module_iter)->bocLinkRx(2));
    unsigned int rx_channel_Link1 = bocLink1%10;
    unsigned int rx_plugin_Link1 = bocLink1/10;
    unsigned int rx_channel_Link2 = bocLink2%10;
    unsigned int rx_plugin_Link2 = bocLink2/10;

    std::string MBs = "";
    try {
      PixLib::Config &casted_scan_config = const_cast<PixLib::Config &>( scan_data_server->getScanConfig() );
      std::string speed = confVal<std::string>(casted_scan_config["mcc"]["mccBandwidth"]);
      if(speed=="SINGLE_40" || speed=="DOUBLE_40") MBs = "_40";
      else if(speed=="SINGLE_80" || speed=="DOUBLE_80") MBs = "_80";
    } catch(...) {
    }
    
    m_newInput->del_Link1 = -1;
    m_newInput->del_Link2 = -1;
    m_newInput->thr_Link1 = -1;
    m_newInput->thr_Link2 = -1;
    
    if ((rx_plugin_Link1 >= 0) && (rx_plugin_Link1 < 4) && (rx_channel_Link1 >= 0) && (rx_channel_Link1 < 8)) {
      try {

	std::stringstream config_name;
	config_name << "PixRx" << rx_plugin_Link1;

	PixLib::Config &casted_boc_config = const_cast<PixLib::Config &>( scan_data_server->getBocConfig() );
	PixLib::Config &rx_config( casted_boc_config.subConfig(config_name.str()));

	{
	  std::stringstream var_name;
	  var_name << "DataDelayF" << MBs << ((MBs == "") ? "" : "_") << rx_channel_Link1+2;
	  m_newInput->del_Link1 = confVal<float>(rx_config["General"][var_name.str()]);
	  std::cout << "JDDBG: RxDelay Link1 of " << m_newInput->moduleName << ": " << m_newInput->del_Link1 << std::endl;
	}

	{
	  std::stringstream var_name;
	  var_name << "RxThresholdF" << MBs << ((MBs == "") ? "" : "_") << rx_channel_Link1+2;
	  m_newInput->thr_Link1 = confVal<float>(rx_config["Opt"][var_name.str()]);
	  std::cout << "JDDBG: RxThreshold Link1 of " << m_newInput->moduleName << ": " << m_newInput->thr_Link1 << std::endl;
	}

	{
	  // HERE WE ASSUME THAT THE RECENT Viset in config is the one used during a 2D scan
	  std::stringstream var_name;
	  var_name << "Viset" << MBs;
	  m_newInput->viset_Link1 = confVal<float>(rx_config["Opt"][var_name.str()]);
	  std::cout << "JDDBG: RxThreshold Link1 of " << m_newInput->moduleName << ": " << m_newInput->thr_Link1 << std::endl;
	}
      } catch (CAN::MRSException &err) {
  //      MRSStream out((mrsPartition.get() ? *(mrsPartition.get()) : *(ipcPartition.get())));
  //      err.sendMessage(out, std::string(argv[0])+":main" );
	std::cerr << "ERROR [AutomaticOPTOLINKanalysis:getBasics] Caught exception " << err.getDescriptor() << std::endl;
      } catch (...) {
	std::cout << "JDDBG: Didn't expect it, but we got a random exception - hope this didn't harm the analysis :-/" << std::endl;
      }
    } else {
      std::cout << "Config for Link1 seems to be missing, skipping -> Setting Link1 Thr to 0 and Delay to inappropiate Value" << std::endl;
      m_newInput->thr_Link1 = 0;
      m_newInput->del_Link1 = -1;
      m_newInput->viset_Link1 = -1;
    }

    if ((rx_plugin_Link2 >= 0) && (rx_plugin_Link2 < 4) && (rx_channel_Link2 >= 0) && (rx_channel_Link2 < 8)) {
      try {

	std::stringstream config_name;
	config_name << "PixRx" << rx_plugin_Link2;

	PixLib::Config &casted_boc_config = const_cast<PixLib::Config &>( scan_data_server->getBocConfig() );
	PixLib::Config &rx_config( casted_boc_config.subConfig(config_name.str()));

	{
	  std::stringstream var_name;
	  var_name << "DataDelay" << MBs << ((MBs == "") ? "" : "_") << rx_channel_Link2+2;
	  m_newInput->del_Link2 = confVal<int>(rx_config["General"][var_name.str()]);
	  std::cout << "JDDBG: RxDelay Link2 of " << m_newInput->moduleName << ": " << m_newInput->del_Link2 << std::endl;
	}

	{
	  std::stringstream var_name;
	  var_name << "RxThreshold" << MBs << ((MBs == "") ? "" : "_") << rx_channel_Link2+2;
	  m_newInput->thr_Link2 = confVal<int>(rx_config["Opt"][var_name.str()]);
	  std::cout << "JDDBG: RxThreshold Link2 of " << m_newInput->moduleName << ": " << m_newInput->thr_Link2 << std::endl;
	}

	{
	  std::stringstream var_name;
	  var_name << "Viset" << MBs;
	  m_newInput->viset_Link2 = confVal<float>(rx_config["Opt"][var_name.str()]);
	  std::cout << "JDDBG: RxThreshold Link1 of " << m_newInput->moduleName << ": " << m_newInput->thr_Link1 << std::endl;
	}
      } catch (CAN::MRSException &err) {
  //      MRSStream out((mrsPartition.get() ? *(mrsPartition.get()) : *(ipcPartition.get())));
  //      err.sendMessage(out, std::string(argv[0])+":main" );
	std::cerr << "ERROR [AutomaticOPTOLINKanalysis:getBasics] Caught exception " << err.getDescriptor() << std::endl;
      } catch (...) {
	std::cout << "JDDBG: Didn't expect it, but we got a random exception - hope this didn't harm the analysis :-/" << std::endl;
      }
    } else {
      std::cout << "Config for Link2 seems to be missing, skipping -> Setting Link2 Thr to 0 and Delay to inappropiate Value" << std::endl;
      m_newInput->thr_Link2 = 0;
      m_newInput->del_Link2 = -1;
      m_newInput->viset_Link2 = -1;
    }


  }


  void AutomaticOPTOLINKanalysis::getScanConfigs(PixLib::Config& scan_config) 
  {
//    scan_config.dump(std::cout);
    std::vector<int> patternseeds = confVal< std::vector<int> >(scan_config["general"]["patternSeeds"]);
    int sizeofpattern = (patternseeds.size()) * 8 * confVal<int>(scan_config["general"]["repetitions"]);
    std::cout << "JDDBG: Getting maximum err count, pattern had a length of " << patternseeds.size() << " INTs and was multiplied by " <<
    confVal<int>(scan_config["general"]["repetitions"]) << " and divided by four, to be a more reasonable error count" << std::endl;
    m_newInput->Maxcount = (confVal<std::string>(scan_config["general"]["runType"]) == "FMT_COUNT") ? 32768 : sizeofpattern;
    std::string speed = confVal<std::string>(scan_config["mcc"]["mccBandwidth"]);
    if(speed=="SINGLE_40" || speed=="DOUBLE_40") m_newInput->MBs=40;
    else if(speed=="SINGLE_80" || speed=="DOUBLE_80") m_newInput->MBs=80;
    else m_newInput->MBs=-1; //should not happen

    m_newInput->axis = *converter.GetTheAxis(scan_config);
  }


  void AutomaticOPTOLINKanalysis::getModuleConfigs(PixLib::Config& module_config) 
  {
  }


  void AutomaticOPTOLINKanalysis::getHistograms(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos) 
  {
    cout << "JDDBG: In getHistograms..." << endl;
    string conn_name;
    m_feflv= PixGeometry::FEI2_MODULE;//Default Pixel
    
    vector<const TH2*> h_link1, h_link2;  
    if(m_CTdata) {
      DataServerCT * dataServerCT = dynamic_cast<DataServerCT*>( ana_data_server->getDataServer() );
      
      h_link1 = converter.GetTheHisto2Dvect_old( *iterator(dataServerCT->getHistograms( m_histo_type, m_pp0_name).begin(),pos),  
					     *iterator(dataServerCT->getScanConfigRefs( m_histo_type, m_pp0_name).begin(),pos),  m_histo_type, -1,-1, 0, 0);
      h_link2 = converter.GetTheHisto2Dvect_old( *iterator(dataServerCT->getHistograms( m_histo_type, m_pp0_name).begin(),pos),  
					       *iterator(dataServerCT->getScanConfigRefs( m_histo_type, m_pp0_name).begin(),pos), m_histo_type, -1, -1, 0, 1);
    }
    else {
      conn_name = PixA::connectivityName( module_iter );  
      IDataServer* data_server = ana_data_server->getDataServer();

      h_link1 = converter.GetTheHisto2Dvect( m_histo_type, data_server, conn_name, -1, -1, 0, 0);
      h_link2 = converter.GetTheHisto2Dvect( m_histo_type, data_server, conn_name, -1, -1, 0, 1);
    }
    if(!h_link1.empty()){
      cout << "JDDBG: Filling histo for Link1 of " << conn_name << endl;
      m_newInput->histo_Link1 = h_link1;

        //Cannot find a better way to distinguish between Pixel and IBL
        std::cout<<"Histo bins X/Y"<<h_link1[0]->GetNbinsX()<<" "<< h_link1[0]->GetNbinsY()<<std::endl;
        if(h_link1[0]->GetNbinsX()==4 && h_link1[0]->GetNbinsY()==1){//IBL
          m_feflv = PixGeometry::FEI4_MODULE;
        }
    }
    if(!h_link2.empty()) {
      cout << "JDDBG: Filling histo for Link2 of " << conn_name << endl;
      m_newInput->histo_Link2 = h_link2;
    }
    std::cout<<"FE flavour "<<m_feflv<<std::endl;
    m_newInput->_ChipFeFlav = m_feflv;
  }


  void AutomaticOPTOLINKanalysis::saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result2) 
  {
    ResultOPTOLINK* module_result = static_cast<ResultOPTOLINK*>(module_result2);
    unique_ptr<ROOTanalysisHelper> helper(new ROOTanalysisHelper());

    std::cout<<"AutomaticOPTOLINKanalysis::saveAnalysisSpecificResults "<<std::endl;

      if(m_feflv == PixGeometry::FEI4_MODULE ){
        analysis_results.addValue("optimalPhase_LINK1",module_result->moduleName.Data(), module_result->optimalPhase_LINK1);
        analysis_results.addValue("optimalPhase_LINK2",module_result->moduleName.Data(), module_result->optimalPhase_LINK2 );
        analysis_results.addValue("passed_LINK1",module_result->moduleName.Data(),module_result->passed_LINK1);
        analysis_results.addValue("passed_LINK2",module_result->moduleName.Data(),module_result->passed_LINK2);
        std::cout<<"Done saving results"<<std::endl;
        return;
      }

    analysis_results.addValue<float>("bestViset",module_result->moduleName.Data(),module_result->bestViset);
    analysis_results.addValue<float>("optimalThreshold_Link1",module_result->moduleName.Data(),module_result->optimalThreshold_Link1);
    analysis_results.addValue<float>("optimalThreshold_Link2",module_result->moduleName.Data(),module_result->optimalThreshold_Link2);
    analysis_results.addValue<float>("optimalDelay_Link1",module_result->moduleName.Data(),module_result->optimalDelay_Link1);
    analysis_results.addValue<float>("optimalDelay_Link2",module_result->moduleName.Data(),module_result->optimalDelay_Link2);
    analysis_results.addValue<float>("configThreshold_Link1",module_result->moduleName.Data(),module_result->configThreshold_Link1);
    analysis_results.addValue<float>("configThreshold_Link2",module_result->moduleName.Data(),module_result->configThreshold_Link2);
    analysis_results.addValue<float>("configDelay_Link1",module_result->moduleName.Data(),module_result->configDelay_Link1);
    analysis_results.addValue<float>("configDelay_Link2",module_result->moduleName.Data(),module_result->configDelay_Link2);
    analysis_results.addValue<float>("configViSet_Link1",module_result->moduleName.Data(),module_result->configViSet_Link1);
    analysis_results.addValue<float>("configViSet_Link2",module_result->moduleName.Data(),module_result->configViSet_Link2);
    analysis_results.addValue<float>("Common_Spots",module_result->moduleName.Data(),module_result->commonSpots);
    analysis_results.addValue<float>("minErrors_Link1",module_result->moduleName.Data(),module_result->minErrors_Link1);
    analysis_results.addValue<float>("minErrors_Link2",module_result->moduleName.Data(),module_result->minErrors_Link2);
    analysis_results.addValue<float>("minErrors",module_result->moduleName.Data(),module_result->minErrors);
    analysis_results.addValue<bool>("goodThr_Link1",module_result->moduleName.Data(),module_result->goodThr_Link1);
    analysis_results.addValue<bool>("goodThr_Link2",module_result->moduleName.Data(),module_result->goodThr_Link2);
    analysis_results.addValue<bool>("goodDel_Link1",module_result->moduleName.Data(),module_result->goodDel_Link1);
    analysis_results.addValue<bool>("goodDel_Link2",module_result->moduleName.Data(),module_result->goodDel_Link2);
    analysis_results.addValue<bool>("goodTuning_Link1",module_result->moduleName.Data(),module_result->goodTuning_Link1);
    analysis_results.addValue<bool>("goodTuning_Link2",module_result->moduleName.Data(),module_result->goodTuning_Link2);
    analysis_results.addValue<bool>("ViSet_Ok",module_result->moduleName.Data(),module_result->visetOk);
    analysis_results.addValue<bool>("ViSet_Low",module_result->moduleName.Data(),module_result->visetLow);
    analysis_results.addValue<bool>("TuningOk",module_result->moduleName.Data(),module_result->passed);
    analysis_results.addValue<bool>("useNewTuning",module_result->moduleName.Data(),module_result->useNewTuning);

  }

}
