#ifndef _HistoConverter_hh_
#define _HistoConverter_hh_

/**
 // A little helper class used by all the Automatic analyses to convert pixlib histos to root histos
 */
#include "DataContainer/HistoRef.h"
#include "DataContainer/HistoInfo_t.h"
#include "DataContainer/HistoUtil.h"
#include "DataContainer/PixDbDataContainer.h"
#include <ConfigWrapper/ConfObjUtil.h>

#include "analysisInput_BOC.h"
#include "IDataServer.hh"


namespace CAN {

  class HistoConverter 
  {
  public:
    //old versions
    const TH1* GetTheHisto1D_old(const PixA::HistoHandle *histo_handle, const PixA::ScanConfigRef* scanConfig,std::string histoType, int ia, int ib, int iz, int ih); 
    const TH2* GetTheHisto2D_old(const PixA::HistoHandle *histo_handle, const PixA::ScanConfigRef* scanConfig,std::string histoType, int ia, int ib, int iz, int ih); 
    std::vector<const TH2*> GetTheHisto2Dvect_old(const PixA::HistoHandle *histo_handle, const PixA::ScanConfigRef* scanConfig,std::string histoType, int ia, int ib, int iz, int ih); 
    //new versions 
    const TH2* GetTheHisto2D(std::string histo_type, IDataServer *data_server, std::string conn_name, int ia, int ib, int iz, int ih); 
    std::vector<const TH2*> GetTheHisto2Dvect(std::string histo_type, IDataServer *data_server, std::string conn_name, int ia, int ib, int iz, int ih); 

    std::vector<histoAxis_t> * GetTheAxis_old(const PixA::HistoHandle *histo_handle, const PixA::ScanConfigRef* scanConfig) ;
    std::vector<histoAxis_t> * GetTheAxis(PixLib::Config &scan_config); 

    unsigned int getTotalMaskStageSteps(const std::string &label);
    
    TObject* transformHisto(const Histo* h);
    
  private:    
    PixLib::Histo* GetPixLibHisto_old(const PixA::HistoHandle *histo_handle, const PixA::ScanConfigRef* scanConfig,std::string histoType, int ia, int ib, int iz, int ih);
    void makeHistoName(const PixA::ScanConfigRef &scan_config, PixA::HistoRef &histo, std::vector<std::string> &histo_name,std::string histoType) const;

    //statics
//     static void initHistoNameTypeList();
//     static int getHistoType(const std::string &histo_name);
//     static std::map< std::string, int > s_mhistoNameToType;
  };

}
#endif

