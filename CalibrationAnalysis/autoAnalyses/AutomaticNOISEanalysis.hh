#ifndef _CAN_AutomaticNOISEanalysis_hh_
#define _CAN_AutomaticNOISEanalysis_hh_

#include "AutomaticAnalysisBase.hh"
#include "NOISEanalysisROOT.h"
#include "coreAnalyses/analysisInput_NOISE.h"
#include "coreAnalyses/analysisOutput_NOISE.h"



namespace CAN {

  /** 
   * Analysis for DIGITAL scans, for one pp0 
   */
  class AutomaticNOISEanalysis : public AutomaticAnalysisBase
  {
  public:
    AutomaticNOISEanalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &);
    ~AutomaticNOISEanalysis() {};
   
  private:
    void newAnalysisInput() { m_newInput = new AnalysisInputNOISE(); };
    void addNewAnalysisInput(AnalysisInput_t analysisInput) {analysisInput->Add(m_newInput);};
    void requestScanConfigs(IAnalysisDataServer *ana_data_server);
    void requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name);
    void getHistograms(IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos);
    void getBasics(IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos);
    void getScanConfigs(PixLib::Config& scan_config);
    void getModuleConfigs(PixLib::Config& module_config);
    void saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result);

  private:
    std::string m_histo_type; 
    AnalysisInputNOISE* m_newInput;
      //int vcal;
      //bool digital;
      std::string maskStagesMode;
  };

}
#endif

