#ifndef _CAN_AutomaticMONLEAKAnalysis_hh_
#define _CAN_AutomaticMONLEAKAnalysis_hh_

#include "AutomaticAnalysisBase.hh"
#include "MONLEAKanalysisROOT.h"
#include "coreAnalyses/analysisInput_MONLEAK.h"
#include "coreAnalyses/analysisOutput_MONLEAK.h"
#include "ExtendedAnalysisResultList_t.hh"


namespace CAN {


  /** 
   *  Analyzes Monleak scan for one pp0
   */
  class AutomaticMONLEAKAnalysis : public AutomaticAnalysisBase 
  {
  public:
    AutomaticMONLEAKAnalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &);
    ~AutomaticMONLEAKAnalysis() {};

  private:
    void newAnalysisInput() { m_newInput = new AnalysisInputMONLEAK(); };
    void addNewAnalysisInput(AnalysisInput_t analysisInput) { analysisInput->Add(m_newInput); };  
    ////
    void requestScanConfigs  (IAnalysisDataServer *ana_data_server); 
    void requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name);
    void getBasics       (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos); 
    void getScanConfigs  (PixLib::Config&   scan_config);
    void getModuleConfigs(PixLib::Config& module_config);
    void getHistograms   (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos);
    void saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result);

  private:
    std::string             m_histo_type_monleak;

    AnalysisInputMONLEAK* m_newInput;
  };

}
#endif

