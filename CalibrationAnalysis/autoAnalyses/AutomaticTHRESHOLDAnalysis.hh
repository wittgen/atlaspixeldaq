#ifndef _CAN_AutomaticTHRESHOLDAnalysis_hh_
#define _CAN_AutomaticTHRESHOLDAnalysis_hh_

#include "AutomaticAnalysisBase.hh"
#include "THRESHOLDanalysisROOT.h"
#include "coreAnalyses/analysisInput_THRESHOLD.h"
#include "coreAnalyses/analysisOutput_THRESHOLD.h"



namespace CAN {


  /** 
   *  Analyzes threshold mean, noise and chi2 histograms, for one pp0  
   */
  class AutomaticTHRESHOLDAnalysis : public AutomaticAnalysisBase 
  {
  public:
    AutomaticTHRESHOLDAnalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &);
    ~AutomaticTHRESHOLDAnalysis() {};

  private:
    void newAnalysisInput() { m_newInput = new AnalysisInputTHRESHOLD(); };
    void addNewAnalysisInput(AnalysisInput_t analysisInput) { analysisInput->Add(m_newInput); };  
    ////
    void requestScanConfigs  (IAnalysisDataServer *ana_data_server); 
    void requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name);
    void getHistograms   (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos);
    void getBasics       (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos); 
    void getScanConfigs  (PixLib::Config&   scan_config);
    void getModuleConfigs(PixLib::Config& module_config);
    void saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result);

  private:
    std::string             m_histo_type_chi2; 
    std::string             m_histo_type_mean; 
    std::string             m_histo_type_sigma; 
    AnalysisInputTHRESHOLD* m_newInput;
  };

}
#endif

