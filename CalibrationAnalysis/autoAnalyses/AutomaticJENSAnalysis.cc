#include "AutomaticJENSAnalysis.hh"
//#include "ConfigWrapper/ConnectivityUtil.h"
//#include "DataContainer/HistInfo_t.h"


namespace CAN {

  AutomaticJENSanalysis::AutomaticJENSanalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &oi)  
    : AutomaticAnalysisBase( analysis_serial_number, oi) 
  {
    m_name="JENSanalysis";
    m_config=PixLib::Config("JENSanalysis");
    m_analysis = auto_ptr<JENSanalysisROOT>(new JENSanalysisROOT);
    JENSanalysisROOT* anptr = static_cast<JENSanalysisROOT*>(m_analysis.get());
    
    m_config.addGroup("general");
    PixLib::ConfGroup &group=m_config["general"];
    group.addString("m_filename", anptr->m_filename, "JENSanalysisOUTPUT", "File for tons of debug s...tuff", true);

    //

    // todo: add variables you want to configure


    
    vector<string> boc_names;
    m_histo_type = "RAW_DATA_DIFF_1"; //for BOC scans
    boc_names.push_back(m_histo_type); 
    m_scan_histo_names[""] = boc_names; // For analysis with only one scan, scan name doesn't matter

    std::cout << "AutomaticJENSanalysis:   constructed." << std::endl;
  }


  void AutomaticJENSanalysis::requestScanConfigs(IAnalysisDataServer *ana_data_server) 
  {
    ana_data_server->getDataServer()->requestScanConfig();
    ana_data_server->getDataServer()->requestBocConfig();
  }

  void AutomaticJENSanalysis::requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name) 
  {
  
  }

  void AutomaticJENSanalysis::getBasics(IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos) 
  {

    IDataServer *scan_data_server = ana_data_server->getDataServer();

    const PixLib::ModuleConnectivity *module_conn = const_cast<PixLib::ModuleConnectivity *>(*module_iter);
    assert( module_conn );

    m_newInput->pp0Name = m_pp0_name;
    m_newInput->moduleName = getModuleName( ana_data_server, module_iter, pos); 
    int bocLink1 = static_cast<int>( const_cast<PixLib::ModuleConnectivity *>(*module_iter)->bocLinkRx(1));
    int bocLink2 = static_cast<int>( const_cast<PixLib::ModuleConnectivity *>(*module_iter)->bocLinkRx(2));
    unsigned int rx_channel_Link1 = bocLink1%10;
    unsigned int rx_plugin_Link1 = bocLink1/10;
    unsigned int rx_channel_Link2 = bocLink2%10;
    unsigned int rx_plugin_Link2 = bocLink2/10;

    try {
      PixLib::Config &casted_boc_config = const_cast<PixLib::Config &>( scan_data_server->getBocConfig() );
    } catch (CAN::MRSException &err) {
//      MRSStream out((mrsPartition.get() ? *(mrsPartition.get()) : *(ipcPartition.get())));
//      err.sendMessage(out, std::string(argv[0])+":main" );
      std::cerr << "ERROR [AutomaticJENSanalysis:getBasics] Caught exception " << err.getDescriptor() << std::endl;
    } catch (...) {
      std::cout << "JDDBG: Didn't expect it, but we got a random exception - hope this didn't harm the analysis :-/" << std::endl;
    }
    m_newInput->del_Link1 = -1;
    m_newInput->del_Link2 = -1;
    m_newInput->thr_Link1 = -1;
    m_newInput->thr_Link2 = -1;
    
    if ((rx_plugin_Link1 >= 0) && (rx_plugin_Link1 < 4) && (rx_channel_Link1 >= 0) && (rx_channel_Link1 < 8)) {
      try {

	std::stringstream config_name;
	config_name << "PixRx" << rx_plugin_Link1;

	PixLib::Config &casted_boc_config = const_cast<PixLib::Config &>( scan_data_server->getBocConfig() );
	PixLib::Config &rx_config( casted_boc_config.subConfig(config_name.str()));

	{
	  std::stringstream var_name;
	  var_name << "DataDelay" << rx_channel_Link1+2;
	  m_newInput->del_Link1 = confVal<int>(rx_config["General"][var_name.str()]);
	  std::cout << "JDDBG: RxDelay Link1 of " << m_newInput->moduleName << ": " << m_newInput->del_Link1 << std::endl;
	}

	{
	  std::stringstream var_name;
	  var_name << "RxThreshold" << rx_channel_Link1+2;
	  m_newInput->thr_Link1 = confVal<int>(rx_config["Opt"][var_name.str()]);
	  std::cout << "JDDBG: RxThreshold Link1 of " << m_newInput->moduleName << ": " << m_newInput->thr_Link1 << std::endl;
	}
      } catch (CAN::MRSException &err) {
  //      MRSStream out((mrsPartition.get() ? *(mrsPartition.get()) : *(ipcPartition.get())));
  //      err.sendMessage(out, std::string(argv[0])+":main" );
	std::cerr << "ERROR [AutomaticJENSanalysis:getBasics] Caught exception " << err.getDescriptor() << std::endl;
      } catch (...) {
	std::cout << "JDDBG: Didn't expect it, but we got a random exception - hope this didn't harm the analysis :-/" << std::endl;
      }
    } else {
      std::cout << "Config for Link1 seems to be missing, skipping -> Setting Link1 Thr to 0 and Delay to inappropiate Value" << std::endl;
      m_newInput->thr_Link1 = 0;
      m_newInput->del_Link1 = -1;
    }

    if ((rx_plugin_Link2 >= 0) && (rx_plugin_Link2 < 4) && (rx_channel_Link2 >= 0) && (rx_channel_Link2 < 8)) {
      try {

	std::stringstream config_name;
	config_name << "PixRx" << rx_plugin_Link2;

	PixLib::Config &casted_boc_config = const_cast<PixLib::Config &>( scan_data_server->getBocConfig() );
	PixLib::Config &rx_config( casted_boc_config.subConfig(config_name.str()));

	{
	  std::stringstream var_name;
	  var_name << "DataDelay" << rx_channel_Link2+2;
	  m_newInput->del_Link2 = confVal<int>(rx_config["General"][var_name.str()]);
	  std::cout << "JDDBG: RxDelay Link2 of " << m_newInput->moduleName << ": " << m_newInput->del_Link2 << std::endl;
	}

	{
	  std::stringstream var_name;
	  var_name << "RxThreshold" << rx_channel_Link2+2;
	  m_newInput->thr_Link2 = confVal<int>(rx_config["Opt"][var_name.str()]);
	  std::cout << "JDDBG: RxThreshold Link2 of " << m_newInput->moduleName << ": " << m_newInput->thr_Link2 << std::endl;
	}
      } catch (CAN::MRSException &err) {
  //      MRSStream out((mrsPartition.get() ? *(mrsPartition.get()) : *(ipcPartition.get())));
  //      err.sendMessage(out, std::string(argv[0])+":main" );
	std::cerr << "ERROR [AutomaticJENSanalysis:getBasics] Caught exception " << err.getDescriptor() << std::endl;
      } catch (...) {
	std::cout << "JDDBG: Didn't expect it, but we got a random exception - hope this didn't harm the analysis :-/" << std::endl;
      }
    } else {
      std::cout << "Config for Link2 seems to be missing, skipping -> Setting Link2 Thr to 0 and Delay to inappropiate Value" << std::endl;
      m_newInput->thr_Link2 = 0;
      m_newInput->del_Link2 = -1;
    }


  }


  void AutomaticJENSanalysis::getScanConfigs(PixLib::Config& scan_config) 
  {
//    scan_config.dump(std::cout);
    float zaehler = confVal<int>(scan_config["general"]["maskStageSteps"]); 
    float nenner = converter.getTotalMaskStageSteps(confVal<std::string>(scan_config["general"]["maskStageTotalSteps"])); 

    if(nenner!=0) m_newInput->nPixel = 92160*int(zaehler/nenner);
   
    std::string speed = confVal<std::string>(scan_config["mcc"]["mccBandwidth"]);
    if(speed=="SINGLE_40" || speed=="DOUBLE_40") m_newInput->MBs=40;
    else if(speed=="SINGLE_80" || speed=="DOUBLE_80") m_newInput->MBs=80;
    else m_newInput->MBs=-1; //should not happen

    m_newInput->axis = *converter.GetTheAxis(scan_config);
  }


  void AutomaticJENSanalysis::getModuleConfigs(PixLib::Config& module_config) 
  {
  }


  void AutomaticJENSanalysis::getHistograms(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos) 
  {
    cout << "JDDBG: In getHistograms..." << endl;
    string conn_name;
    
    vector<const TH2*> h_link1, h_link2;  
    if(m_CTdata) {
      DataServerCT * dataServerCT = dynamic_cast<DataServerCT*>( ana_data_server->getDataServer() );
      
      h_link1 = converter.GetTheHisto2Dvect_old( *iterator(dataServerCT->getHistograms( m_histo_type, m_pp0_name).begin(),pos),  
					     *iterator(dataServerCT->getScanConfigRefs( m_histo_type, m_pp0_name).begin(),pos),  m_histo_type, -1,0, 0, 0);
      h_link2 = converter.GetTheHisto2Dvect_old( *iterator(dataServerCT->getHistograms( m_histo_type, m_pp0_name).begin(),pos),  
					       *iterator(dataServerCT->getScanConfigRefs( m_histo_type, m_pp0_name).begin(),pos), m_histo_type, -1, 0, 0, 1);
    }
    else {
      conn_name = PixA::connectivityName( module_iter );  
      IDataServer* data_server = ana_data_server->getDataServer();

      h_link1 = converter.GetTheHisto2Dvect( m_histo_type, data_server, conn_name, -1, 0, 0, 0);
      h_link2 = converter.GetTheHisto2Dvect( m_histo_type, data_server, conn_name, -1, 0, 0, 1);
    }
    if(!h_link1.empty()) {
      cout << "JDDBG: Filling histo for Link1 of " << conn_name << endl;
      m_newInput->histo_Link1 = h_link1; }
    if(!h_link2.empty()) { 
      cout << "JDDBG: Filling histo for Link2 of " << conn_name << endl;
      m_newInput->histo_Link2 = h_link2; }
  }


  void AutomaticJENSanalysis::saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result2) 
  {
    ResultJENS* module_result = static_cast<ResultJENS*>(module_result2);
    auto_ptr<ROOTanalysisHelper> helper(new ROOTanalysisHelper());

    analysis_results.addValue<float>("idealMSR0",module_result->moduleName.Data(),module_result->idealMSR0);
    analysis_results.addValue<float>("idealMSR1",module_result->moduleName.Data(),module_result->idealMSR1);
    analysis_results.addValue<float>("mindiff0",module_result->moduleName.Data(),module_result->minval0);
    analysis_results.addValue<float>("mindiff1",module_result->moduleName.Data(),module_result->minval1);

  }

}
