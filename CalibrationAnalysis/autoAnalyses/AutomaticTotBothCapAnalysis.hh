#ifndef _CAN_AutomaticTotBothCapAnalysis_hh_
#define _CAN_AutomaticTotBothCapAnalysis_hh_

#include "AutomaticAnalysisBase.hh"
#include "TotBothCapAnalysisROOT.h"
#include "coreAnalyses/analysisInput_TotBothCap.h"
#include "coreAnalyses/analysisOutput_TotBothCap.h"


namespace CAN {

  /** 
   *  Analyzes threshold mean, noise and chi2 histograms, for one pp0  
   */
  class AutomaticTotBothCapAnalysis : public AutomaticAnalysisBase 
  {
  public:
    AutomaticTotBothCapAnalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &);
    ~AutomaticTotBothCapAnalysis() { cout << "AMB  In destructor for AutomaticTotBothCapAnalysis" << endl; };

  private:
    void newAnalysisInput() { cout << "AMB  In AutomaticTotBothCapAnalysis::newAnalysisInput" << endl; m_newInput = new AnalysisInputTotBothCap(); };
    void addNewAnalysisInput(AnalysisInput_t analysisInput) { cout << "AMB  In AutomaticTotBothCapAnalysis::addNewAnalysisInput" << endl; analysisInput->Add(m_newInput); };  
    void requestScanConfigs  (IAnalysisDataServer *ana_data_server); 
    void requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name);
    void getBasics       (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos); 
    void getScanConfigs  (PixLib::Config&   scan_config);
//     void getScanConfigsChigh_full  (PixLib::Config&   scan_config);
//     void getScanConfigsChigh_lim  (PixLib::Config&   scan_config);
    void getModuleConfigs(PixLib::Config& module_config);
    void getHistograms   (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos);
    void saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result);
    void fillNamedScans();
//     void analyse(IAnalysisDataServer *ana_data_server,
// 		 const PixA::Pp0LocationBase &pp0_location, ExtendedAnalysisResultList_t &analysis_results);

  private:
    std::string             m_histo_type_chi2; 
    std::string             m_histo_type_mean; 
    std::string             m_histo_type_sigma; 
    std::string m_clow;
    std::string m_chigh_full;
    std::string m_chigh_lim;
    AnalysisInputTotBothCap * m_newInput;

  private: //TOT specific
    int m_NvcalSteps_clow;
    //int m_NvcalSteps_chigh_full;
    //int m_NvcalSteps_chigh_lim;
    std::vector<float> m_vcal_array_clow;
    std::vector<float> m_vcal_array_chigh_full;
    std::vector<float> m_vcal_array_chigh_lim;

  };

}
#endif

