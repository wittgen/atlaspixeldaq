#include "AutomaticTOTbasicAnalysis.hh"
#include "ExtendedAnalysisResultList_t.hh"


namespace CAN {
  
  AutomaticTOTbasicAnalysis::AutomaticTOTbasicAnalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &oi)
    : AutomaticAnalysisBase(analysis_serial_number, oi)
  {
//    std::cout<<"AutomaticTOTbasicAnalysis: Starting constructor"<<std::endl;
    m_name = "TOTbasicAnalysis";
    m_config = PixLib::Config("TOTbasicAnalysis");
    m_analysis = unique_ptr<TOTbasicAnalysisROOT>(new TOTbasicAnalysisROOT);
    TOTbasicAnalysisROOT* anptr = static_cast<TOTbasicAnalysisROOT*>(m_analysis.get());

//    std::cout<<"AutomaticTOTbasicAnalysis: Grabbing config"<<std::endl;
    m_config.addGroup("general");
    PixLib::ConfGroup &group = m_config["general"];
    
//    std::cout<<"AutomaticTOTbasicAnalysis: Starting adding"<<std::endl;
    // more variables can be added here
    group.addFloat("expected_mean", anptr->expected_mean, 30., "Expected TOT_MEAN", true);
      //    group.addFloat("expected_sigma", anptr->expected_sigma, 2.5, "Expected TOT_SIGMA", true); [maria elena]
    group.addFloat("expected_sigmaError", anptr->expected_sigmaError, 6., "Integration limits for SIGMA per pixel", true);
    group.addFloat("expected_meanError", anptr->expected_meanError, 5., "Integration limits for MEAN per pixel", true);
    group.addFloat("expected_sigmaFail", anptr->expected_sigmaFail, 6.0, "Allowable SIGMA per Module", true);
    group.addFloat("expected_meanFail", anptr->expected_meanFail, 0.2, "Allowable error on MEAN per Module", true);
    group.addFloat("max_failing_pixels", anptr->max_failing_pixels, 10, "Maximum pct of failing pixels", true);
    group.addBool("isAnalogScan", anptr->isAnalogScan, true, "Fixed charged analog test with ToT histograms", true);
    group.addBool("isFDACTuneScan", anptr->isFDACTuneScan, false, "FDAC tune scan with FDAC histograms", true);
    group.addBool("isIFTuneScan", anptr->isIFTuneScan, false, "IF tune scan with IF histograms", true);

    std::cout<<"AutomaticTOTbasicAnalysis: constructed."<<std::endl;

  }

  void AutomaticTOTbasicAnalysis::requestHistograms(IAnalysisDataServer *ana_data_server, const PixA::Pp0LocationBase &pp0_location)
  {
    TOTbasicAnalysisROOT* anptr = static_cast<TOTbasicAnalysisROOT*>(m_analysis.get());
    vector<std::string> totbasic_names;
    m_histo_type[0] = "TOT_MEAN"; m_histo_type[1]="TOT_SIGMA"; m_histo_type[2]="FDAC_TOT"; m_histo_type[3]="IF_TOT";
    if (anptr->isAnalogScan) {
   std::cout<<"it's an analog scan."<<std::endl;
      totbasic_names.push_back(m_histo_type[0]); 
      totbasic_names.push_back(m_histo_type[1]);
   std::cout<<"loaded histos will be "<<m_histo_type[0]<<" "<<m_histo_type[1]<<std::endl;

      m_scanType = 0;
    }
    if (anptr->isFDACTuneScan) {
   std::cout<<"it's an FDAC scan."<<std::endl;
      totbasic_names.push_back(m_histo_type[2]);
   std::cout<<"loaded histos will be "<<m_histo_type[2]<<std::endl;

      m_scanType = 1;
    }
    if (anptr->isIFTuneScan) {
   std::cout<<"it's an IF scan."<<std::endl;
      totbasic_names.push_back(m_histo_type[3]);
   std::cout<<"loaded histos will be "<<m_histo_type[3]<<std::endl;

      m_scanType = 2;
    }
    m_scan_histo_names[""] = totbasic_names;

    AutomaticAnalysisBase::requestHistograms(ana_data_server, pp0_location);
  }

  void AutomaticTOTbasicAnalysis::requestScanConfigs(IAnalysisDataServer* ana_data_server)
  {
    std::cout<<"AutomaticTOTbasicAnalysis: Request Scan Configs."<<std::endl;
    ana_data_server->getDataServer()->requestScanConfig();
  }

  void AutomaticTOTbasicAnalysis::requestModuleConfigs(IAnalysisDataServer* ana_data_server, string module_name)
  {
    std::cout<<"AutomaticTOTbasicAnalysis: Module Configs."<<std::endl;
      ana_data_server->getDataServer()->requestModuleConfig(module_name);
  }

  void AutomaticTOTbasicAnalysis::getBasics(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos)
  {
    std::cout<<"AutomaticTOTbasicAnalysis: Get Basics."<<std::endl;
    m_newInput->pp0Name = m_pp0_name;
    m_newInput->moduleName = getModuleName(ana_data_server, module_iter, pos);
  }

  void AutomaticTOTbasicAnalysis::getScanConfigs(PixLib::Config& scan_config)
  {
    std::cout<<"AutomaticTOTbasicAnalysis: getScanConfigs"<<std::endl;
    m_newInput->Nmasks = confVal<int>(scan_config["general"]["maskStageSteps"]);
    m_newInput->Nevents = confVal<int>(scan_config["general"]["repetitions"]);
    m_newInput->NTotmasks = (int) converter.getTotalMaskStageSteps(confVal<std::string>(const_cast< PixLib::Config &>(scan_config)["general"]["maskStageTotalSteps"]));
      
    bool digital = confVal<bool>(scan_config["fe"]["digitalInjection"]);
    m_newInput->digitalInjection = digital;

    std::string speed = confVal<std::string>(scan_config["mcc"]["mccBandwidth"]);
    if(speed == "SINGLE_40" || speed == "DOUBLE_40") m_newInput->MBs=40;
    else if(speed == "SINGLE_80" || speed == "DOUBLE_80") m_newInput->MBs=80;
    else m_newInput->MBs=-1; // should not happen

    m_newInput->scanType=m_scanType;

    m_newInput->axis = *converter.GetTheAxis(scan_config);
  }

  void AutomaticTOTbasicAnalysis::getModuleConfigs(PixLib::Config& module_config)
  {
    std::cout<<"AutomaticTOTbasicAnalysis: getModuleConfigs"<<std::endl;
    getDisabledFECP (module_config, m_feflv , m_newInput->fe_disable, m_newInput->cp_enable);
  }


  void AutomaticTOTbasicAnalysis::getHistograms(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos)
  {
    std::cout<<"AutomaticTOTbasicAnalysis: getHistograms"<<std::endl;
    const TH2 *h_mean=0, *h_sigma=0;

    if(m_CTdata){
      std::cout<<"AutomaticTOTbasicAnalysis: CT server"<<std::endl;
      DataServerCT* dataServerCT = dynamic_cast<DataServerCT*>(ana_data_server->getDataServer());

      if (m_scanType==0){
        h_mean = converter.GetTheHisto2D_old(*iterator(dataServerCT->getHistograms(m_histo_type[0], m_pp0_name).begin(),pos),*iterator(dataServerCT->getScanConfigRefs(m_histo_type[0], m_pp0_name).begin(),pos), m_histo_type[0], 0,0,0,0);
        h_sigma = converter.GetTheHisto2D_old(*iterator(dataServerCT->getHistograms(m_histo_type[1], m_pp0_name).begin(),pos),*iterator(dataServerCT->getScanConfigRefs(m_histo_type[1], m_pp0_name).begin(),pos), m_histo_type[1], 0,0,0,0);
      } else if (m_scanType==1) {
        h_mean = converter.GetTheHisto2D_old(*iterator(dataServerCT->getHistograms(m_histo_type[2], m_pp0_name).begin(),pos),*iterator(dataServerCT->getScanConfigRefs(m_histo_type[2], m_pp0_name).begin(),pos), m_histo_type[2], 0,0,0,0);
      } else if (m_scanType==2){
        h_mean = converter.GetTheHisto2D_old(*iterator(dataServerCT->getHistograms(m_histo_type[3], m_pp0_name).begin(),pos),*iterator(dataServerCT->getScanConfigRefs(m_histo_type[3], m_pp0_name).begin(),pos), m_histo_type[3], 0,0,0,0);
      } else if (m_scanType==-1){ // Decide what type of scan this is!!
        cout << "Trying to select the type of scan" << endl;
        try {
          h_mean = converter.GetTheHisto2D_old(*iterator(dataServerCT->getHistograms(m_histo_type[2], m_pp0_name).begin(),pos),*iterator(dataServerCT->getScanConfigRefs(m_histo_type[2], m_pp0_name).begin(),pos), m_histo_type[2], 0,0,0,0);
        } catch (...) {
          cout << "Scan not FDAC" << endl;
          h_mean = 0;
        }
        if (h_mean){
          cout << "This was an FDAC scan!!  Using FDAC histograms." << endl;
          m_scanType=1;
          m_histo_type[0] = "FDAC_TOT";
        } else {
          // Check for IF scan...
          try {
            h_mean = converter.GetTheHisto2D_old(*iterator(dataServerCT->getHistograms(m_histo_type[3], m_pp0_name).begin(),pos),*iterator(dataServerCT->getScanConfigRefs(m_histo_type[3], m_pp0_name).begin(),pos), m_histo_type[3], 0,0,0,0);
          } catch (...) {
            cout << "Scan was not IF" << endl;
            h_mean = 0;
          }
          if (h_mean){
            cout << "This was an IF scan!!  Using IF histograms." << endl;
            m_scanType=2;
            m_histo_type[0] = "IF_TOT";
          } else {
            try {
              h_mean = converter.GetTheHisto2D_old(*iterator(dataServerCT->getHistograms(m_histo_type[0], m_pp0_name).begin(),pos),*iterator(dataServerCT->getScanConfigRefs(m_histo_type[0], m_pp0_name).begin(),pos), m_histo_type[0], 0,0,0,0);
              h_sigma = converter.GetTheHisto2D_old(*iterator(dataServerCT->getHistograms(m_histo_type[1], m_pp0_name).begin(),pos),*iterator(dataServerCT->getScanConfigRefs(m_histo_type[1], m_pp0_name).begin(),pos), m_histo_type[1], 0,0,0,0);
            } catch (...) {
              cout << "Couldn't select a scan type this time around - will retry" << endl;
              h_mean = 0;
            }
            if (h_mean && h_sigma){
              cout << "Recognized scan as Analog!" << endl;
              m_scanType=0;
            }
          }
        }
        if (m_scanType != -1) m_newInput->scanType=m_scanType;
      }

    } else {
      cout<<"AutomaticTOTbasicAnalysis: Connectivity"<<endl;
      string conn_name = PixA::connectivityName(module_iter);
        std::cout<<"conn_name "<<conn_name<<std::endl;
      IDataServer* data_server = ana_data_server->getDataServer();
        
                
      if (m_scanType==0){
        h_mean = converter.GetTheHisto2D(m_histo_type[0], data_server, conn_name, 0,0,0,0);
        h_sigma = converter.GetTheHisto2D(m_histo_type[1], data_server, conn_name, 0,0,0,0);
      } else if (m_scanType==1){
        h_mean = converter.GetTheHisto2D(m_histo_type[2], data_server, conn_name, 0,0,0,-1);
        if (h_mean != NULL) std::cout<<"for FDAC mean is:"<<h_mean->GetMean()<<std::endl;
      } else if (m_scanType==2){
        h_mean = converter.GetTheHisto2D(m_histo_type[3], data_server, conn_name, 0,0,0,-1);
        if (h_mean != NULL) std::cout<<"for IF mean is:"<<h_mean->GetMean()<<std::endl;
      } else if (m_scanType==-1){
        cout << "Attempting to determine scan type..." << endl;
        // Check for FDAC scan...
        try {
          h_mean = converter.GetTheHisto2D(m_histo_type[2], data_server, conn_name, 0,0,0,-1);
        } catch (...) {
          cout << "Scan was not an FDAC" << endl;
          h_mean = 0;
        }
        if (h_mean){
          cout << "This was an FDAC scan!!  Using FDAC histograms." << endl;
          m_scanType=1;
          m_histo_type[0] = "FDAC_TOT";
        } else {
          cout << "We don't think it's an FDAC: " << h_mean << endl;
          // Check for IF scan...
          try {
            h_mean = converter.GetTheHisto2D(m_histo_type[3], data_server, conn_name, 0,0,0,-1);
          } catch (...) {
            cout << "Scan was not IF." << endl;
            h_mean = 0;
          }
          if (h_mean){
            cout << "This was an IF scan!!  Using IF histograms." << endl;
            m_scanType=2;
            m_histo_type[0] = "IF_TOT";
          } else {
            cout << "We don't think it's an IF: " << h_mean << endl;
            try {
              h_mean = converter.GetTheHisto2D(m_histo_type[0], data_server, conn_name, 0,0,0,0);
              h_sigma = converter.GetTheHisto2D(m_histo_type[1], data_server, conn_name, 0,0,0,0);
            } catch (...) {
              cout << "Couldn't select a scan type this time around - will retry" << endl;
              h_mean = 0;
            }
            if (h_mean && h_sigma){
              cout << "Found an analog scan" << endl;
              m_scanType=0;
            }
          }
        }
        if (m_scanType != -1) m_newInput->scanType=m_scanType;

      }
    }

    if( (m_scanType==0 && h_mean && h_sigma) ||
        (m_scanType==1 && h_mean) ||
        (m_scanType==2 && h_mean) )
      {
        std::cout<<"AutomaticTOTbasicAnalysis: saving"<<std::endl;
 	m_newInput->histoMean = h_mean;
        m_newInput->histoSigma = h_sigma;
      }
    else{
       cout<<"something went wrong, histogram is empty: mean="<<h_mean<<" and sigma="<<h_sigma <<endl;
       m_newInput->histoMean = 0;
       m_newInput->histoSigma = 0;
    }
    if (h_mean) {
      PixGeometry geo(PixA::nRows(h_mean),PixA::nColumns(h_mean)); 
      m_feflv = geo.pixType();
      std::cout << "FE flavor: " << m_feflv << std::endl;
    }
  }
  

  void AutomaticTOTbasicAnalysis::saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result2)
  {
    std::cout<<"AutomaticTOTbasicAnalysis: saveResults"<<std::endl;
    ResultTOTbasic* module_result = static_cast<ResultTOTbasic*>(module_result2);
    unique_ptr<ROOTanalysisHelper> helper(new ROOTanalysisHelper());

    std::cout<<"AutomaticTOTbasicAnalysis: basics"<<std::endl;
    analysis_results.addValue<float>("PixPassing", module_result->moduleName.Data(), module_result->PixPassing);
    analysis_results.addValue<float>("PctPassing", module_result->moduleName.Data(), module_result->PctPassing);
    analysis_results.addValue<float>("PixFailingHigh", module_result->moduleName.Data(), module_result->PixFailingHigh);
    analysis_results.addValue<float>("PixFailingLow", module_result->moduleName.Data(), module_result->PixFailingLow);
    analysis_results.addValue<float>("PixFailingSigma", module_result->moduleName.Data(), module_result->PixFailingSigma);
    analysis_results.addValue<float>("PctFailingHigh", module_result->moduleName.Data(), module_result->PctFailingHigh);
    analysis_results.addValue<float>("PctFailingLow", module_result->moduleName.Data(), module_result->PctFailingLow);
    analysis_results.addValue<float>("PctFailingSigma", module_result->moduleName.Data(), module_result->PctFailingSigma);
    analysis_results.addValue<float>("PixGood", module_result->moduleName.Data(), module_result->PixGood);
    analysis_results.addValue<float>("PixDead", module_result->moduleName.Data(), module_result->PixDead);
    analysis_results.addValue<float>("PctGood", module_result->moduleName.Data(), module_result->PctGood);
    analysis_results.addValue<float>("PctDead", module_result->moduleName.Data(), module_result->PctDead);

    std::cout<<"AutomaticTOTbasicAnalysis: generals"<<std::endl;
    analysis_results.addValue<float>("TOTperMod", module_result->moduleName.Data(), module_result->TOTperMod);
    analysis_results.addValue<float>("SIGperMod", module_result->moduleName.Data(), module_result->SIGperMod);
    analysis_results.addValue<float>("TOTEperMod", module_result->moduleName.Data(), module_result->TOTEperMod);
    analysis_results.addValue<float>("SIGEperMod", module_result->moduleName.Data(), module_result->SIGEperMod);

    analysis_results.addValue<float>("TOT_FE0", module_result->moduleName.Data(), module_result->TOTfe0);
    analysis_results.addValue<float>("TOT_FE1", module_result->moduleName.Data(), module_result->TOTfe1);
    if(m_feflv == PixGeometry::FEI2_CHIP || m_feflv == PixGeometry::FEI2_MODULE){
      analysis_results.addValue<float>("TOT_FE2", module_result->moduleName.Data(), module_result->TOTfe2);
      analysis_results.addValue<float>("TOT_FE3", module_result->moduleName.Data(), module_result->TOTfe3);
      analysis_results.addValue<float>("TOT_FE4", module_result->moduleName.Data(), module_result->TOTfe4);
      analysis_results.addValue<float>("TOT_FE5", module_result->moduleName.Data(), module_result->TOTfe5);
      analysis_results.addValue<float>("TOT_FE6", module_result->moduleName.Data(), module_result->TOTfe6);
      analysis_results.addValue<float>("TOT_FE7", module_result->moduleName.Data(), module_result->TOTfe7);
      analysis_results.addValue<float>("TOT_FE8", module_result->moduleName.Data(), module_result->TOTfe8);
      analysis_results.addValue<float>("TOT_FE9", module_result->moduleName.Data(), module_result->TOTfe9);
      analysis_results.addValue<float>("TOT_FE10", module_result->moduleName.Data(), module_result->TOTfe10);
      analysis_results.addValue<float>("TOT_FE11", module_result->moduleName.Data(), module_result->TOTfe11);
      analysis_results.addValue<float>("TOT_FE12", module_result->moduleName.Data(), module_result->TOTfe12);
      analysis_results.addValue<float>("TOT_FE13", module_result->moduleName.Data(), module_result->TOTfe13);
      analysis_results.addValue<float>("TOT_FE14", module_result->moduleName.Data(), module_result->TOTfe14);
      analysis_results.addValue<float>("TOT_FE15", module_result->moduleName.Data(), module_result->TOTfe15);
    }
    analysis_results.addValue<float>("_SIG_FE0", module_result->moduleName.Data(), module_result->SIGfe0);
    analysis_results.addValue<float>("_SIG_FE1", module_result->moduleName.Data(), module_result->SIGfe1);
    if(m_feflv == PixGeometry::FEI2_CHIP || m_feflv == PixGeometry::FEI2_MODULE){
      analysis_results.addValue<float>("_SIG_FE2", module_result->moduleName.Data(), module_result->SIGfe2);
      analysis_results.addValue<float>("_SIG_FE3", module_result->moduleName.Data(), module_result->SIGfe3);
      analysis_results.addValue<float>("_SIG_FE4", module_result->moduleName.Data(), module_result->SIGfe4);
      analysis_results.addValue<float>("_SIG_FE5", module_result->moduleName.Data(), module_result->SIGfe5);
      analysis_results.addValue<float>("_SIG_FE6", module_result->moduleName.Data(), module_result->SIGfe6);
      analysis_results.addValue<float>("_SIG_FE7", module_result->moduleName.Data(), module_result->SIGfe7);
      analysis_results.addValue<float>("_SIG_FE8", module_result->moduleName.Data(), module_result->SIGfe8);
      analysis_results.addValue<float>("_SIG_FE9", module_result->moduleName.Data(), module_result->SIGfe9);
      analysis_results.addValue<float>("_SIG_FE10", module_result->moduleName.Data(), module_result->SIGfe10);
      analysis_results.addValue<float>("_SIG_FE11", module_result->moduleName.Data(), module_result->SIGfe11);
      analysis_results.addValue<float>("_SIG_FE12", module_result->moduleName.Data(), module_result->SIGfe12);
      analysis_results.addValue<float>("_SIG_FE13", module_result->moduleName.Data(), module_result->SIGfe13);
      analysis_results.addValue<float>("_SIG_FE14", module_result->moduleName.Data(), module_result->SIGfe14);
      analysis_results.addValue<float>("_SIG_FE15", module_result->moduleName.Data(), module_result->SIGfe15);
    }
    ExtendedAnalysisResultList_t& e_analysis_results = (ExtendedAnalysisResultList_t&)analysis_results;
    if (module_result->TOTperFE)
      e_analysis_results.addHisto<TH1>("TOT per FrontEnd", module_result->moduleName.Data(), std::shared_ptr<TH1>(module_result->TOTperFE));
    if (module_result->SIGperFE)
      e_analysis_results.addHisto<TH1>("Sigma per FrontEnd", module_result->moduleName.Data(), std::shared_ptr<TH1>(module_result->SIGperFE));
    if (module_result->TOTperPixel)
      e_analysis_results.addHisto<TH1>("TOT per Pixel", module_result->moduleName.Data(), std::shared_ptr<TH1>(module_result->TOTperPixel));
    if (module_result->TOTperPixel)
      e_analysis_results.addHisto<TH1>("Sigma per Pixel", module_result->moduleName.Data(), std::shared_ptr<TH1>(module_result->SIGperPixel));

  }

}

