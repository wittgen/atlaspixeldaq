#ifndef _CAN_AutomaticBOCanalysis_hh_
#define _CAN_AutomaticBOCanalysis_hh_

#include "AutomaticAnalysisBase.hh"
#include "BOCanalysisROOT.h"
#include "coreAnalyses/analysisInput_BOC.h"
#include "coreAnalyses/analysisOutput_BOC.h"



namespace CAN {


  /** 
   *  Analyzes threshold mean, noise and chi2 histograms, for one pp0  
   */
  class AutomaticBOCanalysis : public AutomaticAnalysisBase 
  {
  public:
    AutomaticBOCanalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &);
    ~AutomaticBOCanalysis() {};

  private:
    void newAnalysisInput() { m_newInput = new AnalysisInputBOC(); };
    void addNewAnalysisInput(AnalysisInput_t analysisInput) { analysisInput->Add(m_newInput); };  
    ////
    void requestScanConfigs  (IAnalysisDataServer *ana_data_server); 
    void requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name);
    void getBasics       (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos); 
    void getScanConfigs  (PixLib::Config&   scan_config);
    void getModuleConfigs(PixLib::Config& module_config);
    void getHistograms   (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos);
    void saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result);

  private:
    std::string    m_histo_type; 
    AnalysisInputBOC* m_newInput;
  };

}
#endif

