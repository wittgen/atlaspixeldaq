#include "AutomaticTotBothCapAnalysis.hh"
//#include "ConfigWrapper/ConnectivityUtil.h"
//#include "DataContainer/HistInfo_t.h"


namespace CAN {

  AutomaticTotBothCapAnalysis::AutomaticTotBothCapAnalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &oi)  
    : AutomaticAnalysisBase( analysis_serial_number, oi) 
  {
    cout << "AMB  In constructor for AutomaticTotBothCapAnalysis" << endl;
    m_name="TotBothCapAnalysis";
    m_config=PixLib::Config("TotBothCapAnalysis");
    m_analysis = unique_ptr<TotBothCapAnalysisROOT>(new TotBothCapAnalysisROOT);
    
    m_config.addGroup("general");
    PixLib::ConfGroup &group=m_config["general"];
    group.addString("ScanNameClow", m_clow, "clow", "scanNameClow", true);
    group.addString("ScanNameChigh_full", m_chigh_full, "chigh_full", "scanNameChigh_full", true);
    group.addString("ScanNameChigh_lim", m_chigh_lim, "chigh_lim", "scanNameChigh_lim", true);

    group.addFloat("n_pixeltypes",(static_cast<TotBothCapAnalysisROOT*>(m_analysis.get()))->n_pixeltypes, 2, "number of pixel types", true);
    
    cout << "AutomaticTotBothCapAnalysis:   constructed." << endl;
  }

  void AutomaticTotBothCapAnalysis::fillNamedScans()
  {
    // Needed for analyses with multiple scans!
    cout << "AMB  In AutomaticTotBothCapAnalysis::fillNamedScans" << endl;
    vector<string> tot_names;
    m_histo_type_mean = "TOT_MEAN";
    m_histo_type_sigma = "TOT_SIGMA";
    tot_names.push_back(m_histo_type_mean);
    tot_names.push_back(m_histo_type_sigma);
    m_scan_histo_names.insert(make_pair(m_clow, tot_names));
    m_scan_histo_names.insert(make_pair(m_chigh_full, tot_names));
    m_scan_histo_names.insert(make_pair(m_chigh_lim, tot_names));
  }

  void AutomaticTotBothCapAnalysis::requestScanConfigs(IAnalysisDataServer *ana_data_server) 
  {
    cout << "AMB  In AutomaticTotBothCapAnalysis::requestScanConfigs" << endl;
    ana_data_server->getDataServer()->requestScanConfig();
  }

  void AutomaticTotBothCapAnalysis::requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name) 
  {
    cout << "AMB  In AutomaticTotBothCapAnalysis::requestModuleConfigs" << endl;
    ana_data_server->getDataServer()->requestModuleConfig(module_name);
  }

  void AutomaticTotBothCapAnalysis::getBasics(IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos) 
  {
    cout << "AMB  In AutomaticTotBothCapAnalysis::getBasics" << endl;
    m_newInput->pp0Name = m_pp0_name;
    m_newInput->moduleName = getModuleName( ana_data_server, module_iter, pos);
  }

//   void AutomaticTotBothCapAnalysis::analyse(IAnalysisDataServer *ana_data_server,
// 					    const PixA::Pp0LocationBase &pp0_location, ExtendedAnalysisResultList_t &analysis_results)
//   {
//     cout << "AMB  In AutomaticTotBothCapAnalysis::analyse" << endl;
//     cout << "AMB  1" << endl;
//     AutomaticAnalysisBase::analyse(ana_data_server, pp0_location, analysis_results);
//     cout << "AMB 2" << endl;
//     try
//       {
// 	cout << "AMB  3" << endl;
// 	PixLib::Config& scan_configChigh_full = const_cast<PixLib::Config&>(ana_data_server->getDataServer(m_chigh_full)->getScanConfig());
// 	cout << "AMB  4" << endl;
// 	PixLib::Config& scan_configChigh_lim = const_cast<PixLib::Config&>(ana_data_server->getDataServer(m_chigh_lim)->getScanConfig());
// 	cout << "AMB  5" << endl;
// 	getScanConfigsChigh_full(scan_configChigh_full);
// 	cout << "AMB  6" << endl;
// 	getScanConfigsChigh_lim(scan_configChigh_lim);
// 	cout << "AMB  7" << endl;
//       }
//     catch(CAN::ConfigException &err)
//       {
// 	if (m_logLevel>=kErrorLog)
// 	  {
// 	    cerr << "ERROR [AutomaticAnalysisBase::analyse] Caught exception while getting additional scan configs: " 
// 		 << err.getDescriptor() << endl;
// 	  }
//       } 
//   }

  void AutomaticTotBothCapAnalysis::getScanConfigs(PixLib::Config& scan_config) 
  {
    cout << "AMB  In AutomaticTotBothCapAnalysis::getScanConfigs" << endl;

    m_vcal_array_clow.clear();
    int Nevents = confVal<int>(scan_config["general"]["repetitions"]);
    int NmaskStages = confVal<int>(scan_config["general"]["maskStageSteps"]);     
    string loop_var = confVal<string>(scan_config["loops"]["paramLoop_0"]);
    m_NvcalSteps_clow = confVal<int>(scan_config["loops"]["loopVarNStepsLoop_0"]);
    bool uniform = confVal<bool>(scan_config["loops"]["loopVarUniformLoop_0"]);
    
    if (uniform)
      {
	m_NvcalSteps_clow = confVal<int>(scan_config["loops"]["loopVarNStepsLoop_0"]);
	float min_val = confVal<float>(scan_config["loops"]["loopVarMinLoop_0"]);
	float max_val = confVal<float>(scan_config["loops"]["loopVarMaxLoop_0"]);
	float bin_width = max_val - min_val;
	if (m_NvcalSteps_clow > 1)
	  {
	    bin_width /= (m_NvcalSteps_clow-1);
	  }
	else
	  {
	    if (min_val==max_val)
	      bin_width=0.5 ;
	  }
	float vcal = min_val;
	for (int step_i=0; step_i!=m_NvcalSteps_clow; ++step_i)
	  {
	    m_vcal_array_clow.push_back(vcal);
	    vcal += bin_width;
	  }
      }
    else
      {
	m_vcal_array_clow = confVal<vector<float> >(scan_config["loops"][string("loopVarValuesLoop_0")]);
      }
    
    for (unsigned int i=0; i<m_vcal_array_clow.size(); i++) cout << " " << m_vcal_array_clow[i];
    cout << endl;
    
    m_newInput->charge_clow.clear();
    if (loop_var == "CHARGE") // looping over charge, so vcal array actually contains charge
      {
	cout << "   AMB Looping over charge." << endl;
	for (int fe=0; fe!=16; ++fe)
	  {
	    vector<float> dummy(0);
	    m_newInput->charge_clow.push_back(dummy);
	    m_newInput->charge_clow[fe] = m_vcal_array_clow;
	  }
      }
    m_newInput->n_maskstages_clow = NmaskStages;
    m_newInput->n_events_clow = Nevents;
    m_newInput->n_vcalsteps_clow = m_NvcalSteps_clow;

    // I can't seem to get scan configs from multiple scans, so the here's the values expected
    // from the chigh scans hard coded.
    // AMB TODO: fix that!!
    cout << "INFO Using default values for scan config of chigh scans." << endl;
    m_newInput->n_maskstages_chigh_full = 32;
    m_newInput->n_events_chigh_full = 50;
    m_newInput->n_vcalsteps_chigh_full = 6;
    m_newInput->n_maskstages_chigh_lim = 32;
    m_newInput->n_events_chigh_lim = 50;
    m_newInput->n_vcalsteps_chigh_lim = 6;
    if (! m_vcal_array_chigh_full.size())
      {
	m_vcal_array_chigh_full.push_back(150);
	m_vcal_array_chigh_full.push_back(300);
	m_vcal_array_chigh_full.push_back(450);
	m_vcal_array_chigh_full.push_back(600);
	m_vcal_array_chigh_full.push_back(750);
	m_vcal_array_chigh_full.push_back(900);
      }
    if (! m_vcal_array_chigh_lim.size())
      {
	m_vcal_array_chigh_lim.push_back(25);
	m_vcal_array_chigh_lim.push_back(50);
	m_vcal_array_chigh_lim.push_back(75);
	m_vcal_array_chigh_lim.push_back(100);
	m_vcal_array_chigh_lim.push_back(125);
	m_vcal_array_chigh_lim.push_back(150);
      }
  }
 
//   void AutomaticTotBothCapAnalysis::getScanConfigsChigh_full(PixLib::Config& scan_config) 
//   {
//     cout << "AMB  In AutomaticTotBothCapAnalysis::getScanConfigsChigh_full" << endl;
//     cout << "AMB dumping scan_config" << endl;
//     scan_config.dump(cout);
//   }
  
//   void AutomaticTotBothCapAnalysis::getScanConfigsChigh_lim(PixLib::Config& scan_config) 
//   {
//     cout << "AMB  In AutomaticTotBothCapAnalysis::getScanConfigsChigh_lim" << endl;
//     cout << "AMB dumping scan_config" << endl;
//     scan_config.dump(cout);
//   }
  
  void AutomaticTotBothCapAnalysis::getModuleConfigs(PixLib::Config& module_config) 
  {
    cout << "AMB  In AutomaticTotBothCapAnalysis::getModuleConfigs Config" << endl;
    if (m_newInput->charge_clow.size() != 0) return; // I am looping over charge and the array is already filled

    PixLib::Config &casted_a_config=const_cast<PixLib::Config &>(module_config);
    if (casted_a_config.size()<=0 && casted_a_config.subConfigSize()<=0) return;

    vector<vector<float> > charge_array_clow;
    vector<vector<float> > charge_array_chigh_full;
    vector<vector<float> > charge_array_chigh_lim;
    
    for (int fe=0; fe!=16; ++fe)
      {
	vector<float> dummy(0);
	charge_array_clow.push_back(dummy);
	charge_array_chigh_full.push_back(dummy);
	charge_array_chigh_lim.push_back(dummy);
	char fe_name[50];
	sprintf(fe_name,"PixFe_%i",fe); 
	PixLib::Config& fe_config = module_config.subConfig(string(fe_name));
	float cInj_clow = ((PixLib::ConfFloat&)fe_config["Misc"]["CInjLo"]).value();
	float cInj_chigh = ((PixLib::ConfFloat&)fe_config["Misc"]["CInjHi"]).value();
	float vcal_a = ((PixLib::ConfFloat&)fe_config["Misc"]["VcalGradient3"]).value();
	float vcal_b = ((PixLib::ConfFloat&)fe_config["Misc"]["VcalGradient2"]).value();
	float vcal_c = ((PixLib::ConfFloat&)fe_config["Misc"]["VcalGradient1"]).value();
	float vcal_d = ((PixLib::ConfFloat&)fe_config["Misc"]["VcalGradient0"]).value();
	for (int step=0; step!=m_NvcalSteps_clow; ++step)
	  {
	    float v = m_vcal_array_clow[step];
	    float q = (vcal_a*v*v*v + vcal_b*v*v + vcal_c*v + vcal_d)*cInj_clow/0.160218;
	    charge_array_clow[fe].push_back(q);
	  }
	for (unsigned int step=0; step != m_vcal_array_chigh_full.size(); ++step)
	  {
	    float v = m_vcal_array_chigh_full[step];
	    float q = (vcal_a*v*v*v + vcal_b*v*v + vcal_c*v + vcal_d)*cInj_chigh/0.160218;
	    charge_array_chigh_full[fe].push_back(q);
	  }
	for (unsigned int step=0; step != m_vcal_array_chigh_lim.size(); ++step)
	  {
	    float v =m_vcal_array_chigh_lim[step];
	    float q = (vcal_a*v*v*v + vcal_b*v*v + vcal_c*v + vcal_d)*cInj_chigh/0.160218;
	    charge_array_chigh_lim[fe].push_back(q);
	  }
      }
    // Charge array for clow is straightforward.
    m_newInput->charge_clow = charge_array_clow;
    // Charge array for m_newInput for chigh is raw; it will be scaled by the algorithm in TotBothCapAnalysisROOT.
    m_newInput->charge_chigh_full = charge_array_chigh_full;
    m_newInput->charge_chigh_lim = charge_array_chigh_lim;
  }
  
  void AutomaticTotBothCapAnalysis::getHistograms(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos) 
  {
    cout << "AMB  In AutomaticTotBothCapAnalysis::getHistograms" << endl;

    //if (m_newInput->charge.size() == 0) getModuleConfigs(PixA::connectivityName(module_iter));
    
    for (int step=0; step!=m_NvcalSteps_clow; ++step)
      {
	const TH2 * h_mean;
	const TH2 * h_sigma;  
	if(m_CTdata)
	  {
	    cout << "   AMB I think this if branch should never happen. ERROR WARNING" << endl;
// 	    DataServerCT * dataServerCT = dynamic_cast<DataServerCT*>( ana_data_server->getDataServer() );
	
// 	    h_mean  = converter.GetTheHisto2D_old( *iterator(dataServerCT->getHistograms( m_histo_type_mean, m_pp0_name).begin(),pos),  
// 						   *iterator(dataServerCT->getScanConfigRefs( m_histo_type_mean, m_pp0_name).begin(),pos),  m_histo_type_mean, 0,0,step,0);
// 	    h_sigma = converter.GetTheHisto2D_old( *iterator(dataServerCT->getHistograms( m_histo_type_sigma, m_pp0_name).begin(),pos),  
// 						   *iterator(dataServerCT->getScanConfigRefs( m_histo_type_sigma, m_pp0_name).begin(),pos),  m_histo_type_sigma, 0,0,step,0);
	  }
	else
	  {
	    string conn_name = PixA::connectivityName( module_iter );  
	    IDataServer* data_server = ana_data_server->getDataServer(m_clow);
	    IDataServer* data_server2 = ana_data_server->getDataServer(m_chigh_full);
	    IDataServer* data_server3 = ana_data_server->getDataServer(m_chigh_lim);
	    h_mean  = converter.GetTheHisto2D( m_histo_type_mean, data_server, conn_name, 0,0,step,0);
	    h_sigma = converter.GetTheHisto2D( m_histo_type_sigma,data_server, conn_name, 0,0,step,0);

	    const TH2 * h_mean2  = converter.GetTheHisto2D( m_histo_type_mean, data_server2, conn_name, 0,0,step,0);
	    const TH2 * h_sigma2 = converter.GetTheHisto2D( m_histo_type_sigma,data_server2, conn_name, 0,0,step,0);
	    const TH2 * h_mean3  = converter.GetTheHisto2D( m_histo_type_mean, data_server3, conn_name, 0,0,step,0);
	    const TH2 * h_sigma3 = converter.GetTheHisto2D( m_histo_type_sigma,data_server3, conn_name, 0,0,step,0);

	    bool emptyHisto = false;
	    if(h_mean)   m_newInput->histo_mean_clow.push_back(h_mean);
	    else emptyHisto = true;
	    if(h_sigma)  m_newInput->histo_sigma_clow.push_back(h_sigma);
	    else emptyHisto = true;
	    if(h_mean2)  m_newInput->histo_mean_chigh_full.push_back(h_mean2);
	    else emptyHisto = true;
	    if(h_sigma2) m_newInput->histo_sigma_chigh_full.push_back(h_sigma2);
	    else emptyHisto = true;
	    if(h_mean3)  m_newInput->histo_mean_chigh_lim.push_back(h_mean3);
	    else emptyHisto = true;
	    if(h_sigma3) m_newInput->histo_sigma_chigh_lim.push_back(h_sigma3);
	    else emptyHisto = true;
	    if (emptyHisto) cout << "ERROR At least one null mean or sigma histogram from the ToT Calibration scan(s)." << endl; 
	  }
      }
  }


  void AutomaticTotBothCapAnalysis::saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result2) 
  {
    cout << "AMB  In AutomaticTotBothCapAnalysis::saveAnalysisSpecificResults" << endl;
    ResultTotBothCap* module_result = static_cast<ResultTotBothCap*>(module_result2);
    unique_ptr<ROOTanalysisHelper> helper(new ROOTanalysisHelper());
    ExtendedAnalysisResultList_t& extended_results = (ExtendedAnalysisResultList_t&)analysis_results;
    char tempstr[50];

    cout << "**********************************" << endl;
    cout << "**********************************" << endl;
    cout << "Results for module " << module_result->moduleName.Data() << ":" << endl;
    
//     for (int i = 0; i < module_result->fe_pixtype_tot.size(); i++) // par
//       {
// 	for (int j = 0; j < module_result->fe_pixtype_tot[i].size(); j++) // fe
// 	  {
// 	    cout << "AMB par" << i << ", fe" << j;
// 	    for (int k = 0; k < module_result->fe_pixtype_tot[i][j].size(); k++) // pixtype
// 	      {
// 		cout << ", pixtype" << k << "=" << module_result->fe_pixtype_tot[i][j][k];
// 	      }
// 	    cout << endl;
// 	  }
//       }
    
    for (int fe=0; fe < helper->NfrontEnds; ++fe)
      {
	for (int pixtype=0; pixtype < 2; pixtype++)
	  {
	    std::string pixtypeStr;
	    pixtypeStr = helper->getPixelClassString(pixtype+1,2);
	    std::cout << "Results for module " << module_result->moduleName.Data() << " FE " << fe << " " << pixtypeStr << "pixels: " << std::endl;
	    for (int par=0; par < 3; ++par)
	      {
		sprintf(tempstr,"tot_p%i_FE%i_%s_pixels",par,fe,pixtypeStr.c_str());
		extended_results.addValue<float>(tempstr,module_result->moduleName.Data(),module_result->fe_pixtype_tot[par][fe][pixtype]);
		cout << tempstr << ": " << module_result->fe_pixtype_tot[par][fe][pixtype] << endl;
		
		sprintf(tempstr,"tot_p%i_err_FE%i_%s_pixels",par,fe,pixtypeStr.c_str());
		extended_results.addValue<float>(tempstr,module_result->moduleName.Data(),module_result->fe_pixtype_tot_err[par][fe][pixtype]);
		cout << tempstr << ": " << module_result->fe_pixtype_tot_err[par][fe][pixtype] << endl;
	      } // for par
	  } // for pixtype
      } // for fe
	
    cout << "**********************************" << endl;
    cout << "**********************************" << endl;
    cout << "Those were the results!" << endl;
  }
}
  
