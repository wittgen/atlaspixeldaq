#ifndef _CAN_AutomaticBONDAnalysis_hh_
#define _CAN_AutomaticBONDAnalysis_hh_

#include "AutomaticAnalysisBase.hh"
#include "BONDanalysisROOT.h"
#include "coreAnalyses/analysisInput_BOND.h"
#include "coreAnalyses/analysisOutput_BOND.h"



namespace CAN {


  /** 
   *  Compares threshold noise histograms taken with HVon/off, for one pp0 
   */
  class AutomaticBONDAnalysis : public AutomaticAnalysisBase 
  {
  public:
    AutomaticBONDAnalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &);
    ~AutomaticBONDAnalysis() {};

  private:
    void newAnalysisInput() { m_newInput = new AnalysisInputBOND(); };
    void addNewAnalysisInput(AnalysisInput_t analysisInput) { analysisInput->Add(m_newInput); };  
    ////
    void requestScanConfigs  (IAnalysisDataServer *ana_data_server); 
    void requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name);
    void getBasics       (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos); 
    void getScanConfigs  (PixLib::Config&   scan_config);
    void getHistograms   (IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos);
    void getModuleConfigs(PixLib::Config& module_config);
    void saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result);

  private:
    // Helper funciton of AutomaticBONDAnalysis: 
    int getNpixels(IDataServer* data_server);
    int getNpixels(std::list<PixA::ScanConfigRef*>::const_iterator scanConfig_iter_sigma); //for CT
    string             m_histo_type;
    string             m_scanNameHVon;
    string             m_scanNameHVoff;
    AnalysisInputBOND* m_newInput;
  };

}
#endif

