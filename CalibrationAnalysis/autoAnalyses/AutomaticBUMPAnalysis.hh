#ifndef _CAN_AutomaticBUMPAnalysis_hh_
#define _CAN_AutomaticBUMPAnalysis_hh_

#include "AutomaticAnalysisBase.hh"
#include "BUMPanalysisROOT.h"
#include "coreAnalyses/analysisInput_BUMP.h"
#include "coreAnalyses/analysisOutput_BUMP.h"
#include "ExtendedAnalysisResultList_t.hh"


namespace CAN {

  /** 
   * Analysis for BUMP scans, for one pp0 
   */
  class AutomaticBUMPAnalysis : public AutomaticAnalysisBase 
  {
  public:
    AutomaticBUMPAnalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &);
    ~AutomaticBUMPAnalysis() {};

  private:
    void newAnalysisInput() { m_newInput = new AnalysisInputBUMP(); };
    void addNewAnalysisInput(AnalysisInput_t analysisInput) { analysisInput->Add(m_newInput); };  
    ////
    void requestScanConfigs  (IAnalysisDataServer *ana_data_server); 
    void requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name);
    void getHistograms   (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos);
    void getBasics       (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos); 
    void getScanConfigs  (PixLib::Config&   scan_config);
    void getModuleConfigs(PixLib::Config& module_config);
    void saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result);
    void fillNamedScans();
      
  private:
    string                  m_xtalkScan;
      //****attempt****//
    string                  m_analogScan;
    string                  m_analogScan_merg;
    string                  m_thresholdScan;
    AnalysisInputBUMP*  m_newInput;
  };

}
#endif

