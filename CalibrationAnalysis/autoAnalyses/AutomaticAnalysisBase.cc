#include "AutomaticAnalysisBase.hh"
#include "ConfigWrapper/ConnectivityUtil.h"
#include <ConfigWrapper/PixDisableUtil.h>

#include "PixFe/PixGeometry.h"
#include "DataContainer/GenericHistogramAccess.h"

namespace CAN {

  AutomaticAnalysisBase::AutomaticAnalysisBase(SerialNumber_t analysis_serial_number, const ObjectInfo_t &) : 
    m_serialNumber(analysis_serial_number), m_logLevel(kWarnLog),  m_abortRequest(false), m_CTdata(false), m_config(""), m_feflv(PixGeometry::INVALID_MODULE)
  {
  }
    
    
  /**
   *  Request all the histograms whos names are specified in the 'm_scan_histo_names' array.
   *  It has to be filled in each analysis's constructor!
   */
  void AutomaticAnalysisBase::requestHistograms(IAnalysisDataServer *ana_data_server, const PixA::Pp0LocationBase &pp0_location)
  {
    fillNamedScans();

    m_pp0_name = pp0_location;
    m_CTdata = (dynamic_cast<DataServerCT*>(ana_data_server->getDataServer()) != NULL); //just checking for the first scan

    if(m_CTdata) { //in case we are reading CT files
      for(map<string,vector<string> >::const_iterator scan_histo_names = m_scan_histo_names.begin(); scan_histo_names!=m_scan_histo_names.end(); ++scan_histo_names) 
	for(unsigned int j=0; j<scan_histo_names->second.size(); j++)
	  if(scan_histo_names->second.size()==1){
	    ana_data_server->getDataServer()->requestHistogram( m_scan_histo_names.begin()->second[j], m_pp0_name); 
	  }else{
	    ana_data_server->getDataServer( scan_histo_names->first )->requestHistogram( scan_histo_names->second[j], m_pp0_name);
	  }
      return;
    }

    // All histograms which should be analysed in analyse need to be requested. 
    // Otherwise they will not be downloaded from the histogram server.
    const PixA::ModuleList &module_list = ana_data_server->connectivity().modules(pp0_location);
    PixA::ModuleList::const_iterator module_iter=module_list.begin();

    // LOOP over modules 
    for(; module_iter != module_list.end(); ++module_iter) {
      if(!PixA::enabled( ana_data_server->getCombinedModuleDisabledList(pp0_location), module_iter )) continue; // Only request for enabled modules

      std::string conn_name = PixA::connectivityName( module_iter );  
      //**
      requestModuleConfigs(ana_data_server,conn_name);
      //**
      // Loop over scans
      for(map<string,vector<string> >::const_iterator scan_histo_names = m_scan_histo_names.begin(); scan_histo_names!=m_scan_histo_names.end(); ++scan_histo_names) {
          std::cout<<scan_histo_names->first<<std::endl;
	// Loop over histo types for one scan
	for(unsigned int j=0; j<scan_histo_names->second.size(); j++) {
        std::cout<<scan_histo_names->second.size()<<std::endl;
	  // Request histos for each module and histo type and scan!
	  if (scan_histo_names->second.size()==1 && scan_histo_names->second[0]=="All"){
	    std::vector<std::string> all_scan_histo_names = ana_data_server->getDataServer(scan_histo_names->first )->availableHistograms(conn_name);
	    std::vector<std::string> ::iterator histo_iterator;
	    for (histo_iterator = all_scan_histo_names.begin() ; histo_iterator != all_scan_histo_names.end() ; histo_iterator++){
	      ana_data_server->getDataServer( scan_histo_names->first )->requestHistogram( *histo_iterator, conn_name); 
	    }
	  }
	  //end peter
	      // Request histos for each module and histo type and scan!
/*	      else if(scan_histo_names->second.size()==1){
              std::cout<<"flag 9"<<std::endl;
              std::cout<<scan_histo_names->second[j]<<std::endl;
		ana_data_server->getDataServer( scan_histo_names->first )->requestHistogram( m_scan_histo_names.begin()->second[j], conn_name);
	      }*/
          else{
              std::cout<<scan_histo_names->second[j]<<std::endl;
		ana_data_server->getDataServer( scan_histo_names->first )->requestHistogram( scan_histo_names->second[j], conn_name);
               std::cout<<conn_name<<std::endl;
	      }
	}
      }
    }
    if (m_logLevel>=kInfoLog) {
      std::cout << "INFO [AutomaticAnalysisBase::requestHistogram] requested histograms for " << m_pp0_name << "."<< std::endl;
    }
    //**
    requestScanConfigs(ana_data_server);
    //**
  }

  void AutomaticAnalysisBase::analyse(IAnalysisDataServer *ana_data_server, const PixA::Pp0LocationBase &pp0_location, ExtendedAnalysisResultList_t &analysis_results) 
  {
    m_pp0_name = pp0_location; 

    if (m_logLevel>=kAllDiagnosticsLog) {
      std::cout << "INFO [AutomaticAnalysisBase::analyse]" << std::endl;
    }
    m_analysis->Reset();

    if (m_logLevel>=kInfoLog) {
      std::cout << "INFO [AutomaticAnalysisBase::analyse]  starting.." << std::endl;
    }

    AnalysisInput_t analysisInput(new TObjArray());
    analysisInput->SetOwner(); // such that the objects get deleted 

    int pos(0);
    std::list<PixA::HistoHandle*> histos;
    std::list<PixA::HistoHandle*>::const_iterator histos_iter;
    PixA::ModuleList::const_iterator module_iter;
    if(m_CTdata) {
      DataServerCT * dataServerCT = dynamic_cast<DataServerCT*>( ana_data_server->getDataServer() ); 
      if(!m_scan_histo_names.empty()) {
	histos = dataServerCT->getHistograms( m_scan_histo_names[0][0], m_pp0_name); //take first histo for looping
	histos_iter = histos.begin();
      } //how to loop if there are no histos?, like for meta-analysis, old results would have to be provided by DataServerCT 
    }
    //    else //workes with CTdata???
    const PixA::ModuleList& module_list( ana_data_server->connectivity().modules(pp0_location) ); 
    module_iter = module_list.begin();

    //    if(!ana_data_server->getCombinedModuleDisabledList(pp0_location).isValid()) {
    //std::cout << "AutomaticAnalysisBase:  No valid module_disabled list !!   -> will tread all modules as enabled." << std::endl;
    //       if (m_logLevel>=kInfoLog) {
    // // size method does not exist anymore
    // 	std::cout << "INFO [AutomaticAnalysisBase::analyse]    disable list size: " << ana_data_server->getCombinedModuleDisabledList(pp0_location).size() << std::endl;//temp
    //       }
    //}

    bool alldisabled = true;
    /// LOOP over modules 
    while(!abortRequested()) {

      bool module_enabled = PixA::enabled( ana_data_server->getCombinedModuleDisabledList(pp0_location), module_iter );
      // Check for disabled modules
      if (m_logLevel>=kInfoLog) {
	std::cout << "INFO [AutomaticAnalysisBase::analyse]  Module " << PixA::connectivityName(module_iter)
		  << " is " << (module_enabled ? "enabled" : "disabled") 
		  << std::endl;
      }

      if(module_enabled) { 

	alldisabled = false;
	// Start filling analysisInput for this module
	newAnalysisInput();

	try {
	  getBasics       ( ana_data_server, module_iter, pos );
	  //**
	  getHistograms   ( ana_data_server, module_iter, pos );
	  //**
          getScanConfigs  ( ana_data_server, module_iter, pos );
          //**
	  getModuleConfigs( ana_data_server, module_iter, pos );
	  //**
	}
	catch(CAN::ConfigException &err) {
	  if (m_logLevel>=kErrorLog) std::cerr << "ERROR [AutomaticAnalysisBase::analyse] Caught exception while retrieving histograms and configurations : " 
					       << err.getDescriptor() << std::endl;
	}
	addNewAnalysisInput( analysisInput );  //add input for all enabled modules, so that if no histos -> results can be set accordingly 
      }
      // More modules to loop?
      if(m_CTdata) {
	if(iterator(histos_iter,++pos)==histos.end()) break;
      }
      else {
	if(++module_iter==module_list.end()) break;
      }
    }

   if(alldisabled){
     std::cout<<"All modules in this PP0 are disabled, skipping..."<<std::endl;
     return;
   }

    if(!m_CTdata){
      if (m_logLevel>=kInfoLog) {
	std::cout << "INFO [AutomaticAnalysisBase::analyse] Running in online framework, preparation of analysis input finished." << std::endl;    
      }
    }
    //**
    m_analysis->Analyse(analysisInput); //allways called, core-analysis has to handle 'no histo' case  
    //**
    if (m_logLevel>=kDiagnostics5Log) {
      std::cout << "DIAGNOSTICS [AutomaticAnalysisBase::analyse] Analysis done for " << pp0_location << "." << std::endl;
    }
    if(m_CTdata) {
      DataServerCT * dataServerCT = dynamic_cast<DataServerCT*>( ana_data_server->getDataServer() );
      m_analysis->WriteRootFile(dataServerCT->GetFileName(),analysisInput,pp0_location);
      if (m_logLevel>=kInfoLog) {
	std::cout << "INFO [AutomaticAnalysisBase::analyse] Running in stand alone mode, analysis input root file has been created, "
		  << "it can be analysed in the offline framework." << std::endl;
      }
    }
    else
      saveAnalysisResults( analysis_results, m_analysis->GetResult() );

    // Clean up
    // analysisInput->Delete(); delete analysisInput; 
    if (m_logLevel>=kDiagnostics5Log) {
      std::cout << "DIAGNOSTICS [AutomaticAnalysisBase::analyse] Will now cleanup " << pp0_location << "." << std::endl;
    }
    analysisInput.reset();
    if (m_logLevel>=kDiagnostics5Log) {
      std::cout << "DIAGNOSTICS [AutomaticAnalysisBase::analyse] cleanup is done " << pp0_location << "." << std::endl;
    }
  }

  void AutomaticAnalysisBase::getScanConfigs(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos) 
  {

    if(m_CTdata) {
      DataServerCT * dataServerCT = dynamic_cast<DataServerCT*>( ana_data_server->getDataServer() );
      PixA::ConfigRef scan_config_ref = (*iterator(dataServerCT->getScanConfigRefs( *m_scan_histo_names.begin()->second.begin(), m_pp0_name).begin(),pos))->pixScanConfig();
      //@todo should pass a const PixLib::Config
      getScanConfigs(const_cast<PixLib::Config &>(scan_config_ref.config()));
    }
    else {
        for(map<string,vector<string> >::const_iterator scan_histo_names = m_scan_histo_names.begin(); scan_histo_names!=m_scan_histo_names.end(); ++scan_histo_names) {
            
            std::cout<<ana_data_server<<std::endl;
            std::cout<<scan_histo_names->first<<std::endl;

      //@todo should pass a const PixLib::Config
      PixLib::Config& scan_config = const_cast<PixLib::Config&>(ana_data_server->getDataServer(scan_histo_names->first)->getScanConfig());
            getScanConfigs(scan_config);
        }
    }
  }

  void AutomaticAnalysisBase::getModuleConfigs(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos)
  {
     if(!ana_data_server->getDataServer()->needModuleConfig()){
          return; }//suppose its the same for all scans
     if(m_CTdata) {
      DataServerCT * dataServerCT = dynamic_cast<DataServerCT*>( ana_data_server->getDataServer() );
      PixA::ConfigRef module_config_ref = (*iterator(dataServerCT->getScanConfigRefs( *m_scan_histo_names.begin()->second.begin(), m_pp0_name).begin(),pos))->modConfig();
       getModuleConfigs(const_cast<PixLib::Config &>(module_config_ref.config()));
    }
    else {
        for(map<string,vector<string> >::const_iterator scan_histo_names = m_scan_histo_names.begin(); scan_histo_names!=m_scan_histo_names.end(); ++scan_histo_names) {
            std::cout<<ana_data_server<<std::endl;
            std::cout<<scan_histo_names->first<<std::endl;
            std::cout<<PixA::connectivityName(module_iter)<<std::endl;
            ana_data_server->getDataServer(scan_histo_names->first)->getScanConfig();
            ana_data_server->getDataServer(scan_histo_names->first)->getModuleConfig(PixA::connectivityName(module_iter));
      PixLib::Config& module_config = const_cast<PixLib::Config&>(ana_data_server->getDataServer(scan_histo_names->first)->getModuleConfig(PixA::connectivityName(module_iter)));
                getModuleConfigs(module_config);
        }
    }
  }

  void AutomaticAnalysisBase::saveAnalysisResults(AnalysisResultList_t &analysis_results, AnalysisResult_t results) 
  {
    TIter next(results.get());
    bool comb_status(true);
    string pp0Name("");
    while(AnalysisResult * scan_iter = static_cast<AnalysisResult*>(next.Next()) ) { 
      pp0Name = scan_iter->pp0Name;
      analysis_results.addValue<bool>("Status",scan_iter->moduleName.Data(),scan_iter->passed);
      comb_status &= scan_iter->passed;
      analysis_results.addValue<float>("failures",scan_iter->moduleName.Data(),scan_iter->tests);
      //**
      saveAnalysisSpecificResults( analysis_results, scan_iter ); 
      //**
    }
    if (pp0Name != "") analysis_results.addValue("Status", pp0Name, comb_status);
  }

  string AutomaticAnalysisBase::getModuleName(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos) 
  {
    if(!m_CTdata) {
      return PixA::connectivityName( module_iter );  
    }
    else {
      std::stringstream modName;
      DataServerCT * dataServerCT = dynamic_cast<DataServerCT*>( ana_data_server->getDataServer() );
      std::string conn_name = (*iterator(dataServerCT->getScanConfigRefs( *m_scan_histo_names.begin()->second.begin(), m_pp0_name).begin(),pos))->connName();
      std::string prod_name = (*iterator(dataServerCT->getScanConfigRefs( *m_scan_histo_names.begin()->second.begin(), m_pp0_name).begin(),pos))->prodName();
      if(!conn_name.empty()) 
	modName << conn_name << "(" << prod_name << ")";
      else 
	modName << prod_name;
      return modName.str();
    }
  }

  //This function return a map with the different parameter loop, only active loops are stored
  //The std::pair<std::string, int> stores param loop name and param loop number (0-3)
  //The std::vector stores the parameter loop values
  const std::map<std::pair<std::string, int>, std::vector<float> > AutomaticAnalysisBase::getParamLoop(const PixLib::Config& scan_config) const{

  std::map<std::pair<std::string, int>, std::vector<float> > paramLoopMap;

    for(int p=0;p<3;p++){

      std::string loop = "Loop_"+std::to_string(p);
      std::vector<float> paramVector;

        if( ! (confVal<bool>( scan_config ["loops"]["active"+loop]) ) ){
          paramLoopMap [std::make_pair("NO_VAL", p)] = paramVector;
          continue;
        }

      std::string paramName = confVal<std::string>( scan_config ["loops"]["param"+loop]);
        if (!confVal<bool>(scan_config ["loops"]["loopVarUniform"+loop]) ){
          paramLoopMap [std::make_pair(paramName, p) ] = confVal<std::vector<float> >( scan_config ["loops"][std::string("loopVarValues"+loop)]);
        } else {
          float min_val = confVal<float>(scan_config["loops"]["loopVarMin"+loop]);
          float max_val = confVal<float>(scan_config["loops"]["loopVarMax"+loop]);
          int nSteps = confVal<int>(scan_config["loops"]["loopVarNSteps"+loop]);

            for (int step=0; step < nSteps; step++){
              float paramValue = min_val;
              if(nSteps > 1) paramValue = min_val + ((max_val - min_val) / (nSteps - 1)) * step;
              paramVector.emplace_back(paramValue);
            }
          paramLoopMap[std::make_pair(paramName, p) ] = paramVector;
        }
     }

   return paramLoopMap;
  
  }

  void  AutomaticAnalysisBase::getDisabledFECP (PixLib::Config& module_config, const PixGeometry::pixelType feFlv , std::vector<unsigned short int> &fe_disable, std::vector<std::vector<unsigned short int> > &cp_enable){

      ROOTanalysisHelper helper;

      const int Nfe = helper.getNfrontEnd(feFlv); // # of FEs in one module
      const int Ncp = helper.getNcp(feFlv); // # of the enable column pairs

      if (module_config.size()==0 && module_config.subConfigSize()==0) return;
      for (int fe=0; fe<Nfe; ++fe) {
          std::vector<unsigned short int> fecp_enable;
	  std::string fe_name = "PixFe_" + std::to_string(fe);

          PixLib::Config& fe_config = module_config.subConfig(fe_name);
          if(fe_config.size() == 0 && fe_config.subConfigSize() == 0) continue;

          bool enable_scan = ((PixLib::ConfBool&)fe_config["Misc"]["ScanEnable"]).value();
          bool enable_config = ((PixLib::ConfBool&)fe_config["Misc"]["ConfigEnable"]).value();
          bool enable_dacs = ((PixLib::ConfBool&)fe_config["Misc"]["DacsEnable"]).value();

            if (!enable_scan || !enable_config || !enable_dacs) fe_disable.emplace_back(1);
            else fe_disable.emplace_back(0);

          // get global register
          PixLib::Config& globreg = fe_config.subConfig(std::string("GlobalRegister_0/GlobalRegister"));
          if(globreg.size() == 0 && globreg.subConfigSize() == 0) continue;
          // loop over all 9 column pairs to get disabled CPs
          for(int cp = 0; cp < Ncp; ++cp){
              if(feFlv == PixGeometry::FEI2_CHIP || feFlv == PixGeometry::FEI2_MODULE){
		  std::string cp_name = "ENABLE_CP" + std::to_string(cp);
                  unsigned short int enable_cp = 0;
                  enable_cp = static_cast<PixLib::ConfInt&>(globreg["GlobalRegister"][cp_name]).value();
                  if (enable_cp == 0) cout << "DEBUG, found disabled CP: "<< cp << " on FE: " <<fe <<endl;
                  fecp_enable.push_back(enable_cp);
              } else if(feFlv == PixGeometry::FEI4_CHIP || feFlv == PixGeometry::FEI4_MODULE){
                  int cpid = 0;
                  if(cp<16) cpid = 0;
                  else if(cp<32) cpid = 1;
                  else if(cp<40) cpid = 2;
		  std::string cp_name = "DisableColumnCnfg" + std::to_string(cpid);
                  unsigned short int disable_cp = 1;
                  unsigned short int enable_cp = 0;
                  int shift_bit = cp%16;
                  int disable_bit = static_cast<PixLib::ConfInt&>(globreg["GlobalRegister"][cp_name]).value();
                  disable_cp = (disable_bit >> shift_bit) & 0x01;
                  
                  if(disable_cp == 0) enable_cp = 1;
                  if (enable_cp == 0) cout << "DEBUG, found disabled CP: "<< cp << " on FE: " <<fe <<endl;
                  fecp_enable.push_back(enable_cp);
              }
          }
          cp_enable.push_back(fecp_enable);
      }
  }

}

