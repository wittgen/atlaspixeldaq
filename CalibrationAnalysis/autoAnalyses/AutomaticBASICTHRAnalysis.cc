#include "AutomaticBASICTHRAnalysis.hh"
#include "PixFe/PixGeometry.h"
#include "DataContainer/GenericHistogramAccess.h"

namespace CAN {

  AutomaticBASICTHRAnalysis::AutomaticBASICTHRAnalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &oi)
    : AutomaticAnalysisBase( analysis_serial_number, oi)
  {
    m_name="BASICTHRanalysis";
    m_config=PixLib::Config("BASICTHRanalysis");
    m_analysis = unique_ptr<BASICTHRanalysisROOT>(new BASICTHRanalysisROOT);
    BASICTHRanalysisROOT* anptr = static_cast<BASICTHRanalysisROOT*>(m_analysis.get());

    m_config.addGroup("general");
    PixLib::ConfGroup &group=m_config["general"];
    group.addInt("cut_failed_fit_max", anptr->cut_failed_fit_max, 64, "Maximum number of pixels failing the fit", true);
    group.addFloat("cut_average_threshold_min", anptr->cut_average_threshold_min, 3000, "Minimum average threshold", true);
    group.addFloat("cut_average_threshold_max", anptr->cut_average_threshold_max, 5000, "Maximum average threshold", true);
    group.addFloat("cut_RMS_threshold_max", anptr->cut_RMS_threshold_max, 200, "Maximum threshold dispersion", true);
    group.addFloat("cut_average_noise_min", anptr->cut_average_noise_min, 140, "Minimum average noise", true);
    group.addFloat("cut_average_noise_max", anptr->cut_average_noise_max, 190, "Maximum average noise", true);
    group.addFloat("cut_RMS_noise_max", anptr->cut_RMS_noise_max, 18, "Maximum noise RMS", true);

    vector<string> threshold_names;
    m_histo_type_chi2 = "SCURVE_CHI2";
    m_histo_type_threshold = "SCURVE_MEAN";
    m_histo_type_noise = "SCURVE_SIGMA";
    threshold_names.push_back(m_histo_type_chi2);  
    threshold_names.push_back(m_histo_type_threshold);  
    threshold_names.push_back(m_histo_type_noise);
    m_scan_histo_names[""] = threshold_names;     
    std::cout << "AutomaticBASICTHRAnalysis:   constructed." << std::endl;
  }

  void AutomaticBASICTHRAnalysis::requestScanConfigs(IAnalysisDataServer *ana_data_server) 
  {
    ana_data_server->getDataServer()->requestScanConfig();
  }

  void AutomaticBASICTHRAnalysis::requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name) 
  {
    ana_data_server->getDataServer()->requestModuleConfig(module_name);
  }

  void AutomaticBASICTHRAnalysis::getBasics(IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos) 
  {
    m_newInput->pp0Name = m_pp0_name;
    m_newInput->moduleName = getModuleName( ana_data_server, module_iter, pos); 
  }


  void AutomaticBASICTHRAnalysis::getScanConfigs(PixLib::Config&   scan_config) 
  {
    unique_ptr<ROOTanalysisHelper> helper(new ROOTanalysisHelper());

    float runMS = confVal<int>(const_cast< PixLib::Config &>(scan_config)["general"]["maskStageSteps"]);
    float totMS = converter.getTotalMaskStageSteps(confVal<std::string>(const_cast< PixLib::Config &>(scan_config)["general"]["maskStageTotalSteps"]));
    m_newInput->nMaskStages = (int)runMS;
    m_newInput->nPixel_abs_frac = (int)( (runMS+1) / (totMS+1));
    m_newInput->nTOTMaskStages = (int)totMS;
    std::cout << "Scanned " << runMS << " mask stages out of " << totMS << " = " << m_newInput->nPixel_abs_frac << " pixels out of " << helper->Ncols * helper->Nrows << std::endl;
  }
  
 
  void AutomaticBASICTHRAnalysis::getModuleConfigs(PixLib::Config& module_config) 
  {
      getDisabledFECP (module_config, m_feflv , m_newInput->fe_disable, m_newInput->cp_enable);
  }


  void AutomaticBASICTHRAnalysis::getHistograms(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos) 
  {
    const TH2 *h_chi2, *h_threshold, *h_noise;  
    if(m_CTdata) {
      DataServerCT * dataServerCT = dynamic_cast<DataServerCT*>( ana_data_server->getDataServer() );

      h_chi2  = converter.GetTheHisto2D_old( *iterator(dataServerCT->getHistograms( m_histo_type_chi2, m_pp0_name).begin(),pos),  
					     *iterator(dataServerCT->getScanConfigRefs( m_histo_type_chi2, m_pp0_name).begin(),pos),  m_histo_type_chi2, -1,0,0,0);
      h_threshold  = converter.GetTheHisto2D_old( *iterator(dataServerCT->getHistograms( m_histo_type_threshold, m_pp0_name).begin(),pos),  
					     *iterator(dataServerCT->getScanConfigRefs( m_histo_type_threshold, m_pp0_name).begin(),pos),  m_histo_type_threshold, -1,0,0,0);
      h_noise = converter.GetTheHisto2D_old( *iterator(dataServerCT->getHistograms( m_histo_type_noise, m_pp0_name).begin(),pos),  
					     *iterator(dataServerCT->getScanConfigRefs( m_histo_type_noise, m_pp0_name).begin(),pos),  m_histo_type_noise, -1,0,0,0);
    }
    else {
      string conn_name = PixA::connectivityName( module_iter );  
      IDataServer* data_server = ana_data_server->getDataServer();
      h_chi2  = converter.GetTheHisto2D( m_histo_type_chi2, data_server, conn_name, -1,0,0,0);
      h_threshold  = converter.GetTheHisto2D( m_histo_type_threshold, data_server, conn_name, -1,0,0,0);
      h_noise = converter.GetTheHisto2D( m_histo_type_noise,data_server, conn_name, -1,0,0,0);
    }
    if(h_chi2!=NULL)  { 
      m_newInput->histo_chi2  = h_chi2; 
    }
    if(h_threshold!=NULL) {
      m_newInput->histo_threshold  = h_threshold; 
    }
    if(h_noise!=NULL) { 
      m_newInput->histo_noise = h_noise; 
    }

    if(h_chi2!=NULL)  { 
      PixGeometry geo(PixA::nRows(h_chi2),PixA::nColumns(h_chi2)); 
      m_feflv = geo.pixType();
      std::cout << "FE flavor: " << m_feflv << std::endl;
    }else{
      cout << "The histogram of chi2 is not obtained. Please check!" << std::endl;
    }
  }


  void AutomaticBASICTHRAnalysis::saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result2) 
  {
    ResultBASICTHR* module_result = static_cast<ResultBASICTHR*>(module_result2);
    analysis_results.addValue<bool>("Passed_unfitted_pixels_max",module_result->moduleName.Data(),module_result->passed_failed_fit_max);
    analysis_results.addValue<bool>("Passed_average_threshold_min",module_result->moduleName.Data(),module_result->passed_average_threshold_min);
    analysis_results.addValue<bool>("Passed_average_threshold_max",module_result->moduleName.Data(),module_result->passed_average_threshold_max);
    analysis_results.addValue<bool>("Passed_RMS_threshold_max",module_result->moduleName.Data(),module_result->passed_RMS_threshold_max);
    analysis_results.addValue<bool>("Passed_average_noise_min",module_result->moduleName.Data(),module_result->passed_average_noise_min);
    analysis_results.addValue<bool>("Passed_average_noise_max",module_result->moduleName.Data(),module_result->passed_average_noise_max);
    analysis_results.addValue<bool>("Passed_RMS_noise_max",module_result->moduleName.Data(),module_result->passed_RMS_noise_max);
    analysis_results.addValue<float>("Failed_fit",module_result->moduleName.Data(),(Float_t)(module_result->failed_fit));
    analysis_results.addValue<float>("Average_threshold",module_result->moduleName.Data(),module_result->average_threshold);
    analysis_results.addValue<float>("Average_noise",module_result->moduleName.Data(),module_result->average_noise);
    analysis_results.addValue<float>("RMS_threshold",module_result->moduleName.Data(),module_result->RMS_threshold);
    analysis_results.addValue<float>("RMS_noise",module_result->moduleName.Data(),module_result->RMS_noise);
  }

}


