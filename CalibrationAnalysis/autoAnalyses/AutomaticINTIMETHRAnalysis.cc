#include "AutomaticINTIMETHRAnalysis.hh"
//#include "ConfigWrapper/ConnectivityUtil.h"
//#include "DataContainer/HistInfo_t.h"


namespace CAN
{

AutomaticINTIMETHRAnalysis::AutomaticINTIMETHRAnalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &oi)  
: AutomaticAnalysisBase( analysis_serial_number, oi) 
{
  m_name="INTIMETHRanalysis";
  m_config=PixLib::Config("INTIMETHRanalysis");
  m_analysis = unique_ptr<INTIMETHRanalysisROOT>(new INTIMETHRanalysisROOT);
  
  m_config.addGroup("general");
  PixLib::ConfGroup &group=m_config["general"];
  
  group.addFloat("cut_noise_min normal",		(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_noise_min[0], 180, "noise min cut for normal pixels", true);
  group.addFloat("cut_noise_max normal",		(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_noise_max[0], 400, "noise max cut for normal pixels", true);
  group.addFloat("cut_noise_min long",		(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_noise_min[1], 200, "noise min cut for long pixels", true);
  group.addFloat("cut_noise_max long",		(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_noise_max[1], 450, "noise max cut for long pixels", true);
  group.addFloat("cut_noise_min ganged",		(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_noise_min[2], 280, "noise min cut for ganged pixels", true);
  group.addFloat("cut_noise_max ganged",		(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_noise_max[2], 650, "noise max cut for ganged pixels", true);
  group.addFloat("cut_noise_min interganged",	(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_noise_min[3], 190, "noise min cut for interganged pixels", true);
  group.addFloat("cut_noise_max interganged",	(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_noise_max[3], 350, "noise max cut for interganged pixels", true);
  
  group.addInt("cut_noise_min_percent normal",		(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_noise_min_percent[0], 90, "percent of normal pixels required above noise min cut", true);
  group.addInt("cut_noise_max_percent normal",		(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_noise_max_percent[0], 90, "percent of normal pixels required below noise max cut", true);
  group.addInt("cut_noise_min_percent long",			(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_noise_min_percent[1], 90, "percent of long pixels required above noise min cut", true);
  group.addInt("cut_noise_max_percent long",			(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_noise_max_percent[1], 90, "percent of long pixels required below noise max cut", true);
  group.addInt("cut_noise_min_percent ganged",		(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_noise_min_percent[2], 90, "percent of ganged pixels required above noise min cut", true);
  group.addInt("cut_noise_max_percent ganged",		(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_noise_max_percent[2], 90, "percent of ganged pixels required below noise max cut", true);
  group.addInt("cut_noise_min_percent interganged",	(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_noise_min_percent[3], 90, "percent of interganged pixels required above noise min cut", true);
  group.addInt("cut_noise_max_percent interganged",	(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_noise_max_percent[3], 90, "percent of interganged pixels required below noise max cut", true);
  
  group.addFloat("cut_thr_min normal_plus_ganaged",	(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_thr_min[0], 4400, "threshold min cut for normal_plus_ganaged pixels", true);
  group.addFloat("cut_thr_max normal_plus_ganaged",	(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_thr_max[0], 6700, "threshold max cut for normal_plus_ganaged pixels", true);
  group.addFloat("cut_thr_min long",					(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_thr_min[1], 4600, "threshold min cut for long pixels", true);
  group.addFloat("cut_thr_max long",					(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_thr_max[1], 7000, "threshold max cut for long pixels", true);
  group.addFloat("cut_thr_min interganged",			(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_thr_min[2], 4300, "threshold min cut for interganged pixels", true);
  group.addFloat("cut_thr_max interganged",			(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_thr_max[2], 5700, "threshold max cut for interganged pixels", true);
  
  group.addInt("cut_thr_min_percent normal_plus_ganaged",	(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_thr_min_percent[0], 95, "percent of normal_plus_ganaged pixels required above threshold min cut", true);
  group.addInt("cut_thr_max_percent normal_plus_ganaged", (static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_thr_max_percent[0], 95, "percent of normal_plus_ganaged pixels required below threshold max cut", true);
  group.addInt("cut_thr_min_percent long",				(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_thr_min_percent[1], 95, "percent of long pixels required above threshold min cut", true);
  group.addInt("cut_thr_max_percent long", 				(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_thr_max_percent[1], 95, "percent of long pixels required below threshold max cut", true);
  group.addInt("cut_thr_min_percent interganged",			(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_thr_min_percent[2], 95, "percent of interganged pixels required above threshold min cut", true);
  group.addInt("cut_thr_max_percent interganged", 		(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_thr_max_percent[2], 95, "percent of interganged pixels required below threshold max cut", true);
  
  group.addFloat("cut_threshold_mean_min normal_plus_ganaged",	(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_thr_mean_min[0], 5000, "threshold mean min cut for normal_plus_ganaged pixels", true);
  group.addFloat("cut_threshold_mean_max normal_plus_ganaged",	(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_thr_mean_max[0], 5800, "threshold mean max cut for normal_plus_ganaged pixels", true);
  group.addFloat("cut_threshold_mean_min long",					(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_thr_mean_min[1], 5200, "threshold mean min cut for long pixels", true);
  group.addFloat("cut_threshold_mean_max long",					(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_thr_mean_max[1], 6000, "threshold mean max cut for long pixels", true);
  group.addFloat("cut_threshold_mean_min interganged",			(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_thr_mean_min[2], 4400, "threshold mean min cut for interganged pixels", true);
  group.addFloat("cut_threshold_mean_max interganged",			(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_thr_mean_max[2], 5200, "threshold mean max cut for interganged pixels", true);

  group.addFloat("cut_thr_RMS_max normal_plus_ganaged",	(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_thr_RMS_max[0], 200, "threshold RMS max cut for normal_plus_ganaged pixels", true);
  group.addFloat("cut_thr_RMS_max long",					(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_thr_RMS_max[1], 250, "threshold RMS max cut for long pixels", true);
  group.addFloat("cut_thr_RMS_max interganged",			(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_thr_RMS_max[2], 200, "threshold RMS max cut for interganged pixels", true);

  group.addInt("threshold_mean_range_min",	(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->range_thr_mean_min,		3000, "threshold range min for mean and RMS calc", true);
  group.addInt("threshold_mean_range_max",	(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->range_thr_mean_max,		10000, "threshold range max for mean and RMS calc", true);
  group.addInt("cut_num_bad_pixels_per_FE",	(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->cut_num_bad_pixels_per_FE,	200, "max number bad pixels per FE", true);
  group.addBool("create_Pixelmap",			(static_cast<INTIMETHRanalysisROOT*>(m_analysis.get()))->create_Pixelmap,			true, "create a map of dead pixel", true);

  vector<string> threshold_names;
  m_histo_type_chi2 = "SCURVE_CHI2";
  m_histo_type_mean = "SCURVE_MEAN";
  m_histo_type_sigma = "SCURVE_SIGMA";
  threshold_names.push_back(m_histo_type_chi2);  threshold_names.push_back(m_histo_type_mean);  threshold_names.push_back(m_histo_type_sigma);
  m_scan_histo_names[""] = threshold_names; // For analysis with only one scan, scan name doesn't matter
  
  std::cout << "AutomaticINTIMETHRAnalysis:   constructed." << std::endl;
}

void AutomaticINTIMETHRAnalysis::requestScanConfigs(IAnalysisDataServer *ana_data_server)
{
  ana_data_server->getDataServer()->requestScanConfig();
}

void AutomaticINTIMETHRAnalysis::requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name)
{
  ana_data_server->getDataServer()->requestModuleConfig(module_name);
  std::cout<<"AutomaticINTIMETHRAnalysis:: requesting configuration for module "<<module_name<<std::endl;
}

void AutomaticINTIMETHRAnalysis::getBasics(IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos)
{
  m_newInput->pp0Name = m_pp0_name;
  m_newInput->moduleName = getModuleName( ana_data_server, module_iter, pos);
}

void AutomaticINTIMETHRAnalysis::getScanConfigs(PixLib::Config&   scan_config)
{
  cout << "AutomaticINTIMETHRAnalysis::getScanConfigs   at start." << endl;
  float runMS = confVal<int>(const_cast< PixLib::Config &>(scan_config)["general"]["maskStageSteps"]);
  float totMS = converter.getTotalMaskStageSteps(confVal<std::string>(const_cast< PixLib::Config &>(scan_config)["general"]["maskStageTotalSteps"]));
  
  m_newInput->nMaskStages = (int)runMS;
  m_newInput->nPixel = 92160*(int)(runMS / totMS);
  
  string speed = confVal<std::string>(scan_config["mcc"]["mccBandwidth"]);
  //float zaehler = confVal<int>(scan_config["general"]["maskStageSteps"]);
  //float nenner = converter.getTotalMaskStageSteps(confVal<std::string>(scan_config["general"]["maskStageTotalSteps"]));
  //if(nenner!=0) m_newInput->nPixel = 92160*int(zaehler/nenner);
  
  if(speed=="SINGLE_40" || speed=="DOUBLE_40") m_newInput->MBs=40;
  else if(speed=="SINGLE_80" || speed=="DOUBLE_80") m_newInput->MBs=80;
  else m_newInput->MBs=-1; //should not happen
}

void AutomaticINTIMETHRAnalysis::getModuleConfigs(PixLib::Config& module_config)
{
  cout << "AutomaticINTIMETHRAnalysis::getModuleConfigs   at start." << endl;
  getDisabledFECP (module_config, m_feflv , m_newInput->fe_disable, m_newInput->cp_enable);
}

void AutomaticINTIMETHRAnalysis::getHistograms(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos) 
{
  const TH2 *h_chi2, *h_mean, *h_sigma;
  if(m_CTdata)
    {
      DataServerCT * dataServerCT = dynamic_cast<DataServerCT*>(ana_data_server->getDataServer());
      
      h_chi2  = converter.GetTheHisto2D_old(*iterator(dataServerCT->getHistograms( m_histo_type_chi2, m_pp0_name).begin(),pos),
					    *iterator(dataServerCT->getScanConfigRefs( m_histo_type_chi2, m_pp0_name).begin(),pos),  m_histo_type_chi2, -1,0,0,0);
      h_mean  = converter.GetTheHisto2D_old(*iterator(dataServerCT->getHistograms( m_histo_type_mean, m_pp0_name).begin(),pos),
					    *iterator(dataServerCT->getScanConfigRefs( m_histo_type_mean, m_pp0_name).begin(),pos),  m_histo_type_mean, -1,0,0,0);
      h_sigma = converter.GetTheHisto2D_old(*iterator(dataServerCT->getHistograms( m_histo_type_sigma, m_pp0_name).begin(),pos),
					    *iterator(dataServerCT->getScanConfigRefs( m_histo_type_sigma, m_pp0_name).begin(),pos),  m_histo_type_sigma, -1,0,0,0);
    }
  else
    {
      string conn_name = PixA::connectivityName(module_iter);
      IDataServer* data_server = ana_data_server->getDataServer();
      h_chi2  = converter.GetTheHisto2D( m_histo_type_chi2, data_server, conn_name, -1,0,0,0);
      h_mean  = converter.GetTheHisto2D( m_histo_type_mean, data_server, conn_name, -1,0,0,0);
      h_sigma = converter.GetTheHisto2D( m_histo_type_sigma,data_server, conn_name, -1,0,0,0);
    }

  if(h_chi2!=NULL) m_newInput->histo_chi2  = h_chi2; 
  else cout << "chi2 histogram is not found" << endl;
  if(h_mean!=NULL) m_newInput->histo_mean  = h_mean;
  else cout << " mean histogram is not found" << endl;
  if(h_sigma!=NULL) m_newInput->histo_sigma = h_sigma;
  else cout << " sigma histogram is not found" << endl;

  PixGeometry geo(PixA::nRows(h_mean),PixA::nColumns(h_mean)); 
  m_feflv = geo.pixType();
  std::cout << "FE flavor: " << m_feflv << std::endl;
}

void AutomaticINTIMETHRAnalysis::saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result2) 
{
  ResultINTIMETHR* module_result = static_cast<ResultINTIMETHR*>(module_result2);
  unique_ptr<ROOTanalysisHelper> helperNoise(new ROOTanalysisHelper(4,true));	// helperNoise initialized to use 4 pixel classes
  unique_ptr<ROOTanalysisHelper> helperThr(new ROOTanalysisHelper(3,true));	// helperThr initialized to use 3 pixel classes
  
  // FEs results
  analysis_results.addValue<float>("number of good FEs",module_result->moduleName.Data(),module_result->numGoodFEs);
  analysis_results.addValue<bool>("passed FE0",module_result->moduleName.Data(),module_result->passed_FE_cuts[0]);
  analysis_results.addValue<bool>("passed FE01",module_result->moduleName.Data(),module_result->passed_FE_cuts[1]);
  analysis_results.addValue<bool>("passed FE02",module_result->moduleName.Data(),module_result->passed_FE_cuts[2]);
  analysis_results.addValue<bool>("passed FE03",module_result->moduleName.Data(),module_result->passed_FE_cuts[3]);
  analysis_results.addValue<bool>("passed FE04",module_result->moduleName.Data(),module_result->passed_FE_cuts[4]);
  analysis_results.addValue<bool>("passed FE05",module_result->moduleName.Data(),module_result->passed_FE_cuts[5]);
  analysis_results.addValue<bool>("passed FE06",module_result->moduleName.Data(),module_result->passed_FE_cuts[6]);
  analysis_results.addValue<bool>("passed FE07",module_result->moduleName.Data(),module_result->passed_FE_cuts[7]);
  analysis_results.addValue<bool>("passed FE08",module_result->moduleName.Data(),module_result->passed_FE_cuts[8]);
  analysis_results.addValue<bool>("passed FE09",module_result->moduleName.Data(),module_result->passed_FE_cuts[9]);
  analysis_results.addValue<bool>("passed FE10",module_result->moduleName.Data(),module_result->passed_FE_cuts[10]);
  analysis_results.addValue<bool>("passed FE11",module_result->moduleName.Data(),module_result->passed_FE_cuts[11]);
  analysis_results.addValue<bool>("passed FE12",module_result->moduleName.Data(),module_result->passed_FE_cuts[12]);
  analysis_results.addValue<bool>("passed FE13",module_result->moduleName.Data(),module_result->passed_FE_cuts[13]);
  analysis_results.addValue<bool>("passed FE14",module_result->moduleName.Data(),module_result->passed_FE_cuts[14]);
  analysis_results.addValue<bool>("passed FE15",module_result->moduleName.Data(),module_result->passed_FE_cuts[15]);
  
  // Whole module results
  analysis_results.addValue<bool>("passed all FE bad pixel cuts",module_result->moduleName.Data(),module_result->passed_all_fe_bad_pixel);
  analysis_results.addValue<bool>("passed all FE noise cuts",module_result->moduleName.Data(),module_result->passed_all_fe_noise);
  analysis_results.addValue<bool>("passed all FE threshold mean cuts",module_result->moduleName.Data(),module_result->passed_all_fe_threshold_mean);
  analysis_results.addValue<bool>("passed all FE threshold RMS cuts",module_result->moduleName.Data(),module_result->passed_all_fe_threshold_RMS);
  analysis_results.addValue<bool>("passed module threshold mean cuts",module_result->moduleName.Data(), true); //module_result->passed_module_threshold_mean);
  analysis_results.addValue<bool>("passed module threshold RMS cuts",module_result->moduleName.Data(), true); //module_result->passed_module_threshold_RMS);
  analysis_results.addValue<bool>("passed module noise cuts",module_result->moduleName.Data(),true); //module_result->passed_module_noise);

  // Whole module variables
  for(int pixtype=0; pixtype<helperThr->NpixelClasses; pixtype++)
    {
      string type(helperThr->getPixelClassString(pixtype+1,helperThr->NpixelClasses));
      analysis_results.addValue<float>("average threshold for "+type+" pixel",module_result->moduleName.Data(),module_result->average_threshold[pixtype]);
      analysis_results.addValue<float>("threshold RMS for "+type+" pixel",module_result->moduleName.Data(),module_result->RMS_threshold[pixtype]);
    }
  for(int pixtype=0; pixtype<helperNoise->NpixelClasses; pixtype++)
    {
      string type(helperNoise->getPixelClassString(pixtype+1,helperNoise->NpixelClasses));
      analysis_results.addValue<float>("average noise for "+type+" pixel",module_result->moduleName.Data(),module_result->average_noise[pixtype]);
      analysis_results.addValue<float>("noise RMS for "+type+" pixel",module_result->moduleName.Data(),module_result->RMS_noise[pixtype]);
    }
  
  analysis_results.addValue<float>("number of bad pixels",module_result->moduleName.Data(),module_result->numBadPixels);
  analysis_results.addValue<float>("number of disconnected pixels",module_result->moduleName.Data(),module_result->numDisconnectedPixels);
  
  // Pixel Map
  if(module_result->MakePixelMap) // checks if this was enabled in analysis configuration
    analysis_results.addValue<PixelMap_t>("bad pixel map",module_result->moduleName.Data(),module_result->badPixelMap);
}
}
