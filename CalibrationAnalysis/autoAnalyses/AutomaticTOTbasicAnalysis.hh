#ifndef _CAN_AutomaticTOTbasicAnalysis_hh_
#define _CAN_AutomaticTOTbasicAnalysis_hh_

#include "AutomaticAnalysisBase.hh"
#include "TOTbasicAnalysisROOT.h"
#include "coreAnalyses/analysisInput_TOTbasic.h"
#include "coreAnalyses/analysisOutput_TOTbasic.h"



namespace CAN {

  /** 
   * Analysis for TOT basic scans, for one pp0 
   */
  class AutomaticTOTbasicAnalysis : public AutomaticAnalysisBase 
  {
  public:
    AutomaticTOTbasicAnalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &);
    ~AutomaticTOTbasicAnalysis() {};

    void requestHistograms(IAnalysisDataServer *data_server, const PixA::Pp0LocationBase &pp0_location);
   
  private:
    void newAnalysisInput() { m_newInput = new AnalysisInputTOTbasic(); };
    void addNewAnalysisInput(AnalysisInput_t analysisInput) {analysisInput->Add(m_newInput);};
    void requestScanConfigs(IAnalysisDataServer *ana_data_server);
    void requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name);
    void getBasics(IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos);
    void getScanConfigs(PixLib::Config& scan_config);
    void getModuleConfigs(PixLib::Config& module_config);
    void getHistograms(IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos);
    void saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result);

  private:
    std::string m_histo_type[4]; 
    int m_scanType;
    AnalysisInputTOTbasic* m_newInput;
  };

}
#endif

