#include "AutomaticTDACFASTTUNEanalysis.hh"


namespace CAN {
  
  AutomaticTDACFASTTUNEanalysis::AutomaticTDACFASTTUNEanalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &oi)
    : AutomaticAnalysisBase(analysis_serial_number, oi)
  {
    m_name = "TDACFASTTUNEanalysis";
    m_config = PixLib::Config("TDACFASTTUNEanalysis");
    m_analysis = unique_ptr<TDACFASTTUNEanalysisROOT>(new TDACFASTTUNEanalysisROOT);
    TDACFASTTUNEanalysisROOT* anptr = static_cast<TDACFASTTUNEanalysisROOT*>(m_analysis.get());
    

    m_config.addGroup("general");
    PixLib::ConfGroup &group = m_config["general"];
    
    // more variables can be added here
    group.addFloat("TDACmean_lowerCut", anptr->TDACmean_lowerCut, 50, "TDACmean_lowerCut", true);
    group.addFloat("TDACmean_uppercut", anptr->TDACmean_upperCut, 65, "TDACmean_upperCut", true);
    group.addFloat("TDAC_toolow", anptr->TDAC_toolow, 10, "defines low TDAC for next cut", true);
    group.addFloat("Percentage_lowTDAC", anptr->Percentage_lowTDAC, 10, "Allowed percentage of pixel with low TDAC", true);
    group.addFloat("TDAC_toohigh", anptr->TDAC_toohigh, 110, "defines high TDAC for next cut", true);
    group.addFloat("Percentage_highTDAC", anptr->Percentage_highTDAC, 10, "Allowed percentage of pixel with high TDAC" , true);
    group.addFloat("Mean_lowerCut", anptr->Mean_lowerCut, 45, "Mean_lowerCut", true);
    group.addFloat("Mean_upperCut", anptr->Mean_upperCut, 55, "Mean_upperCut", true);
    group.addFloat("RMS_upperCut", anptr->RMS_upperCut, 18, "RMS_upperCut", true);
    group.addFloat("Pixel_w_no_hits", anptr->Pixel_w_no_hits, 100, "Max. number of pixel seeing no hits", true);
    group.addFloat("Pixel_w_all_hits", anptr->Pixel_w_all_hits, 100, "Max. number of pixel seeing all hits", true);
    group.addFloat("max_chi2", anptr->max_chi2, 3, "max_chi2", true);

  
    vector<string> tdactune_names;
    m_histo_type_occ = "OCCUPANCY";
    m_histo_type_tdac = "TDAC_THR";
    tdactune_names.push_back(m_histo_type_occ);
    tdactune_names.push_back(m_histo_type_tdac);
    m_scan_histo_names[""] = tdactune_names;

    std::cout<<"AutomaticTDACFASTTUNEanalysis: constructed."<<std::endl;

  }

  void AutomaticTDACFASTTUNEanalysis::requestScanConfigs(IAnalysisDataServer* ana_data_server)
  {
    ana_data_server->getDataServer()->requestScanConfig();
  }

  void AutomaticTDACFASTTUNEanalysis::requestModuleConfigs(IAnalysisDataServer* ana_data_server, string module_name)
  {
  }

  void AutomaticTDACFASTTUNEanalysis::getBasics(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos)
  {
    m_newInput->pp0Name = m_pp0_name;
    m_newInput->moduleName = getModuleName(ana_data_server, module_iter, pos);
  }

  void AutomaticTDACFASTTUNEanalysis::getScanConfigs(PixLib::Config& scan_config)
  {
    m_newInput->Nmasks = confVal<int>(scan_config["general"]["maskStageSteps"]);
    m_newInput->Nevents = confVal<int>(scan_config["general"]["repetitions"]);
    
    m_Ntdacsteps = confVal<int>(scan_config["loops"]["loopVarNStepsLoop_0"]);
    m_newInput->Ntdacsteps = m_Ntdacsteps;
    
    std::string speed = confVal<std::string>(scan_config["mcc"]["mccBandwidth"]);
    if(speed == "SINGLE_40" || speed == "DOUBLE_40") m_newInput->MBs=40;
    else if(speed == "SINGLE_80" || speed == "DOUBLE_80") m_newInput->MBs=80;
    else m_newInput->MBs=-1; // should not happen

    m_newInput->axis = *converter.GetTheAxis(scan_config);
  }

  void AutomaticTDACFASTTUNEanalysis::getModuleConfigs(PixLib::Config& module_config)
  {
  }


  void AutomaticTDACFASTTUNEanalysis::getHistograms(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos)
  {
    const TH2* h_occ=NULL;
    const TH2* h_tdac=NULL;
    
    //loop over TDAC steps & get occupancy histograms for each step & the one TDAC histogram
    

    for(int step = 0; step < m_Ntdacsteps; step++)
      {
	if(m_CTdata)
	  {
	    DataServerCT* dataServerCT = dynamic_cast<DataServerCT*>(ana_data_server->getDataServer());
	    
	    h_occ = converter.GetTheHisto2D_old(*iterator(dataServerCT->getHistograms(m_histo_type_occ, m_pp0_name).begin(),pos),*iterator(dataServerCT->getScanConfigRefs(m_histo_type_occ, m_pp0_name).begin(),pos), m_histo_type_occ, 0,0,step,0);
	    
	    if(step == m_Ntdacsteps-1) h_tdac= converter.GetTheHisto2D_old(*iterator(dataServerCT->getHistograms(m_histo_type_tdac, m_pp0_name).begin(),pos),*iterator(dataServerCT->getScanConfigRefs(m_histo_type_tdac, m_pp0_name).begin(),pos), m_histo_type_tdac,0,0,0,0);
	    
	    cout<<"CTdata is there"<<endl;
	  }
	else
	  {
	    string conn_name = PixA::connectivityName(module_iter);
	    IDataServer* data_server = ana_data_server->getDataServer();
	    h_occ = converter.GetTheHisto2D(m_histo_type_occ, data_server, conn_name, 0,0,step,0);
	    if(step == m_Ntdacsteps-1) h_tdac = converter.GetTheHisto2D(m_histo_type_tdac, data_server, conn_name, 0,0,0,0); // request only once
	  }
	if(h_occ!=NULL)
	  {
	    m_newInput->histo_occ.push_back(h_occ);
	  }
	else cout<<"something went wrong, occupancy histogram is empty"<<endl;
      }
    // loop over tdac steps ends here
    
    if(h_tdac!=NULL)
      {
	m_newInput->histo_tdac = h_tdac;
      }
    else cout<<"something went wrong, tdac histogram is empty"<<endl;
  }
  

  void AutomaticTDACFASTTUNEanalysis::saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result2)
  {
    ResultTDACFASTTUNE* module_result = static_cast<ResultTDACFASTTUNE*>(module_result2);
    unique_ptr<ROOTanalysisHelper> helper(new ROOTanalysisHelper());
    
    
    analysis_results.addValue<float>("MeanPercentage", module_result->moduleName.Data(), module_result->MeanPercentage);
    analysis_results.addValue<float>("RMSPercentage", module_result->moduleName.Data(), module_result->RMSPercentage);
    analysis_results.addValue<float>("Chi2", module_result->moduleName.Data(), module_result->Chi2);
    analysis_results.addValue<float>("MeanTDAC", module_result->moduleName.Data(), module_result->MeanTDAC);
    analysis_results.addValue<bool>("passedMean", module_result->moduleName.Data(), module_result->passedMean);
    analysis_results.addValue<bool>("passedRMS", module_result->moduleName.Data(), module_result->passedRMS);
    analysis_results.addValue<bool>("passedChi2", module_result->moduleName.Data(), module_result->passedChi2);
    analysis_results.addValue<bool>("passedTDACs", module_result->moduleName.Data(), module_result->passedTDACs);

    for(int step = 0; step < m_Ntdacsteps; step++){
      std::string varname = "extra Hits in tuning step "+ std::to_string(step+1);
      analysis_results.addValue<float>(varname, module_result->moduleName.Data(), module_result->vec_extraHits[step]);
    }
  }



}




 
