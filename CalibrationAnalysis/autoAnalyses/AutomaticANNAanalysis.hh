#ifndef _CAN_AutomaticANNAanalysis_hh_
#define _CAN_AutomaticANNAanalysis_hh_

#include "AutomaticAnalysisBase.hh"
#include "ANNAanalysisROOT.h"
#include "coreAnalyses/analysisInput_ANNA.h"
#include "coreAnalyses/analysisOutput_ANNA.h"



namespace CAN {

  /** 
   * Analysis for DIGITAL scans, for one pp0 
   */
  class AutomaticANNAanalysis : public AutomaticAnalysisBase 
  {
  public:
    AutomaticANNAanalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &);
    ~AutomaticANNAanalysis() {};
   
  private:
    void newAnalysisInput() { m_newInput = new AnalysisInputANNA(); };
    void addNewAnalysisInput(AnalysisInput_t analysisInput) {analysisInput->Add(m_newInput);};
    void requestScanConfigs(IAnalysisDataServer *ana_data_server);
    void requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name);
    void getBasics(IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos);
    void getScanConfigs(PixLib::Config& scan_config);
    void getModuleConfigs(PixLib::Config& module_config);
    void getHistograms(IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos);
    void saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result);


  private:
    std::string m_histo_type; 
    AnalysisInputANNA* m_newInput;
  };

}
#endif

