#ifndef _CAN_AutomaticDIFFAnalysis_hh_
#define _CAN_AutomaticDIFFAnalysis_hh_

#include "AutomaticAnalysisBase.hh"
#include "DIFFanalysisROOT.h"
#include "coreAnalyses/analysisInput_DIFF.h"
#include "coreAnalyses/analysisOutput_DIFF.h"
#include "ExtendedAnalysisResultList_t.hh"


namespace CAN {

  /** 
   * Analysis for DIFF scans, for one pp0 
   */
  class AutomaticDIFFAnalysis : public AutomaticAnalysisBase 
  {
  public:
    AutomaticDIFFAnalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &);
    ~AutomaticDIFFAnalysis() {};

  private:
    void newAnalysisInput() { m_newInput = new AnalysisInputDIFF(); };
    void addNewAnalysisInput(AnalysisInput_t analysisInput) { analysisInput->Add(m_newInput); };  
    ////
    void requestScanConfigs  (IAnalysisDataServer *ana_data_server); 
    void requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name);
    void getBasics       (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos); 
    void getScanConfigs  (PixLib::Config&   scan_config);
    void getModuleConfigs(PixLib::Config& module_config);
    void getHistograms   (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos);
    void saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result);
    void fillNamedScans();

  private:
    string                  m_scanA;
    string                  m_scanB;
    AnalysisInputDIFF*  m_newInput;
  };

}
#endif

