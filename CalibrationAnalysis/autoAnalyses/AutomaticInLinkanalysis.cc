#include "AutomaticInLinkanalysis.hh"
#include "PixConnectivity/OBConnectivity.h"
#include <ExtendedAnalysisResultList_t.hh>
#include <stdlib.h>//srand
#include <time.h>  //time

namespace CAN {

  AutomaticInLinkanalysis::AutomaticInLinkanalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &oi)  
    : AutomaticAnalysisBaseROD( analysis_serial_number, oi) 
  {
    m_name="InLinkanalysis";
    m_config=PixLib::Config("InLinkanalysis");
    m_analysis = unique_ptr<InLinkanalysisROOT>(new InLinkanalysisROOT);
    
    m_config.addGroup("general");
    PixLib::ConfGroup &group=m_config["general"];
    //
    group.addFloat("cut_I_pin_mod_min(uA) InLink", (static_cast<InLinkanalysisROOT*>(m_analysis.get()))->cut_I_pin_mod_min, 40, "cut_I_pin_mod_min (uA)", true);
    group.addFloat("cut_I_pin_mod_max(uA) InLink", (static_cast<InLinkanalysisROOT*>(m_analysis.get()))->cut_I_pin_mod_max, 2000, "cut_I_pin_mod_max (uA)", true);
    group.addFloat("cut_I_pin_Pp0_min(uA) InLink", (static_cast<InLinkanalysisROOT*>(m_analysis.get()))->cut_I_pin_Pp0_min,  2000, "cut_I_pin_Pp0_min (uA)", true);
    group.addFloat("cut_I_pin_Pp0_max(uA) InLink", (static_cast<InLinkanalysisROOT*>(m_analysis.get()))->cut_I_pin_Pp0_max,  8000, "cut_I_pin_Pp0_max (uA)", true);

    
    vector<string> threshold_names;
    m_histo_type_inlink = "INLINKMAP";//"HistoSummary";
    
    threshold_names.push_back(m_histo_type_inlink);
    m_scan_histo_name = threshold_names[0]; // For analysis with only one scan, scan name doesn't matter

    std::cout << "AutomaticInLinkanalysis:   constructed." << std::endl;

    //initialization
    global_ROD_name ="";
    m_Status_AllChannel=true;
    m_Status_AllPp0=true;
  }


  void AutomaticInLinkanalysis::requestScanConfigs(IAnalysisDataServer *ana_data_server) 
  {
    ana_data_server->getDataServer()->requestScanConfig();
  }

  void AutomaticInLinkanalysis::requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name) 
  {
    ana_data_server->getDataServer()->requestModuleConfig(module_name);
    std::cout<<"AutomaticInlinkanalysis:: requesting configuration for module "<<module_name<<std::endl;
  }

  void AutomaticInLinkanalysis::getBasics(IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter) 
  {
    //Initialize ROD when getting new modules 

    if(global_ROD_name!=m_ROD_name){
       global_ROD_name = m_ROD_name;
       m_Status_AllChannel=true;
    }

    m_newInput->rodName = m_ROD_name;
    m_newInput->pp0Name = m_pp0_name;
    // m_newInput->moduleID   = getModuleId  ( ana_data_server, module_iter, pos);  
    m_newInput->moduleName = const_cast<PixLib::ModuleConnectivity*>(*module_iter)->name();
    m_newInput->moduleId   = const_cast<PixLib::ModuleConnectivity*>(*module_iter)->modId;
    m_newInput->inLink     = const_cast<PixLib::ModuleConnectivity*>(*module_iter)->inLink();
    m_newInput->outLink1A  = const_cast<PixLib::ModuleConnectivity*>(*module_iter)->outLink1A(PixLib::PixScan::SINGLE_40); //PixLib/PixModuleGroup/PixModuleGroup.h defined enum 
    m_newInput->outLink2A  = const_cast<PixLib::ModuleConnectivity*>(*module_iter)->outLink2A(PixLib::PixScan::SINGLE_40);
    m_newInput->bocLinkTx  = const_cast<PixLib::ModuleConnectivity*>(*module_iter)->bocLinkTx(); // 10 * TX board + TX channel (0-7)

    cout <<"INFO AutomaticInLinkanalysis::getBasics "
         <<" moduleName " 
         << m_newInput->rodName <<" "<<m_newInput->moduleName 
         <<" modid " << m_newInput->moduleId
         <<" bocTxinLink "<< m_newInput->bocLinkTx 
         <<" inLink "<< m_newInput->inLink <<endl;

  }


  void AutomaticInLinkanalysis::getScanConfigs(PixLib::Config&   scan_config) 
  {
/*
 inLink scan doesn't store information in the ScanConfigs
*/
  }

 
  void AutomaticInLinkanalysis::getModuleConfigs(PixLib::Config& module_config) 
  {
  }


  void AutomaticInLinkanalysis::getHistograms(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter) 
  {
    global_ROD_name = m_ROD_name;
    string conn_name = m_ROD_name;
    IDataServer* data_server = ana_data_server->getDataServer();

    const TH2* h_InLink =  converter.GetTheHisto2D( m_histo_type_inlink, data_server, conn_name, 0,0,0,0);
    if(h_InLink){
      global_histo_InLink = h_InLink;
      m_newInput->histo_InLink  = h_InLink;
      cout <<"Get Histo "<< global_histo_InLink->GetName()<<endl;
    }else{
      global_histo_InLink = NULL;
      m_newInput->histo_InLink = NULL;
      cerr <<"ERROR AutomaticInLinkanalysis::getHistograms: Can't find histogram: " << m_histo_type_inlink
           <<" with conn_name: " << conn_name 
           <<" for module: "<<PixA::connectivityName( module_iter )
           <<endl;
    }
    
  }
  void AutomaticInLinkanalysis::analyseResultsPerROD(IAnalysisDataServer *ana_data_server, ExtendedAnalysisResultList_t &analysis_results)
  {
    if(!global_histo_InLink) return;
    analysis_results.addValue<bool>("Status_ch", m_ROD_name, m_Status_AllChannel); 
    analysis_results.addValue<bool>("Status_Pp0", m_ROD_name, m_Status_AllPp0);

  }

  //append 8 TX plugin channels per PP0, iterate all pp0 per ROD
  //the last channel has no electrical connection on PP0, 
  //there are four PP0s for L1, L2 and two pp0s for B-layer and disk on each ROD
  void AutomaticInLinkanalysis::analyseResultsPerPP0(IAnalysisDataServer *ana_data_server, ExtendedAnalysisResultList_t &analysis_results, PixA::Pp0List::const_iterator pp0_iter)
  {
  //currently disable publishing PP0 fiber channel PIN current since it generates too many connections to analysis result DB.
  //Let's try to add it back when we have a better understanding.
   
    if(!global_histo_InLink) return;
	
    PixLib::OBConnectivity* ob=  const_cast<PixLib::Pp0Connectivity*>(*pp0_iter)->ob();//PixLib/PixConnectivity/Pp0Connectivity.h
    int bocTxSlot=ob->bocTxSlot();//PixLib/PixConnectivity/OBConnectivity.h
    string pp0name = const_cast<PixLib::Pp0Connectivity*>(*pp0_iter)->name();

    unsigned int iseed = (unsigned int) time(NULL);
    srand(iseed);
    int sleep_time = (int)(1000*rand()/(double)RAND_MAX); 
    cout <<"AutomaticInLinkanalysis::analyseResultsPerPP0: " << m_ROD_name <<" "<< pp0name << " bocTxSlot "<<bocTxSlot <<" sleep "<< sleep_time; 
    char varName[30];

    double val=global_histo_InLink->GetBinContent(bocTxSlot*8+1,1);
    printf(" %8.1f ",val);
    	
    sprintf(varName, "I_pin_base");
    analysis_results.addValue<float>(varName , pp0name , val);

    sprintf(varName, "I_pin_ped");
    val = global_histo_InLink->GetBinContent(1,1);
    printf(" ped %8.1f ",val);
    analysis_results.addValue<float>(varName , pp0name , val);
	 
    for(int ic=0;ic<8;ic++){ 
      int inX=bocTxSlot*8+ic+1;
      val=global_histo_InLink->GetBinContent(inX,3);
      printf(" %8.1f ",val);
      
      sprintf(varName, "I_pin_ch%d",ic);
      analysis_results.addValue<float>(varName , pp0name , val );
    }
    printf("\n");

    double cut_I_pin_mod_min =(static_cast<InLinkanalysisROOT*>(m_analysis.get()))->cut_I_pin_mod_min;
    double cut_I_pin_mod_max =(static_cast<InLinkanalysisROOT*>(m_analysis.get()))->cut_I_pin_mod_max;
    bool comb_status =true;
    for(int ic=0;ic<7;ic++){
      int inX=bocTxSlot*8+ic+1;
      double val2=global_histo_InLink->GetBinContent(inX,3);
      if(val2< cut_I_pin_mod_min || val2> cut_I_pin_mod_max ) comb_status = false; 
    }
    
    m_Status_AllChannel &= comb_status;  
    analysis_results.addValue<bool>("Status_ch", pp0name, comb_status);    
    cout <<"Status based on Tx channel: " << m_ROD_name <<" ("<< m_Status_AllChannel<<") "<< pp0name <<" ("<<comb_status<<")"<<endl;  

    //Pp0 Pin current when all channels are on. It is the same for all channels in the same PP0 
    double I_pin_Pp0 = global_histo_InLink->GetBinContent(bocTxSlot*8+1,6);
    double cut_I_pin_Pp0_min =(static_cast<InLinkanalysisROOT*>(m_analysis.get()))->cut_I_pin_Pp0_min;
    double cut_I_pin_Pp0_max =(static_cast<InLinkanalysisROOT*>(m_analysis.get()))->cut_I_pin_Pp0_max;
    comb_status =true;
    if (I_pin_Pp0 < cut_I_pin_Pp0_min || I_pin_Pp0> cut_I_pin_Pp0_max ) comb_status = false;
    analysis_results.addValue<float>("I_pin_Pp0", pp0name, I_pin_Pp0);
    analysis_results.addValue<bool>("Status_Pp0", pp0name, comb_status);
    m_Status_AllPp0    &= comb_status;
  
 
  }


  void AutomaticInLinkanalysis::saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result2) 
  {
    if(!global_histo_InLink) return;

    ResultInLink* module_result = static_cast<ResultInLink*>(module_result2);
    //unique_ptr<ROOTanalysisHelper> helper(new ROOTanalysisHelper());
    //analysis_results.addValue<bool>("passed threshold mean max cut",module_result->moduleName.Data(),module_result->passed_threshold_mean_max);
    //
    analysis_results.addValue<float>("I_pin_mod",module_result->moduleName.Data(),module_result->I_pin_mod);
    analysis_results.addValue<bool>("Status_Pp0",module_result->moduleName.Data(),module_result->passed);
    analysis_results.addValue<bool>("Status_ch" ,module_result->moduleName.Data(),module_result->passed);
    //analysis_results.addValue<float>("ratio bits",module_result->moduleName.Data(),module_result->ratiobits);
    //
    
    
  }
}
