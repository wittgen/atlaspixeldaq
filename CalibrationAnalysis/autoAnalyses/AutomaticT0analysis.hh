#ifndef _CAN_AutomaticT0analysis_hh_
#define _CAN_AutomaticT0analysis_hh_

#include "AutomaticAnalysisBase.hh"
#include "T0analysisROOT.h"
#include "coreAnalyses/analysisInput_T0.h"
#include "coreAnalyses/analysisOutput_T0.h"
#include "ConfigWrapper/ConfObjUtil.h"
#include <PixModule/PixModule.h>
#include "PixMcc/PixMcc.h"


namespace CAN {
  class AutomaticT0analysis : public AutomaticAnalysisBase 
  {
  public:
    AutomaticT0analysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &);
    ~AutomaticT0analysis() {};

  private:
    void newAnalysisInput() { m_newInput = new AnalysisInputT0(); };
    void addNewAnalysisInput(AnalysisInput_t analysisInput) { analysisInput->Add(m_newInput); };  
    ////
    void requestScanConfigs  (IAnalysisDataServer *ana_data_server); 
    void requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name);
    void getBasics       (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos); 
    void getScanConfigs  (PixLib::Config&   scan_config);
    void getModuleConfigs(PixLib::Config& module_config);
    void getHistograms   (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos);
    void saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result);
    void getDelaySteps(const PixLib::Config &scan_config);

  private:

    AnalysisInputT0*  m_newInput;
    std::string m_histo_type;
    std::map<std::string,std::shared_ptr<PixLib::PixModuleData_t> > m_newModuleCfg;
    int m_delRangeSteps;
    int m_delSteps;
    int m_delStepIndex;
    int m_delRangeIndex;
  };

}
#endif

