#include "AutomaticDIFFAnalysis.hh"
#include "../Lock.hh"
#include "../PixDbGlobalMutex.hh"


namespace CAN {

  AutomaticDIFFAnalysis::AutomaticDIFFAnalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &oi)
    : AutomaticAnalysisBase( analysis_serial_number, oi)
  {
    m_name="DIFFanalysis";
    m_config=PixLib::Config("DIFFanalysis");
    m_analysis = unique_ptr<DIFFanalysisROOT>(new DIFFanalysisROOT);
 
    m_config.addGroup("general");
    PixLib::ConfGroup &group=m_config["general"];
    group.addString("ScanName1", m_scanA, "a","scanName1", true);
    group.addString("ScanName2", m_scanB, "b","scanName2", true);

  }

  void AutomaticDIFFAnalysis::fillNamedScans(){

    vector<string> threshold_names;
    threshold_names.push_back((string)"All");
    m_scan_histo_names[m_scanA] = threshold_names;   
    m_scan_histo_names[m_scanB] = threshold_names;
  }
  void AutomaticDIFFAnalysis::requestScanConfigs(IAnalysisDataServer *ana_data_server) 
  {
    ana_data_server->getDataServer()->requestScanConfig();
  }

  void AutomaticDIFFAnalysis::requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name) 
  {
  }

  void AutomaticDIFFAnalysis::getBasics(IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos) 
  {
    m_newInput->pp0Name = m_pp0_name;
    m_newInput->moduleName = getModuleName( ana_data_server, module_iter, pos);  
  }


  void AutomaticDIFFAnalysis::getScanConfigs(PixLib::Config&   scan_config) 
  {
    unique_ptr<ROOTanalysisHelper> helper(new ROOTanalysisHelper());

    float runMS = confVal<int>(const_cast< PixLib::Config &>(scan_config)["general"]["maskStageSteps"]);
    float totMS = converter.getTotalMaskStageSteps(confVal<std::string>(const_cast< PixLib::Config &>(scan_config)["general"]["maskStageTotalSteps"]));
    m_newInput->nMaskStages = (int)runMS;
    m_newInput->nPixel = (int)(helper->Ncols * helper->Nrows * runMS / totMS);
    std::cout << "Scanned " << runMS << " mask stages out of " << totMS << " = " << m_newInput->nPixel << " pixels out of " << helper->Ncols * helper->Nrows << std::endl;
  }
  
 
  void AutomaticDIFFAnalysis::getModuleConfigs(PixLib::Config& module_config) 
  {
  }


  void AutomaticDIFFAnalysis::getHistograms(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos) 
  {
    CAN::Lock lock( CAN::PixDbGlobalMutex::mutex() );

    string conn_name = PixA::connectivityName( module_iter );  
    //  index.push_back((int)-1);

    HistoInfo_t info;

    std::map< std::string, std::pair<std::shared_ptr<TH1F>,std::shared_ptr<TH1F> > > h_1d_histos;
    std::map< std::string, std::pair<std::shared_ptr<TH2F>,std::shared_ptr<TH2F> > > h_2d_histos;
    
    std::vector<std::string> scanA_histo_names = ana_data_server->getDataServer(m_scanA)->dataServerHistos(conn_name);
    std::vector<std::string> scanB_histo_names = ana_data_server->getDataServer(m_scanB)->dataServerHistos(conn_name);
    std::vector<std::string>::iterator scanA_names_iter;
    std::vector<std::string>::iterator scanB_names_iter;

    for (scanA_names_iter = scanA_histo_names.begin(); scanA_names_iter != scanA_histo_names.end(); scanA_names_iter++){
      for (scanB_names_iter = scanB_histo_names.begin(); scanB_names_iter != scanB_histo_names.end(); scanB_names_iter++){
	if ((*scanA_names_iter) == (*scanB_names_iter) && (*scanA_names_iter) != "All"){
	  ana_data_server->getDataServer(m_scanA)->numberOfHistograms((*scanA_names_iter),conn_name, info);
	  PixA::Index_t index;
	  std::string index_name;
	  index.clear();
	  unsigned int i[4];
	  i[0] = 0;
	  i[1] = 0;
	  i[2] = 0; 
	  i[3] = 0; 
	  bool finished = false;
	  while (!finished){	    
	    index.clear();
	    std::stringstream ss;
	    ss << "_";
	    for(int ii = 0 ; ii < 4 ; ii++){
	      if (info.maxIndex(ii) != 0){
		index.push_back(i[ii]);
		ss << i[ii];
	      }
	    }
	    ss >> index_name;
	    const Histo* histo_A( ana_data_server->getDataServer(m_scanA)->getHistogram((*scanA_names_iter), conn_name, index) );
	    const Histo* histo_B( ana_data_server->getDataServer(m_scanB)->getHistogram((*scanA_names_iter), conn_name, index) );
	    if (histo_A != NULL && histo_B != NULL){
	      if (histo_A->GetDimension() == histo_B->GetDimension()){
		std::cout<<"Matched "<<(*scanA_names_iter)<<std::endl;
		if (histo_A->GetDimension() == 1) {
		  std::shared_ptr<TH1F> h1A(new TH1F(*(const TH1F*)histo_A));
		  std::shared_ptr<TH1F> h1B(new TH1F(*(const TH1F*)histo_B));
		  if (h1A != NULL && h1B != NULL){
		    pair<std::shared_ptr<TH1F>,std::shared_ptr<TH1F> > h1_pair = make_pair(h1A,h1B);
		    h_1d_histos[(*scanA_names_iter)+index_name] = h1_pair;
		  }
		}
		if (histo_A->GetDimension() == 2) {
		  std::shared_ptr<TH2F> h2A(new TH2F(*(const TH2F*)histo_A));
		  std::shared_ptr<TH2F> h2B(new TH2F(*(const TH2F*)histo_B));
		  if (h2A != NULL && h2B != NULL){
		    pair<std::shared_ptr<TH2F>,std::shared_ptr<TH2F> > h2_pair = make_pair(h2A,h2B);
		    h_2d_histos[(*scanA_names_iter)+index_name] = h2_pair;
		  }
		}	      
	      }
	    }
	    i[3]++;
	    if (i[3] >= info.maxIndex(3)){ 
	      i[3] = 0; 
	      i[2]++;
	      if (i[2] >= info.maxIndex(2)){ 
		i[2] = 0; 
		i[1]++;
		if (i[1] >= info.maxIndex(1)){ 
		  i[1] = 0; 
		  i[0]++;
		  if (i[0] >= info.maxIndex(0)){ finished = true;}}}}	   
	  }	  
	}
      }
    }
    
    std::cout<<"I have "<<h_1d_histos.size()<<" one dimensional histograms"<<std::endl;
    std::cout<<"I have "<<h_2d_histos.size()<<" two dimensional histograms"<<std::endl;
    
    if(h_1d_histos.size() != 0)  { 
      m_newInput->histos_1d  = h_1d_histos; 
    }
    if(h_2d_histos.size() != 0)  { 
      m_newInput->histos_2d  = h_2d_histos; 
    }
    
  }
  

  void AutomaticDIFFAnalysis::saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result2) 
  {
    ResultDIFF* module_result = static_cast<ResultDIFF*>(module_result2);
    
    ExtendedAnalysisResultList_t& extended_results = (ExtendedAnalysisResultList_t&)analysis_results;
    std::string histo_name; 

    std::map< std::string, std::shared_ptr<TH1F> >::iterator histo_1d_iter;
    std::map< std::string, std::shared_ptr<TH2F> >::iterator histo_2d_iter;
    std::map< std::string, float>::iterator histo_dev_iter;
    
    for (histo_1d_iter =  module_result->histos_1d_out.begin();
	 histo_1d_iter != module_result->histos_1d_out.end();
	 histo_1d_iter++){
      histo_name = histo_1d_iter->first + "_DIFF";
      //std::shared_ptr<TH1F> out_ptr_1d(new TH1F(histo_1d_iter->second));
      extended_results.addHisto<TH1>(histo_name,module_result->moduleName.Data(),std::shared_ptr<TH1>(histo_1d_iter->second));
    }

    for (histo_2d_iter =  module_result->histos_2d_out.begin();
	 histo_2d_iter != module_result->histos_2d_out.end();
	 histo_2d_iter++){
      histo_name = histo_2d_iter->first + "_DIFF";
      //std::shared_ptr<TH2F> out_ptr_2d(new TH2F(histo_2d_iter->second));
      extended_results.addHisto<TH1>(histo_name,module_result->moduleName.Data(),std::shared_ptr<TH1>(histo_2d_iter->second));
    }

    for (histo_dev_iter =  module_result->histos_dev_out.begin();
	 histo_dev_iter != module_result->histos_dev_out.end();
	 histo_dev_iter++){
      extended_results.addValue(histo_dev_iter->first,module_result->moduleName.Data(),histo_dev_iter->second);
    }
    
  }
}


