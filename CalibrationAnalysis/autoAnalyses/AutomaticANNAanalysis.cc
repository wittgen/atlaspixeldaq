#include "AutomaticANNAanalysis.hh"


namespace CAN {
  
  AutomaticANNAanalysis::AutomaticANNAanalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &oi)
    : AutomaticAnalysisBase(analysis_serial_number, oi)
  {
    m_name = "ANNAanalysis";
    m_config = PixLib::Config("ANNAanalysis");
    m_analysis = auto_ptr<ANNAanalysisROOT>(new ANNAanalysisROOT);
    ANNAanalysisROOT* antpr = static_cast<ANNAanalysisROOT*>(m_analysis.get());
    

    m_config.addGroup("general");
    PixLib::ConfGroup &group = m_config["general"];
    
    // more variables can be added here


    vector<string> digital_names;
    m_histo_type = "OCCUPANCY";
    digital_names.push_back(m_histo_type);
    m_scan_histo_names[""] = digital_names;

    std::cout<<"AutomaticANNAanalysis: constructed."<<std::endl;

  }

  void AutomaticANNAanalysis::requestScanConfigs(IAnalysisDataServer* ana_data_server)
  {
    ana_data_server->getDataServer()->requestScanConfig();
  }

  void AutomaticANNAanalysis::requestModuleConfigs(IAnalysisDataServer* ana_data_server, string module_name)
  {
  }

  void AutomaticANNAanalysis::getBasics(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos)
  {
    m_newInput->pp0Name = m_pp0_name;
    m_newInput->moduleName = getModuleName(ana_data_server, module_iter, pos);
  }

  void AutomaticANNAanalysis::getScanConfigs(PixLib::Config& scan_config)
  {
    float masks = confVal<int>(scan_config["general"]["maskStageSteps"]);
    float events = confVal<int>(scan_config["general"]["repetitions"]);

    m_newInput->Nmasks = masks;
    m_newInput->Nevents = events;

    std::string speed = confVal<std::string>(scan_config["mcc"]["mccBandwidth"]);
    if(speed == "SINGLE_40" || speed == "DOUBLE_40") m_newInput->MBs=40;
    else if(speed == "SINGLE_80" || speed == "DOUBLE_80") m_newInput->MBs=80;
    else m_newInput->MBs=-1; // should not happen

    m_newInput->axis = *converter.GetTheAxis(scan_config);
  }

  void AutomaticANNAanalysis::getModuleConfigs(PixLib::Config& module_config)
  {
  }


  void AutomaticANNAanalysis::getHistograms(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos)
  {
    const TH2* h_occ;
    if(m_CTdata)
      {
 	DataServerCT* dataServerCT = dynamic_cast<DataServerCT*>(ana_data_server->getDataServer());

 	h_occ = converter.GetTheHisto2D_old(*iterator(dataServerCT->getHistograms(m_histo_type, m_pp0_name).begin(),pos),*iterator(dataServerCT->getScanConfigRefs(m_histo_type, m_pp0_name).begin(),pos), m_histo_type, 0,0,0,0);
	cout<<"CTdata is there"<<endl;
      }
    else
      {
 	string conn_name = PixA::connectivityName(module_iter);
 	IDataServer* data_server = ana_data_server->getDataServer();
 	h_occ = converter.GetTheHisto2D(m_histo_type, data_server, conn_name, 0,0,0,0);
      }
    if(h_occ!=NULL)
      {
 	m_newInput->histo = h_occ;
	//debugging
      }
    else cout<<"something went wrong, histogram is empty"<<endl;
  }
  

  void AutomaticANNAanalysis::saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result2)
  {
    ResultANNA* module_result = static_cast<ResultANNA*>(module_result2);
    auto_ptr<ROOTanalysisHelper> helper(new ROOTanalysisHelper());
    
    analysis_results.addValue<float>("PixMissingHits", module_result->moduleName.Data(), module_result->PixMissingHits);
    analysis_results.addValue<float>("PixExtraHits", module_result->moduleName.Data(), module_result->PixExtraHits);
    analysis_results.addValue<float>("MaxMissingHits", module_result->moduleName.Data(), module_result->MaxMissingHits);
    analysis_results.addValue<float>("MaxExtraHits", module_result->moduleName.Data(), module_result->MaxExtraHits);
    analysis_results.addValue<bool>("ModStatus", module_result->moduleName.Data(), module_result->ModStatus);
  }


}




 
