#include "AutomaticAnalysisBaseROD.hh"
#include "ConfigWrapper/ConnectivityUtil.h"
#include <ConfigWrapper/PixDisableUtil.h>

#include <ExtendedAnalysisResultList_t.hh>

namespace CAN {

  AutomaticAnalysisBaseROD::AutomaticAnalysisBaseROD(SerialNumber_t analysis_serial_number, const ObjectInfo_t &) : 
    m_serialNumber(analysis_serial_number), m_logLevel(kWarnLog), m_abortRequest(false), m_config("")
  {
  }

  /**
   *  Request all the histograms whos names are specified in the 'm_scan_histo_names' array.
   *  It has to be filled in each analysis's constructor!
   */
  void AutomaticAnalysisBaseROD::requestHistograms(IAnalysisDataServer *ana_data_server, const PixA::RODLocationBase &rod_location)
  {
    //In request Histograms, we should not set the m_ROD_name. requetHistograms and analyze function can run in parallel
    //such that they can interfere each other.
    //m_ROD_name = rod_location; 

    // All histograms which should be analysed in analyse need to be requested. 
    // Otherwise they will not be downloaded from the histogram server.

    // Request histogram per ROD
     ana_data_server->getDataServer()->requestHistogram( m_scan_histo_name, rod_location );
    
    //**
    requestScanConfigs(ana_data_server);

    ana_data_server->createCombinedModuleDisabledList( rod_location );

    //**
    if (m_logLevel>=kInfoLog) std::cout << "INFO [AutomaticAnalysisBaseROD::requestHistogram] Requested histograms for " << rod_location << std::endl;
  }


  void AutomaticAnalysisBaseROD::analyse(IAnalysisDataServer *ana_data_server, const PixA::RODLocationBase &rod_location, ExtendedAnalysisResultList_t &analysis_results) 
  {
    m_ROD_name = rod_location;
    if (m_logLevel>=kInfoLog) std::cout << "INFO [AutomaticAnalysisBaseROD::analyse] starting... ROD name = " <<  m_ROD_name << std::endl;

    AnalysisInput_t analysisInput(new TObjArray());

    bool rod_enabled=false;
    // Loop over pp0s
    const PixA::Pp0List &pp0_list = ana_data_server->connectivity().pp0s(m_ROD_name);
    for (PixA::Pp0List::const_iterator pp0_iter=pp0_list.begin(); pp0_iter!=pp0_list.end(); ++pp0_iter) {
      if(!ana_data_server->enabled(pp0_iter)) continue; 

      bool pp0_enabled=false;    
      //get pp0name
      m_pp0_name = const_cast<PixLib::Pp0Connectivity*>(*pp0_iter)->name();

      // Loop over modules 
      const PixA::ModuleList &module_list = ana_data_server->connectivity().modules(PixA::location(pp0_iter));
      for(auto module_iter=module_list.begin(); module_iter != module_list.end(); ++module_iter) {
//	if(!PixA::enabled( ana_data_server->getCombinedModuleDisabledList(PixA::location(pp0_iter)), module_iter )) continue; 

        bool module_enabled = PixA::enabled( ana_data_server->getCombinedModuleDisabledList(PixA::location(pp0_iter)), module_iter );

        // Check for disabled modules
        if (m_logLevel>=kDiagnosticsLog) std::cout << "DIAGNOSTICS [AutomaticAnalysisBaseROD::analyse]  Module "<<m_ROD_name <<" "
						 << m_pp0_name<<" "<< PixA::connectivityName(module_iter) << " is "
						 << (module_enabled ? "enabled" : "disabled" ) ;

        if (!module_enabled) continue;
        rod_enabled=true;
        pp0_enabled=true; 

        newAnalysisInput();

        try {
	    //**
	    getBasics       ( ana_data_server, module_iter);
	    //**
	    getScanConfigs  ( ana_data_server, module_iter);
	    //**
	    getHistograms   ( ana_data_server, module_iter);
	    //**
	  }
	  catch(CAN::ConfigException &err) {
	    if (m_logLevel>=kErrorLog) std::cerr << "ERROR [AutomaticAnalysisBase::analyse] Caught exception while retrieving histograms and configurations : "
					       << err.getDescriptor() << std::endl;
	  }
	  addNewAnalysisInput( analysisInput );  //add input for all enabled modules, so that if no histos -> results can be set accordingly 
      }//loop module

      //there's possibility that all modules are disabled but pp0 is enabled
      if(pp0_enabled)
         analyseResultsPerPP0(ana_data_server,analysis_results,pp0_iter);

    }//loop pp0

    if(m_ROD_name != rod_location && m_logLevel>=kErrorLog) {
      std::cerr <<"ERROR [AutomaticAnalysisBaseROD::analyse] m_ROD_name="<<m_ROD_name <<" while rod_location="<< rod_location <<std::endl; 
    }
    if (m_logLevel>=kInfoLog)  std::cout << "INFO [AutomaticAnalysisBaseROD::analyse] Running in online framework, preparation of analysis input finished."
					 << std::endl;    

    if(rod_enabled){
      analyseResultsPerROD(ana_data_server,analysis_results);
      //**
      m_analysis->Analyse(analysisInput); //allways called, core-analysis has to handle 'no histo' case  
      //**
      saveAnalysisResults(analysis_results, m_analysis->GetResult() );
      //**
    }
    // Clean up
    analysisInput.reset();
//    m_analysis->GetResult().reset();
    m_analysis->Reset();

   }

  void AutomaticAnalysisBaseROD::getScanConfigs(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter) 
  {
    //PixLib::Config& scan_config = const_cast<PixLib::Config&>(ana_data_server->getDataServer()->getScanConfig());
    //getScanConfigs(scan_config);
    //CLA: taken out for now, since neither InlinkOutlin not Inlink needs scan-config vars; otherwise need to implement the 'requestScanConfigs()'s
  }


  void AutomaticAnalysisBaseROD::saveAnalysisResults(AnalysisResultList_t &analysis_results, AnalysisResult_t results) 
  {
    TIter next(results.get());

    unsigned int pp0Index=0;
    vector<string> pp0List;
    bool pp0_status[4]={true,true,true,true};
    bool comb_status(true);
    string rodName = m_ROD_name; 

    while(AnalysisResult * scan_iter = static_cast<AnalysisResult*>(next.Next()) ) { 
      rodName = string(scan_iter->rodName.Data());
      string pp0Name (scan_iter->pp0Name.Data());
      pp0Index=0;
      for(;pp0Index<pp0List.size();pp0Index++) {
          if(pp0List[pp0Index]==pp0Name) break;
          pp0List.push_back(pp0Name);
      }
      if(pp0Index<4) pp0_status[pp0Index] &= scan_iter->passed;

      analysis_results.addValue<bool>("Status",scan_iter->moduleName.Data(),scan_iter->passed);
      comb_status &= scan_iter->passed;
      
      if (m_logLevel>=kInfoLog) cout << "INFO [AutomaticAnalysisBaseROD::saveAnalysisResults] "<< rodName <<" "<< pp0Name << " "
				     <<scan_iter->moduleName <<" status "<< scan_iter->passed <<" combined "<< comb_status <<endl;
      //analysis_results.addValue<unsigned int>("failures",scan_iter->moduleName.Data(),scan_iter->tests);
      //**
      saveAnalysisSpecificResults( analysis_results, scan_iter ); 
      //**
    }

    for(pp0Index=0;pp0Index<pp0List.size();pp0Index++){
      if(pp0Index<4) {
        analysis_results.addValue("Status", pp0List[pp0Index], pp0_status[pp0Index]);
      }
      else if (m_logLevel>=kInfoLog) {
	cout << "AutomaticAnalysisBaseROD::saveAnalysisResults: More than 4 pp0:  "<< pp0Index<<" "<<pp0List[pp0Index]<<endl;
      }
    }
    analysis_results.addValue("Status", rodName, comb_status);
  }


}


