#include "AutomaticTHRESHOLDAnalysis.hh"
//#include "ConfigWrapper/ConnectivityUtil.h"
//#include "DataContainer/HistInfo_t.h"


namespace CAN {

  AutomaticTHRESHOLDAnalysis::AutomaticTHRESHOLDAnalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &oi)  
    : AutomaticAnalysisBase( analysis_serial_number, oi) 
  {
    m_name="THRESHOLDanalysis";
    m_config=PixLib::Config("THRESHOLDanalysis");
    m_analysis = unique_ptr<THRESHOLDanalysisROOT>(new THRESHOLDanalysisROOT);
    
    m_config.addGroup("general");
    PixLib::ConfGroup &group=m_config["general"];
    //
    group.addFloat("cut_noise_max normal", (static_cast<THRESHOLDanalysisROOT*>(m_analysis.get()))->cut_noise_max[0], 400, "noise max cut for normal pixels", true);
    group.addFloat("cut_noise_max ganged", (static_cast<THRESHOLDanalysisROOT*>(m_analysis.get()))->cut_noise_max[2], 450, "noise max cut for ganged pixels", true);
    group.addFloat("cut_noise_max special-non-ganged", (static_cast<THRESHOLDanalysisROOT*>(m_analysis.get()))->cut_noise_max[1], 420, "noise max cut for special-non-ganged pixels", true);
    //
    group.addInt("cut_noise_max normal percent", (static_cast<THRESHOLDanalysisROOT*>(m_analysis.get()))->cut_noise_max_percent[0], 90, "percent of normal pixels required below noise max cut", true);
    group.addInt("cut_noise_max ganged percent", (static_cast<THRESHOLDanalysisROOT*>(m_analysis.get()))->cut_noise_max_percent[2], 90, "percent of ganged pixels required below noise max cut", true);
    group.addInt("cut_noise_max special-non-ganged percent", (static_cast<THRESHOLDanalysisROOT*>(m_analysis.get()))->cut_noise_max_percent[1], 90, "percent of special-non-ganged pixels required below noise max cut", true);
    //
    group.addFloat("cut_thr_min", (static_cast<THRESHOLDanalysisROOT*>(m_analysis.get()))->cut_thr_min, 3000, "threshold min cut", true);
    group.addFloat("cut_thr_max", (static_cast<THRESHOLDanalysisROOT*>(m_analysis.get()))->cut_thr_max, 5500, "threshold max cut", true);
    //
    group.addInt("cut_thr_min_percent", (static_cast<THRESHOLDanalysisROOT*>(m_analysis.get()))->cut_thr_min_percent, 95, "percent of pixels required above threshold min cut", true);
    group.addInt("cut_thr_max_percent", (static_cast<THRESHOLDanalysisROOT*>(m_analysis.get()))->cut_thr_max_percent, 95, "percent of pixels required below threshold max cut", true);
    //
    group.addFloat("cut_threshold_mean_min", (static_cast<THRESHOLDanalysisROOT*>(m_analysis.get()))->cut_thr_mean_min, 3200, "threshold mean min cut", true);
    group.addFloat("cut_threshold_mean_max", (static_cast<THRESHOLDanalysisROOT*>(m_analysis.get()))->cut_thr_mean_max, 5300, "threshold mean max cut", true);
    group.addFloat("cut_thr_RMS_max", (static_cast<THRESHOLDanalysisROOT*>(m_analysis.get()))->cut_thr_RMS_max, 400, "threshold RMS max cut", true);
    group.addInt("threshold_mean_range_min", (static_cast<THRESHOLDanalysisROOT*>(m_analysis.get()))->range_thr_mean_min, 1000, "threshold range min for mean and RMS calc", true);
    group.addInt("threshold_mean_range_max", (static_cast<THRESHOLDanalysisROOT*>(m_analysis.get()))->range_thr_mean_max, 6000, "threshold range max for mean and RMS calc", true);
    //
    group.addFloat("cut_thresholdOverNoise_min", (static_cast<THRESHOLDanalysisROOT*>(m_analysis.get()))->cut_thresholdOverNoise_min, 10, "thresholdOverNoise min cut (for bad pixel definition)", true); 
    group.addInt("cut_num_bad_pixels_per_FE", (static_cast<THRESHOLDanalysisROOT*>(m_analysis.get()))->cut_num_bad_pixels_per_FE, 200, "number bad pixels per FE max", true);
    group.addBool("create_Pixelmap", (static_cast<THRESHOLDanalysisROOT*>(m_analysis.get()))->create_Pixelmap, true, "create a map of dead pixel", true);

    vector<string> threshold_names;
    m_histo_type_chi2 = "SCURVE_CHI2";
    m_histo_type_mean = "SCURVE_MEAN";
    m_histo_type_sigma = "SCURVE_SIGMA";
    threshold_names.push_back(m_histo_type_chi2);  threshold_names.push_back(m_histo_type_mean);  threshold_names.push_back(m_histo_type_sigma);
    m_scan_histo_names[""] = threshold_names; // For analysis with only one scan, scan name doesn't matter

    std::cout << "AutomaticTHRESHOLDAnalysis:   constructed." << std::endl;
  }


  void AutomaticTHRESHOLDAnalysis::requestScanConfigs(IAnalysisDataServer *ana_data_server) 
  {
    ana_data_server->getDataServer()->requestScanConfig();
  }

  void AutomaticTHRESHOLDAnalysis::requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name) 
  {
    ana_data_server->getDataServer()->requestModuleConfig(module_name);
    std::cout<<"AutomaticTHRESHOLDAnalysis:: requesting configuration for module "<<module_name<<std::endl;
  }

  void AutomaticTHRESHOLDAnalysis::getHistograms(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos) 
  {
    const TH2 *h_chi2, *h_mean, *h_sigma;  
    if(m_CTdata) {
      DataServerCT * dataServerCT = dynamic_cast<DataServerCT*>( ana_data_server->getDataServer() );

      h_chi2  = converter.GetTheHisto2D_old( *iterator(dataServerCT->getHistograms( m_histo_type_chi2, m_pp0_name).begin(),pos),  
					     *iterator(dataServerCT->getScanConfigRefs( m_histo_type_chi2, m_pp0_name).begin(),pos),  m_histo_type_chi2, -1,0,0,0);
      h_mean  = converter.GetTheHisto2D_old( *iterator(dataServerCT->getHistograms( m_histo_type_mean, m_pp0_name).begin(),pos),  
					     *iterator(dataServerCT->getScanConfigRefs( m_histo_type_mean, m_pp0_name).begin(),pos),  m_histo_type_mean, -1,0,0,0);
      h_sigma = converter.GetTheHisto2D_old( *iterator(dataServerCT->getHistograms( m_histo_type_sigma, m_pp0_name).begin(),pos),  
					     *iterator(dataServerCT->getScanConfigRefs( m_histo_type_sigma, m_pp0_name).begin(),pos),  m_histo_type_sigma, -1,0,0,0);
    }
    else {
      string conn_name = PixA::connectivityName( module_iter );  
      IDataServer* data_server = ana_data_server->getDataServer();
      h_chi2  = converter.GetTheHisto2D( m_histo_type_chi2, data_server, conn_name, -1,0,0,0);
      h_mean  = converter.GetTheHisto2D( m_histo_type_mean, data_server, conn_name, -1,0,0,0);
      h_sigma = converter.GetTheHisto2D( m_histo_type_sigma,data_server, conn_name, -1,0,0,0);
    }
    if(h_chi2==NULL || h_mean==NULL || h_sigma==NULL){
      string conn_name = PixA::connectivityName( module_iter );  
      std::cout<<"Empty histograms for "<<conn_name<<std::endl;
      char mes[128];
      sprintf(mes, "Histogram for %s not found.", conn_name.c_str());
      throw PixA::ConfigException(mes);
    }

    //cout << " adding  chi2 histogram" << endl;
    m_newInput->histo_chi2  = h_chi2; 
    //cout << " adding  mean histogram" << endl;
    m_newInput->histo_mean  = h_mean; 
    //cout << " adding  sigma histogram" << endl;
    m_newInput->histo_sigma = h_sigma; 
    
    PixGeometry geo(PixA::nRows(h_mean),PixA::nColumns(h_mean));
    m_feflv = geo.pixType();
    std::cout << "FE flavor: " << m_feflv << std::endl;

  }

  void AutomaticTHRESHOLDAnalysis::getBasics(IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos) 
  {
    m_newInput->pp0Name = m_pp0_name;
    m_newInput->moduleName = getModuleName( ana_data_server, module_iter, pos); 
  }


  void AutomaticTHRESHOLDAnalysis::getScanConfigs(PixLib::Config&   scan_config) 
  {
    cout << "AutomaticTHRESHOLDAnalysis::getScanConfigs   at start." << endl;
    float runMS = confVal<int>(const_cast< PixLib::Config &>(scan_config)["general"]["maskStageSteps"]);
    float totMS = converter.getTotalMaskStageSteps(confVal<std::string>(const_cast< PixLib::Config &>(scan_config)["general"]["maskStageTotalSteps"]));

    m_newInput->nMaskStages = (int)runMS;
    m_newInput->nPixel = 92160*(int)( runMS / totMS);
    m_newInput->nTotMaskStages = (int)totMS;

    string speed = confVal<std::string>(scan_config["mcc"]["mccBandwidth"]);
    //float zaehler = confVal<int>(scan_config["general"]["maskStageSteps"]); 
    //float nenner = converter.getTotalMaskStageSteps(confVal<std::string>(scan_config["general"]["maskStageTotalSteps"])); 
    //if(nenner!=0) m_newInput->nPixel = 92160*int(zaehler/nenner);
    
    if(speed=="SINGLE_40" || speed=="DOUBLE_40") m_newInput->MBs=40;
    else if(speed=="SINGLE_80" || speed=="DOUBLE_80") m_newInput->MBs=80;
    else m_newInput->MBs=-1; //should not happen
  }

 
  void AutomaticTHRESHOLDAnalysis::getModuleConfigs(PixLib::Config& module_config) 
  {
    cout << "AutomaticTHRESHOLDAnalysis::getModuleConfigs   at start." << endl;
    getDisabledFECP (module_config, m_feflv , m_newInput->fe_disable, m_newInput->cp_enable);

  }
  

  void AutomaticTHRESHOLDAnalysis::saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result2)
  {
    ResultTHRESHOLD* module_result = static_cast<ResultTHRESHOLD*>(module_result2);
    unique_ptr<ROOTanalysisHelper> helper(new ROOTanalysisHelper());

    //FEs
    analysis_results.addValue<float>("number of good FEs",module_result->moduleName.Data(),module_result->numGoodFEs);
    analysis_results.addValue<bool>("passed FE0",module_result->moduleName.Data(),module_result->passed_FE_cuts[0]);
    analysis_results.addValue<bool>("passed FE01",module_result->moduleName.Data(),module_result->passed_FE_cuts[1]);
    analysis_results.addValue<bool>("passed FE02",module_result->moduleName.Data(),module_result->passed_FE_cuts[2]);
    analysis_results.addValue<bool>("passed FE03",module_result->moduleName.Data(),module_result->passed_FE_cuts[3]);
    analysis_results.addValue<bool>("passed FE04",module_result->moduleName.Data(),module_result->passed_FE_cuts[4]);
    analysis_results.addValue<bool>("passed FE05",module_result->moduleName.Data(),module_result->passed_FE_cuts[5]);
    analysis_results.addValue<bool>("passed FE06",module_result->moduleName.Data(),module_result->passed_FE_cuts[6]);
    analysis_results.addValue<bool>("passed FE07",module_result->moduleName.Data(),module_result->passed_FE_cuts[7]);
    analysis_results.addValue<bool>("passed FE08",module_result->moduleName.Data(),module_result->passed_FE_cuts[8]);
    analysis_results.addValue<bool>("passed FE09",module_result->moduleName.Data(),module_result->passed_FE_cuts[9]);
    analysis_results.addValue<bool>("passed FE10",module_result->moduleName.Data(),module_result->passed_FE_cuts[10]);
    analysis_results.addValue<bool>("passed FE11",module_result->moduleName.Data(),module_result->passed_FE_cuts[11]);
    analysis_results.addValue<bool>("passed FE12",module_result->moduleName.Data(),module_result->passed_FE_cuts[12]);
    analysis_results.addValue<bool>("passed FE13",module_result->moduleName.Data(),module_result->passed_FE_cuts[13]);
    analysis_results.addValue<bool>("passed FE14",module_result->moduleName.Data(),module_result->passed_FE_cuts[14]);
    analysis_results.addValue<bool>("passed FE15",module_result->moduleName.Data(),module_result->passed_FE_cuts[15]);  

    //total module
    analysis_results.addValue<bool>("passed all FE bad pixel cuts",module_result->moduleName.Data(),module_result->passed_all_fe_bad_pixel);
    analysis_results.addValue<bool>("passed all FE noise cuts",module_result->moduleName.Data(),module_result->passed_all_fe_noise);
    analysis_results.addValue<bool>("passed all FE threshold cuts",module_result->moduleName.Data(),module_result->passed_all_fe_threshold);
    analysis_results.addValue<bool>("passed all FE threshold RMS cuts",module_result->moduleName.Data(),module_result->passed_all_fe_threshold_RMS);
    analysis_results.addValue<bool>("passed module threshold RMS cuts",module_result->moduleName.Data(),module_result->passed_module_threshold_RMS);

    //
    analysis_results.addValue<float>("average threshold",module_result->moduleName.Data(),module_result->average_threshold);
    analysis_results.addValue<float>("threshold RMS",module_result->moduleName.Data(),module_result->RMS_threshold);
  
    analysis_results.addValue<float>("average thresholdOverNoise",module_result->moduleName.Data(),module_result->average_thresholdOverNoise);
    analysis_results.addValue<float>("thresholdOverNoise min cut",module_result->moduleName.Data(),(static_cast<THRESHOLDanalysisROOT*>(m_analysis.get()))->cut_thresholdOverNoise_min);
    //
    for (int pixtype=0; pixtype<helper->NpixelClasses; pixtype++) {
      string type(helper->getPixelTypeString(pixtype));
      analysis_results.addValue<float>("average noise for "+type+" pixel",module_result->moduleName.Data(),module_result->average_noise[pixtype]);
      analysis_results.addValue<float>("noise RMS for "+type+" pixel",module_result->moduleName.Data(),module_result->RMS_noise[pixtype]);
    }
    //
    analysis_results.addValue<float>("number of bad pixels",module_result->moduleName.Data(),module_result->numBadPixels);
    //
    if(module_result->MakePixelMap) // checks if this was enabled in analysis configuration
      analysis_results.addValue<PixelMap_t>("bad pixel map",module_result->moduleName.Data(),module_result->badPixelMap);
     
  }

}
