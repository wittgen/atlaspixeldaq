#ifndef _CAN_AutomaticAnalysisBase_hh_
#define _CAN_AutomaticAnalysisBase_hh_

#include "Analysis.hh"
#include "coreAnalyses/ROOTanalysisBase.h"
#include "coreAnalyses/ROOTanalysisHelper.h"
#include "coreAnalyses/analysisOutput_Base.h"

#include "DataContainer/HistoRef.h"
#include "DataContainer/HistoInfo_t.h"
#include "DataContainer/HistoUtil.h"
#include "DataContainer/PixDbDataContainer.h"
#include <ConfigWrapper/ConfObjUtil.h>
#include <ConfigWrapper/PixDisableUtil.h>

#include "DataServerCT.hh"
#include "IAnalysisDataServer.hh"
#include "HistoConverter.hh"
#include <Config/Config.h>

#include <ExtendedAnalysisResultList_t.hh>



namespace CAN {

  class ObjectInfo_t;
  class AnalysisInput;

  class AutomaticAnalysisBase : public IAnalysisPerPP0 
  {
  public:
    AutomaticAnalysisBase(SerialNumber_t analysis_serial_number, const ObjectInfo_t &);
    ~AutomaticAnalysisBase() {};

    /** 
     * The public funcitions are implemented once in the base class.
     * To implement analysis specific parts use the functions listed below.
     */
      
    void requestHistograms(IAnalysisDataServer *data_server, const PixA::Pp0LocationBase &pp0_location);

    void analyse(IAnalysisDataServer *data_server, const PixA::Pp0LocationBase &pp0_location, ExtendedAnalysisResultList_t &analysis_results);

    PixLib::Config &config()            { return m_config; }
    const std::string& name() const     { return m_name; };
    SerialNumber_t serialNumber() const { return m_serialNumber;};
    void requestAbort()                 { m_abortRequest=true;};

    void setLogLevel(ELogLevel log_level) {
      m_logLevel = log_level;
    }

  protected:
    bool abortRequested() const         { return m_abortRequest; };

    SerialNumber_t          m_serialNumber;
    ELogLevel               m_logLevel;

    bool                    m_abortRequest;
    string                  m_name;

    /**
     * These are the function that should be defined by each derived analysis class:
     */
  protected:
    virtual void newAnalysisInput    () {}; 
    virtual void addNewAnalysisInput (AnalysisInput_t analysisInput) {};  
    virtual void requestScanConfigs  (IAnalysisDataServer *ana_data_server) {}; 
    virtual void requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name) {};
    virtual void getBasics           (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos) {}; 
    virtual void getScanConfigs      (PixLib::Config&   scan_config) {};
    virtual void getModuleConfigs    (PixLib::Config& module_config) {};
    virtual void getHistograms       (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos) {};
    virtual void saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result) {};
    virtual void fillNamedScans      (){};
    // ****************************************

   const std::map<std::pair<std::string, int>, std::vector<float> > getParamLoop(const PixLib::Config& scan_config) const;
   void getDisabledFECP (PixLib::Config& module_config, const PixGeometry::pixelType feFlv , std::vector<unsigned short int> &fe_disable, std::vector<std::vector<unsigned short int> > &cp_enable);

  private:
    void getScanConfigs  (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos);
    void getModuleConfigs(IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos);
    void saveAnalysisResults(AnalysisResultList_t &analysis_results, AnalysisResult_t results); 

  protected:
    unique_ptr<ROOTanalysisBase>  m_analysis;

    map<string,vector<string> > m_scan_histo_names; // vector containing the histo names for each scan
    string                      m_pp0_name;
    bool                        m_CTdata; // supposed to be the same for all analyses (all the pp0s) run within the same Object! 

    PixLib::Config              m_config;

    PixGeometry::pixelType      m_feflv; 

    HistoConverter              converter;

  protected:
    // Helper functions for all derived analysis classes
    string getModuleName(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos); 
    inline list<PixA::HistoHandle*>::const_iterator iterator(list<PixA::HistoHandle*>::const_iterator iter, int pos) { for(int i=0; i<pos; i++) ++iter; return iter; };
    inline list<PixA::ScanConfigRef*>::const_iterator iterator(list<PixA::ScanConfigRef*>::const_iterator iter, int pos) { for(int i=0; i<pos; i++) ++iter; return iter; };
  };

}
#endif

