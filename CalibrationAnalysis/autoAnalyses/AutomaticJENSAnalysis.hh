#ifndef _CAN_AutomaticJENSanalysis_hh_
#define _CAN_AutomaticJENSanalysis_hh_

#include "AutomaticAnalysisBase.hh"
#include "DataContainer/HistoInfo_t.h"
#include "JENSanalysisROOT.h"
#include "coreAnalyses/analysisInput_JENS.h"
#include "coreAnalyses/analysisOutput_JENS.h"



namespace CAN {


  /** 
   *  Analyzes threshold mean, noise and chi2 histograms, for one pp0  
   */
  class AutomaticJENSanalysis : public AutomaticAnalysisBase 
  {
  public:
    AutomaticJENSanalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &);
    ~AutomaticJENSanalysis() {};

  private:
    void newAnalysisInput() { m_newInput = new AnalysisInputJENS(); };
    void addNewAnalysisInput(AnalysisInput_t analysisInput) { analysisInput->Add(m_newInput); };  
    ////
    void requestScanConfigs  (IAnalysisDataServer *ana_data_server); 
    void requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name);
    void getBasics       (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos); 
    void getScanConfigs  (PixLib::Config&   scan_config);
    void getModuleConfigs(PixLib::Config& module_config);
    void getHistograms   (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos);
    void saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result);

  private:
    std::string    m_histo_type; 
    AnalysisInputJENS* m_newInput;
  };

}
#endif

