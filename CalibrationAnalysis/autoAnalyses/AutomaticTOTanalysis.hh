#ifndef _CAN_AutomaticTOTanalysis_hh_
#define _CAN_AutomaticTOTanalysis_hh_

#include "AutomaticAnalysisBase.hh"
#include "TOTanalysisROOT.h"
#include "coreAnalyses/analysisInput_TOT.h"
#include "coreAnalyses/analysisOutput_TOT.h"


namespace CAN {


  /** 
   *  Analyzes threshold mean, noise and chi2 histograms, for one pp0  
   */
  class AutomaticTOTanalysis : public AutomaticAnalysisBase 
  {
  public:
    AutomaticTOTanalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &);
    ~AutomaticTOTanalysis() {};

  private:
    void newAnalysisInput() { m_newInput = new AnalysisInputTOT(); };
    void addNewAnalysisInput(AnalysisInput_t analysisInput) { analysisInput->Add(m_newInput); };  
    ////
    void requestScanConfigs  (IAnalysisDataServer *ana_data_server); 
    void requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name);
    void getBasics       (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos); 
    void getScanConfigs  (PixLib::Config&   scan_config);
    void getModuleConfigs(PixLib::Config& module_config);
    void getModuleConfigs(std::string module);
    void getHistograms   (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos);
    void saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result);

  private:
    std::string             m_histo_type_chi2; 
    std::string             m_histo_type_mean; 
    std::string             m_histo_type_sigma; 
    AnalysisInputTOT* m_newInput;

  private: //TOT specific
    int m_NvcalSteps;
    bool m_chargeInjCapHigh;
    std::string maskStagesMode;
    std::vector<float> m_vcal_array;
  };

}
#endif

