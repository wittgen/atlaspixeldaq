#ifndef _CAN_AutomaticOCCUPANCYanalysis_hh_
#define _CAN_AutomaticOCCUPANCYanalysis_hh_

#include "AutomaticAnalysisBase.hh"
#include "OCCUPANCYanalysisROOT.h"
#include "coreAnalyses/analysisInput_OCCUPANCY.h"
#include "coreAnalyses/analysisOutput_OCCUPANCY.h"



namespace CAN {

  /** 
   * Analysis for DIGITAL scans, for one pp0 
   */
  class AutomaticOCCUPANCYanalysis : public AutomaticAnalysisBase 
  {
  public:
    AutomaticOCCUPANCYanalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &);
    ~AutomaticOCCUPANCYanalysis() {};
   
  private:
    void newAnalysisInput() { m_newInput = new AnalysisInputOCCUPANCY(); };
    void addNewAnalysisInput(AnalysisInput_t analysisInput) {analysisInput->Add(m_newInput);};
    void requestScanConfigs(IAnalysisDataServer *ana_data_server);
    void requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name);
    void getHistograms(IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos);
    void getBasics(IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos);
    void getScanConfigs(PixLib::Config& scan_config);
    void getModuleConfigs(PixLib::Config& module_config);
    void saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result);

  private:
    std::string m_histo_type; 
    AnalysisInputOCCUPANCY* m_newInput;
      int vcal;
      bool digital;
      std::string maskStagesMode;
  };

}
#endif

