#include "AutomaticMONLEAKAnalysis.hh"
//#include "ConfigWrapper/ConnectivityUtil.h"
//#include "DataContainer/HistInfo_t.h"


namespace CAN {

  AutomaticMONLEAKAnalysis::AutomaticMONLEAKAnalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &oi)  
    : AutomaticAnalysisBase( analysis_serial_number, oi) 
  {
    m_name="MONLEAKanalysis";
    m_config=PixLib::Config("MONLEAKanalysis");
    m_analysis = unique_ptr<MONLEAKanalysisROOT>(new MONLEAKanalysisROOT);
    
    m_config.addGroup("general");

    //group.addFloat("cut_monleak_max", (static_cast<MONLEAKanalysisROOT*>(m_analysis.get()))->cut_monleak_max[0], 1024, "maximum monleak value", true);
    
    vector<string> monleak_names;
    m_histo_type_monleak = "MONLEAK_HISTO";
    monleak_names.push_back(m_histo_type_monleak);
    
    m_scan_histo_names[""] = monleak_names; // For analysis with only one scan, scan name doesn't matter
    
    std::cout << "AutomaticMONLEAKAnalysis:   constructed." << std::endl;
  }
  
  
  void AutomaticMONLEAKAnalysis::requestScanConfigs(IAnalysisDataServer *ana_data_server) 
  {
    ana_data_server->getDataServer()->requestScanConfig();
  }
  
  void AutomaticMONLEAKAnalysis::requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name) 
  {
    ana_data_server->getDataServer()->requestModuleConfig(module_name);
    std::cout<<"AutomaticMONLEAKAnalysis:: requesting configuration for module "<<module_name<<std::endl;
  }
  
  void AutomaticMONLEAKAnalysis::getBasics(IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos) 
  {
    m_newInput->pp0Name = m_pp0_name;
    m_newInput->moduleName = getModuleName( ana_data_server, module_iter, pos); 
  }
  
  
  void AutomaticMONLEAKAnalysis::getScanConfigs(PixLib::Config&   scan_config) 
  {
    //     cout << "AutomaticMONLEAKAnalysis::getScanConfigs   at start." << endl;
    //     float runMS = confVal<int>(const_cast< PixLib::Config &>(scan_config)["general"]["maskStageSteps"]);
    //     float totMS = converter.getTotalMaskStageSteps(confVal<std::string>(const_cast< PixLib::Config &>(scan_config)["general"]["maskStageTotalSteps"]));
    
    //     m_newInput->nMaskStages = (int)runMS;
    //     m_newInput->nPixel = 92160*(int)( runMS / totMS);
    
    //     string speed = confVal<std::string>(scan_config["mcc"]["mccBandwidth"]);
    //float zaehler = confVal<int>(scan_config["general"]["maskStageSteps"]); 
    //float nenner = converter.getTotalMaskStageSteps(confVal<std::string>(scan_config["general"]["maskStageTotalSteps"])); 
    //if(nenner!=0) m_newInput->nPixel = 92160*int(zaehler/nenner);
    
    //  if(speed=="SINGLE_40" || speed=="DOUBLE_40") m_newInput->MBs=40;
    //     else if(speed=="SINGLE_80" || speed=="DOUBLE_80") m_newInput->MBs=80;
    //     else m_newInput->MBs=-1; //should not happen
  }
    
    
  void AutomaticMONLEAKAnalysis::getModuleConfigs(PixLib::Config& module_config) 
  {
    cout << "AutomaticMONLEAKAnalysis::getModuleConfigs   at start." << endl;
    PixLib::Config &casted_a_config=const_cast<PixLib::Config &>(module_config);
    if (casted_a_config.size()<=0 && casted_a_config.subConfigSize()<=0) return;
    
    std::vector<unsigned short int> fe_disable;

    ROOTanalysisHelper helper;
    int Nfe = helper.getNfrontEnd(m_feflv); // # of FEs in one module

    //     for (int fe=0; fe!=16; ++fe) {
    for (int fe=0; fe!=Nfe; ++fe) {
       char fe_name[50];
       sprintf(fe_name,"PixFe_%i",fe); 
       unsigned short int disable = 0;
       PixLib::Config& fe_config = module_config.subConfig(std::string(fe_name));
       bool enable_scan = ((PixLib::ConfBool&)fe_config["Misc"]["ScanEnable"]).value();
       bool enable_config = ((PixLib::ConfBool&)fe_config["Misc"]["ConfigEnable"]).value();
       bool enable_dacs = ((PixLib::ConfBool&)fe_config["Misc"]["DacsEnable"]).value();
       if (!enable_scan || !enable_config || !enable_dacs) disable = 1;
      
       fe_disable.push_back(disable);
      
     }
     m_newInput->fe_disable = fe_disable;
  }
  

  
  void AutomaticMONLEAKAnalysis::getHistograms(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos) 
  {
    
    const TH2 *histo_monleak;
    
    if(m_CTdata) {
      DataServerCT * dataServerCT = dynamic_cast<DataServerCT*>( ana_data_server->getDataServer() );
    
      histo_monleak  = converter.GetTheHisto2D_old( *iterator(dataServerCT->getHistograms( m_histo_type_monleak, m_pp0_name).begin(),pos), *iterator(dataServerCT->getScanConfigRefs( m_histo_type_monleak, m_pp0_name).begin(),pos), m_histo_type_monleak, 0,0,0,0); //-1?
      cout<<"CTdata is there"<<endl;
    }
    else {
      string conn_name = PixA::connectivityName( module_iter );  
      cout << conn_name << endl;
      IDataServer* data_server = ana_data_server->getDataServer();
      histo_monleak  = converter.GetTheHisto2D( m_histo_type_monleak, data_server, conn_name, 0,0,0,0); //-1?
      cout << "Got histogram" << endl;
    }
    
    if(histo_monleak!=NULL)  { m_newInput->histo_monleak  = histo_monleak; }
    else{
      string conn_name = PixA::connectivityName( module_iter );  
      cout<<"something went wrong, histogram is empty"<<endl;
      char mes[128];
      sprintf(mes, "Histogram for %s not found.", conn_name.c_str());
      throw PixA::ConfigException(mes);
    }

    PixGeometry geo(PixA::nRows(histo_monleak),PixA::nColumns(histo_monleak)); 
    m_feflv = geo.pixType();
    std::cout << "FE flavor: " << m_feflv << std::endl;
  }
  
  void AutomaticMONLEAKAnalysis::saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result2) 
  {
    
    ResultMONLEAK* module_result = static_cast<ResultMONLEAK*>(module_result2);
    unique_ptr<ROOTanalysisHelper> helper(new ROOTanalysisHelper());
    
    ExtendedAnalysisResultList_t& extended_results = (ExtendedAnalysisResultList_t&)analysis_results;
    cout << "Saving results for modules " << module_result->moduleName << endl;
    
   
    // -- results per module
    
    analysis_results.addValue<float>("iLeak_all",module_result->moduleName.Data(),module_result->iLeak_all);
    analysis_results.addValue<float>("iLeak_L0",module_result->moduleName.Data(),module_result->iLeak_L0);
    analysis_results.addValue<float>("iLeak_L1",module_result->moduleName.Data(),module_result->iLeak_L1);
    analysis_results.addValue<float>("iLeak_L2",module_result->moduleName.Data(),module_result->iLeak_L2);
    analysis_results.addValue<float>("iLeak_disks",module_result->moduleName.Data(),module_result->iLeak_disks);
    analysis_results.addValue<float>("iLeakTotal",module_result->moduleName.Data(),module_result->iLeakTotal);
    analysis_results.addValue<float>("iLeak_FEmax",module_result->moduleName.Data(),module_result->iLeak_FEmax);
    analysis_results.addValue<float>("iLeak_FEmin",module_result->moduleName.Data(),module_result->iLeak_FEmin);
    analysis_results.addValue<float>("iLeak_FE0",module_result->moduleName.Data(),module_result->iLeak_FE0);
    analysis_results.addValue<float>("iLeak_FE1",module_result->moduleName.Data(),module_result->iLeak_FE1);
    if(m_feflv == PixGeometry::FEI4_CHIP || m_feflv == PixGeometry::FEI4_MODULE){
      analysis_results.addValue<float>("iLeak_FE2",module_result->moduleName.Data(),module_result->iLeak_FE2);
      analysis_results.addValue<float>("iLeak_FE3",module_result->moduleName.Data(),module_result->iLeak_FE3);
      analysis_results.addValue<float>("iLeak_FE4",module_result->moduleName.Data(),module_result->iLeak_FE4);
      analysis_results.addValue<float>("iLeak_FE5",module_result->moduleName.Data(),module_result->iLeak_FE5);
      analysis_results.addValue<float>("iLeak_FE6",module_result->moduleName.Data(),module_result->iLeak_FE6);
      analysis_results.addValue<float>("iLeak_FE7",module_result->moduleName.Data(),module_result->iLeak_FE7);
      analysis_results.addValue<float>("iLeak_FE8",module_result->moduleName.Data(),module_result->iLeak_FE8);
      analysis_results.addValue<float>("iLeak_FE9",module_result->moduleName.Data(),module_result->iLeak_FE9);
      analysis_results.addValue<float>("iLeak_FE10",module_result->moduleName.Data(),module_result->iLeak_FE10);
      analysis_results.addValue<float>("iLeak_FE11",module_result->moduleName.Data(),module_result->iLeak_FE11);
      analysis_results.addValue<float>("iLeak_FE12",module_result->moduleName.Data(),module_result->iLeak_FE12);
      analysis_results.addValue<float>("iLeak_FE13",module_result->moduleName.Data(),module_result->iLeak_FE13);
      analysis_results.addValue<float>("iLeak_FE14",module_result->moduleName.Data(),module_result->iLeak_FE14);
      analysis_results.addValue<float>("iLeak_FE15",module_result->moduleName.Data(),module_result->iLeak_FE15);
    }

    extended_results.addHisto<TH1>("iLeakMap",module_result->moduleName.Data(),std::shared_ptr<TH1>(module_result->iLeakMap));
    extended_results.addHisto<TH1>("iLeak",module_result->moduleName.Data(),std::shared_ptr<TH1>(module_result->iLeak));
    extended_results.addHisto<TH1>("iLeak_FE",module_result->moduleName.Data(),std::shared_ptr<TH1>(module_result->iLeak_FE));
    
  }
}
