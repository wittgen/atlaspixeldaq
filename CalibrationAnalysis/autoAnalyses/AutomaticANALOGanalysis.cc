#include "AutomaticANALOGanalysis.hh"
#include "ConfigWrapper/ConnectivityUtil.h"

namespace CAN {

  AutomaticANALOGanalysis::AutomaticANALOGanalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &)  
    : m_serialNumber(analysis_serial_number), m_abortRequest(false), m_name("ANALOGanalysis"), m_config("TestAnalysis") 
  {
    m_config.addGroup("general");
    PixLib::ConfGroup &group=m_config["general"];
    group.addFloat("cut_current_max", m_analysis.cut_current_max, 400, "current_max", true);

    m_histo_type = "OCCUPANCY"; //for ANALOG scans
  }

  void AutomaticANALOGanalysis::requestHistograms(IAnalysisDataServer *ana_data_server, 
						   const PixA::Pp0LocationBase &pp0_location)
  {
    IDataServer *data_server = ana_data_server->getDataServer();
    DataServerCT * dataServer = dynamic_cast<DataServerCT*>(data_server);
    if(dataServer!=NULL) { //in case we are reading CT files  
      m_pp0_name = pp0_location; //its a string
      data_server->requestHistogram( m_histo_type, m_pp0_name);
      return;
    }

    /// LOOP over modules 
    const PixA::ModuleList &module_list=ana_data_server->connectivity().modules(pp0_location);
    PixA::ModuleList::const_iterator module_iter=module_list.begin();
    PixA::ModuleList::const_iterator module_last=module_list.end();
    for(; module_iter != module_last && !abortRequested(); ++module_iter) {
      std::string conn_name = PixA::connectivityName( module_iter );  
      data_server->requestHistogram( m_histo_type, conn_name);  
    }
  }
    
  void AutomaticANALOGanalysis::analyse(IAnalysisDataServer *ana_data_server, 
					const PixA::Pp0LocationBase &pp0_location, 
					AnalysisResultList_t &analysis_results) 
  {
    std::cout << "AutomaticANALOGanalysis: Starting preparation of analysis input data." << std::endl;
    IDataServer *data_server = ana_data_server->getDataServer();
    DataServerCT * dataServer = dynamic_cast<DataServerCT*>(data_server);

    AnalysisInput_t analysisInput;
    if(dataServer!=NULL) { //in case we are reading CT files  

      analysisInput = ExtractANALOGdata( data_server->getHistograms( m_histo_type, m_pp0_name), dataServer->getScanConfigRefs(m_histo_type,m_pp0_name) );
      
      m_analysis.WriteRootFile(dataServer->GetFileName().c_str(),analysisInput,pp0_location);
      
      m_analysis.SetDebugLevel(1);
      m_analysis.Analyse(analysisInput);
    }
    else { // when using HistoServer
      analysisInput = new TObjArray(); 

      const PixA::ModuleList &module_list=ana_data_server->connectivity().modules(pp0_location);
      PixA::ModuleList::const_iterator module_iter=module_list.begin();
      PixA::ModuleList::const_iterator module_last=module_list.end();

      /// LOOP over modules 
      for(; module_iter != module_last && !abortRequested(); ++module_iter) {
	// fill analysisInput
	ScanHistoANALOG * newHisto = new ScanHistoANALOG;

	const TH2* h;
	try {
	  std::string conn_name = PixA::connectivityName( module_iter );  
	  const PixLib::Config &scan_config = data_server->getScanConfig();

	  newHisto->moduleName = conn_name;
	  newHisto->pp0Name = m_pp0_name;

	  std::string speed = confVal<std::string>(const_cast< PixLib::Config &>(scan_config)["mcc"]["mccBandwidth"]);
	  if(speed=="SINGLE_40" || speed=="DOUBLE_40") newHisto->MBs=40;
	  else if(speed=="SINGLE_80" || speed=="DOUBLE_80") newHisto->MBs=80;
	  else newHisto->MBs=-1; //should not happen
	  
	  //newHisto->axis = *converter.GetTheAxis(const_cast< PixLib::Config &>(scan_config));//crashing
	  h = converter.GetTheHisto2D( m_histo_type, data_server, conn_name, 0, 0, 0, 0);
	}
	catch(CAN::ConfigException &err) {
	  std::cout << "Unexpected error caught in AutomaticANALOGanalysis.cc" << std::endl;
	}
	if(h!=NULL) newHisto->histo = h;
	analysisInput->Add( newHisto );
      }
    }
    std::cout << "AutomaticANALOGanalysis: Running in online framework, preparation of analysis input finished." << std::endl;    

    //if( m_analysis.GetStatus()==ROOTanalysisBase::kPassed ) {
    if( true ) { //always!, at least for now

      // fill the generic result record ...
      std::cout << "AutomaticANALOGanalysis: Saving results into database." << std::endl;
      auto_ptr<ROOTanalysisHelper> helper(new ROOTanalysisHelper());
      TIter next(m_analysis.GetResult());
      bool comb_status(true);
      while(ResultANALOG * scan_iter = static_cast<ResultANALOG*>(next.Next()) ) { 
	analysis_results.addValue<bool>("Status",scan_iter->moduleName.Data(),scan_iter->passed);
	comb_status &= scan_iter->passed;
	analysis_results.addValue<unsigned int>("failures",scan_iter->moduleName.Data(),scan_iter->tests);
	for (int pixtype=0; pixtype<6; pixtype++) {
	  string type(helper->getPixelTypeString(pixtype));
	  analysis_results.addValue<float>("average current  for "+type+" pixel",scan_iter->moduleName.Data(),scan_iter->average[pixtype]);
	}
      }
      analysis_results.addValue("Status",pp0_location, comb_status);
    }      
    else
      std::cout << "AutomaticANALOGanalysis: Overall analysis status was 'failed', results are NOT passed to database." << std::endl;

    //clean up
    TIter next(analysisInput);
    while(ScanHistoANALOG * scan_iter = static_cast<ScanHistoANALOG*>(next.Next()) )
      delete scan_iter;
    delete analysisInput;
  } //crashing here

  AnalysisInput_t AutomaticANALOGanalysis::ExtractANALOGdata(std::list<PixA::HistoHandle*> histograms, std::list<PixA::ScanConfigRef *> scan_configs )
  {
    std::cout << " Start of ExtractBocdata    list histograms " << histograms.size() << " ,  scan_config.size " << scan_configs.size() << std::endl;

    AnalysisInput_t analysisInput = new TObjArray();
    std::list<PixA::ScanConfigRef*>::const_iterator scanConfig_iter = scan_configs.begin();
    std::list<PixA::HistoHandle*>::const_iterator handle_iter=histograms.begin();
    
    for (; handle_iter!=histograms.end(); handle_iter++, scanConfig_iter++) {
      ScanHistoANALOG * newHisto = new ScanHistoANALOG;

      //      PixA::HistoHandle * a_histo_handle(*handle_iter);
      //      PixA::PixDbDataContainer container(*(*scan_config_iter), *(*handle_iter));
      //      int histo_type = getHistoType(m_histo_type);
      
      std::stringstream modName;
      if (!(*scanConfig_iter)->connName().empty()) {
	modName << (*scanConfig_iter)->connName() << "(" << (*scanConfig_iter)->prodName() << ")";
      }
      else {
	modName << (*scanConfig_iter)->prodName();
      }
      newHisto->moduleName = modName.str();
      newHisto->pp0Name = m_pp0_name;

      //      std::cout << " ----------  module=" << modName.str();
      //      std::cout << modName.str();
      //      std::cout << " histo=\"" << histo_name << "\"" << std::endl;

//       //      PixA::HistoRef histo(a_histo_handle->ref());
//       HistoInfo_t info;
//       //    histo.numberOfHistos(histo_name, info); //CLA: NO this works only for Histos not for PixScanHistos !
//       container.numberOfHistos(histo_type,info); //this gives the path output
//       //      std::cout << "idx:";
//       //      for (int i=0; i<3; i++) {
//       //	std::cout << info.maxIndex(i) << (i<2 ? "," : "");
//       //      }
//       //      std::cout << " n histos=" << info.minHistoIndex() << " - " << info.maxHistoIndex() << std::endl;      
//       std::string speed = confVal<std::string>((*scan_config_iter)->pixScanConfig()["mcc"]["mccBandwidth"]);
//       ////      std::string speed = confVal<std::string>((*scanConfig)["mcc"]["mccBandwidth"]);
//       //      std::cout << " SPEED= " << speed << std::endl;
      
//      newHisto->axis = *converter.GetTheAxis( *handle_iter,  *scanConfig_iter);

      std::string speed = confVal<std::string>((*scanConfig_iter)->pixScanConfig()["mcc"]["mccBandwidth"]);
      if(speed=="SINGLE_40" || speed=="DOUBLE_40") newHisto->MBs=40;
      else if(speed=="SINGLE_80" || speed=="DOUBLE_80") newHisto->MBs=80;
      else newHisto->MBs=-1; //should not happen
      
//       for (uint iz=0;iz<info.maxIndex(2);iz++){
// 	for (uint ih=info.minHistoIndex();ih<info.maxHistoIndex();ih++) {
// 	  int idx_in[4]={-1,-1,iz,ih};
// 	  //      Histo hix= Histo(*(container.getGenericPixLibHisto(histo_type,idx_in)));
// 	  PixLib::Histo * h_pixlib = container.getGenericPixLibHisto(histo_type,idx_in);
// 	  TH2F * h_root = dynamic_cast<TH2F *>(PixA::transform<PixLib::Histo>(h_pixlib));
// 	  if(ih==0)
// 	    newHisto->histo_Link1.push_back( h_root );
// 	  else //ih==1
// 	    newHisto->histo_Link2.push_back( h_root );
// 	  //std::cout << "IDX   iz=" << iz << "  ih=" << ih << std::endl;
// 	}
//       }      

      newHisto->histo = *converter.GetTheHisto2D_old( *handle_iter,  *scanConfig_iter,  m_histo_type, 0, 0, 0, 0);

      analysisInput->Add( newHisto );
    }
    return analysisInput;
  }

}


