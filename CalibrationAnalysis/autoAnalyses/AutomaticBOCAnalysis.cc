#include "AutomaticBOCAnalysis.hh"
//#include "ConfigWrapper/ConnectivityUtil.h"
//#include "DataContainer/HistInfo_t.h"


namespace CAN {

  AutomaticBOCanalysis::AutomaticBOCanalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &oi)  
    : AutomaticAnalysisBase( analysis_serial_number, oi) 
  {
    m_name="BOCanalysis";
    m_config=PixLib::Config("BOCanalysis");
    m_analysis = unique_ptr<BOCanalysisROOT>(new BOCanalysisROOT);
    
    m_config.addGroup("general");
    //PixLib::ConfGroup &group=m_config["general"];
    //

    // todo: add variables you want to configure


    
    vector<string> boc_names;
    m_histo_type = "RAW_DATA_DIFF_2"; //for BOC scans
    boc_names.push_back(m_histo_type); 
    m_scan_histo_names[""] = boc_names; // For analysis with only one scan, scan name doesn't matter

    std::cout << "AutomaticBOCanalysis:   constructed." << std::endl;
  }


  void AutomaticBOCanalysis::requestScanConfigs(IAnalysisDataServer *ana_data_server) 
  {
    ana_data_server->getDataServer()->requestScanConfig();
  }

  void AutomaticBOCanalysis::requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name) 
  {
  }

  void AutomaticBOCanalysis::getBasics(IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos) 
  {
    m_newInput->pp0Name = m_pp0_name;
    m_newInput->moduleName = getModuleName( ana_data_server, module_iter, pos); 
  }


  void AutomaticBOCanalysis::getScanConfigs(PixLib::Config& scan_config) 
  {
    float zaehler = confVal<int>(scan_config["general"]["maskStageSteps"]); 
    float nenner = converter.getTotalMaskStageSteps(confVal<std::string>(scan_config["general"]["maskStageTotalSteps"])); 
    if(nenner!=0) m_newInput->nPixel = 92160*int(zaehler/nenner);
   
    std::string speed = confVal<std::string>(scan_config["mcc"]["mccBandwidth"]);
    if(speed=="SINGLE_40" || speed=="DOUBLE_40") m_newInput->MBs=40;
    else if(speed=="SINGLE_80" || speed=="DOUBLE_80") m_newInput->MBs=80;
    else m_newInput->MBs=-1; //should not happen

    m_newInput->axis = *converter.GetTheAxis(scan_config);
  }


  void AutomaticBOCanalysis::getModuleConfigs(PixLib::Config& module_config) 
  {
  }


  void AutomaticBOCanalysis::getHistograms(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos) 
  {
    //cout << "AutomaticBOCanalysis::getHistograms   at start." << endl;

    vector<const TH2*> h_link1, h_link2;  
    if(m_CTdata) {
      DataServerCT * dataServerCT = dynamic_cast<DataServerCT*>( ana_data_server->getDataServer() );
      
      h_link1 = converter.GetTheHisto2Dvect_old( *iterator(dataServerCT->getHistograms( m_histo_type, m_pp0_name).begin(),pos),  
					     *iterator(dataServerCT->getScanConfigRefs( m_histo_type, m_pp0_name).begin(),pos),  m_histo_type, -1,-1, 0, 0);
      h_link2 = converter.GetTheHisto2Dvect_old( *iterator(dataServerCT->getHistograms( m_histo_type, m_pp0_name).begin(),pos),  
					       *iterator(dataServerCT->getScanConfigRefs( m_histo_type, m_pp0_name).begin(),pos), m_histo_type, -1, -1, 0, 1);
    }
    else {
      string conn_name = PixA::connectivityName( module_iter );  
      IDataServer* data_server = ana_data_server->getDataServer();

      h_link1 = converter.GetTheHisto2Dvect( m_histo_type, data_server, conn_name, -1, -1, 0, 0);
      h_link2 = converter.GetTheHisto2Dvect( m_histo_type, data_server, conn_name, -1, -1, 0, 1);
    }
    if(!h_link1.empty()) {
      //cout << " adding  mean histogram" << endl;
      m_newInput->histo_Link1 = h_link1; }
    if(!h_link2.empty()) { 
      //cout << " adding  sigma histogram" << endl;
      m_newInput->histo_Link2 = h_link2; }
  }


  void AutomaticBOCanalysis::saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result2) 
  {
    ResultBOC* module_result = static_cast<ResultBOC*>(module_result2);
    unique_ptr<ROOTanalysisHelper> helper(new ROOTanalysisHelper());

    analysis_results.addValue<float>("bestViset",module_result->moduleName.Data(),module_result->bestViset);
    analysis_results.addValue<float>("optimalThreshold",module_result->moduleName.Data(),module_result->optimalThreshold);
    analysis_results.addValue<float>("optimalDelay",module_result->moduleName.Data(),module_result->optimalDelay);
  }

}
