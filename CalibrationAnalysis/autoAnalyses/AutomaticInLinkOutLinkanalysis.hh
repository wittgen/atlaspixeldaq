#ifndef _CAN_AutomaticInLinkOutLinkanalysis_hh_
#define _CAN_AutomaticInLinkOutLinkanalysis_hh_

#include "AutomaticAnalysisBaseROD.hh"
#include "InLinkOutLinkanalysisROOT.h"
#include "coreAnalyses/analysisInput_InLinkOutLink.h"
#include "coreAnalyses/analysisOutput_InLinkOutLink.h"



namespace CAN {


  /** 
   *  Analyzes threshold mean, noise and chi2 histograms, for one pp0  
   */
  class AutomaticInLinkOutLinkanalysis : public AutomaticAnalysisBaseROD 
  {
  public:
    AutomaticInLinkOutLinkanalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &);
    ~AutomaticInLinkOutLinkanalysis() {};

  private:
    void newAnalysisInput() { m_newInput = new AnalysisInputInLinkOutLink(); };
    void addNewAnalysisInput(AnalysisInput_t analysisInput) { analysisInput->Add(m_newInput); };  
    ////
    void requestScanConfigs  (IAnalysisDataServer *ana_data_server); 
    void requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name);
    void getBasics       (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter); 
    void getScanConfigs  (PixLib::Config&   scan_config);
    void getModuleConfigs(PixLib::Config& module_config);
    void getHistograms   (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter);
    void saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result);

  private:

    //We only store one 2D histogram per ROD
    const TH2*                    global_histo_InLinkOutLink;
    std::string             global_ROD_name;
    std::string             global_pp0name;

    std::string             m_histo_type_name; 
    AnalysisInputInLinkOutLink* m_newInput;
  };

}
#endif

