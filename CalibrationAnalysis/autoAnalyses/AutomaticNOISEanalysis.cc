#include "AutomaticNOISEanalysis.hh"
#include "PixFe/PixGeometry.h"
#include "DataContainer/GenericHistogramAccess.h"

namespace CAN {
  
  AutomaticNOISEanalysis::AutomaticNOISEanalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &oi)
    : AutomaticAnalysisBase(analysis_serial_number, oi)
  {
    m_name = "NOISEanalysis";
    m_config = PixLib::Config("NOISEanalysis");
    m_analysis = unique_ptr<NOISEanalysisROOT>(new NOISEanalysisROOT);
    NOISEanalysisROOT* anptr = static_cast<NOISEanalysisROOT*>(m_analysis.get());
    
    m_config.addGroup("general");
    PixLib::ConfGroup &group = m_config["general"];
    
    // more variables can be added here
    group.addFloat("max_noisy_pixel", anptr->max_noisy_pixel, 50, "maximum number of noisy pixels", true);
    group.addInt("max_noise_allowed", anptr->max_noise_allowed, 6, "maximum noise allowed", true);

    vector<string> noise_names;
    m_histo_type = "OCCUPANCY";
    noise_names.push_back(m_histo_type);
    m_scan_histo_names[""] = noise_names;
    std::cout<<"AutomaticNOISEanalysis: constructed."<<std::endl;

  }

  void AutomaticNOISEanalysis::requestScanConfigs(IAnalysisDataServer* ana_data_server)
  {
    ana_data_server->getDataServer()->requestScanConfig();
  }

  void AutomaticNOISEanalysis::requestModuleConfigs(IAnalysisDataServer* ana_data_server, string module_name)
  {
    ana_data_server->getDataServer()->requestModuleConfig(module_name);
    std::cout<<"AutomaticNOISEanalysis:: requesting configuration for module "<<module_name<<std::endl;
  }

void AutomaticNOISEanalysis::getHistograms(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos)
  {
    cout << "AutomaticNOISEanalysis::getHisto   at start." << endl;
    const TH2* h_occ=0;
    string conn_name;
    if(m_CTdata)
      {
 	DataServerCT* dataServerCT = dynamic_cast<DataServerCT*>(ana_data_server->getDataServer());
 	h_occ = converter.GetTheHisto2D_old(*iterator(dataServerCT->getHistograms(m_histo_type, m_pp0_name).begin(),pos),*iterator(dataServerCT->getScanConfigRefs(m_histo_type, m_pp0_name).begin(),pos), m_histo_type, 0,0,0,0);
	cout<<"CTdata is there"<<endl;
      }
    else
      {
 	conn_name = PixA::connectivityName(module_iter);
 	IDataServer* data_server = ana_data_server->getDataServer();
 	h_occ = converter.GetTheHisto2D(m_histo_type, data_server, conn_name, 0,0,0,0);
      }
    if(h_occ!=NULL)
      {
 	m_newInput->histo = h_occ;
	
      }
    else cout<<"something went wrong, histogram is empty"<<endl;
      
      
    PixGeometry geo(PixA::nRows(h_occ),PixA::nColumns(h_occ)); 
    m_feflv = geo.pixType();

    cout << "AutomaticNOISEanalysis::getHisto   at end." << endl;
  }
  
  void AutomaticNOISEanalysis::getBasics(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos)
  {
    m_newInput->pp0Name = m_pp0_name;
    m_newInput->moduleName = getModuleName(ana_data_server, module_iter, pos);
  }

  void AutomaticNOISEanalysis::getScanConfigs(PixLib::Config& scan_config)
  {
    cout << "AutomaticNOISEanalysis::getScanConfigs   at start." << endl;
    m_newInput->Nmasks = (int) converter.getTotalMaskStageSteps(confVal<std::string>(const_cast< PixLib::Config &>(scan_config)["general"]["maskStageTotalSteps"]));
    m_newInput->Nmasksdone = confVal<int>(scan_config["general"]["maskStageSteps"]);
      std::cout<<"Nmasks is:"<< (int) converter.getTotalMaskStageSteps(confVal<std::string>(const_cast< PixLib::Config &>(scan_config)["general"]["maskStageTotalSteps"])) <<std::endl;
    m_newInput->Nevents = confVal<int>(scan_config["general"]["repetitions"]);
      maskStagesMode = confVal<string>(scan_config["general"]["maskStageMode"]);

    std::string speed = confVal<std::string>(scan_config["mcc"]["mccBandwidth"]);
    if(speed == "SINGLE_40" || speed == "DOUBLE_40") m_newInput->MBs=40;
    else if(speed == "SINGLE_80" || speed == "DOUBLE_80") m_newInput->MBs=80;
    else m_newInput->MBs=-1; // should not happen

    m_newInput->axis = *converter.GetTheAxis(scan_config);
    cout << "AutomaticNOISEanalysis::getScanConfigs   at end." << endl;
  }
  
  void AutomaticNOISEanalysis::getModuleConfigs(PixLib::Config& module_config)
  {
     cout << "AutomaticNOISEanalysis::getModuleConfigs   at start." << endl;
     getDisabledFECP (module_config, m_feflv , m_newInput->fe_disable, m_newInput->cp_enable);
  }
  
  void AutomaticNOISEanalysis::saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result2)
  {
    ResultNOISE* module_result = static_cast<ResultNOISE*>(module_result2);
    unique_ptr<ROOTanalysisHelper> helper(new ROOTanalysisHelper());
    
    
    analysis_results.addValue<float>("PixNoisy", module_result->moduleName.Data(), module_result->PixNoisy);
    if(module_result->MakePixelMap){ // checks if this was enabled in analysis configuration
	analysis_results.addValue<PixelMap_t>("Pixel noisy", module_result->moduleName.Data(), module_result->noisyPixelMap);
    }
  }

}



 
