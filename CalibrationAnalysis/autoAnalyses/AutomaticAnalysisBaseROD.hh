#ifndef _CAN_AutomaticAnalysisBaseROD_hh_
#define _CAN_AutomaticAnalysisBaseROD_hh_

#include "Analysis.hh"
#include "coreAnalyses/ROOTanalysisBase.h"
#include "coreAnalyses/ROOTanalysisHelper.h"
#include "coreAnalyses/analysisOutput_Base.h"

#include "DataContainer/HistoRef.h"
#include "DataContainer/HistoInfo_t.h"
#include "DataContainer/HistoUtil.h"
#include "DataContainer/PixDbDataContainer.h"
#include <ConfigWrapper/ConfObjUtil.h>
#include <ConfigWrapper/PixDisableUtil.h>

#include "DataServerCT.hh"
#include "IAnalysisDataServer.hh"
#include "HistoConverter.hh"
#include <Config/Config.h>



namespace CAN {

  class ObjectInfo_t;
  class AnalysisInput;
  class ExtendedAnalysisResultList_t;
  class AnalysisResultList_t;

  class AutomaticAnalysisBaseROD : public IAnalysisPerROD 
  {
  public:
    AutomaticAnalysisBaseROD(SerialNumber_t analysis_serial_number, const ObjectInfo_t &);
    ~AutomaticAnalysisBaseROD() {};

    /** 
     * The public funcitions are implemented once in the base class.
     * To implement analysis specific parts use the functions listed below.
     */
    void requestHistograms(IAnalysisDataServer *data_server, const PixA::RODLocationBase &rod_location);

    void analyse(IAnalysisDataServer *data_server, const PixA::RODLocationBase &rod_location, ExtendedAnalysisResultList_t &analysis_results);

    PixLib::Config &config()            { return m_config; }
    const std::string& name() const     { return m_name; };
    SerialNumber_t serialNumber() const { return m_serialNumber;};
    void requestAbort()                 { m_abortRequest=true;};

    void setLogLevel(ELogLevel log_level) {
      m_logLevel = log_level;
    }

  protected:
    bool abortRequested() const         { return m_abortRequest; };

    SerialNumber_t          m_serialNumber;
    ELogLevel               m_logLevel;

    bool                    m_abortRequest;
    string                  m_name;
    string                  m_pp0_name;

    /**
     * These are the function that should be defined by each derived analysis class:
     */
  protected:
    virtual void newAnalysisInput    () {}; 
    virtual void addNewAnalysisInput (AnalysisInput_t analysisInput) {};  
    virtual void requestScanConfigs  (IAnalysisDataServer *ana_data_server) {}; 
    virtual void getBasics           (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter) {}; 
    virtual void getScanConfigs      (PixLib::Config&   scan_config) {};
    virtual void getHistograms       (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter) {};
    virtual void saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result) {};
    virtual void analyseResultsPerPP0(IAnalysisDataServer *ana_data_server,ExtendedAnalysisResultList_t &analysis_results, PixA::Pp0List::const_iterator pp0_iter) {};
    virtual void analyseResultsPerROD(IAnalysisDataServer *ana_data_server,ExtendedAnalysisResultList_t &analysis_results){};
    // ****************************************

  private:
    void getScanConfigs  (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter);
    void saveAnalysisResults(AnalysisResultList_t &analysis_results, AnalysisResult_t results); 

  protected:
    unique_ptr<ROOTanalysisBase>  m_analysis;

    string                      m_scan_histo_name; // containing the histo name
    string                      m_ROD_name;

    PixLib::Config              m_config;

    HistoConverter              converter;
  };

}
#endif

