#ifndef _CAN_AutomaticINTIMETHRAnalysis_hh_
#define _CAN_AutomaticINTIMETHRAnalysis_hh_

#include "AutomaticAnalysisBase.hh"
#include "INTIMETHRanalysisROOT.h"
#include "coreAnalyses/analysisInput_INTIMETHR.h"
#include "coreAnalyses/analysisOutput_INTIMETHR.h"



namespace CAN {


  /** 
   *  Analyzes threshold mean, noise and chi2 histograms, for one pp0  
   */
  class AutomaticINTIMETHRAnalysis : public AutomaticAnalysisBase 
  {
  public:
    AutomaticINTIMETHRAnalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &);
    ~AutomaticINTIMETHRAnalysis() {};

  private:
    void newAnalysisInput() { m_newInput = new AnalysisInputINTIMETHR(); };
    void addNewAnalysisInput(AnalysisInput_t analysisInput) { analysisInput->Add(m_newInput); };  
    ////
    void requestScanConfigs  (IAnalysisDataServer *ana_data_server); 
    void requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name);
    void getBasics       (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos); 
    void getScanConfigs  (PixLib::Config&   scan_config);
    void getModuleConfigs(PixLib::Config& module_config);
    void getHistograms   (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos);
    void saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result);

  private:
    std::string             m_histo_type_chi2; 
    std::string             m_histo_type_mean; 
    std::string             m_histo_type_sigma; 
    AnalysisInputINTIMETHR* m_newInput;
  };

}
#endif

