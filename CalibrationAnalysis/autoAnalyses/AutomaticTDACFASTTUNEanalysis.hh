#ifndef _CAN_AutomaticTDACFASTTUNEanalysis_hh_
#define _CAN_AutomaticTDACFASTTUNEanalysis_hh_

#include "AutomaticAnalysisBase.hh"
#include "TDACFASTTUNEanalysisROOT.h"
#include "coreAnalyses/analysisInput_TDACFASTTUNE.h"
#include "coreAnalyses/analysisOutput_TDACFASTTUNE.h"
#include "ExtendedAnalysisResultList_t.hh"



namespace CAN {

  /** 
   * Analysis for TDAC_FAST_TUNE, for one pp0 
   */
  class AutomaticTDACFASTTUNEanalysis : public AutomaticAnalysisBase 
  {
  public:
    AutomaticTDACFASTTUNEanalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &);
    ~AutomaticTDACFASTTUNEanalysis() {};
   
  private:
    void newAnalysisInput() { m_newInput = new AnalysisInputTDACFASTTUNE(); };
    void addNewAnalysisInput(AnalysisInput_t analysisInput) {analysisInput->Add(m_newInput);};
    void requestScanConfigs(IAnalysisDataServer *ana_data_server);
    void requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name);
    void getBasics(IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos);
    void getScanConfigs(PixLib::Config& scan_config);
    void getModuleConfigs(PixLib::Config& module_config);
    void getHistograms(IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos);
    void saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result);


  private:
    std::string m_histo_type_occ;
    std::string m_histo_type_tdac;
    AnalysisInputTDACFASTTUNE* m_newInput;
    int m_Ntdacsteps;
  };

}
#endif

