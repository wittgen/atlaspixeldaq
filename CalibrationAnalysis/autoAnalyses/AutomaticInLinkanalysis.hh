#ifndef _CAN_AutomaticInLinkanalysis_hh_
#define _CAN_AutomaticInLinkanalysis_hh_

#include "AutomaticAnalysisBaseROD.hh"
#include "InLinkanalysisROOT.h"
#include "coreAnalyses/analysisInput_InLink.h"
#include "coreAnalyses/analysisOutput_InLink.h"



namespace CAN {


  /** 
   *  Analyzes threshold mean, noise and chi2 histograms, for one pp0  
   */
  class AutomaticInLinkanalysis : public AutomaticAnalysisBaseROD 
  {
  public:
    AutomaticInLinkanalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &);
    ~AutomaticInLinkanalysis() {};

  private:
    void newAnalysisInput() { m_newInput = new AnalysisInputInLink(); };
    void addNewAnalysisInput(AnalysisInput_t analysisInput) { analysisInput->Add(m_newInput); };  
    ////
    void requestScanConfigs  (IAnalysisDataServer *ana_data_server); 
    void requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name);
    void getBasics       (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter); 
    void getScanConfigs  (PixLib::Config&   scan_config);
    void getModuleConfigs(PixLib::Config& module_config);
    void getHistograms   (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter);
    void saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result);
    void analyseResultsPerPP0(IAnalysisDataServer *ana_data_server,ExtendedAnalysisResultList_t &analysis_results, PixA::Pp0List::const_iterator pp0_iter);
    void analyseResultsPerROD(IAnalysisDataServer *ana_data_server,ExtendedAnalysisResultList_t &analysis_results);

  private:

    //We only store one 2D histogram per ROD
    const TH2*                    global_histo_InLink;
    std::string             global_ROD_name;
    std::string             global_pp0name;
    std::string             m_histo_type_inlink; 
    AnalysisInputInLink*    m_newInput;
    // Status flag for all channels (0~7) per Tx plugin. 
    bool                    m_Status_AllChannel;
    bool                    m_Status_AllPp0;
  };

}
#endif

