#include "AutomaticDIGITALanalysis.hh"
#include "ConfigWrapper/ConnectivityUtil.h"

namespace CAN {

  void AutomaticDIGITALanalysis::requestHistograms(IDataServer *data_server, 
						   const PixA::Pp0LocationBase &pp0_location)
  {
    m_histo_type = "OCCUPANCY"; //for DIGITAL scans

    /// LOOP over modules 
    const PixA::ModuleList &module_list=data_server->connectivity().modules(pp0_location);
    PixA::ModuleList::const_iterator module_iter=module_list.begin();
    PixA::ModuleList::const_iterator module_last=module_list.end();
    for(; module_iter != module_last && !abortRequested(); ++module_iter) {
	std::string conn_name = PixA::connectivityName( module_iter );  

	data_server->requestHistogram( m_histo_type, conn_name);  
    }
  }
    
  void AutomaticDIGITALanalysis::analyse(IDataServer *data_server, 
					 const PixA::Pp0LocationBase &pp0_location, 
					 AnalysisResultList_t &analysis_results) 
  {
    DataServerCT * dataServer = dynamic_cast<DataServerCT*>(data_server);

    AnalysisInput_t analysisInput;
    if(dataServer!=NULL) { //in case we are reading CT files  

      analysisInput = ExtractDIGITALdata( data_server->getHistograms( m_histo_type, m_pp0_name), dataServer->getScanConfigRefs(m_histo_type,m_pp0_name) );
    
      m_analysis.WriteRootFile(dataServer->GetFileName().c_str(),analysisInput,pp0_location);
      
      m_analysis.SetDebugLevel(1);
      m_analysis.Analyse(analysisInput);
    }
    else { // when using HistoServer
      analysisInput = new TObjArray(); 

      const PixA::ModuleList &module_list=data_server->connectivity().modules(pp0_location);
      PixA::ModuleList::const_iterator module_iter=module_list.begin();
      PixA::ModuleList::const_iterator module_last=module_list.end();

      /// LOOP over modules 
      for(; module_iter != module_last && !abortRequested(); ++module_iter) {
	// fill analysisInput
	ScanHistoDIGITAL * newHisto = new ScanHistoDIGITAL;

	try {
	  std::string conn_name = PixA::connectivityName( module_iter );  
	  const PixLib::Config &scan_config = data_server->getScanConfig();

	  newHisto->moduleName = (const_cast<PixLib::ModuleConnectivity*>(*module_iter))->prodId();
	  newHisto->pp0Name = m_pp0_name;

	  std::string speed = confVal<std::string>(const_cast< PixLib::Config &>(scan_config)["mcc"]["mccBandwidth"]);
	  if(speed=="SINGLE_40" || speed=="DOUBLE_40") newHisto->MBs=40;
	  else if(speed=="SINGLE_80" || speed=="DOUBLE_80") newHisto->MBs=80;
	  else newHisto->MBs=-1; //should not happen
	  
	  newHisto->axis = *converter.GetTheAxis(const_cast< PixLib::Config &>(scan_config));
	  newHisto->Nmasks  = confVal<int>(const_cast< PixLib::Config &>(scan_config)["general"]["maskStageSteps"]);
	  newHisto->Nevents = confVal<int>(const_cast< PixLib::Config &>(scan_config)["general"]["repetitions"]);
	  
	  newHisto->histo = converter.GetTheHisto2D( m_histo_type, data_server, conn_name, 0, 0, 0, 0);
	}
	catch(CAN::ConfigException &err) {
	  std::cout << "Unexpected error caught in AutomaticDIGITALanalysis.cc" << std::endl;
	}

	analysisInput->Add( newHisto );
      }
    }

    if( m_analysis.GetStatus()==ROOTanalysisBase::kPassed ) {
      // fill the generic result record ...
      TIter next(m_analysis.GetResult());
      while(ScanHistoDIGITAL * scan_iter = static_cast<ScanHistoDIGITAL*>(next.Next()) ) { 
	analysis_results.addValue<unsigned int>("failures",scan_iter->moduleName.Data(),scan_iter->failure);
	analysis_results.addValue<float>("average",scan_iter->moduleName.Data(),scan_iter->average);
      }
    }      
    //clean up
    TIter next(analysisInput);
    while(ScanHistoDIGITAL * scan_iter = static_cast<ScanHistoDIGITAL*>(next.Next()) )
      delete scan_iter;
    delete analysisInput;
  }

  AnalysisInput_t AutomaticDIGITALanalysis::ExtractDIGITALdata(std::list<PixA::HistoHandle*> histograms, 
							     std::list<PixA::ScanConfigRef *> scan_configs)
  {
    std::cout << " Start of ExtractDIGITALdata    list histograms " 
	      << histograms.size() << " ,  scan_config.size " << scan_configs.size() << std::endl;

    AnalysisInput_t analysisInput = new TObjArray();
    std::list<PixA::ScanConfigRef*>::const_iterator scanConfig_iter = scan_configs.begin();
    std::list<PixA::HistoHandle*>::const_iterator handle_iter=histograms.begin();
 
   
    for (; handle_iter!=histograms.end(); handle_iter++, scanConfig_iter++) {
   
      ScanHistoDIGITAL * newHisto = new ScanHistoDIGITAL;

      std::stringstream modName;
      if (!(*scanConfig_iter)->connName().empty()) {
	modName << (*scanConfig_iter)->connName() << "(" << (*scanConfig_iter)->prodName() << ")";
      }
      else {
	modName << (*scanConfig_iter)->prodName();
      }
      newHisto->moduleName = modName.str();
      newHisto->pp0Name = m_pp0_name;

      newHisto->axis = *converter.GetTheAxis_old( *handle_iter,  *scanConfig_iter);

      std::string speed = confVal<std::string>((*scanConfig_iter)->pixScanConfig()["mcc"]["mccBandwidth"]);
      if(speed=="SINGLE_40" || speed=="DOUBLE_40") newHisto->MBs=40;
      else if(speed=="SINGLE_80" || speed=="DOUBLE_80") newHisto->MBs=80;
      else newHisto->MBs=-1; //should not happen
     
      int masks = confVal<int>((*scanConfig_iter)->pixScanConfig()["general"]["maskStageSteps"]);
      newHisto->Nmasks = masks;


      int events = confVal<int>((*scanConfig_iter)->pixScanConfig()["general"]["repetitions"]);
      newHisto->Nevents = events;


      newHisto->histo = converter.GetTheHisto2D_old( *handle_iter,  *scanConfig_iter,  m_histo_type, 0, 0, 0, 0);
 
      analysisInput->Add( newHisto );
    }
    return analysisInput;
  }



}


