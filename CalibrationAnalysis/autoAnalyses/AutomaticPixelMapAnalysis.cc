#include "AutomaticPixelMapAnalysis.hh"


namespace CAN {

  AutomaticPixelMapAnalysis::AutomaticPixelMapAnalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &oi)  
    : AutomaticAnalysisBase( analysis_serial_number, oi) 
  {
    m_name="PixelMapAnalysis";
    m_config=PixLib::Config("PixelMapAnalysis");
    m_analysis = auto_ptr<PixelMapAnalysisROOT>(new PixelMapAnalysisROOT);
    
    m_config.addGroup("general");
    PixLib::ConfGroup &group=m_config["general"];
    group.addString("NameAnalog",   m_analysisNameAnalog,   "ANALOGanalysis",   "NameAnalog",   true);
    group.addString("NameDigital",  m_analysisNameDigital,  "DIGITALanalysis",  "NameDigital",  true);
    group.addString("NameThreshold",m_analysisNameThreshold,"THRESHOLDanalysis","NameThreshold",true);
    group.addString("NameBond",     m_analysisNameBond,     "BONDanalysis",     "NameBond",     true);
    group.addString("NameTOT",      m_analysisNameTOT,      "TOTanalysis",      "NameTOT",      true);
    group.addString("NameCrossTalk",m_analysisNameCrossTalk,"CROSSTALKanalysis","NameCrossTalk",true);
    
    // There are no histo inputs

    std::cout << "AutomaticPixelMapAnalysis:   constructed." << std::endl;
  }


  void AutomaticPixelMapAnalysis::requestConfigs(IAnalysisDataServer *ana_data_server) 
  {
  }


  void AutomaticPixelMapAnalysis::getBasics(IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos) 
  {
    m_newInput->pp0Name = m_pp0_name;
    if(!m_CTdata) {
      m_newInput->moduleName = PixA::connectivityName( module_iter );  
    }
    else {
      // Nothing implemented in DataServerCT to retrieve results of former analyses.
    }
  }


  void AutomaticPixelMapAnalysis::getScanConfigs(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos) 
  {
    // If not all mask steps are taken, this needs to be saved in results of individual analyses!
  }

 
  void AutomaticPixelMapAnalysis::getModuleConfigs(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos) 
  {
  }


  void AutomaticPixelMapAnalysis::getHistograms(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos) 
  {
    PixelMap_t pixelmap;
    m_newInput->pm_analog = ana_data_server->getDataServer()->getResult_PixelMap(m_analysisNameAnalog);
    if(!m_newInput->pm_analog.empty())  m_newInput->use_analog=true;

    m_newInput->pm_digital = ana_data_server->getDataServer()->getResult_PixelMap(m_analysisNameDigital);    
    if(!m_newInput->pm_digital.empty())  m_newInput->use_digital=true;

    m_newInput->pm_threshold = ana_data_server->getDataServer()->getResult_PixelMap(m_analysisNameThreshold);    
    if(!m_newInput->pm_threshold.empty())  m_newInput->use_threshold=true;

    m_newInput->pm_bond = ana_data_server->getDataServer()->getResult_PixelMap(m_analysisNameBond);    
    if(!m_newInput->pm_bond.empty())  m_newInput->use_bond=true;

    m_newInput->pm_tot = ana_data_server->getDataServer()->getResult_PixelMap(m_analysisNameTOT);    
    if(!m_newInput->pm_tot.empty())  m_newInput->use_tot=true;

    m_newInput->pm_crosstalk = ana_data_server->getDataServer()->getResult_PixelMap(m_analysisNameCrossTalk);    
    if(!m_newInput->pm_crosstalk.empty())  m_newInput->use_crosstalk=true;

//     m_newInput->pm_calibrationMask = ana_data_server->getDataServer()->getResult_PixelMap(m_analysisNameCalibrationMask);    
//     if(!m_newInput->pm_calibrationMask.empty())  m_newInput->use_calibrationMask=true;

//     m_newInput->pm_datatakingMask = ana_data_server->getDataServer()->getResult_PixelMap(m_analysisNameDatatakingMask);    
//     if(!m_newInput->pm_datatakingMask.empty())  m_newInput->use_datatakingMask=true;
  }


  void AutomaticPixelMapAnalysis::saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result2) 
  {
    ResultPixelMap* module_result = static_cast<ResultPixelMap*>(module_result2);
    auto_ptr<ROOTanalysisHelper> helper(new ROOTanalysisHelper());
    for (int pixtype=0; pixtype<6; pixtype++) {
      string type(helper->getPixelTypeString(pixtype));
      analysis_results.addValue<unsigned int>("number of bad pixels for "+type+" pixel",module_result->moduleName.Data(),module_result->numBadPixels[pixtype]);
    }
    analysis_results.addValue<unsigned int>("total number of bad pixels",module_result->moduleName.Data(),module_result->numBadPixels_total);
    analysis_results.addValue<PixelMap_t>("final bad pixel map",module_result->moduleName.Data(),module_result->finalPixelMap);
    analysis_results.addValue<unsigned int>("scans used",module_result->moduleName.Data(),module_result->infoUsed);
  }

}
