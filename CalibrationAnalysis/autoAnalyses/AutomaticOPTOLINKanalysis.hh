#ifndef _CAN_AutomaticOPTOLINKanalysis_hh_
#define _CAN_AutomaticOPTOLINKanalysis_hh_

#include "AutomaticAnalysisBase.hh"
#include "OPTOLINKanalysisROOT.h"
#include "analysisInput_OPTOLINK.h"
#include "analysisOutput_OPTOLINK.h"



namespace CAN {


  /** 
   *  Analyzes threshold mean, noise and chi2 histograms, for one pp0  
   */
  class AutomaticOPTOLINKanalysis : public AutomaticAnalysisBase 
  {
  public:
    AutomaticOPTOLINKanalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &);
    ~AutomaticOPTOLINKanalysis() {};

  private:
    void newAnalysisInput() { m_newInput = new AnalysisInputOPTOLINK(); };
    void addNewAnalysisInput(AnalysisInput_t analysisInput) { analysisInput->Add(m_newInput); };  
    ////
    void requestScanConfigs  (IAnalysisDataServer *ana_data_server); 
    void requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name);
    void getBasics       (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos); 
    void getScanConfigs  (PixLib::Config&   scan_config);
    void getModuleConfigs(PixLib::Config& module_config);
    void getHistograms   (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos);
    void saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result);

  private:
    std::string    m_histo_type; 
    AnalysisInputOPTOLINK* m_newInput;
  };

}
#endif

