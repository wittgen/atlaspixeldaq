#include <AnalysisFactory.hh>

#include <AutomaticTHRESHOLDAnalysis.hh>
//#include <AutomaticANNAanalysis.hh>
// #include <AutomaticJENSAnalysis.hh>
#include <AutomaticTOTanalysis.hh>
#include <AutomaticTotBothCapAnalysis.hh>
#include <AutomaticBONDAnalysis.hh>
#include <AutomaticBOCAnalysis.hh>
#include <AutomaticOPTOLINKanalysis.hh>
#include <AutomaticBASICTHRAnalysis.hh>
#include <AutomaticT0analysis.hh>
#include <AutomaticInLinkanalysis.hh>
#include <AutomaticInLinkOutLinkanalysis.hh> 
#include <AutomaticDIFFAnalysis.hh>
#include <AutomaticTOTbasicAnalysis.hh>
#include <AutomaticOCCUPANCYanalysis.hh>
#include <AutomaticTDACFASTTUNEanalysis.hh>
#include <AutomaticBUMPAnalysis.hh>
#include <AutomaticMONLEAKAnalysis.hh>
#include <AutomaticINTIMETHRAnalysis.hh>
#include <AutomaticNOISEanalysis.hh>

DEFINE_SEAL_MODULE ()

DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::AutomaticTHRESHOLDAnalysis, "THRESHOLDanalysis")
//DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::AutomaticANNAanalysis, "ANNAanalysis")
DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::AutomaticTOTanalysis, "TOTanalysis")
DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::AutomaticTotBothCapAnalysis, "TotBothCapAnalysis")
DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::AutomaticBONDAnalysis, "BONDAnalysis")
DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::AutomaticBOCanalysis, "BOCanalysis")
DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::AutomaticOPTOLINKanalysis, "OPTOLINKanalysis")
DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::AutomaticBASICTHRAnalysis, "BASICTHRanalysis")
DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::AutomaticT0analysis, "T0analysis")
DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::AutomaticInLinkanalysis, "InLinkanalysis")
DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::AutomaticInLinkOutLinkanalysis, "InLinkOutLinkanalysis")
DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::AutomaticDIFFAnalysis, "DIFFAnalysis")
DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::AutomaticTOTbasicAnalysis, "TOTbasicAnalysis")
DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::AutomaticOCCUPANCYanalysis, "OCCUPANCYanalysis")
DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::AutomaticTDACFASTTUNEanalysis, "TDACFASTTUNEanalysis")
DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::AutomaticBUMPAnalysis, "BUMPAnalysis")
DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::AutomaticMONLEAKAnalysis, "MONLEAKanalysis")
DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::AutomaticINTIMETHRAnalysis, "INTIMETHRanalysis")
DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::AutomaticNOISEanalysis, "NOISEanalysis")
