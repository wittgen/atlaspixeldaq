#include "HistoConverter.hh"

namespace CAN {

  PixLib::Histo* HistoConverter::GetPixLibHisto_old(const PixA::HistoHandle *histo_handle, const PixA::ScanConfigRef* scanConfig,std::string histoType, int ia, int ib, int iz, int ih) 
  {
    PixA::HistoRef histo(histo_handle->ref());

    std::vector<std::string> full_histo_name;
    makeHistoName(*scanConfig, histo, full_histo_name, histoType);

    int idx_in[4]={ia,ib,iz,ih}; //for both
    
    PixLib::Histo* h_pixlib = histo.histo(full_histo_name,idx_in); //This is the new way
    //cout << "got pixlibhisto " << h_pixlib << endl;
    
    return h_pixlib;   
    
  }

  const TH1* HistoConverter::GetTheHisto1D_old(const PixA::HistoHandle *histo_handle, const PixA::ScanConfigRef* scanConfig,std::string histoType, int ia, int ib, int iz, int ih) 
  {
    PixLib::Histo* h_pixlib(GetPixLibHisto_old(histo_handle, scanConfig,histoType,ia,ib,iz,ih));
    if(h_pixlib==0) return NULL;
    const TH1* h = dynamic_cast<const TH1*>(PixA::transform<PixLib::Histo>(h_pixlib));
    delete h_pixlib;
    return h;
  }

  const TH2* HistoConverter::GetTheHisto2D_old(const PixA::HistoHandle *histo_handle, const PixA::ScanConfigRef* scanConfig,std::string histoType, int ia, int ib, int iz, int ih) 
  {
    PixLib::Histo* h_pixlib(GetPixLibHisto_old(histo_handle, scanConfig,histoType,ia,ib,iz,ih));
    if(h_pixlib==0) return NULL;

    // std::cout << "before try" << std::endl;
    // TH1F * htemp = dynamic_cast<TH1F *>(PixA::transform<PixLib::Histo>(h_pixlib));
    // std::cout << "nDim" << h_pixlib->nDim() << std::endl;
    const TH2* h = dynamic_cast<const TH2*>(PixA::transform<PixLib::Histo>(h_pixlib));
    delete h_pixlib;
    return h;
  }

  const TH2* HistoConverter::GetTheHisto2D(std::string histo_type, IDataServer *data_server, std::string conn_name, int ia, int ib, int iz, int ih) 
  {
    PixA::Index_t index;

    if(ia!=-1) { 
      index.push_back(ia);
      //std::cout << "push_back ia" << std::endl;
    }
    if(ib!=-1) {
      index.push_back(ib);
      //std::cout << "push_back ib" << std::endl;
    }
    if(iz!=-1) {
      index.push_back(iz);
      //std::cout << "push_back iz" << std::endl;
    }
    if(ih!=-1) {
      index.push_back(ih);
      //std::cout << "push_back ih" << std::endl;
    }
    const Histo* h( data_server->getHistogram(histo_type, conn_name, index) );
    if(h==NULL) {
      std::cout << "ERROR did not find histo with index " << ia << ib << iz << ih << std::endl; 
      return NULL;
    }
    //std::cout<<"mean is:"<<h->GetMean()<<std::endl;
    //TH2F* h_th2f = dynamic_cast<TH2F*>(transformHisto(h));
    const TH2* h_th2 = dynamic_cast<const TH2*>(h);
    if(h_th2==NULL) std::cout << "ERROR could not convert to TH2* !!" << std::endl;
    return h_th2;
  }

  std::vector<const TH2*> HistoConverter::GetTheHisto2Dvect(std::string histo_type, IDataServer *data_server, std::string conn_name, int ia, int ib, int iz, int ih) 
  {
    std::vector<const TH2*> vect;
    std::cout << "JDDBG: Vectorizing 2D histos... ";

    HistoInfo_t info;
    data_server->numberOfHistograms(histo_type, conn_name, info);

    std::cout << "Maximum Loop Indices: " << info.maxIndex(0) << " " << info.maxIndex(1) << " " << info.maxIndex(2) << std::endl;

    int counter = 0;

    if (!(info.maxIndex(0))) {
      std::cout << " only one histogram of type " << histo_type << " histos for " << conn_name;
      const TH2* h( GetTheHisto2D(histo_type, data_server, conn_name, ia, ib, -1, ih) );
      if(h!=NULL) {
        vect.push_back(h);
	counter++;
      }
    } else {
      std::cout << info.maxIndex(0) << " histograms of type " << histo_type << " histos for " << conn_name;
      for(uint IZ=0; IZ<info.maxIndex(0); IZ++) { //info.min_index(2)
	const TH2* h( GetTheHisto2D(histo_type, data_server, conn_name, ia, ib, IZ, ih) );
	if(h!=NULL) {
          vect.push_back(h);
	  counter++;
	}
      }
    }
    
    std::cout << " [done, got " << counter << " histograms]" << std::endl;
    
    return vect;
  }

  std::vector<const TH2*> HistoConverter::GetTheHisto2Dvect_old(const PixA::HistoHandle *histo_handle, const PixA::ScanConfigRef* scanConfig,std::string histoType, int ia, int ib, int iz, int ih) 
  {
    std::vector<const TH2*> vect;
    PixA::HistoRef histo(histo_handle->ref());
    std::vector<std::string> full_histo_name;
    makeHistoName(*scanConfig, histo, full_histo_name, histoType);

    HistoInfo_t info;
    histo.numberOfHistos(full_histo_name,info);

    for (uint IZ=0;IZ<info.maxIndex(2);IZ++) {
      const TH2* h( GetTheHisto2D_old(histo_handle, scanConfig,histoType,ia,ib,IZ,ih) );
      if(h!=NULL) vect.push_back(h);
    }
    return vect;
  }

  std::vector<histoAxis_t> * HistoConverter::GetTheAxis_old(const PixA::HistoHandle *histo_handle, const PixA::ScanConfigRef* scanConfig) 
  {
    //    PixA::HistoHandle * a_histo_handle(*handle_handle);
    PixA::PixDbDataContainer container( *scanConfig, *histo_handle );

    std::vector<histoAxis_t> * axis = new std::vector<histoAxis_t>; //all axis of the histogram
    for (unsigned int loop_i=0; loop_i<3; loop_i++) {
      //CLA
      histoAxis_t one_axis = *(new histoAxis_t());
      one_axis.variableName = container.getScanPar(loop_i);
      one_axis.firstBin = container.getScanStart(loop_i);
      one_axis.lastBin = container.getScanStop(loop_i);
      one_axis.nSteps = container.getScanSteps(loop_i);
      ///
      //	std::cout << loop_i << " : " << one_axis.variableName << " :: "
      //		  << one_axis.firstBin << " - " << one_axis.lastBin << " / " << one_axis.nSteps 
      //		  << std::endl;
      if(one_axis.nSteps!=0) axis->push_back(one_axis);
    }
    //      std::cout << " events = " << container.getNevents() << std::endl;
    return axis;
  }

  std::vector<histoAxis_t> * HistoConverter::GetTheAxis(PixLib::Config &scan_conf) 
  {
    std::vector<histoAxis_t> * axis = new std::vector<histoAxis_t>; //all axis of the histogram

    for (unsigned int loop_i=0; loop_i<3; loop_i++) {
      histoAxis_t one_axis = *(new histoAxis_t());

      std::stringstream lnum;
      lnum << loop_i;
      one_axis.nSteps = confVal<int>(scan_conf["loops"]["loopVarNStepsLoop_"+lnum.str()]);
      ///
      if(one_axis.nSteps!=0) { 
	one_axis.variableName = confVal<std::string>(scan_conf["loops"]["paramLoop_"+lnum.str()]);
	one_axis.firstBin = confVal<float>(scan_conf["loops"]["loopVarMinLoop_"+lnum.str()]);
	one_axis.lastBin = confVal<float>(scan_conf["loops"]["loopVarMaxLoop_"+lnum.str()]);
//	if (one_axis.firstBin != 0.) one_axis.lastBin = static_cast<int>( one_axis.firstBin+one_axis.firstBin/abs(one_axis.firstBin)*one_axis.nSteps );
//	else one_axis.lastBin = one_axis.firstBin;
	axis->push_back(one_axis);
      }
    }
    return axis;
  }



  //from PixScanHistoVisualiser
  void HistoConverter::makeHistoName(const PixA::ScanConfigRef &scan_config, PixA::HistoRef &histo, std::vector<std::string> &histo_name, const std::string histoType) const
  {
    enum EModuleNameingConvention {kUnknownModuleNaming,kNewModuleNaming, kOldModuleNaming } m_moduleNaming(kUnknownModuleNaming);
    unsigned int m_modID(static_cast<unsigned int>(-1));

    histo_name.push_back(histoType);
    if (m_moduleNaming!= kOldModuleNaming) {
      histo_name.push_back(scan_config.connName());
      if (m_moduleNaming == kUnknownModuleNaming) {

	if (histo.haveHisto(histo_name)) {
	  m_moduleNaming=kNewModuleNaming;
	}
	else {
	  histo_name.pop_back();
	}
      }
    }
    if (histo_name.size()==1) {
      if (m_modID==static_cast<unsigned int>(-1)) {
	try {
	  m_modID=confVal<int>(scan_config.modConfig()["general"]["ModuleId"]);
	}catch(...) {
	}
      }
      std::stringstream a_name;
      a_name << "Mod" << m_modID;
      histo_name.push_back(a_name.str());
     if (m_moduleNaming == kUnknownModuleNaming) {
	if (histo.haveHisto(histo_name)) {
	  m_moduleNaming=kOldModuleNaming;
	}
      }
    }
  }

  unsigned int HistoConverter::getTotalMaskStageSteps(const std::string &label) 
  {
    /*    if(label=="STEPS_1_1_DC") return 1;
    else if(label=="STEPS_2_2_DC") return 2;
    else if(label=="STEPS_3_3_DC") return 3;
    else if(label=="STEPS_4_4_DC") return 4;
    else if(label=="STEPS_5_5_DC") return 5;
    else if(label=="STEPS_6_6_DC") return 6;
    else if(label=="STEPS_7_7_DC") return 7;
    else if(label=="STEPS_8_8_DC") return 8;
    else if(label=="STEPS_32_32_DC") return 32;
    else if(label=="STEPS_1_DC") return 1;
    else if(label=="STEPS_2_DC") return 2;
    else if(label=="STEPS_3_DC") return 3;
    else if(label=="STEPS_4_DC") return 4;
    else if(label=="STEPS_5_DC") return 5;
    else if(label=="STEPS_6_DC") return 6;
    else if(label=="STEPS_7_DC") return 7;
    else if(label=="STEPS_8_DC") return 8;
    else if(label=="STEPS_32_DC") return 32;
    else if(label=="STEPS_1") return 1;
    else if(label=="STEPS_2") return 2;
    else if(label=="STEPS_3") return 3;
    else if(label=="STEPS_4") return 4;
    else if(label=="STEPS_5") return 5;
    else if(label=="STEPS_6") return 6;
    else if(label=="STEPS_7") return 7;
    else if(label=="STEPS_8") return 8; 
    else if(label=="STEPS_32") return 32;   
    else if(label=="STEPS_1_1") return 1;
    else if(label=="STEPS_2_2") return 2;
    else if(label=="STEPS_3_3") return 3;
    else if(label=="STEPS_4_4") return 4;
    else if(label=="STEPS_5_5") return 5;
    else if(label=="STEPS_6_6") return 6;
    else if(label=="STEPS_7_7") return 7;
    else if(label=="STEPS_8_8") return 8;
    else if(label=="STEPS_32_32") return 32;
    else if(label=="STEPS_40") return 40;
    else if(label=="STEPS_64") return 64;
    else if(label=="STEPS_80") return 80;
    else if(label=="STEPS_160") return 160;
    else if(label=="STEPS_320") return 320;
    else if(label=="STEPS_2880") return 2880;
    else if(label=="STEPS_26880_DC") return 26880;*/

    std::string trunca;
    trunca = label.substr(6,label.size());
    std::cout<<"truncated label is: "<<trunca<<std::endl;
    std::size_t pos = trunca.find("_");
    if(pos==std::string::npos)pos=trunca.size();
    std::string number;
    number = trunca.substr(0,pos); 
    std::cout<<"number label is: "<<number<<std::endl;
    unsigned int ret = (unsigned int)atoi(number.c_str());
    std::cout<<"the integer is: "<<ret<<std::endl;
    return ret;  
    return 0;
  }

  TObject* HistoConverter::transformHisto(const Histo* h) {
    if (!h) return NULL;
    
    if (h->GetDimension()==1) {
      
      int NbinsX = h->GetNbinsX();
      float BinWidthX = h->GetBinWidth(1);
      float MinX = h->GetBinCenter(1)-BinWidthX/2.;
      float MaxX = h->GetBinCenter(NbinsX)+BinWidthX/2.;

      TH1 *h1 = new TH1F(h->GetName(),h->GetTitle(),NbinsX,MinX,MaxX);
      h1->SetDirectory(0);
      int i;
      for (i=0; i!=NbinsX; ++i) {
	h1->SetBinContent(i+1,static_cast<float>(h->GetBinContent(i+1)));
      }
      return h1;

    }
    else if (h->GetDimension()==2) {
      
      int NbinsX = h->GetNbinsX();
      float BinWidthX = h->GetBinWidth(1);
      float MinX = h->GetBinCenter(1)-BinWidthX/2.;
      float MaxX = h->GetBinCenter(NbinsX)+BinWidthX/2.;
      
      int NbinsY = h->GetNbinsY();
      float BinWidthY = h->GetBinWidth(1);
      float MinY = h->GetBinCenter(1)-BinWidthY/2.;
      float MaxY = h->GetBinCenter(NbinsY)+BinWidthY/2.;
      
      TH2 *h2 = new TH2F(h->GetName(),h->GetTitle(),NbinsX,MinX,MaxX,NbinsY,MinY,MaxY);
      
      h2->SetDirectory(0);

      int i,j;
      float a_min=FLT_MAX;
      float a_max=-FLT_MAX;
      for (i=0; i!=NbinsX; ++i) {   
	for (j=0; j!=NbinsY; ++j) {
	  h2->SetBinContent(i+1,j+1,static_cast<float>(h->GetBinContent(i+1,j+1)));
	  if (h->GetBinContent(i+1,j+1) < a_min ) a_min = h->GetBinContent(i+1,j+1);
	if (h->GetBinContent(i+1,j+1) > a_max ) a_max = h->GetBinContent(i+1,j+1);
	}
      }
      if (a_min==a_max) {
	if (a_min==0.) {
	  h2->SetMinimum(-1.);
	  h2->SetMaximum(1.);
	}
	else {
	  h2->SetMinimum(a_min*(1-1e-3));
	  h2->SetMaximum(a_min*(1+1e-3));
	}
      }
      return h2;

    }
    else {
      return NULL;
    }
  }



} //end namespace CAN
