#include "AutomaticBUMPAnalysis.hh"
#include "../Lock.hh"
#include "../PixDbGlobalMutex.hh"


namespace CAN {

  AutomaticBUMPAnalysis::AutomaticBUMPAnalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &oi)
    : AutomaticAnalysisBase( analysis_serial_number, oi)
  {
    std::cout << "AutomaticBUMPAnalysis start" << std::endl;
    m_name="BUMPanalysis";
    m_config=PixLib::Config("BUMPanalysis");
    m_analysis = unique_ptr<BUMPanalysisROOT>(new BUMPanalysisROOT);
 
    m_config.addGroup("general");
    PixLib::ConfGroup &group=m_config["general"];
    group.addString("ScanNameXtalk", m_xtalkScan, "xtalk","scanNameXtalk", true);
    group.addString("ScanNameAnalog", m_analogScan, "analog","scanNameAnalog", true);
//*************************************************************
    group.addString("ScanNameAnalog_merg", m_analogScan_merg, "analog_merg","scanNameAnalog_merg", true);
//******************************************************************************************
    group.addString("ScanNameThreshold", m_thresholdScan, "threshold","scanNameThreshold", true);
    group.addFloat("normpix noisecut", (static_cast<BUMPanalysisROOT*>(m_analysis.get()))->m_normalNoiseCut, 160, "noise max cut for normal pixels", true);
    group.addFloat("longpix noisecut", (static_cast<BUMPanalysisROOT*>(m_analysis.get()))->m_longNoiseCut, 240, "noise max cut for long and ganged pixels", true);
    group.addFloat("minHitsAnalog", (static_cast<BUMPanalysisROOT*>(m_analysis.get()))->m_minHitsAnalog, 1, "minimum hits in analog scan for healthy pixel", true);
    group.addFloat("minHitsXtalk", (static_cast<BUMPanalysisROOT*>(m_analysis.get()))->m_minHitsXtalk, 1, "minimum hits in xtalk scan for healthy pixel", true);
//*********************************************************************
//group.addBool("is_merged", (static_cast<BUMPanalysisROOT*>(m_analysis.get()))->m_is_merged, false, "to have a merged bump analysis apart from the dead_pixel one", true);
//**********************************************************************************************
      
      
      
    std::cout << "AutomaticBUMPAnalysis end" << std::endl;
  }
    
    void AutomaticBUMPAnalysis::fillNamedScans(){
        vector<string> occupancy;
        occupancy.push_back((string)"OCCUPANCY");
        vector<string> sigma;
        sigma.push_back((string)"SCURVE_CHI2");
        sigma.push_back((string)"SCURVE_MEAN");
        sigma.push_back((string)"SCURVE_SIGMA");
        m_scan_histo_names.clear();
        std::cout<<"size is: "<<m_scan_histo_names.size()<<std::endl;
        m_scan_histo_names.insert(make_pair(m_xtalkScan,occupancy));
        m_scan_histo_names.insert(make_pair(m_analogScan,occupancy));
        m_scan_histo_names.insert(make_pair(m_thresholdScan,sigma));
        //*********************************************************************
        m_scan_histo_names.insert(make_pair(m_analogScan_merg,occupancy));
        //**********************************************************************************************
        std::cout<<"size is: "<<m_scan_histo_names.size()<<std::endl;
    }

  void AutomaticBUMPAnalysis::requestScanConfigs(IAnalysisDataServer *ana_data_server) 
  {
    std::cout << "requestScanConfigs start" << std::endl;
      ana_data_server->getDataServer(m_xtalkScan)->requestScanConfig();
      ana_data_server->getDataServer(m_analogScan)->requestScanConfig();
      ana_data_server->getDataServer(m_analogScan_merg)->requestScanConfig();
      ana_data_server->getDataServer(m_thresholdScan)->requestScanConfig();
    std::cout << "requestScanConfigs end" << std::endl;
  }

  void AutomaticBUMPAnalysis::requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name) 
  {
    std::cout << "requestModuleConfigs start" << std::endl;
    std::cout << "module name is "<< module_name << std::endl;
    ana_data_server->getDataServer(m_xtalkScan)->requestModuleConfig(module_name);
    ana_data_server->getDataServer(m_analogScan)->requestModuleConfig(module_name);
    ana_data_server->getDataServer(m_thresholdScan)->requestModuleConfig(module_name);
    ana_data_server->getDataServer(m_analogScan_merg)->requestModuleConfig(module_name);

    std::cout<<"AutomaticBUMPAnalysis:: requesting configuration for module "<<module_name<<std::endl;
    std::cout << "requestModuleConfigs end" << std::endl;
  }

  void AutomaticBUMPAnalysis::getHistograms(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos) 
  {
    std::cout << "in getHistograms" << std::endl;
    CAN::Lock lock( CAN::PixDbGlobalMutex::mutex() );

    string conn_name = PixA::connectivityName( module_iter );  
  
    PixA::Index_t indexOccupancy;
    PixA::Index_t indexSigma;
    indexOccupancy.clear();
    indexSigma.clear();
    indexOccupancy.push_back(0);
    for(int ii = 1 ; ii < 4 ; ii++){
      indexOccupancy.push_back(0);
      indexSigma.push_back(0);
    }
    const TH2* histo_xtalk = dynamic_cast<const TH2*> (ana_data_server->getDataServer(m_xtalkScan)->getHistogram("OCCUPANCY", conn_name, indexOccupancy));
    const TH2* histo_analog = dynamic_cast<const TH2*> (ana_data_server->getDataServer(m_analogScan)->getHistogram("OCCUPANCY", conn_name, indexOccupancy));
    const TH2* histo_sigma = converter.GetTheHisto2D("SCURVE_SIGMA",ana_data_server->getDataServer(m_thresholdScan), conn_name, -1,0,0,0);
//*********************************************************************
const TH2* histo_analog_merg = dynamic_cast<const TH2*> (ana_data_server->getDataServer(m_analogScan_merg)->getHistogram("OCCUPANCY", conn_name, indexOccupancy));
//**********************************************************************************************

    //    const TH2* histo_sigma = dynamic_cast<const TH2*> (ana_data_server->getDataServer(m_thresholdScan)->getHistogram("SCURVE_SIGMA", conn_name, indexSigma));

    if (histo_xtalk == NULL){
      std::cout << "no xtalk histo found" << std::endl;
      std::cout << "The hit occupancy plot might not be stored in the root-file." << std::endl;
      std::cout << "Please check the check-button to store the occupancy plot in config-editor window." << std::endl;
    }

    PixGeometry geo(PixA::nRows(histo_xtalk),PixA::nColumns(histo_xtalk)); 
    m_feflv = geo.pixType();
    std::cout << "FE flavor: " << m_feflv << std::endl;

    m_newInput->histo_xtalk = histo_xtalk;
    m_newInput->histo_analog = histo_analog;
    m_newInput->histo_sigma = histo_sigma;
//*********************************************************************
m_newInput->histo_analog_merg = histo_analog_merg;
//**********************************************************************************************
std::cout << "end getHistograms" << std::endl;
  }
  
  void AutomaticBUMPAnalysis::getBasics(IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos) 
  {
    std::cout << "getBasics start" << std::endl;
    m_newInput->pp0Name = m_pp0_name;
    m_newInput->moduleName = getModuleName( ana_data_server, module_iter, pos);  
    std::cout << "getBasics end" << std::endl;
  }


  void AutomaticBUMPAnalysis::getScanConfigs(PixLib::Config&   scan_config) 
  {
    std::cout << "getScanConfigs start" << std::endl;
      m_newInput->Nmasks = confVal<int>(scan_config["general"]["maskStageSteps"]);
      m_newInput->Nevents = confVal<int>(scan_config["general"]["repetitions"]);
      m_newInput->NTotmasks = (int) converter.getTotalMaskStageSteps(confVal<std::string>(const_cast< PixLib::Config &>(scan_config)["general"]["maskStageTotalSteps"]));

    /*
    unique_ptr<ROOTanalysisHelper> helper(new ROOTanalysisHelper());

    float runMS = confVal<int>(const_cast< PixLib::Config &>(scan_config)["general"]["maskStageSteps"]);
    float totMS = converter.getTotalMaskStageSteps(confVal<std::string>(const_cast< PixLib::Config &>(scan_config)["general"]["maskStageTotalSteps"]));
    m_newInput->nMaskStages = (int)runMS;
    m_newInput->nPixel = (int)(helper->Ncols * helper->Nrows * runMS / totMS);
    std::cout << "Scanned " << runMS << " mask stages out of " << totMS << " = " << m_newInput->nPixel << " pixels out of " << helper->Ncols * helper->Nrows << std::endl;
    */
  }
  
 
  void AutomaticBUMPAnalysis::getModuleConfigs(PixLib::Config& module_config) 
  {
    std::cout << "getModuleConfigs start" << std::endl;
    getDisabledFECP (module_config, m_feflv , m_newInput->fe_disable, m_newInput->cp_enable);
    std::cout << "getModuleConfigs end" << std::endl;
  }


  void AutomaticBUMPAnalysis::saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result2) 
  {
    ResultBUMP* module_result = static_cast<ResultBUMP*>(module_result2);
    ExtendedAnalysisResultList_t& extended_results = (ExtendedAnalysisResultList_t&)analysis_results;

    analysis_results.addValue<float>("nAnalogDead",module_result->moduleName.Data(),module_result->nAnalogDead);
    analysis_results.addValue<float>("nXtalkDead",module_result->moduleName.Data(),module_result->nXtalkDead);
    analysis_results.addValue<float>("nDisconnected",module_result->moduleName.Data(),module_result->nDisconnected);
    extended_results.addHisto<TH1>("DisconnectedMap",module_result->moduleName.Data(),std::shared_ptr<TH1>(module_result->disconnectedPixels));
    //*************************************************************
    analysis_results.addValue<float>("nAnalogMerged",module_result->moduleName.Data(),module_result->nAnalogMerged);
    //**************************************************************************
  }
}


