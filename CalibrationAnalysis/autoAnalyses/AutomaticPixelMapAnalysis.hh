#ifndef _CAN_AutomaticPixelMapAnalysis_hh_
#define _CAN_AutomaticPixelMapAnalysis_hh_

#include "AutomaticAnalysisBase.hh"
#include "PixelMapAnalysisROOT.h"
#include "coreAnalyses/analysisInput_PixelMap.h"
#include "coreAnalyses/analysisOutput_PixelMap.h"



namespace CAN {


  /** 
   *  Meta analysis combining the pixel maps from: 
   *  Analog, digital, threshold, bond, tot and the calibration and data-taking maskes 
   *  to construct the special pixel map for the offline.
   */
  class AutomaticPixelMapAnalysis : public AutomaticAnalysisBase 
  {
  public:
    AutomaticPixelMapAnalysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &);
    ~AutomaticPixelMapAnalysis() {};

  private:
    void newAnalysisInput() { m_newInput = new AnalysisInputPixelMap(); };
    void addNewAnalysisInput(AnalysisInput_t analysisInput) { analysisInput->Add(m_newInput); };  
    ////
    void requestConfigs  (IAnalysisDataServer *ana_data_server); 
    void getBasics       (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos); 
    void getScanConfigs  (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos);
    void getModuleConfigs(IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos);
    void getHistograms   (IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos);
    void saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result);

  private:
    std::string             m_histo_type_chi2; 
    std::string             m_histo_type_mean; 
    std::string             m_histo_type_sigma; 
    AnalysisInputPixelMap* m_newInput;

    std::string m_analysisNameAnalog;
    std::string m_analysisNameDigital;
    std::string m_analysisNameThreshold;
    std::string m_analysisNameBond;
    std::string m_analysisNameTOT;
    std::string m_analysisNameCrossTalk;
  };

}
#endif

