#include "AutomaticT0analysis.hh"

#include "PixFe/PixFe.h"
#include "PixFe/PixFeI3.h"
#include "PixFe/PixFeI4.h"

namespace CAN {

  AutomaticT0analysis::AutomaticT0analysis(SerialNumber_t analysis_serial_number, const ObjectInfo_t &oi)
    : AutomaticAnalysisBase( analysis_serial_number, oi)
  {
    m_name="T0analysis";
    m_config=PixLib::Config("T0analysis");
    m_analysis = make_unique<T0analysisROOT>();
    T0analysisROOT* anptr = static_cast<T0analysisROOT*>(m_analysis.get());

    m_config.addGroup("general");
    PixLib::ConfGroup &group=m_config["general"];
    group.addFloat("delay_offset", anptr->delay_offset, 5.0, "Delay offset after full occupancy in ns ", true);
    group.addFloat("pixel_occ", anptr->pixel_occ, 95, "Percentage of occupancy ", true);
    group.addFloat("rising_edge_occ", anptr->rising_edge_occ, 75, "Percentage of occupancy to determine the rising edge ", true);
    group.addFloat("falling_edge_occ", anptr->falling_edge_occ, 25, "Percentage of occupancy to determine the falling edge ", true);
    group.addFloat("T0_occ", anptr->T0_occ, 95, "Percentage of occupancy to determine T0 ", true);
    group.addInt("min_del_diff", anptr->min_del_diff, 10, "Minimum delay difference between rising and falling edges ", true);

    m_histo_type = "OCCUPANCY";
    m_scan_histo_names[""] = std::vector<std::string> { "OCCUPANCY" };
    std::cout << "AutomaticT0analysis: constructed.\n";
  }

  void AutomaticT0analysis::requestScanConfigs(IAnalysisDataServer *ana_data_server) 
  {
    ana_data_server->getDataServer()->requestScanConfig();
  }

  void AutomaticT0analysis::requestModuleConfigs(IAnalysisDataServer *ana_data_server, string module_name) 
  {
    ana_data_server->getDataServer()->requestModuleConfig(module_name);
  }

  void AutomaticT0analysis::getBasics(IAnalysisDataServer *ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos) 
  {
    std::cout<<__PRETTY_FUNCTION__ << " start." << std::endl;
    m_newInput->pp0Name = m_pp0_name;
    m_newInput->moduleName = getModuleName( ana_data_server, module_iter, pos);
    
    IDataServer* data_server = ana_data_server->getDataServer();
    string conn_name = PixA::connectivityName( module_iter ); 
    // enabled status is checked in the AutomaticAnalysisBase 

    m_newModuleCfg[conn_name]= data_server->createConfiguredModule(conn_name);

    std::cout<<__PRETTY_FUNCTION__ << " end." << std::endl;
  }


  void AutomaticT0analysis::getScanConfigs(PixLib::Config& scan_config) {

    std::cout<<__PRETTY_FUNCTION__ << " start." << std::endl;

    int runMS = confVal<int>(scan_config["general"]["maskStageSteps"]);
    float totMS = converter.getTotalMaskStageSteps(confVal<std::string>(scan_config["general"]["maskStageTotalSteps"]));

    m_newInput->Nevents = confVal<int>(scan_config["general"]["repetitions"]);
    m_newInput->nMaskStages = runMS;
    m_newInput->NTotmasks = (int)totMS;
    m_newInput->nDelRangeSteps = m_delRangeSteps;
    m_newInput->nDelSteps = m_delSteps;

    std::cout<<__PRETTY_FUNCTION__<<" end"<<std::endl;

  }

  void AutomaticT0analysis::getModuleConfigs(PixLib::Config& module_config) {

    std::cout<<__PRETTY_FUNCTION__<<" start"<<std::endl;
    
    getDisabledFECP (module_config, m_feflv , m_newInput->fe_disable, m_newInput->cp_enable);
    
    std::cout<<__PRETTY_FUNCTION__ << " end" << std::endl;
  }

  void AutomaticT0analysis::getDelaySteps(const PixLib::Config& scan_config){

    m_delSteps = -1;
    m_delRangeSteps = -1;
    m_delRangeIndex=-1;
    m_delStepIndex=-1;

    auto paramLoop = getParamLoop(scan_config);

      for (auto& [paramPair, paramVector] : paramLoop){
       auto& [paramName, paramIndex] = paramPair;
       //std::cout<<paramName<<" "<<paramName<<" "<<paramVector.size()<<std::endl;
        if(paramName == "STROBE_DEL_RANGE"){
          m_newInput->delRange_array = paramVector;
          m_delRangeSteps = paramVector.size();
          m_delRangeIndex=paramIndex;
        } else if (paramName == "STROBE_DELAY"){
          m_newInput->delStep_array = paramVector;
          m_delSteps = paramVector.size();
          m_delStepIndex=paramIndex;
        }
      }

    //In case there is no loop over the strobe range
    if(m_delRangeSteps <=0){
      float val = -1;//If -1 DB value will be kept
        if(confVal<bool>(scan_config["trigger"]["overrideStrobeMCCDelayRange"]) ){
          val = confVal<int>(scan_config["trigger"]["strobeMCCDelayRange"]);//If override delay range this value will be used
        }
      m_newInput->delRange_array.emplace_back(val);
      m_delRangeSteps=1;
    }
  }

  void AutomaticT0analysis::getHistograms(IAnalysisDataServer* ana_data_server, PixA::ModuleList::const_iterator module_iter, int pos)
  {
    std::cout<<"getHisto starts "<<std::endl;
  
    if(m_CTdata) {
      DataServerCT * dataServerCT = dynamic_cast<DataServerCT*>( ana_data_server->getDataServer() );
      PixA::ConfigRef scan_config_ref = (*iterator(dataServerCT->getScanConfigRefs( *m_scan_histo_names.begin()->second.begin(), m_pp0_name).begin(),pos))->pixScanConfig();
      PixLib::Config &scan_config =const_cast<PixLib::Config &>(scan_config_ref.config());
      getDelaySteps(scan_config);
    } else {
        for (const auto& [scan_histo_name, value]: m_scan_histo_names){
          PixLib::Config &scan_config = const_cast<PixLib::Config &>(ana_data_server->getDataServer(scan_histo_name)->getScanConfig());
          getDelaySteps(scan_config);
          break;
        }
    }

    string conn_name = PixA::connectivityName( module_iter );  

    std::cout<<"Del range steps "<<m_delRangeSteps<<std::endl;

    for (int i=0; i<m_delRangeSteps; i++) {
      std::vector <const TH2*> h_array;
      for (int j=0; j<m_delSteps; j++) {
        const TH2 *h_occ = nullptr;
        int ia =0,ib=0,ic=0;

        if(m_delRangeIndex==2)ia = i;
        else if(m_delRangeIndex==1)ib = i;

        if(m_delStepIndex ==0){
          ic=j;
        } else {
          std::cout<<"Strobe delay range should be in loop0 (assuming DSP processing), while I found in index "<<m_delStepIndex<<" aborting analysis"<<std::endl;
          return;
        }

          if(m_CTdata) {
	    DataServerCT * dataServerCT = dynamic_cast<DataServerCT*>( ana_data_server->getDataServer() );
	    
	    h_occ = converter.GetTheHisto2D_old(*iterator(dataServerCT->getHistograms(m_histo_type, m_pp0_name).begin(),pos),*iterator(dataServerCT->getScanConfigRefs(m_histo_type, m_pp0_name).begin(),pos), m_histo_type, ia,ib,ic,0);
	  } else {
	    IDataServer* data_server = ana_data_server->getDataServer();
	    h_occ  = converter.GetTheHisto2D( m_histo_type, data_server, conn_name, ia,ib,ic,0);
          }

          if(h_occ){
            //std::cout<<"Got histo "<<h_occ->GetName()<<std::endl;
            h_array.push_back(h_occ);
            if(m_feflv == -1){
	      PixGeometry geo(PixA::nRows(h_occ),PixA::nColumns(h_occ)); 
	      m_feflv = geo.pixType();
	      std::cout << "FE flavor: " << (int)m_feflv << std::endl;
            }
          }
       }
      m_newInput->histo.push_back(h_array);
    }

  m_newInput->_ChipFeFlav = m_feflv;

  }

  void AutomaticT0analysis::saveAnalysisSpecificResults(AnalysisResultList_t &analysis_results, AnalysisResult* module_result2) {
    // for one module
    ResultT0* module_result = static_cast<ResultT0*>(module_result2);
    unique_ptr<ROOTanalysisHelper> helper(new ROOTanalysisHelper());
    //ExtendedAnalysisResultList_t& extended_results = (ExtendedAnalysisResultList_t&)analysis_results; //needed to output histos
    if(m_feflv == PixGeometry::FEI2_CHIP || m_feflv == PixGeometry::FEI2_MODULE){
      analysis_results.addValue("convFactor", module_result->moduleName.Data(), module_result->conv[0]);
      analysis_results.addValue("risingEdge", module_result->moduleName.Data(), module_result->rising_edge[0]);
      analysis_results.addValue("fallingEdge", module_result->moduleName.Data(), module_result->falling_edge[0]);
      analysis_results.addValue("scanFailed", module_result->moduleName.Data(), module_result->failed_scan[0]);
      analysis_results.addValue("T0", module_result->moduleName.Data(),static_cast<float>(module_result->T0[0]));
      analysis_results.addValue("DelayRange", module_result->moduleName.Data(), static_cast<float>(module_result->best_delay_range[0]));

      ExtendedAnalysisResultList_t *extended_results = static_cast<ExtendedAnalysisResultList_t *>(&analysis_results);
        if (!module_result->passed){
          std::cout << "Module "<< module_result->moduleName <<" failed T0 analysis. No module cfg update will be done." << std::endl;
          return; 
        }
        
      if (!extended_results)return;  
      auto mod_iter =  m_newModuleCfg.find(std::string(module_result->moduleName)); //??
	if (mod_iter == m_newModuleCfg.end()){
	  std::cout << "No configuration for the module "<< module_result->moduleName <<" found. Continuing w/o changing module cfg." << std::endl;
	  return;
	}
      bool changed_cfg=false;
      //compare old and new, in case of difference set new and set flag
      PixLib::ConfGroup &reg_conf = mod_iter->second->pixMCC()->config()["Registers"];

      auto& cal_delay = confVal<int>(reg_conf["CAL_Delay"]);
      auto& cal_range = confVal<int>(reg_conf["CAL_Range"]);

	if ( cal_delay != module_result->T0[0]) {
	  std::cout << module_result->moduleName << " Change cal delay from " << cal_delay << " to " << module_result->T0[0] << '\n';
	  cal_delay = module_result->T0[0];
	  changed_cfg=true;
	}
	if ( ( cal_range != module_result->best_delay_range[0] ) && module_result->best_delay_range[0] >= 0 ) {
	  std::cout << module_result->moduleName << " Change cal range from "<< cal_range << " to "<< module_result->best_delay_range[0] << '\n';
	  cal_range = module_result->best_delay_range[0];
	  changed_cfg=true;

	  //If the CAL_Range changes, also updated the strobe conv factor
          PixLib::ConfGroup &strobe_conf = mod_iter->second->pixMCC()->config()["Strobe"];
	  auto& strobe_conv = confVal<float>(strobe_conf["DELAY_"+std::to_string(module_result->best_delay_range[0])]);
	  if (strobe_conv != module_result->conv[0]){
	    std::cout << module_result->moduleName << " Changed strob conv factor from " << strobe_conv << " to " << module_result->conv[0] << '\n';
	    strobe_conv = module_result->conv[0];
	  }
	}

	if (changed_cfg) {
	  extended_results->addNewPixModuleConfig( mod_iter->second );
	  extended_results->addValue("updatedCfg", module_result->moduleName.Data(), changed_cfg);
	}
    } else if (m_feflv == PixGeometry::FEI4_CHIP || m_feflv == PixGeometry::FEI4_MODULE){

      //Granularity is MCC for FEI3 (1 chip) and FE for FE-I4 (2 Chips)
      int const maxFe = (m_feflv == PixGeometry::FEI4_CHIP) ? 1 : 2;

        for(int fe=0;fe<maxFe;fe++){
          analysis_results.addValue("convFactor_FE"+std::to_string(fe), module_result->moduleName.Data(), module_result->conv[fe]);
          analysis_results.addValue("risingEdge_FE"+std::to_string(fe), module_result->moduleName.Data(), module_result->rising_edge[fe]);
          analysis_results.addValue("fallingEdge_FE"+std::to_string(fe), module_result->moduleName.Data(), module_result->falling_edge[fe]);
          analysis_results.addValue("scanFailed_FE"+std::to_string(fe), module_result->moduleName.Data(), module_result->failed_scan[fe]);
          analysis_results.addValue("T0_FE"+std::to_string(fe), module_result->moduleName.Data(), static_cast<float>(module_result->T0[fe]));
          analysis_results.addValue("DelayRange_FE"+std::to_string(fe), module_result->moduleName.Data(), static_cast<float>(module_result->best_delay_range[fe]));
        }

      ExtendedAnalysisResultList_t *extended_results = static_cast<ExtendedAnalysisResultList_t *>(&analysis_results);

      if (!extended_results)return;  
      auto mod_iter =  m_newModuleCfg.find(std::string(module_result->moduleName));
	if (mod_iter == m_newModuleCfg.end()){
	  std::cout << "No configuration for the module "<< module_result->moduleName <<" found. Continuing w/o changing module cfg." << std::endl;
	  return; 
	}

        bool changed_cfg=false;
        for(int fe=0;fe<maxFe;fe++){
          if (!module_result->passedFE[fe]){
            std::cout << "Module "<< module_result->moduleName <<"_FE"<<fe<<" failed T0 analysis. No module cfg update will be done." << std::endl;
            continue;
          }

          PixLib::Config& globreg = mod_iter->second->pixFE(fe)->config().subConfig("GlobalRegister_0/GlobalRegister");

          auto& cal_delay = (PixLib::ConfInt&)globreg["GlobalRegister"]["PlsrDelay"];
          auto& cal_range = (PixLib::ConfInt&)globreg["GlobalRegister"]["PlsrIdacRamp"];

          //compare old and new, in case of difference set new and set flag
          if ( cal_delay.value() != module_result->T0[fe] ) {
	  std::cout << module_result->moduleName<<"_FE"<<fe<<" Change PlsrDelay from "<< cal_delay.value() <<" to "<< module_result->T0[fe] <<std::endl;
	  cal_delay.setValue(module_result->T0[fe]);
	  changed_cfg=true;
	  }
	  if ( ( cal_range.value() != module_result->best_delay_range[fe]) && (module_result->best_delay_range[fe] >=0) ) {
	    std::cout << module_result->moduleName<<"_FE"<<fe<<" Change PlsrIdacRamp from "<< cal_range.value() <<" to "<< module_result->best_delay_range[fe] <<std::endl;
	    cal_range.setValue(module_result->best_delay_range[fe]);
	    changed_cfg=true;
	  }
	}

	if (changed_cfg) {
	 std::cout << "Saving results...\n";
	 extended_results->addNewPixModuleConfig( mod_iter->second );
	 extended_results->addValue("updatedCfg",module_result->moduleName.Data(), changed_cfg);
	 std::cout << "Results saved\n";
	}
    }
  }
}


