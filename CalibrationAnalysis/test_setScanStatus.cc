#include <ipc/partition.h>
#include <ipc/core.h>

#include <is/infodictionary.h>
#include <is/inforeceiver.h>
#include <is/infoiterator.h>
#include <is/callbackinfo.h>
#include <is/infoT.h>

#include "ScanIsStatus.hh"
#include "isUtil.hh"

#include <cassert>

#include <vector>
#include <iostream>
#include <iomanip>
#include <stdexcept>

#include <time.h>

int main(int argc, char **argv) {
  // GET COMMAND LINE

  // Check arguments

  std::string ipcPartitionName;
  std::string is_server("CAN");
  std::vector<std::string> rod_name;
  std::string status_name;
  unsigned int serial_number=0;

  bool error=false;
  
  //  bool ping_only=false;
  //  bool query_queue_states=false;
  //  bool reset_queues=false;
  for (int arg_i=1; arg_i<argc; arg_i++) {

    if (strcmp(argv[arg_i],"-p")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      ipcPartitionName = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-n")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      is_server = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-r")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      rod_name.push_back( argv[++arg_i] );
    }
    else if (strcmp(argv[arg_i],"-s")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      serial_number = atoi( argv[++arg_i] );
    }
    else if (strcmp(argv[arg_i],"-t")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      status_name = argv[++arg_i];
    }
    else {
      error=true;
    }
  }

  CAN::ScanIsStatus::EStatus scan_status = CAN::ScanIsStatus::status(status_name);

  if( scan_status == CAN::ScanIsStatus::kUnknown
      || error || ipcPartitionName.empty() || is_server.empty() || status_name.empty() || rod_name.empty() || serial_number==0) {
      std::cout << "USAGE: " << argv[0] << " -p partition [-n IS-server-name] -r rod_name."  << std::endl
		<< "\t-p partition name\t the partition of the CAN/PixScan is server." << std::endl
		<< "\t-n IS-server-name\t the name of the CAN/PixScan is server." << std::endl
		<< "\t-s serial number\tThe serial number"
		<< "\t-r ROD name\t\tThe ROD for which the status should be changed." << std::endl
		<< "\t-t status\t\tThe new status name must be : ";
      for (unsigned int status_i=0; status_i<=CAN::ScanIsStatus::kAborted; status_i++) {
 	std::cout << CAN::ScanIsStatus::name(static_cast<CAN::ScanIsStatus::EStatus>(status_i)) << " ";
      }
      std::cout << std::endl;
      return 1;
  }

  // START IPC

  // Start IPCCore
  IPCCore::init(argc, argv);

  IPCPartition ipcPartition(ipcPartitionName.c_str());
  if (!ipcPartition.isValid()) {
    std::cerr << "ERROR [" << argv[0] << ":main] Not a valid partition " << ipcPartitionName << std::endl;
    return EXIT_FAILURE;
  }

  for(std::vector<std::string>::const_iterator rod_iter = rod_name.begin();
      rod_iter != rod_name.end();
      ++rod_iter) {
    std::stringstream is_val_name;
    is_val_name << is_server << '.' << "S" << std::setw(9) << std::setfill('0') << serial_number << '/' << *rod_iter << '/' << "STATUS";
    std::cout << "INFO [" << argv[0] << ":main] set " << is_val_name.str() << " to "  << CAN::ScanIsStatus::name(scan_status)  << std::endl;

    ISInfoDictionary dictionary(ipcPartition);
    ISInfoString status;
    status.setValue(CAN::ScanIsStatus::name(scan_status));
    ISType a_type(status.type());
    setIsValue(dictionary, a_type, is_val_name.str() , status);
  }
  return 0;
}
