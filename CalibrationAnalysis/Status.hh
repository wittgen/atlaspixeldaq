#ifndef _CAN_AnalysisStatus_t_hh_
#define _CAN_AnalysisStatus_t_hh_

#include <cassert>
#include <string>
#include <cstdlib>
#include <Common.hh>

namespace CAN {

  class AnalysisStatus {
  public:
    enum EStatus {kUnknown, kSubmitted, kPending, kWaiting, kReady, kRunning, kSuccess, kFailure, kTimeOut, kAborted } ;

    /** Get the name of a status which is published in IS.
     */
    static const char *name(EStatus status) { assert(status <= kAborted ); return s_name[status]; }

    /** Convert the status name back into a status value.
     */
    static EStatus status(const char *status_name) {
      for (unsigned int status_i=0; status_i <= kAborted; status_i++) {
	if (strcmp(s_name[status_i],status_name)==0) return static_cast<EStatus>(status_i);
      }
      return kUnknown;
    }

    /** Convert the status name back into a status value.
     */
    static EStatus status(const std::string &status_name) {
      return status(status_name.c_str());
    }

    /** Extract the ROD name from the analysis IS status variable.
     * @param full_is_var_name the full statis name in the IS server.
     * @return the connectivity name of the corresponding ROD.
     */
    static std::string extractRodName(const std::string &full_is_var_name);

    /** Create a regular expression to match the per ROD status variable in IS.
     */
    static std::string makeRegex(SerialNumber_t serial_number);

    /** Create a regular expression or variable name for a particular variable and a particular ROD or matching a particular ROD pattern. 
     */
    static std::string makeRegexOrVarName(const std::string &is_header, const std::string &conn_name, const std::string &var_name) {
      std::string temp(is_header);
      temp += '/';
      if (!conn_name.empty()) {
	temp += conn_name;
	temp += '/';
      }
      temp += var_name;
      return temp;
    }

    /** Create a regular expression or variable name for a particular variable. 
     */
    static std::string makeRegexOrVarName(const std::string &is_header, const std::string &var_name) {
      std::string temp(is_header);
      temp += '/';
      temp += var_name;
      return temp;
    }

    /** Create the IS variable name of the per ROD status.
     */
    static std::string makeIsVarName(const std::string &is_server_name, const std::string &rod_connectivity_name, SerialNumber_t analysis_serial_number);

    /** Create the global IS  status name.
     */
    static std::string makeGlobalIsVarName(const std::string &is_server_name, SerialNumber_t analysis_serial_number);

    /** Create the IS variable header.
     * The header contains everything but the variable name and eventually the rod name. To get the full 
     * call 
     * <pre>
     * std::string is_header = AnalysisStatus::makeIsHeader(is_server_name, analysis_serial_number);
     * AnalysisStatus::makeRegexOrVarName(is_header, ".*", AnalysisStatus::varName());
     * AnalysisStatus::makeRegexOrVarName(is_header, "ROD_C1_S16", AnalysisStatus::varName());
     * AnalysisStatus::makeRegexOrVarName(is_header, "", AnalysisStatus::globalVarName());
     *</pre>
     */
    static std::string makeIsHeader(const std::string &is_server_name, SerialNumber_t analysis_serial_number);


    static const std::string &varName()        { return s_statusVarName; }
    static const std::string &globalVarName()  { return s_globalStatusVarName; }

  private:
    static const char *s_name[kAborted+1];
    static const std::string s_empty;
    static const std::string s_statusVarName;
    static const std::string s_globalStatusVarName;
    static const char        s_measurementTypeId = 'A';
  };

}

#endif
