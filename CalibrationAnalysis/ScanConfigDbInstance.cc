#include "ScanConfigDbInstance.hh"
#include "PixDbScanConfigDb.hh"
#include "PixDbConfigDb.hh"

namespace CAN {

  const char *ScanConfigDbInstance::s_scanClassName="PixScan";

  IConfigDb *ScanConfigDbInstance::create() {
     // std::cout<<"gia, passo di qui"<<std::endl;
    const char *scan_config_db = getenv("PIXSCAN_CFG_PATH");
    if (!scan_config_db || strlen(scan_config_db)==0) {
      throw MissingConfig(" Environment variable PIXSCAN_CFG_PATH is undefined. It defines the location of the scan configuration db, ");
    }
    return new PixDbConfigDb( scan_config_db);
  }

}
