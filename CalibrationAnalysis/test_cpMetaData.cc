#include <memory>

#include <CoralDbClient.hh>
#include "ScanInfo_t.hh"



#include "exception.hh"
#include "PixMetaException.hh"
#include <vector>

int main(int argc, char **argv)
{
  bool execute=false;
  bool error=false;
  std::string src_db;
  std::string dest_db;
  std::string metadb_env_name="PIX_METADATA_DB";

  unsigned int serial_number=0;

  const char *db=getenv(metadb_env_name.c_str());
  if (db) {
    dest_db=db;
  }

  for (int arg_i=1; arg_i<argc; arg_i++) {
    if (strcmp(argv[arg_i],"-s")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      serial_number = atoi(argv[++arg_i]);
    }
    else if ((strcmp(argv[arg_i],"-i")==0 || strcmp(argv[arg_i],"--db-src")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      src_db=argv[++arg_i];
    }
    else if ((strcmp(argv[arg_i],"-o")==0 || strcmp(argv[arg_i],"--db-dest")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      dest_db=argv[++arg_i];
    }
    else if ((strcmp(argv[arg_i],"-x")==0 || strcmp(argv[arg_i],"--execute")==0) ) {
      execute=true;
    }
    else {
      std::cout << "ERROR ["<< argv[0] << ":main] unhandled argument " << arg_i << " : " << argv[arg_i]<< "." << std::endl;
      error=true;
      break;
    }
  }
  std::cout << "INFO [main] copy meta data from scan " << serial_number << " from " << src_db << " to " << dest_db
	    << (execute ? "(executing ...)" : "(simulation! Specifiy -x for actually executing the copying)")
	    << std::endl;
  if (dest_db == src_db) {
    std::cerr << "ERROR [main] The src and dest db must not be the same." << std::endl;
    return -1;
  }
  if (src_db.empty() || dest_db.empty() || error) {
    std::cout << "usage: " << argv[0] << "-i/--db-src src-connection-string -s serial-number [-o/--db-dest dest-connection-string] [-x]" << std::endl;
    return -1;
  }

  std::auto_ptr<CAN::PixCoralDbClient> src_client;
  std::auto_ptr<CAN::PixCoralDbClient> dest_client;

  try {
  {
    setenv(metadb_env_name.c_str(), src_db.c_str(), true);
    src_client=std::auto_ptr<CAN::PixCoralDbClient>( new CAN::PixCoralDbClient(true) );
  }

  {
    setenv(metadb_env_name.c_str(), dest_db.c_str(), true);
    dest_client=std::auto_ptr<CAN::PixCoralDbClient>( new CAN::PixCoralDbClient(true) );
  }

  if (!src_client.get() || !dest_client.get()) {
    std::cerr << "ERROR [main] Failed to create src or destination database client." << std::endl;
    return -1;
  }



  PixLib::ScanInfo_t scan_info = src_client->getMetadata<PixLib::ScanInfo_t>(serial_number);
  if (execute) {
    dest_client->storeMetadata(serial_number,&scan_info);
  }
  

  }
  catch (CAN::PixMetaException &err) {
    std::cerr << "ERROR [" << argv[0] << ":main] Caught PixMetaException " << err.what() << std::endl;
  }
  catch (CAN::MRSException &err) {
    std::cerr << "ERROR [" << argv[0] << ":main] Caught exception " << err.getDescriptor() << std::endl;
  }
  catch (std::exception &err) {
    std::cerr << "ERROR [" << argv[0] << ":main] Caught standard exception " << err.what() << std::endl;
  }

  return 0;
}
