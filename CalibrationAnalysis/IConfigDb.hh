#ifndef _CAN_IConfigDb_hh_
#define _CAN_IConfigDb_hh_

#include "exception.hh"
#include <ConfigWrapper/pixa_exception.h>
#include <ConfigWrapper/ConfigRef.h>

#include <vector>

#include "ConfigCatalogueListener.hh"

namespace CAN {

  /** Exception which gets thrown if a missing module, boc or scan config is accessed.
   */
  typedef PixA::ConfigException ConfigException;

  /** Exception which gets thrown if a config does not exist
   */
  class MissingConfig : public ConfigException
  {
  public:
    MissingConfig(const std::string &message) : ConfigException(message) {}
  };

  class IConfigDb
  {
  public:
    enum EConfigLevel { kNormal, kExpert, kDebug, kHidden, kNLevels };

    virtual ~IConfigDb() {};

    /** Get the name of the configuration file for the given type, tag and revision.
     */
    virtual std::string configFileName(const std::string &type_name,
				       const std::string &tag_name,
				       Revision_t revision=0) = 0;

      virtual std::string givepath() = 0;
      virtual void create_default_config(Revision_t max_revision, std::string scan_type, std::string class_name, std::string tag_name_flavor)=0;
      virtual void create_copy_config(Revision_t max_revision, std::string scan_type, std::string class_name, std::string tag_name_flavor,const PixA::ConfigHandle &vconfig)=0;

    /** Get the configuration for the given type, tag and revision.
     */
    virtual PixA::ConfigHandle config(const std::string &type_name,
				      const std::string &tag_name,
				      Revision_t revision=0) = 0;

    virtual PixA::ConfigHandle config_1(const std::string &type_name,
                                        const std::string &tag_name,
                                        Revision_t revision=0, std::string string="") = 0;
      

    /** Get the configuration with a given name for the given type, tag and revision.
     */
    virtual PixA::ConfigHandle config(const std::string &type_name,
				      const std::string &tag_name,
				      Revision_t revision,
				      const std::string &config_name) = 0;

    /** Add a new configuration for the given type, with given tag.
     * @return the revision
     * The revision will be set to the current time.
     */
    virtual Revision_t addConfig(const std::string &class_name,
				 const std::string &type_name,
				 const std::string &tag_name,
				 const PixA::ConfigHandle &handle) = 0;

    /** Add a new configuration for the given type, with given tag and given revision
     */
    virtual Revision_t addConfig(const std::string &class_name,
				 const std::string &type_name,
				 const std::string &tag_name,
				 Revision_t revision,
				 const PixA::ConfigHandle &handle) = 0;

    /** Add a list of configuration for the given type, with given tag.
     * @return the revision
     * The revision will be set to the current time.
     */
    virtual Revision_t addConfigList(const std::string &class_name,
				     const std::string &type_name,
				     const std::string &tag_name,
				     const std::vector< std::pair< std::string, PixA::ConfigHandle> > &config_list) = 0;

    /** Get the description of a type.
     */
    virtual void getDescription(const std::string &type_name) = 0 ;

    /** Get all descriptions.
     * @param class_name the name of a particular class or an empty string.
     */
    virtual void getDescription(const std::string &class_name, IDescriptionListener &listener) = 0 ;

    /** Get the comment for a particular data set.
     */
    virtual void getComments(const std::string &type_name,
			     const std::string &tag_name,
			     Revision_t revision,
			     ICommentListener &listener) = 0 ;

    /** Get a list of all type names for a certain class.
     */
    virtual std::set<std::string> listTypes(const std::string &class_name, EConfigLevel level=kDebug) = 0 ;

    /** Get a list of all type names.
     */
    virtual std::set<std::string> listTypes() = 0 ;

    /** Get a list of all tags used for the given type.
     */
    virtual std::set<std::string> listTags(const std::string &type_name) = 0 ;

    /** Get a list of all revisions which exist for the given type in the given tag.
     */
    virtual std::set<Revision_t> listRevisions(const std::string &type_name,
					       const std::string &tag_name) = 0 ;

  };

}

#endif
