#include <ipc/partition.h>
#include <ipc/server.h>
#include <ipc/core.h>
#include <ipc/partition.h>
#include <ipc/object.h>

#include "TestServer.hh"

#include <memory>

namespace CAN {

  class TestServer_impl : public IPCNamedObject<POA_CAN::TestServer,ipc::single_thread>
  {
  public:

    TestServer_impl(IPCPartition &ipc_partition) 
      : IPCNamedObject<POA_CAN::TestServer,ipc::single_thread>(ipc_partition,ipc_partition.name()+"_TestServer"),
	m_counter(0)
    {
      publish();
    }

    void test1(const char *analysis_name,
	       ESerialNumberType src_serial_number_type,
	       SerialNumber_t src_serial_number,
	       const char *tag,
	       Revision_t revision) {

      m_analysisName = analysis_name;
      m_srcSerialNumberType = src_serial_number_type;
      m_srcSerialNumber = src_serial_number;
      m_tag = tag;
      m_revision = revision;

      m_counter++;
    }

    void test2(const AnalysisInfoData_t &analysis_data) {
      m_analysisName = analysis_data.m_analysisName;
      m_srcSerialNumberType = analysis_data.m_srcSerialNumberType;
      m_srcSerialNumber = analysis_data.m_srcSerialNumber;
      m_tag = analysis_data.m_tag;
      m_revision = analysis_data.m_revision;

      m_counter++;
    }

    void resetCounter() {
      m_counter = 0;
    }

    CORBA::ULong getCounter() {
      return m_counter;
    }

    void shutdown() {
      s_ipcServer.stop();
    }

    static void run() {
      s_ipcServer.run();
    }

  private:
    unsigned int      m_counter;
    std::string       m_analysisName;
    ESerialNumberType m_srcSerialNumberType;
    SerialNumber_t    m_srcSerialNumber;
    std::string       m_tag;
    Revision_t        m_revision;

    static IPCServer s_ipcServer;
  };

  IPCServer TestServer_impl::s_ipcServer;

}



int main(int argc, char **argv)
{
  // GET COMMAND LINE

  // Check arguments
  if(argc<2) {
    std::cout << "USAGE: testServer \"partition name\"" << std::endl;
    return 1;
  }

  std::string ipcPartitionName = argv[1];

  // START IPC

  // Start IPCCore
  IPCCore::init(argc, argv);

  // Create IPCPartition
  std::cout << "Getting partition " << ipcPartitionName << std::endl;
  std::auto_ptr<IPCPartition> ipcPartition (new IPCPartition(ipcPartitionName.c_str()));

  try {
    new CAN::TestServer_impl(*(ipcPartition.get()));
    CAN::TestServer_impl::run();
  }
  catch (std::exception &err) {
    std::cerr << err.what() << std::endl;
  }

  return 0;
}


