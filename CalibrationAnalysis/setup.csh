set mydir=$1
if ( $mydir == "" ) set mydir=DAQ/Calib-test
endif
set olddir=`pwd`
cd
mkdir -p $mydir
cd $mydir
setenv WorkDir `pwd`
daq_setup $mydir
setenv CAN_CALIB_META ${WorkDir}/Applications/Pixel/PixCalibDbCoral
setenv CAN_CALIB_RESULTS ${WorkDir}/Applications/Pixel/PixResultsDbCoral
setenv PIX_ANA ${WorkDir}/Applications/Pixel/PixAnalysis
#setenv SN_SVC ${WorkDir}/Applications/Pixel/SerialNumberService
setenv CAN ${WorkDir}/Applications/Pixel/CalibrationAnalysis
setenv SEAL_PLUGINS ${CAN}/plugins/test:${CAN}/autoAnalyses:$SEAL_PLUGINS
setenv LD_LIBRARY_PATH ${CAN}:${CAN_CALIB_META}:${CAN_CALIB_RESULTS}:${PIX_ANA}/ConfigWrapper:${PIX_ANA}/DataContainer:${CAN}/plugins/test:${CAN}/autoAnalyses:${LD_LIBRARY_PATH}
setenv CAN_CFG_PATH /tmp/${USER}/conf
setenv PIXSCAN_CFG_PATH /tmp/${USER}/conf
mkdir -p ${CAN_CFG_PATH}
setenv PIX_METADATA_DB "sqlite_file:/tmp/${USER}/calib/metadb.sql"
#setenv PIX_METADATA_DB /tmp/${USER}/
mkdir -p /tmp/${USER}/calib

setenv CAN_CALIB_DB "sqlite_file:/tmp/${USER}/calib/dbcalib.sql"
 cd $olddir
unset olddir mydir
