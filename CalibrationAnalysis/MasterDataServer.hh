#ifndef _CAN_MasterDataServer_hh_
#define _CAN_MasterDataServer_hh_

#include "IMasterDataServer.hh"
#include "IExecutionUnit.hh"
#include "exception.hh"
//#include <mrs/message.h>

#include <ConfigWrapper/Connectivity.h>

#include "MetaDataService.hh"
#include "ScanInfo_t.hh"

#include "IsInfoReceiverThread.hh"
#include "IsDictionary.hh"
#include "ScanIsStatus.hh"

#include "HistoArray.hh"

#include "DataServer.hh"

#include <deque>
#include <map>
#include <memory>


#include "Lock.hh"
#include "Flag.hh"

#include "exception.hh"

#include <time.h>

#include "LogLevel.hh"

namespace PixLib{
  class PixMessages;
}

namespace CAN {
  
  class DataServer;
  class IHistoReceiver;

  class MasterDataServer : public IMasterDataServer, public omni_thread
  {
  public:
    typedef std::deque< std::pair< PixA::RodLocationBase, std::pair< SerialNumber_t, DataServer* > > > DataServerList_t;
    friend class DataServer;

  protected:
    class DoneFlag {
    public:
      DoneFlag() : m_done(false) {}
      operator bool() const { return m_done;}
      void setDone() { m_done = true;}
    private:
      bool m_done;
    };

    //dirty trick to circumvent access rights
    static void setDataServerAbort(DataServer *data_server) {
      data_server->setAbort();
    }

    //dirty trick to circumvent access rights
    static void setChildHasData(DataServer *data_server);
  
    // dirty trick to circumvent access rights
    // implementation only if AnalysisDataServer is included before
    static bool hasAllData(DataServer *data_server);
    
    class AnalysisData_t {
    public:
      AnalysisData_t() {}
      AnalysisData_t(DataServer* data_server) : m_dataServer(data_server) {}

      DataServer* dataServer()  { return m_dataServer;   }
      bool hasData() const      { return m_hasData;      }
      void setHasData()         { m_hasData.setDone(); 	MasterDataServer::setChildHasData(dataServer()); }

      void setAbort()           { m_aborted.setDone(); MasterDataServer::setDataServerAbort(dataServer());}

      bool isAborted() const    { return m_aborted; }

    private:
      DataServer* m_dataServer;
      DoneFlag     m_hasData;
      DoneFlag     m_aborted;
    };

    static void abortAnalysisData(AnalysisData_t &analysis_data) {
      analysis_data.setAbort();
    }


    bool hasDataForDownloading();
    bool cleanup();


    class RodData_t {
    public:
      RodData_t(ScanIsStatus::EStatus initial_status) : m_scanStatus( initial_status ), m_creationTime( time(NULL) ) {}

      void addDataServer( std::pair<SerialNumber_t, DataServer* > &data_server)
      {
	Lock lock(m_dataServerMutex);
	std::pair< std::map< SerialNumber_t, AnalysisData_t >::iterator, bool>
	  insert_data_server_ret = m_dataServer.insert( std::make_pair(data_server.first, AnalysisData_t(data_server.second)) );

	assert( insert_data_server_ret.second ); //there should be no data server with the same rod name and the same analysis serial number
      }

      std::map< SerialNumber_t, AnalysisData_t >::iterator       beginDataServer()     { return m_dataServer.begin(); }
      std::map< SerialNumber_t, AnalysisData_t >::const_iterator endDataServer() const { return m_dataServer.end(); }
      bool empty() const { return m_dataServer.empty(); }

      std::map< SerialNumber_t, AnalysisData_t >::iterator       findDataServer(SerialNumber_t analysis_serial_number)
      { return m_dataServer.find(analysis_serial_number); }

      void erase(std::map< SerialNumber_t, AnalysisData_t >::iterator analysis_iter)
      { m_dataServer.erase(analysis_iter); }

      time_t creationTime() const { return m_creationTime; }

      ScanIsStatus::EStatus   scanStatus() const {return m_scanStatus; }

      /** Update the scan status and return true if the scan has finished now.
       * @return return true if the scan was not finished before but is now.
       */
      bool updateScanStatus();

      ISInfoString &isScanStatusString() { return m_infoScanStatusString; }

      Mutex &dataServerMutex() { return m_dataServerMutex; }

      bool hasData() const { return m_hasData; }

      void setHasData()    { m_hasData.setDone(); }

      bool isBeingProcessed() const { return m_isBeingProcessed; }

      void markForProcessing()    { m_isBeingProcessed.setDone(); }

      /** Reset the rod data.
       * Must only be called by the main loop of the master data server.
       */
      void reset(IExecutionUnit *execution_unit, const std::string &rod_name);

    protected:
      Mutex                                      m_dataServerMutex;
      ISInfoString                               m_infoScanStatusString;
      ScanIsStatus::EStatus                      m_scanStatus;
      std::map< SerialNumber_t, AnalysisData_t > m_dataServer;
      time_t                                     m_creationTime;

      DoneFlag m_hasData;
      DoneFlag m_isBeingProcessed;
    };

    class ScanData_t
    {
      friend class MasterDataServer;


    public:
      ScanData_t( MasterDataServer *the_master, const PixA::ConnectivityRef &conn ) 
	: m_master(the_master),
	  m_scanConn(conn)
      { if (m_scanConn) { m_scanConnSet.setDone(); } } // There must be a valid connecitivity somewhere, if the connectivity is invalid it has not been fetched yet.

      ~ScanData_t() { Lock lock(m_rodDataMutex); m_rodData.clear(); }

      std::map< PixA::RodLocationBase, std::shared_ptr<RodData_t> >::const_iterator findRod(PixA::RodLocationBase &rod_location) const
      { Lock lock(m_rodDataMutex); return m_rodData.find(rod_location); }

      std::map< PixA::RodLocationBase, std::shared_ptr<RodData_t> >::iterator findRod(PixA::RodLocationBase &rod_location)
      { Lock lock(m_rodDataMutex); return m_rodData.find(rod_location); }

      std::map< PixA::RodLocationBase, std::shared_ptr<RodData_t> >::iterator  beginRod()            { return m_rodData.begin(); }

      std::map< PixA::RodLocationBase, std::shared_ptr<RodData_t> >::const_iterator endRod() const   { return m_rodData.end(); }

      void erase(std::map< PixA::RodLocationBase, std::shared_ptr<RodData_t> >::iterator rod_iter)
      {
	Lock lock(m_rodDataMutex);

	// paranoia check
	for (std::map< SerialNumber_t, AnalysisData_t >::const_iterator  analysis_iter = rod_iter->second->beginDataServer();
	     analysis_iter != rod_iter->second->endDataServer();
	     analysis_iter++) {
	  assert ( analysis_iter->second.hasData());
	}
	assert( rod_iter->second->hasData() );

	m_rodData.erase(rod_iter);
      }

      /** Add a data server to the new data server list.
       * Will try to lock @ref newDataServerMutex.
       */
      void addNewDataServer(const PixA::RodLocationBase &rod_location,
			    SerialNumber_t analysis_serial_number,
			    DataServer* data_server)
      {
	assert( data_server );
        assert( dynamic_cast<DataServer *>(data_server) );
	Lock lock(m_newDataServerListMutex);
	m_newDataServerList.push_back( std::make_pair(rod_location, std::make_pair(analysis_serial_number,data_server ) ));
      }

      Mutex &newDataServerMutex() { return m_newDataServerListMutex; }
      Mutex &rodDataMutex()       { return m_rodDataMutex; }

      DataServerList_t::iterator beginNewDataServer()           { return m_newDataServerList.begin(); }

      DataServerList_t::const_iterator endNewDataServer() const { return m_newDataServerList.end(); }

      void clearNewDataServer() { m_newDataServerList.clear(); }

      /** Move a data server from the list of new data servers to the per rod data container.
       * @param new_data_server_iter an iterator pointing to a new data server which should be moved to the per ROD data list.
       * The iterator will be erased from the new data server list.
       * Will lock @ref newDataServerMutex and  @rodDataMutex.
       */
      std::shared_ptr<RodData_t> insertDataServer(const DataServerList_t::iterator &new_data_server_iter, ScanIsStatus::EStatus initial_status)
      {
	std::shared_ptr<RodData_t> new_rod_data;
	std::map< PixA::RodLocationBase, std::shared_ptr<RodData_t> >::iterator rod_data_iter;
	{
	  Lock lock(m_rodDataMutex);
	  rod_data_iter = m_rodData.find(new_data_server_iter->first);
	  if ( rod_data_iter == m_rodData.end() ) {
	    std::pair< std::map< PixA::RodLocationBase, std::shared_ptr<RodData_t> >::iterator, bool >
	      ret =  m_rodData.insert( std::make_pair(new_data_server_iter->first, std::make_shared<RodData_t>(initial_status)) );
	    if (!ret.second) {
	      // cannot do anything about this.
// 	      throw MRSException("MDSRV_internal_error", MRS_FATAL, "MasterDataServer::ScanData_t::insertDataServer",
// 				 new_data_server_iter->first, "Failed to add data structure for ROD.");
	      throw MRSException(PixLib::PixMessages::FATAL, "MasterDataServer::ScanData_t::insertDataServer",
				 new_data_server_iter->first, "Failed to add data structure for ROD.");
	    }
	    rod_data_iter = ret.first;
	    new_rod_data = rod_data_iter->second;
	  }
	}

	rod_data_iter->second->addDataServer( new_data_server_iter->second );
	Lock lock(m_newDataServerListMutex);
	m_newDataServerList.erase(new_data_server_iter);

	return new_rod_data;
      }

      /** Remove a data server from the list of new data servers.
       * @param new_data_server_iter an iterator pointing to a new data server.
       * The iterator will be erased from the new data server list.
       * Will lock @ref newDataServerMutex.
       */
      void removeNewDataServer(const DataServerList_t::iterator &new_data_server_iter)
      {
	Lock lock(m_newDataServerListMutex);
	m_newDataServerList.erase(new_data_server_iter);
      }


      /** Return true if there are no data servers attached.
       * Will try to lock @ref rodDataMutex and @ref newDataServerMutex.
       */
      bool empty() const
      {
	Lock rod_data_lock( m_rodDataMutex );
	Lock new_data_server_lock( m_newDataServerListMutex );
	return m_rodData.empty() && m_newDataServerList.empty();
      }

      /** Return true if there is new data associated to this scan data container.
       * Will try to lock @ref newDataServerMutex.
       */
      bool hasNewData() const
      {
	Lock new_data_server_lock( m_newDataServerListMutex );
	return !m_newDataServerList.empty();
      }

      void statusChanged(ISCallbackInfo *info);

      /** Reset the scan data.
       * must only be called by the main loop of the master data server.
       */
      void reset(IExecutionUnit *execution_unit);

    protected:
      static ISInfoString      s_test;

      MasterDataServer *master()    { return m_master; }

      bool scanConnSet() const                               { return m_scanConnSet; }
      void setScanConnSet()                                  { m_scanConnSet.setDone(); }
      const PixA::ConnectivityRef &connectivity()            { return m_scanConn; }
      void setConnectivity(const PixA::ConnectivityRef &a_conn) { m_scanConn = a_conn; }

      bool scanCfgSet() const                                { return m_scanCfgSet; }
      void setScanCfgSet()                                   { m_scanCfgSet.setDone(); }
      PixA::ConfigHandle scanCfg()                           { return m_scanCfg; }
      void setScanCfg(const PixA::ConfigHandle &a_config)    { m_scanCfg = a_config; }

    private:
      MasterDataServer *m_master;
      mutable Mutex   m_newDataServerListMutex;
      mutable Mutex   m_rodDataMutex;
      DataServerList_t m_newDataServerList;
      std::map< PixA::RodLocationBase, std::shared_ptr<RodData_t> > m_rodData;

      InfoPtr_t<PixLib::ScanInfo_t> m_scanInfo;
      PixA::ConnectivityRef  m_scanConn;
      PixA::ConfigHandle     m_scanCfg;

      DoneFlag  m_scanConnSet;
      DoneFlag  m_scanCfgSet;
      DoneFlag  m_abort;
    };

  public:
    MasterDataServer(IPCPartition &scan_partition,
		     const std::string &scan_is_server_name,
		     IHistoReceiver *histo_receiver);
    //		     IPCPartition &mrs_partition);

    /** Create data server which will serve data for exactly one analysis serial number, one scan and one ROD.
     * @param analysis_serial_number the serial number of the analysis.
     * @param rod_conn_name the connectivity name of the ROD.
     * @param scan_serial_number the serial number of the scan which is to be analysed
     * @param scan_info the scan meta data.
     * @param conn a reference of the connectivity
     * The master data will not keep any reference of the newly created data server. To make the
     * data server known it has to be handed back by calling @ref addDataServer once the data
     * server is fully setup i.e. all requests have been collected additional parameters have been
     * set, a reference of the data server has been passed to the associated joblett.
     * @sa addDataServer
     */
  private:
    DataServer* createDataServer(SerialNumber_t analysis_serial_number,
				 const PixA::RodLocationBase &rod_conn_name,
				 SerialNumber_t scan_serial_number,
				 const InfoPtr_t<PixLib::ScanInfo_t> &scan_info,
				 const SerialNumber_t src_analysis_serial_number);
  public:

    /** Create the umbrella data server which contains the individual scan data server.
     * An analysis can only have one connectivity which is provided by this analysis data server.
     * The analysis data server serves the data for one analysis serial number and one ROD.
     * @sa addAnalysisDataServer
     */
    std::shared_ptr<IAnalysisDataServer> createAnalysisDataServer(SerialNumber_t analysis_serial_number,
								    const PixA::RodLocationBase &rod_conn_name,
								    SerialNumbers_t scan_serial_numbers,
								    const InfoPtr_t<PixLib::ScanInfo_t> &scan_info,
								    const SerialNumber_t src_analysis_serial_number,
								    PixA::ConnectivityRef &conn,
								    const std::vector<PixA::DisabledListRoot> &disabled_lists);


  private:
    /** Make a fully setup data server known to the master data server.
     *      cretate a DataServer specific for one scan (and ROD)
     */
    void addDataServer(SerialNumber_t analysis_serial_number, const PixA::RodLocationBase &rod_conn_name, DataServer*  data_server, const PixA::ConnectivityRef &conn );

  public:

    /** create AnalysisDataServers (one per analysis and ROD), which handels several DataServers for different scans
     *	Make a fully setup data server known to the master data server.
     *  The master data server will start to fulfil the requests. Once
     *  all the requests have been fulfilled the execution unit will be signaled.
     */
    void addAnalysisDataServer(SerialNumber_t analysis_serial_number, const PixA::RodLocationBase &rod_conn_name, const std::shared_ptr<IAnalysisDataServer>  &data_server );

    void start(IExecutionUnit &execution_unit);

    void abort(SerialNumber_t analysis_serial_number);

    /** mark all pending data requests as done and notify execution unit, then clear all data structures.
     */
    void reset();

    void initiateShutdown() { m_shutdown=true; m_reset=true; m_isListener.shutdown(); m_dataRequest->setFlag(); }

    void shutdownWait() {
      m_isListener.shutdownWait();
      if (state()==omni_thread::STATE_RUNNING) {
	initiateShutdown();
	m_exit.wait();
	while (state()!=omni_thread::STATE_TERMINATED) {
	  usleep(1000);
	}
      }
    }

    void setDataRequest() { m_dataRequest->setFlag(); }

    void dump();

    void setLogLevel(ELogLevel log_level) {
      m_logLevel = log_level;
    }

  protected:
    void *run_undetached( void *);

    ISInfoReceiver   &isReceiver() { return m_isListener.receiver(); }

    /** To be called if a scan has terminated.
     */
    void dataAvailable() { m_dataRequest->setFlag(); }

  private:
    void _reset();

    bool NextScanToDownload(SerialNumber_t &serial_number_for_download, std::map< PixA::RodLocationBase,std::shared_ptr<RodData_t> >::iterator &rod_for_download, std::map<SerialNumber_t, std::shared_ptr<ScanData_t> >::iterator &scan_data_of_rod); //CLA 


    void downloadHistogramms(SerialNumber_t serial_number_for_download,
			     const ScanData_t &scan_data,
			     std::map< PixA::RodLocationBase,std::shared_ptr<RodData_t> >::iterator rod_for_download);

    void checkForNewDataServers(); //CLA
    void updateScanConfigurations(std::map<SerialNumber_t, std::shared_ptr<ScanData_t> >::iterator& scan_data_with_new_data_server);
    void updateConfigurations(std::map<SerialNumber_t, std::shared_ptr<ScanData_t> >::iterator& scan_data_with_new_data_server); //CLA

    /** Listen for scan status changes of the given scan.
     */
    void subscribe(SerialNumber_t scan_serial_number);

    /** Listen for scan status changes of the given scan.
     */
    void unsubscribe(SerialNumber_t scan_serial_number);

    static std::string makeIsScanStatusPattern(SerialNumber_t scan_serial_number);
    static std::string makeIsScanStatusPattern(SerialNumber_t scan_serial_number, const std::string &rod_name);

    std::shared_ptr<Flag>  m_dataRequest;
    Flag  m_exit;
    Flag  m_resetDone;
    bool  m_reset;
    bool  m_shutdown;
    bool  m_abortRequest;

    Mutex m_scanDataMutex;
    std::map<SerialNumber_t, std::shared_ptr<ScanData_t> > m_scanData;


    IExecutionUnit *m_executionUnit;
    //    Mutex                    m_mrsMutex;
    //    std::auto_ptr<MRSStream> m_out;

    IsInfoReceiverThread     m_isListener;
    IPCPartition            *m_scanPartition;
    std::string              m_isServerName;
    std::unique_ptr<IHistoReceiver> m_histoReceiver;
    unsigned int m_waitBeforeCleaning;
    
    ELogLevel                m_logLevel;

  };

#ifdef _CAN_AnalysisDataServer_hh_
    inline bool MasterDataServer::hasAllData(DataServer *data_server) {
      return data_server->parent().hasAllData();
    }

    inline void MasterDataServer::setChildHasData(DataServer *data_server) {
      data_server->parent().childHasData();
    }

#endif

}

#endif
