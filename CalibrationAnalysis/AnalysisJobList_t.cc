#include "AnalysisJobList_t.hh"
#include "AnalysisMonitor.hh"
#include <is/infoiterator.h>

#include "isUtil.hh"

namespace CAN {

  ISInfoString AnalysisJobList_t::s_test;

  void AnalysisJobList_t::subscribe() {

    {
      std::string is_status_pattern(AnalysisStatus::makeRegexOrVarName(m_isHeader, ".*", AnalysisStatus::varName() ));

      ISInfoIterator ii( m_analysisMonitor->partition(), m_analysisMonitor->isServerName(), s_test.type() && is_status_pattern );
      while( ii() ) {

	std::string rod_name( AnalysisStatus::extractRodName(ii.name()) );
	iterator job_info_iter = find(rod_name);
	if (job_info_iter != end()) {
	  ISInfoString &rod_status = job_info_iter->second.isStatus();
	  ii.value( rod_status );
	  job_info_iter->second.updateStatusFromIs();
	}

      }

      m_analysisMonitor->isReceiver().subscribe(m_analysisMonitor->isServerName(),
						s_test.type() && is_status_pattern,
						&CAN::AnalysisJobList_t::statusChanged,
						this);
    }


//     {
//       std::string is_global_status_name(m_isHeader);
//       is_global_status_name += ".globalstatus";

//       ISInfoIterator ii( m_analysisMonitor->partition(), m_analysisMonitor->isServerName(), s_test.type() && is_global_status_name );
//       while( ii() ) {
// 	ii.value( m_isGlobalStatus );
//       }
//       m_analysisMonitor->isReceiver().subscribe(m_analysisMonitor->isServerName(),
// 					       s_test.type() && is_global_status_name,
// 					       &CAN::AnalysisJobList_t::globalStatusChanged,
// 					       this);
//     }

  }

  void AnalysisJobList_t::unsubscribe() {

    {
      std::string is_status_pattern(AnalysisStatus::makeRegexOrVarName(m_isHeader, ".*", AnalysisStatus::varName() ));

      m_analysisMonitor->isReceiver().unsubscribe(m_analysisMonitor->isServerName(), s_test.type() && is_status_pattern);

      ISInfoIterator ii( m_analysisMonitor->partition(), m_analysisMonitor->isServerName(), s_test.type() && is_status_pattern );
      while( ii() ) {
	m_analysisMonitor->dictionary().remove(ii.name());
      }

    }


//     {
//       std::string is_global_status_name(m_isHeader);
//       is_global_status_name += ".globalstatus";

//       ISInfoIterator ii( m_analysisMonitor->partition(), m_analysisMonitor->isServerName(), s_test.type() && is_global_status_name );
//       //       while( ii() ) {
//       // 	m_analysisMonitor->dictionary().remove(ii.name());
//       //       }
//     }

  }

  void AnalysisJobList_t::statusChanged(ISCallbackInfo *info)
  {
    if( info->type() == s_test.type() ) {
      //      std::shared_ptr<AnalysisJobList_t> lock(this);
      // get the ROD name
      std::string rod_name( info->name() );
      // strlen(".status")
      if (rod_name.size() > 7) {
	//
	rod_name.erase(rod_name.size()-7,7);
	std::string::size_type pos = rod_name.rfind("/");
	if (pos != std::string::npos) {
	  rod_name.erase(0,pos+1);
	  iterator job_info_iter = find(rod_name);
	  if (job_info_iter != end()) {
	    AnalysisStatus::EStatus old_status = job_info_iter->second.status();
	    info->value(job_info_iter->second.isStatus() );
	    job_info_iter->second.updateStatusFromIs();
	    AnalysisStatus::EStatus new_status = job_info_iter->second.status();
	    if (old_status != AnalysisStatus::kRunning && new_status == AnalysisStatus::kRunning) {
	      job_info_iter->second.setReferenceTime(time(NULL));
	    }

	    //	    assert(new_status>=old_status || new_status == kUnknown);

	    if (new_status>=old_status || new_status == AnalysisStatus::kUnknown) {
	    }

// 	    if (old_status<kRunning) {

// 	      if ( new_status>=kRunning) {
// 		// in principle the transition into the running state should always happen after a state < kRunning
// 		// Could the state transition be missed ?
// 		job_info_iter->second.workerInfo().decWaitingJobs();
// 		if ( new_status==kRunning) {
// 		  job_info_iter->second.workerInfo().incRunningJobs();
// 		}
// 	      }
// 	    }
// 	    else if (old_status==kRunning) {
// 	      if (new_status>kRunning) {
// 		job_info_iter->second.workerInfo().decRunningJobs();
// 		if (job_info_iter->second.workerInfo().nFreeSlots()==0) {
// 		  job_info_iter->second.workerInfo().resetFreeSince();
// 		}
// 	      }
// 	    }
 	  }
	}
      }
    }

  }

//   void AnalysisJobList_t::globalStatusChanged(ISCallbackInfo *info)
//   {
//     if( info->type() == s_test.type() ) {
//       std::shared_ptr<AnalysisJobList_t> lock(this);

//       info->value(m_isGlobalStatus);
//     }

//   }

  void AnalysisJobList_t::addJobs(const std::string &is_dictionary_header,
				  std::shared_ptr<WorkerInfo_t> &worker,
				  const std::vector< std::pair<unsigned int, std::string> > &rod_list)
  {
    Lock dictionary_lock( m_analysisMonitor->mutex() );

    ISType test;

    for (std::vector< std::pair<unsigned int,std::string> >::const_iterator rod_iter = rod_list.begin();
	 rod_iter != rod_list.end();
	 rod_iter++) {

      std::string a_rod_name(rod_iter->second);
      std::map< std::string, JobInfo_t >::iterator job_iter = m_jobs.find(a_rod_name);
      if (job_iter == m_jobs.end()) {
	std::pair< std::map< std::string, JobInfo_t >::iterator, bool>
	  ret =  m_jobs.insert(std::make_pair(a_rod_name, JobInfo_t(worker, rod_iter->first)));
	if (!ret.second) {
	  std::stringstream message;
	  message << "Failed to add job for ROD " << a_rod_name << ".";
	  throw std::runtime_error(message.str());
	}
	job_iter = ret.first;
      }
      else {

	// only jobs which were marked for resubmission should actually be resubmitted
	assert( job_iter->second.markedForResubmission() );
	job_iter->second.resubmit(worker);
      }

      // publish per ROD status in IS
      std::string is_dictionary_name(AnalysisStatus::makeRegexOrVarName(is_dictionary_header, a_rod_name , AnalysisStatus::varName() ));
      job_iter->second.setStatus(AnalysisStatus::kSubmitted);
      setIsValue(m_analysisMonitor->dictionary(),test, is_dictionary_name, job_iter->second.isStatus());
    }

    worker->submitJobs(rod_list.size());

  }

}
