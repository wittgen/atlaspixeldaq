#ifndef _CAN_DummyConfigDb_hh_
#define _CAN_DummyConfigDb_hh_

#include "IObjectConfigDb.hh"
#include <Config/Config.h>

namespace CAN {

  /** Interface for a configuration database.
   */
  class DummyConfigDb : public IObjectConfigDb {
  public:
    void readConfig(const ObjectInfo_t &/*object_info*/, PixLib::Config &config, std::string string="") { config.reset(); }
  };

}

#endif
