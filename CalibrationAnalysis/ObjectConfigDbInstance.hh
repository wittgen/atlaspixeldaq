/* Dear emacs, this is -*-c++-*- */
#ifndef _CAN_ObjectConfigDbInstance_hh_
#define _CAN_ObjectConfigDbInstance_hh_

#include "IConfigDb.hh"
#include <cassert>

namespace CAN {

  class ObjectConfigDbInstance
  {
  public:
    enum EClass {kAnalysis, kClassification, kPostProcessing, kNClasses};

    static IConfigDb *create();

    static const char *objectClassName(EClass class_i) { assert(class_i<kNClasses); return s_className[class_i]; }
  private:
    static const char *s_className[3];

  };

}
#endif
