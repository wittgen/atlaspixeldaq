// analysis data server needs to be included before master data server 
// otherwise the method MasterDataServer::hasAllData is not available
#include "AnalysisDataServer.hh"
#include "MasterDataServer.hh"

#include "sendMessage.hh"

#include "ExecutionUnit.hh"

#include "ScanConfigDb.hh"

#include <is/infoiterator.h>

#include "IHistoReceiver.hh"

#include "ConnObjConfigDbGlobalMutex.hh"
#include "PixDbGlobalMutex.hh"
#include <ConfigWrapper/ConnectivityManager.h>

namespace CAN {


  ISInfoString MasterDataServer::ScanData_t::s_test;

  void MasterDataServer::ScanData_t::statusChanged(ISCallbackInfo *info)
  {
    if( info->type() == s_test.type() ) {

      std::string rod_name( ScanIsStatus::extractRodName( info->name() ) );

      if (! rod_name.empty() ) {

	  std::map< PixA::RodLocationBase, std::shared_ptr<RodData_t> >::iterator rod_iter = findRod(rod_name);
	  if (rod_iter  != endRod() ) {

	    info->value( rod_iter->second->isScanStatusString() );

	    if ( rod_iter->second->updateScanStatus() ) {
	      master()->dataAvailable();
	    }
	  }
      }
    }
  }


  MasterDataServer::MasterDataServer(IPCPartition &scan_partition, const std::string &scan_is_server_name,
				     IHistoReceiver *histo_receiver)
  //				     IPCPartition &mrs_partition)
    : m_dataRequest(new Flag),
      m_reset(false),
      m_shutdown(false),
      m_abortRequest(false),
      m_executionUnit(NULL),
      //      m_out(new MRSStream(mrs_partition)),
      m_isListener(scan_partition),
      m_scanPartition(&scan_partition),
      m_isServerName(scan_is_server_name),
      m_histoReceiver(histo_receiver),
      m_waitBeforeCleaning(30),
      m_logLevel(kWarnLog)      
  {
    assert( m_histoReceiver.get() );
  }


  //CLA: not using shared_ptr, since they are stored in AnalysisDataser
  DataServer* MasterDataServer::createDataServer(SerialNumber_t analysis_serial_number,
						  const PixA::RodLocationBase &rod_conn_name,
						  SerialNumber_t scan_serial_number,
						  const InfoPtr_t<PixLib::ScanInfo_t> &scan_info,
						  const SerialNumber_t src_analysis_serial_number)
  {
    return new DataServer(scan_serial_number,scan_info, src_analysis_serial_number, m_dataRequest); //conn,rod_conn_name, *this;
  }


  std::shared_ptr<IAnalysisDataServer>
  MasterDataServer::createAnalysisDataServer(SerialNumber_t analysis_serial_number,
					     const PixA::RodLocationBase &rod_conn_name,
					     SerialNumbers_t serial_number_list,
					     const InfoPtr_t<PixLib::ScanInfo_t> &scan_info,
					     const SerialNumber_t src_analysis_serial_number,
					     PixA::ConnectivityRef &conn,
					     const std::vector<PixA::DisabledListRoot> &disabled_lists)
  {
    AnalysisDataServer *an_analysis_data_server(new AnalysisDataServer(conn));
    std::shared_ptr<IAnalysisDataServer> data_server(an_analysis_data_server);

    std::vector< PixA::DisabledListRoot >::const_iterator disabled_list_iter = disabled_lists.begin();

      for(SerialNumbers_t::const_iterator serial_number_iter=serial_number_list.begin();
	  serial_number_iter != serial_number_list.end();
	  ++serial_number_iter, ++disabled_list_iter) {

      if (serial_number_iter->second.first == kScanNumber) {

	DataServer* scan_data_server = createDataServer(analysis_serial_number,
							rod_conn_name,
							serial_number_iter->second.second,
							scan_info,
							src_analysis_serial_number);


	// now the disabled list needs to be set
	scan_data_server->setDisabledList( PixA::createPp0DisabledList( conn,
									*disabled_list_iter,
									rod_conn_name) );

	if (m_logLevel>=kDiagnosticsLog) 
	{
	  std::stringstream message;
	  message << "Add data server for " << rod_conn_name << " scan " << makeScanSerialNumberString(serial_number_iter->second.second) << " ";
	  if (!serial_number_iter->first.empty()) {
	    message << " named \"" << serial_number_iter->first  << "\"";
	  }
	  message << " for analysis " << makeAnalysisSerialNumberString(analysis_serial_number) << ".";
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"DATASRV_analysis_data",MRS_DIAGNOSTIC, "MasterDataServer::createAnalysisDataServer", message.str() );
	  ers::debug(PixLib::pix::daq (ERS_HERE,  "MasterDataServer::createAnalysisDataServer", message.str()) );
	}
	an_analysis_data_server->addDataServer(serial_number_iter->first, scan_data_server);
	scan_data_server->setParent(an_analysis_data_server);

	if (m_logLevel>=kDiagnosticsLog) {
	  std::cout << "DIAGNOSTICS [MasterDataServer::createAnalysisDataServer] Added data server for "
		    << makeScanSerialNumberString(serial_number_iter->second.second) << std::endl;
	}
      }
      else {
	// @todo for the time beeing analysis serial numbers are not supported
	if (m_logLevel>=kErrorLog) {
	  std::stringstream message;
	  message << "No support for analysis data yet (" << makeAnalysisSerialNumberString(analysis_serial_number) << ")";
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"DATASRV_analysis_data",MRS_ERROR, "MasterDataServer::createAnalysisDataServer", message.str() );
	  ers::error(PixLib::pix::daq (ERS_HERE, "MasterDataServer::createAnalysisDataServer", message.str()));
	}
      }

    }

      if (m_logLevel>=kDiagnosticsLog) {
	std::cout << "DIAGNOSTICS [MasterDataServer::addAnalysisDataServer] Finished analysis data server for " 
		  <<  makeAnalysisSerialNumberString(analysis_serial_number) << "." << std::endl;
      }

    return data_server;
  }

  void MasterDataServer::addAnalysisDataServer(SerialNumber_t analysis_serial_number, 
					       const PixA::RodLocationBase &rod_conn_name, 
					       const std::shared_ptr<IAnalysisDataServer>  &data_server )
  {
    AnalysisDataServer *analysis_data_server = dynamic_cast<AnalysisDataServer *>(data_server.get());
    assert(analysis_data_server);

    IDataServer* scan_data_server;
    while( (scan_data_server=analysis_data_server->NextDataServer())!=NULL ) {
      assert( dynamic_cast<DataServer *>(scan_data_server) );
      addDataServer( analysis_serial_number, rod_conn_name, static_cast<DataServer *>(scan_data_server), data_server->connectivity() );
    }
  }

  void MasterDataServer::addDataServer(SerialNumber_t analysis_serial_number,
				       const PixA::RodLocationBase &rod_conn_name,
				       DataServer* data_server,
				       const PixA::ConnectivityRef &scan_conn )
  {
    // only a data server which was created by the master should be added to the master
    // and the master will create IDataServer of type DataServer
    // @sa createDataServer
    assert(data_server);
    data_server->closeRequests();

    std::string is_status_pattern = makeIsScanStatusPattern(data_server->scanSerialNumber());

    //    ISInfoIterator ii( *m_scanPartition, m_isServerName , ScanData_t::s_test.type() && is_status_pattern );

    std::map<SerialNumber_t, std::shared_ptr<ScanData_t> >::iterator scan_data_iter;
    {
    Lock lock(m_scanDataMutex);

    scan_data_iter =  m_scanData.find(data_server->scanSerialNumber());

    if (scan_data_iter == m_scanData.end()) {
      std::pair< std::map<SerialNumber_t, std::shared_ptr<ScanData_t> >::iterator, bool >
	ret = m_scanData.insert( std::make_pair(data_server->scanSerialNumber(),
						std::make_shared<ScanData_t>( this, scan_conn)));

      if (!ret.second) {
	if (m_logLevel>=kFatalLog) {
	  std::stringstream message;
	  message << "Failed to add scan data container for analysis " << makeAnalysisSerialNumberString(analysis_serial_number) 
		  << makeScanSerialNumberString(data_server->scanSerialNumber() )
		  << ".";
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"DATASRV_scanDataInsertErr",MRS_FATAL, "MasterDataServer::addDataServer", message.str());
	  ers::fatal(PixLib::pix::daq (ERS_HERE, "MasterDataServer::addDataServer", message.str()));
	}
	return;
      }
      scan_data_iter = ret.first;

      isReceiver().subscribe(m_isServerName,
			     ScanData_t::s_test.type() && is_status_pattern ,
			     &CAN::MasterDataServer::ScanData_t::statusChanged,
			     scan_data_iter->second.get());
    }

    // add new data server and announce it.
    scan_data_iter->second->addNewDataServer( rod_conn_name, analysis_serial_number, data_server);
    }
    m_dataRequest->setFlag();
  }

  void MasterDataServer::subscribe(SerialNumber_t scan_serial_number)
  {
  }

  bool MasterDataServer::RodData_t::updateScanStatus()
  {
    ScanIsStatus::EStatus old_status = m_scanStatus;

    m_scanStatus = ScanIsStatus::status( m_infoScanStatusString.getValue() );
    return  (old_status < ScanIsStatus::kSuccess && m_scanStatus >= ScanIsStatus::kSuccess);
  }


  void MasterDataServer::unsubscribe(SerialNumber_t scan_serial_number)
  {

    std::string is_status_pattern = makeIsScanStatusPattern(scan_serial_number);

    isReceiver().unsubscribe(m_isServerName,ScanData_t::s_test.type() && is_status_pattern );
  }


  void MasterDataServer::start(IExecutionUnit &execution_unit)
  {
    m_executionUnit=&execution_unit;
    m_isListener.listen();
    start_undetached();
  }


  void *MasterDataServer::run_undetached( void *)
  {
    assert(m_executionUnit);

    if (m_logLevel>=kInfoLog) {
//       Lock mrs_lock(m_mrsMutex);
//       sendMessage(*m_out,"DATASRV_start",MRS_INFORMATION, "MasterDataServer::run_detached", "Start data retrieval.");
      ers::info(PixLib::pix::daq (ERS_HERE, "MasterDataServer::run_detached", "Start data retrieval."));
    }
//std::cout<<"run undetached master 1"<<std::endl;
    bool something_to_cleanup = false;
    bool wait_only=false;
    for (;!m_shutdown;) {
      if (m_reset && !m_shutdown)  {
	_reset();
      }
//        std::cout<<"run undetached master 2"<<std::endl;

      if (DataServer::memoryUsageAboveThreshold()) {
	if (m_logLevel>=kInfoLog) {
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"DATASRV_doozing",MRS_INFORMATION, "MasterDataServer::run_detached", "Wait until some memory gets freed.");
	  ers::info(PixLib::pix::daq (ERS_HERE,  "MasterDataServer::run_detached", "Wait until some memory gets freed."));
	}

	do {
//        std::cout<<"run undetached master 3"<<std::endl;

	  m_dataRequest->timedwait(m_waitBeforeCleaning, m_shutdown);
	} while (!DataServer::memoryUsageBelowThreshold() && !m_reset);

      }
      else if (!m_dataRequest->isFlagged()) {
	if (hasDataForDownloading()) {

	  if (m_logLevel>=kDiagnosticsLog) {
//          std::cout<<"run undetached master 4"<<std::endl;

// 	    Lock mrs_lock(m_mrsMutex);
// 	    sendMessage(*m_out,"DATASRV_new_requests",MRS_DIAGNOSTIC, "MasterDataServer::run_detached",
	    ers::debug(PixLib::pix::daq (ERS_HERE, "MasterDataServer::run_detached",
			"There is data for downloading, which was not marked before - strange."));
	    if (m_logLevel>=kDiagnostics4Log) {
	      dump();
	    }
	  }
	  m_dataRequest->setFlag();
	}
	else {

	  if (m_logLevel>=kInfoLog) {
// 	    Lock mrs_lock(m_mrsMutex);
// 	    sendMessage(*m_out,"DATASRV_doozing",MRS_INFORMATION, "MasterDataServer::run_detached", "Nothing to do.");
	    ers::info(PixLib::pix::daq (ERS_HERE, "MasterDataServer::run_detached", "Nothing to do."));
	  }
        
//        std::cout<<"run undetached master 5"<<std::endl;


	  // @todo wait with time out and shutdown connection to IS and OH 
	  //   if the time out happens. Once, there is data restart IS and OH connection.
	  if (something_to_cleanup || wait_only) {
	    do {
//            std::cout<<"run undetached master 6"<<std::endl;

	      m_dataRequest->timedwait(m_waitBeforeCleaning, m_shutdown);
	    } while (!DataServer::memoryUsageBelowThreshold());
	    wait_only=false;
	  }
	  else {
	    m_dataRequest->wait(m_shutdown);
	  }
	  if (m_logLevel>=kInfoLog) {
// 	    Lock mrs_lock(m_mrsMutex);
// 	    sendMessage(*m_out,"DATASRV_new_requests",MRS_INFORMATION, "MasterDataServer::run_detached", "Got new requests.");
	    ers::info(PixLib::pix::daq (ERS_HERE, "MasterDataServer::run_detached", "Got new requests."));
	  }
	}
      }

      m_dataRequest->clearFlag();
      if (m_shutdown) break;

      if (something_to_cleanup) {
	something_to_cleanup=false;
	cleanup();
      }
      // download histograms for all RODs for which the scan has terminated
      //   @todo should the download first try to finish analyses than scans and rods ?

      for(;!m_reset;) {
	if (DataServer::memoryUsageAboveThreshold()) {
	  break;
	}

	// scan all queued scans from which histograms are requested.

	SerialNumber_t serial_number_for_download;
	std::map< PixA::RodLocationBase,std::shared_ptr<RodData_t> >::iterator rod_for_download;
	std::map<SerialNumber_t, std::shared_ptr<ScanData_t> >::iterator scan_data_of_rod;

	if( !NextScanToDownload( serial_number_for_download, rod_for_download, scan_data_of_rod) ) break;
	if (m_reset) break;

	// download histograms

	downloadHistogramms( serial_number_for_download, *(scan_data_of_rod->second), rod_for_download );
	if (m_reset) break;

	// the rod should be fully processed
 	scan_data_of_rod->second->erase(rod_for_download);
 	if (scan_data_of_rod->second->empty() ) {
	  something_to_cleanup=true;
	  //  	  unsubscribe(scan_data_of_rod->first);
	  //  	  Lock lock(m_scanDataMutex);
	  //  	  m_scanData.erase(scan_data_of_rod);
 	}

      }

      // All histograms of finished scans are downloaded, now new
      // data servers can be added if they exist
      checkForNewDataServers();
      if (m_abortRequest) {
	wait_only=true;
      }

    }

    _reset();

    if (m_logLevel>=kInfoLog) {
//       Lock mrs_lock(m_mrsMutex);
//       sendMessage(*m_out,"DATASRV_term",MRS_INFORMATION, "MasterDataServer::run_detached", "Stopped data retrieval.");
      ers::info(PixLib::pix::daq (ERS_HERE, "MasterDataServer::run_detached", "Stopped data retrieval."));
    }
      
//      std::cout<<"run undetached master end"<<std::endl;
      
    m_exit.setFlag();
    return NULL;
  }


  /** CLA:
   *
   */
  bool MasterDataServer::NextScanToDownload(SerialNumber_t &serial_number_for_download,
					    std::map< PixA::RodLocationBase,std::shared_ptr<RodData_t> >::iterator &rod_for_download,
					    std::map<SerialNumber_t, std::shared_ptr<ScanData_t> >::iterator &scan_data_of_rod) 
  {
    // @todo is there a possibility to avoid to start looping always from the beginning
    //       without holding the lock ?
    bool has_rod_for_download=false;
    unsigned int tot_rods_without_data_finished=0;
    unsigned int tot_rods_without_data_unfinished=0;
    {
      Lock lock(m_scanDataMutex);

      // first search for rods for which the scan has finished.
      // start with oldest scans first
      std::map<SerialNumber_t, std::shared_ptr<ScanData_t> >::iterator scan_data_iter = m_scanData.begin();
      bool scan_data_end = (scan_data_iter == m_scanData.end());
      for (; !scan_data_end; )
	{
	  std::map<SerialNumber_t, std::shared_ptr<ScanData_t> >::iterator current_scan_data_iter = scan_data_iter++;
	  scan_data_end = (scan_data_iter == m_scanData.end());

	  unsigned int rods_without_data_finished=0;
	  unsigned int rods_without_data_unfinished=0;
	  bool got_all_data=true;

	  {
	    Lock rod_data_lock (current_scan_data_iter->second->rodDataMutex());

	    if (m_logLevel>=kDiagnostics1Log) {
	      for ( std::map< PixA::RodLocationBase, std::shared_ptr<RodData_t> >::iterator rod_iter = current_scan_data_iter->second->beginRod();
		    rod_iter != current_scan_data_iter->second->endRod();
		    rod_iter++) {

		if (!rod_iter->second->hasData()) { // no rod in the list should already have data
		  if (rod_iter->second->scanStatus() >= ScanIsStatus::kSuccess) {
		    rods_without_data_finished++;
		  }
		  else {
		    rods_without_data_unfinished++;
		  }
		}
	      }
	    }

	    for ( std::map< PixA::RodLocationBase, std::shared_ptr<RodData_t> >::iterator rod_iter = current_scan_data_iter->second->beginRod();
		  rod_iter != current_scan_data_iter->second->endRod();
		  rod_iter++) {

	      if (!rod_iter->second->hasData() && !rod_iter->second->empty()) { // no rod in the list should already have data
		got_all_data=false;
		if (rod_iter->second->scanStatus() >= ScanIsStatus::kSuccess) {
		
		  rod_iter->second->markForProcessing();
		  scan_data_of_rod = current_scan_data_iter;
		  serial_number_for_download = current_scan_data_iter->first;
		  rod_for_download = rod_iter;
		  has_rod_for_download = true;
		  break;
		}
		else if (m_logLevel>=kDiagnosticsLog) {
		  std::stringstream message;
		  message << "Status of " << makeScanSerialNumberString(current_scan_data_iter->first) << " for  " << rod_iter->first 
			  << " is " << ScanIsStatus::name(rod_iter->second->scanStatus()) << std::endl;
		  {
// 		    Lock mrs_lock(m_mrsMutex);
// 		    sendMessage(*m_out,"DATASRV_scanProcessed",MRS_DIAGNOSTIC, "MasterDataServer::NextScanToDownload", message.str());
		    ers::debug(PixLib::pix::daq (ERS_HERE, "MasterDataServer::NextScanToDownload", message.str()));
		  }
		}
	      }
	    }
	  }

	  if (m_logLevel>=kDiagnostics1Log) {
	    std::stringstream message;
	    message << "Scan " << makeScanSerialNumberString(current_scan_data_iter->first) << " still has " 
		    << rods_without_data_finished << " + " << rods_without_data_unfinished << " ROD(s) without data "
		    << "" << std::endl;
	    {
// 	      Lock mrs_lock(m_mrsMutex);
// 	      sendMessage(*m_out,"DATASRV_scanProcessed",MRS_DIAGNOSTIC, "MasterDataServer::NextScanToDownload", message.str());
	      ers::debug(PixLib::pix::daq (ERS_HERE, "MasterDataServer::NextScanToDownload", message.str()));
	    }
	    tot_rods_without_data_unfinished += rods_without_data_unfinished;
	    tot_rods_without_data_finished += rods_without_data_finished;
	  }

	  if (has_rod_for_download) {
	    break;
	  }
	  else if (got_all_data && !current_scan_data_iter->second->hasNewData() ) {

	    if (m_logLevel>=kDiagnosticsLog) {
	      std::stringstream message;
	      message << "Finished processing histogram requests for scan " << makeScanSerialNumberString(current_scan_data_iter->first) << ".";
	      {
// 		Lock mrs_lock(m_mrsMutex);
// 		sendMessage(*m_out,"DATASRV_scanProcessed",MRS_DIAGNOSTIC, "MasterDataServer::NextScanToDownload", message.str());
		ers::debug(PixLib::pix::daq (ERS_HERE, "MasterDataServer::NextScanToDownload", message.str()));
	      }
	    }

	    // the jobletts keep a copy of the data
	    // so the scan data can be deleted from the master data server
	    unsubscribe(current_scan_data_iter->first);
	    m_scanData.erase(current_scan_data_iter);
	  }
	}
    }
    if (m_logLevel>=kDiagnostics1Log) {
      std::stringstream message;
      message << "Total number of waiting RODs : " << tot_rods_without_data_finished << " + " << tot_rods_without_data_unfinished << " ROD(s) without data."
	      << (has_rod_for_download ? " Offered ROD for download." : "")
	      << std::endl;
      {
// 	Lock mrs_lock(m_mrsMutex);
// 	sendMessage(*m_out,"DATASRV_scanProcessed",MRS_DIAGNOSTIC, "MasterDataServer::NextScanToDownload", message.str());
	ers::debug(PixLib::pix::daq (ERS_HERE, "MasterDataServer::NextScanToDownload", message.str()));
      }
    }

    return has_rod_for_download;
  }


  void MasterDataServer::downloadHistogramms(SerialNumber_t serial_number_for_download,
					     const ScanData_t &scan_data,
					     std::map< PixA::RodLocationBase,std::shared_ptr<RodData_t> >::iterator rod_for_download) 
  {
    std::pair<SerialNumber_t, PixA::RodLocationBase> joblett_key;
    {
      Lock rod_data_server_list_lock(rod_for_download->second->dataServerMutex() );
      bool end = (rod_for_download->second->beginDataServer() == rod_for_download->second->endDataServer());
      for (std::map< SerialNumber_t, AnalysisData_t >::iterator analysis_iter = rod_for_download->second->beginDataServer();
	   !end;
	   ) {

	std::map< SerialNumber_t, AnalysisData_t >::iterator current_analysis_iter = analysis_iter++;
	end = (analysis_iter == rod_for_download->second->endDataServer());

	// When adding new data servers it is verified that the data server is of type DataServer,
	// so the following cast is correct and the pointer is valid (theoretically ;-)
	DataServer *data_server = static_cast<DataServer *>(current_analysis_iter->second.dataServer() );

	if (!current_analysis_iter->second.isAborted() && !m_reset) {

	if (m_logLevel>=kDiagnosticsLog) {
	  std::stringstream message;
	  message << "Download histograms for analysis " << makeAnalysisSerialNumberString(current_analysis_iter->first)
		  << " of scan " << makeScanSerialNumberString(serial_number_for_download) 
		  << " for ROD " << rod_for_download->first << ".";
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"DATASRV_getHistoList",MRS_DIAGNOSTIC, "MasterDataServer::run_detached", message.str());
	  ers::debug(PixLib::pix::daq (ERS_HERE, "MasterDataServer::run_detached", message.str()));
	}

	unsigned int n_rod_histos=0;

// 	if (!data_server->abort()) {
// 	//inserted by peter
// 	std::vector<std::string> visible_histos = data_server->dataServerHistos();
// 	if (visible_histos.size() == 1 && (*(visible_histos.begin())) == "All")
// 	  {
// 	    std::vector<std::string> all_conn_names;
// 	    std::vector<std::string>::iterator all_conn_iter;

// 	    std::map<std::string, TopLevelHistoArray *>::iterator conn_iter;
// 	    for (conn_iter  = data_server->histoBegin()->second.begin();
// 		 conn_iter != data_server->histoBegin()->second.end();
// 		 conn_iter++){
// 	      all_conn_names.push_back(conn_iter->first);
// 	    }
// 	    std::vector<std::string> all_histo_names;
// 	    std::vector<std::string>::iterator all_histo_iter;

// 	    all_conn_iter = all_conn_names.begin();

// 	    while (all_histo_names.size()==0 && all_conn_iter != all_conn_names.end()){
// 	      all_histo_names = m_histoReceiver->availableHistograms(serial_number_for_download, 
// 								     rod_for_download->first,
// 								     (*all_conn_iter));
// 	      std::cout<<"Tried "<<(*all_conn_iter)<<" Got : "<<all_histo_names.size()<<" histograms"<<std::endl;
// 	      all_conn_iter++;
// 	     }

// 	     if (all_conn_names.size() != 0 && all_histo_names.size() != 0){
// 	      data_server->toggleRequests(true);
// 	      for(all_conn_iter = all_conn_names.begin() ; all_conn_iter != all_conn_names.end() ; all_conn_iter++){
// 		for (all_histo_iter = all_histo_names.begin() ; all_histo_iter != all_histo_names.end() ; all_histo_iter++){
// 		  data_server->requestHistogram( (*all_histo_iter),(*all_conn_iter) );
// 		}
// 	      }
// 	      data_server->toggleRequests(false);
// 	    }
// 	    else{
// 	      std::cout<<"Could not find any histograms for this pp0"<<std::endl;
// 	    }
// 	  }
// 	//end peter
// 	}

	for (std::map<std::string, std::map<std::string, TopLevelHistoArray *> >::iterator histo_name_iter = data_server->histoBegin();
	     histo_name_iter != data_server->histoEnd();
	     histo_name_iter++) {

	  if (data_server->abort()) {
	    break;
	  }

	  std::stringstream message;
	  message << "Download histogram " << histo_name_iter->first << " for " << rod_for_download->first << " :";

	  std::string modules_with_missing_histograms;

	  for(std::map<std::string, TopLevelHistoArray *>::iterator conn_iter = histo_name_iter->second.begin();
	      conn_iter != histo_name_iter->second.end();
	      conn_iter++) {

	    if (data_server->abort()) {
	      break;
	    }
	    try {
	      if (!conn_iter->second) {
		conn_iter->second = new TopLevelUpdateHistoArray;
	      }
	      //	      {
	      //		std::stringstream message;
	      //		message << "Try to get histogram " << histo_name_iter->first << " for analysis " << makeAnalysisSerialNumberString(current_analysis_iter->first)
	      //			<< " of scan " << makeScanSerialNumberString(serial_number_for_download) 
	      //			<< " for module " <<  conn_iter->first << ".";
	      //		sendMessage(*m_out,"DATASRV_getHistoList",MRS_DIAGNOSTIC, "MasterDataServer::run_detached", message.str());
	      //	      }
	      int n_histos = m_histoReceiver->getHistograms(serial_number_for_download,
							    rod_for_download->first,
							    conn_iter->first,
							    histo_name_iter->first,
							    *(conn_iter->second));

	      if (n_histos==0) {
		modules_with_missing_histograms += conn_iter->first;
		modules_with_missing_histograms += " ";
	      }
	      else {
		n_rod_histos+=n_histos;
		message << " " << conn_iter->first;
	      }
	    }
	    catch (daq::is::Exception &err) {
	      if (m_logLevel>=kErrorLog) {
		std::stringstream error_message;
		error_message << "Caught exception while retrieving histograms for analysis " << makeAnalysisSerialNumberString(current_analysis_iter->first)
			      << " of scan " << makeScanSerialNumberString(serial_number_for_download)
			      << " for  "  << conn_iter->first << " of ROD " << rod_for_download->first << " : "
			      << err.message();
// 		Lock mrs_lock(m_mrsMutex);
// 		sendMessage(*m_out,"DATASRV_missingHisto",MRS_ERROR, "MasterDataServer::run_detached", error_message.str());
		ers::error(PixLib::pix::daq (ERS_HERE, "MasterDataServer::run_detached", error_message.str()));
	      }
	    }
	    catch (...) {
	      if (m_logLevel>=kErrorLog) {
		std::stringstream error_message;
		error_message << "Caught unknown exception while retrieving histograms for analysis " << makeAnalysisSerialNumberString(current_analysis_iter->first)
			      << " of scan " << makeScanSerialNumberString(serial_number_for_download)
			      << " for  "  << conn_iter->first << " of ROD " << rod_for_download->first << ".";
// 		Lock mrs_lock(m_mrsMutex);
// 		sendMessage(*m_out,"DATASRV_missingHisto",MRS_ERROR, "MasterDataServer::run_detached", error_message.str());
		ers::error(PixLib::pix::daq (ERS_HERE, "MasterDataServer::run_detached", error_message.str()));
	      }
	    }
	  }

	  if (n_rod_histos==0 && m_logLevel>=kDiagnosticsLog) {
	    message.clear();
	    message << "Did not download any histograms " << histo_name_iter->first << "for " << rod_for_download->first << ".";
	  }
	  else {
	    if (!modules_with_missing_histograms.empty()) {
	      if (m_logLevel>=kErrorLog) {
		std::stringstream error_message;
		error_message << "Did not get any histograms for analysis " << makeAnalysisSerialNumberString(current_analysis_iter->first)
			      << " of scan " << makeScanSerialNumberString(serial_number_for_download)
			      << " for modules of ROD " << rod_for_download->first << " : " << modules_with_missing_histograms << ".";
// 		Lock mrs_lock(m_mrsMutex);
// 		sendMessage(*m_out,"DATASRV_missingHisto",MRS_ERROR, "MasterDataServer::run_detached", error_message.str());
		ers::error(PixLib::pix::daq (ERS_HERE, "MasterDataServer::run_detached", error_message.str()));
	      }
	    }

	    if (m_logLevel>=kDiagnosticsLog) {
	      message << ". Downloaded " << n_rod_histos << " histograms in total.";
	    }
	  }

	  if (m_logLevel>=kDiagnosticsLog) {
// 	    Lock mrs_lock(m_mrsMutex);
// 	    sendMessage(*m_out,"DATASRV_getHisto",MRS_DIAGNOSTIC, "MasterDataServer::downloadHistogramms", message.str());
	    ers::debug(PixLib::pix::daq (ERS_HERE, "MasterDataServer::downloadHistogramms", message.str()));
	  }
	}
	}
	else if (m_logLevel>=kDiagnosticsLog) {
	  std::stringstream message;
	  message << "Aborted histogram download for analysis " << makeAnalysisSerialNumberString(current_analysis_iter->first)
		  << " of scan " << makeScanSerialNumberString(serial_number_for_download) 
		  << " for ROD " << rod_for_download->first << ".";
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"DATASRV_getHistoList",MRS_DIAGNOSTIC, "MasterDataServer::run_detached", message.str());
	  ers::debug(PixLib::pix::daq (ERS_HERE, "MasterDataServer::run_detached", message.str()));
	}
	current_analysis_iter->second.setHasData();

	if (m_logLevel>=kDiagnosticsLog) {
	  std::stringstream message;
	  message << (!data_server->abort() ? "Processed" : "Aborted") 
		  << " all histogram requests for analysis " << makeAnalysisSerialNumberString(current_analysis_iter->first)
		  << " of scan " << makeScanSerialNumberString(serial_number_for_download) 
		  << " for ROD " << rod_for_download->first << ".";
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"DATASRV_finishedreq",MRS_DIAGNOSTIC, "MasterDataServer::downloadHistogramms", message.str());
	 ers::debug(PixLib::pix::daq (ERS_HERE, "MasterDataServer::downloadHistogramms", message.str()));
	}
	if( hasAllData(data_server) ) {
	  m_executionUnit->dataAvailable(current_analysis_iter->first, rod_for_download->first);
	}
	rod_for_download->second->erase(current_analysis_iter);
      }
    }
    rod_for_download->second->setHasData();
  }

  void MasterDataServer::checkForNewDataServers()
  {
    for (;!m_reset;) {
      std::map<SerialNumber_t, std::shared_ptr<ScanData_t> >::iterator scan_data_with_new_data_server;
      DataServerList_t::iterator new_data_server_iter;
      bool has_new_data_server=false;
      {
	Lock lock(m_scanDataMutex);

	// first search for rods for which the scan has finished.
	// start with oldest scans first
	// @todo better than always start from the beginning ?
	for (std::map<SerialNumber_t, std::shared_ptr<ScanData_t> >::iterator scan_data_iter = m_scanData.begin();
	     scan_data_iter != m_scanData.end();
	     ) {
	  std::map<SerialNumber_t, std::shared_ptr<ScanData_t> >::iterator current_scan_data_iter = scan_data_iter++;
	  Lock rod_data_lock (current_scan_data_iter->second->newDataServerMutex());
	  if (current_scan_data_iter->second->beginNewDataServer() != current_scan_data_iter->second->endNewDataServer()) {
	    if (m_logLevel>=kDiagnosticsLog) {
	      std::cout << "DIAGNOSTICS [MasterDataServer::checkForNewDataServers] new data server for " <<  current_scan_data_iter->first << std::endl;
	    }

	    scan_data_with_new_data_server = current_scan_data_iter;
	    new_data_server_iter = current_scan_data_iter->second->beginNewDataServer();
	    has_new_data_server=true;
	    break;
	  }
	}
      }
      if (!has_new_data_server) break;
      if (m_logLevel>=kDiagnosticsLog) {
	std::stringstream message;
	message << "Add data server for analysis " << makeAnalysisSerialNumberString(new_data_server_iter->second.first) << "."
		<< " of scan " << makeScanSerialNumberString(scan_data_with_new_data_server->first) << " for ROD "
		<< new_data_server_iter->first
		<< ".";
 	{
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"DATASRV_addDataServer",MRS_DIAGNOSTIC, "MasterDataServer::run_detached", message.str());
	  ers::debug(PixLib::pix::daq (ERS_HERE, "MasterDataServer::run_detached", message.str()));
	}
      }

      // before a new data server is added the configuration requests are processed.
      updateScanConfigurations( scan_data_with_new_data_server );
      if (m_reset) return;
      updateConfigurations( scan_data_with_new_data_server );
      if (m_reset) return;
      if (new_data_server_iter->second.second->abort()) {
	// do not process this data server any further
	scan_data_with_new_data_server->second->removeNewDataServer(new_data_server_iter);
	m_executionUnit->dataAvailable(new_data_server_iter->second.first, new_data_server_iter->first);
	continue;
      }
      // finally add the data server
      SerialNumber_t analysis_serial_number = new_data_server_iter->second.first;
      std::string rod_name = new_data_server_iter->first;
      ScanIsStatus::EStatus initial_status = ScanIsStatus::kUnknown;
      {
	DataServer *data_server = dynamic_cast<DataServer *>( new_data_server_iter->second.second );
	if (data_server) {
	  switch (data_server->scanInfo().status()) {
	  case CAN::kSuccess:
	  case CAN::kUnknown:
	    initial_status = ScanIsStatus::kSuccess;
	    break;
	  case CAN::kFailure:
	    initial_status = ScanIsStatus::kFailed;
	    break;
	  case CAN::kAborted:
	    initial_status = ScanIsStatus::kAborted;
	    break;
	  default:
	    break;
	  }
	  if (m_logLevel>=kDiagnosticsLog) {
	    std::stringstream message;
	    message << makeAnalysisSerialNumberString(new_data_server_iter->second.first) << " Initial status of scan "
		    << makeScanSerialNumberString(scan_data_with_new_data_server->first) << " for ROD "
		    << new_data_server_iter->first
		    << " is " << ScanIsStatus::name(initial_status)
		    << ".";
	    {
// 	      Lock mrs_lock(m_mrsMutex);
// 	      sendMessage(*m_out,"DATASRV_addDataServer",MRS_DIAGNOSTIC, "MasterDataServer::run_detached", message.str());
	      ers::debug(PixLib::pix::daq (ERS_HERE, "MasterDataServer::run_detached", message.str()));
	    }
	  }

	}
      }
      std::shared_ptr<RodData_t> new_rod_data = scan_data_with_new_data_server->second->insertDataServer(new_data_server_iter, initial_status);
      // now new_data_server_iter is invalid
      bool has_data_server_with_data=false;
      if (new_rod_data.get()) {
	if (initial_status< ScanIsStatus::kSuccess ) {
	  // 	  std::stringstream message;
	  // 	  message << "Scan not yet finished :" << makeAnalysisSerialNumberString(analysis_serial_number) << " "
	  // 		  << " of scan " << makeScanSerialNumberString(scan_data_with_new_data_server->first) << " for ROD "
	  // 		  << rod_name
	  // 		  << ".";
	  // 	  {
	  // 	    Lock mrs_lock(m_mrsMutex);
	  // 	    sendMessage(*m_out,"DATASRV_canDownLoad",MRS_DIAGNOSTIC, "MasterDataServer::checkForNewDataServers", message.str());
	  // 	  }

	  // get the current status
	  std::string is_status_pattern = makeIsScanStatusPattern(scan_data_with_new_data_server->first, new_data_server_iter->first);
	  ISInfoIterator ii( *m_scanPartition, m_isServerName , ScanData_t::s_test.type() && is_status_pattern );
	  while (ii()) {
	    ii.value( new_rod_data->isScanStatusString() );
	    new_rod_data->updateScanStatus();
	    if ( new_rod_data->scanStatus()>=ScanIsStatus::kSuccess ) {
	      if (m_logLevel>=kDiagnosticsLog) {
		std::stringstream message;
		message << "Histograms for " << makeAnalysisSerialNumberString(analysis_serial_number) << "."
			<< " of scan " << makeScanSerialNumberString(scan_data_with_new_data_server->first) << " for ROD "
			<< rod_name
			<< " are available (2).";
		{
// 		  Lock mrs_lock(m_mrsMutex);
// 		  sendMessage(*m_out,"DATASRV_canDownLoad",MRS_DIAGNOSTIC, "MasterDataServer::run_detached", message.str());
		  ers::debug(PixLib::pix::daq (ERS_HERE, "MasterDataServer::run_detached", message.str()));
		}
	      }
	      has_data_server_with_data=true;
	    }
	  }
	}
	else {
	  has_data_server_with_data=true;
	  // 	  std::stringstream message;

	  // 	  message << "Scan has finished :" << makeAnalysisSerialNumberString(analysis_serial_number) << " "
	  // 		  << " of scan " << makeScanSerialNumberString(scan_data_with_new_data_server->first) << " for ROD "
	  // 		  << rod_name
	  // 		  << ".";
	  // 	  {
	  // 	    Lock mrs_lock(m_mrsMutex);
	  // 	    sendMessage(*m_out,"DATASRV_canDownLoad",MRS_DIAGNOSTIC, "MasterDataServer::checkForNewDataServers", message.str());
	  // 	  }

	}
	
	if (has_data_server_with_data) {
	  m_dataRequest->setFlag();
	  break;
	}
      }
    }//end for loop
  }

  void MasterDataServer::updateScanConfigurations(std::map<SerialNumber_t, std::shared_ptr<ScanData_t> >::iterator& scan_data_with_new_data_server)
  {
 //     std::cout <<"rodname is in this point: "<< scan_data_with_new_data_server->second->beginNewDataServer()->first<<std::endl;
    ScanData_t &scan_data = *(scan_data_with_new_data_server->second);
    DataServerList_t::iterator new_data_server_iter = scan_data.beginNewDataServer();
    DataServer *data_server = static_cast<DataServer *>( new_data_server_iter->second.second );
      const PixA::ConnectivityRef &conn( scan_data.connectivity());
      std::vector<std::string>::const_iterator module_iter = data_server->modConfigBegin();

    if (!scan_data.scanConnSet() && !data_server->abort()) {
      // get the connectivity for this particular scan
      const PixLib::ScanInfo_t &a_scan_info( data_server->scanInfo() );
      try {

	if (m_logLevel>=kDiagnosticsLog) {
	  std::stringstream message;
	  message << "Query connectivity : "
		  << a_scan_info.connTag();
	  if (a_scan_info.aliasTag() != a_scan_info.connTag() && a_scan_info.dataTag() != a_scan_info.connTag() ) {
	    message << " " << a_scan_info.aliasTag() << " " << a_scan_info.dataTag();
	  }
	  message << " " << a_scan_info.cfgTag() << " / " << a_scan_info.cfgRev();
	  if (a_scan_info.cfgTag() != a_scan_info.modCfgTag()) {
	    message << " " << a_scan_info.modCfgTag() << " / " << a_scan_info.modCfgRev();
	  }

// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"DATASRV_connRequest",MRS_DIAGNOSTIC, "CAN::MasterDataServer::getScanParameter",message.str());
	  ers::debug(PixLib::pix::daq (ERS_HERE, "CAN::MasterDataServer::getScanParameter",message.str()));
	}

	Lock globalLock(PixDbGlobalMutex::mutex());
	scan_data.setConnectivity(PixA::ConnectivityManager::getConnectivity( a_scan_info.idTag(),
									      a_scan_info.connTag(),
									      a_scan_info.aliasTag(),
									      a_scan_info.dataTag(),
									      a_scan_info.cfgTag(),
									      a_scan_info.modCfgTag(),
									      a_scan_info.cfgRev()) );
          
      }
      catch(PixA::ConfigException &err) {
	if (m_logLevel>=kErrorLog) {
	  std::string message(" While requesting connectivity : Caught exception : " );
	  message += err.getDescriptor() ;
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"DATASRV_no_conn",MRS_ERROR, "CAN::MasterDataServer::getScanParameter",message);
	  ers::info(PixLib::pix::daq (ERS_HERE, "CAN::MasterDataServer::getScanParameter",message));
	}
      }
      catch(...) {
	if (m_logLevel>=kErrorLog) {
	  std::stringstream message;
	  message << "Caught unknown exception while requesting connectivity : "
		  << a_scan_info.connTag();
	  if (a_scan_info.aliasTag() != a_scan_info.connTag() && a_scan_info.dataTag() != a_scan_info.connTag() ) {
	    message << " " << a_scan_info.aliasTag() << " " << a_scan_info.dataTag();
	  }
	  message << " " << a_scan_info.cfgTag() << " / " << a_scan_info.cfgRev();
	  if (a_scan_info.cfgTag() != a_scan_info.modCfgTag()) {
	    message << " " << a_scan_info.modCfgTag() << " / " << a_scan_info.modCfgRev();
	  }

// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"DATASRV_no_conn",MRS_ERROR, "CAN::MasterDataServer::getScanParameter",message.str());
	  ers::error(PixLib::pix::daq (ERS_HERE,  "CAN::MasterDataServer::getScanParameter",message.str()));
	}
      }
     scan_data.setScanConnSet();
    }
    // get the scan configuration
    if (data_server->needScanConfig() && !data_server->abort()) {
 //std::cout<<"vediamo dove sono 12 m"<<std::endl;
      try {
 //          std::cout<<"vediamo dove sono 12 n"<<std::endl;
	if (module_iter != data_server->modConfigEnd()) {
//         std::cout<<"vediamo dove sono 12 o"<<std::endl;
	  PixA::ConfigHandle scan_config;

	  const PixLib::ScanInfo_t &a_scan_info( data_server->scanInfo() );
	  scan_data.setScanCfg( PixA::ConfigHandle(ScanConfigDb::instance()->config_1(a_scan_info.scanType(),
										     a_scan_info.scanTypeTag(),
										     a_scan_info.scanTypeRev(), conn.modConf_name(*module_iter)) ));
	  scan_data.setScanCfgSet();
//         std::cout<<"vediamo dove sono 12 p"<<std::endl;
	}

	data_server->addScanConfig(scan_data.scanCfg());
      }
      catch(MRSException &err) {
	if (m_logLevel>=kErrorLog) {
	  //	  Lock mrs_lock(m_mrsMutex);
	  err.sendMessage();//*m_out,"MasterDataServer::updateScanConfigurations");
	}
      }
      catch(...) {
	if (m_logLevel>=kErrorLog) {
	  std::stringstream message;
	  message << "Failed to get scan config for " << makeScanSerialNumberString(scan_data_with_new_data_server->first) << ".";
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"DATASRV_no_scan_config",MRS_ERROR, "MasterDataServer::updateScanConfigurations", message.str());
	  ers::error(PixLib::pix::daq (ERS_HERE, "MasterDataServer::updateScanConfigurations", message.str()));
	}
      }
    }

  }


  void MasterDataServer::updateConfigurations(std::map<SerialNumber_t, std::shared_ptr<ScanData_t> >::iterator& scan_data_with_new_data_server)
  {
    ScanData_t &scan_data = *(scan_data_with_new_data_server->second);
    DataServerList_t::iterator new_data_server_iter = scan_data_with_new_data_server->second->beginNewDataServer();
    DataServer *data_server = static_cast<DataServer *>( new_data_server_iter->second.second );


    // The connectivity must be the same for all analyses, but
    // the configuration can be different for the individual scans 
    // of an analysis.
    // So, the connectivity attached to the scan is used
    // to get the configurations.
    const PixA::ConnectivityRef &conn( scan_data.connectivity());

    if (data_server->needPixModuleGroupConfig() && !data_server->abort()) {
      assert( scan_data.scanConnSet() );

      try {
	Lock config_db_lock(ConnObjConfigDbGlobalMutex::mutex());
	PixA::ConnObjConfigDbTagRef cfg_tag( conn.configDb() );
	Lock globalLock(PixDbGlobalMutex::mutex());
	std::shared_ptr<const PixLib::PixModuleGroup> module_group( cfg_tag.configuredModuleGroupPtr(new_data_server_iter->first));
	if (module_group.get()) {
	  data_server->addModuleGroupConfig(module_group);
	  if (m_logLevel>=kDiagnosticsLog) {
	    std::stringstream message;
	    message << "Got a PixModuleGroup for " << new_data_server_iter->first
 		    << ", scan " << makeScanSerialNumberString(scan_data_with_new_data_server->first) << ".";
// 	    sendMessage(*m_out,"DATASRV_bocConfig",MRS_DIAGNOSTIC, "MasterDataServer::updateConfigurations", message.str());
	      ers::debug(PixLib::pix::daq (ERS_HERE, "MasterDataServer::updateConfigurations", message.str()));
	  }
	}
      }
      catch( PixA::ConfigException &err) {
	if (m_logLevel>=kErrorLog) {
	  std::stringstream message;
	  message << "While trying to get a PixModuleGroup for " << new_data_server_iter->first
		  << ", scan " << makeScanSerialNumberString(scan_data_with_new_data_server->first) << ", caught exception : "
		  << err.getDescriptor();
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"DATASRV_bocConfig",MRS_ERROR, "MasterDataServer::updateConfigurations", message.str());
	  ers::error(PixLib::pix::daq (ERS_HERE, "MasterDataServer::updateConfigurations", message.str()));
	}
      }
      catch( ... ) {
      }

    }
    else if (data_server->needBocConfig() && !data_server->abort()) {
      assert( scan_data.scanConnSet() );
      PixA::ConfigHandle boc_config;
      try {
	Lock config_db_lock(ConnObjConfigDbGlobalMutex::mutex());
	Lock globalLock(PixDbGlobalMutex::mutex());
	boc_config = conn.bocConfFromRodLocation(new_data_server_iter->first);
	if (boc_config) {
	  // need to protect against concurrent RootDb reading
	  data_server->addBocConfig(boc_config);
	}
      }
      catch( PixA::ConfigException &err) {
	if (m_logLevel>=kErrorLog) {
	  std::stringstream message;
	  message << "While trying to access the BOC configuration for " << new_data_server_iter->first
		  << ", scan " << makeScanSerialNumberString(scan_data_with_new_data_server->first) << ", caught exception : "
		  << err.getDescriptor();
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"DATASRV_bocConfig",MRS_ERROR, "MasterDataServer::updateConfigurations", message.str());
	  ers::error(PixLib::pix::daq (ERS_HERE, "MasterDataServer::updateConfigurations", message.str()));
	}
      }
      catch( ... ) {
      }

      if (!boc_config) {
	if (m_logLevel>=kErrorLog) {
	  std::stringstream message;
	  message << "No boc config for " << makeScanSerialNumberString(scan_data_with_new_data_server->first) << " and ROD " << new_data_server_iter->first << ".";
// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"DATASRV_bocConfig",MRS_ERROR, "MasterDataServer::updateConfigurations", message.str());
	  ers::error(PixLib::pix::daq (ERS_HERE, "MasterDataServer::updateConfigurations", message.str()));
	}
      }
    }

    if (data_server->needModuleConfig() && !data_server->abort()) {
      assert( scan_data.scanConnSet() );

      // need to protect against concurrent RootDb reading
      Lock config_db_lock(ConnObjConfigDbGlobalMutex::mutex());
      Lock globalLock(PixDbGlobalMutex::mutex());
      unsigned int error_counter=0;
      std::string error_module_list;
      for(std::vector<std::string>::const_iterator module_iter = data_server->modConfigBegin();
	  module_iter != data_server->modConfigEnd() && !data_server->abort() && !m_reset;
	  module_iter++) {

	PixA::ConfigHandle module_config;
	try {

	  module_config = conn.modConf(*module_iter);
	  if (module_config) {
	    data_server->addModuleConfig(*module_iter, module_config);
	  }
}
	catch( PixA::ConfigException &err) {
	  module_config=PixA::ConfigHandle();
	  if (error_counter<2) {
	    if (m_logLevel>=kErrorLog) {
	      std::stringstream message;
	      message << "While trying to access the Module configuration for " << *module_iter << " / " << new_data_server_iter->first
		      << ", scan " << makeScanSerialNumberString(scan_data_with_new_data_server->first) << ", caught exception : "
		      << err.getDescriptor();
	      if (error_counter==1) {
		message << " Will not be repeated for following modules.";
	      }
// 	      Lock mrs_lock(m_mrsMutex);
// 	      sendMessage(*m_out,"DATASRV_moduleConfig",MRS_ERROR, "MasterDataServer::updateConfigurations", message.str());
	      ers::error(PixLib::pix::daq (ERS_HERE, "MasterDataServer::updateConfigurations", message.str()));
	    }
	  }
	  error_counter++;
	}
	catch (...) {
	}
	if (!module_config) {
	  if (!error_module_list.empty()) {
	    error_module_list+=", ";
	  }
	  error_module_list+=  *module_iter;
	}
      }
      if (!error_module_list.empty() && m_logLevel>=kErrorLog) {
	std::stringstream message;
	message << "No module config for module(s) " << error_module_list << " of ROD " << new_data_server_iter->first 
		<< " in scan " << makeScanSerialNumberString(scan_data_with_new_data_server->first) << ".";
// 	Lock mrs_lock(m_mrsMutex);
// 	sendMessage(*m_out,"DATASRV_moduleConfig",MRS_ERROR, "MasterDataServer::updateConfigurations", message.str());
	ers::error(PixLib::pix::daq (ERS_HERE, "MasterDataServer::updateConfigurations", message.str()));
      }

    }

    if (m_logLevel>=kDiagnosticsLog) {
      std::stringstream message;
      message << "Finished configuration requests for " << makeAnalysisSerialNumberString(new_data_server_iter->second.first) << "."
	      << " of scan " << makeScanSerialNumberString(scan_data_with_new_data_server->first) << " for ROD "
	      << new_data_server_iter->first
	      << ".";
      {
// 	Lock mrs_lock(m_mrsMutex);
// 	sendMessage(*m_out,"DATASRV_addDataServer",MRS_DIAGNOSTIC, "MasterDataServer::run_detached", message.str());
	ers::debug(PixLib::pix::daq (ERS_HERE, "MasterDataServer::run_detached", message.str()));
      }
    }
  }

  std::string MasterDataServer::makeIsScanStatusPattern(SerialNumber_t scan_serial_number) {
    std::stringstream is_status_pattern;
    is_status_pattern << ".*(/)*S" << std::setw(9) << std::setfill('0') << scan_serial_number << "/.*" << "/" << "STATUS";
    return is_status_pattern.str();
  }

  std::string MasterDataServer::makeIsScanStatusPattern(SerialNumber_t scan_serial_number, const std::string &rod_name) {
    std::stringstream is_status_pattern;
    is_status_pattern << ".*(/)*S" << std::setw(9) << std::setfill('0') << scan_serial_number << "/.*" << rod_name << "/" << "STATUS";
    return is_status_pattern.str();
  }

  class FlagToggler
  {
  public:
    FlagToggler(bool &flag) :m_flag(&flag){ *m_flag = true;}
    ~FlagToggler() { *m_flag=false;}
  private:
    bool *m_flag;
  };

  void MasterDataServer::abort(SerialNumber_t analysis_serial_number)
  {
    FlagToggler toggle(m_abortRequest);

    unsigned int n_rods=0;
    unsigned int n_rods_processed=0;
    {
      Lock lock(m_scanDataMutex);

      for(std::map<SerialNumber_t, std::shared_ptr<ScanData_t> >::iterator scan_data_iter = m_scanData.begin();
	  scan_data_iter != m_scanData.end();
	  ++scan_data_iter) {

	// abort new data server
	{
	  Lock new_data_mutex_lock(scan_data_iter->second->newDataServerMutex() );

	  for(DataServerList_t::iterator  rod_iter = scan_data_iter->second->beginNewDataServer();
	    rod_iter != scan_data_iter->second->endNewDataServer();
	    ++rod_iter) {

	    if (rod_iter->second.first == analysis_serial_number) {
	      rod_iter->second.second->setAbort();
	      n_rods++;
	    }

	  }
	}

	// abort added data server
	{
	Lock rod_mutex_lock(scan_data_iter->second->rodDataMutex() );

	for(std::map< PixA::RodLocationBase, std::shared_ptr<RodData_t> >::iterator  rod_iter = scan_data_iter->second->beginRod();
	    rod_iter != scan_data_iter->second->endRod();
	    ++rod_iter) {

	  {

	    Lock rod_data_server_list_lock(rod_iter->second->dataServerMutex() );

	    // the rod could be processed at the moment
	    std::map< SerialNumber_t, AnalysisData_t >::iterator analysis_iterator = rod_iter->second->findDataServer(analysis_serial_number);
	    if ( analysis_iterator != rod_iter->second->endDataServer()) {

	      abortAnalysisData(analysis_iterator->second);
	      n_rods++;

	      if (!rod_iter->second->isBeingProcessed()) {
		// the rods which are not yet processed can be removed straight away.
		analysis_iterator->second.setHasData();
		if( MasterDataServer::hasAllData(analysis_iterator->second.dataServer()) ) {//CLA: only ready if all scans of one analysis have been downloaded
		  m_executionUnit->dataAvailable(analysis_iterator->first, rod_iter->first);
		}
		rod_iter->second->erase(analysis_iterator);
	      }
	      else {
		n_rods_processed++;
	      }
	    }
	    else if (m_logLevel>=kDiagnosticsLog) {
	      std::stringstream message;
	      message << "No analysis " << makeAnalysisSerialNumberString(analysis_serial_number)  << " registered for ROD " << rod_iter->first << " have : ";
	      for (std::map< SerialNumber_t, AnalysisData_t >::iterator analysis_iter = rod_iter->second->beginDataServer();
		   analysis_iter != rod_iter->second->endDataServer();
		   ++analysis_iter) {
		message << makeAnalysisSerialNumberString(analysis_iter->first) << " ";
	      }
	      message << "." ;
// 	      Lock mrs_lock(m_mrsMutex);
// 	      sendMessage(*m_out,"DATASRV_abort",MRS_DIAGNOSTIC, "MasterDataServer::abort", message.str());
	      ers::debug(PixLib::pix::daq (ERS_HERE, "MasterDataServer::abort", message.str()));

	    }
	  }

	}
	}
      }
    }
    
    if (m_logLevel>=kDiagnosticsLog) {
      std::stringstream message;
      message << "Marked " << n_rods << " - " << n_rods_processed << " RODs of analysis " << makeAnalysisSerialNumberString(analysis_serial_number) << " for aborting data retrieval.";

//       Lock mrs_lock(m_mrsMutex);
//       sendMessage(*m_out,"DATASRV_abort",MRS_DIAGNOSTIC, "MasterDataServer::abort", message.str());
      ers::debug(PixLib::pix::daq (ERS_HERE,  "MasterDataServer::abort", message.str()));
    }
    m_dataRequest->setFlag();

  }

  void MasterDataServer::reset() {
    m_reset=true;
    {
      Lock lock(m_scanDataMutex);
      for(std::map<SerialNumber_t, std::shared_ptr<ScanData_t> >::iterator scan_data_iter = m_scanData.begin();
	  scan_data_iter != m_scanData.end();
	  ++scan_data_iter) {

	Lock rod_mutex_lock(scan_data_iter->second->rodDataMutex() );

	for(std::map< PixA::RodLocationBase, std::shared_ptr<RodData_t> >::iterator  rod_iter = scan_data_iter->second->beginRod();
	    rod_iter != scan_data_iter->second->endRod();
	    ++rod_iter) {

	  Lock rod_data_server_list_lock(rod_iter->second->dataServerMutex() );
	  for (std::map< SerialNumber_t, AnalysisData_t >::iterator analysis_iter = rod_iter->second->beginDataServer();
	       analysis_iter != rod_iter->second->endDataServer();
	       ++analysis_iter) {

	    abortAnalysisData(analysis_iter->second);
	  }
	}
      }
    }

    m_dataRequest->setFlag();
    m_resetDone.wait();
  }

  void MasterDataServer::RodData_t::reset(IExecutionUnit *execution_unit, const std::string &rod_name) {
    
    Lock rod_data_server_list_lock(dataServerMutex() );
    std::map< SerialNumber_t, AnalysisData_t >::iterator next_analysis_iter = beginDataServer();
    bool end = (next_analysis_iter == endDataServer());
    for (;!end;) {
      std::map< SerialNumber_t, AnalysisData_t >::iterator analysis_iter = next_analysis_iter++;
      end = (next_analysis_iter == endDataServer());

      analysis_iter->second.setAbort();

      // the rods which are not yet processed can be removed straight away.
      analysis_iter->second.setHasData();
      if( MasterDataServer::hasAllData(analysis_iter->second.dataServer()) ) {//CLA: only ready if all scans of one analysis have been downloaded
	execution_unit->dataAvailable(analysis_iter->first, rod_name);
      }
      erase(analysis_iter);
    }
  }

  void MasterDataServer::ScanData_t::reset(IExecutionUnit *execution_unit) {

    // abort new data server
 

    Lock lock( rodDataMutex() );
    std::map< PixA::RodLocationBase, std::shared_ptr<RodData_t> >::iterator next_rod_data_iter = beginRod();
    bool end = (next_rod_data_iter == endRod());
    for(;!end;) {
      std::map< PixA::RodLocationBase, std::shared_ptr<RodData_t> >::iterator rod_data_iter = next_rod_data_iter++;
      end = (next_rod_data_iter == endRod());
      rod_data_iter->second->reset(execution_unit, rod_data_iter->first);
      m_rodData.erase(rod_data_iter);
    }
  }

  void MasterDataServer::_reset()
  {
    if (m_reset) {


      Lock lock(m_scanDataMutex);

      for (std::map<SerialNumber_t, std::shared_ptr<ScanData_t> >::iterator scan_data_iter = m_scanData.begin();
	   scan_data_iter != m_scanData.end();
	   ++scan_data_iter) {

	{
	  Lock new_data_mutex_lock(scan_data_iter->second->newDataServerMutex() );

	  for(DataServerList_t::iterator  rod_iter = scan_data_iter->second->beginNewDataServer();
	      rod_iter != scan_data_iter->second->endNewDataServer();
	      ++rod_iter) {
	    rod_iter->second.second->setAbort();
	    m_executionUnit->dataAvailable(rod_iter->second.first, rod_iter->first);
	  }

	  scan_data_iter->second->clearNewDataServer();
	}

	scan_data_iter->second->reset(m_executionUnit);
	unsubscribe(scan_data_iter->first);
      }
      m_scanData.clear();

      if (m_scanData.size()>0 && m_logLevel>=kDiagnosticsLog) {
	std::stringstream message;
	message << "Failed to aborted data retrieval for " << m_scanData.size() << " scan(s).";

// 	  Lock mrs_lock(m_mrsMutex);
// 	  sendMessage(*m_out,"DATASRV_cancel",MRS_DIAGNOSTIC, "MasterDataServer::_reset", message.str());
	ers::debug(PixLib::pix::daq (ERS_HERE, "MasterDataServer::_reset", message.str()));
	}
      }

    if (m_logLevel>=kInfoLog) {
//       Lock mrs_lock(m_mrsMutex);
//       sendMessage(*m_out,"DATASRV_reset",MRS_INFORMATION, "MasterDataServer::_reset", "Data server reset complete.");
      ers::info(PixLib::pix::daq (ERS_HERE, "MasterDataServer::_reset", "Data server reset complete."));
    }

    m_reset=false;
    m_resetDone.setFlag();
  }

  void MasterDataServer::dump() {
    if (m_logLevel>=kInfoLog) {
      std::cout << "INFO [MasterDataServer::dump] begin " << std::endl;
      Lock lock(m_scanDataMutex);
      for(std::map<SerialNumber_t, std::shared_ptr<ScanData_t> >::const_iterator scan_data_iter = m_scanData.begin();
	  scan_data_iter != m_scanData.end();
	  ++scan_data_iter) {

	std::cout << "INFO [MasterDataServer::dump] scan " << scan_data_iter->first 
		  << " empty=" << scan_data_iter->second->empty() 
		  << " new data=" << scan_data_iter->second->hasNewData() << std::endl;

	Lock rod_mutex_lock(scan_data_iter->second->rodDataMutex() );

	for(std::map< PixA::RodLocationBase, std::shared_ptr<RodData_t> >::const_iterator  rod_iter = scan_data_iter->second->beginRod();
	    rod_iter != scan_data_iter->second->endRod();
	    ++rod_iter) {

	  std::cout << "INFO [MasterDataServer::dump] rod " << scan_data_iter->first << " / "<< rod_iter->first << " : "
		    << " scan status " << ScanIsStatus::name(rod_iter->second->scanStatus()) << "(" << rod_iter->second->scanStatus() << ")"
		    << " emppty=" << rod_iter->second->empty()
		    << " has data=" << rod_iter->second->hasData() << " is being processed =" << rod_iter->second->isBeingProcessed()
		    << std::endl;

	  Lock rod_data_server_list_lock(rod_iter->second->dataServerMutex() );
	  for (std::map< SerialNumber_t, AnalysisData_t >::const_iterator analysis_iter = rod_iter->second->beginDataServer();
	       analysis_iter != rod_iter->second->endDataServer();
	       ++analysis_iter) {

	    std::cout << "INFO [MasterDataServer::dump] analysis S" << scan_data_iter->first << " / "<< rod_iter->first << " / "
		      << "A" << analysis_iter->first << ":"
		      << " has data=" << analysis_iter->second.hasData()
		      << std::endl;
	  }
	}
      }
      std::cout << "INFO [MasterDataServer::dump] end " << std::endl;
    }
  }

  bool MasterDataServer::hasDataForDownloading() {
    Lock lock(m_scanDataMutex);
    for(std::map<SerialNumber_t, std::shared_ptr<ScanData_t> >::const_iterator scan_data_iter = m_scanData.begin();
	scan_data_iter != m_scanData.end();
	++scan_data_iter) {
      
      Lock rod_mutex_lock(scan_data_iter->second->rodDataMutex() );
      
      for(std::map< PixA::RodLocationBase, std::shared_ptr<RodData_t> >::const_iterator  rod_iter = scan_data_iter->second->beginRod();
	  rod_iter != scan_data_iter->second->endRod();
	  ++rod_iter) {

	if (rod_iter->second->scanStatus() >= ScanIsStatus::kSuccess) {

	  Lock rod_data_server_list_lock(rod_iter->second->dataServerMutex() );
	  for (std::map< SerialNumber_t, AnalysisData_t >::const_iterator analysis_iter = rod_iter->second->beginDataServer();
	       analysis_iter != rod_iter->second->endDataServer();
	       ++analysis_iter) {
	    
	    if (!analysis_iter->second.hasData() && rod_iter->second->scanStatus() >= ScanIsStatus::kSuccess) {
	      return true;
	    }
	  }
	}
      }
    }
    return false;
  }

  bool MasterDataServer::cleanup()
  {
    Lock lock(m_scanDataMutex);
    std::map<SerialNumber_t, std::shared_ptr<ScanData_t> >::iterator scan_data_iter = m_scanData.begin();
    bool end = (scan_data_iter == m_scanData.end() );

    for(;!end;) {
      
      std::map<SerialNumber_t, std::shared_ptr<ScanData_t> >::iterator  current_scan_data_iter = scan_data_iter++;
      end = (scan_data_iter == m_scanData.end() );

      if (current_scan_data_iter->second->empty()) {
	unsubscribe(current_scan_data_iter->first);
	m_scanData.erase(current_scan_data_iter);
      }
      
    }
    return true;
  }

}
