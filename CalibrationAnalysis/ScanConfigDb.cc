#include "ScanConfigDb.hh"
#include "PixDbScanConfigDb.hh"

namespace CAN {

  IScanConfigDb *ScanConfigDb::s_instance = NULL;

  IScanConfigDb *ScanConfigDb::instance() {
    if (!s_instance) {
      s_instance = new PixDbScanConfigDb();
    }
    return s_instance;
  }

  /** Destroy an existing instance of the scan config db interface.
   */
  void ScanConfigDb::deepClenup() {
    if (s_instance) {
      delete s_instance;
      s_instance=NULL;
    }
  }

}
