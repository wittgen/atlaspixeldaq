#ifndef _CAN_MasterDataServerCT_hh_
#define _CAN_MasterDataServerCT_hh_

#include "IMasterDataServer.hh"
#include "IExecutionUnit.hh"
#include "exception.hh"
//#include <mrs/message.h>

#include <ConfigWrapper/Connectivity.h>

#include "MetaDataService.hh"
#include "ScanInfo_t.hh"

#include "IsInfoReceiverThread.hh"
#include "IsDictionary.hh"
#include "ScanIsStatus.hh"

#include "HistoArray.hh"

#include "DataServer.hh"

#include <map>
#include <deque>

#include "Lock.hh"
#include "Flag.hh"

#include "exception.hh"

#include <time.h>

namespace PixLib{
  class PixMessages;
}

namespace CAN {
  
  class DataServer;
  class IHistoReceiver;

  class MasterDataServerCT : public IMasterDataServer, public omni_thread
  {
  public:
    typedef std::deque< std::pair< PixA::RodLocationBase, std::pair< SerialNumber_t, std::shared_ptr<IDataServer> > > > DataServerList_t;
    friend class DataServer;

  protected:

    class AnalysisData_t {
    public:
      AnalysisData_t() : m_hasData(false) {}
      AnalysisData_t(std::shared_ptr<IDataServer> &data_server) : m_dataServer(data_server), m_hasData(false) {}

      std::shared_ptr<IDataServer> &dataServer() { return m_dataServer; }
      bool hasData() const                         { return m_hasData; }
      void setHasData()                            { m_hasData=true; }

    private:
      std::shared_ptr<IDataServer> m_dataServer;
      bool m_hasData;
    };

    class RodData_t {
    public:
      RodData_t() : m_scanStatus(ScanIsStatus::kUnknown), m_creationTime( time(NULL) ), m_hasData(false) {}

      void addDataServer( std::pair<SerialNumber_t, std::shared_ptr<IDataServer> > &data_server)
      {
	Lock lock(m_dataServerMutex);
	std::pair< std::map< SerialNumber_t, AnalysisData_t >::iterator, bool>
	  insert_data_server_ret = m_dataServer.insert( std::make_pair(data_server.first, AnalysisData_t(data_server.second)) );

	// there should be only one data server with the same rod name and the same analysis serial number
	assert ( insert_data_server_ret.second );
      }

      std::map< SerialNumber_t, AnalysisData_t >::iterator       beginDataServer()     { return m_dataServer.begin(); }
      std::map< SerialNumber_t, AnalysisData_t >::const_iterator endDataServer() const { return m_dataServer.end(); }

      time_t creationTime() const { return m_creationTime; }

      ScanIsStatus::EStatus   scanStatus() const {return m_scanStatus; }

      /** Update the scan status and return true if the scan has finished now.
       * @return return true if the scan was not finished before but is now.
       */
      bool updateScanStatus();

      ISInfoString &isScanStatusString() { return m_infoScanStatusString; }

      Mutex &dataServerMutex() { return m_dataServerMutex; }

      bool hasData() const { return m_hasData; }

      void setHasData()    { m_hasData=true; }

    protected:
      Mutex                                      m_dataServerMutex;
      ISInfoString                               m_infoScanStatusString;
      ScanIsStatus::EStatus                      m_scanStatus;
      std::map< SerialNumber_t, AnalysisData_t > m_dataServer;
      time_t                                     m_creationTime;

      bool m_hasData;
    };

    class ScanData_t
    {
      friend class MasterDataServerCT;

    public:
      ScanData_t( MasterDataServerCT *the_master ) : m_master(the_master) {}

      std::map< PixA::RodLocationBase, std::shared_ptr<RodData_t> >::const_iterator findRod(PixA::RodLocationBase &rod_location) const
      { Lock lock(m_rodDataMutex); return m_rodData.find(rod_location); }

      std::map< PixA::RodLocationBase, std::shared_ptr<RodData_t> >::iterator findRod(PixA::RodLocationBase &rod_location)
      { Lock lock(m_rodDataMutex); return m_rodData.find(rod_location); }

      std::map< PixA::RodLocationBase, std::shared_ptr<RodData_t> >::iterator  beginRod()            { return m_rodData.begin(); }

      std::map< PixA::RodLocationBase, std::shared_ptr<RodData_t> >::const_iterator endRod() const   { return m_rodData.end(); }

      void erase(std::map< PixA::RodLocationBase, std::shared_ptr<RodData_t> >::iterator rod_iter)
      {
	Lock lock(m_rodDataMutex);

	// paranoia check
	for (std::map< SerialNumber_t, AnalysisData_t >::const_iterator  analysis_iter = rod_iter->second->beginDataServer();
	     analysis_iter != rod_iter->second->endDataServer();
	     analysis_iter++) {
	  assert ( analysis_iter->second.hasData());
	}
	assert( rod_iter->second->hasData() );

	m_rodData.erase(rod_iter); 
	return;
      }

      /** Add a data server to the new data server list.
       * Will try to lock @ref newDataServerMutex.
       */
      void addNewDataServer(const PixA::RodLocationBase &rod_location,
			    SerialNumber_t analysis_serial_number,
			    const std::shared_ptr<IDataServer> &data_server)
      {
	assert( data_server.get() );
        assert( dynamic_cast<DataServer *>(data_server.get()) );
	Lock lock(m_newDataServerListMutex);
	m_newDataServerList.push_back( std::make_pair(rod_location, std::make_pair(analysis_serial_number,data_server ) ));
      }

      Mutex &newDataServerMutex() { return m_newDataServerListMutex; }
      Mutex &rodDataMutex()       { return m_rodDataMutex; }

      DataServerList_t::iterator beginNewDataServer()           { return m_newDataServerList.begin(); }

      DataServerList_t::const_iterator endNewDataServer() const { return m_newDataServerList.end(); }

      /** Move a data server from the list of new data servers to the per rod data container.
       * @param new_data_server_iter an iterator pointing to a new data server which should be moved to the per ROD data list.
       * The iterator will be erased from the new data server list.
       * Will lock @ref newDataServerMutex and  @rodDataMutex.
       */
      std::shared_ptr<RodData_t> insertDataServer(const DataServerList_t::iterator &new_data_server_iter) 
      {
	std::shared_ptr<RodData_t> new_rod_data;
	std::map< PixA::RodLocationBase, std::shared_ptr<RodData_t> >::iterator rod_data_iter;
	{
	  Lock lock(m_rodDataMutex);
	  rod_data_iter = m_rodData.find(new_data_server_iter->first);
	  if ( rod_data_iter == m_rodData.end() ) {
	    std::pair< std::map< PixA::RodLocationBase, std::shared_ptr<RodData_t> >::iterator, bool >
	      ret =  m_rodData.insert( std::make_pair(new_data_server_iter->first, std::shared_ptr<RodData_t>(new RodData_t())) );
	    if (!ret.second) {
	      // cannot do anything about this.
// 	      throw MRSException("MDSRV_internal_error",
// 				 MRS_FATAL,
// 				 "MasterDataServerCT::ScanData_t::insertDataServer",
// 				 new_data_server_iter->first,
// 				 "Failed to add data structure for ROD.");
	      throw MRSException(PixLib::PixMessages::FATAL,
				 "MasterDataServerCT::ScanData_t::insertDataServer",
				 new_data_server_iter->first,
				 "Failed to add data structure for ROD.");
	    }
	    rod_data_iter = ret.first;
	    new_rod_data = rod_data_iter->second;
	  }
	}

	rod_data_iter->second->addDataServer( new_data_server_iter->second );
	Lock lock(m_newDataServerListMutex);
	m_newDataServerList.erase(new_data_server_iter);

	return new_rod_data;
      }

      /** Return true if there are no data servers attached.
       * Will try to lock @ref rodDataMutex and @ref newDataServerMutex.
       */
      bool empty() const
      {
	Lock rod_data_lock( m_rodDataMutex );
	Lock new_data_server_lock( m_newDataServerListMutex );
	return m_rodData.empty() && m_newDataServerList.empty();
      }

      /** Return true if there is new data associated to this scan data container.
       * Will try to lock @ref newDataServerMutex.
       */
      bool hasNewData() const
      {
	Lock new_data_server_lock( m_newDataServerListMutex );
	return !m_newDataServerList.empty();
      }


      void statusChanged(ISCallbackInfo *info);

    protected:
      static ISInfoString      s_test;

      MasterDataServerCT *master() { return m_master; }

    private:
      MasterDataServerCT *m_master;
      mutable Mutex   m_newDataServerListMutex;
      mutable Mutex   m_rodDataMutex;
      DataServerList_t m_newDataServerList;
      std::map< PixA::RodLocationBase, std::shared_ptr<RodData_t> > m_rodData;

      InfoPtr_t<PixLib::ScanInfo_t> m_scanInfo;
    };

  public:
    //    MasterDataServerCT(IPCPartition &mrs_partition, std::string path, std::string file);
    MasterDataServerCT(std::string path, std::string file);

    /** Create data server which will serve data for exactly one analysis serial number and one ROD.
     * @param analysis_serial_number the serial number of the analysis.
     * @param rod_conn_name the connectivity name of the ROD.
     * @param scan_serial_number the serial number of the scan which is to be analysed
     * @param scan_info the scan meta data.
     * @param conn a reference of the connectivity 
     * The creator needs a reference of the connectivity because the data server also serves the connectivity 
     * to the analysis.
     * The master data will not keep any reference of the newly created data server. To make the
     * data server known it has to be handed back by calling @ref addDataServer once the data
     * server is fully setup i.e. all requests have been collected additional parameters have been
     * set, a reference of the data server has been passed to the associated joblett.
     * @sa addDataServer
    */
    std::shared_ptr<IDataServer> createDataServer(SerialNumber_t analysis_serial_number,
						    const PixA::RodLocationBase &rod_conn_name,
						    SerialNumber_t scan_serial_number,
						    const InfoPtr_t<PixLib::ScanInfo_t> &scan_info,
						    const SerialNumber_t src_analysis_serial_number,
						    PixA::ConnectivityRef &conn);

    /** Make a fully setup data server known to the master data server.
     * The master data server will start to fulfil the requests. Once
     * All the requests have been fulfilled the execution unit will signaled
     */
    void addDataServer(SerialNumber_t analysis_serial_number, const PixA::RodLocationBase &rod_conn_name, const std::shared_ptr<IDataServer>  &data_server );

    void start(IExecutionUnit &execution_unit);
    
    void reset() {}

    void initiateShutdown() { m_shutdown=true; m_dataRequest.setFlag(); }

    void shutdownWait() {
      if (state()!=omni_thread::STATE_RUNNING) {
      m_exit.wait();
      while (state()!=omni_thread::STATE_TERMINATED) {
	usleep(1000);
      }
    }
    }

    void setDataRequest() { m_dataRequest.setFlag(); }

  protected:
    void *run_undetached(void *);

    //ISInfoReceiver   &isReceiver() { return m_isListener.receiver(); }

    /** To be called if a scan has terminated.
     */
    void dataAvailable() { m_dataRequest.setFlag(); }

  private:
    /** Listen for scan status changes of the given scan.
     */
    void subscribe(SerialNumber_t scan_serial_number);

    /** Listen for scan status changes of the given scan.
     */
    void unsubscribe(SerialNumber_t scan_serial_number);


    Flag  m_dataRequest;
    Flag  m_exit;
    bool  m_shutdown;

    Mutex m_scanDataMutex;
    std::map<SerialNumber_t, std::shared_ptr<ScanData_t> > m_scanData;


    IExecutionUnit *m_executionUnit;
    //    std::auto_ptr<MRSStream> m_out;
    //IsInfoReceiverThread     m_isListener;
    //IPCPartition            *m_scanPartition;
    std::string              m_isServerName;
    //std::auto_ptr<IHistoReceiver> m_histoReceiver;

    std::string m_path;
    std::string m_file;
  };

}

#endif
