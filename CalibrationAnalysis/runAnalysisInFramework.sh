#!/bin/bash

E_WRONG_ARGS=65
parameters="path CTfile type"
knownTypes="BOC, THR, DIGI, ANALOG, TOT"

if [ $# -ne 3 ]
then
  echo "Usage: `basename $0` $parameters"
  echo "       Known types are: $knownTypes"  
  # `basename $0` is the script's filename.
  exit $E_WRONG_ARGS
fi

#rm /tmp/$USER/.$USER.is_test_pids
sh test.sh start
#What this does:
# Starting of the ipc root server
# # ipc_server #CLA:disabled since only one common ipc_server is used (and should already be running)
#
# and creation of a particular partition
# ipc_server -p $partition_name 
#
# and starting of the mrs server in the partition
# ( the partition in which mrs is started has to exist.)
# mrs_server -p $partition_name
#
# and starting of an IS server in the partition with a certain name
# ( the partition in which IS is started has to exist.)
# is_server -n $is_name -p $partition_name
#
# Finally, the serial number service is started. It hands out
# new serial numbers for scans and analyses.
# Also for the serial number service, the partition has to exist.


# Starting MRS receiver for all messages 
mrs_receiver -p ${USER}_test
#mrs_receiver -p ${USER}_test > mrs.out &

# MRS receiver for FATAL messages
# mrs_receiver -p ${USER}_test -c FATAL

# MRS receiver for messages in specific modules
#mrs_receiver -p ${USER}_test -c ANAEXEC_start_analysis

# Starting IS to get informed about the analysis state for each ROD (tool available after "make check")
# (-p partition, -n is-server-name) 
./test_isReader -p ${USER}_test -n CAN

#
# Startup analysis infrastructure
#
# start up the Scheduler as a daemon in partition ${USER}_test
./manitu -d ${USER}_test 

# start a Worker which will use CPUs in parallel on the the same machine which will search the scheduler in partition ${USER}_test
./startWorker -d ${USER}_test -n 2 -p $1 -f $2
#./startWorker -d ${USER}_test -n 1 -p . -f "SCAN_28AUG07_C6_E50_V12.root"

# ping worker : worker should send an alive message to MRS in partition 
#./kill_manitu -a ${USER}_test

# shutdown worker and scheduler in partition test
#./kill_manitu ${USER}_test

#
# Run analyses
#

# create some dummy scan meta data (tool available after make check)
# scan with serial number 1
#./test_scanMetaDataWriter -p ${USER}_test -t C34_ISP_v32 CT-C34ISP -n DIGITAL_TEST
./test_scanMetaDataWriter -p ${USER}_test -t BiStave_5 DataTaking-4000e-ST -n THRESHOLD
# the tags must be valid connectivity tags 
# You get the list of tags with ${PIX_LIB}/Examples/ConnDbCreate -lt 

# Create fake analysis meta data with
./test_analysisMetaDataWriter -p ${USER}_test -a THRESHOLDanalysis test 0 -c TestClassification test 0 -x TestPostProcessing2 test 0 -s 1
./test_analysisMetaDataWriter -p ${USER}_test -a BOCanalysis test 0 -c TestClassification test 0 -x TestPostProcessing2 test 0 -s 1
./test_analysisMetaDataWriter -p ${USER}_test -a DIGITALanalysis test 0 -c TestClassification test 0 -x TestPostProcessing2 test 0 -s 1
./test_analysisMetaDataWriter -p ${USER}_test -a ANALOGanalysis test 0 -c TestClassification test 0 -x TestPostProcessing2 test 0 -s 1
./test_analysisMetaDataWriter -p ${USER}_test -a TOTanalysis test 0 -c TestClassification test 0 -x TestPostProcessing2 test 0 -s 1

# scan with serial number 2
#./test_scanMetaDataWriter -p test -t C34_ISP_v32 CT-C34ISP -n ANALOG_TEST
# command will print out the scan serial number under which the fake scan is filed.

# submit an analysis of scan 1 to scheduler in partition test (-p)
# Analysis plugin (-a)  : TestAnalysis with configuration tag test and revision 0
# Classification plugin (-c)
# Post-processing plugin (-x)
# ....
# tags and revisions have no impact since there is no configuration database 

#To create a temp configuration file:
#configWriter/configWriter -a TestAnalysis TEST 1234
#configWriter/configWriter -a TestClassification TagClass 12
#configWriter/configWriter -a TestPostProcessing TagPost 12

#for root files:
#configWriter/configWriter -a THRESHOLDanalysis root 12
#configWriter/configWriter -a TestClassification root 12
#configWriter/configWriter -a TestPostProcessing root 12

#To test plugin:
#./test_analysisFactory -o TestAnalysis test 0  -s 1

if [ "$3" = "DIGI" ]
then 
  echo "digital"
#  ./test_submitAnalysis -p ${USER}_test -a AutomaticDIGITALanalysis test 0 -c DIGITALclassification test 0 -x DIGITALpostProcessing test 0 -s 1 
  ./test_submitAnalysis -p ${USER}_test -a AutomaticDIGITALanalysis test 0 -c TestClassification test 0 -x TestPostProcessing test 0 -s 1 
elif [ "$3" = "THR" ]
then
  echo "threshold"
#  ./test_submitAnalysis -p ${USER}_test -a AutomaticTHRESHOLDAnalysis test 0 -c THRESHOLDclassification test 0 -x THRESHOLDPostProcessing test 0 -s 1 
#  ./test_submitAnalysis -p ${USER}_test -a AutomaticTHRESHOLDAnalysis test 0 -c TestClassification test 0 -x TestPostProcessing test 0 -s 1 
  ./test_submitAnalysis -p ${USER}_test -a THRESHOLDanalysis TAG1 15 -c TestClassification test 0 -x TestPostProcessing test 0 -s 1 
#sqlfiles: ./test_submitAnalysis -p ${USER}_test -a THRESHOLDanalysis TAG1 16 -c TestClassification TagClass 12 -x TestPostProcessing TagPost 12 -s 1
#root:./test_submitAnalysis -p ${USER}_test -a THRESHOLDanalysis root 12 -c TestClassification root 12 -x TestPostProcessing root 12 -s 1

elif [ "$3" = "BOC" ]
then
    ./test_submitAnalysis -p ${USER}_test -a AutomaticBOCAnalysis test 0 -c TestClassification test 0 -x TestPostProcessing test 0 -s 1 
elif [ "$3" = "TOT" ]
then
    ./test_submitAnalysis -p ${USER}_test -a AutomaticTOTanalysis test 0 -c TestClassification test 0 -x TestPostProcessing test 0 -s 1 
elif [ "$3" = "ANALOG" ]
then
    ./test_submitAnalysis -p ${USER}_test -a AutomaticANALOGanalysis test 0 -c TestClassification test 0 -x TestPostProcessing test 0 -s 1 
fi

#comment: scheduler seems to crash if no scan exists for the given scan serial number.


#const PixA::Pp0LocationBase pp0_location(""); //this will go through all pp0s, and getHistograms will give a list with all modules from all the pp0s
#////const PixA::Pp0LocationBase pp0_location("L2_B25_S2_C6");

#ToDo: ana.requestHistograms( &dataServer, pp0_location);

#ToDo:   check if file _fds exists


