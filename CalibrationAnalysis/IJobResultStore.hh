#ifndef _CAN_IJobResultStore_hh_
#define _CAN_IJobResultStore_hh_

namespace CAN {
  class IJobResultStore
  {
  public:
    virtual ~IJobResultStore() {}
    virtual store(JobLett_t *results) {}
  }
}

#endif
