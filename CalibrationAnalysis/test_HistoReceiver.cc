#include "RootFileHistoReceiver.hh"
#include "HistoReceiver.hh"
#include <iostream>
#include "HistoArray.hh"
#include <DataContainer/HistoInfo_t.h>
#include <PixMetaException.hh>
#include "exception.hh"

#include <ipc/partition.h>
#include <ipc/core.h>

#include "TH1.h"

int main(int argc, char **argv)
{
  CAN::SerialNumber_t serial_number=0;
  std::string rod_name;
  std::string  conn_name;
  std::string histo_name;
  std::string partition_name;
  std::string oh_server_name;
  bool error=false;
  bool verbose=false;
  Double_t sum=0;

  std::auto_ptr<CAN::IHistoReceiver> histo_receiver(new CAN::RootFileHistoReceiver);
  std::auto_ptr<IPCPartition> partition;
  try {
    IPCCore::init(argc, argv);

    for (int arg_i=1; arg_i<argc; arg_i++) {
      if (strcmp(argv[arg_i],"-s")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	serial_number = atoi(argv[++arg_i]);
      }
      else if (strcmp(argv[arg_i],"--oh-partition")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	partition_name = argv[++arg_i];
      }
      else if ( (strcmp(argv[arg_i],"--oh-name")==0 || strcmp(argv[arg_i],"-o")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	oh_server_name = argv[++arg_i];
	if (!partition_name.empty()) {
	  partition = std::auto_ptr<IPCPartition>  (new IPCPartition(partition_name.c_str()));
	  histo_receiver = std::auto_ptr<CAN::IHistoReceiver>(new CAN::HistoReceiver(*(partition.get()),
										     oh_server_name));
	}
      }
      else if (strcmp(argv[arg_i],"-n")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	histo_name = argv[++arg_i];
      }
      else if (strcmp(argv[arg_i],"-r")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	rod_name = argv[++arg_i];
      }
      else if (strcmp(argv[arg_i],"-m")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	conn_name = argv[++arg_i] ;

	CAN::TopLevelUpdateHistoArray arr;
	histo_receiver->getHistograms(serial_number, rod_name, conn_name, histo_name, arr);
	const HistoInfo_t &info( arr.info() );
	if (verbose) {
	  std::cout << "INFO [" << argv[0] << "] info : ";
	  for (unsigned int i=0; i<HistoInfo_t::maxLevels()-1; i++) {
	    if (info.maxIndex(i)<=0) break;
	    std::cout << info.maxIndex(i) << " ";
	  }
	  std::cout << info.minHistoIndex() << " - " << info.maxHistoIndex();
	}
	std::vector<unsigned int > index;
	info.makeDefaultIndexList(index);
	const TH1 *a_histo = arr.histo(index);
	if (a_histo) {
	  if (verbose) {
	    std::cout << "INFO [" << argv[0] << "] name = " << a_histo->GetName() << std::endl;
	  }
	  sum += a_histo->Integral(0,a_histo->GetNbinsX()+1);
	}

      }
      else if (strcmp(argv[arg_i],"-v")==0 ) {
	verbose=true;
	histo_receiver = std::auto_ptr<CAN::IHistoReceiver>(new CAN::RootFileHistoReceiver(true) );	
      }
      else {
	std::cout << "ERROR ["<< argv[0] << ":main] unhandled argument " << arg_i << " : " << argv[arg_i]<< "." << std::endl;
	error=true;
	break;
      }
    }

    if (histo_name.empty() || conn_name.empty() || serial_number==0 || error) {
      std::cout << "usage: " << argv[0] << "-n histo-name -r ROD-connectivity-name -m module-connectivity-name -s scan-serial-number" << std::endl;
      return -1;
    }

  }
  catch (CAN::PixMetaException &err) {
    std::cerr << "ERROR [" << argv[0] << ":main] Caught PixMetaException " << err.what() << std::endl;
  }
  catch (CAN::MRSException &err) {
    std::cerr << "ERROR [" << argv[0] << ":main] Caught exception " << err.getDescriptor() << std::endl;
  }
  catch (std::exception &err) {
    std::cerr << "ERROR [" << argv[0] << ":main] Caught standard exception " << err.what() << std::endl;
  }
  std::cout << "INFO [" << argv[0] << "] result = " << sum << std::endl;
  return 0;
}

