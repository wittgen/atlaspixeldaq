#include "AnalysisMonitor.hh"
#include "Scheduler_impl.hh"
#include "isUtil.hh"
//#include "sendMessage.hh"
#include "PixUtilities/PixMessages.h"
#include <MetaDataService.hh>

namespace CAN {


  const std::string AnalysisMonitor::s_componentName("ANAMON");

  //  AnalysisMonitor::AnalysisMonitor(IPCPartition &ipc_partition, IPCPartition &mrs_partition, const std::string &is_server_name, Scheduler_impl &scheduler)
  AnalysisMonitor::AnalysisMonitor(IPCPartition &ipc_partition, const std::string &is_server_name, Scheduler_impl &scheduler)
    : m_partition(&ipc_partition),
      m_isServerName(is_server_name),
      m_isListener(ipc_partition),
      m_isDictionary(ipc_partition),
      m_scheduler(&scheduler),
      //      m_out(new MRSStream(mrs_partition)),
      m_updateFreq(10 /*sec*/),
      m_timeOutSubmissionMax(60 /*sec*/),   //depends on the time needed to load the connectivity
      //      m_timeOutSubmissionMin(50 /*sec*/),   //dito.
      m_timeOutStart(3*60/*sec*/),          //depends on what ?
      m_timeOutRunningAndArchivingMax( 30*60 /*sec*/),  //depends on maximum time required for an analysis.
      //      m_timeOutRunningMin( 13*60 /*sec*/),  //dito.
      m_abortTimeMax( 30 /*sec*/),          //send abort again if no status change happens within this time span.
      m_keepJobInfoTime(60),                //only relevant if the detailed information should stay in IS.
      m_nMissingResponsesMax(2),
      m_nResubmissionsMax(10),
      m_logLevel(kWarnLog),
      m_abort(false),
      m_reset(false)
  {    start_undetached(); m_isListener.listen(); }


  // helper class to store information for resubmission of failed / un processed analysis jobs.
  class ResubmissionList_t
  {
  public:
    ResubmissionList_t(std::shared_ptr<AnalysisJobList_t> &job_list) : m_jobList(job_list), m_round(0) {}

    //    void addRodName( unsigned int expected_load, std::string &rod_name)         { m_rodList.push_back(std::make_pair(expected_load, rod_name)); }

    //    std::shared_ptr<AnalysisJobList_t> &jobList()             { return m_jobList; }
    const std::shared_ptr<AnalysisJobList_t> &jobList() const { return m_jobList; }

    std::vector< std::pair<unsigned int, std::string> > &rodList()             { return m_rodList; }
    const std::vector< std::pair<unsigned int, std::string> > &rodList() const { return m_rodList; }

    void incRound()            { m_round++; }
    unsigned int round() const { return m_round; }

  private:
    std::shared_ptr<AnalysisJobList_t> m_jobList;
    std::vector< std::pair<unsigned int, std::string> >  m_rodList;
    unsigned int                         m_round;
  };


  void AnalysisMonitor::monitor()
  {
    if (m_logLevel>=kInfoLog) {
//      Lock mrs_lock(m_mrsMutex);
//       sendMessage(*m_out,"ANAMON_up",s_componentName,MRS_INFORMATION, "CAN::AnalysisMonitor::monitor", 
// 		  "Analysis monitoring is running."  );
      ers::info(PixLib::pix::daq (ERS_HERE, "CAN::AnalysisMonitor::monitor", "Analysis monitoring is running."));
      
    }
    time_t last_worker_ping=time(NULL);

    std::map<SerialNumber_t, ResubmissionList_t > resubmission_list;

    while ( !m_abort ) {

      if ( m_flag.timedwait( m_updateFreq, m_reset) ) {
	// something has definitely changed.
      }
      if (m_reset) {
	resubmission_list.clear();
	_reset();
	continue;
      }

      time_t current_time = time(NULL);

      if (static_cast<unsigned long>(last_worker_ping - current_time) > m_updateFreq) {
	m_scheduler->testWorker(m_nMissingResponsesMax);
      }

      {
      Lock queue_lock(m_jobQueueMutex);

      bool resubmit = findCrashedWorker(current_time);

      bool end=(m_queuedJobs.begin()==m_queuedJobs.end());
      // determine global status, resubmit jobs if needed, cleanup job lists
      for (JobQueue_t::iterator list_loop_iter = m_queuedJobs.begin();
	   !end;
	   ) {
	JobQueue_t::iterator list_iter = list_loop_iter++;
	end = (list_loop_iter == m_queuedJobs.end());

	AnalysisStatus::EStatus old_global_status = list_iter->second->globalStatus();
	AnalysisStatus::EStatus new_global_status = determineGlobalStatus(*list_iter);

	if (resubmit && !m_abort) {

	  // get resubmission list for this analysis
	  std::map<SerialNumber_t, ResubmissionList_t>::iterator resubmission_list_iter = resubmission_list.find(list_iter->first);
	  if (resubmission_list_iter == resubmission_list.end()) {
	    std::pair<std::map<SerialNumber_t, ResubmissionList_t>::iterator, bool>
	      ret = resubmission_list.insert(std::make_pair(list_iter->first, ResubmissionList_t(list_iter->second)));
	    if (ret.second) {
	      resubmission_list_iter = ret.first;
	    }
	  }

	  // Now there should be a resubmission list for the analysis but we check nevertheless
	  if (resubmission_list_iter != resubmission_list.end()) {
	    std::vector< std::pair<unsigned int, std::string> > &a_rod_list = resubmission_list_iter->second.rodList();

	    for (AnalysisJobList_t::iterator job_loop_iter = list_iter->second->begin();
		 job_loop_iter != list_iter->second->end();
		 ) {
	      AnalysisJobList_t::iterator   job_iter = job_loop_iter++;

	      // ignore all jobs which are already marked for resubmission 
	      if (job_iter->second.markedForResubmission()) continue;

	      // ... or which are done 
	      if (job_iter->second.status()<= AnalysisStatus::kRunning) {
		if (job_iter->second.workerInfo().toBeRemoved()) {

		    // assume that the running jobs crashed the worker. So, they are not resubmitted but marked as
		    // failed.
		    //		    if (   job_iter->second.status() != AnalysisStatus::kRunning
		    //                  || current_time <= job_iter->second.lastStatusChange() + m_timeOutRunningMin  ) {

		    if (job_iter->second.status() != AnalysisStatus::kRunning ) {

		      // 		      std::stringstream message;
		      // 		      message << "Consider for resubmission : " << list_iter->first << " / " << job_iter->first << " status = ";
		      // 		      if (job_iter->second.status() <= AnalysisStatus::kAborted) {
		      // 			message << AnalysisStatus::name( job_iter->second.status() );
		      // 		      }
		      // 		      else {
		      // 			message << job_iter->second.status();
		      // 		      }
		      // 		      message << " changed  " << (current_time - list_iter->second->statusChangeTime())  << " sec ago.";

		      // 		      {
		      // 			Lock mrs_lock(m_mrsMutex);
		      // 			sendMessage(*m_out,"ANAMON_resubmission_prepare",s_componentName,MRS_DIAGNOSTIC, "CAN::AnalysisMonitor::monitor", message.str());
		      // 		      }

		      // publish global status
		      // the scheduler should have created the dictionary
		      {
			std::string is_status_name( AnalysisStatus::makeRegexOrVarName( AnalysisStatus::makeIsHeader(m_isServerName, list_iter->first), 
											job_iter->first,
											AnalysisStatus::varName()));
			job_iter->second.setStatus(AnalysisStatus::kUnknown);
			Lock dictionary_lock(m_isDictionary.mutex());
			updateIsValue(m_isDictionary.dictionary(),is_status_name, job_iter->second.isStatus());
		      }
		      a_rod_list.push_back( std::make_pair( job_iter->second.expectedLoad(), job_iter->first) );
		      job_iter->second.markForResubmission();
		    }
		    else {
		      // mark running jobs as failed.
		      //		      if (job_iter->second.status() == AnalysisStatus::kRunning)
		      std::string is_status_name( AnalysisStatus::makeRegexOrVarName( AnalysisStatus::makeIsHeader(m_isServerName, list_iter->first),
										      job_iter->first,
										      AnalysisStatus::varName()));

		      job_iter->second.setStatus(AnalysisStatus::kFailure);
		      Lock dictionary_lock(m_isDictionary.mutex());
		      updateIsValue(m_isDictionary.dictionary(),is_status_name, job_iter->second.isStatus());
		      std::stringstream message;
		      message << "Aborted job  : " << list_iter->first << " / " << job_iter->first << ". Set  status to  ";
		      if (job_iter->second.status() <= AnalysisStatus::kAborted) {
			message << AnalysisStatus::name( job_iter->second.status() );
		      }
		      else {
			message << job_iter->second.status();
		      }

		      if (m_logLevel>=kDiagnosticsLog) {
			//Lock mrs_lock(m_mrsMutex);
			//			sendMessage(*m_out,"ANAMON_abort_job",s_componentName,MRS_DIAGNOSTIC, "CAN::AnalysisMonitor::monitor", message.str());
			ers::debug(PixLib::pix::daq (ERS_HERE, "CAN::AnalysisMonitor::monitor", message.str()));
		      }

		    }

		    //		    break;
		}
	      }
	    }
	  }
	  else {
	    if (m_logLevel>=kFatalLog) {
	      std::stringstream message;
	      message << "Failed to prepare list to resubmit jobs for analysis " << list_iter->first << " .";
// 	      Lock mrs_lock(m_mrsMutex);
// 	      sendMessage(*m_out,"ANAMON_resubmit_fail",s_componentName,MRS_FATAL, "CAN::AnalysisMonitor::monitor", message.str());
	      ers::fatal(PixLib::pix::daq (ERS_HERE, "CAN::AnalysisMonitor::monitor", message.str()));
	    }
	  }
	}

	//debug:	{
	//debug: 	std::stringstream message;
	//debug: 	message << " global status : old " << old_global_status << " =?= " << new_global_status << ".";
	//debug:	Lock mrs_lock(m_mrsMutex);
	//debug: 	sendMessage(*m_out,"ANAMON_global_status",s_componentName,MRS_DIAGNOSTIC, "CAN::AnalysisMonitor::monitor", message.str());
	//debug:	}

	// publish new global status in IS

	if (new_global_status > AnalysisStatus::kRunning) {
	  // Verify that there are no jobs waiting for resubmission
	  std::map<SerialNumber_t, ResubmissionList_t >::const_iterator  resubmit_iter = resubmission_list.find(list_iter->first);
	  if (resubmit_iter != resubmission_list.end() && !resubmit_iter->second.rodList().empty()) {
	    new_global_status=AnalysisStatus::kPending;

	    if (m_logLevel>=kDiagnosticsLog) {
	      std::stringstream message;
	      message << "The following jobs of analysis " << resubmit_iter->first << " wait for resubmission (round " << resubmit_iter->second.round() << ") : ";
	      for (std::vector< std::pair<unsigned int, std::string> >::const_iterator rod_iter = resubmit_iter->second.rodList().begin();
		   rod_iter != resubmit_iter->second.rodList().end();
		   rod_iter++) {
		message << rod_iter->second << " ";
	      }
	      message << ".";
// 	      Lock mrs_lock(m_mrsMutex);
// 	      sendMessage(*m_out,"ANAMON_resubmission_awaited",s_componentName,MRS_DIAGNOSTIC, "CAN::AnalysisMonitor::monitor", message.str());
	      ers::debug(PixLib::pix::daq (ERS_HERE, "CAN::AnalysisMonitor::monitor", message.str()));
	    }

	  }
	} 


	if (old_global_status != new_global_status) {

	  list_iter->second->setGlobalStatus(new_global_status);
	  if (old_global_status <= AnalysisStatus::kRunning && new_global_status>AnalysisStatus::kRunning) {
	    list_iter->second->setStatusChangeTime();
	  }
	  list_iter->second->setGlobalStatus(new_global_status);

	  // update analysis status, and analysis end time in the the meta data db.
	  if (new_global_status>AnalysisStatus::kRunning) {
	    CAN::GlobalStatus global_analysis_status=metaDataStatus(new_global_status);
	    if (global_analysis_status != CAN::kNotSet) {
	      //debug:	      {
	      //debug:		std::stringstream message;
	      //debug:		message << " Set final meta data status  : " << new_global_status << " -> " << global_analysis_status <<  ".";
	      //debug:		Lock mrs_lock(m_mrsMutex);
	      //debug:		sendMessage(*m_out,"ANAMON_final_status",s_componentName,MRS_DIAGNOSTIC, "CAN::AnalysisMonitor::monitor", message.str());
	      //debug:	      }

	      MetaDataService::instance()->setFinalAnalysisStatus( list_iter->first, global_analysis_status, &(list_iter->second->statusChangeTime())) ;
	    }

	  }
	  // publish global status
	  // the scheduler should have created the dictionary
	  {
	    std::string is_global_status_name( AnalysisStatus::makeRegexOrVarName( AnalysisStatus::makeIsHeader(m_isServerName, list_iter->first),
										   AnalysisStatus::globalVarName()));
	    Lock dictionary_lock(m_isDictionary.mutex());

	    try {
	      updateIsValue(m_isDictionary.dictionary(),is_global_status_name, list_iter->second->isGlobalStatus());
	    }
	    catch (daq::is::Exception & ex) {
	    }

	  }

	}
	else if ( old_global_status>AnalysisStatus::kRunning) {

	  // 	    std::stringstream message;
	  // 	    message << "Analysis " << list_iter->first << " finished " << (current_time - list_iter->second->statusChangeTime())  << " sec ago.";
	  // 	    {
	  // 	      Lock mrs_lock(m_mrsMutex);
	  // 	      sendMessage(*m_out,"ANAMON_cleanup",s_componentName,MRS_DIAGNOSTIC, "CAN::AnalysisMonitor::monitor", message.str());
	  // 	    }

	    if ( current_time > list_iter->second->statusChangeTime() + m_keepJobInfoTime) {
	      // erase terminated jobs after a certain amount of time.
	      if (!resubmit) {

		std::stringstream message;
		message << "Analysis " << list_iter->first << " finished with status ";
		if (list_iter->second->globalStatus()<=AnalysisStatus::kAborted) {
		  message << AnalysisStatus::name( list_iter->second->globalStatus() ) ;
		}
		else {
		  message << list_iter->second->globalStatus() ;
		}
		message	<< " " << (current_time - list_iter->second->statusChangeTime() )  << " sec ago.";

// 		message << " Stati :";
// 		for (AnalysisJobList_t::iterator job_iter = list_iter->second->begin();
// 		     job_iter != list_iter->second->end();
// 		     job_iter++) {
// 		  if (job_iter->second.status()<=AnalysisStatus::kAborted) {
// 		    message << job_iter->first << " = " << AnalysisStatus::name( job_iter->second.status() ) << std::endl;
// 		  }
// 		  else {
// 		    message << job_iter->first << " = " << job_iter->second.status() << std::endl;
// 		  }
// 		}

		if (m_logLevel>=kInfoLog) {
		  message << " Will remove job information from memory and IS.";
		  {
// 		    Lock mrs_lock(m_mrsMutex);
// 		    sendMessage(*m_out,"ANAMON_cleanup",s_componentName,MRS_INFORMATION, "CAN::AnalysisMonitor::monitor", message.str());
		    ers::info(PixLib::pix::daq (ERS_HERE, "CAN::AnalysisMonitor::monitor", message.str()));
		  }
		}

		m_queuedJobs.erase(list_iter);
	      }
	    }
	    //	  }
	}
	else if (list_iter->second->abort() && current_time-list_iter->second->statusChangeTime() > m_abortTimeMax) {
	  m_scheduler->sendAbortAnalysis( list_iter->first );
	}
      }

      }

      m_scheduler->removeWorker();

      // resubmit jobs
      if (resubmission_list.size()>0) {

	bool end=(resubmission_list.begin()==resubmission_list.end());
	for( std::map<SerialNumber_t, ResubmissionList_t >::iterator resubmit_loop_iter = resubmission_list.begin();
	     !end;
	     ) {

	  std::map<SerialNumber_t, ResubmissionList_t >::iterator resubmit_iter = resubmit_loop_iter++;
	  end= (resubmit_loop_iter==resubmission_list.end() );
	  if (resubmit_iter->second.rodList().empty() || resubmit_iter->second.jobList()->abort()) {
	    resubmission_list.erase(resubmit_iter);
	    continue;
	  }

	  if (m_logLevel>=kDiagnosticsLog) {
	    std::stringstream message;
	    message << "Resubmit jobs : ";
	    for (std::vector< std::pair<unsigned int, std::string> >::const_iterator rod_iter = resubmit_iter->second.rodList().begin();
		 rod_iter != resubmit_iter->second.rodList().end();
		 rod_iter++) {
	      message << rod_iter->second << " ";
	    }
	    message <<  "for analysis " << resubmit_iter->first << ".";
	    {
// 	      Lock mrs_lock(m_mrsMutex);
// 	      sendMessage(*m_out,"ANAMON_resubmit",s_componentName,MRS_DIAGNOSTIC, "CAN::AnalysisMonitor::monitor", message.str());
	     ers::debug(PixLib::pix::daq (ERS_HERE, "CAN::AnalysisMonitor::monitor", message.str()));
	    }
	  }

	  if (m_scheduler->spreadJobs (resubmit_iter->first, resubmit_iter->second.jobList(), resubmit_iter->second.rodList()) ) {
	    // submission was successful
	    resubmission_list.erase(resubmit_iter);
	  }
	  else {
	    // need to resbumit at least some jobs

	    resubmit_iter->second.incRound();
	    if (resubmit_iter->second.round() > m_nResubmissionsMax ) {

	      if (m_logLevel>=kFatalLog) {
		std::stringstream message;
		message << "Tried " << resubmit_iter->second.round() << " times to resubmit jobs : ";
		for (std::vector<std::pair<unsigned int, std::string> >::const_iterator rod_iter = resubmit_iter->second.rodList().begin();
		     rod_iter != resubmit_iter->second.rodList().end();
		     rod_iter++) {
		  message << rod_iter->second << " ";
		}
		message <<  "for analysis " << resubmit_iter->first << ". Will give up.";
		{
// 		  Lock mrs_lock(m_mrsMutex);
// 		  sendMessage(*m_out,"ANAMON_resubmit_failed",s_componentName,MRS_FATAL, "CAN::AnalysisMonitor::monitor", message.str());
		  ers::fatal(PixLib::pix::daq (ERS_HERE, "CAN::AnalysisMonitor::monitor", message.str()));
		}
	      }
	      resubmission_list.erase( resubmit_iter );
	    }
	    else {
	      Lock queue_lock(m_jobQueueMutex);
	      // check which jobs have been resubmitted succcessfully
	      for (AnalysisJobList_t::const_iterator job_iter = resubmit_iter->second.jobList()->begin();
		   job_iter != resubmit_iter->second.jobList()->end();
		   job_iter++) {

		// jobs which are still marked for resubmission
		// have not been submitted successfully so they have to stay in the list
		if (job_iter->second.markedForResubmission()) continue;

		std::vector<std::pair<unsigned int, std::string> > &a_rod_list = resubmit_iter->second.rodList();
		for (std::vector< std::pair<unsigned int, std::string> >::iterator rod_iter = a_rod_list.begin();
		     rod_iter != a_rod_list.end();
		     rod_iter++) {
		  if (job_iter->first == rod_iter->second ) {
		    a_rod_list.erase(rod_iter);
		    break;
		  }

		}
	      }

	      if (!resubmit_iter->second.rodList().empty() && m_logLevel>kDiagnosticsLog) {
		std::stringstream message;
		message << "Will try later to resubmit jobs : ";
		for (std::vector<std::pair<unsigned int,std::string> >::const_iterator rod_iter = resubmit_iter->second.rodList().begin();
		     rod_iter != resubmit_iter->second.rodList().end();
		     rod_iter++) {
		  message << rod_iter->second << " ";
		}
		message <<  "for analysis " << resubmit_iter->first << ".";
// 		Lock mrs_lock(m_mrsMutex);
// 		sendMessage(*m_out,"ANAMON_resubmit",s_componentName,MRS_DIAGNOSTIC, "CAN::AnalysisMonitor::monitor", message.str());
		ers::debug(PixLib::pix::daq (ERS_HERE, "CAN::AnalysisMonitor::monitor", message.str()));
	      }


	    }

	  }

	}

      }

    }


    if (m_abort) {
      abortAllRunningJobs();
    }

    if (m_logLevel>=kInfoLog) {
//       Lock mrs_lock(m_mrsMutex);
//       sendMessage(*m_out,"ANAMON_down",s_componentName,MRS_INFORMATION, "CAN::AnalysisMonitor::monitor", 
// 		  "Analysis monitoring is stopped."  );
      ers::info(PixLib::pix::daq (ERS_HERE, "CAN::AnalysisMonitor::monitor", 
		  "Analysis monitoring is stopped." ) );
    }

  }

  AnalysisStatus::EStatus AnalysisMonitor::determineGlobalStatus(const std::pair<SerialNumber_t, std::shared_ptr<AnalysisJobList_t> > &analysis_status )
  {
    AnalysisStatus::EStatus new_global_status = AnalysisStatus::kUnknown;

    // determine global status from worker status.
    // jobs to be resubmitted are considered later
    for (AnalysisJobList_t::iterator job_iter = analysis_status.second->begin();
	 job_iter != analysis_status.second->end();
	 job_iter++) {

      // ignore all jobs which are already marked for resubmission
      if (job_iter->second.markedForResubmission()) {
	new_global_status = AnalysisStatus::kPending;
	continue;
      }

      new_global_status = combinedStatus(new_global_status, job_iter->second.status() );

    }

    return new_global_status;
  }

  void *AnalysisMonitor::run_undetached(void *arg)
  {
    // thread should ne be started with arguments
    assert (arg==NULL);
    monitor();
    m_exitMonitoring.setFlag();
    return NULL;
  }

  void AnalysisMonitor::shutdownWait()
  {
    abort();

    if (state()!=omni_thread::STATE_TERMINATED) {
      if (m_logLevel>=kDiagnosticsLog) {
// 	Lock mrs_lock(m_mrsMutex);
// 	sendMessage(*m_out,"ANAMON_shutdown_wait",s_componentName, MRS_DIAGNOSTIC, "CAN::AnalysisMonitor::shutdownWait", 
// 		    "Waiting for execution processor thread to shut down.");
	ers::debug(PixLib::pix::daq (ERS_HERE, "CAN::AnalysisMonitor::shutdownWait", 
		    "Waiting for execution processor thread to shut down."));
      }
      m_exitMonitoring.wait();
      while (state()!=omni_thread::STATE_TERMINATED) {
	usleep(1000);
      }
    }
  }

    /** Reset list of monitored jobs.
     */
  void AnalysisMonitor::_reset()
  {
    Lock lock(m_jobQueueMutex);
    abortAllRunningJobs();

    m_queuedJobs.clear();

    if (m_logLevel>=kInfoLog) {
//       Lock mrs_lock(m_mrsMutex);
//       sendMessage(*m_out,"ANAMON_reset",s_componentName,MRS_INFORMATION, "CAN::AnalysisMonitor::_reset",
// 		  "Marked all running analyses as aborted and stopped monitoring them.");
      ers::info(PixLib::pix::daq (ERS_HERE, "CAN::AnalysisMonitor::_reset",
		  "Marked all running analyses as aborted and stopped monitoring them."));
    }
    m_reset=false;
  }

  void AnalysisMonitor::reset()
  {
    m_reset=true;
    m_flag.setFlag();
  }

  void AnalysisMonitor::abortAllRunningJobs()
  {
    // set the status of all jobs which are not done yet to aborted
    // and update global status
    for (JobQueue_t::iterator list_loop_iter = m_queuedJobs.begin();
	 list_loop_iter != m_queuedJobs.end();
	 ) {
      JobQueue_t::iterator list_iter = list_loop_iter++;
      abortAllRunningJobs( *list_iter );
    }
  }

  void AnalysisMonitor::abortAllRunningJobs( std::pair< const SerialNumber_t, std::shared_ptr<AnalysisJobList_t> > &analysis_job_list)
  {
    try {
      // set but not used
      //    bool aborted_something=false;

    std::stringstream message;
    message << "Jobletts :";

    AnalysisStatus::EStatus new_global_status = AnalysisStatus::kUnknown;

    for (AnalysisJobList_t::iterator job_iter = analysis_job_list.second->begin();
	 job_iter != analysis_job_list.second->end();
	 job_iter++) {

      message << job_iter->first  << "= " << AnalysisStatus::name ( job_iter->second.status())  << " ";
      if (job_iter->second.status() < AnalysisStatus::kRunning) {
	job_iter->second.setStatus(AnalysisStatus::kAborted);
	job_iter->second.updateIsStatus();
	//	aborted_something=true;
      }
      new_global_status = combinedStatus(new_global_status, job_iter->second.status() );

    }

    if (m_logLevel>=kDiagnosticsLog) {
//       Lock mrs_lock(m_mrsMutex);
//       sendMessage(*m_out,"ANAMON_reset",s_componentName,MRS_DIAGNOSTIC, "CAN::AnalysisMonitor::abortAllRunningJobs",
// 		  message.str());
      ers::debug(PixLib::pix::daq (ERS_HERE, "CAN::AnalysisMonitor::abortAllRunningJobs",
		  message.str()));
    }

    if (new_global_status != analysis_job_list.second->globalStatus()) {

      analysis_job_list.second->setGlobalStatus(new_global_status);
      analysis_job_list.second->setStatusChangeTime();

      CAN::GlobalStatus global_analysis_status=metaDataStatus(new_global_status);

      MetaDataService::instance()->setFinalAnalysisStatus( analysis_job_list.first, global_analysis_status, &(analysis_job_list.second->statusChangeTime())) ;

      // publish global status
      // the scheduler should have created the dictionary
      {
	std::string is_global_status_name( AnalysisStatus::makeRegexOrVarName( AnalysisStatus::makeIsHeader(m_isServerName, analysis_job_list.first),
									       AnalysisStatus::globalVarName()));
	Lock dictionary_lock(m_isDictionary.mutex());
	updateIsValue(m_isDictionary.dictionary(),is_global_status_name, analysis_job_list.second->isGlobalStatus());
      }
    }
    }
    catch (daq::is::Exception & ex) {
    }
  }

  void AnalysisMonitor::setGlobalStatusToAborted(SerialNumber_t analysis_serial_number)
  {

    try {
    ISInfoString global_status;
    global_status.setValue(AnalysisStatus::name(AnalysisStatus::kAborted));

    std::string is_global_status_name( AnalysisStatus::makeRegexOrVarName( AnalysisStatus::makeIsHeader(m_isServerName, analysis_serial_number),
									   AnalysisStatus::globalVarName()));
    Lock dictionary_lock(m_isDictionary.mutex());
    ISType type( global_status.type() );
    setIsValue(m_isDictionary.dictionary(),type, is_global_status_name, global_status);
    }
    catch (daq::is::Exception & ex) {
    }

  }


  bool AnalysisMonitor::findCrashedWorker(const time_t &current_time)
  {
    bool resubmit=false;

    // search for crashed / hanging worker
    // The worker need to be restarted and all the jobs which are assigned to then need to be resubmitted.
    for (JobQueue_t::iterator list_loop_iter = m_queuedJobs.begin();
	 list_loop_iter != m_queuedJobs.end();
	 ) {
      JobQueue_t::iterator list_iter = list_loop_iter++;

      for (AnalysisJobList_t::iterator job_iter = list_iter->second->begin();
	   job_iter != list_iter->second->end();
	   job_iter++) {

	// ignore all jobs which are already marked for resubmission
	if (job_iter->second.markedForResubmission()) continue;

	bool worker_has_crashed=false;

	// Reasons to restart a worker
	// 1. the job status should immediately (< sec. ) change from submitted to pending where pending means the analysis request has been accepted. 
	//    a margin of a few seconds is acceptable.
	// 2. jobs should not run an infinit amount of time. So, if the running time exceeds a certain time the analysis is considered to hang in an
	//    infinit loop, or is considered to have crashed.
	// 3. if a worker has free slots for a too long period, the worker is considered to have crashed or to hang.

	// 1. and 2.
	if ( (job_iter->second.status() == AnalysisStatus::kSubmitted && (current_time > job_iter->second.lastStatusChange() + m_timeOutSubmissionMax) )
	     || (job_iter->second.status() == AnalysisStatus::AnalysisStatus::kRunning 
		 && current_time > job_iter->second.lastStatusChange() + m_timeOutRunningAndArchivingMax)) {

	  if (m_logLevel>=kWarnLog) {
	    std::stringstream message;
	    if (job_iter->second.status() == AnalysisStatus::kSubmitted) {
	      message << " Analysis " << list_iter->first << " of ROD " << job_iter->first << " is in submitted state for "
		      << (current_time - job_iter->second.lastStatusChange()) <<  " sec  >  " << m_timeOutSubmissionMax << " sec. Will resubmit job.";
	    }
	    else {
	      message << " Analysis " << list_iter->first << " of ROD " << job_iter->first << " is running since  "
		      << (current_time - job_iter->second.lastStatusChange()) <<  " sec  >  " 
		      << m_timeOutRunningAndArchivingMax << " sec. Presumably the worker is hanging or has crashed will kill worker.";
	    }

// 	    Lock mrs_lock(m_mrsMutex);
// 	    sendMessage(*m_out,"ANAMON_fault_detected",s_componentName,MRS_WARNING, "CAN::AnalysisMonitor::monitor", message.str());
	    ers::warning(PixLib::pix::daq (ERS_HERE, "CAN::AnalysisMonitor::monitor", message.str()));
	  }
	  worker_has_crashed=true;
	}
	else {
	  // 3.
	  // if there are waiting jobs but some slots are not used for a certain amount of time or if a job remains 
	  const WorkerInfo_t &worker_info = job_iter->second.workerInfo();
	  if (worker_info.toBeRemoved()) {
	    worker_has_crashed=true;
	  }
	  else if (worker_info.waitingJobs()>0 
		   && current_time > worker_info.hangingSince() + ( worker_info.runningJobs()< worker_info.nSlots() ?
								    m_timeOutStart 
								    : m_timeOutRunningAndArchivingMax )
		   && !worker_info.toBeRemoved()) {
	    if (m_logLevel>=kDiagnosticsLog) {
	      std::stringstream message;
	      message << " Worker for Analysis " << list_iter->first << " of ROD " << job_iter->first << " has free slots since "
		      << (current_time - worker_info.hangingSince()) << " sec >  " << m_timeOutStart << ". Worker seems to have crashed. " 
		      << " Status : running = " << worker_info.runningJobs() << " / " << worker_info.nSlots()
		      << " Waiting = " << worker_info.waitingJobs()
		      << " hanging since = " << current_time - worker_info.hangingSince() << ".";


// 	      Lock mrs_lock(m_mrsMutex);
// 	      sendMessage(*m_out,"ANAMON_fault_detected",s_componentName,MRS_WARNING, "CAN::AnalysisMonitor::monitor", message.str());
	     ers::warning(PixLib::pix::daq (ERS_HERE, "CAN::AnalysisMonitor::monitor", message.str()));
	    }
	    else if (m_logLevel>=kWarnLog) {
	      std::stringstream message;
	      message << " Worker for Analysis " << list_iter->first << " of ROD " << job_iter->first << " has free slots since "
		      << (current_time - worker_info.hangingSince()) << " sec >  " << m_timeOutStart << ". Worker seems to have crashed." ;
	      {
// 		Lock mrs_lock(m_mrsMutex);
// 		sendMessage(*m_out,"ANAMON_fault_detected",s_componentName,MRS_WARNING, "CAN::AnalysisMonitor::monitor", message.str());
		ers::warning(PixLib::pix::daq (ERS_HERE, "CAN::AnalysisMonitor::monitor", message.str()));
	      }
	    }


	    worker_has_crashed=true;
	  }
	}

	if (worker_has_crashed) {
	  job_iter->second.workerInfo().markForRemoval();
	  if (job_iter->second.status()<=AnalysisStatus::kRunning) {
	    // jobs which are not running yet will be resubmitted
	    // jobs which are running will be marked as failed see below

	    if (m_logLevel>=kDiagnosticsLog) {
// 	      Lock mrs_lock(m_mrsMutex);
// 	      sendMessage(*m_out,"ANAMON",s_componentName,MRS_DIAGNOSTIC, "CAN::AnalysisMonitor::findCrashedWorker", "Need to resubmit jobs");
	       ers::debug(PixLib::pix::daq (ERS_HERE, "CAN::AnalysisMonitor::findCrashedWorker", "Need to resubmit jobs"));
	    }

	    resubmit=true;
	  }
	}
      }
    }
    return resubmit;
  }


}
