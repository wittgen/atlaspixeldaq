#ifndef _CAN_IDataServer_hh_
#define _CAN_IDataServer_hh_

#include <string>
  // @todo remove once the CAN::Histo type is used instead of the PixLib::Histo
#include <list>
#include <cassert>
#include <DataContainer/HistoInfo_t.h>
#include <ConfigWrapper/pixa_exception.h>
#include "exception.hh"
#include "Histo.hh"
#include "Common.hh"

namespace PixA {
  class ConnectivityRef;
   // @todo remove once the CAN::Histo type is used instead of the PixLib::Histo
  class HistoHandle;
  class DisabledList;
}

namespace PixLib {

  class Config;
  class ConfigObj;
  // @todo remove once the CAN::Histo type is used instead of the PixLib::Histo
  class Histo;

  class PixModuleGroup;
  class PixModule;

  // In principle not the whole PixModule / PixModuleGroup should be exposed since there are many methods which
  // will crash without hardware.
  typedef PixModuleGroup PixModuleGroupData_t;
  typedef PixModule      PixModuleData_t;
}

namespace CAN {

  /** Exception which gets thrown if a missing module, boc or scan config is accessed.
   */
  typedef PixA::ConfigException ConfigException;

  /** Exception which gets thrown if a histo info is requested but does not exist.
   */
  class  HistoException : public BaseException
  {
  public:
    HistoException(const std::string &name) : BaseException(name) {}

  };


  // @todo uncomment when Histo is unsed instead of PixLib::Histo
  class ExecutionUnit;
  class AverageResult_t;

  /** Server of histograms, configurations and the connecitivity.
   * The server will automatically provide the connectivity and the scan configuration.
   * Histograms and module or boc configurations etc. need to be requested before they
   * can be retrieved from the server.
   */
  class IDataServer {

    friend class ExecutionUnit;

  public:
    virtual ~IDataServer() {}

    // methods to request resources
    virtual void requestHistogram(const std::string &histogram_type_name,
				  const std::string &conn_name) = 0;

    virtual std::vector<std::string>  availableHistograms(const std::string &conn_name) {
      std::vector<std::string> dummy;
      return dummy;
    }
    
    virtual std::vector<std::string> dataServerHistos (const std::string &conn_name){
      std::vector<std::string> dummy;
      return dummy;
    }


    /** Request a tagged analysis summary value. 
     * A variable with a certain name and for a connectivity name can only requested for one tag per data server.
     */
    virtual void requestUIntAnalysisResult(const std::string &tag_name,
					   Revision_t &revision,
					   const std::string &conn_name,
					   const std::string &variable_name) = 0;

    virtual void requestBoolAnalysisResult(const std::string &tag_name,
					   Revision_t &revision,
					   const std::string &conn_name,
					   const std::string &variable_name) = 0;

    virtual void requestAvAnalysisResult(const std::string &tag_name,
					   Revision_t &revision,
					   const std::string &conn_name,
					   const std::string &variable_name) = 0;

    virtual void requestScanConfig() = 0;

    virtual void requestBocConfig() = 0;

    virtual void requestModuleConfig(const std::string &module_conn_name) = 0;

    virtual void requestModuleGroupConfig() = 0;

    //     virtual void requestModuleConfigObject(const std::string &conn_name,
    // 					   const std::string &config_group_name,
    // 					   const std::string &config_object_name) = 0;

    //     virtual void requestModuleConfigObject(const std::string &conn_name,
    // 					   const std::vector<std::string> &sub_config_and_group_name,
    // 					   const std::string &config_object_name) = 0;

    // methods to retrieve resources
    // resources are generally only available if they have been requested before hand

    /** Get the pix scan histogram of the given name and for the given connectivity object.
     * @param histogram_type_name the histogram name.
     * @param conn_name the name of the connectiivty object.
     * @param info will be filled with dimensions in each level of the pix scan histo.
     * @sa getHistogram.
     */
    virtual void numberOfHistograms(const std::string &histogram_type_name,
				    const std::string &conn_name,
				    HistoInfo_t &info) const =0 ;

    /** Get the pix scan histogram of the given name and for the given connectivity object.
     * @param histogram_type_name the histogram name.
     * @param conn_name the name of the connectiivty object.
     * @param index an array of indices which specifies the location in the pix-scan histo array.
     * This method may return NULL in case the specified histogram does not exist (but also,
     * if the histogram was not requested). In case the indices specified in index do not exceed
     * the index range which can be queried with @ref numberOfHistograms, the histogram should
     * exist in general. Howerve, this may not always be the case, In particular cases, the maximum
     * index range may depend on a particular branch of the pix scan histogram array. In the latter
     * case the information returned by numberOfHistograms corresponds to the union of all branches.
     * So, depending on the particular case a missing histogram may or may not be an error.
     * @sa numberOfHistograms.
     */
    virtual const Histo *getHistogram(const std::string &histogram_type_name,
				      const std::string &conn_name,
                                      const PixA::Index_t &index) const =0 ;

    /** Get a PixLib histogram (deprecated).
     * @todo this method will temporarily return a PixLib histo to
     *      keep compatibility with the autoAnalyses.
     * @deprecated
     */
    virtual PixLib::Histo *getHistogram(const std::string &histogram_type_name,
					const std::string &conn_name) const { assert(false); };

     /** Get a list of histogram handles for the PP0 (deprecated).
     * This method depends on a particular feature of the CT DataServer 
     * which will not exist for other implementations. So, in principle this
     * method should not be used.
     * @deprecated
     */
    virtual std::list<PixA::HistoHandle*> getHistograms(const std::string &scanType,const std::string &pp0name) { assert(false);} //CLA


    virtual unsigned int getUIntAnalysisResult(const std::string &conn_name,
					       const std::string &variable_name) const =0;

    virtual unsigned int getBoolAnalysisResult(const std::string &conn_name,
					       const std::string &variable_name) const =0;

    virtual const AverageResult_t &getAvAnalysisResult(const std::string &conn_name,
						       const std::string &variable_name) const =0;

    virtual const PixLib::Config &getScanConfig() const = 0;

    virtual const PixLib::Config &getBocConfig() const = 0;

    virtual const PixLib::Config &getModuleConfig(const std::string &module_conn_name) const = 0;

    virtual const PixA::DisabledList &getPp0DisabledList() const = 0;

    /** Creates a configured PixModule.
     * This function creates a copy of an internally cached PixModule.
     * It is intended to be used in case a PixModule configuration is to be changed
     * by an analysis. For read only access the @ref getModuleConfig method should be used
     * instead (memory and CPU efficiency).
     * The responsibility for the returned object is by the caller. Though, the shared point
     * will take care of deleting the object.
     * The return type is a shared pointer to a PixModuleData_t. The latter is
     * equivalent to a PixModule for the time being, but in the future the PixModuleData_t
     * may only provide a subset of the full interface of a PixModule.
     */
    virtual std::shared_ptr<PixLib::PixModuleData_t> createConfiguredModule(const std::string &module_conn_name) const = 0;
    
    /** Creates a configured PixModuleGroup.
     * This function creates a copy of an internally cached PixModuleGroup.
     * It is intended to be used in case a PixModuleGroup configuration is to be changed
     * by an analysis. For read only access the @ref getModuleConfig method should be used
     * instead (memory and CPU efficiency).
     * The responsibility for the returned object is by the caller. Though, the shared point
     * will take care of deleting the object.
     * The return type is a shared pointer to a PixModuleGroupData_t. The latter is
     * equivalent to a PixModuleGroup for the time being, but in the future the PixModuleGroupData_t
     * may only provide a subset of the full interface of a PixModuleGroup.
     */
    virtual std::shared_ptr<PixLib::PixModuleGroupData_t> createConfiguredModuleGroup() const = 0;

    //     virtual const PixLib::ConfigObj &getModuleConfigObject(const std::string &module_conn_name,
    // 							   const std::vector<std::string> &sub_config_and_group_name,
    // 							   const std::string &config_object_name) const = 0;

    //     virtual const PixLib::ConfigObj &getModuleConfigObject(const std::string &module_conn_name,
    // 							   const std::string &config_group_name,
    // 							   const std::string &config_object_name) const = 0;

    virtual bool needModuleConfig() const = 0;

  };

}

#endif
