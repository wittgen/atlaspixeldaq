#ifndef _Scheduler_hh_
#define _Scheduler_hh_

#include "Scheduler.hh"

#include "WorkerInfo_t.hh"
#include "AnalysisJobList_t.hh"
#include "AnalysisMonitor.hh"

#include <ipc/partition.h>
#include <ipc/object.h>

#include "Lock.hh"

#include <ConfigWrapper/Connectivity.h>
//#include <SerialNumberServer_ref.hh>

#include <map>
#include <vector>
#include <string>
#include <cassert>
#include <memory>

#include <memory>

#include <time.h>
//#include <iostream>
//#include <stdexcept>

#include "AnalysisInfo_t.hh"
#include <IMetaDataService.hh>

#include <is/infoT.h>

#include "LogLevel.hh"

//class MRSStream;

namespace PixLib{
  class PixMessages;
}

namespace CAN {

  class SubmissionRequest_t {
  public:
    SubmissionRequest_t(SerialNumber_t analysis_serial_number, const AnalysisInfoData_t &analysis_info);

    SerialNumber_t       analysisSerialNumber() const { return m_analysisSerialNumber; }
    const AnalysisInfo_t &analysisInfo() const        { return m_analysisInfo; }

    bool abort() const   { return m_abort; }
    void setAbort()      { m_abort=true;   }
    void resetAbort()    { m_abort=false;  }

  private:
    SerialNumber_t m_analysisSerialNumber;
    AnalysisInfo_t m_analysisInfo;
    bool           m_abort;
  };

  /**
   * @todo try to dstribute the same ROD always to the same worker nodes
   */
  class Scheduler_impl : public IPCNamedObject<POA_CAN::Scheduler,ipc::multi_thread>
  {
    class SubmissionProcessor;

    friend class SubmissionProcessor;
    friend class AnalysisMonitor;

  public:

    /** Initialise the scheduler.
     * The scheduler will get a reference to the serial number service which is expected in the same partition.
     * @todo Should there be the possibility to specify different partitions for the serial number service 
     *  and the scheduler ?
     */
    //    Scheduler_impl(IPCPartition &ipc_partition, IPCPartition &mrs_partition, const std::string &is_server_name);
    Scheduler_impl(IPCPartition &ipc_partition, const std::string &is_server_name);

    /** Submit an analysis request.
     * @param analysis_serial_number the serial number of the analysis.
     * @param analysis_info structure which describes the analysis algorithm, the classification and the post-processing. 
     * The scheduler will submit the specified analysis jobs for each ROD which was used in the scan (or analysis).
     * The serial number will be used to get the scan (or analysis) parameters including connectivity tags. Then
     * a job will be submitted for each used ROD. All jobs will be distributed among the registered nodes and queued.
     * The jobs will publish the progress in IS. If at some point nodes become free and there are still pending jobs,
     * then the pending jobs will be dequeued and re-exectuted on the free node.
     */
    void submitAnalysis(SerialNumber_t analysis_serial_number, const AnalysisInfoData_t &analysis_info);

    /** Query the status of all queues.
     *  This call may ask the worker nodes for the current status, thus it could block.
     */
    void getQueueStatus(CORBA::ULong &jobs_waiting_for_data, CORBA::ULong &jobs_waiting, CORBA::ULong  &jobs_running, CORBA::ULong  &empty_slots);

    /** Change the verbosity of the scheduler and the worker.
     * @param level the verbosity level : 0=fatal, 1=error, 2=warning,3=information,>=4 diagnostics.
     */
    void setLogLevel(unsigned short level);

    /** Abort all queueud and running jobs for the given analysis.
     */
    void abortAnalysis(SerialNumber_t analysis_serial_number);

    /** Register a worker node with the scheduler
     * @param obj an object of type CAN::Worker
     * @param identity a string which identifies the worker e.g. TDAQ_APPLICATION_OBJECT_ID.
     * @param n_slots the number of analyses which can be performed simultaniously (i.e. CPU cores).
     * @return the scheduler will return the current logging level.
     */
    unsigned short registerWorker( CORBA::Object_ptr obj , const char *identity, CORBA::ULong n_slots);

    /** De-register a worker node.
     * @param obj the object of type CAN::Worker
     */
    void deregisterWorker( CORBA::Object_ptr obj );

    /** resubmit the given analyses.
     * @param analysis_serial_number serial number of the analysis.
     * @param job_list the job list which can be registered already with the analysis monitor, but can also be a new list.
     * @param rod_list list of rod names and the expected load for which new analysis jobs should be created.
     * Will distribute the jobs over the available worker. 
     */
    bool spreadJobs(SerialNumber_t analysis_serial_number,
		    const std::shared_ptr<AnalysisJobList_t> &job_list,
		    const std::vector< std::pair<unsigned int, std::string > > &rod_list);

    /** Shutdown all worker and the scheduler itself.
     * This will abort all jobs, then shutdown all workers and finally shutdown the scheduler.
     */
    void shutdown();

    /** Reset worker.
     * This will abort all jobs, clear all queues, and unload all SEAL plugins.
     */
    void reset();

    /** Shutdown worker and send them restart signal.
     * If the worker are started by the respawn wrapper, the worker will be restarted with the same arguments.
     */
    void restartWorker();

    /** Method to verify whether the scheduler is still alive.
     */
    bool alive() {return true;}

    /** Will kall the alive method of all workers.
     * A message is send for eachs responding worker.
     */
    void pingWorker();

    void testWorker(unsigned int remove_after_n_missing_responses );

    void removeWorker();


    /** Remove a worker from the internal list.
     */
    void removeWorker( Worker_ptr a_worker);

  protected:
    void processSubmissionRequests();

    void processSubmissionRequest(SubmissionRequest_t &submission_request);

    bool sendAbortAnalysis(SerialNumber_t analysis_serial_number);

    unsigned int nSubmissionRequests() {
      Lock lock(m_submissionQueueMutex);
      return m_submissionRequests.size();
    }

  private:

    /** Get the scan info for the referenced master scan and compare the connectivity tags with all the other referenced scans.
     *  Will return an invalid scan info in case of failure.
     */
    InfoPtr_t<PixLib::ScanInfo_t> getScanInfoAndCompareConnTags(SerialNumber_t analysis_serial_number, const AnalysisInfo_t &the_analysis_info);

    /** Query the status of all queues.
     *  This call may ask the worker nodes for the current status, thus it could block.
     */
    void checkWorkerAndRemoveDeadOnes(CORBA::ULong &jobs_waiting_for_data,
				      CORBA::ULong &jobs_waiting,
				      CORBA::ULong &jobs_running,
				      CORBA::ULong &empty_slots,
				      unsigned int remove_after_n_missing_responses );

    bool spreadJobs(SerialNumber_t analysis_serial_number,
		    const std::shared_ptr<AnalysisJobList_t> &job_list,
		    const AnalysisInfoData_t &analysis_info_data,
		    const std::vector< std::pair<unsigned int, std::string > > &rod_list);

    bool submitJobs( SerialNumber_t analysis_serial_number,
		     const std::shared_ptr<AnalysisJobList_t> &job_list,
		     const AnalysisInfoData_t &analysis_info_data,
		     std::shared_ptr<WorkerInfo_t> the_worker,
		     const std::vector< std::pair<unsigned int, std::string > > &rod_list);

    std::pair<unsigned int, unsigned> workerSlots();

    class SubmissionProcessor : public omni_thread
    {
    public:
      SubmissionProcessor(Scheduler_impl &scheduler) : m_scheduler(&scheduler), m_wait(30), m_shutdown(false) { start_undetached(); }
      ~SubmissionProcessor() { shutdownWait(); }

      void shutdownWait();

      void newRequest() {
	m_hasRequests.setFlag();
      }

      void wait(unsigned long wait_in_seconds) {
	m_wait=wait_in_seconds;
      }

    protected:
      void *run_undetached(void *arg);

    private:

      Scheduler_impl *m_scheduler;
      Flag            m_hasRequests;
      unsigned long   m_wait;

      Flag            m_exit;
      bool            m_shutdown;
    };
    
    //    SENSE::SerialNumberServer_ref m_serialNumberService;

    Mutex m_connMutex;
    Mutex m_metaDataMutex;
    Mutex m_herdMutex;
    Mutex m_spreadMutex;
    //    Mutex m_mrsMutex;
    std::vector< std::shared_ptr<WorkerInfo_t > > m_herd;
    std::map< std::string, std::vector< std::shared_ptr<WorkerInfo_t > > > m_preferredWorker;

    //    std::auto_ptr<MRSStream> m_out;

    Mutex m_queueMutex;

    Mutex                              m_submissionQueueMutex;
    std::deque< SubmissionRequest_t >  m_submissionRequests;
    std::unique_ptr<SubmissionProcessor> m_submissionProcessor;

    AnalysisMonitor m_analysisMonitor;

    unsigned int m_maxConcurrentSubmissions;
    unsigned int m_maxSubmissionQueueLength;
    unsigned long m_retrySubmissionTime;
    unsigned int m_nWorker;

    static std::string s_componentName;
    std::string m_isServerName;

    ELogLevel m_logLevel;
  };
}
#endif
