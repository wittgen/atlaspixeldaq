#include "Scheduler_ref.hh"
#include "exception.hh"

namespace CAN {

  Scheduler_ref::Scheduler_ref(IPCPartition &ipc_partition)
    : m_handle(ipc_partition.lookup<CAN::Scheduler>(s_schedulerName) )
  {
    if (CORBA::is_nil(m_handle)) {
      std::string message("Cannot find scheduler of name " );
      message += s_schedulerName;
      message += " in partition ";
      message += ipc_partition.name();
      message += ".";
      //      throw MRSException("NO_SCHED", MRS_FATAL, "CAN::Scheduler_ref::ctor",message);
      throw MRSException(PixLib::PixMessages::FATAL, "CAN::Scheduler_ref::ctor",message);
    }
  }

  Scheduler_ref::~Scheduler_ref() {}

}
