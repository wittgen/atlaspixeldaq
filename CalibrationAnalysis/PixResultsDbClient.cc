#include "PixResultsDbClient.hh"
#include "PixelCoralClientUtils/CoralClient.hh"

class PixResultsDbClientWrapper  : public IPixResultsDbClient {

public:
  PixResultsDbClientWrapper(const std::string &connection_string, bool verbose, coral::AccessMode access_mode) 
    : m_client(new PixCoralClient(connection_string, verbose, access_mode)) {}

  PixResultsDbClientWrapper(bool verbose, coral::AccessMode access_mode) 
    : m_client(new PixCoralClient(verbose, access_mode)) {}

  ~PixResultsDbClientWrapper() { delete m_client; }

  void disconnect() { m_client->disconnect(); }
 

  //PVSS methods
  double get_value_from_PVSSarch(std::string name,const coral::TimeStamp &start,const coral::TimeStamp &end) {
    return m_client->get_value_from_PVSSarch(name, start,end);
  }

  double get_values_from_PVSSarch(std::string name, const coral::TimeStamp &start, const coral::TimeStamp &end) {
    return m_client->get_values_from_PVSSarch(name, start,end);
  }

  CAN::AnalysisResultList_t getAnalysisResultsFromDB(CAN::SerialNumber_t anal_id, std::string varname="", std::string connName="") {
    return m_client->getAnalysisResultsFromDB(anal_id, varname, connName);
  }

  CAN::AnalysisResultList_t getAnalysisResultsFromDB(CAN::SerialNumber_t anal_id, const std::vector<std::string> &connName, std::string varname="") {
    return m_client->getAnalysisResultsFromDB(anal_id, connName, varname);
  }

  /** Get analysis results for a list of connectivity objects.
   * @param analysis_id list of analysis serial numbers,
   * @param connName list of connectivity names for which the values should be retrieved
   * @param listener object which will be notified about new variables.
   */
  void getAnalysisResultsFromDB(std::vector<CAN::SerialNumber_t> analysis_id,
					const std::vector<std::string> &connName,
					IPixResultsDbListener &listener) {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
		std::cout << "analysis_id=" << analysis_id[0] << std::endl;
		std::cout << "connName.size()=" << connName.size() << std::endl;
		//std::cout << "connName="; for (auto s : connName ) std::cout << s << "\t"; std::cout << std::endl;
		//m_client->verboseOutput(true);
    m_client->getAnalysisResultsFromDB(analysis_id, connName, listener);

  }

  void verboseOutput(bool verbose) {
    m_client->verboseOutput(verbose);
  }

private:
  PixCoralClient *m_client;

};


IPixResultsDbClient *PixResultsDbClient::createClient(const std::string &connection_string, bool verbose, coral::AccessMode access_mode) {
  return new PixResultsDbClientWrapper(connection_string, verbose, access_mode);
}

IPixResultsDbClient *PixResultsDbClient::createClient(bool verbose, coral::AccessMode access_mode) {
  return new PixResultsDbClientWrapper(verbose, access_mode);
}
