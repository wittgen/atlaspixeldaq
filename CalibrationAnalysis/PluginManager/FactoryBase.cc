#include "FactoryBase.hh"
#include "PluginManagerInstance.hh"

#ifdef DEBUG
#  define DEBUG_TRACE(a) { a }
#else
#  define DEBUG_TRACE(a) {}
#endif

namespace CAN {

  FactoryBase::FactoryBase() {
  }

  void FactoryBase::registerMe() {
    PluginManagerInstance::instance()->registerFactory(this);
  }

  FactoryBase::~FactoryBase() {
    PluginManagerInstance::instance()->deregisterFactory(this);
    for(KitList_t::iterator iter = m_kitList.begin();
	iter != m_kitList.end();
	++iter) {
      iter->second.prepareUnload();
    }
  }

  const KitInfo &FactoryBase::_getKit(const std::string &name)
  {
    DEBUG_TRACE( std::cout << "INFO [FactoryBase::_getKit] " << name << std::endl; )
    KitList_t::iterator kit_iter = m_kitList.find(name);
    for(unsigned int pass_i=0; pass_i<2; pass_i++) {
    if (kit_iter == m_kitList.end()) {
      PluginManagerInstance::instance()->searchPlugin( name );
      DEBUG_TRACE( std::cout << "INFO [FactoryBase::_getKit] searched plugins " << name << std::endl;      )
      DEBUG_TRACE( dumpKits(); )
      kit_iter = m_kitList.find(name);
      if (kit_iter == m_kitList.end()) {
	throw MissingKitException(name);
      }
    }
    if (!kit_iter->second.hasKit()) {
      if (pass_i>0 || !kit_iter->second.canLoad()) {
	throw MissingKitException(name);
      }
      try {
	PluginManagerInstance::instance()->loadPlugin( kit_iter->second.libraryName());
      }
      catch( ... ) {
	if (pass_i==0) {
	  kit_iter = m_kitList.end();
	  continue;
	}
	else {
	  throw;
	}
      }
    }
    }
    DEBUG_TRACE( dumpKits(); )
    assert( kit_iter != m_kitList.end() && kit_iter->second.hasKit());
    return kit_iter->second;
  }

  bool FactoryBase::_registerKit(const std::string &name, IObjectKit *kit) {
    if (kit) {
      KitList_t::iterator kit_iter = m_kitList.find(name);
      if (kit_iter != m_kitList.end()) {
	if (kit_iter->second.hasKit()) {
	  std::cout << "WARNING [FactoryBase::_registerPlugin] Kit does already exist for " << name << ". Will delete old kit." << std::endl;
	  kit_iter->second.prepareUnload();
	}
	kit_iter->second.setKit(kit);
	kit_iter->second.setLibraryName(m_scanLibrary);
	return true;
      }
      else {
	return m_kitList.insert( std::make_pair(name, KitInfo(kit,m_scanLibrary)) ).second;
      }
    }
    return false;
  }

  bool FactoryBase::registerKit(const std::string &name, const std::string &library_name) {
    if (!library_name.empty()) {
      KitList_t::iterator kit_iter = m_kitList.find(name);
      if (kit_iter != m_kitList.end()) {
	if (!kit_iter->second.libraryName().empty() && kit_iter->second.libraryName()!= library_name) {
	  std::cout << "WARNING [FactoryBase::_registerPlugin] There is alread a library name assigned to Kit " << name
		    << ". Will use new library : " << library_name << std::endl;
	}
	kit_iter->second.setLibraryName(library_name);
	return true;
      }
      else {
	DEBUG_TRACE( std::cout << "INFO [FactoryBase::_registerPlugin] register kit " << name << " which is provided by " << library_name << std::endl; )
	return m_kitList.insert( std::make_pair(name, KitInfo(library_name)) ).second;
      }
    }
    return false;

    m_kitList.insert(std::make_pair(name, KitInfo(library_name)));
    return true;
  }

  void FactoryBase::prepareForUnloading(const std::string &library_name) {
    for(KitList_t::iterator iter = m_kitList.begin();
	iter != m_kitList.end();
	++iter) {
      if (iter->second.libraryName() == library_name) {
	iter->second.prepareUnload();
      }
    }    
  }


  void FactoryBase::dumpKits() const {

    std::cout << "INFO [PluginFactory::dumpKits] List of kits of " << name()  << std::endl;

    for(KitList_t::const_iterator iter = m_kitList.begin();
	iter != m_kitList.end();
	++iter) {
      std::cout << "INFO [PluginFactory::dumpKits] " << iter->first << " : "
		<<  iter->second.libraryName() << " "
		<< (iter->second.hasKit() ? "loaded" : "") << std::endl;
    }
  }

  void FactoryBase::kitList(std::map< std::string, std::map< std::string, std::vector<std::string> > > &kit_names) const {
    for(KitList_t::const_iterator iter = m_kitList.begin();
	iter != m_kitList.end();
	++iter) {
      if (iter->second.canLoad()) {
	kit_names[ iter->second.libraryName() ][ name() ].push_back(iter->first);
      }
    }
  }

}
