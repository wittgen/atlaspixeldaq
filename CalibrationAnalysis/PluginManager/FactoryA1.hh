/* Dear emacs, this is -*-c++-*- */
#ifndef _CAN_FactoryA1_h_
#define _CAN_FactoryA1_h_

#include "FactoryBase.hh"
#include <iostream>

namespace CAN {

  // two argument constructor kits and corresponding factory

  /** Interface of a kit which creates objects with a two argument constructor
   */


  /** 
   * Somewhere in the code the factory needs to be instantiated:
   * FACTORY_INSTANCE( FactoryA1<MyClass, MyArg1> )
   */
  template <class T_base, class T_arg1>
  class FactoryA1 : public FactoryBase
  {
  public:
    FactoryA1() : m_name(s_instanceName) { registerMe(); }

    /** Abstract kit interface.
     */
    class IFactoryKit : public IObjectKit
    {
    public: 
      virtual T_base *create(T_arg1 a1) const = 0;
    };

    /** Specific kit implementation
     */
    template <class T_object> 
    class FactoryKit : public IFactoryKit
    {
    public: 
      virtual T_base *create(T_arg1 a1) const
      { return new T_object(a1); }
    };


    bool registerKit(const std::string &name, IFactoryKit *kit) {
      return _registerKit(name, kit);
    }

    template <class T_object>
    bool registerKit(const std::string &name) {
      std::cout << "INFO [FactoryA1::registerKit] " << name << " with " << this->name() << std::endl;
      return registerKit(name, new FactoryKit<T_object> );
    }

    T_base *create(const std::string &name, T_arg1 a1) {
      const KitInfo &kit_info = _getKit(name);
      assert(dynamic_cast< const IFactoryKit *>(kit_info.kit()) );
      return static_cast< const IFactoryKit *>(kit_info.kit())->create(a1);
    }

    const std::string &name() const {
      return m_name;
    }

    static FactoryA1<T_base, T_arg1> *get() {
      if (!s_instance) {
	s_instance = new FactoryA1;
	std::cout << "INFO [FactoryA1::get] create new instance for " << s_instanceName << std::endl;
      }
      return s_instance;
    }

  private:
    static FactoryA1 *s_instance;
    static const char *s_instanceName;
    std::string m_name;
  };

}
#endif
