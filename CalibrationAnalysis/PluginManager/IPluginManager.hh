/* Dear emacs, this is -*-c++-*- */
#ifndef _CAN_IPluginManager_h_
#define _CAN_IPluginManager_h_

#include <stdexcept>
#include <string>

namespace CAN {

  class FactoryBase;

  class PluginError : public std::runtime_error
  {
  public:
    PluginError(const std::string &message) : std::runtime_error(message) {}
  };

  class IPluginManager
  {
  public:
    virtual ~IPluginManager() {}
    virtual bool loadPlugin(const std::string &name) = 0;
    virtual bool unloadPlugin(const std::string &name) = 0;

    virtual void registerFactory(FactoryBase *factory) = 0;
    virtual void deregisterFactory(FactoryBase *factory) = 0;

    virtual bool searchPlugin(const std::string &name) = 0;

    virtual void dumpCache() = 0;

  };

}
#endif
