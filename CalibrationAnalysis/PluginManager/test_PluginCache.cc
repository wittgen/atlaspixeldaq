#include "FactoryA2.hh"
#include "test_ITestPlugin.hh"
#include "PluginCache.hh"
#include "PluginManager.hh"

#include <memory>
#include <exception>
#include <iostream>
#include <vector>

typedef CAN::FactoryA2<CAN::ITestPlugin,std::string,unsigned int> TestFactory;

CAN_FACTORY_INSTANCE( TestFactory )

int main(int argc, char **argv)
{
  std::string arg1="dummy";
  unsigned int arg2=4711;
  TestFactory::get()->dumpKits();
  std::vector<std::string> plugin_names;

  //  std::auto_ptr<CAN::PluginManager>  pm;
  //  CAN::PluginCache::instance()->readCache();

  for (unsigned int pass_i=0; pass_i<3; pass_i++) {
    std::cout << "INFO [main] pass=" << pass_i << std::endl;
    for (int arg_i=1; arg_i<argc; arg_i++) {
      if (strcmp(argv[arg_i],"-l")==0) {
	++arg_i;
	if (pass_i==0) {
	  plugin_names.push_back(argv[arg_i]);
	}
      }
      else {
	std::string name(argv[arg_i]);
	try {
	  std::cout << "INFO [main] Try to ." << std::endl;
	  unsigned int a_arg = arg_i;
	  std::auto_ptr<CAN::ITestPlugin> test( TestFactory::get()->create(name ,std::string("Should execute : ")+name,a_arg) );
	  if (test.get()) {
	    test->execute();
	  }
	}
	catch (std::exception &err) {
	  std::cerr << "Caught excetpion : " << err.what() << std::endl;
	}
      }
    }
    if (pass_i==0) {
      //      pm=std::auto_ptr<CAN::PluginManager>(new CAN::PluginManager);
      std::cout << "INFO [main] Try to load libraries." << std::endl;
      for(std::vector<std::string>::const_iterator plugin_iter = plugin_names.begin();
	  plugin_iter != plugin_names.end();
	  ++plugin_iter) {
	CAN::PluginCache::instance()->scanLibrary(*plugin_iter);
	//	pm->loadPlugin(*plugin_iter);
      }
      std::cout << "INFO [main] PluginCache::dumpKitList:" << std::endl;
      CAN::PluginCache::instance()->dumpKitList();
      std::cout << "INFO [main] TestFactory::dumpKits:" << std::endl;
      TestFactory::get()->dumpKits();
      CAN::PluginCache::instance()->writeCache();
    }
    else if (pass_i==1) {
      std::cout << "INFO [main] Try to unload libraries." << std::endl;
      for(std::vector<std::string>::const_iterator plugin_iter = plugin_names.begin();
	  plugin_iter != plugin_names.end();
	  ++plugin_iter) {
	CAN::PluginCache::instance()->unloadLibrary(*plugin_iter);
      }
      std::cout << "INFO [main] PluginCache::dumpKitList:" << std::endl;
      CAN::PluginCache::instance()->dumpKitList();
      std::cout << "INFO [main] TestFactory::dumpKits:" << std::endl;
      TestFactory::get()->dumpKits();
    }
  }

  CAN::PluginCache::instance()->dumpKitList();
  return 0;
}
