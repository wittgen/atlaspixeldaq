/* Dear emacs, this is -*-c++-*- */
#ifndef _CAN_DummyPluginManager_h_
#define _CAN_DummyPluginManager_h_

#include <IPluginManager>

namespace CAN {

  class IPluginManager
  {
  public:
    bool loadPlugin(const std::string &) { return false; } 
    bool unloadPlugin(const std::string &) { return false };
    void registerFactory(FactoryBase *factory) {}
    void deregisterFactory(FactoryBase *factory) {}

  };

}
