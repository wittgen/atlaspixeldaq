/* Dear emacs, this is -*-c++-*- */
#ifndef _CAN_PluginManager_h_
#define _CAN_PluginManager_h_

#include <stdexcept>
#include <map>
#include <string>
#include <memory>

#include "IPluginManager.hh"

namespace CAN {
  class PluginCache;

  class PluginManager : public IPluginManager
  {

    class PluginInfo
    {
    public:
      PluginInfo()
	: m_handle(NULL)
      {}

      PluginInfo(void *handle)
	: m_handle(handle)
      {}

      bool isLoaded() const { return m_handle != NULL; }
      void setHandle(void *handle) { m_handle=handle; }
      void clearHandle() { m_handle=NULL; }

      void *handle() { return m_handle; }

    private:
      void *m_handle;
    };

    typedef std::map<std::string, PluginInfo> PluginList_t;
  public:
    PluginManager();
    ~PluginManager();
    bool loadPlugin(const std::string &name);
    bool unloadPlugin(const std::string &name);

    void registerFactory(FactoryBase *factory);
    void deregisterFactory(FactoryBase *factory);

    bool searchPlugin(const std::string &name);

    void dumpCache();

  private:
    std::unique_ptr<PluginCache> m_cache;
    PluginList_t m_pluginList;
  };

}
#endif
