#include "test_ITestPlugin.hh"
#include "FactoryA2.hh"
#include <iostream>

namespace CAN {

class TestPlugin3 : public ITestPlugin
{
public:

  TestPlugin3(const std::string &name, unsigned int version)
    : m_name(name),
      m_version(version)
  {}
  
  void execute() {
    std::cout << "INFO [TestPlugin3::execute] Plugin III " << m_name << " v" << m_version << std::endl;
  }
private:
  std::string m_name;
  unsigned int m_version;
};

typedef CAN::FactoryA2<CAN::ITestPlugin,std::string,unsigned int> Factory;
CAN_REGISTER_OBJECT(Factory, TestPlugin3)

}
