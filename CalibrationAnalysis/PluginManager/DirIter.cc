#include "DirIter.hh"

#include <iostream>

namespace CAN {

  const char DirIter::s_folderSep='/';

  time_t DirLister::lastModificationTime(const std::string &path, const std::string &name) {
    struct ::stat   a_stat;

    if ( lstat(DirLister::createPath(path, name).c_str(), &a_stat ) == 0) {
      return a_stat.st_mtime;
    }
    return 0;
  }

  bool DirLister::rename(const std::string &old_name, const std::string &new_name) {
    return ::rename(old_name.c_str(), new_name.c_str()) == 0;
  }

  std::string DirLister::tempnam(const std::string &path, const std::string &prefix) {
    std::string file_name;
    // std::cout << "INFO [DirLister::tempnam] dir_name =" << path.c_str() << " prefix=" << prefix.c_str() << std::endl;
    char *ptr = ::tempnam(path.c_str(),(prefix.empty() ? NULL : prefix.c_str()));
    if (ptr) {
      file_name=ptr;
      free(ptr);
      std::string::size_type pos=file_name.rfind(DirIter::folderSep());
      if (pos!=std::string::npos && file_name.size()-pos>prefix.size()+1) {
         file_name = file_name.substr(pos+1,file_name.size()-pos-1);
      }
      file_name = DirLister::createPath(path, file_name);
   }
    return file_name;
  }

}
