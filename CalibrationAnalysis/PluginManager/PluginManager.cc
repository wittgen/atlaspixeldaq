#include "PluginManager.hh"
#include "PluginCache.hh"
#include <dlfcn.h>

#include <sstream>
#include <iostream>

#ifdef DEBUG
#  define DEBUG_TRACE(a) { a }
#else
#  define DEBUG_TRACE(a) {}
#endif

namespace CAN {

  PluginManager::PluginManager() : m_cache(new PluginCache) {}
  PluginManager::~PluginManager() {}

  bool PluginManager::loadPlugin(const std::string &name) {
    DEBUG_TRACE( std::cout << "INFO [PluginManager::loadPlugin] will load " << name << std::endl; )

    PluginList_t::iterator library_iter = m_pluginList.find(name);
    if (library_iter == m_pluginList.end()) {
      std::pair<PluginList_t::iterator, bool> ret = m_pluginList.insert(std::make_pair(name,PluginInfo()));
      if (ret.second == false) {
	std::stringstream message;
	message << "Failed to load library " << name << ".";
	throw std::runtime_error(message.str());
      }
      library_iter = ret.first;
    }

    if (!library_iter->second.isLoaded()) {
      DEBUG_TRACE( std::cout << "INFO [PluginManager::loadPlugin] will load " << name << std::endl; )
      void *handle = ::dlopen(name.c_str(),  RTLD_LAZY | RTLD_GLOBAL );
      DEBUG_TRACE( std::cout << "INFO [PluginManager::loadPlugin] dlopen of " << name << " returned " <<  handle << "." << std::endl; )
      if (!handle) {

	std::stringstream message;
	message << "ERROR [PluginManager::loadPlugin] Failed to load " << name << " : "
		<<  ::dlerror ();
	DEBUG_TRACE( std::cerr << message.str() << std::endl; )
	
	throw PluginError(message.str());
      }
      library_iter->second.setHandle(handle);
    }

    return true;
  }

  bool PluginManager::unloadPlugin(const std::string &name) {
    PluginList_t::iterator library_iter = m_pluginList.find(name);

    if (library_iter != m_pluginList.end()) {
      if (library_iter->second.isLoaded()) {

	DEBUG_TRACE( std::cout << "INFO  [PluginManager::unloadPlugin] try to unload " << library_iter->first << std::endl; )
	if ( ::dlclose( library_iter->second.handle()) != 0 ) {

	  std::stringstream message;
	  message << "ERROR [PluginManager::unloadPlugin] Failed to load " << name << " : "
		  <<  ::dlerror ();
	  std::cerr << message.str() << std::endl;

	  return false;
	}
	library_iter->second.clearHandle();
      }
    }
    return true;
  }

  void PluginManager::registerFactory(FactoryBase *factory) {
    m_cache->registerFactory(factory);
  }

  void PluginManager::deregisterFactory(FactoryBase *factory) {
    m_cache->deregisterFactory(factory);
  }

  void PluginManager::dumpCache() {
    m_cache->dumpKitList();
  }

  bool PluginManager::searchPlugin(const std::string &name) {
    return m_cache->searchPlugin(name);
  }


}
