/* Dear emacs, this is -*-c++-*- */
#ifndef _CAN_DirIter_hh_
#define _CAN_DirIter_hh_

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <memory>

#include <ConfigWrapper/Regex_t.h>

namespace CAN {

  class IDirIter
  {
  public:
    virtual ~IDirIter() {}
    virtual const std::string &name() const = 0;
    virtual std::string pathName() const = 0;
    virtual bool next() = 0;
    virtual time_t lastModificationTime() const = 0;
  };

  class DirIter : public IDirIter
  {

    friend class DirLister;
  protected:
    DirIter( const std::string &path_name, const std::string &regex, bool list_dirs_only)
      : m_pathName(path_name),
	m_listDirs( list_dirs_only )
    {
      if (!m_pathName.empty() && m_pathName[m_pathName.size()-1]!=s_folderSep) {
	m_pathName+=s_folderSep;
      }
      if (!regex.empty()) {
	m_regex=std::make_unique<PixA::Regex_t>(regex);
      }

      m_directory = opendir(path_name.c_str());
    }

    static const char folderSep() { return s_folderSep; }

  public:
    ~DirIter() {
      if (m_directory) {
	closedir(m_directory);
	m_directory=NULL;
      }
    }

    const std::string &name() const {
      return m_currentName;
    }

    std::string pathName() const {
      return m_pathName + m_currentName;
    }

    bool next() {
      if (!m_directory) return false;
      int ret;
      struct dirent *ptr;
      for(;;) {
	ret = readdir_r (m_directory, &m_entry,&ptr);

	m_currentName = ( m_entry.d_name );

	if (ret!=0 || ptr==0) break;
	if (m_currentName=="." || m_currentName=="..") continue;

	if (m_regex.get()) {
	  if (!m_regex->match(m_currentName)) continue;
	}

	stat((m_pathName + m_currentName).c_str(), &m_stat);
	bool is_dir = S_ISDIR(m_stat.st_mode)!=0;

	if (is_dir == m_listDirs) break;
      }

      return ret==0 && ptr!=0;
    }

    time_t lastModificationTime() const {
      return m_stat.st_mtime;
    }

  private:
    std::string                        m_pathName;
    std::unique_ptr<PixA::Regex_t>  m_regex;

    DIR          *m_directory;
    struct dirent m_entry;
    struct stat   m_stat;
    
    std::string   m_currentName;

    static const char s_folderSep;

    bool m_listDirs;
  };

  class DirLister {
  public:

    static IDirIter *fileIterator(const std::string &path_name, const std::string &extnsion) {
      return new DirIter(path_name, extnsion, false);
    }

    static IDirIter *directoryIterator(const std::string &path_name) {
      return new DirIter(path_name, "", true);
    }

    static std::string createPath(const std::string &path, const std::string &name) {
      return path + DirIter::folderSep() + name;
    }

    static time_t lastModificationTime(const std::string &path, const std::string &name);

    static bool rename(const std::string &old_name, const std::string &new_name);

    static std::string tempnam(const std::string &path, const std::string &prefix);

  };

  class FileInfo {
  public:
    FileInfo(const std::string &file_name) {
      int ret = stat(file_name.c_str(), &m_stat);
      m_fileExists = (ret==0);
    }

    bool exists() const { return m_fileExists; }
    bool isDir() const  { return m_fileExists && (S_ISDIR(m_stat.st_mode)!=0); }
    bool isFile() const { return (S_ISREG(m_stat.st_mode)!=0); }

  private:
    struct stat   m_stat;
    bool m_fileExists;
  };


}
#endif
