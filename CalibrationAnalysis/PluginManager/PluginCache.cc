#include "PluginCache.hh"
#include "PluginManagerInstance.hh"
#include "FactoryBase.hh"

#include <set>
#include <fstream>

#include "DirIter.hh"

#ifdef DEBUG
#  define DEBUG_TRACE(a) { a }
#else
#  define DEBUG_TRACE(a) {}
#endif


namespace CAN {
  //  PluginCache *PluginCache::s_instance=NULL;
  const char *PluginCache::s_cacheFileName=".can.cache";
  const char *PluginCache::s_cacheFileTempPrefix=".canc";
  const char *PluginCache::s_moduleRegistryPattern=".*\\.reg$";

  PluginCache::PluginCache() { 
    DEBUG_TRACE( std::cout << "INFO [PluginCache::ctor] " << std::endl; )
  }

  PluginCache::~PluginCache() {}

  void PluginCache::registerFactory(FactoryBase *factory) {
    if (factory) {
      DEBUG_TRACE( std::cout << "INFO [PluginCache::registerFactory] factory "  << factory->name() << std::endl; )
      m_factoryList[factory->name()]=factory;
      //      readCache(factory->name());
    }
  }

  void PluginCache::deregisterFactory(FactoryBase *factory) {
    std::map<std::string, FactoryBase *>::iterator iter = m_factoryList.find(factory->name());
    if (iter != m_factoryList.end()) {
      DEBUG_TRACE( std::cout << "INFO [PluginCache::deregisterFactory] deregister factory "  << iter->second->name() << std::endl; )

      m_factoryList.erase(iter);
    }
  }

  void PluginCache::dumpKitList() {
    std::cout << "INFO [PluginCache::dumpKitList] dump kits of "  << m_factoryList.size() << " factories."  << std::endl;

    std::map< std::string, std::map< std::string, std::vector<std::string> > > kit_list;
    for (std::map<std::string, FactoryBase *>::const_iterator factory_iter = m_factoryList.begin();
	 factory_iter != m_factoryList.end();
	 ++factory_iter) {
      factory_iter->second->kitList(kit_list);
    }

    for( std::map< std::string, std::map< std::string , std::vector<std::string> > >::const_iterator lib_iter =  kit_list.begin();
	 lib_iter != kit_list.end();
	 ++lib_iter) {
      std::cout << "INFO [PluginCache::dumpKitList] module " << lib_iter->first << std::endl;

      for( std::map< std::string , std::vector<std::string> >::const_iterator factory_iter =  lib_iter->second.begin();
	   factory_iter != lib_iter->second.end();
	   ++factory_iter) {
	for (std::vector<std::string>::const_iterator  kit_iter = factory_iter->second.begin();
	     kit_iter != factory_iter->second.end();
	     ++kit_iter) {
	  std::cout << "INFO [PluginCache::dumpKitList]   " << factory_iter->first << " " << *kit_iter << std::endl;
	}
      }
    }
  }

//   void PluginCache::writeCache(const std::string &dir_name) {
//     std::cout << "INFO [PluginCache::writeCache]" << std::endl;
//     std::map< std::string, std::map< std::string, std::vector<std::string> > > kit_list;
//     for (std::map<std::string, FactoryBase *>::const_iterator factory_iter = m_factoryList.begin();
// 	 factory_iter != m_factoryList.end();
// 	 ++factory_iter) {
//       factory_iter->second->kitList(kit_list);
//     }
//   }

  void PluginCache::writeCache(const std::string &dir_name,
			       std::map< std::string, std::pair< std::map< std::string, std::vector<std::string> >, time_t> > &kit_list)
  {

    std::string temp_name = DirLister::tempnam(dir_name,s_cacheFileTempPrefix);
    std::ofstream out( temp_name.c_str() );

    for( std::map< std::string, std::pair< std::map< std::string , std::vector<std::string> >, time_t>  >::const_iterator lib_iter =  kit_list.begin();
	 lib_iter != kit_list.end();
	 ++lib_iter) {

      if (lib_iter->second.first.empty()) continue;

      out << "module " << lib_iter->first << " " << lib_iter->second.second << std::endl;
      out << "begin" << std::endl;
      for( std::map< std::string , std::vector<std::string> >::const_iterator factory_iter =  lib_iter->second.first.begin();
	   factory_iter != lib_iter->second.first.end();
	   ++factory_iter) {

	for (std::vector<std::string>::const_iterator  kit_iter = factory_iter->second.begin();
	     kit_iter != factory_iter->second.end();
	     ++kit_iter) {
	  out << "   " << factory_iter->first << " " << *kit_iter << std::endl;
	  DEBUG_TRACE( std::cout << "INFO [PluginCache::writeCache]" << lib_iter->first << "   " << factory_iter->first << " " << *kit_iter << std::endl; )
	}
      }
      out << "end" << std::endl;
    }
    DirLister::rename(temp_name,DirLister::createPath(dir_name,s_cacheFileName));
  }
  

  void PluginCache::scanLibrary(const std::string &name, bool unload_libraries) {
    for (std::map<std::string, FactoryBase *>::const_iterator factory_iter = m_factoryList.begin();
	 factory_iter != m_factoryList.end();
	 ++factory_iter) {
      factory_iter->second->setScanLibrary(name);
    }
    
    try {
      PluginManagerInstance::instance()->loadPlugin(name);
    }
    catch( std::exception &err) {
      std::cerr << "ERROR [PluginCache::scanLibrary] Caught exception while loading " << name << " : " << err.what() << std::endl;
    }

    DEBUG_TRACE( dumpKitList(); )

    for (std::map<std::string, FactoryBase *>::const_iterator factory_iter = m_factoryList.begin();
	 factory_iter != m_factoryList.end();
	 ++factory_iter) {
      factory_iter->second->clearScanLibrary();
    }
    if (unload_libraries) {
      unloadLibrary(name);
    }
  }

  void PluginCache::unloadLibrary(const std::string &name)
  {
    DEBUG_TRACE( std::cout << "INFO [PluginCache::unloadLibrary] prepare for unloading plugin " << name << std::endl; )
    std::map< std::string, std::map< std::string, std::vector<std::string> > > kit_list;
    for (std::map<std::string, FactoryBase *>::const_iterator factory_iter = m_factoryList.begin();
	 factory_iter != m_factoryList.end();
	 ++factory_iter) {
      factory_iter->second->prepareForUnloading(name);    
    }
    DEBUG_TRACE( std::cout << "INFO [PluginCache::unloadLibrary] no unload plugin " << name << std::endl; )

    try {
      PluginManagerInstance::instance()->unloadPlugin(name);
    }
    catch( std::exception &err) {
      std::cerr << "ERROR [PluginCache::scanLibrary] Caught exception while loading " << name << " : " << err.what() << std::endl;
    }
    
  }
/*
  void PluginCache::readCache(const std::string &factory_name,
			      const std::string &dir_name)
  {
    std::map< std::string, std::pair< std::map< std::string, std::vector<std::string> >, time_t> > kit_list;
    readCache(factory_name,dir_name);
    registerKits(kit_list);
  }
*/
  void PluginCache::registerKits(std::map< std::string, std::pair< std::map< std::string, std::vector<std::string> >, time_t> > &kit_list)
  {
    for( std::map< std::string, std::pair< std::map< std::string , std::vector<std::string> >, time_t>  >::const_iterator lib_iter =  kit_list.begin();
	 lib_iter != kit_list.end();
	 ++lib_iter) {
      for( std::map< std::string , std::vector<std::string> >::const_iterator factory_iter =  lib_iter->second.first.begin();
	   factory_iter != lib_iter->second.first.end();
	   ++factory_iter) {
	for (std::vector<std::string>::const_iterator  kit_iter = factory_iter->second.begin();
	     kit_iter != factory_iter->second.end();
	     ++kit_iter) {

	  std::map<std::string, FactoryBase *>::iterator the_factory = m_factoryList.find(factory_iter->first);
	  if (the_factory != m_factoryList.end()) {
	    the_factory->second->registerKit(*kit_iter,lib_iter->first);
	  }

	}
      }
    }
    
  }

  bool PluginCache::readCache(const std::string &factory_name,
			      const std::string &dir_name,
			      std::map< std::string, std::pair< std::map< std::string, std::vector<std::string> >, time_t> > &kit_list,
			      const std::string &search_kit_name) {

    bool ret=false;
    DEBUG_TRACE(  std::cout << "INFO [PluginCache::readCache]  " << factory_name <<  std::endl; )
    std::ifstream in( DirLister::createPath(dir_name,s_cacheFileName).c_str() );
    while (in) {
      std::string id;
      std::string name;
      std::string factory;
      std::string lib;
      ::time_t last_mod_time;
      in >> id;
      if (id == "module") {
	in >> lib;
	in >> last_mod_time;
	kit_list[lib].second=last_mod_time;
	while (in) {
	  in >> id;
	  if (id == "begin") {
	    while (in) {
	      in >> factory;
	      if (factory == "end") break;
	      in >> name;
	      if (factory_name.empty() || factory_name == factory) {
		DEBUG_TRACE( std::cout << " INFO [PluginCache::readCache] " << name << " : " << factory << " : " << lib << std::endl; )
		std::map<std::string, FactoryBase *>::iterator factory_iter = m_factoryList.find(factory);
		if (factory_iter != m_factoryList.end()) {
		  if (name == search_kit_name) {
		    ret=true;
		  }
		  kit_list[lib].first[factory].push_back(name);
		  DEBUG_TRACE( std::cout << " INFO [PluginCache::readCache] register " << name <<  " -> " << lib << " with " << factory << std::endl; )
		  //		  factory_iter->second->registerKit(name,lib);
		}
	      }
	    }
	    break;
	  }
	}
      }
    }
    return ret;
  }

  bool PluginCache::searchPlugin(const std::string &kit_name) {
    DEBUG_TRACE( std::cout << "INFO [PluginCache::searchPlugin] kit = " << kit_name << std::endl; )

    std::vector<std::string> env_names;
    env_names.push_back("CAN_PLUGIN_PATH");
    env_names.push_back("SEAL_PLUGINS");
    for (std::vector<std::string>::const_iterator env_iter = env_names.begin();
	 env_iter != env_names.end();
	 ++env_iter) {

      const char *env = ::getenv(env_iter->c_str() );

      if (env) {
	std::string dir_list(env);
	std::string::size_type start=0;
	while (start < dir_list.size()) {
	  std::string::size_type end=dir_list.find(":",start);
	  if (end==std::string::npos) {
	    end=dir_list.size();
	  }
	  DEBUG_TRACE( std::cout << "INFO [PluginCache::searchPlugin] kit = " << kit_name << " in " << dir_list.substr(start,end-start) << std::endl; )

	  if ( analyseDir(dir_list.substr(start,end-start), kit_name) && !kit_name.empty()) {
	    DEBUG_TRACE( dumpKitList(); )
	    return true;
	  }

	  start=end+1;
	}

	break;
      }
    }
    DEBUG_TRACE( dumpKitList(); )
    return false;
  }

  void PluginCache::readReg(const std::string &name, std::vector<std::string> &plugin_list) {
    DEBUG_TRACE( std::cout << "INFO [PluginCache::readReg]  " << name <<  std::endl; )
    std::ifstream in( name.c_str() );
    while (in) {
      std::string id;
      std::string lib;
      in >> id;
      if (id == "module") {
	in >> lib;
	if (!lib.empty()) {
	  plugin_list.push_back(lib);
	}
      }
    }
  }

  bool PluginCache::analyseDir(const std::string &dir_name, const std::string &kit_name) {
    DEBUG_TRACE( std::cout << "INFO [PluginCache::analyseDir] " << dir_name << " search for " << kit_name << "." << std::endl; )

    std::map< std::string, std::pair< std::map< std::string, std::vector<std::string> >, time_t> > cached_kit_list;
    std::map< std::string, time_t> plugin_list;
    bool ret = readCache("", dir_name,cached_kit_list, kit_name);

//     std::map< std::string, std::map< std::string, std::vector<std::string> > > kit_list;
//     for (std::map<std::string, FactoryBase *>::const_iterator factory_iter = m_factoryList.begin();
// 	 factory_iter != m_factoryList.end();
// 	 ++factory_iter) {
//       factory_iter->second->kitList(kit_list);
//     }

    std::unique_ptr<IDirIter> iter(DirLister::fileIterator(dir_name, s_moduleRegistryPattern ));
    std::set<std::string> unload_lib;
    bool need_to_update_cache_file=false;
    while (iter->next()) {
      time_t cache_mod_time = DirLister::lastModificationTime(dir_name,s_cacheFileName);
	// it takes some time to write the chache file so the reg files may have changed in between
	std::vector<std::string> a_plugin_list;
	readReg(DirLister::createPath(dir_name,iter->name()), a_plugin_list);

	for (std::vector<std::string>::const_iterator plugin_iter = a_plugin_list.begin();
	     plugin_iter != a_plugin_list.end();
	     ++plugin_iter) {

	  if (cache_mod_time==0
              || cache_mod_time-60 < iter->lastModificationTime()
              || cache_mod_time-60 < DirLister::lastModificationTime(dir_name,*plugin_iter)
              || cached_kit_list.find(*plugin_iter)==cached_kit_list.end() ) {
	  plugin_list[*plugin_iter] = iter->lastModificationTime();
	  DEBUG_TRACE( std::cout << "INFO [PluginCache::analyseDir] scan " << *plugin_iter  << " last mod time = " << plugin_list[*plugin_iter] << std::endl; )
	  scanLibrary(*plugin_iter, false);
	  unload_lib.insert(*plugin_iter);
          need_to_update_cache_file=true;
	}
      }
    }

    std::map< std::string, std::map< std::string, std::vector<std::string> > > new_kit_list;
    for (std::map<std::string, FactoryBase *>::const_iterator factory_iter = m_factoryList.begin();
	 factory_iter != m_factoryList.end();
	 ++factory_iter) {
      factory_iter->second->kitList(new_kit_list);
    }

    for( std::map< std::string, std::map< std::string , std::vector<std::string> > >::const_iterator lib_iter =  new_kit_list.begin();
	 lib_iter != new_kit_list.end();
	 ++lib_iter) {
      bool has_kit=false;

      std::pair< std::map< std::string, std::vector<std::string> >, time_t> &lib_kit_list = cached_kit_list[lib_iter->first];

      std::map< std::string, time_t>::const_iterator plugin_iter =  plugin_list.find(lib_iter->first);
      if (plugin_iter == plugin_list.end())  continue;

      lib_kit_list.second = plugin_iter->second;
      lib_kit_list.first.clear();

      for( std::map< std::string , std::vector<std::string> >::const_iterator factory_iter =  lib_iter->second.begin();
	   factory_iter != lib_iter->second.end();
	   ++factory_iter) {
	if (!factory_iter->second.empty()) {
	  std::vector<std::string> &factory_kit_list = lib_kit_list.first[factory_iter->first];
	  
	  std::set<std::string> temp_kit_list;
	  for(std::vector<std::string>::const_iterator kit_iter = factory_kit_list.begin();
	      kit_iter != factory_kit_list.end();
	      ++kit_iter) {
	    temp_kit_list.insert(*kit_iter);
	  }
	  factory_kit_list.clear();

	  for (std::vector<std::string>::const_iterator  kit_iter = factory_iter->second.begin();
	       kit_iter != factory_iter->second.end();
	       ++kit_iter) {
	    temp_kit_list.insert(*kit_iter);
	  }
	  for(std::set<std::string>::const_iterator kit_iter = temp_kit_list.begin();
	      kit_iter != temp_kit_list.end();
	      ++kit_iter) {
	    if (*kit_iter == kit_name ) {
	      DEBUG_TRACE( std::cout << "INFO [PluginCache::analyseDir]  " << dir_name << " -> " << lib_iter->first << " contains " << kit_name << "." << std::endl; )
	      has_kit = true;
	    }

	    factory_kit_list.push_back(*kit_iter);
	  }	  
	}
      }
      if (has_kit) ret=true;
      else {
	if (unload_lib.find(lib_iter->first) != unload_lib.end()) {
	  DEBUG_TRACE( std::cout << "INFO [PluginCache::analyseDir] try to unload " << dir_name << " -> " << lib_iter->first <<  "." << std::endl; )
	  unloadLibrary(lib_iter->first);
	}
      }
    }

    
    if (need_to_update_cache_file) {
       DEBUG_TRACE( std::cout << "INFO [PluginCache::analyseDir] Write cache " << dir_name << std::endl; )
       writeCache(dir_name,cached_kit_list);
    }
    registerKits(cached_kit_list);

    return ret;
  }


}
