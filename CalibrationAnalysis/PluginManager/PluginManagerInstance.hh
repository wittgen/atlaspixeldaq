/* Dear emacs, this is -*-c++-*- */
#ifndef _CAN_PluginManagerInstance_h_
#define _CAN_PluginManagerInstance_h_

#include <IPluginManager.hh>

namespace CAN {

  class PluginManagerInstance
  {
  public:
    static IPluginManager *instance();

  private:
    static IPluginManager *s_instance;
  };

}
#endif
