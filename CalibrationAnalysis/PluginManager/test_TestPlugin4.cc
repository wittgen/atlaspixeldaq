#include "test_ITestPlugin.hh"
#include "FactoryA1.hh"
#include <iostream>

namespace CAN {

class TestPlugin4 : public ITestPlugin
{
public:

  TestPlugin4(const std::string &name)
    : m_name(name)
  {}
  
  void execute() {
    std::cout << "INFO [TestPlugin4::execute] Plugin IV : " << m_name  << std::endl;
  }
private:
  std::string m_name;
};

typedef CAN::FactoryA1<CAN::ITestPlugin,std::string> Factory;
CAN_REGISTER_OBJECT(Factory, TestPlugin4)

}
