/* Dear emacs, this is -*-c++-*- */
#ifndef _test_ITestPlugin_h_
#define _test_ITestPlugin_h_

namespace CAN {
  class ITestPlugin
  {
  public:
    virtual ~ITestPlugin()  {}
    virtual void execute() = 0;
  };
}
#endif
