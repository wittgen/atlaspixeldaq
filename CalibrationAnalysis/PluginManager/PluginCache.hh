/* Dear emacs, this is -*-c++-*- */
#ifndef _CAN_PluginCache_hh_
#define _CAN_PluginCache_hh_

#include <string>
#include <map>
#include <vector>

namespace CAN {
  class FactoryBase;

class PluginCache
{
public:
  PluginCache();
  ~PluginCache();

  void registerFactory(FactoryBase *factory);
  void deregisterFactory(FactoryBase *factory);

  //void readCache(const std::string &factory_name, const std::string &dir_name);

  bool readCache(const std::string &factory_name,
			      const std::string &dir_name,
			      std::map< std::string, std::pair< std::map< std::string, std::vector<std::string> >, time_t> > &kit_list,
			      const std::string &search_kit_name="");

  void registerKits(std::map< std::string, std::pair< std::map< std::string, std::vector<std::string> >, time_t> > &kit_list);
  
  void dumpKitList();

//   static PluginCache *instance() {
//     if (!s_instance) {
//       s_instance=new PluginCache;
//     }
//     return s_instance;
//   }
  
  void scanLibrary(const std::string &name, bool unload_libraries=true);
  void unloadLibrary(const std::string &name);

  void searchPlugins() { searchPlugin(""); }
  bool searchPlugin(const std::string &kit_name);

protected:
  bool analyseDir(const std::string &dir_name, const std::string &kit_name);
  void readReg(const std::string &name, std::vector<std::string> &plugin_list);

  void writeCache(const std::string &dir_name,
		  std::map< std::string, std::pair< std::map< std::string, std::vector<std::string> >, time_t> > &kit_list);

private:
  std::map<std::string, FactoryBase *> m_factoryList;

  static const char *s_cacheFileName;
  static const char *s_cacheFileTempPrefix;
  static const char *s_moduleRegistryPattern;
//  static PluginCache *s_instance;
};


}
#endif
