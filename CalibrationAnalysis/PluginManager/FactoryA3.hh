/* Dear emacs, this is -*-c++-*- */
#ifndef _CAN_FactoryA3_h_
#define _CAN_FactoryA3_h_

#include "FactoryBase.hh"
#include <cassert>

#ifdef DEBUG
#  include <iostream>
#  define DEBUG_TRACE(a) { a }
#else
#  define DEBUG_TRACE(a) {}
#endif

namespace CAN {

  // two argument constructor kits and corresponding factory

  /** Interface of a kit which creates objects with a two argument constructor
   */


  /** 
   * Somewhere in the code the factory needs to be instantiated:
   * FACTORY_INSTANCE( FactoryA3<MyClass, MyArg1, MyArg2> )
   */
  template <class T_base, class T_arg1, class T_arg2, class T_arg3>
  class FactoryA3Base : public FactoryBase
  {
  public:
    FactoryA3Base(const std::string &name) : m_name(name) { registerMe(); }

    /** Abstract kit interface.
     */
    class IFactoryKit : public IObjectKit
    {
    public: 
      virtual T_base *create(T_arg1 a1, T_arg2 a2, T_arg3 a3) const = 0;
    };

    /** Specific kit implementation
     */
    template <class T_object> 
    class FactoryKit : public IFactoryKit
    {
    public: 
      virtual T_base *create(T_arg1 a1, T_arg2 a2, T_arg3 a3) const
      { return new T_object(a1, a2,a3); }
    };


    bool registerKit(const std::string &name, IFactoryKit *kit) {
      return _registerKit(name, kit);
    }

    template <class T_object>
    bool registerKit(const std::string &name) {
      DEBUG_TRACE( std::cout << "INFO [FactoryA3::registerKit] " << name << " with " << this->name() << std::endl; )
      return registerKit(name, new FactoryKit<T_object> );
    }

    virtual T_base *create(const std::string &name, T_arg1 a1, T_arg2 a2, T_arg3 a3) {
      const KitInfo &kit_info = _getKit(name);
      assert(dynamic_cast< const IFactoryKit *>(kit_info.kit()) );
      return static_cast< const IFactoryKit *>(kit_info.kit())->create(a1,a2,a3);
    }

    const std::string &name() const {
      return m_name;
    }

  private:
    std::string m_name;
  };

  template <class T_base, class T_arg1, class T_arg2, class T_arg3>
  class FactoryA3 : public FactoryA3Base<T_base, T_arg1, T_arg2, T_arg3>
  {
  public:
    FactoryA3() : FactoryA3Base<T_base, T_arg1, T_arg2, T_arg3>(s_instanceName) {}

    static FactoryA3<T_base, T_arg1, T_arg2, T_arg3> *get() {
      if (!s_instance) {
	s_instance = new FactoryA3<T_base, T_arg1, T_arg2, T_arg3>;
	DEBUG_TRACE( std::cout << "INFO [FactoryA3::get] create new instance for " << s_instanceName << std::endl; )
      }
      return s_instance;
    }

  private:
    static FactoryA3  *s_instance;
    static const char *s_instanceName;
  };


}
#undef DEBUG_TRACE
#endif
