#include "PluginManagerInstance.hh"
#include "PluginManager.hh"

namespace CAN {
  IPluginManager *PluginManagerInstance::s_instance=NULL;

  IPluginManager *PluginManagerInstance::instance(){
    if (!s_instance) {
      s_instance = new PluginManager;
    }
    return s_instance;
  }

}
