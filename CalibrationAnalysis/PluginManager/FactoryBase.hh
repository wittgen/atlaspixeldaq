/* Dear emacs, this is -*-c++-*- */
#ifndef _CAN_FactoryBase_h_
#define _CAN_FactoryBase_h_

#include <iostream>
#include <map>
#include <vector>

// #define TMPL_FACTORY_INSTANCE(a)  template <>  a *a::s_instance = NULL;

// create an instance of a factory : the factory class is a template FactoryA2<int,char> :
//
//   typedef FactoryA2<int,char> MyFactory;
//   template <> CAN_FACTORY_INSTANCE( MyFactory )
//
//  or 
//
//   template <> CAN_FACTORY_NAMED_INSTANCE( MyFactory, "MyFactory" )
// 
// to register an object :
//
//   typedef CAN::FactoryA2<CAN::ITestPlugin,std::string,unsigned int> MyFactory;
//   CAN_REGISTER_NAMED_OBJECT(MyFactory, CAN::TestPlugin, "TestPlugin")
//
// or
//
//   typedef CAN::FactoryA2<CAN::ITestPlugin,std::string,unsigned int> MyFactory;
//   namespace CAN {
//      CAN_REGISTER_OBJECT(MyFactory, TestPlugin)
//   }

#define CAN_FACTORY_INSTANCE(factory)  template <> factory *factory::s_instance = NULL ; template <> const char *factory::s_instanceName = #factory ;
// #define CAN_FACTORY_NAMED_INSTANCE(factory,name)  CAN::NamedInstance<factory> factory::s_instance(name) ;

#define CAN_MK_INIT_NAME(a) CAN_MK_INIT_NAME2(_init, a)
#define CAN_MK_INIT_NAME2(a,b) a ## b

#define CAN_REGISTER_NAMED_OBJECT(factory, object_name, register_name) \
namespace { bool CAN_MK_INIT_NAME(__LINE__) = factory ::get()->registerKit<object_name>(register_name); }

#define CAN_REGISTER_OBJECT(factory, object_name) \
namespace { bool CAN_MK_INIT_NAME(__LINE__) = factory ::get()->registerKit<object_name>( #object_name ); }

#include <string>
#include <cassert>
#include <map>
#include <stdexcept>

namespace CAN {
  /** Base kit class.
   * @todo useful ?
   */
  class IObjectKit
  {
  public:
    virtual ~IObjectKit() {}
    //    virtual T_base *create() = 0;
  };


  /** Necessary information to create an object.
   * Also the information needed to load a shared library is stored.
   */
  class KitInfo {
  public:
    KitInfo() : m_kit(NULL) { }

    KitInfo(IObjectKit *kit) : m_kit(kit) { assert(kit); }
    KitInfo(IObjectKit *kit, const std::string &scan_library) : m_kit(kit),m_libraryName(scan_library) { assert(kit); }
    KitInfo(const std::string &library_name) : m_kit(NULL), m_libraryName(library_name) {  }
    ~KitInfo() { }

    bool hasKit() const { return m_kit; }
    const IObjectKit *kit() const { return m_kit; }

    void setKit(IObjectKit *kit)  { 
      assert( !m_kit);
      m_kit=kit; 
    }

    const std::string &libraryName() const {return m_libraryName; }
    void setLibraryName(const std::string &library_name)  { m_libraryName = library_name; }

    bool canLoad() const { return !m_libraryName.empty(); }

    void prepareUnload() {
      if (canLoad()) {
	delete m_kit;
	m_kit=0;
      }
    }

  private:
    IObjectKit *m_kit;
    std::string m_libraryName;
  };


  class MissingKitException : public std::runtime_error
  {
  public:
    MissingKitException(const std::string &name) : std::runtime_error(makeText(name)) {}

    static std::string makeText(const std::string &name) {
      return std::string("No Kit named : ")+name+".";      
    }
  };

  template <class T>
  class NamedInstance 
  {
  public:
    NamedInstance(const char *name) : m_instance(NULL), m_name(name) { std::cout << "INFO [NamedInstance::ctor] init " << m_name << std::endl; }
    
    T *instance() { return m_instance;}
    T *instance(T *new_instance) { delete m_instance; m_instance=new_instance; return m_instance; }

    const std::string &name() { return m_name; }

  private:
    T *m_instance;
    std::string m_name;
  };

  class FactoryBase 
  {
    typedef std::map<std::string, KitInfo > KitList_t;

  protected:
    FactoryBase();

    void registerMe();

    virtual ~FactoryBase();

    const KitInfo &_getKit(const std::string &name);

    bool _registerKit(const std::string &name, IObjectKit *kit);

  public:
    bool registerKit(const std::string &name, const std::string &library_name);

    void dumpKits() const;

    void kitList(std::map< std::string, std::map< std::string, std::vector<std::string> > > &kit_names) const;

    void setScanLibrary(const std::string &library_name) { m_scanLibrary=library_name; }

    void clearScanLibrary() {m_scanLibrary=""; }

    void prepareForUnloading(const std::string &library_name);

    virtual const std::string &name() const = 0;

  private:
    KitList_t   m_kitList;
    std::string m_scanLibrary;
  };



}
#endif
