#include "FactoryA1.hh"
#include "FactoryA2.hh"
#include "test_ITestPlugin.hh"
#include "PluginManagerInstance.hh"

#include <memory>
#include <exception>
#include <iostream>
#include <vector>

typedef CAN::FactoryA1<CAN::ITestPlugin,std::string> TestFactoryA1;
typedef CAN::FactoryA2<CAN::ITestPlugin,std::string,unsigned int> TestFactoryA2;

CAN_FACTORY_INSTANCE( TestFactoryA1 )
CAN_FACTORY_INSTANCE( TestFactoryA2 )

int main(int argc, char **argv)
{
  std::string arg1="dummy";
  unsigned int arg2=4711;
  TestFactoryA1::get()->dumpKits();
  std::vector<std::string> plugin_names;

  //  std::auto_ptr<CAN::PluginManager>  pm;
  //  CAN::PluginCache::instance()->readCache();

    for (int arg_i=1; arg_i<argc; arg_i++) {
      bool will_fail=false;
      if (strcmp(argv[arg_i],"--fail")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	will_fail=true;
	++arg_i;
      }
      std::string name(argv[arg_i]);
      try {
	std::cout << "INFO [main] *** Try to create "  << name << " ***" << std::endl;
	unsigned int a_arg = arg_i;
	std::auto_ptr<CAN::ITestPlugin> test( TestFactoryA1::get()->create(name ,std::string("Should execute : ")+name) );
	if (test.get()) {
	  test->execute();
	}
	if (will_fail) {
	  std::cerr << "ERROR [main] Expected execution of " << name << " to fail but it did not." << std::endl;
	}
      }
      catch (std::exception &err) {
	if (will_fail) {
	  std::cerr << "INFO [main] Expected failure and caught excetpion : " << err.what() << std::endl;
	}
	else {
	  std::cerr << "ERROR [main] Caught excetpion : " << err.what() << std::endl;
	}
      }

      try {
	std::cout << "INFO [main] *** Try to create "  << name << " ***" << std::endl;
	unsigned int a_arg = arg_i;
	std::auto_ptr<CAN::ITestPlugin> test( TestFactoryA2::get()->create(name ,std::string("Should execute : ")+name, a_arg) );
	if (test.get()) {
	  test->execute();
	}
	if (will_fail) {
	  std::cerr << "ERROR [main] Expected execution of " << name << " to fail but it did not." << std::endl;
	}
      }
      catch (std::exception &err) {
	if (will_fail) {
	  std::cerr << "INFO [main] Expected failure and caught excetpion : " << err.what() << std::endl;
	}
	else {
	  std::cerr << "ERROR [main] Caught excetpion : " << err.what() << std::endl;
	}
      }
    }

  CAN::PluginManagerInstance::instance()->dumpCache();
  return 0;
}
