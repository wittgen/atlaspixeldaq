#ifndef _CAN_IObjectConfigDb_hh_
#define _CAN_IObjectConfigDb_hh_

#include <string>

namespace PixLib {
  class Config;
}

namespace CAN {

  class ObjectInfo_t;

  /** Interface for a configuration database.
   */
  class IObjectConfigDb {
  public:
    virtual ~IObjectConfigDb() {}
      virtual void readConfig(const ObjectInfo_t &object_info, PixLib::Config &config, std::string string="") = 0;
  };

}

#endif
