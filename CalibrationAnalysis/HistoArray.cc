#include "HistoArray.hh"
#include "HistoDef.hh"

namespace CAN {

  unsigned long AccountingHistoPtr_t::s_memoryUsed = 0L;

  void AccountingHistoPtr_t::reset() { s_memoryUsed -= usedMemory(); delete m_ptr; m_ptr=NULL;}

  HistoArray::~HistoArray() {
    for (std::vector<AccountingHistoPtr_t>::iterator histo_iter=m_histo.begin();
	 histo_iter != m_histo.end();
	 histo_iter++) {
      histo_iter->reset();
    }
    m_histo.clear();

    for (std::vector<HistoArray *>::iterator child_iter=m_child.begin();
	 child_iter != m_child.end();
	 child_iter++) {
      delete *child_iter;
      *child_iter=NULL;
    }
    m_child.clear();
  }


  void HistoArray::addHisto(const PixA::Index_t &index, unsigned int level, HistoPtr_t histo_ptr) {
    // paranoia check
    assert(level < index.size());
    // check whether the index looks resonable
    assert(index.size()+1000 > index[level]);

    if (level+1 == index.size()) {

      if (index[level]+1>m_histo.size()) {
	m_histo.resize(index[level]+1,NULL);
      }
      m_histo[ index[level] ].reset();
      m_histo[ index[level] ]=histo_ptr;
    }
    else {
      if (index[level]+1>m_child.size()) {
	//      unsigned int old_size=m_child.size();
	m_child.resize(index[level]+1,NULL);
      }
      if (!m_child[ index[level] ]) {
	m_child[ index[level] ]=new HistoArray(m_level+1);
      }
      m_child[ index[level] ]->addHisto(index,level+1, histo_ptr);
    }
  }

}
