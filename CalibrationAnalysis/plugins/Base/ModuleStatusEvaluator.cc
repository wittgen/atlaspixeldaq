#include "ModuleStatusEvaluator.hh"
#include <ExtendedAnalysisResultList_t.hh>
#include <ConfigWrapper/Connectivity.h>
#include <IDataServer.hh>
#include <IAnalysisDataServer.hh>

#include <for_each.hh>

namespace CAN {

  ModuleStatusEvaluator::ModuleStatusEvaluator(SerialNumber_t serial_number, const ObjectInfo_t &)
    : m_config("ModuleStatusEvaluator"),
      m_moduleStatusVariableName("Status"),
      m_orModuleStatus(false)
  {
    {
      m_config.addGroup("Rules");
      PixLib::ConfGroup &test_group=m_config["Rules"];
      test_group.addString("ModuleStatusVarName", m_moduleStatusVariableName, m_moduleStatusVariableName, "Name of the boolean status variable.", true);
      test_group.addBool("OneModulePassedIsSuccess", m_orModuleStatus, false, "Success, if at least one Module has passed.", true);
      test_group.addBool("ConsiderFailed", m_considerFailedInstead, false, "Consider failed analyses instead of succeeded ones.", true);
    }
  }

  class ModuleStatusEvaluatorHelper {
  public:
    ModuleStatusEvaluatorHelper( const ModuleStatusEvaluator &status_evaluator,
			      const ExtendedAnalysisResultList_t &analysis_results)
      : m_statusEvaluator(&status_evaluator),
	m_analysisResults( &analysis_results),
	m_combinedStatus( !m_statusEvaluator->m_orModuleStatus ),
	m_hasModule(false) 
    { assert(m_statusEvaluator); assert(m_analysisResults); }

    void operator()( const PixA::ModuleList::const_iterator &module_iter) {
      bool status = m_statusEvaluator->m_considerFailedInstead;
      try {
	status = m_analysisResults->value<bool>(m_statusEvaluator->m_moduleStatusVariableName, PixA::connectivityName(module_iter) );
	if (m_statusEvaluator->m_considerFailedInstead) {
          status = !status;
        }
	m_hasModule=true;
      }
      catch (AnalysisResultValueNotFound &err) {
      }

      if (m_statusEvaluator->m_orModuleStatus) {
	m_combinedStatus |= status;
      }
      else {
	m_combinedStatus &= status;
      }
    }

    bool status() const {
      return m_combinedStatus & m_hasModule;
    }

  private:
    const ModuleStatusEvaluator *m_statusEvaluator;
    const ExtendedAnalysisResultList_t *m_analysisResults;
    bool m_combinedStatus;
    bool m_hasModule;
  };

  bool ModuleStatusEvaluator::classify(IAnalysisDataServer *data_server,
				    const PixA::RODLocationBase &rod_location,
				    ExtendedAnalysisResultList_t &analysis_results) {
    assert(data_server);

    // get default data server
    IDataServer *scan_data_server = data_server->getDataServer();
    assert(scan_data_server);

    ModuleStatusEvaluatorHelper module_status_evaluator(*this, analysis_results);
    for_each_module(data_server->connectivity(),
		 rod_location,
		 scan_data_server->getPp0DisabledList(),
		 module_status_evaluator);

    return module_status_evaluator.status();
  }

}
