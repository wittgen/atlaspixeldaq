#ifndef _CAN_StatusEvaluator_hh_
#define _CAN_StatusEvaluator_hh_

#include "StatusEvaluatorBase.hh"
#include <Config/Config.h>

namespace CAN {

  class ObjectInfo_t;

  class StatusEvaluator : public StatusEvaluatorBase
  {
  public:
    StatusEvaluator(SerialNumber_t serial_number, const ObjectInfo_t &);

    bool classify(IAnalysisDataServer *data_server,
		  const PixA::RODLocationBase &rod_location,
		  ExtendedAnalysisResultList_t &analysis_results);

    PixLib::Config &config() { return m_config; }

  private:
    PixLib::Config m_config;

    std::string m_statusVariableName;
    bool  m_considerFailedInstead;
  };

}

#endif
