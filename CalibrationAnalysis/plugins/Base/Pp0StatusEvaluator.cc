#include "Pp0StatusEvaluator.hh"
#include <ExtendedAnalysisResultList_t.hh>
#include <ConfigWrapper/Connectivity.h>
#include <IDataServer.hh>
#include <IAnalysisDataServer.hh>

#include <for_each.hh>

namespace CAN {

  Pp0StatusEvaluator::Pp0StatusEvaluator(SerialNumber_t serial_number, const ObjectInfo_t &)
    : m_config("Pp0StatusEvaluator"),
      m_pp0StatusVariableName("Status"),
      m_orPp0Status(false)
  {
    {
      m_config.addGroup("Rules");
      PixLib::ConfGroup &test_group=m_config["Rules"];
      test_group.addString("Pp0StatusVarName", m_pp0StatusVariableName, m_pp0StatusVariableName, "Name of the boolean status variable.", true);
      test_group.addBool("OnePp0PassedIsSuccess", m_orPp0Status, false, "Success, if at least one Pp0 has passed.", true);
      test_group.addBool("ConsiderFailed", m_considerFailedInstead, false, "Consider failed analyses instead of succeeded ones.", true);
    }
  }

  class Pp0StatusEvaluatorHelper {
  public:
    Pp0StatusEvaluatorHelper( const Pp0StatusEvaluator &status_evaluator,
			      const ExtendedAnalysisResultList_t &analysis_results)
      : m_statusEvaluator(&status_evaluator),
	m_analysisResults( &analysis_results),
	m_combinedStatus( !m_statusEvaluator->m_orPp0Status ),
	m_hasPp0(false) 
    { assert(m_statusEvaluator); assert(m_analysisResults); }

    void operator()( const PixA::Pp0List::const_iterator &pp0_iter) {
      bool status = false;
      try {
	status = m_analysisResults->value<bool>(m_statusEvaluator->m_pp0StatusVariableName, PixA::connectivityName(pp0_iter) );
	m_hasPp0=true;
      }
      catch (AnalysisResultValueNotFound &err) {
      }

      if (m_statusEvaluator->m_orPp0Status) {
	m_combinedStatus |= status;
      }
      else {
	m_combinedStatus &= status;
      }
    }

    bool status() const {
      return m_combinedStatus & m_hasPp0;
    }

  private:
    const Pp0StatusEvaluator *m_statusEvaluator;
    const ExtendedAnalysisResultList_t *m_analysisResults;
    bool m_combinedStatus;
    bool m_hasPp0;
  };

  bool Pp0StatusEvaluator::classify(IAnalysisDataServer *data_server,
				    const PixA::RODLocationBase &rod_location,
				    ExtendedAnalysisResultList_t &analysis_results) {
    assert(data_server);

    // get default data server
    IDataServer *scan_data_server = data_server->getDataServer();
    assert(scan_data_server);

    Pp0StatusEvaluatorHelper pp0_status_evaluator(*this, analysis_results);
    for_each_pp0(data_server->connectivity(),
		 rod_location,
		 scan_data_server->getPp0DisabledList(),
		 pp0_status_evaluator);

    return pp0_status_evaluator.status();
  }

}
