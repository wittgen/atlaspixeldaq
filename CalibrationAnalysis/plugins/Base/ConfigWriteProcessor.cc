#include "ConfigWriteProcessor.hh"
#include <ExtendedAnalysisResultList_t.hh>
#include <IAnalysisDataServer.hh>
#include <sendMessage.hh>
#include <exception.hh>

#include <ConfigWrapper/Connectivity.h>
#include <ConfigWrapper/ConnObjConfigDb.h>

#include <PixModule/PixModule.h>
#include <PixModuleGroup/PixModuleGroup.h>

#include <sstream>
#include <fstream>

#include <time.h>
#include <algorithm>
#include <iomanip>

namespace CAN {

  ConfigWriteProcessor::ConfigWriteProcessor(SerialNumber_t serial_number, const ObjectInfo_t &)
    : m_serialNumber(serial_number),
      m_logLevel(kWarnLog),
      m_config("ConfigWriteProcessor") 
  {
    {
      m_config.addGroup("Tags");
      PixLib::ConfGroup &tags_group=m_config["Tags"];
      tags_group.addBool("CloneTags", m_cloneTag, true, "Clone the corresponding cfg tags (only if default is not marked)", true);
      tags_group.addBool("UseDefaultTags", m_defaultTag, true, "Use the tags of the scan", true);
      tags_group.addString("CustomIdTag", m_customIdTag, m_customIdTag, "The name of a custom ID tag", true);
      tags_group.addString("CustomTag", m_customTag, m_customTag, "The name of a custom configuration tag", true);
      tags_group.addString("CustomModuleTag", m_customModuleTag, m_customModuleTag, "The name of a custom module configuration tag", true);
    }

  }

  void ConfigWriteProcessor::requestData(IAnalysisDataServer */*data_server*/,
					 const PixA::RODLocationBase & /*rod_location*/) {
    // nothing to be requested
  }

  template <class T>
  std::string pixObjectName(const T &a);

  template<> std::string pixObjectName<PixLib::PixModule>(const PixLib::PixModule &a) {
    return const_cast<PixLib::PixModule &>(a).connName() + "(" +const_cast<PixLib::PixModule &>(a).moduleName() + ")";
  }

  template<> std::string pixObjectName<PixLib::PixModuleGroup>(const PixLib::PixModuleGroup &a) {
    return const_cast<PixLib::PixModuleGroup &>(a).getName() + "(" +const_cast<PixLib::PixModuleGroup &>(a).getRodName() + ")";
  }

  // helper class to store the configs of e.g modules.
  template <class T>
  class StoreConfig : public std::unary_function<typename std::vector< std::shared_ptr<T >  >::iterator, void>
  {
  public:
    StoreConfig(PixA::ConnObjConfigDbTagRef config_tag, const std::string &pending_tag, Revision_t revision,  ELogLevel log_level)
      : m_configTag(config_tag),
	m_pendingTag(pending_tag),
	m_revision(revision),
	m_logLevel(log_level)
    {}

    void operator()(const typename std::shared_ptr<T> &a)  {
      if (m_logLevel>=kDiagnosticsLog) {
	std::cout << "DIAGNOSTICS [StoreConfig::operator()] " << pixObjectName(*a) << std::endl;
      }
      m_configTag.storeConfig(  *a ,
			       m_revision,
			       m_pendingTag,
			       false);
    }

  private:
    PixA::ConnObjConfigDbTagRef m_configTag;
    std::string                 m_pendingTag;
    Revision_t                  m_revision;
    ELogLevel m_logLevel;
  };

  std::string makeAnalysisSerialNumberName(SerialNumber_t analysis_serial_number)
  {
     std::stringstream serial_number_name;
     serial_number_name << 'A' << std::setfill('0') << std::setw(9) << analysis_serial_number;
     return serial_number_name.str();
  }

  void ConfigWriteProcessor::process(IAnalysisDataServer *data_server,
				     const PixA::RODLocationBase &rod_location,
				     ExtendedAnalysisResultList_t &analysis_results) {
    assert(data_server);

    if ( analysis_results.hasModifiedConfigurations()) {
      Revision_t revision = ::time(0);
      std::string pending_tag = makeAnalysisSerialNumberName(m_serialNumber);
      //pending_tag += "_Tmp";

      if (analysis_results.modifiedPixModuleGroupConfig().get()) {

	// store the configuration of one pix module group
	StoreConfig<PixLib::PixModuleGroupData_t> store_module_group(getConfigTag(data_server),
								     pending_tag,
								     revision,
								     m_logLevel);
	store_module_group(analysis_results.modifiedPixModuleGroupConfig());

      }

      if (analysis_results.beginModifiedModuleConfig() != analysis_results.endModifiedModuleConfig() ) {

	// store the configuration of all modules
	std::for_each(analysis_results.beginModifiedModuleConfig(),
		      analysis_results.endModifiedModuleConfig(),
		      StoreConfig<PixLib::PixModuleData_t>(getModuleConfigTag(data_server),
							   pending_tag,
							   revision,
							   m_logLevel) );
      }
    }
  }

  PixA::ConnObjConfigDbTagRef ConfigWriteProcessor::getTag(IAnalysisDataServer *data_server, bool module_config_tag)
  {
    assert(data_server);

      if (!m_defaultTag) {
	PixA::ConnectivityRef conn(data_server->connectivity());
	assert(conn);
	std::string id_tag = m_customIdTag;
	if (id_tag.empty()) {
	  id_tag = conn.tag(PixA::IConnectivity::kId);
	}

	if (id_tag.empty() || (m_customTag.empty() && m_customModuleTag.empty())) {
	  //	  throw MRSException("EmptyDestCfgTags", MRS_FATAL, "CAN::ConfigWriteProcessor::process", "Custom ID or configuration tag is missing." );
	  throw MRSException(PixLib::PixMessages::FATAL, "CAN::ConfigWriteProcessor::process", "Custom ID or configuration tag is missing." );
	}
	PixA::ConnObjConfigDb cfg_db( PixA::ConnObjConfigDb::sharedInstance() );
	return cfg_db.getTag(id_tag, ( (!module_config_tag || m_customModuleTag.empty()) ? m_customTag : m_customModuleTag ),0 );
      }
      else {

	PixA::ConnectivityRef conn( data_server->connectivity() );
	assert( conn );

	if (module_config_tag) {
	  return conn.moduleConfigDb();
	}
	else {
	  return conn.configDb();
	}

      }
    }

}
