#include "NoPostProcessing.hh"
#include <ExtendedAnalysisResultList_t.hh>

#include <sstream>
#include <fstream>

namespace CAN {

  NoPostProcessing::NoPostProcessing(SerialNumber_t /*serial_number*/, const ObjectInfo_t &)
    : m_config("NoPostProcessing") 
  {
  }

  void NoPostProcessing::requestData(IAnalysisDataServer * /*data_server*/,
				       const PixA::RODLocationBase &/*rod_location*/)
  {
    /* Nothing to be requested. */
  }

  void NoPostProcessing::process(IAnalysisDataServer */*data_server*/,
				 const PixA::RODLocationBase &/*rod_location*/,
				 ExtendedAnalysisResultList_t &/*analysis_results*/) {
  }

}
