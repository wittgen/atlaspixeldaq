#ifndef _CAN_OptoLinkConfigWriter_hh_
#define _CAN_OptoLinkConfigWriter_hh_

#include "ConfigWriteProcessor.hh"
#include <map>

namespace CAN {

  class SetBocPixRxSettingsFromAnalysis;
  class SetBocIblRxSettingsFromAnalysis;

  class OptoLinkConfigWriter : public ConfigWriteProcessor
  {
    friend class SetBocPixRxSettingsFromAnalysis;
    friend class SetBocIblRxSettingsFromAnalysis;

  public:
    OptoLinkConfigWriter(SerialNumber_t serial_number, const ObjectInfo_t &obj_info);

    void requestData(IAnalysisDataServer *data_server,
		     const PixA::RODLocationBase &rod_location);

    void process(IAnalysisDataServer *data_server,
		 const PixA::RODLocationBase &rod_location,
		 ExtendedAnalysisResultList_t &analysis_results);

    enum EMccSpeed {kDefault, kMcc40, kMcc80};
  protected:
    std::string m_threshold[2];
    std::string m_delay[2];
    std::string m_msr;
    std::string m_viset;
    std::string m_keepSettingsName;

    float       m_thresholdMin;
    float       m_thresholdMax;
    float       m_delayMin;
    float       m_delayMax;
    float       m_msrMin;
    float       m_msrMax;
    float       m_visetMin;
    float       m_visetMax;
    float       m_visetSpreadMax;
    bool        m_adjustFineDelay;
    bool	m_forcedStorage;
 
    static std::map<std::string, EMccSpeed> s_speedMap;
  };

}

#endif
