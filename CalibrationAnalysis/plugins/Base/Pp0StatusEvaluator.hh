#ifndef _CAN_Pp0StatusEvaluator_hh_
#define _CAN_Pp0StatusEvaluator_hh_

#include "StatusEvaluatorBase.hh"
#include <Config/Config.h>

namespace CAN {

  class ObjectInfo_t;

  class Pp0StatusEvaluatorHelper;

  class Pp0StatusEvaluator : public StatusEvaluatorBase
  {
    friend class Pp0StatusEvaluatorHelper;
  public:
    Pp0StatusEvaluator(SerialNumber_t serial_number, const ObjectInfo_t &);

    bool classify(IAnalysisDataServer *data_server,
		  const PixA::RODLocationBase &rod_location,
		  ExtendedAnalysisResultList_t &analysis_results);

    PixLib::Config &config() { return m_config; }

  private:
    PixLib::Config m_config;

    std::string m_pp0StatusVariableName;
    bool  m_orPp0Status;
    bool  m_considerFailedInstead;
  };

}

#endif
