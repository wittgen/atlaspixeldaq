#include "StatusEvaluator.hh"
#include <ExtendedAnalysisResultList_t.hh>

namespace CAN {

  StatusEvaluator::StatusEvaluator(SerialNumber_t serial_number, const ObjectInfo_t &)
    : m_config("StatusEvaluator"),
      m_statusVariableName("Status")
  {
    {
      m_config.addGroup("Rules");
      PixLib::ConfGroup &test_group=m_config["Rules"];
      test_group.addString("StatusVarName", m_statusVariableName, m_statusVariableName, "Name of the boolean status variable", true);
      test_group.addBool("ConsiderFailed", m_considerFailedInstead, false, "Consider failed analyses instead of succeeded ones", true);
    }
  }

  bool StatusEvaluator::classify(IAnalysisDataServer * /*data_server*/,
				 const PixA::RODLocationBase &rod_location,
				 ExtendedAnalysisResultList_t &analysis_results) {

    bool status=false;
    try {
      status = analysis_results.value<bool>(m_statusVariableName,rod_location);
    }
    catch (AnalysisResultValueNotFound &err) {
    }
    return status ^ m_considerFailedInstead;
  }

}
