#include "OptoLinkConfigWriter.hh"
#include "ExtendedAnalysisResultList_t.hh"
#include <IAnalysisDataServer.hh>
#include <IDataServer.hh>

#include <ConfigWrapper/ConnectivityUtil.h>
#include <ConfigWrapper/PixDisableUtil.h>
#include <ConfigWrapper/ConfObjUtil.h>
#include <for_each.hh>
#include <exception.hh>

#include <PixModuleGroup/PixModuleGroup.h>
#include <PixBoc/PixBoc.h>

namespace CAN {


  std::map<std::string, OptoLinkConfigWriter::EMccSpeed> OptoLinkConfigWriter::s_speedMap;

  OptoLinkConfigWriter::OptoLinkConfigWriter(SerialNumber_t serial_number, const ObjectInfo_t &obj_info) 
    : ConfigWriteProcessor(serial_number, obj_info)
  {
    m_threshold[0]="optimalThreshold_Link1";
    m_threshold[1]="optimalThreshold_Link2";
    m_delay[0]="optimalDelay_Link1";
    m_delay[1]="optimalDelay_Link2";
    m_msr="idealMSR";

    m_thresholdMin=0;
    m_thresholdMax=256;
    m_delayMin=0;
    m_delayMax=25;
    m_msrMin=0;
    m_msrMax=32;
    m_visetMin=0.6;
    m_visetMax=1.5;

    m_viset="bestViset";
    m_keepSettingsName="Status";

    m_visetSpreadMax=.1;

    config().addGroup("Params");
    PixLib::ConfGroup &name_group=config()["Params"];

    name_group.addString("ThresholdNameDTO2",m_threshold[0],m_threshold[0],"The variable name of the best threshold for DTO2.",true);
    name_group.addString("ThresholdNameDTO",m_threshold[1],m_threshold[1],"The variable name of the best threshold for DTO.",true);
    name_group.addString("DelayDTO2",m_delay[0],m_delay[0],"The variable name of the best data delay for DTO2.",true);
    name_group.addString("DelayDTO",m_delay[1],m_delay[1],"The variable name of the best data delay for DTO.",true);
    name_group.addString("MSR",m_msr,m_msr,"The variable name of the ideal MSR",true);
    name_group.addString("Viset",m_viset,m_viset,"The variable name of the best Viset.",true);
    name_group.addString("StatusName",m_keepSettingsName,m_keepSettingsName,"Name of a boolean variable - settings okay, do not change.",true);

    name_group.addFloat("ThresholdMax",m_thresholdMax, m_thresholdMax,"The maximum valid threshold.",true);
    name_group.addFloat("ThresholdMin",m_thresholdMin, m_thresholdMin,"The minimum valid threshold.",true);
    name_group.addFloat("DelayMax",m_delayMax, m_delayMax,"The maximum valid delay.",true);
    name_group.addFloat("DelayMin",m_delayMin, m_delayMin,"The minimum valid delay.",true);
    name_group.addFloat("MsrMax",m_msrMax, m_msrMax,"The maximum valid MSR.",true);
    name_group.addFloat("MsrMin",m_msrMin, m_msrMin,"The minimum valid MSR.",true);
    name_group.addFloat("VisetMax",m_visetMax, m_visetMax,"The maximum valid viset.",true);
    name_group.addFloat("VisetMin",m_visetMin, m_visetMin,"The minimum valid viset.",true);
    name_group.addFloat("VisetSpreadMax",m_visetSpreadMax, m_visetSpreadMax,"The maximum spread of the optimum viset values per module.",true);
    name_group.addBool("adjustFineDelay",m_adjustFineDelay, true,"Adjust BPM-FineDelays in case MSR's are set:",true);
    name_group.addBool("forcedStorage",m_forcedStorage, false,"Force storage of analysis results, even if status ok:",true);
    if (s_speedMap.empty()) {
      s_speedMap["SINGLE_40"]=kMcc40;
      s_speedMap["DOUBLE_40"]=kMcc40;
      s_speedMap["SINGLE_80"]=kMcc80;
      s_speedMap["DOUBLE_80"]=kMcc80;
    }

  }

  void OptoLinkConfigWriter::requestData(IAnalysisDataServer *data_server,
					 const PixA::RODLocationBase & /*rod_location*/) {
    // need entire pix module group
    assert(data_server);

    // get default data server
    IDataServer *scan_data_server = data_server->getDataServer();
    assert(scan_data_server);
    scan_data_server->requestModuleGroupConfig();
    scan_data_server->requestScanConfig();

  }

  /** Helper class to set semi-random tdac values to all front-ends of all modules.
   */
  class SetBocPixRxSettingsFromAnalysis : public ModuleAnalysisBase
  {
  public:

    class Stat_t {
    public:
      Stat_t() : m_n(0), m_sum(0), m_sum2(0) {}

      void add(double val) { m_sum+= val; m_sum2+=val*val; m_n++;}
      void calc() { if (m_n>1) { double mean = m_sum / m_n; m_sum2 = sqrt ( (m_sum2 - mean * m_sum)/ (m_n-1)); m_sum =mean; } }

      double mean() const { return m_sum; }
      double rms() const  { return m_sum2; }
      double n() const    { return m_n; }

    private:
      double m_n;
      double m_sum;
      double m_sum2;
    };

    SetBocPixRxSettingsFromAnalysis( OptoLinkConfigWriter &post_processor,
				  const PixA::RODLocationBase &rod_location,
				  ExtendedAnalysisResultList_t &analysis_results,
				  PixLib::PixModuleGroupData_t &module_group_data,
				  OptoLinkConfigWriter::EMccSpeed speed)
      : ModuleAnalysisBase(rod_location,analysis_results),
	m_postProcessor( &post_processor),
        //m_moduleGroupData(&module_group_data),
	m_pixBoc(module_group_data.getPixBoc()),
	m_speedMode(speed),
	m_moduleGroupIsOk(true),
	m_moduleGroupIsModified(false)
    {}

    ~SetBocPixRxSettingsFromAnalysis() {
     std::cout << "INFO [SetBocPixRxSettingsFromAnalysis::dtor] " <<rodLocation() << " enter." << std::endl;
     if (isModuleGroupModified()) {
	if (m_pixBoc && m_pixBoc->getConfig()) {
	  PixLib::Config *config= m_pixBoc->getConfig();
	  bool viset_error=false;
	  for (unsigned int plugin_i=0; plugin_i<4; plugin_i++) {
	    m_viset[plugin_i].calc();
	    if (   m_viset[plugin_i].n()>0
		&& m_viset[plugin_i].mean()>=m_postProcessor->m_visetMin && m_viset[plugin_i].mean()<m_postProcessor->m_visetMax) {

	      PixLib::Config &rx_config = config->subConfig( rxName(plugin_i) );
	      confVal<float>(rx_config["Opt"][     visetName(m_speedMode) ])=static_cast<float>(m_viset[plugin_i].mean());
	      confVal<float>(rx_config["Opt"][     visetName(OptoLinkConfigWriter::kDefault) ])=static_cast<float>(m_viset[plugin_i].mean());
	      if (m_postProcessor->logLevel()>=kInfoLog) {
		std::cout << "INFO [SetBocPixRxSettingsFromAnalysis::dtor] " <<rodLocation() << " set Opt "
			  << visetName(m_speedMode)
			  << " to " <<  static_cast<float>(m_viset[plugin_i].mean())
							    << " (" << m_viset[plugin_i].mean() << " +- " << m_viset[plugin_i].rms() << ")"<< std::endl;
	      }
	      if (m_viset[plugin_i].rms() > m_postProcessor->m_visetSpreadMax) {
		viset_error=true;
	      }
	    }
	  }

	  if (viset_error) {
	    m_moduleGroupIsOk=false;
	    visetError();
	  }
	}
      }
    }

    bool isModuleGroupOk() {
      return m_moduleGroupIsOk & m_moduleGroupIsModified;
    }

    bool isModuleGroupModified() {
      return m_moduleGroupIsModified;
    }

    void operator()( const PixA::ModuleList::const_iterator &module_iter) {
      try {
	if (m_pixBoc && m_pixBoc->getConfig()) {
	  PixLib::Config *config= m_pixBoc->getConfig();

	  // 	  OptoLinkConfigWriter::EMccSpeed module_speed = OptoLinkConfigWriter::kMcc40;
	  // 	  if (   const_cast<PixLib::ModuleConnectivity *>(*module_iter)->readoutSpeed == PixLib::SINGLE_80
	  // 	      || const_cast<PixLib::ModuleConnectivity *>(*module_iter)->readoutSpeed == PixLib::DOUBLE_80) {
	  // 	    module_speed == OptoLinkConfigWriter::kMcc80;
	  // 	  }

	  PixA::ModulePluginConnection tx_connection = PixA::txConnection(module_iter);
	  PixA::ModulePluginConnection rx_connection_dto1 = PixA::rxConnection(module_iter, PixA::DTO1);
	  PixA::ModulePluginConnection rx_connection_dto2 = PixA::rxConnection(module_iter, PixA::DTO2);
	  PixA::ModulePluginConnection *rx_connection[2];

	  // Note : dto2 -> 0 and dto -> 1
	  rx_connection[0]=&rx_connection_dto2;
	  rx_connection[1]=&rx_connection_dto1;
	  bool error=false;
	  bool config_error=false;

	  if (tx_connection.isValid()) {
            if (resultList().has<float>(m_postProcessor->m_msr, PixA::connectivityName(module_iter) )) {
	      try {
               bool keep_settings=false;
        	if (!m_postProcessor->m_keepSettingsName.empty()) {
                    keep_settings=resultList().value<bool>(m_postProcessor->m_keepSettingsName,PixA::connectivityName(module_iter) );
        	}

        	if (!(keep_settings && !m_postProcessor->m_forcedStorage)) {
		  float msr=resultList().value<float>(m_postProcessor->m_msr, PixA::connectivityName(module_iter) );

		  if (m_postProcessor->logLevel()>=kInfoLog) {

		    std::cout << "INFO [SetBocTxSettingsFromAnalysis::operator()] " << PixA::connectivityName(module_iter)
			      << " msr = " << msr << std::endl;
		  }
		  if (   msr >= m_postProcessor->m_msrMin && msr < m_postProcessor->m_msrMax) {
		    if (m_postProcessor->logLevel()>=kInfoLog) {
		      std::cout << "INFO [SetBocTxSettingsFromAnalysis::operator()] " << PixA::connectivityName(module_iter)
				<< " get tx config : " << txName(tx_connection.plugin()) << std::endl;
		    }
		    PixLib::Config &tx_config = config->subConfig( txName(tx_connection.plugin()) );
		    if (m_postProcessor->logLevel()>=kInfoLog) {
		      std::cout << "INFO [SetBocTxSettingsFromAnalysis::operator()] " << PixA::connectivityName(module_iter)
				<< " get msr : " << msrName(tx_connection.channel()) << std::endl;
		    }
                    int old_msr = confVal<int>(tx_config["Bpm"][ msrName(tx_connection.channel()) ]);
		    confVal<int>(tx_config["Bpm"][ msrName(tx_connection.channel()) ])=static_cast<int>(msr);
                    if (m_postProcessor->m_adjustFineDelay) {
		      if (m_postProcessor->logLevel()>=kInfoLog) {
			std::cout << "INFO [SetBocTxSettingsFromAnalysis::operator()] " << PixA::connectivityName(module_iter)
		                  << ": Re-setting fineDelay" << std::endl;
			std::cout << "INFO [SetBocTxSettingsFromAnalysis::operator()] " << PixA::connectivityName(module_iter)
				  << " get fineDelay : " << fineDelayName(tx_connection.channel()) << std::endl;
		      }
		      int oldFineDelay = confVal<int>(tx_config["Bpm"][ fineDelayName(tx_connection.channel()) ]);
		      int fineDelay = oldFineDelay - (((static_cast<int>(msr)) - old_msr)/2); 
		      confVal<int>(tx_config["Bpm"][ fineDelayName(tx_connection.channel()) ]) = fineDelay;
		      if (m_postProcessor->logLevel()>=kInfoLog) {
			std::cout << "INFO [SetBocTxSettingsFromAnalysis::operator()] " << PixA::connectivityName(module_iter)
				  << " FineDelay was set to: " << oldFineDelay << " Correcting due to msr moving from "
				  << old_msr << " to " << msr << std::endl;
			std::cout << "INFO [SetBocTxSettingsFromAnalysis::operator()] " << PixA::connectivityName(module_iter)
				  << " FineDelay is now set to: " << fineDelay << std::endl;
		      }
		    }
		    if (m_postProcessor->logLevel()>=kInfoLog) {
		      std::cout << "INFO [SetBocTxSettingsFromAnalysis::operator()] " << PixA::connectivityName(module_iter)
				<< " set General "
				<< msrName(tx_connection.channel())
				<< " to " <<  static_cast<int>( msr ) << " ( " 
				<< m_postProcessor->m_msr << " = " << msr << ")"
				<< std::endl;
		    }

		    m_moduleGroupIsModified=true;
		  }
		  else {
		    config_error=true;
		  }
		}
	      }
	      catch(PixA::ConfigException &err) {
		config_error=true;
	      }
	      catch(...) {
		std::cout << "INFO [SetBocTxSettingsFromAnalysis::operator()] " << PixA::connectivityName(module_iter) 
			  << " caught an unidentified exception" << std::endl;
	      }
            } else {
              if (m_postProcessor->logLevel()>=kInfoLog) {
		std::cout << "INFO [SetBocTxSettingsFromAnalysis::operator()] " << PixA::connectivityName(module_iter)
			  << " has no MSR tuned value in ResultList, skipping" << std::endl;
	      }
	    }
	  }

	  for (unsigned int link_i=0; link_i<2; link_i++) {
	    if (rx_connection[link_i]->isValid()) {
              if (resultList().has<float>(m_postProcessor->m_delay[link_i], PixA::connectivityName(module_iter) ) &&
                  resultList().has<float>(m_postProcessor->m_threshold[link_i], PixA::connectivityName(module_iter) )) {
		try {
        	 bool keep_settings=false;
                  if (!m_postProcessor->m_keepSettingsName.empty()) {
                      keep_settings=resultList().value<bool>(m_postProcessor->m_keepSettingsName,PixA::connectivityName(module_iter) );
                  }

                  if (keep_settings && !m_postProcessor->m_forcedStorage) continue;
		  float delay=resultList().value<float>(m_postProcessor->m_delay[link_i], PixA::connectivityName(module_iter) );
		  float threshold=resultList().value<float>(m_postProcessor->m_threshold[link_i], PixA::connectivityName(module_iter) );

		  if (m_postProcessor->logLevel()>=kInfoLog) {

		    std::cout << "INFO [SetBocPixRxSettingsFromAnalysis::operator()] " << PixA::connectivityName(module_iter)
			      << " delay = " << delay << " threshold = " << threshold << std::endl;
		  }
		  if (   delay >= m_postProcessor->m_delayMin && delay < m_postProcessor->m_delayMax
		       && threshold >= m_postProcessor->m_thresholdMin && threshold < m_postProcessor->m_thresholdMax ) {
		  if (m_postProcessor->logLevel()>=kInfoLog) {
		    std::cout << "INFO [SetBocPixRxSettingsFromAnalysis::operator()] " << PixA::connectivityName(module_iter)
			      << " get rx config : " << rxName(rx_connection[link_i]->plugin()) << std::endl;
		  }
		  PixLib::Config &rx_config = config->subConfig( rxName(rx_connection[link_i]->plugin()) );
		  if (m_postProcessor->logLevel()>=kInfoLog) {
		    std::cout << "INFO [SetBocPixRxSettingsFromAnalysis::operator()] " << PixA::connectivityName(module_iter)
			      << " get data delay : " << dataDelayName(m_speedMode, rx_connection[link_i]->channel()) << std::endl;
		  }

		  confVal<float>(rx_config["General"][ dataDelayName(m_speedMode, rx_connection[link_i]->channel()) ])=delay;
		  confVal<float>(rx_config["Opt"][     thresholdName(m_speedMode, rx_connection[link_i]->channel()) ])=threshold;
		  //confVal<float>(rx_config["General"][ dataDelayName(OptoLinkConfigWriter::kDefault, rx_connection[link_i]->channel()) ])=delay;
		  //confVal<float>(rx_config["Opt"][     thresholdName(OptoLinkConfigWriter::kDefault, rx_connection[link_i]->channel()) ])=threshold;

		  if (m_postProcessor->logLevel()>=kInfoLog) {
		    std::cout << "INFO [SetBocPixRxSettingsFromAnalysis::operator()] " << PixA::connectivityName(module_iter)
			      << " set General "
			      << dataDelayName( m_speedMode, rx_connection[link_i]->channel())
			      << " to " <<  delay << " ( " 
			      << m_postProcessor->m_delay[link_i] << " = " << delay << ")"
			      << std::endl;
		  }

		  if (m_postProcessor->logLevel()>=kInfoLog) {
		    std::cout << "INFO [SetBocPixRxSettingsFromAnalysis::operator()]" << PixA::connectivityName(module_iter)
			      << "  set Opt " << thresholdName(m_speedMode, rx_connection[link_i]->channel())
			      << " to " << threshold << " ("
			      << m_postProcessor->m_threshold[link_i]<< " ="
			      << threshold << ")" << std::endl;
		  }

		  m_moduleGroupIsModified=true;
		  }
		  else {
		    config_error=true;
		  }
		}
		catch(PixA::ConfigException &err) {
		  config_error=true;
		}
		catch(...) {
		  std::cout << "INFO [SetBocPixRxSettingsFromAnalysis::operator()] " << PixA::connectivityName(module_iter)
			    << " caught an unidentified exception" << std::endl;
		}
	      }
            } else {
              if (m_postProcessor->logLevel()>=kInfoLog) {
		std::cout << "INFO [SetBocTxSettingsFromAnalysis::operator()] " << PixA::connectivityName(module_iter)
			  << " did not deliver RX-Threshold AND RX-Delay" << std::endl;
	      }
	    }
	  }
	  
	  if (resultList().has<float>(m_postProcessor->m_viset, PixA::connectivityName(module_iter) )) {
	    try {
	      float viset=(resultList().value<float>(m_postProcessor->m_viset, PixA::connectivityName(module_iter) )) / 1000.0;
              std::cout << "INFO [SetBocPixRxSettingsFromAnalysis::operator()]" << PixA::connectivityName(module_iter) << " has Viset = " << viset << "mV" << std::endl;
	      for (unsigned int link_i=0; link_i<2; link_i++) {
		if (rx_connection[link_i]->isValid()) {
		  if (rx_connection[link_i]->plugin()<4  && viset >= m_postProcessor->m_visetMin   && viset < m_postProcessor->m_visetMax ) {
		    m_viset[rx_connection[link_i]->plugin()].add(viset);
		  }
		  else {
		    pluginError(module_iter);
		  }
		}
	      }
	    }
	    catch(PixA::ConfigException &err) {
	      config_error=true;
	    }
	    catch(...) {
	      std::cout << "INFO [SetBocPixRxSettingsFromAnalysis::operator()] " << PixA::connectivityName(module_iter) << " caught an unidentified exception" << std::endl;
	    }
          } else {
            if (m_postProcessor->logLevel()>=kInfoLog) {
	      std::cout << "INFO [SetBocTxSettingsFromAnalysis::operator()] " << PixA::connectivityName(module_iter) << " had no ViSet written out" << std::endl;
	    }
	  }
	  error |= config_error;

	  if ( !rx_connection[0]->isValid()  && !rx_connection[1]->isValid()) {
	    noValidDTO(module_iter);
	    error=true;
	  }

	  if (!error) {
	    setModuleStatus(module_iter , true );
	    return;
	  }

	  if (config_error) {
	    configError(module_iter);
	  }
	}
	else {
	  noPixBoc(module_iter);
	}
      }
      catch(...) {
      }
      m_moduleGroupIsOk=false;
      setModuleStatus(module_iter , false );
    }

  protected:

    void noPixBoc(const PixA::ModuleList::const_iterator &module_iter) {
      resultList().addValue("HavePixBoc",PixA::connectivityName(module_iter), false);
    }

    void configError(const PixA::ModuleList::const_iterator &module_iter) {
      resultList().addValue("ConfigOk",PixA::connectivityName(module_iter), false);
    }

    void pluginError(const PixA::ModuleList::const_iterator &module_iter) {
      resultList().addValue("PluginOk",PixA::connectivityName(module_iter), false);
    }

    void noValidDTO(const PixA::ModuleList::const_iterator &module_iter) {
      resultList().addValue("validDTO",PixA::connectivityName(module_iter), false);
    }

    void visetError() {
      resultList().addValue("VisetOk",rodLocation(), false);
    }

    static std::string rxName(int plugin) {
      std::stringstream rx_id;
      rx_id << "PixRx" << plugin;
      return rx_id.str();
    }
 
    static std::string txName(int plugin) {
      std::stringstream tx_id;
      tx_id << "PixTx" << plugin;
      return tx_id.str();
    }

    static std::string channelName(const std::string &head, OptoLinkConfigWriter::EMccSpeed speed, int channel) {
      std::stringstream data_delay;
      data_delay << head;
      if (speed==OptoLinkConfigWriter::kMcc40) {
	data_delay << "_40_";
      }
      else if (speed==OptoLinkConfigWriter::kMcc80) {
	data_delay << "_80_";
      }
      data_delay << channel;
      return data_delay.str();
    }

    static std::string visetName(OptoLinkConfigWriter::EMccSpeed speed) {
      std::string viset_name("Viset");
      if (speed==OptoLinkConfigWriter::kMcc40) {
	viset_name+="_40";
      }
      else if (speed==OptoLinkConfigWriter::kMcc80) {
	viset_name+="_80";
      }
      return viset_name;
    }

    static std::string dataDelayName(OptoLinkConfigWriter::EMccSpeed speed, int channel) {
      return channelName("DataDelayF",speed, channel+2);
    }

    static std::string thresholdName(OptoLinkConfigWriter::EMccSpeed speed, int channel) {
      return channelName("RxThresholdF",speed, channel+2);
    }

    static std::string msrName(int channel) {
      return channelName("MarkSpace",OptoLinkConfigWriter::kDefault, channel+2);
    }

    static std::string fineDelayName(int channel) {
      return channelName("FineDelay",OptoLinkConfigWriter::kDefault, channel+2);
    }

  private:
    OptoLinkConfigWriter           *m_postProcessor;
    //PixLib::PixModuleGroupData_t   *m_moduleGroupData;
    PixLib::PixBoc                 *m_pixBoc;
    OptoLinkConfigWriter::EMccSpeed m_speedMode;

    Stat_t m_viset[4];

    bool m_moduleGroupIsOk;
    bool m_moduleGroupIsModified;
  };

  class SetBocIblRxSettingsFromAnalysis : public ModuleAnalysisBase{
    
    public:

      SetBocIblRxSettingsFromAnalysis( const PixA::RODLocationBase &rod_location,
				  ExtendedAnalysisResultList_t &analysis_results,
				  PixLib::PixModuleGroupData_t &module_group_data)
      : ModuleAnalysisBase(rod_location,analysis_results),
	m_pixBoc( module_group_data.getPixBoc() ),
	m_moduleGroupIsModified(false){
	  std::cout << "INFO [SetBocRxPhases::ctor]" << std::endl;
	}

	void operator()( const PixA::ModuleList::const_iterator &module_iter) {

	  std::cout<<"Inside SetBocIblRxSettingsFromAnalysis "<<rodLocation()<<std::endl;
	  std::string moduleName = PixA::connectivityName(module_iter);

	  if( !(m_pixBoc && m_pixBoc->getConfig() ) ){
	    std::cout<<"Cannot get PixBoc"<<std::endl;
	    return;
	  }

	  PixLib::Config *config= m_pixBoc->getConfig();

	  std::cout<<"Got config for "<<rodLocation()<<std::endl;

	  PixA::ModulePluginConnection rx_connection_dto1 = PixA::rxConnection(module_iter, PixA::DTO1);
	  PixA::ModulePluginConnection rx_connection_dto2 = PixA::rxConnection(module_iter, PixA::DTO2);

	  PixA::ModulePluginConnection *rx_connection[2];
	  // Note : dto2 -> 0 and dto -> 1
	  rx_connection[0]=&rx_connection_dto2;
	  rx_connection[1]=&rx_connection_dto1;

	    for (unsigned int link=0; link<2; link++) {

	      int plugin = rx_connection[link]->plugin();

	      std::string rxName = "PixRx" + std::to_string(plugin);
	      PixLib::Config &rx_config = config->subConfig( rxName );

	      std::string resultPhase = "optimalPhase_LINK" + std::to_string(link+1);
	      unsigned int newPhase = resultList().value<unsigned int>(resultPhase, moduleName  );
	      std::string phaseName = "Phases_"+std::to_string((rx_connection[link]->channel()+2) );

	      unsigned int dbPhase = confVal<unsigned int>(rx_config["General"][phaseName]);

	      std::cout<<moduleName<<" DTO "<<(int)(link+1)<<" "<<rxName<<" "<<phaseName<<" channel "<<rx_connection[link]->channel()<<" changed from "<< dbPhase<<" to "<<newPhase<<std::endl;

	      std::string passed = "passed_LINK"+std::to_string(link+1);
	       if( ( resultList().value<bool>(passed, moduleName) ) && (newPhase != dbPhase)){
	         confVal<unsigned int>(rx_config["General"][phaseName])= newPhase;
	         //std::cout<<moduleName<<" LINK "<<(int)(link+1)<<" "<<rxName<<" "<<phaseName<<" changed from "<< dbPhase<<" to "<<newPhase<<std::endl;
	         m_moduleGroupIsModified = true;
	       }
	     }
	  }

     bool isModuleGroupModified(){
       return m_moduleGroupIsModified;
     }

    private:
    PixLib::PixBoc                 *m_pixBoc;

    bool m_moduleGroupIsModified;
  };

  void OptoLinkConfigWriter::process(IAnalysisDataServer *data_server,
				     const PixA::RODLocationBase &rod_location,
				     ExtendedAnalysisResultList_t &analysis_results)
  {

    assert(data_server);

    // get default data server
    IDataServer *scan_data_server = data_server->getDataServer();
    assert(scan_data_server);

    try {
      const PixLib::Config &scan_config = scan_data_server->getScanConfig();

      std::string speed = confVal<std::string>(const_cast<PixLib::Config &>(scan_config)["mcc"]["mccBandwidth"]);
      std::map<std::string, EMccSpeed>::const_iterator speed_iter = s_speedMap.find(speed);
      if (speed_iter == s_speedMap.end()) {
	std::string message("Unhandled mcc bandwidth parameter: ");
	message += speed;
	message += ".";
	//	throw MRSException("PARAMERR", MRS_FATAL, "CAN::OptoLinkConfigWriter::process",message);
	throw MRSException(PixLib::PixMessages::FATAL, "CAN::OptoLinkConfigWriter::process",message);
      }

      std::shared_ptr<PixLib::PixModuleGroupData_t> pix_module_group = scan_data_server->createConfiguredModuleGroup();
      if (pix_module_group.get()) {
	// Function object to set rx delay and threshold of a module to pseudo random values
	// the modifier will also set the status in the analysis result list  : per module and the combined per ROD status.
	// @todo have to get speed from scan_param
        bool write_config=false;

	if((*pix_module_group).getCtrlType() == PixLib::CtrlType::L12ROD){
	  SetBocPixRxSettingsFromAnalysis modify_rx_settings(*this, rod_location, analysis_results, *pix_module_group, speed_iter->second);

	  // loop over each module and execute function which will modify rx delay and threshold
	  for_each_module( data_server->connectivity(),
			 rod_location,
			 scan_data_server->getPp0DisabledList(),
			 modify_rx_settings);
	  write_config = modify_rx_settings.isModuleGroupModified();
        } else if ((*pix_module_group).getCtrlType() == PixLib::CtrlType::CPPROD){
          std::cout<<"Saving Rx phases "<<std::endl;
          SetBocIblRxSettingsFromAnalysis modify_rx_settings(rod_location, analysis_results, *pix_module_group);

	  // loop over each module and execute function which will modify rx phases
	  for_each_module( data_server->connectivity(),
			 rod_location,
			 scan_data_server->getPp0DisabledList(),
			 modify_rx_settings);
	  write_config = modify_rx_settings.isModuleGroupModified();
        } else{
          std::cout<<"Unsupported Ctrl type "<<(*pix_module_group).getCtrlType()<<std::endl;
          return;
        }
        
	if( write_config) {
	  analysis_results.addNewPixModuleGroupConfig(pix_module_group);
	  ConfigWriteProcessor::process(data_server,rod_location, analysis_results);
	  analysis_results.addValue("WroteConfig",rod_location,true);
	  if (logLevel()>=kInfoLog) {
	    std::cout << "INFO [OptoLinkConfigWriter::process] config writing was successful." << std::endl;
	  }
	  return;
	} else {
          if (logLevel()>=kErrorLog) {
	     std::cerr << "ERROR [OptoLinkConfigWriter::process] Was not supposed to write any config, check your logic!" << std::endl;
	   }
	}
      } else {
        if (logLevel()>=kErrorLog) {
          std::cerr << "ERROR [OptoLinkConfigWriter::process] Could not get ModuleGroup" << std::endl;
        }
      }
    }
    catch( CAN::MRSException &err) {
      if (logLevel()>=kErrorLog) {
        std::cerr << "ERROR [OptoLinkConfigWriter::process] Caught MRSexception : " << err.getDescriptor() << std::endl;
      }
    }
    catch(PixA::BaseException &err ) {
      if (logLevel()>=kErrorLog) {
    	std::cerr << "ERROR [OptoLinkConfigWriter::process] Config writing failed : "  << err.getDescriptor() << std::endl;
      }
    }
    catch(std::runtime_error &err ) {
      if (logLevel()>=kErrorLog) {
    	std::cerr << "ERROR [OptoLinkConfigWriter::process] Config writing failed : "  << err.what() << std::endl;
      }
    }
    catch(...) {
      if (logLevel()>=kErrorLog) {
        std::cerr << "ERROR [OptoLinkConfigWriter::process] Config writing failed, caught unknown exception" << std::endl;
      }
    }
    analysis_results.addValue("WroteConfig",rod_location,false);
    if (logLevel()>=kErrorLog) {
      std::cerr << "ERROR [OptoLinkConfigWriter::process] Config writing failed." << std::endl;
    }

  }
}
