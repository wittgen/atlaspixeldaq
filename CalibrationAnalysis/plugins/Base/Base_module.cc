#include <AnalysisFactory.hh>
#include "ConfigWriteProcessor.hh"
#include "StatusEvaluator.hh"
#include "Pp0StatusEvaluator.hh"
#include "ModuleStatusEvaluator.hh"
#include "OptoLinkConfigWriter.hh"
#include "NoPostProcessing.hh"

DEFINE_SEAL_MODULE ()
DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::ConfigWriteProcessor,    "ConfigWriteProcessor")
DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::NoPostProcessing,        "NoPostProcessing")
DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::StatusEvaluator,         "StatusEvaluator")
DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::Pp0StatusEvaluator,      "Pp0StatusEvaluator")
DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::ModuleStatusEvaluator,   "ModuleStatusEvaluator")
DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::OptoLinkConfigWriter,    "OptoLinkConfigWriter")
