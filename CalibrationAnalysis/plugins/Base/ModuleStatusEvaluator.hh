#ifndef _CAN_ModuleStatusEvaluator_hh_
#define _CAN_ModuleStatusEvaluator_hh_

#include <StatusEvaluatorBase.hh>
#include <Config/Config.h>

namespace CAN {

  class ObjectInfo_t;

  class ModuleStatusEvaluatorHelper;

  class ModuleStatusEvaluator : public StatusEvaluatorBase
  {
    friend class ModuleStatusEvaluatorHelper;
  public:
    ModuleStatusEvaluator(SerialNumber_t serial_number, const ObjectInfo_t &);

    bool classify(IAnalysisDataServer *data_server,
		  const PixA::RODLocationBase &rod_location,
		  ExtendedAnalysisResultList_t &analysis_results);

    PixLib::Config &config() { return m_config; }

  private:
    PixLib::Config m_config;

    std::string m_moduleStatusVariableName;
    bool  m_orModuleStatus;
    bool  m_considerFailedInstead;
  };

}

#endif
