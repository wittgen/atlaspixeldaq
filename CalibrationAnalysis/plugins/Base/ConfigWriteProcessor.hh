#ifndef _CAN_ConfigWriteProcessor_hh_
#define _CAN_ConfigWriteProcessor_hh_

#include <IPostProcessing.hh>
#include <Config/Config.h>
#include <Common.hh>

#include <ConfigWrapper/IConnObjConfigDb.h>

namespace CAN {

  class ObjectInfo_t;

  class ConfigWriteProcessor : public IPostProcessing
  {
  public:
    ConfigWriteProcessor(SerialNumber_t serial_number, const ObjectInfo_t &);

    void requestData(IAnalysisDataServer *data_server,
		     const PixA::RODLocationBase &rod_location);

    void process(IAnalysisDataServer *data_server,
		 const PixA::RODLocationBase &rod_location,
		 ExtendedAnalysisResultList_t &analysis_results);

    PixLib::Config &config() { return m_config; }

    void setLogLevel(ELogLevel log_level) { m_logLevel=log_level; }

  protected:
    ELogLevel logLevel() const { return m_logLevel; }

  private:
    PixA::ConnObjConfigDbTagRef getConfigTag(IAnalysisDataServer *data_server)       { return getTag(data_server, false);  }
    PixA::ConnObjConfigDbTagRef getModuleConfigTag(IAnalysisDataServer *data_server) { return getTag(data_server, true);  }
    PixA::ConnObjConfigDbTagRef getTag(IAnalysisDataServer *data_server, bool module_config_tag);

    SerialNumber_t m_serialNumber;
    ELogLevel m_logLevel;
    PixLib::Config m_config;
    
    bool        m_defaultTag;
    bool        m_cloneTag;
    std::string m_customIdTag;
    std::string m_customTag;
    std::string m_customModuleTag;
  };

}

#endif
