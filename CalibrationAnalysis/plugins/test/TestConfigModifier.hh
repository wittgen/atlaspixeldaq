#ifndef _CAN_TestConfigModifier_hh_
#define _CAN_TestConfigModifier_hh_

#include <IAnalysisPerROD.hh>
#include <Config/Config.h>
//#include <Analysis.hh>

namespace CAN {

  class ObjectInfo_t;
  class IDataServer;

  class TestConfigModifier : public IAnalysisPerROD
  {
  public:
    TestConfigModifier(SerialNumber_t serial_number, const ObjectInfo_t &);

    void requestHistograms(IAnalysisDataServer *data_server,
			   const PixA::RODLocationBase &rod_location);

    void analyse(IAnalysisDataServer *data_server,
    		 const PixA::RODLocationBase &rod_location,
		 ExtendedAnalysisResultList_t &analysis_results);

    PixLib::Config &config() { return m_config; }

    /** Return the name of the analysis.
     */
    const std::string &name() const { return s_name; }

    /** A unique identifier of an analysis. Could for example be the scan id.
     * This identifier is used to localise histograms.
     */
    SerialNumber_t serialNumber() const { return m_serialNumber; }

    void requestAbort() { m_abort = true;}

    void setLogLevel(ELogLevel log_level) { m_logLevel=log_level; }

  protected:
    bool abortRequested() const { return m_abort; };

  private:
    PixLib::Config m_config;

    SerialNumber_t m_serialNumber;

    ELogLevel m_logLevel;
    bool m_modifyTDAC;
    bool m_modifyRX;
    bool m_abort;

    static std::string s_name;
  };

}

#endif
