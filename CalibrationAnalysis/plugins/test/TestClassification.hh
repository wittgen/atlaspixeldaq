#ifndef _CAN_TestClassification_hh_
#define _CAN_TestClassification_hh_

#include <IClassification.hh>
#include <Config/Config.h>

namespace CAN {

  class ObjectInfo_t;

  class TestClassification : public IClassification
  {
  public:
    TestClassification(SerialNumber_t serial_number, const ObjectInfo_t &);

    bool classify(IAnalysisDataServer *data_server,
		  const PixA::RODLocationBase &rod_location,
		  ExtendedAnalysisResultList_t &analysis_results);

    PixLib::Config &config() { return m_config; }
    void setLogLevel(ELogLevel log_level) {}

  private:
    PixLib::Config m_config;

    bool  m_crash;
    bool  m_default;
  };

}

#endif
