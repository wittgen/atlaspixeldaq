#include <AnalysisFactory.hh>
#include <RxThresholdDelayCheck.hh>
#include <ShapescanCheck.hh>
#include <MSRtune.hh>

DEFINE_SEAL_MODULE ()
DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::RxThresholdDelayCheck, "RxThresholdDelayCheck")
DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::ShapescanCheck, "ShapescanCheck")
DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::MSRtune, "MSRtune")
