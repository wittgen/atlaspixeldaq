#include "TestAnalysis.hh"
#include <Config/ConfGroup.h>
#include <ConfigWrapper/Connectivity.h>
#include <IDataServer.hh>
#include <IAnalysisDataServer.hh>
#include <ExtendedAnalysisResultList_t.hh>
#include <ConfigWrapper/ConnectivityUtil.h>
#include <ConfigWrapper/PixDisableUtil.h>
#include <exception.hh>

#include <time.h>

namespace CAN {

  std::string TestAnalysis::s_name("TestAnalysis");

  TestAnalysis::TestAnalysis(SerialNumber_t serial_number, const ObjectInfo_t &)
    : m_config("TestAnalysis"),
      m_serialNumber(serial_number),
      m_logLevel(kWarnLog),
      m_abort(false)
  {
    m_scanName[0]="TestAna";
    {
      m_config.addGroup("general");
      PixLib::ConfGroup &group=m_config["general"];
      group.addFloat("cut", m_cut, 0., "A cut", true);
    }
    {
      m_config.addGroup("test");
      PixLib::ConfGroup &test_group=m_config["test"];
      test_group.addInt("analysisTime", m_sleep, 500, "The time the analysis will take in ms", true);
      test_group.addBool("crash", m_crash, false, "Produce segmentation fault", true);
      test_group.addString("crashRodName",   m_crashRodName, "", "Crash on this ROD", true);
      test_group.addString("hangRodName",    m_hangRodName, "",  "Hang on this ROD", true);
      test_group.addInt("hangTime",          m_hangTime, 300*1000,     "Time it waill take to process this rod (in ms)", true);
      test_group.addFloat("failProbability", m_failProbability, 0, "Analysis fail Probability ", true);
      test_group.addFloat("hangProbability", m_hangProbability, 0, "Analysis hang Probability ", true);
      test_group.addBool("allowForMissingHistograms", m_allowForMissingHistograms, true, "Allow for missing histograms", true);
   }
    {
      m_config.addGroup("DataSrv");
      PixLib::ConfGroup &test_group=m_config["DataSrv"];
      test_group.addString("histogramName", m_histogramName, "OCCUPANCY", "Histogram to be requested", true);
      test_group.addBool("isRODHistogram", m_isRodHistogram, false, "Is a per ROD histogram", true);
      test_group.addBool("needModuleCfg", m_needModuleConfig, false, "Need module configurations", true);
      test_group.addBool("needBocCfg", m_needBocConfig, false, "Need BOC configurations", true);
      test_group.addBool("needScanCfg", m_needScanConfig, false, "Need Scan configurations", true);
      test_group.addString("scanName", m_scanName[0], "TestAna", "The name of the named scan", true);
      test_group.addString("scanName2", m_scanName[1], "", "Optional second scan.", true);
   }

    {
      m_config.addGroup("Results");
      PixLib::ConfGroup &test_group=m_config["Results"];
      test_group.addBool("addFloatResult",   m_addFloatResult,   false, "Add a float result", true);
      test_group.addBool("addStateResult",   m_addStateResult,   false, "Add a state result", true);
      test_group.addBool("addAverageResult", m_addAverageResult, false, "Add an average result", true);
      test_group.addBool("addBoolResult",    m_addBoolResult,    false, "Add a boolean result", true);
      test_group.addBool("addSpecialPixelMapResult",    m_addSpecialPixelMapResult,    false, "Add a special pixel map result", true);
      test_group.addBool("countInputData",   m_countInputData,   true, "Add fields which count histograms, configs etc.", true);
    }
  }

  void TestAnalysis::requestHistograms(IAnalysisDataServer *data_server,
				       const PixA::RODLocationBase &rod_location) {
    for (unsigned int scan_i=0; scan_i<2; scan_i++) {
      if (scan_i>0 && m_scanName[scan_i].empty()) continue;

    IDataServer *scan_data_server = data_server->getDataServer(m_scanName[scan_i]);
    if (m_needScanConfig) {
      scan_data_server->requestScanConfig();
    }
    if (m_needBocConfig) {
      scan_data_server->requestBocConfig();
    }

    if (!m_histogramName.empty()) {
      if (m_isRodHistogram) {
	scan_data_server->requestHistogram(m_histogramName, rod_location );
      }
      // keep the root list to ensure that the pix disable lives long enough
      const PixA::DisabledList &pp0_disabled_list( scan_data_server->getPp0DisabledList() );

      const PixA::Pp0List pp0_list( data_server->connectivity().pp0s(rod_location) );
      PixA::Pp0List::const_iterator pp0_list_begin=pp0_list.begin();
      PixA::Pp0List::const_iterator pp0_list_end=pp0_list.end();

      for (PixA::Pp0List::const_iterator pp0_iter=pp0_list_begin;
	   pp0_iter != pp0_list_end && !abortRequested();
	   ++pp0_iter) {

	if (!PixA::enabled( pp0_disabled_list, pp0_iter )) continue;
	const PixA::DisabledList module_disabled_list( moduleDisabledList(pp0_disabled_list, pp0_iter) );

	PixA::ModuleList::const_iterator module_list_begin=modulesBegin(pp0_iter);
	PixA::ModuleList::const_iterator module_list_end=modulesEnd(pp0_iter);

	for (PixA::ModuleList::const_iterator module_iter=module_list_begin;
	     module_iter != module_list_end && !abortRequested();
	     ++module_iter) {

	  if (!PixA::enabled( module_disabled_list, module_iter )) continue;
	  
	  if (!m_isRodHistogram) {
	    scan_data_server->requestHistogram(m_histogramName, PixA::connectivityName( module_iter ) );
	  }
	  if (m_needModuleConfig) {
	    scan_data_server->requestModuleConfig( PixA::connectivityName( module_iter ) );
	  }
	}
      }
    }
    }
  }


  void TestAnalysis::analyse(IAnalysisDataServer *data_server,
			     const PixA::RODLocationBase &rod_location,
			     ExtendedAnalysisResultList_t &analysis_results) 
  {
    if (m_crash) {
      if (m_crashRodName.empty() || rod_location==m_crashRodName) {
	// produce segmentation fault
	char *null_ptr = NULL;
	(*null_ptr)++;
      }
    }

    PixA::ConnectivityRef conn = data_server->connectivity();

    // verify whether the connectivity is valid
    // should always be the case.
    if (!conn) {
      analysis_results.addValue("Status",rod_location, false);
      return;
    }

    // get the data server for the named scan
    // the AnalysisDataServer should throw an exception if the 
    // data server for the given scan does not exists
    // so no test needed.


//     std::cout << "INFO [TestAnalysis::analyse] use data server " << static_cast<void *>(data_server)
// 	      << " has connectivity " << ((conn) ? "yes" : "no") 
// 	      << std::endl;

    if (m_failProbability>1.) {
      m_failProbability/=100;
    }
    if (m_hangProbability>1.) {
      m_hangProbability/=100;
    }
    const PixA::Pp0List &pp0_list=conn.pp0s(rod_location);
    PixA::Pp0List::const_iterator pp0_list_begin=pp0_list.begin();
    PixA::Pp0List::const_iterator pp0_list_end=pp0_list.end();
    bool comb_status = true;

    for (unsigned int scan_i=0; scan_i<2  && !abortRequested(); scan_i++) {
    if (scan_i>0 && m_scanName[scan_i].empty()) continue;

    IDataServer *scan_data_server = data_server->getDataServer(m_scanName[scan_i]);

    //    if (!m_histogramName.empty()) {
    try {
      // get scan and boc config.
      if (m_needBocConfig)
	{
	  const PixLib::Config &a_config = scan_data_server->getBocConfig();
	  PixLib::Config &casted_a_config=const_cast<PixLib::Config &>(a_config);
	  if (casted_a_config.size()>0 || casted_a_config.subConfigSize()>0) {
	    if (m_countInputData) {
	      analysis_results.addValue("hasBocConfig",rod_location, true);
	    }
	  }
	  else {
	    comb_status = false;
	  }
	}

      if (m_needScanConfig)
	{
	  const PixLib::Config &a_config = scan_data_server->getScanConfig();
	  PixLib::Config &casted_a_config=const_cast<PixLib::Config &>(a_config);
	  if (casted_a_config.size()==0 && casted_a_config.subConfigSize()==0) {
	    if (m_countInputData) {
	      analysis_results.addValue("hasScanConfig",rod_location, false);
	    }
	    comb_status = false;
	  }
	  else {
	    if (m_countInputData) {
	      analysis_results.addValue("hasScanConfig",rod_location, true);
	    }
	  }
	}
    }
    catch (PixA::ConfigException &err) {
      comb_status = false;
    }

    const PixA::DisabledList &pp0_disabled_list( scan_data_server->getPp0DisabledList() );

    unsigned int n_rod_histos=0;

    if (!m_histogramName.empty() && m_isRodHistogram) {
      // download all histograms of the given name
      std::pair<unsigned int, bool> histo_status = checkHistograms( scan_data_server, analysis_results, m_histogramName, rod_location );
      comb_status &= histo_status.second;
      unsigned int n_histos = histo_status.first;
      n_rod_histos+=n_histos;
    }

    if (m_hangRodName==rod_location ) {
      timespec sleep_time;
      sleep_time.tv_sec=m_hangTime/1000;
      sleep_time.tv_nsec=(m_hangTime%1000)*1000000;
      nanosleep(&sleep_time,NULL);
    }

    for (PixA::Pp0List::const_iterator pp0_iter=pp0_list_begin;
	 pp0_iter != pp0_list_end && !abortRequested();
	 ++pp0_iter) {

      // test whether the pp0 is enabled and get a disabled list for the modules of the pp0
      if (!PixA::enabled( pp0_disabled_list, pp0_iter )) continue;
      const PixA::DisabledList module_disabled_list( moduleDisabledList(pp0_disabled_list, pp0_iter) );

      PixA::ModuleList::const_iterator module_list_begin=modulesBegin(pp0_iter);
      PixA::ModuleList::const_iterator module_list_end=modulesEnd(pp0_iter);

      for (PixA::ModuleList::const_iterator module_iter=module_list_begin;
	   module_iter != module_list_end && !abortRequested();
	   ++module_iter) {

	if (!PixA::enabled( module_disabled_list, module_iter )) continue;

	bool mod_status=true;

	try {

	  // get module configuration
	  if (m_needModuleConfig)
	    {
	      const PixLib::Config &a_config = scan_data_server->getModuleConfig( PixA::connectivityName(module_iter) );
	      PixLib::Config &casted_a_config=const_cast<PixLib::Config &>(a_config);
	      if (casted_a_config.size()>0 || casted_a_config.subConfigSize()>0) {
		if (m_countInputData) {
		  analysis_results.addValue("hasModConfig",PixA::connectivityName(module_iter), true);
		}
	      }
	      else {
		mod_status = false;
	      }
	    }

	}
	catch (PixA::ConfigException &err) {
	  mod_status = false;
	  comb_status =false;
	}

	if (!m_histogramName.empty() && !m_isRodHistogram) {
	  // download all histograms of the given name
	  std::pair<unsigned int, bool> histo_status = checkHistograms( scan_data_server, analysis_results, m_histogramName, PixA::connectivityName( module_iter ) );
	  mod_status &= histo_status.second;
	  unsigned int n_histos = histo_status.first;
	  n_rod_histos+=n_histos;

	}
	if (m_sleep>0) {
	  timespec sleep_time;
	  sleep_time.tv_sec=m_sleep/1000;
	  sleep_time.tv_nsec=(m_sleep%1000)*1000000;
	  nanosleep(&sleep_time,NULL);
	}

        bool analysis_failed=(m_failProbability<1. ? ( rand() <= static_cast<int>(m_failProbability * RAND_MAX)) : false );
	analysis_results.addValue("Status",location(module_iter), mod_status && !analysis_failed);
        comb_status &= !analysis_failed;

	// should missing histograms impact the analysis status ?
	if (!m_allowForMissingHistograms) {
	   comb_status &= mod_status;
        }

	if (!analysis_failed) {

	  // produce some random results
	  if (m_addFloatResult) {
	    analysis_results.addValue("AFloat",location(module_iter), static_cast<float>( static_cast<double>(rand())/RAND_MAX *20.-5.) );
	  }
	  if (m_addAverageResult) {

	    float mean = static_cast<float>( static_cast<double>(rand())/RAND_MAX *20.-5.);
	    analysis_results.addValue("AnAverageResult",
				      location(module_iter), 
				      AverageResult_t(160*18-100 +static_cast<unsigned int>(rand() % 100),
						      mean,
						      static_cast<float>( static_cast<double>(rand())/RAND_MAX *5),
						      mean-static_cast<float>( static_cast<double>(rand())/RAND_MAX *10.),
						      mean+static_cast<float>( static_cast<double>(rand())/RAND_MAX *10.)));
	  }
	  if (m_addStateResult) {
	    analysis_results.addValue("AState",location(module_iter), static_cast<unsigned int>(rand() % 10)) ;
	  }
	  if (m_addBoolResult) {
	    analysis_results.addValue("ABool",location(module_iter), static_cast<bool>( (rand() % 1)) ) ;
	  }
	  if (m_addSpecialPixelMapResult) {
	    PixelMap_t pixelmap;
	    for (int p=0; p<1+static_cast<int>( (rand() % 3)); ++p) {
	      pixelmap[std::make_pair(static_cast<unsigned int>( (rand() % 320)),static_cast<unsigned int>( (rand() % 144)))]=static_cast<unsigned int>( (rand() % 30));
	    }
	    analysis_results.addValue("ASpecialPixelMap",location(module_iter),pixelmap ) ;
	  }
	}
      }

    }
    if (!m_histogramName.empty()) {
      if (n_rod_histos == 0  && m_isRodHistogram) {
	comb_status = false;
      }
    }
    }
    if(!abortRequested()) {
      analysis_results.addValue("Status",rod_location, comb_status);
    }
  }

  std::pair<unsigned int, bool> TestAnalysis::checkHistograms(IDataServer *scan_data_server,
							      ExtendedAnalysisResultList_t &analysis_results,
							      const std::string &histogram_name,
							      const std::string &connectivity_name) 
  {
    unsigned int n_histos=0;
    bool mod_status=true;

    try {
      HistoInfo_t info;
      scan_data_server->numberOfHistograms(histogram_name, connectivity_name, info);

      PixA::Index_t index;
      info.makeDefaultIndexList(index);
      unsigned int n_histos=0;
      if (index.size()>0) {
	PixA::Index_t min_index(index);

	// loop over all indices
	for( ;index[0]<info.maxIndex(0); ) {

	  // get histogram
	  try {
	    const Histo *a_histo = scan_data_server->getHistogram(histogram_name, connectivity_name, index);
	    if (a_histo) {
	      n_histos++;
	    }
	  }
	  catch (HistoException &err) {
	    mod_status = false;
	  }

	  // next index;
	  unsigned int level_i=index.size();
	  for (; level_i-->0; ) {
	    if (index[level_i]+1<info.maxIndex(level_i)) {
	      index[level_i]++;
	      break;
	    }
	    else {
	      index[level_i]=min_index[level_i];
	    }
	  }
	  if (level_i>index.size()) break;
	}
	if (n_histos==0) {
	  mod_status = false;
	}
      }
      else {
	mod_status = false;
      }
      if (m_countInputData) {
	analysis_results.addValue("NHistograms",connectivity_name, static_cast<float>(n_histos));
      }

    }
    catch (CAN::HistoException &err) {
      mod_status = false;
    }
    return std::make_pair(n_histos, mod_status);
  }
}
