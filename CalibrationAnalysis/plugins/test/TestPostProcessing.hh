#ifndef _CAN_TestPostProcessing_hh_
#define _CAN_TestPostProcessing_hh_

#include <IPostProcessing.hh>
#include <Config/Config.h>
#include <Common.hh>

namespace CAN {

  class ObjectInfo_t;

  class TestPostProcessing : public IPostProcessing
  {
  public:
    TestPostProcessing(SerialNumber_t serial_number, const ObjectInfo_t &);

    void requestData(IAnalysisDataServer *data_server,
		     const PixA::RODLocationBase &rod_location);

    void process(IAnalysisDataServer *data_server,
		 const PixA::RODLocationBase &rod_location,
		 ExtendedAnalysisResultList_t &analysis_results);

    PixLib::Config &config() { return m_config; }
    void setLogLevel(ELogLevel log_level) {}

  private:
    SerialNumber_t m_serialNumber;
    PixLib::Config m_config;

    bool  m_crash;
  };

}

#endif
