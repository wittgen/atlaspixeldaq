#include "TestClassification.hh"
#include <ExtendedAnalysisResultList_t.hh>

namespace CAN {

  TestClassification::TestClassification(SerialNumber_t serial_number, const ObjectInfo_t &)
    : m_config("TestClassification")
  {
    {
      m_config.addGroup("test");
      PixLib::ConfGroup &test_group=m_config["test"];
      test_group.addBool("default", m_default, true, "Default answer", true);
      test_group.addBool("crash", m_crash, false, "Produce segmentation fault", true);
    }
  }

  bool TestClassification::classify(IAnalysisDataServer * /*data_server*/,
				    const PixA::RODLocationBase &rod_location,
				    ExtendedAnalysisResultList_t &analysis_results) {

    if (m_crash) {
      // produce segmentation fault
      char *null_ptr = NULL;
      (*null_ptr)++;
    }


    bool status=m_default;
    try {
      status = analysis_results.value<bool>("Status",rod_location);
    }
    catch (AnalysisResultValueNotFound &err) {
    }
    return status;
  }

}
