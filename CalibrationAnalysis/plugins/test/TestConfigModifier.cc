#include "TestConfigModifier.hh"
#include <Config/ConfGroup.h>
#include <ConfigWrapper/Connectivity.h>
#include <IDataServer.hh>
#include <IAnalysisDataServer.hh>
#include <ExtendedAnalysisResultList_t.hh>
#include <ConfigWrapper/ConnectivityUtil.h>
#include <ConfigWrapper/PixDisableUtil.h>
#include <ConfigWrapper/ConfObjUtil.h>
#include <for_each.hh>
#include <exception.hh>

#include <PixModule/PixModule.h>
#include <PixModuleGroup/PixModuleGroup.h>
#include <PixBoc/PixBoc.h>
#include <PixFe/PixFe.h>

#include <time.h>

namespace CAN {

  std::string TestConfigModifier::s_name("TestConfigModifier");

  TestConfigModifier::TestConfigModifier(SerialNumber_t serial_number, const ObjectInfo_t &)
    : m_config("TestConfigModifier"),
      m_serialNumber(serial_number),
      m_abort(false)
  {
    {
      m_config.addGroup("Modify");
      PixLib::ConfGroup &modify_group=m_config["Modify"];
      modify_group.addBool("ModifyTDAC", m_modifyTDAC, true, "Modify all tdacs of a module", true);
      modify_group.addBool("ModifyRX", m_modifyRX, true, "Modify the BOC RX delay and threshold ", true);
   }
  }

  void TestConfigModifier::requestHistograms(IAnalysisDataServer *data_server,
					     const PixA::RODLocationBase &rod_location)
  {
    assert(data_server);

    IDataServer *scan_data_server = data_server->getDataServer();
    assert(scan_data_server);

    if (m_modifyRX) {
      scan_data_server->requestModuleGroupConfig();
    }

    if (m_modifyTDAC) {
      PixA::ConnectivityRef conn( data_server->connectivity());
      const PixA::DisabledList &pp0_disabled_list( scan_data_server->getPp0DisabledList() );

      //      std::vector<std:;string> empty_histogram_list;
      //      for_each_module(scan_data_server, Requesthistograms(scan_data_server, empty_histogram_list, true /* */) );
      std::vector<std::string> empty_histogram_list;
      RequestModuleConfig module_config_requestor(scan_data_server);
      for_each_module(conn, rod_location, pp0_disabled_list, module_config_requestor);
    }

  }

  /** Helper class to set semi-random tdac values to all front-ends of all modules.
   */
  class SetRandomBocRxSettings : public ModuleAnalysisBase
  {
  public:
    enum ESpeedMode {kDefault, kBoc40, kBoc80};

    SetRandomBocRxSettings( const PixA::RODLocationBase &rod_location,
			    ExtendedAnalysisResultList_t &analysis_results,
			    PixLib::PixModuleGroupData_t &module_group_data,
			    ESpeedMode speed,
			    ELogLevel log_level)
      : ModuleAnalysisBase(rod_location,analysis_results),
        m_moduleGroupData(&module_group_data),
	m_pixBoc(module_group_data.getPixBoc()),
	m_speedMode(speed),
	m_delay1(12),
	m_threshold1(127),
	m_delay2(13),
	m_threshold2(128),
	m_logLevel(log_level),
	m_moduleGroupIsOk(true),
	m_moduleGroupIsModified(false)
    {}

    bool isModuleGroupOk() {
      return m_moduleGroupIsOk & m_moduleGroupIsModified;
    }

    bool isModuleGroupModified() {
      return m_moduleGroupIsModified;
    }

    void operator()( const PixA::ModuleList::const_iterator &module_iter) {
      try {
	if (m_pixBoc && m_pixBoc->getConfig()) {
	  PixLib::Config *config= m_pixBoc->getConfig();

	  PixA::ModulePluginConnection rx_connection_dto1 = PixA::rxConnection(module_iter, PixA::DTO1);
	  PixA::ModulePluginConnection rx_connection_dto2 = PixA::rxConnection(module_iter, PixA::DTO2);
	  bool error=false;

	  if (rx_connection_dto1.isValid()) {

	    // set rx delay and threshold to semi random values :
	    // alternative :
	    // 	    m_pixBoc->getRx( rx_connection_dto1.plugin() )->setRxDataDelay( rx_connection_dto1.channel(), m_delay1);
	    // 	    m_pixBoc->getRx( rx_connection_dto1.plugin() )->setRxThreshold( rx_connection_dto1.channel(), m_threshold1);

	    try {
	      if (m_logLevel>=kInfoLog) std::cout << "INFO [ModifyBocRxSettings::operator()] get rx config : " << rxName(rx_connection_dto1.plugin()) << std::endl;

	      PixLib::Config &rx_config = config->subConfig( rxName(rx_connection_dto1.plugin()) );

	      if (m_logLevel>=kInfoLog) std::cout << "INFO [ModifyBocRxSettings::operator()] get data delay : "
						  << dataDelayName(m_speedMode, rx_connection_dto1.channel()) << std::endl;

	      confVal<int>(rx_config["General"][ dataDelayName(m_speedMode, rx_connection_dto1.channel()) ])=m_delay1;
	      confVal<int>(rx_config["Opt"][     thresholdName(m_speedMode, rx_connection_dto1.channel()) ])=m_threshold1 + PixA::modId(module_iter);
	      m_moduleGroupIsModified=true;
	    }
	    catch(PixA::ConfigException &err) {
	      configError(module_iter);
	      error=true;
	    }

	  }

	  if (rx_connection_dto2.isValid()) {
	    // set rx delay and threshold to semi random values :

	    // alternative:
	    //	    m_pixBoc->getRx( rx_connection_dto2.plugin() )->setRxDataDelay( rx_connection_dto2.channel(), m_delay1);
	    //	    m_pixBoc->getRx( rx_connection_dto2.plugin() )->setRxThreshold( rx_connection_dto2.channel(), m_threshold2);

	    try {
	      // assign
	      if (m_logLevel>=kInfoLog) std::cout << "INFO [ModifyBocRxSettings::operator()] get rx config : " << rxName(rx_connection_dto2.plugin()) << std::endl;

	      PixLib::Config &rx_config = config->subConfig( rxName(rx_connection_dto2.plugin()) );

	      if (m_logLevel>=kInfoLog) std::cout << "INFO [ModifyBocRxSettings::operator()] get data delay : "
						  << dataDelayName(m_speedMode, rx_connection_dto2.channel()) << std::endl;

	      confVal<int>(rx_config["General"][ dataDelayName(m_speedMode, rx_connection_dto2.channel()) ])=m_delay2;
	      confVal<int>(rx_config["Opt"][     thresholdName(m_speedMode, rx_connection_dto2.channel()) ])=m_threshold2 + PixA::modId(module_iter);
	      m_moduleGroupIsModified=true;
	    }
	    catch(PixA::ConfigException &err) {
	      configError(module_iter);
	      error=true;
	    }
	  }

	  if ( !rx_connection_dto2.isValid()  && !rx_connection_dto1.isValid()) {
	    noValidDTO(module_iter);
	    error=true;
	  }

	  if (!error) {
	    setModuleStatus(module_iter , true );
	    return;
	  }

	}
	else {
	  noPixBoc(module_iter);
	}
      }
      catch(...) {
      }
      m_moduleGroupIsOk=false;
      setModuleStatus(module_iter , false );
    }

  protected:

    void noPixBoc(const PixA::ModuleList::const_iterator &module_iter) {
      resultList().addValue("HavePixBoc",PixA::connectivityName(module_iter), false);
    }

    void configError(const PixA::ModuleList::const_iterator &module_iter) {
      resultList().addValue("ConfigOk",PixA::connectivityName(module_iter), false);
    }

    void noValidDTO(const PixA::ModuleList::const_iterator &module_iter) {
      resultList().addValue("validDTO",PixA::connectivityName(module_iter), false);
    }

    static std::string rxName(int plugin) {
      std::stringstream rx_id;
      rx_id << "PixRx" << plugin;
      return rx_id.str();
    }
 
    static std::string channelName(const std::string &head, ESpeedMode speed, int channel) {
      std::stringstream data_delay;
      data_delay << head;
      if (speed==kBoc40) {
	data_delay << "_40_";
      }
      else if (speed==kBoc80) {
	data_delay << "_80_";
      }
      data_delay << channel;
      return data_delay.str();
    }

    static std::string dataDelayName(ESpeedMode speed, int channel) {
      return channelName("DataDelay",speed, channel+2);
    }

    static std::string thresholdName(ESpeedMode speed, int channel) {
      return channelName("RxThreshold",speed, channel+2);
    }

  private:
    PixLib::PixModuleGroupData_t *m_moduleGroupData;
    PixLib::PixBoc               *m_pixBoc;
    ESpeedMode                    m_speedMode;

    int m_delay1;
    int m_threshold1;
    int m_delay2;
    int m_threshold2;

    ELogLevel                     m_logLevel;

    bool m_moduleGroupIsOk;
    bool m_moduleGroupIsModified;
  };


  /** Helper class to set semi-random tdac values to all front-ends of all modules.
   */
  class SetRandomModuleTDAC : public ModuleAnalysisBase
  {
  protected:

    /** Helper class to set a semi-random tdac value to all front-ends of one module.
     */
    class ModifyFeTDac : public std::unary_function<PixLib::PixFe *, void>
    {
    public:
      ModifyFeTDac(unsigned short tdac) : m_tdac(tdac) {}

      void setTdacValue(unsigned short tdac) { m_tdac= tdac; }

      void operator()( PixLib::PixFe *front_end) {
	assert(front_end);
	PixLib::ConfMask<unsigned short int> &tdacs = front_end->readTrim("TDAC");

	// set a random TDac value to the entire front-end
	unsigned short tdac_val = static_cast<unsigned short>(front_end->number());
	tdac_val += m_tdac;
	for (int col=0; col<tdacs.ncol(); col++) {
	  for (int row=0; row<tdacs.nrow(); row++) {
	    tdacs.set(col, row, tdac_val);
	  }
	}

      }
    private:

      unsigned short m_tdac;
    };


  public:
    SetRandomModuleTDAC(IDataServer *data_server, const PixA::RODLocationBase &rod_location, ExtendedAnalysisResultList_t &analysis_results)
      : ModuleAnalysisBase(rod_location, analysis_results),
        m_dataServer(data_server),
	m_modifyFeTDac(64)
    {}

    void operator()( const PixA::ModuleList::const_iterator &module_iter) {
      std::shared_ptr<PixLib::PixModuleData_t> module_data;

      try {
	module_data = m_dataServer->createConfiguredModule( PixA::connectivityName(module_iter) );
      }
      catch (...) {
      }

      if (module_data.get()) {
	// set a random TDac value
	try {
	  m_modifyFeTDac.setTdacValue( PixA::modId(module_iter)+32 );
	  std::for_each(module_data->feBegin(),
			module_data->feEnd(),
			m_modifyFeTDac );

	  m_hasModules=true;
	  resultList().addNewPixModuleConfig(module_data);
	  setModuleStatus( PixA::connectivityName(module_iter), true );
	  return;
	}
	catch (...) {
	}
      }
      setModuleStatus( PixA::connectivityName(module_iter), false );

    }


  private:

    IDataServer                  *m_dataServer;
    ModifyFeTDac                  m_modifyFeTDac;
    bool m_combinedStatus;
    bool m_hasModules;
  };


  void TestConfigModifier::analyse(IAnalysisDataServer *data_server,
				   const PixA::RODLocationBase &rod_location,
				   ExtendedAnalysisResultList_t &analysis_results) 
  {

    assert(data_server);

    // get default data server
    IDataServer *scan_data_server = data_server->getDataServer();
    assert(scan_data_server);

    if (m_modifyRX) {
      std::shared_ptr<PixLib::PixModuleGroupData_t> pix_module_group = scan_data_server->createConfiguredModuleGroup();
      if (pix_module_group.get()) {

	// Function object to set rx delay and threshold of a module to pseudo random values
	// the modifier will also set the status in the analysis result list  : per module and the combined per ROD status.
	// @todo have to get speed from scan_param
	SetRandomBocRxSettings modify_rx_settings(rod_location, analysis_results, *pix_module_group, SetRandomBocRxSettings::kDefault, m_logLevel);

	// loop over each module and execute function which will modify rx delay and threshold
	for_each_module( data_server->connectivity(),
			 rod_location,
			 scan_data_server->getPp0DisabledList(),
			 modify_rx_settings);
	if (modify_rx_settings.isModuleGroupModified()) {
	  analysis_results.addNewPixModuleGroupConfig(pix_module_group);
	}
      }
      else {
	analysis_results.addValue(rod_location,"HasPixModuleGroup",false);
	analysis_results.addValue(rod_location,"Status",false);
      }
    }
    if (m_modifyTDAC) {

      // Function object to modify TDAC settings of a module.
      // the modifier will also set the status in the analysis result list  : per module and the combined per ROD status.
      SetRandomModuleTDAC module_tdac_modifier(scan_data_server, rod_location, analysis_results);

      // loop over each module and execute function which will modify tdac  settgins
      for_each_module( data_server->connectivity(),
		       rod_location,
		       scan_data_server->getPp0DisabledList(),
		       module_tdac_modifier);

    }
  }
}
