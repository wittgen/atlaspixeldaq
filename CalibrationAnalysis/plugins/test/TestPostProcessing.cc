#include "TestPostProcessing.hh"
#include <ExtendedAnalysisResultList_t.hh>

#include <sstream>
#include <fstream>

namespace CAN {

  TestPostProcessing::TestPostProcessing(SerialNumber_t serial_number, const ObjectInfo_t &)
    : m_serialNumber(serial_number),m_config("TestPostProcessing") 
  {
    {
      m_config.addGroup("test");
      PixLib::ConfGroup &test_group=m_config["test"];
      test_group.addBool("crash", m_crash, false, "Produce segmentation fault", true);
    }

  }

  void TestPostProcessing::requestData(IAnalysisDataServer * /*data_server*/,
				       const PixA::RODLocationBase &/*rod_location*/)
  {
    /* Nothing to be requested. */
  }

  void TestPostProcessing::process(IAnalysisDataServer */*data_server*/,
				   const PixA::RODLocationBase &rod_location,
				   ExtendedAnalysisResultList_t &analysis_results) {
    std::stringstream file_name;
    file_name << "/tmp/" << getenv("USER") << "/A" << m_serialNumber << "_" << rod_location << ".txt";
    std::ofstream fout (file_name.str().c_str());

    for (std::map<std::string, std::map<std::string, bool > >::const_iterator iter=analysis_results.begin<bool>();
   	 iter != analysis_results.end<bool>();
   	 iter ++) {

      for (std::map<std::string, bool >::const_iterator val_iter=iter->second.begin();
   	   val_iter != iter->second.end();
   	   val_iter ++) {

	fout << iter->first << " " << val_iter->first << " " << val_iter->second << std::endl;
      }
    }
  }

}
