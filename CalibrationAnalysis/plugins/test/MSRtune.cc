#include "MSRtune.hh"
#include <Config/Config.h>
//#include <Analysis.hh>
#include <Config/ConfGroup.h>
#include <ConfigWrapper/Connectivity.h>
#include <IAnalysisDataServer.hh>
#include <IDataServer.hh>
#include <ExtendedAnalysisResultList_t.hh>
#include <ConfigWrapper/ConnectivityUtil.h>
#include <ConfigWrapper/ConfObjUtil.h>
#include <ConfigWrapper/PixConfigWrapper.h>
#include <exception.hh>

#include <TH2.h>
#include <DataContainer/HistoUtil.h>

#include <TH1.h>
#include <TH2.h>
#include <TGraph.h>
#include <Histo/Histo.h>
#include <sstream>
#include <iostream>
#include <cassert>
#include <set>

#include <DataContainer/GenericHistogramAccess.h>
#include <DataContainer/Average_t.h>
#include <ConfigWrapper/ConfigRef.h>
#include <ConfigWrapper/ConfObjUtil.h>
#include <ConfigWrapper/pixa_exception.h>

#include <DataContainer/HistoAxisInfo_t.h>
#include <DataContainer/HistoExtraInfo_t.h>
#include <DataContainer/HistoExtraInfoList.h>


namespace CAN {

  const std::string MSRtune::s_name("MSRtune");
  const std::string MSRtune::s_rawDataDiff1Name("RAW_DATA_DIFF_1");
  const std::string MSRtune::s_statusVarName("Status");
  const std::string MSRtune::s_hasHistosVarName("HAS_HISTOS");
  const std::string MSRtune::s_hasBocCfgVarName("HAS_BOC_CFG");
  const std::string MSRtune::s_linkInfoVarName("RX_LINKCFG_OK");

  MSRtune::MSRtune(SerialNumber_t serial_number, const ObjectInfo_t &info) 
    : m_config("MSRtune"),
      m_serialNumber(serial_number),
      m_logLevel(kWarnLog),
      m_abort(false)
  {
    m_config.addGroup("general");
    PixLib::ConfGroup &group=m_config["general"];
    group.addBool("use80mbit", m_use80mbit, false, "Analyse an 80MBit mode scan (40MHz clock)", true);
  }

  void MSRtune::requestHistograms(IAnalysisDataServer *data_server,
						const PixA::Pp0LocationBase &/*pp0_location*/,
						const PixA::ModuleLocationBase &module_location) 
  {
    IDataServer *scan_data_server = data_server->getDataServer();

    scan_data_server->requestScanConfig();
    scan_data_server->requestBocConfig();

    scan_data_server->requestHistogram(s_rawDataDiff1Name, module_location );
    
    scan_data_server->requestModuleConfig(module_location);
  }

  void MSRtune::analyse(IAnalysisDataServer *data_server,
				      const PixA::Pp0LocationBase &pp0_location,
				      const PixA::ModuleLocationBase &module_location,
				      ExtendedAnalysisResultList_t &analysis_results)
  {

    IDataServer *scan_data_server = data_server->getDataServer();


    const PixLib::Config &scan_config = scan_data_server->getScanConfig();
    PixLib::Config &casted_scan_config=const_cast<PixLib::Config &>(scan_config);


    PixA::ConnectivityRef conn( data_server->connectivity() );
    assert( conn ) ;
    const PixLib::ModuleConnectivity *module_conn = conn.findModule(module_location);
    PixLib::ModuleConnectivity *casted_module_conn = const_cast<PixLib::ModuleConnectivity * >(module_conn);
    assert( module_conn );

    HistoInfo_t info;
    scan_data_server->numberOfHistograms(s_rawDataDiff1Name, module_location, info);
    bool has_histogram = ( info.minHistoIndex() < info.maxHistoIndex() );
    if (!has_histogram) return;

    PixA::Index_t index;
    info.makeDefaultIndexList(index);
    
    // there cannot be more than two DTO links
    // if so, the interpretation is wrong
    if (m_logLevel>=kInfoLog) {
      std::cout << "INFO [MSRtune::analyse] " <<  "maxHistoIndex: " << info.maxHistoIndex() << std::endl;
      for (unsigned int i=0; i<index.size(); i++) {
	std::cout << "INFO [MSRtune::analyse] " <<  "maxIndex[" << i << "] :" << info.maxIndex(i) << std::endl;
      }
    }

    assert( info.maxHistoIndex()<=2);

    int oldMsr = 19; //Creating MSR variable, which is set to default and tried to be read from config
    const PixLib::Config &boc_config = scan_data_server->getBocConfig();
    PixLib::Config &casted_boc_config=const_cast<PixLib::Config &>(boc_config);
    std::stringstream config_name;
    config_name << "PixTx" << (casted_module_conn->bocLinkTx()/10);
    PixLib::Config &tx_config( casted_boc_config.subConfig(config_name.str()));
    std::stringstream msrName;
    msrName << "MarkSpace" << ((casted_module_conn->bocLinkTx()%10) + 2);
    oldMsr = confVal<int>(tx_config["Bpm"][ msrName.str() ]);

    bool has_histos[2] = { true, true};
    //    bool is_okay[2] = { false, false};

    std::vector<unsigned int> minsettings;
    std::vector<float> detuned;

    if (index.size()>0) {
      PixA::Index_t min_index(index);
      PixA::Index_t max_index(index.size());
      for (unsigned int i=0; i<index.size()-1; i++) {
	max_index[i]=info.maxIndex(i);
      }
      max_index.back() = info.maxHistoIndex();

      // loop over all indices
      if (m_logLevel>=kInfoLog) std::cout << "INFO [MSRtune::analyse] " <<  "Looping over Index[0] to maximum: " << max_index[0] << std::endl;

      unsigned int old_index = index[index.size()-1] - 1;
      for( ;index[index.size()-1]<max_index[index.size()-1]; ) {
        if(old_index != index[index.size()-1]) {
	  old_index = index[index.size()-1];
	}
        if (m_logLevel>=kInfoLog) std::cout << "INFO [MSRtune::analyse] " <<  "Index[0]: " << index[0] << std::endl;
	// should identify the Viset loop
	// and produce a result per Viset

	try {
	  if (m_logLevel>=kInfoLog) {
	    std::cout << "INFO [MSRtune::analyse] " << PixA::connectivityName( module_conn ) <<  " get histogram  = : ";
	    for (unsigned int i=0; i<index.size(); i++)  {
	      std::cout << index[i] << "(" << max_index[i] << "), ";
	    }
	    std::cout <<std::endl;
	  }
	  const Histo *a_histo = scan_data_server->getHistogram(s_rawDataDiff1Name, PixA::connectivityName( module_conn ), index);

	  if (a_histo) {

	    // create histogram with corrected binning
	    PixA::ConfigHandle wrapped_scan_config( PixA::PixConfigWrapper::wrap(casted_scan_config)); 
	    std::auto_ptr<TH2> rebinned_h2( PixA::createHisto(*a_histo, s_rawDataDiff1Name, wrapped_scan_config.ref(), index) );

	    if (m_logLevel>=kInfoLog) {
	      std::cout << "INFO [MSRtune::analyse] " <<  PixA::connectivityName( module_conn ) << " got new histo : "
			<< rebinned_h2->GetXaxis()->GetXmin() << " - " << rebinned_h2->GetXaxis()->GetXmax() << " / " << rebinned_h2->GetNbinsX()
			<< ", " << rebinned_h2->GetYaxis()->GetXmin() << " - " << rebinned_h2->GetYaxis()->GetXmax() << " / " << rebinned_h2->GetNbinsY()
			<< std::endl;
	    }

	    int minset = -1;
	    double minval = 25000.0 ;
            double val;
	    
            for(int xcoord=1; xcoord<=rebinned_h2->GetNbinsX(); xcoord++) {
              if (m_logLevel>=kInfoLog) std::cout << "INFO [MSRtune::analyse] " <<  PixA::connectivityName( module_conn ) << " xcoord " << xcoord << ": ";
              for(int ycoord=1; ycoord<=rebinned_h2->GetNbinsY(); ycoord++) {
	        if (m_logLevel>=kInfoLog) std::cout << rebinned_h2->GetBinContent( xcoord, ycoord ) << " ";
	        if ((val = fabs(rebinned_h2->GetBinContent( xcoord, ycoord ))) < minval) {
		  minval = val;
		  minset = rebinned_h2->GetYaxis()->GetBinCenter(ycoord);
		}
	      }
	      if (m_logLevel>=kInfoLog) std::cout << std::endl;
            }
	    if (m_logLevel>=kInfoLog) std::cout << "INFO [MSRtune::analyse] " <<  PixA::connectivityName( module_conn )
						 << " has minmum val " << minval << " at setting " << minset << std::endl;
            minsettings.push_back(minset);
	    detuned.push_back(fabs(rebinned_h2->GetBinContent( 1, (oldMsr + 1) )));
	  }
	  else {
	    if (m_logLevel>=kInfoLog) std::cout << "INFO [MSRtune::analyse] Did not get histogram. " <<  std::endl;
	    has_histos[ index.back() ]=false;
	  }
	}
	catch (HistoException &err) {
	  if (m_logLevel>=kInfoLog) std::cout << "INFO [MSRtune::analyse] Caught Histo exception. " <<  std::endl;
	  has_histos[ index.back() ]=false;
	}

	// next index;
	for (unsigned int level_i=0; level_i<index.size(); level_i++) {
	  //	    if (index[level_i]+1<max_index[level_i]) {
	  index[level_i]++;
	  if ((index[level_i]>=max_index[level_i]) && !(level_i==index.size()-1)) {
	    index[level_i]=min_index[level_i];
	  }
	  else {
	    break;
	  }
	}
      }
    }

    bool combined_status = true;
//    for (unsigned int dto_i=info.minHistoIndex(); dto_i< info.maxHistoIndex(); ++dto_i) {
    if(detuned.size() > 0) {
      char Name_with_index[200];
      sprintf(Name_with_index, "Cycle2CycleDiff");
      analysis_results.addValue<float>(Name_with_index, module_location, (float) (detuned[0]));
    }
    if (minsettings.size()>2) {
      char Name_with_index[200];
      sprintf(Name_with_index, "idealMSR");
      analysis_results.addValue<float>(Name_with_index, module_location, (float) (minsettings[0]));
      for(unsigned int i=0; i<minsettings.size(); i++) {
	sprintf(Name_with_index, "IdealMSR_Histo%d", i);
	analysis_results.addValue<float>(Name_with_index, module_location, (float) (minsettings[i]));
      }
    } else {
      char Name_with_index[200];
      sprintf(Name_with_index, "idealMSR");
      analysis_results.addValue<float>(Name_with_index, module_location, (float) (minsettings[0]));
      if (minsettings.size() == 2) {
	sprintf(Name_with_index, "MSRdeviation_0-1");
	float deviation = (float) (((float)minsettings[0]) - ((float) minsettings[1]));
	analysis_results.addValue<float>(Name_with_index, module_location, deviation);
      }
    }
    analysis_results.addValue<bool>(s_statusVarName, module_location, combined_status );
      
  }

}


  
