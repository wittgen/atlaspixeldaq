#include <AnalysisPerX.hh>
#include <ConfigWrapper/ConnectivityUtil.h>

namespace PixLib {
  class ModuleConnectivity;
}

namespace CAN {

  class ObjectInfo_t;
  class IDataServer;

  class MSRtune : public IAnalysisPerModule
  {
  public:
    MSRtune(SerialNumber_t serial_number, const ObjectInfo_t &info);

    void requestHistograms(IAnalysisDataServer *data_server,
			   const PixA::Pp0LocationBase &/*pp0_location*/,
			   const PixA::ModuleLocationBase &module_location);

    void analyse(IAnalysisDataServer *data_server,
		 const PixA::Pp0LocationBase &pp0_location,
		 const PixA::ModuleLocationBase &module_location,
		 ExtendedAnalysisResultList_t &analysis_results);

    PixLib::Config &config() { return m_config; }

    /** Return the name of the analysis.
     */
    const std::string &name() const { return s_name; }

    /** A unique identifier of an analysis. Could for example be the scan id.
     * This identifier is used to localise histograms.
     */
    SerialNumber_t serialNumber() const { return m_serialNumber; }

    void requestAbort() { m_abort = true;}

    void setLogLevel(ELogLevel log_level) { m_logLevel=log_level; }

  protected:
    bool abortRequested() const { return m_abort; };

  private:
    PixLib::Config m_config;

    bool m_use80mbit;

    SerialNumber_t m_serialNumber;
    ELogLevel      m_logLevel;
    bool m_abort;

    static const std::string s_name;
    static const std::string s_rawDataDiff1Name;

    static const std::string s_statusVarName;
    static const std::string s_hasHistosVarName;
    static const std::string s_hasBocCfgVarName;
    static const std::string s_linkInfoVarName;
  };

}
