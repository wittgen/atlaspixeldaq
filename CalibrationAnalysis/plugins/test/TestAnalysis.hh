#ifndef _TestAnalaysis_hh_
#define _TestAnalaysis_hh_

#include <IAnalysisPerROD.hh>
#include <Config/Config.h>
//#include <Analysis.hh>

namespace CAN {

  class ObjectInfo_t;
  class IDataServer;

  class TestAnalysis : public IAnalysisPerROD
  {
  public:
    TestAnalysis(SerialNumber_t serial_number, const ObjectInfo_t &);

    void requestHistograms(IAnalysisDataServer *data_server,
			   const PixA::RODLocationBase &rod_location);

    void analyse(IAnalysisDataServer *data_server,
    		 const PixA::RODLocationBase &rod_location,
		 ExtendedAnalysisResultList_t &analysis_results);

    PixLib::Config &config() { return m_config; }

    /** Return the name of the analysis.
     */
    const std::string &name() const { return s_name; }

    /** A unique identifier of an analysis. Could for example be the scan id.
     * This identifier is used to localise histograms.
     */
    SerialNumber_t serialNumber() const { return m_serialNumber; }

    void requestAbort() { m_abort = true;}

    void setLogLevel(ELogLevel log_level) { m_logLevel = log_level; }


  protected:
    bool abortRequested() const { return m_abort; };

  private:
    std::pair<unsigned int, bool> checkHistograms(IDataServer *scan_data_server,
						  ExtendedAnalysisResultList_t &analysis_results,
						  const std::string &histogram_name,
						  const std::string &connectivity_name);

    PixLib::Config m_config;

    float m_cut;
    bool  m_crash;
    bool  m_allowForMissingHistograms;

    std::string m_crashRodName;
    int   m_sleep;
    float m_failProbability;
    float m_hangProbability;
    std::string m_hangRodName;
    int   m_hangTime;
    std::string m_histogramName;
    bool m_isRodHistogram;
    bool m_needModuleConfig;
    bool m_needBocConfig;
    bool m_needScanConfig;

    bool m_addFloatResult;
    bool m_addStateResult;
    bool m_addAverageResult;
    bool m_addBoolResult;
    bool m_addSpecialPixelMapResult;
    bool m_countInputData;

    SerialNumber_t m_serialNumber;
    ELogLevel m_logLevel;

    bool m_abort;

    static std::string s_name;
    std::string  m_scanName[2];  //CLA
  };

}

#endif
