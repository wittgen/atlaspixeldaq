#include <AnalysisFactory.hh>
#include <TestAnalysis.hh>
#include <TestClassification.hh>
#include <TestPostProcessing.hh>
#include <TestConfigModifier.hh>

DEFINE_SEAL_MODULE ()
DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::TestAnalysis,          "TestAnalysis")
DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::TestClassification,    "TestClassification")
DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::TestPostProcessing,    "TestPostProcessing")
DEFINE_SEAL_PLUGIN (CAN::AnalysisFactory, CAN::TestConfigModifier,    "TestConfigModifier")

