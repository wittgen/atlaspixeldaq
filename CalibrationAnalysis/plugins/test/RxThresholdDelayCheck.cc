#include "RxThresholdDelayCheck.hh"
#include <Config/Config.h>
//#include <Analysis.hh>
#include <Config/ConfGroup.h>
#include <ConfigWrapper/Connectivity.h>
#include <IAnalysisDataServer.hh>
#include <IDataServer.hh>
#include <ExtendedAnalysisResultList_t.hh>
#include <ConfigWrapper/ConnectivityUtil.h>
#include <ConfigWrapper/ConfObjUtil.h>
#include <ConfigWrapper/PixConfigWrapper.h>
#include <exception.hh>

#include <TH2.h>
#include <DataContainer/HistoUtil.h>

#include <TH1.h>
#include <TH2.h>
#include <TGraph.h>
#include <Histo/Histo.h>
#include <sstream>
#include <iostream>
#include <cassert>
#include <set>

#include <DataContainer/GenericHistogramAccess.h>
#include <DataContainer/Average_t.h>
#include <ConfigWrapper/ConfigRef.h>
#include <ConfigWrapper/ConfObjUtil.h>
#include <ConfigWrapper/pixa_exception.h>

#include <DataContainer/HistoAxisInfo_t.h>
#include <DataContainer/HistoExtraInfo_t.h>
#include <DataContainer/HistoExtraInfoList.h>


namespace CAN {

  const std::string RxThresholdDelayCheck::s_name("RxThresholdDelayCheck");
  const std::string RxThresholdDelayCheck::s_rawDataDiff2Name("RAW_DATA_DIFF_2");
  const std::string RxThresholdDelayCheck::s_statusVarName("STATUS");
  const std::string RxThresholdDelayCheck::s_hasHistosVarName("HAS_HISTOS");
  const std::string RxThresholdDelayCheck::s_hasBocCfgVarName("HAS_BOC_CFG");
  const std::string RxThresholdDelayCheck::s_linkInfoVarName("RX_LINKCFG_OK");

  RxThresholdDelayCheck::RxThresholdDelayCheck(SerialNumber_t serial_number, const ObjectInfo_t &info) 
    : m_config("TestAnalysis"),
      m_serialNumber(serial_number),
      m_logLevel(kWarnLog),
      m_abort(false)
  {
    m_config.addGroup("general");
    PixLib::ConfGroup &group=m_config["general"];
    group.addInt("minErrorDistanceDelay", m_minErrorDistanceDelay, 2, "Minimum distance to an error in delay direction", true);
    group.addInt("minErrorDistanceThreshold", m_minErrorDistanceThreshold, 50, "Minimum distance to an error in threshold direction", true);
    group.addFloat("minErrorRadius", m_minErrorRadius, 0.05, "Minimum relative distance (axis range) to an error ", true);
  }

  void RxThresholdDelayCheck::requestHistograms(IAnalysisDataServer *data_server,
						const PixA::Pp0LocationBase &/*pp0_location*/,
						const PixA::ModuleLocationBase &module_location) 
  {
    IDataServer *scan_data_server = data_server->getDataServer();

    scan_data_server->requestScanConfig();
    scan_data_server->requestBocConfig();

    scan_data_server->requestHistogram(s_rawDataDiff2Name, module_location );
    
  }

  void RxThresholdDelayCheck::analyse(IAnalysisDataServer *data_server,
				      const PixA::Pp0LocationBase &pp0_location,
				      const PixA::ModuleLocationBase &module_location,
				      ExtendedAnalysisResultList_t &analysis_results)
  {

    IDataServer *scan_data_server = data_server->getDataServer();


    const PixLib::Config &scan_config = scan_data_server->getScanConfig();
    PixLib::Config &casted_scan_config=const_cast<PixLib::Config &>(scan_config);


    PixA::ConnectivityRef conn( data_server->connectivity() );
    assert( conn ) ;
    const PixLib::ModuleConnectivity *module_conn = conn.findModule(module_location);
    assert( module_conn );

    HistoInfo_t info;
    scan_data_server->numberOfHistograms(s_rawDataDiff2Name, module_location, info);
    bool has_histogram = ( info.minHistoIndex() < info.maxHistoIndex() );
    if (!has_histogram) return;

    // there cannot be more than two DTO links
    // if so, the interpretation is wrong
    std::pair<unsigned int, unsigned int > rx_delay_threshold_list[2];
    assert( info.maxHistoIndex()<=2);
    for (unsigned int dto_i=info.minHistoIndex(); dto_i< info.maxHistoIndex(); dto_i++) {
      rx_delay_threshold_list[dto_i] =  getRxThresholdDelay(*scan_data_server, module_conn, static_cast<PixA::EDataLink>( dto_i) );
    }

    PixA::Index_t index;
    info.makeDefaultIndexList(index);

    bool has_histos[2] = { true, true};
    bool is_okay[2] = { false, false};
    unsigned int max_delta_threshold[2]={0,0};
    unsigned int max_delta_delay[2]={0,0};
    float max_error_free_radius[2] = {0., 0.};
    
    float radius_cut = m_minErrorRadius*m_minErrorRadius;

    if (index.size()>0) {
      PixA::Index_t min_index(index);
      PixA::Index_t max_index(index.size());
      for (unsigned int i=0; i<index.size()-1; i++) {
	max_index[i]=info.maxIndex(i);
      }
      max_index.back() = info.maxHistoIndex();

      // loop over all indices
      for( ;index[0]<max_index[0]; ) {

	// should identify the Viset loop
	// and produce a result per Viset

	try {

	  if (m_logLevel>=kInfoLog) {
	    std::cout << "INFO [RxThresholdDelayCheck::analyse] " << PixA::connectivityName( module_conn ) <<  " get histogram  = : ";
	    for (unsigned int i=0; i<index.size(); i++)  {
	      std::cout << index[i] << "(" << max_index[i] << "), ";
	    }
	    std::cout <<std::endl;
	  }

	  const Histo *a_histo = scan_data_server->getHistogram(s_rawDataDiff2Name, PixA::connectivityName( module_conn ), index);

	  if (a_histo) {

	    // get the RX delay and threshold settings which are in the BOC config which was used for this scan.
	    // the last index indicates the DTO link
	    assert(index.back()<2);
	    const std::pair<unsigned int, unsigned int > &rx_delay_threshold =  rx_delay_threshold_list[index.back()];

	    if (m_logLevel>=kInfoLog) std::cout << "INFO [RxThresholdDelayCheck::analyse] " <<  PixA::connectivityName( module_conn )
						<< " delay = " << rx_delay_threshold.first << " threshold = " << rx_delay_threshold.second << std::endl;

	    if (rx_delay_threshold.first==UINT_MAX) {
	      analysis_results.addValue(s_linkInfoVarName,  module_location, false);
	    }
	    else {

	      // create histogram with corrected binning
	      PixA::ConfigHandle wrapped_scan_config( PixA::PixConfigWrapper::wrap(casted_scan_config)); 
	      std::auto_ptr<TH2> rebinned_h2( PixA::createHisto(*a_histo, s_rawDataDiff2Name, wrapped_scan_config.ref(), index) );

	      if (m_logLevel>=kInfoLog) {
		std::cout << "INFO [RxThresholdDelayCheck::analyse] " <<  PixA::connectivityName( module_conn ) << " got new histo : "
			  << rebinned_h2->GetXaxis()->GetXmin() << " - " << rebinned_h2->GetXaxis()->GetXmax() << " / " << rebinned_h2->GetNbinsX()
			  << ", " << rebinned_h2->GetYaxis()->GetXmin() << " - " << rebinned_h2->GetYaxis()->GetXmax() << " / " << rebinned_h2->GetNbinsY()
			  << std::endl;
	      }

	      unsigned int delay_bin = rebinned_h2->GetXaxis()->FindBin(rx_delay_threshold.first);
	      unsigned int threshold_bin = rebinned_h2->GetYaxis()->FindBin(rx_delay_threshold.second);
	      if (delay_bin > static_cast<unsigned int>(rebinned_h2->GetNbinsX()) ) {
		delay_bin = static_cast<unsigned int>( rebinned_h2->GetNbinsX() );
	      }
	      if (delay_bin<1) {
		delay_bin = 1;
	      }

	      if (threshold_bin > static_cast<unsigned int>( rebinned_h2->GetNbinsY()) ) {
		threshold_bin = static_cast<unsigned int>(rebinned_h2->GetNbinsY());
	      }
	      if (threshold_bin < 1) {
		threshold_bin = 1;
	      }

	      unsigned  max_radius = rebinned_h2->GetNbinsY() - threshold_bin; 
	      if (threshold_bin-1 > max_radius ) {
		max_radius = threshold_bin-1;
	      }
	      if ( rebinned_h2->GetNbinsX() - delay_bin > max_radius ) {
		max_radius = rebinned_h2->GetNbinsX() - delay_bin;
	      }
	      if ( delay_bin-1 > max_radius ) {
		max_radius = delay_bin -1;
	      }

	      if (m_logLevel>=kInfoLog) std::cout << "INFO [RxThresholdDelayCheck::analyse] max_radius =   " << max_radius << std::endl;

	      unsigned int max_delay_distance = 0;
	      unsigned int max_threshold_distance = 0;
	      bool error=false;
	      for (unsigned int radius_i=0; radius_i < max_radius && !error; radius_i++) {

		int start_bin_x = ( radius_i >= static_cast<unsigned int>(rebinned_h2->GetNbinsX()/2) ? delay_bin - rebinned_h2->GetNbinsX()/2 :  delay_bin - radius_i);
		int end_bin_x = ( radius_i >= static_cast<unsigned int>(rebinned_h2->GetNbinsX()/2) ? delay_bin + rebinned_h2->GetNbinsX()/2 :  delay_bin + radius_i);

		if (m_logLevel>=kInfoLog) std::cout << "INFO [RxThresholdDelayCheck::analyse] "
						    <<  PixA::connectivityName( module_conn ) << " "<< radius_i << " x-range  =  "
						    << start_bin_x << " - " << end_bin_x  << std::endl;

		unsigned int start_bin_y = ( threshold_bin-1 > radius_i ? threshold_bin - radius_i : 1);
		unsigned int end_bin_y = ( threshold_bin+radius_i <= static_cast<unsigned int>(rebinned_h2->GetNbinsY()) 
					   ?   threshold_bin + radius_i 
					   : rebinned_h2->GetNbinsY());

		if (m_logLevel>=kInfoLog) std::cout << "INFO [RxThresholdDelayCheck::analyse] " << radius_i
						    << " y-range  =  " << start_bin_y << " - " << end_bin_y  << std::endl;

		for (int binx_i = start_bin_x ; binx_i <= end_bin_x && !error; binx_i++) {

		  unsigned int eff_bin_x = binx_i;
		  if (binx_i < 1 ) {
		    eff_bin_x = rebinned_h2->GetNbinsX() + binx_i;
		  }
		  if (binx_i > rebinned_h2->GetNbinsX() ) {
		    eff_bin_x = binx_i - rebinned_h2->GetNbinsX();
		  }
		  assert(eff_bin_x >=1);
		  assert(eff_bin_x <= static_cast<unsigned int>(rebinned_h2->GetNbinsX()));

		  for (unsigned int biny_i = start_bin_y ; biny_i <= end_bin_y; biny_i++) {


		    double bin_content = rebinned_h2->GetBinContent( rebinned_h2->GetBin( eff_bin_x, biny_i) );
		    if (bin_content>0) {

		      max_threshold_distance = static_cast<unsigned int>( std::abs( rebinned_h2->GetYaxis()->GetBinCenter(biny_i) - rebinned_h2->GetYaxis()->GetBinCenter(threshold_bin) ) );
		      max_delay_distance = static_cast<unsigned int>( std::abs( rebinned_h2->GetXaxis()->GetBinCenter(eff_bin_x) - rebinned_h2->GetXaxis()->GetBinCenter(delay_bin) ) );

		      float radius=m_minErrorRadius*2;
		      if (rebinned_h2->GetYaxis()->GetXmax() > rebinned_h2->GetYaxis()->GetXmin() && rebinned_h2->GetXaxis()->GetXmax() > rebinned_h2->GetXaxis()->GetXmin()) {
			float rel_dist_y = max_threshold_distance/(rebinned_h2->GetYaxis()->GetXmax() - rebinned_h2->GetYaxis()->GetXmin());
			float rel_dist_x = max_delay_distance/(rebinned_h2->GetXaxis()->GetXmax() - rebinned_h2->GetXaxis()->GetXmin());
			radius = rel_dist_x * rel_dist_x + rel_dist_y * rel_dist_y;
			if (radius > max_error_free_radius[index.back()] ) {
			  max_error_free_radius[index.back()]  = radius;
			}
		      }

		      if (max_threshold_distance > max_delta_threshold[ index.back() ]) {
			max_delta_threshold[ index.back() ] = max_threshold_distance;
		      }
		      if (max_delay_distance > max_delta_delay[ index.back() ]) {
			max_delta_delay[ index.back() ] = max_delay_distance;
		      }

		      if (m_logLevel>=kInfoLog) {
			std::cout << "INFO [RxThresholdDelayCheck::analyse] reach error = x " << eff_bin_x << " delay = " << rebinned_h2->GetXaxis()->GetBinCenter(eff_bin_x)
				  << ", y= " << biny_i  << " threshold = " << rebinned_h2->GetYaxis()->GetBinCenter(biny_i)
				  << " delta_threshold = " << max_threshold_distance << " delta_delay = " << max_delay_distance << std::endl;
		      }


		      bool this_histogram_ok = (   max_delay_distance > static_cast<unsigned int>(m_minErrorDistanceDelay)
						&& max_threshold_distance>static_cast<unsigned int>(m_minErrorDistanceThreshold) 
						&& radius > radius_cut);
		      is_okay[ index.back() ] = this_histogram_ok;

		      if (m_logLevel>=kInfoLog) {
			std::cout << "INFO [RxThresholdDelayCheck::analyse] reach error = x " << eff_bin_x << " delay = " << rebinned_h2->GetXaxis()->GetBinCenter(eff_bin_x)
				  << ", y= " << biny_i  << " threshold = " << rebinned_h2->GetYaxis()->GetBinCenter(biny_i)
				  << " delta_threshold = " << max_threshold_distance << " delta_delay = " << max_delay_distance 
				  << " radius = " << sqrt(radius) << " okay ? " << is_okay[index.back()]
				  << std::endl;
		      }

		      error=true;
		      break;
		    }

		  }
		}
	      }
	    }
	  }
	  else {
	    if (m_logLevel>=kInfoLog) std::cout << "INFO [RxThresholdDelayCheck::analyse] Did not get histogram. " <<  std::endl;
	    has_histos[ index.back() ]=false;
	  }
	}
	catch (HistoException &err) {
	  if (m_logLevel>=kInfoLog) std::cout << "INFO [RxThresholdDelayCheck::analyse] Caught Histo exception. " <<  std::endl;
	  has_histos[ index.back() ]=false;
	}

	// next index;
	unsigned int level_i=index.size();
	for (; level_i-->0; ) {
	  //	    if (index[level_i]+1<max_index[level_i]) {
	  index[level_i]++;
	  if (index[level_i]>=max_index[level_i] && !level_i==0) {
	    index[level_i]=min_index[level_i];
	  }
	  else {
	    break;
	  }
	}
      }
    }

    bool combined_status = true;
    for (unsigned int dto_i=info.minHistoIndex(); dto_i< info.maxHistoIndex(); ++dto_i) {
      assert( dto_i<2) ;
      std::stringstream status_name;
      status_name << s_statusVarName << "_DTO" << dto_i+1;

      std::stringstream max_delta_thr_name;
      max_delta_thr_name << "MAX_DELTA_THR" << "_DTO" << dto_i+1;

      std::stringstream max_delta_delay_name;
      max_delta_delay_name << "MAX_DELTA_DELAY" << "_DTO" << dto_i+1;

      std::stringstream max_radius_name;
      max_radius_name << "MAX_RADIUS" << "_DTO" << dto_i+1;

      std::stringstream has_histos_name;
      has_histos_name << s_hasHistosVarName << "_DTO" << dto_i+1;

      analysis_results.addValue(status_name.str(),module_location, is_okay[dto_i]);
      analysis_results.addValue(has_histos_name.str(), module_location, has_histos[dto_i]);
      if (m_logLevel>=kInfoLog) std::cout << "INFO [RxThresholdDelayCheck::analyse] " << module_location  << " / " << dto_i
					  << " has histos = " << has_histos[dto_i]  << " status = " << is_okay[dto_i] << std::endl;

      analysis_results.addValue(max_delta_thr_name.str(),module_location, static_cast<float>(max_delta_threshold[dto_i]));
      analysis_results.addValue(max_delta_delay_name.str(),module_location, static_cast<float>(max_delta_delay[dto_i]));
      analysis_results.addValue(max_radius_name.str(),module_location, static_cast<float>(sqrt(max_error_free_radius[dto_i])));
      combined_status &= is_okay[dto_i];
    }
    analysis_results.addValue(s_statusVarName, module_location, combined_status );
      
  }


  std::pair<unsigned int, unsigned int> RxThresholdDelayCheck::getRxThresholdDelay( IDataServer &scan_data_server, const PixLib::ModuleConnectivity *module_conn, PixA::EDataLink data_link )
  {

    std::pair<unsigned int, unsigned int> delay_threshold;
    delay_threshold.first = UINT_MAX;
    delay_threshold.second = UINT_MAX;
    // // whether a scan config exists is checked before.
    //       try {
    // 	bool altlink = confVal<unsigned int>(scan_data_server.getScanConfig()["general"]["useAltModLinks"]);
    // 	if (altlink ) {
    // 	  data_link = (data_link == PixA::kDTO1 ?  PixA::kDTO2 : PixA::kDTO2);
    // 	}
    //       }
    //       catch (...){
    //       }

    PixA::ModulePluginConnection other_module_rx_connection_dtox = PixA::rxConnection(module_conn, static_cast<PixA::EDataLink>( (data_link+1)%2) );
    PixA::ModulePluginConnection module_rx_connection_dtox = PixA::rxConnection(module_conn, data_link);
    unsigned int rx_channel = module_rx_connection_dtox.channel();
    unsigned int rx_plugin = module_rx_connection_dtox.plugin();

    const PixLib::Pp0Connectivity *pp0_conn(PixA::pp0(module_conn));
    if (pp0_conn) {
      unsigned int pp0_rx_plugin = PixA::rxPlugin(pp0_conn, data_link);
      if (pp0_rx_plugin != module_rx_connection_dtox.plugin() && pp0_rx_plugin == other_module_rx_connection_dtox.plugin()) {
	rx_channel = other_module_rx_connection_dtox.channel();
	rx_plugin  = other_module_rx_connection_dtox.plugin();
      }
    }
	
    //       if (rx_plugin>=4) {
    // 	// nothing useful in the module connectivity 
    // 	// try the pp0
      
    // 	const PixLib::Pp0Connectivity *pp0_conn(PixA::pp0(module_conn));
    // 	if (pp0_conn) {
    // 	  unsigned int pp0_rx_plugin = PixA::rxPlugin(pp0_conn, data_link);
    // 	  if ( /*(rx_channel>=10 || rx_plugin>=4) &&*/ (pp0_rx_plugin<4) ) {
    // 	    rx_plugin =  pp0_rx_plugin;
    // 	  }
    // 	}
    //       }

    if (m_logLevel>=kInfoLog) {
      std::cout << "INFO [RxThresholdDelayCheck::getRxThresholdDelay] " <<  PixA::connectivityName(module_conn)<<  " / " << (data_link%2) << " : plugin = " 
		<< module_rx_connection_dtox.plugin() << " channel = " << module_rx_connection_dtox.channel() << std::endl;
      std::cout << "INFO [RxThresholdDelayCheck::getRxThresholdDelay] " <<  PixA::connectivityName(module_conn)<<  " / " << (data_link+1)%2 << " : plugin = " 
		<< other_module_rx_connection_dtox.plugin() << " channel = " << other_module_rx_connection_dtox.channel() << std::endl;
      std::cout << "INFO [RxThresholdDelayCheck::getRxThresholdDelay] " <<  PixA::connectivityName(module_conn)<<  " / " << data_link
		<< " channel : plugin = " << rx_plugin << " channel = " << rx_channel << std::endl;
    }
    if (rx_channel>=10 || rx_plugin>=4) {
      return delay_threshold;
    }

    //      unsigned int min_delay_distance=0;
    //      unsigned int min_threshold_distance=0;

    // are there more than one Viset ?
    // if so we just require one Viset to be good (should do analysis per PP0)

    // get current delay and threshold from BOC config

    try {

      std::stringstream config_name;
      config_name << "PixRx" << rx_plugin;

      //	const PixLib::Config &boc_config = scan_data_server.getBocConfig();
      PixLib::Config &casted_boc_config = const_cast<PixLib::Config &>( scan_data_server.getBocConfig() );
      PixLib::Config &rx_config( casted_boc_config.subConfig(config_name.str()));

      {
	std::stringstream var_name;
	var_name << "DataDelay" << rx_channel+2;
	delay_threshold.first = confVal<int>(rx_config["General"][var_name.str()]);
      }

      {
	std::stringstream var_name;
	var_name << "RxThreshold" << rx_channel+2;
	delay_threshold.second = confVal<int>(rx_config["Opt"][var_name.str()]);
      }
    }
    catch (...) {
    }

    return delay_threshold;

  }

}


  
