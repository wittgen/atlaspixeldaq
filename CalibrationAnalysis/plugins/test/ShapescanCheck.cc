#include "ShapescanCheck.hh"
#include <Config/Config.h>
//#include <Analysis.hh>
#include <Config/ConfGroup.h>
#include <ConfigWrapper/Connectivity.h>
#include <IAnalysisDataServer.hh>
#include <IDataServer.hh>
#include <ExtendedAnalysisResultList_t.hh>
#include <ConfigWrapper/ConnectivityUtil.h>
#include <ConfigWrapper/ConfObjUtil.h>
#include <ConfigWrapper/PixConfigWrapper.h>
#include <exception.hh>


#include <TH2.h>
#include <DataContainer/HistoUtil.h>

#include <TH1.h>
#include <TH2.h>
#include <TGraph.h>
#include <Histo/Histo.h>
#include <sstream>
#include <iostream>
#include <cassert>
#include <set>

#include <DataContainer/GenericHistogramAccess.h>
#include <DataContainer/Average_t.h>
#include <ConfigWrapper/ConfigRef.h>
#include <ConfigWrapper/ConfObjUtil.h>
#include <ConfigWrapper/pixa_exception.h>

#include <DataContainer/HistoAxisInfo_t.h>
#include <DataContainer/HistoExtraInfo_t.h>
#include <DataContainer/HistoExtraInfoList.h>


namespace CAN {

  const std::string ShapescanCheck::s_name("ShapescanCheck");
  const std::string ShapescanCheck::s_rawData2Name("RAW_DATA_2");
  const std::string ShapescanCheck::s_statusVarName("STATUS");
  const std::string ShapescanCheck::s_hasHistosVarName("HAS_HISTOS");
  const std::string ShapescanCheck::s_hasBocCfgVarName("HAS_BOC_CFG");
  const std::string ShapescanCheck::s_linkInfoVarName("RX_LINKCFG_OK");

  ShapescanCheck::ShapescanCheck(SerialNumber_t serial_number, const ObjectInfo_t &info) 
    : m_config("ShapescanAnalysis"),
      m_serialNumber(serial_number),
      m_logLevel(kInfoLog),
      m_abort(false)
  {
    m_config.addGroup("general");
    PixLib::ConfGroup &group=m_config["general"];
    group.addBool("use80mbit", m_use80mbit, false, "Analyse an 80MBit mode scan (40MHz clock)", true);
  }

  void ShapescanCheck::requestHistograms(IAnalysisDataServer *data_server,
						const PixA::Pp0LocationBase &/*pp0_location*/,
						const PixA::ModuleLocationBase &module_location) 
  {
    IDataServer *scan_data_server = data_server->getDataServer();

    scan_data_server->requestScanConfig();
    scan_data_server->requestBocConfig();

    scan_data_server->requestHistogram(s_rawData2Name, module_location );
    
  }

  void ShapescanCheck::analyse(IAnalysisDataServer *data_server,
				      const PixA::Pp0LocationBase &pp0_location,
				      const PixA::ModuleLocationBase &module_location,
				      ExtendedAnalysisResultList_t &analysis_results)
  {

    IDataServer *scan_data_server = data_server->getDataServer();


    const PixLib::Config &scan_config = scan_data_server->getScanConfig();
    PixLib::Config &casted_scan_config=const_cast<PixLib::Config &>(scan_config);

    std::vector<float> something;

    bool uniform = confVal<bool>(casted_scan_config["loops"]["loopVarUniformLoop_2"]);
    if (uniform)  {
      int NSteps = confVal<int>(casted_scan_config["loops"]["loopVarNStepsLoop_2"]);
//       m_NvcalSteps = 6;
      float min_val = confVal<float>(casted_scan_config["loops"]["loopVarMinLoop_2"]);
      float max_val = confVal<float>(casted_scan_config["loops"]["loopVarMaxLoop_2"]);
      float bin_width = max_val-min_val;
      if (NSteps > 1) {
	bin_width /= (NSteps-1);
      }
      else {
	if (min_val==max_val) bin_width=0.5 ;
      }
      float val = min_val;
      for (int step_i=0; step_i!=NSteps; ++step_i) {
	something.push_back(val);
	val += bin_width;
      }
    }
    else {
      something = confVal<std::vector<float> >(casted_scan_config["loops"][std::string("loopVarValuesLoop_2")]);
    }


    PixA::ConnectivityRef conn( data_server->connectivity() );
    assert( conn ) ;
    const PixLib::ModuleConnectivity *module_conn = conn.findModule(module_location);
    assert( module_conn );

    HistoInfo_t info;
    scan_data_server->numberOfHistograms(s_rawData2Name, module_location, info);
    bool has_histogram = ( info.minHistoIndex() < info.maxHistoIndex() );
    if (!has_histogram) return;

    unsigned int zeros[2]={0, 0};
    unsigned int risingedges[2] = {0,0};
    unsigned int fallingedges[2] = {0,0};
    double maxval[2] = {0.,0.}, minval[2] = {0.,0.};

    PixA::Index_t index;
    info.makeDefaultIndexList(index);

    // there cannot be more than two DTO links
    // if so, the interpretation is wrong
    if (m_logLevel>=kInfoLog) {
      std::cout << "INFO [ShapescanCheck::analyse] " <<  "maxHistoIndex: " << info.maxHistoIndex() << std::endl;
      for (unsigned int i=0; i<index.size(); i++) {
	std::cout << "INFO [ShapescanCheck::analyse] " <<  "maxIndex[" << i << "] :" << info.maxIndex(i) << std::endl;
      }
    }

    assert( info.maxHistoIndex()<=2);

    bool has_histos[2] = { true, true};
    //    bool is_okay[2] = { false, false};

    std::vector<std::vector<unsigned int> > rising_edges;
    std::vector<std::vector<unsigned int> > falling_edges;
    std::vector<std::vector<unsigned int> > signal_width;
    float slope_rising, slope_falling;

    if (index.size()>0) {
      PixA::Index_t min_index(index);
      PixA::Index_t max_index(index.size());
      for (unsigned int i=0; i<index.size()-1; i++) {
	max_index[i]=info.maxIndex(i);
      }
      max_index.back() = info.maxHistoIndex();

      // loop over all indices
      if (m_logLevel>=kInfoLog) std::cout << "INFO [ShapescanCheck::analyse] " <<  "Looping over Index[0] to maximum: " << max_index[0] << std::endl;

      unsigned int old_index = index[index.size()-1] - 1;
      for( ;index[index.size()-1]<max_index[index.size()-1]; ) {
        if(old_index != index[index.size()-1]) {
	  signal_width.push_back(std::vector<unsigned int>());
	  rising_edges.push_back(std::vector<unsigned int>());
          falling_edges.push_back(std::vector<unsigned int>());
	  old_index = index[index.size()-1];
	}
        if (m_logLevel>=kInfoLog) std::cout << "INFO [ShapescanCheck::analyse] " <<  "Index[0]: " << index[0] << std::endl;
	// should identify the Viset loop
	// and produce a result per Viset

	try {
	  if (m_logLevel>=kInfoLog) {
	    std::cout << "INFO [ShapescanCheck::analyse] " << PixA::connectivityName( module_conn ) <<  " get histogram  = : ";
	    for (unsigned int i=0; i<index.size(); i++)  {
	      std::cout << index[i] << "(" << max_index[i] << "), ";
	    }
	    std::cout <<std::endl;
	  }
	  const Histo *a_histo = scan_data_server->getHistogram(s_rawData2Name, PixA::connectivityName( module_conn ), index);

	  if (a_histo) {

	    // create histogram with corrected binning
	    PixA::ConfigHandle wrapped_scan_config( PixA::PixConfigWrapper::wrap(casted_scan_config)); 
	    std::auto_ptr<TH2> rebinned_h2( PixA::createHisto(*a_histo, s_rawData2Name, wrapped_scan_config.ref(), index) );

	    if (m_logLevel>=kInfoLog) {
	      std::cout << "INFO [ShapescanCheck::analyse] " <<  PixA::connectivityName( module_conn ) << " got new histo : "
			<< rebinned_h2->GetXaxis()->GetXmin() << " - " << rebinned_h2->GetXaxis()->GetXmax() << " / " << rebinned_h2->GetNbinsX()
			<< ", " << rebinned_h2->GetYaxis()->GetXmin() << " - " << rebinned_h2->GetYaxis()->GetXmax() << " / " << rebinned_h2->GetNbinsY()
			<< std::endl;
	    }
            if ( (rebinned_h2->GetNbinsX() != 50) || (rebinned_h2->GetNbinsY() < 6) ) {
	      if (m_logLevel>=kInfoLog) std::cout << "INFO [ShapescanCheck::analyse] "
						  <<  PixA::connectivityName( module_conn ) << " didn't contain correctly sized histograms" << std::endl;
	      analysis_results.addValue(s_linkInfoVarName,  module_location, false);
	    } else {

//	      unsigned int delay_bin = rebinned_h2->GetXaxis()->FindBin(rx_delay_threshold.first);
//	      unsigned int threshold_bin = rebinned_h2->GetYaxis()->FindBin(rx_delay_threshold.second);

//            double bin_content = rebinned_h2->GetBinContent( rebinned_h2->GetBin( eff_bin_x, biny_i) );

	      if (m_logLevel>=kInfoLog) std::cout << "INFO [ShapescamnCheck::analyse] " <<  PixA::connectivityName( module_conn )
						  << " seemed to contain a more or less normal SHAPESCAN ... analysing" << std::endl;
	      
// Find edges in the scan

              unsigned int j,k;
              std::vector<double> integral( rebinned_h2->GetNbinsX() );
              double val;

	      for (j=1; j<static_cast<unsigned int>(rebinned_h2->GetNbinsX()); j++) {
	        integral[j] = 0.0;
		for (k=1; k<static_cast<unsigned int>(rebinned_h2->GetNbinsY()); k++) {
		  val = rebinned_h2->GetBinContent( j, k );
                  integral[j] += val;
//		  if (j == k) std::cout << "INFO [ShapescanCheck::analyse] Histoval[" << j << ", " << k << "]: " << val << std::endl;
		  if (val < 0.5) zeros[index.back()]++;
        	}
		if ((j == 1) || (integral[j] < minval[index.back()])) minval[index.back()] = integral[j];
		if (integral[j] > maxval[index.back()]) maxval[index.back()] = integral[j];
              }

              double midval = (maxval[index.back()] + minval[index.back()]) / 2.0;
              if (m_logLevel>=kInfoLog) std::cout << "INFO [ShapescanCheck::analyse] has a maximum signal height of "
						  << maxval[index.back()] << " observed, noise floor is at " << minval[index.back()] << std::endl;
              risingedges[index.back()] = 0;
	      fallingedges[index.back()] = 0;
	      for (j=1; j<static_cast<unsigned int>(rebinned_h2->GetNbinsX()); j++) {
	        if ((integral[((j-1) % ((int) rebinned_h2->GetNbinsX() - 1))+1] < midval) && (integral[((j % ((int) rebinned_h2->GetNbinsX() - 1)) + 1 )] >= midval)) {
		  risingedges[index.back()]++;
                  if (m_logLevel>=kInfoLog) std::cout << "INFO [ShapescanCheck::analyse] has a rising edge at  "
						      << (((j-1) % ((int) rebinned_h2->GetNbinsX() - 1))+1) << std::endl;
//		  std::cout << "INFO [ShapescanCheck::analyse] had its previous rising edge at  " << rising_edges.back().rbegin() << std::endl;
//		  std::cout << "INFO [ShapescanCheck::analyse] had a slope of " << slope_rising << " between settings: " << something << " " << something_else << std::endl;
		  rising_edges.back().push_back((unsigned int) (((j-1) % ((int) rebinned_h2->GetNbinsX() - 1))+1));
		}
	        if ((integral[((j-1) % ((int) rebinned_h2->GetNbinsX() - 1))+1] >= midval) && (integral[((j % ((int) rebinned_h2->GetNbinsX() - 1)) + 1 )] < midval)) {
		  fallingedges[index.back()]++;
                  if (m_logLevel>=kInfoLog) std::cout << "INFO [ShapescanCheck::analyse] has a falling edge at " << (((j-1) % ((int) rebinned_h2->GetNbinsX() - 1))+1) << std::endl;
//		  std::cout << "INFO [ShapescanCheck::analyse] had its previous falling edge at  " << falling_edges.back().rbegin() << std::endl;
		  falling_edges.back().push_back((unsigned int) (((j-1) % ((int) rebinned_h2->GetNbinsX() - 1))+1));
		}
              }

              signal_width.back().push_back(((falling_edges.back().back() + 50) - rising_edges.back().back()) % 50);

              if (m_logLevel>=kInfoLog) std::cout << "INFO [ShapescanCheck::analyse] has " << risingedges[index.back()]
						  << " rising edges and " << fallingedges[index.back()] << " falling edges" << std::endl;
	      
            }
	  }
	  else {
	    if (m_logLevel>=kInfoLog) std::cout << "INFO [ShapescanCheck::analyse] Did not get histogram. " <<  std::endl;
	    has_histos[ index.back() ]=false;
	  }
	}
	catch (HistoException &err) {
	  if (m_logLevel>=kInfoLog) std::cout << "INFO [ShapescanCheck::analyse] Caught Histo exception. " <<  std::endl;
	  has_histos[ index.back() ]=false;
	}

	// next index;
	for (unsigned int level_i=0; level_i<index.size(); level_i++) {
	  //	    if (index[level_i]+1<max_index[level_i]) {
	  index[level_i]++;
	  if (index[level_i]>=max_index[level_i] && !level_i==index.size()-1) {
	    index[level_i]=min_index[level_i];
	  }
	  else {
	    break;
	  }
	}
      }
    }

    std::ofstream outputfile;
    char filename[200];
    std::string modulename(PixA::connectivityName( module_conn ));
    if (m_logLevel>=kInfoLog) std::cout << "INFO [ShapescanCheck::analyse] Dumping dat for module "
                                        << PixA::connectivityName( module_conn ) << "into file: " << filename << std::endl;
  if (m_logLevel>=kDiagnosticsLog) {
    sprintf(filename, "dumpedinfo_%s.csv", modulename.c_str());
    outputfile.open(filename);
    for(std::vector<std::vector<unsigned int> >::const_iterator variable_iter = rising_edges.begin();
        variable_iter != rising_edges.end();
	variable_iter++) {
      outputfile << "Rising edges of " << PixA::connectivityName( module_conn ) << " at: " << std::endl;
      int i=0;
      for(std::vector<unsigned int>::const_iterator link_iter = variable_iter->begin();
          link_iter != variable_iter->end();
	  link_iter++) {
	outputfile << i << "\t" << *link_iter << std::endl;
	i++;
      }
      outputfile <<  std::endl;
    }

    for(std::vector<std::vector<unsigned int> >::const_iterator variable_iter = falling_edges.begin();
        variable_iter != falling_edges.end();
	variable_iter++) {
      outputfile << "Falling edges of " << PixA::connectivityName( module_conn ) << " at: " << std::endl;
      int i=0;
      for(std::vector<unsigned int>::const_iterator link_iter = variable_iter->begin();
          link_iter != variable_iter->end();
	  link_iter++) {
	outputfile << i << "\t" << *link_iter << std::endl;
	i++;
      }
      outputfile <<  std::endl;
    }

    for(std::vector<std::vector<unsigned int> >::const_iterator variable_iter = signal_width.begin();
        variable_iter != signal_width.end();
	variable_iter++) {
      outputfile << "Signal width of " << PixA::connectivityName( module_conn ) << " at: " << std::endl;
      int i=0;
      for(std::vector<unsigned int>::const_iterator link_iter = variable_iter->begin();
          link_iter != variable_iter->end();
	  link_iter++) {
	outputfile << i << "\t" << *link_iter << std::endl;
	i++;
      }
      outputfile <<  std::endl;
    }

    outputfile.close();
  }

    bool combined_status = true;
    for (unsigned int dto_i=info.minHistoIndex(); dto_i< info.maxHistoIndex(); ++dto_i) {
      assert( dto_i<2) ;
      char Name_with_index[200];
      sprintf(Name_with_index, "Rising_edges_found_Link_%d", dto_i);
      analysis_results.addValue<float>(Name_with_index, module_location, (float) (risingedges[dto_i]));
      sprintf(Name_with_index, "Falling_edges_found_Link_%d", dto_i);
      analysis_results.addValue<float>(Name_with_index, module_location, (float) (fallingedges[dto_i]));
      sprintf(Name_with_index, "Minimum_integral_value_Link_%d", dto_i);
      analysis_results.addValue<float>(Name_with_index, module_location, (float) (minval[dto_i]));
      sprintf(Name_with_index, "Maximum_integral_value_Link_%d", dto_i);
      analysis_results.addValue<float>(Name_with_index, module_location, (float) (maxval[dto_i]));
      sprintf(Name_with_index, "Zeros_Link_%d", dto_i);
      analysis_results.addValue<float>(Name_with_index, module_location, (float) (zeros[dto_i]));
    }
    analysis_results.addValue<bool>(s_statusVarName, module_location, combined_status );
      
  }

}


  
