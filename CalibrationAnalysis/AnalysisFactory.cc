#include "AnalysisFactory.hh"
#include "exception.hh"

//#include <PluginManager/PluginManager.h>


// same definitions are done by seal and omni

#ifdef HAVE_NAMESPACES
#  undef HAVE_NAMESPACES
#endif
#ifdef HAVE_BOOL
#  undef HAVE_BOOL
#endif
#ifdef HAVE_DYNAMIC_CAST
#  undef HAVE_DYNAMIC_CAST
#endif

#include "AnalysisInfo_t.hh"
#include "IObjectConfigDb.hh"
#include "IAnalysisObject.hh"
//#include <PluginManager/PluginManager.h>
//#include <PluginManager/ModuleCache.h>

#include <memory>

namespace CAN {

  AnalysisFactory::AnalysisFactory()
    : AnalysisPluginFactory ("AnalysisFactory")
  {
    //    seal::PluginManager::get ()->initialise ();
  }

    IAnalysisObject *AnalysisFactory::create(SerialNumber_t serial_number, const ObjectInfo_t &object_info, IObjectConfigDb *db)
  {
    std::unique_ptr<IAnalysisObject> an_analysis( AnalysisPluginFactory::create (object_info.name(), serial_number, object_info) );

    if (!an_analysis.get()) {
      std::string message(" Failed to create analysis object of name ");
      message += object_info.name() ;
      message += ". Verify plugin and linker path (environment variables SEAL_PLUGINS, LD_LIBRARY_PATH). You may have to check the .cache file in the directory where your plugin in located.";
      //      throw MRSException("FACT_no_object", MRS_FATAL, "CAN::AnalysisFactory::create",message);
      throw MRSException(PixLib::PixMessages::FATAL, "CAN::AnalysisFactory::create",message);
    }
    // @todo in principle the config db should take care of correctly setting the config.
    //    an_analysis->config().reset();
    db->readConfig(object_info, an_analysis->config());
    return an_analysis.release();
  }
    IAnalysisObject *AnalysisFactory::create(SerialNumber_t serial_number, const ObjectInfo_t &object_info, IObjectConfigDb *db, std::string string)
    {
//        std::cout<<"creation starts here"<<std::endl;
        std::unique_ptr<IAnalysisObject> an_analysis( AnalysisPluginFactory::create (object_info.name(), serial_number, object_info) );
 //       std::cout<<" creation ends here"<<std::endl;

        if (!an_analysis.get()) {
            std::string message(" Failed to create analysis object of name ");
            message += object_info.name() ;
            message += ". Verify plugin and linker path (environment variables SEAL_PLUGINS, LD_LIBRARY_PATH). You may have to check the .cache file in the directory where your plugin in located.";
            //throw MRSException("FACT_no_object", MRS_FATAL, "CAN::AnalysisFactory::create",message);
	    throw MRSException(PixLib::PixMessages::FATAL, "CAN::AnalysisFactory::create",message);
        }
        // @todo in principle the config db should take care of correctly setting the config.
        //    an_analysis->config().reset();
//        std::cout<<" ti chiamo iooooooooooooooooooo"<<std::endl;
        db->readConfig(object_info, an_analysis->config(), string);
//        std::cout<<" ti chiamo ioo  ooo  ooo  ooo  ooo  ooo  oo"<<std::endl;
        return an_analysis.release();
    }

  void AnalysisFactory::unloadAll() 
  {

    //    plugin_mg->refresh();
  }


  AnalysisFactory *AnalysisFactory::s_instance=NULL;


}
