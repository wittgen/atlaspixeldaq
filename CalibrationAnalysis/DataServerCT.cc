#include <memory>

#include "DataServerCT.hh"

#include "sendMessage.hh"
#include "exception.hh"
#include "PixUtilities/PixMessages.h"
#include <iomanip>
#include <sstream>
#include "DataContainer/PixDbHistoAccessor.h"
#include "DataContainer/PixDbDataContainer.h"
#include "DataContainer/HistoUtil.h"

#include "MasterDataServerCT.hh"



namespace CAN {

  DataServerCT::DataServerCT(PixA::ConnectivityRef &conn, const PixA::RodLocationBase &rod_name, 
	       MasterDataServerCT *master) 
    : m_connectivity(conn),
      m_rodName(rod_name),
      m_master(master)
  { 
    std::cout << "INFO [DataServerCT::ctor] new data server ," << static_cast<void *>(this) << ", "
	      << " for ROD " << m_rodName << ". Has connectivity ? " << ((m_connectivity) ? "yes" : "no") << std::endl;
  }

  DataServerCT::~DataServerCT() {
    std::cout << "INFO [DataServerCT::dtor] destructed data server for ROD " << m_rodName << "." << std::endl;
    if(streamer) delete streamer;
    if(listener) delete listener;
  }


  std::list<std::string> DataServerCT::getpp0Names() 
  {
    std::map<std::string,PixLib::Histo> histoMap;
    GetHistoMap(&histoMap); //to initialize

    streamer->read(); 

    m_connectivity = listener->GetConnectivity(); //really needed somewhere??

    return listener->Getpp0Names();
  } 

  void DataServerCT::requestHistogram(const std::string & histo_type,
				      const std::string & pp0_name) {
//     std::map<std::string,PixLib::Histo> histoMap;
//     GetHistoMap(&histoMap); 
    //streamer->read(pp0_name); //only read the requested pp0,  //now done in getPP0s for all 

    if(!listener->Found_pp0(pp0_name)) { 
      std::cout << "DataServerCT::requestHistogram  WARNING the requested pp0 " << pp0_name << " was not found!" << std::endl;      
      //      throw MRSException("DATASRV_NOTFOUNDPP0",MRS_FATAL,"CAN::DataServerCT::requestHistogram","pp0 not found.");
      throw MRSException(PixLib::PixMessages::FATAL,"CAN::DataServerCT::requestHistogram","pp0 not found.");
      //      return false;
    }
    if(!listener->Found_type(histo_type)) {
      std::cout << "DataServerCT::requestHistogram  WARNING the requested histogram type " << histo_type << " was not found!" << std::endl;      
      //      throw MRSException("DATASRV_NOTFOUNDPP0",MRS_FATAL,"CAN::DataServerCT::requestHistogram","pp0 not found.");
      throw MRSException(PixLib::PixMessages::FATAL,"CAN::DataServerCT::requestHistogram","pp0 not found.");
      //      return false;
    }
    //    return true; //request sucessfull
  }

  void DataServerCT::requestUIntAnalysisResult(const std::string &tag_name,
					       Revision_t &revision,
					       const std::string &conn_name,
					       const std::string &variable_name) {
  }

  void DataServerCT::requestBoolAnalysisResult(const std::string &tag_name,
					       Revision_t &revision,
					       const std::string &conn_name,
					       const std::string &variable_name) {
  }

  void DataServerCT::requestAvAnalysisResult(const std::string &tag_name,
			       Revision_t &revision,
			       const std::string &conn_name,
			       const std::string &variable_name) {
  }
  
  void DataServerCT::requestBocConfig() {
  }

  void DataServerCT::requestModuleConfig(const std::string &) {
  }

  void DataServerCT::requestModuleConfigObject(const std::string &,
					       const std::string &,
					       const std::string &) {
  }

  void DataServerCT::requestModuleConfigObject(const std::string &,
					       const std::vector<std::string> &,
					       const std::string &) {
  }

  void DataServerCT::thatIsAll() { 
    m_master->setDataRequest(); 
    m_master=NULL;
  }

  void DataServerCT::SelectScanLabel(const std::string scan_label)
  {


  }

  void DataServerCT::SelectModule(const std::string module_name)
  {


  }

  //CLA
  void DataServerCT::sendToHistoServer(std::string &name, PixLib::Histo &histo) 
  {

  }

  //CLA
  void DataServerCT::getFromHistoServer(std::string &name, PixLib::Histo &histo)
  {

  }

  // convert  to PixLibHisto 
  PixLib::Histo* DataServerCT::Convert2PixLibHisto(const PixA::DbHandle lasthisto, const std::string histogram_type)
  {  
    PixA::HistoHandle histo_handle( PixA::PixDbHistoAccessor::histoHandle( lasthisto ) );
    PixA::HistoRef historef(histo_handle.ref());
    
    //      PixA::HistoHandle a_histo_handle(histo_handle);
    /////    PixA::ConfigHandle  scan_config = PixA::DbManager::configHandle(histo_handle);a
    //         ScanConfigRef scan_config(ScanConfigRef::makeScanConfig(const_cast<PixLib::ModuleConnectivity *>(*module_iter)->prodId(),
    // 							    const_cast<PixLib::ModuleConnectivity *>(*module_iter)->name(), 
    // 							    modConf(const_cast<PixLib::ModuleConnectivity *>(*module_iter)->prodId()), 
    // 							    pix_scan_config, 
    // 							    boc_config, 
    // 							    const_cast<PixLib::ModuleConnectivity *>(*module_iter)->bocLinkRx(0),
    // 							    const_cast<PixLib::ModuleConnectivity *>(*module_iter)->bocLinkRx(1)));
    ///////    PixA::PixDbDataContainer container(scan_config, histo_handle);  //why need scanConfig???
    
    HistoInfo_t info( GetHistoInfo( histogram_type, histo_handle) );
    PixLib::Histo* hix = nullptr;
    for (size_t iz=0; iz < info.maxIndex(2); iz++){
      for (size_t ih=info.minHistoIndex();ih<info.maxHistoIndex();ih++) {
	std::array<int,4> idx_in {-1, 0, static_cast<int>(iz), static_cast<int>(ih)};
	std::cout << "iz " << iz << ", ih " << ih << std::endl;
	if( !historef.haveHisto(histogram_type) ) 
	  std::cout << "Error in newHisto: histogram " << histogram_type.c_str() << " not found" << std::endl;
	else {
	  //	    Histo hix= Histo(*(container.getGenericPixLibHisto(histogram_type,idx_in)));
	  //	    Histo hix = Histo(*(historef.histo(name,idx_in)));
	  //       if ((name=="PowerMesurements" || name=="PowerMeasurements") && ih==1 || ih==2) continue;      
	  hix = historef.histo( histogram_type, idx_in.data() );
	  std::cout << "got one:  " << histogram_type.c_str() << std::endl;
	}
      }
    }
    return hix;
  }

//   TH2F* DataServerCT::getRootHistogram( const std::string &histogram_type, const std::string &conn_name)  
//   {  
//     return dynamic_cast<TH2F *>(PixA::transform<PixLib::Histo>(getHistogram(histogram_type,conn_name)));
//   }
//   TH1F* DataServerCT::getRoot1DHistogram( const std::string &histogram_type, const std::string &conn_name)  
//   {  
//     return dynamic_cast<TH1F *>(PixA::transform<PixLib::Histo>(getHistogram(histogram_type,conn_name)));
//   }
    
//   // convert  to PixLibHisto 
//   PixLib::Histo* DataServerCT::Ref2Histo(PixA::HistoRef historef, const std::string histogram_type,std::vector<histoAxis_t> axis)
//   {  
//     HistoInfo_t info;
//     historef.numberOfHistos(histogram_type, info);
// //     for (int i=0; i<3; i++) {
// //       std::cout << "||||" << info.maxIndex(i) << (i<2 ? "," : "");
// //     }
//     std::cout << "||||" << info.maxIndex(2) << " .. " << info.minHistoIndex() << " - " << info.maxHistoIndex() << std::endl;
    

//     PixLib::Histo * hix;
//     for (uint iz=0;iz<info.maxIndex(2);iz++){
//       for (uint ih=info.minHistoIndex();ih<info.maxHistoIndex();ih++) {
// //     std::cout << " trying ..." << std::endl;    
// //     axis[2];
// //     std::cout << " ok." << std::endl;    
// //     for (uint iz=0;iz<axis[2].lastBin;iz++){
// //       for (uint ih=axis[] info.minHistoIndex();ih<info.maxHistoIndex();ih++) {

// 	int idx_in[4]={-1,0,iz,ih};
// 	std::cout << "iz " << iz << ", ih " << ih << std::endl;
// 	if( !historef.haveHisto(histogram_type) ) 
// 	  std::cout << "Error in newHisto: histogram " << histogram_type.c_str() << " not found" << std::endl;
// 	else {
// 	  //	    Histo hix= Histo(*(container.getGenericPixLibHisto(histogram_type,idx_in)));
// 	  //	    Histo hix = Histo(*(historef.histo(name,idx_in)));
// 	  //       if ((name=="PowerMesurements" || name=="PowerMeasurements") && ih==1 || ih==2) continue;      
// 	  hix = historef.histo( histogram_type, idx_in );
// 	  std::cout << "got it !!111" << std::endl;
// 	  if(hix)
// 	    std::cout << "got it !!222" << std::endl;
// 	  else 
// 	    std::cout << "noooooo!!" << std::endl;		  
// 	}
//       }
//     }
//     return hix;
//  }


//   // If there are still histograms in the list, return the next one
//   // if list is empty, create a new one
//   PixLib::Histo* DataServerCT::getHistogram( const std::string &histogram_type, const std::string &conn_name) {  // const {      
//     if(listener->HistoListEmpty()) { 
//       std::cout << "histo list is empty!" << std::endl;
//       return NULL;
//     }
//     //find the requested scna type
//     /////////    PixA::HistoRef histoRef = listener->GetNextHisto();
//     //   std::string histogram_type = listener->GetNextHistoType(); //dos not work with const
//     std::string histogram_type2 = listener->GetNextHistoType();
//     std::cout << "getHistogram:: histogram_type: " << histogram_type2.c_str() << std::endl;      
//     //    conn_name = listener->GetNextConnName();

//     //    return Ref2Histo(histoRef,histogram_type2);
//     return NULL; //shortcut
//   }

//   // If there are still histograms in the list, return the next one
//   // if list is empty, create a new one
//   std::list<scanHisto_t> DataServerCT::getHistogramList( const std::string histogram_type) {      
//     std::list<scanHisto_t> histoList;
//     if(listener->HistoListEmpty()) { 
//       std::cout << "histo list is empty!" << std::endl;
//       return histoList; //empty
//     }
//     //find the requested scan type
//     bool first(true);
//     while(!listener->HistoListEmpty()) {
//       std::vector<PixLib::Histo *> hL1 = listener->GetNextHisto_Link1();
//       std::vector<PixLib::Histo *> hL2 = listener->GetNextHisto_Link2();
//       //   std::string histogram_type = listener->GetNextHistoType(); //dos not work with const
//       std::string histogram_type2 = listener->GetNextHistoType();
//       std::cout << "getHistogram:: histogram_type: " << histogram_type2.c_str() << std::endl;      
//       //    conn_name = listener->GetNextConnName();
//       if(histogram_type2==histogram_type) {
// 	std::cout << "found histogramtype requested" << std::endl;
// 	scanHisto_t h3;
// 	h3.axis = listener->GetNextAxis();

// 	for(std::vector<PixLib::Histo *>::const_iterator iter = hL1.begin(); iter!=hL1.end(); iter++ ) 
// 	  h3.histo2D_Link1.push_back( dynamic_cast<TH2F *>(PixA::transform<PixLib::Histo>(*iter)) );

// 	for(std::vector<PixLib::Histo *>::const_iterator iter = hL2.begin(); iter!=hL2.end(); iter++ ) 
// 	  h3.histo2D_Link2.push_back( dynamic_cast<TH2F *>(PixA::transform<PixLib::Histo>(*iter)) );

// 	h3.moduleName = listener->GetNextModuleName();
// 	h3.MHz = listener->GetNextMHz();
// 	histoList.push_back( h3 );
// 	first=false;
//       } 
//       else if(!first) break;
//     }
//     return histoList;
//   }


  unsigned int DataServerCT::getUIntAnalysisResult(const std::string &conn_name,
				     const std::string &variable_name) const { 
    //    throw MRSException("DATASRV_NOIMPL",MRS_FATAL,"CAN::DataServerCT::getAnalysisResult","Not implemented.");
    throw MRSException(PixLib::PixMessages::FATAL,"CAN::DataServerCT::getAnalysisResult","Not implemented.");
  }

  unsigned int DataServerCT::getBoolAnalysisResult(const std::string &conn_name,
						   const std::string &variable_name) const { 
    //    throw MRSException("DATASRV_NOIMPL",MRS_FATAL,"CAN::DataServerCT::getAnalysisResult","Not implemented.");
    throw MRSException(PixLib::PixMessages::FATAL,"CAN::DataServerCT::getAnalysisResult","Not implemented.");
  }

  const AverageResult_t& DataServerCT::getAvAnalysisResult(const std::string &conn_name,
							   const std::string &variable_name) const { 
    //    throw MRSException("DATASRV_NOIMPL",MRS_FATAL,"CAN::DataServerCT::getAnalysisResult","Not implemented.");
    throw MRSException(PixLib::PixMessages::FATAL,"CAN::DataServerCT::getAnalysisResult","Not implemented.");
  }

  const PixLib::Config& DataServerCT::getScanConfig() const { 
    //    throw MRSException("DATASRV_NOIMPL",MRS_FATAL,"CAN::DataServerCT::getScanConfig","Not implemented.");
    throw MRSException(PixLib::PixMessages::FATAL,"CAN::DataServerCT::getScanConfig","Not implemented.");
  }

  std::list<PixA::ScanConfigRef*> DataServerCT::getScanConfigRefs(const std::string &scanType,const std::string &pp0name) const { 
    return listener->getScanConfigRefs(scanType,pp0name);
  }

  const PixLib::Config& DataServerCT::getBocConfig() const { 
    //    throw MRSException("DATASRV_NOIMPL",MRS_FATAL,"CAN::DataServerCT::getBocConfig","Not implemented.");
    throw MRSException(PixLib::PixMessages::FATAL,"CAN::DataServerCT::getBocConfig","Not implemented.");
  }

  const PixLib::Config& DataServerCT::getModuleConfig(const std::string &module_conn_name) const { 
    //    throw MRSException("DATASRV_NOIMPL",MRS_FATAL,"CAN::DataServerCT::getModuleConfig","Not implemented.");
    throw MRSException(PixLib::PixMessages::FATAL,"CAN::DataServerCT::getModuleConfig","Not implemented.");
  }

  const PixLib::ConfigObj& DataServerCT::getModuleConfigObject(const std::string &module_conn_name,
						 const std::vector<std::string> &sub_config_and_group_name,
						 const std::string &config_object_name) const { 
    //    throw MRSException("DATASRV_NOIMPL",MRS_FATAL,"CAN::DataServerCT::getModuleConfigObject","Not implemented.");
    throw MRSException(PixLib::PixMessages::FATAL,"CAN::DataServerCT::getModuleConfigObject","Not implemented.");
  }

  const PixLib::ConfigObj& DataServerCT::getModuleConfigObject(const std::string &module_conn_name,
							       const std::string &config_group_name,
							       const std::string &config_object_name) const { 
    //    throw MRSException("DATASRV_NOIMPL",MRS_FATAL,"CAN::DataServerCT::getModuleConfigObject","Not implemented.");
    throw MRSException(PixLib::PixMessages::FATAL,"CAN::DataServerCT::getModuleConfigObject","Not implemented.");
  }


/////////////////////////////////////////

  //CLA
  // from current  path and file settings 
  //
  void DataServerCT::GetHistoMap(std::map<std::string,PixLib::Histo> *histoMap)
  {
    if(!m_paths.empty()) {
      m_files.clear();
      for(std::vector<std::string>::iterator iter = m_paths.begin(); iter != m_paths.end(); iter++) {
	std::cout << "GetFolderList appending files from path " << (*iter) << std::endl;
	AppendFilesInPath(*iter);
      }
    }
    if(!m_files.empty())
      for(std::vector<std::string>::iterator iter = m_files.begin(); iter != m_files.end(); iter++) { 
	std::cout << "GetFolderList appending folders in file " << (*iter) << std::endl;	
	AppendFolders(*iter,histoMap);
      }
  }


  void DataServerCT::AppendFolders( const std::string file_name, std::map<std::string,PixLib::Histo> *histoMap) 
  {
    std::cout << "DataServerCT::AppendFolders..." << std::endl;
    streamer = new PixA::ScanResultStreamer(m_path+"/"+file_name);
    listener = new ScanResultListener;
    streamer->setListener(listener);
    // would still need something like:  histoMap += listener->GetFoundHistos();

    //    streamer->readLabels(); //only read lables
    //    streamer->read(); // process all lables //now in requestHistogram!!!!

//     PixA::DbHandle rootHandle(PixA::DbManager::dbHandle(file_name));
//     PixA::DbRef root(rootHandle.ref());
//     std::cout << "AppendFolders...1111111" << std::endl;

//     // loop over labels
//     PixLib::dbRecordIterator recordbegin(root.recordBegin());
//     PixLib::dbRecordIterator recordend(root.recordEnd());
//     for (PixLib::dbRecordIterator label_iter = recordbegin; label_iter!=recordend; label_iter++) {
//       std::cout << "AppendFolders...2222222" << std::endl;
//       PixA::DbHandle label_handle(root.access(label_iter));
//       PixA::DbRef label_record(label_handle.ref());
//       std::cout << "AppendFolders...3333333" << std::endl;

//       ///////
//       PixLib::dbRecordIterator bStart = label_record.recordBegin();
//       PixLib::dbRecordIterator bEnd = label_record.recordEnd();
//       std::cout << "AppendFolders...44444444" << std::endl;
      
//       // loop over all or selected rods
//       //rods=GetSelectedRods()   /CLA still to implement !
//       for(PixLib::dbRecordIterator baseIter = bStart; baseIter!=bEnd; baseIter++){
// 	std::cout << "AppendFolders...5555555" << std::endl;
	
// 	PixA::DbHandle rod_handle( label_record.access(baseIter) );
// 	PixA::DbRef rod_record( rod_handle.ref() );
// 	////////

// 	PixA::DbHandle scan_data_handle;
// 	if( !GetScanData(scan_data_handle, rod_record) ) { 
// 	  std::cerr << "ERROR [DataServerCT::AppendFolders] No scanData found !" << std::endl;
// 	  return;  
// 	}
// 	std::cout << "AppendFolders...666666666" << std::endl;
// 	std::vector<StackElement_t> folder_stack;
      
// 	// initial folder is the module group folder
// 	std::vector<std::string> folder_hierarchy;
// 	folder_stack.push_back( StackElement_t(scan_data_handle, folder_hierarchy.size()) );
// 	while (!folder_stack.empty()) {
// 	  std::cout << "AppendFolders...77777777" << std::endl;

// 	  // processing folders
// 	  PixA::DbHandle folder_handle = folder_stack.back().folder();
// 	  unsigned hierarchy_index = folder_stack.back().hierarchyIndex();
// 	  PixA::ConfigHandle scan_info = folder_stack.back().scanInfo();
// 	  std::cout << "AppendFolders...888888888" << std::endl;
	  
// 	  std::vector<std::string>::iterator pos=folder_hierarchy.begin();
// 	  for (unsigned int i=0; i< hierarchy_index; i++) {
// 	    std::cout << "AppendFolders...9999999aaaaa" << std::endl;
// 	    assert(pos != folder_hierarchy.end() );    ////CLA ?????????
// 	    std::cout << "AppendFolders...999999bbbbbb" << std::endl;    
// 	    pos++;
// 	  }
// 	  folder_hierarchy.erase(pos,folder_hierarchy.end());
// 	  PixA::DbRef folder(folder_handle.ref());
// 	  folder_stack.pop_back();
// 	  std::cout << "AppendFolders...1010101010" << std::endl;
    
// 	  if(folder_handle!=scan_data_handle) {
// 	    folder_hierarchy.push_back(folder.name());
// 	    //	    newFolder(folder_hierarchy);
// 	  }
// 	  folder = folder_handle.ref();
// 	  folderList->push_back(folder);
// 	  std::cout << "AppendFolders...11 11 11" << std::endl;
// 	}
// 	std::cout << "AppendFolders...end1111." << std::endl;   
//       }
//       std::cout << "AppendFolders...end222." << std::endl;
//     }
//     std::cout << "AppendFolders...end." << std::endl;
  }
      
// Private Functions: (from Streamer)

  void DataServerCT::addFile(const std::string file_name) 
  {
    m_files.push_back(file_name);
    //    setLabel("");
  }

  /* CLA
   * Add all root files from the given path to DB
   * instead of init
   */ 
  void DataServerCT::addPath(const std::string path) 
  {
    m_paths.push_back(path);
  }

  void DataServerCT::AppendFilesInPath(const std::string path) 
  {

    //implementation needed !!

  }

  //CLA, NEW
  bool DataServerCT::GetScanData(PixA::DbHandle &scan_data_handle, PixA::DbRef rod_record)
  {
    unsigned int record_i=0;
    try {
      for(PixLib::dbRecordIterator record_iter = rod_record.recordBegin(); record_iter != rod_record.recordEnd(); record_iter++){
	
	PixA::DbHandle tmp_handle( rod_record.access(record_iter) );
	PixA::DbRef tmp_record( tmp_handle.ref() );
	
	if(tmp_record.className()=="PixScanData")
	  scan_data_handle = tmp_handle;
      }
    }
    catch (PixLib::PixDBException &err) {
      std::cerr << "ERROR [DataServerCT::GetScanData] Failed to process record " << record_i << " of " << rod_record.name() << " : " << std::endl
		<< err.getDescriptor() << std::endl;
      return false;
    }
    return true;
  }

  // CLA, NEW
  // Loops over the record given as 'folder'
  //    adds all elements with  className==selectedClassName  to  mlist
  void  DataServerCT::GetRecordLists(std::vector<PixA::DbHandle> &mlist, PixA::DbRef folder, const std::string selectClassName)
  {
    unsigned int record_i=0;
    try {
      for(PixLib::dbRecordIterator rec_iter = folder.recordBegin(); rec_iter != folder.recordEnd(); rec_iter++,record_i++) {

	PixA::DbHandle a_record_handle(folder.access(rec_iter));
	PixA::DbRef a_record(a_record_handle.ref());
	std::string class_name( a_record.className() );
	
	if(class_name==selectClassName && !a_record.empty()) 
	  mlist.push_back(a_record_handle);
      } 
    } 
    catch (PixLib::PixDBException &err) {
      std::cerr << "ERROR [DataServerCT::readCurrentLabel] Failed to process record " << record_i << " of " << folder.name() << " : " << std::endl
		<< err.getDescriptor() << std::endl;
    }    
  }  

  
  // CLA
  // Converts   histo_handle   ->   histoInfo
  //
  HistoInfo_t DataServerCT::GetHistoInfo(const std::string &name,const PixA::HistoHandle &histo_handle)
  {
    HistoInfo_t info;
    PixA::HistoHandle a_histo_handle(histo_handle);
    PixA::HistoRef historef(a_histo_handle.ref());
    historef.numberOfHistos(name, info);
    return info;
  }
  
}


  /*  uncomment-start


//  DataServerCT::init2(DbHandle &root_handle, const std::string &label_name) : m_rootHandle(root_handle) {
//    setLabel(label_name);
//  }

  //
  // Loops over m_rootHandles, and 
  // looks for lable_name in each record.
  // if found it sets m_lableHandel.
  void DataServerCT::setLabel(const std::string &label_name) {
    if (label_name.empty()) {
      m_labelHandle=PixA::DbHandle();
      return; 
    }
    for (std::vector<PixA::DbHandle>::iterator rootHandle_iter = m_rootHandles.begin(); rootHandle_iter != m_rootHandles.end(); rootHandle_iter++) {
      DbRef root(rootHandle_iter->ref());
      PixLib::dbRecordIterator iter = root.findRecord(label_name);
      if (iter == root.recordEnd()) 
	continue;
      else {
	m_labelHandle=root.access(iter);	  
	return;
      }
    }
    std::stringstream message;
    message << "ERROR [DataServerCT::DataServerCT] No label " << label_name << " in database.";
    throw PixLib::PixDBException(message.str());
  }

  //
  // Calls readCurrentLable()
  void DataServerCT::readLabel(const std::string &label_name, const std::string &mname)
  {
    setLabel(label_name);
    if(m_labelHandle) readCurrentLabel(mname);
  }

  //
  // Loops over m_rootHandles
  //     sets  m_labelHandle  and  calls readCurrentLable() for each record
  void DataServerCT::read(const std::string &mname) {
    if (m_labelHandle) {
      readCurrentLabel(mname);
    }
    else {
      for (std::vector<PixA::DbHandle>::iterator rootHandle_iter = m_rootHandles.begin(); rootHandle_iter != m_rootHandles.end(); rootHandle_iter++) {
	DbRef root(rootHandle_iter->ref());
	for (PixLib::dbRecordIterator label_iter = root.recordBegin(); label_iter!=root.recordEnd(); label_iter++) {
	  m_labelHandle=root.access(label_iter);
	  readCurrentLabel(mname);
	}
      }
    }
  }

  //
  // Loops over m_rootHandles, and 
  // calls newLable() print out for each record
  void DataServerCT::readLabels() {
    for (std::vector<PixA::DbHandle>::iterator rootHandle_iter = m_rootHandles.begin(); rootHandle_iter != m_rootHandles.end(); rootHandle_iter++) {
      DbRef root(rootHandle_iter->ref());
      for (PixLib::dbRecordIterator label_iter = root.recordBegin();
	   label_iter != root.recordEnd();
	   label_iter++) {
	PixA::DbHandle label_handle(root.access(label_iter));
	DbRef label_record(label_handle.ref());
	if(label_record.className()=="PixScanResult") {
	  ConfigHandle label_fake_config = DbManager::configHandle(label_handle);
	  newLabel(label_record.name(), label_fake_config);
	}
      }
    }
  }

  //CLA
  // also fills m_config
  void DataServerCT::GetScanDataAndBocConfig(PixA::DbHandle &scan_data_handle, PixA::ConfigHandle &boc_config, DbRef rod_record)
  {
    unsigned int record_i=0;
    try {
      for(PixLib::dbRecordIterator record_iter = rod_record.recordBegin(); record_iter != rod_record.recordEnd(); record_iter++){
	
	PixA::DbHandle tmp_handle( rod_record.access(record_iter) );
	DbRef tmp_record( tmp_handle.ref() );
	
	std::string class_name( tmp_record.className() );
	//	  std::cout << "INFO [DataServerCT::readCurrentLabel]  " << rod_name << " records  : " << tmp_record.name() << " / " << class_name << std::endl;
	
	if(class_name=="PixScanData"){
	  scan_data_handle = tmp_handle;
	}
	else if(class_name=="PixModule") {
	  m_configList.insert(std::make_pair(tmp_record.name(),PixA::DbManager::configHandle(tmp_handle)));
	}
	else if(class_name=="PixBoc") {
	  boc_config = DbManager::configHandle(tmp_record.access(record_iter));
	}
      }
    }
    catch (PixLib::PixDBException &err) {
      std::cerr << "ERROR [DataServerCT::readCurrentLabel] Failed to process record " << record_i << " of " << rod_record.name() << " : " << std::endl
		<< err.getDescriptor() << std::endl;
    }
    if(!scan_data_handle) return false;
    //
    DbRef scan_data(scan_data_handle.ref());
    /////// PixLib::dbRecordIterator debug_boc_config_record_iter = rod_record.findRecord("OpticalBoc");
    if (!boc_config) {
      // then search for global boc config
      DbRef root_record(m_rootHandles[0].ref());
      PixLib::dbRecordIterator global_rod_record_iter = root_record.findRecord(rod_name);
      if (global_rod_record_iter != root_record.recordEnd()) {
	PixA::DbHandle global_rod_handle(root_record.access(global_rod_record_iter));
	DbRef global_rod_record(global_rod_handle.ref());
	//	      std::auto_ptr<PixLib::DbRecord> global_rod_record( *m_rootRecord->getDb()->DbProcess(global_rod_record_iter,PixLib::PixDb::DBREAD));
	PixLib::dbRecordIterator boc_config_record_iter = global_rod_record.findRecord("OpticalBoc/PixBoc");
	if (boc_config_record_iter != global_rod_record.recordEnd()) {
	  boc_config = DbManager::configHandle(global_rod_record.access(boc_config_record_iter));
	}
      }
    }
    return true;
  }




  /// Main method
  //  Input: a module name or ""(?)
  //
  //  Starts from  m_labelHandle  ->  records of RODs
  //  Loops over selected RODs
  //    loops  over  scans 
  //       extracts   scan_data, config, bocConfig
  //       loops  through  StackElement(scan_data)
  //           loop over  filder-Elements  ("PixModule","PixScanInfo","PixHisto")
  //                fill     configList,  pix_scan_histo_list   and   histo_list
  //
  void DataServerCT::readCurrentLabel(const std::string &mname) {
    if (!m_labelHandle) {
      std::cerr << "ERROR [DataServerCT::readCurrentLabel] No label selected." << std::endl;
      return;
    }

    DbRef label_record(m_labelHandle.ref());
    if(label_record.className()!="PixScanResult") return;

    ConfigHandle label_fake_config = DbManager::configHandle(m_labelHandle);
    newLabel(label_record.name(), label_fake_config); //print our name + time stamp

    std::string modNam="", grpNam="";
    PixLib::dbRecordIterator bStart = label_record.recordBegin();
    PixLib::dbRecordIterator bEnd = label_record.recordEnd();

    // if a particular module name was given ....
    if(!mname.empty()) modNam = mname;
    if(!modNam.empty()){
      grpNam = modNam;
      std::string::size_type pos = modNam.find("/");
      if(pos!=std::string::npos){
	modNam.erase(0,pos+1);
	grpNam.erase(pos, grpNam.length()-pos);
      }
      bStart = label_record.findRecord(grpNam+"/PixModuleGroup");
      if(bStart!=bEnd){
	bEnd = bStart;
	bEnd++;
      }
    }

    // loop over all or the selected rods
    for(PixLib::dbRecordIterator baseIter = bStart; baseIter!=bEnd; baseIter++){

      PixA::DbHandle rod_handle( label_record.access(baseIter) );
      DbRef rod_record( rod_handle.ref() );

      if(rod_record.className()==("PixModuleGroup")) continue;

      std::string rod_name = rod_record.name();

      m_configList.clear();
      m_globalConfigList.clear();
      
      newPixModuleGroup(rod_name);  //only prints ROD name
      
      PixA::DbHandle scan_data_handle;
      PixA::ConfigHandle boc_config;
      if( !GetScanDataAndBocConfig(scan_data_handle,boc_config,rod_record) ) continue;  //CLA: only if  scan_data  found
       
      std::vector<StackElement_t> folder_stack;
      //	  std::auto_ptr<PixLib::DbRecord> m_rootRecord ( DBscfg->getDb()->readRootRecord() );
      //	  std::string root_file_name;
      //	  if (m_rootRecord.get()) {
      //	    root_file_name=getFileNameFromRecord(m_rootRecord.get());
      //	  }
      
      // initial folder is the module group folder
      std::vector<std::string> folder_hierarchy;
      folder_stack.push_back( StackElement_t(scan_data_handle, folder_hierarchy.size()) );
      while (!folder_stack.empty()) {
	// processing folders
	PixA::DbHandle folder_handle = folder_stack.back().folder();
	unsigned hierarchy_index = folder_stack.back().hierarchyIndex();
	ConfigHandle scan_info = folder_stack.back().scanInfo();
	
	std::vector<std::string>::iterator pos=folder_hierarchy.begin();
	for (unsigned int i=0; i< hierarchy_index; i++) {
	  assert(pos != folder_hierarchy.end() );    ////CLA ?????????
	  pos++;
	}
	folder_hierarchy.erase(pos,folder_hierarchy.end());
	DbRef folder(folder_handle.ref());
	folder_stack.pop_back();

	//	    std::cout << "INFO [DataServerCT::readCurrentLabel] folder = " << folder.name() << std::endl;
	
	if (folder_handle == scan_data_handle) {
	  //	      newLabel(folder.name());
	}
	else {
	  folder_hierarchy.push_back(folder.name());
	  newFolder(folder_hierarchy);
	}
	
	//	    std::cout << "INFO [DataServerCT::readCurrentLabel] processing " << folder.pathName() << " class = " << folder.className()
	//		      << std::endl;
	
	// 	    // get path name for DBtoMA
	// 	    std::string db_path = folder->getDecName();
	// 	    std::string db_file_name = getFileNameFromRecord(folder.get());
	// 	    if (db_file_name == root_file_name) {
	// 	      db_path.erase(0,db_file_name.size());
	// 	      db_path=orig_file_name + db_path;
	// 	    }
	// 	    //printf("db_path 1: %s \n", db_path.c_str(a));
	// 	    std::string::size_type slpos = db_path.find_last_of("/");
	// 	    if(slpos!=std::string::npos) 
	// 	      db_path.erase(slpos, db_path.length()-slpos);
	// 	    //printf("db_path 2: %s \n", db_path.c_str());
	// 	    slpos = db_path.find_last_of("/");
	// 	    if(slpos!=std::string::npos) 
	// 	      db_path.erase(slpos, db_path.length()-slpos);
	// 	    std::cout << "INFO [DataServerCT::readCurrentLabel] processing " <<
	// 		      << " folder = " << folder->getDecName() << " / class = " << folder->getClassName()
	// 		      << "  path = " << db_path
	// 		      << std::endl;
	
	//	    std::cout << "INFO [DataServerCT::readCurrentLabel] processing "
	//		      << " folder = " << folder.pathName() << " / class = " << folder.className()
	//		      << std::endl;
	

	// get a scan info if existing
	PixLib::dbRecordIterator scan_info_iter = scan_data.findRecord("ScanInfo");
	if (scan_info_iter != scan_data.recordEnd()) {
	  try {
	    PixA::DbHandle scan_info_handle ( scan_data.access(scan_info_iter) );
	    scan_info = DbManager::configHandle(scan_info_handle);
	  }
	  catch(...) {
	  }
	}
	
	//	    std::cout << "INFO [DataServerCT::readCurrentLabel] folder " << folder.name()
	//		      << " contains " <<  " : "
	//		      << std::endl;
	folder = folder_handle.ref();

	std::vector<PixA::DbHandle> pix_scan_histo_list;
	std::vector<PixA::DbHandle> histo_list;
	GetHistoLists( pix_scan_histo_list, histo_list, folder, folder_stack);
	
	if (!pix_scan_histo_list.empty()) {
	  
	  // Can we get a connectivity ?
	  m_conn.reset();
	  try  m_conn = ScanInfo2Connectivity(scan_info);
	  catch(...) {}
	  
	  // No local module config folder ? 
	  if (m_configList.empty() && m_globalConfigList.empty()) {
	    
	    // Then search for global module config folder
	    if (m_globalConfigList.empty()) {
	      if (!createGlobaleConfigList(m_rootHandles[0], rod_name, mname)) {
		std::cout << "INFO [DataServerCT::read] No local or global module configuration." << std::endl;
		// or try connectivity ?
		if (!createConfigListFromConnectivity(rod_name)) {
		  throw PixA::BaseException("ERROR [DataServerCT::read] Did not find local or global module configurations."
					    "Also did not get module configuration from connectivity database." );
		}
	      }
	    }
	  }
	  // exit if no config
	  //	      if (!config_handle_list.empty() || !global_config_handle_list.empty()) {
	  // now process pix scan histos
	  
	  std::set<std::string> module_names(GetModuleNames());
	  
	  PixA::ConfigHandle  pix_scan_config = PixA::DbManager::configHandle(folder_handle);
	  
	  //CLA: Collected all info, so do processing
	  if (m_conn) {
	    processModulesFromConnectivity(module_names, folder_handle, pix_scan_histo_list, pix_scan_config, boc_config,rod_name);
	  }
	  else {
	    processModulesFromConfigList(module_names, folder_handle, pix_scan_histo_list, pix_scan_config, boc_config);
	  }
	} //if !pix_scan_histo_list.empty()
	
	if (!histo_list.empty()) {
	  PixA::HistoHandle histo_handle( PixDbHistoAccessor::histoHandle( folder_handle ) );
	  
	  for ( std::vector< PixA::DbHandle >::iterator  histo_iter = histo_list.begin();
		histo_iter != histo_list.end();
		histo_iter++) {
	    DbRef histo_record(histo_iter->ref());
	    std::string histo_name = histo_record.name();
	    
	    newHisto( histo_name, histo_handle, rod_name, scan_info);
	  }
	}
	//	    folder_hierarchy.pop_back();
      }
    }
  }


  // CLA
  void  DataServerCT::GetHistoLists(std::vector<PixA::DbHandle> &pix_scan_histo_list, std::vector<PixA::DbHandle> &histo_list, DbRef folder, std::vector<StackElement_t> &folder_stack)
  {
    unsigned int record_i=0;
    try {
      for(PixLib::dbRecordIterator rec_iter = folder.recordBegin(); rec_iter != folder.recordEnd(); rec_iter++,record_i++) {

	PixA::DbHandle a_record_handle(folder.access(rec_iter));
	DbRef a_record(a_record_handle.ref());
	//	      std::cout << "INFO [DataServerCT::readCurrentLabel]  "
	//			<< " processing record  = " << a_record.name()
	//			<< " / class = " << a_record.className()
	//			<< std::endl;
	std::string class_name( a_record.className() );
	
	if(class_name=="PixModule") {
	  // a module config
	  //		std::cout << "INFO [DataServerCT::readCurrentLabel]  "
	  //			  << " add local module config " << a_record.name()
	  //			  << std::endl;	  
	  m_configList.insert(std::make_pair(a_record.name(),PixA::DbManager::configHandle(a_record_handle)));
	}
	//	      else if(a_record->getClassName()=="PixScanHisto") {
	//		std::vector<std::string>::const_iterator hist_type_name_iter=find(a_record->getClassName(), s_histoTypeList.begin(),s_histoTypeList.end());
	//		if (hist_type_name_iter != s_histoTypeList.end()) {
	//		  pix_scan_histo_list.push_back(a_record.release());
	//		}
	//		else {
	//		  histo_list.push_back(a_record.release());
	//		}
	//	      }
	else if(class_name=="Histo") {
	  // a custom histogram
	  if (!a_record.empty()) {
	    histo_list.push_back(a_record_handle);
	  }
	}
	else if(class_name=="PixScan_Histo") {
	  // a custom pix scan histogram
	  if (!a_record.empty()) {
	    pix_scan_histo_list.push_back(a_record_handle);
	  }
	}
	else if(class_name=="PixScan_Info") {
	  // do nothing
	}
	else {
	  if (s_histoTypeList.empty()) initHistoTypes();
	  std::set<std::string>::const_iterator hist_type_name_iter=s_histoTypeList.find(class_name);
	  if (hist_type_name_iter != s_histoTypeList.end()) {
	    // standard pix scan histo new and old fashioned
	    if (!a_record.empty()) {
	      pix_scan_histo_list.push_back(a_record_handle);
	    }
	  }
	  else {
	    if(class_name=="PixScanHisto") {
	      if (!a_record.empty()) 
		histo_list.push_back(a_record_handle);
	    } else {
	      // a folder
	      //		    std::cout << "INFO [DataServerCT::readCurrentLabel]  "
	      //			      << " add folder " << a_record.name() << " to stack."
	      //			      << std::endl;
	      
	      folder_stack.push_back( StackElement_t( a_record_handle, folder_hierarchy.size(), scan_info ) );
	    }
	  }
	}
	
      } // for loop over folderElements
    } //try
    catch (PixLib::PixDBException &err) {
      std::cerr << "ERROR [DataServerCT::readCurrentLabel] Failed to process record " << record_i << " of " << folder.name() << " : " << std::endl
		<< err.getDescriptor() << std::endl;
    }    
  }  

  // CLA
  std::set<std::string> DataServerCT::GetModuleNames(std::vector<PixA::DbHandle> pix_scan_histo_list) {
    // get list of modules with hisotograms
    std::set<std::string> module_names;
    for ( std::vector<PixA::DbHandle>::iterator  histo_iter=pix_scan_histo_list.begin(); histo_iter!=pix_scan_histo_list.end(); histo_iter++) {
      
      DbRef histo_record(histo_iter->ref());
      //		std::cout << "INFO [DataServerCT::readCurrentLabel]  "
      //			  << " process histos " << histo_record.name() << "."
      //			  << std::endl;
      
      for (PixLib::dbRecordIterator module_iter = histo_record.recordBegin(); module_iter != histo_record.recordEnd(); module_iter++) {

	PixA::DbHandle module_handle(histo_record.access(module_iter));
	DbRef module_record(module_handle.ref());
	
	std::string module_name = module_record.name();
	if (module_record.className() == "PixScanHisto" || module_name.size() == 4 && module_name.compare(0,3,"Mod")) {
	  //		    std::cout << "INFO [DataServerCT::readCurrentLabel] add module " << module_name << std::endl;
	  module_names.insert(module_name);
	}
      }
    }
    return module_names
  }

  
  // not used anywhere
  bool DataServerCT::createGlobaleConfigList(PixA::DbHandle &root_handle, const std::string &rod_name, const std::string &module_name) {
    //    std::cout << "INFO [DataServerCT::createGlobaleConfigList] search for global module config for ROD " << rod_name << "."
    //    	      << std::endl;

    DbRef root_record(root_handle.ref());
    PixLib::dbRecordIterator ri = root_record.findRecord(rod_name+"/PixModules");
    if (ri != root_record.recordEnd()) {
      PixA::DbHandle global_mod_config_handle( root_record.access(ri) );
      DbRef global_mod_config(global_mod_config_handle.ref());

      PixLib::dbRecordIterator mod_end = global_mod_config.recordEnd();
      PixLib::dbRecordIterator mod_start = mod_end;
      if (!module_name.empty()){
	// only consider the module of the given name
	mod_start = global_mod_config.findRecord(module_name+"/PixModule");
	if (mod_start!=mod_end) {
	  mod_end = mod_start;
	  mod_end++;
	}
      }
      else {
	mod_start = global_mod_config.recordBegin();
      }

      for (PixLib::dbRecordIterator mod_conf_iter = mod_start;
	   mod_conf_iter!= mod_end;
	   mod_conf_iter++) {
	PixA::DbHandle a_mod_conf_handle(global_mod_config.access( mod_conf_iter) );
	DbRef a_mod_conf(a_mod_conf_handle.ref());
	std::string module_name = a_mod_conf.name();
	//	std::cout << "INFO [DataServerCT::createGlobaleConfigList]  "
	//		  << " add global module config " << module_name
	//		  << std::endl;
	m_globalConfigList.insert(std::make_pair(module_name, PixA::DbManager::configHandle(a_mod_conf_handle) ));
      }
    }
    return !m_globalConfigList.empty();
  }

  //
  // Called from readCurrentLabel !
  // Loops over pp0s and modules, and 
  // fills  *m_globalConfigList*   with  pair(Connectivity,Configuration) 
  //
  bool DataServerCT::createConfigListFromConnectivity(const std::string &rod_name) {
    try {
      if (m_conn) {
	//	std::cout << "INFO [DataServerCT::createConfigListFromConnectivity] Try connectivity to get module configurations." << std::endl;
	//conn will wrap a pointer to IConnectivity
	// IConnectivity will contain a PixConnectivity and will cache config objects.
	// The connectivity will be a shared property. If there are no more users, the 
	// connectivity will be deleted eventually.
	const PixA::Pp0List &pp0s=m_conn.pp0s(rod_name);
	// read only
	for (PixA::Pp0List::const_iterator pp0_iter=pp0s.begin();
	     pp0_iter != pp0s.end(); // may be slightly inefficient unless the compiler is smart enough.
	     pp0_iter++) {

	  // it may be more efficient to call modulesBegin and modulesEnd only once:
	  PixA::ModuleList::const_iterator modules_begin=PixA::modulesBegin(pp0_iter);
	  PixA::ModuleList::const_iterator modules_end=PixA::modulesEnd(pp0_iter);
	  for (PixA::ModuleList::const_iterator module_iter=modules_begin; module_iter != modules_end; module_iter++) 
	    m_globalConfigList.insert(std::make_pair(const_cast<PixLib::ModuleConnectivity*>(*module_iter)->prodId(), m_conn.modConf(module_iter)) );
	}
      }
    }
    catch(...) {
    }
    return !m_globalConfigList.empty();
  }

  //
  // Called from readCurrentLabel
  // Loops over pp0s and modules, and
  // if module in module_names (given):
  //    Creats a scan configuration and 
  //    Loops over  pix_scan_histo_list (given), and
  //    calls  newPixScanHisto()
  bool DataServerCT::processModulesFromConnectivity(const std::set<std::string> &module_names,
						    const PixA::DbHandle &histo_folder_handle,
						    const std::vector< PixA::DbHandle > &pix_scan_histo_list,
						    const PixA::ConfigHandle &pix_scan_config,
						    const PixA::ConfigHandle &boc_config,
						    const std::string &rod_name)
  {
    assert (m_conn);

    // if there is a scan info section then use connectivity information

    //conn will wrap a pointer to IConnectivity
    // IConnectivity will contain a PixConnectivity and will cache config objects.
    // The connectivity will be a shared property. If there are no more users, the 
    // connectivity will be deleted eventually.
    const PixA::Pp0List &pp0s=m_conn.pp0s(rod_name);
    // read only
    for (PixA::Pp0List::const_iterator pp0_iter=pp0s.begin();
	 pp0_iter != pp0s.end(); // may be slightly inefficient unless the compiler is smart enough.
	 pp0_iter++) {
      newPp0( const_cast<PixLib::Pp0Connectivity*>(*pp0_iter)->name());
	// it may be more efficient to call modulesBegin and modulesEnd only once:
      PixA::ModuleList::const_iterator modules_begin=PixA::modulesBegin(pp0_iter);
      PixA::ModuleList::const_iterator modules_end=PixA::modulesEnd(pp0_iter);
      for (PixA::ModuleList::const_iterator module_iter=modules_begin;
	   module_iter != modules_end;
	   module_iter++) {

	// first try connectivity name 
	std::set<std::string>::const_iterator a_mod = module_names.find(const_cast<PixLib::ModuleConnectivity *>(*module_iter)->name());
	if (a_mod == module_names.end()) { 
	  //  then the old name
	  // If there are connectivity tags then the new pix scan format should be in use which names the modules by the full connectiivty name
	  std::stringstream a_mod_name;
	  a_mod_name << "Mod" << (*module_iter)->modId;
	  a_mod = module_names.find(a_mod_name.str());
	  std::cerr << "INFO [DataServerCT::processModulesFromConnectivity] Try old name  " << a_mod_name.str() << std::endl;
	}
	if (a_mod != module_names.end()) { 
	  ScanConfigRef scan_config(ScanConfigRef::makeScanConfig(const_cast<PixLib::ModuleConnectivity *>(*module_iter)->prodId(),
								  const_cast<PixLib::ModuleConnectivity *>(*module_iter)->name(), 
								  modConf(const_cast<PixLib::ModuleConnectivity *>(*module_iter)->prodId()), 
								  pix_scan_config, 
								  boc_config, 
								  const_cast<PixLib::ModuleConnectivity *>(*module_iter)->bocLinkRx(0),
								  const_cast<PixLib::ModuleConnectivity *>(*module_iter)->bocLinkRx(1)));

	  PixA::HistoHandle histo_handle( PixDbHistoAccessor::histoHandle( histo_folder_handle ) );

	  for (std::vector< PixA::DbHandle >::const_iterator hist_iter = pix_scan_histo_list.begin();
		 hist_iter != pix_scan_histo_list.end();
		 hist_iter++) {
	    //PixA::HistoHandle histo_handle( PixDbHistoAccessor::histoHandle( *hist_iter ) );
	      DbRef histo_record(hist_iter->ref());
	      std::string histo_name = histo_record.name();
	      newPixScanHisto( histo_name, 
			       scan_config, 
			       histo_handle);
	  }
	}
	else {
	  std::cerr << "INFO [DataServerCT::processModulesFromConnectivity] Nothing for module " 
		    << const_cast<PixLib::ModuleConnectivity *>(*module_iter)->name() 
		    << "(" << const_cast<PixLib::ModuleConnectivity *>(*module_iter)->prodId() << ")" << std::endl;
	}
      }
      finishPp0();
    }
    return true;
  }

  // CLA
  // Called  from  processModulesFromConfigList() 
  // 
  ModuleList_t DataServerCT::GetModuleList(const std::map<std::string, PixA::ConfigHandle> *config_list)
  {
    ModuleList_t ordered_module_list;
    // order modules wrt. to stave id
    for (std::map<std::string, PixA::ConfigHandle>::const_iterator module_conf_iter = config_list->begin(); module_conf_iter!=config_list->end(); module_conf_iter++) {
      ConfigRef mod_conf( module_conf_iter->second.ref() );
      int ass_SN=-1;
      std::string mod_conn_name;
      try {
	std::string ass_type = confVal<std::string>(mod_conf["geometry"]["Type"]);
	if (ass_type != "unknown") {
	  ass_SN=confVal<int>(mod_conf["geometry"]["staveID"]);
	}
	mod_conn_name = confVal<std::string>(mod_conf["geometry"]["connName"]);
      }
      catch(...) {
      }
      ordered_module_list.insert(std::make_pair(ass_SN,std::make_pair(mod_conn_name,module_conf_iter)));
    }
    return ordered_module_list;
  }

  //
  // Called from readCurrentLabel
  // Loops over moduelList
  //    gets scanConfigucration, and
  //    calls  newPixScanHisto()  for all histos 
  bool DataServerCT::processModulesFromConfigList(const std::set<std::string> &module_names,
						  const PixA::DbHandle &histo_folder_handle,
						  const std::vector< PixA::DbHandle > &pix_scan_histo_list,
						  const PixA::ConfigHandle &pix_scan_config,
						  const PixA::ConfigHandle &boc_config)
  {
    const std::map<std::string, PixA::ConfigHandle> *config_list=(!m_configList.empty() ? &m_configList : &m_globalConfigList);
    if (config_list->empty()) return false;
    // if there is a scan info section then use connectivity information

    // dirty trick to keep the RootDb file open 
    ConfigRef first_mod_conf( config_list->begin()->second.ref() );  //not used anywhere

    ModuleList_t ordered_module_list(GetModuleList(config_list));

    // now process modules in increasing stave id order.
    int last_stave_id = -1;
    for(ModuleList_t::const_iterator ordered_iter=ordered_module_list.begin(); ordered_iter!=ordered_module_list.end(); ordered_iter++) {

      if (ordered_iter->first>0 && ordered_iter->first != last_stave_id) {
	// stave id is not a Pp0, but we do not have anything better
	std::stringstream stave_label;
	stave_label << ordered_iter->first;
	newPp0(stave_label.str());
	last_stave_id = ordered_iter->first;
      }

      const std::map<std::string, PixA::ConfigHandle>::const_iterator &mod_conf_iter = ordered_iter->second.second;
      const std::string &mod_conn_name = ordered_iter->second.first;

      ScanConfigRef scan_config(ScanConfigRef::makeScanConfig( mod_conf_iter->first , mod_conn_name, mod_conf_iter->second, pix_scan_config, boc_config ));

      PixA::HistoHandle histo_handle( PixDbHistoAccessor::histoHandle( histo_folder_handle ) );
      for (std::vector< PixA::DbHandle >::const_iterator hist_iter = pix_scan_histo_list.begin();
	   hist_iter != pix_scan_histo_list.end();
	   hist_iter++) {
	// PixA::HistoHandle histo_handle( PixDbHistoAccessor::histoHandle( *hist_iter ) );
	DbRef histo_record(hist_iter->ref());
	std::string histo_name = histo_record.name();

	newPixScanHisto( histo_name,  scan_config, histo_handle);
      }
    }
    finishPp0();

    return true;
  }

  //
  // Called from processModulesFromConnectivity  in ScanConfigRef !
  // Converts   module_name  ->   configHandle
  //
  PixA::ConfigHandle DataServerCT::modConf(const std::string &module_prod_name) {
    PixA::ConfigHandle handle;
    if (!m_configList.empty()) {
      std::map<std::string, PixA::ConfigHandle>::iterator module_conf_iter = m_configList.find(module_prod_name);
      if (module_conf_iter != m_configList.end()) {
	handle = module_conf_iter->second;
      }
    }
    else if (!m_globalConfigList.empty()) {
      std::map<std::string, PixA::ConfigHandle>::iterator module_conf_iter = m_globalConfigList.find(module_prod_name);
      if (module_conf_iter != m_globalConfigList.end()) {
	handle = module_conf_iter->second;
      }
    }
    else if (m_conn) {
      try {
	handle = m_conn.modConf(module_prod_name);
      }
      catch(PixA::ConfigException &) {
      }
    }

    if (!handle) {
      std::stringstream message;
      message << "ERROR [DataServerCT::modConf] No module configuration for module " << module_prod_name << ".";
      throw PixA::ConfigException(message.str());
    }
    return handle;
  }


  //called from readCurrentLabel  
  //convertes:  ConfigHandle -> ConnectivityRef 
  PixA::ConnectivityRef DataServerCT::ScanInfo2Connectivity(const ConfigHandle &scan_info_handle)
  {
    PixA::ConnectivityRef m_conn;
    if (scan_info_handle) {
      try {
	PixA::ConfigRef  scan_info( scan_info_handle.ref() );

	PixA::ConfGrpRef tags(scan_info["Tags"]);
	std::string tag_conn  = confVal<std::string>(tags["Conn"]);
	std::string tag_alias = confVal<std::string>(tags["Alias"]);
	std::string tag_data  = confVal<std::string>(tags["Data"]);
	std::string tag_cfg   = confVal<std::string>(tags["Cfg"]);
	std::string tag_mod_cfg   = tag_cfg;
	// 	std::string tag_mod_cfg   = confVal<std::string>(tags["modCfg"]);

	unsigned int revision;
	try {
	  revision = confVal<unsigned int>(tags["CfgRev"]);
	}
	catch (PixA::ConfigTypeMismatch &) {
	  revision = static_cast< unsigned int>( confVal<int>(tags["CfgRev"]) );
	}

	std::cout << " conn  = " << tag_conn 
		  << " alias = " << tag_alias
		  << " data  = " << tag_data
		  << " cfg   = " << tag_cfg
		  << " mod cfg   = " << tag_mod_cfg
		  << " cfg Rev.  = " << revision
		  << std::endl;
	m_conn = PixA::ConnectivityManager::getConnectivity( tag_conn, tag_alias, tag_data,tag_cfg, tag_mod_cfg, revision);
      }
      catch(...) {
      }
    }
    return m_conn;
  }
}


// Private Functions: (from Listener)


// ----------------------------------------------
// Called from readLabels and readCurrenLabel.
// Only prints out: Name (given as input), ScanNumber, TimeStampa
// ----------------------------------------------
void DataServerCT::newLabel(const std::string &name, const PixA::ConfigHandle &label_config) 
{
  std::string comment;
  std::string time_stamp;
  unsigned int scan_number=0;
  if (label_config) {
    PixA::ConfigRef label_config_ref = label_config.ref();
    try {
      comment = confVal<std::string>(label_config_ref[""]["Comment"]);
      scan_number = confVal<unsigned int>(label_config_ref[""]["ScanNumber"]);
      time_stamp = confVal<std::string>(label_config_ref[""]["TimeStamp"]);
    }
    catch (PixA::ConfigException &err) {
      // catch exceptions which are thrown if a field does not exist e.g. ScanNumber
    }
  }
  std::cout << "INFO [DataServerCT::newLabel] ";
  if (scan_number>0) {
    std::cout << scan_number;
  }
  std::cout << " : " << name << "\"";
  if (!time_stamp.empty()) {
    std::cout << "  - " << time_stamp;
  }
  if (!comment.empty()) {
    std::cout << "  :: " << comment << "";
  }
  std::cout << std::endl;
}


// ----------------------------------------------
// Concrete implementation of IScanResultListener
// ----------------------------------------------
void DataServerCT::newPixModuleGroup(const std::string &name) 
{
  rodname = name;
  std::cout << "INFO [DataServerCT::newPixModuleGroup] pix module group=\"" << name << "\"" << std::endl;
}


// ----------------------------------------------
// Concrete implementation of IScanResultListener
// ----------------------------------------------
void DataServerCT::newPp0(const std::string &name) 
{
  std::cout << "INFO [DataServerCT::newPp0] PP0 =\"" << name << "\"" << std::endl;
//   //  vector <Histo> histovec;
//   v_histo.clear();
//   v_modulename.clear();
//   v_connname.clear();
//   v_histname.clear();
//   v_inLink.clear();
//   v_outLink.clear();
//   v_scansteps0.clear();
//   v_scansteps1.clear();
//   v_scansteps2.clear();
//   v_scanmin0.clear();
//   v_scanmin1.clear();
//   v_scanmin2.clear();
//   v_scanmax0.clear();
//   v_scanmax1.clear();
//   v_scanmax2.clear();
//   v_scanpar0.clear();
//   v_scanpar1.clear();
//   v_scanpar2.clear();
//   v_scanlevel.clear();
//   v_nhist.clear();
//   v_nlink.clear();
//   v_linkmin.clear();
//   v_linkmax.clear();
//   v_speed.clear();
  pp0name=name;
}


// ----------------------------------------------
// Concrete implementation of IScanResultListener
// ----------------------------------------------
void DataServerCT::finishPp0() 
{
  cout << "AAA1" << endl;
  std::cout << "INFO [DataServerCT::finishPp0] PP0 =\"" << pp0name << "\"" << std::endl;
  return;
}


// 
// Called from  readCurrentLabel
// only print out
void DataServerCT::newFolder(const std::vector<std::string> &folder_hierarchy) 
{
  std::cout << "INFO [DataServerCT::newFolder] path=\"";
  for (std::vector<std::string>::const_iterator folder_iter = folder_hierarchy.begin();
       folder_iter!=folder_hierarchy.end();
       folder_iter++) {
    std::cout << *folder_iter << "/";
    myfold = (*folder_iter); //not used anywhere
  }
  std::cout << "\"" << std::endl;
}


std::set<std::string> DataServerCT::s_histoTypeList;

void DataServerCT::initHistoTypes() {
  if (s_histoTypeList.empty()) {
    std::auto_ptr<PixLib::PixScan> ps(new PixLib::PixScan);
    std::map<std::string, int> types = ps->getHistoTypes();
    for (std::map<std::string, int>::const_iterator type_iter = types.begin();
	 type_iter != types.end();
	 type_iter++) {
      s_histoTypeList.insert(type_iter->first);
    }
  }
}

// CLA
// Converts histo_name (given)  ->  histoType   (from PixScan)
//
int DataServerCT::HistoName2Type(const std::string &histo_name) 
{
  HistoNameToType_t s_histoNameToType;
  std::auto_ptr<PixLib::PixScan> ps(new PixLib::PixScan());
  s_histoNameToType = ps->getHistoTypes();

  std::map< std::string, int >::const_iterator iter = s_histoNameToType.find(histo_name);
  int histo_type =-1;
  if (iter != s_histoNameToType.end()) {
    histo_type= iter->second;
  }
  if (histo_type<0) cout << "INFO [DataServerCT::newPixScanHisto] problem! no histo_type found"<<endl;
  return hsito_type;
}

//CLA
// Extracts  speed   and   module   from   Scan-Configuration
// 
void DataServerCT::PrintSpeedAndModule(const PixA::ScanConfigRef &scan_config) 
{
  std::string speed = confVal<std::string>(scan_config.pixScanConfig()["mcc"]["mccBandwidth"]);
  //  v_speed.push_back(speed);
  std::cout << "INFO [DataServerCT::newPixScanHisto] speed = "<<speed.c_str()<<endl;
  
  std::cout << "INFO [DataServerCT::newPixScanHisto] module=";
  if (!scan_config.connName().empty()) {
    std::cout << scan_config.connName() << "(" << scan_config.prodName() << ")";
    //    v_connname.push_back(scan_config.connName());
    //    v_modulename.push_back(scan_config.prodName());
  }
  else {
    std::cout << scan_config.prodName();
    //    v_modulename.push_back(scan_config.prodName());
  }
  std::cout << " histo=\"" << histo_name << "\"" << std::endl;
  //  v_histname.push_back(histo_name);
}

// ----------------------------------------------
// Concrete implementation of IScanResultListener
//
// ---> Reads PixScan histograms
// called from   processModulesFromConfigList   and   processModulesFromConnectivity
//
// Extracts   information  from  PixDbDataContainer(scan_config, histo_handle),  and
// prints info    or   saves in vars
void DataServerCT::newPixScanHisto(const std::string &histo_name,
				    const PixA::ScanConfigRef &scan_config,
				    const PixA::HistoHandle &histo_handle) 
{
  std::cout << "INFO [DataServerCT::newPixScanHisto] Histo =\"" << histo_name << "\"" << std::endl;

  // ignore the RAW_DATA_REF histograms right away.
  // Actually, removing the next line will cause to skip RAW_DATA_DIFF_1 and BOC SLOW will not be analyzed!!!
  //####-->  if (histo_name=="RAW_DATA_REF" || histo_name=="RAW_DATA_0" || histo_name=="RAW_DATA_1") return;
  //if (histo_name=="RAW_DATA_REF" || histo_name=="RAW_DATA_0") return;
  int histo_type(HistoName2Type(histo_name));

  PrintSpeedAndModule(scan_config);
  PixA::PixDbDataContainer container(scan_config, histo_handle);
  
  //  PixA::HistoHandle a_histo_handle(histo_handle);
  //CLA  wozu?  PixA::HistoRef histo(a_histo_handle.ref());
  HistoInfo_t info;
  container.numberOfHistos(histo_type,info);  //puts number of histos in info
  
  if (container.getScanSteps(0)<0) {std::cout << "No loop in Scan =>Return"<<endl; return;}
//   v_scansteps0.push_back(container.getScanSteps(0));
//   v_scansteps1.push_back(container.getScanSteps(1));
//   v_scansteps2.push_back(container.getScanSteps(2));
//   v_scanmin0.push_back(container.getScanStart(0));
//   v_scanmin1.push_back(container.getScanStart(1));
//   v_scanmin2.push_back(container.getScanStart(2));
//   v_scanmax0.push_back(container.getScanStop(0));
//   v_scanmax1.push_back(container.getScanStop(1));
//   v_scanmax2.push_back(container.getScanStop(2));
//   v_scanpar0.push_back(container.getScanPar(0));
//   v_scanpar1.push_back(container.getScanPar(1));
//   v_scanpar2.push_back(container.getScanPar(2));
//   v_scanlevel.push_back(container.getScanLevel());
  
//   v_nhist.push_back(info.maxIndex(2));
//   v_nlink.push_back(info.maxHistoIndex()-info.minHistoIndex());
//   v_linkmax.push_back(info.maxHistoIndex());
//   v_linkmin.push_back(info.minHistoIndex());

}


// ----------------------------------------------
// Concrete implementation of IScanResultListener
//
// ---> Reads summary histograms
// called from   readCurrentLables  
//
// Prints our some info 
//
void DataServerCT::newHisto(const std::string &name, const PixA::HistoHandle &histo_handle, 
			     const std::string &rod_name, const PixA::ConfigHandle &scan_config) 
{
  std::cout << "INFO [DataServerCT::newHisto] ";
  std::cout << "\"" << name << "\"" << std::endl;

  HistoInfo_t info(GetHistoInfo(name,histo_handle));

  std::cout << "idx:";
  for (int i=0; i<3; i++) 
    std::cout << info.maxIndex(i) << (i<2 ? "," : "");

  std::cout << " n histos=" << info.minHistoIndex() << " - " << info.maxHistoIndex() << std::endl;
//   for (uint ih=info.minHistoIndex();ih<info.maxHistoIndex();ih++) {
//     if ( name=="PowerMesurements" || name=="PowerMeasurements" ) {
//       if ((name=="PowerMesurements" || name=="PowerMeasurements") && ih==1 || ih==2) continue;
      
//       int idx_in[4] = {-1, -1, -1, ih};
//       bool hh = historef.haveHisto(name);
//       if (!hh) std::cout << "Error in newHisto: histogram " << name.c_str() << " not found"<<endl;
//       Histo hix = Histo(*(historef.histo(name,idx_in)));
//       v_sumhisto.push_back(hix);
//     }
//   }
  const int nhist = v_sumhisto.size();
  if(nhist<1) return;

  int nDim = v_sumhisto.at(0).nDim();
  cout << "Number of dimensions: " << nDim<<endl;
  cout << "end of newHisto"<<endl;
}


*/
