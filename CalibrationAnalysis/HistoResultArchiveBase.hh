#ifndef _CAN_HistoResultArchiveBase_hh_
#define _CAN_HistoResultArchiveBase_hh_

#include "IResultStore.hh"
//#include <mrs/message.h>
#include "sendMessage.hh"
#include "Common.hh"
#include "exception.hh"
#include <PixHistoServer/PixHistoServerInterface.h>
#include <PixUtilities/PixMessages.h>


#include <memory>

#include "Lock.hh"
#include "Flag.hh"

namespace CAN {
  /** Abstract interface of the class which archives the analysis results
   */
  class HistoResultArchiveBase : public IResultStore
  {
  public:
    void setServers(IPCPartition* ipcPartition, std::string ohServerName, std::string nameServerName)
    {
      m_ipcPartition = ipcPartition;
      m_ohServerName = ohServerName;
      m_nameServerName = nameServerName;
    }

    void remakeOHInterface()
    {
      Lock lock(m_mutex);
      if (OHInterface.get() == NULL){

	OHInterface = std::make_unique<PixLib::PixHistoServerInterface>(m_ipcPartition,
							      m_ohServerName,
							      m_nameServerName,
							      (std::string)"AnalysisOutputHistograms");
      }
    }

  protected:
    std::unique_ptr<PixLib::PixHistoServerInterface> OHInterface;
    Mutex m_mutex;

  private:
    IPCPartition* m_ipcPartition;
    std::string m_ohServerName;
    std::string m_nameServerName;
    
    

  };

}
#endif
