#include "PixDbConfigDb.hh"
#include <ConfigWrapper/DbManager.h>
#include <ConfigWrapper/Connectivity.h>
#include "ConfigCatalogue.hh"

#include <sys/stat.h>

#include "PluginManager/DirIter.hh"

//*****[maria elena] added (still to select the real necessary)
#include <ScanConfigDbInstance.hh>
#include <ConfigWrapper/PixConfigWrapper.h>
#include <PixController/PixScan.h>

#ifdef __RCE__
#include "rcecalib/server/PixScan.hh"
#endif
//************

//***************************** [maria elena] added (still to select the real necessary)
#include "MasterDataServer.hh"
//*****************************


namespace CAN {

  std::string replaceSlashes(const std::string &folder_name) {
    std::string temp(folder_name);
    for (std::string::iterator char_iter = temp.begin();
	 char_iter != temp.end();
	 char_iter++) {
      if (*char_iter == '/') {
	*char_iter = '|';
      }
    }
    return temp;
  }

  PixDbConfigDb::PixDbConfigDb(const std::string path_name) 
    : m_pathName(path_name)
  {
//      std::cout << "PixDbConfigDb first call" << std::endl;
    if (!m_pathName.empty() && m_pathName[m_pathName.size()-1]!='/') {
      m_pathName+='/';
    }
    m_catalogueName ="sqlite_file:";
    m_catalogueName +=m_pathName;
    m_catalogueName +="catalogue.sql";
//std::cout << "PixDbConfigDb first call 1" << std::endl;
    CAN::FileInfo info(m_pathName);
    if (!info.isDir()) {
      std::string message("Not a directory : ");
      message += m_pathName;
      message += ".";
      throw MissingConfig(message);
    }
//      std::cout << "PixDbConfigDb first call 2" << std::endl;
  }

  PixDbConfigDb::~PixDbConfigDb() {}
        
    //****************************
    
    std::string PixDbConfigDb::configFileName(const std::string &type_name,
                                                       const std::string &tag_name,
                                                       Revision_t revision)
    {
        std::string tag_dir(m_pathName);
        std::string wildcard("_#FLAV");   // T: modify fileName with wildcard !
        
        CAN::FileInfo a_dir_info(tag_dir);

        tag_dir += type_name;
        tag_dir +='/';
        tag_dir += tag_name;
        tag_dir+= wildcard;
        tag_dir +='/';
        tag_dir += type_name;
        tag_dir += '_';
        tag_dir += tag_name;
        tag_dir+= wildcard;
        tag_dir += '_';
        std::stringstream revision_str;
        revision_str << std::setw(9) << std::setfill('0') << revision;
        tag_dir += revision_str.str();
        tag_dir +=".root";
         
        return tag_dir;
    }

    //****************************
    
    
  Revision_t PixDbConfigDb::addConfig(const std::string &class_name,
				      const std::string &type_name,
				      const std::string &tag_name,
				      const PixA::ConfigHandle &handle)
  {
    Revision_t revision = time(0);
    std::vector< std::pair< std::string, PixA::ConfigHandle> > config_list;
    config_list.push_back(std::make_pair(std::string(""),handle) );

    return addConfig(class_name, type_name, tag_name, revision, config_list);
  }

  Revision_t PixDbConfigDb::addConfigList(const std::string &class_name,
					  const std::string &type_name,
					  const std::string &tag_name,
					  const std::vector< std::pair< std::string, PixA::ConfigHandle> > &config_list)
  {
    Revision_t revision = time(0);
    return addConfig(class_name, type_name, tag_name, revision, config_list);
  }


  Revision_t PixDbConfigDb::addConfig(const std::string &class_name,
				      const std::string &type_name,
				      const std::string &tag_name,
				      Revision_t revision,
				      const PixA::ConfigHandle &handle) 
  {
    std::vector< std::pair< std::string, PixA::ConfigHandle> > config_list;
    config_list.push_back(std::make_pair(std::string(""),handle) );

    return addConfig(class_name, type_name, tag_name, revision, config_list);
  }

  class UMaskChanger 
  {
  public:
    UMaskChanger(mode_t new_mask) { m_oldMask=umask(new_mask); }
    ~UMaskChanger() { umask(m_oldMask);}
  private:
    mode_t  m_oldMask;
  };

  Revision_t PixDbConfigDb::addConfig(const std::string &class_name,
				      const std::string &type_name,
				      const std::string &tag_name,
				      Revision_t revision,
				      const std::vector< std::pair< std::string, PixA::ConfigHandle> > &config_list)
  {
    std::string tag_dir(m_pathName);

    tag_dir+= type_name;
    UMaskChanger umask_changer(0);
    {
      CAN::FileInfo a_file_info(tag_dir);
      if (!a_file_info.isDir()) {
	if (mkdir(tag_dir.c_str(),0777) != 0 ) {
	  std::string message("Failed to create new directory : ");
	  message += tag_dir;
	  message += ".";
	  throw MissingConfig(message);
	}
      }
    }

    tag_dir+='/';
    tag_dir+= tag_name;

    {
      CAN::FileInfo a_file_info(tag_dir);
      if (!a_file_info.isDir()) {
	if (mkdir(tag_dir.c_str(),0777) != 0 ) {
	  std::string message("Failed to create new directory : ");
	  message += tag_dir;
	  message += ".";
	  throw MissingConfig(message);
	}
      }
    }

    tag_dir += '/';
    tag_dir += type_name;
    tag_dir += '_';
    tag_dir += tag_name;
    tag_dir += '_';

    std::stringstream revision_str;
    revision_str << std::setw(9) << std::setfill('0') << revision;
    tag_dir += revision_str.str();
    tag_dir +=".root";

    // does only work if the given config objects are not 
    // PixConfigWrapper
    //    addParameter(type_name, tag_name, revision, config_list);

    CAN::FileInfo a_file_info(tag_dir);
    if (a_file_info.exists()) {
      std::string message("New scan parameter file exists already : ");
      message += tag_dir;
      message += ".";
      throw MissingConfig(message);
    }

    try {
    PixA::DbHandle db_handle = PixA::DbManager::dbHandle(tag_dir, false);

    if (!db_handle) {
      std::string message("Could not create file  : ");
      message += tag_dir;
      message += "to store parameters.";
      throw MissingConfig(message);
    }


    PixA::DbRef sub_folder(db_handle.writeHandle().ref());

    for(std::vector< std::pair< std::string, PixA::ConfigHandle> >::const_iterator config_iter=config_list.begin();
	config_iter != config_list.end();
	config_iter++) {
      PixA::ConfigRef a_config = config_iter->second.ref();
      if (config_iter->first.empty()) {
	const_cast<PixLib::Config &>(a_config.config()).write( sub_folder.record().get() );
      }
      else {
	const_cast<PixLib::Config &>(a_config.config()).write( sub_folder.addRecord("PixScan",replaceSlashes(config_iter->first)).get() );
      }
    }

    // remove write permission
    chmod(tag_dir.c_str(),S_IRGRP|S_IRUSR|S_IROTH);

    ConfigCatalogue config_catalog(m_catalogueName, /*verbose*/ false, /*update*/ true);
    config_catalog.addConfigDescription(type_name,class_name,"");
    }
    catch( PixLib::PixDBException &err) {
      throw MissingConfig( err.getDescriptor() );
    }

    return revision;

  }

  PixA::ConfigHandle PixDbConfigDb::config(const std::string &type_name,
					   const std::string &tag_name, Revision_t revision)
    {
        if (revision>0) {
            const Parameter_t *a_param = find(type_name,tag_name,revision);
            if (a_param) {
                return a_param->handle();
            }
        }
        std::string file_name = configFileName(type_name, tag_name, revision);
        
        return PixA::DbManager::configHandle( PixA::DbManager::dbHandle(file_name) );
    }
    
    

    PixA::ConfigHandle PixDbConfigDb::config_1(const std::string &type_name,
                                               const std::string &tag_name,
                                               Revision_t revision, std::string string)
    {

        std::string file_name = configFileName(type_name, tag_name, revision);
        
        if(revision>0){

            const Parameter_t *a_param = find(type_name,tag_name,revision);
            if (a_param) {
                 return a_param->handle_1(file_name,string);
            }
        }
        
        return PixA::DbManager::configHandle( PixA::DbManager::dbHandle_1(file_name,string) );
    }
        
    
  PixA::ConfigHandle PixDbConfigDb::config(const std::string &type_name,
					   const std::string &tag_name,
					   Revision_t revision,
					   const std::string &config_name)
    {
        if (revision>0) {

            const Parameter_t *a_param = find(type_name,tag_name,revision);
            if (a_param) {
                return a_param->handle(config_name);
            }
        }
        
        std::string file_name = configFileName(type_name, tag_name, revision);
        if (config_name.empty()) {
            return PixA::DbManager::configHandle( PixA::DbManager::dbHandle(file_name) );
        }
        else {
            
            PixA::DbHandle sub_folder_handle = PixA::DbManager::dbHandle(file_name);
            if (!sub_folder_handle) {
                std::stringstream message;
                message << " No configuration "
                << " for type " << type_name << " in " << tag_name << " / " << revision
                << ".";
                throw MissingConfig(message.str());
            }
            
            PixA::DbRef sub_folder = sub_folder_handle.ref();
            PixLib::dbRecordIterator folder_record = sub_folder.findRecord(replaceSlashes(config_name));
            if (folder_record == sub_folder.recordEnd()) {
                std::stringstream message;
                message << " No configuration with name " << config_name
                << " for type " << type_name << " in " << tag_name << " / " << revision
                << ".";
                throw MissingConfig(message.str());
            }
            return PixA::DbManager::configHandle(sub_folder.access(folder_record));
            
        }
    }

  class TypeNameCollector : public IDescriptionListener
  {
  public:
    void newDescription(const std::string &type_name, const ConfigDescription &/*description*/) {
      m_types.insert(type_name);
    };

    const std::set<std::string> &types() const { return m_types; }

  private:
    std::set<std::string> m_types;
  };

  std::set<std::string> PixDbConfigDb::listTypes(const std::string &class_name,  EConfigLevel level) {
    ConfigCatalogue config_catalog(m_catalogueName, /*verbose*/ false, /*update*/ false);
    std::set<std::string> type_names;
    TypeNameCollector listener;
    assert( static_cast<unsigned short>(ConfigDescription::kNLevels) == static_cast<unsigned short>(kNLevels) );

    config_catalog.getDescription(class_name, listener, static_cast<ConfigDescription::EConfigLevel>(level));
    return listener.types();
  }


  std::set<std::string> PixDbConfigDb::listTypes() {

    std::set<std::string> type_list;
    std::string type_name(m_pathName);
    std::unique_ptr<IDirIter> dir_iter(CAN::DirLister::directoryIterator(type_name));
    while (dir_iter->next()) {
      type_list.insert( dir_iter->name());
    }
    return type_list;
  }

  std::set<std::string> PixDbConfigDb::listTags(const std::string &type_name)
  {

    std::string type_dir(m_pathName);
    type_dir+= type_name;

    CAN::FileInfo a_dir_info(type_dir);
    if (!a_dir_info.isDir()) {
      std::string message("Not a directory : ");
      message += type_dir;
      message += ".";
      throw MissingConfig(message);
    }

    std::set<std::string> tag_list;
    std::unique_ptr<IDirIter> dir_iter(CAN::DirLister::directoryIterator(type_dir));
    while (dir_iter->next()) {
      tag_list.insert( dir_iter->name() );
    }
    return tag_list;
  }

  std::set<Revision_t> PixDbConfigDb::listRevisions(const std::string &type_name,
					            const std::string &tag_name)
  {
    std::string tag_dir(m_pathName);
    tag_dir+= type_name;
    tag_dir+='/';
    tag_dir+= tag_name;

    CAN::FileInfo a_dir_info(tag_dir);
    if (!a_dir_info.isDir()) {
      std::string message("Not a directory : ");
      message += tag_dir;
      message += ".";
      throw MissingConfig(message);
    }

    std::unique_ptr<IDirIter> file_iter(CAN::DirLister::fileIterator(tag_dir, std::string("^")+type_name+'_'+tag_name+"_.+\\.root$"));
    std::set<Revision_t> rev_list;
    while (file_iter->next()) {
      std::string file_name( file_iter->name() );
      std::string::size_type extension_pos = file_name.rfind(".");
      if (extension_pos != std::string::npos && extension_pos>0) {
	std::string::size_type revision_start = file_name.rfind("_",extension_pos-1);
	if (revision_start+1<extension_pos) {
	  revision_start++;
	  if (isdigit(file_name[revision_start]) && isdigit(file_name[extension_pos-1])) {
	    rev_list.insert( atoi( &(file_name[revision_start])) );
	  }
	}
      }
    }
    return rev_list;
  }

  /** Get the description of a type.
   */
  void PixDbConfigDb::getDescription(const std::string &/*type_name*/) {
    throw std::runtime_error("not implemented.");
  }

  /** Get all descriptions.
   * @param class_name the name of a particular class or an empty string.
   */
  void PixDbConfigDb::getDescription(const std::string &/*class_name*/, IDescriptionListener &/*listener*/) {
    throw std::runtime_error("not implemented.");
  }

  /** Get the comment for a particular data set.
   */
  void PixDbConfigDb::getComments(const std::string &/*type_name*/,
				  const std::string &/*tag_name*/,
				  Revision_t /*revision*/,
				  ICommentListener &/*listener*/) {
    throw std::runtime_error("not implemented.");
    }


  void PixDbConfigDb::cleanup() {

    unsigned int counter =0;
    for (std::map<std::string, std::map< std::string, std::map<Revision_t, Parameter_t> > >::iterator type_iter = m_cache.begin();
	 type_iter == m_cache.end();
	 type_iter++) {
      for (std::map< std::string, std::map<Revision_t, Parameter_t> >::iterator tag_iter = type_iter->second.begin();
	   tag_iter == type_iter->second.end();
	   tag_iter++) {
	for (std::map<Revision_t, Parameter_t>::iterator revision_iter = tag_iter->second.begin();
	     revision_iter== tag_iter->second.end();
	     revision_iter++) {
	  counter++;
	}
      }
    }

    for (std::map<std::string, std::map< std::string, std::map<Revision_t, Parameter_t> > >::iterator type_iter = m_cache.begin();
	 ;) {

      std::map<std::string, std::map< std::string, std::map<Revision_t, Parameter_t> > >::iterator current_type_iter = type_iter++;
      bool reached_type_end = (type_iter == m_cache.end());

      for (std::map< std::string, std::map<Revision_t, Parameter_t> >::iterator tag_iter = type_iter->second.begin();
	   ;) {


	std::map< std::string, std::map<Revision_t, Parameter_t> >::iterator current_tag_iter = tag_iter++;
	bool reached_tag_end = (tag_iter == type_iter->second.end());

	for (std::map<Revision_t, Parameter_t>::iterator revision_iter = tag_iter->second.begin();
	     ;) {

	  std::map<Revision_t, Parameter_t>::iterator current_revision_iter = revision_iter++;
	  bool reached_revision_end = (revision_iter == current_tag_iter->second.end());
	  if (current_revision_iter->second.cleanup()) {
	    if ( counter>s_maxConfigs ) {
	      current_tag_iter->second.erase(current_revision_iter);
	    }
	  }
	  if (reached_revision_end) break;
	}

	if (current_tag_iter->second.empty()) {
	  current_type_iter->second.erase( current_tag_iter);
	}

	if (reached_tag_end) break;
      }

      if (current_type_iter->second.empty()) {
	m_cache.erase( current_type_iter);
      }

      if (reached_type_end) break;

    }

  }

    
//*************
    
    std::string PixDbConfigDb::givepath(){
        
        return m_pathName;
    }

    
    void PixDbConfigDb::create_default_config(Revision_t max_revision, std::string scan_type, std::string class_name, std::string tag_name_flavor){
        
        
        
        std::stringstream revision_string;
        revision_string << std::setw(9) << std::setfill('0') << max_revision;
        
        try {
            std::unique_ptr<CAN::IConfigDb> parameter_db((!m_pathName.empty()) ?
                                                       static_cast<CAN::IConfigDb *>(new CAN::PixDbConfigDb(m_pathName))
                                                       : CAN::ScanConfigDbInstance::create());
            
            PixLib::PixScan * a_pix_scan=new PixLib::PixScan();
            
            std::map<std::string, int> type_list = a_pix_scan->getScanTypes();
            bool IsAddParam=false;
            
            for (std::map<std::string, int>::const_iterator type_iter = type_list.begin();
                 type_iter != type_list.end();
                 type_iter++) {
                
                if(scan_type!="AllScan"){
                    bool IsMatch=false;
                    if(scan_type == type_iter->first) IsMatch=true;
                    else if(scan_type == std::string("BOC_INLINKOUTLINK")){
                        if(type_iter->first == std::string("BOC_LINKSCAN")) IsMatch=true;
                    }
                    if(!IsMatch) continue;
                }
                
                
                try {
                    // must be executed for *every* iteration in "AllScan"-mode, otherwise default values will not be correct for next scan
                    a_pix_scan->initConfig();
                    
                    // T: easy way to convert string to enum
                    /* ignore invalid scan types */
                    if(a_pix_scan->preset(static_cast<PixLib::PixScan::ScanType>(type_iter->second), PixLib::EnumFEflavour::PM_FE_I4)==false) throw PixLib::PixScanExc(PixLib::PixScanExc::WARNING,"invalid scan preset");
                    
                    IsAddParam=true;
                    // T
                    std::cerr << a_pix_scan << std::endl;
                    std::cout << a_pix_scan->getLVL1Latency() << std::endl;
                    PixA::ConfigHandle config_handle(PixA::PixConfigWrapper::wrap(a_pix_scan->config()) );
                    parameter_db->addConfig(class_name, type_iter->first, tag_name_flavor,max_revision, PixA::PixConfigWrapper::wrap(a_pix_scan->config()) ); // T: revision as param !
                }
                catch ( PixLib::PixScanExc &) {
                    std::cerr << "ERROR: sth went wrong with preset/addConfig" << std::endl;
                }
            }
            if(!IsAddParam) std::cerr <<"Preset definition (PixController/PixScan.cxx) doesn't include "<< scan_type <<std::endl;
            
        }
        catch(CAN::MissingConfig &err) {
            std::cerr << "ERROR Caught MissingConfig exception : " << err.getDescriptor() << std::endl;
        }
        catch(PixA::ConfigException &err) {
            std::cerr << "ERROR Caught ConfigError exception : " << err.getDescriptor() << std::endl;
        }
        catch(std::exception &err) {
            std::cerr << "ERROR Caught std exception : " << err.what() << std::endl;
        }
        catch(...) {
            std::cerr << "ERROR Caught unknown exception." << std::endl;
        }
        
        
        
        
        
        
    }

    
    void PixDbConfigDb::create_copy_config(Revision_t max_revision, std::string scan_type, std::string class_name, std::string tag_name_flavor,const PixA::ConfigHandle &vconfig){
    
        
        std::stringstream revision_string;
        revision_string << std::setw(9) << std::setfill('0') << max_revision;
        
        try {
            std::unique_ptr<CAN::IConfigDb> parameter_db((!m_pathName.empty()) ?
                                                       static_cast<CAN::IConfigDb *>(new CAN::PixDbConfigDb(m_pathName))
                                                       : CAN::ScanConfigDbInstance::create());
            
                            
                try {
                    PixA::ConfigHandle config_handle(vconfig);
                    parameter_db->addConfig(class_name, scan_type, tag_name_flavor,max_revision, vconfig ); // T: revision as param !
                }
                catch ( PixLib::PixScanExc &) {
                    std::cerr << "ERROR: sth went wrong with preset/addConfig" << std::endl;
                }
            
        }
        catch(CAN::MissingConfig &err) {
            std::cerr << "ERROR Caught MissingConfig exception : " << err.getDescriptor() << std::endl;
        }
        catch(PixA::ConfigException &err) {
            std::cerr << "ERROR Caught ConfigError exception : " << err.getDescriptor() << std::endl;
        }
        catch(std::exception &err) {
            std::cerr << "ERROR Caught std exception : " << err.what() << std::endl;
        }
        catch(...) {
            std::cerr << "ERROR Caught unknown exception." << std::endl;
        }
        
        
        
        
        
    
    }
//********************

    
}

