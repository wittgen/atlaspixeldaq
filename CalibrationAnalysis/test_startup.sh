#!/bin/sh

#CLA: This script strats workers on different computers

export MATER=`pwd`

partition=${USER}_test
test -n $MATER || (echo "ERROR [$0] Environment MATER not set" >&2; false ) || exit -1
test -x $MATER/rat || (echo "ERROR [$0] The rat application does not exist. Check environment variable MATER" >&2; false ) || exit -1
test -x $MATER/manitu || (echo "ERROR [$0] The manitu application does not exist. Check environment variable MATER" >&2; false ) || exit -1

export PATH=$MATER:$PATH

# Start scheduler

echo "INFO [$0] Start manitu."
host=ros01
ssh -x $host sh -c "\"source ~/src/pixeldaq/env.sh; export PATH=\\\$MATER:\\\$PATH; export TDAQ_APPLICATION_OBJECT_ID=manitu_\\\${host}; manitu -d ${partition} \""
sleep 3

# Start worker

counter=0
for i in ros01:2 ros01:2 control2:1 control4:1 ; do
# for i in ros01:4  ; do
  slots=${i#*:}
  host=${i%:*}
  echo "INFO [$0] start rat on $host with ${slots} slots."
  ssh -x $host sh -c "\"source ~/src/pixeldaq/env.sh; export PATH=\\\$MATER:\\\$PATH; export TDAQ_APPLICATION_OBJECT_ID=rat_\\\${HOSTNAME}_${nr}; rat -d ${partition} -n ${slots} \"" &
  counter=$((counter + 1))
done

wait
echo "INFO [$0] Ready."

