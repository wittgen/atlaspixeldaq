#include "ExtendedAnalysisResultList_t.hh"

#include <PixModuleGroup/PixModuleGroup.h>

namespace CAN {
  ExtendedAnalysisResultList_t::~ExtendedAnalysisResultList_t() {}

//   std::ostream &operator<<( std::ostream &out, const std::shared_ptr<TH1> &h1) {
//     out << h1->GetName();
//     return out;
//   }

//   template <class T>
//   void printValues(std::string header,
// 		   typename std::map<std::string, std::map<std::string, T> >::const_iterator begin_iter,
// 		   typename std::map<std::string, std::map<std::string, T> >::const_iterator end_iter,
// 		   std::ostream &out) {

//     for (typename std::map<std::string, std::map<std::string, T> >::const_iterator iter = begin_iter;
// 	 iter != end_iter;
// 	 ++iter) {

//       out<< header << " " <<iter->first<<" in ";

//       for (typename std::map<std::string,T>::const_iterator value_iter = iter->second.begin();
// 	   value_iter != iter->second.end() ;
// 	   ++value_iter){
// 	out<< value_iter->first <<"=" << value_iter->second << " ";
//       }

//       out<<std::endl;
//     }

//   }

  template <class T>
  void printValueNames(std::string header,
		       typename std::map<std::string, std::map<std::string, T> >::const_iterator begin_iter,
		       typename std::map<std::string, std::map<std::string, T> >::const_iterator end_iter,
		       std::ostream &out) {

    for (typename std::map<std::string, std::map<std::string, T> >::const_iterator iter = begin_iter;
	 iter != end_iter;
	 ++iter){

      out<< header << " " <<iter->first<<" in ";

      for (typename std::map<std::string,T>::const_iterator value_iter = iter->second.begin();
	   value_iter != iter->second.end() ;
	   ++value_iter){

	out<< value_iter->first <<" ";

      }

      out<<std::endl;
    }

  }

  template <class T>
  void printConnNames(std::string header,
		      typename std::map<std::string, std::map<std::string, T> >::const_iterator begin_iter,
		      typename std::map<std::string, std::map<std::string, T> >::const_iterator end_iter,
		      std::ostream &out) {

    for (typename std::map<std::string, std::map<std::string, T> >::const_iterator iter = begin_iter;
	 iter != end_iter;
	 ++iter){
      out << header << " "<< iter->first <<" ";
    }

  }

  void ExtendedAnalysisResultList_t::showContents() const
  {
    std::cout<<"Printing Result ListContents"<<std::endl;
    printValueNames<bool>("bool",begin<bool>(), end<bool>(), std::cout);
    printConnNames<float>("float",begin<float>(), end<float>(), std::cout);
    printConnNames<unsigned int>("unsigned int",begin<unsigned int>(), end<unsigned int>(), std::cout);
    printConnNames<std::shared_ptr<TH1> >("TH1",beginHisto<TH1>(), endHisto<TH1>(), std::cout);

    //     printValues<bool>("bool",begin<bool>(), end<bool>(), std::cout);
    //     printValues<float>("float",begin<float>(), end<float>(), std::cout);
    //     printValues<unsigned int>("unsigned int",begin<unsigned int>(), end<unsigned int>(), std::cout);
    //     printValues<std::shared_ptr<TH1> >("TH1",beginHisto<TH1>(), endHisto<TH1>(), std::cout);
  }

}
