#include <unistd.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <map>
#include <cstring>

class Info_t
{
public:
  Info_t(const std::string &name, unsigned long size) : m_name(name), m_size(size) {}

  std::string   m_name;
  unsigned long m_size;
};

typedef std::map<unsigned long , Info_t > MemMapInfo_t;
MemMapInfo_t s_last;

unsigned long vmsize(bool show)
{
  std::stringstream file_name_base;
  file_name_base << "/proc/" << getpid();
  char buffer[1024];
  unsigned long vm_size=0;
  {
   std::ifstream in((file_name_base.str()+"/status").c_str());
   while (in) {
     in.getline(buffer,1023, '\n');
     buffer[1023]='\0';
     if (strncmp(buffer,"VmSize:",7)==0) {
       const char *ptr=buffer+7;
       while (isspace(*ptr)) ptr++;
       std::cout << "vm size (here): " << ptr << std::endl;
       vm_size=strtol(ptr,NULL,10);
     }
     if (show) {
       std::cout << buffer << std::endl;
     }
   }
  }
  return vm_size;
}

void memstat()
{
  std::stringstream file_name_base;
  file_name_base << "/proc/" << getpid();
  char buffer[1024];
//   {
//   std::ifstream in((file_name_base.str()+"/status").c_str());
//   while (in) {
//     in.getline(buffer,1023, '\n');
//     buffer[1023]='\0';
//     std::cout << buffer << std::endl;
//   }
//   }

  {
  std::ifstream in((file_name_base.str()+"/maps").c_str());
  while (in) {
    in.getline(buffer,1023, '\n');
    buffer[1023]='\0';
    std::stringstream line;
    line << buffer;

    std::string range;
    std::string perm;
    std::string offset;
    std::string time; 
    std::string inode; 
    std::string name; 
    line >> range >> perm >> offset >> time >> inode >> name;

    if (!range.empty()) {
      std::string::size_type pos=range.find("-");
      if (pos!=std::string::npos && pos>0 && pos+1 < range.size()) {
	//	std::string start_addr_str="0x";
	//	start_addr_str+=std::string(range,0,pos);
	unsigned long start_addr = strtol(range.c_str(),NULL,16);
	//	range[pos-1]='0';
	//	range[pos]='x';
	unsigned long end_addr = strtol(&(range.c_str()[pos+1]),NULL,16);
	MemMapInfo_t::iterator mem_map_iter = s_last.find(start_addr);
	if (mem_map_iter == s_last.end()) {
	  //	  std::cout << range << " " << name << std::endl;
	  //std::pair<MemMapInfo_t::iterator,bool> ret =
              (void)s_last.insert(std::make_pair(start_addr,Info_t(name,end_addr-start_addr)));
	}
	else {
	  unsigned long new_size = end_addr- start_addr;
	  if (mem_map_iter->second.m_size != new_size) {
	    bool sign = (new_size > mem_map_iter->second.m_size);
	    unsigned long diff = ( sign ? new_size - mem_map_iter->second.m_size : mem_map_iter->second.m_size - new_size );
	    mem_map_iter->second.m_size=new_size;

	    std::string unit(" b  ");
	    if (diff>8*1024*1024) {
	      unit=" Mb ";
	      diff /= (1024 *1024);
	    }
	    else if (diff>8*1024) {
	      unit=" kb ";
	      diff /= (1024);
	    }

	    std::string size_unit(" b  ");
// 	    if (new_size>8*1024*1024) {
// 	      size_unit=" Mb ";
// 	      new_size /= (1024 *1024);
// 	    }
// 	    else if (diff>8*1024) {
// 	      size_unit=" kb ";
// 	      new_size /= (1024);
// 	    }

 	    std::cout << std::setw(9) << new_size  << size_unit
		      << ( sign ? "+" : "-") << std::setw(9) << diff << unit
		      << std::hex << mem_map_iter->first << std::dec << " : " << mem_map_iter->second.m_name
		      << std::endl;
	  }
	}
      }
    }

  }
  }
}
