#!/bin/sh

pid_file=/tmp/${USER}/.${USER}.is_test_pids
partition_name=${USER}_test
is_name=RunParams
oh_name=PixelHistoServer
oh2_name=pixel_histo_server_PixNameServerInternal
histo_name_server_name="nameServer"
db_server_name="PixelDbServer"

wait_before_term=5
#serial_number_path=/tmp/${USER}
command=""
script_name=$0

while test "$1"; do
    case $1 in
	(--db-server|--cache)
	   start_db_server=yes
	   ;;
	(start|stop)
	   command=$1
	   ;;
	(*)
	  echo "USAGE: ${script_name} [--cache] start stop"
	  echo ""
	  echo "       --cache  will start a db server, which will be used to cache"
          echo "                the connectivity and the module/BOC configurations."
	  exit -1
	  ;;
    esac
    shift
done

# check existance of tools
tools="ipc_ls ipc_server mrs_server is_ls is_server ${PIX_LIB}/Applications/PixNameServer"
if test x${start_db_server} = xyes; then
  tools=${tools}" ${PIX_LIB}/Applications/PixDbServer ${CAN}/test_readEnableDbServer"
fi

for i in `echo $tools`; do
  if (which $i >/dev/null 2>/dev/null); then
      echo "INFO $i exists."
  else
    echo "ERROR [$0] $i does not exist. Maybe You have to adjust Your environment ?   EXIT." >&2
    exit -1
  fi
done

# no start or stop services

case $command in
(start) 
if test -f $pid_file; then
  echo "WARNING: The servers may be running already. Stop them first." >&2
else

if (ipc_ls -P | grep -q initial); then
  echo "ipc root server already running. Will use the existing root ipc server"
  ipc_server_root_pid=""
else
echo "INFO: No ipc root  server running. Will start ipc root server."
echo "ipc root server"
ipc_root_server_id=$(if (ipc_ls -P 2>/dev/null | grep initial >/dev/null);  then
  :
else
  # echo "--- ipc_server  ----"
  ipc_server_root_pid=""
  ipc_err_file=`mktemp /tmp/${USER}/$USER.ipc_server_err_XXXX`
  ipc_server > /dev/null  2>$ipc_err_file &
  ipc_server_root_pid=$!
  while ! (ipc_ls -P 2>/dev/null | grep initial >/dev/null); do
      if test -s $ipc_err_file; then
	  echo "ERROR: while starting initial ipc server." >&2
	  cat $ipc_err_file >&2
	  exit -1
      fi
      sleep 1
  done
#  echo "Startet new root ipc server ($HOSTNAME:$ipc_server_root_pid)"
#  echo ""
  echo "ipc_root_server::$USER@$HOSTNAME:$ipc_server_root_pid"
fi) || exit -1
fi
echo "IPC partition server"
ipc_partition_server_id=$(if (ipc_ls -P 2>/dev/null | grep $partition_name >/dev/null);  then
  :
else
  # echo "---- ipc server partition ${partition_name} ----"
  ipc_partition_server_pid=""
  ipc_part_err_file=`mktemp /tmp/${USER}/$USER.ipc_part_server_err_XXXX`
  ipc_server -p $partition_name > /dev/null 2> ${ipc_part_err_file} &
  ipc_partition_server_pid=$!
  while ! (ipc_ls -P 2> /dev/null | grep $partition_name >/dev/null); do
      if test -s $ipc_part_err_file; then
	  echo "ERROR: while starting ipc server for partition ${partition_name}" >&2
	  cat $ipc_part_err_file >&2
	  exit -1
      fi
      sleep 1
  done
  # echo "Started new ipc server for partition ${partition_name} ($HOSTNAME:$ipc_partition_server_pid)"
  # echo ""
  echo "ipc_partition_server::$USER@$HOSTNAME:$ipc_partition_server_pid"
fi) || exit -1

echo "MRS server and worker." 
mrs_server_id=$(if (ipc_ls -p $partition_name 2> /dev/null | grep MRS >/dev/null);  then
  :
else
  # echo "---- ipc server partition ${partition_name} ----"
  mrs_server_server_pid=""
  ipc_part_err_file=`mktemp /tmp/${USER}/$USER.ipc_part_server_err_XXXX`
  mrs_server -p $partition_name > /dev/null 2> ${ipc_part_err_file} &
  mrs_server_server_pid=$!
  while ! (ipc_ls -p $partition_name 2> /dev/null | grep MRS >/dev/null); do
      if test -s $ipc_part_err_file; then
	  echo "ERROR: while starting mrs server in partition ${partition_name}" >&2
	  cat $ipc_part_err_file >&2
	  exit -1
      fi
      sleep 1
  done
  # echo "Started new ipc server for partition ${partition_name} ($HOSTNAME:$mrs_server_server_pid)"
  # echo ""
  echo "mrs_server_server::$USER@$HOSTNAME:$mrs_server_server_pid"

  mrs_worker_pid=""
  ipc_part_err_file=`mktemp /tmp/${USER}/$USER.ipc_part_server__err_XXXX`
  mrs_worker -p $partition_name > /dev/null 2> ${ipc_part_err_file} &
  mrs_worker_pid=$!
  if test -e test_mrs; then
    ./test_mrs $partition_name >&2 ||	 ( echo "ERROR: MRS test on ${partition_name} failed" >&2; false ) || exit -1
  fi
   echo "mrs_worker::$USER@$HOSTNAME:$mrs_worker_pid"
fi
) || exit -1



echo "IS server"
is_server_id=$(if (is_ls -p ${partition_name} 2>/dev/null | grep -v -E "^Partition" | grep $is_name >/dev/null);  then
  :
else
  is_server_pid=""
  is_server_err_file=`mktemp /tmp/${USER}/$USER.is_server_err_XXXX`
  is_server -n $is_name -p $partition_name >/dev/null 2> ${is_server_err_file} &
  is_server_pid=$!
  while ! (is_ls -p ${partition_name} 2>/dev/null | grep -v -E "^Partition" | grep $is_name >/dev/null); do
      is_ls -p ${partition_name} >&2
      if test -s $is_server_err_file; then
	  echo "ERROR: while starting is server for ${is_name}" >&2
	  cat $is_server_err_file >&2
	  exit -1
      fi
      sleep 1
  done
  # echo "Started new is server for ${server_name} (pid=$is_server_pid)"
  # echo ""
  echo "is_server::$USER@$HOSTNAME:$is_server_pid"
fi) || exit -1
echo "IS server : $(is_ls -p ${partition_name} 2>/dev/null| grep -v -E "^Partition" | grep $is_name)"

echo "OH server : $oh_name"
oh_server_id=$(if (is_ls -p ${partition_name} 2>/dev/null | grep -v -E "^Partition" | grep $oh_name >/dev/null);  then
  :
else
  oh_server_pid=""
  oh_server_err_file=`mktemp /tmp/${USER}/$USER.oh_server_err_XXXX`
  is_server -n $oh_name -p $partition_name >/dev/null 2> ${oh_server_err_file} &
  oh_server_pid=$!
  while ! (is_ls -p ${partition_name} 2>/dev/null | grep -v -E "^Partition" | grep $oh_name >/dev/null); do
      is_ls -p ${partition_name} >&2
      if test -s $oh_server_err_file; then
	  echo "ERROR: while starting OH server for ${oh_name}" >&2
	  cat $oh_server_err_file >&2
	  exit -1
      fi
      sleep 1
  done
  # echo "Started new is server for ${server_name} (pid=$oh_server_pid)"
  # echo ""
  echo "oh_server::$USER@$HOSTNAME:$oh_server_pid"
fi) || exit -1
echo "OH server : $(is_ls -p ${partition_name} 2>/dev/null| grep -v -E "^Partition" | grep $oh_name)"

echo "OH server : $oh2_name."
oh_server_id=$(if (is_ls -p ${partition_name} 2>/dev/null | grep -v -E "^Partition" | grep $oh2_name >/dev/null);  then
  :
else
  oh2_server_pid=""
  oh2_server_err_file=`mktemp /tmp/${USER}/$USER.oh2_server_err_XXXX`
  is_server -n $oh2_name -p $partition_name >/dev/null 2> ${oh2_server_err_file} &
  oh2_server_pid=$!
  while ! (is_ls -p ${partition_name} 2>/dev/null | grep -v -E "^Partition" | grep $oh2_name >/dev/null); do
      is_ls -p ${partition_name} >&2
      if test -s $oh2_server_err_file; then
	  echo "ERROR: while starting OH server for ${oh2_name}" >&2
	  cat $oh2_server_err_file >&2
	  exit -1
      fi
      sleep 1
  done
  # echo "Started new is server for ${server_name} (pid=$oh2_server_pid)"
  # echo ""
  echo "oh_server::$USER@$HOSTNAME:$oh2_server_pid"
fi) || exit -1
echo "OH server : $(is_ls -p ${partition_name} 2>/dev/null| grep -v -E "^Partition" | grep $oh2_name)"

if true; then
echo "Histo Name server "
 echo "> ${ohname_exe} ${partition_name} ${histo_name_server_name} ${oh2_name}"
ipc_service_name=${histo_name_server_name}
ohname_server_id=$( (ipc_ls -p $partition_name 2> /dev/null | grep ${ipc_service_name})>&2
if (ipc_ls -p $partition_name 2> /dev/null | grep ${ipc_service_name} >/dev/null);  then
  :
else
#  echo "---- partition ${partition_name} ----" >&2
  ohname_server_server_pid=""
  ipc_part_err_file=`mktemp /tmp/${USER}/$USER.ohname_server_err_XXXX`
  ohname_exe=${PIX_LIB}/Applications/PixNameServer
  if test -x ${ohname_exe}; then
#  echo "---- start ----" >&2
  ${ohname_exe} ${partition_name} ${histo_name_server_name} > /dev/null 2> ${ipc_part_err_file} &
  ohname_server_server_pid=$!
  while ! (ipc_ls -p $partition_name 2> /dev/null | grep ${ipc_service_name} >/dev/null); do
#      echo "---- wait ----" >&2
      if test -s $ipc_part_err_file; then
	  echo "ERROR: while starting histo name server in partition ${partition_name}" >&2
	  cat $ipc_part_err_file >&2
	  exit -1
      fi
      sleep 1
  done
  # echo "Started new ipc server for partition ${partition_name} ($HOSTNAME:$mrs_server_server_pid)"
  # echo ""
  echo "${ipc_service_name}::$USER@$HOSTNAME:$ohname_server_server_pid"
  else
      echo "ERROR [Histo name server] Not an executable : ${ohname_exe} " >&2
      exit -1
  fi
fi) || exit -1
fi

db_server_id=""

if test x${start_db_server} = xyes; then
echo "DB server "

ipc_service_name=${db_server_name}
db_server_id=$(if (ipc_ls -p $partition_name 2> /dev/null | grep ${ipc_service_name} >/dev/null);  then
  :
else
  # echo "---- ipc server partition ${partition_name} ----"
  db_server_server_pid=""
  ipc_part_err_file=`mktemp /tmp/${USER}/$USER.db_server_err_XXXX`
  db_exe=${PIX_LIB}/Applications/PixDbServer
  if test -x ${db_exe}; then
  ${db_exe} ${partition_name} ${db_server_name} ${db_name} > /dev/null 2> ${ipc_part_err_file} &
  db_server_server_pid=$!
  while ! (ipc_ls -p $partition_name 2> /dev/null | grep ${ipc_service_name} >/dev/null); do
      if test -s $ipc_part_err_file; then
	  echo "ERROR: while starting Db server in partition ${partition_name}" >&2
	  cat $ipc_part_err_file >&2
	  exit -1
      fi
      sleep 1
  done
  # echo "Started new ipc server for partition ${partition_name} ($HOSTNAME:$mrs_server_server_pid)"
  # echo ""
  echo "${ipc_service_name}::$USER@$HOSTNAME:$db_server_server_pid"
  else
      echo "ERROR [Db server] Not an executable : ${db_exe} " >&2
      exit -1
  fi

  ${CAN}/test_readEnableDbServer -p ${partition_name} --db-server-name ${db_server_name} >&2 || exit -1
fi) || exit -1
fi

#echo "Serial number service"
#ipc_service_name=${partition_name}_SerialNumberService
#sense_server_id=$(if (ipc_ls -p $partition_name 2> /dev/null | grep ${ipc_service_name} >/dev/null);  then
#  :
#else
  # echo "---- ipc server partition ${partition_name} ----"
#  sense_server_server_pid=""
#  ipc_part_err_file=`mktemp /tmp/${USER}/$USER.sense_server_err_XXXX`
#  sense_exe=${SN_SVC}/serialNumberServer
#  export LD_LIBRARY_PATH=${PIX_ANA}/ConfigWrapper:${SN_SVC}:$LD_LIBRARY_PATH; 
#  if test -x ${sense_exe}; then
#  ${sense_exe} ${partition_name} ${serial_number_path} > /dev/null 2> ${ipc_part_err_file} &
#  sense_server_server_pid=$!
#  while ! (ipc_ls -p $partition_name 2> /dev/null | grep ${ipc_service_name} >/dev/null); do
#      if test -s $ipc_part_err_file; then
#	  echo "ERROR: while starting serial number service in partition ${partition_name}" >&2
#	  cat $ipc_part_err_file >&2
#	  exit -1
#      fi
#      sleep 1
#  done
  # echo "Started new ipc server for partition ${partition_name} ($HOSTNAME:$mrs_server_server_pid)"
  # echo ""
#  echo "${ipc_service_name}::$USER@$HOSTNAME:$sense_server_server_pid"
#  else
#      echo "ERROR [Serial number service] Not an executable : ${sense_exe} " >&2
#      exit -1
#  fi
#fi) || exit -1



echo $ipc_root_server_id $rdb_server_id $ipc_partition_server_id $mrs_server_id ${is_server_id} ${oh_server_id} ${oh2_server_id} ${ohname_server_id} ${db_server_id} > ${pid_file} 
fi
;;
(stop)
if test -f $pid_file; then
  for i in `cat $pid_file`; do
     name=${i%::*}
     if test "$name" = "$i"; then
       :
     else 
     i=${i#*::}
     user=${i%%\@*} 
     hostname=${i#*\@}
     pid=${hostname##*:}
     hostname=${hostname%:*}
     echo "kill $name (pid = $pid) : $user @ $hostname "
     if test x$HOSTNAME = x$hostname && test x$USER = x$user; then
	 ( kill $pid 2>/dev/null; sleep ${wait_before_term}; kill -9 $pid 2> /dev/null) &
     else 
	 ssh $user@$hostname sh -c "kill $pid 2>/dev/null; sleep ${wait_before_term}; kill -9 $pid 2>/dev/null " &
     fi
     fi
  done
  rm $pid_file
fi 
;;
(*)
   echo "USAGE ${script_name} [--cache] start | stop"
   echo ""
   echo "Will start (or stop) : "
   echo " * ipc root partition,"
   echo " * ipc partition ${partition_name},"
   echo " * is server ${is_name} in partition ${partition_name},"
   echo " * mrs server in partition ${partition_name},"
   exit -1
   ;;
esac


