// Dear emacs, this is -*-c++-*-
#ifndef _CAN_HistoArray_hh_
#define _CAN_HistoArray_hh_

#include <DataContainer/HistoInfo_t.h>
#include <vector>
#include <cassert>

#include "Histo.hh"

namespace CAN {

  typedef Histo *HistoPtr_t;
  
  /** Class to help keeping track of the total memory consumed for histograms
   */ 
  class AccountingHistoPtr_t {
  public:
    AccountingHistoPtr_t() : m_ptr(0) {}
    ~AccountingHistoPtr_t()                                        { s_memoryUsed -= usedMemory();}

    AccountingHistoPtr_t(const AccountingHistoPtr_t &a) : m_ptr(a.m_ptr) { s_memoryUsed += usedMemory(); }
    AccountingHistoPtr_t(Histo *a_histo)          : m_ptr(a_histo) { s_memoryUsed += usedMemory(); }

    AccountingHistoPtr_t &operator=(const AccountingHistoPtr_t &a) { 
      s_memoryUsed -= usedMemory();
      m_ptr=a.m_ptr;
      s_memoryUsed += usedMemory();
      return *this;
    }

    AccountingHistoPtr_t &operator=(Histo *a_histo) { 
      s_memoryUsed -= usedMemory();
      m_ptr=a_histo;
      s_memoryUsed += usedMemory();
      return *this;
    }

    bool operator !() const {
      return !m_ptr;
    }

    HistoPtr_t ptr()             { return m_ptr; }
    const HistoPtr_t ptr() const { return m_ptr; }

    void reset();

    static unsigned long usedMemoryTotal() {
      return s_memoryUsed;
    }

    unsigned long usedMemory() const {
      return (m_ptr ? CAN::usedMemory(m_ptr) : 0);
    }

  private:
    static unsigned long s_memoryUsed;
    HistoPtr_t m_ptr;
  };


  /** Multi dimensional array of histograms.
   */
  class HistoArray
  {
  protected:
    HistoArray(unsigned int level) : m_level(level) { }
  public:
    HistoArray() : m_level(0) {}
    ~HistoArray();

    /** Add a histogram at the location specified by the multidimensional index.
     * @param index the multi dimensional index.
     * @param histo_ptr pointer of the histogram.
     * The histogram array will claim ownership.
     */
    void addHisto(const PixA::Index_t &index, Histo *histo_ptr) {
      if (!histo_ptr) return;

      addHisto(index, 0, histo_ptr);
    }

    /** Get the histogram from the location specified by the multi dimensional index.
     * @param index the multi dimensional index.
     * @return pointer of the histogram or NULL if no histogram exists at the specified location.
     * The histogram is owned by the histogram array and must not be deleted e.g.
     * <pre>
     * // in case Histo_t is a ROOT TH1 :
     * HistoPtr_t h1 = array.hist(index);
     * h1->Draw();                               // ERROR: will segfault at some point
     * h1=static_cast<HistoPtr_t>(h1->Clone());  // this should work
     * </pre>
     */
    const HistoPtr_t histo(const PixA::Index_t &index) const {
      if (index.size()>0) {
	return histo(index, 0);
      }
      else {
	return 0;
      }
    }

  protected:

    void addHisto(const PixA::Index_t &index, unsigned int level, HistoPtr_t histo_ptr);

    const HistoPtr_t histo(const PixA::Index_t &index, unsigned int level) const {
      assert(level<index.size());
      if (level+1==index.size()) {
	if (index[level] < m_histo.size()) return m_histo[ index[level] ].ptr();
      }
      else {
	if (m_child.size() <= index[level] || !m_child[ index[level] ]) return 0;
	return m_child[ index[level] ]->histo(index, level+1);
      }
      return 0;
    }

    std::vector<AccountingHistoPtr_t>   m_histo;
    std::vector<HistoArray *> m_child;
    unsigned int m_level;
  };

  class TopLevelHistoArray : public HistoArray
  {
  public:

    TopLevelHistoArray(bool update_info=false) : m_updateInfo(update_info) {
      // initialise the info array with an unpossible value.
      if (update_info) {
	m_info.reset();
      }
      else {
	m_info.setMaxIndex(0,static_cast<unsigned int>(-1));
      }
    }

    /** Return true if the array does not contain any histograms.
     * If the cache contains such an element the histogram array can
     * be considered as missing for the given connectivity object.
     */
    bool empty() const {
      return !hasValidInfo() && m_histo.empty() && m_child.empty();
    }

    /** Return the histogram information.
     * The histogram information contains the number of childs or histograms per level.
     */
    const HistoInfo_t &info() const {
      return m_info;
    }

    /** Return true if the histogram info is initialised.
     */
    bool hasValidInfo() const {
      return m_info.maxIndex(0)!=static_cast<unsigned int>(-1);
    }

    /** Set histogram info.
     * The histogram information contains the number of childs or histograms per level.
     */
    void setInfo(const HistoInfo_t &info) {
      m_info = info;
    }

    void addHisto(const PixA::Index_t &index, HistoPtr_t histo_ptr) {
      if (!histo_ptr) return;

      // paranoia checks
      if (m_updateInfo) {
	updateInfo( index );
      }
      else {
	assert( checkIndex( index ) );
      }
      HistoArray::addHisto(index,histo_ptr);

    }

    bool checkIndex(const PixA::Index_t &index) const {
      if (index.empty()) return false;

      if (hasValidInfo()) {
	if (index.size()>m_info.maxLevels()) return false;

	for (unsigned int level_i=0; level_i+1<index.size(); level_i++) {
	  if  ( index[level_i] >= m_info.maxIndex(level_i) ) return false;
	}
	if ( index.back() < m_info.minHistoIndex() || index.back()>= m_info.maxHistoIndex() ) return false;
      }
      return true;
    }

    void updateInfo(const PixA::Index_t &index) {
      if (index.empty() || index.size()>m_info.maxLevels()) return;

      for (unsigned int level_i=0; level_i<index.size()-1; level_i++) {

	if  ( index[level_i] >= m_info.maxIndex(level_i) ) {
	  m_info.setMaxIndex(level_i, index[level_i] + 1);
	}
      }
      if (index.back()>= m_info.maxHistoIndex()) {
	m_info.setMaxHistoIndex(index.back()+1);
      }
      if (index.back()< m_info.minHistoIndex()) {
	m_info.setMinHistoIndex(index.back());
      }
    }


  private:
    HistoInfo_t m_info;
    bool m_updateInfo;
  };

  class TopLevelUpdateHistoArray : public TopLevelHistoArray
  {
  public:
    TopLevelUpdateHistoArray() : TopLevelHistoArray(true) {}
  };

}
#endif
