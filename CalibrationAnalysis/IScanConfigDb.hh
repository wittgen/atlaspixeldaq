#ifndef _CAN_IScanConfigDb_hh_
#define _CAN_IScanConfigDb_hh_

#include "exception.hh"
#include <ConfigWrapper/pixa_exception.h>
#include <ConfigWrapper/ConfigRef.h>

#include "Common.hh"

namespace CAN {

  /** Exception which gets thrown if a missing module, boc or scan config is accessed.
   */
  typedef PixA::ConfigException ConfigException;

  class IScanConfigDb
  {
  public:
    virtual ~IScanConfigDb() {};

    /** Get a scan configuration.
     * @throw ConfigException in case no scan config exists for the given serial number.
     */
    virtual PixA::ConfigRef config(const std::string &name, const std::string &tag, unsigned int revision = 0) = 0 ;
      virtual PixA::ConfigRef config_1(const std::string &name, const std::string &tag, unsigned int revision = 0, std::string string="" ) = 0 ;


  };

}

#endif
