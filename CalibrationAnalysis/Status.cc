#include "Status.hh"
#include <sstream>
#include <iomanip>

namespace CAN {

  const char *AnalysisStatus::s_name[kAborted+1]={
    "Unknown",
    "Submitted",
    "Pending",
    "Waiting",
    "Ready",
    "Running",
    "Success",
    "Failure",
    "TimeOut",
    "Aborted"
  };

  const std::string AnalysisStatus::s_empty;
  const std::string AnalysisStatus::s_statusVarName("status");
  const std::string AnalysisStatus::s_globalStatusVarName("globalStatus");

  std::string AnalysisStatus::extractRodName(const std::string &full_is_var_name)
  {
    std::string rod_name(full_is_var_name);
    if (rod_name.size() > 7) {
      rod_name.erase(rod_name.size()-7,7);
      std::string::size_type pos = rod_name.rfind("/");
      if (pos != std::string::npos) {
	rod_name.erase(0,pos+1);
	return rod_name;
      }
    }
    return s_empty;
  }

  std::string AnalysisStatus::makeRegex(SerialNumber_t analysis_serial_number) {
    std::stringstream is_dictionary_string;
    is_dictionary_string << ".*"
			 << '/' << s_measurementTypeId << std::setw(9) << std::setfill('0') << analysis_serial_number
			 << "/.*/" << varName();
    return is_dictionary_string.str();
  }


  std::string AnalysisStatus::makeIsVarName(const std::string &is_server_name, const std::string &rod_connectivity_name, SerialNumber_t analysis_serial_number)
  {
    std::stringstream is_dictionary_string;
    is_dictionary_string << is_server_name << '.'
			 << '/' << s_measurementTypeId << std::setw(9) << std::setfill('0') << analysis_serial_number
			 << '/' << rod_connectivity_name << '/' << varName();
    return is_dictionary_string.str();
  }

  std::string AnalysisStatus::makeGlobalIsVarName(const std::string &is_server_name, SerialNumber_t analysis_serial_number)
  {
    std::stringstream is_dictionary_string;
    is_dictionary_string << is_server_name << '.'
			 << '/' << s_measurementTypeId << std::setw(9) << std::setfill('0') << analysis_serial_number
			 << '/' << globalVarName();
    return is_dictionary_string.str();
  }

  std::string AnalysisStatus::makeIsHeader(const std::string &is_server_name, SerialNumber_t analysis_serial_number)
  {
    std::stringstream is_dictionary_string;
    if (!is_server_name.empty()) {
      is_dictionary_string << is_server_name << '.';
    }
    is_dictionary_string << '/' << s_measurementTypeId << std::setw(9) << std::setfill('0') << analysis_serial_number;
    return is_dictionary_string.str();
  }
}
