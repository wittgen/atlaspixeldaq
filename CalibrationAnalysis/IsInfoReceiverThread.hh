#ifndef _CAN_IsInfoReceiverThread_hh_
#define _CAN_IsInfoReceiverThread_hh_

#include "Flag.hh"
#include <is/inforeceiver.h>
// replaced removed run/stop functions from ISInfoReceiver
// by corresponding functions from OWLsemaphore
#include <owl/semaphore.h>

namespace CAN {

  class IsInfoReceiverThread : public omni_thread
  {
  public:
    IsInfoReceiverThread(IPCPartition &partition) : m_isReceiver(partition) {}
    void listen() { start_undetached(); }
    //void shutdown() { m_isReceiver.stop(); }
    void shutdown() { m_semaphore.post(); }

    void shutdownWait() {     
      if (state()==omni_thread::STATE_RUNNING) {
	m_exit.wait(); 
      }
    }

    ISInfoReceiver &receiver() {return m_isReceiver;}

  private:
    /** inhibit */
    void start() {};

    void *run_undetached(void *arg) {
      // thread should ne be started with arguments
      assert (arg==NULL);
      //      m_isReceiver.run();
      m_semaphore.wait();
      m_exit.setFlag();

      return NULL;
    }

    ISInfoReceiver m_isReceiver;
    OWLSemaphore m_semaphore;
    Flag m_exit;
  };

}

#endif
