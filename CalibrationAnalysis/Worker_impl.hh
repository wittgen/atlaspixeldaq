#ifndef _Worker_impl_hh_
#define _Worker_impl_hh_

#include "Worker.hh"
#include <string>
#include "ExecutionUnit.hh"
#include "AnalysisRequestProcessor.hh"
#include "IMasterDataServer.hh"
#include "IsDictionary.hh"

#include "LogLevel.hh"

#include "omnithread.h"
#include "Flag.hh"
#include "IWorker.hh"
#include "RestartThread.hh"

namespace PixLib{
  class PixMessages;
}

namespace CAN {

  /*
   * threads:
   *  - histogram download
   *  - analysis request processor
   *  - analysis executor
   *  - analysis
   *
   * executeAnalysis :
   *  - queue analysis request
   *
   * analysis request queue :
   *  - query AnalysisInfo_t from database server (cached)
   *  - ask factory for analysis object (pass AnalysisInfo_t) (cached).
   *      (analysis object provides analyse method which is able to analyse any ROD)
   *  - call analysis prefetch method for analysis 
   *      + queues histogram requests
   *  - queue analysis + rod in analysis queue
   * 
   * histogram download
   *  - download all requested histograms ordered per ascending analysis serial number and ROD
   *  - if all histograms for one ROD of one analysis serial number are downloaded mask 
   *    corresponding analysis as ready and signal analysis executor
   * 
   * analysis executor
   *  - while there are free analysis slots execute analyses masked as ready, ordered by ascending 
   *    analyis serial number.
   *
   * cache :
   *  - caches arbitrary objects
   *  - per object type structures
   *  - 
   *  - information : 
   *     + object specific key to find object in per object structure 
   *     + number of users (shared_ptr)
   *     + flag : destruction in next go possible, marked for destruction in next go
   *     + relevance indicator (objects with lower relevance will be deleted first)
   *  - cached objects :
   *     + AnalysisInfo_t
   *     + ScanInfo_t
   *     + ScanParameter_t
   *     + Histograms
   *     + Reference distributions
   *     + Analysis objects
   *     + Module configs ?
   *     + BOC configs ?
   */
  class Worker_impl  : public IPCNamedObject<POA_CAN::Worker,ipc::single_thread>, public IWorker
  {
  public:
    /** Initialise the Worker.
     */
    Worker_impl(IPCPartition &ipc_partition,
		//IPCPartition &mrs_partition, 
		const std::string &can_is_server_name,
		IMasterDataServer * masterDataServer,
		std::vector<IResultStore *> result_store_list,
		const std::string &name,
		unsigned int n_slots);
		//,RestartThread *restart_thread=nullptr);

    /** Initialise the Worker with dummy data server used by the test plugin
     */
    //    Worker_impl(IPCPartition &ipc_partition, IPCPartition &mrs_partition, const std::string &name, unsigned int n_slots);
    Worker_impl(IPCPartition &ipc_partition, const std::string &name, unsigned int n_slots);

    ~Worker_impl();

    void submitAnalysis(SerialNumber_t    analysis_serial_number,
			const AnalysisInfoData_t &analysis_info,
			const StringVec &rod_conn_name);

    bool dequeuAnalysis(SerialNumber_t    analysis_serial_number,
			const char *rod_conn_name);

    /** Abort all pending, waiting, running tasks associated to the given analysis serial  number.
     */
    void abortAnalysis(SerialNumber_t    analysis_serial_number, bool ignore_results);

    /** Abort analysis of the given serial number and rod name.
     */
    void abortJob(SerialNumber_t analysis_serial_number, const char *rod_conn_name, bool ignore_results);

    void getQueueStatus(CORBA::ULong &jobs_waiting_for_data,
			CORBA::ULong &jobs_waiting,
			CORBA::ULong &jobs_running,
			CORBA::ULong &jobs_exectued,
			CORBA::ULong &empty_slots);

    /** Change the verbosity of the scheduler and the worker.
     * @param level the verbosity level : 0=fatal, 1=error, 2=warning,3=information,>=4 diagnostics.
     */
    void setLogLevel(unsigned short level);

    void restart() {
      triggerRestart();
    }

    void triggerRestart() {
      if (m_restartThread) {
	m_restartThread->restart();
      }
      else {
	_restart();
      }
    }

    void reset();

    void shutdown() {
      if (m_restartThread) {
	m_restartThread->kill();
      }
      else {
	_shutdown();
      }     
    }

    bool alive();

    bool respawnWorker() const { return m_restart; }

    void _restart();

    void _shutdown();

  private:

    RestartThread                   *m_restartThread; /**< TODO: currently the restart thread does not get destructed. */

    //    std::auto_ptr<MRSStream>         m_out;
    IPCPartition                     m_partition;
    IsDictionary                     m_isDictionary;
    std::unique_ptr<IMasterDataServer> m_dataServer;
    ExecutionUnit                    m_executionUnit;
    AnalysisRequestProcessor         m_analysisRequestProcessor;
    std::string                      m_ident;
    ELogLevel                        m_logLevel;

    bool                             m_registered;
    bool                             m_restart;
  };
}
#endif
