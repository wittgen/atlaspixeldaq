#ifndef _CAN_IMasterDataServer_hh_
#define _CAN_IMasterDataServer_hh_

#include <memory>
#include "Common.hh"
#include "IExecutionUnit.hh"

#include <ConfigWrapper/Locations.h>
#include <string>

#include "LogLevel.hh"

namespace PixA {
  class DisabledListRoot;
}

namespace CAN {

  class IDataServer;

  class IMasterDataServer {
  public:
    virtual ~IMasterDataServer() {}

    /** Create a server which serves histograms and configs for the given analysis and ROD.
     * @param analysis_serial_number serial number of the analysis.
     * @param rod_conn_name the connectivity name of the ROD.
     * @param scan_serial_number the serial number of the scan which is to be analysed
     * @param scan_info the scan meta data.
     * @param src_analysis_serial_number the source analysis serial number if the analysis analyses an analysis otherwise zero.
     * @param conn a reference for the connectivity.
     * @param disabled_lists the a list which contains for each source a list of disabled RODs, modules.
     * This function only creates a data server. It is added with @ref addDataServer.
     * Data servers must not be added if they were created by a different master data server than the one
     * to which they are added.
     * Once a data server is created, the user can request data, which should be provided by
     * the data server during the analysis.
     */
    virtual std::shared_ptr<IAnalysisDataServer> createAnalysisDataServer(SerialNumber_t analysis_serial_number,
									    const PixA::RodLocationBase &rod_conn_name,
									    SerialNumbers_t scan_serial_numbers,
									    const InfoPtr_t<PixLib::ScanInfo_t> &scan_info,
									    const SerialNumber_t src_analysis_serial_number,
									    PixA::ConnectivityRef &conn,
									    const std::vector<PixA::DisabledListRoot> &disabled_lists) = 0;

    /** Add a data server to the master data server.
     * @param analysis_serial_number the serial number of the analysis
     * @param rod_conn_name the connectivity name of the ROD.
     * @param data_server the data server to be added.
     * After a data server has been added to the master data server 
     */
    virtual void addAnalysisDataServer(SerialNumber_t analysis_serial_number,
				       const PixA::RodLocationBase &rod_conn_name,
				       const std::shared_ptr<IAnalysisDataServer>  &data_server) = 0;

    /** Start the data server and signal the given execution unit when data becomes available.
     */
    virtual void start(IExecutionUnit &) = 0;

    /** Abort a single analysis.
     */
    virtual void abort(SerialNumber_t analysis_serial_number) = 0;

    /** Reset all data request queues.
     */
    virtual void reset() = 0;

    /** Ask the data server to shutdown.
     */
    virtual void initiateShutdown() = 0;

    /** Wait until the data server has shutdown.
     */
    virtual void shutdownWait() = 0;

    /** Set the log level of the master data server.
     */
    virtual void setLogLevel(ELogLevel log_level) = 0;

  };

}

#endif
