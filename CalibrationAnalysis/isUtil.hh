#ifndef _isUtil_h_
#define _isUtil_h_

#include <is/infodictionary.h>
#include <is/infoT.h>


#ifdef TDAQ010601

#  include <stdexcept>
namespace daq {

  namespace is {

    class Exception : public std::runtime_exception
    {
    public:
      Exception(const std::string &message) : std::runtime_exception(message) {}
    };

  }

}

#endif



template <class T>
inline void setIsValue(ISInfoDictionary &dictionary, ISType &type, const std::string &is_dictionary_name, ISInfoT<T> &info_val)
{
#ifdef TDAQ010601
  if (dictionary.findType(is_dictionary_name,type) == ISInfo::Success) {
    dictionary.insert(is_dictionary_name, info_val );
    // // throw exception in case the insert failes
    // std::stringstream message;
    // message << "Failed to insert " << is_dictionary_name << " into IS dictionary." 
    // throw daq::is::Exception(message.str());
  }
  else {
    dictionary.update(is_dictionary_name, info_val );
  }
#else
  try {
    dictionary.findType(is_dictionary_name,type);
    dictionary.update(is_dictionary_name, info_val );
  } catch ( daq::is::Exception & ex ) {
    dictionary.insert(is_dictionary_name, info_val );
  }
#endif
}

template <class T>
inline void updateIsValue(ISInfoDictionary &dictionary, const std::string &is_dictionary_name, ISInfoT<T> &info_val) 
{
#ifdef TDAQ010601
    dictionary.update(is_dictionary_name, info_val );
    // // throw exception in case the insert failes
    // std::stringstream message;
    // message << "Failed to insert " << is_dictionary_name << " into IS dictionary." 
    // throw daq::is::Exception(message.str());
#else
    dictionary.update(is_dictionary_name, info_val );
#endif
}


#endif

