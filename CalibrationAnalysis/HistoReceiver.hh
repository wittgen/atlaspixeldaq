#ifndef _CAN_HistoReceiver_hh_
#define _CAN_HistoReceiver_hh_

#include "IHistoReceiver.hh"
#include <map>

class IPCPartition;

namespace CAN {

  class HistoReceiver : public IHistoReceiver
  {
  public:
    HistoReceiver(IPCPartition &oh_partition,
		  const std::string &oh_server_name,
		  bool verbose=false);

    ~HistoReceiver();

    /** Fill all histograms with the given ROD, connectivity and histogram name into the pix scan histo array.
     * @param scan_serial_number
     * @param rod_name the name of the ROD
     * @param conn_name the connectivity name
     * @param histo_name the pix scan histo name
     * @param array the pix scan histo array to be filled.
     * @return the number of retrieved histograms.
     * The histograms are expected to be named : "ROD"/"connectivity name"/"histogram name"/(Ax/)(Bx/)(Cx/)x:"original histogram name"
     * @todo what about analysis histograms ? 
     */
    unsigned int getHistograms(SerialNumber_t scan_serial_number,
			       const std::string &rod_name,
			       const std::string &conn_name,
			       const std::string &histo_name,
			       TopLevelHistoArray &array);
  private:

    unsigned int getHistograms(const std::string &pix_scan_histo_name,
			       TopLevelHistoArray &array,
			       const std::string &oh_server_name);

    IPCPartition *m_partition;
    std::string   m_ohServerBaseName;
    std::map<std::string,std::string> m_rodToHistoServerName;
    bool m_verbose;
  };

}
#endif
