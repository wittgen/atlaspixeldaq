#ifndef __SCAN_RESULT_LISTENER_H__
#define __SCAN_RESULT_LISTENER_H__
#include "ScanResultStreamer.h"
#include "DataContainer/IScanResultListener.h"
#include "DataContainer/PixDbDataContainer.h"
#include "DataContainer/HistoRef.h"
#include "DataContainer/HistoInfo_t.h"
#include <iostream>
#include <sstream>
#include <PixController/PixScan.h>

#include <ConfigWrapper/ConfObjUtil.h>
#include <ConfigWrapper/ConfigHandle.h>
#include <list> 
#include <ConfigWrapper/ScanConfig.h>


enum DEBUG_LEVEL {INFO, WARNING, ERROR};

class ScanResultListener : public PixA::IScanResultListener 
{
public:
  
  class MHisto
  {
  public:
    MHisto(PixA::HistoHandle *h,std::string pp0N,std::string moduleN,int t,PixA::ScanConfigRef *scan) 
      :handle(h),pp0Name(pp0N),moduleName(moduleN),type(t),scanConf(scan) {};
    ~MHisto() {};
    PixA::HistoHandle *handle;
    std::string pp0Name;
    std::string moduleName;
    int type;
    PixA::ScanConfigRef *scanConf;
  };

  ScanResultListener(): debugLevel(INFO) {};

  ~ScanResultListener() {
    std::list<MHisto>::iterator iter=m_histos.begin();
    std::list<MHisto>::iterator end=m_histos.end();
    for(;iter!=end;iter++) {
      delete iter->handle;
      delete iter->scanConf;
		}
  };

  void newFile(const std::string &name) {}

  void newLabel(const std::string &name, const PixA::ConfigHandle &label_config);

  /** Intentionally does nothing.
   */
  void newScanInfo(const PixA::ConfigHandle&) {}

  /** Intentionally does nothing.
   */
  void newPixScan(const PixA::ConfigHandle&) {}

  /** Called whenever the pix module group changes.
   */
  void newPixModuleGroup(const std::string &name) {
    Cout("newPixModuleGroup] pix module group=\""+name+"\"");
  }

  /** Called whenever the pix module group changes.
   */
  void finishPixModuleGroup() {
    Cout("finishPixModuleGroup] done with pix module group.");
  }

  /** Called whenever the Pp0 changes.
   * newPp0 will never be called without newPixModuleGroup being called.
   */
  void newPp0(const std::string &name) {
    Cout("newPp0] PP0=\""+name+"\"");
    //    if(m_requested_pp0!="" && name!=m_requested_pp0) m_found_pp0=false;
    //    else m_found_pp0=true;
    m_found_pp0=true;
    m_currentpp0Name=name;
    //    m_scanConfigRefs = new std::list<const PixA::ScanConfigRef*>;
  }

  void finishPp0() {
    Cout("finishPp0] done with PP0.");
  }

  /** Called whenever the result folder changes
   * @param folder_list the names of the folder hierarchy
   */
  void newFolder(const std::vector<std::string> &folder_hierarchy);

  //  /** Called whenever the scan changes
  //   * @param folder_list the names of the folder hierarchy
  //   */
  //   void newScan(const ScanConfigRef &scan_config) = 0;
  
  /** Called for each pix scan histo.
   */
  void newPixScanHisto(const std::string &histo_name, const PixA::ScanConfigRef &scan_config, const PixA::HistoHandle &histo_handle);
  
    /** Called for each normal histo.
     */
  void newHisto(const std::string &name, const PixA::HistoHandle &histo_handle, const std::string &rod_name, const PixA::ConfigHandle &scan_config);

  void finish() {
    Cout("finish] all done.");
  }

  /** Does intentionally nothing.
   */
  void reportProblem(const std::string &) {}

private:
  static void initHistoNameTypeList();
  static int getHistoType(const std::string &histo_name);
  static std::map< std::string, int > s_mhistoNameToType;

  PixA::ConnectivityRef m_conn;

  //  std::map< int, std::list<PixA::HistoHandle*> > m_foundHistos;  //list of found histos for each histo_type 
  //  std::map< int, std::list<std::string> > m_foundModuleNames;
  bool m_found_pp0;
  //  std::list<std::string> m_found_lables;
  //  std::map< int, std::list<PixA::ScanConfigRef*> > m_scanConfigRefs;
  //  //  std::string m_requested_pp0; //only known in Streamer
  std::string m_currentpp0Name;
  //  std::list<std::string> m_pp0Names;  

  ////
  std::list<MHisto> m_histos;
  ////

  DEBUG_LEVEL debugLevel; 
  void Cout(std::string message, bool newline=true, DEBUG_LEVEL dl=INFO) {
    std::stringstream out;
    switch(dl) {
    case INFO: out << "INFO"; break;
    case WARNING: out << "WARNING"; break;
    case ERROR: out << "ERROR"; break;
    }
    out << "[ScanResultListener::" << message;
    if(newline) out << std::endl;
    if(debugLevel<=dl) std::cout << out.str();
  }
public:
  const PixA::ConnectivityRef &GetConnectivity() { return m_conn; };
  
  void SetDebugLevel(DEBUG_LEVEL dl) { debugLevel=dl; };

  std::list<PixA::ScanConfigRef*> getScanConfigRefs(std::string histo_typeStr,const std::string &pp0name) { 
    int histo_type = getHistoType(histo_typeStr);
//     //check if have entries of this type
//     std::map< int, std::list<PixA::ScanConfigRef*> >::const_iterator histo_type_iter = m_scanConfigRefs.find(histo_type);
//     if( histo_type_iter == m_scanConfigRefs.end() ) 
//       return NULL; 
//     return &m_scanConfigRefs[histo_type];

    std::list<PixA::ScanConfigRef*> mOut;    
    std::list<MHisto>::const_iterator iter=m_histos.begin();
    std::list<MHisto>::const_iterator end=m_histos.end();
    for(;iter!=end;iter++)
      if(iter->type==histo_type && iter->pp0Name==pp0name) mOut.push_back( iter->scanConf );
    return mOut;
  }

  std::list<PixA::HistoHandle*> GetHistos(std::string histo_typeStr, const std::string &pp0name) { 
    int histo_type = getHistoType(histo_typeStr);
//     //check if have entries of this type
//     std::map< int, std::list<PixA::HistoHandle*> >::const_iterator histo_type_iter = m_foundHistos.find(histo_type);
//     if( histo_type_iter == m_foundHistos.end() ) 
//       return NULL; //&std::list<PixA::HistoHandle>(); //dummy
//     return &m_foundHistos[histo_type];

    std::list<PixA::HistoHandle*> mOut;    
    std::list<MHisto>::const_iterator iter=m_histos.begin();
    std::list<MHisto>::const_iterator end=m_histos.end();
    for(;iter!=end;iter++)
      if(iter->type==histo_type && iter->pp0Name==pp0name) mOut.push_back( iter->handle );
    return mOut;
  };
  std::list<std::string> GetModuleNames(std::string histo_typeStr, const std::string &pp0name) { 
    int histo_type = getHistoType(histo_typeStr);
//     //check if have entries of this type
//     std::map< int, std::list<std::string> >::const_iterator histo_type_iter = m_foundModuleNames.find(histo_type);
//     if( histo_type_iter == m_foundModuleNames.end() ) 
//       return NULL; //&std::list<PixA::HistoHandle>(); //dummy
//     return &m_foundModuleNames[histo_type];

    std::list<std::string> mOut;    
    std::list<MHisto>::const_iterator iter=m_histos.begin();
    std::list<MHisto>::const_iterator end=m_histos.end();
    for(;iter!=end;iter++)
      if(iter->type==histo_type && iter->pp0Name==pp0name) mOut.push_back( iter->moduleName );
    return mOut;
  };

  std::list<std::string> Getpp0Names() { 
    //    int histo_type = getHistaoType(histo_typeStr);
//     //check if have entries of this type
//     std::map< int, std::list<std::string> >::const_iterator histo_type_iter = m_foundModuleNames.find(histo_type);
//     if( histo_type_iter == m_foundModuleNames.end() ) 
//       return NULL; //&std::list<PixA::HistoHandle>(); //dummy
//     return &m_foundModuleNames[histo_type];

    std::list<std::string> mOut;    
    std::list<MHisto>::const_iterator iter=m_histos.begin();
    std::list<MHisto>::const_iterator end=m_histos.end();
    for(;iter!=end;iter++)
      if(find(mOut.begin(),mOut.end(),iter->pp0Name)==mOut.end()) mOut.push_back( iter->pp0Name );
    return mOut;
  };

  bool Found_pp0(std::string pp0name) { 
    std::list<std::string> mOut;    
    std::list<MHisto>::const_iterator iter=m_histos.begin();
    std::list<MHisto>::const_iterator end=m_histos.end();
    for(;iter!=end;iter++)
      if(iter->pp0Name==pp0name) return true;
    return false;
  } 

  bool Found_type(std::string typeStr) { 
    int histo_type = getHistoType(typeStr);
    std::list<std::string> mOut;    
    std::list<MHisto>::const_iterator iter=m_histos.begin();
    std::list<MHisto>::const_iterator end=m_histos.end();
    for(;iter!=end;iter++)
      if(iter->type==histo_type) return true;
    return false;
  } 

  //  bool Found_pp0() { return !m_foundHistos.empty(); } //only one previously requested pp0 is considered !

//   std::list<std::string> * GetFound_lables() {
//     return &m_found_lables;
//   }
};

//std::map< std::string, int> ScanResultListener::s_mhistoNameToType;
#endif


