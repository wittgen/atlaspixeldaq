#ifndef _CAN_IExecutionUnit_hh_
#define _CAN_IExecutionUnit_hh_

#include <ConfigWrapper/Locations.h>
#include "Common.hh"
#include "Joblett_t.hh"
#include "IMetaDataService.hh"

namespace PixA {
  class ConnectivityRef;
  class DisabledListRoot;
}

namespace CAN {

  /** Reduced interface of the execution unit.
   * @todo Think about : since there is and will be only one implementation, the virtualisation may not be very useful.
   */
  class IExecutionUnit
  {
  public:
    virtual ~IExecutionUnit() {}

    /**  Queue the list of jobs.
     * @param analysis_serial_number the serial number of the analysis the jobs are about
     * @param scan_serial_number the scan serial number.
     * @param scan_info the scan meta data.
     * @param src_analysis_serial_number serial number of a source analysis or zero
     * @param conn reference to a valid connectivity which is needed by the data server (because it has to provide the connectivity to the analysis).
     * @param disabled_lists the a list which contains for each source a list of disabled RODs, modules.
     * @param joblett_begin begin iterator of the job list.
     * @param joblett_end   end iterator of the job list.
     * @todo other solution than passing the connectivity as a parameter ?
     */
    virtual void queueJob(const SerialNumber_t analysis_serial_number,
			  const SerialNumbers_t scan_serial_numbers, //CLA
			  const InfoPtr_t<PixLib::ScanInfo_t> &scan_info,
			  const SerialNumber_t src_analysis_serial_number,
			  PixA::ConnectivityRef &conn,
			  const std::vector<PixA::DisabledListRoot> &disabled_lists,
			  JoblettList_t::iterator joblett_begin,
			  JoblettList_t::const_iterator joblett_end) = 0;

    /** inform the execution unit that the data server has the requested data.
     * @param serial_number the serial number for which the data is available
     * @param rod_location the ROD connectivity name for which the data is available
     * @return true if a job was waiting for this information.
     * There should always be a job which is waiting for this information, unless the
     * queues are resetted in between.
     */
    virtual bool dataAvailable(SerialNumber_t serial_number, const PixA::RodLocationBase &rod_location) = 0;

    /** Ask the execution to abort the processing of the given analysis.
     * @param analysis_serial_number the serial number of the analysis which should be aborted
     * @param ignore_results set to true if the results of the anlysis should be ignored ( what ever that could mean)
     */
    virtual void abortAnalysis(SerialNumber_t analysis_serial_number, bool ignore_results) = 0;

    /** Inform the execution unit that the data server is down.
     */
    virtual void dataServerIsDown() = 0;

  };

}
#endif
