#ifndef _CAN_Joblett_t_hh__
#define _CAN_Joblett_t_hh__

#include <memory>
#include "Common.hh"
#include <is/infoT.h>
#include <string>
#include <cassert>
#include <memory>
#include <ConfigWrapper/Locations.h>
#include "IAnalysisPerROD.hh"
#include "IClassification.hh"
#include "IPostProcessing.hh"
#include "ExtendedAnalysisResultList_t.hh"
#include "Status.hh"

#include <vector>
#include <utility>

namespace CAN {
  
    class IAnalysisPerROD;
  //    class IAnalysisPerModule;
  //    class IAnalysisPerPP0;
    class IClassification;
    class IPostProcessing;

    class JobObjects_t {
    public:
      JobObjects_t(IAnalysisPerROD *analysis,
	    IClassification *classification,
	    IPostProcessing *post_processing)
	: m_analysis(analysis),
	  m_classification(classification),
	  m_postProcessing(post_processing) {};

      /** Return a pointer to the analysis object.
       * The analysis object is owned by the job and must not be deleted.
       */
      IAnalysisPerROD *analysis() {return m_analysis.get();}

      /** Return a pointer to the classification object.
       * The clasification object is owned by the job and must not be deleted.
       */
      IClassification *classification() {return m_classification.get();}

      /** Return a pointer to the post-processing object.
       * The clasification object is owned by the job and must not be deleted.
       */
      IPostProcessing *postProcessing() {return m_postProcessing.get();}
    private:
      std::unique_ptr<IAnalysisPerROD> m_analysis;
      std::unique_ptr<IClassification> m_classification;
      std::unique_ptr<IPostProcessing> m_postProcessing;
    };


    /** Piece of an analysis which is responsible for the processing of one ROD.
    */
    class Joblett_t {
      friend class AnalysisRequestProcessor;
      //      friend class ExecutionUnit::Request_t;

    public:

      //    protected:
      /** Creates a partial initialised jobs.
       * @param analysis_serial_number the serial number of the analysis which used for the IS dictionary name
       * @param rod_name the name of the ROD this job will be dealing with which is used for the IS dictionary name.
       * @param can_is_server_name the name of the IS server where the analysis status gets published.
       * The objects are not yet set.
       */
      Joblett_t(SerialNumber_t analysis_serial_number, const PixA::RodLocation &rod_name, const std::string &can_is_server_name);

      void setObjects(std::shared_ptr<JobObjects_t> objects) { m_objects = objects; }

      void setDataServer(std::shared_ptr<IAnalysisDataServer> &data_server) { 
	//	std::cout << "INFO [Joblett_t::setDataServer] got " << static_cast<void *>(data_server.get()) << " for  " << m_isDictionaryName << "." << std::endl;
	m_dataServer = data_server; 
      }

      std::shared_ptr<IAnalysisDataServer> &dataServer()                     { 
	//	std::cout << "INFO [Joblett_t::dataServer] use " << static_cast<void *>(m_dataServer.get()) << " for " << m_isDictionaryName << std::endl;
	return m_dataServer;
      }



      Joblett_t(std::shared_ptr<JobObjects_t> objects)
	: m_objects(objects),
	  m_results(new ExtendedAnalysisResultList_t),
	  m_abortRequest(false)
      {
	m_status=AnalysisStatus::kWaiting;
      }


      /** Return a pointer to the analysis object.
       * The analysis object is owned by the job and must not be deleted.
       */
      IAnalysisPerROD *analysis() {return m_objects->analysis();}

      /** Return a pointer to the classification object.
       * The clasification object is owned by the job and must not be deleted.
       */
      IClassification *classification() {return m_objects->classification();}

      /** Return a pointer to the post-processing object.
       * The clasification object is owned by the job and must not be deleted.
       */
      IPostProcessing *postProcessing() {return m_objects->postProcessing();}

      /** Called by the execution unit when the job is executed.
       */
      void setWaiting() { m_status=AnalysisStatus::kWaiting;}

      /** Called by the data server when all histograms have been downloaded.
       */
      void setReady()   { m_status=AnalysisStatus::kReady;}

      /** Called by the execution unit when the job is executed.
       */
      void setRunning() { m_status=AnalysisStatus::kRunning;}

      /** Called by the execution unit when the job is executed.
       */
      void setAborted() { m_status=AnalysisStatus::kAborted; setIsDone();}


      /** Called by the JobThread when the job was successful.
       */
      void setSuccess() { m_status=AnalysisStatus::kSuccess;}


      /** Called by the JobThread when the job was a failure.
       */
      void setFailure() { m_status=AnalysisStatus::kFailure;}


      bool isReady() const   {return m_status == AnalysisStatus::kReady;}
      bool isRunning() const {return m_status == AnalysisStatus::kRunning;}

      AnalysisStatus::EStatus status() const { return m_status; }

      void updateIsStatus() { m_isStatus.setValue(AnalysisStatus::name(m_status) ); }
      //      void updateStatusFrom() { m_status = AnalysisStatus::status( m_isStatus.getValue() ); }

      const ISType &isStatusType() const { return m_isStatus.type(); }
      ISInfoString &isStatus()  { updateIsStatus(); return m_isStatus; }
      const std::string &isDictionaryName() {return m_isDictionaryName;}

      bool ignoreResults() const {return m_ignoreResults; }

      void requestAbort() {
	m_abortRequest=true;
	if (analysis()) {
	  analysis()->requestAbort();
	}
	//	if (!isRunning()) {
	//	  setAborted();
	//	}
      }

      bool isAbortRequested() const {
	return m_abortRequest;
      }

      void requestAbortAndIgnore() {
	requestAbort();
	m_ignoreResults = true;
      }

      ExtendedAnalysisResultList_t *takeResults()         { return m_results.release(); }

      ExtendedAnalysisResultList_t &results()             { return *m_results.get(); }

      const ExtendedAnalysisResultList_t &results() const { return *m_results.get(); }

      /** Mark joblett as done provided it is not running anymore.
       */
      void setIsDone() { assert (!isRunning()); m_isDone = true; }

      /** Return true if the joblett is marked as done.
       */
      bool isDone() const { return m_isDone; }

    private:
      std::shared_ptr<JobObjects_t> m_objects;
      std::shared_ptr<IAnalysisDataServer>  m_dataServer;

      std::unique_ptr<ExtendedAnalysisResultList_t> m_results;

      
      AnalysisStatus::EStatus m_status;
      ISInfoString            m_isStatus;
      std::string             m_isDictionaryName;
      bool m_abortRequest;
      bool m_ignoreResults;
      bool m_isDone;
    };

  typedef std::pair<PixA::RodLocationBase, std::shared_ptr<Joblett_t> > JoblettStub_t;
  typedef std::vector< JoblettStub_t > JoblettList_t;

}
#endif
