#ifndef _CAN_AnalysisPerX_hh_
#define _CAN_AnalysisPerX_hh_

#include "Analysis.hh"
#include "ExtendedAnalysisResultList_t.hh"


namespace CAN {

  class AnalysisPerROD;

  /** A special implementation of an analysis per PP0 which uses an analysis which anlyses per Module.
   */
  class AnalysisPerPP0 : public IAnalysisPerPP0
  {
    friend class AnalysisPerROD;

  public:
    /** Take ownership of the given analysis and pretend to have PP0 granularity.
     */
    AnalysisPerPP0(IAnalysisPerModule &analysis);

    /** calls the destructor of the member analysis.
     */
    ~AnalysisPerPP0() { delete m_analysis;}

    const std::string &name() const {return m_analysis->name();} 

    /** Call foreach module of the given pp0 the prefetch method of the member analysis.
     */
    void requestHistograms(IAnalysisDataServer *data_server,
			   const PixA::Pp0LocationBase &pp0_location);


    /** Call for each module of the given PP0 the analyse method of the member analysis.
     */
    void analyse(IAnalysisDataServer *data_server,
		 const PixA::Pp0LocationBase &pp0_location,
		 ExtendedAnalysisResultList_t &analysis_results);

    SerialNumber_t serialNumber() const { return m_analysis->serialNumber(); }

    void requestAbort()         { return m_analysis->requestAbort(); }

    void setLogLevel(ELogLevel log_level) {
      m_analysis->setLogLevel(log_level);
    }

  protected:

    bool abortRequested() const { return m_analysis->abortRequested();}

    PixLib::Config &config()    {return m_analysis->config();}

  private:
    IAnalysisPerModule *m_analysis;
  };


  /** A special implementation of an analysis per PP0 which uses an analysis which anlyses per Module.
   */
  class AnalysisPerROD : public IAnalysisPerROD
  {
  public:
    /** Take ownership of the given analysis with module granularity and pretend to have ROD granularity.
     */
    AnalysisPerROD(IAnalysisPerModule &analysis) :  m_analysis(new AnalysisPerPP0(analysis)) { }

    /** Take ownership of the given analysis with PP0 granularity and pretend to have ROD granularity.
     */
    AnalysisPerROD(IAnalysisPerPP0 &analysis) : m_analysis(&analysis) { }

    /** calls the destructor of the member analysis.
     */
    ~AnalysisPerROD() {delete m_analysis;}

    const std::string &name() const {return m_analysis->name();}


    /** Call foreach module of the given pp0 the prefetch method of the member analysis.
     */
    void requestHistograms(IAnalysisDataServer *data_server,
			   const PixA::RODLocationBase &rod_location);

    /** Call for each module of the given PP0 the analyse method of the member analysis.
     */
    void analyse(IAnalysisDataServer *data_server,
		 const PixA::RODLocationBase &rod_location,
		 ExtendedAnalysisResultList_t &analysis_results);

    virtual SerialNumber_t serialNumber() const { return m_analysis->serialNumber(); }

    void requestAbort()         { return m_analysis->requestAbort(); }

    void setLogLevel(ELogLevel log_level) {
      m_logLevel = log_level;
      m_analysis->setLogLevel(log_level);
    }

  protected:

    bool abortRequested() const { return m_analysis->abortRequested();}

    PixLib::Config &config()    {return m_analysis->config();}
    ELogLevel logLevel() const { return m_logLevel; }

  private:
    ELogLevel m_logLevel;
    IAnalysisPerPP0 *m_analysis;
  };

}
#endif
