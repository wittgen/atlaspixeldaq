#ifndef _CAN_ScanIsStatus_hh_
#define _CAN_ScanIsStatus_hh_

#include <cassert>
#include <cstdlib>
#include <cstring>

#include <string>

// for the serial number
#include <Common.hh>

namespace CAN {


  /** Translation between string and integer pix scan status values.
   */
  class ScanIsStatus {
  public:
    enum EStatus {kUnknown, kNone, kInitialising, kRunning, kSuccess, kFailed, kAborted };

    /** Find the status value to the give status name.
     * @param the value of the status as published in IS
     * @return the corresponding status flag.
     */
    static EStatus status(const std::string &name) { return status(name.c_str()); }

    /** Find the status value to the give status name.
     * @param the value of the status as published in IS
     * @return the corresponding status flag.
     */
    static EStatus status(const char *a_name) {
      for(unsigned int state_i=0; state_i<9; state_i++) {
	if (strcmp(a_name, s_allStatusName[state_i])==0) return s_statusMap[state_i];
      }
      return kUnknown;
    }

    /** Get the name assigned to the given scan status.
     * @param status the scan status.
     * @return the name associated to the given status.
     */
    static const char *name(EStatus status) { assert(status<= kAborted); return s_statusName[status];}

    /** Extract the ROD name from the scan IS status variable.
     * @param full_is_var_name the full statis name in the IS server.
     * @return the connectivity name of the corresponding ROD.
     */
    static std::string extractRodName(const std::string &full_is_var_name);


    /** Create a regular expression to match the per ROD status variable in IS for a certain serial number.
     */
    static std::string makeRegex(SerialNumber_t serial_number);


    /** Create a regular expression to match the per ROD status variable in IS for all serial numbers.
     */
    static std::string makeRegex();

    /** Create the IS variable name of the per ROD status.
     */
    static std::string makeIsVarName(const std::string &is_server_name, const std::string &rod_connectivity_name, SerialNumber_t serial_number);

    /** Return the name of the bare status variable.
     */
    static const std::string &varName() { return s_statusVarName; }

  private:
    static const char       *s_statusName[kAborted+1];
    static const char       *s_allStatusName[9];
    static const EStatus     s_statusMap[9];
    static const std::string s_emptyString;
    static const std::string s_statusVarName;
    static const char        s_measurementTypeId = 'S';
  };

}

#endif
