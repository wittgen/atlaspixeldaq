#include "HistoReceiver.hh"
#include <ipc/partition.h>
#include "PixScanHistoReceiver.hh"

#include <oh/OHIterator.h>
#include <is/serveriterator.h>

namespace CAN {


  HistoReceiver::HistoReceiver(IPCPartition &oh_partition,
			       const std::string &oh_server_base_name,
			       bool verbose)
    : m_partition( &oh_partition),
      m_ohServerBaseName(oh_server_base_name),
      m_verbose(verbose)
  {
  }

  HistoReceiver::~HistoReceiver() {}

  unsigned int HistoReceiver::getHistograms(SerialNumber_t scan_serial_number,
					    const std::string &rod_name,
					    const std::string &conn_name,
					    const std::string &histo_name,
					    TopLevelHistoArray &array)
  {
    std::stringstream pix_scan_histo_name;
    pix_scan_histo_name << "/S" << std::setw(9) << std::setfill('0') << scan_serial_number << '/';
    if (rod_name.size()>0) {
      pix_scan_histo_name << rod_name << "/";
    }
    if (conn_name.size()>0 && conn_name != rod_name ) {
      pix_scan_histo_name << conn_name << "/";
    }
    pix_scan_histo_name << histo_name << "/";

    try {

    std::map<std::string,std::string>::const_iterator histo_server_name_iter = m_rodToHistoServerName.find(rod_name);
    if (histo_server_name_iter != m_rodToHistoServerName.end()) {
      return getHistograms(pix_scan_histo_name.str(), array, histo_server_name_iter->second );
    }
    else {
      std::string is_server_pattern(m_ohServerBaseName);
      is_server_pattern += ".*";

      ISServerIterator isIter(*m_partition, is_server_pattern);
      while(isIter++) {
	std::string is_server_name ( isIter.name() );
//	if (is_server_name.compare(0, m_ohServerBaseName.size(), m_ohServerBaseName)==0) {
	  unsigned int count = getHistograms(pix_scan_histo_name.str(), array, is_server_name);
	  if (count>0) {
	    m_rodToHistoServerName.insert(std::make_pair(rod_name, is_server_name));
	    return count;
	  }
	}
 //     }
    }
    } catch ( daq::is::Exception & ex ) {
      if (m_verbose) {
	std::cout << "INFO [HistoReceiver::getHistograms] When trying to get histograms " << pix_scan_histo_name.str() 
		  << ", caught exception : " << ex.what() << std::endl;
      }
    }
    return 0;
  }

  unsigned int HistoReceiver::getHistograms(const std::string &pix_scan_histo_name,
					    TopLevelHistoArray &array,
					    const std::string &oh_server_base_name)
  {
    unsigned int counter=0;
    try {
      if (m_verbose) {
	std::cout << "INFO [HistoReceiver::getHistograms] Search for histograms " << pix_scan_histo_name << " in " << oh_server_base_name << "." << std::endl;
      }
    PixScanHistoReceiver receiver( array,  pix_scan_histo_name);

    OHHistogramIterator  histo_iter( *m_partition,
				     oh_server_base_name,
				     ".*" /* Care about the provider name ? */,
				     pix_scan_histo_name+".*");
    while ( histo_iter++ ) {
      histo_iter.retrieve( receiver );
      counter++;
    }
    } catch ( daq::is::Exception & ex ) {
      if (m_verbose) {
	std::cout << "INFO [HistoReceiver::getHistograms] When trying to get histograms " << pix_scan_histo_name 
		  << ", caught exception : " << ex.what() << std::endl;
      }
    }
    return counter;

  }

}
