#include "MetaDataService.hh"

#include "ScanInfo_t.hh"

#include <ipc/partition.h>
#include <ipc/core.h>
//#include <SerialNumberServer.hh>
//#include <SerialNumberServer_ref.hh>
#include <ConfigWrapper/DbManager.h>
#include <ConfigWrapper/DbRef.h>
#include "ScanConfigDb.hh"
#include "PixDbConfigDb.hh"
#include "ScanConfigDbInstance.hh"

#include "exception.hh"
#include "PixMetaException.hh"

#include <vector>

int main(int argc, char **argv)
{
  std::string partition_name;
  std::string tag_name;
  std::string cfg_tag_name;
  std::string mod_cfg_tag_name;
  unsigned int cfg_revision=0;
  std::string scan_type_name;
  std::string scan_type_tag = "TEST";
  unsigned int scan_type_revision=0;
  std::string id_tag_name = "CT";
  std::string scan_result_file;
  std::string temporary_scan_file;
  std::vector<std::string> folders;
  CAN::SerialNumber_t serial_number=0;
  bool error=false;

  for (int arg_i=1; arg_i<argc; arg_i++) {
    if (strcmp(argv[arg_i],"-p")==0 && arg_i+1<argc) {
      partition_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-i")==0 && arg_i+1<argc) {
      id_tag_name = argv[++arg_i];
    }
   else if (strcmp(argv[arg_i],"-t")==0 && arg_i+2<argc && argv[arg_i+2][0]!='-') {
      tag_name = argv[++arg_i];
      cfg_tag_name = argv[++arg_i];
      if (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	mod_cfg_tag_name = argv[++arg_i];
	if (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	  cfg_revision = static_cast<unsigned int>(atoi(argv[++arg_i]));
	}
      }

    }
    else if (strcmp(argv[arg_i],"-n")==0 && arg_i+1<argc) {
      scan_type_name=argv[++arg_i];
      if (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	scan_type_tag = argv[++arg_i];
	if (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	  scan_type_revision = static_cast<unsigned int>(atoi(argv[++arg_i]));
	}
      }
    }
    else if (strcmp(argv[arg_i],"-s")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      serial_number = atoi(argv[++arg_i]);
    }
    else if (strcmp(argv[arg_i],"-r")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      scan_result_file = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-f")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      temporary_scan_file = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-d")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      while (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	folders.push_back( argv[++arg_i] );
      }
    }
    else {
      std::cout << "ERROR ["<< argv[0] << ":main] unhandled argument " << arg_i << " : " << argv[arg_i]<< "." << std::endl;
      error=true;
      break;
    }
  }
  std::cout << " tag = " << id_tag_name << " : " << tag_name << " cfg = " << cfg_tag_name << " / " << mod_cfg_tag_name << ";" << cfg_revision << std::endl;
  std::cout << " scan = " << scan_type_name << " cfg = " << scan_type_tag << ";" << scan_type_revision << std::endl;
  std::cout << " result = " << scan_result_file << std::endl;
  if (tag_name.empty() || cfg_tag_name.empty() || scan_type_name.empty() || scan_type_tag.empty() || scan_result_file.empty() || error) {
      std::cout << "usage: " << argv[0] << " -t tag cfg-tag [mod-cfg [cfg-rev] ] -n scan-type [scan-tag [scan-rev] ] [-s enforced-serial-number] [-f temp-scan-param-file] -r scan-result-file" << std::endl;
      return -1;
  }

  CAN::InfoPtr_t<PixLib::ScanInfo_t> scan_info = new PixLib::ScanInfo_t;

  //  if (!scan_type_name.empty())
  scan_info->setScanType(scan_type_name);
  scan_info->setScanTypeTag(scan_type_tag);
  scan_info->setScanTypeRev(scan_type_revision);

  scan_info->setIdTag(id_tag_name);
  scan_info->setConnTag(tag_name);
  scan_info->setDataTag(tag_name);
  scan_info->setAliasTag(tag_name);
  scan_info->setCfgTag(cfg_tag_name);
  if (!mod_cfg_tag_name.empty()) {
    scan_info->setModCfgTag(mod_cfg_tag_name);
  }
  scan_info->setCfgRev(cfg_revision);
  scan_info->setScanStart(time(0));
  scan_info->setQuality(CAN::kDebug);
  scan_info->setFile(scan_result_file);
  scan_info->setStatus(CAN::kUnknown);

  // START IPC

  // Start IPCCore
  IPCCore::init(argc, argv);


  try {
    // Get serial number from CORAL MetaDataService if it exists
    if (serial_number == 0) {
       serial_number = CAN::MetaDataService::instance()->newSerialNumber(CAN::kScanNumber);
    }
//     if (serial_number == 0) {
//       // Create IPCPartition
//       std::cout << "INFO [" << argv[0] << ":main] Get serial number from partition " << partition_name << std::endl;
//       IPCPartition *ipcPartition = new IPCPartition(partition_name.c_str());
//       //      SENSE::SerialNumberServer_ref serial_number_ref(*ipcPartition);
//       //      serial_number = serial_number_ref.getNewSerialNumber(SENSE::kScan);
//     }
    CAN::MetaDataService::instance()->addInfo(serial_number,scan_info);
    std::cout << "INFO [" << argv[0] << ":main] Added scan meta data :: " << serial_number << " : " 
	      << scan_info->scanType() << ", "
	      << scan_info->connTag() << ", "
	      << scan_info->cfgTag() << " / "
	      << scan_info->cfgRev()
	      << std::endl;

//     const char *scan_config_db = getenv("PIXSCAN_CFG_PATH");
//     if (!scan_config_db) {
//       std::cerr << "ERROR [" << argv[0] << ":main] Define PIXSCAN_CFG_PATH. It should point to the location of the scan configuration db." << std::endl;
//       return -1;
//     }

    if (!temporary_scan_file.empty()) {

    PixA::DbHandle a_db = PixA::DbManager::dbHandle(temporary_scan_file);
    if (!a_db) {
      std::cerr << "ERROR [" << argv[0] << ":main] Failed to open temporary scan file " << temporary_scan_file<< std::endl;
      return -1;
    }

    PixA::ConfigHandle scan_config_handle( PixA::DbManager::configHandle(a_db) );
    //      PixA::ConfigRef full_scan_config_ref(scan_config_handle.ref().createFullCopy());


    if (!folders.empty()) {
      std::vector<std::pair<std::string, PixA::ConfigHandle> > config_list;
      for (std::vector<std::string>::const_iterator folder_iter = folders.begin();
	   folder_iter != folders.end();
	   folder_iter++) {
	config_list.push_back(std::make_pair(*folder_iter, scan_config_handle));
      }
      std::auto_ptr<CAN::IConfigDb> db( CAN::ScanConfigDbInstance::create());
      db->addConfigList(CAN::ScanConfigDbInstance::scanClassName(), scan_info->scanType(), scan_info->scanTypeTag(), config_list);
      //      std::cout << "ERROR ["<< argv[0] << ":main] scans like the CT with multiple scan configurations per serial number are currently not supported." << std::endl;
      //      return -1;
    }
    else {
      std::auto_ptr<CAN::IConfigDb> db( CAN::ScanConfigDbInstance::create());
      db->addConfig(CAN::ScanConfigDbInstance::scanClassName(), scan_info->scanType(), scan_info->scanTypeTag(), scan_config_handle);

      //	CAN::ScanConfigDb::instance()->writeConfig( serial_number, full_scan_config_ref );
    }
    }

  }
  catch (CAN::PixMetaException &err) {
    std::cerr << "ERROR [" << argv[0] << ":main] Caught PixMetaException " << err.what() << std::endl;
  }
  catch (CAN::MRSException &err) {
    std::cerr << "ERROR [" << argv[0] << ":main] Caught exception " << err.getDescriptor() << std::endl;
  }
  catch (std::exception &err) {
    std::cerr << "ERROR [" << argv[0] << ":main] Caught standard exception " << err.what() << std::endl;
  }

  return 0;
}
