#include <ConfigWrapper/DbManager.h>
#include <ConfigWrapper/DbRef.h>
#include <ConfigWrapper/ConfObjUtil.h>
#include <ConfigWrapper/PixConfigWrapper.h>

#include <qapplication.h>
#include "OptionsPanel.h"

#include <sstream>
#include <memory>

#include "AnalysisFactory.hh"
#include "AnalysisInfo_t.hh"
#include "IAnalysisObject.hh"
#include "AnalysisPerX.hh"
#include "IClassification.hh"
#include "IPostProcessing.hh"

#include "IConfigDb.hh"
#include "DummyConfigDb.hh"
#include "PixDbConfigDb.hh"
#include "PixDbObjectConfigDb.hh"
#include "ObjectConfigDbInstance.hh"

#include "exception.hh"
#include <iostream>

#include <time.h>
#include <unistd.h>

// for mkdir:
#include <sys/stat.h>
#include <sys/types.h>
#include <cerrno>

#include <memory>
#include <vector>

std::map<std::string, bool> s_boolNames;

bool setValue(PixLib::Config &a_config, const std::string &field_name, const std::string &value)
{
  if (s_boolNames.empty()) {
    s_boolNames["true"]=true;
    s_boolNames["TRUE"]=true;
    s_boolNames["false"]=false;
    s_boolNames["FALSE"]=false;
    s_boolNames["0"]=false;
    s_boolNames["1"]=true;
    s_boolNames["f"]=false;
    s_boolNames["t"]=true;
    s_boolNames["off"]=false;
    s_boolNames["on"]=true;
    s_boolNames["disabled"]=false;
    s_boolNames["enabled"]=true;
  }
  std::string::size_type pos = field_name.find("_");
  if (pos != std::string::npos) {
    PixLib::ConfGroup &group=a_config[field_name.substr(0,pos)];
    PixLib::ConfObj &obj=group[field_name.substr(pos+1,field_name.size()-pos-1)];
    if (obj.type()!=PixLib::ConfObj::VOID) {
      try {
	switch ( obj.type() ) {
	case PixLib::ConfObj::STRING: {
	  confVal<std::string>(obj)=value;
	  break;
	}
	case PixLib::ConfObj::FLOAT: {
	  char *end_ptr = NULL;
	  double float_value =  strtod(value.c_str(), &end_ptr);
	  if (!end_ptr || *end_ptr) {
	    std::cerr << "ERROR [setValue] Value " << value << " given for field config[" << field_name.substr(0,pos) << "]["
		      << field_name.substr(pos+1,field_name.size()-pos-1) << "] is not a valid float."
		      << std::endl;
	    return false;
	  }
	  confVal<float>(obj)=static_cast<float>(float_value);
	  break;
	}
	case PixLib::ConfObj::INT:
	case PixLib::ConfObj::LIST: {
	  char *end_ptr;
	  long long_value =  strtol(value.c_str(), &end_ptr, 10);
	  if (!end_ptr || *end_ptr) {
	    std::cerr << "ERROR [setValue] Value " << value << " given for field config[" << field_name.substr(0,pos) << "]["
		      << field_name.substr(pos+1,field_name.size()-pos-1) << "] is not a valid integer."
		      << std::endl;
	    return false;
	  }
	  PixLib::ConfInt &conf_int = static_cast<PixLib::ConfInt&>(obj);
	  switch (conf_int.subtype()) {
	  case PixLib::ConfInt::S32:
	    confVal<signed int>(obj)=static_cast<signed int>(long_value);
	    break;
	  case PixLib::ConfInt::U32:
	    confVal<unsigned int>(obj)=static_cast<unsigned int>(long_value);
	    break;
	  case PixLib::ConfInt::S16:
	    confVal<signed short>(obj)=static_cast<signed short>(long_value);
	    break;
	  case PixLib::ConfInt::U16:
	    confVal<unsigned short>(obj)=static_cast<unsigned short>(long_value);
	    break;
	  case PixLib::ConfInt::S8:
	    confVal<char>(obj)=static_cast<char>(long_value);
	    break;
	  case PixLib::ConfInt::U8:
	    confVal<unsigned char>(obj)=static_cast<unsigned char>(long_value);
	  }
	  break;
	}
	case PixLib::ConfObj::BOOL: {
	  std::map<std::string,bool>::const_iterator bool_name_iter = s_boolNames.find(value);
	  if (bool_name_iter != s_boolNames.end()) {
	    confVal<bool>(obj)=bool_name_iter->second;
	  }
	  else {
	    std::cerr << "ERROR [setValue] Value " << value << " given for field config[" << field_name.substr(0,pos) << "]["
		      << field_name.substr(pos+1,field_name.size()-pos-1) << "] is not a valid boolean (true, false, t, f, 0, 1, on, off)."
		      << std::endl;
	    return false;
	  }
	  break;
	}
	default: {
	  std::cerr << "ERROR [setValue] Type " << obj.type() << " of field config[" << field_name.substr(0,pos) << "]["
		    << field_name.substr(pos+1,field_name.size()-pos-1) << "] is not supported. Apologies."
		    << std::endl;
	  return false;
	  break;
	}
	}
	return true;
      }
      catch(...) {
	std::cerr << "ERROR [setValue] Failed to set  : config[" << field_name.substr(0,pos) << "]["
		  << field_name.substr(pos+1,field_name.size()-pos-1) << "] to " << value << "."
		  << std::endl;
	return false;
      }
    }
    else {
      std::cerr << "ERROR [setValue] Field_name : config[" << field_name.substr(0,pos) << "]["
		<< field_name.substr(pos+1,field_name.size()-pos-1) << "] does not exist."
		<< std::endl;
      return false;
    }
  }
  else {
    std::cerr << "ERROR [setValue] Not a valid field_name : " << field_name << ". Expect something like groupName_fieldName." << std::endl;
    return false;
  }
}

int main( int argc, char ** argv )
{

  std::string conf_db_path;
  char * ctmp = getenv("CAN_CFG_PATH");
  ctmp == NULL ? conf_db_path = "" : conf_db_path = ctmp;

  std::string name[1];
  CAN::Revision_t rev[1]={0};
  std::string tag[1];

  std::vector< std::pair<std::string, std::string> > set_values;

  bool read_only=false;
  bool no_gui=false;
  bool update=false;

  for (int arg_i=1; arg_i<argc; arg_i++) {
    if (strcmp(argv[arg_i],"-d")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      conf_db_path=argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-r")==0) {
      read_only=true;
    }
    else if (strcmp(argv[arg_i],"-u")==0) {
      update=true;
    }
    else if (strcmp(argv[arg_i],"--no-gui")==0) {
      no_gui=true;
    }
    else if (strcmp(argv[arg_i],"-a")==0 && arg_i+3<argc && argv[arg_i+1][0]!='-') {
      name[0] = argv[++arg_i];
      tag[0] = argv[++arg_i];
      if (argv[arg_i+1][0]!='-') {
	++arg_i;
	rev[0] = atoi(argv[arg_i]);
      }
    }
    else if (strcmp(argv[arg_i],"-s")==0 && arg_i+2<argc && argv[arg_i+1][0]!='-') {
      std::string field = argv[++arg_i];
      std::string value = argv[++arg_i];
      set_values.push_back(std::make_pair(field,value));
    }
    else {
      std::cerr << "ERROR ["<< argv[0] << ":main] unhandled argument " << arg_i << " : " << argv[arg_i]<< "." << std::endl;
    }
  }

  if (rev[0]==0) rev[0] = time(NULL);

  if (conf_db_path.empty() || name[0].empty() || tag[0].empty()) {
    std::cerr << "ERROR  ["<< argv[0] << ":main] usage :" << std::endl
	      << argv[0] << " -d conf-db-path -a name tag revision [-r]" << std::endl
	      << "-d conf-db-path       the path were to store the RootDb configuration files." << std::endl
	      << "-a name tag revision  the analysis name, the tag and the revision of the configuration." << std::endl
	      << "--no-gui              do not show the panel, just write." << std::endl
              << "-r                    only read a configuration."
              << "-u                    update an exisitng config."
	      << "-s field value        set field to given value." << std::endl;
    return -1;
  }
  if (conf_db_path.empty() || conf_db_path[conf_db_path.size()-1]!='/') {
    conf_db_path+='/';
  }

  // first create the analysis object
  for(int i=0; i<2; i++){
    std::string tag_name = tag[0];
    if(i==1) tag_name.insert(tag_name.size(), "_I4");

    //    CAN::ObjectInfo_t obj_info(name[0],tag[0],rev[0]);
    CAN::ObjectInfo_t obj_info(name[0], tag_name, rev[0]);

    std::cout << "INFO [" << argv[0] << ":main] create analysis object : " 
	      << obj_info.name() << "/"
	      << obj_info.tag() << "/"
	      << obj_info.revision()
	      << std::endl;
    
    try {
      std::auto_ptr<CAN::IObjectConfigDb> db( (read_only || update ?
					       static_cast<CAN::IObjectConfigDb *>(new CAN::PixDbObjectConfigDb)
					       : static_cast<CAN::IObjectConfigDb *>(new CAN::DummyConfigDb  )));
      std::auto_ptr<CAN::IAnalysisObject> obj(CAN::AnalysisFactory::get()->create(0,obj_info, db.get()));
      if (!obj.get()) {
	std::cerr << "ERROR [" << argv[0] << ":main] Failed to create object."
		  << std::endl;
	return -1;
      }
      
      if (read_only) {
	obj->config().dump(std::cout);
	return 0;
      }
      
      // change a value;
      
      // set config from command line values
      bool success=true;
      for (std::vector<std::pair<std::string, std::string> >::const_iterator set_value_iter = set_values.begin();
	   set_value_iter != set_values.end();
	   set_value_iter++) {
	success &= setValue(obj->config(), set_value_iter->first,set_value_iter->second);
      }
      if (!success) {
	return -1;
      }
      
      bool save_changes = no_gui;
      if (!no_gui) {
	QApplication app( argc, argv );
	app.connect( &app, SIGNAL( lastWindowClosed() ), &app, SLOT( quit() ) );
	optionsPanel config_panel(obj->config(), NULL,true);
	// now show panel
	config_panel.setWindowTitle("Analysis Configuration Editor");
	config_panel.exec();
	if (config_panel.saveChangesChoosen()) {
	  save_changes = true;
	}
      }
      
      if (save_changes) {
	std::auto_ptr<CAN::IConfigDb> db( new CAN::PixDbConfigDb(conf_db_path));
	std::string class_name = "IAnalysisObject";
	if (   dynamic_cast<CAN::IAnalysisPerROD *>(obj.get())
	       || dynamic_cast<CAN::IAnalysisPerPP0 *>(obj.get())
	       || dynamic_cast<CAN::IAnalysisPerModule *>(obj.get()) ) {
	  class_name = CAN::ObjectConfigDbInstance::objectClassName(CAN::ObjectConfigDbInstance::kAnalysis);
	}
	else if (dynamic_cast<CAN::IClassification *>(obj.get())) {
	  class_name = CAN::ObjectConfigDbInstance::objectClassName(CAN::ObjectConfigDbInstance::kClassification);
	}
	else if (dynamic_cast<CAN::IPostProcessing *>(obj.get())) {
	  class_name = CAN::ObjectConfigDbInstance::objectClassName(CAN::ObjectConfigDbInstance::kPostProcessing);
	}
	else {
	  std::cout << "ERROR object not of type : IAnalysisPerROD, IClassification, IPostProcessing." << std::endl;
	}

	db->addConfig(class_name, obj_info.name(), obj_info.tag(), obj_info.revision(), PixA::PixConfigWrapper::wrap(obj->config()) );

	//std::cout << " --- created configuration for : " << obj_info.name() << " / " << obj_info.tag() << " / " << time(NULL) << std::endl;
	std::cout << " --- created configuration for : " << obj_info.name() << " / " << obj_info.tag() << " / " << obj_info.revision() << std::endl;
	obj->config().dump(std::cout);
	std::cout << "INFO [main:" << argv[0] << "] No error detected." << std::endl;
      }
      else {
	std::cout << "INFO [main:" << argv[0] << "] Cancelled changes." << std::endl;
      }
      
    }
    catch( CAN::MRSException &err) {
      std::cerr << "ERROR [" << argv[0] << ":main] Caught exception : "
		<< err.getDescriptor()
		<< std::endl;
      return -1;
    }
    catch( std::exception &err) {
      std::cerr << "ERROR [" << argv[0] << ":main] Caught exception : "
		<< err.what()
		<< std::endl;
      return -1;
    }
    catch( ... ) {
      std::cerr << "ERROR [" << argv[0] << ":main] Unknown exception . "
		<< std::endl;
      return -1;
    }
  }//loop for Base, Base_I4

  return 0;
}
