#ifndef OPTIONSPANEL_H
#define OPTIONSPANEL_H

#include <qdialog.h>
#include <vector>
#include <string>

class QTabWidget;
class QWidget;
class QPushButton;

namespace PixLib{
 class Config;
 class ConfGroup;
}
class optionsFrame;
class optionsPanel : public QDialog{

  Q_OBJECT

 public:
  optionsPanel( PixLib::Config &in_cfg, QWidget* parent = 0, bool subGrps = false);
  ~optionsPanel();

   QTabWidget *getTabWidget() ;//{return m_optionsFrame->getTabWidget();}
//  void loadSubConf(PixLib::Config &inConf);

   bool saveChangesChoosen() const;// { return m_save;}


  optionsFrame *m_optionsFrame;
  
  PixLib::Config &m_config;
  QPushButton *saveB, *cancB;


 public slots:
  void save();
  void cancel();//{close();};

 private:
  bool m_save;

};

#endif //  OPTIONSPANEL_H
