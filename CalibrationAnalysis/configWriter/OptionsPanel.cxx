#include "OptionsPanel.h"

#include <Config/Config.h>
#include <Config/ConfGroup.h>
#include <Config/ConfObj.h>

#include <QPushButton>
#include <QLabel>
#include <QTabWidget>
#include <QFrame>
#include <QVBoxLayout>
#include <QHBoxLayout>

#include "QTEditors/OptionsFrame.h"

#include <stdio.h>

using namespace PixLib;

optionsPanel::optionsPanel( PixLib::Config &in_cfg, QWidget* parent, bool subGrps)
  : QDialog (parent),
    m_config(in_cfg),
    m_save(false) 
{

    setWindowTitle("Config Editor");

    QVBoxLayout *BaseLayout = new QVBoxLayout( this );
    QHBoxLayout *layout1 = new QHBoxLayout( 0 );
    QVBoxLayout *layout2 = new QVBoxLayout( 0 );

    m_optionsFrame = new optionsFrame(in_cfg,this,subGrps);
    layout2->addWidget(m_optionsFrame);

    saveB = new QPushButton( this );
    layout1->addWidget(saveB);
    saveB->setText("Save");

    QSpacerItem *spacer1 = new QSpacerItem( 41, 21, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout1->addItem( spacer1 );

    cancB = new QPushButton( this );
    layout1->addWidget(cancB);
    cancB->setText("Cancel");

    QSpacerItem *spacer2 = new QSpacerItem( 21, 41, QSizePolicy::Minimum, QSizePolicy::Expanding );
    layout1->addItem( spacer2 );

    layout2->addLayout( layout1 );
    BaseLayout->addLayout( layout2 );

    resize( QSize(450, 250).expandedTo(minimumSizeHint()) );

    connect( saveB, SIGNAL( clicked() ), this, SLOT( save() ) );
    connect( cancB, SIGNAL( clicked() ), this, SLOT( cancel() ) );
}
optionsPanel::~optionsPanel(){
}

//void optionsPanel::loadSubConf(PixLib::Config &inConf)
//{
//}

QTabWidget* optionsPanel::getTabWidget() {return m_optionsFrame->getTabWidget();}


 bool optionsPanel::saveChangesChoosen() const { return m_save;}

void optionsPanel::save(){
 // m_optionsFrame->save();
  // close panel
  m_save=true;
  close();}

  void optionsPanel::cancel() {close();}



