#include "ObjectConfigDbInstance.hh"
#include "PixDbConfigDb.hh"

namespace CAN {

  const char *ObjectConfigDbInstance::s_className[3]={"AnalysisPerX","IClassification","IPostProcessing"};

  IConfigDb *ObjectConfigDbInstance::create() {
    char * config_db = getenv("CAN_CFG_PATH");
    if (!config_db || strlen(config_db)==0) {
      throw MissingConfig( "Environment variable CAN_CFG_PATH is undefined. It defines the location of the analysis configuration db, ");
    }
    return new PixDbConfigDb( config_db);
  }

}
