#ifndef _CAN_IClassification_hh_
#define _CAN_IClassification_hh_

#include "IAnalysisObject.hh"
#include <ConfigWrapper/Locations.h>
#include "Common.hh"

namespace CAN {

  class ExtendedAnalysisResultList_t;
  class IAnalysisDataServer;

  /** The interface of analysis which analyse data per module.
   * These analyses can be layered by an analyses which analyse per PP0
   */
  class IClassification : public IAnalysisObject
  {
  public:

    /** Classify the analysis according to the result.
     * @param rod_location the name of the rod.
     * @param analysis_result the analysis results which will be extended by the classification.
     * @return return if the analysis is classified as successful.
     */
    virtual bool classify(IAnalysisDataServer *data_server,
			  const PixA::RODLocationBase &rod_location,
			  ExtendedAnalysisResultList_t &analysis_results) = 0;
  };

}

#endif
