#!/bin/sh

rat_exe=${CAN}/rat
mrs_exe=${CAN}/sendMrsMessage
# wait  sec before respawning the rat.
wait=10

#
# arguments for the worker
#
args=$*

#
# extract mrs partition argument
#
mrs_partition=""
ipc_partition=""
n_part=0
while test -n "$1"; do
    arg=$1
    case $arg in 
    (-p)
	shift
	n_part=$(($n_part + 1))
	ipc_partition=$1
	;;
    (-m)
	shift
	mrs_partition=$1
	if test -n "$ipc_partition";then
	   n_part=$(($n_part + 1))
        fi
        ;;
    (-*)
       shift
       ;;
    (*)
	if test $n_part = 1; then
	    mrs_partition="$arg"
	elif test $n_part = 0; then
	    ipc_partition="$arg"
	fi
	n_part=$(($n_part + 1))
        ;;
    esac
    shift
done

if test -z "$mrs_partition"; then
   mrs_partition=$ipc_partition
fi

if test -n "$mrs_partition"; then
    if test -x $mrs_exe; then
	echo "INFO MRS messages  will go to partition $mrs_partition."
    else
	mrs_partition=""
    fi
fi

if test -n "$mrs_partition"; then
     $mrs_exe -m $mrs_partition -i "RESPAWN" -s "INFORMATION" -f "respawn_rat" \
     -t "Worker will be started by respawn"
fi

if test -x $rat_exe; then
  :
else
  echo "ERROR: no rat at ${CAN}/rat. Environment variables correctly set ? " >&2
  exit 1
fi

while (true); do
  echo $rat_exe `echo $args`
  $rat_exe `echo $args`
  ret=$?
  echo "INFO rat returned $ret" >&2
  case $ret in
  (13|134)
     if test -n "$mrs_partition"; then
       $mrs_exe -m $mrs_partition -i "RESPAWN" -s "FATAL" -f "respawn_rat" \
       -t "The worker seems to have crashed. Will respawn the worker in $wait sec."
     fi
     if test x$ret = x134; then
        echo "INFO [$0] The worker stopped with a segmentation fault."
     fi
     echo "INFO [$0] Will respawn rat with arguments $args in $wait sec.";
     ;;
  (0)
     echo "INFO [$0] The worker exited with exit status 0. So, we are done. Bye-bye!";
     exit 0
     ;;
  (*)
     echo "INFO [$0] Unexpected return state. Will respawn nevertheless in $wait sec.";
     ;;
  esac
  sleep $wait
done
