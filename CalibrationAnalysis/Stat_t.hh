#ifndef _CAN_Stat_t_hh_
#define _CAN_Stat_t_hh_

#include <map>
#include <iostream>
#include <string>

namespace CAN {
  class Stat_t
  {
  public:
    Stat_t(const std::string &name) :m_name(name) { s_counter[name]++; s_ctor[name]++; stat(); }
    ~Stat_t() { s_counter[m_name]--; s_dtor[m_name]++; stat(); }

    void stat() {
      std::cout << "INFO [Stat_t::stat] " << m_name << " counter :" << s_counter[m_name] << " = " << s_ctor[m_name] << " - " << s_dtor[m_name] << "." <<std::endl;
    }

  private:
    std::string m_name;

    static std::map<std::string,unsigned int> s_counter;
    static std::map<std::string,unsigned int> s_ctor;
    static std::map<std::string,unsigned int> s_dtor;
  };
}
#endif
