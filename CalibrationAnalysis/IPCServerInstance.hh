#ifndef _CAN_IPCServerInstance_hh_
#define _CAN_IPCServerInstance_hh_

//#include <ipc/server.h>
#include <owl/semaphore.h>
#include <pmg/pmg_initSync.h>
#include <cstdlib>

// JGK: converted from IPCServer to OWLsemaphore since the former was removed
//      from TDAQ as of rel. 05-03-00

namespace CAN {

  class IPCServerInstance
  {
  private:
    IPCServerInstance();
  public:

    //    static IPCServer *instance() 
    static OWLSemaphore *instance()
    {
      if (!s_instance) {
	s_instance=new OWLSemaphore;
      }
      return s_instance;
    }

    static void run() {
      //IPCServer *an_instance = instance();
      OWLSemaphore* an_instance = instance();

      // to avoid the warning at startup if manitu is started 
      // manually
      {
	const char *pmg_sync_file = getenv("PMG_SYNC_FILE");
	if (pmg_sync_file) {
	  pmg_initSync();
	}
      }
      //an_instance->run();
      an_instance->wait();
    }

    static void stop() {
      //      IPCServer *an_instance = instance();
      //      an_instance->stop();
      OWLSemaphore *an_instance = instance();
      an_instance->post();
    }

  private:
    //    static IPCServer *s_instance;
    static OWLSemaphore *s_instance;
  };

}
#endif
