#include <memory>

#include "AnalysisDataServer.hh"
#include "DataServer.hh" //for delete


namespace CAN {

  AnalysisDataServer::AnalysisDataServer(PixA::ConnectivityRef &conn)
    : m_childsWithData(0),
      m_conn(conn),
      currentDataServer(m_DataServers.end())     
  {
    assert(m_conn);
  }

  AnalysisDataServer::~AnalysisDataServer() 
  {
    for(std::map<std::string,IDataServer*>::const_iterator iter = m_DataServers.begin(); iter!=m_DataServers.end(); ++iter)
      delete iter->second;
  }

  void AnalysisDataServer::addDataServer(const std::string &scanName, IDataServer* dataServer)
  {
    assert( dataServer);
    std::cout << "INFO [AnalysisDataServer::addDataServer] add \"" << scanName << "\"."<< std::endl;
    m_DataServers[scanName] = dataServer;

    if(m_DataServers.empty()) {
      std::cout << "ERROR  AnalysisDataServer::addDataServer    No DataServer in the list" << std::endl;
    }
  }

  IDataServer* AnalysisDataServer::getDataServer(const std::string &scanName) 
  {
    std::map<std::string, IDataServer *>::iterator data_server_iter = m_DataServers.find(scanName);
    if(data_server_iter ==m_DataServers.end()) {
      std::stringstream message;
      message << "No data server for scan named \"" << scanName << "\". Has : ";
      for (std::map<std::string, IDataServer *>::const_iterator iter = m_DataServers.begin();
	   iter != m_DataServers.end();
	   ++iter) {
	message << iter->first << " ";
      }
      //      throw MRSException("DATASRV_no_server", MRS_FATAL, "CAN::AnalysisDataServer::getDataServer",message.str());
      throw MRSException(PixLib::PixMessages::FATAL, "CAN::AnalysisDataServer::getDataServer",message.str());
    }
    return data_server_iter->second;
  }

  IDataServer* AnalysisDataServer::getDataServer() 
  {
    if(m_DataServers.empty()) {
      std::stringstream message;
      message << "Data server list is empty. Internal error.";
      //      throw MRSException("DATASRV_no_server", MRS_FATAL, "CAN::AnalysisDataServer::getDataServer",message.str());
      throw MRSException(PixLib::PixMessages::FATAL, "CAN::AnalysisDataServer::getDataServer",message.str());
    }
    return m_DataServers.begin()->second;
  }

  /** Return the connectivity.
   * The connecitivity returned is garanteed to be okay, since it already has been used to 
   * to setup the joblets.
   */
  const PixA::ConnectivityRef& AnalysisDataServer::connectivity() {
    return m_conn;
  }

  IDataServer* AnalysisDataServer::NextDataServer() {
    if(currentDataServer==m_DataServers.end()) 
      currentDataServer=m_DataServers.begin();
    else
      ++currentDataServer;
    if(currentDataServer==m_DataServers.end())
      return NULL;
    return currentDataServer->second;
  }


  bool AnalysisDataServer::enabled(PixA::Pp0List::const_iterator pp0_iter)
  {
    for(std::map<std::string,IDataServer*>::const_iterator iter = m_DataServers.begin(); iter!=m_DataServers.end(); ++iter) {
      if(!PixA::enabled( iter->second->getPp0DisabledList(), pp0_iter ))
	return false;
    }
    return true;
  }


  void AnalysisDataServer::createCombinedModuleDisabledList(const PixA::RODLocationBase &rod_location)
  {
    std::cout << "INFO [AnalysisDataServer::getCombinedModuleDisabledList] " << rod_location << std::endl;//temp
    std::shared_ptr<PixLib::PixDisable> rod_pix_disable(new PixLib::PixDisable(rod_location));

    const PixA::Pp0List &pp0_list = m_conn.pp0s(rod_location);
    PixA::Pp0List::const_iterator pp0_iter=pp0_list.begin();
    PixA::Pp0List::const_iterator pp0_last=pp0_list.end();

    // Loop over all pp0s of this rod
    for (; pp0_iter != pp0_last; ++pp0_iter) {

      //if(m_DataServers.size()==1)
      //This currently breaks when >1 data server exists
      if (true)
	m_moduleDisabledLists[location(pp0_iter)] = PixA::moduleDisabledList( m_DataServers.begin()->second->getPp0DisabledList(), pp0_iter);
      else {
	const PixA::ModuleList &module_list = m_conn.modules(pp0_iter);
	PixA::ModuleList::const_iterator module_iter=module_list.begin();
	PixA::ModuleList::const_iterator module_last=module_list.end();

	for(std::map<std::string,IDataServer*>::const_iterator iter = m_DataServers.begin(); iter!=m_DataServers.end(); ++iter) {
	  const PixA::DisabledList module_disabled_list( PixA::moduleDisabledList( iter->second->getPp0DisabledList(), pp0_iter) );//for given scan and pp0

	  //	  std::cout << "INFO [AnalysisDataServer::getCombinedModuleDisabledList] disable list size: " << module_disabled_list.size() << std::endl;//temp

	  /// LOOP over modules 
	  for(; module_iter != module_last; ++module_iter) {

	    if(!PixA::enabled( module_disabled_list, module_iter)) {
	      //std::cout << "AnalysisDataServer::getCombinedModuleDisabledList     found disabled module." << std::endl; //temp
	      rod_pix_disable->disable(PixA::connectivityName(module_iter) );
	    }
	  }
	}
	m_moduleDisabledLists[location(pp0_iter)] = PixA::moduleDisabledList( PixA::pp0DisabledList( PixA::rodDisabledList( PixA::DisabledListRoot( rod_pix_disable ), 
															    PixA::crate( PixA::rodBoc(*pp0_iter)) ),
												     PixA::rodBoc(*pp0_iter)),
									      pp0_iter);
      }
    }//pp0 loop
  }

}

