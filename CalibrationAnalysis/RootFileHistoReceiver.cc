#include <memory>

#include "RootFileHistoReceiver.hh"
#include "PixDbGlobalMutex.hh"
#include <ScanInfo_t.hh>
#include "exception.hh"
#include "sendMessage.hh"
#include <MetaDataService.hh>
//#include <IMetaDataService.hh>
#include <TFile.h>
#include <TROOT.h>
#include <TKey.h>
#include <TH1.h>

#include <sstream>
#include <iomanip>

#include "HistoArray.hh"


namespace CAN {

  class UnLock {
  public:
    UnLock(Mutex &a_mutex) : m_mutex(&a_mutex) {m_mutex->unlock();}

    ~UnLock()  {m_mutex->lock();}

  private:
    Mutex *m_mutex;
  };

  RootFileHistoReceiver::FileData_t::~FileData_t() {
    if (m_file.use_count()<=1) {
      std::cout << "INFO [RootFileHistoReceiver::FileData_t::dtor] destruct " << (m_file.get() ? m_file->GetName() : "") << "." << std::endl;
    }
  }

  RootFileHistoReceiver::RootFileHistoReceiver(bool verbose) : m_verbose(verbose) {}
  RootFileHistoReceiver::~RootFileHistoReceiver() {}

  class RootDirRestore
  {
  public:
    RootDirRestore() : m_file(gFile), m_directory(gDirectory) {}
    ~RootDirRestore() { gFile=m_file; gDirectory = m_directory; }

  private:
    TFile      *m_file;
    TDirectory *m_directory;
  };

  bool RootFileHistoReceiver::s_initialised=RootFileHistoReceiver::initROOT();

  bool RootFileHistoReceiver::initROOT() {
    TH1::AddDirectory(kFALSE);
    TObject::SetObjectStat(kFALSE);
    return true;
  }

  unsigned int RootFileHistoReceiver::getHistograms(SerialNumber_t scan_serial_number,
						    const std::string &rod_name,
						    const std::string &conn_name,
						    const std::string &histo_name,
						    TopLevelHistoArray &array) {

    if (histo_name.empty()) {
      std::string message(" Missing histogram name in analysis of scan " );
      message += makeScanSerialNumberString(scan_serial_number);
      message += ".";
      //      throw MRSException("NOSCANRESULTFILE", MRS_FATAL, "CAN::RootFileHistoReceiver::getHistograms",message);
      throw MRSException(PixLib::PixMessages::FATAL, "CAN::RootFileHistoReceiver::getHistograms",message);
    }

    unsigned int n_histograms = 0;

    Lock lock( PixDbGlobalMutex::mutex() );
    RootDirRestore restore;
    std::map< SerialNumber_t, FileData_t >::iterator file_iter = m_cache.find(scan_serial_number);
    if (file_iter == m_cache.end()) {
      if  (m_cache.size()>s_maxCacheSize) {
	while (m_cache.size()>s_maxCacheSize) {
	  std::map< SerialNumber_t, FileData_t >::iterator delete_file_iter = m_cache.begin();
	  for (std::map< SerialNumber_t, FileData_t >::iterator cache_iter = m_cache.begin();
	       ++cache_iter != m_cache.end();
	       ) {
	    if (cache_iter->second.used() < delete_file_iter->second.used()) {
	      delete_file_iter = cache_iter;
	    }
	  }
	  m_cache.erase(delete_file_iter);
	}
	for (std::map< SerialNumber_t, FileData_t >::iterator cache_iter = m_cache.begin();
	     ++cache_iter != m_cache.end();
	     ) {
	  cache_iter->second.resetUsed();
	}
      }

      InfoPtr_t<PixLib::ScanInfo_t> scan_info;
      std::string file_name;
      {
	UnLock un_lock( PixDbGlobalMutex::mutex() );
	scan_info = InfoPtr_t<PixLib::ScanInfo_t>( MetaDataService::instance()->getScanInfo(scan_serial_number) );

	file_name = fileName(*scan_info);
	if (file_name.empty()) {
	  std::string message(" No file name given for scan " );
	  message += makeScanSerialNumberString(scan_serial_number);
	  message += ".";
	  //	  throw MRSException("NOSCANRESULTFILE", MRS_FATAL, "CAN::RootFileHistoReceiver::getHistograms",message);
	  throw MRSException(PixLib::PixMessages::FATAL, "CAN::RootFileHistoReceiver::getHistograms",message);
	}
      }

      // protect against parallel usage of ROOT
      std::pair<TFile *, TDirectory *> ret= open(file_name, scan_serial_number);
      if (m_verbose && ret.first) {
	std::cout << "INFO [RootFileHistoReceiver::getHistograms] created " << ret.first->GetName() << std::endl;
      }
      std::pair< std::map< SerialNumber_t, FileData_t >::iterator , bool > 
	new_elm = m_cache.insert(std::make_pair(scan_serial_number, FileData_t(std::shared_ptr<TFile>(ret.first), ret.second)));
      if (!new_elm.second) {
	std::string message(" Failed to add file " );
	message += file_name;
	message += " to internal list for scan ";
	message += makeScanSerialNumberString(scan_serial_number);
	message += ".";
	//	throw MRSException("NOSCANRESULTFILE", MRS_FATAL, "CAN::RootFileHistoReceiver::getHistograms",message);
	throw MRSException(PixLib::PixMessages::FATAL, "CAN::RootFileHistoReceiver::getHistograms",message);
      }
      file_iter = new_elm.first;
    }

    file_iter->second.incrementUsed();
    TDirectory *a_directory = file_iter->second.directory();
    if (!rod_name.empty()) {
      a_directory = enter(a_directory, rod_name);
    }

    if (!conn_name.empty() && conn_name != rod_name) {
      a_directory = enter(a_directory, conn_name);
    }

    a_directory = enter(a_directory, histo_name);

    n_histograms = loadHistograms(a_directory, array);

    return n_histograms;
  }

  bool RootFileHistoReceiver::inheritsFrom(const char *class_name, const TClass *a_class) {
    TClass *given_class = gROOT->GetClass(class_name);
    if (given_class) {
      if (given_class->InheritsFrom(a_class)) {
	return true;
      }
    }
    else {
      if (m_verbose) {
	std::cout << "WARNING [RootFileHistogramProvider::inheritsFrom] class "  << class_name << " does not exist." << std::endl;
      }
    }
    return false;
  }

  unsigned int RootFileHistoReceiver::loadHistograms(TDirectory *a_directory, TopLevelHistoArray &histo_array) {

    if (!a_directory) return 0;

    std::vector< unsigned int > index_arr;
    index_arr.reserve(4);
    std::vector< TListIter > directory_stack;
    directory_stack.push_back( TListIter(a_directory->GetListOfKeys()) );
    bool first_histo=true;
    unsigned int n_histograms=0;

    while (!directory_stack.empty()) {
      TObject *key_obj;
      while ( (key_obj = directory_stack.back().Next())) {
	assert( key_obj->InheritsFrom(TKey::Class()));
	TKey *a_key = static_cast<TKey *>(key_obj);
	std::string dir_name ( a_key->GetName() );
	if (m_verbose) {
	  std::cout << "INFO [RootFileHistogramProvider::numberOfHistograms] name = " << dir_name << " class = " << a_key->GetClassName() << std::endl;
	}
	bool is_histogram=false;
	std::string::size_type start_pos=0;

	if ( inheritsFrom(a_key->GetClassName(),  TH1::Class())) {
	    std::string::size_type dir_pos = dir_name.rfind('/');
	    if (dir_pos != std::string::npos && dir_pos+1 < dir_name.size()) {
	      start_pos=dir_pos+1;
	    }
	    is_histogram=true;
	}
	else if ( !inheritsFrom(a_key->GetClassName(),  TDirectory::Class())) {
	  if (m_verbose) {
	    
	    std::cout << "WARNING [RootFileHistogramProvider::numberOfHistograms] Only expected directories and histograms in the PixScanHisto directory " 
		      << a_directory->GetName()
		      << "." << std::endl;
	  }
	}


	bool is_pix_scan_histo;

	if (
	       (   (!is_histogram && dir_name.size()>1 && dir_name[0]>='A' && dir_name[0]<='Z' && dir_name.size()<7 && start_pos==0
		 /* : just allow a reasonable number of levels, to not confuse a pix scan histo with e.g. an analysis or scan.*/)
		|| ( is_histogram && dir_name.size()>start_pos+1 && isdigit(dir_name[start_pos])) ) ) {

	  is_pix_scan_histo=true;
	  for (unsigned int pos=start_pos+1; pos< dir_name.size(); ++pos) {
	    if (!isdigit(dir_name[pos])) {
	      if (!is_histogram || dir_name[pos]!=':') {
		is_pix_scan_histo=false;
	      }
	      break;
	    }
	  }

	  if (m_verbose) {

	    std::cout << "INFO [RootFileHistogramProvider::numberOfHistograms] name = " << dir_name << " class = " << a_key->GetClassName()
		      << (is_pix_scan_histo ? " is pix scan histo" : "")<< std::endl;
	  }

	  if (is_pix_scan_histo) {
	    assert(directory_stack.size()>0); // paranoia_check
	    if (   dir_name[start_pos]==(char)('A'+directory_stack.size()-1)
		   || ( (directory_stack.size()==index_arr.size() || ( first_histo  && directory_stack.size()==index_arr.size()+1))  && isdigit(dir_name[start_pos]))) {
	      if ( dir_name[start_pos]==(char)('A'+directory_stack.size()-1 )) {
		start_pos++;
	      }
	      else {
		first_histo=false;
	      }

	      unsigned int index=atoi(&((dir_name.c_str())[start_pos]));
	      if (directory_stack.size()>index_arr.size()) {
		index_arr.resize(directory_stack.size(), UINT_MAX);
	      }
	      index_arr[directory_stack.size()-1]=index;

	      if (is_histogram) {
		std::unique_ptr<TObject> obj( a_key->ReadObj() );
		if (obj.get() && obj->InheritsFrom(TH1::Class())) {
		  if (m_verbose) {
		    std::cout << "INFO [RootFileHistogramProvider::loadHistograms] add histogram : ";
		    for (unsigned int i =0; i<index_arr.size(); i++) {
		      std::cout << " " << index_arr[i];
		      if (i+1<index_arr.size()) {
			std::cout << ", ";
		      }
		    }
		    std::cout << static_cast<TH1 *>(obj.get())->GetName() << std::endl;
		  }
		  // take ownership from ROOT
		  static_cast<TH1 *>(obj.get())->SetDirectory(0);
		  histo_array.addHisto(index_arr,static_cast<TH1 *>(obj.release()));
		  n_histograms++;
		}
		else {
		  if (m_verbose) {
		    std::cout << "ERROR [RootFileHistogramProvider::loadHistograms] Not a histogram " 
			      << a_key->GetName()
			      << " in " << a_directory->GetName()
			      << "." << std::endl;
		  }
		}
	      }

	      if (m_verbose) {
		std::cout << "INFO [RootFileHistogramProvider::loadHistograms] name = " << &(dir_name.c_str()[start_pos]) << " class = " << a_key->GetClassName()
			  << " -> " << directory_stack.size()-1 << " : " 
			  << " - " << index_arr[directory_stack.size()-1] << std::endl;
	      }

	    }
	    else {
	      std::cout << "WARNING [RootFileHistogramProvider::numberOfHistograms] invalid subdirectory name in " << a_directory->GetName() 
			<< "." << std::endl;
	    }
	  }
	}
	else {
	  is_pix_scan_histo=false;
	}

	if (!is_pix_scan_histo) {
	  std::cout << "WARNING [RootFileHistogramProvider::numberOfHistograms] directory " << a_directory->GetName() 
		    << " contains other than pix scan histos." << std::endl;
	  continue;
	}

	if (!is_histogram) {
	  std::unique_ptr<TObject> dir_obj( a_key->ReadObj() );
	  if (dir_obj.get()) {
	    assert( dir_obj->InheritsFrom(TDirectory::Class()));
	    TDirectory *a_dir = static_cast<TDirectory *>(dir_obj.release());
	    directory_stack.push_back( TListIter(a_dir->GetListOfKeys()) );
	  }
	}
      }
      directory_stack.pop_back();
    }
    return n_histograms;
  }

  std::string RootFileHistoReceiver::fileName(const PixLib::ScanInfo_t &scan_info) {

    // try first the file name given in the meta data, then the alternate file name. Finally the
    // the path defined by the environment variable PIXSCAN_STORAGE_PATH + the base file name given
    // by the meta data.
    for (unsigned int attempt_i=0; attempt_i<3; ++attempt_i) {
      std::string file_name;
      switch (attempt_i) {
      case 0:
	file_name += scan_info.file();
	break;
      case 1:
	file_name += scan_info.alternateFile();
	break;
      case 2: {
	char *scan_data_path =getenv("PIXSCAN_STORAGE_PATH");
	if (scan_data_path && strlen(scan_data_path)>0) {
	  file_name = scan_data_path;

	  if (file_name.empty()) break; // paranoia check

	  if (file_name[file_name.size()-1]!='/') {
	    if (file_name.size()>5 && file_name.compare(file_name.size()-5,5,".root")==0) {
	      std::string::size_type pos = file_name.rfind("/");
	      if (pos!=std::string::npos) {
		file_name.erase(pos+1,file_name.size()-pos-1);
	      }
	    }
	    else {
	      file_name+='/';
	    }
	  }
	}
	const std::string &the_file_name = scan_info.file();
	std::string::size_type pos = the_file_name.rfind("/");
	if (pos==std::string::npos) {
	  pos=0;
	}
	else {
	  pos++;
	}
	std::string::size_type end_pos = the_file_name.find(".root::",pos);
	if (end_pos==std::string::npos) {
	  end_pos=the_file_name.size();
	}
	else {
	  end_pos+=5;
	}

	file_name += the_file_name.substr(pos,end_pos-pos);
	break;
      }
      }

      std::string::size_type end_pos = file_name.find(".root::");
      if (end_pos!=std::string::npos) {
	file_name.erase(end_pos+5, file_name.size()-end_pos-5);
      }
      if (file_name.empty()) continue;

      std::ifstream test_in(file_name.c_str());
      if (test_in) {
	return file_name;
      }
    }
    return "";
  }

  std::pair<TFile *, TDirectory *> RootFileHistoReceiver::open(const std::string &file_name, SerialNumber_t scan_serial_number)
  {

    std::unique_ptr<TFile> a_file(TFile::Open( file_name.c_str()));
    if (!a_file.get()) {
      std::string message("Failed to open file  ");
      message += file_name ;
      message += " does not contain directory for scan ";
      message += makeScanSerialNumberString(scan_serial_number);
      message += ".";
      //      throw MRSException("NOSCANRESULTFILE", MRS_FATAL, "CAN::RootFileHistoReceiver::open",message);
      throw MRSException(PixLib::PixMessages::FATAL, "CAN::RootFileHistoReceiver::open",message);
    }

    std::stringstream folder_name;
    folder_name << 'S' << std::setw(9) << std::setfill('0') << scan_serial_number;
    try {
      TDirectory *a_directory = enter(a_file.get(), folder_name.str());
      std::pair<TFile *, TDirectory *> ret;
      ret.first = a_file.release();
      ret.second = a_directory;
      return ret;
    }
    catch (MRSException &err) {
      std::string message("File name ");
      message += file_name ;
      message += " does not contain directory for scan ";
      message += makeScanSerialNumberString(scan_serial_number);
      message += ".";
      //      throw MRSException("NOSCANRESULTFILE", MRS_FATAL, "CAN::RootFileHistoReceiver::open",message);
      throw MRSException(PixLib::PixMessages::FATAL, "CAN::RootFileHistoReceiver::open",message);
    }

  }

  TDirectory *RootFileHistoReceiver::enter(TDirectory *a_directory, const std::string &dir_name)
  {

    TObject *obj = NULL;

    if (a_directory) {
      obj = a_directory->Get(dir_name.c_str() );
    }

    if (!obj || !obj->InheritsFrom(TDirectory::Class())) {
      std::string message("File name ");
      message += a_directory->GetFile()->GetName();
      message += " does not contain directory ";
      message += dir_name;
      message += ".";
      //      throw MRSException("NOSCANRESULTFILE", MRS_FATAL, "CAN::RootFileHistoReceiver::open",message);
      throw MRSException(PixLib::PixMessages::FATAL, "CAN::RootFileHistoReceiver::open",message);
    }

    TDirectory *sub_directory = static_cast<TDirectory *>(obj);
    sub_directory->cd();
    return sub_directory;

  }


  std::vector<std::string>  RootFileHistoReceiver::availableHistograms(SerialNumber_t scan_serial_number,
							  const std::string &rod_name,
							  const std::string &conn_name){
    std::vector<std::string> histo_names_list;

    std::map< SerialNumber_t, FileData_t >::iterator file_iter = m_cache.find(scan_serial_number);
    Lock lock( PixDbGlobalMutex::mutex() );
    
    if (file_iter == m_cache.end()) {
      if  (m_cache.size()>s_maxCacheSize) {
	while (m_cache.size()>s_maxCacheSize) {
	  std::map< SerialNumber_t, FileData_t >::iterator delete_file_iter = m_cache.begin();
	  for (std::map< SerialNumber_t, FileData_t >::iterator cache_iter = m_cache.begin();
	       ++cache_iter != m_cache.end();
	       ) {
	    if (cache_iter->second.used() < delete_file_iter->second.used()) {
	      delete_file_iter = cache_iter;
	    }
	  }
	  m_cache.erase(delete_file_iter);
	}
	for (std::map< SerialNumber_t, FileData_t >::iterator cache_iter = m_cache.begin();
	     ++cache_iter != m_cache.end();
	     ) {
	  cache_iter->second.resetUsed();
	}
      }

      InfoPtr_t<PixLib::ScanInfo_t> scan_info;
      std::string file_name;
      {
	UnLock un_lock( PixDbGlobalMutex::mutex() );
	scan_info = InfoPtr_t<PixLib::ScanInfo_t>( MetaDataService::instance()->getScanInfo(scan_serial_number) );

	file_name = fileName(*scan_info);
	if (file_name.empty()) {
	  std::string message(" No file name given for scan " );
	  message += makeScanSerialNumberString(scan_serial_number);
	  message += ".";
	  //	  throw MRSException("NOSCANRESULTFILE", MRS_FATAL, "CAN::RootFileHistoReceiver::getHistograms",message);
	  throw MRSException(PixLib::PixMessages::FATAL, "CAN::RootFileHistoReceiver::getHistograms",message);
	}
      }

      // protect against parallel usage of ROOT
      std::pair<TFile *, TDirectory *> ret= open(file_name, scan_serial_number);
      if (m_verbose && ret.first) {
	std::cout << "INFO [RootFileHistoReceiver::getHistograms] created " << ret.first->GetName() << std::endl;
      }
      std::pair< std::map< SerialNumber_t, FileData_t >::iterator , bool > 
	new_elm = m_cache.insert(std::make_pair(scan_serial_number, FileData_t(std::shared_ptr<TFile>(ret.first), ret.second)));
      if (!new_elm.second) {
	std::string message(" Failed to add file " );
	message += file_name;
	message += " to internal list for scan ";
	message += makeScanSerialNumberString(scan_serial_number);
	message += ".";
	//	throw MRSException("NOSCANRESULTFILE", MRS_FATAL, "CAN::RootFileHistoReceiver::getHistograms",message);
	throw MRSException(PixLib::PixMessages::FATAL, "CAN::RootFileHistoReceiver::getHistograms",message);
      }
      file_iter = new_elm.first;
    }

    file_iter->second.incrementUsed();
    TDirectory *a_directory = file_iter->second.directory();
    if (!rod_name.empty()) {
      a_directory = enter(a_directory, rod_name);
    }
    
    if (!conn_name.empty() && conn_name != rod_name) {
      a_directory = enter(a_directory, conn_name);
  
    }
    TIter nextkey(a_directory->GetListOfKeys());
    TKey *key;
    while ((key = (TKey*) nextkey())) {
      std::unique_ptr<TObject>obj(key->ReadObj());
      std::cout << "INFO [RootFileHistoReceiver::availableHistograms] " << key->GetName() << " =?= " << obj->GetName()<< std::endl;
      if (key->GetName() != NULL){
	std::string  histo_out_name = key->GetName();
	histo_names_list.push_back(histo_out_name);
     }
    }
    return histo_names_list;
  }
    
}
