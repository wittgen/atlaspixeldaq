#ifndef _AnalysisRequestProcessor_hh_
#define _AnalysisRequestProcessor_hh_

#include "Common.hh"
#include "Flag.hh"
#include <map>
#include <deque>
#include <memory>

//#include <mrs/message.h>

#include "Joblett_t.hh"
#include "AnalysisInfo_t.hh"
#include "IObjectConfigDb.hh"

#include "LogLevel.hh"

class IPCPartition;

namespace CAN {

  class IsDictionary;
  class IExecutionUnit;

  class AnalysisRequestProcessor : public omni_thread
  {
  public:

    AnalysisRequestProcessor(//IPCPartition &mrs_partition,
			     IsDictionary &dictionary,
			     const std::string &can_is_server_name,
			     IExecutionUnit &execution_unit);

    void queueRequest(SerialNumber_t    analysis_serial_number,
		      const AnalysisInfoData_t analysis_info,
		      const StringVec &rod_name_list);

    void abortAnalysis(SerialNumber_t    analysis_serial_number);

    void reset();

    void initiateShutdown() {
      m_shutdown=true;
      m_queueFilled.setFlag();
    }

    void shutdownWait();

    void setLogLevel(ELogLevel log_level) { m_logLevel = log_level; }

  protected:

      void *run_undetached(void *arg);

    void processRequests();

  private:

    class Request_t
    {

    public:
      Request_t(SerialNumber_t    analysis_serial_number,
		const AnalysisInfoData_t analysis_info)
	: m_analysisSerialNumber(analysis_serial_number)
      {
	copyAnalysisInfo(analysis_info, m_analysisInfo);
      }

      /** Reserve storage for the jobs to be created.
       * Calling @ref reserve is not required. It only avoids unecessary allocation and copying.
       */
      void reserve(unsigned int n_rods) { m_joblettList.reserve(n_rods);}

      /** Create an more or less empty job structure to be filled by the different steps: request queuing, processing, execution.
       */
      Joblett_t &createJob(SerialNumber_t analysis_serial_number, const std::string &rod_name, const std::string &can_is_server_name) {
	m_joblettList.push_back( std::make_pair<std::string,std::shared_ptr<Joblett_t> >((std::string)rod_name,
											   std::make_shared<Joblett_t>(analysis_serial_number,
																      rod_name,
																      can_is_server_name)));
	return *(m_joblettList.back().second.get());
      }

    public:
      SerialNumber_t serialNumber() const            { return m_analysisSerialNumber; }
      const AnalysisInfo_t &analysisInfo() const     { return m_analysisInfo; }

      JoblettList_t::const_iterator beginJoblett() const {
	return m_joblettList.begin();
      }

      JoblettList_t::iterator beginJoblett() {
	return m_joblettList.begin();
      }

      JoblettList_t::const_iterator endJoblett() {
	return m_joblettList.end();
      }

      unsigned int nJobletts() {
	return m_joblettList.size();
      }

    private:
      SerialNumber_t     m_analysisSerialNumber;
      AnalysisInfo_t     m_analysisInfo;
      std::vector< JoblettStub_t >  m_joblettList;
    };

    bool checkAbort() {
      if (m_abortRequest) {
	m_abortRequest=false;
	m_aborted.setFlag();
	return true;
      }
      else {
	return false;
      }
    }

    void setAbortedAnalysisStatus( Request_t *analysis_request);
    void abortException(SerialNumber_t analysis_serial_number);

    Mutex m_queueMutex;       /**< Mutex to ensure that the queue is only manipulated by one thread. */
    Flag  m_queueFilled;
    Flag  m_exitProcessor;
    std::deque< Request_t *> m_requestQueue;

    //    Mutex                   m_analysisJobListMutex;
    //    AnalysisJobList_t       m_analysisJobList;
    

    bool m_shutdown;
    std::unique_ptr<IObjectConfigDb> m_db;

//     Mutex                    m_mrsMutex;
//     std::auto_ptr<MRSStream> m_out;
    IsDictionary            *m_isDictionary;
    std::string              m_canIsServerName;

    IExecutionUnit          *m_executionUnit;

    Mutex                    m_currentAnalysisMutex;
    SerialNumber_t           m_currentAnalysis;

    ELogLevel                m_logLevel;

    Flag                     m_aborted;
    bool                     m_abortRequest;

    //bool m_running;
    
    static std::string s_componentAnaReqProcName;
  };
}

#endif
