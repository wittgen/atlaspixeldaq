#ifndef _CAN_ConnObjConfigDbGlobalMutex_hh_
#define _CAN_ConnObjConfigDbGlobalMutex_hh_

#include "Lock.hh"

namespace CAN {
   class ConnObjConfigDbGlobalMutex {
   public:
      static Mutex &mutex() { return s_mutex;}
   private:
      static Mutex s_mutex;
   };
}

#endif
