#ifndef _ExecutionUnit_hh_
#define _ExecutionUnit_hh_

#include "omnithread.h"

#include "Flag.hh"
#include "Joblett_t.hh"
#include "IExecutionUnit.hh"

//#include <mrs/message.h>

#include "Common.hh"
#include <map>
#include <memory>

#include "HistoResultArchive.hh"
#include "HistoResultArchiveBase.hh"

#include "LogLevel.hh"

//class MRSStream;

namespace PixLib{
  class PixMessages;
}

namespace PixA {
  class ConnectivityRef; 
}

namespace CAN {

  class IMasterDataServer;
  class IArchivingUnit;
  class IsDictionary;
  class IWorker;

  class ExecutionUnit : public IExecutionUnit, public omni_thread
  {

  public:
    class  JobThread;
    friend class JobThread;

    class JobThread : public omni_thread
    {
    private:
      /** inhibited */
      void start() {}

    public:
      JobThread(IsDictionary &is_dictionary,
		ExecutionUnit &execution_unit,
		const std::shared_ptr<IArchivingUnit> &archiver,
		//		IPCPartition &mrs_partition,
		ELogLevel log_level);

      ~JobThread();

      void setJob(SerialNumber_t analysis_serial_number, const std::string &rod_name, Joblett_t *the_joblett) {

	assert(! isUsed() );
	m_analysisSerialNumber = analysis_serial_number;
	m_joblett = the_joblett;
	m_rodName = rod_name;
	m_isUsed=true;
	m_newJob.setFlag();

      }

      bool isUsed() const {return m_isUsed;}

      void abort(bool ignore_result) {
	if (m_joblett) {
	  if (ignore_result) {
	    m_joblett->requestAbortAndIgnore();
	  }
	  else {
	    m_joblett->requestAbort();
	  }
	}
      }

      void initiateShutdown(bool ignore_result) {
	m_shutdown=true;	
	abort(ignore_result);
	m_newJob.setFlag();
      }

      void shutdownWait() {
	m_shutdown=true;
	m_newJob.setFlag();

	m_exit.wait();
	while (state()!=omni_thread::STATE_TERMINATED) {
	  usleep(1000);
	}
      }

      void setLogLevel(ELogLevel log_level) {
	m_logLevel=log_level;
      }

    protected:

      void *run_undetached(void *arg);

      void analyse();

    private:

      //IPCPartition *ipcPartition;


      Joblett_t *m_joblett;
      SerialNumber_t m_analysisSerialNumber;
      std::string   m_rodName;
      IsDictionary *m_isDictionary;
      ExecutionUnit *m_executionUnit;
      std::shared_ptr<IArchivingUnit> m_resultArchive;
      std::unique_ptr<HistoResultArchiveBase> m_histoArchiveBase;
      //      std::auto_ptr<MRSStream> m_out;

      ELogLevel  m_logLevel;

      Flag       m_exit;
      Flag       m_newJob;

      bool m_isUsed;
      bool m_shutdown;
    };

    ExecutionUnit(//IPCPartition &mrs_partition,
		  IsDictionary &dictionary,
		  IMasterDataServer &data_server,
		  IArchivingUnit *archiver,
		  unsigned int n_slots,
		  IWorker &worker);

    ~ExecutionUnit();

    /** Queue the given jobs with specified analysis serial number.
     * @param analysis_serial_number the analysis serial number.
     * @param serial_number_list the list of serial numbers of scans or analyses which define the sources of the analysis
     * @param scan_info the scan meta data.
     * @param src_analysis_serial_number serial number of a source analysis or zero
     * @param conn reference of the connectivity.
     * @param disabled_lists the a list which contains for each source a list of disabled RODs, modules.
     * @param joblett_begin begin job iterator.
     * @param joblett_end end job iterator.
     * @todo other solution than passing the connectivity, disabled list as a parameter (?)
     */
    void queueJob(const SerialNumber_t analysis_serial_number,
		  const SerialNumbers_t serial_number_list,
		  const InfoPtr_t<PixLib::ScanInfo_t> &scan_info,
		  const SerialNumber_t src_analysis_serial_number,
		  PixA::ConnectivityRef &conn,
		  const std::vector<PixA::DisabledListRoot> &disabled_lists,
		  JoblettList_t::iterator joblett_begin,
		  JoblettList_t::const_iterator joblett_end);

    void getQueueStatus(CORBA::ULong &jobs_waiting_for_data,
			CORBA::ULong &jobs_waiting,
			CORBA::ULong &jobs_running,
			CORBA::ULong &jobs_exectued,
			CORBA::ULong &empty_slots);

    bool dequeuAnalysis(SerialNumber_t    analysis_serial_number,
			const char *rod_conn_name);

    /** inform the execution unit that the data server has the requested data.
     * @param serial_number the serial number for which the data is available
     * @param rod_location the ROD connectivity name for which the data is available
     * @return true if a job was waiting for this information.
     * There should always be a job which is waiting for this information, unless the
     * queues are resetted in between.
     */
    bool dataAvailable(SerialNumber_t serial_number, const PixA::RodLocationBase &rod_location);//CLA: serial_number is the one of the analysis

    void abortAnalysis(SerialNumber_t analysis_serial_number, bool ignore_results);

    void dataIsArchived(SerialNumber_t analysis_serial_number);

    void dataServerIsDown() { m_dataServerIsDown=true; }

    void abortAnalysis(SerialNumber_t analysis_serial_number, const char *rod_conn_name, bool ignore_result);

    void reset();

    void initiateShutdown(bool ignore_result);

    void shutdownWait();

    /** Set the log level of the execution unit.
     */
    void setLogLevel(ELogLevel log_level);

  protected:
    void *run_undetached(void *arg);

    void processExecutionQueue();

    void joblettFinished() {
      m_threadFree.setFlag();
    }

    //    Mutex &mrsMutex() { return m_mrsMutex; }

    static const std::string &componentName() { return s_componentName; }

    /** Check the VM size of the worker.
     * If the VM size exceeds a certain limit a restart of the worker is triggered.
     * This method must only be called by the method @processExecutionQueue
     * @return true if the status should be checked again.
     */
    bool checkWorkerStatus();

  private:

    typedef std::map<PixA::RodLocationBase, std::shared_ptr<Joblett_t> > AnalysisThreadList_t;
    typedef std::map<SerialNumber_t, AnalysisThreadList_t> AnalysisList_t;

    Mutex                   m_analysisListMutex;
    Flag                    m_analysisReady;
    AnalysisList_t          m_analysisList;

    Flag                    m_threadFree;
    Flag                    m_exitProcessing;

    //    Mutex                   m_freeThreadListMutex;
    typedef std::vector<JobThread *> JobThreadList_t;
    JobThreadList_t           m_threadList;

    //    Mutex                    m_mrsMutex;
    //    std::auto_ptr<MRSStream> m_out;
    PixLib::PixMessages     *m_msg;
    IsDictionary            *m_isDictionary;
    IMasterDataServer       *m_dataServer;
    std::shared_ptr<IArchivingUnit>    m_resultArchive;
    unsigned int m_nJoblettsQueued;
    unsigned int m_nJoblettsReady;
    unsigned int m_nJoblettsExecuted;

    static std::string s_componentName;

    IWorker                  *m_worker;
    unsigned long             m_maxIdleVMSize; /**< maximum VM size when idle before restarting the worker in kB */

    ELogLevel m_logLevel;

    bool m_shutdown;
    bool m_dataServerIsDown;
  };
}

#endif
