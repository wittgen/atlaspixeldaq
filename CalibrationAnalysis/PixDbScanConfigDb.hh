/* Dear emacs, this is -*-c++-*- */
#ifndef _CAN_PixDbScanConfigDb_hh_
#define _CAN_PixDbScanConfigDb_hh_

#include "IScanConfigDb.hh"
#include "IConfigDb.hh"

#include <memory>

namespace CAN {

  /** Interface for a scan parameter database
   */
  class PixDbScanConfigDb : public IScanConfigDb
  {
  public:
    PixDbScanConfigDb();
    ~PixDbScanConfigDb();

    PixA::ConfigRef config(const std::string &name, const std::string &tag, unsigned int revision = 0 );
      PixA::ConfigRef config_1(const std::string &name, const std::string &tag, unsigned int revision = 0 , std::string string="");


  private:
    std::unique_ptr<IConfigDb> m_configDb;
  };

}
#endif
