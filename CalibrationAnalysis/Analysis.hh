#ifndef _CAN_Analysis_hh_
#define _CAN_Analysis_hh_

#include "IAnalysisPerROD.hh"
#include <ConfigWrapper/PixDisableUtil.h>


namespace CAN {

  class IAnalysisDataServer;

  /** The interface of analysis which analyse data per pp0.
   * These analyses can be layered by an analyses which analyse per ROD
   */
  class IAnalysisPerPP0 : public IAnalysisBase
  {
  public:

    /** Histograms need to be requested from the data server prior to the analysis.
     */
    virtual void requestHistograms(IAnalysisDataServer *data_server,
				   const PixA::Pp0LocationBase &pp0_location) = 0;

    virtual void analyse(IAnalysisDataServer *data_server,
			 const PixA::Pp0LocationBase &pp0_location,
			 ExtendedAnalysisResultList_t &analysis_results) = 0;
  };


  /** The interface of analysis which analyse data per module.
   * These analyses can be layered by an analyses which analyse per PP0
   */
  class IAnalysisPerModule : public IAnalysisBase
  {
  public:
    friend class AnalysisPerPP0;

    /** Histograms need to be requested from the data server prior to the analysis.
     */
    virtual void requestHistograms(IAnalysisDataServer *data_server,
				   const PixA::Pp0LocationBase &pp0_location,
				   const PixA::ModuleLocationBase &module_location) = 0;

    /** An analysis should resturn a status whether it was successful or whether it failed 
     *
     */
    virtual void analyse(IAnalysisDataServer *data_server,
			 const PixA::Pp0LocationBase &pp0_location,
			 const PixA::ModuleLocationBase &module_location,
			 ExtendedAnalysisResultList_t &analysis_results) = 0;
  };

  class DefaultAnalysisBase : virtual public IAnalysisBase
  {
  public:
    DefaultAnalysisBase(SerialNumber_t analysis_serial_number)
      : m_serialNumber(analysis_serial_number),
	m_abortRequest(false)
    {}

    SerialNumber_t serialNumber() const         { return m_serialNumber;}

    void requestAbort()                         { m_abortRequest=true;}

  protected:
    bool abortRequested() const                 { return m_abortRequest; }

    // non privte to give quick non-virtual access

    SerialNumber_t  m_serialNumber;
    bool            m_abortRequest;

  };

}
#endif

