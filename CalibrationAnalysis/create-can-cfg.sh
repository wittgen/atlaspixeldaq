#!/bin/sh

can_tag=Base
simu=""
if test -n ${CAN_CFG_PATH} && test -d ${CAN_CFG_PATH}; then

if test -x ./configWriter/configWriter; then
  ${simu} ./configWriter/configWriter -a TestAnalysis       ${can_tag} 0 --no-gui  -s DataSrv_histogramName "" -s test_analysisTime 50 || exit -1

#  for plugin in TestClassification TestPostProcessing THRESHOLDanalysis BONDanalysis RxThresholdDelayCheck; do
  for plugin in TestClassification TestPostProcessing THRESHOLDanalysis RxThresholdDelayCheck NOISEanalysis OCCUPANCYanalysis BASICTHRanalysis BUMPAnalysis TOTanalysis TOTbasicAnalysis T0analysis; do
     ${simu} ./configWriter/configWriter -a ${plugin} ${can_tag} 0 --no-gui || exit -1
  done || exit -1

else
  echo "ERROR [create-can-cfg.sh] Need the program \"configWriter\" which is expected to be in \"./configWriter/\" ." >&2
fi

else
  echo "ERROR [create-can-cfg.sh] The environment CAN_CFG_PATH needs to be defined. It should point to a directory for the analysis configuration files." >&2
fi
