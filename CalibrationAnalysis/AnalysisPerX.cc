#include "AnalysisPerX.hh"
#include "IAnalysisDataServer.hh"

#ifdef DEPRECATED
#include "DataServerCT.hh" //should probably not be here, but need to do different loop for this case
#endif

namespace CAN {

  AnalysisPerPP0::AnalysisPerPP0(IAnalysisPerModule &analysis) : m_analysis(&analysis) { 
  }


  void AnalysisPerPP0::requestHistograms(IAnalysisDataServer *data_server,
					 const PixA::Pp0LocationBase &pp0_location)  {

    const PixA::ModuleList &module_list=data_server->connectivity().modules(pp0_location);
    PixA::ModuleList::const_iterator module_list_begin=module_list.begin();
    PixA::ModuleList::const_iterator module_list_end=module_list.end();

    for (PixA::ModuleList::const_iterator module_iter=module_list_begin;
	 module_iter != module_list_end;
	 ++module_iter) {
      m_analysis->requestHistograms(data_server, pp0_location, PixA::location(module_iter));
    }

  }

  void AnalysisPerPP0::analyse(IAnalysisDataServer *data_server,
			       const PixA::Pp0LocationBase &pp0_location,
			       ExtendedAnalysisResultList_t &analysis_results) {

    const PixA::ModuleList &module_list=data_server->connectivity().modules(pp0_location);
    PixA::ModuleList::const_iterator module_list_begin=module_list.begin();
    PixA::ModuleList::const_iterator module_list_end=module_list.end();

    for (PixA::ModuleList::const_iterator module_iter=module_list_begin;
	 module_iter != module_list_end && !abortRequested();
	 ++module_iter) {
      m_analysis->analyse(data_server, pp0_location, PixA::location(module_iter),analysis_results);
    }
  }


  void AnalysisPerROD::requestHistograms(IAnalysisDataServer *data_server,
					 const PixA::RODLocationBase &rod_location)  {
#ifdef DEPRECATED
    DataServerCT* data_serverCT(dynamic_cast<DataServerCT*>(data_server));
    if(data_serverCT!=NULL) { 
      std::list<std::string> pp0_list = data_serverCT->getpp0Names();   //use the pp0 in the file! not the pp0 from the dummy connectivity! 
      
      std::list<std::string>::const_iterator pp0_iter     = pp0_list.begin();
      std::list<std::string>::const_iterator pp0_list_end = pp0_list.end();
      
      for (; pp0_iter != pp0_list.end(); ++pp0_iter) 
	m_analysis->requestHistograms(data_server, *pp0_iter ); //calling always the same analysis for different pp0s !!
    }
    else
#endif
    { 
      const PixA::Pp0List &pp0_list = data_server->connectivity().pp0s(rod_location);

      PixA::Pp0List::const_iterator pp0_list_begin=pp0_list.begin();
      PixA::Pp0List::const_iterator pp0_list_end=pp0_list.end();

      //       bool has_enabled_pp0s=false;
      //       for (PixA::Pp0List::const_iterator pp0_iter=pp0_list_begin;
      // 	   pp0_iter != pp0_list_end;
      // 	   ++pp0_iter) {
      // 	if( data_server->enabled(pp0_iter) ) { //CLA
      // 	  has_enabled_pp0s=true;
      // 	  break;
      // 	}
      //       }
      //       if (!has_enabled_pp0s) return;

      data_server->createCombinedModuleDisabledList( rod_location );

      for (PixA::Pp0List::const_iterator pp0_iter=pp0_list_begin;
	   pp0_iter != pp0_list_end;
	   ++pp0_iter) {
	
	if( data_server->enabled(pp0_iter) ) { //CLA
	  m_analysis->requestHistograms(data_server, location(pp0_iter));
	}

      }

    }
  }

  void AnalysisPerROD::analyse(IAnalysisDataServer *data_server,
			       const PixA::RODLocationBase &rod_location,
			       ExtendedAnalysisResultList_t &analysis_results) {
#ifdef DEPRECATED
    DataServerCT* data_serverCT(dynamic_cast<DataServerCT*>(data_server));
    if(data_serverCT!=NULL) { 
      std::list<std::string> pp0_list = data_serverCT->getpp0Names();   //use the pp0 in the file! not the pp0 from the dummy connectivity! 
      
      std::list<std::string>::const_iterator pp0_iter     = pp0_list.begin();
      std::list<std::string>::const_iterator pp0_list_end = pp0_list.end();
      
      for (; pp0_iter != pp0_list.end(); ++pp0_iter) 
	m_analysis->analyse(data_server, *pp0_iter, analysis_results); //calling always the same analysis for different pp0s !!
    }
    else 
#endif
    { 

      const PixA::Pp0List &pp0_list = data_server->connectivity().pp0s(rod_location);

      PixA::Pp0List::const_iterator pp0_list_begin=pp0_list.begin();
      PixA::Pp0List::const_iterator pp0_list_end=pp0_list.end();

      for (PixA::Pp0List::const_iterator pp0_iter=pp0_list_begin;
	   pp0_iter != pp0_list_end;
	   ++pp0_iter) {

	if( data_server->enabled(pp0_iter) ) {
	  if (logLevel()>=kDiagnosticsLog) {
	    std::cout << "DIAGNOSTIC [AnalysisPerROD::analyse] pp0 " << PixA::location(pp0_iter) << " is enabled." << std::endl;
	  }
	  m_analysis->analyse(data_server, PixA::location(pp0_iter), analysis_results);
	}
      }
    }
  }

}

