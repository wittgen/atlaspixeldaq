# Usage: . compile.csh [tag]
#   where tag is head by default, or a specific tag for CalibrationAnalysis

# Make sure the AFS token is valid
tokens | /bin/grep AFS;
if ( $? == 1 ) then
    echo Renewing AFS ticket ...
    aklog
    tokens | /bin/grep AFS
    if ( $? == 1 ) klog
else
    echo Already authenticated with AFS.
endif


# Compiles everything.
set tag=$1
if ($tag != "") set tag="-r $1"
cd ${WorkDir}
cvs co -r Pixel-DAQ RodDaq VmeInterface
cvs co -r PIT_CT_1_3 Applications/Pixel/CoralDB Applications/Pixel/PixLib Applications/Pixel/PixAnalysis
cvs co -P $tag Applications/Pixel/CalibrationAnalysis Applications/Pixel/PixCalibDbCoral
#cp -a /afs/cern.ch/user/g/goetz/public/src/SerialNumberService ${WorkDir}/Applications/Pixel

echo "======= RodDaq/RodCrate ======"
cd ${WorkDir}/RodDaq/RodCrate
make
make inst

echo "======= VmeInterface ======"
cd ${WorkDir}/VmeInterface
make
make inst

echo "======= Applications/Pixel/CoralDB ======"
cd ${WorkDir}/Applications/Pixel/CoralDB
make

echo "======= Applications/Pixel/PixLib ======"
cd ${WorkDir}/Applications/Pixel/PixLib
make
make inst
cd ${WorkDir}/Applications/Pixel/PixLib/Examples
make HistoServer

echo "======= $PIX_ANA ======"
cd $PIX_ANA
make

#echo "======= $SN_SVC ======"
#cd $SN_SVC
#make

echo "======= $CAN ======"
cd ${WorkDir}/Applications/Pixel/PixCalibDbCoral
make
cd ${WorkDir}/Applications/Pixel/PixResultsDbCoral
make
cd $CAN
make
