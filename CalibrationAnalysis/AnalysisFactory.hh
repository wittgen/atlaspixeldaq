#ifndef _AnalysisFactory_hh_
#define _AnalysisFactory_hh_

// same definitions are done by seal and omni

#ifdef HAVE_NAMESPACES
#  undef HAVE_NAMESPACES
#endif
#ifdef HAVE_BOOL
#  undef HAVE_BOOL
#endif
#ifdef HAVE_DYNAMIC_CAST
#  undef HAVE_DYNAMIC_CAST
#endif
// #include <PluginManager/PluginFactory.h>
#include <PluginManager/FactoryA2.hh>

// same definitions are done by seal and omni
#ifdef HAVE_NAMESPACES
#  undef HAVE_NAMESPACES
#endif
#ifdef HAVE_BOOL
#  undef HAVE_BOOL
#endif
#ifdef HAVE_DYNAMIC_CAST
#  undef HAVE_DYNAMIC_CAST
#endif

#include "Common.hh"

#ifdef DEFINE_SEAL_MODULE
#  undef DEFINE_SEAL_MODULE
#endif
#define DEFINE_SEAL_MODULE()

#ifdef DEFINE_SEAL_MODULE
#  undef DEFINE_SEAL_PLUGIN
#endif
#define DEFINE_SEAL_PLUGIN(a,b,c) CAN_REGISTER_NAMED_OBJECT(a,b,c)

namespace CAN {

  class IAnalysisObject;
  class IObjectConfigDb;
  class ObjectInfo_t;

  /**
   * @todo: does the constructor of an alysis need the serial number of the analysis ? I am tempted to say no
   * I do not remember why I added the serial number to the constructor.
   */
  //  typedef seal::PluginFactory< IAnalysisObject *(SerialNumber_t serial_number, const ObjectInfo_t &info) > AnalysisPluginFactory;
  typedef FactoryA2Base<IAnalysisObject, SerialNumber_t , const ObjectInfo_t &> AnalysisPluginFactory;

  /** The AnalysisFactory could actually a general factory for analysis algorithms, xclassification objects, and post-processing.
   */
  class AnalysisFactory  : public AnalysisPluginFactory
  {
  public:
    AnalysisFactory();

      IAnalysisObject *create(SerialNumber_t serial_number, const ObjectInfo_t &object_info, IObjectConfigDb *db);
      IAnalysisObject *create(SerialNumber_t serial_number, const ObjectInfo_t &object_info, IObjectConfigDb *db, std::string string);

    static AnalysisFactory *get() {
      if (!s_instance) {
	s_instance = new AnalysisFactory;
      }
      return s_instance; 
    }

    static void unloadAll();


  private:
    using AnalysisPluginFactory::create;
    static AnalysisFactory *s_instance;
  };

}

#endif
