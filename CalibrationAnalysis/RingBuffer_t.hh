#ifndef _CAN_RingBuffer_t_hh_
#define _CAN_RingBuffer_t_hh_

#include <vector>
#include <stdexcept>
#include <assert.h>

namespace CAN {

  template <class T>
  class NopCleaner
  {
  public:
    void operator()(T &) {
    }
    
  };


  template <class T, class T_ref, class T_Cleanup  = NopCleaner<T_ref> >
  class RingBuffer_t {
  public:
    RingBuffer_t(unsigned int size) 
      : m_ring(size),
	m_readIter(m_ring.begin()),
	m_writeIter(m_ring.begin())
    { assert(size>1); }

    ~RingBuffer_t() {
      while (hasElement()) takeElement();
    }

    /** Return true if there is space for an additional element
     */
    bool hasFreeSlot() const { 
      typename std::vector<T *>::iterator new_elm = m_writeIter;
      ++new_elm;
      return  (new_elm != m_readIter);
    }

    /** Add a new element to the ring buffer.
     * This function needs to be protected with a mutex, in case multiple threads add
     * elements simultaneously.
     */
    bool addElement( const T_ref elm ) {
      typename std::vector<T>::iterator new_elm = m_writeIter;
      ++new_elm;
      if (new_elm ==m_ring.end()) new_elm=m_ring.begin();

      if (new_elm == m_readIter) return false;
      *new_elm = elm;
      m_writeIter = new_elm;
      return true;
    }

    /** Return true if there is a new element.
     */
    bool hasElement() const {
      return (m_readIter != m_writeIter);
    }

    /** Return a new element or NULL.
     */
    T takeElement() {
      if (m_readIter==m_writeIter) {
	throw std::runtime_error("Buffer is empty");
      }
      ++m_readIter;
      if (m_readIter==m_ring.end()) m_readIter=m_ring.begin();
      T elm =  *(m_readIter);
      T_Cleanup cleaner;
      cleaner(*m_readIter);
      return elm;
    }

  private:
    std::vector<T> m_ring;

    typename std::vector<T>::iterator m_readIter;
    typename std::vector<T>::iterator m_writeIter;
  };

}

#endif
