#include "ArchivingUnit.hh"
#include "ExtendedAnalysisResultList_t.hh"
#include "exception.hh"
#include <is/infoT.h>
#include "isUtil.hh"

namespace CAN {

  std::string ArchivingUnit::s_componentName("CANARC");
  ArchivingUnit::ResultInfo_t ArchivingUnit::Cleanup::s_empty;

  ArchivingUnit::ArchivingUnit(//IPCPartition &mrs_partition,
			       IsDictionary &dictionary,
			       std::vector<IResultStore *> archiver,
			       ELogLevel log_level)
    : m_buffer(20),
      //m_out(new MRSStream(mrs_partition)),
      m_isDictionary(&dictionary),
      m_archiver(archiver),
      m_logLevel(log_level),
      m_reset(false)
  { start(); }

  ArchivingUnit::~ArchivingUnit()
  {
    if (m_logLevel>=kDiagnostics4Log) {
      std::cout << "DIAGNOSTICS [ArchivingUnit::dtor] Will shutdown." << std::endl;
    }
    shutdownWait();
    if (m_logLevel>=kDiagnostics4Log) {
      std::cout << "DIAGNOSTICS [ArchivingUnit::dtor] Will clear buffer." << std::endl;
    }
    clearBuffer();
    if (m_logLevel>=kDiagnostics4Log) {
      std::cout << "DIAGNOSTICS [ArchivingUnit::dtor] Will delete archiver." << std::endl;
    }

    for (std::vector<IResultStore *>::iterator archiver_iter = m_archiver.begin();
	 archiver_iter != m_archiver.end();
	 ++archiver_iter) {
      (*archiver_iter)->initiateShutdown();
    }

    for (std::vector<IResultStore *>::iterator archiver_iter = m_archiver.begin();
	 archiver_iter != m_archiver.end();
	 ++archiver_iter) {
      delete *archiver_iter;
    }
    if (m_logLevel>=kDiagnostics4Log) {
      std::cout << "DIAGNOSTICS [ArchivingUnit::dtor] All done." << std::endl;
    }
  }

  void ArchivingUnit::main()
  {
    if (m_logLevel>=kSlowInfoLog) {
      //      sendMessage(*m_out, componentName(), "ARCHIVING_started",MRS_INFORMATION, "ArchivingUnit::run_undetached", "Started");
      ers::info(PixLib::pix::daq (ERS_HERE, "ArchivingUnit::run_undetached", "Started"));
    }

    while (!shutdownRequested()) {
      m_newResults.wait( m_reset );
      if (shutdownRequested()) break;
      if ( m_reset ) {
	clearBuffer();
	if (m_logLevel>=kDiagnostics4Log) {
// 	  sendMessage(*m_out, componentName(), "ARCHIVING_reset",MRS_DIAGNOSTIC, "ArchivingUnit::run_undetached", "Removed all pending results!");
	  ers::debug(PixLib::pix::daq (ERS_HERE, "ArchivingUnit::run_undetached", "Removed all pending results!"));
	}
	if (shutdownRequested()) break;
      }
      else {
	archiveElements();
      }
    }
    clearBuffer();
    if (m_logLevel>=kSlowInfoLog) {
//       sendMessage(*m_out, componentName(), "ARCHIVING_terminated",MRS_INFORMATION, "ArchivingUnit::run_undetached", "Terminated");
      ers::info(PixLib::pix::daq (ERS_HERE, "ArchivingUnit::run_undetached", "Terminated"));
    }
  }

  void ArchivingUnit::archiveElements()
  {
    try {
      while (!shutdownRequested() && m_buffer.hasElement()) {
	m_archiving=true;
	ResultInfo_t a_result( m_buffer.takeElement() );
	if (!a_result) return;
	std::unique_ptr<ExtendedAnalysisResultList_t> result_ptr(a_result.result());

	m_freeSlots.setFlag();

	if (m_logLevel>=kDiagnosticsLog) {
	  std::stringstream message;
	  message << "Archive results for " << a_result.serialNumber() << " " << a_result.rodName() << ".";
// 	  sendMessage(*m_out, componentName(), "ARCHIVING",MRS_DIAGNOSTIC, "ArchivingUnit::run_undetached", message.str());
	  ers::debug(PixLib::pix::daq (ERS_HERE, "ArchivingUnit::run_undetached", message.str()));
	}

	std::string error_message;
	bool success=true;
	unsigned int counter=0;
	for (std::vector<IResultStore *>::iterator archiver_iter = m_archiver.begin();
	     archiver_iter != m_archiver.end();
	     ++archiver_iter, ++counter) {
	  if ( m_reset ) {
	    success=false;
	    break;
	  }
	  try {
	    if (m_logLevel>=kDiagnostics4Log) {
	      std::stringstream message;
	      message << "Running archiver " << counter << " for " << a_result.serialNumber() << " " << a_result.rodName() << " are archived.";
// 	      sendMessage(*m_out, componentName(), "ARCHIVING",MRS_DIAGNOSTIC, "ArchivingUnit::run_undetached", message.str());
	      ers::debug(PixLib::pix::daq (ERS_HERE, "ArchivingUnit::run_undetached", message.str()));
	    }
	    (*archiver_iter)->put(a_result.serialNumber(), a_result.rodName(), a_result.result());
	  }
	  catch(MRSException &err) {
	    err.sendMessage();//*m_out,"ArchivingUnit::archiveElements");
	    success=false;
	  }
	  catch(BaseException &err) {
	    error_message="Caught exception : ";
	    error_message += err.getDescriptor();
	  }
	  catch(std::exception &err) {
	    error_message="Caught exception : ";
	    error_message += err.what();
	  }
	  catch(...) {
	    error_message="Caught unknown exception when trying to store results for ";
	    std::stringstream message;
	    message << "A" << a_result.serialNumber();
	    error_message+=message.str();
	    error_message+=" ";
	    error_message+=a_result.rodName();
	    error_message+=".";
	  }
	  if (!error_message.empty() && m_logLevel>=kFatalLog) {
	    //	    sendMessage(*m_out, componentName(), "CANARC_failed",MRS_FATAL, "ArchivingUnit::archiveElements", error_message);
	    ers::fatal(PixLib::pix::daq (ERS_HERE, "ArchivingUnit::archiveElements", error_message));
	    success=false;
	  }
	  else {
	    if (m_logLevel>=kDiagnostics5Log) {
	      std::stringstream message;
	      message << "Results for " << a_result.serialNumber() << " " << a_result.rodName() << " are archived.";
// 	      sendMessage(*m_out, componentName(), "ARCHIVING",MRS_DIAGNOSTIC, "ArchivingUnit::run_undetached", message.str());
	      ers::debug(PixLib::pix::daq (ERS_HERE, "ArchivingUnit::run_undetached", message.str()));
	    }

	  }

	}

	if (success && m_logLevel==kDiagnostics4Log) {
	  std::stringstream message;
	  message << "All results for " << a_result.serialNumber() << " " << a_result.rodName() << " are archived.";
// 	  sendMessage(*m_out, componentName(), "ARCHIVING",MRS_DIAGNOSTIC, "ArchivingUnit::run_undetached", message.str());
	  ers::debug(PixLib::pix::daq (ERS_HERE, "ArchivingUnit::run_undetached", message.str()));
	}


	// publish the final analysis status
	setIsStatus(a_result.isName(), (success ? a_result.status() : AnalysisStatus::kFailure));
      }
    }
    catch(...) {
      if (m_logLevel>=kDiagnosticsLog) {
// 	sendMessage(*m_out, componentName(), "CANARC_empty",MRS_DIAGNOSTIC, "ArchivingUnit::archiveElements", "Tried to take element from empty buffer.");
	ers::debug(PixLib::pix::daq (ERS_HERE, "ArchivingUnit::archiveElements", "Tried to take element from empty buffer."));
      }
    }
    m_archiving=false;

  }

  void ArchivingUnit::clearBuffer()
  {
    Lock lock(m_writeLock);

    try {
      for(;m_buffer.hasElement();) {
	ResultInfo_t result( m_buffer.takeElement() );
	if (result.serialNumber()>0) {
	  delete result.result();

	  setIsStatus(result.isName(), AnalysisStatus::kAborted);
	  if (m_logLevel>=kDiagnostics4Log) {
	    std::stringstream error_message;
	    error_message << "Removed results for A" << result.serialNumber()  << " " << result.rodName()  << ".";
// 	    sendMessage(*m_out, componentName(), "CANARC_cleanup",MRS_ERROR, "ArchivingUnit::clearBuffer", error_message.str());
	    ers::error(PixLib::pix::daq (ERS_HERE, "ArchivingUnit::clearBuffer", error_message.str()));
	  }
	}
      }
      m_freeSlots.setFlag();

    }
    catch(...) {
      if (m_logLevel>=kDiagnosticsLog) {
// 	sendMessage(*m_out, componentName(), "CANARC_empty",MRS_DIAGNOSTIC, "ArchivingUnit::clearBuffer", "Tried to take element from empty buffer.");
	ers::debug(PixLib::pix::daq (ERS_HERE, "ArchivingUnit::clearBuffer", "Tried to take element from empty buffer."));
      }
    }
    m_reset=false;
  }

  void ArchivingUnit::queueForArchiving(SerialNumber_t analysis_serial_number,
					const std::string &rod_name,
					AnalysisStatus::EStatus final_status,
					const std::string &is_name,
					ExtendedAnalysisResultList_t *analysis_results)
  {
    unsigned int counter=0;
    bool success=false;
    while (!shutdownRequested()) {
      Lock lock(m_writeLock);
      if (m_reset) {
	success=false;
	break;
      }
      ResultInfo_t a_result(analysis_serial_number, rod_name, final_status, is_name, analysis_results);
      if (m_buffer.addElement(a_result )) {
	if (m_logLevel>=kInfoLog && counter>0) {
	  std::stringstream error_message;
	  error_message << "Needed " << counter << " attempts to queue analysis resutls for A" << analysis_serial_number << " " << rod_name << " will try again.";
// 	  sendMessage(*m_out, componentName(), "CANARC_jammed",MRS_INFORMATION, "ArchivingUnit::queueForArchiving", error_message.str());
	  ers::info(PixLib::pix::daq (ERS_HERE, "ArchivingUnit::queueForArchiving", error_message.str()));
	}

	success=true;
	break;
      }

      if (m_logLevel>=kInfoLog && counter==0) {
	std::stringstream error_message;
	error_message << "Could not queue analysis results of A" << analysis_serial_number << " " << rod_name << " for storing because the queue is full. Will try again.";
// 	sendMessage(*m_out, componentName(), "CANARC_jammed",MRS_INFORMATION, "ArchivingUnit::queueForArchiving", error_message.str());
	ers::info(PixLib::pix::daq (ERS_HERE, "ArchivingUnit::queueForArchiving", error_message.str()));
      }
      counter++;
      m_freeSlots.wait( shutdownFlag() );
    }

    if (!success) {
      setIsStatus(is_name, AnalysisStatus::kFailure);
      if (m_logLevel>=kErrorLog) {
	std::stringstream error_message;
	error_message << "Failed to queue analysis resutls for A" << analysis_serial_number << " " << rod_name << ".";
// 	sendMessage(*m_out, componentName(), "CANARC_failed",MRS_ERROR, "ArchivingUnit::queueForArchiving", error_message.str());
	ers::error(PixLib::pix::daq (ERS_HERE, "ArchivingUnit::queueForArchiving", error_message.str()));
      }
    }

    // set the new results flag anyway to wake up the result archiving thread
    m_newResults.setFlag();
  }

  void ArchivingUnit::setIsStatus(const std::string &is_name, AnalysisStatus::EStatus final_status)
  {
      try {

	Lock lock(m_isDictionary->mutex());

	ISInfoString info;
	info.setValue(AnalysisStatus::name(final_status));
	updateIsValue(m_isDictionary->dictionary(), is_name, info);

      } catch ( daq::is::Exception & ex ) {
	if (m_logLevel>=kErrorLog) {
// 	  sendMessage(*m_out, componentName(), "CANARC_iserror",MRS_ERROR, "ArchivingUnit::setIsStatus", ex.what());
	  ers::error(PixLib::pix::daq (ERS_HERE, "ArchivingUnit::setIsStatus", ex.what()));
	}
      }
  }


}
