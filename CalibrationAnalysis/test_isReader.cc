#include <ipc/partition.h>
#include <ipc/core.h>

#include <is/infodictionary.h>
#include <is/inforeceiver.h>
#include <is/infoiterator.h>
#include <is/callbackinfo.h>
#include <is/infoT.h>

#include <cassert>

#include <vector>
#include <iostream>
#include <iomanip>
#include <stdexcept>

#include <time.h>

#include "Status.hh"
#include "ScanIsStatus.hh"

class listener
{
public:
 
  listener(IPCPartition &partition, const std::string &is_server_name, 
	   const std::vector<CAN::SerialNumber_t> &monitor = std::vector<CAN::SerialNumber_t>(),
	   bool exit_when_done=true)
    : m_partition(&partition),
      m_isReceiver(partition),
      m_exitWhenDone(exit_when_done),
      m_status(true),
      m_done(false)
  {

    for (std::vector<CAN::SerialNumber_t>::const_iterator serial_number_iter = monitor.begin();
	 serial_number_iter != monitor.end();
	 serial_number_iter++) {
      m_analysis[*serial_number_iter]=false;
    }

    // get initial config values from IS server
    {
      ISInfoIterator ii( *m_partition, is_server_name, s_test.type() && ".*/status" );
      while( ii() ) {
	ii.value( m_values[ii.name()] );
	CAN::AnalysisStatus::EStatus status=CAN::AnalysisStatus::status( m_values[ii.name()].getValue() );
	std::cout << ii.name() << " : " << m_values[ii.name()].getValue()  << " -> " << (status <= CAN::AnalysisStatus::kAborted ?
					      m_values[ii.name()].getValue() 
					    : CAN::AnalysisStatus::name(CAN::AnalysisStatus::kUnknown))<< std::endl;
      }
    }
    std::string is_status_pattern( CAN::AnalysisStatus::makeRegexOrVarName( ".*",
									    CAN::AnalysisStatus::varName()));
    m_isReceiver.subscribe(is_server_name.c_str(), s_test.type() && is_status_pattern, &listener::changed, this);

    {
      std::string is_global_status_pattern( CAN::AnalysisStatus::makeRegexOrVarName( ".*",
										     CAN::AnalysisStatus::globalVarName()));
      ISInfoIterator ii( *m_partition, is_server_name, s_test.type() && is_global_status_pattern );
    while( ii() ) {
      ii.value( m_values[ii.name()] );
      CAN::AnalysisStatus::EStatus status=CAN::AnalysisStatus::status( m_values[ii.name()].getValue() );
      CAN::SerialNumber_t analysis_serial_number = serialNumber(ii.name());
      updateStatus(analysis_serial_number, status);

      std::cout << ii.name() << " : " << m_values[ii.name()].getValue() << " -> " << (status <= CAN::AnalysisStatus::kAborted ?
					    m_values[ii.name()].getValue()
					  : CAN::AnalysisStatus::name(CAN::AnalysisStatus::kUnknown) )<< std::endl;
    }
    }
    m_isReceiver.subscribe(is_server_name.c_str(), s_test.type() && ".*/globalStatus", &listener::changed, this);
    //m_isReceiver.run();
    m_semaphore.wait();
  }

  bool done() const { return m_done; }
  bool success() const { return m_status; }

protected:

  CAN::SerialNumber_t serialNumber(const std::string &is_name) {
    std::string::size_type pos = is_name.find("./A");
    const char *serial_number_start = NULL;
    if (pos == std::string::npos) {
      pos = is_name.find(".A");
      serial_number_start=&(is_name.c_str()[pos+2]);
    }
    else {
      serial_number_start=&(is_name.c_str()[pos+3]);
    }
    if (serial_number_start) {
      return atoi(serial_number_start);
    }
    return 0;
  }

  void updateStatus(CAN::SerialNumber_t analysis_serial_number, CAN::AnalysisStatus::EStatus status) {
    if (status > CAN::AnalysisStatus::kRunning && !m_analysis.empty()) {
      std::cout << " INFO  Analysis " << analysis_serial_number << " is done." << std::endl;
      bool done =true;
      for (std::map<CAN::SerialNumber_t, bool>::iterator analysis_iter = m_analysis.begin();
	   analysis_iter != m_analysis.end();
	   analysis_iter++) {
	if (analysis_iter->first == analysis_serial_number) {
	  analysis_iter->second = true;
	  if (status != CAN::AnalysisStatus::kSuccess) {
	    m_status=false;
	  }
	}
	if (!analysis_iter->second) {
	  done = false;
	}
      }
      if (done) {
	m_done=true;
	std::cout << " INFO  All Analyses are  done. This was a "  << (status ? "success" : "failure") << "."<< std::endl;
	if (m_exitWhenDone) {
	  //	  m_isReceiver.stop();
	  m_semaphore.post();
	}
      }
    }
  }

//   static void _changed(ISCallbackInfo *info)
//   {
//     listener *the_listener=reinterpret_cast<listener *>( info->parameter() );
//     the_listener->changed(info);
//   }

  void changed(ISCallbackInfo *info)
  {
    assert(info->type() == s_test.type());
    ISInfoString &val=m_values[info->name()];
    info->value(val);
    CAN::AnalysisStatus::EStatus status=CAN::AnalysisStatus::status( val.getValue() );
    std::string var_name(info->name());
    if (var_name.find("/globalStatus")!=std::string::npos) {
      CAN::SerialNumber_t analysis_serial_number = serialNumber(info->name());
      updateStatus(analysis_serial_number, status);
    }
    
    std::cout << info->name() << " : " << val.getValue() << " -> " << (status <= CAN::AnalysisStatus::kAborted ? 
					     val.getValue()
					   : CAN::AnalysisStatus::name(CAN::AnalysisStatus::kUnknown) )<< std::endl;
  }

  

  std::map<std::string, ISInfoString> m_values;
  IPCPartition   *m_partition;
  ISInfoReceiver m_isReceiver;
  // added to replace run/stop from ISInfoReceiver which no longer exists
  OWLSemaphore m_semaphore;

  std::map<CAN::SerialNumber_t, bool> m_analysis;
  bool m_exitWhenDone;
  bool m_status;
  bool m_done;
  static ISInfoString s_test;
};

class scan_is_listener
{
public:
  scan_is_listener(IPCPartition &partition, const std::string &is_server_name) : m_partition(&partition),m_isReceiver(partition) {
    // get initial config values from IS server
    {
      ISInfoIterator ii( *m_partition, is_server_name, s_test.type() && CAN::ScanIsStatus::makeRegex() );
      while( ii() ) {
	ii.value( m_values[ii.name()] );
	std::cout << ii.name() << " : " << (m_values[ii.name()].getValue())<< std::endl;
      }
    }
    m_isReceiver.subscribe(is_server_name.c_str(), s_test.type() && CAN::ScanIsStatus::makeRegex(), &scan_is_listener::changed, this);
    //    m_isReceiver.run();
    m_semaphore.wait();
  }

//   static void _changed(ISCallbackInfo *info)
//   {
//     listener *the_listener=reinterpret_cast<listener *>( info->parameter() );
//     the_listener->changed(info);
//   }

  void changed(ISCallbackInfo *info)
  {
    assert(info->type() == s_test.type());
    ISInfoString &val=m_values[info->name()];
    info->value(val);
    std::cout << info->name() << " : " << val.getValue() << std::endl;
  }

  
  std::map<std::string, ISInfoString> m_values;
  IPCPartition   *m_partition;
  ISInfoReceiver m_isReceiver;
  // added to replace run/stop from ISInfoReceiver which no longer exists
  OWLSemaphore m_semaphore;
  static ISInfoString s_test;
};

ISInfoString listener::s_test;
ISInfoString scan_is_listener::s_test;

int main(int argc, char **argv) {
  // GET COMMAND LINE

  // Check arguments

  std::string ipcPartitionName;
  std::string is_server("CAN");
  bool monitor_scan_is_status=false;
  std::vector<CAN::SerialNumber_t> monitor_analysis;
  //  bool ping_only=false;
  //  bool query_queue_states=false;
  //  bool reset_queues=false;
  for (int arg_i=1; arg_i<argc; arg_i++) {

    if (strcmp(argv[arg_i],"-p")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      ipcPartitionName = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-n")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      is_server = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-m")==0  && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      CAN::SerialNumber_t analysis_serial_number = atoi(argv[++arg_i]);
      if (analysis_serial_number>0) {
	monitor_analysis.push_back(analysis_serial_number);
      }
    }
    else if (strcmp(argv[arg_i],"-s")==0 ) {
      monitor_scan_is_status=true;
    }
    else {
      std::cout << "USAGE: " << argv[0] << " -p partition [-n IS-server-name] [-s]."  << std::endl
		<< "\t-p partition name\t the partition of the CAN/PixScan is server." << std::endl
		<< "\t-n IS-server-name\t the name of the CAN/PixScan is server." << std::endl
		<< "\t-s\t\t\tMonitor the scan status instead of the CAN status." << std::endl
		<< "\t-m [serial-number]\t\t\tMonitor the given analyses and exit if all are done." << std::endl;
      return 1;
    }
  }

  if(ipcPartitionName.empty() || is_server.empty()) {
    std::cout << "USAGE: " << argv[0] << " -p partition [-n IS-server-name]."  << std::endl;
    return 1;
  }

  // START IPC

  // Start IPCCore
  IPCCore::init(argc, argv);

  IPCPartition ipcPartition(ipcPartitionName.c_str());
  if (!ipcPartition.isValid()) {
    std::cerr << "ERROR [" << argv[0] << ":main] Not a valid partition " << ipcPartitionName << std::endl;
    return EXIT_FAILURE;
  }

  if (!monitor_analysis.empty()) {

    std::cout << "INFO [" << argv[0] << ":main] monitor analyses ";
    for (std::vector<CAN::SerialNumber_t>::const_iterator iter = monitor_analysis.begin();
	 iter != monitor_analysis.end();
	 iter++) {
      std::cout << *iter;
      if (monitor_analysis.end()-iter>1) {
	std::cout << ", ";
      }
    }
    std::cout << " on is-server "  << is_server << " in partition " << ipcPartitionName  << std::endl;
    listener a_listener(ipcPartition,is_server,monitor_analysis, true);
    for(; !a_listener.done() ; ) {
      sleep(10);
    }
    if (a_listener.success()) return 0;
    else return -1;
  }
  else if (!monitor_scan_is_status) {

  std::cout << "INFO [" << argv[0] << ":main] listen for analysis status changes on is-server "  << is_server << " in partition " << ipcPartitionName  << std::endl;
  listener a_listener(ipcPartition,is_server);
  for(;;) {
    sleep(10);
  }
  
  }
  else {

  std::cout << "INFO [" << argv[0] << ":main] listen for scan status changes on is-server "  << is_server << " in partition " << ipcPartitionName  << std::endl;
  scan_is_listener a_scan_listener(ipcPartition,is_server);
  
  for(;;) {
    sleep(10);
  }

  }
  return 0;
}
