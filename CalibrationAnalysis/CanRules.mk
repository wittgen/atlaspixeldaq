include $(PIX_LIB)/PixLib.mk   #defines CFLAGS and LFLAGS

ifeq ($(origin CPPFLAGS), undefined)
  CPPFLAGS := $(CFLAGS)
  CFLAGS = $(CPPFLAGS)
endif

SOLFLAGS = -shared
#SOLFLAGS += $(shell root-config --ldflags) \
#	    -L$(DET_COMMON)/InstallArea/$(CMTCONFIG)/lib -lCoraCool \
#            -L$(COOL)/$(CMTCONFIG)/lib -llcg_CoolKernel -llcg_CoolApplication
LFLAGS += -L$(DET_COMMON)/InstallArea/$(CMTCONFIG)/lib -lCoraCool \
            -L$(COOL)/$(CMTCONFIG)/lib -llcg_CoolKernel -llcg_CoolApplication
SOLFLAGS += $(LFLAGS)

INSTALL := install
INSTALL_PROGRAM = $(INSTALL)

install_bin:
	test -n "$(PIX_INSTALL)"
	test -n "$(CMTCONFIG)"
	mkdir -p $(PIX_INSTALL)/$(CMTCONFIG)/lib
	test -z "$(INST_LIBS)" || ( mkdir -p $(PIX_INSTALL)/$(CMTCONFIG)/lib && $(INSTALL_PROGRAM) $(INST_LIBS) $(PIX_INSTALL)/$(CMTCONFIG)/lib )
	test -z "$(INST_APPS)" || ( mkdir -p $(PIX_INSTALL)/$(CMTCONFIG)/bin && $(INSTALL_PROGRAM) $(INST_APPS)  $(PIX_INSTALL)/$(CMTCONFIG)/bin )

# remove the SEAL plugin cache
remove_cache:
	rm -f $(PIX_INSTALL)/$(CMTCONFIG)/lib/.cache

remove_local_cache:
	rm -f .cache

.depend/%.d:  %.cc $(ALL_IDL_HH)
	[ -d .depend ] || mkdir -p .depend
	$(CPP) -MM -I$(CMTCONFIG) $(CPPFLAGS) $< | sed -e 's/\($*\)\.o[ :]*/\1.o $*.d : /g' > $@
	[ -s $@ ] || (rm -f $@; exit 1)

clean_depend:
	rm -rf .depend/*.d
