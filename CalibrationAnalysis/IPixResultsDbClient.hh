#ifndef _IPixResultsDbClient_hh_
#define _IPixResultsDbClient_hh_

#include <string>

//#include "Common.hh"
#include "PixelCoralClientUtils/AnalysisResultList_t.hh"

//using namespace cool;

namespace CAN {

#ifdef __x86_64__
  typedef unsigned int SerialNumber_t;
#else
  typedef unsigned long SerialNumber_t;
#endif
  class AverageResult_t;
}

namespace coral {
  class TimeStamp;
}

class IPixResultsDbListener;

class IPixResultsDbClient {

public:

  virtual ~IPixResultsDbClient() {}

  virtual void disconnect() = 0;
 

  //PVSS methods
  virtual double get_value_from_PVSSarch(std::string,const coral::TimeStamp &,const coral::TimeStamp &) = 0;
  virtual double get_values_from_PVSSarch(std::string,const coral::TimeStamp &,const coral::TimeStamp &) = 0;

  virtual CAN::AnalysisResultList_t getAnalysisResultsFromDB(CAN::SerialNumber_t anal_id, std::string varname="", std::string connName="") = 0;
  virtual CAN::AnalysisResultList_t getAnalysisResultsFromDB(CAN::SerialNumber_t anal_id, const std::vector<std::string> &connName, std::string varname="") = 0;

  /** Get analysis results for a list of connectivity objects.
   * @param analysis_id list of analysis serial numbers,
   * @param connName list of connectivity names for which the values should be retrieved
   * @param listener object which will be notified about new variables.
   */
  virtual void getAnalysisResultsFromDB(std::vector<CAN::SerialNumber_t> analysis_id,
					const std::vector<std::string> &connName,
					IPixResultsDbListener &listener) = 0;

  /** Get analysis results for a list of connectivity objects.
   * @param analysis_id the analysis serial number,
   * @param connName list of connectivity names for which the values should be retrieved
   * @param listener object which will be notified about new variables.
   */
  void getAnalysisResultsFromDB(CAN::SerialNumber_t analysis_id, const std::vector<std::string> &connName, IPixResultsDbListener &listener) {
    std::vector<CAN::SerialNumber_t> temp;
    temp.push_back(analysis_id);
    getAnalysisResultsFromDB(temp,connName,listener);
  }

  virtual void verboseOutput(bool verbose) = 0 ;

};

#endif
