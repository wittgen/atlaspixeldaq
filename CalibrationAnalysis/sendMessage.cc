#include "sendMessage.hh"
#include <sstream>
#include <iostream>
#include <iomanip>

namespace CAN {

//   void sendMessage(MRSStream &out,
// 		   const std::string &message_id,
// 		   MRSSeverity a_severity,
// 		   const std::string &function_name,
// 		   const std::string &message)
//   {
//     std::string::size_type pos=message_id.find("_");
//     out << message_id
// 	<< a_severity;
//     if (pos != std::string::npos && pos != message_id.size() ) {
//       out << MRS_QUALIF( std::string(message_id,0,pos).c_str() );
//     }
//     out << MRS_OPT_PARAM<const char *>("method","[%s]", function_name.c_str() )
// 	<< MRS_TEXT(message)
// 	<< ENDM;

//   }

//   void sendMessage(MRSStream &out,
// 		   const std::string &message_id,
// 		   MRSSeverity a_severity,
// 		   const std::string &function_name,
// 		   const std::string &connectivity_name,
// 		   const std::string &message)
//   {
//     std::string::size_type pos=message_id.find("_");
//     out << message_id
// 	<< a_severity;
//     if (pos != std::string::npos && pos != message_id.size() ) {
//       out << MRS_QUALIF( std::string(message_id,0,pos).c_str() );
//     }
//     out << MRS_OPT_PARAM<const char *>("method","[%s]", function_name.c_str() )
// 	<< MRS_QUALIF(connectivity_name.c_str())
// 	<< MRS_TEXT(message)
// 	<< ENDM;

//   }

//   void sendMessage(MRSStream &out,
// 		   const std::string &message_id,
// 		   const std::string &module_id,
// 		   MRSSeverity a_severity,
// 		   const std::string &function_name,
// 		   const std::string &message)
//   {
//     out << message_id
// 	<< a_severity
// 	<< MRS_QUALIF(module_id.c_str())
// 	<< MRS_OPT_PARAM<const char *>("method","[%s]", function_name.c_str() )
// 	<< MRS_TEXT(message)
// 	<< ENDM;

//   }

//   void sendMessage(MRSStream &out,
// 		   const std::string &message_id,
// 		   const std::string &module_id,
// 		   MRSSeverity a_severity,
// 		   const std::string &function_name,
// 		   const std::string &connectivity_name,
// 		   const std::string &message) {
//     out << message_id
// 	<< a_severity
// 	<< MRS_QUALIF(module_id.c_str())
// 	<< MRS_OPT_PARAM<const char *>("method","[%s]", function_name.c_str() )
// 	<< MRS_QUALIF(connectivity_name.c_str())
// 	<< MRS_TEXT(message)
// 	<< ENDM;

//   }


  std::string makeSerialNumberString(char type, SerialNumber_t serial_number)
  {
    unsigned int million = (serial_number % 1000000000)/1000000;
    unsigned int thousand = (serial_number % 1000000)/1000;
    unsigned int one = (serial_number % 1000);

    std::stringstream serial_number_string;
    serial_number_string << type
			 << std::setfill('0');

    if (serial_number>=1000000000) {
      serial_number_string << std::setw(1) << serial_number / 1000000000
			   << "-";
    }
    if (serial_number>=1000000) {
      serial_number_string << std::setw(3) << million
			   << "-";
    }
    serial_number_string << std::setw(3) << thousand
			 << "-"
			 << std::setw(3) << one;
    return serial_number_string.str();
  }

}
