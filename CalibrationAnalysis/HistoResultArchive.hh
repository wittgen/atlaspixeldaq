#ifndef _CAN_HistoResultArchive_hh_
#define _CAN_HistoResultArchive_hh_

#include "IResultStore.hh"
#include "HistoResultArchiveBase.hh"
//#include <mrs/message.h>
#include "sendMessage.hh"
#include "Common.hh"
#include "exception.hh"
#include <PixHistoServer/PixHistoServerInterface.h>
#include "Lock.hh"
#include "Flag.hh"

namespace CAN {

  /** Class to store analysis histograms in the histograms server
   */
  class HistoResultArchive : public HistoResultArchiveBase
  {
  public:

    void put(SerialNumber_t id, const std::string& rod_name, ExtendedAnalysisResultList_t *results) {

      if (   results->beginHisto<TH1>() != results->endHisto<TH1>()) {

	// Lock lock(m_mutex);
	for (std::map<std::string, std::map<std::string, std::shared_ptr<TH1> > >::const_iterator full_iter=results->beginHisto<TH1>();
	     full_iter != results->endHisto<TH1 >();
	     full_iter ++) {

	  for (std::map<std::string, std::shared_ptr<TH1> >::const_iterator val_iter=full_iter->second.begin();
	       val_iter != full_iter->second.end();
	       val_iter ++) {

	    std::stringstream proper_name;
	    proper_name << '/' << 'A';
	    proper_name	<< std::setw(9) << std::setfill('0') << id ;
	    proper_name	<< '/' << rod_name ;
	    proper_name << '/' << val_iter->first ;
	    proper_name	<< '/' << full_iter->first ;
	    proper_name	<< "/A0/0:ah_" ;
	    proper_name << full_iter->first;

	    (OHInterface.get())->sendHisto(proper_name.str(),static_cast<TH1 &>(*val_iter->second));
	  }
	}
      }
    }

    void initiateShutdown() {}

  };

}

#endif

// old version:
//      PixLib::PixHistoServerInterface *histoServerInterface = 
//	new PixLib::PixHistoServerInterface(mrs_partition, (std::string)"PixelHistoServer", (std::string)"HistoNameServer", (std::string)"RootDbHistoServerInput");
