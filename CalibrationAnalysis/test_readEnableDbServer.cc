#include <PixDbServer/PixDbServerInterface.h>
#include <string>
#include <ipc/core.h>
#include <ipc/partition.h>
#include <map>

int main(int argc, char **argv)
{
  // GET COMMAND LINE


  std::string partition_name;
  std::string db_server_name;

  bool enabled = true;

  bool error=false;
  for (int arg_i=1; arg_i<argc; arg_i++) {
    if (   (strcmp(argv[arg_i],"-p")==0  || strcmp(argv[arg_i],"--partition")==0  || strcmp(argv[arg_i],"--oh-partition")==0)
	&& arg_i+1<argc && argv[arg_i+1][0]!='-') {
      partition_name = argv[++arg_i];
    }
    else if ( (strcmp(argv[arg_i],"-D")==0
	       || strcmp(argv[arg_i],"--db-server-name")==0)
	      && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      db_server_name = argv[++arg_i];
    }
    else if ( strcmp(argv[arg_i],"--db-server-partition")==0
	      && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      partition_name = argv[++arg_i];
    }
    else if ( strcmp(argv[arg_i],"--disable")==0 || strcmp(argv[arg_i],"-d")==0) {
      enabled = false;
    }
    else if (strcmp(argv[arg_i],"-h")==0) {
      error=true;
    }
  }
  if (error || partition_name.empty() || db_server_name.empty()) {
    std::cout << "USAGE: test_runAnalysis [-h] [-p//--partition] \"partition name\"" << std::endl
	      << "-D/--db-server-name\tthe name of the DB server." << std::endl
	      << "--db-server-partition\tthe partition for the db-server" << std::endl
	      << std::endl
	      << "\t-h\t\t\tShow this help."
	      << std::endl;
    return EXIT_FAILURE;
  }

  try {
    // Start IPCCore
    IPCCore::init(argc, argv);

    std::cout << "INFO ["<<argv[0]<<":main] Search for the Pixel db server in " << partition_name << std::endl;
    std::auto_ptr<IPCPartition> partition(new IPCPartition(partition_name.c_str()));

    std::map< std::string, ipc::PixDbServerInterface_var > objects;
    partition->getObjects<ipc::PixDbServerInterface>( objects );
    if (objects.begin() != objects.end()) {
      objects.begin()->second->ipc_setReadEnable(enabled);

      std::cout << "INFO [main" << argv[0] << "] " << (enabled ? "enabled" : "disabled")  << " read of Db server "
		<<  db_server_name << " in  partition " << partition_name
		<< std::endl;
    }

  }
  catch (std::exception &err) {
    std::cerr << "ERROR [" << argv[0] << ":main] Caught standard exception " << err.what() << std::endl;
  }


  return 0;
}
