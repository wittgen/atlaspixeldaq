// Dear emacs, this is -*-c++-*-
#ifndef _CAN_DirectRetrievalDataServer_hh_
#define _CAN_DirectRetrievalDataServer_hh_

#include "IDataServer.hh"
#include "IHistoReceiver.hh"
#include "sendMessage.hh"
#include "HistoArray.hh"
#include <ConfigWrapper/Connectivity.h>
#include <ScanInfo_t.hh>
#include <IMetaDataService.hh>
#include <ScanConfigDb.hh>

#include <string>
#include <stdexcept>

#include <PixModuleGroup/PixModuleGroup.h>
#include <PixModule/PixModule.h>
#include <PixBoc/PixBoc.h>
#include <PixController/PixController.h>

namespace CAN {

  class DirectRetrievalDataServer : public IDataServer
  {
  public:
    class OneWayFlag
    {
    public:
      OneWayFlag() : m_flag(false) {}
      void set() { m_flag=true; }
      operator bool() const { return m_flag; }

    private:
      bool m_flag;
    };
    typedef OneWayFlag RequestFlag;

    enum ERequestCfg {kScanCfg, kBocCfg, kModuleCfg, kModuleGroupCfg};
    
      DirectRetrievalDataServer(IHistoReceiver &histo_receiver,
				SerialNumber_t scan_serial_number,
				const std::string &rod_name,
				const PixA::ConnectivityRef &conn,
				const InfoPtr_t<PixLib::ScanInfo_t> &scan_info)
	: m_histoReceiver(&histo_receiver),
	  m_serialNumber(scan_serial_number),
	  m_scanInfo(scan_info),
	  m_conn(conn),
	  m_rodName(rod_name)
    {
    }

    ~DirectRetrievalDataServer() {}

    void requestHistogram(const std::string &histogram_type_name,
			  const std::string &conn_name) {
      try {

	TopLevelHistoArray &array = m_histogramList[conn_name][histogram_type_name];
	m_histoReceiver->getHistograms(m_serialNumber,
				       m_rodName,
				       conn_name,
				       histogram_type_name,
				       array);
	if (array.hasValidInfo()) {
	  std::cout << "INFO [DirectRetrievalDataServer::requestHistogram] info : ";
	  for (unsigned int histo_i=0; histo_i<3; histo_i++) {
	    if (array.info().maxIndex(histo_i)==0) break;
	    std::cout << array.info().maxIndex(histo_i) << ", ";
	  }
	  std::cout << array.info().minHistoIndex() << " - " << array.info().maxHistoIndex() << "." << std::endl;
	}
      }
      catch(std::exception &err) {
	std::cout << "ERROR [DirectRetrievalDataServer::requestHistogram] Caught exception while trying to retrieve histogram "
		  << histogram_type_name << " for " << conn_name << " : " << err.what() << std::endl;
      }
      catch(...) {
	std::cout << "ERROR [DirectRetrievalDataServer::requestHistogram] Caught unknown exception while trying to retrieve histogram "
		  << histogram_type_name << " for " << conn_name << "." << std::endl;
      }
    }

    /** Request a tagged analysis summary value.
     * A variable with a certain name and for a connectivity name can only requested for one tag per data server.
     */
    void requestUIntAnalysisResult(const std::string &tag_name,
    				   Revision_t &revision,
    				   const std::string &conn_name,
    				   const std::string &variable_name) {
      std::string message;
      message = "Not implemented.";
      //      throw MRSException("DATASRV", MRS_FATAL, "CAN::DirectRetrievalDataServer::requestUIntAnalysisResult",message);
      throw MRSException(PixLib::PixMessages::FATAL, "CAN::DirectRetrievalDataServer::requestUIntAnalysisResult",message);
    }

    void requestBoolAnalysisResult(const std::string &tag_name,
    				   Revision_t &revision,
    				   const std::string &conn_name,
    				   const std::string &variable_name) {
      std::string message;
      message = "Not implemented.";
      //      throw MRSException("DATASRV", MRS_FATAL, "CAN::DirectRetrievalDataServer::requestBoolAnalysisResult",message);
      throw MRSException(PixLib::PixMessages::FATAL, "CAN::DirectRetrievalDataServer::requestBoolAnalysisResult",message);
    }

    void requestAvAnalysisResult(const std::string &tag_name,
				  Revision_t &revision,
				  const std::string &conn_name,
				  const std::string &variable_name)  {
       std::string message;
       message = "Not implemented.";
       //       throw MRSException("DATASRV", MRS_FATAL, "CAN::DirectRetrievalDataServer::requestAvAnalysisResult",message);
       throw MRSException(PixLib::PixMessages::FATAL, "CAN::DirectRetrievalDataServer::requestAvAnalysisResult",message);
     }

    void requestScanConfig() { m_requestedCfg[kScanCfg].set(); }

    void requestBocConfig()  { m_requestedCfg[kBocCfg].set();  }

    void requestModuleConfig(const std::string &module_conn_name) {
      m_requestedCfg[kModuleCfg].set();
      m_moduleCfgList.insert(std::make_pair(module_conn_name, std::make_pair( OneWayFlag(), PixA::ConfigHandle())));
    }

    void requestModuleGroupConfig() { m_requestedCfg[kModuleGroupCfg].set(); }

    void numberOfHistograms(const std::string &histogram_type_name,
			    const std::string &conn_name,
			    HistoInfo_t &info) const {
      info.reset();
      const TopLevelHistoArray &top_level = histoArray(histogram_type_name, conn_name);
      if (top_level.hasValidInfo()) {
	info = histoArray(histogram_type_name, conn_name).info();
      }
    }

    const Histo *getHistogram(const std::string &histogram_name, const std::string &conn_name, const PixA::Index_t &index) const {
    return histoArray(histogram_name, conn_name).histo(index);
    }

    std::vector<std::string>  availableHistograms( const std::string &conn_name ){
      return  m_histoReceiver->availableHistograms(m_serialNumber, m_rodName, conn_name);
    }

    std::vector<std::string> dataServerHistos (const std::string &conn_name){
      std::map< std::string, std::map<std::string, TopLevelUpdateHistoArray > >::iterator conn_iter;
      std::map<std::string, TopLevelUpdateHistoArray >::iterator histo_iter;
      std::vector<std::string> scanNames;
      conn_iter = m_histogramList.find(conn_name);
      if (conn_iter != m_histogramList.end()){
	for(histo_iter = conn_iter->second.begin(); histo_iter != conn_iter->second.end() ; histo_iter++){
	  scanNames.push_back(histo_iter->first);
	}
      }
      return scanNames;
    }


    unsigned int getUIntAnalysisResult(const std::string &conn_name,
    				       const std::string &variable_name) const {
      std::string message;
      message = "Not implemented.";
      //      throw MRSException("DATASRV", MRS_FATAL, "CAN::DirectRetrievalDataServer::getUIntAnalysisResult",message);
      throw MRSException(PixLib::PixMessages::FATAL, "CAN::DirectRetrievalDataServer::getUIntAnalysisResult",message);
    }

    unsigned int getBoolAnalysisResult(const std::string &conn_name,
    				       const std::string &variable_name) const {
      std::string message;
      message = "Not implemented.";
      //      throw MRSException("DATASRV", MRS_FATAL, "CAN::DirectRetrievalDataServer::getBoolAnalysisResult",message);
      throw MRSException(PixLib::PixMessages::FATAL, "CAN::DirectRetrievalDataServer::getBoolAnalysisResult",message);
    }

    const AverageResult_t &getAvAnalysisResult(const std::string &conn_name,
    					       const std::string &variable_name) const {
      std::string message;
      message = "Not implemented.";
      //      throw MRSException("DATASRV", MRS_FATAL, "CAN::DirectRetrievalDataServer::getAvAnalysisResult",message);
      throw MRSException(PixLib::PixMessages::FATAL, "CAN::DirectRetrievalDataServer::getAvAnalysisResult",message);
    }

    const PixLib::Config &getScanConfig() const {
      if (!m_requestedCfg[kScanCfg]) {
	std::string message;
	message = "Scan config was not requested.";
	//	throw MRSException("DATASRV", MRS_FATAL, "CAN::DirectRetrievalDataServer::getScanConfig",message);
	throw MRSException(PixLib::PixMessages::FATAL, "CAN::DirectRetrievalDataServer::getScanConfig",message);
      }

      if (m_scanConfigRef.empty()) {
	if (!m_scanConfigHandle.first) {
	  if (m_serialNumber && m_scanInfo) {
	    //	    Lock globalLock(PixDbGlobalMutex::mutex());
	    m_scanConfigHandle.second = ScanConfigDb::instance()->config( m_scanInfo->scanType(),
									  m_scanInfo->scanTypeTag(),
									  m_scanInfo->scanTypeRev());
	    m_scanConfigHandle.first.set();
	  }
	}

	if (!m_scanConfigHandle.second) {
	  throw PixA::ConfigException("No scan config.");
	}

	PixA::ConfigRef temp(m_scanConfigHandle.second.ref());
	m_scanConfigRef.push_back( temp.createFullCopy() );
      }
      return m_scanConfigRef.back().config();
    }

    const PixLib::Config &getBocConfig() const {
      if (!m_requestedCfg[kBocCfg]) {
	std::string message;
	message =  "BOC config was not requested.";
	//	throw MRSException("DATASRV", MRS_FATAL, "CAN::DirectRetrievalDataServer::getBocConfig",message);
	throw MRSException(PixLib::PixMessages::FATAL, "CAN::DirectRetrievalDataServer::getBocConfig",message);
      }

      if (m_bocConfigRef.empty()) {
	if (!m_bocConfigHandle.first) {


	  // @todo the connectivity wrapper does not allow to access the Boc config from 
	  //       the rod name but only from the pp0 name. This does not make a lot of sense.
	  PixA::Pp0List pp0_list=m_conn.pp0s(m_rodName);
	  PixA::Pp0List::const_iterator pp0_begin = pp0_list.begin();
	  PixA::Pp0List::const_iterator pp0_end = pp0_list.end();
	  for(PixA::Pp0List::const_iterator pp0_iter = pp0_begin;
	      pp0_iter != pp0_end;
	      ++pp0_iter) {

	    try {
	      Lock globalLock(PixDbGlobalMutex::mutex());
	      m_bocConfigHandle.second = m_conn.bocConf(pp0_iter);
	      if (m_bocConfigHandle.second) {
		m_bocConfigHandle.first.set();
		break;
	      }
	    }
	    catch( PixA::ConfigException &err) {
	      std::cout << "ERROR [DirectRetrievalDataServer::getBocConfig] Caught exception while trying to retrieve scan config : "<< err.getDescriptor()
			<< std::endl;
	    }
	    catch( ... ) {
	      std::cout << "ERROR [DirectRetrievalDataServer::getBocConfig] Caught unknown exception."
			<< std::endl;
	    }

	  }
	}

	if (!m_bocConfigHandle.second) {
	  std::string message;
	  message += "No BOC config for";
	  message += m_rodName;
	  message += ".";
	  throw PixA::ConfigException( message );
	}

	PixA::ConfigRef temp(m_bocConfigHandle.second.ref());
	m_bocConfigRef.push_back( temp.createFullCopy() );
      }
      return m_bocConfigRef.back().config();
    }

    const PixLib::Config &getModuleConfig(const std::string &module_conn_name) const {
      if (!m_requestedCfg[kModuleCfg]) {
	std::string message;
	message = "No module config was not requested.";
	//	throw MRSException("DATASRV", MRS_FATAL, "CAN::DirectRetrievalDataServer::getModuleConfig",message);
	throw MRSException(PixLib::PixMessages::FATAL, "CAN::DirectRetrievalDataServer::getModuleConfig",message);
      }

      std::map< std::string, PixA::ConfigRef >::iterator
	module_conf_ref_iter = m_moduleCfgRefList.find(module_conn_name);
      if (module_conf_ref_iter == m_moduleCfgRefList.end()) {

	Lock globalLock(PixDbGlobalMutex::mutex());

	std::map< std::string, std::pair< OneWayFlag, PixA::ConfigHandle> >::iterator
	  module_conf_handle_iter = m_moduleCfgList.find(module_conn_name);
	if (module_conf_handle_iter == m_moduleCfgList.end()) {
	  std::string message;
	  message  = "No module config was requested for ";
	  message += module_conn_name;
	  message += ".";

	  //	  throw MRSException("DATASRV", MRS_FATAL, "CAN::DirectRetrievalDataServer::getModuleConfig",message);
	  throw MRSException(PixLib::PixMessages::FATAL, "CAN::DirectRetrievalDataServer::getModuleConfig",message);
	}
	else if (!module_conf_handle_iter->second.first) {
        std::cout<<"retrivo diretto is calling"<<std::endl;
	  module_conf_handle_iter->second.second = m_conn.modConf( module_conn_name );
	}

	if (!module_conf_handle_iter->second.second) {
	  std::string message;
	  message += "No module config for";
	  message += module_conn_name;
	  message += ".";
	  throw PixA::ConfigException( message );
	}
	PixA::ConfigRef temp = module_conf_handle_iter->second.second.ref();
	std::pair< std::map< std::string, PixA::ConfigRef >::iterator, bool>
	  ret = m_moduleCfgRefList.insert(std::make_pair(module_conn_name, temp.createFullCopy()));
	if (!ret.second) {
	  std::string message;
	  message  = "Failed to add module config for ";
	  message += module_conn_name;
	  message += " to internal list.";

	  //	  throw MRSException("DATASRV", MRS_FATAL, "CAN::DirectRetrievalDataServer::getModuleConfig",message);
	  throw MRSException(PixLib::PixMessages::FATAL, "CAN::DirectRetrievalDataServer::getModuleConfig",message);
	}
	module_conf_ref_iter = ret.first;
      }

      return module_conf_ref_iter->second.config();
    }

    std::shared_ptr<PixLib::PixModuleData_t> createConfiguredModule(const std::string &module_conn_name) const {
      const PixLib::Config &mod_config = getModuleConfig(module_conn_name);
      std::string module_name(const_cast<PixLib::Config&>(mod_config).name());
      std::string::size_type tail_pos=module_name.find("_");
      if (tail_pos != std::string::npos && tail_pos>0) {
	module_name = module_name.substr(0,tail_pos);
      }
      std::shared_ptr<PixLib::PixModuleData_t> obj(new PixLib::PixModule(NULL,NULL, "", "", module_name));
      obj->config() = const_cast<PixLib::Config &>(mod_config);
      obj->setModuleGeomConnName(module_conn_name);
      return  obj;
    }

    std::shared_ptr<PixLib::PixModuleGroupData_t> createConfiguredModuleGroup() const {
      if (!m_requestedCfg[kModuleGroupCfg]) {
	std::string message;
	message = "Scan config was not requested.";
	//	throw MRSException("DATASRV", MRS_FATAL, "CAN::DirectRetrievalDataServer::createConfiguredModuleGroup",message);
	throw MRSException(PixLib::PixMessages::FATAL, "CAN::DirectRetrievalDataServer::createConfiguredModuleGroup",message);
      }
      Lock globalLock(PixDbGlobalMutex::mutex());
      PixA::ConnObjConfigDbTagRef cfg_tag( m_conn.configDb() );

      std::shared_ptr<const PixLib::PixModuleGroup> module_group_ref( cfg_tag.configuredModuleGroupPtr(m_rodName));

      std::shared_ptr<PixLib::PixModuleGroupData_t> obj(new PixLib::PixModuleGroup( m_rodName ));

      if (!obj->getPixBoc() || !const_cast<PixLib::PixModuleGroupData_t &>(*module_group_ref).getPixBoc()) {
	std::stringstream message;
	message << "No PixBoc in module group " << const_cast<PixLib::PixModuleGroupData_t &>(*module_group_ref).getName()
		<< " for " << makeScanSerialNumberString(scanSerialNumber()) << ".";
	//	throw MRSException("NOBOC", MRS_FATAL, "CAN::DataServer::createConfiguredModuleGroup",message.str());
	throw MRSException(PixLib::PixMessages::FATAL, "CAN::DataServer::createConfiguredModuleGroup",message.str());
      }
      if (!obj->getPixController()  || !const_cast<PixLib::PixModuleGroupData_t &>(*module_group_ref).getPixController()) {
	std::stringstream message;
	message << "No PixController in module group " << const_cast<PixLib::PixModuleGroupData_t &>(*module_group_ref).getName()
		<< " for " << makeScanSerialNumberString(scanSerialNumber()) << ".";
	//	throw MRSException("NOBOC", MRS_FATAL, "CAN::DataServer::createConfiguredModuleGroup",message.str());
	throw MRSException(PixLib::PixMessages::FATAL, "CAN::DataServer::createConfiguredModuleGroup",message.str());
      }

      // copy the configurations
      obj->config() = const_cast<PixLib::PixModuleGroupData_t &>(*module_group_ref).config();

      if (obj->getPixBoc()->getConfig() &&  const_cast<PixLib::PixModuleGroupData_t &>(*module_group_ref).getPixBoc()->getConfig()) {
	*obj->getPixBoc()->getConfig() = *const_cast<PixLib::PixModuleGroupData_t &>(*module_group_ref).getPixBoc()->getConfig();
      }
      else if (obj->getPixBoc()->getConfig() || const_cast<PixLib::PixModuleGroupData_t &>(*module_group_ref).getPixBoc()->getConfig()) {
	std::stringstream message;
	message << "Missing config object for PixBoc in module group " << const_cast<PixLib::PixModuleGroupData_t &>(*module_group_ref).getName()
		<< " for " << makeScanSerialNumberString(scanSerialNumber()) << ".";
	//	throw MRSException("NOBOC", MRS_FATAL, "CAN::DataServer::createConfiguredModuleGroup",message.str());
	throw MRSException(PixLib::PixMessages::FATAL, "CAN::DataServer::createConfiguredModuleGroup",message.str());
      }

      obj->getPixController()->config() = const_cast<PixLib::PixModuleGroupData_t &>(*module_group_ref).getPixController()->config();

      return obj;
    }


    const PixA::DisabledList &getPp0DisabledList() const { return m_pp0DisabledList; }

    void setDisabledList(const PixA::DisabledListRoot &pp0_disabled_list_root) { m_pp0DisabledList = pp0_disabled_list_root; }

    bool needModuleConfig() const { return !m_moduleCfgList.empty(); }

  protected:
    const TopLevelHistoArray &histoArray(const std::string &histogram_type_name,
				   const std::string &conn_name) const {
      std::map< std::string, std::map<std::string, TopLevelUpdateHistoArray > >::const_iterator 
	conn_iter = m_histogramList.find(conn_name);
      if (conn_iter == m_histogramList.end()) {
	std::string message;
	message ="No histograms requested for ";
	message += conn_name;
	message += ".";
	//	throw MRSException("DATASRV_no_histo", MRS_FATAL, "CAN::DirectRetrievalDataServer::histoArray",message);
	throw MRSException(PixLib::PixMessages::FATAL, "CAN::DirectRetrievalDataServer::histoArray",message);
      }

      std::map<std::string, TopLevelUpdateHistoArray >::const_iterator
	histo_iter = conn_iter->second.find(histogram_type_name);
      if (histo_iter == conn_iter->second.end()) {
	std::string message;
	message = "No histogram ";
	message += histogram_type_name;
	message += "  requested for ";
	message +=  conn_name;
	message += ".";
	//	throw MRSException("DATASRV_no_histo", MRS_FATAL, "CAN::DirectRetrievalDataServer::histoArray",message);
	throw MRSException(PixLib::PixMessages::FATAL, "CAN::DirectRetrievalDataServer::histoArray",message);
      }
      return histo_iter->second;

    }

    SerialNumber_t scanSerialNumber() const { return m_serialNumber; }
 private:
    IHistoReceiver       *m_histoReceiver;
    SerialNumber_t        m_serialNumber;
    InfoPtr_t<PixLib::ScanInfo_t> m_scanInfo;
    PixA::ConnectivityRef m_conn;

    std::string           m_rodName;

    RequestFlag m_requestedCfg[3];

    mutable std::map< std::string, std::pair<OneWayFlag, PixA::ConfigHandle> > m_moduleCfgList;
    mutable std::map< std::string, PixA::ConfigRef >    m_moduleCfgRefList;

    mutable std::pair<OneWayFlag, PixA::ConfigHandle> m_scanConfigHandle;
    mutable std::pair<OneWayFlag, PixA::ConfigHandle> m_bocConfigHandle;
    mutable std::vector<PixA::ConfigRef> m_scanConfigRef;
    mutable std::vector<PixA::ConfigRef> m_bocConfigRef;
    mutable std::map< std::string, std::map<std::string, TopLevelUpdateHistoArray > > m_histogramList;

    PixA::DisabledListRoot m_pp0DisabledList;
  };

}
#endif
