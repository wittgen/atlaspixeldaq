#ifndef _CAN_Scheduler_ref_hh_
#define _CAN_Scheduler_ref_hh_

#include <ipc/partition.h>
#include "Scheduler/Scheduler.hh"
#include <stdexcept>


namespace CAN {

  class Scheduler_ref
  {
  protected:

  public:
    Scheduler_ref(IPCPartition &ipc_partition);

    ~Scheduler_ref();

    /** Submit an analysis request.
     * @param analysis_serial_number the serial number of the analysis.
     * @param analysis_info structure which describes the analysis algorithm, the classification and the post-processing. 
     * The scheduler will submit the specified analysis jobs for each ROD which was used in the scan (or analysis).
     * The serial number will be used to get the scan (or analysis) parameters including connectivity tags. Then
     * a job will be submitted for each used ROD. All jobs will be distributed among the registered nodes and queued.
     * The jobs will publish the progress in IS. If at some point nodes become free and there are still pending jobs,
     * then the pending jobs will be dequeued and re-exectuted on the free node.
     */
    void submitAnalysis(SerialNumber_t analysis_serial_number, const AnalysisInfoData_t &analysis_info) {
      m_handle->submitAnalysis(analysis_serial_number, analysis_info);
    }

    /** Query the status of all queues.
     *  This call may ask the worker nodes for the current status, thus it could block.
     */
    void getQueueStatus(CORBA::ULong &jobs_waiting_for_data, CORBA::ULong &jobs_waiting, CORBA::ULong  &jobs_running, CORBA::ULong  &empty_slots) {
      m_handle->getQueueStatus(jobs_waiting_for_data, jobs_waiting, jobs_running, empty_slots);
    }

    /** Abort all queueud and running jobs for the given analysis.
     */
    void abortAnalysis(SerialNumber_t analysis_serial_number) {
      m_handle->abortAnalysis(analysis_serial_number);
    }

    void shutdown() {
      m_handle->shutdown();
    }

  protected:
    CAN::Scheduler_var m_handle;
    
  };

}
#endif
