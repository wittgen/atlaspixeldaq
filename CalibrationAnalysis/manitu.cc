#include <ipc/partition.h>
#include <ipc/core.h>
#include "Scheduler_impl.hh"
#include "Scheduler.hh"
#include "sendMessage.hh"
#include "PixUtilities/PixMessages.h"

#include "IPCServerInstance.hh"

#include "exception.hh"

#include <ConfigWrapper/ConnectivityManager.h>
#include <ConfigWrapper/createDbServer.h>

#include "daemonise.hh"

#include <csignal>


void signal_handler(int signal) {
  CAN::IPCServerInstance::stop();
}

int main(int argc, char **argv)
{
  // GET COMMAND LINE
  std::cout << "MANITU STARTS!!!" << std::endl;

  std::string ipcPartitionName;
  std::string mrsPartitionName;
  std::string db_server_partition_name;
  std::string db_server_name;
  std::string can_is_server_name="RunParams";
  bool do_daemonise=false;

  const char *obj_id = getenv("TDAQ_APPLICATION_OBJECT_ID");
  if (obj_id==0) {
    std::string ident("manitu_");
    std::string hostname(1024,'\0');
    gethostname(&(hostname[0]),hostname.size());
    hostname.erase(strlen(hostname.c_str()));
    ident+=hostname;

    setenv("TDAQ_APPLICATION_OBJECT_ID",ident.c_str(),0);
  }

  {
    const char *app_name = getenv("TDAQ_APPLICATION_NAME");
    if (!app_name) {
      std::string new_app_name;
      std::string hostname(1024,'\0');
      gethostname(&(hostname[0]),hostname.size());
      hostname.erase(strlen(hostname.c_str()));
      new_app_name="manitu_";
      new_app_name+=hostname;
      setenv("TDAQ_APPLICATION_NAME",new_app_name.c_str(),0);
    }
  }

  for (int arg_i=1; arg_i < argc; arg_i++) {
    if (strcmp(argv[arg_i],"-d")==0) {
      do_daemonise=true;
    }
    else if (strcmp(argv[arg_i],"-p")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      ipcPartitionName = argv[++arg_i];
    }
    else if ( (strcmp(argv[arg_i],"-n")==0
	       || strcmp(argv[arg_i],"--can-is-server")==0  /* deprecated */
	       || strcmp(argv[arg_i],"--can-is-name")==0) 
	      && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      can_is_server_name = argv[++arg_i];
    }
    else if ( (strcmp(argv[arg_i],"-D")==0
	       || strcmp(argv[arg_i],"--db-server-name")==0)
	      && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      db_server_name = argv[++arg_i];
    }
    else if ( strcmp(argv[arg_i],"--db-server-partition")==0
	      && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      db_server_partition_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-m")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      mrsPartitionName = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-h")==0) {
      ipcPartitionName.clear();
      break;
    }
    else if (argv[arg_i][0]=='-') {
	PIX_ERROR("ERROR [" << argv[0] << ":main] unsupported argument : " << argv[arg_i] << ".");
	ipcPartitionName="";
        break;
    }
    else {
      if (ipcPartitionName.empty()) {
         ipcPartitionName=argv[arg_i];
      }
      else {
         mrsPartitionName=argv[arg_i];
      }
    }
  }

  // Check arguments
  if(ipcPartitionName.empty()) {
    std::cout << "USAGE: manitu [-d]  (-p) \"partition name\" [(-m) mrs partition name] [-n/--can-is-server name]" << std::endl
	      << std::endl
	      << "\t-d\t\t\tDaemonise manitu (Huh, isn't manitu a daemon already ? ;-)." << std::endl
	      << "\t-p partition\t\tSet name of the ipc partition. The -p is optional." << std::endl
	      << "\t-m mrs-partition\tSet the mrs partition name. If -m is omitted the second name will be used." << std::endl
              << "\t-n/--can-is-name\tSet the name of the IS server used to publish the anlysis results." << std::endl
              << "\t-D/--db-server-name\tSet the name of the db server." << std::endl
              << "\t--db-server-partition\tSet the name of the db server partition in case it is different from manitu's." << std::endl
	      << std::endl
	      << "\t-h\t\t\tShow this help."
	      << std::endl
	      << "The ipc partition has to be specified. The other arguments are optional."
	      << std::endl;
    return EXIT_FAILURE;
  }

  // START IPC
  PIX_INFO(argv[0] << ":main] Starting scheduler on " << ipcPartitionName);
  //<< ". Messages will go to MRS on partition " << (!mrsPartitionName.empty() ? mrsPartitionName : ipcPartitionName) << std::endl;

  if (do_daemonise) {

    int ret = daemonise(); //"manitu.log");
    if (ret != 0 ) return ret;

  }

  std::cout << "Starting IPC.." << std::endl;

  // Start IPCCore
  IPCCore::init(argc, argv);

  // Create IPCPartition
  std::unique_ptr ipcPartition = std::make_unique<IPCPartition>(ipcPartitionName.c_str());

  if (!ipcPartition->isValid()) {
    std::cerr << "ERROR [" << argv[0] << ":main] Not a valid partition " << ipcPartitionName << std::endl;
    return EXIT_FAILURE;
  }

//   std::auto_ptr<IPCPartition> mrsPartition;
//   if (!mrsPartitionName.empty()) {
//     mrsPartition = std::auto_ptr<IPCPartition>(new IPCPartition(mrsPartitionName.c_str()));
//     if (!mrsPartition->isValid()) {
//       std::cerr << "ERROR [" << argv[0] << ":main] Not a valid partition " << mrsPartitionName << " for MRS." << std::endl;
//       return EXIT_FAILURE;
//     }
//   }

  std::unique_ptr<IPCPartition> db_server_partition;
  if (!db_server_name.empty()) {
    if (!db_server_partition_name.empty()) {
      db_server_partition = std::make_unique<IPCPartition>(db_server_partition_name.c_str());
      if (!db_server_partition->isValid()) {
	std::cerr << "ERROR [" << argv[0] << ":main] Not a valid partition " << db_server_partition_name
		  << " for the db server." << std::endl;
	return EXIT_FAILURE;
      }
    }

    std::shared_ptr<PixLib::PixDbServerInterface> db_server( PixA::createDbServer((db_server_partition.get() ?
										       *db_server_partition
										     : *ipcPartition),
										    db_server_name) );
    if (db_server.get()) {
      PixA::ConnectivityManager::useDbServer( db_server );
    }
    else {
//       MRSStream out((mrsPartition.get() ? *(mrsPartition.get()) : *(ipcPartition.get())));
//       CAN::sendMessage(out,"SCHED_no_db_server","SCHED",MRS_WARNING,"main","Failed to create the db server.");
      
      PIX_WARNING("Failed to create the db server.");
    }
  }

  // Install the signal handler
  std::signal(SIGINT   , signal_handler);
  std::signal(SIGQUIT  , signal_handler);
  std::signal(SIGILL   , signal_handler);
  std::signal(SIGTRAP  , signal_handler);
  std::signal(SIGABRT  , signal_handler);
  std::signal(SIGIOT   , signal_handler);
  std::signal(SIGBUS   , signal_handler);
  std::signal(SIGFPE   , signal_handler);
  std::signal(SIGKILL  , signal_handler);
  std::signal(SIGSEGV  , signal_handler);
  std::signal(SIGPIPE  , signal_handler);
  std::signal(SIGTERM  , signal_handler);
  std::signal(SIGSTKFLT, signal_handler);
  std::signal(SIGSYS   , signal_handler);

  try {
    std::cout << "starting scheduler." << std::endl;
    //    new CAN::Scheduler_impl(*(ipcPartition.get()), (mrsPartition.get() ? *(mrsPartition.get()) : *(ipcPartition.get())), can_is_server_name);
    new CAN::Scheduler_impl(*(ipcPartition.get()), can_is_server_name);
  }
  catch (CAN::MRSException &err) {
    //MRSStream out((mrsPartition.get() ? *(mrsPartition.get()) : *(ipcPartition.get())));
    err.sendMessage();//out, std::string(argv[0])+":main" );
    PIX_ERROR(argv[0] << ":main] Caught exception " << err.getDescriptor());
  }
  catch (std::exception &err) {
    PIX_ERROR( argv[0] << ":main] Caught standard exception " << err.what());
  }
  std::cout << "manitu finished." << std::endl;

  return EXIT_SUCCESS;
}
