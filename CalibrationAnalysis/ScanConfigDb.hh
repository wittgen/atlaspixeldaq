#ifndef _CAN_ScanConfigDb_hh_
#define _CAN_ScanConfigDb_hh_

#include "exception.hh"

#include "Common.hh"
#include "IScanConfigDb.hh"

namespace CAN {

  /** Helper class to get an instance of the scan config db.
   */
  class ScanConfigDb
  {
  public:

    /** Get an instance of the scan config db.
     */
    static IScanConfigDb *instance();

    /** Destroy an existing instance of the scan config db interface.
     */
    static void deepClenup();
  private:
    static IScanConfigDb *s_instance;
  };

}

#endif
