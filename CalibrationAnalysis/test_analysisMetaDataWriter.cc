#include "MetaDataService.hh"

#include "AnalysisInfo_t.hh"

#include <ipc/partition.h>
#include <ipc/core.h>
//#include <SerialNumberServer.hh>
//#include <SerialNumberServer_ref.hh>

#include "PixMetaException.hh"

int main(int argc, char **argv)
{
  std::string partition_name;
  std::string name[3];
  CAN::Revision_t rev[3];
  std::string tag[3];
  CAN::SerialNumber_t scan_serial_number=0;

  for (int arg_i=1; arg_i<argc; arg_i++) {
    if (strcmp(argv[arg_i],"-p")==0 && arg_i+1<argc) {
      partition_name = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-a")==0 && arg_i+3<argc && argv[arg_i+1][0]!='-' && argv[arg_i+2][0]!='-') {
      name[0] = argv[++arg_i];
      tag[0] = argv[++arg_i];
      rev[0] = atoi(argv[++arg_i]);
    }
    else if (strcmp(argv[arg_i],"-c")==0 && arg_i+3<argc && argv[arg_i+1][0]!='-' && argv[arg_i+2][0]!='-') {
      name[1] = argv[++arg_i];
      tag[1] = argv[++arg_i];
      rev[1] = atoi(argv[++arg_i]);
    }
    else if (strcmp(argv[arg_i],"-x")==0 && arg_i+3<argc && argv[arg_i+1][0]!='-' && argv[arg_i+2][0]!='-') {
      name[2] = argv[++arg_i];
      tag[2] = argv[++arg_i];
      rev[2] = atoi(argv[++arg_i]);
    }
    else if (strcmp(argv[arg_i],"-s")==0 && arg_i+1<argc) {
      scan_serial_number = atoi(argv[++arg_i]);
    }
    else {
      std::cerr << "ERROR ["<< argv[0] << ":main] unhandled argument " << arg_i << " : " << argv[arg_i]<< "." << std::endl;
    }
  }
  
  std::vector<CAN::ObjectInfo_t> obj_list;
  for (unsigned int obj_i=0; obj_i<3; obj_i++) {
    if (name[obj_i].empty() || tag[obj_i].empty()) {
      std::cerr << "usage: " << argv[0] << "-p partition -a analysis tag rev -c classification tag rev -x post-processing tag rev." << std::endl;
      return -1;
    }
    else {
      obj_list.push_back(CAN::ObjectInfo_t(name[obj_i],tag[obj_i],rev[obj_i]));
    }
  }
  assert(obj_list.size()==3);

  CAN::InfoPtr_t<CAN::AnalysisInfo_t> analysis_info(new CAN::AnalysisInfo_t(obj_list[0],obj_list[1],obj_list[2], scan_serial_number, 0,CAN::kTrash,"my comment",time(0),0,"file1","file2"));

  // START IPC

  // Start IPCCore
  IPCCore::init(argc, argv);

  try {
    // Get serial number from CORAL MetaDataService if it exists
    CAN::SerialNumber_t serial_number = CAN::MetaDataService::instance()->newSerialNumber(CAN::kAnalysisNumber);
//     if (serial_number == 0) {
//       // Create IPCPartition
//       std::cout << "INFO [" << argv[0] << ":main] Get serial number from partition " << partition_name << std::endl;
//       IPCPartition *ipcPartition = new IPCPartition(partition_name.c_str());
//       //      SENSE::SerialNumberServer_ref serial_number_ref(*ipcPartition);
//       //      serial_number = serial_number_ref.getNewSerialNumber(SENSE::kAnalysis);
//     }
    CAN::MetaDataService::instance()->addInfo(serial_number,analysis_info);

    std::cout << "INFO [" << argv[0] << ":main] Added analysis meta data :: " << serial_number << " : " << std::endl << "      "
	      << analysis_info->objectInfo(CAN::kAlg).name() << "/"
	      << analysis_info->objectInfo(CAN::kAlg).tag() << "/"
	      << analysis_info->objectInfo(CAN::kAlg).revision() << std::endl << "      "
	      << analysis_info->objectInfo(CAN::kClassification).name() << "/"
	      << analysis_info->objectInfo(CAN::kClassification).tag() << "/"
	      << analysis_info->objectInfo(CAN::kClassification).revision() << std::endl << "      "
	      << analysis_info->objectInfo(CAN::kPostProcessing).name() << "/"
	      << analysis_info->objectInfo(CAN::kPostProcessing).tag() << "/"
	      << analysis_info->objectInfo(CAN::kPostProcessing).revision() << std::endl << "      "
	      << " analysis of "
	      << analysis_info->scanSerialNumber()
	      << std::endl;
    
  }
  catch (CAN::PixMetaException &err) {
    std::cerr << "ERROR [" << argv[0] << ":main] Caught PixMetaException " << err.what() << std::endl;
  }
  catch (std::exception &err) {
    std::cerr << "ERROR [" << argv[0] << ":main] Caught standard exception " << err.what() << std::endl;
  }

  return 0;
}
