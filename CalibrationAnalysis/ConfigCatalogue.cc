//////////////////////////////////////////////////
///
/** @file CoralDbclient.cpp

C++ class for creating the scan/analysis metadata DB schema,
fill the DB, read it back

*/
///////////////////////////////////////////////////

// SEAL API
#ifdef CORAL_ONE
#  include "SealKernel/Context.h"
#  include "SealKernel/ComponentLoader.h"
#  include <SealServices/Application.h>
#else
#  include "CoralKernel/Context.h"
#  include "CoralBase/MessageStream.h"
#endif

// CORAL API
#include "CoralBase/Attribute.h"
#include "CoralBase/AttributeList.h"
#include "CoralBase/AttributeListException.h"
#include "RelationalAccess/IConnectionService.h"
//#include "RelationalAccess/IConnectionServiceConfiguration.h"
#include "RelationalAccess/ISessionProxy.h"
#include "RelationalAccess/ISchema.h"
#include "RelationalAccess/ITransaction.h"

#include "RelationalAccess/ITable.h"
#include "RelationalAccess/ITableDescription.h"
#include "RelationalAccess/IColumn.h"
#include "RelationalAccess/ICursor.h"
#include "RelationalAccess/IQuery.h"
#include "RelationalAccess/TableDescription.h"
#include "RelationalAccess/ITableDataEditor.h"
//#include "RelationalAccess/ITableSchemaEditor.h"
//#include "RelationalAccess/IBulkOperation.h"
#include "RelationalAccess/SchemaException.h"


#include "ConfigCatalogue.hh"
//#include "PixMetaException.hh"

#include <sys/stat.h>

// for FileInfo
#include "PluginManager/DirIter.hh"

namespace CAN {

 class UMaskChanger
  {
  public:
    UMaskChanger(mode_t new_mask) { m_oldMask=umask(new_mask); }
    ~UMaskChanger() { umask(m_oldMask);}
  private:
    mode_t  m_oldMask;
  };


  const char *ConfigCatalogue::s_configKeyTable = "CFG_KEY";
  const char *ConfigCatalogue::s_configNameTable = "CFG_NAMES";
  const char *ConfigCatalogue::s_configCatalogTable = "CFG_CATALOG";
  const char *ConfigCatalogue::s_configCommentTable = "CFG_COMMENTS";
  const char *ConfigCatalogue::s_columnNames[ConfigCatalogue::kNColumns] = {
    "fk",
    "name",
    "name_fk",
    "class_name",
    "category",
    "level",
    "tag_name",
    "revision",
    "date",
    "comment"
  };

  const char *ConfigDescription::s_levelName[ConfigDescription::kNLevels] = {
    "Normal",
    "Expert",
    "Debug",
    "Hidden"
  };


  ConfigCatalogue::ConfigCatalogue(const std::string &connection_string, bool verbose, bool update)
    : m_verbose( verbose )
  {
#ifdef CORAL_ONE
    seal::Application app;
    seal::Context *context = app.context();
    init( *context, connection_string, update );
#else
    init(connection_string, update);
#endif
  }

#ifdef CORAL_ONE
  void ConfigCatalogue::init(seal::Context &context, const std::string &connection_string, bool update)
  {
    UMaskChanger umask_changer(0);
    if (m_verbose) std::cout << "Creating Connection Service (here)"<<std::endl;
    // get all IConnectionService's already loaded
    std::vector< seal::IHandle< coral::IConnectionService > > loadedServices;
    context.query( loadedServices );

    if ( loadedServices.empty() ) { // if no connection service available
      // Get the component loader
      seal::Handle<seal::ComponentLoader> loader = 
	context.component<seal::ComponentLoader>();
      // Load the COOLCORAL connection service
      loader->load( "CORAL/Services/ConnectionService" );
      context.query( loadedServices );
      if ( loadedServices.empty() ) { 
	throw MissingConfigDescription("Failed to load the connection service needed to get the config description from CORAL.");
      }
    }

    seal::IHandle<coral::IConnectionService> connectionService = loadedServices.front();

    /// connection to CORAL
    m_session = std::auto_ptr<coral::ISessionProxy>( connectionService->connect( connection_string,  (update ? coral::Update : coral::ReadOnly)) );
    std::string sqlite_header("sqlite_file:");
    if (connection_string.compare(0,sqlite_header.size(),sqlite_header)==0) {
	std::string file_name(connection_string.substr(sqlite_header.size(),connection_string.size()-sqlite_header.size()));
        FileInfo file_info(file_name);
	if (file_info.exists()) {
          // make file world readable and writeable
  	   chmod(file_name.c_str(),S_IRGRP|S_IRUSR|S_IROTH|S_IWRITE|S_IWGRP|S_IWOTH); 
        }
    }
    m_connString = connection_string;
  }
#else
  void ConfigCatalogue::init(const std::string &connection_string, bool update)
  {
   // Create the RCDConnectionService
   if(m_verbose) std::cout << "INFO [ConfigCatalogue::init]" <<std::endl;
   // get all IConnectionService's already loaded
 
   // Instantiate connection service
   coral::Context* context = &coral::Context::instance();
 
   // Load CORAL connection service
   coral::IHandle<coral::IConnectionService> lookSvcH = context->query<coral::IConnectionService>();
   if (!lookSvcH.isValid()) {
       context->loadComponent( "CORAL/Services/ConnectionService" );
       lookSvcH = context->query<coral::IConnectionService>();
   }
   if (!lookSvcH.isValid()) {
       throw std::runtime_error( "Could not locate the connection service" );
   }
   /// connection to CORAL
   if(m_verbose) {
     std::cout << "INFO [ConfigCatalogue::init] connect to " << connection_string << " with access mode " 
               << (update ? coral::Update : coral::ReadOnly) <<  std::endl;
     std::cout << "INFO [ConfigCatalogue::init] change verbosity level : " <<  std::endl;
     coral::MessageStream::setMsgVerbosity(coral::Verbose);
   }
   m_session = std::unique_ptr<coral::ISessionProxy>( lookSvcH->connect( connection_string, (update ? coral::Update : coral::ReadOnly) ) );
   std::string sqlite_header("sqlite_file:");
   if (connection_string.compare(0,sqlite_header.size(),sqlite_header)==0) {
     std::string file_name(connection_string.substr(sqlite_header.size(),connection_string.size()-sqlite_header.size()));
     FileInfo file_info(file_name);
     if (file_info.exists()) {
       // make file world readable and writeable
       chmod(file_name.c_str(),S_IRGRP|S_IRUSR|S_IROTH|S_IWRITE|S_IWGRP|S_IWOTH); 
     }
   }
   m_connString = connection_string;

  }
#endif

  ConfigCatalogue::~ConfigCatalogue() {
    if (m_verbose) std::cout << "INFO [ConfigCatalogue::dtor] Desconnecting from "<< m_connString << "." << std::endl;
  }

  void ConfigCatalogue::printTables()
  {
    if (m_verbose) std::cout << "INFO [ConfigCatalogue::printTableContent] Print tables." << std::endl;

    transactionStartReadOnly();
    std::set<std::string> tables = m_session->nominalSchema().listTables();
    transactionCommit();
    for ( std::set<std::string>::iterator tName = tables.begin();
	  tName != tables.end(); ++tName ){
      std::cout << "\t" << *tName << std::endl;
    }
  }

  void ConfigCatalogue::printTableDesc()
  {
    if (m_verbose) std::cout << "INFO [ConfigCatalogue::printTableContent] Print table description " << s_configCatalogTable << "." << std::endl;

    transactionStartReadOnly();
    const coral::ITableDescription& tableDesc =
      m_session->nominalSchema().tableHandle(s_configCatalogTable).description();
    std::string Name = tableDesc.name();
    int NumCol = tableDesc.numberOfColumns();
    std::vector<std::string> colName; colName.resize(NumCol);
    std::vector<std::string> colType; colType.resize(NumCol);
    for(int i=0; i<NumCol; ++i){
      const coral::IColumn& colDesc = tableDesc.columnDescription(i);
      colName[i] = colDesc.name();
      colType[i] = colDesc.type();
    }

    transactionCommit();

    std::cout << "\nName         : "<< Name << std::endl;
    printf(" Num of cols : %d",NumCol);
    std::cout << "\n Columns     :"<< std::endl;
    for(int i=0; i<NumCol; ++i)
      std::cout <<"	"<< colName[i] <<"	("<< colType[i]<<")"<< std::endl;
  }

  void ConfigCatalogue::printTableContent(){
    if (m_verbose) std::cout << "INFO [ConfigCatalogue::printTableContent] Print table " << s_configCatalogTable << "." << std::endl;

    transactionStartReadOnly();
    std::unique_ptr<coral::IQuery> query( m_session->nominalSchema().tableHandle(s_configCatalogTable).newQuery() );
    coral::ICursor& cursor = query->execute();
    //  int nRows = 0;
    while ( cursor.next() ) {
      cursor.currentRow().toOutputStream( std::cout ) << std::endl;
      //    ++nRows;
    }
    transactionCommit();

  }

  void ConfigCatalogue::createTables(){
    UMaskChanger umask_changer(0);
    if (m_verbose) std::cout << "INFO [ConfigCatalogue::createTables] Create table " << s_configCatalogTable << "." << std::endl;

    transactionStartUpdate();

    {
      // config name table
      std::string tablename = s_configNameTable;

      if (!m_session->nominalSchema().existsTable(tablename)) {
	//    if (m_verbose) std::cout << "INFO [ConfigCatalogue::createTables] createTable " << s_configCatalogTable << "." << std::endl;

	coral::TableDescription columns;
	columns.setName( tablename );
	columns.insertColumn( s_columnNames[kName],
			      coral::AttributeSpecification::typeNameForType<std::string>());
	columns.insertColumn( s_columnNames[kNameFk],
			      coral::AttributeSpecification::typeNameForType<long long>());

	//	columns.setPrimaryKey( s_columnNames[kName] );
	columns.setNotNullConstraint ( s_columnNames[kName] );
	columns.setUniqueConstraint(s_columnNames[kNameFk],std::string( s_columnNames[kNameFk])+"_uniq");

	// Create the actual table
	m_session->nominalSchema().createTable( columns );
      }
      else {
	if (m_verbose) std::cout << "INFO [ConfigCatalogue::createTables] table  " << s_configCatalogTable << " exists already." << std::endl;
      }
    }

    {
      // config description table
      std::string tablename = s_configCatalogTable;

      if (!m_session->nominalSchema().existsTable(tablename)) {
	//    if (m_verbose) std::cout << "INFO [ConfigCatalogue::createTables] createTable " << s_configCatalogTable << "." << std::endl;

	coral::TableDescription columns;
	columns.setName( tablename );
	columns.insertColumn( s_columnNames[kNameFk],
			      coral::AttributeSpecification::typeNameForType<long long>());
	columns.insertColumn( s_columnNames[kClassName],
			      coral::AttributeSpecification::typeNameForType<std::string>());
	columns.insertColumn( s_columnNames[kCategory],
			      coral::AttributeSpecification::typeNameForType<std::string>());
	columns.insertColumn( s_columnNames[kLevel],
			      coral::AttributeSpecification::typeNameForType<unsigned short>());

	//	columns.setPrimaryKey( s_columnNames[kNameFk] );
	columns.createForeignKey( std::string("description")+"_fk", s_columnNames[kNameFk], s_configNameTable,  s_columnNames[kNameFk]);
	//	columns.setNotNullConstraint ( s_columnNames[kNameFk] );

	// Create the actual table
	m_session->nominalSchema().createTable( columns );
      }
      else {
	if (m_verbose) std::cout << "INFO [ConfigCatalogue::createTables] table  " << s_configCatalogTable << " exists already." << std::endl;
      }
    }

    {
      // config comment table
      std::string tablename = s_configCommentTable;

      if (!m_session->nominalSchema().existsTable(tablename)) {
	//    if (m_verbose) std::cout << "INFO [ConfigCatalogue::createTables] createTable " << s_configCatalogTable << "." << std::endl;

	coral::TableDescription columns;
	columns.setName( tablename );
	columns.insertColumn( s_columnNames[kNameFk],
			      coral::AttributeSpecification::typeNameForType<long long>());
	columns.insertColumn( s_columnNames[kCommentColumnTagName],
			      coral::AttributeSpecification::typeNameForType<std::string>());
	columns.insertColumn( s_columnNames[kCommentColumnRevision],
			      coral::AttributeSpecification::typeNameForType<Revision_t>());
	columns.insertColumn( s_columnNames[kCommentColumnDate],
			      coral::AttributeSpecification::typeNameForType<CAN::time_t>());
	columns.insertColumn( s_columnNames[kCommentColumnCommentText],
			      coral::AttributeSpecification::typeNameForType<std::string>());

	columns.createForeignKey( std::string("comment")+"_fk", s_columnNames[kNameFk], s_configNameTable,  s_columnNames[kNameFk]);
	columns.setNotNullConstraint ( s_columnNames[kCommentColumnDate] );

	//	columns.setNotNullConstraint ( s_columnNames[kCommentColumnName] );

	// Create the actual table
	m_session->nominalSchema().createTable( columns );
      }
      else {
	if (m_verbose) std::cout << "INFO [ConfigCatalogue::createTables] table  " << s_configCatalogTable << " exists already." << std::endl;
      }
    }
    transactionCommit();

    if (m_verbose) std::cout << "INFO [ConfigCatalogue::createTables] done with " << s_configCatalogTable << "." << std::endl;
  }

  void ConfigCatalogue::dropTables(){
    if (m_verbose) std::cout << "INFO [ConfigCatalogue::dropTables] drop " << s_configCatalogTable << "." << std::endl;

    transactionStartUpdate();
    m_session->nominalSchema().dropTable(s_configCatalogTable);
    m_session->nominalSchema().dropTable(s_configCommentTable);
    m_session->nominalSchema().dropTable(s_configNameTable);
    transactionCommit();

    if (m_verbose) std::cout << "INFO [ConfigCatalogue::dropTables] dropped " << s_configCatalogTable << "." << std::endl;
  }

  ConfigDescription ConfigCatalogue::getDescription(const std::string &name)
  {

    std::string tablename = s_configCatalogTable;

    if (m_verbose) std::cout << "INFO [ConfigCatalogue::getDescription] get description for " << name << "." << std::endl;

    transactionStartReadOnly();


    ConfigDescription description;
    {
      std::unique_ptr<coral::IQuery> query( m_session->nominalSchema().newQuery() );

      // Get the list of columns
      //  const coral::ITableDescription& tableDesc =
      //    m_session->nominalSchema().tableHandle(tablename).description();
      //  int NumCol = tableDesc.numberOfColumns();
      //  for(int i=0; i<NumCol; ++i){
      //    const coral::IColumn& colDesc = tableDesc.columnDescription(i);
      //    query->addToOutputList(colDesc.name());
      //if (m_verbose) std::cout <<"	"<<i<<": "<< colDesc.name() << std::endl;
      //  }
      query->addToTableList(s_configCatalogTable);
      query->addToTableList(s_configNameTable);

      query->addToOutputList(s_columnNames[kClassName]);
      query->addToOutputList(s_columnNames[kCategory]);
      query->addToOutputList(s_columnNames[kLevel]);

      query->defineOutputType(s_columnNames[kClassName],coral::AttributeSpecification::typeNameForType<std::string>());
      query->defineOutputType(s_columnNames[kCategory],coral::AttributeSpecification::typeNameForType<std::string>());
      query->defineOutputType(s_columnNames[kLevel],coral::AttributeSpecification::typeNameForType<unsigned short>());

      std::string cond = s_configNameTable;
      cond += ".";
      cond += s_columnNames[kName];
      cond += " = :name";
      cond += " AND ";
      cond += s_configNameTable;
      cond += ".";
      cond += s_columnNames[kNameFk];
      cond += " = ";
      cond += s_configCatalogTable;
      cond +=  ".";
      cond += s_columnNames[kNameFk];
      coral::AttributeList cond_data;
      cond_data.extend<std::string>( "name" );
      query->setCondition( cond, cond_data);
      cond_data["name"].data<std::string>() = name;
      coral::ICursor& cursor = query->execute();

      if ( cursor.next() ) {
	const coral::AttributeList &row = cursor.currentRow();
	description.setClassName( row[ s_columnNames[kClassName] ].data<std::string>() );
	description.setCategory( row[ s_columnNames[kCategory] ].data<std::string>() );
	description.setLevel( static_cast<ConfigDescription::EConfigLevel>(row[ s_columnNames[kLevel] ].data<unsigned short>()) );
	if (description.level()>ConfigDescription::kNLevels) {
	  std::stringstream message;
	  message<<" ERROR [ConfigCatalogue::getDescription] Invalid level type." << name << ".";
	  throw CAN::MissingConfigDescription(message.str());
	}
      } else {
	std::stringstream message;
	message<<" ERROR [ConfigCatalogue::getDescription] No description for " << name << ".";
	throw CAN::MissingConfigDescription(message.str());
      }
    }
    transactionCommit();

    if (m_verbose) std::cout << "INFO [ConfigCatalogue::getDescription] got description for " << name << " : " 
			     << description.className()
			     << ", " << description.category()
			     << ", " << description.levelName()
			     << std::endl;

    return description;
  }

  long long ConfigCatalogue::getKey(const std::string &name)
  {

    std::string tablename = s_configNameTable;

    if (m_verbose) std::cout << "INFO [ConfigCatalogue::getKey] get config name key " << name << "." << std::endl;

    transactionStartReadOnly();
    long long key=0;

    {
      std::unique_ptr<coral::IQuery> query( m_session->nominalSchema().tableHandle(tablename).newQuery() );

      query->addToOutputList(s_columnNames[kNameFk]);

      query->defineOutputType(s_columnNames[kNameFk],coral::AttributeSpecification::typeNameForType<long long>());

      std::string cond = s_configNameTable;
      cond += ".";
      cond += s_columnNames[kName];
      cond += " = :name";

      coral::AttributeList cond_data;
      cond_data.extend<std::string>( "name" );
      query->setCondition( cond, cond_data);
      cond_data["name"].data<std::string>() = name;
      coral::ICursor& cursor = query->execute();

      if ( cursor.next() ) {
	const coral::AttributeList &row = cursor.currentRow();
	key = row[s_columnNames[kNameFk]].data<long long>();
      } else {
	std::stringstream message;
	message<<" ERROR [ConfigCatalogue::getDescription] No config key for " << name << ".";
	throw CAN::MissingConfigKey(message.str());
      }
    }
    transactionCommit();

    return key;
  }

  void ConfigCatalogue::createAuxTables(){
    UMaskChanger umask_changer(0);
    std::string FK_TABLE = s_configKeyTable;
    transactionStartUpdate();

    //   std::cout << "Deleting the old table: " <<  FK_TABLE << std::endl;
    //   m_session->nominalSchema().dropIfExistsTable(FK_TABLE);

    if (!m_session->nominalSchema().existsTable(FK_TABLE)) {
      if (m_verbose) std::cout << "INFO [ConfigCatalogue::createAuxTables] create table " << FK_TABLE << " ."; 
      coral::TableDescription key_columns( "SchemaDefinition_KEY" );
      key_columns.setName( FK_TABLE );
      key_columns.insertColumn( s_columnNames[kKey],
				coral::AttributeSpecification::typeNameForType<long long>());
      key_columns.createIndex (std::string(s_columnNames[kKey])+"_index", s_columnNames[kKey]);
      // Create the actual table
      try {
	m_session->nominalSchema().createTable( key_columns );
	// Fill the first key
	coral::ITableDataEditor& keyeditor = m_session->nominalSchema().tableHandle( FK_TABLE ).dataEditor();
	coral::AttributeList rowBuffer;
	rowBuffer.extend<long long>( s_columnNames[kKey] );
	long long& key = rowBuffer[ s_columnNames[kKey] ].data<long long>();
	key = 1000;
	keyeditor.insertRow( rowBuffer );
      }  catch (coral::TableAlreadyExistingException & ex) {
	//Must have been created in parallel
      }
    }
    transactionCommit();
    if (m_verbose) std::cout << "INFO [ConfigCatalogue::createAuxTables] created table " << FK_TABLE << " ."; 
  }

  long long ConfigCatalogue::updateKey()
  {
    if (m_verbose) std::cout << "INFO [ConfigCatalogue::updateKey] get a new key."; 
    std::string FK_TABLE = s_configKeyTable;
    transactionStartUpdate();
    if (!m_session->nominalSchema().existsTable(FK_TABLE)) {
      createAuxTables();
      transactionStartUpdate();
    }

    coral::ITableDataEditor& keyeditor = m_session->nominalSchema().tableHandle( FK_TABLE ).dataEditor();
    std::string updateAction = s_columnNames[kKey];
    updateAction += " = ";
    updateAction += s_columnNames[kKey];
    updateAction += " + :offset";
    coral::AttributeList updateData;
    updateData.extend<long long>("offset");
    updateData[0].data<long long>() = 1;
    keyeditor.updateRows( updateAction, "", updateData );

    coral::IQuery* query = m_session->nominalSchema().tableHandle(FK_TABLE).newQuery();
    query->addToOutputList(s_columnNames[kKey]); 
    coral::ICursor& cursor = query->execute();
    long long key=0;
    while ( cursor.next() ) {
      const coral::AttributeList &row0 = cursor.currentRow();
      key = row0[0].data<long long>();
      if (m_verbose) std::cout << "INFO [ConfigCatalogue::updateKey] got key " << key << " .";
    }
    transactionCommit();
    return key;
  }

  void ConfigCatalogue::addComment(const std::string &type_name, const std::string &tag_name, Revision_t revision, const std::string &comment)
  {

    if (m_verbose) std::cout << "INFO [ConfigCatalogue::addComment] add description for " << type_name << "." << std::endl;


    long long key=getKey(type_name);

    transactionStartUpdate();

    std::string tablename = s_configCommentTable;

    // Create if not yet there
    if (!m_session->nominalSchema().existsTable(tablename)) {
      createTables();
      transactionStartUpdate();
    }

    coral::ITableDataEditor& config_catalogue_editor = m_session->nominalSchema().tableHandle(tablename).dataEditor();
    coral::AttributeList config_catalogue_row;

    config_catalogue_row.extend<long long>( s_columnNames[kNameFk] );
    config_catalogue_row[ s_columnNames[kNameFk] ].data<long long>() = key;

    if (!tag_name.empty()) {
      config_catalogue_row.extend<std::string>( s_columnNames[kCommentColumnTagName] );
      config_catalogue_row[ s_columnNames[kCommentColumnTagName] ].data<std::string>() = tag_name;
    }
    if (revision>0) {
      config_catalogue_row.extend<Revision_t>( s_columnNames[kCommentColumnRevision] );
      config_catalogue_row[ s_columnNames[kCommentColumnRevision] ].data<Revision_t>() = revision;
    }
    config_catalogue_row.extend<CAN::time_t>( s_columnNames[kCommentColumnDate] );
    config_catalogue_row[ s_columnNames[kCommentColumnDate] ].data<CAN::time_t>() = time(0);

    config_catalogue_row.extend<std::string>( s_columnNames[kCommentColumnCommentText] );
    config_catalogue_row[ s_columnNames[kCommentColumnCommentText] ].data<std::string>() = comment;

    config_catalogue_editor.insertRow(config_catalogue_row);

    transactionCommit();
    if (m_verbose) {
      std::cout << "INFO [ConfigCatalogue::addComment] committed comment for  " << type_name;
      if (!tag_name.empty()){
	std::cout <<  " tag= " << tag_name;
      }
      if (revision>0){
	std::cout <<  " revison= " << revision;
      }	
      std::cout << std::endl;
    }
  }

  void ConfigCatalogue::addConfigDescription(const std::string &name, const std::string &class_name, const std::string &category, ConfigDescription::EConfigLevel level)
  {

    if (m_verbose) std::cout << "INFO [ConfigCatalogue::addConfigDescription] add description for " << name << "." << std::endl;

    // check whether there is already a description
    try {
      /*long long old_key =*/ getKey(name);
      if (m_verbose) std::cout << "INFO [ConfigCatalogue::addConfigDescription] Entry for for " << name << " exsits already nothing added." << std::endl;
      // yes there is 
      return;
    }
    catch(MissingConfigKey &) {
    }
    catch(...) {
    }

    // get a new foreign key
    long long foreign_key = updateKey();

    transactionStartUpdate();
    std::string tablename = s_configCatalogTable;

    // Create if not yet there
    if (!m_session->nominalSchema().existsTable(tablename)) {
      createTables();
      transactionStartUpdate();
    }

    // associate foreign key to name
    {
      coral::ITableDataEditor& config_name_editor = m_session->nominalSchema().tableHandle(s_configNameTable).dataEditor();
      coral::AttributeList config_name_row;
      config_name_row.extend<std::string>( s_columnNames[kName] );
      config_name_row.extend<long long>( s_columnNames[kNameFk] );

      config_name_row[ s_columnNames[kName] ].data<std::string>() = name;
      config_name_row[ s_columnNames[kNameFk] ].data<long long>() = foreign_key;
      config_name_editor.insertRow(config_name_row);
    }

    // now fill in description
    {
      coral::ITableDataEditor& config_catalogue_editor = m_session->nominalSchema().tableHandle(tablename).dataEditor();
      coral::AttributeList config_catalogue_row;
      config_catalogue_row.extend<long long>( s_columnNames[kNameFk] );
      config_catalogue_row.extend<std::string>( s_columnNames[kClassName] );
      config_catalogue_row.extend<std::string>( s_columnNames[kCategory] );
      config_catalogue_row.extend<unsigned short>( s_columnNames[kLevel] );

      config_catalogue_row[ s_columnNames[kNameFk] ].data<long long>() = foreign_key;
      config_catalogue_row[ s_columnNames[kClassName] ].data<std::string>() = class_name;
      config_catalogue_row[ s_columnNames[kCategory] ].data<std::string>() = category;
      config_catalogue_row[ s_columnNames[kLevel] ].data<unsigned short>() = static_cast<unsigned short>(level);

      config_catalogue_editor.insertRow(config_catalogue_row);
    }
    transactionCommit();
    if (m_verbose) std::cout << "INFO [ConfigCatalogue::addConfigDescription] committed description for " << name << "." << std::endl;

  }

  void ConfigCatalogue::getDescription(const std::string &class_name,
				       IDescriptionListener &listener,
				       ConfigDescription::EConfigLevel max_level) 
  {
    std::string tablename = s_configCatalogTable;

    if (m_verbose) std::cout << "INFO [ConfigCatalogue::getDescription] get description for class " << class_name << "." << std::endl;

    transactionStartReadOnly();


    ConfigDescription description;
    {
      std::unique_ptr<coral::IQuery> query( m_session->nominalSchema().newQuery() );

      // Get the list of columns
      //  const coral::ITableDescription& tableDesc =
      //    m_session->nominalSchema().tableHandle(tablename).description();
      //  int NumCol = tableDesc.numberOfColumns();
      //  for(int i=0; i<NumCol; ++i){
      //    const coral::IColumn& colDesc = tableDesc.columnDescription(i);
      //    query->addToOutputList(colDesc.name());
      //if (m_verbose) std::cout <<"	"<<i<<": "<< colDesc.name() << std::endl;
      //  }
      query->addToTableList(s_configCatalogTable);
      query->addToTableList(s_configNameTable);

      query->addToOutputList(s_columnNames[kName]);
      query->addToOutputList(s_columnNames[kClassName]);
      query->addToOutputList(s_columnNames[kCategory]);
      query->addToOutputList(s_columnNames[kLevel]);

      query->defineOutputType(s_columnNames[kName],coral::AttributeSpecification::typeNameForType<std::string>());
      query->defineOutputType(s_columnNames[kClassName],coral::AttributeSpecification::typeNameForType<std::string>());
      query->defineOutputType(s_columnNames[kCategory],coral::AttributeSpecification::typeNameForType<std::string>());
      query->defineOutputType(s_columnNames[kLevel],coral::AttributeSpecification::typeNameForType<unsigned short>());

      std::string cond;
      coral::AttributeList cond_data;
      if (!class_name.empty()) {
	cond += s_configCatalogTable;
	cond += ".";
	cond += s_columnNames[kClassName];
	cond += " = :";
	cond += s_columnNames[kClassName];
	cond += " AND ";
	cond_data.extend<std::string>( s_columnNames[kClassName] );
	cond_data[ s_columnNames[kClassName] ].data<std::string>() = class_name;
      }
      cond += s_configNameTable;
      cond += ".";
      cond += s_columnNames[kNameFk];
      cond += " = ";
      cond += s_configCatalogTable;
      cond +=  ".";
      cond += s_columnNames[kNameFk];

      if (max_level<ConfigDescription::kNLevels) {
	cond += " AND ";
	cond += s_configCatalogTable;
	cond += ".";
	cond += s_columnNames[kLevel];
	cond += " <= :";
	cond += s_columnNames[kLevel];
	cond_data.extend<unsigned short>( s_columnNames[kLevel] );
	cond_data[ s_columnNames[kLevel] ].data<unsigned short>() = max_level;
      }

      query->setCondition( cond, cond_data);

      coral::ICursor& cursor = query->execute();

      while ( cursor.next() ) {
	const coral::AttributeList &row = cursor.currentRow();
	description.setClassName( row[ s_columnNames[kClassName] ].data<std::string>() );
	description.setCategory( row[ s_columnNames[kCategory] ].data<std::string>() );
	description.setLevel( static_cast<ConfigDescription::EConfigLevel>(row[ s_columnNames[kLevel] ].data<unsigned short>()) );
	if (description.level()>=ConfigDescription::kNLevels) {
	  std::stringstream message;
	  message<<" ERROR [ConfigCatalogue::getDescription] Invalid level type for " <<  row[s_columnNames[kName]].data<std::string>() << ".";
	  std::cout << message.str() << std::endl;
	}
	listener.newDescription( row[s_columnNames[kName]].data<std::string>(), description);
      }
    }
    transactionCommit();

    if (m_verbose) std::cout << "INFO [ConfigCatalogue::getDescription] Done with descriptions for " << class_name << " . " << std::endl;

  }

  void ConfigCatalogue::getComments(const std::string &name,
				    const std::string &tag_name,
				    Revision_t revision,
				    ICommentListener &listener,
				    bool top_level_only)
  {


    std::string tablename = s_configCatalogTable;

    if (m_verbose) std::cout << "INFO [ConfigCatalogue::getComments] get comments for " << name 
			     << " " << tag_name << " / " << revision
			     << "." << std::endl;

    transactionStartReadOnly();


    {
      std::unique_ptr<coral::IQuery> query( m_session->nominalSchema().newQuery() );

      // Get the list of columns
      //  const coral::ITableDescription& tableDesc =
      //    m_session->nominalSchema().tableHandle(tablename).description();
      //  int NumCol = tableDesc.numberOfColumns();
      //  for(int i=0; i<NumCol; ++i){
      //    const coral::IColumn& colDesc = tableDesc.columnDescription(i);
      //    query->addToOutputList(colDesc.name());
      //if (m_verbose) std::cout <<"	"<<i<<": "<< colDesc.name() << std::endl;
      //  }
      query->addToTableList(s_configCommentTable);
      query->addToTableList(s_configNameTable);

      query->addToOutputList(s_columnNames[kName]);
      query->defineOutputType(s_columnNames[kName],coral::AttributeSpecification::typeNameForType<std::string>());
      if (tag_name.empty()) {
	query->addToOutputList(s_columnNames[kCommentColumnTagName]);
	query->defineOutputType(s_columnNames[kCommentColumnTagName],coral::AttributeSpecification::typeNameForType<std::string>());
      }
      if (revision==0) {
	query->addToOutputList(s_columnNames[kCommentColumnRevision]);
	query->defineOutputType(s_columnNames[kCommentColumnRevision],coral::AttributeSpecification::typeNameForType<Revision_t>());
      }
      query->addToOutputList(s_columnNames[kCommentColumnDate]);
      query->defineOutputType(s_columnNames[kCommentColumnDate],coral::AttributeSpecification::typeNameForType<CAN::time_t>());

      query->addToOutputList(s_columnNames[kCommentColumnCommentText]);
      query->defineOutputType(s_columnNames[kCommentColumnCommentText],coral::AttributeSpecification::typeNameForType<std::string>());



      coral::AttributeList cond_data;

      std::string cond = s_configNameTable;
      cond += ".";
      cond += s_columnNames[kName];
      cond += " = :";
      cond += s_columnNames[kName];

      cond_data.extend<std::string>( s_columnNames[kName] );
      cond_data[ s_columnNames[kName] ].data<std::string>() = name;

      cond += " AND ";
      cond += s_configNameTable;
      cond += ".";
      cond += s_columnNames[kNameFk];
      cond += " = ";
      cond += s_configCommentTable;
      cond +=  ".";
      cond += s_columnNames[kNameFk];
      if (!tag_name.empty() || top_level_only) {
	cond += " AND ";
	cond += s_columnNames[kCommentColumnTagName];
	cond += " = :";
	cond += s_columnNames[kCommentColumnTagName];

	cond_data.extend<std::string>( s_columnNames[kCommentColumnTagName] );
	cond_data[ s_columnNames[kCommentColumnTagName] ].data<std::string>() = tag_name;
      }

      if (revision > 0 || top_level_only) {
	cond += " AND ";
	cond += s_columnNames[kCommentColumnRevision];
	cond += " = :";
	cond += s_columnNames[kCommentColumnRevision];

	cond_data.extend<Revision_t>( s_columnNames[kCommentColumnRevision] );
	cond_data[ s_columnNames[kCommentColumnRevision] ].data<Revision_t>() = revision;
      }

      query->setCondition( cond, cond_data);
      coral::ICursor& cursor = query->execute();

      while ( cursor.next() ) {
	const coral::AttributeList &row = cursor.currentRow();
	listener.newComment( row[s_columnNames[kName] ].data<std::string>(), 
			     (tag_name.empty() ? row[s_columnNames[kCommentColumnTagName] ].data<std::string>() : tag_name),
			     (revision==0 ? row[s_columnNames[kCommentColumnRevision] ].data<Revision_t>() : revision),
			     row[s_columnNames[kCommentColumnDate] ].data<CAN::time_t>(),
			     row[s_columnNames[kCommentColumnCommentText] ].data<std::string>());
      }
    }
    transactionCommit();

    if (m_verbose) std::cout << "INFO [ConfigCatalogue::getDescription] finished listing comments for  " << name << " : " 
			     << std::endl;


  }


  template <class T>
  bool ConfigCatalogue::updateField(const std::string &name, const std::string &field_name, T new_value) {

    std::string tablename = s_configCatalogTable;

    if (m_verbose) std::cout << "INFO [ConfigCatalogue::addConfigDescription] update field " << field_name << " for " << name << " to " << new_value<< "." << std::endl;


    //@todo replace by combined operation ?
    long long key=getKey(name);

    transactionStartUpdate();

    // Get all entries for scan id
    coral::ITableDataEditor& editor = m_session->nominalSchema().tableHandle(tablename).dataEditor();

    coral::AttributeList row;
    row.extend<long long>(s_columnNames[kNameFk]);
    row.extend<T>(field_name);

    row[s_columnNames[kNameFk] ].data<long long>() = key;
    row[field_name ].data<std::string>() = new_value;
    std::string updatestring = field_name + " = :" + field_name;

    std::string condition = s_columnNames[kNameFk];
    condition += " = :";
    condition += s_columnNames[kNameFk];

    long rowsUpdated = editor.updateRows(updatestring, condition, row);
    if (m_verbose) std::cout << " Updated " << rowsUpdated << " rows" << std::endl;

    transactionCommit();
    if (m_verbose) std::cout << "INFO [ConfigCatalogue::addConfigDescription] updated field for " << name << " to " << new_value<< "." << std::endl;

    return rowsUpdated>0;
  }

  template
  bool ConfigCatalogue::updateField<std::string>(const std::string &name, const std::string &field_name, std::string new_value);

  void ConfigCatalogue::transactionStartReadOnly(){
    if (m_session->transaction().isActive()) m_session->transaction().rollback();
    m_session->transaction().start(true /*ReadOnly*/);
  }

  void ConfigCatalogue::transactionStartUpdate(){
    if (m_session->transaction().isActive()) m_session->transaction().rollback();
    m_session->transaction().start(false /*update*/);
  }

  void ConfigCatalogue::transactionCommit(){
    if (m_session->transaction().isActive()) m_session->transaction().commit();
  }


} //namespace CAN

