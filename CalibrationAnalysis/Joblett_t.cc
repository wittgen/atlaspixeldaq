#include "Joblett_t.hh"


namespace CAN {

  Joblett_t::Joblett_t(SerialNumber_t analysis_serial_number,
		       const PixA::RodLocation &rod_name,
		       const std::string &can_is_server_name)
    : m_results(new ExtendedAnalysisResultList_t),
      m_status(AnalysisStatus::kPending),
      m_abortRequest(false),
      m_ignoreResults(false),
      m_isDone(false)
  {
    std::stringstream is_dictionary_string;
    is_dictionary_string << can_is_server_name << "." << "/A" << std::setw(9) << std::setfill('0') << analysis_serial_number << "/" << rod_name << "/status";
    m_isDictionaryName=is_dictionary_string.str();
  }

}
