#include <ipc/partition.h>
#include <ipc/core.h>
#include "Scheduler.hh"
#include "Common.hh"

#include "LogLevel.hh"
#include <iomanip>

int lower_case ( int c )
{
  return tolower ( c );
}

std::map<std::string,CAN::ELogLevel> s_logLevelNames;

CAN::ELogLevel parseLogLevel(const char *log_level_string)
{
  if (strlen(log_level_string)<1) {
      throw std::runtime_error("Empty log level name.");
  }

  if (isdigit(log_level_string[0])) {
    return static_cast<CAN::ELogLevel>(atoi(log_level_string));
  }
  else {
    if (s_logLevelNames.empty()) {
      s_logLevelNames["silent"]=CAN::kSilentLog;
      s_logLevelNames["quiet"]=CAN::kSilentLog;
      s_logLevelNames["fatal"]=CAN::kFatalLog;
      s_logLevelNames["error"]=CAN::kErrorLog;
      s_logLevelNames["warn"]=CAN::kWarnLog;
      s_logLevelNames["warning"]=CAN::kWarnLog;
      s_logLevelNames["slowinfo"]=CAN::kSlowInfoLog;
      s_logLevelNames["slowinformation"]=CAN::kSlowInfoLog;
      s_logLevelNames["info"]=CAN::kInfoLog;
      s_logLevelNames["information"]=CAN::kInfoLog;
      s_logLevelNames["debug"]=CAN::kDiagnosticsLog;
      s_logLevelNames["debug1"]=CAN::kDiagnostics1Log;
      s_logLevelNames["debug2"]=CAN::kDiagnostics2Log;
      s_logLevelNames["debug3"]=CAN::kDiagnostics3Log;
      s_logLevelNames["debug4"]=CAN::kDiagnostics4Log;
      s_logLevelNames["debug5"]=CAN::kDiagnostics5Log;
      s_logLevelNames["debug6"]=CAN::kAllDiagnosticsLog;
      s_logLevelNames["diagnostics"]=CAN::kDiagnosticsLog;
      s_logLevelNames["diagnostics1"]=CAN::kDiagnostics1Log;
      s_logLevelNames["diagnostics2"]=CAN::kDiagnostics2Log;
      s_logLevelNames["diagnostics3"]=CAN::kDiagnostics3Log;
      s_logLevelNames["diagnostics4"]=CAN::kDiagnostics4Log;
      s_logLevelNames["diagnostics5"]=CAN::kDiagnostics5Log;
      s_logLevelNames["diagnostics6"]=CAN::kAllDiagnosticsLog;
    }
    
    std::string level_name(log_level_string);
    transform ( level_name.begin(), level_name.end(), level_name.begin(), lower_case );
    
    std::map<std::string,CAN::ELogLevel>::const_iterator log_iter = s_logLevelNames.find(level_name);
    if (log_iter ==s_logLevelNames.end()) {
      std::stringstream message;
      message << "Not a known log level name : " << level_name << ".";
      throw std::runtime_error(message.str());
    }
    return log_iter->second;
  }

}

enum ECommand {kQueueStatus,kPing,kReset,kRestart,kLogLevel,kKill, kAbort};

int main(int argc, char **argv) {
  // GET COMMAND LINE

  // Check arguments

  std::vector<ECommand> command_list;
  std::string ipcPartitionName;
  //bool ping_only=false;
  //bool query_queue_states=false;
  //bool reset_queues=false;
  //bool restart_worker=false;
  //bool kill_can=false;

  //bool change_log_level=false;
  //  CAN::ELogLevel log_level;
  std::vector<CAN::ELogLevel> log_level;
  std::vector<CAN::SerialNumber_t> abort_serial_number;

  for (int arg_i=1; arg_i<argc; arg_i++) {

    if (strcmp(argv[arg_i],"-q")==0 || strcmp(argv[arg_i],"--status")==0 || strcmp(argv[arg_i],"-s")==0) {
      //query_queue_states=true;
      command_list.push_back(kQueueStatus);
    }
    else if (strcmp(argv[arg_i],"-a")==0 || strcmp(argv[arg_i],"--ping")==0) {
      //ping_only=true;
      command_list.push_back(kPing);
    }
    else if (strcmp(argv[arg_i],"-r")==0  || strcmp(argv[arg_i],"--reset")==0) {
      //reset_queues=true;
      command_list.push_back(kReset);
    }
    else if (strcmp(argv[arg_i],"-c")==0  || strcmp(argv[arg_i],"--restart-worker")==0) {
      //      restart_worker=true;
      command_list.push_back(kRestart);
    }
    else if ((strcmp(argv[arg_i],"-l")==0  || strcmp(argv[arg_i],"--log-level")==0) && arg_i+1<argc) {
      ++arg_i;
      log_level.push_back( parseLogLevel(argv[arg_i]) );
      // change_log_level=true;
      command_list.push_back(kLogLevel);
    }
    else if (strcmp(argv[arg_i],"-h")==0) {
      ipcPartitionName.clear();
      break;
    }
    else if (strcmp(argv[arg_i],"--kill")==0 || strcmp(argv[arg_i],"-k")==0) {
      //kill_can=true;
      command_list.push_back(kKill);
    }
    else if (strcmp(argv[arg_i],"--abort")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      CAN::SerialNumber_t an_abort_serial_number = atoi(argv[++arg_i]);
      if (an_abort_serial_number==0) {
	std::cerr << "ERROR ["<<argv[0] << ":main] expected analysis serial number after --abort but got " << argv[arg_i] << "." << std::endl;
	return -1;
      }
      abort_serial_number.push_back(an_abort_serial_number);
      command_list.push_back(kAbort);
      break;
    }
    else if (strcmp(argv[arg_i],"-p")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      ipcPartitionName = argv[++arg_i];
    }
    else {
      if (argv[arg_i][0]=='-') {
	std::cerr << "ERROR Unsupported option : " << argv[arg_i] <<"." <<std::endl;
	ipcPartitionName.clear();
	break;
      }
      else {
	ipcPartitionName=argv[arg_i];
      }
    }
  }

  if(ipcPartitionName.empty()) {
    std::cout << "USAGE: kill_manitu [-h]  (-p) \"partition name\"" << std::endl
	      << "                   [-a/--ping] [-q/-s/--status]  " << std::endl
	      << "                   [-r/--reset] [-c/--restart-worker] [--kill/-k] [--abort analysis-id] " <<std::endl
	      << std::endl
	      << "\t-p partition\t\t Set name of the ipc partition. The -p is optional." << std::endl
	      << "\t-a/--ping\t\t\t Contact all workers. Will produce alive messages in MRS." << std::endl
	      << "\t-r/--reset\t\t\t Reset all workers. Will abort jobs, empty queues, unload seal plugins." << std::endl
	      << "\t-c/--restart-worker\t Restart all workers. Will abort jobs, shutdown the worker, which will exit with the return status 13." << std::endl
	      << "\t-k/--kill\t\t\t Shutdown workers and manitu." << std::endl
	      << "\t-q/-s/--status\t\t Get the combined status of  of the worker queues : running or waiting jobs, free slots."
	      << std::endl
	      << "\t-h\t\tShow this help."
	      << std::endl
	      << "The partition name is required all other arguments are optional."
	      << std::endl;
    return EXIT_FAILURE;
  }

  // START IPC

  // Start IPCCore
  IPCCore::init(argc, argv);

  IPCPartition ipcPartition(ipcPartitionName.c_str());

  if (!ipcPartition.isValid()) {
    std::cerr << "ERROR [" << argv[0] << ":main] Not a valid partition " << ipcPartitionName << std::endl;
    return EXIT_FAILURE;
  }

  bool scheduler_comm_error=false;
  try {
  CAN::Scheduler_var manitu = ipcPartition.lookup<CAN::Scheduler>(CAN::s_schedulerName);
  if (!CORBA::is_nil (manitu) ) {
    std::vector<CAN::SerialNumber_t>::const_iterator abort_iter = abort_serial_number.begin();
    std::vector<CAN::ELogLevel>::const_iterator level_iter = log_level.begin();
    for (std::vector<ECommand>::const_iterator command_iter = command_list.begin();
	 command_iter != command_list.end();
	 ++command_iter) {
      switch (*command_iter) {
      case kAbort : {
	if (abort_iter != abort_serial_number.end()) {

	  if (*abort_iter>0) {
	    std::cerr << "INFO [" << argv[0] << ":main] Request abortion of  " << *abort_iter << "." << std::endl;
	    manitu->abortAnalysis(*abort_iter);
	  }
	  ++abort_iter;
	}
	break;
      }
      case kQueueStatus: {
	CORBA::ULong jobs_waiting_for_data;
	CORBA::ULong jobs_waiting;
	CORBA::ULong jobs_running;
	CORBA::ULong empty_slots;
	manitu->getQueueStatus(jobs_waiting_for_data, jobs_waiting, jobs_running, empty_slots);
	std::cout << "INFO [" << argv[0] << ":main] queue status : " << std::endl
		  << "         running = " << std::setw(10) << jobs_running << std::endl
		  << "         waiting = " << std::setw(10) << jobs_waiting << std::endl
		  << "waiting for data = " << std::setw(10) << jobs_waiting_for_data << std::endl
		  << "            free = " << std::setw(10) << empty_slots << std::endl;
	break;
      }
      case kLogLevel: {
	if(level_iter != log_level.end()) {
	  manitu->setLogLevel(*level_iter);
	  ++level_iter;
	}
	break;
      }
      case kPing: {
	manitu->pingWorker();
	break;
      }
      case kReset: {
	manitu->reset();
	break;
      }
      case kRestart: {
	manitu->restartWorker();
	break;
      }
      case kKill: {
	manitu->shutdown();
	break;
      }
      }
    }
  }
  }
  catch (CORBA::TRANSIENT& err) {
    scheduler_comm_error=true;
  }
  catch (CORBA::COMM_FAILURE &err) {
    scheduler_comm_error=true;
  }
  catch (std::exception &err) {
    std::cerr << "ERROR [" << argv[0] << ":main] Caught standard exception " << err.what() << std::endl;
    return EXIT_FAILURE;
  }
  catch (...) {
    scheduler_comm_error=true;
  }
  if (scheduler_comm_error) {
    std::cerr << "ERROR [" << argv[0] << ":main] Failed to communicate with scheduler in partition " << ipcPartitionName << std::endl;
    return EXIT_FAILURE;
  }


}
