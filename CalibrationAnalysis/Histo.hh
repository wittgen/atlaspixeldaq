#ifndef _CAN_Histo_hh_
#define _CAN_Histo_hh_

class TH1;

namespace CAN {
  typedef TH1 Histo;
  
  // defined in DataServer.cc
  unsigned long usedMemory(const Histo *a);
}

#endif
