/* Dear emacs, this is -*-c++-*- */
#ifndef _CAN_PixDbConfigDb_hh_
#define _CAN_PixDbConfigDb_hh_

#include "IConfigDb.hh"
#include <ConfigWrapper/ConfigHandle.h>
#include <map>
#include <set>
#include <stdexcept>

namespace CAN {


  /** Interface for a configuration database
   */
  class PixDbConfigDb : public IConfigDb
  {
  public:
    PixDbConfigDb(const std::string path_name);
    ~PixDbConfigDb();

   std::string configFileName(const std::string &type_name,
			       const std::string &tag_name,
			       Revision_t revision=0);
      
    std::string givepath();
      void create_default_config(Revision_t max_revision, std::string scan_type, std::string class_name, std::string tag_name_flavor);
      void create_copy_config(Revision_t max_revision, std::string scan_type, std::string class_name, std::string tag_name_flavor,const PixA::ConfigHandle &vconfig);
      
    PixA::ConfigHandle config(const std::string &type_name,
			      const std::string &tag_name,
                              Revision_t revision=0);
    PixA::ConfigHandle config_1(const std::string &type_name,
                                const std::string &tag_name,
                                Revision_t revision=0, std::string string="");

    PixA::ConfigHandle config(const std::string &type_name,
			      const std::string &tag_name,
			      Revision_t revision,
			      const std::string &config_name);

    Revision_t addConfig(const std::string &class_name,
			 const std::string &type_name,
			 const std::string &tag_name,
			 const PixA::ConfigHandle &handle);

    Revision_t addConfigList(const std::string &class_name,
			     const std::string &type_name,
			     const std::string &tag_name,
			     const std::vector< std::pair< std::string, PixA::ConfigHandle> > &config_list);

    Revision_t addConfig(const std::string &class_name,
			 const std::string &type_name,
			 const std::string &tag_name,
			 Revision_t revision,
			 const PixA::ConfigHandle &handle);

    Revision_t addConfig(const std::string &class_name,
			 const std::string &type_name,
			 const std::string &tag_name,
			 Revision_t revision,
			 const std::vector< std::pair< std::string, PixA::ConfigHandle> > &config_list);

      
    /** Get the description of a type.
     */
    void getDescription(const std::string &type_name);

    /** Get all descriptions.
     * @param class_name the name of a particular class or an empty string.
     */
    void getDescription(const std::string &class_name, IDescriptionListener &listener);

    /** Get the comment for a particular data set.
     */
    void getComments(const std::string &type_name,
			     const std::string &tag_name,
			     Revision_t revision,
			     ICommentListener &listener);

    std::set<std::string> listTypes(const std::string &class_name, EConfigLevel level)
;

    std::set<std::string> listTypes();

    std::set<std::string> listTags(const std::string &type_name);

    std::set<Revision_t> listRevisions(const std::string &type_name,
				       const std::string &tag_name);

  private:

    void cleanup();

    class Parameter_t {
    public:
      Parameter_t(const PixA::ConfigHandle &handle) : m_used(0) { m_handleList[""]=handle;}

      Parameter_t(const std::vector<std::pair<std::string, PixA::ConfigHandle> > handle_list ) : m_used(0)
      {
	for (std::vector< std::pair<std::string, PixA::ConfigHandle> >::const_iterator handle_iter =  handle_list.begin();
	     handle_iter != handle_list.end();
	     handle_iter++) {
	  m_handleList.insert( *handle_iter );
	}
      }


      bool cleanup()  {
	if (m_used==0) return true;
	else {
	  m_used--;
	  return false;
	}
      }

      PixA::ConfigHandle handle() {
	return handle("");
      }

      const PixA::ConfigHandle handle() const {
	return handle("");
      }
        
      const PixA::ConfigHandle handle_1(const std::string name, std::string string ) const {
    //return handle_1(name, string);
    return handle();
      }

      PixA::ConfigHandle handle(const std::string &name) {
	m_used = s_usedCounter;
	std::map<std::string,PixA::ConfigHandle>::iterator iter = m_handleList.find(name);
	if (iter == m_handleList.end()) return PixA::ConfigHandle();
	return iter->second;
      }

      const PixA::ConfigHandle handle(const std::string &/*name*/) const { 
	return const_cast<Parameter_t *>(this)->handle();
      }

    private:
      std::map<std::string, PixA::ConfigHandle> m_handleList;
      mutable unsigned int m_used;
      static const unsigned int s_usedCounter = 10;
    };

    const Parameter_t *find(const std::string &type_name,
			    const std::string &tag_name,
			    Revision_t revision)
    {
      const std::map<Revision_t, Parameter_t> *tag_param = find(type_name, tag_name);
      if (tag_param && tag_param->size()>0) {
	if (revision==0) {
	  return &(tag_param->rend()->second);
	}
	else {
	  std::map<Revision_t, Parameter_t>::const_iterator revision_iter = tag_param->lower_bound(revision);
	  if (revision_iter != tag_param->end()) {
	    return &(revision_iter->second);
	  }
	}
      }
      return 0;
    }


    const std::map<Revision_t, Parameter_t> *find(const std::string &type_name,
						  const std::string &tag_name) 
    {
      std::map<std::string, std::map< std::string, std::map<Revision_t, Parameter_t> > >::const_iterator type_iter =  m_cache.find(type_name);
      if (type_iter != m_cache.end()) {
	std::map< std::string, std::map<Revision_t, Parameter_t> >::const_iterator tag_iter = type_iter->second.find(tag_name);
	if ( tag_iter != type_iter->second.end()) {
	  return &(tag_iter->second);
	}
      }
      return NULL;
    }

    void addParameter(const std::string &type_name,
		      const std::string tag_name,
		      Revision_t revision,
		      const std::vector< std::pair< std::string, PixA::ConfigHandle> > &config_list) 
    {
      if (type_name.empty() || tag_name.empty() || revision==0) {
	throw std::logic_error("Invalid name, tag or revision for scan parameters");
      }
      m_cache[type_name][tag_name].insert(std::make_pair(revision, Parameter_t(config_list)));
    }
      
    std::string m_pathName;
    std::string m_catalogueName;

    std::map<std::string, std::map< std::string, std::map<Revision_t, Parameter_t> > > m_cache;
    static const unsigned int s_maxConfigs = 30;
  };

}
#endif

