#include <memory>

#include "DataServer.hh"
#include <PixModule/PixModule.h>
#include <ConfigWrapper/PixConfigWrapperTopLevel.h>
#include "ConfigWrapper/ConfObjUtil.h"
#include <PixBoc/PixBoc.h>
#include "sendMessage.hh"

#include "TH1.h"
#include "Flag.hh"

namespace CAN {

  unsigned long usedMemory(const Histo *a) {
    return a->GetNbinsX()*a->GetNbinsY()*8;
  }

  DataServer::~DataServer() {

    // cleanup 
    m_histoList.clear();
    m_bocConfig.clear();
    m_modConfig.clear();
    m_fullModuleGroupConfig.reset();

    //    std::cout << "INFO [DataServer::dtor] current memory usage "  <<  AccountingHistoPtr_t::usedMemoryTotal() << std::endl;
    // now check memory 
    if (memoryUsageBelowThreshold()) {
      m_dataRequest->setFlag();
    }
  }


  void DataServer::requestModuleConfig(const std::string &module_conn_name)
  {
    assert(m_takesRequests);
    m_configRequestFlags |= kModuleConfig;

    // only request configs once.
    if (std::find(m_modConfigList.begin(),m_modConfigList.end(), module_conn_name)==m_modConfigList.end()) {
std::cout<<"grazie al cielo entro qui"<<module_conn_name<<std::endl;
      m_modConfigList.push_back(module_conn_name);
    }
  }

  void DataServer::addModuleGroupConfig(std::shared_ptr<const PixLib::PixModuleGroupData_t> &module_group)
  {
    assert( module_group.get());

    if (!const_cast<PixLib::PixModuleGroupData_t &>(*module_group).getPixBoc()) {
      std::stringstream message;
      message << "No PixBoc in module group " << const_cast<PixLib::PixModuleGroupData_t &>(*module_group).getName()
	      << " for " << makeScanSerialNumberString(scanSerialNumber()) << ".";
      //      throw MRSException("NOBOC", MRS_FATAL, "CAN::DataServer::addModuleGroupConfig",message.str());
      throw MRSException(PixLib::PixMessages::FATAL, "CAN::DataServer::addModuleGroupConfig",message.str());
    }

    m_fullModuleGroupConfig=module_group;
    PixA::ConfigHandle boc_config( *(new PixA::PixConfigWrapperTopLevel<const PixLib::PixModuleGroupData_t>(module_group,
												      *(const_cast<PixLib::PixModuleGroupData_t &>(*module_group).getPixBoc()->getConfig()))) );
    addBocConfig(boc_config);
  }

  const PixLib::Config &DataServer::getBocConfig() const {
    PixLib::PixModuleGroup *const_casted_module_group = const_cast<PixLib::PixModuleGroup *>(m_fullModuleGroupConfig.get());
    if (const_casted_module_group && const_casted_module_group->getPixBoc() && const_casted_module_group->getPixBoc()->getConfig()) {
      return *(const_casted_module_group->getPixBoc()->getConfig());
    }
    if (m_bocConfig.size()>0 ) {
      return (m_bocConfig.back().config());
    }
    throw PixA::ConfigException("No boc config.");
    }


  std::shared_ptr<PixLib::PixModuleData_t> DataServer::createConfiguredModule(const std::string &module_conn_name) const
  {
    const PixLib::Config &mod_config = getModuleConfig(module_conn_name);
    std::string module_name = mod_config.name();
    std::string::size_type tail_pos=module_name.find("_");
    if (tail_pos != std::string::npos && tail_pos>0) {
      module_name = module_name.substr(0,tail_pos);
    }

    int const mccFlv = confVal<int>(mod_config["general"]["MCC_Flavour"]);
    int const feFlav = confVal<int>(mod_config["general"]["FE_Flavour"]);
    int nFE= 16;
    if(mccFlv==PixLib::PixModule::PM_NO_MCC && (feFlav == PixLib::PixModule::PM_FE_I4 ))nFE=2;

    //std::cout<<module_name<<" "<<" "<<mccFlv<<" "<<feFlav<<" "<<nFE<<std::endl;

    auto obj = std::make_shared <PixLib::PixModule> (nullptr, nullptr, "", "", module_name, mccFlv, feFlav, nFE );
    obj->config() = mod_config;
    obj->setModuleGeomConnName(module_conn_name);
    return  obj;
  }

  std::shared_ptr<PixLib::PixModuleGroupData_t> DataServer::createConfiguredModuleGroup() const
  {

    if (!m_fullModuleGroupConfig.get()) {
      std::stringstream message;
      message << "Missing PixModuleGroup for " << makeScanSerialNumberString(scanSerialNumber()) << ".";
      //      throw MRSException("NOBOC", MRS_FATAL, "CAN::DataServer::createConfiguredModuleGroup",message.str());
      throw MRSException(PixLib::PixMessages::FATAL, "CAN::DataServer::createConfiguredModuleGroup",message.str());
    }

    auto obj = std::make_shared<PixLib::PixModuleGroup> ( const_cast<PixLib::PixModuleGroupData_t&> (*m_fullModuleGroupConfig).getName(), const_cast<PixLib::PixModuleGroupData_t &>(*m_fullModuleGroupConfig).getCtrlType() );

    if (!obj->getPixBoc() || !const_cast<PixLib::PixModuleGroupData_t &>(*m_fullModuleGroupConfig).getPixBoc()) {
      std::stringstream message;
      message << "No PixBoc in module group " << const_cast<PixLib::PixModuleGroupData_t &>(*m_fullModuleGroupConfig).getName()
	      << " for " << makeScanSerialNumberString(scanSerialNumber()) << ".";
      //      throw MRSException("NOBOC", MRS_FATAL, "CAN::DataServer::createConfiguredModuleGroup",message.str());
      throw MRSException(PixLib::PixMessages::FATAL, "CAN::DataServer::createConfiguredModuleGroup",message.str());
    }
    if (!obj->getPixController()  || !const_cast<PixLib::PixModuleGroupData_t &>(*m_fullModuleGroupConfig).getPixController()) {
      std::stringstream message;
      message << "No PixController in module group " << const_cast<PixLib::PixModuleGroupData_t &>(*m_fullModuleGroupConfig).getName()
	      << " for " << makeScanSerialNumberString(scanSerialNumber()) << ".";
      //      throw MRSException("NOBOC", MRS_FATAL, "CAN::DataServer::createConfiguredModuleGroup",message.str());
      throw MRSException(PixLib::PixMessages::FATAL, "CAN::DataServer::createConfiguredModuleGroup",message.str());
    }

    // copy the configurations
    obj->config() = const_cast<PixLib::PixModuleGroupData_t &>(*m_fullModuleGroupConfig).config();

    if (obj->getPixBoc()->getConfig() &&  const_cast<PixLib::PixModuleGroupData_t &>(*m_fullModuleGroupConfig).getPixBoc()->getConfig()) {
      *obj->getPixBoc()->getConfig() = *const_cast<PixLib::PixModuleGroupData_t &>(*m_fullModuleGroupConfig).getPixBoc()->getConfig();
    }
    else if (obj->getPixBoc()->getConfig() || const_cast<PixLib::PixModuleGroupData_t &>(*m_fullModuleGroupConfig).getPixBoc()->getConfig()) {
      std::stringstream message;
      message << "Missing config object for PixBoc in module group " << const_cast<PixLib::PixModuleGroupData_t &>(*m_fullModuleGroupConfig).getName()
	      << " for " << makeScanSerialNumberString(scanSerialNumber()) << ".";
      //      throw MRSException("NOBOC", MRS_FATAL, "CAN::DataServer::createConfiguredModuleGroup",message.str());
      throw MRSException(PixLib::PixMessages::FATAL, "CAN::DataServer::createConfiguredModuleGroup",message.str());
    }

    obj->getPixController()->config() = const_cast<PixLib::PixModuleGroupData_t &>(*m_fullModuleGroupConfig).getPixController()->config();

    return obj;
  }

}
