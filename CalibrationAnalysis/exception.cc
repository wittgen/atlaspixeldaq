#include <exception.hh>

namespace CAN {

  //    void MRSException::sendMessage(MRSStream &m_out, const std::string &caught_in)
    void MRSException::sendMessage()
    {
//       m_out << m_messageId
// 	    << m_severity
// 	    << MRS_OPT_PARAM<const char *>("caught-in","[%s]", caught_in.c_str() )
// 	    << MRS_OPT_PARAM<const char *>("method","[%s]", m_functionName.c_str() );
//       if (!m_connName.empty()) {
// 	m_out << MRS_QUALIF(m_connName.c_str());
//       }
//       m_out << MRS_TEXT(getDescriptor())
// 	    << ENDM;
      PixLib::PixMessages msg;
      msg.publishMessage(m_severity, m_functionName, getDescriptor());
    }

}
