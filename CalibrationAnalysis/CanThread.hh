#ifndef _CAN_Thread_hh_
#define _CAN_Thread_hh_

#include <unistd.h>

namespace CAN {
  class CanThread : public omni_thread 
  {
  public :
    CanThread() : m_shutdown(false) {}
    ~CanThread() { shutdownWait() ;}

    void start() { /*m_start.setFlag(); */ 
      if (state()!=omni_thread::STATE_RUNNING) {     
	start_undetached(); 
      }
    }

    void initiateShutdown() {
      m_shutdown=true;
      m_exit.setFlag();
    }

    void shutdownWait() {
      initiateShutdown();
      if (state()==omni_thread::STATE_RUNNING) {
	m_exit.wait();
	while (state()!=omni_thread::STATE_TERMINATED) {
	  usleep(1000);
	}
      }
    }

    bool shutdownRequested() const {return m_shutdown; }
    
  protected:
    virtual void wakeupForShutdown() = 0;

    /** the thread main method
     *<pre>
     * void main() {
     *    for(;!shutdownRequested();) {
     *       ...
     *       if (shutdownRequested()) break;
     *       if (resetRequested()) {
     *           ...
     *          if (shutdownRequested()) break;
     *          clearResetFlag();
     *       }
     *       ...
     *    }
     * }
     *</pre>
     */
    virtual void main() = 0;

    bool &shutdownFlag()     { return m_shutdown; }
    const bool &shutdownFlag() const { return m_shutdown; }

    void *run_undetached(void *) {
      //      m_start.setFlag();
      main();
      m_exit.setFlag();
      return NULL;
    }

  private:
    //    Flag m_start;
    Flag m_exit;

  protected:
    bool m_shutdown;
  };

}
#endif
