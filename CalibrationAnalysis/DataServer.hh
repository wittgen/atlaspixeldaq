#ifndef _CAN_DataServer_hh_
#define _CAN_DataServer_hh_

#include <string>
  // @todo remove once the CAN::Histo type is used instead of the PixLib::Histo
#include <list>
#include <cassert>

#include "IDataServer.hh"
#include <ConfigWrapper/PixDisableUtil.h>
#include "HistoArray.hh"
#include <ScanInfo_t.hh>
#include <IMetaDataService.hh>

namespace PixA {
  class ConnectivityRef;
   // @todo remove once the CAN::Histo type is used instead of the PixLib::Histo
  class HistoHandle;
}

namespace PixLib {

  class Config;
  class ConfigObj;
}

namespace CAN {

  class AnalysisDataServer;
  class Flag;

  class ConfigBuffer_t {
  public:
    ConfigBuffer_t(const PixA::ConfigHandle &config_handle)
      : m_handle(config_handle),
	m_configRef(config_handle.ref().createFullCopy())
    {
      m_handle=PixA::ConfigHandle(m_configRef);
      m_config=&m_configRef.config();
    }

    ConfigBuffer_t(const ConfigBuffer_t &a)
      : m_handle ( a.m_handle),
	m_configRef( m_handle.ref()),
	m_config(&m_configRef.config() )
    {
    }

    const PixLib::Config &config() const {
      return *m_config;
    }

  private:
    PixA::ConfigHandle m_handle;
    PixA::ConfigRef    m_configRef;
    const PixLib::Config    *m_config;
   };


  class HistoArrayList_t
  {
  public:

    HistoArrayList_t() : m_completed(false) {}

    ~HistoArrayList_t() {
      clear();
    }

    void clear()
    {
      for(std::map< std::string, std::map<std::string, TopLevelHistoArray *> >::iterator histo_name_iter = m_histoList.begin();
	  histo_name_iter != m_histoList.end();
	  histo_name_iter++) {
	for(std::map<std::string, TopLevelHistoArray *>::iterator conn_iter = histo_name_iter->second.begin();
	    conn_iter != histo_name_iter->second.end();
	    conn_iter++) {
	  delete conn_iter->second;
	}
      }
      m_histoList.clear();
    }

    /** To fill the HistoArrayList once a scan has finished.
     * First index histogram name, second index connectivity name, 
     */
    std::map<std::string, std::map<std::string, TopLevelHistoArray *> >::iterator begin()           { return m_histoList.begin(); }
    std::map<std::string, std::map<std::string, TopLevelHistoArray *> >::const_iterator end() const { return m_histoList.end(); }
    std::map<std::string, std::map<std::string, TopLevelHistoArray *> >::iterator find(std::string key)  { return m_histoList.find(key); }

    void addHistoArray(const std::string &histogram_name, const std::string &connectivity_name) {
      std::map<std::string, TopLevelHistoArray *> &histo_map = m_histoList[histogram_name];
      std::map<std::string, TopLevelHistoArray *>::iterator histo_iter = histo_map.find(connectivity_name);
      if (histo_iter == histo_map.end()) {
	histo_map.insert(std::make_pair(connectivity_name, static_cast<TopLevelHistoArray *>(NULL)));
      }
    }

    void numberOfHistos(const std::string &histogram_name, const std::string &connectivity_name, HistoInfo_t &info) const {
      const TopLevelHistoArray *histo_ptr = getHistoArray(histogram_name, connectivity_name);
      if (histo_ptr && histo_ptr->hasValidInfo()) {
	info = histo_ptr->info();
	return;
      }
      throw HistoException("No information about array dimensions.");
    }

    //@todo throw an exception rather than returning zero.
    const HistoPtr_t histo(const std::string &histogram_name, const std::string &connectivity_name, const PixA::Index_t &index) const {
      const TopLevelHistoArray *histo_ptr = getHistoArray(histogram_name, connectivity_name);
      if (histo_ptr) {
	return histo_ptr->histo(index);
      }
      return NULL;
    }

    const TopLevelHistoArray *getHistoArray(const std::string &histogram_name, const std::string &connectivity_name) const {
      std::map<std::string, std::map<std::string, TopLevelHistoArray *> >::const_iterator histo_iter = m_histoList.find(histogram_name);
      if (histo_iter == m_histoList.end()) return NULL;

      std::map<std::string, TopLevelHistoArray *>::const_iterator conn_iter = histo_iter->second.find(connectivity_name);
      if (conn_iter == histo_iter->second.end()) return NULL;
      return conn_iter->second;
    }

    void setCompleted()    { m_completed=true; }
    bool completed() const { return m_completed; }

  private:
    bool m_completed;
    std::map<std::string, std::map<std::string, TopLevelHistoArray *> > m_histoList;
  };

  // @todo uncomment when Histo is unsed instead of PixLib::Histo
  // class Histo;
  class ExecutionUnit;
  class AverageResult_t;
  class MasterDataServer;
  class MasterDataServerCT;

  /** Server of histograms, configurations and the connecitivity.
   * The server will automatically provide the connectivity and the scan configuration.
   * Histograms and module or boc configurations etc. need to be requested before they
   * can be retrieved from the server.
   */
  class DataServer : public IDataServer
  {


  private:

    enum EGetConfigs {kScanConfig=1, kModuleConfig=2, kBocConfig=4, kModuleGroupConfig=8};
    friend class MasterDataServer;
    friend class MasterDataServerCT;

  public:
    DataServer(SerialNumber_t scan_serial_number,
	       const InfoPtr_t<PixLib::ScanInfo_t> &scan_info,
	       const SerialNumber_t src_analysis_serial_number,
	       const std::shared_ptr<Flag> &data_request)
      : m_parent(NULL),
        m_dataRequest(data_request),
	m_scanSerialNumber(scan_serial_number),
        m_scanInfo(scan_info),
        m_srcAnalysisSerialNumber(src_analysis_serial_number),
	m_configRequestFlags(0),
	m_takesRequests(true),
	m_abort(false)
    { if (src_analysis_serial_number==0) requestScanConfig(); }

    ~DataServer();

    // methods to request resources
    void requestHistogram(const std::string &histogram_name,
			  const std::string &conn_name) {
      assert(m_takesRequests);
      m_histoList.addHistoArray(histogram_name, conn_name);
    }

    /** Request a tagged analysis summary value.
     * A variable with a certain name and for a connectivity name can only requested for one tag per data server.
     */
    // @todo implement me
    void requestUIntAnalysisResult(const std::string &tag_name,
				   Revision_t &revision,
				   const std::string &conn_name,
				   const std::string &variable_name) { assert(m_takesRequests);  assert(false); }

    // @todo implement me
    void requestBoolAnalysisResult(const std::string &tag_name,
				   Revision_t &revision,
				   const std::string &conn_name,
				   const std::string &variable_name) { assert(m_takesRequests); assert(false); }

    // @todo implement me
    void requestAvAnalysisResult(const std::string &tag_name,
				 Revision_t &revision,
				 const std::string &conn_name,
				 const std::string &variable_name) { assert(m_takesRequests); assert(false); }

    void requestBocConfig()  { assert(m_takesRequests); m_configRequestFlags |= kBocConfig; }

    void requestScanConfig() { assert(m_takesRequests); m_configRequestFlags |= kScanConfig; }

    void requestModuleConfig(const std::string &module_conn_name);

    void requestModuleGroupConfig() { assert(m_takesRequests); m_configRequestFlags |= kModuleGroupConfig; }

    //     void requestModuleConfigObject(const std::string &conn_name,
    // 				   const std::string &config_group_name,
    // 				   const std::string &config_object_name) = 0;

    //     void requestModuleConfigObject(const std::string &conn_name,
    // 				   const std::vector<std::string> &sub_config_and_group_name,
    // 				   const std::string &config_object_name) = 0;

    // methods to retrieve resources
    // resources are generally only available if they have been requested before hand

    /** Get the pix scan histogram of the given name and for the given connectivity object.
     * @param histogram_type_name the histogram name.
     * @param conn_name the name of the connectiivty object.
     * @param info will be filled with dimensions in each level of the pix scan histo.
     * @sa getHistogram.
     * @throw HistoException
     */
    void numberOfHistograms(const std::string &histogram_name,
			    const std::string &conn_name,
			    HistoInfo_t &info) const 
    {
      m_histoList.numberOfHistos(histogram_name, conn_name, info);
    }

    /** Get the pix scan histogram of the given name and for the given connectivity object.
     * @param histogram_name the histogram name.
     * @param conn_name the name of the connectiivty object.
     * @param index an array of indices which specifies the location in the pix-scan histo array.
     * @return const pointer to the histogram or NULL.
     * This method may return NULL in case the specified histogram does not exist (but also,
     * if the histogram was not requested). In case the indices specified in index do not exceed
     * the index range which can be queried with @ref numberOfHistograms, the histogram should
     * exist in general. Howerve, this may not always be the case, In particular cases, the maximum
     * index range may depend on a particular branch of the pix scan histogram array. In the latter
     * case the information returned by numberOfHistograms corresponds to the union of all branches.
     * So, depending on the particular case a missing histogram may or may not be an error.
     * @sa numberOfHistograms. 
     */
    const Histo *getHistogram(const std::string &histogram_name,
			      const std::string &conn_name,
			      const PixA::Index_t &index) const {
      return m_histoList.histo(histogram_name, conn_name, index);
    }

    std::vector<std::string>  availableHistograms( const std::string &conn_name ){
      //I can't get a list of histograms here, I need to wait until I have
      //a Historeceiver avaiable, probably in MasterDataServer.
      //Returning an empty map for now.
      //we also need to put a token inside of the histolist
      //so that we know to fill it later on
      std::vector<std::string> dummy_map;
      m_histoList.addHistoArray("All", conn_name);
      return dummy_map;
    }

    std::vector<std::string> dataServerHistos (const std::string &conn_name = "dummy"){
      std::map<std::string, std::map<std::string, TopLevelHistoArray *> >::iterator histo_iter;
      std::vector<std::string> scanNames;
      for(histo_iter = m_histoList.begin(); histo_iter != m_histoList.end() ; histo_iter++){
	scanNames.push_back(histo_iter->first);
      }
      return scanNames;
    }

    void toggleRequests(bool toggle){ m_takesRequests = toggle;}


    //@todo to be implemented
    unsigned int getUIntAnalysisResult(const std::string &conn_name,
				       const std::string &variable_name) const { assert(false); }

    //@todo to be implemented
    unsigned int getBoolAnalysisResult(const std::string &conn_name,
				       const std::string &variable_name) const { assert(false); }

    //@todo to be implemented
    const AverageResult_t &getAvAnalysisResult(const std::string &conn_name,
					       const std::string &variable_name) const { assert(false); }

    // @todo will be changed such that the data object is returned instead
    // of the config object
    const PixLib::Config &getScanConfig() const { 
      if (m_scanConfig.size()>0 ) {
	return m_scanConfig.back().config();
      }
      std::cout << "DataServer::getScanConfig   NO SCANCONFIG  -> throwing execption." << std::endl;
      throw PixA::ConfigException("No scan config.");
    }

    // @todo will be changed such that the data object is returned instead
    // of the config object
    const PixLib::Config &getBocConfig() const;

    // @todo will be changed such that the data object is returned instead
    // of the config object
    const PixLib::Config &getModuleConfig(const std::string &module_conn_name) const {
      std::map<std::string, ConfigBuffer_t >::const_iterator mod_config_iter = m_modConfig.find(module_conn_name);
        //std::cout<<"getModuleConfig 2.2"<<m_modConfig.end()->first<<std::endl;
      if ( mod_config_iter != m_modConfig.end() ) {
	return mod_config_iter->second.config();
      }
      throw PixA::ConfigException("No module config.");
    }


    std::shared_ptr<PixLib::PixModuleData_t> createConfiguredModule(const std::string &module_conn_name) const;

    std::shared_ptr<PixLib::PixModuleGroupData_t> createConfiguredModuleGroup() const;


//     const PixLib::ConfigObj &getModuleConfigObject(const std::string &module_conn_name,
// 							   const std::vector<std::string> &sub_config_and_group_name,
// 							   const std::string &config_object_name) const = 0;

//     const PixLib::ConfigObj &getModuleConfigObject(const std::string &module_conn_name,
// 							   const std::string &config_group_name,
// 							   const std::string &config_object_name) const = 0;

    /** Get a helper object which allows to iterator over all enabled Pp0s and modules.
     */
    const PixA::DisabledList &getPp0DisabledList() const { return m_pp0DisabledList; }

    /** Return true if the scan configuration is needed.
     */
    bool needScanConfig() const { return m_configRequestFlags & kScanConfig; }

    /** Return true if the BOC configuration is needed.
     */
    bool needBocConfig() const { return m_configRequestFlags & kBocConfig; }

    /** Return true if the BOC configuration is needed.
     */
    bool needPixModuleGroupConfig() const { return m_configRequestFlags & kModuleGroupConfig; }

    /** Return true if module configurations are needed.
     * @sa modConfigBegin, modConfigEnd
     */
    bool needModuleConfig() const { return m_configRequestFlags & kModuleConfig; }

    /** Begin iterator of the module configuration requests.
     */
    std::vector<std::string>::const_iterator modConfigBegin() const { return m_modConfigList.begin(); }

    /** End iterator of the module configuration requests.
     */
    std::vector<std::string>::const_iterator modConfigEnd() const   { return m_modConfigList.end(); }

    /** Add the scan configuration to the data server.
     */
    void addScanConfig(PixA::ConfigHandle handle)
    {
      if (!handle) return;
      //@todo in principle the pointer to the config object should be const, though non of the methods of the config
      // object are actualy const.
      m_scanConfig.push_back(ConfigBuffer_t(handle));
    }

    /** Add the BOC configuration to the data server
     */
    void addBocConfig(PixA::ConfigHandle &handle)
    { 
      if (!handle) return;
      //@todo in principle the pointer to the config object should be const, though non of the methods of the config
      // object are actualy const.
      m_bocConfig.push_back( ConfigBuffer_t(handle));
    }

    /** Add the full pix module group configuration to the data server
     */
    void addModuleGroupConfig(std::shared_ptr<const PixLib::PixModuleGroupData_t> &module_group);

    /** Add the module configuration of the specified module to the data server
     */
    void addModuleConfig(const std::string &module_name, PixA::ConfigHandle &handle)
    {
      if (!handle) return;
      //      assert( m_modConfig.find(module_name) == m_modConfig.end());

      //@todo in principle the pointer to the config object should be const, though non of the methods of the config
      // object are actualy const.
      std::pair< std::map<std::string, ConfigBuffer_t >::iterator , bool >
	ret = m_modConfig.insert( std::make_pair( module_name , ConfigBuffer_t(handle) ));
      assert( ret.second );
    }

    std::map<std::string, std::map<std::string, TopLevelHistoArray *> >::iterator histoBegin()           { return m_histoList.begin(); }
    std::map<std::string, std::map<std::string, TopLevelHistoArray *> >::const_iterator histoEnd() const { return m_histoList.end(); }


  protected:
    void closeRequests() { m_takesRequests=false; }

    SerialNumber_t scanSerialNumber() const { return m_scanSerialNumber; }

    const PixLib::ScanInfo_t &scanInfo() const { return *m_scanInfo; }

    void setDisabledList(const PixA::DisabledListRoot &pp0_disabled_list_root) { m_pp0DisabledList = pp0_disabled_list_root; }

    bool abort() const { return m_abort; }
    void setAbort()    { m_abort=true; }

    void setParent(AnalysisDataServer *parent) { m_parent=parent;}
    AnalysisDataServer &parent() { assert(m_parent); return *m_parent; }

    static bool memoryUsageBelowThreshold() {
      return AccountingHistoPtr_t::usedMemoryTotal() < s_memoryUsageLowerThreshold;
    }

    static bool memoryUsageAboveThreshold() {
      return AccountingHistoPtr_t::usedMemoryTotal() > s_memoryUsageUpperThreshold;
    }

  private:
    AnalysisDataServer           *m_parent;
    std::shared_ptr<Flag>       m_dataRequest;
    
    SerialNumber_t                m_scanSerialNumber;
    InfoPtr_t<PixLib::ScanInfo_t> m_scanInfo;
    SerialNumber_t                m_srcAnalysisSerialNumber;

    HistoArrayList_t        m_histoList;

    unsigned int            m_configRequestFlags;

    // Presumably will be changed 
    std::vector<std::string>  m_modConfigList;
    std::map<std::string, ConfigBuffer_t >    m_modConfig;

    // overkill but easy, will be filled with at most one element.
    std::vector< ConfigBuffer_t >                         m_bocConfig;
    std::vector< ConfigBuffer_t >                         m_scanConfig;
    std::shared_ptr<const PixLib::PixModuleGroupData_t> m_fullModuleGroupConfig;

    PixA::DisabledListRoot m_pp0DisabledList;

    bool m_takesRequests;
    bool m_abort;

    static const unsigned long s_memoryUsageLowerThreshold = 1300*1024*1024L;  
    static const unsigned long s_memoryUsageUpperThreshold = 1400*1024*1024L;
  };

}

#endif
