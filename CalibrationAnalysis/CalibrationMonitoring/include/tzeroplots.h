#ifndef PIX_HISTORY_PLOT_TZERO_H
#define PIX_HISTORY_PLOT_TZERO_H

#include "def.h"
#include "plotutil.h"
#include "pixhistoryplotparam.h"

// DEFINITION

const String PIX_TZERO_MEAN          = "TOT_MEAN";
const String PIX_TZERO_MAP_MODE_MEAN = "MeanMap";

// -------------------------------------------------------
// PROTOTYPE DECLARATION
// -------------------------------------------------------

bool CreateTzeroPlot             ( PixFileHistPair*         pPair,
                                   PixHistoryPlotParameter* pParam );

bool Tzero_LoadFile              ( PixStructType            structType,
                                   PixFileHistPair*         pPair,
                                   PixHistoryPlotParameter* pParam,
                                   TDirectory*              pDir );

bool Tzero_LoadModuleForPixel    ( TDirectory*              pDir,
                                   PixFileHistPair*         pPair,
                                   PixHistoryPlotParameter* pParam,
                                   const String&            slotFolder,
                                   PixStructType            structType );
bool Tzero_LoadModuleForIBL      ( TDirectory*              pDir,
                                   PixFileHistPair*         pPair,
                                   PixHistoryPlotParameter* pParam,
                                   const String&            slotFolder,
                                   PixStructType            structType );

bool Tzero_LoadTotHist           ( TDirectory*              pDir,
                                   PixFileHistPair*         pPair,
                                   PixHistoryPlotParameter* pParam,
                                   PixStructType            structType,
                                   const String&            totMode,
                                   const String&            mapMode );

bool Tzero_FillHistsForPix       ( PixFileHistPair*         pPair,
                                   PixHistoryPlotParameter* pParam,
                                   const String&            structTypeStr,
                                   int                      col,
                                   int                      row,
                                   double                   val );

bool Tzero_FillHistsForIBL       ( PixFileHistPair*         pPair,
                                   PixHistoryPlotParameter* pParam,
                                   const String&            feName,
                                   int                      col,
                                   int                      row,
                                   double                   val );

#endif // PIX_HISTORY_PLOT_TZERO_H
