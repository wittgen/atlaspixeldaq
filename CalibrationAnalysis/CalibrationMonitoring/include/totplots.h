#ifndef PIX_HISTORY_PLOT_TOT_H
#define PIX_HISTORY_PLOT_TOT_H

#include "def.h"
#include "plotutil.h"
#include "pixhistoryplotparam.h"

// DEFINITION

const String PIX_TOT_MEAN          = "TOT_MEAN";
const String PIX_TOT_MAP_MODE_MEAN = "MeanMap";

// -------------------------------------------------------
// PROTOTYPE DECLARATION
// -------------------------------------------------------

bool CreateTotPlot             ( PixFileHistPair*         pPair,
                                 PixHistoryPlotParameter* pParam );

bool TOT_LoadFile              ( PixStructType            structType,
                                 PixFileHistPair*         pPair,
                                 PixHistoryPlotParameter* pParam,
                                 TDirectory*              pDir );

bool TOT_LoadModuleForPixel    ( TDirectory*              pDir,
                                 PixFileHistPair*         pPair,
                                 PixHistoryPlotParameter* pParam,
                                 const String&            slotFolder,
                                 PixStructType            structType );
bool TOT_LoadModuleForIBL      ( TDirectory*              pDir,
                                 PixFileHistPair*         pPair,
                                 PixHistoryPlotParameter* pParam,
                                 const String&            slotFolder,
                                 PixStructType            structType );

bool TOT_LoadTotHist           ( TDirectory*              pDir,
                                 PixFileHistPair*         pPair,
                                 PixHistoryPlotParameter* pParam,
                                 PixStructType            structType,
                                 const String&            totMode,
                                 const String&            mapMode );

bool TOT_FillHistsForPix       ( PixFileHistPair*         pPair,
                                 PixHistoryPlotParameter* pParam,
                                 const String&            structTypeStr,
                                 int                      col,
                                 int                      row,
                                 double                   val );

bool TOT_FillHistsForIBL       ( PixFileHistPair*         pPair,
                                 PixHistoryPlotParameter* pParam,
                                 const String&            feName,
                                 int                      col,
                                 int                      row,
                                 double                   val );

#endif // PIX_HISTORY_PLOT_TOT_H
