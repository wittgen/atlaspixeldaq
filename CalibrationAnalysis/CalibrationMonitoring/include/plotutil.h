#ifndef PIX_HISTORY_PLOT_UTIL_H
#define PIX_HISTORY_PLOT_UTIL_H

#include "TH1.h"

// DEFINITION
const String PIX_ROD_PREFIX                       = "ROD_";
const String PIX_BARREL_HDR                       = "B";
const String PIX_DISK_HDR                         = "D";
const String PIX_IBL_HDR                          = "I";
const String PIX_LAYER_HDR                        = "L";
const String PIX_BISTAVE_HDR                      = "B";
const String PIX_SLOT_HDR                         = "S";
const String PIX_STAVE_HDR                        = "S";
const String PIX_MODULE_HDR                       = "M";

const String PIX_BLAYER_STR                       = "B-Layer";
const String PIX_LAYER1_STR                       = "Layer-1";
const String PIX_LAYER2_STR                       = "Layer-2";
const String PIX_ECALL_STR                        = "EndCaps";
const String PIX_ECA1_STR                         = "ECA1";
const String PIX_ECA2_STR                         = "ECA2";
const String PIX_ECA3_STR                         = "ECA3";
const String PIX_ECC1_STR                         = "ECC1";
const String PIX_ECC2_STR                         = "ECC2";
const String PIX_ECC3_STR                         = "ECC3";
const String PIX_IBL_STR                          = "IBL";

const String PIX_BLAYER_HDR_STR                   = "L0";
const String PIX_LAYER1_HDR_STR                   = "L1";
const String PIX_LAYER2_HDR_STR                   = "L2";
const String PIX_IBL_HDR_STR                      = "LI";
const String PIX_ECA1_HDR_STR                     = "D1A";
const String PIX_ECA2_HDR_STR                     = "D2A";
const String PIX_ECA3_HDR_STR                     = "D3A";
const String PIX_ECC1_HDR_STR                     = "D1C";
const String PIX_ECC2_HDR_STR                     = "D2C";
const String PIX_ECC3_HDR_STR                     = "D3C";

const String PIX_HISTORY_PIXTYPE_NORMAL           = "Normal";
const String PIX_HISTORY_PIXTYPE_GANGED           = "Ganged";
const String PIX_HISTORY_PIXTYPE_INTERGANGED      = "Inter Ganged";
const String PIX_HISTORY_PIXTYPE_LONG             = "Long";
const String PIX_HISTORY_PIXTYPE_LONG_GANGED      = "Long Ganged";
const String PIX_HISTORY_PIXTYPE_LONG_INTERGANGED = "Long Inter Ganged";

// TYPEDEF
typedef std::pair< String, std::map< String, TH1F* >* > PixFileHistPair;

enum PixStructType {
    BLayer,
    Layer1,
    Layer2,
    ECALL,
    ECA1,
    ECA2,
    ECA3,
    ECC1,
    ECC2,
    ECC3,
    IBL,
    IBLPLANNER,
    IBL3D,
};

enum PixType {
    PIX_TYPE_UNKNOWN,
    PIX_TYPE_NORMAL,
    PIX_TYPE_GANGED,
    PIX_TYPE_INTERGANGED,
    PIX_TYPE_LONG,
    PIX_TYPE_LONG_GANGED,
    PIX_TYPE_LONG_INTERGANGED,
};

// -------------------------------------------------------
// PROTOTYPE DECLARATION
// -------------------------------------------------------

// Load parameters from configuration information
bool     LoadConfigParam        ( PixHistoryPlotConfig*    pPlotCfg,
                                  PixHistoryPlotParameter* pCfgParam );

bool     LoadScanFiles          ( PixHistoryPlotConfig*    pPlotCfg,
                                  PixHistoryPlotParameter* pCfgParam );
bool     LoadScanType           ( PixHistoryPlotConfig*    pPlotCfg,
                                  PixHistoryPlotParameter* pCfgParam );
bool     LoadDisplays           ( PixHistoryPlotConfig*    pPlotCfg,
                                  PixHistoryPlotParameter* pCfgParam );
bool     LoadShowAllPixelType   ( PixHistoryPlotConfig*    pPlotCfg,
                                  PixHistoryPlotParameter* pCfgParam );
bool     LoadHorizontalAxis     ( PixHistoryPlotConfig*    pPlotCfg,
                                  PixHistoryPlotParameter* pCfgParam );

bool     IsPixel                ( PixStructType            type );
bool     IsPixel                ( PixHistoryPlotParameter* pParam );
bool     IsIBL                  ( PixStructType            type );
bool     IsIBL                  ( PixHistoryPlotParameter* pParam );

bool     IsBarrelLayers         ( PixStructType            structType );
bool     IsEndCaps              ( PixStructType            structType );

PixType  GetPixTypeForPixel     ( int                      col,
                                  int                      row );
PixType  GetPixTypeForIBL       ( int                      col,
                                  int                      row );

int      GetNumOfHists          ( PixDisplayType           dispType );

bool     CreateCrateNameStr     ( PixStructType            structType,
                                  std::list< String >&     crateNameList );

void     CreateStructTypeStr    ( PixStructType            structType,
                                  String&                  str );

void     CreateStructTypeHdrStr ( PixStructType            structType,
                                  std::list< String >&     strList );

void     CreateScanTypeStr      ( PixHistoryPlotParameter* pCfgParam,
                                  std::list< String >&     scanTypeStrList );

String   CreateScanTypeStrForPix( PixStructType            structType );

String   CreatePixTypeStr       ( const PixType&           pixType );

void     GetNumberforDisks      ( int&                     num,
                                  const String&            crateNameStr );

bool     GetModuleInfo          ( const String&            feName,
                                  bool&                    isAside,
                                  int&                     moduleNo );

bool     GetSensorTypeForIBL    ( const String&            feName,
                                  PixHistoryPlotParameter* pParam,
                                  std::list< String >&     strList );

bool     GetPixTypeStr          ( PixHistoryPlotParameter* pParam,
                                  String&                  str );

String   GetScanTypeFromKey     ( const String&            key,
                                  PixHistoryPlotParameter* pCfgParam );

#endif // PIX_HISTORY_PLOT_UTIL_H
