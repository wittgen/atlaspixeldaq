#ifndef PIX_HISTORY_PLOT_THRESHOLD_H
#define PIX_HISTORY_PLOT_THRESHOLD_H

#include "def.h"
#include "plotutil.h"
#include "pixhistoryplotparam.h"

// DEFINITION
const String PIX_THR_SCURVE_MODE_MEAN   = "SCURVE_MEAN";
const String PIX_THR_SCURVE_MODE_SIGMA  = "SCURVE_SIGMA";
const String PIX_THR_SCURVE_MODE_CHI2   = "SCURVE_CHI2";
const String PIX_THR_MAP_MODE_MEAN      = "MeanMap";
const String PIX_THR_MAP_MODE_SIGMA     = "SigmaMap";
const String PIX_THR_MAP_MODE_CHI2      = "Chi2Map";

// -------------------------------------------------------
// PROTOTYPE DECLARATION
// -------------------------------------------------------

bool CreateThresholdPlot        ( PixFileHistPair*         pPair,
                                  PixHistoryPlotParameter* pParam );

bool THR_LoadFile               ( PixStructType            structType,
                                  PixFileHistPair*         pPair,
                                  PixHistoryPlotParameter* pParam,
                                  TDirectory*              pDir );

bool THR_LoadModuleForPixel     ( TDirectory*              pDir,
                                  PixFileHistPair*         pPair,
                                  PixHistoryPlotParameter* pParam,
                                  const String&            slotFolder,
                                  PixStructType            structType );
bool THR_LoadModuleForIBL       ( TDirectory*              pDir,
                                  PixFileHistPair*         pPair,
                                  PixHistoryPlotParameter* pParam,
                                  const String&            slotFolder,
                                  PixStructType            structType );

bool THR_LoadScurveHist         ( TDirectory*              pDir,
                                  PixFileHistPair*         pPair,
                                  PixHistoryPlotParameter* pParam,
                                  PixStructType            structType,
                                  const String&            scurveMode,
                                  const String&            mapMode );

bool THR_FillHistsForPix        ( PixFileHistPair*         pPair,
                                  PixHistoryPlotParameter* pParam,
                                  const String&            structTypeStr,
                                  int                      col,
                                  int                      row,
                                  double                   val );

bool THR_FillHistsForIBL        ( PixFileHistPair*         pPair,
                                  PixHistoryPlotParameter* pParam,
                                  const String&            feName,
                                  int                      col,
                                  int                      row,
                                  double                   val );

bool THR_CreateSCurveParamStr   ( PixHistoryPlotParameter* pParam,
                                  String&                  sCurveModeStr,
                                  String&                  mapModeStr );

#endif // PIX_HISTORY_PLOT_THRESHOLD_H
