#ifndef PIX_HISTORY_PLOT_CONFIG_H
#define PIX_HISTORY_PLOT_CONFIG_H

#include "def.h"

// ROOT
#include "TXMLEngine.h"

class PixHistoryPlotConfig
{
public:
    PixHistoryPlotConfig      ( String                  fileName );  // Constructor
    ~PixHistoryPlotConfig     ( ); // Destructor

    bool   Read               ( );  // reading config.xml
    String GetFileName        ( ) { return _fileName; }

    bool   LoadConfigValue    ( const String&           tagName,
                                const String&           attrName,
                                String&                 val );
    bool   LoadConfigValue    ( const String&           tagName,
                                const String&           attrName,
                                std::list< String >&    valList,
                                bool                    single = false );

private:
    bool   GetNodeRecur       ( XMLNodePointer_t        pNode,
                                const String&           tagName,
                                const String&           attrName,
                                std::list< String >&    valList,
                                bool single );
    bool   GetAttrRecur       ( XMLAttrPointer_t        pAttr,
                                const String&           attrName,
                                std::list< String >&    valList );

private:
    String                    _fileName;
    XMLDocPointer_t           _pDocPtr;
};

#endif // PIX_HISTORY_PLOT_CONFIG_H
