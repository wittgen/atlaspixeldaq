//////////////////////////////////////////////////////////////////
//
// Utility
//
// 2014/1/29
// Satoshi Higashino
// satoshi.higashino@cern.ch
//
//////////////////////////////////////////////////////////////////
#ifndef SH_UTIL_H
#define SH_UTIL_H

// COMMON
#include "def.h"

// File operation
#include <dirent.h>

class ShUtil
{
public:

    //////////////////////////////////////////////////////////////////
    //
    // Converter
    //
    //////////////////////////////////////////////////////////////////

    // ---------------------------------------------------------------
    // -> string data converter
    template <typename T> static String ToString( const T& val )
    {
        String str = "";
        StringStream stream;

        stream << val;
        stream >> str;

        return str;
    }

    // ---------------------------------------------------------------
    // -> string data converter
    static int StrToInt( const String& str )
    {
        StringStream stream;
        int retVal = 0;
        
        stream << str;
        stream >> retVal;

        return retVal;
    }

        // ---------------------------------------------------------------
    // -> string data converter
    static double StrToDouble( const String& str )
    {
        StringStream stream;
        double retVal = 0.0;
        
        stream << str;
        stream >> retVal;

        return retVal;
    }

    // ---------------------------------------------------------------
    // Convert angle unit
    // ---------------------------------------------------------------
    // ---------------------------------------------------------------
    // Radian -> Degree
    static double RadToDeg( double rad )
    {
        return rad * 180.0 / PI;
    }

    // ---------------------------------------------------------------
    // Degree -> Radian
    static double DegToRad( double deg )
    {
        return deg * PI / 180.0;
    }

    // ---------------------------------------------------------------
    // Convert timee unit
    // ---------------------------------------------------------------
    // ---------------------------------------------------------------
    // hour -> minute
    static double HourToMin( double hour )
    {
        return hour * 60.0;
    }

    // ---------------------------------------------------------------
    // hour -> second
    static double HourToSec( double hour )
    {
        return hour * 3600.0;
    }

    // ---------------------------------------------------------------
    // minute -> hour
    static double MinToHour( double min )
    {
        return min / 60.0;
    }

    // ---------------------------------------------------------------
    // minute -> second
    static double MinToSec( double min )
    {
        return min * 60.0;
    }

    // ---------------------------------------------------------------
    // second -> hour
    static double SecToHour( double sec )
    {
        return sec * 3600.0;
    }

    // ---------------------------------------------------------------
    // second -> minute
    static double SecToMin( double sec )
    {
        return sec * 60.0;
    }

    // ---------------------------------------------------------------
    // Console Output
    // ---------------------------------------------------------------
    // ---------------------------------------------------------------
    // only applied for string value.
    // eg. MuUitl::Cout( "Example" );
    template <typename T> static void Cout( const T& val )
    {
        std::cout << val << std::endl;
    }

    // ---------------------------------------------------------------
    // output error message
    template <typename T> static void Cerr( const T& val )
    {
        std::cout << "Error: " << val << std::endl;
    }

    // ---------------------------------------------------------------
    // output error message
    template <typename T> static void Cwarn( const T& val )
    {
        std::cout << "Warning: " << val << std::endl;
    }

    // ---------------------------------------------------------------
    // output error message
    template <typename T> static void Cinfo( const T& val )
    {
        std::cout << "Information: " << val << std::endl;
    }

    //////////////////////////////////////////////////////////////////
    //
    // Comparetor
    //
    //////////////////////////////////////////////////////////////////
    // ---------------------------------------------------------------
    // Comparing "double" value
    // at 10e-6 (the significance of float value)
    // return :
    //     1 for first > second
    //    -1 for first < second
    //     0 for first = second
    static int CompareDouble( const double first, const double second )
    {
        if( fabs( first - second ) < 0.000001 ) {
            return 0;
        }
        else {
            if( first > second ) {
                return 1;
            }
            else {
                return -1;
            }
        }
    }

    //////////////////////////////////////////////////////////////////
    //
    // File Checker
    //
    //////////////////////////////////////////////////////////////////
    // ---------------------------------------------------------------
    // return:
    //    true ...exist 
    //    false...NOT exist 
    static bool ExistFile( const String& filePath )
    {
        bool isExist = false;

        FILE* fp = fopen( filePath.c_str( ), "r" );
        if( fp == NULL ) {
            return isExist;
        } else {
            isExist = true;
            fclose( fp );
        }

        return isExist;
    }

    // ---------------------------------------------------------------
    // Check directry
    // return:
    //    true ...exist 
    //    false...NOT exist 
    static bool ExistDir( const String& dirPath )
    {
        bool isExist = false;

        DIR* dp = opendir( dirPath.c_str( ) );
        if( dp == NULL ) {
            return isExist;
        } else {
            isExist = true;
            closedir( dp );
        }

        return true;
    }

    // ---------------------------------------------------------------
    // Check directry belonging to file path
    // return:
    //    true ...exist 
    //    false...NOT exist 
    static bool ExistFilePathDir( const String& filePath )
    {
        int index = filePath.rfind( "/", filePath.size( ) - 1 );
        String dirPath = filePath.substr( 0, index );

        return ShUtil::ExistDir( dirPath );
    }

    // ---------------------------------------------------------------
    // Get file name from full path
    // return: file name
    static String GetFileName( const String& path )
    {
        size_t pos1 = path.rfind( '/' );
        if( pos1 != String::npos ) {
            return path.substr( pos1 + 1, path.size( ) - pos1 - 1 );
        }
        
        return path;
    }

    // ---------------------------------------------------------------
    // Extract extension from file name
    // return: file name (without extension)
    static String ExtractPathWithoutExt( const String& path )
    {
        String::size_type pos;
        if( ( pos = path.find_last_of( "." ) ) == String::npos ) {
            return path;
        }

        return path.substr( 0, pos );
    }

};

#endif // SH_UTIL_H

//////////////////////////////////////////////////////////////////
//
// Update history
//
// 2014/01/29 Created
// Satoshi
//
// 2015/11/01 Modified
// Satoshi

