#ifndef PIX_HISTORY_PLOT_PARAM_H
#define PIX_HISTORY_PLOT_PARAM_H

#include "def.h"

// DEFINITION

// XML
const String PIX_HISTORY_XMLTAG_CONFIG            = "Configuration";

const String PIX_HISTORY_XMLTAG_SCANFILE          = "ScanFile";

const String PIX_HISTORY_XMLTAG_SCANTYPE          = "ScanType";
const String PIX_HISTORY_XML_SCANTYPE_THRESHOLD   = "Threshold";
const String PIX_HISTORY_XML_SCANTYPE_ENC         = "ENC";
const String PIX_HISTORY_XML_SCANTYPE_CHI2        = "Chi2";
const String PIX_HISTORY_XML_SCANTYPE_TOT         = "ToT";
const String PIX_HISTORY_XML_SCANTYPE_T0          = "T0";
const String PIX_HISTORY_XML_SCANTYPE_MEAN        = "Mean";
const String PIX_HISTORY_XML_SCANTYPE_RMS         = "RMS";

const String PIX_HISTORY_XMLTAG_DISP              = "Display";
const String PIX_HISTORY_XML_DISP_PIXALL          = "Pixel_All";
const String PIX_HISTORY_XML_DISP_PIXL0           = "Pixel_Layer-0";
const String PIX_HISTORY_XML_DISP_PIXL1           = "Pixel_Layer-1";
const String PIX_HISTORY_XML_DISP_PIXL2           = "Pixel_Layer-2";
const String PIX_HISTORY_XML_DISP_PIXEC           = "Pixel_EC";
const String PIX_HISTORY_XML_DISP_PIXECA          = "Pixel_ECA";
const String PIX_HISTORY_XML_DISP_PIXECC          = "Pixel_ECC";
const String PIX_HISTORY_XML_DISP_IBLALL          = "IBL_All";
const String PIX_HISTORY_XML_DISP_IBLPLANNER      = "IBL_Planner";
const String PIX_HISTORY_XML_DISP_IBL3D           = "IBL_3D";
const String PIX_HISTORY_XML_DISP_IBL3DFBK        = "IBL_3D_FBK";
const String PIX_HISTORY_XML_DISP_IBL3DCNM        = "IBL_3D_CNM";

const String PIX_HISTORY_XMLTAG_SHOWALLPIXTYPE    = "ShowAllPixelType";

const String PIX_HISTORY_XMLTAG_XAXIS             = "HorizontalAxis";
const String PIX_HISTORY_XML_XAXIS_FILENAME       = "ScanFileName";
const String PIX_HISTORY_XML_XAXIS_DATE           = "Date";
const String PIX_HISTORY_XML_XAXIS_LUMINOSITY     = "Luminosity";
const String PIX_HISTORY_XML_XAXIS_TID            = "TID";
const String PIX_HISTORY_XML_XAXIS_CUSTOM         = "Custom";

const String PIX_HISTORY_XML_COMMON_YES           = "Yes";
const String PIX_HISTORY_XML_COMMON_NO            = "No";

const String PIX_HISTORY_XMLATTR_PATH             = "path";
const String PIX_HISTORY_XMLATTR_NAME             = "name";
const String PIX_HISTORY_XMLATTR_TYPE             = "type";
const String PIX_HISTORY_XMLATTR_VAL              = "val";

// HISTOGRAM
const String PIX_HISTORY_HIST_PIXALL              = "Pixel";
const String PIX_HISTORY_HIST_PIXL0               = "Pixel B-Layer";
const String PIX_HISTORY_HIST_PIXL1               = "Pixel Layer-1";
const String PIX_HISTORY_HIST_PIXL2               = "Pixel Layer-2";
const String PIX_HISTORY_HIST_PIXEC               = "Pixel EC (both sides)";
const String PIX_HISTORY_HIST_PIXECA              = "Pixel ECA";
const String PIX_HISTORY_HIST_PIXECC              = "Pixel ECC";
const String PIX_HISTORY_HIST_IBLALL              = "IBL";
const String PIX_HISTORY_HIST_IBLPLANNER          = "IBL Planner";
const String PIX_HISTORY_HIST_IBL3D               = "IBL 3D";
const String PIX_HISTORY_HIST_IBL3DFBK            = "IBL 3D FBK";
const String PIX_HISTORY_HIST_IBL3DCNM            = "IBL 3D CNM";

const String PIX_HISTORY_XAXIS_FILENAME           = "Scan File Name";
const String PIX_HISTORY_XAXIS_DATE               = "Date";
const String PIX_HISTORY_XAXIS_LUMINOSITY         = "Integrated Luminosity [pb^-1]";
const String PIX_HISTORY_XAXIS_TID                = "TID [Mrad]";

const String PIX_HISTORY_YAXIS_THRESHOLD          = "Threshold [e-]";
const String PIX_HISTORY_YAXIS_ENC                = "ENC [e-]";
const String PIX_HISTORY_YAXIS_CHI2               = "Chi2";
const String PIX_HISTORY_YAXIS_TOT                = "ToT [B.C.]";
const String PIX_HISTORY_YAXIS_T0                 = "T0";
const String PIX_HISTORY_YAXIS_MEAN               = "Mean";
const String PIX_HISTORY_YAXIS_RMS                = "RMS of Mean";

enum PixScanType {
    SCAN_UNKNOWN,
    SCAN_THRESHOLD,
    SCAN_ENC,
    SCAN_SCURVE_CH2,
    SCAN_TOT,
    SCAN_T0,
};

enum PixValType {
    VAL_UNKNOWN,
    VAL_MEAN,
    VAL_RMS,
};

enum PixDisplayType {
    DISP_NONE         = 0,
    DISP_PIX_ALL      = 1,
    DISP_PIX_LAYER_0  = 2,
    DISP_PIX_LAYER_1  = 4,
    DISP_PIX_LAYER_2  = 8,
    DISP_PIX_EC       = 16,
    DISP_PIX_EC_ASIDE = 32,
    DISP_PIX_EC_CSIDE = 64,
    DISP_IBL_ALL      = 128,
    DISP_IBL_PLANNER  = 256,
    DISP_IBL_3D       = 512,
    DISP_IBL_3D_FBK   = 1024,
    DISP_IBL_3D_CNM   = 2048,
    DISP_MAX          = 4096,
};

enum PixHorizontalAxis {
    XAXIS_UNKNOWN,
    XAXIS_SCANFILENAME,
    XAXIS_DATE,
    XAXIS_LUMINOSITY,
    XAXIS_TID,
    XAXIS_CUSTOM,
};

struct PixConfigScanFile
{
    // Constructor
    PixConfigScanFile( const String& path = "", const String& name = "" )
        : _path  ( path )
        , _name  ( name ) { }

    String _path; // calibration scan file
    String _name; // label for x axis
};

struct PixConfigScanType
{
    PixScanType _type; // calibration scan type
    PixValType  _val;  // calibration scan value (mean or rms)
};

/////////////////////////////////////////////////////////////////////////
//
// CLASS : PixHistoryPlotParameter
//
/////////////////////////////////////////////////////////////////////////
class PixHistoryPlotParameter
{
public:
    // Constructor
    PixHistoryPlotParameter( )
        : _dispType        ( DISP_NONE )
        , _showAllPixType  ( false )
        , _xAxis           ( XAXIS_UNKNOWN ) { }

    // Destructor
    ~PixHistoryPlotParameter( ) { }

    // Console output internal value (mainly for debug)
    void                           Print                 ( );
        
    std::list< PixConfigScanFile > _scanFileList;
    PixConfigScanType              _scanType;
    int                            _dispType;
    bool                           _showAllPixType;
    PixHorizontalAxis              _xAxis;
};

void
PixHistoryPlotParameter::Print( )
{
    std::cout << "---- ScanFiles ----" << std::endl;
    std::list< PixConfigScanFile >::iterator itr = _scanFileList.begin( );
    while( itr != _scanFileList.end( ) ) {
        std::cout << "path : " << (*itr)._path << " name : " << (*itr)._name << std::endl;
        ++itr;
    }
    
    std::cout << "---- ScanType ----" << std::endl;

    std::cout << "type : " << _scanType._type << " val : " << _scanType._val << std::endl;

    std::cout << "---- Display ----" << std::endl;

    std::cout << "type : " << _dispType << std::endl;

    std::cout << "---- ShowAllPixelType ----" << std::endl;

    std::cout << "val : " << _showAllPixType << std::endl;

    std::cout << "---- HorizontalAxis ----" << std::endl;

    std::cout << "val : " << _xAxis << std::endl;

    std::cout << std::endl;
}

#endif // PIX_HISTORY_PLOT_PARAM_H
