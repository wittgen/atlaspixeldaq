#include "include/util.h"

#include "include/pixhistoryplotconfig.h"
#include "include/pixhistoryplotparam.h"
#include "include/thresholdplots.h"
#include "include/totplots.h"
#include "include/tzeroplots.h"

// ATLAS LABEL
#include "include/AtlasLabels.h"
#include "include/AtlasUtils.h"
#include "include/AtlasStyle.h"

// ROOT
#include "TROOT.h"
#include "TStyle.h"
#include "TString.h"
#include "TCanvas.h"
#include "TLegend.h"

#ifndef __CINT__
#include "src/pixhistoryplotconfig.C"
#include "src/thresholdplots.C"
#include "src/totplots.C"
#include "src/tzeroplots.C"
#include "src/AtlasLabels.C"
#include "src/AtlasUtils.C"
#include "src/AtlasStyle.C"
#endif

// -------------------------------------------------------
// PROTOTYPE DECLARATION
// -------------------------------------------------------

// Main function
void   historyplot             ( );

// Create history plot 
bool   CreatePlots             ( PixHistoryPlotParameter*       pCfgParam );

bool   PrepareHists            ( std::list< PixFileHistPair* >* pHistoryList,
                                 PixHistoryPlotParameter*       pParam );

bool   CreateTHist             ( const String&                  fileNameStr,
                                 PixHistoryPlotParameter*       pCfgParam,
                                 std::map< String, TH1F* >*     pMap );

void   AddHist                 ( PixHistoryPlotParameter*       pCfgParam,
                                 std::map< String, TH1F* >*     pPlotFileTable,
                                 const String&                  nameStr );

// Visual
String GetXAxisLabel           ( PixHistoryPlotParameter*       pParam );
String GetYAxisLabel           ( PixHistoryPlotParameter*       pParam );

void   GetYRange               ( PixHistoryPlotParameter*       pParam,
                                 double&                        minY,
                                 double&                        maxY );

void   ReLabelXAxis            ( PixHistoryPlotParameter*       pParam,
                                 TH1F*                          pPlot );

// Create x axsi
double GetXAxisValFromFileName ( const String&                  fileName,
                                 PixHistoryPlotParameter*       pParam );
// Delete list components
void   DeleteListComponents    ( std::list< PixFileHistPair* >* pList );  

///////////////////////////////////////////////////////////
// historyplot
///////////////////////////////////////////////////////////
void
historyplot( ) {

    // ATLAS Style
    SetAtlasStyle( );
        
    // Read config.xml
    PixHistoryPlotConfig* pPlotCfg = new PixHistoryPlotConfig( "config.xml" );
    if( pPlotCfg->Read( ) == false ) {
        std::cout << "Error : failed to read configuration file : ./config/config.xml" << std::endl;
        return;
    }

    // Load each config parameter
    PixHistoryPlotParameter configParam;
    if( LoadConfigParam( pPlotCfg, &configParam ) == false ) {
        ShUtil::Cerr( "Error : failed to load configuration (you may write wrong description in config.xml)" );
        return;
    }

    // for debug
    configParam.Print( );

    // create plots
    if( CreatePlots( &configParam ) == false ) {
        std::cout << "Error : failed to create plot..." << std::endl;
        return;
    }
}

///////////////////////////////////////////////////////////
// CreatePlots
///////////////////////////////////////////////////////////
bool
CreatePlots( PixHistoryPlotParameter* pParam )
{
    if( pParam == NULL ) return false;

    std::list< PixFileHistPair* > historyList;
    if( PrepareHists( &historyList, pParam ) == true ) {

        // Don't have to delete list component if no components are in the list.
        if( historyList.size( ) <= 0 ) return false;

        std::list< String > scanTypeList;
        CreateScanTypeStr( pParam, scanTypeList );

        std::map< String, TH1F* > plotFileTable;
        std::list< String >::iterator scanTypeListItr =  scanTypeList.begin( );
        while( scanTypeListItr != scanTypeList.end( ) ) {
            String name = *scanTypeListItr;
            AddHist( pParam, &plotFileTable, name );
            ++scanTypeListItr;
        }
        
        // Create plot
        int index = 1;
        std::list< PixFileHistPair* >::iterator listItr = historyList.begin( );
        while( listItr != historyList.end( ) ) {

            PixFileHistPair* pPair = *listItr;
            if( pPair == NULL ) continue;

            // Threshold 
            if( pParam->_scanType._type == SCAN_THRESHOLD ||
                pParam->_scanType._type == SCAN_ENC       ||
                pParam->_scanType._type == SCAN_SCURVE_CH2 ) {

                if( CreateThresholdPlot( pPair, pParam ) == false ) {
                    ++listItr;
                    continue;
                }
            }
            else if( pParam->_scanType._type == SCAN_TOT ) {

                if( CreateTotPlot( pPair, pParam ) == false ) {
                    ++listItr;
                    continue;
                }
            }
            else if( pParam->_scanType._type == SCAN_T0 ) {

                if( CreateTzeroPlot( pPair, pParam ) == false ) {
                    ++listItr;
                    continue;
                }
            }

            
            String fileName = pPair->first;
            std::map< String, TH1F* >* pHistTable = pPair->second;
            if( pHistTable == NULL ) continue;
            
            std::map< String, TH1F* >::iterator mapItr = pHistTable->begin( );
            while( mapItr != pHistTable->end( ) ) {
                TH1F* pHist = mapItr->second;
                if( pHist == NULL ) continue;

                String key = mapItr->first;
                String scanTypeStr = GetScanTypeFromKey( key, pParam );
                if( scanTypeStr.length( ) <= 0 ) continue;

                std::map< String, TH1F* >::iterator targetHistItr = plotFileTable.find( scanTypeStr );
                if( targetHistItr != plotFileTable.end( ) ) {
                    TH1F* pTargetHist = targetHistItr->second;
                    if( pTargetHist == NULL ) continue;

                    // x axis
                    double xLabel = static_cast< double >( index );
                    if( pParam->_xAxis == XAXIS_LUMINOSITY ||
                        pParam->_xAxis == XAXIS_TID ) {
                        xLabel = GetXAxisValFromFileName( fileName, pParam );
                    }

                    if( pParam->_scanType._val == VAL_MEAN )
                        pTargetHist->Fill( xLabel, pHist->GetMean( ) );
                    else if( pParam->_scanType._val == VAL_RMS )
                        pTargetHist->Fill( xLabel, pHist->GetRMS( ) );

                }

                ++mapItr;
            }
            
            ++index;
            ++listItr;
        }

        // Draw plot
        TCanvas* pCvs = new TCanvas( "cvs", "cvs", 0, 0, 800, 600 );
        TLegend* pLegend = new TLegend( 0.8, 0.75, 0.97, 0.97 );

        DEBUG( plotFileTable.size() );
        
        std::map< String, TH1F* >::iterator drawItr = plotFileTable.begin( );
        int color = 2;
        while( drawItr != plotFileTable.end( ) ) {
            TH1F* pPlot = (*drawItr).second;
            if( pPlot == NULL ) continue;

            pPlot->SetMarkerStyle( 21 );
            pPlot->SetMarkerSize( 1 );
            pPlot->SetMarkerColor( color );
            pPlot->SetLineStyle( 1 );
            pPlot->SetLineColor( color );

            pPlot->GetXaxis( )->SetTitle( GetXAxisLabel( pParam ).c_str( ) );
            pPlot->GetYaxis( )->SetTitle( GetYAxisLabel( pParam ).c_str( ) );

            // Re-labeling for x axis
            if( drawItr == plotFileTable.begin( ) ) {
                ReLabelXAxis( pParam, pPlot );
            }
            
            // pPlot->SetCanExtend(TH1::kAllAxes);
            pPlot->Draw( "samehistp" );
            pLegend->AddEntry( pPlot, (*drawItr).first.c_str( ) );
            
            ++color;
            ++drawItr;
        }

        pLegend->Draw( );
    }

    DeleteListComponents( &historyList );
    
    return true;
}

///////////////////////////////////////////////////////////
// PrepareHists
///////////////////////////////////////////////////////////
bool
PrepareHists( std::list< PixFileHistPair* >* pHistoryList,
              PixHistoryPlotParameter*       pParam )
{
    if( pHistoryList == NULL || pParam == NULL ) return false;

    bool retVal = false;
    std::list< PixConfigScanFile >::iterator itr = pParam->_scanFileList.begin( );
    while( itr != pParam->_scanFileList.end( ) ) {

        std::map< String, TH1F* >* pHistMapPerFile = new std::map< String, TH1F* >;
        if( CreateTHist( (*itr)._path, pParam, pHistMapPerFile ) == false ||
            pHistMapPerFile->size( ) <= 0 ) {
            delete pHistMapPerFile;
            continue;
        }

        PixFileHistPair* pPair = new PixFileHistPair( (*itr)._path, pHistMapPerFile );
        pHistoryList->push_back( pPair );
        retVal = true;        
        ++itr;
    }

    return retVal;
}

///////////////////////////////////////////////////////////
// CreateTHist
///////////////////////////////////////////////////////////
bool
CreateTHist( const String&              fileNameStr,
             PixHistoryPlotParameter*   pCfgParam,
             std::map< String, TH1F* >* pMap )
{
    if( fileNameStr.length( ) <= 0 ) return false;
    if( pCfgParam == NULL || pMap == NULL ) return false;

    std::list< String > scanTypeStrList;
    CreateScanTypeStr( pCfgParam, scanTypeStrList );
    if( scanTypeStrList.size( ) <= 0 ) return false;

    std::list< String >::iterator itr =  scanTypeStrList.begin( );
    while( itr != scanTypeStrList.end( ) ) {
        String scanTypeStr = *itr;

        String allHistNameStr = "%s_%s";
        char*  allPixName = Form( allHistNameStr.c_str( ), fileNameStr.c_str( ), scanTypeStr.c_str( ) );
        TH1F*  pAllHistPix = new TH1F( allPixName, allPixName, 10000, 0, 10000 );

        pMap->insert( make_pair( pAllHistPix->GetName( ), pAllHistPix ) );

        if( pCfgParam->_showAllPixType == true ) {
            String histNameStr = "%s_%s_%s";

            // Normal pixel
            char* normalPixName      = Form( histNameStr.c_str( ),
                                             fileNameStr.c_str( ),
                                             scanTypeStr.c_str( ),
                                             PIX_HISTORY_PIXTYPE_NORMAL.c_str( ) );
            TH1F* pHistNormalPix      = new TH1F( normalPixName, normalPixName, 10000, 0, 10000 );
            pMap->insert( make_pair( pHistNormalPix->GetName( ), pHistNormalPix ) );

            
            // Long pixel
            char* longPixName        = Form( histNameStr.c_str( ),
                                             fileNameStr.c_str( ),
                                             scanTypeStr.c_str( ),
                                             PIX_HISTORY_PIXTYPE_LONG.c_str( ) );
            TH1F* pHistLongPix        = new TH1F( longPixName,   longPixName,   10000, 0, 10000 );
            pMap->insert( make_pair( pHistLongPix->GetName( ),   pHistLongPix ) );

            if( IsPixel( pCfgParam ) == true ) {
            
                // Ganged pixel
                char* gangPixName        = Form( histNameStr.c_str( ),
                                                 fileNameStr.c_str( ),
                                                 scanTypeStr.c_str( ),
                                                 PIX_HISTORY_PIXTYPE_GANGED.c_str( ) );
                TH1F* pHistGangPix        = new TH1F( gangPixName,        gangPixName,        10000, 0, 10000 );
                pMap->insert( make_pair( pHistGangPix->GetName( ),        pHistGangPix ) );

                // Inter-Ganged pixel
                char* intGangPixName     = Form( histNameStr.c_str( ),
                                                 fileNameStr.c_str( ),
                                                 scanTypeStr.c_str( ),
                                                 PIX_HISTORY_PIXTYPE_INTERGANGED.c_str( ) );
                TH1F* pHistIntGangPix     = new TH1F( intGangPixName,     intGangPixName,     10000, 0, 10000 );
                pMap->insert( make_pair( pHistIntGangPix->GetName( ),     pHistIntGangPix ) );

                // Long Ganged pixel
                char* longGangPixName    = Form( histNameStr.c_str( ),
                                                 fileNameStr.c_str( ),
                                                 scanTypeStr.c_str( ),
                                                 PIX_HISTORY_PIXTYPE_LONG_GANGED.c_str( ) );
                TH1F* pHistLongGangPix    = new TH1F( longGangPixName,    longGangPixName,    10000, 0, 10000 );
                pMap->insert( make_pair( pHistLongGangPix->GetName( ),    pHistLongGangPix ) );

                // Long Inter-Ganged pixel
                char* longIntGangPixName = Form( histNameStr.c_str( ),
                                                 fileNameStr.c_str( ),
                                                 scanTypeStr.c_str( ),
                                                 PIX_HISTORY_PIXTYPE_LONG_INTERGANGED.c_str( ) );
                TH1F* pHistLongIntGangPix = new TH1F( longIntGangPixName, longIntGangPixName, 10000, 0, 10000 );
                pMap->insert( make_pair( pHistLongIntGangPix->GetName( ), pHistLongIntGangPix ) );
            }
        }

        ++itr;
    }
    
    return true;
}

///////////////////////////////////////////////////////////
// AddHist
///////////////////////////////////////////////////////////
void
AddHist( PixHistoryPlotParameter*   pCfgParam,
         std::map< String, TH1F* >* pPlotFileTable,
         const String&              name )
{
    if( pCfgParam == NULL || pPlotFileTable == NULL ) return;
    if( name.length( ) <= 0 ) return;

    TH1F* pHistoryHist = new TH1F( name.c_str( ), name.c_str( ), 10000, 0.5, 10000.5 );
    pPlotFileTable->insert( make_pair( name, pHistoryHist ) );
    if( pCfgParam->_showAllPixType == true ) {
        String pixTypeName = "";

        // normal
        pixTypeName = name + "_" + PIX_HISTORY_PIXTYPE_NORMAL;
        pHistoryHist = new TH1F( pixTypeName.c_str( ), pixTypeName.c_str( ), 10000, 0.5, 10000.5 );
        pPlotFileTable->insert( make_pair( pixTypeName, pHistoryHist ) );

        // long
        pixTypeName = name + "_" + PIX_HISTORY_PIXTYPE_LONG;
        pHistoryHist = new TH1F( pixTypeName.c_str( ), pixTypeName.c_str( ), 10000, 0.5, 10000.5 );
        pPlotFileTable->insert( make_pair( pixTypeName, pHistoryHist ) );

        if( IsPixel( pCfgParam ) == true ) {
            // ganged
            pixTypeName = name + "_" + PIX_HISTORY_PIXTYPE_GANGED;
            pHistoryHist = new TH1F( pixTypeName.c_str( ), pixTypeName.c_str( ), 10000, 0.5, 10000.5 );
            pPlotFileTable->insert( make_pair( pixTypeName, pHistoryHist ) );

            // inter-ganged
            pixTypeName = name + "_" + PIX_HISTORY_PIXTYPE_INTERGANGED;
            pHistoryHist = new TH1F( pixTypeName.c_str( ), pixTypeName.c_str( ), 10000, 0.5, 10000.5 );
            pPlotFileTable->insert( make_pair( pixTypeName, pHistoryHist ) );

            // long ganged
            pixTypeName = name + "_" + PIX_HISTORY_PIXTYPE_LONG_GANGED;
            pHistoryHist = new TH1F( pixTypeName.c_str( ), pixTypeName.c_str( ), 10000, 0.5, 10000.5 );
            pPlotFileTable->insert( make_pair( pixTypeName, pHistoryHist ) );

            // long inter-ganged
            pixTypeName = name + "_" + PIX_HISTORY_PIXTYPE_LONG_INTERGANGED;
            pHistoryHist = new TH1F( pixTypeName.c_str( ), pixTypeName.c_str( ), 10000, 0.5, 10000.5 );
            pPlotFileTable->insert( make_pair( pixTypeName, pHistoryHist ) );
        }
    }
    
    return;
}

///////////////////////////////////////////////////////////
// GetXAxisLabel / GetYAxisLabel
///////////////////////////////////////////////////////////
String
GetXAxisLabel( PixHistoryPlotParameter* pParam )
{
    if( pParam == NULL ) return "";

    if( pParam->_xAxis == XAXIS_SCANFILENAME ) {
        return PIX_HISTORY_XAXIS_FILENAME;
    }
    else if( pParam->_xAxis == XAXIS_DATE ) {
        return PIX_HISTORY_XAXIS_DATE;
    }
    else if( pParam->_xAxis == XAXIS_LUMINOSITY ) {
        return PIX_HISTORY_XAXIS_LUMINOSITY;
    }
    else if( pParam->_xAxis == XAXIS_TID ) {
        return PIX_HISTORY_XAXIS_TID;
    }
        
    return "";
}

String
GetYAxisLabel( PixHistoryPlotParameter* pParam )
{
    if( pParam == NULL ) return "";

    String typeStr = "";
    if( pParam->_scanType._type == SCAN_THRESHOLD )
        typeStr = PIX_HISTORY_YAXIS_THRESHOLD;
    else if( pParam->_scanType._type == SCAN_ENC )
        typeStr = PIX_HISTORY_YAXIS_ENC;
    else if( pParam->_scanType._type == SCAN_SCURVE_CH2 )
        typeStr = PIX_HISTORY_YAXIS_CHI2;
    else if( pParam->_scanType._type == SCAN_TOT )
        typeStr = PIX_HISTORY_YAXIS_TOT;
    else if( pParam->_scanType._type == SCAN_T0 )
        typeStr = PIX_HISTORY_YAXIS_T0;

    String valStr  = "";
    if( pParam->_scanType._val == VAL_MEAN )
        valStr = PIX_HISTORY_YAXIS_MEAN;
    else if( pParam->_scanType._val == VAL_RMS )
        valStr = PIX_HISTORY_YAXIS_RMS;

    String labelStr = valStr + " " + typeStr;    
    
    return labelStr;
}

///////////////////////////////////////////////////////////
// GetYRange
///////////////////////////////////////////////////////////
void
GetYRange( PixHistoryPlotParameter* pParam, double& minY, double& maxY )
{
    if( pParam->_scanType._type == SCAN_THRESHOLD ) {
        if( pParam->_scanType._val == VAL_MEAN ) {
            minY = 1400.0;
            maxY = 3600.0;
        }
        else if( pParam->_scanType._val == VAL_RMS ) {
            minY = 0.0;
            maxY = 0.0;
        }
    }
    else if( pParam->_scanType._type ==SCAN_ENC ) {
        if( pParam->_scanType._val == VAL_MEAN ) {
            minY = 0.0;
            maxY = 0.0;
        }
        else if( pParam->_scanType._val == VAL_RMS ) {
            minY = 0.0;
            maxY = 0.0;
        }
    }
    else if( pParam->_scanType._type ==SCAN_SCURVE_CH2 ) {
        if( pParam->_scanType._val == VAL_MEAN ) {
            minY = 0.0;
            maxY = 0.0;
        }
        else if( pParam->_scanType._val == VAL_RMS ) {
            minY = 0.0;
            maxY = 0.0;
        }
    }
    else if( pParam->_scanType._type == SCAN_TOT ) {
        if( pParam->_scanType._val == VAL_MEAN ) {
            minY = 0.0;
            maxY = 0.0;
        }
        else if( pParam->_scanType._val == VAL_RMS ) {
            minY = 0.0;
            maxY = 0.0;
        }
    }
    else if( pParam->_scanType._type == SCAN_T0 ) {
        if( pParam->_scanType._val == VAL_MEAN ) {
            minY = 0.0;
            maxY = 0.0;
        }
        else if( pParam->_scanType._val == VAL_RMS ) {
            minY = 0.0;
            maxY = 0.0;
        }
    }

    return;
}

///////////////////////////////////////////////////////////
// ReLabelXAxis
///////////////////////////////////////////////////////////
void
ReLabelXAxis( PixHistoryPlotParameter* pParam, TH1F* pPlot )
{
    if( pParam == NULL || pPlot == NULL ) return;

    if( pParam->_xAxis == XAXIS_SCANFILENAME ) {

        double max = static_cast< double >( pParam->_scanFileList.size( ) + 0.5 );
        pPlot->GetXaxis( )->SetRangeUser( 0.5, max );
        
        int idx = 1;
        std::list< PixConfigScanFile >::iterator itr = pParam->_scanFileList.begin( );
        while( itr != pParam->_scanFileList.end( ) ) {
            String fileName = (*itr)._path;

            pPlot->GetXaxis( )->SetBinLabel( idx, fileName.c_str( ) );
            
            ++itr;
            ++idx;
        }
    }
    else if( pParam->_xAxis == XAXIS_DATE ) {

        double max = static_cast< double >( pParam->_scanFileList.size( ) + 0.5 );
        pPlot->GetXaxis( )->SetRangeUser( 0.5, max );

        int idx = 1;
        std::list< PixConfigScanFile >::iterator itr = pParam->_scanFileList.begin( );
        while( itr != pParam->_scanFileList.end( ) ) {
            String fileName = (*itr)._path;
            TFile tFile( fileName.c_str( ) );

            TDatime modifyDate = tFile.GetModificationDate( );
            String dateStr = modifyDate.AsString( );

            pPlot->GetXaxis( )->SetBinLabel( idx, dateStr.c_str( ) );
            
            ++itr;
            ++idx;
        }
    }
    else if( pParam->_xAxis == XAXIS_LUMINOSITY ||
             pParam->_xAxis == XAXIS_TID ) {
        std::list< PixConfigScanFile >::iterator itr = pParam->_scanFileList.begin( );
        double min = ShUtil::StrToDouble( (*itr)._name );
        double max = ShUtil::StrToDouble( (*itr)._name );
        ++itr;

        while( itr != pParam->_scanFileList.end( ) ) {
            double val = ShUtil::StrToDouble( (*itr)._name );
            if( min > val ) min = val;            
            if( max < val ) max = val;            
            ++itr;
        }

        pPlot->GetXaxis( )->SetRangeUser( min - 0.5, max + 0.5 );        
    }
    
    return;
}

///////////////////////////////////////////////////////////
// GetXAxisValFromFileName
///////////////////////////////////////////////////////////
double GetXAxisValFromFileName( const String& fileName, PixHistoryPlotParameter* pParam )
{
    double retVal = 0.0;
    if( fileName.length( ) <= 0 || pParam == NULL ) return retVal;

    std::list< PixConfigScanFile >::iterator itr = pParam->_scanFileList.begin( );
    while( itr != pParam->_scanFileList.end( ) ) {

        PixConfigScanFile config = *itr;
        if( config._path == fileName ) {
            retVal = ShUtil::StrToDouble( config._name );
            break;
        }

        ++itr;
    }

    return retVal;
}


///////////////////////////////////////////////////////////
// DeleteListComponents
///////////////////////////////////////////////////////////
void
DeleteListComponents( std::list< PixFileHistPair* >* pList )
{
    if( pList == NULL ) return;

    std::list< PixFileHistPair* >::iterator itr = (*pList).begin( );
    while( itr != (*pList).end( ) ) {
        if( *itr != NULL )
            delete (*itr)->second;
        delete *itr;
        ++itr;
    }
}

///////////////////////////////////////////////////////////
// main
///////////////////////////////////////////////////////////
int main( ) {

    historyplot( );
    return 0;
}
