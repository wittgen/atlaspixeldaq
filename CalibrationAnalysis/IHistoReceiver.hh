#ifndef _CAN_IHistoReceiver_hh_
#define _CAN_IHistoReceiver_hh_

#include <string>
#include <Common.hh>
#include <vector>
#include <fstream>

class IPCPartition;

namespace CAN {

  class TopLevelHistoArray;

  class IHistoReceiver
  {
  public:
    virtual ~IHistoReceiver() {}

    /** Fill all histograms with the given ROD, connectivity and histogram name into the pix scan histo array.
     * @param scan_serial_number
     * @param rod_name the name of the ROD
     * @param conn_name the connectivity name
     * @param histo_name the pix scan histo name
     * @param array the pix scan histo array to be filled.
     * @return the number of retrieved histograms.
     * The histograms are expected to be named : "ROD"/"connectivity name"/"histogram name"/(Ax/)(Bx/)(Cx/)x:"original histogram name"
     * @todo what about analysis histograms ? 
     */
    virtual unsigned int getHistograms(SerialNumber_t scan_serial_number,
				       const std::string &rod_name,
				       const std::string &conn_name,
				       const std::string &histo_name,
				       TopLevelHistoArray &array) = 0;

    virtual std::vector<std::string>  availableHistograms(SerialNumber_t scan_serial_number,
							   const std::string &rod_name,
							   const std::string &conn_name){
      std::vector<std::string> all_scan_histo_names;
      return all_scan_histo_names; 
    };


  };

}
#endif
