//
//

#include "DataServerCT.hh"
#include "Histo/Histo.h"
#include <TCanvas.h>
#include <TFile.h>
#include <TString.h>
#include "AutomaticBOCAnalysis.hh"
#include "AutomaticT0analysis.hh"
#include "AutomaticTHRESHOLDAnalysis.hh"
#include "AutomaticDIGITALanalysis.hh"
#include "AutomaticANALOGanalysis.hh"
#include "AutomaticTOTanalysis.hh"
#include "AutomaticTotBothCapAnalysis.hh"
#include "AnalysisInfo_t.hh"


int main(int argc, char **argv)
{
  std::string dir,fileName,type;
  if(argc!=4) {
    std::cout << "USAGE: runAnalysis \"path\" \"CTfileName\" \"type\" " << std::endl;
    std::cout << "       known types are: BOC, THR, DIGI, ANALOG, TOT, T0 " << std::endl;
    return 1;
  }
  else {
    dir = argv[1];    
    fileName = argv[2];    
    type = argv[3];
  }

//   PixA::ConnectivityRef conn();
//   const std::string rod_name="";
//   MasterDataServer *master(NULL);
//   CAN::DataServerCT dataServer( conn, rod_name, master); 
  CAN::DataServerCT dataServer;   
  dataServer.setInputDir(dir);
  dataServer.addFile(fileName);

  //check if we extracted this allready, in this case ready from extracted file
  TFile f(("_"+fileName).c_str());  

  std::list<std::string> pp0_list = dataServer.getpp0Names();   //use the pp0 in the file! not the pp0 from the dummy connectivity! 
  std::list<std::string>::const_iterator pp0_iter=pp0_list.begin();
  std::list<std::string>::const_iterator pp0_list_end=pp0_list.end();
  for(; pp0_iter != pp0_list.end(); ++pp0_iter) {
    // starting a seperate analysis for each pp0 !!
    std::cout << ">runAnalysis: Current pp0 is: " << *pp0_iter << std::endl;

    if( !f.IsZombie() ) {
      AnalysisInput_t analysisInput;
      //    f.GetObject("scanHistos;1",analysisInput);
      f.GetObject("scanHistos_"+TString(*pp0_iter)+";1",analysisInput);
      if(analysisInput==NULL) {
	cout << "NO analysisInput FOUND FOR  " << TString(*pp0_iter) << endl;
      }
      
      if(type=="BOC") {
	BOCanalysisROOT analysis; 
	analysis.Analyse(analysisInput);
      }
      if(type=="DIGI") {
	DIGITALanalysisROOT analysis; 
	analysis.Analyse(analysisInput);
      }
      if(type=="ANALOG") {
	ANALOGanalysisROOT analysis; 
	analysis.Analyse(analysisInput);
      }
      if(type=="TOT") {
	TOTanalysisROOT analysis; 
	analysis.Analyse(analysisInput);
      }
      if(type=="THR") {     
	THRESHOLDanalysisROOT analysis;
	analysis.Analyse(analysisInput);
      }
      if(type=="T0") {
      	T0analysisROOT analysis;
	analysis.Analyse(analysisInput);
      }
    }
    else {
      cout << ">runAnalysis: AnalysisInput root file either not existing or corrupted, reading PixDb file..." << endl;
      
      const PixA::Pp0LocationBase pp0_location(*pp0_iter); 
      
      if(type=="BOC") {
	CAN::AutomaticBOCAnalysis ana(111111,CAN::ObjectInfo_t());
	//const PixA::Pp0LocationBase pp0_location("L2_B25_S2_C6"); //for SCAN_24Aug07_BOC_Fast_C6C7.root
	try {
	  ana.requestHistograms( &dataServer, pp0_location);  
	}
	catch(...) {
	  return -1;
	} 
	CAN::AnalysisResultList_t analysis_results;
	ana.analyse( &dataServer, pp0_location, analysis_results);
      }
      if(type=="DIGI") {
	CAN::AutomaticDIGITALanalysis ana(111111,CAN::ObjectInfo_t());
	//const PixA::Pp0LocationBase pp0_location("L2_B25_S2_C6");
	try {
	  ana.requestHistograms( &dataServer, pp0_location);
	}
	catch(...) {
	  return -1;
	} 
	CAN::AnalysisResultList_t analysis_results;
	ana.analyse( &dataServer, pp0_location, analysis_results);
      }
      if(type=="ANALOG") {
      //  std::cout << "I am calling the requestHistograms." << std::endl;
	CAN::AutomaticANALOGanalysis ana(111111,CAN::ObjectInfo_t());
	//const PixA::Pp0LocationBase pp0_location("L2_B25_S2_C6");
	try {
	  ana.requestHistograms( &dataServer, pp0_location);
	}
	catch(...) {
	  return -1;
	} 
	CAN::AnalysisResultList_t analysis_results;
      ana.analyse( &dataServer, pp0_location, analysis_results);
      }
      if(type=="TOT") {
	CAN::AutomaticTOTanalysis ana(111111,CAN::ObjectInfo_t());
	//const PixA::Pp0LocationBase pp0_location("L2_B25_S2_C6"); //for SCAN_24Aug07_BOC_Fast_C6C7.root
	try {
	  ana.requestHistograms( &dataServer, pp0_location);
	}
	catch(...) {
	  return -1;
	} 
	CAN::AnalysisResultList_t analysis_results;
	ana.analyse( &dataServer, pp0_location, analysis_results);
      }
      if(type=="THR") {
	CAN::AutomaticTHRESHOLDAnalysis ana(111111,CAN::ObjectInfo_t());
	//  const PixA::Pp0LocationBase pp0_location("L2_B25_S2_C6"); //in SCAN_28AUG07_C6_E50_V12.root
	//const PixA::Pp0LocationBase pp0_location("L2_B25_S2_C6"); //SCAN_28AUG07_C6_E50_V12.root  threshold
	try {
	  ana.requestHistograms( &dataServer, pp0_location);
	}
	catch(...) {
	  return -1;
	} 
	ana.requestHistograms( &dataServer, pp0_location);
	CAN::AnalysisResultList_t analysis_results;
	ana.analyse( &dataServer, pp0_location, analysis_results);
      }
      if(type=="TO") {
	CAN::AutomaticTOTanalysis ana(111111,CAN::ObjectInfo_t());
	//const PixA::Pp0LocationBase pp0_location("L2_B25_S2_C6"); //for SCAN_24Aug07_BOC_Fast_C6C7.root
	try {
	  ana.requestHistograms( &dataServer, pp0_location);
	}
	catch(...) {
	  return -1;
	} 
	CAN::AnalysisResultList_t analysis_results;
	ana.analyse( &dataServer, pp0_location, analysis_results);
      }
      f.Close();
    }
  }//end of loop over pp0's
  
//  std::cout << "runAnalysis done." << std::endl;
  return 0;
}
