#ifndef _CAN_IArchivingUnit_hh_
#define _CAN_IArchivingUnit_hh_

#include "LogLevel.hh"
#include "Status.hh"
#include <string>

namespace CAN {

  class ExtendedAnalysisResultList_t;

  /** Interface of the unit which archives the analysis results and histograms.
   */
  class IArchivingUnit
  {
  public:
    virtual ~IArchivingUnit() {}

    /** Add the given analysis result to the archiving queue.
     * @param analysis_serial_number the serial number of the analysis to which this result belongs.
     * @param rod_name the name of the ROD to which this result belongs.
     * @param final_status the final status of the anlysis which could still get reduced to failed or aborted.
     * @param is_name the name which will be used when publishing the final analysis status in IS.
     * @param analysis_results the analysis result structure.
     * NOTE: The ownership of the analysis results will be handed over to the archiving unit.
     */
    virtual void queueForArchiving(SerialNumber_t analysis_serial_number,
				   const std::string& rod_name,
				   AnalysisStatus::EStatus final_status, 
				   const std::string &is_name,
				   ExtendedAnalysisResultList_t *analysis_results) = 0;

    /** Tell the archving unit to shutdown.
     * Will not wait until the shutdown is complete.
     */
    virtual void initiateShutdown() = 0;
    
    /** Remove all pending results from the queue.
     */
    virtual void reset() = 0;

    /** Wait until the archiver is shutdown.
     * This will also initiate the shutdown if not done already.
     */
    virtual void shutdownWait() = 0;

    /** Alter the log level of the archiver.
     */
    virtual void setLogLevel(ELogLevel log_level) = 0;

    /** Return true if the archiving unit still has something to archive.
     */
    virtual bool isArchiving() = 0;
  };
  
}
#endif
