#ifndef _CAN_ConfigDescription_hh_
#define _CAN_ConfigDescription_hh_

#include <string>

namespace CAN {

    class ConfigDescription
    {
    public:

      enum EConfigLevel { kNormal, kExpert, kDebug, kHidden, kNLevels };

      ConfigDescription() : m_level(kNormal) {}

      const std::string &className() const { return m_className; };
      const std::string &category() const  { return m_category; };
      const char *levelName() const        { return levelName(m_level); }
      EConfigLevel level() const          { return m_level; };

      void setClassName( const std::string &class_name) { m_className = class_name; }
      void setCategory( const std::string &category) { m_category = category; }
      void setLevel(EConfigLevel level) { m_level = level; }

      static const char *levelName(EConfigLevel level) { assert(level<kNLevels); return s_levelName[level]; };

    private:
      std::string  m_className;
      std::string  m_category;
      EConfigLevel m_level;
      static const char *s_levelName[kNLevels];
    };

}

#endif
