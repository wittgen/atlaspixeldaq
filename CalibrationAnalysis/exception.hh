#ifndef _CAN_exception_hh_
#define _CAN_exception_hh_

#include <BaseException.h>

//#include <mrs/message.h>
#include "PixUtilities/PixMessages.h"

namespace CAN {

// #ifndef _CAN_sendMessage_hh_
//   typedef severity MRSSeverity;
// #endif

  typedef SctPixelRod::BaseException BaseException;

  /** A MRS complian exception.
   * The exception features all the necessary information to produce messages for MRS
   * in the agreed format.
   */
  class  MRSException : public BaseException
  {
  public:
    /** Create an MRS complient exception for general exceptional cases which do not concern a connectivity object in particular.
     * @param message_id unique string for this exceptional message.
     * @param function_name the name of the function including namespace and class name in which the exception occurred.
     * @param mesage the error message.
     */
    MRSException(PixLib::PixMessages::MESSAGE_TYPE a_severity,
		 const std::string &function_name,
		 const std::string &message)
      : BaseException(message),
	m_severity(a_severity),
	m_functionName(function_name)
      {}

    /** Create an MRS complient exception for exceptional cases concerning individual connectivity objects (e.g. RODs).
     * @param message_id unique string for this exceptional message.
     * @param function_name the name of the function including namespace and class name in which the exception occurred.
     * @param connectivity_name the name of the connectivity object for which the exception occured.
     * @param mesage the error message.
     */
    MRSException(PixLib::PixMessages::MESSAGE_TYPE a_severity,
		 const std::string &function_name,
		 const std::string &connectivity_name,
		 const std::string &message) 
      : BaseException(message),
	m_severity(a_severity),
	m_functionName(function_name),
	m_connName(connectivity_name)
    {}

    /** Send the message of this exception to MRS.
     * @param m_out the MRS stream to which the message is sent.
     * @param caught_in the name of the methid which caught the exception.
     */
    //    void sendMessage(MRSStream &m_out, const std::string &caught_in);
    void sendMessage();//MRSStream &m_out, const std::string &caught_in);

    /** To change, the severity e.g. after catching the exception and before sending the message.
     * @param new_severity the new severity.
     */
    void setSeverity(PixLib::PixMessages::MESSAGE_TYPE new_severity) { m_severity = new_severity;}

//     /** Return the messageID.
//      */
//     const std::string &messageId() const {return m_messageId;}

  private:
    PixLib::PixMessages::MESSAGE_TYPE m_severity;
    std::string m_functionName;
    std::string m_connName;
  };


}
#endif
