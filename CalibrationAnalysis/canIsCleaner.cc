#include <ipc/partition.h>
#include <ipc/core.h>

#include <is/infodictionary.h>
#include <is/inforeceiver.h>
#include <is/infoiterator.h>
#include <is/callbackinfo.h>
#include <is/infoT.h>

#include <cassert>

#include <iostream>
#include <stdexcept>

#include <time.h>

int main(int argc, char **argv) {
  // GET COMMAND LINE

  // Check arguments

  std::string ipcPartitionName;
  std::string is_server("CAN");
  std::string pattern;
  bool cleanup_can_is_status=true;
  bool cleanup_global_status=false;
  bool test_run=false;

  for (int arg_i=1; arg_i<argc; arg_i++) {

    if (strcmp(argv[arg_i],"-p")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      ipcPartitionName = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-n")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      is_server = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-e")==0  && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      pattern=argv[++arg_i];
      cleanup_can_is_status=false;
    }
    else if (strcmp(argv[arg_i],"-f")==0 ) {
      cleanup_global_status=true;
    }
    else if (strcmp(argv[arg_i],"-t")==0 ) {
      test_run=true;
    }
    else {
      std::cout << "USAGE: " << argv[0] << " -p partition [-n IS-server-name] [-e pattern] [-f] [-t]."  << std::endl;
      return 1;
    }
  }

  if(ipcPartitionName.empty() || is_server.empty() || (pattern.empty() && !cleanup_can_is_status) ) {
    std::cout << "USAGE: " << argv[0] << " -p partition [-n IS-server-name] [-e pattern] [-f] [-t]."  << std::endl
	      << std::endl;
    return 1;
  }

  // START IPC

  // Start IPCCore
  IPCCore::init(argc, argv);

  IPCPartition ipcPartition(ipcPartitionName.c_str());
  if (!ipcPartition.isValid()) {
    std::cerr << "ERROR [" << argv[0] << ":main] Not a valid partition " << ipcPartitionName << std::endl;
    return EXIT_FAILURE;
  }


  for (unsigned int pass_i=0; pass_i<(cleanup_can_is_status && cleanup_global_status ? 2 : 1); pass_i++) {

    ISInfoDictionary  dictionary(ipcPartition);
    ISCriteria criteria(pattern);
    if ( cleanup_can_is_status ) {
      ISInfoString temp;
      if (pass_i==1) {
	std::cout << "INFO [" << argv[0] << ":main] remove *.globalStatus from "  << is_server << " in partition " << ipcPartitionName  << std::endl;
	temp.type() && ".*\\.globalStatus";
      }
      else {
	std::cout << "INFO [" << argv[0] << ":main] remove *.status from "  << is_server << " in partition " << ipcPartitionName  << std::endl;
	criteria = temp.type() && ".*\\.status";
      }
    }
    else {
      std::cout << "INFO [" << argv[0] << ":main] remove variables which match \"" << pattern << "\" from "  << is_server << " in partition " << ipcPartitionName  << std::endl;
      criteria = pattern;
    }

    {
      ISInfoIterator ii( ipcPartition, is_server, criteria );
      while( ii() ) {
	if (test_run) {
	  std::cout << "would remove : " << ii.name() << std::endl;
	}
	else {
	  dictionary.remove(ii.name());
	}
      }
    }
  }

  return 0;
}
