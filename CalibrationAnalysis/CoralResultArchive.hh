#ifndef _CAN_CoralResultArchive_hh_
#define _CAN_CoralResultArchive_hh_

#include "IResultStore.hh"
#include "PixelCoralClientUtils/CoralClient.hh"
#include "Common.hh"
#include "exception.hh"

#include <time.h>
#include "CanThread.hh"
#include <memory>

namespace CAN {

  /** Abstract interface of the class which archives the analysis results
   */
  class CoralResultArchive : public IResultStore, protected CanThread
  {
  public:
    CoralResultArchive() : m_closeAfterInactivity(90), m_lastUpdate(0) { start(); }

    ~CoralResultArchive() { CanThread::shutdownWait(); }
    /** Put the given object into the results store and archive it.
     * @param results the analysis result list object to be transferred to the results store.
     * The ResultsStore will take the ownership over the given analysis result list
     */
    void put(SerialNumber_t id, const std::string& rod_name, ExtendedAnalysisResultList_t *results) {
      std::string error_message;
      try {
	Lock lock(m_clientMutex);
	if (!m_client.get()) {
	  m_client=std::make_unique<PixCoralClient>();
	}

	// try three times
	// the problem could be a locked db.
	for (unsigned int pass_i=0; pass_i<10; pass_i++) {
	  m_lastUpdate=::time(NULL);
	  error_message="";
	  try {
	    m_client->fillTables(id,results);
	    if (pass_i>0) {
	      std::cout << "INFO [CoralResultArchive::put] Succeeded after " << pass_i << " attempts " << std::endl;
	    }
	    m_client->disconnect();
	    break;
	  }
	  catch ( std::exception& e ) {
	    std::stringstream attempts;
	    attempts << pass_i;
	    error_message = "Std exception (";
	    error_message += attempts.str();
	    error_message += ") : ";
	    error_message += e.what();

	    // if the database is locked or this strange KEYGEN error appears, then try again
	    // otherwise throw an exception

       
	    if (   error_message.find("database is locked")== std::string::npos
		   && error_message.find("table \"CALIB_KEYGEN\" already exists")==std::string::npos  /*does not seem to help*/
		   && error_message.find("disk I/O error")==std::string::npos) {
	      break;
	    }
	  }
	  catch (...) {
	    std::stringstream attempts;
	    attempts << pass_i;
	    error_message = "Unknown exception caught (";
	    error_message += attempts.str();
	    error_message += ") .";
	    break;
	  }
	  sleep(2);
	}
      }
      // COOL, CORAL POOL exceptions inherit from std exceptions: catching
      // std::exception will catch all errors from COOL, CORAL and POOL
      catch ( std::exception& e ) {
	error_message = "Std exception caught while opening connection: ";
	error_message += e.what();
      }
      catch (...) {
	error_message = "Unknown exception caught while opening connection.";
      }
      if (!error_message.empty()) {
	//	throw MRSException("ResultArchive", MRS_FATAL, "CAN::CoralResultArchive::put", error_message);
      }
    }

    void initiateShutdown() {
      CanThread::initiateShutdown();
    }
  protected:

    void wakeupForShutdown() {
      m_wait.setFlag();
    }

  private:

    void main() {
      // close connection to coral after some time
      while (!shutdownRequested() ) {
	m_wait.timedwait(m_closeAfterInactivity, shutdownFlag() );
	if (shutdownRequested()) break;
	{
	  Lock lock(m_clientMutex);
	  if (m_client.get() && difftime(::time(0),m_lastUpdate) > m_closeAfterInactivity) {
	    m_client.reset();
	  }
	}
      }
    }

    Flag         m_wait;

    unsigned int m_closeAfterInactivity; // in seconds
    time_t       m_lastUpdate;

    Mutex                         m_clientMutex;
    std::unique_ptr<PixCoralClient> m_client;
  };

}

#endif
