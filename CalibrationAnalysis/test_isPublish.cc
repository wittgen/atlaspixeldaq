#include <ipc/partition.h>
#include <ipc/core.h>

#include <is/infodictionary.h>
#include <is/infoiterator.h>
#include <is/infoT.h>


#include <vector>
#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <utility>
#include <time.h>
#include <unistd.h>

#include "isUtil.hh"

enum EStatus {kUnknown,kPending, kWaiting, kReady, kRunning, kSuccess, kFailure, kAborted};

int main(int argc, char **argv) {
  // GET COMMAND LINE

  // Check arguments

  std::string ipcPartitionName;
  std::string is_server("CAN");
  std::string is_name("S0000011.ROD_C1_S14");
  std::vector<std::pair<std::string, std::string> > values;
  std::vector<std::pair<std::string, int> > float_values;
  unsigned int verbose=0;

  bool reset_is=false;
  for (int arg_i=1; arg_i<argc; arg_i++) {

    if (strcmp(argv[arg_i],"-p")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      ipcPartitionName = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-n")==0 && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      is_server = argv[++arg_i];
    }
    else if (strcmp(argv[arg_i],"-r")==0) {
      reset_is=true;
    }
    else if (strcmp(argv[arg_i],"-v")==0) {
      verbose++;
    }
    else if (strcmp(argv[arg_i],"-q")==0) {
      verbose=0;
    }
    else if (argv[arg_i][0]=='-') {
      std::cout << "USAGE: " << argv[0] << " -p partition [-n IS-server-name] [var-name value [...] ] ."  << std::endl;
      return 1;
    }
    else {
      if (arg_i+1<argc && argv[arg_i+1][0]!='-' && argv[arg_i][0]!='-') {
	std::string var_name(argv[arg_i]);
	std::string value_name(argv[++arg_i]);
	bool is_string=false;

	for (unsigned int i=0; i<value_name.size(); i++) {
	  if (!isdigit(value_name[i])) {
	    is_string=true;
	    break;
	  }
	}
	std::cout << "INFO [main] new var " << var_name << " = " << value_name <<std::endl;
	if(is_string) {
	  values.push_back(std::make_pair(var_name, value_name));
	}
	else {
	  float_values.push_back(std::make_pair(var_name, atoi(value_name.c_str())));
	}
      }
      else {
	return 1;
      }
    }
  }

  if(ipcPartitionName.empty()) {
    std::cout << "USAGE: " << argv[0] << " -p partition [-n IS-server-name] var-name."  << std::endl;
    return 1;
  }

  // START IPC

  // Start IPCCore
  IPCCore::init(argc, argv);

  IPCPartition ipcPartition(ipcPartitionName.c_str());
  ISInfoDictionary    dict(ipcPartition);

  if (!values.empty() || !float_values.empty()) {
    if (verbose>0) {
      std::cout << "INFO [" << argv[0] << ":main] have " << values.size() << " string(s) and  " << float_values.size() << "." << std::endl;
    }

    for (std::vector<std::pair<std::string, std::string> >::const_iterator value_iter = values.begin();
	 value_iter != values.end();
	 ++value_iter) {

      try {
	ISInfoString is_string;
	ISType type(is_string.type());
	is_string.setValue(value_iter->second);
	setIsValue(dict, type, is_server+"."+ (value_iter->first), is_string);

	if (verbose>0) {
	  std::cout << "INFO [" << argv[0] << ":main] Set " << value_iter->first << " to " << value_iter->second << "." << std::endl;
	}
      }
      catch ( daq::is::Exception & ex ) {
	std::cout << "ERROR [" << argv[0] << ":main] failed to insert " << is_name << " into IS server " << is_server 
		  << " in partition " <<   ipcPartitionName << ". " << std::endl;
      }
    }

    for (std::vector<std::pair<std::string, int> >::const_iterator value_iter = float_values.begin();
	 value_iter != float_values.end();
	 ++value_iter) {

      try {

	ISInfoInt is_int;
	ISType type(is_int.type());
	is_int.setValue(value_iter->second);
	setIsValue(dict, type, is_server+"."+ (value_iter->first), is_int);
      }
      catch ( daq::is::Exception & ex ) {
	std::cout << "ERROR [" << argv[0] << ":main] failed to insert " << is_name << " into IS server " << is_server 
		  << " in partition " <<   ipcPartitionName << ". " << std::endl;
      }
    }
  }
  else {
  if (reset_is) {
    std::cout << "INFO [" << argv[0] << ":main] remove status from  IS server " << is_server 
	      << " in partition " <<   ipcPartitionName << ". " << std::endl;
    ISInfoInt test;
    ISInfoIterator ii( ipcPartition, is_server, test.type() && ".*\\.status" );
    while( ii() ) {
      dict.remove(ii.name());
      std::cout << "INFO [" << argv[0] << ":main] removed" << ii.name() << " from IS server " <<  is_server 
		<< " in partition " <<   ipcPartitionName << ". " << std::endl;
    }

  }
  else {
  std::vector<int> state_names;
  state_names.push_back(kUnknown);
  state_names.push_back(kPending);
  state_names.push_back(kWaiting);
  state_names.push_back(kReady);
  state_names.push_back(kRunning);
  state_names.push_back(kSuccess);
  state_names.push_back(kFailure);
  state_names.push_back(kAborted);

  ISInfoInt test_is(state_names[0]);
#ifdef TDAQ010601
  if (dict.insert(is_server+"."+is_name+".status",test_is) != ISInfo::Success) 
#else 
  try {
    dict.insert(is_server+"."+is_name+".status",test_is);
  }
  catch ( daq::is::Exception & ex )
#endif
  {
    std::cout << "ERROR [" << argv[0] << ":main] failed to insert " << is_name << " into IS server " << is_server 
	      << " in partition " <<   ipcPartitionName << ". " << std::endl;
  }


  for (unsigned int state_i=1; state_i<state_names.size(); state_i++) {
    sleep(1);
    test_is.setValue(state_names[state_i]);
    dict.update(is_server+"."+is_name+".status",test_is);
  }
  }
  }

}
