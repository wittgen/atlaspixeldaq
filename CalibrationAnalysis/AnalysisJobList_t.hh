#ifndef _CAN_AnalysisJobList_t_hh_
#define _CAN_AnalysisJobList_t_hh_

#include "AnalysisInfo_t.hh"
#include "Status.hh"
#include "WorkerInfo_t.hh"

#include "IsDictionary.hh"
#include <is/infoT.h>
#include <memory>

#include <cassert>

class ISCallbackInfo;

namespace CAN {

  class AnalysisMonitor;

    /** Status information about an analysis job for a particular ROD.
     * Status of submitted analysis jobs.
       * <ul>
       *    <li><i>kUnknown</i> - job is in an unknown state.</li>
       *    <li><i>kPending</i> - job is submitted to the worker and the execution request has been queued.</li>
       *    <li><i>kWaiting</i> - the execution request has been processed : the analysis is initialised, histograms 
       *                          are requested and the job is now waiting for the histograms and a free CPU core.</li>
       *    <li><i>kRunning</i> - the analysis algorithm, classification or post-processing is running.</li>
       *    <li><i>kSuccess</i> - the analysis ended successfully.</li>
       *    <li><i>kFailed</i>  - the analysis aborted or ended unsuccessfully.</li>
       * </ul>
     * @todo: can the state handling be improved ?
       */
    class JobInfo_t {
    public:

      /** Job was assigned to the given worker.
       * @param the_workter worker to which the job was assigned
       * @param expected_load the load this jobs is expected to cause on the worker.
       * It is expected that the load is already to the total worker load @sa WorkerInfo_t
       * The expected load will be removed from the total worker load once this job is
       * finished.
       */
      JobInfo_t(std::shared_ptr<WorkerInfo_t> &the_worker, unsigned int expected_load)
	: m_worker(the_worker),
	  m_finalStatus(AnalysisStatus::kSuccess),
	  m_lastStatusChange(time(NULL)),
	  m_expectedLoad(expected_load),
	  m_markedForResubmission(false)
      { assert(the_worker.get()); m_status = AnalysisStatus::kUnknown;}

      // The copy constructor will be called when creating the map
      // The original element will be deleted
      // to ensure that the load of the worker is set correctly 
      // the copy contructor also has to add load to the worker.
      // The destructor of the original job_info will take away 
      // the load which is now counted twice.
      JobInfo_t(const JobInfo_t &job_info)
	: m_worker(job_info.m_worker),
	  m_status( job_info.m_status ),
	  m_finalStatus(job_info.m_finalStatus),
	  m_lastStatusChange(job_info.m_lastStatusChange),
	  m_expectedLoad( job_info.m_expectedLoad),
	  m_markedForResubmission(job_info.m_markedForResubmission)
      { if (m_worker.get()) { m_worker->giveLoad(m_expectedLoad); }  }

      ~JobInfo_t() { takeLoadFromWorker(); }

      Worker_ptr worker()                 { assert(m_worker.get()); return m_worker->worker();}
      const Worker_ptr worker() const     { assert(m_worker.get());return m_worker->worker();}

      WorkerInfo_t &workerInfo()                { assert(m_worker.get());return *m_worker;}
      const WorkerInfo_t &workerInfo() const    { assert(m_worker.get());return *m_worker;}

      bool hasWorker() const {return m_worker.get(); }

      AnalysisStatus::EStatus status() const              { return m_status;}
      AnalysisStatus::EStatus expectedFinalStatus() const { return m_finalStatus;}

      void updateStatusFromIs() { m_status = AnalysisStatus::status( m_isStatus.getValue() ); if (isTerminated()) { takeLoadFromWorker(); } };
      void updateIsStatus() { m_isStatus.setValue( AnalysisStatus::name(m_status) ); };

      bool shouldBeAborted()        const { return m_finalStatus == AnalysisStatus::kAborted;}
      bool shouldFinishSuccessful() const { return m_finalStatus == AnalysisStatus::kSuccess;}

      void abortRequested()     {m_finalStatus=AnalysisStatus::kAborted;}

      bool isCorrectlyTerminated() const { return (isFinished() || isAborted()); }
      bool isTerminated() const { return m_status >= AnalysisStatus::kSuccess; }
      bool isTerminatedAsExpected() const { return (shouldBeAborted() ? isCorrectlyTerminated() : isSuccessful()); }
 
      bool isPending() const    {return m_status == AnalysisStatus::kPending;}
      bool isWaiting() const    {return m_status == AnalysisStatus::kWaiting;}
      bool isRunning() const    {return m_status == AnalysisStatus::kRunning;}
      bool isFinished() const   {return m_status == AnalysisStatus::kSuccess || m_status == AnalysisStatus::kFailure; }
      bool isSuccessful() const {return m_status == AnalysisStatus::kSuccess;}
      bool isAborted() const    {return m_status == AnalysisStatus::kAborted;}

      time_t lastStatusChange() const {return m_lastStatusChange;}

      void setStatus(AnalysisStatus::EStatus status) {
	m_status = status;
	if (isTerminated()) {
	  takeLoadFromWorker();
	}
      }

      ISInfoString &isStatus() { updateIsStatus(); return m_isStatus; }

      void setReferenceTime(time_t time) {m_lastStatusChange = time;}

      void markForResubmission()          { m_worker=std::shared_ptr<WorkerInfo_t> (); m_markedForResubmission=true; }
      bool markedForResubmission()  const { return m_markedForResubmission; }

      void resubmit( std::shared_ptr<WorkerInfo_t> &new_worker ) 
      { m_worker = new_worker; m_markedForResubmission=false; m_lastStatusChange=time(NULL); }

      unsigned int expectedLoad() const    { return m_expectedLoad; }

    private:
      void takeLoadFromWorker() {
	if (m_worker.get()) {
	  m_worker->takeLoad( m_expectedLoad );
	  m_expectedLoad=0;
	}
      }

      std::shared_ptr<WorkerInfo_t>  m_worker;
      ISInfoString                     m_isStatus;
      AnalysisStatus::EStatus          m_status;
      AnalysisStatus::EStatus          m_finalStatus;
      time_t                           m_lastStatusChange;
      unsigned int                     m_expectedLoad;
      bool                             m_markedForResubmission;
    };

    class AnalysisJobList_t
    {
    private:
      AnalysisJobList_t(const AnalysisJobList_t &a) {}

    public:

      AnalysisJobList_t(const AnalysisInfoData_t &analysis_info,
			AnalysisMonitor &is_server_info,
			const std::string &is_header)
	: m_analysisMonitor(&is_server_info),
	  m_isHeader(is_header),
	  m_abort(false)
      {
	setAnalysisInfo(analysis_info);
	m_globalStatus=AnalysisStatus::kSubmitted;
	subscribe();
	setStatusChangeTime();
      }

      AnalysisJobList_t(const AnalysisInfo_t &analysis_info,
			AnalysisMonitor &is_server_info,
			const std::string &is_header)
	: m_analysisInfo(analysis_info),
	  m_analysisMonitor(&is_server_info),
	  m_isHeader(is_header),
	  m_abort(false)
      {
	m_globalStatus= AnalysisStatus::kSubmitted;
	subscribe();
	setStatusChangeTime();
      }

      ~AnalysisJobList_t() {
	unsubscribe();
      }

      void setAnalysisInfo(const AnalysisInfoData_t &analysis_info) {
	copyAnalysisInfo(analysis_info,m_analysisInfo);
      }

      const AnalysisInfo_t &info() const {return m_analysisInfo;};

      AnalysisInfo_t &info() {return m_analysisInfo;};

      //      void setConnectivity( PixA::ConnectivityRef &conn_ref) const {return m_connectivit=conn_ref;};
      //      const PixA::ConnectivityRef  connectivity() const {return m_connectivity;};

      void addJobs(const std::string &is_dictionary_header,
		   std::shared_ptr<WorkerInfo_t> &worker,
		   const std::vector< std::pair<unsigned int, std::string> > &rod_list);

      typedef std::map< std::string, JobInfo_t >::iterator iterator;
      typedef std::map< std::string, JobInfo_t >::const_iterator const_iterator;

      iterator begin()             {return m_jobs.begin();}
      const_iterator begin() const {return m_jobs.begin();}
      const_iterator end()   const {return m_jobs.end();}
      iterator find(const std::string &rod_name) { return m_jobs.find(rod_name); }
      void erase(iterator job_iter) { m_jobs.erase(job_iter); return;}

      void setGlobalStatus(AnalysisStatus::EStatus status) {
	m_globalStatus = status;
      }
      void updateIsGlobalStatus() {
	m_isGlobalStatus = AnalysisStatus::name( m_globalStatus );
      }
      void updateGlobalStatusFromIs() {
	m_globalStatus = AnalysisStatus::status( m_isGlobalStatus.getValue() );
      }
      AnalysisStatus::EStatus globalStatus() const {
	return m_globalStatus;
      }
      ISInfoString &isGlobalStatus() { updateIsGlobalStatus(); return m_isGlobalStatus; }
      void statusChanged(ISCallbackInfo *info);
      //      void globalStatusChanged(ISCallbackInfo *info);

      void setStatusChangeTime() { m_statusChangeTime=time(NULL);}
      const time_t &statusChangeTime() const { return m_statusChangeTime; }

      void setAbort()    { m_abort=true; setStatusChangeTime(); }
      bool abort() const { return m_abort; }

      bool isPlaceHolder() const { return m_abort && m_statusChangeTime==0 && m_jobs.empty(); }

    protected:
      void subscribe();
      void unsubscribe();

    private:
      AnalysisInfo_t m_analysisInfo;
      //PixA::ConnectivityRef   m_connectivity;

      std::map< std::string, JobInfo_t > m_jobs;
      ISInfoString                 m_isGlobalStatus;
      AnalysisStatus::EStatus      m_globalStatus;
      time_t                       m_statusChangeTime;

      AnalysisMonitor             *m_analysisMonitor;
      std::string                  m_isHeader;

      bool                         m_abort;

      static ISInfoString s_test;
    };

}
#endif
