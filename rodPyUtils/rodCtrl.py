#!/usr/bin/env python
from __future__ import print_function
import argparse
import subprocess
import os
import commands

#class CmdType(Enum):
#   flashFw = 'flashFw'
#   flashSw = 'flashSw'
#   def __str__(self):
#	return self.value

dryRun=False
echoOnly=False
echoOnDestOnly=False
printCmd=True

class Programs:
    def __init__(self,_flashPrg, _programFpga,_resetRod, _simpleVmeUart, _pixelSetup, _ubootImage,_masterFw):
        self.flashPrg = _flashPrg
        self.programFpga = _programFpga
        self.resetRod = _resetRod
        self.simpleVmeUart = _simpleVmeUart
        self.pixelSetup = _pixelSetup
        self.ubootImage = _ubootImage
        self.masterFw = _masterFw
# P1
prgP1 = Programs(
    _flashPrg='/det/pix/daq/staging-tdaq7/RodDaq/IblUtils/Vme/bin/x86_64-slc6-gcc62-opt/ProgramVirtexFlash',
    _programFpga='$ROD_DAQ/IblUtils/Vme/bin/x86_64-slc6-gcc62-opt/AceProgrammer',
#    _programFpga='/det/pix/daq/staging-tdaq7/RodDaq/IblUtils/Vme/bin/x86_64-slc6-gcc62-opt/AceProgrammer',
    _resetRod='$ROD_DAQ/IblUtils/Jp_25/bin/reset_ROD_FPGAs',
#    _simpleVmeUart='$ROD_DAQ/IblUtils/Vme/bin/x86_64-slc6-gcc62-opt/simpleVmeUart',
_simpleVmeUart='/det/pix/daq/staging-tdaq7/RodDaq/IblUtils/Vme/bin/x86_64-slc6-gcc62-opt/simpleVmeUart',
    _pixelSetup='source /det/pix/scripts/Default-pixel.sh current;',
    #_ubootImage='/detpriv/pix/daq/binaries/IBLROD/linux/master/sw/20191021/boot/default/u-boot.img',
    _ubootImage='~/linux/u-boot_boot.img',
    _masterFw='/detpriv/pix/daq/binaries/IBLROD/linux/master/fw/rodMaster_bootAll_bin.ace'
    )


#SR1
pixeldaq='/home/pixeldaq/daq/RodDaq'
prgSR1 = Programs(    
    _flashPrg='$ROD_DAQ/IblUtils/Vme/bin/x86_64-slc6-gcc62-opt/ProgramVirtexFlash',
    _programFpga=pixeldaq+'/IblUtils/Vme/bin/x86_64-slc6-gcc62-opt/AceProgrammer',
    _resetRod=pixeldaq+'/IblUtils/Jp_25/bin/reset_ROD_FPGAs',
    _simpleVmeUart=pixeldaq+'/IblUtils/Vme/bin/x86_64-slc6-gcc62-opt/simpleVmeUart',
    _pixelSetup='source /daq/releases/slc6-tdaq-07/zzzz_daq.sh;',
    _ubootImage='/mnt/pixel_mnt/data/Point1/pix/daq/binaries/IBLROD/M8/binaries/rodMaster/linux/official/u-boot.img',
    _masterFw='/mnt/pixel_mnt/data/Point1/pix/daq/binaries/IBLROD/M8/binaries/rodMaster/ufficial/rodMaster_bootAll_bin.ace'
    )



class Rod:
   def __init__(self, _slot, _name, _hostname, _ip, _netmask, _gateway,_serverip,
           _mac, _sbc,_location, _serverip2 = None):
      self.slot=_slot
      self.name=_name
      self.hostname=_hostname
      self.ip=_ip
      self.netmask=_netmask
      self.gateway=_gateway
      self.serverip=_serverip
      self.serverip2=_serverip2
      self.mac=_mac
      self.sbc=_sbc
      self.location=_location


   def __str__(self):
      return '{} {} {} {} {} {} {} {} {}'.format(self.slot, self.name,self.hostname, self.ip, self.netmask, self.gateway, self.serverip, self.mac, self.sbc)

   def isIblRod(self):
        return any( True for x in {'I1', 'C3_S11', 'C3_S18'} if( x in self.name) )

   def execute(self, cmds):
      if rod.location == "P1": prg = prgP1
      elif rod.location == "SR1": prg = prgSR1
      else: raise Exception('Unkonwn location')

      sbcCmd = []
      if echoOnly: sbcCmd.append('echo')

      # TODO this is becommeing ungly  - change the command implementation in next release
      # also need to allow passing dbg flag for start and getfromtftp
      rodCmd=False
      allowed_rodCmd = ['start', 'stop', 'restart', 'status', 'getfromtftp', 'start_dbg', 'getfromtftp_dbg']
      allowed_hcpCmd = ['ecrON', 'ecrOFF']
      allowed_sbcCmd = ['flashSw', 'flashFw', 'reset', 'simpleVmeUart', 'ping']
      allowed_scpCmd = ['put']

      if all(elem in allowed_rodCmd for elem in cmds):
           rodCmd=True
           # run on ROD
           #sbcCmd.extend(['sshpass', '-p', 'r88t', 'ssh', '-t','root@'+rod.ip, 'echo',
           sbcCmd.extend(['sshpass', '-p', 'r88t','ssh', '-t','-o', 'StrictHostKeyChecking=no', 'root@'+rod.ip, 'echo',
           '"Running $HOSTNAME" ;'])
      elif all(elem in allowed_sbcCmd for elem in cmds):
           # run on SBC
           sbcCmd.extend(['ssh', rod.sbc, 'echo "Running on $HOSTNAME";', prg.pixelSetup])
      elif all(elem in allowed_scpCmd for elem in cmds):
           pass
      elif all(elem in allowed_hcpCmd for elem in cmds):
           pass
      else:
           raise Exception('Mixing commands to be executed on the rod and on the sbc is not supported')


      
      for cmd in cmds:
         if echoOnDestOnly: sbcCmd.append('echo')
         if cmd == "flashSw":
            sbcCmd.extend([prg.flashPrg, str(rod.slot), prg.ubootImage, 'master', 
               '--ip', str(rod.ip), 
               '--mac', rod.mac,
               '--hostname', rod.hostname,
               '--serverip', rod.serverip,
               '--netmask', rod.netmask,
               '--gateway', rod.gateway,
               '--serverip2', rod.serverip2 if rod.serverip2 is not None else "0.0.0.0"
               ';'
               ])
            if dryRun:
               sbcCmd.extend( ['--dry'])
         elif cmd == "flashFw":
            sbcCmd.extend([prg.programFpga, str(rod.slot), prg.masterFw, ';' ])
         elif cmd == "reset":
            sbcCmd.extend([prg.resetRod, str(rod.slot), 'all yes', ';'])
         elif cmd == "simpleVmeUart":
            sbcCmd.extend([prg.simpleVmeUart, str(rod.slot), '0', ';'])
         elif cmd == "ping":
            sbcCmd.extend(['ping', str(rod.ip), '-c1', ';'])

         elif cmd in ['start', 'stop', 'status', 'restart', 'getfromtftp']:
            sbcCmd.extend(['/etc/init.d/S70pixel_daq', cmd])

         elif cmd in ["start_dbg", "getfromtftp_dbg"]:
            (cmd, opt) = cmd.split('_')
            sbcCmd.extend(['/etc/init.d/S70pixel_daq', cmd, 'dbg'])
         elif cmd in ["put"]:
            # TOD make this steerable and for dbg
            sw_binary = os.getenv('HOME')+'daq/installed/ppc_ppc_main'
            sbcCmd.extend(['sshpass', '-p','r88t','scp', '-o', 'StrictHostKeyChecking=no','/home/okepka/daq/installed/ppc/ppc_main', 'root@'+rod.ip+':/root' ])
         elif cmd == "ecrON":
             sbcCmd = commands.ecrON(sbcCmd, rod.name, rod.isIblRod())
         elif cmd == "ecrOFF":
             sbcCmd = commands.ecrOFF(sbcCmd, rod.name)
         else:
            raise Exception('Unknown command: {}'.format(cmd))


      if printCmd == True:
          print("Command: "+" ".join(map(str, sbcCmd)))

#      syscmd=subprocess.Popen(sbcCmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, env = os.environ.copy())
#      stdout,stderr = syscmd.communicate()
      syscmd=subprocess.Popen( "".join(sbcCmd), stdout=subprocess.PIPE, stderr=subprocess.STDOUT, env = os.environ.copy(), shell=True)
      stdout,stderr = syscmd.communicate()
      print(stdout.decode('utf8'))
      print(stderr)
      
	


netmask_P1='255.255.255.0'
serverip_P1='10.145.78.33'
netmask_SR1='255.255.0.0'
serverip_SR1='192.168.0.2'


def gen_crate(rods, crates, crate_name,slot_range,ip_part,mac_part,gw,nm,sbc,serverip,location,serverip2=None):
    
    # if crate has not yet been registered
    if not crate_name in crate_dict:
        crate_dict[crate_name] = []

    # add all rods, slot_range can be pair or list
    slots = slot_range
    if not isinstance(slot_range, list):
        (first,last) = slot_range
        slots = range(first,last+1)

    offset = 0
    if isinstance(ip_part, tuple):
        (ip_part, offset) = ip_part


    for s in slots:
        if s == 13: continue

        # rod lable
        name = 'ROD_{}_S{}'.format(crate_name, s)

        rod_map[name]=Rod(s, 
                            name, 
                            'rod-pix-{}-s{:02d}-master'.format(crate_name.lower(), s), 
                            '{}2{:02d}'.format(ip_part,s+offset), 
                            nm, gw, serverip,
                            '{}{:02x}'.format(mac_part,s),
                            sbc,
                            location)
        crate_dict[crate_name].append(name)

# book of crates and rods
crate_dict = {}
rod_map = {}

gen_crate(rod_map, crate_dict,
    crate_name='I1',slot_range=(6,21),ip_part="10.145.89.",mac_part="00:0a:35:42:51:",
    gw='10.145.89.1',nm=netmask_P1,sbc='sbc-pix-rcc-i-1',serverip=serverip_P1,location="P1")

gen_crate(rod_map, crate_dict,
    crate_name='L1',slot_range=(5,21),ip_part="10.145.97.",mac_part="00:0a:35:79:11:",
    gw='10.145.97.1',nm=netmask_P1,sbc='sbc-pix-rcc-l-1',serverip=serverip_P1,location="P1")

gen_crate(rod_map, crate_dict,
    crate_name='L2',slot_range=(5,21),ip_part="10.145.98.",mac_part="00:0a:35:79:12:",
    gw='10.145.98.1',nm=netmask_P1,sbc='sbc-pix-rcc-l-2',serverip=serverip_P1,location="P1")

gen_crate(rod_map, crate_dict,
    crate_name='L3',slot_range=(5,21),ip_part="10.145.99.",mac_part="00:0a:35:79:13:",
    gw='10.145.99.1',nm=netmask_P1,sbc='sbc-pix-rcc-l-3',serverip=serverip_P1,location="P1")

gen_crate(rod_map, crate_dict,
    crate_name='L4',slot_range=(5,21),ip_part="10.145.100.",mac_part="00:0a:35:79:14:",
    gw='10.145.100.1',nm=netmask_P1,sbc='sbc-pix-rcc-l-4',serverip=serverip_P1,location="P1")

gen_crate(rod_map, crate_dict,
    crate_name='B2',slot_range=(5,21),ip_part="10.145.104.",mac_part="00:0a:35:79:b2:",
    gw='10.145.104.1',nm=netmask_P1,sbc='sbc-pix-rcc-b-2',serverip=serverip_P1,location="P1")

gen_crate(rod_map, crate_dict,
    crate_name='B3',slot_range=(10,16),ip_part=("10.145.104.",30),mac_part="00:0a:35:79:b3:",
    gw='10.145.104.1',nm=netmask_P1,sbc='sbc-pix-rcc-b-3',serverip=serverip_P1,location="P1")

gen_crate(rod_map, crate_dict,
    crate_name='D1',slot_range=(9,17),ip_part="10.145.105.",mac_part="00:0a:35:79:d1:",
    gw='10.145.105.1',nm=netmask_P1,sbc='sbc-pix-rcc-d-1',serverip=serverip_P1,location="P1")

gen_crate(rod_map, crate_dict,
    crate_name='D2',slot_range=(9,17),ip_part=("10.145.105.",30),mac_part="00:0a:35:79:d2:",
    gw='10.145.105.1',nm=netmask_P1,sbc='sbc-pix-rcc-d-2',serverip=serverip_P1,location="P1")


# SR1
gen_crate(rod_map, crate_dict,
    crate_name='C1',slot_range=[7,17],ip_part="192.168.11.",mac_part="00:0a:35:79:11:",
    gw='192.168.11.1',nm=netmask_SR1,sbc='pixrcc03',serverip=serverip_SR1,location="SR1")
gen_crate(rod_map, crate_dict,
    crate_name='C3',slot_range=[8,11,15,18],ip_part="192.168.13.",mac_part="00:0a:35:80:13:",
    gw='192.168.13.1',nm=netmask_SR1,sbc='pixrcc06',serverip=serverip_SR1,location="SR1")
gen_crate(rod_map, crate_dict,
    crate_name='C4',slot_range=[7,18],ip_part="192.168.14.",mac_part="00:0a:35:81:14:",
    gw='192.168.14.1',nm=netmask_SR1,sbc='pixrcc02',serverip=serverip_SR1,location="SR1")




#for x in list(rod_map):
#   print(rod_map[x])

allowed_rods = []
for key, value in rod_map.items():
    allowed_rods.append(value.name)

allowed_crates = []
for key, value in crate_dict.items():
    allowed_crates.append(key)

allowed_targets = allowed_rods
allowed_targets.extend(allowed_crates)
allowed_targets.sort()


parser = argparse.ArgumentParser()
parser.add_argument('target', type=str, choices=allowed_targets, metavar='ROD/CRATE')
parser.add_argument('cmd', nargs="*", type=str, choices=[
    'flashSw', 'flashFw', 'reset', 'simpleVmeUart', 'ping',   # sbc commands
    'start', 'stop', 'restart', 'status', 'reboot', 'getfromtftp', "start_dbg", "getfromtftp_dbg", 'put', 'ecrON', 'ecrOFF' ])   # rod commands


opts=parser.parse_args()
target = opts.target
print(str(opts.target), str(opts.cmd))


if target in crate_dict:
   for rodname in crate_dict[target]:
      print("Working on: {}".format(rodname))
      rod = rod_map[rodname]
      rod.execute(opts.cmd)
elif target in rod_map:
      print("Working on: {}".format(target))
      rod = rod_map[target]
      rod.execute(opts.cmd)
else:
   raise Exception('Unknown target: {}'.format(target))

