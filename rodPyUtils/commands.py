


def ecrON(cmd, rodname, isIBL):

   if isIBL:
      cmd.extend(["HCP_recoAtECR_manager --rodName {0} --ch 0xFFFFFFFF --RecoAtECR_mode 8 --glbRegCfg 1 --pixRegCfg 1 --pixRegCfgMask 0xFFFFFFFF;".format( rodname)] )
   else:
      cmd.extend(["HCP_recoAtECR_manager --rodName {0} --ch 0xFFFFFFFF --RecoAtECR_mode 8 --mccCfg 1 --fei3GlobCfg 1;".format(rodname)] )

   cmd.extend(['sleep 0.5;', "HCP_recoAtECR_manager --rodName {0}  --RecoAtECR_mode 2;".format(rodname)] )

   return cmd
   


def ecrOFF(cmd, rodname):

   cmd.extend( [ "HCP_recoAtECR_manager --rodName {0} --RecoAtECR_mode 0".format(rodname)] )


   return cmd
