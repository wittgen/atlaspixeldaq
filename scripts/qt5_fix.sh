#!/bin/bash

filelist=$(find ${CMTCONFIG} -name "*.make")

for f in  ${filelist}
do

sed -i.bkp  "s|$QTDIR/bin/moc|LD_LIBRARY_PATH=$TDAQ_LD_LIBRARY_PATH $QTDIR/bin/moc|g" ${f}
sed -i.bkp  "s|$QTDIR/bin/rcc|LD_LIBRARY_PATH=$TDAQ_LD_LIBRARY_PATH $QTDIR/bin/rcc|g" ${f}
sed -i.bkp  "s|$QTDIR/bin/uic|LD_LIBRARY_PATH=$TDAQ_LD_LIBRARY_PATH $QTDIR/bin/uic|g" ${f}
done
