#!/bin/bash
#

echo From ${USER} on $(hostname) @ $(date +%s) [$(date)] >> $PIX_LOGS/DisableMessages.txt

export TDAQ_PARTITION=$1
export TDAQ_APPLICATION_NAME=$3
export TDAQ_ERS_ERROR="lstderr,mts"
export TDAQ_ERS_WARNING="lstderr,mts"
sendDisableMessageExe $2 $3 $4 | /usr/bin/tee -a $PIX_LOGS/DisableMessages.txt
