#!/usr/bin/python
import socket
import struct
UDP_IP="192.168.1.141"
UDP_PORT=10427
sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) 
sock.bind((UDP_IP, UDP_PORT))
proc_state=["NEW","RUN","READY","WAIT","TIM_WAIT","DELAY","DEAD"]
net_stat=[ "xmit","recv","fw","drop","chkerr","lenerr","memerr","rterr","proterr","opterr","err","cachehit"]
net_proto=["link","tcp","udp"]
mallinfo=["arena","uordblks","fordblks","keepcost"]

while True:
    size=0
    data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
    fmt="!IIIiI"
    size+=struct.calcsize(fmt)
    header=struct.unpack_from(fmt,data)    
    fmt="!%dI"%header[4];    
    sched_hist=struct.unpack_from(fmt,data[size:])
    size+=struct.calcsize(fmt)
    fmt="!I"
    nproc=struct.unpack_from(fmt,data[size:])[0]
    size+=struct.calcsize(fmt)
    fmt="!12sIBIBIII"
    print "kernel ticks: ",header[2]
    print "threads:      ",nproc
    print "%12s %8s %8s %8s %8s %8s %8s %8s"%("task name","pid","state","aticks","prio","stackaddr","stacksize","stackptr")
    for i in range(40): print "-",
    print
    for p in range(nproc):
        proc=struct.unpack_from(fmt,data[size:])
        size+=struct.calcsize(fmt)
        name=proc[0].rstrip('\0x00')
        if(name==""): name="UNKNOWN"
        print "%12s %8d %8s %8d %8d %8s %8s %8s"%(name,proc[1],proc_state[proc[2]],proc[3],proc[4],hex(proc[5]),hex(proc[6]),hex(proc[7]))
    fmt="!I" #network stats
    net=struct.unpack_from(fmt,data[size:])[0]
    size+=struct.calcsize(fmt)
    fmt="!12I"
    for n in range(net):
        ns=struct.unpack_from(fmt,data[size:])
        size+=struct.calcsize(fmt)
        for i in range(12):
            print "%16s: %8d    "%(net_proto[n]+"_"+net_stat[i],ns[i])
    fmt="!4I"
    malloc=struct.unpack_from(fmt,data[size:])
    size+=struct.calcsize(fmt)
    for i in range(4):
        print "%16s: %8d"%("malloc_"+mallinfo[i],malloc[i])
    print "packet length: ",size
