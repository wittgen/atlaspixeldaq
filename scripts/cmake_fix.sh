#!/bin/bash

filelist=$(find ${CMTCONFIG} -name "*.make")

for f in  ${filelist}
do

sed -i.bkp  "s|$CXX|\$\(CXX\)|g" ${f}
sed -i.bkp  "s|$CC|\$\(CC\)|g" ${f}
done
