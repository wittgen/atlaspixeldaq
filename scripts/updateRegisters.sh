#!/bin/bash

PIXELDAQ_HOME=/home/pixeldaq

pushd $(dirname ${0} ) >/dev/null
PIXELDAQ_ROOT=$(pwd)
PIXELDAQ_ROOT=${PIXELDAQ_ROOT/\/scripts/}
popd >/dev/null
echo "Inferring PIXELDAQ_ROOT from script location: ${PIXELDAQ_ROOT}"

ROD_MASTER_FW_DIR=${PIXELDAQ_ROOT}/RodDaq/IblDaq/RodMaster/Firmware
ROD_MASTER_SW_DIR=${PIXELDAQ_ROOT}/RodDaq/IblDaq/RodMaster/Software
ROD_SLAVE_FW_DIR=${PIXELDAQ_ROOT}/RodDaq/IblDaq/RodSlave/Firmware

outFile_HCP=${PIXELDAQ_ROOT}/RodDaq/IblUtils/HostCommandPattern/tools/dumpRodRegisters.cxx
outFile_inc=${ROD_MASTER_SW_DIR}/PPCpp/CmdFactory/Tools/inc/DumpRodRegisters.h
outFile_src=${ROD_MASTER_SW_DIR}/PPCpp/CmdFactory/Tools/src/DumpRodRegisters.cxx


function printMasterRegisters() {
	grep '^#define' ${ROD_MASTER_FW_DIR}/inc/rodMasterReg.hxx | awk '{ print $2 }' | grep -v '_BIT\|_WORD' | grep -v 'ROD_MASTER_REG_H\|VHDL_COMPILE'
}

function printSlaveRegisters() {
	grep '^#define' ${ROD_SLAVE_FW_DIR}/inc/rodSlaveReg.hxx | awk '{ print $2 }' | grep -v '_BIT\|_WORD' | grep -v 'ROD_SLAVE_REG_H\|SLV_VHDL_COMPILE_DATE\|SLV_COMMUNICATION_DPR_BASE'
}

function replaceByRegisterMaster() {
	string=${1}
	pattern1=${2}
	if [ -z ${3} ] ; then
	printMasterRegisters | while read line ; do
		echo -e "${string}" | sed "s/${pattern1}/${line}/g"
	done
	elif [ -z ${4} ] ; then
	pattern2=${3}
	printMasterRegisters | while read line ; do
		varLine=$( echo ${line} | sed -e 's/\([A-Z]\)\([A-Z]*\)/\1\L\2/g' -e 's/_\([0-9]*\)/\1/g' -e 's/^Slv//g' )
		output=$( echo "${string}" | sed "s/${pattern1}/${line}/g" | sed "s/${pattern2}/${varLine}/g" )
		case line in
		esac
		echo -e "${output}"
	done
	else
	pattern2=${3}
	pattern3=${4}
	somma=0
	printMasterRegisters | while read line ; do
		varLine=$( echo ${line} | sed -e 's/\([A-Z]\)\([A-Z]*\)/\1\L\2/g' -e 's/_\([0-9]*\)/\1/g' -e 's/^Slv//g' )
		output=$( echo "${string}" | sed "s/${pattern1}/${line}/g" | sed "s/${pattern2}/${varLine}/g" | sed "s/${pattern3}/${somma}/g" )
		let somma=somma+1
		case line in
		esac
		echo -e "${output}"
	done
	fi
}

function replaceByRegisterSlave() {
	string=${1}
	pattern1=${2}
	if [ -z ${3} ] ; then
	printSlaveRegisters | while read line ; do
		echo -e "${string}" | sed "s/${pattern1}/${line}/g"
	done
	elif [ -z ${4} ] ; then
	pattern2=${3}
	printSlaveRegisters | while read line ; do
		varLine=$( echo ${line} | sed -e 's/\([A-Z]\)\([A-Z]*\)/\1\L\2/g' -e 's/_\([0-9]*\)/\1/g' -e 's/^Slv//g' )
		output=$( echo "${string}" | sed "s/${pattern1}/${line}/g" | sed "s/${pattern2}/${varLine}/g" )
		case line in
		esac
		echo -e "${output}"
	done
	else
	pattern2=${3}
	pattern3=${4}
	somma=0
	printSlaveRegisters | while read line ; do
		varLine=$( echo ${line} | sed -e 's/\([A-Z]\)\([A-Z]*\)/\1\L\2/g' -e 's/_\([0-9]*\)/\1/g' -e 's/^Slv//g' )
		output=$( echo "${string}" | sed "s/${pattern1}/${line}/g" | sed "s/${pattern2}/${varLine}/g" | sed "s/${pattern3}/${somma}/g" )
		let somma=somma+1
		case line in
		esac
		echo -e "${output}"
	done
	fi
}


function replaceBycountRegisters() {
	string=${1}
	pattern1=${2}
	somma=0
	for lines in `printMasterRegisters` ; do
		let somma=$somma+1
	done
	echo -e "${string}" | sed "s/${pattern1}/${somma}/g"
}

function updateIncFile(){
	cat <<EOF >${outFile_inc}
	#ifndef _DUMPRODREGISTERS_H_
	#define _DUMPRODREGISTERS_H_
	/*
	 * Authors: K. Potamianos <karolos.potamianos@cern.ch>,
	 * L. Jeanty <laura.jeanty@cern.ch>
	 * 
	 * Modified by N. Giangiacomi <nico.giangiacomi@cern.ch>
	 * Date: 2018-Jan-10
	 *
	 * This command prints the current values of the registers on the ROD
	 * to both the PPC output stream as well as to the host via serialized results.
	 * An example of how to run it is available in:
	 * RodDaq/IblUtils/HostCommandPattern/test/dumpRodRegisters.cxx
	 * If dumpOnPPC = true, then the value of the registers is output to the kermit logger
	 * as well as returned to the host via the serial stream
	 * 
	 */

	#include "CppCompatibility.h" // Keep this header first!
	#include "RodCommand.h"
	#include "EasySerializable.h"

	class ResultInstance(RodRegisters), public EasySerializable { // declares type of command result
	 public:

	  // All the data we want back from the command
		uint32_t epcBaseValue;
		uint32_t epcLocalValue;
		uint32_t epcSlvARegsValue;
		uint32_t epcSlvARamValue;

		uint32_t epcSlvBRegsValue;
		uint32_t epcSlvBRamValue;
		uint8_t epcBocValue;
		uint32_t epcLocalCtrlValue;

		uint32_t epcLocalStatValue;
		uint32_t epcLocalSpMaskValue;
		uint32_t dspRegBaseValue;
		uint32_t designRegPPCValue;


		// ======================== Master Registers ==========================
EOF
	replaceByRegisterMaster "\tuint32_t @@@Value;" "%%%%" "@@@" >> ${outFile_inc}
	echo -e "\t//======================== Slave A Registers ==========================" >> ${outFile_inc}
	replaceByRegisterSlave "\tuint32_t @@@Value_A;" "%%%%" "@@@" >> ${outFile_inc}
	echo -e "\t//======================== Slave B Registers ==========================" >> ${outFile_inc}
	replaceByRegisterSlave "\tuint32_t @@@Value_B;" "%%%%" "@@@" >> ${outFile_inc}
	cat <<EOF >>${outFile_inc}

		//	SERIAL_H();
		SERIAL_H(
			prep(epcBaseValue); prep(epcLocalValue); prep(epcSlvARegsValue); prep(epcSlvARamValue);
			prep(epcSlvBRegsValue); prep(epcSlvBRamValue); prep(epcBocValue); prep(epcLocalCtrlValue);
			prep(epcLocalStatValue); prep(epcLocalSpMaskValue); prep(dspRegBaseValue); prep(designRegPPCValue);
EOF

	replaceByRegisterMaster "\t\t\tprep(@@@Value);" "%%%%" "@@@" >> ${outFile_inc}
	replaceByRegisterSlave "\t\t\tprep(@@@Value_A);" "%%%%" "@@@" >> ${outFile_inc}
	replaceByRegisterSlave "\t\t\tprep(@@@Value_B);" "%%%%" "@@@" >> ${outFile_inc}

	#replaceByRegisterMaster "\t\tprep(masterRegisters[???]);" "%%%" "@@@" "???" >> ${outFile_inc}

	cat <<EOF >>${outFile_inc}
	)

		  };

	// If inheriting from RodCommand, the command will also have its ID, until the TypeId class is updated
	class CommandInstance(DumpRodRegisters), public EasySerializable { // declares type of command
	public:
		typedef RodRegisters ResultType;
		ResultType result;

		static void dump();
		DumpRodRegisters();
	
		virtual void execute();
		virtual const Command* getResult() const {
			return &result;
		}

		virtual ~DumpRodRegisters() {}

		// the only parameter of this command
	 	bool	dumpOnPPC;

		SERIAL_H(prep(dumpOnPPC);)
	};
	#endif //_DUMPRODREGISTERS_H_
EOF
	echo "File ${outFile_inc} updated!"
}

function updateHostCommandPatternFile() {
	cat <<EOF >${outFile_HCP}
	#include "DumpRodRegisters.h"
	#include <iostream>
	#include "DefaultClient.h"


	#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

	// what the client (the host) should do
	// in summary, the host does this:
	// 1) instantiate a command
	// 2) configure the command
	// 3) connect to the ppc i.e. server  (done automatically by DefaultClient)
	// 4) send the command to the ppc
	// 5) disconnect from server (done automatically by DefaultClient)
	// 6) process results

	int main(int argc, char** argv){

		DumpRodRegisters cmd;
		cmd.dumpOnPPC = true;
		if( getenv("NO_DUMP_ON_PPC") )
			cmd.dumpOnPPC = false;

		Options options(argc , argv);
		DefaultClient client(options);


		if(!client.run(cmd)) return EXIT_FAILURE;

		// print result on host
		std::cout<<" ===================================================================="<<std::endl;
		std::cout<<" ROD with IP: "<<options.getIp()<<" register values: "<<std::endl;
		std::cout<<" ===================================================================="<<std::endl;
		std::cout<<endl;

		std::cout.width(30); std::cout<<std::left<<" epcBaseValue = "<<HEX(cmd.result.epcBaseValue)<<std::endl;	        
		std::cout.width(30); std::cout<<std::left<<" epcLocalValue = "<<HEX(cmd.result.epcLocalValue)<<std::endl;	        
		std::cout.width(30); std::cout<<std::left<<" epcSlvARegsValue = "<<HEX(cmd.result.epcSlvARegsValue)<<std::endl;	        
		std::cout.width(30); std::cout<<std::left<<" epcSlvARamValue = "<<HEX(cmd.result.epcSlvARamValue)<<std::endl;	        
		std::cout<<endl;

		std::cout.width(30); std::cout<<std::left<<" epcSlvBRegsValue = "<<HEX(cmd.result.epcSlvBRegsValue)<<std::endl;	        
		std::cout.width(30); std::cout<<std::left<<" epcSlvBRamValue = "<<HEX(cmd.result.epcSlvBRamValue)<<std::endl;	        
		std::cout.width(30); std::cout<<std::left<<" epcBocValue = "<<HEX(cmd.result.epcBocValue)<<std::endl;	        
		std::cout.width(30); std::cout<<std::left<<" epcLocalCtrlValue = "<<HEX(cmd.result.epcLocalCtrlValue)<<std::endl;	        
		std::cout<<endl;
		std::cout.width(30); std::cout<<std::left<<" epcLocalStatValue = "<<HEX(cmd.result.epcLocalStatValue)<<std::endl;	        
		std::cout.width(30); std::cout<<std::left<<" epcLocalSpMaskValue = "<<HEX(cmd.result.epcLocalSpMaskValue)<<std::endl;  
		std::cout.width(30); std::cout<<std::left<<" dspRegBaseValue = "<<HEX(cmd.result.dspRegBaseValue)<<std::endl;	        
		std::cout.width(30); std::cout<<std::left<<" designRegPPCValue = "<<HEX(cmd.result.designRegPPCValue)<<std::endl;         
		std::cout<<endl;
		std::cout << "\n\n ======================== Master Registers ==========================\n";
EOF

	replaceByRegisterMaster "\t\tstd::cout.width(30); std::cout<<std::left<<\" @@@ = \"<<HEX(cmd.result.@@@Value)<<std::endl;"  "%%%%" "@@@" >> ${outFile_HCP}
	echo "		std::cout << \"\\n\\n ======================== Slave A Registers ==========================\\n\";" >> ${outFile_HCP}
	replaceByRegisterSlave "\t\tstd::cout.width(30); std::cout<<std::left<<\" @@@_A = \"<<HEX(cmd.result.@@@Value_A)<<std::endl;"  "%%%%" "@@@" >> ${outFile_HCP}
	echo "		std::cout << \"\\n\\n ======================== Slave B Registers ==========================\\n\";" >> ${outFile_HCP}
	replaceByRegisterSlave "\t\tstd::cout.width(30); std::cout<<std::left<<\" @@@_B = \"<<HEX(cmd.result.@@@Value_B)<<std::endl;"  "%%%%" "@@@" >> ${outFile_HCP}

	cat <<EOF >>${outFile_HCP}

		std::cout<<endl;

		std::cout<<" ===================================================================="<<std::endl;


		return 0;
	}
EOF
	echo "File ${outFile_HCP} updated!"
}



function updateSrcFile() {
	cat <<EOF >${outFile_src}
	/*
	 * L. Jeanty <laura.jeanty@cern.ch>
	 * Date: 2013-Sep-30
	 */

	#include "DumpRodRegisters.h"
	#include "RodMasterRegisters.h"

	#ifdef __XMK__
	#include "HwRegsInCpp.h"
	#endif

	#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

	void DumpRodRegisters::dump() {

		std::cout << " ===================================================================="<<std::endl;
		std::cout << std::endl;  
		std::cout << "EpcBase @ " << HEX( IblRod::EpcBase::addr() ) << ": " << HEX( IblRod::EpcBase::read() ) << std::endl;
		std::cout << "EpcLocal @ " << HEX( IblRod::EpcLocal::addr() ) << ": " << HEX( IblRod::EpcLocal::read() ) << std::endl;
		std::cout << "SlaveA::RegBase @ " << HEX( IblRod::SlaveA::RegBase::addr() ) << ": " << HEX( IblRod::SlaveA::RegBase::read() ) << std::endl;
		std::cout << "SlaveA::Ram @ " << HEX( IblRod::SlaveA::Ram::addr() ) << ": " << HEX( IblRod::SlaveA::Ram::read() ) << std::endl;
		std::cout << "SlaveB::RegBase @ " << HEX( IblRod::SlaveB::RegBase::addr() ) << ": " << HEX( IblRod::SlaveB::RegBase::read() ) << std::endl;
		std::cout << "SlaveB::Ram @ " << HEX( IblRod::SlaveB::Ram::addr() ) << ": " << HEX( IblRod::SlaveB::Ram::read() ) << std::endl;
		std::cout << "EpcBoc @ " << HEX( IblRod::EpcBoc::addr() ) << ": " << HEX( IblRod::EpcBoc::read() ) << std::endl;
		std::cout << "EpcLocalCtrl @ " << HEX( IblRod::EpcLocalCtrl::addr() ) << ": " << HEX( IblRod::EpcLocalCtrl::read() ) << std::endl;
		std::cout << "EpcLocalStat @ " << HEX( IblRod::EpcLocalStat::addr() ) << ": " << HEX( IblRod::EpcLocalStat::read() ) << std::endl;
		std::cout << "EpcLocalSpMask @ " << HEX( IblRod::EpcLocalSpMask::addr() ) << ": " << HEX( IblRod::EpcLocalSpMask::read() ) << std::endl;
		std::cout << "DesignRegPPC @ " << HEX( IblRod::DesignRegPPC::addr() ) << ": " << HEX( IblRod::DesignRegPPC::read() ) << std::endl;
		std::cout << std::endl;
		std::cout << "\n\n ======================== Master Registers ==========================\n";
EOF
	replaceByRegisterMaster "\t\tstd::cout << \"@@@ (0x\" << HEX( IblRod::Master::@@@::addr() ) << \"):= \" << HEX( IblRod::Master::@@@::read() ) << std::endl;" "%%%%" "@@@" >> ${outFile_src}
	echo "		std::cout << \"\\n\\n ======================== Slave A Registers ==========================\\n\";" >> ${outFile_src}
	replaceByRegisterSlave "\t\tstd::cout << \"@@@ (0x\" << HEX( IblRod::SlaveA::@@@::addr() ) << \"):= \" << HEX( IblRod::SlaveA::@@@::read() ) << std::endl;" "%%%%" "@@@" >> ${outFile_src}
	echo "		std::cout << \"\\n\\n ======================== Slave B Registers ==========================\\n\";" >> ${outFile_src}
	replaceByRegisterSlave "\t\tstd::cout << \"@@@ (0x\" << HEX( IblRod::SlaveB::@@@::addr() ) << \"):= \" << HEX( IblRod::SlaveB::@@@::read() ) << std::endl;" "%%%%" "@@@" >> ${outFile_src}

	cat <<EOF >>${outFile_src}
		std::cout << " ====================================================================" << std::endl;
		std::cout<<std::endl;
	}

	DumpRodRegisters::DumpRodRegisters(){ 
		setResultPtr(); // necessary step to tell command where the result class is
		dumpOnPPC = true; // default to print info to kermit logger as well as return to host
	}

	// inside execute() the actual meat of the command happens on the ppc
	void DumpRodRegisters::execute() {

		// put stuff that should only happen on the PPC (not on the host nor on the emulator) 
		// inside of this ifdef

	#ifdef __XMK__
	  result.epcBaseValue = IblRod::EpcBase::read();
	  result.epcLocalValue = IblRod::EpcLocal::read();
	  result.epcSlvARegsValue = IblRod::SlaveA::RegBase::read();
	  result.epcSlvARamValue = IblRod::SlaveA::Ram::read();
	  result.epcSlvBRegsValue = IblRod::SlaveB::RegBase::read();
	  result.epcSlvBRamValue = IblRod::SlaveB::Ram::read();
	  result.epcBocValue = IblRod::EpcBoc::read();
	  result.epcLocalCtrlValue = IblRod::EpcLocalCtrl::read();
	  result.epcLocalStatValue = IblRod::EpcLocalStat::read();
	  result.epcLocalSpMaskValue = IblRod::EpcLocalSpMask::read();
	  result.dspRegBaseValue = IblRod::DspRegBase::read();
	  result.designRegPPCValue = IblRod::DesignRegPPC::read();

EOF
	replaceByRegisterMaster "\t\tresult.@@@Value = IblRod::Master::@@@::read();" "%%%%" "@@@" >> ${outFile_src}
	replaceByRegisterSlave "\t\tresult.@@@Value_A = IblRod::SlaveA::@@@::read();" "%%%%" "@@@" >> ${outFile_src}
	replaceByRegisterSlave "\t\tresult.@@@Value_B = IblRod::SlaveB::@@@::read();" "%%%%" "@@@" >> ${outFile_src}

	cat <<EOF >>${outFile_src}

		if(dumpOnPPC) dump();

	#else
		std::cout<<"nothing to do, in emulator mode"<<std::endl;
	#endif
	}

	//  LocalWords:  endl

EOF
	
	echo "File ${outFile_src} updated!"
}

################################# ACTUAL BEGINNING OF SCRIPT #######################################

source ./updateMasterRegisters.sh
source ./updateSlaveRegisters.sh
updateIncFile
updateSrcFile
updateHostCommandPatternFile


