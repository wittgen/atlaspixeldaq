#!/bin/bash

PIXELDAQ_HOME=.

pushd $(dirname ${0} ) >/dev/null
PIXELDAQ_ROOT=$(pwd)
PIXELDAQ_ROOT=${PIXELDAQ_ROOT/\/scripts/}
popd >/dev/null
echo "Inferring PIXELDAQ_ROOT from script location: ${PIXELDAQ_ROOT}"

ROD_MASTER_FW_DIR=${PIXELDAQ_ROOT}/RodDaq/IblDaq/RodMaster/Firmware
ROD_MASTER_SW_DIR=${PIXELDAQ_ROOT}/RodDaq/IblDaq/RodMaster/Software

outFile=${ROD_MASTER_SW_DIR}/PPCpp/SysConnect/inc/RodMasterRegistersFromMaster.hxx


function printSlaveRegisters() {
	grep '^#define' ${ROD_MASTER_FW_DIR}/inc/rodMasterReg.hxx | awk '{ print $2 }' | grep -v '_BIT\|_WORD' | grep -v 'ROD_MASTER_REG_H\|VHDL_COMPILE'
}

function replaceByRegister() {
	string=${1}
	pattern1=${2}
	if [ -z ${3} ] ; then
	printSlaveRegisters | while read line ; do
		echo "${string}" | sed "s/${pattern1}/${line}/g"
	done
	else
	pattern2=${3}
	printSlaveRegisters | while read line ; do
		varLine=$( echo ${line} | sed -e 's/\([A-Z]\)\([A-Z]*\)/\1\L\2/g' -e 's/_\([0-9]*\)/\1/g' -e 's/^Slv//g' )
		output=$( echo "${string}" | sed "s/${pattern1}/${line}/g" | sed "s/${pattern2}/${varLine}/g" )
		case line in
		esac
		echo "${output}"
	done
	fi
}

cat <<EOF >${outFile}
// This file was generated automatically by ${BASH_SOURCE[0]} at $( date )
#ifndef __ROD_MASTER_REGISTERS_FROM_MASTER_HXX__
#define __ROD_MASTER_REGISTERS_FROM_MASTER_HXX__
#include "HwRegsInCpp.h"
#include <stdint.h>
#include "MMapHandler.h"

#define REGISTER(type, addr, mask, off, policy) typedef reg_t<type, addr, mask, off, policy<type> >

#ifdef __XMK__

EOF

replaceByRegister "#define ROD_MASTER_%%% COMMON_REG_BASE + %%% * sizeof(uint32_t)" "%%%" >>${outFile}

cat <<EOF >>${outFile}

#endif

struct RodMasterRegistersFromMaster {
EOF

cat <<EOF >>${outFile}

	struct Master {
	protected:
#ifndef __XMK__
EOF
replaceByRegister "		static uint32_t ROD_MASTER_%%%;" "%%%"  >>${outFile}
cat <<EOF >>${outFile}
#endif
	public:
EOF
replaceByRegister "		typedef reg_t<uint32_t, ROD_MASTER_%%%, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > @@@;" "%%%" "@@@" >>${outFile}
cat <<EOF >>${outFile}
	};

EOF

cat <<EOF >>${outFile}
};
#endif
EOF

echo "File ${outFile} updated!"

outFile=${ROD_MASTER_SW_DIR}/PPCpp/SysConnect/inc/RodMasterRegistersFromMaster.ixx

cat <<EOF >${outFile}
// This file was generated automatically by ${BASH_SOURCE[0]} at $( date )
// Please do not include this file unless you know what you're doing!
#ifndef __ROD_MASTER_REGISTERS_FROM_MASTER_IXX__
#define __ROD_MASTER_REGISTERS_FROM_MASTER_IXX__
#ifndef __XMK__
EOF

replaceByRegister "uint32_t RodMasterRegistersFromMaster::Master::ROD_MASTER_%%%;" "%%%" >>${outFile}

cat <<EOF >>${outFile}
#endif
#endif
EOF

echo "File ${outFile} updated!"

