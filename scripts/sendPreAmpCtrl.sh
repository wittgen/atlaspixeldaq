#!/bin/bash
#
export TDAQ_PARTITION=${2:-ATLAS}
export TDAQ_ERS_ERROR="lstderr,mts"
export TDAQ_ERS_WARNING="lstderr,mts"
if test ${3:-Ctrl_Pixel} = "Ctrl_Pixel"; then
  if test ${1:-ON} = "ON"; then
    sendPreAmpCtrlExe ${1:-ON} Ctrl_PixelBarrel
    sendPreAmpCtrlExe ${1:-ON} Ctrl_PixelBLayer
    sendPreAmpCtrlExe ${1:-ON} Ctrl_PixelDisk
  else
    sendPreAmpCtrlExe ${1:-ON} Ctrl_Pixel
  fi
else
  sendPreAmpCtrlExe ${1:-ON} ${3:-Ctrl_Pixel}
fi
