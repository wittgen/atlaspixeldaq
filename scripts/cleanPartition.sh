#!/bin/bash

CMD=echo

[ -z ${1} ] && echo "Usage: ${0} partition" && echo "Please try pmg_kill_partition first!" && exit

[ x${1} != xall ]  && ps aux | grep ${1} | grep -v grep | awk '{ print $2 }' | xargs ${CMD} kill -9 && exit

[ x${1} == xinitial ] && ${CMD} rm -rf /tmp/nightly/initial && ${CMD} rm -rf /tmp/ProcessManager/${USER}/pmg:/$( hostname )/initial


for i in {es,is,ipc,mrs,rdb}_server  {mrs_audio,mrs}_receiver emon_conductor mrs_worker rdb_writer ; do
	echo "# Cleaning up ${i}"
	ps aux | grep ${i} | awk '{ print $2 }' | xargs ${CMD} kill -9 
done

