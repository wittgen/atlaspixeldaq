
wget -q -O - --no-check-certificate https://raw.github.com/petervanderdoes/gitflow/develop/contrib/gitflow-installer.sh | sudo env GIT_SSL_NO_VERIFY=true bash -s install stable

if [ $? -eq 0 ] ; then
	echo "=== Installation complete: please run 'git flow init -d' from within your git repository to get started ==="
else
	echo "=== Installation NOT complete! Check https://github.com/petervanderdoes/gitflow/wiki/Installing-manually for manual installation"
fi
