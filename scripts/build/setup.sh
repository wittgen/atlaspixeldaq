
[ ${PWD/afs.cern.ch/} == ${PWD} ] && echo "Please run this script for the ATLAS Pixel DAQ AFS release folder" && return

pushd $(dirname ${BASH_SOURCE[0]} ) >/dev/null
export PIXELDAQ_REL=${PWD}
popd >/dev/null
echo "Inferring base location from script: ${PIXELDAQ_REL}"

export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${PIXELDAQ_REL}/RodDaq/IblUtils/HostCommandPattern/lib
