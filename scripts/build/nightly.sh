#!/bin/bash


function parse_git_dirty {
  [[ $(git status 2> /dev/null | tail -n1) != "nothing to commit (working directory clean)" ]] && echo "-dirty"
}

PARALLEL=cat

# Setting PIXELDAQ_ROOT from script
pushd $(dirname ${0} ) >/dev/null
PIXELDAQ_ROOT=$(pwd)
PIXELDAQ_ROOT=${PIXELDAQ_ROOT/\/scripts*/}
popd >/dev/null
echo "# Inferring PIXELDAQ_ROOT from script location: ${PIXELDAQ_ROOT}"

[ -z ${FW_MAKE_JOBS} ] && FW_MAKE_JOBS=4
[ -z ${SW_MAKE_JOBS} ] && SW_MAKE_JOBS=4

ARGS="${@}"

for i in $@ ; do [ x${i} == x-p ] && PARALLEL="parallel --no-notice"; done

pushd ${PIXELDAQ_ROOT} >/dev/null

# Acquiring Xilinx licenses
. scripts/atlaspixeldaqrc

GIT_HASH=$( git rev-parse HEAD )
#GIT_HASH=$( git describe --always --dirty --abbrev=40 )
GIT_BRANCH=$( git rev-parse --abbrev-ref HEAD )
DATE=$(date +%Y%m%d%H%M%S)

echo "# Git branch: ${GIT_BRANCH} [${GIT_HASH}]"
echo "# Date: ${DATE}"

for i in RodDaq/IblDaq/Rod{Master,Slave}/{Firmware,Software} RodDaq/IblUtils/{HostCommandPattern,Boc} RodDaq/RodCrate VmeInterface PixLibInterface RodDaq/IblDaq/Boc/Firmware RodDaq/IblDaq/RodPrm ${PWD} ; do

#Ignoring Firmware folders when building software
[[ "${i/Firmware/}" != "${i}" && "${ARGS/--sw/}" != "${ARGS}" ]] && continue
[[ "${i/Firmware/}" != "${i}" && "${ARGS/--elf/}" != "${ARGS}" ]] && continue

#Ignoring software folders when building FW
[[ "${i/Firmware/}" == "${i}" && "${ARGS/--fw/}" != "${ARGS}" ]] && continue
[[ "${i/Firmware/}" == "${i}" && "${ARGS/--bit/}" != "${ARGS}" ]] && continue

#Ignoring ROD folders when building BOC
[[ "${i/Boc/}" == "${i}" && "${ARGS/--boc/}" != "${ARGS}" ]] && continue

#Ignoring BOC and PRM folders when building ROD
[[ "${i/IblDaq\/RodPrm/}" != "${i}" && "${ARGS/--rod/}" != "${ARGS}" ]] && continue
[[ "${i/IblDaq\/Rod/}" == "${i}" && "${ARGS/--rod/}" != "${ARGS}" ]] && continue

#Selecting master or slave related items
[[ "${i/RodMaster/}" == "${i}" && "${ARGS/--master/}" != "${ARGS}" ]] && continue
[[ "${i/RodSlave/}" == "${i}" && "${ARGS/--slave/}" != "${ARGS}" ]] && continue

#Selecting PRM related items
[[ "${i/RodPrm/}" == "${i}" && "${ARGS/--prm/}" != "${ARGS}" ]] && continue

#Selecting PWD related items
[[ "${i/${PWD}/}" == "${i}" && "${ARGS/--pwd/}" != "${ARGS}" ]] && continue

logDir=/tmp/atlaspixeldaq_nightly_${GIT_HASH}
mkdir -p ${logDir}
logFile=${logDir}/log_${i//\//_}

echo "# Building ${i}"

[ x${i/Boc\/Firmware} != x${i} ] && echo "make -j${FW_MAKE_JOBS} -C ${i} revD all >& ${logFile}" && continue
if [[ "${ARGS/--flashLoader/}" != "${ARGS}" ]] ; then
	[ x${i/Firmware/} != x${i} ] && echo "make -j${FW_MAKE_JOBS} -C ${i} bitloader_flash >& ${logFile/Firmware/FlashLoaderFirmware}" && continue
else
	[ x${i/Firmware/} != x${i} ] && echo "make -j${FW_MAKE_JOBS} -C ${i} bitloader >& ${logFile}" && continue
fi
[ x${i/Software/} != x${i} ] && echo "make -j${SW_MAKE_JOBS} -C ${i} >& ${logFile}" && continue
[ x${i} == x${PWD} ] && echo "make -C ${i} JOBS=${SW_MAKE_JOBS} >& ${logFile}" && continue

echo "make -C ${i} >& ${logFile}"

done | ${PARALLEL}

popd >/dev/null
