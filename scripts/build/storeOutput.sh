#!/bin/bash

pushd $(dirname ${0} ) >/dev/null
PIXELDAQ_ROOT=$(pwd)
PIXELDAQ_ROOT=${PIXELDAQ_ROOT/\/scripts*/}
popd >/dev/null
echo "Inferring PIXELDAQ_ROOT from script location: ${PIXELDAQ_ROOT}"

pushd ${PIXELDAQ_ROOT}

GIT_HASH=$( git describe --abbrev=40 --always )
GIT_HASH_SHORT=$( git describe --abbrev=10 --always )
GIT_BRANCH=$( git rev-parse --abbrev-ref HEAD )

logDir=/tmp/atlaspixeldaq_nightly_${GIT_HASH}
afsDir=/afs/cern.ch/user/p/pixeldaq/public/nightly/${GIT_BRANCH}/${GIT_HASH_SHORT}
mkdir -p ${afsDir}

# Git status, diff, etc.
git status -u > ${afsDir}/gitStatus
git diff > ${afsDir}/gitDiff

# List untracked files
if [ ! -f ${afsDir}/untracked.tar.gz ] ; then
git ls-files --others --exclude-standard -z | xargs -0 tar rvf ${afsDir}/untracked.tar
pushd ${afsDir} ; gzip -9 untracked.tar ; popd
fi

# List ignored files
if [ ! -f ${afsDir}/ignored.tar.gz ] ; then
git ls-files --others -i --exclude-standard -z | xargs -0 tar rvf ${afsDir}/ignored.tar
pushd ${afsDir} ; gzip -9 ignored.tar ; popd
fi

# Log files
if [ ! -f ${afsDir}/cron_log.tar.gz ] ; then
pushd ${logDir} ; tar czf ${afsDir}/cron_log.tar.gz * ; popd
fi


# Create iblRodCtrl

rsync -aP scripts/iblRodCtrl ${afsDir}/iblRodCtrl
sed -i -e "s@\(defaultFW_cpp\).*@\1=${afsDir}/RodDaq/IblDaq/RodMaster/Firmware/build/rodMaster_${GIT_HASH}.bit@" \
	-e "s@\(defaultSW_cpp\).*@\1=${afsDir}/RodDaq/IblDaq/RodMaster/Software/PPCpp/bin/PPCpp.elf@" \
	-e "s@\(defaultFW_sp6\).*@\1=${afsDir}/RodDaq/IblDaq/RodSlave/Firmware/build/sp6fmt_${GIT_HASH}.bit@" \
	-e "s@\(defaultSW_sp6\).*@\1=${afsDir}/RodDaq/IblDaq/RodSlave/Software/bin/histClient.elf@" \
	-e "s@\(defaultFW_ppc\).*@\1=${afsDir}/RodDaq/IblDaq/RodMaster/Firmware/build/rodMaster_${GIT_HASH}.bit@" \
	-e "s@\(defaultSW_ppc\).*@\1=${afsDir}/RodDaq/IblDaq/RodMaster/Software/PPC/bin/iblDsp_ppc.elf@" \
	${afsDir}/iblRodCtrl

# Synch the files:

rsync -aP --include "*/" --include "*${GIT_HASH}.bit" --include "*PPCpp.[bels]*" --include "*bin/histClient.[bels]*" \
	--include "*iblDsp_ppc.[bels]*" \
	--include "*IblUtils/v1/bin/*" --include "*IblUtils/v1/lib/*" \
	--include "*IblUtils/HostCommandPattern/bin/*" --include "*IblUtils/HostCommandPattern/lib/*" \
	--include "ReleaseNotes/*" --include "README_RELEASE" \
	--exclude "*" -m . ${afsDir}

rsync -aP scripts/build/setup.sh ${afsDir}/setup.sh

popd # PIXELDAQ_ROOT
