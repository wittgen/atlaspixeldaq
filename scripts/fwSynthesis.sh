#if [ -z ${PIXELDAQ_ROOT} ] ; then
        pushd $(dirname ${0} ) >/dev/null
        PIXELDAQ_ROOT=$(pwd)
        PIXELDAQ_ROOT=${PIXELDAQ_ROOT/\/scripts/}
        popd >/dev/null
        echo "Inferring PIXELDAQ_ROOT from script location: ${PIXELDAQ_ROOT}"
#fi

. $(dirname ${0})/atlaspixeldaqrc
setup_xilinx 14.5

make -C ${PIXELDAQ_ROOT}/RodDaq/IblDaq/RodSlave/Firmware bitloader
