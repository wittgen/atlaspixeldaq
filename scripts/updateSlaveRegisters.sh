#!/bin/bash

PIXELDAQ_HOME=.

pushd $(dirname ${0} ) >/dev/null
PIXELDAQ_ROOT=$(pwd)
PIXELDAQ_ROOT=${PIXELDAQ_ROOT/\/scripts/}
popd >/dev/null
echo "Inferring PIXELDAQ_ROOT from script location: ${PIXELDAQ_ROOT}"

ROD_SLAVE_FW_DIR=${PIXELDAQ_ROOT}/RodDaq/IblDaq/RodSlave/Firmware
ROD_MASTER_SW_DIR=${PIXELDAQ_ROOT}/RodDaq/IblDaq/RodMaster/Software

outFile=${ROD_MASTER_SW_DIR}/PPCpp/SysConnect/inc/RodSlaveRegistersFromMaster.hxx


function printSlaveRegisters() {
	grep '^#define' ${ROD_SLAVE_FW_DIR}/inc/rodSlaveReg.hxx | awk '{ print $2 }' | grep -v '_BIT\|_WORD' | grep -v 'ROD_SLAVE_REG_H\|SLV_VHDL_COMPILE_DATE\|SLV_COMMUNICATION_DPR_BASE'
}

function replaceByRegister() {
	string=${1}
	pattern1=${2}
	if [ -z ${3} ] ; then
	printSlaveRegisters | while read line ; do
		echo "${string}" | sed "s/${pattern1}/${line}/g"
	done
	else
	pattern2=${3}
	printSlaveRegisters | while read line ; do
		varLine=$( echo ${line} | sed -e 's/\([A-Z]\)\([A-Z]*\)/\1\L\2/g' -e 's/_\([0-9]*\)/\1/g' -e 's/^Slv//g' )
		output=$( echo "${string}" | sed "s/${pattern1}/${line}/g" | sed "s/${pattern2}/${varLine}/g" )
		case line in
			INMEM_FIFO_CH_SEL) output=$( echo "${output}" | sed 's/0xFFFFFFFF/0x00000003/g') ;;
		esac
		echo "${output}"
	done
	fi
}

cat <<EOF >${outFile}
// This file was generated automatically by ${BASH_SOURCE[0]} at $( date )
#ifndef __ROD_SLAVE_REGISTERS_FROM_MASTER_HXX__
#define __ROD_SLAVE_REGISTERS_FROM_MASTER_HXX__
#include "HwRegsInCpp.h"
#include <stdint.h>
#include "MMapHandler.h"
#define RAM_OFFS (SLV_COMMUNICATION_DPR_BASE * sizeof(uint32_t)) 

#define REGISTER(type, addr, mask, off, policy) typedef reg_t<type, addr, mask, off, policy<type> >

#ifdef __XMK__

EOF

for s in A B ; do
	echo "#define SLV_${s}_RAM (SLV_${s}_REG_BASE + RAM_OFFS)"
	replaceByRegister "#define SLV_${s}_%%% SLV_${s}_REG_BASE + %%% * sizeof(uint32_t)" "%%%"
done >> ${outFile}

cat <<EOF >>${outFile}

#endif

struct RodSlaveRegistersFromMaster {
EOF

for s in A B ; do
	cat <<EOF >>${outFile}

	struct Slave${s} {
	protected:
#ifndef __XMK__
EOF
	echo "		static uint32_t SLV_${s}_RAM;"
	echo "		static uint32_t SLV_${s}_REG_BASE;"
	replaceByRegister "		static uint32_t SLV_${s}_%%%;" "%%%" 
	cat <<EOF >>${outFile}
#endif
	public:
EOF
 	echo "		// Shouldn't be needed but just in case. Handle with care!"
    	echo "		typedef reg_t<uint32_t, SLV_${s}_REG_BASE, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_slave${s} > RegBase;"
	echo ""
	echo "		typedef reg_t<uint32_t, SLV_${s}_RAM, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_slave${s} > Ram;"
	replaceByRegister "		typedef reg_t<uint32_t, SLV_${s}_%%%, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_slave${s} > @@@;" "%%%" "@@@"
	cat <<EOF >>${outFile}
	};

EOF

done >>${outFile}

cat <<EOF >>${outFile}
};
#endif
EOF

echo "File ${outFile} updated!"

outFile=${ROD_MASTER_SW_DIR}/PPCpp/SysConnect/inc/RodSlaveRegistersFromMaster.ixx

cat <<EOF >${outFile}
// This file was generated automatically by ${BASH_SOURCE[0]} at $( date )
// Please do not include this file unless you know what you're doing!
#ifndef __ROD_SLAVE_REGISTERS_FROM_MASTER_IXX__
#define __ROD_SLAVE_REGISTERS_FROM_MASTER_IXX__
#ifndef __XMK__
EOF

for s in A B ; do
	echo "uint32_t RodSlaveRegistersFromMaster::Slave${s}::SLV_${s}_RAM;"
	echo "uint32_t RodSlaveRegistersFromMaster::Slave${s}::SLV_${s}_REG_BASE;"
	replaceByRegister "uint32_t RodSlaveRegistersFromMaster::Slave${s}::SLV_${s}_%%%;" "%%%"
done >>${outFile}

cat <<EOF >>${outFile}
#endif
#endif
EOF




echo "File ${outFile} updated!"

