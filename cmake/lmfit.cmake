include(ExternalProject)
#set(LMFIT_LIB  "${PROJECT_SOURCE_DIR}/installed/$ENV{CMTCONFIG}/lib/liblmfit.so")
#if (EXISTS  "${LMFIT_LIB}" )
#message(STATUS "lmfit already built")
#else()
message("Building lmfit")
ExternalProject_Add (
 lm-fit
 #GIT_REPOSITORY ../lmfit.git 
 #GIT_TAG v8.3
 PREFIX "${PROJECT_SOURCE_DIR}/external/build/"
 SOURCE_DIR  "${PROJECT_SOURCE_DIR}/external/build/lmfit"
 CONFIGURE_COMMAND cd ${PROJECT_SOURCE_DIR}/external/build/lmfit && mkdir -p build && cd build && cmake -DCMAKE_INSTALL_PREFIX=${PROJECT_SOURCE_DIR}/installed/$ENV{CMTCONFIG} ..
 BUILD_COMMAND cd ${PROJECT_SOURCE_DIR}/external/build/lmfit/build && ls -l && make -j4
 INSTALL_COMMAND  cd ${PROJECT_SOURCE_DIR}/external/build/lmfit/build && make install -j4
 UPDATE_COMMAND ""
)
#endif()
