

find_program ( CROSSCOMPILER_CPP powerpc-unknown-linux-uclibc-g++ PATHS /home/sw/toolchain/current/x-tools/powerpc-unknown-linux-uclibc/bin /det/pix/sw/toolchain/current/x-tools/powerpc-unknown-linux-uclibc/bin )
#For P1 at some point: /det/pix/sw/toolchain/20190306/x-tools/
if ( CROSSCOMPILER_CPP AND BUILD_PPC )
get_filename_component(CROSSCOMPILER_PATH ${CROSSCOMPILER_CPP} DIRECTORY)
message("PPC cross-compiler found: " ${CROSSCOMPILER_PATH})
ExternalProject_Add (
   PPC
   UPDATE_COMMAND ""
   INSTALL_COMMAND make install
   SOURCE_DIR ${PROJECT_SOURCE_DIR}/RodDaq
   BINARY_DIR ${PROJECT_SOURCE_DIR}/$ENV{CMTCONFIG}/PPC_build_dir
   CMAKE_ARGS -DCROSSCOMPILER_PATH=${CROSSCOMPILER_PATH} -DPPC_BUILD=1 -DCMAKE_TOOLCHAIN_FILE=ppc_toolchain.txt
   BUILD_ALWAYS      TRUE
)

elseif( CROSSCOMPILER_CPP )
	message("INFO: PPC cross-compiler found - but skipping cross compilation since BUILD_PPC is not set")
elseif( BUILD_PPC )
	message("WARNING: No PPC cross-compiler found despite asking for compilation- skipping cross compilation")
else()
	message("INFO: No PPC cross-compiler found, nor cross compilation selected - skipping")
endif()


