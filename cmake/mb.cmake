#search for Xilinx ISE_DS 14.7 installation 
find_program ( LIBGEN libgen PATHS /opt/Xilinx/14.7/ISE_DS/EDK/bin/lin )
if(LIBGEN AND BUILD_MB)
get_filename_component(LIBGEN_PATH ${LIBGEN} DIRECTORY)
message("Xilinx ISE_DS 14.7 found: " ${LIBGEN_PATH})
ExternalProject_Add (MB
UPDATE_COMMAND ""
INSTALL_COMMAND ""
CONFIGURE_COMMAND ""
SOURCE_DIR "${PROJECT_SOURCE_DIR}/RodDaq/IblDaq/RodSlave/Software"
BUILD_COMMAND cd ${PROJECT_SOURCE_DIR}/RodDaq/IblDaq/RodSlave/Software && make ALWAYS TRUE)

ExternalProject_Add_Step(MB clean
COMMAND cd ${PROJECT_SOURCE_DIR}/RodDaq/IblDaq/RodSlave/Software && make clean ALWAYS TRUE
EXCLUDE_FROM_MAIN TRUE
)
ExternalProject_Add_StepTargets(MB clean)

elseif( LIBGEN )
        message("INFO:  - but skipping MicroBlaze build since BUILD_MB is not set")
elseif( BUILD_MB )
        message("WARNING: No Xilinx ISE_DS 14.7 not found - skipping MicroBlaze build")
else()
        message("INFO: Xilinx ISE_DS 14.7 not found, nor cross MicroBlaze build selected - skipping")
endif()
