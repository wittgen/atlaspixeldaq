/////////////////////////////////////////////////////////////////////
// PixRCDCoolAgent.h
// version 1.1
/////////////////////////////////////////////////////////////////////

#ifndef _PIXRCD_PIXRCDCOOLAGENT
#define _PIXRCD_PIXRCDCOOLAGENT


//#include "ROSInterruptScheduler/InterruptHandler.h"
//#include "ROSInterruptScheduler/ScheduledUserAction.h"
//#include "ROSCore/SequentialDataChannel.h"
#include "ROSCore/ReadoutModule.h"
//#include "ROSModules/VMEInterruptDescriptor.h"
// To handle the config exception in the "try-catch" instructions
#include "DFSubSystemItem/ConfigException.h"

#include "ers/ers.h"

#include "ipc/core.h"
#include "ipc/partition.h"

#include "pmg/pmg_initSync.h"

#include "dal/Partition.h"
#include "dal/util.h"
#include "dal/IS_InformationSources.h"
#include "dal/IS_EventsAndRates.h"
#include "is/info.h"
#include "is/infodynany.h"
#include "is/infodictionary.h"
#include "is/infodocument.h"
#include "is/inforeceiver.h"
#include "is/type.h"
#include "rc/RunParams.h"
#include <TRP/LuminosityInfo.h>

#include "RunControl/Common/Exceptions.h"


// COOL include (from is2cool of GLM)
#include <CoralBase/Attribute.h>
#include <CoolKernel/Exception.h>
#include <CoolKernel/ValidityKey.h>
#include <CoolKernel/IDatabaseSvc.h>
#include <CoolKernel/IDatabase.h>
#include <CoolKernel/IFolder.h>
#include <CoolKernel/IRecordSpecification.h>
#include <CoolKernel/Record.h>
#include <CoolApplication/DatabaseSvcFactory.h>


// Forward declarations
class IPCPartition;

namespace SctPixelRod {
   class VmeInterface;
}

namespace PixLib {
  class RodCrateConnectivity;
  class PixISManager;
  class PixConnectivity;
  class PixDbServerInterface;
  class PixDisable;
}

namespace daq {
  namespace rc{
    class CoolDB_Pixel;
  }
}

namespace ROS {

  class PixRCDCoolAgent : public ReadoutModule {

    friend class daq::rc::CoolDB_Pixel;

  public:

    PixRCDCoolAgent();
    virtual ~PixRCDCoolAgent() noexcept (true);

    //state transitions
    virtual void setup(DFCountedPointer<ROS::Config> configuration);
    virtual void configure(const daq::rc::TransitionCmd&);
    virtual void connect(const daq::rc::TransitionCmd&);
    virtual void prepareForRun(const daq::rc::TransitionCmd&);
    virtual void pause(void);
    virtual void stopEB(const daq::rc::TransitionCmd&);
    virtual void disconnect(const daq::rc::TransitionCmd&);
    virtual void unconfigure(const daq::rc::TransitionCmd&);
    virtual void clearInfo();
    virtual DFCountedPointer<ROS::Config> getInfo();
    virtual void probe(void);
    virtual void publish() { this->probe(); } // Calling probe from publish, which is the replacement to probe in daq::rc::Controllable
    virtual void publishFullStatistics(void);
    virtual void cleanup();

    virtual const std::vector<DataChannel*>* channels();

    
  private:
    //Private methods to handle parameters and read from IS
    void updateParams(std::string& paramValue, std::string paramName, bool publish=true);
    void updateParams(unsigned int& paramValue, std::string paramName);
    void readFromSchema();
    void getTags_CrateList(bool mapIsGood);
    void putPixDisable();

    // RCD CONFIGURATION: ROD module DataChannels (unused)
    std::vector<DataChannel *> m_dataChannels;

    // ROD module configuration
    DFCountedPointer<ROS::Config> m_configuration;

    // DAL configuration
    Configuration* m_conf;

    // ROD module parameters:
    unsigned long long m_RunNo;
    std::string m_runType;
    unsigned int m_runTime; 
    std::string m_crateName;
    std::string m_partName;
    std::string m_connTag;
    std::string m_cfgTag; 
    std::string m_cfgModTag;
    std::string m_idTag;  
    unsigned int m_cfgRev;
    unsigned int m_cfgModRev;
    std::string m_disName;
    std::string m_newDisName;
    unsigned int m_disRev;
    std::string m_runName;
    unsigned int m_runRev;

    // List of dependent RODCRATES
    std::map<std::string, PixLib::RodCrateConnectivity*> m_rodCrates;
    std::set<std::string> actEnaROD; // Single actions scheduled for allocation
    std::set<std::string> actEnaTIM; // Single actions scheduled for allocation

    // Map for configurationDB objects parameters conversion
    std::map<std::string,std::string> m_convMap;

    // COOL records and stuff
    std::string m_SOR_Name;    
    cool::RecordSpecification m_SOR_Spec;
    daq::rc::CoolDB_Pixel *m_dbHandler;

    std::string m_SOR_Name_speed;    
    cool::RecordSpecification m_SOR_Spec_speed;

    std::string m_SOR_Name_HitDiscCnfg;
    cool::RecordSpecification m_SOR_Spec_HitDiscCnfg;

    std::string m_coolDb;
    std::string m_SQLiteDb;
    std::string m_folder;

    // SOFTWARE INFRASTRUCTURE  
    //ISManager
    std::string m_isName;
    PixLib::PixISManager * m_is;
    PixLib::PixISManager * m_isRunParams;
    std::string m_isNameDD;
    PixLib::PixISManager * m_isDD;

    // IPC partitions
    IPCPartition *m_ipcPartition;
    IPCPartition *m_ipcPartitionDD;

    // PixConnectivity
    PixLib::PixConnectivity* m_conn;
    PixLib::PixDbServerInterface* m_dbServer;

    // PixDisable object
    PixLib::PixDisable* m_disable;
    bool firstDisable;

    // Timing variables
    struct timeval m_t0;
    struct timeval m_t1;
    int m_time;
    bool m_pause;
  };

  inline const std::vector<DataChannel*>* PixRCDCoolAgent::channels() {
    return &m_dataChannels;
  }
  
}

#endif // _PIXRCD_PIXRCDCOOLAGENT





