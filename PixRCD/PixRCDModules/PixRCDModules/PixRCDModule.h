/////////////////////////////////////////////////////////////////////
// PixRCDModule.h
// version 1.1
/////////////////////////////////////////////////////////////////////
//
// 31/01/06  Version 1.0 (CS)
//           Initial release
//
// 23/02/06  Version 1.1 (CS)
//           First multithreaded implementation
//
// 11/10/06  Version 1.2 (CS)
//           Implementation using PixBrokers and PixActions
//
// 5/7/07    Version 1.3 (GAO)
//           First BrokerMultiCrate usage

//! Pixel RCD Module class

#ifndef _PIXRCD_PIXRCDMODULE
#define _PIXRCD_PIXRCDMODULE


//#include "ROSInterruptScheduler/InterruptHandler.h"
//#include "ROSInterruptScheduler/ScheduledUserAction.h"
//#include "ROSCore/SequentialDataChannel.h"
#include "ROSCore/ReadoutModule.h"
//#include "ROSModules/VMEInterruptDescriptor.h"
// To handle the config exception in the "try-catch" instructions
#include "DFSubSystemItem/ConfigException.h"
#include <atomic>


// Forward declarations
class IPCPartition;

namespace SctPixelRod {
   class VmeInterface;
}

namespace PixLib {
  class PixBrokerMultiCrate;
  class PixActions;
  class RodCrateConnectivity;
  class PixISManager;
  class PixConnectivity;
  class PixDbServerInterface;
  class PixDisable;
}

namespace ROS {

  class PixRCDModule : public ReadoutModule {

  public:

    PixRCDModule();
    virtual ~PixRCDModule();

    //state transitions
    virtual void setup(DFCountedPointer<ROS::Config> configuration);
    virtual void configure(const daq::rc::TransitionCmd&);
    virtual void connect(const daq::rc::TransitionCmd&);
    virtual void prepareForRun(const daq::rc::TransitionCmd&);
    virtual void pause(void);
    virtual void resume(void);
    virtual void stopROIB(const daq::rc::TransitionCmd&);
    virtual void stopDC(const daq::rc::TransitionCmd&);
    virtual void disconnect(const daq::rc::TransitionCmd&);
    virtual void unconfigure(const daq::rc::TransitionCmd&);
    virtual void clearInfo();
    virtual DFCountedPointer<ROS::Config> getInfo();
    virtual void probe(void);
    virtual void publish() { this->probe(); } // Calling probe from publish, which is the replacement to probe in daq::rc::Controllable
    virtual void publishFullStatistics(void);
    virtual void restartActions();
    virtual void cleanup();
    virtual void disable(const std::vector<std::string>&);
    virtual void enable(const std::vector<std::string>&);
    virtual void user(const daq::rc::UserCmd&);
    virtual void resynch(const daq::rc::ResynchCmd&);

    virtual const std::vector<DataChannel*>* channels();

  private:
    //Private methods to handle parameters and read from IS
    void updateParams(std::string& paramValue, std::string paramName, bool publish=true);
    
    void readFromSchema();
    void readPartNameFromSchema();
    void getTags_CrateList(bool mapIsGood);
    // Private method to create PixBrokerMultiCrate and allocate PixActionsSingle
    bool allocate();
    void disabledFromDCS(); 
    std::string disableModules();
    //Define the RodBusyMask to be sent to the PixActionsSingleTIM of the dependent crates and propagate it to the actions.
    void computeRodBusyMask();
    void putPixDisable();
    void checkLatency();

    // RCD CONFIGURATION: ROD module DataChannels (unused)
    std::vector<DataChannel *> m_dataChannels;

    // ROD module configuration
    DFCountedPointer<ROS::Config> m_configuration;
    // DAL configuration
    //Configuration* m_conf;

    // ROD module parameters:
    std::string m_crateName;
    std::string m_partName;
    std::string m_connTag;
    std::string m_cfgTag;
    std::string m_cfgModTag;
    std::string m_idTag;
    unsigned int m_cfgRev;
    unsigned int m_cfgModRev;
    std::string m_disName;
    std::string m_newDisName;
    unsigned int m_disRev;
    std::string m_runName;
    unsigned int m_runRev;

    // List of dependent RODCRATES
    std::map<std::string, PixLib::RodCrateConnectivity*> m_rodCrates;
    std::set<std::string> actEnaROD; // Single actions scheduled for allocation
    std::set<std::string> actEnaTIM; // Single actions scheduled for allocation

    // Map for configurationDB objects parameters conversion
    std::map<std::string,std::string> m_convMap;

      // SOFTWARE INFRASTRUCTURE  
    //ISManager
    std::string m_isName;
    PixLib::PixISManager * m_is;
    std::string m_is2Name;
    PixLib::PixISManager * m_is2;
    std::string m_isNameDD;
    PixLib::PixISManager * m_isDD;

    // IPC partitions
    IPCPartition *m_ipcPartition;
    IPCPartition *m_ipcPartitionDD;

    // Brokers and Actions
    PixLib::PixBrokerMultiCrate* m_broker;
    PixLib::PixActions* m_rodActions;
    PixLib::PixActions* m_timActions;

    // PixConnectivity
    PixLib::PixConnectivity* m_conn;
    PixLib::PixDbServerInterface* m_dbServer;

    //PixDisable object
    PixLib::PixDisable* m_disable;
    bool firstDisable;

    // Timing variables
    struct timeval m_t0;
    struct timeval m_t1;
    int m_time;
    bool m_pause;
    bool m_runStarted;
    bool m_preAmpOff;

    bool m_inPixRCDModuleUnconfigure;
    bool m_passedStartTransition;
    std::atomic<bool> m_alreadyRanCleanup;
    
    std::string msg;
  };

  inline const std::vector<DataChannel*>* PixRCDModule::channels() {
    return &m_dataChannels;
  }
  
}

#endif // _PIXRCD_PIXRCDMODULE



