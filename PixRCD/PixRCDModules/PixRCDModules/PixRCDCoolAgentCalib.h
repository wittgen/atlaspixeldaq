/////////////////////////////////////////////////////////////////////
// PixRCDCoolAgentCalib.h
// version 1.0
// Tool to check if corresponding calibration data is available
// for a given set of configuration tags.
// Associates the foreign key of calibration in cool part of
// CoraCool calibration database.
/////////////////////////////////////////////////////////////////////

#ifndef _PIXRCD_PIXRCDCOOLAGENTCALIB
#define _PIXRCD_PIXRCDCOOLAGENTCALIB


#include "ROSCore/ReadoutModule.h"
#include "DFSubSystemItem/ConfigException.h"

#include "ers/ers.h"

#include "ipc/core.h"
#include "ipc/partition.h"

#include "pmg/pmg_initSync.h"

#include "dal/Partition.h"
#include "dal/util.h"
#include "dal/IS_InformationSources.h"
#include "dal/IS_EventsAndRates.h"
#include "is/info.h"
#include "is/infodynany.h"
#include "is/infodictionary.h"
#include "is/infodocument.h"
#include "is/inforeceiver.h"
#include "is/type.h"
#include "rc/RunParams.h"
#include <TRP/LuminosityInfo.h>

#include "RunControl/Common/Exceptions.h"

// COOL include (from is2cool of GLM)
#include <CoralBase/Attribute.h>
#include <CoolKernel/Exception.h>
#include <CoolKernel/ValidityKey.h>
#include <CoolKernel/IDatabaseSvc.h>
#include <CoolKernel/IDatabase.h>
#include <CoolKernel/IFolder.h>
#include <CoolKernel/IRecordSpecification.h>
#include <CoolKernel/Record.h>
#include <CoolApplication/DatabaseSvcFactory.h>

// Forward declarations
class PixCalibRefCoolDb;
class PixCalibKnowledgeDb;

class IPCPartition;

namespace SctPixelRod {
   class VmeInterface;
}

namespace PixLib {
  class RodCrateConnectivity;
  class PixISManager;
  class PixConnectivity;
  class PixDbServerInterface;
  class PixDisable;
}

namespace daq {
  namespace rc{
    class CoolDB_Pixel;
  }
}

namespace ROS {

  class PixRCDCoolAgentCalib : public ReadoutModule {

    friend class daq::rc::CoolDB_Pixel;
    friend class PCDio;

  public:

    PixRCDCoolAgentCalib();
    virtual ~PixRCDCoolAgentCalib() noexcept (true);

    //state transitions
    virtual void setup(DFCountedPointer<ROS::Config> configuration);
    virtual void configure(const daq::rc::TransitionCmd&);
    virtual void connect(const daq::rc::TransitionCmd&);
    virtual void prepareForRun(const daq::rc::TransitionCmd&);
    virtual void startTrigger(const daq::rc::TransitionCmd&);
    virtual void stopTrigger(const daq::rc::TransitionCmd&);
    virtual void pause(void);
    virtual void stopEB(const daq::rc::TransitionCmd&);
    virtual void disconnect(const daq::rc::TransitionCmd&);
    virtual void unconfigure(const daq::rc::TransitionCmd&);
    virtual void clearInfo();
    virtual DFCountedPointer<ROS::Config> getInfo();
    virtual void probe(void);
    virtual void publish() { this->probe(); } // Calling probe from publish, which is the replacement to probe in daq::rc::Controllable
    virtual void publishFullStatistics(void);
    virtual void cleanup();
    virtual const std::vector<DataChannel*>* channels();

  private:
    //Private methods to handle parameters and read from IS
    void updateParams(std::string& paramValue, std::string paramName, bool publish=true);
    void updateParams(unsigned int& paramValue, std::string paramName);
    void readFromSchema();
    void getTags_CrateList(bool mapIsGood);
    //    void putPixDisable();

    // RCD CONFIGURATION: ROD module DataChannels (unused)
    std::vector<DataChannel *> m_dataChannels;

    // ROD module configuration
    DFCountedPointer<ROS::Config> m_configuration;

    // DAL configuration
    Configuration* m_conf;

    // ROD module parameters:
    unsigned long long m_RunNo;
    std::string m_runType;
    unsigned int m_runTime; 
    std::string m_crateName;
    std::string m_partName;
    std::string m_connTag;
    std::string m_cfgTag; 
    std::string m_cfgModTag;
    std::string m_idTag;  
    unsigned int m_cfgRev;
    unsigned int m_cfgModRev;
    std::string m_disName;
    std::string m_newDisName;
    unsigned int m_disRev;
    std::string m_runName;
    unsigned int m_runRev;

    // Map for configurationDB objects parameters conversion
    std::map<std::string,std::string> m_convMap;

    // pointers to database classes 
    PixCalibKnowledgeDb* m_knowledgeDB;
    PixCalibRefCoolDb* m_coolDB;

    //latest calibrationTags valid at the time of data-taking
    std::string m_calibTags;

    // SOFTWARE INFRASTRUCTURE  
    //ISManager
    std::string m_isName;
    PixLib::PixISManager * m_is;
    std::string m_isNameDD;
    PixLib::PixISManager * m_isDD;

    // IPC partitions
    IPCPartition *m_ipcPartition;
    IPCPartition *m_ipcPartitionDD;

    // PixConnectivity
    PixLib::PixConnectivity* m_conn;
    PixLib::PixDbServerInterface* m_dbServer;

    //keep track if coracool (offline) db is available for writing run-interval
    bool m_cc_ready;

    // Timing variables
    struct timeval m_t0;
    struct timeval m_t1;
    int m_time;
    bool m_pause;

  };
  
  inline const std::vector<DataChannel*>* PixRCDCoolAgentCalib::channels() {
    return &m_dataChannels;
  }

}

#endif // _PIXRCD_PIXRCDCOOLAGENTCALIB





