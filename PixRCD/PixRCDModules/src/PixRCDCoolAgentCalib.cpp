/////////////////////////////////////////////////////////////////////
// PixRCDCoolAgentCalib.cpp
// version 1.0
/////////////////////////////////////////////////////////////////////

// ONLINE SW includes
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <stdlib.h>
#include "ipc/partition.h"
#include "dal/util.h"
#include <sys/types.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <signal.h>
#include <dirent.h>
#include <errno.h>

// PIXRCD includes
#include "PixRCDModules/PixRCDCoolAgentCalib.h"
#include "PixRCDConfiguration/PixRCDConfigHandler.h"

// PIXLIB includes
#include "PixDbInterface/PixDbInterface.h"
#include "PixConnectivity/PixConnectivity.h"
#include "PixConnectivity/PartitionConnectivity.h"
#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixConnectivity/RodBocConnectivity.h"
#include "PixConnectivity/TimConnectivity.h"
#include "PixConnectivity/SbcConnectivity.h"
#include "PixConnectivity/OBConnectivity.h"
#include "PixConnectivity/Pp0Connectivity.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "PixBroker/PixBrokerMultiCrate.h"
#include "PixActions/PixActionsMulti.h"
#include "PixUtilities/PixISManager.h"
#include "PixUtilities/PixMessages.h"
#include "PixDbServer/PixDbServerInterface.h"
#include "PixConnectivity/PixDisable.h"
#include "rc/RunParams.h"

// COOL includes
#include <CoolKernel/IObjectIterator.h>
#include "CoolKernel/IObject.h"

// CORAL stuff
#include "CoralKernel/Context.h"
#include "CoralBase/Attribute.h"
#include "CoralBase/AttributeList.h"
#include "CoralBase/AttributeListException.h"
#include "RelationalAccess/IConnectionService.h"
#include "RelationalAccess/IConnectionServiceConfiguration.h"
#include "RelationalAccess/AccessMode.h"
#include "RelationalAccess/ISessionProxy.h"
#include "RelationalAccess/ISchema.h"
#include "RelationalAccess/ITransaction.h"
#include "RelationalAccess/AccessMode.h"
    
#include "RelationalAccess/ITable.h"
#include "RelationalAccess/ITableDescription.h"
#include "RelationalAccess/IColumn.h"
#include "RelationalAccess/ICursor.h"
#include "RelationalAccess/IQuery.h"
#include "RelationalAccess/TableDescription.h"
#include "RelationalAccess/ITableDataEditor.h"
#include "RelationalAccess/ITableSchemaEditor.h"
#include "RelationalAccess/IBulkOperation.h"
#include "RelationalAccess/SchemaException.h"

// COOL includes
#include <CoolKernel/IObjectIterator.h>
#include "CoolKernel/IObject.h"

using namespace ROS;
using namespace std;

//***********************************************************************
// Custom ERS exceptions

namespace daq {
ERS_DECLARE_ISSUE(      rc,
                        BadCoolDB_Pixel,
                        "Archiving to COOL failed because " << explanation,
                        ((const char*) explanation)
                )

ERS_DECLARE_ISSUE(      rc,
                        BackupArchive_Pixel,
                        "Archiving to COOL is falling back to SQLite " << name,
                        ((const char*) name)
                )
}

//class to access local CORAL database-file
class PixCalibKnowledgeDb
{
 public:
  PixCalibKnowledgeDb(std::string connString, int verbose);
  ~PixCalibKnowledgeDb();

  bool init(coral::AccessMode access_mode);
  
  void findLatestCalibTags(string& calibTags, string& calib_idTag, string& calib_connTag, string& calib_cfgTag, string& calib_cfgModTag );
  //void findCalibDataRef(string& calibTag, long long& calibIOVBegin, long long& calibIOVEnd, const std::string idTag,const std::string connTag,const std::string cfgTag,const std::string cfgModTag,const unsigned int cfgrev=0,const unsigned int cfgmodrev=0);
 
 private:
  std::string m_connString;
  int m_verbose;
  coral::ISessionProxy *m_session;
};

void PixCalibKnowledgeDb::findLatestCalibTags(string& calibTags, string& calib_idTag, string& calib_connTag, string& calib_cfgTag, string& calib_cfgModTag ){

  if (m_session->transaction().isActive()) m_session->transaction().rollback();
  m_session->transaction().start(true /*ReadOnly*/);
  
  //get key
  coral::IQuery* query = m_session->nominalSchema().tableHandle("CALIB_KEYGEN").newQuery();
  query->addToOutputList("KEY");
  coral::ICursor& cursor = query->execute();

  long long fkey = -1;
  // go through the list of found tables
  while ( cursor.next() ) {
    const coral::AttributeList &row0 = cursor.currentRow();
    fkey = row0[0].data<long long>();
  }
  //std::cout << "Latest KEY " << fkey << std::endl;

  delete query;

  //get latest calibrationTags
  coral::IQuery* query2 = m_session->nominalSchema().tableHandle("CALIB_TAGS").newQuery();
  
  query2->addToOutputList("UNIXTimeInSeconds");
  query2->defineOutputType("UNIXTimeInSeconds",coral::AttributeSpecification::typeNameForType<long int>());
  query2->addToOutputList("RunNumber");
  query2->defineOutputType("RunNumber",coral::AttributeSpecification::typeNameForType<long int>());
  query2->addToOutputList("calibrationTags");
  query2->defineOutputType("calibrationTags",coral::AttributeSpecification::typeNameForType<string>());
  query2->addToOutputList("idTag");
  query2->defineOutputType("idTag",coral::AttributeSpecification::typeNameForType<string>());
  query2->addToOutputList("connTag");
  query2->defineOutputType("connTag",coral::AttributeSpecification::typeNameForType<string>());
  query2->addToOutputList("cfgTag");
  query2->defineOutputType("cfgTag",coral::AttributeSpecification::typeNameForType<string>());
  query2->addToOutputList("cfgModTag");
  query2->defineOutputType("cfgModTag",coral::AttributeSpecification::typeNameForType<string>());
  query2->addToOutputList("FK");
  query2->defineOutputType("FK",coral::AttributeSpecification::typeNameForType<long long>());
  
  coral::AttributeList pixel_knowledgeData;
  string pixel_knowledge = "CALIB_TAGS.FK = :FK";
  pixel_knowledgeData.extend<long long>("FK");
  pixel_knowledgeData[0].data<long long>() = fkey;


  //set condition to match fkey, this always refers to the -latest- entry in the knowledge Db!
  query2->setCondition( pixel_knowledge, pixel_knowledgeData);
  
  //std::cout << "execute query!" << std::endl;
  coral::ICursor& cursor2 = query2->execute();
  // go through the list of found tables
  while ( cursor2.next() ) {
    const coral::AttributeList &row0 = cursor2.currentRow();
    calib_idTag = row0[3].data<string>();
    calib_connTag = row0[4].data<string>();
    calib_cfgTag = row0[5].data<string>();
    calib_cfgModTag = row0[6].data<string>();
    calibTags = row0[2].data<string>();
  }

  delete query2;
  //retrieve calibTags
}

// void PixCalibKnowledgeDb::findCalibDataRef(string& calibTag, long long& calibIOVBegin, long long& calibIOVEnd, const string idtag,const string conntag,const string cfgtag,const string cfgmodtag,const unsigned int cfgrev,const unsigned int cfgmodrev){

//   std::cout << "In findCalibDataRef" << std::endl;

//   if (m_session->transaction().isActive()) m_session->transaction().rollback();
//   m_session->transaction().start(true /*ReadOnly*/);
//   //build a query
//   coral::IQuery* query = m_session->nominalSchema().tableHandle("CALIB_CFG").newQuery();
  
//   query->addToOutputList("idTag");
//   query->defineOutputType("idTag",coral::AttributeSpecification::typeNameForType<string>());
//   query->addToOutputList("connTag");
//   query->defineOutputType("connTag",coral::AttributeSpecification::typeNameForType<string>());
//   query->addToOutputList("cfgTag");
//   query->defineOutputType("cfgTag",coral::AttributeSpecification::typeNameForType<string>());
//   query->addToOutputList("cfgModTag");
//   query->defineOutputType("cfgModTag",coral::AttributeSpecification::typeNameForType<string>());
//   query->addToOutputList("cfgRev");
//   query->defineOutputType("cfgRev",coral::AttributeSpecification::typeNameForType<unsigned int>());
//   query->addToOutputList("cfgModRev");
//   query->defineOutputType("cfgModRev",coral::AttributeSpecification::typeNameForType<unsigned int>());
//   //query->addToOutputList("FK");

//   query->addToOutputList("calibTag");
//   query->defineOutputType("calibTag",coral::AttributeSpecification::typeNameForType<string>());
//   query->addToOutputList("IOVBegin");
//   query->defineOutputType("IOVBegin",coral::AttributeSpecification::typeNameForType<long long>());
//   query->addToOutputList("IOVEnd");
//   query->defineOutputType("IOVEnd",coral::AttributeSpecification::typeNameForType<long long>());

//   coral::AttributeList pixel_knowledgeData;
//   string pixel_knowledge = "CALIB_CFG.idTag = :idtag";
//   pixel_knowledgeData.extend<string>("idtag");
//   pixel_knowledgeData[0].data<string>() = idtag;
//   pixel_knowledge += " AND CALIB_CFG.connTag = :conntag";
//   pixel_knowledgeData.extend<string>("conntag");
//   pixel_knowledgeData["conntag"].data<string>() = conntag;
//   pixel_knowledge += " AND CALIB_CFG.cfgTag = :cfgtag";
//   pixel_knowledgeData.extend<string>("cfgtag");
//   pixel_knowledgeData["cfgtag"].data<string>() = cfgtag;
//   pixel_knowledge += " AND CALIB_CFG.cfgModTag = :cfgmodtag";
//   pixel_knowledgeData.extend<string>("cfgmodtag");
//   pixel_knowledgeData["cfgmodtag"].data<string>() = cfgmodtag;
//   query->setCondition( pixel_knowledge, pixel_knowledgeData);
    
//   std::cout << "setConditions has " << pixel_knowledge << " " << pixel_knowledgeData << std::endl;

//   std::cout << "Execute query for findCalibDataRef" << std::endl;
//   coral::ICursor& cursor = query->execute();
//   std::cout << "PixCalibKnowledgeDb::FindCalibDataRef: succesfully executed query!" << std::endl;

//   long long foreignkey = -1;
//   // go through the list of found tables
//   while ( cursor.next() ) {
//     std::cout << "cursor.next() " << std::endl;
//     const coral::AttributeList &row0 = cursor.currentRow();
//     string idTag = row0[0].data<string>();
//     string connTag = row0[1].data<string>();
//     string cfgTag = row0[2].data<string>();
//     unsigned int cfgRev = row0[4].data<unsigned int>();
//     string cfgModTag = row0[3].data<string>();
//     unsigned int cfgModRev = row0[5].data<unsigned int>();
//     //    long long fkey = row0[6].data<long long>();
//     calibTag = row0[6].data<string>();
//     calibIOVBegin = row0[7].data<long long>();
//     calibIOVEnd = row0[8].data<long long>();
    
//     std::cout << "calibTag " << calibTag << std::endl;
//     std::cout << "calibIOVBegin " << calibIOVBegin << std::endl;
//     std::cout << "calibIOVEnd " << calibIOVEnd << std::endl;

//     //take latest foreignkey for which the cfg-revision used by on-line is equal or higher than revision stored in knowledge DB!
//     // not sure if this is approriate, need to find out if revisions will be stored in knowledgeDB or not...
//     //     std::cout << "fkey " << fkey << " foreignkey " << foreignkey << std::endl;
//     std::cout << "cfgRev " << cfgRev << std::endl;
//     std::cout << "cfgModRev " << cfgModRev << std::endl;
//  //    if( fkey > foreignkey ){
// //       if( cfgmodrev > cfgModRev || cfgmodrev == 0 ) {
// // 	if( cfgrev > cfgRev || cfgrev == 0 ) foreignkey = fkey;
// //       }
// //     }
//   }
//   delete query;

//   //if (m_session->transaction().isActive()) m_session->transaction().commit();
// }

//---------------------------------------------------------------------------
/** Constructor.
    Open the default database and seal context.
 */
PixCalibKnowledgeDb::PixCalibKnowledgeDb(string connString, int verbose) :
  m_connString(connString),m_verbose(verbose), m_session(0) {}

PixCalibKnowledgeDb::~PixCalibKnowledgeDb()
{
  if (m_verbose) cout << "Disconnecting \"" << m_connString << "\"" << endl;
  delete m_session;
}

/** initialize connection
 */
bool PixCalibKnowledgeDb::init(coral::AccessMode access_mode)
{
  if (m_verbose) cout << "Creating Connection Service to \"" << m_connString << "\"" << endl;
  coral::Context* context = &coral::Context::instance();
  coral::IHandle<coral::IConnectionService> lookSvcH = context->query<coral::IConnectionService>();
  if (!lookSvcH.isValid()) {
    context->loadComponent("CORAL/Services/ConnectionService");
    lookSvcH = context->query<coral::IConnectionService>();
  }
  if (!lookSvcH.isValid()) {
    throw runtime_error("Could not locate the connection service");
    return false;
  }

  m_session = lookSvcH->connect(m_connString, access_mode);
  if( m_session ) return true;
  else return false;
}

// Declare PixCalibRefCoolDb to store reference to calibration data corresponding to specific run-conditions and current UNIXTime
// note: run-conditions are already stored so this a duplication, nevermind this for now...

class PixCalibRefCoolDb {
public:
  PixCalibRefCoolDb (std::string dbIdentifier);
  ~PixCalibRefCoolDb();
  void close();
  cool::IDatabasePtr getValidDb();
private:
 
  cool::DatabaseId m_dbId;
  cool::IDatabasePtr m_db;
};

PixCalibRefCoolDb::PixCalibRefCoolDb(std::string dbIdentifier): m_dbId(dbIdentifier) {

  // Foldernames
  // Foldernames
  std::string folderName = ("/PIXEL/Onl/PixCalibRef");
  // Create Folder Specifications
  cool::RecordSpecification recordSpec;
  recordSpec.extend("UNIXTimeInSeconds",cool::StorageType::Int64);
  recordSpec.extend("idTag",cool::StorageType::String255);
  recordSpec.extend("connTag",cool::StorageType::String255);
  recordSpec.extend("cfgTag",cool::StorageType::String255);
  recordSpec.extend("cfgModTag",cool::StorageType::String255);
  recordSpec.extend("calibTags",cool::StorageType::String255);

  // Open or Create the main DB
  try {
    m_db = cool::DatabaseSvcFactory::databaseService().openDatabase(m_dbId, false); // false = !read_only    
  }
  catch (cool::DatabaseDoesNotExist &e) {
    try {
      m_db = cool::DatabaseSvcFactory::databaseService().createDatabase(m_dbId);
    }
    catch(std::exception &ex) {
      daq::rc::BadCoolDB_Pixel i(ERS_HERE, ex.what());
      ers::error(i);
    }
  }
  catch (std::exception &ex) {
      daq::rc::BadCoolDB_Pixel i(ERS_HERE, ex.what());
      ers::error(i);    
  }
 

  // Create any missing folders on the DB (shouldn't be necessary)
  if (m_db) {
     if (!m_db->existsFolder(folderName)) {
       std::string Desc("<timeStamp>run-lumi</timeStamp><addrHeader><address_header service_type=\"71\" clid=\"40774348\" /></addrHeader><typeName>AthenaAttributeList</typeName>");
#ifdef COOL_MIGRATION_FIXED
       // This code needs migration to COOL 3.x as we cannot use the legacy functions anymore
       cool::IFolderPtr SOR_Folder = m_db->createFolder(folderName, recordSpec, Desc, cool::FolderVersioning::SINGLE_VERSION, true);
       ERS_DEBUG(3, "DB Folder PixelCalibRef created.");
#else
       std::cout << "Skipping folder creation because migration to COOL 3.x not yet completed." << std::endl; // Fix me
#endif
     }
   }
 
}

PixCalibRefCoolDb::~PixCalibRefCoolDb() {
  this->close();
}

cool::IDatabasePtr PixCalibRefCoolDb::getValidDb() {
  try {
    m_db = cool::DatabaseSvcFactory::databaseService().openDatabase(m_dbId, false);
    return m_db;
  }
  catch(coral::Exception &e) {
    daq::rc::BadCoolDB_Pixel i(ERS_HERE, "Pixel calibration reference COOL DB can not be opened.");
    throw i;
  }
}

void PixCalibRefCoolDb::close() {
  if (m_db) m_db->closeDatabase();
}

//start of PixRCDCoolAgentCalib class declarations

PixRCDCoolAgentCalib* pixrcd = 0;

void exitHandler(int signum) {
  std::cout << "Executing PixRCDCoolAgentCalib exit handler" << std::endl;
  if (pixrcd != 0) pixrcd->cleanup();
}

PixRCDCoolAgentCalib::PixRCDCoolAgentCalib() {
  
  std::cout << "In PixRCDCoolAgentCalib constructor!" << std::endl;
  m_conn = NULL;
}

PixRCDCoolAgentCalib::~PixRCDCoolAgentCalib() noexcept (true) {
  if( m_conn ) delete m_conn;
  pixrcd = 0;
}

void PixRCDCoolAgentCalib::cleanup() {
  ers::info(PixLib::pix::daq (ERS_HERE,m_partName,"Executing PixRCDCoolAgentCalib::cleanup()"));  
  if (m_is!=0) {
    delete m_is;
    m_is = 0;
  }
  if (m_isDD!=0) {
    delete m_isDD;
    m_isDD = 0;  }
  if (m_ipcPartition!=0) {
    delete m_ipcPartition;
    m_ipcPartition = 0;
  }
  if (m_ipcPartitionDD!=0) {
    delete m_ipcPartitionDD;
    m_ipcPartitionDD = 0;
  }
  if( m_knowledgeDB != 0){
    delete m_knowledgeDB;
    m_knowledgeDB = 0;
  }
  if( m_coolDB != 0){
    delete m_coolDB;
    m_coolDB = 0;
  }
}

void PixRCDCoolAgentCalib::setup(DFCountedPointer<ROS::Config> configuration) {
  m_time=gettimeofday(&m_t0,NULL);
  
  std::cout<<"Called PixRCDCoolAgentCalib::setup"<<std::endl;

  //pointers initialization  
  m_ipcPartition = 0;
  m_ipcPartitionDD = 0;
  m_is = 0;
  m_isDD = 0;
  m_knowledgeDB = 0;
  m_coolDB = 0;
  pixrcd = this;

  m_cc_ready = false;

  // CREATE IPC PARTITION
  std::string IPCPartitionName = "";
  IPCPartitionName = getenv("TDAQ_PARTITION_INFRASTRUCTURE");
  if (IPCPartitionName != "") {
    std::cout << IPCPartitionName << std::endl;
  } else {   
    std::cout << "PixRCDCoolAgentCalib::setup: ERROR!! Could not read 'TDAQ_PARTITION_INFRASTRUCTURE' from XML files" << std::endl << std::endl;
  }

  std::cout << "Trying to connect to partition " << IPCPartitionName << " ... " << std::endl;
  try {
    m_ipcPartition = new IPCPartition(IPCPartitionName);
  } catch(...) {
    std::cout << "PixRCDCoolAgentCalib::setup: ERROR!! Problems while connecting to the partition"+ IPCPartitionName << std::endl;
    exit(-1);
  }
  std::cout << "Succesfully connected to partition " << IPCPartitionName << std::endl << std::endl;

  std::string IPCPartitionNameDD = "";
  IPCPartitionNameDD = getenv("TDAQ_PARTITION");
  if (IPCPartitionNameDD == "") {
    std::cout << "PixRCDCoolAgentCalib::setup: ERROR!! Could not read 'TDAQ_PARTITION' from XML files" << std::endl << std::endl;
    exit(-1);
  }
  std::cout << "Trying to connect to partition " << IPCPartitionNameDD << " ... " << std::endl;
  m_ipcPartitionDD=0;
  try {
    m_ipcPartitionDD = new IPCPartition(IPCPartitionNameDD);
  } catch(...) {
    std::cout << "PixRCDCoolAgentCalib::setup: ERROR!! Problems while connecting to the partition"+ IPCPartitionNameDD << std::endl;
    //    exit(-1);
  }
  std::cout << "Succesfully connected to partition " << IPCPartitionNameDD << std::endl << std::endl;

  // Initialize IS server manager
  bool IsReady=true;
  m_isName = "PixelRunParams";
  try {
    m_is = new PixLib::PixISManager(m_ipcPartition, m_isName);
  } catch(...) {
    IsReady=false;
    ers::error(PixLib::pix::daq (ERS_HERE, "NO_IS_SERVER","Could not connect to IsServer "+m_isName));
  }
  if (IsReady) std::cout << "Succesfully created PixIsManager " << m_isName << std::endl << std::endl;  

  m_isNameDD = "RunParams";
  try {
    m_isDD = new PixLib::PixISManager(m_ipcPartitionDD, m_isNameDD);
  } catch(...) {
    IsReady=false;
    ers::error(PixLib::pix::daq (ERS_HERE, "NO_IS_SERVER","Could not connect to IsServer "+m_isNameDD));
  }
  if (IsReady) std::cout << "Succesfully created PixIsManager " << m_isNameDD << std::endl << std::endl;  
  
  // GET CONFIGURATION from XML
  m_configuration = configuration; //Pointer to the ReadoutModule configuration
  bool mapIsGood = false;
  PixRCDConfigHandler* configHandler = new PixRCDConfigHandler(m_configuration);
  mapIsGood = configHandler->extractConversionMap(m_convMap);
  if(!mapIsGood) ers::warning(PixLib::pix::daq (ERS_HERE, "NO_XML_INFO","Could not information from XML"));
  // Here the substitution map is extracted from Configuration, in order to control substituttion of variables with their content. 

  getTags_CrateList(mapIsGood);
  //IS access for tags update
  //bool tagsFromIs = m_configuration->getBool("TagsFromIS");
  delete configHandler;
  
  std::cout << std::endl << "Tags before reading from IS:" << std::endl;
  std::cout << "- Connectivity Tag : " << m_connTag << std::endl;
  std::cout << "- Id tag: " << m_idTag << std::endl;
  std::cout << "- Configuration Tag: " << m_cfgTag << std::endl;
  std::cout << "- Module Configuration Tag: " << m_cfgModTag << std::endl << std::endl; 

  updateParams(m_connTag  , "DataTakingConfig-ConnTag");
  updateParams(m_cfgTag   , "DataTakingConfig-CfgTag");
  updateParams(m_cfgModTag, "DataTakingConfig-ModCfgTag");
  updateParams(m_idTag    , "DataTakingConfig-IdTag");
  updateParams(m_cfgRev   , "DataTakingConfig-CfgRev");
  updateParams(m_cfgModRev, "DataTakingConfig-CfgModRev");

  //check if calibration excist for this set of configuration tags
  //string connString = "sqlite_file:/home/mlimper/my_db/pcd_knowledge.db";
  string connString = "sqlite_file:/detpriv/pix/data/PixCalibDataOffline/pcd_knowledge.db";
//   char* charConnString = getenv("PIX_KNOWLEDGE_DB");
//   if( charConnString == 0 ){
//     //throw exception
//     m_myMessages->publishMessage(PixLib::PixMessages::INFORMATION, "CALIB_KNOWLEDGE_DB", "Could not read env-variable 'PIX_KNOWLEDGE_DB' from XML files");
//     m_myMessages->publishMessage(PixLib::PixMessages::INFORMATION, "CALIB_KNOWLEDGE_DB", "Use default: ", connString );
//   }
//   else connString = charConnString;
  //std::cout << "connString " << connString << std::endl;

  m_knowledgeDB = new PixCalibKnowledgeDb(connString, true);
  if( !(m_knowledgeDB->init(coral::ReadOnly)) ){
    ers::fatal(PixLib::pix::daq (ERS_HERE, "CALIB_DB_ERROR","Failed to connect to CALIB_DB at "+ connString));
    exit(-1);
  }
  else {
    std::cout << "Succesfully connected to " << connString << std::endl;
  }

  m_calibTags = "";
 
  std::string calib_idTag = "";
  std::string calib_connTag = "";
  std::string calib_cfgTag = "";
  std::string calib_cfgModTag = "";
  //access knowledgeDB in local sqlite-file
  m_knowledgeDB->findLatestCalibTags(m_calibTags,calib_idTag,calib_connTag,calib_cfgTag,calib_cfgModTag);
  if( m_calibTags != "" ) {
    std::cout << "Latest calibTags: \n " << m_calibTags << std::endl;
  }
  else{
    ers::fatal(PixLib::pix::daq (ERS_HERE, "NO_CALIB_DATA","Did not manage to find latest calibration Tags from pixel knowledge DB "));
    exit(-1);
  }

  //check that run-conditions in IS match those corresponding to latest calibTags
  if( calib_idTag != m_idTag || calib_connTag != m_connTag || calib_cfgTag != m_cfgTag || calib_cfgModTag != m_cfgModTag){
    ers::fatal(PixLib::pix::daq (ERS_HERE, "IS_TAG_PROBLEM","Configuration from IS does not match config from latest calibration"));
    ers::fatal(PixLib::pix::daq (ERS_HERE, "IS_TAG_PROBLEM","   IS tags: "+m_idTag+" "+m_connTag+" "+m_cfgTag+" "+m_cfgModTag));
    ers::fatal(PixLib::pix::daq (ERS_HERE, "IS_TAG_PROBLEM","Calib tags: "+calib_idTag+" "+calib_connTag+" "+calib_cfgTag+" "+calib_cfgModTag ));
    ers::info(PixLib::pix::daq (ERS_HERE, "IS_TAGS_PROBLEM","Fix tags in IS or disable PixRCDCoolAgentCalib to continue" ));
    exit(-1);
  }

 
  //initialize the cool-database if not already done
  if( m_coolDB == 0){
    std::string dbIdentifier = "PCOOLRP/CONDBR2" ;
    //associate the foreign-key with the current run-conditions
    m_coolDB = new PixCalibRefCoolDb(dbIdentifier);
    if( !(m_coolDB ) ){
      ers::warning(PixLib::pix::daq (ERS_HERE, "CALIB_DB_ERROR", "Failed to connect to "+ dbIdentifier));
      // exit(-1);
    }
    else{
        std::cout << "Close the DB; reopen everytime there is an entry to be inserted..." << std::endl;
	// Close the DBs; reopen everytime there is an entry to be inserted...
	m_coolDB->close();
    }
  }

  // Install the exit handler
  struct sigaction sa;
  sigemptyset(&sa.sa_mask); 
  sa.sa_flags = 0;
  sa.sa_handler = exitHandler; 
  int ret1=0, ret2=0;
  ret1 = sigaction(SIGTERM, &sa, NULL);
  ret2 = sigaction(SIGINT, &sa, NULL);
  if (ret1 != 0 || ret2 != 0) {
    ers::fatal(PixLib::pix::daq (ERS_HERE, "NO_EXTHDL",m_partName+": cannot install the exit handler")); 
    exit(-1);
  }
 
  m_time=gettimeofday(&m_t1,NULL);
  std::ostringstream text;
  text << m_partName << ": time to setup is:  " << m_t1.tv_sec - m_t0.tv_sec+ 1e-6*(m_t1.tv_usec-m_t0.tv_usec);
  ers::debug(PixLib::pix::daq (ERS_HERE,"TIME_INFO",text.str())); 
}

void PixRCDCoolAgentCalib::configure(const daq::rc::TransitionCmd&) {
}

void PixRCDCoolAgentCalib::connect(const daq::rc::TransitionCmd&) {
}

void PixRCDCoolAgentCalib::prepareForRun(const daq::rc::TransitionCmd&) {

  //std::cout << "called PixRCDCOOLAgentCalib::prepareForRun " << std::endl;

  // Read info from IS
  std::string value;
  RunParams sor;
  try {
    if (m_isDD != 0) {
      sor = m_isDD->read<RunParams>("RunParams");
      m_RunNo = sor.run_number;
      std::ostringstream text;
      text << "Run Number (" << m_RunNo << ") read from RunParams.RunParams";
      ers::info(PixLib::pix::daq (ERS_HERE,"RUN_NUMBER",text.str())); 
    } else {
      ers::error(PixLib::pix::daq (ERS_HERE,"RUN_NUMBER_ERROR","Impossible to read run number from PixelRunParams")); 
    }
  } catch(...) {
   ers::error(PixLib::pix::daq (ERS_HERE, "FAIL_IN_IS", "Problem in accessing IsServer RunParams to read RunParams"));
  }

  updateParams(m_connTag  , "DataTakingConfig-ConnTag");
  updateParams(m_cfgTag   , "DataTakingConfig-CfgTag");
  updateParams(m_cfgModTag, "DataTakingConfig-ModCfgTag");
  updateParams(m_idTag    , "DataTakingConfig-IdTag");

  // This is not needed, right?
  //   std::string tagName =  "PixCalibData-UPD4-000-00";
  //   char* charTag = getenv("CALIB_TAG");
  //   if( charTag == 0 ){
  //     //throw exception
  //     m_myMessages->publishMessage(PixLib::PixMessages::WARNING, "NO_CALIB_TAG", "Could not read env-variable 'CALIB_TAG' from XML files");
  //     m_myMessages->publishMessage(PixLib::PixMessages::WARNING, "NO_CALIB_TAG", "Using default: ", tagName);
  //   }
  //   tagName = charTag;

  //write info into text-file
  //std::cout << "output in a temporary file the currently valid calibration-tags to be stored in the Cool DB" << std::endl;
  std::string tmpFilePath = "";
  tmpFilePath = getenv("CORAL_DBLOOKUP_PATH");
  
  std::ostringstream bufFileName;
  bufFileName << tmpFilePath.c_str() << "/.PixRCDCoolAgent/CalibRefEntry_" << m_RunNo << ".txt";
  //std::cout << " bufFileName " << bufFileName.str().c_str() << std::endl;
  std::ofstream *bf = new std::ofstream(bufFileName.str().c_str());
  //m_myMessages->publishMessage(PixLib::PixMessages::INFORMATION, "TMP_FILE_INFO", "writing temporary run data in file ", bufFileName.str().c_str() );

  //also store current time in seconds
  struct timeval UNIXTime;
  gettimeofday(&UNIXTime,NULL);

  *bf << 
    m_RunNo << std::endl <<
    UNIXTime.tv_sec << std::endl <<
    m_idTag << std::endl <<
    m_connTag << std::endl <<
    m_cfgTag << std::endl <<
    m_cfgModTag << std::endl <<
    m_calibTags << std::endl ;
  delete bf;
}

DFCountedPointer<ROS::Config> PixRCDCoolAgentCalib::getInfo() {
  // Get config object
  DFCountedPointer<ROS::Config> configuration = ROS::Config::New();
  return(configuration);
}

void PixRCDCoolAgentCalib::probe() {
  
  std::cout << "In probe" << std::endl;
  std::string tmpFilePath = "";
  
  try {
    tmpFilePath = getenv("CORAL_DBLOOKUP_PATH");
    //std::cout << "Look for temporary runData-files in " << tmpFilePath << std::endl;
  }
  catch (...){
    ers::warning(PixLib::pix::daq (ERS_HERE, "BAD_DIR", "exception in getenv " + tmpFilePath));
    return;
  }
  
  std::ostringstream dirName;
  dirName << tmpFilePath.c_str() << "/.PixRCDCoolAgent";

  DIR *dp;
  struct dirent *dirp;
  vector<std::string> files = vector<std::string>();
  
  if((dp  = opendir(dirName.str().c_str())) == NULL) {
    std::ostringstream error;
    error << "Unable to access directory " << dirName.str().c_str() << " no run conditions archived in COOL";
    ers::warning(PixLib::pix::daq (ERS_HERE, "BAD_DIR", error.str()));
    return;
  }
  while ((dirp = readdir(dp)) != NULL) {
    std::string tmpString;
    tmpString = dirp->d_name;
    files.push_back(tmpString);
    //    std::cout << "read file" << tmpString << "from /daq/db/.PixRCDCoolAgent" << std::endl;
  }
  closedir(dp);
  
  //Open DB
  cool::IDatabasePtr db;
  try{
    db= m_coolDB->getValidDb();
    std::cout << "DB opened!" << std::endl;
    ERS_DEBUG(3, "DB opened");
  }
  catch (daq::rc::BadCoolDB_Pixel &e){
    ers::error(e);
    return;
  }

  //find unprocessed text-files and store info of run-interval into coracool DB
  // get the first entry with no "PP" (for processed) in front  
  int i = 0; 
  bool find = false;
  for(; i < (int)files.size(); i++){
    if(files[i].c_str()[0] != 'P' && files[i].c_str()[0] == 'C' && files[i].c_str()[0] != '.') {find = true; break;} 
  }
  if(find == true){
    ers::info(PixLib::pix::daq (ERS_HERE, "TMP_FILE_INFO", "Saving runData in "+files[i]+ " into COOL"));
    std::ostringstream bufFileName;
    bufFileName << dirName.str() << "/" << files[i].c_str();
    std::ifstream *bf = new std::ifstream(bufFileName.str().c_str());
    std::cout << "open to process file " << bufFileName.str().c_str() << std::endl;
    
    unsigned long runNo;
    long int UNIXTimeInSeconds;
    std::string idTag;
    std::string connTag;
    std::string cfgTag;
    std::string cfgModTag;
    std::string calibTags;
    
    *bf >> 
      runNo >>
      UNIXTimeInSeconds >>
      idTag >>
      connTag >>
      cfgTag >>
      cfgModTag >>
      calibTags ;
    delete bf;
    
    // Index by run number and lumi block (lb=0)
    cool::UInt63 a = runNo;
    cool::ValidityKey since = a << 32;
    cool::ValidityKey until = cool::ValidityKeyMax;
    
    //define RecordSpecification
    cool::RecordSpecification recordSpec;
    recordSpec.extend("UNIXTimeInSeconds",cool::StorageType::Int64);
    recordSpec.extend("idTag",cool::StorageType::String255);
    recordSpec.extend("connTag",cool::StorageType::String255);
    recordSpec.extend("cfgTag",cool::StorageType::String255);
    recordSpec.extend("cfgModTag",cool::StorageType::String255);
    recordSpec.extend("calibTags",cool::StorageType::String255);
    
    // Fill in the data
    cool::Record coolRecord = cool::Record(recordSpec);
    coolRecord["UNIXTimeInSeconds"].setValue<cool::Int64>( UNIXTimeInSeconds );
    coolRecord["idTag"].setValue<cool::String255>( idTag );
    coolRecord["connTag"].setValue<cool::String255>( connTag );
    coolRecord["cfgTag"].setValue<cool::String255>( cfgTag );
    coolRecord["cfgModTag"].setValue<cool::String255>( cfgModTag );
    coolRecord["calibTags"].setValue<cool::String255>( calibTags );
    
    
    ERS_DEBUG(3, "Payload with calib ref filled.");
    // Commit to DB
    
    try {
      cool::IFolderPtr coolFolder = db->getFolder("/PIXEL/Onl/PixCalibRef");
      ERS_DEBUG(3, "CoolFolder retrieved.");
      coolFolder->storeObject(since, until, coolRecord, 0);
      ERS_DEBUG(3, "Successfully stored data in CoolFolder stored.");
      
      // renaming the processed file in order to take out from the waiting list
      std::ostringstream bufFileReNameOld;
      std::ostringstream bufFileReNameNew;
      bufFileReNameOld << dirName.str() << "/" << files[i];
      bufFileReNameNew << dirName.str() << "/P" << files[i];
      rename(bufFileReNameOld.str().c_str(), bufFileReNameNew.str().c_str());
    }
    catch (std::exception &e){
      daq::rc::BadCoolDB_Pixel j(ERS_HERE, e.what());
      ers::warning(j);
    }
    
  }//end finding first unproccessed file
  
  std::cout << "close DB-connection" << std::endl;
  m_coolDB->close();

  return;
}

void PixRCDCoolAgentCalib::startTrigger(const daq::rc::TransitionCmd&) {
}

void PixRCDCoolAgentCalib::stopTrigger(const daq::rc::TransitionCmd&) {
}

void PixRCDCoolAgentCalib::pause() {
}

void PixRCDCoolAgentCalib::stopEB(const daq::rc::TransitionCmd&) {
}


void PixRCDCoolAgentCalib::disconnect(const daq::rc::TransitionCmd&) {
}

void PixRCDCoolAgentCalib::unconfigure(const daq::rc::TransitionCmd&) {
  cleanup();
}

void PixRCDCoolAgentCalib::publishFullStatistics() {
}

void PixRCDCoolAgentCalib::clearInfo() {
}

// CODE NEEDED BY THE PLUGIN FACTORY
extern "C"
{
  extern ReadoutModule* createPixRCDCoolAgentCalib();
}
ReadoutModule* createPixRCDCoolAgentCalib()
{
  return (new PixRCDCoolAgentCalib());
}

//copy method for retrieving configuration tags from PixRCDCoolAgent.cpp

void PixRCDCoolAgentCalib::updateParams(std::string& paramValue, std::string paramName, bool publish) {
  // Reads if paramName is published in IS. If yes, it overwrites the local paramValue;
  // if not, it publishes local paramValue.
  std::string value;
  try {
    if (m_is!=0) value = m_is->read<std::string>(paramName);
  } catch(...) {
    ers::warning(PixLib::pix::daq (ERS_HERE, "FAIL_IN_IS", "Problem in reading parameter <"+paramName+"> from IsServer"));
  }
  if (value == "")  {
    if (publish) {
      ers::info(PixLib::pix::daq (ERS_HERE, "NO_TAG_IN_IS","Parameter <"+paramName+"> was not found in IS. Updating IS with value: "+paramValue));
      if (m_is!=0) m_is->publish(paramName, paramValue);
    }
  } else {
    paramValue = value;
    ers::info(PixLib::pix::daq (ERS_HERE, "IS_UPDATE","Parameter <"+paramName+"> found in IS with value: "+paramValue));
  }
}

void PixRCDCoolAgentCalib::updateParams(unsigned int& paramValue, std::string paramName) {
  // Reads if paramName is published in IS.
  unsigned int value=0;
  try {
    if (m_is!=0) value = m_is->read<unsigned int>(paramName);
  } catch(...) {
    ers::warning(PixLib::pix::daq (ERS_HERE, "FAIL_IN_IS", "Problem in reading parameter <"+paramName+"> from IsServer"));
  }
  paramValue = value;
}

void PixRCDCoolAgentCalib::readFromSchema() {
  //std::cout<<"PixRCDCoolAgent::setup: taking default Connectivity tags directly from attr name definition in Crates xml."<<std::endl;
  m_connTag   = m_configuration->getString("ConnectivityTagC");
  m_cfgTag    = m_configuration->getString("ConnectivityTagCF");
  m_cfgModTag = m_configuration->getString("ConnectivityTagModCF");
  m_idTag = m_configuration->getString("ConnectivityTagID");
}

void PixRCDCoolAgentCalib::getTags_CrateList(bool mapIsGood) {
  bool error = false;  
  if(mapIsGood) {
    std::map<std::string,std::string>::iterator mapit;
    mapit = (m_convMap).find("CONNECTIVITY_RUN_TAG_C");
    if (mapit!=(m_convMap).end()) m_connTag = (*mapit).second;
    else {
      ers::warning(PixLib::pix::daq (ERS_HERE, "NO_PAR_IN_XML","Cannot find 'CONNECTIVITY_RUN_TAG_C' var into the conversion map."));
      error = true;
    }
    mapit = (m_convMap).find("CONNECTIVITY_RUN_TAG_CF");
    if (mapit!=(m_convMap).end()) m_cfgTag = (*mapit).second;
    else {
      ers::warning(PixLib::pix::daq (ERS_HERE, "NO_PAR_IN_XML","Cannot find 'CONNECTIVITY_RUN_TAG_CF' var into the conversion map."));
      error = true;
    }
    mapit = (m_convMap).find("CONNECTIVITY_RUN_TAG_MOD_CF");
    if (mapit!=(m_convMap).end()) m_cfgModTag = (*mapit).second;
    else {
      ers::warning(PixLib::pix::daq (ERS_HERE, "NO_PAR_IN_XML","Cannot find 'CONNECTIVITY_RUN_TAG_MOD_CF' var into the conversion map."));
      error = true;
    }
    mapit = (m_convMap).find("ID_TAG");
    if (mapit!=(m_convMap).end()) m_idTag = (*mapit).second;
    else {
      ers::warning(PixLib::pix::daq (ERS_HERE, "NO_PAR_IN_XML","Cannot find 'ID_TAG' var into the conversion map."));
      error = true;
    }
  }
  else
    readFromSchema();
  if (error)
    readFromSchema();
}




