/////////////////////////////////////////////////////////////////////
// PixRCDModule.cpp
// version 1.2
/////////////////////////////////////////////////////////////////////
//
// 31/01/06  Version 1.0 (CS)
//           Initial release
//
// 23/02/06  Version 1.1 (CS)
//           First multithreaded implementation
//
// 11/10/06  Version 1.2 (CS)
//           Implementation using PixBrokers and PixActions
//
// 5/7/07    Version 1.3 (GAO)
//           First BrokerMultiCrate usage
//           Uses PixRCDConfigHandler to manage general Config obj from xml

//! Pixel RCD Module class


// ONLINE SW includes
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <stdlib.h>
#include "ipc/partition.h"
#include "dal/util.h"
#include <sys/types.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <signal.h>

#include <TRP/LuminosityInfo.h>

// PIXRCD includes
#include "PixRCDModules/PixRCDModule.h"
#include "PixRCDConfiguration/PixRCDConfigHandler.h"

// VME INTERAFCE includes
#include "RCCVmeInterface.h"

// PIXLIB includes
#include "PixDbInterface/PixDbInterface.h"
#include "PixConnectivity/PixConnectivity.h"
#include "PixConnectivity/PartitionConnectivity.h"
#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixConnectivity/RodBocConnectivity.h"
#include "PixConnectivity/TimConnectivity.h"
#include "PixConnectivity/SbcConnectivity.h"
#include "PixConnectivity/OBConnectivity.h"
#include "PixConnectivity/Pp0Connectivity.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "PixBroker/PixBrokerMultiCrate.h"
#include "PixActions/PixActionsMulti.h"
#include "PixUtilities/PixISManager.h"
#include "PixUtilities/PixMessages.h"
#include "PixDbServer/PixDbServerInterface.h"
#include "PixConnectivity/PixDisable.h"
#include "rc/RunParams.h"
#include "ers/ers.h"
#include <ers/InputStream.h>
#include <ers/ers.h>

#include "RunControl/Common/UserExceptions.h"


#include "config/Configuration.h"
#include "dal/TriggerConfiguration.h"


using namespace ROS;
using namespace std;

PixRCDModule* pixrcd = nullptr;

void exitHandler(int signum) {
  PIX_WARNING("Executing PixRCDModule exit handler with signal "<<signum );
  if (pixrcd != 0) pixrcd->cleanup();
  PIX_LOG("Execution of PixRCDModule exit handler complete");
}

PixRCDModule::PixRCDModule() {
    //pointers initialization  
  m_ipcPartition = nullptr;
  m_ipcPartitionDD = nullptr;
  m_is = nullptr;
  m_is2 = nullptr;
  m_isDD = nullptr;
  m_broker = nullptr;
  m_timActions = nullptr;
  m_rodActions = nullptr;
  m_dbServer = nullptr;
  m_disable =nullptr;
  pixrcd = this;
  m_pause = false;
  m_runStarted = false;
  m_passedStartTransition = false;
  m_preAmpOff = false;
  m_inPixRCDModuleUnconfigure = false;
  m_alreadyRanCleanup = true;
}

PixRCDModule::~PixRCDModule() {
  std::cout<<"Exiting PixRCD Application "<<std::endl;
  cleanup();
  std::cout<<"Aplication exit normally "<<std::endl;
}

void PixRCDModule::restartActions() {
  bool allowActionsRestart = false;
  if(m_is->exists("PIX_RestartActionsEnabled"))
    allowActionsRestart = m_is->read<bool>("PIX_RestartActionsEnabled");
  else std::cout << __PRETTY_FUNCTION__ << ": PIX_RestartActionsEnabled not published in PixelInfr PixelRunParams. Assuming false" << std::endl;

  if(!allowActionsRestart) {
    std::cout << __PRETTY_FUNCTION__ << ": prevented from restarting actions because PIX_RestartActionsEnabled is not true" << std::endl;
    return;
  }
/*
  bool allowActionsRestartInLocalDataTaking = false;
  if(m_is->exists("PIX_RestartActionsInLocalDataTakingEnabled"))
    allowActionsRestartInLocalDataTaking = m_is->read<bool>("PIX_RestartActionsInLocalDataTakingEnabled");

  if( !allowActionsRestartInLocalDataTaking && m_ipcPartitionDD->name() != "ATLAS" ) {
    std::cout << __PRETTY_FUNCTION__ << ": would have restarted the ActionsServers had we been running in ATLAS" << std::endl;
    return;
  }
*/
//#define ASYNC_ACTIONS_RESTART
#ifdef ASYNC_ACTIONS_RESTART
  std::string isName = "DDC";
  PixLib::PixISManager *isM(nullptr);
  try {
    isM = new PixLib::PixISManager(m_ipcPartition, isName);
  } catch(...) {
    msg= m_partName+": could not connect to IsServer "+isName;
    ers::error(PixLib::pix::daq(ERS_HERE, "NO_ISM", msg));
  }
  int restartActions = 1;
  std::cout << "KDebug: " << m_broker->name() << std::endl;
  isM->publish("PIX_RestartActions", restartActions);
#else
  std::map<std::string, PixLib::RodCrateConnectivity*>::iterator cr;
  std::map<std::string,ipc::PixBroker_var> brokers;
  std::string brokerFlavour[2] = {"TIM:","CRATE:"};
  for(cr = m_rodCrates.begin(); cr != m_rodCrates.end(); cr++) {
   if(cr->second->enableReadout && cr->second->active() && !m_disable->isDisable(cr->first))
   
   msg= "Restarting actions in"+ brokerFlavour[0] + (*cr).first + " "+  brokerFlavour[1] + (*cr).first;
   ers::info(PixLib::pix::daq (ERS_HERE, "ACTIONS_RESTART", msg));
   
   for(int i=0;i<2;i++){
     std::string object_name = brokerFlavour[i] + (*cr).first;
     if (m_ipcPartition->isObjectValid<ipc::PixBroker>(object_name)) {
       ipc::PixBroker_var broker;
       try {
	 broker = m_ipcPartition->lookup<ipc::PixBroker>(object_name);
       } catch(...) {
         ers::error(PixLib::pix::daq (ERS_HERE, "BROKER_CONNECTION_FAILURE", "Error creating broker handle"));
       }
       std::string name = broker->ipc_name();
       if (name == object_name)brokers[name] = broker;
     } else {
       msg= "IPC Object "+object_name+" is invalid";
       ers::error(PixLib::pix::daq(ERS_HERE, "BROKER_INVALID", msg));
     }
   }
  }
  for (auto br : brokers) {
    (br.second)->ipc_restartActions();
  }
#ifdef WAIT_FOR_ACTIONS_TO_RESTART
  bool all_done = true;
  do {
    sleep(2);
    for (auto br : brokers) {
      if (!(br.second)->ipc_restartActionsCompleted()) all_done = false;
    }
  } while (!all_done); 
#endif // WAIT_FOR_ACTIONS_TO_RESTART
#endif
  std::cout << __PRETTY_FUNCTION__ << ": Done" << std::endl;
}

void PixRCDModule::cleanup() {
  if (m_alreadyRanCleanup) return;
  
  m_alreadyRanCleanup = true;
  
  ers::info(PixLib::pix::daq (ERS_HERE, m_partName,"Executing PixRCDModule::cleanup()"));
    
  if (m_timActions != nullptr) {
    m_timActions->unconfigure();
    m_broker->deallocateActions(m_timActions);
    m_timActions = nullptr;
  }

  if (m_rodActions != nullptr) {
    m_rodActions->unconfigure();
    m_broker->deallocateActions(m_rodActions);
    m_rodActions = nullptr;
  }

  std::cout << "Destroying broker @ m_broker=" << m_broker << std::endl;
  if (m_broker != 0) {
    m_broker->_destroy();
  }
  std::cout << "Broker @ m_broker=" << m_broker << " destroyed" << std::endl;
  
  ers::log(PixLib::pix::daq(ERS_HERE, m_partName,"Actions successfully deallocated"));

#define ALLOW_RESTART_ACTIONS
#ifdef ALLOW_RESTART_ACTIONS
    // De-allocation implied by restart
    // Perform restart only if called via unconfigure transition (and not exit handler)
  if (m_inPixRCDModuleUnconfigure && m_passedStartTransition) {
    restartActions();
  }
#endif

  if (m_conn!=nullptr) {
    delete m_conn;
    m_conn=nullptr;
  }
  std::cout<<"conn deleted "<<std::endl;
  
  if (m_disable!=nullptr) {
    delete m_disable;
    m_disable=nullptr;;
  }
  std::cout<<"disable deleted "<<std::endl;
  if (m_dbServer!=nullptr) {
    delete m_dbServer;
    m_dbServer=nullptr;;
  }
  std::cout<<"dbServer deleted "<<std::endl;
  if (m_is!=nullptr) {
    delete m_is;
    m_is = nullptr;
  }
  std::cout<<"m_is deleted "<<std::endl;
  if (m_isDD!=nullptr) {
    delete m_isDD;
    m_isDD = nullptr;
  }
  std::cout<<"m_isDD deleted "<<std::endl;
  if (m_is2!=nullptr) {
    delete m_is2;
    m_is2  = nullptr;
  }
  if (m_ipcPartition!=nullptr) {
    delete m_ipcPartition;
    m_ipcPartition = nullptr;
  }
  std::cout<<"m_ipcPartition deleted "<<std::endl;
  if (m_ipcPartitionDD!=nullptr) {
    delete m_ipcPartitionDD;
    m_ipcPartitionDD  = nullptr;
  }
  std::cout<<"m_ipcPartitionDD deleted "<<std::endl;

 ers::info(PixLib::pix::daq (ERS_HERE, m_partName,"PixRCDModule::cleanup() done"));

}

void PixRCDModule::setup(DFCountedPointer<ROS::Config> configuration) {
  m_time=gettimeofday(&m_t0,NULL);

  m_alreadyRanCleanup = false;
  
  std::cout<<"Called PixRCDModule::setup"<<std::endl;

  // Install the exit handler
  struct sigaction sa;
  sigemptyset(&sa.sa_mask); 
  sa.sa_flags = 0;
  sa.sa_handler = exitHandler; 
  int ret1=0, ret2=0;
  ret1 = sigaction(SIGTERM, &sa, nullptr);
  ret2 = sigaction(SIGINT, &sa, nullptr);
  if (ret1 != 0 || ret2 != 0) {
    msg= "NO_EXTHDL "+m_partName+": cannot install the exit handler";
    ers::error(PixLib::pix::daq (ERS_HERE, "PixRCDModule::setup", msg));
  }

  // Decide if you get all the environment variable directly from xml ("getenv"), or from substitution map !!!! 

  // CREATE IPC PARTITION
  std::string IPCPartitionName = "";
  IPCPartitionName = getenv("TDAQ_PARTITION_INFRASTRUCTURE");
  if (IPCPartitionName != "") {
    std::cout << IPCPartitionName << std::endl;
  } else {
    msg= "PixRCDModule::setup: ERROR!! Could not read 'TDAQ_PARTITION_INFRASTRUCTURE' from XML files";
    ers::fatal(PixLib::pix::daq (ERS_HERE, "PixRCDModule::setup", msg));
    return;
  }

  std::cout << "Trying to connect to partition " << IPCPartitionName << " ... " << std::endl;
  try {
    m_ipcPartition = new IPCPartition(IPCPartitionName);
  } catch(...) {
    msg= "Problems while connecting to the partition"+ IPCPartitionName;
    ers::fatal(PixLib::pix::daq (ERS_HERE, "PixRCDModule::setup", msg));
    return;
  }
  std::cout << "Succefully connected to partition " << IPCPartitionName << std::endl << std::endl;
  
  std::string IPCPartitionNameDD = "";
  IPCPartitionNameDD = getenv("TDAQ_PARTITION");
  if (IPCPartitionNameDD == "") {
    msg= "Could not read 'TDAQ_PARTITION' from XML files";
    ers::fatal(PixLib::pix::daq (ERS_HERE, "PixRCDModule::setup", msg));
    return;
  }
  std::cout << "Trying to connect to partition " << IPCPartitionNameDD << " ... " << std::endl;
  m_ipcPartitionDD=0;
  try {
    m_ipcPartitionDD = new IPCPartition(IPCPartitionNameDD);
  } catch(...) {
    msg= "Problems while connecting to the partition"+ IPCPartitionNameDD;
    ers::fatal(PixLib::pix::daq (ERS_HERE, "PixRCDModule::setup", msg));
    return;
  }
  std::cout << "Succefully connected to partition " << IPCPartitionNameDD << std::endl << std::endl;

  // Initialize IS server manager
  bool IsReady=true;
  m_isName = "PixelRunParams";
  try {
    m_is = new PixLib::PixISManager(m_ipcPartition, m_isName);
  } catch(...) {
    IsReady=false;
    msg= "Could not connect to IsServer "+m_isName;
    ers::fatal(PixLib::pix::daq (ERS_HERE, "NO_IS_SERVER", msg));
    return;
  }
  if (IsReady) std::cout << "Succefully created PixIsManager " << m_isName << std::endl << std::endl;  
  
  m_is2Name = "RunParams";
  try {
    m_is2 = new PixLib::PixISManager(m_ipcPartition, m_is2Name);
  } catch(...) {
    IsReady=false;
    msg= "NO_IS_SERVER","Could not connect to IsServer "+m_is2Name;
    ers::error(PixLib::pix::daq (ERS_HERE, "NO_IS_SERVER", msg));
  }
  if (IsReady) std::cout << "Succefully created PixIsManager " << m_isName << std::endl << std::endl;  
  
  m_isNameDD = "RunParams";
  try {
    m_isDD = new PixLib::PixISManager(m_ipcPartitionDD, m_isNameDD);
  } catch(...) {
    IsReady=false;
    msg="NO_IS_SERVER","Could not connect to IsServer "+m_isNameDD;
    ers::error(PixLib::pix::daq (ERS_HERE, "NO_IS_SERVER", msg));
  }
  if (IsReady) std::cout << "Succefully created PixIsManager " << m_isNameDD << std::endl << std::endl;  
  
  // GET CONFIGURATION from XML
  m_configuration = configuration; //Pointer to the ReadoutModule configuration
  bool mapIsGood = false;
  PixRCDConfigHandler* configHandler = new PixRCDConfigHandler(m_configuration);
  mapIsGood = configHandler->extractConversionMap(m_convMap);
  if(!mapIsGood){  
    ers::warning(PixLib::pix::daq (ERS_HERE, "NO_XML_INFO","Could not get information from XML"));
  }
  // Here the substitution map is extracted from Configuration, in order to control substituttion of variables with their content. 

  getTags_CrateList(mapIsGood);
  //IS access for tags update
  //bool tagsFromIs = m_configuration->getBool("TagsFromIS");
  delete configHandler;
  
  std::cout << std::endl << "Tags before reading from IS:" << std::endl;
  std::cout << "- Connectivity Tag : " << m_connTag << std::endl;
  std::cout << "- Id tag: " << m_idTag << std::endl;
  std::cout << "- Configuration Tag: " << m_cfgTag << std::endl;
  std::cout << "- Module Configuration Tag: " << m_cfgModTag << std::endl << std::endl; 

  updateParams(m_connTag  , "DataTakingConfig-ConnTag");
  updateParams(m_cfgTag   , "DataTakingConfig-CfgTag");
  updateParams(m_cfgModTag, "DataTakingConfig-ModCfgTag");
  updateParams(m_idTag    , "DataTakingConfig-IdTag");
  updateParams(m_disName  , "DataTakingConfig-Disable",false);
  updateParams(m_runName  , "DataTakingConfig-RunConfig",false);
  //updateParams(m_cfgRev   , "DataTakingConfig-CfgRev");
  //updateParams(m_disRev   , "DataTakingConfig-DisableRev");
  //updateParams(m_runRev   , "DataTakingConfig-RunConfigRev");
  
  if (m_disName=="") {
    ers::warning(PixLib::pix::daq (ERS_HERE, "PUT_DEF", "Using default name 'GlobalDis' for PixDisable object"));
    m_disName="GlobalDis";
  }	
  if (m_runName=="") {
    ers::warning(PixLib::pix::daq (ERS_HERE, "PUT_DEF", "Using default name 'Global' for PixRunConfig object"));
    m_runName="Global";
  }
 
  msg ="Using tag: "+m_idTag+" / "+m_connTag+" / "+m_cfgTag+" / "+m_cfgModTag;
  ers::log(PixLib::pix::daq (ERS_HERE, "SUMMARY", msg));
  
  std::ostringstream dump;
  dump << "PixDisable: "+m_disName+" ( / " << m_disRev << ")  /  PixRunConfig:"+m_runName+" ("<<m_runRev << ")";
  ers::log(PixLib::pix::daq (ERS_HERE, "SUMMARY", dump.str()));


  // GETTING PARTITION FROM CRATE NAME
  std::string choice = "";
  choice = getenv("DBSRVSEL");
  if (choice=="") {
    ers::warning(PixLib::pix::daq (ERS_HERE, "NO_PAR_IN_XML","Variables DBSRVSEL not present in xml! Using default name 'DbServer'"));
    choice="DbServer";
  }
  
  if (choice=="DbServer" ) {
    std::string serverName = "";
    serverName = getenv("PIXDBSERVER_NAME");
    if (serverName=="") {
    ers::warning(PixLib::pix::daq (ERS_HERE, "NO_PAR_IN_XML","NO_PAR_IN_XML","Variables PIXDBSERVER_NAME not present in xml! Using default name 'DbServer'"));
      serverName="PixelDbServer";
    
    }
    bool ready=false;
    int nTry=0;
    std::cout << "INFO: Trying to connect to DbServer with name " << serverName << "  ..... " << std::endl;
    do {
      sleep(1);
      m_dbServer=new  PixLib::PixDbServerInterface(m_ipcPartition,serverName,PixLib::PixDbServerInterface::CLIENT, ready);
      if(!ready)  {
	delete m_dbServer;
	m_dbServer = 0;
      }
      else break;
      std::cout << " ..... attempt number: " << nTry << std::endl;
      nTry++;
    } while(nTry<10);
    if(!ready) {
      msg= "Impossible to connect to DbServer "+serverName;
      ers::fatal(PixLib::pix::daq (ERS_HERE, "NO_DBSERVER",msg));
      return;
    } else {
      msg= "Successfully connected to DbServer with name "+serverName; 
      ers::info(PixLib::pix::daq (ERS_HERE, "DBSERVER_OK",msg));
    }
    
    sleep(1);
    m_dbServer->ready();
      ers::info(PixLib::pix::daq (ERS_HERE, "DBSERVER_OK","DbServer has been enabled for reading"));
    try{
      m_conn = new PixLib::PixConnectivity(m_dbServer, m_connTag, m_connTag, m_connTag, m_cfgTag, m_idTag, m_cfgModTag);
      m_conn->loadConn();
    }
    catch(PixLib::PixConnectivityExc& e) { 
      msg="PixConnectivity Exception caught in constructor: "+e.getDescr(); 
      ers::fatal(PixLib::pix::daq (ERS_HERE, "NO_PIXCONN",msg));
      return;
    }
    catch(...) { 
      ers::fatal(PixLib::pix::daq(ERS_HERE, "NO_PIXCONN","Unknown exception caught when creating PixConnectivity"));
      return;
    }
  } else {
    std::cout << "INFO: DBSRVSEL is different from 'DbServer' and the connectivity will be loaded in the normal way" << std::endl;
    try{
      m_conn = new PixLib::PixConnectivity(m_connTag, m_connTag, m_connTag, m_cfgTag, m_idTag, m_cfgModTag);
    }
    catch(PixLib::PixConnectivityExc& e) { 
      msg="PixConnectivity Exception caught: "+e.getDescr(); 
      ers::fatal(PixLib::pix::daq (ERS_HERE, "NO_PIXCONN",msg));
      return;
    }
    catch(...) { 
      ers::fatal(PixLib::pix::daq (ERS_HERE, "NO_PIXCONN","Unknown exception caught"));
      return;
    }
    m_conn->loadConn();
  }
  
  firstDisable=true;
  if(m_dbServer != 0) putPixDisable();
  firstDisable=false;

  readPartNameFromSchema();
  // Looking for crate name and partition from where this RunControl belongs to: 
  m_crateName = "UNKNOWN";//Does it make any sense? RCD can allocate multiple crates
  //char rcc_Name[64];
  //int size=64;
  //gethostname(rcc_Name, size);
  //std::cout<<"rcc_name =" <<rcc_Name<<std::endl;
  //std::string ipName(rcc_Name); 
  //std::cout<<"hostname = "<<ipName<<std::endl;

  PixLib::PartitionConnectivity* myPartition = NULL;
  std::map<std::string, PixLib::SbcConnectivity*>::iterator sbcIt;
  for(sbcIt = (m_conn->sbcs).begin(); sbcIt != (m_conn->sbcs).end(); sbcIt++ ) {
    //std::cout<<"Sbc name = " << (sbcIt->second)->ipName() << " -----> partition name = " << (((sbcIt->second)->crate() )->partition())->name() << std::endl;
    //std::cout<< (((sbcIt->second)->crate() )->partition())->name() <<" --> "<<m_partName <<std::endl;
    if( (((sbcIt->second)->crate() )->partition())->name() == m_partName) {
      //m_crateName = ((sbcIt->second)->crate())->name();
      myPartition = ( (sbcIt->second)->crate() )->partition();
      break;
    }
  }

  if (myPartition==NULL) {
    msg= "Could not find Partition connectivity for "+m_partName;
    ers::fatal(PixLib::pix::daq (ERS_HERE, "NO_PART_FOUND",msg));
    return;
  }
  msg = m_partName," has PartitionConnectivity Name:"+myPartition->name();
  ers::info(PixLib::pix::daq(ERS_HERE, "SUMMARY",msg));
  
  // Retrieving crates list and IS server name:
  if(myPartition != NULL) {
    m_rodCrates = (myPartition)->rodCrates();
    m_isName = myPartition->ddcISServName();
  }
  if (m_rodCrates.size()==0) {
    msg = m_partName+": PixRCDModule has an empty RodCrates list: cannot allocate anything. ";
    ers::error(PixLib::pix::daq (ERS_HERE, "NO_CRATES",msg));
  }

  if(!allocate())return;
  disableModules();
  m_time=gettimeofday(&m_t1,NULL);
  std::ostringstream text;
  text << m_partName << ": time to setup is:  " << m_t1.tv_sec - m_t0.tv_sec+ 1e-6*(m_t1.tv_usec-m_t0.tv_usec);
  ers::debug(PixLib::pix::daq (ERS_HERE, "TIME_INFO",text.str()));
}

void PixRCDModule::configure(const daq::rc::TransitionCmd&) {
  m_time=gettimeofday(&m_t0,NULL);
  if(m_timActions != 0) {
    m_timActions->reloadTags(m_cfgTag,m_cfgModTag,m_cfgRev,m_cfgModRev);
    m_timActions->configure(m_ipcPartitionDD->name(), PixLib::PixActions::Synchronous);
    std::cout<<"Sent configure transition to TIM actions, if present"<<std::endl;
  }

  if(m_rodActions != 0) {
    m_rodActions->reloadTags(m_cfgTag,m_cfgModTag,m_cfgRev,m_cfgModRev);
    m_rodActions->configure(m_ipcPartitionDD->name(), PixLib::PixActions::Synchronous);
    std::cout<<"Sent configure transition to ROD actions, if present"<<std::endl;
  }
  std::cout<<"Called PixRCDModule::configure, all done."<<std::endl;
  m_time=gettimeofday(&m_t1,NULL);
  std::ostringstream text;
  text << m_partName << ": time to configure is:  " << m_t1.tv_sec - m_t0.tv_sec+ 1e-6*(m_t1.tv_usec-m_t0.tv_usec);
  ers::debug(PixLib::pix::daq (ERS_HERE, "TIME_INFO",text.str())); 

}


void PixRCDModule::connect(const daq::rc::TransitionCmd&) {
  std::cout<<"Called PixRCDModule::connect (calls connect of actions)"<<std::endl;
  m_time=gettimeofday(&m_t0,NULL); 
  if(m_rodActions != 0)
    m_rodActions->connect();
  if(m_timActions != 0)
    m_timActions->connect();
  m_time=gettimeofday(&m_t1,NULL);
  std::ostringstream text;
  text << m_partName << ": time to connect is:  " << m_t1.tv_sec - m_t0.tv_sec+ 1e-6*(m_t1.tv_usec-m_t0.tv_usec);
  ers::debug(PixLib::pix::daq (ERS_HERE, "TIME_INFO",text.str())); 
}

void PixRCDModule::prepareForRun(const daq::rc::TransitionCmd&) {
  std::cout<<"Called PixRCDModule::prepareForRun (calls setActionsConfig and prepareForRun)"<<std::endl;
  if (m_isDD != 0 && m_is != 0) {
    RunParams p = m_isDD->read<RunParams>("RunParams");
    unsigned int rn = p.run_number;
    try {
      m_is->publish("DT-RunNumber", rn);
      std::ostringstream text;
      text << "Run Number (" << rn << ") copied to PixelRunParams";
      ers::info(PixLib::pix::daq (ERS_HERE, "RUN_NUMBER",text.str()));
      if (m_isDD->exists("LumiBlock")) {
        LuminosityInfo lbInfo;
        lbInfo = m_isDD->read<LuminosityInfo>("LumiBlock");
        std::ostringstream lbText;
        lbText << "LumiBlock number " << lbInfo.LumiBlockNumber;
        ers::info(PixLib::pix::daq (ERS_HERE, "LUMI_BLOCK",lbText.str()));
      }
    }
    catch (...) {
      ers::error(PixLib::pix::daq (ERS_HERE,"RUN_NUMBER_ERROR","Exception caught trying to copy run number to PixelRunParams"));
    }
  } else {
    ers::error(PixLib::pix::daq (ERS_HERE,"RUN_NUMBER_ERROR","Impossible to copy run number to PixelRunParams"));
  }
  putPixDisable();
  disabledFromDCS();
  computeRodBusyMask();
  std::string dis = disableModules();
  if (dis != "") {
    daq::rc::ModulesDisabled issueHW(ERS_HERE, dis.c_str(), true);
    ers::info(issueHW);
  }
  m_time=gettimeofday(&m_t0,NULL); 
  if(m_timActions != 0) {
    m_timActions->setActionsConfig();
    m_timActions->prepareForRun();
  }
  checkLatency();
  
  if(m_rodActions != 0) {
    m_rodActions->setActionsConfig(); 
    m_time=gettimeofday(&m_t1,NULL);
    std::cout<< std::endl << "PrepareForRun, after setActionsConfig:  " << m_t1.tv_sec - m_t0.tv_sec+ 1e-6*(m_t1.tv_usec-m_t0.tv_usec) <<std::endl << std::endl;
    m_rodActions->prepareForRun();
    m_time=gettimeofday(&m_t1,NULL);
    std::cout<< std::endl << "PrepareForRun, after prepareForRun:  " << m_t1.tv_sec - m_t0.tv_sec+ 1e-6*(m_t1.tv_usec-m_t0.tv_usec) <<std::endl << std::endl;
    m_rodActions->startTrigger();
    m_time=gettimeofday(&m_t1,NULL);
    std::cout<< std::endl << "PrepareForRun, after startTrigger:  " << m_t1.tv_sec - m_t0.tv_sec+ 1e-6*(m_t1.tv_usec-m_t0.tv_usec) <<std::endl << std::endl;
  }
  if(m_timActions != 0) {
    m_timActions->startTrigger();
  }
  m_pause = false;
  m_runStarted = true;
  m_passedStartTransition = true;
  m_time=gettimeofday(&m_t1,NULL);
  std::ostringstream text;
  text << m_partName << ": time to prepareForRun is:  " << m_t1.tv_sec - m_t0.tv_sec+ 1e-6*(m_t1.tv_usec-m_t0.tv_usec);
  ers::debug(PixLib::pix::daq (ERS_HERE, "TIME_INFO",text.str())); 
}

void PixRCDModule::stopROIB(const daq::rc::TransitionCmd&) {
  m_pause = false;
  m_runStarted = false;
}

void PixRCDModule::pause() {
  m_pause = true;
  if (m_pause) {
    if(m_timActions != 0) {
      m_timActions->stopTrigger();
      putPixDisable();
      computeRodBusyMask();
      m_timActions->startTrigger();
    }
  }
}

void PixRCDModule::resume() {
  m_pause = false;
}

void PixRCDModule::stopDC(const daq::rc::TransitionCmd&) {
  if(m_timActions != 0) {
    m_timActions->stopTrigger();
  }
  if(m_rodActions != 0) {
    m_rodActions->stopTrigger();
    m_rodActions->stopEB();
  }
  if(m_timActions != 0) {
    m_timActions->stopEB();
  }
}


void PixRCDModule::disconnect(const daq::rc::TransitionCmd&) {
  //reenable everything in the actions multi 
  if(m_timActions != 0) {
    // for(std::set<std::string>::iterator it = actEnaTIM.begin(); it !=  actEnaTIM.end(); it++) m_timActions->setRodActive(*it, true);
    m_timActions->setRodActive(true);
    m_timActions->disconnect();
  }
  if(m_rodActions != 0) {
    // for(std::set<std::string>::iterator it = actEnaROD.begin(); it !=  actEnaROD.end(); it++) m_rodActions->setRodActive(*it, true);
    m_rodActions->setRodActive(true);
    m_rodActions->disconnect();
  }
}

void PixRCDModule::unconfigure(const daq::rc::TransitionCmd&) {
  m_inPixRCDModuleUnconfigure = true;
  cleanup();
  m_inPixRCDModuleUnconfigure = false;
}

DFCountedPointer<ROS::Config> PixRCDModule::getInfo() {
  // Get config object
  DFCountedPointer<ROS::Config> configuration = ROS::Config::New();
  return(configuration);
}

void PixRCDModule::probe() {
  if(m_alreadyRanCleanup)return;

  try {
    if (m_runStarted) {
      if(m_timActions != 0)
	m_timActions->probe();
      if (m_rodActions != 0)
	m_rodActions->probe();
    }
  }
  catch(...) {
    ers::warning(PixLib::pix::daq (ERS_HERE,"PROBE Execution failed",""));  
  }
}

void PixRCDModule::disable(const std::vector<std::string>& objNames) {
  // Pause triggers (set ROD busy at the TIM)
  if (m_timActions != 0) {
    m_timActions->stopTrigger();
  }
  bool found = false;
  if (m_rodActions != 0) {
    for(const auto& objName : objNames) {
     std::cout << "Calling disable with par = " << objName << std::endl; 
     m_rodActions->disable(objName);

      std::map<std::string, PixLib::RodCrateConnectivity*>::iterator ic;
      if (!m_disable->isDisable(m_partName)) {
	for (ic = m_rodCrates.begin(); ic != m_rodCrates.end(); ++ic) {
	  PixLib::RodCrateConnectivity *c = ic->second;
	  if(c->enableReadout && c->active() && !m_disable->isDisable(ic->first)) {
	    std::map<int, PixLib::RodBocConnectivity*>::iterator ir;
	    for (ir=c->rodBocs().begin(); ir!=c->rodBocs().end(); ++ir) {
	      if (ic->second != NULL) {
		PixLib::RodBocConnectivity *r = ir->second;
		if (objName == r->name()) {
		  r->active() = false;
		  found = true;
		}
	      }
	    }
	  }
	}
      }
    }
  }
  if (found) computeRodBusyMask();
  // Resume triggers
  if (m_timActions != 0) {
    m_timActions->startTrigger();
  }
}

void PixRCDModule::enable(const std::vector<std::string>& objNames) {
  // Pause triggers (set ROD busy at the TIM)
  if (m_timActions != 0) {
    m_timActions->stopTrigger();
  }
  bool found = false;
  for(const auto& objName : objNames) {
    std::map<std::string, PixLib::RodCrateConnectivity*>::iterator ic;
    if (!m_disable->isDisable(m_partName)) {
      for (ic = m_rodCrates.begin(); ic != m_rodCrates.end(); ++ic) {
	PixLib::RodCrateConnectivity *c = ic->second;
	if(c->enableReadout && c->active() && !m_disable->isDisable(ic->first)) {
	  std::map<int, PixLib::RodBocConnectivity*>::iterator ir;
	  for (ir=c->rodBocs().begin(); ir!=c->rodBocs().end(); ++ir) {
	    if (ic->second != NULL) {
	      PixLib::RodBocConnectivity *r = ir->second;
	      if (objName == r->name()) {
		r->active() = true;
		found = true;
	      }
	    }
	  }
	}
      }
    }
    if (found) computeRodBusyMask();
    if (m_rodActions != 0) {
      m_rodActions->enable(objName);
    }
  }
  // Resume triggers
  if (m_timActions != 0) {
    m_timActions->startTrigger();
  }
}

void PixRCDModule::user(const daq::rc::UserCmd& usrCmd) {
  if(m_isDD->exists("LumiBlock")) {
    LumiBlock lbInfo = m_isDD->read<LumiBlock>("LumiBlock");
    std::cout << __PRETTY_FUNCTION__ << " recieved at LB " << lbInfo.LumiBlockNumber << std::endl;
  }
  // User command name and arguments
  std::string cmd = usrCmd.commandName();
  std::string par; 
  const auto& params = usrCmd.commandParameters();
  std::cout << "USER Command name is: " << cmd << std::endl;
  std::cout << "USER Command parameters: ";
  for(const auto& p : params) {
    std::cout << "- " << p;
    if (par != "") par = par + " ";
    par = par + p;
  }
  std::cout << std::endl;
  std::cout << "USER Command parameter: -" << par << "-" << std::endl;

  msg = "Cmd = "+cmd+" Par = "+par;
  if(cmd != "AUTODISENA" )ers::warning(PixLib::pix::daq (ERS_HERE,"USER_COMMAND",msg)); 
  if (m_rodActions != 0) {
    if (cmd == "PIXEL_UP" || cmd == "PIXEL_DOWN" || cmd == "RESYNC" ) {
      m_rodActions->warmReconfig(cmd, par);
    } else if (cmd == "RECOVER"){
      m_rodActions->recover(cmd, par);
     } else if (cmd == "AUTODISENA"){
      std::string itemType, itemName,applName;
      std::stringstream ss(par);
      ss >> itemName >> itemType>>applName;

      if (itemType == "MOD") {
        if (itemName[0] == '+') {
          std::string mod = itemName.substr(1);
          daq::rc::ModulesEnabled issueHW(ERS_HERE, mod.c_str(), false);
          ers::warning(issueHW);
        } else {
          std::string mod = itemName;
          daq::rc::ModulesDisabled issueHW(ERS_HERE, mod.c_str(), false);
          ers::warning(issueHW);
        }
      } else if (itemType == "ROD") {
  
        if (itemName[0] == '*') {
          std::string rod = itemName.substr(1);
          daq::rc::HardwareRecovered issueHW(ERS_HERE, rod.c_str(),applName.c_str());
          ers::warning(issueHW);
        } else if (itemName[0] == '+') {
          std::string rod = itemName.substr(1);
          daq::rc::ReadyForHardwareRecovery issueHW(ERS_HERE, rod.c_str(),applName.c_str());
          ers::warning(issueHW);
        } else  {
          std::string rod = itemName;
          daq::rc::HardwareError issueHW(ERS_HERE, rod.c_str(),applName.c_str());
          ers::warning(issueHW);
        }
      }

    } else if (cmd == "ENABLEMODULES") {
      std::istringstream ecrS(par);
      unsigned int ecrC = 0;
      std::string modName;
      ecrS >> ecrC >> modName;
      std::vector<std::string> modNames(1,modName);
      enable(modNames);
    } else if (cmd == "RECONFIG") {
      m_rodActions->enable("RECONFIG "+par);
    }
  }
}

void PixRCDModule::resynch(const daq::rc::ResynchCmd& resyncCmd) {
  // resynch command name and arguments
  std::uint32_t ecr = resyncCmd.ecrCounts();
  std::string par = std::to_string(ecr);
	const std::vector<std::string> &mods = resyncCmd.modules();
	const std::vector<std::string> *modsPtr = &mods;

	if( modsPtr != 0 ) {
		std::cout << __PRETTY_FUNCTION__ << " modules: ";
		for(auto& m : mods) std::cout << m << "\t";
		std::cout << std::endl;
	} else std::cout << "resyncCmd.modules() returned a NULL-reference" << std::endl;

	if(mods.size()==1) par += " " + mods[0];

  msg = "Par = "+par; 
  ers::warning(PixLib::pix::daq (ERS_HERE,"RESYNCH_COMMAND",msg)); 
  if (m_rodActions != 0) {
    m_rodActions->warmReconfig("RESYNC", par);
  }
}

void PixRCDModule::publishFullStatistics() {
}

void PixRCDModule::clearInfo() {
}

// CODE NEEDED BY THE PLUGIN FACTORY
extern "C"
{
  extern ReadoutModule* createPixRCDModule();
}
ReadoutModule* createPixRCDModule()
{
  return (new PixRCDModule());
}

void PixRCDModule::updateParams(std::string& paramValue, std::string paramName, bool publish) {
  // Reads if paramName is published in IS. If yes, it overwrites the local paramValue;
  // if not, it publishes local paramValue.
  std::string value="";
  try {
    if(m_is->exists(paramName)) value = m_is->read<std::string>(paramName);
    else {
      msg ="Parameter <"+paramName+"> not found in IsServer";
      ers::warning(PixLib::pix::daq (ERS_HERE, "FAIL_IN_IS",msg));
    }
  } catch(...) {
      msg= "Problem in reading parameter <"+paramName+"> from IsServer, check PixelInfr";
      ers::fatal(PixLib::pix::daq (ERS_HERE, "FAIL_IN_IS",msg));
  }
  if (value == "")  {
    if (publish) {
      msg = "Parameter <"+paramName+"> was not found in IS. Updating IS with value: "+paramValue;
      ers::warning(PixLib::pix::daq (ERS_HERE, "NO_TAG_IN_IS",msg));
      if (m_is!=0) m_is->publish(paramName, paramValue);
    }
  } else {
    paramValue = value;
    msg = "Parameter <"+paramName+"> found in IS with value: "+paramValue;
    ers::info(PixLib::pix::daq (ERS_HERE, "IS_UPDATE",msg));
  }
}

void PixRCDModule::readFromSchema() {
  std::cout<<"PixRCDModule::setup: taking default Connectivity tags directly from attr name definition in Crates xml."<<std::endl;
  m_connTag   = m_configuration->getString("ConnectivityTagC");
  m_cfgTag    = m_configuration->getString("ConnectivityTagCF");
  m_cfgModTag = m_configuration->getString("ConnectivityTagModCF");
  m_idTag = m_configuration->getString("ConnectivityTagID");
}

void PixRCDModule::readPartNameFromSchema() {
m_partName = m_configuration->getString("LocalPartitionName");
std::cout<<"Local partition Name "<< m_partName<<std::endl;
}

void PixRCDModule::getTags_CrateList(bool mapIsGood) {
  bool error = false;  
  if(mapIsGood) {
    std::map<std::string,std::string>::iterator mapit;
    mapit = (m_convMap).find("CONNECTIVITY_RUN_TAG_C");
    if (mapit!=(m_convMap).end()) m_connTag = (*mapit).second;
    else {
      ers::warning(PixLib::pix::daq (ERS_HERE, "NO_PAR_IN_XML","Cannot find 'CONNECTIVITY_RUN_TAG_C' var into the conversion map."));
      error = true;
    }
    mapit = (m_convMap).find("CONNECTIVITY_RUN_TAG_CF");
    if (mapit!=(m_convMap).end()) m_cfgTag = (*mapit).second;
    else {
      ers::warning(PixLib::pix::daq (ERS_HERE, "NO_PAR_IN_XML","Cannot find 'CONNECTIVITY_RUN_TAG_C' var into the conversion map."));
      error = true;
    }
    mapit = (m_convMap).find("CONNECTIVITY_RUN_TAG_MOD_CF");
    if (mapit!=(m_convMap).end()) m_cfgModTag = (*mapit).second;
    else {
      ers::warning(PixLib::pix::daq (ERS_HERE, "NO_PAR_IN_XML","Cannot find 'CONNECTIVITY_RUN_TAG_MOD_CF' var into the conversion map."));
      error = true;
    }
    mapit = (m_convMap).find("ID_TAG");
    if (mapit!=(m_convMap).end()) m_idTag = (*mapit).second;
    else {
      ers::warning(PixLib::pix::daq (ERS_HERE, "NO_PAR_IN_XML","Cannot find 'ID_TAG' var into the conversion map."));
      error = true;
    }
  }
  else
    readFromSchema();
  if (error)
    readFromSchema();
}


void PixRCDModule::disabledFromDCS() {
  std::string isName = "DDC";
  PixLib::PixISManager *isM(nullptr);
  try {
    isM = new PixLib::PixISManager(m_ipcPartition, isName);
  } catch(...) {
    msg = m_partName+": could not connect to IsServer "+isName;
    ers::error(PixLib::pix::daq (ERS_HERE, "NO_ISM",msg));
  }
  if (isM!=0) {
    std::map<std::string, std::string> disabledDCS;
    try {
      std::string isValue = isM->readDcs<std::string>("PIX_DisabledModuleList");
      std::string word;
      for (std::size_t i=0; i<isValue.size(); i++) {
	if (isValue[i] == ',') {
	  if (word.size() > 0) {
	    disabledDCS[word] = word;
	  }
	  word = "";
	} else {
	  word += isValue[i];
	}
      }
      if(word.size()) disabledDCS[word] = word;
    }
    catch (...) {
      msg = m_partName+": could not read DCS disabled module list";
      ers::error(PixLib::pix::daq (ERS_HERE, "NO_DISDCS",msg));
    }
    std::map<std::string, std::string>::iterator it;
    std::cout << "=== Modules disabled in DCS" << std::endl; 
    for (it=disabledDCS.begin(); it!=disabledDCS.end(); it++) {
      std::cout << "===   " << it->first;
      //PixLib::ModuleConnectivity *mod(nullptr);
      std::map<std::string, PixLib::RodCrateConnectivity*>::iterator ic;
      for (ic = m_rodCrates.begin(); ic != m_rodCrates.end(); ++ic) {
	PixLib::RodCrateConnectivity *c = ic->second;
	std::map<int, PixLib::RodBocConnectivity*>::iterator ir;
	for (ir=c->rodBocs().begin(); ir!=c->rodBocs().end(); ++ir) {
	  if (ic->second != NULL) {
	    PixLib::RodBocConnectivity *r = ir->second;
	    for (int i=0;i<5;i++) {
	      if (r->obs(i) != NULL) {
		if (r->obs(i)->pp0() != NULL) {
		  for (int j=0;j<=8;j++) {
		    PixLib::ModuleConnectivity *mod=r->obs(i)->pp0()->modules(j);
		    if (mod!=NULL) {
		      if (mod->name() == it->first) {
			if (mod->enableReadout && mod->active()) {
			  msg = "Module "+it->first+" disabled in DCS but not in DAQ - Disabled for data-taking";
			  ers::info(PixLib::pix::daq (ERS_HERE, "NOT_DISABLED_IN_DAQ",msg));
			  mod->active() = false;   	  
			  std::cout << " Will be disabled in data-taking"; 
			} else {
			  std::cout << " Disabled in PixDisable"; 
			}
		      }
		    }
		  }
		}
	      }
	    }
	  }
	}
      }
      std::cout << std::endl;
    }
  }
}

std::string PixRCDModule::disableModules() {
  std::string isName = "RunParams";
  PixLib::PixISManager *isM(nullptr);
  try {
    isM = new PixLib::PixISManager(m_ipcPartition, isName);
  } catch(...) {
    msg = m_partName+": could not connect to IsServer "+isName;
    ers::error(PixLib::pix::daq (ERS_HERE, "NO_ISM",msg));
  }
  std::string disabledModules = "";
  if (!m_disable->isDisable(m_partName)) {
    std::map<std::string, PixLib::RodCrateConnectivity*>::iterator cr;
    for(cr = m_rodCrates.begin(); cr != m_rodCrates.end(); cr++) {
      if(cr->second->enableReadout && cr->second->active() && !m_disable->isDisable(cr->first)) {
	std::map<int, PixLib::RodBocConnectivity*>::iterator rb;
	if (m_timActions && (cr->second)->tim() != NULL) {
	  PixLib::TimConnectivity *t = (cr->second)->tim();
	  std::string actionName = "SINGLE_TIM:" + (*cr).first + "/" +t->name();
	  if (t->enableReadout && t->active() && !m_disable->isDisable(t->name())) 
	    m_timActions->setRodActive(actionName, true);
	  else m_timActions->setRodActive(actionName, false);
	}
	for (rb=(cr->second->rodBocs()).begin(); rb!=(cr->second->rodBocs()).end(); ++rb) {
	  if (rb->second != NULL) {
	    PixLib::RodBocConnectivity *r = rb->second;
	    std::string actionName = "SINGLE_ROD:" + (*cr).first + "/" +r->name();
	    if (r->enableReadout && r->active() && !m_disable->isDisable(r->name())) {
	      m_rodActions->setRodActive(actionName,true);
	      for (int i=0;i<5;i++) {
		if (r->obs(i) != NULL) {
		  bool oen=false;
		  if (r->obs(i)->active() && !m_disable->isDisable(r->obs(i)->name())) oen=true;
		  if (r->obs(i)->pp0() != NULL) {
		    for (int j=0;j<=8;j++) {
		      PixLib::ModuleConnectivity *mod=r->obs(i)->pp0()->modules(j);
		      if (mod!=NULL) {
			bool actual=false;
			if (mod->enableReadout && mod->active() && oen && !m_disable->isDisable(mod->name())) actual=true;
			std::string mAct = r->name()+"/"+mod->name()+"/Active";
			int isValue=5;
			bool isBool=true;
			if (isM!=0) isValue = isM->read<int>(mAct.c_str());
			std::cout << "Value for " << mAct << " is " << isValue << std::endl;
			if (isValue==0) isBool=false;
			else if(isValue==1) isBool=true;
			if (isBool==actual) {
			  std::cout << "Module: " << mod->name() << " already in state " << actual << std::endl;
			} else {
			  std::cout << "Module: " << mod->name() << " putting in state " << actual << std::endl;
			  m_rodActions->setModuleActive(actionName,mod->modId,actual);
			}
                        if (!actual) {
                          if (disabledModules.size() > 0) disabledModules += ",";
                          disabledModules += mod->name();
			}
		      }
		    }
		  }
		}
	      }
	    } else m_rodActions->setRodActive(actionName,false);
	  }
	}	
      } else {
	if ((cr->second)->tim() != NULL) {
	  PixLib::TimConnectivity *t = (cr->second)->tim();
	  std::string actionName = "SINGLE_TIM:" + (*cr).first + "/" +t->name();
	  m_timActions->setRodActive(actionName, false);
	}
	std::map<int, PixLib::RodBocConnectivity*>::iterator rb;
	for (rb=(cr->second->rodBocs()).begin(); rb!=(cr->second->rodBocs()).end(); ++rb) {
	  if (rb->second != NULL) {
	    PixLib::RodBocConnectivity *r = rb->second;
	    std::string actionName = "SINGLE_ROD:" + (*cr).first + "/" +r->name();
	    m_rodActions->setRodActive(actionName,false);
	  }
	}
      }
    }
  }
  delete isM;
  return disabledModules;
}
  

bool PixRCDModule::allocate() {
  msg =m_partName+": creating PixBrokerMulticrate";
  ers::debug(PixLib::pix::daq (ERS_HERE, "BROKER",msg));

  std::string brokerName = m_partName+"_MultiCrate_Broker";
  brokerName += "_" + m_ipcPartitionDD->name();
  m_broker = new PixLib::PixBrokerMultiCrate(brokerName, m_ipcPartition);
  //types of error expected: exception, return??????????????????

  std::cout << "Created " << m_broker->name() << " broker" << std::endl;

  std::set<std::string> actionsList = m_broker->listActions(false);
  std::set<std::string> actionsListFree = m_broker->listActions(true);
  
  // Create the Multi for RODs
  std::set<std::string> RodsOnCrates;
  std::set<std::string>::iterator actions;
  bool actAllocated=false, notFound = false;
  std::string allocated, missing;
  
  //std::map<std::string, PixLib::RodCrateConnectivity*>::iterator cr;
  //std::map<int, PixLib::RodBocConnectivity*>::iterator j;
  for(const auto & cr : m_rodCrates) {
    bool crEna =(cr.second)->enableReadout &&(cr.second)->active();
    if(!crEna){
      std::cout << " Crate " << cr.first << " is DISABLED" << std::endl;
      continue;
    }
    std::cout << " Crate " << cr.first << " is ENABLED" << std::endl;
      for (const auto & j : (cr.second)->rodBocs() ) {
	if ( !j.second){
	  msg = m_partName+": no ROD active in the connectivity for RodCrate "+cr.first;
	  ers::warning(PixLib::pix::daq (ERS_HERE, "NO_ACTIONS",msg));
	  continue;
	}
	PixLib::RodBocConnectivity *r = j.second;
	bool rodEna = (r->enableReadout && r->active());
	std::string actionName = "SINGLE_ROD:" + cr.first + "/" +r->name();
	std::cout<<"ROD "<<actionName;
	rodEna ? std::cout<<" is ENABLED -- " : std::cout<<" is DISABLED -- ";
	
	  if ((actions=actionsListFree.find(actionName)) != actionsListFree.end()){
	      if(rodEna){
	        actEnaROD.insert((*actions));
	      }
	    std::cout<<"FREE"<<std::endl;
	  } else if ( (actions=actionsList.find(actionName)) != actionsList.end() ){
	      if(rodEna){
	        actAllocated = true; allocated+=actionName+"; ";
	      }
	    std::cout<<"ALLOCATED"<<std::endl;
	  } else {
	      if(rodEna){
	        notFound = true; missing+=actionName+"; ";
	      }
	    std::cout<<"NOT FOUND"<<std::endl;
	  }
      }
    PixLib::TimConnectivity *t = (cr.second)->tim();
    bool timEna = t->enableReadout && t->active();
    std::string actionName = "SINGLE_TIM:" + cr.first + "/" +t->name();
    std::cout<<"TIM "<<actionName;
    timEna ? std::cout<<" is ENABLED -- " : std::cout<<" is DISABLED -- ";
	if ((actions=actionsListFree.find(actionName)) != actionsListFree.end()){
	    if(timEna){
	      actEnaTIM.insert((*actions));
	    }
	  std::cout<<"FREE"<<std::endl;
	} else if ((actions=actionsList.find(actionName)) != actionsList.end() ){
	    if(timEna){
	      actAllocated = true; allocated+=actionName+"; ";
	    }
	  std::cout<<"ALLOCATED"<<std::endl;
	} else {
	    if(timEna){
	      notFound = true; missing+=actionName+"; ";
	    }
	  std::cout<<"NOT FOUND"<<std::endl;
	}
  
  }

  if(actAllocated){
    msg =  m_partName+": requested action(s): "+allocated+" already allocated.";
    //ers::fatal(PixLib::pix::daq (ERS_HERE, "FAIL_IN_ALLOCATION",msg));
    PIX_FATAL(msg);
    return false;
  }

  if(notFound){
    msg = m_partName+": requested action: "+missing+" not found, check PixelInfr";
    //ers::fatal(PixLib::pix::daq (ERS_HERE, "FAIL_IN_ALLOCATION",msg));
    PIX_FATAL(msg);
    return false;
  }

  // Fill m_rodActions
  m_rodActions = m_broker->allocateActions(actEnaROD);
  if(m_rodActions != 0) { 
    std::ostringstream text;
    text << m_partName << ": successfully allocated " <<  actEnaROD.size() << " ActionsSingleROD";
    //ers::log(PixLib::pix::daq (ERS_HERE, "ALLOCATION_OK",text.str()));
     PIX_LOG(m_partName << ": successfully allocated " <<  actEnaROD.size() << " ActionsSingleROD");
  } else {
  
    msg = m_partName+": No ActionSingleROD allocated";
    //ers::error(PixLib::pix::daq (ERS_HERE, "ALLOCATION_EMPTY",msg));
    PIX_ERROR(m_partName<<": No ActionSingleROD allocated");
  }

  // Create the Multi for TIM 
  std::cout << std::endl;
  std::cout << "Creating TIM actions" << std::endl;

  std::cout << __PRETTY_FUNCTION__ << " : Done looping over connectivity" << std::endl; // susepcting issue here - theim
  // Fill m_timActions
  if(m_rodActions != 0) {
    m_timActions = m_broker->allocateActions(actEnaTIM);
    if(m_timActions != 0) {
      std::ostringstream text;
      text << m_partName << ": successfully allocated " <<  actEnaTIM.size() << " ActionsSingleTIM";
      ers::log(PixLib::pix::daq (ERS_HERE, "ALLOCATION_OK",text.str()));
    } else {
      msg = m_partName+": No ActionSingleTIM allocated";
      ers::warning(PixLib::pix::daq (ERS_HERE, "ALLOCATION_EMPTY",msg));
    }
  }
  std::cout << std::endl;
  std::cout << "Compute Rod Busy Mask" << std::endl;
  computeRodBusyMask();
  std::cout << "End of PixRCDModule.cpp:allocate()" <<std::endl;
  
  return true;
  
}


void PixRCDModule::computeRodBusyMask() {
  
  if(m_timActions != 0) {
    std::set<std::string>::iterator actions; // Iterator on actions scheduled for allocation
    std::string actTimTarget; // ActionSingleTIM to be pushed with the RodBusyMask
    //!Here RodBusy mask is computed from RODs connected in the crate
    std::map<std::string, PixLib::RodCrateConnectivity*>::iterator j;
    unsigned int mask;
    if (!m_disable->isDisable(m_partName)) {
      for (j = m_rodCrates.begin(); j != m_rodCrates.end(); ++j) {
	mask = 0;
	PixLib::RodCrateConnectivity *c = j->second;
	if(c->enableReadout && c->active() && !m_disable->isDisable(j->first)) {
	  PixLib::TimConnectivity* t = c->tim();
	  std::string crateNam = j->first;
	  actTimTarget = "SINGLE_TIM:" + crateNam + "/" +t->name();
	  std::map<int, PixLib::RodBocConnectivity*>::iterator k;
	  for (k=c->rodBocs().begin(); k!=c->rodBocs().end(); ++k) {
	    if ((*k).second != NULL) {
	      PixLib::RodBocConnectivity *r = (*k).second;
	      if(r->enableReadout && r->active() && !m_disable->isDisable(r->name()))  {
		for(actions = actEnaROD.begin(); actions != actEnaROD.end(); actions++) {
		  std::cout << " --> rod called " << r->name() << " is set in RodBusyMask" << std::endl;
		  int vmeSlot = r->vmeSlot();
		  if (vmeSlot > 0 && vmeSlot < 22 && vmeSlot != 13) {
		    int offset = vmeSlot-5;
		    if (vmeSlot > 13) offset--;
		    mask |= (0x1<<offset);
		  }
		}
	      }
	    }
	  }
	  // All PixActionsSingleTIM are disabled, only the one belonging to the actual crate is enabled.
	  // The following code is not completely clean. I take the actEnaTIM container, which contains
	  // the PixActionsMulti of PixActionsSingleTIM name. So, this is expected to be like
	  // 'MULTI:SingleActionsName' with just one single action. This is an assumption, so it's 
	  // in some sense hardcoded. But it works, since we have just one single TIM action inside
	  // the PixActionsMulti(single) belonging to the single crates
	  
	  for(std::set<std::string>::iterator it = actEnaTIM.begin(); it !=  actEnaTIM.end(); it++){
	    if (m_timActions!=0)  m_timActions->setRodActive(*it, false);
	  }
	  if (m_timActions!=0) {
	    m_timActions->setRodActive(actTimTarget, true);
	    m_timActions->setRodBusyMask(mask);  
	    std::cout << "RodBusyMask = 0x" << std::hex << mask << std::dec << std::endl;
	  }
	}
      }
      // Re-enable everything before to exit
      for(std::set<std::string>::iterator it = actEnaTIM.begin(); it !=  actEnaTIM.end(); it++) {
	if (m_timActions!=0) m_timActions->setRodActive(*it, true);
      }
    }
  }
}

void PixRCDModule::putPixDisable() {
  if (firstDisable) {
    m_disable = m_conn->getDisable(m_disName);
    if (m_disable != NULL) {
      m_disable->put(m_conn);
    } else {
      msg = "Cannot read PixDisable "+m_disName+" from DbServer";
      ers::fatal(PixLib::pix::daq (ERS_HERE, "NO_OBJ_IN_DB",msg));
      return;
    }
  } else {
    m_newDisName = "";
    try {
      if (m_is!=0) m_newDisName = m_is->read<string>("DataTakingConfig-Disable");
    } catch (...) {
      msg = "Problem in reading PixDisable name from IS, default name is GlobalDis";
      ers::error(PixLib::pix::daq (ERS_HERE, "FAIL_IN_IS",msg));
    }
    if (m_newDisName=="") m_newDisName="GlobalDis";
    PixLib::PixDisable* myDisable = m_conn->getDisable(m_newDisName);
    if (myDisable != NULL) {
      myDisable->put(m_conn);
      delete myDisable;
    } else {
      msg = "Cannot read PixDisable "+m_newDisName+" from DbServer";
      ers::fatal(PixLib::pix::daq (ERS_HERE, "NO_OBJ_IN_DB",msg));
    }
  }
}

void PixRCDModule::checkLatency(){

  try{
  // Checking that the latency settings published by the TIMs are OK for ATLAS data-taking
  std::string tdaq_db = "";
  if (getenv("TDAQ_DB"))
    tdaq_db = getenv("TDAQ_DB");

  // Getting reference latency from ATLAS
  ::Configuration db(tdaq_db);
  const daq::core::Partition * partition = daq::core::get_partition(db, m_ipcPartitionDD->name());
  if(!partition && m_ipcPartitionDD->name().find("ATLAS") != std::string::npos) {
    std::stringstream ss;
    ss << "Cannot obtain the partition information for " << m_ipcPartitionDD->name() << " to determine the latency reference. Something is wrong. Call the Pixel On-Call at 16-0032!!!";
    ers::error(PixLib::pix::daq (ERS_HERE, "LATENCY_CHECK_ERROR",ss.str()));
  }
  
  const daq::core::TriggerConfiguration * tc = partition->get_TriggerConfiguration();
  int32_t atlasRefLatency = 0;
  if(tc) {
    atlasRefLatency = tc->get_LatencyValue();
  } else if( m_ipcPartitionDD->name().find("ATLAS") != std::string::npos ) {
    std::stringstream ss0;
    ss0 << "Cannot get TriggerConfiguration, and therefore cannot determine the latency reference. Something is wrong. Call the Pixel On-Call at 16-0032!!!";
    ers::error(PixLib::pix::daq (ERS_HERE, "LATENCY_CHECK_ERROR",ss0.str()));
  } else {
    std::stringstream ss0;
    ss0 << "You are not running in ATLAS, the reference latency is set to 0 by default";
    ers::warning(PixLib::pix::daq (ERS_HERE, "LATENCY_CHECK",ss0.str()));

  }

  uint32_t RODlatency=0;
  uint32_t pixReadoutWindowSize=0;
  uint32_t timDelay=0;
  uint32_t pixRefTimDelay = 122;

   //Loop over enabled RODs
   std::map<std::string, PixLib::RodCrateConnectivity*>::iterator ic;
   for (ic = m_rodCrates.begin(); ic != m_rodCrates.end(); ++ic) {
     PixLib::RodCrateConnectivity *c = ic->second;
	if(c->enableReadout && c->active() && !m_disable->isDisable(ic->first)) {
	  PixLib::TimConnectivity* t = c->tim();
	  std::string timTriggerDelay = t->name() + "/TimTriggerDelay";
	    if(m_is2->exists(timTriggerDelay.c_str())){
	       timDelay = m_is2->read<int>(timTriggerDelay.c_str());
	    } else {
	      msg = "Cannot get TIM delay for "+t->name() + " "+timTriggerDelay;
	      ers::error(PixLib::pix::daq(ERS_HERE, "LATENCY_CHECK_ERROR",msg));
	      }
	  std::map<int, PixLib::RodBocConnectivity*>::iterator ir;
	    for (ir=c->rodBocs().begin(); ir!=c->rodBocs().end(); ++ir) {
	      if (ic->second != NULL){
	        PixLib::RodBocConnectivity *r = ir->second;
	       if(r->enableReadout && r->active() && !m_disable->isDisable(r->name()) ){
	        std::string rodLatency = c->name() + "/" +r->name()+"/LATENCY";
	        std::string rodLVL1 = c->name() + "/" + r->name()+"/LVL1";
	          if(m_is2->exists(rodLatency.c_str())){
	             RODlatency = m_is2->read<int>(rodLatency.c_str());
	          } else { 
	            msg = "Cannot get ROD latency for " + r->name() + " "+rodLatency;
	            ers::error(PixLib::pix::daq (ERS_HERE, "LATENCY_CHECK_ERROR",msg));
	          }
	          if(m_is2->exists(rodLVL1.c_str())) {
	           pixReadoutWindowSize = m_is2->read<int>(rodLVL1.c_str());
	          } else{
	           msg = "Cannot get readout window for " + r->name()+ " "+rodLVL1;
	           ers::error(PixLib::pix::daq (ERS_HERE, "LATENCY_CHECK_ERROR",msg));
	          }
	        // In BC
	        uint32_t refDelay = pixRefTimDelay - atlasRefLatency;
      	        uint32_t pixWindowLowerBound = timDelay+255-RODlatency;// Latency: 150 blayer 255 others
      	        uint32_t pixWindowUpperBound =  pixWindowLowerBound + pixReadoutWindowSize;

      	        std::stringstream ss0;
      	        ss0<<"ATLAS latency: "<<atlasRefLatency <<" "<<t->name()<<" delay: "<< timDelay<<" "<<r->name()<<" has a latency of: "<<RODlatency<<" and readout window of: "<< pixReadoutWindowSize;
	        ers::info(PixLib::pix::daq (ERS_HERE, "LATENCY_CHECK_ERROR","LATENCY_SETTINGS",ss0.str()));
	       if( refDelay < pixWindowLowerBound || refDelay >= pixWindowUpperBound ) {
        	if(tc){
        	 msg = "Possibily wrong latency setting for "+ r->name() +"! Data quality possibly adversely affected! Check with Pixel On-Call 160032!";
	         ers::error(PixLib::pix::daq (ERS_HERE, "LATENCY_CHECK_ERROR",msg));
        	   if(r->name() == "ROD_I1_S21"){//Skip fatal for DBM
        	     msg = "Possibily wrong latency setting for "+ r->name() +" --> DBM Check with the corresponding expert";
	             ers::error(PixLib::pix::daq (ERS_HERE, "LATENCY_CHECK_ERROR",msg));
        	   } else{
        	   daq::rc::LatencyMismatch issueLatency(ERS_HERE, "PIX/IBL", "Possibily wrong latency setting for "+ r->name() +" Check with Pixel On-Call 160032!");
        	   ers::fatal(issueLatency);
        	   }
        	 } else{
        	 msg = "Possibily wrong latency setting for "+ r->name() +"!  but we are not in ATLAS, so who cares?";
	           ers::error(PixLib::pix::daq (ERS_HERE, "LATENCY_CHECK_ERROR",msg));
        	 }
      	       } else{
	          ers::info(PixLib::pix::daq (ERS_HERE, "LATENCY_OK",""));
               }
              }
	    }
	  }
	}
    }
  }
  catch (...) {
    //std::cout<<__PRETTY_FUNCTION__ <<std::endl;
    std::stringstream ss4;
    ss4 <<" Cannot check the latency. Something is wrong. Call the Pixel On-Call at 16-0032!!!";
    ers::error(PixLib::pix::daq (ERS_HERE, "LATENCY_CHECK_ERROR",ss4.str()));
  }

}


