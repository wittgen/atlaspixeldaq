/////////////////////////////////////////////////////////////////////
// PixRCDCoolAgent.cpp
// version 1.2
/////////////////////////////////////////////////////////////////////
//
// 31/01/06  Version 1.0 (CS)
//           Initial release
//
// 23/02/06  Version 1.1 (CS)
//           First multithreaded implementation
//
// 11/10/06  Version 1.2 (CS)
//           Implementation using PixBrokers and PixActions
//
// 5/7/07    Version 1.3 (GAO)
//           First BrokerMultiCrate usage
//           Uses PixRCDConfigHandler to manage general Config obj from xml
// 21/08/08  Version 2.0 (GG)
//           Major changes in order to use as COOL run condition agent
//! Pixel RCD CoolAgent class

#define TMP_KAROLOS_HIT_DISC_CNFG 1

// ONLINE SW includes
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <stdlib.h>
#include "ipc/partition.h"
#include "dal/util.h"
#include <sys/types.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <signal.h>
#include <dirent.h>
#include <errno.h>
#include <sys/stat.h>

// PIXRCD includes
#include "PixRCDModules/PixRCDCoolAgent.h"
#include "PixRCDConfiguration/PixRCDConfigHandler.h"

// VME INTERAFCE includes
#include "RCCVmeInterface.h"

// PIXLIB includes
#include "PixDbInterface/PixDbInterface.h"
#include "PixConnectivity/PixConnectivity.h"
#include "PixConnectivity/PartitionConnectivity.h"
#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixConnectivity/RodBocConnectivity.h"
#include "PixConnectivity/TimConnectivity.h"
#include "PixConnectivity/SbcConnectivity.h"
#include "PixConnectivity/OBConnectivity.h"
#include "PixConnectivity/Pp0Connectivity.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "PixBroker/PixBrokerMultiCrate.h"
#include "PixActions/PixActionsMulti.h"
#include "PixUtilities/PixISManager.h"
#include "PixUtilities/PixMessages.h"
#include "PixDbServer/PixDbServerInterface.h"
#include "PixConnectivity/PixDisable.h"
#include "rc/RunParams.h"

// COOL includes
#include <CoolKernel/IObjectIterator.h>
#include "CoolKernel/IObject.h"

// CORAL stuff
#include "CoralKernel/Context.h"
#include "CoralBase/Attribute.h"
#include "CoralBase/AttributeList.h"
#include "CoralBase/AttributeListException.h"
#include "RelationalAccess/IConnectionService.h"
#include "RelationalAccess/IConnectionServiceConfiguration.h"
#include "RelationalAccess/AccessMode.h"
#include "RelationalAccess/ISessionProxy.h"
#include "RelationalAccess/ISchema.h"
#include "RelationalAccess/ITransaction.h"
#include "RelationalAccess/AccessMode.h"
    
#include "RelationalAccess/ITable.h"
#include "RelationalAccess/ITableDescription.h"
#include "RelationalAccess/IColumn.h"
#include "RelationalAccess/ICursor.h"
#include "RelationalAccess/IQuery.h"
#include "RelationalAccess/TableDescription.h"
#include "RelationalAccess/ITableDataEditor.h"
#include "RelationalAccess/ITableSchemaEditor.h"
#include "RelationalAccess/IBulkOperation.h"
#include "RelationalAccess/SchemaException.h"

// From is2cool.cpp of GLM (for reference)

//***********************************************************************
// Custom ERS exceptions

namespace daq {
ERS_DECLARE_ISSUE(      rc,
                        BadCoolDB_Pixel,
                        "Archiving to COOL failed because " << explanation,
                        ((const char*) explanation)
                )

ERS_DECLARE_ISSUE(      rc,
                        BackupArchive_Pixel,
                        "Archiving to COOL is falling back to SQLite " << name,
                        ((const char*) name)
                )
}
//************************************************************************************
// Helper class to deal with DB connections

using namespace ROS;
using namespace std;

namespace daq {
  namespace rc {
    class CoolDB_Pixel {
    public:
      CoolDB_Pixel(std::string dbName, std::string dbBkpName, std::string dbSpeedName, std::string folder, PixRCDCoolAgent* rcd);
      ~CoolDB_Pixel();
      void close();
      cool::IDatabasePtr getValidDb(bool backupDb = false);
      cool::IDatabasePtr getValidDb_speed();
    private:
      cool::DatabaseId m_mainDbId;
      cool::DatabaseId m_bkpDbId;
      cool::DatabaseId m_speedDbId;
      cool::IDatabasePtr m_db;
      cool::IDatabasePtr m_bkpDb;
      cool::IDatabasePtr m_db_speed;
    };
  }
}

daq::rc::CoolDB_Pixel::CoolDB_Pixel(std::string dbName, std::string dbBkpName, std::string dbSpeedName, std::string folder, PixRCDCoolAgent* rcd): m_mainDbId(dbName), m_bkpDbId(dbBkpName), m_speedDbId(dbSpeedName) {

  // Foldernames
  rcd->m_SOR_Name = ("/" + folder + "/SOR_Params");
  // Create Folder Specifications
  rcd->m_SOR_Spec.extend("RunNumber",cool::StorageType::UInt32);
  rcd->m_SOR_Spec.extend("SORTime",cool::StorageType::UInt63);
  rcd->m_SOR_Spec.extend("RunType",cool::StorageType::String255);
  rcd->m_SOR_Spec.extend("connTag",cool::StorageType::String255);
  rcd->m_SOR_Spec.extend("cfgTag",cool::StorageType::String255);
  rcd->m_SOR_Spec.extend("cfgModTag",cool::StorageType::String255);
  rcd->m_SOR_Spec.extend("idTag",cool::StorageType::String255);
  rcd->m_SOR_Spec.extend("cfgRev",cool::StorageType::UInt63);
  rcd->m_SOR_Spec.extend("cfgModRev",cool::StorageType::UInt63);
  rcd->m_SOR_Spec.extend("disName",cool::StorageType::String255);
  rcd->m_SOR_Spec.extend("newDisName",cool::StorageType::String255);
  rcd->m_SOR_Spec.extend("disRev",cool::StorageType::UInt63);
  rcd->m_SOR_Spec.extend("runName",cool::StorageType::String255);
  rcd->m_SOR_Spec.extend("runRev",cool::StorageType::UInt63);

  // Open or Create the main DB
  try {
    m_db = cool::DatabaseSvcFactory::databaseService().openDatabase(m_mainDbId, false); // false = !read_only    
  }
  catch (cool::DatabaseDoesNotExist &e) {
    try {
      m_db = cool::DatabaseSvcFactory::databaseService().createDatabase(m_mainDbId);
    }
    catch(std::exception &ex) {
      daq::rc::BadCoolDB_Pixel i(ERS_HERE, ex.what());
      ers::error(i);
    }
  }
  catch (std::exception &ex) {
      daq::rc::BadCoolDB_Pixel i(ERS_HERE, ex.what());
      ers::error(i);    
  }
 
  // Open or Create the backup DB
  try {
    m_bkpDb = cool::DatabaseSvcFactory::databaseService().openDatabase(m_bkpDbId, false); // false = !read_only    
  }
  catch (cool::DatabaseDoesNotExist &e) {
    try {
      m_bkpDb = cool::DatabaseSvcFactory::databaseService().createDatabase(m_bkpDbId);
    }
    catch(std::exception &ex) {
      daq::rc::BadCoolDB_Pixel i(ERS_HERE, ex.what());
      ers::error(i);
    }
  }
  catch (std::exception &ex) {
      daq::rc::BadCoolDB_Pixel i(ERS_HERE, ex.what());
      ers::error(i);
  }

  // Create any missing folders on the main and backup DB (shouldn't be necessary)
  if (m_db) {
     if (!m_db->existsFolder(rcd->m_SOR_Name)) {
       std::string SOR_Desc("<timeStamp>run-lumi</timeStamp><addrHeader><address_header service_type=\"71\" clid=\"40774348\" /></addrHeader><typeName>AthenaAttributeList</typeName>");
#ifdef COOL_MIGRATION_FIXED
       // This code needs migration to COOL 3.x as we cannot use the legacy functions anymore
       cool::IFolderPtr SOR_Folder = m_db->createFolder(rcd->m_SOR_Name, rcd->m_SOR_Spec, SOR_Desc, cool::FolderVersioning::SINGLE_VERSION, true);
       ERS_DEBUG(3, "DB Folder SOR created.");
#else
       std::cout << "Skipping folder creation because migration to COOL 3.x not yet completed." << std::endl; // Fix me
#endif
     }
   }
 
   if (m_bkpDb && (m_bkpDb != m_db)) {
     if (!m_bkpDb->existsFolder(rcd->m_SOR_Name)) {
       std::string SOR_Desc("<timeStamp>run-lumi</timeStamp><addrHeader><address_header service_type=\"71\" clid=\"40774348\" /></addrHeader><typeName>AthenaAttributeList</typeName>");
#ifdef COOL_MIGRATION_FIXED
       // This code needs migration to COOL 3.x as we cannot use the legacy functions anymore
       cool::IFolderPtr SOR_Folder = m_bkpDb->createFolder(rcd->m_SOR_Name, rcd->m_SOR_Spec, SOR_Desc, cool::FolderVersioning::SINGLE_VERSION, true);
       ERS_DEBUG(3, "BCK DB Folder SOR created.");
#else
       std::cout << "Skipping folder creation because migration to COOL 3.x not yet completed." << std::endl; // Fix me
#endif
     }
   }

   
   //repeat for readout speed DB
   // Foldernames
   rcd->m_SOR_Name_speed = ("/" + folder + "/ReadoutSpeed");
   // Create Folder Specifications
   rcd->m_SOR_Spec_speed.extend("RunNumber",cool::StorageType::UInt32);
   rcd->m_SOR_Spec_speed.extend("readoutspeed_per_ROD",cool::StorageType::Blob64k);
   
#if TMP_KAROLOS_HIT_DISC_CNFG
   rcd->m_SOR_Name_HitDiscCnfg = ( "/" + folder + "/HitDiscCnfg" );
   rcd->m_SOR_Spec_HitDiscCnfg.extend("RunNumber", cool::StorageType::UInt32);
   rcd->m_SOR_Spec_HitDiscCnfg.extend("HitDiscCnfgData",cool::StorageType::Blob64k);
#endif

   // Open or Create the main DB
   try {
     m_db_speed = cool::DatabaseSvcFactory::databaseService().openDatabase(m_speedDbId, false); // false = !read_only    
   }
   catch (cool::DatabaseDoesNotExist &e) {
     try {
       m_db_speed = cool::DatabaseSvcFactory::databaseService().createDatabase(m_speedDbId);
     }
     catch(std::exception &ex) {
       daq::rc::BadCoolDB_Pixel i(ERS_HERE, ex.what());
       ers::error(i);
     }
   }
   catch (std::exception &ex) {
     daq::rc::BadCoolDB_Pixel i(ERS_HERE, ex.what());
     ers::error(i);    
   }
   
   // Create any missing folders on the speed DB (shouldn't be necessary)
   if (m_db_speed) {
     if (!m_db_speed->existsFolder(rcd->m_SOR_Name_speed)) {
       std::string SOR_Desc("<timeStamp>run-lumi</timeStamp><addrHeader><address_header service_type=\"71\" clid=\"40774348\" /></addrHeader><typeName>AthenaAttributeList</typeName>");
#ifdef COOL_MIGRATION_FIXED
       // This code needs migration to COOL 3.x as we cannot use the legacy functions anymore
       cool::IFolderPtr SOR_Folder = m_db_speed->createFolder(rcd->m_SOR_Name_speed, rcd->m_SOR_Spec_speed, SOR_Desc, cool::FolderVersioning::SINGLE_VERSION, true);
       ERS_DEBUG(3, "DB Folder for readout speed created.");
#else
       std::cout << "Skipping folder creation because migration to COOL 3.x not yet completed." << std::endl; // Fix me
#endif
     }
#if TMP_KAROLOS_HIT_DISC_CNFG //_CREATE_FOLDER
     std::cout << "Creating " << rcd->m_SOR_Name_HitDiscCnfg << " folder in backup db!" << std::endl;
     // HitDiscCnfg folder is in the same DB as ReadoutSpeed
     if (m_bkpDb && (m_bkpDb != m_db) && !m_bkpDb->existsFolder(rcd->m_SOR_Name_HitDiscCnfg)) {
       std::string SOR_Desc("<timeStamp>run-lumi</timeStamp><addrHeader><address_header service_type=\"71\" clid=\"40774348\" /></addrHeader><typeName>AthenaAttributeList</typeName>");
#ifdef COOL_MIGRATION_FIXED
       // This code needs migration to COOL 3.x as we cannot use the legacy functions anymore
       cool::IFolderPtr SOR_Folder = m_bkpDb->createFolder(rcd->m_SOR_Name_HitDiscCnfg, rcd->m_SOR_Spec_HitDiscCnfg, SOR_Desc, cool::FolderVersioning::SINGLE_VERSION, true);
#else
       std::cout << "Skipping folder creation because migration to COOL 3.x not yet completed." << std::endl; // Fix me
#endif
       ERS_DEBUG(3, "DB Folder for readout HitDiscCnfg created.");
     }
     /*
     if (!m_db_speed->existsFolder(rcd->m_SOR_Name_HitDiscCnfg)) {
       std::string SOR_Desc("<timeStamp>run-lumi</timeStamp><addrHeader><address_header service_type=\"71\" clid=\"40774348\" /></addrHeader><typeName>AthenaAttributeList</typeName>");
       cool::IFolderPtr SOR_Folder = m_db_speed->createFolder(rcd->m_SOR_Name_HitDiscCnfg, rcd->m_SOR_Spec_HitDiscCnfg, SOR_Desc, cool::FolderVersioning::SINGLE_VERSION, true);
       ERS_DEBUG(3, "DB Folder for readout HitDiscCnfg created.");
     }
     */
#endif
   }
}

daq::rc::CoolDB_Pixel::~CoolDB_Pixel() {
  this->close();
}

cool::IDatabasePtr daq::rc::CoolDB_Pixel::getValidDb(bool backupDb) { // getBackupDb defaults to false
  try {
    if( backupDb ) return (m_bkpDb = cool::DatabaseSvcFactory::databaseService().openDatabase(m_bkpDbId, false));
    m_db = cool::DatabaseSvcFactory::databaseService().openDatabase(m_mainDbId, false);
    return m_db;
  }
  catch(coral::Exception &e1) {
    daq::rc::BackupArchive_Pixel x(ERS_HERE, m_bkpDbId.c_str());
    ers::warning(x);
    try {
      m_bkpDb = cool::DatabaseSvcFactory::databaseService().openDatabase(m_bkpDbId, false);
      return m_bkpDb;
    }
   
    catch(coral::Exception &e2) {
      daq::rc::BadCoolDB_Pixel i(ERS_HERE, "Neither main nor backup COOL DB can be opened.");
      throw i;
    }
    catch(std::exception &e3) {    
      daq::rc::BadCoolDB_Pixel i(ERS_HERE, "Neither main nor backup COOL DB can be opened.");
      throw i;
    }
  }
  catch(std::exception &e4) {
    daq::rc::BackupArchive_Pixel x(ERS_HERE, m_bkpDbId.c_str());
    ers::warning(x);
    try {
      m_bkpDb = cool::DatabaseSvcFactory::databaseService().openDatabase(m_bkpDbId, false);
      return m_bkpDb;
    }
    catch(std::exception &e5) {    
      daq::rc::BadCoolDB_Pixel i(ERS_HERE, "Neither main nor backup COOL DB can be opened.");
      throw i;
    }
  }
}

cool::IDatabasePtr daq::rc::CoolDB_Pixel::getValidDb_speed() {
  try {
    m_db_speed = cool::DatabaseSvcFactory::databaseService().openDatabase(m_speedDbId, false);
    return m_db_speed;
  }
  catch(coral::Exception &e) {
    daq::rc::BadCoolDB_Pixel i(ERS_HERE, "Readout speed COOL DB can not be opened.");
    throw i;
  }
  catch(std::exception &e) {
    daq::rc::BadCoolDB_Pixel i(ERS_HERE, "Readout speed COOL DB can not be opened.");
    throw i;
  }
}

void daq::rc::CoolDB_Pixel::close() {
  if (m_db) m_db->closeDatabase();
  if (m_bkpDb) m_bkpDb->closeDatabase();
  if (m_db_speed) m_db_speed->closeDatabase();
}

PixRCDCoolAgent* pixrcd = 0;

void exitHandler(int signum) {
  std::cout << "Executing PixRCDCoolAgent exit handler" << std::endl;
  if (pixrcd != 0) pixrcd->cleanup();
}

PixRCDCoolAgent::PixRCDCoolAgent() {
  
  std::cout << "In PixRCDCoolAgent constructor!" << std::endl;
  m_conn = NULL;
}

PixRCDCoolAgent::~PixRCDCoolAgent() noexcept (true) {
  if( m_conn ) delete m_conn;
  pixrcd = 0;
}

void PixRCDCoolAgent::cleanup() {
  ers::info(PixLib::pix::daq (ERS_HERE,m_partName,"Executing PixRCDCoolAgent::cleanup()"));  
  if (m_disable!=0) {
    delete m_disable;
    m_disable = 0;
  }
  if (m_is!=0) {
    delete m_is;
    m_is = 0;
  }
  if (m_isDD!=0) {
    delete m_isDD;
    m_isDD = 0;
  }
  if (m_ipcPartition!=0) {
    delete m_ipcPartition;
    m_ipcPartition = 0;
  }
  if (m_ipcPartitionDD!=0) {
    delete m_ipcPartitionDD;
    m_ipcPartitionDD = 0;
  }
  if (m_dbHandler != 0){
    delete m_dbHandler;
    m_dbHandler = 0;
  }
}

void PixRCDCoolAgent::setup(DFCountedPointer<ROS::Config> configuration) {
  m_time=gettimeofday(&m_t0,NULL);
  
  std::cout<<"Called PixRCDCoolAgent::setup"<<std::endl;

  //pointers initialization  
  m_ipcPartition = 0;
  m_ipcPartitionDD = 0;
  m_is = 0;
  m_isDD = 0;
  m_dbServer = 0;
  m_dbHandler = 0;
  m_disable =0;
  pixrcd = this;
  m_pause = false;

  // CREATE IPC PARTITION
  std::string IPCPartitionName = "";
  IPCPartitionName = getenv("TDAQ_PARTITION_INFRASTRUCTURE");
  if (IPCPartitionName != "") {
    std::cout << IPCPartitionName << std::endl;
  } else {   
    std::cout << "PixRCDCoolAgent::setup: ERROR!! Could not read 'TDAQ_PARTITION_INFRASTRUCTURE' from XML files" << std::endl << std::endl;
  }

  std::cout << "Trying to connect to partition " << IPCPartitionName << " ... " << std::endl;
  try {
    m_ipcPartition = new IPCPartition(IPCPartitionName);
  } catch(...) {
    std::cout << "PixRCDCoolAgent::setup: ERROR!! Problems while connecting to the partition"+ IPCPartitionName << std::endl;
    exit(-1);
  }
  std::cout << "Succesfully connected to partition " << IPCPartitionName << std::endl << std::endl;

  std::string IPCPartitionNameDD = "";
  IPCPartitionNameDD = getenv("TDAQ_PARTITION");
  if (IPCPartitionNameDD == "") {
    std::cout << "PixRCDCoolAgent::setup: ERROR!! Could not read 'TDAQ_PARTITION' from XML files" << std::endl << std::endl;
    exit(-1);
  }
  std::cout << "Trying to connect to partition " << IPCPartitionNameDD << " ... " << std::endl;
  m_ipcPartitionDD=0;
  try {
    m_ipcPartitionDD = new IPCPartition(IPCPartitionNameDD);
  } catch(...) {
    std::cout << "PixRCDCoolAgent::setup: ERROR!! Problems while connecting to the partition"+ IPCPartitionNameDD << std::endl;
    //    exit(-1);
  }
  std::cout << "Succesfully connected to partition " << IPCPartitionNameDD << std::endl << std::endl;

  // Initialize IS server manager
  bool IsReady=true;
  m_isName = "PixelRunParams";
  try {
    m_is = new PixLib::PixISManager(m_ipcPartition, m_isName);
  } catch(...) {
    IsReady=false;
   ers::error(PixLib::pix::daq (ERS_HERE, "NO_IS_SERVER","Could not connect to IsServer "+m_isName));
  }
  if (IsReady) std::cout << "Succesfully created PixIsManager " << m_isName << std::endl << std::endl;  

  m_isNameDD = "RunParams";
  try {
    m_isDD = new PixLib::PixISManager(m_ipcPartitionDD, m_isNameDD);
  } catch(...) {
    IsReady=false;
    ers::error(PixLib::pix::daq (ERS_HERE, "NO_IS_SERVER","Could not connect to IsServer "+m_isNameDD));
  }
  if (IsReady) std::cout << "Succesfully created PixIsManager " << m_isNameDD << std::endl << std::endl;  
  
  m_isName = "RunParams";
  try {
    m_isRunParams = new PixLib::PixISManager(m_ipcPartition, m_isName);
  } catch(...) {
    IsReady=false;
    ers::error(PixLib::pix::daq (ERS_HERE, "NO_IS_SERVER","Could not connect to IsServer "+m_isName));
  }
  if (IsReady) std::cout << "Succesfully created PixIsManager " << m_isName << std::endl << std::endl;  

  //where do I get serverName from?! 
  std::string serverName = getenv("PIXDBSERVER_NAME");
  std::cout << "serverName " << serverName << std::endl;
  unsigned int nTry=0;
  bool ready=false;
  do {
    sleep(1);
    m_dbServer=new  PixLib::PixDbServerInterface(m_ipcPartition,serverName,PixLib::PixDbServerInterface::CLIENT, ready);
    if(!ready) delete m_dbServer;
    else break;
    //std::cout << " ..... attempt number: " << nTry << std::endl;
    nTry++;
  } while(nTry<10);  
  if(!ready) {
    std::cout << "Impossible to connect to DbServer " << serverName << std::endl;
    exit(-1);
  }
  else std::cout << "connected to DbServer " << serverName << std::endl;

  // GET CONFIGURATION from XML
  m_configuration = configuration; //Pointer to the ReadoutModule configuration
  bool mapIsGood = false;
  PixRCDConfigHandler* configHandler = new PixRCDConfigHandler(m_configuration);
  mapIsGood = configHandler->extractConversionMap(m_convMap);
  if(!mapIsGood) ers::warning(PixLib::pix::daq (ERS_HERE, "NO_XML_INFO","Cuold not information from XML"));
  // Here the substitution map is extracted from Configuration, in order to control substituttion of variables with their content. 

  getTags_CrateList(mapIsGood);
  //IS access for tags update
  //bool tagsFromIs = m_configuration->getBool("TagsFromIS");
  delete configHandler;
  
  std::cout << std::endl << "Tags before reading from IS:" << std::endl;
  std::cout << "- Connectivity Tag : " << m_connTag << std::endl;
  std::cout << "- Id tag: " << m_idTag << std::endl;
  std::cout << "- Configuration Tag: " << m_cfgTag << std::endl;
  std::cout << "- Module Configuration Tag: " << m_cfgModTag << std::endl << std::endl; 

  updateParams(m_connTag  , "DataTakingConfig-ConnTag");
  updateParams(m_cfgTag   , "DataTakingConfig-CfgTag");
  updateParams(m_cfgModTag, "DataTakingConfig-ModCfgTag");
  updateParams(m_idTag    , "DataTakingConfig-IdTag");
  updateParams(m_disName  , "DataTakingConfig-Disable",false);
  updateParams(m_runName  , "DataTakingConfig-RunConfig",false);
  updateParams(m_cfgRev   , "DataTakingConfig-CfgRev");
  updateParams(m_cfgModRev, "DataTakingConfig-CfgModRev");
  updateParams(m_disRev   , "DataTakingConfig-DisableRev");
  updateParams(m_runRev   , "DataTakingConfig-RunConfigRev");
  
  if (m_disName=="") {
    ers::info(PixLib::pix::daq (ERS_HERE, "PUT_DEF", "Using default name 'GlobalDis' for PixDisable object"));
    m_disName="GlobalDis";
  }	
  if (m_runName=="") {
    ers::info(PixLib::pix::daq (ERS_HERE, "PUT_DEF", "Using default name 'Global' for PixRunConfig object"));
    m_runName="Global";
  }
 
  ers::log(PixLib::pix::daq (ERS_HERE, "SUMMARY","Using tag: "+m_idTag+" / "+m_connTag+" / "+m_cfgTag+" / "+m_cfgModTag));
  std::ostringstream dump;
  dump << "PixDisable: "+m_disName+" ( / " << m_disRev << ")  /  PixRunConfig:"+m_runName+" ("<<m_runRev << ")";
  ers::log(PixLib::pix::daq (ERS_HERE, "SUMMARY",dump.str()));

  //create new PixConnectivity-object using tags
  if(m_conn) delete m_conn;
  m_conn= new PixLib::PixConnectivity(m_dbServer, m_connTag, m_connTag, m_connTag, m_cfgTag, m_idTag);
  m_conn->loadConn();

  // Get the DB
  m_coolDb = "PCOOLRP/CONDBR2";
  std::string coolDb_backup = "PCOOLRP_BAK/CONDBR2";
  m_folder = "PIXEL";
  //m_myMessages->publishMessage(PixLib::PixMessages::WARNING, "FOLDER_NAME", "Folder name is "+m_folder);
  //std::cout << "creating new pointer to DB" << std::endl;
  m_dbHandler = new daq::rc::CoolDB_Pixel(m_coolDb, coolDb_backup,m_coolDb, m_folder, this);
  
  std::cout << "Close the DBs; reopen everytime there is an entry to be inserted..." << std::endl;
  // Close the DBs; reopen everytime there is an entry to be inserted...
  m_dbHandler->close();
  ERS_DEBUG(3, "DB closed.");

  // Install the exit handler
  struct sigaction sa;
  sigemptyset(&sa.sa_mask); 
  sa.sa_flags = 0;
  sa.sa_handler = exitHandler; 
  int ret1=0, ret2=0;
  ret1 = sigaction(SIGTERM, &sa, NULL);
  ret2 = sigaction(SIGINT, &sa, NULL);
  if (ret1 != 0 || ret2 != 0) {
    ers::log(PixLib::pix::daq (ERS_HERE, "NO_EXTHDL",m_partName+": cannot install the exit handler")); 
    exit(-1);
  }
    
  m_time=gettimeofday(&m_t1,NULL);
  std::ostringstream text;
  text << m_partName << ": time to setup is:  " << m_t1.tv_sec - m_t0.tv_sec+ 1e-6*(m_t1.tv_usec-m_t0.tv_usec);
  ers::debug(PixLib::pix::daq (ERS_HERE,"TIME_INFO",text.str())); 

  std::cout<<"finished PixRCDCoolAgent::setup"<<std::endl;
}

void PixRCDCoolAgent::configure(const daq::rc::TransitionCmd&) {
}

void PixRCDCoolAgent::connect(const daq::rc::TransitionCmd&) {
}

void PixRCDCoolAgent::prepareForRun(const daq::rc::TransitionCmd&) {

  std::cout << "called PixRCDCOOLAgent::prepareForRun " << std::endl;
  
  // Read info from IS
  std::string value;
  RunParams sor;
  try {
    if (m_isDD != 0) {
      sor = m_isDD->read<RunParams>("RunParams");
      m_RunNo = sor.run_number;
      m_runTime = sor.timeSOR.total_mksec_utc() * 1000 ; // convert from microseconds into nanoseconds
      m_runType = sor.run_type;
      std::ostringstream text;
      text << "Run Number (" << m_RunNo << ") read from RunParams.RunParams";
      ers::info(PixLib::pix::daq (ERS_HERE,"RUN_NUMBER",text.str())); 
    } else {
      ers::error(PixLib::pix::daq (ERS_HERE,"RUN_NUMBER_ERROR","Impossible to read run number from PixelRunParams")); 
    }
  } catch(...) {
    ers::error(PixLib::pix::daq (ERS_HERE, "FAIL_IN_IS", "Problem in accessing IsServer RunParams to read RunParams"));
  }

  updateParams(m_connTag  , "DataTakingConfig-ConnTag");
  updateParams(m_cfgTag   , "DataTakingConfig-CfgTag");
  updateParams(m_cfgModTag, "DataTakingConfig-ModCfgTag");
  updateParams(m_idTag    , "DataTakingConfig-IdTag");
  updateParams(m_disName  , "DataTakingConfig-Disable",false);
  updateParams(m_newDisName  , "DataTakingConfig-WsDisable",false);
  updateParams(m_runName  , "DataTakingConfig-RunConfig",false);
  updateParams(m_cfgRev   , "DataTakingConfig-CfgRev");
  updateParams(m_cfgModRev   , "DataTakingConfig-CfgModRev");
  updateParams(m_disRev   , "DataTakingConfig-DisableRev");
  updateParams(m_runRev   , "DataTakingConfig-RunConfigRev");


  std::string tmpFilePath = "";
  tmpFilePath = getenv("CORAL_DBLOOKUP_PATH");

  //get readoutspeed from RunParams!
  //need connectivity info
 
  std::cout << "output in a temporary file the read-out speed" << std::endl;
  // output in a temporary file the run conditions
  std::ostringstream bufFileName_speed;
  bufFileName_speed << tmpFilePath.c_str() << "/.PixRCDCoolAgent/speed_" << m_RunNo << ".txt";
  std::ofstream *bf_speed = new std::ofstream(bufFileName_speed.str().c_str());

#if TMP_KAROLOS_PREVENT_REWRITE
  // This is a fix to avoid problems when preforming TTC restart
  // Check if file already present, eventually in a processed form (thus starting with a P)
  struct stat statBuffer;
  std::string filePath_speed = bufFileName_speed.str();
  int i = 0;
  if( stat( filePath_speed.c_str(), &statBuffer) == 0 ) { // File already exists
    // Lazy coding...
    std::ostringstream ss;
    ss << tmpFilePath.c_str() << "/.PixRCDCoolAgent/Pspeed_" << m_RunNo << ".txt"; // Prepend P so that it's not processed
    filePath_speed = ss.str();
    // Find which file does NOT exist
    while( stat ((filePath_speed + ".pass" + std::to_string(++i)).c_str(), &statBuffer) == 0 );
    filePath_speed += ".pass" + std::to_string(++i);
  }

  // Todo: validate the new file against the old, to check if some readout speed has been changed!!

  std::ofstream *bf_speed = new std::ofstream(filePath_speed.c_str());
#endif

  *bf_speed << m_RunNo << std::endl;

//#define GET_READOUT_SPEED_FROM_TEMPLATE_FILE

  //loop over all RODs
  //std::string RodName = "";
  std::map<std::string, PixLib::RodBocConnectivity *>::iterator rod;
  //if ((rod=conn->rods.find(RodName)) != m_conn->rods.end()) {
  
  std::cout << "m_conn->rods.size() " << m_conn->rods.size() << std::endl;
  for(rod=m_conn->rods.begin(); rod != m_conn->rods.end() ; rod++){
    std::string RodOfflineId = (rod->second->offlineId()).substr(2);
    std::string RodName = rod->first;
    std::cout << "RodOfflineId/Name " << RodOfflineId << " " << RodName << std::endl;
    std::string value_str="SINGLE_40";
    if(m_is->exists("DataTakingSpeed-"+RodName)) value_str = m_is->read<std::string>("DataTakingSpeed-"+RodName);
    else std::cout << "WARNING: couldn't find " << "DataTakingSpeed-"+RodName << " from IS" << std::endl;
    std::cout << "ReadOutSpeed " << value_str << std::endl;
#ifndef GET_READOUT_SPEED_FROM_TEMPLATE_FILE
    *bf_speed << RodOfflineId << "," << value_str << std::endl;
#endif
  }

#ifdef GET_READOUT_SPEED_FROM_TEMPLATE_FILE
  std::cout << "Using template file for readout speed" << std::endl;
  std::ifstream iF("/det/pix/db/.PixRCDCoolAgent/Prod_readout_speeds_160.txt");
  std::string line;
  while( iF.good() ) {
    iF >> line;
    if( iF.good() ) *bf_speed << line << std::endl;
  }
  iF.close();

  //std::string cmd = "cat /det/pix/db/.PixRCDCoolAgent/Prod_readout_speeds_160.txt >> ";
  //cmd += bufFileName_speed.str();
  //std::cout << cmd << std::endl;
  //system( cmd.c_str() );
#endif

  delete bf_speed;

  //end comment out readout-stuff

  std::cout << "output in a temporary file the run conditions" << std::endl;
  // output in a temporary file the run conditions
  //std::string tmpFilePath = "";
  //tmpFilePath = getenv("CORAL_DBLOOKUP_PATH");
  std::ostringstream bufFileName;
  bufFileName << tmpFilePath.c_str() << "/.PixRCDCoolAgent/" << m_RunNo << ".txt";
  std::cout << " bufFileName " << bufFileName.str().c_str() << std::endl;
  std::ofstream *bf = new std::ofstream(bufFileName.str().c_str());
  //m_myMessages->publishMessage(PixLib::PixMessages::INFORMATION, "TMP_FILE_INFO", "writing temporary run data in file ", bufFileName.str().c_str() );

  *bf << 
    m_RunNo << std::endl <<
    m_runType << std::endl <<
    m_runTime << std::endl << 
    m_connTag << std::endl <<
    m_cfgTag << std::endl << 
    m_cfgModTag << std::endl <<
    m_idTag << std::endl <<  
    m_cfgRev << std::endl <<
    m_cfgModRev << std::endl <<
    m_disName << std::endl <<
    m_newDisName << std::endl <<
    m_disRev << std::endl <<
    m_runName << std::endl <<
    m_runRev << std::endl;
  delete bf;

  std::cout << "output in a temporary file the HitDiscCnfg settings" << std::endl;
  std::string bufFileName_HitDiscCnfg = tmpFilePath + "/.PixRCDCoolAgent/hitDiscCnfgData_" + std::to_string(m_RunNo) + ".txt";
  std::ofstream oBF_HitDiscCnfg(bufFileName_HitDiscCnfg.c_str());
  ers::info(PixLib::pix::daq (ERS_HERE, "TMP_FILE_INFO","writing temporary HitDiscCnfg data in file "+std::string( bufFileName_HitDiscCnfg.c_str())));

  oBF_HitDiscCnfg << m_RunNo << std::endl;
  std::vector<std::string> hdcIsVars = m_isRunParams->listVariables("*HitDiscCnfgData");
  for(auto hdcVarName : hdcIsVars) {
    uint32_t hdcData = m_isRunParams->read<uint32_t>(hdcVarName);
    std::cout << hdcVarName << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << hdcData << std::dec << std::endl;
    oBF_HitDiscCnfg << "0x" << std::hex << std::setw(8) << std::setfill('0') << hdcData << std::dec << std::endl;
  }

  oBF_HitDiscCnfg.close();

}

void PixRCDCoolAgent::pause() {
}

void PixRCDCoolAgent::stopEB(const daq::rc::TransitionCmd&) {
}

void PixRCDCoolAgent::disconnect(const daq::rc::TransitionCmd&) {
}

void PixRCDCoolAgent::unconfigure(const daq::rc::TransitionCmd&) {
  cleanup();
}

DFCountedPointer<ROS::Config> PixRCDCoolAgent::getInfo() {
  // Get config object
  DFCountedPointer<ROS::Config> configuration = ROS::Config::New();
  return(configuration);
}

void PixRCDCoolAgent::probe() {

  //transfer data from temporary files into COOL DB, every five seconds

  std::cout << "In probe" << std::endl;
  std::string tmpFilePath = "";

  try {
    tmpFilePath = getenv("CORAL_DBLOOKUP_PATH");
    std::cout << "Look for temporary runData-files in " << tmpFilePath << std::endl;
    //m_myMessages->publishMessage(PixLib::PixMessages::INFORMATION, "TMP_FILE_INFO", "Look for temporary runData-files in " + tmpFilePath);
  }
  catch (...){
   ers::warning(PixLib::pix::daq (ERS_HERE, "BAD_DIR", "exception in getenv " + tmpFilePath));
    return;
  }
  //Open DB and retrieve folder
  cool::IDatabasePtr db;
  try{
    db= m_dbHandler->getValidDb();
    ERS_DEBUG(3, "DB opened");
  }
  catch (daq::rc::BadCoolDB_Pixel &e){
    ers::error(e);
    return;
  }

   //check data current RunNumber //tmp!
//   coral::AttributeList currentData(cool::Record(m_SOR_Spec).attributeList());
//   try {
//     cool::IFolderPtr SOR_Folder = db->getFolder(m_SOR_Name);
//     ERS_DEBUG(3, "SOR_Folder retrieved.");
//     cool::IObjectIteratorPtr objects = SOR_Folder->browseObjects( m_RunNo , m_RunNo+1 , 0 );
//     ERS_DEBUG(3, "browsing objects for this runNumber...");
//     m_myMessages->publishMessage(PixLib::PixMessages::WARNING, "INFO" , "Retrieving data for current runNumber from COOL " );
//     if( objects->size() == 0 ) m_myMessages->publishMessage(PixLib::PixMessages::WARNING, "INFO" , "No data for current runNumber in COOL" );
//     while( objects->goToNext() ){
//       //std::ostringstream data;
//       //data << objects->currentRef() ;
//       //m_myMessages->publishMessage(PixLib::PixMessages::WARNING, "INFO" , "Next data-object "+data.str() );
//       coral::AttributeList payloadC = objects->currentRef().payload().attributeList() ;
 
//       //retrieve data of latest runNumber
//       unsigned long c_RunNo;
//       std::string c_runType;
//       unsigned int c_runTime; 
//       std::string c_connTag;
//       std::string c_cfgTag; 
//       std::string c_cfgModTag;
//       std::string c_idTag;  
//       unsigned int c_cfgRev;
//       unsigned int c_cfgModRev;
//       std::string c_disName;
//       std::string c_newDisName;
//       unsigned int c_disRev;
//       std::string c_runName;
//       unsigned int c_runRev;

//       c_RunNo = payloadC["RunNumber"].data<cool::UInt32>();
//       c_runTime = payloadC["SORTime"].data<cool::UInt63>();
//       c_runType = payloadC["RunType"].data<cool::String255>();
//       c_connTag = payloadC["connTag"].data<cool::String255>() ;
//       c_cfgTag = payloadC["cfgTag"].data<cool::String255>(); 
//       c_cfgModTag = payloadC["cfgModTag"].data<cool::String255>();
//       c_idTag = payloadC["idTag"].data<cool::String255>();  
//       c_cfgRev = payloadC["cfgRev"].data<cool::UInt63>();
//       c_cfgModRev = payloadC["cfgModRev"].data<cool::UInt63>();
//       c_disName = payloadC["disName"].data<cool::String255>();
//       c_newDisName = payloadC["newDisName"].data<cool::String255>();
//       c_disRev = payloadC["disRev"].data<cool::UInt63>();
//       c_runName = payloadC["runName"].data<cool::String255>();
//       c_runRev = payloadC["runRev"].data<cool::UInt63>();
      
//       std::ostringstream data;
//       data << c_RunNo ;
//       m_myMessages->publishMessage(PixLib::PixMessages::WARNING, "INF)" , "runNumber "+data.str() );
//       m_myMessages->publishMessage(PixLib::PixMessages::WARNING, "INFO" , "runName "+c_runName );
//       m_myMessages->publishMessage(PixLib::PixMessages::WARNING, "INFO" , "runType "+c_runType );
//       m_myMessages->publishMessage(PixLib::PixMessages::WARNING, "INFO" , "idTag "+c_idTag );
//       m_myMessages->publishMessage(PixLib::PixMessages::WARNING, "INFO" , "connTag "+c_connTag );
//       m_myMessages->publishMessage(PixLib::PixMessages::WARNING, "INFO" , "cfgTag "+c_cfgTag );
//       m_myMessages->publishMessage(PixLib::PixMessages::WARNING, "INFO" , "cfgModTag "+c_cfgModTag );
//     }
//   }
//   catch (std::exception &e){
//     daq::rc::BadCoolDB_Pixel i(ERS_HERE, e.what());
//    ers::warning(i);
//  }
  //end of check data current run-number

  // m_myMessages->publishMessage(PixLib::PixMessages::WARNING, "BAD_DIR", "get path " + tmpFilePath);
  std::ostringstream dirName;
  dirName << tmpFilePath.c_str() << "/.PixRCDCoolAgent";
  //m_myMessages->publishMessage(PixLib::PixMessages::WARNING, "BAD_DIR", "get path " + dirName.str());
  //std::cout << dirName.str().c_str();

  //m_myMessages->publishMessage(PixLib::PixMessages::WARNING, "BAD_DIR", "start accessing directory ");
  DIR *dp;
  struct dirent *dirp;
  vector<std::string> files = vector<std::string>();

  if((dp  = opendir(dirName.str().c_str())) == NULL) {
    std::ostringstream error;
    error << "Unable to access directory " << dirName.str().c_str() << " no run conditions archived in COOL";
    ers::warning(PixLib::pix::daq (ERS_HERE, "BAD_DIR", error.str()));
    return;
  }
  // Todo: check only recent files (but how recent?)
  while ((dirp = readdir(dp)) != NULL) {
    std::string tmpString;
    tmpString = dirp->d_name;
    if(tmpString[0] != 'P' && tmpString.find(".txt") != std::string::npos) // Files with P are already processed!
      files.push_back(tmpString);
    //    std::cout << "read file" << tmpString << "from /daq/db/.PixRCDCoolAgent" << std::endl;
  }
  closedir(dp);

  // get the first entry with no "PP" (for processed) in front  
  int i = 0; 
  bool find = false;
  for(; i < (int)files.size(); i++){
    // Warning: doesn't use active logic! Todo: fix!!!
    if(files[i].c_str()[0] != 'P' && files[i].c_str()[0] != 's' && files[i].c_str()[0] != 'h' && files[i].c_str()[0] != 'C' && files[i].c_str()[0] != '.') {find = true; std::cout << i << std::endl; break;} 
  }
  if(find == true){
    ers::info(PixLib::pix::daq (ERS_HERE, "TMP_FILE_INFO", "Saving runData in "+files[i]+ " into COOL"));
    
    unsigned long t_RunNo;
    std::string t_runType;
    unsigned int t_runTime; 
    std::string t_connTag;
    std::string t_cfgTag; 
    std::string t_cfgModTag;
    std::string t_idTag;  
    unsigned int t_cfgRev;
    unsigned int t_cfgModRev;
    std::string t_disName;
    std::string t_newDisName;
    unsigned int t_disRev;
    std::string t_runName;
    unsigned int t_runRev;
    
    std::ostringstream bufFileName;
    bufFileName << dirName.str() << "/" << files[i].c_str();
    std::ifstream *bf = new std::ifstream(bufFileName.str().c_str());
    std::cout << "open to process file " << bufFileName.str().c_str() << std::endl;
    *bf >> 
      t_RunNo >>
      t_runType >> 
      t_runTime >>  
      t_connTag >> 
      t_cfgTag >>  
      t_cfgModTag >> 
      t_idTag >>   
      t_cfgRev >> 
      t_cfgModRev >> 
      t_disName >> 
      t_newDisName >> 
      t_disRev >> 
      t_runName >> 
      t_runRev;
    delete bf;
  
    // Index by run number and lumi block (lb=0)
    cool::UInt63 a = t_RunNo;
    //cool::UInt63 z = t_RunNo+1;
    cool::ValidityKey since = a <<32;
    cool::ValidityKey until = cool::ValidityKeyMax;
    
    //   cool::UInt32 a = t_RunNo;
    //   cool::UInt32 z = t_RunNo+1;
    //   cool::ValidityKey since = t_RunNo ; 
    //   cool::ValidityKey until = t_RunNo+1 ;
    
    // Fill in the data
    cool::Record coolRecord  = cool::Record(m_SOR_Spec);
    
    coolRecord["RunNumber"].setValue<cool::UInt32>( t_RunNo );
    coolRecord["SORTime"].setValue<cool::UInt63>( t_runTime );
    coolRecord["RunType"].setValue<cool::String255>( t_runType );
    coolRecord["connTag"].setValue<cool::String255>( t_connTag );
    coolRecord["cfgTag"].setValue<cool::String255>( t_cfgTag );
    coolRecord["cfgModTag"].setValue<cool::String255>( t_cfgModTag );
    coolRecord["idTag"].setValue<cool::String255>( t_idTag );
    coolRecord["cfgRev"].setValue<cool::UInt63>( t_cfgRev );
    coolRecord["cfgModRev"].setValue<cool::UInt63>( t_cfgModRev );
    coolRecord["disName"].setValue<cool::String255>( t_disName );
    coolRecord["newDisName"].setValue<cool::String255>( t_newDisName );
    coolRecord["disRev"].setValue<cool::UInt63>( t_disRev );
    coolRecord["runName"].setValue<cool::String255>( t_runName );
    coolRecord["runRev"].setValue<cool::UInt63>( t_runRev );

    ERS_DEBUG(3, "SOR info filled.");
    // Commit to DB
    
    try {
      cool::IFolderPtr SOR_Folder = db->getFolder(m_SOR_Name);
      ERS_DEBUG(3, "SOR_Folder retrieved.");
      SOR_Folder->storeObject(since, until, coolRecord, 0);
      ERS_DEBUG(3, "SOR folder stored.");
    
      // renaming the processed file in order to take out from the waiting list
      std::ostringstream bufFileReNameOld;
      std::ostringstream bufFileReNameNew;
      bufFileReNameOld << dirName.str() << "/" << files[i];
      bufFileReNameNew << dirName.str() << "/P" << files[i];
      rename(bufFileReNameOld.str().c_str(), bufFileReNameNew.str().c_str());
    }
    catch (std::exception &e){
      daq::rc::BadCoolDB_Pixel j(ERS_HERE, e.what());
      ers::warning(j);
    }
  }

  //repeat for speed-file
  // get the first entry with no "PP" (for processed) in front and starting with 's'  
  i = 0; 
  find = false;
  for(; i < (int)files.size(); i++){
    if(files[i].c_str()[0] != 'P' && files[i].c_str()[0] == 's' && files[i].c_str()[0] != '.') {find = true; std::cout << i << std::endl; break;} 
  }
  if(find == true){
    ers::info(PixLib::pix::daq (ERS_HERE, "TMP_FILE_INFO", "Saving runData in "+files[i]+ " into COOL"));
    
    std::ostringstream bufFileName;
    bufFileName << dirName.str() << "/" << files[i].c_str();
    std::ifstream *bf = new std::ifstream(bufFileName.str().c_str());
    
    unsigned long t_RunNo;
    *bf >> 
      t_RunNo;

    string s;
    string dataString;
    unsigned int line=0;
    while (*bf) {
      getline(*bf,s);
      if (bf->eof()) break;
      if(line>0) dataString += s + '\n';
      line++;
    }
    delete bf;
  
    // Index by run number and lumi block (lb=0)
    cool::UInt63 a = t_RunNo;
    //cool::UInt63 z = t_RunNo+1;
    cool::ValidityKey since = a <<32;
    cool::ValidityKey until = cool::ValidityKeyMax;
    
    // Fill in the data
    cool::Record coolRecord = cool::Record(m_SOR_Spec_speed);
    coolRecord["RunNumber"].setValue<cool::UInt32>( t_RunNo );
    coral::Blob blob;
    unsigned int len = dataString.size();
    blob.resize(len);
    char* p = static_cast<char*>(blob.startingAddress());
    for (unsigned int k = 0; k!=len; ++k) *p++ = dataString[k];    
    coolRecord["readoutspeed_per_ROD"].setValue<coral::Blob>(blob);

    ERS_DEBUG(3, "readoutspeed_per_ROD info filled.");
    // Commit to DB
    
    try {
      cool::IFolderPtr SOR_Folder_speed = db->getFolder(m_SOR_Name_speed);
      ERS_DEBUG(3, "SOR_Folder_speed retrieved.");
      SOR_Folder_speed->storeObject(since, until, coolRecord, 0);
      ERS_DEBUG(3, "SOR folder_speed stored.");
    
      //rename file to remove it from waiting list
      std::ostringstream bufFileReNameOld;
      std::ostringstream bufFileReNameNew;
      bufFileReNameOld << dirName.str() << "/" << files[i];
      bufFileReNameNew << dirName.str() << "/P" << files[i];
      rename(bufFileReNameOld.str().c_str(), bufFileReNameNew.str().c_str());
    }
    catch (std::exception &e){
      daq::rc::BadCoolDB_Pixel j(ERS_HERE, e.what());
      ers::warning(j);
    }    
  }    

#if TMP_KAROLOS_HIT_DISC_CNFG
  // Adding HitDiscCnfg information
  // get the first entry with no "PP" (for processed) in front and starting with 'h'  
  i = 0; 
  find = false;
  for(; i < (int)files.size(); i++){
    // Skipping files startin with P for faster processing
    if(files[i].c_str()[0] != 'P' && files[i].c_str()[0] == 'h') {find = true; std::cout << i << std::endl; break;} 
  }
  if(find == true){
    ers::info(PixLib::pix::daq (ERS_HERE, "TMP_FILE_INFO", "Saving runData in "+files[i]+ " into COOL"));
    
    std::ostringstream bufFileName;
    bufFileName << dirName.str() << "/" << files[i].c_str();
    std::ifstream iBF(bufFileName.str().c_str());
    
    unsigned long t_RunNo;
    iBF >> t_RunNo;

    vector<uint32_t> dataHitDiscCnfg;
    while( !iBF.eof() ) {
      uint32_t hdcData;
      iBF >> std::hex >> hdcData;
      if( dataHitDiscCnfg.size() == 0 || dataHitDiscCnfg.back() != hdcData )
        dataHitDiscCnfg.push_back(hdcData);
    }

    iBF.close();

    // Index by run number and lumi block (lb=0)
    cool::UInt63 a = t_RunNo;
    //cool::UInt63 z = t_RunNo+1;
    cool::ValidityKey since = a <<32;
    cool::ValidityKey until = cool::ValidityKeyMax;
    // Format is as follows 0xFLDDRRRR. e.g. 0x71140061
    cool::Record coolRecord = cool::Record(m_SOR_Spec_HitDiscCnfg);
    coolRecord["RunNumber"].setValue<cool::UInt32>( t_RunNo );
    coral::Blob blob;
    std::size_t len = dataHitDiscCnfg.size();
    blob.resize(len*sizeof(uint32_t));
    uint32_t* p = static_cast<uint32_t*>(blob.startingAddress());
    for (unsigned int k = 0; k!=len; ++k) *p++ = dataHitDiscCnfg[k];    
    coolRecord["HitDiscCnfgData"].setValue<coral::Blob>(blob);

    ERS_DEBUG(3, "HitDiscCnfgData info filled.");
    // Commit to DB
    
    try {
#ifdef TMP_KAROLOS_HIT_DISC_CNFG_USE_BACKUP_DB
      cool::IDatabasePtr dbBackup;
      try{
        dbBackup= m_dbHandler->getValidDb(true);
        ERS_DEBUG(3, "DB opened");
      }
      catch (daq::rc::BadCoolDB_Pixel &e){
        ers::error(e);
        return;
      }
#endif

#ifdef TMP_KAROLOS_HIT_DISC_CNFG_USE_BACKUP_DB
      cool::IFolderPtr SOR_Folder_HitDiscCnfg = dbBackup->getFolder(m_SOR_Name_HitDiscCnfg);
#else
      cool::IFolderPtr SOR_Folder_HitDiscCnfg = db->getFolder(m_SOR_Name_HitDiscCnfg);
#endif
      ERS_DEBUG(3, "SOR_Folder_HitDiscCnfg retrieved.");
      SOR_Folder_HitDiscCnfg->storeObject(since, until, coolRecord, 0);
      ERS_DEBUG(3, "SOR folder_HitDiscCnfg stored.");
    
      //rename file to remove it from waiting list
      std::ostringstream bufFileReNameOld;
      std::ostringstream bufFileReNameNew;
      bufFileReNameOld << dirName.str() << "/" << files[i];
      bufFileReNameNew << dirName.str() << "/P" << files[i];
      rename(bufFileReNameOld.str().c_str(), bufFileReNameNew.str().c_str());
    }
    catch (std::exception &e){
      daq::rc::BadCoolDB_Pixel j(ERS_HERE, e.what());
      ers::warning(j);
    }    
  }
#endif

    
  m_dbHandler->close();

  return;  
}

void PixRCDCoolAgent::publishFullStatistics() {
}

void PixRCDCoolAgent::clearInfo() {
}

// CODE NEEDED BY THE PLUGIN FACTORY
extern "C"
{
  extern ReadoutModule* createPixRCDCoolAgent();
}
ReadoutModule* createPixRCDCoolAgent()
{
  return (new PixRCDCoolAgent());
}

void PixRCDCoolAgent::updateParams(std::string& paramValue, std::string paramName, bool publish) {
  // Reads if paramName is published in IS. If yes, it overwrites the local paramValue;
  // if not, it publishes local paramValue.
  std::string value;
  try {
    if (m_is!=0) value = m_is->read<std::string>(paramName);
  } catch(...) {
    if( paramName != "DataTakingConfig-CfgRev" && paramName != "DataTakingConfig-CfgModRev" && paramName != "DataTakingConfig-DisableRev" && paramName != "DataTakingConfig-RunConfigRev" ) ers::warning(PixLib::pix::daq (ERS_HERE, "FAIL_IN_IS","Problem in reading parameter <"+paramName+"> from IsServer"));
    else ers::info(PixLib::pix::daq (ERS_HERE, "FAIL_IN_IS", "Problem in reading parameter <"+paramName+"> from IsServer"));
  }
  if (value == "")  {
    if (publish) {
      ers::info(PixLib::pix::daq (ERS_HERE, "NO_TAG_IN_IS","Parameter <"+paramName+"> was not found in IS. Updating IS with value: "+paramValue));
      if (m_is!=0) m_is->publish(paramName, paramValue);
    }
  } else {
    paramValue = value;
    ers::info(PixLib::pix::daq (ERS_HERE, "IS_UPDATE", "Parameter <"+paramName+"> found in IS with value: "+paramValue));
  }
}

void PixRCDCoolAgent::updateParams(unsigned int& paramValue, std::string paramName) {
  // Reads if paramName is published in IS.
  unsigned int value=0;
  try {
    if (m_is!=0) value = m_is->read<unsigned int>(paramName);
  } catch(...) {
    if( paramName != "DataTakingConfig-CfgRev" && paramName != "DataTakingConfig-CfgModRev" && paramName != "DataTakingConfig-DisableRev" && paramName != "DataTakingConfig-RunConfigRev" ) ers::warning(PixLib::pix::daq (ERS_HERE, "FAIL_IN_IS","Problem in reading parameter <"+paramName+"> from IsServer"));
    else ers::info(PixLib::pix::daq (ERS_HERE, "FAIL_IN_IS", "Problem in reading parameter <"+paramName+"> from IsServer"));
  }
  paramValue = value;
}

void PixRCDCoolAgent::readFromSchema() {
  std::cout<<"PixRCDCoolAgent::setup: taking default Connectivity tags directly from attr name definition in Crates xml."<<std::endl;
  m_connTag   = m_configuration->getString("ConnectivityTagC");
  m_cfgTag    = m_configuration->getString("ConnectivityTagCF");
  m_cfgModTag = m_configuration->getString("ConnectivityTagModCF");
  m_idTag = m_configuration->getString("ConnectivityTagID");
}

void PixRCDCoolAgent::getTags_CrateList(bool mapIsGood) {
  bool error = false;  
  if(mapIsGood) {
    std::map<std::string,std::string>::iterator mapit;
    mapit = (m_convMap).find("CONNECTIVITY_RUN_TAG_C");
    if (mapit!=(m_convMap).end()) m_connTag = (*mapit).second;
    else {
     ers::warning(PixLib::pix::daq (ERS_HERE, "NO_PAR_IN_XML","Cannot find 'CONNECTIVITY_RUN_TAG_C' var into the conversion map."));
      error = true;
    }
    mapit = (m_convMap).find("CONNECTIVITY_RUN_TAG_CF");
    if (mapit!=(m_convMap).end()) m_cfgTag = (*mapit).second;
    else {
      ers::warning(PixLib::pix::daq (ERS_HERE, "NO_PAR_IN_XML","Cannot find 'CONNECTIVITY_RUN_TAG_CF' var into the conversion map."));
      error = true;
    }
    mapit = (m_convMap).find("CONNECTIVITY_RUN_TAG_MOD_CF");
    if (mapit!=(m_convMap).end()) m_cfgModTag = (*mapit).second;
    else {
      ers::warning(PixLib::pix::daq (ERS_HERE,  "NO_PAR_IN_XML","Cannot find 'CONNECTIVITY_RUN_TAG_MOD_CF' var into the conversion map."));
      error = true;
    }
    mapit = (m_convMap).find("ID_TAG");
    if (mapit!=(m_convMap).end()) m_idTag = (*mapit).second;
    else {
      ers::warning(PixLib::pix::daq (ERS_HERE, "NO_PAR_IN_XML","Cannot find 'ID_TAG' var into the conversion map."));
      error = true;
    }
  }
  else
    readFromSchema();
  if (error)
    readFromSchema();
}




