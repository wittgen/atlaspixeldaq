/*-----------------------------------------------------------------------------------
Implementation of PixRCDModuleConfig configuration plugin
Author: C. Schiavi
Initial implementation date: 31/1/2006
-------------------------------------------------------------------------------------*/

#ifndef PIXRCDMODULECONFIG_H
#define PIXRCDMODULECONFIG_H

/**
 * The PixRCDModuleConfig class inherits from the RCDConfig class
 * It is used as a plugin to the readout application
 * to manage the configuration of the PixRCDModule readout module.
 */

#include "ROSApplication/RCDConfig.h"

class PixRCDModuleConfig : public ROS::RCDConfig {
 public:

  PixRCDModuleConfig (char * name);

  virtual ~PixRCDModuleConfig () {};

  /**
   * Load the type and configuration of the RCD input modules.
   * The configuration parameters are stored in ModuleConfig objects.
   */

  virtual void getModule(const daq::df::ReadoutModule* rom,
                         DFCountedPointer<ROS::Config> ModuleConfig);

};
#endif // PIXRCDMODULECONFIG_H
