/*-----------------------------------------------------------------------------------
Implementation of PixRCDCoolAgentConfig configuration plugin
Author: C. Schiavi
Initial implementation date: 31/1/2006
-------------------------------------------------------------------------------------*/

#ifndef PIXRCDCOOLAGENTCONFIG_H
#define PIXRCDCOOLAGENTCONFIG_H

/**
 * The PixRCDCoolAgentConfig class inherits from the RCDConfig class
 * It is used as a plugin to the readout application
 * to manage the configuration of the PixRCDModule readout module.
 */

#include "ROSApplication/RCDConfig.h"

class PixRCDCoolAgentConfig : public ROS::RCDConfig {
 public:

  PixRCDCoolAgentConfig (char * name);

  virtual ~PixRCDCoolAgentConfig () {};

  /**
   * Load the type and configuration of the RCD input modules.
   * The configuration parameters are stored in ModuleConfig objects.
   */

  virtual void getModule(const daq::df::ReadoutModule* rom,
                         DFCountedPointer<ROS::Config> ModuleConfig);

};
#endif // PIXRCDCOOLAGENTCONFIG_H
