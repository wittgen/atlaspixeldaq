/*-----------------------------------------------------------------------------------
Implementation of PixRCDConfigHandler configuration controller

-) Access to general configuration from xml db;
-) Access to conversion map of parameters defined into xml files;

Author: Gian Andrea Odino
Initial implementation date: 31/7/2007
-------------------------------------------------------------------------------------*/

#ifndef PIXRCDCONFIGHANDLER_H
#define PIXRCDCONFIGHANDLER_H

#include "ROSInterruptScheduler/InterruptHandler.h"
#include "ROSCore/ScheduledUserAction.h"
#include "ROSCore/SequentialDataChannel.h"
#include "ROSCore/ReadoutModule.h"

/*
 * The PixRCDConfigHandler class is used in order to handle an access to
 * global configuration object.
 * 1) Substitution map is computed and used;
 * 2) Robin map is computed starting from PixConnectivity and robins are
 *     enabled and disabled accordingly. TO BE DONE
*/

namespace ROS {
  
  class PixRCDConfigHandler {
  public:
    PixRCDConfigHandler(Configuration * conf);      //Standard constructor: it takes general Configuration object
    PixRCDConfigHandler(DFCountedPointer<ROS::Config> configuration);   //Constructor: it retrieves general Configuration obj from ROS::Config obj
    ~PixRCDConfigHandler();
    
    bool extractConversionMap(std::map<std::string, std::string>& outMap);
    
    // accessor to global configuration
    Configuration* getGlobalDb();   
    
  private:
    // Define class objs
    void setConfig();
    
    // DAL configuration
    Configuration* m_conf;

    // ROS::Config configuration
    DFCountedPointer<ROS::Config> m_configuration;

    // Partition pointer
    daq::core::Partition* m_partition;

    //SubstituteVariables obj (used to retrieve substitution map)
    daq::core::SubstituteVariables * substVariables;

    // To be decided/done: define setROBIN channels map
    // Robin map
    // std::map<int, >
  };
}
#endif // PIXRCDCONFIGHANDLER_H
