/*-----------------------------------------------------------------------------------
Implementation of PixRCDCoolAgentCalibConfig configuration plugin
Author: C. Schiavi
Initial implementation date: 31/1/2006
-------------------------------------------------------------------------------------*/

#ifndef PIXRCDCOOLAGENTCALIBCONFIG_H
#define PIXRCDCOOLAGENTCALIBCONFIG_H

/**
 * The PixRCDCoolAgentCalibConfig class inherits from the RCDConfig class
 * It is used as a plugin to the readout application
 * to manage the configuration of the PixRCDModule readout module.
 */

#include "ROSApplication/RCDConfig.h"

class PixRCDCoolAgentCalibConfig : public ROS::RCDConfig {
 public:

  PixRCDCoolAgentCalibConfig (char * name);

  virtual ~PixRCDCoolAgentCalibConfig () {};

  /**
   * Load the type and configuration of the RCD input modules.
   * The configuration parameters are stored in ModuleConfig objects.
   */

  virtual void getModule(const daq::df::ReadoutModule* rom,
                         DFCountedPointer<ROS::Config> ModuleConfig);

};
#endif // PIXRCDCOOLAGENTCALIBCONFIG_H
