/*-----------------------------------------------------------------------------------
Implementation of PixRCDCoolAgentConfig configuration plugin
Author: C. Schiavi
Initial implementation date: 31/1/2006
-------------------------------------------------------------------------------------*/

#include "PixRCDConfiguration/PixRCDCoolAgentConfig.h"
#include "PixRCDCoolAgentdal/PixRCDCoolAgent.h"
#include "DFDebug/DFDebug.h"

PixRCDCoolAgentConfig::PixRCDCoolAgentConfig (char * name) : ROS::RCDConfig (name) {}

void PixRCDCoolAgentConfig::getModule (const daq::df::ReadoutModule *rom, 
                                  DFCountedPointer<ROS::Config> moduleConfig) 
{
  // This method reads in the attributes and relationships of a module (a ReadoutModule)
  // in the configuration database
  // and makes them available in a "Config" object (moduleConfig).
  // This object contains pairs of information: strings and the values associated to it.

  if (const PixRCDCoolAgentdal::PixRCDCoolAgent* eFm = 
      m_confDB->cast<PixRCDCoolAgentdal::PixRCDCoolAgent,
      daq::df::ReadoutModule>(rom)) {
    
    // Get the PixRCDCoolAgent parameters from the configuration:
    
    //moduleConfig->set("PixConnectivityDb", eFm->PixConnectivityDb());
    moduleConfig->set("CrateName", eFm->get_CrateName());
    moduleConfig->set("ConnectivityTagC", eFm->get_ConnectivityTagC());
    moduleConfig->set("ConnectivityTagP", eFm->get_ConnectivityTagP());
    moduleConfig->set("ConnectivityTagA", eFm->get_ConnectivityTagA());
    moduleConfig->set("ConnectivityTagCF", eFm->get_ConnectivityTagCF());
    moduleConfig->set("AllocateActions", eFm->get_AllocateActions());
  }

  // In case of another type of module: search in the RCDConfig modules, 
  // and later on in the ROSDBConfig ones. 
  else {
      RCDConfig::getModule(rom, moduleConfig);
  }
}

/** Shared library entry point */
// No need to change this part
extern "C" {
  extern ROS::IOManagerConfig* createPixRCDCoolAgentConfig(void* name);
}

ROS::IOManagerConfig* createPixRCDCoolAgentConfig(void* name) {
  return (new PixRCDCoolAgentConfig((char*) name));
}
