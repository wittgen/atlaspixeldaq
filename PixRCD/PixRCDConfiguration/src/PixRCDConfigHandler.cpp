/*-----------------------------------------------------------------------------------
Implementation of PixRCDConfigHandler configuration controller
Author: Gian Andrea Odino
Initial implementation date: 31/7/2007
-------------------------------------------------------------------------------------*/

// ONLINE SW includes
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <stdlib.h>
#include "ipc/partition.h"
#include "dal/util.h"
#include "PixRCDConfiguration/PixRCDConfigHandler.h"

using namespace ROS;

PixRCDConfigHandler::PixRCDConfigHandler(DFCountedPointer<ROS::Config> configuration) : 
m_configuration(configuration) {

  m_conf = 0;
  m_partition = 0;
  
  try {
    m_conf = m_configuration->getPointer<Configuration>("configurationDB");
  }
  catch(...) {
    std::cout<<"Unknown error in PixRCDConfigHandler::setup. Failed to retrieve essential DB information. Cannot retreive substitution map."<<std::endl ;
  }
  setConfig();
  substVariables = 0;
}

PixRCDConfigHandler::PixRCDConfigHandler(Configuration * conf) : 
m_conf(conf) {

  m_partition = 0;
  setConfig();
  substVariables = 0;
}

PixRCDConfigHandler::~PixRCDConfigHandler() {
  if(substVariables != 0)
    delete substVariables;
}

void PixRCDConfigHandler::setConfig() {

  if(m_conf->loaded()) {
    //std::cout<<"Loading the Config Database from PixRCDConfigHandler::setConfig"<<std::endl;
    
    //get_partition method searches partition object by given name. If name is empty, it takes the name from TDAQ_PARTITION env.
    std::cout<<"ConfigHandler: tdaq partition is called"<<getenv("TDAQ_PARTITION")<<std::endl;
    m_partition = (daq::core::Partition*) daq::core::get_partition(*m_conf, (getenv("TDAQ_PARTITION")));
    
    if(!m_partition){
      std::cout<< "PixRCDConfigHandler::setConfig(), Partition does not exist, cannot retreive substitution map."<<std::endl ;
    }
  }
  else std::cout<< "ERROR in PixRCDConfigHandler::setConfig(), Database not loaded!"<<std::endl ;
}


bool PixRCDConfigHandler::extractConversionMap(std::map<std::string, std::string>& outMap) {
  
  bool mapIsGood=false;
  
  if(m_conf->loaded() && m_partition != 0) {
    substVariables = new daq::core::SubstituteVariables(/* *m_conf,*/*m_partition);
    outMap = (std::map<std::string, std::string>&) *(substVariables->get_conversion_map());
    mapIsGood = true;  
    
    std::map<std::string,std::string>::iterator mapit;
    std::cout<<"Conversion map dump:"<<std::endl;
    for(mapit = outMap.begin(); mapit != outMap.end(); mapit++) 
      std::cout<<mapit->first<<" - "<<mapit->second<<std::endl;
  }
  return mapIsGood;
}

// accessor to global configuration
Configuration* PixRCDConfigHandler::getGlobalDb() {
  return m_conf;
}   

/*
//ROBIN map handler



*/
