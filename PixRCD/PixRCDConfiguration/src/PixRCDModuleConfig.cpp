/*-----------------------------------------------------------------------------------
Implementation of PixRCDModuleConfig configuration plugin
Author: C. Schiavi
Initial implementation date: 31/1/2006
-------------------------------------------------------------------------------------*/

#include "PixRCDConfiguration/PixRCDModuleConfig.h"
#include "PixRCDModuledal/PixRCDModule.h"
#include "DFDebug/DFDebug.h"

PixRCDModuleConfig::PixRCDModuleConfig (char * name) : ROS::RCDConfig (name) {}

void PixRCDModuleConfig::getModule (const daq::df::ReadoutModule *rom, 
                                  DFCountedPointer<ROS::Config> moduleConfig) 
{
  // This method reads in the attributes and relationships of a module (a ReadoutModule)
  // in the configuration database
  // and makes them available in a "Config" object (moduleConfig).
  // This object contains pairs of information: strings and the values associated to it.

  if (const PixRCDModuledal::PixRCDModule* eFm = 
      m_confDB->cast<PixRCDModuledal::PixRCDModule,
      daq::df::ReadoutModule>(rom)) {
    
    // Get the PixRCDModule parameters from the configuration:
    
    //moduleConfig->set("PixConnectivityDb", eFm->PixConnectivityDb());
    moduleConfig->set("CrateName", eFm->get_CrateName());
    moduleConfig->set("ConnectivityTagC", eFm->get_ConnectivityTagC());
    moduleConfig->set("ConnectivityTagP", eFm->get_ConnectivityTagP());
    moduleConfig->set("ConnectivityTagA", eFm->get_ConnectivityTagA());
    moduleConfig->set("ConnectivityTagCF", eFm->get_ConnectivityTagCF());
    moduleConfig->set("AllocateActions", eFm->get_AllocateActions());
  }

  // In case of another type of module: search in the RCDConfig modules, 
  // and later on in the ROSDBConfig ones. 
  else {
      RCDConfig::getModule(rom, moduleConfig);
  }
}

/** Shared library entry point */
// No need to change this part
extern "C" {
  extern ROS::IOManagerConfig* createPixRCDModuleConfig(void* name);
}

ROS::IOManagerConfig* createPixRCDModuleConfig(void* name) {
  return (new PixRCDModuleConfig((char*) name));
}
