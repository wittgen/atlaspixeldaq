# Author: K. Potamianos <karolos.potamianos@cern.ch>

cmake_minimum_required(VERSION 3.6.0)
set(TDAQ_INST_PATH $ENV{TDAQ_INST_PATH})
find_package(TDAQ-PIX)
#find_package(TDAQ)
include(CTest)

#set(TDAQ_DB_PATH /atlas/oks/tdaq-07-01-00)
#set(ENV{PIXRCD_FOLDER} /atlas/oks/tdaq-07-01-00/pixel)

set(PACKAGE PixRCDConfiguration)
tdaq_package(${PACKAGE} 1.0.0 USES tdaq 7.1.0)

foreach(dal PixGnamLibrary PixRCDCoolAgent PixRCDCoolAgentCalib PixRCDModule)
  tdaq_generate_dal(TARGET ${dal}dal
    $ENV{PIXRCD_FOLDER}/schema/${dal}.schema.xml
    NAMESPACE ${dal}dal
    PACKAGE ${PACKAGE}
    INCLUDE_DIRECTORIES dal gnam DFConfiguration
    #INCLUDE ${dal}dal
    CPP_OUTPUT dal_cpp_srcs_${dal}dal)
  #tdaq_add_library(pixgnamlibrarydal DAL ${dal_cpp_srcs_PixGnamLibrarydal} LINK_LIBRARIES Boost)
  string(TOLOWER ${dal} dal_lc)
  tdaq_add_library(${dal_lc}dal DAL ${dal_cpp_srcs_PixGnamLibrarydal})
  tdaq_add_library(${dal_lc}obj STATIC ${dal_cpp_srcs_PixGnamLibrarydal}
    INCLUDE_DIRECTORIES $ENV{BOOSTINC}
    )
  target_include_directories(${dal_lc}obj PUBLIC $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>)

  add_dependencies(${dal_lc}dal ${dal}dal)
  #tdaq_add_library(${dal}Config src/${dal}Config.cpp)
endforeach(dal)


#Don't build PixRCDCoolAgentCalib PixRCDCoolAgent PixRCDModule
foreach(pkg PixRCDConfigHandler)
  tdaq_add_library(${pkg} src/${pkg}.cpp ${PACKAGE}/PixRCDConfigHandler.h
    LINK_LIBRARIES Boost
    )
  set_target_properties(${pkg} PROPERTIES PUBLIC_HEADER ${PACKAGE}/PixRCDConfigHandler.h)
  install(TARGETS ${pkg}
    LIBRARY DESTINATION lib
    PUBLIC_HEADER DESTINATION include/${PACKAGE}
    )
endforeach(pkg)

