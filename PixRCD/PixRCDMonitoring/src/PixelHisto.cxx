#ifndef _GNAM_PATCH_H_
#define _GNAM_PATCH_H_

#ifndef _GNAM_NEW_API_

///////////////////////////////////////////////////////////////////////////////
//// HACK to bypass undefined symbol in coral:
#include "CoralBase/AttributeList.h"
#include "CoralBase/AttributeListSpecification.h"

namespace coral {
   const AttributeListSpecification& AttributeList::specification() const
   {
      return *m_specification;
   }
}
///////////////////////////////////////////////////////////////////////////////

namespace tGnamHisto {
  // bit masks for GnamHisto c'tors
  const int NEVER_RESET(0x00);
  const int RESET_AT_WASTART(0x10);
  const int RESET_AT_WASTOP(0x20);
  const int RESET_AT_WASS(0x30);

  const int FILL_ANYTIME(0x00);
  const int FILL_WHEN_READY(0x40);
  const int FILL_WHEN_STANDBY(0x80);
  const int NEVER_FILL(0xC0);

  // values for the ghctl calls
  const int GNAM_NEVER_RESET = 7;
  const int GNAM_RESET_AT_WASTART = 8;
  const int GNAM_RESET_AT_WASTOP = 9;
  const int GNAM_RESET_AT_WASS = 10;

  const int GNAM_FILL_ANYTIME = 11;
  const int GNAM_FILL_WHEN_READY = 12;
  const int GNAM_FILL_WHEN_STANDBY = 13;
  const int GNAM_NEVER_FILL = 14;

  // macros for the ghctl calls
#define GH_NEVER_RESET(GH)        (GH->ghctl(7))
#define GH_RESET_AT_WASTART(GH)   (GH->ghctl(8))
#define GH_RESET_AT_WASTOP(GH)    (GH->ghctl(9))
#define GH_RESET_AT_WASS(GH)      (GH->ghctl(10))

#define GH_FILL_ANYTIME(GH)       (GH->ghctl(11))
#define GH_FILL_WHEN_READY(GH)    (GH->ghctl(12))
#define GH_FILL_WHEN_STANDBY(GH)  (GH->ghctl(13))
#define GH_NEVER_FILL(GH)         (GH->ghctl(14))

  extern "C" void warmStart (void);
  extern "C" void warmStop (void);

#define MY_WARM_START_STOP(ArgC,ArgV) do if (ArgC == 1) {	\
    if (strcasecmp(ArgV[0],"warmStart") == 0) warmStart();	\
    else if (strcasecmp(ArgV[0],"warmStop") == 0) warmStop();	\
  } while(0)
}

#endif // _GNAM_NEW_API_

#endif // _GNAM_PATCH_H_

#include <time.h>
#include <vector>
#include <string>
#include <stdlib.h>
#include <errno.h>
#include <map>
#include <list>
#include <stack>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cstdio>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <wait.h>
#include <algorithm>

// GNAM: global include file for libraries
#include <gnam/gnamutils/GnamUtils.h>
// TDAQ: OKS DB entry point
#include <config/Configuration.h>
// TDAQ: for DB object casting
#include <dal/util.h>
// Pixel: definition of the class corresponding to PixelEvent
#include "PixRCDMonitoring/PixelEvent.h"
// Pixel: definition of PixelHisto class (this class)
#include "PixRCDMonitoring/PixelHisto.h"
// Pixel: detector parameters
#include "PixRCDMonitoring/GeographicHisto.h"
#include "PixRCDMonitoring/PixelParameters.h"
// Pixel: exception definitions
#include "PixRCDMonitoring/PixelGnamException.h"
// DAL: definition of the DB object corresponding to PixGnamLibrary
#include "PixGnamLibrarydal/PixGnamLibrary.h"
#include "Config/Config.h"
// Pixel: putPixDisable
//#include "PixRCDModules/PixRCDModule.h" // this causes a linker error when configuring (ROSException not found)
//#include "PixConnectivity/GenericConnectivityObject.h"
#include "PixConnectivity/PixConnectivity.h"
#include "PixConnectivity/PartitionConnectivity.h"
#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixConnectivity/RosConnectivity.h"
#include "PixConnectivity/RodBocConnectivity.h"
#include "PixConnectivity/OBConnectivity.h"
#include "PixConnectivity/Pp0Connectivity.h"
#include "PixModuleGroup/PixModuleGroup.h"
#include "PixConnectivity/ModuleConnectivity.h"

#include "PixUtilities/PixISManager.h"
#include "PixUtilities/PixMessages.h"

// for IS reading of PixDisable
#include <is/infoT.h>
#include <is/infodictionary.h>
#include <memory>
#include <PixConnectivity/PixDisable.h>

bool isStartOfRun=false;
using namespace std;

bool use_PixDBServer = true;
// bool use_ModGroup = false;

//-----------------------------------------------------------------------------
// Publishing of different components can be changed by issuing commands to
// OHP during run. If ROD publish flags are set from the start, there will be
// OHP tabs for each ROD
//-----------------------------------------------------------------------------

bool publish_geodisks = false;
bool publish_geolayers = false;

// fill ROD (logical ID), off by default
// bool fill_ROD[MAX_ROS_NUMBER][MAX_ROD_PER_ROS] = {};

bool fill_single_modules = false;

// Do basic clustering by searching for neighbor pixels in a row: ToT is
// filled only if a cluster is found and the sum of the ToT values of the hit
// pixels is filled.
bool do_clustering = false;

/*
// Automatic adjustment of expected occupancy based on number of monitored
// events
bool adjust_occupancy = true;
double occupancy_acceptance = 0.5;
double expected_occupancy = 1E-6;
*/

// GNAM
static std::vector<GnamHisto*> *histolist = NULL;
uint PrintOutLevel = 10;
static int errCout = 0; // diane - to stop printing warning when overwhelming
//static int FEerrCout = 0; // diane - TMP to check Service Code for IBL

// Histograms
//------------

// Module histograms are dynamically allocated to module that are to be
// watched (there's a total of 10 statically aloocated, they are then
// linked to a module if it is to be filled
const uint MAX_DYN_HISTO = 10;
// Hitmap -> per pixel basis
static GnamHisto *module_hitmap[MAX_DYN_HISTO];
// ToT
static GnamHisto *module_tot[MAX_DYN_HISTO];
// Clustered ToT summary
static GnamHisto *module_tot_cluster[MAX_DYN_HISTO];
// ToT cluster size
static GnamHisto *module_tot_cluster_size[MAX_DYN_HISTO];
// Time alignment
static GnamHisto *module_hittime[MAX_DYN_HISTO];
// FE/MCC error flags
static GnamHisto *module_ercode[MAX_DYN_HISTO];
// Number of hits per event
static GnamHisto *module_hits[MAX_DYN_HISTO];
// BCID of hits
static GnamHisto *module_bcid[MAX_DYN_HISTO];
// Number of monitored events for this module
static GnamHisto *module_events[MAX_DYN_HISTO];
// LVL1ID errors
//static GnamHisto *module_lvl1id[MAX_DYN_HISTO];

// Bin 0 contains the number of headers counted or the number of headers per event respectively. Bin 1 contains the enabled status
static GnamHisto *module_status[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB];
static GnamHisto *module_statusRatio[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB];

// These histograms are for timing checks
static GnamHisto *module_level1Skips[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB];
static GnamHisto *global_level1Skips;
static GnamHisto *module_level1Id[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB];
static GnamHisto *global_level1Id;

// and some more module level histograms
static GnamHisto *module_ToT[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB];
static GnamHisto *module_numHits[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB];
static GnamHisto *module_LVL1IDErrors[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB];
list<time_t> l_module_LVL1IDErrors[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB]; //actually bcid error for 3-layer pixel
list<time_t> l_histofills;


//for new occupancy
int module_nHitsSinceUpdate[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB];
float nEvents_thisUpdate;
float nEvents_lastUpdate;


// Global
// Hitmap of the detector -> per module basis
static GnamHisto *detector_hitmap;
// Occupancy of the detector -> per module basis
static GnamHisto *detector_occupancy;
// Occupancy of Layer2 -> per module basis
static GnamHisto *layer2_occupancy;
// Occupancy of Layer1 -> per module basis
static GnamHisto *layer1_occupancy;
// Occupancy of Layer0 -> per module basis
static GnamHisto *layer0_occupancy;
// Occupancy of LayerIbl -> per module basis
static GnamHisto *layerIbl_occupancy;
// Occupancy of LayerDbm -> per module basis
static GnamHisto *layerDbm_occupancy;
// Occupancy of DiskA -> per module basis
static GnamHisto *diskA_occupancy;
// Occupancy of DiskC -> per module basis
static GnamHisto *diskC_occupancy;

// Hitmap for outer edge of IBL
static GnamHisto *ibl_edge_hitmap;
// Occupancy for outer edge of IBL
static GnamHisto *ibl_edge_occupancy;


//Christian June2011

//Occupancy of the detecor -> not per module but 1-dimensional
//static GnamHisto *detector_occupancy_distribution;

//Occupancy of the Layer2 -> not per module but 1-dimensional
static GnamHisto *layer2_occupancy_distribution;

//Occupancy of the Layer1 -> not per module but 1-dimensional
static GnamHisto *layer1_occupancy_distribution;

//Occupancy of the Layer0 -> not per module but 1-dimensional
static GnamHisto *layer0_occupancy_distribution;

//Occupancy of the LayerIbl -> not per module but 1-dimensional
static GnamHisto *layerIbl_occupancy_distribution;

//Occupancy of the LayerDbm -> not per module but 1-dimensional
static GnamHisto *layerDbm_occupancy_distribution;

//Occupancy of the diskA -> not per module but 1-dimensional
static GnamHisto *diskA_occupancy_distribution;

//Occupancy of the DiskC -> not per module but 1-dimensional
static GnamHisto *diskC_occupancy_distribution;


// Occupancy of Layer2 -> per module basis
static GnamHisto *layer2_occupancy_since_update;
// Occupancy of Layer1 -> per module basis
static GnamHisto *layer1_occupancy_since_update;
// Occupancy of Layer0 -> per module basis
static GnamHisto *layer0_occupancy_since_update;
// Occupancy of LayerIbl -> per module basis
static GnamHisto *layerIbl_occupancy_since_update;
// Occupancy of LayerDbm -> per module basis
static GnamHisto *layerDbm_occupancy_since_update;
// Occupancy of DiskA -> per module basis
static GnamHisto *diskA_occupancy_since_update;
// Occupancy of DiskC -> per module basis
static GnamHisto *diskC_occupancy_since_update;

//Christian June2011


//Occupancy of the Layer2 -> not per module but 1-dimensional
static GnamHisto *layer2_occupancy_distribution_sinceUpdate;

//Occupancy of the Layer1 -> not per module but 1-dimensional
static GnamHisto *layer1_occupancy_distribution_sinceUpdate;

//Occupancy of the Layer0 -> not per module but 1-dimensional
static GnamHisto *layer0_occupancy_distribution_sinceUpdate;

//Occupancy of the LayerIbl -> not per module but 1-dimensional
static GnamHisto *layerIbl_occupancy_distribution_sinceUpdate;

//Occupancy of the LayerDbm -> not per module but 1-dimensional
static GnamHisto *layerDbm_occupancy_distribution_sinceUpdate;

//Occupancy of the diskA -> not per module but 1-dimensional
static GnamHisto *diskA_occupancy_distribution_sinceUpdate;

//Occupancy of the DiskC -> not per module but 1-dimensional
static GnamHisto *diskC_occupancy_distribution_sinceUpdate;

// Disabled of Layer2 -> per module basis
static GnamHisto *layer2_disabled;
// Disabled of Layer1 -> per module basis
static GnamHisto *layer1_disabled;
// Disabled of Layer0 -> per module basis
static GnamHisto *layer0_disabled;
// Disabled of LayerIbl -> per module basis
static GnamHisto *layerIbl_disabled;
// Disabled of LayerDbm -> per module basis
static GnamHisto *layerDbm_disabled;
// Disabled of DiskA -> per module basis
static GnamHisto *diskA_disabled;
// Disabled of DiskC -> per module basis
static GnamHisto *diskC_disabled;


// LVL1ID errors in the last 5 minutes of Layer2 -> per module basis
static GnamHisto *layer2_lvl1id_errors_lastfiveminutes;
// LVL1ID errors in the last 5 minutes of Layer1 -> per module basis
static GnamHisto *layer1_lvl1id_errors_lastfiveminutes;
// LVL1ID errors in the last 5 minutes of Layer0 -> per module basis
static GnamHisto *layer0_lvl1id_errors_lastfiveminutes;
// LVL1ID errors in the last 5 minutes of LayerIbl -> per module basis
static GnamHisto *layerIbl_lvl1id_errors_lastfiveminutes;
// LVL1ID errors in the last 5 minutes of LayerDbm -> per module basis
static GnamHisto *layerDbm_lvl1id_errors_lastfiveminutes;
// LVL1ID errors in the last 5 minutes of DiskA -> per module basis
static GnamHisto *diskA_lvl1id_errors_lastfiveminutes;
// LVL1ID errors in the last 5 minutes of DiskC -> per module basis
static GnamHisto *diskC_lvl1id_errors_lastfiveminutes;

// LVL1ID errors per event in the last 5 minutes of LayerIbl -> per module basis
static GnamHisto *layerIbl_lvl1id_errorfraction_lastfiveminutes;
// LVL1ID errors per event in the last 5 minutes of LayerDbm -> per module basis
static GnamHisto *layerDbm_lvl1id_errorfraction_lastfiveminutes;



// Error fraction of Layer2 -> per module basis
static GnamHisto *layer2_sumerrorfraction;
static GnamHisto *layer2_anyerrorfraction;
// Error Fraction of Layer1 -> per module basis
static GnamHisto *layer1_sumerrorfraction;
static GnamHisto *layer1_anyerrorfraction;
// Error Fraction of Layer0 -> per module basis
static GnamHisto *layer0_sumerrorfraction;
static GnamHisto *layer0_anyerrorfraction;
// Error Fraction of LayerIbl -> per module basis
static GnamHisto *layerIbl_sumerrorfraction;
static GnamHisto *layerIbl_anyerrorfraction;
// Error Fraction of LayerDbm -> per module basis
static GnamHisto *layerDbm_sumerrorfraction;
static GnamHisto *layerDbm_anyerrorfraction;
// Error Fraction of DiskA -> per module basis
static GnamHisto *diskA_sumerrorfraction;
static GnamHisto *diskA_anyerrorfraction;
// Error Fraction of DiskC -> per module basis
static GnamHisto *diskC_sumerrorfraction;
static GnamHisto *diskC_anyerrorfraction;

//// IBL/DBM specific histograms
// tally of each type of IBL/DBM FE error
static GnamHisto *layerIblDbm_FEServiceCode;
static GnamHisto *layerIblDbm_FEerrors;
static GnamHisto *layerIblDbm_FEerrors_notweighted;
// Diane - Ongoing adding dedicated histograms
//static GnamHisto *layerDbm_FEServiceCode;
//static GnamHisto *layerDbm_FEerrors;
//static GnamHisto *layerDbm_FEerrors_notweighted;

/* obsolete due to DQMF
// Summarizes detector pamrameters and gives a red/green statuscode
static GnamHisto *detector_summary;
*/

// Overview of the occupancy (how many pixels with which occupancy)
static GnamHisto *detector_occupancy_summary;
// Overview of the occupancy (how many pixels with which occupancy)
static GnamHisto *detector_occupancy_summary_sinceUpdate;
// ToT overview histogram, filled with maxima of the Landau distribution of
//  ToT from the modules
static GnamHisto *detector_tot;
// LVL1A distribution of the detector
static GnamHisto *detector_time;

/*
// Shows the disks with their modules and if they are noisy
static GnamHisto *detector_disks;
// Shows the B-layer of the Barrel and if a module is noisy
static GnamHisto *detector_blayer;
// Shows Layer 1 and if a module is noisy
static GnamHisto *detector_layer1;
// Shows Layer 2 and if a module is noisy
static GnamHisto *detector_layer2;
*/

// BCID of hits summary
static GnamHisto *detector_BCID;

// Some error flags of the ROD, per ROD
static GnamHisto *detector_3lp_ROD_errors;
static GnamHisto *detector_3lp_ROD_errorsvsBCID;
static GnamHisto *detector_IblDbm_ROD_errors;
static GnamHisto *detector_IblDbm_ROD_errorfraction;
static GnamHisto *detector_IblDbm_ROD_errorsvsBCID;
// Time profile for the 5 error categories
static GnamHisto *detector_ROD_syncherrors;
static GnamHisto *detector_ROD_transerrors;
static GnamHisto *detector_ROD_SEUerrors;
static GnamHisto *detector_ROD_trunchighoccerrors;
static GnamHisto *detector_ROD_DAQerrors;
static GnamHisto *detector_ROD_timeouterrors;
// for time istograms
time_t startTime; // time of start of run
// keep track of the number of time intervals passed since last incident for time resoluted errors
int timeIntervals_ROD_syncherrors = -1;
int timeIntervals_ROD_transerrors = -1;
int timeIntervals_ROD_SEUerrors = -1;
int timeIntervals_ROD_trunchighoccerrors = -1;
int timeIntervals_ROD_DAQerrors = -1;
int timeIntervals_ROD_timeouterrors = -1;


// Histograms for usage with ROD Simulator
static GnamHisto *FE_vs_L1ID;
static GnamHisto *TOT_vs_BCID;
static GnamHisto *ROD_FE_vs_L1ID[MAX_ROD_PER_ROS];
static GnamHisto *ROD_TOT_vs_BCID[MAX_ROD_PER_ROS];


// Global
// Number of events
static GnamHisto *global_events;
// Number of hits
static GnamHisto *global_hits;
// Number of hits normalized to one module (can be merged)
static GnamHisto *global_hits_normalized;

//additional data
static int global_errorsum[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int global_errorany[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};

// Connectivity Database


// Tags for reading the Connectivity Database
string m_ros_name, conn_tag, cfg_tag, server_tag, id_tag;
uint m_ros_number = 0;
bool m_ros_name_set = false;
bool conn_tag_set = false;
bool cfg_tag_set = false;
bool server_tag_set = false;
bool id_tag_set = false;

string partition_tag; // tag for the initial partition
bool partition_tag_set = false;

// MPG: make all maps static

// ModLogMap contains the logical mapping of bytestream module identifier to
// the Connectivity DB identifier. The pair of numbers is
// source_identifier/linknumber. The array consists of
// ROD number/Optoboard_number/Module_number (unique).
static map< pair<int,int>,vector<int> > ModLogMap;
// ModSourceMap maps the source ID to the Module name
static map< pair<int,int>,string > ModSourceMap;
// ModGeoMap contains the geographical mapping of modules GEOIDs to the
// logical ID
static map< vector<int>,string > ModGeoMap;
// ModGeoMap_all contains the geographical mapping of modules GEOIDs to the
// logical ID, also for disabled modules
static map< vector<int>,string > ModGeoMap_all;
// ModNamMap maps the logical ID of the module to its name
static map< string,vector<int> > ModNamMap;
// ModOffMap maps the logical ID of the module to its offline ID
// (EC/disk/Iphi/Ieta)
static map< vector<int>,vector<int> > ModOffMap;
// inverse ModOffMap
static map< vector<int>,vector<int> > ModModelMap;
/*
// ModPosMap maps the noisiness oft a module to its x and y position
static map< vector<int>,int > ModPosMap;
*/
// RODLogMap contains the logical mapping of source identifier to
// the corresponding ROD number
static map< int, int > RODLogMap;
// RODGeoMap contains the geographical mapping of ROD GEOIDs to logical ID
// (ROD number)
static map< int,string > RODGeoMap;
// RODNamMap maps the logical ID (ROD number) of the ROD to its name
static map< string,int > RODNamMap;
// OBGeoMap maps the name of the Optoboard to its logical ID. The vector
// contains the ROD id / OB id pair
static map< vector<int>,string > OBGeoMap;
// map containing information about the allocation of modules to the module histograms
static map< string,int > ModDynMap;
// vector associated to ModDynMap to find, which places are already taken
//MPG vector<bool> ModDynMap_places;
static bool ModDynMap_places[MAX_DYN_HISTO];

// store the name and phinumber of disabled modules
static map<string,int> disabled_modules;
static map<string,int> autoDisabled_modules;
static map<string,int> autoDisabled_RODs;
static map<pair<int,int>, string> disabled_modules_names;

IPCPartition* ipcPartitionDD = NULL;

PixISManager *is_manager = NULL;
static char modulenames[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB][30];
// Module IS variables, initialize with false, nothing to publish
static int is_mod_BCID_errors[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_BCID_errors_ref[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_timeout_errors[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_timeout_errors_ref[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_LVL1ID_errors[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_LVL1ID_errors_ref[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_preamble_errors[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_preamble_errors_ref[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_trailer_errors[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_trailer_errors_ref[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_limit_errors[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_limit_errors_ref[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_overflow_errors[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_overflow_errors_ref[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};

// FE flag errors from FE for IBL (just a couple to start)
static int is_mod_IblDbmFEBCID_errors[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_IblDbmFEBCID_errors_ref[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_IblDbmFEHamming_errors[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_IblDbmFEHamming_errors_ref[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_IblDbmFEL1_errors[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_IblDbmFEL1_errors_ref[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};

// FE flag errors from FE and MCC for Pixels
static int is_mod_FEEOC_errors[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_FEEOC_errors_ref[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_FEHamming_errors[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_FEHamming_errors_ref[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_FEregparity_errors[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_FEregparity_errors_ref[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_FEhitparity_errors[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_FEhitparity_errors_ref[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_MCChitoverflow_errors[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_MCChitoverflow_errors_ref[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_MCCEoEoverflow_errors[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_MCCEoEoverflow_errors_ref[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_MCCL1ID_errors[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_MCCL1ID_errors_ref[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_MCCBCIDEoE_errors[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_MCCBCIDEoE_errors_ref[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_MCCL1IDEoE_errors[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
static int is_mod_MCCL1IDEoE_errors_ref[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};

static int is_mod_lvl1id_errors_lastfiveminutes_ref[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB]={};

//static int is_mod_NHits_ref[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
// ROD IS variables, initialize with false, nothing to publish
//static char timnames[MAX_CRATE_NUMBER][20]; 
const uint MAX_ROD_NAME_LENGTH = 20;
static char rodnames[MAX_ROD_PER_ROS][MAX_ROD_NAME_LENGTH]; 
static int is_rod_BCID_errors[MAX_ROD_PER_ROS] = {};
static int is_rod_BCID_errors_ref[MAX_ROD_PER_ROS] = {};
static int is_rod_LVL1ID_errors[MAX_ROD_PER_ROS] = {};
static int is_rod_LVL1ID_errors_ref[MAX_ROD_PER_ROS] = {};
static int is_rod_timeout_errors[MAX_ROD_PER_ROS] = {};
static int is_rod_timeout_errors_ref[MAX_ROD_PER_ROS] = {};
static int is_rod_dataincorrect_errors[MAX_ROD_PER_ROS] = {};
static int is_rod_dataincorrect_errors_ref[MAX_ROD_PER_ROS] = {};
static int is_rod_overflow_errors[MAX_ROD_PER_ROS] = {};
static int is_rod_overflow_errors_ref[MAX_ROD_PER_ROS] = {};
static int is_rod_fragment_size[MAX_ROD_PER_ROS] = {};
static int is_rod_fragment_size_ref[MAX_ROD_PER_ROS] = {};

//DADA
static const PixelEvent *pixel = NULL;

// int checkArrayBounds(int ros, int rod, int ob, int mod);
// int checkArrayBounds(int ros, int rod);
// int checkArrayBounds(int histo);

int getOutlink(PixLib::ModuleConnectivity* modConn, const string &readoutSpeed, const std::string &mod_name, int *source_id); // source_id is modified
int getOutlinkPIX(PixLib::ModuleConnectivity* modConn, const string &readoutSpeed);
int getOutlinkL12(PixLib::ModuleConnectivity* modConn, int &source_id); // source_id is modified
int getOutlinkIBL(const std::string &mod_name, int &source_id); // source_id is modified
int getOutlinkDBM(const std::string &mod_name, int &source_id); // source_id is modified

bool isIbl(const int &source_id);
bool isDbm(const int &source_id);
bool isIblOrDbm(const int &source_id);
bool isIblOrDbm(const string &modname);
//diane
bool hasUpgradedReadout(const int &source_id);
bool isCrateL1(const int &source_id);
bool isCrateL2(const int &source_id);
bool isCrateL3(const int &source_id);
bool isCrateL4(const int &source_id);

void parse_mod_name_of_layer(const string &mod_name, int *layernumber, int *bistavenumber, int *stavenumber, int *etanumber);
void parse_mod_name_of_disk(const string &mod_name, int *disknumber, bool *a_side);
void update_title(GnamHisto* hist, const int &run_number, const int &luminosity_block);
void reset_module_hist(GnamHisto* h, const string &title, const string &name);
void insert_disabled_modules(Pp0Connectivity* Pp0Conn, const string &readoutSpeed, int &source_id, const int &rod_number, const int &ob_number);
void insert_disabled_module(const int &source_id, const int &outLink, const string &mod_name, const int &mod_phi, const int &rod_number, const int &ob_number, const int &mod_number);
void insert_enabled_module(const int &source_id, const int &outLink, const string &mod_name, const int &mod_EC,  const int &mod_Disk,  const int &mod_phi,  const int &mod_eta, const int &rod_number, const int &ob_number, const int &mod_number);
int get_ibl_stavenumber( string mod_name );
int ibl_edge_check( string mod_name, uint kCol);

int min_link(int a, int b);
int find_max_bin(GnamHisto *histo);
//DADA void fill_cluster(const PixelEvent *pixel);
void fill_cluster();
void fillConnectivity();

const int nmodperstave = 13;
const char *modlabel[nmodperstave] = {"M6C","M5C","M4C","M3C","M2C","M1C","M0","M1A","M2A","M3A","M4A","M5A","M6A"};

// phi shifted by 1 relative to Layer1
const int nstavelayer2 = 53;
const char *stavelayer2[nstavelayer2] = {"B01_S2","B02_S1","B02_S2","B03_S1",
					 "B03_S2","B04_S1","B04_S2","B05_S1",
					 "B05_S2","B06_S1","B06_S2","B07_S1",
					 "B07_S2","B08_S1","B08_S2","B09_S1",
					 "B09_S2","B10_S1","B10_S2","B11_S1",
					 "B11_S2","B12_S1","B12_S2","B13_S1",
					 "B13_S2","B14_S1","B14_S2","B15_S1",
					 "B15_S2","B16_S1","B16_S2","B17_S1",
					 "B17_S2","B18_S1","B18_S2","B19_S1",
					 "B19_S2","B20_S1","B20_S2","B21_S1",
					 "B21_S2","B22_S1","B22_S2","B23_S1",
					 "B23_S2","B24_S1","B24_S2","B25_S1",
					 "B25_S2","B26_S1","B26_S2","B01_S1",
					 ""};

const int nstavelayer1 = 39;
const char *stavelayer1[nstavelayer1] = {"B01_S1","B01_S2","B02_S1","B02_S2",
					 "B03_S1","B03_S2","B04_S1","B04_S2",
					 "B05_S1","B05_S2","B06_S1","B06_S2",
					 "B07_S1","B07_S2","B08_S1","B08_S2",
					 "B09_S1","B09_S2","B10_S1","B10_S2",
					 "B11_S1","B11_S2","B12_S1","B12_S2",
					 "B13_S1","B13_S2","B14_S1","B14_S2",
					 "B15_S1","B15_S2","B16_S1","B16_S2",
					 "B17_S1","B17_S2","B18_S1","B18_S2",
					 "B19_S1","B19_S2",""};

// phi shifted by -1 relative to Layer1
const int nstavelayer0 = 23;
const char *stavelayer0[nstavelayer0] = {"B11_S2","B01_S1","B01_S2","B02_S1",
					 "B02_S2","B03_S1","B03_S2","B04_S1",
					 "B04_S2","B05_S1","B05_S2","B06_S1",
					 "B06_S2","B07_S1","B07_S2","B08_S1",
					 "B08_S2","B09_S1","B09_S2","B10_S1",
					 "B10_S2","B11_S1",""};


// IBL has a different module arrangement, so it needs its own modlable array
const int nmodperstaveIbl = 16;
const char *modlabelIbl[nmodperstaveIbl] = {"M4_C8","M4_C7","M3_C6","M3_C5","M2_C4","M2_C3","M1_C2","M1_C1","M1_A1","M1_A2","M2_A3","M2_A4","M3_A5","M3_A6","M4_A7","M4_A8"};

const int nstavelayerIbl = 15; // IBL has 14 staves, plus one empty entry (DBM is elsewhere)
const char *stavelayerIbl[nstavelayerIbl] = {"S01","S02","S03","S04","S05","S06","S07",
					     "S08","S09","S10","S11","S12","S13","S14", 
					     ""}; // including a blank entry because the other layers have it.

// DBM has a different module arrangement, so it needs its own modlabel array
const int nmodpertelescopeDbm = 6; // 3 on each side
const char *modlabelDbm[nmodpertelescopeDbm] = {"C_lay2","C_lay1","C_lay0","A_lay0","A_lay1","A_lay2"};

const int ntelescopeDbm = 5; // DBM has 4 telescopes, plus one empty entry
const char *telescopeDbm[ntelescopeDbm] = {"M1","M2","M3","M4",
					   ""}; // including a blank entry because the other layers have it.
const int nphiperdisk = 49;
const char *nstaveA[nphiperdisk] = {
  "B01_S2_M1", "B01_S2_M6", "B01_S2_M2", "B01_S2_M5", "B01_S2_M3", "B01_S2_M4", 
  "B02_S1_M1", "B02_S1_M6", "B02_S1_M2", "B02_S1_M5", "B02_S1_M3", "B02_S1_M4", 
  "B02_S2_M1", "B02_S2_M6", "B02_S2_M2", "B02_S2_M5", "B02_S2_M3", "B02_S2_M4", 
  "B03_S1_M1", "B03_S1_M6", "B03_S1_M2", "B03_S1_M5", "B03_S1_M3", "B03_S1_M4", 
  "B03_S2_M1", "B03_S2_M6", "B03_S2_M2", "B03_S2_M5", "B03_S2_M3", "B03_S2_M4", 
  "B04_S1_M1", "B04_S1_M6", "B04_S1_M2", "B04_S1_M5", "B04_S1_M3", "B04_S1_M4", 
  "B04_S2_M1", "B04_S2_M6", "B04_S2_M2", "B04_S2_M5", "B04_S2_M3", "B04_S2_M4", 
  "B01_S1_M1", "B01_S1_M6", "B01_S1_M2", "B01_S1_M5", "B01_S1_M3", "B01_S1_M4",""};

const char *nstaveC[nphiperdisk] = {
  "B01_S2_M4", "B01_S2_M3", "B01_S2_M5", "B01_S2_M2", "B01_S2_M6", "B01_S2_M1", 
  "B02_S1_M4", "B02_S1_M3", "B02_S1_M5", "B02_S1_M2", "B02_S1_M6", "B02_S1_M1", 
  "B02_S2_M4", "B02_S2_M3", "B02_S2_M5", "B02_S2_M2", "B02_S2_M6", "B02_S2_M1", 
  "B03_S1_M4", "B03_S1_M3", "B03_S1_M5", "B03_S1_M2", "B03_S1_M6", "B03_S1_M1", 
  "B03_S2_M4", "B03_S2_M3", "B03_S2_M5", "B03_S2_M2", "B03_S2_M6", "B03_S2_M1", 
  "B04_S1_M4", "B04_S1_M3", "B04_S1_M5", "B04_S1_M2", "B04_S1_M6", "B04_S1_M1", 
  "B04_S2_M4", "B04_S2_M3", "B04_S2_M5", "B04_S2_M2", "B04_S2_M6", "B04_S2_M1", 
  "B01_S1_M4", "B01_S1_M3", "B01_S1_M5", "B01_S1_M2", "B01_S1_M6", "B01_S1_M1",""};

const int ndisks = 3;
const char *disks[ndisks] = {"Disk1","Disk2","Disk3"}; //MPG

int m_nmods=0; // Number of modules in this GNAM application
int m_nrods=0; // Number of RODS in this GNAM application
//const int nrods_all = 147; // PIX has 132, IBL has 14, and DBM has 1

const int nrods_3lp = 132; // PIX has 132, IBL has 14, and DBM has 1
const char *rodnames_3lp[nrods_3lp] =  { // From where does this order come?
  "ROD_B1_S10","ROD_B1_S11","ROD_B1_S12","ROD_B1_S14","ROD_B1_S15","ROD_B1_S16","ROD_B1_S17","ROD_B1_S18","ROD_B1_S19","ROD_B1_S20","ROD_B1_S21",
  "ROD_B1_S5","ROD_B1_S6","ROD_B1_S7","ROD_B1_S8","ROD_B1_S9",

  "ROD_B2_S10","ROD_B2_S11","ROD_B2_S12","ROD_B2_S14","ROD_B2_S15","ROD_B2_S16","ROD_B2_S17","ROD_B2_S18","ROD_B2_S19","ROD_B2_S20","ROD_B2_S21",
  "ROD_B2_S5","ROD_B2_S6","ROD_B2_S7","ROD_B2_S8","ROD_B2_S9",

  "ROD_B3_S10","ROD_B3_S11","ROD_B3_S12","ROD_B3_S14","ROD_B3_S15","ROD_B3_S16","ROD_B3_S17","ROD_B3_S18","ROD_B3_S19",
  "ROD_B3_S7","ROD_B3_S8","ROD_B3_S9",

  "ROD_D1_S10","ROD_D1_S11","ROD_D1_S12","ROD_D1_S14","ROD_D1_S15","ROD_D1_S16","ROD_D1_S17","ROD_D1_S18","ROD_D1_S19","ROD_D1_S20","ROD_D1_S21",
  "ROD_D1_S5","ROD_D1_S6","ROD_D1_S7","ROD_D1_S8","ROD_D1_S9",

  "ROD_D2_S10","ROD_D2_S11","ROD_D2_S12","ROD_D2_S14","ROD_D2_S15","ROD_D2_S16","ROD_D2_S17",
  "ROD_D2_S9",

  "ROD_L1_S10","ROD_L1_S11","ROD_L1_S12","ROD_L1_S14","ROD_L1_S15","ROD_L1_S16","ROD_L1_S17","ROD_L1_S18","ROD_L1_S19","ROD_L1_S20","ROD_L1_S21",
  "ROD_L1_S5","ROD_L1_S6","ROD_L1_S7","ROD_L1_S8","ROD_L1_S9",

  "ROD_L2_S10","ROD_L2_S11","ROD_L2_S12","ROD_L2_S14","ROD_L2_S15","ROD_L2_S16","ROD_L2_S17","ROD_L2_S18","ROD_L2_S19","ROD_L2_S20","ROD_L2_S21",
  "ROD_L2_S5","ROD_L2_S6","ROD_L2_S7","ROD_L2_S8","ROD_L2_S9",

  "ROD_L3_S10","ROD_L3_S11","ROD_L3_S12","ROD_L3_S14","ROD_L3_S15","ROD_L3_S16","ROD_L3_S17","ROD_L3_S18","ROD_L3_S19","ROD_L3_S20","ROD_L3_S21",
  "ROD_L3_S5","ROD_L3_S6","ROD_L3_S7","ROD_L3_S8","ROD_L3_S9",

  "ROD_L4_S10","ROD_L4_S11","ROD_L4_S12","ROD_L4_S14","ROD_L4_S15","ROD_L4_S16","ROD_L4_S17","ROD_L4_S18","ROD_L4_S19","ROD_L4_S20","ROD_L4_S21",
  "ROD_L4_S5","ROD_L4_S6","ROD_L4_S7","ROD_L4_S8","ROD_L4_S9"
};

const int nrods_IblDbm = 15; // PIX has 132, IBL has 14, and DBM has 1
const char *rodnames_IblDbm[nrods_IblDbm] =  {
  "ROD_I1_S10","ROD_I1_S11","ROD_I1_S12","ROD_I1_S14","ROD_I1_S15","ROD_I1_S16","ROD_I1_S17","ROD_I1_S18","ROD_I1_S19","ROD_I1_S20","ROD_I1_S21",
  "ROD_I1_S6","ROD_I1_S7","ROD_I1_S8","ROD_I1_S9"
};


// insert rodname and arrayposition into a map for faster finding (in fillConnectivity)
map <string, int> rodnames_3lp_map;
map <string, int> rodnames_IblDbm_map;

/*
// Reference for time information
int eventTime_reference;
bool eventTime_reference_set = false;
*/

/* ************ *
 *    initDB    *
 * ************ */

/* ****************************************************************** *
 * Called once just after library loading if configuring from OKS DB. *
 * ****************************************************************** */
extern "C" void
initDB(Configuration *confDB, const gnamdal::GnamLibrary *library)
{
 try {
  if (PrintOutLevel>=1){
    printf("PixelHisto: Entering initDB\n");
    fflush(stdout);
  }
  ERS_PIX_INFO("GNAM Pixel initDB entry point called");


  // Library casting as taken from GNAM standard
  const PixGnamLibrarydal::PixGnamLibrary *lib = 
    confDB->cast<PixGnamLibrarydal::PixGnamLibrary,
    gnamdal::GnamLibrary>(library);
  if (lib==NULL){
    ERS_PIX_ERROR("PixGnamLibrarydal::PixGnamLibrary " << "pointer not initialized", ConfigError);
    cout <<"ERROR: Error reading DB configuration." << endl;
    //    abort();
  }

  //Christian
  if (m_IPCPartition != NULL) delete m_IPCPartition;
  m_IPCPartition = new IPCPartition(getenv("TDAQ_PARTITION"));
  if (m_ISDict != NULL) delete m_ISDict;
  m_ISDict = new ISInfoDictionary(*m_IPCPartition);
  if (m_ISReceiver != NULL) delete m_ISReceiver;
  m_ISReceiver = new ISInfoReceiver(*m_IPCPartition);

  try {
    m_ISReceiver->subscribe("RunParams.LuminosityInfo", callback_RunParams);
  }
  catch(daq::is::RepositoryNotFound){
    ERS_PIX_ERROR("no RunParams repository found",ConfigError);
    cout <<"ERROR: no RunParams repository found" << endl;
  }
  catch(daq::is::InfoNotFound){
    ERS_PIX_INFO("no info object LuminosityInfo for repository RunParams found");
  }


  // Read values from xml
  PrintOutLevel = lib->get_Verbosity();
  m_ros_name = lib->get_ROSName();
  conn_tag = lib->get_ConnectionTag();
  cfg_tag = lib->get_ConfigTag();
  partition_tag = lib->get_PartitionTag();
  server_tag = lib->get_ServerTag();
  id_tag = lib->get_IdTag();
  ERS_PIX_INFO("ROS Name: " << m_ros_name.c_str());
  ERS_PIX_INFO("Connection Tag: " << conn_tag.c_str());
  ERS_PIX_INFO("Config Tag: " << cfg_tag.c_str());
  ERS_PIX_INFO("Partition Tag: " << partition_tag.c_str());
  ERS_PIX_INFO("Server Tag: " << server_tag.c_str());
  ERS_PIX_INFO("Id Tag: " << id_tag.c_str());
  if (PrintOutLevel>=1){
    printf("Read from Connectivity DB:\n");
    printf("--------------------------\n");
    printf("PrintOutLevel: %i\n",PrintOutLevel);
    printf("ROS Name: %s\n",m_ros_name.c_str());
    printf("Connection Tag: %s\n",conn_tag.c_str());
    printf("Config Tag: %s\n",cfg_tag.c_str());
    printf("Partition Tag: %s\n",partition_tag.c_str());
    printf("Server Tag: %s\n",server_tag.c_str());
    printf("Id Tag: %s\n",id_tag.c_str());
    printf("--------------------------\n");
    fflush(stdout);
  }

  // Reset all maps
  ModLogMap.clear();
  ModSourceMap.clear(); // BPG
  ModGeoMap.clear();
  ModGeoMap_all.clear();
  ModNamMap.clear();
  ModOffMap.clear();
  //  ModPosMap.clear();
  RODLogMap.clear();
  RODGeoMap.clear();
  RODNamMap.clear();
  OBGeoMap.clear();
  ModModelMap.clear();
  ModDynMap.clear();
  for (uint i=0; i<MAX_DYN_HISTO; ++i) ModDynMap_places[i]=false;
  disabled_modules.clear();
  autoDisabled_modules.clear();
  autoDisabled_RODs.clear();
  disabled_modules_names.clear();
  // Fill connectivity later to catch readout speed

  // Fill the Rodnames into a map for faster access during the run
  for (int i=0; i<nrods_3lp; ++i) rodnames_3lp_map.insert(make_pair(rodnames_3lp[i],i));
  for (int i=0; i<nrods_IblDbm; ++i) rodnames_IblDbm_map.insert(make_pair(rodnames_IblDbm[i],i));

  //Christian
  run_number = 0;
  luminosity_block = 0;
  // /Christian

  if (PrintOutLevel>=1){
    printf("PixelHisto: Leaving initDB\n");
    fflush(stdout);
  }

 }
 catch(std::exception& e){
   cout<<"Exception caught in PixelHisto::initDB of type std::exception: " << e.what() << endl;
 }
 catch(char const* msg){
   cout<<"Exception caught in PixelHisto::initDB of type char const*: " << msg << endl;
 }
 catch(...){
   cout<<"Exception caught in PixelHisto::initDB of unknown type." << endl;
 }

}


/* ********* *
 *    end    *
 * ********* */

/* ************************************************************************* *
 * Called once just before library unloading. Performs final clean-up tasks. *
 * ************************************************************************* */
extern "C" void
end(void)
{
 try {
  if (PrintOutLevel>=1){
    printf("PixelHisto: Entering end\n");
    fflush(stdout);
  }
  ERS_PIX_INFO("GNAM Pixel end entry point called");

  // Manually delete histograms
  if (histolist!=NULL){
    size_t k, max;
    max = histolist->size();
    ERS_PIX_DEBUG(1,"Ready to delete histolist = " << showbase << hex << histolist << " (size = " << dec << max << ")");
    for (k=0; k<max; ++k) {
      if ( (*histolist)[k] != NULL) delete (*histolist)[k];
    }
    if (histolist != NULL) delete histolist;
    histolist = NULL;
  }

  if (is_manager != NULL) delete is_manager;
  is_manager = NULL;
  if (ipcPartitionDD != NULL) delete ipcPartitionDD;
  ipcPartitionDD = NULL;
  
  if (PrintOutLevel>=1){
    printf("PixelHisto: Leaving end\n");
    fflush(stdout);
  } 
  // MPG: removed map clearing since already done at start

 }
 catch(std::exception& e){
   cout<<"Exception caught in PixelHisto::end of type std::exception: " << e.what() << endl;
 }
 catch(char const* msg){
   cout<<"Exception caught in PixelHisto::end of type char const*: " << msg << endl;
 }
 catch(...){
   cout<<"Exception caught in PixelHisto::end of unknown type." << endl;
 }

}


/* *************** *
 *    fillHisto    *
 * *************** */

/* *********************************************** *
 * Fills histos from event container (branch(es)). *
 * *********************************************** */
extern "C" void
fillHisto(void)
{
 try {

  std::string applName="PixRCDMonitoring" + m_ros_name;

  int local_errorsum[MAX_ROD_PER_ROS][MAX_OB_PER_ROD][MAX_MOD_PER_OB] = {};
  string mod_name;

  map< pair<int,int>,string >::iterator ModSourceMap_iterator;
  map< pair<int,int>,vector<int> >::iterator ModLogMap_iterator;
  pair<int,int> ModMap_Key;

  global_events->Fill(0);
  global_hits->Fill(pixel->NHits);

  if (fill_single_modules){
    // Add one monitored event for each module
    for (map<string,int>::iterator ModDynMap_iterator=ModDynMap.begin(); ModDynMap_iterator != ModDynMap.end(); ++ModDynMap_iterator){
      uint histo_number = ModDynMap_iterator->second;
      if(histo_number >= MAX_DYN_HISTO){
	ERS_PIX_ERROR("histo_number="<<histo_number<<" >= MAX_DYN_HISTO("<<MAX_DYN_HISTO<<"): " << "this should never happen.",MapError);
	cout <<"ERROR: histo_number="<<histo_number<<" out of range...  Setting it to MAX_DYN_HISTO - 1 ..."<<endl;
	histo_number = MAX_DYN_HISTO - 1;
      }
      module_events[histo_number]->Fill(0);
    }
  }


  //fill ROD BCID of the hit from the first ROD found
  if (pixel->NRods > 0)
    detector_BCID->Fill(pixel->rod[0].bunchCross);
  
  /*
  // Get the time information for this event
  int eventTime_absolute = pixel->eventInfo.bunchCrossingTimeSec;
  if (!eventTime_reference_set){
    eventTime_reference = eventTime_absolute;
    eventTime_reference_set = true;
  }
  int eventTime = eventTime_absolute - eventTime_reference;
  */
  
  ///Christian2
  time_t RATE_CurrTime;

  time ( &RATE_CurrTime );
  //float lagtime = difftime(RATE_CurrTime, RATE_LastTime); //how long since we started the run;
  l_histofills.push_back(RATE_CurrTime);


  //--------------------------------------------------------
  // fill the error information to corresponding histograms
  //--------------------------------------------------------
  // loop over all FE error flags in the PixelEvent class
  
  for (uint kRod=0; kRod < pixel->NRods; ++kRod){ // loop over all ROD statuswords for IS

    // fill ROD statusword errors in summary per ROD and in time resoluted per errors category
    if (pixel->rod[kRod].rodStatusWord1 !=0){

      map <int,int>::iterator RODLogMap_iterator = RODLogMap.find(pixel->rod[kRod].rodRoLink);
      if (RODLogMap_iterator != RODLogMap.end()){
	uint rod_number = RODLogMap_iterator->second;
	if(rod_number >= MAX_ROD_PER_ROS){
	  ERS_PIX_ERROR("rod_number="<<rod_number<<" >= MAX_ROD_PER_ROS("<<MAX_ROD_PER_ROS<<"): "<< "this should never happen.",MapError);
	  cout <<"ERROR: rod_number="<<rod_number<<" out of range...  Try to continue to next rod..."<<endl;
	  continue;
	}
	string rod_name = RODGeoMap[rod_number];

	map<string, int>::iterator rodname_number = rodnames_3lp_map.find(rod_name); // ROD number for 3lp histograms
	if (rodname_number != rodnames_3lp_map.end()){

	  if ((pixel->rod[kRod].rodStatusWord1 & (1<<0))!=0){ // BCID error
	    detector_3lp_ROD_errors->Fill(rodname_number->second,0);
	    detector_3lp_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,0);
	    fillTimeHisto(detector_ROD_syncherrors,timeIntervals_ROD_syncherrors,rod_number,1);
	    is_rod_BCID_errors[rod_number]++;
	  }
	  if ((pixel->rod[kRod].rodStatusWord1 & (1<<1))!=0){ // LVL1ID error
	    detector_3lp_ROD_errors->Fill(rodname_number->second,1);
	    detector_3lp_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,1);
	    fillTimeHisto(detector_ROD_syncherrors,timeIntervals_ROD_syncherrors,rod_number,1);
	    is_rod_LVL1ID_errors[rod_number]++;
	  }
	  if ((pixel->rod[kRod].rodStatusWord1 & (1<<2))!=0){ // FE Timeout
	    detector_3lp_ROD_errors->Fill(rodname_number->second,2);
	    detector_3lp_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,2);
	    fillTimeHisto(detector_ROD_timeouterrors,timeIntervals_ROD_timeouterrors,rod_number,1);
	    is_rod_timeout_errors[rod_number]++;
	  }
	  if ((pixel->rod[kRod].rodStatusWord1 & (1<<3))!=0){ // Data may be incorrect
	    detector_3lp_ROD_errors->Fill(rodname_number->second,3);
	    detector_3lp_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,3);
	    is_rod_dataincorrect_errors[rod_number]++;
	  }
	  if ((pixel->rod[kRod].rodStatusWord1 & (1<<4))!=0){ // Internal Buffer overflow
	    detector_3lp_ROD_errors->Fill(rodname_number->second,4);
	    detector_3lp_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,4);
	    fillTimeHisto(detector_ROD_trunchighoccerrors,timeIntervals_ROD_trunchighoccerrors,rod_number,1);
	    is_rod_overflow_errors[rod_number]++;
	  }
	  if ((pixel->rod[kRod].rodStatusWord1 & (1<<16))!=0){ // Almost Full / Data Truncated
	    detector_3lp_ROD_errors->Fill(rodname_number->second,5);
	    detector_3lp_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,5);
	    fillTimeHisto(detector_ROD_trunchighoccerrors,timeIntervals_ROD_trunchighoccerrors,rod_number,1);
	  }
	  if ((pixel->rod[kRod].rodStatusWord1 & (1<<17))!=0){ // Data Overflow
	    detector_3lp_ROD_errors->Fill(rodname_number->second,6);
	    detector_3lp_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,6);
	    fillTimeHisto(detector_ROD_trunchighoccerrors,timeIntervals_ROD_trunchighoccerrors,rod_number,1);
	  }
	  if ((pixel->rod[kRod].rodStatusWord1 & (1<<18))!=0){ // Header Bit Error
	    detector_3lp_ROD_errors->Fill(rodname_number->second,7);
	    detector_3lp_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,7);
	    fillTimeHisto(detector_ROD_transerrors,timeIntervals_ROD_transerrors,rod_number,1);
	  }
	  if ((pixel->rod[kRod].rodStatusWord1 & (1<<19))!=0){ // Sync Error
	    detector_3lp_ROD_errors->Fill(rodname_number->second,8);
	    detector_3lp_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,8);
	    fillTimeHisto(detector_ROD_transerrors,timeIntervals_ROD_transerrors,rod_number,1);
	  }
	  if ((pixel->rod[kRod].rodStatusWord1 & (1<<20))!=0){ // invalid row or column
	    detector_3lp_ROD_errors->Fill(rodname_number->second,9);
	    detector_3lp_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,9);
	    fillTimeHisto(detector_ROD_transerrors,timeIntervals_ROD_transerrors,rod_number,1);
	  }
	  if ((pixel->rod[kRod].rodStatusWord1 & (1<<21))!=0){ // MCC sent empty event
	    detector_3lp_ROD_errors->Fill(rodname_number->second,10);
	    detector_3lp_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,10);
	    fillTimeHisto(detector_ROD_trunchighoccerrors,timeIntervals_ROD_trunchighoccerrors,rod_number,1);
	  }
	  if ((pixel->rod[kRod].rodStatusWord1 & (1<<22))!=0){ // MCC EOCOVERFLOW
	    detector_3lp_ROD_errors->Fill(rodname_number->second,11);
	    detector_3lp_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,11);
	    fillTimeHisto(detector_ROD_trunchighoccerrors,timeIntervals_ROD_trunchighoccerrors,rod_number,1);
	  }
	  if ((pixel->rod[kRod].rodStatusWord1 & (1<<23))!=0){ // MCC HAMMINGCODE
	    detector_3lp_ROD_errors->Fill(rodname_number->second,12);
	    detector_3lp_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,12);
	    fillTimeHisto(detector_ROD_SEUerrors,timeIntervals_ROD_SEUerrors,rod_number,1);
	  }
	  if ((pixel->rod[kRod].rodStatusWord1 & (1<<24))!=0){ // MCC REGPARITY
	    detector_3lp_ROD_errors->Fill(rodname_number->second,13);
	    detector_3lp_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,13);
	    fillTimeHisto(detector_ROD_SEUerrors,timeIntervals_ROD_SEUerrors,rod_number,1);
	  }
	  if ((pixel->rod[kRod].rodStatusWord1 & (1<<25))!=0){ // MCC HITPARITY
	    detector_3lp_ROD_errors->Fill(rodname_number->second,14);
	    detector_3lp_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,14);
	    fillTimeHisto(detector_ROD_SEUerrors,timeIntervals_ROD_SEUerrors,rod_number,1);
	  }
	  if ((pixel->rod[kRod].rodStatusWord1 & (1<<26))!=0){ // MCC BITFLIP
	    detector_3lp_ROD_errors->Fill(rodname_number->second,15);
	    detector_3lp_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,15);
	    fillTimeHisto(detector_ROD_SEUerrors,timeIntervals_ROD_SEUerrors,rod_number,1);
	  }
	  if ((pixel->rod[kRod].rodStatusWord1 & (1<<27))!=0){ // MCC HITOVERFLOW
	    detector_3lp_ROD_errors->Fill(rodname_number->second,16);
	    detector_3lp_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,16);
	    fillTimeHisto(detector_ROD_trunchighoccerrors,timeIntervals_ROD_trunchighoccerrors,rod_number,1);
	  }
	  if ((pixel->rod[kRod].rodStatusWord1 & (1<<28))!=0){ // MCC EOEOVERFLOW
	    detector_3lp_ROD_errors->Fill(rodname_number->second,17);
	    detector_3lp_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,17);
	    fillTimeHisto(detector_ROD_trunchighoccerrors,timeIntervals_ROD_trunchighoccerrors,rod_number,1);
	  }
	  if ((pixel->rod[kRod].rodStatusWord1 & (1<<29))!=0){ // MCC L1CHKFAILFE
	    detector_3lp_ROD_errors->Fill(rodname_number->second,18);
	    detector_3lp_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,18);
	    fillTimeHisto(detector_ROD_syncherrors,timeIntervals_ROD_syncherrors,rod_number,1);
	  }
	  if ((pixel->rod[kRod].rodStatusWord1 & (1<<30))!=0){ // MCC BCIDCHKFAIL
	    detector_3lp_ROD_errors->Fill(rodname_number->second,19);
	    detector_3lp_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,19);
	    fillTimeHisto(detector_ROD_syncherrors,timeIntervals_ROD_syncherrors,rod_number,1);
	  }
	  if ((pixel->rod[kRod].rodStatusWord1 & (1<<31))!=0){ // MCC L1CHKFAILGLOBAL
	    detector_3lp_ROD_errors->Fill(rodname_number->second,20);
	    detector_3lp_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,20);
	    fillTimeHisto(detector_ROD_syncherrors,timeIntervals_ROD_syncherrors,rod_number,1);
	  }

	} else { // not a 3lp ROD, it must be an IblDbm ROD
	  rodname_number = rodnames_IblDbm_map.find(rod_name); // ROD number for IblDbm histograms
	  if (rodname_number != rodnames_IblDbm_map.end()){

	    //// IBL ROD StatusWord1 Errors:
	    //// These are flags created by the ROD:
	    // bit 0: bcid error
	    // bit 1: l1id error
	    // bit 2: readout timeout error
	    // bit 3: logical OR of bits 31 downto 14
	    // bit 4: logical OR of bits 15 and 14
	    // bit 15-5: all zeros
	    // bit 16: non sequential order between the FEs data
	    // bit 17: preamble error in the FE (header not starting with E9)
	    // bit 18: raw data (inherited from Pixel..but not really meaninful here)
	    // bit 19: invalid row/column.
	    //// These should be the same as FE service codes without payload:
	    // bit 20: there are skipped triggers in the FEs (at least one skipped trigger).
	    // bit 21: FE BCID counter error
	    // bit 22: FE Hamming code error in word0, word1, word2
	    // bit 23: FE L1_in counter error, L1 request error, L1 register error, L1 trigger ID error
	    // bit 24: FE readout processor error
	    // bit 25: FE triple redundant mismatch
	    // bit 26: FE write register data error
	    // bit 27: FE address error
	    // bit 28: FE command decoder error
	    // bit 29: FE SEU upset detected in command decoder
	    // bit 30: FE data bus address error
	    // bit 31: FE triple redundant mismatch


	    if ((pixel->rod[kRod].rodStatusWord1 & (1<<0))!=0){ // BCID error
	      detector_IblDbm_ROD_errors->Fill(rodname_number->second,0);
	      detector_IblDbm_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,0);
	      fillTimeHisto(detector_ROD_syncherrors,timeIntervals_ROD_syncherrors,rod_number,1);
	      is_rod_BCID_errors[rod_number]++;
	    }
	    if ((pixel->rod[kRod].rodStatusWord1 & (1<<1))!=0){ // LVL1ID error
	      detector_IblDbm_ROD_errors->Fill(rodname_number->second,1);
	      detector_IblDbm_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,1);
	      fillTimeHisto(detector_ROD_syncherrors,timeIntervals_ROD_syncherrors,rod_number,1);
	      is_rod_LVL1ID_errors[rod_number]++;
	    }
	    if ((pixel->rod[kRod].rodStatusWord1 & (1<<2))!=0){ // Readout Timeout Error
	      detector_IblDbm_ROD_errors->Fill(rodname_number->second,2);
	      detector_IblDbm_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,2);
	      fillTimeHisto(detector_ROD_timeouterrors,timeIntervals_ROD_timeouterrors,rod_number,1);
	      is_rod_timeout_errors[rod_number]++;
	    }
	    if ((pixel->rod[kRod].rodStatusWord1 & (1<<3))!=0){ // Logical OR of bits 31 downto 14
	      detector_IblDbm_ROD_errors->Fill(rodname_number->second,3);
	      detector_IblDbm_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,3);
	      is_rod_dataincorrect_errors[rod_number]++;
	    }
	    if ((pixel->rod[kRod].rodStatusWord1 & (1<<4))!=0){ // Logical OR of bits 14 and 15
	      detector_IblDbm_ROD_errors->Fill(rodname_number->second,4);
	      detector_IblDbm_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,4);
	      fillTimeHisto(detector_ROD_trunchighoccerrors,timeIntervals_ROD_trunchighoccerrors,rod_number,1);
	      //is_rod_overflow_errors[rod_number]++;
	    }
	    if ((pixel->rod[kRod].rodStatusWord1 & (1<<16))!=0){ // Non-sequential order between the FEs data
	      detector_IblDbm_ROD_errors->Fill(rodname_number->second,5);
	      detector_IblDbm_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,5);
	      fillTimeHisto(detector_ROD_trunchighoccerrors,timeIntervals_ROD_trunchighoccerrors,rod_number,1);
	    }
	    if ((pixel->rod[kRod].rodStatusWord1 & (1<<17))!=0){ // Preamble error in the FE (header not starting with E9)
	      detector_IblDbm_ROD_errors->Fill(rodname_number->second,6);
	      detector_IblDbm_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,6);
	      fillTimeHisto(detector_ROD_trunchighoccerrors,timeIntervals_ROD_trunchighoccerrors,rod_number,1);
	    }
	    if ((pixel->rod[kRod].rodStatusWord1 & (1<<18))!=0){ // Raw data (inherited from Pixel..but not really meaninful here)
	      detector_IblDbm_ROD_errors->Fill(rodname_number->second,7);
	      detector_IblDbm_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,7);
	      fillTimeHisto(detector_ROD_transerrors,timeIntervals_ROD_transerrors,rod_number,1);
	    }
	    if ((pixel->rod[kRod].rodStatusWord1 & (1<<19))!=0){ // Invalid row/column.
	      detector_IblDbm_ROD_errors->Fill(rodname_number->second,8);
	      detector_IblDbm_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,8);
	      fillTimeHisto(detector_ROD_transerrors,timeIntervals_ROD_transerrors,rod_number,1);
	    }
	    if ((pixel->rod[kRod].rodStatusWord1 & (1<<20))!=0){ // There are skipped triggers in the FEs (at least one skipped trigger).
	      detector_IblDbm_ROD_errors->Fill(rodname_number->second,9);
	      detector_IblDbm_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,9);
	      fillTimeHisto(detector_ROD_transerrors,timeIntervals_ROD_transerrors,rod_number,1);
	    }
	    if ((pixel->rod[kRod].rodStatusWord1 & (1<<21))!=0){ // FE BCID counter error
	      detector_IblDbm_ROD_errors->Fill(rodname_number->second,10);
	      detector_IblDbm_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,10);
	      fillTimeHisto(detector_ROD_trunchighoccerrors,timeIntervals_ROD_trunchighoccerrors,rod_number,1);
	    }
	    if ((pixel->rod[kRod].rodStatusWord1 & (1<<22))!=0){ // FE Hamming code error in word0, word1, word2
	      detector_IblDbm_ROD_errors->Fill(rodname_number->second,11);
	      detector_IblDbm_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,11);
	      fillTimeHisto(detector_ROD_trunchighoccerrors,timeIntervals_ROD_trunchighoccerrors,rod_number,1);
	    }
	    if ((pixel->rod[kRod].rodStatusWord1 & (1<<23))!=0){ // FE L1_in counter error, L1 request error, L1 register error, L1 trigger ID error
	      detector_IblDbm_ROD_errors->Fill(rodname_number->second,12);
	      detector_IblDbm_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,12);
	      fillTimeHisto(detector_ROD_SEUerrors,timeIntervals_ROD_SEUerrors,rod_number,1);
	    }
	    if ((pixel->rod[kRod].rodStatusWord1 & (1<<24))!=0){ // FE readout processor error
	      detector_IblDbm_ROD_errors->Fill(rodname_number->second,13);
	      detector_IblDbm_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,13);
	      fillTimeHisto(detector_ROD_SEUerrors,timeIntervals_ROD_SEUerrors,rod_number,1);
	    }
	    if ((pixel->rod[kRod].rodStatusWord1 & (1<<25))!=0){ // FE triple redundant mismatch
	      detector_IblDbm_ROD_errors->Fill(rodname_number->second,14);
	      detector_IblDbm_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,14);
	      fillTimeHisto(detector_ROD_SEUerrors,timeIntervals_ROD_SEUerrors,rod_number,1);
	    }
	    if ((pixel->rod[kRod].rodStatusWord1 & (1<<26))!=0){ // FE write register data error
	      detector_IblDbm_ROD_errors->Fill(rodname_number->second,15);
	      detector_IblDbm_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,15);
	      fillTimeHisto(detector_ROD_SEUerrors,timeIntervals_ROD_SEUerrors,rod_number,1);
	    }
	    if ((pixel->rod[kRod].rodStatusWord1 & (1<<27))!=0){ // FE address error
	      detector_IblDbm_ROD_errors->Fill(rodname_number->second,16);
	      detector_IblDbm_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,16);
	      fillTimeHisto(detector_ROD_trunchighoccerrors,timeIntervals_ROD_trunchighoccerrors,rod_number,1);
	    }
	    if ((pixel->rod[kRod].rodStatusWord1 & (1<<28))!=0){ // FE command decoder error
	      detector_IblDbm_ROD_errors->Fill(rodname_number->second,17);
	      detector_IblDbm_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,17);
	      fillTimeHisto(detector_ROD_trunchighoccerrors,timeIntervals_ROD_trunchighoccerrors,rod_number,1);
	    }
	    if ((pixel->rod[kRod].rodStatusWord1 & (1<<29))!=0){ // FE SEU upset detected in command decoder
	      detector_IblDbm_ROD_errors->Fill(rodname_number->second,18);
	      detector_IblDbm_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,18);
	      fillTimeHisto(detector_ROD_syncherrors,timeIntervals_ROD_syncherrors,rod_number,1);
	    }
	    if ((pixel->rod[kRod].rodStatusWord1 & (1<<30))!=0){ // FE data bus address error
	      detector_IblDbm_ROD_errors->Fill(rodname_number->second,19);
	      detector_IblDbm_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,19);
	      fillTimeHisto(detector_ROD_syncherrors,timeIntervals_ROD_syncherrors,rod_number,1);
	    }
	    if ((pixel->rod[kRod].rodStatusWord1 & (1<<31))!=0){ // FE triple redundant mismatch
	      detector_IblDbm_ROD_errors->Fill(rodname_number->second,20);
	      detector_IblDbm_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,20);
	      fillTimeHisto(detector_ROD_syncherrors,timeIntervals_ROD_syncherrors,rod_number,1);
	    }
	    
	  } else { // not a 3lp or IblDbm ROD!
	    ERS_PIX_WARNING( "corresponding ROD for " << rod_name << " not found, this should not happen" , MapError);
	    cout << "WARNING: corresponding ROD for " << rod_name << " not found, this should not happen" << endl;
	  }
	}

      } else {
	ERS_PIX_WARNING( "invalid ROD found for source ID: 0x" << std::hex << pixel->rod[kRod].rodRoLink << std::dec , MapError);
	cout << "WARNING: invalid ROD found for source ID: 0x" << std::hex << pixel->rod[kRod].rodRoLink << std::dec << endl;
      }
    }

    if (pixel->rod[kRod].rodStatusWord2 !=0){ 

      map <int,int>::iterator RODLogMap_iterator = RODLogMap.find(pixel->rod[kRod].rodRoLink);
      if (RODLogMap_iterator != RODLogMap.end()){
	uint rod_number = RODLogMap_iterator->second;
	if(rod_number >= MAX_ROD_PER_ROS){
	  ERS_PIX_ERROR("rod_number="<<rod_number<<" >= MAX_ROD_PER_ROS("<<MAX_ROD_PER_ROS<<"): " << "this should never happen.",MapError);
	  cout <<"ERROR: rod_number="<<rod_number<<" out of range...  Try to continue to next rod..."<<endl;
	  continue;
	}
	string rod_name = RODGeoMap[rod_number];

	map<string, int>::iterator rodname_number = rodnames_3lp_map.find(rod_name);
	if (rodname_number != rodnames_3lp_map.end()){

	  if ((pixel->rod[kRod].rodStatusWord2 & (1<<16))!=0){ // TIM Clock error
	    detector_3lp_ROD_errors->Fill(rodname_number->second,21);
	    detector_3lp_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,21);
	    fillTimeHisto(detector_ROD_DAQerrors,timeIntervals_ROD_DAQerrors,rod_number,1);
	  }
	  if ((pixel->rod[kRod].rodStatusWord2 & (1<<17))!=0){ // BOC Clock error
	    detector_3lp_ROD_errors->Fill(rodname_number->second,22);
	    detector_3lp_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,22);
	    fillTimeHisto(detector_ROD_DAQerrors,timeIntervals_ROD_DAQerrors,rod_number,1);
	  }
	  if ((pixel->rod[kRod].rodStatusWord2 & (1<<18))!=0){ // ROL Test Block Indicator
	    detector_3lp_ROD_errors->Fill(rodname_number->second,23);
	    detector_3lp_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,23);
	    fillTimeHisto(detector_ROD_DAQerrors,timeIntervals_ROD_DAQerrors,rod_number,1);
	  }

	} else {
	  rodname_number = rodnames_IblDbm_map.find(rod_name);
	  if (rodname_number != rodnames_IblDbm_map.end()){

	    //// IBL ROD StatusWord2 Errors:
	    // bits 31-18:  all zeros
	    // bit 17: BOC clock error
	    // bit 16: TIM clock error
	    // bits 15-0: number of errors counted in this event

	    if ((pixel->rod[kRod].rodStatusWord2 & (1<<16))!=0){ // TIM Clock error
	      detector_IblDbm_ROD_errors->Fill(rodname_number->second,21);
	      detector_IblDbm_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,21);
	      fillTimeHisto(detector_ROD_DAQerrors,timeIntervals_ROD_DAQerrors,rod_number,1);
	    }
	    if ((pixel->rod[kRod].rodStatusWord2 & (1<<17))!=0){ // BOC Clock error
	      detector_IblDbm_ROD_errors->Fill(rodname_number->second,22);
	      detector_IblDbm_ROD_errorsvsBCID->Fill(pixel->rod[kRod].bunchCross,22);
	      fillTimeHisto(detector_ROD_DAQerrors,timeIntervals_ROD_DAQerrors,rod_number,1);
	    }
	    
	  } else {
	    ERS_PIX_WARNING( "corresponding ROD for " << rod_name << " not found, this should not happen" , MapError);
	    cout << "WARNING: corresponding ROD for " << rod_name << " not found, this should not happen" << endl;
	  }
	}

      } else {
	ERS_PIX_WARNING( "invalid ROD found for source ID: 0x" << std::hex << pixel->rod[kRod].rodRoLink << std::dec , MapError);
	cout << "WARNING: invalid ROD found for source ID: 0x" << std::hex << pixel->rod[kRod].rodRoLink << std::dec << endl;
      }
    }
    
  }
  
  for (uint kMod=0; kMod < pixel->NMods; ++kMod){ // loop over all modules and find the header error flags

    int  link = pixel->mod[kMod].linkNumber; // linknumber of this module
    uint kRod = pixel->mod[kMod].inRod; // ROD of the module
    int  source_id = pixel->rod[kRod].rodRoLink; // source_id of the ROL
    ModMap_Key = make_pair(source_id,link);

    // map<pair<int,int>, string>::iterator disabled_it = disabled_modules_names.find(ModMap_Key);

    /*
      if (disabled_it != disabled_modules_names.end()) // check if module is disabled (this should not happen)
      msg_gnam->publishMessage(PixMessages::WARNING, applName, "Disabled module " + disabled_it->second + " sending headers"); 
    */ //this should go back in once it is clear, why disabled modules seem to send headers
      
    ModSourceMap_iterator = ModSourceMap.find(ModMap_Key);
    // MPG: removed check on ModSourceMap_iterator, added it for
    //     ModLogMap_iterator
    // FH: added check again, otherwise crash
    // ModMap_Key = make_pair(source_id,link);
    // ModLogMap_iterator = ModLogMap.find(ModMap_Key);
    if (ModSourceMap_iterator!=ModSourceMap.end()){
      ModLogMap_iterator = ModLogMap.find(ModMap_Key);
      if (ModLogMap_iterator!=ModLogMap.end()){

	uint rod_number = ((ModLogMap_iterator->second))[0];
	uint ob_number = ((ModLogMap_iterator->second))[1];
	uint mod_number = ((ModLogMap_iterator->second))[2];
	if(rod_number >= MAX_ROD_PER_ROS){
	  ERS_PIX_ERROR("rod_number="<<rod_number<<" >= MAX_ROD_PER_ROS("<<MAX_ROD_PER_ROS<<"): " << "this should never happen.",MapError);
	  cout <<"ERROR: rod_number="<<rod_number<<" out of range...  Try to continue to next mod..."<<endl;
	  continue;
	}
	if(ob_number >= MAX_OB_PER_ROD){
	  ERS_PIX_ERROR("ob_number="<<ob_number<<" >= MAX_OB_PER_ROD("<<MAX_OB_PER_ROD<<"): " << "this should never happen.",MapError);
	  cout <<"ERROR: ob_number="<<ob_number<<" out of range...  Try to continue to next mod..."<<endl;
	  continue;
	}
	if(mod_number > MAX_MOD_PER_OB){
	  ERS_PIX_ERROR("mod_number="<<mod_number<<" > MAX_MOD_PER_OB("<<MAX_MOD_PER_OB<<"): " << "this should never happen.",MapError);
	  cout <<"ERROR: mod_number="<<mod_number<<" out of range...  Try to continue to next mod..."<<endl;
	  continue;
	}

	//.. Fill number of headers per event
	// std::stringstream entry; 
	// entry << "filling status: rod number " << rod_number 
	//       << " opto number " << ob_number << " mod number " << mod_number; 
	// ERS_PIX_INFO(entry.str());
	//try {
	module_status[rod_number][ob_number][mod_number-1]->Fill(0);
	module_level1Skips[rod_number][ob_number][mod_number-1]->Fill(pixel->mod[kMod].level1Skips);
	global_level1Skips->Fill(pixel->mod[kMod].level1Skips);
	module_level1Id[rod_number][ob_number][mod_number-1]->Fill(pixel->mod[kMod].level1Id);
	global_level1Id->Fill(pixel->mod[kMod].level1Id);

	module_numHits[rod_number][ob_number][mod_number-1]->Fill(pixel->mod[kMod].numHits);
	//} catch (...) {
	//}
	// ERS_PIX_INFO("status filled");
	// IS error variables
	
	// MPG: modified argument (mod_number->mod_number-1) since mod_number runs from 1-MAX_MOD_PER_OB (Beate: really imprortant as array has MAX_MOD_PER_OB elements, 0 -- MAX_MOD_PER_OB-1)
	if (pixel->mod[kMod].timeOutError){
	  is_mod_timeout_errors[rod_number][ob_number][mod_number-1]++;
	  local_errorsum[rod_number][ob_number][mod_number-1]++;
	}
	if (pixel->mod[kMod].level1Error){
	  is_mod_LVL1ID_errors[rod_number][ob_number][mod_number-1]++;
	  local_errorsum[rod_number][ob_number][mod_number-1]++;
	  module_LVL1IDErrors[rod_number][ob_number][mod_number-1]->Fill(1);
	  if(isIblOrDbm(source_id)){ // count Level 1 ID erros for IBL & DBM
	    l_module_LVL1IDErrors[rod_number][ob_number][mod_number-1].push_back(RATE_CurrTime);	    
	  }
	}
	if (pixel->mod[kMod].bunchCrossError){
	  is_mod_BCID_errors[rod_number][ob_number][mod_number-1]++;
	  local_errorsum[rod_number][ob_number][mod_number-1]++;
	  if(!isIblOrDbm(source_id)){ // count bunch crossing ID erros for 3lp
	    l_module_LVL1IDErrors[rod_number][ob_number][mod_number-1].push_back(RATE_CurrTime);
	  }
	}
	if (pixel->mod[kMod].trailerError){
	  is_mod_trailer_errors[rod_number][ob_number][mod_number-1]++;
	  local_errorsum[rod_number][ob_number][mod_number-1]++;
	}
	if (pixel->mod[kMod].limitError){
	  is_mod_limit_errors[rod_number][ob_number][mod_number-1]++;
	  local_errorsum[rod_number][ob_number][mod_number-1]++;
	}
	if (pixel->mod[kMod].overFlow){
	  is_mod_overflow_errors[rod_number][ob_number][mod_number-1]++;
	  local_errorsum[rod_number][ob_number][mod_number-1]++;
	}
      }
      else{ 
	if (PrintOutLevel>=1){
	  cout << "WARNING: No connectivity information for source_id " << hex << source_id << dec << " link " << link << ", ignoring Hit" << endl;
	}
      }
    }
    else{ 
      if (PrintOutLevel>=1){
	cout << "WARNING: No connectivity information for source_id " << hex << source_id << dec << " link " << link << ", ignoring Hit" << endl;
      }
    }
  }

  for (uint kErr=0; kErr < pixel->NFlag; ++kErr){ // loop over all FE flags and fill them

    uint kMod = pixel->fee[kErr].inMod; // find the module that collected the hit
    if(kMod>=pixel->NMods){
      ERS_PIX_WARNING( "kMod(" << dec << kMod << ") >= pixel->NMods(" << pixel->NMods << ")", MapError);
      cout << "WARNING: kMod(" << dec << kMod << ") >= pixel->NMods(" << pixel->NMods << ")... Skip and try to continue..." << endl;
      continue;
    }

    uint kRod = pixel->mod[kMod].inRod; // ROD of the module
    if(kRod>=pixel->NRods){
      ERS_PIX_WARNING( "kRod(" << dec << kRod << ") >= pixel->NRods(" << pixel->NRods << ")", MapError);
      cout << "WARNING: kRod(" << dec << kRod << ") >= pixel->NRods(" << pixel->NRods << ")... Skip and try to continue..." << endl;
      continue;
    }

    int  link = pixel->mod[kMod].linkNumber; // linknumber of this module
    int  source_id = pixel->rod[kRod].rodRoLink; // source_id of the ROL

    ModMap_Key = make_pair(source_id,link);
    ModSourceMap_iterator = ModSourceMap.find(ModMap_Key);
    // MPG: removed check on ModSourceMap_iterator, added it for
    //     ModLogMap_iterator
    // FH: added check again, otherwise crash
    // ModMap_Key = make_pair(source_id,link);
    // ModLogMap_iterator = ModLogMap.find(ModMap_Key);
    if (ModSourceMap_iterator!=ModSourceMap.end()){
      ModLogMap_iterator = ModLogMap.find(ModMap_Key);
      if (ModLogMap_iterator!=ModLogMap.end()){
	ModLogMap_iterator = ModLogMap.find(ModMap_Key);

	uint rod_number = ((ModLogMap_iterator->second))[0];
	uint ob_number = ((ModLogMap_iterator->second))[1];
	uint mod_number = ((ModLogMap_iterator->second))[2];
	if(rod_number >= MAX_ROD_PER_ROS){
	  ERS_PIX_ERROR("rod_number="<<rod_number<<" >= MAX_ROD_PER_ROS("<<MAX_ROD_PER_ROS<<"): " << "this should never happen.",MapError);
	  cout <<"ERROR: rod_number="<<rod_number<<" out of range...  Try to continue to next mod..."<<endl;
	  continue;
	}
	if(ob_number >= MAX_OB_PER_ROD){
	  ERS_PIX_ERROR("ob_number="<<ob_number<<" >= MAX_OB_PER_ROD("<<MAX_OB_PER_ROD<<"): " << "this should never happen.",MapError);
	  cout <<"ERROR: ob_number="<<ob_number<<" out of range...  Try to continue to next mod..."<<endl;
	  continue;
	}
	if(mod_number > MAX_MOD_PER_OB){
	  ERS_PIX_ERROR("mod_number="<<mod_number<<" > MAX_MOD_PER_OB("<<MAX_MOD_PER_OB<<"): " << "this should never happen.",MapError);
	  cout <<"ERROR: mod_number="<<mod_number<<" out of range...  Try to continue to next mod..."<<endl;
	  continue;
	}
	
	uint FE_error = pixel->fee[kErr].FEerrorCode;
	uint MCC_error = pixel->fee[kErr].MCCerrorCode;
	
	if(!pixel->fee[kErr].IblError){
	  // For Pixels
	  is_mod_FEEOC_errors[rod_number][ob_number][mod_number-1] += (FE_error & 0x1);
	  is_mod_FEHamming_errors[rod_number][ob_number][mod_number-1] += ((FE_error & 0x2) >> 1);
	  is_mod_FEregparity_errors[rod_number][ob_number][mod_number-1] += ((FE_error & 0x4) >> 2);
	  is_mod_FEhitparity_errors[rod_number][ob_number][mod_number-1] += ((FE_error & 0x8) >> 3);
	  is_mod_MCChitoverflow_errors[rod_number][ob_number][mod_number-1] += (MCC_error & 0x1);
	  is_mod_MCCEoEoverflow_errors[rod_number][ob_number][mod_number-1] += ((MCC_error & 0x2) >> 1);
	  is_mod_MCCL1ID_errors[rod_number][ob_number][mod_number-1] += ((MCC_error & 0x4) >> 2);
	  is_mod_MCCBCIDEoE_errors[rod_number][ob_number][mod_number-1] += ((MCC_error & 0x8) >> 3);
	  is_mod_MCCL1IDEoE_errors[rod_number][ob_number][mod_number-1] += ((MCC_error & 0x10) >> 4);
	  
	  // MPG: added check on array bounds
	  // BH changed slightly to also take ROS number 
	  // IS error variables
	  
	  // MPG: modified argument (mod_number->mod_number-1) since mod_number runs from 1-MAX_MOD_PER_OB (Beate: really imprortant as array has MAX_MOD_PER_OB elements, 0 -- MAX_MOD_PER_OB-1)
	  for (int i = 0; i<8; ++i){ // check each of the 8 bits
	    if ((FE_error >> i) & 1)
	      local_errorsum[rod_number][ob_number][mod_number-1]++;
	    if ((MCC_error >> i) & 1)
	      local_errorsum[rod_number][ob_number][mod_number-1]++;
	  }
	}
	else{ // This is an IBL/DBM error
	  uint32_t IblFEServiceCode = pixel->fee[kErr].IblFEerrorNumber;
	  uint32_t IblFEerrorWeight = pixel->fee[kErr].IblFEerrorWeight; // DC: trying to use the weight
	  //uint32_t IblFEerrorWeight = 1; // Only increment by 1 per code

	  local_errorsum[rod_number][ob_number][mod_number-1] += IblFEerrorWeight; // increment global counter

          //if (FEerrCout++ < 500) std::cout << "IBL FE Service Code: " << IblFEServiceCode << " Error Weight: " << IblFEerrorWeight << std::endl; 

	  if (IblFEServiceCode != 14){ // DC: We don't want "3 MSBs of bunch counter and 7 MSBs of L1A counter" info in the histogram
              layerIblDbm_FEServiceCode->Fill(IblFEServiceCode, IblFEerrorWeight);
              // Diane - go on next time possible
              // if(isIbl(source_id)){
              //     std::cout << "test ibl: " << isIbl(source_id) << " source: " << source_id << std::endl;
              //     layerIblDbm_FEServiceCode->Fill(IblFEServiceCode, IblFEerrorWeight);
              // }
              // else{
              //     std::cout << "test dbm: " << isIbl(source_id) << " source: " << source_id << std::endl;
              //     layerDbm_FEServiceCode->Fill(IblFEServiceCode, IblFEerrorWeight);
              // }
          }
	  
	  // Individual counters, just a few implemented to start.
	  switch(IblFEServiceCode){
	  case 0: // BCID counter error
	    layerIblDbm_FEerrors->Fill(0, IblFEerrorWeight); // DC: fix histo fill, was starting at 1 before fix
            layerIblDbm_FEerrors_notweighted->Fill(0, 1); // DC: adding one histogram with no weight to compare to layerIblDbm_FEerrors
            is_mod_IblDbmFEBCID_errors[rod_number][ob_number][mod_number-1] += IblFEerrorWeight;
	    break;
	  case 1: // Hamming code error in word 0
	    layerIblDbm_FEerrors->Fill(1, IblFEerrorWeight);
            layerIblDbm_FEerrors_notweighted->Fill(1, 1);
	    is_mod_IblDbmFEHamming_errors[rod_number][ob_number][mod_number-1] += IblFEerrorWeight;
	    break;
	  case 2: // Hamming code error in word 1
	    layerIblDbm_FEerrors->Fill(2, IblFEerrorWeight);
            layerIblDbm_FEerrors_notweighted->Fill(2, 1);
	    is_mod_IblDbmFEHamming_errors[rod_number][ob_number][mod_number-1] += IblFEerrorWeight;
	    break;
	  case 3: // Hamming code error in word 2
	    layerIblDbm_FEerrors->Fill(3, IblFEerrorWeight);
            layerIblDbm_FEerrors_notweighted->Fill(3, 1);
	    is_mod_IblDbmFEHamming_errors[rod_number][ob_number][mod_number-1] += IblFEerrorWeight;
	    break;
	  case 4: // L1_in counter error
	    layerIblDbm_FEerrors->Fill(4, IblFEerrorWeight);
            layerIblDbm_FEerrors_notweighted->Fill(4, 1);
	    is_mod_IblDbmFEL1_errors[rod_number][ob_number][mod_number-1] += IblFEerrorWeight;
	    break;
	  case 5: // L1 request counter error
	    layerIblDbm_FEerrors->Fill(5, IblFEerrorWeight);
            layerIblDbm_FEerrors_notweighted->Fill(5, 1);
	    is_mod_IblDbmFEL1_errors[rod_number][ob_number][mod_number-1] += IblFEerrorWeight;
	    break;
	  case 6: // L1 register error
	    layerIblDbm_FEerrors->Fill(6, IblFEerrorWeight);
            layerIblDbm_FEerrors_notweighted->Fill(6, 1);
	    is_mod_IblDbmFEL1_errors[rod_number][ob_number][mod_number-1] += IblFEerrorWeight;
	    break;
	  case 7: // L1 Trigger ID error
	    layerIblDbm_FEerrors->Fill(7, IblFEerrorWeight);
            layerIblDbm_FEerrors_notweighted->Fill(7, 1);
	    is_mod_IblDbmFEL1_errors[rod_number][ob_number][mod_number-1] += IblFEerrorWeight;
	    break;
	  case 8: // readout processor error
	    layerIblDbm_FEerrors->Fill(8, IblFEerrorWeight);
            layerIblDbm_FEerrors_notweighted->Fill(8, 1);
	    break;
	  case 9: // Fifo_Full flag pulsed
	    break;
	  case 10: // HitOr bus pulsed
	    break;
	  case 11: // not used
	  case 12: // not used
	  case 13: // not used
	    break;
	  case 14: // 3 MSBs of bunch counter and 7 MSBs of L1A counter
	    break;
	  case 15: // Skipped trigger counter
	    layerIblDbm_FEerrors->Fill(9, IblFEerrorWeight);
            layerIblDbm_FEerrors_notweighted->Fill(9, 1);
	    break;
	  case 16: // Truncated event flag and counter
	    layerIblDbm_FEerrors->Fill(10, IblFEerrorWeight);
            layerIblDbm_FEerrors_notweighted->Fill(10, 1);
	    break;
	  case 17: // not used
	    break;
	  case 18: // not used
	    break;
	  case 19: // not used
	    break;
	  case 20: // not used
	    break;
	  case 21: // Reset bar RA2b pulsed
	    break;
	  case 22: // PLL generated clock phase faster than reference
	    break;
	  case 23: // Reference clock phase faster than PLL
	    break;
	  case 24: // Triple redundant mismatch
	    layerIblDbm_FEerrors->Fill(11, IblFEerrorWeight);
            layerIblDbm_FEerrors_notweighted->Fill(11, 1);
	    break;
	  case 25: // Write register data error
	    layerIblDbm_FEerrors->Fill(12, IblFEerrorWeight);
            layerIblDbm_FEerrors_notweighted->Fill(12, 1);
	    break;
	  case 26: // Address error
	    layerIblDbm_FEerrors->Fill(13, IblFEerrorWeight);
            layerIblDbm_FEerrors_notweighted->Fill(13, 1);
	    break;
	  case 27: // Other command decoder error- see CMD section
	    layerIblDbm_FEerrors->Fill(14, IblFEerrorWeight);
            layerIblDbm_FEerrors_notweighted->Fill(14, 1);
	    break;
	  case 28: // Bit flip detected in command decoder input stream
	    layerIblDbm_FEerrors->Fill(15, IblFEerrorWeight);
            layerIblDbm_FEerrors_notweighted->Fill(15, 1);
	    break;
	  case 29:// SEU upset detected in command decoder (triple redundant mismatch)
	    layerIblDbm_FEerrors->Fill(16, IblFEerrorWeight);
            layerIblDbm_FEerrors_notweighted->Fill(16, 1);
	    break;
	  case 30: // Data bus address error
	    layerIblDbm_FEerrors->Fill(17, IblFEerrorWeight);
            layerIblDbm_FEerrors_notweighted->Fill(17, 1);
	    break;
	  case 31: // Triple redundant mismatch
	    layerIblDbm_FEerrors->Fill(18, IblFEerrorWeight);
            layerIblDbm_FEerrors_notweighted->Fill(18, 1);
	    break;
	  }
	  
	}
      }
      else{ 
	if (PrintOutLevel>=1){
	  cout << "WARNING: No connectivity information for source_id " << hex << source_id << dec << " link " << link << ", ignoring Hit" << endl;
	}
      }
    } 
    else{ 
      if (PrintOutLevel>=1){
	cout << "WARNING: No connectivity information for source_id " << hex << source_id << dec << " link " << link << ", ignoring Hit" << endl;
      }
    }
  }

  // update global error counters
  for (uint rod=0; rod < MAX_ROD_PER_ROS; ++rod){
    for (uint ob=0; ob < MAX_OB_PER_ROD; ++ob){
      for (uint mod=0; mod < MAX_MOD_PER_OB; ++mod){
	if( local_errorsum[rod][ob][mod] ){
	  global_errorany[rod][ob][mod]++;
	  global_errorsum[rod][ob][mod] += local_errorsum[rod][ob][mod];
	}
      }
    }
  }
  

  //------------------------------------------------------
  // fill the hit information to corresponding histograms
  //------------------------------------------------------
  // loop over all hits stored in the PixelEvent class
  for (uint kHit=0; kHit<pixel->NHits; ++kHit){

    uint kMod = pixel->hit[kHit].inMod; // find the module that collected the hit
    if(kMod>=pixel->NMods){
      ERS_PIX_WARNING( "kMod(" << dec << kMod << ") >= pixel->NMods(" << pixel->NMods << ")", MapError);
      cout << "WARNING: kMod(" << dec << kMod << ") >= pixel->NMods(" << pixel->NMods << ")... Skip and try to continue..." << endl;
      continue;
    }

    uint kRod = pixel->mod[kMod].inRod; // ROD of the module
    if(kRod>=pixel->NRods){
      ERS_PIX_WARNING( "kRod(" << dec << kRod << ") >= pixel->NRods(" << pixel->NRods << ")", MapError);
      cout << "WARNING: kRod(" << dec << kRod << ") >= pixel->NRods(" << pixel->NRods << ")... Skip and try to continue..." << endl;
      continue;
    }

    int  link = pixel->mod[kMod].linkNumber; // linknumber of this module
    int  source_id = pixel->rod[kRod].rodRoLink; // source_id of the ROL

    ModMap_Key = make_pair(source_id,link);
    ModSourceMap_iterator = ModSourceMap.find(ModMap_Key);
    // MPG: removed check on ModSourceMap_iterator, added it for
    //     ModLogMap_iterator
    // FH: added check again, otherwise crash
    // ModMap_Key = make_pair(source_id,link);
    // ModLogMap_iterator = ModLogMap.find(ModMap_Key);
    if (ModSourceMap_iterator!=ModSourceMap.end()){
      ModLogMap_iterator = ModLogMap.find(ModMap_Key);
      if (ModLogMap_iterator!=ModLogMap.end()){

	// Get information from the map
	uint rod_number = ((ModLogMap_iterator->second))[0];
	uint ob_number = ((ModLogMap_iterator->second))[1];
	uint mod_number = ((ModLogMap_iterator->second))[2];
	if(rod_number >= MAX_ROD_PER_ROS){
	  ERS_PIX_ERROR("rod_number="<<rod_number<<" >= MAX_ROD_PER_ROS("<<MAX_ROD_PER_ROS<<"): " << "this should never happen.",MapError);
	  cout <<"ERROR: rod_number="<<rod_number<<" out of range...  Try to continue to next mod..."<<endl;
	  continue;
	}
	if(ob_number >= MAX_OB_PER_ROD){
	  ERS_PIX_ERROR("ob_number="<<ob_number<<" >= MAX_OB_PER_ROD("<<MAX_OB_PER_ROD<<"): " << "this should never happen.",MapError);
	  cout <<"ERROR: ob_number="<<ob_number<<" out of range...  Try to continue to next mod..."<<endl;
	  continue;
	}
	if(mod_number > MAX_MOD_PER_OB){
	  ERS_PIX_ERROR("mod_number="<<mod_number<<" > MAX_MOD_PER_OB("<<MAX_MOD_PER_OB<<"): " << "this should never happen.",MapError);
	  cout <<"ERROR: mod_number="<<mod_number<<" out of range...  Try to continue to next mod..."<<endl;
	  continue;
	}
	
	mod_name = ModSourceMap_iterator->second; // ... 
	
	// Get hit data
	uint kRow = pixel->hit[kHit].pixelRow;
	uint kCol = pixel->hit[kHit].pixelCol;
	uint kFE = pixel->hit[kHit].FEnumber;

	// Fill the expert histograms for simulator mode

	FE_vs_L1ID->Fill(pixel->mod[kMod].level1Id, kFE);
	TOT_vs_BCID->Fill(pixel->mod[kMod].bunchCrossId,pixel->hit[kHit].pixelToT);
	ROD_FE_vs_L1ID[rod_number]->Fill(pixel->mod[kMod].level1Id, kFE);
	ROD_TOT_vs_BCID[rod_number]->Fill(pixel->mod[kMod].bunchCrossId,pixel->hit[kHit].pixelToT);
	
	// fill module level histogram
	module_ToT[rod_number][ob_number][mod_number-1]->Fill(pixel->hit[kHit].pixelToT);

	ERS_PIX_DEBUG(1,"Found hit - Row = " << showbase << dec << kRow << ", Column = " << kCol << ", FE = " << kFE);

	//// Translate from FE row/column to module row/column ////
	// Calculate right row and column number with regard to the frontend number
	if(!isIblOrDbm(source_id)){ // Pixel module
	  if (kFE<8){
	    kCol = kCol+kFE*18;
	  } else {
	    kCol = 143-(kCol+(kFE-8)*18);
	    kRow = 319-kRow;
	  }
	}
	
	ERS_PIX_DEBUG(1,"Calculated Hitmap position - Row = " << showbase << dec << kRow << ", Column = " << kCol);
	
	detector_tot->Fill(pixel->hit[kHit].pixelToT);
	detector_time->Fill(pixel->mod[kMod].timeSlot);
	
	// Single Module histogram filling, only entered if a least one module is enabled for filling
	if (fill_single_modules){
	  map<string,int>::iterator ModDynMap_iterator=ModDynMap.find(mod_name);
	  if (ModDynMap_iterator!=ModDynMap.end()){

	    uint histo_number = ModDynMap_iterator->second;
	    if(histo_number >= MAX_DYN_HISTO){
	      ERS_PIX_ERROR("histo_number="<<histo_number<<" >= MAX_DYN_HISTO("<<MAX_DYN_HISTO<<"): " << "this should never happen.",MapError);
	      cout <<"ERROR: histo_number="<<histo_number<<" out of range...  Setting it to MAX_DYN_HISTO - 1 ..."<<endl;
	      histo_number = MAX_DYN_HISTO - 1;
	    }
	    
	    module_hitmap[histo_number]->Fill(kCol,kRow);
	    module_hittime[histo_number]->Fill(pixel->mod[kMod].timeSlot);
	    module_hits[histo_number]->Fill(pixel->mod[kMod].numHits);
	    module_tot[histo_number]->Fill(pixel->hit[kHit].pixelToT);
	    module_bcid[histo_number]->Fill(pixel->mod[kMod].bunchCrossId);
	    
	    if (pixel->mod[kMod].preambleError!=0)
	      module_ercode[histo_number]->Fill(0);
	    if (pixel->mod[kMod].timeOutError!=0)
	      module_ercode[histo_number]->Fill(1);
	    if (pixel->mod[kMod].level1Error!=0){
	      //  		     module_lvl1id[histo_number]->Fill(1);
	      module_ercode[histo_number]->Fill(2);

	    }
	    if (pixel->mod[kMod].bunchCrossError!=0)
	      module_ercode[histo_number]->Fill(3);
	    if (pixel->mod[kMod].timeOut!=0)
	      module_ercode[histo_number]->Fill(20);
	    if (pixel->mod[kMod].trailerError!=0)
	      module_ercode[histo_number]->Fill(21);
	    if (pixel->mod[kMod].limitError!=0)
	      module_ercode[histo_number]->Fill(22);
	    if (pixel->mod[kMod].overFlow!=0)
	      module_ercode[histo_number]->Fill(23);
	  }
	}

	int ibl_edge = ibl_edge_check(mod_name,kCol);
	if( ibl_edge !=0 ){
	  ibl_edge_hitmap->Fill(ibl_edge, get_ibl_stavenumber(mod_name)-1);
	}
	
	// logical mapping of ROD/OB/Mod
	detector_hitmap->Fill(m_ros_number*MAX_ROD_PER_ROS+rod_number,
			      ob_number*MAX_MOD_PER_OB+mod_number-1);
        module_nHitsSinceUpdate[rod_number][ob_number][mod_number-1]++;
      } else {
	if (errCout++ < 500) { // diane - tmp for log size
	  ERS_PIX_WARNING( "ModLogMap: No connectivity information for source_id = " << showbase << hex << source_id << dec << ", link = " << link << ". Ignoring hit.",ConfigError);
	  cout << "WARNING: ModLogMap: No connectivity information for source_id = " << showbase << hex << source_id << dec << ", link = " << link << ". Ignoring hit." << endl;
	  //cout << "ModSourceMap_iterator: " << ModSourceMap_iterator->second << " ModSourceMap.end(): " << ModSourceMap.end()->second << endl;
	}
      }
    } else {
      if (errCout++ < 500) { // diane - tmp for log size
	ERS_PIX_WARNING( "ModSourceMap: No connectivity information for source_id = " << showbase << hex << source_id << dec << ", link = " << link << ". Ignoring hit.",ConfigError);
	cout << "WARNING: ModSourceMap: No connectivity information for source_id = " << showbase << hex << source_id << dec << ", link = " << link << ". Ignoring hit." << endl;
	//cout << "ModSourceMap_iterator: " << ModSourceMap_iterator->second << " ModSourceMap.end(): " << ModSourceMap.end()->second << endl;
      }
    }
  }
  // fill the ToT histograms with clustering
  //DADA if (do_clustering) fill_cluster(pixel);
  if (do_clustering) fill_cluster();
 
 }
 catch(std::exception& e){
   cout<<"Exception caught in PixelHisto::fillHisto of type std::exception: " << e.what() << endl;
 }
 catch(char const* msg){
   cout<<"Exception caught in PixelHisto::fillHisto of type char const*: " << msg << endl;
 }
 catch(...){
   cout<<"Exception caught in PixelHisto::fillHisto of unknown type." << endl;
 }
}


/* ********** *
 * startOfRun *
 * ********** */

/* ***************************************************************** *
 * Called once every start of run; library is prepared for histogram *
 * filling. Argument "run" is the run number.                        *
 * ***************************************************************** */
extern "C" const std::vector<GnamHisto*> *
startOfRun(int run, std::string type)
{
 (void) run;
 (void) type;
 try {
  if (PrintOutLevel>=1){
    printf("PixelHisto: Entering startOfRun\n");
    fflush(stdout);
  }
  isStartOfRun=true;
  
  // This is just to have something in the log files to know what version of the code is running.
  int internal_code_version = 20150720; // set this to whatever you want
  cout << "PixelHisto internal code version " << internal_code_version << endl;

  //DADA: defined outside
  //static const PixelEvent *pixel = NULL;
  try {
    GNAM_BRANCH_FILL("PixelEvent",PixelEvent,pixel);
    //GNAM_BRANCH_FILL("PixelEvent",PixelEvent,static_cast<const PixelEvent*>(pixel));
  } catch(daq::gnamlib::NotExistingBranch &exc){
    throw daq::gnamlib::CannotFindBranch(ERS_HERE,"PixelEvent");
  }

  //DADA
  const_cast<PixelEvent*>(pixel)->subDetIdList.clear();
  // fill the connectivity here
  fillConnectivity();

  ERS_ASSERT(histolist==NULL);
  histolist = new vector<GnamHisto*>;

  if (PrintOutLevel>=1){
    printf("\tHistogram booking starting\n");
    printf("\thistolist = %p\n",(void*)histolist);
    fflush(stdout);
  }
  ERS_PIX_INFO("Histogram booking starting");
  ERS_PIX_DEBUG(1,"histolist = " << showbase << hex << histolist);
  if (histolist!=NULL){
    ERS_PIX_DEBUG(1,"histolist size = " << showbase << dec << histolist->size());
  }

  book_global_histograms();

  if (PrintOutLevel>=1){
    printf("histolist size after global histograms: %li\n",histolist->size());
  }
  if (histolist!=NULL){
    ERS_PIX_DEBUG(1,"histolist size = " << showbase << dec << histolist->size() << " (after global histograms)");
  }

  book_module_histograms();

  if (PrintOutLevel>=1){
    printf("\tsizeof(GnamHisto*) = %lu\n\n",sizeof(GnamHisto*));
    printf("histolist size after module histograms: %li\n",histolist->size());
  }
  if (histolist!=NULL){
    ERS_PIX_DEBUG(1,"histolist size = " << showbase << dec << histolist->size() << " (after module histograms)");
  }
  ERS_PIX_INFO("Histogram booking completed");
  if (PrintOutLevel>=1){
    printf("PixelHisto: Leaving startOfRun\n");
    fflush(stdout);
  }

  time(&startTime); // mark start of run
  RATE_LastTime=startTime;

  nEvents_thisUpdate=0;
  nEvents_lastUpdate=0;

  return histolist;

 }
 catch(std::exception& e){
   cout<<"Exception caught in PixelHisto::startOfRun of type std::exception: " << e.what() << endl;
 }
 catch(char const* msg){
   cout<<"Exception caught in PixelHisto::startOfRun of type char const*: " << msg << endl;
 }
 catch(...){
   cout<<"Exception caught in PixelHisto::startOfRun of unknown type." << endl;
 }
 return NULL;
}


/* **************** *
 *    prePublish    *
 * **************** */

/* *********************************************************************** *
 * Performs special operations right before the publication of histos; for *
 * instance: fitting procedures.                                           *
 * *********************************************************************** */
extern "C" void
prePublish(bool isEndOfRun)
{
 try {

   //printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
  if (PrintOutLevel>=1){
    printf("PixelHisto: Entering prePublish\n");
    fflush(stdout);
  }
  if (isEndOfRun)
    ERS_PIX_INFO("GNAM Pixel prePublish entry point called at isEndOfRun");

  // manually set the publish flag for the disabled histograms to trick ohp in displaying them
  layer2_disabled->SetPublishFlag();
  layer1_disabled->SetPublishFlag();
  layer0_disabled->SetPublishFlag();
  layerIbl_disabled->SetPublishFlag();
  layerDbm_disabled->SetPublishFlag();
  diskA_disabled->SetPublishFlag();
  diskC_disabled->SetPublishFlag();

  // manually set the publish flag for ROD merged histogram to force the gatherer to work, also do this for all other merged histos to prevent problems
  detector_3lp_ROD_errors->SetPublishFlag();
  detector_3lp_ROD_errorsvsBCID->SetPublishFlag();
  detector_IblDbm_ROD_errors->SetPublishFlag();
  detector_IblDbm_ROD_errorfraction->SetPublishFlag();
  detector_IblDbm_ROD_errorsvsBCID->SetPublishFlag();
  global_hits_normalized->SetPublishFlag();
  detector_occupancy_summary->SetPublishFlag();
  detector_occupancy_summary_sinceUpdate->SetPublishFlag();
  layer2_sumerrorfraction->SetPublishFlag();
  layer1_sumerrorfraction->SetPublishFlag();
  layer0_sumerrorfraction->SetPublishFlag();
  layerIbl_sumerrorfraction->SetPublishFlag();
  layerDbm_sumerrorfraction->SetPublishFlag();
  diskA_sumerrorfraction->SetPublishFlag();
  diskC_sumerrorfraction->SetPublishFlag();
  layer2_anyerrorfraction->SetPublishFlag();
  layer1_anyerrorfraction->SetPublishFlag();
  layer0_anyerrorfraction->SetPublishFlag();
  layerIbl_anyerrorfraction->SetPublishFlag();
  layerDbm_anyerrorfraction->SetPublishFlag();
  diskA_anyerrorfraction->SetPublishFlag();
  diskC_anyerrorfraction->SetPublishFlag();

  layerIblDbm_FEServiceCode->SetPublishFlag();
  layerIblDbm_FEerrors->SetPublishFlag();
  layerIblDbm_FEerrors_notweighted->SetPublishFlag(); // DC: added 22/10/2016
  
  layer2_lvl1id_errors_lastfiveminutes->SetPublishFlag();
  layer1_lvl1id_errors_lastfiveminutes->SetPublishFlag();
  layer0_lvl1id_errors_lastfiveminutes->SetPublishFlag();
  layerIbl_lvl1id_errors_lastfiveminutes->SetPublishFlag();
  layerDbm_lvl1id_errors_lastfiveminutes->SetPublishFlag();
  diskA_lvl1id_errors_lastfiveminutes->SetPublishFlag();
  diskC_lvl1id_errors_lastfiveminutes->SetPublishFlag();

  layerIbl_lvl1id_errorfraction_lastfiveminutes->SetPublishFlag();
  layerDbm_lvl1id_errorfraction_lastfiveminutes->SetPublishFlag();

  ibl_edge_occupancy->SetPublishFlag();

  layer2_occupancy->SetPublishFlag();
  layer1_occupancy->SetPublishFlag();
  layer0_occupancy->SetPublishFlag();
  layerIbl_occupancy->SetPublishFlag();
  layerDbm_occupancy->SetPublishFlag();
  diskA_occupancy->SetPublishFlag();
  diskC_occupancy->SetPublishFlag();
  layer2_occupancy_distribution->SetPublishFlag();
  layer1_occupancy_distribution->SetPublishFlag();
  layer0_occupancy_distribution->SetPublishFlag();
  layerIbl_occupancy_distribution->SetPublishFlag();
  layerDbm_occupancy_distribution->SetPublishFlag();
  diskA_occupancy_distribution->SetPublishFlag();
  diskC_occupancy_distribution->SetPublishFlag();
  layer2_occupancy_since_update->SetPublishFlag();
  layer1_occupancy_since_update->SetPublishFlag();
  layer0_occupancy_since_update->SetPublishFlag();
  layerIbl_occupancy_since_update->SetPublishFlag();
  layerDbm_occupancy_since_update->SetPublishFlag();
  diskA_occupancy_since_update->SetPublishFlag();
  diskC_occupancy_since_update->SetPublishFlag();
  layer2_occupancy_distribution_sinceUpdate->SetPublishFlag();
  layer1_occupancy_distribution_sinceUpdate->SetPublishFlag();
  layer0_occupancy_distribution_sinceUpdate->SetPublishFlag();
  layerIbl_occupancy_distribution_sinceUpdate->SetPublishFlag();
  layerDbm_occupancy_distribution_sinceUpdate->SetPublishFlag();
  diskA_occupancy_distribution_sinceUpdate->SetPublishFlag();
  diskC_occupancy_distribution_sinceUpdate->SetPublishFlag();

  detector_ROD_syncherrors->SetPublishFlag();
  
  //do the same for the LVL1ID Errors for DQMD
  for(map< vector<int>,string >::iterator it = ModGeoMap_all.begin(); it != ModGeoMap_all.end(); ++it){

    uint rod = (it->first)[0];
    uint ob = (it->first)[1];
    uint mod = (it->first)[2];
    // const char* modname = it->second.c_str();
    if(rod >= MAX_ROD_PER_ROS){
      ERS_PIX_ERROR("rod="<<rod<<" >= MAX_ROD_PER_ROS("<<MAX_ROD_PER_ROS<<"): " << "this should never happen.",MapError);
      cout <<"ERROR: rod="<<rod<<" out of range...  Try to continue to next mod..."<<endl;
      continue;
    }
    if(ob >= MAX_OB_PER_ROD){
      ERS_PIX_ERROR("ob="<<ob<<" >= MAX_OB_PER_ROD("<<MAX_OB_PER_ROD<<"): " << "this should never happen.",MapError);
      cout <<"ERROR: ob="<<ob<<" out of range...  Try to continue to next mod..."<<endl;
      continue;
    }
    if(mod > MAX_MOD_PER_OB){
      ERS_PIX_ERROR("mod="<<mod<<" > MAX_MOD_PER_OB("<<MAX_MOD_PER_OB<<"): " << "this should never happen.",MapError);
      cout <<"ERROR: mod="<<mod<<" out of range...  Try to continue to next mod..."<<endl;
      continue;
    }

    module_LVL1IDErrors[rod][ob][mod-1]->SetPublishFlag();
  } 
  
  // now fill the disabled modules and RODs from the autodisable, taken from AutoDisReport
  // int totDis = 0;
  if (is_manager != NULL){
    std::vector<std::string> varList = is_manager->listVariables("*AUTO_DISABLED_MODULES");
    for (uint iv = 0; iv<varList.size(); ++iv) {
      std::string mods = is_manager->read<std::string>(varList[iv].c_str());
      if (mods != "") { 
	int nmods = 0;
	std::size_t pos;
	std::string mod_name = "";
	mod_name=element(mods,nmods,'/');
	if( (pos = mod_name.find("(")) != std::string::npos )
	  mod_name.erase(pos); // get right format
	while (mod_name != "") {
	  ++nmods;
	  if (ModNamMap.find(mod_name) != ModNamMap.end()){
	    if (autoDisabled_modules.find(mod_name) == autoDisabled_modules.end()){ // module has to be added to disabled map
	      int mod_phi = (ModNamMap.find(mod_name)->second)[2];
	      autoDisabled_modules.insert(make_pair(mod_name,mod_phi));
	      int disabled_bin_content = 2;
	      if (mod_name.find("L")!=string::npos){ // Layer module

 		int layernumber, bistavenumber, stavenumber, etanumber;
		parse_mod_name_of_layer(mod_name, &layernumber, &bistavenumber, &stavenumber, &etanumber);
		
		
		if (layernumber==2){	
		  if ((bistavenumber==1) && (stavenumber==1)){
		    // highest bin due to shift in phi relative to Layer1
		    layer2_disabled->Fill(etanumber,51,disabled_bin_content);
		  } else {
		    // stave number shifted by 1 due to shift in phi relative to
		    // Layer1
		    layer2_disabled->Fill(etanumber,
					  (bistavenumber-1)*2+stavenumber-2,
					  disabled_bin_content);
		  }
		} else if (layernumber==1){
		  layer1_disabled->Fill(etanumber,
					(bistavenumber-1)*2+stavenumber-1,
					disabled_bin_content);
		} else if (layernumber==0){
		  if ((bistavenumber==11) && (stavenumber==2)){
		    // highest bin due to shift in phi relative to Layer1
		    layer0_disabled->Fill(etanumber,0,disabled_bin_content);
		  } else {
		    // stave number shifted by 1 due to shift in phi relative to
		    // Layer1
		    layer0_disabled->Fill(etanumber,
					  (bistavenumber-1)*2+stavenumber,
					  disabled_bin_content);
		  }
		} else if (layernumber==-1){ // IBL
		  layerIbl_disabled->Fill(etanumber,
					  stavenumber-1, // IBL just uses stave numbers 1-14
					  disabled_bin_content);
		} else if (layernumber==-2){ // DBM
		  layerDbm_disabled->Fill(etanumber, // telescope layer
					  stavenumber-1, // telescope number 1-4
					  disabled_bin_content);
		}

	      } else if (mod_name.find("D")!=string::npos){ // Disk module

		int disknumber;
		bool a_side;
		parse_mod_name_of_disk(mod_name, &disknumber, &a_side);
		
		if (a_side){
		  diskA_disabled->Fill(disknumber,mod_phi,disabled_bin_content);
		} else {
		  diskC_disabled->Fill(disknumber,mod_phi,disabled_bin_content);
		}
	      }
	    }
	  }
	  mod_name=element(mods,nmods,'/');
	  if( (pos = mod_name.find("(")) != std::string::npos )
	    mod_name.erase(pos); // get right format
	}
	// totDis += nmods;
	//      std::cout << element(varList[iv],1,'/') << " = " << smods << std::endl;
      }
    }
  }
  
  /*
  int totDisRods = 0;
  std::vector<std::string> varListRod = isManager->listVariables("*AUTO_DISABLED_ROD");
  for (uint ivr = 0; ivr<varList.size(); ++ivr) {
    std::string rod = isManager->read<std::string>(varListRod[ivr].c_str());
    if (rod != "") { 
      totDisRods++;
      std::cout << element(varListRod[ivr],1,'/') << " = " << rod << std::endl;
    }
  }
  */
  
  // GetBinContent returns the content of the bin one less than the argument
  int event_number = (int)global_events->GetBinContent(1);
  
  if (event_number!=0){

    // Fill the normalized hits per event histogram
    global_hits_normalized->Reset();
    for (int i=1; i<=global_hits->GetNbinsX(); ++i){
      float normalized_number = (float)(i-1)/(float)m_nmods;
      global_hits_normalized->Fill(normalized_number,global_hits->GetBinContent(i));
    }
    time_t RATE_CurrTime;

    time ( &RATE_CurrTime );
    
    // publish variables to IS
    if (is_manager !=NULL) {
      //	if (timnames[crate] == "") continue;
      //	string name(timnames[crate]);
      //	is_manager->publish(name+"/SampledEvents", (int)event_number);
      for (uint rod=0; rod<MAX_ROD_PER_ROS; ++rod){
	string name(rodnames[rod]);
	if (name=="") continue;
	//always publish the event number
	try{
	  is_manager->publish(name+"/Nevents",event_number);
	  if (is_rod_BCID_errors[rod]!=is_rod_BCID_errors_ref[rod]) {
	    is_manager->publish(name+"/BCID_errors",is_rod_BCID_errors[rod]);
	    is_rod_BCID_errors_ref[rod]=is_rod_BCID_errors[rod];
	  }
	  if (is_rod_LVL1ID_errors[rod]!=is_rod_LVL1ID_errors_ref[rod]) {
	    is_manager->publish(name+"/LVL1ID_errors",is_rod_LVL1ID_errors[rod]);
	    is_rod_LVL1ID_errors_ref[rod]=is_rod_LVL1ID_errors[rod];
	  }
	  if (is_rod_timeout_errors[rod]!=is_rod_timeout_errors_ref[rod]) {
	    is_manager->publish(name+"/timeout_errors",is_rod_timeout_errors[rod]);
	    is_rod_timeout_errors_ref[rod]=is_rod_timeout_errors[rod];
	  }
	  if (is_rod_dataincorrect_errors[rod]!=is_rod_dataincorrect_errors_ref[rod]) {
	    is_manager->publish(name+"/dataincorrect_errors",is_rod_dataincorrect_errors[rod]);
	    is_rod_dataincorrect_errors_ref[rod]=is_rod_dataincorrect_errors[rod];
	  }
	  if (is_rod_overflow_errors[rod]!=is_rod_overflow_errors_ref[rod]) {
	    is_manager->publish(name+"/dataoverflow_errors",is_rod_overflow_errors[rod]);
	    is_rod_overflow_errors_ref[rod]=is_rod_overflow_errors[rod];
	  }
	  if (is_rod_fragment_size[rod]!=is_rod_fragment_size_ref[rod]) {
	    is_manager->publish(name+"/FragmentSize",is_rod_fragment_size[rod]);
	    is_rod_fragment_size_ref[rod]=is_rod_fragment_size[rod];
	  }
	  for (uint ob=0; ob<MAX_OB_PER_ROD; ++ob){
	    for (uint mod=0; mod<MAX_MOD_PER_OB; ++mod){
	      string name(modulenames[rod][ob][mod]);
	      if (name=="") continue;

	      ///lastfiveminutes. fill in removal of all list contents farther away than last five minutes here

	      //              list<time_t>::iterator it =l_module_LVL1IDErrors[rod][ob][mod].begin();
	      //            while(difftime(RATE_CurrTime,*it)>300){
	      //		list.remove(it);
	      //		++it;//popfront?
	      //		}

              while(!l_module_LVL1IDErrors[rod][ob][mod].empty() &&
                    difftime(RATE_CurrTime,l_module_LVL1IDErrors[rod][ob][mod].front())>300){
		l_module_LVL1IDErrors[rod][ob][mod].pop_front();
	      }
              while(!l_histofills.empty() &&
                    difftime(RATE_CurrTime,l_histofills.front())>300){
		l_histofills.pop_front();
	      }
	      
	      if (is_mod_BCID_errors[rod][ob][mod]!=is_mod_BCID_errors_ref[rod][ob][mod]) {
		is_manager->publish(name+"/BCID_errors",is_mod_BCID_errors[rod][ob][mod]);
		is_mod_BCID_errors_ref[rod][ob][mod]=is_mod_BCID_errors[rod][ob][mod];
	      }
	      if (is_mod_timeout_errors[rod][ob][mod]!=is_mod_timeout_errors_ref[rod][ob][mod]) {
		is_manager->publish(name+"/timeout_errors",is_mod_timeout_errors[rod][ob][mod]);
		is_mod_timeout_errors_ref[rod][ob][mod]=is_mod_timeout_errors[rod][ob][mod];
	      }
	      if (is_mod_LVL1ID_errors[rod][ob][mod]!=is_mod_LVL1ID_errors_ref[rod][ob][mod]) {
		is_manager->publish(name+"/LVL1ID_errors",is_mod_LVL1ID_errors[rod][ob][mod]);
		is_mod_LVL1ID_errors_ref[rod][ob][mod]=is_mod_LVL1ID_errors[rod][ob][mod];
	      }
	      if (is_mod_preamble_errors[rod][ob][mod]!=is_mod_preamble_errors_ref[rod][ob][mod]) {
		is_manager->publish(name+"/preamble_errors",is_mod_preamble_errors[rod][ob][mod]);
		is_mod_preamble_errors_ref[rod][ob][mod]=is_mod_preamble_errors[rod][ob][mod];
	      }
	      if (is_mod_trailer_errors[rod][ob][mod]!=is_mod_trailer_errors_ref[rod][ob][mod]) {
		is_manager->publish(name+"/trailerbit_errors",is_mod_trailer_errors[rod][ob][mod]);
		is_mod_trailer_errors_ref[rod][ob][mod]=is_mod_trailer_errors[rod][ob][mod];
	      }
	      if (is_mod_limit_errors[rod][ob][mod]!=is_mod_limit_errors_ref[rod][ob][mod]) {
		is_manager->publish(name+"/headtraillimit_errors",is_mod_limit_errors[rod][ob][mod]);
		is_mod_limit_errors_ref[rod][ob][mod]=is_mod_limit_errors[rod][ob][mod];
	      }
	      if (is_mod_overflow_errors[rod][ob][mod]!=is_mod_overflow_errors_ref[rod][ob][mod]) {
		is_manager->publish(name+"/dataoverflow_errors",is_mod_overflow_errors[rod][ob][mod]);
		is_mod_overflow_errors_ref[rod][ob][mod]=is_mod_overflow_errors[rod][ob][mod];
	      }
              int currVal=l_module_LVL1IDErrors[rod][ob][mod].size();
	      if(currVal!=is_mod_lvl1id_errors_lastfiveminutes_ref[rod][ob][mod] || isStartOfRun){
                is_manager->publish(name+"/bcid_errors_lastfiveminutes",currVal);
 		is_mod_lvl1id_errors_lastfiveminutes_ref[rod][ob][mod]=currVal;
	      }
	      /*
		pair<int,int> position = make_pair(crate*MAX_ROD_PER_CRATE+rod, ModCorr[ob*MAX_MOD_PER_OB+mod+rod*28+crate*16*28]);
		int hitmap_bin_content = (int)detector_hitmap->GetBinContent(position.first+1,position.second+1);
		if (hitmap_bin_content!=is_mod_NHits_ref[rod][ob][mod]) {
		is_manager->publish(name+"/NHits", hitmap_bin_content);
		is_mod_NHits_ref[rod][ob][mod]=hitmap_bin_content;
		}
	      */

	      if (is_mod_IblDbmFEBCID_errors[rod][ob][mod]!=is_mod_IblDbmFEBCID_errors_ref[rod][ob][mod]) {
		is_manager->publish(name+"/IblDbmFEBCID_errors", is_mod_IblDbmFEBCID_errors[rod][ob][mod]);
		is_mod_IblDbmFEBCID_errors_ref[rod][ob][mod]=is_mod_IblDbmFEBCID_errors[rod][ob][mod];
	      }
	      if (is_mod_IblDbmFEHamming_errors[rod][ob][mod]!=is_mod_IblDbmFEHamming_errors_ref[rod][ob][mod]) {
		is_manager->publish(name+"/IblDbmFEHamming_errors", is_mod_IblDbmFEHamming_errors[rod][ob][mod]);
		is_mod_IblDbmFEHamming_errors_ref[rod][ob][mod]=is_mod_IblDbmFEHamming_errors[rod][ob][mod];
	      }
	      if (is_mod_IblDbmFEL1_errors[rod][ob][mod]!=is_mod_IblDbmFEL1_errors_ref[rod][ob][mod]) {
		is_manager->publish(name+"/IblDbmFEL1_errors", is_mod_IblDbmFEL1_errors[rod][ob][mod]);
		is_mod_IblDbmFEL1_errors_ref[rod][ob][mod]=is_mod_IblDbmFEL1_errors[rod][ob][mod];
	      }

	      if (is_mod_FEEOC_errors[rod][ob][mod]!=is_mod_FEEOC_errors_ref[rod][ob][mod]) {
		is_manager->publish(name+"/FEEOC_errors", is_mod_FEEOC_errors[rod][ob][mod]);
		is_mod_FEEOC_errors_ref[rod][ob][mod]=is_mod_FEEOC_errors[rod][ob][mod];
	      }
	      if (is_mod_FEHamming_errors[rod][ob][mod]!=is_mod_FEHamming_errors_ref[rod][ob][mod]) {
		is_manager->publish(name+"/FEHamming_errors", is_mod_FEHamming_errors[rod][ob][mod]);
		is_mod_FEHamming_errors_ref[rod][ob][mod]=is_mod_FEHamming_errors[rod][ob][mod];
	      }
	      if (is_mod_FEregparity_errors[rod][ob][mod]!=is_mod_FEregparity_errors_ref[rod][ob][mod]) {
		is_manager->publish(name+"/FEregparity_errors", is_mod_FEregparity_errors[rod][ob][mod]);
		is_mod_FEregparity_errors_ref[rod][ob][mod]=is_mod_FEregparity_errors[rod][ob][mod];
	      }
	      if (is_mod_FEhitparity_errors[rod][ob][mod]!=is_mod_FEhitparity_errors_ref[rod][ob][mod]) {
		is_manager->publish(name+"/FEhitparity_errors", is_mod_FEhitparity_errors[rod][ob][mod]);
		is_mod_FEhitparity_errors_ref[rod][ob][mod]=is_mod_FEhitparity_errors[rod][ob][mod];
	      }
	      if (is_mod_MCChitoverflow_errors[rod][ob][mod]!=is_mod_MCChitoverflow_errors_ref[rod][ob][mod]) {
		is_manager->publish(name+"/MCChitoverflow_errors", is_mod_MCChitoverflow_errors[rod][ob][mod]);
		is_mod_MCChitoverflow_errors_ref[rod][ob][mod]=is_mod_MCChitoverflow_errors[rod][ob][mod];
	      }
	      if (is_mod_MCCEoEoverflow_errors[rod][ob][mod]!=is_mod_MCCEoEoverflow_errors_ref[rod][ob][mod]) {
		is_manager->publish(name+"/MCCEoEoverflow_errors", is_mod_MCCEoEoverflow_errors[rod][ob][mod]);
		is_mod_MCCEoEoverflow_errors_ref[rod][ob][mod]=is_mod_MCCEoEoverflow_errors[rod][ob][mod];
	      }
	      if (is_mod_MCCL1ID_errors[rod][ob][mod]!=is_mod_MCCL1ID_errors_ref[rod][ob][mod]) {
		is_manager->publish(name+"/MCCL1ID_errors", is_mod_MCCL1ID_errors[rod][ob][mod]);
		is_mod_MCCL1ID_errors_ref[rod][ob][mod]=is_mod_MCCL1ID_errors[rod][ob][mod];
	      }
	      if (is_mod_MCCBCIDEoE_errors[rod][ob][mod]!=is_mod_MCCBCIDEoE_errors_ref[rod][ob][mod]) {
		is_manager->publish(name+"/MCCBCIDEoE_errors", is_mod_MCCBCIDEoE_errors[rod][ob][mod]);
		is_mod_MCCBCIDEoE_errors_ref[rod][ob][mod]=is_mod_MCCBCIDEoE_errors[rod][ob][mod];
	      }
	      if (is_mod_MCCL1IDEoE_errors[rod][ob][mod]!=is_mod_MCCL1IDEoE_errors_ref[rod][ob][mod]) {
		is_manager->publish(name+"/MCCL1IDEoE_errors", is_mod_MCCL1IDEoE_errors[rod][ob][mod]);
		is_mod_MCCL1IDEoE_errors_ref[rod][ob][mod]=is_mod_MCCL1IDEoE_errors[rod][ob][mod];
	      }
	    }
	  }
	}
	catch (...){
	  ERS_PIX_INFO("Tried to publish IS info for ROD: " << name << " but an error was thrown, ignoring this ROD");
	}
      }
    }
    isStartOfRun=false;

    int source_id;
    int hitmap_bin_content;
    int hitmap_bin_content_ibl_edge;
    int sumerrors_bin_content;
    int anyerrors_bin_content;
    float occupancy_bin_content;
    float occupancy_bin_content_ibl_edge;
    float occupancy_since_update_bin_content;
    float sumerrorfraction_bin_content;
    float anyerrorfraction_bin_content;
    float lvl1id_errors_lastfiveminutes_bin_content;
    float n_events_since_update=300.;

    nEvents_lastUpdate=nEvents_thisUpdate;
    nEvents_thisUpdate=(int)global_events->GetBinContent(1);
    n_events_since_update=nEvents_thisUpdate - nEvents_lastUpdate;



    map< pair<int,int>,vector<int> >::iterator ModLogMap_iterator;
    pair<int,int> position;
    //    int noisy = 1;

    // Reset the Occupancy histograms
    detector_occupancy->Reset();
    //    detector_summary->Reset();
    detector_occupancy_summary->Reset();
    //    detector_summary->Reset();
    detector_occupancy_summary_sinceUpdate->Reset();

    ibl_edge_occupancy->Reset();

    layer2_occupancy->Reset();
    layer1_occupancy->Reset();
    layer0_occupancy->Reset();
    layerIbl_occupancy->Reset();
    layerDbm_occupancy->Reset();
    diskA_occupancy->Reset();
    diskC_occupancy->Reset();
    layer2_occupancy_distribution->Reset();
    layer1_occupancy_distribution->Reset();
    layer0_occupancy_distribution->Reset();
    layerIbl_occupancy_distribution->Reset();
    layerDbm_occupancy_distribution->Reset();
    diskA_occupancy_distribution->Reset();
    diskC_occupancy_distribution->Reset();
    layer2_occupancy_since_update->Reset();
    layer1_occupancy_since_update->Reset();
    layer0_occupancy_since_update->Reset();
    layerIbl_occupancy_since_update->Reset();
    layerDbm_occupancy_since_update->Reset();
    diskA_occupancy_since_update->Reset();
    diskC_occupancy_since_update->Reset();
    layer2_occupancy_distribution_sinceUpdate->Reset();
    layer1_occupancy_distribution_sinceUpdate->Reset();
    layer0_occupancy_distribution_sinceUpdate->Reset();
    layerIbl_occupancy_distribution_sinceUpdate->Reset();
    layerDbm_occupancy_distribution_sinceUpdate->Reset();
    diskA_occupancy_distribution_sinceUpdate->Reset();
    diskC_occupancy_distribution_sinceUpdate->Reset();
    layer2_sumerrorfraction->Reset();
    layer1_sumerrorfraction->Reset();
    layer0_sumerrorfraction->Reset();
    layerIbl_sumerrorfraction->Reset();
    layerDbm_sumerrorfraction->Reset();
    diskA_sumerrorfraction->Reset();
    diskC_sumerrorfraction->Reset();
    layer2_anyerrorfraction->Reset();
    layer1_anyerrorfraction->Reset();
    layer0_anyerrorfraction->Reset();
    layerIbl_anyerrorfraction->Reset();
    layerDbm_anyerrorfraction->Reset();
    diskA_anyerrorfraction->Reset();
    diskC_anyerrorfraction->Reset();
    layer2_lvl1id_errors_lastfiveminutes->Reset();
    layer1_lvl1id_errors_lastfiveminutes->Reset();
    layer0_lvl1id_errors_lastfiveminutes->Reset();
    layerIbl_lvl1id_errors_lastfiveminutes->Reset();
    layerDbm_lvl1id_errors_lastfiveminutes->Reset();
    diskA_lvl1id_errors_lastfiveminutes->Reset();
    diskC_lvl1id_errors_lastfiveminutes->Reset();
    layerIbl_lvl1id_errorfraction_lastfiveminutes->Reset();
    layerDbm_lvl1id_errorfraction_lastfiveminutes->Reset();

    // Reset the Errorfraction histograms

    //    ModPosMap.clear();
    /*
    if (adjust_occupancy){
      // this adjusts the expected occupancy during runtime (as more and more
      // hits are collected). Eventually a limit is reached, this should be
      // implemented FIXME
      expected_occupancy = (1.-1./(float(event_number)*PIXEL_PER_MOD))
	* occupancy_acceptance + 1./(float(event_number)*PIXEL_PER_MOD);
    }
    */

    for (ModLogMap_iterator=ModLogMap.begin(); ModLogMap_iterator!=ModLogMap.end(); ++ModLogMap_iterator){
      uint rod_number = ((ModLogMap_iterator->second))[0];
      uint ob_number = ((ModLogMap_iterator->second))[1];
      uint mod_number = ((ModLogMap_iterator->second))[2];
      if(rod_number >= MAX_ROD_PER_ROS){
	ERS_PIX_ERROR("rod_number="<<rod_number<<" >= MAX_ROD_PER_ROS("<<MAX_ROD_PER_ROS<<"): " << "this should never happen.",MapError);
	cout <<"ERROR: rod_number="<<rod_number<<" out of range...  Try to continue to next mod..."<<endl;
	continue;
      }
      if(ob_number >= MAX_OB_PER_ROD){
	ERS_PIX_ERROR("ob_number="<<ob_number<<" >= MAX_OB_PER_ROD("<<MAX_OB_PER_ROD<<"): " << "this should never happen.",MapError);
	cout <<"ERROR: ob_number="<<ob_number<<" out of range...  Try to continue to next mod..."<<endl;
	continue;
      }
      if(mod_number > MAX_MOD_PER_OB){
	ERS_PIX_ERROR("mod_number="<<mod_number<<" > MAX_MOD_PER_OB("<<MAX_MOD_PER_OB<<"): " << "this should never happen.",MapError);
	cout <<"ERROR: mod_number="<<mod_number<<" out of range...  Try to continue to next mod..."<<endl;
	continue;
      }
      
      source_id = ModLogMap_iterator->first.first;
      float pixels_per_mod = (isIblOrDbm(source_id))? PIX_PER_MOD_ibl : PIX_PER_MOD_3lp;
      if(isIblOrDbm(source_id)) pixels_per_mod = (isIbl(source_id))? PIX_PER_MOD_ibl : PIX_PER_MOD_dbm; //new

      // Diane - test of the source id and pixels per mod:
      //std::cout << "source_id " << source_id << " pixels_per_mod " << pixels_per_mod << std::endl; 
      
      // build the headers/event ratio here
      int nHeaders = module_status[rod_number][ob_number][mod_number-1]->GetBinContent(1);
      module_statusRatio[rod_number][ob_number][mod_number-1]->SetBinContent(1, (float)nHeaders / (float)event_number);

      position = make_pair(m_ros_number*MAX_ROD_PER_ROS+rod_number, ob_number*MAX_MOD_PER_OB+mod_number-1);

      hitmap_bin_content = (int)detector_hitmap->GetBinContent(position.first+1, position.second+1);

      sumerrors_bin_content = global_errorsum[rod_number][ob_number][mod_number-1];
      anyerrors_bin_content = global_errorany[rod_number][ob_number][mod_number-1];

      // calculate occupancy as hits per event per pixel
      occupancy_bin_content = float(hitmap_bin_content) / ( float(event_number) * pixels_per_mod ); 

      if(n_events_since_update>0){ //protect against too short update weirdness
	occupancy_since_update_bin_content = 
	  float(module_nHitsSinceUpdate[rod_number][ob_number][mod_number-1]) / (n_events_since_update * pixels_per_mod );
      }
      else{
	occupancy_since_update_bin_content=0;
      }
      module_nHitsSinceUpdate[rod_number][ob_number][mod_number-1]=0; // reset it now, so next fillHisto will start at 0
     
      sumerrorfraction_bin_content = float(sumerrors_bin_content) / float(event_number);
      anyerrorfraction_bin_content = float(anyerrors_bin_content) / float(event_number);

      lvl1id_errors_lastfiveminutes_bin_content=l_module_LVL1IDErrors[rod_number][ob_number][mod_number-1].size();

      detector_occupancy->Fill(position.first,position.second, occupancy_bin_content);
      // detector_occupancy_distribution->Fill(occupancy_bin_content);

      detector_occupancy_summary->Fill(occupancy_bin_content);
      detector_occupancy_summary_sinceUpdate->Fill(occupancy_since_update_bin_content);
      /*
	if (occupancy_bin_content>expected_occupancy) {
	detector_summary->Fill(position.first,position.second,1);
	noisy = 2;
	} else {
	detector_summary->Fill(position.first,position.second,2);
	}
      */
      // now fill the Layer / Disk overview histograms
      map< vector<int>,string >::iterator ModGeoMap_iterator =
	ModGeoMap.find(ModLogMap_iterator->second);
      int phi;
 
      if (ModGeoMap_iterator!=ModGeoMap.end()){
	string mod_name;
	mod_name = ModGeoMap_iterator->second;
	if (mod_name.find("L")!=string::npos){ // Layer module

	  int layernumber, bistavenumber, stavenumber, etanumber;
	  parse_mod_name_of_layer(mod_name, &layernumber, &bistavenumber, &stavenumber, &etanumber);
	  
	  // now fill the Layer / Disk occupancy overview
	  // the kind of filling can be changed to use phi coordinates,
	  // but it's not feasible for the SR1 TOOTHPIX setup FIXME
	  if (layernumber==2){
	    /*
	    // This can be used to write C0 ROD 15 in the high occupancy bins (for the SR1 setup) remove for PIT!!
	    if ((bistavenumber==99) && (stavenumber==1)){
	    layer2_occupancy->Fill(etanumber,51,occupancy_bin_content);
	    layer2_sumerrorfraction->Fill(etanumber,51,
	    sumerrorfraction_bin_content);
	    }
	    if ((bistavenumber==99) && (stavenumber==2)){
	    layer2_occupancy->Fill(etanumber,50,occupancy_bin_content);
	    layer2_sumerrorfraction->Fill(etanumber,50,
	    sumerrorfraction_bin_content);
	    }
	    */
	    if ((bistavenumber==1) && (stavenumber==1)){
	      // highest bin due to shift in phi relative to Layer1
	      layer2_occupancy->Fill(etanumber,51,occupancy_bin_content);
	      layer2_occupancy_distribution->Fill(occupancy_bin_content);
	      layer2_occupancy_since_update->Fill(etanumber,51,occupancy_since_update_bin_content);
	      layer2_occupancy_distribution_sinceUpdate->Fill(occupancy_since_update_bin_content);
	      // highest bin due to shift in phi relative to Layer1
	      layer2_sumerrorfraction->Fill(etanumber,51,
					    sumerrorfraction_bin_content);
	      layer2_anyerrorfraction->Fill(etanumber,51,
					    anyerrorfraction_bin_content);
	      layer2_lvl1id_errors_lastfiveminutes->Fill(etanumber,51,
							 lvl1id_errors_lastfiveminutes_bin_content);
	    } else {
	      // stave number shifted by 1 due to shift in phi relative to
	      // Layer1
	      layer2_occupancy_since_update->Fill(etanumber,
						  (bistavenumber-1)*2+stavenumber-2,
						  occupancy_since_update_bin_content);
	      layer2_occupancy_distribution_sinceUpdate->Fill(occupancy_since_update_bin_content);
	      layer2_occupancy->Fill(etanumber,
				     (bistavenumber-1)*2+stavenumber-2,
				     occupancy_bin_content);
	      layer2_occupancy_distribution->Fill(occupancy_bin_content);
	      // stave number shifted by 1 due to shift in phi relative to
	      // Layer1
	      layer2_sumerrorfraction->Fill(etanumber,
					    (bistavenumber-1)*2+stavenumber-2,
					    sumerrorfraction_bin_content);
	      layer2_anyerrorfraction->Fill(etanumber,
					    (bistavenumber-1)*2+stavenumber-2,
					    anyerrorfraction_bin_content);
	      layer2_lvl1id_errors_lastfiveminutes->Fill(etanumber,
							 (bistavenumber-1)*2+stavenumber-2,
							 lvl1id_errors_lastfiveminutes_bin_content);
	    }

	  } else if (layernumber==1){
	    layer1_occupancy->Fill(etanumber,
				   (bistavenumber-1)*2+stavenumber-1,
				   occupancy_bin_content);
	    layer1_occupancy_distribution->Fill(occupancy_bin_content);
	    layer1_occupancy_since_update->Fill(etanumber,
						(bistavenumber-1)*2+stavenumber-1,
						occupancy_since_update_bin_content);
	    layer1_occupancy_distribution_sinceUpdate->Fill(occupancy_since_update_bin_content);
	    layer1_sumerrorfraction->Fill(etanumber,
					  (bistavenumber-1)*2+stavenumber-1,
					  sumerrorfraction_bin_content);
	    layer1_anyerrorfraction->Fill(etanumber,
					  (bistavenumber-1)*2+stavenumber-1,
					  anyerrorfraction_bin_content);
	    layer1_lvl1id_errors_lastfiveminutes->Fill(etanumber,
						       (bistavenumber-1)*2+stavenumber-1,
						       lvl1id_errors_lastfiveminutes_bin_content);

	  } else if (layernumber==0){
	    if ((bistavenumber==11) && (stavenumber==2)){
	      // highest bin due to shift in phi relative to Layer1
	      layer0_occupancy->Fill(etanumber,0,occupancy_bin_content);
	      layer0_occupancy_distribution->Fill(occupancy_bin_content);
	      layer0_occupancy_since_update->Fill(etanumber,0,occupancy_since_update_bin_content);
	      layer0_occupancy_distribution_sinceUpdate->Fill(occupancy_since_update_bin_content);
	      // highest bin due to shift in phi relative to Layer1
	      layer0_sumerrorfraction->Fill(etanumber,0,
					    sumerrorfraction_bin_content);
	      layer0_anyerrorfraction->Fill(etanumber,0,
					    anyerrorfraction_bin_content);
	      layer0_lvl1id_errors_lastfiveminutes->Fill(etanumber,0,
							 lvl1id_errors_lastfiveminutes_bin_content);
	    } else {
	      // stave number shifted by 1 due to shift in phi relative to
	      // Layer1
	      layer0_occupancy_distribution->Fill(occupancy_bin_content);
	      layer0_occupancy->Fill(etanumber,
				     (bistavenumber-1)*2+stavenumber,
				     occupancy_bin_content);
	      layer0_occupancy_distribution_sinceUpdate->Fill(occupancy_since_update_bin_content);
	      layer0_occupancy_since_update->Fill(etanumber,
						  (bistavenumber-1)*2+stavenumber,
						  occupancy_since_update_bin_content);
	      layer0_lvl1id_errors_lastfiveminutes->Fill(etanumber,
							 (bistavenumber-1)*2+stavenumber,
							 lvl1id_errors_lastfiveminutes_bin_content);
	      // stave number shifted by 1 due to shift in phi relative to
	      // Layer1
	      layer0_sumerrorfraction->Fill(etanumber,
					    (bistavenumber-1)*2+stavenumber,
					    sumerrorfraction_bin_content);
	      layer0_anyerrorfraction->Fill(etanumber,
					    (bistavenumber-1)*2+stavenumber,
					    anyerrorfraction_bin_content);
	    }

	  } else if (layernumber==-1){ // IBL
	    int ibl_edge = bistavenumber; // For IBL, bistavenumber tells if this is an edge module

	    if( ibl_edge != 0 ){
	      hitmap_bin_content_ibl_edge = (int)ibl_edge_hitmap->GetBinContent(ibl_edge, stavenumber-1);
	      // calculate occupancy as hits per event per pixel
	      occupancy_bin_content_ibl_edge = float(hitmap_bin_content_ibl_edge) / ( float(event_number) * 336*2 ); // edge is 2 columns with 336 rows
	      ibl_edge_occupancy->Fill(ibl_edge, stavenumber-1, occupancy_bin_content_ibl_edge);
	    }


	    layerIbl_occupancy->Fill(etanumber,
				     stavenumber-1,
				     occupancy_bin_content);
	    layerIbl_occupancy_distribution->Fill(occupancy_bin_content);
	    layerIbl_occupancy_since_update->Fill(etanumber,
						  stavenumber-1,
						  occupancy_since_update_bin_content);
	    layerIbl_occupancy_distribution_sinceUpdate->Fill(occupancy_since_update_bin_content);
	    layerIbl_sumerrorfraction->Fill(etanumber,
					    stavenumber-1,
					    sumerrorfraction_bin_content);
	    layerIbl_anyerrorfraction->Fill(etanumber,
					    stavenumber-1,
					    anyerrorfraction_bin_content);
	    layerIbl_lvl1id_errors_lastfiveminutes->Fill(etanumber,
							 stavenumber-1,
							 lvl1id_errors_lastfiveminutes_bin_content);
	    layerIbl_lvl1id_errorfraction_lastfiveminutes->Fill(etanumber,
								stavenumber-1,
								lvl1id_errors_lastfiveminutes_bin_content/l_histofills.size());
	    
	  } else if (layernumber==-2){ // DBM
	    layerDbm_occupancy->Fill(etanumber,
				     stavenumber-1,
				     occupancy_bin_content);
	    layerDbm_occupancy_distribution->Fill(occupancy_bin_content);
	    layerDbm_occupancy_since_update->Fill(etanumber,
						  stavenumber-1,
						  occupancy_since_update_bin_content);
	    layerDbm_occupancy_distribution_sinceUpdate->Fill(occupancy_since_update_bin_content);
	    layerDbm_sumerrorfraction->Fill(etanumber,
					    stavenumber-1,
					    sumerrorfraction_bin_content);
	    layerDbm_anyerrorfraction->Fill(etanumber,
					    stavenumber-1,
					    anyerrorfraction_bin_content);
	    layerDbm_lvl1id_errors_lastfiveminutes->Fill(etanumber,
							 stavenumber-1,
							 lvl1id_errors_lastfiveminutes_bin_content);
	    layerDbm_lvl1id_errorfraction_lastfiveminutes->Fill(etanumber,
								stavenumber-1,
								lvl1id_errors_lastfiveminutes_bin_content/l_histofills.size());
	  }

	} else if (mod_name.find("D")!=string::npos){ // Disk module

	  int disknumber;
	  bool a_side;
	  parse_mod_name_of_disk(mod_name, &disknumber, &a_side);
	  
	  map< vector<int>,vector<int> >::iterator ModModelMap_iterator =
	    ModModelMap.find(ModLogMap_iterator->second);
	  if (ModModelMap_iterator!=ModModelMap.end()){
	    // disk module phi coordinate
	    phi = (ModModelMap_iterator->second)[2];
	    if (a_side){
	      diskA_occupancy->Fill(disknumber,phi,occupancy_bin_content);
	      diskA_occupancy_distribution->Fill(occupancy_bin_content);
	      diskA_occupancy_since_update->Fill(disknumber,phi,occupancy_since_update_bin_content);
	      diskA_occupancy_distribution_sinceUpdate->Fill(occupancy_since_update_bin_content);
	      diskA_sumerrorfraction->Fill(disknumber,phi,
					   sumerrorfraction_bin_content);
	      diskA_anyerrorfraction->Fill(disknumber,phi,
					   anyerrorfraction_bin_content);
	      diskA_lvl1id_errors_lastfiveminutes->Fill(disknumber,phi,
							lvl1id_errors_lastfiveminutes_bin_content);
	    } else {
	      diskC_occupancy->Fill(disknumber,phi,occupancy_bin_content);
	      diskC_occupancy_distribution->Fill(occupancy_bin_content);
	      diskC_occupancy_since_update->Fill(disknumber,phi,occupancy_since_update_bin_content);
	      diskC_occupancy_distribution_sinceUpdate->Fill(occupancy_since_update_bin_content);
	      diskC_sumerrorfraction->Fill(disknumber,phi,
					   sumerrorfraction_bin_content);
	      diskC_anyerrorfraction->Fill(disknumber,phi,
					   anyerrorfraction_bin_content);
	      diskC_lvl1id_errors_lastfiveminutes->Fill(disknumber,phi,
							lvl1id_errors_lastfiveminutes_bin_content);
	      
	      
	    }
	  } else {
	    ERS_PIX_WARNING("Module found in GeoMap, but not in Offmap: " << "this should never happen.",MapError);
	  }
	}

      } else {
	ERS_PIX_WARNING( "Module not found in GeoMap, this is bad. " << "Layer/Disk overview corrupted.",MapError);
	cout << "WARNING: Module not found in GeoMap, this is bad. " << "Layer/Disk overview corrupted." << endl;
      }
      /*
	ModPosMap.insert(make_pair(ModLogMap_iterator->second,noisy));
	noisy = 1;
      */
    }
    
    // Fill ROD error fractions
    for( int ix=1; ix<=detector_IblDbm_ROD_errorfraction->GetNbinsX(); ++ix){
      for( int iy=1; iy<=detector_IblDbm_ROD_errorfraction->GetNbinsY(); ++iy){
	detector_IblDbm_ROD_errorfraction->SetBinContent(ix,iy,detector_IblDbm_ROD_errors->GetBinContent(ix,iy)/event_number);
      }
    }
    
  } // event_number!=0
  
  update_title( ibl_edge_occupancy, run_number, luminosity_block);

  update_title( layerIbl_occupancy, run_number, luminosity_block);
  update_title( layerDbm_occupancy, run_number, luminosity_block);
  update_title( layer0_occupancy, run_number, luminosity_block);
  update_title( layer1_occupancy, run_number, luminosity_block);
  update_title( layer2_occupancy, run_number, luminosity_block);
  update_title( diskA_occupancy, run_number, luminosity_block);
  update_title( diskC_occupancy, run_number, luminosity_block);

  update_title( layerIbl_occupancy_distribution, run_number, luminosity_block);
  update_title( layerDbm_occupancy_distribution, run_number, luminosity_block);
  update_title( layer0_occupancy_distribution, run_number, luminosity_block);
  update_title( layer1_occupancy_distribution, run_number, luminosity_block);
  update_title( layer2_occupancy_distribution, run_number, luminosity_block);
  update_title( diskA_occupancy_distribution, run_number, luminosity_block);
  update_title( diskC_occupancy_distribution, run_number, luminosity_block);

  update_title( layerIbl_disabled, run_number, luminosity_block);
  update_title( layerDbm_disabled, run_number, luminosity_block);
  update_title( layer0_disabled, run_number, luminosity_block);
  update_title( layer1_disabled, run_number, luminosity_block);
  update_title( layer2_disabled, run_number, luminosity_block);
  update_title( diskA_disabled, run_number, luminosity_block);
  update_title( diskC_disabled, run_number, luminosity_block);

  update_title( layerIbl_sumerrorfraction, run_number, luminosity_block);
  update_title( layerDbm_sumerrorfraction, run_number, luminosity_block);
  update_title( layer0_sumerrorfraction, run_number, luminosity_block);
  update_title( layer1_sumerrorfraction, run_number, luminosity_block);
  update_title( layer2_sumerrorfraction, run_number, luminosity_block);
  update_title( diskA_sumerrorfraction, run_number, luminosity_block);
  update_title( diskC_sumerrorfraction, run_number, luminosity_block);

  update_title( layerIbl_anyerrorfraction, run_number, luminosity_block);
  update_title( layerDbm_anyerrorfraction, run_number, luminosity_block);
  update_title( layer0_anyerrorfraction, run_number, luminosity_block);
  update_title( layer1_anyerrorfraction, run_number, luminosity_block);
  update_title( layer2_anyerrorfraction, run_number, luminosity_block);
  update_title( diskA_anyerrorfraction, run_number, luminosity_block);
  update_title( diskC_anyerrorfraction, run_number, luminosity_block);

  update_title( layerIblDbm_FEServiceCode, run_number, luminosity_block);
  update_title( layerIblDbm_FEerrors, run_number, luminosity_block);
  update_title( layerIblDbm_FEerrors_notweighted, run_number, luminosity_block); // DC: added 22/10/2016

  update_title( detector_3lp_ROD_errors, run_number, luminosity_block);
  update_title( detector_IblDbm_ROD_errors, run_number, luminosity_block);
  update_title( detector_IblDbm_ROD_errorfraction, run_number, luminosity_block);

  update_title( detector_ROD_transerrors, run_number, luminosity_block);
  update_title( detector_ROD_SEUerrors, run_number, luminosity_block);
  update_title( detector_ROD_trunchighoccerrors, run_number, luminosity_block);
  update_title( detector_ROD_DAQerrors, run_number, luminosity_block);
  update_title( detector_ROD_timeouterrors, run_number, luminosity_block);

  if (PrintOutLevel>=1){
    printf("PixelHisto: Leaving prePublish\n");
    fflush(stdout);
  }

 }
 catch(std::exception& e){
   cout<<"Exception caught in PixelHisto::prePublish of type std::exception: " << e.what() << endl;
 }
 catch(char const* msg){
   cout<<"Exception caught in PixelHisto::prePublish of type char const*: " << msg << endl;
 }
 catch(...){
   cout<<"Exception caught in PixelHisto::prePublish of unknown type." << endl;
 }
}


/* ******** *
 * endOfRun *
 * ******** */

/* ************************************************* *
 * Called once every end of run, performing clean-up *
 * ************************************************* */
extern "C" void
endOfRun(void)
{
 try {
  if (PrintOutLevel>=1){
    printf("PixelHisto: Entering endOfRun\n");
    fflush(stdout);
  }

  ERS_ASSERT(histolist!=NULL);
  size_t k, max;
  max = histolist->size();
  if (PrintOutLevel>=1){
    printf("\tReady to delete histolist = %p (size = %lu)\n",(void*)histolist,max);
  }
  ERS_PIX_DEBUG(1,"Ready to delete histolist = " << showbase << hex << histolist << " (size = " << dec << max << ")");

  for (k=0; k<max; ++k){
    if( (*histolist)[k] != NULL) delete (*histolist)[k];
  }
  if (histolist != NULL) delete histolist;
  histolist = NULL;

  // Reset global variables
  for (uint rod=0; rod < MAX_ROD_PER_ROS; ++rod){
    for (uint ob=0; ob < MAX_OB_PER_ROD; ++ob){
      for (uint mod=0; mod < MAX_MOD_PER_OB; ++mod){
	global_errorsum[rod][ob][mod] = 0;
	global_errorany[rod][ob][mod] = 0;
      }
    }
  }

  if (PrintOutLevel>=1){
    printf("PixelHisto: Leaving endOfRun\n");
    fflush(stdout);
  }

 }
 catch(std::exception& e){
   cout<<"Exception caught in PixelHisto::endOfRun of type std::exception: " << e.what() << endl;
 }
 catch(char const* msg){
   cout<<"Exception caught in PixelHisto::endOfRun of type char const*: " << msg << endl;
 }
 catch(...){
   cout<<"Exception caught in PixelHisto::endOfRun of unknown type." << endl;
 }
}


/* ********* *
 * DFStopped *
 * ********* */

/* ******************************************************************** *
 * Called after endOfRun, when the DataFlow is completely stopped.      *
 * Histograms should not be updated here since they are not going to be *
 * published by Gnam. The function is here mainly for MonaIsa support   *
 * (IS gathering after the end of run).                                 *
 * ******************************************************************** */
extern "C" void
DFStopped(void)
{
  if (PrintOutLevel>=1){
    printf("PixelHisto: Entering DFStopped\n");
    fflush(stdout);
  }
  ERS_DEBUG(2,"Starting");

  if (PrintOutLevel>=1){
    printf("PixelHisto: Leaving DFStopped\n");
    fflush(stdout);
  }
}


/* ******************* *
 *    customCommand    *
 * ******************* */

/* ****************************************** *
 * Executes custom commands received from OH. *
 *  should be tested and checked FIXME        *
 * ****************************************** */
extern "C" void
customCommand(const GnamHisto *histo, int argc, const char* const *argv)
{
 (void) histo;
 try {
  if (PrintOutLevel>=1){
    printf("PixelHisto: Entering customCommand\n");
    fflush(stdout);
  }
  ERS_INFO("GNAM Pixel customCommand entry point called");

  int k;
  for (k=0; k<argc; ++k){
    if (strcmp(argv[k],"test")==0){
      cout << "test works!" << endl;
    }

    /*    
    else if (strcmp(argv[k],"+pdisks")==0){
      publish_geodisks = true;
      ERS_PIX_INFO("Now starting publishing the geographical disk overview");
    } else if (strcmp(argv[k],"-pdisks")==0){
      publish_geodisks = false;
      ERS_PIX_INFO("Now stopping publishing the geographical disk overview");
      detector_disks->Reset();
    } else if (strcmp(argv[k],"+players")==0){
      publish_geolayers = true;
      ERS_PIX_INFO("Now starting publishing the geographical layer overview");
    } else if (strcmp(argv[k],"-players")==0){
      publish_geolayers = false;
      ERS_PIX_INFO("Now stopping publishing the geographical layer overview");
      detector_blayer->Reset();
      detector_layer1->Reset();
      detector_layer2->Reset();
    }
    */
    
    //--------------------------------------------------------------
    // stops / starts the filling of module histograms
    // module name is given and associated to one of the histograms
    // wildcards and intelligence to be added FIXME
    //--------------------------------------------------------------
    else if (strcmp(argv[k],"+fm") == 0){
      if (argc > k+1 && argv[k+1]!=NULL){
	ERS_PIX_INFO("+fm command received for module " << dec << argv[k+1]);
	string name = argv[k+1];
	if (ModNamMap.find(name)!=ModNamMap.end()){ // module name valid
	  if (ModDynMap.size()<MAX_DYN_HISTO){ // check if there's a free module slot
	    uint number = 0;
	    //find the first free module slot
	    while (number<MAX_DYN_HISTO && ModDynMap_places[number]==true){
	      number++;
	    }
	    if(number<MAX_DYN_HISTO){
	      // BH: did not get Mapo's suggestion to protect it to work => left it as it was
	      ModDynMap.insert(make_pair(name,number));
	      ModDynMap_places[number] = true;
	      fill_single_modules = true;
	      
	      // MPG: postponed call to publish_modules
	      
	      reset_module_hist( module_hitmap[number], string("Hitmap for module ")+name, string("/SHIFT/Pixel/Modules/")+name+"/Hitmap");
	      reset_module_hist( module_tot[number], string("ToT for module ")+name, string("/SHIFT/Pixel/Modules/")+name+"/ToT");
	      reset_module_hist( module_tot_cluster[number], string("ClusterToT for module ")+name, string("/SHIFT/Pixel/Modules/")+name+"/ClusterToT");
	      reset_module_hist( module_tot_cluster_size[number], string("ClusterSize for module ")+name, string("/SHIFT/Pixel/Modules/")+name+"/ClusterSize");
	      reset_module_hist( module_hittime[number], string("LVL1As for module ")+name, string("/SHIFT/Pixel/Modules/")+name+"/LVL1As");
	      reset_module_hist( module_ercode[number], string("Error codes for module ")+name, string("/SHIFT/Pixel/Modules/")+name+"/Errcodes");
	      reset_module_hist( module_hits[number], string("Hits for module ")+name, string("/SHIFT/Pixel/Modules/")+name+"/Hits");
	      reset_module_hist( module_bcid[number], string("BCID distribution for module ")+name, string("/SHIFT/Pixel/Modules/")+name+"/BCID");
	      reset_module_hist( module_events[number], string("Monitored Events for module ")+name, string("/SHIFT/Pixel/Modules/")+name+"/Events");
	      // reset_module_hist( module_lvl1id[number], string("LVL1ID errors for module ")+name, string("/SHIFT/Pixel/Modules/")+name+"/LVL1IDErrors");
	      
	      // MPG: now publish modules
	      publish_modules(true,number);
	      
	      ERS_PIX_INFO("Module successfully added for filling and " << "publishing on position " << dec << number);
	    } else {
	      ERS_PIX_INFO("Could not find a free module position for " << name << ". Free a space with -fm first");
	    }
	  } else {
	    ERS_PIX_INFO("No free module position to add module " << name << ". Free a space with -fm first");
	  }
	} else {
	  ERS_PIX_INFO("Module name " << name << " not found");
	}
	++k;
      } else {
	ERS_PIX_INFO("Give me a module name with that");
	break;
      }
    } else if (strcmp(argv[k],"-fm")==0){
      if (argc > k+1 && argv[k+1]!=NULL){
	string name = argv[k+1];
	// MPG: modified access to ModDynMap
	map< string,int >::iterator ModDynMap_iterator = ModDynMap.find(name);
	if (ModDynMap_iterator!=ModDynMap.end()){
	  //	if (ModDynMap.find(name) != ModDynMap.end()){
	  uint number = ModDynMap[name];
	  ModDynMap.erase(name);
	  if(number<MAX_DYN_HISTO){
	    ModDynMap_places[number] = false;
	    publish_modules(false,number);
	  } else {
	    ERS_PIX_ERROR("number="<<number<<" for module" << name << " is out of bounds.  Something very bad has happened!", MapError);
	    cout <<"ERROR: number="<<number<<" for module" << name << " is out of bounds.  Something very bad has happened!" << endl;
	  }
	  if (ModDynMap.empty()){
	    fill_single_modules = false;
	  }
	} else {
	  ERS_PIX_INFO("Module " << name << " was not filled");
	}
	++k;
      } else {
	ERS_PIX_INFO("Give me a module name with that");
	break;
      }
    }


    /*    
    //---------------------------------------------------------------------//
    // stops/starts the filling of ROD histograms depending on the string  //
    // given as a second argument. Features wildcards '*' as placeholders, //
    // wildcards can not be given at the beginning or end of the string    //
    // (if all RODS from crate 1 are to be enabled: +fR C1, not +fR C1*)   //
    // number of wildcards is limited to 3, more wildcards are ignored     //
    //---------------------------------------------------------------------//
    else if (strcmp(argv[k],"-fR")==0){
      if (argv[k+1]!=NULL){
	int num_wildcards = 0;
	string rod_name;
	// this stores the different parts of the commandstring, seperated by
	// wildcards
	char command[4][20] = {};
	int end_last_cmd = 0;  // position of end of last command
	int command_size = strlen(argv[k+1]);
	for (int i=0; i<command_size; ++i){
	  if (argv[k+1][i]=='*'){
	    num_wildcards++;  // count the number of wildcards
	    end_last_cmd = i+1;
	  }
	  else command[num_wildcards][i-end_last_cmd] = argv[k+1][i];
	  if (num_wildcards>3) continue;
	}
	int current_wildcard;
	int matched_commands;
	for (map< string,pair<int,int> >::iterator
	       RODNamMap_iterator=RODNamMap.begin();
	     RODNamMap_iterator!=RODNamMap.end(); ++RODNamMap_iterator){
	  current_wildcard = 0;
	  matched_commands = 0;
	  rod_name = RODNamMap_iterator->first;
	  // Check if the ROD name contains the argument given
	  for (; current_wildcard <= num_wildcards; ++current_wildcard)
	    if (rod_name.find(command[current_wildcard])!=string::npos)
	      matched_commands++;
	  if (matched_commands-1==num_wildcards){  // all command stings found
	    cout << "Now stopping filling of " << rod_name
		 << " ROD histograms" << endl;
	    checkArrayBounds(RODNamMap_iterator->second.first,RODNamMap_iterator->second.second);
	    fill_ROD[RODNamMap_iterator->second.first][RODNamMap_iterator->second.second] = false;
	  }
	}
	k++;
      }
      else cout << "Argument missing, continuing" << endl;
    } else if (strcmp(argv[k],"+fR")==0){
      if (argv[k+1]!=NULL){
	int num_wildcards = 0;
	string rod_name;
	// this stores the different parts of the commandstring, seperated by
	// wildcards
	char command[4][20] = {};
	int end_last_cmd = 0;  // position of end of last command
	int command_size = strlen(argv[k+1]);
	for (int i=0; i<command_size; ++i){
	  if (argv[k+1][i]=='*'){
	    num_wildcards++;  // count the number of wildcards
	    end_last_cmd = i+1;
	  }
	  else command[num_wildcards][i-end_last_cmd] = argv[k+1][i];
	  if (num_wildcards>3) continue;
	}
	int current_wildcard;
	int matched_commands;
	for (map< string,pair<int,int> >::iterator
	       RODNamMap_iterator=RODNamMap.begin();
	     RODNamMap_iterator!=RODNamMap.end(); ++RODNamMap_iterator){
	  current_wildcard = 0;
	  matched_commands = 0;
	  rod_name = RODNamMap_iterator->first;
	  // Check if the ROD name contains the argument given
	  for (; current_wildcard<=num_wildcards; ++current_wildcard)
	    if (rod_name.find(command[current_wildcard])!=string::npos)
	      matched_commands++;
	  if (matched_commands-1==num_wildcards){  // all command stings found
	    cout << "Now starting filling of " << rod_name
		 << " ROD histograms" << endl;
	    checkArrayBounds(RODNamMap_iterator->second.first,
			     RODNamMap_iterator->second.second);
	    fill_ROD[RODNamMap_iterator->second.first][RODNamMap_iterator->second.second] = true;
	  }
	}
	k++;
      }
      else cout << "Argument missing, continuing" << endl;
    }
    */
      
    /*
    else if (strcmp(argv[k],"+eO")==0){
      if (argv[k+1]!=NULL){
	expected_occupancy = atof(argv[k+1]);
	ERS_PIX_INFO("Set expected occupancy to " << expected_occupancy);;
	ERS_PIX_INFO("Not adjusting occupancy automatically anymore");
	adjust_occupancy = false;
	k++;
      } else {
	ERS_PIX_INFO("Argument missing, continuing");
      }
    } else if (strcmp(argv[k],"-eO")==0){
      adjust_occupancy = true;
      ERS_PIX_INFO("Automatically adjusting expected occupancy");
    } else if (strcmp(argv[k],"+oa")==0){
      if (argv[k+1]!=NULL){
	occupancy_acceptance = atof(argv[k+1]);
	adjust_occupancy = true;
	ERS_PIX_INFO("Set occupancy acceptance to " << occupancy_acceptance);
	ERS_PIX_INFO("Automatically adjusting expected occupancy");
	k++;
      } else {
	ERS_PIX_INFO("Argument missing, continuing");
      }
    }
    */
    else {
      ERS_PIX_INFO("unknown command, continuing");
    }
  }

  if (PrintOutLevel>=1){
    printf("PixelHisto: Leaving customCommand\n");
    fflush(stdout);
  }
 }
 catch(std::exception& e){
   cout<<"Exception caught in PixelHisto::customCommand of type std::exception: " << e.what() << endl;
 }
 catch(char const* msg){
   cout<<"Exception caught in PixelHisto::customCommand of type char const*: " << msg << endl;
 }
 catch(...){
   cout<<"Exception caught in PixelHisto::customCommand of unknown type." << endl;
 }
}


// -------------------------
// Access to Connectivity DB
// -------------------------

void
fillConnectivity()
{
  if (PrintOutLevel>=1){
    printf("PixelHisto: Entering fillConnectivity\n");
    fflush(stdout);
  }

  std::string applName="PixRCDMonitoring" + m_ros_name;
  
  IPCPartitionNameDD = getenv("TDAQ_PARTITION");
  if (IPCPartitionNameDD == "") {
    ERS_PIX_FATAL( "PixRCDMonitoring::setup", " ERROR!! Could not read 'TDAQ_PARTITION' from XML files");
    exit(-1);
  }
  std::cout << "Trying to connect to partition " << IPCPartitionNameDD << " ... " << std::endl;

  try {
    if (ipcPartitionDD != NULL) delete ipcPartitionDD;
    ipcPartitionDD = new IPCPartition(IPCPartitionNameDD);
  } catch(...) {
    cout << "FATAL ERROR: PixRCDMonitoring: ERROR!! Problems while connecting to the partition"+ IPCPartitionNameDD << endl;
    ERS_PIX_FATAL("FATAL! PixRCDMonitoring: ERROR!! Problems while connecting to the partition"+ IPCPartitionNameDD, ConnError );
    exit(-1);
  }
  std::cout << "Succefully connected to partition " << IPCPartitionNameDD << std::endl << std::endl;
  
  ERS_PIX_INFO("Access to Connectivity DB");

  // Reset the IS variables
  for (uint rod=0; rod<MAX_ROD_PER_ROS; ++rod){
    is_rod_BCID_errors[rod] = 0;
    is_rod_LVL1ID_errors[rod] = 0;
    is_rod_timeout_errors[rod] = 0;
    is_rod_dataincorrect_errors[rod] =0;
    is_rod_overflow_errors[rod]=0;
    is_rod_fragment_size[rod]=0;
    for (uint ob=0; ob<MAX_OB_PER_ROD; ++ob){
      for (uint mod=0; mod<MAX_MOD_PER_OB; ++mod){
	is_mod_timeout_errors[rod][ob][mod]=0;
	is_mod_LVL1ID_errors[rod][ob][mod]=0;
	is_mod_BCID_errors[rod][ob][mod]=0;
	is_mod_preamble_errors[rod][ob][mod]=0;
	is_mod_trailer_errors[rod][ob][mod]=0;
	is_mod_limit_errors[rod][ob][mod]=0;
	is_mod_overflow_errors[rod][ob][mod]=0;

	is_mod_IblDbmFEBCID_errors[rod][ob][mod]=0;
	is_mod_IblDbmFEHamming_errors[rod][ob][mod]=0;
	is_mod_IblDbmFEL1_errors[rod][ob][mod]=0;

	is_mod_FEEOC_errors[rod][ob][mod]=0;
	is_mod_FEHamming_errors[rod][ob][mod]=0;
	is_mod_FEregparity_errors[rod][ob][mod]=0;
	is_mod_FEhitparity_errors[rod][ob][mod]=0;
	is_mod_MCChitoverflow_errors[rod][ob][mod]=0;
	is_mod_MCCEoEoverflow_errors[rod][ob][mod]=0;
	is_mod_MCCL1ID_errors[rod][ob][mod]=0;
	is_mod_MCCBCIDEoE_errors[rod][ob][mod]=0;
	is_mod_MCCL1IDEoE_errors[rod][ob][mod]=0;
      }
    }
  }
  
  //  PartitionConnectivity* PartConn = NULL;
  RosConnectivity* RosConn = NULL;
  RobinConnectivity* RobinConn = NULL;
  RolConnectivity* RolConn = NULL;
  //  RodCrateConnectivity* RodCrateConn = NULL;
  RodBocConnectivity* RodBocConn = NULL;
  OBConnectivity* OBConn = NULL;
  Pp0Connectivity* Pp0Conn = NULL;
  ModuleConnectivity* ModConn = NULL;
  //   PixModuleGroup* ModGroup = NULL;

  //  map<string,PartitionConnectivity*>::iterator part_iterator;
  map<string,RosConnectivity*>::iterator ros_iterator;
  // map<string,RobinConnectivity*>::iterator robin_iterator;
  map<string,RolConnectivity*>::iterator rol_iterator;
  //  map<string,RodCrateConnectivity*>::iterator crate_iterator;
  map<int,RodBocConnectivity*>::iterator rod_iterator;

  uint rod_number = 0;
  string ros_name, rol_name, robin_name, rod_name, ob_name, Pp0_name, mod_name;

  string mod_offline_ID;
  int mod_EC, mod_Disk, mod_phi, mod_eta;

  pair<int,int> ModMap_Key;
  int RODMap_Key;
  vector<int> OBMap_Key;

  int outLink;

  //  bool active, enabled;
  string activeP, activeC, enabledC;
  string activeR, enabledR, activeM, enabledM;
  string activeB, active0;

  pair< map< pair<int,int>,vector<int> >::iterator,bool > ModLogRet;
  //unused: pair< map< int, int >::iterator,bool > RODLogRet;

  ERS_PIX_INFO("***** CREATING NEW PIXCONNECTIVITY *****");
  string IPCPartitionName = "";
  IPCPartitionName = getenv("TDAQ_PARTITION_INFRASTRUCTURE");

  if (IPCPartitionName == "") {
    cout << "FATAL ERROR: PixRCDMonitoring::setup: ERROR!! Could not read 'TDAQ_PARTITION_INFRASTRUCTURE' from XML files, exiting" << endl;
    ERS_PIX_FATAL("PixRCDMonitoring::setup: ERROR!! Could not read 'TDAQ_PARTITION_INFRASTRUCTURE' from XML files, exiting",ConnError);
  }

  ERS_PIX_INFO("Trying to connect to partition %s ...\n" << IPCPartitionName);
  IPCPartition* ipcPartition = NULL;
  try {
    ipcPartition = new IPCPartition(IPCPartitionName);
  } catch(...){
    ERS_PIX_FATAL("FATAL! Partition " << IPCPartitionName << " could not be created.", ConnError);
    cout << "FATAL ERROR: Partition " << IPCPartitionName << " could not be created." << endl;
    exit(-1);
  }
  ERS_PIX_INFO("Successfully connected to partition " << IPCPartitionName);

  // Get the IS manager
  try{
    if (is_manager != NULL) delete is_manager;
    is_manager = new PixISManager(ipcPartition,"RunParams");
  }catch(...) {
    ERS_PIX_ERROR("Could not connect to IS server RunParams, no publishing to IS");
  }
  if (is_manager!=NULL) {
    try{
      is_manager->removeVariables("Nevents");
      is_manager->removeVariables("_errors");
      is_manager->removeVariables("NHits");
      is_manager->removeVariables("FragmentSize");
      is_manager->removeVariables("SampledEvents");
      printf("IS variables deleted before start: \n");
      fflush(stdout);
    }
    catch (...){
    }
  }
  // Get the IS dictionary
  auto info_dictionary = std::make_unique<ISInfoDictionary>(*ipcPartition);
  std::string is_name_for_tags = "PixelRunParams";
  std::string is_var_header = is_name_for_tags + ".DataTakingConfig-";
  std::string var_name = "Disable";
  ISInfoString is_string;
  info_dictionary->getValue( (is_var_header+var_name).c_str(),is_string);
  std::string disable_name = is_string.getValue();

  // Get the IS manager to read the tags from
  PixISManager *is_manager_pixel = NULL;
  try {
    is_manager_pixel = new PixISManager(ipcPartition, "PixelRunParams");
  } catch(...) {
    ERS_PIX_ERROR("Could not connect to IS server PixelRunParams, read tags from xml");
  }
  // read the run tags from the is manager
  printf("\n Before All Check: \n");
  printf("Using Tag: %s / %s / %s / %s \n\n",id_tag.c_str(),conn_tag.c_str(),cfg_tag.c_str(), disable_name.c_str());
 
  std::string cfgTag="", connTag="", idTag="";
  if (is_manager_pixel!=0){
    try {
      connTag = is_manager_pixel->read<string>("DataTakingConfig-ConnTag");
    } catch (...) {
      ERS_PIX_WARNING("Cannot read DataTakingConfig-ConnTag from IS, reading from environment ....");
    }	
    try {
      cfgTag = is_manager_pixel->read<string>("DataTakingConfig-CfgTag");
    } catch (...) {
      ERS_PIX_WARNING( "Cannot read DataTakingConfig-CfgTag from IS, reading from environment ....");
    }
    try {
      idTag = is_manager_pixel->read<string>("DataTakingConfig-IdTag");
    } catch (...) {
      ERS_PIX_WARNING("Cannot read DataTakingConfig-IdTag from IS, reading from environment ....");
    }
    try {
      disable_name = is_manager_pixel->read<string>("DataTakingConfig-Disable");
    } catch (...) {
      ERS_PIX_WARNING("Cannot read DataTakingConfig-Disable from IS, reading from environment ....");
    }
  }
  printf("\n After Reading from IS: \n");
  printf("Using Tag: %s / %s / %s / %s\n\n",idTag.c_str(),connTag.c_str(),cfgTag.c_str(),disable_name.c_str());

  if (idTag=="")     idTag=getenv("ID_TAG");
  if (connTag=="") connTag=getenv("CONNECTIVITY_TAG_C");
  if (cfgTag=="")  cfgTag=getenv("CONNECTIVITY_TAG_CF");
  
  if (idTag=="") cout << "Cannot read ID_TAG from PixelEnv, using the one in GNAM XML ...." << endl;  
  else id_tag=idTag;
  
  if (connTag=="") cout << "Cannot read CONNECTIVITY_TAG_C from PixelEnv, using the one in GNAM XML ...." << endl;  
  else conn_tag=connTag;

  if (cfgTag=="") cout << "Cannot read CONNECTIVITY_TAG_CF from PixelEnv, using the one in GNAM XML ...." << endl;  
  else cfg_tag=cfgTag;


  PixDbServerInterface* dbServer = NULL;
  PixConnectivity* Conn = NULL;

  if (use_PixDBServer){
    string serverName = getenv("PIXDBSERVER_NAME");
    bool ready = false;
    int nTry = 0;
    ERS_PIX_INFO("Trying to connect to DbServer with name " << serverName);
    do {
      sleep(1);
      try {
	dbServer = new PixDbServerInterface(ipcPartition,serverName,
					    PixDbServerInterface::CLIENT,
					    ready);
      } catch(...){
	ERS_PIX_WARNING("Could not create db server interface.", ConnError);
      }
      if (!ready){
	if (dbServer != NULL) delete dbServer;
	dbServer = NULL;
      }
      else break;
      ERS_PIX_INFO("...attempt number " << dec << nTry);
      nTry++;
    } while(nTry<20);

    if(!ready){
      cout << "FATAL ERROR: Impossible to connect to DbServer " << serverName << endl;
      ERS_PIX_FATAL( "Could not connect to DbServer " + serverName + ", exiting");
      exit(-1);
    } else {
      ERS_PIX_INFO("Successfully connected to DbServer with name " << serverName);
    }

    dbServer->ready();
    ERS_PIX_INFO("DbServer has been enabled for reading");
    ERS_PIX_INFO("Exited from ready() status\n");

    ERS_PIX_INFO("Configuration parameters:");
    const char* name = getenv("CORAL_AUTH_PATH");

    if (name!=NULL){
      ERS_PIX_INFO("CORAL_AUTH_PATH is " << name);
    } else {
      ERS_PIX_WARNING("CORAL_AUTH_PATH not found",ConnError);
    }
    ERS_PIX_INFO("ServerName: " << serverName);
    ERS_PIX_INFO("Connection Tag: " << conn_tag);
    ERS_PIX_INFO("Config Tag: " << cfg_tag);
    ERS_PIX_INFO("Id Tag: " << id_tag);

    if (name!=NULL) printf("CORAL_AUTH_PATH is %s\n",name);
    else printf("CORAL_AUTH_PATH not found\n");
    printf("ServerName: %s\n",serverName.c_str());
    printf("Connection Tag: %s\n",conn_tag.c_str());
    printf("Config Tag: %s\n",cfg_tag.c_str());
    printf("Id Tag: %s\n",id_tag.c_str());
    
    try {
      Conn = new PixConnectivity(dbServer,conn_tag,conn_tag,
				 conn_tag,cfg_tag,id_tag);
    }
    catch (PixConnectivityExc e){
      ERS_PIX_WARNING("Exception caught in fillConnectivity: " << e.getDescr(),ConnError);
    }
    catch (...){
      ERS_PIX_WARNING("Unknown exception caught in fillConnectivity " << "(PixConnectovity constructor)",ConnError);
    }
    ERS_PIX_INFO("***** NEW PIXCONNECTIVITY CREATED! *****");
  } else {
    ERS_PIX_INFO("Configuration parameters:");
    ERS_PIX_INFO("Connection Tag: " << conn_tag);
    ERS_PIX_INFO("Config Tag: " << cfg_tag);
    ERS_PIX_INFO("Id Tag: " << id_tag);

    printf("-----------------------------------\n");
    printf("        Printing System env\n");
    printf("----------------------------------- \n");
    printf("Connection Tag: %s\n",conn_tag.c_str());
    printf("Config Tag: %s\n\n",cfg_tag.c_str());
    printf("Id Tag: %s\n\n",id_tag.c_str());
    try {
      Conn = new PixConnectivity(conn_tag,conn_tag,
				 conn_tag,cfg_tag,id_tag);
    }
    catch (PixConnectivityExc e){
      ERS_PIX_WARNING("Exception caught in fillConnectivity: " << e.getDescr(),ConnError);
    }
    catch (...){
      ERS_PIX_WARNING("Unknown exception caught in fillConnectivity " << "(PixConnectivity constructor)",ConnError);
    }
    ERS_PIX_INFO("***** NEW PIXCONNECTIVITY CREATED! *****");
  }

  try {
    Conn->loadConn();
  }
  catch (PixConnectivityExc e){ 
    ERS_PIX_WARNING("Exception caught in fillConnectivity at loadConn(): " << e.getDescr(),ConnError);
  }
  catch (...){
    ERS_PIX_WARNING("Unknown exception caught in fillConnectivity " << "at loadConn()",ConnError);
  }
  ERS_PIX_INFO("***** PIXCONNECTIVITY::loadConn() executed! *****");

  if (Conn!=NULL){
    // now read the disable object
    ERS_PIX_DEBUG(1, "Read disabled object " << disable_name);
    cout << "Read disabled object " << disable_name << endl;
    auto pix_disable = std::make_unique<PixLib::PixDisable>(disable_name);
    Conn->readDisable(pix_disable.get(),disable_name);

    ERS_PIX_DEBUG(1,"Connectivity contains " << Conn->roses.size()
		  << " ROSes");
    cout << "Connectivity contains " << Conn->roses.size() << " ROSes" << endl;

    // loop over ROSes
    int ros_i=-1;
    for (ros_iterator=Conn->roses.begin(); ros_iterator!=Conn->roses.end(); ++ros_iterator){
      ++ros_i;
      RosConn = ros_iterator->second;
      if (RosConn==NULL) continue;
      ros_name = RosConn->name();
      ERS_PIX_DEBUG(1,ros_name << " found");
      cout << ros_name << " found" << endl;
      if (ros_name!=m_ros_name){ // This is not the right ROS
	ERS_PIX_DEBUG(1,"Skipping for ROS " << m_ros_name);
	continue;
      } else {
	ERS_PIX_DEBUG(1,"Matching ROS");
	cout << "Matching ROS" << endl;
      }

      // loop over ROBINS
      for (uint robin_number=0; robin_number<ROBIN_PER_ROS; ++robin_number){
	RobinConn = RosConn->robins(robin_number);
	if (RobinConn==NULL) continue;
	robin_name = RobinConn->name();
	ERS_PIX_DEBUG(1,"ROBIN " << robin_name << " found");

	// loop over ROLs
	for (uint rol_number=0; rol_number<ROL_PER_ROBIN; ++rol_number){
	  if(rod_number >= MAX_ROD_PER_ROS){
	    ERS_PIX_ERROR("rod_number="<<rod_number<<" has gotten larger than MAX_ROD_PER_ROS("<<MAX_ROD_PER_ROS<<"): " << "this should never happen.",MapError);
	    cout<<"ERROR: rod_number="<<rod_number<<" out of range...  Break the loop!"<<endl;
	    break;
	  }	  
	  RolConn = RobinConn->rols(rol_number);
	  if (RolConn==NULL) continue;
	  rol_name = RolConn->name();
	  // find corresponding ROD
	  RodBocConn = RolConn->rod();
	  if (RodBocConn == NULL) continue;
	  rod_name = RodBocConn->name();
	  string readoutSpeed = "SINGLE_40"; // defaultvalue
	  string readoutSpeedVariableName = "DataTakingSpeed-"+rod_name;
	  if (is_manager_pixel->exists(readoutSpeedVariableName)){
	    readoutSpeed = is_manager_pixel->read<string>(readoutSpeedVariableName);
	    cout << "found readout speed " << readoutSpeed << " for ROD " << rod_name << endl;
	  }
	  else{
	    ERS_PIX_DEBUG(1,"Could not find IS varaible " << readoutSpeedVariableName << " in PixelRunParams");
	  }
	  
	  if(rod_name.size()>=MAX_ROD_NAME_LENGTH){
	    ERS_PIX_ERROR("rod_name="<<rod_name<<" is longer than MAX_ROD_NAME_LENGTH("<<MAX_ROD_NAME_LENGTH<<"): " << "this should never happen.",MapError);
	    cout <<"ERROR: rod_name="<<rod_name<<" is too long...  Try continue with truncated name... ";
	    rod_name = rod_name.substr(0,MAX_ROD_NAME_LENGTH);
	    cout<<rod_name<<endl;
	  }

	  sprintf(rodnames[rod_number], "%s", rod_name.c_str()); // needed for IS
	  int source_id = strtol(RodBocConn->offlineId().c_str(),NULL,16);  // source id for ROD (not ROL)
	  ERS_PIX_INFO("Got source_id: " << hex << source_id << dec);
      
	  if( isIblOrDbm(source_id) ){
	    // IBL and DBM have four ROL_IDs per ROD
	    // JOE: I don't think this is really needed for subDetIdList, but will leave it for now
	    const_cast<PixelEvent*>(pixel)->subDetIdList.push_back( (source_id & 0xfffffff0) + 0);
	    const_cast<PixelEvent*>(pixel)->subDetIdList.push_back( (source_id & 0xfffffff0) + 1);
	    const_cast<PixelEvent*>(pixel)->subDetIdList.push_back( (source_id & 0xfffffff0) + 2);
	    const_cast<PixelEvent*>(pixel)->subDetIdList.push_back( (source_id & 0xfffffff0) + 3);
	  }else if(hasUpgradedReadout(source_id)){ //diane 
	    const_cast<PixelEvent*>(pixel)->subDetIdList.push_back( (source_id & 0xffffff3f) + 0x00);
	    const_cast<PixelEvent*>(pixel)->subDetIdList.push_back( (source_id & 0xffffff3f) + 0x40);
	  }else{
	    const_cast<PixelEvent*>(pixel)->subDetIdList.push_back(source_id);
	  }


	  /*
	  //new active treatment with PixDisable
	  active = RodBocConn->active();
	  enabled = RodBocConn->enableReadout;
	  if (PrintOutLevel>=1){
	    activeR = "inactive";
	    enabledR = "disabled";
	    if (active) activeR = "active";
	    if (enabled) enabledR = "enabled";
	    ERS_PIX_DEBUG(2,"ROL " << rol_name << ", ROD " << rod_name
			  << " (" << activeR << " and " << enabledR
			  << ") -> " << rod_number << " (source_id "
			  << showbase << hex << source_id << dec << ")");
	  }
	  if (!active || !enabled){
	    active = true;
	    enabled = true;
	    if (PrintOutLevel>=1) printf("\n");
	    continue;
	  }
	  */  

	  if (pix_disable->isDisable(rod_name) || pix_disable->isDisable(RodBocConn->crate()->name()) || !(RodBocConn->active()) || !(RodBocConn->enableReadout) /* needed because disable not propagated from crate */){ // insert all modules of this ROD in the disable map
	    for (uint ob_number=0; ob_number<MAX_OB_PER_ROD; ++ob_number){
	      OBConn = RodBocConn->obs(ob_number);
	      if (OBConn==NULL) continue;
	      Pp0Conn = OBConn->pp0();
	      if (Pp0Conn==NULL) continue;
	      insert_disabled_modules(Pp0Conn, readoutSpeed, source_id, rod_number, ob_number);
	    }
	    continue;
	  }

	  RODMap_Key = rod_number;
	  // map source_id <-> rod number
	  // RODLogRet = RODLogMap.insert(make_pair(source_id,RODMap_Key));
	  if( isIblOrDbm(source_id) ){
	    // IBL and DBM have four ROL_IDs per ROD
	    // JOE: this is definitely needed
	    RODLogMap.insert(make_pair( (source_id & 0xfffffff0) + 0, RODMap_Key));
	    RODLogMap.insert(make_pair( (source_id & 0xfffffff0) + 1, RODMap_Key));
	    RODLogMap.insert(make_pair( (source_id & 0xfffffff0) + 2, RODMap_Key));
	    RODLogMap.insert(make_pair( (source_id & 0xfffffff0) + 3, RODMap_Key));
	  }else if(hasUpgradedReadout(source_id)){
	    RODLogMap.insert(make_pair( (source_id & 0xffffff3f) + 0x00, RODMap_Key));
	    RODLogMap.insert(make_pair( (source_id & 0xffffff3f) + 0x40, RODMap_Key));
	  }else{
	    RODLogMap.insert(make_pair(source_id, RODMap_Key));
	  }
	  
	  // rod number <-> rod name
	  RODGeoMap.insert(make_pair(RODMap_Key,rod_name));
	  // rod name <-> rod number
	  RODNamMap.insert(make_pair(rod_name,RODMap_Key));

	  // loop over optoboards: 0...max possible
	  for (uint ob_number=0; ob_number<MAX_OB_PER_ROD; ++ob_number){
	    OBConn = RodBocConn->obs(ob_number);
	    if (OBConn==NULL) continue;
	    ob_name = OBConn->name();

	    /*
	    active = OBConn->active();
	    if (PrintOutLevel>=1){
	      activeB = "inactive";
	      if (active) activeB = "active";
	      ERS_PIX_DEBUG(2,"OB " << ob_name << " ("
			    << activeB << ") -> " << ob_number);
	    }
	    if (!active || !enabled){
	      active = true;
	      enabled = true;
	      if (PrintOutLevel>=1) printf("\n");
	      continue;
	    }
	    */

	    if (pix_disable->isDisable(ob_name) || !(OBConn->active())){ // insert all modules of this OB in the disable map
	      Pp0Conn = OBConn->pp0();
	      if (Pp0Conn==NULL) continue;
	      insert_disabled_modules(Pp0Conn, readoutSpeed, source_id, rod_number, ob_number);
	      continue;
	    }
	    
	    OBMap_Key.clear();
	    OBMap_Key.push_back(rod_number);
	    OBMap_Key.push_back(ob_number);
	    // fill OBGeoMap for the optoboards
	    OBGeoMap.insert(make_pair(OBMap_Key,ob_name));
	    Pp0Conn = OBConn->pp0();
	    if (Pp0Conn==NULL) continue;
	    Pp0_name = Pp0Conn->name();

	    /*
	    active = Pp0Conn->active();
	    if (PrintOutLevel>=1){
	      active0 = "inactive";
	      if (active) active0 = "active";
	      ERS_PIX_DEBUG(2,"Pp0 " << Pp0_name << " ("
			    << active0 << ")");
	    }
	    if (!active || !enabled){
	      active = true;
	      enabled = true;
	      if (PrintOutLevel>=1) printf("\n");
	      continue;
	    }
	    */

	    if (pix_disable->isDisable(Pp0_name) || !(Pp0Conn->active())){ // insert all modules of this pp0 in the disable map
	      insert_disabled_modules(Pp0Conn, readoutSpeed, source_id, rod_number, ob_number);
	      continue;
	    }

	    // loop over modules: 1..MAX_MOD_PER_OB
	    for (uint mod_number=1; mod_number<=MAX_MOD_PER_OB; ++mod_number){
	      ModConn = Pp0Conn->modules(mod_number);
	      if (ModConn==NULL) continue;
	      mod_name = ModConn->name();

	      // decode Offline ID format: [2.1.EC.Disk.phi.eta.0]
	      mod_offline_ID = ModConn->offlineId();

	      int points[6];
	      points[0] = mod_offline_ID.find(".");
	      for (int p = 1; p<6; ++p){
		points[p] = mod_offline_ID.find(".",points[p-1]+1);
	      }
	      
	      mod_EC   = atoi( mod_offline_ID.substr(points[1]+1, points[2]-points[1]-1) .c_str() );
	      mod_Disk = atoi( mod_offline_ID.substr(points[2]+1, points[3]-points[2]-1) .c_str() );
	      mod_phi  = atoi( mod_offline_ID.substr(points[3]+1, points[4]-points[3]-1) .c_str() );
	      mod_eta  = atoi( mod_offline_ID.substr(points[4]+1, points[5]-points[4]-1) .c_str() );
	      
      	      // the smallest link number is always sent with the bytestream
	      outLink = getOutlink(ModConn, readoutSpeed, mod_name, &source_id); // modifies source_id for IBL to be for ROL
	      std::cout << std::hex << "Info: " << source_id << std::dec << ", mod_name=" << mod_name << std::endl;
	      
	      if (pix_disable->isDisable(mod_name) || !(ModConn->active()) || !(ModConn->enableReadout)){
		insert_disabled_module(source_id, outLink, mod_name, mod_phi, rod_number, ob_number, mod_number);
		continue;
	      }
	      
 	      ++m_nmods; // increase number of modules in this GNAM application (only if module enabled/active)
	      insert_enabled_module(source_id, outLink, mod_name, mod_EC, mod_Disk, mod_phi, mod_eta, rod_number, ob_number, mod_number);

	      // needed for IS
	      string is_name = rod_name;
	      is_name += "/";
	      is_name += mod_name;
	      // MPG: modified argument (mod_number->mod_number-1)
	      sprintf(modulenames[rod_number][ob_number][mod_number-1],"%s",is_name.c_str());
	    }
	    if (PrintOutLevel>=1) printf("\n");
	  }
	  if (PrintOutLevel>=1) printf("\n");
	  ++rod_number;
	  
	}
      }
      m_nrods = rod_number; // total number of RODs in this ROS connectivity
      rod_number = 0;
      if (PrintOutLevel>=1) printf("\n");
      m_ros_number = ros_i; // assumes only one ROS per application
    }
    if (PrintOutLevel>=1) printf("\n");
    cout << "Found a total of " << m_nrods << " RODs and " << m_nmods << " modules in the connectivity" << endl;

  } else {
    ERS_PIX_WARNING("PixConnectivity object not found, " << "no connectivity information loaded",ConnError);
    cout <<"WATNING: PixConnectivity object not found, " << "no connectivity information loaded" << endl;
  }

  std::cout << "List of sub-detector IDs being considered by this instance: " << std::hex;
  std::copy(pixel->subDetIdList.begin(), pixel->subDetIdList.end(), std::ostream_iterator<uint32_t>(std::cout, ";"));
  std::cout << std::dec << std::endl;

  if (Conn != NULL) delete Conn;
  Conn = NULL;
  if (dbServer != NULL) delete dbServer;
  dbServer = NULL;
  if (is_manager_pixel != NULL)  delete is_manager_pixel;
  is_manager_pixel = NULL;
  if (ipcPartition != NULL) delete ipcPartition;
  ipcPartition = NULL;

  if (PrintOutLevel>=1){
    printf("PixelHisto: Leaving fillConnectivity\n");
    fflush(stdout);
  }
}


// --------------------------
// Histograms booking methods
// --------------------------

void
publish_modules(bool publish_flag, int number)
{
  if (PrintOutLevel>=1){
    printf("PixelHisto: Entering publish_modules\n");
    fflush(stdout);
  }
  if(number < (int)MAX_DYN_HISTO){
    //---------------------------------------------
    // Set if per module information is published
    //---------------------------------------------
    module_hitmap[number]->IsPublished(publish_flag);
    module_tot[number]->IsPublished(publish_flag);
    module_tot_cluster[number]->IsPublished(publish_flag);	  
    module_tot_cluster_size[number]->IsPublished(publish_flag);
    module_hittime[number]->IsPublished(publish_flag);
    module_ercode[number]->IsPublished(publish_flag);
  }else{
    ERS_PIX_WARNING("number="<<number<< " is out of bounds. Ignoring!", MapError);
  }
  
  if (PrintOutLevel>=1){
    printf("PixelHisto: Leaving publish_modules\n");
    fflush(stdout);
  }
}

void
book_module_histograms()
{
  if (PrintOutLevel>=1){
    printf("PixelHisto: Entering book_module_histograms\n");
    fflush(stdout);
  }
  //---------------------------------------------
  // book histograms hitmap/ToT/timing/errorflags for each module
  // called by bookHisto
  //---------------------------------------------
  char name[256];
  for (uint i=0; i<MAX_DYN_HISTO; ++i){
    
    sprintf(name,"/SHIFT/Pixel/Modules/Hitmap-%i",i);
    module_hitmap[i] = new GnamHisto(name,name,
				     MAX_ROD_NUMBER,-0.5,MAX_ROD_NUMBER-0.5,
				     MAX_MOD_PER_ROD,-0.5,MAX_MOD_PER_ROD-0.5);
    module_hitmap[i]->GetXaxis()->SetTitle("Column");
    module_hitmap[i]->GetYaxis()->SetTitle("Row");
    GH_RESET_AT_WASTART(module_hitmap[i]);
    histolist->push_back(module_hitmap[i]);

    sprintf(name,"/SHIFT/Pixel/Modules/ToT-%i",i);
    module_tot[i] = new GnamHisto(name,name,256,-0.5,255.5, GnamHisto::INTBIN);
    module_tot[i]->GetXaxis()->SetTitle("ToT");
    module_tot[i]->GetYaxis()->SetTitle("Number");
    GH_RESET_AT_WASTART(module_tot[i]);
    histolist->push_back(module_tot[i]);

    sprintf(name,"/SHIFT/Pixel/Modules/Cluster-%i",i);
    module_tot_cluster[i] = new GnamHisto(name,name,256,-0.5,255.5, GnamHisto::INTBIN);
    module_tot_cluster[i]->GetXaxis()->SetTitle("ToT");
    module_tot_cluster[i]->GetYaxis()->SetTitle("Number");
    GH_RESET_AT_WASTART(module_tot_cluster[i]);
    histolist->push_back(module_tot_cluster[i]);

    sprintf(name,"/SHIFT/Pixel/Modules/ClusterSize-%i",i);
    module_tot_cluster_size[i] = new GnamHisto(name,name,10,-0.5,9.5);
    module_tot_cluster_size[i]->GetXaxis()->SetTitle("ToT_Cluster_Size");
    module_tot_cluster_size[i]->GetYaxis()->SetTitle("Number");
    GH_RESET_AT_WASTART(module_tot_cluster_size[i]);
    histolist->push_back(module_tot_cluster_size[i]);

    sprintf(name,"/SHIFT/Pixel/Modules/HitTime-%i",i);   
    module_hittime[i] = new GnamHisto(name,name,40,-19.5,19.5);
    module_hittime[i]->GetXaxis()->SetTitle("L1A");
    module_hittime[i]->GetYaxis()->SetTitle("Number");
    GH_RESET_AT_WASTART(module_hittime[i]);
    histolist->push_back(module_hittime[i]);

    sprintf(name,"/SHIFT/Pixel/Modules/Errcode-%i",i);
    module_ercode[i] = new GnamHisto(name,name,24,-0.5,23.5);
    module_ercode[i]->GetXaxis()->SetTitle("ErrorCode");
    module_ercode[i]->GetYaxis()->SetTitle("Number");
    GH_RESET_AT_WASTART(module_ercode[i]);
    histolist->push_back(module_ercode[i]);

    sprintf(name,"/SHIFT/Pixel/Modules/NumberOfHits-%i",i);
    module_hits[i] = new GnamHisto(name,name,500,-0.5,499.5, GnamHisto::INTBIN);
    module_hits[i]->GetXaxis()->SetTitle("Number of Hits per Event");
    module_hits[i]->GetYaxis()->SetTitle("Occurence");
    GH_RESET_AT_WASTART(module_hits[i]);
    histolist->push_back(module_hits[i]);

    sprintf(name,"/SHIFT/Pixel/Modules/BCID-%i",i);
    module_bcid[i] = new GnamHisto(name,name,500,-0.5,499.5, GnamHisto::INTBIN);
    module_bcid[i]->GetXaxis()->SetTitle("BCID of Hits");
    module_bcid[i]->GetYaxis()->SetTitle("Occurence");
    GH_RESET_AT_WASTART(module_bcid[i]);
    histolist->push_back(module_bcid[i]);

    // sprintf(name,"/SHIFT/Pixel/Modules/LVL1IDErrors-%i",i);
    // module_lvl1id[i] = new GnamHisto(name,name,2,-0.5,1.5, GnamHisto::INTBIN);
    // module_lvl1id[i]->GetXaxis()->SetTitle("LVl1ID Errors");
    // module_lvl1id[i]->GetYaxis()->SetTitle("Occurence");
    // GH_RESET_AT_WASTART(module_lvl1id[i]);
    // histolist->push_back(module_lvl1id[i]);
    
    sprintf(name,"/SHIFT/Pixel/Modules/MonitoredEvents-%i",i);
    module_events[i] = new GnamHisto(name,name,1,-0.5,0.5, GnamHisto::INTBIN);
    module_events[i]->GetXaxis()->SetTitle("");
    module_events[i]->GetYaxis()->SetTitle("Number of Events");
    GH_RESET_AT_WASTART(module_events[i]);
    histolist->push_back(module_events[i]);
  }

  // book the module status and timing histograms, for all modules in the map
  for(map< vector<int>,string >::iterator it = ModGeoMap_all.begin(); it != ModGeoMap_all.end(); ++it){

    uint rod = (it->first)[0];
    uint ob = (it->first)[1];
    uint mod = (it->first)[2];
    const char* modname = it->second.c_str();
    if(rod >= MAX_ROD_PER_ROS){
      ERS_PIX_ERROR("rod="<<rod<<" >= MAX_ROD_PER_ROS("<<MAX_ROD_PER_ROS<<"): " << "this should never happen.",MapError);
      cout <<"ERROR: rod="<<rod<<" out of range...  Try to continue to next mod..."<<endl;
      continue;
    }
    if(ob >= MAX_OB_PER_ROD){
      ERS_PIX_ERROR("ob="<<ob<<" >= MAX_OB_PER_ROD("<<MAX_OB_PER_ROD<<"): " << "this should never happen.",MapError);
      cout <<"ERROR: ob="<<ob<<" out of range...  Try to continue to next mod..."<<endl;
      continue;
    }
    if(mod > MAX_MOD_PER_OB){
      ERS_PIX_ERROR("mod="<<mod<<" > MAX_MOD_PER_OB("<<MAX_MOD_PER_OB<<"): " << "this should never happen.",MapError);
      cout <<"ERROR: mod="<<mod<<" out of range...  Try to continue to next mod..."<<endl;
      continue;
    }

    sprintf(name,"/EXPERT/Pixel/Modules/%s-Status",modname);
    module_status[rod][ob][mod-1] = new GnamHisto(name,name,2,-0.5,1.5,GnamHisto::INTBIN);
    module_status[rod][ob][mod-1]->GetXaxis()->SetBinLabel(1,"Number of headers");
    module_status[rod][ob][mod-1]->GetXaxis()->SetBinLabel(2,"Enabled");
    if (disabled_modules.find(it->second) == disabled_modules.end()){ // module is not disabled
      module_status[rod][ob][mod-1]->SetBinContent(2,1);
    }
    histolist->push_back(module_status[rod][ob][mod-1]);

    sprintf(name,"/EXPERT/Pixel/Modules/%s-StatusRatio",modname);
    module_statusRatio[rod][ob][mod-1] = new GnamHisto(name,name,2,-0.5,1.5,GnamHisto::FLOATBIN);
    module_statusRatio[rod][ob][mod-1]->GetXaxis()->SetBinLabel(1,"Number of headers per event");
    module_statusRatio[rod][ob][mod-1]->GetXaxis()->SetBinLabel(2,"Enabled");
    if (disabled_modules.find(it->second) == disabled_modules.end()){ // module is not disabled
      module_statusRatio[rod][ob][mod-1]->SetBinContent(2,1);
    }
    histolist->push_back(module_statusRatio[rod][ob][mod-1]);
    
    sprintf(name,"/EXPERT/Pixel/Modules/%s-level1Skips",modname);
    module_level1Skips[rod][ob][mod-1] = new GnamHisto(name,name,21,-0.5,20,GnamHisto::INTBIN);
    module_level1Skips[rod][ob][mod-1]->GetXaxis()->SetTitle("Level1Skips");
    module_level1Skips[rod][ob][mod-1]->GetYaxis()->SetTitle("Occurence");
    GH_RESET_AT_WASTART(module_level1Skips[rod][ob][mod-1]);
    histolist->push_back(module_level1Skips[rod][ob][mod-1]);

    sprintf(name,"/EXPERT/Pixel/Modules/%s-level1Id",modname);
    module_level1Id[rod][ob][mod-1] = new GnamHisto(name,name,21,-0.5,20,GnamHisto::INTBIN);
    module_level1Id[rod][ob][mod-1]->GetXaxis()->SetTitle("Level1Id");
    module_level1Id[rod][ob][mod-1]->GetYaxis()->SetTitle("Occurence");
    GH_RESET_AT_WASTART(module_level1Id[rod][ob][mod-1]);
    histolist->push_back(module_level1Id[rod][ob][mod-1]);

    sprintf(name,"/EXPERT/Pixel/Modules/%s-ToT",modname);
    if(isIblOrDbm(modname)) module_ToT[rod][ob][mod-1] = new GnamHisto(name,name,17,-0.5,16.5,GnamHisto::INTBIN);
    else module_ToT[rod][ob][mod-1] = new GnamHisto(name,name,256,-0.5,255.5,GnamHisto::INTBIN);
    module_ToT[rod][ob][mod-1]->GetXaxis()->SetTitle("ToT");
    module_ToT[rod][ob][mod-1]->GetYaxis()->SetTitle("Occurence");
    GH_RESET_AT_WASTART(module_ToT[rod][ob][mod-1]);
    histolist->push_back(module_ToT[rod][ob][mod-1]);

    sprintf(name,"/EXPERT/Pixel/Modules/%s-Occupancy",modname);
    module_numHits[rod][ob][mod-1] = new GnamHisto(name,name,501,-0.5,500.5,GnamHisto::INTBIN);
    module_numHits[rod][ob][mod-1]->GetXaxis()->SetTitle("Number of Hits per Event");
    module_numHits[rod][ob][mod-1]->GetYaxis()->SetTitle("Occurence");
    GH_RESET_AT_WASTART(module_numHits[rod][ob][mod-1]);
    histolist->push_back(module_numHits[rod][ob][mod-1]);
    
    sprintf(name,"/EXPERT/Pixel/Modules/%s-LVL1IDErrors",modname);
    module_LVL1IDErrors[rod][ob][mod-1] = new GnamHisto(name,name,2,-0.5,1.5,GnamHisto::INTBIN);
    module_LVL1IDErrors[rod][ob][mod-1]->GetXaxis()->SetBinLabel(1,"LVL1 ID Errors");
    module_LVL1IDErrors[rod][ob][mod-1]->GetXaxis()->SetBinLabel(2,"Occurence");
    //   if (disabled_modules.find(it->second) == disabled_modules.end()){ // module is not disabled
    //      module_status[rod][ob][mod-1]->SetBinContent(2,1);
    //   }
    GH_RESET_AT_WASTART(module_LVL1IDErrors[rod][ob][mod-1]);
    histolist->push_back(module_LVL1IDErrors[rod][ob][mod-1]);
  }

  if (PrintOutLevel>=1){
    printf("PixelHisto: Leaving book_module_histograms\n");
    fflush(stdout);
  }
}


void
book_global_histograms()
{
  if (PrintOutLevel>=1){
    printf("PixelHisto: Entering book_global_histograms\n");
    fflush(stdout);
  }
  //---------------------------------------------
  // book global and global statistics histograms
  // called by bookHisto
  //---------------------------------------------

  char name[256], title[256];

  // book the histograms for the disabled modules
  sprintf(name,"/SHIFT/Pixel/Layer2_Disabled_Merged");
  sprintf(title,"Layer2 Disabled");
  ERS_PIX_DEBUG(2,"Boooking Layer2 Disabled under " << name);
  layer2_disabled = new GnamHisto(name,title,nmodperstave,-0.5,
				  nmodperstave-0.5,nstavelayer2,-0.5,
				  nstavelayer2-0.5,"paoGlo",
				  GnamHisto::INTBIN);
  layer2_disabled->GetXaxis()->SetTitle("module");
  layer2_disabled->GetYaxis()->SetTitle("stave");
  histolist->push_back(layer2_disabled);
  for (int i=0; i<nmodperstave; ++i)
    layer2_disabled->GetXaxis()->SetBinLabel(i+1,modlabel[i]);
  for (int i=0; i<nstavelayer2; ++i)
    layer2_disabled->GetYaxis()->SetBinLabel(i+1,stavelayer2[i]); 

  sprintf(name,"/SHIFT/Pixel/Layer1_Disabled_Merged");
  sprintf(title,"Layer1 Disabled");
  ERS_PIX_DEBUG(2,"Boooking Layer1 Disabled under " << name);
  layer1_disabled = new GnamHisto(name,title,nmodperstave,-0.5,
				  nmodperstave-0.5,nstavelayer1,-0.5,
				  nstavelayer1-0.5,"paoGlo",
				  GnamHisto::INTBIN);
  layer1_disabled->GetXaxis()->SetTitle("module");
  histolist->push_back(layer1_disabled);
  layer1_disabled->GetYaxis()->SetTitle("stave");
  for (int i=0; i<nmodperstave; ++i)
    layer1_disabled->GetXaxis()->SetBinLabel(i+1,modlabel[i]);
  for (int i=0; i<nstavelayer1; ++i)
    layer1_disabled->GetYaxis()->SetBinLabel(i+1,stavelayer1[i]); 

  sprintf(name,"/SHIFT/Pixel/Layer0_Disabled_Merged");
  sprintf(title,"Layer0 Disabled");
  ERS_PIX_DEBUG(2,"Boooking Layer0 Disabled under " << name);
  layer0_disabled = new GnamHisto(name,title,nmodperstave,-0.5,
				  nmodperstave-0.5,nstavelayer0,-0.5,
				  nstavelayer0-0.5,"paoGlo",
				  GnamHisto::INTBIN);
  layer0_disabled->GetXaxis()->SetTitle("module");
  layer0_disabled->GetYaxis()->SetTitle("stave");
  histolist->push_back(layer0_disabled);
  for (int i=0; i<nmodperstave; ++i)
    layer0_disabled->GetXaxis()->SetBinLabel(i+1,modlabel[i]);
  for (int i=0; i<nstavelayer0; ++i)
    layer0_disabled->GetYaxis()->SetBinLabel(i+1,stavelayer0[i]);

  sprintf(name,"/SHIFT/Pixel/LayerIbl_Disabled_Merged");
  sprintf(title,"LayerIbl Disabled");
  ERS_PIX_DEBUG(2,"Boooking LayerIbl Disabled under " << name);
  layerIbl_disabled = new GnamHisto(name,title,nmodperstaveIbl,-0.5,
				    nmodperstaveIbl-0.5,nstavelayerIbl,-0.5,
				    nstavelayerIbl-0.5,"paoGlo",
				    GnamHisto::INTBIN);
  layerIbl_disabled->GetXaxis()->SetTitle("module");
  layerIbl_disabled->GetYaxis()->SetTitle("stave");
  histolist->push_back(layerIbl_disabled);
  for (int i=0; i<nmodperstaveIbl; ++i)
    layerIbl_disabled->GetXaxis()->SetBinLabel(i+1,modlabelIbl[i]);
  for (int i=0; i<nstavelayerIbl; ++i)
    layerIbl_disabled->GetYaxis()->SetBinLabel(i+1,stavelayerIbl[i]);

  sprintf(name,"/SHIFT/Pixel/LayerDbm_Disabled_Merged");
  sprintf(title,"LayerDbm Disabled");
  ERS_PIX_DEBUG(2,"Boooking LayerDbm Disabled under " << name);
  layerDbm_disabled = new GnamHisto(name,title,nmodpertelescopeDbm,-0.5,
				    nmodpertelescopeDbm-0.5,ntelescopeDbm,-0.5,
				    ntelescopeDbm-0.5,"paoGlo",
				    GnamHisto::INTBIN);
  layerDbm_disabled->GetXaxis()->SetTitle("module");
  layerDbm_disabled->GetYaxis()->SetTitle("telescope");
  histolist->push_back(layerDbm_disabled);
  for (int i=0; i<nmodpertelescopeDbm; ++i)
    layerDbm_disabled->GetXaxis()->SetBinLabel(i+1,modlabelDbm[i]);
  for (int i=0; i<ntelescopeDbm; ++i)
    layerDbm_disabled->GetYaxis()->SetBinLabel(i+1,telescopeDbm[i]);

  sprintf(name,"/SHIFT/Pixel/DiskA_Disabled_Merged");
  sprintf(title,"Disk A Disabled");
  ERS_PIX_DEBUG(2,"Boooking Disk Disabled under " << name);
  diskA_disabled = new GnamHisto(name,title,ndisks,-0.5,ndisks-0.5,
				 nphiperdisk,-0.5,nphiperdisk-0.5,
				 "paoGlo",GnamHisto::INTBIN);
  histolist->push_back(diskA_disabled);
  for (int i=0; i<ndisks; ++i)
    diskA_disabled->GetXaxis()->SetBinLabel(i+1,disks[i]);
  for (int i=0; i<nphiperdisk; ++i)
    diskA_disabled->GetYaxis()->SetBinLabel(i+1,nstaveA[i]);

  sprintf(name,"/SHIFT/Pixel/DiskC_Disabled_Merged");
  sprintf(title,"Disk C Disabled");
  ERS_PIX_DEBUG(2,"Boooking Disk Disabled under " << name);
  diskC_disabled = new GnamHisto(name,title,ndisks,-0.5,ndisks-0.5,
				 nphiperdisk,-0.5,nphiperdisk-0.5,
				 "paoGlo",GnamHisto::INTBIN);
  histolist->push_back(diskC_disabled);
  for (int i=0; i<ndisks; ++i)
    diskC_disabled->GetXaxis()->SetBinLabel(i+1,disks[i]);
  for (int i=0; i<nphiperdisk; ++i)
    diskC_disabled->GetYaxis()->SetBinLabel(i+1,nstaveC[i]);




  // fill the module in the disabled histogram
  for (map<string,int>::iterator disabled_it = disabled_modules.begin(); disabled_it != disabled_modules.end(); ++disabled_it){
    string mod_name = disabled_it->first;
    int mod_phi = disabled_it->second;
    int disabled_bin_content = 1;
    if (mod_name.find("L")!=string::npos){ // Layer module
      
      int layernumber, bistavenumber, stavenumber, etanumber;
      parse_mod_name_of_layer(mod_name, &layernumber, &bistavenumber, &stavenumber, &etanumber);
      
      if (layernumber==2){	
	if ((bistavenumber==1) && (stavenumber==1)){
	  // highest bin due to shift in phi relative to Layer1
	  layer2_disabled->Fill(etanumber,51,disabled_bin_content);
	} else {
	  // stave number shifted by 1 due to shift in phi relative to
	  // Layer1
	  layer2_disabled->Fill(etanumber,
				(bistavenumber-1)*2+stavenumber-2,
				disabled_bin_content);
	}
      } else if (layernumber==1){
	layer1_disabled->Fill(etanumber,
			      (bistavenumber-1)*2+stavenumber-1,
			      disabled_bin_content);
      } else if (layernumber==0){
	if ((bistavenumber==11) && (stavenumber==2)){
	  // highest bin due to shift in phi relative to Layer1
	  layer0_disabled->Fill(etanumber,0,disabled_bin_content);
	} else {
	  // stave number shifted by 1 due to shift in phi relative to
	  // Layer1
	  layer0_disabled->Fill(etanumber,
				(bistavenumber-1)*2+stavenumber,
				disabled_bin_content);
	}
      } else if (layernumber==-1){ // IBL
	layerIbl_disabled->Fill(etanumber,stavenumber-1,disabled_bin_content);
      } else if (layernumber==-2){ // DBM
	layerDbm_disabled->Fill(etanumber,stavenumber-1,disabled_bin_content);
      }
    } else if (mod_name.find("D")!=string::npos){ // Disk module

      int disknumber;
      bool a_side;
      parse_mod_name_of_disk(mod_name, &disknumber, &a_side);
      
      if (a_side){
	diskA_disabled->Fill(disknumber,mod_phi,disabled_bin_content);
      } else {
	diskC_disabled->Fill(disknumber,mod_phi,disabled_bin_content);
      }
    }
  }


  sprintf(name,"/SHIFT/Pixel/Ibl_Edge_Hitmap");
  sprintf(title,"Ibl Edge Hitmap");
  ERS_PIX_DEBUG(2,"Boooking Ibl Edge Hitmap under " << name);
  ibl_edge_hitmap = new GnamHisto(name,title,3,-1.5,1.5,
				  nstavelayerIbl,-0.5,nstavelayerIbl-0.5,
				  "paoGlo",GnamHisto::FLOATBIN);
  ibl_edge_hitmap->GetXaxis()->SetTitle("Edge");
  ibl_edge_hitmap->GetYaxis()->SetTitle("stave");
  GH_RESET_AT_WASTART(ibl_edge_hitmap);
  histolist->push_back(ibl_edge_hitmap);
  ibl_edge_hitmap->GetXaxis()->SetBinLabel(1,"C8_2");
  ibl_edge_hitmap->GetXaxis()->SetBinLabel(3,"A8_2");
  for (int i=0; i<nstavelayerIbl; ++i){
    ibl_edge_hitmap->GetYaxis()->SetBinLabel(i+1,stavelayerIbl[i]);
  }
  
  sprintf(name,"/SHIFT/Pixel/Ibl_Edge_Occupancy_Merged");
  sprintf(title,"Ibl Edge Occupancy");
  ERS_PIX_DEBUG(2,"Boooking Ibl Edge Occupancy under " << name);
  ibl_edge_occupancy = new GnamHisto(name,title,3,-1.5,1.5,
				     nstavelayerIbl,-0.5,nstavelayerIbl-0.5,
				     "paoGlo",GnamHisto::FLOATBIN);
  ibl_edge_occupancy->GetXaxis()->SetTitle("Edge");
  ibl_edge_occupancy->GetYaxis()->SetTitle("stave");
  GH_RESET_AT_WASTART(ibl_edge_occupancy);
  histolist->push_back(ibl_edge_occupancy);
  ibl_edge_occupancy->GetXaxis()->SetBinLabel(1,"C8_2");
  ibl_edge_occupancy->GetXaxis()->SetBinLabel(3,"A8_2");
  for (int i=0; i<nstavelayerIbl; ++i){
    ibl_edge_occupancy->GetYaxis()->SetBinLabel(i+1,stavelayerIbl[i]);
  }
  


  sprintf(name,"/SHIFT/Pixel/Detector_Hitmap");
  sprintf(title,"Detector Hitmap");
  ERS_PIX_DEBUG(2,"Boooking Detector Hitmap under " << name);
  detector_hitmap = new GnamHisto(name,title,MAX_ROD_NUMBER,-0.5,
				  MAX_ROD_NUMBER-0.5,MAX_MOD_PER_ROD,-0.5,
				  MAX_MOD_PER_ROD-0.5,"paoGlo",
				  GnamHisto::FLOATBIN);
  detector_hitmap->GetXaxis()->SetTitle("Ros*MAX_ROD_PER_ROS + ROD");
  detector_hitmap->GetYaxis()->SetTitle("OB*MAX_MOD_PER_OB+Module-1");
  GH_RESET_AT_WASTART(detector_hitmap);
  histolist->push_back(detector_hitmap);

  sprintf(name,"/SHIFT/Pixel/Detector_Occupancy");
  sprintf(title,"Detector Occupancy");
  ERS_PIX_DEBUG(2,"Boooking Detector Occupancy under " << name);
  detector_occupancy = new GnamHisto(name,title,MAX_ROD_NUMBER,-0.5,
				     MAX_ROD_NUMBER-0.5,MAX_MOD_PER_ROD,-0.5,
				     MAX_MOD_PER_ROD-0.5,"paoGlo",
				     GnamHisto::FLOATBIN);
  detector_occupancy->GetXaxis()->SetTitle("Ros*MAX_ROD_PER_ROS + ROD");
  detector_occupancy->GetYaxis()->SetTitle("OB*MAX_MOD_PER_OB+Module-1");
  GH_RESET_AT_WASTART(detector_occupancy);
  histolist->push_back(detector_occupancy);

  sprintf(name,"/SHIFT/Pixel/Layer2_Occupancy_Merged");
  sprintf(title,"Layer2 Occupancy");
  ERS_PIX_DEBUG(2,"Boooking Layer2 Occupancy under " << name);
  layer2_occupancy = new GnamHisto(name,title,nmodperstave,-0.5,
				   nmodperstave-0.5,nstavelayer2,-0.5,
				   nstavelayer2-0.5,"paoGlo",
				   GnamHisto::FLOATBIN);
  layer2_occupancy->GetXaxis()->SetTitle("module");
  layer2_occupancy->GetYaxis()->SetTitle("stave");
  GH_RESET_AT_WASTART(layer2_occupancy);
  histolist->push_back(layer2_occupancy);
  for (int i=0; i<nmodperstave; ++i){
    layer2_occupancy->GetXaxis()->SetBinLabel(i+1,modlabel[i]);
  }
  for (int i=0; i<nstavelayer2; ++i){
    layer2_occupancy->GetYaxis()->SetBinLabel(i+1,stavelayer2[i]); 
  }
  
  sprintf(name,"/SHIFT/Pixel/Layer2_Occupancy_Summary_Merged");
  sprintf(title,"Layer2 Occupancy_Summary");
  ERS_PIX_DEBUG(2,"Boooking Layer2 Occupancy Summary under " << name);
  layer2_occupancy_distribution = new GnamHisto(name,title,100,1e-5,8e-4,"paoGlo",
						GnamHisto::INTBIN);
  layer2_occupancy_distribution->GetXaxis()->SetTitle("Occupancy");
  layer2_occupancy_distribution->GetYaxis()->SetTitle("Occurence");
  GH_RESET_AT_WASTART(layer2_occupancy_distribution);
  histolist->push_back(layer2_occupancy_distribution);


  sprintf(name,"/SHIFT/Pixel/Layer1_Occupancy_Merged");
  sprintf(title,"Layer1 Occupancy");
  ERS_PIX_DEBUG(2,"Boooking Layer1 Occupancy under " << name);
  layer1_occupancy = new GnamHisto(name,title,nmodperstave,-0.5,
				   nmodperstave-0.5,nstavelayer1,-0.5,
				   nstavelayer1-0.5,"paoGlo",
				   GnamHisto::FLOATBIN);
  layer1_occupancy->GetXaxis()->SetTitle("module");
  layer1_occupancy->GetYaxis()->SetTitle("stave");
  GH_RESET_AT_WASTART(layer1_occupancy);
  histolist->push_back(layer1_occupancy);
  for (int i=0; i<nmodperstave; ++i){
    layer1_occupancy->GetXaxis()->SetBinLabel(i+1,modlabel[i]);
  }
  for (int i=0; i<nstavelayer1; ++i){
    layer1_occupancy->GetYaxis()->SetBinLabel(i+1,stavelayer1[i]); 
  }

  sprintf(name,"/SHIFT/Pixel/Layer1_Occupancy_Summary_Merged");
  sprintf(title,"Layer1 Occupancy_Summary");
  ERS_PIX_DEBUG(2,"Boooking Layer1 Occupancy Summary under " << name);
  layer1_occupancy_distribution = new GnamHisto(name,title,100,1e-5,8e-4,"paoGlo",
						GnamHisto::INTBIN);
  layer1_occupancy_distribution->GetXaxis()->SetTitle("Occupancy");
  layer1_occupancy_distribution->GetYaxis()->SetTitle("Occurence");
  GH_RESET_AT_WASTART(layer1_occupancy_distribution);
  histolist->push_back(layer1_occupancy_distribution);


  sprintf(name,"/SHIFT/Pixel/Layer0_Occupancy_Merged");
  sprintf(title,"Layer0 Occupancy");
  ERS_PIX_DEBUG(2,"Boooking Layer0 Occupancy under " << name);
  layer0_occupancy = new GnamHisto(name,title,nmodperstave,-0.5,
				   nmodperstave-0.5,nstavelayer0,-0.5,
				   nstavelayer0-0.5,"paoGlo",
				   GnamHisto::FLOATBIN);
  layer0_occupancy->GetXaxis()->SetTitle("module");
  layer0_occupancy->GetYaxis()->SetTitle("stave");
  GH_RESET_AT_WASTART(layer0_occupancy);
  histolist->push_back(layer0_occupancy);
  for (int i=0; i<nmodperstave; ++i){
    layer0_occupancy->GetXaxis()->SetBinLabel(i+1,modlabel[i]);
  }
  for (int i=0; i<nstavelayer0; ++i){
    layer0_occupancy->GetYaxis()->SetBinLabel(i+1,stavelayer0[i]);
  }
  
  sprintf(name,"/SHIFT/Pixel/Layer0_Occupancy_Summary_Merged");
  sprintf(title,"Layer0 Occupancy_Summary");
  ERS_PIX_DEBUG(2,"Boooking Layer0 Occupancy Summary under " << name);
  layer0_occupancy_distribution = new GnamHisto(name,title,100,1e-5,8e-4,"paoGlo",
						GnamHisto::INTBIN);
  layer0_occupancy_distribution->GetXaxis()->SetTitle("Occupancy");
  layer0_occupancy_distribution->GetYaxis()->SetTitle("Occurence");
  GH_RESET_AT_WASTART(layer0_occupancy_distribution);
  histolist->push_back(layer0_occupancy_distribution);




  sprintf(name,"/SHIFT/Pixel/LayerIbl_Occupancy_Merged");
  sprintf(title,"LayerIbl Occupancy");
  ERS_PIX_DEBUG(2,"Boooking LayerIbl Occupancy under " << name);
  layerIbl_occupancy = new GnamHisto(name,title,nmodperstaveIbl,-0.5,
				     nmodperstaveIbl-0.5,nstavelayerIbl,-0.5,
				     nstavelayerIbl-0.5,"paoGlo",
				     GnamHisto::FLOATBIN);
  layerIbl_occupancy->GetXaxis()->SetTitle("module");
  layerIbl_occupancy->GetYaxis()->SetTitle("stave");
  GH_RESET_AT_WASTART(layerIbl_occupancy);
  histolist->push_back(layerIbl_occupancy);
  for (int i=0; i<nmodperstaveIbl; ++i){
    layerIbl_occupancy->GetXaxis()->SetBinLabel(i+1,modlabelIbl[i]);
  }
  for (int i=0; i<nstavelayerIbl; ++i){
    layerIbl_occupancy->GetYaxis()->SetBinLabel(i+1,stavelayerIbl[i]);
  }
  
  sprintf(name,"/SHIFT/Pixel/LayerIbl_Occupancy_Summary_Merged");
  sprintf(title,"LayerIbl Occupancy_Summary");
  ERS_PIX_DEBUG(2,"Boooking LayerIbl Occupancy Summary under " << name);
  layerIbl_occupancy_distribution = new GnamHisto(name,title,100,1e-5,8e-4,"paoGlo",
						GnamHisto::INTBIN);
  layerIbl_occupancy_distribution->GetXaxis()->SetTitle("Occupancy");
  layerIbl_occupancy_distribution->GetYaxis()->SetTitle("Occurence");
  GH_RESET_AT_WASTART(layerIbl_occupancy_distribution);
  histolist->push_back(layerIbl_occupancy_distribution);


  sprintf(name,"/SHIFT/Pixel/LayerDbm_Occupancy_Merged");
  sprintf(title,"LayerDbm Occupancy");
  ERS_PIX_DEBUG(2,"Boooking LayerDbm Occupancy under " << name);
  layerDbm_occupancy = new GnamHisto(name,title,nmodpertelescopeDbm,-0.5,
				     nmodpertelescopeDbm-0.5,ntelescopeDbm,-0.5,
				     ntelescopeDbm-0.5,"paoGlo",
				     GnamHisto::FLOATBIN);
  layerDbm_occupancy->GetXaxis()->SetTitle("module");
  layerDbm_occupancy->GetYaxis()->SetTitle("telescope");
  GH_RESET_AT_WASTART(layerDbm_occupancy);
  histolist->push_back(layerDbm_occupancy);
  for (int i=0; i<nmodpertelescopeDbm; ++i){
    layerDbm_occupancy->GetXaxis()->SetBinLabel(i+1,modlabelDbm[i]);
  }
  for (int i=0; i<ntelescopeDbm; ++i){
    layerDbm_occupancy->GetYaxis()->SetBinLabel(i+1,telescopeDbm[i]);
  }
  
  sprintf(name,"/SHIFT/Pixel/LayerDbm_Occupancy_Summary_Merged");
  sprintf(title,"LayerDbm Occupancy_Summary");
  ERS_PIX_DEBUG(2,"Boooking LayerDbm Occupancy Summary under " << name);
  layerDbm_occupancy_distribution = new GnamHisto(name,title,100,1e-5,8e-4,"paoGlo",
						GnamHisto::INTBIN);
  layerDbm_occupancy_distribution->GetXaxis()->SetTitle("Occupancy");
  layerDbm_occupancy_distribution->GetYaxis()->SetTitle("Occurence");
  GH_RESET_AT_WASTART(layerDbm_occupancy_distribution);
  histolist->push_back(layerDbm_occupancy_distribution);


  sprintf(name,"/SHIFT/Pixel/DiskA_Occupancy_Merged");
  sprintf(title,"Disk A Occupancy");
  ERS_PIX_DEBUG(2,"Boooking Disk Occupancy under " << name);
  diskA_occupancy = new GnamHisto(name,title,ndisks,-0.5,ndisks-0.5,
				  nphiperdisk,-0.5,nphiperdisk-0.5,
				  "paoGlo",GnamHisto::FLOATBIN);
  GH_RESET_AT_WASTART(diskA_occupancy);
  histolist->push_back(diskA_occupancy);
  for (int i=0; i<ndisks; ++i){
    diskA_occupancy->GetXaxis()->SetBinLabel(i+1,disks[i]);
  }
  for (int i=0; i<nphiperdisk; ++i){
    diskA_occupancy->GetYaxis()->SetBinLabel(i+1,nstaveA[i]);
  }

  sprintf(name,"/SHIFT/Pixel/DiskA_Occupancy_Summary_Merged");
  sprintf(title,"Disk A Occupancy_Summary");
  ERS_PIX_DEBUG(2,"Boooking DiskA Occupancy Summary under " << name);
  diskA_occupancy_distribution = new GnamHisto(name,title,100,1e-5,8e-4,"paoGlo",
					       GnamHisto::INTBIN);
  diskA_occupancy_distribution->GetXaxis()->SetTitle("Occupancy");
  diskA_occupancy_distribution->GetYaxis()->SetTitle("Occurence");
  GH_RESET_AT_WASTART(diskA_occupancy_distribution);
  histolist->push_back(diskA_occupancy_distribution);

  sprintf(name,"/SHIFT/Pixel/DiskC_Occupancy_Merged");
  sprintf(title,"Disk C Occupancy");
  ERS_PIX_DEBUG(2,"Boooking Disk Occupancy under " << name);
  diskC_occupancy = new GnamHisto(name,title,ndisks,-0.5,ndisks-0.5,
				  nphiperdisk,-0.5,nphiperdisk-0.5,
				  "paoGlo",GnamHisto::FLOATBIN);
  GH_RESET_AT_WASTART(diskC_occupancy);
  histolist->push_back(diskC_occupancy);
  for (int i=0; i<ndisks; ++i){
    diskC_occupancy->GetXaxis()->SetBinLabel(i+1,disks[i]);
  }
  for (int i=0; i<nphiperdisk; ++i){
    diskC_occupancy->GetYaxis()->SetBinLabel(i+1,nstaveC[i]);
  }

  sprintf(name,"/SHIFT/Pixel/DiskC_Occupancy_Summary_Merged");
  sprintf(title,"Disk C Occupancy_Summary");
  ERS_PIX_DEBUG(2,"Boooking DiskC Occupancy Summary under " << name);
  diskC_occupancy_distribution = new GnamHisto(name,title,100,1e-5,8e-4,"paoGlo",
					       GnamHisto::INTBIN);
  diskC_occupancy_distribution->GetXaxis()->SetTitle("Occupancy");
  diskC_occupancy_distribution->GetYaxis()->SetTitle("Occurence");
  GH_RESET_AT_WASTART(diskC_occupancy_distribution);
  histolist->push_back(diskC_occupancy_distribution);
 




  sprintf(name,"/SHIFT/Pixel/Layer2_Occupancy_sinceUpdate_Merged");
  sprintf(title,"Layer2 Occupancy");
  ERS_PIX_DEBUG(2,"Boooking Layer2 Occupancy under " << name);
  layer2_occupancy_since_update = new GnamHisto(name,title,nmodperstave,-0.5,
						nmodperstave-0.5,nstavelayer2,-0.5,
						nstavelayer2-0.5,"paoGlo",
						GnamHisto::FLOATBIN);
  layer2_occupancy_since_update->GetXaxis()->SetTitle("module");
  layer2_occupancy_since_update->GetYaxis()->SetTitle("stave");
  GH_RESET_AT_WASTART(layer2_occupancy_since_update);
  histolist->push_back(layer2_occupancy_since_update);
  for (int i=0; i<nmodperstave; ++i){
    layer2_occupancy_since_update->GetXaxis()->SetBinLabel(i+1,modlabel[i]);
  }
  for (int i=0; i<nstavelayer2; ++i){
    layer2_occupancy_since_update->GetYaxis()->SetBinLabel(i+1,stavelayer2[i]); 
  }

  sprintf(name,"/SHIFT/Pixel/Layer2_Occupancy_Summary_sinceUpdate_Merged");
  sprintf(title,"Layer2 Occupancy_Summary");
  ERS_PIX_DEBUG(2,"Boooking Layer2 Occupancy Summary under " << name);
  layer2_occupancy_distribution_sinceUpdate = new GnamHisto(name,title,100,1e-5,8e-4,"paoGlo",
							    GnamHisto::INTBIN);
  layer2_occupancy_distribution_sinceUpdate->GetXaxis()->SetTitle("Occupancy");
  layer2_occupancy_distribution_sinceUpdate->GetYaxis()->SetTitle("Occurence");
  GH_RESET_AT_WASTART(layer2_occupancy_distribution_sinceUpdate);
  histolist->push_back(layer2_occupancy_distribution_sinceUpdate);




  sprintf(name,"/SHIFT/Pixel/Layer1_Occupancy_sinceUpdate_Merged");
  sprintf(title,"Layer1 Occupancy");
  ERS_PIX_DEBUG(2,"Boooking Layer1 Occupancy under " << name);
  layer1_occupancy_since_update = new GnamHisto(name,title,nmodperstave,-0.5,
						nmodperstave-0.5,nstavelayer1,-0.5,
						nstavelayer1-0.5,"paoGlo",
						GnamHisto::FLOATBIN);
  layer1_occupancy_since_update->GetXaxis()->SetTitle("module");
  layer1_occupancy_since_update->GetYaxis()->SetTitle("stave");
  GH_RESET_AT_WASTART(layer1_occupancy_since_update);
  histolist->push_back(layer1_occupancy_since_update);
  for (int i=0; i<nmodperstave; ++i){
    layer1_occupancy_since_update->GetXaxis()->SetBinLabel(i+1,modlabel[i]);
  }
  for (int i=0; i<nstavelayer1; ++i){
    layer1_occupancy_since_update->GetYaxis()->SetBinLabel(i+1,stavelayer1[i]); 
  }
  
  sprintf(name,"/SHIFT/Pixel/Layer1_Occupancy_Summary_sinceUpdate_Merged");
  sprintf(title,"Layer1 Occupancy_Summary");
  ERS_PIX_DEBUG(2,"Boooking Layer1 Occupancy Summary under " << name);
  layer1_occupancy_distribution_sinceUpdate = new GnamHisto(name,title,100,1e-5,8e-4,"paoGlo",
							    GnamHisto::INTBIN);
  layer1_occupancy_distribution_sinceUpdate->GetXaxis()->SetTitle("Occupancy");
  layer1_occupancy_distribution_sinceUpdate->GetYaxis()->SetTitle("Occurence");
  GH_RESET_AT_WASTART(layer1_occupancy_distribution_sinceUpdate);
  histolist->push_back(layer1_occupancy_distribution_sinceUpdate);

  sprintf(name,"/SHIFT/Pixel/Layer0_Occupancy_sinceUpdate_Merged");
  sprintf(title,"Layer0 Occupancy");
  ERS_PIX_DEBUG(2,"Boooking Layer0 Occupancy under " << name);
  layer0_occupancy_since_update = new GnamHisto(name,title,nmodperstave,-0.5,
						nmodperstave-0.5,nstavelayer0,-0.5,
						nstavelayer0-0.5,"paoGlo",
						GnamHisto::FLOATBIN);
  layer0_occupancy_since_update->GetXaxis()->SetTitle("module");
  layer0_occupancy_since_update->GetYaxis()->SetTitle("stave");
  GH_RESET_AT_WASTART(layer0_occupancy_since_update);
  histolist->push_back(layer0_occupancy_since_update);
  for (int i=0; i<nmodperstave; ++i){
    layer0_occupancy_since_update->GetXaxis()->SetBinLabel(i+1,modlabel[i]);
  }
  for (int i=0; i<nstavelayer0; ++i){
    layer0_occupancy_since_update->GetYaxis()->SetBinLabel(i+1,stavelayer0[i]);
  }

  sprintf(name,"/SHIFT/Pixel/Layer0_Occupancy_Summary_sinceUpdate_Merged");
  sprintf(title,"Layer0 Occupancy_Summary");
  ERS_PIX_DEBUG(2,"Boooking Layer0 Occupancy Summary under " << name);
  layer0_occupancy_distribution_sinceUpdate = new GnamHisto(name,title,100,1e-5,8e-4,"paoGlo",
							    GnamHisto::INTBIN);
  layer0_occupancy_distribution_sinceUpdate->GetXaxis()->SetTitle("Occupancy");
  layer0_occupancy_distribution_sinceUpdate->GetYaxis()->SetTitle("Occurence");
  GH_RESET_AT_WASTART(layer0_occupancy_distribution_sinceUpdate);
  histolist->push_back(layer0_occupancy_distribution_sinceUpdate);


  sprintf(name,"/SHIFT/Pixel/LayerIbl_Occupancy_sinceUpdate_Merged");
  sprintf(title,"LayerIbl Occupancy");
  ERS_PIX_DEBUG(2,"Boooking LayerIbl Occupancy under " << name);
  layerIbl_occupancy_since_update = new GnamHisto(name,title,nmodperstaveIbl,-0.5,
						nmodperstaveIbl-0.5,nstavelayerIbl,-0.5,
						nstavelayerIbl-0.5,"paoGlo",
						GnamHisto::FLOATBIN);
  layerIbl_occupancy_since_update->GetXaxis()->SetTitle("module");
  layerIbl_occupancy_since_update->GetYaxis()->SetTitle("stave");
  GH_RESET_AT_WASTART(layerIbl_occupancy_since_update);
  histolist->push_back(layerIbl_occupancy_since_update);
  for (int i=0; i<nmodperstaveIbl; ++i){
    layerIbl_occupancy_since_update->GetXaxis()->SetBinLabel(i+1,modlabelIbl[i]);
  }
  for (int i=0; i<nstavelayerIbl; ++i){
    layerIbl_occupancy_since_update->GetYaxis()->SetBinLabel(i+1,stavelayerIbl[i]);
  }

  sprintf(name,"/SHIFT/Pixel/LayerIbl_Occupancy_Summary_sinceUpdate_Merged");
  sprintf(title,"LayerIbl Occupancy_Summary");
  ERS_PIX_DEBUG(2,"Boooking LayerIbl Occupancy Summary under " << name);
  layerIbl_occupancy_distribution_sinceUpdate = new GnamHisto(name,title,100,1e-5,8e-4,"paoGlo",
							    GnamHisto::INTBIN);
  layerIbl_occupancy_distribution_sinceUpdate->GetXaxis()->SetTitle("Occupancy");
  layerIbl_occupancy_distribution_sinceUpdate->GetYaxis()->SetTitle("Occurence");
  GH_RESET_AT_WASTART(layerIbl_occupancy_distribution_sinceUpdate);
  histolist->push_back(layerIbl_occupancy_distribution_sinceUpdate);


  sprintf(name,"/SHIFT/Pixel/LayerDbm_Occupancy_sinceUpdate_Merged");
  sprintf(title,"LayerDbm Occupancy");
  ERS_PIX_DEBUG(2,"Boooking LayerDbm Occupancy under " << name);
  layerDbm_occupancy_since_update = new GnamHisto(name,title,nmodpertelescopeDbm,-0.5,
						nmodpertelescopeDbm-0.5,ntelescopeDbm,-0.5,
						ntelescopeDbm-0.5,"paoGlo",
						GnamHisto::FLOATBIN);
  layerDbm_occupancy_since_update->GetXaxis()->SetTitle("module");
  layerDbm_occupancy_since_update->GetYaxis()->SetTitle("telescope");
  GH_RESET_AT_WASTART(layerDbm_occupancy_since_update);
  histolist->push_back(layerDbm_occupancy_since_update);
  for (int i=0; i<nmodpertelescopeDbm; ++i){
    layerDbm_occupancy_since_update->GetXaxis()->SetBinLabel(i+1,modlabelDbm[i]);
  }
  for (int i=0; i<ntelescopeDbm; ++i){
    layerDbm_occupancy_since_update->GetYaxis()->SetBinLabel(i+1,telescopeDbm[i]);
  }

  sprintf(name,"/SHIFT/Pixel/LayerDbm_Occupancy_Summary_sinceUpdate_Merged");
  sprintf(title,"LayerDbm Occupancy_Summary");
  ERS_PIX_DEBUG(2,"Boooking LayerDbm Occupancy Summary under " << name);
  layerDbm_occupancy_distribution_sinceUpdate = new GnamHisto(name,title,100,1e-5,8e-4,"paoGlo",
							    GnamHisto::INTBIN);
  layerDbm_occupancy_distribution_sinceUpdate->GetXaxis()->SetTitle("Occupancy");
  layerDbm_occupancy_distribution_sinceUpdate->GetYaxis()->SetTitle("Occurence");
  GH_RESET_AT_WASTART(layerDbm_occupancy_distribution_sinceUpdate);
  histolist->push_back(layerDbm_occupancy_distribution_sinceUpdate);


  sprintf(name,"/SHIFT/Pixel/DiskA_Occupancy_sinceUpdate_Merged");
  sprintf(title,"Disk A Occupancy");
  ERS_PIX_DEBUG(2,"Boooking Disk Occupancy under " << name);
  diskA_occupancy_since_update = new GnamHisto(name,title,ndisks,-0.5,ndisks-0.5,
					       nphiperdisk,-0.5,nphiperdisk-0.5,
					       "paoGlo",GnamHisto::FLOATBIN);
  GH_RESET_AT_WASTART(diskA_occupancy_since_update);
  histolist->push_back(diskA_occupancy_since_update);
  for (int i=0; i<ndisks; ++i){
    diskA_occupancy_since_update->GetXaxis()->SetBinLabel(i+1,disks[i]);
  }
  for (int i=0; i<nphiperdisk; ++i){
    diskA_occupancy_since_update->GetYaxis()->SetBinLabel(i+1,nstaveA[i]);
  }

  sprintf(name,"/SHIFT/Pixel/DiskA_Occupancy_Summary_sinceUpdate_Merged");
  sprintf(title,"Disk A Occupancy_Summary");
  ERS_PIX_DEBUG(2,"Boooking DiskA Occupancy Summary under " << name);
  diskA_occupancy_distribution_sinceUpdate = new GnamHisto(name,title,100,1e-5,8e-4,"paoGlo",
							   GnamHisto::INTBIN);
  diskA_occupancy_distribution_sinceUpdate->GetXaxis()->SetTitle("Occupancy");
  diskA_occupancy_distribution_sinceUpdate->GetYaxis()->SetTitle("Occurence");
  GH_RESET_AT_WASTART(diskA_occupancy_distribution_sinceUpdate);
  histolist->push_back(diskA_occupancy_distribution_sinceUpdate);

  sprintf(name,"/SHIFT/Pixel/DiskC_Occupancy_sinceUpdate_Merged");
  sprintf(title,"Disk C Occupancy");
  ERS_PIX_DEBUG(2,"Boooking Disk Occupancy under " << name);
  diskC_occupancy_since_update = new GnamHisto(name,title,ndisks,-0.5,ndisks-0.5,
					       nphiperdisk,-0.5,nphiperdisk-0.5,
					       "paoGlo",GnamHisto::FLOATBIN);
  GH_RESET_AT_WASTART(diskC_occupancy_since_update);
  histolist->push_back(diskC_occupancy_since_update);
  for (int i=0; i<ndisks; ++i){
    diskC_occupancy_since_update->GetXaxis()->SetBinLabel(i+1,disks[i]);
  }
  for (int i=0; i<nphiperdisk; ++i){
    diskC_occupancy_since_update->GetYaxis()->SetBinLabel(i+1,nstaveC[i]);
  }

  sprintf(name,"/SHIFT/Pixel/DiskC_Occupancy_Summary_sinceUpdate_Merged");
  sprintf(title,"Disk C Occupancy_Summary");
  ERS_PIX_DEBUG(2,"Boooking DiskC Occupancy Summary under " << name);
  diskC_occupancy_distribution_sinceUpdate = new GnamHisto(name,title,100,1e-5,8e-4,"paoGlo",
							   GnamHisto::INTBIN);
  diskC_occupancy_distribution_sinceUpdate->GetXaxis()->SetTitle("Occupancy");
  diskC_occupancy_distribution_sinceUpdate->GetYaxis()->SetTitle("Occurence");
  GH_RESET_AT_WASTART(diskC_occupancy_distribution_sinceUpdate);
  histolist->push_back(diskC_occupancy_distribution_sinceUpdate);
  
  sprintf(name,"/SHIFT/Pixel/Layer2_Errorfraction_Merged");
  sprintf(title,"Layer2 Errorfraction");
  ERS_PIX_DEBUG(2,"Boooking Layer2 Errorfraction under " << name);
  layer2_sumerrorfraction = new GnamHisto(name,title,nmodperstave,-0.5,
				       nmodperstave-0.5,nstavelayer2,-0.5,
				       nstavelayer2-0.5,"paoGlo",
				       GnamHisto::FLOATBIN);
  layer2_sumerrorfraction->GetXaxis()->SetTitle("module");
  layer2_sumerrorfraction->GetYaxis()->SetTitle("stave");
  GH_RESET_AT_WASTART(layer2_sumerrorfraction);
  histolist->push_back(layer2_sumerrorfraction);
  for (int i=0; i<nmodperstave; ++i){
    layer2_sumerrorfraction->GetXaxis()->SetBinLabel(i+1,modlabel[i]);
  }
  for (int i=0; i<nstavelayer2; ++i){
    layer2_sumerrorfraction->GetYaxis()->SetBinLabel(i+1,stavelayer2[i]); 
  }

  sprintf(name,"/SHIFT/Pixel/Layer1_Errorfraction_Merged");
  sprintf(title,"Layer1 Errorfraction");
  ERS_PIX_DEBUG(2,"Boooking Layer1 Errorfraction under " << name);
  layer1_sumerrorfraction = new GnamHisto(name,title,nmodperstave,-0.5,
				       nmodperstave-0.5,nstavelayer1,-0.5,
				       nstavelayer1-0.5,"paoGlo",
				       GnamHisto::FLOATBIN);
  layer1_sumerrorfraction->GetXaxis()->SetTitle("module");
  layer1_sumerrorfraction->GetYaxis()->SetTitle("stave");
  GH_RESET_AT_WASTART(layer1_sumerrorfraction);
  histolist->push_back(layer1_sumerrorfraction);
  for (int i=0; i<nmodperstave; ++i){
    layer1_sumerrorfraction->GetXaxis()->SetBinLabel(i+1,modlabel[i]);
  }
  for (int i=0; i<nstavelayer1; ++i){
    layer1_sumerrorfraction->GetYaxis()->SetBinLabel(i+1,stavelayer1[i]); 
  }

  sprintf(name,"/SHIFT/Pixel/Layer0_Errorfraction_Merged");
  sprintf(title,"Layer0 Errorfraction");
  ERS_PIX_DEBUG(2,"Boooking Layer0 Errorfraction under " << name);
  layer0_sumerrorfraction = new GnamHisto(name,title,nmodperstave,-0.5,
				       nmodperstave-0.5,nstavelayer0,-0.5,
				       nstavelayer0-0.5,"paoGlo",
				       GnamHisto::FLOATBIN);
  layer0_sumerrorfraction->GetXaxis()->SetTitle("module");
  layer0_sumerrorfraction->GetYaxis()->SetTitle("stave");
  GH_RESET_AT_WASTART(layer0_sumerrorfraction);
  histolist->push_back(layer0_sumerrorfraction);
  for (int i=0; i<nmodperstave; ++i){
    layer0_sumerrorfraction->GetXaxis()->SetBinLabel(i+1,modlabel[i]);
  }
  for (int i=0; i<nstavelayer0; ++i){
    layer0_sumerrorfraction->GetYaxis()->SetBinLabel(i+1,stavelayer0[i]);
  }


  sprintf(name,"/SHIFT/Pixel/LayerIbl_Errorfraction_Merged");
  sprintf(title,"LayerIbl Errorfraction");
  ERS_PIX_DEBUG(2,"Boooking LayerIbl Errorfraction under " << name);
  layerIbl_sumerrorfraction = new GnamHisto(name,title,nmodperstaveIbl,-0.5,
				       nmodperstaveIbl-0.5,nstavelayerIbl,-0.5,
				       nstavelayerIbl-0.5,"paoGlo",
				       GnamHisto::FLOATBIN);
  layerIbl_sumerrorfraction->GetXaxis()->SetTitle("module");
  layerIbl_sumerrorfraction->GetYaxis()->SetTitle("stave");
  GH_RESET_AT_WASTART(layerIbl_sumerrorfraction);
  histolist->push_back(layerIbl_sumerrorfraction);
  for (int i=0; i<nmodperstaveIbl; ++i){
    layerIbl_sumerrorfraction->GetXaxis()->SetBinLabel(i+1,modlabelIbl[i]);
  }
  for (int i=0; i<nstavelayerIbl; ++i){
    layerIbl_sumerrorfraction->GetYaxis()->SetBinLabel(i+1,stavelayerIbl[i]);
  }

  sprintf(name,"/SHIFT/Pixel/LayerDbm_Errorfraction_Merged");
  sprintf(title,"LayerDbm Errorfraction");
  ERS_PIX_DEBUG(2,"Boooking LayerDbm Errorfraction under " << name);
  layerDbm_sumerrorfraction = new GnamHisto(name,title,nmodpertelescopeDbm,-0.5,
				       nmodpertelescopeDbm-0.5,ntelescopeDbm,-0.5,
				       ntelescopeDbm-0.5,"paoGlo",
				       GnamHisto::FLOATBIN);
  layerDbm_sumerrorfraction->GetXaxis()->SetTitle("module");
  layerDbm_sumerrorfraction->GetYaxis()->SetTitle("telescope");
  GH_RESET_AT_WASTART(layerDbm_sumerrorfraction);
  histolist->push_back(layerDbm_sumerrorfraction);
  for (int i=0; i<nmodpertelescopeDbm; ++i){
    layerDbm_sumerrorfraction->GetXaxis()->SetBinLabel(i+1,modlabelDbm[i]);
  }
  for (int i=0; i<ntelescopeDbm; ++i){
    layerDbm_sumerrorfraction->GetYaxis()->SetBinLabel(i+1,telescopeDbm[i]);
  }


  sprintf(name,"/SHIFT/Pixel/DiskA_Errorfraction_Merged");
  sprintf(title,"Disk A Errorfraction");
  ERS_PIX_DEBUG(2,"Boooking Disk Errorfraction under " << name);
  diskA_sumerrorfraction = new GnamHisto(name,title,ndisks,-0.5,ndisks-0.5,
				      nphiperdisk,-0.5,nphiperdisk-0.5,
				      "paoGlo",GnamHisto::FLOATBIN);
  GH_RESET_AT_WASTART(diskA_sumerrorfraction);
  histolist->push_back(diskA_sumerrorfraction);
  for (int i=0; i<ndisks; ++i){
    diskA_sumerrorfraction->GetXaxis()->SetBinLabel(i+1,disks[i]);
  }
  for (int i=0; i<nphiperdisk; ++i){
    diskA_sumerrorfraction->GetYaxis()->SetBinLabel(i+1,nstaveA[i]);
  }

  sprintf(name,"/SHIFT/Pixel/DiskC_Errorfraction_Merged");
  sprintf(title,"Disk C Errorfraction");
  ERS_PIX_DEBUG(2,"Boooking Disk Errorfraction under " << name);
  diskC_sumerrorfraction = new GnamHisto(name,title,ndisks,-0.5,ndisks-0.5,
				      nphiperdisk,-0.5,nphiperdisk-0.5,
				      "paoGlo",GnamHisto::FLOATBIN);
  GH_RESET_AT_WASTART(diskC_sumerrorfraction);
  histolist->push_back(diskC_sumerrorfraction);
  for (int i=0; i<ndisks; ++i){
    diskC_sumerrorfraction->GetXaxis()->SetBinLabel(i+1,disks[i]);
  }
  for (int i=0; i<nphiperdisk; ++i){
    diskC_sumerrorfraction->GetYaxis()->SetBinLabel(i+1,nstaveC[i]);
  }

  sprintf(name,"/SHIFT/Pixel/Layer2_AnyErrorfraction_Merged");
  sprintf(title,"Layer2 AnyErrorfraction");
  ERS_PIX_DEBUG(2,"Boooking Layer2 AnyErrorfraction under " << name);
  layer2_anyerrorfraction = new GnamHisto(name,title,nmodperstave,-0.5,
				       nmodperstave-0.5,nstavelayer2,-0.5,
				       nstavelayer2-0.5,"paoGlo",
				       GnamHisto::FLOATBIN);
  layer2_anyerrorfraction->GetXaxis()->SetTitle("module");
  layer2_anyerrorfraction->GetYaxis()->SetTitle("stave");
  GH_RESET_AT_WASTART(layer2_anyerrorfraction);
  histolist->push_back(layer2_anyerrorfraction);
  for (int i=0; i<nmodperstave; ++i){
    layer2_anyerrorfraction->GetXaxis()->SetBinLabel(i+1,modlabel[i]);
  }
  for (int i=0; i<nstavelayer2; ++i){
    layer2_anyerrorfraction->GetYaxis()->SetBinLabel(i+1,stavelayer2[i]); 
  }

  sprintf(name,"/SHIFT/Pixel/Layer1_AnyErrorfraction_Merged");
  sprintf(title,"Layer1 AnyErrorfraction");
  ERS_PIX_DEBUG(2,"Boooking Layer1 AnyErrorfraction under " << name);
  layer1_anyerrorfraction = new GnamHisto(name,title,nmodperstave,-0.5,
				       nmodperstave-0.5,nstavelayer1,-0.5,
				       nstavelayer1-0.5,"paoGlo",
				       GnamHisto::FLOATBIN);
  layer1_anyerrorfraction->GetXaxis()->SetTitle("module");
  layer1_anyerrorfraction->GetYaxis()->SetTitle("stave");
  GH_RESET_AT_WASTART(layer1_anyerrorfraction);
  histolist->push_back(layer1_anyerrorfraction);
  for (int i=0; i<nmodperstave; ++i){
    layer1_anyerrorfraction->GetXaxis()->SetBinLabel(i+1,modlabel[i]);
  }
  for (int i=0; i<nstavelayer1; ++i){
    layer1_anyerrorfraction->GetYaxis()->SetBinLabel(i+1,stavelayer1[i]); 
  }

  sprintf(name,"/SHIFT/Pixel/Layer0_AnyErrorfraction_Merged");
  sprintf(title,"Layer0 AnyErrorfraction");
  ERS_PIX_DEBUG(2,"Boooking Layer0 AnyErrorfraction under " << name);
  layer0_anyerrorfraction = new GnamHisto(name,title,nmodperstave,-0.5,
				       nmodperstave-0.5,nstavelayer0,-0.5,
				       nstavelayer0-0.5,"paoGlo",
				       GnamHisto::FLOATBIN);
  layer0_anyerrorfraction->GetXaxis()->SetTitle("module");
  layer0_anyerrorfraction->GetYaxis()->SetTitle("stave");
  GH_RESET_AT_WASTART(layer0_anyerrorfraction);
  histolist->push_back(layer0_anyerrorfraction);
  for (int i=0; i<nmodperstave; ++i){
    layer0_anyerrorfraction->GetXaxis()->SetBinLabel(i+1,modlabel[i]);
  }
  for (int i=0; i<nstavelayer0; ++i){
    layer0_anyerrorfraction->GetYaxis()->SetBinLabel(i+1,stavelayer0[i]);
  }


  sprintf(name,"/SHIFT/Pixel/LayerIbl_AnyErrorfraction_Merged");
  sprintf(title,"LayerIbl AnyErrorfraction");
  ERS_PIX_DEBUG(2,"Boooking LayerIbl AnyErrorfraction under " << name);
  layerIbl_anyerrorfraction = new GnamHisto(name,title,nmodperstaveIbl,-0.5,
				       nmodperstaveIbl-0.5,nstavelayerIbl,-0.5,
				       nstavelayerIbl-0.5,"paoGlo",
				       GnamHisto::FLOATBIN);
  layerIbl_anyerrorfraction->GetXaxis()->SetTitle("module");
  layerIbl_anyerrorfraction->GetYaxis()->SetTitle("stave");
  GH_RESET_AT_WASTART(layerIbl_anyerrorfraction);
  histolist->push_back(layerIbl_anyerrorfraction);
  for (int i=0; i<nmodperstaveIbl; ++i){
    layerIbl_anyerrorfraction->GetXaxis()->SetBinLabel(i+1,modlabelIbl[i]);
  }
  for (int i=0; i<nstavelayerIbl; ++i){
    layerIbl_anyerrorfraction->GetYaxis()->SetBinLabel(i+1,stavelayerIbl[i]);
  }

  sprintf(name,"/SHIFT/Pixel/LayerDbm_AnyErrorfraction_Merged");
  sprintf(title,"LayerDbm AnyErrorfraction");
  ERS_PIX_DEBUG(2,"Boooking LayerDbm AnyErrorfraction under " << name);
  layerDbm_anyerrorfraction = new GnamHisto(name,title,nmodpertelescopeDbm,-0.5,
				       nmodpertelescopeDbm-0.5,ntelescopeDbm,-0.5,
				       ntelescopeDbm-0.5,"paoGlo",
				       GnamHisto::FLOATBIN);
  layerDbm_anyerrorfraction->GetXaxis()->SetTitle("module");
  layerDbm_anyerrorfraction->GetYaxis()->SetTitle("telescope");
  GH_RESET_AT_WASTART(layerDbm_anyerrorfraction);
  histolist->push_back(layerDbm_anyerrorfraction);
  for (int i=0; i<nmodpertelescopeDbm; ++i){
    layerDbm_anyerrorfraction->GetXaxis()->SetBinLabel(i+1,modlabelDbm[i]);
  }
  for (int i=0; i<ntelescopeDbm; ++i){
    layerDbm_anyerrorfraction->GetYaxis()->SetBinLabel(i+1,telescopeDbm[i]);
  }


  sprintf(name,"/SHIFT/Pixel/DiskA_AnyErrorfraction_Merged");
  sprintf(title,"Disk A AnyErrorfraction");
  ERS_PIX_DEBUG(2,"Boooking Disk AnyErrorfraction under " << name);
  diskA_anyerrorfraction = new GnamHisto(name,title,ndisks,-0.5,ndisks-0.5,
				      nphiperdisk,-0.5,nphiperdisk-0.5,
				      "paoGlo",GnamHisto::FLOATBIN);
  GH_RESET_AT_WASTART(diskA_anyerrorfraction);
  histolist->push_back(diskA_anyerrorfraction);
  for (int i=0; i<ndisks; ++i){
    diskA_anyerrorfraction->GetXaxis()->SetBinLabel(i+1,disks[i]);
  }
  for (int i=0; i<nphiperdisk; ++i){
    diskA_anyerrorfraction->GetYaxis()->SetBinLabel(i+1,nstaveA[i]);
  }

  sprintf(name,"/SHIFT/Pixel/DiskC_AnyErrorfraction_Merged");
  sprintf(title,"Disk C AnyErrorfraction");
  ERS_PIX_DEBUG(2,"Boooking Disk AnyErrorfraction under " << name);
  diskC_anyerrorfraction = new GnamHisto(name,title,ndisks,-0.5,ndisks-0.5,
				      nphiperdisk,-0.5,nphiperdisk-0.5,
				      "paoGlo",GnamHisto::FLOATBIN);
  GH_RESET_AT_WASTART(diskC_anyerrorfraction);
  histolist->push_back(diskC_anyerrorfraction);
  for (int i=0; i<ndisks; ++i){
    diskC_anyerrorfraction->GetXaxis()->SetBinLabel(i+1,disks[i]);
  }
  for (int i=0; i<nphiperdisk; ++i){
    diskC_anyerrorfraction->GetYaxis()->SetBinLabel(i+1,nstaveC[i]);
  }


  sprintf(name,"/SHIFT/Pixel/LayerIbl_FEServiceCode_Merged");
  sprintf(title,"IBL FE Service Code");
  ERS_PIX_DEBUG(2,"Boooking IBL FE Service Code under " << name);
  layerIblDbm_FEServiceCode = new GnamHisto(name,title,32,-0.5,31.5,"paoGlo",
				    GnamHisto::INTBIN);
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(1,"BCID counter error");
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(2,"Hamming code error in word 0");
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(3,"Hamming code error in word 1");
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(4,"Hamming code error in word 2");
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(5,"L1_in counter error");
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(6,"L1 request counter error");
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(7,"L1 register error");
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(8,"L1 Trigger ID error");
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(9,"readout processor error");
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(10,"Fifo_Full flag pulsed");
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(11,"HitOr bus pulsed");
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(12,"not used");
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(13,"not used");
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(14,"not used");
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(15,"3 MSBs of bunch counter and 7 MSBs of L1A counter");
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(16,"Skipped trigger counter");
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(17,"Truncated event flag and counter");
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(18,"not used");
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(19,"not used");
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(20,"not used");
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(21,"not used");
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(22,"Reset bar RA2b pulsed");
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(23,"PLL generated clock phase faster than reference");
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(24,"Reference clock phase faster than PLL");
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(25,"Triple redundant mismatch");
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(26,"Write register data error");
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(27,"Address error");
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(28,"Other command decoder error- see CMD section");
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(29,"Bit flip detected in command decoder input stream");
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(30,"SEU upset detected in command decoder (triple redundant mismatch)");
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(31,"Data bus address error");
  layerIblDbm_FEServiceCode->GetXaxis()->SetBinLabel(32,"Triple redundant mismatch");
  layerIblDbm_FEServiceCode->GetYaxis()->SetTitle("Count");
  GH_RESET_AT_WASTART(layerIblDbm_FEServiceCode);
  histolist->push_back(layerIblDbm_FEServiceCode);



  sprintf(name,"/SHIFT/Pixel/LayerIbl_FEerrors_Merged");
  sprintf(title,"IBL FE Errors");
  ERS_PIX_DEBUG(2,"Boooking IBL FE Errors under " << name);
  layerIblDbm_FEerrors = new GnamHisto(name,title,19,-0.5,18.5,"paoGlo",
				    GnamHisto::INTBIN);
  layerIblDbm_FEerrors->GetXaxis()->SetBinLabel(1,"BCID counter error");
  layerIblDbm_FEerrors->GetXaxis()->SetBinLabel(2,"Hamming code error in word 0");
  layerIblDbm_FEerrors->GetXaxis()->SetBinLabel(3,"Hamming code error in word 1");
  layerIblDbm_FEerrors->GetXaxis()->SetBinLabel(4,"Hamming code error in word 2");
  layerIblDbm_FEerrors->GetXaxis()->SetBinLabel(5,"L1_in counter error");
  layerIblDbm_FEerrors->GetXaxis()->SetBinLabel(6,"L1 request counter error");
  layerIblDbm_FEerrors->GetXaxis()->SetBinLabel(7,"L1 register error");
  layerIblDbm_FEerrors->GetXaxis()->SetBinLabel(8,"L1 Trigger ID error");
  layerIblDbm_FEerrors->GetXaxis()->SetBinLabel(9,"readout processor error");
  layerIblDbm_FEerrors->GetXaxis()->SetBinLabel(10,"Skipped trigger counter");
  layerIblDbm_FEerrors->GetXaxis()->SetBinLabel(11,"Truncated event flag and counter");
  layerIblDbm_FEerrors->GetXaxis()->SetBinLabel(12,"Triple redundant mismatch");
  layerIblDbm_FEerrors->GetXaxis()->SetBinLabel(13,"Write register data error");
  layerIblDbm_FEerrors->GetXaxis()->SetBinLabel(14,"Address error");
  layerIblDbm_FEerrors->GetXaxis()->SetBinLabel(15,"Other command decoder error- see CMD section");
  layerIblDbm_FEerrors->GetXaxis()->SetBinLabel(16,"Bit flip detected in command decoder input stream");
  layerIblDbm_FEerrors->GetXaxis()->SetBinLabel(17,"SEU upset detected in command decoder (triple redundant mismatch)");
  layerIblDbm_FEerrors->GetXaxis()->SetBinLabel(18,"Data bus address error");
  layerIblDbm_FEerrors->GetXaxis()->SetBinLabel(19,"Triple redundant mismatch");
  layerIblDbm_FEerrors->GetYaxis()->SetTitle("Count");
  GH_RESET_AT_WASTART(layerIblDbm_FEerrors);
  histolist->push_back(layerIblDbm_FEerrors);

  sprintf(name,"/SHIFT/Pixel/LayerIbl_FEerrors_NotWeighted_Merged"); // DC: added 22/10/2016
  sprintf(title,"IBL FE Errors Not Weighted");
  ERS_PIX_DEBUG(2,"Boooking IBL FE Errors Not Weighted under " << name);
  layerIblDbm_FEerrors_notweighted = new GnamHisto(name,title,19,-0.5,18.5,"paoGlo",
				    GnamHisto::INTBIN);
  layerIblDbm_FEerrors_notweighted->GetXaxis()->SetBinLabel(1,"BCID counter error");
  layerIblDbm_FEerrors_notweighted->GetXaxis()->SetBinLabel(2,"Hamming code error in word 0");
  layerIblDbm_FEerrors_notweighted->GetXaxis()->SetBinLabel(3,"Hamming code error in word 1");
  layerIblDbm_FEerrors_notweighted->GetXaxis()->SetBinLabel(4,"Hamming code error in word 2");
  layerIblDbm_FEerrors_notweighted->GetXaxis()->SetBinLabel(5,"L1_in counter error");
  layerIblDbm_FEerrors_notweighted->GetXaxis()->SetBinLabel(6,"L1 request counter error");
  layerIblDbm_FEerrors_notweighted->GetXaxis()->SetBinLabel(7,"L1 register error");
  layerIblDbm_FEerrors_notweighted->GetXaxis()->SetBinLabel(8,"L1 Trigger ID error");
  layerIblDbm_FEerrors_notweighted->GetXaxis()->SetBinLabel(9,"readout processor error");
  layerIblDbm_FEerrors_notweighted->GetXaxis()->SetBinLabel(10,"Skipped trigger counter");
  layerIblDbm_FEerrors_notweighted->GetXaxis()->SetBinLabel(11,"Truncated event flag and counter");
  layerIblDbm_FEerrors_notweighted->GetXaxis()->SetBinLabel(12,"Triple redundant mismatch");
  layerIblDbm_FEerrors_notweighted->GetXaxis()->SetBinLabel(13,"Write register data error");
  layerIblDbm_FEerrors_notweighted->GetXaxis()->SetBinLabel(14,"Address error");
  layerIblDbm_FEerrors_notweighted->GetXaxis()->SetBinLabel(15,"Other command decoder error- see CMD section");
  layerIblDbm_FEerrors_notweighted->GetXaxis()->SetBinLabel(16,"Bit flip detected in command decoder input stream");
  layerIblDbm_FEerrors_notweighted->GetXaxis()->SetBinLabel(17,"SEU upset detected in command decoder (triple redundant mismatch)");
  layerIblDbm_FEerrors_notweighted->GetXaxis()->SetBinLabel(18,"Data bus address error");
  layerIblDbm_FEerrors_notweighted->GetXaxis()->SetBinLabel(19,"Triple redundant mismatch");
  layerIblDbm_FEerrors_notweighted->GetYaxis()->SetTitle("Count");
  GH_RESET_AT_WASTART(layerIblDbm_FEerrors_notweighted);
  histolist->push_back(layerIblDbm_FEerrors_notweighted);

  sprintf(name,"/SHIFT/Pixel/Layer2_BCIDErrors_LastFiveMinutes_Merged");
  sprintf(title,"Layer2 LVL1IDErrors");
  ERS_PIX_DEBUG(2,"Boooking Layer2 LVL1IDErrors under " << name);
  layer2_lvl1id_errors_lastfiveminutes = new GnamHisto(name,title,nmodperstave,-0.5,
						       nmodperstave-0.5,nstavelayer2,-0.5,
						       nstavelayer2-0.5,"paoGlo",
						       GnamHisto::FLOATBIN);
  layer2_lvl1id_errors_lastfiveminutes->GetXaxis()->SetTitle("module");
  layer2_lvl1id_errors_lastfiveminutes->GetYaxis()->SetTitle("stave");
  GH_RESET_AT_WASTART(layer2_lvl1id_errors_lastfiveminutes);
  histolist->push_back(layer2_lvl1id_errors_lastfiveminutes);
  for (int i=0; i<nmodperstave; ++i){
    layer2_lvl1id_errors_lastfiveminutes->GetXaxis()->SetBinLabel(i+1,modlabel[i]);
  }
  for (int i=0; i<nstavelayer2; ++i){
    layer2_lvl1id_errors_lastfiveminutes->GetYaxis()->SetBinLabel(i+1,stavelayer2[i]); 
  }

  sprintf(name,"/SHIFT/Pixel/Layer1_BCIDErrors_LastFiveMinutes_Merged");
  sprintf(title,"Layer1 LVL1IDErrors");
  ERS_PIX_DEBUG(2,"Boooking Layer1 LVL1IDErrors under " << name);
  layer1_lvl1id_errors_lastfiveminutes = new GnamHisto(name,title,nmodperstave,-0.5,
						       nmodperstave-0.5,nstavelayer1,-0.5,
						       nstavelayer1-0.5,"paoGlo",
						       GnamHisto::FLOATBIN);
  layer1_lvl1id_errors_lastfiveminutes->GetXaxis()->SetTitle("module");
  layer1_lvl1id_errors_lastfiveminutes->GetYaxis()->SetTitle("stave");
  GH_RESET_AT_WASTART(layer1_lvl1id_errors_lastfiveminutes);
  histolist->push_back(layer1_lvl1id_errors_lastfiveminutes);
  for (int i=0; i<nmodperstave; ++i){
    layer1_lvl1id_errors_lastfiveminutes->GetXaxis()->SetBinLabel(i+1,modlabel[i]);
  }
  for (int i=0; i<nstavelayer1; ++i){
    layer1_lvl1id_errors_lastfiveminutes->GetYaxis()->SetBinLabel(i+1,stavelayer1[i]); 
  }

  sprintf(name,"/SHIFT/Pixel/Layer0_BCIDErrors_LastFiveMinutes_Merged");
  sprintf(title,"Layer0 LVL1IDErrors");
  ERS_PIX_DEBUG(2,"Boooking Layer0 LVL1IDErrors under " << name);
  layer0_lvl1id_errors_lastfiveminutes = new GnamHisto(name,title,nmodperstave,-0.5,
						       nmodperstave-0.5,nstavelayer0,-0.5,
						       nstavelayer0-0.5,"paoGlo",
						       GnamHisto::FLOATBIN);
  layer0_lvl1id_errors_lastfiveminutes->GetXaxis()->SetTitle("module");
  layer0_lvl1id_errors_lastfiveminutes->GetYaxis()->SetTitle("stave");
  GH_RESET_AT_WASTART(layer0_lvl1id_errors_lastfiveminutes);
  histolist->push_back(layer0_lvl1id_errors_lastfiveminutes);
  for (int i=0; i<nmodperstave; ++i){
    layer0_lvl1id_errors_lastfiveminutes->GetXaxis()->SetBinLabel(i+1,modlabel[i]);
  }
  for (int i=0; i<nstavelayer0; ++i){
    layer0_lvl1id_errors_lastfiveminutes->GetYaxis()->SetBinLabel(i+1,stavelayer0[i]);
  }


  sprintf(name,"/SHIFT/Pixel/LayerIbl_LVL1IDErrors_LastFiveMinutes_Merged");
  sprintf(title,"LayerIbl LVL1IDErrors");
  ERS_PIX_DEBUG(2,"Boooking LayerIbl LVL1IDErrors under " << name);
  layerIbl_lvl1id_errors_lastfiveminutes = new GnamHisto(name,title,nmodperstaveIbl,-0.5,
							 nmodperstaveIbl-0.5,nstavelayerIbl,-0.5,
							 nstavelayerIbl-0.5,"paoGlo",
							 GnamHisto::FLOATBIN);
  layerIbl_lvl1id_errors_lastfiveminutes->GetXaxis()->SetTitle("module");
  layerIbl_lvl1id_errors_lastfiveminutes->GetYaxis()->SetTitle("stave");
  GH_RESET_AT_WASTART(layerIbl_lvl1id_errors_lastfiveminutes);
  histolist->push_back(layerIbl_lvl1id_errors_lastfiveminutes);
  for (int i=0; i<nmodperstaveIbl; ++i){
    layerIbl_lvl1id_errors_lastfiveminutes->GetXaxis()->SetBinLabel(i+1,modlabelIbl[i]);
  }
  for (int i=0; i<nstavelayerIbl; ++i){
    layerIbl_lvl1id_errors_lastfiveminutes->GetYaxis()->SetBinLabel(i+1,stavelayerIbl[i]);
  }


  sprintf(name,"/SHIFT/Pixel/LayerDbm_LVL1IDErrors_LastFiveMinutes_Merged");
  sprintf(title,"LayerDbm LVL1IDErrors");
  ERS_PIX_DEBUG(2,"Boooking LayerDbm LVL1IDErrors under " << name);
  layerDbm_lvl1id_errors_lastfiveminutes = new GnamHisto(name,title,nmodpertelescopeDbm,-0.5,
							 nmodpertelescopeDbm-0.5,ntelescopeDbm,-0.5,
							 ntelescopeDbm-0.5,"paoGlo",
							 GnamHisto::FLOATBIN);
  layerDbm_lvl1id_errors_lastfiveminutes->GetXaxis()->SetTitle("module");
  layerDbm_lvl1id_errors_lastfiveminutes->GetYaxis()->SetTitle("telescope");
  GH_RESET_AT_WASTART(layerDbm_lvl1id_errors_lastfiveminutes);
  histolist->push_back(layerDbm_lvl1id_errors_lastfiveminutes);
  for (int i=0; i<nmodpertelescopeDbm; ++i){
    layerDbm_lvl1id_errors_lastfiveminutes->GetXaxis()->SetBinLabel(i+1,modlabelDbm[i]);
  }
  for (int i=0; i<ntelescopeDbm; ++i){
    layerDbm_lvl1id_errors_lastfiveminutes->GetYaxis()->SetBinLabel(i+1,telescopeDbm[i]);
  }


  sprintf(name,"/SHIFT/Pixel/DiskA_BCIDErrors_LastFiveMinutes_Merged");
  sprintf(title,"Disk A LVL1IDErrors");
  ERS_PIX_DEBUG(2,"Boooking Disk LVL1IDErrors under " << name);
  diskA_lvl1id_errors_lastfiveminutes = new GnamHisto(name,title,ndisks,-0.5,ndisks-0.5,
						      nphiperdisk,-0.5,nphiperdisk-0.5,
						      "paoGlo",GnamHisto::FLOATBIN);
  GH_RESET_AT_WASTART(diskA_lvl1id_errors_lastfiveminutes);
  histolist->push_back(diskA_lvl1id_errors_lastfiveminutes);
  for (int i=0; i<ndisks; ++i){
    diskA_lvl1id_errors_lastfiveminutes->GetXaxis()->SetBinLabel(i+1,disks[i]);
  }
  for (int i=0; i<nphiperdisk; ++i){
    diskA_lvl1id_errors_lastfiveminutes->GetYaxis()->SetBinLabel(i+1,nstaveA[i]);
  }

  sprintf(name,"/SHIFT/Pixel/DiskC_BCIDErrors_LastFiveMinutes_Merged");
  sprintf(title,"Disk C LVL1IDErrors");
  ERS_PIX_DEBUG(2,"Boooking Disk LVL1IDErrors under " << name);
  diskC_lvl1id_errors_lastfiveminutes = new GnamHisto(name,title,ndisks,-0.5,ndisks-0.5,
						      nphiperdisk,-0.5,nphiperdisk-0.5,
						      "paoGlo",GnamHisto::FLOATBIN);
  GH_RESET_AT_WASTART(diskC_lvl1id_errors_lastfiveminutes);
  histolist->push_back(diskC_lvl1id_errors_lastfiveminutes);
  for (int i=0; i<ndisks; ++i){
    diskC_lvl1id_errors_lastfiveminutes->GetXaxis()->SetBinLabel(i+1,disks[i]);
  }
  for (int i=0; i<nphiperdisk; ++i){
    diskC_lvl1id_errors_lastfiveminutes->GetYaxis()->SetBinLabel(i+1,nstaveC[i]);
  }



  sprintf(name,"/SHIFT/Pixel/LayerIbl_LVL1IDErrorFraction_LastFiveMinutes_Merged");
  sprintf(title,"LayerIbl LVL1IDErrorFraction");
  ERS_PIX_DEBUG(2,"Boooking LayerIbl LVL1IDErrorFraction under " << name);
  layerIbl_lvl1id_errorfraction_lastfiveminutes = new GnamHisto(name,title,nmodperstaveIbl,-0.5,
								nmodperstaveIbl-0.5,nstavelayerIbl,-0.5,
								nstavelayerIbl-0.5,"paoGlo",
								GnamHisto::FLOATBIN);
  layerIbl_lvl1id_errorfraction_lastfiveminutes->GetXaxis()->SetTitle("module");
  layerIbl_lvl1id_errorfraction_lastfiveminutes->GetYaxis()->SetTitle("stave");
  GH_RESET_AT_WASTART(layerIbl_lvl1id_errorfraction_lastfiveminutes);
  histolist->push_back(layerIbl_lvl1id_errorfraction_lastfiveminutes);
  for (int i=0; i<nmodperstaveIbl; ++i){
    layerIbl_lvl1id_errorfraction_lastfiveminutes->GetXaxis()->SetBinLabel(i+1,modlabelIbl[i]);
  }
  for (int i=0; i<nstavelayerIbl; ++i){
    layerIbl_lvl1id_errorfraction_lastfiveminutes->GetYaxis()->SetBinLabel(i+1,stavelayerIbl[i]);
  }
  
  
  sprintf(name,"/SHIFT/Pixel/LayerDbm_LVL1IDErrorFraction_LastFiveMinutes_Merged");
  sprintf(title,"LayerDbm LVL1IDErrorFraction");
  ERS_PIX_DEBUG(2,"Boooking LayerDbm LVL1IDErrorFraction under " << name);
  layerDbm_lvl1id_errorfraction_lastfiveminutes = new GnamHisto(name,title,nmodpertelescopeDbm,-0.5,
								nmodpertelescopeDbm-0.5,ntelescopeDbm,-0.5,
								ntelescopeDbm-0.5,"paoGlo",
								GnamHisto::FLOATBIN);
  layerDbm_lvl1id_errorfraction_lastfiveminutes->GetXaxis()->SetTitle("module");
  layerDbm_lvl1id_errorfraction_lastfiveminutes->GetYaxis()->SetTitle("telescope");
  GH_RESET_AT_WASTART(layerDbm_lvl1id_errorfraction_lastfiveminutes);
  histolist->push_back(layerDbm_lvl1id_errorfraction_lastfiveminutes);
  for (int i=0; i<nmodpertelescopeDbm; ++i){
    layerDbm_lvl1id_errorfraction_lastfiveminutes->GetXaxis()->SetBinLabel(i+1,modlabelDbm[i]);
  }
  for (int i=0; i<ntelescopeDbm; ++i){
    layerDbm_lvl1id_errorfraction_lastfiveminutes->GetYaxis()->SetBinLabel(i+1,telescopeDbm[i]);
  }


  sprintf(name,"/SHIFT/Pixel/RODErrors_3LayerPixel_Merged");
  sprintf(title,"ROD Errors (3 Layer Pixel)");
  ERS_PIX_DEBUG(2,"Booking ROD Errors (3 Layer Pixel) under " << name);
  detector_3lp_ROD_errors = new GnamHisto(name,title,nrods_3lp,-0.5,nrods_3lp-0.5,
				      24,-0.5,23.5,
				      "paoGlo",GnamHisto::INTBIN);
  detector_3lp_ROD_errors->GetXaxis()->SetTitle("ROD");
  detector_3lp_ROD_errors->GetYaxis()->SetTitle("");
  GH_RESET_AT_WASTART(detector_3lp_ROD_errors);
  histolist->push_back(detector_3lp_ROD_errors);
  for (int i=0; i<nrods_3lp; ++i){
    detector_3lp_ROD_errors->GetXaxis()->SetBinLabel(i+1,rodnames_3lp[i]);
  }
  detector_3lp_ROD_errors->GetYaxis()->SetBinLabel(1,"BCID");
  detector_3lp_ROD_errors->GetYaxis()->SetBinLabel(2,"LVL1ID");
  detector_3lp_ROD_errors->GetYaxis()->SetBinLabel(3,"FE module timeout");
  detector_3lp_ROD_errors->GetYaxis()->SetBinLabel(4,"Data incorrect?");
  detector_3lp_ROD_errors->GetYaxis()->SetBinLabel(5,"Buffer Overflow");
  detector_3lp_ROD_errors->GetYaxis()->SetBinLabel(6,"Data Truncated");
  detector_3lp_ROD_errors->GetYaxis()->SetBinLabel(7,"Data Overflow");
  detector_3lp_ROD_errors->GetYaxis()->SetBinLabel(8,"Header Bit Error");
  detector_3lp_ROD_errors->GetYaxis()->SetBinLabel(9,"Sync Error");
  detector_3lp_ROD_errors->GetYaxis()->SetBinLabel(10,"Invalid row/column");
  detector_3lp_ROD_errors->GetYaxis()->SetBinLabel(11,"MCC sent Empty Event");
  detector_3lp_ROD_errors->GetYaxis()->SetBinLabel(12,"MCC EOCOVERFLOW");
  detector_3lp_ROD_errors->GetYaxis()->SetBinLabel(13,"MCC HAMMINGCODE");
  detector_3lp_ROD_errors->GetYaxis()->SetBinLabel(14,"MCC REGPARITY");
  detector_3lp_ROD_errors->GetYaxis()->SetBinLabel(15,"MCC HITPARITY");
  detector_3lp_ROD_errors->GetYaxis()->SetBinLabel(16,"MCC BITFLIP");
  detector_3lp_ROD_errors->GetYaxis()->SetBinLabel(17,"MCC HITOVERFLOW");
  detector_3lp_ROD_errors->GetYaxis()->SetBinLabel(18,"MCC EOEOVERFLOW");
  detector_3lp_ROD_errors->GetYaxis()->SetBinLabel(19,"MCC L1CHKFAILFE");
  detector_3lp_ROD_errors->GetYaxis()->SetBinLabel(20,"MCC BCIDCHKFAIL");
  detector_3lp_ROD_errors->GetYaxis()->SetBinLabel(21,"MCC L1CHKFAILGLOBAL");
  detector_3lp_ROD_errors->GetYaxis()->SetBinLabel(22,"TIM Clock Error");
  detector_3lp_ROD_errors->GetYaxis()->SetBinLabel(23,"BOC Clock Error");
  detector_3lp_ROD_errors->GetYaxis()->SetBinLabel(24,"ROL Test Block");

  sprintf(name,"/SHIFT/Pixel/RODErrors_IblDbm_Merged");
  sprintf(title,"ROD Errors (IBL+DBM)");
  ERS_PIX_DEBUG(2,"Booking ROD Errors (IBL+DBM) under " << name);
  detector_IblDbm_ROD_errors = new GnamHisto(name,title,nrods_IblDbm,-0.5,nrods_IblDbm-0.5,
				      23,-0.5,22.5,
				      "paoGlo",GnamHisto::INTBIN);
  detector_IblDbm_ROD_errors->GetXaxis()->SetTitle("ROD");
  detector_IblDbm_ROD_errors->GetYaxis()->SetTitle("");
  GH_RESET_AT_WASTART(detector_IblDbm_ROD_errors);
  histolist->push_back(detector_IblDbm_ROD_errors);
  for (int i=0; i<nrods_IblDbm; ++i){
    detector_IblDbm_ROD_errors->GetXaxis()->SetBinLabel(i+1,rodnames_IblDbm[i]);
  }

  detector_IblDbm_ROD_errors->GetYaxis()->SetBinLabel(1,"bcid");
  detector_IblDbm_ROD_errors->GetYaxis()->SetBinLabel(2,"l1id");
  detector_IblDbm_ROD_errors->GetYaxis()->SetBinLabel(3,"readout timeout");
  detector_IblDbm_ROD_errors->GetYaxis()->SetBinLabel(4,"Any FE error");
  detector_IblDbm_ROD_errors->GetYaxis()->SetBinLabel(5,"");
  detector_IblDbm_ROD_errors->GetYaxis()->SetBinLabel(6,"Non-sequential FE data");
  detector_IblDbm_ROD_errors->GetYaxis()->SetBinLabel(7,"Preamble error in FE");
  detector_IblDbm_ROD_errors->GetYaxis()->SetBinLabel(8,"Raw data");
  detector_IblDbm_ROD_errors->GetYaxis()->SetBinLabel(9,"Invalid row/column");
  detector_IblDbm_ROD_errors->GetYaxis()->SetBinLabel(10,"Skipped triggers");
  detector_IblDbm_ROD_errors->GetYaxis()->SetBinLabel(11,"FE BCID counter error");
  detector_IblDbm_ROD_errors->GetYaxis()->SetBinLabel(12,"FE Hamming code");
  detector_IblDbm_ROD_errors->GetYaxis()->SetBinLabel(13,"FE L1 Cnt,Req,Reg,TID");
  detector_IblDbm_ROD_errors->GetYaxis()->SetBinLabel(14,"FE readout processor");
  detector_IblDbm_ROD_errors->GetYaxis()->SetBinLabel(15,"FE trpl red. mismatch");
  detector_IblDbm_ROD_errors->GetYaxis()->SetBinLabel(16,"FE write register");
  detector_IblDbm_ROD_errors->GetYaxis()->SetBinLabel(17,"FE address");
  detector_IblDbm_ROD_errors->GetYaxis()->SetBinLabel(18,"FE command decoder");
  detector_IblDbm_ROD_errors->GetYaxis()->SetBinLabel(19,"FE SEU upset");
  detector_IblDbm_ROD_errors->GetYaxis()->SetBinLabel(20,"FE data bus address");
  detector_IblDbm_ROD_errors->GetYaxis()->SetBinLabel(21,"FE trpl red. mismatch");
  detector_IblDbm_ROD_errors->GetYaxis()->SetBinLabel(22,"TIM Clock Error");
  detector_IblDbm_ROD_errors->GetYaxis()->SetBinLabel(23,"BOC Clock Error");

  sprintf(name,"/SHIFT/Pixel/RODErrorFraction_IblDbm_Merged");
  sprintf(title,"ROD Errors / Event (IBL+DBM)");
  ERS_PIX_DEBUG(2,"Booking ROD Error Fraction (IBL+DBM) under " << name);
  detector_IblDbm_ROD_errorfraction = new GnamHisto(name,title,nrods_IblDbm,-0.5,nrods_IblDbm-0.5,
						      23,-0.5,22.5,
						      "paoGlo",GnamHisto::INTBIN);
  detector_IblDbm_ROD_errorfraction->GetXaxis()->SetTitle("ROD");
  detector_IblDbm_ROD_errorfraction->GetYaxis()->SetTitle("");
  GH_RESET_AT_WASTART(detector_IblDbm_ROD_errorfraction);
  histolist->push_back(detector_IblDbm_ROD_errorfraction);
  for (int i=0; i<nrods_IblDbm; ++i){
    detector_IblDbm_ROD_errorfraction->GetXaxis()->SetBinLabel(i+1,rodnames_IblDbm[i]);
  }
  
  detector_IblDbm_ROD_errorfraction->GetYaxis()->SetBinLabel(1,"bcid");
  detector_IblDbm_ROD_errorfraction->GetYaxis()->SetBinLabel(2,"l1id");
  detector_IblDbm_ROD_errorfraction->GetYaxis()->SetBinLabel(3,"readout timeout");
  detector_IblDbm_ROD_errorfraction->GetYaxis()->SetBinLabel(4,"Any FE error");
  detector_IblDbm_ROD_errorfraction->GetYaxis()->SetBinLabel(5,"");
  detector_IblDbm_ROD_errorfraction->GetYaxis()->SetBinLabel(6,"Non-sequential FE data");
  detector_IblDbm_ROD_errorfraction->GetYaxis()->SetBinLabel(7,"Preamble error in FE");
  detector_IblDbm_ROD_errorfraction->GetYaxis()->SetBinLabel(8,"Raw data");
  detector_IblDbm_ROD_errorfraction->GetYaxis()->SetBinLabel(9,"Invalid row/column");
  detector_IblDbm_ROD_errorfraction->GetYaxis()->SetBinLabel(10,"Skipped triggers");
  detector_IblDbm_ROD_errorfraction->GetYaxis()->SetBinLabel(11,"FE BCID counter error");
  detector_IblDbm_ROD_errorfraction->GetYaxis()->SetBinLabel(12,"FE Hamming code");
  detector_IblDbm_ROD_errorfraction->GetYaxis()->SetBinLabel(13,"FE L1 Cnt,Req,Reg,TID");
  detector_IblDbm_ROD_errorfraction->GetYaxis()->SetBinLabel(14,"FE readout processor");
  detector_IblDbm_ROD_errorfraction->GetYaxis()->SetBinLabel(15,"FE trpl red. mismatch");
  detector_IblDbm_ROD_errorfraction->GetYaxis()->SetBinLabel(16,"FE write register");
  detector_IblDbm_ROD_errorfraction->GetYaxis()->SetBinLabel(17,"FE address");
  detector_IblDbm_ROD_errorfraction->GetYaxis()->SetBinLabel(18,"FE command decoder");
  detector_IblDbm_ROD_errorfraction->GetYaxis()->SetBinLabel(19,"FE SEU upset");
  detector_IblDbm_ROD_errorfraction->GetYaxis()->SetBinLabel(20,"FE data bus address");
  detector_IblDbm_ROD_errorfraction->GetYaxis()->SetBinLabel(21,"FE trpl red. mismatch");
  detector_IblDbm_ROD_errorfraction->GetYaxis()->SetBinLabel(22,"TIM Clock Error");
  detector_IblDbm_ROD_errorfraction->GetYaxis()->SetBinLabel(23,"BOC Clock Error");


  sprintf(name,"/SHIFT/Pixel/RODErrorsvsBCID_3LayerPixel_Merged");
  sprintf(title,"ROD Errors vs BCID (3 Layer Pixel)");
  ERS_PIX_DEBUG(2,"Booking ROD Errors vs BCID (3 Layer Pixel) under " << name);
  detector_3lp_ROD_errorsvsBCID = new GnamHisto(name,title,4097,-0.5,4096.5,
						24,-0.5,23.5,
						"paoGlo",GnamHisto::INTBIN);
  detector_3lp_ROD_errorsvsBCID->GetXaxis()->SetTitle("ROD BCID");
  detector_3lp_ROD_errorsvsBCID->GetYaxis()->SetTitle("");
  GH_RESET_AT_WASTART(detector_3lp_ROD_errorsvsBCID);
  histolist->push_back(detector_3lp_ROD_errorsvsBCID);
  detector_3lp_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(1,"BCID");
  detector_3lp_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(2,"LVL1ID");
  detector_3lp_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(3,"FE module timeout");
  detector_3lp_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(4,"Data incorrect?");
  detector_3lp_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(5,"Buffer Overflow");
  detector_3lp_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(6,"Data Truncated");
  detector_3lp_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(7,"Data Overflow");
  detector_3lp_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(8,"Header Bit Error");
  detector_3lp_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(9,"Sync Error");
  detector_3lp_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(10,"Invalid row/column");
  detector_3lp_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(11,"MCC sent Empty Event");
  detector_3lp_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(12,"MCC EOCOVERFLOW");
  detector_3lp_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(13,"MCC HAMMINGCODE");
  detector_3lp_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(14,"MCC REGPARITY");
  detector_3lp_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(15,"MCC HITPARITY");
  detector_3lp_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(16,"MCC BITFLIP");
  detector_3lp_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(17,"MCC HITOVERFLOW");
  detector_3lp_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(18,"MCC EOEOVERFLOW");
  detector_3lp_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(19,"MCC L1CHKFAILFE");
  detector_3lp_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(20,"MCC BCIDCHKFAIL");
  detector_3lp_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(21,"MCC L1CHKFAILGLOBAL");
  detector_3lp_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(22,"TIM Clock Error");
  detector_3lp_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(23,"BOC Clock Error");
  detector_3lp_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(24,"ROL Test Block");

  sprintf(name,"/SHIFT/Pixel/RODErrorsvsBCID_IblDbm_Merged");
  sprintf(title,"ROD Errors vs BCID (IBL+DBM)");
  ERS_PIX_DEBUG(2,"Booking ROD Errors vs BCID (IBL+DBM) under " << name);
  detector_IblDbm_ROD_errorsvsBCID = new GnamHisto(name,title,4097,-0.5,4096.5,
						   23,-0.5,22.5,
						   "paoGlo",GnamHisto::INTBIN);
  detector_IblDbm_ROD_errorsvsBCID->GetXaxis()->SetTitle("ROD BCID");
  detector_IblDbm_ROD_errorsvsBCID->GetYaxis()->SetTitle("");
  GH_RESET_AT_WASTART(detector_IblDbm_ROD_errorsvsBCID);
  histolist->push_back(detector_IblDbm_ROD_errorsvsBCID);

  detector_IblDbm_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(1,"bcid");
  detector_IblDbm_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(2,"l1id");
  detector_IblDbm_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(3,"readout timeout");
  detector_IblDbm_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(4,"Any FE error");
  detector_IblDbm_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(5,"");
  detector_IblDbm_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(6,"Non-sequential FE data");
  detector_IblDbm_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(7,"Preamble error in FE");
  detector_IblDbm_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(8,"Raw data");
  detector_IblDbm_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(9,"Invalid row/column");
  detector_IblDbm_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(10,"Skipped triggers");
  detector_IblDbm_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(11,"FE BCID counter error");
  detector_IblDbm_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(12,"FE Hamming code");
  detector_IblDbm_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(13,"FE L1 Cnt,Req,Reg,TID");
  detector_IblDbm_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(14,"FE readout processor");
  detector_IblDbm_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(15,"FE trpl red. mismatch");
  detector_IblDbm_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(16,"FE write register");
  detector_IblDbm_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(17,"FE address");
  detector_IblDbm_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(18,"FE command decoder");
  detector_IblDbm_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(19,"FE SEU upset");
  detector_IblDbm_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(20,"FE data bus address");
  detector_IblDbm_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(21,"FE trpl red. mismatch");
  detector_IblDbm_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(22,"TIM Clock Error");
  detector_IblDbm_ROD_errorsvsBCID->GetYaxis()->SetBinLabel(23,"BOC Clock Error");

  /*
    sprintf(name,"/SHIFT/Pixel/Detector_Summary");
    sprintf(title,"Detector Summary");
    ERS_PIX_DEBUG(2,"Boooking Detector Summary under " << name);
    detector_summary = new GnamHisto(name,title,MAX_ROD_NUMBER,-0.5,
    MAX_ROD_NUMBER-0.5,MAX_MOD_PER_ROD,-0.5,
    MAX_MOD_PER_ROD-0.5,"paoGlo");
    detector_summary->GetXaxis()->SetTitle("Ros*MAX_ROD_PER_ROS + ROD");
    detector_summary->GetYaxis()->SetTitle("OB*MAX_MOD_PER_OB+Module-1");
    histolist->push_back(detector_summary);
  */

  sprintf(name,"/SHIFT/Pixel/Detector_Occupancy_summary_Merged"); // put back merged again after patch, FIXME
  sprintf(title,"Detector Occupancy summary");
  ERS_PIX_DEBUG(2,"Boooking Detector Occupancy summary under " << name);
  detector_occupancy_summary = new GnamHisto(name,title,100,1e-10,8e-4,"paoGlo",
					     GnamHisto::INTBIN);
  detector_occupancy_summary->GetXaxis()->SetTitle("Occupancy");
  detector_occupancy_summary->GetYaxis()->SetTitle("Occurence");
  GH_RESET_AT_WASTART(detector_occupancy_summary);
  histolist->push_back(detector_occupancy_summary);
  sprintf(name,"/SHIFT/Pixel/Detector_Occupancy_summary_sinceUpdate_Merged"); // put back merged again after patch, FIXME
  sprintf(title,"Detector Occupancy summary");
  detector_occupancy_summary_sinceUpdate = new GnamHisto(name,title,100,1e-10,8e-4,"paoGlo",
							 GnamHisto::INTBIN);
  detector_occupancy_summary_sinceUpdate->GetXaxis()->SetTitle("Occupancy");
  detector_occupancy_summary_sinceUpdate->GetYaxis()->SetTitle("Occurence");
  GH_RESET_AT_WASTART(detector_occupancy_summary_sinceUpdate);
  histolist->push_back(detector_occupancy_summary_sinceUpdate);

  sprintf(name,"/SHIFT/Pixel/NumberOfEvents");
  sprintf(title,"Number of Monitored Events");
  ERS_PIX_DEBUG(2,"Booking global number of events histogram under " << name);
  global_events = new GnamHisto(name,title,1,-0.5,0.5,"paoGlo",
				GnamHisto::INTBIN);
  global_events->GetYaxis()->SetTitle("Events");
  GH_RESET_AT_WASTART(global_events);
  histolist->push_back(global_events);

  sprintf(name,"/SHIFT/Pixel/Detector_ToT_%s",m_ros_name.c_str());
  sprintf(title,"Detector Time over Threshold %s",m_ros_name.c_str());
  ERS_PIX_DEBUG(2,"Booking Detector ToT histogram under " << name);
  detector_tot = new GnamHisto(name,title,256,-0.5,255.5,"paoGlo",
			       GnamHisto::INTBIN);
  detector_tot->GetXaxis()->SetTitle("TOT");
  detector_tot->GetYaxis()->SetTitle("Occurrence");
  GH_RESET_AT_WASTART(detector_tot);
  histolist->push_back(detector_tot);

  sprintf(name,"/SHIFT/Pixel/Detector_Time_%s",m_ros_name.c_str());
  sprintf(title,"Detector Timing (LVL1A distribution) %s",m_ros_name.c_str());
  ERS_PIX_DEBUG(2,"Booking Detector Timing histogram under " << name);
  detector_time = new GnamHisto(name,title,16,-0.5,15.5,"paoGlo",
				GnamHisto::INTBIN);
  detector_time->GetXaxis()->SetTitle("LVL1A");
  detector_time->GetYaxis()->SetTitle("Occurrence");
  GH_RESET_AT_WASTART(detector_time);
  histolist->push_back(detector_time);


  sprintf(name,"/EXPERT/Pixel/FE_vs_L1ID_%s",m_ros_name.c_str());
  sprintf(title,"FE number vs L1ID for %s",m_ros_name.c_str());
  FE_vs_L1ID = new GnamHisto(name,title,16,-0.5,15.5,16,-0.5,15.5,"paoGlo");
  FE_vs_L1ID->GetXaxis()->SetTitle("L1ID");
  FE_vs_L1ID->GetYaxis()->SetTitle("FE number");
  GH_RESET_AT_WASTART(FE_vs_L1ID);
  histolist->push_back(FE_vs_L1ID);

  sprintf(name,"/EXPERT/Pixel/TOT_vs_BCID_%s",m_ros_name.c_str());
  sprintf(title,"TOT vs BCID for %s",m_ros_name.c_str());
  TOT_vs_BCID = new GnamHisto(name,title,256,-0.5,255.5,256,-0.5,255.5,"paoGlo");
  TOT_vs_BCID->GetXaxis()->SetTitle("BCID");
  TOT_vs_BCID->GetYaxis()->SetTitle("TOT");
  GH_RESET_AT_WASTART(TOT_vs_BCID);
  histolist->push_back(TOT_vs_BCID);


  map< int,string >::iterator RODGeoMap_iterator;

  for (RODGeoMap_iterator=RODGeoMap.begin();
       RODGeoMap_iterator!=RODGeoMap.end(); ++RODGeoMap_iterator){

    uint rod_number = RODGeoMap_iterator->first;
    if(rod_number >= MAX_ROD_PER_ROS){
      ERS_PIX_ERROR("rod_number="<<rod_number<<" >= MAX_ROD_PER_ROS("<<MAX_ROD_PER_ROS<<"): " << "this should never happen.",MapError);
      cout <<"ERROR: rod_number="<<rod_number<<" out of range...  Try to continue to next mod..."<<endl;
      continue;
    }

    const char* rod_name = RODGeoMap_iterator->second.c_str();
    
    sprintf(name,"/EXPERT/Pixel/ROD_FE_vs_L1ID_%s",rod_name);
    sprintf(title,"FE number vs L1ID for %s",rod_name);
    ROD_FE_vs_L1ID[rod_number] = new GnamHisto(name,title,16,-0.5,15.5,16,-0.5,15.5,"paoGlo");
    ROD_FE_vs_L1ID[rod_number]->GetXaxis()->SetTitle("L1ID");
    ROD_FE_vs_L1ID[rod_number]->GetYaxis()->SetTitle("FE number");
    GH_RESET_AT_WASTART(ROD_FE_vs_L1ID[rod_number]);
    histolist->push_back(ROD_FE_vs_L1ID[rod_number]);

    sprintf(name,"/EXPERT/Pixel/ROD_TOT_vs_BCID_%s",rod_name);
    sprintf(title,"TOT vs BCID for %s",rod_name);
    ROD_TOT_vs_BCID[rod_number] = new GnamHisto(name,title,256,-0.5,255.5,256,-0.5,255.5,"paoGlo");
    ROD_TOT_vs_BCID[rod_number]->GetXaxis()->SetTitle("BCID");
    ROD_TOT_vs_BCID[rod_number]->GetYaxis()->SetTitle("TOT");
    GH_RESET_AT_WASTART(ROD_TOT_vs_BCID[rod_number]);
    histolist->push_back(ROD_TOT_vs_BCID[rod_number]);
  }

  sprintf(name,"/SHIFT/Pixel/NumberOfHits_%s",m_ros_name.c_str());
  sprintf(title,"Number of Hits per Event %s",m_ros_name.c_str());
  ERS_PIX_DEBUG(2,"Booking global number of hits histogram under " << name);
  global_hits = new GnamHisto(name,title,100,-0.5,99.5,"paoGlo",
			      GnamHisto::INTBIN);
  global_hits->GetXaxis()->SetTitle("Number of Hits");
  global_hits->GetYaxis()->SetTitle("Occurrence");
  GH_RESET_AT_WASTART(global_hits);
  histolist->push_back(global_hits);

  /*
    sprintf(name,"/SHIFT/Pixel/NumberOfHits_Merged");
    sprintf(title,"Number of Hits per Event per module");
    ERS_PIX_DEBUG(2,"Booking global number of hits histogram under " << name);
    global_hits_normalized = new GnamHisto(name,title,10,-9.5,0.5,"paoGlo",
    GnamHisto::INTBIN);
    global_hits_normalized->GetXaxis()->SetTitle("Number of Hits per module");
    global_hits_normalized->GetYaxis()->SetTitle("Occurrence");
    histolist->push_back(global_hits_normalized);
    global_hits_normalized->GetXaxis()->SetBinLabel(10,">1");
    global_hits_normalized->GetXaxis()->SetBinLabel(9,"- 1");
    for (int i=8; i>1; i--){
    char bin_name[20];
    sprintf(bin_name,"- 10^-%i",i-1);
    global_hits_normalized->GetXaxis()->SetBinLabel(10-i,bin_name);
    }
    global_hits_normalized->GetXaxis()->SetBinLabel(1,"<10^-8");
  */

  sprintf(name,"/SHIFT/Pixel/NumberOfHits_Merged"); // put back merged again after patch, FIXME
  sprintf(title,"Number of Hits per Event per module");
  ERS_PIX_DEBUG(2,"Booking global number of hits histogram under " << name);
  double global_hits_normalized_bins[] = {0,
					  1E-7,2E-7,3E-7,4E-7,5E-7,6E-7,7E-7,8E-7,9E-7,
					  1E-6,2E-6,3E-6,4E-6,5E-6,6E-6,7E-6,8E-6,9E-6,
					  1E-5,2E-5,3E-5,4E-5,5E-5,6E-5,7E-5,8E-5,9E-5,
					  1E-4,2E-4,3E-4,4E-4,5E-4,6E-4,7E-4,8E-4,9E-4,
					  1E-3,2E-3,3E-3,4E-3,5E-3,6E-3,7E-3,8E-3,9E-3,
					  1E-2,2E-2,3E-2,4E-2,5E-2,6E-2,7E-2,8E-2,9E-2,
					  1E-1,2E-1,3E-1,4E-1,5E-1,6E-1,7E-1,8E-1,9E-1,
					  1};
  global_hits_normalized = new GnamHisto(name,title,64,global_hits_normalized_bins,"paoGlo",
					 GnamHisto::INTBIN);
  global_hits_normalized->GetXaxis()->SetTitle("Number of Hits per module");
  global_hits_normalized->GetYaxis()->SetTitle("Occurrence");
  GH_RESET_AT_WASTART(global_hits_normalized);
  histolist->push_back(global_hits_normalized);


  /*
    sprintf(name,"/SHIFT/Pixel/Detector_Disks");
    sprintf(title,"Overview of the detector disks");
    ERS_PIX_DEBUG(2,"Booking overview of the detector disks under " << name);
    detector_disks = new GnamHisto(name,title,1000,0,999,1000,0,999,"paoGlo",
    GnamHisto::INTBIN);
    histolist->push_back(detector_disks);

    sprintf(name,"/SHIFT/Pixel/Detector_Blayer");
    sprintf(title,"Overview of the B-layer");
    ERS_PIX_DEBUG(2,"Booking overview of the B-layer under " << name);
    detector_blayer = new GnamHisto(name,title,500,0,499,500,0,499,"paoGlo",
    GnamHisto::INTBIN);
    histolist->push_back(detector_blayer);

    sprintf(name,"/SHIFT/Pixel/Detector_Layer1");
    sprintf(title,"Overview of Layer 1");
    ERS_PIX_DEBUG(2,"Booking overview of Layer 1 under " << name);
    detector_layer1 = new GnamHisto(name,title,500,0,499,500,0,499,"paoGlo",
    GnamHisto::INTBIN);
    histolist->push_back(detector_layer1);

    sprintf(name,"/SHIFT/Pixel/Detector_Layer2");
    sprintf(title,"Overview of Layer 2");
    ERS_PIX_DEBUG(2,"Booking overview of Layer 2 under " << name);
    detector_layer2 = new GnamHisto(name,title,500,0,499,500,0,499,"paoGlo",
    GnamHisto::INTBIN);
    histolist->push_back(detector_layer2); 
  */

  sprintf(name,"/SHIFT/Pixel/Detector_BCID_%s",m_ros_name.c_str());
  sprintf(title,"BCID of hits summary %s",m_ros_name.c_str());
  ERS_PIX_DEBUG(2,"Booking overview of BCID under " << name);  
  detector_BCID = new GnamHisto(name,title,3564,-0.5,3563.5,"paoGlo",
				GnamHisto::INTBIN);
  GH_RESET_AT_WASTART(detector_BCID);
  histolist->push_back(detector_BCID); 

  sprintf(name,"/SHIFT/Pixel/Detector_ROD_syncherrors_%s",m_ros_name.c_str());
  sprintf(title,"ROD Synchronization errors for ROS %s",m_ros_name.c_str());
  ERS_PIX_DEBUG(2,"Booking detector ROD synch errors time profile under " << name);
  detector_ROD_syncherrors = new GnamHisto(name,title,m_nrods,-0.5,m_nrods-0.5,
					   721,-0.5,720.5,"paoGlo",
					   GnamHisto::INTBIN);
  //  GH_RESET_AT_WASTART(detector_ROD_syncherrors);
  histolist->push_back(detector_ROD_syncherrors);
  detector_ROD_syncherrors->GetYaxis()->SetBinLabel(1,"");
  for (map<int,string>::iterator rod_iterator = RODGeoMap.begin(); rod_iterator != RODGeoMap.end(); ++rod_iterator)
    detector_ROD_syncherrors->GetXaxis()->SetBinLabel(rod_iterator->first+1,(rod_iterator->second).c_str());

  sprintf(name,"/SHIFT/Pixel/Detector_ROD_transerrors_%s",m_ros_name.c_str());
  sprintf(title,"ROD Optical transmission errors for ROS %s",m_ros_name.c_str());
  ERS_PIX_DEBUG(2,"Booking detector ROD trans errors time profile under " << name);
  detector_ROD_transerrors = new GnamHisto(name,title,m_nrods,-0.5,m_nrods-0.5,
					   721,-0.5,720.5,"paoGlo",
					   GnamHisto::INTBIN);
  //  GH_RESET_AT_WASTART(detector_ROD_transerrors);
  histolist->push_back(detector_ROD_transerrors);
  detector_ROD_transerrors->GetYaxis()->SetBinLabel(1,"");
  for (map<int,string>::iterator rod_iterator = RODGeoMap.begin(); rod_iterator != RODGeoMap.end(); ++rod_iterator)
    detector_ROD_transerrors->GetXaxis()->SetBinLabel(rod_iterator->first+1,(rod_iterator->second).c_str());

  sprintf(name,"/SHIFT/Pixel/Detector_ROD_SEUerrors_%s",m_ros_name.c_str());
  sprintf(title,"ROD SEU errors for ROS %s",m_ros_name.c_str());
  ERS_PIX_DEBUG(2,"Booking detector ROD SEU errors time profile under " << name);
  detector_ROD_SEUerrors = new GnamHisto(name,title,m_nrods,-0.5,m_nrods-0.5,
					 721,-0.5,720.5,"paoGlo",
					 GnamHisto::INTBIN);
  //  GH_RESET_AT_WASTART(detector_ROD_SEUerrors);
  histolist->push_back(detector_ROD_SEUerrors);
  detector_ROD_SEUerrors->GetYaxis()->SetBinLabel(1,"");
  for (map<int,string>::iterator rod_iterator = RODGeoMap.begin(); rod_iterator != RODGeoMap.end(); ++rod_iterator)
    detector_ROD_SEUerrors->GetXaxis()->SetBinLabel(rod_iterator->first+1,(rod_iterator->second).c_str());

  sprintf(name,"/SHIFT/Pixel/Detector_ROD_trunchighoccerrors_%s",m_ros_name.c_str());
  sprintf(title,"ROD Truncation or High occupancy errors for ROS %s",m_ros_name.c_str());
  ERS_PIX_DEBUG(2,"Booking detector ROD trunchighocc errors time profile under " << name);
  detector_ROD_trunchighoccerrors = new GnamHisto(name,title,m_nrods,-0.5,m_nrods-0.5,
						  721,-0.5,720.5,"paoGlo",
						  GnamHisto::INTBIN);
  //  GH_RESET_AT_WASTART(detector_ROD_trunchighoccerrors);
  histolist->push_back(detector_ROD_trunchighoccerrors);
  detector_ROD_trunchighoccerrors->GetYaxis()->SetBinLabel(1,"");
  for (map<int,string>::iterator rod_iterator = RODGeoMap.begin(); rod_iterator != RODGeoMap.end(); ++rod_iterator)
    detector_ROD_trunchighoccerrors->GetXaxis()->SetBinLabel(rod_iterator->first+1,(rod_iterator->second).c_str());

  sprintf(name,"/SHIFT/Pixel/Detector_ROD_DAQerrors_%s",m_ros_name.c_str());
  sprintf(title,"ROD DAQ errors for ROS %s",m_ros_name.c_str());
  ERS_PIX_DEBUG(2,"Booking detector ROD DAQ errors time profile under " << name);
  detector_ROD_DAQerrors = new GnamHisto(name,title,m_nrods,-0.5,m_nrods-0.5,
					 721,-0.5,720.5,"paoGlo",
					 GnamHisto::INTBIN);
  //  GH_RESET_AT_WASTART(detector_ROD_DAQerrors);
  histolist->push_back(detector_ROD_DAQerrors);
  detector_ROD_DAQerrors->GetYaxis()->SetBinLabel(1,"");
  for (map<int,string>::iterator rod_iterator = RODGeoMap.begin(); rod_iterator != RODGeoMap.end(); ++rod_iterator)
    detector_ROD_DAQerrors->GetXaxis()->SetBinLabel(rod_iterator->first+1,(rod_iterator->second).c_str());

  sprintf(name,"/SHIFT/Pixel/Detector_ROD_timeouterrors_%s",m_ros_name.c_str());
  sprintf(title,"ROD timeout errors for ROS %s",m_ros_name.c_str());
  ERS_PIX_DEBUG(2,"Booking detector ROD Timeout errors time profile under " << name);
  detector_ROD_timeouterrors = new GnamHisto(name,title,m_nrods,-0.5,m_nrods-0.5,
					     721,-0.5,720.5,"paoGlo",
					     GnamHisto::INTBIN);
  //  GH_RESET_AT_WASTART(detector_ROD_timeouterrors);
  histolist->push_back(detector_ROD_timeouterrors);
  detector_ROD_timeouterrors->GetYaxis()->SetBinLabel(1,"");
  for (map<int,string>::iterator rod_iterator = RODGeoMap.begin(); rod_iterator != RODGeoMap.end(); ++rod_iterator)
    detector_ROD_timeouterrors->GetXaxis()->SetBinLabel(rod_iterator->first+1,(rod_iterator->second).c_str());




  sprintf(name,"/EXPERT/Pixel/level1Skips_%s",m_ros_name.c_str());
  sprintf(title,"Level1Skips for ROS %s",m_ros_name.c_str());
  global_level1Skips = new GnamHisto(name,title,21,-0.5,20.5,GnamHisto::INTBIN);
  global_level1Skips->GetXaxis()->SetTitle("Level1Skips");
  global_level1Skips->GetYaxis()->SetTitle("Occurence");
  GH_RESET_AT_WASTART(global_level1Skips);
  histolist->push_back(global_level1Skips);

  sprintf(name,"/EXPERT/Pixel/level1Id_%s",m_ros_name.c_str());
  sprintf(title,"Level1Id for ROS %s",m_ros_name.c_str());
  global_level1Id = new GnamHisto(name,title,21,-0.5,20.5,GnamHisto::INTBIN);
  global_level1Id->GetXaxis()->SetTitle("Level1Id");
  global_level1Id->GetYaxis()->SetTitle("Occurence");
  GH_RESET_AT_WASTART(global_level1Id);
  histolist->push_back(global_level1Id);


  if (PrintOutLevel>=1){
    printf("PixelHisto: Leaving book_global_histograms\n");
    fflush(stdout);
  }
}




// ---------------
// Utility methods
// ---------------

/*
// checkArrayBounds functions were being called without using return
// value, so they were removed.

int checkArrayBounds(int ros,int rod,int ob,int mod)
{
int rc = 0;
if ((ros<0) || (ros>=MAX_ROS_NUMBER)) rc |= 0x1;
if ((rod<0) || (rod>=MAX_ROD_PER_ROS)) rc |= 0x2;
if ((ob<0) || (ob>=MAX_OB_PER_ROD)) rc |= 0x4;
if ((mod<0) || (mod>=MAX_MOD_PER_OB)) rc |= 0x8;
//if (PrintOutLevel>=1) if (rc!=0) printf("\tcheckArrayBounds is %x\n",rc);
return rc;
}


int checkArrayBounds(int ros,int rod)
{
  return checkArrayBounds(ros,rod,0,0);
}


int checkArrayBounds(int histo)
{
  int rc = 0;
  if ((histo<0) || (histo>=MAX_DYN_HISTO)) rc |= 0x1;
  //if (PrintOutLevel>=1) if (rc!=0) printf("\tcheckArrayBounds is %x\n",rc);
  return rc;
}
*/

int min_link(int a,int b)
{
  if (a<0) return b;
  else if (b<0) return a;
  else if (a<b) return a;
  else return b;
}


int find_max_bin(GnamHisto* histo)
{
  //---------------------------------------------
  // returns the number of the bin with maximum bincontent
  // returns -1 if no bin has content
  //---------------------------------------------
  int nbins = histo->GetNbinsX();
  int maxbin = -1;
  int maxcontent = 0;
  for (int bin=0; bin<nbins; ++bin){
    if (histo->GetBinContent(bin+1)>maxcontent){
      maxbin = bin;
      maxcontent = (int)histo->GetBinContent(bin);
    }
  }
  return maxbin;
}


void fill_cluster()
//DADA fill_cluster(const PixelEvent* pixel)
{
  // search for clusters in rownumber -> easy to find because they are read
  // out consecutively by the FrontEnd
  if (PrintOutLevel>=1){
    printf("PixelHisto: Entering fill_cluster\n");
    fflush(stdout);
  }
  if (pixel->NHits>0){

    int cluster_size; // is there a cluster?

    int sum_ToT;  // sum of ToT of the cluster

    map< string,int >::iterator ModDynMap_iterator;
    pair<int,int> ModMap_Key;

    // loop over all hits stored in the PixelEvent class
    for (uint kHit=0; kHit<(pixel->NHits-1); ++kHit){
      cluster_size = 1;
      sum_ToT = 0;

      uint kMod = pixel->hit[kHit].inMod; // find the module that collected the hit
      if(kMod>=pixel->NMods){
	ERS_PIX_WARNING( "kMod(" << dec << kMod << ") >= pixel->NMods(" << pixel->NMods << ")", MapError);
	cout << "WARNING: kMod(" << dec << kMod << ") >= pixel->NMods(" << pixel->NMods << ")... Skip and try to continue..." << endl;
	continue;
      }
      
      // next module in list must be the same module
      if (kMod==pixel->hit[kHit+1].inMod){
	uint32_t kRow = pixel->hit[kHit].pixelRow;
	uint32_t kCol = pixel->hit[kHit].pixelCol;
	uint32_t kFE = pixel->hit[kHit].FEnumber;
	if ((kRow == pixel->hit[kHit+1].pixelRow-1) &&
	    (kCol == pixel->hit[kHit].pixelCol) &&
	    (kFE == pixel->hit[kHit].FEnumber)){
	  sum_ToT += pixel->hit[kHit].pixelToT;
	  cluster_size++;
	  kHit++;
	}
      }
      
      uint kRod = pixel->mod[kMod].inRod; // ROD of the module
      if(kRod>=pixel->NRods){
	ERS_PIX_WARNING( "kRod(" << dec << kRod << ") >= pixel->NRods(" << pixel->NRods << ")", MapError);
	cout << "WARNING: kRod(" << dec << kRod << ") >= pixel->NRods(" << pixel->NRods << ")... Skip and try to continue..." << endl;
	continue;
      }
      
      int  link = pixel->mod[kMod].linkNumber; // linknumber of this module
      int  source_id = pixel->rod[kRod].rodRoLink; // source_id of the ROL
      ModMap_Key = make_pair(source_id,link);

      // What if ModMap_Key is not found?
      //ModDynMap_iterator = ModDynMap.find(ModSourceMap.find(ModMap_Key)->second);
      if( ModSourceMap.find(ModMap_Key)!=ModSourceMap.end() ){
	ModDynMap_iterator = ModDynMap.find(ModSourceMap.find(ModMap_Key)->second);
      }else{
	ModDynMap_iterator = ModDynMap.end();
      }

      if (ModDynMap_iterator!=ModDynMap.end()){
	uint histo_number = ModDynMap_iterator->second;
	if(histo_number >= MAX_DYN_HISTO){
	  ERS_PIX_ERROR("histo_number="<<histo_number<<" >= MAX_DYN_HISTO("<<MAX_DYN_HISTO<<"): " << "this should never happen.",MapError);
	  cout <<"ERROR: histo_number="<<histo_number<<" out of range...  Setting it to MAX_DYN_HISTO - 1 ..."<<endl;
	  histo_number = MAX_DYN_HISTO - 1;
	}
	
	module_tot_cluster_size[histo_number]->Fill(cluster_size);
	if (cluster_size>1){
	  // add the ToT of the last pixel in the cluster
	  sum_ToT += pixel->hit[kHit].pixelToT;
	  module_tot_cluster[histo_number]->Fill(sum_ToT);
	}
      }
    }
  }
  if (PrintOutLevel>=1){
    printf("PixelHisto: Leaving fill_cluster\n");
    fflush(stdout);
  }
}

// Fill a time based histogram sweeping over 60 minutes for all error flags, time on the Y axis
void fillTimeHisto(GnamHisto* histo, int& timeIntervals, int position, int value)
{
  time_t rawtime;
  struct tm * timeinfo;
  char buffer1 [14];

  time ( &rawtime );
  timeinfo = localtime ( &rawtime );
  strftime (buffer1,14,"%I:%M:%S",timeinfo);

  float lagtime = difftime(rawtime, startTime); //how long since we started the run;

  int binNoX = histo->GetNbinsX();
  int binNoY = histo->GetNbinsY();
  int timeInterval = 60;     // number of seconds per bin
  //   int histoTime = binNoY*timeInterval;  //number of seconds this histogram represents;
  int intervalNumber = (int) lagtime / timeInterval;   //what interval we are in.  Keep this stored as number of Entries;
  if (intervalNumber > timeIntervals){
    int bins2shift = intervalNumber - timeIntervals; //how many bins to shift the bin contents;
    timeIntervals = intervalNumber;
    for (int x=1; x<=binNoX; ++x){
      for(int y=1; y <= binNoY-bins2shift; ++y) //loop over all bins that need moved;
	histo->SetBinContent(x, y, histo->GetBinContent(x, y+bins2shift)); //move bins to right
      for(int y = binNoY; y > binNoY-bins2shift; y--)
	histo->SetBinContent(x, y, 0); //clear new bins
      histo->GetYaxis()->SetBinLabel(binNoY,buffer1); // write the timestamp on the last bin
      for (int kkk = 1; kkk <= binNoY/30; ++kkk){ // put a mark every 30 ticks as well
	time_t timemark = rawtime - kkk*30*timeInterval;
	struct tm * timemarkinfo;
	timemarkinfo = localtime(&timemark);
	char buffer2[14];
	strftime (buffer2,14,"%I:%M:%S",timemarkinfo);
	histo->GetYaxis()->SetBinLabel(binNoY-30*kkk,buffer2);
      }
    }
  }
  histo->Fill(position, binNoY-1, value);
}

// decoding of string for AutoDisable
std::string element(std::string str, int num, char sep) {
  uint ip;
  int start = -1,end = -1;
  int nel = -1;
  bool spc = true;
  bool quote = false;
                                                                                                                                        
  for (ip=0; ip<str.size(); ++ip) {
    if (str[ip] == '"') quote = !quote;
    if (str[ip] == sep || (sep == 0 && 
			   (str[ip] == 9 || str[ip]==10 || str[ip] == 32))) {
      if (!spc && !quote) {
        spc = true;
        if (nel==num) end = ip-1;
      }
    } else {
      if (spc) {
        spc = false;
        nel++;
        if (nel==num) start = ip;
      }
    }
  }
  if (!spc && nel==num) end = str.size()-1;
                                                                                                                                        
  if (start >= 0) {
    std::string c;
    std::string s = str.substr(start,end-start+1);
    for (ip=0; ip<s.size(); ++ip) {
      if (s[ip] != '"') c += s[ip];
    }
    return c;
  } else {
    return "";
  }
}

/* From the IBL cabling map
 * 0       0        12     -1      140193  140190  0       6       0       7       LI_S13_C_M1_C1
 * 0       0        12     -2      140193  140190  0       4       0       5       LI_S13_C_M1_C2
 * 0       0        12     -3      140193  140190  0       2       0       3       LI_S13_C_M2_C3
 * 0       0        12     -4      140193  140190  0       0       0       1       LI_S13_C_M2_C4
 * 0       0        12     -5      140192  140190  0       6       0       7       LI_S13_C_M3_C5
 * 0       0        12     -6      140192  140190  0       4       0       5       LI_S13_C_M3_C6
 * 0       0        12     -7      140192  140190  0       3       0       F       LI_S13_C_M4_C7_1
 * 0       0        12     -8      140192  140190  0       2       0       F       LI_S13_C_M4_C7_2
 * 0       0        12     -9      140192  140190  0       1       0       F       LI_S13_C_M4_C8_1
 * 0       0        12     -10     140192  140190  0       0       0       F       LI_S13_C_M4_C8_2
 * 0       0        12     9       140191  140190  0       7       0       F       LI_S13_A_M4_A8_2
 * 0       0        12     8       140191  140190  0       6       0       F       LI_S13_A_M4_A8_1
 * 0       0        12     7       140191  140190  0       5       0       F       LI_S13_A_M4_A7_2
 * 0       0        12     6       140191  140190  0       4       0       F       LI_S13_A_M4_A7_1
 * 0       0        12     5       140191  140190  0       2       0       3       LI_S13_A_M3_A6
 * 0       0        12     4       140191  140190  0       0       0       1       LI_S13_A_M3_A5
 * 0       0        12     3       140190  140190  0       6       0       7       LI_S13_A_M2_A4
 * 0       0        12     2       140190  140190  0       4       0       5       LI_S13_A_M2_A3
 * 0       0        12     1       140190  140190  0       2       0       3       LI_S13_A_M1_A2
 * 0       0        12     0       140190  140190  0       0       0       1       LI_S13_A_M1_A1
 */

// A-side: 1 -> 0, 2 -> 2, 3 -> 4, 4 -> 6, 5 -> 0, 6 -> 2, 7 -> 4, 8 -> 6
// C-side: 8 -> 0, 7 -> 2, 6 -> 4, 5 -> 6, 4 -> 0, 3 -> 2, 2 -> 4, 1 -> 6

// This is a temporary function that returns the outlink AND MODIFIES the source_id as needed for IBL
int getOutlinkIBL(const std::string &mod_name, int &source_id) {
  ERS_PIX_DEBUG(1,"getOutlinkIBL(" << mod_name << "," << hex << source_id << dec << ")");
  if(PrintOutLevel >= 2) cout << "DEBUG: getOutlinkIBL(" << mod_name << "," << hex << source_id << dec << ")" << endl;

  int outLink(0);
  bool is_3d_module = (std::count( mod_name.begin(), mod_name.end(), '_' ) == 5);
  std::size_t last_ = mod_name.find_last_of('_');
  bool is_C_side = ( mod_name.find("_C_") != std::string::npos );
  int mod_number = (is_3d_module)? atoi( mod_name.substr(last_-1,1).c_str() ) : atoi( mod_name.substr(last_+2,1).c_str() );

  ERS_PIX_DEBUG(1, std::dec << "is_3d_module:" << is_3d_module << ", is_C_side:" << is_C_side << ", mod_number:" << mod_number );
  if(PrintOutLevel >= 2) std::cout << std::dec << "is_3d_module:" << is_3d_module << ", is_C_side:" << is_C_side << ", mod_number:" << mod_number << std::endl;

  if(is_3d_module){ // JOE: log files show mod_names never have _1 or _2, so is_3d_module is always false (2015 June 23)
    static int ibl_link_lookup_A_side[4] = { 4, 5, 6, 7 };
    static int ibl_link_lookup_C_side[4] = { 3, 2, 1, 0 };
    int mod_subnumber = atoi( mod_name.substr(last_+1,1).c_str() );
    int index = (mod_number-7)*2 + mod_subnumber-1;
    outLink = is_C_side ? ibl_link_lookup_C_side[index%4] : ibl_link_lookup_A_side[index%4]; // add %4 just to be sure index is in bounds
  }else{    

    static int ibl_link_lookup_A_side[4] = { 6, 0, 2, 4 };
    static int ibl_link_lookup_C_side[4] = { 0, 6, 4, 2 };
    outLink = is_C_side ? ibl_link_lookup_C_side[mod_number%4] : ibl_link_lookup_A_side[mod_number%4];
  }
  
  // set the correct source_id
  if(is_C_side){
    if(mod_number <= 4){
      source_id = (source_id & 0xfffffff0) + 3;
    }else{
      source_id = (source_id & 0xfffffff0) + 2;
    }
  }else{ // A side
    if(mod_number >= 5){
      source_id = (source_id & 0xfffffff0) + 1;
    }else{
      source_id = (source_id & 0xfffffff0) + 0;
    }
  }

  ERS_PIX_INFO( __PRETTY_FUNCTION__ << mod_name << "\t" << std::hex << source_id << std::dec << "\t" << mod_number << "\t" << outLink );
  std::cout << __PRETTY_FUNCTION__ << mod_name << "\t" << std::hex << source_id << std::dec << "\t" << mod_number << "\t" << outLink << std::endl;

  return outLink;
}


// From the DBM cabling map
//	B_EC	L_Disk	Phi_mod	Eta_mod	ROBID	RODID	40FMT	40Link	80FMT	80Link	DCS Geographical ID
//	4	0	2	0	150210	150210	0	6	0	F	LI_S15_A_12_M1_A1	
//	4	1	2	0	150210	150210	0	5	0	F	LI_S15_A_12_M1_A2	
//	4	2	2	0	150210	150210	0	4	0	F	LI_S15_A_12_M1_A3	
//	4	0	3	0	150210	150210	0	2	0	F	LI_S15_A_12_M2_A4	
//	4	1	3	0	150210	150210	0	1	0	F	LI_S15_A_12_M2_A5	
//	4	2	3	0	150210	150210	0	0	0	F	LI_S15_A_12_M2_A6	
//	4	0	0	0	150211	150210	0	6	0	F	LI_S15_A_34_M3_A7	
//	4	1	0	0	150211	150210	0	5	0	F	LI_S15_A_34_M3_A8	
//	4	2	0	0	150211	150210	0	4	0	F	LI_S15_A_34_M3_A9	
//	4	0	1	0	150211	150210	0	2	0	F	LI_S15_A_34_M4_A10	
//	4	1	1	0	150211	150210	0	1	0	F	LI_S15_A_34_M4_A11	
//	4	2	1	0	150211	150210	0	0	0	F	LI_S15_A_34_M4_A12	
//	-4	0	2	0	150212	150210	0	2	0	F	LI_S15_C_12_M1_C1	
//	-4	1	2	0	150212	150210	0	1	0	F	LI_S15_C_12_M1_C2	
//	-4	2	2	0	150212	150210	0	0	0	F	LI_S15_C_12_M1_C3	
//	-4	0	3	0	150212	150210	0	6	0	F	LI_S15_C_12_M2_C4	
//	-4	1	3	0	150212	150210	0	5	0	F	LI_S15_C_12_M2_C5	
//	-4	2	3	0	150212	150210	0	4	0	F	LI_S15_C_12_M2_C6	
//	-4	0	0	0	150213	150210	0	2	0	F	LI_S15_C_34_M3_C7	
//	-4	1	0	0	150213	150210	0	1	0	F	LI_S15_C_34_M3_C8	
//	-4	2	0	0	150213	150210	0	0	0	F	LI_S15_C_34_M3_C9	
//	-4	0	1	0	150213	150210	0	6	0	F	LI_S15_C_34_M4_C10	
//	-4	1	1	0	150213	150210	0	5	0	F	LI_S15_C_34_M4_C11	
//	-4	2	1	0	150213	150210	0	4	0	F	LI_S15_C_34_M4_C12	

// A-side: 1/7 -> 6, 2/8 -> 5, 3/9 -> 4, 4/10 -> 2, 5/11 -> 1, 6/12 -> 0
// C-side: 1/7 -> 2, 2/8 -> 1, 3/9 -> 0, 4/10 -> 6, 5/11 -> 5, 6/12 -> 4

// This is a temporary function that returns the outlink AND MODIFIES the source_id as needed for DBM
int getOutlinkDBM(const std::string &mod_name, int &source_id) {
  ERS_PIX_DEBUG(1,"getOutlinkDBM(" << mod_name << "," << hex << source_id << dec << ")");

  std::size_t last_ = mod_name.find_last_of('_');
  int mod_number = atoi( mod_name.substr(last_+2).c_str() );
  bool is_C_side = ( mod_name.find("_C_") != std::string::npos );
  static int dbm_link_lookup_A_side[6] = { 0, 6, 5, 4, 2, 1 };
  static int dbm_link_lookup_C_side[6] = { 4, 2, 1, 0, 6, 5 };
  int outLink = is_C_side ? dbm_link_lookup_C_side[mod_number%6] : dbm_link_lookup_A_side[mod_number%6];

  // set the correct source_id
  if(is_C_side){
    if(mod_number >= 7){
      source_id = (source_id & 0xfffffff0) + 3;
    }else{
      source_id = (source_id & 0xfffffff0) + 2;
    }
  }else { // A side 
    if(mod_number >= 7){
      source_id = (source_id & 0xfffffff0) + 1;
    }else{
      source_id = (source_id & 0xfffffff0) + 0;
    }
  }

  ERS_PIX_INFO( __PRETTY_FUNCTION__ << mod_name << "\t" << std::hex << source_id << std::dec << "\t" << mod_number << "\t" << outLink );
  std::cout << __PRETTY_FUNCTION__ << mod_name << "\t" << std::hex << source_id << std::dec << "\t" << mod_number << "\t" << outLink << std::endl;

  return outLink;
}

int getOutlinkL12(ModuleConnectivity* modConn, int &source_id){ //readoutspeed is not needed for L1 and L2

  int rxCh(0);
  int bocOutputLink = modConn->bocLinkRx(1); // to be fixed for L1, should check if needs 1 or 2 as argument

  switch(bocOutputLink/10) {
   case 0: rxCh = 31 - (bocOutputLink%10) ; break;
   case 1: rxCh = 23 - (bocOutputLink%10) ; break;
   case 2: rxCh =  7 - (bocOutputLink%10) ; break;
   case 3: rxCh = 15 - (bocOutputLink%10) ; break;
  }
  
  cout << "bocOutputLink: "<< bocOutputLink << " rxCh: " << rxCh << endl;

  // Temporary, before adding static function to ModuleConnectivity
  ModuleConnectivity* mod = modConn;
  if( mod && mod->pp0() && mod->pp0()->ob() && mod->pp0()->ob()->rodBoc() && mod->pp0()->ob()->rodBoc()->linkMap() ) {
    auto linkMapRx40 = mod->pp0()->ob()->rodBoc()->linkMap()->rodRx40;
    rxCh = linkMapRx40[bocOutputLink/10][bocOutputLink%10];
  }

  cout << "[KJP] bocOutputLink: "<< bocOutputLink << " rxCh: " << rxCh << endl;

  
  int outlink = rxCh;
  outlink = (rxCh%4 + ((rxCh%16)/8)*4) << 4 | rxCh%8;
  
  cout << "outlink L12: " << hex << "Ox"<< outlink << dec  << endl;
  
  if((bocOutputLink/10)<2) source_id=((source_id & 0xFFFFFF3F) + 0x40);
  else source_id=((source_id & 0xFFFFFF3F) + 0x00); //sanity line ?

  return outlink;

}


int getOutlinkPIX(ModuleConnectivity* modConn, const string &readoutSpeed){
  PixLib::PixModuleGroup::MccBandwidth bandwidth = PixLib::PixModuleGroup::SINGLE_40;
  if (readoutSpeed == "SINGLE_80"){
    bandwidth = PixLib::PixModuleGroup::SINGLE_80;
  }
  else if (readoutSpeed == "DOUBLE_40"){
    bandwidth = PixLib::PixModuleGroup::DOUBLE_40;
  }
  else if (readoutSpeed == "DOUBLE_80"){
    bandwidth = PixLib::PixModuleGroup::DOUBLE_80;
  }

  int out1 = modConn->outLink1A(bandwidth);
  //   int out2 = modConn->outLink1B(bandwidth);
  //   int out3 = modConn->outLink2A(bandwidth);
  //   int out4 = modConn->outLink2B(bandwidth);
  //  cout << "Module " << modConn->name() << " has links " << out1 << " " << out2 << " " << out3 << " " << out4 << endl;
  //  cout << "they get changed to " << out1 << " " << out2 << " " << out3 << " " << out4 << endl;
  //int outlink = min_link(min_link(out1,out2),min_link(out3,out4));
  int outlink = out1;

  if (readoutSpeed == "SINGLE_80" || readoutSpeed == "DOUBLE_40") {
    if (outlink%16 ==2) {
      outlink = (outlink/16)*16 + 1;
    }
  }
  return outlink;
}

int getOutlink(ModuleConnectivity* modConn, const string &readoutSpeed, const std::string& mod_name, int *source_id) { // Using pointer to make it clear that we pass by reference
  if( isIbl(*source_id) ){
    return getOutlinkIBL(mod_name, *source_id); // warning: modifies source_id
  }else if( isDbm(*source_id) ){
    return getOutlinkDBM(mod_name, *source_id); // warning: modifies source_id
  }else if( hasUpgradedReadout(*source_id) ){
    return getOutlinkL12(modConn, *source_id); // warning: modifies source_id
  }else{
    return getOutlinkPIX(modConn,readoutSpeed);
  }
}

void callback_RunParams(ISCallbackInfo * isc) {
  LuminosityInfo* rp = new LuminosityInfo();  // memory leak? callback_RunParams only called in initDB(), so this is probably okay.
  isc->value(*rp);
  run_number = rp->RunNumber;
  luminosity_block = rp->LumiBlockNumber;
  return;
} 

void parse_mod_name_of_disk(const string &mod_name, int *disknumber, bool *a_side){
  
  *a_side = true;
  if (mod_name.find("D1A")!=string::npos)
    *disknumber = 0;
  else if (mod_name.find("D2A")!=string::npos)
    *disknumber = 1;
  else if (mod_name.find("D3A")!=string::npos)
    *disknumber = 2;
  else if (mod_name.find("D1C")!=string::npos){
    *disknumber = 0;
    *a_side = false;
  } else if (mod_name.find("D2C")!=string::npos){
    *disknumber = 1;
    *a_side = false;
  } else if (mod_name.find("D3C")!=string::npos){
    *disknumber = 2;
    *a_side = false;
  }
}

void parse_mod_name_of_layer(const string &mod_name, int *layernumber, int *bistavenumber, int *stavenumber, int *etanumber){

  //////////// The code needs testing to check for bugs

  if (mod_name.find("L2")!=string::npos) *layernumber = 2; // Layer2 module
  else if (mod_name.find("L1")!=string::npos) *layernumber = 1; // Layer1 module
  else if (mod_name.find("L0")!=string::npos) *layernumber = 0; // Layer0 module
  else if (mod_name.find("LI_S15")!=string::npos) *layernumber = -2; // DBM module
  else if (mod_name.find("LI")!=string::npos) *layernumber = -1; // IBL module (must come after DBM check)
  else *layernumber=666; // something is wrong with the label if this happens

  // example modulenames
  // pixel: L0_B11_S2_C6_M2C
  //   IBL: LI_S14_A_M1_A2,    LI_S02_A_M4_A7_2
  //   DBM: L1_S15_C_12_M2_A4, L1_S15_C_34_M4_A10
  
  size_t underscores[5] = {string::npos,string::npos,string::npos,string::npos,string::npos};
  underscores[0] = mod_name.find("_");
  for (uint u=1; u<5; ++u) underscores[u] = mod_name.find("_",underscores[u-1]+1);
  
  if( (*layernumber) == -1){ // IBL
    // LI_S02_C_M4_C8_2 : 0
    // LI_S02_C_M4_C8_1 : 0
    // LI_S02_C_M4_C7_2 : 1
    // LI_S02_C_M4_C7_1 : 1
    // LI_S02_C_M3_C6   : 2
    // ...
    // LI_S02_C_M1_C1   : 7
    // LI_S02_A_M1_A1   : 8
    // ...
    // LI_S02_A_M3_A6   : 13
    // LI_S02_A_M4_A7_1 : 14
    // LI_S02_A_M4_A7_2 : 14
    // LI_S02_A_M4_A8_1 : 15
    // LI_S02_A_M4_A8_2 : 15

    if(underscores[3]==string::npos){
      ERS_PIX_WARNING( "Problem parsing IBL mod_name: "<<mod_name , MapError);
      cout <<"WARNING: Problem parsing IBL mod_name: "<<mod_name<<endl;
    }
    string stavename = mod_name.substr(underscores[0]+2,underscores[1]-underscores[0]-2);
    string etaname = mod_name.substr(underscores[3]+2,1);
    
    // now convert strings to integer
    *bistavenumber = 0;
    *stavenumber = atoi(stavename.c_str());
    *etanumber = atoi(etaname.c_str());
    
    if (mod_name.find("C") != string::npos){ // C side
      *etanumber = 8 - *etanumber;
    } else { // A side
      *etanumber += 7;
    }

    // use bistavenumber to keep track of outermost FEs
    if( mod_name.find("C8_2") != string::npos ){
      *bistavenumber = -1;
    }else if( mod_name.find("A8_2") != string::npos ){
      *bistavenumber = 1;
    }
    
  }else if( (*layernumber) == -2){ // DBM
    // Name: "eta" bin number for plot
    // LI_S15_C_12_M1_C3  : 0
    // LI_S15_C_12_M1_C2  : 1
    // LI_S15_C_12_M1_C1  : 2
    // LI_S15_A_12_M1_A1  : 3
    // LI_S15_A_12_M1_A2  : 4
    // LI_S15_A_12_M1_A3  : 5
    // LI_S15_C_12_M2_C6  : 0
    // ...
    // LI_S15_A_34_M3_A9   : 5
    // LI_S15_C_34_M4_C12  : 0
    // LI_S15_C_34_M4_C11  : 1
    // LI_S15_C_34_M4_C10  : 2
    // LI_S15_A_34_M4_A10  : 3
    // LI_S15_A_34_M4_A11  : 4
    // LI_S15_A_34_M4_A12  : 5

    if(underscores[4]==string::npos){
      ERS_PIX_WARNING( "Problem parsing DBM mod_name: "<<mod_name , MapError);
      cout <<"WARNING: Problem parsing DBM mod_name: "<<mod_name<<endl;
    }
    string telescopename = mod_name.substr(underscores[3]+2,1); // 1-4
    string etaname = mod_name.substr(underscores[4]+2); // 1-12
    
    // now convert strings to integer
    *bistavenumber = 0;
    *stavenumber = atoi(telescopename.c_str());
    *etanumber = atoi(etaname.c_str());

    *etanumber = (*etanumber -1)%3; // 1-12 --> 0-2
    if (mod_name.find("C") != string::npos){ // C side
      *etanumber = 2 - *etanumber;
    } else { // A side
      *etanumber += 3;
    }
    
  }else{ // Pixel layers
    // L0_B11_S2_C6_M6C : 0
    // L0_B11_S2_C6_M5C : 1
    // ...
    // L0_B11_S2_A7_M0  : 6
    // ...
    // L0_B11_S2_A7_M5A : 11
    // L0_B11_S2_A7_M6A : 12

    if(underscores[3]==string::npos){
      ERS_PIX_WARNING( "Problem parsing pixel mod_name: "<<mod_name , MapError);
      cout <<"WARNING: Problem parsing pixel mod_name: "<<mod_name<<endl;
    }
    string bistavename = mod_name.substr(underscores[0]+2,underscores[1]-underscores[0]-2);
    string stavename = mod_name.substr(underscores[1]+2,underscores[2]-underscores[1]-2);
    string etaname = mod_name.substr(underscores[3]+2,1);
    
    // now convert strings to integer
    *bistavenumber = atoi(bistavename.c_str());
    *stavenumber = atoi(stavename.c_str());
    *etanumber = atoi(etaname.c_str());
    
    if (mod_name.find("C") != string::npos){ // C side
      *etanumber = 6 - *etanumber;
    } else{
      *etanumber += 6;
    }
  }
}

void update_title(GnamHisto* hist, const int &run_number, const int &luminosity_block){
  // this code could be optimized, but first I just want to get it into a function
  
  //char old_title[256], new_title[256];
  char old_title[512], new_title[512];
  char* test;
  
  sprintf(old_title,"%s", hist->GetTitle());
  test = strtok(old_title, "(");
  sprintf(new_title,"%s(%i.%i)", test, run_number, luminosity_block);
  hist->SetTitle(new_title);
}


void reset_module_hist(GnamHisto* h, const string &title, const string &name){
  h->Reset();
  h->SetTitle(title.c_str());
  h->SetName(name.c_str());
}

// diane
bool hasUpgradedReadout(const int &source_id){
  return true; // All the detector has the new readout
  if(isCrateL1(source_id) || isCrateL2(source_id)) return true;
  if(isCrateL3(source_id) || isCrateL4(source_id)) return true;
  return false;
}
bool isCrateL1(const int &source_id){ // L1:0x00112400
  return ((source_id & 0x00ffff00) == 0x00112400);
}
bool isCrateL2(const int &source_id){ // L2:0x00112500
  return ((source_id & 0x00ffff00) == 0x00112500);
}
bool isCrateL3(const int &source_id){ // L3:0x00111700
  return ((source_id & 0x00ffff00) == 0x00111700);
}
bool isCrateL4(const int &source_id){ // L4:0x00111800
  return ((source_id & 0x00ffff00) == 0x00111800);
}


bool isIbl(const int &source_id){
  return ( (source_id & 0x00ff0000) == PixelIblId );
}
bool isDbm(const int &source_id){
  return ( (source_id & 0x00ff0000) == PixelDbmId );
}
bool isIblOrDbm(const int &source_id){
  return ( isIbl(source_id) || isDbm(source_id) );
}
bool isIblOrDbm(const string &modname){
    if(modname.find("LI_") != std::string::npos) return true;
    return false;
}

void insert_disabled_modules(Pp0Connectivity* Pp0Conn, const string &readoutSpeed, int &source_id, const int &rod_number, const int &ob_number){
  ModuleConnectivity* ModConn = NULL;
  string mod_name;
  string mod_offline_ID;
  int outLink;
  int mod_phi;

  for (uint mod_number=1; mod_number<=MAX_MOD_PER_OB; ++mod_number){
    ModConn = Pp0Conn->modules(mod_number);
    if (ModConn==NULL) continue;
    mod_name = ModConn->name();
    mod_offline_ID = ModConn->offlineId();
    outLink = getOutlink(ModConn,readoutSpeed,mod_name,&source_id);

    // parse offline ID to get module phi
    size_t points[6];
    points[0] = mod_offline_ID.find(".");
    for (uint p = 1; p<6; ++p){
      points[p] = mod_offline_ID.find(".",points[p-1]+1);
    }
    mod_phi = atoi(mod_offline_ID.substr(points[3]+1, points[4]-points[3]-1).c_str());
    
    insert_disabled_module(source_id, outLink, mod_name, mod_phi, rod_number, ob_number, mod_number);
  }
}

void insert_disabled_module(const int &source_id, const int &outLink, const string &mod_name, const int &mod_phi, const int &rod_number, const int &ob_number, const int &mod_number){
  pair<int,int> ModMap_Key = make_pair(source_id,outLink);
  disabled_modules_names.insert(make_pair(ModMap_Key,mod_name));
  disabled_modules.insert(make_pair(mod_name,mod_phi));
  // Another hack for IBL
  if( isIbl(source_id) ) {
    bool is_3d_module = (std::count( mod_name.begin(), mod_name.end(), '_' ) == 5);
    // Only addressing the cases for planar modules
    if(!is_3d_module) {
      ModMap_Key = make_pair(source_id,outLink+1);
      disabled_modules_names.insert(make_pair(ModMap_Key,mod_name));
      disabled_modules.insert(make_pair(mod_name,mod_phi));
    }
  }
  vector<int> ModLogMap_Value;
  ModLogMap_Value.push_back(rod_number);
  ModLogMap_Value.push_back(ob_number);
  ModLogMap_Value.push_back(mod_number);
  ModGeoMap_all.insert(make_pair(ModLogMap_Value,mod_name));
}

void insert_enabled_module(const int &source_id, const int &outLink, const string &mod_name, const int &mod_EC,  const int &mod_Disk,  const int &mod_phi,  const int &mod_eta, const int &rod_number, const int &ob_number, const int &mod_number){
  //-------------------------------------------//
  // fill ModGeoMap and ModLogMap to allocate  //
  // bytestream identifier <-> Connectivity DB //
  // information correctly                     //
  //-------------------------------------------//
  pair<int,int> ModMap_Key = make_pair(source_id,outLink);
  
  vector<int> ModLogMap_Value;
  ModLogMap_Value.push_back(rod_number);
  ModLogMap_Value.push_back(ob_number);
  ModLogMap_Value.push_back(mod_number);
  
  vector<int> ModOffMap_Value;
  ModOffMap_Value.push_back(mod_EC);
  ModOffMap_Value.push_back(mod_Disk);
  ModOffMap_Value.push_back(mod_phi);
  ModOffMap_Value.push_back(mod_eta);
  
  if (PrintOutLevel>=2){
    ERS_PIX_DEBUG(3,"ModMap_Key: source id " << showbase << hex << ModMap_Key.first << ", link " << dec << ModMap_Key.second);
  }
  // map source_id/link <->
  // rod_number/ob_number/mod_number
  // ModLogRet =
  ModLogMap.insert(make_pair(ModMap_Key,ModLogMap_Value));
  // map source_id/link <-> module name
  ModGeoMap.insert(make_pair(ModLogMap_Value,mod_name));
  ModGeoMap_all.insert(make_pair(ModLogMap_Value,mod_name));
  // map module name <->
  // rod_number/ob_number/mod_number
  ModNamMap.insert(make_pair(mod_name,ModOffMap_Value));
  // map module
  // rod_number/ob_number/mod_number <->
  // module offline ID
  ModOffMap.insert(make_pair(ModOffMap_Value,ModLogMap_Value));
  ModModelMap.insert(make_pair(ModLogMap_Value,ModOffMap_Value));
  ModSourceMap.insert(make_pair(ModMap_Key,mod_name));
  
  // Another hack for IBL
  if( isIbl(source_id) ) {
    bool is_3d_module = (std::count( mod_name.begin(), mod_name.end(), '_' ) == 5);
    // Only addressing the cases for planar modules
    if(!is_3d_module) {
      ModMap_Key = make_pair(source_id,outLink+1);
      ModLogMap.insert(make_pair(ModMap_Key,ModLogMap_Value));
      ModSourceMap.insert(make_pair(ModMap_Key,mod_name));
    }
  }
}

int ibl_edge_check( string mod_name, uint kCol){
  if( mod_name.find("C8_2") != string::npos && kCol < 2){
    return -1;
  }else if( mod_name.find("A8_2") != string::npos && kCol > 157){
    return 1;
  }
  return 0;
}

int get_ibl_stavenumber( string mod_name ){

  if(mod_name.find("LI_S") == string::npos){
    return -1;  // This is only for IBL
  }

  string stavename = mod_name.substr(4,2);  // LI_S02_A_M4_A8_2
  cout << "get_ibl_stavenumber(" << mod_name << ") => " << stavename << endl;
  return atoi(stavename.c_str());  // now convert string to integer
}
