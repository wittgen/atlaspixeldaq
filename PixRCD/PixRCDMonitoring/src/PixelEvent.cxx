#include "PixRCDMonitoring/PixelEvent.h"

// GNAM: global include file for libraries
#include "gnam/gnamutils/GnamUtils.h"

PixelEvent::PixelEvent()
{
  resetEvent();
}

void PixelEvent::resetEvent()
{
  NRods = 0;
  rod.clear();
  NMods = 0;
  mod.clear();
  NHits = 0;
  hit.clear();
  NFlag = 0;
  fee.clear();
  NRawData = 0;
  rawData.clear();
  NTimeOut = 0;
  timeOut.clear();
}

void PixelEvent::setEventEventData(uint32_t eventFragmentSize,
				   uint32_t sourceID,
				   uint32_t bunchCrossingTimeSec,
				   uint32_t bunchCrossingTimeNanoSec,
				   uint32_t globalEventID,
				   uint32_t RunType,
				   uint32_t RunNR,
				   uint32_t LumiBlockNR,
				   uint32_t ExtL1ID,
				   uint32_t BCID)
{
  eventInfo.eventFragmentSize = eventFragmentSize;
  eventInfo.sourceID = sourceID;
  eventInfo.bunchCrossingTimeSec = bunchCrossingTimeSec;
  eventInfo.bunchCrossingTimeNanoSec = bunchCrossingTimeNanoSec;
  eventInfo.globalEventID = globalEventID;
  eventInfo.RunType =RunType;
  eventInfo.RunNR = RunNR;
  eventInfo.LumiBlockNR = LumiBlockNR;
  eventInfo.ExtL1ID = ExtL1ID;
  eventInfo.BCID = BCID;
}

void PixelEvent::setEventRodData(uint32_t RodSubDetector,
				 uint32_t RodRoLink,
				 uint32_t RunNumber,
				 uint32_t ExtendedL1Id,
				 uint32_t BunchCross,
				 uint32_t Level1Type,
				 uint32_t RodStatusWord1,
				 uint32_t RodStatusWord2)
{
  rodData data;
  data.rodSubDetector = RodSubDetector;
  data.rodRoLink = RodRoLink;
  data.runNumber = RunNumber;
  data.extendedL1Id = ExtendedL1Id;
  data.bunchCross = BunchCross;
  data.level1Type = Level1Type;
  data.rodStatusWord1 = RodStatusWord1;
  data.rodStatusWord2 = RodStatusWord2;

  rod.push_back(data);

  ++NRods;
}

void PixelEvent::setEventModData(uint32_t PreambleError,
				 uint32_t TimeOutError,
				 uint32_t Level1Error,
				 uint32_t BunchCrossError,
				 uint32_t LinkNumber,
				 uint32_t Level1Skips,
				 uint32_t Level1Id,
				 uint32_t BunchCrossId,
				 uint32_t TrailerError,
				 uint32_t LimitError,
				 uint32_t OverFlow)
{
  if(NRods<1){ // we have a problem
    cout<<"ERROR: A mod without a ROD! Skipping ..."<<endl;
    return;
  }

  modData data;

  data.inRod = NRods - 1;

  //------------------------------------------
  // Set the timing information
  // look if the module did already have a hit
  // If yes, calculate LVL1A /timeSlot) with bunchCrossId_offset, if not set BCID_offset
  //------------------------------------------
  int iMod = NMods - 1;
  while ((iMod>=0) && ((LinkNumber!=mod[iMod].linkNumber) || (data.inRod!=mod[iMod].inRod))){
    iMod--;
  }
  if (iMod<0){ // module was not found - did not send a hit in this event
    data.timeSlot = 0;
    data.bunchCrossId_offset = BunchCrossId; // offset is set to calculate LVL1A if the module sends hitdata again
  }
  else{
    data.timeSlot = BunchCrossId - mod[iMod].bunchCrossId_offset;  // this calculates LVL1A corresponding to bunchCrossID_offset. If the same LVL1A is calculated more then one time, this will NOT recognize
    data.bunchCrossId_offset = mod[iMod].bunchCrossId_offset;  // this is necessary, because if the module sends another hit later on, it will take the offset from THIS module
  }

  data.preambleError = PreambleError;
  data.timeOutError = TimeOutError;
  data.level1Error = Level1Error;
  data.bunchCrossError = BunchCrossError;
  data.linkNumber = LinkNumber;
  data.level1Skips = Level1Skips;
  data.level1Id = Level1Id;
  data.bunchCrossId = BunchCrossId;
  data.trailerError = TrailerError;
  data.limitError = LimitError;
  data.overFlow = OverFlow;
  data.rawData = 0;
  data.timeOut = 0;

  data.numHits = 0;

  mod.push_back(data);

  ++NMods;
}


void PixelEvent::setEventHitData(uint32_t FENumber,
				 uint32_t PixelRow,
				 uint32_t PixelCol,
				 uint32_t PixelToT)
{
  if(NMods<1){ // we have a problem
    cout<<"ERROR: A hit without a mod! Skipping ..."<<endl;
    return;
  }

  hitData data;

  mod[NMods - 1].numHits++;

  data.inMod = NMods - 1;
  data.FEnumber = FENumber;
  data.pixelRow = PixelRow;
  data.pixelCol = PixelCol;
  data.pixelToT = PixelToT;

  hit.push_back(data);

  ++NHits;
}

void PixelEvent::setEventFeeFlag(uint32_t FENumber,
				 uint32_t FEErrorCode,
				 uint32_t MCCErrorCode)
{
  if(NMods<1){ // we have a problem
    cout<<"ERROR: An FE flag without a mod! Skipping ..."<<endl;
    return;
  }

  feeFlag data;

  data.inMod = NMods - 1;
  data.IblError = 0;
  data.FEnumber = FENumber;
  data.FEerrorCode = FEErrorCode;
  data.MCCerrorCode = MCCErrorCode;

  fee.push_back(data);

  ++NFlag;
}

void PixelEvent::setEventFeeFlagIbl(uint32_t FENumber,
				    uint32_t IblFEerrorNumber,
				    uint32_t IblFEerrorWeight)
{
  if(NMods<1){ // we have a problem
    cout<<"ERROR: An FE flag without a mod! Skipping ..."<<endl;
    return;
  }

  feeFlag data;

  data.inMod = NMods - 1;
  data.IblError = 1;
  data.FEnumber = FENumber;
  data.IblFEerrorNumber = IblFEerrorNumber;
  data.IblFEerrorWeight = IblFEerrorWeight;

  fee.push_back(data);

  ++NFlag;
}

void PixelEvent::setEventRawData(uint32_t RawData)
{
  if(NMods<1){ // we have a problem
    cout<<"ERROR: A raw data without a mod! Skipping ..."<<endl;
    return;
  }

  rawDataWord data;

  data.inMod = NMods - 1;
  data.rawData = RawData;
  rawData.push_back(data);

  ++NRawData;
}

void PixelEvent::setEventTimeOut()
{
  if(NMods<1){ // we have a problem
    cout<<"ERROR: A timeout without a mod! Skipping ..."<<endl;
    return;
  }

  timeOutData data;

  data.inMod = NMods - 1;
  timeOut.push_back(data);

  ++NTimeOut;
}

void PixelEvent::setSubDetId(uint32_t source_id) 
{
  subDetIdList.push_back(source_id);  
}

PixelEvent::~PixelEvent()
{

}

//DADA
//SetSourceId ???
