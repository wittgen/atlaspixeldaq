#include <time.h>
#include <vector>
#include <stdint.h>
#include <stdlib.h>

// GNAM: global include file for libraries
#include <gnam/gnamutils/GnamUtils.h>
// TDAQ: OKS DB entry point
#include <config/Configuration.h>
// TDAQ: include file for DB object casting
#include <dal/util.h>
// Pixel: definition of the class corresponding to PixelEvent
#include "PixRCDMonitoring/PixelEvent.h"
// Pixel: detector parameters
#include "PixRCDMonitoring/PixelParameters.h"
// Pixel: exception definitions
#include "PixRCDMonitoring/PixelGnamException.h"
// DAL: definition of the DB object corresponding to PixGnamLibrary
#include "PixGnamLibrarydal/PixGnamLibrary.h"

using namespace std;

static uint32_t PrintOutLevel = 0;

#define min(a,b) (((a)<(b))?(a):(b))
#define max(a,b) (((a)>(b))?(a):(b))

int32_t beginOfRodDataBlock(const uint32_t *rod, uint32_t size);
int32_t beginOfRodStatusBlock(const uint32_t *rod, uint32_t size);
uint32_t rodCheck(const uint32_t *rod, uint32_t size, int32_t beginStatus, int32_t beginData);
void addIblHits(uint32_t tot, uint32_t row, uint32_t col, uint32_t theRodId);

// GNAM
static PixelEvent *pixel = NULL;


/* ************ *
 *    initDB    *
 * ************ */

/* ****************************************************************** *
 * Called once just after library loading if configuring from OKS DB. *
 * ****************************************************************** */
extern "C" void
initDB(Configuration *confDB,const gnamdal::GnamLibrary *library)
{
  try {
    if (PrintOutLevel>=1){
      printf("PixelDecode: Entering initDB\n");
      fflush(stdout);
    }
    ERS_PIX_INFO("GNAM PixelDecode initDB entry point called");

    const PixGnamLibrarydal::PixGnamLibrary *lib = 
      confDB->cast<PixGnamLibrarydal::PixGnamLibrary, 
      gnamdal::GnamLibrary>(library);
    if (lib==NULL){
      // ERS_PIX_WARNING("PixGnamLibrarydal::PixGnamLibrary "
      // 		    << "pointer not initialized",
      // 		    ConfigError);
      // Promote this to an error. -Joe
      ERS_PIX_ERROR("PixGnamLibrarydal::PixGnamLibrary "
		    << "pointer not initialized",
		    ConfigError);
      printf("ERROR: Error reading DB configuration.\n");
      fflush(stdout);
    } else {
      PrintOutLevel = lib->get_Verbosity();
      ERS_PIX_DEBUG(1,"Configuration parameters:");
      ERS_PIX_DEBUG(1,"PrintOutLevel = " << PrintOutLevel);
      if (PrintOutLevel>=1){
	printf("Configuration parameters:\n");
	printf("-------------------------\n");
	printf("PrintOutLevel: %i\n",PrintOutLevel);
	printf("--------------------------\n");
      }
    }

    try{
      pixel = new PixelEvent();
    }catch(...){
      std::cout << __PRETTY_FUNCTION__ << " exception caught after trying to create new PixelEvent!" << std::endl;
      std::cerr << __PRETTY_FUNCTION__ << " exception caught after trying to create new PixelEvent!" << std::endl;
      ERS_PIX_ERROR("exception caught after trying to create new PixelEvent!", ConfigError);
    }
    
    // #define GNAM_BRANCH_REGISTER( branchname, p2object)
    //  the 1st parameter is the name of the branch
    //  the 2nd parameter is the pointer to the object to be stored
    try {
      GNAM_BRANCH_REGISTER("PixelEvent",pixel);
    } catch (daq::gnamlib::AlreadyExistingBranch &exc){
      throw daq::gnamlib::CannotRegisterBranch(ERS_HERE,"PixelEvent");
    }

    if (PrintOutLevel>=1){
      printf("PixelDecode: Leaving initDB\n");
      fflush(stdout);
    }
  
  }
  catch(std::exception& e){
    cout<<"Exception caught in PixelDecode::initDB of type std::exception:" << e.what() << endl;
  }
  catch(char const* msg){
    cout<<"Exception caught in PixelDecode::initDB of type char const*: " << msg << endl;
  }
  catch(...){
    cout<<"Exception caught in PixelDecode::initDB of unknown type." << endl;
  }
}


/* ********* *
 *    end    *
 * ********* */

/* ************************************************************************* *
 * Called once just before library unloading. Performs final clean-up tasks. *
 * ************************************************************************* */
extern "C" void
end(void)
{
  try {
    if (PrintOutLevel>=1){
      printf("PixelDecode: Entering end\n");
      fflush(stdout);
    }
    ERS_PIX_INFO("GNAM PixelDecode end entry point called");
    
    delete pixel;
    
    if (PrintOutLevel>=1){
      printf("PixelDecode: Leaving end\n");
      fflush(stdout);
    }
  }
  catch(std::exception& e){
    cout<<"Exception caught in PixelDecode::end of type std::exception:" << e.what() << endl;
  }
  catch(char const* msg){
    cout<<"Exception caught in PixelDecode::end of type char const*: " << msg << endl;
  }
  catch(...){
    cout<<"Exception caught in PixelDecode::end of unknown type." << endl;
  }

}


/* ****** *
 * decode *
 * ****** */

/* ********************************************** *
 * Raw data decoded and stored in shared objects. *
 * ********************************************** */
extern "C" void
decode(const std::vector<uint32_t const *>* rods,
       const std::vector<unsigned long int>* sizes,
       const uint32_t *event, unsigned long int event_size)
{
  try {
    static uint cnt(0);
    static uint cntmod(1);
    if(cnt%cntmod==0) {
      if(cnt>=10*cntmod) cntmod*=10;
      std::cout << cnt << ": " << __PRETTY_FUNCTION__ << " with arguments:" << std::endl;
      std::cout << cnt << ": " << "rods->size()=" << rods->size() << ", sizes->size()=" << sizes->size() << std::endl;
      std::cout << cnt << ": " << "*event=" << *event << ", event_size=" << event_size << std::endl;
      ++cnt;
    }
  
    if (PrintOutLevel>=2){
      printf("PixelDecode: calling decode\n");
      fflush(stdout);
    }

    static int counter_RCW; // counter for rodCheckWord warnings
    static int counter_Header; // counter for header errors
    
    const uint32_t *rod;
    int32_t beginStatus, beginData;
    uint32_t rodSize,
      rodCheckWord,
      //rodFormatVersion,
      rodSubDetector,
      rodRoLink,
      runNumber,
      extendedL1Id,
      bunchCross,
      level1Type,
      //detEventType,
      numStatus,
      rodStatusWord1,
      rodStatusWord2,
      numData,
      endData,
      dataHeader,
      preambleError,
      timeOutError,
      level1Error,
      bunchCrossError,
      linkNumber,
      level1Skips,
      level1Id,
      bunchCrossId,
      dataTrailer,
      trailerError,
      limitError,
      overFlow,
      FEnumber,
      pixelRow,
      pixelCol,
      pixelToT,
      MCCerrorCode,
      FEerrorCode,
      rawData,
      eventFragmentSize,
      sourceID,
      bunchCrossingTimeSec,
      bunchCrossingTimeNanoSec;
    uint32_t globalEventID, RunType, RunNR, LumiBlockNR, ExtL1ID, BCID;   // from ATLAS header

    pixel->resetEvent();

    eventFragmentSize = event[1];
    sourceID = event[4];
    bunchCrossingTimeSec = event[8];
    bunchCrossingTimeNanoSec = event[9];
    globalEventID = event[10];
    RunType= event[11];
    RunNR= event[12];
    LumiBlockNR= event[13];
    ExtL1ID= event[14];
    BCID= event[15];

    pixel->setEventEventData(eventFragmentSize, sourceID, bunchCrossingTimeSec, bunchCrossingTimeNanoSec, 
			     globalEventID, RunType, RunNR, LumiBlockNR, ExtL1ID, BCID);

    for (uint32_t k=0; k<rods->size(); k++){
      rod = (*rods)[k];
      rodSize = (uint32_t)(*sizes)[k];

      if(rodSize<10){
	cout<<"rodSize="<<rodSize<<" is too small, skipping..."<<endl;
	continue;
      }

      uint32_t theRodId = rod[3];
      
      // ---- start of cut 
      // decode ROD header content
      //unused: rodFormatVersion = rod[2];
      //   if (rodFormatVersion==0x03000000)
      //       rodSubDetector = (rod[3] & RodSourceIdMask300) >> 16;
      //     else if (rodFormatVersion==0x02040000)
      //       rodSubDetector = (rod[3] & RodSourceIdMask204) >> 8;
      rodSubDetector = (rod[3] & 0x00ff0000);

      //std::cout << "SubDetector ? " <<  rodSubDetector << std::endl;
      if ((rodSubDetector&0x00f00000)!=PixelAnyId) continue; // MPG ADDED THIS

      static int cnt(0);
      if(cnt++<100) {
	std::cout << __PRETTY_FUNCTION__ << ": ";
	std::cout << "List of sub-detector IDs being considered by this instance: " << std::hex;
	std::copy(pixel->subDetIdList.begin(), pixel->subDetIdList.end(), std::ostream_iterator<uint32_t>(std::cout, ";"));
	std::cout << std::dec << std::endl;
      }

      //uint32_t theRodId = rod[3];

      //// For IBL and DBM, the ROD_ID ends in 0 and then there are four possible ROL_IDs = ROD_ID+0, ROD_ID+1, ROD_ID+2, ROD_ID+3
      if(rodSubDetector == PixelIblId || rodSubDetector == PixelDbmId) theRodId = rod[3] & 0x00fffff0;

      std::vector<uint32_t>::iterator it_subDetId = std::find(pixel->subDetIdList.begin(), pixel->subDetIdList.end(), theRodId);
      if(it_subDetId == pixel->subDetIdList.end()) continue;

      if(cnt<100) std::cout << "ROD ID: " << std::hex << theRodId << std::dec << std::endl;
      //---- end of cut

      beginStatus = beginOfRodStatusBlock(rod,rodSize); // call these once here and pass to rodCheck
      beginData = beginOfRodDataBlock(rod,rodSize);
      rodCheckWord = rodCheck(rod,rodSize,beginStatus,beginData);

      // was here before putting it above: uint32_t theRodId = rod[3];

      // if ((rodCheckWord&0xffffffff)!=0){
      //     std::cout << "value if different from 0: " << rodCheckWord <<std::endl;
      //     if ((rodCheckWord&0xfffffffb)!=0) std::cout << "should raise a warning" << std::endl;
      // }

      if ((rodCheckWord&0x00000003)!=0){ // header must have ROD marker and correct size to continue
	ERS_PIX_WARNING("ROD ID: " << showbase << hex << theRodId << " - rodCheckWord = " << rodCheckWord << dec, DecodeError);

        time_t rawtime;
        struct tm * timeinfo;
        char buffer [14];
        
        time ( &rawtime );
        timeinfo = localtime ( &rawtime );
        strftime (buffer,14,"%I:%M:%S",timeinfo);

        
        if(counter_RCW++ <500){ // diane - printing full word for corruption cases (stopping at 500 not to exceed logfile size
            cout << " ----- Printing full fragment for ROD ID: " << showbase << hex << theRodId << dec << " -----" <<endl;
            cout << "At " << buffer << ", coming from rodCheckWord alarm: " << showbase << hex << rodCheckWord << dec << endl;
            for(uint32_t y=0; y<rodSize; y++){
                cout << y << " - " << showbase << hex << rod[y] << dec << endl;
            }
            cout << " --- End of fragment for ROD ID: " << showbase << hex << theRodId << dec << " -----" << endl;
        }
        
	continue;
      }

      // ---- cut was here before

      
      rodRoLink = (rod[3] & 0x00ffffff);
      runNumber = rod[4];
      extendedL1Id = rod[5];
      bunchCross = rod[6];
      level1Type = rod[7];
      //unused: detEventType = rod[8];

      //DADA: this should raise the 0x8 rodCheckWord alarm
      if ((rodCheckWord&0xfffffffb)!=0){ // check all bits, except format version
          ERS_PIX_WARNING("ROD ID: " << showbase << hex << theRodId << " - rodCheckWord = " << rodCheckWord << " , 0x8.. is fragment size error."<< dec, DecodeError);

        time_t rawtime;
        struct tm * timeinfo;
        char buffer [14];
        
        time ( &rawtime );
        timeinfo = localtime ( &rawtime );
        strftime (buffer,14,"%I:%M:%S",timeinfo);

        // TMP 14/05/18 - dump only for known problematic channels - TBR ASAP !
        if(theRodId == 0x111752 || theRodId == 0x111861 || theRodId == 0x111857 || theRodId == 0x130156 || theRodId == 0x130116 || theRodId == 0x130250 || theRodId == 0x130105 || theRodId == 0x130256 || theRodId == 0x130215 || theRodId == 0x130146 || theRodId == 0x130158){
            if(counter_RCW++ <500){ // diane - printing full word for corruption cases (stopping at 500 not to exceed logfile size
                cout << " ----- Printing full fragment for ROD ID: " << showbase << hex << theRodId << dec << " -----" <<endl;
                cout << "At " << buffer << ", coming from rodCheckWord alarm: " << showbase << hex << rodCheckWord << dec << endl;
                for(uint32_t y=0; y<rodSize; y++){
                    cout << y << " - " << showbase << hex << rod[y] << dec << endl;
                }
                cout << " --- End of fragment for ROD ID: " << showbase << hex << theRodId << dec << " -----" << endl;
            }
        }
        
	continue;
      }

      // decode ROD status block content
      numStatus = rod[rodSize-3];
      rodStatusWord1 = rod[beginStatus];
      rodStatusWord2 = numStatus>1 ? rod[beginStatus+1] : 0;

      pixel->setEventRodData(rodSubDetector,rodRoLink,runNumber,
			     extendedL1Id,bunchCross,level1Type,
			     rodStatusWord1,rodStatusWord2);

      // decode ROD data block content
      numData = rod[rodSize-2]; 
      endData = beginData + numData - 1;
      dataHeader = beginData;
      //dataTrailer = endData; 

      while (dataHeader<endData){
	// pattern 0x001... from module header
	
	if ((rod[dataHeader] & 0xe0000000)!=0x20000000){  // unexpected word found
            ERS_PIX_WARNING("ROD ID: " << showbase << hex << theRodId << " - Data header not starting with 0x001... ("  
                            << rod[dataHeader] << dec << ")", DecodeError);

            time_t rawtime;
            struct tm * timeinfo;
            char buffer [14];
            
            time ( &rawtime );
            timeinfo = localtime ( &rawtime );
            strftime (buffer,14,"%I:%M:%S",timeinfo);

            // TMP 14/05/18 - dump only for known problematic channels - TBR ASAP !
            if(theRodId == 0x111752 || theRodId == 0x111861 || theRodId == 0x111857 || theRodId == 0x130156 || theRodId == 0x130116 || theRodId == 0x130250 || theRodId == 0x130105 || theRodId == 0x130256 || theRodId == 0x130215 || theRodId == 0x130146 || theRodId == 0x130158){
                if(counter_Header++ <500){ // diane - printing full word for corruption cases (stopping at 500 not to exceed logfile size
                    cout << " ----- Printing full fragment for ROD ID: " << showbase << hex << theRodId << dec << " -----" <<endl;
                    cout << "At " << buffer << ", coming from \"Data header not starting with 0x001...\"" << endl;
                    for(uint32_t y=0; y<rodSize; y++){
                        cout << y << " - " << showbase << hex << rod[y] << dec << endl;
                    }
                    cout << " --- End of fragment for ROD ID: " << showbase << hex << theRodId << dec << " -----" << endl;
                }
            }

            ++dataHeader;
	} else {
	  dataTrailer = dataHeader + 1; // Trailer NOT yet found
  // Find trailer, begins with 0x010...
	  while ((dataTrailer<=endData) && ((rod[dataTrailer] & 0xe0000000)!=0x40000000)){
	    ++dataTrailer;
	  }
	  if (dataTrailer>endData){
	    break; // didn't find any trailer, should probably be an error, FIXME
	  }


	  if(rodSubDetector == PixelIblId || rodSubDetector == PixelDbmId) { //// IBL/DBM Decoding ////
	
	    // decode link header content
	    //// header 001n nnnn FLLL LLLL LLLL LLBB BBBB BBBB (iblRodBocManual v1.2.3, p67)
	    linkNumber = (rod[dataHeader] & 0x07000000) >> 24; //// nnnnn: slave ID(1b), efb ID(1b), formatter ID(1b), link ID(2b)
	    //unused: uint32_t feI4Bflag = (rod[dataHeader] & 0x00800000) >> 23;
	    level1Skips = 0; // absent
	    level1Id = (rod[dataHeader] & 0x007ffc00) >> 10;
	    bunchCrossId = (rod[dataHeader] & 0x000003ff);
	  
	    // decode link trailer content
	    //// trailer 010n nnnn EcPp lbzh vMMM MMMM MMMB BBBB (iblRodBocManual v1.2.3, p67)

	    //unused: uint32_t linkNumberTrailer = (rod[dataTrailer] & 0x07000000) >> 24; // check that this matches header?
	    timeOutError = (rod[dataTrailer] & 0x00800000) >> 23;
	    //unused: uint32_t condensedMode = (rod[dataTrailer] & 0x00400000) >> 22;
	    //unused: uint32_t linkMaskedByPpc = (rod[dataTrailer] & 0x00200000) >> 21;
	    preambleError = (rod[dataTrailer] & 0x00100000) >> 20;
	    level1Error = (rod[dataTrailer] & 0x00080000) >> 19;
	    bunchCrossError = (rod[dataTrailer] & 0x00040000) >> 18;
	    trailerError = (rod[dataTrailer] & 0x00020000) >> 17;
	    limitError = (rod[dataTrailer] & 0x00010000) >> 16;
	    overFlow = (rod[dataTrailer] & 0x00008000) >> 15; // called "row/column error" in iblRodBocManual

	    // This will need to be updated if we want to include new variables
	    pixel->setEventModData(preambleError,timeOutError,level1Error,
				   bunchCrossError,linkNumber,level1Skips,level1Id,
				   bunchCrossId,trailerError,limitError,overFlow);

	    for (uint32_t j=dataHeader+1; j<dataTrailer; ++j){

	      if ((rod[j] & 0xe0000000)==0x80000000){ // hit (long)
		// Long hit bit pattern:
		//  100n nnnn TTTT TTTT CCCC CCCR RRRR RRRR
	      
		//uint32_t linkNum = (rod[j] & 0x07000000) >> 24; // not sure if we want to use this, but it's there
		pixelToT = (rod[j] & 0x00ff0000) >> 16;
		pixelCol = (rod[j] & 0x0000fe00) >> 9;
		pixelRow = (rod[j] & 0x000001ff);
		addIblHits(pixelToT,pixelCol,pixelRow,theRodId);

	      } else if ((rod[j] & 0xe0000000)==0xa0000000){ // hit (condensed mode)
		// Condensed mode bit pattern:
		//  101RRRRRTTTTTTTTCCCCCCCRRRRRRRRR
		//  1CCCRRRRRRRRRTTTTTTTTCCCCCCCRRRR
		//  1TTTCCCCCCCRRRRRRRRRTTTTTTTTCCCC
		//  111TTTTTTTTCCCCCCCRRRRRRRRRTTTTT
	      
		if ( (rod[j+1] & 0x80000000) != 0x80000000  || 
		     (rod[j+2] & 0x80000000) != 0x80000000  || 
		     (rod[j+3] & 0xe0000000) != 0xe0000000  ){
		  // MSBs don't have the correct pattern for condensed mode
		  ERS_PIX_WARNING("MSBs in some words are wrong for a condenced hit:" << showbase 
				  << hex << rod[j] <<" " << rod[j+1] <<" " << rod[j+2] <<" " << rod[j+3] <<" " 
				  << dec, DecodeError);
		  continue; // go on to next word
		}	      

		// first hit pair
		// j:  ---- ---- TTTT TTTT CCCC CCCR RRRR RRRR
		pixelRow = (rod[j] & 0x000001ff);
		pixelCol = (rod[j] & 0x0000fe00) >> 9;
		pixelToT = (rod[j] & 0x00ff0000) >> 16;
		addIblHits(pixelRow,pixelCol,pixelToT,theRodId);

		// second hit pair
		// j:  ---R RRRR ---- ---- ---- ---- ---- ----
		// j+1:---- ---- ---- -TTT TTTT TCCC CCCC RRRR
		pixelRow = (rod[j] & 0x1f000000) >> 24 | (rod[j+1] & 0x0000000f) << 5;
		pixelCol = (rod[j+1] & 0x000007f0) >> 4;
		pixelToT = (rod[j+1] & 0x0007f800) >> 11;
		addIblHits(pixelRow,pixelCol,pixelToT,theRodId);

		// third hit pair
		// j+1:-CCC RRRR RRRR R--- ---- ---- ---- ----
		// j+2:---- ---- ---- ---- ---- TTTT TTTT CCCC
		pixelRow = (rod[j+1] & 0x0ff80000) >> 19;
		pixelCol = (rod[j+1] & 0x70000000) >> 28 | (rod[j+2] & 0x0000000f) << 3;
		pixelToT = (rod[j+2] & 0x00000ff0) >> 4;
		addIblHits(pixelRow,pixelCol,pixelToT,theRodId);

		// fourth hit pair
		// j+2:-TTT CCCC CCCR RRRR RRRR ---- ---- ----
		// j+3:---- ---- ---- ---- ---- ---- ---T TTTT
		pixelRow = (rod[j+2] & 0x001ff000) >> 12;
		pixelCol = (rod[j+2] & 0x0fe00000) >> 21;
		pixelToT = (rod[j+2] & 0x70000000) >> 28 | (rod[j+3] & 0x0000001f) << 3;
		addIblHits(pixelRow,pixelCol,pixelToT,theRodId);

		// fifth hit pair
		// j+3:---T TTTT TTTC CCCC CCRR RRRR RRR- ----
		pixelRow = (rod[j+3] & 0x00003fe0) >> 5;
		pixelCol = (rod[j+3] & 0x001fc000) >> 14;
		pixelToT = (rod[j+3] & 0x1fe00000) >> 21;
		addIblHits(pixelRow,pixelCol,pixelToT,theRodId);

		j = j+3; // decoded up to j+3

		//} else if ((rod[j] & 0xf0001e00)==0x10001e00){ //FE flag error <-- this was wrong
	      } else if ((rod[j] & 0xe0000000)==0x00000000){ //FE flag error
		//// FE flag error 000n nnnn xSSS SSSx xxxx xxDD DDDD DDDD (iblRodBocManual v1.2.3, p67)
		FEnumber = 0; // absent
		MCCerrorCode = 0; // absent
		FEerrorCode = 0; // absent
		//uint32_t linkNum = (rod[j] & 0x07000000) >> 24; // not sure if we want to use this, but it's there
		// FE error codes have a different meaning for IBL, so using different variables:
		uint32_t FEi4errorCode = (rod[j] & 0x007e0000) >> 17;
		uint32_t FEi4errorCount = (rod[j] & 0x000003ff);
		pixel->setEventFeeFlagIbl(FEnumber,FEi4errorCode,FEi4errorCount);
	      
	      } else if ((rod[j] & 0xe0000000)==0x30000000){ // IBL many not have this, need to check
		rawData = (rod[j] & 0x1fffffff);
		pixel->setEventRawData(rawData);
	      
	      } else if (rod[j]==0x20004000){ // I haven't seen thin in the documentation, need to check
		pixel->setEventTimeOut();
	      }
	    }
	  
	  } else { //// Pixel Decoding ////
	  
	    // decode link header content
	    preambleError = (rod[dataHeader] & 0x10000000) >> 28;
	    timeOutError = (rod[dataHeader] & 0x08000000) >> 27;
	    level1Error = (rod[dataHeader] & 0x04000000) >> 26;
	    bunchCrossError = (rod[dataHeader] & 0x02000000) >> 25;
	    // note: link masked by DSP is bit 23, but was not read in this code -Joe
	    linkNumber = (rod[dataHeader] & 0x007f0000) >> 16;
	    level1Skips = (rod[dataHeader] & 0x0000f000) >> 12;
	    level1Id = (rod[dataHeader] & 0x00000f00) >> 8;
	    bunchCrossId = (rod[dataHeader] & 0x000000ff);
	    // decode link trailer content
	    trailerError = (rod[dataTrailer] & 0x10000000) >> 28;
	    limitError = (rod[dataTrailer] & 0x08000000) >> 27;
	    overFlow = (rod[dataTrailer] & 0x04000000) >> 26;
	    pixel->setEventModData(preambleError,timeOutError,level1Error,
				   bunchCrossError,linkNumber,level1Skips,level1Id,
				   bunchCrossId,trailerError,limitError,overFlow);
	    // decode hit content
	    for (uint32_t j=dataHeader+1; j<dataTrailer; ++j){
	      if ((rod[j] & 0xe0000000)==0x80000000){
	      
		FEnumber = (rod[j] & 0x0f000000) >> 24;
		pixelToT = (rod[j] & 0x00ff0000) >> 16;
		pixelCol = (rod[j] & 0x00001f00) >> 8;
		pixelRow = (rod[j] & 0x000000ff);
		//	    pixelToT = (rod[j] & 0x000000ff);
	      
		pixel->setEventHitData(FEnumber,pixelRow,pixelCol,pixelToT);

	      } else if ((rod[j] & 0xf0001e00)==0x00001e00){
		FEnumber = (rod[j] & 0x000000f0) >> 4;
		//	    FEnumber = (rod[j] & 0x0f000000) >> 24;
		MCCerrorCode = 0;
		FEerrorCode = (rod[j] & 0x0000000f);
		pixel->setEventFeeFlag(FEnumber,FEerrorCode,MCCerrorCode);

	      } else if ((rod[j] & 0xf01f0000)==0x101f0000){
		FEnumber = (rod[j] & 0x0f000000) >> 24;
		MCCerrorCode = (rod[j] & 0x0000ff00) >> 8;
		FEerrorCode = (rod[j] & 0x000000ff);
		pixel->setEventFeeFlag(FEnumber,FEerrorCode,MCCerrorCode);

	      } else if ((rod[j] & 0xe0000000)==0x30000000){
		rawData = (rod[j] & 0x1fffffff);
		pixel->setEventRawData(rawData);

	      } else if (rod[j]==0x20004000){
		pixel->setEventTimeOut();
	      }
	    }
	  }

	  dataHeader = dataTrailer + 1;
	}
      }
    }
  }
  catch(std::exception& e){
    cout<<"Exception caught in PixelDecode::decode of type std::exception:" << e.what() << endl;
  }
  catch(char const* msg){
    cout<<"Exception caught in PixelDecode::decode of type char const*: " << msg << endl;
  }
  catch(...){
    cout<<"Exception caught in PixelDecode::decode of unknown type." << endl;
  }
}


/* **************** *
 * Custom functions *
 * **************** */

int32_t beginOfRodDataBlock(const uint32_t *rod, uint32_t size)
{
  if (PrintOutLevel>=1){
    printf("PixelDecode: Entering beginOfRodDataBlock\n");
    fflush(stdout);
  }

  int32_t begin;
  uint32_t headerSize = rod[1];
  uint32_t statusBlockPosition = rod[size-1];
  uint32_t numberOfStatusElements = rod[size-3];
  if (statusBlockPosition==1) begin = headerSize;
  else if (statusBlockPosition==0) begin = headerSize + numberOfStatusElements;
  else begin = -1;
  if (begin>(int32_t)size) begin = -2;

  if (PrintOutLevel>=1){
    printf("PixelDecode: Leaving beginOfRodDataBlock (%i)\n",begin);
    fflush(stdout);
  }

  return begin;
}

int32_t beginOfRodStatusBlock(const uint32_t *rod, uint32_t size)
{
  if (PrintOutLevel>=1){
    printf("PixelDecode: Entering beginOfRodStatusBlock\n");
    fflush(stdout);
  }

  int32_t begin;
  uint32_t headerSize = rod[1];
  uint32_t statusBlockPosition = rod[size-1];
  uint32_t numberOfDataElements = rod[size-2]; 
  if (statusBlockPosition==0) begin = headerSize;
  else if (statusBlockPosition==1) begin = headerSize + numberOfDataElements;
  else begin = -1;
  if (begin>(int32_t)size) begin = -2;

  if (PrintOutLevel>=1){
    printf("PixelDecode: Leaving beginOfRodStatusBlock (%i)\n",begin);
    fflush(stdout);
  }

  return begin;
}


uint32_t rodCheck(const uint32_t *rod, uint32_t size, int32_t beginStatus, int32_t beginData)
{
  if (PrintOutLevel>=1){
    printf("PixelDecode: Entering rodCheck\n");
    fflush(stdout);
  }

  uint32_t checkWord = 0x00000000;
    
  if (rod[0]!=RodHeaderMarker){
    ERS_PIX_INFO("ROD not starting with header marker (" << showbase << hex
		  << rod[0] << dec << ")");
    checkWord |= 0x00000001;
  }
  uint32_t headerSize = rod[1];
  if (headerSize!=9){
    ERS_PIX_INFO("Header size is " << dec << headerSize);
    checkWord |= 0x00000002;
  }
  //   if (rod[2]!=0x03010000 && rod[2]!=0x0400000){
  //     ERS_DEBUG(1,"Unknown format (major) version number (" << hex <<rod[2]
  // 	      << dec << ")");
  //     checkWord |= 0x00000004;
  //   }
  uint32_t numberOfStatusElements = rod[size-3]; 
  if (numberOfStatusElements!=2){
    ERS_PIX_INFO(" Number of status elements is, test, " << dec
		  << numberOfStatusElements);
    checkWord |= 0x00000008;
    //std::cout << "culprit on status elements: " << showbase << hex << checkWord << dec << std::endl;
  }
  uint32_t numberOfDataElements = rod[size-2];
  uint32_t totalLength = headerSize + numberOfStatusElements +
    numberOfDataElements + 3;
  if (totalLength!=size){
    ERS_PIX_INFO("Total ROD length is " << dec << totalLength
		  << " instead of " << size);
    checkWord |= 0x00000010;
  }
  uint32_t statusBlockPosition = rod[size-1];
  if (statusBlockPosition>1){
    ERS_PIX_INFO("Status Block Position is " << dec << statusBlockPosition);
    checkWord |= 0x00000020;
  }

  //int32_t beginData = beginOfRodDataBlock(rod,size);
  if (beginData < -1){
    ERS_PIX_INFO("Data Block Position is beyond ROD Size (" << dec
		  << beginData << ")");
    checkWord |= 0x00000040;
  }

  //int32_t beginStatus = beginOfRodStatusBlock(rod,size);
  if (beginStatus<-1){
    ERS_PIX_INFO("Status Block Position is beyond ROD Size (" << dec
		  << beginStatus << ")");
    checkWord |= 0x00000080;
  }

  if (statusBlockPosition==1){
    if (beginData!=(int32_t)headerSize){
      ERS_PIX_INFO("Data Block Position is " << dec << beginData
		    << " instead of " << headerSize);
      checkWord |= 0x00000100;
    }
  } else {
    if (beginStatus!=(int32_t)headerSize){
      ERS_PIX_INFO("Status Block Position is " << dec << beginStatus
		    << " instead of " << headerSize);
      checkWord |= 0x000000200;
    }
  }
  int32_t dataSize = statusBlockPosition==0 ? (size-3) - beginData : beginStatus - headerSize;
  if (dataSize!=(int32_t)numberOfDataElements){
    ERS_PIX_INFO(dec << dataSize << " instead of "
		  << numberOfDataElements << " data elements");
    checkWord |= 0x00000400;
  }
  int32_t statusSize = statusBlockPosition==1 ? (size-3) - beginStatus : beginData - headerSize;
  if (statusSize!=(int32_t)numberOfStatusElements){
    ERS_PIX_INFO(dec << statusSize << " instead of "
		  << numberOfStatusElements << " status elements");
    checkWord |= 0x00000800;
  }

  if (PrintOutLevel>=1){
    printf("PixelDecode: Leaving rodCheck (%i)\n",checkWord);
    fflush(stdout);
  }

  return checkWord;
}

void addIblHits(uint32_t pixelToT, uint32_t pixelCol, uint32_t pixelRow, uint32_t theRodId){

  //static int counter_ToT; // TMP Diane: counter for no hits
  uint32_t FEnumber = 0; // absent for IBL
  uint32_t tot1 = (pixelToT & 0xf0) >> 4; // ToT for (row, column)
  uint32_t tot2 = pixelToT & 0x0f; // ToT for (row+1, column)

  if(tot1 == 0x0){// no hit
    //if(theRodId != 0x150210) { //explicitly exclude DBM for now...
      ERS_PIX_WARNING("ROD ID: " << showbase << hex << theRodId << " ToT has no hit (first pixel in pair): ToT=" << tot1 << dec //add ROD ID
		      << ", col=" << pixelCol 
		      << ", row=" << pixelRow, DecodeError);
      
      // TMP Diane: need to find a way to print the fragment for this warning too.
      //if(counter_ToT++ <500){ // diane - printing full word for corruption cases (stopping at 500 not to exceed logfile size
      //    cout << " ----- Printing full fragment for ROD ID: " << showbase << hex << theRodId << dec << " -----" <<endl;
      //    cout << "--> coming from \"ToT has no hit (first pixel in pair)\" "<< endl;
      //    for(uint32_t y=0; y<rodSize; y++){
      //        cout << y << " - " << showbase << hex << rod[y] << dec << endl;
      //    }
      //    cout << " --- End of fragment for ROD ID: " << showbase << hex << theRodId << dec << " -----" << endl;
      //}
      //}
  }else{
 
    pixel->setEventHitData(FEnumber,pixelRow,pixelCol,tot1);

    // only check for hit in second pixel if the first one had a hit
    if( tot2 == 0x0 || tot2 == 0xf ){ // no hit for second pixelq
      // Joe: This is okay, so don't send a warning
      // ERS_PIX_WARNING("ToT has no hit (second pixel in pair)"<< showbase << hex 
      // 		      << ": ToT=" << tot2
      //                      << dec  
      // 		      << ", col=" << pixelCol 
      // 		      << ", row+1=" << pixelRow+1 
      // 		      << dec, DecodeError);
    }else{ 
      pixel->setEventHitData(FEnumber,pixelRow+1,pixelCol,tot2);
    }
  }
}
