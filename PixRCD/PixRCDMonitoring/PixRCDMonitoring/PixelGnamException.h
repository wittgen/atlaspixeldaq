#ifndef PixelGnamException_h
#define PixelGnamException_h

#include <string>
#include <sstream>
#include "ers/ers.h"
#include "gnam/gnamutils/GnamLibException.h"

namespace daq{

  ERS_DECLARE_ISSUE_BASE(PixRCDMonitoring,
                         PixRCDMonitoringBase,
                         gnamlib::LibraryUnusableBase, , , ) VSJFSC;

  ERS_DECLARE_ISSUE_BASE(PixRCDMonitoring,
			 Information,
			 PixRCDMonitoring::PixRCDMonitoringBase,
                         "Information: "<<message,
			 ,
			 ((std::string) message)) VSJFSC;

  ERS_DECLARE_ISSUE_BASE(PixRCDMonitoring,
			 Debug1,
			 PixRCDMonitoring::PixRCDMonitoringBase,
                         "\tDebug: "<<message,
			 ,
			 ((std::string) message)) VSJFSC;

  ERS_DECLARE_ISSUE_BASE(PixRCDMonitoring,
			 Debug2,
			 PixRCDMonitoring::PixRCDMonitoringBase,
                         "\t\tDebug: "<<message,
			 ,
			 ((std::string) message)) VSJFSC;

  ERS_DECLARE_ISSUE_BASE(PixRCDMonitoring,
			 Debug3,
			 PixRCDMonitoring::PixRCDMonitoringBase,
                         "\t\t\tDebug: "<<message,
			 ,
			 ((std::string) message)) VSJFSC;

  ERS_DECLARE_ISSUE_BASE(PixRCDMonitoring,
			 DecodeError,
			 PixRCDMonitoring::PixRCDMonitoringBase,
                         "Decoding error: "<<message<<")",
			 ,
			 ((std::string) message)) VSJFSC;

  ERS_DECLARE_ISSUE_BASE(PixRCDMonitoring,
			 ConfigError,
			 PixRCDMonitoring::PixRCDMonitoringBase,
                         "Configuration error: "<<message,
			 ,
			 ((std::string) message)) VSJFSC;

  ERS_DECLARE_ISSUE_BASE(PixRCDMonitoring,
			 MapError,
			 PixRCDMonitoring::PixRCDMonitoringBase,
                         "Mapping error: "<<message,
			 ,
			 ((std::string) message)) VSJFSC;

  ERS_DECLARE_ISSUE_BASE(PixRCDMonitoring,
			 ConnError,
			 PixRCDMonitoring::PixRCDMonitoringBase,
                         "Connectivity error: "<<message,
			 ,
			 ((std::string) message)) VSJFSC;

} // daq namespace

/* From common/ers/ers/ers.h                                                 */
/* -------------------------                                                 */
/* ers::debug - "lstdout" - prints issues to the standard C++ output stream  */
/* ers::info - "lstdout" - prints issues to the standard C++ output stream   */
/* ers::warning - "lstderr" - prints issues to the standard C++ error stream */
/* ers::error - "lstderr" - prints issues to the standard C++ error stream   */
/* ers::fatal - "lstderr" - prints issues to the standard C++ error stream   */

#ifndef ERS_PIX_INFO
#define ERS_PIX_INFO(message)						\
  {									\
    ERS_REPORT_IMPL(ers::info,daq::PixRCDMonitoring::Information,message, ); \
  }
#endif

#ifndef ERS_PIX_DEBUG
#define ERS_PIX_DEBUG(level,message)					\
  {									\
    if (level==0)							\
      ERS_PIX_INFO(message);						\
    if (level==1)							\
      ERS_REPORT_IMPL(ers::debug,daq::PixRCDMonitoring::Debug1,message,1); \
    if (level==2)							\
      ERS_REPORT_IMPL(ers::debug,daq::PixRCDMonitoring::Debug2,message,2); \
    if (level==3)							\
      ERS_REPORT_IMPL(ers::debug,daq::PixRCDMonitoring::Debug3,message,3); \
  }
#endif

#ifndef ERS_PIX_WARNING
#define ERS_PIX_WARNING(message,issue)					\
  {									\
    ERS_REPORT_IMPL(ers::warning,daq::PixRCDMonitoring::issue,message, ); \
  }
#endif

#ifndef ERS_PIX_ERROR
#define ERS_PIX_ERROR(message,issue)					\
  {									\
    ERS_REPORT_IMPL(ers::error,daq::PixRCDMonitoring::issue,message, );	\
  }
#endif

#ifndef ERS_PIX_FATAL
#define ERS_PIX_FATAL(message,issue)					\
  {									\
    ERS_REPORT_IMPL(ers::fatal,daq::PixRCDMonitoring::issue,message, );	\
  }
#endif

#endif
