#ifndef PixelEvent_h
#define PixelEvent_h

using namespace std;

#include <stdint.h>
#include <vector>

typedef struct eventHeaderData_s {
  uint32_t eventFragmentSize;
  uint32_t eventheaderSize;
  uint32_t formatversion;
  uint32_t sourceID;
  uint32_t numStatus;
  uint32_t status0;    
  uint32_t checkSumType;
  uint32_t bunchCrossingTimeSec;
  uint32_t bunchCrossingTimeNanoSec;
  uint32_t globalEventID;
  uint32_t RunType;
  uint32_t RunNR;
  uint32_t LumiBlockNR;
  uint32_t ExtL1ID;
  uint32_t BCID;
} eventHeaderData;

typedef struct rodData_s{
  // ROD header information
  uint32_t rodSubDetector;
  uint32_t rodRoLink;
  uint32_t runNumber;
  uint32_t extendedL1Id;
  uint32_t bunchCross;
  uint32_t level1Type;
  // ROD status information
  uint32_t rodStatusWord1;
  uint32_t rodStatusWord2;
} rodData;

typedef struct modData_s{
  uint32_t inRod;
  uint32_t timeSlot;
  // Link header information
  uint32_t preambleError;
  uint32_t timeOutError;
  uint32_t level1Error;
  uint32_t bunchCrossError;
  uint32_t linkNumber;
  uint32_t level1Skips;
  uint32_t level1Id;
  uint32_t bunchCrossId;
  uint32_t bunchCrossId_offset; // offset to calculate LVL1A (timeSlot)
  // Link trailer information
  uint32_t trailerError;
  uint32_t limitError;
  uint32_t overFlow;
  // FE information
  uint32_t rawData;
  uint32_t timeOut;
  uint32_t numHits;
} modData;


typedef struct hitData_s{
  uint32_t inMod;
  // Hit information
  uint32_t FEnumber;
  uint32_t pixelRow;
  uint32_t pixelCol;
  uint32_t pixelToT;
} hitData;

typedef struct feeFlag_s{
  uint32_t inMod;
  // FE error flag for Pixel
  uint32_t FEnumber;
  uint32_t FEerrorCode;
  uint32_t MCCerrorCode;
  // For IBL (different meaning so different variables)
  bool IblError;
  uint32_t IblFEerrorNumber;
  uint32_t IblFEerrorWeight;

} feeFlag;

typedef struct rawDataWord_s{
  uint32_t inMod;
  uint32_t rawData;
} rawDataWord;

typedef struct timeOutData_s{
  uint32_t inMod;
} timeOutData;

class PixelEvent{
 public:
  eventHeaderData eventInfo;

  uint32_t NRods;
  vector<rodData> rod;

  uint32_t NMods;
  vector<modData> mod;

  uint32_t NHits;
  vector<hitData> hit;

  uint32_t NFlag;
  vector<feeFlag> fee;

  uint32_t NRawData;
  vector<rawDataWord> rawData;

  uint32_t NTimeOut;
  vector<timeOutData> timeOut;

  //DADA
  vector<uint32_t> subDetIdList;

  PixelEvent();
  virtual ~PixelEvent();
  void resetEvent();

  void setSubDetId(uint32_t source_id);
  void setEventEventData(uint32_t eventFragmentSize,
			 uint32_t sourceID,
			 uint32_t bunchCrossingTimeSec,
			 uint32_t bunchCrossingTimeNanoSec,
			 uint32_t globalEventID,
			 uint32_t RunType,
			 uint32_t RunNR,
			 uint32_t LumiBlockNR,
			 uint32_t ExtL1ID,
			 uint32_t BCID);
 
  void setEventRodData(uint32_t RodSubDetector,
		       uint32_t RodRoLink,
		       uint32_t RunNumber,
		       uint32_t ExtendedL1Id,
		       uint32_t BunchCross,
		       uint32_t Level1Type,
		       uint32_t RodStatusWord1,
		       uint32_t RodStatusWord2);
  void setEventModData(uint32_t PreambleError,
		       uint32_t TimeOutError,
		       uint32_t Level1Error,
		       uint32_t BunchCrossError,
		       uint32_t LinkNumber,
		       uint32_t Level1Skips,
		       uint32_t Level1Id,
		       uint32_t BunchCrossId,
		       uint32_t TrailerError,
		       uint32_t LimitError,
		       uint32_t OverFlow);
  void setEventHitData(uint32_t FENumber,
		       uint32_t PixelRow,
		       uint32_t PixelCol,
		       uint32_t PixelToT);
  void setEventFeeFlag(uint32_t FENumber,
		       uint32_t FEErrorCode,
		       uint32_t MCCErrorCode);
  void setEventFeeFlagIbl(uint32_t FENumber,
			  uint32_t IblFEErrorNumber,
			  uint32_t IblFEErrorWeight);

  void setEventRawData(uint32_t RawData);

  void setEventTimeOut();

};

#endif
