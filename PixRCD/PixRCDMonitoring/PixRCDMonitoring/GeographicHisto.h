#ifndef __GeographicHisto__
#define __GeographicHisto__

#include "gnam/gnamutils/GnamUtils.h"

inline double Round(double number){
  return floor(number+0.5);
}

// Hands over the starting position as well as the end position of the line
// and the histgoram which should be filled
void DrawLine(const int& x1, const int& y1, const int& x2, const int& y2,
	      GnamHisto *histoname);

// Hands over the position where the filling is started, the fill color, the
// color which will be overwritten and the histogram which will be filled
void Fill4(int x, int y, const int& fill_col, const int& start_col,
	   GnamHisto *histoname);

// Hands over the center of the circle, the radius, the number of the modules
// in the disk, at which Endcap the disk is mounted, the number of the disk
// and the histogram which should be filled
void DrawDisk(const int& x0, const int& y0, const int& r, const int& nmodules,
	      int EC, int disk, GnamHisto *histoname);

void DrawLayer(const int& x0, const int& y0,
	       const int& a, const int& b,
	       const int& nstaves, const int& nmodules,
	       GnamHisto *histoname);

#endif
