#ifndef __PixelHisto__
#define __PixelHisto__
//Christian. Why the hell are the includes in the .cxx file?! ask somebody about this later.
#include "is/infodictionary.h"
#include "is/inforeceiver.h"
#include "TRP/LuminosityInfo.h"
// /Christian

// GNAM
/*
  Define GNAM_DEBUG if you want to enable the GNAM_MESS_REP_DEB macro;
  otherwise, GNAM_MESS_REP_DEB will expand to nothing.
*/
// #define GNAM_DEBUG

// #define DEBUG // define DEBUG to dump fillHisto info messages: should be disabled at runtime for performance

#include <iostream>

using namespace std;


namespace PixLib{
  class ModuleConnectivity;
}
//---------------------------------------------
// Set if per module information is published
//---------------------------------------------
void publish_modules(bool publish_flag, int number);

//---------------------------------------------
// Set if per ROD information is published
//---------------------------------------------
// void publish_RODs (bool publish_flag, const char* ROD);

//---------------------------------------------
// book histograms hitmap/ToT/timing/errorflags for each module
// called by bookHisto
//---------------------------------------------
void book_module_histograms();

//---------------------------------------------
// book histograms hitmap and occupancy for each ROD
// called by bookHisto
//---------------------------------------------
// void book_ROD_histograms();

//---------------------------------------------
// book global and global statistics histograms
// called by bookHisto
//---------------------------------------------
void book_global_histograms();

//---------------------------------------------
// Fill a time based histogram sweeping over 60 minutes
// called by fillHisto
//---------------------------------------------
void fillTimeHisto(GnamHisto* histo, int& timeIntervals, int position, int value);

//---------------------------------------------
// decoding of string for AutoDisable
// called by prePublish, taken from AutoDisReport
//---------------------------------------------
std::string element(std::string str, int num, char sep);

//void fillDisabled(); // never defined -Joe

//Christian
// For IS run parameters
unsigned int run_number;
unsigned int luminosity_block;
IPCPartition* m_IPCPartition;
ISInfoDictionary* m_ISDict;
ISInfoReceiver* m_ISReceiver;

///Christian2
time_t RATE_LastTime;
// /Christian
static void callback_RunParams(ISCallbackInfo * isc);


#endif
