#ifndef PixelParameters_h
#define PixelParameters_h

//// Make parameters as clear as possible and let compiler optimize.

//// Legend: ////
// 3lp = 3-Layer Pixel system
// ibl = Insertable B-Layer
// FEC = Front End Chip (FE-I3 for 3lp, FE-I4 for Ibl)
// MOD = DAQ Module (16 FEC for 3lp, 1 double-chip planar or 2 3D chips for Ibl)
// OB = Opto Box = 1 PP0
// ROD = Read Out Driver
// ROS = Read Out System

//// 3-Layer Pixels Parameters ////
static const uint     PIX_PER_FEC_3lp = 2880; // PIX_PER_FE
static const uint     FEC_PER_MOD_3lp = 16; // FE_PER_MOD
static const uint MAX_MOD_PER_OB_3lp  = 7; // 6 or 7 MODs/OB
static const uint MAX_OB_PER_ROD_3lp  = 4; // Actual number is (1,2,4) for Layer (0,1,2), so max is 4
static const uint MAX_ROD_PER_ROS_3lp = 24;
static const uint MAX_ROS_NUMBER_3lp  = 9;
static const uint     MOD_NUMBER_3lp =  1744; // = 13*(22+38+52) + 6*8*6
static const uint PIX_PER_MOD_3lp = PIX_PER_FEC_3lp * FEC_PER_MOD_3lp;

//// IBL Parameters ////
static const uint     PIX_PER_FEC_ibl = 26880; // 336x80
static const uint     FEC_PER_MOD_ibl = 2; // 1 DAQ module is either 1 double-chip planar or 2 single-chip 3D
static const uint     MOD_PER_OB_ibl  = 8; // 1 stave ==> 1 OB
static const uint     OB_PER_ROD_ibl  = 1; // 1 OB ==> 1 ROD
static const uint MAX_ROD_PER_ROS_ibl = 6;
static const uint MAX_ROS_NUMBER_ibl  = 3;
static const uint     MOD_NUMBER_ibl  = 224; // = (16 modules/stave)*(14 staves)
static const uint PIX_PER_MOD_ibl = PIX_PER_FEC_ibl * FEC_PER_MOD_ibl;
static const uint PIX_PER_MOD_dbm = PIX_PER_FEC_ibl; // DBM has just one FE while IBL has two

//// Used for arrays and histograms ////
static const uint MAX_ROS_NUMBER  = MAX_ROS_NUMBER_3lp + MAX_ROS_NUMBER_ibl;
static const uint MAX_ROD_PER_ROS = MAX_ROD_PER_ROS_3lp; // 24 > 6
static const uint MAX_ROD_NUMBER  = MAX_ROS_NUMBER * MAX_ROD_PER_ROS;
static const uint MAX_OB_PER_ROD  = MAX_OB_PER_ROD_3lp; // 4 > 1
static const uint MAX_MOD_PER_OB  = MOD_PER_OB_ibl; // 7 < 8
static const uint MAX_MOD_PER_ROD = MAX_OB_PER_ROD * MAX_MOD_PER_OB; // use for histogram range

//// Used in loops ////
static const uint ROBIN_PER_ROS = 2;
static const uint ROL_PER_ROBIN = 12;

//// Decoding ////
static const uint32_t RodHeaderMarker = 0xee1234ee;
static const uint32_t PixelAnyId =      0x00100000;
// static const uint32_t PixelBarrelId =   0x00110000;
// static const uint32_t PixelForwardAId = 0x00120000;
// static const uint32_t PixelForwardBId = 0x00130000;
static const uint32_t PixelDbmId =   0x00150000;
static const uint32_t PixelIblId =   0x00140000;




//const int PIXEL_PER_FE = 2880;
//const int PIXEL_PER_MOD = PIXEL_PER_FE * FE_PER_MOD;

#endif
