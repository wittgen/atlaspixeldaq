enable_testing()

####################
#   rat & manitu   #
####################
add_test( NAME rat_empty_execution 
	  WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/installed/$ENV{CMTCONFIG}/bin
	  COMMAND ./rat)

set_tests_properties( rat_empty_execution PROPERTIES PASS_REGULAR_EXPRESSION "USAGE: rat")

add_test( NAME manitu_empty_execution 
	  WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/installed/$ENV{CMTCONFIG}/bin
	  COMMAND ./manitu)

set_tests_properties( manitu_empty_execution PROPERTIES PASS_REGULAR_EXPRESSION "USAGE: manitu")

####################
#       ccdb       #
####################
add_test( NAME ccdb_empty_execution 
	  WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/installed/$ENV{CMTCONFIG}/bin
	  COMMAND ./ccdb --help)

set_tests_properties( ccdb_empty_execution PROPERTIES PASS_REGULAR_EXPRESSION "ccdb - A test utility for CoralDB")

####################
#   CORAL stuff    #
####################
add_test( NAME CORAL-create_empty_execution 
	  WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/installed/$ENV{CMTCONFIG}/bin
	  COMMAND ./CORAL-create)

set_tests_properties( CORAL-create_empty_execution PROPERTIES PASS_REGULAR_EXPRESSION "CORAL-create starting")

add_test( NAME CORAL-fill_empty_execution 
	  WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/installed/$ENV{CMTCONFIG}/bin
	  COMMAND ./CORAL-fill)

set_tests_properties( CORAL-fill_empty_execution PROPERTIES PASS_REGULAR_EXPRESSION "CORAL-fill starting")

add_test( NAME CORAL-read_empty_execution 
	  WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/installed/$ENV{CMTCONFIG}/bin
	  COMMAND ./CORAL-read)

set_tests_properties( CORAL-read_empty_execution PROPERTIES PASS_REGULAR_EXPRESSION "CORAL-read starting")

####################
#   FFTVListener   #
####################
add_test( NAME FFTVListener_empty_execution 
	  WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/installed/$ENV{CMTCONFIG}/bin
	  COMMAND ./FFTVListener)

set_tests_properties( FFTVListener_empty_execution PROPERTIES PASS_REGULAR_EXPRESSION "Please provide TIM connectivity name")

####################
#  sendMrsMessage  #
####################
add_test( NAME sendMrsMessage_empty_execution 
	  WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/installed/$ENV{CMTCONFIG}/bin
	  COMMAND ./sendMrsMessage)

set_tests_properties( sendMrsMessage_empty_execution PROPERTIES PASS_REGULAR_EXPRESSION "USAGE: mrsMessage")

####################
#    setStatus     #
####################
add_test( NAME setStatus_empty_execution 
	  WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/installed/$ENV{CMTCONFIG}/bin
	  COMMAND ./setStatus)

set_tests_properties( setStatus_empty_execution PROPERTIES PASS_REGULAR_EXPRESSION "USAGE .*setStatus")

########################
# test_ConfigContainer #
########################
add_test( NAME test_ConfigContainer_empty_execution 
	  WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/installed/$ENV{CMTCONFIG}/bin
	  COMMAND ./test_ConfigContainer)

set_tests_properties( test_ConfigContainer_empty_execution PROPERTIES PASS_REGULAR_EXPRESSION "BEGININQUIRE test")

########################
#     test_Disbale     #
########################
add_test( NAME test_Disable_empty_execution 
	  WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/installed/$ENV{CMTCONFIG}/bin
	  COMMAND ./test_Disable)

set_tests_properties( test_Disable_empty_execution PROPERTIES PASS_REGULAR_EXPRESSION "Usage: test_Disable.cxx")

########################
# test_modConnectivity #
########################
add_test( NAME test_modConnectivity_empty_execution 
	  WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/installed/$ENV{CMTCONFIG}/bin
	  COMMAND ./test_modConnectivity)

set_tests_properties( test_modConnectivity_empty_execution PROPERTIES PASS_REGULAR_EXPRESSION "usage: .*test_modConnectivity")

########################
#    test_PixConfig    #
########################
add_test( NAME test_PixConfig_empty_execution 
	  WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/installed/$ENV{CMTCONFIG}/bin
	  COMMAND ./test_PixConfig)

set_tests_properties( test_PixConfig_empty_execution PROPERTIES PASS_REGULAR_EXPRESSION "BEGININQUIRE test")

########################
#  test_storeDisable   #
########################
add_test( NAME test_storeDisable_empty_execution 
	  WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/installed/$ENV{CMTCONFIG}/bin
	  COMMAND ./test_storeDisable)

set_tests_properties( test_storeDisable_empty_execution PROPERTIES PASS_REGULAR_EXPRESSION "USAGE .*test_storeDisable")

########################
#     UDPPublisher     #
########################
add_test( NAME test_UDPPublisher_empty_execution 
	  WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/installed/$ENV{CMTCONFIG}/bin
	  COMMAND ./UDPPublisher)

set_tests_properties( test_UDPPublisher_empty_execution PROPERTIES PASS_REGULAR_EXPRESSION "UDPPublisher: -p partition_name required.")

########################
#    UDPUartSpoofer    #
########################
add_test( NAME test_UDPUartSpoofer_empty_execution 
	  WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/installed/$ENV{CMTCONFIG}/bin
	  COMMAND ./UDPUartSpoofer)

set_tests_properties( test_UDPUartSpoofer_empty_execution PROPERTIES PASS_REGULAR_EXPRESSION "Usage: UDPUartSpoofer")

########################
#  PixLib Unit tests   #
########################
add_test( NAME test_TestCasesPixLib 
	  WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/installed/$ENV{CMTCONFIG}/bin
	  COMMAND ./TestCasesPixLib)
