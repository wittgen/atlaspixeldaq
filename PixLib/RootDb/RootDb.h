/////////////////////////////////////////////////////////////////////
// RootDb.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 23/06/06  Version 1.0 (GG)
//           Initial release
//! Implementation of abstract DB using root 

#ifndef _PIXLIB_ROOTDB
#define _PIXLIB_ROOTDB

#include "PixDbInterface/PixDbInterface.h"
#include "PixDbInterface/PixDbCompoundTag.h"
#include "PixDbInterface/PixDbTagStatus.h"
#include "PixDbInterface/PixDbAlias.h"
#include <string>
#include <map>
#include <mutex>
#include "TNamed.h"
#include "TFile.h"
#include "TArrayI.h"
#include "TArrayF.h"
#include "TArrayD.h"
#include "TCanvas.h"
#include "TMath.h"

using namespace PixLib;
using namespace PixLib::PixDb;

//Forward Declarations

namespace PixLib{
  class RootDbRecord;
  class RootDb;

  class RootDbField;
  class RootDbRecord;

  class RootDbFieldIterator{    
  public:
    int m_number; // which field number is connected this pointer
    RootDbField* m_field; // which field this iterator points to
    RootDbRecord* m_parentRecord; // which record this iterator refers
  public:
    // Accessors
    DbField* operator*() const;
    //not needed: DbField** operator->();
    int isNumber() const {return m_number;};
    // Modifiers
    RootDbFieldIterator& operator++();
    //not needed: RootDbFieldIterator operator++(int);
    friend bool operator==(const RootDbFieldIterator& left,const RootDbFieldIterator& right);
    friend bool operator!=(const RootDbFieldIterator& left,const RootDbFieldIterator& right);
   // void copyData(const RootDbFieldIterator fieldFromCopy);
    void pointsTo(RootDbField* newField); // make the iterator point to another field
    // constructors
    RootDbFieldIterator(int number, RootDbField* field, RootDbRecord* record) : m_number(number), m_field(field), m_parentRecord(record){}
    RootDbFieldIterator(): m_number(-1), m_field(0), m_parentRecord(0) {}
    RootDbFieldIterator(RootDbField* field) : m_number(-1), m_field(field), m_parentRecord(0) {}
    // destructor (delete the stored field!)
    ~RootDbFieldIterator();
  };

  class RootDbRecordIterator{
  public:
    int m_number; // which record number is connected this pointer
    RootDbRecord* m_record; // which record this iterator points to
    RootDbRecord* m_parentRecord; // which record this iterator has to ask for depending Records
  public:
    // Accessors
    DbRecord* operator*() const;
    //not needed: DbRecord** operator->();
    int isNumber() const {return m_number;}
    // Modifiers
    RootDbRecordIterator& operator++();
    //not needed: RootDbRecordIterator operator++(int);
    friend bool operator==(const RootDbRecordIterator& left,const RootDbRecordIterator& right);
    friend bool operator!=(const RootDbRecordIterator& left,const RootDbRecordIterator& right);
    void pointsTo(RootDbRecord* newRecord); // make the iterator point to another record
    // constructors
    RootDbRecordIterator(int number, RootDbRecord* record, RootDbRecord* parent) : m_number(number), m_record(record), m_parentRecord(parent){}
    RootDbRecordIterator(): m_number(-1), m_record(0), m_parentRecord(0) {}
    RootDbRecordIterator(RootDbRecord* record) : m_number(-1), m_record(record), m_parentRecord(0) {}
    //destructor (delete the stored record)
    ~RootDbRecordIterator();
  };

  bool operator==(const RootDbRecordIterator& left,const RootDbRecordIterator& right);
  bool operator!=(const RootDbRecordIterator& left,const RootDbRecordIterator& right);
  bool operator==(const RootDbFieldIterator& left,const RootDbFieldIterator& right);
  bool operator!=(const RootDbFieldIterator& left,const RootDbFieldIterator& right);

  class RootDbField : public TNamed, public DbField{
  private:
    static TCanvas* dumpCanvas; //! service canvas
    friend class RootDbRecord;
    friend class RootDb;

#ifndef __CINT__
    friend std::vector<DbRecord*> rootGroupModuleWizard(DbRecord* groupModuleRecord, float rootDbVersion, float turboDaqVerion, std::vector<std::string> turboDaqConfigFileNames, std::string plPath, bool tdaqName);
#endif
    string m_name; // the field name i.e. VCAL
    string m_decName; // the fully decorated name i.e. file:/dir/dir/dir/mydir(parentRecord)/VCAL;1
    TArrayI m_myIntCont;    // Integer content of the field
    TArrayF m_myFloatCont;  // float content of the field
    TArrayD m_myDoubleCont;  // float content of the field
    TString m_myStringCont; // string content of the field
    
    PixDbInterface* m_myDb; //! pointer to the database (not permanent)
    enum DbDataType m_dataType; // field data type (int, vector<int> etc. (see definition of enum DbDataType in PixDbInterface.h
    
    RootDbField* copyField(RootDbFieldIterator f); // copy a field from a field iterator 
    
    //    RootDbField(string name, int m_id); // private constructor to be used by RootDb TBtree find method 		
  public:
    RootDbField(const std::string &name);
    RootDbField() : m_myDb(0), m_dataType(DBEMPTY){}
    // Accessors
    virtual string getName() const {return m_name;} // read the field name from TNamed object
    virtual string getDecName() const {return m_decName;} // read the field decorated name
    virtual PixDbInterface* getDb() const {return m_myDb;} // get the DataBase of this field;
    virtual enum DbDataType getDataType() const {return m_dataType;} // read the data type
    virtual void dump(std::ostream& os) const; // Dump - debugging purpose

    // Virtual destructor
    virtual ~RootDbField(){/* static int dcount = 0; if(dcount%100 == 0) std::cout << "destructed dcount" << dcount++ << endl; else dcount++; */}
    // ROOT specific function overload
    void Dump(void) const; // *MENU* Dump to screen the content of this RootDbField
    //void ModifyFieldValue(char* newValue); // *MENU* modify the content of the Field
    ULong_t Hash() const;
    Bool_t  IsEqual(const TObject *obj) const;
    Bool_t  IsSortable() const;
    Int_t   Compare(const TObject *obj) const;
    // make ROOT know about RootDbField (note:this must be last line of the class definition
    ClassDef(RootDbField,1) 
};
  
class Histo;
 
class RootDbRecord : public TNamed, public DbRecord{
  friend class RootDbRecordIterator;
  friend class RootDbFieldIterator;
  friend class RootDb;
    
#ifndef __CINT__
  friend std::vector<DbRecord*> rootGroupModuleWizard(RootDbRecord* groupModuleRecord, float rootDbVersion, float turboDaqVerion, std::vector<std::string> turboDaqConfigFileNames, std::string plPath, bool tdaqName);
#endif
  
  string m_className; // the record name - the class object e.g. PixFE
  string m_name; // the record decorated name - e.g. PixFE_1
  string m_decName; // the record fully decorated name e.g. file:/dir/dir/dir/dir/M510902/PixFE_1/PixFE;1
  vector<string> m_myFieldNameList; // the list of names of the fields belonging to this record in format file:/dir/dir/dir/PixFE_1/name;1
  vector<string> m_myRecordNameList; // the list of names of the record belonging to this record in format file:/dir/dir/dir/PixFE_1/decName/name;
  vector<string> m_myRecordLogicalNameList; // the list of logical names - in the format decName
  vector<bool> m_myLink; // is a link the record?
  RootDb* m_myDb; //! pointer to the database (not permanent)

  map<string, RootDbField*> pushedFields; //! temporary memory cache, ordered by name and temporary stored data
  map<string, RootDbRecord*> pushedRecords; //! temporary memory cache, ordered by decname and temporary stored data

  //  RootDbRecord(string decname, int m_id);  // private constructor to be used by RootDb 
  
  void makeDecName(RootDbRecord* child, RootDbRecord* parent); // internal manipulator to link the child record to the parent



 public:
  // Constructors
  RootDbRecord() : m_className(""), m_myDb(0){};
  RootDbRecord(const std::string &className, const std::string &name);
  // Accessors
  virtual PixDbInterface* getDb() const; // get the DataBase of this record;
  virtual dbRecordIterator recordBegin(); // the iterator to the depending records		
  virtual dbFieldIterator fieldBegin(); // the iterator to the record data fields	
  virtual dbRecordIterator recordEnd(); // the end iterator to the depending records	
  virtual dbFieldIterator fieldEnd(); // the end iterator to the record data fields	
  virtual dbRecordIterator findRecord(const string& recordName); // find a subrecord by its name
  virtual dbFieldIterator findField(const string& fieldName); // find a field by its name
  virtual void dump(std::ostream& os) const; // Dump - debugging purpose
  virtual int getDependingRecordSize(void) const; // Get the amount of depending records
  virtual int getDependingFieldSize(void) const; // Get the amount of depending field
  virtual RootDbFieldIterator getField(int fieldNumber); // Get the pointer to the field number fieldNumber; throw an exception if fails
  virtual RootDbRecordIterator getRecord(int recordNumber); // Get the pointer to the record number recordNumber; throw an exception if fails

  virtual std::string getRecordName(int number); // get subrecord SLOT name
  virtual std::string getFieldDecName(int number); // get the field decname from the key number 
  virtual std::string getRecordDecName(int number); // get the record dec name from the key number
  virtual bool isLink(int number); // is the subrecord number a link - true or false
  // Modifiers
  virtual std::vector<std::string> &fieldNameList() { return m_myFieldNameList; };

  virtual string getClassName() const {return m_className;} // get the record name - the base class name of the object the record refers to
  virtual string getName() const {return m_name;} // get the fully decorated name
  virtual string getDecName() const {return m_decName;} // get the decorated name portion of this record
  virtual dbFieldIterator addField(const string& name); // add a field to the DbRecord fields and return the corresponding iterator
  virtual dbFieldIterator pushField(DbField* in); // add a field to the DbRecord fields and return the corresponding iterator
  virtual void eraseField(dbFieldIterator it); // erase a field.
  virtual dbRecordIterator linkRecord(DbRecord* in, const string& linkName, const string& dstSlotName); // link an existing record and return the corresponding iterator
  virtual dbRecordIterator updateLink(DbRecord* dstRec, const string& srcSlotName, const string& dstSlotName); // reset an existing soft link at srcSlotName to point to dstRec/dstSlotName
  virtual dbRecordIterator pushRecord(DbRecord* in, std::string sltName = ""); // add a record to the record list and return the corresponding iterator
  virtual void eraseRecord(dbRecordIterator it); // erase a record
  virtual DbRecord* addRecord(const string& name, const string& decName, const string& ID); // add an record
  // ROOT specific function (TBrowser pop-up menu)
  void Dump(void) const; // *MENU* Dump to screen the content of this RootDbRecord

  ULong_t Hash() const;
  Bool_t  IsEqual(const TObject *obj) const;
  Bool_t  IsSortable() const;
  Int_t   Compare(const TObject *obj) const;
  // Virtual destructor
  virtual ~RootDbRecord() {}
  ClassDef(RootDbRecord,1) 
};


class RootDb : public PixDbInterface {
  friend class RootDbRecord;
#ifndef __CINT__
  friend std::vector<DbRecord*> rootGroupModuleWizard(DbRecord* groupModuleRecord, float rootDbVersion, float turboDaqVerion, std::vector<std::string> turboDaqConfigFileNames, std::string plPath, bool tdaqName);
#endif
  
  TFile* m_theFile;
  string m_theFileName;
  string m_theOriginalFileName;
  std::map<std::string, TFile*> m_theFileHandlers;
  std::mutex m_mutex;
  
  DbRecord* processRecord(DbRecord* currRecord);
  bool commitRootRecordReplaceTree(RootDbRecord* currRecord); // recursively write to disk
  bool putRecord(RootDbRecord* recordToPut); // write a record into disk without taking it out from cache memory - record to put is valid after calling
  
  bool putField(RootDbField* fieldToPut, RootDbRecord* recordWherePut);
  RootDbRecord* getRecordByDecName(string name); // read from disk and add into cache memory
  RootDbField* getFieldByDecName(string name); // read from disk and add into cache memory
  void RootCDCreate(TDirectory* startingDir, RootDbRecord* record); // makes the directory tree pointed by record - propedeutic to any writing to the disk
  TFile* getFileHandler(std::string fileName);

 public:
  // constructors
  RootDb(string namefile, string mode = "READ", string tdaqnamefile = ""); // construct a rootdb database from the file name
  // Factory
  virtual DbField* makeField(const string& name);
  virtual DbRecord* makeRecord(const string& name, const string& decName);
  // Accessors
  virtual DbRecord* readRootRecord(); // get the root record 

  
  // Methods to work with tags required by PixDbInterface.
  virtual PixDbCompoundTag compoundTag() { return PixDbCompoundTag(); }
  
  virtual void getExistingObjectDictionaryTags(vector<PixDbTagStatus>& output) const { output.clear(); }
  virtual void getExistingConnectivityTags(vector<PixDbTagStatus>& output, const string&) const { output.clear(); }
  virtual void getExistingDataTags(vector<PixDbTagStatus>& output, const string&) const { output.clear(); }
  virtual void getExistingAliasTags(vector<PixDbTagStatus>& output, const string&) const { output.clear(); }
  


  virtual void copyConnectivityTagAndSwitch(const string& /* newtag */) {}
  virtual void copyDataTagAndSwitch(const string& /* newtag */) {}
  virtual void copyAliasTagAndSwitch(const string& /* newtag */) {}

  // These methods ignore any cached values and read the database on each call.


  // Transaction control

  virtual void transactionStart() {}
  virtual void transactionCommit() {}


  // Methods dealing with aliases for object IDs

  virtual vector<PixDbAlias> findAliases(const string& /* id */) const { return vector<PixDbAlias>(); }
  virtual string findAlias(const string& /* id */, const string& /* convention */) const { return ""; }



  virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, bool&); // read or commit the field pointed by the iterator
  virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, vector<bool>&);
  virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, int&);
  virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, vector<int>&);
  virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, unsigned int &);
  virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, float&);
  virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, vector<float>&);
  virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, double&);
  virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, vector<double>&);
  virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, Histo&);
  virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, string&);
  virtual DbRecord* DbProcess(DbRecord* therecord, enum DbAccessType mode); // read or commit (possibly recursively) the record pointed by iterator
  virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode);

  // DbProcess(abstract_iterator,..) are now generically defined via DbProcess(pointer,...) in PixDbInterface.
  // Here we keep the old code using concrete RootDb iterators
  virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, bool&); // read or commit the field pointed by the iterator
  virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, vector<bool>&);
  virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, int&);
  virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, vector<int>&);
  virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, unsigned int &);
  virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, float&);
  virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, vector<float>&);
  virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, double&);
  virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, vector<double>&);
  virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, Histo&);
  virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, string&);
  virtual dbRecordIterator DbProcess(dbRecordIterator therecord, enum DbAccessType mode); // read or commit (possibly recursively) the record pointed by iterator
  virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode);

  
  dbFieldIterator DbProcess(RootDbFieldIterator theField, enum DbAccessType mode, bool&); // read or commit the field pointed by the iterator
  dbFieldIterator DbProcess(RootDbFieldIterator theField, enum DbAccessType mode, vector<bool>&);
  dbFieldIterator DbProcess(RootDbFieldIterator theField, enum DbAccessType mode, int&);
  dbFieldIterator DbProcess(RootDbFieldIterator theField, enum DbAccessType mode, vector<int>&);
  dbFieldIterator DbProcess(RootDbFieldIterator theField, enum DbAccessType mode, unsigned int &);
  dbFieldIterator DbProcess(RootDbFieldIterator theField, enum DbAccessType mode, float&);
  dbFieldIterator DbProcess(RootDbFieldIterator theField, enum DbAccessType mode, vector<float>&);
  dbFieldIterator DbProcess(RootDbFieldIterator theField, enum DbAccessType mode, double&);
  dbFieldIterator DbProcess(RootDbFieldIterator theField, enum DbAccessType mode, vector<double>&);
  dbFieldIterator DbProcess(RootDbFieldIterator theField, enum DbAccessType mode, Histo&);
  dbFieldIterator DbProcess(RootDbFieldIterator theField, enum DbAccessType mode, string&);
  dbRecordIterator DbProcess(RootDbRecordIterator therecord, enum DbAccessType mode); // read or commit (possibly recursively) the record pointed by iterator
  dbFieldIterator DbProcess(RootDbFieldIterator theField, enum DbAccessType mode);

  virtual DbRecord* DbFindRecordByName(const string& name); // find a record by its name
  virtual DbField* DbFindFieldByName(const string& name); // find a data field by its name
  // Virtual destructor
  virtual ~RootDb();
  ClassDef(RootDb,1) 
};

} // namespace PixLib

#endif
