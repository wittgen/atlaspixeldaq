#include "RootDb/RootDb.h"
#include "Histo/Histo.h"
#include <sstream>

#include "TKey.h"
#include "TH1.h"
#include "TH2.h"
#include "TROOT.h"
#include <limits.h>
#include <libgen.h>
#include <DFThreads/DFMutex.h>
using std::stringstream;
using namespace PixLib;
typedef unsigned int uint;

ClassImp(RootDbField)
ClassImp(RootDbRecord)
ClassImp(RootDb)

using namespace PixLib;

namespace PixLib {
  //================================================================
  // private class implementing RootDb specific record iterator
  class RecordIteratorWrapper : virtual public IRecordIterator {
  public:
    RootDbRecordIterator m_iter;
    RecordIteratorWrapper(const RootDbRecordIterator& i) : m_iter(i) {}

    // ----------------------------------------------------------------
    // Basic iterator functionality
    virtual DbRecord* operator->() { return m_iter.operator*(); }
      
    // prefix increment
    virtual IIterator<DbRecord>& operator++() { ++m_iter; return *this; }

    virtual bool operator==(const IIterator<DbRecord>& rhs) const;

    // ----------------------------------------------------------------
    // The connectivity part...
    virtual DbRecord* srcObject() const { if(m_iter.m_parentRecord != 0) return m_iter.m_parentRecord; else throw std::runtime_error("RecordIteratorWrapper::srcObject(): RootDbRecordIterator stored do not have a valid pointer to the parent record. Load it with a call to DbProcess before accessing data");}
    virtual string srcSlotName() const { if(m_iter.m_parentRecord != 0) return m_iter.m_parentRecord->getRecordName(m_iter.m_number); else throw std::runtime_error("RecordIteratorWrapper::srcSlotName(): RootDbRecordIterator stored do not have a valid pointer to the parent record. Load it with a call to DbProcess before accessing data");}
    virtual DbRecord* dstObject() const { if(m_iter.m_record != 0) return m_iter.m_record; else throw std::runtime_error("RecordIteratorWrapper::dstObject(): RootDbRecordIterator stored do not have a valid pointer to the pointed record. Load it with a call to DbProcess before accessing data"); }
    virtual string dstSlotName() const { throw std::runtime_error("RecordIteratorWrapper::dstSlotName(): not implemented"); }
    virtual bool isLink() const { if(m_iter.m_parentRecord != 0) return m_iter.m_parentRecord->isLink(m_iter.m_number); else throw std::runtime_error("RecordIteratorWrapper::isLink(): RootDbRecordIterator stored do not have a valid pointer to the parent record. Load it with a call to DbProcess before accessing data");}
      
    // ----------------------------------------------------------------
    // IRecordIterator requirement
    virtual IRecordIterator* clone() const { return new RecordIteratorWrapper(*this); }
  };

  bool RecordIteratorWrapper::operator==(const IIterator<DbRecord>& rhs) const { 


    const RecordIteratorWrapper* right = dynamic_cast<const RecordIteratorWrapper*>(&rhs);


    return right ? (m_iter == right->m_iter) : false;
  }




  //================================================================
  // private class implementing RootDb specific field iterator
  class FieldIteratorWrapper : virtual public IFieldIterator {
  public: 
    RootDbFieldIterator m_iter;

    FieldIteratorWrapper(const RootDbFieldIterator& i) : m_iter(i) {}
      
    virtual DbField* operator->() { return m_iter.operator*(); }

    // prefix increment
    virtual IIterator<DbField>& operator++() { ++m_iter; return *this; }

    virtual bool operator==(const IIterator<DbField>& rhs) const;
      
    virtual IFieldIterator* clone() const { return new FieldIteratorWrapper(*this); }
    
    virtual void releaseMem(){
      if(m_iter.m_field) {
	delete m_iter.m_field;
	m_iter.m_field = 0;
      }
    }
    
  };

  bool FieldIteratorWrapper::operator==(const IIterator<DbField>& rhs) const { 
    //    cerr<<"operator==, &rhs = "<<(&rhs)
    //	<<", typename(rhs) =  "<< typeid(&rhs).name()<<std::endl;

    const FieldIteratorWrapper* right = dynamic_cast<const FieldIteratorWrapper*>(&rhs);
    return right ? (m_iter == right->m_iter) : false;
  }

}




DbField* RootDbFieldIterator::operator*() const {
    return m_field;
}

//not needed:DbField** RootDbFieldIterator::operator->() {
//not needed:  return &m_field;
//not needed:}

//not needed: RootDbFieldIterator RootDbFieldIterator::operator++(int i){
//not needed: //  DbRecord* record = m_field->getParentRecord();
//not needed:   RootDbRecord* record = m_parentRecord;
//not needed:   if((m_number + 1) > record->getDependingFieldSize()){
//not needed:     std::stringstream a;
//not needed:     a << "RootDbFieldIterator::operator++() : field number " << (m_number+1) << " not existing in record: ";
//not needed:     a << m_parentRecord;
//not needed:     //return 0;
//not needed:     throw PixDBException(a.str().c_str());    
//not needed:   }
//not needed:   else if((m_number + 1) == record->getDependingFieldSize()){
//not needed:     *this = record->fieldEnd();
//not needed:     return *this;
//not needed:   }
//not needed:   else{
//not needed:     m_number = record->getField(m_number+1).m_number;
//not needed:     m_field = record->getField(m_number).m_field;
//not needed:     return *this;
//not needed:   }
//not needed: }

RootDbFieldIterator& RootDbFieldIterator::operator++(){
//  DbRecord* record = m_field->getParentRecord();
  RootDbRecord* record = m_parentRecord;
  if((m_number + 1) > record->getDependingFieldSize()){
    std::stringstream a;
    a << "RootDbFieldIterator::operator++() : field number " << (m_number+1) << " not existing in record: ";
    a << m_parentRecord;
    //return 0;
    throw PixDBException(a.str().c_str());    
  }
  else if((m_number +1) == record->getDependingFieldSize()){
    *this = RootDbFieldIterator(record->m_myFieldNameList.size(),NULL,record);
    return *this;
  } 
  else{
    m_number = record->getField(m_number+1).m_number;
    m_field = record->getField(m_number).m_field;
    return *this;
  }
}

bool PixLib::operator==(const RootDbFieldIterator& left,const RootDbFieldIterator& right){
  if((left.m_parentRecord == right.m_parentRecord) &&
     (left.m_number == right.m_number))
    return true;
  return false;
}

bool PixLib::operator!=(const RootDbFieldIterator& left,const RootDbFieldIterator& right){
  if(left == right) return false;
  else return true;
}

void RootDbFieldIterator::pointsTo(RootDbField* newField){ // make the iterator point to another field
	this->m_field = newField;
}

RootDbFieldIterator::~RootDbFieldIterator(){ /* if(m_field) delete m_field; */}

DbRecord* RootDbRecordIterator::operator*() const {
  return m_record;
}

//not needed:DbRecord** RootDbRecordIterator::operator->() {
//not needed:  return &m_record;
//not needed:}

//not needed: RootDbRecordIterator RootDbRecordIterator::operator++(int i){
//not needed:   if((m_number + 1) > m_parentRecord->getDependingRecordSize()){
//not needed:     std::stringstream a;
//not needed:     a << "RootDbRecordIterator::operator++() : record number " << (m_number+1) << " not existing in record: ";
//not needed:     a << m_parentRecord;
//not needed:     //return 0;
//not needed:     throw PixDBException(a.str().c_str());    
//not needed:   }
//not needed:   else if((m_number +1) == m_parentRecord->getDependingRecordSize()){
//not needed:     *this = m_parentRecord->recordEnd();
//not needed:     return *this;
//not needed:   }
//not needed:   else{
//not needed:     m_number = m_parentRecord->getRecord(m_number+1).m_number;
//not needed:     m_record = m_parentRecord->getRecord(m_number).m_record;
//not needed:     return *this;
//not needed:   }
//not needed: }

RootDbRecordIterator& RootDbRecordIterator::operator++(){
  if((m_number + 1) > m_parentRecord->getDependingRecordSize()){
    std::stringstream a;
    a << "RootDbRecordIterator::operator++() : record number " << (m_number+1) << " not existing in record: ";
    a << m_parentRecord;
    //return 0;
    throw PixDBException(a.str().c_str());    
  }
  else if((m_number+1) == m_parentRecord->getDependingRecordSize()){
    *this = RootDbRecordIterator(m_parentRecord->m_myRecordNameList.size(),NULL,m_parentRecord);
    return *this;
  }
  else{
    m_number = m_parentRecord->getRecord(m_number+1).m_number;
    m_record = m_parentRecord->getRecord(m_number).m_record;
    return *this;
  }
}

bool PixLib::operator==(const RootDbRecordIterator& left,const RootDbRecordIterator& right){
  if((left.m_parentRecord == right.m_parentRecord) &&
     (left.m_number == right.m_number))
    return true;
  return false;
}

bool PixLib::operator!=(const RootDbRecordIterator& left,const RootDbRecordIterator& right){
  if(left == right) return false;
  else return true;
}

void RootDbRecordIterator::pointsTo(RootDbRecord* newRecord){ // make the iterator point to another record
	this->m_record = newRecord;
}

#if 0
void RootDbFieldIterator::copyData(const RootDbFieldIterator fFromCopy){
  dbFieldIterator fieldFromCopy(new FieldIteratorWrapper(fFromCopy));
  int i_copyvalue = 0;
  float f_copyvalue = 0;
  double d_copyvalue = 0;
  bool b_copyvalue = false;
  std::vector<int> vi_copyvalue;
  std::vector<float> vf_copyvalue;
  std::vector<double> vd_copyvalue;
  std::vector<bool> vb_copyvalue;
  std::string s_copyvalue;
  Histo h_copyvalue;
  stringstream a;
  switch(this->m_field->getDataType()){
  case DBINT:
    (*fieldFromCopy)->getDb()->DbProcess(fieldFromCopy,DBREAD,i_copyvalue);
    this->m_field->getDb()->DbProcess(new FieldIteratorWrapper((*(const_cast<RootDbFieldIterator*>(this)))),DBCOMMIT,i_copyvalue);
    break;
  case DBFLOAT:
    (*fieldFromCopy)->getDb()->DbProcess(fieldFromCopy,DBREAD,f_copyvalue);
    this->m_field->getDb()->DbProcess(new FieldIteratorWrapper((*(const_cast<RootDbFieldIterator*>(this)))),DBCOMMIT,f_copyvalue);
    break;
  case DBDOUBLE:
    (*fieldFromCopy)->getDb()->DbProcess(fieldFromCopy,DBREAD,d_copyvalue);
    this->m_field->getDb()->DbProcess(new FieldIteratorWrapper((*(const_cast<RootDbFieldIterator*>(this)))),DBCOMMIT,d_copyvalue);
    break;
  case DBSTRING:
    (*fieldFromCopy)->getDb()->DbProcess(fieldFromCopy,DBREAD,s_copyvalue);
    this->m_field->getDb()->DbProcess(new FieldIteratorWrapper((*(const_cast<RootDbFieldIterator*>(this)))),DBCOMMIT,s_copyvalue);
    break;
  case DBBOOL:
    (*fieldFromCopy)->getDb()->DbProcess(fieldFromCopy,DBREAD,b_copyvalue);
    this->m_field->getDb()->DbProcess(new FieldIteratorWrapper((*(const_cast<RootDbFieldIterator*>(this)))),DBCOMMIT,b_copyvalue);
    break;
  case DBVECTORINT:
    (*fieldFromCopy)->getDb()->DbProcess(fieldFromCopy,DBREAD,vi_copyvalue);
    this->m_field->getDb()->DbProcess(new FieldIteratorWrapper((*(const_cast<RootDbFieldIterator*>(this)))),DBCOMMIT,vi_copyvalue);
    break;
  case DBVECTORFLOAT:
    (*fieldFromCopy)->getDb()->DbProcess(fieldFromCopy,DBREAD,vf_copyvalue);
    this->m_field->getDb()->DbProcess(new FieldIteratorWrapper((*(const_cast<RootDbFieldIterator*>(this)))),DBCOMMIT,vf_copyvalue);
    break;
  case DBVECTORBOOL:
    (*fieldFromCopy)->getDb()->DbProcess(fieldFromCopy,DBREAD,vb_copyvalue);
    this->m_field->getDb()->DbProcess(new FieldIteratorWrapper((*(const_cast<RootDbFieldIterator*>(this)))),DBCOMMIT,vb_copyvalue);
    break;
  case DBVECTORDOUBLE:
    (*fieldFromCopy)->getDb()->DbProcess(fieldFromCopy,DBREAD,vd_copyvalue);
    this->m_field->getDb()->DbProcess(new FieldIteratorWrapper((*(const_cast<RootDbFieldIterator*>(this)))),DBCOMMIT,vd_copyvalue);
    break;
  case DBHISTO:
    (*fieldFromCopy)->getDb()->DbProcess(fieldFromCopy,DBREAD,h_copyvalue);
    this->m_field->getDb()->DbProcess(new FieldIteratorWrapper((*(const_cast<RootDbFieldIterator*>(this)))),DBCOMMIT,h_copyvalue);
    break;
  case DBEMPTY: default:
    a << "RootDbFieldIterator::copyData() : copying empty or not supported data type ";
    a << DbDataTypeNames[this->m_field->getDataType()];
    throw PixDBException(a.str().c_str());    
	}	
}
#endif
RootDbRecordIterator::~RootDbRecordIterator(){/* if(m_record) delete m_record; */}


//================================================================
// And here are the wrappers



  //################################################################

namespace{

  std::vector<std::string> pathItemize(std::string& inputString){
    // utility function to itemize a root path - used to create the directory structure in the disk if it is not there
    // a path like file:/dir/dir/dir/dir/dir is itemized in a vector containing the "dir"
    
    std::vector<std::string> retval;
    int runnpos = 0;
    runnpos = (int)inputString.find_first_of(':'); // skip the long file name 
    if(runnpos == (int)std::string::npos) runnpos = 0; // in the case there are not ':', this returns to the previous situation
    else inputString.erase(0,runnpos+1);
    runnpos = (int)inputString.find_first_of('/');
    if(runnpos == (int)std::string::npos) return retval; 
    runnpos += 1;
    while(inputString.find_first_of('/',runnpos) != std::string::npos){
      std::string item = inputString.substr(runnpos,inputString.find_first_of('/',runnpos+1)-runnpos);
      retval.push_back(item);
      runnpos = inputString.find_first_of('/',runnpos) + 1;
    }
    if(runnpos != (int)inputString.size()){
      retval.push_back(inputString.substr(runnpos, inputString.size()));
    }
    return retval;
  }

#if 0
  string getAbsolutePath(TDirectory* currDir){ // from a root directory file:/dir/dir/dir to a string /dir/dir/dir
    string path = currDir->GetPath();
    int colonPos = path.find(":");
    path.erase(0,colonPos+1);
    return path;
  }
#endif

  string getFileName(string in){
    string retval = in;
    std::size_t colonPos = retval.find(":");
    if (colonPos != std::string::npos) {
      retval.erase(colonPos, retval.size());
      return retval;
    }
    return "";
  }

  string removeFileName(string in){
    string retval = in;
    std::size_t colonPos = retval.find(":");
    if (colonPos != std::string::npos) {
      retval.erase(0, colonPos+1);
      return retval;
    }
    return in;
  }

  static string purgeSlash(string in){
    // from file:/dir/dir/dir/dir/dir/ to file:/dir/dir/dir/dir/dir
    // from / to nothing
    string retval(in);
    if(retval.size() != 0){
      if(in.at(in.size()-1) == '/'){
	retval =  in.substr(0,in.size()-1);
      }
    }
    return retval;
  }

#if 0
  string getMyVersionName(string name){
    // from file:/dir/dir/dir/dir/name;1 to name;1
    int pos = (int)name.find_last_of("/");
    return name.substr(pos+1);
  }
#endif

  string getMyDirectoryName(string name){
    // from file:/dir/dir/dir/dir/dir/name;1 to file:/dir/dir/dir/dir/dir
    int pos = (int)name.find_last_of("/");
    return name.substr(0,pos);
  }

  string getMyIDName(string name){
    // from file:/dir/dir/dir/dir/subdir/name;1 to subdir
    string retval;
    int pos = name.find_last_of("/");
    int pos2 = pos - 1;
    pos--;
    pos = name.find_last_of("/",pos);
    retval = name.substr(pos+1, pos2-pos);
    return retval;
  }

  string getSubRecordNameFromList(string listItem){
    // from file:/dir/dir/dir/mydir/subdir/name to subdir/name
    // from file:/subdir/name to subdir/name
    string retval;
    int pos = listItem.find_last_of("/");
    pos--;
    pos = listItem.find_last_of("/",pos);
    retval = listItem.substr(pos+1, listItem.size());
    return retval;
  }

  string getFieldNameFromList(string listItem){
    // from file:/dir/dir/dir/mydir/subdir/name to name
    // from file:/subdir/name to name
    string retval;
    int pos = listItem.find_last_of("/");
    if(pos != (int)std::string::npos){
      retval = listItem.substr(pos+1, listItem.size());
    }
    return retval;
  }

  string stripIteration(string name){
    // from file:/dir/dir/dir/dir/name;1 to file:/dir/dir/dir/dir/name
    string retval = name;
    int resize = (int)retval.find_last_of(";");
    if(resize != (int)std::string::npos)
      retval.resize(resize);
    return retval;
  }
#if 0    
  string addFileToName(std::string toBeAdded, std::string fileName){
    // from dir/dir/dir to filename:/dir/dir/dir
    // from file:/dir/dir/dir to fileName:/dir/dir/dir
    int retval = (int)toBeAdded.find(":");
    if(retval == (int)std::string::npos){
      string tmp = fileName;
      tmp += ":";
      tmp += toBeAdded;
      return tmp;
    }
    else{
      toBeAdded.erase(0,retval+1);
      string tmp = fileName;
      tmp += ":";
      tmp += toBeAdded;
      return tmp;    
    }
  }
#endif

  std::vector<double> histoToVector(Histo& his){
    std::vector<double> retval;
    uint i = 0;
    int ii = 0;
    retval.push_back(0.987654321); // code for identify an histogram

    retval.push_back(his.name().size());
    for(i = 0; i < his.name().size(); i++){
      retval.push_back(his.name().at(i));
    }
    retval.push_back(his.title().size());
    for(i = 0; i < his.title().size(); i++){
      retval.push_back(his.title().at(i));
    }
    retval.push_back(his.nDim());
    if(his.nDim() == 1){
      retval.push_back(his.nBin(0));
      retval.push_back(his.max(0));
      retval.push_back(his.min(0));
      for(ii = 0; ii < his.nBin(0); ii++){
	retval.push_back(his(ii));
      }
    }
    else if(his.nDim() == 2){
      retval.push_back(his.nBin(0));
      retval.push_back(his.max(0));
      retval.push_back(his.min(0));
      retval.push_back(his.nBin(1));
      retval.push_back(his.max(1));
      retval.push_back(his.min(1));
      int j = 0;
      for(ii = 0; ii < his.nBin(0); ii++){
	for(j = 0; j < his.nBin(1); j++){
	  retval.push_back(his(ii,j));
	}
      }
    }
    else{
      stringstream a;
      a << "histoToVector error: nDim " << his.nDim() << "not allowed";
      throw PixDBException(a.str());
    }

    return retval;
  }

  Histo vectorToHisto(std::vector<double>& vec){
    if(vec.size() == 0){
      stringstream a;
      a << "histoToVector error: vector size is zero" ; 
      throw PixDBException(a.str());
    }
    if(vec[0] != 0.987654321){
      stringstream a;
      a << "vectorToHisto error: vec[0] is " << vec[0] << "not 0.987654321, vector is not an histo ";
      throw PixDBException(a.str());
    }

    std::string hisName;
    hisName.resize(static_cast<int>(vec[1]));
    uint i;
    for(i = 0; i < hisName.size(); i++){
      hisName.at(i) = static_cast<int>(vec[2+i]);
    }

    std::string hisTitle;
    hisTitle.resize(static_cast<int>(vec[2+hisName.size()]));

    for(i = 0; i < hisTitle.size(); i++){
      hisTitle.at(i) = static_cast<int>(vec[2+hisName.size()+1+i]);
    }
    int hisnDim = static_cast<int>(vec[2+hisName.size()+hisTitle.size()+1]);
    double hisnBin[2];
    double hisMax[2];
    double hisMin[2];

    std::vector<std::vector<double> > his;

    if(hisnDim == 1){
      hisnBin[0] = vec[2+hisName.size()+hisTitle.size()+1+1];
      hisMax[0] = vec[2+hisName.size()+hisTitle.size()+1+2];
      hisMin[0] = vec[2+hisName.size()+hisTitle.size()+1+3];
      std::vector<double> hisRow;
      for(i = 0; i < hisnBin[0]; i++){
	hisRow.push_back(vec[2+hisName.size()+hisTitle.size()+5+i]);
      }
      his.push_back(hisRow);
    }
    else if(hisnDim == 2){
      hisnBin[0] = vec[2+hisName.size()+hisTitle.size()+1+1];
      hisMax[0] = vec[2+hisName.size()+hisTitle.size()+1+2];
      hisMin[0] = vec[2+hisName.size()+hisTitle.size()+1+3];
      hisnBin[1] = vec[2+hisName.size()+hisTitle.size()+1+4];
      hisMax[1] = vec[2+hisName.size()+hisTitle.size()+1+5];
      hisMin[1] = vec[2+hisName.size()+hisTitle.size()+1+6];
      uint j = 0;
      for(i = 0; i < hisnBin[0]; i++){
	std::vector<double> hisRow;
	for(j = 0; j < hisnBin[1]; j++){
	  hisRow.push_back(vec[2+hisName.size()+hisTitle.size()+1+6+1+j+i*(int)hisnBin[1]]);
	}
	his.push_back(hisRow);
      }
    }
    else{
      stringstream a;
      a << "vectorToHisto error: nDim is " << hisnDim << " value not allowed";
      throw PixDBException(a.str());
    }

    if(hisnDim == 1){
      Histo retval(hisName,hisTitle,(int)hisnBin[0],hisMin[0],hisMax[0]);
      for(i = 0; i < hisnBin[0]; i++){
	retval.set(i,his[0][i]);
      }
      return retval;
    }
    else{
      Histo retval(hisName,hisTitle,(int)hisnBin[0],hisMin[0],hisMax[0],(int)hisnBin[1],hisMin[1],hisMax[1]);
      for(i = 0; i < hisnBin[0]; i++){
	for(uint j = 0; j < hisnBin[1]; j++){
	  retval.set(i,j,his[i][j]);
	}
      }
      return retval;
    }
  }

  int vectorHistoDim(vector<double>& vec){ // returns 0 if vec is not an Histo, 1 or 2 as Histo dimension
    if(vec.size() == 0 || vec[0] != 0.987654321){
      return 0;
    }	
    return static_cast<int>(vec[(int)(2+vec[1]+ vec[2+(int)vec[1]]+1)]);
  }


}


TCanvas* RootDbField::dumpCanvas = 0;


RootDbField* RootDbField::copyField(RootDbFieldIterator f){

  // copy the content of a field pointed by f to the current field. If the field already does have a content, erase it.
  // the name (e.g. VCAL) and the decname (e.g. file:/dir/mydir(parentRecord)/VCAL;1) of the field is copied too. 
  dbFieldIterator tmpf;
  DbField* ft;
  bool allocated = false;

  if(!f.m_field){
    tmpf = dynamic_cast<RootDb*>(this->getDb())->DbProcess(f, PixDb::DBREAD);
    ft = *tmpf;
    allocated = true;
  }
  else ft = *f;
  
  this->m_name = ft->getName();
  this->m_decName = ft->getDecName();
  
  if(m_dataType != DBEMPTY){
    m_myIntCont.Set(1);
    m_myFloatCont.Set(1);
    m_myDoubleCont.Set(1);
    m_myStringCont = "";
    m_dataType = DBEMPTY;
  }
  m_dataType = ft->getDataType();

  RootDbFieldIterator fiter(this);
  switch(m_dataType){
  case DBINT:
    {int valInt;
    this->getDb()->DbProcess(*f,PixDb::DBREAD,valInt);
    m_myIntCont.Set(1);
    m_myIntCont.AddAt(valInt,0);
    break;}
  case DBVECTORINT:
    {vector<int> valVInt;
    this->getDb()->DbProcess(*f,PixDb::DBREAD,valVInt);
    m_myIntCont.Set(valVInt.size());
    for(size_t i = 0; i < valVInt.size(); i++){
      m_myIntCont.AddAt(valVInt[i],i);      
    }}
    break;
  case DBULINT:
    {unsigned int valUInt;
    this->getDb()->DbProcess(*f,PixDb::DBREAD,valUInt);
	stringstream s;
	s << valUInt;
    m_myStringCont = s.str().c_str();
    break;}
  case DBFLOAT:
    {float valFloat;
    this->getDb()->DbProcess(*f,PixDb::DBREAD,valFloat);
    m_myFloatCont.Set(1);
    m_myFloatCont.AddAt(valFloat,0);
    break;}
  case DBVECTORFLOAT:
    {vector<float> valVFloat;
    this->getDb()->DbProcess(*f,PixDb::DBREAD,valVFloat);
    m_myFloatCont.Set(valVFloat.size());
    for(size_t i = 0; i < valVFloat.size(); i++){
      m_myFloatCont.AddAt(valVFloat[i],i);      
    }}
    break;
  case DBDOUBLE:
    {double valDouble;
    this->getDb()->DbProcess(*f,PixDb::DBREAD,valDouble);
    m_myDoubleCont.Set(1);
    m_myDoubleCont.AddAt(valDouble,0);
    break;}
  case DBVECTORDOUBLE:
    {vector<double> valVDouble;
    this->getDb()->DbProcess(*f,PixDb::DBREAD,valVDouble);
    m_myDoubleCont.Set(valVDouble.size());
    for(size_t i = 0; i < valVDouble.size(); i++){
      m_myDoubleCont.AddAt(valVDouble[i],i);      
    }}
    break;
  case DBBOOL:
    {bool valBool;
    this->getDb()->DbProcess(*f,PixDb::DBREAD,valBool);
    m_myIntCont.Set(1);
    m_myIntCont.AddAt(valBool,0);
    break;}
  case DBVECTORBOOL:
    {vector<bool> valVBool;
    this->getDb()->DbProcess(*f,PixDb::DBREAD,valVBool);
    m_myIntCont.Set(valVBool.size());
    for(size_t i = 0; i < valVBool.size(); i++){
      m_myIntCont.AddAt(valVBool[i],i);      
    }}
    break;
  case DBHISTO:
    {Histo valHisto;
    this->getDb()->DbProcess(*f,PixDb::DBREAD,valHisto);
    this->getDb()->DbProcess(dbFieldIterator(new FieldIteratorWrapper(fiter)),DBCOMMIT,valHisto);
    break;}
  case DBSTRING:
    {string valString;
    this->getDb()->DbProcess(*f,PixDb::DBREAD,valString);
    m_myStringCont = valString.c_str();
    }
    break;
  default:
    {
      //       string error;
      //       error += "RootDbField::copyField error: type not processed. Type is ";
      //       error += DbDataTypeNames[m_dataType];
      //       throw PixDBException(error);
    }
  }
  if(allocated) tmpf.releaseMem();
  return this;
}

// RootDbField::RootDbField(string decname, int id) : m_decName(decname), m_dataType(DBEMPTY){
//   this->m_myFloatCont.Set(1);
//   this->m_myDoubleCont.Set(1);
//   this->m_myIntCont.Set(1);
// }

RootDbField::RootDbField(const std::string &name) : m_name(name), m_dataType(DBEMPTY){
  this->m_myDoubleCont.Set(1);
  this->m_myFloatCont.Set(1);
  this->m_myIntCont.Set(1);
} 

void RootDbField::dump(std::ostream& os) const{
  
  os << "Dumping RootDbField named: " << m_name << std::endl;
  os << "RootDbField decName: " << m_decName << std::endl;
  os << "Data type: " <<  DbDataTypeNames[m_dataType] << std::endl;
  os << "Data content: ";
  switch (m_dataType){
  case DBBOOL:
    if(m_myIntCont.At(0) == 0) os << "false" << std::endl; else os << "true" << std::endl;
    break;
  case DBVECTORBOOL:
    int count;
    for(count = 0; count < 5; count++){
      os << "" << count << '\t';
    }
    os << std::endl;
    for(count = 0; count < m_myIntCont.GetSize(); count++){
      if(!(count%5) || count == 0) os << count << '\t'; else os << '\t';
      if(m_myIntCont.At(count) == 0) os << "false" << '\t'; else os << "true" << '\t';
      if(!((count+1)%5)) os << std::endl;
    }
    os << std::endl;
    break;
  case DBINT:
    os << m_myIntCont.At(0) << std::endl;
    break;
  case DBULINT:
    os << m_myStringCont.Data() << std::endl;
    break;
  case DBFLOAT:
    os << m_myFloatCont.At(0) << std::endl;
    break;
  case DBDOUBLE:
    os << m_myDoubleCont.At(0) << std::endl;
    break;
  case DBVECTORINT:
    for(count = 0; count < 5; count++){
      os << "" << count << '\t';
    }
    os << std::endl;
    for(count = 0; count < m_myIntCont.GetSize(); count++){
      if(!(count%5) || count == 0) os << count << '\t'; else os << '\t';
      os << m_myIntCont.At(count) << '\t'; 
      if(!((count+1)%5)) os << std::endl;
    }
    os << std::endl;
    break;
  case DBVECTORFLOAT:
    for(count = 0; count < 5; count++){
      os << "" << count << '\t';
    }
    os << std::endl;
    for(count = 0; count < m_myFloatCont.GetSize(); count++){
      if(!(count%5) || count == 0) os << count << '\t'; else os << '\t';
      os << m_myFloatCont.At(count) << '\t'; 
      if(!((count+1)%5)) os << std::endl;
    }
    os << std::endl;
    break;
  case DBVECTORDOUBLE:
    for(count = 0; count < 5; count++){
      os << "" << count << '\t';
    }
    os << std::endl;
    for(count = 0; count < m_myDoubleCont.GetSize(); count++){
      if(!(count%5) || count == 0) os << count << '\t'; else os << '\t';
      os << m_myDoubleCont.At(count) << '\t'; 
      if(!((count+1)%5)) os << std::endl;
    }
    os << std::endl;
    break;
  case DBHISTO:{
    os << "dumping histogram to Dump Window" << std::endl;
    
    if(!gROOT->GetListOfCanvases()->FindObject("Dump Window")){
      dumpCanvas = new TCanvas("Dump Window","Dump Window",2);
    }
    dumpCanvas->cd();
    std::vector<double> v;
    for(count = 0; count < m_myDoubleCont.GetSize(); count++){
      v.push_back(m_myDoubleCont[count]);
    }
    TH1D* histo1;
    TH2D* histo2;
    Histo his(vectorToHisto(v));
    if(vectorHistoDim(v) == 1){
      histo1 = new TH1D(his.name().c_str(),his.title().c_str(), his.nBin(0), his.min(0),his.max(0));
      for(count = 0; count < his.nBin(0); count++){
	histo1->SetBinContent(count+1,his(count));
      }
      histo1->Draw();
    }
    else if(vectorHistoDim(v) == 2){
      histo2 = new TH2D(his.name().c_str(),his.title().c_str(), his.nBin(0), his.min(0),his.max(0),his.nBin(1), his.min(1),his.max(1));
      int count2;
      for(count = 0; count < his.nBin(0); count++){
	for(count2 = 0; count2 < his.nBin(1); count2++){
	  histo2->SetBinContent(count+1, count2+1, his(count,count2));
	}
      }
      histo2->Draw();
    }
    else{
      stringstream a;
      a << "RootDbField::Dump : Histogram is not 1 or 2 dimensional " << vectorHistoDim(v);
      throw PixDBException(a.str());
    }
    dumpCanvas->Modified();
    dumpCanvas->Update();}
    break;
  case DBSTRING:
    os << m_myStringCont.Data() << std::endl;
    break;
  case DBEMPTY:
    os << "field does not have data content" << std::endl;
  default:
    break;
  }
}

void RootDbField::Dump(void) const {
  dump(std::cout);
}
#if 0
void RootDbField::ModifyFieldValue(char* newValue){ // *MENU* modify the content of the Field
  int i = 0; 
  int hi = 0; 
  float f = 0;
  double d = 0;
  bool b = false;
  
  stringstream ss;
  ss.str(newValue);
  ss >> i;
  ss.clear();
  ss.str(newValue);
  ss >> std::hex >> hi >> std::dec;
  ss.clear();
  ss.str(newValue);
  ss >> f;
  ss.clear();
  ss.str(newValue);
  ss >> d;
  ss.clear();
  ss.str(newValue);
  ss >> std::boolalpha >> b;
  
  switch(this->getDataType()){
  case DBINT:
    if(hi!=0 && i==0)
      this->m_myIntCont[0] = hi;
    else 
      this->m_myIntCont[0] = i;
		break;
  case DBULINT:
    this->m_myStringCont = ss.str().c_str();
    break;
  case DBFLOAT:
    this->m_myFloatCont[0] = f;
    break;
  case DBBOOL:
    this->m_myIntCont[0] = b;
    break;
  case DBSTRING:
    this->m_myStringCont = ss.str().c_str();
    break;
  case DBDOUBLE:
    this->m_myDoubleCont[0] = b;
    break;
  default:
    std::cout << "RootDbField::ModifyFieldValue : not supported type" << std::endl;
  }  
  this->Write(this->getName().c_str(),TObject::kOverwrite);
}
#endif

ULong_t RootDbField::Hash() const { return TMath::Hash(m_decName.c_str()); }

Bool_t  RootDbField::IsEqual(const TObject *obj) const {
		if (dynamic_cast<RootDbField*>(const_cast<TObject*>(obj)) == NULL) return kFALSE;
		if(m_decName == dynamic_cast<RootDbField*>(const_cast<TObject*>(obj))->m_decName) return kTRUE; else return kFALSE;
	}

Bool_t  RootDbField::IsSortable() const { return kTRUE; }

Int_t   RootDbField::Compare(const TObject *obj) const {
  if (dynamic_cast<RootDbField*>(const_cast<TObject*>(obj)) == NULL) {
    std::stringstream a;
    a << "RootDbField::Compare(): Bad comparison";
    throw PixDBException(a.str());
  }
  ULong_t h1,h2;
  h1 = TMath::Hash(m_decName.c_str());
  h2 = TMath::Hash(dynamic_cast<RootDbField*>(const_cast<TObject*>(obj))->m_decName.c_str());
  if (h1 > h2) return 1;
  else if (h1 < h2) return -1;
  else return 0; }

void RootDbRecord::makeDecName(RootDbRecord* child, RootDbRecord* parent){
  // create the child fully decorated name from its names and the dec name of the parent
  // e.g. from child myDecName file1:/dir/dir/M510234/PixModule;1 and parent mydecName file2:/dir/pixmodgroup1/PixModuleGroup;1 
  // to child myDecName file2:/dir/pixmodgroup1/M510234/PixModule;1
  string tmpDecName = getMyDirectoryName(parent->getDecName());
  child->m_decName = purgeSlash(tmpDecName);
  child->m_decName += "/";
  child->m_decName += child->getName();
  child->m_decName += "/";
  child->m_decName += child->getClassName();
}




std::string RootDbRecord::getRecordName(int number){
  if((uint)number >= m_myRecordNameList.size() || number < 0) return "";
  //  else if(m_isLink == false) return stripIteration(getSubRecordNameFromList(m_myRecordNameList[number]));
  else return m_myRecordLogicalNameList[number];
}

std::string RootDbRecord::getFieldDecName(int number){
  if((uint)number >= m_myFieldNameList.size() || number < 0) return "";
  else return m_myFieldNameList[number];
}

std::string RootDbRecord::getRecordDecName(int number){
  if((uint)number >= m_myRecordNameList.size() || number < 0) return "";
  else return m_myRecordNameList[number];
}

bool RootDbRecord::isLink(int number){
  if((uint)number >= m_myLink.size() || number < 0) return "";
  else return m_myLink[number];
}


PixDbInterface* RootDbRecord::getDb() const {return m_myDb;}

dbRecordIterator RootDbRecord::recordBegin() {
  if (m_myRecordNameList.size() == 0) return recordEnd();
  return dbRecordIterator(new RecordIteratorWrapper(getRecord(0)));
} // the iterator to the depending records		

dbFieldIterator RootDbRecord::fieldBegin() {	
  if (m_myFieldNameList.size() == 0) return fieldEnd();
  return dbFieldIterator(new FieldIteratorWrapper(getField(0)));
} // the iterator to the record data fields	

dbRecordIterator  RootDbRecord::recordEnd() {
  dbRecordIterator r(new RecordIteratorWrapper(RootDbRecordIterator(m_myRecordNameList.size(),NULL,this)));
  return r;
} // the end iterator to the depending records	

dbFieldIterator  RootDbRecord::fieldEnd() {
  dbFieldIterator r(new FieldIteratorWrapper(RootDbFieldIterator(m_myFieldNameList.size(),NULL,this)));
  return r;
} // the end iterator to the record data fields	

dbRecordIterator RootDbRecord::findRecord(const string& recordName){
  uint it;
  // try first with namedir/name 
  // if not found, try with file:/dir/dir/dir/namedir/name;1
  // if not found, try with slotName
  // if not found, try with namedir
  for(it = 0; it < m_myRecordNameList.size(); it++)
    {
      if(stripIteration(getSubRecordNameFromList(m_myRecordNameList[it])) == stripIteration(recordName)) break;
    }
  if(it == m_myRecordNameList.size()){
    for(it = 0; it < m_myRecordNameList.size(); it++)
      {
	if(m_myRecordNameList[it] == recordName) break;
      }
  }
  if(it == m_myRecordNameList.size()){
    for(it = 0; it < m_myRecordLogicalNameList.size(); it++)
      {
	if(m_myRecordLogicalNameList[it] == recordName) break;
      }
  }
  if(it == m_myRecordNameList.size()){
    for(it = 0; it < m_myRecordNameList.size(); it++)
      {
	if(stripIteration(getMyIDName(m_myRecordNameList[it])) == stripIteration(recordName)) break;
      }
  }
  if(it == m_myRecordNameList.size()){
    return recordEnd();
  }
  else{
    RootDbRecordIterator returnvalue(it, NULL, this);
    return dbRecordIterator(new RecordIteratorWrapper(returnvalue));
  }
  //  else return getRecord(it);
} // find a subrecord by its name

dbFieldIterator RootDbRecord::findField(const string& fieldName){
  uint it;
  for(it = 0; it < m_myFieldNameList.size(); it++)
    {
      if(stripIteration(getFieldNameFromList(m_myFieldNameList[it])) == stripIteration(fieldName)) break;
    }
  if(it == m_myFieldNameList.size()){
    for(it = 0; it < m_myFieldNameList.size(); it++)
      {
	if(m_myFieldNameList[it] == fieldName) break;
      }
  }
  if(it == m_myFieldNameList.size()){
    return fieldEnd();
  }
  else{
    RootDbFieldIterator retvalue(it, NULL, this);
    return dbFieldIterator(new FieldIteratorWrapper(retvalue));
  }
  //  return getField(it); // commented since findField should only return the iterator, not actually access to the database
} // find a field by its name

#if 0
dbRecordIterator RootDbRecord::findRecord(const char* name){
  string sname(name);
  return findRecord(sname);
} // find a subrecord by its name

dbFieldIterator RootDbRecord::findField(const char* name){
  string sname(name);
  return findField(sname);
} // find a field by its name
#endif

void RootDbRecord::dump(std::ostream& os) const{
  os << "Dumping RootDbRecord" << std::endl;
  os << "record ClassName: " << m_className << std::endl;
  os << "record Name: " << m_name << std::endl;
  os << "record DecName: " << m_decName << std::endl;
  
  os << "List of depending field: " << m_myFieldNameList.size() << " depending fields found" << std::endl;
  unsigned int i;
  for(i = 0; i < m_myFieldNameList.size(); i++){
    os << m_myFieldNameList[i] << std::endl;
    //   dbFieldIterator f = (const_cast<RootDbRecord*>(this))->getField(i);
    //(*f)->dump(os);
  }
  os << "List of depending record: " << m_myRecordNameList.size() << " depending records found" << std::endl;
  for(i = 0; i < m_myRecordNameList.size(); i++){
    os << m_myRecordLogicalNameList[i] << " " << (m_myLink[i] ? "ISLINK " : " ") << m_myRecordNameList[i] << std::endl;
    //dbRecordIterator f = (const_cast<RootDbRecord*>(this))->getRecord(i);
    //(*f)->dump(os);
  }
  
} // Dump - debugging purpose

int RootDbRecord::getDependingRecordSize(void) const {return m_myRecordNameList.size();} // RootDbRecord::Get the amount of depending records

int RootDbRecord::getDependingFieldSize(void) const{return m_myFieldNameList.size();} // Get the amount of depending field

RootDbFieldIterator RootDbRecord::getField(int fieldNumber){
  uint in = fieldNumber;
  if(in < 0 || in > m_myFieldNameList.size()){
    std::stringstream a;
    a << "RootDbRecord::getField(int fieldNumber): record number " << in << " not found in record " << getName().c_str() << getDecName().c_str();
    throw PixDBException(a.str().c_str());    
  }
  else if(in == m_myFieldNameList.size()){
    //return fieldEnd();
    return RootDbFieldIterator(m_myFieldNameList.size(),NULL,this);
  }
  else{
    //    dbFieldIterator r(fieldNumber,m_myDb->DbFindFieldByName(this->m_myFieldNameList[in]),this); 
    RootDbFieldIterator r(fieldNumber,0,this); 
    return r;
  }
} // Get the pointer to the field number fieldNumber; throw an exception if fails

RootDbRecordIterator RootDbRecord::getRecord(int recordNumber){
  uint in = recordNumber;
  if(in < 0 || in > m_myRecordNameList.size()){
    std::stringstream a;
    a << "RootDbRecord::getRecord(int recordNumber): record number " << in << " not found in record " << getName().c_str() << getDecName().c_str();
    throw PixDBException(a.str().c_str());    
  }
  else if(in == m_myRecordNameList.size()){
    //return recordEnd();
    return RootDbRecordIterator(m_myRecordNameList.size(),NULL,this);
  }
  else{
    RootDbRecord* rec = dynamic_cast<RootDbRecord*>(m_myDb->DbFindRecordByName(m_myRecordNameList[in]));
    if(!rec) {
      throw PixDBException("RootDbRecord::getRecord(int): can't find the matching name");
    }
    RootDbRecordIterator r(recordNumber,rec,this); 
    return r;
  }
} // Get the pointer to the record number recordNumber; throw an exception if fails

dbFieldIterator RootDbRecord::pushField(DbField* in_raw){
  RootDbField *in = dynamic_cast<RootDbField*>(in_raw);
  if(!in) {
    throw PixDBException("RootDbRecord::pushField(DbField*): the field is not RootDbField");
  }
  // push the field in into the record; temporary copy it into the list of pushed fields
  RootDbField* dbf = dynamic_cast<RootDbField*>(this->getDb()->makeField(in->getName()));
  RootDbFieldIterator f;
  f.pointsTo(in);
  dbf->copyField(f);

  dbf->m_decName = getMyDirectoryName(this->m_decName);
  dbf->m_decName += "/";
  dbf->m_decName += dbf->getName();
  int pos = (int)(dbf->getDecName().find(";1"));
  if(pos == (int)std::string::npos) // found no ;1 in the name: adding it
    dbf->m_decName += ";1";  

  //overwrite if same name
  dbFieldIterator fieldToFind = findField(dbf->getName());
  if(fieldToFind == fieldEnd()){
    this->m_myFieldNameList.push_back(dbf->getDecName());         
  }
  else{
    //    dbFieldIterator fi = findField(in->getName());
    eraseField(fieldToFind);
    this->m_myFieldNameList.push_back(dbf->getDecName());             
  }
  // access to the file 
  m_myDb->putField(dbf, this);
  m_myDb->putRecord(this);
  // get back the field
  dbFieldIterator retval = this->findField(dbf->getDecName());
//   FieldIteratorWrapper *w = dynamic_cast<FieldIteratorWrapper*>(retval.getWrapper());
//   if(!w) {
//     throw PixDBException("RootDbRecord::eraseField(): iterator wrapper is not FieldIteratorWrapper");
//   }
//   RootDbFieldIterator& ittosee = w->m_iter;
  
  // delete the temporary field
  delete dbf;

  return retval;
} // add a field to the DbRecord fields and return the corresponding iterator. the field is actually copied into the database

void RootDbRecord::eraseField(dbFieldIterator abstract_it){  
  if(abstract_it != this->fieldEnd()){
    FieldIteratorWrapper *w = dynamic_cast<FieldIteratorWrapper*>(abstract_it.getWrapper());
    if(!w) {
      throw PixDBException("RootDbRecord::eraseField(): iterator wrapper is not FieldIteratorWrapper");
    }
    RootDbFieldIterator& it = w->m_iter;
    this->m_myFieldNameList.erase(it.isNumber() + m_myFieldNameList.begin());
    // access to file
    m_myDb->putRecord(this);
  }
  else{
    stringstream a;
    a << "RootDbRecord::eraseField: field is endField() " << *this;
    std::cout << a.str();
    return;
    throw PixDBException(a.str());
  }
  return;
  
} // erase a field. This means taking it out of the list of depending fields of this record. The disk content and memory content is not changed, since other records could refer to the same field

dbRecordIterator RootDbRecord::linkRecord(DbRecord* in, const string& linkName, const string& dstSlotName){
  //  throw runtime_error("RootDbRecord::linkRecord(): not implemented with dstSlotName");
  
  
  string tmpDecName = in->getName(); string tmpName = in->getClassName();
  unsigned int i ;//  ,ipos;
  bool found = false;
  for (i=0; i<m_myRecordLogicalNameList.size(); i++) {
    if (m_myRecordLogicalNameList[i] == linkName) {
     // ipos = i;
      found = true;
      break;
    }
  }
  try{
    if( dstSlotName != "" && dstSlotName != "_________DONOTRECURSETHATLINK_________" && found != true){ // bidirectional link; try to link the destination
      in->linkRecord(this,dstSlotName, ""); 
    }
    else if(dstSlotName == "" && found != true){
      in->linkRecord(this, this->getName(), "_________DONOTRECURSETHATLINK_________");
    } 
  }
  catch(PixDBException&){
    stringstream a;
    a << "RootDbRecord::linkRecord: bidirectional link failed because linkName already exists in destination link ";
    //std::cout << a.str() << m_myRecordLogicalNameList[ipos] << std::endl << *this  ;
    throw PixDBException(a.str());
  }
  if (found) {
    stringstream a;
    a << "RootDbRecord::linkRecord: linkName already exists ";
    //std::cout << a.str() << m_myRecordLogicalNameList[ipos] << std::endl << *this  ;
    throw PixDBException(a.str());
  } else {
    
    this->m_myRecordNameList.push_back(in->getDecName());         
    this->m_myRecordLogicalNameList.push_back(linkName);
    this->m_myLink.push_back(true);
  }
  // write the pushing record - access to file
  m_myDb->putRecord(this);

  dbRecordIterator retval = this->findRecord(linkName);
  return retval;
}

dbRecordIterator RootDbRecord::updateLink(DbRecord* dstRec, const string& srcSlotName, const string& dstSlotName) {
  RootDbRecord* newLinked = 0;
  dbRecordIterator oldLinkedIt;

  if((newLinked = dynamic_cast<RootDbRecord*>(dstRec)) == 0){
    stringstream a;
    a << "RootDbRecord::updateLink: trying to link a non RootDbRecord";
    throw PixDBException(a.str());
  }
  
  //1) delete link dstSlotName from oldLink, if there is any
  oldLinkedIt = findRecord(srcSlotName);
  if(oldLinkedIt != recordEnd()){
    RecordIteratorWrapper *w1 = dynamic_cast<RecordIteratorWrapper*>(oldLinkedIt.getWrapper());
    if(!w1) {
      throw PixDBException("RootDbRecord::updatelink(): iterator wrapper is not RecordIteratorWrapper");
    }
    RootDbRecordIterator& it = w1->m_iter;
    if(m_myLink[it.isNumber()] != true){
      stringstream a;
      a << "RootDbRecord::updateLink: trying to update a non link object";
      throw PixDBException(a.str());
    }
    oldLinkedIt = this->getDb()->DbProcess(oldLinkedIt, PixDb::DBREAD);
    w1 = dynamic_cast<RecordIteratorWrapper*>(oldLinkedIt.getWrapper());
    if(!w1) {
      throw PixDBException("RootDbRecord::updatelink(): iterator wrapper is not RecordIteratorWrapper");
    }
    it = w1->m_iter;
    RootDbRecord* oldSecondLink = dynamic_cast<RootDbRecord*>(*it);
    dbRecordIterator oldReturnLinkIt = (*oldLinkedIt)->findRecord(dstSlotName);
    if(oldReturnLinkIt != (*oldLinkedIt)->recordEnd()){
      RecordIteratorWrapper *w2 = dynamic_cast<RecordIteratorWrapper*>(oldLinkedIt.getWrapper());
      if(!w2) {
	throw PixDBException("RootDbRecord::updatelink(): iterator wrapper is not RecordIteratorWrapper");
      }
      RootDbRecordIterator& itit = w2->m_iter;
      oldSecondLink->m_myRecordNameList.erase(itit.isNumber() + oldSecondLink->m_myRecordNameList.begin());
      oldSecondLink->m_myRecordLogicalNameList.erase(itit.isNumber() + oldSecondLink->m_myRecordLogicalNameList.begin());
      oldSecondLink->m_myLink.erase(itit.isNumber() + oldSecondLink->m_myLink.begin());
    }
  }
  uint i;
  for(i = 0; i < m_myRecordLogicalNameList.size(); i++){
    if(m_myRecordLogicalNameList[i] == srcSlotName){
      m_myRecordNameList.erase(i + m_myRecordNameList.begin());
      m_myRecordLogicalNameList.erase(i + m_myRecordLogicalNameList.begin());
      m_myLink.erase(i + m_myLink.begin());
      break;
    }
  }
  m_myRecordNameList.push_back(newLinked->getDecName());         
  m_myRecordLogicalNameList.push_back(srcSlotName);        
  m_myLink.push_back(true);
  
  for(i = 0; i < newLinked->m_myRecordLogicalNameList.size(); i++){
    if(newLinked->m_myRecordLogicalNameList[i] == dstSlotName){
      if(newLinked->m_myLink[i] != true){
	stringstream a;
	a << "RootDbRecord::updateLink: trying to update a non link object in third object";
	throw PixDBException(a.str());	  
      }
      newLinked->m_myRecordNameList.erase(i + newLinked->m_myRecordNameList.begin());
      newLinked->m_myRecordLogicalNameList.erase(i + newLinked->m_myRecordLogicalNameList.begin());
      newLinked->m_myLink.erase(i + newLinked->m_myLink.begin());
      break;
    }
  }
  
  newLinked->m_myRecordNameList.push_back(getDecName());         
  newLinked->m_myRecordLogicalNameList.push_back(dstSlotName);        
  newLinked->m_myLink.push_back(true);
  return findRecord(srcSlotName);
}

dbRecordIterator RootDbRecord::pushRecord(DbRecord* in, std::string sltName){
  // create a copy of the pushed record
  RootDbRecord* dbi = dynamic_cast<RootDbRecord*>(this->getDb()->makeRecord(in->getClassName(), in->getName()));
  // change the fully decorated name
  makeDecName(dbi,this);
  // overwrite if find a record ->of the same name
  string tmpDecName = dbi->getName(); string tmpName = dbi->getClassName();
  if(sltName == "") sltName = dbi->getName();
  if(findRecord(sltName) == recordEnd()){
    this->m_myRecordNameList.push_back(dbi->getDecName());         
    this->m_myRecordLogicalNameList.push_back(sltName);        
    this->m_myLink.push_back(false);
  }
  else{
    dbRecordIterator ri = findRecord((tmpDecName + "/" + tmpName));
    eraseRecord(ri);
    this->m_myRecordNameList.push_back(in->getDecName());             
    this->m_myRecordLogicalNameList.push_back(dbi->getName());         
    this->m_myLink.push_back(false);
  }

  // push fields of the pushed record
  dbFieldIterator fi = in->fieldBegin();
  for(; fi != in->fieldEnd(); fi++){
    dbi->pushField(*fi);
  }
  // write the pushing record and the pushed record - both have changed - access to file
  m_myDb->putRecord(dbi);
  m_myDb->putRecord(this);
  // get back the field
  dbRecordIterator retval = this->findRecord(sltName);
  // delete the temporary field
  delete dbi;

  return retval;
} // add a record to the record list and return the corresponding iterator

void RootDbRecord::eraseRecord(dbRecordIterator abstract_it) {
  bool bidirLinkErase = false;
  if(abstract_it != this->recordEnd()){
    RecordIteratorWrapper *w = dynamic_cast<RecordIteratorWrapper*>(abstract_it.getWrapper());
    if(!w) {
      throw PixDBException("RootDbRecord::eraseRecord(): iterator wrapper is not RecordIteratorWrapper");
    }
    RootDbRecordIterator& it = w->m_iter;
    if(it.isNumber() >= (int)m_myRecordNameList.size()){
      std::stringstream a;
      a << "RootDbRecord::eraseRecord: iterator passed with it.isNumber() " << it.isNumber() << " is greater than the size of depending record " <<  m_myRecordNameList.size();
      throw PixDBException(a.str());      
    }
    dbRecordIterator thisRecLink;
    
    if(this->m_myLink[it.isNumber()] == true){
      abstract_it = this->getDb()->DbProcess(abstract_it,PixDb::DBREAD);
      thisRecLink = (*abstract_it)->findRecord(this->getDecName());
      if(thisRecLink != (*abstract_it)->recordEnd()) bidirLinkErase = true;
    }
    this->m_myRecordNameList.erase(it.isNumber() + m_myRecordNameList.begin());
    this->m_myRecordLogicalNameList.erase(it.isNumber() + m_myRecordLogicalNameList.begin());
    this->m_myLink.erase(it.isNumber() + m_myLink.begin());
    // access to file
    m_myDb->putRecord(this);
    if (bidirLinkErase){
      abstract_it = this->getDb()->DbProcess(abstract_it,PixDb::DBREAD);
      thisRecLink = (*abstract_it)->findRecord(this->getDecName());
      (*abstract_it)->eraseRecord(thisRecLink);
    }
  }
  return;
} // erase a record

dbFieldIterator RootDbRecord::addField(const string& name) {
  DbField* tmpField = m_myDb->makeField(name);
  dbFieldIterator retval = pushField(tmpField);
  delete tmpField;
  return retval;
}


DbRecord* RootDbRecord::addRecord(const string& name, const string& dirName, const string& ID) {
  //  if(ID != "")  throw runtime_error("RootDbRecord::addRecord() is not implemented with ID");
  dbRecordIterator ri;
  std::string IDstring = ID;
  if(ID == "") IDstring = dirName;
  dbRecordIterator fi = findRecord(IDstring+"/"+name);
  if (fi == recordEnd()) {
    //std::cout << "record not found" << std::endl;
    DbRecord *newInq = m_myDb->makeRecord(name, IDstring);
    ri = pushRecord(newInq, dirName);
    ri = getDb()->DbProcess(ri,PixDb::DBREAD);
    delete newInq;
  } else {
    ri = getDb()->DbProcess(fi,PixDb::DBREAD);
  }
  return *ri;
}

void RootDbRecord::Dump(void) const{
  dump(std::cout);
}



ULong_t RootDbRecord::Hash() const {
  return TMath::Hash(m_decName.c_str());
}

Bool_t  RootDbRecord::IsEqual(const TObject *obj) const {
//   if (dynamic_cast<RootDbRecord*>(const_cast<TObject*>(obj)) == NULL) return kFALSE;
//   if((m_decName == dynamic_cast<RootDbRecord*>(const_cast<TObject*>(obj))->m_decName) &&
//      (m_id == dynamic_cast<RootDbRecord*>(const_cast<TObject*>(obj))->m_id))
//     return kTRUE; else return kFALSE;
  return kTRUE;
}

Bool_t  RootDbRecord::IsSortable() const { 
  return kTRUE; 
}

Int_t   RootDbRecord::Compare(const TObject *obj) const {
//   if (dynamic_cast<RootDbRecord*>(const_cast<TObject*>(obj)) == NULL) {
//     std::stringstream a;
//     a << "RootDbRecord::Compare(): Bad comparison";
//     throw PixDBException(a.str());
//   }
//   ULong_t h1,h2;
//   stringstream s1,s2;
//   s1 << m_id;
//   s2 << dynamic_cast<RootDbRecord*>(const_cast<TObject*>(obj))->m_id;
//   std::string nameNumber = m_decName + s1.str();
//   h1 = TMath::Hash(nameNumber.c_str());
//   RootDbRecord* tr =  dynamic_cast<RootDbRecord*>(const_cast<TObject*>(obj));
//   h2 = TMath::Hash((tr->m_decName+s2.str()).c_str());
//   if (h1 > h2) return 1;
//   else if (h1 < h2) return -1;
//   else return 0; 
  return 0;
}



RootDbRecord::RootDbRecord(const std::string &className, const std::string &name):
  m_className(className),
  m_name(name)
{}

//RootDbRecord::RootDbRecord(const std::string &className) :
//_className(className)
//{}

// RootDbRecord::RootDbRecord(string decName, int id) : 
//   m_decName(decName)
// {} // constructor used in RootDb::getRecordbydecname. In the TBTree records are ordered by thei unique mydecname

bool  RootDb::commitRootRecordReplaceTree(RootDbRecord* currRecord){
  bool retval;
  int i;
  retval = putRecord(currRecord);
  for(i = 0; i < (int)currRecord->m_myRecordNameList.size(); i++){
    retval = commitRootRecordReplaceTree(dynamic_cast<RootDbRecord*>(*(currRecord->getRecord(i))));
  }
  return retval;
}

bool RootDb::putRecord(RootDbRecord* recordToPut){
  bool retval;
  // write the record to the disk
  // first: move to the correct directory, possibly creating it
  m_mutex.lock();
  try{
    TDirectory* currDir = gDirectory;
    TFile* theFile = getFileHandler(recordToPut->getDecName());
    //  m_theFile->cd();
    theFile->cd();
    //    gDirectory = m_theFile;
    //gDirectory = theFile;
    //  std::cout << gDirectory->GetPath() << std::endl;
    RootCDCreate(theFile,recordToPut);
    // second: write the record to the disk
    retval = recordToPut->Write(stripIteration(recordToPut->getClassName().c_str()).c_str(),TObject::kOverwrite);
    // third: delete the record from memory
    //  std::string recordName = recordToPut->getDecName();
    // forth: reload the record from disk and put it into the database, in order to achieve a "clean" situation  
    //recordToPut = getRecordByDecName(recordName);
    // sixth: return to the original directory
    gDirectory = currDir;
    m_mutex.unlock();
    return retval;
  }
  catch(PixDBException& ex){
    m_mutex.unlock();
    // ex.what(std::cerr);
    throw ex;
  }
  catch(...){
    std::cerr << "unhandled non-PixDBException in RootDb::putRecord(), returning" << std::endl;
    m_mutex.unlock();
    throw;
  }

}

bool RootDb::putField(RootDbField* fieldToPut, RootDbRecord* recordWherePut){
  // first: move to the correct directory, possibly creating it
  bool retval;
  m_mutex.lock();
  try{
    TDirectory* currDir = gDirectory; 
    TFile* theFile = getFileHandler(recordWherePut->getDecName());
    theFile->cd();
    //  gDirectory = m_theFile;
    RootCDCreate(gDirectory,recordWherePut);
    // write the field to the disk
    retval = fieldToPut->Write(stripIteration(fieldToPut->getName().c_str()).c_str(),TObject::kOverwrite);
    // remove the field from memory
    currDir->cd();
    m_mutex.unlock();
    return retval;
  }
  catch(PixDBException& ex){
    m_mutex.unlock();
    // ex.what(std::cerr);
    throw ex;
  }
  catch(...){
    std::cerr << "unhandled non-PixDBException in RootDb::putField(), returning" << std::endl;
    m_mutex.unlock();
    throw;
  }
}

RootDbRecord* RootDb::getRecordByDecName(string name) {
  m_mutex.lock();
  try{
    TDirectory* currDir = gDirectory; 
    TFile* theFile = getFileHandler(name);
    RootDbRecord * rootval;
    RootDbRecord * retval = 0;

    std::string fileName = getFileName(name);
    if (fileName == ""){ 
      stringstream a;
      a << "RootDb::getFieldByDecName(string name):Invalid file name";
      throw PixDBException(a.str());
    }
    // handles first the case that the file name requested is the original file name, the ones stored into rootrecord. If so, returns the current file. 
    if(m_theOriginalFileName != "__NONAME__"){
      if(fileName == m_theOriginalFileName){
	string objName = removeFileName(name);
	name = m_theFileName + ":" + objName;
      }
    }

    // add the ;1 required by root in order to get the object from file rather than from memory, if in the name there is not that ;1
    int pos = (int)name.find(";1");
    if(pos == (int)std::string::npos) // found no ;1 in the name: adding it
      name += ";1";
    
    rootval = (RootDbRecord*)theFile->Get(name.c_str());
    if(rootval != 0){ // found and loaded into memory a record named name.c_str()
      //rootval->m_myDb = this;
      retval = dynamic_cast<RootDbRecord*>(this->makeRecord(" "," "));
      //dbRecordIterator ri(rootval);
      
      retval->m_className = rootval->m_className;
      retval->m_name = rootval->m_name;
      retval->m_decName = rootval->m_decName;
      retval->m_myFieldNameList = rootval->m_myFieldNameList; // the list of names of the fields belonging to this record in format file:/dir/dir/dir/PixFE_1/name;1
      retval->m_myRecordNameList = rootval->m_myRecordNameList; // the list of names of the record belonging to this record in format file:/dir/dir/dir/PixFE_1/decName/name;
      retval->m_myRecordLogicalNameList = rootval->m_myRecordLogicalNameList; // the list of logical names - in the format decName
      retval->m_myLink = rootval->m_myLink; // is a link the record?
      //      theFile->Delete(stripIteration(name).c_str()); //delete from memory, we already have a copy in memory..
    }
    gDirectory = currDir;
    m_mutex.unlock();
    return retval;
  }
  catch(PixDBException& ex){
    m_mutex.unlock();
    // ex.what(std::cerr);
    throw ex;
  }
  catch(...){
    std::cerr << "unhandled non-PixDBException in RootDb::getRecordByDecName(), returning" << std::endl;
    m_mutex.unlock();
    throw;
  }  
}

RootDbField* RootDb::getFieldByDecName(string name) {
  m_mutex.lock();
  try{
    TDirectory* currDir = gDirectory; 
    TFile* theFile = getFileHandler(name);
    RootDbField* rootval = 0;
    RootDbField* retval = 0;

    std::string fileName = getFileName(name);
    if (fileName == ""){ 
      stringstream a;
      a << "RootDb::getFieldByDecName(string name):Invalid file name";
      throw PixDBException(a.str());
    }
    // handles first the case that the file name requested is the original file name, the ones stored into rootrecord. If so, returns the current file. 
    if(m_theOriginalFileName != "__NONAME__"){
      if(fileName == m_theOriginalFileName){
	string objName = removeFileName(name);
	name = m_theFileName + ":" + objName;
      }
    }

    // add the ;1 required by root in order to get the object from file rather than from memory, if in the name there is not that ;1
    int pos = (int)name.find(";1");
    if(pos == (int)std::string::npos) // found no ;1 in the name: adding it
      name += ";1";
    
    rootval = (RootDbField*)theFile->Get(name.c_str());
    if(rootval != 0) {
      rootval->m_myDb = this;
      retval = dynamic_cast<RootDbField*>(this->makeField(" "));
      RootDbFieldIterator fi(rootval);
      retval->copyField(fi);
      //      theFile->Delete(stripIteration(name).c_str());
      
    }
    gDirectory = currDir;
    m_mutex.unlock();
    return retval;
  }
  catch(PixDBException& ex){
    m_mutex.unlock();
    // ex.what(std::cerr);
    throw ex;
  }
  catch(...){
    std::cerr << "unhandled non-PixDBException in RootDb::getFieldByDecName(), returning" << std::endl;
    m_mutex.unlock();
    throw;
  }
}

void RootDb::RootCDCreate(TDirectory* startingDir, RootDbRecord* record){
  std::vector<string> items;
  std::string tmpstring = getMyDirectoryName(record->getDecName());
  items = pathItemize(tmpstring);
  if(items.size() == 0) return;
  uint count = 0;
  // create the directory if needed
  for(;count < items.size(); count++){
    if(gDirectory->GetKey(purgeSlash(items[count]).c_str()) == 0){
      gDirectory->mkdir(purgeSlash(items[count]).c_str());
    }
    gDirectory->cd(items[count].c_str());
  }
}

TFile* RootDb::getFileHandler(std::string name){

//   char absolutePath[PATH_MAX];
//   char* retvalue = realpath(namefile.c_str(), absolutePath);
//   if(retvalue == 0){
//     stringstream a;
//     a << "RootDb::RootDb: error in namefile " << namefile.c_str() << " not resolved to an absolute name. realpath returned: " << errno;
//     throw PixDBException(a.str());
//   }
//   namefile = absolutePath;

  std::string fileName = getFileName(name);
  if (fileName == ""){ 
    stringstream a;
    a << "RootDb::getFileHandler():Invalid file name";
    throw PixDBException(a.str());
  }
  // handles first the case that the file name requested is the original file name, the ones stored into rootrecord. If so, returns the current file. 
  if(m_theOriginalFileName != "__NONAME__"){
    if(fileName == m_theOriginalFileName) return m_theFile;
  }

  std::map<std::string, TFile*>::iterator itFind = m_theFileHandlers.find(fileName);
  if(itFind != m_theFileHandlers.end()){
    return (*itFind).second; 
  }
  else{
    TFile * addedFile = new TFile(fileName.c_str(), "READ");
    if (addedFile->IsZombie()){
      stringstream a;
      a << "File " << addedFile->GetName() << "not opened";
      throw PixDBException(a.str());
    }
    std::pair<std::string, TFile*> insPair(fileName, addedFile);
    m_theFileHandlers.insert(insPair);
    return addedFile;
  }
}

DbRecord* RootDb::makeRecord(const string& name, const string& decName){ // name: the object this Record points to, e.g. PixFE; decName: the decorated Name, that is the mydir of the record 
  //  throw runtime_error("RootDb::makeRecord(const string&, const string&): the meaning of the agruments is to be discussed");
  if(decName.size() == 0){
    throw PixDBException("RootDb::MakeRecord: Directory name not provided, decName.size() == 0");    
  }
  //  decName = addFileToName(decName, this->m_theFileName);
  RootDbRecord * dbrecord = new RootDbRecord(name, decName);
  dbrecord->m_myDb = this;
  return dbrecord;
}

DbField* RootDb::makeField(const string& name){
  RootDbField * dbfield = new RootDbField(name);
  dbfield->m_myDb = this;
  return dbfield;
}

DbRecord* RootDb::readRootRecord() { // get the root record 
  m_mutex.lock();
  try{
    TDirectory* k = gDirectory;
    // build the ROOT name of the required rootRecord
    m_theFile->cd();
    string name(m_theFile->GetPath());
    name += "rootRecord;1";
    // get the rootRecord from file or from the TBtree of the records
    m_mutex.unlock();
    RootDbRecord* retval = getRecordByDecName(name);
    m_mutex.lock();
    if(retval == 0){
      string error("RootDb::readRootRecord(): not found root record");
      throw PixDBException(error.c_str());    
    }
    gDirectory = k;
    m_mutex.unlock();
    string rootRecordFileName = getFileName(retval->getDecName());
    if(rootRecordFileName != m_theFileName){
      m_theOriginalFileName = rootRecordFileName;
    } 
    return retval;
  }
  catch(PixDBException& ex){
    m_mutex.unlock();
    // ex.what(std::cerr);
    throw ex;
  }
  catch(...){
    std::cerr << "unhandled non-PixDBException in RootDb::readRootRecord(), returning" << std::endl;
    m_mutex.unlock();
    throw;
  }
}

//################################################################
dbFieldIterator RootDb::DbProcess(RootDbFieldIterator theField, enum DbAccessType mode, Histo& retval){
  std::vector<double> tmpvec;
  bool allocated = false;
  RootDbField* field = 0;
  if(*theField != 0){
    field = dynamic_cast<RootDbField*>(*theField);
    if(field == 0){
      stringstream a;
      a << "RootDb::DbProcess: dbFieldIterator is invalid, field " << dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number) << " is not present on this Db";
      throw PixDBException(a.str());
    }
  }
  else{
    if(theField.m_parentRecord != 0) {
      RootDbFieldIterator rf(theField.m_number, getFieldByDecName(dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number)), theField.m_parentRecord);
      allocated = true;
      theField = rf;
    }
    else{
      stringstream a;
      a << "RootDb::DbProcess:HISTO dbFieldIterator is invalid, parent is zero ";
      throw PixDBException(a.str());
    }
    field = dynamic_cast<RootDbField*>(*theField);
    if(field == 0){
      stringstream a;
      a << "RootDb::DbProcess: dbFieldIterator is invalid, field " << dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number) << " is not present on this Db";
      throw PixDBException(a.str());
    }
  }
  if(!(field->getDataType() == DBHISTO || field->getDataType() == DBEMPTY)){
    stringstream a;
    a << "RootDb::DbProcess: dbFieldIterator is a " << DbDataTypeNames[field->getDataType()] << " on a string process";
    throw PixDBException(a.str());
  }    
  
  switch (mode){
  case DBREAD:
    if(field->getDataType() == DBEMPTY){
      stringstream a;
      a << "RootDb::DbProcess: try to read an empty dbFieldIterator";
      throw PixDBException(a.str());
    }    
    for(int i = 0; i < field->m_myDoubleCont.GetSize(); i++){
      tmpvec.push_back(field->m_myDoubleCont.At(i));
    }
    retval = vectorToHisto(tmpvec);		
    return dbFieldIterator(new FieldIteratorWrapper(theField));
  case DBCOMMIT:{
    field->m_dataType = DBHISTO;
    tmpvec = histoToVector(retval);
    field->m_myDoubleCont.Set(tmpvec.size());
    for(uint ii = 0; ii < tmpvec.size(); ii++){
      field->m_myDoubleCont[ii] = tmpvec[ii];
    }
    if(theField.m_parentRecord != 0){
      if(theField.m_parentRecord->findField(theField.m_field->getName()) != theField.m_parentRecord->fieldEnd()){
	this->putRecord(theField.m_parentRecord);
	this->putField(field, theField.m_parentRecord);	
      }
    }
    if(allocated && theField.m_field) delete theField.m_field;
    return dbFieldIterator(new FieldIteratorWrapper(theField));
    break;}
  default:
    stringstream a;
    a << "RootDb::DbProcess: mode not implemented histo" << mode;
    throw PixDBException(a.str());
    break;
  }

} // read or commit the field pointed by the iterator

dbFieldIterator RootDb::DbProcess(RootDbFieldIterator theField, enum DbAccessType mode, string& retval){
  bool allocated = false;
  std::vector<double> tmpvec;
  RootDbField* field = 0;
  if(*theField != 0){
    field = dynamic_cast<RootDbField*>(*theField);
    if(field == 0){
      stringstream a;
      a << "RootDb::DbProcess: dbFieldIterator is invalid, field " << dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number) << " is not present on this Db";
      throw PixDBException(a.str());
    }
  }
  else{
    if(theField.m_parentRecord != 0) {
      RootDbFieldIterator rf(theField.m_number, getFieldByDecName(dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number)), theField.m_parentRecord);
      allocated = true;
      theField = rf;
    }
    else{
      stringstream a;
      a << "RootDb::DbProcess:STRING dbFieldIterator is invalid, parent is zero ";
      throw PixDBException(a.str());
    }
    field = dynamic_cast<RootDbField*>(*theField);
    if(field == 0){
      stringstream a;
      a << "RootDb::DbProcess: dbFieldIterator is invalid, field " << dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number) << " is not present on this Db";
      throw PixDBException(a.str());
    }
  }
  if(!(field->getDataType() == DBSTRING || field->getDataType() == DBEMPTY)){
    stringstream a;
    a << "RootDb::DbProcess: dbFieldIterator is a " << DbDataTypeNames[field->getDataType()] << " on a string process";
    throw PixDBException(a.str());
  }    
  switch (mode){
  case DBREAD:
    if(field->getDataType() == DBEMPTY){
      stringstream a;
      a << "RootDb::DbProcess: try to read an empty dbFieldIterator";
      throw PixDBException(a.str());
    }    
    retval = field->m_myStringCont;
    return dbFieldIterator(new FieldIteratorWrapper(theField));
  case DBCOMMIT:
    field->m_dataType = DBSTRING;
    field->m_myStringCont = retval.c_str();
    if(theField.m_parentRecord != 0){
      if(theField.m_parentRecord->findField(field->getName()) != theField.m_parentRecord->fieldEnd()){
	this->putRecord(theField.m_parentRecord);
	this->putField(field, theField.m_parentRecord);
      }
    }
    if(allocated && theField.m_field) delete theField.m_field;
    return dbFieldIterator(new FieldIteratorWrapper(theField));
    break;
  default:
    stringstream a;
    a << "RootDb::DbProcess: mode not implemented string" << mode;
    throw PixDBException(a.str());
    break;
  }
} // read or commit the field pointed by the iterator

dbFieldIterator RootDb::DbProcess(RootDbFieldIterator theField, enum DbAccessType mode, int& retval){
  std::vector<double> tmpvec;
  bool allocated = false;
  RootDbField* field = 0;
  if(*theField != 0){
    field = dynamic_cast<RootDbField*>(*theField);
    if(field == 0){
      stringstream a;
      a << "RootDb::DbProcess: dbFieldIterator is invalid, field " << dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number) << " is not present on this Db";
      throw PixDBException(a.str());
    }
  }
  else{
    if(theField.m_parentRecord != 0) {
      RootDbFieldIterator rf(theField.m_number, getFieldByDecName(dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number)), theField.m_parentRecord);
      allocated = true;
      theField = rf;
    }
    else{
      stringstream a;
      a << "RootDb::DbProcess: INT dbFieldIterator is invalid, parent is zero ";
      throw PixDBException(a.str());
    }
    field = dynamic_cast<RootDbField*>(*theField);
    if(field == 0){
      stringstream a;
      a << "RootDb::DbProcess: dbFieldIterator is invalid, field " << dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number) << " is not present on this Db";
      throw PixDBException(a.str());
    }
  }
  if(!(field->getDataType() == DBINT || field->getDataType() == DBEMPTY)){
    stringstream a;
    a << "RootDb::DbProcess: dbFieldIterator is a " << DbDataTypeNames[field->getDataType()] << " on a string process";
    throw PixDBException(a.str());
  }    

  switch (mode){
  case DBREAD:
    if(field->getDataType() == DBEMPTY){
      stringstream a;
      a << "RootDb::DbProcess: try to read an empty dbFieldIterator";
      throw PixDBException(a.str());
    }    
    retval = field->m_myIntCont.At(0);
    return dbFieldIterator(new FieldIteratorWrapper(theField));
  case DBCOMMIT:
    field->m_dataType = DBINT;
    field->m_myIntCont[0] = retval;
    if(theField.m_parentRecord != 0){
      if(theField.m_parentRecord->findField(field->getName()) != theField.m_parentRecord->fieldEnd()){
	this->putRecord(theField.m_parentRecord);
	this->putField(field, theField.m_parentRecord);
      }
    }
    if(allocated && theField.m_field) delete theField.m_field;
    return dbFieldIterator(new FieldIteratorWrapper(theField));
    break;
  default:
    stringstream a;
    a << "RootDb::DbProcess: mode not implemented int" << mode;
    throw PixDBException(a.str());
    break;
  }
} // read or commit the field pointed by the iterator

dbFieldIterator RootDb::DbProcess(RootDbFieldIterator theField, enum DbAccessType mode, unsigned int & retval){
  std::vector<double> tmpvec;
  stringstream s;
  RootDbField* field = 0; bool allocated = false;
  if(*theField != 0){
    field = dynamic_cast<RootDbField*>(*theField);
    if(field == 0){
      stringstream a;
      a << "RootDb::DbProcess: dbFieldIterator is invalid, field " << dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number) << " is not present on this Db";
      throw PixDBException(a.str());
    }
  }
  else{
    if(theField.m_parentRecord != 0) {
      RootDbFieldIterator rf(theField.m_number, getFieldByDecName(dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number)), theField.m_parentRecord);
      allocated = true; theField = rf;
    }
    else{
      stringstream a;
      a << "RootDb::DbProcess:UINT dbFieldIterator is invalid, parent is zero ";
      throw PixDBException(a.str());
    }
    field = dynamic_cast<RootDbField*>(*theField);
    if(field == 0){
      stringstream a;
      a << "RootDb::DbProcess: dbFieldIterator is invalid, field " << dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number) << " is not present on this Db";
      throw PixDBException(a.str());
    }
  }
  if(!(field->getDataType() == DBULINT || field->getDataType() == DBEMPTY)){
    stringstream a;
    a << "RootDb::DbProcess: dbFieldIterator is a " << DbDataTypeNames[field->getDataType()] << " on a string process";
    throw PixDBException(a.str());
  }    

  switch (mode){
  case DBREAD:
    {    
      if(field->getDataType() == DBEMPTY){
	stringstream a;
	a << "RootDb::DbProcess: try to read an empty dbFieldIterator";
	throw PixDBException(a.str());
      }    
      unsigned int i, hi;
      s.str(field->m_myStringCont.Data());
      s >> i;
      s.clear();
      s.str(field->m_myStringCont.Data());
      s >> std::hex >> hi >> std::dec;
      if(hi!=0 && i==0) 
	retval = hi;
      else
	retval = i;
      return dbFieldIterator(new FieldIteratorWrapper(theField));}
    break;
  case DBCOMMIT:
    field->m_dataType = DBULINT;
    s << retval;
    s >> field->m_myStringCont;
    if(theField.m_parentRecord != 0){
      if(theField.m_parentRecord->findField(field->getName()) != theField.m_parentRecord->fieldEnd()){
	this->putRecord(theField.m_parentRecord);
	this->putField(field, theField.m_parentRecord);
      }
    }
    if(allocated && theField.m_field) delete theField.m_field;      
    return dbFieldIterator(new FieldIteratorWrapper(theField));
    break;
  default:
    stringstream a;
    a << "RootDb::DbProcess: mode not implemented uint" << mode;
    throw PixDBException(a.str());
    break;
  }
} // read or commit the field pointed by the iterator

dbFieldIterator RootDb::DbProcess(RootDbFieldIterator theField, enum DbAccessType mode, float& retval){
  std::vector<double> tmpvec;
  RootDbField* field = 0; bool allocated = false;
  if(*theField != 0){
    field = dynamic_cast<RootDbField*>(*theField);
    if(field == 0){
      stringstream a;
      a << "RootDb::DbProcess: dbFieldIterator is invalid, field " << dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number) << " is not present on this Db";
      throw PixDBException(a.str());
    }
  }
  else{
    if(theField.m_parentRecord != 0) {
      RootDbFieldIterator rf(theField.m_number, getFieldByDecName(dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number)), theField.m_parentRecord);
      allocated = true; theField = rf;
    }
    else{
      stringstream a;
      a << "RootDb::DbProcess: FLOAT dbFieldIterator is invalid, parent is zero ";
      throw PixDBException(a.str());
    }
    field = dynamic_cast<RootDbField*>(*theField);
    if(field == 0){
      stringstream a;
      a << "RootDb::DbProcess: dbFieldIterator is invalid, field " << dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number) << " is not present on this Db";
      throw PixDBException(a.str());
    }
  }
  if(!(field->getDataType() == DBFLOAT || field->getDataType() == DBEMPTY)){
    stringstream a;
    a << "RootDb::DbProcess: dbFieldIterator is a " << DbDataTypeNames[field->getDataType()] << " on a string process";
    throw PixDBException(a.str());
  }    
  
  switch (mode){
  case DBREAD:
    if(field->getDataType() == DBEMPTY){
      stringstream a;
      a << "RootDb::DbProcess: try to read an empty dbFieldIterator";
      throw PixDBException(a.str());
    }    
    retval = field->m_myFloatCont.At(0);
    return dbFieldIterator(new FieldIteratorWrapper(theField));
  case DBCOMMIT:
    field->m_dataType = DBFLOAT;
    field->m_myFloatCont[0] = retval;
    if(theField.m_parentRecord != 0){
      if(theField.m_parentRecord->findField(field->getName()) != theField.m_parentRecord->fieldEnd()){
	this->putRecord(theField.m_parentRecord);
	this->putField(field, theField.m_parentRecord);
      }
    }
    if(allocated && theField.m_field) delete theField.m_field;
    return dbFieldIterator(new FieldIteratorWrapper(theField));
    break;
  default:
    stringstream a;
    a << "RootDb::DbProcess: mode not implemented float " << mode;
    throw PixDBException(a.str());
    break;
  }
} // read or commit the field pointed by the iterator

dbFieldIterator RootDb::DbProcess(RootDbFieldIterator theField, enum DbAccessType mode, double& retval){
  std::vector<double> tmpvec;
  RootDbField* field = 0; bool allocated = false;
  if(*theField != 0){
    field = dynamic_cast<RootDbField*>(*theField);
    if(field == 0){
      stringstream a;
      a << "RootDb::DbProcess: dbFieldIterator is invalid, field " << dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number) << " is not present on this Db";
      throw PixDBException(a.str());
    }
  }
  else{
    if(theField.m_parentRecord != 0) {
      RootDbFieldIterator rf(theField.m_number, getFieldByDecName(dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number)), theField.m_parentRecord);
      allocated = true; theField = rf;
    }
    else{
      stringstream a;
      a << "RootDb::DbProcess: DOUBLE dbFieldIterator is invalid, parent is zero ";
      throw PixDBException(a.str());
    }
    field = dynamic_cast<RootDbField*>(*theField);
    if(field == 0){
      stringstream a;
      a << "RootDb::DbProcess: dbFieldIterator is invalid, field " << dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number) << " is not present on this Db";
      throw PixDBException(a.str());
    }
  }
  if(!(field->getDataType() == DBDOUBLE || field->getDataType() == DBEMPTY)){
    stringstream a;
    a << "RootDb::DbProcess: dbFieldIterator is a " << DbDataTypeNames[field->getDataType()] << " on a string process";
    throw PixDBException(a.str());
  }    

  
  switch (mode){
  case DBREAD:
    if(field->getDataType() == DBEMPTY){
      stringstream a;
      a << "RootDb::DbProcess: try to read an empty dbFieldIterator";
      throw PixDBException(a.str());
    }    
    retval = field->m_myDoubleCont.At(0);
    return dbFieldIterator(new FieldIteratorWrapper(theField));
    break;
  case DBCOMMIT:
    field->m_dataType = DBDOUBLE;
    field->m_myDoubleCont[0] = retval;
    if(theField.m_parentRecord != 0){
      if(theField.m_parentRecord->findField(field->getName()) != theField.m_parentRecord->fieldEnd()){
	this->putRecord(theField.m_parentRecord);
	this->putField(field, theField.m_parentRecord);
      }
    }    
    if(allocated && theField.m_field) delete theField.m_field;
    return dbFieldIterator(new FieldIteratorWrapper(theField));
    break;
  default:
    stringstream a;
    a << "RootDb::DbProcess: mode not implemented double" << mode;
    throw PixDBException(a.str());
    break;
  }
} // read or commit the field pointed by the iterator

dbFieldIterator RootDb::DbProcess(RootDbFieldIterator theField, enum DbAccessType mode, bool& retval){
  std::vector<double> tmpvec;
  RootDbField* field = 0; bool allocated = false;
  if(*theField != 0){
    field = dynamic_cast<RootDbField*>(*theField);
    if(field == 0){
      stringstream a;
      a << "RootDb::DbProcess: dbFieldIterator is invalid, field " << dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number) << " is not present on this Db";
      throw PixDBException(a.str());
    }
  }
  else{
    if(theField.m_parentRecord != 0) {
      RootDbFieldIterator rf(theField.m_number, getFieldByDecName(dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number)), theField.m_parentRecord);
      allocated = true; theField = rf;
    }
    else{
      stringstream a;
      a << "RootDb::DbProcess:BOOL dbFieldIterator is invalid, parent is zero ";
      throw PixDBException(a.str());
    }
    field = dynamic_cast<RootDbField*>(*theField);
    if(field == 0){
      stringstream a;
      a << "RootDb::DbProcess: dbFieldIterator is invalid, field " << dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number) << " is not present on this Db";
      throw PixDBException(a.str());
    }
  }
  if(!(field->getDataType() == DBBOOL || field->getDataType() == DBEMPTY)){
    stringstream a;
    a << "RootDb::DbProcess: dbFieldIterator is a " << DbDataTypeNames[field->getDataType()] << " on a string process";
    throw PixDBException(a.str());
  }    

  switch (mode){
  case DBREAD:
    if(field->getDataType() == DBEMPTY){
      stringstream a;
      a << "RootDb::DbProcess: try to read an empty dbFieldIterator";
      throw PixDBException(a.str());
    }    
    retval = field->m_myIntCont.At(0);
    return dbFieldIterator(new FieldIteratorWrapper(theField));
    break;
  case DBCOMMIT:
    field->m_dataType = DBBOOL;
    field->m_myIntCont[0] = retval;
    if(theField.m_parentRecord != 0){
      if(theField.m_parentRecord->findField(field->getName()) != theField.m_parentRecord->fieldEnd()){
	this->putRecord(theField.m_parentRecord);
	this->putField(field, theField.m_parentRecord);
      }
    }
    if(allocated && theField.m_field) delete theField.m_field;  
    return dbFieldIterator(new FieldIteratorWrapper(theField));
    break;
  default:
    stringstream a;
    a << "RootDb::DbProcess: mode not implemented bool" << mode;
    throw PixDBException(a.str());
    break;
  }
} // read or commit the field pointed by the iterator


dbFieldIterator RootDb::DbProcess(RootDbFieldIterator theField, enum DbAccessType mode, vector<int>& retval){
  std::vector<double> tmpvec;
  RootDbField* field = 0; bool allocated = false;
  if(*theField != 0){
    field = dynamic_cast<RootDbField*>(*theField);
    if(field == 0){
      stringstream a;
      a << "RootDb::DbProcess: dbFieldIterator is invalid, field " << dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number) << " is not present on this Db";
      throw PixDBException(a.str());
    }
  }
  else{
    if(theField.m_parentRecord != 0) {
      RootDbFieldIterator rf(theField.m_number, getFieldByDecName(dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number)), theField.m_parentRecord);
      allocated = true; theField = rf;
    }
    else{
      stringstream a;
      a << "RootDb::DbProcess:VECTOR INT dbFieldIterator is invalid, parent is zero ";
      throw PixDBException(a.str());
    }
    field = dynamic_cast<RootDbField*>(*theField);
    if(field == 0){
      stringstream a;
      a << "RootDb::DbProcess: dbFieldIterator is invalid, field " << dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number) << " is not present on this Db";
      throw PixDBException(a.str());
    }
  }
  if(!(field->getDataType() == DBVECTORINT || field->getDataType() == DBEMPTY)){
    stringstream a;
    a << "RootDb::DbProcess: dbFieldIterator is a " << DbDataTypeNames[field->getDataType()] << " on a string process";
    throw PixDBException(a.str());
  }    
  
  switch (mode){
  case DBREAD:
    if(field->getDataType() == DBEMPTY){
      stringstream a;
      a << "RootDb::DbProcess: try to read an empty dbFieldIterator";
      throw PixDBException(a.str());
    }    
    for(int i = 0; i < field->m_myIntCont.GetSize(); i++){
      retval.push_back(field->m_myIntCont.At(i));
    }
    return dbFieldIterator(new FieldIteratorWrapper(theField));
    break;
  case DBCOMMIT:
    uint ii;
    field->m_dataType = DBVECTORINT;
    field->m_myIntCont.Set(retval.size());
    for(ii = 0; ii < retval.size(); ii++){
      field->m_myIntCont[ii] = retval[ii];
    }
    if(theField.m_parentRecord != 0){
      if(theField.m_parentRecord->findField(field->getName()) != theField.m_parentRecord->fieldEnd()){
		this->putRecord(theField.m_parentRecord);
	this->putField(field, theField.m_parentRecord);
      }
    }
    if(allocated && theField.m_field) delete theField.m_field;
    return dbFieldIterator(new FieldIteratorWrapper(theField));
    break;
  default:
    stringstream a;
    a << "RootDb::DbProcess: mode not implemented vectorint" << mode;
    throw PixDBException(a.str());
    break;
  }
} // read or commit the field pointed by the iterator


dbFieldIterator RootDb::DbProcess(RootDbFieldIterator theField, enum DbAccessType mode, vector<bool>& retval){
  std::vector<double> tmpvec;
  RootDbField* field = 0; bool allocated = false;
  if(*theField != 0){
    field = dynamic_cast<RootDbField*>(*theField);
    if(field == 0){
      stringstream a;
      a << "RootDb::DbProcess: dbFieldIterator is invalid, field " << dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number) << " is not present on this Db";
      throw PixDBException(a.str());
    }
  }
  else{
    if(theField.m_parentRecord != 0) {
      RootDbFieldIterator rf(theField.m_number, getFieldByDecName(dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number)), theField.m_parentRecord);
      allocated = true; theField = rf;
    }
    else{
      stringstream a;
      a << "RootDb::DbProcess:VECTOR BOOL dbFieldIterator is invalid, parent is zero ";
      throw PixDBException(a.str());
    }
    field = dynamic_cast<RootDbField*>(*theField);
    if(field == 0){
      stringstream a;
      a << "RootDb::DbProcess: dbFieldIterator is invalid, field " << dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number) << " is not present on this Db";
      throw PixDBException(a.str());
    }
  }
  if(!(field->getDataType() == DBVECTORBOOL || field->getDataType() == DBEMPTY)){
    stringstream a;
    a << "RootDb::DbProcess: dbFieldIterator is a " << DbDataTypeNames[field->getDataType()] << " on a string process";
    throw PixDBException(a.str());
  }    

  switch (mode){
  case DBREAD:
    if(field->getDataType() == DBEMPTY){
      stringstream a;
      a << "RootDb::DbProcess: try to read an empty dbFieldIterator";
      throw PixDBException(a.str());
    }    
    for(int i = 0; i < field->m_myIntCont.GetSize(); i++){
      retval.push_back(field->m_myIntCont.At(i));
    }
    return dbFieldIterator(new FieldIteratorWrapper(theField));
    break;
  case DBCOMMIT:
    uint ii;
    field->m_dataType = DBVECTORBOOL;
    field->m_myIntCont.Set(retval.size());
    for(ii = 0; ii < retval.size(); ii++){
      field->m_myIntCont[ii] = retval[ii];
    }
    if(theField.m_parentRecord != 0){
      if(theField.m_parentRecord->findField(field->getName()) != theField.m_parentRecord->fieldEnd()){
	this->putRecord(theField.m_parentRecord);
	this->putField(field, theField.m_parentRecord);
      }
    }
    if(allocated && theField.m_field) delete theField.m_field;
    return dbFieldIterator(new FieldIteratorWrapper(theField));
    break;
  default:
    stringstream a;
    a << "RootDb::DbProcess: mode not implemented vectorbool" << mode;
    throw PixDBException(a.str());
    break;
  }
} // read or commit the field pointed by the iterator


dbFieldIterator RootDb::DbProcess(RootDbFieldIterator theField, enum DbAccessType mode, vector<float>& retval){
  std::vector<double> tmpvec;
  RootDbField* field = 0; bool allocated = false;
  if(*theField != 0){
    field = dynamic_cast<RootDbField*>(*theField);
    if(field == 0){
      stringstream a;
      a << "RootDb::DbProcess: dbFieldIterator is invalid, field " << dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number) << " is not present on this Db";
      throw PixDBException(a.str());
    }
  }
  else{
    if(theField.m_parentRecord != 0) {
      RootDbFieldIterator rf(theField.m_number, getFieldByDecName(dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number)), theField.m_parentRecord);
      allocated = true; theField = rf;
    }
    else{
      stringstream a;
      a << "RootDb::DbProcess:VECTOR FLOAT dbFieldIterator is invalid, parent is zero ";
      throw PixDBException(a.str());
    }
    field = dynamic_cast<RootDbField*>(*theField);
    if(field == 0){
      stringstream a;
      a << "RootDb::DbProcess: dbFieldIterator is invalid, field " << dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number) << " is not present on this Db";
      throw PixDBException(a.str());
    }
  }
  if(!(field->getDataType() == DBVECTORFLOAT || field->getDataType() == DBEMPTY)){
    stringstream a;
    a << "RootDb::DbProcess: dbFieldIterator is a " << DbDataTypeNames[field->getDataType()] << " on a string process";
    throw PixDBException(a.str());
  }    
  
  switch (mode){
  case DBREAD:
    if(field->getDataType() == DBEMPTY){
      stringstream a;
      a << "RootDb::DbProcess: try to read an empty dbFieldIterator";
      throw PixDBException(a.str());
    }    
    for(int i = 0; i < field->m_myFloatCont.GetSize(); i++){
      retval.push_back(field->m_myFloatCont.At(i));
    }
    return dbFieldIterator(new FieldIteratorWrapper(theField));
    break;
  case DBCOMMIT:
    uint ii;
    field->m_dataType = DBVECTORFLOAT;
    field->m_myFloatCont.Set(retval.size());
    for(ii = 0; ii < retval.size(); ii++){
      field->m_myFloatCont[ii] = retval[ii];
    }

    if(theField.m_parentRecord != 0){
      if(theField.m_parentRecord->findField(field->getName()) != theField.m_parentRecord->fieldEnd()){
	this->putRecord(theField.m_parentRecord);
	this->putField(field, theField.m_parentRecord);
      }
    }
    if(allocated && theField.m_field) delete theField.m_field;
    return dbFieldIterator(new FieldIteratorWrapper(theField));
    break;
  default:
    stringstream a;
    a << "RootDb::DbProcess: mode not implemented vectorfloat " << mode;
    throw PixDBException(a.str());
    break;
  }
} // read or commit the field pointed by the iterator

dbFieldIterator RootDb::DbProcess(RootDbFieldIterator theField, enum DbAccessType mode, vector<double>& retval){
  std::vector<double> tmpvec;
  RootDbField* field = 0; bool allocated = false;
  if(*theField != 0){
    field = dynamic_cast<RootDbField*>(*theField);
    if(field == 0){
      stringstream a;
      a << "RootDb::DbProcess: dbFieldIterator is invalid, field " << dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number) << " is not present on this Db";
      throw PixDBException(a.str());
    }
  }
  else{
    if(theField.m_parentRecord != 0) {
      RootDbFieldIterator rf(theField.m_number, getFieldByDecName(dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number)), theField.m_parentRecord);
      allocated = true; theField = rf;
    }
    else{
      stringstream a;
      a << "RootDb::DbProcess:VECTOR DOUBLE dbFieldIterator is invalid, parent is zero ";
      throw PixDBException(a.str());
    }
    field = dynamic_cast<RootDbField*>(*theField);
    if(field == 0){
      stringstream a;
      a << "RootDb::DbProcess: dbFieldIterator is invalid, field " << dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number) << " is not present on this Db";
      throw PixDBException(a.str());
    }
  }
  if(!(field->getDataType() == DBVECTORDOUBLE || field->getDataType() == DBEMPTY)){
    stringstream a;
    a << "RootDb::DbProcess: dbFieldIterator is a " << DbDataTypeNames[field->getDataType()] << " on a string process";
    throw PixDBException(a.str());
  }    

  switch (mode){
  case DBREAD:
    if(field->getDataType() == DBEMPTY){
      stringstream a;
      a << "RootDb::DbProcess: try to read an empty dbFieldIterator";
      throw PixDBException(a.str());
    }    
    for(int i = 0; i < field->m_myDoubleCont.GetSize(); i++){
      retval.push_back(field->m_myDoubleCont.At(i));
    }
    return dbFieldIterator(new FieldIteratorWrapper(theField));
    break;
  case DBCOMMIT:
    uint ii;
    field->m_dataType = DBVECTORDOUBLE;
    field->m_myDoubleCont.Set(retval.size());
    for(ii = 0; ii < retval.size(); ii++){
      field->m_myDoubleCont[ii] = retval[ii];
    }
    if(theField.m_parentRecord != 0){
      if(theField.m_parentRecord->findField(field->getName()) != theField.m_parentRecord->fieldEnd()){
	this->putRecord(theField.m_parentRecord);
	this->putField(field, theField.m_parentRecord);
      }
    }
    if(allocated && theField.m_field) delete theField.m_field;
    return dbFieldIterator(new FieldIteratorWrapper(theField));
    break;
  default:
    stringstream a;
    a << "RootDb::DbProcess: mode not implemented vector double" << mode;
    throw PixDBException(a.str());
    break;
  }
} // read or commit the field pointed by the iterator

dbRecordIterator RootDb::DbProcess(RootDbRecordIterator therecord, enum DbAccessType mode){ // read or commit (possibly recursively) the record pointed by iterator
  RootDbRecord* dbi = dynamic_cast<RootDbRecord*>(*therecord);
  //if(dbi == 0) {
  //    stringstream a;
  //  a << "RootDb::DbProcess: trying to process an non RootDbRecord";
  //  throw PixDBException(a.str());
  //}
  if(mode == PixDb::DBREAD){
    RootDbRecordIterator rf(therecord.m_number, getRecordByDecName(dynamic_cast<RootDbRecord*>(therecord.m_parentRecord)->getRecordDecName(therecord.m_number)), therecord.m_parentRecord);
    return dbRecordIterator(new RecordIteratorWrapper(rf));
  }
  else if (mode == DBCOMMITTREE){
    commitRootRecordReplaceTree(dbi);
    return dbRecordIterator(new RecordIteratorWrapper(therecord));
  }
  else if (mode == DBCOMMIT){
    putRecord(dbi);
    therecord.pointsTo(dbi);
    return dbRecordIterator(new RecordIteratorWrapper(therecord));
  }
  else{
    return  (*therecord)->recordEnd();
  }
}

dbFieldIterator RootDb::DbProcess(RootDbFieldIterator theField, enum DbAccessType mode){
  if(mode != PixDb::DBREAD){
    stringstream a;
    a << "RootDb::DbProcess: this call can only DBREAD a field!";
    throw PixDBException(a.str());
  }
  RootDbField* field = 0;
  if(theField.m_parentRecord != 0) {
    RootDbFieldIterator rf(theField.m_number, getFieldByDecName(dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number)), theField.m_parentRecord);
    if(theField.m_field){
      theField.m_field->copyField(rf);
      delete rf.m_field;
    }
    else theField = rf;
  }
  else{
    stringstream a;
    a << "RootDb::DbProcess:NO ARGUMENT dbFieldIterator is invalid, parent is zero ";
    throw PixDBException(a.str());
  }
  field = dynamic_cast<RootDbField*>(*theField);
  if(field == 0){
    stringstream a;
    a << "RootDb::DbProcess: dbFieldIterator is invalid, field " << dynamic_cast<RootDbRecord*>(theField.m_parentRecord)->getFieldDecName(theField.m_number) << " is not present on this Db";
      throw PixDBException(a.str());
  }
  
  return dbFieldIterator(new FieldIteratorWrapper(theField));
}


dbFieldIterator RootDb::DbProcess(DbField* theField, enum DbAccessType mode, bool& v){RootDbFieldIterator f(dynamic_cast<RootDbField*>(theField)); return RootDb::DbProcess(f,mode, v);} // read or commit the field pointed by the iterator
dbFieldIterator RootDb::DbProcess(DbField* theField, enum DbAccessType mode, vector<bool>& v){RootDbFieldIterator f(dynamic_cast<RootDbField*>(theField)); return RootDb::DbProcess(f,mode, v);}
dbFieldIterator RootDb::DbProcess(DbField* theField, enum DbAccessType mode, int& v){RootDbFieldIterator f(dynamic_cast<RootDbField*>(theField)); return RootDb::DbProcess(f,mode, v);}
dbFieldIterator RootDb::DbProcess(DbField* theField, enum DbAccessType mode, vector<int>& v){RootDbFieldIterator f(dynamic_cast<RootDbField*>(theField)); return RootDb::DbProcess(f,mode, v);}
dbFieldIterator RootDb::DbProcess(DbField* theField, enum DbAccessType mode, unsigned int & v){RootDbFieldIterator f(dynamic_cast<RootDbField*>(theField)); return RootDb::DbProcess(f,mode, v);}
dbFieldIterator RootDb::DbProcess(DbField* theField, enum DbAccessType mode, float& v){RootDbFieldIterator f(dynamic_cast<RootDbField*>(theField)); return RootDb::DbProcess(f,mode, v);}
dbFieldIterator RootDb::DbProcess(DbField* theField, enum DbAccessType mode, vector<float>& v){RootDbFieldIterator f(dynamic_cast<RootDbField*>(theField)); return RootDb::DbProcess(f,mode, v);}
dbFieldIterator RootDb::DbProcess(DbField* theField, enum DbAccessType mode, double& v){RootDbFieldIterator f(dynamic_cast<RootDbField*>(theField)); return RootDb::DbProcess(f,mode, v);}
dbFieldIterator RootDb::DbProcess(DbField* theField, enum DbAccessType mode, vector<double>& v){RootDbFieldIterator f(dynamic_cast<RootDbField*>(theField)); return RootDb::DbProcess(f,mode, v);}
dbFieldIterator RootDb::DbProcess(DbField* theField, enum DbAccessType mode, Histo& v){RootDbFieldIterator f(dynamic_cast<RootDbField*>(theField)); return RootDb::DbProcess(f,mode, v);}
dbFieldIterator RootDb::DbProcess(DbField* theField, enum DbAccessType mode, string& v){RootDbFieldIterator f(dynamic_cast<RootDbField*>(theField)); return RootDb::DbProcess(f,mode, v);}
DbRecord* RootDb::DbProcess(DbRecord* therecord, enum DbAccessType mode){RootDbRecordIterator r(dynamic_cast<RootDbRecord*>(therecord)); return *RootDb::DbProcess(r,mode);} // read or commit (possibly recursively) the record pointed by iterator
dbFieldIterator RootDb::DbProcess(DbField* theField, enum DbAccessType mode){RootDbFieldIterator f(dynamic_cast<RootDbField*>(theField)); return RootDb::DbProcess(f,mode);}


dbFieldIterator RootDb::DbProcess(dbFieldIterator fIterator, enum DbAccessType mode, bool& v){  
  FieldIteratorWrapper *w = dynamic_cast<FieldIteratorWrapper*>(fIterator.getWrapper());
  if(!w) {
    throw PixDBException("RootDbField::DbProcess: iterator wrapper is not FieldIteratorWrapper");
  }
  return RootDb::DbProcess(w->m_iter,mode, v);
} // read or commit the field pointed by the iterator

dbFieldIterator RootDb::DbProcess(dbFieldIterator fIterator, enum DbAccessType mode, vector<bool>& v){
  FieldIteratorWrapper *w = dynamic_cast<FieldIteratorWrapper*>(fIterator.getWrapper());
  if(!w) {
    throw PixDBException("RootDbField::DbProcess: iterator wrapper is not FieldIteratorWrapper");
  }
  return RootDb::DbProcess(w->m_iter,mode, v);
}

dbFieldIterator RootDb::DbProcess(dbFieldIterator fIterator, enum DbAccessType mode, int& v){  
  FieldIteratorWrapper *w = dynamic_cast<FieldIteratorWrapper*>(fIterator.getWrapper());
  if(!w) {
    throw PixDBException("RootDbField::DbProcess: iterator wrapper is not FieldIteratorWrapper");
  }
  return RootDb::DbProcess(w->m_iter,mode, v);
}

dbFieldIterator RootDb::DbProcess(dbFieldIterator fIterator, enum DbAccessType mode, vector<int>& v){  
  FieldIteratorWrapper *w = dynamic_cast<FieldIteratorWrapper*>(fIterator.getWrapper());
  if(!w) {
    throw PixDBException("RootDbField::DbProcess: iterator wrapper is not FieldIteratorWrapper");
  }
  return RootDb::DbProcess(w->m_iter,mode, v);
}

dbFieldIterator RootDb::DbProcess(dbFieldIterator fIterator, enum DbAccessType mode, unsigned int & v){  
  FieldIteratorWrapper *w = dynamic_cast<FieldIteratorWrapper*>(fIterator.getWrapper());
  if(!w) {
    throw PixDBException("RootDbField::DbProcess: iterator wrapper is not FieldIteratorWrapper");
  }
  return RootDb::DbProcess(w->m_iter,mode, v);
}

dbFieldIterator RootDb::DbProcess(dbFieldIterator fIterator, enum DbAccessType mode, float& v){  
  FieldIteratorWrapper *w = dynamic_cast<FieldIteratorWrapper*>(fIterator.getWrapper());
  if(!w) {
    throw PixDBException("RootDbField::DbProcess: iterator wrapper is not FieldIteratorWrapper");
  }
  return RootDb::DbProcess(w->m_iter,mode, v);
}

dbFieldIterator RootDb::DbProcess(dbFieldIterator fIterator, enum DbAccessType mode, vector<float>& v){  
  FieldIteratorWrapper *w = dynamic_cast<FieldIteratorWrapper*>(fIterator.getWrapper());
  if(!w) {
    throw PixDBException("RootDbField::DbProcess: iterator wrapper is not FieldIteratorWrapper");
  }
  return RootDb::DbProcess(w->m_iter,mode, v);
}

dbFieldIterator RootDb::DbProcess(dbFieldIterator fIterator, enum DbAccessType mode, double& v){  
  FieldIteratorWrapper *w = dynamic_cast<FieldIteratorWrapper*>(fIterator.getWrapper());
  if(!w) {
    throw PixDBException("RootDbField::DbProcess: iterator wrapper is not FieldIteratorWrapper");
  }
  return RootDb::DbProcess(w->m_iter,mode, v);
}

dbFieldIterator RootDb::DbProcess(dbFieldIterator fIterator, enum DbAccessType mode, vector<double>& v){  
  FieldIteratorWrapper *w = dynamic_cast<FieldIteratorWrapper*>(fIterator.getWrapper());
  if(!w) {
    throw PixDBException("RootDbField::DbProcess: iterator wrapper is not FieldIteratorWrapper");
  }
  return RootDb::DbProcess(w->m_iter,mode, v);
}

dbFieldIterator RootDb::DbProcess(dbFieldIterator fIterator, enum DbAccessType mode, Histo& v){  
  FieldIteratorWrapper *w = dynamic_cast<FieldIteratorWrapper*>(fIterator.getWrapper());
  if(!w) {
    throw PixDBException("RootDbField::DbProcess: iterator wrapper is not FieldIteratorWrapper");
  }
  return RootDb::DbProcess(w->m_iter,mode, v);
}

dbFieldIterator RootDb::DbProcess(dbFieldIterator fIterator, enum DbAccessType mode, string& v){  
  FieldIteratorWrapper *w = dynamic_cast<FieldIteratorWrapper*>(fIterator.getWrapper());
  if(!w) {
    throw PixDBException("RootDbField::DbProcess: iterator wrapper is not FieldIteratorWrapper");
  }
  return RootDb::DbProcess(w->m_iter,mode, v);
}

dbRecordIterator RootDb::DbProcess(dbRecordIterator rIterator, enum DbAccessType mode){  
  RecordIteratorWrapper *w = dynamic_cast<RecordIteratorWrapper*>(rIterator.getWrapper());
  if(!w) {
    throw PixDBException("RootDb::DbProcess: iterator wrapper is not RecordIteratorWrapper");
  }
  return RootDb::DbProcess(w->m_iter,mode);
} // read or commit (possibly recursively) the record pointed by iterator

dbFieldIterator RootDb::DbProcess(dbFieldIterator fIterator, enum DbAccessType mode){
  FieldIteratorWrapper *w = dynamic_cast<FieldIteratorWrapper*>(fIterator.getWrapper());
  if(!w) {
    throw PixDBException("RootDbField::DbProcess: iterator wrapper is not FieldIteratorWrapper");
  }
  return RootDb::DbProcess(w->m_iter,mode);
}

DbRecord* RootDb::DbFindRecordByName(const string& name) {
  DbRecord* retvalue = 0;
  TDirectory* dir = gDirectory; // copy locally the current directory
  m_theFile->cd(); // go to the root directory
  retvalue = getRecordByDecName(name);
  if(retvalue == 0){
    stringstream a;
    a << "RootDb::DbFindRecordByName(): record" << name.c_str() << " not found";
    throw PixDBException(a.str());
  }
  dir->cd();
  return retvalue;
} // find a record by its name, returning a vector of records which fulfill the find requests

DbField* RootDb::DbFindFieldByName(const string& name) {
  DbField* retvalue = 0; 
  TDirectory* dir = gDirectory; // copy locally the current directory
  m_theFile->cd(); // go to the root directory
  retvalue = getFieldByDecName(name);
  if(retvalue == 0){
    stringstream a;
    a << "RootDb::DbFindFieldByName(): field" << name.c_str() << " not found";
    throw PixDBException(a.str());
  }
  dir->cd();
  return retvalue;
} // find a data field by its name, returning a vector of fields which fulfill the find requests

RootDb::RootDb(string namefile, string mode, string tdaqNameFile){	
  // create mutex
  std::cout << "RootDb::RootDb" << namefile<< " " << mode << " " << tdaqNameFile << std::endl;
  if(tdaqNameFile == "") tdaqNameFile = namefile;
  //create a RootDb from
  // a) a .cfg file used as template
  // b) a 
  char absolutePath[PATH_MAX];
  char dirNamePath[PATH_MAX];
  char baseNamePath[PATH_MAX];
  string dirNamePathret;
  string baseNamePathret;
  strncpy(dirNamePath, namefile.c_str(), namefile.size()+1);
  strncpy(baseNamePath, namefile.c_str(), namefile.size()+1);
  dirNamePathret = dirname(dirNamePath);
  //std::cout << "dirname =  " << dirNamePathret.c_str() << std::endl;
  baseNamePathret = basename(baseNamePath);
  //std::cout << "basename =  " << baseNamePathret.c_str() << std::endl;
  char* retvalue = realpath(dirNamePathret.c_str(), absolutePath);
  //std::cout << "absolutepath =  " << absolutePath << std::endl;
  if(retvalue == 0){
    stringstream a;
    a << "RootDb::RootDb: error in namefile " << namefile.c_str() << " not resolved to an absolute name. realpath returned: " << errno;
    throw PixDBException(a.str());
  }
  std::string absolutepathstring = absolutePath; absolutepathstring += "/";
  std::string basepathstring = baseNamePathret;
  namefile = absolutepathstring + basepathstring;
  //std::cout << "realpath on namefile gave:" << namefile << std::endl;

  m_mutex.lock();

  try{
    if (mode == "NEW" || mode == "RECREATE"){
      // part 1: opening the root file
      m_theFile = new TFile(namefile.c_str(), mode.c_str());
      std::cout << "name of the TFile: " << m_theFile->GetName() << std::endl;
      m_theFileName = namefile;
      m_theOriginalFileName = "__NONAME__";
      if(m_theFile->IsZombie()) 
	throw PixDBException("RootDb constructor: Application description file not open as good");
      m_theFile->cd();
      std::pair<std::string, TFile*> insPair(namefile, m_theFile);
      m_theFileHandlers.insert(insPair);
      
      // part 2: creating the root record
      string rootDecName = m_theFileName + ":/rootRecord;1";
      RootDbRecord* rootrec = dynamic_cast<RootDbRecord*>(makeRecord("rootRecord","/")); 
      rootrec->m_name = "";
      rootrec->m_decName = rootDecName;
      // part 3: committing the newly created rootrecord into the database
      m_mutex.unlock();
      this->DbProcess(rootrec,DBCOMMIT);
      m_mutex.lock();
      // part 4: closing and reopening in "UPDATE" mode the newly created database
      m_theFile->Close();
      m_theFileHandlers.clear();
      m_theFile = new TFile(namefile.c_str(),"UPDATE");
      if(m_theFile->IsZombie()) 
      throw PixDBException("RootDb constructor: Application description file not open as good");
      std::pair<std::string, TFile*> insPair2(namefile, m_theFile);
      m_theFileHandlers.insert(insPair2);
    }
    else if (mode == "UPDATE" || mode == "READ"){
      // part 1: opening the root file in the mode specified by the user
      m_theFile = new TFile(namefile.c_str(), mode.c_str());
      m_theFileName = namefile;
      m_theOriginalFileName = "__NONAME__";
      if(m_theFile->IsZombie()) 
	throw PixDBException("RootDb constructor: Application description file not open as good");
      std::pair<std::string, TFile*> insPair2(namefile, m_theFile);
      m_theFileHandlers.insert(insPair2);
    }
    // access to the root record in order to know the original file name
    m_mutex.unlock();
    DbRecord* tmpRec = readRootRecord();
    m_mutex.lock();
    delete tmpRec;
    m_theFile->cd();
    gDirectory = m_theFile;
  }
  catch(PixDBException& ex){
    m_mutex.unlock();
    // ex.what(std::cerr);
    throw ex;
  }
  catch(...){
    std::cerr << "unhandled non-PixDBException in RootDb contructor, returning" << std::endl;
    m_mutex.unlock();
    throw;
  }
  m_mutex.unlock();
}

RootDb::~RootDb(){
  m_mutex.lock();
  try{
    if(!(m_theFile->IsZombie()))
      m_theFile->Close();
    std::map<std::string, TFile*>::iterator i;
    for(i = m_theFileHandlers.begin(); i != m_theFileHandlers.end(); i++){
      if(!((*i).second->IsZombie()))
	(*i).second->Close();
    }
  }
  catch(PixDBException& ex){
    m_mutex.unlock();
    // ex.what(std::cerr);
  }
  catch(...){
    std::cerr << "unhandled non-PixDBException in RootDb destructor, returning" << std::endl;
    m_mutex.unlock();
  }
  m_mutex.unlock();

}

//#endif
