#ifndef _PIXLIB_PIXFEI1CONFIG
#define _PIXLIB_PIXFEI1CONFIG

#include <map>
#include <string>
#include <memory>

#include "PixFe/PixFeConfig.h"
#include "Fei3.h"

namespace PixLib {
	
  class Config;
  template<class T> class ConfMask;
  class PixDbInterface;
  class DbRecord;

class PixFeI3Config : public PixFeConfig {

  public:
    PixFeI3Config(PixDbInterface *db, DbRecord *dbRecord, std::string name, int number);
    PixFeI3Config(PixDbServerInterface *dbServer, const std::string& dom, const std::string& tag,
            std::string name, const std::string& configName, int number);

    PixFeI3Config(const PixFeI3Config& c);
    virtual ~PixFeI3Config() = default;
    PixFeI3Config& operator= (const PixFeI3Config& c); //! Assignment operator

    //! Config object accessor
    Config& config() final {return *m_conf;}

    int number() final {return m_index;} // Return FE number

    void writeGlobRegister(Fei3::GlobReg reg, int value); // Write a value in the Global Register mem copy
    int& readGlobRegister(Fei3::GlobReg reg);             // Read a value from the Global Register mem copy
    ConfMask<bool>& readPixRegister(Fei3::PixReg reg);              // Read a value from the Pixel Register mem copy
    ConfMask<unsigned short int>& readTrim(Fei3::PixReg reg);              // Read a value from the Trim mem copy

    void loadConfig(DbRecord *dbRecord) final;   // Read the config from DB
    void saveConfig(DbRecord *dbRecord) final;   // Save the config to DB
    
    void dump(std::ostream &out) final; // Dump FE config info to output stream
    
  protected:
    std::string m_name; // FE name
    std::string m_className = "PixFeI2"; // FE derived class name

    static constexpr int nRow = Fei3::nPixRow;
    static constexpr int nCol = Fei3::nPixCol;

    std::unique_ptr<Config> m_conf {};
    std::array<int, Fei3::nGlobRegs> m_globRegisters {}; // FE Global register values
    std::array<ConfMask<bool>, Fei3::nPixRegs> m_pixRegisters {}; // FE Pixel register values
    std::array<ConfMask<unsigned short int>, Fei3::nPixRegs> m_trims {}; // FE Trim values

    void setupData(); // Setup FE configuration default data
    void setupConfigObject(); // Setup FE configuration block
  };
}
#endif
