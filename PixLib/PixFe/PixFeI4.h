#ifndef _PIXLIB_PIXFEI4
#define _PIXLIB_PIXFEI4

#include <vector>
#include <string>
#include "PixFe.h"
#include "Fei4.h"
#include "PixFeI4Config.h"

namespace PixLib {
  
  class Config;
  template<class T> class ConfMask;
  class PixModule;
  class PixDbInterface;
  class DbRecord;
  class PixDbServerInterface;
  
  class PixFeI4 : public PixFe {

    friend class PixModule;
		
  public:
    PixFeI4(DbRecord *dbRecord, PixModule *mod, std::string name, int number); // Constructor
    PixFeI4(PixDbServerInterface *dbServer, PixModule *mod, std::string dom, std::string tag, std::string name, int number); // Constructor

    virtual ~PixFeI4() = default; // Destructor

      Config &config();                                      // Configuration object accessor
      Config &config(std::string configName);                // Alternate configuration objects accessor
      PixFeI4Config &feConfig();                               // Configuration object accessor
      PixFeI4Config &feConfig(std::string configName);         // Alternate configuration objects accessor

      int number();                                          // Return FE number

      void writeGlobRegister(Fei4::GlobReg reg, int value); // Write a value in the Global Register mem copy
      int& readGlobRegister(Fei4::GlobReg reg);
      void writeGlobRegister(Fei4::MergedGlobReg reg, int value); // Write a value in the Global Register mem copy
      int& readGlobRegister(Fei4::MergedGlobReg reg);
      ConfMask<bool>& readPixRegister(Fei4::PixReg reg);              // Read a value from the Pixel Register mem copy
      ConfMask<unsigned short int>& readTrim(Fei4::PixReg reg);              // Read a value from the Trim mem copy

      bool restoreConfig(std::string configName = "INITIAL"); //! Restore a configuration from map (calls configure)
      void storeConfig(std::string configName);               //! Store a configuration into map
      void deleteConfig(std::string configName);              //! Remove a configuration from the map
      void loadConfig(DbRecord *dbr);                         //! read a configuration from the DB
      void saveConfig(DbRecord *dbr);                         //! write the current configuration to the DB

      void dump(std::ostream &out); // Dump FE info to output stream
      void setVcal(float charge, bool Chigh){};

      bool isFeI4() const {return true;};
  protected:
      // Pointer to corresponding db record
      DbRecord *m_dbRecord;

      // Pointer to parent PixModule
      PixModule *m_module;
      // FE info
      std::string m_name;

      std::unique_ptr<PixFeI4Config> m_data;                           // Current FE configuration
      std::map<std::string, std::unique_ptr<PixFeI4Config>> m_configs; // Stored FE configurations
  };
}
#endif
