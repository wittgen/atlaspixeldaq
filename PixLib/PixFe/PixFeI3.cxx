#include <math.h>

#include "Config/Config.h"
#include "PixDbInterface/PixDbInterface.h"
#include "PixController/PixController.h"
#include "PixModule/PixModule.h"
#include "PixFe/PixFeConfig.h"
#include "PixFe/PixFeI3Config.h"
#include "PixFe/PixFeI3.h"

// temporary place, should go somewhere more general
int solveCubic(double par[], double *res){
  if(par[3]==0 && par[2]==0 && par[1]==0 ) return 0;

  if(par[3]==0 && par[2]==0 ){
    double tmp1 = -par[0]/par[1];
    res[0] = tmp1;
    return 1;
  }

  if(par[3]==0){
    double tmp1 = par[0]/par[2]-par[1]*par[1]/(4*par[2]*par[2]);
    if(tmp1<0) return 0;
    res[0] = sqrt(tmp1)-par[1]/2/par[2];
    res[1] = -sqrt(tmp1)-par[1]/2/par[2];
    return 2;
  }

  double p,q,D;

  double a = par[2]/par[3];
  double b = par[1]/par[3];
  double c = par[0]/par[3];

  p = b - pow(a,2)/3;
  q = c + 2*pow(a,3)/27 - a*b/3;

  D = q*q/4 + p*p*p/27;

  if(D<-1e-10){
    double tmp1 = sqrt(-p/3), tmp2 = acos(-q/2*sqrt(-27/p/p/p));
    double pi=3.1415926535897932384626433832795;
    res[0] =  2*tmp1*cos(tmp2/3) - a/3;
    res[1] = -2*tmp1*cos(tmp2/3+pi/3) - a/3;
    res[2] = -2*tmp1*cos(tmp2/3-pi/3) - a/3;
    return 3;
  } else if(D>1e-10){
    double tmp1, tmp2;
    tmp1 = -q/2+sqrt(D);
    if(tmp1<0) 
      tmp1 = -pow(-tmp1,0.33333333333);
    else
      tmp1 = pow(tmp1,.33333333333);
    tmp2 = -q/2-sqrt(D);
    if(tmp2<0) 
      tmp2 = -pow(-tmp2,0.3333333333);
    else
      tmp2 = pow(tmp2,.33333333);
    res[0] = tmp1 + tmp2 - a/3;
    return 1;
  } else{
    if(p==q){
      res[0] = 0;
      return 1;
    } else{
      res[0] = -pow((4*q),.33333333)-b/3/a;
      res[1] = pow((q/2),.33333333)-a/3;
      return 2;
    }
  }
  
}

using namespace PixLib;

PixFeI3::PixFeI3(DbRecord *dbRecord, PixModule *mod, std::string name, int number) :
m_dbRecord(dbRecord), m_module(mod), m_name(name) {
  // Create FE config
  m_data = std::make_unique<PixFeI3Config>(m_dbRecord->getDb(), m_dbRecord, m_name, number);
}

PixFeI3::PixFeI3(PixDbServerInterface *dbServer, PixModule *mod, std::string dom, std::string tag, std::string name, int number) {
  // Members initialization
  m_dbRecord = nullptr;
  m_module = mod;
  m_name = name;
  m_data = nullptr;

  // Create FE config
  m_data = std::make_unique<PixFeI3Config>(dbServer, dom, tag, m_name, m_module->moduleName(), number);
}

Config &PixFeI3::config() {
  return m_data->config();
}
Config &PixFeI3::config(std::string configName) {
  if (m_configs.find(configName) != m_configs.end()) {
    return m_configs[configName]->config();
  }
  throw std::runtime_error("Config "+configName+" for PixFeI3 not found in map");
  return m_data->config();
}
PixFeI3Config& PixFeI3::feConfig() {
  return *m_data.get();
}
PixFeI3Config &PixFeI3::feConfig(std::string configName) {
  if (m_configs.find(configName) != m_configs.end()) {
    return *m_configs[configName].get();
  }
  throw std::runtime_error("PixFeI3Config "+configName+" for PixFeI3 not found in map");
  return *m_data.get();
} 

//! Return FE number
int PixFeI3::number() {
  return m_data->number();
}

void PixFeI3::writeGlobRegister(Fei3::GlobReg reg, int value) {
  m_data->writeGlobRegister(reg, value);
}
int& PixFeI3::readGlobRegister(Fei3::GlobReg reg) {
  return m_data->readGlobRegister(reg);
}
ConfMask<bool>& PixFeI3::readPixRegister(Fei3::PixReg reg) {
  return m_data->readPixRegister(reg);
}
ConfMask<unsigned short int>& PixFeI3::readTrim(Fei3::PixReg reg) {
  return m_data->readTrim(reg);
}

//! Restore a configuration from map
bool PixFeI3::restoreConfig(std::string configName /*= "INITIAL"*/) {
  if (auto it = m_configs.find(configName); it != m_configs.end()) {
    m_data = std::move(m_configs.extract(it).mapped());
    return true;
  }
  return false;
}

//! Store a configuration into map
void PixFeI3::storeConfig(std::string configName) {
  m_configs[configName] = std::make_unique<PixFeI3Config>(*m_data.get());
}

//! Remove a configuration from the map
void PixFeI3::deleteConfig(std::string configName) {
  if (m_configs.find(configName) != m_configs.end()) {
    m_configs.erase(configName);
  }
}

// ------ Interfacing the database ------
// This is code shared with PixFei3, might be worth refactoring
void PixFeI3::loadConfig(DbRecord *dbRecord) {
  m_data->loadConfig(dbRecord);
}

void PixFeI3::saveConfig(DbRecord *dbRecord) {
  m_data->saveConfig(dbRecord);
}

void PixFeI3::dump(std::ostream &out) {
  m_data->dump(out);
}

//Set Vcal and the capacity
void PixFeI3::setVcal(float charge, bool Chigh) {
  Config &feconf = m_data->config();
  if(feconf["Misc"].name()!="__TrashConfGroup__"){
    double Cval, par[4], result[3];
    if(Chigh)
      Cval = (double)((ConfFloat&)feconf["Misc"]["CInjHi"]).value();
    else
      Cval = (double)((ConfFloat&)feconf["Misc"]["CInjLo"]).value();
    char vcalstr[30];
    for(int k=0;k<4;k++){
      sprintf(vcalstr,"VcalGradient%d",k);
      par[k] = (double)((ConfFloat&)feconf["Misc"][vcalstr]).value();
    }
    par[0] -= (double)charge*0.160218/Cval;
    int myval = -1;
    int nsol = solveCubic(par,result);
    int iV;
    for(iV=0;iV<nsol;iV++){
      //	      printf("%lf\n",result[iV]);
      if(result[iV]>0 && result[iV]<1023)
	myval = (int)(result[iV]+0.5);
    }
    Config *subcfg=0;
    for(unsigned int j=0; j<feconf.subConfigSize(); j++){
      if(feconf.subConfig(j).name()=="GlobalRegister_0"){
	subcfg = &(feconf.subConfig(j));
	break;
      }
    }
    if(subcfg==0){
      std::cerr << "FE " << m_data->number() << " had no globreg config" << std::endl;
    } else{
      Config &globreg = *subcfg;
      if(myval>0 && globreg.name()!="__TrashConfGroup__" &&
	 globreg["GlobalRegister"].name()!="__TrashConfGroup__") {
	ConfInt& myco = (ConfInt&)globreg["GlobalRegister"]["DAC_VCAL"];
	*((int *)myco.m_value) = (int)myval;
      }
    }
  }
}
