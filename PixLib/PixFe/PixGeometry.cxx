#include "PixFe/PixGeometry.h"

namespace PixLib {
  PixGeometry::PixGeometry(int row,int col) {  // guess type from input 
    if(row==320  && col==144) {init(FEI2_MODULE); return;}
    if(row==160  && col==18)  {init(FEI2_CHIP); return ;}
    if(row==336  && col==80)  {init(FEI4_CHIP); return ;}
    if(row==336  && col==160) {init(FEI4_MODULE); return ;}
    init(INVALID_MODULE);
    }

  PixGeometry::PixGeometry(enum pixelType type) {
    init(type);
  }
  PixCoord PixGeometry::coord(int chip,int col,int row) {
      int pixel=chip*m_nChipPixel+row+col*m_nChipRow;
      int x,y;
      int nFeRows = m_nRow/m_nChipRow;
      if(chip<m_nChip/nFeRows || m_nChip==1) {
	x=chip*m_nChipCol+col;
	y=row;
      } else {
	x=(m_nChipCol-1-col + m_nChipCol*(m_nChip-1-chip));
	y=m_nRow-1-row;
      }
      
      PixLib::PixCoord result(chip,col,row,pixel,x,y);
      return result;
    }
  PixCoord PixGeometry::coord(int x, int y) {
      int col,row,chip;
      if(y<m_nChipRow){ // chips 0-7
	row  = y;
	if(m_nChip==1) {
	  col=x;
	  chip=0;
	} else {
	  col  = x%m_nChipCol;
	  chip = x/m_nChipCol;
	}
      } else{
	row  = m_nRow-1- y;
	col  = m_nChipCol-1  - x%m_nChipCol;
	chip = m_nChip-1 - x/m_nChipCol;
      }
      int pixel=chip*m_nChipPixel+row+col*m_nChipRow;
      //      std::cout << m_nChipRow << std::endl;
      PixCoord result(chip,row,col,pixel,x,y);
      return result;
  }   
  void PixGeometry::init(enum pixelType type) {
 
    /** assume that FE-I2 and FE-I3
     * are technically the same */
    if(type==FEI3_MODULE)
      type=FEI2_MODULE;
    if(type==FEI3_CHIP)
      type=FEI2_CHIP;

    if(type==FEI2_MODULE) {  
      m_nRow=320;
      m_nCol=144;
      m_nChip=16;
      m_nChipRow=160;
      m_nChipCol=18;
      m_pixType=type;
      m_nChipPixel=2880;
      return;
    } 
    if(type==FEI2_CHIP) {  
      m_nRow=160;
      m_nCol=18;
      m_nChip=1;
      m_nChipRow=160;
      m_nChipCol=18;
      m_pixType=type;
      m_nChipPixel=2880;
      return;
    } 
    if(type==FEI4_MODULE) {
      m_nRow=336;
      m_nCol=160;
      m_nChip=2;
      m_nChipRow=336;
      m_nChipCol=80;
      m_pixType=type;
      m_nChipPixel=26880;
      return ;
    } 
    if(type==FEI4_CHIP) {
      m_nRow=336;
      m_nCol=80;
      m_nChip=1;
      m_nChipRow=336;
      m_nChipCol=80;
      m_pixType=type;
      m_nChipPixel=26880;
      return ;
    } 
    m_pixType=INVALID_MODULE;
    m_nRow=0;
    m_nCol=0;
    m_nChip=0;
    m_nChipRow=0;
    m_nChipCol=0;
    m_nChipPixel=0;
  }
}
