#ifndef __PIXGEOMETRY_H__
#define __PIXGEOMETRY_H__
#include <ostream>
namespace PixLib {
  class PixCoord {
  public:
    PixCoord(int chip,int row,int col,int pixel,int x, int y):m_chip(chip),m_row(row),m_col(col),m_pixel(pixel),m_x(x),m_y(y) {}
    int row() {return m_row;}
    int col() {return m_col;}
    int chip() {return m_chip;}
    int pixel() {return m_pixel;}
    int x() {return m_x;}
    int y() {return m_y;}
    friend std::ostream &operator<<(std::ostream &stream, PixCoord coord) {
      stream << "chip= "<<coord.m_chip<< " row="<<coord.m_row<<" col="<<coord.m_col<<" pixel="<<coord.m_pixel<<" x="<<coord.m_x<<" y="<<coord.m_y; return stream;
    }
  private:
    int m_chip;
    int m_row;
    int m_col;
    int m_pixel;
    int m_x;
    int m_y;
  };
  
  class PixGeometry {
  public:

    enum pixelType { INVALID_MODULE=-1,FEI2_MODULE=1,FEI2_CHIP=2,FEI4_MODULE=3,FEI4_CHIP=4,FEI3_MODULE=5,FEI3_CHIP=6 };

    int const nRow() {return m_nRow;}
    int const nCol() {return m_nCol;}
    int const nChip() {return m_nChip;}
    int const nChipRow() {return m_nChipRow;}
    int const nChipCol() {return m_nChipCol;}
    int const nChipPixel() {return m_nChipPixel;}
    pixelType const pixType() {return m_pixType;}

    PixGeometry(int row,int col);
    PixGeometry(pixelType type);
    PixCoord coord(int chip,int col,int row);
    PixCoord coord(int x, int y);

    // just a test function
    bool test() {return true;}

  private:    
    pixelType m_pixType;
    int m_nRow;
    int m_nCol;
    int m_nChip;
    int m_nChipCol;
    int m_nChipPixel;
    int m_nChipRow;
    void init(pixelType);
  };

} 
#endif

