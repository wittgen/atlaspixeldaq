#ifndef _PIXLIB_PIXFE
#define _PIXLIB_PIXFE

#include <vector>
#include <string>

#include "PixModule/PixModule.h"
#include "PixFe/PixFeConfig.h"

namespace PixLib {
  
  class Bits;
  class Config;
  template<class T> class ConfMask;
  class PixModule;

  class PixFe {

    friend class PixModule;
  public:

    virtual ~PixFe() = default; // Destructor
    virtual Config &config()=0;                                // Configuration object accessor
    virtual Config &config(std::string configName) = 0;        // Alternate configuration objects accessor
    virtual PixFeConfig &feConfig()=0;                         // Configuration object accessor
    virtual PixFeConfig &feConfig(std::string configName) = 0; // Alternate configuration objects accessor

    virtual int  number()=0; // Return FE number

    virtual bool restoreConfig(std::string configName = "INITIAL")=0; // Restore a configuration from map
    virtual void storeConfig(std::string configName)=0;               // Store a configuration into map
    virtual void deleteConfig(std::string configName)=0;              // Remova a config from the map

    virtual void loadConfig(DbRecord *dbr)=0;                         //! read a configuration from the DB
    virtual void saveConfig(DbRecord *dbr)=0;                         //! write the current configuration to the DB

    virtual void dump(std::ostream &out)=0; // Dump FE info to output stream
    virtual void setVcal(float charge, bool CHigh)=0;

    virtual bool isFeI4() const = 0;
  };
}
#endif
