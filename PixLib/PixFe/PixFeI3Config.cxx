//! PixFeConfig implementation for FE-I3

#include "Config/Config.h"
#include "PixDbInterface/PixDbInterface.h"
#include "PixFe/PixFeConfig.h"
#include "PixFe/PixFeI3Config.h"

using namespace PixLib;

PixFeI3Config::PixFeI3Config(PixDbInterface *db, DbRecord *dbRecord, std::string name, int number) :
PixFeConfig(db, dbRecord, number, number), m_name(std::move(name)) {
  setupData();
  setupConfigObject();
  // Load initial config from database
  if(m_dbN && m_dbRecord) loadConfig(m_dbRecord);
}

PixFeI3Config::PixFeI3Config(PixDbServerInterface *dbServer, const std::string& dom, const std::string& tag,
        std::string name, const std::string& configName, int number):
        PixFeConfig(nullptr, nullptr, number, number),
        m_name(std::move(name)) {
  setupData();
  setupConfigObject();
  // Load initial config from database
  std::ostringstream str;
  str << configName << "|PixFe_" << number;
  if(!m_conf->read(dbServer, dom, tag, str.str()))
    std::cout << "PixFeI3Config: problem in reading configuration " << str.str() << std::endl;
}

PixFeI3Config::PixFeI3Config(const PixFeI3Config &rhs) : PixFeConfig(rhs) {
  m_name = rhs.m_name;
  m_className = rhs.m_className;

  m_globRegisters = rhs.m_globRegisters;
  m_pixRegisters = rhs.m_pixRegisters;
  m_trims = rhs.m_trims;

  setupConfigObject();
}

PixFeI3Config &PixFeI3Config::operator=(const PixFeI3Config &c) {
  if (this != &c) {
    PixFeConfig::operator=(c);
    m_name = c.m_name;
    m_className = c.m_className;

    m_globRegisters = c.m_globRegisters;
    m_pixRegisters = c.m_pixRegisters;
    m_trims = c.m_trims;

    setupConfigObject();
  }
  return *this;
}

void PixFeI3Config::writeGlobRegister(Fei3::GlobReg reg, int value) {
  m_globRegisters[static_cast<int>(reg)] = value;
}
int& PixFeI3Config::readGlobRegister(Fei3::GlobReg reg) {
  return m_globRegisters[static_cast<int>(reg)];
}
ConfMask<bool>& PixFeI3Config::readPixRegister(Fei3::PixReg reg) {
  return m_pixRegisters[static_cast<int>(reg)];
}
ConfMask<unsigned short int>& PixFeI3Config::readTrim(Fei3::PixReg reg) {
  return m_trims[static_cast<int>(reg)];
}

void PixFeI3Config::loadConfig(DbRecord *dbRecord) {
  std::cout << "PixFeI3Config:loadConfig(DbRecord) called for " << m_conf->m_confName << "/" <<  m_conf->m_tag << "/" << m_conf->m_pendingTag << "/" << m_conf->m_revision << "\n";
  m_conf->read(dbRecord);
}

void PixFeI3Config::saveConfig(DbRecord *dbRecord) {
  m_conf->write(dbRecord);
}

void PixFeI3Config::dump(std::ostream &out) {
  // TODO All of this has to be reimplemented
}

void PixFeI3Config::setupData() {

  m_globRegisters.fill(0);

  m_pixRegisters[static_cast<int>(Fei3::PixReg::HitBus)] = ConfMask<bool>(nCol, nRow, true, true);
  m_pixRegisters[static_cast<int>(Fei3::PixReg::Select)] = ConfMask<bool>(nCol, nRow, true, true);
  m_pixRegisters[static_cast<int>(Fei3::PixReg::Preamp)] = ConfMask<bool>(nCol, nRow, true, true);
  m_pixRegisters[static_cast<int>(Fei3::PixReg::Enable)] = ConfMask<bool>(nCol, nRow, true, true);

  m_trims[static_cast<int>(Fei3::PixReg::TDAC)] = ConfMask<unsigned short int>(nCol, nRow, 0xff, 0x7f);
  m_trims[static_cast<int>(Fei3::PixReg::FDAC)] = ConfMask<unsigned short int>(nCol, nRow, 0x7, 0x3);
}

void PixFeI3Config::setupConfigObject() {
  // Create the Config object
  std::stringstream configName;
  configName << "PixFe_"<<m_index<<"/PixFe";
  m_conf = std::make_unique<Config>(configName.str());

  Config &conf = *m_conf;

  //Idicates that no other Config owns this config, even if we link it in!
  conf.m_ownership = false;

  // Group class info
  conf.addGroup("ClassInfo");
  conf["ClassInfo"].addString("ClassName", m_className, m_className, "Derived class name", true);
  // Global register subconfig
  conf.addConfig("GlobalRegister_0/GlobalRegister");
  conf.subConfig("GlobalRegister_0/GlobalRegister").addGroup("GlobalRegister");

  for(auto const & [registerName, globalRegister]: Fei3::globRegNames) {
    conf.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"].addInt(
            std::string(registerName),
	    m_globRegisters[static_cast<int>(globalRegister)],
	    readGlobRegister(globalRegister),
	    std::string(registerName).append(" register"),
	    true);
  }

  // Pixel register group
  conf.addConfig("PixelRegister_0/PixelRegister");
  conf.subConfig("PixelRegister_0/PixelRegister").addGroup("PixelRegister");
  // Trim group
  conf.addConfig("Trim_0/Trim");
  conf.subConfig("Trim_0/Trim").addGroup("Trim");

  for(auto const & [registerName, pixelRegister]: Fei3::pixRegNames) {
      if(pixelRegister == Fei3::PixReg::TDAC || pixelRegister == Fei3::PixReg::FDAC) {
        conf.subConfig("Trim_0/Trim")["Trim"].addMatrix(
              std::string(registerName),
	      m_trims[static_cast<int>(pixelRegister)],
	      1,
	      std::string(registerName).append(" trim"),
	      true);
      } else {
        conf.subConfig("PixelRegister_0/PixelRegister")["PixelRegister"].addMatrix(
              std::string(registerName),
	      m_pixRegisters[static_cast<int>(pixelRegister)],
	      true,
	      std::string(registerName).append(" register"),
	      true);
      }
  };

  // Miscellanea group
  conf.addGroup("Misc");
  conf["Misc"].addInt("Index", m_index, m_index, "FE index", true);
  conf["Misc"].addInt("Address", m_cmdAddr, m_cmdAddr, "FE geographical address", true);
  conf["Misc"].addFloat("CInjLo", m_cInjLo, m_cInjLo, "Charge injection low", true);
  conf["Misc"].addFloat("CInjHi", m_cInjHi, m_cInjHi, "Charge injection high", true);
  conf["Misc"].addFloat("VcalGradient0", m_vcalGradient[0], m_vcalGradient[0], "VCAL gradient", true);
  conf["Misc"].addFloat("VcalGradient1", m_vcalGradient[1], m_vcalGradient[1], "VCAL gradient", true);
  conf["Misc"].addFloat("VcalGradient2", m_vcalGradient[2], m_vcalGradient[2], "VCAL gradient", true);
  conf["Misc"].addFloat("VcalGradient3", m_vcalGradient[3], m_vcalGradient[3], "VCAL gradient", true);
  conf["Misc"].addFloat("OffsetCorrection", m_offsetCorrection, m_offsetCorrection, "Internal injection offset correction", true);
  conf["Misc"].addBool("ConfigEnable", m_cfgEnable, m_cfgEnable, "FE configuration enable", true);
  conf["Misc"].addBool("ScanEnable", m_scanEnable, m_scanEnable, "FE scan/readout enable", true);
  conf["Misc"].addBool("DacsEnable", m_dacsEnable, m_dacsEnable, "FE DACs enable", true);

  // Select default values
  conf.reset();
}
