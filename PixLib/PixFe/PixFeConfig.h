//! Image of internal FE configuration

#ifndef _PIXLIB_PIXFECONFIG
#define _PIXLIB_PIXFECONFIG

#include <map>
#include <string>

namespace PixLib {
	
  class Config;
  template<class T> class ConfMask;
  class PixFe;
  class PixDbInterface;
  class DbRecord;

  class PixFeConfig {
    
    friend class PixFe;
    
  public:
    //! Destructor
    virtual ~PixFeConfig() {};
		
    //virtual PixFeConfig& operator = (const PixFeConfig& c) {return *this;} //! Assignment operator
    PixFeConfig ( const PixFeConfig & ) = default;
    PixFeConfig(PixDbInterface* db_interface, DbRecord* db_record, int index, int cmdAdr) :
    m_dbN(db_interface), m_dbRecord(db_record),
    m_index(index), m_cmdAddr(cmdAdr){};
    //! Config object accessor
    virtual Config &config()=0;

    virtual int  number() = 0; // Return FE number

    virtual void loadConfig(DbRecord *dbRecord)=0;   // Read the config from DB
    virtual void saveConfig(DbRecord *dbRecord)=0;   // Save the config to DB

    virtual void dump(std::ostream &out) = 0; // Dump FE config info to output stream

  protected:
    PixDbInterface *m_dbN;    // Pointer to db interface
    DbRecord *m_dbRecord;     // Pointer to corresponding db record

    int m_index;   // Front End Index
    int m_cmdAddr; // Geographical address

    bool m_cfgEnable;  // Mask enable FE config (true if chip has to be configured)
    bool m_scanEnable; // Mask scan FE (true if the chip has to be scanned)
    bool m_dacsEnable; // Enable the Dacs in the chip (true if the dacs in the chip have to be enabled)

    int m_defPixelFdac; // Value to be copied into the PixelDacScanData struct
    int m_defPixelTdac; // Value to be copied into the PixelDacScanData struct

    float m_cInjLo;           // Charge injection LOW or CAP1
    float m_cInjMed;          // Charge injection CAP0
    float m_cInjHi;           // Charge injection HIGH or CAP0+CAP1
    std::array<float, 4> m_vcalGradient;  // VCAL FE gradient
    float m_vcalDummy;        // VCAL dummy value
    float m_offsetCorrection; // Internal injection offset correction
  };

}

#endif
