#ifndef _PIXLIB_PIXFEI3
#define _PIXLIB_PIXFEI3

#include <vector>
#include <string>
#include "PixFe/PixFe.h"
#include "PixFe/PixFeI3Config.h"
#include "Fei3.h"

namespace PixLib {
  
  class Config;
  template<class T> class ConfMask;
  class PixDbInterface;
  class DbRecord;
	  
  class PixFeI3 : public PixFe {

  public:
    PixFeI3(DbRecord *dbInquire, PixModule *mod, std::string name, int number); // Constructor
    PixFeI3(PixDbServerInterface *dbServer, PixModule *mod, std::string dom, std::string tag, std::string name, int number);
    virtual ~PixFeI3() = default; // Destructor
		
    Config &config();                                      // Configuration object accessor
    Config &config(std::string configName);                // Alternate configuration objects accessor
    PixFeI3Config &feConfig();                               // Configuration object accessor
    PixFeI3Config &feConfig(std::string configName);         // Alternate configuration objects accessor

    int number();                                          // Return FE number

    void writeGlobRegister(Fei3::GlobReg reg, int value); // Write a value in the Global Register mem copy
    int& readGlobRegister(Fei3::GlobReg reg);             // Read a value from the Global Register mem copy
    ConfMask<bool>& readPixRegister(Fei3::PixReg reg);              // Read a value from the Pixel Register mem copy
    ConfMask<unsigned short int>& readTrim(Fei3::PixReg reg);              // Read a value from the Trim mem copy

    bool restoreConfig(std::string configName = "INITIAL"); //! Restore a configuration from map (calls configure)
    void storeConfig(std::string configName);               //! Store a configuration into map
    void deleteConfig(std::string configName);              //! Remove a configuration from the map
    void loadConfig(DbRecord *dbr);                         //! read a configuration from the DB
    void saveConfig(DbRecord *dbr);                         //! write the current configuration to the DB

    void dump(std::ostream &out); // Dump FE info to output stream
    void setVcal(float charge, bool Chigh);
		
    bool isFeI4() const {return false;};
  protected:
    // Pointer to corresponding db record
    DbRecord *m_dbRecord;
		
    // Pointer to parent PixModule
    PixModule *m_module;
    // FE info
    std::string m_name;
		
    // Command and data structures
    std::unique_ptr<PixFeI3Config> m_data;                           // Current FE configuration
    std::map<std::string, std::unique_ptr<PixFeI3Config>> m_configs; // Stored FE configurations
  };
}
#endif
