//! PixFeConfig implementation for FE-I4(B)

#include <cmath>
#include <iomanip>

#include "Config/Config.h"
#include "PixFe/PixFeConfig.h"
#include "PixFe/PixFeI4Config.h"

using namespace PixLib;

PixFeI4Config::PixFeI4Config(PixDbInterface *db, DbRecord *dbRecord, std::string name, int number):
PixFeConfig(db, dbRecord, number, number), m_name(std::move(name)){
  setupData();
  setupConfigObject();
  // Load initial config from database
  if(m_dbN && m_dbRecord) loadConfig(m_dbRecord);
}

PixFeI4Config::PixFeI4Config(PixDbServerInterface *dbServer, std::string dom, std::string tag, std::string name, std::string configName, int number):
PixFeConfig(nullptr, nullptr, number, number), m_name(std::move(name)){
  setupData();
  setupConfigObject();
  // Load initial config from database
  std::ostringstream str;
  str << configName << "|PixFe_" << number;
  if(!(m_conf->read(dbServer, dom, tag, str.str()))){
    if(dbServer) { std::cout << "PixFeI4Config: problem in reading configuration " << str.str() <<  std::endl; }
    else { std::cout << "PixFeI4Config: problem in reading configuration because dbServer not available " << str.str() <<  std::endl; }
  }
}

PixFeI4Config::PixFeI4Config(const PixFeI4Config &rhs) : PixFeConfig(rhs){
  m_name = rhs.m_name;
  m_className = rhs.m_className;

  m_globRegisters = rhs.m_globRegisters;
  m_globMergedRegisters = rhs.m_globMergedRegisters;
  m_pixRegisters = rhs.m_pixRegisters;
  m_trims = rhs.m_trims;

  setupConfigObject();
}

PixFeI4Config &PixFeI4Config::operator=(const PixFeI4Config &c) {
  if (this != &c) {
    PixFeConfig::operator=(c);
    m_name = c.m_name;
    m_className = c.m_className;

    m_globRegisters = c.m_globRegisters;
    m_globMergedRegisters = c.m_globMergedRegisters;
    m_pixRegisters = c.m_pixRegisters;
    m_trims = c.m_trims;
    m_delayCalib = c.m_delayCalib;

    setupConfigObject();
  }
  return *this;
}

void PixFeI4Config::writeGlobRegister(Fei4::GlobReg reg, int value) {
  m_globRegisters[static_cast<int>(reg)] = value;
}
int& PixFeI4Config::readGlobRegister(Fei4::GlobReg reg) {
  return m_globRegisters[static_cast<int>(reg)];
}
void PixFeI4Config::writeGlobRegister(Fei4::MergedGlobReg reg, int value) {
  m_globMergedRegisters[static_cast<int>(reg)] = value;
}
int& PixFeI4Config::readGlobRegister(Fei4::MergedGlobReg reg) {
  return m_globMergedRegisters[static_cast<int>(reg)];
}
ConfMask<bool>& PixFeI4Config::readPixRegister(Fei4::PixReg reg) {
  return m_pixRegisters[static_cast<int>(reg)];
}
ConfMask<unsigned short int>& PixFeI4Config::readTrim(Fei4::PixReg reg) {
  return m_trims[static_cast<int>(reg)];
}

void PixFeI4Config::loadConfig(DbRecord *dbRecord) {
  std::cout << "PixFeI4Config:loadConfig(DBRecord) called for " << m_conf->m_confName << "/" <<  m_conf->m_tag << "/" << m_conf->m_pendingTag << "/" << m_conf->m_revision << "\n";
  m_conf->read(dbRecord);
}

void PixFeI4Config::saveConfig(DbRecord *dbRecord) {
  m_conf->write(dbRecord);
}

void PixFeI4Config::dump(std::ostream &out) {
  // TODO
}
    
void PixFeI4Config::setupData() {

  // set up general stuff
  m_cInjLo = 1.9;
  m_cInjMed = 3.9;
  m_cInjHi = 5.7;
  m_vcalGradient = {0., 1.5, 0., 0.};
  m_offsetCorrection = 0.;
  m_delayCalib = 1.;

  m_cfgEnable = true;
  m_scanEnable = true;
  m_dacsEnable = true;

  m_globRegisters.fill(0);
  m_globMergedRegisters.fill(0);

  m_pixRegisters[static_cast<int>(Fei4::PixReg::Enable)] = ConfMask<bool>(nCol, nRow, true, true);
  m_pixRegisters[static_cast<int>(Fei4::PixReg::LargeCap)] = ConfMask<bool>(nCol, nRow, true, true);
  m_pixRegisters[static_cast<int>(Fei4::PixReg::SmallCap)] = ConfMask<bool>(nCol, nRow, true, true);
  m_pixRegisters[static_cast<int>(Fei4::PixReg::HitBus)] = ConfMask<bool>(nCol, nRow, true, true);

  m_trims[static_cast<int>(Fei4::PixReg::TDAC)] = ConfMask<unsigned short int>(nCol, nRow, 31 , 15);
  m_trims[static_cast<int>(Fei4::PixReg::FDAC)] = ConfMask<unsigned short int>(nCol, nRow, 15, 7);

  //TODO
  //Setting default values from the FE-I4 manual
  writeGlobRegister(Fei4::GlobReg::Amp2Vbn, 79);
  writeGlobRegister(Fei4::GlobReg::Amp2Vbp, 85);
  writeGlobRegister(Fei4::GlobReg::Amp2Vbpff, 50);
  writeGlobRegister(Fei4::GlobReg::Amp2VbpFol, 26);
  writeGlobRegister(Fei4::GlobReg::BufVgOpAmp, 160); //170
  writeGlobRegister(Fei4::GlobReg::CAL, 0);
  writeGlobRegister(Fei4::GlobReg::EFUSE, 15);
  writeGlobRegister(Fei4::MergedGlobReg::CLK0, 1); // check if boc is looking for 160 mhz to go to 4
  writeGlobRegister(Fei4::MergedGlobReg::CLK1, 0);
  writeGlobRegister(Fei4::GlobReg::c2o, 0);
  writeGlobRegister(Fei4::GlobReg::CMDcnt12, 0);
  writeGlobRegister(Fei4::GlobReg::Colpr_Addr, 0);
  writeGlobRegister(Fei4::MergedGlobReg::CP, 0);
  writeGlobRegister(Fei4::GlobReg::CAE, 1);
  writeGlobRegister(Fei4::GlobReg::DHS, 1);
  writeGlobRegister(Fei4::GlobReg::DJO, 0);
  writeGlobRegister(Fei4::GlobReg::DisVbn, 26);
  writeGlobRegister(Fei4::GlobReg::EFS, 0);
  writeGlobRegister(Fei4::GlobReg::EmptyRecordCnfg, 0);
  writeGlobRegister(Fei4::GlobReg::PLL, 1);
  writeGlobRegister(Fei4::GlobReg::EN_160, 1);
  writeGlobRegister(Fei4::GlobReg::EN_320, 0);
  writeGlobRegister(Fei4::GlobReg::EN_40M, 1);
  writeGlobRegister(Fei4::GlobReg::EN_80M, 0);
  writeGlobRegister(Fei4::GlobReg::ErrorMask_0, 0);
  writeGlobRegister(Fei4::GlobReg::ErrorMask_1, 0);
  writeGlobRegister(Fei4::GlobReg::EventLimit, 0);
  writeGlobRegister(Fei4::GlobReg::XAC, 0);
  writeGlobRegister(Fei4::GlobReg::XDC, 0);
  writeGlobRegister(Fei4::GlobReg::FDACVbn, 50);
  writeGlobRegister(Fei4::GlobReg::ADC, 0);
  writeGlobRegister(Fei4::GlobReg::GADCSel, 0);
  writeGlobRegister(Fei4::GlobReg::GADCCompBias, 100);
  writeGlobRegister(Fei4::GlobReg::HOR, 0);
  writeGlobRegister(Fei4::MergedGlobReg::HD, 2);
  writeGlobRegister(Fei4::GlobReg::HLD, 0);
  writeGlobRegister(Fei4::GlobReg::ILR, 0); // NO DEFAULT FOUND IN MANUAL
  writeGlobRegister(Fei4::GlobReg::LEN, 0);
  writeGlobRegister(Fei4::GlobReg::LVE, 1);
  writeGlobRegister(Fei4::GlobReg::LVDSDrvIref, 171);
  writeGlobRegister(Fei4::GlobReg::LV0, 1);
  writeGlobRegister(Fei4::GlobReg::LV1, 1);
  writeGlobRegister(Fei4::GlobReg::LV3, 1);
  writeGlobRegister(Fei4::GlobReg::LVDSDrvVos, 105);
  writeGlobRegister(Fei4::GlobReg::PllIbias, 88);
  writeGlobRegister(Fei4::GlobReg::PllIcp, 28);
  writeGlobRegister(Fei4::GlobReg::PlsrDAC, 800);
  writeGlobRegister(Fei4::GlobReg::PlsrDACbias, 96);
  writeGlobRegister(Fei4::GlobReg::PlsrDelay, 2);
  writeGlobRegister(Fei4::GlobReg::PlsrIDACRamp, 213); // 180
  writeGlobRegister(Fei4::GlobReg::PPW, 1);
  writeGlobRegister(Fei4::GlobReg::PlsrRiseUpTau, 7);
  writeGlobRegister(Fei4::GlobReg::PlsrVgOpAmp, 255);
  writeGlobRegister(Fei4::GlobReg::PrmpVbnFol, 106);
  writeGlobRegister(Fei4::GlobReg::PrmpVbnLCC, 0);
  writeGlobRegister(Fei4::GlobReg::PrmpVbp, 43);
  writeGlobRegister(Fei4::GlobReg::PrmpVbpf, 20); //100
  writeGlobRegister(Fei4::GlobReg::PrmpVbp_L, 43);
  writeGlobRegister(Fei4::GlobReg::b7E, 0);
  writeGlobRegister(Fei4::GlobReg::PrmpVbp_R, 43);
  writeGlobRegister(Fei4::GlobReg::Pixel_latch_strobe, 0);
  writeGlobRegister(Fei4::GlobReg::RER, 0);
 /* writeGlobRegister(Fei4::GlobReg::"Reg13Spare", 0);
  writeGlobRegister(Fei4::GlobReg::"Reg17Spare", 0);
  writeGlobRegister(Fei4::GlobReg::"Reg1Spare", 0);
  writeGlobRegister(Fei4::GlobReg::"Reg21Spare", 0);
  writeGlobRegister(Fei4::GlobReg::"Reg22Spare1", 0);
  writeGlobRegister(Fei4::GlobReg::"Reg22Spare2", 0);
  writeGlobRegister(Fei4::GlobReg::"Reg27Spare1", 0);
  writeGlobRegister(Fei4::GlobReg::"Reg27Spare2", 0);
  writeGlobRegister(Fei4::GlobReg::"Reg28Spare", 0);
  writeGlobRegister(Fei4::GlobReg::"Reg29Spare1", 0);
  writeGlobRegister(Fei4::GlobReg::"Reg29Spare2", 0);
  writeGlobRegister(Fei4::GlobReg::"Reg2Spare", 0);
  writeGlobRegister(Fei4::GlobReg::"Reg30Spare", 0);
  writeGlobRegister(Fei4::GlobReg::"Reg31Spare", 0);
  writeGlobRegister(Fei4::GlobReg::"Reg34Spare1", 0);
  writeGlobRegister(Fei4::GlobReg::"Reg34Spare2", 0);
  writeGlobRegister(Fei4::GlobReg::"Reg6Spare", 0);
  writeGlobRegister(Fei4::GlobReg::"Reg9Spare", 0); */
  writeGlobRegister(Fei4::GlobReg::S0, 0);
  writeGlobRegister(Fei4::GlobReg::S1, 0);
  writeGlobRegister(Fei4::GlobReg::SRR, 0);
  writeGlobRegister(Fei4::GlobReg::SME, 0);
  writeGlobRegister(Fei4::GlobReg::SRK, 0);
  writeGlobRegister(Fei4::GlobReg::SRC, 0);
  writeGlobRegister(Fei4::GlobReg::STP, 0);
  writeGlobRegister(Fei4::GlobReg::STC, 0);
  writeGlobRegister(Fei4::GlobReg::TDACVbp, 255);
  writeGlobRegister(Fei4::GlobReg::TempSensIbias, 0);
  writeGlobRegister(Fei4::MergedGlobReg::TM, 0);
  writeGlobRegister(Fei4::GlobReg::TMD, 0);
  writeGlobRegister(Fei4::GlobReg::Trig_Count, 1);
  writeGlobRegister(Fei4::GlobReg::Trig_Lat, 210);
  writeGlobRegister(Fei4::GlobReg::VrefAnTune, 0);
  writeGlobRegister(Fei4::GlobReg::VrefDigTune, 0);
  writeGlobRegister(Fei4::GlobReg::Vthin_Coarse, 2);
  writeGlobRegister(Fei4::GlobReg::Vthin_Fine, 46);
  writeGlobRegister(Fei4::GlobReg::N8b, 0);
}

void PixFeI4Config::setupConfigObject() {
  // Create the Config object
  std::stringstream configName;
  configName << "PixFe_"<<m_index<<"/PixFe";
  m_conf = std::make_unique<Config>(configName.str());
  Config &conf = *m_conf;

  //Idicates that no other Config owns this config, even if we link it in!
  conf.m_ownership = false;

  // Group class info
  conf.addGroup("ClassInfo");
  conf["ClassInfo"].addString("ClassName", m_className, m_className, "Derived class name", true);
  // Global register subconfig
  conf.addConfig("GlobalRegister_0/GlobalRegister");
  conf.subConfig("GlobalRegister_0/GlobalRegister").addGroup("GlobalRegister");

  for(auto const & [registerName, globalRegister]: Fei4::globRegNames) {
      conf.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"].addInt(
              std::string(registerName),
	      m_globRegisters[static_cast<int>(globalRegister)],
	      readGlobRegister(globalRegister),
	      std::string(registerName).append(" register"),
	      true);
  }
  for(auto const & [registerName, mergedGlobalRegister]: Fei4::mergedGlobRegNames) {
      conf.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"].addInt(
              std::string(registerName),
	      m_globMergedRegisters[static_cast<int>(mergedGlobalRegister)],
	      readGlobRegister(mergedGlobalRegister),
	      std::string(registerName).append(" register"),
	      true);
  }

  // Pixel register group
  conf.addConfig("PixelRegister_0/PixelRegister");
  conf.subConfig("PixelRegister_0/PixelRegister").addGroup("PixelRegister");
  // Trim group
  conf.addConfig("Trim_0/Trim");
  conf.subConfig("Trim_0/Trim").addGroup("Trim");

  for(auto const & [registerName, pixelRegister]: Fei4::pixRegNames) {
      if(registerName == "TDAC" || registerName == "FDAC") {
        conf.subConfig("Trim_0/Trim")["Trim"].addMatrix(
                std::string(registerName),
		m_trims[static_cast<int>(pixelRegister)],
		1,
		std::string(registerName).append(" trim"),
		true);

      } else {
        conf.subConfig("PixelRegister_0/PixelRegister")["PixelRegister"].addMatrix(
                std::string(registerName),
	        m_pixRegisters[static_cast<int>(pixelRegister)],
	        true,
	        std::string(registerName).append(" register"),
		true);
      }
  }

  // Miscellanea group
  conf.addGroup("Misc");
  conf["Misc"].addInt("Index", m_index, m_index, "FE index", true);
  conf["Misc"].addInt("Address", m_cmdAddr, m_cmdAddr, "FE geographical address", true);
  conf["Misc"].addFloat("CInjLo", m_cInjLo, m_cInjLo, "Charge injection CAP1", true);
  conf["Misc"].addFloat("CInjMed", m_cInjMed, m_cInjMed, "Charge injection CAP0", true);
  conf["Misc"].addFloat("CInjHi", m_cInjHi, m_cInjHi, "Charge injection CAP0+CAP1", true);
  conf["Misc"].addFloat("VcalGradient0", m_vcalGradient[0], m_vcalGradient[0], "VCAL gradient", true);
  conf["Misc"].addFloat("VcalGradient1", m_vcalGradient[1], m_vcalGradient[1], "VCAL gradient", true);
  conf["Misc"].addFloat("VcalGradient2", m_vcalGradient[2], m_vcalGradient[2], "VCAL gradient", true);
  conf["Misc"].addFloat("VcalGradient3", m_vcalGradient[3], m_vcalGradient[3], "VCAL gradient", true);
  conf["Misc"].addFloat("OffsetCorrection", m_offsetCorrection, m_offsetCorrection, "Internal injection offset correction", true);
  conf["Misc"].addFloat("DelayCalib", m_delayCalib, m_delayCalib, "Calibration of PlsrDelay to ns", true);
  conf["Misc"].addBool("ConfigEnable", m_cfgEnable, m_cfgEnable, "FE configuration enable", true);
  conf["Misc"].addBool("ScanEnable", m_scanEnable, m_scanEnable, "FE scan/readout enable", true);
  conf["Misc"].addBool("DacsEnable", m_dacsEnable, m_dacsEnable, "FE DACs enable", true);

  // Select default values
  conf.reset();
}
