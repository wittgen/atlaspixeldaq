//! FE-I4 implementation

#include <math.h>
#include "PixDbInterface/PixDbInterface.h"
#include "PixController/PixController.h"
#include "PixModule/PixModule.h"
#include "PixFe/PixFeI4Config.h"
#include "PixFe/PixFeI4.h"

using namespace PixLib;

//! Constructor
PixFeI4::PixFeI4(DbRecord *dbRecord, PixModule *mod, std::string name, int number) {
  // Members initialization
  m_dbRecord = dbRecord;
  m_module = mod;
  m_name = name;

  // Create FE config
  if (m_dbRecord == NULL) {
    m_data = std::make_unique<PixFeI4Config>((PixDbInterface *)NULL, (DbRecord *)NULL, m_name, number);
  } else {
    m_data = std::make_unique<PixFeI4Config>(m_dbRecord->getDb(), m_dbRecord, m_name, number);
  }
}

PixFeI4::PixFeI4(PixDbServerInterface *dbServer, PixModule *mod, std::string dom, std::string tag, std::string name, int number) {
  // Members initialization
  m_dbRecord = NULL;
  m_module = mod;
  m_name = name;
  // Create FE config
  m_data = std::make_unique<PixFeI4Config>(dbServer, dom, tag, m_name, m_module->moduleName(), number);
}

Config &PixFeI4::config() {
  return m_data->config();
}
Config &PixFeI4::config(std::string configName) {
  if (m_configs.find(configName) != m_configs.end()) {
    return m_configs[configName]->config();
  }
  throw std::runtime_error("Config "+configName+" for PixFeI4 not found in map");
  return m_data->config();
}
PixFeI4Config &PixFeI4::feConfig() {
  return *m_data;
}
PixFeI4Config &PixFeI4::feConfig(std::string configName) {
  if (m_configs.find(configName) != m_configs.end()) {
    return *m_configs[configName];
  }
  throw std::runtime_error("PixFeI4Config "+configName+" for PixFeI4 not found in map");
  return *m_data;
}

int PixFeI4::number() {
  return m_data->number();
}

void PixFeI4::writeGlobRegister(Fei4::GlobReg reg, int value) {
  m_data->writeGlobRegister(reg, value);
}
int& PixFeI4::readGlobRegister(Fei4::GlobReg reg) {
  return m_data->readGlobRegister(reg);
}
void PixFeI4::writeGlobRegister(Fei4::MergedGlobReg reg, int value) {
  m_data->writeGlobRegister(reg, value);
}
int& PixFeI4::readGlobRegister(Fei4::MergedGlobReg reg) {
  return m_data->readGlobRegister(reg);
}
ConfMask<bool>& PixFeI4::readPixRegister(Fei4::PixReg reg) {
  return m_data->readPixRegister(reg);
}
ConfMask<unsigned short int>& PixFeI4::readTrim(Fei4::PixReg reg) {
  return m_data->readTrim(reg);
}

//! Restore a configuration from map
bool PixFeI4::restoreConfig(std::string configName /*= "INITIAL"*/) {
  if (auto it = m_configs.find(configName); it != m_configs.end()) {
    m_data = std::move(m_configs.extract(it).mapped());
    return true;
  }
  return false;
}

//! Store a configuration into map
void PixFeI4::storeConfig(std::string configName) {
  m_configs[configName] = std::make_unique<PixFeI4Config>(*m_data.get());
}

//! Remove a configuration from the map
void PixFeI4::deleteConfig(std::string configName) {
  if (m_configs.find(configName) != m_configs.end()) {
    m_configs.erase(configName);
  }
}

// ------ Interfacing the database ------
// This is code shared with PixFei3, might be worth refactoring
void PixFeI4::loadConfig(DbRecord *dbRecord) {
  m_data->loadConfig(dbRecord);
}

void PixFeI4::saveConfig(DbRecord *dbRecord) {
  m_data->saveConfig(dbRecord);
}

void PixFeI4::dump(std::ostream &out) {
  m_data->dump(out);
}
