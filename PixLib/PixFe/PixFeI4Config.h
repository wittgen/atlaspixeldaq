//! Image of internal FE-I4(B) configuration

#ifndef _PIXLIB_PIXFEI4CONFIG
#define _PIXLIB_PIXFEI4CONFIG

#include <map>
#include <string>
#include <string_view>
#include "PixFe/PixFeConfig.h"
#include "Fei4.h"
#include <memory>
#include "Config/ConfMask.h"
#include "Config/Config.h"

namespace PixLib {
	
  class Config;
  class PixDbInterface;
  class PixDbServerInterface;

  class PixFeI4Config : public PixFeConfig {

  public:
    PixFeI4Config(PixDbInterface *db, DbRecord *dbrecord, std::string name, int number); //! Constructor
    PixFeI4Config(PixDbServerInterface *dbServer, std::string dom, std::string tag,
            std::string name, std::string configName, int number); //! Constructor

    PixFeI4Config(const PixFeI4Config& c); //! Copy constructor
    virtual ~PixFeI4Config() = default; //! Destructor
    PixFeI4Config& operator = (const PixFeI4Config& c); //! Assignment operator

    //! Config object accessor
    Config &config() final {return *m_conf;}

    int number() final {return m_index;} // Return FE number

    void writeGlobRegister(Fei4::GlobReg reg, int value); // Write a value in the Global Register mem copy
    int& readGlobRegister(Fei4::GlobReg reg);             // Read a value from the Global Register mem copy
    void writeGlobRegister(Fei4::MergedGlobReg reg, int value); // Write a value in the Global Register mem copy
    int& readGlobRegister(Fei4::MergedGlobReg reg);             // Read a value from the Global Register mem copy
    ConfMask<bool>& readPixRegister(Fei4::PixReg reg);              // Read a value from the Pixel Register mem copy
    ConfMask<unsigned short int>& readTrim(Fei4::PixReg reg);              // Read a value from the Trim mem copy

    void loadConfig(DbRecord *dbRecord) final;   // Read the config from DB
    void saveConfig(DbRecord *dbRecord) final;   // Save the config to DB
    
    void dump(std::ostream &out) final; // Dump FE config info to output stream

  protected:
    std::string m_name; // FE name
    std::string m_className = "PixFeI4B"; // FE derived class name
    static constexpr int nRow = 336;
    static constexpr int nCol = 80;

    std::unique_ptr<Config> m_conf;
    std::array<int, Fei4::nGlobRegs> m_globRegisters {}; // FE Global register values
    std::array<int, Fei4::nMergedGlobRegs> m_globMergedRegisters {}; // FE Merged Global register values
    std::array<ConfMask<bool>, Fei4::nPixRegs> m_pixRegisters {}; // FE Pixel register values
    std::array<ConfMask<unsigned short int>, Fei4::nPixRegs> m_trims {}; // FE Trim values

    float m_delayCalib = 0.;       // Calibration of PlsrDelay to ns (only FE-I4)

    void setupData(); // Setup FE configuration default data
    void setupConfigObject(); // Setup FE configuration block
  };
}
#endif
