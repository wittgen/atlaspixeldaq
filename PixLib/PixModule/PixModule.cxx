/////////////////////////////////////////////////////////////////////
// PixModule.cxx
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 11/04/03  Version 1.0 (CS)
//           Initial release
//

//! Class for he Pixel Module

//#include "serialStreams.h"
//#include "pixelConfigStructures.h"
#include "Bits/Bits.h"
#include "PixModuleGroup/PixModuleGroup.h"
#include "PixMcc/PixMccI1.h"
#include "PixMcc/PixMccI2.h"
#include "PixFe/PixFe.h"
#include "PixFe/PixFeI3.h"
#include "PixFe/PixFeI4.h"
#include "PixController/PixController.h"

using namespace SctPixelRod;
using namespace PixLib;

std::string strElement(std::string s, int num, char sep) {
  unsigned int ip;
  int start = -1,end = -1;
  int nel = -1;
  bool spc = true;
  bool quote = false;
                                                                                
  for (ip=0;ip<s.size();ip++) {
    if (s[ip] == '"') quote = !quote;
    if (s[ip] == sep || (sep == 0 && (
        s[ip] == 9 || s[ip]==10 || s[ip] == 32))) {
      if (!spc && !quote) {
        spc = true;
        if (nel==num) end = ip-1;
      }
    } else {
      if (spc) {
        spc = false;
        nel++;
        if (nel==num) start = ip;
      }
    }
  }
  if (!spc && nel==num) end = s.size()-1;
                                                                                
  if (start >= 0) {
    std::string c;
    std::string s1 = s.substr(start,end-start+1);
    for (ip=0;ip<s1.size();ip++) {
      if (s1[ip] != '"') c += s1[ip];
    }
    return c;
  } else {
    return "";
  }
}

PixModule::PixModule(DbRecord *dbRecord, PixModuleGroup *modGrp, std::string name) :
  m_name(name), m_group(modGrp), m_inputLink(0),  m_outputLink1(1), m_outputLink2(1), m_outputLink3(1), m_outputLink4(1), 
  m_delay(750), m_latency(200), m_outputBandwidth(0),m_geo(PixGeometry::INVALID_MODULE)
{

  //Initialize the Modules properties during scan

  m_configActive  = false;
  m_triggerActive = false;
  m_strobeActive  = false;
  m_readoutActive = false;

  // Initialize the config object
  m_dbRecord = dbRecord;
  initConfig();
  Config &conf = *m_conf;
  m_localCfgStored = false;
  m_cfgChanged = false;
  //  m_modConfig = NULL;
  //bool err = false;
  m_mcc = 0;

  for(dbRecordIterator it = dbRecord->recordBegin(); it != dbRecord->recordEnd(); it++){
    // Look for MCC inquire
    if((*it)->getClassName() == "PixMcc") {
      // Read MCC derived class
      dbFieldIterator f = (*it)->findField("ClassInfo_ClassName");
      if(f!=(*it)->fieldEnd()) {
        std::string className; 
	try {
	  dbRecord->getDb()->DbProcess(f, PixDb::DBREAD, className);
	  // Switch between different MCC derived classes
	  if(className == "PixMccI1") {
	    m_mcc = new PixMccI1(*it, this, "MCC");
	    conf.addConfig(&(m_mcc->config()));
	  } else if(className == "PixMccI2") {
	    m_mcc = new PixMccI2(*it, this, "MCC");
	    conf.addConfig(&(m_mcc->config()));
	  }
	}
	catch (const PixDBException &ex) {
	  //err = true;
	}
      }
    }
    // Look for FE inquires
    if((*it)->getClassName() == "PixFe"){
      std::stringstream o((*it)->getName().substr((*it)->getName().rfind("_")+1,2));
      int a;
      o >> a;
      // Read FE derived class
      dbFieldIterator f = (*it)->findField("ClassInfo_ClassName");
      if(f!=(*it)->fieldEnd()) {
        std::string className;
	try {
	  dbRecord->getDb()->DbProcess(f, PixDb::DBREAD, className);
          // Switch between different FE derived classes
          if(className == "PixFeI1") {
	    m_fe.push_back(new PixFeI3(*it, this, (*it)->getName(), a));
	    conf.addConfig(&(m_fe.back()->config()), false);
	  } else if(className == "PixFeI2") {
	    m_fe.push_back(new PixFeI3(*it, this,(*it)->getName(),a));
	    conf.addConfig(&(m_fe.back()->config()), false);
	  } else if(className == "PixFeI4A") {
	    m_fe.push_back(new PixFeI4(*it, this,(*it)->getName(),a));
	    conf.addConfig(&(m_fe.back()->config()), false);
	  } else if(className == "PixFeI4B") {
	    m_fe.push_back(new PixFeI4(*it, this, (*it)->getName(), a));
	    conf.addConfig(&(m_fe.back()->config()), false);
	  }
        }
	catch (const PixDBException &ex) {
	  //err = true;
	}
      }
    }
  }
  m_nFe = (int)m_fe.size();
}

PixModule::PixModule(std::string dom, std::string tag, std::string name,
             int mccFlavour, int feFlavour, int nFe) :
m_name(name), m_group(NULL), m_inputLink(0),  m_outputLink1(1), m_outputLink2(1), m_outputLink3(1), m_outputLink4(1),
  m_delay(750), m_latency(200), m_outputBandwidth(0), m_geo(PixGeometry::INVALID_MODULE)
{
    //Initialize the Modules properties during scan

  m_configActive  = false;
  m_triggerActive = false;
  m_strobeActive  = false;
  m_readoutActive = false;

  // Initialize the config object
  m_dbRecord = NULL;
  initConfig();
  m_cfgChanged = false;
  Config &conf = *m_conf;
  m_localCfgStored = false;
  //  m_modConfig = NULL;
  m_mcc = 0;
  // only needed if not read from db
  m_mccFlavour = mccFlavour;
  m_feFlavour = feFlavour;
  m_nFe = nFe;


  switch(m_mccFlavour){
  case PM_MCC_I2:
    m_mcc = new PixMccI2(NULL, this, dom, tag, "MCC");
    break;
//   case PM_MCC_I1:
//     m_mcc = new PixMccI1(dbServer, this, dom, tag, "MCC");
//     break;
  default:
  case PM_NO_MCC:
    m_mcc = 0;
    break;
  }
  if(m_mcc!=0) conf.addConfig(&(m_mcc->config()));

  for (int i=0; i<m_nFe; i++) {
    PixFe *newFe;
    switch(m_feFlavour){
//     case PM_FE_I1:
//       newFe = new PixFeI1(dbServer, this, dom, tag,"FE",i);
//       break;
    case PM_FE_I2:
      newFe = new PixFeI3(NULL, this, dom, tag,"FE",i);
      break;
    case PM_FE_I4A:
      newFe = new PixFeI4(NULL, this, dom, tag,"FE",i);
      break;
    case PM_FE_I4B:
      newFe = new PixFeI4(NULL, this, dom, tag,"FE",i);
      break;
    default:
    case PM_NO_FE:
      newFe=0;
      break;
    }
    if(newFe!=0){
      m_fe.push_back(newFe);
      conf.addConfig(&(newFe->config()), false);
    }
  }

  if(m_mccFlavour==PM_MCC_I2 && m_feFlavour==PM_FE_I2){
    m_geo=PixGeometry(PixGeometry::FEI2_MODULE);
    //    std::cout << "Module " << m_name << " is of type FE-I3/MCC "<< std::endl;
  }
//   else if(m_mccFlavour==PM_MCC_I1 && m_feFlavour==PM_FE_I1){
//     m_geo=PixGeometry(PixGeometry::FEI2_MODULE);
//   }
  else if(m_mccFlavour==PM_NO_MCC && (m_feFlavour&PM_FE_I4)!=0){
    m_geo=PixGeometry((nFe==1)?PixGeometry::FEI4_CHIP:PixGeometry::FEI4_MODULE);
    //    std::cout << "Module " << m_name << " is of type FE-I4 "<< m_feFlavour << ", " << m_nFe <<std::endl;
  }
  else     std::cerr << " Inconsistent or non-existing MCC/FE types for module " << m_name <<  std::endl;

}
PixModule::PixModule(PixDbServerInterface *dbServer, PixModuleGroup *modGrp, std::string dom, std::string tag, std::string name,
		     int mccFlavour, int feFlavour, int nFe) :
  m_name(name), m_group(modGrp), m_inputLink(0),  m_outputLink1(1), m_outputLink2(1), m_outputLink3(1), m_outputLink4(1), 
  m_delay(750), m_latency(200), m_outputBandwidth(0), m_geo(PixGeometry::INVALID_MODULE)
{
  
  //Initialize the Modules properties during scan

  m_configActive  = false;
  m_triggerActive = false;
  m_strobeActive  = false;
  m_readoutActive = false;

  // Initialize the config object
  m_dbRecord = NULL;
  initConfig();
  m_cfgChanged = false;
  Config &conf = *m_conf;
  m_localCfgStored = false;
  //  m_modConfig = NULL;
  m_mcc = 0;
  // only needed if not read from db
  m_mccFlavour = mccFlavour;
  m_feFlavour = feFlavour;
  m_nFe = nFe;

  if(dbServer==NULL) std::cout << "PixModule.cxx :: PixDbServerInterface is NULL, default config has been loaded (this explains following error)!!!" << std::endl;

  // Read the config object
  if(!m_conf->read(dbServer, dom, tag, m_name)) 
    std::cout << " Problem in reading configuration for module " << m_name <<  std::endl;

  switch(m_mccFlavour){
  case PM_MCC_I2:
    m_mcc = new PixMccI2(dbServer, this, dom, tag, "MCC");
    break;
//   case PM_MCC_I1:
//     m_mcc = new PixMccI1(dbServer, this, dom, tag, "MCC");
//     break;
  default:
  case PM_NO_MCC:
    m_mcc = 0;
    break;
  }
  if(m_mcc!=0) conf.addConfig(&(m_mcc->config()));

  for (int i=0; i<m_nFe; i++) {
    PixFe *newFe;
    switch(m_feFlavour){
//     case PM_FE_I1:
//       newFe = new PixFeI3(dbServer, this, dom, tag,"FE",i);
//       break;
    case PM_FE_I2:
      newFe = new PixFeI3(dbServer, this, dom, tag,"FE",i);
      break;
    case PM_FE_I4A:
      newFe = new PixFeI4(dbServer, this, dom, tag,"FE",i);
      break;
    case PM_FE_I4B:
      newFe = new PixFeI4(dbServer, this, dom, tag, "FE", i);
      break;
    default:
    case PM_NO_FE:
      newFe=0;
      break;
    }
    if(newFe!=0){
      m_fe.push_back(newFe);
      conf.addConfig(&(newFe->config()), false);
    }
  }

  if(m_mccFlavour==PM_MCC_I2 && m_feFlavour==PM_FE_I2){
    m_geo=PixGeometry(PixGeometry::FEI2_MODULE);
    //    std::cout << "Module " << m_name << " is of type FE-I3/MCC "<< std::endl;
  }
//   else if(m_mccFlavour==PM_MCC_I1 && m_feFlavour==PM_FE_I1){
//     m_geo=PixGeometry(PixGeometry::FEI2_MODULE);
//   }
  else if(m_mccFlavour==PM_NO_MCC && (m_feFlavour&PM_FE_I4)!=0){
    m_geo=PixGeometry((nFe==1)?PixGeometry::FEI4_CHIP:PixGeometry::FEI4_MODULE);
    //    std::cout << "Module " << m_name << " is of type FE-I4 "<< m_feFlavour << ", " << m_nFe <<std::endl;
  }
  else     std::cerr << " Inconsistent or non-existing MCC/FE types for module " << m_name <<  std::endl;

}

void PixModule::relinkFEConfigs() {
  for(auto& fe: m_fe) {
      m_conf->insertConfigNoDelete(&(fe->config()), false); 
  }
}

void PixModule::initConfig() {
  // Create the Config object
  stringstream configName;
  configName<<m_name<<"_0"<<"/PixModule";
  m_conf = new Config(configName.str());
  Config &conf = *m_conf;

  // Insert the configuration parameters in the Config object
  std::map<std::string, int> tf;
  tf["FALSE"] = 0;
  tf["TRUE"] = 1;
  std::map<std::string, int> mccfl;
  mccfl["NO MCC"] = PM_NO_MCC;
  mccfl["MCC_I1"] = PM_MCC_I1;
  mccfl["MCC_I2"] = PM_MCC_I2;
  std::map<std::string, int> fefl;
  fefl["FE_I1"] = PM_FE_I1;
  fefl["FE_I2"] = PM_FE_I2;
  fefl["FE_I4A"] = PM_FE_I4A;
  fefl["FE_I4B"] = PM_FE_I4B;

  // Group general
  conf.addGroup("general");

  conf["general"].addInt("ModuleId", m_moduleId, 0, 
                         "Module Identifier", true);
  conf["general"].addInt("GroupId", m_groupId, 0, 
                         "Group Identifier", true);
  conf["general"].addList("Active", m_active, 1, tf, 
                          "Module active", true);
  conf["general"].addList("Present", m_present, 1,tf, 
                          "Module present", true);
  conf["general"].addInt("InputLink", m_inputLink, 0, 
                         "DCI link", true);
  conf["general"].addInt("OutputLink1", m_outputLink1, 0, 
                         "DTO link 1", true);
  conf["general"].addInt("OutputLink2", m_outputLink2, 0, 
                         "DTO link 2", true);
  conf["general"].addInt("OutputLink3", m_outputLink3, 0, 
                         "DTO link 3", true);
  conf["general"].addInt("OutputLink4", m_outputLink4, 0, 
                         "DTO link 4", true);
  conf["general"].addInt("BocInputLink", m_bocInputLink, -1, 
                         "BOC DCI link", true);
  conf["general"].addInt("BocOutputLink1", m_bocOutputLink1, -1, 
                         "BOC DTO link 1", true);
  conf["general"].addInt("BocOutputLink2", m_bocOutputLink2, -1, 
                         "DTO link 2", true);
  conf["general"].addList("MCC_Flavour", m_mccFlavour, static_cast<int>(PM_MCC_I2), mccfl, 
                          "MCC Flavour", true);
  conf["general"].addList("FE_Flavour", m_feFlavour, static_cast<int>(PM_FE_I2), fefl, 
                          "FE Flavour", true);
  conf["general"].addBool("useAltLink", m_useAltLink, false, 
                          "Use alternate module link if possible", true);

  std::map<std::string, int> geomTyp;
  geomTyp["unknown"] = NONE;
  geomTyp["Stave"] = STAVE;
  geomTyp["Sector"] = SECTOR;

  // Group general
  conf.addGroup("geometry");

  conf["geometry"].addList("Type", m_geomType, NONE, geomTyp, 
                           "Geometry type", true);
  conf["geometry"].addInt("position", m_geomPosition, 0, 
                          "Module position in assembly", true);
  conf["geometry"].addInt("staveID", m_geomAssemblyId, 0, 
                          "Assembly identifier", true);
  conf["geometry"].addString("connName", m_geomConnName, "Dxx-Bxx-Sxx", 
                             "Geographical identifier", true);

  std::map<std::string, int> pp0Typ;
  pp0Typ["Optical"] = OPTICAL;
  pp0Typ["Optical_Test"] = OPTICAL_TEST;
  pp0Typ["Electrical"] = ELECTRICAL;

  // Group general
  conf.addGroup("pp0");

  conf["pp0"].addList("Type", m_pp0Type, OPTICAL, pp0Typ, 
                           "PP0 type", true);
  conf["pp0"].addInt("position", m_pp0Position, 1, 
                          "PP0 position", true);

  conf.reset();
  if (m_dbRecord != NULL) {
    conf.read(m_dbRecord);
  }
}

PixModule::~PixModule() {
  // Delete MCC
  if(m_mcc!=0) delete m_mcc;

  // Delete FEs
  for(std::vector<PixFe*>::iterator fe = m_fe.begin(); fe != m_fe.end(); fe++)
    delete *fe;

  //  if (m_modConfig != NULL) delete m_modConfig;

}

std::string PixModule::pp0Name() {
  std::string geom = connName();
  if (geom[0] == 'D') {
    return strElement(geom,0,'_')+"_"+strElement(geom,1,'_')+"_"+strElement(geom,2,'_');
  } else {
    return strElement(geom,0,'_')+"_"+strElement(geom,1,'_')+"_"+strElement(geom,2,'_')+"_"+strElement(geom,3,'_');
  }
}

// std::string PixModule::pp0Name() {
//   std::string geom = connName();
//   if (geom[0] == 'D') {
//     return strElement(geom,0,'_')+"_"+strElement(geom,1,'_')+"_"+strElement(geom,2,'_');
//   } 
//   else if(geom[1] == 'I') {
//     return strElement(geom,0,'_')+"_"+strElement(geom,1,'_')+"_"+strElement(geom,2,'_')+"_"+strElement(geom,3,'_')+"_"+strElement(geom,4,'_');
//   }
//   else {
//     return strElement(geom,0,'_')+"_"+strElement(geom,1,'_')+"_"+strElement(geom,2,'_')+"_"+strElement(geom,3,'_');
//   }
// }

std::string PixModule::connName() {
  std::string name = m_geomConnName;
  if (name[0] == 'D' && strElement(name,1,'-') != "") {
    std::string mod = strElement(name, 3, '-');
    if (mod == "") {
      mod = strElement(name, 2, '-');
      std::string disk = strElement(name, 0, '-');
      std::string sect = strElement(name, 1, '-');
      if (sect == "S1") sect = "B01_S2";
      if (sect == "S2") sect = "B02_S1";
      if (sect == "S3") sect = "B02_S2";
      if (sect == "S4") sect = "B03_S1";
      if (sect == "S5") sect = "B03_S2";
      if (sect == "S6") sect = "B04_S1";
      if (sect == "S7") sect = "B04_S2";
      if (sect == "S8") sect = "B01_S1";
      return disk+"_"+sect+"_"+mod;
    }
    return m_geomConnName;
  } else {
    return m_geomConnName;
  }
}

void PixModule::loadConfig(DbRecord *dbRecord) {
  // Read config object
  m_dbRecord = dbRecord;
  m_conf->read(dbRecord);
}

void PixModule::saveConfig(DbRecord *dbRecord) {
  PixDbInterface *rDb = dbRecord->getDb();
  dbRecordIterator findRec;

  // Create a record for the MCC
  if(m_mcc!=0){
    dbRecordIterator mccRecI;
    findRec = dbRecord->findRecord("PixMcc_0/PixMcc");
    if (findRec == dbRecord->recordEnd()) {
      DbRecord* mccRec = rDb->makeRecord("PixMcc", "PixMcc_0");
      mccRecI = dbRecord->pushRecord(mccRec);
      mccRecI = rDb->DbProcess(mccRecI, PixDb::DBREAD);
    }
    //dbRecord->addRecord("PixMcc", "PixMcc_0");
  }

  // Create a record for the FEs
  for(std::vector<PixFe*>::iterator fe = m_fe.begin(); fe != m_fe.end(); fe++) {
    std::stringstream configName;
    configName << "PixFe_" << (*fe)->number();
    dbRecordIterator feRecI;
    findRec = dbRecord->findRecord(configName.str()+"/PixFe");
    if (findRec == dbRecord->recordEnd()) {
      DbRecord* feRec = rDb->makeRecord("PixFe", configName.str());
      feRecI = dbRecord->pushRecord(feRec);
      feRecI = rDb->DbProcess(feRecI, PixDb::DBREAD);
    }
    //dbRecord->addRecord("PixFe", configName.str());
  }

  // Write config object
  m_dbRecord = dbRecord;
  m_conf->write(dbRecord);
}

void PixModule::loadConfig(PixConnectivity *conn, unsigned int rev) {  //! load configuration from the database
  if (conn != NULL) {
    DbRecord *r;
    if (rev != 0) {
      r = conn->getCfg(m_name, rev);
    } else {
      r = conn->getCfg(m_name);
    }
    loadConfig(r);
  }
}

void PixModule::saveConfig(PixConnectivity *conn, unsigned int rev, bool newBE) {  //! save configuration to the db
  if (conn != NULL) {
    if (!newBE) {
      DbRecord *r = conn->addCfg(connName(), rev);
      //DbRecord *r = conn->addCfg(moduleName(), rev);
      saveConfig(r);
      conn->cleanCfgCache();
    } else {
      std::cout << "Saving module: " << connName() << std::endl;
      conn->addCfgNewBE(config(),connName(), rev);  
      //conn->addCfgNewBE(config(),moduleName(), rev);  
      conn->cleanCfgCache();
    }
  }
}

void PixModule::storeConfig(std::string name) {
  // Store configuration data into child MCC
  if(m_mcc!=0) m_mcc->storeConfig(name);

  // Store configuration data into child FEs
  for(std::vector<PixFe*>::iterator fe = m_fe.begin(); fe != m_fe.end(); fe++)
    (*fe)->storeConfig(name);
}

bool PixModule::restoreConfig(std::string name) {
  bool ret = true;
  // Restore configuration data into child MCC
  if (m_mcc!=0 && !m_mcc->restoreConfig(name)) ret = false;
  
  // Restore configuration data into child FEs
  for(std::vector<PixFe*>::iterator fe = m_fe.begin(); fe != m_fe.end(); fe++) {
    if (!(*fe)->restoreConfig(name)) ret = false;
  }

  relinkFEConfigs();
  return ret;
}

void PixModule::deleteConfig(std::string name) {
  // Delete configuration data into child MCC
  if(m_mcc!=0) m_mcc->deleteConfig(name);

  // Delete configuration data into child FEs
  for(std::vector<PixFe*>::iterator fe = m_fe.begin(); fe != m_fe.end(); fe++)
    (*fe)->deleteConfig(name);
}

//void PixModule::setConfig(int structId, int moduleNum) {
//}

void PixModule::selectFEEnable(bool newDspCode) {
  m_localCfgStored = true;
  storeConfig("initialCfg"); 

  if(!newDspCode) {
    for(std::vector<PixFe*>::iterator fea = feBegin(); fea != feEnd(); fea++){
      PixFe *fei4 = dynamic_cast<PixFeI4*>(*fea);
      if(fei4!=0){
	// to do
      } else{
	// set all FEs to DAC=0
	Config &cfg = (*fea)->config();
	Config *subcfg=0;
	for(unsigned int j=0; j<cfg.subConfigSize(); j++){
	  if(cfg.subConfig(j).name()=="GlobalRegister_0"){
	    subcfg = &(cfg.subConfig(j));
	    break;
	  }	  
	}
	if(subcfg==0){
	  std::cerr << "FE " << (*fea)->number() << " had no globreg config" << std::endl;
	} else{
	  Config &globreg = *subcfg;
	  int resVal = 0;
	  
	  *((int *)((ConfInt&)globreg["GlobalRegister"]["DAC_IVDD2"]).m_value) = (int)resVal;
	  *((int *)((ConfInt&)globreg["GlobalRegister"]["DAC_ID"]).m_value) = (int)resVal;
	  *((int *)((ConfInt&)globreg["GlobalRegister"]["DAC_IP"]).m_value) = (int)resVal;
	  *((int *)((ConfInt&)globreg["GlobalRegister"]["DAC_IP2"]).m_value) = (int)resVal;
	  *((int *)((ConfInt&)globreg["GlobalRegister"]["DAC_ITRIMTH"]).m_value) = (int)resVal;
	  *((int *)((ConfInt&)globreg["GlobalRegister"]["DAC_IF"]).m_value) = (int)resVal;
	  *((int *)((ConfInt&)globreg["GlobalRegister"]["DAC_ITRIMIF"]).m_value) = (int)resVal;
	  *((int *)((ConfInt&)globreg["GlobalRegister"]["DAC_ITH1"]).m_value) = (int)resVal;
	  *((int *)((ConfInt&)globreg["GlobalRegister"]["DAC_ITH2"]).m_value) = (int)resVal;
	  *((int *)((ConfInt&)globreg["GlobalRegister"]["DAC_IL"]).m_value) = (int)resVal;
	  *((int *)((ConfInt&)globreg["GlobalRegister"]["DAC_IL2"]).m_value) = (int)resVal;
	  *((int *)((ConfInt&)globreg["GlobalRegister"]["ENABLE_TIMESTAMP"]).m_value) = (int)resVal;
	  *((int *)((ConfInt&)globreg["GlobalRegister"]["ENABLE_CAP_TEST"]).m_value) = (int)resVal;
	  
	  for(int i=0;i<9;i++){
	    std::stringstream a;
	    a << i;
	    std::string cpen = "ENABLE_CP"+a.str();
	    *((int *)((ConfInt&)globreg["GlobalRegister"][cpen]).m_value) = (int)resVal;
	  }
	}
      }
    }
    //Save the FE OFF configuration
    storeConfig("FrontEndOFFCfg");
  }
}

void PixModule::selectFE(int iFE, bool newDspCode) {
  std::cout << "****I'm running with FEbyFE. I'm on FE: " << iFE << std::endl;
  assert (m_localCfgStored);
  
  for(int i=0;i<(int)m_fe.size();i++){
    Config &cfg = pixFE(i)->config();
    bool resVal = (i == iFE);
    if (!newDspCode) {
      if (resVal) pixFE(i)->restoreConfig("initialCfg");
      else pixFE(i)->restoreConfig("FrontEndOFFCfg");
    }
    PixFe *fei4 = dynamic_cast<PixFeI4*>(pixFE(i));
    if(fei4!=0){
      // to do
    } else{
      ((ConfBool&)cfg["Misc"]["ConfigEnable"]).m_value = resVal;
      ((ConfBool&)cfg["Misc"]["ScanEnable"]).m_value = resVal;
    }
  }
}

void PixModule::disableFESelected() {
  
  assert (m_localCfgStored);
  deleteConfig("FrontEndOFFCfg");
  restoreConfig("initialCfg");
  deleteConfig("initialCfg");
  /*for(std::vector<PixFe*>::iterator fea = feBegin(); fea != feEnd(); fea++){
    Config &cfg = (*fea)->config();
    ((ConfBool&)cfg["Misc"]["ConfigEnable"]).m_value = true;
    ((ConfBool&)cfg["Misc"]["ScanEnable"]).m_value = true;
    }*/
}

void PixModule::setVcal(float charge, bool Chigh)  {
  for(std::vector<PixFe*>::iterator fe = m_fe.begin(); fe != m_fe.end(); fe++) {
    (*fe)->setVcal(charge, Chigh);
  }
}

bool  PixModule::writeConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, std::string ptag, unsigned int revision) {
  if(m_conf->write(DbServer, domainName, tag, m_name,"PixModule", ptag, revision))
    return true;
  else {
    std::cout << "PixModule : ERROR in writing config on DBserver" << std::endl;
    return false;
  }
}

bool  PixModule::readConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, unsigned int revision) {
  if(m_conf->read(DbServer, domainName, tag, m_name,revision))
    return true;
  else {
    std::cout << "PixModule : ERROR in reading config from DBserver" << std::endl;
    return false;
  }
}

void PixModule::setTemp() {
  m_conf->penTag("Temp");
}

void PixModule::saveChangedConfig(std::string tag){
  if (m_cfgChanged){
    if (m_group->dbServerInterface() != NULL) {
      setTemp();
      std::string tags;
      if (tag != ""){
	tags = tag;
      }
      else {
	tags = m_group->pixConnectivity()->getModCfgTag();
      }
      std::string dom = "Configuration-" + m_group->pixConnectivity()->getConnTagOI();
      if (writeConfig(m_group->dbServerInterface(), dom, tags, "PixModule")){
	m_cfgChanged = false;
      }
      else {
	std::cout << "PixModule::saveChangedConfig: ERROR in saving config to DbServer" << std::endl;
      }
    }
    else {
      std::cout << "PixModule::saveChangedConfig: ERROR no PixDbServerInterface" << std::endl;
    }
  }
}

void PixModule::reloadConfig(){
  if (m_group->dbServerInterface() != NULL) {
    std::string tags = m_group->pixConnectivity()->getModCfgTag();
    if (readConfig(m_group->dbServerInterface(), "Configuration-" + m_group->pixConnectivity()->getConnTagOI(), tags)){
      m_cfgChanged = false;
    }
    else {
      std::cout << "PixModule::reloadChangedConfig: ERROR in reading config from DbServer" << std::endl;
    }
    }
  else {
    std::cout << "PixModule::reloadChangedConfig: ERROR no PixDbServerInterface" << std::endl;
  }
}

//int PixModule::getModuleActive() {
//  return m_active;
//}

//void PixModule::setModuleActive(int active){
//  m_active = active;
//}



