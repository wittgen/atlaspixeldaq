/////////////////////////////////////////////////////////////////////
// PixModule.h
// version 1.0.1
/////////////////////////////////////////////////////////////////////
//
// 08/04/03  Version 1.0 (PM)
//           Initial release
//
// 13/04/03  Version 1.0.1 (CS)
//           First concrete implementation
//

//! Class for the Pixel Module

#ifndef _PIXLIB_MODULE
#define _PIXLIB_MODULE

#ifdef WIN32
#pragma warning(disable: 4786)
#pragma warning(disable: 4800)
#endif

#include <vector>
#include <string>

#include "BaseException.h"
#include "PixFe/PixGeometry.h"
#include "PixEnumBase.h"
namespace SctPixelRod {
  class RodPrimitive;
}

namespace PixLib {
  
  class PixController;
  class RodPixController;
  class PixDbInterface;
  class DbRecord;
  class PixModuleGroup;
  class PixMcc;
  class PixFe;
  class Config;
  class PixDbServerInterface;
  class PixConnectivity;

  //! Pix Module Exception class; an object of this type is thrown in case of a module error
  class PixModuleExc : public SctPixelRod::BaseException {
  public:
    enum ErrorLevel{INFO, WARNING, ERROR, FATAL};
    //! Constructor
    PixModuleExc(ErrorLevel el, std::string name) : BaseException(name), m_errorLevel(el), m_name(name) {}; 
    //! Destructor
    virtual ~PixModuleExc() {}; 
 
    //! Dump the error
    virtual void dump(std::ostream &out) {
      out << "Pixel Module " << m_name << " -- Level : " << dumpLevel(); 
    }
    std::string dumpLevel() {
      switch (m_errorLevel) {
      case INFO : 
        return "INFO";
      case WARNING :
        return "WARNING";
      case ERROR :
        return "ERROR";
      case FATAL :
        return "FATAL";
      default :
        return "UNKNOWN";
      }
    }
    //! m_errorLevel accessor
    ErrorLevel getErrorLevel() { return m_errorLevel; };
    //! m_name accessor
    std::string getCtrlName() { return m_name; };
  private:
    ErrorLevel m_errorLevel;
    std::string m_name;
  };
  
  class PixModule : public EnumAssemblyType,public EnumPP0Type,public EnumMCCflavour,public EnumFEflavour {
    friend class PixModuleGroup;
  public:
    /*    enum AssemblyType { NONE, STAVE, SECTOR };
    enum PP0Type { OPTICAL, OPTICAL_TEST, ELECTRICAL };
    enum MCCflavour {PM_NO_MCC=0, PM_MCC_I1=1, PM_MCC_I2=2};
    enum FEflavour {PM_NO_FE=0, PM_FE_I1=1, PM_FE_I2=2, PM_FE_I4A, PM_FE_I4B};
    */
    typedef std::vector<PixFe*>::iterator feIterator;
    
    PixModule(DbRecord *dbRecord, PixModuleGroup *modGrp, std::string name);   // Constructor
    PixModule(PixDbServerInterface *dbServer, PixModuleGroup *modGrp, std::string dom, std::string tag, std::string name,
	      int mccFlavour=PM_MCC_I2, int feFlavour=PM_FE_I2, int nFe=16);
    PixModule(std::string dom, std::string tag, std::string name,
             int mccFlavour, int feFlavour, int nFe);
    ~PixModule();                                                              // Destructor
    
    // Accessors
    std::string moduleName() {return m_name;}             // Name of the module 
    std::string connName();                               // Geogaphical name
    std::string pp0Name();                                // PP0 name
    int pp0Pos() { return m_geomPosition; };              // PP0 position

    int nCol() { return m_geo.nCol();}
    int nRow() { return m_geo.nRow();}
    int nChip() { return m_geo.nChip();}
    //int mccFlavour() { return m_mccFlavour ;}
    int feFlavour() {return m_feFlavour; }
    int nChipCol() {return m_geo.nChipCol();}
    int nChipRow() {return m_geo.nChipRow();}
    PixGeometry geometry() {return m_geo;}
    void setModuleGeomConnName(std::string name){m_geomConnName = name;}
    std::string getModuleGeomConnName(){return m_geomConnName;}


    PixModuleGroup *getPixModGroup() {return m_group;}    // Pointer to Module Group
    PixMcc *pixMCC() {return m_mcc;}                      // Pointer to MCC
    //PixMccI1 *pixMCC1() {return m_mcc1;}                    // Pointer to MCC1
    PixFe  *pixFE(int nFE) {if(nFE>=0 && nFE<m_nFe)return m_fe[nFE];else return 0;}   // Pointers to FEs
    feIterator feBegin() { return m_fe.begin(); };
    feIterator feEnd()   { return m_fe.end(); };

    auto begin() { return std::begin(m_fe); };
    auto cbegin() { return std::cbegin(m_fe); };
    auto end() { return std::end(m_fe); };
    auto cend() { return std::cend(m_fe); };
    
    const Config &config() const { return *m_conf; };
    Config &config()  { return *m_conf; };
    void initConfig();
   
    // Configuration 
    void loadConfig(DbRecord *dbRecord);                          // Load a configuration from DB
    void saveConfig(DbRecord *dbRecord);                          // Save current configuration to DB

    bool writeConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, std::string ptag="PixModule_Tmp", unsigned int revision=0xffffffff);
    bool readConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, unsigned int revision=0xffffffff);
  
    void loadConfig(PixConnectivity *conn, unsigned int rev = 0); // Load a configuration from DB
    void saveConfig(PixConnectivity *conn, unsigned int rev = 0, bool newBE=false);  // Save current configuration to DB

    void setTemp();

    void saveChangedConfig(std::string tag); //! save config to Dbserver if it was modified
    void reloadConfig();                      //! reload config from DbServer, temporary changes are lost!

    void storeConfig(std::string name);            // Save a configuration in the map
    bool restoreConfig(std::string name);          // Restore a configuration from the map
    void deleteConfig(std::string name);           // Remove a configuration from the map

    //    void writeConfig();                            // Load configuration data into ROD memory
    void selectFEEnable(bool newDspCode);          // Save actual module config in memory
    //void selectFE(int iFE, bool newDspCode);     // Selects a single FE for FEbyFE scans
    void selectFE(int iFE, bool newDspCode);       // Selects a single FE for FEbyFE scans
    void disableFESelected();                      // Restore original config (stored in memory) and clean memory
    void changedCfg() {m_cfgChanged = true;};




    // Run control
    //void startHistoRun(int nEv, std::string histoName, int histoBin2) {} // Start a run with histogram output
    //void startVmeRun(int nEv) {}                                         // Start a run with VME output
    //void startSlinkRun(int nEv) {}                                       // Start a run with S-Link output
    //void stopRun() {}                                                    // Stop a run
    //int  nEvTaken() {                                                    // Return the number of events taken so far
    //  return 0;
    //}
    // Histogram handling
    //histo getHisto(std::string histoName) {}   // Read an histogram
    
    // Event handling
   // bool hitReady() {  // Test for pending hits
   //   return false;
   // }
    //int getHit() {     // Read an hit via VME
    //  return 0;
   // }

    // Facilities
    void setVcal(float charge, bool Chigh);
    //bool getModuleActive();
    //void setModuleActive(int active);

    // Scans
    //void thresholdScan(int nEv, int nStep, int vcalMin, int vcalMax, std::string histoName) {} // Perform a threshold scan
   // void tuneTDACScan(int nEv, std::string histoName) {}                                       // Start a TDAC tuning procedure
    //void tuneFDACScan(int nEv, std::string histoName) {}                                       // Start a FDAC tuning procedure
   // void monleakScan(int nEv) {}                                                               // Start a leakage current measurement

    //Accessors
    int moduleId() { return m_moduleId; }
    int groupId() { return m_groupId; }
    bool &useAltLink() { return m_useAltLink; };


  protected:
    void relinkFEConfigs();
	 
    // Module name
    std::string m_name;
    
    // Pointer to parent Pixel Module Group
    PixModuleGroup* m_group;
    
    // Pointer to child  MCC
    PixMcc* m_mcc;
    
    // Pointers to child FEs
    std::vector<PixFe*> m_fe;
    
    // Configuration structure    
    Config *m_conf;
    DbRecord *m_dbRecord;
    bool m_cfgChanged;
    //    PixelModule *m_modConfig;
    
    // Link Parameters
    int m_inputLink;
    int m_outputLink1;
    int m_outputLink2;
    int m_outputLink3;
    int m_outputLink4;
    int m_bocInputLink;
    int m_bocOutputLink1;
    int m_bocOutputLink2;
    int m_delay;
    int m_latency;
    int m_outputBandwidth;
    int m_moduleId;
    int m_groupId;
    int m_active;
    int m_present;
    int m_mccFlavour;
    int m_feFlavour;
    int m_nFe;
    PixGeometry m_geo;
    AssemblyType m_geomType;
    int m_geomPosition;
    int m_geomAssemblyId;
    std::string m_geomConnName;
    PP0Type m_pp0Type;
    int m_pp0Position;
    
    // Modules properties during scan
    bool m_configActive;
    bool m_triggerActive;
    bool m_strobeActive;
    bool m_readoutActive;

    // Misc
    bool m_localCfgStored;
    bool m_useAltLink;    
  };
  
}

#endif
