/////////////////////////////////////////////////////////////////////
// PixActionsMulti.h
// version 1.1
/////////////////////////////////////////////////////////////////////
//
// 18/04/06  Version 1.0 (CS)
//           Initial release - Single threaded version
//
// 07/09/06  Version 1.1 (CS)
//           IPC multithread version
//
//
// 09/02/07 Version 1.1.1 (GAO - NG)
//          Added methods for STControlNG
//



//! Class for the Pixel multiple actions

#ifndef _PIXLIB_ACTIONSMULTI
#define _PIXLIB_ACTIONSMULTI

#include <set>

#include "PixActionsIDL.hh"
#include "PixActions/PixActions.h"
#include "PixController/PixRunConfig.h"



// Forward declarations
namespace SctPixelRod {
  class VmeInterface;
}


namespace PixLib {

  //Forward declaration

  class PixActionsMulti : public PixActions {

    enum TagType{connTagC, connTagP, connTagA, connTagOI, confTag, modConfTag, confTagOI};
    
  public:
    
    // Constructor
    PixActionsMulti(std::string actionName, std::string managingBrokerName, std::string allocatingBrokerName,
		    std::set<std::string> subActions,
		    IPCPartition *ipcPartition);

    ~PixActionsMulti();

    // State transitions
    virtual void initRun          (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void initCal          (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void resetRod         (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void configureBoc     (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void setup            (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void configure        (std::string partName, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void connect          (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void prepareForRun    (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void startTrigger     (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void stopTrigger      (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void stopEB           (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void disconnect       (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void unconfigure      (PixActions::SyncType sync=PixActions::Synchronous);
    // Publishing method 
    virtual void probe            (PixActions::SyncType sync=PixActions::Synchronous);
    //Reset&Config mods
    virtual void resetmods        (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void listRODs         (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void disableFailed    (PixActions::SyncType sync=PixActions::Synchronous);
    // Basic hardware operations
    virtual void reset            (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void resetViset       (PixActions::SyncType sync=PixActions::Synchronous); 
    virtual void DCSsync          (PixActions::SyncType sync=PixActions::Synchronous);

    virtual void optoOnOff     (bool switchOn, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void lvOnOff       (bool switchOn, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void disable       (std::string objName, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void enable        (std::string objName, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void reset         (std::string objName, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void warmReconfig  (std::string mode, std::string ecr, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void recover  (std::string mode, std::string ecr, PixActions::SyncType sync=PixActions::Synchronous);
  
    // Configuration handling
    virtual void readConfig       (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void loadConfig       (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void sendConfig       (PixActions::SyncType sync=PixActions::Synchronous);
    // Scan
    virtual void abortScan        (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void abortConnTest    (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void setActionsConfig (PixActions::SyncType sync=PixActions::Synchronous);


    virtual void setPixScan   (std::string name,          PixActions::SyncType sync=PixActions::Asynchronous);
    virtual void setConnTest  (std::string name,          PixActions::SyncType sync=PixActions::Asynchronous);
    virtual void reloadConfig (std::string actionName,    PixActions::SyncType sync=PixActions::Synchronous);
    virtual void saveConfig   (std::string tag = "",      PixActions::SyncType sync=PixActions::Synchronous);
    
    virtual void getModuleActive      (std::string actionName, long int modID, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void execConnectivityTest (std::string name,       long int id,    PixActions::SyncType sync=PixActions::Synchronous);
    virtual void scan                 (std::string scanName,   long int id,    PixActions::SyncType sync=PixActions::Synchronous);


    virtual void selectFE        (std::string actionName, long int modID, long int FE,     PixActions::SyncType sync=PixActions::Synchronous);
    virtual void configSingleMod (std::string actionName, long int modID, long int maskFE, PixActions::SyncType sync=PixActions::Synchronous); 
    virtual void setHalfclockMod (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void presetScan      (std::string directory,  long int id,    long int val, int feflav, PixActions::SyncType sync=PixActions::Synchronous); // T

    virtual void setVcal      (float charge,   bool Chigh, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void incrMccDelay (float in_delay, bool calib, PixActions::SyncType sync=PixActions::Synchronous);

    virtual void setModuleActive(std::string actionName, long int modID, bool active, PixActions::SyncType sync=PixActions::Synchronous);

    virtual void test1 (std::string test,    PixActions::SyncType sync=PixActions::Synchronous);
    virtual void test2 (std::string test,    PixActions::SyncType sync=PixActions::Synchronous);

    //!Propagate ROD and TIM parameters to the PixActions private objects
    virtual void setRodBusyMask(long int mask, PixActions::SyncType sync=PixActions::Synchronous);

    //Rod Status
    virtual void rodStatus(bool& status);

    virtual std::map<std::string,bool> rodStatus();
    
    virtual void setRodActive(std::string actionName, bool active, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void setRodActive(bool active, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void getRodActive(std::string actionName, PixActions::SyncType sync=PixActions::Synchronous);

    virtual void allocate(const char* allocatingBrokerName);
    virtual void deallocate(const char* deallocatingBrokerName);

    void setRodEnable(std::string actionName, bool active); 
    std::map<std::string, bool> getRodEnable() {return m_rodEnable;}; 

    virtual std::string getConnTagC(const char* subActionName);
    virtual std::string getConnTagP(const char* subActionName);
    virtual std::string getConnTagA(const char* subActionName);
    virtual std::string getConnTagOI(const char* subActionName);
    virtual std::string getCfgTag(const char* subActionName);
    virtual std::string getModCfgTag(const char* subActionName);
    virtual std::string getCfgTagOI(const char* subActionName); 

    virtual const bool isHVOff( );

    virtual void reloadTags(std::string newCfgTag, std::string newCfgModTag, 
			    long int newCfgRev, long int newCfgModRev, PixActions::SyncType sync=PixActions::Synchronous);


    virtual std::vector<std::string> listSubActions(PixActions::Type type = PixActions::ANY_TYPE);

  private:
    void initEnable(IPCPartition *ipcPartition);
    virtual bool belongsToAction(std::string motherAction, std::string daughterAction);
    void callIPCMethod(std::string methodName, PixActions::SyncType sync, ipc::ActionInput input, bool checkRodEnable = true);

    void callIPCInOutMethod(std::string methodName, ipc::ActionOutput& output, bool checkRodEnable = true);

    ipc::ActionInput boolActionInput                 (bool bl);
    ipc::ActionInput intActionInput                  (long int integer);
    ipc::ActionInput stringActionInput               (std::string str);
    ipc::ActionInput stringBoolActionInput           (std::string str, bool bl);
    ipc::ActionInput stringLongActionInput           (std::string str, long lg);
    ipc::ActionInput stringLongLongActionInput       (std::string str, long lg1, long lg2);
    ipc::ActionInput stringLongLongIntActionInput    (std::string str, long lg1, long lg2, int int1);
    ipc::ActionInput stringStringActionInput         (std::string str1, std::string str2);
    ipc::ActionInput stringStringLongLongActionInput (std::string str1, std::string str2, long lg1, long lg2);
    ipc::ActionInput stringLongBoolActionInput       (std::string str, long lg, bool bl);
    ipc::ActionInput floatBoolActionInput            (float fl, bool bl);

    std::string getTag(const char* subActionName, PixActionsMulti::TagType type);
    
  private:
    
    // SW interfaces
    std::map<std::string,ipc::PixActions_var> m_subActions;
    std::map<std::string,bool> m_rodEnable;
    std::map<std::string,std::vector<std::string> > m_singleSubActions;

    std::string m_applName; 
    std::string m_info;
 
  };
}


#endif 
