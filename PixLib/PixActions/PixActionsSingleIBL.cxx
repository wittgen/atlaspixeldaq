////////////////////////////////////////////////////////////////////
// PixActionsSingleIBL.cxx
// version 1.1
/////////////////////////////////////////////////////////////////////
//
// 30/08/06  Version 1.0 (CS,NG,AO)
//           Initial release
//
// 07/09/06  Version 1.1 (CS)
//           New IPC version
//
//
// Febr - March 07 Version 1.1.1 (GAO)
//        Added methods for STControlNG
//

//! Class for the Pixel single ROD actions

#include "PixDbInterface/PixDBException.h"
#include "Config/ConfMask.h"
#include "PixController/PixController.h"
//#include "PixController/PixScanConfig.h"
#include "RootDb/RootDb.h"
//#include "PixController/PixScanConfig.h"
#include "PixFe/PixFe.h"
#include "PixMcc/PixMcc.h"
#include "PixController/PixScan.h"
#include "PixController/PPCppRodPixController.h"
#include "PixConnectivity/PartitionConnectivity.h"
#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixConnectivity/RodBocConnectivity.h"
#include "PixConnectivity/OBConnectivity.h"
#include "PixConnectivity/Pp0Connectivity.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "PixConnectivity/PixDisable.h"
#include "PixDcs/PixDcs.h"
#include "PixActions/PixActionsSingleIBL.h"

#include "DFThreads/DFStandardQueue.h"
#include "DFThreads/DFFastQueue.h"
#include "DFThreads/DFStandardPrioritizedQueue.h"
// #include "PixThreads/PixThreadsScan.h" //  No longer used.
#include "PixThreads/PixThread.h"
#include "PixThreads/PixScanTask.h"
#include "PixThreads/IblCppMonitorTask.h"

#include "PixFe/PixFeI4.h"
#include "PixFe/PixFeI3.h"

using namespace PixLib;
using namespace SctPixelRod;

//! Constructor
PixActionsSingleIBL::PixActionsSingleIBL(std::string actionName, std::string managingBrokerName, 
					 std::string allocatingBrokerName, IPCPartition *ipcPartition, 
					 RodBocConnectivity *rod, 
					 PixConnectivity *conn) :
  PixActions(ipcPartition, actionName.c_str()), m_moduleGroup(NULL), 
  m_pixScan(NULL), m_pixScanTask(0),  m_rod(NULL), m_conn(conn), m_rodConn(rod), m_dbServer(NULL), m_hInt(NULL){

  // Set action attributes
  m_descriptor.name = actionName;
  if(allocatingBrokerName == "") m_descriptor.available = true;
  else m_descriptor.available = false;
  m_descriptor.type = SINGLE_ROD;
  m_descriptor.allocatingBrokerName = allocatingBrokerName;
  m_descriptor.managingBrokerName = managingBrokerName;
  m_runName = "";
  m_domainName = "";
  m_isRoot = "";
  if (m_rodConn != NULL) {
    if (m_rodConn->crate() != NULL) {
      m_isRoot = m_rodConn->crate()->name();
      m_isRoot += "/";
    }
    m_isRoot += m_rodConn->name(); 
    m_isRoot += "/";
  }    
  m_actStart = time(NULL);
  m_rodRev = 0; 
  m_runRev = 0;
  m_cfgTag = "";
  m_rodMonitor = 0;
  m_autoDisTask =0;
  m_pixScanNumber = 0;
  m_isSetup = false;
  m_preAmpKilled = false;
  m_configured = false;
  m_runStarted = false;
  m_isPixelRunParams = NULL;
  m_isPixelDDC = NULL;
  m_isDTRunParams = NULL;
  m_ipcDTPartition = NULL;
  m_actionStatus = 0;
  m_feFlav = 0;
  // Setup action
  setup();
  publishDescriptor();
}

PixActionsSingleIBL::PixActionsSingleIBL(std::string actionName, std::string managingBrokerName, PixDbServerInterface *DbServer, 
					 PixHistoServerInterface *hInt,
					 std::string allocatingBrokerName, IPCPartition *ipcPartition, 
					 RodBocConnectivity *rod, 
					 PixConnectivity *conn) :
  PixActions(ipcPartition, actionName), m_moduleGroup(NULL), m_pixScan(NULL), m_pixScanTask(0), 
  m_rod(NULL), m_conn(conn), m_rodConn(rod), m_dbServer(DbServer), m_hInt(hInt) {

  // Set action attributes
  m_descriptor.name = actionName;
  if(allocatingBrokerName == "") m_descriptor.available = true;
  else m_descriptor.available = false;
  m_descriptor.type = SINGLE_ROD;
  m_descriptor.allocatingBrokerName = allocatingBrokerName;
  m_descriptor.managingBrokerName = managingBrokerName;
  m_runName = "";
  m_rodRev = 0; 
  m_runRev = 0;
  m_rodMonitor = nullptr;
  m_autoDisTask =nullptr;
  m_pixScanNumber = 0;


  m_cfgTag    = m_conn->getCfgTag();
  m_cfgModTag = m_conn->getModCfgTag();
  m_domainName = "Configuration-"+m_conn->getConnTagOI();
  
  m_isRoot = "";
  if (m_rodConn != NULL) {
    if (m_rodConn->crate() != NULL) {
      m_isRoot = m_rodConn->crate()->name();
      m_isRoot += "/";
    }
    m_isRoot += m_rodConn->name(); 
    m_isRoot += "/";
  }    
  m_actStart = time(NULL);
  m_thread = new PixThread(std::string("Thread:")+actionName, m_is, m_isRoot);
  m_thread->startExecution(); 


  // Setup action
  m_isSetup = false;
  m_preAmpKilled = false;
  m_configured = false;
  m_runStarted = false;
  m_isPixelRunParams = NULL;
  m_isPixelDDC = NULL;
  m_isDTRunParams = NULL;
  m_ipcDTPartition = NULL;
  m_actionStatus = 0;
  std::cout << "SetupDb" << std::endl;
  setupDb();
  std::cout << "Publishing descriptor" << std::endl;
  publishDescriptor();
}

PixActionsSingleIBL::~PixActionsSingleIBL() 
{
  if(m_thread){
    m_thread->stopExecution();
    delete m_thread;
  }
  // Delete module group pointer
  if(m_moduleGroup != NULL) delete m_moduleGroup;
  if(m_pixScan != NULL) delete m_pixScan;
}

//! State transitions

void PixActionsSingleIBL::initRun(PixActions::SyncType )
{
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;

  if(m_isSetup) {
    // std::cout << "Configuring ROD " << name() << std::endl;
    configure("");
    //std::cout << "Configured ROD " << name() << ". Resetting" << std::endl;
    reset();   
    //std::cout << "Reset ROD " << name() << ". Loading" << std::endl;
    loadConfig();
    //std::cout << "Loaded ROD " << name() << std::endl

    PixThread::ScopeLock myLock(m_thread,"initRun");
    m_configured = false;
    m_runStarted = false;
  } else {
    retrySetupDb("initRun");
    if (m_isSetup) initRun();
    else printFailSetupDb("initRun");
  }
}

void PixActionsSingleIBL::retrySetupDb(std::string methodName) 
{
  m_info = "No PixModuleGroup found while executing method " + methodName +". Retrying...";
  ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
  setupDb();
}

void PixActionsSingleIBL::printFailSetupDb(std::string methodName)
{
  m_info = "Failed to create ModuleGroup after two attempts. Second attempt initiated by method " + methodName;
    ers::fatal(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
}


std::string PixActionsSingleIBL::getRodName(){

    if (m_moduleGroup != NULL) {
      return m_moduleGroup->getName();
    } else {
      return m_descriptor.name;
    }
}


void PixActionsSingleIBL::initCal(PixActions::SyncType)
{
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;
  if (m_isSetup) {
    try {
      std::cout << "GETTING ROD CONTROLLER ON ACTION " << name() << std::endl;
      m_rod = m_moduleGroup->getPixController();
    } catch (...) {
      m_info = "initCal() - Exception thrown while getting PixController";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }
    if (m_rod == NULL){
      m_info = "initCal() - Problem getting PixController.";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }
    if(m_rodMonitor) unSetupRodMonitoring();
    if(m_autoDisTask) unSetupPixAutoDis();
    reset();
    loadConfig();

  } else {
    retrySetupDb("initCal");
    if (m_isSetup) initCal();
    else printFailSetupDb("initCal");
  }
}

void PixActionsSingleIBL::setup(PixActions::SyncType ) {
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;
  
  publishAction("setup");  

  try {
    std::cout << "CREATING MODULE GROUP ON ACTION " << name() << std::endl;
    m_moduleGroup = new PixLib::PixModuleGroup(m_conn, m_rodConn->name(), m_ipcPartition->name());
    publishStatus("SUCCESS");
    m_info = "Created module Group on action "+name();
    ers::log(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
  }
  catch(const PixConnectivityExc &e) { 
    m_info = "Exception Caught in setup "+ e.getDescr();
    ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    publishStatus("FAILURE");
  }
  catch (const PixModuleGroupExc &e){
    m_info = "Exception Caught in setup "+ e.getDescr();
    ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    publishStatus("FAILURE");
  }
  catch (const PixDBException &e){
    std::ostringstream ostr;
    e.what(ostr);
    m_info = "Exception Caught in setup "+ ostr.str() ;
    ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    publishStatus("FAILURE");
  }  
  catch (...){
    m_info = "Unknown exception caught in setup";
    ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    publishStatus("FAILURE");
  } 

}

void PixActionsSingleIBL::setupDb(PixActions::SyncType ) 
{
  std::cout << "CREATING MODULE GROUP ON ACTION " << name() << std::endl;
  publishAction("setupDb");
  try {
    std::string ddcPartitionName = "";
    char* ddcPart = getenv("PIX_DDC_PARTITION");
    if (ddcPart != NULL) ddcPartitionName = ddcPart;    
    if (ddcPartitionName == "") {
      m_info = "ddcPartitionName is not set";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      m_dcs = NULL;
    } else {
      IPCPartition *ddcPartition;
      ddcPartition = new IPCPartition(ddcPartitionName);
      m_dcs = new PixDcs(ddcPartition, "DDC", m_rodConn->name());
      m_info = "Created dcs on partition " + ddcPartitionName+" by action "+ name();
      ers::log(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }
  } catch (...) {
     m_info = "Problem in creating PixDcs object in action "+ name();
     ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
  }

  try {
    m_moduleGroup = new PixLib::PixModuleGroup(m_conn, m_dbServer, m_dcs, m_rodConn->name(), m_ipcPartition->name());
    m_info = "Created PixModuleGroup on action "+name();
    m_feFlav = m_moduleGroup->getFeFlav();
    std::cout << "FE-flavour loaded : " << m_feFlav << std::endl;
    ers::log(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    m_rodRev = ( m_moduleGroup->config() ).rev();
    m_info = "created module rev " + name();
    ers::info(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    m_modsRev.clear();
    for(std::vector<PixModule*>::iterator it = m_moduleGroup->modBegin(); it != m_moduleGroup->modEnd(); it++)
      m_modsRev.insert( std::make_pair((*it), ((*it)->config()).rev()) );
    m_info =  "created modules " + name();
    ers::info(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    m_isSetup = true;
    publishStatus("SUCCESS");
  } catch(const PixConnectivityExc &e) { 
    m_info = "PixConnectivity Exception Caught in setupDb "+ e.getDescr();
    ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    publishStatus("FAILURE");
  } catch (const PixModuleGroupExc &e) {
    m_info = "PixModuleGroup Exception Caught in setupDb"+ e.getDescr();
    ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    publishStatus("FAILURE");
  } catch (...) {
    m_info = "Unknown Exception Caught in setupDb";
    ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    publishStatus("FAILURE");
  }
}


void PixActionsSingleIBL::setNewCfg(PixConnectivity *newConn,  RodBocConnectivity *newRodBocConn)
{
  if(m_rodMonitor ) unSetupRodMonitoring();
  if(m_autoDisTask) unSetupPixAutoDis();

  // Delete old PixModuleGroup
  if(m_moduleGroup != NULL) {
      delete m_moduleGroup;
      m_moduleGroup=0;
  }

  //Create new PixModuleGroup with new connectivity
  m_rodConn = newRodBocConn;
  try{
    m_conn = newConn;
    m_cfgTag    = newConn->getCfgTag();
    m_cfgModTag = newConn->getModCfgTag(); 
    m_domainName = "Configuration-"+newConn->getConnTagOI();
  } catch (const PixConnectivityExc &e){
    m_info = "Problem getting new tags. "+ e.getDescr();
    ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    publishStatus("FAILURE");
  } catch(...){
    m_info = "Unknown exlusion caught in setNewCfg()";
    ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    publishStatus("FAILURE");
  }
  m_isSetup = false;
  try {
    std::cout << "CREATING MODULE GROUP ON ACTION " << name() << std::endl;
    std::string ddcPartitionName = "";
    char* ddcPart = getenv("PIX_DDC_PARTITION");
    if (ddcPart != NULL) ddcPartitionName = ddcPart;    
    if (ddcPartitionName == "") {
      m_info = "ddcPartitionName is not set";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      m_dcs = NULL;
    } else {
      IPCPartition *ddcPartition;
      ddcPartition = new IPCPartition(ddcPartitionName);
      m_dcs = new PixDcs(ddcPartition, "DDC", m_rodConn->name());
      m_info = "Created dcs on partition " + ddcPartitionName+" by action "+ name();
      ers::log(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }
  } catch (...) {
     m_info = "Problem in creating PixDcs object in action "+ name();
     ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
  }

  try {
    m_moduleGroup = new PixLib::PixModuleGroup(newConn, m_dbServer, m_dcs, newRodBocConn->name(), m_ipcPartition->name());
    m_info = "Created module Group on action "+name();
    ers::log(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    m_rodRev = ( m_moduleGroup->config() ).rev();
    m_info = "created module rev " + name();
    ers::info(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    m_modsRev.clear();
    for(std::vector<PixModule*>::iterator it = m_moduleGroup->modBegin(); it != m_moduleGroup->modEnd(); it++)
      m_modsRev.insert( std::make_pair((*it), ((*it)->config()).rev()) );
    m_info =  "created modules" + name();
    ers::info(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    m_isSetup = true;
    publishStatus("SUCCESS");
  } catch(const PixConnectivityExc &e) {
    m_info = "PixConnectivity Exception Caught in setNewCfg"+ e.getDescr();
    ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    publishStatus("FAILURE");
  } catch (const PixModuleGroupExc &e) {
    m_info = "PixModuleGroup Exception Caught in setNewCfg "+ e.getDescr();
    ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    publishStatus("FAILURE");
  } catch (...) {
    m_info = "Unknown exception caught in setNewCfg";
    ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    publishStatus("FAILURE");
  }
}

void PixActionsSingleIBL::reloadTags(std::string newCfgTag, std::string newCfgModTag, 
				     long int cfgRev, long int modCfgRev, PixActions::SyncType sync) 
{
  // Waiting for Paolo ...
  std::stringstream message;
  message << "Hello! I'm in  PixActionsSingleIBL::reloadTags(" << newCfgTag << ", " << newCfgModTag << ", " << cfgRev << ", " << modCfgRev <<")";
}

void PixActionsSingleIBL::setupRodMonitoring(unsigned int bufRate, unsigned int statusRate, unsigned int busyRate,
                                             unsigned int calRate, unsigned int occupRate, unsigned int monPrescale,
					     unsigned int quickStatusMode,  unsigned int quickStatusConfig, 
					     unsigned int MODsyncConfig, unsigned int RODsyncConfig)
{
  if (m_actionStatus < 1) {
    m_info = "ROD not initialized, cannot start DSP Monitoring";
    ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    unSetupRodMonitoring();
    return;
  }
  PixController* ctrl = m_moduleGroup->getPixController();
  PPCppRodPixController *rod = dynamic_cast<PPCppRodPixController *>(ctrl);
  if (rod){

    std::cout<<"Have controller name: "<<rod->getCtrlName()<<std::endl;

    // this is not needed with the IblCppMonitorTask
    //if (rod->getModGroup().getCtrlType() != ROD ) return; // Karolos: run only for RodPixController
    //if (rod->getModGroup().getCtrlType() == L12ROD ) return; // Karolos: don't run (yet) for BarrelRodPixController

    try{
      std::cout << "start monitoring" << std::endl;
      IblCppMonitorTask* mon;
        mon = new IblCppMonitorTask(m_moduleGroup, m_hInt, m_runName, std::string("Monitoring:")+m_descriptor.name,
                                                        rod, m_ipcPartition->name(),
				                        monPrescale, occupRate, calRate, statusRate, busyRate, bufRate, quickStatusMode, quickStatusConfig, 
							MODsyncConfig, RODsyncConfig);
      m_thread->setNewTask(mon);
      m_rodMonitor = mon;
    } catch (...) {
      m_info = "Exception caught in setupRodMonitoring()";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }
  } else {
    m_info = "Error defining Controller in setupRodMonitoring()";
    ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
  }
}

void PixActionsSingleIBL::unSetupRodMonitoring()
{
  m_thread->removeTask(std::string("Monitoring:")+m_descriptor.name);
  m_rodMonitor = nullptr;
}

void PixActionsSingleIBL::setupPixAutoDis( ){
    try{
      std::cout << "start autodisable task" << std::endl;
      PixAutoDisTask* adis = new PixAutoDisTask(m_moduleGroup, std::string("AutoDis:"+ m_descriptor.name) );
      m_thread->setNewTask(adis);
      m_autoDisTask = adis;
    } catch (...) {
      m_info = "Exception caught in setupPixAutoDis()";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }
}

void PixActionsSingleIBL::unSetupPixAutoDis(){
  m_thread->removeTask(std::string("AutoDis:")+m_descriptor.name);
  m_autoDisTask = nullptr;
}

void PixActionsSingleIBL::configure(std::string partNameArg, PixActions::SyncType ) 
{
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;
  if (m_isSetup){

    // Disable ROD monitoring
    if(m_rodMonitor) unSetupRodMonitoring();
    if(m_autoDisTask) unSetupPixAutoDis();

    try{
      m_thread->lock();
    } catch(DFMutex::AlreadyLockedByThread &v){
      PIX_ERROR( getRodName()<<" Exception caught when trying to lock thread." << v.what());
    }

    m_runPartName = partNameArg;

    publishAction("configure");
    
    m_configured = true;
    m_runStarted = false;

    // Retrieve ROD controller
    std::cout << "GETTING ROD CONTROLLER ON ACTION " << name() << std::endl;
    m_rod = m_moduleGroup->getPixController();
    if (m_rod == NULL){
      m_info = "Error getting PixController in configure()";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }
    
    m_runName = "";
    
    try {
      if (m_ipcDTPartition == NULL) {
	m_ipcDTPartition = new IPCPartition(partNameArg);
      } else {
	if (partNameArg != m_ipcDTPartition->name()) {
	  delete m_ipcDTPartition;
          m_ipcDTPartition = NULL;
	  m_ipcDTPartition = new IPCPartition(partNameArg);
	} else {
	  if (!m_ipcDTPartition->isValid()) {
	    delete m_ipcDTPartition;
	    m_ipcDTPartition = NULL;
	    m_ipcDTPartition = new IPCPartition(partNameArg);
	  }
	}
      }
      if (m_ipcDTPartition != NULL) {
	if (m_isDTRunParams != NULL) delete m_isDTRunParams;
	m_isDTRunParams = new PixISManager(m_ipcDTPartition, "RunParams");
	m_info = "Created ISManager for DT RunParams (" + partNameArg + ")";
	ers::info(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	m_moduleGroup->setDataTakingIS(m_isDTRunParams);
      }
    }
    catch(...) { 
      m_info = "Unknown exception caught while creating ISManager for DT RunParams (" + partNameArg + ")";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      m_isDTRunParams = NULL;
    }

    try {
      if (m_isPixelRunParams == NULL) {
	m_isPixelRunParams = new PixISManager(m_ipcPartition, "PixelRunParams");
      }
    }
    catch(...) { 
      m_info = "Unknown exception caught while creating ISManager for PixelRunParams";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      m_isPixelRunParams = NULL;
    }
    if ( m_isPixelRunParams!=NULL) {
      try {
        // Karolos: pick-up DataTakingConfigIBL-RunConfig if set
        if(m_isPixelRunParams->exists("DataTakingConfigIBL-RunConfig")) {
          m_runName = m_isPixelRunParams->read<string>("DataTakingConfigIBL-RunConfig");
          m_info = "Using RunConfig from DataTakingConfigIBL-RunConfig";
          ers::warning(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
        } else m_runName = m_isPixelRunParams->read<string>("DataTakingConfig-RunConfig");
      } catch (...) {
	m_info = "Cannot read PixRunconfig name from IS, default name is Global"; 
	ers::warning(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      }	
    }
    if (m_runName=="") m_runName="Global";

    m_run = new PixRunConfig(m_runName);
    if(m_runName != "") {
      if(m_dbServer != NULL) {
	try{
	  m_run= m_conn->getRunConfig(m_runName);
    } catch (const PixDBException &e){
	  std::ostringstream ostr;
	  e.what(ostr);
	  m_info = "PixDB Exception caught in configure() "+ ostr.str() ;
	  ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    } catch (const PixConnectivityExc &e){
	  m_info = "PixConnectivity Exception caught in configure() "+ e.getDescr();
	  ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	} catch (...) {
	  m_info = "Unknown Exception caught in configure()" ;
	  ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	}
	m_info = "RunConfig with name "+ m_runName+" has been loaded from DbServer";
	ers::info(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	m_runRev = ( m_run->config() ).rev();
      } else {
	m_info = "The PixActions is configured without DbServer or with a bad pointer to it. Taking default configuration for PixRunConfig";
	ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      }
    } else {
      m_info = "No PixRunConfig names were provided. Cannot correctly set PixRunConfig parameters!";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }
    
    if (m_run==NULL) {
      m_info = "PixRunConfig pointer is NULL; dafult object will be created!";
      m_run = new PixRunConfig(m_runName);
    }
    
    std::vector<std::string> myList=m_run->listSubConf();
    std::cout << "RunConfig " << m_run->getName() << " has " << myList.size() << std::endl;
    for (unsigned int i=0; i< myList.size(); i++)  std::cout << "--> SubConfig " << myList[i] << std::endl;
    
    std::string partName=m_rodConn->crate()->partition()->name();
    std::string crateName=m_rodConn->crate()->name();
    std::string rodName=m_rodConn->name();
    std::cout << "Looking for subconfig: " << partName << "/" << crateName <<  "/" << rodName << std::endl;
    m_run->selectSubConf(partName,crateName,rodName);
    

    // Module configuration update: here, only revision changes are supported
    bool result;
    bool inLoop=false;
    if(m_dbServer != NULL) {
      for(std::map<PixModule*, unsigned int>::iterator it = m_modsRev.begin(); it != m_modsRev.end(); it++) {
	if( ((*it).second) != m_dbServer->getLastRev(m_domainName,m_cfgModTag,it->first->moduleName())) {
	  inLoop=true;
	  std::cout << std::endl << "Dumping parameter for Module " << it->first->moduleName() << std::endl;
	  std::cout << "moduleId : " << it->first->moduleId() << std::endl;
	  std::cout << "groupId  : " << it->first->groupId()  << std::endl;
	  std::cout << "ConnName : " << it->first->connName() << std::endl;
	  std::cout << "pp0Name  : " << it->first->pp0Name() << std::endl;
	  std::cout << "geomConn : " << it->first->getModuleGeomConnName() << std::endl;
	  std::cout << "geomPos  : " << it->first->pp0Pos() << std::endl;
	  

	  result=( (*it).first )->readConfig(m_dbServer, m_domainName, m_cfgModTag);
	  if ( !result){
	    m_info = "Cannot read config for module "+it->first->moduleName()+" from DbServer";
	    ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	  }  
	  std::cout << std::endl << "Dumping parameter AFTER UPDATE for Module " << it->first->moduleName() << std::endl;
	  std::cout << "moduleId : " << it->first->moduleId() << std::endl;
	  std::cout << "groupId  : " << it->first->groupId()  << std::endl;
	  std::cout << "ConnName : " << it->first->connName() << std::endl;
	  std::cout << "pp0Name  : " << it->first->pp0Name() << std::endl;
	  std::cout << "geomConn : " << it->first->getModuleGeomConnName() << std::endl;
	  std::cout << "geomPos  : " << it->first->pp0Pos() << std::endl;
	}
      }
    } else {
      m_info = "Trying to read from DbServer but pointer is NULL or this PixAction is not configured for DbServer usage. Cannot update module configurations";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }
    if (inLoop) {
      m_moduleGroup->updateModParFromConn();
    }
    m_info = "Module configuration have been updated";

    try {
      m_thread->unlock();
    } catch(DFMutex::NotLockedByThread &v){
      PIX_ERROR( getRodName()<<" Exception caught when trying to unlock thread." << v.what());
    }

    std::cout << "RESETTING RODS ON ACTION " << name() << std::endl;
    reset();

    try{
      m_thread->lock();
    } catch(DFMutex::AlreadyLockedByThread &v){
      PIX_ERROR( getRodName()<<" Exception caught when trying to lock thread." << v.what());
    }

    // Get RUN infos from PixRunConfig
    std::cout << std::endl << "-->> Run configuration for action called " << m_descriptor.name << "  is called " << m_run->getName() << std::endl;
    std::cout << "internalGen       = " << m_run->internalGen() << std::endl;
    std::cout << "consecutiveLVL1   = " << m_run->consecutiveLVL1() << std::endl;
    std::cout << "consecutiveLVL1_IBL=" << m_run->consecutiveLVL1_IBL() << std::endl; // Using consecutiveLVL1() if consecutiveLVL1_IBL() == 255
    std::cout << "latency           = " << m_run->latency() << std::endl;
    std::cout << "injPix/mod        = " << m_run->nPixInject() << std::endl;
    std::cout << "digitalInject     = " << m_run->digitalInject() << std::endl;
    std::cout << "cInjectionHigh    = " << m_run->cInjHi() << std::endl;
    std::cout << "Vcal              = " << m_run->vcal() << std::endl;
    std::cout << "injectMode        = " << m_run->injectMode() << std::endl;
    std::cout << "MCC cal delay     = " << m_run->mccCalDelay() << std::endl;
    std::cout << "RoD Cal delay     = " << m_run->rodCalDelay() << std::endl;
    std::cout << "MCC cal width     = " << m_run->mccCalWidth() << std::endl;
    std::cout << "rodBcidOffset     = " << m_run->rodBcidOffset() << std::endl;
    std::cout << "Simulat Enabled   = " << m_run->simActive() << std::endl;
    std::cout << "Int Gen Enable    = " << m_run->internalGen() << std::endl;
    std::cout << "Simulat Mode      = " << m_run->simMode() << std::endl; 
    std::cout << "Sim LVL1Accept    = " << m_run->simLVL1Accept() << std::endl;
    std::cout << "simLVL1 skipped   = " << m_run->simSkippedLVL1() << std::endl;
    std::cout << "sim BitFlip       = " << m_run->simBitFlip() << std::endl;
    std::cout << "sim MCCFlags      = " << m_run->simMCCFlags() << std::endl;
    std::cout << "sim FEFlags       = " << m_run->simFEFlags() << std::endl;
    std::cout << "sim ForMask       = " << m_run->simFormattersMask() << std::endl;
    std::cout << "simModOccupancy   = " << m_run->simModOccupancy() << std::endl;
    std::cout << "startPreAmpKilled = " << m_run->startPreAmpKilled() << std::endl;
    std::cout << "autoDisable       = " << m_run->autoDisable() << std::endl;
    std::cout << "rawDataMode       = " << m_run->rawDataMode() << std::endl;
    std::cout << "rodMonPrescale    = " << m_run->rodMonPrescale() << std::endl;
    std::cout << "dataTakingSpeed   = " << m_run->dataTakingSpeed() << std::endl;
    std::cout << std::endl;
		
		if ( m_is != NULL) {
			m_is->publish(m_moduleGroup->getISRoot()+"LATENCY",m_run->latency());

			if(m_moduleGroup->getCtrlType() == CPPROD && m_run->consecutiveLVL1_IBL() != 255)m_is->publish(m_moduleGroup->getISRoot()+"LVL1",m_run->consecutiveLVL1_IBL());
			else m_is->publish(m_moduleGroup->getISRoot()+"LVL1",m_run->consecutiveLVL1());
		}
		
    // To-do: move this to the controller.... should not be in actions

    std::cout<<"m_feFlav = "<<m_feFlav<<std::endl;
    // Loop over Modules
    for (PixModuleGroup::moduleIterator module = m_moduleGroup->modBegin(); module !=  m_moduleGroup->modEnd(); module++) {
      // FEs loop 
      std::vector<PixFe*> FrontEnd;
      for (std::vector<PixFe*>::iterator fe = (*module)->feBegin(); fe != (*module)->feEnd(); fe++){
	FrontEnd.push_back(*fe);
	auto fei4 = dynamic_cast<PixFeI4*>(*fe);
	auto fei3 = dynamic_cast<PixFeI3*>(*fe);

	if(fei4) {
	  fei4->writeGlobRegister(Fei4::GlobReg::Trig_Lat, m_run->latency());
      fei4->writeGlobRegister(Fei4::GlobReg::PlsrDAC, m_run->vcal());
	} else {
	  fei3->writeGlobRegister(Fei3::GlobReg::Latency, m_run->latency());
      fei3->writeGlobRegister(Fei3::GlobReg::VCal, m_run->vcal());
	}

	if (m_run->digitalInject()) {
	  if ((m_feFlav == PixModule::PM_FE_I4B) || (m_feFlav == PixModule::PM_FE_I4A))
        fei4->writeGlobRegister(Fei4::GlobReg::DHS, 1);
	  else
	    fei3->writeGlobRegister(Fei3::GlobReg::EnableDigInj, 1);
	  ConfMask<bool>&  enable = fei4 ? fei4->readPixRegister(Fei4::PixReg::Enable)
	                                 : fei3->readPixRegister(Fei3::PixReg::Enable);
	  enable.disableAll();
	} else {
     if ((m_feFlav == PixModule::PM_FE_I4B) || (m_feFlav == PixModule::PM_FE_I4A))
       fei4->writeGlobRegister(Fei4::GlobReg::DHS, 0);
     else
       fei3->writeGlobRegister(Fei3::GlobReg::EnableDigInj, 0);
	}
	if (m_run->cInjHi()) {
	  // to-do: which reg to write for fei4?
	  if ((m_feFlav == PixModule::PM_FE_I4B) || (m_feFlav == PixModule::PM_FE_I4A));//std::cout<<"still need to implement cinjHi for Fei4 when data taking"<<std::endl;
	  else fei3->writeGlobRegister(Fei3::GlobReg::HighInjCapSel, 1);
	} 
	else {
	  // to-do: which reg to write for fei4?
	  if ((m_feFlav == PixModule::PM_FE_I4B) || (m_feFlav == PixModule::PM_FE_I4A));//std::cout<<"still need to implement cinjHi for Fei4 when data taking"<<std::endl;
	  else fei3->writeGlobRegister(Fei3::GlobReg::HighInjCapSel, 0);
	}
      }
      
      // 27 Aug 2007 new injection masks - VD
      
      // This should not be in PixActions. It is not flavor robust for Fei4 and returns before finishing
      // To-do: move to the controller or implement for fei4

      if (!((m_feFlav == PixModule::PM_FE_I4B) || (m_feFlav == PixModule::PM_FE_I4A))) {
  //This is for FEI3
	int nPix = m_run->nPixInject();
	unsigned int ife=0, icol=0, irow=0;
	int step=m_run->injStep();
	int start=m_run->injStartPoint();
	
      PixRunConfig::InjectionType injMode= m_run->injType();
      // disable all pixels before enabling some of them according to injection strategy
      for (int l=0;l<16;l++) {
	ConfMask<bool>&  select = dynamic_cast<PixFeI3*>(FrontEnd[l])->readPixRegister(Fei3::PixReg::Select);
	select.disableAll();
      }
      int matrix[18][160];
      
      for (int j=0; j<18; j++) { 
	for (int k=0; k<160; k++) {
	  matrix[j][k]=0;
	}
      }

      if (injMode ==  PixRunConfig::NOINJ) {
	//std::cout << " Datataking without injection " << std::endl;
      } else if (injMode == PixRunConfig::RANDOM) {
	std::cout << " Datataking with random defined injection mask " << std::endl;
	srand(time(NULL));
	while (nPix-- > 0) {
	  ConfMask<bool>& enable = dynamic_cast<PixFeI3*>(FrontEnd[ife])->readPixRegister(Fei3::PixReg::Enable);
	  ConfMask<bool>& select = dynamic_cast<PixFeI3*>(FrontEnd[ife])->readPixRegister(Fei3::PixReg::Select);
	  long int x=rand();
	  icol=(int)(((double)x/(double)RAND_MAX)*18); 
	  x=rand();
	  irow=(int)(((double)x/(double)RAND_MAX)*160); 
	  if (m_run->digitalInject()) enable.enable(icol,irow);
	  else select.enable(icol,irow);
	  std::cout<< "Pixel enabled for injection, FE: " << ife << " col: " << icol << " row: " << irow<< std::endl;
	  if (ife==0) matrix[icol][irow]=1;
	  ife++; 
	  if (ife >= FrontEnd.size()) {
	    ife = 0;
	  }
	}
      } else if (injMode == PixRunConfig::ROW) {
	std::cout << " Datataking with constant row injection mask " << std::endl;
	//checking parameters consistencies
	
	if (start>=160) {
	  m_info = "StartPoint exceeds total row number (160)";
	  ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	  m_info = "Redefinition of StartPoint to default (0)";
	  ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	  start=0;
	}
      
	if (step>=160) {
	  m_info = "Step exceds total row number (160):: Injection only possible on one row.";
	  ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	  m_info = "Redefinition of Step to default (10).";
	  ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	  step=10;
	}
	if (start+step>=160) {
	  m_info = "StartPoint+Step exceds total row number (160):: Injection only possible on one row.";
	  ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	  //std::cout << " Warning:: Redefinition of StartPoint to default (0) " << std::endl << std::endl;
	  //start=0;
	}
      
	irow=start;      
	while (nPix-- > 0) {
	  ConfMask<bool>& enable = dynamic_cast<PixFeI3*>(FrontEnd[ife])->readPixRegister(Fei3::PixReg::Enable);
	  ConfMask<bool>& select = dynamic_cast<PixFeI3*>(FrontEnd[ife])->readPixRegister(Fei3::PixReg::Select);
	  if (m_run->digitalInject()) enable.enable(icol,irow);
	  else select.enable(icol,irow);
	  if (ife==0) matrix[icol][irow]=1;
	  ife++; 
	  if (ife >= FrontEnd.size()) {
	    ife = 0;
	    icol++; 
	    if (icol >= 18) {
	      icol = 0;
	      irow += step;
	    }
	  }
	}
      } else if (injMode == PixRunConfig::COLUMN) {
	std::cout << " Datataking with constant column injection mask " << std::endl;

	if (start>=18) {
	  m_info = "StartPoint exceds total row number (18).";
	  ers::warning(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	  m_info = "Redefinition of StartPoint to default (0).";
	  ers::warning(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	  start=0;
	}
      
	if (step>=18) {
	  m_info = "Step exceds total row number (18):: Injection only possible on one row.";
	  ers::warning(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	  m_info = "Redefinition of Step to default (5).";
	  ers::warning(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	  step=5;
	}
	if (start+step>=18) {
	  m_info = "StartPoint+Step exceds total row number (18):: Injection only possible on one row.";
	  ers::warning(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	  //std::cout << " Warning:: Redefinition of StartPoint to default (0) " << std::endl << std::endl;
	  //start=0;
	}
	
	std::cout << "The maximum number of hit that you can geometrically inject is : " << 16*160*(double)(18-start)/((double)step) << std::endl;
      
	icol=start;
      

	while (nPix-- > 0) {
	  ConfMask<bool>& enable = dynamic_cast<PixFeI3*>(FrontEnd[ife])->readPixRegister(Fei3::PixReg::Enable);
	  ConfMask<bool>& select = dynamic_cast<PixFeI3*>(FrontEnd[ife])->readPixRegister(Fei3::PixReg::Select);
	  if (m_run->digitalInject()) enable.enable(icol,irow);
	  else select.enable(icol,irow);
	  if (ife==0) matrix[icol][irow]=1;
	  ife++; 
	  if (ife >= FrontEnd.size()) {
	    ife = 0;
	    irow++; 
	    if (irow >= 160) {
	      irow = 0;
	      icol += step;
	    }
	  }
	}
      }
      if (injMode != PixRunConfig::NOINJ) {
	std::cout << "Dump of injection mask for 1st FE " << std:: endl;
	for (int j=0; j<160; j++) { 
	  for (int k=0; k<18; k++) {
	    std::cout << matrix[k][j] << " ";
	  }
	  std::cout <<std::endl;
	}
      }
      } // not fei4
    }

    
      std::cout<<"in function: "<<__PRETTY_FUNCTION__<<" at line: "<<__LINE__<<std::endl;

    // Select readout speed
    if (m_run->dataTakingSpeed() == 2) {
      m_moduleGroup->setReadoutSpeed(EnumMccBandwidth::SINGLE_80);
      m_isPixelRunParams->publish("DataTakingSpeed-"+m_rodConn->name(),"SINGLE_80");
    } else if (m_run->dataTakingSpeed() == 4) {
      m_moduleGroup->setReadoutSpeed(EnumMccBandwidth::DOUBLE_80);
      m_isPixelRunParams->publish("DataTakingSpeed-"+m_rodConn->name(),"DOUBLE_80");
    } else {
      m_moduleGroup->setReadoutSpeed(EnumMccBandwidth::SINGLE_40);
      m_isPixelRunParams->publish("DataTakingSpeed-"+m_rodConn->name(),"SINGLE_40");
    }

    try {
      m_thread->unlock();
    } catch(DFMutex::NotLockedByThread &v){
      PIX_ERROR( getRodName()<<" Exception caught when trying to unlock thread." << v.what());
    }
      
    // Load configs into the ROD
    if (m_actionStatus == 1) {
      std::cout << "LOADING CONFIG ON ACTION " << name() << std::endl;
      loadConfig();
      publishStatus("SUCCESS");
    } else {
      publishStatus("FAILURE");
    }
  
  } else {
    retrySetupDb("configure");
    if (m_isSetup) configure(partNameArg);
    else printFailSetupDb("configure");
  }
}

void PixActionsSingleIBL::connect(PixActions::SyncType ) 
{
  if (m_actionStatus < 1) {
    publishAction("connect");
    publishStatus("FAILURE");
    m_info = "ROD not initialized, aborting connect()";
    ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    return;
  }
  if (m_isSetup){

    // Disable ROD monitoring
    if(m_rodMonitor ) unSetupRodMonitoring();
    if(m_autoDisTask) unSetupPixAutoDis();
    
    try{
      m_thread->lock();
    }catch(DFMutex::AlreadyLockedByThread &v){
      m_info = "Exception caught when trying to lock thread in disable().";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }

    publishAction("connect");

    // Configure modules
    std::cout << "SENDING CONFIG ON ACTION " << name() << std::endl;
    m_preAmpKilled = true;
    if (m_run->startPreAmpAuto()) {
      int stableBeams = 0;
      int stableBeamsOverride = 0;
      try {
	if (m_isPixelDDC == NULL) {
	  m_isPixelDDC = new PixISManager(m_ipcPartition, "DDC");
	}  
      }
      catch(...) { 
	m_info = "Unknown exception caught while creating ISManager for DDC";
	ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	m_isPixelDDC = NULL;
      }
      if ( m_isPixelDDC != NULL) {
	try {
	  stableBeams = m_isPixelDDC->readDcs<int>("StableBeamsFlag");
	  std::ostringstream os;
	  os << "Stable Beam flag from IS = " << stableBeams;
          m_info = os.str(); 
	  ers::info(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	} catch (...) {
	  m_info = "Cannot read Stable Beam flag name from IS"; 
	  ers::warning(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	}	
	if (stableBeams > 0) {
	  m_preAmpKilled = false;
	} else {
	  try {
	    stableBeamsOverride = m_isPixelDDC->read<int>("StableBeamsOverride");
	    std::ostringstream os;
	    os << "Stable Beams Override from IS = " << stableBeamsOverride;
	    m_info = os.str(); 
	    ers::info(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	  } catch (...) {
	    m_info = "Cannot read Stable Beams Override flag from IS"; 
	    ers::warning(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	  }	
	  if (stableBeamsOverride > 0) {
	    m_preAmpKilled = false;
	  }
	}
      }
    } else if (!m_run->startPreAmpKilled()) {
      m_preAmpKilled = false;
    }

    try{
      m_thread->unlock();
    }catch(DFMutex::NotLockedByThread &v){
      m_info = "Exception caught when trying to unlock thread in disable().";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }

      if  ( !(m_run->internalGen() || m_run->simActive()) ){
       sendConfig();
      } else {
        ers::warning(PixLib::pix::daq (ERS_HERE,getRodName(),"Skipping send config in emulator mode"));
      }

    if (!m_rodMonitor) setupRodMonitoring(m_run->bufRate(),
					      m_run->statusRate(),
					      m_run->busyRate(),
					      0,
					      m_run->occupRate(),
					      m_run->rodMonPrescale(),
					      m_run->quickStatusMode(),
					      m_run->quickStatusConfig(),
					      m_run->syncConfig(),
					      m_run->syncConfigROD());
    
    
    publishStatus("SUCCESS");

  } else {
    retrySetupDb("connect");
    if (m_isSetup) connect();
    else printFailSetupDb("connect");
  }
}


void PixActionsSingleIBL::prepareForRun(PixActions::SyncType ) {
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;

  if (m_actionStatus < 1) {
    publishAction("prepareForRun");
    publishStatus("FAILURE");
    m_info = "ROD not initialized, aborting prepareForRun()";
    ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    return;
  }
  if (m_isSetup) {
    PixThread::ScopeLock myLock(m_thread,"prepareForRun");
    publishAction("prepareForRun");

    m_configured = true;
    m_runStarted = true;

    std::string eoff = "OFF";
    if (m_run->autoDisable()) {
      eoff = "ON";
    }
    if (m_isDTRunParams) {
      std::stringstream ss;
      ss << "Publishing AUTO_DISABLE_ENA " << eoff << " from run config" << m_run->getName() << std::endl;
      m_info = ss.str();
      ers::info(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      m_isDTRunParams->publish(m_moduleGroup->getISRoot()+"AUTO_DISABLE_ENA", eoff);

      //Publish autoRecover
      m_isDTRunParams->publish(m_moduleGroup->getISRoot()+"PIXWD_AUTO_RECOVER", static_cast<int>(m_run->autoRecover()));
    }
    std::vector<std::string> varList = m_moduleGroup->isManager()->listVariables("*Trick1Count");
    for (unsigned int ivr = 0; ivr<varList.size(); ivr++) {
      try {
	m_moduleGroup->isManager()->removeVariable(varList[ivr]);
      }
      catch (...) {
      }
    }
    m_moduleGroup->resetDisableCounters(true);

    bool simIsOn = m_run->simActive();
    if(simIsOn) {
      std::cout << "Dump of simulation parameters:" << std::endl;
      std::cout << "simLVL1Accept = " << m_run->consecutiveLVL1()-1 << std::endl;
      std::cout << "simLVL1 skipped = " << m_run->simSkippedLVL1() << std::endl;
      std::cout << "simBitFlip = " << m_run->simBitFlip() << std::endl;
      std::cout << "simModOccupancy = " << m_run->simModOccupancy() << std::endl;
      //std::cout << std::hex << "ROD simulation first register 32 bits mask (hex base) = " << (m_rod->simMask()).pack(1) << std::endl;
      //std::cout << "ROD simulation second register 32 bits mask (hex base) = " << (m_rod->simMask()).pack(0) << std::endl;
    }
    try {
      // Put the ROD in data-taking mode
      m_info = "writeConfig() start";
      ers::info(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      m_rod->writeRunConfig(m_run);
      m_info = "writeConfig() end";
      ers::info(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      std::cout<<"DEBUG: PixActionsSingleIBL.cxx internal gen "<<m_run->internalGen()<<" SimActive "<<m_run->simActive()<<std::endl;
      if (m_run->internalGen() || m_run->simActive()){// Enable the events simulation
        std::cout<<"DEBUG: PixActionsSingleIBL.cxx startRun setted the MCC Emu"<<std::endl;
	m_rod->configureEventGenerator(m_run, true); 
      }
      // Read run number from IS
      if ( m_isPixelRunParams != NULL) {
	try {
	  unsigned int rn = m_isPixelRunParams->read<unsigned int>("DT-RunNumber");
	  std::cout << "Setting run number to " << rn << " for " << m_moduleGroup->getName() << std::endl;
          // Set run number in the ROD
	  m_rod->setRunNumber(rn);
	}
	catch (...) {
	  m_info = "Cannot read Run Number from IS"; 
	  ers::warning(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	}
      }
      m_info = "startRun() start";
      ers::info(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      m_rod->startRun(m_run);
      m_rod->setEcrCount(0);
      m_info = "startRun() end";
      ers::info(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      publishStatus("SUCCESS");
    }
    catch (...){
      m_info = "Exception caught in writeRunConfig()";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      publishStatus("FAILURE");
    } 
  } else {
    retrySetupDb("prepareForRun");
    if (m_isSetup) prepareForRun();
    else printFailSetupDb("prepareForRun");
  } 

}

void PixActionsSingleIBL::startTrigger(PixActions::SyncType ) {
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;
  if (m_actionStatus < 1) {
    publishAction("startTrigger");
    publishStatus("FAILURE");
    m_info = "ROD not initialized, aborting startTrigger()";
    ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    return;
  }
  if(!m_autoDisTask)setupPixAutoDis();
  if(m_isSetup){
    PixThread::ScopeLock myLock(m_thread,"startTrigger");
    publishAction("startTrigger");
    try {
      publishStatus("SUCCESS");
    } catch (...) {
      m_info = "Exception caught in startTrigger";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      publishStatus("FAILURE");
    }
  } else {
    retrySetupDb("startTrigger");
    if (m_isSetup) startTrigger();
    else printFailSetupDb("startTrigger");
  } 
}


void PixActionsSingleIBL::stopTrigger(PixActions::SyncType ) {
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;
  if (m_actionStatus < 1) {
    publishAction("stopTrigger");
    publishStatus("FAILURE");
    m_info = "ROD not initialized, aborting stopTrigger()";
   ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    return;
  }
  if(m_autoDisTask != 0) unSetupPixAutoDis();
  if(m_isSetup){
    PixThread::ScopeLock myLock(m_thread,"stopTrigger");
    publishAction("stopTrigger");
    try {
      publishStatus("SUCCESS");
    } catch(...){
      m_info = "Exception caught while configuring Event Generator and stopping run.";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      publishStatus("FAILURE");
    }
  } else {
    retrySetupDb("stopTrigger");
    if (m_isSetup) stopTrigger();
    else printFailSetupDb("stopTrigger");
  } 
}

void PixActionsSingleIBL::stopEB(PixActions::SyncType ) {
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;
  if (m_actionStatus < 1) {
    publishAction("stopEB");
    publishStatus("FAILURE");
    m_info = "ROD not initialized, aborting stopEB()";
    ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    return;
  }
  if(m_isSetup){
    PixThread::ScopeLock myLock(m_thread);
    publishAction("stopEB");
    m_configured = true;
    m_runStarted = false;
    try {
      if (m_run->internalGen() || m_run->simActive())// Disable the events simulation
	m_rod->configureEventGenerator(m_run, false); 
      
      m_rod->stopRun(m_hInt);
      
      publishStatus("SUCCESS");
    } catch(...){
      m_info = "Exception caught while configuring Event Generator and stopping run.";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }
  } else {
    retrySetupDb("stopEB");
    if (m_isSetup) stopEB();
    else printFailSetupDb("stopEB");
  } 
}

void PixActionsSingleIBL::disconnect(PixActions::SyncType ) {
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;
  if(m_isSetup){
    publishAction("disconnect");
    std::cout << "Unsetup DSP Monitoring" << std::endl;
    if(m_rodMonitor) unSetupRodMonitoring();
    if(m_autoDisTask)unSetupPixAutoDis();
    PixThread::ScopeLock myLock(m_thread);
    try {
      m_preAmpKilled = true;
        if  ( !(m_run->internalGen() || m_run->simActive()) ){
          m_rod->sendModuleConfigWithPreampOff(m_moduleGroup->getActiveModuleMask(), 0x2000);
        } else {
          ers::warning(PixLib::pix::daq (ERS_HERE,getRodName(),"Skipping send config in emulator mode"));
        }
      m_moduleGroup->disconnect();
      m_preAmpKilled = false;
      publishStatus("SUCCESS");
    } catch(...){
      m_info = "Exception caught in disconnect()";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      publishStatus("FAILURE");
    }
  } else {
    retrySetupDb("disconnect");
    if (m_isSetup) disconnect();
    else printFailSetupDb("disconnect");
  } 
}

void PixActionsSingleIBL::unconfigure(PixActions::SyncType ) 
{
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;
  
  if(m_is){
  try{
  m_is->removeVariable(m_moduleGroup->getISRoot()+"LATENCY");
  m_is->removeVariable(m_moduleGroup->getISRoot()+"LVL1");
  } catch(...){
      m_info = "Exception caught at the unconfig, removing IS LATENCY and LV1L in PixActionsSingleIBL";
      ers::warning(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }
  }
  
  if (m_actionStatus < 1) {
    publishAction("unconfigure");
    publishStatus("FAILURE");
    m_info = "ROD not initialized, aborting unconfigure()";
    ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    return;
  }
  if (m_isSetup){
    publishAction("unconfigure");
    m_configured = false;
    m_runStarted = false;
    publishStatus("SUCCESS");
  } else {
    retrySetupDb("unconfigure");
    if (m_isSetup) unconfigure();
    else printFailSetupDb("unconfigure");
  } 
}

//! publishing

void PixActionsSingleIBL::probe(PixActions::SyncType ) { 
  //std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;

}


//! Basic hardware operations

void PixActionsSingleIBL::reset(PixActions::SyncType ) 
{
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;
  if(m_isSetup){
    PixThread::ScopeLock myLock(m_thread,"reset");

    publishAction("reset");
    
    // Init ROD
    try {
      m_preAmpKilled = false;
      m_moduleGroup->initHW();
      publishStatus("SUCCESS");
      m_actionStatus = 1;
    } 
    catch (...) {
      m_info = "Exception caught in reset()";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      publishStatus("FAILURE");
      m_actionStatus = 0;
    }
    //std::cout << "done resetting " << m_descriptor.name << std::endl;
    //std::cout << "reset release lock " << m_descriptor.name << std::endl;
  
  } else {
    retrySetupDb("reset");
    if (m_isSetup) reset();
    else printFailSetupDb("reset");
  }
}

void PixActionsSingleIBL::resetRod(PixActions::SyncType ) 
{
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;
  if(m_isSetup){
    PixThread::ScopeLock myLock(m_thread);
    //std::cout << "reset got lock " << m_descriptor.name << std::endl;
    publishAction("resetRod");
    
    // Init ROD
    try {
      m_moduleGroup->resetRod();
      publishStatus("SUCCESS");
    } catch (...){
      m_info = "Exception caught in resetRod()";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      publishStatus("FAILURE");
    }
    //std::cout << "done resetting " << m_descriptor.name << std::endl;
    //std::cout << "reset release lock " << m_descriptor.name << std::endl;
  
  } else {
    retrySetupDb("resetRod");
    if (m_isSetup) resetRod();
    else printFailSetupDb("resetRod");
  }
}

void PixActionsSingleIBL::configureBoc(PixActions::SyncType ) 
{
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;
  if (m_actionStatus < 1) {
    publishAction("configureBoc");
    publishStatus("FAILURE");
    m_info = "ROD not initialized, aborting configureBoc()";
    ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    return;
  }
  if(m_isSetup){
    PixThread::ScopeLock myLock(m_thread);

    //std::cout << "reset got lock " << m_descriptor.name << std::endl;
    publishAction("configureBoc");
    
    // Init ROD
    try {
      m_moduleGroup->configureBoc();
      publishStatus("SUCCESS");
    } catch (...){
      m_info = "Exception caught in configureBoc()";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      publishStatus("FAILURE");
    }
    //std::cout << "done resetting " << m_descriptor.name << std::endl;
    //std::cout << "reset release lock " << m_descriptor.name << std::endl;
  
  } else {
    retrySetupDb("configureBoc");
    if (m_isSetup) configureBoc();
    else printFailSetupDb("configureBoc");
  }
}

void PixActionsSingleIBL::resetViset(PixActions::SyncType )
{
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;
  if(m_isSetup){
    PixThread::ScopeLock myLock(m_thread);
    publishAction("resetViset");
    
    try {
      std::cout << "ActionsSingleIBL::resetViset calling method from PixModuleGroup" << std::endl;
      m_moduleGroup->resetViset();
      publishStatus("SUCCESS");
    }
    catch(const PixModuleGroupExc &e) {
      if (e.getId() == "ISSERVER" || e.getId() == "DDC") {
	std::ostringstream log;
	log  << "ActionsSingleIBL::resetViset - PixModuleGroup " 
	     << m_moduleGroup->getName() << " Exception caught: "<< e.getDescr() << " ";
	m_info = log.str();
	ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      }
    }
    catch(...) {
      m_info = "Unknown Exception caught in resetViset()";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }
  } else {
    retrySetupDb("resetViset");
    if (m_isSetup) resetViset();
    else printFailSetupDb("resetViset");
  }
}


void PixActionsSingleIBL::DCSsync(PixActions::SyncType )
{
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;
  if(m_isSetup){
    PixThread::ScopeLock myLock(m_thread);
    publishAction("DCSsync");
    PublishHelper publisher(*this);
    
    try {
      std::cout << "ActionsSingleIBL::DCSsync calling method from PixModuleGroup" << std::endl;
      m_moduleGroup->DCSsync();
      publisher.setSuccess();
    }
    catch(const PixModuleGroupExc &e) {
      if (e.getId() == "ISSERVER" || e.getId() == "DDC" || e.getId() == "DCSSTATE") {
	std::ostringstream log;
	log  << "ActionsSingleIBL::DCSsync - PixModuleGroup " 
	     << m_moduleGroup->getName() << " Exception caught: "<< e.getDescr() << " ";
	m_info = log.str();
	ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      }
    }
    catch(...) {
      m_info = "Unknown Exception caught in DCSsync()";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }
  } else {
    retrySetupDb("DCSsync");
    if (m_isSetup) DCSsync();
    else printFailSetupDb("DCSsync");
  }
}

void PixActionsSingleIBL::optoOnOff(bool switchOn, PixActions::SyncType )
{
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;
  if(m_isSetup){
    PixThread::ScopeLock myLock(m_thread);
    publishAction("optoOnOff");
    PublishHelper publisher(*this);
    
    try {
      std::cout << "ActionsSingleIBL::optoOnOff calling method from PixModuleGroup" << std::endl;
      m_moduleGroup->optoOnOff(switchOn);
      //publishStatus("SUCCESS"); 
      publisher.setSuccess();
    }
    catch(const PixModuleGroupExc &e) {
      if (e.getId() == "ISSERVER" || e.getId() == "DDC") {
	std::ostringstream log;
	log  << "ActionsSingleIBL::optoOnOff - PixModuleGroup " 
	     << m_moduleGroup->getName() << " Exception caught: "<< e.getDescr() << " ";
	m_info = log.str();
	ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      }
    }
    catch(...) {
      m_info = "Unknown Exception caught in optoOnOff()";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }
  } else {
    retrySetupDb("optoOnOff");
    if (m_isSetup) optoOnOff(switchOn);
    else printFailSetupDb("optoOnOff");
  }
}

void PixActionsSingleIBL::lvOnOff(bool switchOn, PixActions::SyncType )
{
  if(m_isSetup){
    PixThread::ScopeLock myLock(m_thread);
    publishAction("lvOnOff");
    PublishHelper publisher(*this);
    try {
      std::cout << "ActionsSingleIBL::lvOnOff calling method from PixModuleGroup" << std::endl;
      m_moduleGroup->lvOnOff(switchOn);
      //publishStatus("SUCCESS");
      publisher.setSuccess();
    }
    catch(const PixModuleGroupExc &e) {
      if (e.getId() == "ISSERVER" || e.getId() == "DDC") {
	std::ostringstream log;
	log  << "ActionsSingleIBL::lvOnOff - PixModuleGroup " 
	     << m_moduleGroup->getName() << " Exception caught: "<< e.getDescr() << " ";
	m_info = log.str();
	ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      }
    }
    catch(...) {
      m_info = "Unknown Exception caught in lvOnOff()";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }
  } else {
    retrySetupDb("lvOnOff");
    if (m_isSetup) lvOnOff(switchOn);
    else printFailSetupDb("lvOnOff");
  }
}

void PixActionsSingleIBL::disable(std::string objName, PixActions::SyncType ) {
  std::string object = objName;
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<" for object: "<<object<<std::endl;
  if (object == m_rodConn->name()) {
   try{
      m_thread->lock();
    }catch(DFMutex::AlreadyLockedByThread &v){
      m_info = "Exception caught when trying to lock thread in disable().";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }
    try {
      m_rod->stopRun(m_hInt);
      m_moduleGroup->disableRod( );
    }
    catch (...) {
      std::cout << "ERROR!!! Exception caught in PixActionsSingleIBL::disable (ROD)" << std::endl;
    }
   try{
      m_thread->unlock();
    }catch(DFMutex::NotLockedByThread &v){
      m_info = "Exception caught when trying to unlock thread in disable().";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }

  } else if (object.find(m_rodConn->name()) != std::string::npos) { // The ID contains the ROD name but is not equal to it: it's an S-LINK :)
    std::cout<<"going to disable "<<object<<std::endl;
    PixThread::ScopeLock myLock(m_thread);
    try {
      m_moduleGroup->disableSlink(object);
    }
    catch (...) {
      std::cout << "ERROR!!! Exception caught in PixActionsSingleIBL::disable (ROD)" << std::endl;
    }
  } else {
    ers::info(PixLib::pix::daq (ERS_HERE,getRodName(),"Ignoring disable for object"));
  }
}

void PixActionsSingleIBL::enable(std::string objName, PixActions::SyncType ) {
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;

}

void PixActionsSingleIBL::reset(std::string objName, PixActions::SyncType ) {
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;
  
}

void PixActionsSingleIBL::recover(std::string mode, std::string ecr, PixActions::SyncType ) {

  if (m_isSetup) {

    if(mode == "RECOVER") {
      std::string rodName = ecr;
      std::ostringstream txtS;
      if (rodName == m_rodConn->name()|| rodName == "") {
        txtS << "RECOVER (" << rodName << ") used";
        m_info = txtS.str();
        ers::info(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      } else return; // Skip, for this is not the right ROD

      if ( m_is!=NULL) {
      	try {
      	m_is->publish(m_moduleGroup->getISRoot()+"RECOVERY","WAIT");      
      	} catch (...) {
	    m_info = "Cannot publish RECOVER in IS"; 
	    ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	  }
      }
      if(m_rodMonitor) unSetupRodMonitoring();

      resetRod();
      loadConfig();
        if  ( !(m_run->internalGen() || m_run->simActive()) ){
         sendConfig();
        } else {
          ers::warning(PixLib::pix::daq (ERS_HERE,getRodName(),"Skipping send config in emulator mode"));
        }
      //PixThread::ScopeLock myLock(m_thread);

      //m_moduleGroup->setRodDisableReason("R");
      txtS.str("");
      PIX_WARNING(rodName << " recovered");
      m_moduleGroup->updateDisableRodRecover( );

      if ( m_is) {
        try {
          m_is->publish(m_moduleGroup->getISRoot()+"RECOVERY","DONE");
        } catch (...) {
	  m_info = "Cannot publish RECOVER in IS";
	  ers::warning(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	}
      }

    }
  } else {
    printFailSetupDb("recover");
  }

}


void PixActionsSingleIBL::warmReconfig(std::string mode, std::string ecr, PixActions::SyncType ) {
  if (m_actionStatus < 1) {
    publishAction("warmReconfig");
    publishStatus("FAILURE");
    m_info = "ROD not initialized, aborting warmReconfig()";
    ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    return;
  }
  bool resynchSingleROD = false;
  if (m_isSetup) {  
    if (!m_configured){ std::cout<<"ERROR: ROD not configured "<<std::endl; return;}

    try {
      m_thread->lock();
    } catch(DFMutex::AlreadyLockedByThread &v){
      PIX_ERROR( getRodName()<<" Exception caught when trying to lock thread." << v.what());
    }
    
    //if ((mode == "PIXEL_UP") && (!m_preAmpKilled)) return;
    //if ((mode == "PIXEL_DOWN") && m_preAmpKilled) return;
    //if (m_moduleGroup->rodDisabled()) return;
    publishAction("warmReconfig");
    unsigned int ecrC = 0;
    if (mode == "RESYNC") {
      std::istringstream ecrS(ecr);
      std::string rodName = "";
      ecrS >> ecrC;
      ecrS >> rodName;
      std::ostringstream txtS;
      if (rodName == m_rodConn->name() ||rodName == m_rodConn->name() + "_SET" || rodName == "") {
        txtS << "ECR from DAQ (" << ecrC << ") used";
        m_info = txtS.str();
        ers::info(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	if(rodName != "")resynchSingleROD=true;
      } else if (rodName.find(m_rodConn->name()) != std::string::npos ){//SLINK
        ers::info(PixLib::pix::daq (ERS_HERE,getRodName(),"RESYNC receiver for SLINK "+rodName));
        m_moduleGroup->sendDisableMessage("*"+rodName,"ROD");
          try{
           m_thread->unlock();
         } catch(DFMutex::NotLockedByThread &v){
           PIX_ERROR( getRodName()<<" Exception caught when trying to unlock thread." << v.what());
         }
        return;
      } else {
        txtS << "RESYNC received for ROD  " << rodName << "; ignoring";
        m_info = txtS.str();
        ers::info(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
          try{
           m_thread->unlock();
         } catch(DFMutex::NotLockedByThread &v){
           PIX_ERROR( getRodName()<<" Exception caught when trying to unlock thread." << v.what());
         }

        return;
      }
    }

    try{
      m_thread->unlock();
    } catch(DFMutex::NotLockedByThread &v){
      PIX_ERROR( getRodName()<<" Exception caught when trying to unlock thread." << v.what());
    }
    
    // Disable ROD monitoring
    if(m_rodMonitor) unSetupRodMonitoring();
    //if(m_autoDisTask != 0) unSetupPixAutoDis();

    try{
      m_thread->lock();
    } catch(DFMutex::AlreadyLockedByThread &v){
      PIX_ERROR( getRodName()<<" Exception caught when trying to lock thread." << v.what());
    }

    auto begin = std::chrono::high_resolution_clock::now();
    auto end = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();

    // Check stable beams flag
    int stableBeams = 0;
    int stableBeamsOverride = 0;
    try {
      if (m_isPixelDDC == NULL) {
	m_isPixelDDC = new PixISManager(m_ipcPartition, "DDC");
      }  
    }
    catch(...) { 
      m_info = "Unknown exception caught while creating ISManager for DDC";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      m_isPixelDDC = NULL;
    }
    if ( m_isPixelDDC != NULL) {
      try {
	stableBeams = m_isPixelDDC->readDcs<int>("StableBeamsFlag");
	std::ostringstream os;
	os << "Stable Beams flag from IS = " << stableBeams;
	m_info = os.str(); 
	ers::info(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      } catch (...) {
	m_info = "Cannot read Stable Beams flag from IS"; 
	ers::warning(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      }	
      try {
	stableBeamsOverride = m_isPixelDDC->read<int>("StableBeamsOverride");
	std::ostringstream os;
	os << "Stable Beams Override from IS = " << stableBeamsOverride;
	m_info = os.str(); 
	ers::info(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      } catch (...) {
	m_info = "Cannot read Stable Beams Override flag from IS"; 
	ers::warning(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      }
    }
    bool hvOff = getHVOff();
    bool preAmpsOff = false;
    if (stableBeams == 0 && stableBeamsOverride == 0) preAmpsOff = true;
    if (hvOff) preAmpsOff = true;


    m_info = "warmReconfig : "+mode+" "+ecr;
    ers::warning(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    try {
      if (m_runStarted) {
	// Get the ECR counter
        if (mode == "PIXEL_UP" || mode == "PIXEL_DOWN" || mode == "RECONFIG") {
	  ecrC = m_rod->getEcrCount();
	}
	// Disable the events simulation
	if (m_run->internalGen() || m_run->simActive())  
	  m_rod->configureEventGenerator(m_run, false); 
	
	// Stop run
        if (mode != "RESYNC") {
	  m_rod->stopRun(m_hInt);
	}
      }

    end = std::chrono::high_resolution_clock::now();
    duration = std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();
    std::cout<<__PRETTY_FUNCTION__<<" Elapsed time start "<<duration*1E-9 <<std::endl;
    begin = std::chrono::high_resolution_clock::now();

      // Send config to the modules
      if(!resynchSingleROD)m_rod->setConfigurationMode();
      if (mode == "PIXEL_UP") {
	m_preAmpKilled = false;
	unsigned int warmStartModMask = 0xffffffff;
	std::string head = "Warm reconfig " + m_rodConn->name() + " :";
	if ( m_isPixelRunParams != NULL) {
	  std::string warmStartDisableName;
	  try {
            warmStartDisableName = m_isPixelRunParams->read<std::string>("DataTakingConfig-WsDisable");
	  }
	  catch (...) {
	    warmStartDisableName = "";
	  }
	  if (warmStartDisableName != "" && warmStartDisableName != "NONE") {
	    std::cerr << head << " Reading disable " << warmStartDisableName << std::endl;
	    bool lock = false;
	    DFMutex *globMutex=nullptr;
	    std::string globMutexName = "PixAction";
	    if (m_rodConn->crate() != NULL) {
	      globMutexName = "PixAction_"+m_rodConn->crate()->name();
	    }
	    try {
	      PixDisable *dis = m_conn->getDisable(warmStartDisableName);
	      PixDisable *tmp = new PixDisable("tmp");
	      globMutex = DFMutex::Create(const_cast<char*>(globMutexName.c_str()));
	      globMutex->lock();
	      lock = true;
	      tmp->get(m_conn);
	      PixConnectivity *conn = new PixConnectivity(m_conn, m_rodConn->name());
	      dis->put(conn);
	      if (conn->rods.find(m_rodConn->name()) != conn->rods.end()) {
		RodBocConnectivity *crod = conn->rods[m_rodConn->name()];
		for (int i=0; i<4; i++) {
		  if (crod->obs(i) != NULL) {
		    if (crod->obs(i)->pp0() != NULL) {
		      for (int j=1; j<=8; j++) {
			if (crod->obs(i)->pp0()->modules(j) != NULL) {
			  ModuleConnectivity *mod = crod->obs(i)->pp0()->modules(j);
			  if (!mod->active()) {
			    warmStartModMask &= (~m_moduleGroup->getModMaskPosition(mod->name()));
			    std::cerr << head << " Module " << mod->name() << " is disabled" << std::endl;
			  }
			}
		      }
		    }
		  }
		}
		delete conn;
		delete dis;
		tmp->put(m_conn);
		globMutex->unlock();
		lock = false;
		globMutex->destroy();
		delete tmp;
	        std::cerr << head << " Module mask 0x" << std::hex << warmStartModMask << std::dec << std::endl;
	      }
	    }
	    catch (...) {
	      std::cerr << head << " Error reading disable " << warmStartDisableName << "; enabling everything" << std::endl;
	      warmStartModMask = 0xffffffff;
	      if (lock) {
	        globMutex->unlock();
	        lock = false;
	        globMutex->destroy();
	      }
	    }
	  } else {
	    std::cerr << head << " No WS disable; enabling everything" << std::endl;
	  }
	} else {
	  std::cerr << head << " No IS server; enabling everything" << std::endl;
        }
        unsigned int enaMask = m_moduleGroup->getActiveModuleMask();
	std::cerr << head << " Enable mask 0x" << std::hex << enaMask << std::dec << std::endl;
        unsigned int modMask =  enaMask & warmStartModMask;
	if (m_run->internalGen() || m_run->simActive())
	  ers::error(PixLib::pix::daq (ERS_HERE,getRodName()," PIXEL_UP issued in EMULATOR MODE"));

	if (modMask != 0x0) {
	  std::cerr << head << " Config Module mask 0x" << std::hex << modMask << std::dec << std::endl;
	  if (!preAmpsOff) {
            m_rod->sendModuleConfig(modMask,0x2000);
            m_moduleGroup->preAmpsOn() = true;
          }
	}
	if (warmStartModMask != 0xffffffff) m_preAmpKilled = true;
      } else if (mode == "PIXEL_DOWN") {
	m_preAmpKilled = true;
	
	std::string fei4latches = "READBACKFEI4LATCHES";
	if (m_is->exists(m_moduleGroup->getISRoot()+fei4latches)) {
	  if (m_is->read<bool>(m_moduleGroup->getISRoot()+fei4latches)) {
	    //Check that is an IBL module group
	    if (m_moduleGroup->getCtrlType()==CPPROD) { //IBLROD
	      //ReadBack the latches
	      m_rod->readBackFEI4Latches(m_moduleGroup->getActiveModuleMask());
	      //m_rod->readBackFEI4Latches(0xffff00 & m_moduleGroup->getActiveModuleMask());
	    }
	  }//check over IS value of the readback
	}//check that IS exists

	if (m_run->internalGen() || m_run->simActive())
	  ers::error(PixLib::pix::daq (ERS_HERE,getRodName()," PIXEL_DOWN issued in EMULATOR MODE"));

	m_rod->sendModuleConfigWithPreampOff(m_moduleGroup->getActiveModuleMask(), 0x2000);
        m_moduleGroup->preAmpsOn() = false;
      }
      else if(resynchSingleROD){
      //Do nothing since we have sent the configuration before
      }
      else {
        if (preAmpsOff || m_preAmpKilled) {
	  m_rod->sendModuleConfigWithPreampOff(m_moduleGroup->getActiveModuleMask(), 0x3fff);
          m_moduleGroup->preAmpsOn() = false;
	} else {
	  m_rod->sendModuleConfig(m_moduleGroup->getActiveModuleMask(),0x3fff);
          m_moduleGroup->preAmpsOn() = true;
	}
      }

    end = std::chrono::high_resolution_clock::now();
    duration = std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();
    std::cout<<__PRETTY_FUNCTION__<<" Elapsed time sendConfig "<<duration*1E-9 <<std::endl;
    begin = std::chrono::high_resolution_clock::now();

      if (m_runStarted) {
	// Setup simulators
	bool simIsOn = m_run->simActive();
	if(simIsOn) { 
	  //m_rod->computeGeneratorConfig(m_run); // Computes the mask to be set in formatters to setup events simulation
	}
	// Put the ROD in data-taking mode
	m_rod->writeRunConfig(m_run);
	if (m_run->internalGen() || m_run->simActive())  // Enable the events simulation
	 m_rod->configureEventGenerator(m_run, true); 

    end = std::chrono::high_resolution_clock::now();
    duration = std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();
    std::cout<<__PRETTY_FUNCTION__<<" Elapsed time writeRunConfig "<<duration*1E-9 <<std::endl;
    begin = std::chrono::high_resolution_clock::now();

	// Read run number from IS
	if ( m_isPixelRunParams != NULL) {
	  unsigned int rn = m_isPixelRunParams->read<unsigned int>("DT-RunNumber");
	  // Set run number in the ROD
	  m_rod->setRunNumber(rn);
	}
	// Start the run
	m_rod->startRun(m_run);
	// Set the ECR counter
	m_rod->setEcrCount(ecrC);
	// Re-disable auto-disabled modules
	m_moduleGroup->reApplyModuleDisable();
      }
    end = std::chrono::high_resolution_clock::now();
    duration = std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();
    std::cout<<__PRETTY_FUNCTION__<<" Elapsed time Run Params "<<duration*1E-9 <<std::endl;
    begin = std::chrono::high_resolution_clock::now();

      if (mode == "RESYNC") {
	m_moduleGroup->enableRod( );
      }
    } catch(...) {
      m_info = "Exception caught while executing warmReconfig";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      publishStatus("FAILURE");
    }
    end = std::chrono::high_resolution_clock::now();
    duration = std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();
    std::cout<<__PRETTY_FUNCTION__<<" Elapsed time enable Rod "<<duration*1E-9 <<std::endl;

    try{
      m_thread->unlock();
    } catch(DFMutex::NotLockedByThread &v){
      PIX_ERROR( getRodName()<<" Exception caught when trying to lock thread." << v.what());
    }

    // Enable ROD monitoring
    if (!m_rodMonitor) setupRodMonitoring(m_run->bufRate(),
					      m_run->statusRate(),
					      m_run->busyRate(),
					      0,
					      m_run->occupRate(),
					      m_run->rodMonPrescale(),
					      m_run->quickStatusMode(),
					      m_run->quickStatusConfig(),
					      m_run->syncConfig(),
					      m_run->syncConfigROD());
    //if(m_autoDisTask ==0) setupPixAutoDis();

    std::ostringstream os;
    os << ecrC;
    m_info = "warmReconfig : completed ecr = "+os.str();
    ers::info(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    publishStatus("SUCCESS");
  }
}

//!Configure HW
void PixActionsSingleIBL::setActionsConfig(PixActions::SyncType ) { 
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;

  if (m_isSetup){
    PixThread::ScopeLock myLock(m_thread);
    publishAction("setActionsConfig");
    //Rod configuration update:
    if(m_dbServer != NULL) {
      if(m_rodRev != m_dbServer->getLastRev(m_domainName, m_cfgTag, m_moduleGroup->getName() ) ) {
	m_rod->readConfig(m_dbServer, m_domainName, m_cfgTag);
	if ( !(m_rod->readConfig(m_dbServer, m_domainName, m_cfgTag)) ){
	  m_info = "Error from RodPixcontroller::readConfig. Cannot read config from DbServer.";
	  ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	}
      }
    } else {
      m_info = "The PixActions is configured without DbServer or with a bad pointer to it. Cannot update configuration for PixRunConfig";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }
  
    unsigned int mask = 0;
    PixModuleGroup::moduleIterator m;
    for (m=m_moduleGroup->modBegin(); m!=m_moduleGroup->modEnd(); m++) {
      int pos = (*m)->moduleId();
      //std::cout<<"Iterating inside moduleGroup: PixModule::moduleId() put inside map is = "<<pos<<std::endl;
      if(m_moduleGroup->getModuleActive(pos)) 
	mask |= (0x1 << pos); 
      //std::cout<<"Mask is "<<mask << std::endl;
    }
    m_run->defineGroup(0, mask);
    std::cout << "Rod modules mask for DataTaking:" << m_moduleGroup->getRodName() << " Mask = 0x" << std::hex << mask << std::dec << std::endl;
  
    publishStatus("SUCCESS");

  } else {
    retrySetupDb("setActionsConfig");
    if (m_isSetup) setActionsConfig();
    else printFailSetupDb("setActionsConfig");
  } 

}



void PixActionsSingleIBL::loadConfig(PixActions::SyncType ) 
{
   std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;
 if (m_isSetup){
    PixThread::ScopeLock myLock(m_thread);
    publishAction("loadConfig");
    try {
      m_moduleGroup->downloadConfig();
      publishStatus("SUCCESS");
    }
    catch (...){
      m_info = "Exception generated when downloading config to module group.";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      publishStatus("FAILURE");
    }
    //std::cout << "loadConfig done " << m_descriptor.name << std::endl;
    //std::cout << "loadConfig released lock " << m_descriptor.name << std::endl;
  
  } else {
    retrySetupDb("loadConfig");
    if (m_isSetup) loadConfig();
    else printFailSetupDb("loadConfig");
  } 

}

void PixActionsSingleIBL::sendConfig(PixActions::SyncType ) {
  if (m_isSetup){
    PixThread::ScopeLock myLock(m_thread);

    publishAction("sendConfig");
    // Send config to the modules
    try {
      m_moduleGroup->getPixController()->setConfigurationMode();
     // Check stable beams flag
     int stableBeams = 0;
      int stableBeamsOverride = 0;
      try {
	if (m_isPixelDDC == NULL) {
	  m_isPixelDDC = new PixISManager(m_ipcPartition, "DDC");
	}  
      }
      catch(...) { 
	m_info = "Unknown exception caught while creating ISManager for DDC";
	ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	m_isPixelDDC = NULL;
      }
      if ( m_isPixelDDC != NULL) {
	try {
	  stableBeams = m_isPixelDDC->readDcs<int>("StableBeamsFlag");
	  std::ostringstream os;
	  os << "Stable Beams flag from IS = " << stableBeams;
          m_info = os.str(); 
	  ers::info(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	} catch (...) {
	  m_info = "Cannot read Stable Beams flag from IS"; 
	  ers::warning(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	}	
	try {
	  stableBeamsOverride = m_isPixelDDC->read<int>("StableBeamsOverride");
	  std::ostringstream os;
	  os << "Stable Beams Override from IS = " << stableBeamsOverride;
          m_info = os.str(); 
	  ers::info(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	} catch (...) {
	  m_info = "Cannot read Stable Beams Override flag from IS"; 
	  ers::warning(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	}	
      }

      bool hvOff = getHVOff();
      bool preAmpsOff = false;
      if (m_preAmpKilled) preAmpsOff = true;
      if (stableBeams == 0 && stableBeamsOverride == 0) preAmpsOff = true;
      if (hvOff) preAmpsOff = true;

      if (preAmpsOff) {
	m_info = "Send configuration with pre-amp off"; 
	ers::warning(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	m_moduleGroup->getPixController()->sendModuleConfigWithPreampOff(m_moduleGroup->getActiveModuleMask(),0x3fff);
        m_moduleGroup->preAmpsOn() = false;
      } else {
	m_moduleGroup->getPixController()->sendModuleConfig(m_moduleGroup->getActiveModuleMask(),0x3fff);
        m_moduleGroup->preAmpsOn() = true;
      }
      publishStatus("SUCCESS");
    }
    catch (...){
      m_info = "Exception generated when setting configuration mode and sending module config.";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      publishStatus("FAILURE");
    }
  } else {
    retrySetupDb("sendConfig");
    if (m_isSetup) sendConfig();
    else printFailSetupDb("sendConfig");
  } 
}

void PixActionsSingleIBL::saveConfig(std::string tag, PixActions::SyncType ) {
  if (m_isSetup){
    PixThread::ScopeLock myLock(m_thread);
    publishAction("saveConfig");
    // Check if moduleGroup config changed and save it
    try {
      // Save ROD/BOC config (it will check if it was modified)
      m_moduleGroup->saveChangedConfig(tag);
      // Save module configs (it will check it it was modified)
      for(PixModuleGroup::moduleIterator module = m_moduleGroup->modBegin(); module !=  m_moduleGroup->modEnd(); module++) {
	(*module)->saveChangedConfig(tag);
      }
      publishStatus("SUCCESS");
    }
    catch (...){
      m_info = "Exception generated when calling PixModuleGroup::saveChangedConfig and PixModule::saveChangedConfig.";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      publishStatus("FAILURE");
    }
  } else {
    retrySetupDb("saveConfig");
    if (m_isSetup) saveConfig(tag);
    else printFailSetupDb("saveConfig");
  } 
}


void PixActionsSingleIBL::reloadConfig(std::string actionName, PixActions::SyncType ) {
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;
  if(m_isSetup){
    PixThread::ScopeLock myLock(m_thread);
    publishAction("reloadConfig");
    // Check if moduleGroup config changed and save it
    if (actionName == ""){
      try {
	// reload all configs and do not mark them as updated
	m_moduleGroup->reloadConfig();
	for(PixModuleGroup::moduleIterator module = m_moduleGroup->modBegin(); module !=  m_moduleGroup->modEnd(); module++) {
	  (*module)->reloadConfig();
	}
	publishStatus("SUCCESS");
      }
      catch (...){
	m_info = "Exception generated when calling PixModuleGroup::reloadConfig and PixModule::reloadConfig.";
	ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	publishStatus("FAILURE");
      }
    }
    if (actionName == m_descriptor.name) {
      try {
	m_moduleGroup->reloadConfig();
	m_moduleGroup->changedCfg();
	publishStatus("SUCCESS");
      }
      catch (...){
	m_info = "Exception generated when calling PixModuleGroup::reloadConfig and PixModuleGroup::changedConfig.";
	ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	publishStatus("FAILURE");
      }
    }
  } else {
    retrySetupDb("reloadConfig");
    if (m_isSetup) reloadConfig(actionName);
    else printFailSetupDb("reloadConfig");
  } 
}

// Reset Modules
void PixActionsSingleIBL::resetmods(PixActions::SyncType ) {
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;

  if(m_isSetup) {  
    PixThread::ScopeLock myLock(m_thread);
    publishAction("resetmods");
    try {
     m_rod->resetModules(m_moduleGroup->getActiveModuleMask(),31);
      publishStatus("SUCCESS");
    }
    catch (...){
      m_info = "Unknown exception caught in resetmods()";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      publishStatus("FAILURE");
    }
  } else {
    retrySetupDb("resetmods");
    if (m_isSetup) resetmods();
    else printFailSetupDb("resetmods");
  } 
  
}

void PixActionsSingleIBL::setVcal(float charge, bool Chigh, PixActions::SyncType ) {
  if(m_isSetup){
    //Actual code: just call the method on the local PixModuleGroup belonging to this action (right? check it...)
    PixThread::ScopeLock myLock(m_thread);
    try {
      for(PixModuleGroup::moduleIterator module = m_moduleGroup->modBegin(); module !=  m_moduleGroup->modEnd(); module++) {
	(*module)->setVcal(charge, Chigh);
	//Pay attenction to this: you have to write config into ROD as in STcontrol, but first you have to check for rights!!!
	//So, publish ROD status in IS and retrieve its status before to call the method!!!
	//	(*module)->writeConfig();
      }
      m_moduleGroup->writeConfig();
    }
    catch (...){
      m_info = "Unknown exception setVcal()";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }
  } else {
    retrySetupDb("setVcal");
    if (m_isSetup) setVcal(charge, Chigh);
    else printFailSetupDb("setVcal");
  } 
  
}

void PixActionsSingleIBL::listRODs(PixActions::SyncType ) {
  if (m_isSetup){
    PixThread::ScopeLock myLock(m_thread);
    m_moduleGroup->publishROD();
  } else {
    retrySetupDb("listRODs");
    if (m_isSetup) listRODs();
    else printFailSetupDb("listRODs");
  } 
}

const bool PixActionsSingleIBL::isHVOff( ){
  PixThread::ScopeLock myLock(m_thread);
  return getHVOff();
}

const bool PixActionsSingleIBL::getHVOff( )
{
  bool hvOff = false;
  try {
    std::string hvStatusName = "";
    std::string partName=m_rodConn->crate()->partition()->name();
    std::cout<<"PARTNAME = "<<partName<<std::endl;
      if (partName == "PIXEL_I") {
        hvStatusName = "PIX_IBL_HvState";
      } else if (partName == "PIXEL_B") {
        hvStatusName = "PIX_BLAYER_HvState";
      } else if (partName == "PIXEL_L") {
        hvStatusName = "PIX_BARREL_HvState";
      } else if (partName == "PIXEL_D") {
        hvStatusName = "PIX_DISKS_HvState";
      }
    std::cout << "hvStatusName=" << hvStatusName << std::endl;
      if (!hvStatusName.empty()) {
        try {
        if (!m_isPixelDDC) {
          m_isPixelDDC = new PixISManager(m_ipcPartition, "DDC");
         }

        if(!m_isPixelDDC){
          PIX_ERROR("Cannot get DDC partition, assuming HV OFF!!!");
          return true;
        }
          std::string hvStatus = m_isPixelDDC->readDcs<std::string>(hvStatusName.c_str());
          std::ostringstream os;
          os << "HV flag from IS (" << hvStatusName << ") = " << hvStatus;
          m_info = os.str(); 
          ers::info(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
            if (hvStatus != "ON") {
	      hvOff = true;
            }
        } catch (...) {
          m_info = "Cannot read HV status from IS"; 
          ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
        }
      }	else if (partName == "TOOTHPIX") {//SR1
        hvOff= false;
      } else {
        PIX_ERROR("Partition "<<partName<<" not found, assumming HV OFF");
        hvOff = true;
      }
  } catch (...) {
    m_info = "Exception caugh reading HV status from IS"; 
    ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
  }

 return hvOff;
}

void PixActionsSingleIBL::setPixScan(std::string decName, PixActions::SyncType ) 
{
  if(m_isSetup){
    if (m_pixScan != 0) {
      delete m_pixScan;
      m_pixScan = 0;
    }
    std::string fileName;
    int pos = decName.find_first_of(":");
    fileName = decName.substr(0, pos);

    // T: search for "_#FLAV" in string:filename and replace it by the FE-flavour (m_feFlav)
    // ex: str.replace(str.find(str2),str2.length(),"preposition");
    std::string flavour ="_#FLAV"; 
    std::string FEflav;
    std::size_t start_pos = 0;
    if(m_feFlav == PixModule::PM_FE_I2) FEflav = "";  // T
    else if(m_feFlav == PixModule::PM_FE_I4A || m_feFlav == PixModule::PM_FE_I4B) FEflav = "_I4"; // T
    
    while( (start_pos = fileName.find(flavour,start_pos)) != std::string::npos ){
      fileName.replace(start_pos,flavour.length(),FEflav);
      start_pos += FEflav.length();
      std::cout << "start_pos, fileName: " << start_pos << ", " << fileName << std::endl;
    }
    std::cout << "filename after replacing FEflav: " << fileName << std::endl;
    decName = fileName;
    decName += ":/rootRecord;1";
    std::cout << "decname after replacing FEflav: " << decName << std::endl;

    IPCPartition partition( m_ipcPartition->name() );
    ISInfoDictionary dict(partition);
    string srvname("Histogramming"); // TODO: no hardcoding! maybe something like m_dbServer?

    try {
      RootDb* root=new RootDb(fileName, "READ");
      std::cout << "DB filename " << fileName << std::endl;
      DbRecord* rec = root->DbFindRecordByName(decName);
      m_pixScan = new PixScan(rec);
      std::cout << "m_pixscan has been created in setPixScan " << std::endl;
      delete root;
      dict.checkin(  srvname+"."+m_rodConn->name()+"_Scan_Repetitions", ISInfoInt(m_pixScan->getRepetitions()) );
      dict.checkin(  srvname+"."+m_rodConn->name()+"_Scan_MaskTotalSteps", ISInfoInt(EnumMaskSteps::getNumberOfSteps(
				      static_cast<EnumMaskSteps::MaskStageSteps>(m_pixScan->getMaskStageTotalSteps())) ));
      dict.checkin(  srvname+"."+m_rodConn->name()+"_Scan_MaskSteps", ISInfoInt(m_pixScan->getMaskStageSteps()) );
      dict.checkin(  srvname+"."+m_rodConn->name()+"_Scan_Hist_ScurveMean_Fill", ISInfoBool( m_pixScan->getHistogramFilled(PixLib::EnumHistogramType::SCURVE_MEAN) ));
      dict.checkin(  srvname+"."+m_rodConn->name()+"_Scan_Hist_ScurveSigma_Fill", ISInfoBool( m_pixScan->getHistogramFilled(PixLib::EnumHistogramType::SCURVE_SIGMA ) ));
      dict.checkin(  srvname+"."+m_rodConn->name()+"_Scan_Hist_ScurveChi2_Fill", ISInfoBool( m_pixScan->getHistogramFilled(PixLib::EnumHistogramType::SCURVE_CHI2 ) ));
      dict.checkin(  srvname+"."+m_rodConn->name()+"_Scan_Hist_ToTMean_Fill", ISInfoBool( m_pixScan->getHistogramFilled(PixLib::EnumHistogramType::TOT_MEAN ) ));
      dict.checkin(  srvname+"."+m_rodConn->name()+"_Scan_Hist_ToTSigma_Fill", ISInfoBool( m_pixScan->getHistogramFilled(PixLib::EnumHistogramType::TOT_SIGMA ) ));
      dict.checkin(  srvname+"."+m_rodConn->name()+"_Scan_Hist_Occupancy_Fill", ISInfoBool(m_pixScan->getHistogramFilled(PixLib::EnumHistogramType::OCCUPANCY )));
      dict.checkin(  srvname+"."+m_rodConn->name()+"_Scan_Loop0_Steps", ISInfoInt(m_pixScan->getLoopVarNSteps(0)));
      dict.checkin(  srvname+"."+m_rodConn->name()+"_Scan_Loop0_Min", ISInfoInt(m_pixScan->getLoopVarMin(0)));
      dict.checkin(  srvname+"."+m_rodConn->name()+"_Scan_Loop0_Max", ISInfoInt(m_pixScan->getLoopVarMax(0)));
      dict.checkin(  srvname+"."+m_rodConn->name()+"_Scan_Loop0_DSPProcessing", ISInfoBool(m_pixScan->getDspProcessing(0)));
      dict.checkin(  srvname+"."+m_rodConn->name()+"_Scan_FitMethod", ISInfoInt(static_cast<int>(m_pixScan->getFitMethod() ) ) );
    }
    catch(...) {
      m_info = "Exception caught while creating new RootDb and calling RootDb::DbFindRecordByName.";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }
  } else {
    retrySetupDb("setPixScan");
    if (m_isSetup) setPixScan(decName);
    else printFailSetupDb("setPixScan");
  }
}

void PixActionsSingleIBL::incrMccDelay(float in_delay, bool calib, PixActions::SyncType ) {
  
  if (m_isSetup){
    PixThread::ScopeLock myLock(m_thread);
    try {
      for(PixModuleGroup::moduleIterator mi = m_moduleGroup->modBegin(); mi !=  m_moduleGroup->modEnd(); mi++){
	float delay, delIncr = in_delay;
	int range = (*mi)->pixMCC()->readRegister("CAL_Range");
	if(calib){
	  Config &conf = (*mi)->pixMCC()->config();
	  std::stringstream a;
	  a << range;
	  float calfac = ((ConfFloat&)conf["Strobe"]["DELAY_"+a.str()]).m_value;
	  if(calfac!=0) delIncr /= calfac;
	}
	delay = (float)(*mi)->pixMCC()->readRegister("CAL_Delay");
	delay += delIncr;
	if(delay<0) delay = 0;
	if(delay>63) delay = 63;
	(*mi)->pixMCC()->writeRegister("CAL_Delay", (int)delay);
      }
    }
    catch(...) {
      m_info = "Unknown exception caught in incrMccDelay";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }
  } else {
    retrySetupDb("incrMccDelay");
    if (m_isSetup) incrMccDelay(in_delay, calib);
    else printFailSetupDb("incrMccDelay");
  }  
  
}

void PixActionsSingleIBL::disableFailed(PixActions::SyncType ) {

  if (m_isSetup){  
    PixThread::ScopeLock myLock(m_thread);
    int nmod=0;
    std::string module = "MODSTATUS";
    ostringstream num;
    for(PixModuleGroup::moduleIterator mi = m_moduleGroup->modBegin(); mi != m_moduleGroup->modEnd(); mi++){
      //    modStatus ms = getPixModuleStatus((*mi)->moduleId()); //Orig code with STcontrol struct modStatus
      // if(ms.modStat==tfailed){ //Orig code with STcontrol struct modStatus
      //Preliminar solution: read from IS the status of the modules as a string. Use that instead of modStatus
      //Somewhere we need to publish it!!!
      num<<nmod;
      module += num.str();
      std::string status;
      if(m_is) 
	status = m_is->read<std::string>(module.c_str());
      else {
	m_info = "disableFailed(): ISManager not initialized, cannot read from IS.";
	ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	return;
      }
      if(status == "tfailed") {
	try{
	  Config &conf = (*mi)->config();
	  if(conf["general"].name()!="__TrashConfGroup__" &&
	     conf["general"]["Active"].name()!="__TrashConfObj__") 
	    *((int *)((ConfList&)conf["general"]["Active"]).m_value)=
	      (int)((ConfList&)conf["general"]["Active"]).m_symbols["FALSE"];
	}
	catch(...){
	  m_info = "Unknown exception caught in disableFailed()";
	  ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
	}
      }
      nmod++;
    }
  } else {
    retrySetupDb("disableFailed");
    if (m_isSetup) disableFailed();
    else printFailSetupDb("disableFailed");
  }  
}


void PixActionsSingleIBL::selectFE(std::string actionName, long int modID, long int FE, PixActions::SyncType ) {

  if(m_isSetup){
    PixThread::ScopeLock myLock(m_thread);
    //bool newDspCode = isNewDspCodeVersion();  

    try {
      if (actionName == m_descriptor.name) {
	PixModule *selMod = m_moduleGroup->module(modID);
	//selMod->selectFEEnable(newDspCode);
	//selMod->selectFE(FE, newDspCode);
	selMod->selectFEEnable(true);
	selMod->selectFE(FE, true);
	// if(getPixCtrlStatus()==tOK){ //THIS HAS TO BE MODIFIED ACCORDINGLY WITH IS READ
	m_moduleGroup->downloadConfig();
      }
    }
    catch(...) {
      m_info = "Unknown exception caught in selectFE()";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }
  } else {
    retrySetupDb("selectFE");
    if (m_isSetup) selectFE(actionName, modID, FE);
    else printFailSetupDb("selectFE");
  } 
 
}

void PixActionsSingleIBL::configSingleMod(std::string actionName, long int modID, long int maskFE, PixActions::SyncType ) {
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;
 
  if (m_isSetup){
    PixThread::ScopeLock myLock(m_thread);
    try {
      if (actionName == m_descriptor.name) {

	PixModule *selMod = m_moduleGroup->module(modID); 
	Config & modConf = selMod->config();
	if(modConf["general"]["Active"].name()!="__TrashConfObj__")
	  if(((ConfList&)modConf["general"]["Active"]).sValue()!="TRUE") return; // module not active, nothing to do
      
	// set all modules except request inactive, store prev. active/inactive state
	std::map<int, bool> activeStates;
	for(PixModuleGroup::moduleIterator mia = m_moduleGroup->modBegin(); mia != m_moduleGroup->modEnd(); mia++){
	  Config &conf = (*mia)->config();
	  activeStates.insert(std::make_pair((*mia)->moduleId(),((ConfList&)conf["general"]["Active"]).sValue()=="TRUE"));
	  if((*mia)->moduleId()!=modID)
	    // WriteIntConf((ConfInt&)conf["general"]["Active"],0);
	    *((int *)((ConfInt&)conf["general"]["Active"]).m_value) = 0;
	}
	//bool newDspCode = isNewDspCodeVersion();  
      
	// set config for requested FEs
	//selMod->selectFEEnable(newDspCode);
	selMod->selectFEEnable(true);
	bool enabled;
	for (int iFE = 0; iFE < 16; iFE++) {
	  enabled = ((maskFE>>iFE)&1);
	  //if (enabled) selMod->selectFE(iFE, newDspCode);
	  if (enabled) selMod->selectFE(iFE, true);
	}
      
	// if(getPixCtrlStatus()==tOK){ //THIS HAS TO BE MODIFIED ACCORDINGLY WITH IS READ
	m_moduleGroup->downloadConfig();
	/*
	//Is that needed?	
	// configure
	ThreadExecute(MTconfig);
	// wait till thread has finished
	while((ctrlthr->running())){
	usleep(10000);
	m_app->processEvents();
	}
	*/
      
	// set all modules back to their org. active state
	for(PixModuleGroup::moduleIterator mi = m_moduleGroup->modBegin(); mi != m_moduleGroup->modEnd(); mi++){
	  Config &conf = (*mi)->config();
	  if((*mi)->moduleId()!=modID)
	    //WriteIntConf((ConfInt&)conf["general"]["Active"],(int)activeStates[(*mi)->moduleId()]);
	    *((int *)((ConfInt&)conf["general"]["Active"]).m_value) = (int)activeStates[(*mi)->moduleId()];
	}
	//  if(getPixCtrlStatus()==tOK)
	m_moduleGroup->downloadConfig();
      
	// restore org. FE config
	for(PixModuleGroup::moduleIterator mi = m_moduleGroup->modBegin(); mi != m_moduleGroup->modEnd(); mi++) 
	  (*mi)->disableFESelected();
      }
    
    }
    catch(...) {
      m_info = "Unknown exception caught in configSingleMod";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }
  } else {
    retrySetupDb("configSingleMod");
    if (m_isSetup) configSingleMod(actionName, modID, maskFE);
    else printFailSetupDb("configSingleMod");
  } 
}


//JDDEBUG: HERE YOU NEED TO CONTINUE
void PixActionsSingleIBL::setHalfclockMod(PixActions::SyncType sync)
{
  if(m_isSetup) {  
    PixThread::ScopeLock myLock(m_thread);
    publishAction("setHalfclockMod");
    try {
      // Loop over Modules
      for(int im = 0; im < 32; im++){
	if (m_moduleGroup->getModuleActive(im)) {
	  if (m_moduleGroup->module(im) != NULL){
	    // set Half clock out...
	    m_moduleGroup->module(im)->pixMCC()->setRegister("CSR_OutputMode", 0);
	    m_moduleGroup->module(im)->pixMCC()->setRegister("CSR_OutputPattern", 1);
	  }
	}
      }
      publishStatus("SUCCESS");
    }
    catch (...){
      m_info = "Unknown exception caught in resetmods()";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      publishStatus("FAILURE");
    }
  } else {
    retrySetupDb("resetmods");
    if (m_isSetup) resetmods();
    else printFailSetupDb("resetmods");
  } 
}

std::map<string,bool> PixActionsSingleIBL::rodStatus() 
{
  bool status;
  rodStatus(status);
  std::map<std::string,bool> resultMap;
  resultMap.insert(make_pair(m_descriptor.name,status));
  return resultMap;
}

// Rod Processing
void PixActionsSingleIBL::rodStatus(bool& status) 
{
  PixThread::ScopeLock myLock(m_thread);
  // Read ROD status
  try {
    bool checkStatus = m_rod != 0 ? m_rod->runStatus() : false;
    bool checkThread = false;
    std::vector<std::string> tasks = m_thread->listTasks();
    std::vector<std::string>::iterator it, itEnd = tasks.end();
    
    for(it = tasks.begin(); it != itEnd; it++){
      if((*it) == m_pixScanName) checkThread = true;
    }
      status = checkStatus || checkThread;
      
  }
    catch (...){
      m_info = "Unknown exception caught in RodStatus";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }
}

// Abort Scan
void PixActionsSingleIBL::abortScan(PixActions::SyncType ) {
  if (m_isSetup){
    if(m_pixScanTask == 0){
      m_info = "Scan is not being executed.";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }else{
      m_pixScanTask->abort();
      m_pixScanTask = 0;
    }
  } else {
    retrySetupDb("abortScan");
    if (m_isSetup) abortScan();
    else printFailSetupDb("abortScan");
  }
}

void PixActionsSingleIBL::abortConnTest(PixActions::SyncType ) {
}


// Calibration operations

void PixActionsSingleIBL::scan(std::string scanName, long int id, PixActions::SyncType )  {
  if (m_actionStatus < 1) {
    publishAction("connect");
    publishStatus("FAILURE");
    m_info = "ROD not initialized, aborting scan()";
    ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    return;
  }
  if(m_isSetup){

    // publish scanId to IS so that FitServer can retrieve it
    IPCPartition partition( m_ipcPartition->name() );
    ISInfoDictionary dict(partition);
    string srvname("Histogramming"); // TODO: no hardcoding! maybe something like m_dbServer?
    ISInfoInt scanidinfo (id);
    dict.checkin( srvname + "." + m_rodConn->name() + "_ScanId", scanidinfo );

    publishAction("scan");
    if (!m_pixScan) {
      m_info = "scan() was called when m_pixScan is not initialized. Please, call PixActionsSingleIBL::presetScan(std:.string scanType, bool), or call PixActionsSingleIBL::setPixScan(..) before calling this. ";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
      publishStatus("FAILURE");
      m_moduleGroup->publishScanFailed(id);
      return;
    }
  
    if(m_pixScanTask != 0) {
      //    delete m_pixScanTask;
      m_pixScanTask = 0;
    }
    std::cout << "creating scan task" << std::endl;
    std::cout << "creating scan task 2" << std::endl;
    std::string scanFiledir;
    if (scanName != ""){
      int pos = scanName.find_first_of(":");
      scanFiledir = scanName.substr(pos+1);
      scanName = scanName.substr(0,pos);
      std::stringstream suff;
      suff << m_pixScanNumber;
      m_pixScanNumber++;
      scanName= scanName + "_"+suff.str();
    } 
    if (m_pixScan->getCloneCfgTag() || m_pixScan->getCloneModCfgTag()){
      std::stringstream suff;
      suff << "-S";
      suff << std::setw(9) << std::setfill('0') << id;
      m_pixScan->setTagSuffix(m_pixScan->getTagSuffix()+suff.str());
    }

    std::cout << "PixActionsSingleIBL::scan scan Name" << scanName <<std::endl;
    m_pixScanTask = new PixScanTask(m_moduleGroup, m_pixScan, m_hInt, scanName, std::string(m_ipcPartition->name()));
    m_pixScanTask->setDirectory(scanFiledir);
    m_pixScanTask->setID(id);
    m_pixScanName = scanName;
    std::cout << "setting task" << std::endl;
    publishStatus("PROCESSING");
    m_thread->setNewTask(m_pixScanTask);
    /*  m_moduleGroup->steerScan("testScan", m_pixScan); // TO TEST DIRECTLY STEER SCAN
	RootDb* wdb = 0;
	std::string outFileName = getenv("HOME")+std::string("/")+std::string("Test_")+m_moduleGroup->getName()+".root";
	unsigned int it = time(NULL);
	std::stringstream ss;
	ss << it;
	std::string scnOutName = "test_"+ss.str(); 
	std::string grpName = "ScanResults";
      
	try {
	wdb = new RootDb(outFileName,"NEW");
	delete wdb;
	}
	catch (PixDBException &) {
	std::cout << outFileName << " already exists; updating" << std::endl;
	}

	wdb = new RootDb(outFileName,"UPDATE");  
	DbRecord* wr = wdb->readRootRecord(); // read version 1 of root record
	DbRecord* res = wr->addRecord("PixScanResult", scnOutName);
	DbRecord* modgr = res->addRecord("PixModuleGroup", m_moduleGroup->getName()); //which name now?
	DbRecord* data = modgr->addRecord("PixScanData", "Data_Scancfg");
	m_pixScan->write(data, m_moduleGroup);
	delete wdb;
    */
  } else {
    retrySetupDb("scan");
    if (m_isSetup) scan(scanName, id);
    else printFailSetupDb("scan");
  }
}


void PixActionsSingleIBL::presetScan(std::string directory, long int id, long int presetType, int feflav, PixActions::SyncType) {
  if(m_isSetup){
    try {
      PixThread::ScopeLock myLock(m_thread);
      m_preAmpKilled = false;
      m_moduleGroup->initHW();
      if (m_pixScan != 0) {
        delete m_pixScan;
        m_pixScan = 0;
      } 
      m_pixScan = new PixScan((PixScan::ScanType)presetType,(PixModule::FEflavour)feflav);
      std::map<std::string, int> scansList = m_pixScan->getScanTypes();
      for(std::map<std::string, int>::iterator it = scansList.begin(); it!=scansList.end(); it++){
	if (it->second == presetType)   {
	  std::cout << "PixActionsSingleIBL::presetScan" << std::endl;
	  scan(directory, id);
	}
      }
    }
    catch(...) {
      m_info = "Unknown exception caught in presetScan()";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }
  } else {
    retrySetupDb("presetScan");
    if (m_isSetup) presetScan(directory, id, presetType, feflav);
    else printFailSetupDb("presetScan");
  }
}

void PixActionsSingleIBL::getRodActive(std::string actionName, PixActions::SyncType ) 
{ 
  if(m_isSetup){
    PixThread::ScopeLock myLock(m_thread);
    try {
      if (actionName == m_descriptor.name) {
	m_moduleGroup->getActive();
      }
    }
    catch(...) {
      m_info = "Exception caught calling  PixModuleGroup::getActive. ";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }
  } else {
    retrySetupDb("getRodActive");
    if (m_isSetup)getRodActive(actionName) ;
    else printFailSetupDb("getRodActive");
  }
}


void PixActionsSingleIBL::setRodActive(std::string actionName, bool active, PixActions::SyncType ) 
{ 
  if(m_isSetup){  
    try {
      if (actionName == m_descriptor.name) {
	std::cout << "PixActionsSingleIBL::setRodActive " << active << std::endl;
	m_moduleGroup->setActive(active);
      }
    }
    catch(...) {
      m_info = "Exception caught calling PixModuleGroup::setActive. ";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }
  } else {
    retrySetupDb("setRodActive");
    if (m_isSetup) setRodActive(actionName, active);
    else printFailSetupDb("setRodActive");
  }
}

void PixActionsSingleIBL::getModuleActive(std::string actionName, long int modID, PixActions::SyncType ) 
{ 
  if (m_isSetup) {
    string mod;
    try {
      if (actionName == m_descriptor.name) {
	m_moduleGroup->getModuleActive(modID);
      }
    }
    catch(...) {
      m_info = "Exception caught  calling PixModuleGroup::getModuleActive. ";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }
  } else {
    retrySetupDb("getModuleActive");
    if (m_isSetup)getModuleActive(actionName, modID);
    else printFailSetupDb("getModuleActive");
  }
}


void PixActionsSingleIBL::setModuleActive(std::string actionName, long int modID, bool active, PixActions::SyncType ) 
{ 
  if (m_isSetup){
    try {
      if (actionName == m_descriptor.name) {
	//std::cout << "PixActionsSingleIBL::setModuleActive " << active << std::endl;
	m_moduleGroup->setModuleActive(modID, active);
      }
    }
    catch(...) {
      m_info = "Exception caught  calling PixModuleGroup::setModuleActive. ";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }
  } else {
    retrySetupDb("setModuleActive");
    if (m_isSetup)setModuleActive(actionName, modID, active) ;
    else printFailSetupDb("setModuleActive");
  }
} 

std::string PixActionsSingleIBL::getConnTagC(const char* subActionName)
{
  if(m_conn == 0 || name() != std::string(subActionName)) return std::string("");
  return m_conn->getConnTagC();
}

std::string PixActionsSingleIBL::getConnTagP(const char* subActionName)
{
  if(m_conn == 0 || name() != std::string(subActionName)) return std::string("");
  return m_conn->getConnTagP();
}

std::string PixActionsSingleIBL::getConnTagA(const char* subActionName)
{
  if(m_conn == 0 || name() != std::string(subActionName)) return std::string("");
  return m_conn->getConnTagA();
}

std::string PixActionsSingleIBL::getConnTagOI(const char* subActionName)
{
  if(m_conn == 0 || name() != std::string(subActionName)) return std::string("");
  return m_conn->getConnTagOI();
}

std::string PixActionsSingleIBL::getCfgTag(const char* subActionName)
{
  if(m_conn == 0 || name() != std::string(subActionName)) return std::string("");
  return m_conn->getCfgTag();
}

std::string PixActionsSingleIBL::getCfgTagOI(const char* subActionName)
{
  if(m_conn == 0 || name() != std::string(subActionName)) return std::string("");
  std::string patch = m_conn->getConnTagOI() + "-CFG"; //return dummy string until getCfgTagOI is implemented in PixConnectivity
  return patch;
}

std::string PixActionsSingleIBL::getModCfgTag(const char* subActionName)
{
  if(m_conn == 0 || name() != std::string(subActionName)) return std::string("");
  return m_conn->getModCfgTag();
}

void PixActionsSingleIBL::publishDescriptor()
{
  if(m_is != 0 && m_moduleGroup != 0){
    m_is->publish(m_moduleGroup->getISRoot()+"AVAILABLE",m_descriptor.available);
    if(m_descriptor.type == ANY_TYPE){
      m_is->publish(m_moduleGroup->getISRoot()+"TYPE",std::string("ANY_TYPE"));
    }else if(m_descriptor.type == SINGLE_TIM){
      m_is->publish(m_moduleGroup->getISRoot()+"TYPE",std::string("SINGLE_TIM"));
    }else if(m_descriptor.type == SINGLE_ROD){
      m_is->publish(m_moduleGroup->getISRoot()+"TYPE",std::string("SINGLE_IBLROD"));
    }else if(m_descriptor.type == MULTI){
      m_is->publish(m_moduleGroup->getISRoot()+"TYPE",std::string("MULTI"));
    }
    m_is->publish(m_moduleGroup->getISRoot()+"MANAGING-BROKER",m_descriptor.managingBrokerName);
    m_is->publish(m_moduleGroup->getISRoot()+"ALLOCATING-BROKER",m_descriptor.allocatingBrokerName);
    std::string allocatingPartition = m_descriptor.allocatingBrokerName;
    // Determining the partition name from available information
    if( allocatingPartition.size() ) {
      std::size_t pos = allocatingPartition.find("MultiCrate_Broker_");
      if(pos != std::string::npos) {
        allocatingPartition = allocatingPartition.substr(pos+18);
      }
      pos = allocatingPartition.find("CalibrationConsole_");
      if(pos != std::string::npos) {
        allocatingPartition = allocatingPartition.substr(pos+19);
      }
    } else {
      allocatingPartition = "NONE";
    }
    m_is->publish(m_moduleGroup->getISRoot()+"ALLOCATING-PARTITION",allocatingPartition);
  }
}

void PixActionsSingleIBL::publishAction(std::string action)
{
  m_cmdStart = time(NULL);
  if(m_is != 0 && m_moduleGroup != 0) {
    m_is->publish(m_moduleGroup->getISRoot()+"CURRENT-ACTION",action);
    m_is->removeVariable(m_moduleGroup->getISRoot()+"STATUS");
  }
}

void PixActionsSingleIBL::publishStatus(std::string status)
{
  m_cmdStop = time(NULL);
  int dt = m_cmdStop - m_cmdStart;
  int st = m_cmdStart;
  if(m_is != 0 && m_moduleGroup != 0) {
    m_is->publish(m_moduleGroup->getISRoot()+"STATUS",status);
    m_is->publish(m_moduleGroup->getISRoot()+"LAST-CMD-TIME",dt);
    m_is->publish(m_moduleGroup->getISRoot()+"LAST-CMD-START",st);
  }
}

void PixActionsSingleIBL::removeISVariables()
{
  if(m_is != 0 && m_moduleGroup != 0){
    m_is->removeVariable(m_moduleGroup->getISRoot()+"STATUS");
    m_is->removeVariable(m_moduleGroup->getISRoot()+"CURRENT-ACTION");
    m_is->removeVariable(m_moduleGroup->getISRoot()+"AVAILABLE");
    m_is->removeVariable(m_moduleGroup->getISRoot()+"TYPE");
    m_is->removeVariable(m_moduleGroup->getISRoot()+"MANAGING-BROKER");
    m_is->removeVariable(m_moduleGroup->getISRoot()+"ALLOCATING-BROKER");
    m_is->removeVariable(m_moduleGroup->getISRoot()+"ALLOCATING-PARTITION");
  }
}

void PixActionsSingleIBL::allocate(const char* allocatingBrokerName)
{
  if (m_isSetup){
    try{
    PixActions::allocate(allocatingBrokerName);
    } catch(const std::runtime_error &e){
      std::string info = e.what();
      m_info = "Exception caught in allocate()" + info;
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    } catch(...){
      m_info = "Unknown exception caught in allocate()";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    }
  } else {
    retrySetupDb("allocate");
    if (m_isSetup) allocate(allocatingBrokerName);
    else printFailSetupDb("allocate");
  }
}

void PixActionsSingleIBL::deallocate(const char* deallocatingBrokerName)
{
  if (m_isSetup){
    try{
      if(m_rodMonitor) unSetupRodMonitoring();
      if(m_autoDisTask)unSetupPixAutoDis();
      PixActions::deallocate(deallocatingBrokerName);
      std::cout << "Action " << descriptor().name << " deallocating " << std::endl;
    } catch(const std::runtime_error &e){
      std::string info = e.what();
      m_info = "Exception caught in deallocate()" + info;
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
    } catch (...){
      m_info = "Unknown exception caught in deallocate()";
      ers::error(PixLib::pix::daq (ERS_HERE,getRodName(),m_info));
   }
  } else {
    retrySetupDb("deallocate");
    if (m_isSetup) deallocate(deallocatingBrokerName);
    else printFailSetupDb("dallocate");
  }
}


void PixActionsSingleIBL::test1(std::string test, PixActions::SyncType sync) {
  std::cout << "ROD " << name() << " test1 start" << std::endl;
  sleep(10);
  std::cout << "ROD " << name() << " test1 stop" << std::endl;
}

void PixActionsSingleIBL::test2(std::string test, PixActions::SyncType sync) {
  std::cout << "ROD " << name() << " test2 executed - " << test << std::endl;
}

