/////////////////////////////////////////////////////////////////////
// PixActions.cxx
// version 1.1
/////////////////////////////////////////////////////////////////////
//
// 03/04/06  Version 1.0 (CS)
//           Initial release
//
// 07/09/06  Version 1.1 (CS)
//           IPC version
//
//
// 09/02/07 Version 1.1.1 (GAO - NG)
//          Added methods for STControlNG
//

//! Base class for the Pixel actions

#include "PixActions/PixActions.h"

using namespace PixLib;


//! Constructor
PixActions::PixActions(IPCPartition *ipcPartition, std::string ipcName) :
  IPCNamedObject<POA_ipc::PixActions,ipc::single_thread,ipc::persistent>(*ipcPartition,ipcName.c_str()) {  
  m_mutex = new PixMutex(ipcName);
  m_ipcPartition = new IPCPartition(*ipcPartition);
  // Publish in IPC partition
  publish();
  m_is = new PixISManager(ipcPartition->name());
}


//! Destructor
PixActions::~PixActions() { 
  if (m_is != 0)  {
    removeISVariables();
    delete m_is;
  }
  if (m_ipcPartition != 0) delete m_ipcPartition;
  delete m_mutex;
  // Withdraw from IPC partition
  withdraw();
}


//! Callback handling
void PixActions::ipc_setCallback(ipc::PixActionsCallback_ptr cb) {
  PixLock lock(m_mutex);
  //std::cout << "Setting callback" << std::endl;
  // Get callback and save number of current connection
  m_cb = ipc::PixActionsCallback::_duplicate(cb);
  m_cbNumber = m_cb->countConnections();
  // Increase connections counter
  m_cb->increaseConnections();
}

void PixActions::ipc_unsetCallback() {
  PixLock lock(m_mutex);
  // Get callback and save number of current connection
  if(m_cb != 0){
    m_cb->decreaseConnections();
    m_cb = 0;
    m_cbNumber = 0;
  }
}

char* PixActions::ipc_getConnTagC(const char* subActionName)
{
  PixLock lock(m_mutex);
  std::string res = getConnTagC(subActionName);
  if(res == "") return 0;
  char* result = new char[res.size()+1];

  for(unsigned int i = 0; i <= res.size(); i++) result[i] = res.c_str()[i];
  return result;
}

char* PixActions::ipc_getConnTagP(const char* subActionName)
{
  PixLock lock(m_mutex);
  std::string res = getConnTagP(subActionName);
  if(res == "") return 0;
  char* result = new char[res.size()+1];

  for(unsigned int i = 0; i <= res.size(); i++) result[i] = res.c_str()[i];
  return result;
}

char* PixActions::ipc_getConnTagA(const char* subActionName)
{
  PixLock lock(m_mutex);
  std::string res = getConnTagA(subActionName);
  if(res == "") return 0;
  char* result = new char[res.size()+1];

  for(unsigned int i = 0; i <= res.size(); i++) result[i] = res.c_str()[i];
  return result;
}

char* PixActions::ipc_getConnTagOI(const char* subActionName)
{
  PixLock lock(m_mutex);
  std::string res = getConnTagOI(subActionName);
  if(res == "") return 0;
  char* result = new char[res.size()+1];

  for(unsigned int i = 0; i <= res.size(); i++) result[i] = res.c_str()[i];
  return result;
}

char* PixActions::ipc_getCfgTag(const char* subActionName)
{
  PixLock lock(m_mutex);
  std::string res = getCfgTag(subActionName);
  if(res == "") return 0;
  char* result = new char[res.size()+1];

  for(unsigned int i = 0; i <= res.size(); i++) result[i] = res.c_str()[i];
  return result;
}

char* PixActions::ipc_getCfgTagOI(const char* subActionName)
{
  PixLock lock(m_mutex);
  std::string res = getCfgTagOI(subActionName);
  if(res == "") return 0;
  char* result = new char[res.size()+1];

  for(unsigned int i = 0; i <= res.size(); i++) result[i] = res.c_str()[i];
  return result;
}

char* PixActions::ipc_getModCfgTag(const char* subActionName)
{
  PixLock lock(m_mutex);
  std::string res = getModCfgTag(subActionName);
  if(res == "") return 0;
  char* result = new char[res.size()+1];

  for(unsigned int i = 0; i <= res.size(); i++) result[i] = res.c_str()[i];
  return result;
}

char* PixActions::ipc_name()
{
  PixLock lock(m_mutex);
  char* result = new char[m_descriptor.name.size()+1];
  //std::cout << "size " << m_descriptor.name.size() << std::endl;
  for(unsigned int i = 0; i <= m_descriptor.name.size(); i++) result[i] = m_descriptor.name.c_str()[i];
  return result;//(char*)m_descriptor.name.c_str();

}

MY_LONG_INT PixActions::ipc_type()
{
  return (MY_LONG_INT) m_descriptor.type;
}


char* PixActions::ipc_managingBrokerName() 
{
  PixLock lock(m_mutex);
  char* result = new char[m_descriptor.managingBrokerName.size()+1];
  for(unsigned int i = 0; i <= m_descriptor.managingBrokerName.size(); i++) result[i] = m_descriptor.managingBrokerName.c_str()[i];
  return result;

}

char* PixActions::ipc_allocatingBrokerName() 
{
  PixLock lock(m_mutex);
  char* result = new char[m_descriptor.allocatingBrokerName.size()+1];
  for(unsigned int i = 0; i <= m_descriptor.allocatingBrokerName.size(); i++) result[i] = m_descriptor.allocatingBrokerName.c_str()[i];
  return result;

}


void PixActions::ipc_setISManager(const char* isManagerName)
{
  PixLock lock(m_mutex);
  PixISManager *is = new PixISManager(m_ipcPartition);
    
  if(!is){
    std::cout << "[WARNING] PixActions::ipc_setISManager: " << isManagerName << " not found in partition " << m_ipcPartition->name() << std::endl;
  }else{
    m_is = is;
  }

}

void PixActions::allocate(const char* allocatingBrokerName){
  PixLock lock(m_mutex);
  //TODO:Add checks for already allocated actions (in case of Expert broker) and report allocating broker
  std::cout << "Attempting PixActions::allocate " << m_descriptor.name << std::endl;
  //Throw exception to PixActionsSingleROD::allocate if action is already allocated
  if (m_descriptor.available == false) {
    std::string info = m_descriptor.name + " is already allocated by " + m_descriptor.allocatingBrokerName;
    throw std::runtime_error(info) ;
  }
  m_descriptor.available=false; 
  m_descriptor.allocatingBrokerName = allocatingBrokerName;
  publishDescriptor(); 
}

void PixActions::deallocate(const char* deallocatingBrokerName) { 
  PixLock lock(m_mutex);
  //std::cout << "Deallocating broker name " << deallocatingBrokerName << " broker that had it allocated " << m_descriptor.allocatingBrokerName << std::endl; 
  //Throw exception to PixActionsSingleROD::deallocate if action was allocated by a different Broker
  if(m_descriptor.allocatingBrokerName != "" && deallocatingBrokerName != m_descriptor.allocatingBrokerName){
    std::cout << "[WARNING] PixActions::deallocate Attempting to deallocate an action not owned by the broker doing the attempt" << std::endl;
    std::string info = "Attempting to deallocate an action you do not own. "+ m_descriptor.name +" was allocated by " + m_descriptor.allocatingBrokerName;
    throw std::runtime_error(info) ;
    return;
  }
  deallocate();
}

void PixActions::deallocate() {
  PixLock lock(m_mutex);
  m_descriptor.available=true;
  m_descriptor.allocatingBrokerName="";
  publishDescriptor();
}



void PixActions::publishDescriptor()
{
  PixLock lock(m_mutex);
  if(m_is != 0){
    m_is->publish(m_descriptor.name+"/AVAILABLE",m_descriptor.available);
    if(m_descriptor.type == ANY_TYPE){
      m_is->publish(m_descriptor.name+"/TYPE",std::string("ANY_TYPE"));
    }else if(m_descriptor.type == SINGLE_TIM){
      m_is->publish(m_descriptor.name+"/TYPE",std::string("SINGLE_TIM"));
    }else if(m_descriptor.type == SINGLE_ROD){
      m_is->publish(m_descriptor.name+"/TYPE",std::string("SINGLE_ROD"));
    }else if(m_descriptor.type == MULTI){
      m_is->publish(m_descriptor.name+"/TYPE",std::string("MULTI"));
    }
    m_is->publish(m_descriptor.name+"/MANAGING-BROKER",m_descriptor.managingBrokerName);
    m_is->publish(m_descriptor.name+"/ALLOCATING-BROKER",m_descriptor.allocatingBrokerName);
  }
}

void PixActions::publishAction(std::string action)
{
  PixLock lock(m_mutex);
  if(m_is!=0) m_is->publish(m_descriptor.name+"/CURRENT-ACTION",action);
}


void PixActions::publishStatus(std::string status)
{
  PixLock lock(m_mutex);
  if(m_is!=0) m_is->publish(m_descriptor.name+"/STATUS",status);
}

void PixActions::removeISVariables()
{
  PixLock lock(m_mutex);
  m_is->removeVariable(m_descriptor.name+"/STATUS");
  m_is->removeVariable(m_descriptor.name+"/CURRENT-ACTION");
  m_is->removeVariable(m_descriptor.name+"/AVAILABLE");
  m_is->removeVariable(m_descriptor.name+"/TYPE");
  m_is->removeVariable(m_descriptor.name+"/MANAGING-BROKER");
  m_is->removeVariable(m_descriptor.name+"/ALLOCATING-BROKER");
}



//*********************************************************************
//*********************************************************************
//***************  Standard Actions implementation ********************
//*********************************************************************
//*********************************************************************

// State transitions
void PixActions::ipc_initRun(bool sync) 
{
  implement("initRun", PixActions::SyncType((int)sync), ipc::ActionInput());
}

void PixActions::ipc_initCal(bool sync) 
{
  implement("initCal", PixActions::SyncType((int)sync), ipc::ActionInput());
}

void PixActions::ipc_resetRod(bool sync) 
{
  implement("resetRod", PixActions::SyncType((int)sync), ipc::ActionInput());
}

void PixActions::ipc_configureBoc(bool sync) 
{
  implement("configureBoc", PixActions::SyncType((int)sync), ipc::ActionInput());
}

void PixActions::ipc_setup(bool sync) 
{
  implement("setup", PixActions::SyncType((int)sync), ipc::ActionInput());
}

void PixActions::ipc_connect(bool sync) 
{
  implement("connect", PixActions::SyncType((int)sync), ipc::ActionInput());
}

void PixActions::ipc_prepareForRun(bool sync) 
{
  implement("prepareForRun", PixActions::SyncType((int)sync), ipc::ActionInput());
}



void PixActions::ipc_setActionsConfig(bool sync) 
{
  implement("setActionsConfig", PixActions::SyncType((int)sync), ipc::ActionInput());
}

void PixActions::ipc_startTrigger(bool sync) 
{
  implement("startTrigger", PixActions::SyncType((int)sync), ipc::ActionInput());
}

void PixActions::ipc_stopTrigger(bool sync) 
{
  implement("stopTrigger", PixActions::SyncType((int)sync), ipc::ActionInput());
}

void PixActions::ipc_stopEB(bool sync) 
{
  implement("stopEB", PixActions::SyncType((int)sync), ipc::ActionInput());
}

void PixActions::ipc_disconnect(bool sync) 
{
  implement("disconnect", PixActions::SyncType((int)sync), ipc::ActionInput());
}

void PixActions::ipc_unconfigure(bool sync) 
{
  implement("unconfigure", PixActions::SyncType((int)sync), ipc::ActionInput());
}

void PixActions::ipc_probe(bool sync) 
{
  implement("probe", PixActions::SyncType((int)sync), ipc::ActionInput());
}

void PixActions::ipc_reset(bool sync) 
{
  implement("reset", PixActions::SyncType((int)sync), ipc::ActionInput());
}

void PixActions::ipc_resetViset(bool sync) 
{
  implement("resetViset", PixActions::SyncType((int)sync), ipc::ActionInput());
}

void PixActions::ipc_DCSsync(bool sync) 
{
  implement("DCSsync", PixActions::SyncType((int)sync), ipc::ActionInput());
}

void PixActions::ipc_resetmods(bool sync) 
{
  implement("resetmods", PixActions::SyncType((int)sync), ipc::ActionInput());
}
 
void PixActions::ipc_disableFailed(bool sync) 
{
  implement("disableFailed", PixActions::SyncType((int)sync), ipc::ActionInput());
}

void PixActions::ipc_listRODs(bool sync) 
{
  implement("listRODs", PixActions::SyncType((int)sync), ipc::ActionInput());
}



//! Configuration handling
void PixActions::ipc_readConfig(bool sync) 
{
  implement("readConfig", PixActions::SyncType((int)sync), ipc::ActionInput());
}

void PixActions::ipc_loadConfig(bool sync) 
{
  implement("loadConfig", PixActions::SyncType((int)sync), ipc::ActionInput());
}

void PixActions::ipc_sendConfig(bool sync) 
{
  implement("sendConfig", PixActions::SyncType((int)sync), ipc::ActionInput());
}



void PixActions::ipc_configure(bool sync, const ipc::ActionInput& input) 
{
  implement("configure", PixActions::SyncType((int)sync), input);
}

void PixActions::ipc_setPixScan(bool sync, const ipc::ActionInput& input) 
{
  implement("setPixScan", PixActions::SyncType((int)sync), input);
}

void PixActions::ipc_setConnTest(bool sync, const ipc::ActionInput& input) 
{
  implement("setConnTest", PixActions::SyncType((int)sync), input);
}

void PixActions::ipc_saveConfig(bool sync, const ipc::ActionInput& input) 
{
  implement("saveConfig", PixActions::SyncType((int)sync), input);
}

void PixActions::ipc_reloadConfig(bool sync, const ipc::ActionInput& input) 
{
  implement("reloadConfig", PixActions::SyncType((int)sync), input);
}

void PixActions::ipc_getRodActive(bool sync, const ipc::ActionInput& input) 
{ 
  implement("getRodActive", PixActions::SyncType((int)sync), input);
}



void PixActions::ipc_setRodBusyMask(bool sync, const ipc::ActionInput& input) 
{
  implement("setRodBusyMask", PixActions::SyncType((int)sync), input);
}


void PixActions::ipc_rodStatus(ipc::ActionOutput& output) 
{
  implementInOut("rodStatus", output);
}


void PixActions::ipc_setVcal(bool sync, const ipc::ActionInput& input) 
{
  implement("setVcal", PixActions::SyncType((int)sync), input); 
}

void PixActions::ipc_incrMccDelay(bool sync, const ipc::ActionInput& input) 
{
  implement("incrMccDelay", PixActions::SyncType((int)sync), input); 
}

void PixActions::ipc_execConnectivityTest(bool sync, const ipc::ActionInput& input) 
{
  implement("execConnectivityTest", PixActions::SyncType((int)sync), input); 
}

void PixActions::ipc_selectFE(bool sync, const ipc::ActionInput& input) 
{
  implement("selectFE", PixActions::SyncType((int)sync), input); 
}

void PixActions::ipc_configSingleMod(bool sync, const ipc::ActionInput& input) 
{
  implement("configSingleMod", PixActions::SyncType((int)sync), input); 
}

void PixActions::ipc_setHalfclockMod(bool sync) 
{
  implement("setHalfclockMod", PixActions::SyncType((int)sync), ipc::ActionInput()); 
}

void PixActions::ipc_setModuleActive(bool sync, const ipc::ActionInput& input)
{
  implement("setModuleActive", PixActions::SyncType((int)sync), input);
}

void PixActions::ipc_getModuleActive(bool sync, const ipc::ActionInput& input) 
{
  implement("getModuleActive", PixActions::SyncType((int)sync), input); 
}

void PixActions::ipc_allocate(const ipc::ActionInput& input) 
{
  implement("allocate", PixActions::Asynchronous, input);
}

void PixActions::ipc_deallocate(const ipc::ActionInput& input) 
{
  implement("deallocate", PixActions::Asynchronous, input);
}


// Abort 
void PixActions::ipc_abortScan(bool sync) 
{
  implement("abortScan", PixActions::SyncType((int)sync), ipc::ActionInput());
}

void PixActions::ipc_abortConnTest(bool sync) 
{
  implement("abortConnTest", PixActions::SyncType((int)sync), ipc::ActionInput());
}




void PixActions::ipc_setRodActive(bool sync, const ipc::ActionInput& input) 
{ 
  implement("setRodActive", PixActions::SyncType((int)sync), input);
}

void PixActions::ipc_optoOnOff(bool sync, const ipc::ActionInput& input) 
{
  implement("optoOnOff", PixActions::SyncType((int)sync), input);
}

void PixActions::ipc_lvOnOff(bool sync, const ipc::ActionInput& input) 
{
  implement("lvOnOff", PixActions::SyncType((int)sync), input);
}
   
void PixActions::ipc_disable(bool sync, const ipc::ActionInput& input) 
{
  implement("disable", PixActions::SyncType((int)sync), input);
}
   
void PixActions::ipc_enable(bool sync, const ipc::ActionInput& input) 
{
  implement("enable", PixActions::SyncType((int)sync), input);
}
   
void PixActions::ipc_dtreset(bool sync, const ipc::ActionInput& input) 
{
  implement("reset", PixActions::SyncType((int)sync), input);
}
   
void PixActions::ipc_warmReconfig(bool sync, const ipc::ActionInput& input) 
{
  implement("warmReconfig", PixActions::SyncType((int)sync), input);
}

void PixActions::ipc_recover(bool sync, const ipc::ActionInput& input) 
{
  implement("recover", PixActions::SyncType((int)sync), input);
}
   
void PixActions::ipc_scan(bool sync, const ipc::ActionInput& input) 
{
  implement("scan", PixActions::SyncType((int)sync), input);
}

void PixActions::ipc_presetScan(bool sync, const ipc::ActionInput& input) 
{
  implement("presetScan", PixActions::SyncType((int)sync), input);
}

void PixActions::ipc_reloadTags(bool sync, const ipc::ActionInput& input) 
{
  implement("reloadTags", PixActions::SyncType((int)sync), input);
}

void PixActions::ipc_test1(bool sync, const ipc::ActionInput& input) 
{
  implement("test1", PixActions::SyncType((int)sync), input); 
}

void PixActions::ipc_test2(bool sync, const ipc::ActionInput& input) 
{
  implement("test2", PixActions::SyncType((int)sync), input); 
}





// Generic IPC methods
void PixActions::implement(std::string method, PixActions::SyncType sync, const ipc::ActionInput input) 
{
  PixLock lock(m_mutex);
  //std::cout << "Executing " << method << " - " << m_descriptor.name << std::endl;  
  try {
    if(method=="initRun")               initRun(sync);
    else if(method=="initCal")          initCal(sync);
    else if(method=="resetRod")         resetRod(sync);
    else if(method=="configureBoc")     configureBoc(sync);
    else if(method=="setup")            setup(sync);
    else if(method=="connect")          connect(sync);
    else if(method=="prepareForRun")    prepareForRun(sync);
    else if(method=="setActionsConfig") setActionsConfig(sync);
    else if(method=="startTrigger")     startTrigger(sync);
    else if(method=="stopTrigger")      stopTrigger(sync);
    else if(method=="stopEB")           stopEB(sync);
    else if(method=="disconnect")       disconnect(sync);
    else if(method=="unconfigure")      unconfigure(sync);
    else if(method=="probe")            probe(sync);
    else if(method=="reset")            reset(sync);
    else if(method=="resetViset")       resetViset(sync);
    else if(method=="DCSsync")          DCSsync(sync);
    else if(method=="readConfig")       readConfig(sync);
    else if(method=="loadConfig")       loadConfig(sync);
    else if(method=="sendConfig")       sendConfig(sync);
    else if(method=="resetmods")        resetmods(sync);
    else if(method=="abortScan")        abortScan(sync);
    else if(method=="abortConnTest")    abortConnTest(sync);
    else if(method=="disableFailed")    disableFailed(sync);
    else if(method=="listRODs")         listRODs(sync);

    else if(method=="configure")        configure(stringFromActionInput(input, 0), sync);
    else if(method=="setPixScan")       setPixScan(stringFromActionInput(input, 0), sync);
    else if(method=="setConnTest")      setConnTest(stringFromActionInput(input, 0), sync);
    else if(method=="getRodActive")     getRodActive(stringFromActionInput(input, 0), sync);
    else if(method=="saveConfig")       saveConfig(stringFromActionInput(input, 0), sync);
    else if(method=="reloadConfig")     reloadConfig(stringFromActionInput(input, 0), sync);

    else if(method=="setRodBusyMask")   setRodBusyMask(input.m_ints[0], sync);

    else if(method=="setVcal")          setVcal(input.m_floats[0], input.m_bools[0], sync);
    else if(method=="incrMccDelay")     incrMccDelay(input.m_floats[0], input.m_bools[0], sync);

    else if(method=="setRodActive")     setRodActive(stringFromActionInput(input, 0), input.m_bools[0], sync);

    else if(method=="getModuleActive")  getModuleActive(stringFromActionInput(input, 0), input.m_ints[0], sync);
    else if(method=="selectFE")         selectFE(stringFromActionInput(input, 0), input.m_ints[0], sync);
    else if(method=="scan")             scan(stringFromActionInput(input, 0), input.m_ints[0], sync);
    else if(method=="presetScan")       presetScan(stringFromActionInput(input, 0), input.m_ints[0], input.m_ints[1], input.m_ints[2], sync); // T
    else if(method=="execConnectivityTest") execConnectivityTest(stringFromActionInput(input, 0), input.m_ints[0], sync);

    else if(method=="configSingleMod")  configSingleMod(stringFromActionInput(input, 0), input.m_ints[0], input.m_ints[1], sync);
    else if(method=="setHalfclockMod")  setHalfclockMod(sync);
    else if(method=="setModuleActive")  setModuleActive(stringFromActionInput(input, 0), input.m_ints[0], input.m_bools[0], sync); 

    else if(method=="reloadTags")       reloadTags(stringFromActionInput(input, 0), stringFromActionInput(input, 1), input.m_ints[0], input.m_ints[1], sync);

    else if(method=="allocate")         allocate(input.m_strings[0]); 
    else if(method=="deallocate")       deallocate(input.m_strings[0]); 
    
    else if(method=="optoOnOff")        optoOnOff(input.m_bools[0], sync);
    else if(method=="lvOnOff")          lvOnOff(input.m_bools[0], sync);
    else if(method=="disable")          disable(stringFromActionInput(input, 0), sync);
    else if(method=="enable")           enable(stringFromActionInput(input, 0), sync);
    else if(method=="reset")            reset(stringFromActionInput(input, 0), sync);
    else if(method=="warmReconfig")     warmReconfig(stringFromActionInput(input, 0), stringFromActionInput(input, 1), sync);
    else if(method=="recover")          recover(stringFromActionInput(input, 0), stringFromActionInput(input, 1), sync);
    else if(method=="test1")            test1(stringFromActionInput(input, 0), sync);
    else if(method=="test2")            test2(stringFromActionInput(input, 0), sync);

  } catch(...) {
    // Set error in bit mask
    std::cout<<"Exception caugth in "<<__PRETTY_FUNCTION__<<" method "<<method<<std::endl;
    m_cb->setErrors(m_cbNumber);
  }
  
  // Decrease connections counter
  //if(!sync) 
  m_cb->decreaseConnections();

}

// Generic IPC methods
void PixActions::implementInOut(std::string method, ipc::ActionOutput& output) 
{
  PixLock lock(m_mutex);
  try {
    if(method=="rodStatus") rodStatus(output);
  } catch(...) {
    // Set error in bit mask
    m_cb->setErrors(m_cbNumber);
  }
  
  // Decrease connections counter
  //if(!sync) 
  m_cb->decreaseConnections();
}

void PixActions::rodStatus(bool& ) 
{

}

void PixActions::rodStatus(ipc::ActionOutput& output)
{
  PixLock lock(m_mutex);
  std::map<std::string,bool> resultMap = rodStatus();
  boolActionOutput(resultMap, output);
}

std::vector<std::string> PixActions::listSubActions(PixActions::Type type)
{
  return std::vector<std::string>();
}


ipc::stringList* PixActions::ipc_listSubActions(ipc::PixActionsType type){
  PixLock lock(m_mutex);

  // Get list of actions
  PixActions::Type pixlib_type = (PixActions::Type((int)type));
  std::vector<std::string> tempActions = listSubActions(pixlib_type);
  std::vector<std::string>::iterator action = tempActions.begin();
  
  // Copy strings in stringList and return
  ipc::stringList* actions = new ipc::stringList(tempActions.size());
  actions->length(tempActions.size());
  for(int i=0; i<(int)tempActions.size(); i++, action++) {
    int length = action->length();
    char* ret = new char[length+1];
    for(int j=0; j<=length; j++) ret[j]=(action->c_str())[j];
    (*actions)[i] = ret;
  }
  return actions;

}


std::string PixActions::stringFromActionInput(const ipc::ActionInput actionInput, int pos)
{
  PixLock lock(m_mutex);
  const char* out = actionInput.m_strings[pos];
  std::string output(out);
  return output;
}



void PixActions::boolActionOutput(std::map<std::string,bool> resultMap, ipc::ActionOutput& output)
{
  PixLock lock(m_mutex);
  int boolsLength   = output.m_boolsOut.length()+resultMap.size();
  int actionsLength = output.m_actions.length()+resultMap.size();
  ipc::boolList   outputBools  (boolsLength);
  ipc::stringList outputActions(actionsLength);
  outputBools.length(boolsLength);
  outputActions.length(actionsLength);

  for(unsigned int i = 0; i<output.m_actions.length(); i++){
    outputBools[i]   = output.m_boolsOut[i];
    outputActions[i] = output.m_actions[i];
  }
  std::map<std::string,bool>::iterator resultMapIt, resultMapItEnd = resultMap.end();
  resultMapIt = resultMap.begin();
  for(int i = output.m_actions.length(); resultMapIt != resultMapItEnd; resultMapIt++, i++){
    outputBools  [i] = resultMapIt->second;
    outputActions[i] = CORBA::string_dup(resultMapIt->first.c_str());
  }
  output.m_boolsOut.length(boolsLength);
  output.m_actions.length (actionsLength);
  for(int i = 0; i<boolsLength; i++){
    output.m_boolsOut[i] = outputBools[i];
    output.m_actions[i]  = outputActions[i];
  }
  std::cout << "End of PixActions::boolActionOutput - output.m_boolsOut.length() = " << output.m_boolsOut.length() << std::endl;
}


