/////////////////////////////////////////////////////////////////////
// PixActions.h
// version 1.1
/////////////////////////////////////////////////////////////////////
//
// 31/03/06  Version 1.0 (CS)
//           Initial release
//
// 07/09/06  Version 1.1 (CS)
//           IPC version
//
//
// 09/02/07 Version 1.1.1 (GAO - NG)
//          Added methods for STControlNG
//


//! Base class for the Pixel actions

#ifndef _PIXLIB_ACTIONS
#define _PIXLIB_ACTIONS

#include <ipc/partition.h>
#include <ipc/object.h>

//#include "PixController/PixScanConfig.h"

#include "PixActionsIDL.hh"
#include "PixUtilities/PixISManager.h"
#include "PixUtilities/PixLock.h"

#ifndef MY_LONG_INT
#ifdef CORBALONGLONG
#define MY_LONG_INT int32_t
#else
#define MY_LONG_INT CORBA::Long
#endif
#endif

namespace PixLib {

  class PixBrokerMultiCrateExpert;

  class PixActions : public IPCNamedObject<POA_ipc::PixActions,ipc::single_thread,ipc::persistent> {
    friend class PixBrokerMultiCrateExpert;

  public:
    
    enum Type {ANY_TYPE, SINGLE_ROD, SINGLE_TIM, MULTI};
    enum SyncType{Asynchronous = 0, Synchronous = 1};
    
    struct PixActionsDescriptor {
      std::string name;
      bool available;
      Type type;
      std::string allocatingBrokerName;
      std::string managingBrokerName;
      friend bool operator==(PixActionsDescriptor &d1, PixActionsDescriptor &d2) {
	return(d1.name==d2.name &&
	       d1.available==d2.available &&
	       d1.type==d2.type &&
	       d1.allocatingBrokerName==d2.allocatingBrokerName && 
	       d1.managingBrokerName==d2.managingBrokerName);
      }

      friend bool operator<(PixActionsDescriptor &d1, PixActionsDescriptor &d2) {
	return(d1.name<d2.name);
      }
    };
    
  public:
    
    // Constructor
    PixActions(IPCPartition *ipcPartition, std::string ipcName);
    
    // Destructor
    virtual ~PixActions();
    
    // Callback handling
    virtual void ipc_setCallback(ipc::PixActionsCallback_ptr cb);
    virtual void ipc_unsetCallback();

    virtual void ipc_resetmods        (bool sync);
    virtual void ipc_initRun          (bool sync); 
    virtual void ipc_initCal          (bool sync);    
    virtual void ipc_resetRod         (bool sync);
    virtual void ipc_configureBoc     (bool sync);
    virtual void ipc_listRODs         (bool sync);
    virtual void ipc_disableFailed    (bool sync);
    virtual void ipc_setup            (bool sync);
    virtual void ipc_connect          (bool sync);
    virtual void ipc_prepareForRun    (bool sync);
    virtual void ipc_startTrigger     (bool sync);
    virtual void ipc_stopTrigger      (bool sync);
    virtual void ipc_stopEB           (bool sync);
    virtual void ipc_disconnect       (bool sync);
    virtual void ipc_unconfigure      (bool sync);
    virtual void ipc_probe            (bool sync);
    virtual void ipc_abortScan        (bool sync);
    virtual void ipc_abortConnTest    (bool sync);
    virtual void ipc_reset            (bool sync);
    virtual void ipc_resetViset       (bool sync);
    virtual void ipc_readConfig       (bool sync);
    virtual void ipc_loadConfig       (bool sync);
    virtual void ipc_sendConfig       (bool sync);
    virtual void ipc_setActionsConfig (bool sync);
    virtual void ipc_DCSsync          (bool sync);
    virtual void ipc_setHalfclockMod  (bool sync);

    virtual void ipc_configure            (bool sync, const ipc::ActionInput& input);
    virtual void ipc_setPixScan           (bool sync, const ipc::ActionInput& input);
    virtual void ipc_setConnTest          (bool sync, const ipc::ActionInput& input);
    virtual void ipc_incrMccDelay         (bool sync, const ipc::ActionInput& input);
    virtual void ipc_selectFE             (bool sync, const ipc::ActionInput& input);
    virtual void ipc_configSingleMod      (bool sync, const ipc::ActionInput& input);
    virtual void ipc_setModuleActive      (bool sync, const ipc::ActionInput& input);
    virtual void ipc_getModuleActive      (bool sync, const ipc::ActionInput& input);
    virtual void ipc_setVcal              (bool sync, const ipc::ActionInput& input);
    virtual void ipc_setRodActive         (bool sync, const ipc::ActionInput& input);
    virtual void ipc_getRodActive         (bool sync, const ipc::ActionInput& input);
    virtual void ipc_scan                 (bool sync, const ipc::ActionInput& input);
    virtual void ipc_presetScan           (bool sync, const ipc::ActionInput& input);
    virtual void ipc_execConnectivityTest (bool sync, const ipc::ActionInput& input);
    virtual void ipc_saveConfig           (bool sync, const ipc::ActionInput& input);
    virtual void ipc_reloadConfig         (bool sync, const ipc::ActionInput& input);
    virtual void ipc_setRodBusyMask       (bool sync, const ipc::ActionInput& input);
    virtual void ipc_reloadTags           (bool sync, const ipc::ActionInput& input);
    virtual void ipc_optoOnOff            (bool sync, const ipc::ActionInput& input);
    virtual void ipc_lvOnOff              (bool sync, const ipc::ActionInput& input);
    virtual void ipc_disable              (bool sync, const ipc::ActionInput& input);
    virtual void ipc_enable               (bool sync, const ipc::ActionInput& input);
    virtual void ipc_dtreset              (bool sync, const ipc::ActionInput& input);
    virtual void ipc_warmReconfig         (bool sync, const ipc::ActionInput& input);
    virtual void ipc_recover              (bool sync, const ipc::ActionInput& input);
    virtual void ipc_test1                (bool sync, const ipc::ActionInput& input);
    virtual void ipc_test2                (bool sync, const ipc::ActionInput& input);

    //set the ISManager needed to publish and read values
    virtual void ipc_setISManager(const char* isManagerName);

    virtual void ipc_rodStatus(ipc::ActionOutput& output);


    virtual void ipc_allocate  (const ipc::ActionInput& input);
    virtual void ipc_deallocate(const ipc::ActionInput& input);

    // Name
    char*    ipc_name(); 
    MY_LONG_INT  ipc_type();
    char*    ipc_managingBrokerName();
    char*    ipc_allocatingBrokerName(); 
    bool     ipc_available() {return m_descriptor.available;}

    char* ipc_getConnTagC  (const char* subActionName);
    char* ipc_getConnTagP  (const char* subActionName);
    char* ipc_getConnTagA  (const char* subActionName);
    char* ipc_getConnTagOI (const char* subActionName);
    char* ipc_getCfgTag    (const char* subActionName);
    char* ipc_getCfgTagOI  (const char* subActionName);
    char* ipc_getModCfgTag (const char* subActionName);

    bool ipc_isHVOff(){return isHVOff();}

    virtual ipc::stringList* ipc_listSubActions(ipc::PixActionsType type = ipc::ANY_TYPE);


    // State transitions
    virtual void initRun       (PixActions::SyncType sync=PixActions::Synchronous) = 0; 
    virtual void initCal       (PixActions::SyncType sync=PixActions::Synchronous) = 0;
    virtual void resetRod      (PixActions::SyncType sync=PixActions::Synchronous) = 0;
    virtual void configureBoc  (PixActions::SyncType sync=PixActions::Synchronous) = 0;

    virtual void setup         (PixActions::SyncType sync=PixActions::Synchronous) = 0; 
    virtual void configure     (std::string partName, PixActions::SyncType sync=PixActions::Synchronous) = 0;
    virtual void connect       (PixActions::SyncType sync=PixActions::Synchronous) = 0;
    virtual void prepareForRun (PixActions::SyncType sync=PixActions::Synchronous) = 0;
    virtual void startTrigger  (PixActions::SyncType sync=PixActions::Synchronous) = 0;
    virtual void stopTrigger   (PixActions::SyncType sync=PixActions::Synchronous) = 0;
    virtual void stopEB        (PixActions::SyncType sync=PixActions::Synchronous) = 0;
    virtual void disconnect    (PixActions::SyncType sync=PixActions::Synchronous) = 0;
    virtual void unconfigure   (PixActions::SyncType sync=PixActions::Synchronous) = 0;    
    // Publishing method
    virtual void probe         (PixActions::SyncType sync=PixActions::Synchronous) = 0;
    //Reset & Config mods
    virtual void resetmods     (PixActions::SyncType sync=PixActions::Synchronous) = 0;
    virtual void listRODs      (PixActions::SyncType sync=PixActions::Synchronous) = 0;
    virtual void disableFailed (PixActions::SyncType sync=PixActions::Synchronous) = 0;
    // Basic hardware operations
    virtual void reset         (PixActions::SyncType sync=PixActions::Synchronous) = 0;
    virtual void resetViset    (PixActions::SyncType sync=PixActions::Synchronous) = 0;
    virtual void DCSsync       (PixActions::SyncType sync=PixActions::Synchronous) = 0;

    virtual void optoOnOff     (bool switchOn, PixActions::SyncType sync=PixActions::Synchronous) = 0;
    virtual void lvOnOff       (bool switchOn, PixActions::SyncType sync=PixActions::Synchronous) = 0;
    virtual void disable       (std::string objName, PixActions::SyncType sync=PixActions::Synchronous) = 0;
    virtual void enable        (std::string objName, PixActions::SyncType sync=PixActions::Synchronous) = 0;
    virtual void reset         (std::string objName, PixActions::SyncType sync=PixActions::Synchronous) = 0;
    virtual void warmReconfig  (std::string mode, std::string ecr, PixActions::SyncType sync=PixActions::Synchronous) = 0;
    virtual void recover  (std::string mode, std::string ecr, PixActions::SyncType sync=PixActions::Synchronous) = 0;
    
    // Configuration handling
    virtual void readConfig    (PixActions::SyncType sync=PixActions::Synchronous) = 0;
    virtual void loadConfig    (PixActions::SyncType sync=PixActions::Synchronous) = 0;
    virtual void sendConfig    (PixActions::SyncType sync=PixActions::Synchronous) = 0;
    virtual void setActionsConfig(PixActions::SyncType sync=PixActions::Synchronous) = 0;

    virtual void setPixScan    (std::string name,       PixActions::SyncType sync=PixActions::Asynchronous) = 0;
    virtual void setConnTest   (std::string name,       PixActions::SyncType sync=PixActions::Asynchronous) = 0;
    virtual void getRodActive  (std::string actionName, PixActions::SyncType sync=PixActions::Synchronous)=0;


    virtual void selectFE             (std::string actionName, long int modID, long int FE,     PixActions::SyncType sync=PixActions::Synchronous) = 0;
    virtual void configSingleMod      (std::string actionName, long int modID, long int maskFE, PixActions::SyncType sync=PixActions::Synchronous) = 0;
    virtual void setHalfclockMod      (PixActions::SyncType sync=PixActions::Synchronous) = 0;
    virtual void setModuleActive      (std::string actionName, long int modID, bool active,     PixActions::SyncType sync=PixActions::Synchronous) = 0;
    virtual void getModuleActive      (std::string actionName, long int modID, PixActions::SyncType sync=PixActions::Synchronous) = 0;
    //Scan
    virtual void scan                 (std::string scanName,   long int id,    PixActions::SyncType sync=PixActions::Synchronous)=0;
    virtual void presetScan           (std::string directory,  long int id,    long int val, int feflav, PixActions::SyncType sync=PixActions::Synchronous)=0; // T
    virtual void execConnectivityTest (std::string scanName,   long int id,    PixActions::SyncType sync=PixActions::Synchronous) = 0;

    virtual void setVcal        (float charge,   bool Chigh, PixActions::SyncType sync=PixActions::Synchronous) = 0; 
    virtual void incrMccDelay   (float in_delay, bool calib, PixActions::SyncType sync=PixActions::Synchronous) = 0;

    virtual void setRodActive(std::string actionName, bool active, PixActions::SyncType sync=PixActions::Synchronous)=0;
    virtual void setRodActive(bool active, PixActions::SyncType sync=PixActions::Synchronous)=0;
    // Scan
    virtual void abortScan(PixActions::SyncType ) {};
    virtual void abortConnTest(PixActions::SyncType ) {};

    // Rod Status
    virtual void rodStatus(bool& status);
    virtual std::map<std::string,bool> rodStatus(){return std::map<std::string,bool>();}
    
    virtual void saveConfig(std::string tag = "", PixActions::SyncType sync=PixActions::Synchronous) = 0;
    virtual void reloadConfig(std::string actionName, PixActions::SyncType sync=PixActions::Synchronous) = 0;

    //!Propagate ROD and TIM parameters to the PixActions private objects
    virtual void setRodBusyMask(long int mask, PixActions::SyncType sync=PixActions::Synchronous) = 0;

    // Test
    virtual void test1      (std::string test, PixActions::SyncType sync=PixActions::Synchronous) = 0;
    virtual void test2      (std::string test, PixActions::SyncType sync=PixActions::Synchronous) = 0;

    // Resource allocation. 
    virtual void allocate      (const char* allocatingBrokerName);
    virtual void deallocate    (const char* deallocatingBrokerName);
    

    virtual std::string getConnTagC  (const char* subActionName) = 0;
    virtual std::string getConnTagP  (const char* subActionName) = 0;
    virtual std::string getConnTagA  (const char* subActionName) = 0;
    virtual std::string getConnTagOI (const char* subActionName) = 0;
    virtual std::string getCfgTag    (const char* subActionName) = 0;
    virtual std::string getCfgTagOI  (const char* subActionName) = 0;
    virtual std::string getModCfgTag (const char* subActionName) = 0;

    virtual const bool isHVOff() =0;

    virtual void reloadTags(std::string newCfgTag, std::string newCfgModTag, 
			    long int newCfgRev, long int newCfgModRev, PixActions::SyncType sync=PixActions::Synchronous) = 0;

    // Descriptor
    virtual struct PixActionsDescriptor descriptor() {return m_descriptor;}

    //virtual std::string name() {return m_descriptor.name;}
    virtual std::string managingBrokerName() {return m_descriptor.managingBrokerName;}
    virtual bool available() {return m_descriptor.available;}
    virtual Type type() {return m_descriptor.type;}
    virtual std::string allocatingBrokerName() {return m_descriptor.allocatingBrokerName;}

    virtual std::vector<std::string> listSubActions(PixActions::Type type);
    
  protected:

    //Action monitoring
    virtual void publishDescriptor();
    virtual void publishAction(std::string action);
    virtual void publishStatus(std::string status);
    virtual void removeISVariables();

    // Generic implementation methods
    void implement     (std::string method, PixActions::SyncType sync, const ipc::ActionInput input);
    void implementInOut(std::string method, ipc::ActionOutput& output);
    // IPC shutdown
    virtual void shutdown() {}
    virtual void deallocate();

    std::string stringFromActionInput   (const ipc::ActionInput actionInput, int pos);

    void boolActionOutput(std::map<std::string,bool> resultMap, ipc::ActionOutput& output);
    //inout methods with ipc structs
    virtual void rodStatus(ipc::ActionOutput& output);

    //char* getIpcString(std::string str);
    
  protected:

    // SW interfaces
    IPCPartition *m_ipcPartition;
    PixISManager *m_is;
    
    // Callback variables
    ipc::PixActionsCallback_var	m_cb;
    short m_cbNumber;

    // Actions descriptor
    struct PixActionsDescriptor m_descriptor;

    // Mutex
    PixMutex *m_mutex;

  };

  
}

#endif
