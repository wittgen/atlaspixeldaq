/////////////////////////////////////////////////////////////////////
// PixActionsSingleIBL.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 31/08/14  Version 1.0  (PM)
//           Initial release
//

//! Class for the Pixel single IBL ROD actions

#ifndef _PIXLIB_ACTIONSSINGLEIBL
#define _PIXLIB_ACTIONSSINGLEIBL

#include "PixActions/PixActions.h"
#include "PixController/PixRunConfig.h"
#include "PixThreads/PixAutoDisTask.h"
#include "PixDcs/PixDcs.h"
#include <time.h>

class IPCPartition;

namespace PixLib {
  
  // Forward declarations
  class PixDbInterface;
  class PixModuleGroup;
  class PixController;
  class PixConnectivity;
  class dbRecordIterator;
  class DbRecord;
//   class PixThreadsScan; //  No longer used.
  class PixThread;
  class PixScanTask;
  class IblCppMonitorTask;
  class PixRunConfig;
  class PixDbServerInterface;
  class PixHistoServerInterface;
  class PixISManager;

  class PixActionsSingleIBL : public PixActions {

  public:
    
    class PublishHelper {
    public:
      PublishHelper(PixActionsSingleIBL &action) : m_action(&action), m_status(false) {}

      ~PublishHelper() {
	if (!m_status) m_action->publishStatus("SUCCESS");
	else m_action->publishStatus("FAILURE");
      }
      
      void setSuccess() { m_status=true; }
    private:
      PixActionsSingleIBL *m_action;
      bool m_status;
    };
    
    // Constructor
    PixActionsSingleIBL(std::string actionName, std::string managingBrokerName, std::string allocatingBrokerName,
			IPCPartition *ipcPartition,
			RodBocConnectivity *rod, PixConnectivity *conn);

    PixActionsSingleIBL(std::string actionName, std::string managingBrokerName, 
			PixDbServerInterface *DbServer, PixHistoServerInterface *hInt, 
			std::string allocatingBrokerName, IPCPartition *ipcPartition, 
			RodBocConnectivity *rod, PixConnectivity *conn);


    ~PixActionsSingleIBL();

    // Delete PixModuleGroup then recreate it with new connectivity
    virtual void setNewCfg(PixConnectivity *newConn, RodBocConnectivity *newRodBocConn);

    // Rod Status
    virtual void rodStatus(bool& status);
    virtual std::map<std::string,bool> rodStatus();

    // State transitions
    virtual void initRun       (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void initCal       (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void resetRod      (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void configureBoc  (PixActions::SyncType sync=PixActions::Synchronous);

    virtual void setup         (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void configure     (std::string partName, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void connect       (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void prepareForRun (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void startTrigger  (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void stopTrigger   (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void stopEB        (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void disconnect    (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void unconfigure   (PixActions::SyncType sync=PixActions::Synchronous);
    // Publishing method
    virtual void probe         (PixActions::SyncType sync=PixActions::Synchronous);
    // Reset & Config mod
    virtual void resetmods     (PixActions::SyncType sync=PixActions::Synchronous); 
    virtual void listRODs      (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void disableFailed (PixActions::SyncType sync=PixActions::Synchronous);
    // Basic hardware operations
    virtual void reset         (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void resetViset    (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void DCSsync       (PixActions::SyncType sync=PixActions::Synchronous);

    virtual void optoOnOff     (bool switchOn, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void lvOnOff       (bool switchOn, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void disable       (std::string objName, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void enable        (std::string objName, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void reset         (std::string objName, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void warmReconfig  (std::string mode, std::string ecr, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void recover (std::string mode, std::string ecr, PixActions::SyncType sync=PixActions::Synchronous);

    // Configuration handling
    virtual void readConfig    (PixActions::SyncType ){ }
    virtual void loadConfig    (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void sendConfig    (PixActions::SyncType sync=PixActions::Synchronous);
    virtual void setActionsConfig(PixActions::SyncType sync=PixActions::Synchronous);

    virtual void setupDb(PixActions::SyncType sync=PixActions::Synchronous);
    
    // Abort
    virtual void abortScan(PixActions::SyncType sync=PixActions::Synchronous);
    virtual void abortConnTest(PixActions::SyncType sync=PixActions::Synchronous);

    // Scan
    virtual void scan(std::string scanName, long int id, PixActions::SyncType sync=PixActions::Synchronous); // To be called after setPixScan, use it to run a non preset scan
    virtual void presetScan(std::string directory, long int id, long int presetType, int feflav, PixActions::SyncType sync=PixActions::Synchronous); //To be called directly to start a preset scan
   
    //!Configure HW
    virtual void setRodBusyMask(long int , PixActions::SyncType ) {};

    virtual void saveConfig(std::string tag, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void reloadConfig(std::string actionName, PixActions::SyncType sync=PixActions::Synchronous);

    virtual void setVcal(float charge, bool Chigh, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void setPixScan(std::string decName, PixActions::SyncType sync=PixActions::Asynchronous);
    virtual void setConnTest(std::string decName, PixActions::SyncType sync=PixActions::Asynchronous) {};
    virtual void execConnectivityTest(std::string connTestName, long int id, PixActions::SyncType sync=PixActions::Synchronous) {};
    virtual void incrMccDelay(float in_delay, bool calib, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void selectFE(std::string actionName, long int modID, long int FE, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void configSingleMod(std::string actionName, long int modID, long int maskFE, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void setHalfclockMod(PixActions::SyncType sync=PixActions::Synchronous);
    virtual void setModuleActive(std::string actionName, long int modID, bool active, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void getModuleActive(std::string actionName, long int modID, PixActions::SyncType sync=PixActions::Synchronous);

    // Test
    virtual void test1     (std::string test, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void test2     (std::string test, PixActions::SyncType sync=PixActions::Synchronous);

    virtual void getRodActive(std::string actionName, PixActions::SyncType sync=PixActions::Synchronous); 
    virtual void setRodActive(std::string actionName, bool active, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void setRodActive(bool active, PixActions::SyncType sync=PixActions::Synchronous) {};

    virtual std::string getConnTagC  (const char* subActionName);
    virtual std::string getConnTagP  (const char* subActionName);
    virtual std::string getConnTagA  (const char* subActionName);
    virtual std::string getConnTagOI (const char* subActionName);
    virtual std::string getCfgTag    (const char* subActionName);
    virtual std::string getCfgTagOI  (const char* subActionName);
    virtual std::string getModCfgTag (const char* subActionName);

    //HV status
    virtual const bool isHVOff( );
    const bool getHVOff();

    virtual void allocate(const char* allocatingBrokerName);
    virtual void deallocate(const char* deallocatingBrokerName);

    virtual void reloadTags(std::string newCfgTag, std::string newCfgModTag, 
			    long int newCfgRev, long int newCfgModRev, PixActions::SyncType sync=PixActions::Synchronous);

    //Action monitoring
    virtual void publishDescriptor();
    virtual void publishAction(std::string action);
    virtual void publishStatus(std::string status);
    virtual void removeISVariables();
    virtual void setupRodMonitoring(unsigned int bufRate=12, unsigned int statusRate=3, unsigned int busyRate=1,
                                    unsigned int calRate=0, unsigned int occupRate=2, unsigned int monPrescale=1,
				    unsigned int quickStatusMode=1, unsigned int quickStatusConfig=0, 
				    unsigned int MODsyncConfig=0, unsigned int RODsyncConfig=0);
    virtual void unSetupRodMonitoring();
    void setupPixAutoDis();
    void unSetupPixAutoDis();

  protected:

    void retrySetupDb(std::string methodName);
    void printFailSetupDb(std::string methodName);
    std::string getRodName();
    
    // PixLib objects
    PixModuleGroup *m_moduleGroup;
    PixThread *m_thread;

    PixScan *m_pixScan;
    PixScanTask *m_pixScanTask;
    int m_pixScanNumber;
    std::string m_pixScanName;

    PixController *m_rod;
    PixConnectivity *m_conn;
    RodBocConnectivity *m_rodConn;
    PixRunConfig* m_run;    
    std::string m_runName;
    PixDbServerInterface *m_dbServer;
    PixHistoServerInterface *m_hInt;
    std::string m_isRoot;
    std::string m_domainName;    //! DbServer domain name
    unsigned int m_rodRev;       //! Revision for the actual rod configuration
    unsigned int m_runRev;       //! Revision for the actual run configuration
    std::map<PixModule*, unsigned int> m_modsRev; // Revision for the actual modules configuration
    std::string m_cfgTag;        //! Configuration tag
    std::string m_cfgModTag;     //! ModuleConfiguration tag

    std::string m_applName; 
    std::string m_info;

    IblCppMonitorTask* m_rodMonitor; 
    PixAutoDisTask *m_autoDisTask;

    PixDcs *m_dcs;
    bool m_isSetup;
    bool m_preAmpKilled;
    bool m_configured;
    bool m_runStarted;
    std::string m_runPartName;
    unsigned int m_actionStatus;
    int m_feFlav;  // T: redefine it here;      ???

    PixISManager *m_isPixelRunParams; 
    PixISManager *m_isPixelDDC; 
    PixISManager *m_isDTRunParams; 
    IPCPartition *m_ipcDTPartition;

    time_t m_actStart;
    time_t m_cmdStart;
    time_t m_cmdStop;
  };

}

#endif // _PIXLIB_ACTIONSSINGLEIBL

