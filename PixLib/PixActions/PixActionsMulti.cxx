/////////////////////////////////////////////////////////////////////
// PixActionsMulti.cxx
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 03/04/06  Version 1.0 (CS)
//           Initial release - Single threaded version
//

//! Class for the Pixel multiple actions

#include <set>

#include "PixActions/PixActionsCallback.h"
#include "PixActions/PixActionsMulti.h"
#include "PixActionsIDL.hh"
#include "PixUtilities/PixMessages.h"

using namespace PixLib;



//! Constructor
PixActionsMulti::PixActionsMulti(std::string actionName, std::string managingBrokerName, std::string allocatingBrokerName,
				 std::set<std::string> subActions,
				 IPCPartition *ipcPartition) :
  PixActions(ipcPartition,actionName.c_str()) {
  PixLock lock(m_mutex);
  // Set action attributes
  m_descriptor.name = actionName;
  if(allocatingBrokerName == "") m_descriptor.available = true;
  else m_descriptor.available = false;
  m_descriptor.type = MULTI;
  m_descriptor.allocatingBrokerName = allocatingBrokerName;
  m_descriptor.managingBrokerName = managingBrokerName;

  
  ers::log(PixLib::pix::daq (ERS_HERE, m_applName, "Multi created"));

  // Get subactions from IPC
  
  std::set<std::string>::iterator subAction, subActionEnd=subActions.end();
  for(subAction=subActions.begin(); subAction!=subActionEnd; subAction++) {
    try {
      ipc::PixActions_var action = ipcPartition->lookup<ipc::PixActions,ipc::no_cache,ipc::narrow>(*subAction);

      m_subActions.insert(std::make_pair(*subAction, action));
      
      std::vector<std::string> actionsList;
      ipc::stringList* subActionsNames = action->ipc_listSubActions(ipc::SINGLE_ROD);
      for(int i=0; i<(int)subActionsNames->length(); i++){
	std::string temp = std::string((*subActionsNames)[i]._retn());
	actionsList.push_back(temp);
      }
      
      ipc::stringList* subActionsTIM = action->ipc_listSubActions(ipc::SINGLE_TIM);
      
      for(int i=0; i<(int)subActionsTIM->length(); i++){
	std::string temp = std::string((*subActionsTIM)[i]._retn());
	actionsList.push_back(temp);
      }
      m_singleSubActions.insert(std::make_pair(*subAction, actionsList));
      
      delete subActionsNames;
      delete subActionsTIM;
      
    } catch (const daq::ipc::InvalidPartition&) {
      //throw ??
      m_info = "IPC server is unavailable ";
      ers::fatal(PixLib::pix::daq (ERS_HERE, m_applName, m_info));
    } catch (const daq::ipc::InvalidObjectName&) {
      //throw ??
      m_info = "The name of the subAction is null ";
      ers::fatal(PixLib::pix::daq (ERS_HERE, m_applName, m_info));
    } catch (const daq::ipc::ObjectNotFound&) {
    //throw ??
      m_info = "The subAction does not exist or is not registered with the partition server "; 
      ers::fatal(PixLib::pix::daq (ERS_HERE, m_applName, m_info));
    } catch(...) {
      m_info = "Caught unknown exception in PixActionsMulti constructor";
      ers::fatal(PixLib::pix::daq (ERS_HERE, m_applName, m_info));
    }
  }
  
  initEnable(ipcPartition);
  
}


void PixActionsMulti::initEnable(IPCPartition *ipcPartition) 
{
  PixLock lock(m_mutex);

  std::map<std::string, ipc::PixActions_var>::iterator subAction;
  std::string action;
  bool en=true;

  for(subAction=m_subActions.begin(); subAction!=m_subActions.end(); subAction++) { // loop on map of sub-actions
    action=((*subAction).first); // get the sub-action name
    m_rodEnable.insert(std::make_pair(action,en)); // fill your map with all enable
  }
//  std::cout << "I have initialized the enable/disable internale ROD map"<< std::endl;
}


PixActionsMulti::~PixActionsMulti()
{
  PixLock lock(m_mutex);
  m_subActions.clear();
}

void PixActionsMulti::initRun(PixActions::SyncType sync) 
{
  callIPCMethod("initRun",sync,ipc::ActionInput());
}

void PixActionsMulti::initCal(PixActions::SyncType sync) 
{
  callIPCMethod("initCal", sync,ipc::ActionInput() );
}

void PixActionsMulti::resetRod(PixActions::SyncType sync) 
{
  callIPCMethod("resetRod", sync,ipc::ActionInput() );
}

void PixActionsMulti::configureBoc(PixActions::SyncType sync) 
{
  callIPCMethod("configureBoc", sync,ipc::ActionInput() );
}

void PixActionsMulti::setup(PixActions::SyncType sync) 
{
  callIPCMethod("setup",sync,ipc::ActionInput());
}

void PixActionsMulti::configure(std::string partName, PixActions::SyncType sync) 
{
  callIPCMethod("configure",sync, stringActionInput(partName));
}

void PixActionsMulti::connect(PixActions::SyncType sync) 
{
  callIPCMethod("connect",sync,ipc::ActionInput());
}

void PixActionsMulti::prepareForRun(PixActions::SyncType sync) 
{
  callIPCMethod("prepareForRun",sync,ipc::ActionInput());
}

void PixActionsMulti::startTrigger(PixActions::SyncType sync) 
{
  callIPCMethod("startTrigger",sync,ipc::ActionInput());
}

void PixActionsMulti::stopTrigger(PixActions::SyncType sync) 
{
  callIPCMethod("stopTrigger",sync,ipc::ActionInput());
}

void PixActionsMulti::stopEB(PixActions::SyncType sync) 
{
  callIPCMethod("stopEB",sync,ipc::ActionInput());
}

void PixActionsMulti::disconnect(PixActions::SyncType sync) 
{
  callIPCMethod("disconnect",sync,ipc::ActionInput());
}

void PixActionsMulti::unconfigure(PixActions::SyncType sync) 
{
  callIPCMethod("unconfigure",sync,ipc::ActionInput());
}

//! Monitoring in IS

void PixActionsMulti::probe(PixActions::SyncType sync) 
{
  callIPCMethod("probe",sync,ipc::ActionInput());
}

//! Configuration handling

void PixActionsMulti::readConfig(PixActions::SyncType sync) 
{
  callIPCMethod("readConfig",sync,ipc::ActionInput());
}

void PixActionsMulti::loadConfig(PixActions::SyncType sync) 
{
  callIPCMethod("loadConfig",sync,ipc::ActionInput());
}

void PixActionsMulti::sendConfig(PixActions::SyncType sync) 
{
  callIPCMethod("sendConfig",sync,ipc::ActionInput());
}

void PixActionsMulti::setActionsConfig(PixActions::SyncType sync) 
{
  callIPCMethod("setActionsConfig",sync,ipc::ActionInput());
}


//! Basic operations

void PixActionsMulti::reset(PixActions::SyncType sync) 
{
  callIPCMethod("reset",sync,ipc::ActionInput());
}

void PixActionsMulti::resetViset(PixActions::SyncType sync) 
{
  callIPCMethod("resetViset",sync,ipc::ActionInput());
}

void PixActionsMulti::DCSsync(PixActions::SyncType sync) 
{
  callIPCMethod("DCSsync",sync,ipc::ActionInput());
}

void PixActionsMulti::optoOnOff(bool switchOn, PixActions::SyncType sync) 
{
  callIPCMethod("optoOnOff", sync, boolActionInput(switchOn));
}

void PixActionsMulti::lvOnOff(bool switchOn, PixActions::SyncType sync) 
{
  callIPCMethod("lvOnOff", sync, boolActionInput(switchOn));
}

void PixActionsMulti::disable(std::string objName, PixActions::SyncType sync) 
{
  callIPCMethod("disable", sync, stringActionInput(objName));
}

void PixActionsMulti::enable(std::string objName, PixActions::SyncType sync) 
{
  callIPCMethod("enable", sync, stringActionInput(objName));
}

void PixActionsMulti::reset(std::string objName, PixActions::SyncType sync) 
{
  callIPCMethod("reset", sync, stringActionInput(objName));
}

void PixActionsMulti::warmReconfig(std::string mode, std::string ecr, PixActions::SyncType sync) 
{
  callIPCMethod("warmReconfig", sync, stringStringActionInput(mode,ecr));
}

void PixActionsMulti::recover(std::string mode, std::string ecr, PixActions::SyncType sync) 
{
  callIPCMethod("recover", sync, stringStringActionInput(mode,ecr));
}

void PixActionsMulti::resetmods(PixActions::SyncType sync) 
{
  callIPCMethod("resetmods",sync,ipc::ActionInput());
}

void PixActionsMulti::listRODs(PixActions::SyncType sync) 
{
  callIPCMethod("listRODs",sync,ipc::ActionInput());
}

void PixActionsMulti::disableFailed(PixActions::SyncType sync) 
{
  callIPCMethod("disableFailed", sync, ipc::ActionInput());
}

// Scan
void PixActionsMulti::abortScan(PixActions::SyncType sync) 
{
  callIPCMethod("abortScan", sync, ipc::ActionInput());
}

void PixActionsMulti::abortConnTest(PixActions::SyncType sync) 
{
  callIPCMethod("abortConnTest", sync, ipc::ActionInput());
} 


void PixActionsMulti::setRodBusyMask(long int mask, PixActions::SyncType sync) 
{
  callIPCMethod("setRodBusyMask", sync, intActionInput(mask));
}



void PixActionsMulti::saveConfig(std::string tag, PixActions::SyncType sync) 
{
  callIPCMethod("saveConfig", sync, stringActionInput(tag));
}

void PixActionsMulti::reloadConfig(std::string actionName, PixActions::SyncType sync) 
{
  callIPCMethod("reloadConfig",sync,stringActionInput(actionName));
}

void PixActionsMulti::setPixScan(std::string name, PixActions::SyncType sync) 
{
  callIPCMethod("setPixScan",sync,stringActionInput(name));  

}

void PixActionsMulti::setConnTest(std::string name, PixActions::SyncType sync) 
{
  callIPCMethod("setConnTest",sync,stringActionInput(name));  
}

void PixActionsMulti::getModuleActive(std::string actionName, long int modID, PixActions::SyncType sync) 
{
  callIPCMethod("getModuleActive",sync,stringLongActionInput(actionName,modID));  
}

void PixActionsMulti::scan(std::string name, long int id, PixActions::SyncType sync) 
{
  callIPCMethod("scan",sync,stringLongActionInput(name,id));  
}

void PixActionsMulti::execConnectivityTest(std::string connTestName, long int id, PixActions::SyncType sync) 
{
  callIPCMethod("execConnectivityTest",sync,stringLongActionInput(connTestName,id));  
}

void PixActionsMulti::setVcal(float charge, bool Chigh, PixActions::SyncType sync) 
{
  callIPCMethod("setVcal",sync,floatBoolActionInput(charge,Chigh));  
}

void PixActionsMulti::incrMccDelay(float in_delay, bool calib, PixActions::SyncType sync) 
{
  callIPCMethod("incrMccDelay",sync,floatBoolActionInput(in_delay,calib));  
}

void PixActionsMulti::selectFE(std::string actionName, long int modID, long int FE, PixActions::SyncType sync) 
{
  callIPCMethod("selectFE",sync,stringLongLongActionInput(actionName,modID, FE));  
}

void PixActionsMulti::configSingleMod(std::string actionName, long int modID, long int maskFE, PixActions::SyncType sync) 
{
  callIPCMethod("configSingleMod",sync,stringLongLongActionInput(actionName,modID, maskFE));  
}

void PixActionsMulti::setHalfclockMod(PixActions::SyncType sync) 
{
  callIPCMethod("setHalfclockMod",sync,ipc::ActionInput());
}

void PixActionsMulti::setModuleActive(std::string actionName, long int modID, bool active, PixActions::SyncType sync) 
{
  callIPCMethod("setModuleActive",sync,stringLongBoolActionInput(actionName,modID,active),false);  
}

void PixActionsMulti::presetScan(std::string directory, long int id, long int val, int feflav, PixActions::SyncType sync) // T
{
  callIPCMethod("presetScan",sync,stringLongLongIntActionInput(directory,id,val,feflav));  // T
}

void PixActionsMulti::reloadTags(std::string newCfgTag, std::string newCfgModTag, 
				 long int cfgRev, long int cfgModRev, PixActions::SyncType sync)
{
  callIPCMethod("reloadTags",sync,stringStringLongLongActionInput(newCfgTag, newCfgModTag, cfgRev, cfgModRev));  
}

void PixActionsMulti::test1(std::string test, PixActions::SyncType sync) 
{
  callIPCMethod("test1",sync, stringActionInput(test));
}

void PixActionsMulti::test2(std::string test, PixActions::SyncType sync) 
{
  callIPCMethod("test2",sync, stringActionInput(test));
}




void PixActionsMulti::rodStatus(bool& status) 
{
  PixLock lock(m_mutex);
  std::map<std::string,bool> result = rodStatus();
  status = false;
  std::map<std::string,bool>::iterator resultIt, resultItEnd = result.end();
  for(resultIt=result.begin(); resultIt != resultItEnd; resultIt++)
    status |= resultIt->second;
}

std::map<std::string,bool> PixActionsMulti::rodStatus() 
{
  PixLock lock(m_mutex);
  std::map<std::string,bool> result;
  ipc::ActionOutput actionOutput;
  actionOutput.m_boolsOut.length(0);
  actionOutput.m_actions.length(0);
  //  actionOutput.m_boolsOut = ipc::boolList(0);
  // actionOutput.m_actions  = ipc::stringList(0);
  callIPCInOutMethod("rodStatus", actionOutput);
  //fill the map
   for(int i = 0; i<(int)actionOutput.m_boolsOut.length(); i++)
    result.insert(make_pair(std::string(actionOutput.m_actions[i]),actionOutput.m_boolsOut[i]));
  return result;
}

// Recursive allocation/deallocation of subactions
void PixActionsMulti::allocate(const char* allocatingBrokerName) 
{
  PixLock lock(m_mutex);
  if (m_descriptor.available) {
    PixActions::allocate(allocatingBrokerName);
    callIPCMethod("allocate", PixActions::Asynchronous, stringActionInput(std::string(allocatingBrokerName)),false);
  } else {
//     m_info = "PixActionsMulti::Allocate - object "<< m_descriptor.type << "already allocated";
//     m_msg->publishMessage(PixMessages::WARNING, m_applName, m_info);
  }
}

void PixActionsMulti::deallocate(const char* deallocatingBrokerName) 
{
  PixLock lock(m_mutex);
  if (!m_descriptor.available) {
    PixActions::deallocate(deallocatingBrokerName);
    callIPCMethod("deallocate", PixActions::Asynchronous, stringActionInput(std::string(deallocatingBrokerName)),false);
  } else {
    //    m_info = "PixActionsMulti::Allocate - object "<< m_descriptor.type << "already allocated";
    //m_msg->publishMessage(PixMessages::WARNING, m_applName, m_info);    
  }
}

void PixActionsMulti::setRodActive(std::string actionName, bool enable, PixActions::SyncType sync) 
{
  PixLock lock(m_mutex);
  std::map<std::string, ipc::PixActions_var>::iterator subAction;
  std::string name;
  for(subAction=m_subActions.begin(); subAction!=m_subActions.end(); subAction++) {
    name=subAction->first;
    if(belongsToAction(name, actionName)) {
      if (name==actionName) {
	m_rodEnable[name]=enable;
      }
      callIPCMethod("setRodActive", sync, stringBoolActionInput(actionName, enable),false);
    }
  }
}



void PixActionsMulti::setRodActive(bool enable, PixActions::SyncType sync) 
{
  PixLock lock(m_mutex);
  std::map<std::string, ipc::PixActions_var>::iterator subAction;
  std::string name;
  for(subAction=m_subActions.begin(); subAction!=m_subActions.end(); subAction++) {
    m_rodEnable[subAction->first]=enable;
    callIPCMethod("setRodActive", sync, stringBoolActionInput(subAction->first, enable), false);
  }
}





void PixActionsMulti::getRodActive(std::string actionName, PixActions::SyncType sync) 
{
  PixLock lock(m_mutex);
  std::map<std::string, ipc::PixActions_var>::iterator subAction;
  std::string name;
  for(subAction=m_subActions.begin(); subAction!=m_subActions.end(); subAction++) {
    name=subAction->first;
    callIPCMethod("getRodActive", sync, stringActionInput(actionName));
  }
}


void PixActionsMulti::setRodEnable(std::string actionName, bool active) 
{
  PixLock lock(m_mutex);
  std::map<std::string, ipc::PixActions_var>::iterator subAction;
  //std::vector<std::string>::iterator it;
  for(subAction=m_subActions.begin(); subAction!=m_subActions.end(); subAction++) {
    if (subAction->first == actionName) {
      m_rodEnable[subAction->first] = active;
      break;  
    }
  }
}


std::string PixActionsMulti::getConnTagC(const char* subActionName)
{
  PixLock lock(m_mutex);
  return getTag(subActionName, PixActionsMulti::connTagC);
}

std::string PixActionsMulti::getConnTagP(const char* subActionName)
{
  PixLock lock(m_mutex);
  return getTag(subActionName, PixActionsMulti::connTagP);
}

std::string PixActionsMulti::getConnTagA(const char* subActionName)
{
  PixLock lock(m_mutex);
  return getTag(subActionName, PixActionsMulti::connTagA);
}

std::string PixActionsMulti::getConnTagOI(const char* subActionName)
{
  PixLock lock(m_mutex);
  return getTag(subActionName, PixActionsMulti::connTagOI);
}

std::string PixActionsMulti::getCfgTag(const char* subActionName)
{
  PixLock lock(m_mutex);
  return getTag(subActionName, PixActionsMulti::confTag);
}

std::string PixActionsMulti::getModCfgTag(const char* subActionName)
{
  PixLock lock(m_mutex);
  return getTag(subActionName, PixActionsMulti::modConfTag);
}

std::string PixActionsMulti::getCfgTagOI(const char* subActionName)
{
  PixLock lock(m_mutex);
  return getTag(subActionName, PixActionsMulti::confTagOI);
}

const bool PixActionsMulti::isHVOff()
{
  PixLock lock(m_mutex);

  for(auto & [name, subaction] : m_subActions )
   if(subaction->ipc_isHVOff())return true;

  return false;
}

std::string PixActionsMulti::getTag(const char* subActionName, PixActionsMulti::TagType type)
{
  PixLock lock(m_mutex);
  std::map<std::string, ipc::PixActions_var>::iterator subAction, subActionEnd=m_subActions.end();
  char* conntag = 0;
  for(subAction=m_subActions.begin(); subAction!=subActionEnd; subAction++) {
    if     (type == PixActionsMulti::confTag)   conntag = subAction->second->ipc_getCfgTag   (subActionName);
    else if(type == PixActionsMulti::connTagOI) conntag = subAction->second->ipc_getConnTagOI(subActionName);
    else if(type == PixActionsMulti::connTagP)  conntag = subAction->second->ipc_getConnTagP (subActionName);
    else if(type == PixActionsMulti::connTagA)  conntag = subAction->second->ipc_getConnTagA (subActionName);
    else if(type == PixActionsMulti::connTagC)  conntag = subAction->second->ipc_getConnTagC (subActionName);
    else if(type == PixActionsMulti::modConfTag)conntag = subAction->second->ipc_getModCfgTag(subActionName);
    else if(type == PixActionsMulti::confTagOI) conntag = subAction->second->ipc_getCfgTagOI (subActionName);
    
    if(conntag != 0){
      std::string result = std::string(conntag);
      return result;
    }
  }
  return std::string("");
}


void PixActionsMulti::callIPCMethod(std::string methodName, PixActionsMulti::SyncType sync, ipc::ActionInput input, bool checkRodEnable)
{
  PixLock lock(m_mutex);
  bool oneEnabled = false;
  // Create and assign callback
  PixActionsCallback *cb = 0;
  std::map<std::string, ipc::PixActions_var>::iterator subAction, subActionEnd=m_subActions.end();
  if(sync == PixActions::Synchronous){
    cb = new PixActionsCallback;
    for(subAction=m_subActions.begin(); subAction!=subActionEnd; subAction++) {
      if (!checkRodEnable || m_rodEnable[subAction->first]) {
	oneEnabled = true;
	(subAction->second)->ipc_setCallback(cb->_this());
      }
    }
  }
  
  // Call method for subaction
  for(subAction=m_subActions.begin(); subAction!=subActionEnd; subAction++) {
    if(sync == PixActions::Asynchronous && (!checkRodEnable || m_rodEnable[subAction->first])){
      cb = new PixActionsCallback;
      (subAction->second)->ipc_setCallback(cb->_this());
    }
    if (!checkRodEnable || m_rodEnable[subAction->first]) {
      m_info = "Calling " + methodName + " for " + subAction->first;
      ers::info(PixLib::pix::daq (ERS_HERE, m_applName, m_info));
      if     (methodName == "setup")                (subAction->second)->ipc_setup                ((bool) sync);
      else if(methodName == "abortScan")            (subAction->second)->ipc_abortScan            ((bool) sync);
      else if(methodName == "abortConnTest")        (subAction->second)->ipc_abortConnTest        ((bool) sync);
      else if(methodName == "setActionsConfig")     (subAction->second)->ipc_setActionsConfig     ((bool) sync);
      else if(methodName == "reset")                (subAction->second)->ipc_reset                ((bool) sync);
      else if(methodName == "resetViset")           (subAction->second)->ipc_resetViset           ((bool) sync);
      else if(methodName == "DCSsync")              (subAction->second)->ipc_DCSsync              ((bool) sync);
      else if(methodName == "readConfig")           (subAction->second)->ipc_readConfig           ((bool) sync);
      else if(methodName == "loadConfig")           (subAction->second)->ipc_loadConfig           ((bool) sync);
      else if(methodName == "sendConfig")           (subAction->second)->ipc_sendConfig           ((bool) sync);
      else if(methodName == "initRun")              (subAction->second)->ipc_initRun              ((bool) sync);
      else if(methodName == "initCal")              (subAction->second)->ipc_initCal              ((bool) sync);
      else if(methodName == "resetRod")             (subAction->second)->ipc_resetRod             ((bool) sync);
      else if(methodName == "configureBoc")         (subAction->second)->ipc_configureBoc         ((bool) sync);
      else if(methodName == "configure")            (subAction->second)->ipc_configure            ((bool) sync, input);
      else if(methodName == "connect")              (subAction->second)->ipc_connect              ((bool) sync);
      else if(methodName == "prepareForRun")        (subAction->second)->ipc_prepareForRun        ((bool) sync);
      else if(methodName == "startTrigger")         (subAction->second)->ipc_startTrigger         ((bool) sync);
      else if(methodName == "stopTrigger")          (subAction->second)->ipc_stopTrigger          ((bool) sync);
      else if(methodName == "stopEB")               (subAction->second)->ipc_stopEB               ((bool) sync);
      else if(methodName == "disconnect")           (subAction->second)->ipc_disconnect           ((bool) sync);
      else if(methodName == "unconfigure")          (subAction->second)->ipc_unconfigure          ((bool) sync);  
      else if(methodName == "probe")                (subAction->second)->ipc_probe                ((bool) sync);
      else if(methodName == "resetmods")            (subAction->second)->ipc_resetmods            ((bool) sync);
      else if(methodName == "listRODs")             (subAction->second)->ipc_listRODs             ((bool) sync);
      else if(methodName == "disableFailed")        (subAction->second)->ipc_disableFailed        ((bool) sync);
      else if(methodName == "setPixScan")           (subAction->second)->ipc_setPixScan           ((bool) sync, input);
      else if(methodName == "execConnectivityTest") (subAction->second)->ipc_execConnectivityTest ((bool) sync, input);
      else if(methodName == "setRodBusyMask")       (subAction->second)->ipc_setRodBusyMask       ((bool) sync, input);
      else if(methodName == "presetScan")           (subAction->second)->ipc_presetScan           ((bool) sync, input); 
      else if(methodName == "scan")                 (subAction->second)->ipc_scan                 ((bool) sync, input); 
      else if(methodName == "setRodActive")         (subAction->second)->ipc_setRodActive         ((bool) sync, input);
      else if(methodName == "getRodActive")         (subAction->second)->ipc_getRodActive         ((bool) sync, input);
      else if(methodName == "incrMccDelay")         (subAction->second)->ipc_incrMccDelay         ((bool) sync, input);
      else if(methodName == "selectFE")             (subAction->second)->ipc_selectFE             ((bool) sync, input);
      else if(methodName == "configSingleMod")      (subAction->second)->ipc_configSingleMod      ((bool) sync, input);
      else if(methodName == "setHalfclockMod")      (subAction->second)->ipc_setHalfclockMod      ((bool) sync);
      else if(methodName == "setConnTest")          (subAction->second)->ipc_setConnTest          ((bool) sync, input);
      else if(methodName == "setVcal")              (subAction->second)->ipc_setVcal              ((bool) sync, input);
      else if(methodName == "saveConfig")           (subAction->second)->ipc_saveConfig           ((bool) sync, input);
      else if(methodName == "reloadConfig")         (subAction->second)->ipc_reloadConfig         ((bool) sync, input);
      else if(methodName == "setModuleActive")      (subAction->second)->ipc_setModuleActive      ((bool) sync, input);
      else if(methodName == "reloadTags")           (subAction->second)->ipc_reloadTags           ((bool) sync, input); 
      else if(methodName == "optoOnOff")            (subAction->second)->ipc_optoOnOff            ((bool) sync, input); 
      else if(methodName == "lvOnOff")              (subAction->second)->ipc_lvOnOff              ((bool) sync, input); 
      else if(methodName == "disable")              (subAction->second)->ipc_disable              ((bool) sync, input); 
      else if(methodName == "enable")               (subAction->second)->ipc_enable               ((bool) sync, input); 
      else if(methodName == "reset")                (subAction->second)->ipc_dtreset              ((bool) sync, input); 
      else if(methodName == "warmReconfig")         (subAction->second)->ipc_warmReconfig         ((bool) sync, input);
      else if(methodName == "recover")              (subAction->second)->ipc_recover         ((bool) sync, input);
      else if(methodName == "test1")                (subAction->second)->ipc_test1                ((bool) sync, input);
      else if(methodName == "test2")                (subAction->second)->ipc_test2                ((bool) sync, input);
      else if(methodName == "allocate")             (subAction->second)->ipc_allocate             (input);
      else if(methodName == "deallocate")           (subAction->second)->ipc_deallocate           (input);

    }
    if (sync == PixActions::Asynchronous && (!checkRodEnable || m_rodEnable[subAction->first])){
      // Wait for completion ant then destroy callback
      if (!cb->timedRun(300)) {
	m_info = "Timed-out " + methodName + " for " + subAction->first;
	ers::error(PixLib::pix::daq (ERS_HERE, m_applName, m_info));
      }
      m_info = "Completed " + methodName + " for " + subAction->first;
      ers::info(PixLib::pix::daq (ERS_HERE, m_applName, m_info));
      cb->_destroy();
    } 
  }
  
  if (sync == PixActions::Synchronous){
    // Wait for completion and then destroy callback
    if (oneEnabled && cb->countConnections() != 0) {
      if (!cb->timedRun(300)) {
	m_info = "Timed-out " + methodName;
	ers::error(PixLib::pix::daq (ERS_HERE, m_applName, m_info));
      }
    }
    m_info = "Completed " + methodName;
    ers::info(PixLib::pix::daq (ERS_HERE, m_applName, m_info));
    cb->_destroy();
    std::cout << "cb->_destroy() completed" << std::endl;
  }
}

void PixActionsMulti::callIPCInOutMethod(std::string methodName, ipc::ActionOutput& output, bool checkRodEnable)
{
  PixLock lock(m_mutex);
  std::map<std::string, ipc::PixActions_var>::iterator subAction, subActionEnd=m_subActions.end();
  // Call method for subaction
  for(subAction=m_subActions.begin(); subAction!=subActionEnd; subAction++) {
    if (!checkRodEnable || m_rodEnable[subAction->first]) {
      PixActionsCallback *cb = new PixActionsCallback;
      try{
	(subAction->second)->ipc_setCallback(cb->_this());
	if(methodName == "rodStatus") (subAction->second)->ipc_rodStatus(output);
      } catch(...) {
	std::cout << "[WARNING] Exception caught in PixActionsMulti::callIPCInOutMethod" << std::endl;
	//error reporting procedure
      }
      // JGK: commented after removing inheritance from ipc-server - correct replacement?
      //      cb->run();
      cb->wait();
      cb->_destroy();
    }
  }
}


std::vector<std::string> PixActionsMulti::listSubActions(PixActions::Type type){
  PixLock lock(m_mutex);
  // Fill action names vector
  std::vector<std::string> actionsList;

  ipc::PixActionsType ipc_type = (ipc::PixActionsType((int)type));

  // List broker actions
  std::map<std::string,ipc::PixActions_var>::iterator actions, actionsEnd=m_subActions.end();
  for(actions=m_subActions.begin(); actions!=actionsEnd; actions++) {
    ipc::PixActionsType temp_type = ipc::PixActionsType((int)actions->second->ipc_type());
    if(temp_type == ipc_type || temp_type == ipc::ANY_TYPE){
      //Add subaction
      char* subName = actions->second->ipc_name();
      actionsList.push_back(std::string(subName));
      delete [] subName;
    }
    // Add subactions' actions
    ipc::stringList* subActions = (actions->second)->ipc_listSubActions(ipc_type);

    for(int i=0; i<(int)subActions->length(); i++){
      std::string temp = std::string((*subActions)[i]._retn());
      actionsList.push_back(temp);
    }
  }

  return actionsList;

}

bool PixActionsMulti::belongsToAction(std::string motherAction, std::string daughterAction)
{
  PixLock lock(m_mutex);
  if (motherAction == daughterAction) return true;
  std::map<std::string, std::vector<std::string> >::iterator correspondingElement = m_singleSubActions.find(motherAction);
  
  if(correspondingElement != m_singleSubActions.end()){

    std::vector<std::string>::iterator action, actionEnd = correspondingElement->second.end();
    for(action = correspondingElement->second.begin(); action != actionEnd; action++){
      if((*action) == daughterAction) return true;
    }

  }
  return false;
}


ipc::ActionInput PixActionsMulti::boolActionInput(bool bl)
{
  ipc::ActionInput input;
  input.m_bools.length(1);
  input.m_bools[0] = bl;
  return input;
}

ipc::ActionInput PixActionsMulti::intActionInput(long int integer)
{
  ipc::ActionInput input;
  input.m_ints.length(1);
  input.m_ints[0] = integer;
  return input;
}


ipc::ActionInput PixActionsMulti::stringActionInput(std::string str)
{
  ipc::ActionInput input;
  input.m_strings.length(1);
  input.m_strings[0] = CORBA::string_dup(str.c_str());
  return input;
}

ipc::ActionInput PixActionsMulti::stringBoolActionInput (std::string str, bool bl)
{
  ipc::ActionInput input;
  input.m_strings.length(1);
  input.m_strings[0] = CORBA::string_dup(str.c_str());
  input.m_bools.length(1);
  input.m_bools[0] = bl;
  return input;
}

ipc::ActionInput PixActionsMulti::stringLongActionInput (std::string str, long lg)
{
  ipc::ActionInput input;
  input.m_strings.length(1);
  input.m_strings[0] = CORBA::string_dup(str.c_str());
  input.m_ints.length(1);
  input.m_ints[0] = lg;
  return input;
}

ipc::ActionInput PixActionsMulti::stringLongLongActionInput (std::string str, long lg1, long lg2)
{
  ipc::ActionInput input;
  input.m_strings.length(1);
  input.m_strings[0] = CORBA::string_dup(str.c_str());
  input.m_ints.length(2);
  input.m_ints[0] = lg1;
  input.m_ints[1] = lg2;
  return input;
}

ipc::ActionInput PixActionsMulti::stringLongLongIntActionInput (std::string str, long lg1, long lg2, int int1)
{
  ipc::ActionInput input;
  input.m_strings.length(1);
  input.m_strings[0] = CORBA::string_dup(str.c_str());
  input.m_ints.length(3);
  input.m_ints[0] = lg1;
  input.m_ints[1] = lg2;
  input.m_ints[2] = int1;
  return input;
}


ipc::ActionInput PixActionsMulti::stringStringActionInput (std::string str1, std::string str2)
{
  ipc::ActionInput input;
  input.m_strings.length(2);
  input.m_strings[0] = CORBA::string_dup(str1.c_str());
  input.m_strings[1] = CORBA::string_dup(str2.c_str());
  return input;
}

ipc::ActionInput PixActionsMulti::stringStringLongLongActionInput (std::string str1, std::string str2, long lg1, long lg2)
{
  ipc::ActionInput input;
  input.m_strings.length(2);
  input.m_strings[0] = CORBA::string_dup(str1.c_str());
  input.m_strings[1] = CORBA::string_dup(str2.c_str());
  input.m_ints.length(2);
  input.m_ints[0] = lg1;
  input.m_ints[1] = lg2;
  return input;
}

ipc::ActionInput PixActionsMulti::stringLongBoolActionInput (std::string str, long lg, bool bl)
{
  ipc::ActionInput input;
  input.m_strings.length(1);
  input.m_strings[0] = CORBA::string_dup(str.c_str());
  input.m_ints.length(1);
  input.m_ints[0] = lg;
  input.m_bools.length(1);
  input.m_bools[0] = bl;
  return input;
}

ipc::ActionInput PixActionsMulti::floatBoolActionInput  (float fl, bool bl)
{
  ipc::ActionInput input;
  input.m_floats.length(1);
  input.m_floats[0] = fl;
  input.m_bools.length(1);
  input.m_bools[0] = bl;
  return input;
}



