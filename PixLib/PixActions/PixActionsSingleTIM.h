/////////////////////////////////////////////////////////////////////
// PixActionsSingleTIM.h
// version 1.1
/////////////////////////////////////////////////////////////////////
//
// 16/10/06  Version 1.0 (CS,NG,AO)
//           Initial release
//

//! Class for the Pixel single TIM actions

#ifndef _PIXLIB_ACTIONSSINGLETIM
#define _PIXLIB_ACTIONSSINGLETIM


#include "PixActions/PixActions.h"
#include "PixConnectivity/GenericConnectivityObject.h"

namespace PixLib {
  
  // Forward declarations
  class PixTrigController;
  class dbRecordIterator;
  class DbRecord;
  class PixConnectivity;
  class TimConnectivity;
  class PixDbServerInterface;
  class PixHistoServerInterface;

  class PixActionsSingleTIM : public PixActions {   
  public:
    
    // Constructor
    PixActionsSingleTIM(std::string actionName, std::string managingBrokerName, std::string allocatingBrokerName,
			IPCPartition *ipcPartition,
			TimConnectivity *tim, PixConnectivity *conn);
    
    PixActionsSingleTIM(std::string actionName, std::string managingBrokerName, PixDbServerInterface *DbServer, PixHistoServerInterface *hInt, std::string allocatingBrokerName,
			IPCPartition *ipcPartition,
			TimConnectivity *tim, PixConnectivity *conn);
       

    ~PixActionsSingleTIM();

    // State transitions
    virtual void initRun       (PixActions::SyncType sync=PixActions::Asynchronous);
    virtual void configure     (std::string, PixActions::SyncType sync=PixActions::Asynchronous);
    virtual void prepareForRun (PixActions::SyncType sync=PixActions::Asynchronous);
    virtual void startTrigger  (PixActions::SyncType sync=PixActions::Asynchronous);
    virtual void stopTrigger   (PixActions::SyncType sync=PixActions::Asynchronous);
    virtual void stopEB        (PixActions::SyncType sync=PixActions::Asynchronous);
    virtual void unconfigure   (PixActions::SyncType sync=PixActions::Asynchronous);
    virtual void initCal       (PixActions::SyncType ) {}
    virtual void resetRod      (PixActions::SyncType ) {}
    virtual void configureBoc  (PixActions::SyncType ) {}
    virtual void setup         (PixActions::SyncType ) {}
    virtual void connect       (PixActions::SyncType ) {}
    virtual void disconnect    (PixActions::SyncType ) {}

    virtual void listRODs        (PixActions::SyncType ) {}
    virtual void disableFailed   (PixActions::SyncType ) {}
    virtual void getRodActive    (std::string , PixActions::SyncType ) {}
    virtual void setPixScan      (std::string , PixActions::SyncType ) {}
    virtual void setRodActive    (std::string , bool , PixActions::SyncType ) {}
    virtual void setRodActive    (bool , PixActions::SyncType ) {}
    virtual void getModuleActive (std::string , long int , PixActions::SyncType ) {}
    virtual void selectFE        (std::string , long int , long int , PixActions::SyncType ) {}
    virtual void configSingleMod (std::string , long int , long int , PixActions::SyncType ) {}
    virtual void setHalfclockMod (PixActions::SyncType ) {}
    virtual void setModuleActive (std::string , long int , bool , PixActions::SyncType ) {}
    virtual void setVcal         (float , bool ,  PixActions::SyncType ) {}
    virtual void incrMccDelay    (float , bool , PixActions::SyncType ) {}

    // Scan

    virtual void abortScan            (PixActions::SyncType ) {}
    virtual void abortConnTest        (PixActions::SyncType ) {}
    virtual void scan                 (std::string , long int , PixActions::SyncType ) {}
    virtual void execConnectivityTest (std::string , long int , PixActions::SyncType ) {}
    virtual void presetScan           (std::string , long int , long int , int , PixActions::SyncType ) {}
    virtual void setConnTest          (std::string , PixActions::SyncType ) {}

    // Basic hardware operations
    virtual void reset      (PixActions::SyncType ) {}
    virtual void resetViset (PixActions::SyncType ) {}
    virtual void DCSsync    (PixActions::SyncType ) {}
    
    virtual void optoOnOff    (bool switchOn, PixActions::SyncType sync=PixActions::Synchronous) {}
    virtual void lvOnOff      (bool switchOn, PixActions::SyncType sync=PixActions::Synchronous) {}
    virtual void disable      (std::string objName, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void enable       (std::string objName, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void reset        (std::string objName, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void warmReconfig (std::string mode, std::string ecr, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void recover (std::string mode, std::string ecr, PixActions::SyncType sync=PixActions::Synchronous){}
    // Publishing in IS
    virtual void probe(PixActions::SyncType sync=PixActions::Asynchronous);

    //!Configure HW
    virtual void setActionsConfig(PixActions::SyncType sync=PixActions::Asynchronous);
    virtual void setRodBusyMask(long int mask, PixActions::SyncType sync=PixActions::Asynchronous);
    
    // Configuration handling
    virtual void readConfig   (PixActions::SyncType ) {}
    virtual void loadConfig   (PixActions::SyncType ) {}
    virtual void sendConfig   (PixActions::SyncType ) {}
    virtual void saveConfig   (std::string , PixActions::SyncType ) {}
    virtual void reloadConfig (std::string , PixActions::SyncType ) {}

    // Reset & Config mod
    virtual void resetmods(PixActions::SyncType ) {}

    // Test
    virtual void test1     (std::string test, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void test2     (std::string test, PixActions::SyncType sync=PixActions::Synchronous);

    virtual std::string getConnTagC(const char* subActionName){return std::string();};
    virtual std::string getConnTagP(const char* subActionName){return std::string();};
    virtual std::string getConnTagA(const char* subActionName){return std::string();};
    virtual std::string getConnTagOI(const char* subActionName){return std::string();};
    virtual std::string getCfgTag(const char* subActionName){return std::string();};
    virtual std::string getModCfgTag(const char* subActionName){return std::string();};
    virtual std::string getCfgTagOI(const char* subActionName){return std::string();};

    virtual void reloadTags(std::string newCfgTag, std::string newCfgModTag, 
			    long int newCfgRev, long int newCfgModRev, PixActions::SyncType sync=PixActions::Synchronous);
    virtual void setNewCfg( PixConnectivity *newRodBocConn);

    //Nosense for the TIM
    virtual const bool isHVOff( ) {return false;};

  private:
    
    // SW interfaces
   // PixDbInterface *m_RootDb;
    
    // PixLib objects
    PixTrigController *m_tim;
    PixConnectivity *m_conn;
    TimConnectivity *m_timConn;
    std::string m_name;
    PixDbServerInterface *m_dbServer;
    //PixHistoServerInterface *m_hInt;

    std::string m_applName;
    std::string m_info;
    std::string m_runPartName;

    // RodBusyMask to be set by RunControl with a dedicated method
    int m_rodBusyMask;
  };
}

#endif 
