/////////////////////////////////////////////////////////////////////
// PixActionsCallback.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 07/09/06  Version 1.0 (CS)
//           Initial release
//

//! Class for the Pixel actions callback

#ifndef _PIXLIB_ACTIONS_CALLBACK
#define _PIXLIB_ACTIONS_CALLBACK

//#include <ipc/server.h>
// possible replacement
#include <owl/semaphore.h>
#include <ipc/object.h>

#include "PixActionsIDL.hh"
#include "PixUtilities/PixLock.h"


namespace PixLib {
  
  //  class PixActionsCallback : public IPCServer, public IPCObject<POA_ipc::PixActionsCallback> {
  class PixActionsCallback : public OWLSemaphore, public IPCObject<POA_ipc::PixActionsCallback> {
    
  public:
    
    // Constructor
    PixActionsCallback() : m_connections(0), m_errors(0) {
      m_cond = new PixCondMutex("ActionCallBack");
    }
    
    // Destructor
    ~PixActionsCallback() { 
      delete m_cond;
    }
    
    // Reset
    void reset() {
      m_cond->lock();
      m_connections=0; 
      m_errors=0;
      m_cond->unlock();
    }

    bool timedRun(unsigned int timeout) {
      m_cond->lock();
      if (m_connections <= 0) {
	m_cond->unlock();
        return true;
      }
      if (!m_cond->wait(timeout)) {
	m_cond->unlock();
        return false;
      }
      m_cond->unlock();
      return true;
    }

    // Connection counter handling
    void  increaseConnections() {
      m_cond->lock();
      m_connections++;
      m_cond->unlock();
    }
    void  decreaseConnections() {
      m_cond->lock();
      m_connections--; 
      if (m_connections<=0) {
	m_cond->broadcast();
      }
      m_cond->unlock();
    }
    short countConnections() {
      return m_connections;
    }

    // Error bit mask
    void  setErrors(short bit) {
      m_cond->lock();
      m_errors+=(0x1<<bit);
      m_cond->unlock();
    }
    short getErrors() {
      return m_errors;
    }

  private:

    // Connections counter
    short m_connections;
    
    // Error bit mask
    short m_errors;
    
    // Condition lock
    PixCondMutex *m_cond;
  };
  
}

#endif // _PIXLIB_ACTIONS_CALLBACK
