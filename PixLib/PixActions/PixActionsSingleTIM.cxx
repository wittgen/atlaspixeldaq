/////////////////////////////////////////////////////////////////////
// PixActionsSingleTIM.cxx
// version 1.1
/////////////////////////////////////////////////////////////////////
//
// 16/10/06  Version 1.0 (CS,NG,AO)
//           Initial release
//

//! Class for the Pixel single TIM actions

#include "BaseException.h"
#include "RodCrate/TimDefine.h"
#include "RodCrate/TimModule.h"
//#include "PixController/PixScanConfig.h"
#include "PixDbInterface/PixDbInterface.h"
#include "RootDb/RootDb.h"
#include "PixTrigController/PixTrigController.h"
#include "PixTrigController/TimPixTrigController.h"
#include "PixConnectivity/PartitionConnectivity.h"
#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixConnectivity/TimConnectivity.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "PixConnectivity/PixConnectivity.h"

#include "PixActions/PixActionsSingleTIM.h"
#include "PixUtilities/PixMessages.h"

using namespace PixLib;


//! Constructor
PixActionsSingleTIM::PixActionsSingleTIM(std::string actionName, std::string managingBrokerName, std::string allocatingBrokerName,
					 IPCPartition *ipcPartition,
					 TimConnectivity *tim, PixConnectivity *conn) :
  PixActions(ipcPartition,actionName.c_str()), 
  m_conn(conn),
  m_timConn(tim),
  m_dbServer(NULL) {
  
  // Set action attributes
  m_descriptor.name = actionName;
  if(allocatingBrokerName == "") m_descriptor.available = true;
  else m_descriptor.available = false;
  m_descriptor.type = SINGLE_TIM;
  m_descriptor.allocatingBrokerName = allocatingBrokerName;
  m_descriptor.managingBrokerName = managingBrokerName;

  //m_is = new PixISManager(ipcPartition->name());
  m_name = "";
  if (m_timConn != NULL) {
    m_name = m_timConn->name();
    DbRecord *tRec = m_conn->getCfg(m_name);
    if (tRec != NULL) {
      // Create TIM object
      try {
    	m_tim = new TimPixTrigController(tRec, m_name);
      }
      catch (...){
	ers::error(PixLib::pix::daq (ERS_HERE, m_applName, " Exception caught in PixActionsSingleTIM constructor"));
    	return;
      }
    }
  }
  else {
    std::cout<<"PixActionsSingleTIM::PixActionsSingleTIM: NULL TimConnectivity passed to the constructor. Cannot continue!"<<std::endl;
    return;
  }

  try {
	if (!m_tim->status())ers::error(PixLib::pix::daq (ERS_HERE, m_applName,"TIM is not OK, check TIM status"));
      }
      catch (...){
	ers::error(PixLib::pix::daq (ERS_HERE, m_applName, " Exception caught in PixActionsSingleTIM constructor, check TIM status"));
      }

  //setup();
  publishDescriptor();
}


PixActionsSingleTIM::PixActionsSingleTIM(std::string actionName, std::string managingBrokerName, PixDbServerInterface *DbServer, PixHistoServerInterface *hInt, 
					 std::string allocatingBrokerName,
					 IPCPartition *ipcPartition,
					 TimConnectivity *tim, PixConnectivity *conn) :
  PixActions(ipcPartition,actionName.c_str()), 
  m_conn(conn),
  m_timConn(tim),
  m_dbServer(DbServer) {
  
  // Set action attributes
  m_descriptor.name = actionName;
  if(allocatingBrokerName == "") m_descriptor.available = true;
  else m_descriptor.available = false;
  m_descriptor.type = SINGLE_TIM;
  m_descriptor.allocatingBrokerName = allocatingBrokerName;
  m_descriptor.managingBrokerName = managingBrokerName;
  std::string globalTag = m_conn->getCfgTag();
  std::string domain = "Configuration-"+m_conn->getConnTagOI();
  m_name = "";

  m_applName= "PixActionsSingleTim_"+m_descriptor.name;
  if (m_timConn != NULL) {
    m_name = m_timConn->name();
    m_tim = new TimPixTrigController(m_dbServer, domain, globalTag, m_name);
  }
  else {
    ers::error(PixLib::pix::daq (ERS_HERE, m_applName,"PixActionsSingleTIM::PixActionsSingleTIM: NULL TimConnectivity passed to the constructor. Cannot continue!"));
    return;
  }

  try {
	if (!m_tim->status())ers::fatal(PixLib::pix::daq (ERS_HERE, m_applName, "TIM is not OK, check TIM status"));
      }
      catch (...){
	ers::error(PixLib::pix::daq (ERS_HERE, m_applName, " Exception caught in PixActionsSingleTIM constructor, check TIM status"));
      }

  //setup();
  publishDescriptor();
}


PixActionsSingleTIM::~PixActionsSingleTIM() {
  delete m_tim;
}

// State transitions


void PixActionsSingleTIM::initRun(PixActions::SyncType sync) 
{
  try {
    m_tim->init();
  }
  catch (...){
    ers::error(PixLib::pix::daq (ERS_HERE, m_applName, " Exception caught in PixActionsSingleTIM::initRun"));
  }
}

void PixActionsSingleTIM::setNewCfg(PixConnectivity *newRodBocConn){
  try{
  m_conn = newRodBocConn;
  std::string dom = "Configuration-"+m_conn->getConnTagOI();
  std::string tag = m_conn->getCfgTag();
  if(!m_tim->readConfig(m_dbServer, dom, tag) )std::cout <<  "Cannot read config for tim " << m_name << " from DbServer";

  
  } catch (...){
   ers::error(PixLib::pix::daq (ERS_HERE, m_applName, " Exception caught in PixActionsSingleTIM::setNewCfg"));
  }

}

void PixActionsSingleTIM::configure(std::string partName, PixActions::SyncType sync) {
  bool result;
  m_runPartName = partName;
  std::string dom = "Configuration-"+m_conn->getConnTagOI();
  std::string tag = m_conn->getCfgTag();  
  if(m_dbServer != NULL) {
    if( m_tim->config()->rev() != m_dbServer->getLastRev(dom,tag,m_name)) {
      result= m_tim->readConfig(m_dbServer, dom, tag);
      if ( !result){
	std::cout <<  "Cannot read config for tim " << m_name << " from DbServer";
      }  
    }
  }
  m_tim->busyMask() = m_rodBusyMask;
  std::cout << "TIM " << m_name << " RodBusyMask = 0x" << std::hex <<m_rodBusyMask << std::dec << std::endl;
  try {
    m_tim->configureTriggerMode();
  }
  catch (...){
   ers::error(PixLib::pix::daq (ERS_HERE, m_applName, " Exception caught in PixActionsSingleTIM::configure"));
  }
}

void  PixActionsSingleTIM::prepareForRun(PixActions::SyncType sync) {
  try {
    m_tim->prepareForRunTriggerMode();
  }
  catch (...){
    ers::error(PixLib::pix::daq (ERS_HERE, m_applName, " Exception caught in PixActionsSingleTIM::prepareForRun"));
  }
  std::cout << "TIM config freq dump:" <<  m_tim->triggerFreq() << std::endl;
}

void  PixActionsSingleTIM::startTrigger(PixActions::SyncType sync) { // Put TIM busy OFF
  try {
    m_tim->startTriggerMode();
  }
  catch (...){
    ers::error(PixLib::pix::daq (ERS_HERE, m_applName, " Exception caught in PixActionsSingleTIM::startTrigger"));
  }
}

void  PixActionsSingleTIM::stopTrigger(PixActions::SyncType sync) { // Stop sending triggers setting TIM busy 
  try {    
    m_tim->stopTriggerMode();
  }
  catch (...){
   ers::error(PixLib::pix::daq (ERS_HERE, m_applName, " Exception caught in PixActionsSingleTIM::stopTrigger"));
  }
}

void PixActionsSingleTIM::stopEB(PixActions::SyncType sync) {
  try {    
    m_tim->dumpCfgRegisters();
    m_tim->dumpCounters();
  }
  catch (...){
    ers::error(PixLib::pix::daq (ERS_HERE, m_applName, " Exception caught in PixActionsSingleTIM::stopEB"));
  }
}

void PixActionsSingleTIM::unconfigure(PixActions::SyncType sync) { // Same as init
  initRun(); 
}

void PixActionsSingleTIM::probe(PixActions::SyncType sync) {
  try {
    if (m_tim->publishTimInfo()==1)
      ers::error(PixLib::pix::daq (ERS_HERE, m_applName, "BUSY: FFTV Emergency action triggered"));
  }
  catch (...) {
    ers::error(PixLib::pix::daq (ERS_HERE, m_applName, " Exception caught in PixActionsSingleTIM::probe"));
  }
}

void PixActionsSingleTIM::disable(std::string objName, PixActions::SyncType sync) {
}

void PixActionsSingleTIM::enable(std::string objName, PixActions::SyncType sync) {
}

void PixActionsSingleTIM::reset(std::string objName, PixActions::SyncType sync) {
}

void PixActionsSingleTIM::warmReconfig(std::string mode, std::string ecr, PixActions::SyncType sync) {
}


void PixActionsSingleTIM::setActionsConfig(PixActions::SyncType sync) {
  bool result;
  bool inLoop=false;
  std::string dom = "Configuration-"+m_conn->getConnTagOI();
  std::string tag = m_conn->getCfgTag();  
  if(m_dbServer != NULL) {
    if( m_tim->config()->rev() != m_dbServer->getLastRev(dom,tag,m_name)) {
      inLoop=true;
      result= m_tim->readConfig(m_dbServer, dom, tag);
      if ( !result){
	std::cout <<  "Cannot read config for tim " << m_name << " from DbServer";
      }  
    }
  }
  m_tim->busyMask() = m_rodBusyMask;
  std::cout << "TIM " << m_name << " RodBusyMask = 0x" << std::hex <<m_rodBusyMask << std::dec << std::endl;
  if (inLoop) {
    try {
      m_tim->configureTriggerMode();
    }
    catch (...){
      ers::error(PixLib::pix::daq (ERS_HERE, m_applName, " Exception caught in PixActionsSingleTIM::setActionsConfig"));
    }
  }
}
  
void PixActionsSingleTIM::setRodBusyMask(long int mask, PixActions::SyncType sync) {
  m_rodBusyMask = mask;
  if(m_name== "TIM_I1")m_rodBusyMask &= 0x7FFF;//Masking DBM busy hardcoded from now on
  m_tim->timModule().regLoad(SctPixelRod::TIM_REG_ROD_MASK, 0x0); // disable all RODs
  m_tim->timModule().loadBitSet(SctPixelRod::TIM_REG_ROD_MASK, m_rodBusyMask); //set only allowed RODs
}


void PixActionsSingleTIM::reloadTags(std::string newCfgTag, std::string newCfgModTag, 
				     long int cfgRev, long int modCfgRev, PixActions::SyncType sync) 
{
  // Waiting for Paolo ...
  std::stringstream message;
  message << "Hello! I'm in  PixActionsSingleTIM::reloadTags(" << newCfgTag << ", " << newCfgModTag << ", " << cfgRev << ", " << modCfgRev <<")";
  std::cout << message.str() << std::endl;
 

}

void PixActionsSingleTIM::test1(std::string test, PixActions::SyncType sync) {
  std::cout << "TIM " << name() << " test1 start" << std::endl;
  sleep(10);
  std::cout << "TIM " << name() << " test1 stop" << std::endl;
}

void PixActionsSingleTIM::test2(std::string test, PixActions::SyncType sync) {
  std::cout << "TIM " << name() << " test2 executed - " << test << std::endl;
}
