#include "PixUDPWorker.h"

//Decode of QS packet//
#include "QSTalkTask.h"

using namespace PixLib;


std::string PixUDPWorker::showTime(time_t ti) {
  static std::string timeFormat = "[ %F %T ]: ";
  std::string tstrbuf;
  tstrbuf.resize(50);
  strftime(&tstrbuf[0], tstrbuf.size(), timeFormat.c_str(), std::localtime(&ti));
  return tstrbuf;
}


PixUDPWorker::PixUDPWorker(PixLib::PixFitWorkQueue<UDPPacket> *queue, const std::string &partition){
						
  m_verbose   = true;
  m_UDPqueue  = queue; 
  
  m_partition = partition;
  //m_varnames  = {"maskStep","par0","par1","par2","errorState","scanState"};
  //All this need to be cleaned up
  m_varnames[0] = "maskStep";
  m_varnames[1] = "par0";
  m_varnames[2] = "par1";
  m_varnames[3] = "par2";
  m_varnames[4] = "errorState";
  m_varnames[5] = "scanState";
  m_udpsrvname= "RunParams";   //TODO: pass it as argument
  
  m_states = {"NEW","RUN","READY","WAIT","TIM_WAIT","DELAY","DEAD"};
  
}

//TODO: possible to optimise?

/*This method is used to get the RodString from the Rod Ip */
/*In the pit the scheme is the following:
 - 10.145.96+LCrate.200+Slot (old pixel, new readout)
 - i.e. for L1_S5 the ip is 10.145.97.205
 
 while for IBL is
 -10.145.87.200+Slot
 */

/*For SR1 instead the ROD-IP scheme is 
  -192.168.10+Crate.200+Slot
  
 */

std::string PixUDPWorker::getRodStringFromIp(const std::string& ip_addr) {
  
  std::vector<std::string> ip_addr_c;
  
  boost::split(ip_addr_c,ip_addr,boost::is_any_of("."));
  
  //int crateNumber = 0;
  int rodNumber   = 0;
  std::string crateName = "";
  
  if (!ip_addr_c[0].compare("192") && !ip_addr_c[1].compare("168")) {  //SR1
    
    //TDAQ7 scheme
    //crateNumber   = atoi(ip_addr_c[2].c_str())-10;
    rodNumber        = atoi(ip_addr_c[3].c_str())-200;
    
    //TDAQ6
    //crateNumber = atoi(ip_addr_c[2].c_str());   
    //rodNumber   = atoi(ip_addr_c[3].c_str()) / 10;
  }

  else
    {
        switch (atoi(ip_addr_c[2].c_str())){
    case 87:
      crateName = "I1";
      break;
    case 97:
      crateName = "L1";
      break;
    case 98:
      crateName = "L2";
      break;
    case 99:
      crateName = "L3";
      break;
    case 100:
      crateName = "L4";
      break;
      //crateNumber = atoi(ip_addr_c[2]);
    default:
      std::cout<<"Misconfiguration of ROD IP"<<std::endl;
      break;
	}
      rodNumber        = atoi(ip_addr_c[3].c_str())-200;
      
    }
  
  //TDAQ6
  /*
    if (rodNumber != 9)
    crateNumber=3;
    if (rodNumber==7)
    crateNumber=1;
 
    
    else if ((!ip_addr_c[0].compare("10")) && (!ip_addr_c[1].compare("145"))) { //P1
    crateNumber = atoi(ip_addr_c[2].c_str()) - 96;
    rodNumber   = atoi(ip_addr_c[3].c_str()) - 200;
 
    }
  */
  
  std::string rodName = "ROD_";
  rodName=rodName+crateName+"_S";
  std::ostringstream stream;
  stream<<rodNumber;
  rodName+=stream.str();
  return rodName;

  //std::ostringstream stream;
  //stream<<crateNumber;
  //std::string rodName = "ROD_C";
  //rodName+=stream.str()+"_S";
  //stream.str(""); stream.clear();
  //stream<<rodNumber;
  //rodName+=stream.str();
  //return rodName;
    
}


void PixUDPWorker::loop() {

  IPCPartition partInfr(m_partition);
  ISInfoDictionary dict(partInfr);
  unsigned int num=0;
  
  while(true) {
    
    /* get the UDP packet from the queue */
    auto packet = m_UDPqueue->getWork();
    if (m_UDPqueue->stopping())
      return;
    
    /* skip in the case of null ptr - this is actually not needed, I think*/ 

    if (!packet)
      continue;
    
    //Get the packet header to understand what packet is this
    packet->DecodeHeader();

    //The best way to do the following operations would be to use a factory design pattern
    //Base class for UDPpacket, than create what you need and add the proper decode method to those guys
    
    /* Sys Mon packet */
    if (packet->getHeader() == 0xcafeefac)
      {
	std::cout<<"SYS MON PACKET"<<std::endl;
	packet->DecodePacket();
	packet->DumpUDPPacket();
	string rodName = getRodStringFromIp(packet->getIpAddress());
	//need to mutex this?
	updateSysMonIS(rodName,packet.get(),dict);
      }
    
    //Quick status Packet
    else if (packet->getHeader() == 0xabcd1234)
      {
	
	std::cout<<"QS packet"<<std::endl;
	QSTalkTask dummyQSTT;
	dummyQSTT.result.deserialize(packet->m_intbuf);
	m_qsMonInfos = dummyQSTT.result.m_qsMonInfos;
	DumpQSInfo();
      }

    //Debug packets to test server performance
    
    else if (packet->getHeader() == 0xaa4321aa)
      {
	num++;
	packet->DecodeDebugPacket();
	packet->DumpDebugUDPPacket();
	std::cout<<"Thread:"<<this->getThreadId()<<" number of packets decoded:"<<num<<std::endl;
      }
    
    // under mutex? 
    //updateIs(getRodStringFromIp(packet->getIpAddress()),packet->getScanStatus(),dict);
  }
  
  return;
}




void PixUDPWorker::updateSysMonIS(const std::string &RODNAME, UDPPacket const * const packet, const ISInfoDictionary &dict) {
  
  /* Loop over the thread names */
  std::cout<<showTime()<<" "<<RODNAME+" ";
  for (unsigned int i = 0; i < packet->GetThreadNames().size(); i++)
    {
      /* form the string to be checked in: PID_STATE_ATICKS */
      
      std::string value = std::to_string(packet->GetPids().at(i))+"_"+m_states[packet->GetProcessStates().at(i)]+"_"+std::to_string(packet->GetAticks().at(i));
      if (m_verbose)
	{
	  if ((packet->GetThreadNames().at(i)).find("cmdQS")!=std::string::npos ||
	      (packet->GetThreadNames().at(i)).find("sysMon")!=std::string::npos||
	      (packet->GetThreadNames().at(i)).find("tcpip_threa")!=std::string::npos||
	      (packet->GetThreadNames().at(i)).find("cmdsrvd")!=std::string::npos||
	      (packet->GetThreadNames().at(i)).find("UNKNOWN")!=std::string::npos
	      )
	  std::cout<<packet->GetThreadNames().at(i)<<"  "<<value<<"  ";
	}
      
      ISInfoString isString(value);
      dict.checkin(m_udpsrvname + ".ROD_CRATE/"+RODNAME+"/UDPInfo/"+packet->GetThreadNames().at(i),isString);

    }
  std::cout<<std::endl;
}

void PixUDPWorker::updateIs(const std::string &RODNAME, uint32_t data[6], const ISInfoDictionary &dict) {
  
  for (int i = 0; i<6; ++i)
    {
      ISInfoUnsignedInt isUint( data[i] );
      std::cout<<"Worker thread "<<this->getThreadId()<<" will push to Is:"<<m_udpsrvname + ".ROD_CRATE/"+RODNAME+"/UDPInfo/"+m_varnames[i]<<" with value "<< (int)isUint<<std::endl; 
      dict.checkin(m_udpsrvname + ".ROD_CRATE/"+RODNAME+"/UDPInfo/"+m_varnames[i],isUint);
      
    }
  
  return;
}


void PixUDPWorker::stopWork() {
  m_UDPqueue->stopWork();
}


void PixUDPWorker::DumpQSInfo() {
  
  
  UDPPacket::printElement("slaveBusyCounters Slave A");
  std::cout<<std::endl;
  
  for (uint8_t i = 0 ; i<32 ; i++)
    UDPPacket::printElement(m_qsMonInfos.slaveBusyCounters[i]);
  std::cout<<std::endl;
  
  UDPPacket::printElement("slaveBusyCounters Slave B");
  std::cout<<std::endl;
  
  for (uint8_t i = 32 ; i<64 ; i++)
    UDPPacket::printElement(m_qsMonInfos.slaveBusyCounters[i]);
  std::cout<<std::endl;
  
  UDPPacket::printElement("nCFG rod Busy");
  std::cout<<std::endl;
  
  for (uint8_t i = 0 ; i<32 ; i++)
    UDPPacket::printElement(m_qsMonInfos.nCFG_rodBusy[i]);
  std::cout<<std::endl;

  UDPPacket::printElement("nCFG timeout");
  std::cout<<std::endl;
  
  for (uint8_t i = 0 ; i<32 ; i++)
    UDPPacket::printElement(m_qsMonInfos.nCFG_timeout[i]);
  std::cout<<std::endl;


  UDPPacket::printElement("link TrigCounters");
  std::cout<<std::endl;
  
  for (uint8_t i = 0 ; i<32 ; i++)
    UDPPacket::printElement(m_qsMonInfos.linkTrigCounters[i]);
  std::cout<<std::endl;

  UDPPacket::printElement("link OccCounters");
  std::cout<<std::endl;
  
  for (uint8_t i = 0 ; i<32 ; i++)
    UDPPacket::printElement(m_qsMonInfos.linkOccCounters[i]);
  std::cout<<std::endl;

  UDPPacket::printElement("tot Desync Counters");
  std::cout<<std::endl;
  
  for (uint8_t i = 0 ; i<32 ; i++)
    UDPPacket::printElement(m_qsMonInfos.totDesyncCounters[i]);
  std::cout<<std::endl;

  
  UDPPacket::printElement("residual Desync Counters");
  std::cout<<std::endl;
  
  UDPPacket::printElement("timeout Counters");
  std::cout<<std::endl;
  
  for (uint8_t i = 0 ; i<32 ; i++)
    UDPPacket::printElement(m_qsMonInfos.timeoutCounters[i]);
  std::cout<<std::endl;

  UDPPacket::printElement("BOC decoding error Counters");
  std::cout<<std::endl;
  
  for (uint8_t i = 0 ; i<32 ; i++)
    UDPPacket::printElement(m_qsMonInfos.decodingErrorBOC[i]);
  std::cout<<std::endl;

  UDPPacket::printElement("BOC frame error Counters");
  std::cout<<std::endl;
  
  for (uint8_t i = 0 ; i<32 ; i++)
    UDPPacket::printElement(m_qsMonInfos.frameErrorBOC[i]);
  std::cout<<std::endl;

  UDPPacket::printElement("BOC frame count Counters");
  std::cout<<std::endl;
  
  for (uint8_t i = 0 ; i<32 ; i++)
    UDPPacket::printElement(m_qsMonInfos.frameCountBOC[i]);
  std::cout<<std::endl;

  UDPPacket::printElement("BOC mean occupancy Counters");
  std::cout<<std::endl;
  
  for (uint8_t i = 0 ; i<32 ; i++)
    UDPPacket::printElement(m_qsMonInfos.meanOccBOC[i]);
  std::cout<<std::endl;

  // To do: add print out for s-link counters

  UDPPacket::printElement("masterBusy");
  UDPPacket::printElement("fmtLinkEnable");
  UDPPacket::printElement("QSDisabled");
  UDPPacket::printElement("waitingForECR");
  std::cout<<std::endl;
  
  UDPPacket::printElement(m_qsMonInfos.masterBusy);
  UDPPacket::printElement(m_qsMonInfos.fmtLinkEn);
  UDPPacket::printElement(m_qsMonInfos.QSDisabled);
  UDPPacket::printElement(m_qsMonInfos.waitingForECR);
  
  std::cout<<std::endl;
  
  
}
