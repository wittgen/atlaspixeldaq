#ifndef PIXUDPWORKER_H
#define PIXUDPWORKER_H

#include <ipc/core.h>
#include <is/info.h>
#include <is/infoT.h>
#include <is/infodictionary.h>
#include <boost/algorithm/string.hpp>
#include <stdint.h>
#include <memory>
#include <string>
#include <ctime>
#include <vector>

#include "PixFitServer/PixFitWorkQueue.h"
#include "UDPPacket.h"
#include "PixThread.h"
#include "QuickStatus.h"

class UDPPacket;
namespace PixLib {
template <class T> class PixFitWorkQueue;
}  // namespace PixLib

class PixUDPWorker : public PixThread{
  
 public:
  
  PixUDPWorker(PixLib::PixFitWorkQueue<UDPPacket> *queue, const std::string &partition);
  virtual ~PixUDPWorker(){};
  
  //TODO: move to std instead of boost.
  
  void stopWork();

  /* Dump of the qs monitoring info structure */
  void DumpQSInfo();

  /* show current time */			
  std::string showTime(time_t ti = std::time(NULL));
  
 private:
  
  /**Function that does the looping*/
  void loop();
  
  /** Pointer to input queue*/
  PixLib::PixFitWorkQueue<UDPPacket> *m_UDPqueue;
  
  /** Partition name */
  std::string m_partition;
  
  /** converts the ip_address of a ROD to its ROD name */
  std::string getRodStringFromIp(const std::string& ip_addr);
  
  /** Update Is Server*/
  
  void updateIs(const std::string &RODNAME, uint32_t data[6], const ISInfoDictionary &dict);
  
  void updateSysMonIS(const std::string &RODNAME, UDPPacket const * const packet, const ISInfoDictionary &dict);
    
  /** names of the Scan Status */
  
  std::string m_varnames[6];
  
  /** IS Server name */
  std::string m_udpsrvname;

  /* Quick status monitoring structure */
  
  QuickStatus::monitoring_infos m_qsMonInfos;

  //This should go to the UDP Packet instead
  /* UDP Packet States */
  
  std::vector<std::string> m_states;

  /* verbose flag */
  
  bool m_verbose;

};


#endif 
