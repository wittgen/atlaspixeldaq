#ifndef PIXTHREAD_H
#define PIXTHREAD_H

#include <string>
#include <boost/thread.hpp>
#include <ers/ers.h>



class PixThread {

 public:
  
  PixThread();
  ~PixThread(){};
  
  /** Spawns a thread and eventually calls the loop function. */
  void start();

  /* detach a thread */
  
  void detach();
  
  /** join a thread  */
  
  void join();
  
  /** returns Unique identifier to the thread */
  boost::thread::id getThreadId();
  
  /** return Handle on the thread */
  boost::thread* getThread();
      
  /** returns the status of the worker thread - not implemented*/
  //int getStatus();
  

 private: 

  /**Setups and launch the  thread */
  void setupAndlaunch();
  
  //Need to be implemented by the inheriting class
  virtual void loop() = 0;

  /** thread instance */
  boost::thread m_thread;
  /** Identifier for the thread */
  boost::thread::id m_threadId;
  /** ThreadStatus - not implemented*/
  //int m_threadStatus;


};


#endif 
