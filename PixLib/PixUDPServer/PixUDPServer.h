#ifndef PIXUDPSERVER_H
#define PIXUDPSERVER_H

#include <sstream>
#include <memory>
#include <vector>

#include <sys/types.h>
#include <sys/socket.h>


#include "PixUtilities/PixISManager.h"
#include "PixUDPWorker.h"

//Queue for Producer - Worker communication
#include "PixFitServer/PixFitWorkQueue.h" 


/**Instance of a multi-client UDP server using multi-threading. Should be instantiatied only once. No singleton pattern yet */

class PixUDPServer{
 public:
  PixUDPServer(const std::string &partition, const int& nWorkers);
  ~PixUDPServer(){};
  
  /** starts the Server */
  void run();
  
  

 private:
  /** sets up the UDP Server */
  bool setupPixUDPServer();
  /** prints an intial message*/
  void printBanner();
  
  /**start worker threads */
  void startWorkerThreads(PixLib::PixFitWorkQueue<UDPPacket> *queue, const std::string &partition);
  
  /** flag to have verbose output*/
  bool m_verbose;
  
  /** vector containing the pointers to the worker threads */
  std::vector<std::shared_ptr<PixUDPWorker> > m_workers;
    
  
  /**listening port */
  int port;
  struct sockaddr_in server;
  struct sockaddr_in client;
  int sock, n;
  socklen_t clientlen;
 // uint8_t intbuf[STAT_UDP_BUFLEN];  //STAT_UDP_BUFLEN is defined in UDPPacket.h
  std::string m_partition;
  int m_nWorkers;
  
  
};

#endif
