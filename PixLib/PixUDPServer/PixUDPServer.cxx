#include "PixUDPServer.h"


PixUDPServer::PixUDPServer(const std::string &partition, const int &nWorkers) {
  m_verbose=false;
  //port=10427;
  port=11111;
  m_partition=partition;
  m_nWorkers = nWorkers;
  
}

bool PixUDPServer::setupPixUDPServer() {
  
  clientlen=sizeof(struct sockaddr_in);
  
  sock=socket(AF_INET,SOCK_DGRAM,0);
  bzero(&server,sizeof(server));
  server.sin_family=AF_INET;
  server.sin_addr.s_addr=INADDR_ANY;
  server.sin_port=htons(port);
  if (bind(sock,(struct sockaddr* )&server,sizeof(server))<0)
    {
      std::cout<<"Error in binding socket"<<std::endl;
      return false;
    }  
  
  return true;
}


void PixUDPServer::startWorkerThreads(PixLib::PixFitWorkQueue<UDPPacket> *queue, const std::string& partition) {
  
  for (int i=0; i<m_nWorkers; ++i)
    {
      m_workers.push_back(std::make_shared<PixUDPWorker>(queue,partition));
      m_workers[i]->detach();
      m_workers[i]->start();
    }
  return;
}


void PixUDPServer::run(){
  
  printBanner();
  bool setup = setupPixUDPServer();
   
  
  
  if (!setup)
    {
      std::cout<<"Error in setting up the UDP server.Exiting.."<<std::endl;
      return;
    }

  
  PixLib::PixFitWorkQueue<UDPPacket> UDPqueue("UDPqueue");
  
  
  startWorkerThreads(&UDPqueue,m_partition);

  //Server running 
  while (1) {
    auto udp = std::make_unique<UDPPacket>();
    n=recvfrom(sock,udp->m_intbuf,sizeof(udp->m_intbuf),0,(struct sockaddr*)&client,&clientlen);
    if (n<0) 
      std::cout<<"Error receiving packet"<<std::endl;
    
    char ipAddress[INET_ADDRSTRLEN];
    
    
    std::cout<<"Received packet from "<<inet_ntop(AF_INET,&(client.sin_addr),ipAddress,INET_ADDRSTRLEN)<<std::endl;
    
    //auto udp = std::make_shared<UDPPacket>(intbuf,ipAddress);
    udp->setIpAddress(ipAddress);
    
    /* Adding the packet to the work queue */
    UDPqueue.addWork(std::move(udp));
    
  }
}

    
void PixUDPServer::printBanner(){
  std::cout<<"PixUDPServer, version 1.0"<<std::endl;
  std::cout<<"Monitor scan status via PPC-host UDP communication"<<std::endl;
  std::cout<<"Listening on Port:"<<port<<std::endl;
}
