#ifndef UDPPACKET_H
#define UDPPACKET_H

#include <string>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <iostream>
#include <iomanip>


//Make a base class and then separate between the sysMon and the QS packet...

//Would prefer to avoid the use of std container...
#include <vector>
#include <map>

//I'm planning to share the UDPPacket between the PPC and the host.
//The UDPPacket should have an encode function and a decode function.
//Whenever the encode function is changed the decode function should be changed accordingly.
//For the moment I'll just put a decode function. TEMPORARY


//Dangerous.. Use common header with ppc?
#ifndef STAT_UDP_BUFLEN
#define STAT_UDP_BUFLEN 1024
#endif


//Dangerous.. Use common header with ppc?
#ifndef SYS_THREAD_MAX
#define SYS_THREAD_MAX 16
#endif

#ifndef NAME_LENGHT 
#define NAME_LENGHT 12
#endif

class UDPPacket {
  
 public:
  
  UDPPacket();
  
  
  /* This function gets the IpAddress of the Rod that is sending the packet */
  std::string getIpAddress();

  /* This function sets the IpAddress of the Rod that is sending the packet */
  void setIpAddress(const char* ipA) {ipAddress=ipA;}
  
  /* deprecated */
  uint32_t* getScanStatus();

  /* This function decodes the packet calling the proper decoding functions in the right order */
  bool DecodePacket(uint8_t* intbuf);

  /* This function decodes the packet calling the proper decoding functions in the right order */
  bool DecodePacket();

  /* This function decodes the debug packet */
  bool DecodeDebugPacket();
  
  /* Dump the content of the package */
  
  void DumpUDPPacket();

  void DumpDebugUDPPacket();

  /* Dump the information relative to a netBlock */
  
  void DumpNetBlock(uint32_t i);
  
  
  /* Template for dumping (Should be in some sort of general utilities)*/
  template<typename T> static void printElement(T t, const int& width = 12)
    {
      
      std::cout << std::left << std::setw(width) << std::setfill(' ') << t;
    }
  

  /* Public buffer */
  
  uint8_t m_intbuf[STAT_UDP_BUFLEN];

  /* return packet header */
  uint32_t getHeader() {return m_header;}

  /* decode packet header */
  
  void DecodeHeader();

  /* Return the Thread infos vectors */
  
  const std::vector<std::string>& GetThreadNames() const {return m_threadNames;}
  const std::vector<uint32_t>& GetPids() const {return m_pids;}
  const std::vector<uint32_t>& GetAticks() const {return m_aticks;}
  const std::vector<uint8_t>& GetProcessStates() const {return m_processStates;}

  

 private:

  static const int scanPars = 6;
  uint32_t scanStatus[scanPars];
  std::string ipAddress;
  
  /* This variable keeps track of how much has been read of the buffer */
  int m_len;
  
  /* Packet header */
  uint32_t m_header;
  
  /* This reads the kernel ticks, the result of getting the kernel stats structure, the scheduler history */
  bool getKernelInfos(uint8_t* intbuf);
  bool getKernelInfos();

  /* This reads the thread names as well as the strack addr, the stack size, the stack ptr. It also reads the pid, the process state, the kernel ticks and the priority.*/
  bool getThreadInfos(uint8_t* intbuf); 
  bool getThreadInfos(); 

  /* This reads the network information for link, tcp and udp */
  bool getNetInfos(uint8_t* intbuf);
  bool getNetInfos();

  /* This reads the malloc information */
  bool getMallInfos(uint8_t* intbuf);
  bool getMallInfos();
  
  /* Merges together four uint8_t into uint32_t */
  
  uint32_t get32(const uint8_t &part1, const uint8_t &part2, const uint8_t &part3, const uint8_t &part4);

  /* Merges together four uint8_t into one uint32_t and moves m_len of 4 */
  
  uint32_t get32(uint8_t *intbuf);
  
  /* Merges together two uint8_t into uint16_t and moves m_len of 2*/
  
  uint16_t get16(const uint8_t &part1, const uint8_t &part2);
  
  /* Merges together two uint8_t into uint16_t and moves m_len of 2*/
  
  uint16_t get16(uint8_t *intbuf);

  /* returns the buffer element and moves m_len of 1 */
  
  uint8_t get8(uint8_t *intbuf);

  /* Get the current m_len */
  
  int getCurrentLength() {
    return m_len;
  }  
  
  /* Kernel info */
  uint32_t m_ticks;
  uint32_t m_result;
  uint32_t m_schedHist;
  std::vector <uint32_t> m_schedHist_vec;

  /* Threads info */
  uint32_t m_nproc;
  std::vector<std::string> m_threadNames;
  std::vector<uint32_t> m_pids;
  std::vector<uint8_t>  m_processStates;
  std::vector<uint32_t> m_aticks;
  std::vector<uint8_t>  m_priorities;
  std::vector<uint32_t> m_stackaddrs;
  std::vector<uint32_t> m_stacksizes;
  std::vector<uint32_t> m_stackptrs;
  unsigned int m_semaphores;

  /* net info */
  uint32_t m_netBlocks;
  
  std::vector<uint32_t> m_xmit;
  std::vector<uint32_t> m_recv;
  std::vector<uint32_t> m_fw;
  std::vector<uint32_t> m_drop;
  std::vector<uint32_t> m_chkerr;
  std::vector<uint32_t> m_lenerr;
  std::vector<uint32_t> m_memerr;
  std::vector<uint32_t> m_rterr;
  std::vector<uint32_t> m_proterr;
  std::vector<uint32_t> m_opter;
  std::vector<uint32_t> m_err;
  std::vector<uint32_t> m_cachehit;

  /* malloc info */
  
  uint32_t m_arena;
  uint32_t m_uordblks;
  uint32_t m_fordblks;
  uint32_t m_keepcost;
  

  /* debug packet related members.. */
  
  std::vector<uint32_t> m_decodedDebugPacket;
  
  
};

#endif
