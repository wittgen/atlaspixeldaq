#include "UDPPacket.h"

//what if the size of the buffer is not the right one? I think I should use a vector or a []

/* Constructor to be used on the ppc for the future*/ 



/* Constructor to be used on the host */
UDPPacket::UDPPacket() {
 
  m_len = 0;
  //std::copy(intbuf,intbuf+STAT_UDP_BUFLEN,m_intbuf);
        
}

std::string UDPPacket::getIpAddress(){
  return ipAddress; 
}

uint32_t* UDPPacket::getScanStatus(){
  return scanStatus;
}

bool UDPPacket::getKernelInfos(uint8_t *intbuf){

  m_ticks     = get32(intbuf);
  m_result    = get32(intbuf);
  m_schedHist = get32(intbuf);

  for (uint32_t i = 0 ; i<m_schedHist ; i++) {
    m_schedHist_vec.push_back(get32(intbuf));
  }
  
  m_nproc     = get32(intbuf);

  return true;}




bool UDPPacket::getKernelInfos(){

  m_ticks     = get32(m_intbuf);
  m_result    = get32(m_intbuf);
  m_schedHist = get32(m_intbuf);

  for (uint32_t i = 0 ; i<m_schedHist ; i++) {
    m_schedHist_vec.push_back(get32(m_intbuf));
  }
  
  m_nproc     = get32(m_intbuf);

  return true;}



bool UDPPacket::getThreadInfos(uint8_t *intbuf){
  
  for (uint32_t j = 0 ; j<m_nproc ; j++) {
    //problematic..There can be less threads than the maximum!! Shall I send the number of threads ?!?
    //for (uint32_t j = 0 ; j < SYS_THREAD_MAX; j++) {
    //for (l=0; l<NAME_LENGTH;l++) {
    //      }
    //For the moment I'm following the python receiver implementation
    
    //Decode the thread name
    char threadName[NAME_LENGHT] = "";
    for (uint8_t ix = 0; ix<NAME_LENGHT; ix++)
    threadName[ix]=get8(intbuf);
    
    m_threadNames.push_back(threadName);
    m_pids.push_back(get32(intbuf));
    m_processStates.push_back(get8(intbuf));
    m_aticks.push_back(get32(intbuf));
    m_priorities.push_back(get8(intbuf));
    m_stackaddrs.push_back(get32(intbuf));
    m_stacksizes.push_back(get32(intbuf));
    m_stackptrs.push_back(get32(intbuf));
  }
  
  return true;}

bool UDPPacket::getThreadInfos(){
  
  for (uint32_t ix = 0 ; ix<m_nproc ; ix++) {
    //problematic..There can be less threads than the maximum!! Shall I send the number of threads ?!?
    //for (uint32_t j = 0 ; j < SYS_THREAD_MAX; j++) {
    //for (l=0; l<NAME_LENGTH;l++) {
    //      }
    //For the moment I'm following the python receiver implementation
    
    //Decode the thread name
    char threadName[NAME_LENGHT] = "";
    for (uint8_t iy = 0; iy<NAME_LENGHT; iy++)
    threadName[iy]=get8(m_intbuf);
    
    m_threadNames.push_back(threadName);
    m_pids.push_back(get32(m_intbuf));
    m_processStates.push_back(get8(m_intbuf));
    m_aticks.push_back(get32(m_intbuf));
    m_priorities.push_back(get8(m_intbuf));
    m_stackaddrs.push_back(get32(m_intbuf));
    m_stacksizes.push_back(get32(m_intbuf));
    m_stackptrs.push_back(get32(m_intbuf));
  }
  
  return true;}


//Optimise? If I know that I have 3 net blocks I can actually use only an array
bool UDPPacket::getNetInfos(uint8_t *intbuf){
  
  m_xmit.push_back(get32(intbuf));             /* Transmitted packets. */
  m_recv.push_back(get32(intbuf));             /* Received packets. */
  m_fw.push_back(get32(intbuf));               /* Forwarded packets. */
  m_drop.push_back(get32(intbuf));             /* Dropped packets. */
  m_chkerr.push_back(get32(intbuf));           /* Checksum error. */
  m_lenerr.push_back(get32(intbuf));           /* Invalid length error. */
  m_memerr.push_back(get32(intbuf));           /* Out of memory error. */
  m_rterr.push_back(get32(intbuf));            /* Routing error. */
  m_proterr.push_back(get32(intbuf));          /* Protocol error. */
  m_opter.push_back(get32(intbuf));           /* Error in options. */
  m_err.push_back(get32(intbuf));              /* Misc error. */
  m_cachehit.push_back(get32(intbuf));         

  return true;}


//Optimise? If I know that I have 3 net blocks I can actually use only an array
bool UDPPacket::getNetInfos(){
  
  m_xmit.push_back(get32(m_intbuf));             /* Transmitted packets. */
  m_recv.push_back(get32(m_intbuf));             /* Received packets. */
  m_fw.push_back(get32(m_intbuf));               /* Forwarded packets. */
  m_drop.push_back(get32(m_intbuf));             /* Dropped packets. */
  m_chkerr.push_back(get32(m_intbuf));           /* Checksum error. */
  m_lenerr.push_back(get32(m_intbuf));           /* Invalid length error. */
  m_memerr.push_back(get32(m_intbuf));           /* Out of memory error. */
  m_rterr.push_back(get32(m_intbuf));            /* Routing error. */
  m_proterr.push_back(get32(m_intbuf));          /* Protocol error. */
  m_opter.push_back(get32(m_intbuf));           /* Error in options. */
  m_err.push_back(get32(m_intbuf));              /* Misc error. */
  m_cachehit.push_back(get32(m_intbuf));         

  return true;}

bool UDPPacket::getMallInfos(uint8_t *intbuf){
  m_arena    = get32(intbuf);
  m_uordblks = get32(intbuf);
  m_fordblks = get32(intbuf);			
  m_keepcost = get32(intbuf);
  
  return true;}

bool UDPPacket::getMallInfos(){
  
  m_arena    = get32(m_intbuf);
  m_uordblks = get32(m_intbuf);
  m_fordblks = get32(m_intbuf);			
  m_keepcost = get32(m_intbuf);
  
  return true;}

//There is no need to use ntohl(ntohs). These functions should do the trick

uint32_t UDPPacket::get32(const uint8_t &part1, const uint8_t &part2, const uint8_t &part3, const uint8_t &part4)
  {
    uint32_t value = (((part1<<24) | (part2<<16)) | (part3<<8)) | part4;
    m_len+=4;
    
    return value;
  }

uint32_t UDPPacket::get32(uint8_t *intbuf) {
  uint32_t value = (((intbuf[m_len] << 24) | (intbuf[m_len+1] << 16)) | intbuf[m_len+2] << 8  ) | intbuf[m_len+3];
  m_len+=4;
  
  return value;
}

uint16_t UDPPacket::get16(const uint8_t &part1, const uint8_t &part2) {
    uint16_t value = (part1<<8) | part2;
    m_len+=2;
    
    return value;
  }

uint16_t UDPPacket::get16(uint8_t *intbuf) {
  uint16_t value = (intbuf[m_len] << 8) | intbuf[m_len+1];
  m_len+=2;
  
  return value; 
}

 uint8_t UDPPacket::get8(uint8_t* intbuf){
   uint8_t value = intbuf[m_len];
   m_len+=1;
   return value;
 }


void UDPPacket::DecodeHeader() {
  m_header = get32(m_intbuf);
  //std::cout<<"header:: "<<std::hex<<m_header<<std::dec<<std::endl;
}

bool UDPPacket::DecodeDebugPacket() {
  
  int packetsize = get32(m_intbuf);
  
  for (int i=0; i<packetsize-2; i++)
    m_decodedDebugPacket.push_back(get32(m_intbuf));
    
  return true;
}

void UDPPacket::DumpDebugUDPPacket() {
  
  for (unsigned int i=0; i<m_decodedDebugPacket.size(); i++)
    printElement( m_decodedDebugPacket.at(i));
  //std::cout<<std::endl;
  //std::cout<<std::endl;
}

bool UDPPacket::DecodePacket() {
  
  //Get sequence number
  //uint32_t num = get32(m_intbuf);
  //std::cout<<"Sequence number:"<<num<<std::endl;
    
  if (!getKernelInfos()) {
    std::cout<<"Could not decode the kernel info from the packet"<<std::endl;
    return false;
  }


  if (!getThreadInfos()) {
    std::cout<<"Could not decode the thred info from the packet"<<std::endl;
    return false;
  }
  
  m_netBlocks = get32(m_intbuf);
  
  //std::cout<<"netBlocks:"<<m_netBlocks<<std::endl;
  
  for (uint32_t i = 0 ; i< m_netBlocks ; i ++){
    if (!getNetInfos()) {
      std::cout<<"Could not decode Net info from the packet"<<std::endl;
      return false;
    }
  }
  
  if (!getMallInfos()) {
    std::cout<<"Could not decode malloc info from the packet"<<std::endl;
    return false;
  }

  //Semaphores 
  m_semaphores = get32(m_intbuf);
  
  return true;
  
}



void UDPPacket::DumpUDPPacket(){
  

  const char separator = ' ';
  const int width = 12;
  std::cout<<"kernel ticks:" <<m_ticks<<std::endl;
  std::cout<<"result:"<<m_result<<std::endl;
  std::cout<<"SCHEDULED HISTORY SIZE"<<m_schedHist<<std::endl;
  
  //for (uint32_t i = 0 ; i<m_schedHist_vec.size(); i++) {
  //std::cout<<"Scheduler History:"<<m_schedHist_vec[i]<<std::endl;
  //}

  std::cout<<"nproc:"<<m_nproc<<std::endl;
  std::cout<<"m_threadNames size:"<<m_threadNames.size()<<std::endl;
  
  
  //for (uint8_t i=0; i<m_threadNames.size(); i++)
  //std::cout<<"Thread:"<<m_threadNames[i]<<std::endl;
  
  std::cout<<std::left<<std::setw(width)<<std::setfill(separator)
	   <<"Thread"
	   <<std::left<<std::setw(width)<<std::setfill(separator)
	   <<"PID"
	   <<std::left<<std::setw(width)<<std::setfill(separator)
	   <<"State"
	   <<std::left<<std::setw(width)<<std::setfill(separator)
	   <<"aticks"
	   <<std::left<<std::setw(width)<<std::setfill(separator)
	   <<"Priority"
	   <<std::left<<std::setw(width)<<std::setfill(separator)
	   <<"Stack Addr"
	   <<std::left<<std::setw(width)<<std::setfill(separator)
	   <<"Stack Size"
	   <<std::left<<std::setw(width)<<std::setfill(separator)
	   <<"Stack Ptr"
	   <<std::left<<std::setw(width)<<std::setfill(separator)
	   <<std::endl;
  for (uint8_t i=0 ; i<m_nproc; i++)
    std::cout<<std::left<<std::setw(width)<<std::setfill(separator)
	     <<m_threadNames[i]<<std::left<<std::setw(width)<<std::setfill(separator)
	     <<m_pids[i]<<std::left<<std::setw(width)<<std::setfill(separator)
	     <<(int)m_processStates[i]<<std::left<<std::setw(width)<<std::setfill(separator)
	     <<(int)m_aticks[i]<<std::left<<std::setw(width)<<std::setfill(separator)
      	     <<(int)m_priorities[i]<<std::left<<std::setw(width)<<std::setfill(separator)
	     <<std::hex<<m_stackaddrs[i]<<std::left<<std::setw(width)<<std::setfill(separator)
	     <<m_stacksizes[i]<<std::left<<std::setw(width)<<std::setfill(separator)
	     <<m_stackptrs[i]<<std::dec<<std::endl;
  
 
  
  for (uint32_t i = 0; i<m_netBlocks; i++)
    DumpNetBlock(i);
  
  printElement("arena");
  printElement(m_arena);
  std::cout<<std::endl;
  printElement("uordblks");
  printElement(m_uordblks);
  std::cout<<std::endl;
  printElement("fordblks");
  printElement(m_fordblks);
  std::cout<<std::endl;
  printElement("keepcost");
  printElement(m_keepcost);
  std::cout<<std::endl;
  
  printElement("semaphores");
  printElement(m_semaphores);
  std::cout<<std::endl;


  
  
}


void UDPPacket::DumpNetBlock(uint32_t i) {

  
  
  if (i==0)
    {
      printElement("");
      printElement("xmit");
      printElement("recv");
      printElement("fw");
      printElement("drop");
      printElement("chkerr");
      printElement("lenerr");
      printElement("memerr");
      printElement("rterr");
      printElement("proterr");
      printElement("opterr");
      printElement("err");
      printElement("cachehit");
      std::cout<<std::endl;
    }
  
      
  //TO DO: remove hardcodes!
  
  if (i==0) 
    std::cout<<"link"<<std::endl;
  else if (i==1)
    std::cout<<"tcp"<<std::endl;
  else if (i==2)
    std::cout<<"udp"<<std::endl;
	       
  printElement("");
  printElement(m_xmit[i]);
  printElement(m_recv[i]);
  printElement(m_fw[i]);
  printElement(m_drop[i]);
  printElement(m_chkerr[i]);
  printElement(m_lenerr[i]);
  printElement(m_memerr[i]);
  printElement(m_rterr[i]);
  printElement(m_proterr[i]);
  printElement(m_opter[i]);
  printElement(m_err[i]);
  printElement(m_cachehit[i]);
  std::cout<<std::endl;
}
