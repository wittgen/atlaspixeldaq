#include "PixThread.h"


PixThread::PixThread() {  
}

void PixThread::join(){
  m_thread.join();
}

void PixThread::detach() {
  m_thread.detach();
}

void PixThread::start() {
  
  m_thread = boost::thread(&PixThread::setupAndlaunch,this);
}


void PixThread::setupAndlaunch() {
  m_threadId = boost::this_thread::get_id();
  ERS_DEBUG(1, "Started IS thread with ID" << m_threadId);
  std::cout<<"COUT::Started IS thread with ID" << m_threadId<<std::endl;
  loop();
}

boost::thread* PixThread::getThread(){

  return &m_thread;
}

boost::thread::id PixThread::getThreadId() {
  return m_threadId;
}



