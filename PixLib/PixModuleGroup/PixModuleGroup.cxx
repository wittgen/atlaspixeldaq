/////////////////////////////////////////////////////////////////////
// PixModuleGroup.cxx
// version 1.0.2
/////////////////////////////////////////////////////////////////////
//
// 08/04/03  Version 1.0 (PM)
//           Initial release
// 14/04/03  Version 1.0.1 (CS)
//           Added Configuration DataBase
// 16/04/03  Version 1.0.2 (PM)
//           TPLL interface
// 10/10/05  Version 1.0.3 (NG, PM)
//           Scan Implementation

// #define __MCC_TEST__ //  Comment out for regular operation !!!!

#include <sys/stat.h>

#include <ipc/partition.h>
#include "RodCrate/RodStatus.h"
#include "Histo/Histo.h"
#include "PixModuleGroup/PixModuleGroup.h" //JDM
#include "PixDbInterface/PixDbInterface.h"
#include "PixController/PixController.h"
#include "PixController/PixScan.h"
#include "PixModule/PixModule.h"
#include "PixBoc/PixBoc.h"
#include "PixBoc/IblPixBoc.h"
#include "PixFe/PixFe.h"
#include "PixFe/PixFeI3.h"
#include "PixFe/PixFeI4.h"
#include "PixMcc/PixMcc.h"
#include "PixConnectivity/RodBocConnectivity.h"
#include "PixConnectivity/OBConnectivity.h"
#include "PixConnectivity/Pp0Connectivity.h"
#include "PixConnectivity/PartitionConnectivity.h"
#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "PixDcs/PixDcs.h"
#include "PixUtilities/PixISManager.h"
#include "PixUtilities/PixRcCommander.h"

#include "PixFitServer/PixFitFitter_lmfit.h"

#include "RunControl/Common/UserExceptions.h"

#include <algorithm>

using namespace SctPixelRod;
using namespace PixLib;

void ModGrpConfig::subConfRead(std::map< std::string, std::string >fields, std::map<std::string, Config *>&subConf) {
  for (auto rec : fields) {
    std::cout << rec.first << " - " << rec.second << std::endl;
  }
  m_grp.initController();
  if (m_grp.getPixController() != NULL) {
    subConf[m_grp.getName()+"_ROD"] = &(m_grp.getPixController()->config());
  }
  if (m_grp.getPixBoc() != NULL) {
    subConf[m_grp.getName()+"_BOC"] = m_grp.getPixBoc()->getConfig();
  }
}

std::string elementPMG(std::string str, int num, char sep) {
  unsigned int ip;
  int start = -1,end = -1;
  int nel = -1;
  bool spc = true;
  bool quote = false;
                                                                                                                                        
  for (ip=0;ip<str.size();ip++) {
    if (str[ip] == '"') quote = !quote;
    if (str[ip] == sep || (sep == 0 && 
       (str[ip] == 9 || str[ip]==10 || str[ip] == 32))) {
      if (!spc && !quote) {
        spc = true;
        if (nel==num) end = ip-1;
      }
    } else {
      if (spc) {
        spc = false;
        nel++;
        if (nel==num) start = ip;
      }
    }
  }
  if (!spc && nel==num) end = str.size()-1;
                                                                                                                                        
  if (start >= 0) {
    std::string c;
    std::string s = str.substr(start,end-start+1);
    for (ip=0;ip<s.size();ip++) {
      if (s[ip] != '"') c += s[ip];
    }
    return c;
  } else {
    return "";
  }
}

int PixModuleGroup::translateMaskSteps(int mask_step_mode){
  int ntot_mask;
  switch(mask_step_mode){ // define total no. of mask steps
  case PixScan::STEPS_1:
    ntot_mask = 1;
    break;
  case PixScan::STEPS_1_DC:
    ntot_mask = 1;
    break;
  case PixScan::STEPS_2_DC:
    ntot_mask = 2;
    break;
  case PixScan::STEPS_4_DC:
    ntot_mask = 4;
    break;
  case PixScan::STEPS_8_DC:
    ntot_mask = 8;
    break;
  case PixScan::STEPS_8_4DC:
    ntot_mask = 8;
    break;
  case PixScan::STEPS_8_8DC:
    ntot_mask = 8;
    break;
  case PixScan::STEPS_32:
    ntot_mask = 32;
    break;
  case PixScan::STEPS_64:
    ntot_mask = 64;
    break;
  default:
    ntot_mask = 1;
    break;
  }	      
  return ntot_mask;
}

PixModuleGroup::PixModuleGroup(std::string name, CtrlType type, bool bocAvailable) :
  m_dbn(NULL), m_name(name), m_dbRecord(NULL), m_rodConn(NULL),
  m_dcs(NULL) {
  m_createIf = false; 
  initConfig();
  m_ctrlType = type;
  m_bocAvailable = bocAvailable;
  m_name = name;
  // Look for Pixel Controller
  m_modules.clear();
  m_modConn.clear();
  for (int ix=0; ix<32; ix++) m_modIdx[ix] = -1;
  m_pixBoc = NULL;
  m_pixCtrl = NULL;
  m_rodName = m_name+"_ROD";
  if (getCtrlType() == IBLROD) {
    m_pixCtrl = PixController::make(*this, "PPCppRodPixController");
  } else if (getCtrlType() == CPPROD) {
    m_pixCtrl = PixController::make(*this, "PPCppRodPixController");
    if(m_bocAvailable) m_pixBoc = new IblPixBoc(*this); // Todo: fix NKtodo
    // Todo: check m_bocAvailable flag
    // For now, if boc != NULL, ViSet hangs
  } else if (getCtrlType() == L12ROD) {
    m_pixCtrl = PixController::make(*this, "BarrelRodPixController");
    if (m_bocAvailable) m_pixBoc = new PixBoc(*this);
  } else if (getCtrlType() != UNKNOWN) {
    m_pixCtrl = PixController::make(*this, "PPCppRodPixController");
    m_pixBoc = 0;
    if(m_bocAvailable) m_pixBoc = new PixBoc(*this);
  }
  m_localConnectivity = false;
  m_is = NULL;
  m_isPixelDDC = NULL;
  const char* partitionName = getenv("TDAQ_PARTITION");
  if(partitionName)     {
    m_is = new PixISManager(partitionName);
    m_isPixelDDC = new PixISManager(partitionName,"DDC"); 
  }
  m_dtIs = NULL;
  m_conn = NULL;
  m_connGlob = NULL;
  m_cachedCopy = false;
  m_active = false;
  m_cfgChanged = false;
  m_visetUpdated = false; 
  m_feFlav = 0;
  m_rodDisabled = true;
  m_rodDisableReason="";
}

void PixModuleGroup::initController() {
  if (getCtrlType() == IBLROD) {
    m_pixCtrl = PixController::make(*this, "PPCppRodPixController");
    m_pixBoc =0;
    //if (m_bocAvailable) m_pixBoc = new PixBoc(*this); // NKtodo IBLBOC has to be implemented here
  } else if (getCtrlType() == CPPROD) {
    m_pixCtrl = PixController::make(*this, "PPCppRodPixController");
    //Forcing m_bocAvailable=true. Todo: fix this properly and understand why this is necessary
    m_bocAvailable = true;
    if(m_bocAvailable) m_pixBoc = new IblPixBoc(*this);
    // For now, if boc != NULL, ViSet hangs
  } else if (getCtrlType() == L12ROD) {
    m_pixCtrl = PixController::make(*this, "BarrelRodPixController");
    m_bocAvailable = true;
    m_pixBoc = new PixBoc(*this);
  } else {
    m_pixCtrl = PixController::make(*this, "PPCppRodPixController");
    m_pixBoc = new PixBoc(*this);
    m_bocAvailable = true;
  }
  m_pixCtrl->config().name(m_name + "_ROD");
  if (m_bocAvailable) m_pixBoc->getConfig()->name(m_name + "_BOC");
}

PixModuleGroup::PixModuleGroup(PixConnectivity *pixc, std::string name, std::string partitionName) :
  m_dbn(NULL), m_name(name), m_dbRecord(NULL), 
  m_conn(pixc), m_rodConn(NULL), m_dcs(NULL) {
  m_localConnectivity = false;
  m_cachedCopy = false;
  m_createIf = true; 
  m_is = NULL;
  m_isPixelDDC = NULL;
  if (partitionName != ""){
    m_is = new PixISManager(partitionName);
    m_isPixelDDC = new PixISManager(partitionName,"DDC");
  }
  m_dtIs = NULL;
  m_connGlob = m_conn;
  initDb();
  m_cfgChanged = false;
  m_visetUpdated = false;
  m_rodDisabled = true;
  m_rodDisableReason="";
}

PixModuleGroup::PixModuleGroup(PixConnectivity *pixc, std::string name) :
  m_dbn(NULL), m_name(name), m_dbRecord(NULL), 
  m_conn(pixc), m_rodConn(NULL), m_dcs(NULL) {
  m_localConnectivity = false;
  m_cachedCopy = true;
  m_createIf = false; 
  m_is = NULL;
  m_isPixelDDC = NULL;
  m_dtIs = NULL;
  m_connGlob = m_conn;
  initDb();
  m_cfgChanged = false;
  m_visetUpdated = false;
  m_rodDisabled = true;
  m_rodDisableReason="";
}

/////////////// new method (VD)
PixModuleGroup::PixModuleGroup(PixConnectivity *pixc, PixDbServerInterface *dbs, std::string name) :
  m_dbn(NULL), m_name(name), m_dbRecord(NULL), 
  m_conn(pixc), m_rodConn(NULL), m_dcs(NULL) {
  m_dbserverI=dbs;
  m_localConnectivity = false;
  m_cachedCopy = true;
  m_createIf = false; 
  m_is = NULL;
  m_isPixelDDC = NULL;
  m_dtIs = NULL;
  m_connGlob = m_conn;
  initDbServer();
  m_cfgChanged = false;
  m_visetUpdated = false;
  m_rodDisabled = true;
  m_rodDisableReason="";
}

PixModuleGroup::PixModuleGroup(PixConnectivity *pixc, PixDbServerInterface *dbs, PixDcs *dcs, 
			       std::string name, std::string partitionName) :      // T: this is the constructor called in PixActionsSingleROD !
  m_dbn(NULL), m_name(name), m_dbRecord(NULL), 
  m_conn(pixc), m_rodConn(NULL), m_dcs(dcs) {
  m_dbserverI=dbs;
  m_localConnectivity = false;
  m_cachedCopy = true;
  m_createIf = true; 
  m_is = NULL;
  m_isPixelDDC = NULL;
  if (partitionName != ""){
    m_is = new PixISManager(partitionName);
    m_isPixelDDC = new PixISManager(partitionName,"DDC");
  }
  m_dtIs = NULL;
  m_connGlob = m_conn;
  initDbServer();
  m_cfgChanged = false;
  m_visetUpdated = false;
  m_rodDisabled = true;
  m_rodDisableReason="";
}
///////////////////////////////




PixModuleGroup::PixModuleGroup(PixConnectivity *pixc, std::string name, PixDcs *dcs, std::string partitionName) :
  m_dbn(NULL), m_name(name), m_dbRecord(NULL), 
  m_conn(pixc), m_rodConn(NULL), m_dcs(dcs) {
  m_cachedCopy = false;
  m_createIf = true; 
  m_is = NULL;
  m_isPixelDDC = NULL;
  if (partitionName != ""){
    m_is = new PixISManager(partitionName);
    m_isPixelDDC = new PixISManager(partitionName,"DDC");
  }
  m_dtIs = NULL;
  m_connGlob = m_conn;
  initDb();
  m_cfgChanged = false;
  m_visetUpdated = false;
  m_rodDisabled = true;
  m_rodDisableReason=""; 
}

void PixModuleGroup::initDb() {
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  // Read database fields
  initConfig();
  // Look for Pixel Controller
  m_modules.clear();
  m_modConn.clear();
  for (int ix=0; ix<32; ix++) m_modIdx[ix] = -1;
  m_pixBoc = NULL;
  m_pixCtrl = NULL;
  
  if (m_connGlob != NULL) {
    // Get the configuration record from the config DB
    if (m_connGlob->rods.find(m_name) == m_connGlob->rods.end()) {
      throw(PixModuleGroupExc(PixModuleGroupExc::ERROR, "RODBOCNOTFOUD", "RODBOC "+m_name+" not found in connectivity DB"));
    } else {
      m_conn = new PixConnectivity(m_connGlob, m_name);
      m_rodConn = m_conn->rods[m_name];
      m_dbRecord = m_conn->getCfg(m_name);
      m_dbn = m_dbRecord->getDb();
    }

    if (m_dbRecord != NULL) {
      // Read the config object
      m_config->read(m_dbRecord); 
      // Read ROD and BOC
      m_rodName = m_name+"_ROD";
      for(dbRecordIterator it = m_dbRecord->recordBegin(); it != m_dbRecord->recordEnd(); it++) {
	// Look for RodBoc records
	m_pixCtrl = PixController::make(*this, *it, (*it)->getClassName());
	if ((*it)->getClassName() == "PixBoc") {
	  m_pixBoc = new PixBoc(*this, *it);
	}
      }
      if (!m_pixCtrl) {
	throw PixModuleGroupExc(PixModuleGroupExc::FATAL, "NOPIXCTRL", "Missing pix controller. Probable reason : configuration database corruption.");
      }
      // set ROD VME slot from connectivity
      ConfObj &obj=m_pixCtrl->config()["general"]["Slot"];
      if(obj.name()!="__TrashConfObj__")
	static_cast<ConfInt&>(obj).valueInt() = m_rodConn->vmeSlot();
    }
    // Load modules from connectivity
    updateFromConn();
  }
}




////////// new method (VD)
void PixModuleGroup::initDbServer() {
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  // Read database fields
  initConfig();
  // Look for Pixel Controller
  m_modules.clear();
  m_modConn.clear();
  for (int ix=0; ix<32; ix++) m_modIdx[ix] = -1;
  m_pixBoc = NULL;
  m_pixCtrl = NULL;
  std::string globalTag = m_connGlob->getCfgTag();
  std::string domain = "Configuration-"+m_connGlob->getConnTagOI();
  std::string modTag = m_connGlob->getModCfgTag();
  
  if (m_connGlob->rods.find(m_name) == m_connGlob->rods.end()) {
    throw(PixModuleGroupExc(PixModuleGroupExc::ERROR, "RODBOCNOTFOUD", "RODBOC "+m_name+" not found in connectivity DB"));
  } else {
    m_conn = new PixConnectivity(m_connGlob, m_name);
    m_rodConn = m_conn->rods[m_name];
  }
  

  // Read the config object
  if(!m_config->read(m_dbserverI, domain , globalTag, m_name)) 
    throw  PixModuleGroupExc(PixModuleGroupExc::ERROR, "FAILTOLOADCONFIG", " Problem in reading configuration from DbServer");
  
  // Read ROD and BOC
  m_rodName = m_name+"_ROD";
  if (getCtrlType() == IBLROD) {
    m_pixCtrl = PixController::make(*this, m_dbserverI, domain, globalTag, "PPCppRodPixController");
    //if(m_bocAvailable) m_pixBoc = new PixBoc(*this, m_dbserverI, domain, globalTag); // NKtodo IBLBOC has to be implemented here
  } else if (getCtrlType() == CPPROD) {
    m_pixCtrl = PixController::make(*this, m_dbserverI, domain, globalTag, "PPCppRodPixController");
    //m_bocAvailable was already forced to True. Todo: fix this properly and understand why this forcing it to true is needed.
    if(m_bocAvailable) m_pixBoc = new IblPixBoc(*this, m_dbserverI, domain, globalTag);
  } else if (getCtrlType() == L12ROD) {
    m_pixCtrl = PixController::make(*this, m_dbserverI, domain, globalTag, "BarrelRodPixController");
    if (m_bocAvailable) m_pixBoc = new PixBoc(*this, m_dbserverI, domain, globalTag);
  } else {
    m_pixCtrl = PixController::make(*this, m_dbserverI, domain, globalTag, "PPCppRodPixController");
    m_pixBoc = 0;
    //if(m_bocAvailable) m_pixBoc = new PixBoc(*this, m_dbserverI, domain, globalTag);
  }

  
  if (!m_pixCtrl)
    throw PixModuleGroupExc(PixModuleGroupExc::FATAL, "NOPIXCTRL", "Missing pix controller. Probable reason : configuration database corruption.");
  
  // set ROD VME slot from connectivity
  ConfObj &obj=m_pixCtrl->config()["general"]["Slot"];
  if(obj.name()!="__TrashConfObj__")
    static_cast<ConfInt&>(obj).valueInt() = m_rodConn->vmeSlot();
  

  // Load modules from connectivity
  // Save the old module arrays
  std::vector<PixModule*>oldMod = m_modules;
  std::vector<ModuleConnectivity*>oldModConn = m_modConn;
  m_modules.clear();
  m_modConn.clear();
  for (int ix=0; ix<32; ix++) m_modIdx[ix] = -1;

  // Load PixModules from OB/PP0 info
  for (int i=0; i<4; i++) {
    if (m_rodConn->obs(i) != NULL) {
      // std::cout << " found optoboard in slot " << i << std::endl;
      if (m_rodConn->obs(i)->pp0() != NULL) {
       // std::cout << " found pp0 in optoboard  " << i << std::endl;
	for (int j=0; j<=8; j++) {
	  if (m_rodConn->obs(i)->pp0()->modules(j) != NULL) {
	    std::string modName = m_rodConn->obs(i)->pp0()->modules(j)->name();
	    //	    std::cout << " ...calling new PixModule on mod " << modName << std::endl;  
	    {
	      // Check if the module has been already created
	      bool exists=false;
	      for (unsigned int k=0; k<oldModConn.size(); k++) {
		if (oldModConn[k]->name() == m_rodConn->obs(i)->pp0()->modules(j)->name()) {
		  if (oldModConn[k]->modId>=0 && oldModConn[k]->modId<32) m_modIdx[oldModConn[k]->modId] = m_modules.size();
		  m_modules.push_back(oldMod[k]);
		  m_modConn.push_back(oldModConn[k]);
		  exists = true;
		  break;
		}
	      }
	      if (!exists) {
		EnumMCCflavour::MCCflavour mccFlv = EnumMCCflavour::PM_NO_MCC;
		EnumFEflavour::FEflavour feFlv   = EnumFEflavour::PM_NO_FE;
		int nFe=0;
		m_rodConn->obs(i)->pp0()->modules(j)->getModPars(mccFlv, feFlv, nFe);
	        PixModule *tempModule;
		tempModule = new PixModule(m_dbserverI, this, domain, modTag, m_rodConn->obs(i)->pp0()->modules(j)->prodId(), mccFlv, feFlv, nFe);
		int id = m_rodConn->obs(i)->pp0()->modules(j)->modId;
		if (id>=0 && id<32) m_modIdx[id] = m_modules.size();
		m_modules.push_back(tempModule);      
		m_modConn.push_back(m_rodConn->obs(i)->pp0()->modules(j));
		tempModule->m_moduleId = m_rodConn->obs(i)->pp0()->modules(j)->modId;
		tempModule->m_groupId  = m_rodConn->obs(i)->pp0()->modules(j)->groupId;
	      }
	    }
	  }
	}
      }
    }
  }
  // Now delete modules which are no longer used
  for (unsigned int i=0; i<oldModConn.size(); i++) {
    bool exists = false;
    for (unsigned int j=0; j<m_modConn.size(); j++) {
      if (m_modConn[j]->name() == oldModConn[i]->name()) exists = true;
    }
    if (!exists) {
      delete oldMod[i];
    }
  }
  // Must re-initialize the ROD here
  if (m_hwInit) {
    // Initialize the PixController
    m_pixCtrl->initHW();
  }  
  // Set the default speed
  setReadoutSpeed(m_readoutSpeed);
  // Update PixModule parameters from conn DB
  updateModParFromConn(); 
  if (m_modules.size() != 0 ) {
    m_feFlav = m_modules[0]->feFlavour();   // T: FE-flavour information loaded
  } 
}



PixModuleGroup::~PixModuleGroup() 
{
  // Delete Pixel Controller
  if (m_pixCtrl != NULL) delete m_pixCtrl;
  
  // Delete modules
  moduleIterator m, mEnd=m_modules.end();
  for(m=m_modules.begin(); m!=mEnd; m++) delete *m;
  m_modules.clear();

  if(m_dcs) {
    delete m_dcs;
  }  

  // Delete RodBocConnectivity if local
  if (m_localConnectivity) delete m_rodConn;
  m_modConn.clear();

  // delete ISServer
  if(m_is) delete m_is;

}


void PixModuleGroup::initHW() 
{
  updateDCSDisableMap();
  // Initialize the PixController
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<", at line: "<<__LINE__<<std::endl;
  m_pixCtrl->initHW();
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<", at line: "<<__LINE__<<std::endl;
  // Initialize the Boc
  if (m_pixBoc){
    std::cout<<"in function: "<<__PRETTY_FUNCTION__<<", at line: "<<__LINE__<<std::endl;
    updateBOCLink();
    m_pixBoc->BocConfigure();
  }

  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<", at line: "<<__LINE__<<std::endl;
  m_hwInit = true;
  m_configSent = false;

  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<", at line: "<<__LINE__<<std::endl;
}

void PixModuleGroup::resetRod()
{
  std::cout << __PRETTY_FUNCTION__ << std::endl;
  // Initialize the PixController
  initHW();
  if(m_pixBoc)m_pixBoc->BocConfigure();
  m_configSent = false;
  // Set Viset (only the first time)
  //Avoid resetViset while resetROD
  //if (!m_visetUpdated) resetViset();
}

void PixModuleGroup::configureBoc()
{
  std::cout << __PRETTY_FUNCTION__ << std::endl;
  // Initialize the Boc
  if (m_pixBoc) m_pixBoc->BocConfigure();
  m_hwInit = true;
}

void PixModuleGroup::testHW() {
	std::cout << __PRETTY_FUNCTION__ << std::endl;
}

void PixModuleGroup::init() {
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	
	
  initHW();
  downloadConfig();
  configure();
}

bool PixModuleGroup::disabledFromDCS(std::string modName) {
  if (m_modDisabledDCS.find(modName) == m_modDisabledDCS.end())
    //module not present in the disabled ones
    return false;
  else 
    return true;
}

void PixModuleGroup::updateDCSDisableMap() {
  std::cout << __PRETTY_FUNCTION__ << std::endl;
  m_modDisabledDCS.clear();
  std::string st = "PIX_DisabledModuleList";
  
  if (m_isPixelDDC)
    {
    try {
      
      //if (m_isPixelDDC->exists("PIX_DisabledModuleList")) {
      std::string isValue = m_isPixelDDC->readDcs<std::string>(st.c_str());
      std::string word;
      for (unsigned int i=0; i<isValue.size(); i++) {
	if (isValue[i] == ',') {
	  if ((word.size() > 0)) {
	    if (word.find("LI")==std::string::npos)  //skip IBL modules for the moment
	      m_modDisabledDCS[word] = word;
	  }
	  word = "";
	} else {
	  word += isValue[i];
	}
      }
      if(word.size()){ m_modDisabledDCS[word] = word; std::cout<<"Disable module "<<word<<std::endl;}
    }
    catch (...) {
      ers::error(PixLib::pix::daq (ERS_HERE, "NO_DISDCS","PixModuleGroup could not read DCS disabled module list"));
    }
  }
  
    for (unsigned int pmod=0; pmod<m_modules.size(); pmod++)
       if(disabledFromDCS(m_modConn[pmod]->name())) setModuleActive(m_modConn[pmod]->modId, false);
  
}

void PixModuleGroup::configure(){ // set current configuration into the actual phisical modules
	std::cout << __PRETTY_FUNCTION__ << std::endl;
}


void PixModuleGroup::resetViset(float val)
{ //! reset Viset to the values in the BOC configuration for each optoboard
  //  std::cout << "In PixModuleGroup::resetViset()" << std::endl;
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  std::string rodName =  m_rodConn->name();
  
#ifdef __MCC_TEST__
  m_visetUpdated = true;
  return;
#endif

  if( m_is->exists("PIX_ResetVisetDisabled") ) {
    if( m_is->read<bool>("PIX_ResetVisetDisabled") == true ) {
      std::string msg = "Skipping resetViset as requested by RunParams.PIX_ResetVisetDisabled";
      ers::warning(PixLib::pix::daq (ERS_HERE, rodName, msg));
      m_visetUpdated = true;
      return;
    }
  }

  if(getCtrlType() == CPPROD) return;//Just avoid for IBL
  if (m_pixBoc==0) return; // can't process w/o a BOC

  //   PixDcsDefs::DataPointType variableReal = PixDcsDefs::VISET_MON;
  //   PixDcsDefs::DataPointType variableSet = PixDcsDefs::VISET;

  for (unsigned int  ob=0;ob<4;ob++){   
    //    int readValue = 10;
    //    float valReal = -1, valSet = -1;
    if(m_rodConn->obs(ob) != NULL){
      if ((m_rodConn->obs(ob)->pp0() != NULL)){
	int plugin[2];
	float viset[2] = {0., 0.};
	for (int irx = 0; irx < 2; irx++){
	  plugin[irx] = m_rodConn->obs(ob)->bocRxSlot(irx);
	  if (plugin[irx] >=0 && plugin[irx] < 4) {
	    viset[irx] = m_pixBoc->getRx(plugin[irx])->getViset();
	  }
	}

        if (val > 0.0) {
	  viset[0] = val;
          viset[1] = val;
	}
	if (viset[0] != viset[1] && viset[0] != 0 && viset[1] != 0) {
	  std::stringstream info;
	  info << "Two plugins of the same PP0 have different Viset values in the configuration: "
	       << viset[0] << " and " << viset[1] << " I take the first one! "; 
	  ers::warning(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), info.str()));
	}
	std::string pname = m_rodConn->obs(ob)->pp0()->name();
	// PixIs	readDDC(pname, variableReal, valReal);
	// 	readDDC(pname, variableSet, valSet);
	// 	std::cout << "Read values for " << rodName << ", plugin " << plugin[0] << ": Vmon " 
	// 		  << valReal << ", Vset " << valSet << ", Viset " << viset[0] << std::endl; 
	// 	if( (valReal == 0 && valSet != viset[0]) 
	// 	    || (fabs(valReal-viset[0]) > 0.005) ) {
	PixDcsDefs::DDCCommands command = PixDcsDefs::SET_VISET;
	sendDDCcommand(pname, viset[0], command);
	// 	} else {
	// 	  std::cout << "PP0 " << pname << " has already the correct ViSet! " << std::endl;
	// 	}
      }
    }
  }
  m_visetUpdated = true;
}


void PixModuleGroup::DCSsync() 
{ //! disable in DCS all modules disabled in DAQ and enable the enabled ones. Redundant? Can one assume that everything is enabled by default?
	std::cout << __PRETTY_FUNCTION__ << std::endl;

  std::string toBeDisabled = "";
  std::string toBeEnabled = "";

  for (int i=0; i<4; i++) {
    bool pp0Used = false;
    std::string tmpDis = "";
    std::string tmpEn = "";
    if (m_rodConn->obs(i) != NULL) {
      if (m_rodConn->obs(i)->pp0() != NULL) {
	for (int j=0; j<=8; j++) {
	  if (m_rodConn->obs(i)->pp0()->modules(j) != NULL) {
	    if (getModuleActive(m_rodConn->obs(i)->pp0()->modules(j)->modId)) {
	      pp0Used = true;
	      if (tmpEn == "") {
		tmpEn = m_rodConn->obs(i)->pp0()->modules(j)->name();
	      }
	      else {
		tmpEn += " " + m_rodConn->obs(i)->pp0()->modules(j)->name();
	      }
	    } 
	    else {
	      if (tmpDis == "") {
		tmpDis = m_rodConn->obs(i)->pp0()->modules(j)->name();
	      }
	      else {
		tmpDis += " " + m_rodConn->obs(i)->pp0()->modules(j)->name();
	      }
	    }
	  } 
	}
	if (pp0Used) {
	  if (toBeEnabled == "") {
	    toBeEnabled = tmpEn;
	  }
	  else {
	    toBeEnabled += " " + tmpEn;
	  }
	  if (toBeDisabled == "") {
	    toBeDisabled = tmpDis;
	  }
	  else {
	    toBeDisabled += " " + tmpDis;
	  }
	}
      }
    }
  }

  if (toBeEnabled != "") {
    try {
      sendFSMcommand(toBeEnabled, PixDcsDefs::ENABLE);
    }
    catch (const PixModuleGroupExc &e){
      std::stringstream info;
      if (e.getId() == "DCSSTATE") {
	info << "PixModuleGroup::DCSsync - "<< toBeEnabled << ": modules cannot be enabled because they are already enabled";
      }
      else {
	info << "Exception is " << e.getId();
      }
      std::string rodName =  m_rodConn->name();
      ers::error(PixLib::pix::daq (ERS_HERE, rodName, info.str()));
    }
    catch (...) {
      std::cout << "Unknown exception caught" << std::endl;
    }
  }
  if (toBeDisabled != "") {
    try {
      sendFSMcommand(toBeDisabled, PixDcsDefs::DISABLE);
    }
    catch (const PixModuleGroupExc &e){
      std::stringstream info;
      if (e.getId() == "DCSSTATE") {
	info << "PixModuleGroup::DCSsync - "<< toBeDisabled << ": modules cannot be disabled because they are already disabled";
      }
      else {
	info << "Exception is " << e.getId();
      }
      std::string rodName =  m_rodConn->name();
      ers::error(PixLib::pix::daq (ERS_HERE, rodName, info.str()));
    }
    catch (...) {
      std::cout << "Unknown exception caught" << std::endl;
    }
  }
}

void PixModuleGroup::optoOnOff(bool switchOn)
{ //! switches on OBs for which at least one module is enabled. Do we want to switch them on even with all modules disabled?
	std::cout << __PRETTY_FUNCTION__ << std::endl;

  std::string pp0Names = "";
  
  for (int i=0; i<4; i++) {
    bool pp0Used = false;
    if (m_rodConn->obs(i) != NULL) {
      if (m_rodConn->obs(i)->pp0() != NULL) {
	for (int j=0; j<=8; j++) {
	  if (m_rodConn->obs(i)->pp0()->modules(j) != NULL) {
	    if (getModuleActive(m_rodConn->obs(i)->pp0()->modules(j)->modId)) {
	      pp0Used = true;
	    }
	  }
	}
	if (pp0Used) {
	  std::string pname = m_rodConn->obs(i)->pp0()->name();
	  if (pp0Names == "") {
	    pp0Names = pname;
	  }
	  else {
	    pp0Names += " " + pname;
	  }
	}
      }
    }
  }

  if (pp0Names != "") {
    PixDcsDefs::FSMCommands command;
    if (switchOn) {
      command = PixDcsDefs::SWITCH_ON_OPTO;
    }
    else {
      command = PixDcsDefs::BACK_TO_STARTED;
    }
    
    try {
      sendFSMcommand(pp0Names, command);
    }
    catch (const PixModuleGroupExc &e){
      if (e.getId() == "DCSSTATE") {
	std::stringstream info;
	if (switchOn) {
	  info << "PixModuleGroup::optoOnOff - "<< pp0Names << " cannot be switched on because they are not in STARTED state. Nothing to be done if the state is already OPTO_ON.";
	}
	else {
	  info << "PixModuleGroup::optoOnOff - "<< pp0Names << " cannot be switched off because they are not in OPTO_ON state. Nothing to be done if the state is already STARTED.";
	}
	std::string rodName =  m_rodConn->name();
	ers::error(PixLib::pix::daq (ERS_HERE, rodName, info.str()));
      }
    }
  }
  else {
    std::stringstream info;
    info << "PixModuleGroup::optoOnOff - No enabled PP0s on this ROD.";
    std::string rodName =  m_rodConn->name();
    ers::warning(PixLib::pix::daq (ERS_HERE, rodName, info.str()));
  }
}

void PixModuleGroup::lvOnOff(bool switchOn)
{//! switches on LV for PP0s in which at least one module is enabled. 
  // IMPORTANT: only use after disabling in DCS the modules you don't want to turn on!
	std::cout << __PRETTY_FUNCTION__ << std::endl;
 
  std::string pp0Names = "";
  
  for (int i=0; i<4; i++) {
    bool pp0Used = false;
    if (m_rodConn->obs(i) != NULL) {
      if (m_rodConn->obs(i)->pp0() != NULL) {
	for (int j=0; j<=8; j++) {
	  if (m_rodConn->obs(i)->pp0()->modules(j) != NULL) {
	    if (getModuleActive(m_rodConn->obs(i)->pp0()->modules(j)->modId)) {
	      pp0Used = true;
	    }
	  }
	}
	if (pp0Used) {
	  std::string pname = m_rodConn->obs(i)->pp0()->name();
	  if (pp0Names == "") {
	    pp0Names = pname;
	  }
	  else {
	    pp0Names += " " + pname;
	  }
	}
      }
    }
  }

  if (pp0Names != "") {
    PixDcsDefs::FSMCommands command;
    if (switchOn) {
      command = PixDcsDefs::SWITCH_ON_LV;
    }
    else {
      command = PixDcsDefs::BACK_TO_OPTO_ON;
    }
    
    try {
      sendFSMcommand(pp0Names, command);
    }
    catch (const PixModuleGroupExc &e){
      if (e.getId() == "DCSSTATE") {
	std::stringstream info;
	if (switchOn) {
	  info << "PixModuleGroup::optoOnOff - "<< pp0Names << " cannot be switched on because they are not in OPTO_ON state. Nothing to be done if the state is already LV_ON.";
	}
	else {
	  info << "PixModuleGroup::optoOnOff - "<< pp0Names << " cannot be switched off because they are not in LV_ON state. Nothing to be done if the state is already OPTO_ON.";
	}
	std::string rodName =  m_rodConn->name();
	ers::error(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), info.str()));
      }
    }
  }
  else {
    std::stringstream info;
    info << "PixModuleGroup::lvOnOff - No enabled PP0s on this ROD.";
    std::string rodName =  m_rodConn->name();
    ers::warning(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), info.str()));
  }
  
}


void PixModuleGroup::readConfig(DbRecord *dbr) {  //! load configuration from the database
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  loadConfig(dbr);
}

void PixModuleGroup::writeConfig(DbRecord *dbr){ // save current configuration into the database
  saveConfig(dbr);
}

void PixModuleGroup::downloadConfig() { // write current configuration into the (possibly present) PixController 
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  writeConfig();
}

void PixModuleGroup::loadConfig(DbRecord *r) {  //! load configuration from the database
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  if (r->getClassName() == "PixModuleGroup") {
    m_dbRecord = r;
    m_name = r->getName();
    // Read the config object
    m_config->read(r); 
    // Read ROD and BOC config
    for(dbRecordIterator it = r->recordBegin(); it != r->recordEnd(); it++) {
      std::string ctrlTypeName = "PPCppRodPixController";
      if (getCtrlType() == IBLROD) {
	ctrlTypeName = "PPCppRodPixController";
      } else if (getCtrlType() == CPPROD) {
	ctrlTypeName = "PPCppRodPixController";
      } else if (getCtrlType() == L12ROD) {
	ctrlTypeName = "BarrelRodPixController";
      } else {
	ctrlTypeName = "PPCppRodPixController";
      }
      // Look for RodBoc records
      if ((*it)->getClassName() == ctrlTypeName && m_pixCtrl != NULL) {
	m_pixCtrl->loadConfig(*it);
      }
      if ((*it)->getClassName() == "PixBoc" && m_pixBoc != NULL) {
	m_pixBoc->loadConfig(*it);
      }
    }
    // Read modules from connectivity
    updateFromConn();
    m_hwInit = false;
    m_configSent = false;
  }
}

void PixModuleGroup::saveConfig(DbRecord *r){ // save current configuration into the database
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  if (r->getClassName() == "PixModuleGroup") {
    m_dbRecord = r;
    // Read the config object
    m_config->write(r); 
    if (m_pixCtrl != NULL) {
      std::string ctrlTypeName = "PPCppRodPixController";
      if (getCtrlType() == IBLROD) {
	ctrlTypeName = "PPCppRodPixController";
      } else if (getCtrlType() == CPPROD) {
	ctrlTypeName = "PPCppRodPixController";
      } else if (getCtrlType() == L12ROD) {
	ctrlTypeName = "BarrelRodPixController";
      } else {
	ctrlTypeName = "PPCppRodPixController";
      }
      // Create child ROD
      DbRecord *sr = r->addRecord(ctrlTypeName, m_pixCtrl->getCtrlName());
      m_pixCtrl->saveConfig(sr);
    }
    if (m_pixBoc != NULL) {
      // Create child BOC
      DbRecord *sr = r->addRecord("PixBoc", m_pixBoc->getCtrlName());
      m_pixBoc->saveConfig(sr);
    }
  }
}

void PixModuleGroup::loadConfig(PixConnectivity *conn, unsigned int rev) {  //! load configuration from the database
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  if (conn != NULL) {
    DbRecord *r;
    if (rev != 0) {
      r = conn->getCfg(m_name, rev);
    } else {
      r = conn->getCfg(m_name);
    }
    loadConfig(r);
  }
}

void PixModuleGroup::saveConfig(PixConnectivity *conn, unsigned int rev) {  //! save configuration to the db
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  if (conn != NULL) {
    DbRecord *r = conn->addCfg(m_name, rev);
    saveConfig(r);
    conn->cleanCfgCache();
  }
}

void PixModuleGroup::writeConfig() { // write current configuration into the (possibly present) PixController 
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  for(auto m = m_modules.begin(); m != m_modules.end(); m++) {
    std::cout << "Writing for ";
    std::cout << *m << std::endl;
    //    (*m)->writeConfig();
    m_pixCtrl->writeModuleConfig(**m);
    std::cout << "Done for ";
    std::cout << *m << std::endl;
  }
  m_configSent = true;
}

PixModule* PixModuleGroup::module(int im) {
  for (unsigned int i=0; i<m_modules.size(); i++) {
    ConfInt &moduleId = static_cast<ConfInt &>(m_modules[i]->config()["general"]["ModuleId"]);
    if (moduleId.value() == im) return m_modules[i];
  }
  return NULL;
}

ModuleConnectivity* PixModuleGroup::modConn(int im) {
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  for (unsigned int i=0; i<m_modules.size(); i++) {
    if (m_modules[i]->m_moduleId == im) return m_modConn[i];
  }
  return NULL;
}

void PixModuleGroup::initConfig(){ 
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  // Create the config object  
  std::string xname = m_name;
  m_config = new ModGrpConfig("PixModuleGroup", *this);
  Config &conf = *m_config;

  // Group Ggeneral
  conf.addGroup("general");

  conf["general"].addString("ModuleGroupName", m_name, "DEF", 
			    "Module Group Name", true);
  conf["general"].addInt("TriggerDelay", m_triggerDelay, 58, 
                         "Global Trigger Delay for in-time threshold scan", true);
  conf["general"].addInt("VCALmin", m_vcalMin, 0,
			   "Min. value for reduced-range threshold scan", true);
  conf["general"].addInt("VCALmax", m_vcalMax, 200,
			   "Max. value for reduced-range threshold scan", true);
  std::map<std::string, int> mccbwMap;
  mccbwMap["SINGLE_40"] = SINGLE_40;
  mccbwMap["DOUBLE_40"] = DOUBLE_40;
  mccbwMap["SINGLE_80"] = SINGLE_80;
  mccbwMap["DOUBLE_80"] = DOUBLE_80;
  conf["general"].addList("readoutSpeed", m_readoutSpeed, SINGLE_40, mccbwMap,
			  "default MCC output bandwidth", true);
  conf["general"].addString("swapInLinks", m_swapInLinks, "-", 
			    "BOC in links swap", true);
  conf["general"].addString("swapOutLinks", m_swapOutLinks, "-", 
			    "BOC out links swap", true);
  std::map<std::string, int> ctrltypeMap;
  ctrltypeMap["DUMMY"]  = DUMMY;
  ctrltypeMap["ROD"]    = ROD;
  ctrltypeMap["IBLROD"]   = IBLROD;
  ctrltypeMap["CPPROD"]   = CPPROD;
  ctrltypeMap["L12ROD"]   = L12ROD;
  ctrltypeMap["USBPIX"] = USBPIX;
  ctrltypeMap["RCE"]    = RCE;
  conf["general"].addList("ctrlType", m_ctrlType, L12ROD, ctrltypeMap,
			  "Controller type", true);
  std::map<std::string, int> fetypeMap;
  fetypeMap["FE_I_1"]  = FE_I_1;
  fetypeMap["FE_I_2"]  = FE_I_2;
  fetypeMap["FE_I_3"]  = FE_I_3;
  fetypeMap["FE_I_4"]  = FE_I_4;
  
  //write default value (FE_I_3)
  conf["general"].addList("feType", m_feType, FE_I_3, fetypeMap,
			  "Front End type", true);
  conf["general"].addBool("BOCavailable", m_bocAvailable, true,
      "Optical boc present", true);

  // Read from DB
  conf.reset();

  if(m_dbRecord != NULL) {
    conf.read(m_dbRecord);
  } else {
    m_name = xname;
  }
  m_hwInit = false;
  m_configSent = false;
  m_curReadoutSpeed = m_readoutSpeed;
  m_localConnectivity = false;
  m_useScanInSwap = false;
  m_useScanOutSwap = false;
  m_useScanModAltLinks = false;
  m_scanSwapInLinks = "-";
  m_scanSwapOutLinks = "-";
  m_scanModAltLinks = 0;
  m_multiName = "--";
  
}

void PixModuleGroup::buildConnectivity() {
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  if (m_rodConn == NULL) {
    // Create local ROD connectivity object
    m_rodConn = new RodBocConnectivity(m_name);
    m_localConnectivity = true;
    // Loop over modules and extract PP0 names
    std::vector<std::string>pp0Names;
    std::vector<int>pp0Slots;
    for (unsigned int pmod=0; pmod<m_modules.size(); pmod++) {
      std::string pp0 = m_modules[pmod]->pp0Name();
      bool found = false;
      for (unsigned int i=0; i<pp0Names.size(); i++) {
	if (pp0Names[i] == pp0) found = true;
      }
      if (!found) {
	pp0Names.push_back(pp0);
	int slot = ((ConfInt&)(m_modules[pmod])->config()["general"]["InputLink"]).getValue()/12;
	pp0Slots.push_back(slot);
      }
    }
    // Create PP0 connectivity objects and link them to the ROD conn
    for (unsigned int i=0; i<pp0Names.size(); i++) {
      OBConnectivity *ob = new OBConnectivity(pp0Names[i]+"_OB");
      Pp0Connectivity *pp0 = new Pp0Connectivity(pp0Names[i]);
      m_rodConn->addOB(pp0Slots[i], pp0Slots[i], -1, ob);  
      ob->addPp0(pp0);  
      // Loop over modules, create Module connectivity objects and link them
      // to PixModule and Pp0Connectivity objects
      for (unsigned int pmod=0; pmod<m_modules.size(); pmod++) {
	if (m_modules[pmod]->pp0Name() == pp0Names[i]) {
	  ModuleConnectivity* mod = new ModuleConnectivity(m_modules[pmod]->connName());
          pp0->addModule(m_modules[pmod]->pp0Pos(), mod);
          m_modConn[pmod] = mod;
          if (mod->modId >= 0 && mod->modId < 32) m_modIdx[mod->modId] = pmod;
	}
      }
    }
  }
  // Dump local connectivity
  std::cout << "Conn RodBoc = " << m_rodConn->name() << std::endl;
  for (int i=0; i<4; i++) {
    if (m_rodConn->obs(i) != NULL) {
      if (m_rodConn->obs(i)->pp0() != NULL) {
	std::cout << "Conn   PP0 = " << m_rodConn->obs(i)->pp0()->name() << std::endl;
	for (int j=0; j<=8; j++) {
	  if (m_rodConn->obs(i)->pp0()->modules(j) != NULL) {
	    std::cout << "Conn     Mod = " << m_rodConn->obs(i)->pp0()->modules(j)->name();
	    std::cout << " Pos " << m_rodConn->obs(i)->pp0()->modules(j)->pp0Slot() << std::endl;
	  }
	}
      }
    }
  }
}

/*
  void PixModuleGroup::setISManager(IPCPartition* part) {
  if(m_ipcPartition == 0){
  m_ipcPartition = new IPCPartition("Partition_PixelDDC");
  PixISManager* is = new PixISManager(m_ipcPartition);
  
  if(is == false){
  std::cout << "WARNING in PixModuleGroup::setISManager, PixISManager not properly initialized in partition "<<m_ipcPartition->name() << std::endl;
  }else{
  m_is = is;
  }
  }
  }
*/

bool PixModuleGroup::isPixelInMaskSteps(int const col, int const row, PixScan *scn){

  int const ntot_mask = translateMaskSteps(scn->getMaskStageTotalSteps()); //Get total mask steps
  int const mask = scn->getMaskStageSteps();    // only scan masked pixels
  int rest_row = row%ntot_mask;                  //  maskTotalSteps;
  return ((col%2==0 && rest_row < mask) || (col%2==1 && rest_row >= ntot_mask - mask));

}

int PixModuleGroup::getCapType( PixScan *scn){

  int capType = 0;
    if(scn->getChargeInjCapHigh() || scn->getMaskStageMode()==PixScan::FEI4_ENA_BCAP) capType = 2;
    else if (scn->getMaskStageMode()==PixScan::FEI4_ENA_LCAP) capType = 1;

  return capType;
}

int PixModuleGroup::getVCALfromCharge(float charge, PixFe* fe, int capType) {
  //calculate vcal for the desired charge   
  // copied from fcal tuning 
  //PixFe* fei4 = dynamic_cast<PixFeI4*>(fe);
  bool isFei4 = fe->isFeI4();
  float vcal_a = static_cast<ConfFloat &>(fe->config()["Misc"]["VcalGradient3"]).value();
  float vcal_b = static_cast<ConfFloat &>(fe->config()["Misc"]["VcalGradient2"]).value();
  float vcal_c = static_cast<ConfFloat &>(fe->config()["Misc"]["VcalGradient1"]).value();
  float vcal_d = static_cast<ConfFloat &>(fe->config()["Misc"]["VcalGradient0"]).value();

  float cInj   = static_cast<ConfFloat &>(fe->config()["Misc"]["CInjLo"]).value();
  if ((!isFei4 && capType) || (isFei4 && capType==2)){
    cInj = static_cast<ConfFloat &>(fe->config()["Misc"]["CInjHi"]).value();
  } else if(isFei4 && capType==1){
    cInj = static_cast<ConfFloat &>(fe->config()["Misc"]["CInjMed"]).value();
  }
  float qBest = 0;
  int vBest = 0;
  for(int v=0; v<1024; v++) {
    float q = getChargefromVCAL(v, vcal_a, vcal_b, vcal_c, vcal_d, cInj);
    if (abs(q-charge) < abs(qBest-charge)) {
      qBest = q;
      vBest = v;
    }
  }
  return vBest;
}

float PixModuleGroup::getChargefromVCAL(float vcal, float vcal_a, float vcal_b, float vcal_c, float vcal_d, float cinj) {
  if (vcal <= 0) return 0;
  return ( 6.241495961*cinj*((((vcal_a)*vcal + vcal_b)*vcal + vcal_c)*vcal + vcal_d) );
  //    std::cout << "  VCAL: " <<vcal << ", CInj: "<<cinj<<", vcal_a: "<<vcal_a<<", vcal_b: "<<vcal_b<<", vcal_c: "<<vcal_c<<", vcal_d: "<<vcal_d<<", CHARGE: " << q << std::endl;
}

float PixModuleGroup::getChargefromVCAL(float vcal, float vcal_c, float vcal_d, float cinj) {
  if (vcal <= 0) return 0;
  return getChargefromVCAL(vcal, 0, 0, vcal_c, vcal_d ,cinj);
  //    std::cout << "  VCAL: " <<vcal << ", CInj: "<<cinj<<", vcal_c: "<<vcal_c<<", vcal_d: "<<vcal_d<<", CHARGE: " << q << std::endl;
}

float PixModuleGroup::getChargefromVCAL(float vcal, PixFe* fe, int capType) {
  //calculate charge from vcal
  if (vcal <= 0) return 0; 

  bool isFei4 = fe->isFeI4();
  float vcal_a = static_cast<ConfFloat &>(fe->config()["Misc"]["VcalGradient3"]).value();
  float vcal_b = static_cast<ConfFloat &>(fe->config()["Misc"]["VcalGradient2"]).value();
  float vcal_c = static_cast<ConfFloat &>(fe->config()["Misc"]["VcalGradient1"]).value();
  float vcal_d = static_cast<ConfFloat &>(fe->config()["Misc"]["VcalGradient0"]).value();

  float cInj   = static_cast<ConfFloat &>(fe->config()["Misc"]["CInjLo"]).value();
  if ((!isFei4 && capType) || (isFei4 && capType==2)){
    cInj = static_cast<ConfFloat &>(fe->config()["Misc"]["CInjHi"]).value();
  } else if(isFei4 && capType==1){
    cInj = static_cast<ConfFloat &>(fe->config()["Misc"]["CInjMed"]).value();
  }

  return getChargefromVCAL(vcal, vcal_a, vcal_b, vcal_c, vcal_d, cInj);
}

void::PixModuleGroup::setCharge(int nloop, PixScan *scn) {
	std::cout << __PRETTY_FUNCTION__ << std::endl;

  for (unsigned int pmod=0; pmod<m_modules.size(); pmod++) {
    bool isFei4 = isFEI4(m_modules[pmod]->feFlavour());
    if (m_modules[pmod]->m_readoutActive) {
      //unsigned int mod = m_modules[pmod]->m_moduleId;
      
      float q_trg = (scn->getLoopVarValues(nloop))[scn->scanIndex(nloop)];
      std::cout<< "Setup injection charges for target value  " << std::endl;
      
      //calculate vcal for the desired charge   
      for (std::vector<PixFe*>::iterator fe = m_modules[pmod]->feBegin(); fe != m_modules[pmod]->feEnd(); fe++){
	PixFe* fei4 = dynamic_cast<PixFeI4*>(*fe);
	float vcal_a = static_cast<ConfFloat &>((*fe)->config()["Misc"]["VcalGradient3"]).value();
	float vcal_b = static_cast<ConfFloat &>((*fe)->config()["Misc"]["VcalGradient2"]).value();
	float vcal_c = static_cast<ConfFloat &>((*fe)->config()["Misc"]["VcalGradient1"]).value();
	float vcal_d = static_cast<ConfFloat &>((*fe)->config()["Misc"]["VcalGradient0"]).value();
	float cInj   = static_cast<ConfFloat &>((*fe)->config()["Misc"]["CInjLo"]).value();
	if ((!isFei4 && scn->getChargeInjCapHigh()) || (isFei4 && scn->getMaskStageMode()==PixScan::FEI4_ENA_BCAP)){
	  cInj = static_cast<ConfFloat &>((*fe)->config()["Misc"]["CInjHi"]).value();
	} else if(isFei4 && scn->getMaskStageMode()==PixScan::FEI4_ENA_LCAP){
	  cInj = static_cast<ConfFloat &>((*fe)->config()["Misc"]["CInjMed"]).value();
	}
	float qBest=0;
	int vBest=0;
	for (int v=0; v<1024; v++) {
	  float q = 6.241495961*cInj*(((vcal_a*v + vcal_b)*v + vcal_c)*v + vcal_d);
	  if (abs(q - q_trg) < abs(qBest-q_trg)) {
	    qBest = q;
	    vBest = v;
	  }
	}
	if(fei4==0)
	  dynamic_cast<PixFeI3*>(*fe)->writeGlobRegister(Fei3::GlobReg::VCal, vBest);
	else
      dynamic_cast<PixFeI4*>(*fe)->writeGlobRegister(Fei4::GlobReg::PlsrDAC, vBest);

      }
      m_pixCtrl->writeModuleConfig(*(m_modules[pmod]),true);
      //m_modules[pmod]->writeConfig(); 
      
      scn->setFeVCal(0x1fff);   // Rod will not set VCal
      
    }
  }
  m_configSent = true;
}

void PixModuleGroup::endFei4GDACFastFineTuning(int nloop, PixScan *scn, PixHistoServerInterface *hInt, std::string folder) {

  // The histograms from the last two steps are loaded and the tdac value for which the occupancy
  // was closest to 50% is set
  //
  // The available range for tdac settings is 10 to 117
  // Pixel that can't be tuned will be set to 117
  //   criterion here is less than 50% occupancy even at lowest setting
	std::cout<<"Just in the endFei4GDACFastFineTuning"<<std::endl;
  std::stringstream  info;
 	double glob_occ = 0 , glob_events = 0;
 
  // Module loop
  for (unsigned int pmod=0; pmod<m_modules.size(); pmod++) {
    if (m_modules[pmod]->m_readoutActive) {
      unsigned int mod = m_modules[pmod]->m_moduleId;
      PixGeometry geo = m_modules[pmod]->geometry();
      //bool isFei4 =isFEI4(m_modules[pmod]->feFlavour());
      int GDAC_F = 255;      
      Histo *hOcc, *hOccBest, *hGdac; 
			std::cout<<"Getting histos for module: "<<pmod<<std::endl;
      hOcc = &scn->getHisto(PixScan::OCCUPANCY, this, mod, scn->scanIndex(2), scn->scanIndex(1), scn->getLoopVarNSteps(0)-1);
      hOccBest = &scn->getHisto(PixScan::OCCUPANCY, this, mod, scn->scanIndex(2), scn->scanIndex(1)+1, 0);
      hGdac = &scn->getHisto(PixScan::GDAC_T, this, mod, scn->scanIndex(2), scn->scanIndex(1), 0 );
			std::cout<<"GOT THE HISTOS:"<<mod<<std::endl;
      // FE loop
      for (auto fe = m_modules[pmod]->feBegin(); fe != m_modules[pmod]->feEnd(); fe++){
	// do not asjust TDACS of front-ends which are not scan-enabled and
	// for which the dacs are not to be enabled
				Config &cfg = (*fe)->config();
				auto fei4 = dynamic_cast<PixFeI4*>(*fe);
				//PixFeConfig &old_cfg = (*fe)->feConfig("PreScanConfig");
				{ConfObj &cfg_obj = cfg["Misc"]["ScanEnable"];
				if (cfg_obj.type() == ConfObj::BOOL) {
	  		if (static_cast<ConfBool&>(cfg_obj).m_value == false) continue;
				}}
				{ConfObj &cfg_obj = cfg["Misc"]["DacsEnable"];
				if (cfg_obj.type() == ConfObj::BOOL) {
	  			if (static_cast<ConfBool&>(cfg_obj).m_value == false) continue;
				}}
				glob_occ = 0;
				glob_events = 0;
 				ConfMask<unsigned short int> &tdacs = fei4->readTrim(Fei4::PixReg::TDAC);
				ConfMask<bool> pixEnable = fei4->readPixRegister(Fei4::PixReg::Enable);
				for (int col=0; col<tdacs.ncol(); col++) {
	  			for (int row=0; row<tdacs.nrow(); row++) {
	   		 		unsigned int colmod, rowmod;
	   				int ife = (*fe)->number();
	    			PixCoord pc = geo.coord(ife,col,row);
	    			colmod = pc.x();
		    		rowmod = pc.y();
					if ( isPixelInMaskSteps(col, row, scn) ) {
	    	 			  double occ = (*hOcc)(colmod, rowmod);		
	   	  	 			//double occ_best = (*hOccBest)(colmod, rowmod);
	      				  double events = (double)(scn->getRepetitions());	    
					// 	if (occ > events) std::cout<<" glob_occ before: "<<glob_occ<<std::endl;
					  (occ < events) ? glob_occ += occ : glob_occ += events;
					  glob_events += events;
					  (occ < events) ? hOccBest->set(colmod, rowmod, occ) : hOccBest->set(colmod, rowmod, events);
					// if (occ > events) std::cout<< "xxxx OCC: " << occ << "\tevents: " << events  << "\tglob_occ: " << glob_occ << std::endl;
	     				}
						//}
					}
				}
				if(glob_occ/glob_events < 0.5 && glob_events != 0){
					//std::cout<<"xxxx Increasing GDAC, step: "<<(int)scn->getLoopVarValues(nloop)[scn->scanIndex(nloop)]<<std::endl;
					GDAC_F = fei4->readGlobRegister(Fei4::GlobReg::Vthin_Fine);//-(int)scn->getLoopVarValues(nloop)[scn->scanIndex(nloop)];
					if(GDAC_F<0){
						GDAC_F = fei4->readGlobRegister(Fei4::GlobReg::Vthin_Fine)/2;
						std::cout<<"WARNING: trying to set GDAC_F < 0"<<std::endl;
					}
				}
				else if(glob_occ/glob_events >= 0.5 && glob_events != 0){
					//std::cout<<"xxxx Lowering GDAC, step: "<<(int)scn->getLoopVarValues(nloop)[scn->scanIndex(nloop)]<<std::endl;
					GDAC_F =  fei4->readGlobRegister(Fei4::GlobReg::Vthin_Fine);//+(int)scn->getLoopVarValues(nloop)[scn->scanIndex(nloop)];
					if(GDAC_F>255){
						GDAC_F = (fei4->readGlobRegister(Fei4::GlobReg::Vthin_Fine) + 255)/2;
						std::cout<<"WARNING: trying to set GDAC_F > 255"<<std::endl;
					}
				}else{
					//std::cout<<"xxxx Stable GDAC"<<std::endl;
					GDAC_F = fei4->readGlobRegister(Fei4::GlobReg::Vthin_Fine);
				}
				fei4->writeGlobRegister(Fei4::GlobReg::Vthin_Fine, GDAC_F&0xff);
				storeConfig(pmod, "PreScanConfig");
				std::cout<<"xxxxx LOOP over the FEs: "<<(*fe)->number()<<std::endl;
				std::cout<<"xxxxx Global Occ: "<<glob_occ<<"\tGlobal Events:"<<glob_events<<"\tGDAC "<<GDAC_F<<"\tMOD: "<<mod<<std::endl; 

				std::cout<<"BEFORE THE SET OF THE GDAC VALUE"<<std::endl;

				for (int col=0; col<tdacs.ncol(); col++) {
	  			for (int row=0; row<tdacs.nrow(); row++) {
	    			unsigned int colmod, rowmod;
	    			int ife = (*fe)->number();
	    			PixCoord pc = geo.coord(ife,col,row);
	    			colmod = pc.x();
	    			rowmod = pc.y();
	   				hGdac->set(colmod, rowmod, GDAC_F);
					}
				}
			}    
     	m_modules[pmod]->changedCfg(); 

			std::cout<<"About to write and delete histos"<<std::endl;
     	writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::OCCUPANCY, scn->scanIndex(2), scn->scanIndex(1), scn->getLoopVarNSteps(0) - 1 );
      std::cout << "PixModuleGroup::endFei4GDACFineFastTuning succesfully written histo OCCUPANCY " 
		<< scn->scanIndex(2) << "/" << scn->scanIndex(1)  << "/" << scn->getLoopVarNSteps(0)-1 << std::endl;
      writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::GDAC_T, scn->scanIndex(2), scn->scanIndex(1), 0 );
      std::cout << "PixModuleGroup::endFei4GDACFineFastTuning succesfully written histo GDAC_T " 
		<< scn->scanIndex(2) << "/" << scn->scanIndex(1)  << "/" << 0 << std::endl;  
      writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::OCCUPANCY, scn->scanIndex(2), scn->scanIndex(1)+1, 0 );
      std::cout << "PixModuleGroup::endGDACFineFastTuning succesfully written histo OCCUPANCY " 
		<< scn->scanIndex(2) << "/" << scn->scanIndex(1)+1  << "/" << 0 << std::endl;

      if (scn->getCloneModCfgTag()) {
	std::cout << "Writing config to " << pixConnectivity()->getModCfgTag()+scn->getTagSuffix() << std::endl;
	m_modules[pmod]->writeConfig(m_dbserverI, "Configuration-"+pixConnectivity()->getConnTagOI(), pixConnectivity()->getModCfgTag()+scn->getTagSuffix(), "_Tmp");
			
      }
    }   
	}
}

void PixModuleGroup::prepareFei4GDACFineTuning(int nloop, PixScan *scn, PixHistoServerInterface *hInt, std::string folder){


	// gets called before each scan step
	// 	step 0 	- set correct charge injection
	//      	- set GDAC_fine to config file value, or if there isn't any to the middle of the phase space 
	// 	all other steps	- retrieve occupancy histos of previous step 
	//      		- if occ > 50%   Vthin_altFine+step_lenght
	//                        else Vthin_altFine-step_lenght
	//

	std::stringstream info;
	int vBest;
	double glob_occ, glob_occ_best, glob_events;

    // define directory for badPixelLists
    string listDirectory = "/tmp";
    char* path = getenv("PIXSCAN_STORAGE_PATH");
    if (path != NULL) listDirectory = path;
    listDirectory += "/gdacff_badpixlist"; //directory to drop badPixelLists in.
    string cmd = "mkdir -p" + listDirectory;
    system(cmd.c_str()); 				   // TODO, verify that directory is created

    int maximumOccupancy=scn->getRepetitions(); //maximum number of hits possible
	int lowerThresholdShift=0; //lower cut-off = 0 + lTS
	int upperThresholdShift=0; //upper cut-off = maxOcc - uTS

	
	if(!scn->getHistogramKept(PixScan::OCCUPANCY)){
		info<<"PixModuleGroup::prepareFei4GDACFineTuning  Invalid Configuration! Try to keep PixScan::OCCUPANCY histos. ";
		ers::error(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), info.str()));
		throw 42;
	}
	if (!scn->tuningStartsFromCfgValues()) {
	  std::cout<<"Tuning starts from Cfg Value to True: resetting TDAC to 15"<<std::endl;
	}
		
	info<<"Just about to start the scan.";
	ers::info(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), info.str()));
	info.str(std::string());	

	std::cout<<" ----- prepareFei4GDACFineTuning"<<std::endl;	

	int ife, colmod, rowmod;
	//double occ_store, events_store;
	//PixCoord cp;
	int GDAC_F;

	for(unsigned int pmod=0; pmod<m_modules.size(); pmod++){
	// LOOP 0, different from the other loop because of some initial setting
		if (m_modules[pmod]->m_readoutActive) {
		Histo *hOcc=nullptr,  *hOccBest=nullptr,  *hGdac=nullptr;
		PixGeometry geo = m_modules[pmod]->geometry();
		bool isFei4 = isFEI4(m_modules[pmod]->feFlavour());
		unsigned int mod = m_modules[pmod]->m_moduleId;
		if(scn->scanIndex(nloop) == 0){
			std::cout<<"Fei4GDACFineTuning: just in the loop 0"<<std::endl;
			// MODULE LOOP
			//for(unsigned int pmod=0; pmod<m_modules.size(); pmod++){
				// CHECK IF THE MODULE IS ACTIVE
				//if(m_modules[pmod]->m_readoutActive){
					std::cout<<"Loop over the modulei, in module: "<<pmod<<std::endl;
					unsigned int nCol = (unsigned int) m_modules[pmod]->nCol();
					unsigned int nRow = (unsigned int) m_modules[pmod]->nRow();

					std::cout<<"Initialising histos, GDAC_T"<<std::endl;
					hGdac = new Histo("GDAC_T","GDAC_tuning", nCol, -0.5, (float)nCol-0.5f, nRow, -0.5, (float)nRow-0.5f);
					std::cout<<"adding Histo"<<std::endl;				
					scn->addHisto(*hGdac, PixScan::GDAC_T, mod, scn->scanIndex(2), scn->scanIndex(1), 0);
					std::cout<<"Initialising histos, Best Occupancy"<<std::endl;
					hOccBest = new Histo("OCCUPANCY", "Best Occupancy", nCol,-0.5,  (float)nCol-0.5f, nRow, -0.5, (float)nRow-0.5f);
					std::cout<<"adding Histo"<<std::endl;				
					scn->addHisto(*hOccBest, PixScan::OCCUPANCY, mod, scn->scanIndex(2), scn->scanIndex(1)+1, 0);
					std::cout<<"Histos initialised, about to store the config"<<std::endl;
					storeConfig(pmod, "PreScanConfig");
					std::cout<<"CONFIG STORED"<<std::endl;
		
					// FE LOOP
					for(std::vector<PixFe*>::iterator fe = m_modules[pmod]->feBegin(); fe != m_modules[pmod]->feEnd(); fe++){
						ife = (*fe)->number();	
						// CHECK IF FE IS ACTIVE
						if(isFei4 && isFeActive(*fe)){
						   int target = scn->getThresholdTargetValue(m_modules[pmod]->connName() );
						   vBest =  getVCALfromCharge(target, *fe, getCapType(scn));
						   std::cout<<" --------------------------------- PlsrDAC set to: "<<vBest<<std::endl;
                                                   static_cast<PixFeI4*>(*fe)->writeGlobRegister(Fei4::GlobReg::PlsrDAC, vBest);
							//if(!scn->tuningStartsFromCfgValues()){
							//	(*fe)->writeGlobRegister("Vthin_AltFine", 0x7f); // IF you're not starting from config setto middle of the dynamic range
							//	std::cout<<" --------------------------------- Vthin_AltFine set to: "<<0x7f<<std::endl;
							//}
						}
					}
					m_pixCtrl->writeModuleConfig(*(m_modules[pmod]), true);
					std::cout<<"---------------- Module Configuration written"<<std::endl;
					scn->setFeVCal(0x1fff);
					std::cout<<"---------------- Fe Vcal resetted"<<std::endl;
				//}
		//}
		}else{ // get OCCUPANCY of the LAST step

			//if(m_modules[pmod]->m_readoutActive){
				hOccBest = &scn->getHisto(PixScan::OCCUPANCY, this, mod, scn->scanIndex(2), scn->scanIndex(1)+1, 0);
				hOcc = &scn->getHisto(PixScan::OCCUPANCY, this, mod, scn->scanIndex(2), scn->scanIndex(1), (scn->scanIndex(0)) - 1 );
				hGdac = &scn->getHisto(PixScan::GDAC_T, this, mod, scn->scanIndex(2), scn->scanIndex(1), 0 );
				m_pixCtrl->writeModuleConfig(*(m_modules[pmod]),true);
			//}
		}

        bool first = 1;
		for(std::vector<PixFe*>::iterator fe = m_modules[pmod]->feBegin(); fe!= m_modules[pmod]->feEnd(); fe++){
					//Config &cfg = (*fe)->config();
			std::cout<<"In the FE loop within a module: "<<pmod<<std::endl;
			//bool tune_fe=true;
			Config &cfg = (*fe)->config();
			{ConfObj &cfg_obj = cfg["Misc"]["ScanEnable"];
				if (cfg_obj.type() == ConfObj::BOOL) {
	  		//if (static_cast<ConfBool&>(cfg_obj).m_value == false) tune_fe=false;
			}}
			{ConfObj &cfg_obj = cfg["Misc"]["DacsEnable"];
				if (cfg_obj.type() == ConfObj::BOOL) {
	  		//if (static_cast<ConfBool&>(cfg_obj).m_value == false) tune_fe=false;
			}}
			//PixFeConfig &old_cfg = (*fe)->feConfig("PreScanConfig");
			ife = (*fe)->number();

            // open file with badPixelList
			std::ostringstream stringStream;
            stringStream << "/listOfMalfunctPixelsOnModule_"<<m_modConn[pmod]->name()<<"_"<<(*fe)->number();
            string filenameList = listDirectory + stringStream.str();
			bool badPixelListFound = false;
			if(scn->scanIndex(nloop)==0){
			  struct stat buf;
			  badPixelListFound = (stat(filenameList.c_str(), &buf) == 0);
			}

			ofstream toBadPixelList;
            if(!badPixelListFound)
			  toBadPixelList.open(filenameList);

            // log first occurence only
            if(first) {
                if(badPixelListFound) std::cout << "Disabing bad pixels using list in: " << listDirectory << std::endl;
                else std::cout << "Writing bad pixel list to: " << listDirectory << std::endl;
                first = 0;
            }

			
			if(isFei4 && isFeActive(*fe)){
			    auto fei4 = static_cast<PixFeI4*>(*fe);
				std::cout<<"fe is active and it's a FE-I4"<<std::endl;
				glob_occ = 0;
				glob_occ_best = 0;
				glob_events =0;
				ConfMask<unsigned short int> &tdacs = fei4->readTrim(Fei4::PixReg::TDAC);
				//    		ConfMask<bool> pixEnable = (*fe)->readPixRegister("ENABLE");   //this is not actually used

				ConfMask<bool> pixEnable = fei4->readPixRegister(Fei4::PixReg::Enable);
				pixEnable.enableAll();
				string pair;
				ifstream infile;

				if(badPixelListFound){
				  infile.open (filenameList);
                  int feId = (std::strcmp(filenameList.substr(filenameList.size()-1,1).c_str(),"0") == 0) ? 0 : 1;	// feId from file name
				  while(getline(infile,pair))    //Defining mask
				    {
				      std::istringstream iss(pair);
				      std::vector<std::string> coordinates(std::istream_iterator<std::string>{iss}, std::istream_iterator<std::string>());

                      // pixEnable ranges [0,79]. Subtract 80 for fe=1
                      pixEnable.disable((int) (std::stoi(coordinates.at(0))-feId*80), (int) std::stoi(coordinates.at(1)));
				    }
				  infile.close();
				}

				
				for(int col = 0; col<tdacs.ncol(); col++){
					for(int row=0; row<tdacs.nrow(); row++){
					  //Resetting the TDAC to 15 in the case we don't want to start from CfgValue
					  if (!scn->tuningStartsFromCfgValues()) {
					    tdacs.set(col, row, 15);
					  }
				//		std::cout<<"Column: "<<col<<"\tRow: "<<row<<std::endl;
				//		std::cout<<"--- Getting Pix Geometry ---"<<std::endl;
						PixCoord cp = geo.coord(ife, col, row);
						colmod = cp.x();
						rowmod = cp.y();
				//		std::cout<<"--- About to get number of mask steps ---"<<std::endl;
				//		std::cout<<"--- Getting Mask Step ---"<<std::endl;
					//	std::cout<<"REST ROW: "<<rest_row<<"\tMASK: "<<mask<<"\tNTOT_MASK: "<<ntot_mask<<std::endl;
                        if(pixEnable.get(col,row)){
						//	if(col <10 && row <10) std::cout<<"PIX ENABLED"<<std::endl;		
							if (isPixelInMaskSteps(col, row, scn)) {
								if(scn->scanIndex(nloop) == 0){
									hOccBest->set(colmod, rowmod, 0);
								}else{
								//std::cout<<"In the else LOOP"<<std::endl;
									double occ = (*hOcc)(colmod, rowmod);
									double occ_best = (*hOccBest)(colmod,rowmod);
									double events = (double)(scn->getRepetitions());
								//std::cout<<"-*-*-* Occ: "<<occ<<"\tROW: "<<rowmod<<"\tCOL: "<<colmod<<std::endl;
									// if (occ > events) std::cout<<" glob_occ before: "<<glob_occ<<std::endl;
									(occ_best < events) ? glob_occ_best += occ_best : glob_occ_best += events;
									(occ < events) ? glob_occ += occ : glob_occ += events;
									glob_events += events;
									// if (occ > events) std::cout<< "xxxx OCC: " << occ << "\tevents: " << events  << "\tglob_occ: " << glob_occ << std::endl;
								}
							}
                            // filter out pixels and save to the list
							if(scn->scanIndex(nloop)==8 && !badPixelListFound){
							  if((int)(*hOcc)(colmod, rowmod)<=0+lowerThresholdShift || (int)(*hOcc)(colmod, rowmod)>=maximumOccupancy-upperThresholdShift)
							    toBadPixelList<<colmod<<" "<<rowmod<<endl;
							}

						}
					}
				}

				toBadPixelList.close();
				
				if(scn->scanIndex(nloop) != 0){
					if(glob_occ/glob_events < 0.5 && glob_events != 0 && glob_occ != 0){

						GDAC_F =  fei4->readGlobRegister(Fei4::GlobReg::Vthin_Fine)-(int)scn->getLoopVarValues(nloop)[scn->scanIndex(nloop)];
						std::cout<<"GDAC VALUE IN THE GLOB REG:"<<fei4->readGlobRegister(Fei4::GlobReg::Vthin_Fine)<<std::endl;
						std::cout<<"GDAC_F="<<fei4->readGlobRegister(Fei4::GlobReg::Vthin_Fine)-(int)scn->getLoopVarValues(nloop)[scn->scanIndex(nloop)]<<std::endl;
						if(GDAC_F<0) GDAC_F = fei4->readGlobRegister(Fei4::GlobReg::Vthin_Fine)/2;
						if( (glob_occ_best > glob_occ) && glob_occ_best/glob_events < 0.5 && scn->scanIndex(nloop) > 1 ){
							GDAC_F = fei4->readGlobRegister(Fei4::GlobReg::Vthin_Fine)+(int)scn->getLoopVarValues(nloop)[scn->scanIndex(nloop)];
							std::cout<<"PROTECTION AGAINST LOW THRESHOLD!"<<std::endl;
						}
					}
					else if (glob_occ/glob_events >=0.5 && glob_events != 0){
					  std::cout<<"GDAC VALUE IN THE GLOB REG:"<<fei4->readGlobRegister(Fei4::GlobReg::Vthin_Fine)<<std::endl;
					  std::cout<<"GDAC_F="<<fei4->readGlobRegister(Fei4::GlobReg::Vthin_Fine)+(int)scn->getLoopVarValues(nloop)[scn->scanIndex(nloop)]<<std::endl;
						GDAC_F = fei4->readGlobRegister(Fei4::GlobReg::Vthin_Fine)+(int)scn->getLoopVarValues(nloop)[scn->scanIndex(nloop)];
						if(GDAC_F>255){
						  std::cout<<"GDAC_F>255->  "<<(fei4->readGlobRegister(Fei4::GlobReg::Vthin_Fine) + 255)/2<<std::endl;
						  GDAC_F = (fei4->readGlobRegister(Fei4::GlobReg::Vthin_Fine) + 255)/2;
						}
					}
					else GDAC_F = fei4->readGlobRegister(Fei4::GlobReg::Vthin_Fine);
					std::cout<<"-*-*-*-*  About to write the VthinAltFine register, glob_occ = "<<glob_occ<<" | glob_events = "<<glob_events<<" | ratio = "<<glob_occ/glob_events<<std::endl;
					fei4->writeGlobRegister(Fei4::GlobReg::Vthin_Fine, GDAC_F&0xff);
					std::cout<<"-*-*-*-*  Value stored, VthinAltFine set to:"<<(GDAC_F&0xff)<<"\tSTEP SIZE: "<<(int)scn->getLoopVarValues(nloop)[scn->scanIndex(nloop)]<<std::endl;
				
					storeConfig(pmod, "PreScanConfig");
				}
			}
			
		}
		std::cout<<"DONE WITH FE LOOP"<<std::endl;
		m_pixCtrl->writeModuleConfig(*(m_modules[pmod]),true);
			
    if(scn->scanIndex(nloop) > 0){
			writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::OCCUPANCY, scn->scanIndex(2), scn->scanIndex(1), (scn->scanIndex(0)) - 1);
			std::cout << "PixModuleGroup::prepareGDACFineFastTuning succesfully written histo OCCUPANCY " 
				<< scn->scanIndex(2) << "/" << scn->scanIndex(1)  << "/" << (scn->scanIndex(0) - 1) << std::endl;  // 
    }
	}
	}
	m_configSent = true;
	std::cout<<"DONE with Preparation"<<std::endl;
}

void PixModuleGroup::prepareFei4GDACCoarseTuning(int nloop, PixScan *scn){

	// gets called before each scan step
	// 	step 0 	- set correct charge injection
	//      	- set all tdacs to maximum value
	//      	- set GDAC_fine to Max and GDAC_Coarse to 0
	// 	all other steps	- retrieve occupancy histos of previous step 
	//      		- if occ > 50%   Vthin_altCoarse++
	//                        else return
	//

	std::stringstream info;
	Histo *hOcc,  *hOccBest,  *hGdac;
	int vBest;
	double glob_occ, glob_occ_best, glob_events;

	if(!scn->getHistogramKept(PixScan::OCCUPANCY)){
		info<<"Invalid Configuration! Try to keep PixScan::OCCUPANCY histos. ";
		ers::error(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), info.str()));
		throw(PixModuleGroupExc(PixModuleGroupExc::FATAL, "INVALID_SCAN", info.str()));;
	}
	
	std::cout << "PixModuleGroup::prepareFei4GDACCoarseTuning   loop"<< std::dec << nloop << "/" << scn->scanIndex(nloop) << std::endl;

	
	int ife, colmod, rowmod;
	//double occ_store, events_store;
	//PixCoord cp;
	int GDAC_C = 0;



	// LOOP 0, different from the other loop because of some initial setting
	if(scn->scanIndex(nloop) == 0){
		// MODULE LOOP
		for(unsigned int pmod=0; pmod<m_modules.size(); pmod++){
			// CHECK IF THE MODULE IS ACTIVE
			if(m_modules[pmod]->m_readoutActive){
				unsigned int nCol = (unsigned int) m_modules[pmod]->nCol();
				unsigned int nRow = (unsigned int) m_modules[pmod]->nRow();

				PixGeometry geo = m_modules[pmod]->geometry();
				bool isFei4 = isFEI4(m_modules[pmod]->feFlavour());
				unsigned int mod = m_modules[pmod]->m_moduleId;
				hGdac = new Histo("GDAC_T","GDAC_tuning", nCol, -0.5, (float)nCol-0.5, nRow, -0.5, (float)nRow-0.5);
				scn->addHisto(*hGdac, PixScan::GDAC_T, mod, scn->scanIndex(2), scn->scanIndex(1), 0);
			 	hOcc = new Histo("OCCUPANCY", "Occupancy", nCol, -0.5, (float)nCol-0.5, nRow, -0.5, (float)nRow-0.5f);
			 	scn->addHisto(*hOcc, PixScan::OCCUPANCY, mod, scn->scanIndex(2), scn->scanIndex(1)+1, 0);
				hOccBest = new Histo("OCCUPANCY", "Best Occupancy", nCol,-0.5,  (float)nCol-0.5, nRow, -0.5, (float)nRow-0.5);
				scn->addHisto(*hOccBest, PixScan::OCCUPANCY, mod, scn->scanIndex(2), scn->scanIndex(1)+1, 0);
				storeConfig(pmod, "PreScanConfig");
				
				// FE LOOP
				for(std::vector<PixFe*>::iterator fe = m_modules[pmod]->feBegin(); fe != m_modules[pmod]->feEnd(); fe++){
					ife = (*fe)->number();	
					// CHECK IF FE IS ACTIVE
					if(isFei4 && isFeActive(*fe)){
					    auto fei4 = static_cast<PixFeI4*>(*fe);
						vBest = getVCALfromCharge( (int)scn->getThresholdTargetValue(m_modules[pmod]->connName()), *fe, getCapType(scn) );
                        fei4->writeGlobRegister(Fei4::GlobReg::PlsrDAC, vBest);
						//setTDACtoFei4Max(fe);
                        fei4->writeGlobRegister(Fei4::GlobReg::Vthin_Fine, 0xff);

						GDAC_C = 0;

                        fei4->writeGlobRegister(Fei4::GlobReg::Vthin_Coarse, ((GDAC_C<<8)&0x7f00)>>7);
						ConfMask<unsigned short int> &tdacs = fei4->readTrim(Fei4::PixReg::TDAC);
						// LOOP OVER COLUMNS AND ROWS IN A FE
						for(int col = 0 ; col<m_modules[pmod]->nChipCol(); col++){
							for(int row=0; row<m_modules[pmod]->nChipCol(); row++){
								PixCoord cp = geo.coord(ife, col, row);
								colmod = cp.x();
								rowmod = cp.y();
								
								hOccBest->set(colmod, rowmod, 0);
								tdacs.set(col, row, 15); // HARD CODED FOR FEI4
							}
						}
					}
				}
				m_pixCtrl->writeModuleConfig(*(m_modules[pmod]),true);
				scn->setFeVCal(0x1fff);
			}
		}
	}else if (scn->scanIndex(nloop) == 1){ // get OCCUPANCY of the LAST step

		for(unsigned int pmod=0; pmod<m_modules.size(); pmod++){
			if(m_modules[pmod]->m_readoutActive){
				//unsigned int nCol = (unsigned int) m_modules[pmod]->nCol();
				//unsigned int nRow = (unsigned int) m_modules[pmod]->nRow();

				PixGeometry geo = m_modules[pmod]->geometry();
				bool isFei4 = isFEI4(m_modules[pmod]->feFlavour());
				unsigned int mod = m_modules[pmod]->m_moduleId;
				hOccBest = &scn->getHisto(PixScan::OCCUPANCY, this, mod, scn->scanIndex(2), scn->scanIndex(1)+1, 0);
				hOcc = &scn->getHisto(PixScan::OCCUPANCY, this, mod, scn->scanIndex(2), scn->scanIndex(1), (scn->scanIndex(0)) - 1 );
				hGdac = &scn->getHisto(PixScan::GDAC_T, this, mod, scn->scanIndex(2), scn->scanIndex(1), 0 );

				for(std::vector<PixFe*>::iterator fe = m_modules[pmod]->feBegin(); fe!= m_modules[pmod]->feEnd(); fe++){
					if(isFei4 && isFeActive(*fe)){
						GDAC_C = 1;
						static_cast<PixFeI4*>(*fe)->writeGlobRegister(Fei4::GlobReg::Vthin_Coarse, (GDAC_C<<8)&0x7f00>>7);
					}

				}
				m_pixCtrl->writeModuleConfig(*(m_modules[pmod]),true);
				for(std::vector<PixFe*>::iterator fe = m_modules[pmod]->feBegin(); fe!= m_modules[pmod]->feEnd(); fe++){
					ife = (*fe)->number();
					if(isFei4 && isFeActive(*fe)){
						glob_occ = 0;
						glob_occ_best = 0;
						glob_events =0;
						for(int col = 0; col<m_modules[pmod]->nChipCol(); col++){
							for(int row=0; row<m_modules[pmod]->nChipRow(); row++){

								PixCoord cp = geo.coord(ife, col, row);
								colmod = cp.x();
								rowmod = cp.y();
								if ( isPixelInMaskSteps(col, row, scn) ) {
									double occ = (*hOcc)(colmod, rowmod);
									double occ_best = (*hOccBest)(colmod, rowmod);
									double events = (double)(scn->getRepetitions());
									
									glob_occ += occ;
									glob_occ_best += occ_best;
									glob_events += events;
								}	
							}
						}
						if(glob_occ_best/glob_events < 0.5) GDAC_C = 0;
						else{
							if(glob_occ/glob_events < 0.5)GDAC_C = 1;
							
							else GDAC_C =2;
						}
						static_cast<PixFeI4*>(*fe)->writeGlobRegister(Fei4::GlobReg::Vthin_Coarse, (GDAC_C<<8)&0x7f00>>7);
					}
				}

				m_pixCtrl->writeModuleConfig(*(m_modules[pmod]),true);
				
			}
		}
	}
}

/*void PixModuleGroup::setTDACtoFei4Max(PixFe* fe){

	ConfMask<unsigned short int> &tdacs = (*fe)->readTrim("TDAC");   // unnecessary to read setting?


	for (int col=0; col<tdacs.ncol(); col++) {
	  for (int row=0; row<tdacs.nrow(); row++) {
		tdacs.set(col, row, 15);

	  }
	}

}*/

bool PixModuleGroup::isFeActive(PixFe* fe){
  Config &cfg = fe->config();
  {
    ConfObj &cfg_obj = cfg["Misc"]["ScanEnable"];
    if (cfg_obj.type() == ConfObj::BOOL) {
      if(static_cast<ConfBool&>(cfg_obj).m_value == false) return false;
    }
  }
  {
    ConfObj &cfg_obj = cfg["Misc"]["DacsEnable"];
    if (cfg_obj.type() == ConfObj::BOOL) {
      if (static_cast<ConfBool&>(cfg_obj).m_value == false) return false;
    }
  }
  return true;
}


void PixModuleGroup::prepareTDACTuningIterated(int nloop, PixScan *scn) {

  int const TDAC_S = scn->getLoopVarValues(nloop)[scn->scanIndex(nloop)];

  //Target occupancy should be 50% of triggers during standart tuning
  double target = scn->getRepetitions()/2.;
  //Test for noise Scan (noise threshold turning) 
  if(scn->getSelfTrigger())target = scn->getRepetitions()/100.;
  std::cout << " prepareTDACTuningIterated loop " << std::dec << nloop << " TDAC step is " << std::dec << TDAC_S << std::endl;

  for(auto module: m_modules) {
    if (!(module->m_readoutActive)) continue;
    bool cfgChanged = false;
    auto const nCol = module->nCol();
    auto const nRow = module->nRow();
    bool isFei4 = isFEI4(module->feFlavour());
    auto const mod = module->m_moduleId;
    PixGeometry geo = m_modules[mod]->geometry();
    if (scn->scanIndex(nloop) == 0){
      m_tdac_TuneConverge[mod].clear();
      m_tdac_TuneConverge[mod].resize(nCol,std::vector<bool>(nRow,false));
      for(auto fe: *module) {
        auto const vBest = getVCALfromCharge((int)scn->getThresholdTargetValue(module->connName()), fe, getCapType(scn));

        if(isFei4) {
          auto fei4 = static_cast<PixFeI4*>(fe);
	  fei4->readTrim(Fei4::PixReg::TDAC).setAll(TDAC_S);
	  fei4->writeGlobRegister(Fei4::GlobReg::PlsrDAC, vBest);
        } else {
          auto fei3 = static_cast<PixFeI3*>(fe);
	  fei3->readTrim(Fei3::PixReg::TDAC).setAll(TDAC_S);
	  fei3->writeGlobRegister(Fei3::GlobReg::VCal, vBest);
        }
      }
      cfgChanged = true;
    } else {

      int loopIndex = scn->scanIndex(0);
      Histo *hOcc = &scn->getHisto(PixScan::OCCUPANCY, this, mod, scn->scanIndex(2), scn->scanIndex(1), loopIndex - 1 );

      for(auto fe: *module) {
        int ife = fe->number();
        auto fei4 = dynamic_cast<PixFeI4*>(fe);
        auto fei3 = dynamic_cast<PixFeI3*>(fe);
        ConfMask<unsigned short int> &tdacs = fei4 ? fei4->readTrim(Fei4::PixReg::TDAC)
                                                 : fei3->readTrim(Fei3::PixReg::TDAC);

	  for (unsigned int col=0; col<(unsigned int)module->nChipCol(); col++){
	    for (unsigned int row=0; row<(unsigned int)module->nChipRow(); row++){
	      PixCoord pc = geo.coord(ife,col,row);
	      unsigned int colmod = pc.x();
	      unsigned int rowmod = pc.y();
	      if(m_tdac_TuneConverge[mod][colmod][rowmod])continue;
	      cfgChanged = true;
	        if ((*hOcc)(colmod, rowmod) > target){//Stop Tuning for this pixel
	          m_tdac_TuneConverge[mod][colmod][rowmod]=true;
	        } else {
	          tdacs.set(col, row, TDAC_S);//Go to next TDAC step
	        }
	    }
	 }
      }
    }
    if(cfgChanged)m_pixCtrl->writeModuleConfig(*module, true);
  }
  scn->setFeVCal(0x1fff);
  m_configSent = true;

}

void PixModuleGroup::prepareGDACTuningIterated(int nloop, PixScan *scn){

  int const GDAC_FE = scn->getLoopVarValues(nloop)[scn->scanIndex(nloop)];
  //Target occupancy should be 50% of triggers during standart tuning
  double target = scn->getRepetitions()/2.;
  //Test for noise Scan (noise threshold turning) 
  if(scn->getSelfTrigger())target = scn->getRepetitions()/100.;

  std::cout << " prepareGDACTuningIterated loop " << std::dec << nloop << " GDAC step is " << std::dec << GDAC_FE << std::endl;
  auto tuningFromCfgTDAC = scn->tuningStartsFromCfgValues();

  for(auto module: m_modules) {
    if (!(module->m_readoutActive)) continue;
    bool cfgChanged = false;
    bool isFei4 = isFEI4(module->feFlavour());
    auto const mod = module->m_moduleId;
    PixGeometry geo = module->geometry();
    if (scn->scanIndex(nloop) == 0){
      for(auto fe: *module) {
        m_GDAC_tuneConverge[mod][fe->number()]=false;//Initialize global var
        auto const vBest = getVCALfromCharge((int)scn->getThresholdTargetValue(module->connName()), fe, getCapType(scn));

        if(isFei4) {
          auto fei4 = static_cast<PixFeI4*>(fe);
	  if(!tuningFromCfgTDAC) fei4->readTrim(Fei4::PixReg::TDAC).setAll(15);
	  fei4->writeGlobRegister(Fei4::GlobReg::PlsrDAC, vBest);
	  fei4->writeGlobRegister(Fei4::GlobReg::Vthin_Fine, GDAC_FE&0xff);
        } else {
          auto fei3 = static_cast<PixFeI3*>(fe);
	  if(!tuningFromCfgTDAC) fei3->readTrim(Fei3::PixReg::TDAC).setAll(64);
	  fei3->writeGlobRegister(Fei3::GlobReg::VCal, vBest);
	  fei3->writeGlobRegister(Fei3::GlobReg::GlobalTDAC,GDAC_FE&0x1f);
        }
      }
      cfgChanged = true;
    } else {

      int loopIndex = scn->scanIndex(0);
      Histo *hOcc = &scn->getHisto(PixScan::OCCUPANCY, this, mod, scn->scanIndex(2), scn->scanIndex(1), loopIndex - 1 );

      for(auto fe: *module) {
        int count = 0;
        double mean=0;
        int ife = fe->number();
        if(m_GDAC_tuneConverge[mod][ife])continue;
        cfgChanged = true;

	  for (unsigned int col=0; col<(unsigned int)module->nChipCol(); col++){
	    for (unsigned int row=0; row<(unsigned int)module->nChipRow(); row++){
	      unsigned int colmod, rowmod;
	      PixCoord pc = geo.coord(ife,col,row);
	      colmod = pc.x();
	      rowmod = pc.y();
                if ( isPixelInMaskSteps(col, row, scn) ) {
	          mean += (*hOcc)(colmod, rowmod);
	          count++;
	        }
	    }
	  }
	  if(count)mean/=count;

          if (mean > target){//Stop Tuning for this FE
            m_GDAC_tuneConverge[mod][ife] = true;
            //std::cout<<module->connName()<<" FE "<<ife<<" "<<scn->getLoopVarValues(nloop)[loopIndex]<<" Stopping scanning "<<std::endl;
          } else {//Set next GDAC point
            if(isFei4)static_cast<PixFeI4*>(fe)->writeGlobRegister(Fei4::GlobReg::Vthin_Fine, GDAC_FE&0xff);
            else static_cast<PixFeI3*>(fe)->writeGlobRegister(Fei3::GlobReg::GlobalTDAC,GDAC_FE&0x1f);
            //std::cout<<module->connName()<<" FE "<<ife<<" Setting GDAC to "<< GDAC_FE<<" "<<mean<<" "<<target<<std::endl;
          }
      }
    }
    if(cfgChanged)m_pixCtrl->writeModuleConfig(*module, true);
  }
  scn->setFeVCal(0x1fff);
  m_configSent = true;

}

void PixModuleGroup::prepareGDACFastTuning(int nloop, PixScan *scn){

  if (scn->scanIndex(nloop) != 0)return;//Only done in loop 0

  std::cout << __PRETTY_FUNCTION__<< std::endl;
  auto tuningFromCfgTDAC = scn->tuningStartsFromCfgValues();
  for(auto module: m_modules) {
    if (!(module->m_readoutActive)) continue;
    bool isFei4 = isFEI4(module->feFlavour());
    for(auto fe: *module) {
      auto const vBest = getVCALfromCharge((int)scn->getThresholdTargetValue(module->connName()), fe, getCapType(scn));

      if(isFei4) {
        auto fei4 = static_cast<PixFeI4*>(fe);
	if(!tuningFromCfgTDAC) fei4->readTrim(Fei4::PixReg::TDAC).setAll(15);
	fei4->writeGlobRegister(Fei4::GlobReg::PlsrDAC, vBest);
      } else {
        auto fei3 = static_cast<PixFeI3*>(fe);
	if(!tuningFromCfgTDAC) fei3->readTrim(Fei3::PixReg::TDAC).setAll(64);
	fei3->writeGlobRegister(Fei3::GlobReg::VCal, vBest);
      }
    }
    m_pixCtrl->writeModuleConfig(*module, true);
  }
  scn->setFeVCal(0x1fff);
  m_configSent = true;
}

void PixModuleGroup::prepareTDACFastTuning(int nloop, PixScan *scn,
					   PixHistoServerInterface *hInt,
					   std::string folder) {

  // gets called before each scan step
  // step 0 - set correct charge injection
  //        - set all tdacs to initial value or start with config value
  // all other steps - retrieve occupancy histos of previous step 
  //                 - if occ < 50%   tdac - tdac_step
  //                   else tdac + tdac_step
  
  // abort the scan if no occupancy histos are kept
  if (!scn->getHistogramKept(PixScan::OCCUPANCY)) {
    std::string info = "Invalid scan configuration!  Try to keep PixScan::OCCUPANCY histos. ";
    ers::error(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), info));
    throw(PixModuleGroupExc(PixModuleGroupExc::FATAL, "INVALID_SCAN", info));;
  }
  
  std::cout << "PixModuleGroup::prepareTDACFastTuning   loop " << std::dec << nloop <<"/"<< scn->scanIndex(nloop) << std::endl;
 
  for (auto module: m_modules) {
    if(!(module->m_readoutActive)) continue;
    auto const nCol = module->nCol();
    auto const nRow = module->nRow();
    PixGeometry geo = module->geometry();
    bool const isFei4 = isFEI4(module->feFlavour());
    auto const mod = module->m_moduleId;

    Histo *hOcc=nullptr,*hOccBest=nullptr, *hTdac=nullptr, *hDirection = nullptr;
    
    if(scn->scanIndex(nloop) == 0) {
      hTdac = new Histo("TDAC_T", "Tdac Tuning", nCol, -0.5, (float)nCol-0.5f, nRow, -0.5, (float)nRow-0.5f);
      scn->addHisto(*hTdac, PixScan::TDAC_T, mod, scn->scanIndex(2), scn->scanIndex(1), 0);
      hOccBest = new Histo("OCCUPANCY", "Best Occupancy", nCol, -0.5, (float)nCol-0.5, nRow, -0.5, (float)nRow-0.5f);
      scn->addHisto(*hOccBest, PixScan::OCCUPANCY, mod, scn->scanIndex(2), scn->scanIndex(1)+1, 0);

      // used in endTdacFastTune - not created in PixScan::startScan!
      module->storeConfig("PreScanConfig");

      //Retrieve the tuning point 
      float tuningPoint = scn->getThresholdTargetValue(module->connName());

      //calculate vcal for threshold target and set  
      for(auto fe: *module){
        auto vBest = getVCALfromCharge(tuningPoint, fe, getCapType(scn));	
        if(isFei4) static_cast<PixFeI4*>(fe)->writeGlobRegister(Fei4::GlobReg::PlsrDAC, vBest);
        else  static_cast<PixFeI3*>(fe)->writeGlobRegister(Fei3::GlobReg::VCal, vBest);
      }
      m_pixCtrl->writeModuleConfig(*module, true);
      scn->setFeVCal(0x1fff);   // Rod will not set VCal
      
    } else { // get occupancy that was the result of the last scan step
      hOcc = &scn->getHisto(PixScan::OCCUPANCY, this, mod, scn->scanIndex(2), scn->scanIndex(1), (scn->scanIndex(0)) - 1 );
      hOccBest = &scn->getHisto(PixScan::OCCUPANCY, this, mod, scn->scanIndex(2), scn->scanIndex(1)+1, 0 );   
      hTdac = &scn->getHisto(PixScan::TDAC_T, this, mod, scn->scanIndex(2), scn->scanIndex(1), 0 );
  
      if (scn->getHistogramFilled(PixScan::TUNING_DIR)) {
	std::string title = "tuning direction (step: "+std::to_string((int)(scn->getLoopVarValues(nloop))[scn->scanIndex(nloop)])+") ";
	title += isFei4 ? "1:up 2:down" : "1:down 2:up";
        hDirection = new Histo("TUNING_DIR", title,
			       nCol, -0.5, nCol-0.5,
			       nRow, -0.5, nRow-0.5);
        scn->addHisto(*hDirection, PixScan::TUNING_DIR, mod, scn->scanIndex(2), scn->scanIndex(nloop), 0);
      }
    }
    
    for(auto fe: *module) {	
      bool tune_fe = isFeActive(fe);

      auto fei4 = dynamic_cast<PixFeI4*>(fe);
      auto fei3 = dynamic_cast<PixFeI3*>(fe);
      ConfMask<unsigned short int> &tdacs = fei4 ? fei4->readTrim(Fei4::PixReg::TDAC)
                                                 : fei3->readTrim(Fei3::PixReg::TDAC);   // unnecessary to read setting?

      int ife = fe->number();

      for (int col=0; col<tdacs.ncol(); col++) {
        for (int row=0; row<tdacs.nrow(); row++) {
          bool tune_pixel=tune_fe;

          // should only change tdacs of enabled pixel
          //	    if (!hitbus.get(col, row)) tune_pixel=false;

          PixCoord pc = geo.coord(ife,col,row);
          auto const colmod = pc.x();
          auto const rowmod = pc.y();
          
          if (isPixelInMaskSteps(col, row, scn)) {
            int tdac = tdacs.get(col, row);

            if (scn->scanIndex(nloop) == 0) {        // set TDACS to initial values
              if(!scn->tuningStartsFromCfgValues()) {// HERE I read the initial value from scan cfg ONLY for a standard tuning.
                tdac = (int)(scn->getLoopVarValues(nloop))[0];
                if(tune_pixel) tdacs.set(col, row, tdac);
              }
              
              hTdac->set(colmod, rowmod, tdacs.get(col,row));
              hOccBest->set(colmod, rowmod, 0);
            } else {                                  // read the occupancy from the last step and adjust TDAC 
              double occ = (*hOcc)(colmod, rowmod);		               // occ < 50% -> lower tdac!
              double occ_best = (*hOccBest)(colmod, rowmod);	
              double events = (double)(scn->getRepetitions());	    

              // update best match
              if( abs(occ-events/2.) < abs(occ_best-events/2.) ) {
                hOccBest->set(colmod, rowmod, occ);
                hTdac->set(colmod, rowmod, tdacs.get(col,row));
              }

	      int direction = 0;
              if( occ/events < 0.5) {
                tdac = tdac + (isFei4?1:(-1))*(int)(scn->getLoopVarValues(nloop))[scn->scanIndex(nloop)];
		direction = 1;
              } else {
                tdac = tdac + (isFei4?(-1):1)*(int)(scn->getLoopVarValues(nloop))[scn->scanIndex(nloop)];
		direction = 2;
              }
              if(tune_pixel) {
	        tdacs.set(col, row, tdac);
                if(hDirection) {
                  hDirection->set(colmod, rowmod, direction);
		}
	      }
            }
          }  
          // request from Kevin
          // added a simple prevention for values near the end of the available range 
          // todo: instead of leaving them at the end of the range it might be better to leave at prior values

          // also adjust bad TDACs on pixel which are off ?
          if (/* tune_pixel && */ tdacs.get(col, row) > (isFei4?31:127)) tdacs.set(col, row, (isFei4?31:127));
	  else if (/* tune_pixel && */  tdacs.get(col, row) < (isFei4?0:10)) tdacs.set(col, row, (isFei4?0:10));
        }//row loop
      }//col loop
    }//FE loop
    
    m_pixCtrl->writeModuleConfig(*module,true);
    if(scn->scanIndex(nloop) > 0){
      writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::OCCUPANCY, scn->scanIndex(2), scn->scanIndex(1), (scn->scanIndex(0)) - 1);
      std::cout << "PixModuleGroup::prepareTDACFastTuning succesfully written histo OCCUPANCY " 
      << scn->scanIndex(2) << "/" << scn->scanIndex(1)  << "/" << (scn->scanIndex(0) - 1) << std::endl;  // 
    }
  }//module loop
  m_configSent = true;
}

void PixModuleGroup::prepareThrFastScan(int nloop, PixScan *scn, PixHistoServerInterface *hInt, std::string folder){
	// New fast Threshold Scan
	// gets called before each scan step

	std::cout << "PixModuleGroup::prepareThrFastScan loop " << std::dec << nloop << "/" << scn->scanIndex(nloop) << std::endl;
	
	std::stringstream info;
	
	// abort the scan if no occupancy histos are kept
	if(!scn->getHistogramKept(PixScan::OCCUPANCY)) {
		info << "Invalid scan configuration! Try to keep PixScan::OCCUPANCY histos. ";
		ers::error(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), info.str()));
		throw(PixModuleGroupExc(PixModuleGroupExc::FATAL, "INVALID_SCAN", info.str()));
	}
	
	// we're scanning VCAL which makes general scan call code call PixScan::setFeVCal(current scan val.)
	// we want to set VCAL from FE information, which requires PixScan::setFeVCal(0x1fff)
	// ugly fix in the following line - need to think about a better solution
	scn->setFeVCal(0x1fff); //PixController will not set VCal, FE values used instead

	//Module loop
	
	for (unsigned int pmod=0; pmod<m_modules.size(); pmod++){
		if (m_modules[pmod]->m_readoutActive){
			unsigned int nCol=(unsigned int)m_modules[pmod]->nCol();
			unsigned int nRow=(unsigned int)m_modules[pmod]->nRow();
			PixGeometry geo = m_modules[pmod]->geometry();
			bool isFei4 = isFEI4(m_modules[pmod]->feFlavour());
			unsigned int mod = m_modules[pmod]->m_moduleId;
			
			//Initialize histogram
			Histo *hOcc=nullptr, *hVcal=nullptr;
			
			//set correct charge injection
			if (scn->scanIndex(nloop) == 0){
				//Create new Histo
				hVcal = new Histo ("SCURVE_MEAN", "Threshold", nCol, -0.5, (float)nCol-0.5f, nRow, -0.5, (float)nRow-0.5f);
				scn->addHisto(*hVcal, PixScan::SCURVE_MEAN, mod, scn->scanIndex(2), scn->scanIndex(1), -1); // !! Vcal is not a member of PixLib::PixScan 

						
			}
			else { //get occupancy that was the result of the last scan step
				hOcc = &scn->getHisto(PixScan::OCCUPANCY, this, mod, scn->scanIndex(2), scn->scanIndex(1), (scn->scanIndex(0))-1);
				hVcal = &scn->getHisto(PixScan::SCURVE_MEAN, this, mod, scn->scanIndex(2), scn->scanIndex(1), 0);
			}

			//FE Loop
			for (std::vector<PixFe*>::iterator fe = m_modules[pmod]->feBegin(); fe != m_modules[pmod]->feEnd(); fe++){
			  double vcal = 0.;
			  unsigned int colmod=0, rowmod=0;
			  int ife = (*fe)->number();
			  if (scn->scanIndex(nloop) == 0) {
			    vcal = (int)(scn->getLoopVarValues(nloop))[0];  //set  inital vcal value
			    hVcal->set(colmod, rowmod, vcal);  //write inital vcal value to histo
			  } else {
			    double events = (double)(scn->getRepetitions());
			    PixCoord pcv = geo.coord(ife, 1, 1);//col, row);
			    colmod = pcv.x();
			    rowmod = pcv.y();
			    vcal = (*hVcal)(colmod, rowmod); //get vcal from histo
			    std::cout << "PixModuleGroup::prepareThrFastScan step " <<scn->scanIndex(nloop)<< ", mod " <<pmod<< ", FE"<<ife<<", vcal: " << vcal << std::endl;

			    double occ = 0, nent = 0;
			    for (int col=0; col<geo.nChipCol(); col++) {
			      for (int row=0; row<geo.nChipRow(); row++) {
				PixCoord pc = geo.coord(ife, col, row);
				colmod = pc.x();
				rowmod = pc.y();

				if ( isPixelInMaskSteps(col, row, scn) ) {
				  occ += (*hOcc)(colmod, rowmod);
				  nent += 1.;
				}

			      }
			    }
			    if(nent>0) occ /= nent;
			    else       occ = 0.;
			    // check if occupancy is lower or higher than 50% of events
			    std::cout << "PixModuleGroup::prepareThrFastScan step " <<scn->scanIndex(nloop)<< ", mod " <<pmod<< ", FE"<<ife<<", avg. occ: " << 
			      occ << ", events: " << events << std::endl;
			    if (occ/events < 0.5) {
			      vcal +=  (int)(scn->getLoopVarValues(nloop))[scn->scanIndex(nloop)];
			      std::cout << "PixModuleGroup::prepareThrFastScan VCAL occ/events < 0.5: " << vcal << std::endl;
			    }
			    else {
			      vcal -= (int)(scn->getLoopVarValues(nloop))[scn->scanIndex(nloop)];
			      std::cout << "PixModuleGroup::prepareThrFastScan VCAL occ/events >= 0.5: " << vcal << std::endl;
			    }
			  }
			  if(isFei4)
			    static_cast<PixFeI4*>(*fe)->writeGlobRegister(Fei4::GlobReg::PlsrDAC, vcal);
			  else
                static_cast<PixFeI3*>(*fe)->writeGlobRegister(Fei3::GlobReg::VCal, vcal);

			  for (int col=0; col<geo.nChipCol(); col++) {
			    for (int row=0; row<geo.nChipRow(); row++) {
			      PixCoord pc = geo.coord(ife, col, row);
			      colmod = pc.x();
			      rowmod = pc.y();
			      hVcal->set(colmod, rowmod, vcal); // save vcal in Histo hVcal
			    }
			  }

			}
			m_pixCtrl->writeModuleConfig(*(m_modules[pmod]), true);
		}
			
	}
	m_configSent = true;
}


void PixModuleGroup::prepareTDACTuning(int nloop, PixScan *scn) {
  for (auto module: m_modules) {
    if (!(module->m_readoutActive)) continue;
      int const nCol = module->nCol();
      int const nRow = module->nRow();
      const int nFe = module->nChip();
      PixGeometry geo = module->geometry();
      bool const isFei4 =isFEI4(module->feFlavour());
      auto const mod = module->m_moduleId;
      int trg = scn->getThresholdTargetValue(module->connName());
      Histo *hm=nullptr, *hThr=nullptr, *hTdac=nullptr;

      if (scn->scanIndex(nloop) == 0) {
	hThr = new Histo("Threshold", "Threshold", nCol, -0.5, (float)nCol-0.5f, nRow, -0.5, (float)nRow-0.5f);
	hTdac = new Histo("TDAC", "Tdac Tuning", nCol, -0.5, (float)nCol-0.5f, nRow, -0.5, (float)nRow-0.5f);
	// Associate different parameters if the tdac tuning is done in loop 1 or 2
	if (nloop == 1) {
	  scn->addHisto(*hThr, PixScan::TDAC_THR, mod, scn->scanIndex(2), -1, -1);
	  scn->addHisto(*hTdac, PixScan::TDAC_T, mod, scn->scanIndex(2), -1, -1);
	} else if (nloop == 2) {
	  scn->addHisto(*hThr, PixScan::TDAC_THR, mod, -1, -1, -1);
	  scn->addHisto(*hTdac, PixScan::TDAC_T, mod, -1, -1, -1);
	}
      } else {
	if (nloop == 1) {
	  hm = &scn->getHisto(PixScan::SCURVE_MEAN, this, mod, scn->scanIndex(2), scn->scanIndex(1)-1, 0);
	  hThr = &scn->getHisto(PixScan::TDAC_THR, this, mod, scn->scanIndex(2), 0, 0);
	  hTdac = &scn->getHisto(PixScan::TDAC_T, this, mod, scn->scanIndex(2), 0, 0);
	} else if (nloop == 2) {
	  hm = &scn->getHisto(PixScan::SCURVE_MEAN, this, mod, scn->scanIndex(nloop)-1, 0, 0);
	  hThr = &scn->getHisto(PixScan::TDAC_THR, this, mod, 0, 0, 0);
	  hTdac = &scn->getHisto(PixScan::TDAC_T, this, mod, 0, 0, 0);
	}
      }
      if (trg == 0) { //Condition indicates that the tuning is done starting from the actual threshold value as target (measured at the first loop 0 execution)
	if(scn->scanIndex(nloop) == 1) {
	  // Compute here the target value: get it from the scanIndex==0 threshold scan.
	  double mean = 0, meanlocal = 0;
	  int ife, npixel=0;
	  // Cols and rows are the pixel coordinates in FE level
	  for(ife = 0; ife<nFe; ife++) {
	    for(int col=0; col< module->nChipCol(); col++){
	      for(int row=0; row< module->nChipRow(); row++){
		PixCoord pc = geo.coord(ife,col,row);
		auto const colmod = pc.x();
		auto const rowmod = pc.y();
		if(hm) {
		  meanlocal = (int)(*hm)(colmod,rowmod); // Mean value in each pixel
		} else {
		  meanlocal = 4000;
		  std::cout << "PixModuleGroup::prepareTDACTuning: cannot calculate target threshold since an histogram is not retrieved." << std::endl;
		} 
		if(meanlocal != 0) {
		  npixel++;
		  mean = mean + meanlocal;
		}   		    
	      }
	    }
	  }
	  if(npixel != 0) {
	    trg = (int)(mean/npixel);
	    scn->setDynamicThresholdTargetValue(trg);
	  }
	} else if(scn->scanIndex(nloop) != 0) // Take the stored value
	  trg = scn->getDynamicThresholdTargetValue();
      }	// Else, take the non zero value written into the PixScan
      // FE loop
      for (auto fe: *module){
	// Bisection method
        auto fei4 = dynamic_cast<PixFeI4*>(fe);
        auto fei3 = dynamic_cast<PixFeI3*>(fe);
        ConfMask<unsigned short int> &tdacs = fei4 ? fei4->readTrim(Fei4::PixReg::TDAC)
                                                   : fei3->readTrim(Fei3::PixReg::TDAC);
	for (int col=0; col<tdacs.ncol(); col++) {
	  for (int row=0; row<tdacs.nrow(); row++) {
	    unsigned int colmod, rowmod;
	    int ife = fe->number();
	    PixCoord pc = geo.coord(ife,col,row);
	    colmod = pc.x();
	    rowmod = pc.y();
	    if (scn->scanIndex(nloop) == 0) {
	      if(!scn->tuningStartsFromCfgValues()) {// HERE I read the initial value from scan cfg ONLY for a standard tuning.
		tdacs.set(col, row, (int)(scn->getLoopVarValues(nloop))[0]);
	      }
	    } else {
	      //Extract td0 from original config for the bisection correction, if the tuning starts from config values.
	      int extracted_td0 = 0;
	      if(scn->scanIndex(nloop) == 2 && scn->tuningStartsFromCfgValues()) {
		Histo myHisto;
		if(nloop == 1)
		  myHisto = scn->getHisto(PixScan::TDAC_T, this, mod, scn->scanIndex(2), -1, -1);
		else if(nloop == 2)
		  myHisto = scn->getHisto(PixScan::TDAC_T, this, mod, -1, -1, -1);
		if(myHisto.name() == "Not Found") {
		  extracted_td0 = isFei4?15:70;
		}
		else
		  extracted_td0 = (int)myHisto(colmod,rowmod);
	      }
	      // Correct bisection for pixels in the tails 
              int corr = 0;
	      if (scn->scanIndex(nloop) == 2){
		int th0 = (int)((*hThr)(colmod, rowmod));
		int th1 = (int)((*hm)(colmod, rowmod));
                int td1;
		int td0;
		if(!scn->tuningStartsFromCfgValues())
		  td0 = (int)((scn->getLoopVarValues(nloop))[0]);
		else
		  td0 = extracted_td0;

		if (th0 > trg) {
		  td1 = td0 -(int)((scn->getLoopVarValues(nloop))[1]);
		} else {
		  td1 = td0 + (int)((scn->getLoopVarValues(nloop))[1]);
		}
                int delta = (int)((scn->getLoopVarValues(nloop))[2]);
                if (th1 != th0) { 
		  int ttt = td1 + (trg-th1)*(td1-td0)/(th1-th0);
		  if(isFei4){
		    if(ttt<0) ttt=0;
		    if(ttt>31) ttt=31;
		  }else{
		    if (ttt < 25) {
		      ttt = 35;
		    } else if (ttt > 117) {
		      ttt = 107;
		    } else if (ttt < 35) {
		      ttt += 10;
		    } else if (ttt > 100) {
		      ttt -= 6;
		    }
		  }
		  if (td1 < td0 && ttt < td1) {
		    corr = ttt - td1 + delta;
		  } else if (td1 > td0 && ttt > td1) {
                    corr = ttt - td1 - delta;
		  } 
		}
	      }
	      // Update best-match histo
	      if (scn->scanIndex(nloop) == 1) {
		hThr->set(colmod, rowmod, (*hm)(colmod,rowmod));
		hTdac->set(colmod, rowmod, tdacs.get(col,row));
	      } else {
		if (abs((*hm)(colmod,rowmod)-trg) < abs((*hThr)(colmod,rowmod)-trg)) {
		  hThr->set(colmod, rowmod, (*hm)(colmod,rowmod));
		  hTdac->set(colmod, rowmod, tdacs.get(col,row));
		}
	      }
              // Compute new TDAC value
	      int delta = (int)(scn->getLoopVarValues(nloop))[scn->scanIndex(nloop)];
	      // set next scan value - beware: FE-I4 threshold decreases with TDAC, FE-I2/3 threshold increases
	      if ((*hm)(colmod,rowmod) > trg) {
		tdacs.set(col, row, tdacs.get(col, row) + corr + delta*(isFei4?1:(-1)));
	      } else {
		tdacs.set(col, row, tdacs.get(col, row) + corr + delta*(isFei4?(-1):1));
	      }
	    }
	    if (tdacs.get(col, row) > (isFei4?31:127)) tdacs.set(col, row, (isFei4?31:127));
	  }
	}
      }
      m_pixCtrl->writeModuleConfig(*module,true);
  }
  m_configSent = true;
}

void PixModuleGroup::prepareGDACTuning(int nloop, PixScan *scn){
  int const GDAC_FE = scn->getLoopVarValues(nloop)[scn->scanIndex(nloop)];
  std::cout << " prepareGDACTuning loop " << std::dec << nloop << " GDAC step is " << std::dec << GDAC_FE << std::endl;
  auto tuningFromCfgTDAC = scn->tuningStartsFromCfgValues();
  for(auto module: m_modules) {
    if (!(module->m_readoutActive)) continue;
    bool isFei4 = isFEI4(module->feFlavour());
    for(auto fe: *module) {
      if(isFei4) {
        auto fei4 = static_cast<PixFeI4*>(fe);
        fei4->writeGlobRegister(Fei4::GlobReg::Vthin_Fine, GDAC_FE&0xff);
        //DO NOT SET Vthin_Coarse DURING TUNING, DEVELOP GDAC_COARSE SCAN OR SET MANUALLY
        //fei4->writeGlobRegister(Fei4::GlobReg::Vthin_Coarse, (GDAC_FE&0x7f00)>>7);
	if(!tuningFromCfgTDAC) fei4->readTrim(Fei4::PixReg::TDAC).setAll(15);
      } else {
        auto fei3 = static_cast<PixFeI3*>(fe);
        fei3->writeGlobRegister(Fei3::GlobReg::GlobalTDAC,GDAC_FE);
	if(!tuningFromCfgTDAC) fei3->readTrim(Fei3::PixReg::TDAC).setAll(64);
      }
    }
    m_pixCtrl->writeModuleConfig(*module, true);
  }
  m_configSent = true;
}

void PixModuleGroup::prepareFDACFastTuning(int nloop, PixScan *scn,
					  PixHistoServerInterface *hInt,
					  std::string folder){
  
  //gets called before each scan step
  //step 0 - set correct charge injection
  //       - set all fdacs to initial value or start with cfg value
  //all other steps - retrieve tot histos of previous step
  //                - if tot_n < tot_(n-1) fdac - fdac_step
  //                else fdac+fdac_step
  
  std::stringstream info;
  //abort scan if no tot histograms are kept
  if (!scn->getHistogramKept(PixScan::TOT_MEAN)) {
    info<<"Invalid scan configuration. You have to keep PixScan::TOT_MEAN histos.";
    ers::error(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), info.str()));
    throw(PixModuleGroupExc(PixModuleGroupExc::FATAL, "INVALID_SCAN", info.str()));;
  }
  
  std::cout<<"INFO::PixModuleGroup::prepareFDACFastTuning loop"<<std::dec<<nloop<<"/"<<scn->scanIndex(nloop)<<std::endl;

  // Module loop 
  for (unsigned int pmod=0; pmod<m_modules.size();pmod++) {
    if (m_modules[pmod]->m_readoutActive) {
      unsigned int nCol=(unsigned int) m_modules[pmod]->nCol();
      unsigned int nRow=(unsigned int) m_modules[pmod]->nRow();
      PixGeometry geo = m_modules[pmod]->geometry();
      bool isFei4 = isFEI4(m_modules[pmod]->feFlavour());
      unsigned int mod = m_modules[pmod]->m_moduleId;
      int trg = scn->getTotTargetValue(m_modules[pmod]->connName());

      Histo *hTot=nullptr,*hTotBest,*hFdac=nullptr;
      
      if (scn->scanIndex(nloop)==0) {
	//std::cout<<"DEBUG::PF::scanIndex(nloop)==0: scanIndex(2),scanIndex(1)"<<(int)scn->scanIndex(2)<<","<<(int)scn->scanIndex(1)<<std::endl;
	hFdac = new Histo("FDAC_T", "Fdac Tuning", nCol, -0.5, (float)nCol-0.5f,nRow,-0.5,(float)nRow-0.5f);
	scn->addHisto(*hFdac,PixScan::FDAC_T, mod, scn->scanIndex(2),scn->scanIndex(1),0);
	hTotBest = new Histo("TOT", "Best TOT",nCol, -0.5,(float)nCol-0.5f,nRow,-0.5,(float)nRow-0.5f);
	scn->addHisto(*hTotBest, PixScan::TOT_MEAN,mod,scn->scanIndex(2),scn->scanIndex(1)+1,0);
	
	//store the initial configuration
	storeConfig(pmod,"PreScanConfig");
	
	//calculate vcal for tot target and set
	for (std::vector<PixFe*>::iterator fe = m_modules[pmod]->feBegin(); fe != m_modules[pmod]->feEnd(); fe++){
	auto const vBest = getVCALfromCharge(scn->getTotTargetCharge(), *fe, getCapType(scn));
	  if(isFei4)
	    static_cast<PixFeI4*>(*fe)->writeGlobRegister(Fei4::GlobReg::PlsrDAC, vBest);
	//std::cout << "PixModuleGroup::prepareFDACTuning: PlsrDAC=" << vBest << ", " << (*fe)->readGlobRegister("PlsrDAC") << std::endl;
	  else
            static_cast<PixFeI3*>(*fe)->writeGlobRegister(Fei3::GlobReg::VCal, vBest);
	}//fe loop
	
	m_pixCtrl->writeModuleConfig(*(m_modules[pmod]),true);
	scn->setFeVCal(0x1fff);   // Rod will not set VCal
      }//scan index loop = 0
      else { //get ToT that was the result of last scan step
	
	hTot = &scn->getHisto(PixScan::TOT_MEAN,this,mod,scn->scanIndex(2),scn->scanIndex(1),(scn->scanIndex(0)) - 1 );
	hTotBest = &scn->getHisto(PixScan::TOT_MEAN,this,mod,scn->scanIndex(2),scn->scanIndex(1)+1,0);
	hFdac = &scn->getHisto(PixScan::FDAC_T,this,mod,scn->scanIndex(2),scn->scanIndex(1),0);
      } // scan index loop !=0
      
      // FE loop
      for (std::vector<PixFe*>::iterator fe = m_modules[pmod]->feBegin(); fe!=m_modules[pmod]->feEnd(); ++fe)
	{
	  //do not run the tune on disabled fes
	  bool tune_fe = true;
	  Config &cfg = (*fe)->config();
	  {ConfObj &cfg_obj = cfg["Misc"]["ScanEnable"];
	    if (cfg_obj.type()==ConfObj::BOOL) {
	      if (static_cast<ConfBool&>(cfg_obj).m_value == false) tune_fe = false;
	    }}
	  {ConfObj &cfg_obj = cfg["Misc"]["DacsEnable"];
	    if (cfg_obj.type()==ConfObj::BOOL) {
	      if (static_cast<ConfBool&>(cfg_obj).m_value == false) tune_fe = false;
	    }}
	  
	  //To do: exit here if the Fe doesn't need to be tuned
      auto fei4 = dynamic_cast<PixFeI4*>(*fe);
      auto fei3 = dynamic_cast<PixFeI3*>(*fe);
      ConfMask<unsigned short int> &fdacs = fei4 ? fei4->readTrim(Fei4::PixReg::FDAC)
                                                 : fei3->readTrim(Fei3::PixReg::FDAC);
	  for (int col=0; col<fdacs.ncol();col++) {
	    for (int row=0; row<fdacs.nrow();row++) {
	      bool tune_pixel = tune_fe;
	      //shall I read the enable?
	      unsigned int colmod, rowmod;
	      int ife = (*fe)->number();
	      PixCoord pc = geo.coord(ife,col,row);
	      colmod=pc.x();
	      rowmod=pc.y();

	      if ( isPixelInMaskSteps(col, row, scn) ) {
		
		int fdac = fdacs.get(col,row);
		//std::cout<<"readback fdac="<<fdac<<" at scan loop "<<scn->scanIndex(nloop)<<std::endl;
		if (scn->scanIndex(nloop) == 0 )   { //set initial FDACs
		  if (!scn->tuningStartsFromCfgValues()) {
		    fdac = (int)(scn->getLoopVarValues(nloop))[0];
		    //if (tune_pixel) 
		    fdacs.set(col,row,fdac);
		  }
		  hFdac->set(colmod,rowmod,fdac);
		  hTotBest->set(colmod,rowmod,0);
		}
		else { //read the tot from the last step and adjust TDAC
		  //std::cout<<"DEBUG::PF::scanIndex!=0"<<std::endl;
		  double tot = (*hTot)(colmod,rowmod);
		  double tot_best = (*hTotBest)(colmod,rowmod);
						
		  //update ToT best;
		  if (abs(trg-tot) < abs(trg-tot_best)){
		    hTotBest->set(colmod,rowmod,tot);
		    hFdac->set(colmod,rowmod,fdacs.get(col,row));
		  }
		  
		  //std::cout<<"DEBUG::PF::original fdac "<<fdac<<std::endl;
		  //std::cout<<"DEBUG::PF::trg="<<trg<<" tot="<<tot<<std::endl;
		  if (trg-tot < 0 ) 
		    fdac = fdac +  (isFei4?1:(-1)) * (int)(scn->getLoopVarValues(nloop))[scn->scanIndex(nloop)];
		  else
		    fdac = fdac +  (isFei4?(-1):1) * (int)(scn->getLoopVarValues(nloop))[scn->scanIndex(nloop)];
		  if (tune_pixel)
		    {
		      
		      fdacs.set(col,row,fdac);
		    }
		  
		}//  nloop step !=0
	      }//only scan masked pixels
	      
	      //todo: add a custom range for the cut-off
	      if ( fdacs.get(col,row) > (isFei4?15:7)) 
		{
		  //std::cout<<"DEBUG::fdac set fix"<<std::endl;
		  fdacs.set(col,row,(isFei4?15:7));
		}
	      if ( fdacs.get(col,row) < 0) 
		{
		  //std::cout<<"DEBUG::fdac set lower fix"<<std::endl;
		  fdacs.set(col,row,0);
		}
	      
	    }//col loop
	  }// row loop
	  
	}//fe loop
      
      m_pixCtrl->writeModuleConfig(*(m_modules[pmod]),true);
      
      //write the histo to the histo server and delete them
      if (scn->scanIndex(nloop) > 0) {
	writeAndDeleteHisto(scn,hInt,folder,mod, (int)PixScan::TOT_MEAN,scn->scanIndex(2),scn->scanIndex(1),(scn->scanIndex(0))-1);
	//std::cout<<"DEBUG::PF::written TOT_MEAN histogram "<<scn->scanIndex(2)<<"/"<<scn->scanIndex(1)<<"/"<<(scn->scanIndex(0) - 1 )<<std::endl;
      }
      
    }//readout Active
  }//modules loop
  m_configSent = true;
}//end of FDACFastTuning
			
void PixModuleGroup::prepareFDACTuning(int nloop, PixScan *scn){

  if (scn->scanIndex(nloop) != 0)return;//Only done in loop 0

  for(auto module: m_modules) {
    bool const isFei4 = isFEI4(module->feFlavour());
    if(!(module->m_readoutActive)) continue;
    for (auto fe: *module){
      auto const vBest = getVCALfromCharge(scn->getTotTargetCharge(), fe, getCapType(scn));
 
      if(isFei4){
        auto fei4 = static_cast<PixFeI4*>(fe);
        fei4->writeGlobRegister(Fei4::GlobReg::PlsrDAC, vBest);
	std::cout << "PixModuleGroup::prepareFDACTuning: PlsrDAC=" << vBest << ", " << fei4->readGlobRegister(Fei4::GlobReg::PlsrDAC) << std::endl;
      } else {
        auto fei3 = static_cast<PixFeI3*>(fe);
        fei3->writeGlobRegister(Fei3::GlobReg::VCal, vBest);
	std::cout << "PixModuleGroup::prepareFDACTuning: VCAL=" << vBest << ", " << fei3->readGlobRegister(Fei3::GlobReg::VCal) << std::endl;
      }
    }
    m_pixCtrl->writeModuleConfig(*module,true);
  }
  m_configSent = true;
  scn->setFeVCal(0x1fff);
}

void PixModuleGroup::prepareIFTuning(int nloop, PixScan *scn){
   
   if (scn->scanIndex(nloop) != 0)return;//Only done in loop 0
 
   for (auto module: m_modules) {
    bool isFei4 =isFEI4(module->feFlavour());
    if (module->m_readoutActive) {
      for (auto fe: *module){
        auto const vBest = getVCALfromCharge(scn->getTotTargetCharge(), fe, getCapType(scn));

	if(isFei4) static_cast<PixFeI4*>(fe)->writeGlobRegister(Fei4::GlobReg::PlsrDAC, vBest);
	else static_cast<PixFeI3*>(fe)->writeGlobRegister(Fei3::GlobReg::VCal, vBest);
	//std::cout << vBest << " " << (*fe)->readGlobRegister("DAC_VCAL") << std::endl;

	//set all FDACS to 4(I4: 7) before tuning
    
        if (!scn->tuningStartsFromCfgValues()){
          auto fei4 = dynamic_cast<PixFeI4*>(fe);
          auto fei3 = dynamic_cast<PixFeI3*>(fe);
          ConfMask<unsigned short int> &fdacs = fei4 ? fei4->readTrim(Fei4::PixReg::FDAC)
                                                     : fei3->readTrim(Fei3::PixReg::FDAC);
	  fdacs.setAll(isFei4?7:4);
	}
      }
      m_pixCtrl->writeModuleConfig(*(module),true);
      //      module->writeConfig(); //download the config again
    }
  }
  scn->setFeVCal(0x1fff);
  m_configSent = true;
}

void PixModuleGroup::prepareIFFastTuning(int nloop, PixScan *scn,
					 PixHistoServerInterface *hInt,
					 std::string folder){
 // Module loop
 for (auto module: m_modules) {
    if (module->m_readoutActive){
      int nCol=module->nCol();
      int nRow=module->nRow();
      PixGeometry geo = module->geometry();
      bool isFei4 =isFEI4(module->feFlavour());
      int nFePerCol = nCol/module->nChipCol();
      int nFErows = nRow/module->nChipRow();
      //      const int nFe = module->nChip();
      unsigned int mod = module->m_moduleId;
      Histo *hTot=nullptr, *hIFDac=nullptr;
      int trg = scn->getTotTargetValue(module->connName());
      
      hIFDac = new Histo("IF_T", "Tuned IF", nFePerCol, -0.5, -0.5+(double)nFePerCol, nFErows, -0.5, -0.5+(double)nFErows);
			 //nCol, -0.5, (float)nCol-0.5f,nRow , -0.5, (float)nRow-0.5f);
      scn->addHisto(*hIFDac, PixScan::IF_T, mod, scn->scanIndex(2), scn->scanIndex(1), scn->scanIndex(0));
      
      if (scn->scanIndex(nloop) == 0) {
	//calculate vcal for threshold target and set   
      for(auto fe: *module){
          auto const vBest = getVCALfromCharge(scn->getTotTargetCharge(), fe, getCapType(scn));

	  if(isFei4) static_cast<PixFeI4*>(fe)->writeGlobRegister(Fei4::GlobReg::PlsrDAC, vBest);
	  else static_cast<PixFeI3*>(fe)->writeGlobRegister(Fei3::GlobReg::VCal, vBest);

	    if (!scn->tuningStartsFromCfgValues()){
              auto fei4 = dynamic_cast<PixFeI4*>(fe);
              auto fei3 = dynamic_cast<PixFeI3*>(fe);
              ConfMask<unsigned short int> &fdacs = fei4 ? fei4->readTrim(Fei4::PixReg::FDAC): fei3->readTrim(Fei3::PixReg::FDAC);
	      fdacs.setAll(isFei4?7:4);
	     }
       }
	cout << "prepareIFFastTuning: ";
	m_pixCtrl->writeModuleConfig(*(module),true);
	//module->writeConfig(); 
	scn->setFeVCal(0x1fff);   // Rod will not set VCal	
      } else {
	//Where was it filled the 0 one??
	hTot = &scn->getHisto(PixScan::TOT_MEAN, this, mod, scn->scanIndex(2), scn->scanIndex(1), (scn->scanIndex(0)) - 1 );
	//Shall I write the configuration here?
	//I'm very not sure about these histograms.
      }
      
      // FE loop
       for(auto fe: *module){
	int ifVal = 0;
	if(isFei4) {
	  auto fei4 = static_cast<PixFeI4*>(fe);
          ifVal = fei4->readGlobRegister(Fei4::GlobReg::PrmpVbpf);
        } else {
          auto fei3 = static_cast<PixFeI3*>(fe);
          ifVal = fei3->readGlobRegister(Fei3::GlobReg::IF);
        }
	int ife = (fe)->number();
	
	if (scn->scanIndex(nloop) == 0) {                                  // set IF to initial values
	  if (!scn->tuningStartsFromCfgValues())
	    ifVal = (int)(scn->getLoopVarValues(nloop))[0];
	} else {
	  //calculate TOT_MEAN from the previous step and check if it is below or above the target value
	  int count = 0;
	  double mean = 0;
	  for (unsigned int col=0; col<(unsigned int)module->nChipCol(); col++){
	    for (unsigned int row=0; row<(unsigned int)module->nChipRow(); row++){
	      unsigned int colmod, rowmod;
	      PixCoord pc = geo.coord(ife,col,row);
	      colmod = pc.x();
	      rowmod = pc.y();
	      if (((*hTot)(colmod, rowmod)) > 0) {
		mean += (*hTot)(colmod, rowmod); count++;
	      }
	    }   
	  }

	  if(count>0)mean /= count;

	  if(mean < trg){
	    ifVal = ifVal - (int)(scn->getLoopVarValues(nloop))[scn->scanIndex(nloop)];
	  }
	  else{
	    ifVal = ifVal + (int)(scn->getLoopVarValues(nloop))[scn->scanIndex(nloop)];
	  }
	}
// 	for (unsigned int col=0; col<module->nChipCol(); col++){
// 	  for (unsigned int row=0; row<module->nChipRow(); row++){
// 	    unsigned int colmod, rowmod;
// 	    PixCoord pc = geo.coord(ife,col,row);
// 	    colmod = pc.x();
// 	    rowmod = pc.y();
// 	    hIFDac->set(colmod, rowmod, ifVal);
// 	  }
// 	}
	hIFDac->set((ife % nFePerCol) ,(ife < nFePerCol)?0:1, ifVal);

	if(isFei4)
      static_cast<PixFeI4*>(fe)->writeGlobRegister(Fei4::GlobReg::PrmpVbpf, ifVal);
	else
        static_cast<PixFeI3*>(fe)->writeGlobRegister(Fei3::GlobReg::IF, ifVal);
      } // end fe loops

      m_pixCtrl->writeModuleConfig(*(module),true);
      //      module->writeConfig(); //download the config again   necessary???
      //write these two histos to the histo server and delete them
      if(scn->scanIndex(0) > 1){
	writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::TOT_MEAN, scn->scanIndex(2), scn->scanIndex(1), (scn->scanIndex(0)) - 2);
	writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::TOT_SIGMA, scn->scanIndex(2), scn->scanIndex(1), (scn->scanIndex(0)) - 2);
	writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::IF_T, scn->scanIndex(2), scn->scanIndex(1), (scn->scanIndex(0)) - 2);
      }
    } //end if module active
  } //end module loop
  m_configSent = true;

  /* 
  // Module loop
  for (unsigned int pmod=0; pmod<m_modules.size(); pmod++) {
    if (m_modules[pmod]->m_readoutActive){
      unsigned int mod = m_modules[pmod]->m_moduleId;
      Histo *hTot, *hIFDac; 
      hIFDac = new Histo("IF_T", "Tuned IF", 144, -0.5, 143.5, 320, -0.5, 319.5);
      scn->addHisto(*hIFDac, PixScan::IF_T, mod, scn->scanIndex(2), scn->scanIndex(1), scn->scanIndex(0));

      if (scn->scanIndex(nloop) == 0) {   
	//calculate vcal for threshold target and set  
	for (std::vector<PixFe*>::iterator fe = m_modules[pmod]->feBegin(); fe != m_modules[pmod]->feEnd(); fe++){
	  float vcal_a = (dynamic_cast<ConfFloat &>((*fe)->config()["Misc"]["VcalGradient3"])).value();
	  float vcal_b = (dynamic_cast<ConfFloat &>((*fe)->config()["Misc"]["VcalGradient2"])).value();
	  float vcal_c = (dynamic_cast<ConfFloat &>((*fe)->config()["Misc"]["VcalGradient1"])).value();
	  float vcal_d = (dynamic_cast<ConfFloat &>((*fe)->config()["Misc"]["VcalGradient0"])).value();
	  float cInj   = (dynamic_cast<ConfFloat &>((*fe)->config()["Misc"]["CInjLo"])).value();
	  if (scn->getChargeInjCapHigh()){
	    cInj = (dynamic_cast<ConfFloat &>((*fe)->config()["Misc"]["CInjHi"])).value();
	  }   
	  float qBest=0;
	  int vBest=0;
	  for (int v=0; v<1024; v++) {
	    float q = 6.241495961*cInj*(((vcal_a*v + vcal_b)*v + vcal_c)*v + vcal_d)*cInj;
	    if (abs(q-scn->getTotTargetCharge()) < abs(qBest-scn->getTotTargetCharge())) {
	      qBest = q;
	      vBest = v;
	    }
	  }
	  (*fe)->writeGlobRegister("DAC_VCAL", vBest); 	 
	  
	  //set all FDACS to 4 before tuning
	  ConfMask<unsigned short int> &fdacs = (*fe)->readTrim("FDAC"); 
	  for (int col=0; col<fdacs.ncol(); col++) {
	    for (int row=0; row<fdacs.nrow(); row++) {
	      fdacs.set(col, row, 4);
	    }
	  }
	}
	cout << "prepareIFFastTuning: ";
	//m_modules[pmod]->writeConfig(); 
	m_pixCtrl->writeModuleConfig(*(m_modules[pmod]));
	scn->setFeVCal(0x1fff);   // Rod will not set VCal	
      } else {
	hTot = &scn->getHisto(PixScan::TOT_MEAN, this, mod, scn->scanIndex(2), scn->scanIndex(1), (scn->scanIndex(0)) - 1 );
	//hTot = &scn->getHisto(PixScan::TOT_MEAN, mod, scn->scanIndex(2), scn->scanIndex(1), (scn->scanIndex(0)) - 1 );
      }   
      
      // FE loop
      for (std::vector<PixFe*>::iterator fe = m_modules[pmod]->feBegin(); fe != m_modules[pmod]->feEnd(); fe++){
	int ifVal = (*fe)->readGlobRegister("DAC_IF");
	int ife = (*fe)->number();
	
	if (scn->scanIndex(nloop) == 0) {                                  // set IF to initial values
	  ifVal = (int)(scn->getLoopVarValues(nloop))[0];
	  
	} else {
	  //calculate TOT_MEAN from the previous step and check if it is below or above the target value
	  int count = 0;
	  double mean = 0;
	  for (unsigned int col=0; col<18; col++) {
	    for (unsigned int row=0; row<160; row++) {
	      unsigned int colmod, rowmod;
	      if (ife < 8) {
		rowmod = row;
		colmod = col + 18*ife;
	      } else {
		rowmod = 319 - row;
		colmod = 143 - col - 18*(ife-8);
	      }
	      if (((*hTot)(colmod, rowmod)) > 0) {
		mean += (*hTot)(colmod, rowmod); count++;
	      }
	    }   
	  }
	  mean /= count;
	  
	  if(mean < (int)(scn->getTotTargetValue())){
	    ifVal = ifVal - (int)(scn->getLoopVarValues(nloop))[scn->scanIndex(nloop)];
	  }
	  else{
	    ifVal = ifVal + (int)(scn->getLoopVarValues(nloop))[scn->scanIndex(nloop)];
	  }
	}
	for (unsigned int col=0; col<18; col++) {
	  for (unsigned int row=0; row<160; row++) {
	    unsigned int colmod, rowmod;
	    if (ife < 8) {
	      rowmod = row;
	      colmod = col + 18*ife;
	    } else {
	      rowmod = 319 - row;
	      colmod = 143 - col - 18*(ife-8);
	    }
	    hIFDac->set(colmod, rowmod, ifVal);
	  }
	}
	(*fe)->writeGlobRegister("DAC_IF", ifVal);
      } // end fe loops
      cout << "PixModuleGroup::prepareIFFastTuning:";
      //m_modules[pmod]->writeConfig(); //download the config again   necessary???
      m_pixCtrl->writeModuleConfig(*(m_modules[pmod]));
      //write these two histos to the histo server and delete them
      if(scn->scanIndex(0) > 1){
	writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::TOT_MEAN, scn->scanIndex(2), scn->scanIndex(1), (scn->scanIndex(0)) - 2);
	writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::TOT_SIGMA, scn->scanIndex(2), scn->scanIndex(1), (scn->scanIndex(0)) - 2);
	writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::IF_T, scn->scanIndex(2), scn->scanIndex(1), (scn->scanIndex(0)) - 2);
      }
      
    } //end if module active
  } //end module loop
  m_configSent = true;
  */
}



void PixModuleGroup::prepareDiscBiasTuning(int nloop, PixScan* scn){
  PMG_DEBUG = true;
  if(PMG_DEBUG) cout << endl << "start prepareDiscBiasTuning for nloop == " << nloop << endl;
	// Module loop
	for (unsigned int pmod=0; pmod<m_modules.size(); pmod++) {
		if (m_modules[pmod]->m_readoutActive) {
			PixGeometry geo = m_modules[pmod]->geometry();
			unsigned int mod = m_modules[pmod]->m_moduleId;
			unsigned int nCol = (unsigned int) m_modules[pmod]->nCol();
			unsigned int nRow = (unsigned int) m_modules[pmod]->nRow();
			int nFePerCol = nCol/m_modules[pmod]->nChipCol();
			int nFErows = nRow/m_modules[pmod]->nChipRow();
			Histo *hn=nullptr ,*hTime=nullptr, *hDisVbn=nullptr;
			if(PMG_DEBUG) cout << "Scan index is " << scn->scanIndex(nloop) << endl;
			if (scn->scanIndex(nloop) == 0) {
				hTime = new Histo("Timewalk", "Timewalk", nCol, -0.5, nCol-0.5, nRow, -0.5, nRow-0.5);
				hDisVbn = new Histo("DisVbn", "Discriminator Bias Tuning", nFePerCol, -0.5, -0.5+(double)nFePerCol, nFErows, -0.5, -0.5+(double)nFErows);
				// Associate different parameters if the tdac tuning is done in loop 0 or 1 or 2. Most likely 0 in fast tuning...
				if (nloop == 1) {
					if(PMG_DEBUG) cout << " adding histograms in step 0" << endl;
					scn->addHisto(*hTime, PixScan::DISCBIAS_TIMEWALK, mod, scn->scanIndex(2), -1, -1);
					if(PMG_DEBUG) cout << "added histo hTime" << endl;
					scn->addHisto(*hDisVbn, PixScan::DISCBIAS_T, mod, scn->scanIndex(2), -1, -1);
					if(PMG_DEBUG) cout << "added histo hDisVbn" << endl;
				} else if (nloop == 2) {
					scn->addHisto(*hTime, PixScan::DISCBIAS_TIMEWALK, mod, -1, -1, -1);
					scn->addHisto(*hDisVbn, PixScan::DISCBIAS_T, mod, -1, -1, -1);
				}
				for (std::vector<PixFe*>::iterator fe = m_modules[pmod]->feBegin(); fe != m_modules[pmod]->feEnd(); fe++){
					// init control variables
					m_diffUp[pmod][(*fe)->number()]=true;
					m_lastOccAvg[pmod][(*fe)->number()]=-1;
				}
			} else {
				if (nloop == 1) {
					hn = &scn->getHisto(PixScan::TIMEWALK,this, mod, scn->scanIndex(2), scn->scanIndex(nloop)-1, 0);
					hTime = &scn->getHisto(PixScan::DISCBIAS_TIMEWALK,this, mod, scn->scanIndex(2), 0, 0);
					hDisVbn = &scn->getHisto(PixScan::DISCBIAS_T,this, mod, scn->scanIndex(2), 0, 0);
				} else if (nloop == 2) {
					hn = &scn->getHisto(PixScan::TIMEWALK,this, mod, scn->scanIndex(nloop)-1, 0, 0);
					hTime = &scn->getHisto(PixScan::DISCBIAS_TIMEWALK,this, mod, 0, 0, 0);
					hDisVbn = &scn->getHisto(PixScan::DISCBIAS_T,this, mod, 0, 0, 0);
				}
			}

			double FE0Mean = 0;


			// FE loop
			for (std::vector<PixFe*>::iterator fe = m_modules[pmod]->feBegin(); fe != m_modules[pmod]->feEnd(); fe++){
				if(PMG_DEBUG) cout << "FE number = " << (*fe)->number() << endl;
				PixFe* fei4a = nullptr;
				auto fei4b = dynamic_cast<PixFeI4*>(*fe);

				// Get previous DISCBIAS
				int discbias = 0;
				if(scn->getLoopParam(nloop)==PixScan::DISCBIAS){
					if(fei4b!=0){
						discbias = fei4b->readGlobRegister(Fei4::GlobReg::DisVbn);
					}else{
					  auto fei3 = static_cast<PixFeI3*>(*fe);
						discbias = fei3->readGlobRegister(Fei3::GlobReg::GlobalTDAC);
					}
				}/* else if(scn->getLoopParam(nloop)==PixScan::FEI4_GR && scn->getLoopFEI4GR(nloop)=="DisVbn" &&
					(fei4a!=0 || fei4b!=0)){
						discbias = (*fe)->readGlobRegister("DisVbn");
				} */else
					throw PixScanExc(PixControllerExc::ERROR, "Scan variable not compatible with end-of-loop action");

				if(PMG_DEBUG) cout << "\t read old discbias value: " << discbias << endl;

				int newDISCBIAS=0;
				// get new GDAC from Scan parameter, if loop is executed first time
				if (scn->scanIndex(nloop) == 0) {
					newDISCBIAS = (int)(scn->getLoopVarValues(nloop))[0];
				} else {

					// Calculate mean of T0
					double hnMean = 0;
					int nrpixels = 0;

                    auto fei4 = dynamic_cast<PixFeI4*>(*fe);
                    auto fei3 = dynamic_cast<PixFeI3*>(*fe);
                    ConfMask<unsigned short int> &tdacs = fei4 ? fei4->readTrim(Fei4::PixReg::TDAC)
                                                             : fei3->readTrim(Fei3::PixReg::TDAC);
					for (unsigned int col=0; col<(unsigned int)tdacs.ncol(); col++) {
					  for (unsigned int row=0; row<(unsigned int)tdacs.nrow(); row++) {
							unsigned int colmod, rowmod;
							int ife = (*fe)->number();
							PixCoord cp = geo.coord(ife, col, row);
							colmod = cp.x();
							rowmod = cp.y();
							hnMean += (double)(*hn)(colmod, rowmod);
							nrpixels++;
						}
					}
					if (nrpixels != 0) {
						hnMean = (double) hnMean / nrpixels;
						if(PMG_DEBUG) cout << " \t\t MeanT0 = " << hnMean << endl;
					} else {
						if(PMG_DEBUG) cout << "!!! Error, nrpixels is 0, set MeanT0 to 0 !!!" << endl;
						hnMean = 0;
					}

					// If FE0, save mean value for comparison of other FEs
					if((*fe)->number() == 0){
						FE0Mean = hnMean;
						if(PMG_DEBUG) cout << " FE0Mean = " << FE0Mean << endl;
					}

					//ConfMask<unsigned short int> &tdacs = (*fe)->readTrim("TDAC");
					for (unsigned int col=0; col<(unsigned int)tdacs.ncol(); col++) {
						for (unsigned int row=0; row<(unsigned int)tdacs.nrow(); row++) {
							unsigned int colmod, rowmod;
							int ife = (*fe)->number();
							PixCoord cp = geo.coord(ife, col, row);
							colmod = cp.x();
							rowmod = cp.y();
							//if(PMG_DEBUG && col==23 && row==123)
							//	std::cout << "ife = " << ife << ", colmod = " << colmod << ", rowmod = " << rowmod << std::endl;
							// Update best-match histo
							if (scn->scanIndex(nloop) == 1) {
								hTime->set(colmod, rowmod, hnMean);
								hDisVbn->set(colmod, rowmod, discbias);
							} else {
								if (abs(hnMean-FE0Mean) < abs((*hTime)(colmod,rowmod)-FE0Mean)) {
									hTime->set(colmod, rowmod, hnMean);
									hDisVbn->set(colmod, rowmod, discbias);
								}
							}
						}
					}

					// Compute and apply new DisVbn value only for FEs != FE0
					if((*fe)->number() != 0)
					{
						if(PMG_DEBUG) cout << "old discbias before computing: " << discbias << endl;
						// Compute new GDAC value
						if(PMG_DEBUG) cout << "---------- Starting to compute new DISCBIAS value ----------------------" << endl;
						int delta = (int)(scn->getLoopVarValues(nloop))[scn->scanIndex(nloop)];
						if(PMG_DEBUG) cout << "Measured Mean Time is = " << hnMean << endl;

						//


						if (FE0Mean == 0) newDISCBIAS = discbias; // lj protection against 0 in FE0
						else {
						  // 2nd logic statement: protection against running too low in threshold, which is observed
						  // as a decrease in occ. (m_lastOccAvg[pmod][(*fe)->number()]>hnMean) inspite of a decrease
						  // in GDAC (!m_diffUp[pmod][(*fe)->number()]) 
						  if (hnMean < FE0Mean) {
						    if(PMG_DEBUG) cout << "incrementing DISCBIAS, delta = " << delta << ", old DISCBIAS is " << discbias << endl;
						    newDISCBIAS = discbias + delta;
						    //m_diffUp[pmod][(*fe)->number()] = true;
						  } else {
						    if(PMG_DEBUG) cout << "decrementing DISCBIAS, delta = " << delta << ", old DISCBIAS is " << discbias << endl;
						    newDISCBIAS = discbias - delta;
						    //m_diffUp[pmod][(*fe)->number()] = false;
						  }
						}
					}
				}
				//m_lastOccAvg[pmod][(*fe)->number()] = hnMean;
				//				if(PMG_DEBUG) cout << "New GDAC value before cutoff is " << newGDAC << endl;

				//if (scn->getLoopParam(nloop)==PixScan::GDAC && newGDAC > 32767)  newGDAC = 32767;
				//if (scn->getLoopParam(nloop)==PixScan::FEI4_GR && newGDAC > 255) newGDAC = 255;
				if (newDISCBIAS < 0 && scn->scanIndex(nloop) != 0) newDISCBIAS = 0;
				if (newDISCBIAS>=0){
					discbias = (unsigned int) newDISCBIAS;
					if(PMG_DEBUG) cout << " --------------- Final new nDISCBIAS value is " << newDISCBIAS << " --------------- " << endl;
				}

				// Write new discbias value only to FEs!=FE0
				if((*fe)->number() != 0){
					if(fei4a!=0){
                      static_cast<PixFeI4*>(*fe)->writeGlobRegister(Fei4::GlobReg::DisVbn, discbias&0xff);
					}else if(fei4b!=0){
                      static_cast<PixFeI4*>(*fe)->writeGlobRegister(Fei4::GlobReg::DisVbn, discbias&0xff);
					}else
                      static_cast<PixFeI3*>(*fe)->writeGlobRegister(Fei3::GlobReg::GlobalTDAC, discbias);
				}
			}
		}
		m_pixCtrl->writeModuleConfig(*(m_modules[pmod]),true);
		//m_modules[pmod]->writeConfig(); //download the config again
	}
	if(PMG_DEBUG) cout << "ending prepareDiscBiasSet for nloop == " << nloop << endl << endl;
}


void PixModuleGroup::prepareT0Set(int nloop, PixScan *scn){
  // set strobe delay range from scan settings
  // FE-I3: this is actually on the MCC; FE-I4: use PlsrIdacRamp
  if(scn->getOverrideStrobeMCCDelayRange()){
    for (unsigned int pmod=0; pmod<m_modules.size(); pmod++) {
      if (m_modules[pmod]->m_readoutActive) {
	if(isFEI4(m_modules[pmod]->feFlavour())){
	  for (std::vector<PixFe*>::iterator fe = m_modules[pmod]->feBegin(); fe != m_modules[pmod]->feEnd(); fe++){
        static_cast<PixFeI4*>(*fe)->writeGlobRegister(Fei4::GlobReg::PlsrIDACRamp, scn->getStrobeMCCDelayRange());
	  }
	}else{
	  m_modules[pmod]->pixMCC()->writeRegister("CAL_Delay", scn->getStrobeMCCDelayRange());
	}
      }
    }
  }
  // set VCAL or PlsrDAC from ToT target charge - identical to FDAC tuning
  // this will anyway write module config, so take care of propagating strobe delay setting changes
  prepareFDACTuning(nloop, scn);
}

void PixModuleGroup::prepareIncrTdac(int nloop, PixScan *scn) {
  // Module loop
  for (unsigned int pmod=0; pmod<m_modules.size(); pmod++) {
    if (m_modules[pmod]->m_readoutActive) {
      unsigned int mod = m_modules[pmod]->m_moduleId;
      Histo *hm=nullptr, *hs=nullptr, *hc=nullptr, *hTdac=nullptr;
      int nCol=m_modules[pmod]->nCol();
      int nRow=m_modules[pmod]->nRow();
      PixGeometry geo = m_modules[pmod]->geometry();
      if (scn->scanIndex(nloop) == 0) {
	m_modules[pmod]->storeConfig("IncrTdacPreviousConfig");
	hTdac = new Histo("TDAC", "Tdac Tuning", nCol, -0.5, (float)nCol-0.5f, nRow, -0.5, (float)nRow-0.5);
	// Attach the result histogram in the correct place if the scan is done in loop 1 or 2
	if (nloop == 1) {
	  scn->addHisto(*hTdac, PixScan::TDAC_T, mod, scn->scanIndex(2), -1, -1);
	} else if (nloop == 2) {
	  scn->addHisto(*hTdac, PixScan::TDAC_T, mod, -1, -1, -1);
	}
      } else {
	if (nloop == 1) {
	  hm = &scn->getHisto(PixScan::SCURVE_MEAN, this, mod, scn->scanIndex(2), scn->scanIndex(1)-1, 0);
	  hs = &scn->getHisto(PixScan::SCURVE_SIGMA, this, mod, scn->scanIndex(2), scn->scanIndex(1)-1, 0);
	  hc = &scn->getHisto(PixScan::SCURVE_CHI2, this, mod, scn->scanIndex(2), scn->scanIndex(1)-1, 0);
	  hTdac = &scn->getHisto(PixScan::TDAC_T, this, mod, scn->scanIndex(2), 0, 0);
	} else if (nloop == 2) {
	  hm = &scn->getHisto(PixScan::SCURVE_MEAN, this, mod, scn->scanIndex(2)-1, 0, 0);
	  hs = &scn->getHisto(PixScan::SCURVE_SIGMA, this, mod, scn->scanIndex(2), 0, 0);
	  hc = &scn->getHisto(PixScan::SCURVE_CHI2, this, mod, scn->scanIndex(2), 0, 0);
	  hTdac = &scn->getHisto(PixScan::TDAC_T, this, mod, 0, 0, 0);
	}
	// FE loop
	if (!scn->getLoopVarValuesFree(nloop)) {
	  for (std::vector<PixFe*>::iterator fe = m_modules[pmod]->feBegin(); fe != m_modules[pmod]->feEnd(); fe++){
        auto fei4 = dynamic_cast<PixFeI4*>(*fe);
        auto fei3 = dynamic_cast<PixFeI3*>(*fe);
        ConfMask<unsigned short int> &tdacs = fei4 ? fei4->readTrim(Fei4::PixReg::TDAC)
                                                   : fei3->readTrim(Fei3::PixReg::TDAC);
	    ConfMask<unsigned short int> &tdacsPre = fei4 ?
	            (fei4->feConfig("IncrTdacPreviousConfig")).readTrim(Fei4::PixReg::TDAC) :
                (fei3->feConfig("IncrTdacPreviousConfig")).readTrim(Fei3::PixReg::TDAC);
	    for (int col=0; col<tdacs.ncol(); col++) {
	      for (int row=0; row<tdacs.nrow(); row++) {
		unsigned int colmod, rowmod;
		int ife = (*fe)->number();
		PixCoord pc = geo.coord(ife,col,row);
		colmod = pc.x();
		rowmod = pc.y();
		if ((*hs)(colmod,rowmod) == 0 || (*hm)(colmod,rowmod) == 0 || (*hc)(colmod,rowmod)>15) {
		  tdacs.set(col, row, tdacsPre.get(col, row));
		} 
	      }
	    }
	  }
	  m_modules[pmod]->storeConfig("IncrTdacPreviousConfig");
	}
      }

      int modif = 0;
      int zerofit = 0, tozero = 0, td00 = 0, tdp00 = 0;
      for (std::vector<PixFe*>::iterator fe = m_modules[pmod]->feBegin(); fe != m_modules[pmod]->feEnd(); fe++){
       auto fei4 = dynamic_cast<PixFeI4*>(*fe);
       auto fei3 = dynamic_cast<PixFeI3*>(*fe);
       ConfMask<unsigned short int> &tdacs = fei4 ? fei4->readTrim(Fei4::PixReg::TDAC)
                                                  : fei3->readTrim(Fei3::PixReg::TDAC);
        ConfMask<unsigned short int> &tdacsPre = fei4 ?
                                                 (fei4->feConfig("IncrTdacPreviousConfig")).readTrim(Fei4::PixReg::TDAC) :
                                                 (fei3->feConfig("IncrTdacPreviousConfig")).readTrim(Fei3::PixReg::TDAC);
	for (int col=0; col<tdacs.ncol(); col++) {
	  for (int row=0; row<tdacs.nrow(); row++) {
	    unsigned int colmod, rowmod;
	    int ife = (*fe)->number();
	    PixCoord pc = geo.coord(ife,col,row);
	    colmod = pc.x();
	    rowmod = pc.y();
	    int incr = (int)(scn->getLoopVarValues(nloop))[scn->scanIndex(nloop)];
	    if (scn->getLoopVarValuesFree(nloop)) {
	      incr = abs(incr);
	      if (scn->scanIndex(nloop) == 0) {
		if (tdacs.get(col, row) - incr < 0) {
		  tdacsPre.set(col, row, tdacs.get(col, row));
		  tdacs.set(col, row, 0);
		} else {
		  tdacsPre.set(col, row, incr);
		  tdacs.set(col, row, tdacs.get(col, row) - incr);
		}
		modif++;
	      } else {
		if ((*hs)(colmod,rowmod) == 0 || (*hm)(colmod,rowmod) == 0 || (*hc)(colmod,rowmod)>15) {
		  tdacs.set(col, row, tdacs.get(col, row) + tdacsPre.get(col, row));
		  if (tdacsPre.get(col, row) > 0) tdacsPre.set(col, row, tdacsPre.get(col, row) - 1);
                  zerofit++;
		}
		if ((*hm)(colmod,rowmod) > 0 && (*hm)(colmod,rowmod)-4*(*hs)(colmod,rowmod) < 0) {
		  tdacsPre.set(col, row, 0);
                  tozero++;
		} else {
		  if (tdacsPre.get(col, row) > 0) {
		    if (tdacs.get(col, row) - tdacsPre.get(col, row) < 0) {
		      tdacsPre.set(col, row, tdacs.get(col, row));
		      tdacs.set(col, row, 0);
		    } else {
		      tdacs.set(col, row, tdacs.get(col, row) - tdacsPre.get(col, row));
		    }
		    modif++;
		  }
		}
	      }
	      tdp00 = tdacsPre.get(0, 0);
	      td00 = tdacs.get(0, 0);
	    } else {
	      if (scn->scanIndex(nloop) == 0) {
		if (tdacs.get(col, row) + incr > 128) {
		  tdacs.set(col, row, 128);
		} else if (tdacs.get(col, row) + incr < 0) {
		  tdacs.set(col, row, 0);
		} else {
		  tdacs.set(col, row, tdacs.get(col, row) + incr);
		} 
	      } else {
		if ((*hm)(colmod,rowmod) > 0 && (*hm)(colmod,rowmod)-4*(*hs)(colmod,rowmod) > 0) {
		  if (tdacs.get(col, row) + incr > 128) {
		    tdacs.set(col, row, 128);
		  } else if (tdacs.get(col, row) + incr < 0) {
		    tdacs.set(col, row, 0);
		  } else {
		    tdacs.set(col, row, tdacs.get(col, row) +incr);
		  } 
		}
	      }
	      hTdac->set(colmod, rowmod, tdacs.get(col, row));
	    }
	  }
	}
      }
      if (scn->getLoopVarValuesFree(nloop)) {
	if (modif == 0) scn->terminate(nloop);
	std::cout << "+++" << std:: dec << modif << " " << zerofit << " " << tozero << " " << td00 << " " << tdp00 << std::endl;
      }
      m_pixCtrl->writeModuleConfig(*(m_modules[pmod]),true);
      //      m_modules[pmod]->writeConfig(); //download the config again
    }   
  }
  m_configSent = true;
}

void PixModuleGroup::endFDACFastTuning(int nloop, PixScan *scn, PixHistoServerInterface *hInt, std::string folder) {
  
  //The histograms from the last two steps are loaded and the fdac value for which the tot is closest to 
  //the target is set

  //Available range 0-15 (for Fei4), 0-7 for (Fei3)
  //Pixel that can't be tuned will be set to 15/7
  //Untuned pixels have tot < trg at larger tot setting
  
  std::stringstream info;
  
  
  // Module loop
  for (unsigned int pmod=0; pmod<m_modules.size();pmod++) {
    if (m_modules[pmod]->m_readoutActive){

      unsigned int mod = m_modules[pmod]->m_moduleId;
      PixGeometry geo = m_modules[pmod]->geometry();
      bool isFei4 = isFEI4(m_modules[pmod]->feFlavour());

      Histo *hTot, *hTotBest, *hFdac;
      
      hTot     = &scn->getHisto(PixScan::TOT_MEAN,this,mod,scn->scanIndex(2),scn->scanIndex(1),scn->getLoopVarNSteps(0)-1);
      hTotBest = &scn->getHisto(PixScan::TOT_MEAN,this,mod,scn->scanIndex(2),scn->scanIndex(1)+1,0);
      hFdac    = &scn->getHisto(PixScan::FDAC_T,this,mod,scn->scanIndex(2),scn->scanIndex(1),0);

      int trg = scn->getTotTargetValue(m_modules[pmod]->connName() );
      // FE Loop

      for (std::vector<PixFe*>::iterator fe = m_modules[pmod]->feBegin(); fe != m_modules[pmod]->feEnd(); ++fe){
	
	//do not change FDACs of fe that are not scan-enabled
	
	Config &cfg = (*fe)->config();
	//PixFeConfig &old_cfg = (*fe)->feConfig("PreScanConfig");
	{ConfObj &cfg_obj = cfg["Misc"]["ScanEnable"];
	  if (cfg_obj.type() == ConfObj::BOOL) {
	    if (static_cast<ConfBool&>(cfg_obj).m_value == false) continue;
	  }}
	{ConfObj &cfg_obj = cfg["Misc"]["DacsEnable"];
	  if (cfg_obj.type() == ConfObj::BOOL) {
	    if (static_cast<ConfBool&>(cfg_obj).m_value == false) continue;
	  }}

        auto fei4 = dynamic_cast<PixFeI4*>(*fe);
        auto fei3 = dynamic_cast<PixFeI3*>(*fe);
        ConfMask<unsigned short int> &fdacs = fei4 ? fei4->readTrim(Fei4::PixReg::FDAC)
                                                   : fei3->readTrim(Fei3::PixReg::FDAC);
	//ConfMask<unsigned short int> &old_fdacs =  old_cfg.readTrim("FDAC");
	for (int col = 0; col < fdacs.ncol(); col++) {
	  for (int row=0; row < fdacs.nrow(); row++) {
	    
	    unsigned int colmod, rowmod;
	    int ife = (*fe)->number();
	    PixCoord pc = geo.coord(ife,col,row);
	    colmod = pc.x();
	    rowmod = pc.y();
	    
	    if ( isPixelInMaskSteps(col, row, scn) ) {
	      
	      int fdac = fdacs.get(col,row);
	      int fdac_best = (int)(*hFdac)(colmod,rowmod);
	      int tot  = (*hTot)(colmod,rowmod);
	      int tot_best = (*hTotBest)(colmod,rowmod);
	      
	      if (abs(tot-trg) > abs(tot_best - trg)) {
		fdac = fdac_best;
		tot  = tot_best;
	      }
	      
	      if (fdac<0)
		fdac=0;
	      if (isFei4 && fdac > 15)
		fdac=15;
	      if (!isFei4 && fdac > 7)
		fdac=7;
	      fdacs.set(col,row,fdac);
	      
	      hFdac->set(colmod,rowmod,fdacs.get(col,row));
	      hTotBest->set(colmod,rowmod,tot);

	    }//only active pixels
	  }//col
	}//row
      }//fe loop
      
      m_modules[pmod]->changedCfg();

      writeAndDeleteHisto(scn,hInt,folder,mod,(int)PixScan::TOT_MEAN,scn->scanIndex(2),scn->scanIndex(1),scn->getLoopVarNSteps(0)-1);
      writeAndDeleteHisto(scn,hInt,folder,mod,(int)PixScan::FDAC_T,scn->scanIndex(2),scn->scanIndex(1),0);
      writeAndDeleteHisto(scn,hInt,folder,mod,(int)PixScan::TOT_MEAN,scn->scanIndex(2),scn->scanIndex(1)+1,0);
      
      if (scn->getCloneModCfgTag()) {
	std::cout<<"INFO::Writing config to "<<pixConnectivity()->getModCfgTag()+scn->getTagSuffix()<<std::endl;
	m_modules[pmod]->writeConfig(m_dbserverI,"Configuration-"+pixConnectivity()->getConnTagOI(),pixConnectivity()->getModCfgTag()+scn->getTagSuffix(),"_Tmp");
      }
      
    }//readout active
  }//module loop
}

void PixModuleGroup::endTDACFastTuning(int nloop, PixScan *scn, PixHistoServerInterface *hInt, std::string folder) {

  // The histograms from the last two steps are loaded and the tdac value for which the occupancy
  // was closest to 50% is set
  //
  // The available range for tdac settings is 10 to 117
  // Pixel that can't be tuned will be set to 117
  //   criterion here is less than 50% occupancy even at lowest setting
  
  int tdac_min_gen = (int)(scn->getLoopVarValues(nloop))[0];
  for (int step = 1; step < scn->getLoopVarNSteps(nloop); step++){
    tdac_min_gen -= (int)(scn->getLoopVarValues(nloop))[step];
  }
  
  //  std::cout << "General minimum: " << tdac_min_gen << std::endl;
  //  std::string appl_name = "TDACFastTuning";
  //  info <<"Pixels with TDAC < " << tdac_min_gen << " will be set to 127!";
  //  m_msg->publishMessage(PixMessages::INFORMATION, appl_name, info.str());
  
  for (auto module: m_modules) {
    if(!(module->m_readoutActive)) continue;
    auto const mod = module->m_moduleId;
    PixGeometry geo = module->geometry();
    bool const isFei4 = isFEI4(module->feFlavour());

    Histo* hOcc = &scn->getHisto(PixScan::OCCUPANCY, this, mod, scn->scanIndex(2), scn->scanIndex(1), scn->getLoopVarNSteps(0)-1);
    Histo* hOccBest = &scn->getHisto(PixScan::OCCUPANCY, this, mod, scn->scanIndex(2), scn->scanIndex(1)+1, 0);
    Histo* hTdac = &scn->getHisto(PixScan::TDAC_T, this, mod, scn->scanIndex(2), scn->scanIndex(1), 0 );
  
    int const nCol= module->nCol();
    int const nRow= module->nRow();
    Histo *hFail = nullptr;
    if (scn->getHistogramFilled(PixScan::TUNING_FAIL)) {
      hFail = new Histo("FAIL", "TDAC Tuning Failed", nCol, -0.5, (float)nCol-0.5f, nRow, -0.5, (float)nRow-0.5f);
      scn->addHisto(*hFail, PixScan::TUNING_FAIL, mod, scn->scanIndex(2), scn->scanIndex(1), 0);
    }

    for(auto fe: *module){
      // do not asjust TDACS of front-ends which are not scan-enabled and
      // for which the dacs are not to be enabled
      if(!isFeActive(fe)) continue;

      auto fei4 = dynamic_cast<PixFeI4*>(fe);
      auto fei3 = dynamic_cast<PixFeI3*>(fe);
      ConfMask<unsigned short int> &tdacs = fei4 ? fei4->readTrim(Fei4::PixReg::TDAC)
                                                 : fei3->readTrim(Fei3::PixReg::TDAC);
      //ConfMask<unsigned short int> &old_tdacs = old_cfg.readTrim("TDAC");
      ConfMask<unsigned short int> &old_tdacs = fei4 ? (fei4->feConfig("PreScanConfig")).readTrim(Fei4::PixReg::TDAC) :
                                                       (fei3->feConfig("PreScanConfig")).readTrim(Fei3::PixReg::TDAC);
      for(int col=0; col<tdacs.ncol(); col++) {
        for(int row=0; row<tdacs.nrow(); row++) {
          // should only change tdacs of enabled pixel
          //	    if (!hitbus.get(col, row)) continue;
          auto const ife = fe->number();
          PixCoord pc = geo.coord(ife,col,row);
          auto const colmod = pc.x();
          auto const rowmod = pc.y();

          if ( isPixelInMaskSteps(col, row, scn) ) {
            int tdac = tdacs.get(col, row);
            int const tdac_best = (int)(*hTdac)(colmod, rowmod); 
            double occ = (*hOcc)(colmod, rowmod);		
            double const occ_best = (*hOccBest)(colmod, rowmod);
            double const events = (double)(scn->getRepetitions());	    
            
            if( fabs(occ-events/2) > fabs(occ_best-events/2) ) {
	      tdac = tdac_best;
      	      occ = occ_best;
            }
            
	    bool hitRange = false;
            // pixels that could not be tuned are rather set to higher values  
            int tdac_min = tdac_min_gen;
            if (scn->tuningStartsFromCfgValues()) {
      	      tdac_min += old_tdacs.get(col, row) - (int)(scn->getLoopVarValues(nloop))[0];
            }
            if ((tdac <= tdac_min || tdac < (isFei4?0:10)) && (occ < events/2)) {
      	      tdac = (isFei4?31:127);
	      hitRange = true;
            }
            tdacs.set(col, row, tdac);

            // added a simple prevention for values near the end of the available range 
            // todo: instead of leaving them at the end of the range it might be better to leave at prior values
            if (tdacs.get(col, row) > (isFei4?31:127)) {
	      tdacs.set(col, row, (isFei4?31:127));
	      hitRange = true;
	    } else if (tdacs.get(col, row) < (isFei4?0:10)) {
              tdacs.set(col, row, (isFei4?0:10));
	      hitRange = true;
	    }

	    if(hFail && hitRange) {
                hFail->set(colmod, rowmod, 1);
	    }
            hTdac->set(colmod, rowmod, tdacs.get(col,row));
            hOccBest->set(colmod, rowmod, occ);
          }
        }//row loop	
      }//col loop 
    }//FE loop
    module->changedCfg(); 

    //save and delete histos for this module.
    //anything leftover will be steered by the PixScanTask
    writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::OCCUPANCY, scn->scanIndex(2), scn->scanIndex(1), scn->getLoopVarNSteps(0) - 1 );
    std::cout << "PixModuleGroup::endTDACFastTuning succesfully written histo OCCUPANCY " 
      	<< scn->scanIndex(2) << "/" << scn->scanIndex(1)  << "/" << scn->getLoopVarNSteps(0)-1 << std::endl;
    writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::TDAC_T, scn->scanIndex(2), scn->scanIndex(1), 0 );
    std::cout << "PixModuleGroup::endTDACFastTuning succesfully written histo TDAC_T " 
      	<< scn->scanIndex(2) << "/" << scn->scanIndex(1)  << "/" << 0 << std::endl;  
    writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::OCCUPANCY, scn->scanIndex(2), scn->scanIndex(1)+1, 0 );
    std::cout << "PixModuleGroup::endTDACFastTuning succesfully written histo OCCUPANCY " 
      	<< scn->scanIndex(2) << "/" << scn->scanIndex(1)+1  << "/" << 0 << std::endl;

    if (scn->getCloneModCfgTag()) {
      std::cout << "Writing config to " << pixConnectivity()->getModCfgTag()+scn->getTagSuffix() << std::endl;
      module->writeConfig(m_dbserverI, "Configuration-"+pixConnectivity()->getConnTagOI(), pixConnectivity()->getModCfgTag()+scn->getTagSuffix(), "_Tmp");
    }
  }//module loop
}

void PixModuleGroup::endThrFastScan(int nloop, PixScan *scn, PixHistoServerInterface *hInt, std::string folder){
	std::stringstream info;
	
	std::cout << "PixModuleGroup::endThrFastScan"<<endl;
	
	// these wil store reduced range for threshold scan in the end
	m_vcalMin = 200;
	m_vcalMax = 0;

	//Module Loop
	for (unsigned int pmod = 0; pmod<m_modules.size(); pmod++) {
		if (m_modules[pmod]->m_readoutActive) {
			unsigned int mod = m_modules[pmod]->m_moduleId;
			PixGeometry geo = m_modules[pmod]->geometry();
			bool isFei4 = isFEI4(m_modules[pmod]->feFlavour());
			
			Histo *hVcal;
			std::vector<Histo*> hOccp;
			hVcal =&scn->getHisto(PixScan::SCURVE_MEAN, this, mod, scn->scanIndex(2), scn->scanIndex(1), 0);
			const int nscanpts = scn->getLoopVarNSteps(0);
			for(int i=0;i<nscanpts; i++)
			  hOccp.push_back(&scn->getHisto(PixScan::OCCUPANCY, this, mod, scn->scanIndex(2), scn->scanIndex(1), i ));
			
			//FE Loop
			for (std::vector<PixFe*>::iterator fe = m_modules[pmod]->feBegin(); fe!= m_modules[pmod]->feEnd(); fe++) {
			  //get parameters to calculate q from vcal			
			  float vcal_a = static_cast<ConfFloat &>((*fe)->config()["Misc"]["VcalGradient3"]).value();
			  float vcal_b = static_cast<ConfFloat &>((*fe)->config()["Misc"]["VcalGradient2"]).value();
			  float vcal_c = static_cast<ConfFloat &>((*fe)->config()["Misc"]["VcalGradient1"]).value();
			  float vcal_d = static_cast<ConfFloat &>((*fe)->config()["Misc"]["VcalGradient0"]).value();
			  float cInj   = static_cast<ConfFloat &>((*fe)->config()["Misc"]["CInjLo"]).value();
			  int cInjId = 0;
			  if ((!isFei4 && scn->getChargeInjCapHigh()) || (isFei4 && scn->getMaskStageMode()==PixScan::FEI4_ENA_BCAP)){
			    cInj = static_cast<ConfFloat &>((*fe)->config()["Misc"]["CInjHi"]).value();
			    cInjId = 2;
			  }
			  else if(isFei4 && scn->getMaskStageMode()==PixScan::FEI4_ENA_LCAP){
			    cInj = static_cast<ConfFloat &>((*fe)->config()["Misc"]["CInjMed"]).value();
			    cInjId = 1;
			  }

			  // loop over all pixels: check avg. occ. of last two scan points and choos VCAL closest to target
			  double occl = 0, occp = 0., nent1 = 0;
			  unsigned int colmod, rowmod;
			  int ife = (*fe)->number();
			  bool pixel_recalc=scn->getFastThrUsePseudoPix();
			  if(pixel_recalc){
			    // reconstruct VCAL values
			    std::vector<float> loop_val = scn->getLoopVarValues(0);
			    std::vector<int> vcal_steps;
			    vcal_steps.push_back(loop_val[0]);
			    for(int i=0;i<(nscanpts-1); i++){
			      double occ = 0, nent2 = 0;
			      for (int col=0; col<geo.nChipCol(); col++) {
				for (int row=0; row<geo.nChipRow(); row++) {
				  PixCoord pc = geo.coord(ife, col,row);
				  colmod = pc.x();
				  rowmod = pc.y();
				  occ += (*hOccp.at(i))(colmod, rowmod);
				  nent2 += 1.;
				}
			      }
			      if(nent2>0) occ /= nent2;
			      else       occ = 0.;
			      double events = (double)(scn->getRepetitions());
			      float vcal = vcal_steps[i];
			      if (occ/events < 0.5) vcal += loop_val[i+1];
			      else                  vcal -= loop_val[i+1];
			      vcal_steps.push_back(vcal);
			    }
			    // now check for each pixel which is the best occupancy and store corresponding VCAL
			    float vcal_avg = 0., vcal_ssq = 0., n_vcal=0.;
			    for (int col=0; col<geo.nChipCol(); col++) {
			      for (int row=0; row<geo.nChipRow(); row++) {
				PixCoord pc = geo.coord(ife, col,row);
				colmod = pc.x();
				rowmod = pc.y();
				double occ_best = -1;
				for(int i=0;i<nscanpts; i++){
				  double occ = (*hOccp.at(i))(colmod, rowmod);
				  double events = (double)(scn->getRepetitions());
				  if(events>0) occ /= events;
				  if(fabs(occ-0.5) < fabs(occ_best-0.5)){
				    occ_best = occ;
				    hVcal->set(colmod, rowmod, vcal_steps[i]);
				  }
				}
				//calculate q from vcal
				float vcal_best = (*hVcal)(colmod,rowmod);
				float q = 6.241495961*cInj*(((vcal_a*vcal_best + vcal_b)*vcal_best + vcal_c)*vcal_best + vcal_d);
				if(occ_best>.3 && occ_best < 0.7){ // exclude pixels too far away from target
				  hVcal->set(colmod, rowmod, q);
				  vcal_avg += vcal_best;
				  vcal_ssq += vcal_best*vcal_best;
				  n_vcal += 1.;
				} else
				  hVcal->set(colmod, rowmod, -1.);
			      }
			    }
			    // threshold range for reduced normal scan: avg. +- 3*sigma +- 5 (latter to add some extra margin for outliers, noise...)
			    if(n_vcal>1.){
			      vcal_avg /= n_vcal;
			      vcal_ssq = sqrt((vcal_ssq - n_vcal*vcal_avg*vcal_avg)/(n_vcal-1));
			      int vcal_min = (int)(vcal_avg-3.*vcal_ssq+0.5)-5;
			      int vcal_max = (int)(vcal_avg+3.*vcal_ssq+0.5)+5;
			      // restrict vcal to allowed range
			      if(vcal_min<0) vcal_min = 0;
			      if(vcal_max>1023) vcal_max = 1023;
			      if(vcal_min < m_vcalMin) m_vcalMin = vcal_min;
			      if(vcal_max > m_vcalMax) m_vcalMax = vcal_max;
			      std::cout << "PixModuleGroup::endThrFastScan : in pseudo-pixel mode, FE"<<ife<<" had <VCAL>="<<vcal_avg<<" and std.dev="<<vcal_ssq<<
				" resulting in range " << vcal_min<<"..."<<vcal_max<<std::endl;
			    }
			  } else {
			    for (int col=0; col<geo.nChipCol(); col++) {
			      for (int row=0; row<geo.nChipRow(); row++) {
				
				PixCoord pc = geo.coord(ife, col,row);
				colmod = pc.x();
				rowmod = pc.y();
				
				if ( isPixelInMaskSteps(col, row, scn) ) {
				  occl += (*hOccp.at(nscanpts-2))(colmod, rowmod);
				  occp += (*hOccp.at(nscanpts-1))(colmod, rowmod);
				  nent1 += 1.;
				}
			      }
			    }
			    float vcal_corr = 0.;
			    double events = (double)(scn->getRepetitions());
			    if(nent1>0){
			      occl /= nent1;
			      occp /= nent1;
			      if(fabs(occp/events-0.5) < fabs(occl/events-0.5)){ // last step didn't improve, so use previous step's VCAL
				if(occl>occp) vcal_corr = -1.; // VCAL was increased in last step
				else          vcal_corr =  1.; // VCAL was decreased in last step
			      }
			    }
// 			    std::cout << "PixModuleGroup::endThrFastScan: mod " <<pmod<< ", FE"<<ife<<" - last occ. = " <<occl << ", prev. occ. = " << occp
// 				      << ", VCAL corr. = " << vcal_corr << std::endl;
			    for (int col=0; col<geo.nChipCol(); col++) {
			      for (int row=0; row<geo.nChipRow(); row++) {
				
				PixCoord pc = geo.coord(ife, col,row);
				colmod = pc.x();
				rowmod = pc.y();
				// retrieve and correct VCAL
				float vcal_best = (*hVcal)(colmod,rowmod);
				vcal_best += vcal_corr;
				//calculate q from vcal
				float q = 6.241495961*cInj*(((vcal_a*vcal_best + vcal_b)*vcal_best + vcal_c)*vcal_best + vcal_d);
				hVcal->set(colmod, rowmod, q);
				if(col==0 && row==0){ // just need to do this once, all other pixels will be identical anyway
				  int vcal_min = (int)(getVCALfromCharge((q>1000.)?(q-1000.):0., *fe, cInjId)+0.5);
				  int vcal_max = (int)(getVCALfromCharge((q+1500.), *fe, cInjId)+0.5);
				  if(vcal_min < m_vcalMin) m_vcalMin = vcal_min;
				  if(vcal_max > m_vcalMax) m_vcalMax = vcal_max;
				  std::cout << "PixModuleGroup::endThrFastScan : in FE-avg.-mode, FE"<<ife<<" had VCAL range " << vcal_min<<"..."<<vcal_max<<std::endl;
				}
			      }
			    }
			  }
			}
			//save and delete histos for this module.
			//anything leftover will be steered by the PixScanTask
			writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::SCURVE_MEAN, scn->scanIndex(2), scn->scanIndex(1), 0 ); 
			//std::cout << "PixModuleGroup::endThrFastScan succesfully written histo SCURVE_MEAN " << scn->scanIndex(2) << "/" << scn->scanIndex(1) << "/" << (scn->scanIndex(0)) << std::endl;
		}
	}

  // fall-back if VCAL range algorithm fails
  if(m_vcalMax < m_vcalMin){
    m_vcalMax = 200.;
    m_vcalMin = 0.;
  }
  std::cout << "PixModuleGroup::endThrFastScan : storing VCAL range " << m_vcalMin << "..." << m_vcalMax << std::endl;
}


void PixModuleGroup::endTDACTuning(int nloop, PixScan *scn) {

  // Check if the loop is executed on the host
  if (!scn->getDspLoopAction(nloop)) {
    Histo *hm=nullptr, *hThr=nullptr, *hTdac=nullptr;
 
    // Loop on modules
    for (unsigned int pmod=0; pmod<m_modules.size(); pmod++) {
      if (m_modules[pmod]->m_readoutActive) {
      int trg = scn->getThresholdTargetValue(m_modules[pmod]->connName());
	unsigned int mod = m_modules[pmod]->m_moduleId;
	PixGeometry geo = m_modules[pmod]->geometry();
	if (nloop == 1) {
	  hThr = &scn->getHisto(PixScan::TDAC_THR, this, mod, scn->scanIndex(2), 0, 0);
	  hTdac = &scn->getHisto(PixScan::TDAC_T, this, mod, scn->scanIndex(2), 0, 0);
	  hm = &scn->getHisto(PixScan::SCURVE_MEAN, this, mod, scn->scanIndex(2), scn->scanIndex(1), 0);
	  
	} else if (nloop == 2) {
	  hThr = &scn->getHisto(PixScan::TDAC_THR, this, mod, 0, 0, 0);
	  hTdac = &scn->getHisto(PixScan::TDAC_T, this, mod, 0, 0, 0);
	  hm = &scn->getHisto(PixScan::SCURVE_MEAN, this, mod, scn->scanIndex(2), 0, 0);
	}
	// FE loop
	for (std::vector<PixFe*>::iterator fe = m_modules[pmod]->feBegin(); fe != m_modules[pmod]->feEnd(); fe++){
      auto fei4 = dynamic_cast<PixFeI4*>(*fe);
      auto fei3 = dynamic_cast<PixFeI3*>(*fe);
      ConfMask<unsigned short int> &tdacs = fei4 ? fei4->readTrim(Fei4::PixReg::TDAC)
                                                 : fei3->readTrim(Fei3::PixReg::TDAC);
	  for (int col=0; col<tdacs.ncol(); col++) {
	    for (int row=0; row<tdacs.nrow(); row++) {
	      unsigned int colmod, rowmod;
	      int ife = (*fe)->number();
	      PixCoord pc = geo.coord(ife,col,row);
	      colmod = pc.x();
	      rowmod = pc.y();
	      if (abs((*hm)(colmod,rowmod)-trg) < abs((*hThr)(colmod,rowmod)-trg)) {
		hThr->set(colmod, rowmod, (*hm)(colmod,rowmod));
		hTdac->set(colmod, rowmod, tdacs.get(col, row));
	      }
	      // Save in the FEs TDAC tuned values
	      tdacs.set(col, row, (int)(*hTdac)(colmod,rowmod)); 
	    }
	  }
	}
	m_modules[pmod]->changedCfg();
	if (scn->getCloneModCfgTag()) {
	  std::cout << "Writing config to " << pixConnectivity()->getModCfgTag()+scn->getTagSuffix() << std::endl;
	  m_modules[pmod]->writeConfig(m_dbserverI, "Configuration-"+pixConnectivity()->getConnTagOI(), pixConnectivity()->getModCfgTag()+scn->getTagSuffix(), "_Tmp");
	}
      }
    }
  }
}


void PixModuleGroup::endGDACTuning(int nloop, PixScan *scn) {
  std::cout << m_name << ": executing endGDACTuning at loop: " << nloop << " with indices " << scn->scanIndex(2) << " " << scn->scanIndex(1) << std::endl;
  // Check if the loop is executed on the host
  if (scn->getDspLoopAction(nloop)) return;

  for (auto module: m_modules) {
    if (!module->m_readoutActive) continue;
    int const nCol= module->nCol();
    int const nRow= module->nRow();
    PixGeometry geo = module->geometry();
    int const nFePerCol = nCol/(module->nChipCol());
    int const nFErows = nRow/(module->nChipRow());
    int const nFe = module->nChip();
    bool const isFei4 = isFEI4(module->feFlavour());
    auto const mod = module->m_moduleId;

    std::array<int,3> indices = {-1,-1,-1};
    if (nloop == 0) {
      indices[0] = scn->scanIndex(2);
      indices[1] = scn->scanIndex(1);
    }
    else if (nloop == 1) {
      indices[0] = scn->scanIndex(2);
    }

    Histo* hThr = nullptr;
    if (scn->getHistogramFilled(PixScan::GDAC_THR)) {
      hThr = new Histo("Threshold", "Threshold", nCol, -0.5, (float)nCol-0.5f, nRow, -0.5, (float)nRow-0.5f);
      scn->addHisto(*hThr, PixScan::GDAC_THR, mod, indices[0], indices[1], indices[2]);
    }
    Histo *hGdac = nullptr;
    if (scn->getHistogramFilled(PixScan::GDAC_T)) {
      hGdac = new Histo("GDAC", "Gdac Tuning", nFePerCol, -0.5, -0.5+(double)nFePerCol, nFErows, -0.5, -0.5+(double)nFErows);
      scn->addHisto(*hGdac, PixScan::GDAC_T, mod, indices[0], indices[1], indices[2]);
    }
    Histo *hFail = nullptr;
    if (scn->getHistogramFilled(PixScan::TUNING_FAIL)) {
      hFail = new Histo("FAIL", "Gdac Tuning Failed", nFePerCol, -0.5, -0.5+(double)nFePerCol, nFErows, -0.5, -0.5+(double)nFErows);
      scn->addHisto(*hFail, PixScan::TUNING_FAIL, mod, indices[0], indices[1], indices[2]);
    }
    // Create matrix for storing FE mean threshold values for each step
    std::vector<std::vector<double>> meanmat(nFe, std::vector<double>(scn->getLoopVarNSteps(nloop), 0.f));
    // Create vector for storing GDAC values for each step
    std::vector<int> GDACvec(scn->getLoopVarNSteps(nloop));

    // Get target threshold 
    int trg = scn->getThresholdTargetValue(module->connName());
    
    // Load protection values in each FE
    int const thrMin = isFei4?1000:2000; 
    int const thrMax = 7000; 
    int const gdacMin = scn->getLoopVarValues(nloop)[0];    
    int const gdacMax = isFei4?0xffff:31;   

    // Create vectors and matrix for storing FE mean H&L threshold values and GDAC H&L  
    std::vector<double> thrFEL(nFe, thrMin);
    std::vector<double> thrFEH(nFe, thrMax);
    std::vector<int> GdacL(nFe, gdacMin);
    std::vector<int> GdacH(nFe, gdacMax);
    std::vector<int> H(nFe);
    std::vector<int> L(nFe);

    // Loop on steps (finding the mean threshold in each FE)
    for (int step = 0; step<scn->getLoopVarNSteps(nloop); step++){
      Histo* hm = nullptr;
      // Direction determination of GDAC values
      int dir = 0; // Increasing GDAC values
      if (scn->getLoopVarValues(nloop)[0] > scn->getLoopVarValues(nloop)[1]) dir = 1; // Decreasing GDAC values
      // Check if gdac tuning is done in loop 1 or 2
      if (nloop == 1){
        hm      = &scn->getHisto(PixScan::SCURVE_MEAN, this, mod, scn->scanIndex(2), step, 0);
      } else if (nloop == 2){
        hm      = &scn->getHisto(PixScan::SCURVE_MEAN, this, mod, step, 0, 0);
      }
      // Loop on FEs
      for (auto fe: *module){ 
        int ife = fe->number();
        if(ife>=nFe) break; // throw an exception?

        auto c1 = geo.coord(ife, 0, 0);
        auto c2 = geo.coord(ife, module->nChipCol()-1, module->nChipRow()-1);

        auto [sum, entries] = hm->get_sum_cut(std::min(c1.x(), c2.x()), std::max(c1.x(), c2.x()),
    		                          std::min(c1.y(), c2.y()), std::max(c1.y(), c2.y()),
    		                          [](double d){return d>0.;});

        double const mean = (entries != 0) ? sum/entries : 0.;

        // Save values in matrix
        meanmat[ife][step] = mean; // Mean threshold values for each FE in each step
        GDACvec[step] = (scn->getLoopVarValues(nloop))[step]; // GDAC values for each FE in each step
  
        if (mean <= trg && mean > thrFEL[ife]) {  // Update low value
          thrFEL[ife] = mean;        
          GdacL[ife] = (scn->getLoopVarValues(nloop)[step]);
          L[ife] = step; 
        } else if (mean > trg && mean < thrFEH[ife]) { // Update high value  
          thrFEH[ife] = mean;        
          GdacH[ife] = (scn->getLoopVarValues(nloop)[step]);
          H[ife] = step;
        }   
        // If you are in the last step, you have to interpolate between Low and High values to find the tuned value of Gdac
        // There are three cases we can encounter - either we have measurement points lower and higher than the target, then 
        // we simple interpolate. Or we only have higher/lower measurement point, then we extrapolate. There it depends if 
        // we have made the scan with increasing or decreasing GDAC values (dir). Also, we have to differentiate if we are
        // in loop 1 or 2
        //
        if (step == (scn->getLoopVarNSteps(nloop)-1)) {
          int GdacI = 0;
	  bool hitLimit = false;
          // If FE is working
          if (std::any_of(meanmat[ife].cbegin(), meanmat[ife].cend(), [](double d){return d != 0.;})){
            Histo* histLow = nullptr;
            Histo* histHigh = nullptr;
            double tLow = 0;
            double tHigh = 0;
            int gLow = 0;
            int gHigh = 0;

            // If scan has found a value higher than trg and one lower
            if(thrFEL[ife]<=trg && thrFEH[ife]>=trg && thrFEL[ife]!=thrMin && thrFEH[ife]!=thrMax ) { 
              gLow = GdacL[ife];
              gHigh = GdacH[ife];
              tLow = thrFEL[ife];
              tHigh = thrFEH[ife];
              if (nloop == 1){
       	        histLow = &scn->getHisto(PixScan::SCURVE_MEAN, this, mod, scn->scanIndex(2), L[ife], 0);	
       	        histHigh = &scn->getHisto(PixScan::SCURVE_MEAN, this, mod, scn->scanIndex(2), H[ife], 0);
       	      } else if (nloop == 2){
       	        histLow = &scn->getHisto(PixScan::SCURVE_MEAN, this, mod, L[ife], 0, 0);
       	        histHigh = &scn->getHisto(PixScan::SCURVE_MEAN, this, mod, H[ife], 0, 0);
       	      }
            } else if( thrFEL[ife] == thrMin || thrFEH[ife] == thrMax ) { //or we're below/above the measured regin
       	      int lowIndex = 0;
       	      int highIndex = 0;
              if( thrFEL[ife] == thrMin ){
                if(dir==0) {
                  lowIndex = 0;
                  highIndex = 1;
       	        } else {
                  lowIndex = step;
                  highIndex = step-1;
                }
              } else if( thrFEH[ife] == thrMax ){
                if(dir==0) {
                  lowIndex = step-1;
                  highIndex = step;
                } else {
                  lowIndex = 1;
                  highIndex = 0;
                }
              }

              gLow = GDACvec[lowIndex];	      
              gHigh = GDACvec[highIndex];
              tLow = meanmat[ife][lowIndex];      
              tHigh = meanmat[ife][highIndex];      
              
              if (nloop == 1){
                histLow = &scn->getHisto(PixScan::SCURVE_MEAN, this, mod, scn->scanIndex(2), lowIndex, 0);	
                histHigh = &scn->getHisto(PixScan::SCURVE_MEAN, this, mod, scn->scanIndex(2), highIndex, 0);
              } else if (nloop == 2){
                histLow = &scn->getHisto(PixScan::SCURVE_MEAN, this, mod, lowIndex, 0, 0);
                histHigh = &scn->getHisto(PixScan::SCURVE_MEAN, this, mod, highIndex, 0, 0);
              }      
            }

            auto slope = (double)(gHigh-gLow)/(double)(tHigh-tLow);
            auto deltaThreshold = trg - tLow;
            int newGDAC = std::round(gLow + deltaThreshold*slope);
            //std::cout << "New GDAC would be: " << newGDAC << "\n";
           
            if(hThr){
              auto deltaGDAC = newGDAC - gLow;
              for(int col=0; col<module->nChipCol(); col++){
                for(int row=0; row<module->nChipRow(); row++){
                  auto coord = geo.coord(ife,col,row);
                  auto colmod = coord.x();
                  auto rowmod = coord.y();
                  auto sMeanLow = (*histLow)(colmod,rowmod);
                  auto sMeanHigh = (*histHigh)(colmod,rowmod);
                  if (sMeanLow > 0 && sMeanHigh > 0){
                    auto sDelta = sMeanHigh - sMeanLow;
                    auto gdacSlope = sDelta/(gHigh-gLow);
                    auto newThr = sMeanLow+gdacSlope*deltaGDAC;
                    hThr->set(colmod, rowmod, newThr);
                  }
     	        }
              }
            }

            if(newGDAC > gdacMax) {
	      newGDAC = gdacMax; // Protection
	      hitLimit = true;
	    } else if(newGDAC < gdacMin) {
              newGDAC = gdacMin;  // Protection
	      hitLimit = true;
	    }
            GdacI = newGDAC;
          }

	  int const colfe = (ife < nFePerCol) ? ife : nFe-1-ife;
	  int const rowfe = (ife < nFePerCol) ? 0 : 1;
          if(hGdac) {
	    // Fill histo of mean threshold for each FE
	    hGdac->set(colfe, rowfe, GdacI);
	  }
	  if(hFail) {
            hFail->set(colfe, rowfe, (int)hitLimit);
	  }

          // Save in the GDAC tuned values in the configuration
          if(isFei4){
            static_cast<PixFeI4*>(fe)->writeGlobRegister(Fei4::GlobReg::Vthin_Fine, GdacI&0xff);
            //DO NOT SET Vthin_Coarse, DURING TUNING, DEVELOP GDAC_COARSE SCAN OR SET MANUALLY
            //static_cast<PixFeI4*>(fe)->writeGlobRegister(Fei4::GlobReg::Vthin_Coarse, (GdacI&0x7f00)>>7);
          } else {
            static_cast<PixFeI3*>(fe)->writeGlobRegister(Fei3::GlobReg::GlobalTDAC, GdacI&0x1f);
          }
        }//process the last step loop
      }//FE loop
    }//scan step loop
    
    module->changedCfg();
    if(scn->getCloneModCfgTag()) {
      module->writeConfig(m_dbserverI, "Configuration-"+pixConnectivity()->getConnTagOI(), pixConnectivity()->getModCfgTag()+scn->getTagSuffix(), "_Tmp");
    }
  }//module loop
}

void PixModuleGroup::endGDACTuningInterpolate(int nloop, PixScan *scn, bool iterated) {
  std::cout << m_name << ": executing endGDACTuningInterpolate at loop: " << nloop << " with indices " << scn->scanIndex(2) << " " << scn->scanIndex(1) << std::endl;

  for (auto module: m_modules) {
    if (!module->m_readoutActive) continue;
    int const nCol= module->nCol();
    int const nRow= module->nRow();
    PixGeometry geo = module->geometry();
    int const nFePerCol = nCol/(module->nChipCol());
    int const nFErows = nRow/(module->nChipRow());
    int const nFe = module->nChip();
    bool const isFei4 = isFEI4(module->feFlavour());
    auto const mod = module->m_moduleId;

    //Target occupancy should be 50% of triggers during standart tuning
    double target = scn->getRepetitions()/2.;
    //Test for noise Scan (noise threshold turning) 
    if(scn->getSelfTrigger())target = scn->getRepetitions()/100.; //Noise Scan
    float bestDiff=scn->getRepetitions();
    int bestIndex = 0;

    Histo *hOcc = nullptr;
    Histo *hGdac = nullptr;
    Histo *hFail = nullptr;
      if (scn->getHistogramFilled(PixScan::TUNING_FAIL)) {
        hFail = new Histo("FAIL", "Gdac Tuning Failed", nFePerCol, -0.5, -0.5+(double)nFePerCol, nFErows, -0.5, -0.5+(double)nFErows);
        scn->addHisto(*hFail, PixScan::TUNING_FAIL, mod, scn->scanIndex(2), scn->scanIndex(1), 0);
      }
      if (scn->getHistogramFilled(PixScan::GDAC_T)) {
        hGdac = new Histo("GDAC", "Gdac Tuning", nFePerCol, -0.5, -0.5+(double)nFePerCol, nFErows, -0.5, -0.5+(double)nFErows);
        scn->addHisto(*hGdac, PixScan::GDAC_T, mod, 0,0,0);
      }
      for (auto fe: *module){
        int ife = fe->number();
        if(ife>=nFe) break; // throw an exception?
        std::vector<float> mean (scn->getLoopVarNSteps(nloop),0);
	for (int index=0; index<scn->getLoopVarNSteps(nloop); index++) { // Loop on IF values
	   if (nloop == 0) {
             hOcc = &scn->getHisto(PixScan::OCCUPANCY, this, mod, scn->scanIndex(2), scn->scanIndex(1), index);
	   } else if (nloop == 1) {
	     hOcc = &scn->getHisto(PixScan::OCCUPANCY, this, mod, scn->scanIndex(2), index, 0);
	   } else if (nloop == 2){
	     hOcc = &scn->getHisto(PixScan::OCCUPANCY, this, mod, index, 0, 0);
	   }
	  //std::cout<<hTrg->name()<<std::endl;
          int count = 0;
	    for (unsigned int col=0; col<(unsigned int)module->nChipCol(); col++){
	      for (unsigned int row=0; row<(unsigned int)module->nChipRow(); row++){
	        unsigned int colmod, rowmod;
	        PixCoord pc = geo.coord(ife,col,row);
	        colmod = pc.x();
	        rowmod = pc.y();
  		  if ( isPixelInMaskSteps(col, row, scn) ) {
		    mean[index] += (*hOcc)(colmod, rowmod);
		    count++;
	          }
	      }
	    }
	    if(count)mean[index] /= count;
	      if(std::abs(mean[index]-target) < bestDiff){
	        bestDiff = std::abs(mean[index]-target);
	        bestIndex=index;
	      }
	    //std::cout<<module->connName()<<" FE "<<ife<<" "<<scn->getLoopVarValues(nloop)[index]<<" "<<mean[index]<<std::endl;
	  }

	//Interpolate best IF between 2 best values
        float result=-1;
        int bestGDAC = isFei4?255:31;//Set High GDAC thr in case of failure

        if(PixFitFitter_lmfit::interpolate(scn->getLoopVarValues(nloop), mean, target, result ) ){
          bestGDAC = std::round(result);
        } else if (iterated && bestIndex>0){
          bestGDAC = scn->getLoopVarValues(nloop)[bestIndex];
          PIX_WARNING(module->connName()<<" FE "<<ife<<" GDAC_TUNE interpolation failed, setting threshold to closest target value "<<bestGDAC );
           if (hFail)hFail->set((ife % nFePerCol),(ife < nFePerCol)?0:1, 1);
        } else {
          PIX_ERROR(module->connName()<<" FE "<<ife<<" GDAC_TUNE failed, setting threshold high");
          if (hFail)hFail->set((ife % nFePerCol),(ife < nFePerCol)?0:1, 1);
        }

	//std::cout<<__PRETTY_FUNCTION__<<" "<<module->connName()<<" FE "<<ife<<" bestGDAC "<<bestGDAC<<" "<<target<<std::endl;

	hGdac->set( (ife % nFePerCol),(ife < nFePerCol)?0:1, bestGDAC );

          if(isFei4){
            static_cast<PixFeI4*>(fe)->writeGlobRegister(Fei4::GlobReg::Vthin_Fine, bestGDAC&0xff);
            //DO NOT SET Vthin_Coarse DURING TUNING, DEVELOP GDAC_COARSE SCAN OR SET MANUALLY
            //static_cast<PixFeI4*>(fe)->writeGlobRegister(Fei4::GlobReg::Vthin_Coarse, (GdacT&0x7f00)>>7);
          } else {
            static_cast<PixFeI3*>(fe)->writeGlobRegister(Fei3::GlobReg::GlobalTDAC, bestGDAC&0x1f);
          }
      }

    module->changedCfg();
      if(scn->getCloneModCfgTag()) {
        module->writeConfig(m_dbserverI, "Configuration-"+pixConnectivity()->getConnTagOI(), pixConnectivity()->getModCfgTag()+scn->getTagSuffix(), "_Tmp");
      }
  }

}


void PixModuleGroup::endTDACTuningIterated(int nloop, PixScan *scn) {

  std::cout << m_name << ": executing endTDACTuning at loop: " << nloop << " with indices " << scn->scanIndex(2) << " " << scn->scanIndex(1) << std::endl;

  for (auto module: m_modules) {
    if (!module->m_readoutActive) continue;
    int const nCol= module->nCol();
    int const nRow= module->nRow();
    PixGeometry geo = module->geometry();
    int const nFe = module->nChip();
    bool const isFei4 = isFEI4(module->feFlavour());
    auto const mod = module->m_moduleId;

    //Target occupancy should be 50% of triggers during standart tuning
    double target = scn->getRepetitions()/2.;
    //Test for noise Scan (noise threshold turning) 
    if(scn->getSelfTrigger())target = scn->getRepetitions()/100.; //Noise Scan

    Histo *hOcc = nullptr;
    Histo *hTdac = nullptr;
    Histo *hFail= nullptr;
    if (scn->getHistogramFilled(PixScan::TUNING_FAIL)) {
      hFail = new Histo("FAIL", "TDAC Tuning Failed", nCol, -0.5, (float)nCol-0.5f, nRow, -0.5, (float)nRow-0.5f);
      scn->addHisto(*hFail, PixScan::TUNING_FAIL, mod, scn->scanIndex(2), scn->scanIndex(1), 0);
    }
      if (scn->getHistogramFilled(PixScan::TDAC_T)) {
        hTdac = new Histo("TDAC_T", "Tdac Tuning", nCol, -0.5, (float)nCol-0.5f, nRow, -0.5, (float)nRow-0.5f);
        scn->addHisto(*hTdac, PixScan::TDAC_T, mod, 0,0,0);
      }
      for (auto fe: *module){
        int ife = fe->number();
        if(ife>=nFe) break; // throw an exception?
        auto fei4 = dynamic_cast<PixFeI4*>(fe);
        auto fei3 = dynamic_cast<PixFeI3*>(fe);
        ConfMask<unsigned short int> &tdacs = isFei4 ? fei4->readTrim(Fei4::PixReg::TDAC)
                                                 : fei3->readTrim(Fei3::PixReg::TDAC);

	  for (int col=0; col<tdacs.ncol(); col++) {
	    for (int row=0; row<tdacs.nrow(); row++) {
	      unsigned int colmod, rowmod;
	      PixCoord pc = geo.coord(ife,col,row);
	      colmod = pc.x();
	      rowmod = pc.y();
	      std::vector <float> occ (scn->getLoopVarNSteps(nloop),0);

	        for (int index=0; index<scn->getLoopVarNSteps(nloop); index++) { // Loop on TDAC histo
	          if (nloop == 0) {
                    hOcc = &scn->getHisto(PixScan::OCCUPANCY, this, mod, scn->scanIndex(2), scn->scanIndex(1), index);
	          } else if (nloop == 1) {
	            hOcc = &scn->getHisto(PixScan::OCCUPANCY, this, mod, scn->scanIndex(2), index, 0);
	          } else if (nloop == 2){
	            hOcc = &scn->getHisto(PixScan::OCCUPANCY, this, mod, index, 0, 0);
	          }

	          occ[index] = (*hOcc)(colmod, rowmod);
	          //if(col==0 && row==0)std::cout<<module->connName()<<" FE "<<ife<<" "<<scn->getLoopVarValues(nloop)[index]<<" "<<occ[index]<<std::endl;
	        }

	       float result=-1;
	       int TdacT = isFei4?0:127;//Set High TDAC thr in case of failure
	         if(PixFitFitter_lmfit::interpolate(scn->getLoopVarValues(nloop), occ, target, result)){
	           TdacT = std::round(result);
	         } else {
	            if (hFail)hFail->set(colmod, rowmod, 1);
	            if(occ.back() > 0 )TdacT = tdacs.get(col, row);//Not zero but not higher than target
	         }

	       //if(col==0 && row==0)std::cout<<module->connName()<<" FE "<<ife<<" "<<TdacT<<" "<<target<<std::endl;

	       hTdac->set(colmod, rowmod, TdacT);
	       // Save in the FEs TDAC tuned values
	       tdacs.set(col, row, TdacT);
	    }
	  }
      }

     module->changedCfg();
       if (scn->getCloneModCfgTag()) {
         std::cout << "Writing config to " << pixConnectivity()->getModCfgTag()+scn->getTagSuffix() << std::endl;
         module->writeConfig(m_dbserverI, "Configuration-"+pixConnectivity()->getConnTagOI(), pixConnectivity()->getModCfgTag()+scn->getTagSuffix(), "_Tmp");
       }
  }

}


void PixModuleGroup::stepFDACTuning(int nloop, PixScan *scn, PixHistoServerInterface *hInt, std::string folder) {
  
  cout << "DEBUG: PixModuleFroup.cxx::stepFDACTuning nloop="<<nloop<<" idx2="<<scn->scanIndex(2)<<" idx1="<<scn->scanIndex(1)<<" idx0="<<scn->scanIndex(0)<<" NSteps(0)="<<scn->getLoopVarNSteps(0)<<endl;
  for (auto module: m_modules) {
    if(!(module->m_readoutActive)) continue;
    int const nCol = module->nCol();
    int const nRow = module->nRow();
    PixGeometry geo = module->geometry();
    auto const mod = module->m_moduleId;
    // this needs to be done in only after the first step
    // assuming that tuning is done in loop 0

    Histo *hTrg = nullptr;
    if (nloop == 0) {
      hTrg = &scn->getHisto(PixScan::TOT_MEAN, this, mod, scn->scanIndex(2), scn->scanIndex(1), scn->scanIndex(0)-1);
    } else if (nloop == -2){
      hTrg = &scn->getHisto(PixScan::TOT_MEAN, this, mod, scn->scanIndex(2), scn->scanIndex(1), scn->getLoopVarNSteps(0)-1);     
    } else {
      cout << "PixModuleGroup:: ERROR: FDAC Tuning does not run in Loop 0, as it must be!" << endl;
      return;
    }

    if (scn->scanIndex(0) == 0 && nloop != -2){
      hTotHistArr[mod] = new Histo("FDAC_TOT", "Tuned Fdacs ToT", nCol, -0.5, (float)nCol-0.5f, nRow, -0.5, (float)nRow-0.5f);
      hFdacHistArr[mod] = new Histo("FDAC_T", "Tuned Fdacs", nCol, -0.5, (float)nCol-0.5f, nRow, -0.5, (float)nRow-0.5f);

      // Associate different parameters if the fcad tuning is done in loop 0, 1 or 2
      if (nloop == 0) {
        scn->addHisto(*hTotHistArr[mod], PixScan::FDAC_TOT, mod, scn->scanIndex(2), scn->scanIndex(1), -1);
        scn->addHisto(*hFdacHistArr[mod], PixScan::FDAC_T, mod, scn->scanIndex(2), scn->scanIndex(1), -1);
      } else {
        cout << "PixModuleGroup::stepFDACTuning: ERROR: FDAC Tuning is supposed to run in scan loop 0!! " << endl;
        return;
      }
      /*else if (nloop == 1) {
        scn->addHisto(*hTot, PixScan::FDAC_TOT, mod, scn->scanIndex(2), -1, -1);
        scn->addHisto(*hFdac, PixScan::FDAC_T, mod, scn->scanIndex(2), -1, -1);
        } else if (nloop == 2) {
        scn->addHisto(*hTot, PixScan::FDAC_TOT, mod, -1, -1, -1);
        scn->addHisto(*hFdac, PixScan::FDAC_T, mod, -1, -1, -1);
        } */
    } else {
      int trg = scn->getTotTargetValue(module->connName());

      for(auto fe: *module){
        auto fei4 = dynamic_cast<PixFeI4*>(fe);
        auto fei3 = dynamic_cast<PixFeI3*>(fe);
        ConfMask<unsigned short int> &fdacs = fei4 ? fei4->readTrim(Fei4::PixReg::FDAC)
                                                   : fei3->readTrim(Fei3::PixReg::FDAC);
        for(int col=0; col<fdacs.ncol(); col++) {
          for(int row=0; row<fdacs.nrow(); row++) {
            int const ife = fe->number();
            PixCoord pc = geo.coord(ife,col,row);
            auto const colmod = pc.x();
            auto const rowmod = pc.y();
            
            if (nloop == -2){
      	      if(abs((*hTrg)(colmod,rowmod)-trg) < abs((*hTotHistArr[mod])(colmod,rowmod)-trg)) {
      	        hTotHistArr[mod]->set(colmod, rowmod, (*hTrg)(colmod,rowmod));
      	        hFdacHistArr[mod]->set(colmod, rowmod, scn->getLoopVarNSteps(0)-1);
      	      }
            } else { 
      	      if(scn->scanIndex(nloop) == 1) { // called from prepareStep!! so loop 0 just finished 
      	        hTotHistArr[mod]->set(colmod, rowmod, (*hTrg)(colmod, rowmod));
      	        hFdacHistArr[mod]->set(colmod, rowmod, scn->scanIndex(0)-1);
      	      } else {
      	        if(abs((*hTrg)(colmod,rowmod)-trg) < abs((*hTotHistArr[mod])(colmod,rowmod)-trg)) {
      	          hTotHistArr[mod]->set(colmod, rowmod, (*hTrg)(colmod,rowmod));
      	          hFdacHistArr[mod]->set(colmod, rowmod, scn->scanIndex(0)-1);
      	        }
      	      }
            }

            // delete histos on ROD
            // if we are in the last step, finish tuning
            if (nloop == -2){
              int ix = (int)(*hFdacHistArr[mod])(colmod,rowmod);
      	      fdacs.set(col, row, (int)(scn->getLoopVarValues(0))[ix]);
      	      hFdacHistArr[mod]->set(colmod, rowmod, (scn->getLoopVarValues(0))[ix]);
            }
          }//row loop
        }//col loop
      }//FE loop
      
      // if we are in the last step, clone modSfg is desired
      if (nloop==-2){
        cout << "finishing FDAC Tuning" << endl;
        writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::TOT_MEAN, scn->scanIndex(2), scn->scanIndex(1), scn->getLoopVarNSteps(0)-1);
        writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::TOT_SIGMA, scn->scanIndex(2), scn->scanIndex(1), scn->getLoopVarNSteps(0)-1);
        //writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::FDAC_T, scn->scanIndex(2), scn->scanIndex(1), -1);
        //writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::FDAC_TOT, scn->scanIndex(2), scn->scanIndex(1), -1);
        if (scn->getCloneModCfgTag()) {
          std::cout << "PixModuleGroup::stepFDACTuning: Writing config to " << pixConnectivity()->getModCfgTag()+scn->getTagSuffix() << std::endl;
          
          module->writeConfig(m_dbserverI, "Configuration-"+pixConnectivity()->getConnTagOI(), pixConnectivity()->getModCfgTag()+scn->getTagSuffix(), "_Tmp");
        } 
      } else {
        writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::TOT_MEAN, scn->scanIndex(2), scn->scanIndex(1), scn->scanIndex(0)-1);
        writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::TOT_SIGMA, scn->scanIndex(2), scn->scanIndex(1), scn->scanIndex(0)-1);
      }
    }
  }//module loop
}

void PixModuleGroup::endFDACTuning(int nloop, PixScan *scn) {
  // Module loop
  std::cout<<__PRETTY_FUNCTION__<<std::endl;
  for (auto module: m_modules) {
    if (module->m_readoutActive) {
      int nCol=module->nCol();
      int nRow=module->nRow();
      PixGeometry geo = module->geometry();
      unsigned int mod = module->m_moduleId;
      Histo *hTot=nullptr, *hFdac= nullptr, *hTrg=nullptr;
      hTot = new Histo("FDAC_TOT", "Tuned Fdacs ToT", nCol, -0.5, (float)nCol-0.5f, nRow, -0.5, (float)nRow-0.5f);
      hFdac = new Histo("FDAC_T", "Tuned Fdacs", nCol, -0.5, (float)nCol-0.5f, nRow, -0.5, (float)nRow-0.5f);
      Histo *hFail = nullptr;
      if (scn->getHistogramFilled(PixScan::TUNING_FAIL)) {
        hFail = new Histo("FAIL", "ToT >10% away from target", nCol, -0.5, (float)nCol-0.5f, nRow, -0.5, (float)nRow-0.5f);
        scn->addHisto(*hFail, PixScan::TUNING_FAIL, mod, scn->scanIndex(2), scn->scanIndex(1), 0);
      }
      // Associate different parameters if the fcad tuning is done in loop 0, 1 or 2
      if (nloop == 0) {
	scn->addHisto(*hTot, PixScan::FDAC_TOT, mod, scn->scanIndex(2), scn->scanIndex(1), -1);
	scn->addHisto(*hFdac, PixScan::FDAC_T, mod, scn->scanIndex(2), scn->scanIndex(1), -1);
      } else if (nloop == 1) {
	scn->addHisto(*hTot, PixScan::FDAC_TOT, mod, scn->scanIndex(2), -1, -1);
	scn->addHisto(*hFdac, PixScan::FDAC_T, mod, scn->scanIndex(2), -1, -1);
      } else if (nloop == 2) {
	scn->addHisto(*hTot, PixScan::FDAC_TOT, mod, -1, -1, -1);
	scn->addHisto(*hFdac, PixScan::FDAC_T, mod, -1, -1, -1);
      }

      float const trg = scn->getTotTargetValue(module->connName());

       for (auto fe : *module){
        auto fei4 = dynamic_cast<PixFeI4*>(fe);
        auto fei3 = dynamic_cast<PixFeI3*>(fe);
        ConfMask<unsigned short int> &fdacs = fei4 ? fei4->readTrim(Fei4::PixReg::FDAC)
                                                   : fei3->readTrim(Fei3::PixReg::FDAC);
	int const ife = fe->number();
	for (int col=0; col<fdacs.ncol(); col++) {
	  for (int row=0; row<fdacs.nrow(); row++) {
	    PixCoord pc = geo.coord(ife,col,row);
	    unsigned int colmod = pc.x();
	    unsigned int rowmod = pc.y();
	    for (int index=0; index<scn->getLoopVarNSteps(nloop); index++) { // Loop on FDACS values
	      int FDAC_S = scn->getLoopVarValues(nloop)[index];
	      if (nloop == 0) {
		hTrg = &scn->getHisto(PixScan::TOT_MEAN, this, mod, scn->scanIndex(2), scn->scanIndex(1), index);
	      } else if (nloop == 1) {
		hTrg = &scn->getHisto(PixScan::TOT_MEAN, this, mod, scn->scanIndex(2), index, 0);
	      } else if (nloop == 2){
		hTrg = &scn->getHisto(PixScan::TOT_MEAN, this, mod, index, 0, 0);
	      }
	      if (index == 0) {
		hTot->set(colmod, rowmod, (*hTrg)(colmod, rowmod));
		hFdac->set(colmod, rowmod, FDAC_S);
		fdacs.set(col,row, FDAC_S);
	      } else {
		if (abs((*hTrg)(colmod,rowmod)-trg) < abs((*hTot)(colmod,rowmod)-trg)) {
		  hTot->set(colmod, rowmod, (*hTrg)(colmod,rowmod));
		  hFdac->set(colmod, rowmod, FDAC_S);
		  fdacs.set(col,row, FDAC_S);
		}
	      }
	    }
	  }
	}
      }

        if (hFail) {
	  for (int col=0; col<nCol; col++){
	    for (int row=0; row<nRow; row++){
	      float const ratio = (*hTot)(col,row)/trg;
	      if( ratio <0.90 || ratio > 1.10 )//Fails is ToT is 10% away of target
	        hFail->set(col, row, 1);
	    }
	  }
	}

      module->changedCfg();
      if (scn->getCloneModCfgTag()) {
	std::cout << "Writing config to " << pixConnectivity()->getModCfgTag()+scn->getTagSuffix() << std::endl;
	module->writeConfig(m_dbserverI, "Configuration-"+pixConnectivity()->getConnTagOI(), pixConnectivity()->getModCfgTag()+scn->getTagSuffix(), "_Tmp");
      }
    }
  }
}


void PixModuleGroup::endIFTuning(int nloop, PixScan *scn) {
  // Module loop
  std::cout<<"INFO::endIFTuning"<<std::endl;

  for (auto module: m_modules) {
    if (!module->m_readoutActive)continue;
      int nCol=module->nCol();
      int nRow=module->nRow();
      unsigned int mod = module->m_moduleId;
      PixGeometry geo = module->geometry();
      bool isFei4 =isFEI4(module->feFlavour());
      int nFePerCol = nCol/module->nChipCol();
      int nFErows = nRow/module->nChipRow();
      //      const int nFe = module->nChip();
      Histo *hFail = nullptr;
      if (scn->getHistogramFilled(PixScan::TUNING_FAIL)) {
        hFail = new Histo("FAIL", "IF Tuning Failed", nFePerCol, -0.5, -0.5+(double)nFePerCol, nFErows, -0.5, -0.5+(double)nFErows);
        scn->addHisto(*hFail, PixScan::TUNING_FAIL, mod, scn->scanIndex(2), scn->scanIndex(1), 0);
      }
      Histo *hTrg=nullptr;
      Histo *hIFdac = new Histo("IF_T", "Tuned IF", nFePerCol, -0.5, -0.5+(double)nFePerCol, nFErows, -0.5, -0.5+(double)nFErows);
      //      hIFdac = new Histo("IF_T", "Tuned IF", nCol, -0.5, (float)nCol-0.5f, nRow, -0.5, (float)nRow-0.5f);
      // Associate different parameters if the fcad tuning is done in loop 0, 1 or 2
      if (nloop == 0) {
	scn->addHisto(*hIFdac, PixScan::IF_T, mod, scn->scanIndex(2), scn->scanIndex(1), -1);
      } else if (nloop == 1) {
	scn->addHisto(*hIFdac, PixScan::IF_T, mod, scn->scanIndex(2), -1, -1);
      } else if (nloop == 2) {
	scn->addHisto(*hIFdac, PixScan::IF_T, mod, -1, -1, -1);
      }
      float const target = scn->getTotTargetValue(module->connName()); 

      for (auto fe : *module){
	int ife = (fe)->number();
	std::vector<float> mean (scn->getLoopVarNSteps(nloop),0);
	for (int index=0; index<scn->getLoopVarNSteps(nloop); index++) { // Loop on IF values
	   if (nloop == 0) {
             hTrg = &scn->getHisto(PixScan::TOT_MEAN, this, mod, scn->scanIndex(2), scn->scanIndex(1), index);
	   } else if (nloop == 1) {
	     hTrg = &scn->getHisto(PixScan::TOT_MEAN, this, mod, scn->scanIndex(2), index, 0);
	   } else if (nloop == 2){
	     hTrg = &scn->getHisto(PixScan::TOT_MEAN, this, mod, index, 0, 0);
	   }
	  //std::cout<<hTrg->name()<<std::endl;
          int count = 0;
	    for (unsigned int col=0; col<(unsigned int)module->nChipCol(); col++){
	      for (unsigned int row=0; row<(unsigned int)module->nChipRow(); row++){
	        unsigned int colmod, rowmod;
	        PixCoord pc = geo.coord(ife,col,row);
	        colmod = pc.x();
	        rowmod = pc.y();
	          if (((*hTrg)(colmod, rowmod)) > 0) {
		    mean[index] += (*hTrg)(colmod, rowmod); count++;
	          }
	      }
	    }
	    if(count)mean[index] /= count;
	    //std::cout<<module->connName()<<" FE "<<ife<<" "<<scn->getLoopVarValues(nloop)[index] <<" "<<mean[index]<<std::endl;
	  }

        //Interpolate best IF between 2 best values
        float result=-1;
          if(!PixFitFitter_lmfit::interpolate(scn->getLoopVarValues(nloop),mean, target, result ) ){
             if (hFail)hFail->set((ife % nFePerCol),(ife < nFePerCol)?0:1, 1);
            continue;
          }

        int bestIF = std::round(result);

	hIFdac->set( (ife % nFePerCol),(ife < nFePerCol)?0:1, bestIF );

	  if(isFei4)
            static_cast<PixFeI4*>(fe)->writeGlobRegister(Fei4::GlobReg::PrmpVbpf, bestIF);
	  else
            static_cast<PixFeI3*>(fe)->writeGlobRegister(Fei3::GlobReg::IF, bestIF);
      }
    module->changedCfg();
     if (scn->getCloneModCfgTag()) {
       std::cout << "Writing config to " << pixConnectivity()->getModCfgTag()+scn->getTagSuffix() << std::endl;
       module->writeConfig(m_dbserverI, "Configuration-"+pixConnectivity()->getConnTagOI(), pixConnectivity()->getModCfgTag()+scn->getTagSuffix(), "_Tmp");
     }
  }
}

void PixModuleGroup::endIFFastTuning(int nloop, PixScan *scn, PixHistoServerInterface *hInt, std::string folder) {
  // Module loops
  for (auto module: m_modules) {
    if (module->m_readoutActive) {
      int nCol=module->nCol();
      int nRow=module->nRow();
      unsigned int mod = module->m_moduleId;
      PixGeometry geo = module->geometry();
      bool isFei4 =isFEI4(module->feFlavour());
      int nFePerCol = nCol/module->nChipCol();
      int nFErows = nRow/module->nChipRow();
      //      const int nFe = module->nChip();

      Histo *hTot, *hIFDac, *hTotTest, *hIFDacTest;
      hTot = new Histo("IF_TOT", "Tuned IF ToT", nCol, -0.5, (float)nCol-0.5, nRow, -0.5, (float)nRow-0.5f);
      hIFDac = new Histo("IF_T", "Tuned IF", nFePerCol, -0.5, -0.5+(double)nFePerCol, nFErows, -0.5, -0.5+(double)nFErows);
			 //nCol, -0.5, (float)nCol-0.5, nRow, -0.5, (float)nRow-0.5f);

      hTotTest = &scn->getHisto(PixScan::TOT_MEAN, this, mod, scn->scanIndex(2), scn->scanIndex(1), scn->getLoopVarNSteps(0) - 2 );
      hIFDacTest = &scn->getHisto(PixScan::IF_T, this, mod, scn->scanIndex(2), scn->scanIndex(1), scn->getLoopVarNSteps(0) - 2 );

      // Associate different parameters if the IF tuning is done in loop 0, 1 or 2
      scn->addHisto(*hTot, PixScan::IF_TOT, mod, 0, 0, 0);
      scn->addHisto(*hIFDac, PixScan::IF_T, mod, 0, 1, 0);

      for (auto fe : *module) {
	int ife = fe->number();
	//hTotTest = &scn->getHisto(PixScan::TOT_MEAN, mod, scn->scanIndex(2), scn->scanIndex(1), step );
	//hIFDacTest = &scn->getHisto(PixScan::IF_T, mod, scn->scanIndex(2), scn->scanIndex(1), step );

	int countTest = 0;
	double TotValue = 0;
	for (unsigned int col=0; col<(unsigned int)module->nChipCol(); col++){
	  for (unsigned int row=0; row<(unsigned int)module->nChipRow(); row++){
	    PixCoord pc = geo.coord(ife,col,row);
	    unsigned int colmod, rowmod;
	    colmod = pc.x();
	    rowmod = pc.y();
	    if (((*hTotTest)(colmod, rowmod)) > 0) {
	      TotValue += (*hTotTest)(colmod, rowmod); countTest++;
	    }
	  }   
	}
	TotValue /= countTest;

	for (unsigned int col=0; col<(unsigned int)module->nChipCol(); col++){
	  for (unsigned int row=0; row<(unsigned int)module->nChipRow(); row++){
	    PixCoord pc = geo.coord(ife,col,row);
	    unsigned int colmod, rowmod;
	    colmod = pc.x();
	    rowmod = pc.y();
	    hTot->set(colmod, rowmod, TotValue);
	  }
	  //	for(int step = scn->getLoopVarNSteps(0) - 1; step >= 0; step--){
	}

	int IFDacValue = (int)(*hIFDacTest)((ife % nFePerCol) ,(ife < nFePerCol)?0:1);
	hIFDac->set((ife % nFePerCol) ,(ife < nFePerCol)?0:1, IFDacValue);
      
	if(isFei4)
      static_cast<PixFeI4*>(fe)->writeGlobRegister(Fei4::GlobReg::PrmpVbpf, IFDacValue);
	else
      static_cast<PixFeI3*>(fe)->writeGlobRegister(Fei3::GlobReg::IF, IFDacValue);
      } //end fe loop
      module->changedCfg();
      //writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::IF_T, 0, 1, 0);
      //writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::IF_TOT, 0, 0, 0);

      writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::TOT_MEAN, scn->scanIndex(2), scn->scanIndex(1), scn->getLoopVarNSteps(0) - 1 );
      writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::TOT_MEAN, scn->scanIndex(2), scn->scanIndex(1), scn->getLoopVarNSteps(0) - 2 );
      writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::TOT_SIGMA, scn->scanIndex(2), scn->scanIndex(1), scn->getLoopVarNSteps(0) - 1 );
      writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::TOT_SIGMA, scn->scanIndex(2), scn->scanIndex(1), scn->getLoopVarNSteps(0) - 2 );
      writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::IF_T, scn->scanIndex(2), scn->scanIndex(1), scn->getLoopVarNSteps(0) - 1 );
      writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::IF_T, scn->scanIndex(2), scn->scanIndex(1), scn->getLoopVarNSteps(0) - 2 );
      writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::IF_TOT, scn->scanIndex(2), scn->scanIndex(1), scn->getLoopVarNSteps(0) - 1 );
      writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::IF_TOT, scn->scanIndex(2), scn->scanIndex(1), scn->getLoopVarNSteps(0) - 2 );
      if (scn->getCloneModCfgTag()) {
	std::cout << "Writing config to " << pixConnectivity()->getModCfgTag()+scn->getTagSuffix() << std::endl;
	module->writeConfig(m_dbserverI, "Configuration-"+pixConnectivity()->getConnTagOI(), pixConnectivity()->getModCfgTag()+scn->getTagSuffix(), "_Tmp");
      }
    }//end active module
  } //end module loop

  /*
  // Module loops
  for (unsigned int pmod=0; pmod<m_modules.size(); pmod++) {
    if (m_modules[pmod]->m_readoutActive) {
      unsigned int mod = m_modules[pmod]->m_moduleId;
      Histo *hTot, *hIFDac, *hTotLast, *hTotTest, *hIFDacLast, *hIFDacTest;
      int IFDacTest, IFDacFinal;
      float TotFinal = 256;
      hTot = new Histo("IF_TOT", "Tuned IF ToT", 144, -0.5, 143.5, 320, -0.5, 319.5);
      hIFDac = new Histo("IF_T", "Tuned IF", 144, -0.5, 143.5, 320, -0.5, 319.5);

      hTotLast = &scn->getHisto(PixScan::TOT_MEAN, this, mod, scn->scanIndex(2), scn->scanIndex(1), scn->getLoopVarNSteps(0) - 1 );
      hTotTest = &scn->getHisto(PixScan::TOT_MEAN, this, mod, scn->scanIndex(2), scn->scanIndex(1), scn->getLoopVarNSteps(0) - 2 );
      hIFDacLast = &scn->getHisto(PixScan::IF_T, this, mod, scn->scanIndex(2), scn->scanIndex(1), scn->getLoopVarNSteps(0) - 1 );
      hIFDacTest = &scn->getHisto(PixScan::IF_T, this, mod, scn->scanIndex(2), scn->scanIndex(1), scn->getLoopVarNSteps(0) - 2 );

      // Associate different parameters if the IF tuning is done in loop 0, 1 or 2
      scn->addHisto(*hTot, PixScan::IF_TOT, mod, 0, 0, 0);
      scn->addHisto(*hIFDac, PixScan::IF_T, mod, 0, 1, 0);
      int trg = scn->getTotTargetValue(); 

      for (std::vector<PixFe*>::iterator fe = m_modules[pmod]->feBegin(); fe != m_modules[pmod]->feEnd(); fe++) {
	TotFinal = 256; //reset tuned TOT to a large value
	int ife = (*fe)->number();
	//hTotTest = &scn->getHisto(PixScan::TOT_MEAN, mod, scn->scanIndex(2), scn->scanIndex(1), step );
	//hIFDacTest = &scn->getHisto(PixScan::IF_T, mod, scn->scanIndex(2), scn->scanIndex(1), step );
	  
	// retrieve the IF values for the last and penultimate steps
	if(ife < 8){
	  IFDacTest = (int)(*hIFDacTest)(18*ife,0);
	}
	else{
	  IFDacTest = (int)(*hIFDacTest)(143 - 18*(ife - 8), 319);
	}

	int countTest = 0;
	double meanTest = 0;
	for (unsigned int col=0; col<18; col++) {
	  for (unsigned int row=0; row<160; row++) {
	    unsigned int colmod, rowmod;
	    if (ife < 8) {
	      rowmod = row;
	      colmod = col + 18*ife;
	    } else {
	      rowmod = 319 - row;
	      colmod = 143 - col - 18*(ife-8);
	    }
	    if (((*hTotTest)(colmod, rowmod)) > 0) {
	      meanTest += (*hTotTest)(colmod, rowmod); countTest++;
	    }
	  }   
	}
	meanTest /= countTest;
	
	if( fabs(TotFinal - trg) > fabs(meanTest - trg)){
	  IFDacFinal = IFDacTest;
	  TotFinal = meanTest;
	}
      

	for (unsigned int col=0; col<18; col++) {
	  for (unsigned int row=0; row<160; row++) {
	    unsigned int colmod, rowmod;
	    if (ife < 8) {
	      rowmod = row;
	      colmod = col + 18*ife;
	    } else {
	      rowmod = 319 - row;
	      colmod = 143 - col - 18*(ife-8);
	    }
	    hIFDac->set(colmod, rowmod, IFDacFinal);
	    hTot->set(colmod, rowmod, TotFinal);
	  }
	  //	for(int step = scn->getLoopVarNSteps(0) - 1; step >= 0; step--){
	}
      
	(*fe)->writeGlobRegister("DAC_IF", IFDacFinal);
      } //end fe loop
      //m_modules[pmod]->changedCfg();
      writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::IF_T, 0, 1, 0);
      writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::IF_TOT, 0, 0, 0);

      writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::TOT_MEAN, scn->scanIndex(2), scn->scanIndex(1), scn->getLoopVarNSteps(0) - 1 );
      writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::TOT_MEAN, scn->scanIndex(2), scn->scanIndex(1), scn->getLoopVarNSteps(0) - 2 );
      writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::TOT_SIGMA, scn->scanIndex(2), scn->scanIndex(1), scn->getLoopVarNSteps(0) - 1 );
      writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::TOT_SIGMA, scn->scanIndex(2), scn->scanIndex(1), scn->getLoopVarNSteps(0) - 2 );
      writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::IF_T, scn->scanIndex(2), scn->scanIndex(1), scn->getLoopVarNSteps(0) - 1 );
      writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::IF_T, scn->scanIndex(2), scn->scanIndex(1), scn->getLoopVarNSteps(0) - 2 );
      if (scn->getCloneModCfgTag()) {
	std::cout << "Writing config to " << pixConnectivity()->getModCfgTag()+scn->getTagSuffix() << std::endl;
	m_modules[pmod]->writeConfig(m_dbserverI, "Configuration-"+pixConnectivity()->getConnTagOI(), pixConnectivity()->getModCfgTag()+scn->getTagSuffix(), "_Tmp");
      }
    }//end active module
  } //end module loop
  */
}


void PixModuleGroup::endDiscBiasTuning(int nloop, PixScan *scn) {
  PMG_DEBUG = true;
	if(PMG_DEBUG) cout << "Starting PixModuleGroup::endDiscBiasTuning" << endl;
	// Check if the loop is executed on the host
	if (!scn->getDspLoopAction(nloop)) {
		Histo *hn=nullptr, *hTime=nullptr, *hDisVbn=nullptr;

		for (unsigned int pmod=0; pmod<m_modules.size(); pmod++) {
			if (m_modules[pmod]->m_readoutActive) {
				PixGeometry geo = m_modules[pmod]->geometry();
				unsigned int mod = m_modules[pmod]->m_moduleId;
				if (nloop == 1) {
					hTime = &scn->getHisto(PixScan::DISCBIAS_TIMEWALK,this, mod, scn->scanIndex(2), 0, 0);
					hDisVbn = &scn->getHisto(PixScan::DISCBIAS_T,this, mod, scn->scanIndex(2), 0, 0);
					hn = &scn->getHisto(PixScan::TIMEWALK,this, mod, scn->scanIndex(2), scn->getLoopVarNSteps(nloop)-1, 0);
				} else if (nloop == 2) {
					hTime = &scn->getHisto(PixScan::DISCBIAS_TIMEWALK,this, mod, 0, 0, 0);
					hDisVbn = &scn->getHisto(PixScan::DISCBIAS_T,this, mod, 0, 0, 0);
					hn = &scn->getHisto(PixScan::TIMEWALK,this, mod, scn->getLoopVarNSteps(nloop)-1, 0, 0);
				}
				// FE loop
				for (std::vector<PixFe*>::iterator fe = m_modules[pmod]->feBegin(); fe != m_modules[pmod]->feEnd(); fe++){
					PixFe* fei4a = nullptr;
					auto fei4b = dynamic_cast<PixFeI4*>(*fe);
					int discbias = 0;
					if(scn->getLoopParam(nloop)==PixScan::DISCBIAS){
						if(fei4a!=0 || fei4b!=0){
							discbias = fei4b->readGlobRegister(Fei4::GlobReg::DisVbn);
						} else {
						  auto fei3 = dynamic_cast<PixFeI3*>(*fe);
                          discbias = fei3->readGlobRegister(Fei3::GlobReg::GlobalTDAC);
                        }
					}/* else if(scn->getLoopParam(nloop)==PixScan::FEI4_GR && scn->getLoopFEI4GR(nloop)=="DisVbn" &&
						(fei4a!=0 || fei4b!=0)){
							discbias = (*fe)->readGlobRegister("DisVbn");
					} */else
						throw PixScanExc(PixControllerExc::ERROR, "Scan variable not compatible with end-of-loop action");

		//			if(PMG_DEBUG) cout << "\t read old discbias value: " << discbias << endl;

					// Calculate mean of Occ
					int hnMean = 0;
					int nrpixels = 0;
					
					int ife = (*fe)->number();
					hDisVbn->set(ife, 0, discbias); // to-do: fix for fei3

					auto fei4 = dynamic_cast<PixFeI4*>(*fe);
                    auto fei3 = dynamic_cast<PixFeI3*>(*fe);
                    ConfMask<unsigned short int> &tdacs = fei4 ? fei4->readTrim(Fei4::PixReg::TDAC)
                                                               : fei3->readTrim(Fei3::PixReg::TDAC);
					for (unsigned int col=0; col<(unsigned int)tdacs.ncol(); col++) {
						for (unsigned int row=0; row<(unsigned int)tdacs.nrow(); row++) {
							unsigned int colmod, rowmod;
							PixCoord cp = geo.coord(ife, col, row);
							colmod = cp.x();
							rowmod = cp.y();
							hnMean += (int)(*hn)(colmod, rowmod);
							nrpixels++;
							hTime->set(colmod, rowmod, (*hn)(colmod,rowmod));
							//		hDisVbn->set(colmod, rowmod, discbias);
						}
					}
					if (nrpixels != 0) {
						hnMean = (int) ((float)hnMean / (float)nrpixels);
			//			if(PMG_DEBUG) cout << " \t\t Mean Timewalk = " << hnMean << endl;
					} else {
			//			if(PMG_DEBUG) cout << "!!! Aborting, nrpixels is 0 !!!" << endl;
						return;
					}

					if(fei4a!=0 || fei4b!=0) {
						if(scn->getLoopParam(nloop)==PixScan::DISCBIAS)
						  static_cast<PixFeI4*>(*fe)->writeGlobRegister(Fei4::GlobReg::DisVbn, discbias&0xff);
					} else
                      static_cast<PixFeI3*>(*fe)->writeGlobRegister(Fei3::GlobReg::GlobalTDAC, discbias);
				}

				m_modules[pmod]->changedCfg();
				if (scn->getCloneModCfgTag()) {
				  std::cout << "Writing config to " << pixConnectivity()->getModCfgTag()+scn->getTagSuffix() << std::endl;
				  m_modules[pmod]->writeConfig(m_dbserverI, "Configuration-"+pixConnectivity()->getConnTagOI(), pixConnectivity()->getModCfgTag()+scn->getTagSuffix(), "_Tmp");
				}

			}
		}
	}
	if(PMG_DEBUG) cout << "End of PixModuleGroup::endDiscBiasTuning" << endl;
}


void PixModuleGroup::endT0Set(int nloop, PixScan *scn, PixHistoServerInterface *hInt, std::string folder) {
  // We assume loop0 is on MCC delay and loop1 on trigger delay

  if (nloop == 1) {
    // JGK: added extra dim. (arb. depth of 4, to be seen what we need) to vectors 
    // for FE-level storage for FE-I4; FE-I3/MCC uses only element 0
    const int nfeel = 4;
    std::vector<float> mean_del[32][nfeel];     //position of the rising clock edge per trigger delay per module
    std::vector<int> npix_ok, n_del[32][nfeel]; //number of pix OK per trigger delay: per ROD and per module
    std::vector<bool> delay_margin;

    int il1;
    for (il1=0; il1<scn->getLoopVarNSteps(nloop); il1++) {
      npix_ok.push_back(0);
      delay_margin.push_back(true);
    }
    Histo *hTime, *hTime1;
    // Loop on modules
    for (unsigned int pmod=0; pmod<m_modules.size(); pmod++) {
      // if the module is active
      if (m_modules[pmod]->m_readoutActive) {
	unsigned int nCol=(unsigned int)m_modules[pmod]->nCol();
	unsigned int nRow=(unsigned int)m_modules[pmod]->nRow();
	unsigned int mod = m_modules[pmod]->m_moduleId;
	bool isFei4 = m_modules[pmod]->feFlavour()==PixModule::PM_FE_I4A || m_modules[pmod]->feFlavour()==PixModule::PM_FE_I4B;
	// Conversion from MCC range to ns
	double conv[nfeel];//, mean[nfeel] = {0,0,0,0};
	int il;//, count[nfeel] = {0,0,0,0};
	for (il=0; il<scn->getLoopVarNSteps(nloop); il++) {
	  for(int ife=0; ife<nfeel; ife++){
	    mean_del[mod][ife].push_back(0);
	    n_del[mod][ife].push_back(0);
	  }
	}
	for (il=1; il<scn->getLoopVarNSteps(nloop); il++) {
	  //std::cout<<"in function: "<<__PRETTY_FUNCTION__<<", at line: "<<__LINE__<<std::endl;
	  hTime  = &scn->getHisto(PixScan::TIMEWALK, this, mod, scn->scanIndex(2), il, 0);
	  hTime1 = &scn->getHisto(PixScan::TIMEWALK, this, mod, scn->scanIndex(2), il-1, 0);
	  for (unsigned int col=0; col<nCol; col++) {
	    for (unsigned int row=0; row<nRow; row++) {
	      PixCoord coord=m_modules[pmod]->geometry().coord(col,row);
	      int ife = isFei4?coord.chip():0;
	      double a = (*hTime)(col, row);
	      double b = (*hTime1)(col, row);
	      if(a > 0){
		mean_del[mod][ife][il] += a;
		(n_del[mod][ife][il])++;
		npix_ok[il]++;
	      }
	      if(b > 0 && il == 1){
		mean_del[mod][ife][il-1] += b;
		(n_del[mod][ife][il-1])++;
		npix_ok[il-1]++;
	      }
	      // if ( a > 0 && b > 0 ) {
	      // 	mean[ife]+= (a-b); count[ife]++; //this way all valid edges are accounted
	      // }
	    }
	  }
	}
	// calculation of clk edge position per trigger per module 
	for (il=0; il<scn->getLoopVarNSteps(nloop); il++) {
	  for(int ife=0; ife<nfeel; ife++){
	    if(n_del[mod][ife][il]>0)
	      mean_del[mod][ife][il]/=(float)n_del[mod][ife][il];
	    else
	      mean_del[mod][ife][il] = 0;
	  }
	}
        // calculation of conversion factor per module and FE - ife=0 for MCC
        for(int ife=0; ife<(isFei4?nfeel:1); ife++){
	  // simple approach: find 1st pair of useful trigger delay values
	  for (il=1; il<scn->getLoopVarNSteps(nloop); il++) {
	    if (mean_del[mod][ife][il-1] > 0 && mean_del[mod][ife][il] > 0 && n_del[mod][ife][il-1]>1000 && n_del[mod][ife][il]>1000 &&
		(mean_del[mod][ife][il]-mean_del[mod][ife][il-1])>0) {
	      conv[ife] = 25.0/(mean_del[mod][ife][il]-mean_del[mod][ife][il-1]);//count[ife]/mean[ife]; //[ns/#]
	      std::cout << "PixModuleGroup::endT0Set: strobe delay conversion factor ("<< ife <<") calculated: " << conv[ife] <<  std::endl; 
	      break;
	    }
	  }
          if (il==scn->getLoopVarNSteps(nloop)) { // no pair of useful trigger delays was found
            conv[ife] = 1.0; // default - this is multiplied, so do not set to 0!!!
            std::cout << "PixModuleGroup::endT0Set: strobe delay conversion factor ("<< ife <<") couldn't be calculated, set to 1." <<  std::endl;
          } 
        }
        // store conversion factor: multiply determined value with stored one since the latter was used in S-curve fit,
        // so must be taken into account
        if(isFei4){
          for (std::vector<PixFe*>::iterator fe = m_modules[pmod]->feBegin(); fe != m_modules[pmod]->feEnd(); fe++){
            int ife = (*fe)->number();
            if(ife<nfeel){
              float conv_comb = ((ConfFloat&)(*fe)->config()["Misc"]["DelayCalib"]).value() * (float) conv[ife];
              std::cout << "PixModuleGroup::endT0Set: full strobe delay conversion factor (FE"<< ife <<") calculated: " << conv_comb <<  std::endl; 
              ((ConfFloat&)(*fe)->config()["Misc"]["DelayCalib"]).m_value = conv_comb;
              // add 5ns to the clk edge position and check delay margin (25ns)
              for (il=0; il<scn->getLoopVarNSteps(nloop); il++) {
		if(mean_del[mod][ife][il]>0 && conv[0]>0){
		  //mean_del[mod][ife][il] *= (float) conv[ife]; // this part of conversion hasn't been applied yet, now we have ns
		  if(mean_del[mod][ife][il]>0)   mean_del[mod][ife][il]+=5./conv[ife];
		  if(mean_del[mod][ife][il]<25./conv[ife]) delay_margin[il] = false;
		  //if(conv_comb>0) mean_del[mod][ife][il]/=conv_comb; // convert back to DAC units
		}
              }
            }
          }
	} else{
	  std::stringstream a;
	  m_modules[pmod]->pixMCC()->writeRegister("CAL_Delay", scn->getStrobeMCCDelayRange());
	  a << scn->getStrobeMCCDelayRange();
	  float conv_comb = ((ConfFloat&)m_modules[pmod]->pixMCC()->config()["Strobe"]["DELAY_"+a.str()]).value() * (float) conv[0];
	  std::cout << "PixModuleGroup::endT0Set: full strobe delay conversion factor calculated: " << conv_comb <<  std::endl; 
	  ((ConfFloat&)m_modules[pmod]->pixMCC()->config()["Strobe"]["DELAY_"+a.str()]).m_value = conv_comb;
	  // add 5ns to the clk edge position and check delay margin (>30ns)
	  for (il=0; il<scn->getLoopVarNSteps(nloop); il++) {
	    if(mean_del[mod][0][il]>0 && conv[0]>0){
	      mean_del[mod][0][il]+=(5/conv[0]+0.5);
	      if(mean_del[mod][0][il]<30./conv[0]) delay_margin[il] = false;
	    } else
	      delay_margin[il] = false;
	  }
	}
	// no longer need SCURVE_MEAN
	for (il=0; il<scn->getLoopVarNSteps(nloop); il++) 
	  writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::TIMEWALK, scn->scanIndex(2), il, 0 );
      }
    }
    
    // Determination of optimal trigger and strobe delays
    // most pixels could be scanned and there's sufficient room
    // to measure low strobe delay for small charges
    int max_ok = 0, il_ok = -1;
    for (il1=0; il1<scn->getLoopVarNSteps(nloop); il1++) {
      if (npix_ok[il1] > max_ok && delay_margin[il1]) {
	max_ok = npix_ok[il1];
	il_ok = il1;
      }
    }

    // JGK: this doesn't make sense: in data taking we have to work with 1 TTC for 2 FE as well, so should not ajust trigger delay in FE strobe
    /*
    for (unsigned int pmod=0; pmod<m_modules.size(); pmod++) {
      if (m_modules[pmod]->m_readoutActive) {
	bool isFei4 = m_modules[pmod]->feFlavour()==PixModule::PM_FE_I4A || m_modules[pmod]->feFlavour()==PixModule::PM_FE_I4B;
	if(isFei4){
	  unsigned int mod = m_modules[pmod]->m_moduleId;
	  int max_ok_fe = 0; int il_ok_fe = -1;
	  for (std::vector<PixFe*>::iterator fe = m_modules[pmod]->feBegin(); fe != m_modules[pmod]->feEnd(); fe++){
	    for (il=0; il<scn->getLoopVarNSteps(nloop); il++) {
	      if (!(*fe)->number()) {
		std::cout<<"PixModuleGroup::endT0Set: for fe: "<<(*fe)->number()<<" in loop: "<<il<<" total number of pixels = "<<n_del[mod][0][il]<<std::endl;
		if (n_del[mod][0][il] > max_ok_fe) {
		  max_ok_fe = n_del[mod][0][il];
		il_ok_fe = il;
		}
	      }
	    }
	    if (il_ok_fe >= 0) {
	      std::cout<<"il ok fe = "<<il_ok_fe<<std::endl;
	      int fe_triggerDelay = (int)(scn->getLoopVarValues(nloop))[il_ok_fe];
	      std::cout << "fe trigger delay is " << fe_triggerDelay << std::endl;
	      int cmdDelay = scn->getLoopVarValues(nloop)[scn->getLoopVarNSteps(nloop)-1] - fe_triggerDelay;
	      std::cout<<"cmd delay = "<<cmdDelay<<std::endl;
	      int cmdDelayReg = ((*fe)->readGlobRegister("CMDcnt")&0xFF)+(cmdDelay<<8);
	      std::cout<<"cmd delay reg = "<<cmdDelayReg<<std::endl;
	      (*fe)->writeGlobRegister("CMDcnt", (int)(cmdDelayReg));
	    }
	  } // fe loop
	  if (il_ok_fe >= 0) {
	    changedCfg();
	    if (scn->getCloneModCfgTag()) {
	      m_modules[pmod]->writeConfig(m_dbserverI, "Configuration-"+pixConnectivity()->getConnTagOI(), pixConnectivity()->getModCfgTag()+scn->getTagSuffix(), "_Tmp");
	    }
	  }
	} // is fei4
      } // active module
    } // mod loop      
    */
    
    if (il_ok >= 0) {
      m_triggerDelay = (int)((scn->getLoopVarValues(nloop))[il_ok]);
      changedCfg();
      std::cout << "PixModuleGroup::endT0Set: trigger delay for group set to " << m_triggerDelay << std::endl;

      for (unsigned int pmod=0; pmod<m_modules.size(); pmod++) {
	if (m_modules[pmod]->m_readoutActive) {
	  unsigned int mod = m_modules[pmod]->m_moduleId;
	  bool isFei4 = m_modules[pmod]->feFlavour()==PixModule::PM_FE_I4A || m_modules[pmod]->feFlavour()==PixModule::PM_FE_I4B;
	  if(isFei4){
	    for (std::vector<PixFe*>::iterator fe = m_modules[pmod]->feBegin(); fe != m_modules[pmod]->feEnd(); fe++){
	      int ife = (*fe)->number();
	      std::cout << "PixModuleGroup::endT0Set: strobe delay for FE " << ife << " set to " << mean_del[mod][ife][il_ok] << std::endl;
	      
	      if(ife<nfeel)
            dynamic_cast<PixFeI4*>(*fe)->writeGlobRegister(Fei4::GlobReg::PlsrDelay, (int)(mean_del[mod][ife][il_ok]));
	    }
	  } else{
	    m_modules[pmod]->pixMCC()->writeRegister("CAL_Range", scn->getStrobeMCCDelayRange());
	    m_modules[pmod]->pixMCC()->writeRegister("CAL_Delay", (int)(mean_del[mod][0][il_ok]));
	  }
	  m_modules[pmod]->changedCfg();
	  if (scn->getCloneModCfgTag()) {
	    m_modules[pmod]->writeConfig(m_dbserverI, "Configuration-"+pixConnectivity()->getConnTagOI(), pixConnectivity()->getModCfgTag()+scn->getTagSuffix(), "_Tmp");
	  }
	}
      }
    }
  }
}

void PixModuleGroup::mccDelFit(int nloop, PixScan *scn,  PixHistoServerInterface *hInt, std::string folder) {
  // check if this action makes sense at all
  if(!scn->getHistogramFilled(PixScan::TIMEWALK) || !scn->getHistogramFilled(PixScan::OCCUPANCY)) return;
  // Loop on modules
  for (unsigned int pmod=0; pmod<m_modules.size(); pmod++) {
    // if the module is active
    if (m_modules[pmod]->m_readoutActive) {
      unsigned int mod = m_modules[pmod]->m_moduleId;
      bool isFei4 = m_modules[pmod]->feFlavour()==PixModule::PM_FE_I4A || m_modules[pmod]->feFlavour()==PixModule::PM_FE_I4B;
      // create the histos
      Histo *hTime, *hSigma, *hChi;
      float conv = 1.f;
      if(!isFei4){
	// Conversion from MCC range to ns
	std::stringstream a;
	a << scn->getStrobeMCCDelayRange();
	conv = ((ConfFloat&)m_modules[pmod]->pixMCC()->config()["Strobe"]["DELAY_"+a.str()]).m_value;
      }
      // fit MCC delay
      unsigned int nRow=(unsigned int)m_modules[pmod]->nRow();
      unsigned int nCol=(unsigned int)m_modules[pmod]->nCol();
      hTime = new Histo("TIMEWALK", "Timewalk", nCol, -0.5, (float)nCol-0.5f, nRow, -0.5, (float)nRow-0.5);
      hSigma = new Histo("SIGMA", "Sigma", nCol, -0.5, (float)nCol-0.5f, nRow, -0.5, (float)nRow-0.5);
      hChi = new Histo("CHI2", "Chi2", nCol, -0.5, (float)nCol-0.5, nRow, -0.5, (float)nRow-0.5);
      if(!scn->getHistogramKept(PixScan::OCCUPANCY)) // temporarily retrieve histos
	scn->downloadHisto(m_pixCtrl, mod, PixScan::OCCUPANCY);
      PixScanHisto &sh = scn->getHisto(PixScan::OCCUPANCY);
      PixScanHisto *sc = 0;
      if (nloop == 0) {
	if(scn->getHistogramKept(PixScan::TIMEWALK))
	  scn->addHisto(*hTime, PixScan::TIMEWALK, mod, scn->scanIndex(2), scn->scanIndex(1), -1);
	sc = &(sh[mod][scn->scanIndex(2)][scn->scanIndex(1)]);
      }else if (nloop == 1) {
	if(scn->getHistogramKept(PixScan::TIMEWALK))
	  scn->addHisto(*hTime, PixScan::TIMEWALK, mod, scn->scanIndex(2), -1, -1);
	sc = &(sh[mod][scn->scanIndex(2)]);
      }else if (nloop == 2){
	if(scn->getHistogramKept(PixScan::TIMEWALK))
	  scn->addHisto(*hTime, PixScan::TIMEWALK, mod, -1, -1, -1);
	sc = &(sh[mod]);
      }
      // S-curve fit
      scn->fitSCurve(*sc, *hTime, *hSigma, *hChi, 0, scn->getRepetitions());
      // clear occupancy histos if requested by user
      if(!scn->getHistogramKept(PixScan::OCCUPANCY)) sc->clear();
      // Convert all histograms (MCC-del. -> ns)
      for (unsigned int col=0; col<nCol; col++) {
	for (unsigned int row=0; row<nRow; row++) {
	  if(isFei4){
	    // get del DAC -> ns conv. from FE cfg. instead of MCC
	    PixCoord coord=m_modules[pmod]->geometry().coord(col,row);
	    int fe=coord.chip();
	    conv = (dynamic_cast<ConfFloat &>(m_modules[pmod]->pixFE(fe)->config()["Misc"]["DelayCalib"])).value();
			//delete sc;
			//delete &sh;
	  }
	  double val = (*hTime)(col, row)*(double)conv;
	  hTime->set(col, row, val);
	}
      }

			std::cout << "scanIndex_2"<< scn->scanIndex(2) << std::endl;
			std::cout << "scanIndex_1"<< scn->scanIndex(1) << std::endl;
	
			for(int i = 0; i<scn->getLoopVarNSteps(0); i++)
     		writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::OCCUPANCY, scn->scanIndex(2), scn->scanIndex(1), i );
      writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::TIMEWALK, scn->scanIndex(2), scn->scanIndex(1)+1, 0 );
      delete hSigma;
      delete hChi;
			//delete hTime;
      //writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::SIGMA, scn->scanIndex(2), scn->scanIndex(1)+1, 0 );
      //writeAndDeleteHisto(scn, hInt, folder, mod, (int)PixScan::CHI2, scn->scanIndex(2), scn->scanIndex(1)+1, 0 );

    }
  }
}

void PixModuleGroup::endIncrTdac(int nloop, PixScan *scn) {
  // Module loop
  for (unsigned int pmod=0; pmod<m_modules.size(); pmod++) {
    if (m_modules[pmod]->m_readoutActive) {
      m_modules[pmod]->deleteConfig("IncrTdacPreviousConfig");
    }
  }
}


void PixModuleGroup::initScan(PixScan *scn){  
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<", at line: "<<__LINE__<<std::endl;
	scn->initScan(this);
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<", at line: "<<__LINE__<<std::endl;
}

void PixModuleGroup::scanLoopStart(int nloop, PixScan *scn) {  
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	scn->scanLoopStart(nloop, this);
}

void PixModuleGroup::prepareStep(int nloop, PixScan *scn, PixHistoServerInterface *hInt, std::string folder) {
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	scn->prepareStep(nloop, this, hInt, folder);
}

void PixModuleGroup::scanExecute(PixScan *scn) {
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  std::cout << m_name <<" PixModuleGroup:scanExecute "<< scn->scanIndex(0)<<"/"<< scn->scanIndex(1)<<"/"<<scn->scanIndex(2) << std::endl; 
  PixController *rod = (m_pixCtrl);

  // Backward comaptibility for apps without scanTerminate
  m_execToTerminate = true;
  // Check if the controller is a ROD 
  if (rod) {
    std::cout<< m_name <<" PixModuleGroup::scanExecute. rod "<< rod <<std::endl;
    if (scn->getRunType() == PixScan::NORMAL_SCAN) {
      std::cout<< m_name <<" PixModuleGroup::scanExecute. normal scan "<<std::endl;
      std::cout<< m_name << " PixModuleGroup::scanExecute. write scan config & setupGroup " <<std::endl; 
      rod->writeScanConfig(*scn);
      std::cout<< m_name << " PixModuleGroup::scanExecute. startScan"<< std::endl; 
      rod->startScan();
    } else if (scn->getRunType() == PixScan::RAW_EVENT) {
      if(!scn->getDspProcessing(0)){
	// Raw pattern or event
	std::vector< Histo* > vh;
	for (int mod=0; mod<32; mod++) {
	  if (vh[mod] != NULL) {
	    scn->addHisto(*vh[mod], PixScan::RAW_DATA_0, mod, scn->scanIndex(2), scn->scanIndex(1),scn->scanIndex(0), 0);
	  }
	  if (vh[mod+32] != NULL) {
	    scn->addHisto(*vh[mod+32], PixScan::RAW_DATA_0, mod, scn->scanIndex(2), scn->scanIndex(1),scn->scanIndex(0), 1);
	  }
	}
      } else {//here it goes... boc dsp scan (almost like threshold scan)
	std::cout<< m_name <<" PixModuleGroup::scanExecute. BOC DSP scan "<<std::endl;
	//downloadConfig();
	//	rod->setConfigurationMode();
	//m_pixCtrl->setConfigurationMode();
	rod->writeScanConfig(*scn);
	rod->prepareBocScanDsp(scn);
	rod->startScan();
      }
    } else if (scn->getRunType() == PixScan::IN_LINK_SCAN) {
      // IN-Link scan using DDC commands
      Histo *vh = NULL;
      inLinkScan(scn, vh);
      if (vh != NULL) {
	scn->addHisto(*vh, PixScan::INLINKMAP, 0, scn->scanIndex(2), scn->scanIndex(1),scn->scanIndex(0));
      }
    } else if (scn->getRunType() == PixScan::MONLEAKRUN) {
      rod->writeScanConfig(*scn);
      m_pixCtrl->setConfigurationMode();
      rod->prepareMonLeakScan(scn);
      rod->startScan();
    }
  }
}

void PixModuleGroup::scanTerminate(PixScan *scn) {   
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	scn->scanTerminate(this);
}

void PixModuleGroup::scanLoopEnd(int nloop, PixScan *scn, PixHistoServerInterface *hInt, std::string folder) {
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	scn->scanLoopEnd(nloop, this, hInt, folder);
}

void PixModuleGroup::terminateScan(PixScan *scn){  
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	scn->terminateScan(this);
}

void PixModuleGroup::setupMasks(int nloop, PixScan *scn){  
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  // This method should setup the appropriate masks on all the FEs at the beginning of every mask step.
}

void PixModuleGroup::setupScanVariable(int nloop, PixScan *scn){  
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  // This method informs the scan about the variable that must be changed
  if (scn->getLoopActive(nloop) && !scn->getDspProcessing(nloop)) {
    PixScan::ScanParam par = scn->getLoopParam(nloop);
    int level = -1; 
    if (par == PixScan::NO_PAR)            level = 0;
    if (par == PixScan::STROBE_DELAY)      level = 0;
    if (par == PixScan::STROBE_DEL_RANGE)  level = 0;
    if (par == PixScan::TRIGGER_DELAY)     level = 0;
    if (par == PixScan::VCAL)              level = 0;
    if (par == PixScan::BOC_BPH)           level = 0; 
    if (par == PixScan::BOC_BPMPH)         level = 0;
    if (par == PixScan::BOC_VFINE)         level = 0;  
    if (par == PixScan::BOC_VPH1)          level = 0; 
    if (par == PixScan::BOC_VPH0)          level = 0; 
    if (par == PixScan::BOC_VPH)           level = 0; 
    if (par == PixScan::BPM_INVERT)        level = 0; 
    if (par == PixScan::OB_VISET)          level = 0; 
    if (par == PixScan::OPTO_VISET)        level = 0; // 0 means one settings for all the modules
    if (par == PixScan::BOC_TX_CURR)       level = 1;
    if (par == PixScan::BOC_TX_MS)         level = 1;
    if (par == PixScan::BOC_TX_BPM)        level = 1;
    if (par == PixScan::BOC_TX_BPMF)       level = 1;
    if (par == PixScan::BOC_RX_THR_DIFF)   level = 1;
    if (par == PixScan::BOC_RX_DELAY_DIFF) level = 1;
    if (par == PixScan::BOC_RX_THR)        level = 1;
    if (par == PixScan::BOC_RX_DELAY)      level = 1;
    if (par == PixScan::BOC_THR_TUNE)      level = 1; // 1 means module specific settings
    if (par == PixScan::CHARGE)            level = 2;
    if (par == PixScan::GDAC)              level = 2;
    if (par == PixScan::GDAC_COARSE)       level = 2;
    if (par == PixScan::GDAC_VARIATION)    level = 2;
    if (par == PixScan::IF)                level = 2; // 2 means FE specific settings
    if (par == PixScan::TRIMF)             level = 2; 
    if (par == PixScan::TRIMT)             level = 2; 
    if (par == PixScan::FDACS)             level = 3;
    if (par == PixScan::TDACS)             level = 3; // 3 means pixel specific settings

    bool reloadCfg = false;
   // bool first = false;
    double val = (scn->getLoopVarValues(nloop))[scn->scanIndex(nloop)];
    int ival = (int)val;
   // if (scn->scanIndex(nloop) == 0 && !scn->getLoopVarValuesFree(nloop)) first = true;
    PixController *rod = (m_pixCtrl);

    if (level == 0) {
      // Global settings
      switch (par) {
      case PixScan::STROBE_DELAY:
	scn->setStrobeMCCDelay(ival);
	scn->setOverrideStrobeMCCDelay(true);
	break;
      case PixScan::STROBE_DEL_RANGE:  
	scn->setStrobeMCCDelayRange(ival);
	scn->setOverrideStrobeMCCDelayRange(true);
	break;
      case PixScan::TRIGGER_DELAY:
	scn->setStrobeLVL1Delay(ival);
	break;
      case PixScan::VCAL:
	scn->setFeVCal(ival);
	break;
      case PixScan::OB_VISET:
	if (m_dcs != NULL) {
	  std::string allPp0Names;
	  for (int i=0; i<4; i++) {
	    if (m_rodConn->obs(i) != NULL) {
	      if (m_rodConn->obs(i)->pp0() != NULL) {
		std::string pname = m_rodConn->obs(i)->pp0()->name();
		for (int j=0; j<=8; j++) {
		  if (m_rodConn->obs(i)->pp0()->modules(j) != NULL) {
		    if (getModuleActive(m_rodConn->obs(i)->pp0()->modules(j)->modId)) {
		      if (allPp0Names.empty()) allPp0Names = pname;
		      else allPp0Names += " "+pname;
		      break;
		    } 
		  } 
		}
	      }
	    }
	  }
	  PixDcsDefs::DDCCommands command = PixDcsDefs::SET_VISET;
	  sendDDCcommand(allPp0Names, val/1000., command);
	}
	break;
      case PixScan::OPTO_VISET:
	if(m_pixBoc==0) break;
	{
	  PixDcsDefs::DDCCommands command = PixDcsDefs::SET_VISET;
	  // check if dcs exists 
	  if (m_dcs != NULL) {
	    // loop over opto boards 
	    for (int ob=0; ob<4; ob++) {
	      // check existance 
	      if (m_rodConn->obs(ob) != NULL) {
		if (m_rodConn->obs(ob)->pp0() != NULL) {
		  int scanned = 0;
		  for (int j=0; j<=8; j++) {
		    if (m_rodConn->obs(ob)->pp0()->modules(j) != NULL) {
		      if (getModuleActive(m_rodConn->obs(ob)->pp0()->modules(j)->modId)) {
			scanned = 1;
		      } 
		    } 
		  }
		  if (scanned) {
		    // get name of the pp0 
		    std::string pname = m_rodConn->obs(ob)->pp0()->name();
		    // KK comment: 
		    // check if at least one module is active not yet
		    // implemented

		    // get old viset 
		    float oldviset = 0.0; 

		    int plugin = m_rodConn->obs(ob)->bocRxSlot(0); 

		    if (plugin >=0 && plugin < 4) {
		      oldviset = m_pixBoc->getRx(plugin)->getViset()*1000.0;
		    }

		    // calculate new viset 
		    float newviset = nextOptoTune(scn, ob, oldviset); 

		    // set viset to new value 
		    if (newviset != oldviset) {
		      sendDDCcommand(pname, newviset/1000.0, command);
		    }
		    m_pixBoc->getRx(plugin)->setViset(newviset/1000.0); 
		    int plugin1 = m_rodConn->obs(ob)->bocRxSlot(1);
		    if (plugin1 >=0 && plugin1 < 4) {
		      m_pixBoc->getRx(plugin1)->setViset(newviset/1000.0); 
		    }
		    // KK DEBUG
		    std::cout << " KK : opto board : " << ob << " oldviset " << oldviset << " newviset " 
			      << newviset << " " << " val " << val << std::endl; 
		  }
		}
	      }
	    }
	  }
	}
	break; 	
      case PixScan::NO_PAR:
	break;
      default:
	break;
      }     
    } else {
      // Module loop 
      for (unsigned int pmod=0; pmod<m_modules.size(); pmod++) {
         reloadCfg = false;


	if (m_modules[pmod]->m_readoutActive) {
	  if (level == 1) {
	    // Module specific settings
	    if (rod) {
	      //int bil_intermediate,
	      int bol[2]; //,  bpl[2],bpc[2];
	      //int bil;
	      int rxc[4] = { 0, 3, 4, 7 }; 
	     // bil_intermediate    = m_modules[pmod]->m_bocInputLink; // This only gives RXPlugin#*10 plus channel number - The physical channel needed can be derived by:
	      //bil = ((bil_intermediate / 10) * 12) + ((bil_intermediate % 10) + 2); // Otherwise all TX related scans give funny results.
	      bol[0] = m_modules[pmod]->m_bocOutputLink1;
	      bol[1] = m_modules[pmod]->m_bocOutputLink2;
	      // Convert to BocCard format
	      if (bol[0] != -1){
		//bpl[0] = bol[0]/10;
		//bpc[0] = bol[0]%10;
		bol[0] = rxc[bol[0]/10]*12 + (bol[0]%10) + 2;
	      }
	      if (bol[1] != -1){
		//bpl[1] = bol[1]/10;
		//bpc[1] = bol[1]%10;
		bol[1] = rxc[bol[1]/10]*12 + (bol[1]%10) + 2;
	      }
	      //int xval;
	    }
	  } else {
	    // FE loop 
	    for (auto fe = m_modules[pmod]->feBegin(); fe != m_modules[pmod]->feEnd(); fe++) {
	      auto fei4 = dynamic_cast<PixFeI4*>(*fe);
              auto fei3 = dynamic_cast<PixFeI3*>(*fe);
	      if (level == 2) {
		// FE specific settings (global reg)
		//reloadCfg = false;
		switch (par) {
		//JUST Vthin_Fine for FE-I4, Vthin_Coarse corresponds to the next case GDAC_COARSE
		case PixScan::GDAC:
		  if ( !( scn->getLoopAction(nloop) == PixScan::GDAC_TUNING || scn->getLoopAction(nloop) == PixScan::GDAC_TUNING_ITERATED ) ) {
		    if(fei4!=0){
                      fei4->writeGlobRegister(Fei4::GlobReg::Vthin_Fine, ival&0xff);
		    } else{
                      fei3->writeGlobRegister(Fei3::GlobReg::GlobalTDAC, ival);
                    }
                    reloadCfg = true;
                  }
                break;
                //Just Vthin_Coarse for FE-I4
                case PixScan::GDAC_COARSE:
		    if(fei4){
		      // compensate factor x2 introduced for Vthin_Coarse
                      fei4->writeGlobRegister(Fei4::GlobReg::Vthin_Coarse, (ival&0x7f)<<1);
                      reloadCfg = true;
                    }
                break;
		case PixScan::GDAC_VARIATION:
		  if ( !( scn->getLoopAction(nloop) == PixScan::GDAC_TUNING || scn->getLoopAction(nloop) == PixScan::GDAC_TUNING_ITERATED ) ) {
		    reloadCfg = true;
		    if(fei4==0){
		      int actualVal = fei3->readGlobRegister(Fei3::GlobReg::GlobalTDAC);
		      if(actualVal + ival < 1)
                fei3->writeGlobRegister(Fei3::GlobReg::GlobalTDAC, 0);
		      else if(actualVal + ival > 31)
                fei3->writeGlobRegister(Fei3::GlobReg::GlobalTDAC, 31);
		      else
                fei3->writeGlobRegister(Fei3::GlobReg::GlobalTDAC, actualVal + ival);
		    }else{
		      int actualVal = fei4->readGlobRegister(Fei4::GlobReg::Vthin_Fine);
			actualVal += (fei4->readGlobRegister(Fei4::GlobReg::Vthin_Coarse)&0xfe)<<7;
		      actualVal+=ival;
		      if(actualVal < 0) actualVal = 0;
		      if(actualVal > 0xffff) actualVal = 0xffff;
              fei4->writeGlobRegister(Fei4::GlobReg::Vthin_Fine, actualVal&0xff);
              fei4->writeGlobRegister(Fei4::GlobReg::Vthin_Coarse, (actualVal&0x7f00)>>7);
		    }
		  }
		  break;
		case PixScan::IF:
		  if (scn->getLoopAction(nloop) != PixScan::IF_FAST_TUNING){
		    std::cout<<"Changing IF to "<<ival<<std::endl;
		    reloadCfg = true;
		    if(fei4==0)
		      fei3->writeGlobRegister(Fei3::GlobReg::IF, ival);
		    else
		      fei4->writeGlobRegister(Fei4::GlobReg::PrmpVbpf, ival);
		  }
		  break;
		case PixScan::TRIMF:
		  reloadCfg = true;
		  if(fei4==0)
            fei3->writeGlobRegister(Fei3::GlobReg::ITrimIf, ival);
		  else
            fei4->writeGlobRegister(Fei4::GlobReg::FDACVbn, ival);
		  break;
		case PixScan::TRIMT:
		  reloadCfg = true;
		  if(fei4==0)
            fei3->writeGlobRegister(Fei3::GlobReg::ITrimTh, ival);
		  else
            fei4->writeGlobRegister(Fei4::GlobReg::TDACVbp, ival);
		  break;
		case PixScan::CHARGE:  
		  reloadCfg = true;
		  if(fei4==0)
            fei3->writeGlobRegister(Fei3::GlobReg::VCal, getVCALfromCharge(ival, *fe, (int)scn->getChargeInjCapHigh()));
		  else {
		    if (scn->getMaskStageMode()==PixScan::FEI4_ENA_SCAP)
              fei4->writeGlobRegister(Fei4::GlobReg::PlsrDAC, getVCALfromCharge(ival, *fe, 0));
		    if (scn->getMaskStageMode()==PixScan::FEI4_ENA_LCAP)
              fei4->writeGlobRegister(Fei4::GlobReg::PlsrDAC, getVCALfromCharge(ival, *fe, 1));
		    if (scn->getMaskStageMode()==PixScan::FEI4_ENA_BCAP)
              fei4->writeGlobRegister(Fei4::GlobReg::PlsrDAC, getVCALfromCharge(ival, *fe, 2));
		  }
		  scn->setFeVCal(0x1fff);   // Rod will not set VCal
		  break;
		default:
		  break;
		}      
	      } else if (level == 3) {
		//reloadCfg = false;
		// Pixel specific settings (masks)
		ConfMask<unsigned short int> *mask = NULL;

		if (par == PixScan::TDACS) {
		  if (!((scn->getLoopAction(nloop) == PixScan::TDAC_TUNING_ITERATED)
			|| (scn->getLoopAction(nloop) == PixScan::TDAC_FAST_TUNING) ))

		    mask = &(fei4 ? fei4->readTrim(Fei4::PixReg::TDAC) : fei3->readTrim(Fei3::PixReg::TDAC));
		}
		if (par == PixScan::TDACS_VARIATION) {
		  if (scn->getLoopAction(nloop) != PixScan::MIN_THRESHOLD) 
		    mask = &(fei4 ? fei4->readTrim(Fei4::PixReg::TDAC) : fei3->readTrim(Fei3::PixReg::TDAC));
		}
		if (par == PixScan::FDACS) {
		  //if (!((scn->getLoopAction(nloop) == PixScan::FDAC_TUNING)
		  //|| (scn->getLoopAction(nloop)==PixScan::FDAC_FAST_TUNING)))
		  //The lines above should be the correct ones, but I keep the following condition only for 
		  //backward compatibility
		  if (!(scn->getLoopAction(nloop)==PixScan::FDAC_FAST_TUNING))
		    mask = &(fei4 ? fei4->readTrim(Fei4::PixReg::FDAC) : fei3->readTrim(Fei3::PixReg::FDAC));
		}
		if (mask != NULL) {
		  reloadCfg = true;
		  for (int col=0; col<(*mask).ncol(); col++) {
		    for (int row=0; row<(*mask).nrow(); row++) {
		      if (par == PixScan::TDACS_VARIATION) {
			if ((*mask).get(col, row) + ival > 128) {
			  (*mask).set(col, row, 128);
			} else if ((*mask).get(col, row) + ival < 0) {
			  (*mask).set(col, row, 0);
			} else {
			  (*mask).set(col, row, (*mask).get(col, row) + ival);
			}
		      } else {
			(*mask).set(col, row, ival);
		      }
		    }
		  }
		}
	      }
	    }
	  }
	  if (reloadCfg){
	    m_pixCtrl->writeModuleConfig(*(m_modules[pmod]), true);
	    //m_modules[pmod]->writeConfig(); // download the config again
	    cout << "PixModuleGroup::setupScanVariable: ";
	  }
	}

         if (reloadCfg) m_configSent = true;
      }
    }
  }
}


//Obsolete for new readout
void PixModuleGroup::inLinkScan(PixScan *scn, Histo* &vh) 
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;

}


unsigned int PixModuleGroup::nextBocThrTune(PixScan* scn, unsigned int modID, int il, unsigned int oldthresh)
{
  // this method does a nested interval search for the best threshold
  // value. A value of 64k/2 * number of intervals is searched for. 

  // the first iteration returns the first threshold, i.e. the center
  // of the threshold interval. 

  if (scn->scanIndex(1) <= 0)
    return ((unsigned int)(scn->getLoopVarValues(1))[scn->scanIndex(1)]);

  // if this is not the first iteration... 

  // define a histogram (delay vs. error)

  Histo& bocscan = scn->getHisto(PixScan::RAW_DATA_1, 
				 this, modID, 
				 scn->scanIndex(2), 
				 (scn->scanIndex(1) - 1), 
				 -1, 
				 il); //JDDBG: This line hardcodes the BOC TUNE Loop into Loop number 1 and the first link of each module

  // sum all errors over the delay interval

  double sum = 0;

  for (int idelay=0; idelay < bocscan.nBin(1); ++idelay) {
    sum += bocscan(0, idelay); 
  }

  // if the sum is below 64/2 * number of bins then decrease the
  // threshold. the step size is defined a priori.

  unsigned int newthreshold = 0; 

  if (sum < (0x8000*scn->getLoopVarNSteps(0))) {
    newthreshold = (oldthresh - (unsigned int)(scn->getLoopVarValues(1))[scn->scanIndex(1)]);
  } 

  // else if the sum is above (or equal) 64/2 * number of bins then
  // increase the threshold. the steps size is defined a priori.

  else {
    newthreshold = (oldthresh + (unsigned int)(scn->getLoopVarValues(1))[scn->scanIndex(1)]);
  }

  return newthreshold; 

}

float PixModuleGroup::nextOptoTune(PixScan* scn, int obID, float oldviset){

  // based on the direction array m_opto_direction and the old viset
  // value the next viset value is calculated and returned

  // different algorithms can be implemented here. the following is
  // implemented: 

  // idea: 
  // 1. get the deviation and the best threshold for tuning at a
  // particular viset for all modules 
  // 2. decide where to go depending on these values for all modules: 
  //    if (avg. deviation > deviation_max) decrease threshold -> write -1 
  //    if (avg. treshold < threshold_min) increase threshold  -> write +1 
  //    if both stay put                                  -> write  0 

  // KK DEBUG
  if (scn->scanIndex(2) <= 0)
    return (scn->getLoopVarValues(2))[scn->scanIndex(2)]; 

  int pluginID = -1;
  //Get the sum of all plugins belonging to that PP0
  float direction = 0.0; 
  for (int i=0;i<2;i++) {
    pluginID = m_rodConn->obs(obID)->bocRxSlot(i);
    if (pluginID >= 0 && pluginID < 4) direction += m_opto_direction[pluginID];
  }
  //  std::cout << " Size of m_opto_direction : " << m_opto_direction.size() << std::endl; 
  // choose direction based on the average direction of all the
  // modules connected to the optoboard 
  if (direction < 0.0)
    direction = -1.0; 
  if (direction > 0.0)
    direction =  1.0; 

  // calculate new viset 
  float newviset = oldviset + direction * (scn->getLoopVarValues(2))[scn->scanIndex(2)]; 

  // return new viset 
  return newviset; 

}

void PixModuleGroup::endBOCTuning(int nloop, PixScan *scn){
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  // calculate the direction (and magnitude) for the viset step 

  PixController *rod = (m_pixCtrl); 
  if(m_pixBoc==0) return;

  // reset direction array 
  m_opto_direction.clear(); 
  for (int i = 0; i < 4; ++i)
    m_opto_direction.push_back(0.0); 

  // loop over all modules 
  for (unsigned int pmod=0; pmod<m_modules.size(); pmod++) {
    if (m_modules[pmod]->m_readoutActive) {
      if (rod) {
	int bol[2],bpl[2],bpc[2]; 
	int rxc[4] = { 0, 3, 4, 7 }; 
	bol[0] = m_modules[pmod]->m_bocOutputLink1;
	bol[1] = m_modules[pmod]->m_bocOutputLink2;
	// Convert to BocCard format
	if (bol[0] != -1){
	  bpl[0] = bol[0]/10;
	  bpc[0] = bol[0]%10;
	  bol[0] = rxc[bol[0]/10]*12 + (bol[0]%10) + 2;
	}
	if (bol[1] != -1){
	  bpl[1] = bol[1]/10;
	  bpc[1] = bol[1]%10;
	  bol[1] = rxc[bol[1]/10]*12 + (bol[1]%10) + 2;
	}

	// loop over all opto links
	for (int il = 0; il < 2; il++) {
	  if (bol[il] >= 0) {

	    // calculate the integral over all delays for the best threshold

	    Histo& bocscan = scn->getHisto(PixScan::RAW_DATA_2, 
					   this, m_modules[pmod]->m_moduleId, 
					   scn->scanIndex(2), 
					   -1, 
					   -1, 
					   il); //JDDBG: This line hardcodes the BOC TUNE Loop into Loop number 1 and the first link of each module

	    // sum all errors over the delay interval
	    double sum = 0;	    
	    for (int idelay=0; idelay < bocscan.nBin(0); ++idelay) {
	      sum += bocscan(idelay, (scn->getLoopVarNSteps(1) - 1)); 
	    }

	    // calculate deviation 
	    double deviation = (sum - 0x8000*scn->getLoopVarNSteps(0)) / (0x8000*scn->getLoopVarNSteps(0)) * 100.0; 

	    // calculate threshold 
	    int threshold = m_pixBoc->getRx(bpl[il])->getRxThreshold(bpc[il]); 

	    // print to screen 
	    std::cout << " KK : threshold for module " << m_modules[m_modIdx[m_modules[pmod]->m_moduleId]] -> connName() << " " 
		      << threshold << " (dev. " << deviation << "%)" 
		      << std::endl; 

	    // KK comment: 
	    // the direction (and magnitude) for the next step in
	    // viset can be calculated here (module based). 

	    // calculate direction 
	    float deviation_max = 10.0; 
	    float deviation_crit = 40.0; 
	    int threshold_min   = 160; 
	    int threshold_crit   = 50; 

	    // Only takes Thresholds into account, which are higher than 10. Otherwise the threshold are just taken out of the equation to not influence viset adjustement
	    if (deviation > deviation_max)
	      m_opto_direction[bpl[0]] -= 1; 
	    if (deviation > deviation_crit)
	      m_opto_direction[bpl[0]] -= 2; 
	    if ((threshold < threshold_min) && !(threshold < 10))
	      m_opto_direction[bpl[0]] += 1; 
	    if ((threshold < threshold_crit) && !(threshold < 10))
	      m_opto_direction[bpl[0]] += 2; 
	  }
	}

	for (int il = 0; il < 2; il++) {
	  if (bol[il] >= 0) {

	    // calculate the integral over all delays for the best threshold

	    Histo& bocscan = scn->getHisto(PixScan::RAW_DATA_2, 
					   this, m_modules[pmod]->m_moduleId, 
					   scn->scanIndex(2), 
					   -1, 
					   -1, 
					   il); //JDDBG: This line hardcodes the BOC TUNE Loop into Loop number 1 and the first link of each module

	    // sum all errors over the delay interval
	    int sum = 0x8000;
	    int rising = -1, falling = -1;
	    for (int idelay=0; idelay < bocscan.nBin(0); ++idelay) {
	      if ((bocscan(idelay, (scn->getLoopVarNSteps(1) - 1)) < sum) && (bocscan(((idelay + 1) % bocscan.nBin(0)), (scn->getLoopVarNSteps(1) - 1)) >= sum)) rising = idelay;
	      if ((bocscan(idelay, (scn->getLoopVarNSteps(1) - 1)) >= sum) && (bocscan(((idelay + 1) % bocscan.nBin(0)), (scn->getLoopVarNSteps(1) - 1)) < sum)) falling = idelay;
	    }
	    int middle = (((rising + 25 + falling) / 2) % 25);
	    if ((middle + 25 - (rising % 25)) % 25 > 12)
	      middle = (falling % 25);
	    else
	      middle = (rising % 25);

	    std::cout << "JDDBG: Rising edge: " << rising << " Falling edge: " << falling << " Middle at: " << middle << std::endl;
	    if ((rising > -1) && (falling > -1)) {
	      // The following line assumes the BocScan is to be tuned to 5ns after V-Clock=0
	      std::cout << "JDDBG: Setting RX delay to: " << ((32 - middle + m_pixBoc->getRx(bpl[il])->getRxDataDelay(bpc[il]))) << " was: " <<
		m_pixBoc->getRx(bpl[il])->getRxDataDelay(bpc[il]) << std::endl;
	      m_pixBoc->getRx(bpl[il])->setRxDataDelay(bpc[il], ((32 - middle + m_pixBoc->getRx(bpl[il])->getRxDataDelay(bpc[il]))));
	    }
	    // To keep low links of of being noisy!
	    int threshold_stored = -1;
	    if ((threshold_stored=m_pixBoc->getRx(bpl[il])->getRxThreshold(bpc[il])) < 10) {
	      std::cout << "JDDBG - Setting RX Threshold for plugin " << bpl[il] << " channel " << bpc[il] << " not to value:" << threshold_stored << std::endl;
	      m_pixBoc->getRx(bpl[il])->setRxThreshold(bpc[il], 255);
	    } else {
	      std::cout << "JDDBG - Setting RX Threshold for plugin " << bpl[il] << " channel " << bpc[il] << " to value:" << threshold_stored << std::endl;
	      m_pixBoc->getRx(bpl[il])->setRxThreshold(bpc[il], threshold_stored);
	    }
	  }
	}	
      }
    }
  }

  // KK DEBUG

  for (int i = 0; i < 4; ++i)
    std::cout << " direction of plugin " << i << " : " << m_opto_direction[i] << std::endl;

  /******* To be checked and added properly: saving to Db at the end of a 2D scan! */

  if (!scn->getLoopActive(nloop+1)) {
    std::cout << "End of scan: cloning tag if requested" << std::endl;
    if (scn->getCloneCfgTag()) {
      std::cout << "Writing config to " << pixConnectivity()->getCfgTag()+scn->getTagSuffix() << std::endl;
      writeConfigAll(m_dbserverI, "Configuration-"+pixConnectivity()->getConnTagOI(), pixConnectivity()->getCfgTag()+scn->getTagSuffix(), "_Tmp");
      readConfig(m_dbserverI, "Configuration-"+pixConnectivity()->getConnTagOI(), pixConnectivity()->getCfgTag());
    }
    initHW();
  }
}

void PixModuleGroup::endOPTOTuning(int nloop, PixScan *scn){

  // print the values found in the scan 

  std::cout << " KK : end of OPTO tuning " << std::endl; 
  if(m_pixBoc==0) return;
  
  // check if dcs exists 
  if (m_dcs != NULL) {
	    
    // loop over opto boards 
    for (int ob=0; ob<4; ob++) {
	      
      // check existance 
      if (m_rodConn->obs(ob) != NULL) {
	if (m_rodConn->obs(ob)->pp0() != NULL) {
	  		  
	  // get present viset 
	  float presentviset = 0.0; 
		  
	  int plugin = m_rodConn->obs(ob)->bocRxSlot(0); 
		  
	  if (plugin >=0 && plugin < 4) {
	    presentviset = m_pixBoc->getRx(plugin)->getViset()*1000.0;
	  }
	  		  	  
	  std::cout << "    : opto board " << ob << " : viset " << presentviset << " mV" << std::endl; 
	}
      }
    }
  }

  for (unsigned int pmod=0; pmod<m_modules.size(); pmod++) {
    if (m_modules[pmod]->m_readoutActive) {
      int bol[2],bpl[2],bpc[2]; 
      int rxc[4] = { 0, 3, 4, 7 }; 
      bol[0] = m_modules[pmod]->m_bocOutputLink1;
      bol[1] = m_modules[pmod]->m_bocOutputLink2;
      // Convert to BocCard format
      if (bol[0] != -1){
	bpl[0] = bol[0]/10;
	bpc[0] = bol[0]%10;
	bol[0] = rxc[bol[0]/10]*12 + (bol[0]%10) + 2;
      }
      if (bol[1] != -1){
	bpl[1] = bol[1]/10;
	bpc[1] = bol[1]%10;
	bol[1] = rxc[bol[1]/10]*12 + (bol[1]%10) + 2;
      }

      // loop over all opto links
      for (int il = 0; il < 2; il++) {
	if (bol[il] >= 0) {

	  // calculate the integral over all delays for the best threshold
	  std::cout << "JDDBG: Dumping tuning data from endOPTOtune for Module " << m_modules[m_modIdx[m_modules[pmod]->m_moduleId]] -> connName() << ":" << std ::endl;

	  Histo& bocscan = scn->getHisto(PixScan::RAW_DATA_2, 
					 this, m_modules[pmod]->m_moduleId, 
					 (scn->getLoopVarNSteps(2) - 1), 
					 -1, 
					 -1, 
					 il); //JDDBG: This line hardcodes the BOC TUNE Loop into Loop number 1 and the first link of each module

	  // sum all errors over the delay interval
          int sum = 0x8000;
	  int rising = -1, falling = -1;
	  for (int idelay=0; idelay < bocscan.nBin(0); ++idelay) {
	    if ((bocscan(idelay, (scn->getLoopVarNSteps(1) - 1)) < sum) && (bocscan(((idelay + 1) % bocscan.nBin(0)), (scn->getLoopVarNSteps(1) - 1)) >= sum)) rising = idelay;
	    if ((bocscan(idelay, (scn->getLoopVarNSteps(1) - 1)) >= sum) && (bocscan(((idelay + 1) % bocscan.nBin(0)), (scn->getLoopVarNSteps(1) - 1)) < sum)) falling = idelay;
	  }
	  int middle = (((rising + 25 + falling) / 2) % 25);
	  
	  int dist = 0;
	  if ((rising % 25) > (falling % 25)) dist = ((rising % 25) - (falling %25));
	  else dist = ((falling %25) - (rising % 25));
	  
	  if (dist - 12 < 0) dist = 12 - dist;
	  else dist = dist - 12;
	  
	  if (dist < 6) std::cout << "JDDBG: THIS signal does not seem sane to me, concerning distance of rising and falling edge:" << std::endl;
	  
	  if ((middle + 25 - (rising % 25)) % 25 > 12)
	    middle = (falling % 25);
	  else
	    middle = (rising % 25);
	  std::cout << "JDDBG: Rising edge: " << rising << " Falling edge: " << falling << " Middle at: " << middle << std::endl;
	  if ((rising > -1) && (falling > -1)) {
	    // The following line assumes the BocScan is to be tuned to 5ns after V-Clock=0
	    std::cout << "JDDBG: Would set RX delay to: " << ((30 - middle + m_pixBoc->getRx(bpl[il])->getRxDataDelay(bpc[il]))) << " was: " <<
	      m_pixBoc->getRx(bpl[il])->getRxDataDelay(bpc[il]) << std::endl;
	    //	    m_pixBoc->getRx(bpl[il])->setRxDataDelay(bpc[il], ((30 - middle + m_pixBoc->getRx(bpl[il])->getRxDataDelay(bpc[il])) % 25));
	  }
	  // To keep low links of of being noisy!
	  int threshold_stored = -1;
	  if ((threshold_stored=m_pixBoc->getRx(bpl[il])->getRxThreshold(bpc[il])) < 10) {
	    std::cout << "JDDBG - Setting RX Threshold for plugin " << bpl[il] << " channel " << bpc[il] << " not to value:" << threshold_stored << std::endl;
	    m_pixBoc->getRx(bpl[il])->setRxThreshold(bpc[il], 255);
	  } else {
	    std::cout << "JDDBG - Setting RX Threshold for plugin " << bpl[il] << " channel " << bpc[il] << " to value:" << threshold_stored << std::endl;
	    m_pixBoc->getRx(bpl[il])->setRxThreshold(bpc[il], threshold_stored);
	  }
	}
      }	
    }
  }

  m_opto_direction.clear(); 
  changedCfg();
  std::cout << "End of scan: cloning tag if requested" << std::endl;
  if (scn->getCloneCfgTag()) {
    std::cout << "JDDBG: Dumping config for " << m_pixBoc->getCtrlName() << std::endl;
    m_pixBoc->getConfig()->dump(std::cout);
    std::cout << "Writing config to " << pixConnectivity()->getCfgTag()+scn->getTagSuffix() << std::endl;
    writeConfigAll(m_dbserverI, "Configuration-"+pixConnectivity()->getConnTagOI(), pixConnectivity()->getCfgTag()+scn->getTagSuffix(), "_Tmp");
    readConfig(m_dbserverI, "Configuration-"+pixConnectivity()->getConnTagOI(), pixConnectivity()->getCfgTag());
  }
  initHW();
  resetViset();
}

std::string PixModuleGroup::getISRoot() {
	//std::cout << __PRETTY_FUNCTION__ << std::endl;
  if (m_is)  {
    //unsigned int scanID = m_is->read<unsigned int>("SCAN_NUMBER");
    std::string isRoot = "";
    
    if (m_rodConn) {  
      if (m_rodConn->crate()) {
	isRoot = m_rodConn->crate()->name();
	isRoot += "/";
      }
      isRoot += m_rodConn->name(); 
      isRoot += "/";
    }    
    return isRoot;
  } else  {
    return "ERROR";
  }
}



//////////////////////////////////////////////////////////////
//STEERSCAN HELPER METHODS
//////////////////////////////////////////////////////////////

bool PixModuleGroup::setModuleMask(PixScan* scn)
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  unsigned int* modMask = createModuleMask();
  bool ret = setPixScanMask(modMask, scn);
  delete[] modMask;
  return ret;
}

unsigned int* PixModuleGroup::createModuleMask()
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  //Set module mask to be passed into PixScan routine:
  unsigned int* modMask = new unsigned int[4];
  for(int i = 0; i<4; i++) modMask[i]=0;
  int grpId;
  for(moduleIterator mi = modBegin(); mi != modEnd(); mi++) {
    grpId = (*mi)->groupId();
    if(grpId<4) {
      if(getModuleActive((*mi)->moduleId())) modMask[grpId] |= 0x1 << (*mi)->moduleId();
      std::cout<<m_name<<std::dec<<" Mask: groupId "<< grpId << " ModuleID " << (*mi)->moduleId() <<"; Mask status : "<<  std::hex<< modMask[grpId] << std::dec<< std::endl;
    }
  }
  return modMask;
}

void PixModuleGroup::prepareLoop0(PixScan* scn, PixHistoServerInterface *hInt, std::string folder)
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  prepareStep(0, scn, hInt, folder);
  //scanExecute(scn); //moved into PixScanTask.cc MK

  std::string idx0 = getISRoot() + "IDX-0";
  if(m_is){
    if (scn->getDspProcessing(0)) {
      //DSP Status Register is initialized after scanExecute 
      getPixController()->nTrigger();
      int loop0=getPixController()->getLoop0Parameter();
      m_is->publish(idx0.c_str(), loop0);
    } else {
      m_is->publish(idx0.c_str(), scn->scanIndex(0));	    
    }
  }
  scn->loop0Start();
}

bool PixModuleGroup::setPixScanMask(unsigned int* modMask, PixScan* scn)
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  unsigned int actualPixScanMask;
  bool ret = true;
  for(int i = 0; i<4; i++) {
    actualPixScanMask = scn->getModuleMask(i);
    scn->setModuleMask(i, (actualPixScanMask & modMask[i]) );
    std::cout<< getName() <<std::dec<<" Final PixScanMask is: "<< std::hex << scn->getModuleMask(i) << std::dec << " in Group Id =  "<< i << std::endl;
    if( (actualPixScanMask & modMask[i]) != 0) ret = false;
  }
  return ret;
}

bool PixModuleGroup::checkRodController()
{
  PixController *rod = ( getPixController() );
  std::cout << __PRETTY_FUNCTION__ <<" "<<rod<<std::endl;
  if(rod) return true;
  else return false;
}

void PixModuleGroup::prepareScan(PixScan* scn)
{
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<", at line: "<<__LINE__<<std::endl;
  std::cout << getName() << " PixModuleGroup::prepareScan " << std::endl;
  // Reset the ROD // Added by Nicoletta for Remote Version
  //      initHW();
  std::string idx2 = getISRoot() + "IDX-2";
  std::string idx1 = getISRoot() + "IDX-1";
  std::string idx0 = getISRoot() + "IDX-0";
  std::string maskStageStr = getISRoot() + "Mask stage";

  if(m_is){
    m_is->publish(idx0.c_str(), 0);	    
    m_is->publish(idx1.c_str(), 0);	    
    m_is->publish(idx2.c_str(), 0);	    
    m_is->publish(maskStageStr.c_str(), 0);	    
  }
  // Reset the scan
  scn->resetScan(); 
  // Scan global init
  initScan(scn);
  // Scan execution
  scanLoopStart(2, scn);  
}

void PixModuleGroup::prepareLoop2(PixScan* scn, PixHistoServerInterface *hInt, std::string folder)
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  std::string idx2 = getISRoot() + "IDX-2";
  //It is called in prepareLoop(0) anyway
  //getPixController()->nTrigger();
  if(m_is){
    if (scn->getDspProcessing(0)) {
      int loop2 = getPixController()->getLoop2Parameter();
      m_is->publish(idx2.c_str(), loop2);
    } else {
      m_is->publish(idx2.c_str(), scn->scanIndex(2));	    
    }
  }
  prepareStep(2, scn, hInt, folder);
  scanLoopStart(1, scn);
}

void PixModuleGroup::prepareLoop1(PixScan* scn, PixHistoServerInterface *hInt, std::string folder)
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  std::string idx1 = getISRoot() + "IDX-1";
  //getPixController()->nTrigger();
  if(m_is){
    if (scn->getDspProcessing(0)) {
      int loop1=getPixController()->getLoop1Parameter();
      m_is->publish(idx1.c_str(), loop1);
    } else {
      m_is->publish(idx1.c_str(), scn->scanIndex(1));	    
    }
  }
  prepareStep(1, scn, hInt, folder);
  scanLoopStart(0, scn);
}

bool PixModuleGroup::normalScan(PixScan* scn)
{
  return (scn->getRunType() == PixScan::NORMAL_SCAN);
}  

bool PixModuleGroup::checkScanningMode()
{
  std::cout << __PRETTY_FUNCTION__ << std::endl;
  // wait till the rod knows it is in scanning mode
  int timeoutCnt=0; int tMax=240; 
  int mask=(0x1 << SCAN_INIT)+(0x1 << SCAN_IN_PROGRESS) + (0x1 << SCAN_COMPLETE) + (0x1 << SCAN_TERMINATED ) ;
  std::cout<<"mask = 0x"<<std::hex<<mask<<std::dec<<std::endl;
  //std::cout<<"runStatus = 0x"<<getPixController()->runStatus()<<", and runStatus() & mask = 0x"<<std::hex<<int(getPixController()->runStatus() & mask)<< std::dec<< std::endl;
  while ( (getPixController()->runStatus() & mask)==0 && timeoutCnt<tMax) {
    sleep(1);
    timeoutCnt++;
  }
  //  if(m_is) {
  //timeoutMsg += getName() + " failed to start scan in time. \n";
  //m_is->publish((std::string)timeoutMsg.c_str(), timeoutInt);//TO BE SET timeoutInt somewhere!!!
  //}
  if( !(timeoutCnt<tMax) ) {
    std::cout<<"for ROD: "<< m_rodConn->name()<<" I timed out in checkScanningMode()"<<std::endl;
    return false;//timeout, skip this ROD  
  }
  else {
    return true;
  }
}

bool PixModuleGroup::doneScanning(bool wait, PixScan* scn, int& mstage, int& loop0, int& loop1, int &loop2, int& in_exeBlock)
{
  std::cout << __PRETTY_FUNCTION__ << std::endl;
  // wait until ROD has finished scanning
  //std::cout << m_name << ": run status = " << getPixController()->runStatus()  << std::endl;
  int blockc = 0, oldms = -1, oldloop0 = -1, progress=1, stat = 1;
  // int ldloop1 = -1, oldloop2 = -1
  if(!wait){
    blockc = in_exeBlock;
    //oldloop2 = loop2;
    //oldloop1 = loop1;
    oldms = mstage;
    oldloop0 = loop0;
  }
  do {
	
    if (getCtrlType() == L12ROD || getCtrlType() == IBLROD) sleep(10);
    else sleep(1);
    /* I think the nTrigger call here is redundant since nTrigger is called by runStatus. Remove it to reduce the requests to PPC server*/
    
    /* For the moment remove this line only if it is not the L12 Controller*/ 
    
    if (getCtrlType() != L12ROD && getCtrlType() != IBLROD) {
      getPixController()->nTrigger();
    
    
      //inkt outer =(nloop>>20)&0x7ff;
      loop2=getPixController()->getLoop2Parameter(); 
      loop1=getPixController()->getLoop1Parameter(); 
      loop0=getPixController()->getLoop0Parameter();
      mstage=getPixController()->getMaskStage();
      progress = getPixController()->runStatus();}
    
    else {
      //inkt outer =(nloop>>20)&0x7ff;
      progress = getPixController()->runStatus();
      loop2=getPixController()->getLoop2Parameter(); 
      loop1=getPixController()->getLoop1Parameter(); 
      loop0=getPixController()->getLoop0Parameter();
      mstage=getPixController()->getMaskStage();
    }
      
      
    
    if( (progress & (0x1 << SCAN_IDLE)) || (progress & (0x1 << SCAN_COMPLETE)) || (progress & (0x1 << SCAN_TERMINATED))) stat=0;
    else stat=1;

    std::cout << m_name << ": " << blockc
              << " mask stage = "<< mstage <<" loop0 = "<< loop0 
              << " progress ";
    for(int i=SCAN_NUM_STATES-1;i>=0;i--) {
      if(progress & (0x1<<i)) std::cout << 1;
      else                    std::cout << 0;
    }
    std::cout << " ISstate "<< stat << std::endl;

 
    if (mstage == oldms && loop0==oldloop0) {
      blockc++;
    } else {
      blockc = 0;
      oldms =mstage;
      oldloop0 = loop0;
      //oldloop1 = loop1;
      //oldloop2 = loop2;
    }
    publishScanEndStep(mstage,loop0,loop1,loop2,stat,scn);
    if(!wait){

      if (getCtrlType() != L12ROD && getCtrlType() != IBLROD) {
      getPixController()->nTrigger();
      loop2=getPixController()->getLoop2Parameter(); 
      loop1=getPixController()->getLoop1Parameter(); 
      loop0=getPixController()->getLoop0Parameter();
      mstage=getPixController()->getMaskStage();
      }

      std::cout << m_name << " - Refreshed to mask stage = " << mstage << " loop0 = " << loop0 << " loop1 = " << loop1 << " loop2 = " << loop2 << std::endl;
      //in_nloop = nloop;
      in_exeBlock = blockc;
      if(stat == 1) return false;
      else return true;
    }
  }  while(stat == 1 && blockc<150);

  // Re-reading of On-Rod Scan Status:
  getPixController()->nTrigger();
  loop2=getPixController()->getLoop2Parameter(); 
  loop1=getPixController()->getLoop1Parameter(); 
  loop0=getPixController()->getLoop0Parameter();
  mstage=getPixController()->getMaskStage();

  std::cout << m_name << " - Calling runOk with mask stage = " << mstage << " loop0 = " << loop0 << " for ROD: "<<m_name<< std::endl;
  if(wait) return runOk(scn, mstage, loop0, loop1, loop2);
  else return true;
}

bool PixModuleGroup::runOk(PixScan* scn, int mstage, int loop0, int loop1, int loop2)
{
  std::cout << __PRETTY_FUNCTION__ << " for ROD: "<<m_rodConn->name()<<std::endl;
  bool runOk = true;
  std::cout << m_name << ": mask stage comp " <<  scn->getMaskStageSteps()-1 << " loop0 comp " << scn->getLoopVarNSteps(0)-1 << " loop1 comp " << scn->getLoopVarNSteps(1)-1 << " loop2 comp " << scn->getLoopVarNSteps(2)-1 << std::endl;

  if (scn->getLoopActive(2) && scn->getDspProcessing(2) && 
      loop2 != scn->getLoopVarNSteps(2)-1 && scn->getLoopVarNSteps(2)>1) runOk = false;
  if (scn->getLoopActive(1) && scn->getDspProcessing(1) && 
      loop1 != scn->getLoopVarNSteps(1)-1 && scn->getLoopVarNSteps(1)>1) runOk = false;
  if (scn->getRunType() == PixScan::NORMAL_SCAN && 
      mstage != scn->getMaskStageSteps()-1 && scn->getMaskStageSteps()>1) runOk = false;
  if (scn->getLoopActive(0) && scn->getDspProcessing(0) && 
      loop0 != scn->getLoopVarNSteps(0)-1 && scn->getLoopVarNSteps(0)>1) runOk = false;
  return runOk;
}

void PixModuleGroup::endLoop0(PixScan* scn, PixHistoServerInterface* , std::string )
{
  std::cout << __PRETTY_FUNCTION__ << std::endl;
  scn->loop0End();
  scanTerminate(scn);
  scn->next(0);
}

void PixModuleGroup::endLoop1(PixScan* scn, PixHistoServerInterface *hInt, std::string folder)
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  scanLoopEnd(0, scn, hInt, folder);	  
  scn->next(1);
}

void PixModuleGroup::endLoop2(PixScan* scn, PixHistoServerInterface *hInt, std::string folder)
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  scanLoopEnd(1, scn, hInt, folder);	
  scn->next(2);
}

void PixModuleGroup::endScan(PixScan* scn, PixHistoServerInterface *hInt, std::string folder)
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  scanLoopEnd(2, scn, hInt, folder);   
  terminateScan(scn); 
}

//////////////////////////////////////////////////////////////
//DONE WITH STEERSCAN HELPER METHODS
//////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////
//SCAN IS PUBLISHING METHODS
//////////////////////////////////////////////////////////////

void PixModuleGroup::publishScanInit(std::string scanName, int id)
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  if(m_is != 0){
    m_is->publish(getActionString().c_str(), scanName.c_str());
    if(id != -1) m_is->publish(getStatusString(id).c_str(), "INITIALIZING");
  }
}

/*void PixModuleGroup::publishMainFolder(std::string mainFolder)
  {
  if(m_is != 0){
  m_is->publish("MAIN_FOLDER", mainFolder.c_str());
  }

  }*/

void PixModuleGroup::publishScanDone(int id)
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  if(m_is != 0){
    m_is->publish(getActionString().c_str(), "NONE");
    if(id != -1) m_is->publish(getStatusString(id).c_str(), "DONE");
  }
}

void PixModuleGroup::publishScanFailed(int id)
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  if(m_is != 0 && id != -1) m_is->publish(getStatusString(id).c_str(), "FAILED");

  PixController *rod = (m_pixCtrl);
  unsigned int i;
  for (i=0; i<4; i++) {
    // Disable histogramming
    rod->stopHistogramming(i);
  }
}

void PixModuleGroup::publishScanAbort(int id)
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  if(m_is != 0 && id != -1) m_is->publish(getStatusString(id).c_str(), "ABORTED");
}

void PixModuleGroup::publishScanRunning(int id)
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  if(m_is != 0 && id != -1) m_is->publish(getStatusString(id).c_str(), "RUNNING");
}

void PixModuleGroup::publishScanEndStep(int maskStage, int bin0, int bin1, int bin2, int runStatus, PixScan* scn)
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  std::string isRoot = getISRoot();
  std::string idx0 = isRoot + "IDX-0";
  std::string idx1 = isRoot + "IDX-1";
  std::string idx2 = isRoot + "IDX-2";
  std::string maskStageStr = isRoot + "Mask stage";
  std::string binStr = isRoot + "Bin";
  std::string runStatusStr = isRoot + "RunStatus(RodPixController)";

  if(m_is){
    m_is->publish(maskStageStr.c_str(), maskStage);
    m_is->publish(binStr.c_str(), bin2);
    m_is->publish(runStatusStr.c_str(), runStatus);

    if (scn->getDspProcessing(0)) {
      m_is->publish(idx0.c_str(),bin0);
      if (scn->getDspProcessing(1)) {
        m_is->publish(idx1.c_str(),bin1);
        if (scn->getDspProcessing(2)) {
          m_is->publish(idx1.c_str(),bin2);
        } else {
          m_is->publish(idx2.c_str(), scn->scanIndex(2));
        }
      } else {
        m_is->publish(idx2.c_str(), scn->scanIndex(2));
        m_is->publish(idx1.c_str(), scn->scanIndex(1));
      }
    } else {
      m_is->publish(idx2.c_str(), scn->scanIndex(2));
      m_is->publish(idx2.c_str(), scn->scanIndex(1));
      m_is->publish(idx0.c_str(), scn->scanIndex(0));
    } 
  }
}

std::string PixModuleGroup::getStatusString(int id)
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  std::string isRoot = getISRoot();
  if(id != -1){
    std::ostringstream oss;
    oss << id;
    std::string number = oss.str();
    int leading0s = 9-number.size();
    if(leading0s > 0) number = std::string(leading0s,'0')+number;
    number = "S" + number;
    number += "/";
    isRoot = number+isRoot;
  }
  std::string status = isRoot + "STATUS";
  return status;
}

std::string PixModuleGroup::getActionString()
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  std::string isRoot = getISRoot();      
  std::string act = isRoot + "CURRENT-ACTION";
  return act;
}

//////////////////////////////////////////////////////////////
//DONE WITH SCAN IS PUBLISHING METHODS
//////////////////////////////////////////////////////////////



void PixModuleGroup::selectFEEnable() {
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  for(moduleIterator mi = modBegin(); mi != modEnd(); mi++) (*mi)->selectFEEnable(true); 
}


void PixModuleGroup::selectFE(int iFE) {
  std::cout << "************PixModuleGroup::selectFE. I've selected FE: " << iFE << std::endl;

  for(moduleIterator mi = modBegin(); mi != modEnd(); mi++){
    (*mi)->pixMCC()->globalResetFE(31);
    (*mi)->pixMCC()->globalResetMCC();
  }
  for(moduleIterator mi = modBegin(); mi != modEnd(); mi++) (*mi)->selectFE(iFE, true);
  downloadConfig();
  try {
    PixController *rod = ( getPixController() );
    rod->setConfigurationMode();
    rod->sendModuleConfig(0,0x3fff);
    m_preAmpsOn = true;
  } catch (int errn) {
    std::cout << "PixModuleGroup::selectFE(int iFE). FE by FE case. Configuration not sent to the HW !!!" << std::endl;
  }
}
void PixModuleGroup::disableFESelected() {
  
  for( moduleIterator module = modBegin(); module != modEnd(); module++) {
    // reset FE's and then the MCC
    (*module)->pixMCC()->globalResetFE(31);
    (*module)->pixMCC()->globalResetMCC();
    (*module)->disableFESelected();
  }
  
}
  
void PixModuleGroup::updateFromConn() {
  // Not useful when connectivity is not really loaded from DB
  if (m_localConnectivity) return; 
  // Save the old module arrays
  std::vector<PixModule*>oldMod = m_modules;
  std::vector<ModuleConnectivity*>oldModConn = m_modConn;
  m_modules.clear();
  m_modConn.clear();
  for (int ix=0; ix<32; ix++) m_modIdx[ix] = -1;

  // Load PixModules from OB/PP0 info
  for (int i=0; i<4; i++) {
    if (m_rodConn->obs(i) != NULL) {
      if (m_rodConn->obs(i)->pp0() != NULL) {
	for (int j=0; j<=8; j++) {
	  if (m_rodConn->obs(i)->pp0()->modules(j) != NULL) {
	    if (m_rodConn->obs(i)->pp0()->modules(j)->enableReadout)
	      {
		// Check if the module has been already created
		bool exists=false;
		for (unsigned int k=0; k<oldModConn.size(); k++) {
		  if (oldModConn[k]->name() == m_rodConn->obs(i)->pp0()->modules(j)->name()) {
		    if (oldModConn[k]->modId>=0 && oldModConn[k]->modId<32) m_modIdx[oldModConn[k]->modId] = m_modules.size();
		    m_modules.push_back(oldMod[k]);
		    m_modConn.push_back(oldModConn[k]);
		    exists = true;
		    break;
		  }
		}
		if (!exists) {
		  std::string modName = m_rodConn->obs(i)->pp0()->modules(j)->name();
		  DbRecord *mod_rec= nullptr;
		  try {
		    mod_rec = m_conn->getCfg(modName);
		  } 
		  catch(...) {
		    mod_rec =nullptr;
		  }
		  if (mod_rec) {
		    PixModule *tempModule;
		    if (m_cachedCopy) {
		      tempModule = m_conn->getMod(modName);
		    } else {
		      tempModule = new PixModule(mod_rec, this, m_rodConn->obs(i)->pp0()->modules(j)->prodId());
		    }
		    int id = m_rodConn->obs(i)->pp0()->modules(j)->modId;
		    if (id>=0 && id<32) m_modIdx[id] = m_modules.size();
		    m_modules.push_back(tempModule);      
		    m_modConn.push_back(m_rodConn->obs(i)->pp0()->modules(j));
		    //tempModule->loadConfig(mod_rec);
		    tempModule->m_moduleId = m_rodConn->obs(i)->pp0()->modules(j)->modId;
		    tempModule->m_groupId  = m_rodConn->obs(i)->pp0()->modules(j)->groupId;
		  }
		}
	      }
	  }
	}
      }
    }
  }
  // Now delete modules which are no longer used
  for (unsigned int i=0; i<oldModConn.size(); i++) {
    bool exists = false;
    for (unsigned int j=0; j<m_modConn.size(); j++) {
      if (m_modConn[j]->name() == oldModConn[i]->name()) exists = true;
    }
    if (!exists) {
      delete oldMod[i];
    }
  }
  // Must re-initialize the ROD here
  if (m_hwInit) {
    // Initialize the PixController
    m_pixCtrl->initHW();
    // Initialize the Boc
    //if (m_pixBoc != NULL) m_pixBoc->BocConfigure(); 
  }  
  // Set the default speed
  setReadoutSpeed(m_readoutSpeed);
  // Update PixModule parameters from conn DB
  updateModParFromConn();
}

void PixModuleGroup::updateModuleLinks() {

  // std::cout << __PRETTY_FUNCTION__ << std::endl;
  int swf[8] = { 7, 6, 5, 4, 3, 2, 1, 0 };
  RodBocLinkMap *bocl = m_rodConn->linkMap();
  PixController *rod = (m_pixCtrl);
  if (!rod) return;
  // Update module local variables fron conn DB
  // std::cout << "=== UpdateModuleLink" << std::endl;
  for (int i=0; i<8; i++) {
    rod->fmtLinkMap(i) = m_rodConn->fmtLinkMap(i);
  }

  {
    // InLink

    int npos[4] = { 1, 2, 3, 4 };

    // std::cout << " m_modConn has " << m_modConn.size() << " elements " <<  std::endl;
    for (unsigned int i=0; i<m_modConn.size(); i++) {
      // Set inLink
      int link = m_modConn[i]->bocLinkTx();
      int slot = link/10;
      int ch = link%10;
      if (npos[slot] > 0) {
	slot = npos[slot]-1;
      } else {
	slot = -npos[slot]-1;
	ch = swf[ch];
      }
      m_modules[i]->m_bocInputLink = slot*10+ch; 
      if (bocl != NULL) {
	m_modules[i]->m_inputLink = bocl->rodTx[slot][ch];
      } else {
	m_modules[i]->m_inputLink = -1; 
      }
    }
  }

    // OutLink
  {
    int npos[4] = { 1, 2, 3, 4 };
    
     for (unsigned int i=0; i<m_modConn.size(); i++) {

     int link = m_modConn[i]->bocLinkRx(1);
      if (link >= 0) {
	int slot = link/10;
	int ch = link%10;
	if (npos[slot] > 0) {
	  slot = npos[slot]-1;
	} else {
	  slot = -npos[slot]-1;
	  ch = swf[ch];
	}
	m_modules[i]->m_bocOutputLink1 = slot*10+ch; 
	  if (bocl != NULL) {
	   m_modules[i]->m_outputLink1 = bocl->rodRx40[slot][ch];
	   std::cout<<" BocOutputLink "<<(int)m_modules[i]->m_bocOutputLink1<<" slot "<<(int)slot<<" channel "<<(int)ch<<" outputlink "<<(int)m_modules[i]->m_outputLink1<<std::endl;
	  } else {
	   m_modules[i]->m_outputLink1 = -1; 
	  } 
	} else {
	m_modules[i]->m_bocOutputLink1 = -1; 
	m_modules[i]->m_outputLink1 = -1; 
      }
     
     // Set outLink2
      link = m_modConn[i]->bocLinkRx(2);
      if (link >= 0) {
	int slot = link/10;
	int ch = link%10;
	if (npos[slot] > 0) {
	  slot = npos[slot]-1;
	} else {
	  slot = -npos[slot]-1;
	  ch = swf[ch];
	}
	m_modules[i]->m_bocOutputLink2 = slot*10+ch; 
	if (bocl != NULL) {
	    m_modules[i]->m_outputLink2= bocl->rodRx40[slot][ch];
	} else {
	  m_modules[i]->m_outputLink2 = -1; 
	}
      } else {
	m_modules[i]->m_bocOutputLink2 = -1; 
	m_modules[i]->m_outputLink2 = -1; 
      }


      	// Implement alternate link, keep same outputlink1 since whe don't want to screw up the cabling map!!!
		bool altLink = false;
	std::string modName = m_modConn[i]->name();
	/*if(m_readoutSpeed != DOUBLE_80)//Test for modules with problematic DTO1 fibers
		if(   modName == "L0_B01_S2_C6_M1C"
                   || modName == "L0_B02_S1_C7_M0"
                   || modName == "L0_B02_S2_C6_M2C"
                   || modName == "L0_B04_S2_C6_M6C"
                   || modName == "L0_B06_S2_A7_M5A"
                   || modName == "L0_B09_S2_A7_M1A"
                   || modName == "L0_B10_S1_C7_M4C"
		   || modName == "L0_B11_S1_C7_M0"
		   || modName == "L0_B10_S2_A7_M0" 
                   || modName == "L1_B03_S2_C6_M1C"
                   || modName == "L1_B03_S2_C6_M2C"
		   || modName == "L1_B03_S1_C7_M3C"
                   || modName == "L1_B10_S1_A6_M5A"
                   || modName == "L1_B10_S1_A6_M3A"
                   || modName == "L1_B11_S1_A6_M6A"
                   || modName == "L1_B11_S1_C7_M4C"
                   || modName == "L1_B11_S2_A7_M3A"
                   || modName == "L1_B11_S2_A7_M4A"
                   || modName == "L1_B13_S2_C6_M3C"
                   || modName == "L1_B14_S2_A7_M1A"
                   || modName == "L1_B16_S2_C6_M6C"
                   || modName == "L1_B19_S2_C6_M2C"
		   || modName == "L0_B07_S2_C6_M1C")
                    altLink =true;*/

        //if (m_modules[i]->useAltLink() ) altLink = true; //Warning this only listen to the connectivity not current DB, problematic so far!!!
	//Potential FIX if (m_modules[i]->useAltLink() ) altLink = true; To bring in when DB is updated (TONS of module Configs...)
	if (altLink && m_modules[i]->m_bocOutputLink2 !=255 && m_modules[i]->m_outputLink2 !=255) {
          std::cout<<"Using Alt link for module "<<m_modConn[i]->name()<<std::endl;
	  int b1 = m_modules[i]->m_bocOutputLink1;

	  m_modules[i]->m_bocOutputLink1 = m_modules[i]->m_bocOutputLink2;
	  m_modules[i]->m_bocOutputLink2 = b1;
	}

      std::cout << "=== Mod: " << std::dec << m_modConn[i]->name() << std::endl;
      std::cout << "=== inLink: " << m_modules[i]->m_inputLink << std::endl;
      std::cout << "=== outLink1: " << m_modules[i]->m_outputLink1 << std::endl;
      std::cout << "=== outLink2: " << m_modules[i]->m_outputLink2 << std::endl;
      std::cout << "=== bocInLink: " << m_modules[i]->m_bocInputLink << std::endl;
      std::cout << "=== bocOutLink1: " << m_modules[i]->m_bocOutputLink1 << std::endl;
      std::cout << "=== bocOutLink2: " << m_modules[i]->m_bocOutputLink2 << std::endl;

     }

  }
  // std::cout<<"Leaving function: "<<__PRETTY_FUNCTION__<<", at line: "<<__LINE__<<std::endl;
}

bool PixModuleGroup::isSecondLinkAvailable ( ){

for (size_t i=0; i<m_modules.size(); i++) {
  int mID = m_modules[i]->m_moduleId;
  if(outLink1B(mID)!=-1)return true;
 }
return false;

}

int PixModuleGroup::getAltRxChannel (int iMod){

return outLink1B(iMod);

}

void PixModuleGroup::updateModParFromConn() {
  updateModuleLinks();
  setActive(m_rodConn->enableReadout & m_rodConn->active());
  for (unsigned int i=0; i<m_modConn.size(); i++) {
    m_modules[i]->m_moduleId = m_modConn[i]->modId;
    m_modules[i]->m_groupId  = m_modConn[i]->groupId;
    setModuleActive(m_modConn[i]->modId, m_modConn[i]->enableReadout & m_modConn[i]->active());
    m_modules[i]->m_geomConnName = m_modConn[i]->name();
    m_modules[i]->m_geomType = (m_modules[i]->m_geomConnName.substr(0,1)=="D")?PixModule::SECTOR:PixModule::STAVE;
    m_modules[i]->m_geomAssemblyId = (m_modules[i]->m_geomType==PixModule::SECTOR)?9000:4000;
    m_modules[i]->m_geomPosition = m_modConn[i]->pp0Slot();
  }
}

void PixModuleGroup::setReadoutSpeed(MccBandwidth speed, bool forceSetViset) {

  if( getCtrlType() == CPPROD)return;

  if(speed == DOUBLE_80 && !isSecondLinkAvailable()){
  ers::warning(PixLib::pix::daq (ERS_HERE, m_rodConn->name(),"No second link available: This ROD cannot run at 160 Mbps, falling back to 80 or you will go BUSY, please check your RUN CONFIG!!!"));
  speed = SINGLE_80;
  }

  if(m_curReadoutSpeed == speed)return;

  m_curReadoutSpeed = speed;
  int bocMode;

  if (speed == SINGLE_40) {bocMode = 0;}
  if (speed == DOUBLE_40) {bocMode = 0;}
  if (speed == SINGLE_80) {bocMode = 2;}
  if (speed == DOUBLE_80) {bocMode = 2;}

  std::cout << getName() << "PixModuleGroup::setReadoutSpeed: "<< speed <<" bocMode "<< bocMode <<" m_hwInit="<< m_hwInit << std::endl;

  // set the BOC to the mode selected
  if(m_pixBoc){
    Config* cfg = m_pixBoc->getConfig();
    *((int *) (((ConfInt&)(*cfg)["General"]["Mode"]).m_value)) = bocMode;
    m_pixBoc->setBocMode();
      if (m_hwInit)m_pixBoc->BocConfigure();
  }

}

std::pair<int,int> PixModuleGroup::getOBModConnId(int) {
  throw(PixModuleGroupExc(PixModuleGroupExc::FATAL, "DEPRECATED", "Deprecated method; use modConn() instead"));
  return std::make_pair(-1, -1);
}

void PixModuleGroup::setActive(bool active) {
  // m_active = active;
  m_rodConn->active() = active;
  std::string mAct = m_rodConn->name()+"/Active";
  if (m_is) {  
    if (getActive()) {
      m_is->publish(mAct.c_str(), 1);
    }  
    else {
      m_is->publish(mAct.c_str(), 0);
    }  
  }
}

bool PixModuleGroup::getActive() {
  bool active;
  active = m_rodConn->active() & m_rodConn->enableReadout;
  std::string mAct = m_rodConn->name()+"/Active";
  if (active) {
    if(m_is) m_is->publish(mAct.c_str(), 1);  
  } else {
    if(m_is) m_is->publish(mAct.c_str(), 0);  
  }
  return active;
}

bool PixModuleGroup::getModuleActive(int modId) {
  string mod;
  bool active;
  if (modId<0 || modId>31) return false;
  int midx = m_modIdx[modId];
  if (midx >= 0) {
    std::string mAct = m_rodConn->name()+"/"+m_modConn[midx]->name()+"/Active";
    //Check for active module, enabled in DAQ and enabled in DCS
    active = m_modConn[m_modIdx[modId]]->active() & m_modConn[m_modIdx[modId]]->enableReadout;
    if (m_is) {
      if (active) {
	m_is->publish(mAct.c_str(), 1);  
      } else {
	m_is->publish(mAct.c_str(), 0);  
      }
    }
    return active;
  }
  return false;
}

void PixModuleGroup::setModuleActive(int modId, bool active) {
  if (modId<0 || modId>31) return;
  int midx = m_modIdx[modId];
  if (midx >= 0) {
    //m_modConn[midx]->active() = active;
    m_modConn[midx]->active() = active & !disabledFromDCS(m_modConn[midx]->name());//This line would make DCS disable static, do we want this?
    std::string mAct = m_rodConn->name()+"/"+m_modConn[midx]->name()+"/Active";
    if (getModuleActive(modId)) {
      if (m_is) m_is->publish(mAct.c_str(), 1);  
    } else {
      if (m_is) m_is->publish(mAct.c_str(), 0);  
    }
  }
}

unsigned int PixModuleGroup::getActiveModuleMask() {
  unsigned int moduleMask = 0; 
  for (int i = 0; i < 32; i++){
    if (getModuleActive(i)){
      moduleMask |= 0x1<<i;
    }
  }
  return moduleMask;
}
   
std::string PixModuleGroup::getModIdName(int modId) {
  if (modId>=0 && modId<=31) {
    int midx = m_modIdx[modId];
    if (midx >= 0) {
      return m_modConn[midx]->name();
    }
  }
  return "";	
}
 
unsigned int PixModuleGroup::getModMaskPosition(std::string modName) {
  unsigned int moduleMask = 0; 
  for (int i = 0; i < 32; i++){
    int midx = m_modIdx[i];
    if (midx >= 0) {
      if (m_modConn[midx]->name() == modName) {
	moduleMask = 0x1<<i;
	return moduleMask;
      }
    }
  }
  return moduleMask;
}

void PixModuleGroup::setMultiName(std::string mn) {
  m_multiName = mn;    
  std::string mName = m_rodConn->name()+"/MultiName";
  if (m_is) m_is->publish(mName.c_str(), mn);  
}

 
std::string PixModuleGroup::getMultiname() {
  return m_multiName;
}

void PixModuleGroup::publishROD() {

  //SET THE RIGHT COMMAND TO GET THE PRESNCE OF THE PARAM!!!

  if ((&m_pixCtrl)==NULL) return;
  int i=0;
  uint32_t rodserial;
  PixController *rod = (m_pixCtrl);
  rodserial=rod->getRodSerialNumber();
  
  if (m_is) {
    std::string listVarAddr;
    std::string listTest = "listRODs0";
    std::string listVar;
    bool alreadyPublished = m_is->exists(listTest);
    if (!alreadyPublished) {
      listVarAddr = "listRODsAddr0";
      listVar = "listRODsNum0";
      m_is->publish(listVar.c_str(), ( (rodserial>>16)&0xFF));
      m_is->publish(listVarAddr.c_str(), i);
    }
    else {
      int idx = 0;
      ostringstream value;
      while(alreadyPublished) {
	value << idx;
	listTest = "listRods" + value.str(); 
	alreadyPublished = m_is->exists(listTest);
	if (!alreadyPublished) break;
	idx ++;
      }
      listVarAddr = "listRODsAddr" + value.str();
      listVar = "listRODsNum" + value.str();
      m_is->publish(listVar.c_str(), ( (rodserial>>16)&0xFF));
      m_is->publish(listVarAddr.c_str(), i);
    }
  }
  return; 
}

void PixModuleGroup::writeAndDeleteHisto(PixScan* scn, PixHistoServerInterface* hInt, std::string folder, 
					 unsigned int mod, int type, int idx2, int idx1, int idx0)
{
  scn->writeAndDeleteHisto(this, hInt, folder, mod, type,idx2, idx1,idx0);
}

void PixModuleGroup::updateBOCLink(){

if(getCtrlType() != L12ROD)return;

int nRx=0, rxCh[32], dto[32], dto2[32];

 for (size_t i=0; i<m_modules.size(); i++) {
  int mID = m_modules[i]->m_moduleId;
  if(outLink1A(mID)==-1)continue;
  rxCh[nRx] = outLink1A(mID);
  dto[nRx]= bocRxLink1(mID);
  dto2[nRx]= bocRxLink2(mID);
  std::cout<<"Rx "<<rxCh[nRx]<<" DTO "<< dto[nRx]<<" DTO2 "<<dto2[nRx]<<std::endl;
  nRx++;

  if(outLink1B(mID)==-1)continue;
  rxCh[nRx] = outLink1B(mID);
  dto[nRx]= bocRxLink2(mID);
  dto2[nRx]= -1;
  std::cout<<"Rx "<<rxCh[nRx]<<" DTO "<< dto[nRx]<<" DTO2 "<<dto2[nRx]<<std::endl;
  nRx++;
  }

if(m_pixBoc!=0)m_pixBoc->setRxFibreMapping(nRx, rxCh, dto, dto2);

}

int PixModuleGroup::inLink(int modId) {
  if (modId<0 || modId>=32) return -1;
  if (m_modIdx[modId] < 0) return -1;
  return m_modules[m_modIdx[modId]]->m_inputLink;
}

int PixModuleGroup::outLink1A(int modId) {
  if (modId<0 || modId>=32) return -1;
  if (m_modIdx[modId] < 0) return -1;
  return m_modules[m_modIdx[modId]]->m_outputLink1;
}

int PixModuleGroup::outLink1B(int modId) {
  if (modId<0 || modId>=32) return -1;
  if (m_modIdx[modId] < 0) return -1;
  return m_modules[m_modIdx[modId]]->m_outputLink2;
}

int PixModuleGroup::outLink2A(int modId) {
  if (modId<0 || modId>=32) return -1;
  if (m_modIdx[modId] < 0) return -1;
  return m_modules[m_modIdx[modId]]->m_outputLink3;
}

int PixModuleGroup::outLink2B(int modId) {
  if (modId<0 || modId>=32) return -1;
  if (m_modIdx[modId] < 0) return -1;
  return m_modules[m_modIdx[modId]]->m_outputLink4;
}

int PixModuleGroup::bocTxLink(int modId) {
  if (modId<0 || modId>=32) return -1;
  if (m_modIdx[modId] < 0) return -1;
  return m_modules[m_modIdx[modId]]->m_bocInputLink;
}

int PixModuleGroup::bocRxLink1(int modId) {
  if (modId<0 || modId>=32) return -1;
  if (m_modIdx[modId] < 0) return -1;
  return m_modules[m_modIdx[modId]]->m_bocOutputLink1;
}

int PixModuleGroup::bocRxLink2(int modId) {
  if (modId<0 || modId>=32) return -1;
  if (m_modIdx[modId] < 0) return -1;
  return m_modules[m_modIdx[modId]]->m_bocOutputLink2;
}

void PixModuleGroup::setRodBusy(unsigned int fr) {
  if (rodDisabled()) return;
  if (fr >= 98 && fr <= 101) {
    m_rodBusyCount++;
  } else {
    m_rodBusyCount = 0;
  }
  unsigned int fr1 = fr%1000;
  if (fr >= 20 && fr <= 101) {
    m_rodBusyCount1++;
  } else {
    m_rodBusyCount1 = 0;
  }
  if (fr1 >= 30) {
    m_rodBusyCount2++;
  } else {
    m_rodBusyCount2 = 0;
  }
  if (fr >= 1098) {
    m_rodBusyCount3++;
  } else {
    m_rodBusyCount3 = 0;
  }
}

void PixModuleGroup::setModLinkStat(std::string mod, std::string stat) {
  if (rodDisabled()) return;
  if (m_modTimeoutCount.find(mod) == m_modTimeoutCount.end()) m_modTimeoutCount[mod] = 0;
  if (stat[1]=='T') {
    m_modTimeoutCount[mod]++;
  } else {
    if (m_rodBusyCount == 0 && m_rodBusyCount1 == 0 && m_rodBusyCount2 == 0 && m_rodBusyCount3 == 0) {
      if (m_modTimeoutCount[mod] > 0) m_modTimeoutCount[mod]--;
    }
  }
  if (m_modOverflowCount.find(mod) == m_modOverflowCount.end()) m_modOverflowCount[mod] = 0;
  if (stat[2]=='O') {
    m_modOverflowCount[mod]++;
  } else {
    m_modOverflowCount[mod] = 0;
  }
  if (m_modHeaderTrailerCount.find(mod) == m_modHeaderTrailerCount.end()) m_modHeaderTrailerCount[mod] = 0;
  if (stat[3]=='H') {
    m_modHeaderTrailerCount[mod]++;
  } else {
    m_modHeaderTrailerCount[mod] = 0;
  }
  if (m_modBusyCount.find(mod) == m_modBusyCount.end()) m_modBusyCount[mod] = 0;
  if (stat[4]=='B') {
    m_modBusyCount[mod]++;
  } else {
    m_modBusyCount[mod] = 0;
  }
  if (m_modDisCount.find(mod) == m_modDisCount.end()) m_modDisCount[mod] = 0;
  if (stat == "D" || stat == "QSD") {
    m_modDisCount[mod]++;
  } else {
    m_modDisCount[mod] = 0;
    m_modErrCount[mod] = 0;
  }
}

void PixModuleGroup::autoDisable( ) {

  //m_dtIs should be set in PixActionsSingleIBL
  if(!m_dtIs){
    PIX_WARNING("Data taking IS Manager not set skiping autodisable");
    return;
  }
  
  PixController *rod = (m_pixCtrl);
  if (rod) {

    std::map<std::string, std::string>::iterator iter = m_modulesToDisable.find(m_rodConn->name());
    if (iter != m_modulesToDisable.end()) {
      std::string val = iter->second;
      time_t t1, t2 = time(NULL);
      std::istringstream is(elementPMG(val, 1, '/'));
      is >> t1;
      if (t2-t1 > 120) {
	m_modulesToDisable.erase(iter);
      }
    }

    std::string modSum;
    bool moduleDisabled = false;
    if (m_is && m_dtIs) {
      int timeit = time(NULL);
      m_is->publish(getISRoot()+"AUTO_DIS_WD", timeit);
      std::string autoDisEna = m_dtIs->read<std::string>(getISRoot()+"AUTO_DISABLE_ENA");
      if (autoDisEna == "ON") {
        m_interactiveInterlock = false;
	if (rodDisabled()) return;
	unsigned int nModDis=0, nProblems=0, nEna=0, nOK=0;
	std::map<std::string, unsigned int>::iterator im;
        if (m_modDisCount.size() == 0) {
          nEna = 999;
	} else {
	  for (im=m_modDisCount.begin(); im != m_modDisCount.end(); im++) {
	    if (im->second == 0) nEna++;
	  } 
	}
        if (m_modErrCount.size() == 0) {
          nOK = 999;
	} else {
	  for (im=m_modErrCount.begin(); im != m_modErrCount.end(); im++) {
	    if (im->second == 0) nOK++;
	  }
	}
        if (m_rodBusyCount > 0 || m_rodBusyCount1 > 0 || m_rodBusyCount2 > 0 || m_rodBusyCount3 > 0) {
	  std::ostringstream os;
          os << " ROD busy count = " << m_rodBusyCount << " " << m_rodBusyCount1 << " " << m_rodBusyCount2 << " " << m_rodBusyCount3;
          ers::info(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), os.str()));
	} 
        unsigned int nTimeout=0;
	for (im=m_modTimeoutCount.begin(); im != m_modTimeoutCount.end(); im++) {
	  if (im->second > 0) {
	    nTimeout++;
	    std::ostringstream os;
	    os << im->first << " Timeout count = " << im->second;
	    ers::info(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), os.str()));
	  }
	}
	if (nTimeout > 0) {
	  if (m_globTimeoutCount < 42) m_globTimeoutCount += 6;
	} else if (m_globTimeoutCount >0) {
	  if (m_rodBusyCount == 0 && m_rodBusyCount1 == 0 && m_rodBusyCount2 == 0 && m_rodBusyCount3 == 0) {
	    m_globTimeoutCount--;
	  }
	}
	for (im=m_modBusyCount.begin(); im != m_modBusyCount.end(); im++) {
	  if (im->second > 0) {
	    std::ostringstream os;
	    os << im->first << " Busy count = " << im->second;
	    ers::info(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), os.str()));
	  }
	}
	for (im=m_modTimeoutCount.begin(); im != m_modTimeoutCount.end(); im++) {
	  if (im->second > 0 && m_rodBusyCount2 > 0) nProblems++;
	  if (im->second >= 4 && (m_rodBusyCount2) >= 4) {
	      if (disableModule(im->first, "T")){
	        nModDis++;
	        moduleDisabled = true;
	      }
	  }
	}
	if (nModDis > 0) {
	  m_modBusyCount.clear();
	}
	if (nProblems == 0) {
	  nModDis = 0;
	  for (im=m_modBusyCount.begin(); im != m_modBusyCount.end(); im++) {
          //If there are a lot of modules to disable per ROD we are stuck here ~5 sec per Module, adding a break to going out of here if user action
          std::string autoDisCheck = m_dtIs->read<std::string>(getISRoot()+"AUTO_DISABLE_ENA");
          if (autoDisCheck.substr(0,7) == "DISABLE"||autoDisCheck.substr(0,7) == "ENABLE")break;

	    if (im->second > 0 && m_rodBusyCount1 > 0) nProblems++;
	    if (im->second >= 2 && m_rodBusyCount1 >= 2) {
	      	if (disableModule(im->first, "B")){
	          nModDis++;
	          moduleDisabled = true;
	        }
	    }
	  }
	}

        // Automatic ROD disable if all modules disabled
	if (nOK == 0 && nEna == 0) {
	  if(!rodDisabled()){
	    m_rodDisableReason = "Z";
	    sendDisableMessage(m_rodConn->name(), "ROD");
	  }
	} else if (nEna > 0) {
          // Automatic ROBIN reset 
          if (m_globTimeoutCount > 0 && m_rodBusyCount3 >= 6) {
	    ers::error(PixLib::pix::daq (ERS_HERE, m_rodConn->name()," BUSY, due to XOFF/LDOWN "));
	    //SendRobin Reset is obsolete. Do we want to do something in this case?
	    /*char* path = getenv("HOME");
	    if (path != NULL) {
	      std::string cmd = path;
	      std::string autoDisPart = m_dtIs->name();
	      cmd = cmd + "/Applications/PixLib/Examples/sendROBINReset "+autoDisPart;
	      std::string msg ="Sending command "+cmd;
	      ers::info(PixLib::pix::daq (ERS_HERE, m_rodConn->name(),msg));
	      system(cmd.c_str());
	    }*/
	  }
          // Automatic ROD disable in case of permanent busy without known reasons
	   if (nProblems == 0 && m_rodBusyCount >= 5) {
	    std::cout << "KaDebug: nProblems=" << nProblems << ", m_rodBusyCount=" << m_rodBusyCount << std::endl;
	      if(!rodDisabled()){
	        m_rodDisableReason = "B";
	        sendDisableMessage(m_rodConn->name(), "ROD");
	        rod->dumpRodBusyStatus( );
	      }
	   }
	}
      } else if (autoDisEna.substr(0,8) == "DISABLE ") {
        if (!m_interactiveInterlock) {
	  if (m_is) m_is->publish(getISRoot()+"AUTO_DIS_DONE", "WAIT");
	  std::string objName;
          objName = autoDisEna.substr(8);

	  std::istringstream ss(objName);
	  std::string toDis;
	  std::string msg = "AutoDisable command DISABLE ( ";
          if(!rodDisabled()){
            while(std::getline(ss, toDis, ',')){
              msg+=toDis+" ";
	        if (toDis == m_rodConn->name()) {
	          m_rodDisableReason = "U";
	          sendDisableMessage(m_rodConn->name(), "ROD");
                  msg = msg + " " + m_rodConn->name();
	        } else if(toDis.find(m_rodConn->name()) != std::string::npos ){//Slink
	          sendDisableMessage(toDis,"ROD");
	          msg = msg + " " + toDis;
	        } else {//Module
	          for (size_t i=0; i<m_modConn.size(); i++) {
	            if(m_modConn[i]->name() == toDis){
	              disableModule(toDis, "U");
	              moduleDisabled = true;
	              break;
	            }
	          }
	        }
	     }
	  }
          msg += " )";
          ers::info(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), msg));
	  if (m_is) m_is->publish(getISRoot()+"AUTO_DIS_DONE", "DONE");
          m_interactiveInterlock = true;
        }
      } else if (autoDisEna.substr(0,7) == "ENABLE ") {
        if (!m_interactiveInterlock) {
	  if (m_is) m_is->publish(getISRoot()+"AUTO_DIS_DONE", "WAIT");
	  std::string objName;
	  objName = autoDisEna.substr(7);

          std::string msg = "AutoDisable command ENABLE (";
	  
	  std::istringstream ss(objName);
	  std::string toDis;
            while(std::getline(ss, toDis, ',')){
              msg+=toDis+" ";
	      if (toDis == m_rodConn->name()) {
	        if(rodDisabled()){
	          sendDisableMessage("+"+objName, "ROD");
	          enableSlink();
	        }
	      } else if(toDis.find(m_rodConn->name()) != std::string::npos ){//Slink
	        enableSlink(objName);
	      } else {
	        for (size_t i=0; i<m_modConn.size(); i++) {
	          if(m_modConn[i]->name() == toDis){
	            enableModule(toDis, "U");
	            break;
	          }
	        }
	      }
	    }
	  
          msg += " )";
          ers::info(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), msg));
	  if (m_is) m_is->publish(getISRoot()+"AUTO_DIS_DONE", "DONE");
          m_interactiveInterlock = true;
        }
      } else if (autoDisEna == "CLEAN") {
	std::string msg = "AutoDisable command CLEAN";
	ers::info(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), msg));
        m_interactiveInterlock = false;
	resetDisableCounters(false);
	if (m_is) m_is->publish(getISRoot()+"AUTO_DIS_DONE", "DONE");
      } else if (autoDisEna == "DUMP") {
	std::string msg = "AutoDisable command DUMP";
	ers::info(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), msg));
        m_interactiveInterlock = false;
	std::map<std::string, unsigned int>::iterator im;
	for (im=m_modErrCount.begin(); im != m_modErrCount.end(); im++) {
	  std::ostringstream os;
	  os << im->second;
	  modSum = os.str();
	  m_is->publish(getISRoot()+im->first+"/AUTODIS_EC", modSum);
	}
	for (im=m_modBusyCount.begin(); im != m_modBusyCount.end(); im++) {
	  std::ostringstream os;
	  os << im->second;
	  modSum = os.str();
	  m_is->publish(getISRoot()+im->first+"/AUTODIS_BC", modSum);
	}
	m_is->publish(getISRoot()+"AUTODIS_RC", m_rodBusyCount);
	m_is->publish(getISRoot()+"AUTODIS_RC1", m_rodBusyCount1);
	m_is->publish(getISRoot()+"AUTODIS_RC2", m_rodBusyCount2);
	m_is->publish(getISRoot()+"AUTODIS_RC3", m_rodBusyCount3);
	if (m_is) m_is->publish(getISRoot()+"AUTO_DIS_DONE", "DONE");
      } else {
        m_interactiveInterlock = false;
      }
      if(moduleDisabled)rod->disableSLink( );//Check if Slink has to be disabled
    }
  }
}

std::vector<std::string> PixModuleGroup::getModules(std::string modName) {
  std::vector<std::string>lmod;
  unsigned int start = 0;
  for (unsigned int i1=0; i1<=modName.size(); i1++) {
    bool sep = false;
    if (i1 == modName.size()) {
      sep = true;
    } else {
      if (modName[i1] == ',') sep = true;
    }
    if (sep) {
      int nlen = i1-start;
      if (nlen > 0) {
        std::string mod= modName.substr(start, nlen);
	for (int i=0; i<4; i++) {
	  if (m_rodConn->obs(i) != NULL) {
	    if (m_rodConn->obs(i)->pp0() != NULL) {
	      for (int j=0; j<=8; j++) {
		if (m_rodConn->obs(i)->pp0()->modules(j) != NULL) {
		  if (mod == m_rodConn->obs(i)->pp0()->modules(j)->name()) {
		    lmod.push_back(mod);
		  }
		}
	      }
	    }
	  }
	}
      }
      start = i1+1; 
    }
  }
  return lmod;
}


bool PixModuleGroup::sendDisableMessage(const std::string &itemName, std::string itemType) {

  std::string applName = m_rodConn->crate()->partition()->daqPartName();
  std::vector<std::string> cmdArgs;
  cmdArgs.emplace_back(std::move(itemName));
  cmdArgs.emplace_back(std::move(itemType));
  cmdArgs.emplace_back(std::string(applName));

  if (m_dtIs) {
    std::string autoDisPart = m_dtIs->name();
    PixLib::PixRcCommander::sendRcUserCommand(autoDisPart,applName,"AUTODISENA", cmdArgs);
    return true;
  }

return false;

}


std::string PixModuleGroup::getDisReason(std::string nameP) {
  std::string name = nameP;
  if (nameP[nameP.size()-3] == '-') name = nameP.substr(0,nameP.size()-3); 
  std::string reason;
  std::map<std::string, std::string>::iterator it = m_modulesToDisable.find(name);
  if (it != m_modulesToDisable.end()) {
    reason = elementPMG(it->second, 0, '/');
    return reason;
  } else {
    return "NONE";
  } 
}

bool PixModuleGroup::enableModule(std::string modNameP, std::string reason) {

  std::string modName = modNameP;
  if (modNameP[modNameP.size()-3] == '-') modName = modNameP.substr(0,modNameP.size()-3); 
  if (m_autoDisabledModules.find(modName) == m_autoDisabledModules.end()){
    std::cout<<"Module "<<modNameP<<" not found in autodisable module list "<<std::endl;
    return true;
  }
  bool ret = sendDisableMessage("+"+modName);

  PixController *rod = (m_pixCtrl);
  if (rod) {
    for (int i1=0; i1<4; i1++) {
      if (m_rodConn->obs(i1) != NULL) {
	if (m_rodConn->obs(i1)->pp0() != NULL) {
	  for (int j=0; j<=8; j++) {
	    if (m_rodConn->obs(i1)->pp0()->modules(j) != NULL) {
	      ModuleConnectivity *mod = m_rodConn->obs(i1)->pp0()->modules(j);
	      if (mod->name() == modName) {
		if (m_autoDisabledModules.find(modName) == m_autoDisabledModules.end()) {
		  std::string txt = "Module "+modName+" was not disabled";
		 ers::warning(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), txt));
		  ret = false;
		} else {
                  if (reason == "R") {
		  }
                  if (!rodDisabled()) {
                    if(reason != "QS")
		    for (int i=0; i<32; i++) {
		      if (getModIdName(i) == modName) {
			rod->enableLink(rod->moduleOutLink(i));
		      }
		    }
                  }
		  std::string txt;
		  m_modTimeoutCount[modName] = 0;
		  m_modBusyCount[modName] = 0;
		  m_modErrCount[modName] = 0;
                  if (reason == "R") {
		    txt = "Module "+modName+" reconfigured and enabled on request";
		  } else if (reason == "QS") {
		    txt = "Module "+modName+" enabled via QuickStatus";
		  } else {
		    txt = "Module "+modName+" enabled on request";
		  }
		  ers::warning(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), txt));
		  std::map<std::string, std::string>::iterator it;
		  it = m_autoDisabledModules.find(modName);
		  if (it != m_autoDisabledModules.end()) {
		    m_autoDisabledModules.erase(it);
		  }
       		  std::string aDis;
		  for (it = m_autoDisabledModules.begin(); it != m_autoDisabledModules.end(); it++) {
		    aDis = aDis + it->first + "(" + it->second + ")/";
		  }
		  m_is->publish(getISRoot()+"AUTO_DISABLED_MODULES", aDis);
		  m_is->publish(getISRoot()+modName+"/DAQEnabled", 0);
		  ret = true;
		}
	      }
	    }
	  }
	}
      }
    }
  }
  return ret;
}

bool PixModuleGroup::disableModule(std::string modNameP, std::string reason) {
  std::string modName = modNameP;
  if (modNameP[modNameP.size()-3] == '-') modName = modNameP.substr(0,modNameP.size()-3);
  if (m_autoDisabledModules.find(modName) != m_autoDisabledModules.end()){
   std::cout<<"Module "<<modNameP<<" already found in autodisable module list "<<std::endl;
   return true;
  }
 bool ret = sendDisableMessage(modName);
  
 PixController *rod = (m_pixCtrl);
  if (rod) {
    for (int i1=0; i1<4; i1++) {
      if (m_rodConn->obs(i1) != NULL) {
	if (m_rodConn->obs(i1)->pp0() != NULL) {
	  for (int j=0; j<=8; j++) {
	    if (m_rodConn->obs(i1)->pp0()->modules(j) != NULL) {
	      ModuleConnectivity *mod = m_rodConn->obs(i1)->pp0()->modules(j);
	      if (mod->name() == modName) {
		if (m_autoDisabledModules.find(modName) != m_autoDisabledModules.end()) {
		  std::string txt = "Module "+modName+" was already disabled";
		  std::cout<<txt<<std::endl;
		  ers::warning(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), txt));
		  ret = false;
		} else {
                  if (!rodDisabled()) {
                    if(reason != "QS")
		    for (int i2=0; i2<32; i2++) {
		      if (getModIdName(i2) == modName) {
		        rod->disableLink(rod->moduleOutLink(i2));
		        //std::cout<<"Disable link sent for "<<modName<<std::endl;
		      }
		    }
                  }
		  std::string txt;
		  if (reason  == "T") {
		    m_modTimeoutCount[modName] = 0;
		    txt = "Module "+modName+" automatically disabled (Timeout)";
		    m_is->publish(getISRoot()+modName+"/DAQEnabled", 2);
		  } else if (reason == "B") {
		    m_modBusyCount[modName] = 0;
		    txt = "Module "+modName+" automatically disabled (Busy)";
		    m_is->publish(getISRoot()+modName+"/DAQEnabled", 3);
		  } else if (reason == "QS") {
		    txt = "Module "+modName+" disabled via QuickStatus";
		    m_is->publish(getISRoot()+modName+"/DAQEnabled", 5);
		  } else {
		    m_modTimeoutCount[modName] = 0;
		    m_modBusyCount[modName] = 0;
		    txt = "Module "+modName+" disabled on request";
		    m_is->publish(getISRoot()+modName+"/DAQEnabled", 4);
		  }
		  m_modErrCount[modName] = 1;
		  std::cout<<txt<<std::endl;
		  ers::warning(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), txt));
		  m_autoDisabledModules[modName] = reason;
		  std::string aDis;
		  std::map<std::string, std::string>::iterator it;
		  for (it = m_autoDisabledModules.begin(); it != m_autoDisabledModules.end(); it++) {
		    aDis = aDis + it->first + "(" + it->second + ")/";
		  }
		  m_is->publish(getISRoot()+"AUTO_DISABLED_MODULES", aDis);
		  ret = true;
		}
	      }
	    }
	  }
	}
      }
    }
  }
  return ret;
}

bool PixModuleGroup::enableSlink( ) {

  std::cout << __PRETTY_FUNCTION__<< std::endl;

    if(m_is){
      std::string isSLinkName = getISRoot()+"AUTO_DISABLED_SLINKS";
        if(m_is->exists(isSLinkName)){
          std::string disabledSlinks = m_is->read<std::string>(isSLinkName);
          std::cout<<disabledSlinks<<std::endl;
          std::string sLinkName;
            while(disabledSlinks.find(m_rodConn->name()) != std::string::npos){
             sLinkName = disabledSlinks.substr(0,disabledSlinks.find_first_of("/"));
               if(sLinkName.find(m_rodConn->name()) == std::string::npos)return false;
               disabledSlinks.erase(0,disabledSlinks.find_first_of("/")+1);
               PIX_INFO(" Slink to enable "<< sLinkName<<" "<<disabledSlinks);
	       enableSlink (sLinkName);
	    }
        }
      return true;
    }

  return false;

}

bool PixModuleGroup::enableSlink(const std::string &SlinkName) {

  std::cout << __PRETTY_FUNCTION__<<" "<<SlinkName<< std::endl;

    if(m_is){
      std::string isSLinkName = getISRoot()+"AUTO_DISABLED_SLINKS";
        if(m_is->exists(isSLinkName)){
          std::string disabledSlinks =  m_is->read<std::string>(isSLinkName);
	  size_t pos = disabledSlinks.find(SlinkName);
	      if(pos == std::string::npos){
	      ers::error(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), "Slink name "+SlinkName+" not found in disabled Slinks "+disabledSlinks));
	      return false;
	    }
	  std::cout<<__PRETTY_FUNCTION__<<" Disabled slinks "<< disabledSlinks<<std::endl;
	  disabledSlinks.erase(pos,SlinkName.size()+1);
	  sendDisableMessage("+"+SlinkName,"ROD");
	  std::cout<<__PRETTY_FUNCTION__<<" Disabled slinks "<< disabledSlinks<<std::endl;
	  m_is->publish(isSLinkName,disabledSlinks);
        }
      return true;
    }

  return false;

}

bool PixModuleGroup::slinkDisabled(const std::string &SlinkName) {

  if(m_is){
    std::string isDisSlinks = getISRoot()+"AUTO_DISABLED_SLINKS";
    std::string disabledSlinks ="";
    if(m_is->exists(isDisSlinks))disabledSlinks = m_is->read<std::string>(isDisSlinks);
      if(disabledSlinks.find(SlinkName) != std::string::npos) return true;
  }
  return false;

}


bool PixModuleGroup::disableSlink(const std::string &SlinkName) {

 std::cout << __PRETTY_FUNCTION__<< std::endl;

 if (getCtrlType() == L12ROD){
   if( !(SlinkName == m_rodConn->name()+"_0" || SlinkName == m_rodConn->name()+"_1") ){
     ers::error(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), "Slink name "+SlinkName+" doesn't belong to "+m_rodConn->name()));
     return false;
   }
 } else if(getCtrlType() == IBLROD || getCtrlType() == CPPROD){
   if( !(SlinkName == m_rodConn->name()+"_0" || SlinkName == m_rodConn->name()+"_1" || SlinkName == m_rodConn->name()+"_2" || SlinkName == m_rodConn->name()+"_3") ){
     ers::error(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), "Slink name "+SlinkName+" doesn't belong to "+m_rodConn->name()));
     return false;
   }
 } else {
   ers::error(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), "Disable SLink, ROD ctrl type "+std::to_string(getCtrlType())+" not found"));
   return false;
 }
 
  if(m_is){
    std::string isDisSlinks = getISRoot()+"AUTO_DISABLED_SLINKS";
    std::string disabledSlinks ="";
    if(m_is->exists(isDisSlinks))disabledSlinks = m_is->read<std::string>(isDisSlinks);
      if(disabledSlinks.find(SlinkName) == std::string::npos){
        disabledSlinks += SlinkName + "/";
        m_is->publish(isDisSlinks,disabledSlinks);
      }
    return true;
  }

  return false;

}

bool PixModuleGroup::enableRod( ) {
  if (!rodDisabled()) return true;

  std::string txt;

  std::string rodName = "*"+m_rodConn->name();
  sendDisableMessage(rodName, "ROD");

  ers::info(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), "ENABLED"));
  m_rodDisableReason = "";
  m_rodDisabled=false;
  if (m_is) {
    m_is->publish(getISRoot()+"AUTO_DISABLED_ROD","");
    m_is->publish(getISRoot()+"DAQEnabled", 0);  
  }
  return true;
}

bool PixModuleGroup::disableRod( ) {
  if (rodDisabled()) return true;

  ers::info(PixLib::pix::daq (ERS_HERE, m_rodConn->name(),"disableRod called"));

    std::string txt;
    if (m_rodDisableReason == "U") {
      txt = "ROD "+m_rodConn->name()+" disabled on request";
      m_is->publish(getISRoot()+"DAQEnabled", 4);
    } else if (m_rodDisableReason == "B") {
      txt = "ROD "+m_rodConn->name()+" automatically disabled (Busy)";
      m_is->publish(getISRoot()+"DAQEnabled", 3);
    } else if (m_rodDisableReason == "Z") {
      txt = "ROD "+m_rodConn->name()+" automatically disabled (no modules left)";
      m_is->publish(getISRoot()+"DAQEnabled", 2);
    } else {
      txt = "ROD "+m_rodConn->name()+" disabled by unknown reason";
      m_is->publish(getISRoot()+"DAQEnabled", 4);
    }

    ers::warning(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), txt));
    m_rodDisabled = true;

    if (m_is) {
      m_is->publish(getISRoot()+"AUTO_DISABLED_ROD", m_rodConn->name()+"("+m_rodDisableReason+")");  
    }

    resetDisableCounters(false);

  return true;
}

void PixModuleGroup::resetDisableCounters(bool full) {
  m_modTimeoutCount.clear();
  m_modOverflowCount.clear();
  m_modHeaderTrailerCount.clear();
  m_modBusyCount.clear();
  m_modErrCount.clear();
  m_modDisCount.clear();
  m_modResetCount.clear();
  m_rodBusyCount=0;
  m_rodBusyCount1=0;
  m_rodBusyCount2=0;
  m_rodBusyCount3=0;
  m_globTimeoutCount=0;
  if (full) {
    m_autoDisabledModules.clear();
    m_modulesToDisable.clear();
    m_rodDisableReason = "";
    m_rodDisabled = false;
    if (m_is) {
      m_is->publish(getISRoot()+"QS_DISABLED_MODULES", "");
      m_is->publish(getISRoot()+"AUTO_DISABLED_MODULES", "");  
      m_is->publish(getISRoot()+"AUTO_DISABLED_ROD", "");
      m_is->publish(getISRoot()+"AUTO_DISABLED_SLINKS", "");
      m_is->publish(getISRoot()+"DAQEnabled", 0);  
      PixController *rod = m_pixCtrl;
      if (rod) {
	for (int i=0; i<4; i++) {
	  if (m_rodConn->obs(i) != NULL) {
	    if (m_rodConn->obs(i)->pp0() != NULL) {
	      for (int j=0; j<=8; j++) {
		if (m_rodConn->obs(i)->pp0()->modules(j) != NULL) {
		  ModuleConnectivity *mod = m_rodConn->obs(i)->pp0()->modules(j);
		  std::string modName = mod->name();
		  if (mod->active() && mod->enableReadout) {
		    m_is->publish(getISRoot()+modName+"/DAQEnabled", 0);  
		  } else {
		    m_is->publish(getISRoot()+modName+"/DAQEnabled", 1);  
		  }
		}
	      }
	    }
	  }
	}
      }
    }
  }
}

void PixModuleGroup::reApplyModuleDisable() {
  PixController *rod = (m_pixCtrl);
  if (rod) {
    for (int i1=0; i1<4; i1++) {
      if (m_rodConn->obs(i1) != NULL) {
	if (m_rodConn->obs(i1)->pp0() != NULL) {
	  for (int j=0; j<=8; j++) {
	    if (m_rodConn->obs(i1)->pp0()->modules(j) != NULL) {
	      ModuleConnectivity *mod = m_rodConn->obs(i1)->pp0()->modules(j);
	      std::string modName = mod->name();
	      if (m_autoDisabledModules.find(modName) != m_autoDisabledModules.end()) {
		for (int i2=0; i2<32; i2++) {
		  if (getModIdName(i2) == modName) {
		    rod->disableLink(rod->moduleOutLink(i2));
		  }
		}
	      }
	    }
	  }
	}
      }
    }
  }
rod->disableSLink( );//Check if SLink has to be disabled

}

void PixModuleGroup::updateDisableRodRecover( ){

PixController *rod = (m_pixCtrl);
  if (rod) {
    for (int i=0; i<4; i++) {
      if (m_rodConn->obs(i) != nullptr) {
	if (m_rodConn->obs(i)->pp0() != nullptr) {
	  for (int j=0; j<=8; j++) {
	    if (m_rodConn->obs(i)->pp0()->modules(j) != nullptr) {
	      ModuleConnectivity *mod = m_rodConn->obs(i)->pp0()->modules(j);
	      std::string modName = mod->name();
		if (m_autoDisabledModules.find(modName) != m_autoDisabledModules.end()) {
		  std::cout<<__PRETTY_FUNCTION__ <<" "<<modName<<std::endl;
		  enableModule(modName,"R");
		}
	    }
	  }
	}
      }
    }
  }
  else{
  std::cout<<"ROD not found"<<std::endl;
  }

}

bool PixModuleGroup::rodDisabled() {
  return m_rodDisabled;
}

void PixModuleGroup::setRodDisableReason(std::string reason) {
    m_rodDisableReason = reason;
}

void PixModuleGroup::disconnect() {
  m_dtIs = nullptr;
}

void PixModuleGroup::warmReconfig(std::string mode) {
  
  std::string txt = "warmReconfig called; mode = "+mode;
  ers::warning(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), txt));
  sleep(20);
  ers::warning(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), "warmReconfig exiting"));
}

bool  PixModuleGroup::writeConfigAll(PixDbServerInterface *DbServer, std::string domainName, std::string tag, std::string ptag, unsigned int revision) {
  
  if(!m_config->write(DbServer, domainName, tag, m_name,"PixModuleGroup", ptag, revision)) {
    std::cout << "PixModuleGroup : ERROR in writing config on DBserver" << std::endl;
    return false;
  }   

  /*
    std::vector<PixModule*>::iterator mit;
    for( mit= m_modules.begin(); mit!= m_modules.end(); mit++) {
    if(!(*mit)->writeConfig(DbServer, domainName, tag)) 
    return false;
    }
  */
  PixController* rodCtrl = (getPixController());
  if(!rodCtrl->writeConfig(DbServer,domainName, tag, ptag, revision))
    return false;
  
  if(m_pixBoc!=0){
    if(!m_pixBoc->writeConfig(DbServer, domainName, tag, ptag, revision)) 
      return false;
  }

  return true;
}

bool  PixModuleGroup::writeConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, std::string ptag, unsigned int revision) {
  if(m_config->write(DbServer, domainName, tag, m_name,"PixModuleGroup", ptag, revision))
    return true;
  else {
    std::cout << "PixModuleGroup : ERROR in writing config on DBserver" << std::endl;
    return false;
  } 
}

bool PixModuleGroup::readConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, unsigned int revision) {
  if(!m_config->read(DbServer, domainName, tag, m_name,revision)) {
    std::cout << "PixModuleGroup : ERROR in reading config from DBserver" << std::endl;
    return false;
  }   
  
  PixController* rodCtrl = (getPixController());
  if(!rodCtrl->readConfig(DbServer,domainName, tag,revision))
    return false;
  
  if(m_pixBoc!=0){
    if(!m_pixBoc->readConfig(DbServer, domainName, tag,revision)) 
      return false;
  }

  return true;
}

void PixModuleGroup::setTemp() {
  m_config->penTag("Temp");
}

void PixModuleGroup::saveChangedConfig(std::string tag){
  if (m_cfgChanged){
    if (m_dbserverI != NULL) {
      setTemp();
      std::string tags;
      if (tag != ""){
	tags = tag;
      }
      else {
	tags = pixConnectivity()->getCfgTag();
      }
      if (writeConfigAll(m_dbserverI, "Configuration-"+pixConnectivity()->getConnTagOI(), tags, "PixModuleGroup")){
	m_cfgChanged = false;
      }
      else {
	std::cout << "PixModuleGroup::saveChangedConfig: ERROR in saving config to DbServer" << std::endl;
      }
    }
    else {
      std::cout << "PixModuleGroup::saveChangedConfig: ERROR no PixDbServerInterface" << std::endl;
    }
  }
}

void PixModuleGroup::reloadConfig(){
  bool result = false;
  if (m_dbserverI != NULL) {
    std::string tags = pixConnectivity()->getCfgTag();
    result = readConfig(m_dbserverI, "Configuration-"+pixConnectivity()->getConnTagOI(), tags);
    if (result){
      m_cfgChanged = false;
    }
    else {
      std::cout << "PixModuleGroup::reloadConfig: ERROR in reading config from DbServer" << std::endl;
    }
  }
  else {
    std::cout << "PixModuleGroup::reloadConfig: ERROR no PixDbServerInterface" << std::endl;
  }
}




void PixModuleGroup::sendDDCcommand(std::string pp0Name, float value, PixDcsDefs::DDCCommands command)
{
  int status;
  std::stringstream info;
  std::string rodName =  m_rodConn->name();
  int tryToSend = 10;
  if (m_dcs == NULL) return;
  do {
    status = m_dcs->writeDataPoint(pp0Name, value, command);
    if (status==PixDcsDefs::OK) {
      std::cout << "Command succesfully executed by " << rodName << std::endl;
      tryToSend = 0;
    }
    else if (status==PixDcsDefs::ERR_UNDEF) {
      info << "PixDcs::execCommand - undefined status - ROD: "<< rodName << " and pp0: "<< pp0Name << " ";
      ers::error(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), info.str()));
      throw(PixModuleGroupExc(PixModuleGroupExc::FATAL, "DDC", info.str()));
    } 
    else if (status==PixDcsDefs::ERR_NAME) {
      info << "PixDcs::execCommand - invalid command name - ROD: ";
      ers::error(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), info.str()));
      throw(PixModuleGroupExc(PixModuleGroupExc::FATAL, "DDC", info.str()));
    }
    else if (status==PixDcsDefs::ERR_DCSDP) {
      info << "PixDcs::execCommand - invalid dcs datapoint definition - ROD: "<< rodName << " and pp0: "<< pp0Name << " ";
      ers::error(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), info.str()));
      throw(PixModuleGroupExc(PixModuleGroupExc::FATAL, "DDC", info.str()));
    }
    else if (status==PixDcsDefs::ERR_TIMEOUT) {
      std::stringstream info2;
      info2 << "PixDcs::execCommand - timeout expired - retry " << tryToSend << " times ";
      ers::warning(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), info2.str()));
      sleep(1);
      tryToSend--;
      if (tryToSend == 0) {
	std::stringstream info3;
	info3 << "PixDcs::execCommand - timeout expired - ROD: "<< rodName << " and pp0: "<< pp0Name << " ";
	throw(PixModuleGroupExc(PixModuleGroupExc::FATAL, "DDC", info3.str()));
      }
    }
    else if (status==PixDcsDefs::ERR_CONNECT) {
      info << "PixDcs::execCommand - SCADA communication fault (no connection) - ROD:"<< rodName << " and pp0: "<< pp0Name << " ";
      ers::fatal(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), info.str()));
      throw(PixModuleGroupExc(PixModuleGroupExc::FATAL, "DDC", info.str()));
    }
    else if (status==PixDcsDefs::ERR_CONNBROKEN) {
      info << "PixDcs::execCommand - SCADA communication fault (connection broken) - ROD: "<< rodName << " and pp0: "<< pp0Name << " ";
      ers::fatal(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), info.str()));
      throw(PixModuleGroupExc(PixModuleGroupExc::FATAL, "DDC", info.str()));
    } 
    else if (status==PixDcsDefs::ERR_CONFIG) {
      info << "PixDcs::execCommand - invalid configuration - ROD:"<< rodName << " and pp0: "<< pp0Name << " ";
      ers::error(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), info.str()));
      throw(PixModuleGroupExc(PixModuleGroupExc::FATAL, "DDC", info.str()));
    } 
    else if (status==PixDcsDefs::ERR_NTDESC) {
      info << "PixDcs::execCommand - invalid definition of an nt-command - ROD:"<< rodName << " and pp0: "<< pp0Name << " ";
      ers::error(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), info.str()));
      throw(PixModuleGroupExc(PixModuleGroupExc::FATAL, "DDC", info.str()));
    } 
    else if (status==PixDcsDefs::ERR_BUSY) {
      info << "PixDcs::execCommand -  previous command has not ended - ROD: "<< rodName << " and pp0: "<< pp0Name << " ";
      ers::error(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), info.str()));
      sleep(1);
      tryToSend--;
      if (tryToSend == 0) {
	std::stringstream info2;
	info2 << "PixDcs::execCommand - busy - ROD: "<< rodName << " and pp0: "<< pp0Name << " ";
	throw(PixModuleGroupExc(PixModuleGroupExc::FATAL, "DDC", info2.str()));
      } 
    }
    else {
      tryToSend = 0;
    }
  } while (tryToSend>0);
 
}



void PixModuleGroup::readDDC(std::string pp0Name, PixDcsDefs::DataPointType variable, float& value)
{ 
  int tryToRead=3;
  std::string rodName =  m_rodConn->name();
  if (m_dcs == NULL) return;
  do {
    try {
      m_dcs->read(pp0Name, variable, value);
      tryToRead = 0;
    } catch (const PixDcsExc &e) {
      if (e.getId() == "ISINFORMATION") {
	sleep(7); // 4.11.2008: Pixel ELMB readout takes 7s
	tryToRead--;
	if (tryToRead == 0) {
	  std::stringstream message;
	  message << "Information not published - ROD: " << rodName << " pp0: " << pp0Name << " "; 
	  throw(PixModuleGroupExc(PixModuleGroupExc::FATAL, "ISSERVER", message.str()));
	}
      }
      else if (e.getId() == "ISSERVERREPOSITORY" 
	       || e.getId() == "ISSERVERNAME" 
	       || e.getId() == "ISINFOCOMPATIBLE") {
	std::stringstream message;
	message <<  "IS Server cannot be used - ROD: " << rodName << " pp0: " << pp0Name << " "; 
	throw(PixModuleGroupExc(PixModuleGroupExc::FATAL, "ISSERVER", message.str()));
      }
    } catch (...) {
      std::stringstream message;
      message <<  " Unknown exception was thrown - ROD: " << rodName << " pp0: " << pp0Name << " ";
      throw(PixModuleGroupExc(PixModuleGroupExc::FATAL, "UNKNOWN", message.str()));
    }
  } while(tryToRead>0);

}

void PixModuleGroup::sendFSMcommand(std::string objList, PixDcsDefs::FSMCommands command)
{
  int status;
  std::stringstream info;
  std::string rodName =  m_rodConn->name();
  int tryToSend = 10;
  if (m_dcs == NULL) return;
  do {
    status = m_dcs->sendCommand(command, objList);
    if (status==PixDcsDefs::OK) {
      std::cout << "Command succesfully executed by " << rodName << std::endl;
      tryToSend = 0;
    }
    else if (status==PixDcsDefs::ERR_UNDEF) {
      info << "PixDcs::execCommand - undefined status - ROD: "<< rodName << " and pp0s/modules: "<< objList << " ";
      ers::error(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), info.str()));
      throw(PixModuleGroupExc(PixModuleGroupExc::FATAL, "DDC", info.str()));
    } 
    else if (status==PixDcsDefs::ERR_NAME) {
      info << "PixDcs::execCommand - invalid command name - ROD: ";
      ers::error(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), info.str()));
      throw(PixModuleGroupExc(PixModuleGroupExc::FATAL, "DDC", info.str()));
    }
    else if (status==PixDcsDefs::ERR_DCSDP) {
      info << "PixDcs::execCommand - invalid dcs datapoint definition - ROD: "<< rodName << " and pp0s/modules: "<< objList << " ";
     ers::error(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), info.str()));
      throw(PixModuleGroupExc(PixModuleGroupExc::FATAL, "DDC", info.str()));
    }
    else if (status==PixDcsDefs::ERR_TIMEOUT) {
      std::stringstream info2;
      info2 << "PixDcs::execCommand - timeout expired - retry " << tryToSend << " times ";
      ers::warning(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), info2.str()));
      sleep(1);
      tryToSend--;
      if (tryToSend == 0) {
	std::stringstream info3;
	info3 << "PixDcs::execCommand - timeout expired - ROD: "<< rodName << " and pp0s/modules: "<< objList << " ";
	throw(PixModuleGroupExc(PixModuleGroupExc::FATAL, "DDC", info3.str()));
      }
    }
    else if (status==PixDcsDefs::ERR_CONNECT) {
      info << "PixDcs::execCommand - SCADA communication fault (no connection) - ROD:"<< rodName << " and pp0s/modules: "<< objList << " ";
      ers::fatal(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), info.str()));
      throw(PixModuleGroupExc(PixModuleGroupExc::FATAL, "DDC", info.str()));
    }
    else if (status==PixDcsDefs::ERR_CONNBROKEN) {
      info << "PixDcs::execCommand - SCADA communication fault (connection broken) - ROD: "<< rodName << " and pp0s/modules: "<< objList << " ";
      ers::fatal(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), info.str()));
      throw(PixModuleGroupExc(PixModuleGroupExc::FATAL, "DDC", info.str()));
    } 
    else if (status==PixDcsDefs::ERR_CONFIG) {
      info << "PixDcs::execCommand - invalid configuration - ROD:"<< rodName << " and pp0s/modules: "<< objList << " ";
      ers::error(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), info.str()));
      throw(PixModuleGroupExc(PixModuleGroupExc::FATAL, "DDC", info.str()));
    } 
    else if (status==PixDcsDefs::ERR_NTDESC) {
      info << "PixDcs::execCommand - invalid definition of an nt-command - ROD:"<< rodName << " and pp0s/modules: "<< objList << " ";
      ers::error(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), info.str()));
      throw(PixModuleGroupExc(PixModuleGroupExc::FATAL, "DDC", info.str()));
    } 
    else if (status==PixDcsDefs::ERR_BUSY) {
      info << "PixDcs::execCommand -  previous command has not ended - ROD: "<< rodName << " and pp0s/modules: "<< objList << " ";
      ers::warning(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), info.str()));
      sleep(1);
      tryToSend--;
      if (tryToSend == 0) {
	std::stringstream info2;
	info2 << "PixDcs::execCommand - busy - ROD: "<< rodName << " and pp0s/modules: "<< objList << " ";
	throw(PixModuleGroupExc(PixModuleGroupExc::FATAL, "DDC", info2.str()));
      } 
    }
    else if (status==PixDcsDefs::ERR_DCS) {
      info << "PixDcs::execCommand - wrong starting state or object name - ROD:"<< rodName << " and pp0s/modules: "<< objList << " ";
      ers::error(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), info.str()));
      throw(PixModuleGroupExc(PixModuleGroupExc::FATAL, "DCSSTATE", info.str()));
    }
    else {
      tryToSend = 0;
    }
  } while (tryToSend>0);
 
}

// TB: getters, setters and helper functions for the moved PixScan functions

   int PixModuleGroup::getmoduleSize() {
     return m_modules.size();
   }

   void PixModuleGroup::setmoduleConfigActive(int mod, bool mc) {
     m_modules[mod]->m_configActive = mc;
   }
   
   void PixModuleGroup::setmoduleTriggerActive(int mod, bool mc) {
     m_modules[mod]->m_triggerActive = mc;
   }

   void PixModuleGroup::setmoduleStrobeActive(int mod, bool mc) {
     m_modules[mod]->m_strobeActive = mc;
   }

   void PixModuleGroup::setmoduleReadoutActive(int mod, bool mc) {
     m_modules[mod]->m_readoutActive = mc;
   }
   
    int PixModuleGroup::getmoduleId(int mod) { 
      return m_modules[mod]->m_moduleId; 
    }

   void PixModuleGroup::setm_useScanInSwap(bool mc) {
     m_useScanInSwap = mc;
   }

   void PixModuleGroup::setm_useScanOutSwap(bool mc) {
     m_useScanOutSwap = mc;
   }

   void PixModuleGroup::setm_useScanModAltLink(bool mc) {
     m_useScanModAltLinks = mc;
   }
   
   void PixModuleGroup::setm_scanModAltLinks(unsigned int modalt) {
     m_scanModAltLinks = modalt;
   }

void PixModuleGroup::storeConfig(int mod, std::string scanConfig) {
  m_modules[mod]->storeConfig(scanConfig);
 }

void PixModuleGroup::moduleConfigIdentifier() {
  for (int modId = 0; modId<32; modId++){
    std::stringstream info;
    int midx = m_modIdx[modId];
    if (midx == -1)
      continue;
    if(m_modConn[midx]->active() && m_modConn[midx]->enableReadout){
      //readModuleConfigIdentifier test
      char tag[128], idStr[128];
      uint32_t revision, crc32chksm;
      PixController *rod = m_pixCtrl;
      rod->readModuleConfigIdentifier(modId, idStr, tag, &revision,&crc32chksm);
      if (revision == 0 ){
	info << m_name << " modId = " << modId << " modConfig revision = 0 -> CONFIGURATION CORRUPTED, disableing module!!!!! ";
	ers::error(PixLib::pix::daq (ERS_HERE, m_rodConn->name(), info.str()));
	m_modConn[midx]->enableReadout = false;
      }
    }
  }
}    

void PixModuleGroup::sendModuleConfig() {
  m_pixCtrl->sendModuleConfig(getActiveModuleMask(),0x3fff);
 }

void PixModuleGroup::sendModuleConfigWithPreampOff() {
  m_pixCtrl->sendModuleConfigWithPreampOff(getActiveModuleMask(),0x3fff);
 }


void PixModuleGroup::setm_execToTerminate(bool mc) {
  m_execToTerminate = mc;
}


int PixModuleGroup::getm_execToTerminate() {
  return m_execToTerminate;
}


bool PixModuleGroup::getpixCtrl_moduleActive(int mod) {
  return m_pixCtrl->moduleActive(mod);
}


void PixModuleGroup::rodStopScan() {                 
  PixController *rod = (m_pixCtrl);
  rod->stopScan();
}


bool PixModuleGroup::getmoduleReadoutActive(int mod) {
  return m_modules[mod]->m_readoutActive;
}


void PixModuleGroup::restoreModuleConfig(PixScan* scn, std::string scanConfig) {
  for (unsigned int pmod=0; pmod<m_modules.size(); pmod++){
    //schsu: can we only download config to MDSP for active module?  
    //if(!getModuleActive(pmod)){
    //   std::cout <<"debug terminateScan NotActive pmod "<< pmod <<" modId "<< m_modules[pmod]->m_moduleId << std::endl; 
    // continue;
    //}
    for (int gr=0; gr<MAX_GROUPS;gr++){
      if ((scn->getModuleMask(gr)) & ((0x1)<<(m_modules[pmod]->m_moduleId))) { 
	m_modules[pmod]->restoreConfig(scanConfig);
	m_modules[pmod]->deleteConfig(scanConfig);
      }
    }
	    m_pixCtrl->writeModuleConfig(*(m_modules[pmod]));
	    //m_modules[pmod]->writeConfig(); // download the config again
  }
  m_configSent = true;
}

//TBi TODO remove
std::string PixModuleGroup::getRodStat() {
  return std::string();
}

int PixModuleGroup::getFeFlav() {                // T
  //m_feFlav = m_modules[0]->feFlavour();
  return m_feFlav; 
}
