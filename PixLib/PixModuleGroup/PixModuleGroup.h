/////////////////////////////////////////////////////////////////////
// PixModuleGroup.h
// version 1.0.1
/////////////////////////////////////////////////////////////////////
//
// 08/04/03  Version 1.0 (PM)
//           Initial release
//
// 14/04/03  Version 1.0.1 (CS)
//           Added Configuration DataBase
//

//! Class for a group of module connected to the same controller

#ifndef _PIXLIB_MODULEGROUP
#define _PIXLIB_MODULEGROUP

#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include <fstream>
#include <set>
#include <map>

#include "BaseException.h"
#include "iblModuleConfigStructures.h"
#include "moduleConfigStructures.h"


#include "Bits/Bits.h"
#include "Config/Config.h"
#include "PixDcs/PixDcsDefs.h"
#include "PixEnumBase.h"


namespace PixLib {

  class Histo;  
  class PixDbInterface;
  class DbRecord;
  class PixController;
  class PixFe;   
  class PixModule;
  class PixBoc;
  class Config;
  class PixScan;
  class PixConnectivity;
  class RodBocConnectivity;
  class ModuleConnectivity;
  class PixDcs;
  class PixISManager;
  class ConfInt;
  class PixDbServerInterface;
  class PixHistoServerInterface;
  class PixModuleGroup;

 
  /*  enum MccBandwidth   { SINGLE_40 = MCC_SINGLE_40, DOUBLE_40 = MCC_DOUBLE_40, 
			SINGLE_80 = MCC_SINGLE_80, DOUBLE_80 = MCC_DOUBLE_80,
                        MCC_SPEED_UNK = 999 };
  */
  enum CtrlType: int       { DUMMY, ROD, IBLROD, USBPIX, RCE, CPPROD, L12ROD, UNKNOWN=999 };
  /*  enum FeType         { FE_I_1, FE_I_2, FE_I_3, FE_I_4 }; */

  class PixModuleGroupExc : public SctPixelRod::BaseException { 
  public:
    enum ErrorLevel{INFO, WARNING, ERROR, FATAL};
    
    PixModuleGroupExc(ErrorLevel el, std::string id, std::string descr) : BaseException(id), m_errorLevel(el), m_id(id), m_descr(descr) {}; 
    virtual ~PixModuleGroupExc() {};

    virtual void dump(std::ostream &out) {
      out << "Pixel Module Group Exception: " << dumpLevel() << ":" << m_id << " - "<< m_descr << std::endl; 
    };
    std::string dumpLevel() {
      switch (m_errorLevel) {
      case INFO : 
	return "INFO";
      case WARNING :
	return "WARNING";
    case ERROR :
      return "ERROR";
      case FATAL :
	return "FATAL";
      default :
	return "UNKNOWN";
      }
    };
    ErrorLevel getErrorLevel() const  { return m_errorLevel; }; 
    std::string getId() const  { return m_id; };
    std::string getDescr() const  { return m_descr; };
  private:
    ErrorLevel m_errorLevel;
    std::string m_id;
    std::string m_descr;
  };

  class ModGrpConfig : public Config {
    public:
    ModGrpConfig(std::string name, PixModuleGroup &modg) : Config(name), m_grp(modg) {};                          // Constructor
    ModGrpConfig(const ModGrpConfig &modg) : Config(modg), m_grp(modg.m_grp) {};                                  // Copy constructor
    virtual ~ModGrpConfig() {};                                                                                   // Destructor

    virtual void subConfRead(std::map< std::string, std::string >fields, std::map<std::string, Config *>&subConf); 

    private:
    PixModuleGroup &m_grp;
  };

  class PixModuleGroup : public EnumMccBandwidth,public EnumFEflavour {     
  public:
    
    typedef std::vector<PixModule*>::iterator moduleIterator; 
    typedef std::vector<ModuleConnectivity*>::iterator modConnIterator; 
    
    //! Default Constructor 
    PixModuleGroup(std::string name, CtrlType type=ROD, bool bocAvailable=true); 
    //! Constructor with ConnectivityDB
    PixModuleGroup(PixConnectivity *pixc, std::string name, std::string partitionName);
    PixModuleGroup(PixConnectivity *pixc, std::string name);

    //! Constructor from dBServer
    PixModuleGroup(PixConnectivity *pixc,          PixDbServerInterface *dbs,      PixDcs *dcs, 
		   std::string name, 
		   std::string partitionName = "");
    PixModuleGroup(PixConnectivity *pixc, PixDbServerInterface *dbs, std::string name);
    
    //! Constructor with PixDcs 
    PixModuleGroup(PixConnectivity *pixc, std::string name, PixDcs *dcs, std::string partitionName = "");
                                                              
   ~PixModuleGroup();                                         //! Destructor  
   
   void initController();                                     //! Build controller class based on m_crtlType
   void initHW();                                             //! (re)initialize
   void resetRod();                                           //! initHW on the PixController
   void configureBoc();                                       //! call BocConfigure on the boc object (if present). Call only after resetRod, for debugging purposes
   void testHW();                                             //! hardware test
   void init();                                               //! full init
   void configure();                                          //! configure

   void updateDCSDisableMap();                                // Update the list of modules disabled in DCS
   bool disabledFromDCS(std::string modName);    
   bool disableSlink(const std::string &SlinkName);
   bool slinkDisabled(const std::string &SlinkName);
   void resetViset(float val=-1.0);
   void DCSsync();
   void optoOnOff(bool switchOn);
   void lvOnOff(bool switchOn);
   std::pair<int,int> getOBModConnId(int);

   // Accessors
   PixController *getPixController() { return m_pixCtrl; };
   std::string const& getRodName() const { return m_rodName; };
   std::string const& getName()    const { return m_name; };
   PixModule* module(int im);
   moduleIterator modBegin() { return m_modules.begin(); };
   moduleIterator modEnd()   { return m_modules.end(); };
   ModuleConnectivity* modConn(int im);
   modConnIterator modConnBegin() { return m_modConn.begin(); };
   modConnIterator modConnEnd()   { return m_modConn.end(); };
   PixBoc* getPixBoc() { return m_pixBoc; };
   Config &config() { return *m_config; };
   RodBocConnectivity* rodConnectivity() { return m_rodConn; };
   PixConnectivity* pixConnectivity() { return m_conn; };
   PixDbServerInterface *dbServerInterface() {return m_dbserverI;};
   //PixHistoServerInterface *histoServerInterface() {return m_hInt;};
   //PixDcs* &pixDcs() { return m_dcs; };
   PixISManager* isManager() { return m_is;};
   bool& preAmpsOn() { return m_preAmpsOn; };
   CtrlType getCtrlType() { return m_ctrlType; };
   FEflavour getFeType() const { return m_feType; };
   int getFeFlav();
   bool getBocAvailable() const { return m_bocAvailable; };
   bool isSecondLinkAvailable ( );
   int getAltRxChannel(int iMod);

   // Config methods (old style)
   void readConfig(DbRecord *dbr);          //! read a configuration from the DB
   void writeConfig(DbRecord *dbr);         //! write the current configuration to the DB
   void downloadConfig();                   //! write the current configuration into the PixController
   // Config methods (new style)
   void loadConfig(DbRecord *dbr);                                //! read a configuration from the DB
   void saveConfig(DbRecord *dbr);                                //! write the current configuration to the DB
   void loadConfig(PixConnectivity *conn, unsigned int rev = 0);  //! read the current configuration from the DB
   void saveConfig(PixConnectivity *conn, unsigned int rev = 0xffffffff);                        //! write the current configuration to the DB
   void writeConfig();                                            //! load the current configuration into the actual phisical modules

   //// (VD)
   bool writeConfigAll(PixDbServerInterface *DbServer, std::string domainName, std::string tag, std::string ptag="PixModuleGroup_Tmp", unsigned int revision=0xffffffff);
   bool writeConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, std::string ptag="PixModuleGroup_Tmp", unsigned int revision=0xffffffff);
   bool readConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, unsigned int revision=0xffffffff);
  
   void setTemp();

   void saveChangedConfig(std::string tag); //! save config to Dbserver if it was modified
   void reloadConfig();              //! reload config from DbServer if it was modified, changes are lost!

   void storeConfig(std::string cfgName);   //! give the current config a name  

   void initConfig();                       //! load config from the DB
   void buildConnectivity();                //| build a local connectivity structure
   
   //Scan Methods
   void initScan(PixScan *scn);                      //! prepares the module group for the scan
   void scanLoopStart(int nloop, PixScan *scn);      //! prepares the module group for the beginning of a particular loop
   void scanLoopEnd(int nloop, PixScan *scn, PixHistoServerInterface *hInt = 0, std::string folder = "");        //! will perform the end-of-loop actions
   void prepareStep(int nloop, PixScan *scn, PixHistoServerInterface *hInt = 0, std::string folder = "");        //! sets the variables values
   void scanExecute(PixScan *scn);                   //! executes a step of the scan
   void scanTerminate(PixScan *scn);                 //! terminates a step of the scan
   void terminateScan(PixScan *scn);                 //! end-of-scan cleanup
   void setupMasks(int nloop, PixScan *scn);         //! prepares the masks
   void setupScanVariable(int nloop, PixScan *scn);  //! prepares the system for the execution of a single step
   std::string steerScan(std::string name, PixScan* scn);    //! Execute a scan 
   void selectFEEnable();                            //! Store actual configs in PixModule memory to be used for enable/disable FEs
   void selectFE(int iFE);                           //! Internal method: select a single FE enabled for a FEbyFE scan
   void disableFESelected();                         //! Restore a previous saved config and delete it from local PixModule object


   // TB: **New Scan Methods**

   void setm_useScanInSwap(bool mc);

   void setm_useScanOutSwap(bool mc);

   void setm_useScanModAltLink(bool mc);
  
   bool getm_useScanModAltLink( ){return  m_useScanModAltLinks;}
 
   void setm_scanModAltLink(unsigned int modalt);

   void storeConfig(int mod, std::string scanConfig);
   
   void moduleConfigIdentifier();  

   void sendModuleConfig();

   void sendModuleConfigWithPreampOff();

   int getmoduleSize();

   void setmoduleConfigActive(int mod, bool mc);
   
   void setmoduleTriggerActive(int mod, bool mc);

   void setmoduleStrobeActive(int mod, bool mc);

   void setmoduleReadoutActive(int mod, bool mc);
   
    int getmoduleId(int mod);

   void setm_useScanModAltLinks(bool mc);

   void setm_scanModAltLinks(unsigned int modalt);
   
   void setm_execToTerminate(bool mc);
   
   int getm_execToTerminate();
   
   bool getpixCtrl_moduleActive(int mod);
   
   void rodStopScan();
   
   bool getmoduleReadoutActive(int mod);
   
   void restoreModuleConfig(PixScan* scn, std::string scanConfig);

   std::string getRodStat();
     
   ///////////////////////////
   // steer scan helper methods
   ///////////////////////////
   bool setModuleMask(PixScan* scn);
   unsigned int* createModuleMask();
   void prepareLoop0(PixScan* scn,PixHistoServerInterface *hInt = 0, std::string folder = "");
   bool setPixScanMask(unsigned int* modMask, PixScan* scn);
   bool checkRodController();
   void prepareScan(PixScan* scn);
   void prepareLoop2(PixScan* scn, PixHistoServerInterface *hInt = 0, std::string folder = "");
   void prepareLoop1(PixScan* scn, PixHistoServerInterface *hInt = 0, std::string folder = "");
   bool normalScan(PixScan* scn);
   bool checkScanningMode();
   bool doneScanning(bool wait, PixScan* scn, int& mstage, int& loop0, int& loop1, int& loop2, int& in_exeBlock);
   bool runOk(PixScan* scn, int mstage, int loop0, int loop1, int loop2);
   void endLoop0(PixScan* scn, PixHistoServerInterface *hInt = 0, std::string folder = "");
   void endLoop1(PixScan* scn, PixHistoServerInterface *hInt = 0, std::string folder = "");
   void endLoop2(PixScan* scn, PixHistoServerInterface *hInt = 0, std::string folder = "");
   void endScan(PixScan* scn, PixHistoServerInterface *hInt = 0, std::string folder = "");
   void writeAndDeleteHisto(PixScan* scn, PixHistoServerInterface* hInt, std::string folder, 
			    unsigned int mod, int type, int idx2, int idx1, int idx0);

   ///////////////////////////
   // steer scan publishing methods
   ///////////////////////////
   void publishScanInit(std::string scanName, int id = -1);
   void publishScanDone(int id = -1);
   void publishScanFailed(int id = -1);
   void publishScanAbort(int id = -1);
   void publishScanRunning(int id = -1);
   void publishScanEndStep(int maskStage, int bin0, int bin1, int bin2, int runStatus, PixScan* scn);
   //void publishMainFolder(std::string mainFolder);
   std::string getStatusString(int id = -1);
   std::string getActionString();
   void setDataTakingIS(PixISManager* dtIs){m_dtIs = dtIs;}

   //Misc methods
   void setActive(bool active);                     //! Enable/Disable to module group
   bool getActive();                                //! Get global activation status
   void setModuleActive(int modID, bool active);    //! Enable/Disable a module
   bool getModuleActive(int modID);                 //! Get module activation status
   void setMultiName(std::string mn);               //! Set the name of the ActionMulti to which the MG is attached
   std::string getMultiname();                      //! returns the name of the ActionMulti
   std::string getISRoot();                         //! get scan number and also Crate and Rod name from DB to form variable name for IS  
   void publishROD();                               //! Publish ROD serials into IS, to be used by STRodCrateRemote class
   void changedCfg() {m_cfgChanged = true;};                           

   int getVCALfromCharge(float charge, PixFe* fe, int capType); //! Calculate corresponding VCAL
   //                                                          capType: 0=Clow or CAP1, 1=Chigh or CAP0 , 2=Chigh or CAP0+CAP1
   float getChargefromVCAL(float vcal, PixFe* fe, int capType); //! Calculate corresponding VCAL and set the FE config.
   //                                                          capType: 0=Clow or CAP1, 1=Chigh or CAP0 , 2=Chigh or CAP0+CAP1
   float getChargefromVCAL(float vcal, float vcal_a, float vcal_b, float vcal_c, float vcal_d, float cinj); 
   float getChargefromVCAL(float vcal, float vcal_c, float vcal_d, float cinj);
   int getCapType( PixScan *scn);
   bool isPixelInMaskSteps(int const col, int const row, PixScan *scn);

   // Loop action specific methods
   void prepareTDACFastTuning(int nloop, PixScan *scn, PixHistoServerInterface *hInt = 0, std::string folder = "");
   void prepareFDACFastTuning(int nloop, PixScan *scn, PixHistoServerInterface *hInt = 0, std::string folder = "");
   void prepareThrFastScan(int nloop, PixScan *scn, PixHistoServerInterface *hInt = 0, std::string folder = "");
   void prepareTDACTuning(int nloop, PixScan *scn);
   void stepFDACTuning(int nloop, PixScan *scn, PixHistoServerInterface *hInt = 0, std::string folder = "");
   void prepareTDACTuningBisection(int nloop, PixScan *scn);
   void prepareTDACTuningIterated(int nloop, PixScan *scn);
   void prepareGDACTuning(int nloop, PixScan *scn);
   void prepareGDACFastTuning(int nloop, PixScan *scn);
   void prepareGDACTuningIterated(int nloop, PixScan *scn);
   void prepareFDACTuning(int nloop, PixScan *scn);
   void prepareIFTuning(int nloop, PixScan *scn);
   void prepareIFFastTuning(int nloop, PixScan *scn, PixHistoServerInterface *hInt = 0, std::string folder = "");
   void prepareT0Set(int nloop, PixScan *scn);
   void prepareIncrTdac(int nloop, PixScan *scn);
   void setCharge(int nloop, PixScan *scn);

   void endTDACFastTuning(int nloop, PixScan *scn,PixHistoServerInterface *hInt = 0, std::string folder = "");
   void endFDACFastTuning(int nloop, PixScan *scn,PixHistoServerInterface *hInt = 0, std::string folder = "");
   void endThrFastScan(int nloop, PixScan *scn,PixHistoServerInterface *hInt = 0, std::string folder = "");
   void endTDACTuning(int nloop, PixScan *scn);
   void endTDACTuningBisection(int nloop, PixScan *scn);
   void endTDACTuningIterated(int nloop, PixScan *scn);
   void endGDACTuning(int nloop, PixScan *scn);
   void endGDACTuningInterpolate(int nloop, PixScan *scn, bool iterated=false);
   void endFDACTuning(int nloop, PixScan *scn);
   void endIFTuning(int nloop, PixScan *scn);
   void endIFFastTuning(int nloop, PixScan *scn, PixHistoServerInterface *hInt = 0, std::string folder = "");
   void endT0Set(int nloop, PixScan *scn, PixHistoServerInterface *hInt = 0, std::string folder = "");
   void endIncrTdac(int nloop, PixScan *scn);
   void mccDelFit(int nloop, PixScan *scn, PixHistoServerInterface *hInt = 0, std::string folder = "");
//GDAC FAST BY ANTONELLO
   void endFei4GDACFastFineTuning(int nloop, PixScan *scn, PixHistoServerInterface *hInt, std::string folder); 
   void prepareFei4GDACFineTuning(int nloop, PixScan *scn, PixHistoServerInterface *hInt, std::string folder);
   void prepareFei4GDACCoarseTuning(int nloop, PixScan *scn);
   bool isFeActive(PixFe *fe);
   void prepareDiscBiasTuning(int nloop, PixScan *scn);
   void endDiscBiasTuning(int nloop, PixScan *scn);
//   void setTDACtoFei4Max(PixFe* fe);


   /*
    * Calculates the integral of the errors over all delay values and
    * decides whether to increase or decrease the threshold (nested
    * intervals).
    * @param scn The current scan
    * @param modID The module ID 
    * @param il opto link
    * @param oldthresh The old threshold 
    * @return The next threshold value
    */
   unsigned int nextBocThrTune(PixScan* scn, unsigned int modID, int il, unsigned int oldthresh);

   /*
    * Method called at the end of the BOC_TUNING. The values found in
    * the scan are printed to the log file.
    * @param nloop The loop identifier. 
    * @param scn The current scan. 
    */ 
   void endBOCTuning(int nloop, PixScan *scn); 

   /*
    * The nested interval search for an optimal viset is performed
    * here.
    * @param scn The current scan
    * @param pluginID The plugin ID 
    * @param oldviset The old viset value 
    * @return The new viset value 
    */ 
   float nextOptoTune(PixScan* scn, int pluginID, float oldviset);

   /*
    * Method called at the end of the OPTO_TUNING. Prints the viset
    * values into the log file. 
    * @param nloop The loop identifier. 
    * @param scn The scan. 
    */ 
   void endOPTOTuning(int nloop, PixScan *scn);    
   void inLinkScan(PixScan *scn, Histo* &vh);

   // update module settings (links etc.) from connectivity
   void updateFromConn();
   void updateModParFromConn();
   void updateModuleLinks();

   void updateBOCLink();
   void setReadoutSpeed(MccBandwidth speed, bool forceResetViset = false);
   MccBandwidth getReadoutSpeed() { return m_curReadoutSpeed; };
   unsigned int getActiveModuleMask();
   std::string getModIdName(int modId); 
   unsigned int getModMaskPosition(std::string modName);
   std::string getSwapInLinks() { return m_swapInLinks; };
   void setSwapInLinks(std::string swin) { m_swapInLinks = swin; };
   std::string getSwapOutLinks() { return m_swapOutLinks; };
   void setSwapOutLinks(std::string swout) { m_swapOutLinks = swout; };
   int inLink(int modId);
   int outLink1A(int modId);
   int outLink1B(int modId);
   int outLink2A(int modId);
   int outLink2B(int modId);
   int bocTxLink(int modId);
   int bocRxLink1(int modId);
   int bocRxLink2(int modId);

   // Stop-less recovery support
   void setRodBusy(unsigned int fr);
   void setModLinkStat(std::string mod, std::string stat);
   void autoDisable( );
   std::string getDisReason(std::string name);
   bool enableModule(std::string modName, std::string reason);
   bool readyEnableRod( );
   bool enableRod( );
   bool enableSlink(const std::string &SlinkName);
   bool enableSlink( );
   bool disableModule(std::string modName, std::string reason);
   bool disableRod( );
   std::vector<std::string> getModules(std::string modName);
   bool sendDisableMessage(const std::string &itemName, std::string itemType = "MOD");
   void reApplyModuleDisable();
   void updateDisableRodRecover( );
   bool rodDisabled();
   void setRodDisableReason(std::string reason);
   void resetDisableCounters(bool full);
   void disconnect();
   void warmReconfig(std::string mode);

   static int translateMaskSteps(int mask_step_mode);

  protected:   
   // Constructor helpers
   void initDb();
   void initDbServer();

   PixDbInterface *m_dbn;
   PixDbServerInterface *m_dbserverI;
   //   PixHistoServerInterface *m_hInt;
   
   std::string m_name;
   std::string m_rodName;
   PixController *m_pixCtrl;
   std::vector<PixModule*> m_modules;
   std::vector<ModuleConnectivity*> m_modConn;
   int m_modIdx[32];
   DbRecord* m_dbRecord;
   PixBoc *m_pixBoc;
   ModGrpConfig *m_config;
   bool m_execToTerminate;
   int m_triggerDelay;
   PixConnectivity *m_conn;
   PixConnectivity *m_connGlob;
   RodBocConnectivity *m_rodConn;
   bool m_localConnectivity;
   PixDcs *m_dcs;
   int m_currentGroupExec;
   MccBandwidth m_readoutSpeed;
   MccBandwidth m_curReadoutSpeed;
   bool m_hwInit;
   bool m_cachedCopy;
   std::string m_swapInLinks;
   std::string m_swapOutLinks;
   bool m_useScanInSwap;
   bool m_useScanOutSwap;
   bool m_useScanModAltLinks;
   std::string m_scanSwapInLinks;
   std::string m_scanSwapOutLinks;
   unsigned int m_scanModAltLinks;
   bool m_active;
   std::map<std::string, std::string> m_modDisabledDCS;
   std::string m_multiName;
   bool m_cfgChanged;
   std::map<std::string, unsigned int> m_modTimeoutCount;
   std::map<std::string, unsigned int> m_modOverflowCount;
   std::map<std::string, unsigned int> m_modHeaderTrailerCount;
   std::map<std::string, unsigned int> m_modBusyCount;
   std::map<std::string, unsigned int> m_modErrCount;
   std::map<std::string, unsigned int> m_modDisCount;
   std::map<std::string, unsigned int> m_modResetCount;
   unsigned int m_rodBusyCount;
   unsigned int m_rodBusyCount1;
   unsigned int m_rodBusyCount2;
   unsigned int m_rodBusyCount3;
   unsigned int m_globTimeoutCount;
   std::string m_rodDisableReason;
   bool m_rodDisabled;
   std::map<std::string, std::string> m_autoDisabledModules;
   std::map<std::string, std::string> m_modulesToDisable;
   bool m_configSent;
   bool m_interactiveInterlock;
   bool m_visetUpdated;
   bool m_preAmpsOn;
   bool m_createIf;
   CtrlType m_ctrlType;
   FEflavour m_feType;
   int m_feFlav;
   bool m_bocAvailable;
   bool m_secondLinkAvailable;//if second link avaliable 

   // SW interfaces
   PixISManager* m_is;          //IS Manager for PixelInfr and RunParams server
   PixISManager* m_dtIs;        //IS Manager for Data taking
   PixISManager* m_isPixelDDC;  //IS Manager for PixelInfr and DDC server

   /*
    * defines the direction (and magnitude) of the next step in viset
    * for each optoboard.
    * size is the number of plugins. 
    */
   std::vector <float> m_opto_direction; 

  private:
   void sendDDCcommand(std::string pp0Name, float value, PixDcsDefs::DDCCommands command);
   void readDDC(std::string pp0Name, PixDcsDefs::DataPointType variable, float& value);
   void sendFSMcommand(std::string objList, PixDcsDefs::FSMCommands command);

   std::map<int,Histo*> hTotHistArr;
   std::map<int,Histo*> hFdacHistArr;

	 bool m_diffUp[32][16];
	 int m_lastOccAvg[32][16];

	 int m_vcalMin, m_vcalMax;

	 bool PMG_DEBUG;
	 std::array<std::array<bool,16>,32 > m_GDAC_tuneConverge;
	 std::array<std::vector<std::vector <bool>>,32> m_tdac_TuneConverge;
  };
}

#endif
