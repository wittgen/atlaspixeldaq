/** @file PixFitNet.cxx
 *
 *  Created on: Nov 28, 2013
 *      Author: mkretz
 */

#include <iostream>
#include <fstream>	// for dumping raw network data
#include <memory>
#include <functional>

/* Networking via POSIX sockets */
#include <sys/socket.h>
#include <sys/poll.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <unistd.h>

#include <thread>
#include <is/info.h>
#include <is/infoT.h>
#include <is/infodictionary.h>
#include <is/inforeceiver.h>

#include "iblSlaveNetCmds.h"
#include "rodHisto.hxx"

#include "PixFitNet.h"
#include "PixFitInstanceConfig.h"
#include "PixFitManager.h"

#define TOT(v, s, b) ((v >> s) & ((1 << b) - 1))

#define SHORT_TOT_MISSING_OCC(v) \
                             TOT(v, ONEWORD_MISSING_TRIGGERS_RESULT_SHIFT, \
                                    MISSING_TRIGGERS_RESULT_BITS)
#define SHORT_TOT_SUM(v)     TOT(v, ONEWORD_TOT_RESULT_SHIFT, m_scanConfig->tot_result_bits)
#define SHORT_TOT_SUM_SQR(v) TOT(v, ONEWORD_TOTSQR_RESULT_SHIFT, TOTSQR_RESULT_BITS)

#define LONG_TOT_OCC(v)      TOT(v, TWOWORD_OCC_RESULT_SHIFT, OCC_RESULT_BITS)
#define LONG_TOT_SUM(v)      TOT(v, m_scanConfig->twoword_tot_result_shift, m_scanConfig->tot_result_bits)
#define LONG_TOT_SUM_SQR(v)  TOT(v, m_scanConfig->twoword_totsqr_result_shift, m_scanConfig->totsqr_result_bits)

// #define DEBUG_ND

using namespace PixLib;

PixFitNet::PixFitNet(PixFitWorkQueue<RawHisto> *queue,
		PixFitNetConfiguration const *netConfig,
		PixFitInstanceConfig *instanceConfig ) :
		m_scanConfigQueue("ScanConfigQueue") {
	this->m_queue = queue;
	this->m_configuration = netConfig;
	this->m_rodSock = 0;
	this->m_rxBuf = nullptr;
	this->m_currentBin = 0;
	this->m_threadName = "network";
	this->m_instanceConfig = instanceConfig;
	this->m_resetFlag = 0;
	this->m_scanConfigQueue.setBlackList(std::bind(&BlackList::investigateWorkObject, &m_instanceConfig->blacklist, std::placeholders::_1));
	this->m_histoUnitString = netConfig->getHistogrammer()->makeHistoString();
        m_stopFlag = false;
}

PixFitNet::~PixFitNet() {
	/* TODO Gracefully close open connections */
	if(m_localSock>0)close(m_localSock);
	if(m_rodSock>0)close(m_rodSock);
	free(m_rxBuf);
}

/* @todo: error handling for socket -> exit thread? */
void PixFitNet::loop() {
	std::ostringstream buf; // for buffering lines of output
	ERS_LOG(m_histoUnitString << ": Opening socket in thread " << getThreadId() << ".");

	sockaddr_in my_addr{};
	sockaddr_in their_addr{}; // connector's address information
	socklen_t sin_size;
	int rc;
	m_localSock = -1;
        bool exiting = false;
	int netTry = 0;

	/* Create socket. */
	m_localSock = socket(AF_INET, SOCK_STREAM, 0);
	if (m_localSock<0) {
		PIX_ERROR(m_histoUnitString << ": Opening socket failed.");
		return;
	}

	/* Allow port number to be reused. */
	int yes = 1;
	if (setsockopt(m_localSock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
		PIX_ERROR(m_histoUnitString << ": Setting socket option failed.");
		close(m_localSock);
		return;
	}

	/* Configure local part. */
	my_addr.sin_family = AF_INET;
	my_addr.sin_port = m_configuration->getLocalPort();
	my_addr.sin_addr.s_addr = m_configuration->getLocalIpAddress().s_addr;
	memset(my_addr.sin_zero, '\0', sizeof(my_addr.sin_zero));

	rc = bind(m_localSock, (struct sockaddr *) &my_addr, sizeof(my_addr));
	if (rc <0) {
		PIX_ERROR(m_histoUnitString << ": Bind failed "<<strerror(errno));
		close(m_localSock);
		return;
	}

	rc = listen(m_localSock, 10); // @todo backlog
	if (rc<0) {
		PIX_ERROR(m_histoUnitString << ": Listen failed "<<strerror(errno));
		close(m_localSock);
		return;
	}

	/* Get buffer for histogram data. */
	if (0 == (m_rxBuf = (unsigned char*) malloc(s_bufSize))) {
		PIX_ERROR(m_histoUnitString << ": Buffer allocation failed ");
		close(m_localSock);
		return;
	}

	struct timeval timeout;      
        timeout.tv_sec = 1;
        timeout.tv_usec = 0;

        if (setsockopt (m_localSock, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout,sizeof(timeout)) < 0){
          PIX_ERROR(m_histoUnitString <<" setsockopt failed "<<strerror(errno));
          return;
        }

        if (setsockopt (m_localSock, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout,sizeof(timeout)) < 0){
          PIX_ERROR(m_histoUnitString <<" setsockopt failed "<<strerror(errno));
          return;
        }
	/* Main loop.
	 * @todo Communication to/from PixFitManager.
	 * @todo Signal readiness (to PixFitManager). */
	while (!PixFitManager::stop_all) {
		ERS_DEBUG(1, m_histoUnitString << ": Begin of loop, localsock " << m_localSock);

		sin_size = sizeof their_addr;
		/* Blocking here. */
		m_rodSock = accept(m_localSock, (struct sockaddr *) &their_addr, &sin_size);
		if (m_rodSock<0) {
			  if(errno == EAGAIN || errno ==  EWOULDBLOCK || errno ==  EINPROGRESS){
			    PIX_DEBUG(1,m_histoUnitString << "Socket "<<m_localSock<<" timeout in accept with signal "<<strerror(errno)<<" keep trying");
			    continue;
			  }
			PIX_ERROR(m_histoUnitString << ": Accept failed "<<strerror(errno));
			close(m_localSock);
			break;
		}
		ERS_LOG(m_histoUnitString << ": FitServer connection slave at ip " <<  std::string(inet_ntoa(their_addr.sin_addr)) << " successful!");

		/* Set socket to be non-blocking. */
		fcntl(m_rodSock, F_SETFL, O_NONBLOCK);

		/* Discard reset requests from before a connection has actually been established. */
		m_resetMutex.lock();
		m_resetFlag = 0;
		m_resetMutex.unlock();

		/* Read and process commands. */
		bool notyetread = true;
		while (!PixFitManager::stop_all) {
			/* Get scan config from queue if no scan is ongoing. Blocks if no items in queue. */
			if (m_scanConfig == 0) {
				m_dumpFile.close(); /* Close potentially open stream, not throwing by default. */
#ifdef DEBUG_ND
				ERS_LOG(m_histoUnitString << ": Going to getWork");
#endif
				m_scanConfig = m_scanConfigQueue.getWork();
#ifdef DEBUG_ND
				ERS_LOG(m_histoUnitString << ": Beginning Scan with scanId(maskId): " << m_scanConfig->scanId
							  << "(" << m_scanConfig->maskId << ").");
#endif
                                if(m_scanConfigQueue.stopping())
                                {
                                  exiting = true;
                                  break;
                                }
				/* Initialize RawHisto object. */
				m_rawHisto = std::make_unique<RawHisto>(m_scanConfig);
				if (!(m_rawHisto->allocateMemory())) {
					ERS_DEBUG(0, m_histoUnitString <<
							": Allocated " << m_rawHisto->getSize() << " bytes for histogram.");
					m_currentBin = 0;
				}
				else {
					return;
				}

				/* Optionally open file for dumping network data. */
				if (m_instanceConfig->dumpNetwork) {
					std::string crate = std::to_string(m_configuration->getHistogrammer()->crate);
					std::string rod = std::to_string(m_configuration->getHistogrammer()->rod);
					std::string slave = std::to_string(m_configuration->getHistogrammer()->slave);
					std::string histo = std::to_string(m_configuration->getHistogrammer()->histo);
					std::string scanId = std::to_string(m_scanConfig->scanId);
					std::string maskId = std::to_string(m_scanConfig->maskId);
					std::string fileName = "FitServer-" + scanId + "-" + "m" + maskId + "-" + crate + "_"
							+ rod + "_" + slave + "_" + histo + ".dump";
					m_dumpFile.open(fileName.c_str(), std::ios::out | std::ios::binary | std::ios::ate);
					/* @todo: fail check */
				}
			}

#ifdef DEBUG_ND
			ERS_LOG(m_histoUnitString << ": If not already done so, about to enter network read routine . . .");
#endif
			if(notyetread)rc = readCommand();
#ifdef DEBUG_ND
			ERS_LOG(m_histoUnitString << ": Last network read had rc = " << rc);
#endif
			if (1 != rc) {
                                if(rc == -3) exiting = true;
				break;
			}

			if(m_scanConfig == 0){ //  Needed in order to recover from some ugly scan failures:
			  notyetread = false;  //         Instead of wating for next scan in getWork() routine,
			  continue;            //         these cases are stuck polling the network in readCommand() above.
			}                      //         Reset of m_scanConfig in putScanConfig routine signals that histo-data
			notyetread = true;     //         for a new scan has arrived whose config must be retreived by getwork().

			/* Process complete commands. */
			rc = procRequest();
			netTry = 0; //  If we got here and rc >= 0, network tranmission worked, so reset "try" counter;

			/* Ignore blacklisted scans still in the pipeline (abort request seems not to clear them out well) */
			if (3 == rc) {
				std::string crateNumber = std::to_string(m_configuration->getHistogrammer()->crate);
				std::string rodNumber = std::to_string(m_configuration->getHistogrammer()->rod);
				IPCPartition partition(m_instanceConfig->getPartitionName());
				ISInfoDictionary dict(partition);
				std::string serverName = m_instanceConfig->getServerName();
				std::string crateLetter = m_configuration->getHistogrammer()->crateletter;
				std::string rodName = "ROD_" + crateLetter + crateNumber + "_S" + rodNumber;
				ISInfoBool isfinish;
				std::string ISLookupString = serverName + "." + rodName + "_FinishScan";
				dict.getValue(ISLookupString, isfinish);
#ifdef DEBUG_ND
				ERS_LOG("Got isfinish: " << (int)isfinish << " from IS lookup string '" << ISLookupString << "'");
#endif
				if(!isfinish) setISfinishScan(dict, serverName, rodName); //  This should not be necessary, but sometimes isfinish is set back to 0 ???
			}

			/* Publish RawHisto to queue. */
			else if (2 == rc) {
				m_queue->addWork(std::move(m_rawHisto));
				//m_rawHisto.reset();
				m_scanConfig.reset();
			}

			/* Due to error, scan needs to be canceled, which means
			   blacklist scanId, clear (incoming) work queue and lastly
			   tell IS the Scan stopped (so RodPixController::getHisto does not wait unnecessarily for non-existent histos) */
			else if (1 == rc) {
				/* Add scan ID to the global blacklist. */
				ISInfoInt scanId = m_scanConfig->scanId;
				std::string crateNumber = std::to_string(m_configuration->getHistogrammer()->crate);
				std::string rodNumber = std::to_string(m_configuration->getHistogrammer()->rod);
				ERS_LOG(m_histoUnitString << ": Cancelling scan with ID " << scanId << " for crate/ROD " << crateNumber << "/" << rodNumber);
				m_instanceConfig->blacklist.addScanId(scanId);

				clearScanConfigQueue();

				/* Tell IS the Scan stopped */
				IPCPartition partition(m_instanceConfig->getPartitionName());
				ISInfoDictionary dict(partition);

				std::string serverName = m_instanceConfig->getServerName();

				std::string crateLetter = m_configuration->getHistogrammer()->crateletter;
				std::string rodName = "ROD_" + crateLetter + crateNumber + "_S" + rodNumber;

				setISfinishScan(dict, serverName, rodName);
			}
			else if (0 == rc) {

			}
			/* Reset connection in case of error */
			else {
                                if(rc == -3) exiting = true;
				break;
			}
		}

		rc = shutdown(m_rodSock, SHUT_WR);
		  if (rc != 0) {
		    PIX_ERROR(m_histoUnitString << ": Error on shutdown socket "<<strerror(errno));
		  }
		rc = close(m_rodSock);
		if (rc != 0) {
		  PIX_ERROR(m_histoUnitString << ": Error on close socket "<<strerror(errno));
		}
		
              if(exiting) break;
              netTry++;
		if(netTry < 20) {
		  sleep(1);
		  PIX_WARNING(m_histoUnitString << ": Try # " << netTry << " to complete network transmission from slave.");
		} else {
		  m_scanConfig.reset();
		}
	}

	/** @todo Only reached when accept() fails. Free memory (when thread is terminated?) */
        /**  Nick's "stopwork" enhancement bring's you here too                                */
        close(m_localSock);
        m_localSock = -1;
        m_rodSock = -1;
	free(m_rxBuf);
        if(exiting) {
          ERS_LOG(m_histoUnitString << ": Exiting on Network Reset Request");
        }
        else {
	  ERS_LOG(m_histoUnitString << ": Done!");
        }
	return;
}

int PixFitNet::readCommand() {
	unsigned int rxLen = 0;
	int rc = 0;
	int poll_rc;
	pollfd ufds[1];

	/* Fill struct for poll() call. */
	ufds[0].fd = m_rodSock;
	ufds[0].events = POLLIN;

	while (rxLen < sizeof(RodSlvTcpCmd)) {
#ifdef DEBUG_ND
		ERS_LOG(m_histoUnitString << ": Polling network");
#endif
		poll_rc = poll(ufds, 1, s_timeout);
#ifdef DEBUG_ND
		ERS_LOG(m_histoUnitString << ": Done polling network with rc = " << poll_rc);
#endif
		/* Timeout. */
		if (poll_rc == 0) {
			/* Check if reset was requested. */
			if (resetCheck()) return m_stopFlag?-3:-2;
		}
		/* Error on poll. */
		else if (poll_rc == -1) {
			return -2;
		}
		/* Activity on socket. */
		else {
			rc = recv(m_rodSock, m_rxBuf + rxLen, sizeof(RodSlvTcpCmd) - rxLen, 0);
			//printf("Received %d bytes\n",rc);
			if (-1 == rc) {
				ERS_LOG(m_histoUnitString << ": Socket error.");
				return rc;
			}
			else if (0 == rc) {
				ERS_LOG(m_histoUnitString << ": Socket closed from remote.");
				return rc;
			}
			rxLen += rc; // update len
		}
	}

	/* Optionally dump data to file. */
	if (m_dumpFile.is_open()) {
		m_dumpFile.write(reinterpret_cast<char *>(m_rxBuf), rxLen);
	}
	return 1;
}

void PixFitNet::putScanConfig(std::unique_ptr<PixFitScanConfig> scanConfig) {
  
  if(m_scanConfig != 0 && scanConfig->scanId != m_scanConfig->scanId){ //  Needed to recover from some ugly scan failures
    ERS_LOG("Clearing m_scanConfig left over from previous failed scan");
    m_scanConfig.reset();
    m_rawHisto.reset();
  }
  m_scanConfigQueue.addWork(std::move(scanConfig));
}

int PixFitNet::procRequest() {

	RodSlvTcpCmd cmdBuf; // temp buffer is data size too small
	RodSlvTcpCmd cmd;
	int i, j;
	unsigned int rxLen = 0;
	int rc;
	void *buf = (void *) m_rxBuf;

	int poll_rc;
	pollfd ufds[1];

	/* Fill struct for poll() call. */
	ufds[0].fd = m_rodSock;
	ufds[0].events = POLLIN;

	const int numberBins = m_rawHisto->getScanConfigRaw()->getNumOfBins();
	const int numberPixels = m_rawHisto->getScanConfigRaw()->getNumOfPixels();
	const int multiplicity = m_rawHisto->getScanConfigRaw()->getBytesPerPixel();

	memcpy((void*) &cmdBuf, buf, sizeof(RodSlvTcpCmd));

	/* We have a command. */
	if (SLVNET_MAGIC != ntohl(cmdBuf.magic)) {
		ERS_LOG(m_histoUnitString << ": Invalid magic word.");
		printf("Invalid magic word.  Expected 0x%08x, but received 0x%08x\r\n", SLVNET_MAGIC, ntohl(cmdBuf.magic));
		printf("                     Full RodSlvTcpCmd received: ");
		for (i = 0; i < (int)sizeof(RodSlvTcpCmd); i++) printf("%02x", ((char*) &cmdBuf)[i]);
		printf("\r\n");
		return 1;
	}
	else {
		cmdToHost(cmdBuf, cmd); // convert command struct to host order
	}


	/* Process commands. */
	switch (cmd.command) {

	/* SLVNET_HIST_DATA_CMD: Receive pixel data for certain bin. */
	case SLVNET_HIST_DATA_CMD: {
	  ERS_LOG(m_histoUnitString << ": Histogram data for bin = "
			  << cmd.bins << " (" << m_currentBin << ")");

		if (0 != cmd.payloadSize) {
			ERS_DEBUG(0, m_histoUnitString
					<< ": Receiving " << cmd.payloadSize << " bytes of histo data from ROD.");
			while (rxLen < cmd.payloadSize * sizeof(char)) {
				poll_rc = poll(ufds, 1, s_timeout);
					/* Timeout. */
					if (poll_rc == 0) {
						if (resetCheck()) return m_stopFlag?-3:-2;
					}
					/* Error on poll. */
					else if (poll_rc == -1) {
						return -2;
					}
					/* Activity on socket. */
					else {
						rc = recv(m_rodSock, m_histoBuf + rxLen, cmd.payloadSize * sizeof(char) - rxLen, 0);
						if (-1 == rc) {
							ERS_LOG(m_histoUnitString << ": Socket error.");
							return rc;
						}
						if (0 == rc) {
							ERS_LOG(m_histoUnitString << ": Socket closed from remote.");
							return rc;
						}
						rxLen += rc;
					}
			}

#ifdef DEBUG_ND
			ERS_LOG(m_histoUnitString << ": Checking consistency of scanId:  PixLib says it is " << m_scanConfig->scanId << ", Slave says it is " << cmd.scanId);
#endif
			if(m_instanceConfig->blacklist.isScanIdListed(cmd.scanId) ||
			   m_instanceConfig->blacklist.isScanIdListed(m_scanConfig->scanId)){
			     ERS_LOG(m_histoUnitString << ": Skipping blacklisted scan with ID number " << cmd.scanId);
			     return 3;
			}

			/* Optionally dump data to file. */
			if (m_dumpFile.is_open()) {
				m_dumpFile.write(m_histoBuf, rxLen);
			}

			uint32_t* histoBufint = (uint32_t*) m_histoBuf;
			ERS_LOG(m_histoUnitString <<
					": Histogram data received for bin " << m_currentBin << ": " <<
					rxLen / sizeof(char) << " bytes stored at 0x" << std::hex << histoBufint);

			std::string info = "PixFitNet";
                        const int expectedpayloadsize = 4 + 4*((numberPixels - 1)/4); //  Will always get a multiple of 4 bytes
			if ((static_cast<int>(cmd.payloadSize) / multiplicity) != expectedpayloadsize) {
			  std::string mess = "Mismatch in number of pixels. Expected " +
					  std::to_string(expectedpayloadsize) +
					  " but got " + std::to_string(cmd.payloadSize / multiplicity);
			  ers::error(PixLib::pix::daq (ERS_HERE, info, mess));
			  return 1;
			}

			if(m_scanConfig->scanId != static_cast<int>(cmd.scanId) && !m_instanceConfig->usingSlaveEmu()){
			  std::string mess = "Mismatch in scanId. Slave says " +
					  std::to_string(cmd.scanId) + ", PixLib says " +
					  std::to_string(m_scanConfig->scanId);
			  ers::error(PixLib::pix::daq (ERS_HERE, info, mess));
			  return 1;
			}
			if(m_scanConfig->maskId != static_cast<int>(cmd.maskId) && !m_instanceConfig->usingSlaveEmu()){
			  std::string mess = "Mismatch in maskId. Slave says " + std::to_string(cmd.maskId) +
			                                      ", PixLib says " + std::to_string(m_scanConfig->maskId);
			ers::error(PixLib::pix::daq (ERS_HERE, info, mess));
			  return 1;
			}
			/* Get entries and fill RawHisto. */
			RawHisto::histoWord_type *pHisto = m_rawHisto->getRawData();

			/* Memory layout depends on histogrammer readout mode and scan configuration. */
			PixFitScanConfig::scanType scanType = m_scanConfig->findScanType();
			PixFitScanConfig::readoutMode readoutMode =  m_scanConfig->getReadoutMode();

			//ERS_DEBUG(1,m_histoUnitString << ": Scantype " << scanType << " with readout mode " << readoutMode);

			/* -----------------------------------
			 * Analog, digital or threshold scan
			 * ----------------------------------- */
			if (scanType == PixFitScanConfig::scanType::OCCUPANCY || scanType == PixFitScanConfig::scanType::SCURVE) {
			  if (readoutMode == PixFitScanConfig::readoutMode::OFFLINE_OCCUPANCY) {
			    for (j = 0; j < (expectedpayloadsize); j++) {
			      pHisto[numberBins * j + m_currentBin] = (RawHisto::histoWord_type) m_histoBuf[j];
			      //printf("value 0x%x: \t occ %d, \t at %d \n", pHisto[j], (pHisto[j] >> 0*8) & ((1 << 8) - 1), j);
			    }
			  }
			  else if (readoutMode == PixFitScanConfig::readoutMode::ONLINE_OCCUPANCY) {
			    for (j = 0; j < (expectedpayloadsize); j++) {
			      pHisto[numberBins * j + m_currentBin] = (RawHisto::histoWord_type) histoBufint[j];
			      //printf("value 0x%x: \t occ %d \t at %d \n", pHisto[j], pHisto[j], j);
			    }
			  }
			  else if (readoutMode == PixFitScanConfig::readoutMode::SHORT_TOT) {
			    for ( j = 0; j < (expectedpayloadsize); j++) {
			      pHisto[numberBins * j + m_currentBin] = SHORT_TOT_MISSING_OCC(histoBufint[j]);
			      //printf("value 0x%x: \t missing triggers %d \t at %d \n", histoBufint[j],
                              //                                                         SHORT_TOT_MISSING_OCC(histoBufint[j]), j);
			    }
			  }		   
			  else if (readoutMode == PixFitScanConfig::readoutMode::LONG_TOT) {
			    for ( j = 0; j < (expectedpayloadsize * 2); j++) {
			      if (j % 2 == 0) { // first partial data word
			        pHisto[numberBins * j/2 + m_currentBin] = LONG_TOT_OCC(histoBufint[j]);
			        //printf("value 0x%x: \t occ %d \t at %d \n", histoBufint[j],
                                //                                            LONG_TOT_OCC(histoBufint[j]), j);
			      }
			    }
			  }
			  /* End of readoutMode section. */
			}
			/* -----------------------------------
			 * TOT scans
			 * ----------------------------------- */
			else if (scanType == PixFitScanConfig::scanType::TOT) {

			  /* Other readout modes do not make sense for TOT based stuff. */
			  assert(readoutMode == PixFitScanConfig::readoutMode::SHORT_TOT ||
			  readoutMode == PixFitScanConfig::readoutMode::LONG_TOT);

			  /* We have 3 words for TOT: occ/missing occ, TOT, TOT² */
			  const int wordsPerPixel = m_rawHisto->getScanConfigRaw()->getWordsPerPixel();

			  if (readoutMode == PixFitScanConfig::readoutMode::SHORT_TOT) {
			    for (j = 0; j < (expectedpayloadsize); j++) {
			      pHisto[numberBins * wordsPerPixel * j + wordsPerPixel * m_currentBin + 0] =
			        SHORT_TOT_MISSING_OCC(histoBufint[j]); // fill missing triggers
			      pHisto[numberBins * wordsPerPixel * j + wordsPerPixel * m_currentBin + 1] =
			        SHORT_TOT_SUM(histoBufint[j]); // then fill ToT
			      pHisto[numberBins * wordsPerPixel * j + wordsPerPixel * m_currentBin + 2] =
			        SHORT_TOT_SUM_SQR(histoBufint[j]); // then fill ToT2

			      /*printf("value 0x%x: \t missing triggers %d, \t tot %d, \t tot² %d \t at %d \n",  histoBufint[j], 
			             SHORT_TOT_MISSING_OCC(histoBufint[j]),
			             SHORT_TOT_SUM(histoBufint[j]),
			             SHORT_TOT_SUM_SQR(histoBufint[j]), j);*/
			    }
			  }
			  else if (readoutMode == PixFitScanConfig::readoutMode::LONG_TOT) {
			    for (j = 0; j < (expectedpayloadsize * 2); j++) {
			      if (j % 2 == 0) { // first partial data word
			        pHisto[numberBins * wordsPerPixel * j/2 + wordsPerPixel * m_currentBin + 0] =
			          LONG_TOT_OCC(histoBufint[j]); // fill occ
			        if (m_scanConfig->geo->pixType() == PixGeometry::FEI4_CHIP) {
			            //printf("value 0x%x: \t occ %d \t at %d \n", histoBufint[j], LONG_TOT_OCC(histoBufint[j], j);
			        } else {
			          pHisto[numberBins * wordsPerPixel * j/2 + wordsPerPixel * m_currentBin + 1] =
			            LONG_TOT_SUM(histoBufint[j]); // then fill ToT
			          /*printf("value 0x%x: \t occ %d \t at %d \n", histoBufint[j],
			                 LONG_TOT_OCC(histoBufint[j],
			                 LONG_TOT_SUM(histoBufint[j], j);*/

			        }
			      }
			      if(j % 2 == 1) { // second partial word
			        if (m_scanConfig->geo->pixType() == PixGeometry::FEI4_CHIP) {
			          pHisto[numberBins * wordsPerPixel * (j-1)/2 + wordsPerPixel * m_currentBin + 1] =
			            LONG_TOT_SUM(histoBufint[j]); // then fill ToT
			          pHisto[numberBins * wordsPerPixel * (j-1)/2 + wordsPerPixel * m_currentBin + 2] =
			            LONG_TOT_SUM_SQR(histoBufint[j]); // then fill ToT2
			          /*printf("value 0x%x: \t tot %d, \t tot² %d \t at %d \n",  histoBufint[j],
			                 LONG_TOT_SUM(histoBufint[j]),
			                 LONG_TOT_SUM_SQR(histoBufint[j]), j);*/
			        } else {
			          pHisto[numberBins * wordsPerPixel * (j-1)/2 + wordsPerPixel * m_currentBin + 2] =
			          LONG_TOT_SUM_SQR(histoBufint[j]); // then fill ToT2
			          /*printf("value 0x%x: \t tot %d, \t tot² %d \t at %d \n",  histoBufint[j],
			                 LONG_TOT_SUM_SQR(histoBufint[j]), j);*/

			        }
			      }
			    }
			  }
			  /* End of readoutMode section. */
			}
			/* End of scanType section. */

			/* Handle intermediate histograms. */
			if (m_rawHisto->getScanConfig()->doIntermediateHistos()) {
				/* Duplicate PixFitScanConfig. */
				std::shared_ptr<const PixFitScanConfig> newScanConfig(new PixFitScanConfig(*m_scanConfig));

				/* Get access to PixFitScanConfig and manipulate the settings for the intermediate histo. */
				std::shared_ptr <PixFitScanConfig> ncScancfg (std::const_pointer_cast<PixFitScanConfig>(newScanConfig));
				ncScancfg->binNumber = m_currentBin;

				auto newScanType = newScanConfig->findScanType();
				/* THRESHOLD scans */
				if (newScanType == PixFitScanConfig::scanType::OCCUPANCY || newScanType == PixFitScanConfig::scanType::SCURVE) {
					ncScancfg->intermediate = PixFitScanConfig::intermediateType::INTERMEDIATE_OCCUPANCY;
				}
				/* TOT scans */
				else if (newScanType == PixFitScanConfig::scanType::TOT) {
					ncScancfg->intermediate = PixFitScanConfig::intermediateType::INTERMEDIATE_TOT;
				}

				/* Create new RawHisto for intermediate histo. */
				auto tmpRawHisto = std::make_unique<RawHisto>(ncScancfg);

				/* Allocate memory and get raw pointer. */
				tmpRawHisto->allocateMemory();
				RawHisto::histoWord_type *pTmpHisto = tmpRawHisto->getRawData();

				/* Fill RawHisto with relevant data. */
				int numOfPixels = ncScancfg->getNumOfPixels();
				if (newScanType == PixFitScanConfig::scanType::OCCUPANCY || newScanType == PixFitScanConfig::scanType::SCURVE) {
					for (int k = 0; k < numOfPixels; k++) {
						pTmpHisto[k] = pHisto[numberBins * k + m_currentBin];
					}
				}
				else if (newScanType == PixFitScanConfig::scanType::TOT) {
					const int wordsPerPixel = m_rawHisto->getScanConfigRaw()->getWordsPerPixel();
					for (int k = 0; k < numOfPixels; k++) {
						int target = k * wordsPerPixel;
						int source = numberBins * wordsPerPixel * k + wordsPerPixel * m_currentBin;
						pTmpHisto[target + 0] = pHisto[source + 0];
						pTmpHisto[target + 1] = pHisto[source + 1];
						pTmpHisto[target + 2] = pHisto[source + 2];
					}
				}

				ERS_DEBUG(0, tmpRawHisto->getScanConfigRaw()->histogrammer.makeHistoString()
					<< ": Created intermediate histogram for bin " << m_currentBin);

				/* Enqueue intermediate histo object. */
				m_queue->addWork(std::move(tmpRawHisto));
			}
			/* End of intermediate histo section. */
		}
		/* End of non-zero payloadSize section. */

		/* Notice the different numbering schemes (1 to NumOfBins here, from 0 to maxBins above). */
	  	std::string currentHistoUnit = m_rawHisto->getScanConfigRaw()->histogrammer.makeHistoString();
		ERS_LOG(currentHistoUnit << " is at bin " << m_currentBin + 1 << " of " << m_rawHisto->getScanConfigRaw()->getNumOfBins());
		if (m_currentBin + 1 != m_rawHisto->getScanConfigRaw()->getNumOfBins()) {
		  m_currentBin++;
		  return 0;
		}

		ERS_LOG(currentHistoUnit << ": Histogram termination");

                /* Optionally close dump file. */
                if (m_dumpFile.is_open()) {
                    m_dumpFile.write(m_histoBuf, rxLen);
		}

		return 2;
	} //  case SLVNET_HIST_DATA_CMD:  As of now, the only supported "command"!

	/* No other commands supported. */
	default:
	        ERS_LOG(m_histoUnitString << ": Invalid command: " << std::hex << cmd.command);
		return 0;
	} //  switch (cmd.command)
}

const PixFitNetConfiguration* PixFitNet::getConfig() const {
	return m_configuration;
}

void PixFitNet::cmdToHost(const RodSlvTcpCmd &cmd, RodSlvTcpCmd &hostCmd) {
	hostCmd.magic = ntohl(cmd.magic);
	hostCmd.command = ntohl(cmd.command);
	hostCmd.bins = ntohl(cmd.bins);
	hostCmd.payloadSize = ntohl(cmd.payloadSize);
	hostCmd.scanId = ntohl(cmd.scanId);
	hostCmd.maskId = ntohl(cmd.maskId);
}

bool PixLib::PixFitNet::resetCheck() {
	std::lock_guard<std::mutex> lock(m_resetMutex);

	/* Always abort for m_resetFlag == 1 or check if scanId is blacklisted for m_resetFlag == 2. */
	if ((1 == m_resetFlag) || (2 == m_resetFlag && m_instanceConfig->blacklist.isScanIdListed(m_scanConfig->getScanId())) ) {
		ERS_LOG(m_histoUnitString << ": Resetting network thread " << getThreadId());

		cleanup();
                if(m_stopFlag)m_resetMutex.unlock();
		return true;
	}
	/* Flag not set, do not abort. */
#ifdef DEBUG_ND
	ERS_LOG(m_histoUnitString << ": Asked to, but not reseting network thread, reset flag = " << m_resetFlag << ", ScanId = " << m_scanConfig->getScanId() << ".");
#endif
	return false;
}

void PixFitNet::stopNetwork()
{
  std::lock_guard<std::mutex> lock(m_resetMutex);
  m_stopFlag = true;
  resetNetwork();
  m_scanConfigQueue.stopWork();
}

void PixFitNet::resetNetwork(bool ifBlacklisted) {
	if(!m_stopFlag)std::lock_guard<std::mutex> lock(m_resetMutex);
	if (!ifBlacklisted) {
		m_resetFlag = 1;
	}
	else {
		m_resetFlag = 2;
	}
	ERS_DEBUG(0, m_histoUnitString << ": Setting network reset flag to " << m_resetFlag);
}

void PixFitNet::cleanup() {
	shutdown(m_rodSock, SHUT_WR);
	close(m_rodSock);

	m_rawHisto.reset();
	m_scanConfig.reset();
	m_resetFlag = 0;
	ERS_LOG(m_histoUnitString << ": Network thread reset.");
}

void PixFitNet::clearScanConfigQueue() {
	m_scanConfigQueue.clear();
}

void PixFitNet::resetNetworkNow() {
	ERS_LOG(m_histoUnitString << ": Resetting network thread " << getThreadId());
	cleanup();
}

void PixFitNet::setISfinishScan(ISInfoDictionary dict, std::string serverName, std::string rodName) {
	ISInfoBool isfinish(1);
	try {
		dict.checkin(serverName + "." + rodName + "_FinishScan", isfinish);
#ifdef DEBUG_ND
		dict.getValue(serverName + "." + rodName + "_FinishScan", isfinish);
		ERS_LOG("Got isfinish: " << (int)isfinish);
#endif
	} catch(...) {
		ERS_INFO("Dictionary checkin failed for Stopping Rod " << rodName);
	}
	ERS_LOG("Successfully checked Rod " << rodName << " (back?) into IS for Stopping");
}
