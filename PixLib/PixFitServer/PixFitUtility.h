#ifndef PixFitUtility
#define PixFitUtility

#include "PixFitScanConfig.h"
 
namespace PixLib {
  namespace FitUtility {

    inline PixFitScanConfig::scanType findScanType(scanParameters const & pars) {
      if((pars.scanHistScurveMeanFill || pars.scanHistScurveSigmaFill || pars.scanHistScurveChi2Fill) && pars.scanLoop0DSPProcessing ) // checking for this type makes only sense if loop was executed in Controller
        return PixFitScanConfig::scanType::SCURVE;
      else if (pars.scanHistToTMeanFill || pars.scanHistToTSigmaFill)
        return PixFitScanConfig::scanType::TOT; // return scanType::TOT
      else if (pars.scanHistOccupancyFill)// return scanType::OCCUPANCY
        return PixFitScanConfig::scanType::OCCUPANCY;
      else {
       ERS_LOG("This scan type is not yet accommodated for, returning occupancy type as default.");
       return PixFitScanConfig::scanType::OCCUPANCY;
      }
  }

    inline int getNumOfTotalMaskSteps(scanParameters const & pars) {
      return pars.scanMaskTotalSteps;
    }

    inline int getNumOfMaskSteps(scanParameters const & pars) {
      return pars.scanMaskSteps;
    }

  }
}
#endif
