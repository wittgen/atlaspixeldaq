/* @file PixFitResult.cxx
 *
 *  Created on: Dec 3, 2013
 *      Author: mkretz, marx
 */

#include <memory>

#include "PixFitResult.h"

using namespace PixLib;

PixFitResult::PixFitResult(std::shared_ptr<const PixFitScanConfig> scanConfig) {
	this->m_scanConfig = scanConfig;
}

std::shared_ptr<const PixFitScanConfig> PixFitResult::getScanConfig() const {
	return m_scanConfig;
}

PixFitScanConfig const * const PixFitResult::getScanConfigRaw() const {
	    return m_scanConfig.get();
}

PixFitScanConfig::ScanIdType PixFitResult::getScanId() const {
	return m_scanConfig->scanId;
}
