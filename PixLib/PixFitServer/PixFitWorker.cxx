/* @file PixFitWorker.cxx
 *
 *  Created on: Dec 3, 2013
 *      Author: mkretz, marx
 */

#include <memory>

#include "PixFitFitter_lmfit.h"
#include "PixFitWorker.h"
#include "PixFitResult.h"
#include "RawHisto.h"
#include "PixFitInstanceConfig.h" //  for threading settings
#include "PixFitManager.h"

using namespace PixLib;

PixFitWorker::PixFitWorker(PixFitWorkQueue<RawHisto> *histoQueue,
		PixFitWorkQueue<PixFitResult> *resultQueue) {
	this->m_histoQueue = histoQueue;
	this->m_resultQueue = resultQueue;
	this->m_threadName = "worker";
	m_fitter = std::make_unique<PixFitFitter_lmfit>();
}

PixFitWorker::~PixFitWorker() {
}

void PixFitWorker::loop() {

	while (!PixFitManager::stop_all) {
		/* Get work. */
		auto histo = m_histoQueue->getWork();
                if(m_histoQueue->stopping()) return;

		/* Instantiate PixFitFitter. */
		std::unique_ptr<PixFitResult> result;

		/* Make decision whether fit is needed or not.
		 * (so far only analog/digital/threshold scans supported) */
		PixFitScanConfig::scanType scanType = histo->getScanConfig()->findScanType();
		PixFitScanConfig::intermediateType intermediateType = histo->getScanConfig()->intermediate;
		EnumScanFitMethod::FitMethod fitMethod = histo->getScanConfig()->getFitMetod();

		/* INTERMEDIATEs */
		if (intermediateType == PixFitScanConfig::intermediateType::INTERMEDIATE_OCCUPANCY
				|| intermediateType == PixFitScanConfig::intermediateType::INTERMEDIATE_TOT) {
			result = std::make_unique<PixFitResult>(histo->getScanConfig());
			result->rawHisto = std::move(histo);
		}/* THRESHOLD */
		else if (scanType == PixFitScanConfig::scanType::SCURVE) {
			switch (fitMethod) {
        		  case EnumScanFitMethod::FitMethod::FIT_LMMIN :
        		    result = m_fitter->lmfit(PixFitInstanceConfig::getThreadingSettings(), std::move(histo));
        		  break;
        		  case EnumScanFitMethod::FitMethod::FIT_LINEAR_INT :
        		    result = m_fitter->linearInterpolation(std::move(histo));
        		  break;
        		  case EnumScanFitMethod::FitMethod::FIT_DERIVATIVE :
			    result = m_fitter->derivativeMethod(std::move(histo));
			  break;
			  default:
			   result = m_fitter->lmfit(PixFitInstanceConfig::getThreadingSettings(), std::move(histo));
			}
		} /*OCCUPANCY & TOT */
		else if (scanType == PixFitScanConfig::scanType::OCCUPANCY 
				|| scanType == PixFitScanConfig::scanType::TOT){
			result = std::make_unique<PixFitResult>(histo->getScanConfig());
			if(!histo->getScanConfig()->doIntermediateHistos() )result->rawHisto = std::move(histo);
		}

		/* Enqueue PixFitResult object. */
		m_resultQueue->addWork(std::move(result));
	}

  PIX_LOG("Exiting worker...");

}

void PixFitWorker::stopWork()
{
  m_histoQueue->stopWork();
}
