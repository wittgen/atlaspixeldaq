/* @file PixFitPublisher.cxx
 *
 *  Created on: Dec 10, 2013
 *      Author: mkretz, marx
 */

#include <iostream>
#include <sstream>
#include <memory>

#include <ipc/core.h>
#include <oh/OHRootProvider.h>
#include <is/infoT.h>
#include <is/infodictionary.h>

#include <TF1.h>
#include <TH2.h>

#include "PixHistoServer/PixHistoServerInterface.h"

#include "PixFitPublisher.h"
#include "PixFitResult.h"
#include "PixFitInstanceConfig.h"
#include "PixFitManager.h" //for ROOT lock

// #define DEBUG_ND

using namespace PixLib;

/** @todo Remove "global". */
OWLSemaphore semaphore;

class PixFitPublisherCommandListener : public OHCommandListener{
 public:
  void command ( const std::string & name, const std::string & cmd )
  {
    std::cout << " Command " << cmd << " received for the " << name << " histogram" << std::endl;
  }
  
  void command ( const std::string & cmd )
  {
    std::cout << " Command " << cmd << " received for all histograms" << std::endl;
    if ( cmd == "exit" )
      {
	semaphore.post();
      }
  }
};

PixFitPublisher::PixFitPublisher(PixFitWorkQueue<PixFitResult> *publishQueue,
		PixFitInstanceConfig *instanceConfig) {
	this->m_publishQueue = publishQueue;
	this->m_threadName = "publisher";
	this->m_instanceConfig = instanceConfig;
}

PixFitPublisher::~PixFitPublisher() {
}

void PixFitPublisher::loop() {
    
  while (!PixFitManager::stop_all) {

    /* Get work and gather other scanConfig information */
    auto result = m_publishQueue->getWork();

     /* skip in the case of null ptr */
      if (!result){
	continue;
      }

     if(m_publishQueue->stopping()) break;

    auto scanConfig = result->getScanConfig();
    PixFitScanConfig::scanType scanType = scanConfig->findScanType();
    PixFitScanConfig::intermediateType intermediateType = scanConfig->intermediate;

    /* Build an OHRootProvider using scanConfig information */
    IPCPartition partition(scanConfig->partitionName);
    PixFitPublisherCommandListener lst;
    OHRootProvider provider(partition, scanConfig->serverName, scanConfig->providerName, &lst);
    ISInfoDictionary dict(partition);

    /* Decide a folder name */
    std::string ROD_String = "ROD_" +
    		scanConfig->histogrammer.crateletter +
    		std::to_string(scanConfig->histogrammer.crate) +
    		"_S" + std::to_string(scanConfig->histogrammer.rod);

    int usingFei3 = 0;
    if(scanConfig->geo->pixType() ==  PixGeometry::pixelType::FEI2_CHIP) usingFei3 = 1;
    int rodChipId = RxConversion(scanConfig->histogrammer.slave,
                                 scanConfig->histogrammer.histo,
                                 result->chipId, usingFei3 ? 128 : 8);
    std::string folder_Name;
    if(usingFei3) folder_Name = "/" + //  Nick's initial scheme for handling FE-I3
                                 std::to_string(scanConfig->scanId) + "/" + ROD_String + "/MCC" +
                                 std::to_string(rodChipId / 16) + "/Chip" +
                                 std::to_string(rodChipId % 16);
    else folder_Name = "/" + std::to_string(scanConfig->scanId) + "/" + ROD_String + "/Rx" + std::to_string(rodChipId);

    /* Get some info on the histogram as strings. */
    std::string slave = std::to_string(scanConfig->histogrammer.slave);
    std::string histo = std::to_string(scanConfig->histogrammer.histo);
    std::string chipId = std::to_string(result->chipId);
    std::string binNumber = std::to_string(scanConfig->binNumber);

    /* Locking ROOT for the publishing. */
    //std::lock_guard<std::mutex> lock(PixFitManager::root_m);

    /* Publish according to scantype. Check for intermediate types first! */
    if (intermediateType == PixFitScanConfig::intermediateType::INTERMEDIATE_OCCUPANCY) {
      provider.publish(*result->histo_occ, folder_Name + "/Occup_intermediate_" +
		       slave + "_" + histo  + "_" + chipId + "-" + binNumber);
    }
    else if (intermediateType == PixFitScanConfig::intermediateType::INTERMEDIATE_TOT) {
      provider.publish(*result->histo_totmean, folder_Name + "/ToTMean_" + chipId + "-" + binNumber);
      provider.publish(*result->histo_totsum, folder_Name + "/ToTSum_" + chipId + "-" + binNumber);
      provider.publish(*result->histo_totsum2, folder_Name + "/ToTSum2_" + chipId + "-" + binNumber);
      provider.publish(*result->histo_totsigma, folder_Name + "/ToTSigma_" + chipId + "-" + binNumber);
      provider.publish(*result->histo_occ, folder_Name + "/Occup_" + chipId + "-" + binNumber);
    }
    else if (scanType == PixFitScanConfig::scanType::OCCUPANCY && !scanConfig->doIntermediateHistos()) {
      provider.publish(*result->histo_occ, folder_Name + "/Occup_0");  
    }
    else if (scanType == PixFitScanConfig::scanType::SCURVE ) {
      provider.publish(*result->histo_thresh, folder_Name + "/1DTh_" + chipId);
      provider.publish(*result->histo_noise, folder_Name + "/1DNoi_" + chipId);
      provider.publish(*result->histo_chi2, folder_Name + "/1DChi2_" + chipId);
      provider.publish(*result->histo_thresh2D, folder_Name + "/Thr_" + chipId);
      provider.publish(*result->histo_noise2D, folder_Name + "/Noise_" + chipId);
      provider.publish(*result->histo_chi2_2D, folder_Name + "/Chi2_" + chipId);
    }
    else if (scanType == PixFitScanConfig::scanType::TOT && !scanConfig->doIntermediateHistos() ) {
      provider.publish(*result->histo_totmean, folder_Name + "/ToTMean_" + chipId);
      provider.publish(*result->histo_totsum, folder_Name + "/ToTSum_" + chipId);
      provider.publish(*result->histo_totsum2, folder_Name + "/ToTSum2_" + chipId);
      provider.publish(*result->histo_totsigma, folder_Name + "/ToTSigma_" + chipId);
      provider.publish(*result->histo_occ, folder_Name + "/Occup_" + chipId);
    }
    //Do nothing for OCCUPANCY AND TOT with intermediate histos

    //ERS_DEBUG(0, result->getScanConfig()->histogrammer.makeHistoString() << " - " << chipId << ": Histogram was published to OH with internal ID " << result->getScanConfig()->fitFarmId);

    /* Check if publishing of results is complete for a particular ROD and signal to IS.
     * Make sure there is no race introduced here, in case non-intermediate histograms are published
     * before their corresponding intermediate histos. */
    if (intermediateType == PixFitScanConfig::intermediateType::INTERMEDIATE_NONE
    		&& m_instanceConfig->scanlist.reduceScanCount(result->getScanConfigRaw()->fitFarmId)) {
		ERS_LOG("Scan finished! All histograms for " << ROD_String << " are ready.");
		ISInfoBool isfinish(1);
		dict.checkin(scanConfig->serverName + "." + ROD_String + "_FinishScan", isfinish);
#ifdef DEBUG_ND
		dict.getValue(scanConfig->serverName + "." + ROD_String + "_FinishScan", isfinish);
		ERS_LOG("Got isfinish: " << (int)isfinish);
		
#endif
    }
  }

 PIX_LOG("Exiting publisher...");

}


int PixFitPublisher::RxConversion(int slave, int histounit, int chip, int chipsPerUnit) {
  return chipsPerUnit*(2*slave + histounit) + chip;
}

void PixFitPublisher::stopWork()
{
  m_publishQueue->stopWork();
}
