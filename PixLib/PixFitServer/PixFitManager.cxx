/* @file PixFitManager.cxx
 *
 *  Created on: Nov 28, 2013
 *      Author: mkretz, marx
 */

#include <memory>
#include <utility>

#include <vector>
#include <iostream>
#include <bitset>
#include <map>
#include <cmath>
#include <queue>
#include <functional>
#include <mutex>
#include <condition_variable>

#include <thread>
#include <regex>
#include <boost/algorithm/string.hpp>

#include <is/info.h>
#include <is/inforeceiver.h>

#include "RootDb/RootDb.h"
#include "PixModule/PixModule.h"

#include "DefaultClient.h"
#include "SetNetwork.h"
#include "ResetSlave.h"

#include "PixFitManager.h"
#include "PixFitNet.h"
#include "PixFitPublisher.h"
#include "PixFitWorker.h"
#include "PixFitAssembler.h"
#include "PixFitResult.h"

// #define DEBUG_ND

/** @todo: rewrite the locking mechanisms. */
/* Global lock for using anything remotely ROOTish. */
namespace PixLib {
std::mutex PixFitManager::root_m;
std::atomic_bool PixFitManager::stop_all = false;

/* Queue to hold values of callback items, @todo: find another way to do this */
PixFitWorkQueue<std::pair<std::string, std::string>> RODqueue("RODqueue");
}

using namespace PixLib;

PixFitManager::PixFitManager(
  	        const char* server_name, const char* partition_name, const char *dbsrv_name, const char* instance_name,
                bool slaveEmu, const char* slaveEmuConfigFile, const char* ifname,
		const char* idTag, const char* connTag, const char* cfgTag, const char* cfgMTag) :
		fitQueue("FitQueue"),
		resultQueue("ResultQueue"),
		publishQueue("PublishQueue"),
		m_fitFarmCounter(0),
		instanceConfig(server_name, partition_name, dbsrv_name, instance_name, slaveEmu, slaveEmuConfigFile, ifname,
			       idTag, connTag, cfgTag, cfgMTag)
		{

	/* Associate blacklist with the queues. */
	fitQueue.setBlackList(std::bind(&BlackList::investigateWorkObject, &instanceConfig.blacklist, std::placeholders::_1));
	resultQueue.setBlackList(std::bind(&BlackList::investigateWorkObject, &instanceConfig.blacklist, std::placeholders::_1));
	publishQueue.setBlackList(std::bind(&BlackList::investigateWorkObject, &instanceConfig.blacklist, std::placeholders::_1));
}

PixFitManager::PixFitManager(
		const char* server_name, const char* partition_name, const char *dbsrv_name, const char* instance_name,
                bool slaveEmu, const char* slaveEmuConfigFile) :
                PixFitManager(server_name, partition_name, dbsrv_name, instance_name, slaveEmu, slaveEmuConfigFile, "eth0",
        "Base", "Base", "Base", "Base") {}

void PixFitManager::stop(){
  stop_all = true;
  RODqueue.stopWork( );
  PIX_LOG("Stopping FitManager");

}

void PixFitManager::run() {
	printBanner();
	setupPixFitServer();

	/* Setup IPC and IS */
	IPCPartition partition(instanceConfig.getPartitionName());
	ISInfoDictionary dict(partition);

	/* Spawn network threads. */
	ERS_LOG("Starting networking threads.");

	for (auto const & netConfig : networkConfigs) {
		if (netConfig->isActive()) {
		  /* @todo: !! The first variant seems to create SEGFAULTs when the FitServer is started by the PMG, while
		   * running fine standalone. Using the "new" works correct in all cases. Misuse of shared_ptr here?! */
		   m_netObjects.emplace_back(std::make_unique<PixFitNet>(&fitQueue, netConfig.get(), &instanceConfig));
	           m_netObjects.back()->start();
		}
	}

	/* Spawn worker thread. */
	ERS_LOG("Starting fitting threads.");
	PixFitWorker worker(&fitQueue, &resultQueue);
	worker.start();

	/* Spawn assembler thread. */
	ERS_LOG("Starting assembler thread.");
	PixFitAssembler assembler(&resultQueue, &publishQueue, &instanceConfig);
	assembler.start();

	/* Spawn publisher thread. */
	ERS_LOG("Starting publishing thread.");
	PixFitPublisher publisher(&publishQueue, &instanceConfig);
	publisher.start();

	/* Spawn publisher thread 2. */
	ERS_LOG("Starting publishing thread 2.");
	PixFitPublisher publisher2(&publishQueue, &instanceConfig);
	publisher2.start();

	/* Spawn publisher thread 3. */
	ERS_LOG("Starting publishing thread 3.");
	PixFitPublisher publisher3(&publishQueue, &instanceConfig);
	publisher3.start();

	/* Spawn publisher thread 4. */
	ERS_LOG("Starting publishing thread 4.");
	PixFitPublisher publisher4(&publishQueue, &instanceConfig);
	publisher4.start();

	/* Spawn publisher thread 5. */
	ERS_LOG("Starting publishing thread 5.");
	PixFitPublisher publisher5(&publishQueue, &instanceConfig);
	publisher5.start();

	/* Spawn publisher thread 6. */
	//ERS_LOG("Starting publishing thread 6.");
	//PixFitPublisher publisher6(&publishQueue, &instanceConfig);
	//publisher6.start();

	/* Spawn publisher thread 7. */
	//ERS_LOG("Starting publishing thread 7.");
	//PixFitPublisher publisher7(&publishQueue, &instanceConfig);
	//publisher7.start();


	/* Spawn publisher thread8. */
	//ERS_LOG("Starting publishing thread 8.");
	//PixFitPublisher publisher8(&publishQueue, &instanceConfig);
	//publisher8.start();


	/* Spawn publisher thread 9. */
	//ERS_LOG("Starting publishing thread 9.");
	//PixFitPublisher publisher9(&publishQueue, &instanceConfig);
	//publisher9.start();


	/* Spawn publisher thread 10. */
	//ERS_LOG("Starting publishing thread 10.");
	//PixFitPublisher publisher10(&publishQueue, &instanceConfig);
	//publisher10.start();
	

	
	/* Subscribe to IS. */
	ISInfoReceiver rec(partition); 
	ISInfoBool scnfinish(false);
	ISInfoInt scnid;
	int feFlavour;
        int currentcrate1;
        int currentrod1;
	ISInfoInt modmask;
	scanParameters params;

	std::string serverName = instanceConfig.getServerName();

	ISCriteria criteriaStart(".*_StartScan");
	ISCriteria criteriaAbort(".*_AbortScan");

	if (!instanceConfig.usingSlaveEmu()) {

	  /* Get scan configuration information details from IS */

	  /* Get scan start from IS (with lock).
	   * Subscribe here now uses criteria so will callback whenever a controller of the RODs in 
	   * this FitServer instance gets updated. */
	  rec.subscribe(serverName, criteriaStart, PixFitManager::callback);
	  rec.subscribe(serverName, criteriaAbort, PixFitManager::callback);

	  ERS_LOG("THIS FIT SERVER INSTANCE="<<instanceConfig.getInstanceId());
	  while (!stop_all) {

	    /* Blocking call to get ROD string from queue. */
	    auto rodWork = RODqueue.getWork();
	    if(!rodWork){
	      if(!stop_all)PIX_ERROR("Got null ptr from RODQueue, exiting FitManager...");
	      break;
	    }

	    std::string scnstart = rodWork->second;
	    if (rodWork->first == "start") {
	      
	      if (!RodInFitServerInstance(scnstart))
		continue;
	      // Remove old histograms for this ROD
	      std::string rege=instanceConfig.getInstanceId()+"."+"/[0-9]+/"+scnstart+"/.*";
	      try{
		dict.removeAll("Histogramming", ISCriteria(rege));
	      }catch( daq::is::Exception & ex ) {
		ERS_LOG("Error deleting histograms "<<rege);
	      }
	      
	      /* Fill crate and rod based on string in start scan flag. */
	      currentcrate1 = convertISstring(scnstart).second.first;
	      currentrod1 = convertISstring(scnstart).second.second;

	      /* Get feFlavour, decName, scanId and module mask from IS (put a catch here since there is no lock and timing might be off). */
	      try {
		auto readFromIS = [&dict](std::string const & s, auto & val){
                  ISInfoT<std::remove_reference_t<decltype(val)>> IStype;
		  dict.getValue(s, IStype);
		  val = IStype.getValue();
		};

	        readFromIS(serverName + "." + scnstart + ".FEflavour", feFlavour);
	        dict.getValue(serverName +"." + scnstart + "_ScanId", scnid);
	        dict.getValue(serverName + "." + scnstart + "_ScanModuleMask", modmask);
		readFromIS(serverName + "." + scnstart + "_Scan_Repetitions", params.scanRepetitions);
		readFromIS(serverName + "." + scnstart + "_Scan_MaskTotalSteps", params.scanMaskTotalSteps);
                readFromIS(serverName + "." + scnstart + "_Scan_MaskSteps", params.scanMaskSteps);
                readFromIS(serverName + "." + scnstart + "_Scan_Hist_ScurveMean_Fill", params.scanHistScurveMeanFill);
                readFromIS(serverName + "." + scnstart + "_Scan_Hist_ScurveSigma_Fill", params.scanHistScurveSigmaFill);
                readFromIS(serverName + "." + scnstart + "_Scan_Hist_ScurveChi2_Fill", params.scanHistScurveChi2Fill);
                readFromIS(serverName + "." + scnstart + "_Scan_Hist_ToTMean_Fill", params.scanHistToTMeanFill);
                readFromIS(serverName + "." + scnstart + "_Scan_Hist_ToTSigma_Fill", params.scanHistToTSigmaFill);
                readFromIS(serverName + "." + scnstart + "_Scan_Hist_Occupancy_Fill", params.scanHistOccupancyFill);
                readFromIS(serverName + "." + scnstart + "_Scan_Loop0_Steps", params.scanLoop0Steps);
                readFromIS(serverName + "." + scnstart + "_Scan_Loop0_Min", params.scanLoop0Min);
                readFromIS(serverName + "." + scnstart + "_Scan_Loop0_Max", params.scanLoop0Max);
                readFromIS(serverName + "." + scnstart + "_Scan_Loop0_DSPProcessing", params.scanLoop0DSPProcessing);
                readFromIS(serverName + "." + scnstart + "_Scan_FitMethod", params.scanFitMethod);
	      } catch(...) {
	        ERS_INFO("Failed to get info from IS");
	      }
	      if(!instanceConfig.blacklist.isScanIdListed(scnid)){

	        PixGeometry::pixelType scanGeoFlav;
                std::string festring;

	        if(feFlavour == PixModule::PM_FE_I4B) {
		    scanGeoFlav = PixGeometry::pixelType::FEI4_CHIP;
		    ERS_LOG("Selected FE-I4 flavour for crate/ROD " << currentcrate1 << "/" << currentrod1 << ".");
                    festring = "FE-I4";
		  } else if(feFlavour == PixModule::PM_FE_I2) {
		    scanGeoFlav = PixGeometry::pixelType::FEI3_CHIP;
		    ERS_LOG("Selected FE-I3 flavour for crate/ROD " << currentcrate1 << "/" << currentrod1 << ".");
                    festring = "FE-I3";
		  } else {
		    scanGeoFlav = PixGeometry::pixelType::FEI4_CHIP;
		    ERS_LOG("Warning: Wrong front end selection (feFlavour=" << (int) feFlavour
			    << " instead of 2 [FE-I3] or 130 [FE-I4]) for crate/ROD "
			    << currentcrate1 << "/" << currentrod1 << ". Setting it to FE-I4 automatically.");
		  }

		dict.checkin( serverName + "." + scnstart + "_FinishScan", scnfinish);
#ifdef DEBUG_ND
		std::string ISLookupString = serverName + "." + scnstart + "_FinishScan";
		dict.getValue(ISLookupString, scnfinish);
		ERS_LOG("Got scnfinish: " << (int)scnfinish << " from IS lookup string '" << ISLookupString << "'");
#endif
	        ERS_LOG("Started " << festring << " scan " << static_cast<int>(scnid) << " for crate/ROD " << currentcrate1 << "/" << currentrod1 <<
		   ", MCC mask in binary = " << std::bitset<32>(modmask) << std::dec << " with internal ID " << m_fitFarmCounter);

	        /* Setup and fill PixFitScanConfig object with all this information */
	        setupScan(scnid, currentcrate1, currentrod1, modmask, scanGeoFlav, params);
	      } else {
	        ERS_LOG("Cannot start scan with already blacklisted scan ID " << static_cast<int>(scnid));
#ifdef DEBUG_ND
		std::string ISLookupString = serverName + "." + scnstart + "_FinishScan";
		dict.getValue(ISLookupString, scnfinish);
                ERS_LOG("Got scnfinish: " << (int)scnfinish << " from IS lookup string '" << ISLookupString << "'");
#endif
	      }
	    }

	    /* Aborting scan otherwise. */
	    else if (rodWork->first == "abort") {
		/* Fill crate and rod based on string in start scan flag. */
		int currentcrate2 = convertISstring(scnstart).second.first;
		int currentrod2 = convertISstring(scnstart).second.second;
		try {
		  dict.getValue(serverName+"." + scnstart + "_ScanId", scnid);
		}
		catch(...) {
		  ERS_INFO("Failed to get scanid from IS");
		}
		cancelScan(scnid, currentcrate2, currentrod2);
		ISInfoBool isfinish(1);
		try {
		    dict.checkin(serverName + "." + scnstart + "_FinishScan", isfinish);
#ifdef DEBUG_ND
		    dict.getValue(serverName + "." + scnstart + "_FinishScan", isfinish);
		    ERS_LOG("Got isfinish: " << (int)isfinish);
#endif
		} catch(...) {
		    ERS_INFO("Dictionary checkin failed for Stopping Rod " << scnstart);
		}
		ERS_LOG("Successfully checked Rod " << scnstart << " into IS for Stopping");
	    }
	  }
	}
	else {
          PixFitScanConfig::EmuConfig emuConfig = instanceConfig.getEmuConfig();
          uint32_t modMask;
          sscanf(emuConfig.modMask.c_str(),"%x",&modMask);
	  if(setupScan(scnid=33,
	               emuConfig.crate,
	               emuConfig.rod,
	               modMask,
	               // modMask=257, /*  Works for two slaveEmu runs! */
	               emuConfig.flavour,
		       params ))
	  {
            int networking = 1;
            std::string rodIp;
            if(emuConfig.useRod) {
              std::vector<HistoUnit> units = translateModuleMask(modMask);
	      std::array<uint32_t,2> nSlavehistos = {0, 0};
              for (auto &unit : units) {
                unit.crate = emuConfig.crate;
                unit.rod = emuConfig.rod;
                nSlavehistos[unit.slave]++;
              }
              uint32_t iHisto(0);
              SetNetwork netCmd;
              netCmd.ipA_1 = 0; netCmd.ipA_2 = 0; netCmd.ipA_3 = 0; netCmd.ipA_4 = 0; netCmd.portA = 0;
              netCmd.ipB_1 = 0; netCmd.ipB_2 = 0; netCmd.ipB_3 = 0; netCmd.ipA_4 = 0; netCmd.portB = 0;
              for (auto &unit : units) {
                auto net_it_found = std::find_if(std::cbegin(m_netObjects), std::cend(m_netObjects),
                        [&](const auto & ptr){return *(ptr->getConfig()->getHistogrammer()) == unit;}
                );

                if(net_it_found != m_netObjects.end()) {
                  PixFitNetConfiguration const* netConfig = (*net_it_found)->getConfig();
                  rodIp = "192.168.2." + to_string(emuConfig.rod) + "0"; //  This rule works in SR1 for now. SOMETHING MORE GENERAL NEEDED !!!
                  DefaultClient client(rodIp.c_str());
                  iHisto++;
                  if(iHisto < nSlavehistos[unit.slave]) {
                    ipStringToOctets(inet_ntoa(netConfig->getLocalIpAddress()), netCmd.ipA_1, netCmd.ipA_2, netCmd.ipA_3, netCmd.ipA_4);
                    netCmd.portA = ntohs(netConfig->getLocalPort());
                  } else {
                    if(unit.histo == 0) {
                      ipStringToOctets(inet_ntoa(netConfig->getLocalIpAddress()), netCmd.ipA_1, netCmd.ipA_2, netCmd.ipA_3, netCmd.ipA_4);
                      netCmd.portA = ntohs(netConfig->getLocalPort());
                    } else {
                      ipStringToOctets(inet_ntoa(netConfig->getLocalIpAddress()), netCmd.ipB_1, netCmd.ipB_2, netCmd.ipB_3, netCmd.ipB_4);
                      netCmd.portB = ntohs(netConfig->getLocalPort());
                    }
                    if(iHisto == 1) { //  Do something stupid like this, so networking doesn't break over unused histo unit
                      if(unit.histo == 0) {
                        netCmd.ipB_1 = netCmd.ipA_1; netCmd.ipB_2 = netCmd.ipA_2; netCmd.ipB_3 = netCmd.ipA_3; netCmd.ipB_4 =  netCmd.ipA_4;
                        netCmd.portB = netCmd.portA + 1;
                      } else {
                        netCmd.ipA_1 = netCmd.ipB_1; netCmd.ipA_2 = netCmd.ipB_2; netCmd.ipA_3 = netCmd.ipB_3; netCmd.ipA_4 =  netCmd.ipB_4;
                        netCmd.portA = netCmd.portA - 1;
                      }
                    }
                    netCmd.slaveid=unit.slave;
                    std::cout << "Setting network on slave " << unit.slave << std::endl;
                    ipStringToOctets(rodIp, netCmd.ip_1, netCmd.ip_2, netCmd.ip_3, netCmd.ip_4);
                    netCmd.ip_4+=1+unit.slave;
                    ipStringToOctets("255.255.252.0", netCmd.subnet_1, netCmd.subnet_2, netCmd.subnet_3, netCmd.subnet_4);
                    ipStringToOctets("192.168.0.100", netCmd.gw_1, netCmd.gw_2, netCmd.gw_3, netCmd.gw_4);
                    client.run(netCmd);
                    iHisto = 0;
                    netCmd.ipA_1 = 0; netCmd.ipA_2 = 0; netCmd.ipA_3 = 0; netCmd.ipA_4 = 0; netCmd.portA = 0;
                    netCmd.ipB_1 = 0; netCmd.ipB_2 = 0; netCmd.ipB_3 = 0; netCmd.ipA_4 = 0; netCmd.portB = 0;
                  }
                } else {
                  std::cout << " *****\n ***** SetupScan failed to find network thread for the requested crate/slot/modMask combination\n *****" << std::endl;
                  networking = 0;
                  break;
                }
              }
            }

            /* Dirty hack to wait for histogramming to complete in emulator (stand-alone) mode. */
            std::cout << ">>>>> Issue keyboard EOF (ctrl-D) to stop FitServer when histograms are either done or there is no chance they ever will be. <<<<<" << std::endl;
            std::cin.ignore().get();

            if(emuConfig.useRod && networking) {
              // tell the slaves to stop and reset
              DefaultClient client(rodIp.c_str());
              ResetSlave stopSlave;
              stopSlave.whichSlave = 2;
              stopSlave.resetHisto = true;
              stopSlave.resetNetwork = true;
              client.run(stopSlave);

            }
	  }else std::cout << " *****\n ***** SetupScan failed (Is your root scan-cfg file OK?)\n *****" << std::endl;
	}

	/* Unsubscribe from IS. */
	if(!instanceConfig.usingSlaveEmu())rec.unsubscribe(serverName, criteriaStart);
	if(!instanceConfig.usingSlaveEmu())rec.unsubscribe(serverName, criteriaAbort);

        for(auto& netObject : m_netObjects) netObject->stopNetwork();
        publisher.stopWork();
	publisher2.stopWork();
	publisher3.stopWork();
	publisher4.stopWork();		
	publisher5.stopWork();

        assembler.stopWork();
        worker.stopWork();

        for(auto& netObject : m_netObjects)
        {
          std::thread *tT = netObject->getThread();
          tT->join();
        }
	worker.join();
	assembler.join();
	publisher.join();
	publisher2.join();
	publisher3.join();
	publisher4.join();			
	publisher5.join();
	//publisher6.join();
	//publisher7.join();
	//publisher8.join();
	//publisher9.join();
	//publisher10.join();
	PIX_LOG("Exiting fitManager...");
}


int PixFitManager::findFreePort(const char *ipAddress, int startPort, int numOfPorts){
	const int maxPortsToScan = 1000;
	int localSock = -1;
	int availablePorts = 0;

	// ERS_DEBUG(0, Looking for " << numOfPorts << " available ports on IP " << ipAddress);

	for (int i = startPort; i <= startPort + maxPortsToScan; i++) {
		localSock = socket(AF_INET, SOCK_STREAM, 0);

		if (localSock<0) {
			ERS_INFO("Opening socket failed.");
			perror("Error:");
			return -1;
		}

		int yes = 1;
		if (setsockopt(localSock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) <0) {
			ERS_INFO("Setting socket option failed");
			perror("Error:");
			close(localSock);
			return -1;
		}

		sockaddr_in my_addr{};
		my_addr.sin_family = AF_INET;
		inet_aton(ipAddress, &my_addr.sin_addr);
		my_addr.sin_port = htons(i);
		if (bind(localSock, (struct sockaddr *) &my_addr, sizeof(my_addr)) < 0) {
			//ERS_INFO(ipAddress << ":" << i << " is busy.");
			//perror("Error:");
			availablePorts = 0;
		}
		else {
			close(localSock);
			//ERS_INFO(ipAddress << ":" << i <<  " is OK.");
			availablePorts++;
		}
		if (availablePorts == numOfPorts) {
			int startingPort = i - numOfPorts + 1;
			ERS_LOG(numOfPorts << " ports available on IP " << ipAddress << " starting from " << startingPort);
			return startingPort;
		}
	}
	std::string info = "PixFitManager::findFreePort()";
	std::string mess = "Could not find any free ports for IP " + (std::string)ipAddress;
	ers::fatal(PixLib::pix::daq (ERS_HERE, info, mess));

  return -1;
}

void PixFitManager::callback(ISCallbackInfo * isc) {
    std::string type = "";
    ISInfoString isi;
    isc->value(isi);
    std::string val(isc->name());
    ERS_DEBUG(0, "CALLBACK : " << isc->name() << " with content " << isi);

	/* Nasty. Use some other queue type instead to distinguish between abort and start scan. */
	if (val.find("_StartScan") != std::string::npos ) {
	    type = "start";
	} else if (val.find("_AbortScan") != std::string::npos ) {
	    type = "abort";
	} else {
	    PIX_WARNING("Unrecognized callback "<<val);
	    return;
	}

    RODqueue.addWork(std::make_unique<std::pair<std::string, std::string>>(std::make_pair(type, static_cast<std::string>(isi))));
}

bool PixLib::PixFitManager::setupScan(PixFitScanConfig::ScanIdType scanId,
	        int crate, int rod,
		uint32_t modMask,
		PixGeometry::pixelType PixGeoFlav,
                scanParameters params,
                PixActions::SyncType sync) {

  ERS_LOG("Setting up Scan with ID: " << (int)scanId);

  auto geo = PixGeometry{PixGeoFlav};
  PixFitScanConfig::EmuConfig emuConfig;
  int numOfMaskSteps;
  int numOfTotalMaskSteps;
  emuConfig = instanceConfig.getEmuConfig();
  bool usingemu = instanceConfig.usingSlaveEmu();

 /* The number of work packages for this instance can be calculated as follows:
     * #active histo units * #mask steps */
    if(usingemu){
      numOfMaskSteps = (int) emuConfig.masksteps;
      numOfTotalMaskSteps = (int) emuConfig.masksteps;
    }else{
      numOfMaskSteps = params.scanMaskSteps;
      numOfTotalMaskSteps = params.scanMaskTotalSteps;

      std::cout << "Total mask steps: " << params.scanMaskTotalSteps << " we're doing: " << params.scanMaskSteps << "\n";
    }

  /* sanity check to make sure user selected sensible values for scan in Console */
  if(numOfMaskSteps > numOfTotalMaskSteps){
    std::string info = "PixFitManager::setupScan())";
    std::string mess = "Number of mask steps(" + std::to_string(numOfMaskSteps) + ") is larger than number of total mask steps (";
    mess = mess + std::to_string(numOfTotalMaskSteps) + ").  Please correct this in your Console parameter selection";
    ers::error(PixLib::pix::daq (ERS_HERE, info, mess));
  }
  
  /* Get the involved units without correct crate and rod fields from the module mask. */
  std::vector<HistoUnit> units = translateModuleMask(modMask);

  /* Count how many PixFitScanConfig objects are created (ignoring extra ones for mask stepping.) */
  int objCount = 0;
  int chipsPerUnit=0;

  /* Build and enqueue PixFitScanConfig objects. We iterate through the active histo units and
   * look for a match in the networking threads. When a match is found the correct number of
   * PixFitScanConfigs is created and put in the network thread's queue. */
  for (auto &unit : units) {
	  /* Fill with correct values for crate and ROD. */
	  unit.crate = crate;
	  unit.rod = rod;

	  for (auto const & net: m_netObjects) {
	  /* Only create and enqueue object(s) if network thread serves the particular histo unit. */
		if (*(net->getConfig()->getHistogrammer()) == unit) {
			for (int j = 0; j < numOfMaskSteps; j++) {
			  auto pixFitScanConfig = std::make_unique<PixFitScanConfig>(geo, usingemu);
			  pixFitScanConfig->partitionName = instanceConfig.getPartitionName();
			  pixFitScanConfig->serverName = instanceConfig.getServerName();
			  pixFitScanConfig->providerName = instanceConfig.getInstanceId();
			  pixFitScanConfig->emuConfig = emuConfig;
			  pixFitScanConfig->histogrammer = *(net->getConfig()->getHistogrammer());
			  pixFitScanConfig->maskId = j;
			  pixFitScanConfig->NumRow = geo.nRow();
			  pixFitScanConfig->NumCol = geo.nCol();
			  pixFitScanConfig->scanId = scanId;
			  pixFitScanConfig->modMask = modMask;
			  pixFitScanConfig->fitFarmId = m_fitFarmCounter;
			  pixFitScanConfig->setScanParameters(params);
                          chipsPerUnit=pixFitScanConfig->getNumOfChips(); // Inelegant, partial solution to
			                                                 // chip count problem raised below.
			  /* Enqueue pixFitScanConfig into network thread queue. */
			  net->putScanConfig(std::move(pixFitScanConfig));
		} 

			/* Increment created object count (ignoring mask steps) for this fitFarmId.
			* This works for the usual case with 8 chips per histounit. For cases where one ROD
			* might use strange combinations of active FEs per histo unit this will break! */
			objCount += chipsPerUnit;
		}
	  }
  }
  /* Put ID/objCount in ScanList. */
  instanceConfig.scanlist.setScanCount(m_fitFarmCounter, objCount);

  /* Increment internal counter after each setupScan for a ROD. */
  m_fitFarmCounter++;
  ERS_LOG("Finished setting up Scan with ID: " << (int)scanId);
  return true;
  /** @todo Signal readiness to entity that is steering the scan. Set IS variable back to 0? */
}

/** For configuring the FitFarm PixFitServer processes, we need to get some general settings
 * that are valid for all PixFitServer instances (such as partition name), as well as
 * configuration options specific to a particular PixFitServer process.
 * We need to know which histogramming units are associated to a certain PixFitServer process -
 * or rather to which PixFitNet thread - as well as the network configuration of those network
 * threads.
 * We assume that each FitFarm instance is assigned a unique ID directly after startup. It will
 * then use this ID to look up its configuration which consists of a list of assigned RODs. It will
 * then assign IP/port combination for the histogramming units on these RODs.
 * Afterwards it will publish a list of CRATE/ROD/SLAVE/HISTOUNIT, IP:port to IS.
 */
void PixLib::PixFitManager::setupPixFitServer() {
	/* Setup IPC and IS */
	IPCPartition partition(instanceConfig.getPartitionName());
	ISInfoDictionary dict(partition);

	/* Get configuration of networking threads. */
	auto netThreadConfigs = createNetThreadConfigs(instanceConfig.getConfiguration());

	/* Create the network configuration. */
	ERS_LOG("Creating FitFarm instance " << instanceConfig.getInstanceId() << " with configuration:");
	for (auto const & netThreadConfig : netThreadConfigs) {
		auto config = std::make_unique<PixFitNetConfiguration>();
		config->setLocalIpAddress(netThreadConfig->localIp);
		config->setLocalPort(netThreadConfig->localPort);
		config->setHistogrammer(netThreadConfig->histogrammer);
		config->enable();
		networkConfigs.push_back(std::move(config));

		ERS_LOG(netThreadConfig->localIp << ":" << netThreadConfig->localPort << " for "
				<< netThreadConfig->histogrammer.makeHistoString());

		/* Publish IP and port number to IS for Controller/PPC */
		HistoUnit h = netThreadConfig->histogrammer;
		std::string ISvariable = ".ROD_" + h.crateletter +
				std::to_string(h.crate)
		+ "_S" + std::to_string(h.rod)
		+ "_" + std::to_string(h.slave)
		+ "_" + std::to_string(h.histo);
		std::string ISvalue = netThreadConfig->localIp + ":" +
				std::to_string(netThreadConfig->localPort);
		ISInfoString isstring(ISvalue);
		dict.checkin( instanceConfig.getServerName() + ISvariable, isstring );
	}

	ERS_LOG("Network configuration contains " << networkConfigs.size() << " objects.");
}

void PixFitManager::printBanner() {
	std::cout << "===========================" << std::endl;
	std::cout << "Starting FitServer v"	<< PixFitInstanceConfig::fitServerVersion << std::endl;
	std::cout << "===========================" << std::endl;
	std::cout << "Partition: " << instanceConfig.getPartitionName()	<< std::endl;
	std::cout << "Server name: " << instanceConfig.getServerName() << std::endl;
	std::cout << std::endl;

	std::cout << "Installed RAM: " << instanceConfig.getRam() << " MB" << std::endl;
	std::cout << std::endl;

	std::cout << "Local IP address(es):" << std::endl;
	std::map<std::string, std::string> Ips = instanceConfig.getLocalIps();
	for (auto& [device, ip]: Ips) {
		std::cout << device << ": " << ip << std::endl;
	}
	std::cout << std::endl;
}

std::vector<std::unique_ptr<PixFitManager::NetworkThreadConfig> > PixLib::PixFitManager::createNetThreadConfigs(
		std::map<std::string, std::string> const & config_arg) {
  
        std::cout<<__PRETTY_FUNCTION__<<std::endl;
	std::vector<std::unique_ptr<PixFitManager::NetworkThreadConfig> > netThreadConfig;
	std::vector<std::string> rodNetworkIps;
	std::vector<std::string> rods;

	std::map<std::string, std::string> localIps = instanceConfig.getLocalIps();
	for (auto& networkInterface : instanceConfig.getRodNetworkInterfaces()) {
		if (localIps.find(networkInterface) != localIps.end()) {
			rodNetworkIps.push_back(localIps.find(networkInterface)->second);
		}
	}
	/* Preliminary: Listen on all available interfaces if no interfaces are specifically selected. */
	if (instanceConfig.getRodNetworkInterfaces().empty()) {
		ERS_LOG("Fallback: Listening on all available interfaces as rodNetworkInterfaces not configured.");
		rodNetworkIps.emplace_back("0.0.0.0");
	}

	for (auto const & it: config_arg) {
		if (it.second == instanceConfig.getInstanceId()) {
			rods.push_back(it.first);
		}
	}

	if (rodNetworkIps.empty()) {
		ERS_INFO("Error: No IPs available on the ROD network. Aborting setup.");
		return std::vector<std::unique_ptr<PixFitManager::NetworkThreadConfig> >();
	}

	ERS_LOG("Server has " << rodNetworkIps.size() << " IP(s) on the ROD network and is serving " << rods.size() << " ROD(s).");

	/* Now we create 4 histounits out of each ROD string and assign those to one IP/port combination. */

	for (auto & rod: rods) {
		HistoUnit histoUnit;

		/* Match the configuration string. */
		if (convertISstring(rod).first) {

		        histoUnit.crateletter = rod.at(4);
			histoUnit.crate = convertISstring(rod).second.first;
			histoUnit.rod = convertISstring(rod).second.second;

			/* Create 4 histogramming units per ROD. */
			for (int j = 0; j < 4; j++) {
				auto thisconfig = std::make_unique<PixFitManager::NetworkThreadConfig>();
				thisconfig->histogrammer = histoUnit;
				thisconfig->histogrammer.slave = j / 2;
				thisconfig->histogrammer.histo = j % 2;
				netThreadConfig.emplace_back(std::move(thisconfig));
			}
		}
	}

	/** Distribute equally across all available interfaces.
	 * @todo This is actually sub-optimal. Improve algorithm if necessary. */
	int numOfUnits = netThreadConfig.size();
	int numOfNics = rodNetworkIps.size();
	int unitsPerNic = (numOfUnits + numOfNics - 1) / numOfNics;

	int portNum = 0;
	std::string ipAddress;

	for (int i = 0; i < numOfUnits; i++) {
		/* For each new IP find valid range for ports. */
		if (i % unitsPerNic == 0){
			ipAddress = rodNetworkIps[i / unitsPerNic];
			portNum = findFreePort(ipAddress.c_str(), instanceConfig.startPort, unitsPerNic);
		}
		netThreadConfig[i]->localIp = ipAddress;
		netThreadConfig[i]->localPort = portNum + (i % unitsPerNic);
	}

	return netThreadConfig;
}

void PixFitManager::cancelScan(PixFitScanConfig::ScanIdType scanId, int crate, int rod) {
	ERS_LOG("Cancelling scan with ID " << scanId << " for crate/ROD " << crate << "/" << rod);

	/* Add scan ID to the global blacklist. Do this before the next step to make sure queue
	 * blacklists are set up already. */
	instanceConfig.blacklist.addScanId(scanId);

	/* Iterate through network threads to reset them. */
	for (auto& netObject : m_netObjects) {
		auto hist = netObject->getConfig()->getHistogrammer();
		if (hist->crate == crate && hist->rod == rod) {
		    netObject->resetNetworkNow();
		    netObject->clearScanConfigQueue();
		}
	}

	/* Tell assembler to clean up the hold to remove orphaned objects. */
	instanceConfig.assembler->setCleanFlag();
}


std::vector<HistoUnit> PixLib::PixFitManager::translateModuleMask(uint32_t modMask) {
	std::vector<HistoUnit> unitVec;
	HistoUnit histo;

	/* Initialize with invalid entries for crate and rod field. */
	histo.crate = -1;
	histo.rod = -1;

	if ((0x000000FF & modMask) > 0) {
		histo.slave = 0;
		histo.histo = 0;
		unitVec.push_back(histo);
	}
	if ((0x0000FF00 & modMask) > 0) {
		histo.slave = 0;
		histo.histo = 1;
		unitVec.push_back(histo);
	}
	if ((0x00FF0000 & modMask) > 0) {
		histo.slave = 1;
		histo.histo = 0;
		unitVec.push_back(histo);
	}
	if ((0xFF000000 & modMask) > 0) {
		histo.slave = 1;
		histo.histo = 1;
		unitVec.push_back(histo);
	}

	return unitVec;
}

std::pair <bool, std::pair <int,int> >  PixFitManager::convertISstring(std::string const & ISstring){
  std::regex regEx("ROD_[CILBD]([0-9]{1,2})_S([0-9]{1,2})");
  std::smatch match;
  bool success = std::regex_match(ISstring, match, regEx);
  int crate = 0;
  int rod = 0;
  if (success) {
    std::string scrate(match[1].first, match[1].second);
    std::string srod(match[2].first, match[2].second);
    crate = atoi(scrate.c_str());
    rod = atoi(srod.c_str());
  }
  std::cout<<__PRETTY_FUNCTION__<<" "<<ISstring<<" Crate "<<crate<<" ROD "<<rod<<std::endl;

  return std::make_pair(success, make_pair(crate, rod) );
}

//Just hot fix for the moment:TEMPORARY!!
bool PixFitManager::RodInFitServerInstance(std::string const & scnstart) {

  if (((instanceConfig.getInstanceId()).find("FitServer-1") != std::string::npos) && (scnstart.find("I1") != std::string::npos ))
    return true;
  
  else if (((instanceConfig.getInstanceId()).find("FitServer-2") != std::string::npos) && (scnstart.find("L1") != std::string::npos ))
    return true;

  else if (((instanceConfig.getInstanceId()).find("FitServer-3") != std::string::npos) && (scnstart.find("L2") != std::string::npos ))
    return true;

  else if (((instanceConfig.getInstanceId()).find("FitServer-4") != std::string::npos) && (scnstart.find("L3") != std::string::npos ))
    return true;

  else if (((instanceConfig.getInstanceId()).find("FitServer-5") != std::string::npos) && (scnstart.find("L4") != std::string::npos ))
    return true;

  else if (((instanceConfig.getInstanceId()).find("FitServer-6") != std::string::npos) && ((scnstart.find("B2") != std::string::npos) || (scnstart.find("B3") != std::string::npos )))
    return true;

  else if (((instanceConfig.getInstanceId()).find("FitServer-7") != std::string::npos) && ((scnstart.find("D1") != std::string::npos) || (scnstart.find("D2") != std::string::npos )))
    return true;

  // SR1
  else if (((instanceConfig.getInstanceId()).find("FitServer-SR1") != std::string::npos) && ( (scnstart.find("C1") != std::string::npos ) || (scnstart.find("C3") != std::string::npos ) || (scnstart.find("C4") != std::string::npos ) ) )
    return true;

  else
    return false;
}

void PixFitManager::ipStringToOctets(std::string const & ip, uint8_t &oct1, uint8_t &oct2, uint8_t &oct3, uint8_t &oct4) {
    // using ints, as scanf doesn't have a uint8 specifier
    int o1, o2, o3, o4;
    o1 = oct1; o2 = oct2; o3 = oct3; o4 = oct4;
    sscanf(ip.c_str(), "%d.%d.%d.%d", &o1, &o2, &o3, &o4);
    oct1 = o1; oct2 = o2; oct3 = o3; oct4 = o4;
}
