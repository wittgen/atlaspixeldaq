/*
 * PixFitFitter_lmfit.h
 *
 *  Created on: Dec 3, 2013
 *      Author: mkretz, marx
 */

#ifndef PIXFITFITTER_LMFIT_H_
#define PIXFITFITTER_LMFIT_H_

#include <string>
#include <memory>

#include <oh/OHRootProvider.h>

#include <TF1.h>
#include <TH1.h>

#include "lmmin.h"

namespace PixLib {

class PixFitResult;
class RawHisto;

class PixFitFitter_lmfit {
public:
	PixFitFitter_lmfit();
	virtual ~PixFitFitter_lmfit();

	typedef struct {
            const double *t;
            const double *y;
            double (*f) (double t, const double *par, const PixFitFitter_lmfit *fitterInstance);
            const PixFitFitter_lmfit *fitterInstance;
        } pixfit_lmcurve_data_struct;

struct ValidBins {
		int start;
		int end;
		int valid;
		int startsigma;
		int endsigma;
	};

       void analyzeData(std::shared_ptr<RawHisto> histo, ValidBins* validBins, const int& size, const int& pixelNumber);
	// LM fit
	double simpleerf(double x, const double *par) const;
	static double simpleerfWrapper(double x, const double *par, const PixFitFitter_lmfit *fitterInstance);
	static double matchLUT(double x);
	static bool interpolate(const std::vector <float> &x,const std::vector <float> &y, const float &target, float &result, bool debug=false);
	//Experimental derivative method to retreive 50% of the sigmoid, not used
	//static bool derivativeSigmoid(const std::vector <float> &x,const std::vector <float> &y, const float &target, float &result, bool debug = false);

        std::unique_ptr<PixFitResult> lmfit(int nthread, std::shared_ptr<RawHisto> histo);
        std::unique_ptr<PixFitResult> linearInterpolation(std::shared_ptr<RawHisto> histo);
        std::unique_ptr<PixFitResult> derivativeMethod(std::shared_ptr<RawHisto> histo);
private:
        static void lmcurve( int n_par, double *par, int m_dat, 
              const double *t, const double *y,
              double (*f)( double t, const double *par, const PixFitFitter_lmfit *fitterInstance ),
              const lm_control_struct *control,
              lm_status_struct *status, const PixFitFitter_lmfit *fitterInstance );
 	static void lmcurve_evaluate( const double *par, int m_dat, const void *data,
                       double *fvec, int *info );
        int inj_iterations;

	constexpr static const double cSqrt2 = 1.41421356237309504880f;
	constexpr static const double cInvSqrt6 = 0.40824829046f;

	/** Controls the verbosity of the fit output. */
	static const bool s_doverbose = false;
};
} /* end of namespace PixLib */

#endif /* PIXFITFITTER_LMFIT_H_ */
