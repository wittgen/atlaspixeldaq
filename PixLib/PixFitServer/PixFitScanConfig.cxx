/* @file PixFitScanConfig.cxx
 *
 *  Created on: Dec 17, 2013
 *      Author: mkretz, marx, nrosien
 */

#include <map>
#include <memory>
#include <ers/ers.h>

#include "PixFitScanConfig.h"
#include "rodHisto.hxx"
#include "rodHistoL12_addendum.hxx"
#include "PixFitUtility.h"

using namespace PixLib;

// parameter flav is for softcoding
PixFitScanConfig::PixFitScanConfig(PixGeometry flav, bool slaveEmu) {
       intermediate = intermediateType::INTERMEDIATE_NONE;
       binNumber = -1;
       m_slaveEmu = slaveEmu;
       NumChips = 8;
       if(flav.pixType() == PixGeometry::pixelType::FEI2_CHIP) NumChips = 128;

       /** Set the front end flavour
	* and get the number of its
	* rows and columns. */
       geo        = new PixGeometry(flav);
       NumRow     = geo->nRow();
       NumCol     = geo->nCol();

       /** Get Rod Histogrammer output format parameters. */
       getRodHistoParams();

       /** Set to zero just in case maskId gets called
	* directly after the calling of the constructor. */
       maskId = 0;
  
  //ERS_DEBUG(0, "Created PixFitScanConfig at " << std::hex << this)
}

PixFitScanConfig::~PixFitScanConfig() {
	//ERS_DEBUG("DEBUG: Deleted PixFitScanConfig at " << std::hex << this)
}

/* Returns how many bytes per pixel are sent from the slave via ethernet. (e.g. 1, 4, 8) */
int PixFitScanConfig::getBytesPerPixel() const {
	return m_readoutModeBytes.at(getReadoutMode());
}

/** @todo Make generic. */
int PixFitScanConfig::getNumOfPixels() const {
  int ms;

  // get the number of pixels of the front end
  int TotalNumPixelsOfFEI = NumRow*NumCol;

  if (m_slaveEmu) {
	  ms = this->emuConfig.masksteps;
  }
  else {
	  ms = params.scanMaskTotalSteps;
	  if(ms > 32)ms = 32;
  }

  int pixels = TotalNumPixelsOfFEI * this->getNumOfChips() / ms;

  return pixels;
}

int PixFitScanConfig::getInjections() const {
  if (m_slaveEmu) {
	  return this->emuConfig.injections;
  }
  else {
	  return params.scanRepetitions;
  }
}

int PixFitScanConfig::getWordsPerPixel() const {
	return m_scanTypeWords.at(findScanType());
}

PixFitScanConfig::scanType PixFitScanConfig::findScanType() const {
  if (m_slaveEmu) {
    return this->emuConfig.histoType;
  }
  else {
    return FitUtility::findScanType(params);
  }
}

PixFitScanConfig::readoutMode PixFitScanConfig::getReadoutMode() const {
  if (m_slaveEmu) {
	  return this->emuConfig.readoutType;
  }
  else {
	  if (findScanType() == scanType::TOT ) {
		return readoutMode::LONG_TOT;
	  }
	  else {
		return readoutMode::ONLINE_OCCUPANCY;
	  }
	  /** @todo Accommodate for these two readout modes as well */
	  //return readoutMode::OFFLINE_OCCUPANCY;
	  //return readoutMode::SHORT_TOT;
  }
}

EnumScanFitMethod::FitMethod PixFitScanConfig::getFitMetod() const {
  return static_cast<EnumScanFitMethod::FitMethod> (params.scanFitMethod);
}

int PixFitScanConfig::getNumOfBins() const {
	int bins = 1; // ana and digi hardcoded to 1 as getLoopVarNSteps gives 0
	if (intermediate != intermediateType::INTERMEDIATE_NONE) {
         /* For intermediate types we only use one bin. */
         bins = 1;
       } else if(doIntermediateHistos()) {
	  if(m_slaveEmu) {
	    bins = this->emuConfig.bins;
	  } else {
	    bins = params.scanLoop0Steps;
	  }
	}
	return bins;
}

int PixFitScanConfig::getNumOfTotalMaskSteps() const {
  if (m_slaveEmu) {
	  return this->emuConfig.masksteps;
  }
  else {
	  return params.scanMaskTotalSteps;
  }
}

/* This version is for PixFitPublisher */
int PixFitScanConfig::getNumOfMaskSteps() const{
  if (m_slaveEmu) {
	  return this->emuConfig.masksteps;
  }
  else {
	  return params.scanMaskSteps;
  }
}

int PixFitScanConfig::getNumOfChips() const { return NumChips; }

double PixFitScanConfig::getVcalfromBin(double i, bool isNoise) const{
  int const n = params.scanLoop0Steps;
  int const min = params.scanLoop0Min;
  int const max = params.scanLoop0Max;
  double vcal = min + ( (double)(max - min) / (n - 1) ) * i;

  // If this is the noise, then subtract off the min, so that the first step is not added to the noise.
  if(isNoise) vcal-=min;

  //ERS_LOG( "in getVcalfromBin = " << vcal << " min: " << min << " max: " << max << " n: "   << n );
  return vcal;
}

PixFitScanConfig::ScanIdType PixFitScanConfig::getScanId() const {
	return scanId;
}

bool PixFitScanConfig::doIntermediateHistos() const {
  /* Check if there are intermediate histos --> DSP processing on loop 0 */
  return (params.scanLoop0DSPProcessing && (params.scanLoop0Steps>1) );
}

void PixFitScanConfig::getRodHistoParams() {

  if (geo->pixType() == PixGeometry::FEI4_CHIP) {
//  Variables depending on rodHisto.hxx for FE-I4 RODs
    tot_result_bits = TOT_RESULT_BITS;
    totsqr_result_bits = TOTSQR_RESULT_BITS;
    twoword_tot_result_shift = TWOWORD_TOT_RESULT_SHIFT;
    twoword_totsqr_result_shift = TWOWORD_TOTSQR_RESULT_SHIFT;

  } else {  // The only other alternative at this point:
//  Variables depending on rodHistoL12_addendum.hxx for FE-I3 RODs
    tot_result_bits = TOT_RESULT_BITS_L12;
    totsqr_result_bits = TOTSQR_RESULT_BITS_L12;
    twoword_tot_result_shift = TWOWORD_TOT_RESULT_SHIFT_L12;
    twoword_totsqr_result_shift = TWOWORD_TOTSQR_RESULT_SHIFT_L12;
  }

}

void PixFitScanConfig::setScanParameters(scanParameters const & p){
  params = p;
}
