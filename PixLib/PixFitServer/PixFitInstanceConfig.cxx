/** @file PixFitInstanceConfig.cxx
 *
 *  Created on: Jul 14, 2014
 *      Author: mkretz
 *      Author: K. Potamianos <karolos.potamianos@cern.ch>
 *      Update: * 2014-VII-30: adding loading of fitFarmInstance from connectivity
 *      Update: Nick Dreyer 2015-V-20: config-file replaces hardcoding of slaveEmu parameters
 */

#include <string>
#include <map>
#include <vector>
#include <algorithm> // for sorting blacklist
#include <memory>

/* Includes for getting RAM size and IP addresses. */
#include <sys/sysinfo.h>
#include <arpa/inet.h>
#include <ifaddrs.h>

#include <boost/program_options.hpp>
#include <ers/ers.h>

#include "Config/Config.h"

#include "PixUtilities/PixISManager.h"

#include "PixConnectivity/PixConnectivity.h"
#include "PixConnectivity/PartitionConnectivity.h"
#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixConnectivity/RodBocConnectivity.h"


#include "PixFitInstanceConfig.h"

using namespace PixLib;

/* Initializing static const members of PixFitInstanceConfig class. */
const std::string PixFitInstanceConfig::fitServerVersion = "0.5";

PixFitInstanceConfig::PixFitInstanceConfig(std::string serverNameArg,
					   std::string partitionNameArg, std::string dbsrv_nameArg, std::string instanceIDArg,
					   bool slaveEmuArg, std::string slaveEmuConfigFile, 
					   std::string ifname, std::string idTagArg, std::string connTagArg, 
					   std::string cfgTagArg, std::string cfgMTagArg)
{
	this->serverName = serverNameArg;
	this->partitionName = partitionNameArg;
	this->instanceID = instanceIDArg;
	this->slaveEmu = slaveEmuArg;
        if(slaveEmu)this->configSlaveEmu(slaveEmuConfigFile);

        if(slaveEmu)
	  this->rodNetworkInterfaces.push_back("br100");  //  This is needed for slaveEmu in SR1 (running on Analysis2)
        else
	  this->rodNetworkInterfaces.push_back(ifname.c_str());

	this->dbsrv_name = dbsrv_nameArg;
	this->idTag = idTagArg;
	this->connTag = connTagArg;
	this->cfgTag = cfgTagArg;
	this->cfgMTag = cfgMTagArg;
}

PixFitInstanceConfig::~PixFitInstanceConfig() {

}

int PixFitInstanceConfig::getThreadingSettings() {
	return std::thread::hardware_concurrency();
}

std::map<std::string, std::string> PixFitInstanceConfig::getLocalIps() {
	std::map<std::string, std::string> Ips;

	ifaddrs *ifAddrs;
	ifaddrs *ifI;
	sockaddr_in *sockAddr;

	getifaddrs(&ifAddrs);
	/* Iterate over interfaces and collect interface names and IP addresses as strings. */
	for (ifI = ifAddrs; ifI != NULL; ifI = ifI->ifa_next) {
		if (ifI->ifa_addr->sa_family == AF_INET) {
			sockAddr = reinterpret_cast<sockaddr_in *>(ifI->ifa_addr);
			Ips[ifI->ifa_name] = inet_ntoa(sockAddr->sin_addr);
		}
	}

	freeifaddrs(ifAddrs);
	return Ips;
}

unsigned long PixFitInstanceConfig::getRam() {
	struct sysinfo info;
	sysinfo(&info);
	return (size_t) info.totalram * (size_t) info.mem_unit / (1024 * 1024);
}

std::map<std::string, std::string> PixFitInstanceConfig::getConfiguration() {
	std::map<std::string, std::string> config;

	IPCPartition ipcPartition(getPartitionName());

	// Todo: get values from environment
	std::string isServerName     = "PixelRunParams";
	PixDbServerInterface *dbServer;
	PixISManager* isManager;


	bool ready=false;
	int nTry=0;
	ERS_LOG("Trying to connect to DbServer with name " << dbsrv_name << "  ..... ");
	do {
		sleep(1);
		dbServer=new  PixDbServerInterface(&ipcPartition,dbsrv_name,PixDbServerInterface::CLIENT, ready);
		if(!ready) delete dbServer;
		else break;
		std::cout << " ..... attempt number: " << nTry << std::endl;
		nTry++;
	} while(nTry<10);
	if(!ready) {
	  ERS_INFO("Impossible to connect to DbServer " << dbsrv_name);
	  exit(-1);
	}
	ERS_LOG("Succesfully connected to DbServer " << dbsrv_name);

	dbServer->ready();

	try {
		isManager = new PixISManager(&ipcPartition, isServerName);
	}
    catch(PixISManagerExc &e) {
		ERS_INFO("Exception caught in " << __PRETTY_FUNCTION__ << ": " << e.getErrorLevel() <<
				"-" << e.getISmanName() << "-" << e.getExcName());
		return config;
	}
	// @todo: decide whether we want a catch all
	catch(...) {
		ERS_INFO("Unknown exception caught in " << __PRETTY_FUNCTION__ <<
				" while creating ISManager for IsServer " << isServerName);
		return config;
	}
	ERS_LOG("Succesfully connected to IsManager " << isServerName);

	//  read current tags
	try{
	  idTag   = isManager->read<std::string>("DataTakingConfig-IdTag");
	  connTag = isManager->read<std::string>("DataTakingConfig-ConnTag");
	  cfgTag  = isManager->read<std::string>("DataTakingConfig-CfgTag");
	  cfgMTag = isManager->read<std::string>("DataTakingConfig-ModCfgTag");
	  ERS_LOG("Default tags changed to "<<idTag<<"/"<<connTag<<"/"<<cfgTag+"/"<<cfgMTag);
	}
	catch (...) {
	  ERS_LOG("Cannot read default tags from IS, using default: "<<idTag<<"/"<<connTag<<"/"<<cfgTag+"/"<<cfgMTag);
	}
	
	PixConnectivity *conn;
	
	try{
	  conn = new PixConnectivity(dbServer, connTag, connTag, connTag, cfgTag, idTag, cfgMTag);
	  conn->loadConn();
	}
    catch(PixConnectivityExc &e) {
	  ERS_LOG("PixConnectivity exception caught in PixFitInstanceConfig: " << e.getDescr());
	  return config;
	}
	catch(...) { 
	  ERS_LOG("Unknown exception caught in PixFitInstanceConfig while creation PixConnectivity");
	  return config;
	}

	std::map<std::string, PartitionConnectivity*>::iterator it_partConn;
	std::map<std::string, RodCrateConnectivity*>::iterator it_rodCrateConn;
	std::map<int, RodBocConnectivity*>::iterator it_rodBocConn;
	for(it_partConn = conn->parts.begin(); it_partConn != conn->parts.end(); ++it_partConn) {
		for(it_rodCrateConn = it_partConn->second->rodCrates().begin(); it_rodCrateConn != it_partConn->second->rodCrates().end(); ++it_rodCrateConn) {
			for(it_rodBocConn = it_rodCrateConn->second->rodBocs().begin(); it_rodBocConn != it_rodCrateConn->second->rodBocs().end(); ++it_rodBocConn) {
				std::string rodBocName = it_rodBocConn->second->name();
				std::string fsName = ((ConfString&)it_rodBocConn->second->config()["Config"]["fitFarmInstance"]).value();
				std::cout << "Found RODBOC " << rodBocName ;
				std::cout << " with fit server " << fsName << std::endl;
				if(fsName==instanceID) config[rodBocName] = fsName;
			}
		}
	}

	// DO NOT COMMIT
	// Manual override due to problem with connectivity
	// Todo: remove
	// DO NOT COMMIT
	config["ROD_I1_S6"] = "FitServer-1";
	config["ROD_I1_S7"] = "FitServer-1";
	config["ROD_I1_S8"] = "FitServer-1";
	config["ROD_I1_S9"] = "FitServer-1";
	config["ROD_I1_S10"] = "FitServer-1";
	config["ROD_I1_S11"] = "FitServer-1";
	config["ROD_I1_S12"] = "FitServer-1";
	config["ROD_I1_S14"] = "FitServer-1";
	config["ROD_I1_S15"] = "FitServer-1";
	config["ROD_I1_S16"] = "FitServer-1";
	config["ROD_I1_S17"] = "FitServer-1";
	config["ROD_I1_S18"] = "FitServer-1";
	config["ROD_I1_S19"] = "FitServer-1";
	config["ROD_I1_S20"] = "FitServer-1";
	config["ROD_I1_S21"] = "FitServer-1";

	config["ROD_L1_S5"] = "FitServer-2";
	config["ROD_L1_S6"] = "FitServer-2";
	config["ROD_L1_S7"] = "FitServer-2";
	config["ROD_L1_S8"] = "FitServer-2";
	config["ROD_L1_S9"] = "FitServer-2";
	config["ROD_L1_S10"] = "FitServer-2";
	config["ROD_L1_S11"] = "FitServer-2";
	config["ROD_L1_S12"] = "FitServer-2";
	config["ROD_L1_S14"] = "FitServer-2";
	config["ROD_L1_S15"] = "FitServer-2";
	config["ROD_L1_S16"] = "FitServer-2";
	config["ROD_L1_S17"] = "FitServer-2";
	config["ROD_L1_S18"] = "FitServer-2";
	config["ROD_L1_S19"] = "FitServer-2";
	config["ROD_L1_S20"] = "FitServer-2";
	config["ROD_L1_S21"] = "FitServer-2";

	config["ROD_L2_S5"] = "FitServer-3";
	config["ROD_L2_S6"] = "FitServer-3";
	config["ROD_L2_S7"] = "FitServer-3";
	config["ROD_L2_S8"] = "FitServer-3";
	config["ROD_L2_S9"] = "FitServer-3";
	config["ROD_L2_S10"] = "FitServer-3";
	config["ROD_L2_S11"] = "FitServer-3";
	config["ROD_L2_S12"] = "FitServer-3";
	config["ROD_L2_S14"] = "FitServer-3";
	config["ROD_L2_S15"] = "FitServer-3";
	config["ROD_L2_S16"] = "FitServer-3";
	config["ROD_L2_S17"] = "FitServer-3";
	config["ROD_L2_S18"] = "FitServer-3";
	config["ROD_L2_S19"] = "FitServer-3";
	config["ROD_L2_S20"] = "FitServer-3";
	config["ROD_L2_S21"] = "FitServer-3";

	config["ROD_L3_S5"] = "FitServer-4";
	config["ROD_L3_S6"] = "FitServer-4";
	config["ROD_L3_S7"] = "FitServer-4";
	config["ROD_L3_S8"] = "FitServer-4";
	config["ROD_L3_S9"] = "FitServer-4";
	config["ROD_L3_S10"] = "FitServer-4";
	config["ROD_L3_S11"] = "FitServer-4";
	config["ROD_L3_S12"] = "FitServer-4";
	config["ROD_L3_S14"] = "FitServer-4";
	config["ROD_L3_S15"] = "FitServer-4";
	config["ROD_L3_S16"] = "FitServer-4";
	config["ROD_L3_S17"] = "FitServer-4";
	config["ROD_L3_S18"] = "FitServer-4";
	config["ROD_L3_S19"] = "FitServer-4";
	config["ROD_L3_S20"] = "FitServer-4";
	config["ROD_L3_S21"] = "FitServer-4";

	config["ROD_L4_S5"] = "FitServer-5";
	config["ROD_L4_S6"] = "FitServer-5";
	config["ROD_L4_S7"] = "FitServer-5";
	config["ROD_L4_S8"] = "FitServer-5";
	config["ROD_L4_S9"] = "FitServer-5";
	config["ROD_L4_S10"] = "FitServer-5";
	config["ROD_L4_S11"] = "FitServer-5";
	config["ROD_L4_S12"] = "FitServer-5";
	config["ROD_L4_S14"] = "FitServer-5";
	config["ROD_L4_S15"] = "FitServer-5";
	config["ROD_L4_S16"] = "FitServer-5";
	config["ROD_L4_S17"] = "FitServer-5";
	config["ROD_L4_S18"] = "FitServer-5";
	config["ROD_L4_S19"] = "FitServer-5";
	config["ROD_L4_S20"] = "FitServer-5";
	config["ROD_L4_S21"] = "FitServer-5";

	config["ROD_B2_S5"] = "FitServer-6";
	config["ROD_B2_S6"] = "FitServer-6";
	config["ROD_B2_S7"] = "FitServer-6";
	config["ROD_B2_S8"] = "FitServer-6";
	config["ROD_B2_S9"] = "FitServer-6";
	config["ROD_B2_S10"] = "FitServer-6";
	config["ROD_B2_S11"] = "FitServer-6";
	config["ROD_B2_S12"] = "FitServer-6";
	config["ROD_B2_S14"] = "FitServer-6";
	config["ROD_B2_S15"] = "FitServer-6";
	config["ROD_B2_S16"] = "FitServer-6";
	config["ROD_B2_S17"] = "FitServer-6";
	config["ROD_B2_S18"] = "FitServer-6";
	config["ROD_B2_S19"] = "FitServer-6";
	config["ROD_B2_S20"] = "FitServer-6";
	config["ROD_B2_S21"] = "FitServer-6";

	config["ROD_B3_S10"] = "FitServer-6";
	config["ROD_B3_S11"] = "FitServer-6";
	config["ROD_B3_S12"] = "FitServer-6";
	config["ROD_B3_S14"] = "FitServer-6";
	config["ROD_B3_S15"] = "FitServer-6";
	config["ROD_B3_S16"] = "FitServer-6";

	config["ROD_D1_S9"] = "FitServer-7";
	config["ROD_D1_S10"] = "FitServer-7";
	config["ROD_D1_S11"] = "FitServer-7";
	config["ROD_D1_S12"] = "FitServer-7";
	config["ROD_D1_S14"] = "FitServer-7";
	config["ROD_D1_S15"] = "FitServer-7";
	config["ROD_D1_S16"] = "FitServer-7";
	config["ROD_D1_S17"] = "FitServer-7";

	config["ROD_D2_S9"] = "FitServer-7";
	config["ROD_D2_S10"] = "FitServer-7";
	config["ROD_D2_S11"] = "FitServer-7";
	config["ROD_D2_S12"] = "FitServer-7";
	config["ROD_D2_S14"] = "FitServer-7";
	config["ROD_D2_S15"] = "FitServer-7";
	config["ROD_D2_S16"] = "FitServer-7";
	config["ROD_D2_S17"] = "FitServer-7";

	//SR1
	config["ROD_C1_S7"]  = "FitServer-SR1";
	config["ROD_C1_S17"] = "FitServer-SR1";

	config["ROD_C3_S8"]  = "FitServer-SR1";
	config["ROD_C3_S11"] = "FitServer-SR1";
	config["ROD_C3_S15"] = "FitServer-SR1";
	config["ROD_C3_S18"] = "FitServer-SR1";

	config["ROD_C4_S7"] = "FitServer-SR1";
	config["ROD_C4_S18"] = "FitServer-SR1";

	// DO NOT COMMIT

	delete dbServer;
	delete isManager;

	/*
	config["ROD_C2_S9"] = "FitServer-1";
	config["ROD_C2_S16"] = "FitServer-1";
	//config["ROD_C0_S2"] = "FitServer-1";
	//config["ROD_C2_S0"] = "FitServer-1";
	//config["ROD_C2_S15"] = "FitServer-1";
	//config["ROD_C1_S0"] = "FitServer-2";
	config["ROD_I1_S6"] = "FitServer-1";
	config["ROD_I1_S7"] = "FitServer-1";
	config["ROD_I1_S8"] = "FitServer-1";
	config["ROD_I1_S9"] = "FitServer-1";
	config["ROD_I1_S10"] = "FitServer-1";
	config["ROD_I1_S11"] = "FitServer-1";
	config["ROD_I1_S12"] = "FitServer-1";
	config["ROD_I1_S14"] = "FitServer-1";
	config["ROD_I1_S15"] = "FitServer-1";
	config["ROD_I1_S16"] = "FitServer-1";
	config["ROD_I1_S17"] = "FitServer-1";
	config["ROD_I1_S18"] = "FitServer-1";
	config["ROD_I1_S19"] = "FitServer-1";
	config["ROD_I1_S20"] = "FitServer-1";
	config["ROD_I1_S21"] = "FitServer-1";
	*/

	return config;
}

bool PixFitInstanceConfig::usingSlaveEmu() const {
	return slaveEmu;
}

std::string PixFitInstanceConfig::getInstanceId() const {
	return instanceID;
}

const std::string& PixFitInstanceConfig::getPartitionName() const {
	return partitionName;
}

const std::vector<std::string>& PixFitInstanceConfig::getRodNetworkInterfaces() const {
	return rodNetworkInterfaces;
}

const std::string& PixFitInstanceConfig::getServerName() const {
	return serverName;
}

int PixFitInstanceConfig::configSlaveEmu(std::string slaveEmuConfigFile) {
  const char* emuConfigFile = slaveEmuConfigFile.c_str();

  namespace po = boost::program_options;

  po::options_description config("Emulator Options");

  config.add_options()
    ("root-file,f", po::value<std::string>()->required(),
                    "root file for FitServer only")
    ("histo-type,t", po::value<std::string>()->required(),
                     "type of histograms for simulation {OCC, THR, TOT}\n(TOT not fully functional!)")
    ("source,s", po::value<std::string>()->required(),
                 "source of data (FEI4 or FEI3)")
    ("use-rod", po::value<int>()->required(),
                "Is ROD slave FPGA source of histogram emulation? (0 = No, 1 = Yes)")
    ("nChips,n", po::value<int>()->required(),
                "number of chips per histo unit in scan")
    ("bins,b", po::value<int>()->required(),
               "number of bins per histogram")
    ("injections", po::value<int>()->required(),
                   "injections per charge step")
    ("mask-step,m", po::value<int>()->required(),
                    "mask stepping mode to be used (0 through 5)")
    ("readout-type,r", po::value<int>()->required(),
                       "which histogrammer readout type is used (0, 1, 2, 3)")
    ("mod-mask", po::value<std::string>()->required(),
                       "hex mask for modules on ROD in scan (only needed by FitServer)")
    ("crate", po::value<int>()->required(),
                       "which crate for FitServer (only needed by FitServer)")
    ("rod", po::value<int>()->required(),
                       "which rod for FitServer used (only needed by FitServer)")

                        ;
  po::options_description config_options;
  config_options.add(config);
  po::variables_map vm;

  try{
    po::store(po::parse_config_file< char >(emuConfigFile, config_options), vm);
  }
  catch(po::error &e) {
    std::cout << "Error while parsing " << emuConfigFile <<  " : " << e.what() << std::endl;
    return 1;
  }
  if(vm.count("root-file") &&
    vm.count("histo-type") &&
    vm.count("source") &&
    vm.count("use-rod") &&
    vm.count("nChips") &&
    vm.count("bins") &&
    vm.count("injections") &&
    vm.count("mask-step") &&
    vm.count("readout-type") &&
    vm.count("mod-mask") &&
    vm.count("crate") &&
    vm.count("rod"))

  {
    std::cout << "Slave Emulator configuration file: " << emuConfigFile << "." << std::endl;
    if (vm["histo-type"].as<std::string>() == "TOT") {
      emuConfig.histoType = PixFitScanConfig::scanType::TOT;
    }
    else if (vm["histo-type"].as<std::string>() == "THR"){
      emuConfig.histoType = PixFitScanConfig::scanType::SCURVE;
    }
    else if (vm["histo-type"].as<std::string>() == "OCC"){
      emuConfig.histoType = PixFitScanConfig::scanType::OCCUPANCY;
    }
    else {
      std::cout << "Unknown histogram type, choose either OCC, THR or TOT. Exiting." << std::endl;
      return 1;
    }

    emuConfig.rootFile=vm["root-file"].as<std::string>();

    emuConfig.histoSource=vm["source"].as<std::string>();

    emuConfig.flavour = PixGeometry::FEI4_CHIP;
    if(emuConfig.histoSource == "FEI3") emuConfig.flavour = PixGeometry::FEI3_CHIP;

    emuConfig.useRod = vm["use-rod"].as<int>();

    //  NOTE:  emuConfig.nChips is not actually used in FitServer code.  It's only use is in slaveEmu where it limits the number
    emuConfig.nChips = 8; //                                             of pixels for which fake data is generated there.
    if(emuConfig.flavour == PixGeometry::FEI3_CHIP)emuConfig.nChips = 128;
 
    emuConfig.bins = vm["bins"].as<int>();
 
    emuConfig.injections = vm["injections"].as<int>();
 
    emuConfig.masksteps = pow(2,vm["mask-step"].as<int>());

    switch(vm["readout-type"].as<int>()){
      case 0: emuConfig.readoutType = PixFitScanConfig::readoutMode::ONLINE_OCCUPANCY; break;
      case 1: emuConfig.readoutType = PixFitScanConfig::readoutMode::OFFLINE_OCCUPANCY; break;
      case 2: emuConfig.readoutType = PixFitScanConfig::readoutMode::SHORT_TOT; break;
      case 3: emuConfig.readoutType = PixFitScanConfig::readoutMode::LONG_TOT; break;
      default:
        std::cout << "Unknown readout type, choose either 0, 1, 2 or 3. Exiting" << std::endl;
        return 1;
        break;
    }

    emuConfig.modMask=vm["mod-mask"].as<std::string>();

    emuConfig.crate=vm["crate"].as<int>();

    emuConfig.rod=vm["rod"].as<int>();

    std::cout << "histo-type=" << (int)(emuConfig.histoType) << std::endl;
    std::cout << "root-file=" << emuConfig.rootFile << std::endl;
    std::cout << "source=" << emuConfig.histoSource << std::endl;
    std::cout << "nChips=" << emuConfig.nChips << std::endl;
    std::cout << "use-rod=" << emuConfig.useRod << std::endl;
    std::cout << "bins=" << emuConfig.bins << std::endl;
    std::cout << "injections=" << emuConfig.injections << std::endl;
    std::cout << "mask-step=" << emuConfig.masksteps << std::endl;
    std::cout << "readout-type=" << (int)(emuConfig.readoutType) << std::endl;
    std::cout << "mod-mask=" << emuConfig.modMask << std::endl;
    std::cout << "crate=" << emuConfig.crate << std::endl;
    std::cout << "rod=" << emuConfig.rod << std::endl;
    return 0;
  }else{
    std::cout << "Error while parsing " << emuConfigFile
              << ": Not all required options found." << std::endl;
    return 1;
  }
}

/* BlackList implementation. */

BlackList::BlackList() {
}

BlackList::~BlackList() {
}

bool BlackList::isScanIdListed(PixFitScanConfig::ScanIdType scanId) {
	/* Get read access to vector. */
	std::shared_lock<std::shared_mutex> lock(m_mutex);

	/* Search for scan ID. Assume that the vector is sorted! */
	return (std::binary_search(m_blackList.begin(), m_blackList.end(), scanId));
}

bool BlackList::investigateWorkObject(PixFitWorkPackage const * const workPackage) {
	return isScanIdListed(workPackage->getScanId());
}

void BlackList::addScanId(PixFitScanConfig::ScanIdType scanId) {
	/* Get write access to vector. */
	std::unique_lock<std::shared_mutex> lock(m_mutex);

	/* Insert scan ID. */
	m_blackList.push_back(scanId);

	/* Sort vector. */
	std::sort(m_blackList.begin(), m_blackList.end());

	ERS_LOG("Added scan with ID " << scanId << " to blacklist, now contains "
			<< m_blackList.size() << " items.");
}


/* ScanList implementation. */

ScanList::ScanList() {
}

ScanList::~ScanList() {
}

void ScanList::setScanCount(int fitFarmId, int scanCount) {
	/* Get lock on map. */
	std::unique_lock<std::shared_mutex> lock(m_mutex);

	/* Element should not exist. */
	assert(m_scanMap.count(fitFarmId) == 0);

	m_scanMap.insert(std::pair<int, int>(fitFarmId, scanCount));
}


int ScanList::reduceScanCount(int fitFarmId) {
	/* Get lock on map. */
	std::unique_lock<std::shared_mutex> lock(m_mutex);

	/* Element should exist! */
	assert(m_scanMap.count(fitFarmId) > 0);

	m_scanMap[fitFarmId]--;

	/* Remove element if count reached zero. */
	if (m_scanMap[fitFarmId] == 0) {
		m_scanMap.erase(fitFarmId);
		return 1;
	}
	else {
		return 0;
	}
}
