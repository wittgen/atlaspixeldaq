/** @file PixFitAssembler.cxx
 *
 *  Created on: Mar 18, 2014
 *      Author: mkretz
 */

#include <cassert>
#include <map>
#include <memory>
#include <fstream>


#include <TF1.h>
#include <TH1.h>
#include <TH2.h>

#include <ers/ers.h>

#include "PixFitAssembler.h"
#include "PixFitResult.h"
#include "RawHisto.h"
#include "PixFitManager.h" // for ROOT lock

// #define DEBUG_ND
// #define DEBUG_ND_THR

#ifdef DEBUG_ND_THR
#define DEBUG_FILE "/home/ndreyer/ffa.log"
#endif

using namespace PixLib;

#ifdef DEBUG_ND_THR
  std::fstream ffa;
#endif

PixFitAssembler::PixFitAssembler(PixFitWorkQueue<PixFitResult> *resultQueueArg,
		PixFitWorkQueue<PixFitResult> *publishQueueArg, PixFitInstanceConfig *instanceConfig) {
	resultQueue = resultQueueArg;
	publishQueue = publishQueueArg;
	m_threadName = "assembler";
	m_instanceConfig = instanceConfig;
	m_cleanFlag = false;
	instanceConfig->assembler = this; // register with instanceConfig
	m_hNumber = 0; //initialise to 0
}

void PixFitAssembler::loop() {

	while (!PixFitManager::stop_all) {
		/* Get result object. */
		auto result = resultQueue->getWork();
                if(resultQueue->stopping()) return;

		/* Put result in hold. */
		auto scanCfg = result->getScanConfig();
		OuterKey outerKey = std::make_pair(scanCfg->scanId, scanCfg->binNumber);
		OuterMap::iterator outerIt = m_results.find(outerKey);
		HistoUnit histo = scanCfg->histogrammer;

		/* Check if scanId is already present. */
		if (outerIt == m_results.end()) {
			InnerMap iMap;
			iMap.insert(std::make_pair(histo, ResultsVector(scanCfg->getNumOfMaskSteps())));
			m_results.insert(std::make_pair(outerKey, std::move(iMap)));
		}
		/* Check if histogramming unit is already present. */
		else if (outerIt->second.find(histo) == outerIt->second.end()) {
			outerIt->second.insert(std::make_pair(histo, ResultsVector(scanCfg->getNumOfMaskSteps())));
		}
		/* Check if maskID is already present in vector, which should never be the case. */
		else {
			/* Crosscheck vector size and maskId. */
			assert(outerIt->second[histo].size() == static_cast<size_t>(scanCfg->getNumOfMaskSteps()));
			assert(outerIt->second[histo].size() > static_cast<size_t>(scanCfg->maskId));

			/* Error: Result does already exist! */
			if (outerIt->second[histo].at(scanCfg->maskId) != nullptr) {
				ERS_INFO("ERROR: Trying to put already existing PixFitResult object in hold.");
				ERS_INFO(histo.makeHistoString() << ": Scan ID: " << scanCfg->scanId << " binNumber: " << scanCfg->binNumber);
				return;
			}
		}

		/* Insert result into vector. */
		outerIt = m_results.find(outerKey);
		outerIt->second[histo].at(scanCfg->maskId) = std::move(result);

		/* Check if vector is complete for reassembly. */
		/*
		unsigned int resultsCount = 0;
		for (auto& resultEntry : outerIt->second[histo]) {
		  if (resultEntry)
		    resultsCount++;
		}
                */
		/* Reassemble result if all data is there. */
		//ResultsVector resVec;

		auto isDone = std::none_of(std::cbegin(outerIt->second[histo]), std::cend(outerIt->second[histo]),
				           [](auto const & entry) -> bool {return !entry;});
		if(isDone) {
                  ResultsVector resVec;
		  reassemble(outerIt->second[histo],resVec);
			/* Put reassembled result(s) in queue. */
			for (auto& vecIt : resVec) {	
			  publishQueue->addWork(std::move(vecIt));
			}
			
			/* Remove vector (and possibly clean up map). */
			outerIt->second.erase(histo);
			/* Check if other work packages belonging to this scanId exist. */
			if (outerIt->second.empty()) {
			  m_results.erase(outerKey);
			}
		}

		/* Check if we need to clean m_results. */
		std::lock_guard<std::mutex> lock(m_cleanMutex);
		if (m_cleanFlag) {
			cleanAssembler();
			m_cleanFlag = false;
		}
	}
  PIX_LOG("Exiting assembler...");

}

void PixFitAssembler::printMap(int depth) {
	std::cout << "*** Assembler hold ***" << std::endl;
	std::cout << "Buffered Scan IDs: ";
	for (auto& outer : m_results) {
		std::cout << outer.first.first << " ";
	}
	std::cout << std::endl;

	if (depth >= 1) {

		/* Print ScanIDs. */
		for (auto& outer : m_results) {
			std::cout << outer.first.first << " / " << outer.first.second << ":";

			/* Print HistoUnits. */
			for (auto& inner : outer.second) {
				inner.first.printHistoUnit();

				/* Print valid data packages. */
				if (depth >= 2) {
					for (auto vecIt = inner.second.begin(); vecIt != inner.second.end();
							vecIt++) {

						/* Check if data is valid at position and print position in vector. */
						if (vecIt->get() != nullptr) {
							std::cout << vecIt - inner.second.begin() << " ";
						}
					}
					std::cout << std::endl << "--------------------------------" << std::endl;
				}
			}
		}
	}
}

/* First creates the needed ROOT histogram(s) and then fills them with values from the RawHisto
 * or PixFitResult taking mask stepping into account. */

void PixFitAssembler::reassemble(ResultsVector& resVec,ResultsVector& tmpResultVec) {

	auto pScanConfig = resVec.at(0)->getScanConfig();
	int const crate = pScanConfig->histogrammer.crate;
	int const rod = pScanConfig->histogrammer.rod;
	int const slave = pScanConfig->histogrammer.slave;
	int const histo = pScanConfig->histogrammer.histo;

	ERS_DEBUG(0, pScanConfig->histogrammer.makeHistoString() << ": Reassembling result with scanId " << 
	    pScanConfig->scanId << " and binNumber " << pScanConfig->binNumber);

	ERS_DEBUG(0, "Vector contains " << resVec.size() << " object(s) for "
			<< pScanConfig->getNumOfMaskSteps() << " mask step(s).");
	
	PixFitScanConfig::scanType const scanType = pScanConfig->findScanType();
	PixFitScanConfig::intermediateType const intermediateType = pScanConfig->intermediate;
	int const maskSteps = pScanConfig->getNumOfMaskSteps();
	int const numChips = pScanConfig->getNumOfChips();

	
	assert(resVec.size() == static_cast<size_t>(maskSteps));

	/* Locking ROOT for the next two loops. */
	std::lock_guard<std::mutex> lock(PixFitManager::root_m);
	//To make the histograms unique
	m_hNumber++; 
	for (int chip = 0; chip < numChips; chip++) {
		auto tmpResult = std::make_unique<PixFitResult>(pScanConfig);
		tmpResult->chipId = chip;
		int const ncol = tmpResult->getScanConfig()->NumCol;
		int const nrow = tmpResult->getScanConfig()->NumRow;

		/* k is attached to name and title of histograms to make them unique. */
		TString k = Form("_%d_%d-%d-%d-%d-%d",m_hNumber,crate, rod, slave, histo, chip);

		/* Order is important, check for INTERMEDIATE types first! */
		if (intermediateType == PixFitScanConfig::intermediateType::INTERMEDIATE_OCCUPANCY) {
			int const bin = tmpResult->getScanConfig()->binNumber;
			tmpResult->histo_occ = prepareHisto(ncol, nrow, "occupancy-", k, bin);
		} else if (intermediateType == PixFitScanConfig::intermediateType::INTERMEDIATE_TOT) {
			int const bin = tmpResult->getScanConfig()->binNumber;
			tmpResult->histo_totmean = prepareHisto(ncol, nrow, "totmean-", k, bin);
			tmpResult->histo_totsum = prepareHisto(ncol, nrow, "totsum-", k, bin);
			tmpResult->histo_totsum2 = prepareHisto(ncol, nrow, "totsum2-", k, bin);
			tmpResult->histo_totsigma = prepareHisto(ncol, nrow, "totsigma-", k, bin);
			tmpResult->histo_occ = prepareHisto(ncol, nrow, "occ", k, bin);
		} else if (scanType == PixFitScanConfig::scanType::OCCUPANCY && !pScanConfig->doIntermediateHistos() ) {
			tmpResult->histo_occ = prepareHisto(ncol, nrow, "occupancy-", k);
		} else if (scanType == PixFitScanConfig::scanType::SCURVE) {
			TString histoName;

			histoName= "threshold-" + k;
			tmpResult->histo_thresh = std::make_unique<TH1F>(histoName, histoName, 1000, 0., tmpResult->getScanConfig()->getNumOfBins());
			histoName = "noise-" + k;
			tmpResult->histo_noise = std::make_unique<TH1F>(histoName, histoName, 100, 0., 10.);
			histoName = "chi2-" + k;
			tmpResult->histo_chi2 = std::make_unique<TH1F>(histoName, histoName, 25, 0., 50.);
			histoName = "threshold2D-" + k;
			tmpResult->histo_thresh2D = std::make_unique<TH2F>(histoName, histoName, ncol, 0, ncol, nrow, 0, nrow);
			histoName = "noise2D-" + k;
			tmpResult->histo_noise2D = std::make_unique<TH2F>(histoName, histoName, ncol, 0, ncol, nrow, 0, nrow);
			histoName = "chi2_2D-" + k;
			tmpResult->histo_chi2_2D = std::make_unique<TH2F>(histoName, histoName, ncol, 0, ncol, nrow, 0, nrow);
		}
		else if (scanType == PixFitScanConfig::scanType::TOT && !pScanConfig->doIntermediateHistos()) {
			tmpResult->histo_totmean = prepareHisto(ncol, nrow, "totmean-", k);
			tmpResult->histo_totsum = prepareHisto(ncol, nrow, "totsum-", k);
			tmpResult->histo_totsum2 = prepareHisto(ncol, nrow, "totsum2-", k);
			tmpResult->histo_totsigma = prepareHisto(ncol, nrow, "totsigma-", k);
			tmpResult->histo_occ = prepareHisto(ncol, nrow, "occ", k);
		}
		//Do nothing for OCCUPANCY AND TOT with intermediate histos

		tmpResultVec.push_back(std::move(tmpResult));
	}
	

#ifdef DEBUG_ND_THR
	if(!ffa.is_open())ffa.open(DEBUG_FILE, std::fstream::out);
#endif

	/* Fill full chips in case of mask stepping (dependency on injection pattern!). */
	for (auto& result : resVec) {
		if ( (scanType == PixFitScanConfig::scanType::OCCUPANCY && !pScanConfig->doIntermediateHistos()) || intermediateType == PixFitScanConfig::intermediateType::INTERMEDIATE_OCCUPANCY ){
		  for (int j = 0; j < result->rawHisto->getWords(); ++j) {
		    int chip, row, col;
		    if(getRowColumn(j,chip,row,col,result->getScanConfigRaw())) {
		      tmpResultVec[chip]->histo_occ->Fill(col,row,result->rawHisto->getRawData()[j]); 
		    } else {
		      //Skip the filtered out chips in the 64 mask steps
		      continue;
		    }
		  }
		} else if (scanType == PixFitScanConfig::scanType::SCURVE ) {
		        int numOfPixels =  (result->getScanConfig()->getNumOfPixels());
		        int offsetChi2 = numOfPixels * 2;  // chi2 values are behind mu/sigma value pairs in the array
		        for (int j = 0; j < 2 * numOfPixels; j+=2) {
			  int chip, row, col, j2 = j/2;
			  if(getRowColumn(j2,chip,row,col,result->getScanConfigRaw())) {
			    if (result->thresh_array[j] > -1 ) {
			      tmpResultVec[chip]->histo_thresh->Fill(result->thresh_array[j]);
			      tmpResultVec[chip]->histo_noise->Fill(result->thresh_array[j+1]);
			      // only doing bin to vcal conversion for 2D histos for now
			      tmpResultVec[chip]->histo_thresh2D->Fill(col, row, result->getScanConfig()->getVcalfromBin(result->thresh_array[j]));
			      tmpResultVec[chip]->histo_noise2D->Fill(col, row, result->getScanConfig()->getVcalfromBin(result->thresh_array[j+1],true)); // make sure the scan start is not added to the noise
			      tmpResultVec[chip]->histo_chi2->Fill(result->thresh_array[offsetChi2 + j2]);
  			      tmpResultVec[chip]->histo_chi2_2D->Fill(col, row, result->thresh_array[offsetChi2 + j2]);
			  }
			} else {
                          continue;
                        } // else-pixels invalid
                      } //for-loop
		}
		else if ( (scanType == PixFitScanConfig::scanType::TOT && !pScanConfig->doIntermediateHistos()) ||
				intermediateType == PixFitScanConfig::intermediateType::INTERMEDIATE_TOT) {
		  //#ifdef DEBUG_ND
		  //std::cout << "TOT XxXxXxX  result->rawHisto->getWords(): " << result->rawHisto->getWords() << std::endl;
		  //#endif
		  for (int j = 0; j < result->rawHisto->getWords() / result->getScanConfig()->getWordsPerPixel(); ++j) {
		    int chip, row, col;
		    if(getRowColumn(j,chip,row,col,result->getScanConfigRaw())) {
		      /* Compute totmean and totsigma. */
		      double occ = (*(result)->rawHisto)(j,0,0);
		      double tot = (*(result)->rawHisto)(j,0,1);
		      double tot2 = (*(result)->rawHisto)(j,0,2);
		      double totmean = 0;
		      double totsigma = 0;
		      if(occ == 1) {
		        totmean = tot;
		      } else if (occ > 1) {
		        totmean = tot/occ;
		        if(tot2 > 0) totsigma = sqrt( (tot2 - occ*totmean*totmean)/(occ - 1) );
		      } 
		      /** @todo: Put this into some bad pixel histo. */
		      
		      tmpResultVec[chip]->histo_totmean->Fill(col, row, totmean);
		      tmpResultVec[chip]->histo_totsum->Fill(col, row, tot);
		      tmpResultVec[chip]->histo_totsum2->Fill(col, row, tot2);
		      tmpResultVec[chip]->histo_totsigma->Fill(col, row, totsigma);
		      tmpResultVec[chip]->histo_occ->Fill(col, row, occ);
		    } else {
                      continue;
                    }
		  }
		}
		//Do nothing for OCCUPANCY AND TOT with intermediate histos
	}
}

//** Faster Version */
bool PixFitAssembler::getRowColumn(unsigned int j, int &chip, int &row, int &col, PixFitScanConfig const * const scanConfig)
{
  int const maskStep = scanConfig->getNumOfTotalMaskSteps();
  //The Histogrammer only knows 32 mask steps. Therefore we need to filter out all the mask steps bigger than 32 and shift accordingly
  int FWmaskStep = maskStep;
  if (maskStep > 32) FWmaskStep=32;
  int const maskId = scanConfig->maskId;
  int FWmaskId = maskId;
  if (maskId > 31) FWmaskId -=32;
  int const nrow = scanConfig->NumRow;
  int const ncol = scanConfig->NumCol;
  chip = j / ((ncol * nrow) / FWmaskStep); //  was j / ((ncol * nrow) / readoutSteps); TODO: CHECK THIS!
  row = trunc((j - chip * (ncol * nrow) / FWmaskStep) / double(ncol)) * FWmaskStep;
  col = (j % ncol);
  
  if (j % 2 == 0) row = row + FWmaskId;
  else row = row + FWmaskStep - 1 - FWmaskId;

  //Filtering in order to get the proper hits
  
  if(maskStep < 33 || ((maskId < 32 && ((col%2 == 0 && row%64 < 32) || (col%2 == 1 && row%64 > 31))) || //  This does the part of 64 step
                       (maskId > 31 && ((col%2 == 0 && row%64 > 31) || (col%2 == 1 && row%64 < 32))))) {
      //This pixels have to be saved
      return true;
    } else {
      //These pixels have to be filtered out
      return false;
    }
}

void PixLib::PixFitAssembler::setCleanFlag() {
	std::lock_guard<std::mutex> lock(m_cleanMutex);
	m_cleanFlag = true;
}

bool PixFitAssembler::cleanAssembler() {
	bool erasedSomething = false;

	/* Iterate over map and remove inner maps matching scan IDs. */
	OuterMap::iterator outerIt = m_results.begin();
	while (outerIt != m_results.end()) {
		if (m_instanceConfig->blacklist.isScanIdListed(outerIt->first.first)) {
			m_results.erase(outerIt++);
			erasedSomething = true;
		}
		else {
			outerIt++;
		}
	}
	return erasedSomething;
}


std::unique_ptr<TH2F> PixFitAssembler::prepareHisto(int ncol, int nrow, std::string const & name, TString id, int bin) {
    TString histoName;
    if (bin >= 0) {
	histoName = name + id + "-" + std::to_string(bin);
    } else {
	histoName = name + id;
    }
    
    auto histo = std::make_unique<TH2F>(histoName, histoName, ncol, 0, ncol, nrow, 0, nrow);
    histo->GetXaxis()->SetTitle("Column");
    histo->GetYaxis()->SetTitle("Row");
    return histo;
}

void PixFitAssembler::stopWork()
{
  resultQueue->stopWork();
}
