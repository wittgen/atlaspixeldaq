/*
 * PixFitFitter_lmfit.cxx
 *
 *  Created on: Dec 3, 2013
 *      Author: mkretz, marx
 */

#include <iostream>
#include <memory>
#include <fstream>

#include <oh/OHRootProvider.h>

#include <TF1.h>
#include <TFile.h>
#include <TCanvas.h>

#include "lmmin.h"

#include "PixFitFitter_lmfit.h"
#include "RawHisto.h"
#include "PixFitManager.h" // for global locks
#include "PixFitResult.h"

#include <boost/asio.hpp>

// #define DEBUG_ND

#ifdef DEBUG_ND
#define DEBUG_FILE "/home/ndreyer/fff.log"
#endif

using namespace PixLib;

int getRow(unsigned int j);

#ifdef DEBUG_ND
  std::fstream fff;
#endif

PixFitFitter_lmfit::PixFitFitter_lmfit() {
}

PixFitFitter_lmfit::~PixFitFitter_lmfit() {
}

// determines range of "good" data for a single pixel
void PixFitFitter_lmfit::analyzeData(std::shared_ptr<RawHisto> histo, ValidBins* validBins, const int& size, const int& pixelNumber) {
	int i = 0;
	int start = 0;
	int end = 0;
	int startsigma = 0;
	int endsigma = 0;
	double a0_guess, a1_guess, a2_guess;

	// get first bin > 0
	for (i = 0; i < size; i++) {
		if ((*histo)(pixelNumber, i, 0) != 0.0) {
			if (i > 0)
				start = i - 1; // include one zero datapoint
			else
				start = 0;
			break;
		}
	}

	// guess plateau
	a0_guess = 0.999 * (*histo)(pixelNumber, size - 1, 0);
	//a0_guess = 0.999 * inj_iterations;  //alternative
	for (i = start; i < size; i++) {
	  if ((*histo)(pixelNumber, i, 0) >= a0_guess) {
	    end = i;
	    break;
	  }
	}

	// guess lower and upper boundary for sigma
	a1_guess = 0.16 * (*histo)(pixelNumber, size - 1, 0);
	a2_guess = 0.84 * (*histo)(pixelNumber, size - 1, 0);
	//a1_guess = 0.16 * inj_iterations; //alternative
	//a2_guess = 0.84 * inj_iterations; //alternative
	
	// guess the DSP way - find the range over which the data crosses the 16% and 84% points
	int hi1 = 0; int hi2 = 0;
	int lo1 = 0; int lo2 = 0;
	int j = size - 1;
	for(int k = 0; k < size ;) {
	  if(!lo1 && ((*histo)(pixelNumber, k, 0) >= a1_guess)) lo1 = k;
	  if(!lo2 && ((*histo)(pixelNumber, k, 0) >= a2_guess)) lo2 = k;
	  if(!hi1 && ((*histo)(pixelNumber, size - 1 - k, 0) <= a1_guess)) hi1 = j;
	  if(!hi2 && ((*histo)(pixelNumber, size - 1 - k, 0) <= a2_guess)) hi2 = j;
	  --j;
	  ++k;
	}
	startsigma = (lo1 + hi1) * 0.5;
	endsigma = (lo2 + hi2) * 0.5;
	// guess in a more naive way
	/*for (i = start; i < size; i++) {
	  if ((*histo)(pixelNumber, i, 0) >= a1_guess) {
	    startsigma = i;
	    break;
	  }
	}
	for (i = start; i < size; i++) {
	  if ((*histo)(pixelNumber, i, 0) >= a2_guess) {
	    endsigma = i;
	    break;
	  }
	  }*/

	validBins->start = start;
	validBins->end = end;
	validBins->startsigma = startsigma;
	validBins->endsigma = endsigma;
	if( (end - start > 0) || (end - start == 0 && end != 0) ) validBins->valid = end - start + 1;
	else validBins->valid = 0;
	//std::cout << "There are " << validBins->valid << " valid bins" << std::endl;
	//std::cout << "  start = " << validBins->start << ", end = " << validBins->end << std::endl;
	//std::cout << "  sigma from 16% = " << validBins->startsigma << ", 84% = " << validBins->endsigma << std::endl;
}


/////////// LM FIT //////////////
double PixFitFitter_lmfit::simpleerfWrapper(double x, const double *par, const PixFitFitter_lmfit *fitterInstance) {
        return fitterInstance->simpleerf(x, par);
}

double PixFitFitter_lmfit::simpleerf(double x, const double *par) const{
  return 0.5 * inj_iterations * (1 + erf((x - par[0]) / (par[1] * cSqrt2))); // use of erf more direct and intuitive than erfc
	//return 0.5 * inj_iterations * (2 - erfc((x - par[0]) / (par[1] * cSqrt2))); // use analytic function - 2 params (careful with normalisation!)
	//return 0.5 * inj_iterations*( 2 - matchLUT((x-par[0])/(par[1] * cSqrt2)) ); // use LUTs - only works if lmmin.c values get tweaked
	//return lut[((int) x)]*par[1]+par[0]; // use x-y reversed lut - need to change x-y order in lmcurve
}

std::unique_ptr<PixFitResult> PixFitFitter_lmfit::linearInterpolation(std::shared_ptr<RawHisto> histo) {
  // Init options
  inj_iterations = histo->getScanConfig()->getInjections();
  const unsigned int npoints = histo->getScanConfig()->getNumOfBins();
  const unsigned int pixels = histo->getScanConfig()->getNumOfPixels();
  const int n_par = 2;
  const double target = inj_iterations/2.;
  //const int neighbours = 2;

  std::vector<double> par (pixels * n_par + pixels,-1);
  auto result = std::make_unique<PixFitResult>(histo->getScanConfig());

  for (unsigned int pixelNumber = 0; pixelNumber < pixels; pixelNumber++) { 
      for (unsigned int i = 1; i < npoints; i++){
        double const val0 = (*histo)(pixelNumber, i-1, 0);
        double const val1 = (*histo)(pixelNumber, i, 0);
          if((target <= val0 && target >val1) || (target <= val1 && target >val0 ) ){//Second case if is inverted
            if(val1 == val0)continue;
            par[pixelNumber*n_par] = (i -1) + ( target - val0 ) / (val1-val0);//Linear interpolation between the closest values
            par[pixelNumber*n_par+1] = 1./std::abs(val1-val0);//Stepness should be proportional to the slope
            break;
         }
      }
  }

  result->thresh_array = std::move(par);

  return result;
}


std::unique_ptr<PixFitResult> PixFitFitter_lmfit::derivativeMethod(std::shared_ptr<RawHisto> histo) {
  // Init options
  inj_iterations = histo->getScanConfig()->getInjections();
  const unsigned int npoints = histo->getScanConfig()->getNumOfBins();
  const unsigned int pixels = histo->getScanConfig()->getNumOfPixels();
  const int n_par = 2;

  std::vector<double> par (pixels * n_par + pixels,-1);
  auto result = std::make_unique<PixFitResult>(histo->getScanConfig());

    for (unsigned int pixelNumber = 0; pixelNumber < pixels; pixelNumber++) {

      double mean =0;
      double meanSquare =0;
      double sum=0;
        //Assunming derivative of S-Curve is gaussian
        for (unsigned int i = 0; i < npoints-1; i++){
          double const val0 = (*histo)(pixelNumber, i, 0);
          double const val1 = (*histo)(pixelNumber, i+1, 0);
          double const deviation = val1-val0;
          mean += (0.5+i)*deviation;//Mean of the gaussian Sum(x*p(x))
          meanSquare += (0.5+i)*(0.5+i)*deviation;//Sum(x*x*p(x)) to calculate the sigma
          sum += deviation;
         }

       if(sum==0)continue;
       mean /=sum;//Normalize the mean by the sum

       float sigma = 0;
       if(npoints>2) sigma =  std::sqrt((meanSquare/sum) - (mean*mean)); 
       par[pixelNumber*n_par] = mean;
       par[pixelNumber*n_par+1] = sigma;

  }

  result->thresh_array = std::move(par);

  return result;
}

std::unique_ptr<PixFitResult> PixFitFitter_lmfit::lmfit(int nthread, std::shared_ptr<RawHisto> histo) {

	// Init options
        inj_iterations = histo->getScanConfig()->getInjections();
	unsigned int threads = nthread;
	const unsigned int npoints = histo->getScanConfig()->getNumOfBins();
	const unsigned int pixels = histo->getScanConfig()->getNumOfPixels();

	ERS_DEBUG(1, "Reading " << npoints << " points of data for " << pixels << " pixels");

	std::vector<double> x (npoints * pixels);
	std::vector<double> y (npoints * pixels);
	// for debugging purposes, make a root file with occ histograms (only temporary)


	// Init fitter
	//ERS_LOG("Initializing lmfit fitter with " << threads << " threads.");
	std::vector<lm_status_struct> status (pixels);
	/* Initialize outcome so that we can tell if lmfit was actually run for this pixel later on. */
	for (unsigned int i = 0; i < pixels; i++) {
		status[i].outcome = -1;
	}

	std::vector<lm_control_struct> control (pixels);

	const int n_par = 2;
	/* Allocate memory for the results of the fit (n_par variables: mu and sigma) plus chi2 at the end of the array. */
	std::vector<double> par (pixels * n_par + pixels);

	int overall_counter = 0;
	ValidBins validBins;

	// Thread pool

	// Timer
	timeval begin, finish;

	// Counters for fit results
	int exh = 0, trap = 0, zero = 0, convbad = 0, conv = 0;

	// Only keep data that holds bins around s-curve centroid and do fit
	gettimeofday(&begin, 0);

	/* Use boost's io_service to handle the fit work with a threadpool. */
	boost::asio::io_service ioservice(threads);
	std::vector<std::thread> tp;

#ifdef DEBUG_ND
	if(!fff.is_open())fff.open (DEBUG_FILE , std::fstream::out);
#endif
	for (unsigned int i = 0; i < pixels; i++) {
	  control[i] = lm_control_double;
	  control[i].verbosity = 0;
	  analyzeData(histo, &validBins, npoints, i);
	  par[i*n_par+0] = (double)validBins.start + ((double)validBins.valid / 2.);
	  par[i*n_par+1] = (double)(validBins.endsigma - validBins.startsigma) / cSqrt2; //TODO: this could do with a bit of optimization
	  if (i == 0)
	    overall_counter = validBins.start;
	  if(validBins.valid > 0){
	    for (int point = validBins.start; point < (validBins.end + 1); point++) {
	      x[overall_counter] = point; // VCal steps
	      y[overall_counter] = (*histo)(i, point, 0); //number of injections
#ifdef DEBUG_ND
	      if(i == 3338)fff << "S-curve for Pixel 3338: " << x[overall_counter] << " " << y[overall_counter] << std::endl;
	      if(i == 3339)fff << "S-curve for Pixel 3339: " << x[overall_counter] << " " << y[overall_counter] << std::endl;
	      if(i == 3393)fff << "S-curve for Pixel 3393: " << x[overall_counter] << " " << y[overall_counter] << std::endl;
	      if(i == 3410)fff << "S-curve for Pixel 3410: " << x[overall_counter] << " " << y[overall_counter] << std::endl;
#endif
	      overall_counter++;
	    }
	  }
	  //cout << "Pixel "<< i << " start " << data->start << " end " << data->end << " bins " << data->validBins << " counter " << overall_counter << endl;
	  
	  /* Schedule fit only when there are more than 2 valid bins. */
	  if (validBins.valid > 2) {
			ioservice.post(boost::bind(lmcurve, n_par, &par[n_par*i], validBins.valid,
				    &x[overall_counter - validBins.valid],
				    &y[overall_counter - validBins.valid], simpleerfWrapper,
				    &control[i], &status[i], this));
	  }
	  /* @todo: test if code is quick enough and can handle 3 bins or if analytical solution should
	   * be added here also if only 2, use analytical solution from DSP code. */
	  else if (validBins.valid == 2) {
	    par[i*n_par+0] = (double)(validBins.start + validBins.end) / 2.;
	    par[i*n_par+1] = (double)(validBins.end - validBins.start) * cInvSqrt6;
	    if (par [i*n_par+0] < 0)
	      {
		par[i*n_par+0] = -1;
		par[i*n_par+1] = -1;
	      }
		
	  }
	  else {
	    if (validBins.valid == 0) zero++;
	    par[i*n_par+0] = -1;
	    par[i*n_par+1] = -1;
	  }
	}
	for (unsigned int i = 0; i < threads; i++) {
	  tp.emplace_back(boost::bind(&boost::asio::io_service::run, &ioservice));
	}

	
	/* Wait to finish tasks. */
	for (auto& thread : tp) {
            if (thread.joinable()) {
                thread.join();
            }
        }

	gettimeofday(&finish, 0);
	
	//double time = finish.tv_sec - begin.tv_sec + 1e-6 * (finish.tv_usec - begin.tv_usec);
	//ERS_LOG("Done fitting! (in " << time << "s with " << time / static_cast<double>(pixels) * 1e6 << "us per pixel)");

	// Fit results
	for (unsigned int i = 0; i < pixels; i++) {
	  if (status[i].outcome != -1) {
	    std::string searchstatus = lm_infmsg[status[i].outcome];
	    if (searchstatus.find("exhausted") != std::string::npos) exh++;
	    if (searchstatus.find("trapped") != std::string::npos) trap++;
	    if (searchstatus.find("zero") != std::string::npos) zero++;
	    // fit clearly failed - possibly improve these cases (e.g. filter out those that still have approximate s-curve behaviour)
	    if (searchstatus.find("converged") != std::string::npos) conv++; 
	    

	    //fit failed
	    if (!(searchstatus.find("converged") != std::string::npos)) {
				if (s_doverbose	&& !(searchstatus.find("zero") != std::string::npos)) {
					std::cout << "Pixel " << i << ": Fit failed " << searchstatus
									<< " -- mean = " << par[n_par * i + 0]
									<< ", noise = " << par[n_par * i + 1] << std::endl;
					std::cout << "  raw bin values: ";
					for (unsigned int j = 0; j < npoints; j++) {
						std::cout << (*histo)(i, j, 0) << " ";
					}
					std::cout << std::endl;
				}

	      par[n_par*i+0] = -1;
	      par[n_par*i+1] = -1;
	    }
	    if (searchstatus.find("converged") != std::string::npos)
	      {
		if (par[n_par * i + 0] < 0) //  We certainly don't want these in regular histograms.
	          // fit converged but outcome is negative and needs to be understood (most likely noisy pixels)
		  {
		    convbad++;
		    if (s_doverbose) {
		      std::cout << "Pixel " << i << ": Fit converged -- mean = "
				<< par[n_par * i + 0] << ", noise = "
				<< par[n_par * i + 1] << std::endl;
		      std::cout << "  raw bin values: ";
		      for (unsigned int j = 0; j < npoints; j++) {
			std::cout << (*histo)(i, j, 0) << " ";
		      }
		      std::cout << std::endl;
 		    }
		    par[n_par * i + 0] = -1;
		    par[n_par * i + 1] = -1;
		  }
		if (par[n_par * i + 0 ]> 500) //  very large fitted mean (Units in VCAL!  500 correcponds to a little over 20,000 e-)
		  {
		    convbad++;
		    if (s_doverbose) {
		      std::cout << "Pixel " << i << ": Fit converged -- mean = "
				<< par[n_par * i + 0] << ", noise = "
				<< par[n_par * i + 1] << std::endl;
		      std::cout << "  raw bin values: ";
		      for (unsigned int j = 0; j < npoints; j++) {
			std::cout << (*histo)(i, j, 0) << " ";
		      }
		      std::cout << std::endl;
		    }
		    par[n_par * i + 0 ] = -1;
		    par[n_par * i + 1 ] = -1;
		  }
	      }
	  }
	  
	}
	//ERS_LOG("Fit failure summary: total bad = " << exh + trap + convbad << ", total zero = " << zero);
	//ERS_LOG("  (ex " << exh << " / trap " << trap << " / convbad " << convbad << " / converged " << conv << ")");

	/* Fill threshold and noise values for a chip into array to pass to Publisher */
	auto result = std::make_unique<PixFitResult>(histo->getScanConfig());

	/* Get chi2 values and write them at the back of the array. In case no fit was run fill with -1. */
	int offset = pixels*n_par;
	for (unsigned int i = 0; i < pixels; i++) {
	  if (status[i].outcome != -1) {
	    par[offset + i] = status[i].fnorm / (npoints - n_par - 1); //chi2 per n.d.f.
	  }
	  else {
	    par[offset + i] = -1;
	  }
	}

	// In case you want to print the whole shebang
#ifdef DEBUG_ND
        int chipsPerModule = 16;
        int pixelsPerChipPerMaskStep = 90;
        int pixelsPerMaskStep = pixelsPerChipPerMaskStep * chipsPerModule;
	for (unsigned int i = 0; i < pixels; i++) {
// 	  std::cout << "Pixel: " << i "-- status: " << lm_infmsg[status[i].outcome] << std::endl; //  Looks like using m_infmsg[status[i].outcome] breaks things here !!!
	  int iMod = i/pixelsPerMaskStep;
	  int iChip = (i - iMod*pixelsPerMaskStep)/pixelsPerChipPerMaskStep;
	  int iRow = (i - iMod*pixelsPerMaskStep - iChip*pixelsPerChipPerMaskStep)/18;
	  int iCol = i - iMod*pixelsPerMaskStep - iChip*pixelsPerChipPerMaskStep - iRow*18;
	    fff << "Pixel: " << i << " Module: " << iMod << " Chip: " << iChip << " Row: " << iRow << " Col: " << iCol << std::endl;
	    fff << "  Mean = " << par[n_par*i+0] << std::endl;
	    fff << "  Sigma = " << par[n_par*i+1] << std::endl;
	    fff << "  Chi2 (per n.d.f.) = " << par[pixels*n_par + i] << " (n.d.f. = " << npoints - n_par - 1 << ")" << std::endl;
	}
#endif

	result->thresh_array = std::move(par);

	return result;
}

void PixFitFitter_lmfit::lmcurve_evaluate( const double *par, int m_dat, const void *data,
                       double *fvec, int*)
{
    int i;
    for ( i = 0; i < m_dat; i++ )
        fvec[i] =
            ((pixfit_lmcurve_data_struct*)data)->y[i] -
            ((pixfit_lmcurve_data_struct*)data)->f(
                ((pixfit_lmcurve_data_struct*)data)->t[i], par, ((pixfit_lmcurve_data_struct*)data)->fitterInstance );
}


void PixFitFitter_lmfit::lmcurve( int n_par, double *par, int m_dat, 
              const double *t, const double *y,
              double (*f)( double t, const double *par, const PixFitFitter_lmfit *fitterInstance ),
              const lm_control_struct *control,
              lm_status_struct *status, const PixFitFitter_lmfit *fitterInstance )
{
    pixfit_lmcurve_data_struct datavar;
    datavar.t = t;
    datavar.y = y;
    datavar.f = f;
    datavar.fitterInstance = fitterInstance;

    lmmin( n_par, par, m_dat, nullptr, (const void *) &datavar,
           lmcurve_evaluate, control, status );
}

bool PixFitFitter_lmfit::interpolate(const std::vector <float> &x,const std::vector <float> &y, const float &target, float &result, bool debug){

int high = -1, low=-1;
    for(size_t k=1;k<y.size();k++){
     if(debug)std::cout<<x[k-1]<<" "<<x[k] <<" "<<y[k-1]<<" "<<y[k]<<std::endl;
      if(target <= y[k-1] && target > y[k]){
        low = k-1; high = k;
        break;
      }else if(target <= y[k] && target > y[k-1]){//Case is inverted
        low = k; high = k - 1;
        break;
      }
    }

    if(high <0 || low <0 )return false;

    result = x[low] + ( target - y[low] )* (x[high] - x[low]) / (y[high]-y[low]);

    if(debug)std::cout<<x[low]<<" "<<x[high] <<" "<<y[low]<<" "<<y[high]<<" "<<result<<std::endl;

  return true;
}

//Experimental derivative method to get 50% of the sigmoid, doesn't work with tunning but can be interesting for other implementations
/*bool PixFitFitter_lmfit::derivativeSigmoid(const std::vector <float> &x,const std::vector <float> &y, const float &target, float &result, bool debug ){

  double mean =0;
  double sum=0;
  int bestIndex=0;
  double bestVal = std::abs(y[0] - target);
  result=-1;
    for (size_t i = 0; i < y.size()-1; i++){
      double const deviation = y[i+1]-y[i];
      double const index = x[i] + (x[i+1]-x[i])/2.;
      mean += index*deviation;
      sum += deviation;
      if(debug)std::cout<<i<<" "<<index<<" "<<mean<<" "<<sum<<std::endl;

      double const val = std::abs(y[i+1] - target);
      if(val < bestVal ){
        bestIndex=i+1;
        bestVal = val;
      }
    }

  if(sum!=0)result = mean/sum;

  //Sanity check
  if(debug)std::cout<<"Result from Iterative method "<<result<<" "<<bestIndex<<" "<<x[bestIndex]<<std::endl;
    for (int j = bestIndex-2; j <= bestIndex+2; j++){
      if(j <0 || j >= (int)x.size()-1 )continue;
        if((result <= x[j] && result > x[j +1]) || (result <= x[j+1] && result > x[j]))return true;//Succeed
      if(debug)std::cout<<j<<" "<<bestIndex<<" "<<x[j]<<" "<<result<<" "<<y[j]<<" "<<target<<std::endl;
    }

  //Inf case of failue fall back to interpolation method
  if(debug)std::cout<<"Derivative method failed, falling back to interpolate method"<<std::endl;
  return interpolate(x, y, target, result);
}*/


