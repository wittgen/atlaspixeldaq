#include "RootDb/RootDb.h"
#include "PixFe/PixFeI4Config.h"
#include "catch.hpp"

TEST_CASE( "Copy assignment makes a deep copy", "[PixFeI4Config]" ) {
  auto cfg1 = PixFeI4Config(static_cast<PixDbInterface *>(nullptr), nullptr, "fooBar", 0);
  auto cfg2 = PixFeI4Config(cfg1);
  auto cfg3 = cfg1;

  auto cfg1_enable = cfg1.readPixRegister(Fei4::PixReg::Enable);
  auto cfg2_enable = cfg2.readPixRegister(Fei4::PixReg::Enable);
  auto cfg3_enable = cfg2.readPixRegister(Fei4::PixReg::Enable);

  auto cfg1_tdaq = cfg1.readTrim(Fei4::PixReg::TDAC);
  auto cfg2_tdaq = cfg2.readTrim(Fei4::PixReg::TDAC);
  auto cfg3_tdaq = cfg2.readTrim(Fei4::PixReg::TDAC);

  auto constexpr maxX = 80-1;
  auto constexpr maxY = 336-1;

  SECTION("Default fields are initialised properly") {
    //SME is the first Fei4 register, default 0
    REQUIRE(cfg1.readGlobRegister(Fei4::GlobReg::SME) == 0);
    REQUIRE(cfg2.readGlobRegister(Fei4::GlobReg::SME) == 0);
    REQUIRE(cfg3.readGlobRegister(Fei4::GlobReg::SME) == 0);

    //A register in the middle, Vthin_Fine should have default at 46
    REQUIRE(cfg1.readGlobRegister(Fei4::GlobReg::Vthin_Fine) == 46);
    REQUIRE(cfg2.readGlobRegister(Fei4::GlobReg::Vthin_Fine) == 46);
    REQUIRE(cfg3.readGlobRegister(Fei4::GlobReg::Vthin_Fine) == 46);

    //The last Fei4 reg in the Fei4::GlobReg enum, default 15
    REQUIRE(cfg1.readGlobRegister(Fei4::GlobReg::EFUSE) == 15);
    REQUIRE(cfg2.readGlobRegister(Fei4::GlobReg::EFUSE) == 15);
    REQUIRE(cfg3.readGlobRegister(Fei4::GlobReg::EFUSE) == 15);

    //Same with the merged registers, the first (CP) some centre (HD) and last (TM) registers
    REQUIRE(cfg1.readGlobRegister(Fei4::MergedGlobReg::CP) == 0);
    REQUIRE(cfg2.readGlobRegister(Fei4::MergedGlobReg::CP) == 0);
    REQUIRE(cfg3.readGlobRegister(Fei4::MergedGlobReg::CP) == 0);
    REQUIRE(cfg1.readGlobRegister(Fei4::MergedGlobReg::HD) == 2);
    REQUIRE(cfg2.readGlobRegister(Fei4::MergedGlobReg::HD) == 2);
    REQUIRE(cfg3.readGlobRegister(Fei4::MergedGlobReg::HD) == 2);
    REQUIRE(cfg1.readGlobRegister(Fei4::MergedGlobReg::TM) == 0);
    REQUIRE(cfg2.readGlobRegister(Fei4::MergedGlobReg::TM) == 0);
    REQUIRE(cfg3.readGlobRegister(Fei4::MergedGlobReg::TM) == 0);

    //Testing the four corner points and one point in the middle
    REQUIRE(cfg1_enable.get(0, 0) == true);
    REQUIRE(cfg2_enable.get(0, 0) == true);
    REQUIRE(cfg3_enable.get(0, 0) == true);
    REQUIRE(cfg1_enable.get(maxX, 0) == true);
    REQUIRE(cfg2_enable.get(maxX, 0) == true);
    REQUIRE(cfg3_enable.get(maxX, 0) == true);
    REQUIRE(cfg1_enable.get(0, maxY) == true);
    REQUIRE(cfg2_enable.get(0, maxY) == true);
    REQUIRE(cfg3_enable.get(0, maxY) == true);
    REQUIRE(cfg1_enable.get(maxX, maxY) == true);
    REQUIRE(cfg2_enable.get(maxX, maxY) == true);
    REQUIRE(cfg3_enable.get(maxX, maxY) == true);
    REQUIRE(cfg1_enable.get(maxX/2, maxY/2) == true);
    REQUIRE(cfg2_enable.get(maxX/2, maxY/2) == true);
    REQUIRE(cfg3_enable.get(maxX/2, maxY/2) == true);

    //Again the four corners and middle
    REQUIRE(cfg1_tdaq.get(0, 0) == 15);
    REQUIRE(cfg2_tdaq.get(0, 0) == 15);
    REQUIRE(cfg3_tdaq.get(0, 0) == 15);
    REQUIRE(cfg1_tdaq.get(maxX, 0) == 15);
    REQUIRE(cfg2_tdaq.get(maxX, 0) == 15);
    REQUIRE(cfg3_tdaq.get(maxX, 0) == 15);
    REQUIRE(cfg1_tdaq.get(0, maxY) == 15);
    REQUIRE(cfg2_tdaq.get(0, maxY) == 15);
    REQUIRE(cfg3_tdaq.get(0, maxY) == 15);
    REQUIRE(cfg1_tdaq.get(maxX, maxY) == 15);
    REQUIRE(cfg2_tdaq.get(maxX, maxY) == 15);
    REQUIRE(cfg3_tdaq.get(maxX, maxY) == 15);
    REQUIRE(cfg1_tdaq.get(maxX/2, maxY/2) == 15);
    REQUIRE(cfg2_tdaq.get(maxX/2, maxY/2) == 15);
    REQUIRE(cfg3_tdaq.get(maxX/2, maxY/2) == 15);
  }
  constexpr int testX = maxX/2;
  constexpr int testY = maxY/2;

  SECTION("Validating writing changes to the registers work") {
    //Checking test register also before
    REQUIRE(cfg1_enable.get(testX, testY) == true);
    REQUIRE(cfg1_tdaq.get(testX, testY) == 15);
    cfg1.writeGlobRegister(Fei4::GlobReg::Vthin_Fine, 55);
    cfg1.writeGlobRegister(Fei4::MergedGlobReg::TM, 3);
    cfg1_enable.set(maxX/2,  maxY/2, false);
    cfg1_tdaq.set(maxX/2, maxY/2, 22);
    //Validate that the registers changed properly
    REQUIRE(cfg1.readGlobRegister(Fei4::GlobReg::Vthin_Fine) == 55);
    REQUIRE(cfg1.readGlobRegister(Fei4::MergedGlobReg::TM) == 3);
    REQUIRE(cfg1_enable.get(testX, testY) == false);
    REQUIRE(cfg1_tdaq.get(testX, testY) == 22);
    //Validate that cfg2 and cfg3 were not changed
    REQUIRE(cfg2.readGlobRegister(Fei4::GlobReg::Vthin_Fine) == 46);
    REQUIRE(cfg3.readGlobRegister(Fei4::GlobReg::Vthin_Fine) == 46);
    REQUIRE(cfg2.readGlobRegister(Fei4::MergedGlobReg::TM) == 0);
    REQUIRE(cfg3.readGlobRegister(Fei4::MergedGlobReg::TM) == 0);
    REQUIRE(cfg2_enable.get(testX, testY) == true);
    REQUIRE(cfg3_enable.get(testX, testY) == true);
    REQUIRE(cfg2_tdaq.get(testX, testY) == 15);
    REQUIRE(cfg3_tdaq.get(testX, testY) == 15);
  }
}
