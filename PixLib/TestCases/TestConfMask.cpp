#include "Config/ConfMask.h"
#include "catch.hpp"

TEST_CASE( "ConfMask<bool> test cases", "[ConfMask<bool>]" ) {
  constexpr int xSize = 10;
  constexpr int ySize = 10;
  constexpr int testX = xSize / 2;
  constexpr int testY = ySize / 2;
  constexpr int test2X = 3;
  constexpr int test2Y = 3;

  auto bool_mask_true = PixLib::ConfMask<bool>(xSize, ySize, true, true);
  auto bool_mask_false = PixLib::ConfMask<bool>(xSize, ySize, true, false);

  SECTION("Default fields are initialised properly") {
    REQUIRE(bool_mask_true.get(0, 0) == true);
    REQUIRE(bool_mask_false.get(0, 0) == false);
    REQUIRE(bool_mask_true.get(xSize - 1, ySize - 1) == true);
    REQUIRE(bool_mask_false.get(xSize - 1, ySize - 1) == false);
  }
  SECTION("Out-of-bounds get access") {
    REQUIRE_THROWS(bool_mask_true.get(-1, 0));
    REQUIRE_THROWS(bool_mask_true.get(0, -1));
    REQUIRE_THROWS(bool_mask_true.get(0, ySize));
    REQUIRE_THROWS(bool_mask_true.get(xSize, 0));
    REQUIRE_THROWS(bool_mask_true.get(xSize, ySize));
  }

  bool_mask_true.set(testX, testY, false);
  bool_mask_false.set(testX, testY, true);

  SECTION("Changing fields") {
    //Inverting the test fields
    REQUIRE(bool_mask_true.get(testX, testY) == false);
    REQUIRE(bool_mask_false.get(testX, testY) == true);

    //Checking the fields around the test fields
    REQUIRE(bool_mask_true.get(testX-1, testY) == true);
    REQUIRE(bool_mask_true.get(testX+1, testY) == true);
    REQUIRE(bool_mask_true.get(testX, testY-1) == true);
    REQUIRE(bool_mask_true.get(testX, testY+1) == true);
    REQUIRE(bool_mask_true.get(testX-1, testY-1) == true);
    REQUIRE(bool_mask_true.get(testX+1, testY+1) == true);
    REQUIRE(bool_mask_true.get(testX-1, testY+1) == true);
    REQUIRE(bool_mask_true.get(testX+1, testY-1) == true);
  }
  SECTION("Changing put-of-bounds fields") {
    REQUIRE_THROWS(bool_mask_true.set(-1, 0, false));
    REQUIRE_THROWS(bool_mask_true.set(0, -1, false));
    REQUIRE_THROWS(bool_mask_true.set(-1, -1, false));
    REQUIRE_THROWS(bool_mask_true.set(xSize, 0, false));
    REQUIRE_THROWS(bool_mask_true.set(0, ySize, false));
    REQUIRE_THROWS(bool_mask_true.set(xSize, ySize, false));
  }

  //Setting some fields to their opposite value to test setAll
  bool_mask_true.set(0, 0, false);
  bool_mask_true.set(1, 0, false);
  bool_mask_true.set(2, 0, false);
  bool_mask_true.set(0, 1, false);
  bool_mask_true.set(0, 2, false);
  bool_mask_true.setAll(true);
  bool_mask_false.setAll(false);

  SECTION("Testing setAll") {
    REQUIRE(bool_mask_true.get(0, 0) == true);
    REQUIRE(bool_mask_true.get(1, 0) == true);
    REQUIRE(bool_mask_true.get(2, 0) == true);
    REQUIRE(bool_mask_true.get(0, 1) == true);
    REQUIRE(bool_mask_true.get(0, 2) == true);
    REQUIRE(bool_mask_true.get(testX, testY) == true);
  }

  //Setting test field 2 to inverted field, to check if this is properly copied, i.e. in the "true"
  // mask to false and vice-versa
  bool_mask_true.set(test2X,test2Y, false);
  bool_mask_false.set(test2X,test2Y, true);

  auto bool_mask_true_assignment = bool_mask_true;
  auto bool_mask_false_copy = PixLib::ConfMask<bool>(bool_mask_false);

  SECTION("Test deep copy"){
    //Checking that test field 2 is copied properly (should be inverted)
    REQUIRE(bool_mask_true_assignment.get(test2X, test2Y) == false);
    REQUIRE(bool_mask_false_copy.get(test2X, test2Y) == true);
    //Just making sure the default values are still correct
    REQUIRE(bool_mask_true_assignment.get(0, 0) == true);
    REQUIRE(bool_mask_true.get(0, 0) == true);
    REQUIRE(bool_mask_false_copy.get(testX, testX) == false);
    REQUIRE(bool_mask_false.get(testX, testX) == false);
    //Checking that we don't have a shallow copy
    bool_mask_true_assignment.set(0, 0, false);
    REQUIRE(bool_mask_true_assignment.get(0, 0) == false);
    REQUIRE(bool_mask_true.get(0, 0) == true);
    bool_mask_false_copy.set(testX, testY, true);
    REQUIRE(bool_mask_false_copy.get(testX, testX) == true);
    REQUIRE(bool_mask_false.get(testX, testX) == false);
  }
}

TEST_CASE( "ConfMask<unsigned short int> test cases", "[ConfMask<unsigned short int>]" ) {
  constexpr int xSize = 10;
  constexpr int ySize = 10;
  constexpr int testX = xSize / 2;
  constexpr int testY = ySize / 2;

  auto mask_limit_5 = PixLib::ConfMask<unsigned short int>(xSize, ySize, 5, 0);
  auto mask_limit_512 = PixLib::ConfMask<unsigned short int>(xSize, ySize, 512, 2);

  SECTION("Default fields are initialised properly") {
    REQUIRE(mask_limit_5.get(0, 0) == 0);
    REQUIRE(mask_limit_512.get(0, 0) == 2);
    REQUIRE(mask_limit_5.get(xSize-1, ySize-1) == 0);
    REQUIRE(mask_limit_512.get(xSize-1, ySize-1) == 2);
  }

  mask_limit_5.setAll(3);
  mask_limit_512.setAll(451);

  SECTION("setAll works properly") {
    REQUIRE(mask_limit_5.get(0, 0) == 3);
    REQUIRE(mask_limit_5.get(testX, testY) == 3);
    REQUIRE(mask_limit_512.get(testX, testY) == 451);
    REQUIRE(mask_limit_512.get(xSize-1, ySize-1) == 451);
  }

  SECTION("Constructor with too large default"){
    REQUIRE_THROWS(PixLib::ConfMask<unsigned short int>(xSize, ySize, 5, 8));
  }

  SECTION("Write too large values") {
    REQUIRE_NOTHROW(mask_limit_5.setAll(5));
    REQUIRE_THROWS(mask_limit_5.setAll(6));
    REQUIRE_NOTHROW(mask_limit_512.set(testX, testY, 512));
    REQUIRE_THROWS(mask_limit_512.set(testX, testY, 513));
  }

  SECTION("Writing single value and checking adjacent fields") {
    mask_limit_512.set(testX, testY, 257);
    REQUIRE(mask_limit_512.get(testX, testY) == 257);

    REQUIRE(mask_limit_512.get(testX-1, testY) == 451);
    REQUIRE(mask_limit_512.get(testX-1, testY-1) == 451);
    REQUIRE(mask_limit_512.get(testX, testY-1) == 451);
    REQUIRE(mask_limit_512.get(testX+1, testY) == 451);
    REQUIRE(mask_limit_512.get(testX+1, testY+1) == 451);
    REQUIRE(mask_limit_512.get(testX, testY+1) == 451);
    REQUIRE(mask_limit_512.get(testX-1, testY+1) == 451);
    REQUIRE(mask_limit_512.get(testX+1, testY-1) == 451);
  }
}