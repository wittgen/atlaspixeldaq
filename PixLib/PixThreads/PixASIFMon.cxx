//////////////////////////////////////////////////////////////////////
// PixASIFMon.cxx
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 12/02/11  Version 1.0 (PM)
//           
//

#include <iostream>

#include "PixASIFMon.h"
#include "PixUtilities/PixISManager.h"
#include "PixBroker/PixBrokerSingleCrate.h"

using namespace PixLib;

PixASIFMon::PixASIFMon(PixISManager *ism,
		       PixBrokerSingleCrate *broker)
  : PixPeriodicTask("PixActionServerIFMon",10), m_is(ism), m_broker(broker) {
  m_forcePreOffDone = false;
  m_emergencyPreOffDone = false;
}

PixASIFMon::~PixASIFMon() {
}

bool PixASIFMon::initialize() {
  return true;
}

bool PixASIFMon::execute() {
  try {
    std::cout<<__PRETTY_FUNCTION__<<std::endl;
    m_broker->periodicTasks();

    int preAmpKill1, preAmpKill2, preAmpKill3;
    try {
      preAmpKill1 = m_is->readDcs<int>("PIX_AdjustHS_Warning");
    }
    catch (...) {
      preAmpKill1 = 0;
    }
    try {
      preAmpKill2 = m_is->readDcs<int>("PIX_DumpHS_Warning");
    }
    catch (...) {
      preAmpKill2 = 0;
    }
    try {
      preAmpKill3 = m_is->read<int>("PIX_Force_PreAmp_Off");
    }
    catch (...) {
      preAmpKill3 = 0;
    }
 
    m_forcePreOffDone = false;

    if (preAmpKill1 + preAmpKill2 + preAmpKill3 == 0) {
      m_emergencyPreOffDone = false;
    } else {
      if (!m_emergencyPreOffDone) {
	m_broker->emergency("EMERGENCY_PREAMP_KILL", "-");
	m_emergencyPreOffDone = true;
      }
    }
  }
  catch (...) {
    return false;
  }
  return true;
}
