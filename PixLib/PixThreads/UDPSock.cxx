#include "UDPSock.h"


UDPSock::UDPSock(const char* ip, uint16_t p, int timeout) {
  //Setting timeout
    
  len=0;
  to=timeout;
  ipAddress=ip;
  localIp=getLocalIp();
  port = p;
  initializeSocket();
  SetSocketTimeout();
}



UDPSock::UDPSock(const char* ip, std::string rodName, int timeout) {
  Initialize(ip, rodName, timeout);
}


void UDPSock::Initialize(const char* ip, std::string rodName, int timeout) {
  //Setting timeout
  len=0;
  to=timeout;
  ipAddress = ip;
  localIp=getLocalIp();
  port = PortFromRodName(rodName);
  initializeSocket();
  SetSocketTimeout();
}



void UDPSock::CloseSocket(){
  close(sockfd);
}

void UDPSock::clearBuffer(){
  memset(buf, 0, sizeof buf);
} 

bool UDPSock::SetSocketTimeout()
{
  
  //std::cout<<"Setting timeout..."<<to<<" seconds.."<<std::endl;
  struct timeval tt = {to,0};
  if (setsockopt(sockfd,SOL_SOCKET,SO_RCVTIMEO,(char*)&tt,sizeof(struct timeval)) < 0) {
    std::cout<<"UDPSock::could not set the timeout"<<std::endl;
    return false;
  }
  
  return true;
}


uint16_t UDPSock::PortFromRodName(std::string rodName)
{
  
  //IBL baseport = 20100; L1/C1 21100; L2/C2 22200; L3/C3 23300; L4 24400
  uint16_t baseport=30100;
  std::string crateName = rodName.substr(4,2); 
  
  if (crateName.find("I1") != std::string::npos)
    //IBL
    baseport = 30100;
  else if ((crateName.find("L1") != std::string::npos) || (crateName.find("C1") != std::string::npos))
    baseport = 31100;
  else if ((crateName.find("L2") != std::string::npos) || (crateName.find("C2") != std::string::npos))
    baseport = 32200;
  else if ((crateName.find("L3") != std::string::npos) || (crateName.find("C3") != std::string::npos))
    baseport = 33300;
  else if ((crateName.find("L4") != std::string::npos))
    baseport = 34400;
  
  
  std::size_t pos = rodName.find("S");
  uint16_t slotNr = std::stoi(rodName.substr(pos+1));
  
  
  uint16_t port_candidate = baseport+slotNr;
  int ntrials = 6;
  int itrial  = 0;
  bool foundport = false;
  bool doPortSearch = true;
  
  while ((itrial<ntrials) && (!foundport) && (doPortSearch))
    {
      
      port_candidate = baseport+slotNr+(itrial*16);
      int testsockfd;
      struct sockaddr_in myaddr;

      testsockfd = socket(AF_INET,SOCK_DGRAM,0);
      
      myaddr.sin_family = AF_INET;
      myaddr.sin_addr.s_addr = inet_addr(localIp);
      myaddr.sin_port   = htons(port_candidate);
     
      //you should set reuse port on this socket? I'm not sure the kernel is fast enough to free it.
      
      
      if (bind(testsockfd,(struct sockaddr*) &myaddr, sizeof(myaddr)) < 0)
	{
	  foundport=false; 
	  std::cout<<"Could not bind socket.. Port"<<port_candidate<<"is taken!"<<std::endl;
	  itrial++;
	}
      else
	{
	  //std::cout<<"Port candidate:"<<port_candidate<<std::endl;
	  close(testsockfd);
	  foundport=true;
	  itrial++;
	}
    }
  
  
  return port_candidate;
  
}




void UDPSock::add8(uint8_t val)
{buf[len]=val;len++;}

void UDPSock::add16(uint16_t val) {
  uint16_t v=htons(val);
  memcpy((void *)&buf[len],(void*)&v,2);len+=2;
}
//  is never used but defined the wrong size operation
void UDPSock::add32(uint32_t val) {
  uint32_t v=htonl(val);
  memcpy((void *)&buf[len],(void*)&v,4);len+=4;
}

void UDPSock::sendipAddress(const std::string ip) 
{
  add8(ip.size());
  for (unsigned int i=0; i<ip.size(); i++)
    add8(ip[i]);
  add8('\0');
}

//This function has shown problems in the Actions.. Not sure of the reasons..avoid.
void UDPSock::sendipAddressAndPort()
{
  
  std::string tmp=localIp;
  add8(tmp.size());
  for (unsigned int i=0; i<tmp.size(); i++)
    add8(tmp[i]);
  add8('\0');
  add16(port);
}

void UDPSock::sendPort()
{
  add16(port);
}


char* UDPSock::getLocalIp() {
  
  char hostname[100];
  gethostname(hostname,100);
  char* ipbuf;
  
  //I think that there is a better way than this but I don't remember.

  struct hostent *hostentry;
  hostentry = gethostbyname(hostname);
  if (hostentry==NULL)
    { 
      int errsv = errno;
      std::cout<<"UDPSock::Error in getting the hostentry struct. errno:"<< errsv<<"hostname:"<<hostname<<std::endl;
      return (char*)"8.8.8.8";
    }
  
  ipbuf = inet_ntoa(*((struct in_addr*)hostentry->h_addr_list[0]));
  if (ipbuf==NULL)
    {
      int errsv = errno;
      std::cout<<"UDPSock::Error in getting the ipAddress from hostname. errno:"<<errsv<<std::endl; 
      return (char*)"8.8.8.8";
    }
  //std::cout<<"Local ip for "<<hostname<<" "<<ipbuf<<std::endl;
  return ipbuf;
  
}

bool UDPSock::initializeSocket()
{
  
  sockfd = socket(AF_INET,SOCK_DGRAM,0);
  
  //Bind the local socket to a specific port
  memset(&local,0,sizeof(struct sockaddr_in));

  local.sin_family = AF_INET;
  //local.sin_addr.s_addr = htonl(INADDR_ANY); //LocalIpAddress
  local.sin_addr.s_addr = inet_addr(localIp); //LocalIpAddress
  local.sin_port = htons(port);
  
  if (bind(sockfd,(struct sockaddr*)&local,sizeof(local))<0)
    {
      int errsv = errno;
      std::cout<<"UDPSock::could not bind the socket to specified port "<<port<<std::endl;
      std::cout<<"UDPSock::errorno "<<errsv<<std::endl;
    }
   
  else
    std::cout<<"UDPSock::bind socket to port"<<port<<std::endl;
  
    
  //Set up the PPC server structure
  
  server.sin_family      = AF_INET;
  server.sin_addr.s_addr = inet_addr(ipAddress); 
  server.sin_port        = htons(10776); //Hardcoded port for the PPC QS network
  serverlen              = sizeof server;

  return true;
}

int UDPSock::sendBuffer() 
{
  int n = sendto(sockfd,buf,len,0,(const struct sockaddr *) &server, sizeof(struct sockaddr_in));
  return n;
}

int UDPSock::receiveData()
{
  clear();
  char ipA[INET_ADDRSTRLEN];
  clientlen = sizeof client;  //maybe you can put this in the constructor.
  int n = recvfrom(sockfd,buf,sizeof(buf),0,(struct sockaddr*)&client,&clientlen);
  if (n<0)
    std::cout<<"UDPSock::recvfrom error!"<<std::endl;
  else
    {
      std::cout<<"Received packet from "<<inet_ntop(AF_INET,&(client.sin_addr),ipA,INET_ADDRSTRLEN)<<std::endl;
    }
  
  return n;
}

int UDPSock::checkConnection()
{
  return connect(sockfd,(const struct sockaddr*) &server,serverlen);
}


int UDPSock::sendQSCommand(uint8_t command, uint8_t mode)
{
  add8(command);
  sendPort();
  add8(mode);
  int n = sendBuffer();
  return n;
}
