//////////////////////////////////////////////////////////////////////
// PixThread.cxx
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 13/08/07  Version 1.0 (DLM)
//           
//


#include <unistd.h>
#include <time.h>
#include <iostream>

#include "PixThread.h"
#include "PixUtilities/PixISManager.h"
#include "PixUtilities/PixMessages.h"

using namespace PixLib;

PixThread::PixThread(std::string name)
  : DFThread(), m_name(name), m_is(NULL), m_isPrefix(std::string(""))
{
  std::string mutexName = name + "_Mutex";
  m_mutex = DFMutex::Create(const_cast<char*>(mutexName.c_str()));
}

PixThread::PixThread(std::string name, PixISManager *is, std::string isPrefix)
  : DFThread(), m_name(name), m_is(is), m_isPrefix(isPrefix) 
{
  std::string mutexName = name + "_Mutex";
  m_mutex = DFMutex::Create(const_cast<char*>(mutexName.c_str()));
}

PixThread::~PixThread()
{
  m_mutex->destroy();
}

void PixThread::run() 
{

  std::cout << "Started thread: " << DFThread::id() << std::endl;

  while(true){
    unsigned int finalTimePeriod = 0xFFFFFFFF;
    if (m_is != NULL) {
      // Publish WatchDog time
      time_t t = time(NULL);
      m_is->publish(m_isPrefix+"LAST-WD-CALL", (int)t);
    }
    if(m_periodicTasks.size() == 0){
      //      std::cout << "No tasks to execute. Sleeping " << this << std::endl;
      sleep(10);
    }
    cancellationPoint();
    //do not allow changes in the periodic tasks while performing them
    //std::cout << "Thread " << m_name << " asking for lock " << std::endl;
    try {
      m_mutex->lock();
    } catch (DFMutex::AlreadyLockedByThread &v) {
     PIX_ERROR( "PixThread::run(): m_mutex->lock():" << v.what() << " - Continuing...");
    }
    //std::cout << "Thread " << m_name << " got lock " << std::endl;

    std::vector<PixTaskDescr>::iterator td;
    try {
      for(td = m_periodicTasks.begin(); td != m_periodicTasks.end(); td++) {
        unsigned int newTime = (unsigned int) time(NULL);

        if (((double) newTime-td->lastExecute) >= td->task->getPeriod()) {

            if (td->task->execute()) {
                td->eraseMe = false;
            } else {
                td->eraseMe = true;
                std::cout << "PixThread " << m_name << " Task will stop: " << td->task->getName() << std::endl;
            }

            //get new time again
            newTime = (unsigned int) time(NULL);
            td->lastExecute = newTime;
        }
        if (finalTimePeriod > (td->lastExecute + (unsigned int) td->task->getPeriod()) && !td->eraseMe){ 
            finalTimePeriod = td->lastExecute+(unsigned int) td->task->getPeriod();
        }
      }
    } catch (const std::exception& e) {
      PIX_ERROR( "std::exception: " << e.what());
    } catch (...) {
      PIX_ERROR("Unknown exception while executing task " << td->task->getName());
    }
    try {
        m_mutex->unlock();
    } catch (DFMutex::NotLockedByThread &v) {
        PIX_ERROR("PixThread::run(): m_mutex->unlock():" << v.what() << " - Continuing...");
    }

    cancellationPoint();
    //std::cout << "Thread " << m_name << " released lock " << std::endl;

    td = m_periodicTasks.begin();
    while (td != m_periodicTasks.end()) {
        if (td->eraseMe) {
            std::cout << "PixThread " << m_name << " Removing task " << td->task->getName() << std::endl;
            delete td->task;
            m_periodicTasks.erase(td);
            std::cout << "PixThread task deleted " << std::endl;
        } else {
            td++;
        }
        if (td == m_periodicTasks.end()) break;
    }

    cancellationPoint();
    if(finalTimePeriod == 0xFFFFFFFF) continue;
    //get final time
    unsigned int finalTime = (unsigned int) time(NULL);
    if(finalTimePeriod > finalTime) {
        sleep(finalTimePeriod-finalTime);
    }
    sleep(1);
  }
}


void PixThread::cleanup() 
{
    try{
        m_mutex->lock();
    } catch (DFMutex::AlreadyLockedByThread &v) {
        PIX_ERROR("PixThread::cleanup(): m_mutex->lock():" << v.what());
    }
    std::vector<PixTaskDescr>::iterator td;
    td = m_periodicTasks.begin();
    while (td != m_periodicTasks.end()) {
        std::cout << "PixThread::cleanup(): periodic: " << td->task->getName() << std::endl;
        delete td->task;
        m_periodicTasks.erase(td);
        if (td == m_periodicTasks.end()) break;
    }
    m_periodicTasks.clear();
    try {
        m_mutex->unlock();
    } catch (DFMutex::NotLockedByThread &v) {
        PIX_ERROR("PixThread::cleanup(): m_mutex->unlock():" << v.what());
    }
}

bool PixThread::removeTask(std::string name)
{
    std::vector<PixTaskDescr>::iterator td;
    bool result = false;
    try{
        m_mutex->lock();
    } catch (DFMutex::AlreadyLockedByThread &v) {
        PIX_ERROR("PixThread::removeTask(): m_mutex->lock():" << v.what() );
    }
    td = m_periodicTasks.begin();
    while (td != m_periodicTasks.end()) {
        if (td->task->getName() == name){
            std::cout << "PixThread::removeTask(): periodic: " << td->task->getName() << std::endl;
            delete td->task;
            m_periodicTasks.erase(td);
            std::cout << "PixThread::removeTask(): task deleted " << std::endl;
        } else {
            td++;
        }
        if (td == m_periodicTasks.end()) break;
    }
    try {
        m_mutex->unlock();
    } catch (DFMutex::NotLockedByThread &v) {
        PIX_ERROR("PixThread::removeTask(): m_mutex->unlock():" << v.what());
    }
    std::cout << "PixThread::removeTask(): periodic: done " << std::endl;
    return result;
}

std::vector<std::string> PixThread::listTasks()
{
    std::vector<std::string> result;
    std::vector<PixTaskDescr>::iterator td;

    for (td = m_periodicTasks.begin(); td != m_periodicTasks.end(); td++) {
        result.push_back(td->task->getName());
        //    std::cout << "PixThread::listTasks(): result: " << td->task->getName() << std::endl;
    }

    return result;
}

bool PixThread::setNewTask(PixPeriodicTask* task)
{
  std::cout<<"In function: "<<__PRETTY_FUNCTION__<<std::endl;
    try {
        m_mutex->lock();
    } catch (DFMutex::AlreadyLockedByThread &v) {
        PIX_ERROR("PixThread::setNewTask(): m_mutex->lock():" << v.what());
    }
    std::cout << "Initializing new task " << task->getName() << std::endl;
    bool init = task->initialize();
    //std::cout << "Done initializing " << init << std::endl;
    if(init){
        PixTaskDescr descr;
        descr.task = task;
        descr.lastExecute = 0;
        descr.eraseMe = false;
        m_periodicTasks.push_back(descr); 
    }
    try {
        m_mutex->unlock();
    } catch (DFMutex::NotLockedByThread &v) {
        PIX_ERROR("PixThread::setNewTask(): m_mutex->unlock():" << v.what() );
    }
    return init;
}


