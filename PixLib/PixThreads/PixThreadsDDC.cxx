//////////////////////////////////////////////////////////////////////
// PixThreadsDDC.cxx
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 2/08/07  Version 1.0 (NG)
//           
//


#include <unistd.h>
#include <iostream>
#include <sstream>

#include "PixThreadsDDC.h"
#include "DFThreads/DFStandardQueue.h"

#include "PixUtilities/PixISManager.h"
#include "PixDcs/PixDcs.h"

using namespace PixLib;



PixThreadsDDC::PixThreadsDDC(std::string pp0BaseName, int rep, PixDcs *dcs, commandType ct) 
  : m_pp0BaseName(pp0BaseName), m_rep(rep), m_dcs(dcs), m_type(ct){
  m_end=false;
}


void PixThreadsDDC::run() {
  
  std::cout << "PixThreadsDDC::run() Started thread: " << DFThread::id() << std::endl;
  std::cout << "PixThreadsDDC::run() Sleep 2s for reading" << std::endl;
  sleep(2);
  int status;

  if(m_type == DDC) {

    std::string pp0[4];
    for (int j=0; j<4; j++) {
      std::ostringstream os;
      os << m_pp0BaseName << "_" << j;
      pp0[j] = os.str();
      std::cout << "pp0 name: " << pp0[j] << std::endl;
    }
    float val = 0.75;
    for (int i=0; i<m_rep; i++) {
      for(int j=0; j<4; j++) {
	try {	  
	  status = m_dcs->writeDataPoint(pp0[j], val, PixDcsDefs::SET_VISET, 20);
	  if(status!=0){
	    m_failure.push_back(status);
	  }
	  std::cout << "FEEDBACK IS: " << status << std::endl;
	} catch (...) {
	  std::cout << "PixThreadsDDC::run() Thread " << DFThread::id() << " exit at command "<< (i+1)  <<std::endl;
	  cancellationPoint();
	}
      }
      std::cout << "PixThreadsDDC::run(). Thread " << DFThread::id() << " has sent "<< 4*(i+1) << " commands"<<std::endl;
      if (val > 1) { 
	val = 0.75;
      } else  {
	val += 0.05;
      }
    }
    m_end = true;
    
  } else if (m_type == FSM) {
    try {
      status = m_dcs->sendCommand(PixDcsDefs::SWITCH_ON, m_pp0BaseName,20);
      if(status!=0){
	m_failure.push_back(status);
      }
      std::cout << "FEEDBACK IS: " << status << std::endl;
      status = m_dcs->sendCommand(PixDcsDefs::SWITCH_OFF, m_pp0BaseName, 20);
      if(status!=0){
	m_failure.push_back(status);
      }
      std::cout << "FEEDBACK IS: " << status << std::endl;
    } catch (...) {
      std::cout << "PixThreadsDDC::run() Thread " << DFThread::id() << " exit "<<std::endl;
      cancellationPoint();
    }
    m_end = true;
  
  } else {
    std::cout << "WRITE COMMAND TYPE" << std::endl;
  }
  
}


void PixThreadsDDC::cleanup() {

  //std::cout << "The cleanup() method contain the code which has to be executed on thread termination or cancellation!" << std::endl;
  std::cout << "PixThreadsDDC::cleanup(). Stopped thread: " << DFThread::id() << std::endl;
  std::cout << "On thread: "<<DFThread::id()<< " has been registered: "<< m_failure.size() << " error feedback! " << std::endl;
}

bool PixThreadsDDC::checkEnd() {
  return m_end;
}

