//////////////////////////////////////////////////////////////////////
// IblCppMonitorTask.cxx
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 01/04/08  Version 1.0 (KMP)
//           
//


#include <time.h>
#include <iostream>
#include <iomanip>

#include "IblCppMonitorTask.h"
#include "PixConnectivity/RodBocConnectivity.h"
#include "PixConnectivity/OBConnectivity.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "PixUtilities/PixISManager.h"
#include "PixThread.h"

#include "IblRodStatus.h"
//#include "CountOccupancy.h"
#include "QSTalkTask.h"

#define ROD_BUSY_STATUS           (4 + RRIF_STATUS_0)

#define HEXF(x,y) std::hex << "0x" << std::hex << std::setw(x) << std::setfill('0') << static_cast<int>(y) << std::dec

using namespace PixLib;
using namespace SctPixelRod;

IblCppMonitorTask::IblCppMonitorTask(PixModuleGroup *grp,
				     PixHistoServerInterface *hsi,
				     std::string id,
				     std::string name,
				     PPCppRodPixController* rod,
				     std::string partitionName,
				     unsigned int monPrescale,
				     unsigned int occupRate,
				     unsigned int calibRate,
				     unsigned int statusRate,
				     unsigned int busyRate,
				     unsigned int buffRate,
				     unsigned int quickStatusMode,
				     unsigned int quickStatusConfig,
				     unsigned int MODsyncConfig,
				     unsigned int RODsyncConfig)
  :  PixPeriodicTask(name,5), m_grp(grp), m_ctrl(rod), m_partitionName(partitionName),
  m_rodMonPrescale(monPrescale), m_occupRate(occupRate), m_calibRate(calibRate),
  m_statusRate(statusRate), m_busyRate(busyRate), m_buffRate(buffRate), m_count(0),m_quickStatusMode(quickStatusMode), m_quickStatusConfig(quickStatusConfig), m_MODsyncConfig(MODsyncConfig), m_RODsyncConfig(RODsyncConfig)
{
  m_runName = id;
  std::cout << "start the IblCpp Monitoring" << std::endl;
}

IblCppMonitorTask::~IblCppMonitorTask()
{
  setQSMode(QS_OFF);		// Resetting QS variables may hang RCD. Need to understand better the reason before bring this feature back.

}

void IblCppMonitorTask::publishCountAndTimestamp(const std::string& isName, const std::string& isName_time, unsigned int count) {
  time_t seconds = time(NULL);
  char time_stamp[50];
  sprintf(time_stamp, "%ld", seconds);
  int QSAction_timeStamp=atoi(time_stamp);
  m_grp->isManager()->publish(isName, count);
  m_grp->isManager()->publish(isName_time, QSAction_timeStamp);
}

// Copied from PixDSPMonitorTask, with minimum change that initializes the QS for new readout properly
bool IblCppMonitorTask::initialize()
{
  std::cout << __PRETTY_FUNCTION__ << std::endl;

  std::string rodConnName = m_grp->rodConnectivity()->name();
  std::ostringstream os_qsinfo;
  //m_quickStatusMode = QS_MON; //Hardcoding for testing purposes when allocating Console.
  os_qsinfo << "QS in RunConfiguration: QuickStatusMode = "<< m_quickStatusMode << " QuickStatusConfig = " << m_quickStatusConfig;
  ers::info(PixLib::pix::daq (ERS_HERE, "CppMon:QS " + rodConnName, os_qsinfo.str()));
  if (m_quickStatusMode<QS_OFF || m_quickStatusMode>QS_RST_CFG_OFF){
    std::ostringstream os_qsoff;
    os_qsoff << "Invalid RunConfiguration for QuickStatusMode: " << m_quickStatusMode << ". QuickStatus will remain OFF";
    ers::warning(PixLib::pix::daq (ERS_HERE, "CppMon:QS " + rodConnName, os_qsoff.str()));
    m_quickStatusMode = QS_OFF;
  }

  // Parse QS config
  m_fc_max = m_quickStatusConfig & 0xff;
  m_fc_max *= 50;
  m_bu_max = (m_quickStatusConfig >> 8) & 0x1ff;
  m_to_max = (m_quickStatusConfig >> 17) & 0xff;
  m_bu_cfg_max = (m_quickStatusConfig >> 25) & 0x7;
  m_to_cfg_max = (m_quickStatusConfig >> 28) & 0x7;

  // New QS parameters using percentage, hardcoded untill the settings is included in run config 
  // 
  if(m_grp->getRodName().find("ROD_I1") != std::string::npos ){
	  std::cout<<m_grp->getRodName()<<" has custom QS parameters "<<std::endl;
	  m_bu_max = 1;
	  m_to_max = 10;
  } else {
	  m_bu_max = 1;
	  m_to_max = 2;
  }


  std::ostringstream os_qsconf;
  os_qsconf << "QS configuration(" << m_quickStatusConfig << "): FC_MAX = " << m_fc_max << ", BU_MAX = " << m_bu_max << ", TO_MAX = " << m_to_max << ", BU_CFG_MAX = " << m_bu_cfg_max << ", TO_CFG_MAX = " << m_to_cfg_max;
  ers::info(PixLib::pix::daq (ERS_HERE, "CppMon:QS " + rodConnName, os_qsconf.str()));

  //if( m_grp->getCtrlType() == CPPROD ) return true; // Stop here for IBL

  // Parse MOD Sync Config
  m_MODsyncEna         =  m_MODsyncConfig & 0x1;
  m_MODsyncMaxTrials   = (m_MODsyncConfig>>1) & 0x7;
  m_MODsyncISReadDelay = (m_MODsyncConfig>>4) & 0x7F;
  m_MODsyncErrThresh   = (m_MODsyncConfig>>11) & 0x7F;
  m_MODsyncErrThresh   =  m_MODsyncErrThresh*100;
  m_MODsyncMinIdle     = (m_MODsyncConfig>>18) & 0xF;
  
  std::ostringstream os_qsmodsyncconf;
  os_qsmodsyncconf << "MOD sync configuration(" << m_MODsyncConfig << "): Enable = " << m_MODsyncEna << ", syncMaxTrials = " << m_MODsyncMaxTrials << ", syncISReadDelay = " << m_MODsyncISReadDelay << ", syncErrThresh = " << m_MODsyncErrThresh << ", syncMinIdle = " << m_MODsyncMinIdle;
   ers::info(PixLib::pix::daq (ERS_HERE, "CppMon:QS " + rodConnName, os_qsmodsyncconf.str()));

  // Parse ROD Sync Config
  m_RODsyncEna          =  m_RODsyncConfig & 0x1;
  m_RODsyncMaxTrials    = (m_RODsyncConfig>>1) & 0x7;
  m_RODsyncISReadDelay  = (m_RODsyncConfig>>4) & 0x7F;
  m_RODsyncErrThresh    = (m_RODsyncConfig>>11) & 0x7F;
  m_RODsyncErrThresh    =  m_RODsyncErrThresh*100;
  m_RODsyncMinIdle      = (m_RODsyncConfig>>18) & 0xF;
  m_newRODsyncEna       = false; // LASER - default to false
  m_newRODsyncErrThresh = 400; // LASER - default to 400

  std::ostringstream os_qsrodsyncconf;
  os_qsrodsyncconf << "ROD sync configuration(" << m_RODsyncConfig << "): Enable = " << m_RODsyncEna << ", syncMaxTrials = " << m_RODsyncMaxTrials << ", syncISReadDelay = " << m_RODsyncISReadDelay << ", syncErrThresh = " << m_RODsyncErrThresh << ", syncMinIdle = " << m_RODsyncMinIdle;
  ers::info(PixLib::pix::daq (ERS_HERE, "CppMon:QS " + rodConnName, os_qsrodsyncconf.str()));

  // Get module names
  m_links.clear();
  m_fmtstat.clear();
  m_MODsyncTrialCnt.clear();
  m_MODsyncIdleCnt.clear();
  m_RODsyncTrialCnt.clear();
  m_RODsyncIdleCnt.clear();
  m_RODsyncRODIdleCnt = 0;
  m_RODerr.clear();
 
  for (int i=0; i<32; i++) {

    m_QSDisabled[i]=0;
    std::ostringstream os;
    std::string modName = m_grp->getModIdName(m_ctrl->getModuleIDFromRx(i));
    os << "Rx " << i << " Name = " << modName;
    ers::info(PixLib::pix::daq (ERS_HERE, "CppMon:QS " + rodConnName, os.str()));
    if (modName != "") {
      m_links[modName].push_back(i);
      m_fmtstat[modName] = "X";
      m_MODsyncTrialCnt[i] = 0;
      m_MODsyncIdleCnt[i] = m_MODsyncMinIdle+1;
      m_RODsyncTrialCnt[i] = 0;
      m_RODsyncIdleCnt[i] = m_RODsyncMinIdle+1;
      m_RODsyncRODIdleCnt = m_RODsyncMinIdle+1;
      m_RODerr[i].assign(2,0);
    }
  }
  m_MODdelayCnt = 0;
  m_RODdelayCnt = 0;

  /*** QuickStatus setup***/
  std::ostringstream os_qssetup;
  os_qssetup << __PRETTY_FUNCTION__ << ": QuickStatus setup";
  ers::info(PixLib::pix::daq (ERS_HERE, "CppMon:QS " + rodConnName, os_qssetup.str()));
  m_QSModeMap.clear();
  m_QSModeMap["QS_INFO"] = QS_INFO;
  m_QSModeMap["QS_OFF"] = QS_OFF;
  m_QSModeMap["QS_IDLE"] = QS_IDLE;
  m_QSModeMap["QS_MON"] = QS_MON;
  m_QSModeMap["QS_RST"] = QS_RST;
  m_QSModeMap["QS_RST_DIS"] = QS_RST_DIS;
  m_QSModeMap["QS_RST_CFG_ON"] = QS_RST_CFG_ON;
  m_QSModeMap["QS_RST_CFG_OFF"] = QS_RST_CFG_OFF;
  m_QSModeMap["QS_SINGLE_RST"] = QS_SINGLE_RST;
  m_QSModeMap["QS_SINGLE_CFG_ON"] = QS_SINGLE_CFG_ON;
  m_QSModeMap["QS_SINGLE_CFG_OFF"] = QS_SINGLE_CFG_OFF;
  m_QSModeMap["QS_SINGLE_DIS"] = QS_SINGLE_DIS;
  m_QSModeMap["QS_SINGLE_ENA"] = QS_SINGLE_ENA;
  m_QSModeMap["QS_SINGLE_RST_ALL"] = QS_SINGLE_RST_ALL;
  m_QSModeMap["N_FC50_MAX"] = N_FC50_MAX;
  m_QSModeMap["N_BU_MAX"] = N_BU_MAX;
  m_QSModeMap["N_TO_MAX"] = N_TO_MAX;
  m_QSModeMap["N_HT_MAX"] = N_HT_MAX;
  m_QSModeMap["N_RST_MAX"] = N_RST_MAX;
  m_QSModeMap["N_CFG_MAX"] = N_CFG_MAX;
  m_QSModeMap["QS_TEST"] = QS_TEST;
  m_QSModeMap["QS_TEST_SYNCH"] = QS_TEST_SYNCH;
  
  m_QSModeMap_names.clear();
  m_QSModeMap_names[QS_INFO] = "QS_INFO";
  m_QSModeMap_names[QS_OFF] = "QS_OFF";
  m_QSModeMap_names[QS_IDLE] = "QS_IDLE";
  m_QSModeMap_names[QS_MON] = "QS_MON";
  m_QSModeMap_names[QS_RST] = "QS_RST";
  m_QSModeMap_names[QS_RST_DIS] = "QS_RST_DIS";
  m_QSModeMap_names[QS_RST_CFG_ON] = "QS_RST_CFG_ON";
  m_QSModeMap_names[QS_RST_CFG_OFF] = "QS_RST_CFG_OFF";
  m_QSModeMap_names[QS_SINGLE_RST] = "QS_SINGLE_RST";
  m_QSModeMap_names[QS_SINGLE_CFG_ON] = "QS_SINGLE_CFG_ON";
  m_QSModeMap_names[QS_SINGLE_CFG_OFF] = "QS_SINGLE_CFG_OFF";
  m_QSModeMap_names[QS_SINGLE_DIS] = "QS_SINGLE_DIS";
  m_QSModeMap_names[QS_SINGLE_ENA] = "QS_SINGLE_ENA";
  m_QSModeMap_names[QS_SINGLE_RST_ALL] = "QS_SINGLE_RST_ALL";
  m_QSModeMap_names[N_FC50_MAX] = "N_FC50_MAX";
  m_QSModeMap_names[N_BU_MAX] = "N_BU_MAX";
  m_QSModeMap_names[N_TO_MAX] = "N_TO_MAX";
  m_QSModeMap_names[N_HT_MAX] = "N_HT_MAX";
  m_QSModeMap_names[N_RST_MAX] = "N_RST_MAX";
  m_QSModeMap_names[N_CFG_MAX] = "N_CFG_MAX";
  m_QSModeMap_names[QS_TEST] = "QS_TEST";
  m_QSModeMap_names[QS_TEST_SYNCH] = "QS_TEST_SYNCH";
  
  //Publish QSMode and DSP mon trick1 to IS
  try{
    if (m_grp->isManager() != NULL){
      // m_grp->isManager()->publish("DSPMon_trick1","ON");
      
      //if (m_MODsyncEna) m_grp->isManager()->publish("DSPMon_MODsyncEna","ON");
      //else m_grp->isManager()->publish("DSPMon_MODsyncEna","OFF");
      //m_grp->isManager()->publish("DSPMon_MODsyncMaxTrials",m_MODsyncMaxTrials);
      //m_grp->isManager()->publish("DSPMon_MODsyncMinIdle",m_MODsyncMinIdle);
      //m_grp->isManager()->publish("DSPMon_MODsyncISReadDelay",m_MODsyncISReadDelay);
      //m_grp->isManager()->publish("DSPMon_MODsyncErrThresh",m_MODsyncErrThresh);

      //if (m_RODsyncEna) m_grp->isManager()->publish("DSPMon_RODsyncEna","ON");
      //else m_grp->isManager()->publish("DSPMon_RODsyncEna","OFF");
      //m_grp->isManager()->publish("DSPMon_RODsyncMaxTrials",m_RODsyncMaxTrials);
      //m_grp->isManager()->publish("DSPMon_RODsyncMinIdle",m_RODsyncMinIdle);
      //m_grp->isManager()->publish("DSPMon_RODsyncISReadDelay",m_RODsyncISReadDelay);
      //m_grp->isManager()->publish("DSPMon_RODsyncErrThresh",m_RODsyncErrThresh);
      
      std::map<std::string, unsigned int>::iterator itQS;
      for (itQS = m_QSModeMap.begin(); itQS!=m_QSModeMap.end(); itQS++){
	std::string qs="";
	if ((*itQS).second==m_quickStatusMode){
	  qs=(*itQS).first;
	}
	if(qs!=""){
	  m_grp->isManager()->publish(m_grp->getISRoot()+"RODQSMode",qs);
	}
      }
      
      std::ostringstream os_qsis;
      os_qsis << "Clean up IS in IblCppMon init";
      ers::info(PixLib::pix::daq (ERS_HERE, "CppMon:QS " + rodConnName, os_qsis.str()));
      m_grp->isManager()->removeVariables("*QuickStatusAction");
      m_grp->isManager()->publish(m_grp->getISRoot()+"QS_DISABLED_MODULES","");
      for (std::map<std::string, std::vector<uint8_t>>::iterator il=m_links.begin(); il!=m_links.end(); il++) {
	// Deleting the existing IS variables
	m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/TIMEOUT");
	m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/TOT_DESYNC");
	m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/RESIDUAL_DESYNC");
	m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/FMTDesynchL1ID");
	m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/FMTDesynchBCID");
	m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/FMTOccupancy");
	m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/FMTBusyFrac");
	m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/FMTLinkUsage");
	m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/BOCFrameCount");
	m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/BOCDecodingError");
	m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/BOCFrameError");
	m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/nCFG");
	m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/nCFG_lastActiontime");
	m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/nRST");
	m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/nRST_lastActiontime");
	m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/HT");
	m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/MasterClock");
	m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/MasterTot");
	m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/MasterSlave0");
	m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/MasterSlave1");
	m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/MasterEfb");
	m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/MasterFmtMbFef0");
	m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/MasterFmtMbFef1");
	m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/MasterRolPause");
	m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/FMT_OCC");
	if( m_grp->getCtrlType() == CPPROD ){
	  m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/TIMEOUT_0");
	  m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/TIMEOUT_1");
	  m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/TOT_DESYNC_0");
	  m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/TOT_DESYNC_1");
	  m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/RESIDUAL_DESYNC_0");
	  m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/RESIDUAL_DESYNC_1");
	  m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/FMTOccupancy_0");
	  m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/FMTOccupancy_1");
	  m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/FMTBusyFrac_0");
	  m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/FMTBusyFrac_1");
	  m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/FMTLinkUsage_0");
	  m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/FMTLinkUsage_1");
	  m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/BOCFrameCount_0");
	  m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/BOCFrameCount_1");
	  m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/BOCDecodingError_0");
	  m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/BOCDecodingError_1");
	  m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/BOCFrameError_0");
	  m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/BOCFrameError_1");
	  m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/HT_0");
	  m_grp->isManager()->removeVariable(m_grp->getISRoot() + il->first + "/HT_1");
	}
	for(int i=0;i<4;i++){
	  for(int j=0;j<2;j++){
	    std::string hola=(j==0)?"TDAQ":"FTK";
	    std::string isVar_SLinkXOFF = m_grp->getISRoot()+"/SLINK"+std::to_string(i)+"_"+hola+"_XOFF";
	    std::string isVar_SLinkLDOWN = m_grp->getISRoot()+"/SLINK"+std::to_string(i)+"_"+hola+"_LDOWN";
	    m_grp->isManager()->removeVariable(isVar_SLinkXOFF);
	    m_grp->isManager()->removeVariable(isVar_SLinkLDOWN);
	  }
	}
      }
    } 
  } catch(...){
    ers::info(PixLib::pix::daq (ERS_HERE, "CppMon:QS " + rodConnName,"Exception in CppMon Init"));
  }

  // In the future can recyle the input config for RODsync to setup the delay threshold
  m_resetQSActionCounterDelay = 10; // Wait for 10 samplings before resetting the QS action counters
  m_resetQSActionCounterIdleCnt = 0;

  m_microBusyEna = true;
  m_microBusyCntThr = 10;
  m_microBusyFracThr = 0.01;
  m_microBusyMaxTrial = 5;

  for(int iMod=0;iMod<32;iMod++){
    m_microBusyCntArray[iMod] = 0;
    m_microBusyTrialCnt[iMod] = 0;
  }

  std::ostringstream os_qsmicrobusyconf;
  os_qsmicrobusyconf << "Micro-busy configuration: Enable = " << m_microBusyEna << ", MaxTrials = " << m_microBusyMaxTrial << ", microBusyFracThresh = " << m_microBusyFracThr << ", microBusyCntThresh = " << m_microBusyCntThr;
  ers::info(PixLib::pix::daq (ERS_HERE, "CppMon:QS " + rodConnName, os_qsmicrobusyconf.str()));

  for(int iMod=0;iMod<32;iMod++){
    m_nCFG_rodBusy[iMod][0]=m_nCFG_rodBusy[iMod][1]=0;
    m_nCFG_timeout[iMod][0]=m_nCFG_timeout[iMod][1]=0;
    m_nCFG_ht[iMod][0]=m_nCFG_ht[iMod][1]=0;
    m_nRST[iMod][0]=m_nRST[iMod][1]=0;

    m_nCFG_modSync[iMod]=0;
    m_nCFG_microBusy[iMod]=0;
    m_nCFG_SEU[iMod]=0;
    m_nCFG_userAction[iMod]=0;
  }


  // Modify the following part for the new readout
  if(m_grp->preAmpsOn() &&  m_quickStatusMode == QS_RST_CFG_OFF) m_quickStatusMode = QS_RST_CFG_ON;
  if(!m_grp->preAmpsOn() &&  m_quickStatusMode == QS_RST_CFG_ON) m_quickStatusMode = QS_RST_CFG_OFF;

  std::ostringstream os2;    
  os2 << "Sending QSTalkTask. QuickStatus will start in mode: "<<m_QSModeMap_names[m_quickStatusMode];
  ers::info(PixLib::pix::daq (ERS_HERE, "CppMon:QS " + rodConnName, os2.str()));
  setQSMode(m_quickStatusMode);		// Resetting QS variables may hang RCD. Need to understand better the reason before bring this feature back.

  if (m_quickStatusMode>QS_OFF){
    // MSC: Send primitive to start quick status
    try {
      QSTalkTask qsTalkTaskCmd;
      qsTalkTaskCmd.dumpOnPPC = true;
      // Set FC MAX
      qsTalkTaskCmd.qsttmode = QSTalkTask::QSTT_SET_FC;
      qsTalkTaskCmd.fc_max = m_fc_max;
      m_ctrl->sendCommand(qsTalkTaskCmd);
      //sleep(1);
      // Set BU MAX
      qsTalkTaskCmd.qsttmode = QSTalkTask::QSTT_SET_BU;
      qsTalkTaskCmd.bu_max = m_bu_max;
      m_ctrl->sendCommand(qsTalkTaskCmd);
      //sleep(1);
      // Set TO MAX
      qsTalkTaskCmd.qsttmode = QSTalkTask::QSTT_SET_TO;
      qsTalkTaskCmd.to_max = m_to_max;
      m_ctrl->sendCommand(qsTalkTaskCmd);
      //sleep(1);
      // Set NCFG MAX for BU
      qsTalkTaskCmd.qsttmode = QSTalkTask::QSTT_SET_BU_CFG;
      qsTalkTaskCmd.cfg_max = m_bu_cfg_max; // Recycle the variable
      m_ctrl->sendCommand(qsTalkTaskCmd);
      //sleep(1);
      // Set NCFG MAX for TO
      qsTalkTaskCmd.qsttmode = QSTalkTask::QSTT_SET_TO_CFG;
      qsTalkTaskCmd.cfg_max = m_to_cfg_max; // Recycle the variable
      m_ctrl->sendCommand(qsTalkTaskCmd);
      //sleep(1);

    } catch (SctPixelRod::BaseException &v) {
      ers::error(PixLib::pix::daq (ERS_HERE, "Caught Base Exception IblCppMonitorTask::initialize(): ", m_grp->rodConnectivity()->name()));
      std::cout << m_grp->rodConnectivity()->name() << " : BaseException caught in " << __PRETTY_FUNCTION__ << ": Type=" << v.getType() << " Descriptor=" << v.getDescriptor() << std::endl;
    } catch (...){
      ers::error(PixLib::pix::daq (ERS_HERE, "Caught Unknown Exception IblCppMonitorTask::initialize(): ", m_grp->rodConnectivity()->name()));
      std::cout << m_grp->rodConnectivity()->name() <<" : Unknown exception caught in " << __PRETTY_FUNCTION__ << std::endl;
    }
  }
  return true;
}

void IblCppMonitorTask::executeQSActions() {
  std::cout << __PRETTY_FUNCTION__ << std::endl;
  // pick up any QS action from IS and execute this action
  std::vector<std::string> qsActionList = m_grp->isManager()->listVariables("*QuickStatusAction");

  std::string rodX = m_grp->getName();
  if (!qsActionList.empty()) {
    std::cout << "qsActionList: " << qsActionList.size() << std::endl;
    for (size_t i=0; i<qsActionList.size(); i++) {
      std::string var = qsActionList[i];
      std::cout << var << std::endl;
      if (var.substr(0, var.find("/")) == rodX) {
        std::cout << "MyROD" << std::endl;
        std::string whichAction = var.substr(var.find("/")+1, var.length());
        whichAction = whichAction.substr(0, whichAction.find("/"));
        std::cout << "whichAction=" << whichAction << std::endl;

        if (whichAction == "QS_SINGLE_CFG") {
          if (m_grp->preAmpsOn())
            whichAction = "QS_SINGLE_CFG_ON";
          else
            whichAction = "QS_SINGLE_CFG_OFF";
        }

        std::string actionItem = m_grp->isManager()->read<std::string>(var);
        uint32_t item = atol(actionItem.c_str());
        m_grp->isManager()->removeVariable(var);

        // TODO: this part will be nicer once all QS parameters are stored in a struct and easier to handle
        // actions which should be done automatically

        // Checking if QS_MODE is to be changed
        std::cout << "whichAction=" << whichAction << std::endl;
        std::cout << whichAction.find("QS_") << "\t" <<  whichAction.find("QS_SINGLE") << std::endl;
        if(whichAction.find("QS_") == 0 && whichAction.find("QS_SINGLE") == std::string::npos) {
	  // Protection to make sure the QS action is corresponding to pre-amplifier status
	  if ((whichAction == "QS_RST_CFG_ON") && !m_grp->preAmpsOn()){
	    std::cout << "Pre-amps off. Switching QS mode request from QS_RST_CFG_ON to QS_RST_CFG_OFF" << std::endl;
	    whichAction="QS_RST_CFG_OFF";
	  }
	  if ((whichAction == "QS_RST_CFG_OFF") && m_grp->preAmpsOn()){
	    std::cout << "Pre-amps on. Switching QS mode request from QS_RST_CFG_OFF to QS_RST_CFG_ON" << std::endl;
	    whichAction="QS_RST_CFG_ON";
	  }

          std::cout << "whichAction=" << whichAction << std::endl;
	  if(m_QSModeMap.find(whichAction)!=m_QSModeMap.end()) setQSMode(m_QSModeMap[whichAction]);
	  else{
	    std::cout << "WARNING: Unknown QS mode " << whichAction << std::endl;
	  }
        }

        // Prevent non mode-setting actions from running
        if( m_quickStatusMode <= QS_IDLE ) continue;

        QSTalkTask qsTalkTaskCmd;
        qsTalkTaskCmd.dumpOnPPC = true;

        if (whichAction == "RESAMPLING") {
          qsTalkTaskCmd.qsttmode = QSTalkTask::QSTT_USERACTION;
          qsTalkTaskCmd.module_mask = item;
          qsTalkTaskCmd.action = QuickStatus::MON_RESAMPLE_PHASE;
	  m_ctrl->sendCommand(qsTalkTaskCmd);
        }
	else if ( whichAction == "QS_SINGLE_CFG_ON" || whichAction == "QS_SINGLE_CFG_OFF" || whichAction == "RECONFIGURE" || 
          "QS_SINGLE_EXPERT_CFG_ON" || "QS_SINGLE_EXPERT_CFG_OFF" ||  whichAction == "QS_SINGLE_ENA" ||  whichAction == "QS_SINGLE_DIS" ){
	  bool isValidAction=false;
          uint32_t moduleEnabled = 0;
	  item = strtol(actionItem.c_str(), NULL, 0);
	  // Reconfigure using bit mask, which correspond to a string starts with "0x" (otherwise cannot be recognized)
	  if(actionItem.find("0x") == 0){
	    std::cout<<"Mask in string: "<<actionItem<<", converted into mask in uint32_t: "<<HEXF(8, item)<<std::endl;
	    // No need to worry that the input bit mask contains disabled links. Such links will be vetoed by PPC SW
            uint32_t runActiveMask = m_grp->getPixController()->getRunMask();
            if(whichAction == "QS_SINGLE_CFG_ON" || whichAction == "QS_SINGLE_CFG_OFF" || whichAction == "RECONFIGURE") moduleEnabled = item & m_qsMonInfos.fmtLinkEn & runActiveMask;
            else moduleEnabled = item & runActiveMask;
            qsTalkTaskCmd.module_mask = (moduleEnabled);
	  }
	  // Reconfigure using DCS module name
          else{
	    // Loop over modules registered to find the corresponding link number
	    bool moduleFound=false;
	    std::map<std::string, std::vector<uint8_t>>::iterator il = m_links.find(actionItem);

	     if (il != m_links.end()){
                moduleFound=true;
		uint32_t linkMask =0;
		for(size_t s=0;s<il->second.size();s++)linkMask |= 0x1 << il->second[s];
		
		// Convert DCS name into bit mask
                uint32_t runActiveMask = m_grp->getPixController()->getRunMask();
		if(whichAction == "QS_SINGLE_CFG_ON" || whichAction == "QS_SINGLE_CFG_OFF" || whichAction == "RECONFIGURE") moduleEnabled = linkMask & m_qsMonInfos.fmtLinkEn & runActiveMask;
		else moduleEnabled = linkMask & runActiveMask;
	        qsTalkTaskCmd.module_mask = (moduleEnabled);
            }
            if (moduleFound==false){ers::warning(PixLib::pix::daq (ERS_HERE,"CppMon:QS Module not FOUND, check Module name and ROD  ",actionItem));}
	  }
	  if(qsTalkTaskCmd.module_mask !=0) isValidAction=true;
	  // If the bit mask is valid or the DCS module name can be found
	  if(isValidAction){
	    qsTalkTaskCmd.qsttmode = QSTalkTask::QSTT_USERACTION;
	    if (  whichAction == "QS_SINGLE_CFG_ON" || whichAction == "QS_SINGLE_CFG_OFF" || whichAction == "RECONFIGURE" || whichAction == "QS_SINGLE_EXPERT_CFG_ON" || whichAction == "QS_SINGLE_EXPERT_CFG_OFF" ){
               qsTalkTaskCmd.action = QuickStatus::MON_RECONFIGURE;
	       // Record reconfiguration action on each link
               for(int iMod=0;iMod<32;iMod++)
	          if((qsTalkTaskCmd.module_mask & (0x1<<iMod))!=0) m_nCFG_userAction[iMod]++;
            }
	    if (  whichAction == "QS_SINGLE_ENA" && !moduleEnabled ) 
               ers::warning(PixLib::pix::daq (ERS_HERE,"CppMon:QS Module ENABLED by user action: ",actionItem));
	    if (  whichAction == "QS_SINGLE_DIS" && moduleEnabled ) 
               ers::warning(PixLib::pix::daq (ERS_HERE,"CppMon:QS Module DISABLED by user action: ",actionItem));
            if (  (whichAction == "QS_SINGLE_CFG_ON" || whichAction == "QS_SINGLE_CFG_OFF") && moduleEnabled )
               ers::warning(PixLib::pix::daq (ERS_HERE,"CppMon:QS Module RECONFIGURED by user action: ",actionItem));

	    if (  whichAction == "QS_SINGLE_EXPERT_CFG_ON" ) 
               ers::warning(PixLib::pix::daq (ERS_HERE,"CppMon:QS Module reconfigured to preAmps ON by user expert action: ",actionItem));
	    if (  whichAction == "QS_SINGLE_EXPERT_CFG_OFF" ) 
               ers::warning(PixLib::pix::daq (ERS_HERE,"CppMon:QS Module reconfigured to preAmps OFF by user expert action: ",actionItem));

	    if (  whichAction == "QS_SINGLE_ENA" )  qsTalkTaskCmd.action = QuickStatus::MON_ENABLE;
	    if (  whichAction == "QS_SINGLE_DIS" )  qsTalkTaskCmd.action = QuickStatus::MON_DISABLE;
	    if (  whichAction == "QS_SINGLE_EXPERT_CFG_ON" )  qsTalkTaskCmd.action = QuickStatus::MON_EXPERT_RECONFIGURE_ON;
	    if (  whichAction == "QS_SINGLE_EXPERT_CFG_OFF" )  qsTalkTaskCmd.action = QuickStatus::MON_EXPERT_RECONFIGURE_OFF;
	    m_ctrl->sendCommand(qsTalkTaskCmd);
	  }
	  else{
	    std::string rodConnName = m_grp->rodConnectivity()->name();
	    std::string msg = "Invalid action item \"" +actionItem+"\" detected. No action will be taken!";
	    ers::warning(PixLib::pix::daq (ERS_HERE, rodConnName,msg));
	  }
	}
        // TODO: add all other possible options here once they are supported by QS
      } // correct ROD
    } // loop over variable list
  } // qsActionList has entries
}

void IblCppMonitorTask::analyseBusyAndTimeoutError() {
  //std::cout << __PRETTY_FUNCTION__ << std::endl;
  // Note: BUSY data is provided as a function of time (first element in array), so we normalize it

  // Determining BUSY status

  // Checking S-LINK status

  // Todo: use enum for position in array
  // Todo: get the actual S-LINK status if the counters are NULL ?
  const int slvBusyCntIdx=1, slvClockCntIdx=0;
  const uint32_t slaveBusyClockCnt[2] = {m_qsMonInfos.slaveBusyCounters[slvClockCntIdx], m_qsMonInfos.slaveBusyCounters[slvClockCntIdx+32]};

  // Lambda to easily change logic
  auto isDownOrXOff = [](uint32_t cnt, uint32_t clk) {
    // Subtractions faster than division: using them since we done need the exact ratio
    if (clk >0)return (cnt*100/clk) > 0;
    else return false;
  };

  bool sLink0XOff = isDownOrXOff(m_qsMonInfos.slaveBusyCounters[2], slaveBusyClockCnt[0]) || isDownOrXOff(m_qsMonInfos.slaveBusyCounters[2+32], slaveBusyClockCnt[1]);
  bool sLink0Down = isDownOrXOff(m_qsMonInfos.slaveBusyCounters[3], slaveBusyClockCnt[0]) || isDownOrXOff(m_qsMonInfos.slaveBusyCounters[3+32], slaveBusyClockCnt[1]);
  bool sLink1XOff=false, sLink1Down=false;
  if(m_grp->getCtrlType() == CPPROD){
    sLink1XOff = isDownOrXOff(m_qsMonInfos.slaveBusyCounters[4], slaveBusyClockCnt[0]) || isDownOrXOff(m_qsMonInfos.slaveBusyCounters[4+32], slaveBusyClockCnt[1]);
    sLink1Down = isDownOrXOff(m_qsMonInfos.slaveBusyCounters[5], slaveBusyClockCnt[0]) || isDownOrXOff(m_qsMonInfos.slaveBusyCounters[5+32], slaveBusyClockCnt[1]);
  }
  bool sLinkXOffOrDown = sLink0XOff || sLink0Down || sLink1XOff || sLink1Down;
  
  int busyPerc = 0;
  if(slaveBusyClockCnt[0])
    busyPerc = 100 * ((double)m_qsMonInfos.slaveBusyCounters[slvBusyCntIdx]/slaveBusyClockCnt[0]);
  if(slaveBusyClockCnt[1])
    busyPerc += 100 * ((double)m_qsMonInfos.slaveBusyCounters[32+slvBusyCntIdx]/slaveBusyClockCnt[1]);
  if(busyPerc>100) busyPerc=100;

  if (!(m_qsMonInfos.masterBusy & 0x1)) busyPerc=100; // Master busy

  if (sLinkXOffOrDown) busyPerc += 1000;
  if (m_grp->rodDisabled()) busyPerc = 0; // a disabled ROD cannot be busy

  double sLinkN0XOffPerc = 0;
  if(slaveBusyClockCnt[0] >0)sLinkN0XOffPerc = (double)m_qsMonInfos.slaveBusyCounters[2]*100./(double)slaveBusyClockCnt[0];
  double sLinkS0XOffPerc = 0;
  if(slaveBusyClockCnt[1] >0)sLinkS0XOffPerc = (double)m_qsMonInfos.slaveBusyCounters[32+2]*100./(double)slaveBusyClockCnt[1];
  double sLinkN1XOffPerc = 0;
  if(slaveBusyClockCnt[0] >0)sLinkN1XOffPerc = (double)m_qsMonInfos.slaveBusyCounters[4]*100./(double)slaveBusyClockCnt[0];
  double sLinkS1XOffPerc = 0;
  if(slaveBusyClockCnt[1] >0)sLinkS1XOffPerc = (double)m_qsMonInfos.slaveBusyCounters[32+4]*100./(double)slaveBusyClockCnt[1];
  
  if( m_grp->getCtrlType() == CPPROD ){
  m_grp->isManager()->publish(m_grp->getISRoot() + "SLINK_N_0_BUSYPERC", sLinkN0XOffPerc);
  m_grp->isManager()->publish(m_grp->getISRoot() + "SLINK_N_1_BUSYPERC", sLinkN1XOffPerc);
  m_grp->isManager()->publish(m_grp->getISRoot() + "SLINK_S_0_BUSYPERC", sLinkS0XOffPerc);
  m_grp->isManager()->publish(m_grp->getISRoot() + "SLINK_S_1_BUSYPERC", sLinkS1XOffPerc);
  } else {
  m_grp->isManager()->publish(m_grp->getISRoot() + "SLINK_N_BUSYPERC", sLinkN0XOffPerc);
  m_grp->isManager()->publish(m_grp->getISRoot() + "SLINK_S_BUSYPERC", sLinkS0XOffPerc);
  }
  
  double EFB_N_BusyPerc[4] = {0,0,0,0};
  if(slaveBusyClockCnt[0] >0)
  for(int i=0;i<4;i++)EFB_N_BusyPerc[i] = (double)m_qsMonInfos.slaveBusyCounters[22+i]*100./(double)slaveBusyClockCnt[0];
  
  double EFB_S_BusyPerc[4] = {0,0,0,0};
  if(slaveBusyClockCnt[1] >0)
  for(int i=0;i<4;i++)EFB_S_BusyPerc[i] = (double)m_qsMonInfos.slaveBusyCounters[32+22+i]*100./(double)slaveBusyClockCnt[1];

  for(int i=0;i<4;i++){
  std::string varName = m_grp->getISRoot() + "EFB_N_"+ std::to_string(i) +"_BUSYPERC";
  m_grp->isManager()->publish(varName ,EFB_N_BusyPerc[i] );
  
  varName = m_grp->getISRoot() + "EFB_S_"+ std::to_string(i) +"_BUSYPERC";
  m_grp->isManager()->publish(varName ,EFB_S_BusyPerc[i] );
  }

  double RTR_N_BusyPerc[2] = {0,0};
  if(slaveBusyClockCnt[0] >0)
  for(int i=0;i<2;i++)RTR_N_BusyPerc[i] = (double)m_qsMonInfos.slaveBusyCounters[26+i]*100./(double)slaveBusyClockCnt[0];
  
  double RTR_S_BusyPerc[2] = {0,0};
  if(slaveBusyClockCnt[0] >0)
  for(int i=0;i<2;i++)RTR_S_BusyPerc[i] = (double)m_qsMonInfos.slaveBusyCounters[32+26+i]*100./(double)slaveBusyClockCnt[1];

  for(int i=0;i<2;i++){
  std::string varName = m_grp->getISRoot() + "RTR_N_"+ std::to_string(i) +"_BUSYPERC";
  m_grp->isManager()->publish(varName ,RTR_N_BusyPerc[i] );

  varName = m_grp->getISRoot() + "RTR_S_"+ std::to_string(i) +"_BUSYPERC";
  m_grp->isManager()->publish(varName ,RTR_S_BusyPerc[i] );
  }


  std::string rodConnName = m_grp->rodConnectivity()->name();
  std::stringstream os_qsinfo;
  os_qsinfo << "busyPerc = " << busyPerc << std::endl;

  m_grp->isManager()->publish(m_grp->getISRoot() + "BUSY", busyPerc);
  m_grp->setRodBusy(busyPerc);

  double avgOcc = std::accumulate( m_qsMonInfos.linkOccCounters, m_qsMonInfos.linkOccCounters + 32, 0.);
  uint32_t nActiveLinks = std::count_if(m_qsMonInfos.linkTrigCounters,m_qsMonInfos.linkTrigCounters + 32, [](int n){return n > 0;});
  // Alternative: populate m_qsMonInfos.nTriggersOccCounters in PPC QuickStatus with Master triggers (but need to get it right)
  uint32_t nTriggers = std::accumulate(m_qsMonInfos.linkTrigCounters, m_qsMonInfos.linkTrigCounters + 32, 0.) / nActiveLinks;

  constexpr float nPixInFei3 = 16. * 18. * 160.;
  constexpr float nPixInFei4 = 336. * 80.;

  // Normalize the occupancy per trigger and per pixel
  if( nTriggers && nActiveLinks ) {
    avgOcc /= nTriggers; // Divide by the number if triggers
    avgOcc /= nActiveLinks; // Fix me: add the actual number of modules form connectivity
    // To do: use the actual number of enabled pixels ?
    if( m_grp->getCtrlType() == CPPROD ) // IBL or DBM
      avgOcc /= nPixInFei4;
    else
      avgOcc /= nPixInFei3;
  }

  os_qsinfo.str(std::string());
  m_grp->isManager()->publish(m_grp->getISRoot()+"AvgOcc", avgOcc);
  os_qsinfo << "avgOcc = " << avgOcc << std::endl;

  // Note: this is per module link (1 MCC for pixel or 1 FE-I4 for IBL)
  float nPixInMod = (m_grp->getCtrlType() == CPPROD) ? nPixInFei4 : nPixInFei3;

  // Todo: monitor the ID variables

  // Todo: publish per-link occupancy (not supported yet)

  // publishIblRodFmtStat(uint32_t iblRodFmtLinkEnable, uint32_t iblRodFmtBusyStatus, uint32_t fmtBusyFromEfb);
  float iblRodLinkBusyThresh = .99; // Not in percent to avoid multiplying by 100
  const uint32_t& iblRodFmtLinkEnable = m_qsMonInfos.fmtLinkEn;
  uint32_t iblRodFmtBusyStatus = 0x0;
  uint8_t eBM = 0x0; // EFB BUSY mask
  uint8_t rBM = 0x0; // RTR BUSY mask

  // Publishing tot desynchronization and residual desynchronization counters
  //std::cout << "IBLMon: " << m_links.size() << "  " << m_links.empty() << std::endl;

  for (std::map<std::string, std::vector<uint8_t>>::iterator il=m_links.begin(); il!=m_links.end(); il++){
    uint32_t BOCFrameCount = 0,BOCDecodingError=0,BOCFrameError=0;
    double fmtOcc = 0, busyFrac = 0,BOCMeanOcc = 0,timeoutCounter = 0,htCounter = 0,totDesync=0;

    for(size_t s=0;s<il->second.size();s++){
    // ++++++++++++++++ Total desynchronization ++++++++++++++++
    std::string isVar_totDesync = m_grp->getISRoot() + il->first + "/TOT_DESYNC";
    int rxCh = il->second[s];
    int feN = m_ctrl->getFeNumberFromRx(rxCh);//Check for IBL feN
    if( feN!=-1) isVar_totDesync +="_" + std::to_string(feN);
    double desynch =0;
    if(m_qsMonInfos.linkTrigCounters[rxCh] > 0)desynch = 100.*m_qsMonInfos.totDesyncCounters[rxCh]/(double)m_qsMonInfos.linkTrigCounters[rxCh];
    totDesync += desynch;

    m_grp->isManager()->publish(isVar_totDesync, desynch);
    if(s == (il->second.size() -1) && m_grp->getCtrlType() == CPPROD )m_grp->isManager()->publish(m_grp->getISRoot() + il->first + "/TOT_DESYNC", totDesync/(double)il->second.size() );

    // ++++++++++++++++ FMT occupancy ++++++++++++++++
    std::string isVar_FMTOccupancy = m_grp->getISRoot()+ il->first +"/FMTOccupancy";
    if( feN!=-1) isVar_FMTOccupancy += "_" + std::to_string(feN);
    double occ=0;
    if(m_qsMonInfos.linkTrigCounters[rxCh] > 0)occ = m_qsMonInfos.linkOccCounters[rxCh]/(double)m_qsMonInfos.linkTrigCounters[rxCh];
    m_grp->isManager()->publish(isVar_FMTOccupancy, occ/nPixInMod);
    fmtOcc += occ;
    if(s == (il->second.size() -1)  && m_grp->getCtrlType() == CPPROD ){
       m_grp->isManager()->publish(m_grp->getISRoot()+ il->first +"/FMTOccupancy", fmtOcc/(double)il->second.size()/nPixInMod);
         //Publish of 3DBM occ
         if(!m_grp->preAmpsOn()){
           std::string moduleName = il->first;
             if(moduleName == "LI_S01_C_M4_C8" ||moduleName == "LI_S02_C_M4_C8" ||moduleName == "LI_S03_C_M4_C8" || moduleName =="LI_S05_C_M4_C8" || moduleName =="LI_S06_C_M4_C8" || moduleName == "LI_S07_C_M4_C8" || moduleName == "LI_S09_C_M4_C8" || moduleName == "LI_S10_C_M4_C8" || moduleName == "LI_S11_C_M4_C8" || moduleName == "LI_S12_C_M4_C8" || moduleName == "LI_S13_C_M4_C8"){
               std::string isVar_3DBM = m_grp->getISRoot()+ moduleName +"/3DBMOcc";
               m_grp->isManager()->publish(isVar_3DBM, fmtOcc/(double)il->second.size()/672.);
             }
         }
    }

    // ++++++++++++++++ FMT busy fraction ++++++++++++++++
    std::string isVar_FMTBusyFrac = m_grp->getISRoot()+il->first+"/FMTBusyFrac";
    if( feN!=-1) isVar_FMTBusyFrac += "_" + std::to_string(feN);
    double busyF=0;

    int sId=rxCh/16;
    int cntIdx = sId*32+6+rxCh%16;
    if(slaveBusyClockCnt[sId]>0)busyF = double(m_qsMonInfos.slaveBusyCounters[cntIdx])/double(slaveBusyClockCnt[sId]);
    m_grp->isManager()->publish(isVar_FMTBusyFrac, busyF);
    if(busyFrac < busyF)busyFrac = busyF;//Get max busy for IBL
    if(s == (il->second.size() -1) && m_grp->getCtrlType() == CPPROD )m_grp->isManager()->publish( m_grp->getISRoot()+il->first+"/FMTBusyFrac" , busyFrac);

    // ++++++++++++++++ BOC mean occupancy ++++++++++++++++
    std::string isVar_BOCMeanOcc = m_grp->getISRoot()+il->first+"/FMTLinkUsage";
    if( feN!=-1) isVar_BOCMeanOcc += "_" + std::to_string(feN);
    
    m_grp->isManager()->publish(isVar_BOCMeanOcc,  double((100.)*m_qsMonInfos.meanOccBOC[rxCh])/65535.);
    BOCMeanOcc +=  double((100.)*m_qsMonInfos.meanOccBOC[rxCh])/65535.;
    if(s == (il->second.size() -1) && m_grp->getCtrlType() == CPPROD )m_grp->isManager()->publish(m_grp->getISRoot()+il->first+"/FMTLinkUsage" , BOCMeanOcc/il->second.size());

    // ++++++++++++++++ BOC frame count ++++++++++++++++
    std::string isVar_BOCFrameCount = m_grp->getISRoot()+il->first+"/BOCFrameCount";
    if( feN!=-1) isVar_BOCFrameCount += "_" + std::to_string(feN);
    BOCFrameCount += m_qsMonInfos.frameCountBOC[rxCh];
    m_grp->isManager()->publish(isVar_BOCFrameCount, m_qsMonInfos.frameCountBOC[rxCh]);
    if(s == (il->second.size() -1) && m_grp->getCtrlType() == CPPROD )m_grp->isManager()->publish(m_grp->getISRoot()+il->first+"/BOCFrameCount" , BOCFrameCount/2);

    if ( m_grp->getCtrlType() == CPPROD ){//Only present for IBL
    // ++++++++++++++++ BOC decoding error ++++++++++++++++
      std::string isVar_BOCDecodingError = m_grp->getISRoot()+il->first+"/BOCDecodingError";
      if( feN!=-1) isVar_BOCDecodingError += "_" + std::to_string(feN);
      BOCDecodingError += m_qsMonInfos.decodingErrorBOC[rxCh];
      m_grp->isManager()->publish(isVar_BOCDecodingError, m_qsMonInfos.decodingErrorBOC[rxCh]);
      if(s == (il->second.size() -1))m_grp->isManager()->publish(m_grp->getISRoot()+il->first+"/BOCDecodingError" , BOCDecodingError/il->second.size());
    
      // ++++++++++++++++ BOC frame error ++++++++++++++++
      std::string isVar_BOCFrameError = m_grp->getISRoot()+il->first+"/BOCFrameError";
      if( feN!=-1) isVar_BOCFrameError += "_" + std::to_string(feN);
      BOCFrameError += m_qsMonInfos.decodingErrorBOC[rxCh];
      m_grp->isManager()->publish(isVar_BOCFrameError, m_qsMonInfos.decodingErrorBOC[rxCh]);
      if(s == (il->second.size() -1))m_grp->isManager()->publish(m_grp->getISRoot()+il->first+"/BOCFrameError" , BOCFrameError/il->second.size());
    } 

  // ++++++++++++++++ Timeout  counters ++++++++++++++++
    std::string isVar_timeoutCounter = m_grp->getISRoot() + il->first + "/TIMEOUT";
    if( feN!=-1) isVar_timeoutCounter += "_" + std::to_string(feN);
    double timeout=0;
    if(m_qsMonInfos.linkTrigCounters[rxCh] > 0 || m_qsMonInfos.timeoutCounters[rxCh])timeout = 100.*m_qsMonInfos.timeoutCounters[rxCh]/(double)(m_qsMonInfos.linkTrigCounters[rxCh] + m_qsMonInfos.timeoutCounters[rxCh]);
    timeoutCounter += timeout;
    m_grp->isManager()->publish(isVar_timeoutCounter, timeout);
    if(s == (il->second.size() -1) && m_grp->getCtrlType() == CPPROD )m_grp->isManager()->publish( m_grp->getISRoot() + il->first + "/TIMEOUT" , timeout/(double)il->second.size());
    //Debug info
    if (m_qsMonInfos.timeoutCounters[rxCh] > 10)ers::info(PixLib::pix::daq (ERS_HERE, " CppMon:QS timeout counter module " + il->first, " and rxCh "+ std::to_string(rxCh)+ " with "+ std::to_string(timeout) + "timeouts" ));

    // ++++++++++++++++ HT    counters ++++++++++++++++
    std::string isVar_htCounter = m_grp->getISRoot() + il->first + "/HT";
    if( feN!=-1) isVar_htCounter += "_" + std::to_string(feN);
    double ht=0;
    if(m_qsMonInfos.linkTrigCounters[rxCh] > 0 || m_qsMonInfos.htCounters[rxCh])ht = 100.*m_qsMonInfos.htCounters[rxCh]/(double)(m_qsMonInfos.linkTrigCounters[rxCh] + m_qsMonInfos.htCounters[rxCh]);
    htCounter += ht;
    m_grp->isManager()->publish(isVar_htCounter, ht);
    if(s == (il->second.size() -1) && m_grp->getCtrlType() == CPPROD )m_grp->isManager()->publish( m_grp->getISRoot() + il->first + "/HT" , htCounter/(double)il->second.size());
    
    }
  }

   // ++++++++++++++++ master Busy  counters ++++++++++++++++
    
    std::string isVar_BusyClkCntRegValue = m_grp->getISRoot() + "/MasterClock";
    std::string isVar_BusyTotCntRegValue = m_grp->getISRoot() + "/MasterTot";
    std::string isVar_BusySlv0CntRegValue = m_grp->getISRoot() + "/MasterSlave0";
    std::string isVar_BusySlv1CntRegValue = m_grp->getISRoot() + "/MasterSlave1";
    std::string isVar_BusyEfbHeaderpauseCntRegValue = m_grp->getISRoot() + "/MasterEfb";
    std::string isVar_BusyFmtMbFef0CntRegValue = m_grp->getISRoot() + "/MasterFmtMbFef0";
    std::string isVar_BusyFmtMbFef1CntRegValue = m_grp->getISRoot() + "/MasterFmtMbFef1";
    std::string isVar_BusyRolPauseCntRegValue = m_grp->getISRoot() + "/MasterRolPause";

    uint32_t BusyClkCntRegValue = 0; 
    float BusyTotCntRegValue = 0, BusySlv0CntRegValue = 0, BusySlv1CntRegValue = 0, 
     BusyEfbHeaderpauseCntRegValue = 0, BusyFmtMbFef0CntRegValue = 0, BusyFmtMbFef1CntRegValue = 0, BusyRolPauseCntRegValue =0 ;

    BusyClkCntRegValue = m_qsMonInfos.masterBusyCounters[0];
    m_grp->isManager()->publish(isVar_BusyClkCntRegValue, BusyClkCntRegValue);

    if (BusyClkCntRegValue > 0) BusyTotCntRegValue = float (m_qsMonInfos.masterBusyCounters[1])/float(BusyClkCntRegValue);
    else BusyTotCntRegValue = 0;
    m_grp->isManager()->publish(isVar_BusyTotCntRegValue, BusyTotCntRegValue);

    if (BusyClkCntRegValue > 0) BusySlv0CntRegValue = float (m_qsMonInfos.masterBusyCounters[2])/float(BusyClkCntRegValue);
    else BusySlv0CntRegValue = 0;
    m_grp->isManager()->publish(isVar_BusySlv0CntRegValue, BusySlv0CntRegValue);

    if (BusyClkCntRegValue > 0) BusySlv1CntRegValue = float (m_qsMonInfos.masterBusyCounters[3])/float(BusyClkCntRegValue);
    else BusySlv1CntRegValue = 0;
    m_grp->isManager()->publish(isVar_BusySlv1CntRegValue, BusySlv1CntRegValue);

    if (BusyClkCntRegValue > 0) BusyEfbHeaderpauseCntRegValue = float (m_qsMonInfos.masterBusyCounters[4])/float(BusyClkCntRegValue);
    else BusyEfbHeaderpauseCntRegValue = 0;
    m_grp->isManager()->publish(isVar_BusyEfbHeaderpauseCntRegValue, BusyEfbHeaderpauseCntRegValue);

    if (BusyClkCntRegValue > 0) BusyFmtMbFef0CntRegValue = float (m_qsMonInfos.masterBusyCounters[5])/float(BusyClkCntRegValue);
    else BusyFmtMbFef0CntRegValue = 0;
    m_grp->isManager()->publish(isVar_BusyFmtMbFef0CntRegValue, BusyFmtMbFef0CntRegValue);

    if (BusyClkCntRegValue > 0) BusyFmtMbFef1CntRegValue = float (m_qsMonInfos.masterBusyCounters[6])/float(BusyClkCntRegValue);
    else BusyFmtMbFef0CntRegValue = 0;
    m_grp->isManager()->publish(isVar_BusyFmtMbFef1CntRegValue, BusyFmtMbFef1CntRegValue);

    if (BusyClkCntRegValue > 0) BusyRolPauseCntRegValue = float (m_qsMonInfos.masterBusyCounters[7])/float(BusyClkCntRegValue);
    else BusyRolPauseCntRegValue = 0;
    m_grp->isManager()->publish(isVar_BusyRolPauseCntRegValue, BusyRolPauseCntRegValue);

  
  // ++++++++++++++++ S-link counters ++++++++++++++++
  // HOLA0: TDAQ, HOLA1: FTK
  for(int i=0;i<4;i++){
    for(int j=0;j<2;j++){
      std::string hola=(j==0)?"TDAQ":"FTK";
      std::string isVar_SLinkXOFF = m_grp->getISRoot()+"/SLINK"+std::to_string(i)+"_"+hola+"_XOFF";
      std::string isVar_SLinkLDOWN = m_grp->getISRoot()+"/SLINK"+std::to_string(i)+"_"+hola+"_LDOWN";
      m_grp->isManager()->publish(isVar_SLinkXOFF, m_qsMonInfos.SLinkInfo.sLinkCounters[i].Hola[j].Lff);
      m_grp->isManager()->publish(isVar_SLinkLDOWN, m_qsMonInfos.SLinkInfo.sLinkCounters[i].Hola[j].LDown);
    }
  }
  //static size_t myCnt = 0;
 //}

  for(size_t sId = 0 ; sId < 2; ++sId) {
    for(size_t i = 0 ; i < 16 ; i++) {
      if( ((float)m_qsMonInfos.slaveBusyCounters[sId*32+6+i] / slaveBusyClockCnt[sId]) >= iblRodLinkBusyThresh )
        iblRodFmtBusyStatus |= 0x1 << (sId*16+i);
    }
    for(size_t i = 0 ; i < 4 ; i++) {
      if( ((float)m_qsMonInfos.slaveBusyCounters[sId*32+22+i] / slaveBusyClockCnt[sId]) >= iblRodLinkBusyThresh )
       eBM |= 0x1 << (sId*4+i);
    }
    for(size_t i = 0 ; i < 2 ; i++) {
      if( ((float)m_qsMonInfos.slaveBusyCounters[sId*32+26+i] / slaveBusyClockCnt[sId]) >= iblRodLinkBusyThresh )
       rBM |= 0x1 << (sId*2+i);
    }
  }
  //std::cout << rodConnName << " -- eBM: " << HEXF(8,eBM) << std::endl;
  //std::cout << rodConnName << " -- rBM: " << HEXF(8,rBM) << std::endl;

  uint32_t fmtBusyFromEfb = 0x0;
  // update the fmtBusyMask based on the EFBs that are busy
  for (uint32_t i=0; i<8; i++) {
    if (i) {
      fmtBusyFromEfb <<= 4;
    }
    if (eBM & 0x80) {
      fmtBusyFromEfb |= 0xF;
    }
    eBM <<= 1;
  }

  uint32_t fmtBusyFromRtr = 0x0;
  for (uint32_t i=0; i<4; i++) {
    if (i) {
      fmtBusyFromEfb <<= 8;
    }
    if (rBM & 0x08) {
      fmtBusyFromEfb |= 0xFF;
    }
    rBM <<= 1;
  }
  uint32_t fmtBusyFromEfbOrRtr = fmtBusyFromEfb | fmtBusyFromRtr;

  //std::cout << rodConnName << " -- eBM: " << HEXF(8,eBM) << std::endl;
  publishIblRodFmtStat(iblRodFmtLinkEnable, iblRodFmtBusyStatus, fmtBusyFromEfbOrRtr, m_qsMonInfos.waitingForECR);
  if(busyPerc>1 || fmtBusyFromEfbOrRtr || (!(m_qsMonInfos.masterBusy & 0x1)) || sLinkXOffOrDown) {
    
    std::cout << std::endl<<m_grp->getRodName() << "Busy percentage "<<(int)busyPerc << std::endl;
    std::cout << m_grp->getRodName() << " -- " << std::hex << "Master busy = " << HEXF(4, m_qsMonInfos.masterBusy) << std::endl;   
    std::cout << m_grp->getRodName() << " -- " << "busyPerc = " << busyPerc << std::endl;
    std::cout << m_grp->getRodName() << " -- " << std::hex << "sLinkXOffOrDown = " << sLinkXOffOrDown << std::endl;
    std::cout << m_grp->getRodName() << " -- " << "avgOcc = " << avgOcc << std::endl;
    std::cout << m_grp->getRodName() << " -- " << std::hex << "fmtBusyFromEfb = " << HEXF(8, fmtBusyFromEfb) << std::endl;
    std::cout << m_grp->getRodName() << " -- " << std::hex << "fmtBusyFromRtr = " << HEXF(8, fmtBusyFromRtr) << std::endl << std::endl;

    dumpBusyRegisters();
  }
}

bool IblCppMonitorTask::readStatus(){
  //std::cout << __PRETTY_FUNCTION__ << std::endl;

  if (m_grp->isManager() != NULL) {
    try{
      std::string rodX = m_grp->getName();
      std::string rodConnName = m_grp->rodConnectivity()->name();
      
      executeQSActions();
      
      if (m_quickStatusMode<QS_MON)
	{
	  std::cout<<"QS is not running in QS_MON!"<<std::endl;
	  std::cout<<"QS_MON is "<<QS_MON<<"and you are running in"<<m_quickStatusMode<<std::endl;
	}
      

      
      if(m_quickStatusMode<QS_MON) return readIblRodFmtStatus();
      publishQSMode();
    
      // initialize QS
      string layer = "Layer";
      if (m_links.size()>0){
	std::map<std::string, std::vector<uint8_t>>::iterator il=m_links.begin();
	layer += (il->first)[1];
      }

      std::ostringstream syncMsg;	
      unsigned int isVal = m_resetQSActionCounterDelay;
      if(m_grp->isManager()->exists(rodX+"/CppMon_resetQSActionCounterDelay")) isVal = m_grp->isManager()->read<unsigned int>(rodX+"/CppMon_resetQSActionCounterDelay");
      else if(m_grp->isManager()->exists(layer+"/CppMon_resetQSActionCounterDelay")) isVal = m_grp->isManager()->read<unsigned int>(layer+"/CppMon_resetQSActionCounterDelay");
      else if(m_grp->isManager()->exists("CppMon_resetQSActionCounterDelay")) isVal = m_grp->isManager()->read<unsigned int>("CppMon_resetQSActionCounterDelay");
      if (isVal != m_resetQSActionCounterDelay){
	syncMsg.str(std::string());
	syncMsg<<"Changing reset QS action cnt config for "<<rodX<<": resetQSActionCounterDelay_old = "<<m_resetQSActionCounterDelay<<", resetQSActionCounterDelay_new = "<<isVal<<std::endl;
	ers::info(PixLib::pix::daq (ERS_HERE,m_grp->rodConnectivity()->name(),syncMsg.str()));
	m_resetQSActionCounterDelay = isVal;
      }

      // create QS
      QSTalkTask qsTalkTaskCmd;

	qsTalkTaskCmd.dumpOnPPC = false;
	qsTalkTaskCmd.quickStatusMode = m_quickStatusMode;
	qsTalkTaskCmd.qsttmode = QSTalkTask::QSTT_INFO;
	if(m_resetQSActionCounterIdleCnt<m_resetQSActionCounterDelay) qsTalkTaskCmd.fc_max=0; // Not reset QS action counters
	else qsTalkTaskCmd.fc_max=1; // Reset QS action counters
	m_ctrl->sendCommand(qsTalkTaskCmd);

      // Process monitoring data
      m_qsMonInfos = qsTalkTaskCmd.result.m_qsMonInfos;
      // Handle the QS action counter replica at high level

      for(int iMod=0; iMod<32; iMod++){
	// Bookkeeping of actions on PPC
	if(m_resetQSActionCounterIdleCnt==0){ // Just started a new loop
	  m_nCFG_rodBusy[iMod][0]=0;
	  m_nCFG_rodBusy[iMod][1]=m_qsMonInfos.nCFG_rodBusy[iMod];
	  m_nCFG_timeout[iMod][0]=0;
	  m_nCFG_timeout[iMod][1]=m_qsMonInfos.nCFG_timeout[iMod];
	  m_nCFG_ht[iMod][0]=0;
	  m_nCFG_ht[iMod][1]=m_qsMonInfos.nCFG_ht[iMod];
	  // m_nRST[iMod][0]=0;	// nRST should be always 0 as there is no RST action for the new ROD
	  // m_nRST[iMod][1]=m_qsMonInfos.nRST[iMod];
	}
	else{			// Within idle cnt loop
	  m_nCFG_rodBusy[iMod][0]=m_nCFG_rodBusy[iMod][1];
	  m_nCFG_rodBusy[iMod][1]=m_qsMonInfos.nCFG_rodBusy[iMod];
	  m_nCFG_timeout[iMod][0]=m_nCFG_timeout[iMod][1];
	  m_nCFG_timeout[iMod][1]=m_qsMonInfos.nCFG_timeout[iMod];
	  m_nCFG_ht[iMod][0]=m_nCFG_ht[iMod][1];
	  m_nCFG_ht[iMod][1]=m_qsMonInfos.nCFG_ht[iMod];
	  // m_nRST[iMod][0]=m_nRST[iMod][1];	// nRST should be always 0 as there is no RST action for the new ROD
	  // m_nRST[iMod][1]=m_qsMonInfos.nRST[iMod];
	}
      }
      if(m_resetQSActionCounterIdleCnt<m_resetQSActionCounterDelay) m_resetQSActionCounterIdleCnt++;
      else m_resetQSActionCounterIdleCnt=0;

      // Need to alter QS mode because it is inconsistent with mode running on PPC or inconsistent with pre-amps status
      if(m_quickStatusMode != qsTalkTaskCmd.result.quickStatusMode){
	std::ostringstream msgStr;
	msgStr<<"QS mode recorded in IblCppMon ("
	      <<m_QSModeMap_names[m_quickStatusMode]<<") is inconsistent with the QS mode PPC is actually running ("
	      <<m_QSModeMap_names[qsTalkTaskCmd.result.quickStatusMode]
	      <<"). Will enforce the IblCppMon mode to PPC now. Please monitor closely the PPC status. It may have crashed.";
	     // PUBLISHING THIS CRITICAL PROBLEM TO THE DATA-TAKING PARTITION
          std::string msg = "Inconsistent QS mode, a PPC might have crashed, call Pixel on-call 160032";
          ers::error(PixLib::pix::daq (ERS_HERE,m_grp->rodConnectivity()->name(),msg));
          ers::warning(PixLib::pix::daq (ERS_HERE,m_grp->rodConnectivity()->name(),msgStr.str()));
	setQSMode(m_quickStatusMode);
      }
      
      // Need to alter QS mode because it is inconsistent with mode running on PPC.
      
      //if(m_quickStatusMode != qsTalkTaskCmd.result.quickStatusMode){
      //std::cout<<"WARNING: QS mode recorded in IblCppMonitorTask ("
      //	 <<m_QSModeMap_names[m_quickStatusMode]<<") is inconsistent with the QS mode PPC is actually running ("
      //	 <<m_QSModeMap_names[qsTalkTaskCmd.result.quickStatusMode]
      //	 <<"). Correcting IblCppMonitorTask record now."<<std::endl;
      //m_quickStatusMode=qsTalkTaskCmd.result.quickStatusMode;
      //}
      
      //Check pre-amps status.
      if(m_grp->preAmpsOn() &&  m_quickStatusMode == QS_RST_CFG_OFF) setQSMode(QS_RST_CFG_ON);
      else if(!m_grp->preAmpsOn() &&  m_quickStatusMode == QS_RST_CFG_ON) setQSMode(QS_RST_CFG_OFF);
      
      std::string msg = "QS_MODE: " + m_QSModeMap_names[m_quickStatusMode];
      ers::info(PixLib::pix::daq (ERS_HERE,m_grp->rodConnectivity()->name(),msg));
      
      analyseBusyAndTimeoutError();

      //analyseMicroBusyFromQS(rodX);

      bool updateDis=false;
      std::map<std::string, std::vector<uint8_t>>::iterator il;
      size_t disabled = 0;
      for (il=m_links.begin(); il!=m_links.end(); il++) {
	for(size_t s=0;s<il->second.size();s++){
          uint8_t rxCh = il->second[s];
          int feN = m_ctrl->getFeNumberFromRx(rxCh);//Check for IBL feN
          std::string feName=il->first;
          if( feN!=-1) feName +="_FE" + std::to_string(feN+1);
	  std::string mName=il->first;
	  if(updateLink(m_qsMonInfos, mName, rxCh))updateDis=true;
          publishActions(feName, rxCh);
	  if (m_QSDisabled[rxCh])disabled++;
       }

      }

      if(updateDis){
        //std::cout<<m_grp->getISRoot()<<" Calling disable Slink"<<std::endl;
        m_ctrl->disableSLink( );//Check in case an SLink has to be disabled
      }

      // Resetting counters for reconfiguration action issued from the host at the end of the function
      for(int iMod = 0; iMod < 32; iMod++){
	m_nCFG_modSync[iMod]=0;
	m_nCFG_microBusy[iMod]=0;
	m_nCFG_SEU[iMod]=0;
	m_nCFG_userAction[iMod]=0;
      }
      // Add histograming code?
      
      // -----------------------------
      //        QS OFF / IDLE 
      // -----------------------------
      // std::string mode[8] = {"QS_INFO", "QS_OFF" ,"QS_IDLE","QS_MON","QS_RST","QS_RST_DIS","QS_RST_CFG_ON","QS_RST_CFG_OFF"}; // the ordering here has to be the same as for the enum of the QS Modes
      // m_grp->isManager()->publish(m_grp->getISRoot() + "RODQSMode", mode[m_quickStatusMode]);
      // std::cout  << "QSMode is: " << mode[m_quickStatusMode] << " (" << m_quickStatusMode << ")" << std::endl;
      // if(m_quickStatusMode==QS_OFF || m_quickStatusMode==QS_IDLE){
      //    // should be there something in addition to the "standard" monitoring 
      //    std::cout << "QS is off" << std::endl;
      // }
      // else if(m_quickStatusMode > QS_IDLE || m_quickStatusMode < QS_RST_CFG_OFF || m_quickStatusMode == QS_RST_CFG_OFF){
      //   // what information QS monitors
      //   std::cout << "QS is on" << std::endl;
      // }
    }
    catch(...){
    }
  }
  return true;
}



void IblCppMonitorTask::publishActions(std::string& moduleName, int rxCh) {
  if(rxCh<0 || rxCh>31) ers::warning(PixLib::pix::daq (ERS_HERE,m_grp->rodConnectivity()->name(),"Invalid Rx channel number passed to IblCppMonitorTask::publishActions"));

  int nResets=m_nRST[rxCh][1]-m_nRST[rxCh][0]; // It will always be 0 for new readout. Keep for historical reason

  // No need to distinguish IBL and pixel here
  int nCFG_rodBusy=m_nCFG_rodBusy[rxCh][1]-m_nCFG_rodBusy[rxCh][0];
  int nCFG_timeout=m_nCFG_timeout[rxCh][1]-m_nCFG_timeout[rxCh][0];
  int nCFG_ht=0; 
  int nCFG_modSync=m_nCFG_modSync[rxCh];
  int nCFG_microBusy=m_nCFG_microBusy[rxCh];
  int nCFG_SEU=m_nCFG_SEU[rxCh];
  int nCFG_userAction=m_nCFG_userAction[rxCh];
  int nConfigs = nCFG_rodBusy+nCFG_ht+nCFG_timeout+nCFG_modSync+nCFG_microBusy+nCFG_SEU+nCFG_userAction;

  if( nResets!=0 || nConfigs!=0 ){
    std::ostringstream actionMsg;
    actionMsg << "Module "<<moduleName <<" with rxCh "<<rxCh<<" has been";
    if (nResets!=0 && nConfigs!=0) actionMsg<<" reset "<<nResets<<" times and reconfigured "<<nConfigs<<" times. ";
    else if (nResets!=0) actionMsg<<" reset "<<nResets<<" times. ";
    else if (nConfigs!=0) actionMsg<<" reconfigured "<<nConfigs<<" times. ";
    
    actionMsg<<"Cause - There are";
    if(nCFG_rodBusy>0) actionMsg<<" FMT link busy ("<<nCFG_rodBusy<<" reconfiguration attempts) ";
    if(nCFG_timeout>0) actionMsg<<" FMT link timeout ("<<nCFG_timeout<<" reconfiguration attempts) ";
    if(nCFG_ht>0)      actionMsg<<" FMT link ht      ("<<nCFG_ht<<" reconfiguration attempts) ";
    if(nCFG_modSync>0) actionMsg<<" MOD desync ("<<nCFG_modSync<<" reconfiguration attempts) ";
    if(nCFG_microBusy>0) actionMsg<<" micro-busy ("<<nCFG_microBusy<<" reconfiguration attempts) ";
    if(nCFG_SEU>0) actionMsg<<" SEU ("<<nCFG_SEU<<" reconfiguration attempts) ";
    if(nCFG_userAction>0) actionMsg<<" user actions ("<<nCFG_userAction<<" reconfiguration attempts) ";

    ers::info(PixLib::pix::daq (ERS_HERE,m_grp->rodConnectivity()->name(),actionMsg.str()));
  }

  std::string isName_nRST=m_grp->getISRoot()+moduleName+"/nRST";
  std::string isName_nRST_time=m_grp->getISRoot()+moduleName+"/nRST_lastActiontime";
  std::string isName_nCFG=m_grp->getISRoot()+moduleName+"/nCFG";
  std::string isName_nCFG_time=m_grp->getISRoot()+moduleName+"/nCFG_lastActiontime";
  
  unsigned int count_nRST = 0;
  unsigned int count_nCFG = 0;
  
  //publish non zero nRST to IS with time stamp
  try{
    if(m_grp->isManager()->exists(isName_nRST)){
      count_nRST = m_grp->isManager()->read<unsigned int>(isName_nRST);
    } 
    else {
      count_nRST = 0;
    }
  }
  catch (...) {
    count_nRST = 0;
  }
  count_nRST+=nResets;
  
  if(nResets>0){
    publishCountAndTimestamp(isName_nRST, isName_nRST_time, count_nRST);
  }//end of nRST>0
  
  //publish non zero nCFG to IS with time stamp      
  try{
    if (m_grp->isManager()->exists(isName_nCFG)){
      count_nCFG = m_grp->isManager()->read<unsigned int>(isName_nCFG);
    }
    else {
      count_nCFG = 0;
    }
  }
  catch (...) {
    count_nCFG = 0;
  }
  count_nCFG+=nConfigs;
    
  if(nConfigs>0){
    publishCountAndTimestamp(isName_nCFG, isName_nCFG_time, count_nCFG);
  }//end of if nCFG>0 loop
}

void IblCppMonitorTask::setQSMode(unsigned int QSMode, bool resetQS){
  if(m_QSModeMap_names.find(QSMode)==m_QSModeMap_names.end()){
    std::cout << "WARNING: Unknown QS mode number " << QSMode << std::endl;
    return;
  }
  QSTalkTask qsTalkTaskCmd;
  qsTalkTaskCmd.dumpOnPPC = true;
  if(resetQS){
    std::cout << __PRETTY_FUNCTION__ << ": Reset QS variables..." << std::endl;
    qsTalkTaskCmd.qsttmode = QSTalkTask::QSTT_RESET;
    m_ctrl->sendCommand(qsTalkTaskCmd);
  }
  qsTalkTaskCmd.qsttmode = QSTalkTask::QSTT_SET_MODE;
  std::cout << __PRETTY_FUNCTION__ << ": qsttmode=" << qsTalkTaskCmd.qsttmode << " (" << QSTalkTask::QSTT_SET_MODE << ") for ROD:" << m_grp->getName() << std::endl;
  m_quickStatusMode = QSMode;
  qsTalkTaskCmd.quickStatusMode = m_quickStatusMode;
  m_grp->isManager()->publish(m_grp->getISRoot() + "RODQSMode", m_QSModeMap_names[QSMode]);
  m_ctrl->sendCommand(qsTalkTaskCmd);
  std::ostringstream actionMsg;
  actionMsg<<"Change QS mode to "<<m_QSModeMap_names[QSMode];
  ers::info(PixLib::pix::daq (ERS_HERE,m_grp->rodConnectivity()->name(),actionMsg.str()));
}

bool IblCppMonitorTask::updateLink(QuickStatus::monitoring_infos &qsMonInfos, std::string& moduleName, int rxCh_arg) {
  // Update previously QSDisabled : if QSDisabled just now -> set to 1
  uint32_t combinedMask = qsMonInfos.QSDisabled & ~qsMonInfos.fmtLinkEn;

  std::map<std::string, std::vector<uint8_t>>::iterator il = m_links.find(moduleName);

  if (il == m_links.end()){
  std::cout<<"Module "<<moduleName<<" not found in link map "<<std::endl;
  return false;
  }

  bool newDis = false, newEna=false;
  size_t nDis = 0, nEna= 0;
  uint32_t linkMask = 0;
    for(size_t s=0;s<il->second.size();s++){
      uint8_t rxCh1 = il->second[s];
      linkMask |= (0x1 << rxCh1);
      if( (combinedMask & (0x1 << rxCh1) ) ){
        nDis++;
        if(m_QSDisabled[rxCh1] == 0){
          m_QSDisabled[rxCh1]=1;
          newDis=true;
          std::ostringstream msg;
    	  msg << "Module "<<moduleName<<" with rxCh "<<(int)rxCh1<<" disabled by QuickStatus.";
    	  std::cout<<msg.str()<<std::endl;
    	  ers::warning(PixLib::pix::daq (ERS_HERE,m_grp->rodConnectivity()->name(),msg.str()));
        }
      } else {
        nEna++;
        if(m_QSDisabled[rxCh1] == 1){
        m_QSDisabled[rxCh1]=0;
          if ( ( qsMonInfos.QSDisabled & (0x1 << rxCh1) ) == 0){
            newEna = true;
            std::ostringstream msg;
    	    msg << "Module "<<moduleName<<" with rxCh "<<(int)rxCh1<<" enabled by QuickStatus.";
    	    ers::warning(PixLib::pix::daq (ERS_HERE,m_grp->rodConnectivity()->name(),msg.str()));
          }
        }
      }
    }

  if(newDis && (nDis == il->second.size())){//All links in a module disabled
    m_grp->disableModule(moduleName,"QS");
    std::string isMsg_QSDisabled;
    if(m_grp->isManager()->exists(m_grp->getISRoot()+"QS_DISABLED_MODULES"))
      isMsg_QSDisabled = m_grp->isManager()->read<std::string>(m_grp->getISRoot()+"QS_DISABLED_MODULES");
    isMsg_QSDisabled += moduleName +"/";
    m_grp->isManager()->publish(m_grp->getISRoot()+"QS_DISABLED_MODULES", isMsg_QSDisabled);
  }

  if(newEna && ((qsMonInfos.QSDisabled & linkMask) ==0 ) ){//Transition from 0 links disabled to at least one enable
    m_grp->enableModule(moduleName,"QS");
    std::string isMsg_QSDisabled;
    if(m_grp->isManager()->exists(m_grp->getISRoot()+"QS_DISABLED_MODULES"))
      isMsg_QSDisabled = m_grp->isManager()->read<std::string>(m_grp->getISRoot()+"QS_DISABLED_MODULES");
    size_t pos =  isMsg_QSDisabled.find(moduleName);
      if(pos != std::string::npos){
        isMsg_QSDisabled.erase(pos,moduleName.length()+1);
        m_grp->isManager()->publish(m_grp->getISRoot()+"QS_DISABLED_MODULES", isMsg_QSDisabled);
      }
  }

return newDis;

}

bool IblCppMonitorTask::execute(){


  bool b2 = true;
  if (m_statusRate > 0) {
    if (m_count%m_statusRate==0) { 
      b2 = readStatus();
      if (b2) {
        if (m_statusRate > 100) m_statusRate -= 100;
      } else {
        ers::warning(PixLib::pix::daq (ERS_HERE,m_grp->rodConnectivity()->name(), "CppMon:Status Returned false"));
        if (m_statusRate < 100) m_statusRate += 100;
      }
    }
  }
  
  m_count++;

  return true;
}

bool IblCppMonitorTask::publishIblRodFmtStat(uint32_t iblRodFmtLinkEnable, uint32_t iblRodFmtBusyStatus, uint32_t fmtBusyFromEfb, uint32_t iblRodFmtLinkWaitingForECR) {
  //std::cout << m_grp->getRodName() << " -- " << "iblRodFmtBusyStatus= " << HEXF(8,iblRodFmtBusyStatus) << std::endl;
  //std::cout << m_grp->getRodName() << " -- " << "fmtBusyFromEfb= " << HEXF(8,fmtBusyFromEfb) << std::endl;
  //std::cout << m_grp->getRodName() << " -- " << "iblRodFmtLinkEnable= " << HEXF(8,iblRodFmtLinkEnable) << std::endl;
  //std::cout << m_grp->getRodName() << " -- " << "iblRodFmtLinkWaitingForECR= " << HEXF(8,iblRodFmtLinkWaitingForECR) << std::endl;
  std::map<std::string, std::vector<uint8_t>>::iterator il;
  //bool anyFmtBusy = false;
  //std::cout << "m_links.size()=" << m_links.size() << std::endl;
  for (il=m_links.begin(); il!=m_links.end(); il++){
    //std::cout << m_grp->getRodName() << " -- " << il->first << " " << il->second << std::endl;
    std::string fs;
    // fs should read D or ETOHB
    // IBL only: Checking bits n and n+1 since they belong to the same module (n=il->first)
    uint32_t linkMask = 0;
    bool timeout = false;
    bool ht = false;
    size_t QSdis=0;
      for(size_t s=0;s<il->second.size();s++){
      uint8_t rxCh = il->second[s];
      linkMask |= 0x1 << rxCh;
      if(m_qsMonInfos.timeoutCounters[rxCh])timeout = true;
      if(m_qsMonInfos.htCounters[rxCh])ht = true;
      if (m_QSDisabled[rxCh])QSdis++;
      }//// fs should read D or ETOHB
    if( (iblRodFmtLinkEnable) & linkMask ){
      fs = "E";
      // Monitor timeout error
      if (timeout)fs += "T";
      else fs += " ";
      
      fs+=" ";//O
      
      if (ht)fs += "H";
      else fs += " ";
      
      if( (iblRodFmtBusyStatus|fmtBusyFromEfb) & linkMask ){ // Any link item is busy
      //anyFmtBusy = true;
      fs += "B";
      } else fs += " ";
      
      } else if( (iblRodFmtLinkEnable & linkMask) == 0x0 ){ // Both links disabled, hence 'disabled'
      	if (QSdis == il->second.size())
	  fs = "QSD";
	else
	  fs = "D";
      }

     std::cout << m_grp->getISRoot()+il->first+"/FMTSTAT" << "= " << fs << std::endl;
    if (fs != m_fmtstat[il->first]){
      m_fmtstat[il->first] = fs;
      std::string publishString = m_grp->getISRoot() + il->first + "/FMTSTAT";
      std::cout << "IBLMon:Rod Error -- " <<  publishString << ": " << fs << std::endl;
      m_grp->isManager()->publish(m_grp->getISRoot()+il->first+"/FMTSTAT", fs);
    }
  }

  setModLinkMgrp();

  return true;
}



void IblCppMonitorTask::dumpBusyRegisters(){


    std::cout << "ROD Counters normalized to nTrigger ONLY (slave busy and Trigger counters are absolute numbers)" << std::endl;
    std::cout << "Slave BUSY counters:" << std::endl;
    for(size_t i = 0 ; i < sizeof(m_qsMonInfos.slaveBusyCounters)/sizeof(uint32_t) ; ++i)
      std::cout << std::setw(2) << i << ": " << m_qsMonInfos.slaveBusyCounters[i] << "; ";
    std::cout << std::endl;

    std::cout << "Link trigger counters:" << std::endl;
    for(size_t i = 0 ; i < sizeof(m_qsMonInfos.linkTrigCounters)/sizeof(uint32_t) ; ++i)
      std::cout << std::setw(2) << i << ": " << m_qsMonInfos.linkTrigCounters[i] << "; ";
    std::cout << std::endl;

    std::cout << "Link occupancy counters:" << std::endl;
    for(size_t i = 0 ; i < sizeof(m_qsMonInfos.linkOccCounters)/sizeof(uint32_t) ; ++i){
      if(m_qsMonInfos.linkTrigCounters[i]>0) std::cout << std::setw(2) << i << ": " << m_qsMonInfos.linkOccCounters[i]/m_qsMonInfos.linkTrigCounters[i] << "; ";
      else std::cout << std::setw(2) << i << ": ntrigger = 0; ";}
    std::cout << std::endl;

    std::cout << "Total desync counters:" << std::endl;
    for(size_t i = 0 ; i < sizeof(m_qsMonInfos.totDesyncCounters)/sizeof(uint16_t) ; ++i){
      if(m_qsMonInfos.linkTrigCounters[i]>0)std::cout << std::setw(2) << i << ": " << m_qsMonInfos.totDesyncCounters[i]/m_qsMonInfos.linkTrigCounters[i] << "; ";
      else std::cout << std::setw(2) << i << ": ntrigger = 0; ";}
    std::cout << std::endl;

    std::cout << "HT counters: " << std::endl;
    for(size_t i = 0 ; i < sizeof(m_qsMonInfos.htCounters)/sizeof(uint32_t) ; ++i){
      if(m_qsMonInfos.linkTrigCounters[i]>0)std::cout <<  std::setw(2) << i << ": " << m_qsMonInfos.htCounters[i]/m_qsMonInfos.linkTrigCounters[i] << "; ";
      else std::cout << std::setw(2) << i << ": ntrigger = 0; ";}
    std::cout << std::endl;

    std::cout << "Timeout counters:" << std::endl;
    for(size_t i = 0 ; i < sizeof(m_qsMonInfos.timeoutCounters)/sizeof(uint16_t) ; ++i){
      if(m_qsMonInfos.linkTrigCounters[i]>0)std::cout << std::setw(2) << i << ": " << m_qsMonInfos.timeoutCounters[i]/m_qsMonInfos.linkTrigCounters[i] << "; ";
      else std::cout << std::setw(2) << i << ": ntrigger = 0; ";}
    std::cout << std::endl;

    std::cout << "BOC frame count counters:" << std::endl;
    for(size_t i = 0 ; i < sizeof(m_qsMonInfos.frameCountBOC)/sizeof(uint32_t) ; ++i)
      std::cout << std::setw(2) << i << ": " << m_qsMonInfos.frameCountBOC[i] << "; ";
    std::cout << std::endl;

    std::cout << "BOC frame error counters:" << std::endl;
    for(size_t i = 0 ; i < sizeof(m_qsMonInfos.frameErrorBOC)/sizeof(uint32_t) ; ++i)
      std::cout << std::setw(2) << i << ": " << m_qsMonInfos.frameErrorBOC[i] << "; ";
    std::cout << std::endl;

    std::cout << "BOC decoding error counters:" << std::endl;
    for(size_t i = 0 ; i < sizeof(m_qsMonInfos.decodingErrorBOC)/sizeof(uint32_t) ; ++i)
      std::cout << std::setw(2) << i << ": " << m_qsMonInfos.decodingErrorBOC[i] << "; ";
    std::cout << std::endl;

    std::cout << "BOC mean occupancy counters:" << std::endl;
    for(size_t i = 0 ; i < sizeof(m_qsMonInfos.meanOccBOC)/sizeof(uint16_t) ; ++i)
      std::cout << std::setw(2) << i << ": " << m_qsMonInfos.meanOccBOC[i]/65535. << "; ";
    std::cout << std::endl;

    std::cout << "BOC S-link counters:" << std::endl;
    for(size_t i = 0 ; i < 4 ; ++i)
      for(size_t j = 0 ; j < 2 ; ++j)
    	std::cout << std::setw(2) << "S-link " 
    		  << i << " Hola "<< j <<" Lff: " 
    		  << m_qsMonInfos.SLinkInfo.sLinkCounters[i].Hola[j].Lff<<" LDown: " 
    		  << m_qsMonInfos.SLinkInfo.sLinkCounters[i].Hola[j].LDown << "; "<<std::endl;
    std::cout << std::endl;


    std::cout << "Master Busy ROD Counters normalized" << std::endl;

    uint32_t BusyClkCntRegValue = m_qsMonInfos.masterBusyCounters[0]; 
    double BusyTotCntRegValue = 0, BusySlv0CntRegValue = 0, BusySlv1CntRegValue = 0, 
     BusyEfbHeaderpauseCntRegValue = 0, BusyFmtMbFef0CntRegValue = 0, BusyFmtMbFef1CntRegValue = 0, BusyRolPauseCntRegValue =0 ;

    std::cout << "Master BusyClkCntRegValue: " << m_qsMonInfos.masterBusyCounters[0] << std::endl;

    if (BusyClkCntRegValue > 0) BusyTotCntRegValue = double (m_qsMonInfos.masterBusyCounters[1])/double(BusyClkCntRegValue);
    else BusyTotCntRegValue = 0;
    std::cout << "Master BusyTotCntRegValue: " << BusyTotCntRegValue << std::endl;

    if (BusyClkCntRegValue > 0) BusySlv0CntRegValue = double (m_qsMonInfos.masterBusyCounters[2])/double(BusyClkCntRegValue);
    else BusySlv0CntRegValue = 0;
    std::cout << "Master BusySlv0CntRegValue: " << BusySlv0CntRegValue << std::endl;

    if (BusyClkCntRegValue > 0) BusySlv1CntRegValue = double (m_qsMonInfos.masterBusyCounters[3])/double(BusyClkCntRegValue);
    else BusySlv1CntRegValue = 0;
    std::cout << "Master BusySlv1CntRegValue: " << BusySlv1CntRegValue << std::endl;

    if (BusyClkCntRegValue > 0) BusyEfbHeaderpauseCntRegValue = double (m_qsMonInfos.masterBusyCounters[4])/double(BusyClkCntRegValue);
    else BusyEfbHeaderpauseCntRegValue = 0;
    std::cout << "Master BusyEfbHeaderpauseCntRegValue: " << BusyEfbHeaderpauseCntRegValue << std::endl;

    if (BusyClkCntRegValue > 0) BusyFmtMbFef0CntRegValue = double (m_qsMonInfos.masterBusyCounters[5])/double(BusyClkCntRegValue);
    else BusyFmtMbFef0CntRegValue = 0;
    std::cout << "Master BusyFmtMbFef0CntRegValue: " << BusyFmtMbFef0CntRegValue << std::endl;

    if (BusyClkCntRegValue > 0) BusyFmtMbFef1CntRegValue = double (m_qsMonInfos.masterBusyCounters[6])/double(BusyClkCntRegValue);
    else BusyFmtMbFef0CntRegValue = 0;
    std::cout << "Master BusyFmtMbFef1CntRegValue: " << BusyFmtMbFef1CntRegValue << std::endl;

    if (BusyClkCntRegValue > 0) BusyRolPauseCntRegValue = double (m_qsMonInfos.masterBusyCounters[7])/double(BusyClkCntRegValue);
    else BusyRolPauseCntRegValue = 0;
    std::cout << "Master BusyRolPauseCntRegValue: " << BusyRolPauseCntRegValue << std::endl;

}

//FMTSTAT should read D or ETOHB
void IblCppMonitorTask::setModLinkMgrp() {
  std::map<std::string, std::vector<uint8_t>>::iterator il;
  for (il=m_links.begin(); il!=m_links.end(); il++){
    std::string modName = il->first;
      if (m_fmtstat.find(modName) != m_fmtstat.end())continue;
     m_grp->setModLinkStat(modName, m_fmtstat[il->first]);
    }

}

void IblCppMonitorTask::publishQSMode() {
   std::map<std::string, unsigned int>::iterator itQS;
   for (itQS = m_QSModeMap.begin(); itQS!=m_QSModeMap.end(); itQS++){
     std::string qs="";
     if ((*itQS).second==m_quickStatusMode){
       qs=(*itQS).first;
     } 
     if(qs!=""){
       m_grp->isManager()->publish(m_grp->getISRoot()+"RODQSMode",qs);
     }
   }
}


void dump_busy_status(uint32_t busy_status) {
  uint32_t mask = 0x80000000;
  char line[200];
  while (mask) { // The idea is that mask will be 0 when we have done the last shift
    snprintf(line, sizeof(line), " %s", (busy_status & mask) ? "X" : "-");
    std::cout << line;
    mask >>= 1;
  }
  std::cout << std::hex << "\t Value: 0x" << busy_status << std::dec;
  std::cout << std::endl;
}

void show_busy_header_master(const std::string& rodName) {
  std::string printRodName = rodName + ((rodName.size() == 13) ? " " : "");
  std::cout << printRodName << "   MASTER                                  S R F F E S S O" << std::endl;
  std::cout << "                                                         P O M M F L L U" << std::endl;
  std::cout << "                                                         A L T T B V V T" << std::endl;
  std::cout << "                                                         R T 1 0   1 0  " << std::endl;
}

void show_busy_header_slave(const std::string& rodName, bool isSlaveB) {
  std::string printRodName = rodName + ((rodName.size() == 13) ? " " : "");

  if(isSlaveB)
    std::cout << printRodName << "   SLAVE C-side / SOUTH          H H R R E E E E 1 1 0 0 O" << std::endl;
  else
    std::cout << printRodName << "   SLAVE A-side / NORTH          H H R R E E E E 1 1 0 0 O" << std::endl;

  std::cout << "                                               I I T T 2 2 1 1 D L D L U" << std::endl;
  std::cout << "                 FMT3    FMT2    FMT1    FMT0  S S R R S S S S W F W F T" << std::endl;
  std::cout << "               3 2 1 0 3 2 1 0 3 2 1 0 3 2 1 0 1 0 1 0 1 0 1 0 N F N F  " << std::endl;
}

void show_busy_status(const std::string& rodName, ReadRodSlaveBusyStatus &cmdSlave) {
  show_busy_header_slave(rodName, cmdSlave.targetIsSlaveB);
  std::cout << "STATUS  "; dump_busy_status(cmdSlave.result.slaveBusyCurrentStatusValue);
  std::cout << "MASK    "; dump_busy_status(cmdSlave.result.slaveBusyMaskValue);
  std::cout << "FORCE   "; dump_busy_status(cmdSlave.result.slaveBusyForceValue);
}

void show_busy_status(const std::string& rodName, ReadRodMasterBusyStatus &cmdMaster) {
  show_busy_header_master(rodName);
  std::cout << "STATUS  "; dump_busy_status(cmdMaster.result.masterBusyCurrentStatusValue & 0xFF);
  std::cout << "MASK    "; dump_busy_status(cmdMaster.result.masterBusyMaskValue & 0xFF );
  std::cout << "FORCE   "; dump_busy_status((cmdMaster.result.masterBusyMaskValue >> 8) & 0xFF);
}


bool IblCppMonitorTask::readIblRodFmtStatus() {
	//std::cout << __PRETTY_FUNCTION__ << std::endl;
#if 1

  uint32_t iblRodFmtLinkEnable, iblRodFmtBusyStatus;
  uint32_t fmtBusyFromEfb = 0x0;
  uint8_t efbBusyMask = 0x0; // [7-4]: slave B ; [3-0]: slave A
  bool sLinkXOffOrDown = false;

  ReadRodSlaveBusyStatus iblRodSlaveBusyStatus;
  iblRodSlaveBusyStatus.dumpOnPPC = false;
  ReadRodMasterBusyStatus iblRodMasterBusyStatus;
  iblRodMasterBusyStatus.dumpOnPPC = false;
  DumpRodRegisters iblRodRegisters;
  iblRodRegisters.dumpOnPPC = false;

  // Fetching the list of enabled channels
  m_ctrl->sendCommand(iblRodRegisters);
  iblRodFmtLinkEnable = iblRodRegisters.result.FmtLinkEnableValue_A;
  iblRodFmtLinkEnable |= (iblRodRegisters.result.FmtLinkEnableValue_B << 16);

  // Check for busy status for Master
  m_ctrl->sendCommand(iblRodMasterBusyStatus);
  uint32_t rodMasterBusyCurrentStatusValue = iblRodMasterBusyStatus.result.masterBusyCurrentStatusValue;
  uint32_t rodMasterBusyMaskValue = iblRodMasterBusyStatus.result.masterBusyMaskValue & 0xFF;
  uint32_t rodMasterBusyForceValue = (iblRodMasterBusyStatus.result.masterBusyMaskValue >> 8) & 0xFF;

  uint32_t rodMasterBusy = (rodMasterBusyCurrentStatusValue & ~rodMasterBusyMaskValue) | rodMasterBusyForceValue;
  //rodMasterBusy &= 0xFE; // Ignoring second bit (negative logic, and anyhow aggregates the rest of the inforamtion)
  rodMasterBusy = (~rodMasterBusy)&0x1; // ROD master BUSY is negative logic

  if(rodMasterBusy) show_busy_status(m_grp->getRodName(), iblRodMasterBusyStatus);

  // First storing the formatter busy status for slave A
  iblRodSlaveBusyStatus.targetIsSlaveB = false;
  m_ctrl->sendCommand(iblRodSlaveBusyStatus);
/*
  iblRodFmtBusyStatus = iblRodSlaveBusyStatus.result.slaveBusyCurrentStatusValue >> 13;
  iblRodFmtBusyStatus &= ~(iblRodSlaveBusyStatus.result.slaveBusyMaskValue >> 13);
  iblRodFmtBusyStatus |= iblRodSlaveBusyStatus.result.slaveBusyForceValue >> 13;
 */
  iblRodFmtBusyStatus = iblRodSlaveBusyStatus.getBusyOutput() >> 13;

  // Checking the status of the EFB
  efbBusyMask |= (iblRodSlaveBusyStatus.result.slaveBusyCurrentStatusValue >> 5) & 0xF;
  efbBusyMask |= (iblRodSlaveBusyStatus.result.slaveBusyForceValue >> 5) & 0xF;

  // Checking the status of the S-LINK XOff or Down
  if( (m_grp->getCtrlType() == CPPROD && (iblRodSlaveBusyStatus.getBusyOutput() & 0x1E)) ||
	(m_grp->getCtrlType() == L12ROD && (iblRodSlaveBusyStatus.getBusyOutput() & 0x18)) )
    sLinkXOffOrDown = true;

  if(rodMasterBusy) show_busy_status(m_grp->getRodName(), iblRodSlaveBusyStatus);

  // Gather contents of BUSY histo counters
  uint32_t ReadSlaveBusyStatusResult::*slaveBusyHistoPtr[4][4] = {
    { &ReadSlaveBusyStatusResult::slaveBusyFmt0ff0HistValue, &ReadSlaveBusyStatusResult::slaveBusyFmt0ff1HistValue, &ReadSlaveBusyStatusResult::slaveBusyFmt0ff2HistValue, &ReadSlaveBusyStatusResult::slaveBusyFmt0ff3HistValue },
    { &ReadSlaveBusyStatusResult::slaveBusyFmt1ff0HistValue, &ReadSlaveBusyStatusResult::slaveBusyFmt1ff1HistValue, &ReadSlaveBusyStatusResult::slaveBusyFmt1ff2HistValue, &ReadSlaveBusyStatusResult::slaveBusyFmt1ff3HistValue },
    { &ReadSlaveBusyStatusResult::slaveBusyFmt2ff0HistValue, &ReadSlaveBusyStatusResult::slaveBusyFmt2ff1HistValue, &ReadSlaveBusyStatusResult::slaveBusyFmt2ff2HistValue, &ReadSlaveBusyStatusResult::slaveBusyFmt2ff3HistValue },
    { &ReadSlaveBusyStatusResult::slaveBusyFmt3ff0HistValue, &ReadSlaveBusyStatusResult::slaveBusyFmt3ff1HistValue, &ReadSlaveBusyStatusResult::slaveBusyFmt3ff2HistValue, &ReadSlaveBusyStatusResult::slaveBusyFmt3ff3HistValue }
  };

  uint32_t slaveBusyHistoValues[32];

  for(uint32_t l = 0 ; l < 16 ; ++l) {
    slaveBusyHistoValues[l] = iblRodSlaveBusyStatus.result.*(slaveBusyHistoPtr[l/4][l%4]);
  }

	// Then storing the formatter busy status for slave B
	iblRodSlaveBusyStatus.targetIsSlaveB = true;
	m_ctrl->sendCommand(iblRodSlaveBusyStatus);
 /*
	iblRodFmtBusyStatus |= (iblRodSlaveBusyStatus.result.slaveBusyCurrentStatusValue >> 13) << 16;
  iblRodFmtBusyStatus &= ~((iblRodSlaveBusyStatus.result.slaveBusyMaskValue >> 13) << 16);
	iblRodFmtBusyStatus |= (iblRodSlaveBusyStatus.result.slaveBusyForceValue >> 13) << 16;
 */
  iblRodFmtBusyStatus |= (iblRodSlaveBusyStatus.getBusyOutput() >> 13) << 16;

	// Checking the status of the EFB
	efbBusyMask |= ((iblRodSlaveBusyStatus.result.slaveBusyCurrentStatusValue >> 5) & 0xF) << 4;
	efbBusyMask |= ((iblRodSlaveBusyStatus.result.slaveBusyForceValue >> 5) & 0xF) << 4;

  // Checking the status of the S-LINK XOff
  if( (m_grp->getCtrlType() == CPPROD && (iblRodSlaveBusyStatus.getBusyOutput() & 0x1E)) ||
	(m_grp->getCtrlType() == L12ROD && (iblRodSlaveBusyStatus.getBusyOutput() & 0x18)) )
    sLinkXOffOrDown = true;

  if(rodMasterBusy) show_busy_status(m_grp->getRodName(), iblRodSlaveBusyStatus);

  for(uint32_t l = 16 ; l < 32 ; ++l) {
    slaveBusyHistoValues[l] = iblRodSlaveBusyStatus.result.*(slaveBusyHistoPtr[l/4-4][l%4]);
  }

  uint16_t eBM = efbBusyMask;
  // Update the fmtBusyMask based on the EFBs that are busy
  for(uint32_t i = 0 ; i < 8 ; ++i) {
    if(i) fmtBusyFromEfb <<= 4;
    if( eBM & 0x80 ) fmtBusyFromEfb |= 0xF;
    eBM <<= 1;
  }

  std::cout << m_grp->getRodName() << " -- " << std::hex << "efbBusyMask= 0x" << (int)efbBusyMask << std::dec << std::endl;
  std::cout << m_grp->getRodName() << " -- " << std::hex << "iblRodFmtBusyStatus= 0x" << iblRodFmtBusyStatus << std::dec << std::endl;
  std::cout << m_grp->getRodName() << " -- " << std::hex << "fmtBusyFromEfb= 0x" << fmtBusyFromEfb << std::dec << std::endl;
  std::cout << m_grp->getRodName() << " -- " << std::hex << "iblRodFmtLinkEnable= 0x" << iblRodFmtLinkEnable << std::dec << std::endl;

  std::map<std::string,std::vector<uint8_t>>::iterator il;
  bool anyFmtBusy = false;
  std::cout << "m_links.size()=" << m_links.size() << std::endl;
  for (il=m_links.begin(); il!=m_links.end(); il++){
    //std::cout << m_grp->getRodName() << " -- " << il->first << " " << il->second << std::endl;
    std::string fs;
    // fs should read D or ETOHB
    // IBL only: Checking bits n and n+1 since they belong to the same module (n=il->first)
    uint32_t linkMask=0;
    //FMTSTAT should read D or ETOHB
    for(size_t s=0;s<il->second.size();s++)linkMask |= 0x1 << il->second[s];

    if( (iblRodFmtBusyStatus|fmtBusyFromEfb) & linkMask ) // Any link item is busy, hence 'enabled'
      fs = "E   B";
    else if( (iblRodFmtLinkEnable & linkMask) == 0x0 ) // Both links disabled, hence 'disabled'
      fs = "D";
    else fs = "E    "; // The item is 'enabled'

    if( (iblRodFmtBusyStatus|fmtBusyFromEfb) & linkMask ) // Any link item is busy
	anyFmtBusy = true;

    //std::cout << m_grp->getISRoot()+il->second+"/FMTSTAT" << "= " << fs << std::endl;
    if (m_fmtstat[il->first] != fs){
      std::cout<<il->first<<" "<<m_fmtstat[il->first]<<" "<< fs<<std::endl;
      m_fmtstat[il->first] = fs;
      m_grp->isManager()->publish(m_grp->getISRoot()+il->first+"/FMTSTAT", fs);
    }

    // Publish BUSY histo counters to IS
    // Todo: compute deltas and criterion to take actions
    uint32_t busyHistVal = 0;
    for(size_t s=0;s<il->second.size();s++)busyHistVal += slaveBusyHistoValues[il->second[s]];
    busyHistVal /= il->second.size();
    m_grp->isManager()->publish(m_grp->getISRoot()+il->first+"/BUSY_HIST", busyHistVal);
  }

   setModLinkMgrp();

  // Report ROD busy status
  bool rodBusy = anyFmtBusy || rodMasterBusy;

  int busyPerc = rodBusy ? 100 : 0;
  if(sLinkXOffOrDown) busyPerc += 1000;
  if(m_grp->rodDisabled()) busyPerc = 0; // A disabled ROD cannot be BUSY

  m_grp->isManager()->publish(m_grp->getISRoot()+"BUSY",busyPerc);
  m_grp->setRodBusy(busyPerc);

  // Temporarily adding QS_OFF for IBL
  std::string qs = "QS_OFF";
  m_grp->isManager()->publish(m_grp->getISRoot()+"RODQSMode",qs);

  // Todo: call mutiple times and retain maximum for better statistics
  uint64_t fmtOccData[][4] = {
    { ((uint64_t)iblRodRegisters.result.Fmt0OccupancyRegValue_A),
      ((uint64_t)iblRodRegisters.result.Fmt1OccupancyRegValue_A),
      ((uint64_t)iblRodRegisters.result.Fmt2OccupancyRegValue_A),
      ((uint64_t)iblRodRegisters.result.Fmt3OccupancyRegValue_A) },
    { ((uint64_t)iblRodRegisters.result.Fmt0OccupancyRegValue_B),
      ((uint64_t)iblRodRegisters.result.Fmt1OccupancyRegValue_B),
      ((uint64_t)iblRodRegisters.result.Fmt2OccupancyRegValue_B),
      ((uint64_t)iblRodRegisters.result.Fmt3OccupancyRegValue_B) } };

  uint16_t linkOcc[2][4][4];

  for(int s = 0 ; s < 2 ; ++s)
    for(int f = 0 ; f < 4 ; ++f)
      for(int l = 0 ; l < 4 ; ++l)
        linkOcc[s][f][l] = (fmtOccData[s][f]>>(l*10)) & 0x3FF;

  for (il=m_links.begin(); il!=m_links.end(); il++)
    for(size_t s=0;s<il->second.size();s++){
    uint8_t rxCh = il->second[s];
    uint32_t maxFmtOcc = max( linkOcc[rxCh/16][(rxCh%16)/4][rxCh%4], linkOcc[rxCh/16][(rxCh%16)/4][(rxCh%4)+1] );
    m_grp->isManager()->publish(m_grp->getISRoot()+il->first+"/FMT_OCC", maxFmtOcc);
  }


#endif
	return true;
}

