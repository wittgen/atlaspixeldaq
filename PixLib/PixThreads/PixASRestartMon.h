//////////////////////////////////////////////////////////////////////
// PixASRestartMon.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 12/02/11  Version 1.0 (PM)
//           
//

#ifndef PIX_AS_RESTART_MON_H
#define PIX_AS_RESTART_MON_H

#include <string>
#include "PixPeriodicTask.h"

namespace PixLib {

  class PixISManager;
  class PixBrokerSingleCrate;

  class PixASRestartMon : public virtual PixPeriodicTask {

  public:
    PixASRestartMon(PixISManager *ism,
	       PixBrokerSingleCrate *broker);

    virtual ~PixASRestartMon();

    virtual bool initialize();
    virtual bool execute();

  protected:
    PixISManager* m_is;
    PixBrokerSingleCrate* m_broker;

  private:

  };

}

#endif 
