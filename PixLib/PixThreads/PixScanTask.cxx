//////////////////////////////////////////////////////////////////////
// PixScanTask.cxx
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 13/08/07  Version 1.0 (DLM)
//           
//


#include <unistd.h>
#include <exception>
#include <iostream>
#include <iomanip>


#include "../PixCalibDbCoral/ScanInfo_t.hh"
#include "../PixCalibDbCoral/CoralDbClient.hh"
#include "../PixCalibDbCoral/PixMetaException.hh"
#include "../PixCalibDbCoral/Common.hh"

#include "PixScanTask.h"
#include "PixController/PixController.h"
#include "PixController/PixScan.h"
#include "PixModule/PixModule.h"
#include "PixConnectivity/RodBocConnectivity.h"
#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixUtilities/PixMessages.h"

using namespace PixLib;

PixScanTask::PixScanTask(PixModuleGroup *grp, PixScan *scn, PixHistoServerInterface *hInt, std::string name, std::string partitionName)
  : PixPeriodicTask(name, 1), m_grp(grp), m_scn(scn), m_hInt(hInt), m_dir(""), m_stop(false), m_exeBlock(0), m_nstage(0), m_nloop0(0), m_nloop1(0), m_nloop2(0) 

{
  m_id = -1;
  std::string globMutexName = "PixScanTask"+name;
  if (m_grp != NULL) {
    if ( m_grp->rodConnectivity() != NULL) {
      if (m_grp->rodConnectivity()->crate() != NULL) {
	globMutexName = "PixScanTask_"+m_grp->rodConnectivity()->crate()->name() + "_"+name;
      }
    }
  }
  m_globMutex = DFMutex::Create(const_cast<char*>(globMutexName.c_str()));
}

PixScanTask::~PixScanTask()
{
  m_globMutex->destroy();
}

bool PixScanTask::initialize()
{
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<", at line: "<<__LINE__<<std::endl;
  bool status = true;
  try {
    std::cout << m_grp->getName() << " Initializing scan task  " << m_name <<" "<< std::dec << m_id << std::endl;
    m_grp->publishScanInit(m_name, m_id);
    
    if(!setModuleMask()) status = fail();
    if(!m_grp->checkRodController()) status = fail();
    std::cout << m_grp->getName() << " PixScanTask::initialize(): m_scan is: 0x" << m_scn << std::endl;
    std::string folder = getMainFolder();
    m_grp->prepareScan(m_scn);
   // check that at least one module is active! 
    if (m_grp->getActiveModuleMask() == 0){
        ers::error(PixLib::pix::daq (ERS_HERE, "SCAN_INIT_FAILURE", "no active modules on ROD."));
        status = fail();
    }
    m_scn->loop(2);
    m_grp->prepareLoop2(m_scn, m_hInt, folder);
    std::cout << "prepareLoop2 " << folder <<std::endl;
    m_scn->loop(1);
    m_grp->prepareLoop1(m_scn, m_hInt, folder);
    std::cout << "prepareLoop1 " << folder <<std::endl;
    m_scn->loop(0);
    m_grp->prepareLoop0(m_scn, m_hInt, folder);
    std::cout << "prepareLoop0 " << folder <<std::endl;
    
    if(m_stop==true){  //was scan aborted?
      m_scn->terminateScan(m_grp);
      status = false; 
    }
    if(status) m_grp->publishScanRunning(m_id);
  }
  catch(const PixModuleGroupExc &e) {
    ers::error(PixLib::pix::daq (ERS_HERE, "SCAN_INIT_FAILURE", e.getDescr()));
  }
  catch(...) {
    status = fail();
    std::string msg = "Unknown exception caught during scan initialize";
    ers::error(PixLib::pix::daq (ERS_HERE, "SCAN_INIT_FAILURE", msg));
  }
  return status;
}


bool PixScanTask::execute() 
{
  /*  std::stringstream idstream;
  idstream << "S";
  idstream << std::setw(9) << std::setfill('0') << m_id;

  std::string status = idstream.str() +"/" + m_grp->getName()+"/" + "STATUS";

  */
  std::string folder = getMainFolder();
  try {
    for(;;){
      
      if(m_stop==true){ 
	m_scn->terminateScan(m_grp);
	return false;
      }
      m_grp->scanExecute(m_scn); //execute scan
      /* MK not sure the code below fulfills any purpose
      if(m_grp->normalScan(m_scn) && !m_grp->checkScanningMode()){
	std::cout << m_grp->getName() + " failed to start scan in time." << std::endl;
	return false;
      }
      */

      m_scn->setLogsScanId(m_id);
      
      if(m_stop==true){ 
	m_scn->terminateScan(m_grp);
	return false;
      }
      
      if (m_grp->normalScan(m_scn) ||
          m_scn->getRunType() == PixScan::MONLEAKRUN ||
          (m_scn->getRunType() == PixScan::RAW_EVENT && m_scn->getDspProcessing(0))) {
	if (!m_grp->doneScanning(false, m_scn, m_nstage, m_nloop0, m_nloop1, m_nloop2, m_exeBlock)) return true;
	if (!m_grp->runOk(m_scn,m_nstage,m_nloop0,m_nloop1,m_nloop2)) return fail();
      }
      
      std::cout <<  m_grp->getName()  << " endloop0: " <<std::dec << m_scn->scanIndex(0) << "/"
                          << m_scn->scanIndex(1) << "/" << m_scn->scanIndex(2) << std::endl;
      
      // ROD scan termination
 std::cout << "endloop0 " << folder <<std::endl;
      m_grp->endLoop0(m_scn, m_hInt, folder);
     
      if(!m_scn->loop(0)){
	std::cout <<  m_grp->getName() << " endloop 1: " << std::endl;
 std::cout << "endloop1 " << folder <<std::endl;
	m_grp->endLoop1(m_scn, m_hInt, folder);
	initBlockVariables();
	
	if(!m_scn->loop(1)){
	  std::cout <<  m_grp->getName() << " endloop 2" << std::endl;
 std::cout << "endloop2 " << folder <<std::endl;
	  m_grp->endLoop2(m_scn, m_hInt, folder);
	
	  if(!m_scn->loop(2)){
	  std::cout <<  m_grp->getName() << " endscan" << std::endl;
	    m_grp->endScan(m_scn, m_hInt, folder);   
	    saveResults();	
	    std::string msg =  "Scan terminated normally";
	    ers::info(PixLib::pix::daq (ERS_HERE, "SCAN_TERMINATION", msg));
	    return false;
	  } 
	  m_grp->prepareLoop2(m_scn, m_hInt, folder);
	}
	m_grp->prepareLoop1(m_scn, m_hInt, folder);
      }
      // EDIT: Below was a fix for a temporary DSP problem, it is not needed now.
      // It is kept in case it might be useful in the future.
      //Call setReadoutSpeed to initialize ROD and reconfigBOC 
      //Normal Scan with outer loops on host can put ROD into bad state and also lock PP0 to 20MHz mode
      //which requires a restart of action server
      //e.g. T0/THRESHOLD Scan on Layer2 RODs with one additional 8 steps outer loop on host can triggger problem
      //if(m_scn->getRunType() == PixScan::NORMAL_SCAN){
      //   std::cout << m_grp->getName() <<" "<< m_name << " setReadoutSpeed at step: "<< m_scn->scanIndex(0)<<"/"<< m_scn->scanIndex(1)<<"/"<< m_scn->scanIndex(2)<< std::endl;
      //   m_grp->setReadoutSpeed((MccBandwidth)m_scn->getMccBandwidth());
      //}
      m_grp->prepareLoop0(m_scn, m_hInt, folder);
      //execute once more (for scans that don't communicate with ROD)
    }
    return true;
  }
  catch(const SctPixelRod::BaseException & exc) {
    std::string msg =  "Exception caught during scan execution: " + exc.getDescriptor();
    ers::error(PixLib::pix::daq (ERS_HERE, "SCAN_FAILURE", msg));
    return fail();
  }
  catch (...) {
    std::string msg = "Unknown exception caught during scan execution";
    ers::error(PixLib::pix::daq (ERS_HERE, "SCAN_FAILURE", msg));
    return fail();
  }
}

bool PixScanTask::setModuleMask()
{
  return !m_grp->setModuleMask(m_scn);
}

bool PixScanTask::fail()
{
  std::string msg = "Scan failed";
  ers::error(PixLib::pix::daq (ERS_HERE, "SCAN_FAILURE", msg));
  PixController *rod = ( m_grp->getPixController() );
  rod->stopScan();
  ers::error(PixLib::pix::daq (ERS_HERE, "SCAN_FAILURE", msg));
  m_grp->publishScanFailed(m_id);
  return false;
}

void PixScanTask::abort()
{
  m_stop=true;
  std::string msg = "Scan aborted";
  ers::error(PixLib::pix::daq (ERS_HERE, "SCAN_ABORTED", msg));
  PixController *rod = ( m_grp->getPixController() );
  rod->abortScan();
  ers::error(PixLib::pix::daq (ERS_HERE, "SCAN_ABORTED", msg));
  m_grp->publishScanAbort(m_id);
}

void PixScanTask::saveResults()
{
  try {
    struct timeval t0; 
    struct timeval t1;
    //int time;
    std::stringstream folderstream;
    folderstream << "/S";
    folderstream << std::setw(9) << std::setfill('0') << m_id;
    folderstream << "/" << m_grp->getName() << "/";
    std::cout << "  PixScanTask: save folder: " << folderstream.str() << std::endl;
    //time=
            (void)gettimeofday(&t0,NULL);

    if (m_name != "TDAC_FAST_TUNE" && m_name != "FDAC_FAST_TUNE") {
      m_scn->writeHisto(m_hInt, m_grp, folderstream.str());
    }
    // send the results of a scan in metadata :

    std::string StartScan = m_scn->getTimeScanStart();
    std::string EndScan = m_scn->getTimeScanEnd() ;
    std::string StartWrite = m_scn->getTimeScanHwriteStart();
    std::string EndWrite = m_scn->getTimeScanHwriteEnd();
    std::string TimeScan = m_scn->getTimeScanInitEnd();
    int Download = m_scn->getTimeHistoDownload();
    int TimeLoop = m_scn->getTimeLoop();
    
    if (true) {
        try{
            m_globMutex->lock();
        } catch (const DFMutex::AlreadyLockedByThread &v) {
            std::cerr << "PixScanThread::saveResults(): m_mutex->lock():" << v.what() << std::endl;
        }
        
        RODstatus_t rs(CAN::kSuccess,StartScan,EndScan,StartWrite,EndWrite,TimeScan,Download,TimeLoop);      
        try { 
            CAN::PixCoralDbClient coralClient(true);
            std::map<std::string, PixLib::RODstatus_t> info;                    
            std::string ROD=m_grp->getName();
            info.insert(make_pair(ROD,rs));
            coralClient.storeRODstatus(m_id,&info);	  
            PixLib::ScanInfo_t information = coralClient.getMetadata<PixLib::ScanInfo_t>(m_id,CAN::kSuccess);
            std::cout << m_grp->getName() << " END OF SEND IN METADATA " <<std::endl;
        }
        catch (const SctPixelRod::BaseException &err) { 
            std::cout << m_grp->getName() << " ERROR [PixScanTask::saveResults] exception : " << err.getDescriptor() <<std::endl; 
        }
        catch (const std::exception &err) { 
            std::cout << m_grp->getName() << " ERROR [PixScanTask::saveResults] exception : " << err.what() <<std::endl; 
        }
        catch(...) { 
            std::cout << m_grp->getName() << " ERROR [PixScanTask::saveResults] unknown exception : " <<std::endl; 
        }
        try {
            m_globMutex->unlock();
        } catch (const DFMutex::NotLockedByThread &v) {
            std::cerr << "PixScanThread::saveResults(): m_mutex->unlock():" << v.what() << std::endl;
        }

    }


    //time=
            (void)gettimeofday(&t1,NULL);
    std::cout<< std::endl << "Rod: "<< m_grp->getName()<<" spent: " 
        <<t1.tv_sec - t0.tv_sec + 1e-6*(t1.tv_usec-t0.tv_usec) << " s for writing histograms." << std::endl; 
  } 
  catch (...) {
      std::cout << m_grp->getName() << " ERROR! OH!!!! m_scn->writeHisto(m_hInt, m_grp, folderstream.str());" << std::endl;	    
  }

  m_grp->publishScanDone(m_id);

}


void PixScanTask::initBlockVariables()
{
    m_exeBlock = 0;
}


std::string PixScanTask::getMainFolder()
{
    std::stringstream folderstream;
    folderstream << "/S";
    folderstream << std::setw(9) << std::setfill('0') << m_id;
    folderstream << "/" << m_grp->getName() << "/";
    std::cout << "  PixScanTask: getMainFolder(): " << folderstream.str() << " " << std::endl;
    return folderstream.str();
}
