//////////////////////////////////////////////////////////////////////
// PixAutoDisTask.cxx
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 01/04/08  Version 1.0 (KMP)
//           
//


#include <time.h>
#include <iostream>
#include <iomanip>

#include "PixAutoDisTask.h"
#include "PixUtilities/PixMessages.h"

using namespace PixLib;

PixAutoDisTask::PixAutoDisTask(PixModuleGroup *grp,
				     std::string name)
  :  PixPeriodicTask(std::move(name),5), m_grp(grp)
{

  PIX_LOG("Created PixAutDisTask");
}

PixAutoDisTask::~PixAutoDisTask() = default;


bool PixAutoDisTask::initialize()
{

  return true;
}

bool PixAutoDisTask::execute(){

   PIX_LOG("PixAutoDisTask");

   try {
      m_grp->autoDisable( );
    } catch(...){
      PIX_ERROR("Exception caugh in autodisable");
      return false;
    }

return true;

}

