//////////////////////////////////////////////////////////////////////
// IblCppMonitorTask.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 01/04/08  Version 1.0 (KMP)
//           
//

#ifndef IBLCPPMONITORTASK_H
#define IBLCPPMONITORTASK_H

#include "PixModuleGroup/PixModuleGroup.h"
#include "PixController/PPCppRodPixController.h"
#include "PixPeriodicTask.h"

#include "QuickStatus.h"
#include "UDPSock.h"

#include "DumpRodRegisters.h"
#include "ReadRodSlaveBusyStatus.h"
#include "ReadRodMasterBusyStatus.h"

#define ROD_BUSY_STATUS           (4 + RRIF_STATUS_0)

namespace PixLib {
  
  class IblCppMonitorTask : public virtual PixPeriodicTask {

  public:

    IblCppMonitorTask(PixModuleGroup *grp,
		      PixHistoServerInterface *hsi,
                      std::string id, 
		      std::string name,
		      PPCppRodPixController* rod,
		      std::string partitionName,
		      unsigned int monPrescale=1,
		      unsigned int occupRate=2,
		      unsigned int calibRate=0,
		      unsigned int statusRate=3,
		      unsigned int busyRate=1,
		      unsigned int buffRate=12,
		      unsigned int quickStatusMode=1,
		      unsigned int quickStatusConfig=0,
		      unsigned int MODsyncConfig=0,
		      unsigned int RODsyncConfig=0);
    
    virtual ~IblCppMonitorTask();
    virtual bool execute();
    virtual bool initialize();
    void analyseBusyAndTimeoutError();
    void executeQSActions();
    bool readStatus();
    bool readIblRodFmtStatus();
    void resynchModError(const std::string& rodX);
    void publishActions(std::string& moduleName, int rxCh);
    void publishCountAndTimestamp(const std::string& isName, const std::string& isName_time, unsigned int count);
    bool updateLink(QuickStatus::monitoring_infos &qsMonInfos, std::string& moduleName, int rxCh);
    QuickStatus::monitoring_infos m_qsMonInfos;

    void setQSMode(unsigned int QSMode, bool resetQS=false);
    //void analyseMicroBusyFromQS(const std::string& rodX);

    void dumpBusyRegisters();
    
  protected:

    PixModuleGroup* m_grp;
    PPCppRodPixController* m_ctrl;
    
  private:
    std::string m_partitionName;
    std::string m_runName;

    std::map<std::string, std::vector <uint8_t> > m_links;
    std::map<std::string, std::string> m_fmtstat;

    // QS parameters
    unsigned int m_fc_max; // number of fine counts before coarse count
    unsigned int m_bu_max; // max number of busy errors before action taken
    unsigned int m_to_max; // max number of timeout errors before action taken
    unsigned int m_rst_max; // max number of resets allowed
    unsigned int m_cfg_max; // max number of reconfigurations allowed
    unsigned int m_bu_cfg_max;
    unsigned int m_to_cfg_max;
    unsigned int m_resetQSActionCounterDelay;
    unsigned int m_resetQSActionCounterIdleCnt;

    //resync parameters
    bool m_MODsyncEna;
    unsigned int m_MODsyncMaxTrials = 0;
    unsigned int m_MODsyncMinIdle = 0; //measured in number of syncISReadDelay's
    unsigned int m_MODsyncISReadDelay = 0; //should be larger than the time it takes IS to update L1ID error count
    unsigned int m_MODsyncErrThresh = 0;  
    
    unsigned int m_MODdelayCnt = 0; // incremented until m_syncISReadDelay;
    std::map<unsigned int, unsigned int > m_MODsyncTrialCnt;
    std::map<unsigned int, unsigned int > m_MODsyncIdleCnt;
    
    bool m_RODsyncEna;
    unsigned int m_RODsyncMaxTrials = 0;
    unsigned int m_RODsyncMinIdle = 0; //measured in number of syncISReadDelay's
    unsigned int m_RODsyncISReadDelay = 0; //should be larger than the time it takes IS to update L1ID error count
    unsigned int m_RODsyncErrThresh = 0;  
    bool m_newRODsyncEna; // LASER - new ROD sync operation mode
    unsigned int m_newRODsyncErrThresh = 0; // LASER - new ROD sync threshold
    
    unsigned int m_RODdelayCnt = 0; // incremented until m_syncISReadDelay;
    std::map<unsigned int, std::vector<int> > m_RODerr;
    std::map<unsigned int, unsigned int > m_RODsyncTrialCnt;
    std::map<unsigned int, unsigned int > m_RODsyncIdleCnt;
    unsigned int m_RODsyncRODIdleCnt = 0;
    
    
    unsigned int m_rodMonPrescale = 0;
    unsigned int m_occupRate = 0;
    unsigned int m_calibRate = 0;
    unsigned int m_statusRate = 0;
    unsigned int m_busyRate = 0;
    unsigned int m_buffRate = 0;
    unsigned int m_count = 0;
    unsigned int m_quickStatusMode = 0;
    unsigned int m_quickStatusConfig = 0;
    unsigned int m_MODsyncConfig = 0;
    unsigned int m_RODsyncConfig = 0;
    unsigned int m_fullReadCounter = 0;
    unsigned int m_QSDisabled[32];//for 8 formatters and 4 links each

    bool m_microBusyEna;
    unsigned int m_microBusyCntThr;
    unsigned int m_microBusyMaxTrial;
    double m_microBusyFracThr;
    unsigned int m_microBusyCntArray[32];
    unsigned int m_microBusyTrialCnt[32];

    std::map<std::string, unsigned int> m_QSModeMap;
    std::map<unsigned int, std::string> m_QSModeMap_names;

    void publishQSMode();
    void setModLinkMgrp();

    unsigned int m_nRST[32][2], m_nCFG_rodBusy[32][2], m_nCFG_timeout[32][2], m_nCFG_ht[32][2], m_nCFG_modSync[32], m_nCFG_microBusy[32], m_nCFG_SEU[32], m_nCFG_userAction[32];
    bool publishIblRodFmtStat(uint32_t iblRodFmtLinkEnable, uint32_t iblRodFmtBusyStatus, uint32_t fmtBusyFromEfb, uint32_t iblRodFmtLinkWaitingForECR);
  };
}

#endif 
