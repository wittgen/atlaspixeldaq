//////////////////////////////////////////////////////////////////////
// PixThread.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 13/08/07  Version 1.0 (DLM)
//           
//

#ifndef PIXTHREAD_H
#define PIXTHREAD_H

#include "DFThreads/DFThread.h"
#include "DFThreads/DFMutex.h"
#include "PixPeriodicTask.h"
#include "PixUtilities/PixMessages.h"

#include <vector>
#include <string>

namespace PixLib {

  class PixISManager;

  class PixThread : public DFThread {
  public:
    struct PixTaskDescr {
      PixPeriodicTask *task;
      unsigned int lastExecute;
      bool eraseMe;
    };
    PixThread(std::string name);
    PixThread(std::string name, PixISManager *is, std::string isPrefix);
    virtual ~PixThread();
 
    virtual bool setNewTask(PixPeriodicTask* task);
    virtual bool removeTask(std::string name);
    virtual std::vector<std::string> listTasks();

    virtual void lock(){ m_mutex->lock();}
    virtual void unlock(){ m_mutex->unlock();}

    struct ScopeLock{      
    public:
    ScopeLock(PixThread * t, const std::string &n = ""): thread(t), name(n), unlock(false) {      
      try {
	thread->m_mutex->lock();
	//	std::cout<<"locked with :"<<name<<std::endl;
	unlock = true;
      }
      catch(DFMutex::AlreadyLockedByThread &v){
        PIX_ERROR( "PixThread::ScopeLock::ScopeLock(): m_mutex->lock():" << v.what());
      }
    }
      ~ScopeLock() {
	if (unlock) {
	  //	  std::cout<<"unlocking with: "<<name<<std::endl;
	    try {
	  thread->m_mutex->unlock();
      } catch(DFMutex::NotLockedByThread &v){
        PIX_ERROR( "PixThread::ScopeLock::~ScopeLock() m_mutex->unlock():" << v.what());
      }
	}
      }

    protected:
      PixThread * thread;
      std::string name;
      bool unlock;
    };

  protected:

    virtual void run();
    virtual void cleanup();
  
    std::string m_name;
    DFMutex* m_mutex;
    std::vector<PixTaskDescr> m_periodicTasks;
    PixISManager *m_is;
    std::string m_isPrefix;

    friend struct ScopeLock;
  };
}

#endif 
