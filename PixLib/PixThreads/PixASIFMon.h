//////////////////////////////////////////////////////////////////////
// PixASIFMon.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 12/02/11  Version 1.0 (PM)
//           
//

#ifndef PIXASIFMON_H
#define PIXASIFMON_H

#include <string>
#include "PixPeriodicTask.h"

namespace PixLib {

  class PixISManager;
  class PixBrokerSingleCrate;

  class PixASIFMon : public virtual PixPeriodicTask {

  public:
    PixASIFMon(PixISManager *ism,
	       PixBrokerSingleCrate *broker);

    virtual ~PixASIFMon();

    virtual bool initialize();
    virtual bool execute();

  protected:
    PixISManager* m_is;
    PixBrokerSingleCrate* m_broker;

  private:
    bool m_forcePreOffDone;
    bool m_emergencyPreOffDone;

  };

}

#endif 
