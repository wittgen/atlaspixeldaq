//////////////////////////////////////////////////////////////////////
// PixThreadsDDC.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 2/08/07  Version 1.0 (NG)
//           
//

#ifndef PIXTHREADSDDC_H
#define PIXTHREADSDDC_H

#include "DFThreads/DFOutputQueue.h"
#include "DFThreads/DFCountedPointer.h"
#include "DFThreads/DFThread.h"


#include <exception>

namespace PixLib {

  class PixISManager;
  class PixDcs;

  
  class PixThreadsDDC : public DFThread {
  public:
    enum commandType {DDC, FSM};
    PixThreadsDDC(std::string pp0BaseName, int rep, PixDcs *dcs, commandType ct);
    bool checkEnd();
    int failureCount(){return m_failure.size();};
  
  protected:
   virtual void run();
   virtual void cleanup();
  
  private:
   std::string m_pp0BaseName;
   int m_rep;
   PixDcs* m_dcs;
   commandType m_type;
   bool m_end;
   std::vector<int> m_failure;

  };
}

#endif 
