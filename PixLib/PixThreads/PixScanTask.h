//////////////////////////////////////////////////////////////////////
// PixScanTask.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 13/08/07  Version 1.0 (ifndef)
//           
//

#ifndef PIXSCANTASK_H
#define PIXSCANTASK_H

#include <string>
#include "PixPeriodicTask.h"
#include "DFThreads/DFMutex.h"

namespace PixLib {

  class PixModuleGroup;
  class PixScan;
  class PixHistoServerInterface;
   
  class PixScanTask : public virtual PixPeriodicTask {

  public:

    PixScanTask(PixModuleGroup *grp, PixScan *scn, PixHistoServerInterface *hInt, std::string name, std::string partitionName = "");
    virtual ~PixScanTask();

    void abort();

    //if execute returns false, the task can be deleted. Calling execute
    //after that will have no effect
    virtual bool initialize();
    virtual bool execute();
    virtual void setDirectory(std::string dir){m_dir = dir;}
    virtual void setID(int id){m_id = id;};
    std::string getMainFolder();

  protected:
    bool setModuleMask();

    bool fail();
    void initBlockVariables();
    void saveResults();

    //The task has no ownership over the PixScan or the PixModuleGroup
    PixModuleGroup *m_grp;
    PixScan *m_scn;
    PixHistoServerInterface *m_hInt;
    std::string m_dir;
    DFMutex* m_globMutex;
    bool m_stop; 
    //std::string m_scanName, m_fileName, m_grpName; //Lucia
    //std::string m_name;
    int m_exeBlock;
    int m_nstage;
    int m_nloop0;
    int m_nloop1;
    int m_nloop2;
    int m_id;
  };
}

#endif 
