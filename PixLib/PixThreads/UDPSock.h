#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <iostream>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>

#include <errno.h>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#define UDP_SIZE 1400

class UDPSock{

 public:
  
  
  UDPSock(const char* ip, uint16_t port,int timeout=5);
  UDPSock(const char* ip, std::string rodName,int timeout=5);
  UDPSock(){};
  ~UDPSock(){}; //Should close the socket here.
  void add8(uint8_t val);
  void add16(uint16_t val);
  void add32(uint32_t val);
  void clear(){len=0;}  //Just set the lenght of the buffer to 0
  void clearBuffer();  //Set the whole buffer to 0
  void CloseSocket(); //Close the socket
  void sendipAddress(const std::string ip);
  void sendipAddressAndPort();
  void sendPort();
  int sendQSCommand(uint8_t command, uint8_t mode);
  char* getLocalIp();
  void Initialize(const char* ip, std::string rodName, int timeout);
  bool SetSocketTimeout();
  uint8_t* getBuffer(){return buf;} //Note::size is not propagated.
  
  bool initializeSocket();
  uint16_t PortFromRodName(std::string rodName);
  int sendBuffer();
  int receiveData();
  int checkConnection();

 private:
  
  uint8_t buf[UDP_SIZE];
  uint32_t len;
  int sockfd;
  int to;  //socket timeout
  struct sockaddr_in server;
  socklen_t serverlen;
  struct sockaddr_in client;
  socklen_t clientlen;
  struct sockaddr_in local;
  const char* ipAddress;
  char* localIp;
  uint16_t port;
  

};

