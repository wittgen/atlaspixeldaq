//////////////////////////////////////////////////////////////////////
// PixThreadsOH.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 2/08/07  Version 1.0 (NG)
//           
//

#ifndef PIXTHREADSOH_H
#define PIXTHREADSOH_H

#include "DFThreads/DFOutputQueue.h"
#include "DFThreads/DFCountedPointer.h"
#include "DFThreads/DFThread.h"


#include <exception>

namespace PixLib {

  class PixISManager;
  class PixMessages;
  class PixHistoServerInterface;
  class Histo; 

  
  class PixThreadsOH : public DFThread {
  public:
    PixThreadsOH(PixHistoServerInterface *hInt, Histo *his, std::string folderName);
    void setStop(bool stop);
    long int getNumberOfHisto();

  protected:
   virtual void run();
   virtual void cleanup();

  private:
   PixHistoServerInterface *m_hInt;
   Histo *m_histo;
   std::string m_folderName;
   bool m_stop;
   long int m_numberOfHisto;

  };
}

#endif 
