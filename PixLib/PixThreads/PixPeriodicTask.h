//////////////////////////////////////////////////////////////////////
// PixPeriodicTask.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 13/08/07  Version 1.0 (DLM)
//           
//

#ifndef PIXPERIODICTASK_H
#define PIXPERIODICTASK_H

#include <string>

namespace PixLib {

  class PixPeriodicTask {

  public:
    
    PixPeriodicTask(std::string name, double period){m_name = name; m_period = period;}
    virtual ~PixPeriodicTask(){}
    //if execute returns false, the task can be deleted. Calling execute
    //after that will have no effect
    virtual bool initialize() = 0;
    virtual bool execute() = 0;
    void setPeriod(double period){m_period=period;}
    double getPeriod(){return m_period;}
    std::string getName(){return m_name;}

  protected:
    double m_period;
    std::string m_name;
  };
}

#endif 
