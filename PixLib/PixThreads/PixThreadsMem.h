//////////////////////////////////////////////////////////////////////
// PixThreadsMem.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 2/08/07  Version 1.0 (VD)
//           
//

#ifndef PIXTHREADSMEM_H
#define PIXTHREADSMEM_H

#include "DFThreads/DFOutputQueue.h"
#include "DFThreads/DFCountedPointer.h"
#include "DFThreads/DFThread.h"


#include <exception>

namespace PixLib {

  class PixISManager;
  
  class PixThreadsMem : public DFThread {
  public:
    PixThreadsMem(PixISManager *iSM, std::string crateName);
    
  protected:
    virtual void run();
    virtual void cleanup();
    
  private:
    PixISManager *m_is;
    std::string m_crateName;
    
  };
}

#endif 
