//////////////////////////////////////////////////////////////////////
// PixAutoDisTask.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 01/04/08  Version 1.0 (KMP)
//           
//

#ifndef PIX_AUTODIS_TASK
#define PIX_AUTODIS_TASK

#include "PixModuleGroup/PixModuleGroup.h"
#include "PixPeriodicTask.h"

namespace PixLib {

  class PixAutoDisTask : public virtual PixPeriodicTask {

  public:

    PixAutoDisTask(PixModuleGroup *grp, std::string name);    
    virtual ~PixAutoDisTask();
    virtual bool execute();
    virtual bool initialize();

    PixModuleGroup* m_grp;

  };
}

#endif  //PIX_AUTODIS_TASK

