//////////////////////////////////////////////////////////////////////
// PixASRestartMon.cxx
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 12/02/11  Version 1.0 (PM)
//           
//

#include <iostream>

#include "PixASRestartMon.h"
#include "PixUtilities/PixISManager.h"
#include "PixBroker/PixBrokerSingleCrate.h"

using namespace PixLib;

PixASRestartMon::PixASRestartMon(PixISManager *ism,
		       PixBrokerSingleCrate *broker)
  : PixPeriodicTask("PixActionServerRestartMon",5), m_is(ism), m_broker(broker) {
}

PixASRestartMon::~PixASRestartMon() {
}

bool PixASRestartMon::initialize() {
  return true;
}

bool PixASRestartMon::execute() {
  try {
    std::cout<<__PRETTY_FUNCTION__<<std::endl;
    m_broker->periodicTasks();

    // Checking whether one is allowed to restart the actions
    bool allowActionsRestart = false;
    try {
      allowActionsRestart = m_is->read<bool>("PIX_RestartActionsEnabled");
    } catch (...) {
      allowActionsRestart = false;
    }
    if( ! allowActionsRestart ) return true;

    std::string isVarActionsRestart = "PIX_RestartActions";
    int actionsRestart = 0;
    try {
      actionsRestart = m_is->read<int>(isVarActionsRestart);
    } catch (...) {
      actionsRestart = 0;
    }

    if( actionsRestart ) {
      if( allowActionsRestart ) { // Only restart when allowed
        std::cout << "Requestion restarting of actions" << std::endl;
        sleep(5);
        actionsRestart = 0;
        m_is->publish(isVarActionsRestart, actionsRestart );
        m_broker->emergency("RESTART_ACTIONS", "-");
      } else std::cerr << "ERROR: restarting of actions by PixASRestartMon is not allowed" << std::endl;
      // Acknowledge
    }
  }
  catch (...) {
    return false;
  }
  return true;
}
