//////////////////////////////////////////////////////////////////////
// PixThreadsMem.cxx
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 21/07/08  Version 1.0 (VD)
//           
//

#include <unistd.h>
#include <iostream>
#include <fstream>

#include "PixThreadsMem.h"
#include "PixUtilities/PixISManager.h"
#include "Histo/Histo.h"
#include "Config/Config.h"


using namespace PixLib;



PixThreadsMem::PixThreadsMem(PixISManager *iSM, std::string crateName) 
  : m_is(iSM), m_crateName(crateName) {
}


void PixThreadsMem::run() 
{  
  std::cout << "PixThreadsMem::run() Started thread: " << DFThread::id() << std::endl;

  while(1) {
    sleep(1);
    pid_t pid = getpid();
    int memory=0;
    std::ostringstream os;
    os << "/proc/" << pid << "/status";
    std::ifstream st(os.str().c_str());
    while (!st.eof()) {
      std::string line;
      std::getline(st, line);
      if (line.substr(0,6) == "VmRSS:") {
	std::string line2= line.substr(9,line.length()-12).c_str();
	std::istringstream test(line2);
	test >> memory;
	break;
      }
    }
    if (m_is!=0) m_is->publish(m_crateName+"/memory",memory);
    // Publish histo and config counters
    if (m_is != 0) {
      //m_is->publish(m_crateName+"/histoSize",Histo::getSize());
     // m_is->publish(m_crateName+"/histoCounter",Histo::getCounter());
      //m_is->publish(m_crateName+"/cfgSize",ConfObj::getSize());
      //m_is->publish(m_crateName+"/cfgCounter",ConfObj::getCounter());
      //m_is->publish(m_crateName+"/cfgMaskBSize",ConfMask<bool>::getSize());
     // m_is->publish(m_crateName+"/cfgMaskBCounter",ConfMask<bool>::getCounter());
      //m_is->publish(m_crateName+"/cfgMaskISize",ConfMask<unsigned short int>::getSize());
      //m_is->publish(m_crateName+"/cfgMaskICounter",ConfMask<unsigned short int>::getCounter());
    }
    cancellationPoint();
  } 
}

void PixThreadsMem::cleanup() 
{
  //The cleanup() method contains the code which has to be executed on thread termination or cancellation!" 
}


