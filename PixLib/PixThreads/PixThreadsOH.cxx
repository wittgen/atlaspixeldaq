//////////////////////////////////////////////////////////////////////
// PixThreadsOH.cxx
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 21/07/08  Version 1.0 (NG)
//           
//


#include <iostream>
#include <iomanip>

#include "PixThreadsOH.h"
#include "PixHistoServer/PixHistoServerInterface.h"
#include "Histo/Histo.h"


using namespace PixLib;



PixThreadsOH::PixThreadsOH(PixHistoServerInterface *hInt, Histo *his, std::string folderName) 
  : m_hInt(hInt), m_histo(his), m_folderName(folderName) {
  m_stop=false;
  m_numberOfHisto = 0;
}


void PixThreadsOH::run() 
{  
  std::cout << "PixThreadsOH::run() Started thread: " << DFThread::id() << std::endl;
  bool sendOH;	 

  for(;;) {
    sendOH = false;
    std::stringstream addName;
    addName << m_folderName << std::setfill('0') << std::setw(4) << m_numberOfHisto << "/";
    std::cout << "FolderName in run: " << addName.str() << " and sendOH is " << sendOH << std::endl;
    try {
      sendOH = m_hInt->sendHisto(addName.str(), *m_histo);
      std::cout << "sendOH after " << sendOH << std::endl;
    } catch (const PixHistoServerExc &e) {
      std::cout << "PixThreadsOH::run() Thread " << DFThread::id() << " exit at command sendHisto(). " 
		<< "It caught the exception: "<< e.getDescr() << std::endl;
      break;
    }
    catch (...) {
      std::cout << "PixThreadsOH::run() Thread " << DFThread::id() << " exit at command sendHisto(). Unknown exception caught."
		<< std::endl; 
      break;
    }
    if (!sendOH) {
      std::cout << "PixThreadsOH::run() Thread " << DFThread::id() << " return false at command sendHisto()" 
		<< "... EXITING ..." << std::endl;
      break;
    } else {
      cancellationPoint();
      m_numberOfHisto++;
    }
    if (m_stop) break;
  }
  std::cout << "Break the for" << std::endl;
  cancellationPoint();
}

  
void PixThreadsOH::cleanup() 
{
  //The cleanup() method contains the code which has to be executed on thread termination or cancellation!" 

  std::cout << "PixThreadsOH::cleanup(). Stopped thread: " << DFThread::id() 
	    << "After have published " << getNumberOfHisto() << " histograms in OH." << std::endl;
}


void PixThreadsOH::setStop(bool stop)
{
  m_stop = stop;
}

long int PixThreadsOH::getNumberOfHisto()
{
  return m_numberOfHisto;
}
