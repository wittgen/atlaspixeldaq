#include "PixDbCompoundTag.h"

namespace PixLib {
#if 0
} // this brace is for better Emacs indentation
#endif

PixDbCompoundTag::PixDbCompoundTag(const std::string& objectDictionaryTag, const std::string& connectivityTag, const std::string& dataTag, const std::string& aliasTag)
  : m_objectDictionaryTag(objectDictionaryTag),
    m_connectivityTag(connectivityTag),
    m_dataTag(dataTag),
    m_aliasTag(aliasTag)
{}


PixDbCompoundTag::PixDbCompoundTag() {}


std::ostream& operator<<(std::ostream& os, const PixDbCompoundTag& tags) {
  return os<<"PixDbCompoundTag(objectDictionaryTag="<<tags.objectDictionaryTag()
	   <<", connectivityTag="<<tags.connectivityTag()
	   <<", dataTag="<<tags.dataTag()
	   <<", aliasTag="<<tags.aliasTag()
	   <<")";
}


bool operator==(const PixDbCompoundTag& a, const PixDbCompoundTag& b) {
  return 
    a.objectDictionaryTag() == b.objectDictionaryTag() &&
    a.connectivityTag() == b.connectivityTag() &&
    a.dataTag() == b.dataTag() &&
    a.aliasTag() == b.aliasTag() ;
}


#if 0
{ // to make num braces even
#endif
} // namespace PixLib
