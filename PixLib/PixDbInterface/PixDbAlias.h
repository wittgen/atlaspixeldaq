// Dear emacs, this is -*-c++-*-
#ifndef PIXDBALIAS_H
#define PIXDBALIAS_H

#include <string>

namespace PixLib {
#ifndef __CINT__

  class PixDbAlias {
    std::string m_alias;
    std::string m_convention;
    std::string m_id;
  public:
    PixDbAlias(const std::string& alias, const string& convention, const string& id) 
      : m_alias(alias), m_convention(convention), m_id(id)
      {}

    std::string alias()      const { return m_alias; } 
    std::string convention() const { return m_convention; } 
    std::string id()         const { return m_id; } 

  };

#endif
}

#endif/*PIXDBALIAS_H*/
