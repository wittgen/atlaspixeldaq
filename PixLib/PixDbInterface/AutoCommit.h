// Dear emacs, this is -*-c++-*-
#ifndef AUTOCOMMIT_H
#define AUTOCOMMIT_H

namespace PixLib {

  // Using simple bool in constructor args, in
  // conjunction with default args and more than one constructor, and
  // C++ type conversion rules, has had lead to surprises....
  // 
  // Use a dedicated class to specify an auto-commit setting of a PixDbInterface backend.

  class AutoCommit {
    bool m_autocommit;
  public:
    explicit AutoCommit(bool arg) : m_autocommit(arg) {}
    operator bool() { return m_autocommit; }
  };

} // namespace PixLib

#endif/*AUTOCOMMIT_H*/
