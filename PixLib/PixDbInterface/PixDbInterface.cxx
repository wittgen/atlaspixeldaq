/////////////////////////////////////////////////////////////////////
// PixDbInterface.cxx
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 23/06/06  Version 1.0 (GG)
//           Initial release
//
//! Implementatin of methods for the abstract classes for the PixLib Database handling

#include "Histo/Histo.h"
#include "PixDbInterface/PixDbInterface.h"

#include <map>
#include <sstream>


namespace PixLib {
  namespace PixDb {

    //================================================================
    const char* DbDataTypeNames[]  = { 
      "bool", "vector<bool>", "int", "vector<int>", "unsignedint","float", "vector<float>", "double", "vector<double>", "Histo", "string", "empty"
    };

    //================================================================
    namespace {
      typedef std::map<std::string, DbDataType> DbTypeNameMap;
      DbTypeNameMap typeNameMap;

      class Initializer {
      public: 
	Initializer() {
	  for(unsigned i=0; i<sizeof(DbDataTypeNames)/sizeof(DbDataTypeNames[0]); i++) {
	    typeNameMap.insert(std::make_pair(DbDataTypeNames[i], DbDataType(i) ));
	  }
	}
      };
      Initializer dummy;
    }

    //================================================================
    const char* getTypeName(DbDataType t) {
      if(t<0 || t>DBEMPTY) {
	std::ostringstream os;
	os<<"PixLib::PixDb::getTypeName(DbDataType t): invalid t = "<<t;
	throw std::runtime_error(os.str());
      }
      return DbDataTypeNames[t];
    }

    //================================================================
    DbDataType getDbDataType(const std::string& name) {
      DbTypeNameMap::const_iterator p = typeNameMap.find(name);
      if(p == typeNameMap.end() ) {
	throw std::runtime_error("PixLib::PixDb::getDbDataType(name): unknown DB type name \""+name+"\"");
      }
      return p->second;
    }

  } // namespace PixDb
  
  //================================================================
  std::ostream& operator<<(std::ostream& os, const DbField& data) {
    data.dump(os);
    return os;
  }

  //================================================================
  std::ostream& operator<<(std::ostream& os, const DbRecord& data) {
    data.dump(os);
    return os;
  }

  //================================================================
  std::ostream& operator<<(std::ostream& os, const dbRecordIterator& r) {
    os<<"dbRecordIterator(";
    if(r.getWrapper()) {
      os<<"srcObj="<<(r.srcObject() ? r.srcObject()->getDecName() : "(null)")
	<<", srcSlot="<<r.srcSlotName()
	<<", dstObj="<<(r.dstObject() ? r.dstObject()->getDecName() : "(null)")
	<<", dstSlot="<<r.dstSlotName()
	<<", isLink="<<r.isLink()
	;
    }
    else {
      os<<"[null]";
    }
    return os<<")";
  }

  //================================================================
  std::ostream& operator<<(std::ostream& os, const dbFieldIterator& f) {
    os<<"dbFieldIterator(";
    if(f.getWrapper()) {
      const DbField *field = *f;
      if(field) {
	os<<"decName="<<field->getDecName();
      }
      else {
	os<<"field==0";
      }
    }
    else {
      os<<"[null]";
    }
    return os<<")";
  }

} // namespace PixLib
