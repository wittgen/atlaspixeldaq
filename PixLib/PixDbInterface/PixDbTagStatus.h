// Dear emacs, this is -*-c++-*-
#ifndef PIXDBTAGSTATUS_H
#define PIXDBTAGSTATUS_H

#include <string>

namespace PixLib {
#ifndef __CINT__

  class PixDbTagStatus {
    bool m_locked;
    std::string m_tag;
  public:
    PixDbTagStatus(const std::string& tag, bool locked) : m_locked(locked), m_tag(tag) {}
    std::string tag() const { return m_tag; } 
    bool locked() const { return m_locked; }
  };

#endif
}

#endif/*PIXDBTAGSTATUS_H*/
