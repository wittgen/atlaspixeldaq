// Dear emacs, this is -*-c++-*-

#ifndef PIXDBCOMPOUNDTAG_H
#define PIXDBCOMPOUNDTAG_H

#include <string>
#include <iostream>

namespace PixLib {
#ifndef __CINT__
  class PixDbCompoundTag {
    std::string m_objectDictionaryTag;
    std::string m_connectivityTag;
    std::string m_dataTag;
    std::string m_aliasTag;
  public:
    PixDbCompoundTag(const std::string& objectDictionaryTag,
		     const std::string& connectivityTag,
		     const std::string& dataTag,
		     const std::string& aliasTag);

    PixDbCompoundTag();

    std::string objectDictionaryTag() const { return m_objectDictionaryTag; }
    std::string connectivityTag() const { return m_connectivityTag; }
    std::string dataTag() const { return m_dataTag; }
    std::string aliasTag() const { return m_aliasTag; }

    void setObjectDictionaryTag(const std::string& tag) { m_objectDictionaryTag = tag; }
    void setConnectivityTag(const std::string& tag) { m_connectivityTag = tag; }
    void setDataTag(const std::string& tag) { m_dataTag = tag; }
    void setAliasTag(const std::string& tag) { m_aliasTag = tag; }

  };

  std::ostream& operator<<(std::ostream& os, const PixDbCompoundTag& t);
  bool operator==(const PixDbCompoundTag& a, const PixDbCompoundTag& b);
#endif
}

#endif/*PIXDBCOMPOUNDTAG_H*/
