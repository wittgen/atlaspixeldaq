// Dear emacs, this is -*-c++-*-
//
/////////////////////////////////////////////////////////////////////
// PixDBInterface.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 21/06/06  Version 1.0 (GG)
//           Initial release
//
//! Abstract classes for the PixLib Database handling

#include <vector>
#include <string>
#include <memory>
#include <iostream>

#include "PixDBException.h" // <-- this is for broken clients - FIXME

#ifndef __PIX_DB_INTERFACE
#define __PIX_DB_INTERFACE

namespace PixLib{
  
  using namespace std;
  using std::string;
  using std::vector;

  namespace PixDb{
    enum DbAccessType {DBREAD=0, DBCOMMIT, DBCOMMITTREE};
    enum DbDataType{DBBOOL=0, DBVECTORBOOL, DBINT, DBVECTORINT, DBULINT, DBFLOAT, DBVECTORFLOAT, DBDOUBLE, DBVECTORDOUBLE, DBHISTO, DBSTRING, DBEMPTY};

    extern const char *DbDataTypeNames[];
    const char* getTypeName(DbDataType t);
    DbDataType getDbDataType(const std::string& dbDataTypeName);
  }

  using namespace PixLib::PixDb;

  typedef time_t PixDbTimeStamp;

  class Histo; // from PixLib's Histo/Histo.h
  class PixDbCompoundTag; // from PixDbInterface/PixDbCompoundTag.h 
  class PixDbTagStatus; // from PixDbInterface/PixDbTagStatus.h
  class PixDbAlias; // from PixDbInterface/PixDbAlias.h
  
  class DbField;
  class DbRecord;
  class PixDbInterface;

  // Basic iterator functionality must be provided by different
  // implementations of PixDbInterface through this IIterator interface
  template<class T>
  class IIterator {
  public:
    typedef T ElementType;
    
    virtual T* operator->() = 0;
    
    // prefix increment
    virtual IIterator& operator++() = 0;
    
    virtual bool operator==(const IIterator& rhs) const = 0;
    
    virtual ~IIterator() {}
  };

  // ================================================================
  class IConnection {
  public:
    // A record iterator represents a conection.
    // In addition to the basic iterator functionality proveded by
    // IIterator, we need the following methods to access full connectivity information.
    virtual DbRecord* srcObject() const = 0;
    virtual string srcSlotName() const = 0;
    virtual DbRecord* dstObject() const = 0;
    virtual string dstSlotName() const = 0;
    virtual bool isLink() const = 0;
    virtual ~IConnection() {}
  };

  // ================================================================
  class IRecordIterator : virtual public IIterator<DbRecord>,
			  virtual public IConnection 
  {
  public:
    virtual IRecordIterator *clone() const = 0; // to allow assignment of concrete iterators
  };

  // ================================================================
  class IFieldIterator : virtual public IIterator<DbField> {
  public:
    virtual IFieldIterator *clone() const = 0; // to allow assignment of concrete iterators
    virtual void releaseMem() = 0;
  };


  // ================================================================
  // PixDbInterface provides concrete classes dbFieldIterator and
  // dbRecordIterator.  The concrete classes are wrappers around
  // specific implementations of the IIterator interface.  These
  // wrappers are instantiated from the following template
  //
  // Note that prefix and postfix increment operators are defined not
  // here but in a derived class to return exact type.

  template<class Iter> 
  class PixDbIterator {
  public:

    //----------------------------------------------------------------
    // This takes ownership of the passed object.
    PixDbIterator(Iter* i=0) : iter(i) {}
    
    // Need non-default copy ctr
    PixDbIterator(const PixDbIterator& i) : iter( i.iter ) {}

    // Non-default assignment
    PixDbIterator& operator=(const PixDbIterator& rhs) { 
      iter = rhs.iter;
      return *this;
    }

    // Dereferencing
    typename Iter::ElementType* operator*() { return iter->operator->(); }
    const typename Iter::ElementType* operator*() const { return iter->operator->(); }
    
    typename Iter::ElementType** operator->() { return &iter->operator->(); }
    const typename Iter::ElementType** operator->() const { return &iter->operator->(); }
    
    bool operator==(const PixDbIterator& rhs) const { 
      return (iter.get() == rhs.iter.get()) // both can be NULL
	|| (iter.get() && rhs.iter.get() && (*iter.get() == *rhs.iter.get())); // or compare objects
    }
    
    bool operator!=(const PixDbIterator& rhs) const { 
      return !(*this == rhs); 
    }

    // Access to the polymorphic iterator class
    Iter& getIterator() { return *iter.get(); }

  protected:
    std::shared_ptr<Iter> iter;
  };
  
  // ================================================================

  // Here are the concrete iterator classes.  Implementations can
  // create these objects by passing concrete objects of types they
  // derived from IRecordIterator and IFieldIterator.

  class dbRecordIterator : public PixDbIterator<IRecordIterator> {
  public:
    dbRecordIterator(IRecordIterator* i=0) : PixDbIterator<IRecordIterator>(i) {}
    DbRecord* srcObject() const { return iter->srcObject(); }
    string srcSlotName() const { return iter->srcSlotName(); }
    DbRecord* dstObject() const { return iter->dstObject(); }
    string dstSlotName() const { return iter->dstSlotName(); }
    bool isLink() const { return iter->isLink();}

    // prefix increment
    dbRecordIterator& operator++() { iter->operator++(); return *this; }

    // postfix increment
    dbRecordIterator operator++(int) { dbRecordIterator tmp(*this); iter->operator++(); return tmp; }
    
    IRecordIterator* getWrapper() { return iter.get(); }
    const IRecordIterator* getWrapper() const { return iter.get(); }
  };

  class dbFieldIterator  : public PixDbIterator<IFieldIterator > {
  public:
    dbFieldIterator(IFieldIterator* i=0) : PixDbIterator<IFieldIterator >(i) {}

    // prefix increment
    dbFieldIterator& operator++() { iter->operator++(); return *this; }

    // postfix increment
    dbFieldIterator operator++(int) { dbFieldIterator tmp(*this); iter->operator++(); return tmp; }
    
    // cleanup
    void releaseMem(){iter->releaseMem();}

    IFieldIterator* getWrapper() { return iter.get(); }
    const IFieldIterator* getWrapper() const { return iter.get(); }
  };

  //================================================================
  class DbField{ // this class is an interface to the actual field of a record
  public:
    // Accessors
   // virtual PixDbInterface* getDb() const = 0; // get the DataBase of this field
    virtual enum DbDataType getDataType() const = 0; // read the data type.
    virtual void dump(std::ostream& os) const = 0; // Dump - debugging purpose

    virtual string getName() const = 0 ; // read the field name

    // Decorated name is an implementation dependent string that 
    // can  be obtained by a call to DbField::getDecName(), and later
    // used to find the field in the context of a database by calling 
    // PixDbInterface::DbFindFieldByName(decName). 
    // There is no other meaning to it.
    virtual string getDecName() const = 0 ;

    virtual ~DbField(){};
  };


  class DbRecord{ // this class is an interface to the actual record used by an actual database
  public:
    // Accessors
    virtual PixDbInterface* getDb() const = 0; // get the DataBase of this record;
    virtual dbRecordIterator recordBegin() = 0; // the iterator to the depending records
    virtual dbFieldIterator fieldBegin() = 0; // the iterator to the record data fields
    virtual dbRecordIterator recordEnd() = 0; // the end iterator to the depending records
    virtual dbFieldIterator fieldEnd() = 0; // the end iterator to the record data fields
    virtual dbRecordIterator findRecord(const string& srcSlotName) = 0; // find a subrecord by the name of its slot
    virtual dbFieldIterator findField(const string& fieldName) = 0; // find a field by its name
    virtual void dump(std::ostream& os) const = 0; // Dump - debugging purpose

    // get the record class name - the base class name of the object
    // the record refers to (e.g. PixController)
    virtual string getClassName() const = 0;

    // This function returns the ID of the object
    virtual string getName() const = 0;

    // Decorated name is an implementation dependent string that
    // uniquely identifies a record in the context of a database.
    // It can be obtained by a call to DbRecord::getDecName(),
    // and then used as an argument for
    // PixDbInterface::DbFindRecordByName(decName).
    // There is no other meaning to it.
    virtual string getDecName() const = 0;

    virtual dbFieldIterator addField(const string& fieldName) = 0; // NEW: Create a new field attached to this record.
    virtual void eraseField(dbFieldIterator it) = 0; // Erase a field.

    // Creates a hard link to a new DbRecord.  The slot name for the
    // link is srcSlotName, and className and ID are the attributes of
    // the new record.  Empty ID means use srcSlotName as ID.
    virtual DbRecord* addRecord(const string& className, const string& srcSlotName, const string& ID="") = 0;

    // Creates a soft link to an existing record.
    // If dstSlotName is empty, take ID from the src record ("this") as the dstSlotName.
    virtual dbRecordIterator linkRecord(DbRecord* dstRec, const string& srcSlotName, const string& dstSlotName="") = 0;

    // resets an existing soft link at srcSlotName to point to dstRec/dstSlotName
    virtual dbRecordIterator updateLink(DbRecord* dstRec, const string& srcSlotName, const string& dstSlotName="") = 0;

    // Erase a hard or soft link.  The pointed to record is NOT
    // deleted, but in the case of a hard link, its parent is set to 0.
    virtual void eraseRecord(dbRecordIterator it) = 0;

    // Virtual destructor
    virtual ~DbRecord() {};

    //################################################################
    // \deprecated Use rec->addField(name) instead of 
    //     rec->pushField(db->makeField(name)) 
    // and avoid creating orphan fields even temporarily.
    virtual dbFieldIterator pushField(DbField* in) = 0;

    // \deprecated Add a record to the record list and return the corresponding iterator.
    // Use rec->addRecord(...) instead of a combination of makeRecord() and pushRecord().
    virtual dbRecordIterator pushRecord(DbRecord* in, std::string = "") = 0; 
  };


  class PixDbInterface{ // this class is an interface to the actual Db used by the library
  public:
    // Accessors
    virtual DbRecord* readRootRecord() = 0;

    // \deprecated Use DbRecord::addField() instead.
    virtual DbField* makeField(const string& fieldName) = 0;

    // Deprecated in most cases: use DbRecord::addRecord() instead.
    // The non-deprecated use is when you want to have a parentless DbRecord.
    virtual DbRecord* makeRecord(const string& className, const string& ID) = 0;

    // Methods to work with tags. How to set tags is implementation specific - e.g. it can be done through PixDbCoralDB constructor.
    virtual PixDbCompoundTag compoundTag() = 0;
    
    virtual void getExistingObjectDictionaryTags(vector<PixDbTagStatus>& output) const = 0;
    virtual void getExistingConnectivityTags(vector<PixDbTagStatus>& output, const string& idTag="") const = 0;
    virtual void getExistingDataTags(vector<PixDbTagStatus>& output, const string& idTag="") const = 0;
    virtual void getExistingAliasTags(vector<PixDbTagStatus>& output, const string& idTag="") const = 0;
    virtual void copyConnectivityTagAndSwitch(const string& newtag) = 0;
    virtual void copyDataTagAndSwitch(const string& newtag) = 0;
    virtual void copyAliasTagAndSwitch(const string& newtag) = 0;
    virtual void transactionStart() = 0; // starts an UPDATE transaction.  No need to start read-only transactions.
    virtual void transactionCommit() = 0;
    virtual string findAlias(const string& id, const string& convention) const = 0;
    virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode)= 0;

    // A call to one of this functions with
    //
    // DbAccessType==DBCOMMIT
    //   - defines the type and value of the field, and
    //   - commits the field to the database.
    // if the field was not set (DBEMPTY).  If the field has already been assigned,
    // and the new type should is the same as the previously defined type, override
    // the value with the new one.  If new type does not match, throw an exception.
    //
    // DbAccessType==DBREAD
    //   - sets the variable passed by the third argument to the value of the field.
    //     If field value is not set, or if the argument is of a wrong type, it throws an exception.
    //
    // DbAccessType==DBCOMMITTREE
    //   - this option does not make sense.  Throws an exception.
    virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, bool&) = 0;
    virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, vector<bool>&)= 0;
    virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, int&) = 0;
    virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, vector<int>&)= 0;
    virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, unsigned int &) = 0;
    virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, float&)= 0;
    virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, vector<float>&)= 0;
    virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, double&)= 0;
    virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, vector<double>&)= 0;
    virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, Histo&)= 0;
    virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, string&)= 0;

    // DbAccessType==DBCOMMIT means write out:
    //    - The connectivity DB object it represents (class name, ID)
    //    - All subfields of the record
    //    - The connections represented by all hard and soft links of this record. Writing out the connections may
    //      requre writing out additional connectivity DB objects for the linked records, but their fields are not
    //      committed.
    //
    // DbAccessType==DBCOMMITTREE means do DBCOMMIT recursively following all hard and soft links of the record.
    //      Soft links may form loops, an implementation should take care to write them out without going into
    //      an infinite loop.
    //
    // DbAccessType==DBREAD. What is DBREAD meant to do here?
    virtual DbRecord* DbProcess(DbRecord* theRecord, enum DbAccessType mode) = 0;

    virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, bool& data) = 0;
    virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode)  = 0;
    virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, vector<bool>& data) = 0;
    virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, int& data) = 0;
    virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, vector<int>& data)  = 0;
    virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, unsigned int & data)  = 0;
    virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, float& data)  = 0;
    virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, vector<float>& data)  = 0;
    virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, double& data)  = 0;
    virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, vector<double>& data)  = 0;
    virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, Histo& data)  = 0;
    virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, string& data) =0;
    virtual dbRecordIterator DbProcess(dbRecordIterator theRecord, enum DbAccessType mode)  = 0;

    // These functions return an object identified by the decorated name, or 0.
    virtual DbRecord* DbFindRecordByName(const string& decName) = 0; // find a record by its name
    //virtual DbField* DbFindFieldByName(const string& decName) = 0; // find a data field by its name
    // Virtual destructor
    virtual ~PixDbInterface(){};
  };

  std::ostream& operator<<(std::ostream& os, const DbField& data); // output operator to dump the data
  std::ostream& operator<<(std::ostream& os, const DbRecord& data); // output operator to dump the record
  std::ostream& operator<<(std::ostream& os, const dbRecordIterator& r);
  std::ostream& operator<<(std::ostream& os, const dbFieldIterator& f);
}


#endif
