#include "PacketRouter.h"
#include "UDPUartReaderManager.h"

// PPC

#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixConnectivity/RodBocConnectivity.h"
#include "PixConnectivity/PixConnectivity.h"
#include "PixUtilities/PixMessages.h"

#include <sys/stat.h>


#include <filesystem>
#include <regex>

//-------------------------------------------------------------
// Non-class functions
//----------------------------------------------------------------------------

bool __dir_exist(std::string folder) {

    struct stat sb;

    if (stat(folder.c_str(), &sb)==0 && S_ISDIR(sb.st_mode)) return true;
    else return false;
}



//----------------------------------------------------------------------------

/// configDB_hardcode can be used to make arbitrary ROD assignment to crate for testing purpouses
std::vector<RodLogProcessor::RodInfo> configDB_hardcode(UDPUartReaderManager::CrateID crateid) {

    typedef RodLogProcessor::RodInfo RodInfo;

    // crate content
    std::map<UDPUartReaderManager::CrateID, std::vector<RodInfo> > ppcmap;


    if(!SPOOFER_MODE) {
        /*
    ppcmap["ROD_CRATE_1"] = {RodInfo("ROD_C1_S7","192.168.2.70")};
    ppcmap["ROD_CRATE_3"] = {
            RodInfo("ROD_C1_S8","192.168.2.80"),
            RodInfo("ROD_C1_S110","192.168.2.110"),
            RodInfo("ROD_C1_S18","192.168.2.180")};
        */
        ppcmap["ROD_CRATE_1"] = {RodInfo("ROD_C3_S15","192.168.2.150")};
        ppcmap["ROD_CRATE_3"] = { RodInfo("ROD_C3_S18","192.168.2.180") };
    }
    else {
        ppcmap["ROD_CRATE_3"] = {
                        RodInfo("ROD_C3_S5","192.168.2.50"),
                        RodInfo("ROD_C3_S6","192.168.2.60"),
                        RodInfo("ROD_C3_S7","192.168.2.70"),
                        RodInfo("ROD_C3_S8","192.168.2.80"),
                        RodInfo("ROD_C3_S9","192.168.2.90"),
                        RodInfo("ROD_C3_S10","192.168.2.100"),
                        RodInfo("ROD_C3_S11","192.168.2.110"),
                        RodInfo("ROD_C3_S12","192.168.2.120"),
                        RodInfo("ROD_C3_S13","192.168.2.130"),
                        RodInfo("ROD_C3_S14","192.168.2.140"),
                        RodInfo("ROD_C3_S15","192.168.2.150"),
                        RodInfo("ROD_C3_S16","192.168.2.160"),
                        RodInfo("ROD_C3_S17","192.168.2.170"),
                        RodInfo("ROD_C3_S18","192.168.2.180"),
                        RodInfo("ROD_C3_S19","192.168.2.190"),
                        RodInfo("ROD_C3_S20","192.168.2.200"),
                        RodInfo("ROD_C3_S21","192.168.2.210")
    };
    }

    // test if ip is unique, keep adding to a list and check for double occurence
    std::list<std::string> tmplist;
    for(auto & item : ppcmap)
        for(auto &rod : item.second) {
            std::list<std::string>::iterator it = find(tmplist.begin(), tmplist.end(), rod.getIp());
            if(it!=tmplist.end())
                throw std::runtime_error("PixPPCLogReceiver:: Inconsistent Crate - IP assignment. Fix and recompile!");
            tmplist.push_back(rod.getIp());
        }



    // assign ip to a crate
    auto it = ppcmap.find(crateid);
    if(it==ppcmap.end()) return RodInfo::vector();
    else return ppcmap[crateid];

}

//-------------------------------------------------------------
// PixPPCLogReceiver

UDPUartReaderManager::UDPUartReaderManager(std::string _partition_name, CrateID _crateId):
        partition_name(_partition_name),
        verbosity(0),
        inited(false),
        crateid(_crateId),
        applName("PixPPCLogReceiver"),
        runflag(true){

    IPCPartition ipcPartition( partition_name );



    if(SPOOFER_MODE)
        ers::warning(PixLib::pix::daq (ERS_HERE, applName, "SPOOFER_MODE enabled. Fine for testing, should never go to production."));
}


UDPUartReaderManager::~UDPUartReaderManager() {

    // switch off logging before exit
    LPConfig ppcConfig;
    ppcConfig.mode = LogPublisher::MODE_OFF;
    send_command(ppcConfig);

    for( const auto & it : map_ip_rod ) {
        RodLogProcessor::shared_ptr rod =  it.second;
         destroy_process_lock( get_fname_lock(rod->get_destination()) );
    }

    map_ip_rod.clear();  // smart_ptr,  PPCProcessor also deleted

}


std::vector<RodLogProcessor::RodInfo> UDPUartReaderManager::get_rod_setup(UDPUartReaderManager::CrateID crateidArg, PixLib::PixConnectivity *conn) {

    typedef RodLogProcessor::RodInfo RodInfo;

    // crate content
    std::vector<RodInfo> rods;

    if(SPOOFER_MODE) { // for testing only
        ers::warning(PixLib::pix::daq (ERS_HERE, applName, "Using hardcoded setup for SR1. Spoofermode."));
        rods = configDB_hardcode(crateidArg);
        return rods;
    }

    // custom connectivity for testing
    if(CUSTOM_CONNECTIVITY) {

        ers::warning(PixLib::pix::daq (ERS_HERE, applName, "Using hardcoded setup for SR1."));
        rods = configDB_hardcode(crateidArg);

        return rods;
    }


    // Standard way of doing things
    if(conn) {

        // extract all active rods associated with the given crate

        /*
       // prints all rods in the infrastructure
       for(auto & it : conn->crates ) {
       PixLib::RodCrateConnectivity *c = it.second;
       std::cout << "Crate " << it.first << std::endl;

       for(const auto & ir : c-> rodBocs()) {
    PixLib::RodBocConnectivity* rod = ir.second;
       std::cout << "Rod: " << ir.first
     << " offlineID: " << rod->offlineId() <<  "\n"
     << " obNames: " << rod->obNames(ir.first) <<  "\n"
     << " rolNames: " << rod->rolName() <<  "\n"
     << " rodConfig: " << rod->rodConfig() <<  "\n"
     << " linkMapName: " << rod->linkMapName() <<  "\n"
     << " linkMapSName: " << rod->linkMapSName() <<  "\n"
     << " name: " << rod->name() <<  "\n"
    << std::endl;

       }
       }
       */

        auto it = conn-> crates.find(crateidArg);

        if(it!=conn-> crates.end()) {
            PixLib::RodCrateConnectivity *c = it->second;

            for(const auto & ir : c-> rodBocs()) {
                PixLib::RodBocConnectivity* rod = ir.second;
                // rod-> rodBocInfo()  -> IP
                // rod-> name()        -> e.g. ROC_C3_S11
                if(rod-> rodBocInfo().size())  rods.push_back(RodInfo(rod-> name(), rod-> rodBocInfo()));
            }

            ers::warning(PixLib::pix::daq (ERS_HERE, applName, "Using PixConnectivity to configure UDPUartReader."));

        } else {
            std::cout << "No crate with a name " << crateidArg << "found in PixConnectivity\n";
        }
    }


    return rods;
}


void UDPUartReaderManager::initialize(PixLib::PixConnectivity *conn)
{


    // get list of connected PPCs
    std::vector<RodLogProcessor::RodInfo> ppc_list = get_rod_setup (crateid, conn);

    if(verbosity>0) {
       std::cout << "Rod list before custom remove " << std::endl;
       for(const auto & rod : ppc_list) {
          std::cout << rod.getName()  << " " << rod.getIp() << std::endl;
       }
    }

    // Switch for SR1 development only!: filter only some of the rods obtained in previous step
    if(0) {
        auto new_end = std::remove_if(ppc_list.begin(), ppc_list.end(),
                                      [](RodLogProcessor::RodInfo & rod) { 
                                      return ( ( rod.getName() !="ROD_C3_S18")); 
//                                      return ( ( rod.getName() !="ROD_C3_S18") && ( rod.getName() !="ROD_C3_S15")); 
                                      }
                        );
        ppc_list.erase(new_end, ppc_list.end());

        ers::warning(PixLib::pix::daq (ERS_HERE, applName, "initialize:: Some RODs manually disabled. Fine for testing, should never go to production."));
    }

    if(verbosity>0) {
       std::cout << "Rod list assigned to crate  " << crateid << std::endl;
       for(const auto & rod : ppc_list) 
          std::cout << rod.getName()  << " " << rod.getIp() << std::endl;
    }


    // initialize Log processors (one per rod)
    // start the associated io dumpers
    for(const auto & rod : ppc_list) {

        std::string log_path = make_log_path("", rod.getName());

        if(!create_process_lock( get_fname_lock(log_path)) ) continue;

        RodLogProcessor::shared_ptr rodproc = std::make_shared<RodLogProcessor>(rod, log_path);

        map_ip_rod[rod.getIp()] = rodproc;
    }
    // ppc_list might be inaccurate at this point
    ppc_list.clear();

    std::cout << "RodLogProcessors active:" << std::endl;
    for(const auto & it : map_ip_rod) {
        std::cout << it.second-> rodInfo.getName() << std::endl;
    }
    if(!map_ip_rod.size()) {
        ers::error(PixLib::pix::daq (ERS_HERE, applName, "No ROD assigned to crate " + crateid));
        return;
    }

    // transform vector of rods to vector of ip
    std::vector<std::string> ppc_ip_list;
    std::transform(map_ip_rod.begin(), map_ip_rod.end(), back_inserter(ppc_ip_list),
                   [](const std::pair<std::string, std::shared_ptr<RodLogProcessor> > & it) {
        return it.second-> rodInfo.getIp();
    });

    // initialize Packet -> Application router
    PacketRouter * router =PacketRouter::getInstance();
    if(!router) {
        ers::error(PixLib::pix::daq (ERS_HERE, applName, "Cannot get PacketRouter instance"));
        inited = false;
        return;
    }

    router->addDestination(this, ppc_ip_list);

    // send IP/port of the host server to PPC
    LPConfig ppcConfig;
    ppcConfig.mode = LogPublisher::MODE_ON;
    ppcConfig.receiver_ip   = router->listen_ip;
    ppcConfig.receiver_port = router->listen_port;
    send_command(ppcConfig);

    inited = true;
}

#define _unused(x) ((void)x)	// prevent warnings about unused variable




void UDPUartReaderManager::run() {

    if(!inited) {
        std::stringstream ss;
        ss << "UartReaderManager could not be initialized for crate " << crateid;
        ers::fatal(PixLib::pix::daq (ERS_HERE, applName, ss.str()));
        return;
    }
    ers::log(PixLib::pix::daq (ERS_HERE, applName,"Up and running for crate " + crateid));


    std::queue< LPPacketHost::shared_ptr> queue_local;
    wait_time = std::chrono::seconds(0);


    while (runflag.load()) {


        std::unique_lock<std::mutex> lk(mutex);

        if(data_cond.wait_for(lk, wait_time, [this]{return !queue.empty() || !runflag.load(); })) {
            // thread woked-up by new data

            // make a local copy
            while(!queue.empty()){
                queue_local.push(queue.front());
                queue.pop();
            }

            lk.unlock();

            // process local copy
            while(!queue_local.empty()){
                process_packet(queue_local.front());
                queue_local.pop();
            }


        } else {

            // thread woked-up by timeout

        }

        maintenance();

        // update waiting condtion
        wait_time = wait_time.max();
        for(const auto & it : map_ip_rod) {
            RodLogProcessor::shared_ptr rp = it.second;
            wait_time = min(wait_time, rp-> condition_timeout());
        }

        assert(queue_local.size()==0);

    }

    std::cout << "Quit run" << std::endl;
}


void UDPUartReaderManager::process_packet(LPPacketHost::shared_ptr packet) {

    std::shared_ptr<RodLogProcessor> rod_proc = map_ip_rod[packet-> source_IP];
    rod_proc-> process_packet(packet);
}


void UDPUartReaderManager::add_to_queue(std::shared_ptr<LPPacketHost> packet) {

    std::lock_guard<std::mutex> lock(mutex);
    queue.push(packet);
    data_cond.notify_one();
}


void UDPUartReaderManager::send_command(const LPConfig & config) {


    for(const auto & it : map_ip_rod) {
        RodLogProcessor::shared_ptr rp = it.second;

        if(verbosity) std::cout << "Sending command to rod " << rp->rodInfo.getName() << std::endl;
        rp-> send_command(config);

        /*
    std::thread cmd_thread( [rp, config] {rp->send_command(config);} );
    cmd_thread.join();
    */

    }
}


void UDPUartReaderManager::maintenance() {

    for(const auto & it : map_ip_rod) {
        RodLogProcessor::shared_ptr rp = it.second;
        rp-> check_rod_handshake();
        rp-> check_output_stream();  // check if new file should be openned, saved etc.
    }
}


PixLib::PixConnectivity * UDPUartReaderManager::initializePixConnectivity(
                std::string partition_name_arg,
                std::string dbsrv_name,
                std::string idTag,
                std::string cfgTag,
                std::string connTag,
                std::string cfgMTag
                )
{
    using namespace PixLib;

    IPCPartition ipcPartition( partition_name_arg );

    PixDbServerInterface *DbServer = NULL;
    bool ready=false;
    int nTry=0;
    std::string msg = "Trying to connect to DbServer with name "+dbsrv_name ;
    ers::debug(PixLib::pix::daq (ERS_HERE, applName, msg));
    do {
        DbServer=new  PixDbServerInterface(&ipcPartition, dbsrv_name, PixDbServerInterface::CLIENT, ready);
        if(!ready) delete DbServer;
        else break;
        std::stringstream info;
        info << " ..... attempt number: " << nTry;
        msg = info.str();
        ers::debug(PixLib::pix::daq (ERS_HERE, applName, msg));
        nTry++;
        sleep(1);
    } while(nTry<20);
    if(!ready) {
        msg = " Impossible to connect to DbServer " + dbsrv_name;
        ers::fatal(PixLib::pix::daq (ERS_HERE, applName, msg));
        exit(-1);   // TODO Do we want this???
    } else {
        msg = "Successfully connected to DbServer with name " + dbsrv_name;
        ers::debug(PixLib::pix::daq (ERS_HERE, applName, msg));
    }

    sleep(2);  // TODO why?
    DbServer->ready();
    msg = "DbServer is enabled for reading .... ";
    ers::debug(PixLib::pix::daq (ERS_HERE, applName, msg));
    //    time=gettimeofday(&t0,NULL);

    PixConnectivity *conn = NULL;

    try{
        conn = new PixConnectivity(DbServer, connTag, connTag, connTag, cfgTag, idTag, cfgMTag);
        conn->loadConn();
        return conn;
    }
    catch(PixConnectivityExc &e) {
        msg = "PixConnectivity  exception caught in PixActionsServer: " + e.getDescr();
        ers::fatal(PixLib::pix::daq (ERS_HERE, applName, msg));
        return nullptr;
    }
    catch(...) {
        msg = " Unknown exception caught in PixActionsServer";
        ers::fatal(PixLib::pix::daq (ERS_HERE, applName, msg));
        return nullptr;
    }
}

std::string UDPUartReaderManager::make_log_path(const std::string & in_destination, const std::string & rodname ) {

    std::string destination = in_destination;

     // default file destination
    if(!destination.size()) {
        char *c = getenv("PIX_LOGS") ;
        if(c != NULL)
            destination = c;
        else {

            std::cerr << "Warning:: Environmental variable PIX_LOGS empty. Using default location for ROD logs.\n";

            // try extract tdaq version from other tdaq variable and construct log destination
            //TDAQ_INST_PATH=/daq/releases/slc6-tdaq-06/tdaq/tdaq-06-01-01/installed
            std::string tdaq_version;
            char *cc = getenv("TDAQ_INST_PATH");
            if(cc != NULL)
            {
                std::string var = cc;
                const std::regex regpattern("/+");

                std::vector<std::string> split {
                    std::sregex_token_iterator(var.begin(), var.end(), regpattern, -1), std::sregex_token_iterator() };

                tdaq_version = split[split.size()-1];

            }

            // if nothing worked, hardcoded path
            if(tdaq_version.find("tdaq") != std::string::npos)
                destination = "/daq/logs/" + tdaq_version;
            else {
                const std::string logdir_default = "/daq/logs/tdaq-06-01-01";
                destination = logdir_default;

                std::string msg =  "Unable to determine the log directiory. Setting destination base to hardcoded value: " + destination;
                    ers::error(PixLib::pix::daq (ERS_HERE, applName, msg));
            }
        }

    }

    // strip trailing / characters
    auto itback = std::find_if_not(destination.rbegin(), destination.rend(), [](char x){ return (x=='/');});
    if(destination.rbegin() != itback) destination = std::string(destination.begin(), itback.base());

    destination += "/UDPUartLog/";
    destination += rodname;

    // check if directory exist
    if(!__dir_exist(destination)) {

        namespace fs = std::filesystem; 
        std::cout << "Creating log directory "<< destination << " and setting permissions" << std::endl;

        fs::path dir(destination);
        try {
           fs::create_directory (dir);
           fs::permissions(dir, fs::perms::group_write,  fs::perm_options::add );
        }
        catch(...) {
           std::cerr << "Cannot create directory or set permissions" << destination << std::endl;
        }

    }
    std::cout << "Log directory for " << rodname << " " << destination << "\n";

    return destination;
}


bool UDPUartReaderManager::create_process_lock(const std::string & fname_lock) {

    if(std::filesystem::exists(fname_lock)) {

    std::cout << "Checking passed" << std::endl;

        std::fstream file(fname_lock);
        std::string partition;
        std::string date;

        file >> partition;
        getline(file, date);
        file.close();
        std::cout << "part " << partition << "  " << date << std::endl;

        if(partition != partition_name) {
            std::cout << "Read " << partition << " " << date << std::endl;
            ers::warning(PixLib::pix::daq (ERS_HERE, applName,"Lock " + fname_lock + " is used by " + partition + date + "."  ));
            return false;
        } else {

            std::cout << "Lock file for the same partition " << partition << " exists and  will be reopened " << fname_lock << std::endl;

            // lock exist for this infrastructure. Probably was not closed properly. Delete and recreate
            std::filesystem::remove(fname_lock);
        }
    }

    // if file does not exist

    std::ofstream ifile(fname_lock);
    auto now = std::chrono::steady_clock::now();
    ifile << partition_name <<  " locked on " << format_time(now) <<  std::endl;
    ifile.close();

    std::cout << "Process lock " << fname_lock  << "created." << std::endl;
    return true;
}


void UDPUartReaderManager::destroy_process_lock(const std::string & fname_lock) {

    std::cout << "Removing " << fname_lock << std::endl;

    std::filesystem::remove(fname_lock);
}

