#ifndef PACKETROUTER_H
#define PACKETROUTER_H

#include <iosfwd>
#include <vector>
#include <string>
#include <map>
#include <memory>
#include <mutex>
#include "LPPacketHost.h"

class UDPUartReaderManager;


class PacketRouter {

public:
    PacketRouter();

    static PacketRouter * getInstance();

    /// Add new destinatination (the pointer to app) where to forward packets from ips in client_list
    void addDestination(UDPUartReaderManager*, const std::vector<std::string> & client_list);
    bool initialize();

    uint16_t listen_port;							/// Port
    std::string listen_ip;							/// Ip

private:
    void run();										/// Infinite loop
    void route(LPPacketHost::shared_ptr packet);	/// Route one packet


    std::map<std::string, UDPUartReaderManager*> map_client_app;
    uint32_t socketId;

    int npacket; 									/// Packet counter (for debugging)
    double bpacket;									/// Retreived size (for debugging)
    int verbosity;								    /// Verbosity for logs
    std::chrono::steady_clock::time_point stopwatch;/// Timer 	       (for debugging)
    int n_err_log;									/// Number of logs informing about packet from unkonwn source
    int n_err_log_max;								/// Maximal number of n_err_log

    std::mutex mutex;
};

#endif // PACKETROUTER_H
