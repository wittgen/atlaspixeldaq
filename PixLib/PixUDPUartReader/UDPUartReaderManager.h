#ifndef UARTREADERMANAGER_H
#define UARTREADERMANAGER_H

#include "LogPublisherCmd.h"
#include "RodLogProcessor.h"

#include <iosfwd>
#include <queue>
#include <map>
#include <memory>
#include <mutex>
#include <condition_variable>
#include <atomic>

class OutStream;
class RodLogProcessor;
struct LPConfig;

namespace PixLib {
class PixConnectivity;
}

class UDPUartReaderManager {

public:

    // Structure to hold crate identifier
    typedef std::string CrateID ;

    // Constructor
    UDPUartReaderManager(std::string partition_name, CrateID _crateId);
    ~UDPUartReaderManager();

    void setInited(bool x) {
        std::lock_guard<std::mutex> lock(mutex);
        inited = x;
    }

    bool getInited() {
        std::lock_guard<std::mutex> lock(mutex);
        int x = inited;
        return x;
    }

    /// Initialize the connectivity
    PixLib::PixConnectivity * initializePixConnectivity(std::string _partition_name, std::string dbsrv_name,
                                                        std::string idTag,
                                                        std::string cfgTag,
                                                        std::string connTag,
                                                        std::string cfgMTag);
    /// Initialize the UartReaderManager
    void initialize(PixLib::PixConnectivity *conn);

    /// Main run routine
    void run();

    /// Function to be used by PacketRouter
    void add_to_queue(std::shared_ptr<LPPacketHost>);

    inline size_t queue_size() {
        std::lock_guard<std::mutex> lock(mutex);
        return queue.size();
    }

    void inline stop() {
        std::cout << "Stop of uart called" << std::endl;
        runflag = false;
        data_cond.notify_one();
    }

private:

    void process_packet(std::shared_ptr<LPPacketHost>);		/// Process packet and transmit to storage
    void send_command(const LPConfig &config);				/// Send CMD to PPC
    void maintenance();										/// Check PPC activity, storage state

    /// Get rod setup, either from the PixConnectivity (if provided), or from hardcoded values
    std::vector<RodLogProcessor::RodInfo> get_rod_setup(CrateID crateid, PixLib::PixConnectivity *conn);		/// Retrun rod config. Either from PixConnectivity or Local

    std::string make_log_path(const std::string & in_destination, const std::string &rodname);
    inline std::string get_fname_lock(const std::string & destination) { return destination + "/aprocess.lock"; }
    void destroy_process_lock(const std::string & fname_lock);
    bool create_process_lock(const std::string & fname_lock);

    std::string partition_name; 		                    /// Partition name
    int verbosity;											/// App log verbosity
    bool inited;											/// True when correctly initialized
    CrateID crateid;										/// Crate name this Manager manages
    std::string applName;									/// App name, used for PixMessages

    std::queue<std::shared_ptr<LPPacketHost>> queue;		/// Packet queue

    /// ip-> ROD map used to transfer packet to appropriate RodLogProcessor
    std::map <std::string, std::shared_ptr<RodLogProcessor> > map_ip_rod;


    std::mutex mutex;
    std::condition_variable data_cond;						/// condition variable for the running thread
    std::chrono::milliseconds wait_time;
    std::atomic<bool> runflag;							    /// Loop flag, change on SIGINT, SIGTERM, SIGABORT

};

#endif
