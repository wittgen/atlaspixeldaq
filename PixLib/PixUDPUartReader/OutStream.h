#ifndef OUTSTREAM_H
#define OUTSTREAM_H

#include <iosfwd>
#include <fstream>
#include <memory>

class OutStream {

public:

    typedef std::shared_ptr<OutStream> shared_ptr;

    OutStream();
    ~OutStream();

    void insert(std::string);

    void open();
    void close();
    void save();
    bool open_new(std::string);
    inline bool is_open() const {return file.is_open(); }

    inline int bytes_written() { return file.tellp(); }

protected:


private:

    std::ofstream file;
    std::string file_name;

};

#endif // OUTSTREAM_H
