#include "PacketRouter.h"
#include "UDPUartReaderManager.h"

#include <netdb.h>

#include <unistd.h>
#include <cstring>
#include <thread>
#include <limits.h>


PacketRouter::PacketRouter():
    npacket(0),
    bpacket(0),
    verbosity(0),
    n_err_log_max(1000){

        if(! initialize() )
            std::cerr << "PacketRouter could not be initialized. No Socket\n"; //TODO ERS

        std::thread t(&PacketRouter::run, this);
        t.detach();
    }


PacketRouter* PacketRouter::getInstance() {

    static PacketRouter*instance = 0;
    if(!instance) {
        instance = new PacketRouter();
    }
    if(!instance)
        std::cerr << "Couldn't create PacketRouter instance!" << std::endl;

    return instance;
}


void PacketRouter::addDestination(UDPUartReaderManager *application, const std::vector<std::string> & client_list) {

    std::lock_guard<std::mutex> guard(mutex);
    for( auto & cl : client_list)
        map_client_app[cl] = application;
}


bool PacketRouter::initialize() {

    // Setup socket to receive UDP packets, associate with a given port
    // dns resolution to find the functional interface to receive packet to

    char hostname [HOST_NAME_MAX];
    if(gethostname(hostname, HOST_NAME_MAX) !=0 ) {
        std::cout << "Could not get hostname" << std::endl;
        return false;
    }

    struct sockaddr_in server;
    std::memset(&server, 0, sizeof(server));

    struct addrinfo hints, *res, *p;
    int status;
    char ip_address[INET_ADDRSTRLEN];

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;

    if((status = getaddrinfo(hostname, NULL, &hints, &res)) != 0 ) {
        std::cout << "getaddrinfo: " << gai_strerror(status) << std::endl;
    }

    for(p=res; p!= NULL; p = p-> ai_next) {
        struct sockaddr_in *ipv = (struct sockaddr_in *)p-> ai_addr;
        void *addr = &(ipv->sin_addr);

        inet_ntop(p->ai_family, addr, ip_address, sizeof(ip_address));
        //       std::cout << "Resolved address: " << ip_address << std::endl;

        server.sin_addr = ipv->sin_addr;
        server.sin_port = htons(0);

        if( (socketId= socket(p-> ai_family, SOCK_DGRAM, 0)) <0){
            std::cout << "PacketRouter: socket creation faild" << std::endl;
            return false;
        }

        if(bind(socketId, (struct sockaddr*) &server, sizeof(server)) == -1) {
            close(socketId);
            continue;
        }

        break;
    }

    if(p==NULL) {
        std::cerr << "PacketRouter: Cannot bind to any socket" << std::endl;
        return false;
    }

    // get the port information
    struct sockaddr_in addr_tmp;
    socklen_t len = sizeof(addr_tmp);
    if(getsockname(socketId, (struct sockaddr *)&addr_tmp, &len) <0 ) {
        std::cerr << "Error LogReceiver: cannot retrieve sockname" << std::endl;
        return false;
    }

    listen_port = ntohs(addr_tmp.sin_port);
    listen_ip   = ip_address;


    std::cout << "PacketRouter listening on ip: " << listen_ip << " and port: " << listen_port << "\n";


    return true;
}


void PacketRouter::run() {

    int n;
    struct sockaddr_in adr_clnt;
    uint32_t len_inet;
    std::chrono::time_point<std::chrono::steady_clock>timenow;


    while(true) {


        len_inet = sizeof(adr_clnt);
        LPPacketHost::shared_ptr packet = std::make_shared<LPPacketHost>();


        n = recvfrom(socketId,            				// Socket
                packet->buffer,        			// Receiving buffer
                sizeof(packet->buffer), 			// Max recv buf size
                0,            						// Flags: no options
                (struct sockaddr *)&adr_clnt, 		// Addr
                &len_inet);  						// Addr len, in & out
        if ( n < 0 ) {
            std::cerr << "PacketRouter::run Packet receive failed" << std::endl;
            continue;
        }


        timenow = std::chrono::steady_clock::now();

        // decorate the packet
        packet-> source_IP = inet_ntoa(adr_clnt.sin_addr);
        packet-> time = timenow;
        packet-> buffer_n = n;

        //uint16_t port = ntohs( adr_clnt.sin_port);
        //std::cout << "Received packet from  "<< packet-> source_IP << " port: " << port <<   std::endl;

        // In spoofer mode packets come from same ip, but different ports.
        // Redefine IP based on the port, to correspond to full Crate3
        if(SPOOFER_MODE) {
            uint16_t port = ntohs( adr_clnt.sin_port);
            //std::cout << "Received packet from  "<< packet-> source_IP << " port: " << port <<   std::endl;
            if( (packet->source_IP != "192.168.1.143") &&
                    (packet->source_IP != "192.168.1.141") )
                continue;

            const uint16_t portbase = 50000;
            uint16_t ip_part = (port - portbase+5)*10;     //  for base 1000 (1013-1000+5)*10 = 180
            std::stringstream ss; ss <<  "192.168.2." << ip_part;

            //              std::cout << "Interpreting ip: " << packet-> source_IP << " port: " << port << " as " << ss.str() << std::endl;

            packet->source_IP = ss.str();
        }


        route(packet);

        if(verbosity) {
            npacket++;
            bpacket += packet->buffer_n;

            if(npacket == 0) stopwatch = timenow;
            auto tdiff = std::chrono::duration_cast<std::chrono::seconds> (timenow - stopwatch);
            if( tdiff > std::chrono::seconds(5)) {
                double rate = npacket / tdiff.count();
                double brate = bpacket / tdiff.count();
                std:: cout << " Performance: " << rate << "packets/sec " <<  brate << " bytes/sec " << std::endl;
                //                       << "queuee size: " << print_queue() <<  std::endl;
                npacket = 0;
                bpacket = 0;
                stopwatch = timenow;
            }

        }
    }
}


void PacketRouter::route(LPPacketHost::shared_ptr packet) {

    UDPUartReaderManager * app = nullptr;

    std::unique_lock<std::mutex> lk(mutex);
    auto it = map_client_app.find(packet->source_IP);
    if(it!=map_client_app.end())
        app = it-> second;
    lk.unlock();


    if(app)
        app->add_to_queue(packet);
    else {
        std::string msg =  "Error: received a packet from unknown source:" + packet->source_IP + ". No routing.";

        if(n_err_log < n_err_log_max)
            std::cerr << msg << std::endl;
        else  if (n_err_log == n_err_log_max)
            std::cerr << msg << ". Reach maximum "  << n_err_log_max << ". No more logging." << std::endl;
    }
}
