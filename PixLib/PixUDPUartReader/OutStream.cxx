#include "OutStream.h"

#include <iostream>

// OutStream members


OutStream::OutStream()
{
}

OutStream::~OutStream() {
    close();
}


void OutStream::close() {
    file.close();

}


bool OutStream::open_new(std::string fname) {

    if(file.is_open()) {

        std::cout << "OutStream::open_new : closing file " << file_name << std::endl;
        close();
    }

    std::cout << "OutStream::open_new : opening file " << fname<< std::endl;
    file.open(fname, std::ofstream::out);

    if(!file.is_open()) {
        std::cerr << "OutStream: File: " << fname << " cannot be opened" <<  std::endl;
        return 0;
    }
    return 1;

}


void OutStream::save() {

    file.flush();
}


void OutStream::insert(std::string msg) {

    if(!file.is_open()) {
        std::cerr << "File " << file_name << " not open." << std::endl;
        return;
    }

    file << msg;

    // flush the file after each insert on purpuse to have immediate logging
    save();
}
