#include "RodLogProcessor.h"
#include "PacketRouter.h"

// PPC
#include "LogPublisherCmd.h"
#include "Client.h"

#include "PixUtilities/PixMessages.h"

#include <iomanip>


// Non-class functions
//----------------------------------------------------------------------------


std::string format_time(std::chrono::steady_clock::time_point &t) {

    // need to convert steady_clock to system clock
    using std::chrono::steady_clock;
    using std::chrono::system_clock;
    std::time_t tconv = system_clock::to_time_t(system_clock::now() + (t-steady_clock::now()));

    struct tm tt   = *localtime( &tconv );

    std::stringstream ss;
    ss << tt.tm_year + 1900 << "/"
       << std::setfill('0') <<  std::setw(2) << tt.tm_mon +1 << "/"
       << std::setfill('0') <<  std::setw(2) << tt.tm_mday << " "
       << std::setfill('0') <<  std::setw(2) << tt.tm_hour << ":"
       << std::setfill('0') <<  std::setw(2) << tt.tm_min << ":"
       << std::setfill('0') <<  std::setw(2) << tt.tm_sec;

    return ss.str();
}

//----------------------------------------------------------------------------


RodLogProcessor::RodLogProcessor(RodInfo _rodInfo, std::string _destination) :
    Streamer(_rodInfo.getName(), 3, _destination),
    rodInfo(_rodInfo),
    time_maxPPCidle(std::chrono::seconds(150)),
//    time_maxHOSTidle(std::chrono::seconds(120)),
    n_try_handshake(0)
{
    time_lastpacket = std::chrono::steady_clock::now();   // initialization
    new_line.fill(false);

    // initialize postfixes for master, slaveS and slaveB
    Streamer::set_fname_postfix(std::vector<std::string>({"", "SlA_", "SlB_" }));
    Streamer::set_fname_ext(".log");
    Streamer::open_new();
}


// Non member function
/// Insert str after a patter, but not if found at the end of subject
void insert_after_notend(std::string & subject, std::string pattern, std::string str) {

    size_t pos = 0;
    while( (pos = subject.find(pattern, pos)) != std::string::npos) {
        if(pos == subject.size()-1) break;
        subject.insert(pos+1, str);
        pos+= 1 + str.length();
    }

}


void RodLogProcessor::process_packet(std::shared_ptr<LPPacketHost> & packet) {

    time_lastpacket = std::chrono::steady_clock::now();

    // PPC was reconfigured and we received a packet
    if(n_try_handshake) n_try_handshake = 0;

    packet->deserialize();

    const LogPublisher::LogType & log_type = static_cast< LogPublisher::LogType>(packet-> header.log_type);

    switch(log_type) {

    // discard ping PPC packet
    case LogPublisher::LogType::LOG_PING :

        std::cout << "DEBUG process_packet discarted\n";
        return;

    default:

        std::stringstream ss;

        // deal with timestamp
        if(new_line[log_type]){
            ss << format_time(packet->time)+" ";
            new_line[log_type] = false;
        }

        // reuse packet memory, timestamp is added into the body buffer
        std::string & msg = packet-> body;
        if(msg.back() == '\n')  new_line[log_type] = true;

        // put timestamp after any \n, but not at the end
        insert_after_notend(msg, "\n", format_time(packet->time)+" ");

        // TODO would be nice to redefine stream << to do the \n -> timestamp replacement on the fly. Would reduce some copying
        ss << msg;

        insert(log_type, ss.str());
    }

}


// MyClient. Not an optimal implementation. Cannot use DefaultClient.h, one hits multiple declaration error at compile stage
// due to options.ixx included in DefaultClient
class MyClient: public Client {

public:
    MyClient(const char* ip): Client(ip){
    }


    template<typename T> bool run(T& command){
        if (!server.connectToServer(srvIpAddr(), srvIpPort()) ) {
            std::cout << "Client cannot connect to PPC" << srvIpAddr() << ":" <<srvIpPort() << std::endl;
            return false;
        }


        bool retVal = true;
        if( !server.sendCommand(command) )
        {
            std::cout << "Couldn't send command to " << srvIpAddr() << ":" << srvIpPort() << std::endl;
            retVal = false;
        }
        server.disconnectFromServer();

        return retVal;
    }


};


void RodLogProcessor::send_command(const LPConfig &config) {

    time_lastconfig = std::chrono::steady_clock::now();

    // RODs are fake in spoofer mode, never send command
    if(SPOOFER_MODE) return;

    MyClient client(rodInfo.getIp().c_str());

    LogPublisherCmd logcmd;
    logcmd.config = config;

    bool res = client.run(logcmd);

    if(!res) {
        std::stringstream ss;
        ss << "Could not send config to " << rodInfo.getName() << " IP: " << client.srvIpAddr() << " port: " << client.srvIpPort() << "\n";

        ers::error(PixLib::pix::daq (ERS_HERE,  "RodLogProcessor",  ss.str()));

    }
    else {
        //   std::cout  << "Obtained result " << logcmd.result.rconfig.print() << std::endl;
        //   std::cout  << "Obtained result for rod " << rodInfo.ip << "Returned mode " << logcmd.result.rconfig.mode << std::endl;
    }

}


void RodLogProcessor::check_ppc_idle() {

    auto now  = std::chrono::steady_clock::now();

    if( (now - time_lastpacket) > time_maxPPCidle) {

        using namespace std::chrono;
        std::cout << "n_try_handshake " << n_try_handshake << std::endl;
        std::cerr << "n_try_handshake " << n_try_handshake << std::endl;

        const int max_try_handshake = 3; // log inactive rod only ntimes
        if(n_try_handshake > max_try_handshake) {

            std::stringstream ss;
            ss << format_time(now) << rodInfo.getName() <<  " innactive for " << duration_cast<seconds>(time_maxPPCidle).count() << "seconds. Resending config.";
            if(n_try_handshake == max_try_handshake) {

                ss << " Happend " << max_try_handshake << " times. Will continue trying, with no futher message.";
                ers::error(PixLib::pix::daq (ERS_HERE,  "RodLogProcessor",  ss.str()));
                ss << std::endl;

                insert(LogPublisher::LOG_MASTER, ss.str());
            }
        }

        PacketRouter * router = PacketRouter::getInstance();
        LPConfig ppcConfig;
        ppcConfig.mode = LogPublisher::MODE_ON;
        ppcConfig.receiver_ip   = router->listen_ip;
        ppcConfig.receiver_port = router->listen_port;
        send_command(ppcConfig);

        time_lastpacket = std::chrono::steady_clock::now();
        n_try_handshake++;
    }


}


//void RodLogProcessor::check_host_ping() {
//
//    auto now  = std::chrono::steady_clock::now();
//
//    if( (now - time_lastconfig) > time_maxHOSTidle) {
//        // Empty ppcConfig is enough to keep ppc alive
//        LPConfig ppcConfig;
//        send_command(ppcConfig);
//    }
//
//}


void RodLogProcessor::check_rod_handshake() {

    check_ppc_idle();
//    check_host_ping();
}


std::chrono::milliseconds RodLogProcessor::condition_timeout() {

    // get wait_time of parent Outstream
    auto timeout_stream = Streamer::condition_timeout();


    // timeout for PPC activity
    auto timeout_ppc =  std::chrono::duration_cast<std::chrono::milliseconds>(time_maxPPCidle)
            - std::chrono::duration_cast<std::chrono::milliseconds>((std::chrono::steady_clock::now() - time_lastpacket));  // if negative

//    auto timout_host_ping =  std::chrono::duration_cast<std::chrono::milliseconds>(time_maxHOSTidle)
//            - std::chrono::duration_cast<std::chrono::milliseconds>((std::chrono::steady_clock::now() - time_lastconfig));  // if negative

    return min(timeout_stream, timeout_ppc );
}

