#ifndef PPCLOGRECEIVERSTREAMER_H
#define PPCLOGRECEIVERSTREAMER_H

#include <iosfwd>
#include <fstream>
#include <chrono>
#include "OutStream.h"
#include <vector>

class Streamer {

public:

    /// Streamer with a _name containing _N files to maintain opened at _destination (filesystem path)
    Streamer(std::string _name, int _N,  std::string _destanation = "");

    void insert(int i, const std::string &);   		/// Insert a mesage into a stream i
    void check_output_stream();				  		/// check if stream needs to be saved/updated
    OutStream::shared_ptr get_stream(int i);

    template <class T>
    inline void set_fname_postfix(T x)  	 { stream_postfix = x; }		/// postfix for filename composition
    inline void set_fname_ext(std::string x) { stream_ext = x; }	/// File extension
    const std::string & get_destination() 	 { return destination; }

protected:

    void open();
    void close();
    void save();
    void open_new();

    virtual std::chrono::milliseconds condition_timeout();    /// wait time for next needed update


private:

    std::vector<std::string> get_file_names() const;		  /// Get file name with timestamp for particular stream
    bool assert_index(int i) const;

    std::string name;
    std::string destination;

    std::chrono::hours 	 maxOpenTime;			/// Maximum time file is kept open
    std::chrono::minutes maxUnsavedTime;    	/// Maximum time file is unsaved
    long maxFileSize;							/// Max file size in bytes

    std::chrono::steady_clock::time_point time_open;	/// time since open
    std::chrono::steady_clock::time_point time_save;	/// Time since last save

    std::vector<OutStream::shared_ptr> streams;  /// Pointer to the actual stream object


    std::vector<std::string> stream_postfix;	 /// string which is added to the file name base
    std::string stream_ext;					     /// Filename extension

    int N;										 /// Number of strems
    bool inited;								 /// Initialize statues flag

};


#endif // OUTSTREAM_H



