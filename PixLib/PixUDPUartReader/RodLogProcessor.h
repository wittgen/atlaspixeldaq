#ifndef RODLOGPROCESSOR_H
#define RODLOGPROCESSOR_H

#include "LPPacketHost.h"
#include "Streamer.h"

#include <vector>
#include <array>

/// SPPFER_MODE - test functionality of the software, used for debugging only
/// It sets up a range of fictious RODS
/// Packets comming from one IP are routed to various rods (decided based on the port,
/// see PacketRouter and UDPUartSpoofer)
/// Sending commands to PPC is completely disabled (see send_command)

#define SPOOFER_MODE 0

/// CUSTOM_CONNECTIVITY enables custom connectivity instead of the default PixConnectivity
/// This allows testing the code. Should not be enabled for prodution.
/// See UDPUartReaderManager for details

#define CUSTOM_CONNECTIVITY 0

struct LPConfig;


class RodLogProcessor : public Streamer {

public:

    /// Simple class to store ROD basic info
    class RodInfo {
    public:

        typedef std::vector<RodInfo> vector ;

        inline RodInfo(): ip(""), name("") {  }
        inline RodInfo(std::string _name, std::string _ip): ip(_ip), name(_name) {  }

        inline std::string getName() const {return name; }
        inline std::string getIp  () const {return ip; }


        std::string ip;
        std::string name;
    };

    typedef std::shared_ptr<RodLogProcessor> shared_ptr;


    // Constructor
    RodLogProcessor(RodInfo _rodInfo, std::string destination);
    virtual ~RodLogProcessor() {}
    void process_packet	(std::shared_ptr<LPPacketHost> & );		/// Process packet for this Rod
    void send_command	(const LPConfig & config);				/// Send HCP to PPC
    void check_rod_handshake();  								/// Check ROD Host handshake
    std::chrono::milliseconds condition_timeout();     			/// Determine data_condition timeout

    RodInfo rodInfo;											/// Rod info, used by Manager

private:

    void check_ppc_idle();										/// Reconfigure PPC if timeout passed
//    void check_host_ping();										/// Routine ping to PPC


    std::chrono::seconds 					time_maxPPCidle;	/// Max PPC idle time. Reconfigure when passed
    //std::chrono::seconds 					time_maxHOSTidle;	/// Host-> PPC handshake interval. If PPC does not get ping for >2mins, thread goes to sleap
    std::chrono::steady_clock::time_point 	time_lastpacket;	/// Time of the last packet
    std::chrono::steady_clock::time_point 	time_lastconfig;	/// Time of the last config sent to PPC

    int n_try_handshake;										/// Number of tries to reconfigure PPC

    std::array<bool,3> new_line;								/// Next packet on new line. Influences timestamp formating

};



std::string format_time(std::chrono::steady_clock::time_point &t);

#endif // RODLOGPROCESSOR_H
