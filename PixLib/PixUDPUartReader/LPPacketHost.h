#ifndef LPPACKETHOST_H
#define LPPACKETHOST_H

#include "LPPacket.h"
#include <chrono>
#include <string>


/// LogPublisher packet on the host
class LPPacketHost : public LPPacket {

public:
    typedef std::shared_ptr<LPPacketHost> shared_ptr;

    std::string source_IP;							/// Ip of client sending packet
    std::chrono::steady_clock::time_point  time;	/// Time of arrived packet


};

#endif // LPPACKETHOST_H
