#include "Streamer.h"
#include "PixUtilities/PixMessages.h"

#include <sstream>

#include <filesystem>

void archive_logs(const std::string & destination);



// Streamer members
//----------------------------------------------------------------------------

Streamer::Streamer(std::string _name, int _N,  std::string _destination):
        name(_name),
        destination(_destination),
        N(_N),
        inited(false)
{
    // default settings
    maxFileSize = 1e9;
    maxOpenTime    = std::chrono::hours(24);
    maxUnsavedTime = std::chrono::minutes(2);
    time_save = std::chrono::steady_clock::now();

    // archive old logs to save space
    archive_logs(destination);

    // instantiate streams
    for(int i=0; i< N; i++) {
        std::stringstream ss; ss << "_"<< i << ".log";		// default extension, can be modified by user
        streams.push_back(std::make_shared<OutStream>());
    }

    // open_new(get_file_name());   By defaults, files are not open! User can modify postfix settings
    inited = true;
}

OutStream::shared_ptr Streamer::get_stream(int i) {
    if(assert_index(i)) return streams[i];
    else {
        std::cerr << "Streamer::get_stream(int i) :: Invalid index " << i << std::endl;
        return nullptr;
    }
}


std::vector<std::string> Streamer::get_file_names() const {


    // now contains number of seconds since epoch
    auto now = std::chrono::duration_cast<std::chrono::seconds>(
                            std::chrono::system_clock::now().time_since_epoch()).count();

    std::vector<std::string> names;
    std::stringstream ss;
    for(int i=0; i<N; i++) {

        ss.str("");
        ss << destination << "/" <<  name << "_" <<stream_postfix[i] << now << stream_ext;
        names.push_back(ss.str());
    }

    return names;
}


void Streamer::close() {
    for(auto s: streams) s->close();
}


void Streamer::open_new() {

    // to have same timestamp, get names of all stream at once
    std::vector<std::string> fnames = get_file_names();

    bool result = true;
    for(int i=0; i<N; i++)
        if( ! streams[i]-> open_new(fnames[i]) ) result = false;

    time_open = std::chrono::steady_clock::now();

    if(!result) {
        std::string msg("Cannot open file for logging. Consult error log.");
        ers::error(PixLib::pix::daq (ERS_HERE,  "Streamer", msg) );
    }
}


void Streamer::save() {

    for(auto s: streams) s->save();
    time_save = std::chrono::steady_clock::now();
}


void Streamer::insert(int i, const std::string & msg) {

    if(!assert_index(i)) {
        std::cout << "Streamer::insert : index " << i << " out of Streamer range. Probably corrupted packet." << std::endl;
        return;
    }


    // open all streams at once to keep timestamp the same for all of them
    if(! streams[i]->is_open()) open_new();

    streams[i]-> insert(msg);

    // start a new file if file too big (check only the size of master FPGA stream)
    if(streams[0]->bytes_written() > maxFileSize) open_new();
}


void Streamer::check_output_stream() {

    // check if need to save_file

    std::chrono::steady_clock::time_point now = std::chrono::steady_clock::now();

    if( (now - time_save) > maxUnsavedTime) 	save();
    if( (now - time_open) > maxOpenTime ) 	open_new();

}


std::chrono::milliseconds Streamer::condition_timeout() {

    std::chrono::steady_clock::time_point now = std::chrono::steady_clock::now();
    auto diff1 = std::chrono::duration_cast<std::chrono::milliseconds>( maxOpenTime    - (now - time_open) ) ;   // time before timeout in ms
    auto diff2 = std::chrono::duration_cast<std::chrono::milliseconds>( maxUnsavedTime - (now - time_save) ) ;   // time before timeout in ms


    return min(diff1, diff2);
}


bool Streamer::assert_index(int i) const {

    if( (i<N) && (i>=0)) return true;
    else return false;
}


void delete_files(const std::vector<std::filesystem::path> &files) {

   using namespace std::filesystem;

         std::cout << "Removing " << std::endl;
   for(const auto  & it: files) {
      try
      {
         std::cout << it.string() << std::endl;
         remove(it);
      }
      catch (filesystem_error &e)
      {
         std::cerr << e.what() << std::endl;
      }
   }
}


// tar file
void tar(const std::filesystem::path & file) {

      auto target = file;
      target.replace_extension(".tgz");

      std::string cmd("tar -czf "+target.string()+ " -C " + file.parent_path().string() + " " + file.filename().string() );
      int i = system(cmd.c_str());
      if(i != 0 ) std::cerr << "Non-zero value of the tar command" << std::endl;
}


void archive_files(const std::vector<std::filesystem::path> &files) {

    std::cout << "Archiving: " << std::endl;
    for(const auto & it: files) {
        std::cout << it.string() << std::endl;
        tar(it);
    }
}


void archive_logs(const std::string & destination) {

   using namespace std::filesystem;

   path p (destination);

   directory_iterator end_itr;
   std::vector<std::filesystem::path> to_del;
   std::vector<std::filesystem::path> to_tar;

   // cycle through the directory
   for (directory_iterator itr(p); itr != end_itr; ++itr)
   {
      if (!is_regular_file(itr->path()))  continue;
      if( std::filesystem::path(itr-> path()).extension() != ".log") continue;

      // assign current file name to current_file and echo it out to the console.
      std::string current_file = itr->path().string();

      if(!std::filesystem::file_size(current_file)) {
         to_del.push_back(itr->path());
      }
      else {
         to_tar.push_back(itr->path());
         to_del.push_back(itr->path());
      }
   }

   archive_files(to_tar);
   delete_files(to_del);
}
