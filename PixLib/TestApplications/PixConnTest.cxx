#include <iostream>
#include <chrono>
#include <signal.h>
#include <csignal>
#include <sys/time.h>

#include <sys/wait.h>

#include "PixConnectivity/PartitionConnectivity.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "PixConnectivity/PixConnectivity.h"
#include "PixConnectivity/PixConfigDb.h"
#include "PixConnectivity/PixDisable.h"
#include "PixTrigController/TimPixTrigController.h"
#include "PixModule/PixModule.h"
#include "PixBoc/PixBoc.h"
#include "PixUtilities/PixMessages.h"

using namespace PixLib;

int main(int argc, char** argv) {

std::string idTag = "PIXEL20";
std::string connTag = "PIT-CONN";

auto start = std::chrono::steady_clock::now();

PixConnectivity *conn = new PixConnectivity(connTag, connTag, connTag, "Base", idTag, "Base");
auto end = std::chrono::steady_clock::now();
std::chrono::duration<double> elapsed_seconds = end-start;
std::cout << "PixConnectivity created, elapsed time: " << elapsed_seconds.count() << "s attempting to load ...\n";
conn->loadConn();

end = std::chrono::steady_clock::now();
elapsed_seconds = end-start;
std::cout << "elapsed time: " << elapsed_seconds.count() << "s\n";

return EXIT_SUCCESS;
}
