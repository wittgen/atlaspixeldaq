#include <string>
#include <iostream>
#include <ostream>
#include <regex>

#include <is/infoT.h>
#include <cmdl/cmdargs.h>
#include "PixConnectivity/PixConnUtility.h"
#include "PixConnectivity/PixDisable.h"

#include "nlohmann_json/json.hpp"

int main(int argc, char **argv) {

  CmdArgStr cPartition('p', "partition", "partition", "partition name", CmdArg::isREQ);
  CmdArgStr cDbServer('s', "dbserver", "dbserver", "DBServer name", CmdArg::isREQ);
  CmdArgStr cIDTag('i', "idtag", "idtag", "Database ID tag");
  CmdArgStr cCfgTag('c', "cfgtag", "cfgtag", "Database config tag");
  CmdArgStr cConnTag('n', "conntag", "conntag", "Database connectivity tag");
  CmdArgStr cModTag('m', "modtag", "modtag", "Database module config tag");
  CmdArgStr cDisable('d', "disable", "disable", "Disable name");
  CmdArgStr cRod('r', "rod", "rod", "ROD to dump", CmdArg::isREQ);
  CmdArgStr cOutput('o', "output", "output", "JSON output file name", CmdArg::isREQ);

  CmdLine  cmd(*argv, &cPartition, &cDbServer, &cIDTag, &cCfgTag, &cConnTag, &cModTag, &cDisable, &cRod, &cOutput, nullptr);
  CmdArgvIter arg_iter(--argc, ++argv);
  cmd.parse(arg_iter);

  IPCCore::init(argc, argv);

  bool ready = false;
  //These should be unique_ptrs, but TDAQ is a bit weird when it comes to IPC objects - I just let them leak and let the OS clean up
  //In general, don't do this!
  auto ipcPartition = new IPCPartition( cPartition );
  auto dbServer = new PixLib::PixDbServerInterface(ipcPartition, std::string(cDbServer), PixLib::PixDbServerInterface::CLIENT, ready);
  auto isDict = ISInfoDictionary( *ipcPartition ); 
  
  bool allTagsGiven = false;
  bool disableGiven = false;
  
  //Either all database tags are given via command line, or we try to take them from IS
  if( (cIDTag.flags() & CmdArg::GIVEN) && (cCfgTag.flags() & CmdArg::GIVEN) &&
      (cConnTag.flags() & CmdArg::GIVEN) && (cModTag.flags() & CmdArg::GIVEN) ){
      allTagsGiven = true;
  } else if( (cIDTag.flags() & CmdArg::GIVEN) || (cCfgTag.flags() & CmdArg::GIVEN) ||
             (cConnTag.flags() & CmdArg::GIVEN) || (cModTag.flags() & CmdArg::GIVEN) ){
      std::cout << "Only some database tags were given. Please provide all or none - if none are provided, values from IS are used. Terminating" << std::endl;
      return 1;
  }
  if( cDisable.flags() & CmdArg::GIVEN ) disableGiven = true; 
;
  PixLib::ConnUtility::DBTags isTags;
  //If database tags are not given, attempt to load them from IS, if we don't find them we exit
  if(!allTagsGiven) {
    try {
      isTags = PixLib::ConnUtility::getDBTagsFromIS(*ipcPartition);
    } catch (...) {
      std::cout << "Cannot read tags from IS and not all tags were given via the command line. Terminating" << std::endl;
      return 1;
    } 
  }

  std::string isDisable;
  //Similar for disable, however we don't terminate without a valid disable
  bool isDisableFound = false;
  if(!disableGiven) {
    try {
      isDisable = PixLib::ConnUtility::getDisableFromIS(*ipcPartition);
      isDisableFound = true;
    } catch (...) {
      std::cout << "Cannot read DISABLE from IS. No disable was given, continuing without DISABLE" << std::endl;
    } 
  }

  auto const connTag = allTagsGiven ? std::string{cConnTag} : isTags.connTag;
  auto const cfgTag = allTagsGiven ? std::string{cCfgTag} : isTags.cfgTag;
  auto const idTag = allTagsGiven ? std::string{cIDTag} : isTags.idTag;
  auto const cfgModTag = allTagsGiven ? std::string{cModTag} : isTags.modTag;

  std::string disable = "";
  if(disableGiven) disable = cDisable;
  else if(isDisableFound) disable = isDisable;

  std::cout << "Loading connectivity tags: " << connTag << "/" << cfgTag << "/" << idTag << "/" << cfgModTag << '\n';
  if(!disable.empty()) std::cout << "Loading disable: " << disable << '\n';
  auto conn = std::make_unique<PixLib::PixConnectivity>(dbServer, connTag, connTag, connTag, cfgTag, idTag, cfgModTag);
  conn->loadConn();

  auto rodCmdLine = std::string(cRod);
  auto regex = std::regex(",");
  std::vector<std::string> rods {
    std::sregex_token_iterator(rodCmdLine.begin(), rodCmdLine.end(), regex, -1),
    std::sregex_token_iterator()
  };

  PixLib::PixDisable* disablePtr = nullptr;
  //If we do request to load a disable which doesn't exist, we do terminate
  if(!disable.empty()) {
    disablePtr=conn->getDisable(disable);
    if(!disablePtr) {
      std::cout << "Could not load disable: " << disable << ". Terminating.\n";
      return 1;
    }
  }

  nlohmann::json j;
  auto crate_map = PixLib::ConnUtility::getRODsFromConn(rods, *conn);
  
  for(auto & [crate_name, rod_map]: crate_map ) {
    if(disablePtr) j[crate_name]["DISABLED"] = disablePtr->isDisable(crate_name) ? 1 : 0;
    else j[crate_name]["DISABLED"] = 0;
    for(auto & [rod_name, pp0_map]: rod_map ) {
      if(disablePtr) j[crate_name][rod_name]["DISABLED"] = disablePtr->isDisable(rod_name) ? 1 : 0;
      else j[crate_name][rod_name]["DISABLED"] = 0;
      for(auto & [pp0_name, module_map]: pp0_map ) {
        if(disablePtr) j[crate_name][rod_name][pp0_name]["DISABLED"] = disablePtr->isDisable(pp0_name) ? 1 : 0;
        else j[crate_name][rod_name]["DISABLED"] = 0;
        for(auto & [module_name, module_info]: module_map ) {
	    if(disablePtr) j[crate_name][rod_name][pp0_name][module_name]["DISABLED"] = disablePtr->isDisable(module_name) ? 1 : 0;
	    else j[crate_name][rod_name][pp0_name][module_name]["DISABLED"] = 0;
	    j[crate_name][rod_name][pp0_name][module_name]["PRODID"] = module_info.prodID; 
	    j[crate_name][rod_name][pp0_name][module_name]["TX"] = module_info.txCh; 
	    j[crate_name][rod_name][pp0_name][module_name]["RX1"] = module_info.rxCh1; 
	    j[crate_name][rod_name][pp0_name][module_name]["RX2"] = module_info.rxCh2;
	    //This will be executed for every module, but it will be the same for every module - so that should be fine
	    j[crate_name][rod_name][pp0_name]["TYPE"] = module_info.isFeI4 ? "IBL" : "PIXEL";
         }
       }
    } 
  }

  std::ofstream o(cOutput);
  o << std::setw(4) << j << std::endl;
 
  return 0;
}
