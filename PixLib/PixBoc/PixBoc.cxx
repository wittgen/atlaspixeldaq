/*--------------------------------------------------------------*
 *  PixBoc.cxx                                                  *
 *  Version:  1.0                                               *
 *  Created:  03 March 2004                                     *
 *  Author:   Tobias Flick                                      *
 *  Modifications: Iris Rottlaender, June 2005                  *
 *                 Tobias Flick, July 2005                      *
 *                                                              *
 *                                                              *
 *  Interface to the Pixel BOC card                             *
 *--------------------------------------------------------------*/
#include "PixModuleGroup/PixModuleGroup.h"
#include "PixMcc/PixMcc.h"
#include "PixFe/PixFe.h"
#include "RCCVmeInterface.h"
#include "PixDbInterface/PixDbInterface.h"
#include "PixController/PixController.h"
#include "PixController/BarrelRodPixController.h"
#include "PixBoc.h"
#include "BocSendConfig.h"

using namespace PixLib;

//------------------------- Constructor --------------------------

PixBoc::PixBoc() {
  m_modGroup = NULL;     
  m_name = "PixBox";
  configInit();
}

PixBoc::PixBoc(PixModuleGroup &modGrp) : m_modGroup(&modGrp) {     
  m_name = m_modGroup->getName()+"_BOC";
  
  configInit();
  Config &conf = *m_conf;

  // Create 4 Rx/Tx-Boards
  int i = 0;
  for (i=0; i<4; i++) {
    std::ostringstream txid, rxid; 
    txid << "PixTx" << i; 
    m_TxSection[i] = new PixTx(*this, txid.str());
    conf.addConfig((m_TxSection[i]->getConfigTx()));
    rxid << "PixRx" << i; 
    m_RxSection[i] = new PixRx(*this, rxid.str());
    conf.addConfig(m_RxSection[i]->getConfigRx());
  }
  setBocMode();
}

PixBoc::PixBoc(PixModuleGroup &modGrp, DbRecord *dbRecord) : m_modGroup(&modGrp)  {     
  m_name = m_modGroup->getName()+"_BOC";
  
  configInit();
  Config &conf = *m_conf;
  conf.read(dbRecord);
    
  // Create 4 Rx/Tx-Boards
  int i = 0;
  if (dbRecord == NULL) {
    for (i=0; i<4; i++) {
      std::ostringstream txid, rxid; 
      txid << "TX_" << i; 
      m_TxSection[i] = new PixTx(*this, (DbRecord *)NULL, txid.str());
      rxid << "RX_" << i; 
      m_RxSection[i] = new PixRx(*this, (DbRecord *)NULL, rxid.str());
    }
  } else {
    for(dbRecordIterator b = dbRecord->recordBegin(); b != dbRecord->recordEnd(); b++){
      //std::ostringstream txid; 
      //txid << "TX_" << i; 
      if ((*b)->getClassName() == "PixTx"){
        m_TxSection[i] = new PixTx(*this, *b, (*b)->getName());
	conf.addConfig((m_TxSection[i]->getConfigTx()));
	i++;
      }
    }
    i=0;
    for(dbRecordIterator b = dbRecord->recordBegin(); b != dbRecord->recordEnd(); b++){
      //std::ostringstream rxid; 
      //rxid << "RX_" << i; 
      if ((*b)->getClassName() == "PixRx"){
        m_RxSection[i] = new PixRx(*this, *b, (*b)->getName());
	conf.addConfig(m_RxSection[i]->getConfigRx());
	i++;
      }
    }
  }
  setBocMode();
}



PixBoc::PixBoc(PixModuleGroup &modGrp, PixDbServerInterface *dbServer, std::string dom, std::string tag) : m_modGroup(&modGrp)  {     
  m_name = m_modGroup->getName()+"_BOC";
  
  configInit();
  Config &conf = *m_conf;
  conf.read(dbServer,dom,tag,m_name);
 
  // Create 4 Rx/Tx-Boards
  int i = 0;
  
  for(i=0; i<4;i++) {
    std::ostringstream txid; 
    txid << "PixTx" << i; 
    m_TxSection[i] = new PixTx(*this, dbServer, dom, tag, txid.str());
    conf.addConfig((m_TxSection[i]->getConfigTx()));
  }

  for(i=0; i<4;i++) {
    std::ostringstream rxid; 
    rxid << "PixRx" << i; 
    m_RxSection[i] = new PixRx(*this, dbServer, dom, tag, rxid.str());
    conf.addConfig((m_RxSection[i]->getConfigRx()));
  }
  setBocMode();
}


//------------------------- Destructor ---------------------------
PixBoc::~PixBoc() {
  for(int i =0; i<4; i++) {
    delete m_TxSection[i];
    delete m_RxSection[i];
  }
  delete m_conf;
}

void PixBoc::configInit() {
  m_conf = new Config("PixBoc"); 
  Config &conf = *m_conf;

  conf.addGroup("General");
  conf["General"].addString("IpAddr", m_ipaddr, "192.168.0.10", "IBL BOC IP address", true);
  conf["General"].addString("PartialBaseDir", m_partialbasedir, "", "Base directory of partial bitfiles", true);
  conf["General"].addInt("Mode",m_bocMode,0,
			 "Boc Operation Modus",true); 
  
  
  conf.reset();

  return;
}

void PixBoc::setBocMode() {
  switch (m_bocMode){
    case 0:   m_mode=0;  // 40 Mb/s
  	      m_rxConfig = 1;
              break;
    case 2:   m_mode=1;  // 80 Mb/s
	      m_rxConfig = 2;
              break;
    default : m_mode=0; // Default 40 Mb/s
  	      m_rxConfig = 1;
              break; 
  }
  for (int i=0;i<4;i++){
    if (m_RxSection[i]!=0){
      getRx(i)->setRxConfig(m_rxConfig);
    }
  }
}

void PixBoc::loadConfig(DbRecord *rec) {
  if (rec != NULL) {
    m_conf->read(rec);
    setBocMode();
  }
}

void PixBoc::saveConfig(DbRecord *rec) {
  if (rec != NULL) {
    setBocMode();
    m_conf->write(rec);
  }
}

//----------------------- Reset the BOC --------------------------
void PixBoc::BocReset(){
  PixController *ctrl=(getModGroup().getPixController());
  ctrl->resetBoc();
  return;
}

//------------------- Inititalize the BOC ------------------------
void PixBoc::BocInit(){
  PixController *ctrl=(getModGroup().getPixController());
  ctrl->initBoc();
  return;
}

//----------- Load values from database into the BOC -------------
// prelim version of BocConfigure -> channel numbering probably uncorrect

void PixBoc::BocConfigure(){

  BarrelRodPixController* ctrl = dynamic_cast<BarrelRodPixController*>(getModGroup().getPixController());
  BocSendConfig cfg;

  setBocMode();

  cfg.nTx = 32;
  for (uint32_t ip = 0; ip<4; ip++) {
    if (m_TxSection[ip]!=0) {
      //cout << "Reading values for TX-plugin: " << getTx(ip)->plugin() << endl;
      for (uint32_t ic = 0; ic<8; ic++) {
        uint32_t dci = getTx(ip)->plugin()*10 + ic;
	cfg.txCh[ip*8+ic] = getTxChannelFromDci(dci);
	cfg.txCoarseDelay[ip*8+ic] =getTx(ip)->getTxCoarseDelay(ic);
	std::cout<<"TxCh: = "<< cfg.txCh[ip*8+ic]
		 << " coarse delay = " << (uint32_t)getTx(ip)->getTxCoarseDelay(ic) <<std::endl;
      }
    }
  }
  
  cfg.action = SetTxCoarseDelay;
  ctrl->sendCommand(cfg);

  cfg.nRx = 32;
  for (uint32_t ip = 0; ip<4; ip++) {
    if (m_RxSection[ip]!=0) {
      //cout << "BocConfigure : Reading values for RX-plugin: " << getRx(ip)->plugin() << endl;
      for (uint32_t ic = 0; ic<8; ic++) {
	uint32_t ch = getRx(ip)->plugin()*8 + 7 - ic;
	cfg.rxCh[ip*8+ic] = ch;
	cfg.delay[ip*8+ic] = getRx(ip)->getRxDataDelay(ic);
	cfg.threshold[ip*8+ic] = getRx(ip)->getRxThreshold(ic);
	cfg.gain[ip*8+ic] = 0;
	/*std::cout << "BocConfigure : ch = " << cfg.rxCh[ip*8+ic] 
                  << " delay = " << cfg.delay[ip*8+ic] 
                  << " threshold = " << cfg.threshold[ip*8+ic] << std::endl; */
      }
    } 
  }
  cfg.action = SetRxThresholdDelay;
  ctrl->sendCommand(cfg);
}


void PixBoc::setRxFibreMapping(int nRx, int rxCh[32], int dto[32], int dto2[32]){

  if(nRx>31)return;

  BarrelRodPixController* ctrl = dynamic_cast<BarrelRodPixController*>(getModGroup().getPixController());
  BocSendConfig cfg;

  cfg.nRx = nRx;
  for (int i=0; i<nRx;i++) {
    	cfg.rxCh[i] =rxCh[i];
	cfg.masterFibre[i]=getFibreFromDto(dto[i]);
	cfg.slaveFibre[i]=getFibreFromDto(dto2[i]);
	std::cout<<"For rxCh "<<cfg.rxCh[i]<<" master fibre "<<cfg.masterFibre[i]<<" slave fibre "<<cfg.slaveFibre[i]<<std::endl;
      }
  
  cfg.action = SetRxFibreMapping;
  ctrl->sendCommand(cfg);

}

uint32_t PixBoc::getFibreFromDto(uint32_t dto){

if (dto<0 || dto >47)return 0;

uint32_t fibreNr=0;

switch (dto/10){
    case 0: fibreNr = 45 - dto; break;
    case 1: fibreNr = 43 - dto; break;
    case 2: fibreNr = 29 - dto; break;
    case 3: fibreNr = 51 - dto; break;
    }

return fibreNr;

}


int PixBoc::getTxChannelFromDci(uint32_t dci){

if (dci<0 || dci >47)return -1;

int txCh=-1;

    switch(dci/10) {
      case 0: txCh = 23 - (dci%10) ; break;
      case 1: txCh = 31 - (dci%10) ; break;
      case 2: txCh = 15 - (dci%10) ; break;
      case 3: txCh =  7 - (dci%10) ; break;
    }

return txCh;

}


//------------------ Show config of the BOC Hardware -------------
void PixBoc::showBocConfig(){
  return;
}

PixBoc::PixTx::PixTx(PixBoc &pixboc, std::string name) : m_name(name), m_pixboc(pixboc) {
  configInit();
}

PixBoc::PixTx::PixTx(PixBoc &pixboc, DbRecord *dbRecord, std::string name) : m_name(name), m_pixboc(pixboc) {
  configInit();
  if (dbRecord != NULL) m_conf->read(dbRecord);
  if (m_name.substr(0,5) == "PixTx") {
    std::istringstream is(name.substr(5));
    int i;
    is >> i;
    if (i>=0 && i<4) m_plugin = i;
  }
}

PixBoc::PixTx::PixTx(PixBoc &pixboc, PixDbServerInterface *dbServer, std::string dom, std::string tag, std::string name) : m_name(name), m_pixboc(pixboc) {
  configInit();
  if(!m_conf->read(dbServer, dom, tag, pixboc.getCtrlName()+"|"+m_name)) 
    std::cout << " Problem in reading configuration" << std::endl;
  if (m_name.substr(0,5) == "PixTx") {
    std::istringstream is(name.substr(5));
    int i;
    is >> i;
    if (i>=0 && i<4) m_plugin = i;
  }
}

//------------ Setting the values for the TX board ---------------
//----------------------- BpmFineDelay ---------------------------
void PixBoc::PixTx::configInit() {

  vector<int> delayvec;
  string status;
  char number[3];
  string nr;
  int j;

  m_conf = new Config(m_name+"/PixTx"); 
  Config &conf = *m_conf;

  conf.addGroup("General");
  conf.addGroup("Bpm");
  conf.addGroup("Opt");
 
  conf["General"].addInt("Plugin", m_plugin,0,
			  "Plugin of this Tx-Board", true);
  for (j=0; j<8; j++)
    {
    sprintf(number,"%d",j+2);
    nr = number;
   
    conf["Bpm"].addInt("FineDelay"+nr, m_bpmFineDelay[j], 0,
		      "BpmFineDelay for channel "+nr, true); 
    
    conf["Bpm"].addInt("CoarseDelay"+nr, m_bpmCoarseDelay[j], 0,
		      "BpmCoarseDelay for channel"+nr, true); 
    
    conf["Bpm"].addInt("StreamInhibit"+nr, m_bpmStreamInhibit[j], 0,
		      "BpmStreamInhibit for channel"+nr, true); 
    
    conf["Bpm"].addInt("MarkSpace"+nr, m_bpmMarkSpace[j], 0x13,
		      "BpmMarkSpace for channel"+nr, true); 
    
    conf["Opt"].addInt("LaserCurrent"+nr, m_LaserCurrent[j], 0xa0,
		      "LaserCurrent for channel"+nr, true); 

    conf.reset();

  }

}
//------------ Setting the values for the TX board ---------------
//---------------------- BpmFineDelay --------------------------
void PixBoc::PixTx::setTxFineDelay(int channel, uint32_t value){  
    m_bpmFineDelay[channel]=value;
}

//---------------------- BpmCoarseDelay --------------------------
void PixBoc::PixTx::setTxCoarseDelay(int channel, uint32_t value){
    m_bpmCoarseDelay[channel]=value;
}

bool checkChannel (int channel){

  // check if channel is set correctly
  if ((channel < 0) || (channel > 7) ) {
    std::cerr << "ERROR: In " << __PRETTY_FUNCTION__ << ": channel=" << channel << std::endl;
    return false;
  }
return true;
}

//------------ Getting the values for the TX board ---------------
//---------------------- BpmFineDelay ----------------------------
uint32_t PixBoc::PixTx::getTxFineDelay(int channel){

if(!checkChannel(channel))return -1;

return m_bpmFineDelay[channel];
}

//---------------------- BpmCoarseDelay --------------------------
uint32_t PixBoc::PixTx::getTxCoarseDelay(int channel){

if(!checkChannel(channel))return -1;

return m_bpmCoarseDelay[channel];

}

PixBoc::PixRx::PixRx(PixBoc &pixboc, std::string name) : m_name(name), m_pixboc(pixboc) {
  configInit();
}

PixBoc::PixRx::PixRx(PixBoc &pixboc, DbRecord *dbRecord, std::string name) : m_name(name), m_pixboc(pixboc) {
  configInit();
  if (dbRecord != NULL) m_conf->read(dbRecord); 
  if (m_name.substr(0,5) == "PixRx") {
    std::istringstream is(name.substr(5));
    int i;
    is >> i;
    if (i>=0 && i<4) {
      int RxMap[4] = { 3, 2, 0, 1 };
      m_plugin = RxMap[i];
    }
  }
}

PixBoc::PixRx::PixRx(PixBoc &pixboc, PixDbServerInterface *dbServer, std::string dom, std::string tag, std::string name) : m_name(name), m_pixboc(pixboc) {
  configInit();
  if(!m_conf->read(dbServer, dom, tag, pixboc.getCtrlName()+"|"+m_name)) 
    std::cout << " Problem in reading configuration" << std::endl;
  if (m_name.substr(0,5) == "PixRx") {
    std::istringstream is(name.substr(5));
    int i;
    is >> i;
    if (i>=0 && i<4) {
      int RxMap[4] = { 3, 2, 0, 1 };
      m_plugin = RxMap[i];
    } 
  }
}


void PixBoc::PixRx::configInit() {
  vector<int> delayvec;
  string status;
  char number[3];
  string nr;
  int j;
  
  m_conf = new Config(m_name+"/PixRx"); 
  Config &conf = *m_conf;

  conf.addGroup("General");
  conf.addGroup("Opt");
 
  conf["General"].addInt("Plugin", m_plugin,0,
			  "Plugin of this Rx-Board", true);
  for (j=0; j<8; j++) {
    sprintf(number,"%d",j+2);
    nr = number;
    
    conf["General"].addInt("DataDelay"+nr, m_dataDelayInt[0][j], 0,
			   "DataDelay for channel "+nr, true); 
    conf["General"].addInt("DataDelay_40_"+nr, m_dataDelayInt[1][j], -1,
			   "DataDelay for channel "+nr+" 40 Mb/s", true); 
    conf["General"].addInt("DataDelay_80_"+nr, m_dataDelayInt[2][j], -1,
			   "DataDelay for channel "+nr+" 80 Mb/s", true); 
    
    conf["Opt"].addInt("RxThreshold"+nr, m_thresholdInt[0][j], 0xb7,
		       "RxThreshold for channel"+nr, true); 
    conf["Opt"].addInt("RxThreshold_40_"+nr, m_thresholdInt[1][j], -1,
		       "RxThreshold for channel"+nr+" 40 Mb/s", true); 
    conf["Opt"].addInt("RxThreshold_80_"+nr, m_thresholdInt[2][j], -1,
		       "RxThreshold for channel"+nr+" 80 Mb/s", true); 

    conf["General"].addFloat("DataDelayF_40_"+nr, m_dataDelay[1][j], 5.0,
			   "DataDelay for channel "+nr+" 40 Mb/s", true); 
    conf["General"].addFloat("DataDelayF_80_"+nr, m_dataDelay[2][j], 5.0,
			   "DataDelay for channel "+nr+" 80 Mb/s", true); 
    
    conf["Opt"].addFloat("RxThresholdF_40_"+nr, m_threshold[1][j], 128.0,
		       "RxThreshold for channel"+nr+" 40 Mb/s", true); 
    conf["Opt"].addFloat("RxThresholdF_80_"+nr, m_threshold[2][j], 128.0,
		       "RxThreshold for channel"+nr+" 80 Mb/s", true); 
  }
  conf["Opt"].addFloat("Viset", m_viset[0], 0.750,
		       "Viset value (V)", true); 
  conf["Opt"].addFloat("Viset_40", m_viset[1], -0.1,
		       "Viset value 40 Mb/s (V)", true); 
  conf["Opt"].addFloat("Viset_80", m_viset[2], -0.1,
		       "Viset value 80 Mb/s(V)", true); 

  conf.reset();
  m_curRxConfig = 1;
}

//------------ Setting the values for the RX board ---------------
//------------------------ DataDelay -----------------------------
void PixBoc::PixRx::setRxDataDelay(int channel, float value){

    m_dataDelay[0][channel]=value;
    if (m_curRxConfig==1 || m_curRxConfig==2) m_dataDelay[m_curRxConfig][channel]=value;

}

//----------------------- RxThreshold ----------------------------
void PixBoc::PixRx::setRxThreshold(int channel, float value){

    m_threshold[0][channel]=value;
    if (m_curRxConfig==1 || m_curRxConfig==2) m_threshold[m_curRxConfig][channel]=value;
}

//------------ Getting the values for the RX board ---------------
//------------------------ DataDelay -----------------------------
float PixBoc::PixRx::getRxDataDelay(int channel){

if(!checkChannel(channel))return -1;
return m_dataDelay[0][channel];

}

//----------------------- RxThreshold ----------------------------
float PixBoc::PixRx::getRxThreshold(int channel){

if(!checkChannel(channel))return -1;
return m_threshold[0][channel];
}

void PixBoc::PixRx::setRxConfig(int set) {
  if (set==1 || set==2) {
    m_curRxConfig = set;
    for (int i=0; i<8; i++) {
      m_threshold[0][i] = m_threshold[m_curRxConfig][i];
      m_dataDelay[0][i] = m_dataDelay[m_curRxConfig][i];
    }
    m_viset[0] = m_viset[m_curRxConfig];
  }
}

int  PixBoc::PixRx::getRxConfig() {
  return m_curRxConfig;
}

bool PixBoc::writeConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, std::string ptag, unsigned int revision) {
  unsigned int tmprev=revision;
  if(m_conf->write(DbServer, domainName, tag, m_name,"PixBoc", ptag, tmprev))
    return true;
  else {
    std::cout << "PixBoc : ERROR in writing config on DBserver" << std::endl;
    return false;
  }
}

bool PixBoc::readConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, unsigned int revision) {
  if(m_conf->read(DbServer, domainName, tag, m_name,revision))
    return true;
  else {
    std::cout << "PixController : ERROR in writing config on DBserver" << std::endl;
    return false;
  } 
}
