/*--------------------------------------------------------------*
 *  PixBoc.h                                                    *
 *  Version:  1.0                                               *
 *  Created:  03 March 2004                                     *
 *  Author:   Tobias Flick                                      *
 *  Modifications: Iris Rottlaender, June 2005                  * 
 *                 Tobias Flick, July 2005                      *
 *                                                              *
 *  Interface to the PixelBoc                                   *
 *--------------------------------------------------------------*/

#ifndef _PIXLIB_PIXBOC
#define _PIXLIB_PIXBOC

#include "PixModuleGroup/PixModuleGroup.h"
#include "PixController/PixController.h"
#include "Config/Config.h"
#include "Config/ConfObj.h"

#include <string>
#include <sstream>

namespace PixLib { 
  
class DbRecord;
class PixModuleGroup;
class Config;
class PixDbServerInterface;

//------------------------- Class PixBoc--------------------------
// The class contains the inteface between the BocCard owned by 
// PixModuleGroup and accessed through RodPixController. 
// The class has two subclasses: PixTx and PixRx. All 
// accessfunctions are part of the class which should be used 
// from the user outside PixLib.

class PixBoc {
public:

  class PixTx {
    friend class PixBoc;
  public: 
    //Constructor:
    PixTx(PixBoc &pixboc, std::string name);
    PixTx(PixBoc &pixboc, DbRecord *dbRecord, std::string name);
    PixTx(PixBoc &pixboc, PixDbServerInterface *dbServer, std::string dom, std::string tag, std::string name);

    //Destructor
    virtual ~PixTx(){};

    //Function to get config object
    virtual Config* getConfigTx(){return m_conf;};

    //Functions to set values to specific TX registers
    virtual void setTxLaserCurrent(int channel, uint32_t value){m_LaserCurrent[channel]=value;}
    virtual void setTxMarkSpace(int channel, uint32_t value){m_bpmMarkSpace[channel]=value;}
    virtual void setTxFineDelay(int channel, uint32_t value);
    virtual void setTxCoarseDelay(int channel, uint32_t value);

    //Accessors
    virtual uint32_t getTxCoarseDelay(int channel);
    virtual uint32_t getTxFineDelay(int channel);
    virtual uint32_t getTxLaserCurrent(int channel){ return m_LaserCurrent[channel];}
    virtual uint32_t getTxMarkSpace(int channel){ return m_bpmMarkSpace[channel];}
    virtual PixBoc &getPixBoc() { return m_pixboc; };

    //Accessors
    virtual int plugin() { return m_plugin; };
    virtual void setPlugin(int i) { m_plugin = i; };

  protected:
    Config *m_conf;
    int m_plugin;
    int m_bpmFineDelay[8];
    int m_bpmCoarseDelay[8];
    int m_bpmStreamInhibit[8];
    int m_bpmMarkSpace[8];
    int m_LaserCurrent[8];
    std::string m_name;
    PixBoc &m_pixboc;
    virtual void configInit();
    
  };

  class PixRx {
    friend class PixBoc;
  public: 
    //Constructor
    PixRx(PixBoc &pixboc, std::string name);
    PixRx(PixBoc &pixboc, DbRecord *dbRecord, std::string name);
    PixRx(PixBoc &pixboc, PixDbServerInterface *dbServer, std::string dom, std::string tag, std::string name);
 
    //Destructor
    virtual ~PixRx(){};

    //Function to get config object
    
    virtual Config* getConfigRx(){return m_conf;};

    //Functions to set values to specific RX registers
    virtual void setRxDataDelay(int channel, float value);
    virtual void setRxThreshold(int channel, float value);

    //Accessors

    virtual float getRxDataDelay(int channel);
    virtual float getRxThreshold(int channel);

    virtual PixBoc &getPixBoc() { return m_pixboc; };
    virtual int plugin() { return m_plugin; };
    virtual void setPlugin(int i) { m_plugin = i; };
    virtual float getViset() { return m_viset[0]; };
    virtual void setViset(float viset) { m_viset[0] = m_viset[m_curRxConfig] = viset; };
    
    virtual uint32_t getRxPhase(int channel){ return 255;};//specific IBL doing nothing in Pixels
    virtual void setRxPhase(int channel, int value){  }//specific IBL doing nothing in Pixels

  protected:
    void setRxConfig(int set);
    int  getRxConfig();
    
    Config *m_conf;
    int m_plugin;
    float m_dataDelay[3][8];
    float m_threshold[3][8];
    int32_t m_dataDelayInt[3][8];
    int32_t m_thresholdInt[3][8];
    float m_viset[3];
    uint32_t m_phases[8];
    std::string m_name;
    PixBoc &m_pixboc;
    int m_curRxConfig;
    virtual void configInit();
  };

  //Constructor
  PixBoc();
  PixBoc(PixModuleGroup &modGrp);
  PixBoc(PixModuleGroup &modGrp, DbRecord *dbRecord);
  PixBoc(PixModuleGroup &modGrp, PixDbServerInterface *dbServer, std::string dom, std::string tag);
  
  //Destructor
  virtual ~PixBoc();

  //Functions to set values to specific BOC registers
  virtual void BocReset();                  //Reset the BOC
  virtual void BocInit();                   //Initialize the BOC
  virtual void BocConfigure();              //Load configdata to the BOC
  virtual void showBocConfig();             //prints out the config of the BOC
  
  virtual void loadConfig(DbRecord *rec);
  virtual void saveConfig(DbRecord *rec);

  virtual bool writeConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, std::string ptag="Boc_Tmp", unsigned int revision=0xffffffff);
  virtual bool readConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, unsigned int revision=0xffffffff);
    

  virtual void setBocMode();
  virtual void setRxFibreMapping(int nRx, int rxCh[32], int dto[32], int dto2[32]);
  int getTxChannelFromDci(uint32_t dci);
  uint32_t getFibreFromDto(uint32_t dto);

  //Accessors
  virtual PixModuleGroup &getModGroup() { return *m_modGroup; };
  virtual std::string getCtrlName() { return m_name; };
  virtual PixTx* getTx(int nr) { return m_TxSection[nr]; };
  virtual PixRx* getRx(int nr) { return m_RxSection[nr]; };
  virtual Config* getConfig() { return m_conf; };
  virtual int getRxConfig() { return m_rxConfig; }; 

 protected:
  Config *m_conf;

  virtual void configInit();

  PixTx* m_TxSection[4];       //! Pointer to the TX section
  PixRx* m_RxSection[4];       //! Pointer to the RX section
  PixModuleGroup *m_modGroup;  //! Pointer to the module group using this PixBoc
  std::string m_name;          //! Name of the controller
  std::string m_ipaddr;                  //! IP address of the IBLBOC card
  std::string m_partialbasedir;


  int m_mode;
  int m_bocMode;
  int m_rxConfig;
};

} //end of namespace PixLib

#endif      //_PIXLIB_PIXBOC

