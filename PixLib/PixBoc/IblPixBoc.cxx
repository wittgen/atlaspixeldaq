/*--------------------------------------------------------------*
 *  IblPixBoc.h                                                 *
 *  Version:  1.0                                               *
 *  Created:  16 June 2016                                      *
 *  Author:   JuanAn                                            *
 *                                                              *
 *  Interface to the IblBoc                                     *
 *--------------------------------------------------------------*/

#include "PixModuleGroup/PixModuleGroup.h"
#include "PixMcc/PixMcc.h"
#include "PixFe/PixFe.h"
#include "RCCVmeInterface.h"
#include "PixDbInterface/PixDbInterface.h"
#include "PixController/PPCppRodPixController.h"
#include "IblPixBoc.h"
#include "BocSendConfig.h"

using namespace PixLib;

//------------------------- Constructor --------------------------

IblPixBoc::IblPixBoc( ) : PixBoc() {  }


IblPixBoc::IblPixBoc(PixModuleGroup &modGrp) : PixBoc() {  
  std::cout<<__PRETTY_FUNCTION__<<std::endl;
  m_modGroup = &modGrp;
  m_name = m_modGroup->getName()+"_BOC";

  configInit();
  Config &conf = *m_conf;
    
  // Create 4 Rx/Tx-Boards
  int i = 0;
  for (i=0; i<4; i++) {
    std::ostringstream txid, rxid; 
    txid << "PixTx" << i; 
    m_TxSection[i] = new IblTx(*this, txid.str());
    conf.addConfig((m_TxSection[i]->getConfigTx()));
    rxid << "PixRx" << i; 
    m_RxSection[i] = new IblRx(*this, rxid.str());
    conf.addConfig(m_RxSection[i]->getConfigRx());
  }

}

IblPixBoc::IblPixBoc(PixModuleGroup &modGrp, DbRecord *dbRecord) : PixBoc()  {     
std::cout<<__PRETTY_FUNCTION__<<std::endl;

  m_modGroup = &modGrp;
  m_name = m_modGroup->getName()+"_BOC";
  
  configInit();
  Config &conf = *m_conf;
  conf.read(dbRecord);
    
  // Create 4 Rx/Tx-Boards
  int i = 0;
  if (dbRecord == NULL) {
    for (i=0; i<4; i++) {
      std::ostringstream txid, rxid; 
      txid << "TX_" << i; 
      m_TxSection[i] = new IblTx(*this, (DbRecord *)NULL, txid.str());
      rxid << "RX_" << i; 
      m_RxSection[i] = new IblRx(*this, (DbRecord *)NULL, rxid.str());
    }
  } else {
    for(dbRecordIterator b = dbRecord->recordBegin(); b != dbRecord->recordEnd(); b++){
      //std::ostringstream txid; 
      //txid << "TX_" << i; 
      if ((*b)->getClassName() == "PixTx"){
        m_TxSection[i] = new IblTx(*this, *b, (*b)->getName());
	conf.addConfig((m_TxSection[i]->getConfigTx()));
	i++;
      }
    }
    i=0;
    for(dbRecordIterator b = dbRecord->recordBegin(); b != dbRecord->recordEnd(); b++){
      //std::ostringstream rxid; 
      //rxid << "RX_" << i; 
      if ((*b)->getClassName() == "PixRx"){
        m_RxSection[i] = new IblRx(*this, *b, (*b)->getName());
	conf.addConfig(m_RxSection[i]->getConfigRx());
	i++;
      }
    }
  }

}



IblPixBoc::IblPixBoc(PixModuleGroup &modGrp, PixDbServerInterface *dbServer, std::string dom, std::string tag) : PixBoc()  {
std::cout<<__PRETTY_FUNCTION__<<std::endl;

  m_modGroup = &modGrp;
  m_name = m_modGroup->getName()+"_BOC";
  
  configInit();
  Config &conf = *m_conf;
  conf.read(dbServer,dom,tag,m_name);
 
  // Create 4 Rx/Tx-Boards
  int i = 0;
  
  for(i=0; i<4;i++) {
    std::ostringstream txid; 
    txid << "PixTx" << i; 
    m_TxSection[i] = new IblTx(*this, dbServer, dom, tag, txid.str());
    conf.addConfig((m_TxSection[i]->getConfigTx()));
  }

  for(i=0; i<4;i++) {
    std::ostringstream rxid; 
    rxid << "PixRx" << i; 
    m_RxSection[i] = new IblRx(*this, dbServer, dom, tag, rxid.str());
    conf.addConfig((m_RxSection[i]->getConfigRx()));
  }

}

void IblPixBoc::connect(){
m_connected=true;
}

//------------------------- Destructor ---------------------------
IblPixBoc::~IblPixBoc() {

}


//----------- Load values from database into the BOC -------------
// prelim version of BocConfigure -> channel numbering probably uncorrect

void IblPixBoc::BocConfigure(){

PPCppRodPixController* ctrl = dynamic_cast<PPCppRodPixController*>(getModGroup().getPixController());

  BocSendConfig cfg;

  cfg.nTx = 32;
  for (uint32_t ip = 0; ip<4; ip++) {
    if (m_TxSection[ip]!=0) {
      //cout << "Reading values for TX-plugin: " << getTx(ip)->plugin() << endl;
      for (uint32_t ic = 0; ic<8; ic++) {
        uint32_t dci = getTx(ip)->plugin()*10 + ic;
	cfg.txCh[ip*8+ic] = getTxChannelFromDci(dci);
	cfg.txCoarseDelay[ip*8+ic] =getTx(ip)->getTxCoarseDelay(ic);
	//std::cout<<"TxCh: = "<< cfg.txCh[ip*8+ic]<< " coarse delay = " << (uint32_t)getTx(ip)->getTxCoarseDelay(ic) <<std::endl;
      }
    }
  }
  
  cfg.action = SetTxCoarseDelay;
  ctrl->sendCommand(cfg);

  cfg.nRx = 32;
  for (uint32_t ip = 0; ip<4; ip++) {
    if (m_RxSection[ip]!=0) {
      //cout << "BocConfigure : Reading values for RX-plugin: " << getRx(ip)->plugin() << endl;
      for (uint32_t ic = 0; ic<8; ic++) {
        uint32_t dto = getRx(ip)->plugin()*10 + ic;
	cfg.rxCh[ip*8+ic]  = getRxChFromDto(dto);
	cfg.phases[ip*8+ic] = getRx(ip)->getRxPhase(ic);
	//std::cout << "BocConfigure :"<<" Plugin "<<getRx(ip)->plugin()<<" channel "<<(int)cfg.rxCh[ip*8+ic]<<" DTO "<<dto<< " phase = " << (int)cfg.phases[ip*8+ic] << std::endl;
      }
    }
  }
  cfg.action = SetIBLPhases;
  ctrl->sendCommand(cfg);

}

//Note is different wrt Pixel, DTO representation (OB) is wrong for IBL
uint32_t IblPixBoc::getRxChFromDto(uint32_t dto){

  if (dto<0 || dto >37)return 0;

  uint32_t index=0;

    switch (dto/10){
        case 0: index = 2; break;
        case 1: index = 3; break;
        case 2: index = 0; break;
        case 3: index = 1; break;
    }

  uint32_t rxCh=index*8;
  rxCh += dto/20 ? dto%10 : 7-dto%10;

  return rxCh;

}

//Note is different wrt Pixel, DTO representation (OB) is wrong for IBL
uint32_t IblPixBoc::getDtoFromRxCh(uint32_t rxCh){

  if(rxCh<0||rxCh>31)return 0;

  int plugin=0;
    switch (rxCh/8){
      case 0: plugin = 2; break;
      case 1: plugin = 3; break;
      case 2: plugin = 0; break;
      case 3: plugin = 1; break;
    }

  int dto = plugin*10;
  dto += rxCh/16 ? 7-rxCh%8 : rxCh%8;

return dto;

}
/*
rxCh DTO assignement, note that doesn't match rxCh and DTO from connectivity
rxCh	DTO
0	20
1	21
2	22
3	23
4	24
5	25
6	26
7	27
8	30
9	31
10	32
11	33
12	34
13	35
14	36
15	37
16	7
17	6
18	5
19	4
20	3
21	2
22	1
23	0
24	17
25	16
26	15
27	14
28	13
29	12
30	11
31	10
*/
void IblPixBoc::configInit() {
  PixBoc::configInit();
}

void IblPixBoc::setBocMode(){

}

IblPixBoc::IblTx::IblTx(IblPixBoc &pixboc, std::string name) : PixTx(pixboc, name) {

}


IblPixBoc::IblTx::IblTx(IblPixBoc &pixboc, DbRecord *dbRecord, std::string name) : PixTx(pixboc,dbRecord,name) {

}

IblPixBoc::IblTx::IblTx(IblPixBoc &pixboc, PixDbServerInterface *dbServer, std::string dom, std::string tag, std::string name) : PixTx (pixboc, dbServer, dom, tag, name) {

}

IblPixBoc::IblRx::IblRx(IblPixBoc &pixboc, std::string name) : PixRx (pixboc,name) {
    configInit();
    if (m_name.substr(0,5) == "PixRx") {
    std::istringstream is(m_name.substr(5));
    int i;
    is >> i;
    if (i>=0 && i<4) {
      int RxMap[4] = { 0, 1, 2, 3 };
      m_plugin = RxMap[i];
    }
  }
}

IblPixBoc::IblRx::IblRx(IblPixBoc &pixboc, DbRecord *dbRecord, std::string name) : PixRx (pixboc, dbRecord, name)  {
  configInit();
  if (dbRecord != NULL) m_conf->read(dbRecord);
  if (m_name.substr(0,5) == "PixRx") {
    std::istringstream is(m_name.substr(5));
    int i;
    is >> i;
    if (i>=0 && i<4) {
      int RxMap[4] = { 0, 1, 2, 3 };
      m_plugin = RxMap[i];
    }
  }
}

IblPixBoc::IblRx::IblRx(IblPixBoc &pixboc, PixDbServerInterface *dbServer, std::string dom, std::string tag, std::string name) : PixRx (pixboc, dbServer, dom, tag, name) {
  configInit();
  if(!m_conf->read(dbServer, dom, tag, pixboc.getCtrlName()+"|"+m_name)) 
    std::cout << " Problem in reading configuration" << std::endl;
  if (m_name.substr(0,5) == "PixRx") {
    std::istringstream is(m_name.substr(5));
    int i;
    is >> i;
    if (i>=0 && i<4) {
      int RxMap[4] = { 0, 1, 2, 3 };
      m_plugin = RxMap[i];
    }
  }
}


void IblPixBoc::IblRx::configInit() {
std::cout<<__PRETTY_FUNCTION__<<std::endl;
PixBoc::PixRx::configInit();
  //m_conf = new Config(m_name+"/PixRx"); 
  Config &conf = *m_conf;

try{
 for (int j=0; j<8; j++) {
    string nr = std::to_string(j+2);

    conf["General"].addInt("Phases_"+nr, m_phases[j], 255u,
			   "IBL phases for channel "+nr, true);
  }
}catch(...){
std::cout<<"Exception throw "<<__PRETTY_FUNCTION__<<std::endl;
}

conf.reset();

}


void IblPixBoc::IblRx::setRxConfig(int set) {

}

//------------ Setting the values for the RX board ---------------
//------------------------ DataDelay -----------------------------
void IblPixBoc::IblRx::setRxPhase(int channel, int value){
  m_phases[channel]=value;
  std::cout<<"Saving Phase for channel "<<channel<<" value "<<value<<std::endl;
}

//------------ Getting the values for the RX board ---------------
//------------------------ DataDelay -----------------------------
uint32_t IblPixBoc::IblRx::getRxPhase(int channel){

  if ((channel<0) || (channel>=8)){
    cout << "Channel out of range [0:7]: "<<channel<<endl;
    return 255;
    }
    return m_phases[channel];
}

