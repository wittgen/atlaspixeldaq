/*--------------------------------------------------------------*
 *  IblPixBoc.h                                                 *
 *  Version:  1.0                                               *
 *  Created:  16 June 2016                                      *
 *  Author:   JuanAn                                            *
 *                                                              *
 *  Interface to the IblBoc                                     *
 *--------------------------------------------------------------*/

#ifndef _PIXLIB_IBLPIXBOC
#define _PIXLIB_IBLPIXBOC

#include "PixBoc.h"
#include "PixUtilities/PixMessages.h"

#include <string>
#include <sstream>
#include <vector>
#include <array>

namespace PixLib { 
  
class DbRecord;
class PixModuleGroup;
class Config;
class PixDbServerInterface;

//------------------------- Class PixBoc--------------------------
// The class contains the inteface between the BocCard owned by 
// PixModuleGroup and accessed through RodPixController. 
// The class has two subclasses: PixTx and PixRx. All 
// accessfunctions are part of the class which should be used 
// from the user outside PixLib.

class IblPixBoc : public PixBoc {
public:

  class IblTx : public PixBoc::PixTx{
    friend class IblPixBoc;
  public: 
    //Constructor:
    IblTx(IblPixBoc &pixboc, std::string name);
    IblTx(IblPixBoc &pixboc, DbRecord *dbRecord, std::string name);
    IblTx(IblPixBoc &pixboc, PixDbServerInterface *dbServer, std::string dom, std::string tag, std::string name);
   
    //Destructor
    virtual ~IblTx(){};

    private:
  };

  class IblRx : public PixBoc::PixRx {
    friend class IblPixBoc;
  public: 
    //Constructor
    IblRx(IblPixBoc &pixboc, std::string name);
    IblRx(IblPixBoc &pixboc, DbRecord *dbRecord, std::string name);
    IblRx(IblPixBoc &pixboc, PixDbServerInterface *dbServer, std::string dom, std::string tag, std::string name);
 
    //Destructor
    virtual ~IblRx(){};

    //Function to get config object
    
    virtual Config* getConfigRx(){return m_conf;};

    //Functions to set values to specific RX registers
    virtual void setRxPhase(int channel, int value);

    //Accessors
    virtual uint32_t getRxPhase(int channel);
    virtual void setRxConfig(int set);

  protected:
    
    virtual void configInit();
  };

  //Constructor
  IblPixBoc();
  IblPixBoc(PixModuleGroup &modGrp);
  IblPixBoc(PixModuleGroup &modGrp, DbRecord *dbRecord);
  IblPixBoc(PixModuleGroup &modGrp, PixDbServerInterface *dbServer, std::string dom, std::string tag);
  
  //Destructor
  virtual ~IblPixBoc();

  virtual void configInit();
  virtual void setBocMode();
  virtual void BocConfigure();
  static uint32_t getRxChFromDto(uint32_t dto);
  static uint32_t getDtoFromRxCh(uint32_t rxCh);

  virtual void setRxFibreMapping(int nRx, int rxCh[32], int dto[32], int dto2[32]){ };//Empty not used in IBL


 private:
  virtual void connect();
  bool m_connected;
};

} //end of namespace PixLib

#endif      //_PIXLIB_PIXBOC
 
