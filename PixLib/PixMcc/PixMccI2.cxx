//////////////////////////////////////////////////////////////////////
// PixMccI2.cxx
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 30/04/04  Version 1.0 (CS)
//           Initial release
//

//! MCC-I2 implementation

#include <algorithm>

//#include "serialStreams.h"
#include "Bits/Bits.h"
#include "PixDbInterface/PixDbInterface.h"
#include "PixController/PixController.h"
#include "PixModule/PixModule.h"
#include "PixMcc/PixMccStructures.h"
#include "PixMcc/PixMccData.h"
#include "PixMcc/PixMccI2Config.h"
#include "PixMcc/PixMccI2.h"

using namespace PixLib;

//! Constructor
PixMccI2::PixMccI2(DbRecord *dbRecord, PixModule *mod, std::string name) {
  // Members initialization
  m_dbRecord = dbRecord;
  m_module = mod;
  m_name = name;

  // Create MCC config
  m_data = new PixMccI2Config(m_dbRecord->getDb(), m_dbRecord, m_name);
}

PixMccI2::PixMccI2(PixDbServerInterface *dbServer, PixModule *mod, std::string dom, std::string tag, std::string name) {
  // Members initialization
  m_dbRecord = NULL;
  m_module = mod;
  m_name = name;
  
  // Create MCC config
  m_data = new PixMccI2Config(dbServer, dom, tag, m_name, m_module->moduleName());
}

//! Destructor
PixMccI2::~PixMccI2() {
}


//! Store a configuration into map
void PixMccI2::storeConfig(std::string configName) {  
  // Look for the requested configuration inside config map
  PixMccI2Config *cfg = new PixMccI2Config(*m_data);
  if (m_configs.find(configName) != m_configs.end()) {
    delete m_configs[configName];
  }
  m_configs[configName] = cfg;
}

