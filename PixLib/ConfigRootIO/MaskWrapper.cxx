#include "MaskWrapper.h"
#include <TBuffer.h>
#include <Config/ConfMask.h>
#include <cassert>

templateClassImp(MaskWrapperObject)

  template<class T>
MaskWrapperObject<T>::MaskWrapperObject(MaskWrapperObject &obj) : m_ptr(NULL), m_owner(false) 
{
  if (obj.m_owner ) {
    if (obj.m_ptr) {
      m_ptr=new PixLib::ConfMask<T>(*(obj.m_ptr));
      m_owner=true;
    }
    //    std::cout << "INFO [MaskWrapperObject::copy_ctor] use given object." << std::endl;
  }
  else {
    m_ptr = obj.m_ptr ;
    m_owner = false;
    //    std::cout << "INFO [MaskWrapperObject::copy_ctor] use object of source object." << std::endl;
  }
}

template<class T>
MaskWrapperObject<T>::~MaskWrapperObject() { if (m_owner) { /*std::cout << "INFO [MaskWrapperObject::dtor] delete object." << std::endl; */ delete m_ptr; m_ptr=NULL;}}

template<class T>
void MaskWrapperObject<T>::Streamer(TBuffer &R__b)
{
  // Stream an object of class MaskVectorIntWrapperObject.

  if (R__b.IsReading()) {

    int n_col;
    int n_row;
    unsigned int size;
    R__b >> n_col;
    R__b >> n_row;
    R__b >> size;
    if (size == 0) {
      R__b >> size;
      std::cout << "Mask reading recovery : " 
		<< n_col << " " 
		<< n_row << " " 
		<< size << std::endl;
    }
    unsigned int n_val = n_row * n_col;
    if (n_val>0) {
      unsigned int n_bits = (size-1) * 32 / n_val-1;
      unsigned int max_val = (1<<n_bits);

      assert(this->m_ptr == NULL);
      delete this->m_ptr;
      if (n_row>0 && n_col>0) {
	this->m_ptr = new PixLib::ConfMask<T>(n_col, n_row, static_cast<T>(max_val));
	assert( size == this->m_ptr->getMask().size() );
	R__b.ReadFastArray(&((this->m_ptr->getMask())[0]),this->m_ptr->getMask().size());
      }
    }

  } else {

    if (!m_ptr) {
      int zero=0;
      unsigned int u_zero=0;
      R__b << zero;
      R__b << zero;
      R__b << u_zero;
    }
    else {
      int col = this->m_ptr->ncol();
      int row = this->m_ptr->nrow();
      unsigned int size = this->m_ptr->getMask().size();
      R__b << col;
      R__b << row;
      R__b << size;
      R__b.WriteFastArray(&(((this->m_ptr->getMask()))[0]),this->m_ptr->getMask().size());

    }
  }
}

template <class T> void MaskWrapperObject<T>::Copy(TObject &obj) const {
  if (obj.InheritsFrom(ClassName())) {
    MaskWrapperObject<T> &casted_obj = static_cast< MaskWrapperObject<T> &>(obj);
    if (casted_obj.m_ptr) {
      //      std::cout << "INFO [MaskWrapperObject::Copy] copy from " << static_cast<const void *>(this->m_ptr) << " to " 
      //                << static_cast<void *>(casted_obj.m_ptr) << "." << std::endl;
      //      *(casted_obj.m_ptr) =  *(this->m_ptr);
      assert( casted_obj.m_ptr->ncol() == this->m_ptr->ncol() && casted_obj.m_ptr->nrow() == this->m_ptr->nrow());
      casted_obj.m_ptr->getMask() = this->m_ptr->getMask();
    }
    else {
      std::cout << "ERROR [MaskWrapperObject::Copy] No destination mask." << std::endl;
    }
  }
  else {
    std::cout << "ERROR [MaskWrapperObject::Copy] type mismatch.";
  }
}

template class MaskWrapperObject<unsigned short>;
template class MaskWrapperObject<bool>;
