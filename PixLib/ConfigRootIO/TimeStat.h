#ifndef _TimeStat_h_
#define _TimeStat_h_

#include <sys/time.h>
#include <unistd.h>

#include <iostream>
#include <string>
#include <map>
#include <cmath>

namespace Timer {

class TimePoint
{
 private:
  struct timeval m_time;
 public:

  TimePoint() {
    gettimeofday(&m_time,0);
  }

  double sec() const { return m_time.tv_sec; }
  double usec() const { return m_time.tv_usec; }

};

class TimeStat
{
public:

  TimeStat(const std::string &name);

  void add(const TimePoint &start, const TimePoint &end) {
    double delta_sec =end.sec() - start.sec();
    double delta_usec = (end.usec() - start.usec()) * 1e-6;
    m_elapsed += delta_sec;
    m_uelapsed += delta_usec;
    if (m_uelapsed>1.) {
      m_elapsed += floor(m_uelapsed);
      m_uelapsed -= floor(m_uelapsed);
    }
    m_counter++;
  }

  double totalTime() const { return m_elapsed + m_uelapsed; }
  unsigned int counter() const { return m_counter; }
 private:
  double m_elapsed;
  double m_uelapsed;
  unsigned int m_counter;
};



class TimeStatList {
 public:
  void registerTimeStat(const std::string &name, TimeStat *ptr) {
    if (ptr) {
      m_list[name]=ptr;
    }
  }

  void dump() {
    for (std::map<std::string,TimeStat *>::const_iterator time_stat_iter = m_list.begin();
	 time_stat_iter != m_list.end();
	 time_stat_iter++) {
      std::cout << time_stat_iter->first << " : " << time_stat_iter->second->totalTime() << "(" << time_stat_iter->second->counter() << ")" << std::endl;
    }
  }

  static TimeStatList *instance() {
    if (!s_instance) {
      s_instance = new TimeStatList;
    }
    return s_instance;
  }

 private:
  std::map<std::string, TimeStat *> m_list;
  static TimeStatList *s_instance;
};


inline TimeStat::TimeStat(const std::string &name)  : m_elapsed(0),m_uelapsed(0),m_counter(0) {
  TimeStatList::instance()->registerTimeStat(name,this);
}

class Timer
{
public:
  Timer(TimeStat &stat) :m_stat(&stat) { }

  ~Timer() {
    m_stat->add(m_start,TimePoint());
  }

private:
  TimeStat *m_stat;
  TimePoint m_start;
};

}
#endif
