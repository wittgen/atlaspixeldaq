#ifndef _RootConf_ConfigStore_h_
#define _RootConf_ConfigStore_h_

#include "IConfigStore.h"

#include "RootConfException.h"
#include <list>
#include <map>
#include "BranchList.h"

#include <Rtypes.h>

class TFile;
class TBranch;
class TTree;
class TObject;

namespace PixLib {
  class ConfGroup;
}

namespace RootConf {

  class ObjectStoreDriver;

  class ConfigStore : public IConfigStore
  {
    friend class ObjectStoreDriver;
  public:
    ~ConfigStore();

    Revision_t write(const PixLib::Config &config, const std::string &file_name, const std::string &tag, Revision_t revision = 0);

    void read(PixLib::Config &config, const std::string &file_name, const std::string &tag, Revision_t revision = 0);

    void getRevisions(PixLib::Config &config, const std::string &file_name, const std::string &tag, std::vector<Revision_t> &revision);

  protected:
    TFile *open(const std::string &a_name, const std::string &mode);
    TTree *tree(TFile &a_file, const std::string &tag, const std::string &config_name);
    TTree *createTree(TFile &a_file, const std::string &tag, const std::string &config_name);


    void setupBranches(const PixLib::ConfGroup &conf_group, TTree &a_tree, BranchList *branch_list, bool create);

    void setupBranches(const PixLib::Config &config, TTree &a_tree, BranchList *branch_list, bool create);

    TObject **addObject(TObject *new_object) { m_objectsToDelete.push_back(std::make_pair(new_object,new_object)); return &(m_objectsToDelete.back().second); }
    static std::string sanitiseName(const std::string &name);
    void fillBranchMap(TTree &the_tree);

    Long64_t revisionEntry(TTree &the_tree, TBranch &revision_branch, Revision_t revision, Revision_t &entry_revision);

    std::list< std::pair< TObject *,TObject *> > m_objectsToDelete;
    BranchList m_branchList;
  };



}
#endif
