#include "IConfigStoreDriver.h"
#include "BranchUtil.h"
#include <Config/ConfObj.h>
#include "ConfigStoreDriverList.h"
#include "ConfigStore.h"


namespace RootConf {

  /** Driver for type T provided by a ConfObj of type T_ConfObj_t.
   */

  template <class T_ConfObj_t,class T> inline void _setupBranch(const std::string &branch_name,
								T_ConfObj_t *conf_obj,
								TTree &a_tree,
								BranchList &branch_list,
								bool create)
  {
    branch<TTree,T>(a_tree, branch_list, branch_name, static_cast< T*>(&(conf_obj->value())), create);
  }

  template <class T_ConfObj_t,class T> inline void _setupBranch(const std::string &branch_name,
								T_ConfObj_t *conf_obj,
								TTree &a_tree,
								BranchList &branch_list,
								T limit,
								bool create)
  {
    // works for ConfInt, ConfFloat, ConfBool
    assert(sizeof(conf_obj->value()) >= sizeof(T) );
    branch<TTree,T>(a_tree, branch_list, branch_name, static_cast< T*>( static_cast< void *>(&(conf_obj->value()))), limit, create);
  }


  template <class T_ConfObj_t, class T_value_t>
  class SimpleConfObjStoreDriver : public IConfigStoreDriver /*SimpleConfObjStoreDriverBase<T_ConfObj_t>*/ {

    void setupBranch(ConfigStore &config_store, const std::string &branch_name, PixLib::ConfObj *conf_obj, TTree &a_tree, BranchList &branch_list, bool create) {
      assert( dynamic_cast<T_ConfObj_t *>(conf_obj) );
      T_ConfObj_t *casted_conf_obj = static_cast<T_ConfObj_t *>(conf_obj);
      _setupBranch<T_ConfObj_t, T_value_t>(branch_name, casted_conf_obj, a_tree, branch_list, create);
    }

  private:

  };

  class ConfIntStoreDriver : public IConfigStoreDriver {
  public:

    void setupBranch(ConfigStore &config_store, const std::string &branch_name, PixLib::ConfObj *conf_obj, TTree &a_tree, BranchList &branch_list, bool create) {
      _setupIntBranch(branch_name, conf_obj, a_tree, branch_list, 0, create);
    }

  protected:

    void _setupIntBranch(const std::string &branch_name,
			 PixLib::ConfObj *conf_obj,
			 TTree &a_tree,
			 BranchList &branch_list,
			 unsigned int limit,
			 bool create) {
      assert( dynamic_cast<PixLib::ConfInt *>(conf_obj) );
      PixLib::ConfInt *conf_int = static_cast<PixLib::ConfInt *>(conf_obj);
      switch ( conf_int->subtype() ) {
      case PixLib::ConfInt::S32:
	_setupBranch<PixLib::ConfInt, signed int>(branch_name, conf_int, a_tree, branch_list, static_cast<signed int>(limit), create);
	break;
      case PixLib::ConfInt::U32:
	_setupBranch<PixLib::ConfInt, unsigned int>(branch_name, conf_int, a_tree, branch_list, static_cast<unsigned int>(limit), create);
	break;
      case PixLib::ConfInt::S16:
	_setupBranch<PixLib::ConfInt, signed short>(branch_name, conf_int, a_tree, branch_list, static_cast<signed short>(limit), create);
	break;
      case PixLib::ConfInt::U16:
	_setupBranch<PixLib::ConfInt, unsigned short>(branch_name, conf_int, a_tree, branch_list, static_cast<unsigned short>(limit), create);
	break;
      case PixLib::ConfInt::S8:
	_setupBranch<PixLib::ConfInt, signed char>(branch_name, conf_int, a_tree, branch_list, static_cast<signed char>(limit), create);
	break;
      case PixLib::ConfInt::U8:
	_setupBranch<PixLib::ConfInt, unsigned char>(branch_name, conf_int, a_tree, branch_list, static_cast<unsigned char>(limit), create);
	break;
      default: throw std::runtime_error("Invalid Config Subtype");
      }
    }

  };

  class ConfListStoreDriver : public ConfIntStoreDriver {
  public:

    void setupBranch(ConfigStore &config_store, const std::string &branch_name, PixLib::ConfObj *conf_obj, TTree &a_tree, BranchList &branch_list, bool create) {
      assert( dynamic_cast<PixLib::ConfList *>(conf_obj) );
      PixLib::ConfList *conf_list = static_cast<PixLib::ConfList *>(conf_obj);

      _setupIntBranch(branch_name, conf_obj, a_tree, branch_list, conf_list->m_symbols.size(), create);
    }

  };


  bool registerSimpleConfObjDriver() {
    ConfigStoreDriverList::instance()->registerDriver(PixLib::ConfObj::INT,   new ConfIntStoreDriver);
    ConfigStoreDriverList::instance()->registerDriver(PixLib::ConfObj::LIST,  new ConfListStoreDriver);
    ConfigStoreDriverList::instance()->registerDriver(PixLib::ConfObj::FLOAT, new SimpleConfObjStoreDriver<PixLib::ConfFloat, float> );
    ConfigStoreDriverList::instance()->registerDriver(PixLib::ConfObj::BOOL,  new SimpleConfObjStoreDriver<PixLib::ConfBool, bool> );
    return true;
  }

  bool s_SimpleConfObjStoreDriver = registerSimpleConfObjDriver();

}
