#include "TSTLWrapper.h"
#include <TBuffer.h>

templateClassImp(STLWrapperObject)

template<class T>
void STLWrapperObject<T>::Streamer(TBuffer &R__b)
{
  // Stream an object of class STLVectorIntWrapperObject.

  if (R__b.IsReading()) {

    unsigned int size;
    R__b >>size;
    if (!this->m_ptr) {
      throw std::logic_error("NULL pointer given for the vector into which the data was supposed to be read");
    }
    else {
      if (size>0) {
	this->m_ptr->resize(size);
	R__b.ReadFastArray(&((*this->m_ptr)[0]),this->m_ptr->size());
      }
      else {
	this->m_ptr->clear();
      }
    }

  } else {

    unsigned int size = ( this->m_ptr ? this->m_ptr->size() : 0);
    //      std::cout << "INFO [STLWrapperObject::Streamer] write size = " << size << std::endl;
    R__b << size;
    if (this->m_ptr) {
      R__b.WriteFastArray(&((*(this->m_ptr))[0]),this->m_ptr->size());
    }

  }
}

template<>
void STLWrapperObject<std::vector<std::string> >::Streamer(TBuffer &R__b)
{
  // Stream an object of class STLVectorIntWrapperObject.

  if (R__b.IsReading()) {

    unsigned int n_strings;
    R__b >>n_strings;
    if (!this->m_ptr) {
      throw std::logic_error("NULL pointer given for the vector into which the data was supposed to be read");
    }
    else {
      if (n_strings>0) {
	this->m_ptr->resize(n_strings);

	for (unsigned int str_i=0; str_i<n_strings; str_i++) {
	  unsigned int n_chars;
	  R__b >>n_chars;
	  if (n_chars>0) {
	    (*(this->m_ptr))[str_i].resize(n_chars);
	    R__b.ReadFastArray(&((*(this->m_ptr))[str_i][0]),(*(this->m_ptr))[str_i].size());
	  }
	  else {
	    (*(this->m_ptr))[str_i].clear();
	  }
	}
      }
      else {
	this->m_ptr->clear();
      }
    }

  } else {

    unsigned int n_strings = ( this->m_ptr ? this->m_ptr->size() : 0);
    //      std::cout << "INFO [STLWrapperObject::Streamer] write size = " << size << std::endl;
    R__b << n_strings;
    if (this->m_ptr) {
      for (unsigned int str_i=0; str_i<n_strings; str_i++) {
	unsigned int n_chars = (*(this->m_ptr))[str_i].size();
	R__b << n_chars;
	R__b.WriteFastArray(&((*(this->m_ptr))[str_i][0]),(*(this->m_ptr))[str_i].size());
      }
    }

  }
}

template <class T> void STLWrapperObject<T>::Copy(TObject &obj) const {
  if (obj.InheritsFrom(ClassName())) {
    //    std::cout << "INFO [STLWrapperObject::Copy] copy from " << static_cast<const void *>(this) << " to " << static_cast<void *>(&obj) << "." << std::endl;
    STLWrapperObject<T> &casted_obj = static_cast< STLWrapperObject<T> &>(obj);
    *(casted_obj.m_ptr) =  *(m_ptr);
  }
  else {
    std::cerr << "ERROR [STLWrapperObject::Copy] type mismatch.";
  }
}

template class STLWrapperObject<std::string>;
template class STLWrapperObject<std::vector<int> >;
template class STLWrapperObject<std::vector<unsigned int> >;
template class STLWrapperObject<std::vector<float> >;
template class STLWrapperObject<std::vector<std::string> >;
