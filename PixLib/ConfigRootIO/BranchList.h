#ifndef _RootConf_BranchList_h_
#define _RootConf_BranchList_h_

#include <string>
#include <map>

class TBranch;

namespace RootConf {

  class BranchList
  {
  public:
    BranchList() {}
    ~BranchList() {}

    TBranch *branch(const std::string &branch_name) {
      std::map<std::string, TBranch *>::iterator  branch_iter = m_branches.find(branch_name);
      if (branch_iter != m_branches.end()) {
	return branch_iter->second;
      }
      return NULL;
    }

    void addBranch(const std::string &branch_name, TBranch *a_branch) {
      m_branches[branch_name] = a_branch;
    }

    std::map<std::string, TBranch *> &branchMap() { return m_branches;}
    const std::map<std::string, TBranch *> &branchMap() const { return m_branches;}

    BranchList *subBranches(const std::string &sub_list_name) {
      std::map<std::string, BranchList>::iterator  sub_list_iter = m_subList.find(sub_list_name);
      if (sub_list_iter != m_subList.end()) {
	return &(sub_list_iter->second);
      }
      return NULL;
    }

    BranchList *addSubBranches(const std::string &sub_list_name) {
      BranchList *a_branch_list = &m_subList[sub_list_name];
      if (a_branch_list && a_branch_list->fullName().empty()) {
	a_branch_list->setFullName( m_fullName, sub_list_name );
      }
      return a_branch_list;
    }

    const std::string &fullName() const { return m_fullName; }
    void setFullName(const std::string &parent_name, const std::string &sub_list_name)  { 
//      if (!parent_name.empty()) {
      m_fullName += parent_name;
	//	m_fullName += '.';
      //      }
      m_fullName += sub_list_name;
      m_fullName += '.';
    }
    std::string makeFullBranchName(const std::string &branch_name) const {
      return fullName() + branch_name;
    }

  private:
    std::string m_fullName;
    std::map<std::string,TBranch *>  m_branches;
    std::map<std::string,BranchList> m_subList;
  };

}
#endif
