#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link off all typedefs;

// #pragma link C++ class PixLib::ConfMask<bool>;
// #pragma link C++ class PixLib::ConfMask<unsigned short>;

#pragma link C++ class STLWrapperObject<std::string>-;
#pragma link C++ class STLWrapperObject<std::vector<float> >-;
#pragma link C++ class STLWrapperObject<std::vector<unsigned int> >-;
#pragma link C++ class STLWrapperObject<std::vector<int> >-;
#pragma link C++ class STLWrapperObject<std::vector<std::string> >-;

#endif
