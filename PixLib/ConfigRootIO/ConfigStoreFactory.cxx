#include "ConfigStore.h"
#include "ConfigStoreFactory.h"

namespace RootConf {

  IConfigStore *ConfigStoreFactory::configStore() {
    return new ConfigStore;
  }
}
