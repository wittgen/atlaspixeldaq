#include "ObjectStoreDriver.h"
#include "BranchUtil.h"
#include <Config/ConfObj.h>
#include "ConfigStoreDriverList.h"
#include "MaskWrapper.h"
#include "ConfigStore.h"

namespace RootConf {


  class ConfMatrixStoreDriver : public ObjectStoreDriver {
  public:

    void setupBranch(ConfigStore &config_store, const std::string &object_name, PixLib::ConfObj *conf_obj, TTree &a_tree,  BranchList &branch_list, bool create) {
      assert( dynamic_cast<PixLib::ConfMatrix *>(conf_obj) );
      PixLib::ConfMatrix *conf_matrix = static_cast<PixLib::ConfMatrix *>(conf_obj);

      switch ( conf_matrix->subtype() ) {
      case PixLib::ConfMatrix::M_U1: {
	_setupBranch(new MaskU1WrapperObject(&(conf_matrix->valueU1())), config_store, object_name, a_tree, branch_list, create);
	break;
      }
      case PixLib::ConfMatrix::M_U16: {
	_setupBranch(new MaskU16WrapperObject(&(conf_matrix->valueU16())), config_store, object_name, a_tree, branch_list, create);
	break;
      }
      default: throw std::runtime_error("Invalid Config Subtype");
      }

    }

  };


  bool registerConfMatrixDriver() {
      ConfigStoreDriverList::instance()->registerDriver(PixLib::ConfObj::MATRIX,   new ConfMatrixStoreDriver);
      return true;
  }


  bool s_ConfMatrixStoreDriver = registerConfMatrixDriver();

}
