#ifndef _RootConf_IConfigStoreDriver_h_
#define _RootConf_IConfigStoreDriver_h_

#include <string>

class TTree;

namespace PixLib {
  class ConfObj;
}

namespace RootConf {

  class ConfigStore;
  class BranchList;

  class IConfigStoreDriver {
  public:
    virtual ~IConfigStoreDriver() {}

    /** Get an existing branch or create it  and set it up for the given ConfObj.
     * @param config_store pointer to the config store for which the branches are setup
     * @param branch_name the name of the branch
     * @param conf_obj pointer to an existing ConfObj
     * @param a_tree the tree to which the branch will be added
     * @param branch_list the branches organised in a tree like the config objects (for internal use).
     * @param create set to false of missing branches should not be created (e.g. for reading).
     */
    virtual void setupBranch(ConfigStore &config_store, const std::string &branch_name, PixLib::ConfObj *conf_obj, TTree &a_tree, BranchList &branch_list, bool create) = 0;

  };

}

#endif
