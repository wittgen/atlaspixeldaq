#ifndef _RootConf_ConfigStoreFactory_h_
#define _RootConf_ConfigStoreFactory_h_

#include "IConfigStore.h"

namespace RootConf {

  class ConfigStoreFactory
  {
  public:
    //    ConfigStoreFactory() {}
    //    ~ConfigStoreFactory() {}
    //  protected:

    /** Create a config store.
     * e.g. :
     * <verb>
     * std::auto_ptr<RootConf::IConfigStore> store( RootConf::ConfigStoreFactory::configStore());
     * </verb>
     */
    static IConfigStore *configStore();
  private:

  };

}
#endif
