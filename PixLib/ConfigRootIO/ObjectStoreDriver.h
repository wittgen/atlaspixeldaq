#ifndef _RootConf_ObjectStoreDriver_h_
#define _RootConf_ObjectStoreDriver_h_

#include <IConfigStoreDriver.h>
#include <map>

class TObject;
class TBranch;

namespace RootConf {

  class ObjectStoreDriver : public IConfigStoreDriver {
  public:

  protected:
    /** Get an existing branch or create it  and set it up for the given ConfObj.
     * @param the_obj pointer to a root object which should be stored in the branch
     * @param config_store the config_store
     * @param branch_name  the name of the branch
     * @param a_tree the tree to which the branch will be added
     * @param branch_list the branches organised in a tree like the config objects.
     * @param create set to false of missing branches should not be created (e.g. for reading).
     */
    void _setupBranch(TObject *the_obj,
		      ConfigStore &config_store,
		      const std::string &branch_name,
		      TTree &a_tree,
		      BranchList &branch_list,
		      bool create);
  };

}

#endif
