#include "ObjectStoreDriver.h"
#include "BranchUtil.h"
#include "ConfigStore.h"

namespace RootConf {

  void ObjectStoreDriver::_setupBranch(TObject *the_obj,
				       ConfigStore &config_store,
				       const std::string &branch_name,
				       TTree &a_tree,
				       BranchList &branch_list,
				       bool create) {
    assert(the_obj);


    // hand over the ownership of the object to the config store
    // which will delete the object upon destruction of the config store.
    TObject **ptr_to_object = config_store.addObject(the_obj);
    branch(a_tree, branch_list, branch_name, the_obj->ClassName(), ptr_to_object, create);

  }
}
