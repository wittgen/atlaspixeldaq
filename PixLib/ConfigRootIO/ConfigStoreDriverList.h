#ifndef _RootConf_ConfigStoreDriverList_h_
#define _RootConf_ConfigStoreDriverList_h_

#include <map>
#include <Config/ConfObj.h>

namespace RootConf {
  
  class IConfigStoreDriver;

  class ConfigStoreDriverList {
  private:
    ConfigStoreDriverList();

  public:
    ~ConfigStoreDriverList();

    /** Get a driver to setup branches for reading back or writing ConfObj objects of the given type  from/to a ROOT tree.
     */
    IConfigStoreDriver *driver(PixLib::ConfObj::types type_id);

    /** Register a driver which can setup branches for reading back or writing the  ConfObj of the given type from/to a ROOT tree.
     */
    void registerDriver(PixLib::ConfObj::types type_id, IConfigStoreDriver *the_driver);

    /** Get the only instance of the driver list.
     */
    static ConfigStoreDriverList *instance() {
      if (!s_instance) {
	s_instance = new ConfigStoreDriverList;
      }
      return s_instance;
    }

  private:
    std::map<PixLib::ConfObj::types, IConfigStoreDriver *> m_driver;

    static ConfigStoreDriverList *s_instance;
  };

}

#endif
