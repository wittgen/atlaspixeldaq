#include "ConfigStoreDriverList.h"
#include "IConfigStoreDriver.h"

namespace RootConf {
  ConfigStoreDriverList *ConfigStoreDriverList::s_instance=NULL;

  ConfigStoreDriverList::ConfigStoreDriverList() {}

  ConfigStoreDriverList::~ConfigStoreDriverList() {
    for (std::map<PixLib::ConfObj::types, IConfigStoreDriver *>::iterator driver_iter = m_driver.begin();
	 driver_iter != m_driver.end();
	 ++driver_iter) {
      delete driver_iter->second;
      driver_iter->second = NULL;
    }
  }

  IConfigStoreDriver *ConfigStoreDriverList::driver(PixLib::ConfObj::types type_id) {
    std::map<PixLib::ConfObj::types, IConfigStoreDriver *>::iterator driver_iter = m_driver.find(type_id);
    if (driver_iter != m_driver.end() ){
      return driver_iter->second;
    }
    return NULL;
  }

  void ConfigStoreDriverList::registerDriver(PixLib::ConfObj::types type_id, IConfigStoreDriver *the_driver) {

    std::pair< std::map<PixLib::ConfObj::types, IConfigStoreDriver *>::iterator, bool>
      ret  = m_driver.insert(std::make_pair( type_id, the_driver ));
    if (!ret.second) {
      std::cout << "ERROR [ConfigStoreDriverList::registerDriver] Failed to register driver for type " << type_id << "." << std::endl;
    }

  }
}
