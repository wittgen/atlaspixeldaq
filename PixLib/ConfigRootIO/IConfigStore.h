#ifndef _RootConf_IConfigStore_h_
#define _RootConf_IConfigStore_h_

#include <string>
#include <vector>

namespace PixLib {
  class Config;
}

namespace RootConf {

  typedef unsigned int Revision_t;

  class IConfigStore
  {

  public:
    virtual ~IConfigStore() {}

    virtual Revision_t write(const PixLib::Config &config, const std::string &file_name, const std::string &tag, Revision_t revision = 0) = 0;

    virtual void read(PixLib::Config &config, const std::string &file_name, const std::string &tag, Revision_t revision = 0) = 0;

    virtual void getRevisions(PixLib::Config &config, const std::string &file_name, const std::string &tag, std::vector<Revision_t> &revision) = 0;

  };



}
#endif
