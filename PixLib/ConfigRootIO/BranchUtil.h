#ifndef _RootConf_BranchUtil_h_
#define _RootConf_BranchUtil_h_

#include "RootConfException.h"
#include <TTree.h>
#include <TBranch.h>
#include <TBranchObject.h>
#include <TLeaf.h>
#include <TLeafB.h>
#include <TLeafS.h>
#include <TLeafI.h>
#include <TObjArray.h>

#include "BranchList.h"

#include <cassert>
#include <string>
#include <sstream>

namespace RootConf {

  // ROOT leaf types
  //             - C : a character string terminated by the 0 character
  //             - B : an 8 bit signed integer (Char_t)
  //             - b : an 8 bit unsigned integer (UChar_t)
  //             - S : a 16 bit signed integer (Short_t)
  //             - s : a 16 bit unsigned integer (UShort_t)
  //             - I : a 32 bit signed integer (Int_t)
  //             - i : a 32 bit unsigned integer (UInt_t)
  //             - F : a 32 bit floating point (Float_t)
  //             - D : a 64 bit floating point (Double_t)
  //             - L : a 64 bit signed integer (Long64_t)
  //             - l : a 64 bit unsigned integer (ULong64_t)


  class RootLeafTypes {
  public:
    enum ETypeId { kBool, kS8, kU8, kS16, kU16, kS32, kU32, kFloat,kNTypes};

    static const char *typeName(ETypeId type_id) { assert(type_id<kNTypes); return s_typeNames[type_id]; }
    static const char *typeSpecifier(ETypeId type_id) { assert(type_id<kNTypes); return s_typeSpecifier[type_id]; }
    static const Int_t maxSize(ETypeId type_id) { assert(type_id<kNTypes); return s_maxSize[type_id]; }

  private:
    static const char *s_typeNames[kNTypes];
    static const char *s_typeSpecifier[kNTypes];
    static const Int_t s_maxSize[kNTypes];
  };


  class TLeafBool : public TLeafB
  {
  public:
    void adjust() { fMinimum=0; fMaximum=1;SetRange(kTRUE);}
  };

  template <class T_Leaf_t, class T>
  class TLeafList : public T_Leaf_t
  {
  public:
    void adjust(T min_val, T max_val) { this->fMinimum=min_val; this->fMaximum=max_val;this->SetRange(kTRUE);}
  };

  template <class T> RootLeafTypes::ETypeId typeId();

  // this only works if a bool is stored like a char
  template<> inline RootLeafTypes::ETypeId typeId<bool>() { assert(sizeof(bool) == sizeof(Char_t)); return RootLeafTypes::kS8; }

  template<> inline RootLeafTypes::ETypeId typeId<signed char>() { return RootLeafTypes::kS8; }
  template<> inline RootLeafTypes::ETypeId typeId<unsigned char>() { return RootLeafTypes::kU8; }

  template<> inline RootLeafTypes::ETypeId typeId<signed short>() { return RootLeafTypes::kS16; }
  template<> inline RootLeafTypes::ETypeId typeId<unsigned short>() { return RootLeafTypes::kU16; }

  template<> inline RootLeafTypes::ETypeId typeId<signed int>() { return RootLeafTypes::kS32; }
  template<> inline RootLeafTypes::ETypeId typeId<unsigned int>() { return RootLeafTypes::kU32; }

  template<> inline RootLeafTypes::ETypeId typeId<float>() { return RootLeafTypes::kFloat; }


  template <class _T_Tree> inline TBranch *GetBranch(_T_Tree &the_tree,
						     BranchList &branch_list,
						     const std::string &branch_name ) {
    // // to speed up the branch lookup an external branch map is used.
    TBranch *a_branch = branch_list.branch(branch_name);

    return a_branch;
  }

  template <class _T_Tree> TBranch *branch(_T_Tree &the_tree,
					   BranchList &branch_list,
					   const std::string &branch_name,
					   RootLeafTypes::ETypeId type_id)
  {
    assert( type_id < RootLeafTypes::kNTypes );
    TBranch *the_branch = GetBranch(the_tree, branch_list, branch_name);
    if (the_branch) {
      TObjArray *leaves = the_branch->GetListOfLeaves();
      if (leaves && leaves->GetEntries() == 1 ) {
	TLeaf *a_leaf = static_cast<TLeaf *>(the_branch->GetListOfLeaves()->At(0));
	if (strcmp(a_leaf->GetTypeName(), RootLeafTypes::typeName(type_id) )==0) {
	  if (RootLeafTypes::maxSize(type_id)==0 || (a_leaf->IsRange() && RootLeafTypes::maxSize(type_id)==a_leaf->GetMaximum() )) {
	    return the_branch;
	  }
        }
      }

      std::stringstream message;
      message << "A branch exists for " << branch_name << " but it does not match the expected type : "
	      << RootLeafTypes::typeName(type_id);
      if ( RootLeafTypes::maxSize(type_id)>0) {
	message << "(range 0-" << RootLeafTypes::maxSize(type_id) << ").";
      }
      throw BranchHasWrongType(message.str());
    }
    return NULL;
  }

  template <class _T_Tree> TBranch *activateBranch(_T_Tree &the_tee, TBranch *the_branch, void *ptr_to_variable)
  {
    if (!the_branch) return NULL;

    the_branch->ResetBit(kDoNotProcess); // switch the branch on (has to be done before setting the address. Otherwise, the latter will not work)
    the_branch->SetAddress(ptr_to_variable);
    TTree *a_tree = the_branch->GetTree();
    if (!a_tree) {
      std::stringstream message;
      message << "No tree assigned to branch " << the_branch->GetName() << " this should never happen. Internal error.";
      throw BranchDoesNotExist(message.str());
    }

    return the_branch;
  }

  template <class _T_Tree> inline TBranch *addBranch(_T_Tree &the_tree,
						     BranchList &branch_list,
						     const std::string &branch_name,
						     TBranch &the_branch)
  {
    branch_list.branchMap().insert(std::make_pair(branch_name,&the_branch));
    the_tree.GetListOfBranches()->Add(&the_branch);
    return &the_branch;
  }

  template <class _T_Tree> TBranch *createBranch(_T_Tree &the_tree,
						 BranchList &branch_list,
						 const std::string &branch_name,
						 RootLeafTypes::ETypeId type_id,
						 void *ptr_to_variable) {

    assert( type_id < RootLeafTypes::kNTypes );

    std::string branch_type_specifier = branch_name;
    branch_type_specifier += "/";
    branch_type_specifier += RootLeafTypes::typeSpecifier(type_id);
#if ROOT_VERSION_CODE >= ROOT_VERSION(5,18,0)
    TBranch *the_branch = new TBranch( &the_tree,  branch_list.makeFullBranchName(branch_name).c_str() , ptr_to_variable, branch_type_specifier.c_str(),4096);
#else
    TBranch *the_branch = new TBranch( branch_list.makeFullBranchName(branch_name).c_str() , ptr_to_variable, branch_type_specifier.c_str(),4096);
#endif
    addBranch(the_tree, branch_list, branch_name, *the_branch);
    return the_branch;
  }

  template <class T> void adjust(TLeaf* /*leaf*/ ) {}

  template <> inline void adjust<bool>(TLeaf *leaf) {
    TLeafBool *the_leaf = static_cast<  TLeafBool *>( leaf );
    the_leaf->adjust();
  }

  template <class T> void adjust(TLeaf *leaf, T min_val, T max_val);

  template <> inline void adjust<signed char>(TLeaf *leaf, signed char min_val, signed char max_val) {
    TLeafList<TLeafB,char> *the_leaf = static_cast<  TLeafList<TLeafB,char> *>( leaf );
    the_leaf->adjust(min_val, max_val);
  }

  template <> inline void adjust<unsigned char>(TLeaf *leaf, unsigned char min_val, unsigned char max_val) {
    TLeafList<TLeafB, char> *the_leaf = static_cast<  TLeafList<TLeafB, char> *>( leaf );
    the_leaf->adjust( static_cast<char>(min_val), static_cast<char>(max_val));
  }

  template <> inline void adjust<short>(TLeaf *leaf, short min_val, short max_val) {
    TLeafList<TLeafS,short> *the_leaf = static_cast<  TLeafList<TLeafS,short> *>( leaf );
    the_leaf->adjust(min_val, max_val);
  }

  template <> inline void adjust<unsigned short>(TLeaf *leaf, unsigned short min_val, unsigned short max_val) {
    TLeafList<TLeafS, short> *the_leaf = static_cast<  TLeafList<TLeafS, short> *>( leaf );
    the_leaf->adjust( static_cast<short>(min_val), static_cast<short>(max_val));
  }

  template <> inline void adjust<int>(TLeaf *leaf, int min_val, int max_val) {
    TLeafList<TLeafI,int> *the_leaf = static_cast<  TLeafList<TLeafI,int> *>( leaf );
    the_leaf->adjust(min_val, max_val);
  }

  template <> inline void adjust<unsigned int>(TLeaf *leaf, unsigned int min_val, unsigned int max_val) {
    TLeafList<TLeafI, int> *the_leaf = static_cast<  TLeafList<TLeafI, int> *>( leaf );
    the_leaf->adjust( static_cast<int>(min_val), static_cast<int>(max_val));
  }

  // In case a branch was not filled permanently the branch needs to be filled up with dummy entries
  // otherwise new entries will be assigned to wrong existing entries.
  void padBranchWithDummyEntries(TBranch &the_branch, TTree &the_tree, bool create);


  // Get a branch for simple types : int, float, bool
  // The config may not necessarily contain data for a certain variable.
  // This can happen if an old configuration is access which does not yet
  // contain the data for new elements.
  template <class _T_Tree, class T> TBranch *branch(_T_Tree &the_tree,
						    BranchList &branch_list,
						    const std::string &branch_name,
						    T *ptr_to_variable,
						    bool create) {

    TBranch *the_branch1 = branch(the_tree, branch_list, branch_name, typeId<T>());
    if (the_branch1) {

      activateBranch( the_tree, the_branch1, ptr_to_variable);
      padBranchWithDummyEntries(*the_branch1, the_tree,create);
      return the_branch1;

    }
    else {
      if (create) {
	TBranch *the_branch2 = createBranch(the_tree, branch_list, branch_name, typeId<T>(),ptr_to_variable);
	if (the_branch2) {
	  // has been checked already, but to be really, really sure once again:
	  assert(the_branch2->GetListOfLeaves()->GetEntries()==1);
	  assert(the_branch2->GetListOfLeaves()->At(0) && the_branch2->GetListOfLeaves()->At(0)->InheritsFrom(TLeaf::Class()) );
	  TLeaf *the_leaf = static_cast<TLeaf *>(the_branch2->GetListOfLeaves()->At(0));
	  adjust<T>(the_leaf);

	  padBranchWithDummyEntries(*the_branch2, the_tree, create);

	  return the_branch2;
	}
      }
      return NULL;
    }
  }


  // for lists the maximum value should be limit
  template <class _T_Tree, class T> TBranch *branch(_T_Tree &the_tree,
						    BranchList &branch_list,
						    const std::string &branch_name,
						    T *ptr_to_variable,
						    T limit,
						    bool create) {
    TBranch *the_branch1 = branch(the_tree, branch_list, branch_name, typeId<T>());
    if (the_branch1) {

      activateBranch( the_tree, the_branch1, ptr_to_variable);
      padBranchWithDummyEntries(*the_branch1, the_tree, create);
      return the_branch1;

    }
    else {
      if (create) {
	TBranch *the_branch2 = createBranch(the_tree, branch_list, branch_name, typeId<T>(), ptr_to_variable );
	if (the_branch2) {
	  // has been checked already, but to be really, really sure once again:
	  assert(the_branch2->GetListOfLeaves()->GetEntries()==1);
	  assert(the_branch2->GetListOfLeaves()->At(0) && the_branch2->GetListOfLeaves()->At(0)->InheritsFrom(TLeaf::Class()) );
	  TLeaf *the_leaf = static_cast<TLeaf *>( the_branch2->GetListOfLeaves()->At(0) );
	  if (limit>0) {
	    adjust<T>(the_leaf, 0,limit);
	  }

	  padBranchWithDummyEntries(*the_branch2, the_tree,create);

	  return the_branch2;
	}
      }
      return NULL;
    }
  }

  // for lists the maximum value should be limit
  TBranch *branch(TTree &the_tree,
		  BranchList &branch_list,
		  const std::string &branch_name,
		  const std::string &class_name,
		  TObject **ptr_to_variable,
		  bool create);


}
#endif
