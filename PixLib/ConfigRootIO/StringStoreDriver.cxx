#include "ObjectStoreDriver.h"
#include "BranchUtil.h"
#include <Config/ConfObj.h>
#include "ConfigStoreDriverList.h"
#include "TSTLWrapper.h"
#include "ConfigStore.h"

namespace RootConf {


  class ConfStringStoreDriver : public ObjectStoreDriver {
  public:

    void setupBranch(ConfigStore &config_store, const std::string &branch_name, PixLib::ConfObj *conf_obj, TTree &a_tree, BranchList &branch_list, bool create) {
      assert( dynamic_cast<PixLib::ConfString *>(conf_obj)  );
      PixLib::ConfString *conf_string = static_cast<PixLib::ConfString *>(conf_obj);
      _setupBranch(new STLStringWrapperObject(&(conf_string->value())), config_store, branch_name, a_tree, branch_list, create);
    }

  };

  class ConfAliasStoreDriver : public ObjectStoreDriver {
  public:

    void setupBranch(ConfigStore &config_store, const std::string &branch_name, PixLib::ConfObj *conf_obj, TTree &a_tree, BranchList &branch_list, bool create) {
      assert( dynamic_cast<PixLib::ConfAlias *>(conf_obj)  );
      PixLib::ConfAlias *conf_string = static_cast<PixLib::ConfAlias *>(conf_obj);
      _setupBranch(new STLStringWrapperObject(&(conf_string->value())), config_store, branch_name, a_tree, branch_list, create);
    }

  };

  class ConfLinkStoreDriver : public ObjectStoreDriver {
  public:

    void setupBranch(ConfigStore &config_store, const std::string &branch_name, PixLib::ConfObj *conf_obj, TTree &a_tree, BranchList &branch_list, bool create) {
      assert( dynamic_cast<PixLib::ConfLink *>(conf_obj)  );
      PixLib::ConfLink *conf_string = static_cast<PixLib::ConfLink *>(conf_obj);
      _setupBranch(new STLStringWrapperObject(&(conf_string->value())), config_store, branch_name, a_tree, branch_list, create);
    }

  };


  bool registerConfStringDriver() {
      ConfigStoreDriverList::instance()->registerDriver(PixLib::ConfObj::STRING,  new ConfStringStoreDriver);
      ConfigStoreDriverList::instance()->registerDriver(PixLib::ConfObj::LINK,    new ConfLinkStoreDriver);
      ConfigStoreDriverList::instance()->registerDriver(PixLib::ConfObj::ALIAS,   new ConfAliasStoreDriver);
      return true;
  }


  bool s_ConfStringStoreDriver = registerConfStringDriver();

}
