#include "BranchUtil.h"
namespace RootConf {

  const char *RootLeafTypes::s_typeNames[kNTypes]={
    "Char_t",
    "Char_t",
    "UChar_t",
    "Short_t",
    "UShort_t",
    "Int_t",
    "UInt_t",
    "Float_t"
  };

  const char *RootLeafTypes::s_typeSpecifier[kNTypes]={
    "B",
    "B",
    "b",
    "S",
    "s",
    "I",
    "i",
    "F"
  };

  const Int_t RootLeafTypes::s_maxSize[kNTypes]={
    1,
    0,
    0,

    0,
    0,
    0,
    0,
    0
  };

  void padBranchWithDummyEntries(TBranch &the_branch, TTree &the_tree, bool create)
  {
    if (the_branch.GetEntries() != the_tree.GetEntriesFast()) {
      std::stringstream message;
      message << "The numnber of entries in the branch " << the_branch.GetName() << " differ from the entries in the tree :  "
	      << the_branch.GetEntries() << " !=  " << the_tree.GetEntriesFast() << ".";
      if (create) {
	std::cout << "WARNING [padBranchWithDummyEntries] " << message.str() << std::endl;
      }
      else {
	throw InconsistentTree(message.str());
      }
    }

    if (create) {
      // need to write for all already stored entries a dummy entry for this new branch
      the_branch.ResetBit(kDoNotProcess); // switch the branch on
      for (unsigned int old_entry_i=the_branch.GetEntries(); old_entry_i < the_tree.GetEntriesFast(); old_entry_i++) {
	the_branch.Fill();
      }
      std::stringstream message;
    }
  }


  TBranch *branch(TTree &the_tree,
		  BranchList &branch_list,
		  const std::string &branch_name,
		  const std::string &class_name,
		  TObject **ptr_to_variable,
		  bool create)
  {
    TBranch *the_branch1 = GetBranch(the_tree, branch_list,branch_name);
    if (the_branch1) {
      if (the_branch1->InheritsFrom(TBranchObject::Class())) {
	TObjArray *leaves = the_branch1->GetListOfLeaves();
	if (leaves && leaves->GetEntries() == 1 ) {
	  TLeaf *a_leaf = static_cast<TLeaf *>(the_branch1->GetListOfLeaves()->At(0));
	  if (*ptr_to_variable) {
	    const std::string class_name_str = ( (*ptr_to_variable)->ClassName() );
	    if (strcmp(a_leaf->GetTypeName(), class_name_str.c_str() )!=0) {
	      std::stringstream message;
	      message << "A branch exists for " << branch_name << " but its leaf is not of type " << class_name_str << ". It is of the type "
		      << a_leaf->GetTypeName() << ".";
	      throw BranchHasWrongType(message.str());
	    }
	  }
	  else {
	    std::cout << "INFO [branch] No object given yet cannot verify found = " << branch_name << " / " << a_leaf->GetTypeName() << std::endl;
	  }
	}
	else {
	    std::stringstream message;
	    message << "A branch exists for " << branch_name << " but it has more or less than one leaf.";
	    throw BranchHasWrongType(message.str());
	}
      }
      else {
	std::stringstream message;
	message << "A branch exists for " << branch_name << " but it is not of type TBranchObject.";
	throw BranchHasWrongType(message.str());
      }
    }
    if (the_branch1) {
      activateBranch( the_tree, the_branch1, ptr_to_variable);
      padBranchWithDummyEntries(*the_branch1, the_tree,create);
      return the_branch1;
    }
    else {
      if (create) {
#if ROOT_VERSION_CODE >= ROOT_VERSION(5,18,0)
	TBranch *the_branch2 = new TBranchObject( &the_tree, branch_list.makeFullBranchName(branch_name).c_str() , class_name.c_str() , ptr_to_variable, 4096 );
#else  
	TBranch *the_branch2 = new TBranchObject( branch_list.makeFullBranchName(branch_name).c_str() , class_name.c_str() , ptr_to_variable, 4096 );
#endif
	addBranch<TTree>(the_tree, branch_list, branch_name, *the_branch2);

	padBranchWithDummyEntries(*the_branch2, the_tree, create);

	// has been checked already, but to be really, really sure once again:
	assert(the_branch2->GetListOfLeaves()->GetEntries()==1);
	//	assert(the_branch->GetListOfLeaves()->At(0) && the_branch->GetListOfLeaves()->At(0)->InheritsFrom(TLeaf::Class()) );
	//	TLeaf *the_leaf = static_cast<TLeaf *>( the_branch->GetListOfLeaves()->At(0) );
	return the_branch2;
      }
      return NULL;
    }
  }


}
