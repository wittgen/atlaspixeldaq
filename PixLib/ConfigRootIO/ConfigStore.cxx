
#include <sstream>

#include <TFile.h>
#include <TKey.h>
#include <TTree.h>

#include "BranchUtil.h"
#include "IConfigStoreDriver.h"
#include "ConfigStoreDriverList.h"

#include "ConfigStore.h"
#include <Config/Config.h>

#include <TROOT.h>

namespace RootConf {

  ConfigStore::~ConfigStore() {
    for(std::list<std::pair<TObject *, TObject *> >::iterator  object_iter = m_objectsToDelete.begin();
	object_iter != m_objectsToDelete.end();
	object_iter++) {
      if (object_iter->first != object_iter->second) {
	delete object_iter->first;
      }
      object_iter->first = NULL;
      delete object_iter->second;
      
      object_iter->second = NULL;
    }
  }

  TFile *ConfigStore::open(const std::string &file_name, const std::string &mode)
  {
    if (file_name.empty()) {
      std::stringstream message;
      message << "No filename given.";
      throw CannotOpenFile(message.str());
    }
    std::unique_ptr<TFile> a_file;
    if (mode=="UPDATE") {
      a_file=std::make_unique<TFile>(file_name.c_str(), mode.c_str());
      if (!a_file->IsOpen()) {
	a_file.reset();
	a_file =std::make_unique<TFile>(file_name.c_str(), "CREATE");
      }
      if (!a_file->IsWritable()) {
	std::stringstream message;
	message << "File " << file_name << " could not be opened for writing.";
	throw CannotOpenFile(message.str());
      }
    }
    else {
      a_file=std::make_unique<TFile>(file_name.c_str());
    }
    if (!a_file.get() || !a_file->IsOpen()) {
      std::stringstream message;
      message << "File " << file_name << " could not be opened.";
      throw CannotOpenFile(message.str());
    }
    return a_file.release();
  }

  TTree *tree(TFile &a_file, const std::string &tag, const std::string &config_name) {
    TDirectory *a_dir=a_file.GetDirectory(tag.c_str());
    if (a_dir) {
      a_dir->cd();
      TObject *obj = a_dir->Get(config_name.c_str());
      if (obj && obj->InheritsFrom(TTree::Class())) {
	return static_cast<TTree *>(obj);
      }
    }
    std::stringstream message;
    message << "File " << a_file.GetName() << " did not contain a tree for a config " << config_name << " of given tag " << tag << ".";
    throw CannotOpenFile(message.str() );
  }

  TTree *ConfigStore::createTree(TFile &a_file, const std::string &tag, const std::string &config_name) {
    a_file.ls();
    TKey *tag_dir_key = a_file.GetKey(tag.c_str());
    a_file.ls();
    TDirectory *tag_dir=NULL;
    if (!tag_dir_key) {
      tag_dir = a_file.mkdir(tag.c_str());
    }
    else {
      if (tag_dir_key->IsFolder() ) { //correct method ?
        tag_dir=a_file.GetDirectory(tag.c_str());
      }
    }

    if (tag_dir) {
      tag_dir->cd();
      tag_dir->ls();
      TObject *obj = tag_dir->Get(config_name.c_str());
      if (obj) {
	if (obj->InheritsFrom(TTree::Class())) {
	  fillBranchMap( *static_cast<TTree *>(obj) );
	  return static_cast<TTree *>(obj);
	}
	else {
	  std::stringstream message;
	  message << "File " << a_file.GetName() << " already contains an object named " << config_name << " in the given tag " << tag << " but it is not a TTree.";
	  throw CannotOpenFile(message.str());
	}
      }
      else {
	return new TTree(config_name.c_str(), config_name.c_str());
      }
    }
    std::stringstream message;
    message << "Failed to create a tree for config " << config_name << " with the tag "
	    << tag << " in the  file " << a_file.GetName() << ".";
    throw NoTree(message.str());
  }

  TTree *ConfigStore::tree(TFile &a_file, const std::string &tag, const std::string &config_name) {
    TDirectory *tag_dir=a_file.GetDirectory(tag.c_str());
    if (tag_dir) {
      tag_dir->cd();
      TObject *obj = tag_dir->Get(config_name.c_str());
      if (obj) {
	if (obj->InheritsFrom(TTree::Class())) {
	  fillBranchMap( *static_cast<TTree *>(obj) );
	  return static_cast<TTree *>(obj);
	}
	else {
	  std::stringstream message;
	  message << "File " << a_file.GetName() << " contains an object named " << config_name << " in the given tag " << tag << " but it is not a TTree.";
	  throw CannotOpenFile(message.str());
	}
      }
    }
    return NULL;
  }

  Revision_t ConfigStore::write(const PixLib::Config &config, const std::string &file_name, const std::string &tag, Revision_t revision)
  {
    // open will return a valid file or throw an exception
    std::unique_ptr<TFile> a_file(open(file_name,"UPDATE"));

    // tree will return a valid tree or throw an exception
    // when the file will be closed so will be the tree;
    TTree *a_tree = createTree(*a_file, tag, config.name());
    bool create=true;
    // write revision column
    Revision_t the_revision = (revision-1 >= UINT_MAX-1 ? time(0) : revision);
    // create or get an existing branch
    /*TBranch *revision_branch =*/ branch<TTree,Revision_t>(*a_tree, m_branchList,"_rev", &the_revision, create);

    setupBranches(config,*a_tree, &m_branchList, create);

    a_tree->Fill();
    a_tree->Write(a_tree->GetName(),TObject::kOverwrite);
    return the_revision;
  }

  void ConfigStore::read(PixLib::Config &config, const std::string &file_name, const std::string &tag, Revision_t revision)
  {
    // open will return a valid file or throw an exception
    std::unique_ptr<TFile> a_file(open(file_name,""));

    // tree will return a valid tree or throw an exception
    // when the file will be closed so will be the tree;
    TTree *a_tree = tree(*a_file, tag, config.name());
    Long64_t n_entries;
    if (!a_tree || (n_entries = a_tree->GetEntries())==0) { 
      std::stringstream message;
      message << " No revision of config " << config.name() << " in file " << a_file->GetName() << ".";
      throw RevisionNotFound(message.str());
    }

    Revision_t the_revision;
    TBranch *revision_branch = branch<TTree,Revision_t>(*a_tree,m_branchList,"_rev", &the_revision, false);
    if (!revision_branch) {
      std::stringstream message;
      message << " No revision of config " << config.name() << " in file " << a_file->GetName() << ".";
      throw RevisionNotFound(message.str());
    }
    Long64_t entry = revisionEntry(*a_tree, *revision_branch, revision, the_revision);

    // create or get an existing branch
    /*TBranch *revision_branch =*/ branch<TTree,Revision_t>(*a_tree,m_branchList,"_rev", &the_revision, false);

    setupBranches(config, *a_tree, &m_branchList, false);

    //  setup helper objects
    for (std::list< std::pair< TObject *,TObject *> >::iterator obj_iter = m_objectsToDelete.begin();
	 obj_iter != m_objectsToDelete.end();
	 obj_iter++) {
      if (obj_iter->first == obj_iter->second) {
 	obj_iter->second = NULL;
      }
    }

    // read
    unsigned int bytes = a_tree->GetEntry(entry);

    if (bytes==0) {
      std::cerr << "WARNING [ConfigStore::read] The revision does not contain any data." << std::endl;
    }
    else {
      for (std::list< std::pair< TObject *,TObject *> >::iterator obj_iter = m_objectsToDelete.begin();
 	   obj_iter != m_objectsToDelete.end();
 	   obj_iter++) {

 	if (obj_iter->first && obj_iter->second) {
	  obj_iter->second->Copy(*obj_iter->first);
 	}
 	else {
 	  std::cerr << "WARNING [ConfigStore::read] Missing helper object." << std::endl;
 	}
      }
    }
  }

  void ConfigStore::getRevisions(PixLib::Config &config, const std::string &file_name, const std::string &tag, std::vector<Revision_t> &revision)
  {
    // open will return a valid file or throw an exception
    std::unique_ptr<TFile> a_file(open(file_name,""));

    // tree will return a valid tree or throw an exception
    // when the file will be closed so will be the tree;
    TTree *a_tree = tree(*a_file, tag, config.name());
    Long64_t n_entries;
    if (!a_tree || (n_entries = a_tree->GetEntries())==0) { 
      std::stringstream message;
      message << " No revision of config " << config.name() << " in file " << a_file->GetName() << ".";
      throw RevisionNotFound(message.str());
    }

    Revision_t the_revision;
    TBranch *revision_branch = branch<TTree,Revision_t>(*a_tree,m_branchList,"_rev", &the_revision, false);

    for(Long64_t entry =0 ;entry<n_entries;entry++) {
      revision_branch->GetEntry(entry);
      revision.push_back(the_revision);
    }
  }

  Long64_t ConfigStore::revisionEntry(TTree &the_tree, TBranch &revision_branch, Revision_t revision, Revision_t &entry_revision )
  {
    Long64_t entry ;
    Revision_t matching_revision=UINT_MAX;

    if (revision-1 >= UINT_MAX-1) {
      //       for(entry =0 ;entry<n_entries;entry++) {
      // 	revision_branch->GetEntry(entry);
      // 	std::cout << entry_revision << std::endl;
      //       }
      entry = the_tree.GetEntries()-1;
    }
    else {
      Long64_t n_entries = the_tree.GetEntriesFast();

      for(entry =0 ;entry<n_entries;entry++) {
	revision_branch.GetEntry(entry);
	if (entry_revision > revision) {
	  break;
	}
	matching_revision = entry_revision;
      }

      if (entry==0) {
	std::cout << "WARNING [ConfigStore::read] The only revision is  " << entry_revision-revision 
		  << " sec. older. " << std::endl;
	if (n_entries==1) {
	  std::cout << "WARNING [ConfigStore::read] The only revision in the root tree " << revision << " is newer then the requested one " << entry_revision 
		    << ". In old versions, the time of writing was stored instead of the correct revision. So, this is considered to be okay." << std::endl;
	  matching_revision = revision;
	  entry=1;
	}
	else {
	  std::stringstream message;
	  message << " First Revision of config " << the_tree.GetName() << " in file " 
		  << revision_branch.GetFile()->GetName() 
		  << " is newer than the requested one.";
	  throw RevisionNotFound(message.str());
	}
      }
      entry--;
    }
    std::cout << "INFO [ConfigStore::read] get entry " << entry << " which should correspond to revision " << matching_revision << " <= " << revision 
	      << " < " << entry_revision << "." << std::endl;
    return entry;
  }

  void ConfigStore::setupBranches(const PixLib::ConfGroup &conf_group, TTree &a_tree, BranchList *branch_list, bool create)
  {
    if (!branch_list) {
      std::cout << "FATAL [ConfigStore::setupBranches] no branch list for conf group  " << conf_group.name() << "." << std::endl;
      return;
    }
    for (std::vector<PixLib::ConfObj*>::const_iterator conf_obj_iter =  conf_group.begin();
	 conf_obj_iter != conf_group.end();
	 ++conf_obj_iter) {

      PixLib::ConfObj::types obj_type = (*conf_obj_iter)->type();
      IConfigStoreDriver *driver = ConfigStoreDriverList::instance()->driver(obj_type);

      if (driver) {

	// remove the conf group part from the object name such that 
	// config and conf group parts are split apart only by a "."
	std::string group_name(conf_group.name());
	std::string object_name((*conf_obj_iter)->name());
	if ( object_name.size() > group_name.size() && object_name[group_name.size()]=='_') {
	  object_name.erase(0,group_name.size()+1);
	}

	try {
	  driver->setupBranch(*this, object_name, *conf_obj_iter,  a_tree, *branch_list, create);
	}
	catch ( BranchHasWrongType &err ) {
	  std::cerr << "ERROR [ConfigStore::write]" << err.getDescriptor() << std::endl;
	}
	catch ( MissingBranch &err) {
	  std::cerr << "WARNING [ConfigStore::write] No data read for object [" << group_name << "][" << object_name <<"] because of : " 
		    << err.getDescriptor() << std::endl;
	}

      }
      else {
	std::cerr << "ERROR [ConfigStore::write] Don't know how to write config objects of type " << obj_type << std::endl;
      }
    }

  }

  void ConfigStore::setupBranches(const PixLib::Config &config, TTree &a_tree, BranchList *branch_list, bool create)
  {
    if (!branch_list) {
      std::cout << "FATAL [ConfigStore::setupBranches] no branch list for config " << config.name() << "." << std::endl;
      return;
    }
    // @todo try to create sub branches instead of writing conf groups and sub configs at the same level
    for(unsigned int group_i=0; group_i <config.size(); ++group_i) {
      const PixLib::ConfGroup &conf_group(config[group_i]);

      setupBranches(conf_group,
		    a_tree,
		    ( create ?    branch_list->addSubBranches(sanitiseName(conf_group.name()))
			       :  branch_list->subBranches(sanitiseName(conf_group.name())) ),
		    create);
    }

    for (std::map<std::string, PixLib::Config*>::const_iterator config_iter = config.m_config.begin();
	 config_iter != config.m_config.end();
	 ++config_iter) {

      setupBranches(*(config_iter->second),
		    a_tree,
		    ( create ?    branch_list->addSubBranches(sanitiseName(config_iter->first))
			       :  branch_list->subBranches(sanitiseName(config_iter->first)) ),
		    create);
    }

  }

  void ConfigStore::fillBranchMap(TTree &the_tree) {
    // to speed up the branch lookup, organise the branches in 
    // a tree.
    TObjArray *list_of_branchs = the_tree.GetListOfBranches();
    BranchList *last_branch_list=NULL;
    std::string last_branch_path;
    for (int branch_i=0; branch_i < list_of_branchs->GetEntries(); branch_i++) {
      assert( list_of_branchs->At(branch_i)->InheritsFrom(TBranch::Class()));
      TBranch *a_branch = static_cast<TBranch *>( list_of_branchs->At(branch_i) );
      if (!a_branch) continue;

      // Somehow branches disabled in this way do not seem to get activated again.
      a_branch->SetBit(kDoNotProcess); // first switch off all branches

      // the will be reactivated by the method setupBranches
      std::string branch_name(a_branch->GetName());
      assert( !branch_name.empty() );

      std::string::size_type last_pos = branch_name.rfind(".");
      BranchList * this_branch_list;
      if (last_pos!= std::string::npos) {
	if (last_branch_path == branch_name.substr(0,last_pos)) {
	  this_branch_list = last_branch_list;
	}
	else {
	  this_branch_list = &m_branchList;
	  std::string::size_type pos=0;
	  std::string::size_type start_pos = pos;

	  //  navigate down all sub branches.  sub branches are separated by "."
	  while ( (pos = branch_name.find(".",start_pos)) != std::string::npos) {
	    this_branch_list = this_branch_list->addSubBranches(sanitiseName(branch_name.substr(start_pos, pos-start_pos)));
	    start_pos = pos+1;
	  } 
	  last_branch_list = this_branch_list;
	  last_branch_path = branch_name.substr(0,last_pos);
	}
	last_pos++;
      }
      else {
	last_pos =0;
	this_branch_list = &m_branchList;
      }

      this_branch_list->branchMap().insert(std::make_pair( sanitiseName(branch_name.substr(last_pos,branch_name.size()-last_pos)), a_branch) );
    }
  }

  std::string ConfigStore::sanitiseName(const std::string &name) {
    std::string::size_type pos  = name.find("/");
    if (pos !=std::string::npos) {
      std::string temp(name);
      temp.erase(pos,name.size()-pos);
      return temp;
    }
    return name;
  }


}
