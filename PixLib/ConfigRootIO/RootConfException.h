#ifndef _RootConf_Exceptions_h_
#define _RootConf_Exceptions_h_

#include <ConfigRootIO/pixa_exception.h>

namespace RootConf {

  class MissingBranch : public PixLib::ConfigException
  {
  public:
    MissingBranch(const std::string &message) : PixLib::ConfigException(message) {}
  };

  class BranchHasWrongType : public MissingBranch
  {
  public:
    BranchHasWrongType(const std::string &message) : MissingBranch(message) {}
  };

  class BranchDoesNotExist : public MissingBranch
  {
  public:
    BranchDoesNotExist(const std::string &message) : MissingBranch(message) {}
  };

  class InconsistentTree : public MissingBranch
  {
  public:
    InconsistentTree(const std::string &message) : MissingBranch(message) {}
  };

  class NoTree : public MissingBranch
  {
  public:
    NoTree(const std::string &message) : MissingBranch(message) {}
  };

  class CannotOpenFile : public NoTree
  {
  public:
    CannotOpenFile(const std::string &message) : NoTree(message) {}
  };

  class RevisionNotFound : public NoTree
  {
  public:
    RevisionNotFound(const std::string &message) : NoTree(message) {}
  };
}
#endif
