#ifndef _pixlib_exception_h_
#define _pixlib_exception_h_

#include <BaseException.h>
namespace PixLib {

  typedef SctPixelRod::BaseException BaseException;

  class  ConfigException : public BaseException
  {
  public:
    ConfigException(const std::string &name) : BaseException(name) {};

  };

  class  ConfigTypeMismatch : public ConfigException
  {
  public:
    ConfigTypeMismatch() : ConfigException("Config value does not mismatch type.") {};

  };


}
#endif
