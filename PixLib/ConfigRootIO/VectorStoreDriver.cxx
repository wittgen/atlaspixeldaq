#include "ObjectStoreDriver.h"
#include "BranchUtil.h"
#include <Config/ConfObj.h>
#include "ConfigStoreDriverList.h"
#include "TSTLWrapper.h"
#include "ConfigStore.h"

namespace RootConf {


  class ConfVectorStoreDriver : public ObjectStoreDriver {
  public:

    void setupBranch(ConfigStore &config_store, const std::string &branch_name, PixLib::ConfObj *conf_obj, TTree &a_tree, BranchList &branch_list, bool create) {
      assert( dynamic_cast<PixLib::ConfVector *>(conf_obj) );
      PixLib::ConfVector *conf_vector = static_cast<PixLib::ConfVector *>(conf_obj);

      switch ( conf_vector->subtype() ) {
      case PixLib::ConfVector::V_INT: {
	_setupBranch(new STLVectorIntWrapperObject(&(conf_vector->valueVInt())), config_store, branch_name, a_tree, branch_list, create);
	break;
      }
      case PixLib::ConfVector::V_UINT: {
	_setupBranch(new STLVectorUIntWrapperObject(&(conf_vector->valueVUint())), config_store, branch_name, a_tree, branch_list, create);
	break;
      }
      case PixLib::ConfVector::V_FLOAT: {
	_setupBranch(new STLVectorFloatWrapperObject(&(conf_vector->valueVFloat())), config_store, branch_name, a_tree, branch_list, create);
	break;
      }
      default:throw std::runtime_error("Invalid Config Subtype");
      }

    }

  };

  class ConfStringVectorStoreDriver : public ObjectStoreDriver {
  public:

    void setupBranch(ConfigStore &config_store, const std::string &branch_name, PixLib::ConfObj *conf_obj, TTree &a_tree, BranchList &branch_list, bool create) {
      assert( dynamic_cast<PixLib::ConfStrVect *>(conf_obj) );
      PixLib::ConfStrVect *conf_vector = static_cast<PixLib::ConfStrVect *>(conf_obj);
      _setupBranch(new STLVectorStringWrapperObject(&(conf_vector->value())), config_store, branch_name, a_tree, branch_list, create);
    }

  };


  bool registerConfVectorDriver() {
      ConfigStoreDriverList::instance()->registerDriver(PixLib::ConfObj::VECTOR,   new ConfVectorStoreDriver);
      ConfigStoreDriverList::instance()->registerDriver(PixLib::ConfObj::STRVECT,  new ConfStringVectorStoreDriver);
      return true;
  }


  bool s_ConfVectorStoreDriver = registerConfVectorDriver();

}
