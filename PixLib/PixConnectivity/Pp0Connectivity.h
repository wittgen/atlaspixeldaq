/////////////////////////////////////////////////////////////////////
// Pp0ConnectivityObject.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 05/10/06  Version 1.0 (PM)
//           Initial release

//! Class for module connectivity

#ifndef _PIXLIB_PP0CONNOBJ
#define _PIXLIB_PP0CONNOBJ

#include "PixConnectivity/GenericConnectivityObject.h"
#include <array>

namespace PixLib {

class ModuleConnectivity;
class OBConnectivity;

class Pp0Connectivity : public GenericConnectivityObject {
public:
  //! Payload accessors
  std::string& coolingLoop() { return m_coolingLoop; };
  //! Down links
  ModuleConnectivity* modules(int slot);
  void addModule(int slot, ModuleConnectivity* mod);
  //! Up link
  //! Soft links
  auto& getModules() { return m_modules; }
  OBConnectivity *ob() { return m_ob; };
  std::string obName() { return m_obName; };
  std::string obSName() { return m_obSName; };
  void linkOB(OBConnectivity *ob);
  //! Internal connectivity tables
  //! Constructors
  Pp0Connectivity(std::string name);
  Pp0Connectivity(DbRecord *r, int lev);
  //! Config handling
  void initConfig();
  virtual void readConfig(DbRecord *r, int lev);
  virtual void writeConfig(DbRecord *r, int lev);
  //! Link to configuration
private:
  std::array<ModuleConnectivity*,8> m_modules;
  std::string m_obName;
  std::string m_obSName;
  OBConnectivity *m_ob;
  std::string m_coolingLoop;
};

}

#endif
