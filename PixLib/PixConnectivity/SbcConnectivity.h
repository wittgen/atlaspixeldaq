/////////////////////////////////////////////////////////////////////
// SbcConnectivity.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 05/10/06  Version 1.0 (PM)
//           Initial release

//! Class for SBC connectivity

#ifndef _PIXLIB_SBCCONNOBJ
#define _PIXLIB_SBCCONNOBJ

#include "PixConnectivity/GenericConnectivityObject.h"

#include <map>

namespace PixLib {

class RodCrateConnectivity;

class SbcConnectivity : public GenericConnectivityObject {
public:
  //! Payload accessors
  int &vmeSlot() { return m_vmeSlot; };
  std::string &ipName() { return m_ipName; };
  std::string &ipAddr() { return m_ipAddr; };
  std::string &hwAddr() { return m_hwAddr; };
  std::string &ipName1() { return m_ipName1; };
  std::string &ipAddr1() { return m_ipAddr1; };
  std::string &hwAddr1() { return m_hwAddr1; };
  //! Enable parameters
  bool enableReadout;
  //! Down links
  //! Up link
  RodCrateConnectivity *crate() { return m_crate; };
  void linkCrate(RodCrateConnectivity *cr, int slot);
  //! Soft links
  //! Internal connectivity tables
  //! Constructors
  SbcConnectivity(std::string name);
  SbcConnectivity(DbRecord *r, int lev);
  //! Config handling
  void initConfig();
  virtual void readConfig(DbRecord *r, int lev);
  virtual void writeConfig(DbRecord *r, int lev);
  //! Link to configuration
private:
  RodCrateConnectivity *m_crate;
  int m_vmeSlot;
  std::string m_ipName;
  std::string m_ipAddr;
  std::string m_hwAddr;
  std::string m_ipName1;
  std::string m_ipAddr1;
  std::string m_hwAddr1;
};

}

#endif
