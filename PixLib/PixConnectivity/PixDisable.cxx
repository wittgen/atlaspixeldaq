/////////////////////////////////////////////////////////////////////
// PixDisable.cxx
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 22/10/06  Version 1.0 (PM)
//           Initial release

//! Class for Ena/Dis mask permanentization

#include "PixConnectivity/PixConnectivity.h"
#include "PixConnectivity/TimConnectivity.h"
#include "PixConnectivity/PartitionConnectivity.h"
#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixConnectivity/RodBocConnectivity.h"
#include "PixConnectivity/OBConnectivity.h"
#include "PixConnectivity/Pp0Connectivity.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "PixConnectivity/RosConnectivity.h"
#include "PixConnectivity/PixDisable.h"

using namespace PixLib;

PixDisable::PixDisable(std::string name) {
  m_config = new Config(name, "PixDisable");
  Config &conf = *m_config;

  conf.addGroup("Disabled");
  conf["Disabled"].addStrVect("Objects", m_dis, "DUMMY", "List of disabled connectivity objects", true);
  conf.reset();
  m_dis.clear();
}

PixDisable::~PixDisable() {
  if (m_config != NULL) delete m_config;
}

void PixDisable::enable(std::string name) {
  std::vector<std::string>::iterator it;
  for (it = m_dis.begin(); it != m_dis.end(); it++) {
    if (*it == name) {
      m_dis.erase(it);
      break;
    }
  }
}

void PixDisable::disable(std::string name) {
  std::vector<std::string>::iterator it;
  bool found = false;
  for (it = m_dis.begin(); it != m_dis.end(); it++) {
    if (*it == name) found = true;
  }
  if (!found) {
    m_dis.push_back(name);
  }
}

void PixDisable::clear() {
  m_dis.clear();
}

void PixDisable::readConfig(DbRecord *rec) {  //! Read from RootDb
  m_config->read(rec);
}

void PixDisable::writeConfig(DbRecord *rec) { //! Write to RootDb
  m_config->write(rec);
}

bool PixDisable::writeConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, std::string ptag, unsigned int revision) {  //! Read from DB server
  return m_config->write(DbServer, domainName, tag, m_config->name(), m_config->type(), ptag, revision);
}  

bool PixDisable::readConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, unsigned int revision) { //! Write to DB server
  return m_config->read(DbServer, domainName, tag, m_config->name(), revision);
}

void PixDisable::readConfig(PixConnectivity *conn) {  //! Read via a PixConnectivity cfg 
  if (conn != NULL) {
    conn->readDisable(this, m_config->name());
  }
}

void PixDisable::writeConfig(PixConnectivity *conn) {  //! Write via a PixConnectivity cfg
  if (conn != NULL) {
    conn->writeDisable(this, m_config->name());
  }
}

void PixDisable::get(PixConnectivity *conn) { //! Import from a connectivity db
  // Propagate the disable state
  propagate(conn);

  // Import the disable state
  clear();
  std::map<std::string, PartitionConnectivity*>::iterator ipp;
  for (ipp=conn->parts.begin(); ipp!=conn->parts.end(); ++ipp) {
    if (!ipp->second->active()) {
      m_dis.push_back(ipp->second->name());
    } else {
      std::map<std::string, RodCrateConnectivity*>::iterator ipc;
      for (ipc=ipp->second->rodCrates().begin(); ipc!=ipp->second->rodCrates().end(); ipc++) {
	if (!ipc->second->active()) {
	  m_dis.push_back(ipc->second->name());
	} else {
	  std::map<int, RodBocConnectivity*>::iterator ipr;
	  for (ipr=ipc->second->rodBocs().begin(); ipr!=ipc->second->rodBocs().end(); ipr++) {
	    if (!ipr->second->active()) {
	      m_dis.push_back(ipr->second->name());
	    } else {
	      for(int i=0; i<=4; i++) {
		OBConnectivity* obs=ipr->second->obs(i);
		if (obs!=NULL) {
		  if (obs->pp0() != NULL) {
		    if (!obs->pp0()->active()) {
		      m_dis.push_back(obs->pp0()->name());
		    } else {
		      for (int k=1; k<=8; k++) {
			ModuleConnectivity* mod=obs->pp0()->modules(k);
			if (mod!=NULL) {
			  if (!mod->active()) {
			    m_dis.push_back(mod->name());
			  }
			}
		      }
		    }
		  }
		}
	      }
	    }
	  }
	}
      }
    }
  }
}

void PixDisable::put(PixConnectivity *conn) { //! Export to a connectivity db
  // Enable everything
  std::map<std::string, PartitionConnectivity*>::iterator ip;
  for (ip=conn->parts.begin(); ip!=conn->parts.end(); ++ip) {
    ip->second->active() = true;
  }
  std::map<std::string, RodCrateConnectivity*>::iterator icr;
  for (icr=conn->crates.begin(); icr!=conn->crates.end(); ++icr) {
    icr->second->active() = true;
  }
  std::map<std::string, RodBocConnectivity*>::iterator irb;
  for (irb=conn->rods.begin(); irb!=conn->rods.end(); ++irb) {
    irb->second->active() = true;
  }
  std::map<std::string, Pp0Connectivity*>::iterator ipp;
  for (ipp=conn->pp0s.begin(); ipp!=conn->pp0s.end(); ++ipp) {
    ipp->second->active() = true;
  }
  std::map<std::string, ModuleConnectivity*>::iterator im;
  for (im=conn->mods.begin(); im!=conn->mods.end(); ++im) {
    im->second->active() = true;
  }

  // Disable selected 
  std::vector<std::string>::iterator it;
  for (it=m_dis.begin(); it!=m_dis.end(); it++) {
    ip = conn->parts.find(*it);
    if (ip != conn->parts.end()) ip->second->active() = false;
    icr = conn->crates.find(*it);
    if (icr != conn->crates.end()) icr->second->active() = false;
    irb = conn->rods.find(*it);
    if (irb != conn->rods.end()) irb->second->active() = false;
    ipp = conn->pp0s.find(*it);
    if (ipp != conn->pp0s.end()) ipp->second->active() = false;
    im = conn->mods.find(*it);
    if (im != conn->mods.end()) im->second->active() = false;
  }
  // Propagate the disable state
  propagate(conn);
}

std::string PixDisable::bin(PixConnectivity* conn) {
  std::string dis = "";
  char ch = 0;
  unsigned int cch = 0;

  std::map<std::string, PartitionConnectivity*>::iterator ipp;
  for (ipp=conn->parts.begin(); ipp!=conn->parts.end(); ++ipp) {
    if (ipp->second->active()) ch += (0x1<<cch);
    cch++; if (cch==8) { cch = 0; dis += ch; ch = 0; }
    std::map<std::string, RodCrateConnectivity*>::iterator ipc;
    for (ipc=ipp->second->rodCrates().begin(); ipc!=ipp->second->rodCrates().end(); ipc++) {
      if (ipc->second->active()) ch += (0x1<<cch);
      cch++; if (cch==8) { cch = 0; dis += ch; ch = 0; }
      std::map<int, RodBocConnectivity*>::iterator ipr;
      for (ipr=ipc->second->rodBocs().begin(); ipr!=ipc->second->rodBocs().end(); ipr++) {
	if (ipr->second->active()) ch += (0x1<<cch);
	cch++; if (cch==8) { cch = 0; dis += ch; ch = 0; }
	for(int i=0; i<=4; i++) {
	  OBConnectivity* obs=ipr->second->obs(i);
	  if (obs!=NULL) {
	    if (obs->pp0() != NULL) {
	      if (obs->pp0()->active()) ch += (0x1<<cch);
	      cch++; if (cch==8) { cch = 0; dis += ch; ch = 0; }
	      for (int k=1; k<=8; k++) {
		ModuleConnectivity* mod=obs->pp0()->modules(k);
		if (mod!=NULL) {
		  if (mod->active()) ch += (0x1<<cch);
		  cch++; if (cch==8) { cch = 0; dis += ch; ch = 0; }
		}
	      }
	    }
	  }
	}
      }
    }
  }

  return dis;
}

void PixDisable::bin(std::string dis, PixConnectivity* conn) {
  // Import the disable state
  clear();

  unsigned int cch = 0;

  std::map<std::string, PartitionConnectivity*>::iterator ipp;
  for (ipp=conn->parts.begin(); ipp!=conn->parts.end(); ++ipp) {
    if (dis[cch/8]&(0x1<<(cch%8))) m_dis.push_back(ipp->second->name());
    cch++;
    std::map<std::string, RodCrateConnectivity*>::iterator ipc;
    for (ipc=ipp->second->rodCrates().begin(); ipc!=ipp->second->rodCrates().end(); ipc++) {
      if (dis[cch/8]&(0x1<<(cch%8))) m_dis.push_back(ipc->second->name());
      cch++;
      std::map<int, RodBocConnectivity*>::iterator ipr;
      for (ipr=ipc->second->rodBocs().begin(); ipr!=ipc->second->rodBocs().end(); ipr++) {
	if (dis[cch/8]&(0x1<<(cch%8))) m_dis.push_back(ipr->second->name());
	cch++;
	for(int i=0; i<=4; i++) {
	  OBConnectivity* obs=ipr->second->obs(i);
	  if (obs!=NULL) {
	    if (obs->pp0() != NULL) {
	      if (dis[cch/8]&(0x1<<(cch%8))) m_dis.push_back(obs->pp0()->name());
	      cch++;
	      for (int k=1; k<=8; k++) {
		ModuleConnectivity* mod=obs->pp0()->modules(k);
		if (mod!=NULL) {
		  if (dis[cch/8]&(0x1<<(cch%8))) m_dis.push_back(mod->name());
		  cch++;
		}
	      }
	    }
	  }
	}
      }
    }
  }
}

void PixDisable::propagate(PixConnectivity* conn) {
  // Propagate disable state
  std::map<std::string, PartitionConnectivity*>::iterator ipp;
  for (ipp=conn->parts.begin(); ipp!=conn->parts.end(); ++ipp) {
    bool pen = ipp->second->active();
    bool pdis = true;
    std::map<std::string, RodCrateConnectivity*>::iterator ipc;
    for (ipc=ipp->second->rodCrates().begin(); ipc!=ipp->second->rodCrates().end(); ipc++) {
      if (!pen) ipc->second->active() = false;
      bool cen = ipc->second->active();
      if (cen) pdis = false;
      bool cdis = true;
      if (ipc->second->tim()!=NULL) {
	if (cen) ipc->second->tim()->active()=true;
	else ipc->second->tim()->active()=false;
      }
      std::map<int, RodBocConnectivity*>::iterator ipr;
      for (ipr=ipc->second->rodBocs().begin(); ipr!=ipc->second->rodBocs().end(); ipr++) {
	if (!cen) ipr->second->active() = false;
	bool ren = ipr->second->active();
	if (ren) cdis = false;
	bool rdis = true; 
	if (ipr->second->rol() != NULL) {
	  if (!ren) ipr->second->rol()->active() = false;
	}
	for(int i=0; i<=4; i++) {
	  OBConnectivity* obs=ipr->second->obs(i);
	  if (obs!=NULL) {
	    if (obs->pp0() != NULL) {
	      if (!ren) obs->pp0()->active() = false;
	      bool ppen = obs->pp0()->active();
	      if (ppen) rdis = false;
	      bool ppdis = true;
	      for (int k=1; k<=8; k++) {
		ModuleConnectivity* mod=obs->pp0()->modules(k);
		if (mod!=NULL) {
		  if (!ppen) mod->active() = false;
		  if (mod->active()) ppdis = false;
		}
	      }
	      if (ppdis) obs->pp0()->active() = false;
	    }
	  }
	}
	if (rdis) ipr->second->active() = false;
      }
      if (cdis) ipc->second->active() = false;
      std::cout << "Crate  = " << ipc->second->name() << std::endl;
    }
    if (pdis) ipp->second->active() = false;
    std::cout << "Part  = " << ipp->second->name() << std::endl;
  }
  // ROS/ROBIN propagation
  std::map<std::string, RosConnectivity*>::iterator ipro;
  for (ipro=conn->roses.begin(); ipro!=conn->roses.end(); ++ipro) {
    bool rosdis = true;
    RosConnectivity *ros = ipro->second;
    for (unsigned int i=0; i<RosConnectivity::nRobinsPerRos; i++) {
      bool robindis = true;
      RobinConnectivity *robin = ros->robins(i);
      if (robin != NULL) {
	for (unsigned int j=0; j<RobinConnectivity::nRoLinksPerRobin; j++) {
	  RolConnectivity *rol = robin->rols(j);
	  if (rol != NULL) {
	    if (rol->active()) robindis = false;
	  }
	}
	if (robindis) robin->active() = false;
	if (robin->active()) rosdis = false;
      }
    }
    if (rosdis) ros->active() = false;
  }
}


bool PixDisable::isDisable(std::string name) {
  for (unsigned int i=0; i<m_dis.size(); i++) {
    if (name==m_dis[i]) return true;
  }
  return false;
}
