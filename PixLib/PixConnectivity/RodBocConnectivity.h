/////////////////////////////////////////////////////////////////////
// RodBocConnectivity.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 05/10/06  Version 1.0 (PM)
//           Initial release

//! Class for ROD/BOC connectivity

#ifndef _PIXLIB_RODBOCCONNOBJ
#define _PIXLIB_RODBOCCONNOBJ

#include "PixConnectivity/GenericConnectivityObject.h"

#include <map>

namespace PixLib {

class OBConnectivity;
class RodCrateConnectivity;
class RolConnectivity;

class RodBocLinkMap : public GenericConnectivityObject {
public:
  //! Payload accessors
  //! Down links
  //! Up link
  //! Soft links
  //! Internal connectivity tables
  int rodTx[4][8];
  int rodRx40[4][8]; 
  int rodRx80a[4][8];  
  int rodRx80b[4][8];  
  //! Constructors
  RodBocLinkMap(std::string name);
  RodBocLinkMap(DbRecord *r);
  //! Config handling
  void initConfig();
  virtual void readConfig(DbRecord *r);
  virtual void writeConfig(DbRecord *r);
  //! Link to configuration
private:
  using GenericConnectivityObject::readConfig;
  using GenericConnectivityObject::writeConfig;
};

class RodBocConnectivity : public GenericConnectivityObject {
public:
  //! Payload accessors
  unsigned int &fmtLinkMap(int fmt) {
    static unsigned int a;
    if (fmt >= 0 && fmt < 8) {
      return m_fmtLinkMap[fmt];
    } 
    a = 0;
    return a;
  }
  int &vmeSlot() { return m_vmeSlot; };
  std::string &offlineId() { return m_offline; };
  std::string txRibonId[4];
  std::string rxRibbonId[4];
  //Configuration file parameter
  std::string  configFile;
  //! Enable parameters
  bool enableReadout;
  //! Down links
  OBConnectivity* obs(int slot);
  auto& getOBs() {return m_ob; }
  std::string &obNames(int slot);
  void addOB(int s1, int s2, int s3, OBConnectivity *ob);
  RolConnectivity* rol() { return m_rol; };
  std::string rolName() { return m_rolSName; };
  void addRol(RolConnectivity *rol);
  //! Up link
  RodCrateConnectivity *crate() { return m_crate; };
  void linkCrate(RodCrateConnectivity *cr, int slot);
  //! Soft links
  std::string &rodConfig() { return m_rodConfig; };
  std::string &bocConfig() { return m_bocConfig; };
  //! Internal connectivity tables
  void linkMap(RodBocLinkMap *rblm);
  RodBocLinkMap* &linkMap() { return m_linkMap; };
  std::string linkMapName() { return m_linkMapName; };
  std::string linkMapSName() { return m_linkMapSName; };
  std::string rodBocType() const { return m_rodBocType; };
  void setRodBocType(std::string typestrg) { m_rodBocType = typestrg; };
  std::string rodBocInfo() { return m_rodBocInfo; };
  void setRodBocInfo(std::string infostrg) { m_rodBocInfo = infostrg; };
  //std::string fitFarmInstance(){return m_fitFarmInstance;};
  void setFitFarmInstance(std::string fitFarmInstance){m_fitFarmInstance = fitFarmInstance;};
  //! Constructors
  RodBocConnectivity(std::string name);
  RodBocConnectivity(DbRecord *r, int lev);
  //! Config handling
  void initConfig();
  virtual void readConfig(DbRecord *r, int lev);
  virtual void writeConfig(DbRecord *r, int lev);
  //! Link to configuration
private:
  std::array<OBConnectivity*,4> m_ob;
  std::string m_obName[4];
  std::string m_rodConfig;
  std::string m_bocConfig;
  std::string m_rodBocType;
  std::string m_rodBocInfo;
  std::string m_fitFarmInstance;
  RodCrateConnectivity *m_crate;
  int m_vmeSlot;
  RolConnectivity *m_rol;
  std::string m_rolName;
  std::string m_rolSName;
  RodBocLinkMap *m_linkMap;
  std::string m_linkMapName;
  std::string m_linkMapSName;
  unsigned int m_fmtLinkMap[8];
  std::string m_offline;
};

}

#endif
