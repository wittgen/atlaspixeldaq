/////////////////////////////////////////////////////////////////////
// RodBocConnectivity.cxx
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 05/10/06  Version 1.0 (PM)
//           Initial release

//! Class for ROD/BOC connectivity

#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixConnectivity/RodBocConnectivity.h"
#include "PixConnectivity/OBConnectivity.h"
#include "PixConnectivity/RosConnectivity.h"
#include "PixDbInterface/PixDbInterface.h"
#include "Config/Config.h"

using namespace PixLib;

RodBocLinkMap::RodBocLinkMap(std::string name) : 
  GenericConnectivityObject(name) {
  // Create the config object
  initConfig(); 
}

RodBocLinkMap::RodBocLinkMap(DbRecord *r) : 
  GenericConnectivityObject(r) {
  if (r->getClassName() == "RODBOCLINKMAP") {
    // Read record from DB
    storeRec(r);
    // Create the config object
    initConfig();
    readConfig(getRec()); 
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a RodBocLinkMap object");
  }
}

void RodBocLinkMap::initConfig() {
  int rx4[4][8] =  { { 0x00, 0x01, 0x02, 0x03, 0x10, 0x11, 0x12, 0x13 }, 
                     { 0x20, 0x21, 0x22, 0x23, 0x30, 0x31, 0x32, 0x33 }, 
                     { 0x40, 0x41, 0x42, 0x43, 0x50, 0x51, 0x52, 0x53 }, 
                     { 0x60, 0x61, 0x62, 0x63, 0x70, 0x71, 0x72, 0x73 } };
  int rx8a[4][8] = { { 0x00, 0x02, 0x10, 0x12, 0x20, 0x22, 0x30, 0x32 }, 
                     {   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1 }, 
                     {   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1 }, 
                     { 0x40, 0x42, 0x50, 0x52, 0x60, 0x62, 0x70, 0x72 } };
  int rx8b[4][8] = { { 0x01, 0x03, 0x11, 0x13, 0x21, 0x23, 0x31, 0x33 }, 
                     {   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1 }, 
                     {   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1 }, 
                     { 0x41, 0x43, 0x51, 0x53, 0x61, 0x63, 0x71, 0x73 } };
  int tx[4][8] =   { {  2,  3,  4,  5,  6,  7,  8,  9 },
		     { 14, 15, 16, 17, 18, 19, 20, 21 }, 
                     { 26, 27, 28, 29, 30, 31, 32, 33 },
                     { 38, 39, 40, 41, 42, 43, 44, 45 } };
 
  m_config = new Config(m_name, "RODBOCLINKMAP");
  Config &conf = *m_config;
  conf.addGroup("Identifier");
  conf["Identifier"].addString("Id", m_name, m_name, "Object identifier", true);
  conf.addGroup("Maps");
  int i,j;
  for (i=0; i<4; i++) {
    for (j=0; j<8; j++) {
      std::ostringstream o;
      o << "_" << i << "_" << j;
      conf["Maps"].addInt("RodTx"+o.str(), rodTx[i][j], tx[i][j], "BOC/ROD tx map", true);
      conf["Maps"].addInt("RodRx40"+o.str(), rodRx40[i][j], rx4[i][j], "BOC/ROD fmt map", true);
      conf["Maps"].addInt("RodRx80a"+o.str(), rodRx80a[i][j], rx8a[i][j], "BOC/ROD ch map", true);
      conf["Maps"].addInt("RodRx80b"+o.str(), rodRx80b[i][j], rx8b[i][j], "BOC/ROD ch map", true);
    }
  }
  conf.reset();
}

void RodBocLinkMap::readConfig(DbRecord *r) { 
  if (r->getClassName() == "RODBOCLINKMAP") {
    storeRec(r);
    m_name = r->getName();
    // Read the config object
    m_config->read(r); 
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for RodBocLinkMap object");
  }
}

void RodBocLinkMap::writeConfig(DbRecord *r) { 
  if (r->getClassName() == "RODBOCLINKMAP") {
    storeRec(r);
    // Read the config object
    m_config->write(r);
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a RodBocLinkMap object");
  }
}

RodBocConnectivity::RodBocConnectivity(std::string name) : 
  GenericConnectivityObject(name), m_crate(NULL), m_linkMap(NULL) {
  // Create the config object
  int i;
  for (i=0; i<4; i++) m_ob[i] = NULL;
  m_rol = NULL;
  initConfig(); 
}

RodBocConnectivity::RodBocConnectivity(DbRecord *r, int lev) : 
  GenericConnectivityObject(r), m_crate(NULL), m_linkMap(NULL) {
  int i;
  for (i=0; i<4; i++) m_ob[i] = NULL;
  m_rol = NULL;
  if (r->getClassName() == "RODBOC") {
    // Read record from DB
    storeRec(r);
    // Create the config object
    initConfig();
    readConfig(getRec(), lev); 
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a RodBocConnectivity object");
  }
}

void RodBocConnectivity::initConfig() { 
  std::string FileName = "/configs/Crate0Slot0.cfg.root";
  m_config = new Config(m_name, "RODBOC");
  Config &conf = *m_config;

  conf.addGroup("Identifier");
  conf["Identifier"].addAlias("OFFLINEID", m_offline, "0xnnnnnn", "Rod offline identifier", true);

  conf.addGroup("Config");
  conf["Config"].addString("rodConfig", m_rodConfig, "", "Link to ROD configuration", true);
  conf["Config"].addString("bocConfig", m_bocConfig, "", "Link to BOC configuration", true);
  conf["Config"].addString("rodBocType", m_rodBocType, "RODBOC", "Type of controller", true);
  conf["Config"].addString("rodBocInfo", m_rodBocInfo, "", "init. info for RODBOC", true);
  conf["Config"].addString("fitFarmInstance", m_fitFarmInstance, "FitServer-NONE", "Fitting farm instance", true);
  conf.addGroup("Link");
  conf["Link"].addLink("LINK_MAP", m_linkMapName, "NULL", "Link to the I/O channels map", true);
  conf["Link"].addString("MapSName", m_linkMapSName, "NULL", "Link to the I/O channels map", true);
  conf["Link"].addLink("ROL", "ROD", m_rolName, "", "Link to ROL record", true);
  conf.addGroup("Fmt");
  for (unsigned int i=0; i<8; i++) {
    std::ostringstream fnum;
    fnum << i;
    std::string tit = "linkMap_"+fnum.str();
    unsigned int def = 0x32100000;
    if (i==2 || i==6) def = 0xba980000;
    conf["Fmt"].addInt("linkMap_"+fnum.str(), m_fmtLinkMap[i], def,
		      "Formatter "+fnum.str()+" link map", true);
  }
  conf.addGroup("Enable");
  conf["Enable"].addBool("readout", enableReadout, 1, "ROD readout enable", true);
  conf.reset();
}

void RodBocConnectivity::linkMap(RodBocLinkMap *rblm) {
  m_linkMap = rblm;
  m_linkMapName = rblm->decName();
  m_linkMapSName = rblm->name();
}

void RodBocConnectivity::addOB(int s1, int s2, int s3, OBConnectivity *ob) {
  if (s1>=0 && s1 < 4) {
    if (m_ob[s1] != NULL) {
      delete m_ob[s1];
    }
    m_ob[s1] = ob;
    m_obName[s1] = ob->name();
    ob->linkRodBoc(this, s1, s2, s3);
  }
}

void RodBocConnectivity::addRol(RolConnectivity *rol) {
  m_rol = rol;
  rol->linkRod(this);
}

void RodBocConnectivity::linkCrate(RodCrateConnectivity *cr, int slot) {
  m_crate = cr;
  m_vmeSlot = slot;
}

OBConnectivity* RodBocConnectivity::obs(int slot) {
  if (slot >=0 && slot < 4) {
    return m_ob[slot];
  }
  return NULL;
}

std::string &RodBocConnectivity::obNames(int slot) {
  static std::string null = "";
  if (slot >=0 && slot < 4) {
    return m_obName[slot];
  }
  return null;
}

void RodBocConnectivity::readConfig(DbRecord *r, int lev) { 
  if (r->getClassName() == "RODBOC") {
    storeRec(r);
    m_name = r->getName();
    // Read the config object
    m_config->read(r); 
    m_rolSName = "";
    if (m_rolName != "") {
      DbRecord *rl;
      try {
	rl = r->getDb()->DbFindRecordByName(m_rolName);
      }
      catch (const PixDBException&) {
	rl = NULL;
      }
      if (rl != NULL) {
	m_rolSName = rl->getName();
      }
    }
    if (m_linkMap != NULL && m_linkMapName != "") {
      if (m_linkMapSName != m_linkMap->name()) {
	m_linkMap = NULL;
      }
    }
    if (lev != 0) {
      lev--;
      // Create child OBs
      for(dbRecordIterator it = r->recordBegin(); it != r->recordEnd(); it++) {
	// Look for RodBoc records
	if ((*it)->getClassName() == "OPTOBOARD") {
	  std::string dname = it.srcSlotName();
	  int ix[3] = { -1, -1, -1 };
	  if (dname == "A") dname = "AA";
	  if (dname == "B") dname = "BB";
	  if (dname == "C") dname = "CC";
	  if (dname == "D") dname = "DD";
	  for (unsigned int i=0; i<dname.size(); i++) {
	    if (i<3) {
	      if (dname[i] == 'A') ix[i] = 0;
	      if (dname[i] == 'B') ix[i] = 1;
	      if (dname[i] == 'C') ix[i] = 2;
	      if (dname[i] == 'D') ix[i] = 3;
	    }
	  }
	  if (ix[0] >= 0 && ix[1] >=  0) {
	    OBConnectivity *ob = new OBConnectivity(*it);
	    addOB(ix[0], ix[1], ix[2], ob);
	  }
	}
      }
    }
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a RodBocConnectivity object");
  }
}

void RodBocConnectivity::writeConfig(DbRecord *r, int lev) { 
  if (r->getClassName() == "RODBOC") {
    storeRec(r);
    // Check if the link to the link map is up-to-date
    if (m_linkMap != NULL) {
      if (m_linkMap->decName() != "") {
	m_linkMapName = m_linkMap->decName();
	m_linkMapSName = m_linkMap->name();
      }
    }
    // Write the config object
    m_config->write(r);
    if (lev != 0) {
      lev--;
      // Create children
      int i;
      std::string lname[4] = { "A", "B","C", "D" }; 
      for(i = 0; i<4; i++) {
	if (m_ob[i] != NULL) {
	  std::string slotName;
	  slotName = lname[m_ob[i]->bocTxSlot()];
	  slotName += lname[m_ob[i]->bocRxSlot(0)];
	  if (m_ob[i]->bocRxSlot(1)>=0) slotName += lname[m_ob[i]->bocRxSlot(1)];
	  //if (slotName == "AA") slotName = "A";
	  //if (slotName == "BB") slotName = "B";
	  //if (slotName == "CC") slotName = "C";
	  //if (slotName == "DD") slotName = "D";
	  // Write OBs
	  DbRecord *sr;
	  try {
	    sr = r->addRecord("OPTOBOARD", slotName, m_obName[i]);
	  }
	  catch (const PixDBException&) {
	    dbRecordIterator ri;
	    if ((ri = r->findRecord(slotName)) == r->recordEnd()) {
	      throw ConnectivityExc(ConnectivityExc::ERROR,"Cannot create "+m_obName[i]+"/"+lname[i]+"/OPTOBOARD record");
	    }
	    sr = *ri;
	  }
	  m_ob[i]->writeConfig(sr);
	}
      }
    }
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a RodBocConnectivity object");
  }
}

