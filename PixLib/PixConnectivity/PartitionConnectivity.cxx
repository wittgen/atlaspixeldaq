///////////////t//////////////////////////////////////////////////////
// PartitionConnectivity.cxx
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 05/10/06  Version 1.0 (PM)
//           Initial release

//! Class for partition connectivity

#include "PixConnectivity/PartitionConnectivity.h"
#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixDbInterface/PixDbInterface.h"
#include "Config/Config.h"

using namespace SctPixelRod;
using namespace PixLib;

PartitionConnectivity::PartitionConnectivity(std::string name) : GenericConnectivityObject(name) {
  // Create the config object
  initConfig(); 
}

PartitionConnectivity::PartitionConnectivity(DbRecord *r, int lev) : GenericConnectivityObject(r) {
  if (r->getClassName() == "PARTITION") {
    // Read record from DB
    storeRec(r);
    // Create the config object
    initConfig();
    readConfig(getRec(), lev); 
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a PartitonConnectivity object");
  }
}

void PartitionConnectivity::initConfig() { 
  m_rodCrates.clear();
  m_config = new Config(m_name, "PARTITION");
  Config &conf = *m_config;

  conf.addGroup("Config");
  conf["Config"].addString("partConfig", m_partConfig, "", "Link to partition configuration", true);
  conf.addGroup("TDAQ");
  conf["TDAQ"].addString("daqPartName", m_daqPartName, "Partition_Pixel", "DAQ partition name", true);
  conf["TDAQ"].addString("ddcPartName", m_ddcPartName, "Partition_PixelDDC", "DDC partition name", true);
  conf["TDAQ"].addString("calibISServName", m_calibISServName, "RunParams", "Calbration IS server name", true);
  conf["TDAQ"].addString("ddcISServName", m_ddcISServName, "RunParams", "DDC IS server name", true);

  conf.reset();
}

void PartitionConnectivity::addCrate(std::string slot, RodCrateConnectivity *cr) {
  if (m_rodCrates.find(slot) != m_rodCrates.end()) {
    delete m_rodCrates[slot];
  }
  m_rodCrates[slot] = cr;
  cr->linkPartition(this);
}

void PartitionConnectivity::readConfig(DbRecord *r, int lev) { 
  if (r->getClassName() == "PARTITION") {
    storeRec(r);
    // Read the config object
    m_config->read(r); 
    if (lev != 0) {
      lev--;
      // Create child ROD crates
      for(dbRecordIterator it = r->recordBegin(); it != r->recordEnd(); it++) {
	// Look for FE inquires
	if ((*it)->getClassName() == "RODCRATE") {
	  std::string dname = it.srcSlotName(); 
	  std::size_t pos = dname.find("CRATE_");
	  if (pos != std::string::npos) {
	    RodCrateConnectivity *crate = new RodCrateConnectivity(*it, lev);
	    addCrate(dname, crate);
	  }
	}
      }
    }
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a PartitonConnectivity object");
  }
}

void PartitionConnectivity::writeConfig(DbRecord *r, int lev) { 
  if (r->getClassName() == "PARTITION") {
    storeRec(r);
    // Read the config object
    m_config->write(r); 
    if (lev != 0) {
      lev--;
      // Create child ROD crates
      std::map<std::string, RodCrateConnectivity*>::iterator it;
      for(it = m_rodCrates.begin(); it != m_rodCrates.end(); it++) {
	// Write RodCrate
	std::string name = ((*it).second)->name();
	std::cout <<  name << std::endl;
        DbRecord *sr;
	try {
	  sr = r->addRecord("RODCRATE", name);
	}
    catch (PixDBException&) {
	  dbRecordIterator ri;
	  if ((ri = r->findRecord(name)) == r->recordEnd()) {  
	    throw ConnectivityExc(ConnectivityExc::ERROR,"Cannot create "+name+"/RODCRATE record");
	  }
	  sr = *ri;
	}
	((*it).second)->writeConfig(sr, lev);
      }
    }
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a PartitonConnectivity object");
  }
}

