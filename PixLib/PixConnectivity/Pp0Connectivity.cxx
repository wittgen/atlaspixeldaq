/////////////////////////////////////////////////////////////////////
// Pp0Connectivity.cxx
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 05/10/06  Version 1.0 (PM)
//           Initial release

//! Class for PP0 crate connectivity

#include "PixConnectivity/Pp0Connectivity.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "PixConnectivity/RodBocConnectivity.h"
#include "PixConnectivity/OBConnectivity.h"
#include "PixDbInterface/PixDbInterface.h"

using namespace PixLib;

Pp0Connectivity::Pp0Connectivity(std::string name) : 
  GenericConnectivityObject(name), m_ob(NULL) {
  // Create the config object
  int i;
  for (i=0; i<8; i++) m_modules[i] = NULL;
  initConfig(); 
}

Pp0Connectivity::Pp0Connectivity(DbRecord *r, int lev) : 
  GenericConnectivityObject(r), m_ob(NULL) {
  int i;
  for (i=0; i<8; i++) m_modules[i] = NULL;
  if (r->getClassName() == "PP0") {
    // Read record from DB
    storeRec(r);
    // Create the config object
    initConfig();
    readConfig(getRec(), lev); 
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a Pp0Connectivity object");
  }
}

void Pp0Connectivity::addModule(int slot, ModuleConnectivity *mod) {
  if (slot >= 1 && slot <= 8) {
    if (m_modules[slot-1] != NULL) {
      delete m_modules[slot-1];
    }
    m_modules[slot-1] = mod;
    mod->linkPp0(this, slot);
  }
}

void Pp0Connectivity::linkOB(OBConnectivity *ob) {
  m_ob = ob;
  m_obName = ob->decName();
  m_obSName = ob->name();
}

ModuleConnectivity* Pp0Connectivity::modules(int slot) {
  if (slot >=1 && slot <= 8) {
    return m_modules[slot-1];
  }
  return NULL;
}

void Pp0Connectivity::initConfig() { 
  m_config = new Config(m_name, "PP0");
  Config &conf = *m_config;
  //conf.addGroup("Identifier");
  //conf["Identifier"].addString("Id", m_name, m_name, "Object identifier", true);
  conf.addGroup("Links");
  conf["Links"].addLink("OB", "PP0", m_obName, "", "Link to Optoboard record", true);
  conf["Links"].addString("CL", m_coolingLoop, "", "Cooling loop name", false);
  conf.reset();
  m_coolingLoop = "";
}

void Pp0Connectivity::readConfig(DbRecord *r, int lev) { 
  if (r->getClassName() == "PP0") {
    storeRec(r);
    // Read the config object
    m_config->read(r);
    if (m_ob != NULL && m_obName != "") {
      if (m_obSName != m_ob->name()) {
	m_ob = NULL;
      }
    }
    if (lev != 0) {
      lev--;
      // Create child Modules
      for(dbRecordIterator it = r->recordBegin(); it != r->recordEnd(); it++) {
	// Look for Module records
	if ((*it)->getClassName() == "MODULE") {
	  std::string slnam = it.srcSlotName();
	  if (slnam[0] == 'M') slnam = slnam.substr(1);
	  std::istringstream snum(slnam);
	  int islot = -1;
	  snum >> islot;
	  if (islot >= 0) {
	    ModuleConnectivity *mod = new ModuleConnectivity(*it);
	    if (islot >= 1 && islot <= 8) {
	      addModule(islot, mod);
	    }
	  }
	}
	if ((*it)->getClassName() == "COOLING-LOOP") {
	  m_coolingLoop = (*it)->getName();
	}
      } 
    }
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a Pp0Connectivity object");
  }
}

void Pp0Connectivity::writeConfig(DbRecord *r, int lev) { 
  if (r->getClassName() == "PP0") {
    storeRec(r);
    // Check if the links are up to date
    if (m_ob != NULL) {
      if (m_ob->decName() != "") {
	m_obName = m_ob->decName();
	m_obSName = m_ob->name();
      }
    }
    // Write the config object
    m_config->write(r); 
    if (lev != 0) {
      lev--;
      // Create child Modules;
      int i;
      for (i=0; i<8; i++) {
	// Write Module
	std::ostringstream lname;
        lname << "M" << i+1;
	if (m_modules[i] != NULL) {
	  DbRecord *sr;
	  try {
	    sr = r->addRecord("MODULE", lname.str(), m_modules[i]->name());
	  }	
      catch (PixDBException&) {
	    dbRecordIterator ri;
	    if ((ri = r->findRecord(lname.str())) == r->recordEnd()) {  
	      throw ConnectivityExc(ConnectivityExc::ERROR,"Cannot create "+m_modules[i]->name()+"/"+lname.str()+"/MODULE record");
	    }
	    sr = *ri;
	  }
	  m_modules[i]->writeConfig(sr);
	}
      }
    }
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a Pp0Connectivity object");
  }
}


