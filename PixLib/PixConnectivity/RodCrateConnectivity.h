/////////////////////////////////////////////////////////////////////
// RodCrateConnectivity.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 05/10/06  Version 1.0 (PM)
//           Initial release

//! Class for ROD crate connectivity

#ifndef _PIXLIB_RODCRCONNOBJ
#define _PIXLIB_RODCRCONNOBJ

#include "PixConnectivity/GenericConnectivityObject.h"

#include <map>

namespace PixLib {

class RodBocConnectivity;
class TimConnectivity;
class PartitionConnectivity;
class SbcConnectivity;

class RodCrateConnectivity : public GenericConnectivityObject {
public:
  //! Payload accessors
  std::string &histoServName() { return m_histoServName; };
  //! Enable parameters
  bool enableReadout;
  //! Down links
  std::map<int, RodBocConnectivity*> &rodBocs() { return m_rodBocs; };
  void addRodBoc(int slot, RodBocConnectivity *rod);
  TimConnectivity* tim() { return m_tim; };
  int timSlot() { return m_timSlot; };
  void addTim(int slot, TimConnectivity *tim);
  SbcConnectivity* sbc() { return m_sbc; };
  int sbcSlot() { return m_sbcSlot; };
  void addSbc(int slot, SbcConnectivity *sbc);
  //! Up link
  PartitionConnectivity *partition() { return m_partition; };
  void linkPartition(PartitionConnectivity *part);
  //! Soft links
  //! Internal connectivity tables
  //! Constructors
  RodCrateConnectivity(std::string name);
  RodCrateConnectivity(DbRecord *r, int lev);
  //! Config handling
  void initConfig();
  virtual void readConfig(DbRecord *r, int lev);
  virtual void writeConfig(DbRecord *r, int lev);
  //! Link to configuration
private:
  TimConnectivity *m_tim;
  int m_timSlot;
  SbcConnectivity *m_sbc;
  int m_sbcSlot;
  std::map<int, RodBocConnectivity*> m_rodBocs;
  PartitionConnectivity *m_partition;
  std::string m_histoServName;
};

}

#endif
