/////////////////////////////////////////////////////////////////////
// SbcConnectivity.cxx
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 05/10/06  Version 1.0 (PM)
//           Initial release

//! Class for SBC connectivity

#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixConnectivity/SbcConnectivity.h"
#include "PixDbInterface/PixDbInterface.h"
#include "Config/Config.h"

using namespace PixLib;

SbcConnectivity::SbcConnectivity(std::string name) : 
  GenericConnectivityObject(name), m_crate(NULL) {
  // Create the config object
  initConfig(); 
}

SbcConnectivity::SbcConnectivity(DbRecord *r, int lev) : 
  GenericConnectivityObject(r), m_crate(NULL) {
  if (r->getClassName() == "SBC") {
    // Read record from DB
    storeRec(r);
    // Create the config object
    initConfig();
    readConfig(getRec(), lev); 
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a SbcConnectivity object");
  }
}

void SbcConnectivity::initConfig() { 
  m_config = new Config(m_name, "SBC");
  Config &conf = *m_config;

  conf.addGroup("Network");
  conf["Network"].addString("ipName", m_ipName, "pixrcc00.pixel.cern.ch", "SBC dns name eth1", true);
  conf["Network"].addString("ipAddr", m_ipAddr, "0.0.0.0", "SBC IP address eth1", true);
  conf["Network"].addString("hwAddr", m_hwAddr, "00:00:00:00:00:00", "SBC MAC address eth1", true);
  conf["Network"].addString("ipName1", m_ipName1, "", "SBC dns name eth2", true);
  conf["Network"].addString("ipAddr1", m_ipAddr1, "0.0.0.0", "SBC IP address eth2", true);
  conf["Network"].addString("hwAddr1", m_hwAddr1, "00:00:00:00:00:00", "SBC MAC address eth2", true);
  conf.reset();
}

void SbcConnectivity::readConfig(DbRecord *r, int lev) { 
  if (r->getClassName() == "SBC") {
    storeRec(r);
    m_name = r->getName();
    // Read the config object
    m_config->read(r); 
    lev = 0;
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a SbcConnectivity object");
  }
}

void SbcConnectivity::writeConfig(DbRecord *r, int lev) { 
  if (r->getClassName() == "SBC") {
    storeRec(r);
    // Read the config object
    m_config->write(r);
    lev = 0;
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a SbcConnectivity object");
  }
}

void SbcConnectivity::linkCrate(RodCrateConnectivity *cr, int slot) {
  m_crate = cr;
  m_vmeSlot = slot;
}

