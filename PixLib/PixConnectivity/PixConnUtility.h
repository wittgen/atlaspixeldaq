#ifndef __PixConnUtility__
#define __PixConnUtility__

#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixConnectivity/RodBocConnectivity.h"
#include "PixConnectivity/OBConnectivity.h"
#include "PixConnectivity/Pp0Connectivity.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "PixConnectivity/PixConnectivity.h"

#include "ipc/partition.h"
#include "is/infodictionary.h"

namespace PixLib {
namespace ConnUtility {

struct ModuleInfo {
  std::string prodID;
  int txCh = -1;
  int rxCh1 = -1;
  int rxCh2 = -1;
  int DCI = -1;
  int DTO1 = -1;
  int DTO2 = -1;
  int isFeI4 = 0;
};

struct DBTags {
  std::string idTag;
  std::string connTag;
  std::string cfgTag;
  std::string modTag;
};

DBTags getDBTagsFromIS(IPCPartition& partition) {
  auto isDict = ISInfoDictionary( partition ); 
  auto result = DBTags{};
  auto readFromIS = [&isDict](std::string const & s, auto & val){
    ISInfoT<std::remove_reference_t<decltype(val)>> IStype;
    isDict.getValue(s, IStype);
    val = IStype.getValue();
  };

  try {
    readFromIS("PixelRunParams.DataTakingConfig-IdTag", result.idTag);
    readFromIS("PixelRunParams.DataTakingConfig-ConnTag", result.connTag);
    readFromIS("PixelRunParams.DataTakingConfig-CfgTag", result.cfgTag);
    readFromIS("PixelRunParams.DataTakingConfig-ModCfgTag", result.modTag);
  } catch (...) {
    throw std::runtime_error("Database tags not published in IS");
  }

  return result;
}

std::string getDisableFromIS(IPCPartition& partition) {
  auto isDict = ISInfoDictionary( partition ); 
  auto readFromIS = [&isDict](std::string const & s, auto & val){
    ISInfoT<std::remove_reference_t<decltype(val)>> IStype;
    isDict.getValue(s, IStype);
    val = IStype.getValue();
  };
  std::string disable;
  try {
    readFromIS("PixelRunParams.DataTakingConfig-Disable", disable);
  } catch (...) {
    throw std::runtime_error("Disable not published in IS");
  }
  return disable;
}

auto getRODsFromConn(std::vector<std::string> const & rods, PixConnectivity& conn) {
  //map["CRATE"]["ROD"]["PP0"]["Module"]
  std::map<std::string,std::map<std::string,std::map<std::string,std::map<std::string,ModuleInfo>>>> result; 
  for(auto const& rod: rods){
    auto rodConn = conn.rods[rod];
    auto linkMap = rodConn->linkMap();
    auto crate = rodConn->crate();
    for(auto ob: rodConn->getOBs() ) {
      if(ob){
        auto pp0 = ob->pp0();
        for(auto mod: pp0->getModules()){
          if(mod){
            auto& modInfo = result[crate->name()][rodConn->name()][ob->pp0SName()][mod->name()];
            modInfo.prodID = mod->prodId();;
            modInfo.DCI = mod->bocLinkTx();
            modInfo.DTO1 = mod->bocLinkRx(1);
            modInfo.DTO2 = mod->bocLinkRx(2);
	    modInfo.txCh = linkMap->rodTx[modInfo.DCI/10][modInfo.DCI%10];
            modInfo.rxCh1 = linkMap->rodRx40[modInfo.DTO1/10][modInfo.DTO1%10];
            modInfo.rxCh2 = linkMap->rodRx40[modInfo.DTO2/10][modInfo.DTO2%10];
            if(rodConn->rodBocType()=="CPPROD") modInfo.isFeI4 = 1;
          }
        }
      } 
    }
  }
  return result;
}

}} //namespaces
#endif
