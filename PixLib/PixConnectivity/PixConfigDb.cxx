/////////////////////////////////////////////////////////////////////
// PixConfigDb.cxx
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 27/09/14  Version 1.0 (PM)
//           Initial release

//! Helpers for configuration db accss

#include "PixConnectivity/PixConfigDb.h"
#include "PixConnectivity/PixConnectivity.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "ConfigRootIO/ConfigStoreFactory.h"
#include "ConfigRootIO/ConfigStore.h"
#include "CoralDB/Connection.h"
#include "CoralDB/Alias.h"
#include "CoralDB/Encodable.h"

#include "TFile.h"

using namespace PixLib;

PixConfigDb::PixConfigDb(PixConnectivity *conn) {
  std::string tagCF = "Base";
  std::string tagID = "Base";
  m_dbServerModeCfg = false;
  m_dbServerModeCfgWr = false;
  m_dbServer = NULL;
  try{
    init(conn, tagCF, tagID);
  }catch(...){
    std::cerr << "PixConfigDb::PixConfigDb(PixConnectivity *conn): Default Base tag no available in DB - will continue nevertheless." << std::endl;
  }
}

PixConfigDb::~PixConfigDb() {
  if (m_cfgDb != NULL) delete m_cfgDb;
  m_mutex->destroy();
}

void PixConfigDb::init(PixConnectivity *conn, std::string tagCF, std::string tagID) {
  if (conn == NULL) {
    m_haveConnectivity = false;
    m_conn = NULL;
  } else {
    m_haveConnectivity = true;
    m_conn = conn;
  }

  // Create mutex
  m_mutex = PixMutex::create("PixConfigDb_Mutex");

  // Read env variables
  char *fname;
  fname = getenv("PIX_CFG_DB");
  if (fname == NULL) {
    throw(PixConfigDbExc(PixException::FATAL, "CFGDBNOTDEF", "PIX_CFG_DB env variable not defined"));
  }
  m_cfgName = fname;
  fname = getenv("PIX_CFG_PREF");
  if (fname == NULL) {
    throw(PixConfigDbExc(PixException::FATAL, "CFGPREFNOTDEF", "PIX_CFG_PREF env variable not defined"));
  }
  m_cfgPref = fname;

  // Open DB
  m_cfgDb = NULL;
  if (m_dbServerModeCfg) {
  } else {
    openCfg(tagCF, tagID);
  }

}

void PixConfigDb::openCfg(std::string tagCF, std::string tagID) {

  PixLock lock(m_mutex);
  
  time_t t = time(NULL);

  if (m_types.size() == 0) {
    m_types["PixModule"] = "MODULE_CFG";
    m_types["PixModuleGroup"] = "RODBOC_CFG";
    m_types["TimPixTrigController"] = "TIM_CFG";
    m_types["PixDisable"] = "DISABLE_CFG";
    m_types["PixRunConfig"] = "RUNCONF_CFG";
    m_types["PixScan"] = "SCAN_CFG";
  }

  tagID = tagID + "-CFG";
  std::string tag = tagID + "/" + tagCF;

  if (m_cfgDb == NULL) {
    try {
      m_cfgDb = new CoralDB::CoralDB(m_cfgName, 
				     coral::Update,
				     coral::Error,
				     false);
      m_cfgDb->setObjectDictionaryTag(tagID);
      m_cfgDb->setConnectivityTag(tagCF);
      m_cfgDb->setDataTag(tagCF);
      m_cfgDb->setAliasTag(tagCF);
    }
    catch (...) {
      m_cfgDb = NULL;
      throw(PixConfigDbExc(PixException::FATAL, "CANNOTOPEN", "Cannot open configuration db"));
    }
  }

  bool load_ot = false;
  if (m_odTime.find(tagID) == m_odTime.end()) {
    load_ot = true; 
  } else {
    if (t - m_odTime[tagID] > 1200) load_ot = true;
  }
  if (load_ot) {
    try {
      m_cfgDb->setObjectDictionaryTag(tagID);
      m_od[tagID].clear();
      CoralDB::CompoundTag ctoi;
      ctoi.setObjectDictionaryTag(tagID);
      m_cfgDb->getObjectDictionary(m_od[tagID], ctoi);
      m_odTime[tagID] = t;
    }
    catch (...) {
      throw(PixConfigDbExc(PixException::ERROR, "IDNOTFOUND", "Cannot read object table for id tag "+tagID));
    }
  }

  bool load_ct = false;
  if (m_ctTime.find(tagCF) == m_ctTime.end()) {
    load_ct = true; 
  } else {
    if (t - m_ctTime[tagCF] > 1200) load_ct = true;
  }
  if (load_ct) {
    try {
      m_cfgDb->setConnectivityTag(tagCF);
      m_cfgDb->setDataTag(tagCF);
      m_cfgDb->setAliasTag(tagCF);
      m_ct[tagCF].clear();
      CoralDB::CompoundTag ctag(tagID, tagCF, tagCF, tagCF);
      m_cfgDb->getConnectionTable(m_ct[tagCF]);
      std::sort(m_ct[tagCF].begin(), m_ct[tagCF].end());
      m_ctTime[tagCF] = t;
    }
    catch (...) {
      throw(PixConfigDbExc(PixException::FATAL, "TAGNOTFOUND", "Cannot read configuration table for tag "+tagID+"/"+tagCF));
    }
  }

  //std::cout << "+++  Start internal table construction" << std::endl;

  if (load_ct || load_ot) {
    m_objects[tag].clear();
    m_revisions[tag].clear();
    for(CoralDB::ConnectionTableMap::const_iterator i = m_ct[tagCF].begin(); i != m_ct[tagCF].end(); i++) {
      //i->print(cout);
      std::string object = i->fromId();
      std::string file = i->toId();
      if (m_objects[tag].find(object) == m_objects[tag].end()) {
	if (m_od[tagID].find(object) != m_od[tagID].end()) {
	  m_objects[tag][object] = m_od[tagID][object]; 
	}
      }
      if (m_objects[tag].find(object) != m_objects[tag].end()) {
	if (m_od[tagID].find(file) != m_od[tagID].end()) {
	  std::string revid = m_od[tagID][file];
	  CfgRevision rev(revid, file);
	  m_revisions[tag][object][m_od[tagID][file]] = rev;
	}
      }
    }
  }

  // for(std::map<std::string, std::string>::const_iterator i = m_objects[tag].begin(); i != m_objects[tag].end(); i++) {
  //   std::cout << "== " << i->first << " - " << i->second << std::endl;
  //   std::string object = i->first;
  //   for(std::map<std::string, CfgRevision>::const_iterator j = m_revisions[tag][object].begin(); j != m_revisions[tag][object].end(); j++) {
  //     std::cout << j->first << " - " << (j->second).fileName() << " - " << (j->second).time() << " - " << (j->second).cycle() << std::endl;
  //   }
  // }
}

void PixConfigDb::openDbServer(PixDbServerInterface *DbServer) {
  m_dbServer = DbServer;
  if (DbServer != NULL) {
    m_dbServerModeCfg = true;
    m_dbServerModeCfgWr = true;
  } else {
    m_dbServerModeCfg = false;
    m_dbServerModeCfgWr = false;
  }
}

PixDbInterface*  PixConfigDb::openDb(std::string name, std::string mode, PixDbCompoundTag t) {
  int nTry = 0;
  PixDbInterface *db = NULL;
  while (nTry < 5) {
    try {
      PixDbInterfaceFactory fact(name, AutoCommit(false)); 
      if (mode == "READ") {
	db= fact.openDB_readonly(t);
      } else if (mode == "UPDATE") {
	db= fact.openDB_update(t);
      } else if (mode == "RECREATE") {
	db= fact.openDB_recreate(t, "ROOT");
      }
    }
    catch(...) {
      nTry++;
      db = NULL;
      if (nTry >= 5)  throw(PixConfigDbExc(PixException::ERROR, "CFGFILEOPENFAIL", "Cannot open config file "+name));
      sleep(2);
    }
    if (db != NULL) break;
  }
  return db;
}

void PixConfigDb::addConnectivity(PixConnectivity *conn) {
  if (conn == NULL) {
    m_haveConnectivity = false;
    m_conn = NULL;
  } else {
    m_haveConnectivity = true;
    m_conn = conn;
  }
}

void PixConfigDb::setCfgTag(std::string tagCF, std::string tagID) {
  if (m_dbServerModeCfg) {
    if (m_dbServer == NULL)  throw(PixConfigDbExc(PixException::ERROR, "DBSERVNOTDEF", "DB server not defined"));
    std::vector<std::string> tags;
    tags = listCfgTags(tagID);
    bool found = false;
    for (std::string s : tags) { if (s == tagCF) found = true; }
    if (!found) {
      throw(PixConfigDbExc(PixException::ERROR, "TAGNOTFOUND", "Cannot read configuration table for tag "+tagID+"/"+tagCF));
    }
  } else {
    openCfg(tagCF, tagID);
  }
}

bool PixConfigDb::checkCfgTag(std::string tagCF, std::string tagID) {
  time_t t = time(NULL);

  if (m_dbServerModeCfg) {
    if (m_dbServer == NULL)  throw(PixConfigDbExc(PixException::ERROR, "DBSERVNOTDEF", "DB server not defined"));
    std::vector<std::string> tags;
    tags = listCfgTags(tagID);
    bool found = false;
    for (std::string s : tags) { if (s == tagCF) found = true; }
    if (!found) {
      throw(PixConfigDbExc(PixException::ERROR, "TAGNOTFOUND", "Cannot read configuration table for tag "+tagID+"/"+tagCF));
      return false;
    }
    return true;
  } else {
    bool ot_ok = true;
    tagID = tagID + "-CFG";
    if (m_odTime.find(tagID) == m_odTime.end()) {
      ot_ok = false;
    } else {
      if (t - m_odTime[tagID] > 1200) {
        ot_ok = false;
      }
    }
    bool ct_ok = true;
    if (m_ctTime.find(tagCF) == m_ctTime.end()) {
      ct_ok = false; 
    } else {
      if (t - m_ctTime[tagCF] > 1200) {
        ct_ok = false;
      }
    }
    return ct_ok && ot_ok;
  }
}

std::vector<std::string> PixConfigDb::listObjTags() {
  std::vector<std::string> tags,allTags;
  if (m_dbServerModeCfg) {
    if (m_dbServer == NULL)  throw(PixConfigDbExc(PixException::ERROR, "DBSERVNOTDEF", "DB server not defined"));
    m_dbServer->listDomainRem(allTags);
    for (std::string s : allTags) {
      if (s.size() > 14) {
        if (s.substr(0,14) == "Configuration-") {
          tags.push_back(s.substr(14,s.size()-14));
	}
      }
    }
  } else {
    CoralDB::IdTagList idTags;
    {
      PixLock lock(m_mutex);
      m_cfgDb->getExistingObjectDictionaryTags(idTags);
    }
    for(CoralDB::TagList::const_iterator it=idTags.begin(); it!=idTags.end(); it++) {
      std::string tag = it->tag();
      if (tag.size() > 4) {
	if (tag.substr(tag.size()-4,4) == "-CFG") {
	  tags.push_back(tag.substr(0,tag.size()-4));
	}
      }
    }
  }
  return tags;
}

std::vector<std::string> PixConfigDb::listCfgTags(std::string tagID) {
  std::vector<std::string> tags;
  if (m_dbServerModeCfg) {
    if (m_dbServer == NULL)  throw(PixConfigDbExc(PixException::ERROR, "DBSERVNOTDEF", "DB server not defined"));
    m_dbServer->listTagsRem("Configuration-"+tagID, tags);
  } else {
    CoralDB::TagList connectivityTags;
    {
      PixLock lock(m_mutex);
      m_cfgDb->getExistingConnectivityTags(connectivityTags, tagID+"-CFG");
    }
    for (CoralDB::TagList::const_iterator it=connectivityTags.begin(); it!=connectivityTags.end(); it++) {
      tags.push_back(it->tag());
    }
  }
  return tags;
}

std::vector<std::string> PixConfigDb::listObjects(std::string tagCF, std::string tagID) {
  std::vector<std::string> objects, allObjs;
  setCfgTag(tagCF, tagID);
  if (m_dbServerModeCfg) {
    if (m_dbServer == NULL)  throw(PixConfigDbExc(PixException::ERROR, "DBSERVNOTDEF", "DB server not defined"));
    m_dbServer->listObjectsRem("Configuration-"+tagID, tagCF, allObjs);    
    for (std::string s : allObjs) {
      if (s.find('|') == std::string::npos) {
        objects.push_back(s);
      }
    }
  } else {
    for (auto obj : m_objects[tagID+"-CFG/"+tagCF]) {
      if (checkObjectInConnectivity(obj.first, tagCF, tagID)) {
	objects.push_back(obj.first);
      }
    }
  }
  return objects;
}

bool PixConfigDb::checkObjectInConnectivity(std::string object, std::string tagCF, std::string tagID) {
  std::string tag = tagID+"-CFG/"+tagCF;
  std::string type = getObjectType(object, tagCF, tagID);
  if (type != "" && type != "--") {
    if (m_haveConnectivity) {
      if (type == "MODULE_CFG") {
	if (m_conn->mods.find(object) != m_conn->mods.end()) return true;
	for( std::map<std::string, ModuleConnectivity*>::iterator mit=m_conn->mods.begin();  mit!= m_conn->mods.end(); mit++){
	  if (mit->second->prodId()==object) return true;
	}
	return false;
      } else if (type == "RODBOC_CFG") {
	if (m_conn->rods.find(object) == m_conn->rods.end()) return false;
      } else if (type == "TIM_CFG") {
	if (m_conn->tims.find(object) == m_conn->tims.end()) return false;
      }
    }
    return true;
  }
  return false;
}

std::string PixConfigDb::getObjectType(std::string object, std::string tagCF, std::string tagID) {
  std::vector<std::string> objects, allObjs, types;
  setCfgTag(tagCF, tagID);
  if (m_dbServerModeCfg) {
    if (m_dbServer == NULL)  throw(PixConfigDbExc(PixException::ERROR, "DBSERVNOTDEF", "DB server not defined"));
    m_dbServer->listObjectsRem("Configuration-"+tagID, tagCF, "", allObjs, types);    
    for (unsigned int i=0; i<allObjs.size(); i++) {
      if (allObjs[i] == object) {
	std::string type = types[i];
	if (m_types.find(type) != m_types.end()) {
	  type = m_types[type];
	}
	return type;
      }
    }
  } else {
    std::string tag = tagID+"-CFG/"+tagCF;
    if (m_objects[tag].find(object) != m_objects[tag].end()) {
      return m_objects[tag][object];
    }
  }
  return "--";
}

std::vector<unsigned int> PixConfigDb::listRevisions(std::string object, std::string tagCF, std::string tagID) {
  std::vector<unsigned int> revisions;
  setCfgTag(tagCF, tagID);
  if (m_dbServerModeCfg) {
    if (m_dbServer == NULL)  throw(PixConfigDbExc(PixException::ERROR, "DBSERVNOTDEF", "DB server not defined"));
    m_dbServer->listRevisionsRem("Configuration-"+tagID, tagCF, object, revisions);        
  } else {
    std::string tag = tagID + "-CFG/" + tagCF;
    if (m_revisions[tag].find(object) != m_revisions[tag].end()) {
      if (checkObjectInConnectivity(object, tagCF, tagID)) {
	for (auto rev : m_revisions[tag][object]) {
	  revisions.push_back(rev.second.time());  
	}
      }
    }
  }
  if (revisions.size() == 0) {
    throw(PixConfigDbExc(PixException::ERROR, "OBJNOTFOUND", "Cannot find object "+object+" in "+tagID+"/"+tagCF));
  }
  return revisions;
}

std::vector<unsigned int> PixConfigDb::listCycles(std::string object, unsigned int rev, std::string tagCF, std::string tagID) {
  std::vector<unsigned int> cycles;
  setCfgTag(tagCF, tagID);
  if (m_dbServerModeCfg) {
    if (m_dbServer == NULL)  throw(PixConfigDbExc(PixException::ERROR, "DBSERVNOTDEF", "DB server not defined"));
    std::vector<unsigned int> revisions;
    m_dbServer->listRevisionsRem("Configuration-"+tagID, tagCF, object, revisions);        
    if (revisions.size() > 0) {
      cycles.push_back(0);
    }
  } else { 
    std::string tag = tagID + "-CFG/" + tagCF;
    if (m_revisions[tag].find(object) != m_revisions[tag].end()) {
      if (checkObjectInConnectivity(object, tagCF, tagID)) {
	for (auto srev : m_revisions[tag][object]) {
	  if (rev == srev.second.time()) {  
	    cycles.push_back(srev.second.cycle());
	  }
	}
      }
    }
  }
  if (cycles.size() == 0) {
    throw(PixConfigDbExc(PixException::ERROR, "OBJNOTFOUND", "Cannot find object "+object+" in "+tagID+"/"+tagCF));
  }
  return cycles;
}

void PixConfigDb::createCfgTag(std::string tagCF, std::string tagID) {
  /*
  if (m_dbServerModeCfg) {
    if (m_dbServer == NULL)  throw(PixConfigDbExc(PixException::ERROR, "DBSERVNOTDEF", "DB server not defined"));
  } else {
    PixLock lock(m_mutex);
    try {
      m_mutex->lock();
      m_cfgDb->transactionStart();
      m_cfgDb->copyConnectivityTagAndSwitch(tag);
      m_cfgDb->transactionCommit();
      m_cfgDb->transactionStart();
      m_cfgDb->copyDataTagAndSwitch(tag);
      m_cfgDb->transactionCommit();
      m_cfgDb->transactionStart();
      m_cfgDb->copyAliasTagAndSwitch(tag);
      m_cfgDb->transactionCommit();
      if (m_tagCF == m_tagModCF) m_tagModCF = tag;
      m_tagCF = tag;  
      openCfg();
    }
    catch (...) {
      throw(PixConfigDbExc(PixException::ERROR, "Cannot create configuration DB tags"));
    }
  }
  */
}

bool PixConfigDb::checkCfg(Config *cfg, std::string object, unsigned int &rev, std::string tagCF, std::string tagID) { //OK
  std::vector<unsigned int> lRev = listRevisions(object, tagCF, tagID);
  unsigned int bestRev = 0;
  for (unsigned int ir : lRev) {
    if (ir<=rev && ir>bestRev) {
      bestRev = ir;
    }
  }
  if (cfg->tag() != tagCF || cfg->rev() != bestRev) {
    rev = bestRev;
    return false;
  } 
  return true;
} 


void PixConfigDb::writeCfg(Config *cfg, std::string object, std::string tagCF, std::string tagID) {
  unsigned int rev = 0;
  writeCfg(cfg, object, rev, tagCF, tagID, "UNK");
}

void PixConfigDb::writeCfg(Config *cfg, std::string object, unsigned int &rev, std::string tagCF, std::string tagID) {
  writeCfg(cfg, object, rev, tagCF, tagID, "UNK");
}

void PixConfigDb::writeCfg(Config *cfg, std::string object, unsigned int &rev, std::string tagCF, std::string tagID, std::string tagPEND) {
  if (checkObjectInConnectivity(object, tagCF, tagID)) {
    if (rev == 0) rev = time(NULL);
    std::string type = getObjectType(object, tagCF, tagID); 
    if (m_dbServerModeCfgWr) {
      if (m_dbServer == NULL)  throw(PixConfigDbExc(PixException::ERROR, "DBSERVNOTDEF", "DB server not defined"));
      bool res;
      try {
        // Use the class name as type for the DbServer
	for (auto t : m_types) {
	  if (t.second == type) type = t.first;
	}
	res=cfg->write(m_dbServer, "Configuration-"+tagID, tagCF, object, type, tagPEND, rev);
      }  
      catch (...) {
	throw(PixConfigDbExc(PixException::ERROR, "CANTWRITEONSERVER", "Cannot write Config "+object+"/"+type+" in the DbServer"));
      }
      if (!res) throw(PixConfigDbExc(PixException::ERROR, "CANTWRITEONSERVER", "Cannot write Config "+object+"/"+type+" in the DbServer"));
    } else {
      setCfgTag(tagCF, tagID);
    }
  }
}

void PixConfigDb::readCfg(Config *cfg, std::string object, unsigned int &rev, unsigned int &cycle, std::string tagCF, std::string tagID) { //OK
  setCfgTag(tagCF, tagID);
  if (checkObjectInConnectivity(object, tagCF, tagID)) {
    if (rev == 0) rev = time(NULL);
    std::vector<unsigned int> lRev = listRevisions(object, tagCF, tagID);
    unsigned int bestRev = 0;
    for (unsigned int ir : lRev) {
      if (ir <= rev && ir > bestRev) {
        bestRev = ir;
      } 
    }
    std::vector<unsigned int> lCy = listCycles(object, bestRev, tagCF, tagID);
    unsigned int bestCy = 0;
    for (unsigned int ic : lCy) {
      if (ic <= cycle && ic > bestCy) {
        bestCy = ic;
      } 
    }
    std::string type = getObjectType(object, tagCF, tagID); 

    if (cfg->tag() != tagCF || cfg->rev() != bestRev) {
      if (m_dbServerModeCfg) {
	if (m_dbServer == NULL)  throw(PixConfigDbExc(PixException::ERROR, "DBSERVNOTDEF", "DB server not defined"));
	bool res;
	try {         
	  res = cfg->read(m_dbServer, "Configuration-"+tagID, tagCF, object, bestRev); 
	}
	catch (...) {
	  throw(PixConfigDbExc(PixException::ERROR, "CFGRECNOTFOUND", "Config record "+object+" not found"));
	} 
	if (!res) throw(PixConfigDbExc(PixException::ERROR, "CFGRECNOTFOUND", "Config record "+object+" not found"));
      } else {
	std::string tag = tagID + "-CFG/" + tagCF;
	std::ostringstream of;
	of << std::setw(12) << std::setfill('0') << bestRev;
	of << std::setw(3)  << std::setfill('0') << bestCy;
	std::string fName = m_revisions[tag][object][of.str()].fileName();
	if (fName != "") {
	  bool newBE = true;
	  if (type == "MODULE_CFG") {
	    try {
	      PixLock lock(m_mutex);
	      std::unique_ptr<RootConf::IConfigStore> store(RootConf::ConfigStoreFactory::configStore());
	      store->read(*cfg, fName, "Def", 0);
	      return;  
	    } catch(...) {
	      newBE=false;
	    }
	  }
	  if (type != "MODULE_CFG" || !newBE) {
	    PixLock lock(m_mutex);

	    std::string pref = "";
	    if (fName.substr(fName.size()-4) == ".sql") {
	      pref = "sqlite_file:";
	    } 
            PixDbInterface* dbs = NULL;
	    DbRecord *rootRec = NULL;
            try {
	      dbs = openDb(pref+fName, "READ", PixDbCompoundTag("BASE", "BASE", "BASE", "BASE"));
              rootRec = dbs->readRootRecord();
            } 
            catch (...) {
	      throw(PixConfigDbExc(PixException::ERROR, "CFGFILEOPENFAIL", "Cannot open config file "+fName));
	    }
	    if (rootRec == NULL) {
	      throw(PixConfigDbExc(PixException::ERROR, "CFGFILEOPENFAIL", "Cannot open config file "+fName));
	    }	
            DbRecord *rec = NULL; 	
            for (dbRecordIterator it = rootRec->recordBegin(); it != rootRec->recordEnd(); ++it) {
	      if ((*it)->getName() == object) {
	        rec = (*it);
	      }
	    }
	    if (rec != NULL) {
	      cfg->read(rec);
	      cfg->tag(tagCF);
	      cfg->rev(bestRev);
              delete rec;
              delete rootRec;
              delete dbs;
              return;
	    } else {
	      throw(PixConfigDbExc(PixException::ERROR, "CFGRECNOTFOUND", "Config record "+object+" not found"));
	    }
	  }
	}
      }
    }
    return;
  } 
  throw(PixConfigDbExc(PixException::ERROR, "OBJNOTFOUND", "Cannot find object "+object+" in "+tagID+"/"+tagCF));
}

void PixConfigDb::readCfg(Config *cfg, std::string lobj, unsigned int &rev, std::string tagCF, std::string tagID) {
  unsigned int cycle = 0xffffffff;
  readCfg(cfg, lobj, rev, cycle, tagCF, tagID);
}

void PixConfigDb::readCfg(Config *cfg, std::string lobj, std::string tagCF, std::string tagID) {
  unsigned int rev = 0xffffffff;
  unsigned int cycle = 0xffffffff;
  readCfg(cfg, lobj, rev, cycle, tagCF, tagID);
}


/*
DbRecord* PixConfigDb::getCfgFileRecord(std::string lobj, unsigned int &cfgRev, unsigned int &cfgRep) { //OK
  PixDbInterface* &db = m_cfgDb;
  std::string tag = m_tagCF;
  unsigned int rev = m_revCF;
  bool module = isModule(lobj);
  if (module) {
    db = m_cfgModDb;
    tag = m_tagModCF;
    rev = m_revModCF; 
  }
  std::string xobj = element(lobj, 0);
  std::string fName = getCfgFileName(lobj, cfgRev, cfgRep);
  if (fName != "") {
    if (m_dbrs.find(fName) != m_dbrs.end()) {
      return m_dbrs[fName];
    } else {
      m_mutex->lock();
      if (m_dbs.find(fName) == m_dbs.end()) {
	std::string pref = "";
        if (fName.substr(fName.size()-4) == ".sql") {
	  pref = "sqlite_file:";
	} 
	m_dbs[fName] = openDb(pref+fName, "READ", PixDbCompoundTag("BASE", "BASE", "BASE", "BASE"));
      }
      DbRecord *rootRec = m_dbs[fName]->readRootRecord(); 	
      for (dbRecordIterator it = rootRec->recordBegin(); it != rootRec->recordEnd(); ++it) {
	if ((*it)->getName() == xobj) {
	  m_dbrs[fName] = (*it);
	}
      }
      m_mutex->unlock();
      if (m_dbrs.find(fName) != m_dbrs.end()) {
	return m_dbrs[fName];
      }
    }
  } 
  return NULL;
}

DbRecord * PixConfigDb::getSubRec(DbRecord *r, std::string lobj, int el) { //OK
  if (element(lobj, el+1) == "") {
    return r;
  } else { 
    for (dbRecordIterator it = r->recordBegin(); it != r->recordEnd(); ++it) {
      if ((*it)->getName() == element(lobj, el+1)) {
	el++;
	return getSubRec(*it, lobj, el);
      }
    }
    return NULL;
  }
}
*/

std::string PixConfigDb::element(std::string str, int num, char sep) {
  unsigned int ip;
  int start = -1,end = -1;
  int nel = -1;
  bool spc = true;
  bool quote = false;
                                                                                                                                        
  for (ip=0;ip<str.size();ip++) {
    if (str[ip] == '"') quote = !quote;
    if (str[ip] == sep || (sep == 0 && 
       (str[ip] == 9 || str[ip]==10 || str[ip] == 32))) {
      if (!spc && !quote) {
        spc = true;
        if (nel==num) end = ip-1;
      }
    } else {
      if (spc) {
        spc = false;
        nel++;
        if (nel==num) start = ip;
      }
    }
  }
  if (!spc && nel==num) end = str.size()-1;
                                                                                                                                        
  if (start >= 0) {
    std::string c;
    std::string s = str.substr(start,end-start+1);
    for (ip=0;ip<s.size();ip++) {
      if (s[ip] != '"') c += s[ip];
    }
    return c;
  } else {
    return "";
  }
}





