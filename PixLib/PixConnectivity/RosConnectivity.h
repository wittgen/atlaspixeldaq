/////////////////////////////////////////////////////////////////////
// RosConnectivityObject.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 23/04/08  Version 1.0 (PM)
//           Initial release

//! Classes for ROS/ROBIN/ROL connectivity

#ifndef _PIXLIB_ROSCONNOBJ
#define _PIXLIB_ROSCONNOBJ

#include "PixConnectivity/GenericConnectivityObject.h"

namespace PixLib {

class RodBocConnectivity;
class RolConnectivity;
class RobinConnectivity;
class RosConnectivity;

class RolConnectivity : public GenericConnectivityObject {
public:
  //! Payload accessors
  //! Down links
  //! Up link
  RobinConnectivity* robin() { return m_robin; };
  void linkRobin(RobinConnectivity* robin);
  //! Soft links
  RodBocConnectivity* rod() { return m_rod; };
  std::string rodName() { return m_rodName; };
  void linkRod(RodBocConnectivity* rod);
  //! Internal connectivity tables
  //! Constructors
  RolConnectivity(std::string name);
  RolConnectivity(DbRecord *r);
  //! Config handling
  void initConfig();
  virtual void readConfig(DbRecord *r);
  virtual void writeConfig(DbRecord *r);
  //! Link to configuration
private:
  using GenericConnectivityObject::readConfig;
  using GenericConnectivityObject::writeConfig;
  RobinConnectivity *m_robin;
  RodBocConnectivity *m_rod;
  std::string m_rodName;
};

class RobinConnectivity : public GenericConnectivityObject {
public:
  static const unsigned int nRoLinksPerRobin = 12;
  //! Payload accessors
  //! Down links
  RolConnectivity* rols(int slot);
  void addRol(int slot, RolConnectivity* rol);
  //! Up link
  RosConnectivity* ros() { return m_ros; };
  void linkRos(RosConnectivity* ros);
  //! Soft links
  //! Internal connectivity tables
  //! Constructors
  RobinConnectivity(std::string name);
  RobinConnectivity(DbRecord *r, int lev);
  //! Config handling
  void initConfig();
  virtual void readConfig(DbRecord *r, int lev);
  virtual void writeConfig(DbRecord *r, int lev);
  //! Link to configuration
private:
  RolConnectivity* m_rols[nRoLinksPerRobin];
  RosConnectivity *m_ros;
};

class RosConnectivity : public GenericConnectivityObject {
public:
  static const unsigned int nRobinsPerRos = 2;
  //! Payload accessors
  std::string& ebLinkName() { return m_ebLinkName; };
  //! Down links
  RobinConnectivity* robins(int slot);
  void addRobin(int slot, RobinConnectivity* robin);
  //! Up link
  //! Soft links
  //! Internal connectivity tables
  //! Constructors
  RosConnectivity(std::string name);
  RosConnectivity(DbRecord *r, int lev);
  //! Config handling
  void initConfig();
  virtual void readConfig(DbRecord *r, int lev);
  virtual void writeConfig(DbRecord *r, int lev);
  //! Link to configuration
private:
  RobinConnectivity* m_robins[nRobinsPerRos];
  std::string m_ebLinkName;
};

}

#endif
