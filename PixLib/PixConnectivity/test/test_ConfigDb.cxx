
#include "PixModuleGroup/PixModuleGroup.h"
#include "PixTrigController/TimPixTrigController.h"
#include "PixController/PixRunConfig.h"
#include "PixBoc/PixBoc.h"
#include "PixConnectivity/PixConnectivity.h"
#include "PixConnectivity/PixConfigDb.h"
#include "PixConnectivity/PixDisable.h"
#include "PixUtilities/PixLock.h"

#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cstdio>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <wait.h>
#include <memory>

using namespace PixLib;

std::map<std::string, PixMutex*> PixMutex::m_objects = std::map<std::string, PixMutex*>();

std::string element(std::string str, int num, char sep=' ') {
  unsigned int ip;
  int start = -1,end = -1;
  int nel = -1;
  bool spc = true;
  bool quote = false;
                                                                                                                                        
  for (ip=0;ip<str.size();ip++) {
    if (str[ip] == '"') quote = !quote;
    if (str[ip] == sep || (sep == 0 && 
       (str[ip] == 9 || str[ip]==10 || str[ip] == 32))) {
      if (!spc && !quote) {
        spc = true;
        if (nel==num) end = ip-1;
      }
    } else {
      if (spc) {
        spc = false;
        nel++;
        if (nel==num) start = ip;
      }
    }
  }
  if (!spc && nel==num) end = str.size()-1;
                                                                                                                                        
  if (start >= 0) {
    std::string c;
    std::string s = str.substr(start,end-start+1);
    for (ip=0;ip<s.size();ip++) {
      if (s[ip] != '"') c += s[ip];
    }
    return c;
  } else {
    return "";
  }
}

std::string upcase(std::string s) {
  std::string s1;
  std::transform(s.begin(), s.end(), s.begin(), (int(*)(int))std::toupper);
  return s;
}

std::map <std::string, PixDbServerInterface*> dbs;
std::map <std::string, IPCPartition*> dbsPart;

PixDbServerInterface *DbServer = NULL;
bool DbServerMode = false;
std::string DbsFname;

void openDbServer(std::string part, std::string name) {
  std::string fname = part+"/"+name;
  if (dbs.find(fname) != dbs.end()) {
    DbsFname = fname;
    DbServer = dbs[fname];
  } else {
    if (dbsPart.find(part) == dbsPart.end()) {
      // Create IPCPartition constructors                                                              
      try {
        dbsPart[part] = new IPCPartition(part);
      } 
      catch(...) {
        throw(PixException("Cannot connect to partition "+part));
      }
    }
    bool ready=false;
    int nTry=0;
    if (dbsPart[part] != NULL) {
      do {
        DbServer = new PixDbServerInterface(dbsPart[part], name, PixDbServerInterface::CLIENT, ready);
        if (!ready) {
          delete DbServer;
        } else {
          break;
        }
        nTry++;
        sleep(1);
      } while (nTry<20);
      if (!ready) {
        throw(PixException("Cannot connect to DB server "+name+" in "+part));
      }
      DbsFname = fname;
      dbs[fname] = DbServer;
    }
  }
}

int main(int argc, char** argv) {
  try {
    time_t t0 = time(NULL);
    // Start IPCCore    
    IPCCore::init(argc, argv);
    // Create PixConfigDb object 
    PixConfigDb *confDb = new PixConfigDb();  
    std::cout << "Time to open " << "Base/Base" << " = " << time(NULL) - t0 << std::endl;
    std::string tagID = "Base";
    std::string tagCF = "Base";

    bool loop = true;
    while (loop) {
      std::string cmd;
      if (DbServerMode) std::cout << "("+DbsFname+")/";
      std::cout << tagID+"/"+tagCF+" >>> ";
      getline(std::cin, cmd);
      std::string ncmd = upcase(element(cmd, 0));
      
      try {
        if (ncmd == "QUIT") {
          loop = false;
        } else if (ncmd == "HELP") {
          std::cout << "Possible commmands are: " << std::endl;
	  std::cout << "  QUIT" << std::endl;
	  std::cout << "  HELP" << std::endl;
          std::cout << "  DBSERVER ON" << std::endl;
          std::cout << "  DBSERVER OFF" << std::endl;
          std::cout << "  DBSERVER <part> <name>" << std::endl;
	  std::cout << "  LIST ID" << std::endl;
	  std::cout << "  LIST CFG" << std::endl;
	  std::cout << "  LIST OBJ" << std::endl;
	  std::cout << "  LIST REV <object>" << std::endl;
	  std::cout << "  LIST CYC <object> <rev>" << std::endl;
	  std::cout << "  SET ID <id tag>" << std::endl;
	  std::cout << "  SET CFG <cfg tag>" << std::endl;
	  std::cout << "  SET <cfg tag> < id tag>" << std::endl;
	  std::cout << "  SET <cfg tag>" << std::endl;
	  std::cout << "  DUMP <object>" << std::endl;
        } else if (ncmd == "DBSERVER") {
          std::string p1 = upcase(element(cmd, 1));
          if (upcase(p1) == "ON") {
            if (DbServer != NULL) {
              DbServerMode = true;
              confDb->cfgFromDbServ();
            }
          } else if (upcase(p1) == "OFF") {
            DbServerMode = false;
            confDb->cfgFromDb();
          } else {
            std::string p1 = element(cmd, 1);
            std::string p2 = element(cmd, 2);
            if (p2 == "") p2 = "PixelDbServer";
	    openDbServer(p1, p2);
            DbServerMode = true;
            confDb->openDbServer(DbServer);
          }
        } else if (ncmd == "LIST") {
          std::string p1 = upcase(element(cmd, 1));
          if (p1 == "ID") {
            std::vector<std::string> list;
            list = confDb->listObjTags();
            std::cout << "ID tags : " << std::endl;
            for (std::string s : list) {
              std::cout << "  " << s << std::endl;
            }
          } else if (p1 == "CFG") {
            std::vector<std::string> list;
            list = confDb->listCfgTags(tagID);
            std::cout << "CFG tags in " << tagID << " : " << std::endl;
            for (std::string s : list) {
              std::cout << "  " << s << std::endl;
            }
          } else if (p1 == "OBJ") {
            std::vector<std::string> list;
            list = confDb->listObjects(tagCF,tagID);
            std::cout << "Objects in " << tagID << "/" << tagCF << " : " << std::endl;
            for (std::string s : list) {
              std::cout << "  " << s << " " << confDb->getObjectType(s, tagCF, tagID) << std::endl;
            }
          } else if (p1 == "REV") {
            std::string object = element(cmd, 2);
            std::vector<unsigned int> revs; 
            revs = confDb->listRevisions(object, tagCF, tagID);
            std::cout << "Revisions for " << object << " in " << tagID << "/" << tagCF << " : " << std::endl;
            for (unsigned int r : revs) {
              std::cout << "  " << r << std::endl;
            }
          } else if (p1 == "CYC") {
            std::string object = element(cmd, 2);
            std::istringstream srev(element(cmd, 3));
            unsigned int rev;
            srev >> rev;
            std::vector<unsigned int> cycles; 
            cycles = confDb->listCycles(object, rev, tagCF, tagID);
            std::cout << "Cycles for " << object << " rev " << rev << " in " << tagID << "/" << tagCF << " : " << std::endl;
            for (unsigned int c : cycles) {
              std::cout << "  " << c << std::endl;
            }
          }
        } else if (ncmd == "SET") {
          std::string oldID = tagID;
          std::string oldCF = tagCF;
          std::string p1 = element(cmd, 1);
          std::string p2 = element(cmd, 2);
          if (upcase(p1) == "ID") {
            tagID = p2;
          } else if (upcase(p1) == "CFG") {
            tagCF = p2;
          } else {
            tagCF = p1;
            if (p2 != "") tagID = p2;
          }
          t0 = time(NULL);
          try {
            confDb->setCfgTag(tagCF, tagID);
            std::cout << "Time to open " << tagID << "/" << tagCF << " = " << time(NULL) - t0 << std::endl;
          }
	  catch (PixConfigDbExc s) {
	    std::cout << s.dump() << std::endl;
            if (s.getType() == "IDNOTFOUND") {
              tagCF = oldCF;
              tagID = oldID;
            } else if (s.getType() == "TAGNOTFOUND") {
              tagCF = oldCF;
            } else {
              tagCF = oldCF;
              tagID = oldID;
            }
          }
        } else if (ncmd == "DUMP") {
          std::string object = element(cmd, 1);
          std::istringstream srev(element(cmd, 2));
          unsigned int rev;
          srev >> rev;
          std::string type = confDb->getObjectType(object, tagCF, tagID);
          if (type == "DISABLE_CFG") {
            PixDisable dis(object);
            confDb->readCfg(&(dis.config()), object, rev, tagCF, tagID);
            dis.config().dump(std::cout);
          } else if (type == "TIM_CFG") {
            TimPixTrigController tim(object);
            confDb->readCfg(tim.config(), object, rev, tagCF, tagID);
            tim.config()->dump(std::cout);
          } else if (type == "RUNCONF_CFG") {
            PixRunConfig runc(object);
            confDb->readCfg(&(runc.config()), object, rev, tagCF, tagID);
            runc.config().dump(std::cout);
          } else if (type == "RODBOC_CFG") {
            PixModuleGroup modg(object, UNKNOWN, false);
            confDb->readCfg(&(modg.config()), object, rev, tagCF, tagID);
            modg.config().dump(std::cout);
            if (modg.getPixController() != NULL) {
              modg.getPixController()->config().dump(std::cout);
            }
            if (modg.getPixBoc() != NULL) {
              modg.getPixBoc()->getConfig()->dump(std::cout);
            }
          }
        }
      }
      catch (PixConfigDbExc s) {
        std::cout << s.dump() << std::endl;
      }
    }
    delete confDb;
  }
  catch (std::exception e) {
    std::cout << "Unhandled exception - " << e.what() << std::endl;
    return -1;
  }
  return 0;
}
