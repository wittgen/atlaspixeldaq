/////////////////////////////////////////////////////////////////////
// ModuleConnectivity.cxx
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 05/10/06  Version 1.0 (PM)
//           Initial release

//! Class for module connectivity

#include "PixConnectivity/ModuleConnectivity.h"

#include "PixConnectivity/Pp0Connectivity.h"
#include "PixConnectivity/RodBocConnectivity.h"
#include "PixConnectivity/OBConnectivity.h"
#include "PixDbInterface/PixDbInterface.h"

using namespace PixLib;

ModuleConnectivity::ModuleConnectivity(std::string name, EnumMCCflavour::MCCflavour mccFlv, 
				       EnumFEflavour::FEflavour feFlv, int nFe) : 
  GenericConnectivityObject(name), m_pp0(NULL) {
  // Create the config object
  initConfig(); 
  m_mccFlv = mccFlv;
  m_feFlv = feFlv;
  m_nFe = nFe;
}

ModuleConnectivity::ModuleConnectivity(DbRecord *r) : 
  GenericConnectivityObject(r), m_pp0(NULL) {
  if (r->getClassName() == "MODULE") {
    // Read record from DB
    storeRec(r);
    // Create the config object
    initConfig();
    readConfig(getRec()); 
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a ModuleConnectivity object");
  }
}

void ModuleConnectivity::linkPp0(Pp0Connectivity *pp0, int slot) {
  m_pp0 = pp0;
  m_pp0Slot = slot;
  if (modId < 0) {
    if (pp0->ob() != NULL) {
      groupId = pp0->ob()->bocRxSlot(0);
    } else {
      if (groupId < 0) groupId = 0;
    }
    modId = groupId*8 + (slot-1)%8;//changed from 7 to 8
    groupId = modId%4;
  }
}

int ModuleConnectivity::inLink() {
  OBConnectivity *ob;
  RodBocConnectivity *boc;
  OBLinkMap *obl;
  RodBocLinkMap *bocl;
  
  if (m_pp0 != NULL && m_pp0Slot > 0 && m_pp0Slot <= 8) {
    if (m_pp0->ob() != NULL) {
      ob = m_pp0->ob();
      if (ob->rodBoc() != NULL && ob->linkMap() != NULL) {
  	obl = ob->linkMap();
  	boc = ob->rodBoc();
  	if (boc->linkMap() != NULL) {
  	  bocl = boc->linkMap();
	  int OBFiber = obl->dciFiber[m_pp0Slot-1]; 
	  int BocSlot = ob->bocTxSlot();
	  if(OBFiber != -1 && BocSlot != -1){
	    return  bocl->rodTx[BocSlot][OBFiber];
	  }
	  else return -1;
	}
      }
    }
  }
  return -1;
}

int ModuleConnectivity::applyFmtMap(int ol, RodBocConnectivity* rod) {
  if (ol >= 0) {
    int fmt = rod->fmtLinkMap(ol/16);
    int pos =-1;
    for (int ip=0; ip<4; ip++) {
      int ch = (fmt>>(16+4*ip))&0xf;
      if (ol%16 == ch) pos = ip;
    }
    if (pos >= 0) {
      return (ol/16)*16 + pos; 
    } else {
      return -1;
    }
  } else {
    return -1;
  }
}

int ModuleConnectivity::outLink1A(MccBandwidth mode){
  OBConnectivity *ob;
  RodBocConnectivity *boc;
  OBLinkMap *obl;
  RodBocLinkMap *bocl;

  if (m_pp0 != NULL && m_pp0Slot > 0 && m_pp0Slot <= 8) {
    if (m_pp0->ob() != NULL) {
      ob = m_pp0->ob();
      if (ob->rodBoc() != NULL && ob->linkMap() != NULL) {
  	obl = ob->linkMap();
  	boc = ob->rodBoc();
  	if (boc->linkMap() != NULL) {
  	  bocl = boc->linkMap();
	  int OBFiber = obl->dto2Fiber[m_pp0Slot-1];
          if (obl->dtoFiber[m_pp0Slot-1] >= 0 && obl->dto2Fiber[m_pp0Slot-1] >= 0) {
	    if (useAltLink) OBFiber = obl->dtoFiber[m_pp0Slot-1];
	  } 
	  int BocSlotInt = 0;
	  if(OBFiber != -1) if(OBFiber >= 10) {BocSlotInt = 1; OBFiber -= 10;}
	  int BocSlot = ob->bocRxSlot(BocSlotInt);
	  if(OBFiber != -1 && BocSlot != -1){
            int ol = -1;
	    if (mode == SINGLE_40 || mode == DOUBLE_40){
	      ol = bocl->rodRx40[BocSlot][OBFiber];
	    } else if (mode == SINGLE_80 || mode == DOUBLE_80){
	      ol = bocl->rodRx80a[BocSlot][OBFiber];
	    }
	    return applyFmtMap(ol, boc);
	  }	  	  
  	}
      }
    }
  }
  return -1;
}

int ModuleConnectivity::outLink2A(MccBandwidth mode){
  OBConnectivity *ob;
  RodBocConnectivity *boc;
  OBLinkMap *obl;
  RodBocLinkMap *bocl;
  
  if (m_pp0 != NULL && m_pp0Slot > 0 && m_pp0Slot <= 8) {
    if (m_pp0->ob() != NULL) {
      ob = m_pp0->ob();
      if (ob->rodBoc() != NULL && ob->linkMap() != NULL) {
  	obl = ob->linkMap();
  	boc = ob->rodBoc();
  	if (boc->linkMap() != NULL) {
  	  bocl = boc->linkMap();
	  int OBFiber = obl->dtoFiber[m_pp0Slot-1]; 
          if (obl->dtoFiber[m_pp0Slot-1] >= 0 && obl->dto2Fiber[m_pp0Slot-1] >= 0) {
	    if (useAltLink) OBFiber = obl->dto2Fiber[m_pp0Slot-1];
	  } 
	  int BocSlotInt = 0;
	  if(OBFiber != -1) if(OBFiber >= 10) {BocSlotInt = 1; OBFiber -= 10;}
	  int BocSlot = ob->bocRxSlot(BocSlotInt);
	  if(OBFiber != -1 && BocSlot != -1){
	    int ol=0;
	    if (mode == SINGLE_40 || mode == DOUBLE_40){
	      ol = bocl->rodRx40[BocSlot][OBFiber];
	    } else if (mode == SINGLE_80 || mode == DOUBLE_80){
	      ol = bocl->rodRx80a[BocSlot][OBFiber];
	    }
	    return applyFmtMap(ol, boc);
	  }	  	  
  	}
      }
    }
  }
  return -1;
}



int ModuleConnectivity::outLink1B(MccBandwidth mode){
  OBConnectivity *ob;
  RodBocConnectivity *boc;
  OBLinkMap *obl;
  RodBocLinkMap *bocl;
  
  if(mode == SINGLE_40 || mode == DOUBLE_40){
    return -1;
  }
  if (m_pp0 != NULL && m_pp0Slot > 0 && m_pp0Slot <= 8) {
    if (m_pp0->ob() != NULL) {
      ob = m_pp0->ob();
      if (ob->rodBoc() != NULL && ob->linkMap() != NULL) {
  	obl = ob->linkMap();
  	boc = ob->rodBoc();
  	if (boc->linkMap() != NULL) {
  	  bocl = boc->linkMap();
	  int OBFiber = obl->dto2Fiber[m_pp0Slot-1]; 
          if (obl->dtoFiber[m_pp0Slot-1] >= 0 && obl->dto2Fiber[m_pp0Slot-1] >= 0) {
	    if (useAltLink) OBFiber = obl->dtoFiber[m_pp0Slot-1];
	  } 
	  int BocSlotInt = 0;
	  if(OBFiber != -1) if(OBFiber >= 10) {BocSlotInt = 1; OBFiber -= 10;}
	  int BocSlot = ob->bocRxSlot(BocSlotInt);
	  if (OBFiber != -1 && BocSlot != -1){
	    int ol = bocl->rodRx80b[BocSlot][OBFiber];
	    return applyFmtMap(ol, boc);
	  }
  	}
      }
    }
  }
  return -1;
}

int ModuleConnectivity::outLink2B(MccBandwidth mode){
  OBConnectivity *ob;
  RodBocConnectivity *boc;
  OBLinkMap *obl;
  RodBocLinkMap *bocl;

  if(mode == SINGLE_40 || mode == DOUBLE_40){
    return -1;
  }
  if (m_pp0 != NULL && m_pp0Slot > 0 && m_pp0Slot <= 8) {
    if (m_pp0->ob() != NULL) {
      ob = m_pp0->ob();
      if (ob->rodBoc() != NULL && ob->linkMap() != NULL) {
  	obl = ob->linkMap();
  	boc = ob->rodBoc();
  	if (boc->linkMap() != NULL) {
  	  bocl = boc->linkMap();
	  int OBFiber = obl->dtoFiber[m_pp0Slot-1]; 
          if (obl->dtoFiber[m_pp0Slot-1] >= 0 && obl->dto2Fiber[m_pp0Slot-1] >= 0) {
	    if (useAltLink) OBFiber = obl->dto2Fiber[m_pp0Slot-1];
	  } 
	  int BocSlotInt = 0;
	  if(OBFiber != -1) if(OBFiber >= 10) {BocSlotInt = 1; OBFiber -= 10;}
	  int BocSlot = ob->bocRxSlot(BocSlotInt);
	  if (OBFiber != -1 && BocSlot != -1){
	    int ol = bocl->rodRx80b[BocSlot][OBFiber];
	    return applyFmtMap(ol, boc);
	  }
  	}
      }
    }
  }
  return -1;
}

//int ModuleConnectivity::outLink(MccBandwidth mode, bool defaultValue) {
//  return -1;
//}

int ModuleConnectivity::bocLinkTx(){
  OBConnectivity *ob;
  RodBocConnectivity *boc;
  OBLinkMap *obl;
 // RodBocLinkMap *bocl;
  
  if (m_pp0 != NULL && m_pp0Slot > 0 && m_pp0Slot <= 8) {
    if (m_pp0->ob() != NULL) {
      ob = m_pp0->ob();
      if (ob->rodBoc() != NULL && ob->linkMap() != NULL) {
  	obl = ob->linkMap();
  	boc = ob->rodBoc();
  	if (boc->linkMap() != NULL) {
  	  //bocl = boc->linkMap();
	  int OBFiber = obl->dciFiber[m_pp0Slot-1]; 
	  int BocSlot = ob->bocTxSlot();
	  if(OBFiber != -1 && BocSlot != -1){
	    return BocSlot*10 + OBFiber;
	  }
  	}
      }
    }
  }
  return -1;
}

int ModuleConnectivity::bocLinkRx(int slot){

  assert(slot>=0 && slot<=2);

  OBConnectivity *ob;
  RodBocConnectivity *boc;
  OBLinkMap *obl;
  //RodBocLinkMap *bocl;
  
  if (m_pp0 != NULL && m_pp0Slot > 0 && m_pp0Slot <= 8) {
    if (m_pp0->ob() != NULL) {
      ob = m_pp0->ob();
      if (ob->rodBoc() != NULL && ob->linkMap() != NULL) {
  	obl = ob->linkMap();
  	boc = ob->rodBoc();
  	if (boc->linkMap() != NULL) {
  	  //bocl = boc->linkMap();
	  int OBFiber;
	  if(slot == 1)
	    OBFiber = obl->dto2Fiber[m_pp0Slot-1]; 
	  else
	    OBFiber = obl->dtoFiber[m_pp0Slot-1]; 
	  int BocSlotInt = 0;
	  if(OBFiber != -1) if(OBFiber >= 10) {BocSlotInt = 1; OBFiber -= 10;}
	  int BocSlot = ob->bocRxSlot(BocSlotInt);
	  if(OBFiber != -1 && BocSlot != -1){
	    return BocSlot*10 + OBFiber;
	  }
  	}
      }
    }
  }
  
  return -1;
}

int ModuleConnectivity::outLink1A() {
  return outLink1A(readoutSpeed);
}

int ModuleConnectivity::outLink1B() {
  return outLink1B(readoutSpeed);
}

int ModuleConnectivity::outLink2A() {
  return outLink2A(readoutSpeed);
}

int ModuleConnectivity::outLink2B() {
  return outLink2B(readoutSpeed);
}

int ModuleConnectivity::bocRxLink1() {
  return bocLinkRx(1);
}

//int ModuleConnectivity::bocRxLink2() {
//  return bocLinkRx(2);
//}

void ModuleConnectivity::initConfig() { 
  m_config = new Config(m_name, "ModuleConnectivity");
  Config &conf = *m_config;
  conf.addGroup("Identifier");
  //conf["Identifier"].addString("Id", m_name, m_name, "Module identifier", true);
  conf["Identifier"].addAlias("PRODID", m_prod, "Mxxxxxx", "Module production identifier", true);
  conf["Identifier"].addAlias("OFFLINEID", m_offline, "0xnnnnnn", "Module offline identifier", true);
  conf["Identifier"].addInt("ModId", modId, -1, "Module identifier in the ROD", true);
  conf["Identifier"].addInt("GroupId", groupId, -1, "Module group identifier in the ROD", true);

  conf.addGroup("Config");
  //conf["Config"].addString("modConfig", m_modConfig, "NULL", "Link to module configuration", true);
  conf["Config"].addBool("useAltLink", useAltLink, false, "Use alternate link if possible", true);
  std::map<std::string, int> mccbwMap;
  mccbwMap["SINGLE_40"] = SINGLE_40;
  mccbwMap["DOUBLE_40"] = DOUBLE_40;
  mccbwMap["SINGLE_80"] = SINGLE_80;
  mccbwMap["DOUBLE_80"] = DOUBLE_80;
  conf["Config"].addList("readoutSpeed", readoutSpeed, SINGLE_40, mccbwMap, "Default MCC readout speed", true); 
  conf.addGroup("Cables");
  //conf["Cables"].addString("t0CableId", t0CableId, "NULL", "Type 0 cable id", true);
  //conf.addGroup("Up");
  //conf["Up"].addInt("pp0Slot", m_pp0Slot, -1, "PP0 slot", true);
  conf.addGroup("Enable");
  conf["Enable"].addBool("readout", enableReadout, 1, "Module readout enable", true);

  conf.addGroup("Flavour");
  conf["Flavour"].addInt("MCC", m_mccFlv, EnumMCCflavour::PM_MCC_I2, "MCC flavour", true);
  conf["Flavour"].addInt("FE", m_feFlv, EnumFEflavour::PM_FE_I2, "FE flavour", true);
  conf["Flavour"].addInt("nFE", m_nFe, 16, "No. of FEs on module", true);

  conf.reset();
}

void ModuleConnectivity::readConfig(DbRecord *r) { 
  if (r->getClassName() == "MODULE") {
    storeRec(r);
    m_name = r->getName();
    // Read the config object
    m_config->read(r);
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a ModuleConnectivity object");
  }
}

void ModuleConnectivity::writeConfig(DbRecord *r) { 
  if (r->getClassName() == "MODULE") {
    storeRec(r);
    // Write the config object
    m_config->write(r);
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a ModuleConnectivity object");
  }
}

void ModuleConnectivity::getModPars(EnumMCCflavour::MCCflavour &mccFlv, EnumFEflavour::FEflavour &feFlv, int &nFe) {
  mccFlv = m_mccFlv;
  feFlv  = m_feFlv;
  nFe = m_nFe;
}



