/////////////////////////////////////////////////////////////////////
// RosConnectivity.cxx
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 23/04/08  Version 1.0 (PM)
//           Initial release

//! Classes for ROS/ROBIN/ROL connectivity

#include "PixConnectivity/RodBocConnectivity.h"
#include "PixConnectivity/RosConnectivity.h"
#include "PixDbInterface/PixDbInterface.h"
#include "Config/Config.h"

using namespace PixLib;

// ROL Methods

RolConnectivity::RolConnectivity(std::string name) : 
  GenericConnectivityObject(name) {
  m_rod = NULL;
  // Create the config object
  initConfig(); 
}

RolConnectivity::RolConnectivity(DbRecord *r) : 
  GenericConnectivityObject(r) {
  m_rod = NULL;
  if (r->getClassName() == "ROL") {
    // Read record from DB
    storeRec(r);
    // Create the config object
    initConfig();
    readConfig(getRec()); 
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a RolConnectivity object");
  }
}

void RolConnectivity::linkRod(RodBocConnectivity *rod) {
  m_rod = rod;
  m_rodName = rod->name();
}

void RolConnectivity::linkRobin(RobinConnectivity *robin) {
  m_robin = robin;
}

void RolConnectivity::initConfig() { 
  m_config = new Config(m_name, "ROL");
  Config &conf = *m_config;

  conf.reset();
}

void RolConnectivity::readConfig(DbRecord *r) { 
  if (r->getClassName() == "ROL") {
    storeRec(r);
    m_name = r->getName();
    // Read the config object
    m_config->read(r);
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a RolConnectivity object");
  }
}

void RolConnectivity::writeConfig(DbRecord *r) { 
  if (r->getClassName() == "ROL") {
    storeRec(r);
    // Write the config object
    m_config->write(r); 
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a RolConnectivity object");
  }
}

// ROBIN Methods

RobinConnectivity::RobinConnectivity(std::string name) : 
  GenericConnectivityObject(name) {
  for (unsigned int i=0; i<RobinConnectivity::nRoLinksPerRobin; i++) m_rols[i] = NULL;
  // Create the config object
  initConfig(); 
}

RobinConnectivity::RobinConnectivity(DbRecord *r, int lev) : 
  GenericConnectivityObject(r) {
  for (unsigned int i=0; i<RobinConnectivity::nRoLinksPerRobin; i++) m_rols[i] = NULL;
  if (r->getClassName() == "ROBIN") {
    // Read record from DB
    storeRec(r);
    // Create the config object
    initConfig();
    readConfig(getRec(), lev); 
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a RobinConnectivity object");
  }
}

void RobinConnectivity::initConfig() { 
  // std::cout << __PRETTY_FUNCTION__  << " with nRoLinksPerRobin=" << RobinConnectivity::nRoLinksPerRobin << std::endl;
  m_config = new Config(m_name, "ROBIN");
  Config &conf = *m_config;

  conf.reset();
}

void RobinConnectivity::readConfig(DbRecord *r, int lev) { 
  if (r->getClassName() == "ROBIN") {
    storeRec(r);
    m_name = r->getName();
    // Read the config object
    m_config->read(r);
    if (lev != 0) {
      lev--;
      // Create child Modules
      for(dbRecordIterator it = r->recordBegin(); it != r->recordEnd(); it++) {
	// Look for Module records
	if ((*it)->getClassName() == "ROL") {
	  std::string slnam = it.srcSlotName();
	  std::istringstream snum(slnam);
	  int islot = -1;
	  snum >> islot;
	  if (islot >= 0 && islot < int(RobinConnectivity::nRoLinksPerRobin)) {
	    RolConnectivity *rol = new RolConnectivity(*it);
	    addRol(islot, rol);
	  }
	}
      } 
    } 
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a RobinConnectivity object");
  }
}

void RobinConnectivity::writeConfig(DbRecord *r, int lev) { 
  if (r->getClassName() == "ROBIN") {
    storeRec(r);
    // Write the config object
    m_config->write(r); 
     if (lev != 0) {
      lev--;
      // Create child Robins;
     // int i;
      for (unsigned i=0; i<RobinConnectivity::nRoLinksPerRobin; i++) {
	// Write Module
	std::ostringstream lname;
        lname << i;
	if (m_rols[i] != NULL) {
	  DbRecord *sr;
	  try {
	    sr = r->addRecord("ROL", lname.str(), m_rols[i]->name());
	  }	
	  catch (const PixDBException&) {
	    dbRecordIterator ri;
	    if ((ri = r->findRecord(lname.str())) == r->recordEnd()) {  
	      throw ConnectivityExc(ConnectivityExc::ERROR,"Cannot create "+m_rols[i]->name()+"/"+lname.str()+"/ROL record");
	    }
	    sr = *ri;
	  }
	  m_rols[i]->writeConfig(sr);
	}
      }
    }
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a RobinConnectivity object");
  }
}

RolConnectivity* RobinConnectivity::rols(int slot) {
  if (slot >=0 && slot < int(RobinConnectivity::nRoLinksPerRobin)) {
    return m_rols[slot];
  }
  return NULL;
}


void RobinConnectivity::linkRos(RosConnectivity *ros) {
  m_ros = ros;
}

void RobinConnectivity::addRol(int slot, RolConnectivity *rol) {
  if (slot >= 0 && slot < int(RobinConnectivity::nRoLinksPerRobin)) {
    if (m_rols[slot] != NULL) {
      delete m_rols[slot];
    }
    m_rols[slot] = rol;
    rol->linkRobin(this);
  }
}

// ROS Methods

RosConnectivity::RosConnectivity(std::string name) : 
  GenericConnectivityObject(name) {
  for (unsigned int i=0; i<RosConnectivity::nRobinsPerRos; i++) m_robins[i] = NULL;
  // Create the config object
  initConfig(); 
}

RosConnectivity::RosConnectivity(DbRecord *r, int lev) : 
  GenericConnectivityObject(r) {
  for (unsigned int i=0; i<RosConnectivity::nRobinsPerRos; i++) m_robins[i] = NULL;
  if (r->getClassName() == "ROS") {
    // Read record from DB
    storeRec(r);
    // Create the config object
    initConfig();
    readConfig(getRec(), lev); 
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a RosConnectivity object");
  }
}

void RosConnectivity::initConfig() { 
  // std::cout << __PRETTY_FUNCTION__  << " with nRobinsPerRos=" << RosConnectivity::nRobinsPerRos << std::endl;
  m_config = new Config(m_name, "ROS");
  Config &conf = *m_config;

  conf.addGroup("Link");
  conf["Link"].addString("ebLinkName", m_ebLinkName, "", "Link to Event Builder", true);

  conf.reset();
}

void RosConnectivity::readConfig(DbRecord *r, int lev) { 
  if (r->getClassName() == "ROS") {
    storeRec(r);
    m_name = r->getName();
    // Read the config object
    m_config->read(r);
    if (lev != 0) {
      lev--;
      // Create child Modules
      for(dbRecordIterator it = r->recordBegin(); it != r->recordEnd(); it++) {
	// Look for Module records
	if ((*it)->getClassName() == "ROBIN") {
	  std::string slnam = it.srcSlotName();
	  std::istringstream snum(slnam);
	  int islot = -1;
	  snum >> islot;
	  if (islot >= 0 && islot < int(RosConnectivity::nRobinsPerRos)) {
	    RobinConnectivity *robin = new RobinConnectivity(*it, lev);
	    if (islot >= 0 && islot < int(RosConnectivity::nRobinsPerRos)) {
	      addRobin(islot, robin);
	    }
	  }
	}
      } 
    } 
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a RosConnectivity object");
  }
}

void RosConnectivity::writeConfig(DbRecord *r, int lev) { 
  if (r->getClassName() == "ROS") {
    storeRec(r);
    // Write the config object
    m_config->write(r); 
     if (lev != 0) {
      lev--;
      // Create child Robins;
      for (unsigned i=0; i<RosConnectivity::nRobinsPerRos; i++) {
	// Write Module
	std::ostringstream lname;
        lname << i;
	if (m_robins[i] != NULL) {
	  DbRecord *sr;
	  try {
	    sr = r->addRecord("ROBIN", lname.str(), m_robins[i]->name());
	  }	
	  catch (const PixDBException&) {
	    dbRecordIterator ri;
	    if ((ri = r->findRecord(lname.str())) == r->recordEnd()) {  
	      throw ConnectivityExc(ConnectivityExc::ERROR,"Cannot create "+m_robins[i]->name()+"/"+lname.str()+"/ROBIN record");
	    }
	    sr = *ri;
	  }
	  m_robins[i]->writeConfig(sr, lev);
	}
      }
    }
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a RosConnectivity object");
  }
}

RobinConnectivity* RosConnectivity::robins(int slot) {
  if (slot >=0 && slot < int(RosConnectivity::nRobinsPerRos)) {
    return m_robins[slot];
  }
  return NULL;
}


void RosConnectivity::addRobin(int slot, RobinConnectivity *robin) {
  if (slot >= 0 && slot < int(RosConnectivity::nRobinsPerRos)) {
    if (m_robins[slot] != NULL) {
      delete m_robins[slot];
    }
    m_robins[slot] = robin;
    robin->linkRos(this);
  }
}



