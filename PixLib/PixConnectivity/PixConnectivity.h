/////////////////////////////////////////////////////////////////////
// PixConnectivity.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 12/11/06  Version 1.0 (PM)
//           Initial release

//! Helpers for connectivity db access

#ifndef _PIXLIB_PIXCONNDB1
#define _PIXLIB_PIXCONNDB1

#include "BaseException.h"
#include "PixDbInterfaceFactory/PixDbInterfaceFactory.h"
#include "PixDbServer/PixDbServerInterface.h"

#include <string>
#include <map>
#include <vector>

class DFMutex;

namespace PixLib {

class DbRecord;
class PixDbInterface;
class Config;

class PartitionConnectivity;
class RodCrateConnectivity;
class RodBocConnectivity;
class TimConnectivity;
class SbcConnectivity;
class OBConnectivity;
class Pp0Connectivity;
class ModuleConnectivity;
class RosConnectivity;
class RobinConnectivity;
class RolConnectivity;
class OBLinkMap;
class RodBocLinkMap;
class TimPixTrigController;
class PixModule;
class PixModuleGroup;
class PixDisable;
class PixRunConfig;
class PixScan;

//! Connectivity Exception class;
 class PixConnectivityExc : public SctPixelRod::BaseException {
 public:
  enum ErrorLevel{INFO, WARNING, ERROR, FATAL};
  
  PixConnectivityExc(ErrorLevel el, std::string id, std::string descr) : BaseException(id), m_errorLevel(el), m_id(id), m_descr(descr) {}; 
    virtual ~PixConnectivityExc() {};
    
    virtual void dump(std::ostream &out) {
      out << "Pixel Config Exception: " << dumpLevel() << ":" << m_id << " - "<< m_descr << std::endl; 
    };
    std::string dumpLevel() {
      switch (m_errorLevel) {
    case INFO : 
      return "INFO";
    case WARNING :
      return "WARNING";
    case ERROR :
      return "ERROR";
    case FATAL :
      return "FATAL";
    default :
      return "UNKNOWN";
    }
  };
  ErrorLevel getErrorLevel() const  { return m_errorLevel; }; 
  std::string getId() const { return m_id; };
  std::string getDescr() const  { return m_descr; };
private:
  ErrorLevel m_errorLevel;
  std::string m_id;
  std::string m_descr;
};

class PixConnectivity {
public:
  // Constructor
  PixConnectivity(std::string tagC, std::string tagP, std::string tagA, std::string tagCF, std::string tagOI = "M5", std::string tagModCF="", bool open = false);
  PixConnectivity(PixDbServerInterface *DbServer, std::string tagC, std::string tagP, std::string tagA, std::string tagCF, std::string tagOI = "M5", std::string tagModCF="", bool open = false);
  
  PixConnectivity();
  PixConnectivity(PixConnectivity *conn, std::string rodName);
  ~PixConnectivity();

  // temporary 
  //  void fillLink(std::string linkName, std::string objName, std::string objType, unsigned int revision, PixDbDomain::link_object &link);
  
  //! Tags handling
  std::vector<std::string> listConnTagsC();
  std::vector<std::string> listConnTagsP();
  std::vector<std::string> listConnTagsA();
  std::vector<std::string> listConnTagsOI();
  void setConnTag(std::string tagC, std::string tagP, std::string tagA, std::string tagOI= "M5");
  std::string getConnTagC();
  std::string getConnTagP();
  std::string getConnTagA();
  std::string getConnTagOI();
  void createConnTag(std::string tagC, std::string tagP, std::string tagA, std::string tagOI = "M5");  

  //! Connectivity handling
  void clearConn();
  void loadConn();
  void saveConn();
  void cloneConn(PixConnectivity *conn, std::string rodName);
  void connTransStart();
  void connTransCommit();

  //! Configuration handling

  // New interface
public:
  std::vector<std::string> listCfgTags();
  void setCfgTag(std::string tag, std::string oi="", std::string modtag="");
  std::string getCfgTag();
  std::string getModCfgTag();
  //std::string getCfgTagOI();
  void setTagPen(std::string tag);
  //std::string getTagPen();
  void setCfgRev(unsigned int rev);
  void setModCfgRev(unsigned int rev);
  unsigned int getCfgRev();
  //unsigned int getModCfgRev();
  void createCfgTag(std::string tag);  
  void createModCfgTag(std::string tag);  
  void closeCfg(std::string lobj);
 // bool checkCfg(Config *cfg, std::string lobj, unsigned int &rev);
  //bool writeCfg(Config *cfg, std::string lobj);
  //bool readCfg(Config *cfg, std::string lobj);
  std::map<std::string, std::string> listSubCfg(std::string lobj);
  std::map<std::string, std::string> listCfgObj(std::string type);
  std::map<unsigned int, std::string> listCfgRev(std::string lobj);
  std::map<std::string, std::string> listCfgRevRep(std::string lobj);
  void cfgFromDb();
  void cfgFromDbServ() { m_dbServerModeCfg = true; };
private:
  std::string getCfgFileName(std::string lobj, unsigned int &cfgRev, unsigned int &cfgRep);
  DbRecord* getCfgFileRecord(std::string lobj, unsigned int &cfgRev, unsigned int &cfgRep);
  DbRecord* getSubRec(DbRecord *r, std::string lobj, int el);
  bool isModule(std::string &lobj); 
 
public:
  //! Old interface (deprecated)
  PixModule* getMod(std::string name);
  //PixModuleGroup* getModGroup(std::string name);
  //TimPixTrigController* getTim(std::string name);
  PixDisable* getDisable(std::string name);
  PixRunConfig* getRunConfig(std::string name);
  //PixScan* getScan(std::string name);
  //void readMod(PixModule *mod, std::string name);
  //void readModGroup(PixModuleGroup *modg, std::string name);
 // void readTim(TimPixTrigController *tim, std::string name);
  void readDisable(PixDisable *dis, std::string name);
  void readRunConfig(PixRunConfig *runc, std::string name);
  void readScan(PixScan *scn, std::string name);
 // void writeMod(PixModule *mod, std::string name, unsigned int rev=0xffffffff);
 // void writeModGroup(PixModuleGroup *modg, std::string name, unsigned int rev=0xffffffff);
 // void writeTim(TimPixTrigController *tim, std::string name, unsigned int rev=0xffffffff);
  void writeDisable(PixDisable *dis, std::string name, unsigned int rev=0xffffffff);
  void writeRunConfig(PixRunConfig *runc, std::string name, unsigned int rev=0xffffffff);
 // void writeScan(PixScan *scn, std::string name, unsigned int rev=0xffffffff);

  //! Very old interface - CoralDB+Root specific (even more deprecated)
  
  void openCfg(std::string mode);
  void dumpInDb(PixDbServerInterface *DbServer);
  DbRecord* getCfg(std::string obj, std::string type="");
  DbRecord* getCfg(std::string obj, unsigned int rev, std::string type="");
  DbRecord* getCfg(std::string lobj, unsigned int rev, std::string &cfgName, unsigned int &cfgRev, unsigned int &cfgRep, std::string type="");
  std::string getCfgName(std::string lobj, std::string type="");
  std::string getCfgName(std::string lobj, unsigned int rev, std::string type="");
  std::string getCfgName(std::string lobj, unsigned int rev, unsigned int &cfgRev, unsigned int &cfgRep, std::string type="");
  void getCfgName(std::string lobj, unsigned int rev, std::string &cfgName, unsigned int &cfgRev, unsigned int &cfgRep, std::string type="");
  //bool checkCfg(Config *cfg, std::string lobj, unsigned int &rev, PixDbServerInterface *dbserv);
  DbRecord* addCfg(std::string obj, unsigned int rev=0, std::string type="");

  // Expert-mode - maintenance
  void addCfg(std::vector<std::string> obj, std::vector<std::string> fName, std::vector<unsigned int> itim);
  void addCfg(std::string obj, std::string fileName, unsigned int rev);
  void addCfgNewBE(const Config &conf, std::string lobj, unsigned int itim=0, std::string type=""); //method for new backend

  // Obsolete
  //std::string &domainName() { return m_domainName; };
  //bool &cfgUseCoral() { return m_cfgUseCoral; };
  void cleanCfgCache();
  void getCfgObjType(std::string obj, std::string &type, std::string &name);
  
  std::map<std::string, PartitionConnectivity*> parts;
  std::map<std::string, RodCrateConnectivity*> crates;
  std::map<std::string, RodBocConnectivity*> rods;
  std::map<std::string, TimConnectivity*> tims;
  std::map<std::string, SbcConnectivity*> sbcs;
  std::map<std::string, OBConnectivity *> obs;
  std::map<std::string, Pp0Connectivity*> pp0s;
  std::map<std::string, ModuleConnectivity*> mods;
  std::map<std::string, RosConnectivity*> roses;
  std::map<std::string, RobinConnectivity*> robins;
  std::map<std::string, RolConnectivity*> rols;
  std::map<std::string, OBLinkMap*> obmaps;
  std::map<std::string, RodBocLinkMap*> bocmaps;

private:
  
  void loadConnDbServer();
  void loadConnDb();
  void saveConnDb();
  void init(PixDbCompoundTag t1, bool open = true);
  PixDbInterface* openDb(std::string name, std::string mode, PixDbCompoundTag t);
  void add(DbRecord *r, int lev);
  void copyRodCrates(PartitionConnectivity *p);
  void copyRodBocs(RodCrateConnectivity *c);
  void copyOBs(RodBocConnectivity *r);
  void copyPp0s(OBConnectivity *o);
  void copyModules(Pp0Connectivity *p);
  void copyRobins(RosConnectivity *r);
  void copyRols(RobinConnectivity *r);
  void checkCfgRev(DbRecord *cfg, unsigned int rev, std::string fName);
  DbRecord* checkCfgRecord(std::string name, std::string type);
  std::string checkCfgFile(std::string name, unsigned int itim);

  PixDbInterface *m_connDb = nullptr;
  PixDbInterface *m_cfgDb = nullptr;
  PixDbInterface *m_cfgModDb = nullptr;
  PixDbServerInterface *m_dbServer = nullptr;
  std::string m_connName;
  std::string m_cfgName;
  std::string m_cfgPref;
  std::string m_tagC;
  std::string m_tagP;
  std::string m_tagA;
  std::string m_domainName;
  std::string m_tagOI;
  std::string m_tagCF;
  std::string m_tagModCF;
  std::string m_tagCFOI;
  std::string m_tagCFOIcfg;
  std::string m_tagPen;
  std::string m_domainNameCF;
  unsigned int m_revCF;
  unsigned int m_revModCF;
  std::map<std::string, PixDbInterface*> m_dbs;
  std::map<std::string, DbRecord*> m_dbrs;
  bool m_connEmpty = true;
  bool m_connOwn;
  bool m_cfgUseCoral;
  bool m_cfgUpdate;
  bool m_cfgModUpdate;
  DFMutex* m_mutex;
  bool m_dbServerMode;
  bool m_dbServerModeCfg;
  std::map<std::string, std::string> m_types;
};

}

#endif
