/////////////////////////////////////////////////////////////////////
// PartitionConnectivity.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 05/10/06  Version 1.0 (PM)
//           Initial release

//! Class for partition connectivity

#ifndef _PIXLIB_PARTCONNOBJ
#define _PIXLIB_PARTCONNOBJ

#include "PixConnectivity/GenericConnectivityObject.h"

#include <map>

namespace PixLib {

class RodCrateConnectivity;

class PartitionConnectivity : public GenericConnectivityObject {
public:
  //! Payload accessors
  std::string &daqPartName() { return m_daqPartName; };
  std::string &ddcPartName() { return m_ddcPartName; };
  std::string &calibISServName() { return m_calibISServName; };
  std::string &ddcISServName() { return m_ddcISServName; };
  std::string &partConfig() { return m_partConfig; };
  //! Down links
  std::map<std::string, RodCrateConnectivity*> &rodCrates() { return m_rodCrates; };
  void addCrate(std::string slot, RodCrateConnectivity *cr);
  //! Up link 
  //! Soft links
  //! Internal connectivity tables
  //! Constructors
  PartitionConnectivity(std::string name);
  PartitionConnectivity(DbRecord *r, int lev);
  //! Config handling
  void initConfig();
  virtual void readConfig(DbRecord *r, int lev);
  virtual void writeConfig(DbRecord *r, int lev);
  // Link to configuration

private:
  std::map<std::string, RodCrateConnectivity*> m_rodCrates;
  std::string m_daqPartName;
  std::string m_ddcPartName;
  std::string m_calibISServName;
  std::string m_ddcISServName;
  std::string m_partConfig;
};

}

#endif
