/////////////////////////////////////////////////////////////////////
// PixConfigDb.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 27/09/14  Version 1.0 (PM)
//           Initial release

//! Helpers for connectivity db access

#ifndef _PIXLIB_PIXCFGDB
#define _PIXLIB_PIXCFGDB

#include "PixUtilities/PixLock.h"
#include "PixDbInterfaceFactory/PixDbInterfaceFactory.h"
#include "PixDbServer/PixDbServerInterface.h"
#include "CoralDB/CoralDB.h"

#include <string>
#include <map>
#include <vector>
#include <memory>
#include <time.h>

namespace PixLib {

class PixDbInterface;
class DbRecord;
class CoralDb;
class Config;
class PixConnectivity;

//! Configuration Exception class;
class PixConfigDbExc : public PixException {
 public:
 PixConfigDbExc(ErrorLevel level, std::string text) : PixException(level, text) {}; 
 PixConfigDbExc(ErrorLevel level, std::string type, std::string text) : PixException(level, type, text) {}; 
};

class CfgRevision {
public:
 CfgRevision() {};
 CfgRevision(unsigned int time, unsigned int cycle, std::string file) : m_revtime(time), m_cycle(cycle), m_file(file) {};
 CfgRevision(std::string rev, std::string file) : m_file(file) {
    std::istringstream is(rev.substr(0,rev.size()-3));
    is >> m_revtime;
    std::istringstream isr(rev.substr(rev.size()-3,3));
    isr >> m_cycle;
  };
  unsigned int time() const { return m_revtime; };
  unsigned int cycle() const { return m_cycle; };
  std::string fileName() const { return m_file; }; 
 private:
  unsigned int m_revtime;
  unsigned int m_cycle;
  std::string m_file;
};
 
class PixConfigDb {
public:
  // Constructor
  PixConfigDb(PixConnectivity *conn = NULL);
  ~PixConfigDb();

  //! Configuration handling
  void openCfg(std::string tagCF, std::string tagID);
  void openDbServer(PixDbServerInterface *DbServer);
  PixDbInterface* openDb(std::string name, std::string mode, PixDbCompoundTag t);
  void cfgFromDb() { m_dbServerModeCfg = false; };
  void cfgFromDbServ() { m_dbServerModeCfg = true; };
  void cfgToDb() { m_dbServerModeCfgWr = false; };
  void cfgToDbServ() { m_dbServerModeCfgWr = true; };
  void setCfgTag(std::string tagCF, std::string tagID);
  void addConnectivity(PixConnectivity *conn);

  bool checkCfgTag(std::string tagCF, std::string tagID);
  std::vector<std::string> listObjTags();
  std::vector<std::string> listCfgTags(std::string tagID);
  std::vector<std::string> listObjects(std::string tagCF, std::string tagID);
  std::string getObjectType(std::string object, std::string tagCF, std::string tagID);
  std::vector<unsigned int> listRevisions(std::string object, std::string tagCF, std::string tagID);
  std::vector<unsigned int> listCycles(std::string object, unsigned int rev, std::string tagCF, std::string tagID);

  void createCfgTag(std::string tagCF, std::string tagID);    
  bool checkCfg(Config *cfg, std::string object, unsigned int &rev, std::string tagCF, std::string tagID);
  void writeCfg(Config *cfg, std::string object, std::string tagCF, std::string tagID);
  void writeCfg(Config *cfg, std::string object, unsigned int &rev, std::string tagCF, std::string tagID);
  void writeCfg(Config *cfg, std::string object, unsigned int &rev, std::string tagCF, std::string tagID, std::string tagPEND);
  void readCfg(Config *cfg, std::string object, std::string tagCF, std::string tagID);
  void readCfg(Config *cfg, std::string object, unsigned int &rev, std::string tagCF, std::string tagID);
  void readCfg(Config *cfg, std::string object, unsigned int &rev, unsigned int &cycle, std::string tagCF, std::string tagID);

  // Expert-mode - maintenance
  void addCfg(std::vector<std::string> obj, std::vector<std::string> fName, std::vector<unsigned int> itim);
  void addCfg(std::string obj, std::string fileName, unsigned int rev);
  void addCfgNewBE(Config &conf, std::string obj, unsigned int rev=0, std::string type=""); //method for new backend
  
private:
  void init(PixConnectivity* conn, std::string tagCF, std::string tagID);
  bool checkObjectInConnectivity(std::string object, std::string tagCF, std::string tagID);
  //PixDbInterface* openDb(std::string name, std::string mode, PixDbCompoundTag t);
  //std::string getCfgFileName(std::string lobj, unsigned int &cfgRev, unsigned int &cfgRep);
  //bool isModule(std::string &lobj); 

  std::string element(std::string str, int num, char sep='|');

  CoralDB::CoralDB *m_cfgDb;
  PixDbServerInterface *m_dbServer;
  std::string m_cfgName;
  std::string m_cfgPref;
  PixMutex* m_mutex;
  bool m_dbServerModeCfg;
  bool m_dbServerModeCfgWr;
  bool m_haveConnectivity;
  PixConnectivity *m_conn;
  std::map<std::string, std::string> m_types;
  std::map<std::string, std::map<std::string, std::string> > m_objects;
  std::map<std::string, std::map<std::string, std::map<std::string, CfgRevision> > > m_revisions;
  std::map<std::string, CoralDB::ConnectionTableMap> m_ct;
  std::map<std::string, CoralDB::ObjectDictionaryMap> m_od;
  std::map<std::string, time_t> m_ctTime;
  std::map<std::string, time_t> m_odTime;
};

}

#endif
