/////////////////////////////////////////////////////////////////////
// GenericConnectivityObject.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 05/10/06  Version 1.0 (PM)
//           Initial release

//! Base class for connectivity objects

#ifndef _PIXLIB_GENCONNOBJ
#define _PIXLIB_GENCONNOBJ

#include "BaseException.h"

#include <string>
#include <assert.h>

namespace PixLib {

class DbRecord;
class PixDbInterface;
class Config;

//! Connectivity Exception class;
class ConnectivityExc : public SctPixelRod::BaseException {
public:
  enum ErrorLevel{INFO, WARNING, ERROR, FATAL};
  //! Constructor
  ConnectivityExc(ErrorLevel el, std::string name) : BaseException(name), m_errorLevel(el), m_name(name) {}; 
  //! Destructor
  virtual ~ConnectivityExc() {};

  //! Dump the error
  virtual void dump(std::ostream &out) {
    out << "Pixel Connectivity " << m_name << " -- Level : " << dumpLevel(); 
  }
  std::string dumpLevel() {
    switch (m_errorLevel) {
    case INFO : 
      return "INFO";
    case WARNING :
      return "WARNING";
    case ERROR :
      return "ERROR";
    case FATAL :
      return "FATAL";
    default :
      return "UNKNOWN";
    }
  }
  //! m_errorLevel accessor
  ErrorLevel getErrorLevel() { return m_errorLevel; };
  //! m_name accessor
  std::string getCtrlName() { return m_name; };
private:
  ErrorLevel m_errorLevel;
  std::string m_name;
};

class GenericConnectivityObject {
public:
  //! Identifier
  std::string &name() { return m_name; };
  //! Config object
  Config &config() { return *m_config; };
  
  //! Constructors
  GenericConnectivityObject(std::string name) : m_name(name), m_db(NULL), m_recName(""), m_active(true) {};
  GenericConnectivityObject(DbRecord *r);
  //! Destructor
  virtual ~GenericConnectivityObject();
  //! Config handling
  virtual void readConfig(int lev);
  virtual void readConfig(DbRecord *r, int lev);
  virtual void writeConfig(int lev);
  virtual void writeConfig(DbRecord *r, int lev);
  virtual std::string decName();
  virtual void storeRec(DbRecord *r);
  virtual DbRecord *getRec();
  bool &active() { return m_active; };

protected:
  std::string m_name;      //! Object name
  PixDbInterface *m_db;    //! DB reference
  std::string m_recName;   //! DB record name
  Config *m_config;        //! Config object
  bool m_active;           //! Temp activation flag
};

}

#endif
