/////////////////////////////////////////////////////////////////////
// TimConnectivity.cxx
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 05/10/06  Version 1.0 (PM)
//           Initial release

//! Class for TIM connectivity

#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixConnectivity/TimConnectivity.h"
#include "PixDbInterface/PixDbInterface.h"
#include "Config/Config.h"

using namespace PixLib;

TimConnectivity::TimConnectivity(std::string name) : 
  GenericConnectivityObject(name), m_crate(NULL) {
  // Create the config object
  initConfig(); 
}

TimConnectivity::TimConnectivity(DbRecord *r, int lev) : 
  GenericConnectivityObject(r), m_crate(NULL) {
  if (r->getClassName() == "TIM") {
    // Read record from DB
    storeRec(r);
    // Create the config object
    initConfig();
    readConfig(getRec(), lev); 
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a TimConnectivity object");
  }
}

void TimConnectivity::initConfig() { 
  m_config = new Config(m_name, "TIM");
  Config &conf = *m_config;

  //conf.addGroup("Identifier");
  //conf["Identifier"].addString("Id", m_name, m_name, "Object identifier", true);
  conf.addGroup("Config");
  conf["Config"].addString("timConfig", m_timConfig, "NULL", "Link to TIM configuration", true);
  //conf.addGroup("Up");
  //conf["Up"].addInt("VmeSlot", m_vmeSlot, 0, "Vme slot", true);
  conf.addGroup("Enable");
  conf["Enable"].addBool("readout", enableReadout, 1, "TIM readout enable", true);
  conf.reset();
}

void TimConnectivity::readConfig(DbRecord *r, int lev) { 
  if (r->getClassName() == "TIM") {
    storeRec(r);
    m_name = r->getName();
    // Read the config object
    m_config->read(r); 
    lev = 0;
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a TimConnectivity object");
  }
}

void TimConnectivity::writeConfig(DbRecord *r, int lev) { 
  if (r->getClassName() == "TIM") {
    storeRec(r);
    // Read the config object
    m_config->write(r);
    lev = 0;
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a TimConnectivity object");
  }
}

void TimConnectivity::linkCrate(RodCrateConnectivity *cr, int slot) {
  m_crate = cr;
  m_vmeSlot = slot;
}
