/////////////////////////////////////////////////////////////////////
// TimConnectivity.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 05/10/06  Version 1.0 (PM)
//           Initial release

//! Class for TIM connectivity

#ifndef _PIXLIB_TIMCONNOBJ
#define _PIXLIB_TIMCONNOBJ

#include "PixConnectivity/GenericConnectivityObject.h"

#include <map>

namespace PixLib {

class RodCrateConnectivity;

class TimConnectivity : public GenericConnectivityObject {
public:
  //! Payload accessors
  int &vmeSlot() { return m_vmeSlot; };
  //! Enable parameters
  bool enableReadout;
  //! Down links
  //! Up link
  RodCrateConnectivity *crate() { return m_crate; };
  void linkCrate(RodCrateConnectivity *cr, int slot);
  //! Soft links
  std::string &timConfig() { return m_timConfig; };
  //! Internal connectivity tables
  //! Constructors
  TimConnectivity(std::string name);
  TimConnectivity(DbRecord *r, int lev);
  //! Config handling
  void initConfig();
  virtual void readConfig(DbRecord *r, int lev);
  virtual void writeConfig(DbRecord *r, int lev);
  //! Link to configuration
private:
  std::string m_timConfig;
  RodCrateConnectivity *m_crate;
  int m_vmeSlot;
};

}

#endif
