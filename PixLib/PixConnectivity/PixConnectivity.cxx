/////////////////////////////////////////////////////////////////////
// PixConnectivity.cxx
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 12/11/06  Version 1.0 (PM)
//           Initial release

//! Helpers for connectivity db accss

#include "PixConnectivity/PixConnectivity.h"
#include "PixConnectivity/GenericConnectivityObject.h"
#include "PixConnectivity/PartitionConnectivity.h"
#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixConnectivity/RodBocConnectivity.h"
#include "PixConnectivity/TimConnectivity.h"
#include "PixConnectivity/SbcConnectivity.h"
#include "PixConnectivity/OBConnectivity.h"
#include "PixConnectivity/Pp0Connectivity.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "PixConnectivity/RosConnectivity.h"
#include "PixConnectivity/PixDisable.h"
#include "RootDb/RootDb.h"
#include "PixModule/PixModule.h"
#include "PixTrigController/TimPixTrigController.h"
#include "PixController/PixScan.h"

#include <DFThreads/DFMutex.h>
#include <sys/stat.h>

//include for new backend
#include "ConfigRootIO/ConfigStoreFactory.h"
#include "ConfigRootIO/ConfigStore.h"

//#include "ConfigWrapper/createDbServer.h"
//#include "ConfigRootIO/test_compareConfObj.h"


using namespace PixLib;

std::string element(std::string str, int num, char sep='|') {
  unsigned int ip;
  int start = -1,end = -1;
  int nel = -1;
  bool spc = true;
  bool quote = false;
                                                                                                                                        
  for (ip=0;ip<str.size();ip++) {
    if (str[ip] == '"') quote = !quote;
    if (str[ip] == sep || (sep == 0 && 
       (str[ip] == 9 || str[ip]==10 || str[ip] == 32))) {
      if (!spc && !quote) {
        spc = true;
        if (nel==num) end = ip-1;
      }
    } else {
      if (spc) {
        spc = false;
        nel++;
        if (nel==num) start = ip;
      }
    }
  }
  if (!spc && nel==num) end = str.size()-1;
                                                                                                                                        
  if (start >= 0) {
    std::string c;
    std::string s = str.substr(start,end-start+1);
    for (ip=0;ip<s.size();ip++) {
      if (s[ip] != '"') c += s[ip];
    }
    return c;
  } else {
    return "";
  }
}

PixConnectivity::PixConnectivity() {
  m_tagC = "Def";
  m_tagP = "Def";
  m_tagA = "Def";
  m_tagCF = "Def";
  m_tagModCF = "Def";
  m_tagOI = "Def";
  m_tagCFOI = "Def";
  m_tagCFOIcfg = "Def-CFG";
  m_domainName = "Connectivity-"+m_tagOI;
  m_domainNameCF = "Configuration-"+m_tagCFOI;
  m_tagPen = "Generic_Tmp";
  m_revCF = 0xffffffff;
  m_revModCF = 0xffffffff;
  m_cfgUseCoral = false;
  m_dbServerMode = false;
  m_dbServerModeCfg = false;
  // Create mutex
  m_mutex = DFMutex::Create((char*)"PixConnectivity_Mutex"); // TODO: remove DFMutex
  m_mutex->lock();
  m_connOwn = true;
  init(PixDbCompoundTag());
  m_mutex->unlock();
}

PixConnectivity::PixConnectivity(std::string tagC, std::string tagP, std::string tagA, std::string tagCF, std::string tagOI, std::string tagModCF, bool open) {
  m_tagC = tagC;
  m_tagP = tagP;
  m_tagA = tagA;
  m_tagCF = tagCF;
  m_tagModCF = tagModCF;
  if (m_tagModCF == "") m_tagModCF = m_tagCF;
  m_tagOI = tagOI;
  m_tagCFOI = tagOI;
  m_tagCFOIcfg = m_tagCFOI+"-CFG";
  m_domainName = "Connectivity-"+m_tagOI;
  m_domainNameCF = "Configuration-"+m_tagCFOI;
  m_tagPen = "Generic_Tmp";
  m_revCF = 0xffffffff;
  m_revModCF = 0xffffffff;
  m_cfgUseCoral = false;
  m_dbServerMode = false;
  m_dbServerModeCfg = false;
  // Create mutex
  m_mutex = DFMutex::Create((char *)"PixConnectivity_Mutex"); // remove DFMutex
  m_mutex->lock();
  m_connOwn = true;
  init(PixDbCompoundTag(tagOI, tagC, tagP, tagA), open);
  m_mutex->unlock();
}

PixConnectivity::PixConnectivity(PixDbServerInterface *DbServer, std::string tagC, std::string tagP, std::string tagA, std::string tagCF, std::string tagOI, std::string tagModCF, bool open) {
  m_tagC = tagC;
  m_tagP = tagP;
  m_tagA = tagA;
  m_tagCF = tagCF;
  m_tagModCF = tagModCF;
  if (m_tagModCF == "") m_tagModCF = m_tagCF;
  m_tagOI = tagOI;
  m_tagCFOI = tagOI;
  m_tagCFOIcfg = m_tagCFOI+"-CFG";
  m_domainName = "Connectivity-"+m_tagOI;
  m_domainNameCF = "Configuration-"+m_tagCFOI;
  m_tagPen = "Generic_Tmp";
  m_revCF = 0xffffffff;
  m_revModCF = 0xffffffff;
  m_cfgUseCoral = false;
  m_dbServer = DbServer;
  m_dbServerMode = true;
  m_dbServerModeCfg = true;
  // Create mutex
  m_mutex = DFMutex::Create((char *)"PixConnectivity_Mutex"); // TODO: remove DFMutex
  m_mutex->lock();
  m_connOwn = true;
  init(PixDbCompoundTag(tagOI, tagC, tagP, tagA), open);
  m_mutex->unlock();
}

PixConnectivity::PixConnectivity(PixConnectivity* conn, std::string rodName) {
  m_tagC = conn->m_tagC;
  m_tagP = conn->m_tagP;
  m_tagA = conn->m_tagA;
  m_tagCF = conn->m_tagCF;
  m_tagModCF = conn->m_tagModCF;
  if (m_tagModCF == "") m_tagModCF = m_tagCF;
  m_tagOI = conn->m_tagOI;
  m_tagCFOI = conn->m_tagCFOI;
  m_tagCFOIcfg = m_tagCFOI+"-CFG";
  m_domainName = "Connectivity-"+m_tagOI;
  m_domainNameCF = "Configuration-"+m_tagCFOI;
  m_tagPen = conn->m_tagPen;
  m_revCF = conn->m_revCF;
  m_revModCF = conn->m_revModCF;
  m_cfgUseCoral = conn->m_cfgUseCoral;
  m_dbServer = conn->m_dbServer;
  m_dbServerMode = conn->m_dbServerMode;
  m_dbServerModeCfg = conn->m_dbServerModeCfg;
  // Create mutex
  m_mutex = DFMutex::Create((char *)"PixConnectivity_Mutex"); // TODO: remove DFMutex
  m_mutex->lock();
  m_connOwn = false;
  init(PixDbCompoundTag(m_tagOI, m_tagC, m_tagP, m_tagA));
  m_mutex->unlock();
  cloneConn(conn, rodName);
}

//temporary
void fillLink(std::string linkName, std::string objName, std::string objType, unsigned int revision, PixDbDomain::link_object &link) { 
  link.linkName=linkName;
  link.objName=objName;
  link.objType=objType;
  link.revision=revision;
}
// all the uplink will have revision=0

void PixConnectivity::dumpInDb(PixDbServerInterface *DbServer) {
  bool result;
  unsigned int revision;
  std::string tagC = m_tagC;

  std::map<std::string, RosConnectivity*>::iterator Iros;
  for (Iros=roses.begin();Iros!=roses.end();Iros++) {
    // ros content
    PixDbDomain::rev_content contRO;
    (Iros->second->config()).write(contRO);

    for(unsigned int i=0; i<=RosConnectivity::nRobinsPerRos; i++) {
      RobinConnectivity *rb = Iros->second->robins(i);
      if (rb != NULL) {
	PixDbDomain::link_object link1;
	std::ostringstream slot1;
	slot1 << i;
        // robin content
	PixDbDomain::rev_content contRB;
	(rb->config()).write(contRB);
	// robin uplink
	fillLink("ROS",(*Iros).first,"ROS",0,link1);
	contRB.upLink_content.push_back(link1);

	for(unsigned int j=0; j<=RobinConnectivity::nRoLinksPerRobin; j++) {
	  RolConnectivity *rl = rb->rols(j);
	  if (rl != NULL) {
	    PixDbDomain::link_object link2, link3;
	    std::ostringstream slot2;
	    slot2 << j;
	    // rol content	  
	    PixDbDomain::rev_content contRL;
	    (rl->config()).write(contRL); 
            // rol uplink
	    fillLink("ROBIN",rb->name(),"ROBIN",0,link2);  
	    contRL.upLink_content.push_back(link2);
            // rol link (ROD)
	    if (rl->rod() != NULL) {
	      fillLink("ROD",rl->rod()->name(),"ROD_BOC",0,link3);
	      contRL.downLink_content.push_back(link3);
	    }
	    result=DbServer->writeObject(contRL,m_domainName, tagC, rl->name(),"ROL",revision);
	    if (!result) {
	      std::cout << " ERROR: problem in writing ROL " << rl->name() << std::endl;
	    } else {
	      fillLink(slot2.str(),rl->name(),"ROL",revision,link2);
	      contRB.downLink_content.push_back(link2);
	    }
	  }
	}    
	result=DbServer->writeObject(contRB,m_domainName, tagC, rb->name(),"ROBIN",revision);
	if (!result) {
	  std::cout << " ERROR: problem in writing ROBIN " << rb->name() << std::endl;
	} else {
	  fillLink(slot1.str(),rb->name(),"ROBIN",revision,link1);
	  contRO.downLink_content.push_back(link1);
	}      
      }
    }
    result=DbServer->writeObject(contRO,m_domainName, tagC, (*Iros).first,"ROS",revision);
    if (!result) {
      std::cout << " ERROR: problem in writing ROS " << (*Iros).first  << " with tag " <<tagC << std::endl; 
    }
  }

  std::map<std::string, PartitionConnectivity*>::iterator Ipar;
  for (Ipar=parts.begin();Ipar!=parts.end();Ipar++) {
    PixDbDomain::rev_content contP;
    ((*Ipar).second->config()).write(contP);
    std::cout << " written partition " << (*Ipar).first << std::endl;
    std::map<std::string, RodCrateConnectivity*>::iterator Icr;

    for ( Icr=((((*Ipar).second)->rodCrates()).begin()); Icr!=((((*Ipar).second)->rodCrates()).end()); Icr++) {
      PixDbDomain::link_object link1;
       
      //slave object content
      PixDbDomain::rev_content contC;
      ((*Icr).second->config()).write(contC);
      std::cout << " written rod_boc " << (*Icr).first << std::endl;
     
      // slave object uplink
      fillLink("PARTITION",(*Ipar).first,"PARTITION",0,link1);
      contC.upLink_content.push_back(link1);
     

      TimConnectivity* tim=((*Icr).second)->tim();
      if (tim!=NULL) {
	std::string timName=tim->name();
	std::ostringstream slot;
	slot <<((*Icr).second)->timSlot(); 
      
	PixDbDomain::rev_content contT;
	(((*Icr).second)->tim()->config()).write(contT);
	std::cout << " written tim " << timName << std::endl;
	fillLink("ROD_CRATE",(*Icr).first,"ROD_CRATE",0,link1);
	contT.upLink_content.push_back(link1);
	
	result=DbServer->writeObject(contT,m_domainName, tagC,timName,"TIM",revision);
	
	if (!result) std::cout << " ERROR: problem in writing TIM " <<   timName << std::endl;
	else {
	  fillLink(slot.str(),timName,"TIM",revision,link1);
	  contC.downLink_content.push_back(link1);
	}
      } else 
	std::cout << "Crate " << (*Icr).first << " does not contain Tim!! " << std::endl;
      
      std::string sbcName="NONE";
      std::ostringstream slot;
      slot<<((*Icr).second)->sbcSlot();
      result = false;
      if(((*Icr).second)->sbc()!=0){
	sbcName = ((*Icr).second)->sbc()->name();
	
	PixDbDomain::rev_content contS;
	(((*Icr).second)->sbc()->config()).write(contS);
	std::cout << " written sbc " << sbcName << std::endl;
	fillLink("ROD_CRATE",(*Icr).first,"ROD_CRATE",0,link1);
	contS.upLink_content.push_back(link1);
	result=DbServer->writeObject(contS,m_domainName, tagC,sbcName,"SBC",revision);
      }
      
      if (!result) std::cout << " ERROR: problem in writing SBC " << sbcName << std::endl;
      else {
	fillLink(slot.str(),sbcName,"SBC",revision,link1);
	contC.downLink_content.push_back(link1);
      }
      
      std::map<int, RodBocConnectivity*>::iterator Iboc;
      for ( Iboc=((((*Icr).second)->rodBocs()).begin()); Iboc!=((((*Icr).second)->rodBocs()).end()); Iboc++) {
	//master object downlink
	std::cout << " ROB_BOC in slot " << (*Iboc).first << std::endl;
 	std::string bocName=((*Iboc).second)->name();
	
	PixDbDomain::link_object link2;
	std::ostringstream slot2;
	slot2 <<((*Iboc).second)->vmeSlot();
	
	PixDbDomain::rev_content contB;
	(((*Iboc).second)->config()).write(contB); 
	std::cout << " written rodBoc " << bocName << std::endl;
	fillLink("ROD_CRATE",(*Icr).first,"ROD_CRATE",0,link2);  
	contB.upLink_content.push_back(link2);
	
	
	if (((*Iboc).second)->linkMap()==NULL) {
	  std::cout << " pointer to linkMap for rodboc " << bocName << " is NULL " << std::endl;
	} else {
	  std::string linkmapName=((*Iboc).second)->linkMap()->name();
	  PixDbDomain::rev_content contLM;
	  
	  (((*Iboc).second)->linkMap()->config()).write(contLM);
	  std::cout << " written linkMap " << linkmapName << std::endl;
	  fillLink("ROD_BOC",bocName,"ROD_BOC",0,link2);
	  contLM.upLink_content.push_back(link2);
	  result=DbServer->writeObject(contLM,m_domainName, tagC, bocName+"/"+linkmapName,"ROD_BOC_LINKMAP",revision);
	  if (!result) {
	    std::cout << " ERROR: problem in writing ROD_BOC_LINKMAP " << linkmapName << std::endl;
	  } else {
	    fillLink("ROD_BOC_LINKMAP",linkmapName,"ROD_BOC_LINKMAP",revision,link2);
	    contB.downLink_content.push_back(link2);
	  }
	}
        // rol link
	if (Iboc->second->rol() != NULL) {
	  PixDbDomain::link_object link;
	  fillLink("ROL",Iboc->second->rol()->name(),"ROL",0,link);
	  contB.downLink_content.push_back(link);
	}

	for(int i=0; i<=4; i++) {
	  OBConnectivity* obsconn=((*Iboc).second)->obs(i);
	  if (obsconn!=NULL) {
	    std::string obName=obsconn->name();
	    PixDbDomain::link_object link3;
	    std::ostringstream slot3;
	    slot3 << obsconn->bocTxSlot() << " " <<  obsconn->bocRxSlot(0) <<" " << obsconn->bocRxSlot(1);  
            //  std::cout << " slot string is " << slot3.str() << std::endl;
	    
	    PixDbDomain::rev_content contOB;
	    (obsconn->config()).write(contOB); 
	    std::cout << " written Optoboard " << obName << std::endl;
	    fillLink("ROD_BOC",bocName,"ROD_BOC",0,link2);   
	    contOB.upLink_content.push_back(link2);
	    
	    if (obsconn->linkMap()==NULL) {
	      std::cout << " pointer to linkmap for optoboard " << obName << " is NULL " << std::endl;
	    } else {
	      std::string OBlinkmapName=obsconn->linkMap()->name();
	      PixDbDomain::rev_content contOBLM;
	      
	      obsconn->linkMap()->config().write(contOBLM);
	      std::cout << " written OptoboardLinkMap " << OBlinkmapName << std::endl;
	      fillLink("OPTOBOARD",obName,"OPTOBOARD",0,link3);
	      contOBLM.upLink_content.push_back(link3);
	      result=DbServer->writeObject(contOBLM,m_domainName, tagC, obName+"/"+OBlinkmapName,"OB_LINKMAP",revision);
	      if (!result) {
		std::cout << " ERROR: problem in writing OB_LINKMAP " << OBlinkmapName << std::endl;
	      } else {
		fillLink("OB_LINKMAP",OBlinkmapName,"OB_LINKMAP",revision,link3);   
		contOB.downLink_content.push_back(link3);
	      }
	    }	    
	    if (obsconn->pp0()==NULL) {
	      std::cout << " pointer to pp0 for optoboard " << obName << " is NULL " << std::endl;
	    } else {
	      std::string pp0Name=obsconn->pp0()->name();
	      PixDbDomain::rev_content contPP0;      
	      obsconn->pp0()->config().write(contPP0);
	      std::cout << " written pp0 " << pp0Name << std::endl;
	      fillLink("OPTOBOARD",obName,"OPTOBOARD",0,link3);
	      contPP0.upLink_content.push_back(link3);
	      
	      for(int k=1; k<=8; k++) {
		ModuleConnectivity* mod=obsconn->pp0()->modules(k);
		if (mod!=NULL) {
		  std::string modName=mod->name();
		  PixDbDomain::link_object link4;
		  std::ostringstream slot4;
		  slot4 << mod->pp0Slot();
		  
		  PixDbDomain::rev_content contM;
		  (mod->config()).write(contM);
		  std::cout << " written module " << modName << std::endl;
		  fillLink("PP0",pp0Name,"PP0",0,link4);
		  contM.upLink_content.push_back(link4);
		  
		  result=DbServer->writeObject(contM,m_domainName, tagC, modName,"MODULE",revision);
		  if (!result) {
		    std::cout << " ERROR: problem in writing MODULE " << pp0Name << std::endl;
		  } else {
		    fillLink(slot4.str(),modName,"MODULE",revision,link4);
		    contPP0.downLink_content.push_back(link4);
		  }
		} else {
		  std::cout << " no MODULE in slot " << k << " for pp0 " << pp0Name << std::endl;
		}
	      }
	      result=DbServer->writeObject(contPP0,m_domainName, tagC, pp0Name,"PP0",revision);
	      if (!result) {
		std::cout << " ERROR: problem in writing PP0 " << pp0Name << std::endl;
	      } else {
		fillLink("PP0",pp0Name,"PP0",revision,link3);   
		contOB.downLink_content.push_back(link3);
	      }
	    }
	    result=DbServer->writeObject(contOB,m_domainName, tagC, obName,"OPTOBOARD",revision);
	    if (!result) std::cout << " ERROR: problem in writing OPTOBOARD " << obName << std::endl;
	    else {
	      fillLink(slot3.str(),obName,"OPTOBOARD",revision,link3);   
	      contB.downLink_content.push_back(link3);
	    }
	  } else { 
	    std::cout << " no OPTOBOARD in slot " << i << " for rod_boc " << bocName << std::endl;
	  }
	}
	result=DbServer->writeObject(contB,m_domainName, tagC, bocName,"ROD_BOC",revision);
	if (!result) {
	  std::cout << " ERROR: problem in writing ROD_BOC " << bocName << std::endl;
	} else {
	  fillLink(slot2.str(),bocName,"ROD_BOC",revision,link2);
	  contC.downLink_content.push_back(link2);
	}
      }    
      result=DbServer->writeObject(contC,m_domainName, tagC, (*Icr).first,"ROD_CRATE",revision);
      if (!result) {
	std::cout << " ERROR: problem in writing ROD_CRATE " << (*Icr).first << std::endl;
      } else {
	fillLink("ROD_CRATE",(*Icr).first,"ROD_CRATE",revision,link1);
	contP.downLink_content.push_back(link1);
      }      
    }
    result=DbServer->writeObject(contP,m_domainName, tagC, (*Ipar).first,"PARTITION",revision);
    if (!result) {
      std::cout << " ERROR: problem in writing PARTITION " << (*Ipar).first  << " with tag " <<tagC << std::endl; 
    }
  }
}

void PixConnectivity::loadConnDbServer() {
  
  PixDbServerInterface *DbServer = m_dbServer;
  std::vector<std::string> taglist;
  std::string tagC = m_tagC;

  DbServer->listTagsRem(m_domainName,taglist);
  bool found=false;
  bool result;
  for (unsigned int i=0; i<taglist.size(); i++) {
    if(taglist[i]==tagC)
      found=true;
  }
  if (!found)
    throw(PixConnectivityExc(PixConnectivityExc::FATAL, "NOTAGINDBSERVER", " Global Tag not found in dbServer "));

  std::vector<std::string> objlist;
  std::vector<std::string>::iterator objit;

  DbServer->listObjectsRem(m_domainName,tagC,"ROS",objlist);
  for(objit=objlist.begin();objit!=objlist.end();objit++) {
    RosConnectivity *tmpRos=new RosConnectivity((*objit));
    PixDbDomain::rev_content contRO;
    result=DbServer->getObject(contRO, m_domainName, tagC,(*objit));
    if (!result) throw(PixConnectivityExc(PixConnectivityExc::FATAL, "DOWNLOADFAILED", " Impossible to download informatio for object "+(*objit)));
    (tmpRos->config()).read(contRO);
    
    for(unsigned int rob=0;rob<contRO.downLink_content.size();rob++) {
      std::string robName=contRO.downLink_content[rob].objName;
      RobinConnectivity *tmpRob=new RobinConnectivity(robName);
      PixDbDomain::rev_content contRB;
      result=DbServer->getObject(contRB, m_domainName, tagC,robName);
      if (!result) throw(PixConnectivityExc(PixConnectivityExc::FATAL, "DOWNLOADFAILED", " Impossible to download informatio for object "+robName));
      (tmpRob->config()).read(contRB);
      
      for(unsigned int rol=0;rol<contRB.downLink_content.size();rol++) {
	std::string rolName=contRB.downLink_content[rol].objName;
	RolConnectivity *tmpRol=new RolConnectivity(rolName);
	PixDbDomain::rev_content contRL;
	result=DbServer->getObject(contRL, m_domainName, tagC, rolName);
	if (!result) throw(PixConnectivityExc(PixConnectivityExc::FATAL, "DOWNLOADFAILED", " Impossible to download informatio for object "+rolName));
	(tmpRol->config()).read(contRL);
	int slot = atoi((contRB.downLink_content[rol].linkName).c_str());
	tmpRob->addRol(slot,tmpRol);
	rols[rolName]=tmpRol;
      }
      int slot = atoi((contRO.downLink_content[rob].linkName).c_str());
      tmpRos->addRobin(slot,tmpRob);
      robins[robName]=tmpRob;
    }    
    roses[(*objit)]=tmpRos;
  }
   	
  DbServer->listObjectsRem(m_domainName,tagC,"PARTITION",objlist);
  for(objit=objlist.begin();objit!=objlist.end();objit++) {
    //std::cout << " ... loading PARTITION " << (*objit) << std::endl;
    PartitionConnectivity *tmpPart=new PartitionConnectivity((*objit));
    PixDbDomain::rev_content contP;
    result=DbServer->getObject(contP, m_domainName, tagC,(*objit));
    if (!result) throw(PixConnectivityExc(PixConnectivityExc::FATAL, "DOWNLOADFAILED", " Impossible to download informatio for object "+(*objit)));
    (tmpPart->config()).read(contP);
    
    for(unsigned int cr=0;cr<contP.downLink_content.size();cr++) {
      std::string crateName=contP.downLink_content[cr].objName;
      //std::cout << " ... loading ROD_CRATE " << crateName << std::endl;
      RodCrateConnectivity *tmpCrate=new RodCrateConnectivity(crateName);
      PixDbDomain::rev_content contC;
      result=DbServer->getObject(contC, m_domainName, tagC,crateName);
      if (!result) throw(PixConnectivityExc(PixConnectivityExc::FATAL, "DOWNLOADFAILED", " Impossible to download informatio for object "+crateName));
      (tmpCrate->config()).read(contC);
      
      for(unsigned int dl=0;dl<contC.downLink_content.size();dl++) {
	std::string name=contC.downLink_content[dl].objName;
	if (contC.downLink_content[dl].objType=="SBC") {
	  //std::cout << " ... loading SBC " << name << std::endl;
	  SbcConnectivity *tmpSbc=new SbcConnectivity(name);
	  PixDbDomain::rev_content contS;
	  result=DbServer->getObject(contS, m_domainName, tagC,name);
	  if (!result) throw(PixConnectivityExc(PixConnectivityExc::FATAL, "DOWNLOADFAILED", " Impossible to download informatio for object "+name));
	  (tmpSbc->config()).read(contS);
	  int slot;
	  slot= atoi((contC.downLink_content[dl].linkName).c_str());
	  tmpCrate->addSbc(slot,tmpSbc);
	  sbcs[name]=tmpSbc;
	}
	
	if (contC.downLink_content[dl].objType=="TIM") {
	  //std::cout << " ... loading TIM " << name << std::endl;
	  TimConnectivity *tmpTim=new TimConnectivity(name);
	  PixDbDomain::rev_content contT;
	  result=DbServer->getObject(contT, m_domainName, tagC,name);
	  if (!result) throw(PixConnectivityExc(PixConnectivityExc::FATAL, "DOWNLOADFAILED", " Impossible to download informatio for object "+name));
	  (tmpTim->config()).read(contT);
	  int slot;
	  slot= atoi((contC.downLink_content[dl].linkName).c_str());
	  tmpCrate->addTim(slot,tmpTim);
	  tims[name]=tmpTim;
	}
	
	if (contC.downLink_content[dl].objType=="ROD_BOC") {
	  //std::cout << " ... loading ROD_BOC " << name << std::endl;
	  RodBocConnectivity *tmpRod=new RodBocConnectivity(name);
	  PixDbDomain::rev_content contR;
	  result=DbServer->getObject(contR, m_domainName, tagC,name);
	  if (!result) throw(PixConnectivityExc(PixConnectivityExc::FATAL, "DOWNLOADFAILED", " Impossible to download informatio for object "+name));
	  (tmpRod->config()).read(contR);
	  
	  for(unsigned int dlr=0;dlr<contR.downLink_content.size();dlr++) {
	    std::string namer=contR.downLink_content[dlr].objName;
	    if (contR.downLink_content[dlr].objType=="ROD_BOC_LINKMAP") {
	      //std::cout << " ... loading ROD_BOC_LINKMAP " << namer << std::endl;
	      RodBocLinkMap *tmpMap=new RodBocLinkMap(namer);
	      PixDbDomain::rev_content contLM;
	      result=DbServer->getObject(contLM, m_domainName, tagC,name+"/"+namer);
	      if (!result) throw(PixConnectivityExc(PixConnectivityExc::FATAL, "DOWNLOADFAILED", " Impossible to download informatio for object "+namer));
	      (tmpMap->config()).read(contLM);
	      tmpRod->linkMap(tmpMap);  
	      bocmaps[namer]=tmpMap;
	    } else if (contR.downLink_content[dlr].objType=="ROL") {
	      if (rols.find(contR.downLink_content[dlr].objName) != rols.end()) {
		tmpRod->addRol(rols[contR.downLink_content[dlr].objName]);
	      }
	    }
	    
	    if (contR.downLink_content[dlr].objType=="OPTOBOARD") {
	      //std::cout << " ... loading OPTOBOARD " << namer << std::endl;
	      OBConnectivity *tmpOB=new OBConnectivity(namer);
	      PixDbDomain::rev_content contOB;
	      result=DbServer->getObject(contOB, m_domainName, tagC,namer);
	      if (!result) throw(PixConnectivityExc(PixConnectivityExc::FATAL, "DOWNLOADFAILED", " Impossible to download informatio for object "+namer));
	      (tmpOB->config()).read(contOB);
	     
	      for(unsigned int dlob=0;dlob<contOB.downLink_content.size();dlob++) {
		std::string nameob=contOB.downLink_content[dlob].objName;
		if (contOB.downLink_content[dlob].objType=="OB_LINKMAP") {
		  //std::cout << " ... loading OB_LINKMAP " << nameob << std::endl;
		  OBLinkMap *tmpOBMap=new OBLinkMap(nameob);
		  PixDbDomain::rev_content contOBM;
		  result=DbServer->getObject(contOBM, m_domainName, tagC,namer+"/"+nameob);
		  if (!result) throw(PixConnectivityExc(PixConnectivityExc::FATAL, "DOWNLOADFAILED", " Impossible to download informatio for object "+nameob));
		  (tmpOBMap->config()).read(contOBM);
		  tmpOB->linkMap(tmpOBMap);
		  obmaps[nameob]=tmpOBMap;
		}

		if (contOB.downLink_content[dlob].objType=="PP0") {
		  //std::cout << " ... loading PP0 " << nameob << std::endl;
		  
		  Pp0Connectivity *tmpPP0=new Pp0Connectivity(nameob);
		  PixDbDomain::rev_content contPP0;
		  result=DbServer->getObject(contPP0, m_domainName, tagC,nameob);
		  if (!result) throw(PixConnectivityExc(PixConnectivityExc::FATAL, "DOWNLOADFAILED", " Impossible to download informatio for object "+nameob));
		  (tmpPP0->config()).read(contPP0);
		  
		  for(unsigned int dlpp=0;dlpp<contPP0.downLink_content.size();dlpp++) {
		    std::string modname=contPP0.downLink_content[dlpp].objName;
		    //std::cout << " ... loading Module " << modname << std::endl;
		    ModuleConnectivity *tmpMod=new ModuleConnectivity(modname);
		    PixDbDomain::rev_content contM;
		    result=DbServer->getObject(contM, m_domainName, tagC,modname);
		    if (!result) throw(PixConnectivityExc(PixConnectivityExc::FATAL, "DOWNLOADFAILED", " Impossible to download informatio for object "+modname));
		    (tmpMod->config()).read(contM);
		    
		    int slotmod;
		    slotmod= atoi((contPP0.downLink_content[dlpp].linkName).c_str());
		    tmpPP0->addModule(slotmod ,tmpMod);
		    mods[modname]=tmpMod;
		  }
		  
		  tmpOB->addPp0(tmpPP0);
		  pp0s[nameob]=tmpPP0;
		}
	      }
	      std::istringstream tmpr((contR.downLink_content[dlr].linkName).c_str());
	      int tx, rx0, rx1;
	      tmpr >> tx >> rx0  >> rx1;
	      tmpRod->addOB(tx,rx0,rx1,tmpOB);
	      obs[namer]=tmpOB; 
	    }
	  }
	  int slot;
	  slot= atoi((contC.downLink_content[dl].linkName).c_str());
	  //std::cout << " before adding rod " << name << " in slot " << slot << std::endl; 
	  tmpCrate->addRodBoc(slot,tmpRod);
	  rods[name]=tmpRod;
	}
      }
      tmpPart->addCrate(crateName,tmpCrate);
      crates[crateName]=tmpCrate;
    }    
    parts[(*objit)]=tmpPart;
  }
}


void PixConnectivity::init(PixDbCompoundTag t1, bool open) {
  // Read env variables
  char *fname;
  fname = getenv("PIX_CONN_DB");
  if (fname == NULL) {
    throw(PixConnectivityExc(PixConnectivityExc::FATAL, "NOCONNENV", "PIX_CONN_DB env variable not defined"));
  }
  m_connName = fname;
  fname = getenv("PIX_CFG_DB");
  if (fname == NULL) {
    throw(PixConnectivityExc(PixConnectivityExc::FATAL, "NOCFGENV", "PIX_CFG_DB env variable not defined"));
  }
  m_cfgName = fname;
  fname = getenv("PIX_CFG_PREF");
  if (fname == NULL) {
    throw(PixConnectivityExc(PixConnectivityExc::FATAL, "NOCFGPREFENV", "PIX_CFG_PREF env variable not defined"));
  }
  m_cfgPref = fname;

  if (!m_dbServerMode && open) {
    try {
      m_connDb = openDb(m_connName, "READ", t1);
    }
    catch (...) {
      m_connDb = NULL;
      try { throw; } 
      catch(std::exception& e) {
	throw(PixConnectivityExc(PixConnectivityExc::FATAL, "CANTOPENCONN", string("Cannot open connectivity db")+e.what()));
      }
      catch(PixDBException& e) {
	std::ostringstream os;
	os<<"Cannot open connectivity db: ";
	e.what(os);
	throw(PixConnectivityExc(PixConnectivityExc::FATAL, "CANTOPENCONN", os.str()));
      }
    }
  }
  
  if (!m_dbServerModeCfg) {
    openCfg("READ");
  }

}


PixConnectivity::~PixConnectivity() {
  m_mutex->lock();

  std::map<std::string, DbRecord*>::iterator it;
  for (it=m_dbrs.begin(); it!=m_dbrs.end(); it++) delete (*it).second;
  m_dbrs.clear();

  std::map<std::string, PixDbInterface*>::iterator it1;
  for (it1=m_dbs.begin(); it1!=m_dbs.end(); it1++) delete (*it1).second;
  m_dbs.clear();

  if (m_connDb != NULL) delete m_connDb;
  if (m_cfgModDb != NULL && m_cfgModDb != m_cfgDb) delete m_cfgModDb;
  if (m_cfgDb != NULL) delete m_cfgDb;

  m_mutex->unlock();

  clearConn();

  m_mutex->destroy();
}

void PixConnectivity::cloneConn(PixConnectivity *conn, std::string rodName) {
  clearConn();

  m_mutex->lock();

  // Look for the selected ROD
  std::map<std::string, RodBocConnectivity*>::iterator ir;
  for (ir=conn->rods.begin(); ir!=conn->rods.end(); ++ir) {
    if (ir->first == rodName) {
      m_connEmpty = false;
      m_connOwn = false;
      rods[ir->first] = ir->second;

      // Now copy parents and children
      RodBocConnectivity *rod = ir->second;
      if (rod->crate() != NULL) {
	crates[rod->crate()->name()] = rod->crate();
	RodCrateConnectivity *crate = rod->crate();
	if (crate->tim() != NULL) {
	  tims[crate->tim()->name()] = crate->tim();
	}
	if (crate->sbc() != NULL) {
	  sbcs[crate->sbc()->name()] = crate->sbc();
	}
	if (crate->partition() != NULL) {
	  parts[crate->partition()->name()] = crate->partition();
	}
        for (int io=0; io<4; io++) {
	  if (rod->obs(io) != NULL) {
	    OBConnectivity *ob = rod->obs(io);
	    obs[ob->name()] = ob;
	    if (ob->pp0() != NULL) {
	      Pp0Connectivity *pp0 = ob->pp0();
	      pp0s[pp0->name()] = pp0;
	      for (int im=1; im<=8; im++) {
		if (pp0->modules(im) != NULL) {
		  ModuleConnectivity *mod = pp0->modules(im);
		  mods[mod->name()] = mod;
		}
	      }
	    }
	  }
	}
      }
    }
  }
  
  // If the ROD was found, copy all ROS/ROBIN/ROL/OBLINKMAP/BOCLINKMAP
  if (!m_connEmpty) {
    { std::map<std::string, RosConnectivity*>::iterator i;
      for (i=conn->roses.begin(); i!=conn->roses.end(); ++i) roses[i->first] = i->second; 
    }
    { std::map<std::string, RobinConnectivity*>::iterator i;
      for (i=conn->robins.begin(); i!=conn->robins.end(); ++i) robins[i->first] = i->second; 
    }
    { std::map<std::string, RolConnectivity*>::iterator i;
    for (i=conn->rols.begin(); i!=conn->rols.end(); ++i) rols[i->first] = i->second; 
    }
    { std::map<std::string, OBLinkMap*>::iterator i;
      for (i=conn->obmaps.begin(); i!=conn->obmaps.end(); ++i) obmaps[i->first] = i->second; 
    }
    { std::map<std::string, RodBocLinkMap*>::iterator i;
      for (i=conn->bocmaps.begin(); i!=conn->bocmaps.end(); ++i) bocmaps[i->first] = i->second; 
    }
  }

  m_mutex->unlock();
}

void PixConnectivity::openCfg(std::string mode) {
  static std::string old_tagCF = "";
  static std::string old_tagModCF = "";
  static std::string old_tagCFOIcfg = "";

  if (m_types.size() == 0) {
    m_types["MODULE_CFG"] = "PixModule";
    m_types["RODBOC_CFG"] = "PixModuleGroup";
    m_types["TIM_CFG"] = "TimPixTrigController";
    m_types["DISABLE_CFG"] = "PixDisable";
    m_types["RUNCONF_CFG"] = "PixRunConfig";
    m_types["SCAN_CFG"] = "PixScan";
  }

  // Force update mode, unless we are in sqlite mode 
  if (m_cfgName.substr(0,6) != "sqlite") {
    mode = "UPDATE";
    if (old_tagCFOIcfg == m_tagCFOIcfg && old_tagCF == m_tagCF && old_tagModCF == m_tagModCF && m_cfgDb != NULL && m_cfgModDb != NULL) return;
  }

  if (m_cfgModDb != NULL && m_cfgModDb != m_cfgDb) {
    delete m_cfgModDb;
    m_cfgModDb = NULL;
  }
  if (m_cfgDb != NULL) {
    delete m_cfgDb;
    m_cfgDb = NULL;
    m_cfgModDb = NULL;
  }
  try {
    m_cfgDb = openDb(m_cfgName, mode, PixDbCompoundTag(m_tagCFOIcfg, m_tagCF, m_tagCF, m_tagCF));
    old_tagCF = m_tagCF;
    old_tagCFOIcfg = m_tagCFOIcfg;
    if (mode == "READ") {
      m_cfgUpdate = false;
    } else {
      m_cfgUpdate = true;
    }
  }
  catch (...) {
    m_cfgDb = NULL;
    try {
      m_mutex->unlock();
    }
    catch (...) {
      std::cout << "Unable to unlock mutex" << std::endl;
    }
    if (mode == "READ") {
      throw(PixConnectivityExc(PixConnectivityExc::FATAL, "CANTOPENCFG", "Cannot open configuration db"));
    } else {
      throw(PixConnectivityExc(PixConnectivityExc::FATAL, "CANTOPENCFGW", "Cannot open configuration db in update mode"));
    }
  }
  if (m_tagModCF == m_tagCF) {
    m_cfgModDb = m_cfgDb;
    old_tagModCF = m_tagModCF;
  } else {
    try {
      m_cfgModDb = openDb(m_cfgName, mode, PixDbCompoundTag(m_tagCFOIcfg, m_tagModCF, m_tagModCF, m_tagModCF));
      old_tagModCF = m_tagModCF;
      if (mode == "READ") {
	m_cfgModUpdate = false;
      } else {
	m_cfgModUpdate = true;
      }
    }
    catch (...) {
      m_cfgModDb = NULL;
      try {
	m_mutex->unlock();
      }
      catch (...) {
	std::cout << "Unable to unlock mutex" << std::endl;
      }
      if (mode == "READ") {
	throw(PixConnectivityExc(PixConnectivityExc::FATAL, "CANTOPENMCFG", "Cannot open module configuration db"));
      } else {
	throw(PixConnectivityExc(PixConnectivityExc::FATAL, "CANTOPENMCFGW", "Cannot open module configuration db in update mode"));
      }
    }
  }
}

//! Tags handling
std::vector<std::string> PixConnectivity::listConnTagsC() {
  static std::vector<std::string> tags;
  static std::vector<PixLib::PixDbTagStatus> tagstatus;
  tags.clear();

  if (m_dbServerMode) {
    m_dbServer->listTagsRem(m_domainName, tags);
  } else {
    if (m_connDb != NULL) {
      m_mutex->lock();
      m_connDb->getExistingConnectivityTags(tagstatus);
      m_mutex->unlock();
      for(std::vector<PixLib::PixDbTagStatus>::iterator i = tagstatus.begin(); i != tagstatus.end(); i++){
	tags.push_back(i->tag());
      }
    }
  }
  return tags;
}

std::vector<std::string> PixConnectivity::listConnTagsP() {
  static std::vector<std::string> tags;
  static std::vector<PixLib::PixDbTagStatus> tagstatus;
  tags.clear();

  if (m_dbServerMode) {
    m_dbServer->listTagsRem(m_domainName, tags);
  } else {
    if (m_connDb != NULL) {
      m_mutex->lock();
      m_connDb->getExistingDataTags(tagstatus);
      m_mutex->unlock();
      for(std::vector<PixLib::PixDbTagStatus>::iterator i = tagstatus.begin(); i != tagstatus.end(); i++){
	tags.push_back(i->tag());
      }
    }
  }
  return tags;
}

std::vector<std::string> PixConnectivity::listConnTagsA() {
  static std::vector<std::string> tags;
  static std::vector<PixLib::PixDbTagStatus> tagstatus;
  tags.clear();

  if (m_dbServerMode) { 
    m_dbServer->listTagsRem(m_domainName, tags);
  } else {
    if (m_connDb != NULL) {
      m_mutex->lock();
      m_connDb->getExistingAliasTags(tagstatus);
      m_mutex->unlock();
      for(std::vector<PixLib::PixDbTagStatus>::iterator i = tagstatus.begin(); i != tagstatus.end(); i++){
	tags.push_back(i->tag());
      }
    }
  }
  return tags;
}

std::vector<std::string> PixConnectivity::listConnTagsOI() {
  static std::vector<std::string> tags;
  static std::vector<PixLib::PixDbTagStatus> tagstatus;
  tags.clear();

  if (m_dbServerMode) {
    std::vector<std::string> domlist;
    m_dbServer->listDomainRem(domlist);
    std::vector<std::string>::iterator domit;
    for(domit=domlist.begin();domit!=domlist.end();domit++) {
      if ((*domit).substr(0,13) == "Connectivity-") {
	tags.push_back((*domit).substr(13));
      }
    }
  } else {
    if (m_connDb != NULL) {
      m_mutex->lock();
      m_connDb->getExistingObjectDictionaryTags(tagstatus);
      m_mutex->unlock();
      for(std::vector<PixLib::PixDbTagStatus>::iterator i = tagstatus.begin(); i != tagstatus.end(); i++){
	tags.push_back(i->tag());
      }
    }
  }
  return tags;
}


void PixConnectivity::setConnTag(std::string tagC, std::string tagP, std::string tagA, string tagOI  ) {
  if (m_connEmpty) {
    m_tagC = tagC;
    m_tagP= tagP;
    m_tagA = tagA;
    m_tagOI = tagOI;
    m_domainName = "Connectivity-"+m_tagOI;
    try {
      m_mutex->lock();
      if (m_connDb != NULL) delete m_connDb;
      if (!m_dbServerMode) {
	m_connDb = openDb(m_connName, "READ", PixDbCompoundTag(m_tagOI, m_tagC, m_tagP, m_tagA));
      }
      m_mutex->unlock();
    }
    catch (...) {
      m_mutex->unlock();
      m_connDb = NULL;
      try { throw; } 
      catch(std::exception& e) {
	throw(PixConnectivityExc(PixConnectivityExc::FATAL, "CANTOPENCONN", string("Cannot open connectivity db - ")+e.what()));
      }
      catch(PixDBException& e) {
	std::ostringstream os;
	os<<"Cannot open connectivity db: ";
	e.what(os);
	throw(PixConnectivityExc(PixConnectivityExc::FATAL, "CANTOPENCONN", os.str()));
      }
    }
  } else {
    throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTSETTAG", "Cannot change tag when a conn DB is loaded"));
  }
}

std::string PixConnectivity::getConnTagC() {
  return m_tagC;
}
  
std::string PixConnectivity::getConnTagP() {
  return m_tagP;
}
  
std::string PixConnectivity::getConnTagA() {
  return m_tagA;
}

std::string PixConnectivity::getConnTagOI() {
  return m_tagOI;
}

void PixConnectivity::createConnTag(std::string tagC, std::string tagP, std::string tagA, std::string tagOI  ) {
  if (m_dbServerMode) {
    m_tagOI = tagOI;
    m_tagC = tagC;
    m_tagP = tagP;
    m_tagA = tagA;
    m_domainName = "Connectivity-"+m_tagOI;
    dumpInDb(m_dbServer);
  } else {
    if (m_connDb != NULL) {
      try {
	m_mutex->lock();
	delete m_connDb;
	m_connDb = openDb(m_connName, "UPDATE", PixDbCompoundTag(m_tagOI, m_tagC, m_tagP, m_tagA));
	m_connDb->transactionStart();
	m_connDb->copyConnectivityTagAndSwitch(tagC);
	m_connDb->transactionCommit();
	m_tagC = tagC;
	m_connDb->transactionStart();
	m_connDb->copyDataTagAndSwitch(tagP);
	m_connDb->transactionCommit();
	m_tagP = tagP;
	m_connDb->transactionStart();
	m_connDb->copyAliasTagAndSwitch(tagA);
	m_connDb->transactionCommit();
	m_tagA = tagA;
	delete m_connDb;
	m_connDb = NULL;
	m_mutex->unlock();
      }
      catch (...) {
	m_mutex->unlock();
	throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTCREATETAGS", "Cannot create connectivity DB tags"));
      }
      try {
	m_mutex->lock();
	if (m_connDb != NULL) delete m_connDb;
	if (!m_dbServerMode) {
	  m_connDb = openDb(m_connName, "READ", PixDbCompoundTag(m_tagOI, m_tagC, m_tagP, m_tagA));
	}
	m_mutex->unlock();
      }
      catch (...) {
	m_mutex->unlock();
	m_connDb = NULL;
	try { throw; } 
	catch(std::exception& e) {
	  throw(PixConnectivityExc(PixConnectivityExc::FATAL, "CANTOPENCONN", string("Cannot open connectivity db")+e.what()));
	}
	catch(PixDBException& e) {
	  std::ostringstream os;
	  os<<"Cannot open connectivity db: ";
	  e.what(os);
	  throw(PixConnectivityExc(PixConnectivityExc::FATAL, "CANTOPENCONN", os.str()));
	}
      }
    } else {
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CONNNOTOPEN", "Connectivity DB is not open"));
    }
  }
}

void PixConnectivity::cfgFromDb() { 
  m_dbServerModeCfg = false; 
  if (m_cfgDb == NULL || m_cfgModDb == NULL) {
    openCfg("READ");
  }
}

std::vector<std::string> PixConnectivity::listCfgTags() {
  static std::vector<std::string> tags;
  static std::vector<PixLib::PixDbTagStatus> tagstatus;
  tags.clear();
  if (m_dbServerMode) {
    m_dbServer->listTagsRem(m_domainNameCF, tags);
  } else {
    if (m_cfgDb != NULL) {
      m_mutex->lock();
      m_cfgDb->getExistingConnectivityTags(tagstatus);
      m_mutex->unlock();
      for(std::vector<PixLib::PixDbTagStatus>::iterator i = tagstatus.begin(); i != tagstatus.end(); i++){
	tags.push_back(i->tag());
      }
    }
  }
  return tags;
}

void PixConnectivity::setCfgRev(unsigned int rev) {
  m_revCF = rev;
}

void PixConnectivity::setModCfgRev(unsigned int rev) {
  m_revModCF = rev;
}

void PixConnectivity::setTagPen(std::string tag) {
  m_tagPen = tag;
}

void PixConnectivity::setCfgTag(std::string tag, std::string oi, std::string modtag) {
  m_mutex->lock();
  if (modtag == "") modtag = tag;
  m_tagCF = tag;
  m_tagModCF = modtag;
  if (oi != "") {
    m_tagCFOI = oi;
    m_tagCFOIcfg = oi+"-CFG";
    m_domainNameCF = "Configuration-"+m_tagCFOI;
  }
  if (!m_dbServerModeCfg) {
    openCfg("READ");
  }
  m_mutex->unlock();
}

unsigned int PixConnectivity::getCfgRev() {
  return m_revCF;
}

//unsigned int PixConnectivity::getModCfgRev() {
//  return m_revModCF;
//}

//std::string PixConnectivity::getTagPen() {
//  return m_tagPen;
//}

std::string PixConnectivity::getCfgTag() {
  return m_tagCF;
}

std::string PixConnectivity::getModCfgTag() {
  return m_tagModCF;
}

void PixConnectivity::createCfgTag(std::string tag) {
  if (m_dbServerModeCfg) {
  } else {
    try {
      m_mutex->lock();
      openCfg("UPDATE");
      m_cfgDb->transactionStart();
      m_cfgDb->copyConnectivityTagAndSwitch(tag);
      m_cfgDb->transactionCommit();
      m_cfgDb->transactionStart();
      m_cfgDb->copyDataTagAndSwitch(tag);
      m_cfgDb->transactionCommit();
      m_cfgDb->transactionStart();
      m_cfgDb->copyAliasTagAndSwitch(tag);
      m_cfgDb->transactionCommit();
      if (m_tagCF == m_tagModCF) m_tagModCF = tag;
      m_tagCF = tag;  
      openCfg("READ");
      m_mutex->unlock();
    }
    catch (...) {
      openCfg("READ");
      m_mutex->unlock();
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTCREATETAGS", "Cannot create configuration DB tags"));
    }
  }
}

void PixConnectivity::createModCfgTag(std::string tag) {
  if (m_dbServerModeCfg) {
  } else {
    try {
      m_mutex->lock();
      openCfg("UPDATE");
      m_cfgModDb->transactionStart();
      m_cfgModDb->copyConnectivityTagAndSwitch(tag);
      m_cfgModDb->transactionCommit();
      m_cfgModDb->transactionStart();
      m_cfgModDb->copyDataTagAndSwitch(tag);
      m_cfgModDb->transactionCommit();
      m_cfgModDb->transactionStart();
      m_cfgModDb->copyAliasTagAndSwitch(tag);
      m_cfgModDb->transactionCommit();
      m_tagModCF = tag;  
      openCfg("READ");
      m_mutex->unlock();
    }
    catch (...) {
      openCfg("READ");
      m_mutex->unlock();
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTCREATETAGS", "Cannot create configuration DB tags"));
    }
  }
}

//! Connectivity handling
void PixConnectivity::clearConn() {
  m_mutex->lock();

  { std::map<std::string, PartitionConnectivity*>::iterator i;
    if (m_connOwn) { for (i=parts.begin(); i!=parts.end(); ++i) delete (*i).second; }
    parts.clear(); 
  }
  { std::map<std::string, RodCrateConnectivity*>::iterator i;
    if (m_connOwn) { for (i=crates.begin(); i!=crates.end(); ++i) delete (*i).second; }
    crates.clear(); 
  }
  { std::map<std::string, RodBocConnectivity*>::iterator i;
    if (m_connOwn) { for (i=rods.begin(); i!=rods.end(); ++i) delete (*i).second; }
    rods.clear(); 
  }
  { std::map<std::string, TimConnectivity*>::iterator i;
    if (m_connOwn) { for (i=tims.begin(); i!=tims.end(); ++i) delete (*i).second; }
    tims.clear(); 
  }
  { std::map<std::string, SbcConnectivity*>::iterator i;
    if (m_connOwn) { for (i=sbcs.begin(); i!=sbcs.end(); ++i) delete (*i).second; }
    sbcs.clear(); 
  }
  { std::map<std::string, Pp0Connectivity*>::iterator i;
    if (m_connOwn) { for (i=pp0s.begin(); i!=pp0s.end(); ++i) delete (*i).second; }
    pp0s.clear(); 
  }
  { std::map<std::string, OBConnectivity*>::iterator i;
    if (m_connOwn) { for (i=obs.begin(); i!=obs.end(); ++i) delete (*i).second; }
    obs.clear(); 
  }
  { std::map<std::string, ModuleConnectivity*>::iterator i;
    if (m_connOwn) { for (i=mods.begin(); i!=mods.end(); ++i) delete (*i).second; }
    mods.clear(); 
  }
  { std::map<std::string, RosConnectivity*>::iterator i;
    if (m_connOwn) { for (i=roses.begin(); i!=roses.end(); ++i) delete (*i).second; }
    roses.clear(); 
  }
  { std::map<std::string, RobinConnectivity*>::iterator i;
    if (m_connOwn) { for (i=robins.begin(); i!=robins.end(); ++i) delete (*i).second; }
    robins.clear(); 
  }
  { std::map<std::string, RolConnectivity*>::iterator i;
    if (m_connOwn) { for (i=rols.begin(); i!=rols.end(); ++i) delete (*i).second; }
    rols.clear(); 
  }
  { std::map<std::string, OBLinkMap*>::iterator i;
    if (m_connOwn) { for (i=obmaps.begin(); i!=obmaps.end(); ++i) delete (*i).second; }
    obmaps.clear(); 
  }
  { std::map<std::string, RodBocLinkMap*>::iterator i;
    if (m_connOwn) { for (i=bocmaps.begin(); i!=bocmaps.end(); ++i) delete (*i).second; }
    bocmaps.clear(); 
  }
  m_connEmpty = true;
  m_connOwn = true;
  m_mutex->unlock();
}

void PixConnectivity::loadConn() {
  if (m_dbServerMode) {
    loadConnDbServer();
  } else {
    loadConnDb();
  }
}

void PixConnectivity::loadConnDb() {
  //if (m_connDb == NULL) {
    //throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CONNNOTOPEN", "Connectivity DB is not open"));    
  //}

  clearConn();
  m_mutex->lock();

  if (m_connDb != NULL) delete m_connDb;
  try {
    m_connDb = openDb(m_connName, "UPDATE", PixDbCompoundTag(m_tagOI, m_tagC, m_tagP, m_tagA));
  }
  catch (...) {
    m_mutex->unlock();
    m_connDb = NULL;
    throw(PixConnectivityExc(PixConnectivityExc::FATAL, "CANTOPENCONN", "Cannot open configuration db"));
  }
  DbRecord* rootRec = m_connDb->readRootRecord();   
  for (dbRecordIterator it = rootRec->recordBegin(); it != rootRec->recordEnd(); ++it) {
    // Look for ROS container
    if ((*it)->getClassName() == "CONTAINER" && (*it)->getName() == "ROSES") {
      for(dbRecordIterator it1 = (*it)->recordBegin(); it1 != (*it)->recordEnd(); it1++) {
	if ((*it1)->getClassName() == "ROS") {
	  add(*it1,-1);
	}
      }
    }
  }
  for (dbRecordIterator it = rootRec->recordBegin(); it != rootRec->recordEnd(); ++it) {
    // Look for OBLinkMap container
    if ((*it)->getClassName() == "CONTAINER" && (*it)->getName() == "OBLINKMAPS") {
      for(dbRecordIterator it1 = (*it)->recordBegin(); it1 != (*it)->recordEnd(); it1++) {
	if ((*it1)->getClassName() == "OBLINKMAP") {
	  add(*it1,0);
	}
      }
    }
    // Look for RodBocContainer container
    if ((*it)->getClassName() == "CONTAINER" && (*it)->getName() == "RODBOCLINKMAPS") {
      for(dbRecordIterator it1 = (*it)->recordBegin(); it1 != (*it)->recordEnd(); it1++) {
	if ((*it1)->getClassName() == "RODBOCLINKMAP") {
	  add(*it1,0);
	}
      }
    }
    // Look for Partition container
    if ((*it)->getClassName() == "CONTAINER" && (*it)->getName() == "PARTITIONS") {
      for(dbRecordIterator it1 = (*it)->recordBegin(); it1 != (*it)->recordEnd(); it1++) {
	if ((*it1)->getClassName() == "PARTITION") {
	  add(*it1,-1);
	}
      }
    }
  }
  m_connEmpty = false;

  m_mutex->unlock();
}

void PixConnectivity::saveConn() {
  if (m_dbServerMode) {
    dumpInDb(m_dbServer);
  } else {
    saveConnDb();
  }
}

void PixConnectivity::saveConnDb() {
  if (m_connDb == NULL) {
    throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CONNNOTOPEN", "Connectivity DB is not open"));    
  }

  m_mutex->lock();
  
  delete m_connDb;
  m_connDb = openDb(m_connName, "UPDATE", PixDbCompoundTag(m_tagOI, m_tagC, m_tagP, m_tagA));  
  m_connDb->transactionStart();
  DbRecord* rootRec = m_connDb->readRootRecord();   

  DbRecord *parR = NULL;
  DbRecord *pp0R = NULL;
  DbRecord *rosR = NULL;
  DbRecord *bocMapsR = NULL;
  DbRecord *obMapsR = NULL;
  for (dbRecordIterator it = rootRec->recordBegin(); it != rootRec->recordEnd(); ++it) {
    if ((*it)->getClassName() == "CONTAINER" && (*it)->getName() == "PARTITIONS") parR = (*it);
    if ((*it)->getClassName() == "CONTAINER" && (*it)->getName() == "PP0S") pp0R = (*it);
    if ((*it)->getClassName() == "CONTAINER" && (*it)->getName() == "ROSES") rosR = (*it);
    if ((*it)->getClassName() == "CONTAINER" && (*it)->getName() == "RODBOCLINKMAPS") bocMapsR = (*it); 
    if ((*it)->getClassName() == "CONTAINER" && (*it)->getName() == "OBLINKMAPS") obMapsR = (*it);
  }
  if (parR == NULL) parR = rootRec->addRecord("CONTAINER","PARTITIONS");
  if (pp0R == NULL) pp0R = rootRec->addRecord("CONTAINER","PP0S");
  if (rosR == NULL) rosR = rootRec->addRecord("CONTAINER","ROSES");
  if (bocMapsR == NULL) bocMapsR = rootRec->addRecord("CONTAINER","RODBOCLINKMAPS");
  if (obMapsR == NULL) obMapsR = rootRec->addRecord("CONTAINER","OBLINKMAPS");
  
  // Write the OBLinkMap tree to the DB
  std::map<std::string, OBLinkMap*>::iterator itoblm;
  for (itoblm=obmaps.begin(); itoblm!=obmaps.end(); ++itoblm) {
    DbRecord *r;
    dbRecordIterator ir;
    if ((ir = obMapsR->findRecord(((*itoblm).second)->name())) == obMapsR->recordEnd()) {
      r = obMapsR->addRecord("OBLINKMAP", ((*itoblm).second)->name());
    } else {
      r = *ir;
    }
    ((*itoblm).second)->writeConfig(r);
  }
  // Write the RodBocLinkMap tree to the DB
  std::map<std::string, RodBocLinkMap*>::iterator itrblm;
  for (itrblm=bocmaps.begin(); itrblm!=bocmaps.end(); ++itrblm) {
    DbRecord *r;
    dbRecordIterator ir;
    if ((ir = bocMapsR->findRecord(((*itrblm).second)->name())) == bocMapsR->recordEnd()) {
      r = bocMapsR->addRecord("RODBOCLINKMAP", ((*itrblm).second)->name());
    } else {
      r = *ir;
    }
    ((*itrblm).second)->writeConfig(r);
  }
  // Write the ROD tree to the DB
  std::map<std::string, RosConnectivity*>::iterator itros;
  for (itros=roses.begin(); itros!=roses.end(); ++itros) {
    DbRecord *r;
    dbRecordIterator ir;
    if ((ir = rosR->findRecord(((*itros).second)->name())) == rosR->recordEnd()) {
      r = rosR->addRecord("ROS", ((*itros).second)->name());
    } else {
      r = *ir;
    }
    ((*itros).second)->writeConfig(r,-1);
  }
  // Write the Partition tree to the DB
  std::map<std::string, PartitionConnectivity*>::iterator it2;
  for (it2=parts.begin(); it2!=parts.end(); ++it2) {
    DbRecord *r;
    dbRecordIterator ir;
    if ((ir = parR->findRecord(((*it2).second)->name())) == parR->recordEnd()) {
      r = parR->addRecord("PARTITION", ((*it2).second)->name());
    } else {
      r = *ir;
    }
    ((*it2).second)->writeConfig(r,-1);
  }
  // Write the PP0 tree to the DB
  std::map<std::string, Pp0Connectivity*>::iterator it1;
  for (it1=pp0s.begin(); it1!=pp0s.end(); ++it1) {
    // Update the PP0 -> BOC link
    Pp0Connectivity *p = ((*it1).second);
    if (p->ob() != NULL) {
      p->obName() = (p->ob())->decName();
      // Write
      DbRecord *r;
      dbRecordIterator ir;
      if ((ir = pp0R->findRecord(((*it1).second)->name())) == pp0R->recordEnd()) {
	r = pp0R->addRecord("PP0", ((*it1).second)->name());
      } else {
	r = *ir;
      }
      ((*it1).second)->writeConfig(r,-1);
    }
  }
  
  m_connDb->transactionCommit();
  delete m_connDb;
  m_connDb = openDb(m_connName, "READ", PixDbCompoundTag(m_tagOI, m_tagC, m_tagP, m_tagA));  

  m_connEmpty = false;

  m_mutex->unlock();
}

void PixConnectivity::connTransStart() {
  if (m_connDb != NULL) {
    m_connDb->transactionStart();
  }
}

void PixConnectivity::connTransCommit() {
  if (m_connDb != NULL) {
    m_connDb->transactionCommit();
  }
}

//! Configuration handling

//! New back-end independent type-less interface

void PixConnectivity::closeCfg(std::string lobj) { //OK
  if (!m_dbServerModeCfg) {
    std::string fName;
    unsigned int crev, crep;
    fName = getCfgFileName(lobj, crev, crep);
    if (fName != "") {
      m_mutex->lock();
      if (m_dbrs.find(fName) != m_dbrs.end()) {
	delete m_dbrs[fName];
        m_dbrs.erase(m_dbrs.find(fName));
      }
      if (m_dbs.find(fName) != m_dbs.end()) {
	delete m_dbs[fName];
        m_dbs.erase(m_dbs.find(fName));
      }
      m_mutex->unlock();
    }
  }
}
#if 0
bool PixConnectivity::checkCfg(Config *cfg, std::string lobj, unsigned int &rev) { //OK
  // Look for the best revision
  std::string tag = m_tagCF;
  bool module = isModule(lobj);
  if (module) {
    tag = m_tagModCF;
  }

  std::map<unsigned int, std::string> lRev = listCfgRev(lobj);
  std::map<unsigned int, std::string>::iterator ir;
  unsigned int bestRev = 0;
  for (ir=lRev.begin(); ir!=lRev.end(); ir++) {
    if (ir->first < m_revCF && ir->first > bestRev) {
      bestRev = ir->first;
    } 
  }
  if (cfg->tag() != tag || cfg->rev() != bestRev) {
    rev = bestRev;
    return false;
  } 
  return true;
}

bool PixConnectivity::writeCfg(Config *cfg, std::string lobj) {
  return false;
}

bool PixConnectivity::readCfg(Config *cfg, std::string lobj) { //OK
  PixDbInterface* &db = m_cfgDb;
  std::string tag = m_tagCF;
  unsigned int rev = m_revCF;
  bool module = isModule(lobj);
  if (module) {
    db = m_cfgModDb;
    tag = m_tagModCF;
    rev = m_revModCF;
  }

  if (m_dbServerModeCfg) {
    std::vector<unsigned int> lRev;
    m_dbServer->listRevisionsRem(m_domainNameCF, tag, lobj, lRev);
    std::vector<unsigned int>::iterator ir;
    unsigned int bestRev = 0;
    for (ir=lRev.begin(); ir!=lRev.end(); ir++) {
      if (*ir < rev && *ir > bestRev) {
	bestRev = *ir;
      } 
    }
    if (cfg->tag() != tag || cfg->rev() != bestRev) {
      bool res;
      try { 
	res = cfg->read(m_dbServer, m_domainNameCF, tag, lobj, rev); 
      }
      catch (...) {
	throw(PixConnectivityExc(PixConnectivityExc::ERROR, "RECNOTFOUD", "Config record "+lobj+" not found"));
      } 
      if (!res) throw(PixConnectivityExc(PixConnectivityExc::ERROR, "RECNOTFOUD", "Config record "+lobj+" not found"));
    } 
    return true;
  } else {
    // Look for the best revision
    std::map<unsigned int, std::string> lRev = listCfgRev(lobj);
    std::map<unsigned int, std::string>::iterator ir;
    unsigned int bestRev = 0;
    for (ir=lRev.begin(); ir!=lRev.end(); ir++) {
      if (ir->first < rev && ir->first > bestRev) {
	bestRev = ir->first;
      } 
    }
    if (cfg->tag() != tag || cfg->rev() != bestRev) {
      bool newBE = true;
      unsigned int crev1, crep1;
      std::string fName = getCfgFileName(lobj, crev1, crep1);
      if (fName != "") {
	if (module && element(lobj,1) == "") {
	  // Try first with the new ROOT BE
	  try {
	    std::unique_ptr<RootConf::IConfigStore> store(RootConf::ConfigStoreFactory::configStore());
	    store->read(*cfg, fName,"Def",0);
	    return true;  
	  } catch(...) {
	    newBE=false;
	  }
	} else {
	  newBE = false;
	}
	if (!module || !newBE) {
	  unsigned int crev2, crep2;
	  std::string cnam;
          DbRecord *r = getCfgFileRecord(lobj, crev2, crep2);
	  if (r == NULL) {
	    throw(PixConnectivityExc(PixConnectivityExc::ERROR, "RECNOTFOUD", "Config record "+lobj+" not found"));
	  }	
	  m_mutex->lock();
          DbRecord *rs = getSubRec(r, lobj, 0);
	  m_mutex->unlock();
	  if (rs != NULL) {
	    cfg->read(rs);
	    cfg->tag() = tag;
	    cfg->rev() = crev2;
	    return true;
	  } else {
	    throw(PixConnectivityExc(PixConnectivityExc::ERROR, "RECNOTFOUD", "Config record "+lobj+" not found"));
	  }
	} else {
	  throw(PixConnectivityExc(PixConnectivityExc::ERROR, "RECNOTFOUD", "Config record "+lobj+" not found"));
	}
      } else {
	throw(PixConnectivityExc(PixConnectivityExc::ERROR, "RECNOTFOUD", "Config record "+lobj+" not found"));
      }
    }
    return true;
  }
}
#endif
std::map<std::string, std::string> PixConnectivity::listSubCfg(std::string lobj) { //OK
  PixDbInterface* &db = m_cfgDb;
  std::string tag = m_tagCF;
  unsigned int rev = m_revCF;
  bool module = isModule(lobj);
  if (module) {
    db = m_cfgModDb;
    tag = m_tagModCF;
    rev = m_revModCF;
  }
  std::map<std::string, std::string> subRec;
  if (m_dbServerModeCfg) {
    PixDbDomain::rev_content cont;
    bool result = m_dbServer->getObject(cont, m_domainNameCF, tag, lobj, rev);  
    if (!result) {
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "RECNOTFOUD", "Config record "+lobj+" not found"));
    } else {
      unsigned int i;
      for (i=0; i<cont.upLink_content.size(); i++) {
	subRec[cont.upLink_content[i].linkName] = cont.upLink_content[i].objType;
      }
      for (i=0; i<cont.downLink_content.size(); i++) {
	subRec[cont.downLink_content[i].linkName] = cont.downLink_content[i].objType;
      }
    }
  } else {
    unsigned int crev, crep;
    std::string cnam;
    DbRecord *r = getCfgFileRecord(lobj, crev, crep);
    if (r == NULL) {
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "RECNOTFOUD", "Config record "+lobj+" not found"));
    }	
    m_mutex->lock();
    for (dbRecordIterator it = r->recordBegin(); it != r->recordEnd(); ++it) {
      subRec[(*it)->getName()] = (*it)->getClassName();
    }
    m_mutex->unlock();
  }
  return subRec;
}

std::map<std::string, std::string> PixConnectivity::listCfgObj(std::string type) { //OK
  static std::map<std::string, std::string> objs;
  objs.clear();
  if (m_dbServerModeCfg) {
    std::vector<std::string> names;
    std::vector<std::string> types;
    if (type == "MODULE") {
      m_dbServer->listObjectsRem(m_domainNameCF, m_tagModCF, type, names, types);
    } else {
      m_dbServer->listObjectsRem(m_domainNameCF, m_tagCF, type, names, types);
    }
    for (unsigned int i=0; i<names.size(); i++) {
      objs[names[i]] = types[i];
    }
  } else {
    m_mutex->lock();
    DbRecord *rootRec;
    if (type == "MODULE") {
      rootRec = m_cfgModDb->readRootRecord(); 
    } else {
      rootRec = m_cfgDb->readRootRecord(); 
    }
    for (dbRecordIterator it = rootRec->recordBegin(); it != rootRec->recordEnd(); ++it) {
      std::string rtyp = (*it)->getClassName();
      if (rtyp == type || type  == "") {
        if (m_types.find(rtyp) != m_types.end()) {
	  objs[(*it)->getName()] = m_types[rtyp];
	} else {
	  objs[(*it)->getName()] = "Unknown";
	}
      }
    }
    m_mutex->unlock();
  }
  return objs;
}

std::map<unsigned int, std::string> PixConnectivity::listCfgRev(std::string lobj) { //OK
  PixDbInterface* &db = m_cfgDb;
  std::string tag = m_tagCF;
 // unsigned int rev = m_revCF;
  bool module = isModule(lobj);
  if (module) {
    db = m_cfgModDb;
    tag = m_tagModCF;
  //  rev = m_revModCF;
  }
  static std::map<unsigned int, std::string> revs;
  static std::map<unsigned int, unsigned int> revrep;
  revs.clear();
  revrep.clear();
  if (m_dbServerModeCfg) {
    std::vector<unsigned int> lRev;
    m_dbServer->listRevisionsRem(m_domainNameCF, tag, lobj, lRev);
    std::vector<unsigned int>::iterator ir;
    for (ir=lRev.begin(); ir!=lRev.end(); ir++) {
      revs[*ir] = lobj;
    }
  } else {
    m_mutex->lock();
    DbRecord *rootRec = db->readRootRecord();
    std::string obj = element(lobj, 0); 
    for (dbRecordIterator it = rootRec->recordBegin(); it != rootRec->recordEnd(); ++it) {
      if ((*it)->getName() == obj) {
	for(dbRecordIterator it1 = (*it)->recordBegin(); it1 != (*it)->recordEnd(); it1++) {
	  std::string tim = (*it1)->getClassName();
	  std::istringstream is(tim.substr(0,tim.size()-3));
	  unsigned int i;
	  is >> i;
	  std::istringstream isr(tim.substr(tim.size()-3,3));
	  unsigned int j;
	  isr >> j;
	  bool upd = true;
          if (revs.find(i) != revs.end()) {
	    if (revrep[i] > j) upd = false;
	  }
	  if (upd) {
	    revs[i] = (*it1)->getName();
	    revrep[i] = j;
	  }
	}
      }
    }
    m_mutex->unlock();
  }
  return revs;
}

std::map<std::string, std::string> PixConnectivity::listCfgRevRep(std::string lobj) { //OK
  PixDbInterface* &db = m_cfgDb;
  std::string tag = m_tagCF;
 // unsigned int rev = m_revCF;
  bool module = isModule(lobj);
  if (module) {
    db = m_cfgModDb;
    tag = m_tagModCF;
   // rev = m_revModCF;
  }
  static std::map<std::string, std::string> revs;
  revs.clear();
  if (m_dbServerModeCfg) {
    std::vector<unsigned int> lRev;
    m_dbServer->listRevisionsRem(m_domainNameCF, tag, lobj, lRev);
    std::vector<unsigned int>::iterator ir;
    for (ir=lRev.begin(); ir!=lRev.end(); ir++) {
      ostringstream os;
      os << *ir;
      revs[os.str()] = lobj;
    }
  } else {
    m_mutex->lock();
    std::string obj = element(lobj, 0);
    DbRecord *rootRec = db->readRootRecord(); 
    for (dbRecordIterator it = rootRec->recordBegin(); it != rootRec->recordEnd(); ++it) {
      if ((*it)->getName() == obj) {
	for(dbRecordIterator it1 = (*it)->recordBegin(); it1 != (*it)->recordEnd(); it1++) {
	  std::string tim = (*it1)->getClassName();
	  revs[tim] = (*it1)->getName();
	}
      }
    }
    m_mutex->unlock();
  }
  return revs;
}

std::string PixConnectivity::getCfgFileName(std::string lobj, unsigned int &cfgRev, unsigned int &cfgRep) { //OK
  PixDbInterface* &db = m_cfgDb;
  std::string tag = m_tagCF;
  unsigned int rev = m_revCF;
  DbRecord *rootRec = m_cfgDb->readRootRecord();
  bool module = isModule(lobj);
  if (module) {
    db = m_cfgModDb;
    tag = m_tagModCF;
    rev = m_revModCF;
    rootRec = m_cfgModDb->readRootRecord(); 
  }
  std::string xobj = element(lobj, 0);

  m_mutex->lock();
  std::string fName;
  bool found = false;
  unsigned int savtim = 0;
  int savrep = -1;
  for (dbRecordIterator it = rootRec->recordBegin(); it != rootRec->recordEnd(); ++it) {
    if ((*it)->getName() == xobj) {
      for(dbRecordIterator it1 = (*it)->recordBegin(); it1 != (*it)->recordEnd(); it1++) {
	std::string ss = (*it1)->getClassName();
        std::string ss1 = ss.substr(0, ss.size()-3);
	std::istringstream is1(ss1);
	unsigned int i1;
	is1 >> i1;
        std::string ss2 = ss.substr(ss.size()-3, 3);
	std::istringstream is2(ss2);
	int i2;
	is2 >> i2;
        if (i1<=rev && (i1>savtim || (i1==savtim && i2>savrep))) {
	  savtim = i1;
          savrep = i2;
          fName = (*it1)->getName();
	  found = true;
	}
      }
    }
  }
  m_mutex->unlock();
  if (found) {
    cfgRev = savtim;
    cfgRep = savrep;
  } else {
    cfgRev = 0;
    cfgRep = 0;
  }
  return fName;
}

DbRecord* PixConnectivity::getCfgFileRecord(std::string lobj, unsigned int &cfgRev, unsigned int &cfgRep) { //OK
  PixDbInterface* &db = m_cfgDb;
  std::string tag = m_tagCF;
  //unsigned int rev = m_revCF;
  bool module = isModule(lobj);
  if (module) {
    db = m_cfgModDb;
    tag = m_tagModCF;
    //rev = m_revModCF;
  }
  std::string xobj = element(lobj, 0);
  std::string fName = getCfgFileName(lobj, cfgRev, cfgRep);
  if (fName != "") {
    if (m_dbrs.find(fName) != m_dbrs.end()) {
      return m_dbrs[fName];
    } else {
      m_mutex->lock();
      if (m_dbs.find(fName) == m_dbs.end()) {
	std::string pref = "";
        if (fName.substr(fName.size()-4) == ".sql") {
	  pref = "sqlite_file:";
	} 
	m_dbs[fName] = openDb(pref+fName, "READ", PixDbCompoundTag("BASE", "BASE", "BASE", "BASE"));
      }
      DbRecord *rootRec = m_dbs[fName]->readRootRecord(); 	
      for (dbRecordIterator it = rootRec->recordBegin(); it != rootRec->recordEnd(); ++it) {
	if ((*it)->getName() == xobj) {
	  m_dbrs[fName] = (*it);
	}
      }
      m_mutex->unlock();
      if (m_dbrs.find(fName) != m_dbrs.end()) {
	return m_dbrs[fName];
      }
    }
  } 
  return NULL;
}

DbRecord * PixConnectivity::getSubRec(DbRecord *r, std::string lobj, int el) { //OK
  if (element(lobj, el+1) == "") {
    return r;
  } else { 
    for (dbRecordIterator it = r->recordBegin(); it != r->recordEnd(); ++it) {
      if ((*it)->getName() == element(lobj, el+1)) {
	el++;
	return getSubRec(*it, lobj, el);
      }
    }
    return NULL;
  }
}

bool PixConnectivity::isModule(std::string &lobj) { //OK
  std::string xobj = element(lobj,0);
  if (mods.find(xobj) != mods.end()) {
    xobj = mods[lobj]->prodId();
    if (lobj.find_first_of("|") != std::string::npos) {
      lobj = xobj+lobj.substr(lobj.find_first_of("|"));
    }
    return true;
  }
  return false;
}
 
//! Old-style interface (deprecated)

void PixConnectivity::getCfgObjType(std::string obj, std::string &type, std::string &name) {
  type = "";
  name = "";
  /*
  for( std::map<std::string, ModuleConnectivity*>::iterator mit=mods.begin();  mit!= mods.end(); mit++){
  if (mit->second->prodId()==obj) {
  type = "MODULE";
  name = mit->first;
  break;
  }
  }*/
  if (mods.find(obj) != mods.end()) type = "MODULE";
  else if (rods.find(obj) != rods.end()) type = "RODBOC";
  else if (tims.find(obj) != tims.end()) type = "TIM";
  if (type == "") {
    if (obj.substr(0,8) == "DISABLE:") {
      type = "DISABLE";
      name = obj.substr(8);
    } else if (obj.substr(0,8) == "RUNCONF:") {
      type = "RUNCONF";
      name = obj.substr(8);
    } else if (obj.substr(0,7) == "SCAN:") {
      type = "SCAN";
      name = obj.substr(7);
    } else {
      if (m_cfgUpdate || m_cfgModUpdate) openCfg("READ");
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "OBJNOTINCONN", "Object "+obj+" not in connectivity DB"));    
    }
  } else {
    name = obj;
  }
}

std::string PixConnectivity::getCfgName(std::string obj, std::string type) {
  return getCfgName(obj, m_revCF, type);
}

std::string PixConnectivity::getCfgName(std::string lobj, unsigned int rev, std::string type) {
  std::string cnam;
  unsigned int crev, crep;
  getCfgName(lobj, rev, cnam, crev, crep, type);
  return cnam;
}

std::string PixConnectivity::getCfgName(std::string lobj, unsigned int rev, unsigned int &cfgRev, unsigned int &cfgRep, std::string type) {
  std::string cnam;
  getCfgName(lobj, rev, cnam, cfgRev, cfgRep, type);
  return cnam;
}

void PixConnectivity::getCfgName(std::string lobj, unsigned int rev, std::string &cfgName, unsigned int &cfgRev, unsigned int &cfgRep, std::string type) {
  std::string obj;
  if (type == "") {
    getCfgObjType(lobj, type, obj);
  } else {
    obj = lobj;
  }
  m_mutex->lock();
  std::string xobj = obj;
  if (type == "MODULE") xobj = mods[obj]->prodId();
  DbRecord *rootRec;
  if (type == "MODULE") {
    rootRec = m_cfgModDb->readRootRecord(); 
  } else {
    rootRec = m_cfgDb->readRootRecord(); 
  }
  std::string fName;
  bool found = false;
  unsigned int savtim = 0;
  int savrep = -1;
  for (dbRecordIterator it = rootRec->recordBegin(); it != rootRec->recordEnd(); ++it) {
    if ((*it)->getName() == xobj) {
      for(dbRecordIterator it1 = (*it)->recordBegin(); it1 != (*it)->recordEnd(); it1++) {
	std::string ss = (*it1)->getClassName();
        std::string ss1 = ss.substr(0, ss.size()-3);
	std::istringstream is1(ss1);
	unsigned int i1;
	is1 >> i1;
        std::string ss2 = ss.substr(ss.size()-3, 3);
	std::istringstream is2(ss2);
	int i2;
	is2 >> i2;
        if (i1<=rev && (i1>savtim || (i1==savtim && i2>savrep))) {
	  savtim = i1;
          savrep = i2;
          fName = (*it1)->getName();
	  found = true;
	}
      }
    }
  }
  m_mutex->unlock();
  if (found) {
    cfgName = fName;
    cfgRev = savtim;
    cfgRep = savrep;
  } else {
    cfgName = "";
    cfgRev = 0;
    cfgRep = 0;
  }
}

DbRecord* PixConnectivity::getCfg(std::string obj, std::string type) {
  return getCfg(obj, m_revCF, type);
}

DbRecord* PixConnectivity::getCfg(std::string lobj, unsigned int rev, std::string type) {
  std::string cnam;
  unsigned int crev, crep;
  return getCfg(lobj, rev, cnam, crev, crep, type);
}

DbRecord* PixConnectivity::getCfg(std::string lobj, unsigned int rev, std::string &cfgName, unsigned int &cfgRev, unsigned int &cfgRep, std::string type) {
  std::string obj;
  if (type == "") {
    getCfgObjType(lobj, type, obj);
  } else {
    obj = lobj;
  }
  std::string xobj = obj;
  if (type == "MODULE") xobj = mods[obj]->prodId();
  std::string fName;
  unsigned int crev, crep;
  getCfgName(lobj, rev, fName, crev, crep, type);
  if (fName != "") {
    cfgRev = crev;
    cfgRep = crep;
    cfgName = fName;
    m_mutex->lock();
    if (m_dbrs.find(fName) != m_dbrs.end()) {
      m_mutex->unlock();
      return m_dbrs[fName];
    } else {      
      if (m_dbs.find(fName) == m_dbs.end()) {
	std::string pref = "";
        if (fName.substr(fName.size()-4) == ".sql") {
	  pref = "sqlite_file:";
	} 
	m_dbs[fName] = openDb(pref+fName, "READ", PixDbCompoundTag("BASE", "BASE", "BASE", "BASE"));
      }
      DbRecord *rootRec = m_dbs[fName]->readRootRecord(); 	
      for (dbRecordIterator it = rootRec->recordBegin(); it != rootRec->recordEnd(); ++it) {
	if ((*it)->getName() == xobj) {
	  m_dbrs[fName] = (*it);
	  m_mutex->unlock();
	  return (*it);
	}
      }
      m_mutex->unlock();
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "INVCONFFILE", "Config file does not contain a record called "+obj));
    }
  } else {
    throw(PixConnectivityExc(PixConnectivityExc::ERROR, "RECNOTFOUD", "Config record "+obj+" not found"));
  }
  return NULL;
}

PixModule* PixConnectivity::getMod(std::string name) {
  std::cout << "Try to open config with new back-end for module " << name << std::endl;
  bool newBE=true;
  
  std::string type="";
  if (mods.find(name)==mods.end())
    throw(PixConnectivityExc(PixConnectivityExc::ERROR, "OBJNOTINCONN", "Object "+name+" not in connectivity DB"));    
  std::string pname = mods[name]->prodId();
  std::string fName;
  unsigned int crev, crep;
  getCfgName(name, m_revCF, fName, crev, crep, type);

  // get module flavour from connectivity to hand it over to PixModule
  EnumMCCflavour::MCCflavour mccFlv = EnumMCCflavour::PM_NO_MCC;
  EnumFEflavour::FEflavour feFlv   = EnumFEflavour::PM_NO_FE;
  int nFe=0;
  mods[name]->getModPars(mccFlv, feFlv, nFe);

  if (fName != "") {
    PixModule *mod=new PixModule(m_domainNameCF, m_tagCF, pname,
				 mccFlv, feFlv, nFe);
    try {
      std::unique_ptr<RootConf::IConfigStore> store( RootConf::ConfigStoreFactory::configStore());
      store->read(mod->config(), fName,"Def",0);
      if (mod->getModuleGeomConnName()=="Dxx-Bxx-Sxx") {
	std::cout << "Module config has no geomConnName ...... reading it from connectivity" << std::endl;
	std::map<std::string, ModuleConnectivity*>::iterator  modit=mods.find(name);
	if (modit!=mods.end()) mod->setModuleGeomConnName(modit->second->name());
      }
      mod->config().rev(crev);
      mod->config().tag(m_tagCF);
      mod->config().penTag("OnDB");
      return mod;    
    } catch(...) {
      newBE=false;
      std::cout << "Could not open file: " << fName << " with new back-end!!!!" << std::endl;
    }
  } else {
    newBE=false;
    std::cout << "Could not find config file for module " << pname << std::endl;
  }

  if (!newBE) {
    std::cout << "Try with old back-end" << std::endl;
    DbRecord *r = getCfg(name);
    if (r != NULL) {
      std::string fnam = r->getDecName();
      PixModule* mod = new PixModule(r, (PixModuleGroup*)NULL, mods[name]->prodId());
      if (mod->getModuleGeomConnName()=="Dxx-Bxx-Sxx") {
	std::cout << "Module config has no geomConnName ...... reading it from connectivity" << std::endl;
	std::map<std::string, ModuleConnectivity*>::iterator  modit=mods.find(name);
	if (modit!=mods.end()) mod->setModuleGeomConnName(modit->second->name());
	else std::cout << "Module not found" << std::endl;
      }      
      mod->config().rev(crev);
      mod->config().tag(m_tagCF);
      mod->config().penTag("OnDB");
      return mod;
    } else {
      std::cout << "Could not open file with old back-end" << std::endl;
      return NULL;
    }
  } else {
    return NULL;
  }
}
/*
PixModuleGroup* PixConnectivity::getModGroup(std::string name) {
  unsigned int crev, crep;
  std::string cnam;
  DbRecord *r = getCfg(name, m_revCF, cnam, crev, crep, "");
  if (r != NULL) {
    PixModuleGroup *mg = new PixModuleGroup(this, name);
    mg->config().rev(crev);
    mg->config().tag(m_tagCF);
    mg->config().penTag("OnDB");
    return mg; 
  } else {
    return NULL;
  }
}

TimPixTrigController* PixConnectivity::getTim(std::string name) {
  unsigned int crev, crep;
  std::string cnam;
  bool fail = false;
  TimPixTrigController *tr = NULL;
  try {
    DbRecord *r = getCfg(name, m_revCF, cnam, crev, crep, "");
    if (r != NULL) {
      tr = new TimPixTrigController(r, name);
      tr->config()->rev(crev);
      tr->config()->tag(m_tagCF);
      tr->config()->penTag("OnDB");
      return tr;
    } else {
      return NULL;
    }
  }
  catch(PixConnectivityExc& e) {
    std::cout << "Exception caught in PixConnectivity " << e.getDescr() << " - getTim(" << name << ")" << std::endl;
    fail = true;
  } catch(PixDBException& e) {
    std::cout<<"Got PixDBException: ";
    e.what(std::cout);
    std::cout << " - getTim(" << name << ")" << std::endl;
    fail = true;
  } catch (std::exception& e) {
    std::cout << "Got std::exception: " << e.what() << " - getTim(" << name << ")" << std::endl;
    fail = true;
  } catch(...) {
    std::cout << "Unknow exception caught in PixConnectivity - getTim(" << name << ")" << std::endl;
    fail = true;
  }
  if (fail) {
    if (tr != NULL) delete tr;
    try {
      m_mutex->unlock();
    }
    catch (...) { }
    return NULL;
  }
  return NULL;
}
*/
PixDisable* PixConnectivity::getDisable(std::string name) {
  PixDisable *dis = new PixDisable(name);
  bool fail = false;
  try {
    readDisable(dis, name);
  }
  catch(PixConnectivityExc &e) {
    std::cout << "Exception caught in PixConnectivity " << e.getDescr() << " - getDisable(" << name << ")" << std::endl;
    delete dis;
    fail = true;
  } catch(PixDBException& e) {
    std::cout<<"Got PixDBException: ";
    e.what(std::cout);
    std::cout << " - getDisable(" << name << ")" << std::endl;
    delete dis;
    fail = true;
  } catch (std::exception& e) {
    std::cout << "Got std::exception: " << e.what() << " - getDisable(" << name << ")" << std::endl;
    delete dis;
    fail = true;
  } catch(...) {
    std::cout << "Unknow exception caught in PixConnectivity - getDisable(" << name << ")" << std::endl;
    delete dis;
    fail = true;
  }
  if (fail) {
    try {
      m_mutex->unlock();
    }
    catch (...) { }
    return NULL;
  }
  return dis;
}

PixRunConfig* PixConnectivity::getRunConfig(std::string name) {
  PixRunConfig *run = new PixRunConfig(name);
  bool fail = false;
  try {
    readRunConfig(run, name);
  }
  catch(PixConnectivityExc& e) {
    std::cout << "Exception caught in PixConnectivity " << e.getDescr() << " - getRunConfig(" << name << ")" << std::endl;
    delete run;
    fail = true;
  } catch(PixDBException& e) {
    std::cout<<"Got PixDBException: ";
    e.what(std::cout);
    std::cout << " - getRunConfig(" << name << ")" << std::endl;
    delete run;
    fail = true;
  } catch (std::exception& e) {
    std::cout << "Got std::exception: " << e.what() << " - getRunConfig(" << name << ")" << std::endl;
    delete run;
    fail = true;
  } catch(...) {
    std::cout << "Unknow exception caught in PixConnectivity - getRunConfig(" << name << ")" << std::endl;
    delete run;
    fail = true;
  }
  if (fail) {
    try {
      m_mutex->unlock();
    }
    catch (...) { }
    return NULL;
  }
  return run;
}
/*
PixScan* PixConnectivity::getScan(std::string name) {
  PixScan *scn = new PixScan();
  scn->config();
  try {
    readScan(scn, name);
  }
  catch(...) {
    delete scn;
    return NULL;
  }
  return scn;
}
*/
//void PixConnectivity::readMod(PixModule *mod, std::string name) {
//}

//void PixConnectivity::readModGroup(PixModuleGroup *modg, std::string name) {
//}

//void PixConnectivity::readTim(TimPixTrigController *tim, std::string name) {
//}

void PixConnectivity::readDisable(PixDisable *dis, std::string name) {
  if (m_dbServerModeCfg) {
    bool res;
    try { 
      res = dis->m_config->read(m_dbServer, m_domainNameCF, m_tagCF, name, m_revCF); 
    }
    catch (...) {
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "RECNOTFOUD", "Config record "+name+" not found"));
    } 
    if (!res) throw(PixConnectivityExc(PixConnectivityExc::ERROR, "RECNOTFOUD", "Config record "+name+" not found"));

  } else {
    unsigned int crev, crep;
    std::string cnam;
    DbRecord *r = getCfg(name, m_revCF, cnam, crev, crep, "DISABLE");
    if (r != NULL) {
      dis->m_config->read(r);
      dis->m_config->rev(crev);
      dis->m_config->tag(m_tagCF);
      dis->m_config->penTag("OnDB");
    } else {
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "RECNOTFOUD", "Config record "+name+" not found"));
    }
    closeCfg(name);
  }
}

void PixConnectivity::readRunConfig(PixRunConfig *runc, std::string name) {
  if (m_dbServerModeCfg) {
    PixDbDomain::rev_content cont, cont1;
    bool result = m_dbServer->getObject(cont, m_domainNameCF, m_tagCF, name, m_revCF);  
    if (!result) {
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "RECNOTFOUD", "Config record "+name+" not found"));
    } else {
      runc->m_conf->rev(cont.revision);
      runc->m_conf->tag(m_tagCF);
      runc->m_conf->penTag(cont.pendingTag);
      for (unsigned int i = 0; i < runc->m_conf->m_group.size(); i++) {
	runc->m_conf->m_group[i]->read(cont);
      }
      for (unsigned int j = 0; j < cont.downLink_content.size(); j++) {
        if (cont.downLink_content[j].objType == "PixRunConfig") {
	  std::string name1 = name+"|"+cont.downLink_content[j].linkName;
	  PixRunConfig *rc = new PixRunConfig(cont.downLink_content[j].objName);
	  result = m_dbServer->getObject(cont1, m_domainNameCF, m_tagCF, name1, m_revCF);  
	  if (!result) {
	    throw(PixConnectivityExc(PixConnectivityExc::ERROR, "RECNOTFOUD", "Config record "+name1+" not found"));
	  } else {
	    rc->m_conf->rev(cont1.revision);
	    rc->m_conf->tag(m_tagCF);
	    rc->m_conf->penTag(cont1.pendingTag);
	    for (unsigned int i = 0; i < rc->m_conf->m_group.size(); i++) {
	      rc->m_conf->m_group[i]->read(cont1);
	    }
	    runc->addSubConf(rc);
	  }
	}
      }
    }
  } else {
    runc->removeSubConf();
    unsigned int crev, crep;
    std::string cnam;
    DbRecord *r = getCfg(name, m_revCF, cnam, crev, crep, "RUNCONF");
    if (r == NULL) {
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTLOAD", "Config record "+name+" not found"));
    } else {
      runc->m_conf->read(r);
      runc->m_conf->rev(crev);
      runc->m_conf->tag(m_tagCF);
      runc->m_conf->penTag("OnDB");
      for (dbRecordIterator it = r->recordBegin(); it != r->recordEnd(); ++it) {
	if ((*it)->getClassName() == "PixRunConfig") {
	  PixRunConfig *rc = new PixRunConfig((*it)->getName());
	  rc->m_conf->read((*it));
	  rc->m_conf->rev(crev);
	  rc->m_conf->tag(m_tagCF);
	  rc->m_conf->penTag("OnDB");
	  runc->addSubConf(rc);
	}
      }
    }
  }
}

void PixConnectivity::readScan(PixScan *scn, std::string name) {
  if (m_dbServerModeCfg) {
    bool res;
    try { 
      res = scn->m_conf->read(m_dbServer, m_domainNameCF, m_tagCF, name, m_revCF); 
    }
    catch (...) {
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "RECNOTFOUD", "Config record "+name+" not found"));
    } 
    if (!res) throw(PixConnectivityExc(PixConnectivityExc::ERROR, "RECNOTFOUD", "Config record "+name+" not found"));

  } else {
    unsigned int crev, crep;
    std::string cnam;
    DbRecord *r = getCfg(name, m_revCF, cnam, crev, crep, "DISABLE");
    if (r != NULL) {
      scn->m_conf->read(r);
      scn->m_conf->rev(crev);
      scn->m_conf->tag(m_tagCF);
      scn->m_conf->penTag("OnDB");
    } else {
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "RECNOTFOUD", "Config record "+name+" not found"));
    }
  }
}

//void PixConnectivity::writeMod(PixModule *mod, std::string name, unsigned int rev) {
//}

//void PixConnectivity::writeModGroup(PixModuleGroup *modg, std::string name, unsigned int rev) {
//}

//void PixConnectivity::writeTim(TimPixTrigController *tim, std::string name, unsigned int rev) {
//}

void PixConnectivity::writeDisable(PixDisable *dis, std::string name, unsigned int rev) {
  if (m_dbServerModeCfg) {
    bool res;
    try {
      res=dis->m_config->write(m_dbServer, m_domainNameCF, m_tagCF, name, "PixDisable", m_tagPen, rev);
    }  
    catch (...) {
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTWRITEONSERVER", "Cannot write PixDisable "+name+" in the DbServer"));
    }
    if (!res) throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTWRITEONSERVER", "Cannot write PixDisable "+name+" in the DbServer"));
  } else {
    DbRecord *r = NULL;
    r=addCfg(name, rev, "DISABLE");
    if (r!=NULL) {
      try {
	dis->m_config->write(r);
	cleanCfgCache();
      }
      catch (...) {
	throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTWRITEONDISK", "Cannot write PixDisable "+name+" on the disk"));
      }
    } else {
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTWRITEONDISK", "Cannot write PixDisable "+name+" on the disk (record not foud)"));
    }
  }
}

void PixConnectivity::writeRunConfig(PixRunConfig *runc, std::string name, unsigned int rev) {
  if (m_dbServerModeCfg) {
    bool res;
    try {
      res=runc->m_conf->write(m_dbServer, m_domainNameCF, m_tagCF, name, "PixRunConfig", m_tagPen, rev);
    }  
    catch (...) {
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTWRITEONSERVER", "Cannot write PixRunConfig "+name+" in the DbServer"));
    }
    if (!res) throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTWRITEONSERVER", "Cannot write PixRunConfig "+name+" in the DbServer"));
  } else {
    DbRecord *r = NULL;
    r=addCfg(name, rev, "RUNCONF");
    if (r!=NULL) {
      try {
	runc->m_conf->write(r);
	cleanCfgCache();
      }
      catch (...) {
	throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTWRITEONDISK", "Cannot write PixRunConfig "+name+" on the disk"));
      }
    } else {
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTWRITEONDISK", "Cannot write PixRunConfig "+name+" on the disk (record not foud)"));
    }
  }
}
#if 0
void PixConnectivity::writeScan(PixScan *scn, std::string name, unsigned int rev) {
  if (m_dbServerModeCfg) {
    bool res;
    try {
      res=scn->m_conf->write(m_dbServer, m_domainNameCF, m_tagCF, name, "PixScan", m_tagPen, rev);
    }  
    catch (...) {
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTWRITEONSERVER", "Cannot write PixScan "+name+" in the DbServer"));
    }
    if (!res) throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTWRITEONSERVER", "Cannot write PixScan "+name+" in the DbServer"));
  } else {
    DbRecord *r = NULL;
    r=addCfg(name, rev, "SCAN");
    if (r!=NULL) {
      try {
	scn->m_conf->write(r);
	cleanCfgCache();
      }
      catch (...) {
	throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTWRITEONDISK", "Cannot write PixScan "+name+" on the disk"));
      }
    } else {
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTWRITEONDISK", "Cannot write PixScan "+name+" on the disk (record not foud)"));
    }
  }
}
#endif
void PixConnectivity::checkCfgRev(DbRecord *cfg, unsigned int rev, std::string fName) {
  int rep = 0;
  bool done = true;
  std::string srev;
  do {
    done = true;
    std::ostringstream of;
    of << std::setw(12) << std::setfill('0') << rev;
    of << std::setw(3)  << std::setfill('0') << rep;
    srev = of.str();
    for (dbRecordIterator it = cfg->recordBegin(); it != cfg->recordEnd(); ++it) {
      if ((*it)->getClassName() == srev) {
	done = false;
	rep++;
      }
    }
  } while(!done && rep<100);
  if (rep >= 100) {
    throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTCREATEREC", "Cannot create rev record (too many reps)"));    
  } else {
    try {
      cfg->getDb()->transactionStart();
      cfg->addRecord(srev, fName);     
      cfg->getDb()->transactionCommit();
    }
    catch(...) {
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTCREATEREC", "Cannot create rev record"));    
    }
  }
}

DbRecord* PixConnectivity::checkCfgRecord(std::string name, std::string type) {
  struct stat buf;
  int ret;
  bool cfgDirOk = false;
  PixDbInterface *db;
  DbRecord *rootRec;
  if (type == "MODULE") {
    db = m_cfgModDb;
    rootRec = m_cfgModDb->readRootRecord(); 
  } else {
    db = m_cfgDb;
    rootRec = m_cfgDb->readRootRecord(); 
  }
  DbRecord *cfg = NULL;
  for (dbRecordIterator it = rootRec->recordBegin(); it != rootRec->recordEnd(); ++it) {
    if ((*it)->getName() == name && (*it)->getClassName() == type+"_CFG") {
      cfg = (*it);
    }
  }
  if (cfg == NULL) {
    try {
      mode_t m = umask(0000);
      std::string dirn = m_cfgPref + "/" + name;
      mkdir(dirn.c_str(), 0777);
      umask(m);
      ret = stat(dirn.c_str(), &buf);
      if (ret >= 0) {
	if (S_ISDIR(buf.st_mode) != 0) {
	  cfgDirOk = true;
	}
      }
      if (!cfgDirOk) {
	throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTCREATEREC", "Failed to create cfg object folder"));    
      }
      db->transactionStart();
      cfg = rootRec->addRecord(type+"_CFG", name);     
      db->transactionCommit();
    }
    catch (...) {
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTCREATEREC", "Cannot create config record for object "+name));    
    }
  }
  return cfg;
}

std::string PixConnectivity::checkCfgFile(std::string name, unsigned int itim) {

  struct stat buf;
  int ret;
  bool cfgDirOk = false;
  
  ret = stat(m_cfgPref.c_str(), &buf);
  if (ret >= 0) {
    if (S_ISDIR(buf.st_mode) != 0) {
      cfgDirOk = true;
    }
  }
  if (!cfgDirOk) {
    throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTCREATEREC", "Cfg folder does not exists"));    
  }

  int rep = 0;
  bool done = false;
  std::string fName;
  do {
    std::ostringstream of;
    of << std::setw(12) << std::setfill('0') << itim; 
    of << std::setw(3)  << std::setfill('0') << rep;
    if (m_cfgUseCoral) {
      fName = m_cfgPref + "/" + name + "/" + name + "_" + of.str() + ".sql";
    } else {
      fName = m_cfgPref + "/" + name + "/" + name + "_" + of.str() + ".root";
    }
    done = true;
    ret = stat(fName.c_str(), &buf);
    if (ret >= 0) {
      if (S_ISDIR(buf.st_mode) == 0) {
	done = false;
	rep++;
      }
    }
  } while(!done && rep<100);
  if (rep >= 100) {
    throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTCREATEREC", "Cannot create config file"));    
  }
  return fName;  
}  

void PixConnectivity::addCfg(std::vector<std::string> obj, std::vector<std::string> fName, std::vector<unsigned int> itim) {
  m_mutex->lock();
  if (m_dbServerModeCfg) {
  } else {
    openCfg("UPDATE");
    
    PixDbInterface* &db = m_cfgDb;
    for (unsigned int ii=0; ii<obj.size(); ii++) {
      std::string type;
      std::string sobj;
      std::string xobj;
      try {
	getCfgObjType(obj[ii], type, sobj);
      }
      catch (PixConnectivityExc &e) {
	m_mutex->unlock();
	throw(e);
      }
      if (type == "MODULE") {
	xobj = mods[sobj]->prodId();
        db = m_cfgModDb;
      } else {
	xobj = sobj;
      }
      
      DbRecord *cfg = NULL;
      try {
	cfg = checkCfgRecord(xobj, type);
      }
      catch (...) {
	openCfg("READ");
	m_mutex->unlock();
	throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTCREATEREC", "Cannot create config record for object "+obj[ii]));    
      }

      try {
	checkCfgRev(cfg, itim[ii], fName[ii]);
      }
      catch (...) {
	openCfg("READ");
	m_mutex->unlock();
	throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTCREATEREC", "Cannot create revision record for object "+obj[ii]));    
      }    
    } 
    openCfg("READ");
  }
  m_mutex->unlock();
}

void PixConnectivity::addCfg(std::string lobj, std::string fName, unsigned int itim) {
  m_mutex->lock();
  if (m_dbServerModeCfg) {
  } else {
    std::string type;
    std::string obj;
    std::string xobj;
    PixDbInterface* &db = m_cfgDb;
    try {
      getCfgObjType(lobj, type, obj);
    }
    catch (PixConnectivityExc &e) {
      m_mutex->unlock();
      throw(e);
    }
    if (type == "MODULE") {
      xobj = mods[obj]->prodId();
      db = m_cfgModDb;
    } else {
      xobj = obj;
    }

    openCfg("UPDATE");
    DbRecord *cfg = NULL;
    try {
      cfg = checkCfgRecord(xobj, type);
    }
    catch (...) {
      openCfg("READ");
      m_mutex->unlock();
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTCREATEREC", "Cannot create config record for object "+obj));    
    }    

    try {
      checkCfgRev(cfg, itim, fName);
    }
    catch (...) {
      openCfg("READ");
      m_mutex->unlock();
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTCREATEREC", "Cannot create revision record for object "+obj));    
    }    

    openCfg("READ");
    m_mutex->unlock();
  }
}

DbRecord* PixConnectivity::addCfg(std::string lobj, unsigned int itim, std::string type) {
  if (m_dbServerModeCfg) {
  } else {
    m_mutex->lock();
    std::string obj;
    std::string xobj;
    PixDbInterface* &db = m_cfgDb;
    if (type == "") {
      try {
	getCfgObjType(lobj, type, obj);
      }
      catch (PixConnectivityExc &e) {
	m_mutex->unlock();
	throw(e);
      }
    } else {
      obj = lobj;
    }
    if (type == "MODULE") {
      xobj = mods[obj]->prodId();
      db = m_cfgModDb;
    } else {
      xobj = obj;
    }

    openCfg("UPDATE");
    DbRecord *cfg = NULL;
    try {
      cfg = checkCfgRecord(xobj, type);
    }
    catch (...) {
      openCfg("READ");
      m_mutex->unlock();
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTCREATEREC", "Cannot create config record for object "+obj));    
    }

    unsigned int it;
    if (itim == 0xffffffff) {
      it = time(NULL);
    } else if (itim == 0) {
      it = m_revCF;
    } else {
      it = itim;
    }
    
    std::string fName;
    try {  
      fName = checkCfgFile(xobj, it);
      std::string pref = "";
      if (m_cfgUseCoral) pref = "sqlite_file:";
      m_dbs[fName] = openDb(pref+fName, "RECREATE", PixDbCompoundTag("BASE", "BASE", "BASE", "BASE"));
      mode_t m = umask(0000);
      chmod(fName.c_str(), 0777);
      umask(m);
      DbRecord *rootRec = m_dbs[fName]->readRootRecord();
      m_dbs[fName]->transactionStart();
      if (type == "RODBOC") {
	m_dbrs[fName] = rootRec->addRecord("PixModuleGroup", xobj);
      } else if (type == "TIM") {
	m_dbrs[fName] = rootRec->addRecord("TimPixTrigController", xobj);
      } else if (type == "MODULE") {
	m_dbrs[fName] = rootRec->addRecord("PixModule", xobj);
      } else if (type == "DISABLE") { 
	m_dbrs[fName] = rootRec->addRecord("PixDisable", xobj);
      } else if (type == "RUNCONF") { 
	m_dbrs[fName] = rootRec->addRecord("PixRunConfig", xobj);
      } else {
	m_dbrs[fName] = rootRec->addRecord(type+"_CFG", xobj);
      }
      m_dbs[fName]->transactionCommit();
    }
    catch (...) {
      openCfg("READ");
      m_mutex->unlock();
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTCREATEREC", "Cannot create config file for object "+obj));    
    }

    try {
      checkCfgRev(cfg, it, fName);
    }
    catch (...) {
      openCfg("READ");
      m_mutex->unlock();
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTCREATEREC", "Cannot create revision record for object "+obj));    
    }    
    openCfg("READ");
    m_mutex->unlock();
    return m_dbrs[fName];
  }
  return NULL;
}

// add method for new backend
void PixConnectivity::addCfgNewBE(const Config &conf, std::string lobj, unsigned int itim, std::string type) {
  if (m_dbServerModeCfg) {
  } else {
    m_mutex->lock();
    std::string obj;
    std::string xobj;
    PixDbInterface* &db = m_cfgDb;
    if (type == "") {
      try {
	getCfgObjType(lobj, type, obj);
      }
      catch (PixConnectivityExc &e) {
	m_mutex->unlock();
	throw(e);
      }
    } else {
      obj = lobj;
    }
    if (type == "MODULE") {
      xobj = mods[obj]->prodId();
      db = m_cfgModDb;
    } else {
      xobj = obj;
    }

    openCfg("UPDATE");
    DbRecord *cfg = NULL;
    try {
      cfg = checkCfgRecord(xobj, type);
    }
    catch (...) {
      openCfg("READ");
      m_mutex->unlock();
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTCREATEREC", "Cannot create config record for object "+obj));    
    }

    unsigned int it;
    if (itim == 0xffffffff) {
      it = time(NULL);
    } else if (itim == 0) {
      it = m_revCF;
    } else {
      it = itim;
    }
    
    std::string fName;
    try {  
      fName = checkCfgFile(xobj, it);
      std::unique_ptr<RootConf::IConfigStore> store( RootConf::ConfigStoreFactory::configStore());
      store->write(conf, fName,"Def",it);	
    }
    /*
    catch (PixA::ConfigException &excp) {
      std::cout << "Got a config Exception" << std::endl;
      std::cout << "PixConnectivity::add CfgNewEB " << excp.getDescriptor() << std::endl;
      openCfg("READ");
      m_mutex->unlock();
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTCREATEREC", "Cannot create config file for object "+obj));    
    }
    */
    catch (...) {
      openCfg("READ");
      m_mutex->unlock();
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTCREATEREC", "Cannot create config file for object "+obj));    
    }

    try {
      checkCfgRev(cfg, it, fName);
    }
    catch (...) {
      openCfg("READ");
      m_mutex->unlock();
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTCREATEREC", "Cannot create revision record for object "+obj));    
    }    
    openCfg("READ");
    m_mutex->unlock();
  }
}


void PixConnectivity::cleanCfgCache() {
  std::map<std::string, DbRecord*>::iterator it;
  for (it=m_dbrs.begin(); it!=m_dbrs.end(); it++) delete (*it).second;
  m_dbrs.clear();
  std::map<std::string, PixDbInterface*>::iterator it1;
  for (it1=m_dbs.begin(); it1!=m_dbs.end(); it1++) delete (*it1).second;
  m_dbs.clear();
}

// Provate methods for connectivity handling

PixDbInterface* PixConnectivity::openDb(std::string name, std::string mode, PixDbCompoundTag t) {
  //std::cout << "PixConnectivity::openDb( " << name << " " << mode << " " <<
  //	       t.objectDictionaryTag() << "/" << t.connectivityTag() << "/" << t.dataTag() << "/" << t.aliasTag() << '\n'; 
  int nTry = 0;
  PixDbInterface *db = nullptr;
  while (nTry < 5) {
    try {
      PixDbInterfaceFactory fact(name, AutoCommit(false)); 
      if (mode == "READ") {
	db= fact.openDB_readonly(t);
      } else if (mode == "UPDATE") {
	db= fact.openDB_update(t);
      } else if (mode == "RECREATE") {
	db= fact.openDB_recreate(t, "ROOT");
      }
    }
    catch(...) {
      nTry++;
      db = NULL;
      if (nTry >= 5) throw;
      sleep(2);
    }
    if (db != NULL) break;
  }
  return db;
}

void PixConnectivity::add(DbRecord *r, int lev) {
  if (r->getClassName() == "PARTITION") {
    parts[r->getName()] = new PartitionConnectivity(r, lev);
    copyRodCrates(parts[r->getName()]);
  } else  if (r->getClassName() == "RODCRATE") { 
    crates[r->getName()] = new RodCrateConnectivity(r, lev);
    copyRodBocs(crates[r->getName()]);
  } else  if (r->getClassName() == "RODBOC") {
    rods[r->getName()] = new RodBocConnectivity(r, lev);
    copyOBs(rods[r->getName()]);
  } else  if (r->getClassName() == "TIM") {
    tims[r->getName()] = new TimConnectivity(r, lev);
  } else  if (r->getClassName() == "SBC") {
    sbcs[r->getName()] = new SbcConnectivity(r, lev);
  } else  if (r->getClassName() == "OPTOBOARD") {
    obs[r->getName()] = new OBConnectivity(r);
    copyPp0s(obs[r->getName()]);
  } else  if (r->getClassName() == "PP0") {
    pp0s[r->getName()] = new Pp0Connectivity(r, lev);
    copyModules(pp0s[r->getName()]);
  } else  if (r->getClassName() == "MODULE") {
    mods[r->getName()] = new ModuleConnectivity(r);
  } else  if (r->getClassName() == "ROS") { 
    roses[r->getName()] = new RosConnectivity(r, lev);
    copyRobins(roses[r->getName()]);
  } else  if (r->getClassName() == "ROBIN") { 
    robins[r->getName()] = new RobinConnectivity(r, lev);
    copyRols(robins[r->getName()]);
  } else  if (r->getClassName() == "ROL") { 
    rols[r->getName()] = new RolConnectivity(r);
  } else  if (r->getClassName() == "RODBOCLINKMAP") {
    bocmaps[r->getName()] = new RodBocLinkMap(r);
  } else  if (r->getClassName() == "OBLINKMAP") {
    obmaps[r->getName()] = new OBLinkMap(r);
  }
  m_connEmpty = false;
}

void PixConnectivity::copyRodCrates(PartitionConnectivity *p) {
  std::map<std::string, RodCrateConnectivity*>::iterator i;
  for (i=p->rodCrates().begin(); i!=p->rodCrates().end(); ++i) {
    //std::cout << "  Crate " << ((*i).second)->name() << std::endl;
    crates[((*i).second)->name()] = (*i).second;
    copyRodBocs((*i).second);
  }
}

void PixConnectivity::copyRodBocs(RodCrateConnectivity *c) {
  std::map<int, RodBocConnectivity*>::iterator i;
  for (i=c->rodBocs().begin(); i!=c->rodBocs().end(); ++i) {
    //std::cout << "    ROD " << ((*i).second)->name() << std::endl;
    rods[((*i).second)->name()] = (*i).second;
    RodBocConnectivity *r = (*i).second;
    if (r->linkMapSName() != "") {
      if (bocmaps.find(r->linkMapSName()) != bocmaps.end()) {
	r->linkMap(bocmaps[r->linkMapSName()]);
      } 
    }
    if (r->linkMapSName() != "") {
      if (bocmaps.find(r->linkMapSName()) != bocmaps.end()) {
	r->linkMap(bocmaps[r->linkMapSName()]);
      } 
    }
    if (r->linkMap() == NULL) {
      if (r->linkMapName() != "") {
	DbRecord *m;
	if (c->getRec() != NULL) {
	  try {
	    m = c->getRec()->getDb()->DbFindRecordByName(r->linkMapName());
	  }
      catch (PixDBException&) {
	    m = NULL;
	  }
	  if (m != NULL) {
	    bocmaps[m->getName()] = new RodBocLinkMap(m);
	    r->linkMap(bocmaps[m->getName()]);	      
	  }
	}
      }
    }
    if (r->rolName() != "") {
      if (rols.find(r->rolName()) != rols.end()) {
	r->addRol(rols[r->rolName()]);
      }
    }
    copyOBs(r);
  }
  if (c->tim() != NULL) {
    tims[c->tim()->name()] = c->tim();
  }
  if (c->sbc() != NULL) {
    sbcs[c->sbc()->name()] = c->sbc();
  }
}

void PixConnectivity::copyOBs(RodBocConnectivity *r) {
  int l;
  for (l=0; l<4; l++) {
    if (r->obs(l) != NULL) {
      OBConnectivity *o = r->obs(l);
      //std::cout << "      OB " << o->name() << std::endl;
      obs[o->name()] = o;
      o->readPp0(-1);
      if (o->linkMapSName() != "") {
	if (obmaps.find(o->linkMapSName()) != obmaps.end()) {
	  o->linkMap(obmaps[o->linkMapSName()]);
	} 
      }
      if (o->linkMap() == NULL) {
	if (o->linkMapName() != "") {
	  DbRecord *m;
	  if (r->getRec() != NULL) {
	    try {
	      m = r->getRec()->getDb()->DbFindRecordByName(o->linkMapName());
	    }
        catch (PixDBException&) {
	      m = NULL;
	    }
	    if (m != NULL) {
	      obmaps[m->getName()] = new OBLinkMap(m);
	      o->linkMap(obmaps[m->getName()]);	      
	    }
	  }
	}
      }
      copyPp0s(o);
    }
  }
}

void PixConnectivity::copyPp0s(OBConnectivity *o) {
  if (o->pp0() != NULL) {
    Pp0Connectivity *p = o->pp0();
    //std::cout << "      PP0 " << p->name() << std::endl;
    pp0s[p->name()] = p;
    copyModules(p);
  }
}

void PixConnectivity::copyModules(Pp0Connectivity *p) {
  int m;
  for (m=1; m<=8; m++) {
    if (p->modules(m) != NULL) {
      ModuleConnectivity *mod = p->modules(m);
      //std::cout << "        Module " << mod->name() << std::endl;
      mods[mod->name()] = mod;
    }
  }
}

void PixConnectivity::copyRobins(RosConnectivity *r) {
  for (unsigned b=0; b<RosConnectivity::nRobinsPerRos; b++) {
    if (r->robins(b) != NULL) {
      RobinConnectivity *rb = r->robins(b);
      //std::cout << "        Robin " << rb->name() << std::endl;
      robins[rb->name()] = rb;
      copyRols(rb);
    }
  }
}

void PixConnectivity::copyRols(RobinConnectivity *b) {
  for (unsigned r=0; r<RobinConnectivity::nRoLinksPerRobin; r++) {
    if (b->rols(r) != NULL) {
      RolConnectivity *l = b->rols(r);
      //std::cout << "        Rol " << l->name() << std::endl;
      rols[l->name()] = l;
    }
  }
}




