/////////////////////////////////////////////////////////////////////
// PixDisable.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 22/10/06  Version 1.0 (PM)
//           Initial release

//! Class for Ena/Dis mask permanentization

#ifndef _PIXLIB_PIXDISABLE
#define _PIXLIB_PIXDISABLE

#include <map>
#include <string>

#include "Config/Config.h"

namespace PixLib {

class DbRecord;
class PixConnectivity;
class PixDbServerInterface; 
class Config;

class PixDisable {
  friend class PixConnectivity;
public:
  PixDisable(std::string name);
  ~PixDisable();

  void enable(std::string name);
  void disable(std::string name);
  void clear();

  void readConfig(DbRecord *rec);  //! Read from RootDb
  void writeConfig(DbRecord *rec); //! Write to RootDb
  bool readConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, unsigned int revision=0xffffffff);
  bool writeConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, std::string ptag="Disable_Tmp", unsigned int revision=0xffffffff);
  void readConfig(PixConnectivity *conn);  //! Read via a PixConnectivity cfg 
  void writeConfig(PixConnectivity *conn);  //! Write via a PixConnectivity cfg

  void get(PixConnectivity *conn);                  //! Import from a connectivity db
  void put(PixConnectivity *conn);                  //! Export to a connectivity db
  std::string bin(PixConnectivity *conn);           //! Returns a binary encoding based on a connectivity DB
  void bin(std::string bin, PixConnectivity *conn); //! Import a binary encoding based on a connectivity DB

  bool isDisable(std::string name);
  
  Config &config() { return *m_config; };
  std::string name() { return m_config->name(); };
  
protected:
  Config *m_config;        //! Config object
private:
  void propagate(PixConnectivity *conn);

  std::vector<std::string> m_dis;
};

}

#endif
