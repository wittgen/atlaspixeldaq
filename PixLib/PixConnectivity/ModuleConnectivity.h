/////////////////////////////////////////////////////////////////////
// ModuleConnectivity.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 05/10/06  Version 1.0 (PM)
//           Initial release

//! Class for module connectivity

#ifndef _PIXLIB_MODCONNOBJ
#define _PIXLIB_MODCONNOBJ

#include "PixConnectivity/GenericConnectivityObject.h"
#include "PixModuleGroup/PixModuleGroup.h"
#include "PixEnumBase.h"

namespace PixLib {

class Pp0Connectivity;
class RodBocConnectivity;

 class ModuleConnectivity : public GenericConnectivityObject,public EnumMccBandwidth {
public:
  //! Payload accessors
  std::string t0CableId;
  std::string &prodId() { return m_prod; };
  std::string &offlineId() { return m_offline; };
  int groupId;
  int modId;
  MccBandwidth readoutSpeed;
  bool useAltLink;
  int inLink();
  int outLink1A(MccBandwidth mode); 
  int outLink1B(MccBandwidth mode);
  int outLink2A(MccBandwidth mode);
  int outLink2B(MccBandwidth mode);
  //int outLink(MccBandwidth mode = SINGLE_40, bool defaultValue = true);
  int bocLinkTx(); // returns the actual boc TX channel for this module, in the format 10 * TX board + TX channel (0-7)
  int bocLinkRx(int i); // returns the actual boc RX channel DTO2 (i=1) or DTO (i=2) for this module, in the format 10 * RX board + RX channel (0-7)
  int outLink1A(); 
  int outLink1B();
  int outLink2A();
  int outLink2B();
  int bocRxLink1(); 
  //int bocRxLink2();

  //! Enable parameters
  bool enableReadout;
  //! Down links
  //! Up link
  int &pp0Slot() { return m_pp0Slot; };
  Pp0Connectivity* pp0() { return m_pp0; };
  Pp0Connectivity* pp0() const { return m_pp0; };
  void linkPp0(Pp0Connectivity *pp0, int slot);  
  //! Soft links
  std::string &modConfig() { return m_modConfig; };  
  void getModPars(EnumMCCflavour::MCCflavour &mccFlv, EnumFEflavour::FEflavour &feFlv, int &nFe);
  //! Internal connectivity tables
  //! Constructors
  ModuleConnectivity(std::string name, EnumMCCflavour::MCCflavour mccFlv = EnumMCCflavour::PM_MCC_I2,
		     EnumFEflavour::FEflavour feFlv = EnumFEflavour::PM_FE_I2, int nFe=16);
  ModuleConnectivity(DbRecord *r);
  //! Config handling
  void initConfig();
  virtual void readConfig(DbRecord *r);
  virtual void writeConfig(DbRecord *r);
  //! Link to configuration

private:
  using GenericConnectivityObject::readConfig;
  using GenericConnectivityObject::writeConfig;
  int applyFmtMap(int ol, RodBocConnectivity* rod);
  int m_pp0Slot;
  std::string m_modConfig;
  Pp0Connectivity* m_pp0;
  std::string m_prod;
  std::string m_offline;
  EnumMCCflavour::MCCflavour m_mccFlv;
  EnumFEflavour::FEflavour m_feFlv;
  int m_nFe;
  
 };

}

#endif

