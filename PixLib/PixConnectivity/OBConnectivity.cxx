/////////////////////////////////////////////////////////////////////
// OBConnectivity.cxx
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 22/01/07  Version 1.0 (PM)
//           Initial release

//! Class for ROD/BOC connectivity

#include "PixConnectivity/RodBocConnectivity.h"
#include "PixConnectivity/Pp0Connectivity.h"
#include "PixConnectivity/OBConnectivity.h"
#include "PixDbInterface/PixDbInterface.h"
#include "Config/Config.h"

using namespace PixLib;


OBLinkMap::OBLinkMap(std::string name) : 
  GenericConnectivityObject(name) {
  // Create the config object
  initConfig();
}

OBLinkMap::OBLinkMap(DbRecord *r) : 
  GenericConnectivityObject(r) {
  if (r->getClassName() == "OBLINKMAP") {
    // Read record from DB
    storeRec(r);
    // Create the config object
    initConfig();
    readConfig(getRec()); 
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a OBLinkMap object");
  }
}

void OBLinkMap::initConfig() { 
  m_config = new Config(m_name, "OBLINKMAP");
  Config &conf = *m_config;
  conf.addGroup("Identifier");
  conf["Identifier"].addString("Id", m_name, m_name, "Object identifier", true);
  conf.addGroup("Maps");
  int dci[8]  = { 3, 6, 2, 5, 1, 4, 0, 7};
  int dto[8]  = { 4, 1, 5, 2, 6, 3, 7, 8};
  int dto2[8] = { -1, -1, -1, -1, -1, -1, -1, -1 };
  int i;
  std::ostringstream o[8];
  std::string dciFiberStr[8];
  std::string dtoFiberStr[8];
  std::string dto2FiberStr[8];
  for (i=0; i<8; i++) {
    o[i] << "_" << i;
    dciFiberStr[i] = "dciFiber" + o[i].str();
    dtoFiberStr[i] = "dtoFiber" + o[i].str();
    dto2FiberStr[i] = "dto2Fiber" + o[i].str();
    conf["Maps"].addInt(dciFiberStr[i], dciFiber[i], dci[i], "DCI fiber map", true);
    conf["Maps"].addInt(dtoFiberStr[i], dtoFiber[i], dto[i], "DTO fiber map", true);
    conf["Maps"].addInt(dto2FiberStr[i], dto2Fiber[i], dto2[i], "DTO2 fiber map", true);
  }
  conf.reset();
}

void OBLinkMap::readConfig(DbRecord *r) { 
  if (r->getClassName() == "OBLINKMAP") {
    storeRec(r);
    m_name = r->getName();
    // Read the config object
    m_config->read(r);
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a OBLinkMap object");
  }
}

void OBLinkMap::writeConfig(DbRecord *r) { 
  if (r->getClassName() == "OBLINKMAP") {
    storeRec(r);
    // Write the config object
    m_config->write(r); 
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a OBLinkMap object");
  }
}

OBConnectivity::OBConnectivity(std::string name) : 
  GenericConnectivityObject(name), m_pp0(NULL), m_rodBoc(NULL), m_linkMap(NULL) {
  // Create the config object
  initConfig(); 
}

OBConnectivity::OBConnectivity(DbRecord *r) : 
  GenericConnectivityObject(r), m_pp0(NULL), m_rodBoc(NULL), m_linkMap(NULL) {
  if (r->getClassName() == "OPTOBOARD") {
    // Read record from DB
    storeRec(r);
    // Create the config object
    initConfig();
    readConfig(getRec()); 
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a OBConnectivity object");
  }
}

void OBConnectivity::initConfig() { 
  m_config = new Config(m_name, "OPTOBOARD");
  Config &conf = *m_config;

  conf.addGroup("Cables");
  conf["Cables"].addString("txRibbonId", m_txRibbonId, "NULL", "TX ribbon id", true);
  conf["Cables"].addString("rxRibbonId_0", m_rxRibbonId[0], "NULL", "RX ribbon id 0", true);
  conf["Cables"].addString("rxRibbonId_1", m_rxRibbonId[1], "NULL", "RX ribbon id 1", true);
  conf["Cables"].addInt("txPp1Slot", m_txPp1Slot, -1, "TX slot at PP1", true);
  conf["Cables"].addInt("rxPp1Slot_0", m_rxPp1Slot[0], -1, "RX 0 slot at PP1", true);
  conf["Cables"].addInt("rxPp1Slot_1", m_rxPp1Slot[1], -1, "RX 1 slot at PP1", true);
  conf.addGroup("Config");
  conf["Config"].addFloat("vIset", m_vIset, 0.69, "Viset value", true);  
  conf["Config"].addFloat("vPin", m_vPin, 10.0, "Vpin value", true);  
  conf.addGroup("Down");
  conf["Down"].addLinkRO("PP0", m_pp0Name, "", "Link to PP0", true);
  conf.addGroup("Link");
  conf["Link"].addLink("LINK_MAP", m_linkMapName, "", "Link to fiber maps", true);
  conf["Link"].addString("mapSName", m_linkMapSName, "", "Link to fiber map type", true);

  conf.reset();
}

int& OBConnectivity::bocRxSlot(int slot) {
  static int dummy = -1;
  if (slot >=0 && slot < 2) {
    return m_bocRxSlot[slot];
  }
  return dummy;
}


void OBConnectivity::readConfig(DbRecord *r) { 
  if (r->getClassName() == "OPTOBOARD") {
    storeRec(r);
    m_name = r->getName();
    // Read the config object
    m_config->read(r); 
    if (m_linkMap != NULL && m_linkMapName != "") {
      if (m_linkMapSName != m_linkMap->name()) {
	m_linkMap = NULL;
      }
    }
    if (m_pp0 != NULL && m_pp0Name != "") {
      if (m_pp0SName != m_pp0->name()) {
	m_pp0 = NULL;
      }
    }
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a OBConnectivity object");
  }
}

void OBConnectivity::writeConfig(DbRecord *r) { 
  if (r->getClassName() == "OPTOBOARD") {
    storeRec(r);
    // Check if the links are up to date
    if (m_linkMap != NULL) {
      if (m_linkMap->decName() != "") {
	m_linkMapName = m_linkMap->decName();
	m_linkMapSName = m_linkMap->name();
      }
    }
    if (m_pp0 != NULL) {
      if (m_pp0->decName() != "") {
	m_pp0Name = m_pp0->decName();
	m_pp0SName = m_pp0->name();
      }
    }
    // Write the config object
    m_config->write(r); 
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a OBConnectivity object");
  }
}

void OBConnectivity::linkMap(OBLinkMap *obl) {
  m_linkMap = obl;
  m_linkMapName = obl->decName();
  m_linkMapSName = obl->name();
}

void OBConnectivity::linkRodBoc(RodBocConnectivity *rod, int tx, int rx0, int rx1) {
  m_rodBocName = rod->name();
  m_bocTxSlot = tx;
  m_bocRxSlot[0] = rx0;
  m_bocRxSlot[1] = rx1;
  m_rodBoc = rod;
}

void OBConnectivity::readPp0(int lev) {
  DbRecord *r;
  if (m_db != NULL) {
    try {
      r = m_db->DbFindRecordByName(m_pp0Name);
    }
    catch (PixDBException&) {
      r = NULL;
    }
    if (r != NULL) {
      Pp0Connectivity *pp0 = new Pp0Connectivity(r, --lev);
      addPp0(pp0);
    }
  }
}

void OBConnectivity::addPp0(Pp0Connectivity *pp0) {
  if (m_pp0 != NULL) {
    delete m_pp0;
  }
  m_pp0 = pp0;
  m_pp0SName = pp0->name();
  m_pp0Name = pp0->decName();
  pp0->linkOB(this);
}



