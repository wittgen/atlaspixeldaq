/////////////////////////////////////////////////////////////////////
// OBConnectivity.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 05/10/06  Version 1.0 (PM)
//           Initial release

//! Class for ROD/BOC connectivity

#ifndef _PIXLIB_OBCONNOBJ
#define _PIXLIB_OBCONNOBJ

#include "PixConnectivity/GenericConnectivityObject.h"

#include <map>

namespace PixLib {

class Pp0Connectivity;
class RodBocConnectivity;

class OBLinkMap : public GenericConnectivityObject {
public:
  //! Payload accessors
  //! Down links
  //! Up link
  //! Soft links
  //! Internal connectivity tables
  int dciFiber[8];
  int dtoFiber[8];
  int dto2Fiber[8];
  //! Constructors
  OBLinkMap(std::string name);
  OBLinkMap(DbRecord *r);
  //! Config handling
  void initConfig();
  virtual void readConfig(DbRecord *r);
  virtual void writeConfig(DbRecord *r);
  //! Link to configuration
private:
  using GenericConnectivityObject::readConfig;
  using GenericConnectivityObject::writeConfig;    
};

class OBConnectivity : public GenericConnectivityObject {
public:
  //! Payload accessors
  std::string &txRibbonId() { return m_txRibbonId; };
  std::string &txRibbonId(int i) {
    assert(i>=0 && i<2);
    return m_rxRibbonId[i]; 
  };
  int &txPp1Slot() { return m_txPp1Slot; };
  int &rxPp1Slot(int i) {
    assert(i>=0 && i<2);
    return m_rxPp1Slot[i];
  };
  float &vIset() { return m_vIset; };
  float &vPin() { return m_vPin; };
  //! Configuration file parameter
  //! Down links
  //! Up link
  int &bocRxSlot(int slot);
  int &bocTxSlot() { return m_bocTxSlot; };
  std::string &rodBocName() { return m_rodBocName; };
  RodBocConnectivity* rodBoc() { return m_rodBoc; };
  void linkRodBoc(RodBocConnectivity *rb, int tx, int rx0, int rx1);  
  //! Soft links
  Pp0Connectivity* pp0() { return m_pp0; };
  std::string pp0Name() { return m_pp0Name; };
  std::string pp0SName() { return m_pp0SName; };
  void readPp0(int lev);
  void addPp0(Pp0Connectivity *pp0);
  //! Internal connectivity tables
  void linkMap(OBLinkMap *obl);
  OBLinkMap* linkMap() { return m_linkMap; };
  std::string linkMapName() { return m_linkMapName; };
  std::string linkMapSName() { return m_linkMapSName; };
  //! Constructors
  OBConnectivity(std::string name);
  OBConnectivity(DbRecord *r);
  //! Config handling
  void initConfig();
  virtual void readConfig(DbRecord *r);
  virtual void writeConfig(DbRecord *r);
  //! Link to configuration
private:
  int m_bocTxSlot;
  int m_bocRxSlot[2];
  Pp0Connectivity *m_pp0;
  std::string m_pp0Name;
  std::string m_pp0SName;
  RodBocConnectivity *m_rodBoc;
  std::string m_rodBocName;
  OBLinkMap *m_linkMap;
  std::string m_linkMapName;
  std::string m_linkMapSName;
  std::string m_txRibbonId;
  std::string m_rxRibbonId[2];
  int m_txPp1Slot;
  int m_rxPp1Slot[2];
  float m_vIset;
  float m_vPin;
  using GenericConnectivityObject::readConfig;
  using GenericConnectivityObject::writeConfig;
};

}

#endif
