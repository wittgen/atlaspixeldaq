/////////////////////////////////////////////////////////////////////
// RodCrateConnectivity.cxx
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 05/10/06  Version 1.0 (PM)
//           Initial release

//! Class for ROD crate connectivity

#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixConnectivity/RodBocConnectivity.h"
#include "PixConnectivity/TimConnectivity.h"
#include "PixConnectivity/SbcConnectivity.h"
#include "PixDbInterface/PixDbInterface.h"
#include "Config/Config.h"

using namespace SctPixelRod;
using namespace PixLib;

RodCrateConnectivity::RodCrateConnectivity(std::string name) : 
  GenericConnectivityObject(name), m_tim(NULL), m_sbc(NULL), m_partition(NULL) {
  // Create the config object
  initConfig(); 
}

RodCrateConnectivity::RodCrateConnectivity(DbRecord *r, int lev) : 
  GenericConnectivityObject(r), m_tim(NULL), m_sbc(NULL), m_partition(NULL) {
  if (r->getClassName() == "RODCRATE") {
    // Read record from DB
    storeRec(r);
    // Create the config object
    initConfig();
    readConfig(getRec(), lev); 
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a RodCrateConnectivity object");
  }
}

void RodCrateConnectivity::initConfig() { 
  m_rodBocs.clear();
  m_config = new Config(m_name, "RODCRATE");
  Config &conf = *m_config;

  conf.addGroup("TDAQ");
  conf["TDAQ"].addString("histoServName", m_histoServName, "PixHistoServer", "Histogram server name", true);
  conf.addGroup("Enable");
  conf["Enable"].addBool("readout", enableReadout, 1, "Module readout enable", true);

  conf.reset();
}

void RodCrateConnectivity::addRodBoc(int slot, RodBocConnectivity *rod) {
  if (m_rodBocs.find(slot) != m_rodBocs.end()) {
    delete m_rodBocs[slot];
  }
  m_rodBocs[slot] = rod;
  rod->linkCrate(this, slot);
}

void RodCrateConnectivity::addTim(int slot, TimConnectivity *tim) {
  m_timSlot = slot;
  m_tim = tim;
  tim->linkCrate(this, slot);
}

void RodCrateConnectivity::addSbc(int slot, SbcConnectivity *sbc) {
  m_sbcSlot = slot;
  m_sbc = sbc;
  sbc->linkCrate(this, slot);
}

void RodCrateConnectivity::readConfig(DbRecord *r, int lev) { 
  if (r->getClassName() == "RODCRATE") {
    storeRec(r);
    m_name = r->getName();
    // Read the config object
    m_config->read(r); 
    if (lev != 0) {
      lev--;
      // Create child RODs
      for(dbRecordIterator it = r->recordBegin(); it != r->recordEnd(); it++) {
	// Look for RodBoc records
	if ((*it)->getClassName() == "RODBOC") {
	  std::string dname = it.srcSlotName(); 
	  std::size_t pos = dname.find("SLOT_");
	  if (pos != std::string::npos) {
	    std::istringstream snum(dname.substr(pos+5));
	    int islot;
	    snum >> islot;
	    RodBocConnectivity *rod = new RodBocConnectivity(*it, lev);
	    addRodBoc(islot, rod);
	  }
	}
	if ((*it)->getClassName() == "TIM") {
	  std::string dname = it.srcSlotName(); 
	  std::size_t pos = dname.find("SLOT_");
	  if (pos != std::string::npos) {
	    std::istringstream snum(dname.substr(pos+5));
	    int islot;
	    snum >> islot;
	    TimConnectivity *tim = new TimConnectivity(*it, lev);
	    addTim(islot, tim);
	  }
	}
	if ((*it)->getClassName() == "SBC") {
	  std::string dname = it.srcSlotName(); 
	  std::size_t  pos = dname.find("SLOT_");
	  if (pos != std::string::npos) {
	    std::istringstream snum(dname.substr(pos+5));
	    int islot;
	    snum >> islot;
	    SbcConnectivity *sbc = new SbcConnectivity(*it, lev);
	    addSbc(islot, sbc);
	  }
	}
      }
    }
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a RodCrateConnectivity object");
  }
}

void RodCrateConnectivity::writeConfig(DbRecord *r, int lev) { 
  if (r->getClassName() == "RODCRATE") {
    storeRec(r);
    // Read the config object
    m_config->write(r); 
    if (lev != 0) {
      lev--;
      // Create child RODs
      std::map<int, RodBocConnectivity*>::iterator it;
      for(it = m_rodBocs.begin(); it != m_rodBocs.end(); it++) {
	// Write RodBoc
	std::ostringstream lname;
        lname << "SLOT_" << (*it).first;
	DbRecord *sr;
	try {
	  sr = r->addRecord("RODBOC", lname.str(), ((*it).second)->name());
	}
	  catch (const PixDBException&) {
	    dbRecordIterator ri;
	    if ((ri = r->findRecord(lname.str())) == r->recordEnd()) {  
	      throw ConnectivityExc(ConnectivityExc::ERROR,"Cannot create "+lname.str()+"/RODBOC record");
	    }
	    sr = *ri;
	  }

	((*it).second)->writeConfig(sr, lev);
      }
      // Create TIM
      if (m_tim != NULL) {
	std::ostringstream lname;
        lname << "SLOT_" << m_timSlot;
	DbRecord *sr;
	try {
	  sr = r->addRecord("TIM", lname.str(), m_tim->name());
	}
	  catch (const PixDBException&) {
	    dbRecordIterator ri;
	    if ((ri = r->findRecord(lname.str())) == r->recordEnd()) {  
	      throw ConnectivityExc(ConnectivityExc::ERROR,"Cannot create "+lname.str()+"/TIM record");
	    }
	    sr = *ri;
	  }
	m_tim->writeConfig(sr, lev);
      }
      // Create SBC
      if (m_sbc != NULL) {
	std::ostringstream lname;
        lname << "SLOT_" << m_sbcSlot;
	DbRecord *sr;
	try {
	  sr  = r->addRecord("SBC", lname.str(), m_sbc->name());
	}
	catch (const PixDBException&) {
	  dbRecordIterator ri;
	  if ((ri = r->findRecord(lname.str())) == r->recordEnd()) {  
	    throw ConnectivityExc(ConnectivityExc::ERROR,"Cannot create "+lname.str()+"/SBC record");
	  }
	  sr = *ri;
	}
	m_sbc->writeConfig(sr, lev);
      }
    }
  } else {
    throw ConnectivityExc(ConnectivityExc::ERROR,"Wrong record type for a RodCrateConnectivity object");
  }
}

void RodCrateConnectivity::linkPartition(PartitionConnectivity *part) {
  m_partition = part;
}
