/////////////////////////////////////////////////////////////////////
// GenericConnectivityObject.cxx
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 05/10/06  Version 1.0 (PM)
//           Initial release

//! Base Class for connectivity objects

#include "PixConnectivity/GenericConnectivityObject.h"
#include "PixDbInterface/PixDbInterface.h"
#include "Config/Config.h"

using namespace PixLib;

GenericConnectivityObject::GenericConnectivityObject(DbRecord *r) { 
  m_active = true;
  storeRec(r); 
}

GenericConnectivityObject::~GenericConnectivityObject() {
  if (m_config != NULL) delete m_config;
}

void GenericConnectivityObject::readConfig(int lev) { 
  // Read from DB
  if (getRec() != NULL) {
    readConfig(getRec(), lev);
  }
}

void GenericConnectivityObject::readConfig(DbRecord *r, int lev) { 
  // Read from DB
  m_config->read(r);
  storeRec(r);
}

void GenericConnectivityObject::writeConfig(int lev) { 
  // Write to DB
  if (getRec() != NULL) { 
    writeConfig(getRec(), lev);
  }
}

void GenericConnectivityObject::writeConfig(DbRecord *r, int lev) { 
  // Write to DB
  m_config->write(r);
  storeRec(r);
}

std::string GenericConnectivityObject::decName() {
  return m_recName;
}

void GenericConnectivityObject::storeRec(DbRecord *r) {
  if (r != NULL) {
    m_name = r->getName();
    m_db = r->getDb();
    m_recName = r->getDecName();
  }
}

DbRecord *GenericConnectivityObject::getRec() {
  if (m_db != NULL) {
    return m_db->DbFindRecordByName(m_recName);
  }
  return NULL;
}


