#include <iomanip>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <csignal>

#include <ipc/core.h>
#include <owl/semaphore.h>
#include <pmg/pmg_initSync.h>
#include <sys/wait.h>

#include "PixConnectivity/SbcConnectivity.h"
#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixUtilities/PixMessages.h"
#include "PixUtilities/PixISManager.h"
#include "PixActions/PixActionsMulti.h"
#include "PixBroker/PixBrokerSingleCrate.h"
#include "PixThreads/PixThreadsMem.h"
#include "PixHistoServer/PixHistoServerInterface.h"
#include "PixThreads/PixThread.h"
#include "PixThreads/PixASIFMon.h"
#include "PixThreads/PixASRestartMon.h"

using namespace PixLib;
using namespace SctPixelRod;

OWLSemaphore* sem=nullptr;

void signal_handler(int signal) {
  if(sem){
    sem->post();
  }
}

void mem(int del) {
  pid_t pid = getpid();
  std::ostringstream os;
  os << "/proc/" << pid << "/status";
  std::cout << "Mem Info (" << os.str() << "):" << std::endl;
  std::ifstream st(os.str().c_str());
  while (!st.eof()) {
    std::string line;
    std::getline(st, line);
    if (line.substr(0,2) == "Vm") {
      std::cout << "  " << line << std::endl;
    }
  }
  sleep(del);
}

int main(int argc, char **argv) {
  // Check arguments
  mem(0);

  std::string crateBrokerName  = argv[1];
  std::string ipcPartitionName = argv[2];
  std::string serverName = argv[3];
  std::string idTag = argv[4];
  std::string connTag = argv[5];
  std::string  cfgTag = argv[6];
  std::string cfgMTag;
  std::string nameServer;
  std::string oh;
  std::string applName = "";

  if(argc==9) {
    cfgMTag=cfgTag;
    nameServer = argv[7];
    oh = argv[8];
  } else if (argc==10) {
    cfgMTag=argv[7];
    nameServer = argv[8];
    oh = argv[9];
  } else {
    std::cout << "Argc " << argc << std::endl;
    for(int i = 0; i<argc; i++) std::cout << "arg " << i << " " << argv[i] << std::endl;
    std::cout << "USAGE: PixActionsServer [crate name] [partition name] [pixdbservername] [id tag] [conn tag] [cfg tag] [cfgMod tag](optional) [name server] [oh]" << std::endl;
    std::cout << argv[0] << std::endl;
    return 1;
  }
  
  std::string crateName = crateBrokerName.substr (crateBrokerName.find(":")+1,crateBrokerName.length());
  
  char rcc_Name[64];
  int size=64;
  gethostname(rcc_Name, size);
  std::cout<<rcc_Name<<std::endl;
  std::string ipName(rcc_Name); 
  std::cout<<"hostname = "<<ipName<<std::endl;

  struct timeval t0; 
  struct timeval t1;
  struct timeval t2;
  //int time;
  
  // Start IPCCore    
//#define TRACE_IPC
#ifdef TRACE_IPC
  std::list<std::pair<std::string, std::string> > options = IPCCore::extractOptions(argc, argv);
  options.push_front(std::make_pair(std::string("traceLevel"), boost::lexical_cast<std::string>((int) 25)));
  options.push_front(std::make_pair(std::string("traceTime"), boost::lexical_cast<std::string>((int) 1)));
  IPCCore::init(options);
#else
  //IPCCore::init(argc, argv);
  std::list<std::pair<std::string, std::string> > options = IPCCore::extractOptions(argc, argv);
  options.push_front(std::make_pair(std::string("traceLevel"), boost::lexical_cast<std::string>((int) 0)));
  options.push_front(std::make_pair(std::string("traceTime"), boost::lexical_cast<std::string>((int) 0)));
  IPCCore::init(options);
#endif
  IPCPartition *ipcPartition = NULL;
  // Create IPCPartition constructors                                                              
  std::cout << "Starting partition " << ipcPartitionName << std::endl;
  try {
    ipcPartition = new IPCPartition(ipcPartitionName);
  } catch(...) {
    std::cout << "PixActionsServer: FATAL!! problems while connecting to the partition" << std::endl;
    return -1;
  }
  std::cout << "Successfully connected to partition " <<  ipcPartitionName << std::endl << std::endl;

  std::stringstream info1;
  info1 << rcc_Name;
  applName=info1.str();

  //Create IS manager are read current tags
  PixISManager *ism = new PixISManager(ipcPartition, "PixelRunParams");
  try {
    std::string idt = ""; 
    idt = ism->read<string>("DataTakingConfig-IdTag");
    std::string cot = "";
    cot = ism->read<string>("DataTakingConfig-ConnTag");
    std::string cft = "";
    cft = ism->read<string>("DataTakingConfig-CfgTag");
    std::string cfmt = "";
    cfmt = ism->read<string>("DataTakingConfig-ModCfgTag");
    if (idt != "") idTag = idt;
    if (cft != "") cfgTag  = cft;
    if (cot != "") connTag = cot;
    if (cfmt != "") cfgMTag = cfmt;
    std::string msg="Default tags changed to "+idTag+"/"+connTag+"/"+cfgTag+"/"+cfgMTag;
    ers::log(PixLib::pix::daq (ERS_HERE,applName, msg));
  }
  catch (...) {
    ers::warning(PixLib::pix::daq (ERS_HERE,applName,"Cannot read default tags from IS"));
  }
  
  PixDbServerInterface *DbServer = NULL;
  bool ready=false;
  int nTry=0;
  std::string msg = "Trying to connect to DbServer with name "+serverName ;
  ers::debug(PixLib::pix::daq (ERS_HERE,applName, msg));
  do {
    sleep(1);
    DbServer=new  PixDbServerInterface(ipcPartition,serverName,PixDbServerInterface::CLIENT, ready);
    if(!ready) delete DbServer;
    else break;
    std::stringstream info2;
    info2 << " ..... attempt number: " << nTry;
    msg = info2.str();
    ers::debug(PixLib::pix::daq (ERS_HERE,applName, msg));
    nTry++;
  } while(nTry<20);
  if(!ready) {
    msg = " Impossible to connect to DbServer " + serverName;
    ers::fatal(PixLib::pix::daq (ERS_HERE,applName, msg));
    return -1;
  } else {
    msg = "Successfully connected to DbServer with name " + serverName;
    ers::debug(PixLib::pix::daq (ERS_HERE,applName, msg));  
  }
  
  sleep(2);
  DbServer->ready();
  msg = "DbServer is enabled for reading .... ";
  ers::debug(PixLib::pix::daq (ERS_HERE,applName, msg));
  //time=
          (void)gettimeofday(&t0,NULL);

  // ROUTINE:Create new PixConnectivity and get crate name
  PixConnectivity *conn = NULL;
  RodCrateConnectivity *rodCrConn=NULL;
  
  try{
    conn = new PixConnectivity(DbServer, connTag, connTag, connTag, cfgTag, idTag, cfgMTag);
    conn->loadConn();
  }
  catch(PixConnectivityExc &e) {
    msg = "PixConnectivity  exception caught in PixActionsServer: " + e.getDescr();
    ers::fatal(PixLib::pix::daq (ERS_HERE,applName, msg));    
    return -1;
  }
  catch(...) { 
    msg = " Unknown exception caught in PixActionsServer";
    ers::fatal(PixLib::pix::daq (ERS_HERE,applName, msg));
    return -1;
  }
    //time=
            (void)gettimeofday(&t2,NULL);
  //std::cout<< std::endl << "Time to load connectivity from DbServer is: " << t2.tv_sec - t0.tv_sec+ 1e-6*(t2.tv_usec-t0.tv_usec) << " s " << std::endl << std::endl;  
  
  //std::cout << std::endl << " hostname is:  "<<ipName<<std::endl;
  bool isCrateInConn = false;
  std::map<std::string, SbcConnectivity*>::iterator sbcIt;
  for(sbcIt = (conn->sbcs).begin(); sbcIt != (conn->sbcs).end(); sbcIt++ ) {
    std::cout<<" - in loop: SbcName = " << (sbcIt->second)->ipName() << std::endl;
      if (crateName == ((sbcIt->second)->crate())->name()){isCrateInConn = true; rodCrConn = (sbcIt->second)->crate();break;}
  }
  
  if(!isCrateInConn) {
    msg = " Couldn't find "+ crateName +" into connectivity (or I couldn't retrieve the correct name from SBC)";
    ers::fatal(PixLib::pix::daq (ERS_HERE,applName, msg));  
    return -1;
  }
  msg = "Chosen CrateName is: "+crateName;
  ers::debug(PixLib::pix::daq (ERS_HERE,applName, msg));
  
  if (rodCrConn!= NULL)  {
    if (!rodCrConn->enableReadout) { // and isActive??????
      msg = "Crate "+crateName+" is disabled and no BrokerSingleCrate will be created";
      ers::warning(PixLib::pix::daq (ERS_HERE,applName, msg));
      crateName="CRATE_NOT_FOUND";
    }
  }
  
  // Create HistoServerInterface
  PixHistoServerInterface *hInt=nullptr;
  try{
    if (crateName!="CRATE_NOT_FOUND") {
      hInt = new PixHistoServerInterface(ipcPartition, oh, nameServer, PROVIDERBASE+crateName);
    }
  } catch(...){
    msg = "Couldn't create PixHistoServerInterface";
    ers::error(PixLib::pix::daq (ERS_HERE,applName, msg));
    delete hInt;
    hInt = 0;
  }

  // Create PixBroker
  PixBrokerSingleCrate *crateBroker = nullptr;
  try {
    if (crateName!="CRATE_NOT_FOUND" && hInt)
      crateBroker = new PixBrokerSingleCrate(ipcPartition, crateBrokerName, DbServer, hInt, conn);
  } catch(...) {
    msg = "Couldn't create PixBrokerSingleCrate";
    ers::error(PixLib::pix::daq (ERS_HERE,applName, msg));
    //delete crateBroker;
    crateBroker->_destroy();
    crateBroker = nullptr;
  }
  
  // Info dump
  if(crateBroker) {
    std::cout << std::endl;
    std::cout << "Created " << crateBroker->name() << " broker" << std::endl;
    std::set<std::string> brokersList = crateBroker->listSubBrokers();
    std::cout << "Broker contains " << brokersList.size() << " sub brokers" << std::endl;
    std::set<std::string>::iterator broker, brokerEnd=brokersList.end();
    for(broker=brokersList.begin(); broker!=brokerEnd; broker++)
      std::cout << "  " << (*broker) << std::endl;
    std::set<std::string> actionsList = crateBroker->listActions(false);
    std::cout << "Broker contains " << actionsList.size() << " actions" << std::endl;
    std::set<std::string>::iterator actions, actionsEnd=actionsList.end();
    for(actions=actionsList.begin(); actions!=actionsEnd; actions++)
      std::cout << "  name: " << (*actions) << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
  } else {
    std::cout << "NO ENTRIES FOR " << crateName << " CRATE..." << std::endl;
  }

  if(crateBroker) {
    
    //Create IS manager are read current tags
    PixISManager *isDDC = new PixISManager(ipcPartition, "DDC");

    // Create a PixThreads
    PixThread *threadIF = new PixThread("ASIF");
    // Create the ASIF monitor task
    PixASIFMon *mon = new PixASIFMon(isDDC, crateBroker);
    threadIF->setNewTask(mon);
    threadIF->startExecution();
    
    //#define ASYNC_ACTIONS_RESTART
    #ifdef ASYNC_ACTIONS_RESTART
    PixThread *actionsRestartThread = new PixThread("ASRestart");
    PixASRestartMon *actionsRestartMon = new PixASRestartMon(ism, crateBroker);
    actionsRestartThread->setNewTask(actionsRestartMon);
    actionsRestartThread->startExecution();
    #endif

    //time=
              (void)gettimeofday(&t1,NULL);
    std::cout<< std::endl << "Time for the operation is " << t1.tv_sec - t0.tv_sec+ 1e-6*(t1.tv_usec-t0.tv_usec) << " s " << std::endl << std::endl;  
    mem(0);
    pmg_initSync();
 
    PixThreadsMem* memMon=new PixThreadsMem(ism,crateName);
    memMon->startExecution();
    
    // Install the signal handler
    std::signal(SIGINT   , signal_handler);
    std::signal(SIGQUIT  , signal_handler);
    std::signal(SIGILL   , signal_handler);
    std::signal(SIGTRAP  , signal_handler);
    std::signal(SIGABRT  , signal_handler);
    std::signal(SIGIOT   , signal_handler);
    std::signal(SIGBUS   , signal_handler);
    std::signal(SIGFPE   , signal_handler);
    std::signal(SIGKILL  , signal_handler);
    std::signal(SIGSEGV  , signal_handler);
    std::signal(SIGPIPE  , signal_handler);
    std::signal(SIGTERM  , signal_handler);
    std::signal(SIGSTKFLT, signal_handler);
    std::signal(SIGSYS   , signal_handler);

    // Start server
    sem = new OWLSemaphore();
    sem->wait();
    delete sem;

    memMon->stopExecution();
    threadIF->stopExecution();

      if(crateBroker) {
        crateBroker->destroyActions();
        crateBroker->_destroy();
      }
    }

    ers::log(PixLib::pix::daq (ERS_HERE,applName, "PixActionsServer exiting..."));

  return EXIT_SUCCESS;
}


