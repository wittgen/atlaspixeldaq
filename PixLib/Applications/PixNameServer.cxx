#include <set>
#include <iostream>
#include <unistd.h>
#include <csignal>

#include <owl/semaphore.h>
#include <ipc/core.h>

#include <pmg/pmg_initSync.h>

#include "PixHistoServer/PixNameServer.h"
#include "PixUtilities/PixMessages.h"


using namespace PixLib;


OWLSemaphore* sem=nullptr;

void signal_handler(int signal) {
  if(sem){
    sem->post();
  }
}

int main(int argc, char **argv) {


  if(argc!=3) {
    std::cout << "USAGE: PixNameServer [partition name] [name server] " << std::endl;
    return 1;
  }
  
  std::string ipcPartitionName = argv[1];
  std::string nameServer = argv[2];
  std::cout << "partition name: "   << ipcPartitionName << std::endl;
  std::cout << "name server name: " <<       nameServer << std::endl;

  //Starting messages class
  std::string applName = "PixNameServer";
  std::string msg;

  // Start IPCCore    
  IPCCore::init(argc, argv);
  IPCPartition *ipcPartition;
  try {
    ipcPartition = new IPCPartition(ipcPartitionName);
  } catch(...) {
    ers::fatal(PixLib::pix::daq (ERS_HERE, applName,"PixNameServer: FATAL!! problems while connecting to the partition"));
    return -1;
  }
  ers::info(PixLib::pix::daq (ERS_HERE, applName,"Successfully connected to partition "+ ipcPartitionName));

  //Starting the name_server class
  PixNameServer *nServer=NULL;  
  try { 
    nServer = new PixNameServer(ipcPartition, nameServer);
  } catch (const PixNameServerExc &e) {
    msg = "Exception caught in the name server constructor: "+ e.getDescr() + " ";
    ers::fatal(PixLib::pix::daq (ERS_HERE, applName, msg));
    return -1;
  }
  if (nServer==NULL) {
    msg = "The "+nameServer+" has been created null ";
    ers::error(PixLib::pix::daq (ERS_HERE, applName, msg));
    return -1;
  } else {
    applName = "new()";
    msg = "Successfully created the nameServer "+nameServer+" "; 
    ers::log(PixLib::pix::daq (ERS_HERE, applName, msg));
  }

  // Install the signal handler
  std::signal(SIGINT   , signal_handler);
  std::signal(SIGQUIT  , signal_handler);
  std::signal(SIGILL   , signal_handler);
  std::signal(SIGTRAP  , signal_handler);
  std::signal(SIGABRT  , signal_handler);
  std::signal(SIGIOT   , signal_handler);
  std::signal(SIGBUS   , signal_handler);
  std::signal(SIGFPE   , signal_handler);
  std::signal(SIGKILL  , signal_handler);
  std::signal(SIGSEGV  , signal_handler);
  std::signal(SIGPIPE  , signal_handler);
  std::signal(SIGTERM  , signal_handler);
  std::signal(SIGSTKFLT, signal_handler);
  std::signal(SIGSYS   , signal_handler);

  pmg_initSync();

  sem = new OWLSemaphore();
  sem->wait();
  delete sem;

  return EXIT_SUCCESS;
}

