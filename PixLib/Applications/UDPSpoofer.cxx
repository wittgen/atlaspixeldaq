#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstring>

#include "QSTalkTask.h"


uint8_t buffer[1024];
uint32_t len=0;

void add8(uint8_t val) {
  buffer[len]=val; len++;
}


void add16(uint16_t val) {
  uint16_t v=htons(val);
  memcpy((void *)&buffer[len],(void*)&v,2);len+=2;
}


void add32(uint32_t val) {
  uint32_t v=htonl(val);
  memcpy((void*)&buffer[len],(void*)&v,4);len+=4;

}




int main (int argc, char** argv) {
  
  int num=0;
  int packetlen=17;
  int sleeptime = atoi(argv[1]);
  srand(time(NULL));
  
  //Create UDP socket and initialize server
  int socketId = socket(AF_INET,SOCK_DGRAM,0);
  if (socketId < 0)
    return 1;

  struct sockaddr_in serv_addr;
  bzero((char*) &serv_addr, sizeof(serv_addr));
  
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port   = htons(11111);
  serv_addr.sin_addr.s_addr = inet_addr("192.168.1.141");
  
  //Connect blocks every other UDP packet!
  /*
  if (connect(socketId, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) <  0)
  {
    std::cout<<"Could not connect to server"<<std::endl;
    return 1;
  }
  else
    std::cout<<"Connected to the server.."<<std::endl;
  */ 
  
  
  
  while (1)
    {
      
      
      uint32_t crate  = (rand() % 7);
      uint32_t rod    = (rand() % 16)+5;
      std::cout<<"Building and sending packet: "<<num<<"crate:"<<crate<<"rod:"<<rod<<std::endl;
      len=0;
  
      add32(0xaa4321aa); // header
      add32(packetlen);
      add32(num);
      add32(crate);
      add32(rod); 
      
      
      //Stupid message
      add32(1);
      add32(4);
      add32(1);
      add32(1);
      add32(4);
      add32(1);
      add32(1);
      add32(4);
      add32(1);
      add32(14);
      add32(41);
      add32(11);
      
      //Send the message 
      std::cout<<"Sending to socketId"<<socketId<<"  len:"<<len<<std::endl;
      int res = sendto(socketId, buffer, len, 0,(const struct sockaddr *) &serv_addr, sizeof(struct sockaddr_in));

      //In the case of using connect you need to use send, and not sendto
      //int res = send(socketId, buffer, len, 0);
      std::cout<<res<<std::endl;
      
      if (res<0)
	std::cout<<"Send Error"<<std::endl;
      ++num;
      

      sleep(sleeptime);
      
      
    }
}
  
