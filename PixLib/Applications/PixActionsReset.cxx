#include <set>
#include <iostream>

#include "PixBroker/PixBroker.h"
#include "PixBroker/PixExpertBroker.h"
#include "PixUtilities/PixMessages.h"

using namespace PixLib;
#ifdef USE_GCOV
extern "C" {
void __gcov_flush();
}

static void catch_function(int signal) {
   __gcov_flush();
}
#endif

int main(int argc, char **argv) {
#ifdef USE_GCOV
  struct sigaction sa;
  sigemptyset(&sa.sa_mask); 
  sa.sa_flags = 0;
  int ret1=0;
  sa.sa_handler=catch_function;
  ret1 = sigaction(SIGUSR1, &sa, NULL);
#endif
  // Check arguments
  if(argc<2) {
    std::cout << "USAGE: PixActionsReset [partition name]" << std::endl;
    std::cout << argv[0] << std::endl;
    return 1;
  }
  std::string ipcPartitionName = argv[1];

  // Start IPCCore
  IPCCore::init(argc, argv);

  IPCPartition *ipcPartition = new IPCPartition(ipcPartitionName.c_str());
  if(ipcPartition == 0){
    std::cout << "PixActionsReset ERROR: Partition " << ipcPartitionName << " doesn't exist." << std::endl;
    return 0;
  }

  std::cout << "*********************************************************************************" << std::endl;
  std::cout << "DEALLOCATING ALL PIXEL RESOURCES IN PARTITION: " << ipcPartitionName << std::endl;
  std::cout << "*********************************************************************************" << std::endl;
  // Check all Multi Brokers in the partition.
  // deallocate all their actions
  std::cout << "Retrieving all Brokers in the partition" << std::endl;

  PixExpertBroker* expertBroker = new PixExpertBroker(ipcPartition);
  
  expertBroker->deallocateAllMulti();
  expertBroker->deallocateAllInterfaces();
  expertBroker->deallocateAllSingleCrates();
  delete expertBroker;
  return 1;

}
