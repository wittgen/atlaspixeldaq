/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 2016-VI-29
 * Updates: 2016-VIII-19 JuanAn Garcia Adding Recovery of autoDisabled RODs/MODs
 */

#include "PixUtilities/PixISManager.h"
#include "PixUtilities/PixMessages.h"
#include "PixUtilities/PixStoplessRecovery.h"
#include "PixConnectivity/PixConnectivity.h"
#include "PixConnectivity/RodBocConnectivity.h"

#include <pmg/pmg_initSync.h>

#include <mutex>

#include <sys/stat.h>

#include <csignal>

using namespace PixLib;


OWLSemaphore* sem=nullptr;

void signal_handler(int signal) {
  if(sem){
    sem->post();
  }
}

// Todo: use proper std::chrono items
const int32_t maxSecondsBehind = 120; // No too short so we can accomodate longer commands (with exceptions/timeouts)
const int32_t maxSecondsSinceLastNotification = 600; // Re-send notification after 10 minutes
const uint32_t maxSecondsBeforeRodRecovery = 1800; // Do not do the recovery more than once in this time
const int32_t maxRecoveries = 1;//if (numberOfRodRecoveries > maxRecoveries)RecoverROD;
const int32_t nDisModsToRecover = 3;//if (disModsinROD > nDisModsToRecover)RecoverROD;
const uint32_t maxSecondsMultipleRodRecovery = 20; // Do not perform recovery if multiple RODs are recovered within this time

const char* mailRecipients[] = { "41754111753@mail2sms.cern.ch", "karolos@cern.ch", "41754110032@mail2sms.cern.ch", "41754115359@mail2sms.cern.ch" }; // Hard-coded: to get from file/OKS/config DB

std::map<std::string,uint32_t> wdCounter;
std::map<std::string,int32_t> wdTimestamp;
std::map<std::string,int32_t> lastNotificationTimestamp;
std::map<std::string,int32_t> recoveredRods;
std::map<std::string,time_t> recoveredRodsTimeStamp;
std::map<std::string,std::string> allocatedPartitions;
std::map<std::string,bool> autoRecoverFlag;

std::string infrPartName;

int64_t wdTimestampSum;
uint32_t wdTotalSum;

int32_t lastMeanDisplayed;

std::mutex stdoutMutex;


std::string showTime(time_t ti = std::time(NULL)) {
  char buffer[50];
  strftime(buffer, 50, "[ %F %T ]:", std::localtime(&ti));
  return std::string(buffer);
}

void sendMail(std::string rodName, std::string text){

  std::time_t time = std::time(NULL);

  auto it = lastNotificationTimestamp.find(rodName);

  if (it != lastNotificationTimestamp.end()){
    if(time - it->second < maxSecondsSinceLastNotification){
      PIX_INFO("Mail already sent at "<< showTime (it->second) <<", next mail on this ROD will be send after "<< showTime (it->second + maxSecondsBehind));
      return;
    }
  }

  for( auto mR : mailRecipients ) {
    std::string cmd = "echo '"+rodName +"' | mail -s '"+ text + "' " + mR;
    std::cout << cmd << std::endl;
    system(cmd.c_str());
  }

  lastNotificationTimestamp[rodName] = time;

}

void sendRecoverRodMessage(std::string rodName){

  if(rodName == "ROD_I1_S21"){
    PIX_WARNING(rodName<<" belongs to DBM, will not be automatically recovered ");
    return;
  }

  auto it = recoveredRods.find(rodName);
  if (it != recoveredRods.end()){
    PIX_INFO(rodName<<" recovered "<<recoveredRods[rodName]<< " times");

    if(it->second > maxRecoveries){
      PIX_ERROR(rodName<<" already recovered "<<(int)it->second<< " times in this run, it will not be automatically recovered again!!!");
      return;
    }
  }

  time_t time = std::time(NULL);
  auto it1 = recoveredRodsTimeStamp.find(rodName);
  if (it1 != recoveredRodsTimeStamp.end()){
    PIX_INFO(" recovered last Time at "<<showTime(it1->second));

    if((time - it1->second) < maxSecondsBeforeRodRecovery){
      PIX_ERROR(rodName<<" already recovered at "<<showTime(it1->second)<< " it will not be automatically recovered till "<<showTime(it1->second + maxSecondsBeforeRodRecovery));
      return;
    }
  }

  int nRodsRecovered=0;
  for(const auto & it3 : recoveredRodsTimeStamp){
    if((time - it3.second) < maxSecondsMultipleRodRecovery){
      nRodsRecovered++;
      std::cout<<it3.first<<std::endl;
    }

    if(nRodsRecovered>1){
      PIX_ERROR("Two RODs already recovered at "<<showTime(it3.second)<< " too many recoveries at the same time "<<rodName<<" will not be automatically recovered");
      return;
    }
  }

  auto it2 = allocatedPartitions.find(rodName);
  if (it2 == allocatedPartitions.end() ){
    PIX_WARNING(rodName<<" not found in the allocated partitions, the ROD will not be recovered!!!!");
    return;
  }

  auto aR = autoRecoverFlag.find(rodName);
  if(aR !=autoRecoverFlag.end() ){
    if(!aR->second){
      PIX_ERROR("PIXWD_AUTO_RECOVER flag for "<<rodName <<" is not set, the rod will not be automatically recovered!!! You can set-up in the runConfig");
      return;
    }
  } else {
    PIX_ERROR("PIXWD_AUTO_RECOVER flag for "<<rodName <<" is not found, there is something nasty!!!");
    return;
  }

  PIX_WARNING(rodName<<" automatically recovered");

  std::thread t([](std::string rName){
    std::this_thread::sleep_for( std::chrono::seconds(5));
    PixStoplessRecovery::autoRecover(infrPartName, rName);
  },rodName);
  
  t.detach();

  recoveredRods[rodName]++;
  recoveredRodsTimeStamp[rodName]=time;
  std::string mailText = "Automatically recovered "+rodName;
  //sendMail(rodName,mailText);

}

//Clear the recovered RODs map at the start of the Run
void ResetCallback(ISCallbackInfo *isc) {

  static size_t callbackCounter = 0;
  callbackCounter++;

  std::cout << "[" << std::setw(4) << callbackCounter << "] " << showTime() << isc->name()<<std::endl;
  std::lock_guard<std::mutex> lock(stdoutMutex);

  std::cout<<"Stop/Start of Run, reseting counters"<<std::endl;

  std::cout<<"\n"<<"-------------SUMMARY---------------"<<"\n"<<std::endl;
  for (auto it =recoveredRods.begin();it!=recoveredRods.end();it++)
    std::cout<<it->first <<" recovered "<<it->second <<" times; last time at "<<showTime(recoveredRodsTimeStamp.find(it->first)->second)<<std::endl;
  std::cout<<std::endl;

  recoveredRods.clear();
  recoveredRodsTimeStamp.clear();

}


void autoDisRODCallback(ISCallbackInfo *isc) {
  static size_t callbackCounter = 0;
  callbackCounter++;

  ISInfoString autoDisValue;

  if( isc->type() == autoDisValue.type() ) {
    isc->value(autoDisValue);
    std::string autoDisValueStr = autoDisValue.getValue();
    std::lock_guard<std::mutex> lock(stdoutMutex);
    std::cout << "[" << std::setw(4) << callbackCounter << "] " << showTime() << isc->name() << " with value " << autoDisValueStr << std::endl;

    if(autoDisValueStr == "") return;

    std::string disRod;
    std::string reason;

    disRod = autoDisValueStr;
    reason = disRod.substr(disRod.find_first_of("("),disRod.find_first_of(")"));
    reason.erase(0,1);
    reason.erase(reason.size()-1,reason.size());
    disRod.erase(disRod.find_first_of("("),disRod.find_first_of(")")+1);

    PIX_LOG("ROD "<<disRod<<" disabled with reason "<<reason);

    if(reason == "B" || reason == "Z"){//Add more reasons???
      PIX_LOG("ROD recovery for "<< disRod);
      sendRecoverRodMessage(disRod);
    }

  } else {
    std::lock_guard<std::mutex> lock(stdoutMutex);
    std::cout << "Type of " << isc->name() << " is unkown!" << std::endl;
  }
}

void autoDisMODCallback(ISCallbackInfo *isc) {
  static size_t callbackCounter = 0;
  callbackCounter++;

  ISInfoString autoDisValue;

  if( isc->type() == autoDisValue.type() ) {
    isc->value(autoDisValue);
    std::string autoDisValueStr = autoDisValue.getValue();

    std::lock_guard<std::mutex> lock(stdoutMutex);
    std::cout << "[" << std::setw(4) << callbackCounter << "] " << showTime() << isc->name() << " with value " << autoDisValueStr << std::endl;

    if(autoDisValueStr == "") return;

    std::string disMod;
    std::string reason;
    int autoDisMods = 0;

    do{
      disMod = autoDisValueStr.substr(0,autoDisValueStr.find_first_of("/"));
      autoDisValueStr.erase(0,autoDisValueStr.find_first_of("/")+1);
      reason = disMod.substr(disMod.find_first_of("("),disMod.find_first_of(")"));
      reason.erase(0,1);
      reason.erase(reason.size()-1,reason.size());
      disMod.erase(disMod.find_first_of("("),disMod.find_first_of(")")+1);

      PIX_LOG("MOD "<<disMod<<" disabled with reason "<<reason);

      if(reason == "B" || reason == "T"|| reason == "?")//Add more reasons???
        autoDisMods++;

    }while(autoDisValueStr.size( ) > 4 && autoDisValueStr.find("/") !=  std::string::npos);

    if(autoDisMods>nDisModsToRecover){
      std::string rodName = isc->name();
      rodName.erase(0,rodName.find_first_of("/")+1);
      rodName.erase(rodName.find_first_of("/"),rodName.size());
      PIX_WARNING("ROD recovery for "<< rodName <<" with "<<autoDisMods<<" disabled modules");
      sendRecoverRodMessage(rodName);
    }

  } else {
    std::lock_guard<std::mutex> lock(stdoutMutex);
    std::cout << "Type of " << isc->name() << " is unkown!" << std::endl;
  }
}

std::string getRodNameFromIS(std::string isName){

  isName.erase(0,isName.find_first_of("/")+1);
  isName.erase(isName.find_first_of("/"));
  return isName;
}

void allocatedPartitionsCallback(ISCallbackInfo *isc) {
  static size_t callbackCounter = 0;
  callbackCounter++;

  ISInfoString allocationPartition;

  if( isc->type() == allocationPartition.type() ) {
    isc->value(allocationPartition);
    std::string allocationPartitionStr = allocationPartition.getValue();
    allocatedPartitions[getRodNameFromIS(isc->name())] = allocationPartitionStr;
    std::lock_guard<std::mutex> lock(stdoutMutex);
    std::cout << "[" << std::setw(4) << callbackCounter << "] " << showTime() << isc->name() << " with value " << allocationPartitionStr << std::endl;
  } else {
    std::lock_guard<std::mutex> lock(stdoutMutex);
    std::cout << "Type of " << isc->name() << " is unkown!" << std::endl;
  }
}

void autoRecoverCmdCallback(ISCallbackInfo *isc) {

  static size_t callbackCounter = 0;
  callbackCounter++;

  ISInfoString autoRecoverCmd;

  if( isc->type() == autoRecoverCmd.type() ) {
    isc->value(autoRecoverCmd);
    std::string autoRecoverCmdStr = autoRecoverCmd.getValue();

    std::lock_guard<std::mutex> lock(stdoutMutex);
    std::cout << "[" << std::setw(4) << callbackCounter << "] " << showTime() << isc->name() << " with value " << autoRecoverCmdStr << std::endl;
  } else {
    std::lock_guard<std::mutex> lock(stdoutMutex);
    std::cout << "Type of " << isc->name() << " is unkown!" << std::endl;
  }
}

void PixWDautoRecoverFlagCallback(ISCallbackInfo *isc) {

  static size_t callbackCounter = 0;
  callbackCounter++;

  ISInfoInt autoRecoverCmd;

  if( isc->type() == autoRecoverCmd.type() ) {
    isc->value(autoRecoverCmd);
    autoRecoverFlag[getRodNameFromIS(isc->name())] = autoRecoverCmd.getValue();
    std::lock_guard<std::mutex> lock(stdoutMutex);
    std::cout << "[" << std::setw(4) << callbackCounter << "] " << showTime() << isc->name() << " with value " << autoRecoverCmd.getValue() << std::endl;
  } else {
    std::lock_guard<std::mutex> lock(stdoutMutex);
    std::cout << "Type of " << isc->name() << " is unkown!" << std::endl;
  }
}

void getPixWDAutoRecoverFlag(std::string part){

  PixLib::PixISManager *isManager = new PixLib::PixISManager(part, "RunParams");

  std::string str("PIXWD_AUTO_RECOVER");

  std::vector<std::string> autoRecVars = isManager->listVariables(str.c_str());
  for (unsigned int ivr = 0; ivr<autoRecVars.size(); ivr++)
    autoRecoverFlag[getRodNameFromIS(autoRecVars[ivr])] = isManager->read<int>(autoRecVars[ivr].c_str() );

  delete isManager;

}

void getAllocatedRODs(std::string part){
  PixLib::PixISManager *isManager = new PixLib::PixISManager(part, "RunParams");

  std::string str("*ALLOCATING-PARTITION");
  std::vector<std::string> allocated = isManager->listVariables(str.c_str());
  for (unsigned int ivr = 0; ivr<allocated.size(); ivr++)
    allocatedPartitions[getRodNameFromIS(allocated[ivr])] = isManager->read<std::string>(allocated[ivr].c_str()) ;

  delete isManager;

}

void checkCallbackCounters() {
  //for(auto wdC : wdCounter ) {
  //  std::cout << wdC.first << ": " << wdC.second << std::endl;
  //}

  int32_t mean = static_cast<int32_t>( wdTimestampSum / wdTimestamp.size() );
  // Taking the current timestamp as the 'mean'
  mean = std::chrono::duration_cast<std::chrono::seconds>( std::chrono::system_clock::now().time_since_epoch() ).count();

  //std::cout << "The mean is " << mean << std::endl;
  if( mean % 60 == 0 && mean != lastMeanDisplayed ) {
    static size_t callbackCounter = 0;
    callbackCounter++;
    lastMeanDisplayed = mean;

    std::lock_guard<std::mutex> lock(stdoutMutex);
    std::cout << "[" << std::setw(4) << callbackCounter << "] " << showTime() << " mean = " << mean << std::endl;
  }

  int64_t total = 0;
  for(auto wdTS : wdTimestamp ) {
    total += wdTS.second;
    if( wdTS.second < mean - maxSecondsBehind ) { // Don't allow more than maxSecondsBehind seconds beind ...
      std::string rodName = wdTS.first.substr(wdTS.first.find("/ROD_")+1,10);
      if ( rodName[rodName.size()-1] == '/')
        rodName.resize(9);

      if( lastNotificationTimestamp[rodName] <  mean - maxSecondsSinceLastNotification ) {
        std::lock_guard<std::mutex> lock(stdoutMutex);
        std::cout << "ERROR: it seems that one PPC has died!!! It is more than " << maxSecondsBehind << " seconds behind:" << wdTS.first;
        std::cout << "... Value = " << wdTS.second << ", Mean = " << mean << " (Delta=" << wdTS.second - mean << ")" << std::endl;
        std::cout << "Last notification timestamp for " << rodName << " is " << lastNotificationTimestamp[rodName] << std::endl;

        //sendMail(rodName,"PPC_DEAD" + rodName);

        lastNotificationTimestamp[rodName] = mean;
      }
    }
  }

}

void lastWdCallCallback(ISCallbackInfo *isc) {

  //std::cout << __PRETTY_FUNCTION__ << std::endl;
  ISInfoInt lastWdCallValue;

  if( isc->type() == lastWdCallValue.type() ) {
    isc->value(lastWdCallValue);
    //std::cout << isc->name() << ": " << lastWdCallValue.getValue() << std::endl;
    int32_t tsDiff = lastWdCallValue.getValue() - wdTimestamp[isc->name()];
    //std::cout << isc->name() << ": " << tsDiff << std::endl;
    wdTimestamp[isc->name()] = lastWdCallValue.getValue();
    wdTimestampSum += tsDiff;
    wdCounter[isc->name()]++;
    wdTotalSum++;
  } else {
    std::lock_guard<std::mutex> lock(stdoutMutex);
    std::cout << "Type of " << isc->name() << " is unkown!" << std::endl;
  }

  checkCallbackCounters();
}

void autoDisEnaCallback(ISCallbackInfo *isc) {
  static size_t callbackCounter = 0;
  callbackCounter++;

  ISInfoString autoDisEnaValue;

  if( isc->type() == autoDisEnaValue.type() ) {
    isc->value(autoDisEnaValue);
    std::string autoDisEnaValueStr = autoDisEnaValue.getValue();

    std::lock_guard<std::mutex> lock(stdoutMutex);
    std::cout << "[" << std::setw(4) << callbackCounter << "] " << showTime() << isc->name() << " with value " << autoDisEnaValueStr << std::endl;

  } else {
    std::lock_guard<std::mutex> lock(stdoutMutex);
    std::cout << "Type of " << isc->name() << " is unkown!" << std::endl;
  }
}

void help(){
  std::cout << "This program starts the PixelInfr watchdog" << std::endl;
  std::cout << "Usage: PixWD PIXEL_INFR_NAME DAQ_SliceName" << std::endl;
  exit(1);
}


int main(int argc, char** argv) {

  IPCCore::init( argc, argv );
  pmg_initSync();
  
  if(argc != 3)help();

  infrPartName = argv[1];
  if(infrPartName.find("PixelInfr") == std::string::npos){
    std::cout<<"Partition name "<< infrPartName <<" is required to contain PixelInfr "<<std::endl;
    help();
  }

  std::string daqSliceName = argv[2];

  IPCPartition infrPart(infrPartName);
  std::cout << "Listening to " << infrPart.name() << std::endl;

  ISInfoReceiver isInfoReceiver(infrPart);

  getAllocatedRODs(infrPartName);

  std::unique_ptr<PixConnectivity> pixConnectivityPtr = PixStoplessRecovery::getConnectivity(infrPartName);

  if(!pixConnectivityPtr){
    PIX_ERROR("I cannot get connectivity from "<<infrPartName);
    return EXIT_FAILURE;
  }

  for(const auto & rod : pixConnectivityPtr->rods) {
    if (!rod.second)continue;
      std::string isRodBaseStr = "RunParams." +(rod.second)->crate()->name() +"/"+(rod.second)->name();

      std::string isVarName =isRodBaseStr + "/LAST-WD-CALL";
      isInfoReceiver.subscribe( isVarName, lastWdCallCallback );
      isVarName =isRodBaseStr + "/AUTO_DISABLED_ROD";
      isInfoReceiver.subscribe( isVarName, autoDisRODCallback );
      isVarName =isRodBaseStr + "/AUTO_DISABLED_MODULES";
      isInfoReceiver.subscribe( isVarName, autoDisMODCallback );
      isVarName =isRodBaseStr + "/ALLOCATING-PARTITION";
      isInfoReceiver.subscribe( isVarName, allocatedPartitionsCallback );
      isVarName =isRodBaseStr + "/AUTO_RECOVER_COMMAND";
      isInfoReceiver.subscribe( isVarName, autoRecoverCmdCallback );
  }

  ISInfoReceiver isInfoReceiverDT(daqSliceName);
  std::cout << "Setting up callbacks for partition " << daqSliceName << std::endl;
  getPixWDAutoRecoverFlag(daqSliceName);
    try {
        for(const auto & rod : pixConnectivityPtr->rods) {
          if (!rod.second)continue;
          std::string isRodBaseStr = "RunParams." +(rod.second)->crate()->name() +"/"+(rod.second)->name();
          std::string isVarName =isRodBaseStr + "/AUTO_DISABLE_ENA";
          isInfoReceiverDT.subscribe(isVarName, autoDisEnaCallback);
          isVarName =isRodBaseStr +"/PIXWD_AUTO_RECOVER";
          isInfoReceiverDT.subscribe(isVarName, PixWDautoRecoverFlagCallback);
        }

      isInfoReceiverDT.subscribe( "RunParams.RunParams", ResetCallback );
    } catch( daq::is::RepositoryNotFound &e ) {
      PIX_ERROR( "Exception: " << e.what());
    }

  // Install the signal handler
  std::signal(SIGINT   , signal_handler);
  std::signal(SIGQUIT  , signal_handler);
  std::signal(SIGILL   , signal_handler);
  std::signal(SIGTRAP  , signal_handler);
  std::signal(SIGABRT  , signal_handler);
  std::signal(SIGIOT   , signal_handler);
  std::signal(SIGBUS   , signal_handler);
  std::signal(SIGFPE   , signal_handler);
  std::signal(SIGKILL  , signal_handler);
  std::signal(SIGSEGV  , signal_handler);
  std::signal(SIGPIPE  , signal_handler);
  std::signal(SIGTERM  , signal_handler);
  std::signal(SIGSTKFLT, signal_handler);
  std::signal(SIGSYS   , signal_handler);

  sem = new OWLSemaphore();
  sem->wait();
  delete sem;

  return EXIT_SUCCESS;
}

