/*
 * Filename: PixFitServer.cxx
 * Created: September 2013
 * Author: marx
 *
 * Purpose: Executable for IBL PixFitServer 
 * (based on ATLAS TDAQ root_provider)
 */

#include <iostream>
#include <csignal>

#include <cmdl/cmdargs.h>
#include <ipc/core.h>
#include <pmg/pmg_initSync.h>
#include <is/infoT.h>
#include <oh/exceptions.h>

#include "PixFitServer/PixFitManager.h"

#include "TROOT.h"
#include "TH1.h"

using namespace std;
using namespace PixLib;


void signal_handler(int signal) {
  if(!PixFitManager::stop_all)PixFitManager::stop();
}

int main( int argc, char ** argv )
{
    IPCCore::init( argc, argv );
    // Get parameters from the command line
    CmdArgStr partition_name( 'p', "partition", "partition_name",
                              "Partition name", CmdArg::isREQ );
    CmdArgStr server_name( 's', "server", "server_name",
                           "OH (IS) Server name", CmdArg::isREQ );
    CmdArgStr instance_name( 'i', "instance", "instance_name",
                           "fitserver instance name", CmdArg::isREQ );
    CmdArgStr interface_name( 'c', "interface", "interface_name",
                           "fitserver eth. interface name" );
    CmdArgBool     slaveEmu('e', "slaveEmu", "use slave emulator software as client");
    CmdArgBool     update('u', "update", "publish histograms with update mod");
    CmdArgBool     wait('w', "wait", "wait for commands");
    
    CmdArgStr emu_file( 'f', "file", "emu_file",
                        "Slave Emulator config file");

    CmdArgStr dbsrv_name( 'd', "dbsrv", "dbsrv_name", "DBserver name" );
    CmdArgStr idTag( 't', "idTag", "idTag", "ID tag for connectivity loading" );
    CmdArgStr connTag( 'o', "connTag", "connTag", "Connectivity tag for connectivity loading" );
    CmdArgStr cfgTag( 'g', "cfgTag", "cfgTag", "Config. tag for connectivity loading" );
    CmdArgStr cfgMTag( 'm', "cfgMTag", "cfgMTag", "Module config. tag for connectivity loading" );

    CmdLine cmd( *argv, &partition_name, &server_name, &instance_name,
		 &update, &wait, &slaveEmu, &emu_file, &interface_name, 
		 &dbsrv_name, &idTag, &connTag, &cfgTag, &cfgMTag, NULL );
    
    CmdArgvIter arg_iter( --argc, ++argv );
    
    cmd.description( "Application for PixFitServer" );
    
    slaveEmu = false;
    wait = false;
    emu_file = "slaveEmu.cfg";
    dbsrv_name = "PixelDbServer";
    interface_name = "eth0";
    idTag = "Base";
    connTag = "Base";
    cfgTag = "Base";
    cfgMTag = "Base";
    
    cmd.parse(arg_iter);
    
    IPCPartition partition( partition_name );
    //MyOwnCommandListener lst;

   // Install the signal handler
    std::signal(SIGINT   , signal_handler);
    std::signal(SIGQUIT  , signal_handler);
    std::signal(SIGILL   , signal_handler);
    std::signal(SIGTRAP  , signal_handler);
    std::signal(SIGABRT  , signal_handler);
    std::signal(SIGIOT   , signal_handler);
    std::signal(SIGBUS   , signal_handler);
    std::signal(SIGFPE   , signal_handler);
    std::signal(SIGKILL  , signal_handler);
    std::signal(SIGSEGV  , signal_handler);
    std::signal(SIGPIPE  , signal_handler);
    std::signal(SIGTERM  , signal_handler);
    std::signal(SIGSTKFLT, signal_handler);
    std::signal(SIGSYS   , signal_handler);
    
    // signal init to PMG
    pmg_initSync();
    
    ROOT::EnableThreadSafety();
    TH1::AddDirectory(kFALSE);

    try {
      PixFitManager *manager = new PixFitManager ((const char*)server_name, (const char*)partition_name, (const char*)dbsrv_name,
                              (const char*)instance_name, slaveEmu, (const char*)emu_file, (const char*) interface_name,
			      (const char*)idTag, (const char*)connTag, (const char*)cfgTag, (const char*)cfgMTag);
      manager->run();
    } catch( daq::oh::Exception & ex ){
      ers::fatal( ex );
    } catch(...){
      PIX_ERROR("Unknown exception was thrown");
    }
    
   PIX_LOG("Exiting FitServer appl...");

   
   return EXIT_SUCCESS;
}
