/*
 * Authors: 	A. Miucci <antonio.miucci@cern.ch>
 * 		K. Potamianos <karolos.potamiamos@cern.ch>
 * Description: this application poll over the slots of an IBL ROD to extract its internal readout buffers
 */

#include "VmeUartManager.hh"
#include "VmeUtils.hh"

#include <pmg/pmg_initSync.h>

#include <csignal>


void signal_handler(int signal) {
  VmeUtils::VmeUartControl(0, false, true);//Send exit signal via shared memory
}

void help(){
  std::cout << "This program starts the VMEUart on SBCs" << std::endl;
  std::cout << "Usage: VmeUartReader PARTITION_NAME (optional)" << std::endl;
  exit(0);
}

int main(int argc, char** argv){

  VmeUartManager *uMgr=nullptr;

  if(argc== 1){
    uMgr = new VmeUartManager( );
  } else if (argc== 2) {
    std::string partitionName = argv[1];
     if(partitionName.find("PixelInfr") == std::string::npos){
       std::cout<<"Partition name "<< partitionName <<" is required to contain PixelInfr "<<std::endl;
       help();
     }
    IPCCore::init(argc, argv);
    pmg_initSync();
    uMgr = new VmeUartManager(partitionName);
  } else {
    help();
  }

  // Install the signal handler
  std::signal(SIGINT   , signal_handler);
  std::signal(SIGQUIT  , signal_handler);
  std::signal(SIGILL   , signal_handler);
  std::signal(SIGTRAP  , signal_handler);
  std::signal(SIGABRT  , signal_handler);
  std::signal(SIGIOT   , signal_handler);
  std::signal(SIGBUS   , signal_handler);
  std::signal(SIGFPE   , signal_handler);
  std::signal(SIGKILL  , signal_handler);
  std::signal(SIGSEGV  , signal_handler);
  std::signal(SIGPIPE  , signal_handler);
  std::signal(SIGTERM  , signal_handler);
  std::signal(SIGSTKFLT, signal_handler);
  std::signal(SIGSYS   , signal_handler);

  if(uMgr)uMgr->run();

  return EXIT_SUCCESS;
}

