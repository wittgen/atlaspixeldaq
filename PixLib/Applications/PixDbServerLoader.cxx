#include <iostream>
#include <signal.h>
#include <csignal>
#include <ipc/object.h>
#include <pmg/pmg_initSync.h>

#include <sys/wait.h>

#include "PixConnectivity/PartitionConnectivity.h"
#include "PixConnectivity/Pp0Connectivity.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "PixConnectivity/PixConnectivity.h"
#include "PixConnectivity/PixConfigDb.h"
#include "PixConnectivity/PixDisable.h"
#include "PixTrigController/TimPixTrigController.h"
#include "PixModule/PixModule.h"
#include "PixBoc/PixBoc.h"
#include "PixUtilities/PixMessages.h"

#include "RCCVmeInterface.h"

using namespace PixLib;

IPCPartition *ipcPartition;
PixDbServerInterface *DbServer;
PixConnectivity *conn;
PixConfigDb *conf;

std::string ipcPartitionName;
std::string serverName;
std::string   idTag; 
std::string connTag;
std::string  cfgTag;
std::string cfgMTag;
std::string cfgMTag2;
std::string domainName;
std::string firstStep;
std::string applName;

unsigned int errcount=-1;
unsigned int forkcount=-1;

std::atomic_bool stop = false;

#define maxprocs 8
#define maxprocobj 250

void signal_handler(int signal) {
  stop = true;
}

void mem(int del) {
  pid_t pid = getpid();
  std::ostringstream os;
  os << "/proc/" << pid << "/status";
  std::cout << "Mem Info (" << os.str() << "):" << std::endl;
  std::ifstream st(os.str().c_str());
  while (!st.eof()) {
    std::string line;
    std::getline(st, line);
    if (line.substr(0,2) == "Vm") {
      std::cout << "  " << line << std::endl;
    }
  }
  sleep(del);
}

std::string getTime(int i) {
  char t_s[100];
  time_t t;
  tm t_m;
  t = i;
  localtime_r(&t, &t_m);
  strftime(t_s,100,"%Y-%m-%d %H:%M:%S",&t_m);
  std::string date = t_s;
  return date;
}

std::string getTime() {
  char t_s[100];
  time_t t;
  tm t_m;
  t = time(NULL);
  localtime_r(&t, &t_m);
  strftime(t_s,100,"%Y-%m-%d-%H%M%S",&t_m);
  std::string date = t_s;
  return date;
}

std::string bufFileName = "/tmp/loadBuf-" + getTime() + "-";
std::string bufSuffix[maxprocs] = { "0", "1", "2", "3", "4", "5", "6", "7" };  

std::string element(std::string str, int num, char sep) {
  unsigned int ip;
  int start = -1,end = -1;
  int nel = -1;
  bool spc = true;
  bool quote = false;                                                                                                                                        
  for (ip=0;ip<str.size();ip++) {
    if (str[ip] == '"') quote = !quote;
    if (str[ip] == sep || (sep == 0 && (str[ip] == 9 || str[ip]==10 || str[ip] == 32))) {
      if (!spc && !quote) {
        spc = true;
        if (nel==num) end = ip-1;
      }
    } else {
      if (spc) {
        spc = false;
        nel++;
        if (nel==num) start = ip;
      }
    }
  }
  if (!spc && nel==num) end = str.size()-1;                                                                                                                   
  if (start >= 0) {
    std::string c;
    std::string s = str.substr(start,end-start+1);
    for (ip=0;ip<s.size();ip++) {
      if (s[ip] != '"') c += s[ip];
    }
    return c;
  } else {
    return "";
  }
}

void connectToIpc(IPCPartition* &ipcPartitionArg, std::string ipcPartitionNameArg, PixDbServerInterface* &DbServerArg, std::string serverNameArg) {
  //  Create IPCPartition constructors
  //std::cout << std::endl << "Starting partition " << ipcPartitionName << std::endl;
  try {
    ipcPartitionArg = new IPCPartition(ipcPartitionNameArg);
  } catch(...) {
    std::cout << "PixDbServer: ERROR!! problems while connecting to the partition!" << std::endl;
  }
  bool ready=false;
  int nTry=0;
  do {
    sleep(1);
    DbServerArg=new  PixDbServerInterface(ipcPartitionArg,serverNameArg,PixDbServerInterface::CLIENT, ready);
    if(!ready) delete DbServerArg;
    else break;
    //std::cout << " ..... attempt number: " << nTry << std::endl;
    nTry++;
  } while(nTry<10);  
  if(!ready) {
    std::cout << "Impossible to connect to DbServer " << serverNameArg << std::endl;
    exit(-1);
  }
}

bool loadDisable(std::string id, PixDbDomain::obj_id oid) {
  if (oid.type == "PixDisable" || oid.type == "DISABLE_CFG") {
    PixDisable dis(oid.name);
    try {
      unsigned int rev = 0;
      conf->readCfg(&(dis.config()), oid.name, rev, oid.tag, oid.domain);
    }
    catch (PixException &e) {
      std::string msg = "PixDisable with name "+oid.name+" has not been loaded from DB"+e.what();
      ers::error(PixLib::pix::daq (ERS_HERE,applName,msg));
      return false;
    }
    catch (...) {
      ers::error(PixLib::pix::daq (ERS_HERE,applName,"PixDisable with name "+oid.name+" has not been loaded from DB"));
      return false;
    }
    try {
      conf->writeCfg(&(dis.config()), oid.name, oid.rev, oid.tag, oid.domain, "OnDB");        
    }
    catch (PixException &e) {
      std::string msg = "Exception caught while writing PixDisable "+oid.name+" into DbServer - "+e.what();
      ers::error(PixLib::pix::daq (ERS_HERE,applName,msg));
      return false;
    }
    catch (...) {
      ers::fatal(PixLib::pix::daq (ERS_HERE,applName,"Unknown exception caught while writing PixDisable "+oid.name+" into DbServer"));
      return false;
    }
  }
  ers::info(PixLib::pix::daq (ERS_HERE,applName,"PixDisable with name "+oid.name+" has been loaded to DbServer (tag "+oid.tag+")"));
  return true;
}

bool loadRunConf(std::string id, PixDbDomain::obj_id oid) {
  if (oid.type == "PixRunConfig" || oid.type == "RUNCONF_CFG") {
    PixRunConfig runc(oid.name);
    try {
      unsigned int rev = 0;
      conf->readCfg(&(runc.config()), oid.name, rev, oid.tag, oid.domain);
    }
    catch (PixException &e) {
      std::string msg = "PixRunConfig with name "+oid.name+" has not been loaded from DB"+e.what();
      ers::error(PixLib::pix::daq (ERS_HERE,applName,msg));
      return false;
    }
    catch (...) {
      ers::error(PixLib::pix::daq (ERS_HERE,applName,"PixRunConfig with name "+oid.name+" has not been loaded from DB"));
      return false;
    }
    try {
      conf->writeCfg(&(runc.config()), oid.name, oid.rev, oid.tag, oid.domain, "OnDB");        
    }
    catch (PixException &e) {
      std::string msg = "Exception caught while writing PixRunConfig "+oid.name+" into DbServer - "+e.what();
      ers::error(PixLib::pix::daq (ERS_HERE,applName,msg));
      return false;
    }
    catch (...) {
      ers::error(PixLib::pix::daq (ERS_HERE,applName,"Unknown exception caught while writing PixRunConfig "+oid.name+" into DbServer"));
      return false;
    }
  }
  ers::info(PixLib::pix::daq (ERS_HERE,applName,"PixRunConfig with name "+oid.name+" has been loaded to DbServer (tag "+oid.tag+")"));
  return true;
}

bool loadTim(std::string id, PixDbDomain::obj_id oid) {
  if (oid.type == "TimPixTrigController" || oid.type == "TIM_CFG") {
    TimPixTrigController tim(oid.name);
    try {
      unsigned int rev = 0;
      conf->readCfg(tim.config(), oid.name, rev, oid.tag, oid.domain);
    }
    catch (PixException &e) {
      std::string msg = "TimPixTrigController with name "+oid.name+" has not been loaded from DB"+e.what();
      ers::error(PixLib::pix::daq (ERS_HERE,applName,msg));
      return false;
    }
    catch (...) {
      ers::error(PixLib::pix::daq (ERS_HERE,applName,"TimPixTrigController with name "+oid.name+" has not been loaded from DB"));
      return false;
    }
    try {
      conf->writeCfg(tim.config(), oid.name, oid.rev, oid.tag, oid.domain, "OnDB");        
    }
    catch (PixException &e) {
      std::string msg = "Exception caught while writing TimPixTrigController "+oid.name+" into DbServer - "+e.what();
      ers::error(PixLib::pix::daq (ERS_HERE,applName,msg));
      return false;
    }
    catch (...) {
      ers::error(PixLib::pix::daq (ERS_HERE,applName,"Unknown exception caught while writing TimPixTrigController "+oid.name+" into DbServer"));
      return false;
    }
  }
  ers::info(PixLib::pix::daq (ERS_HERE,applName,"TimPixTrigController with name "+oid.name+" has been loaded to DbServer (tag "+oid.tag+")"));
  return true;
}

bool loadModGroup(std::string id, PixDbDomain::obj_id oid) {
  if (oid.type == "PixModuleGroup" || oid.type == "RODBOC_CFG") {
    PixModuleGroup modg(oid.name, UNKNOWN, false);
    try {
      unsigned int rev = 0;
      conf->readCfg(&(modg.config()), oid.name, rev, oid.tag, oid.domain);
    }
    catch (PixException &e) {
      std::string msg = "PixModuleGroup with name "+oid.name+" has not been loaded from DB"+e.what();
      ers::error(PixLib::pix::daq (ERS_HERE,applName,msg));
      return false;
    }
    catch (...) {
      ers::error(PixLib::pix::daq (ERS_HERE,applName,"PixModuleGroup with name "+oid.name+" has not been loaded from DB"));
      return false;
    }
    try {
      conf->writeCfg(&(modg.config()), oid.name, oid.rev, oid.tag, oid.domain, "OnDB");        
      bool res=false;
      if (modg.getPixController() != NULL) {
        Config *cfg = &(modg.getPixController()->config());
	res=cfg->write(DbServer, "Configuration-"+oid.domain, oid.tag, oid.name+"_ROD", cfg->type(), "OnDB", oid.rev);
      }
      if (modg.getPixBoc() != NULL && res) {
        Config *cfg = modg.getPixBoc()->getConfig();
	res=cfg->write(DbServer, "Configuration-"+oid.domain, oid.tag, oid.name+"_BOC", cfg->type(), "OnDB", oid.rev);
      }
      if (!res) {
	std::string msg = "Problem while writing PixModuleGroup BOC or ROD "+oid.name+" into DbServer";
	ers::error(PixLib::pix::daq (ERS_HERE,applName,msg));
	return false;
      }
    }
    catch (PixException &e) {
      std::string msg = "Exception caught while writing PixModuleGroup "+oid.name+" into DbServer - "+e.what();
      ers::error(PixLib::pix::daq (ERS_HERE,applName,msg));
      return false;
    }
    catch (...) {
      ers::error(PixLib::pix::daq (ERS_HERE,applName,"Unknown exception caught while writing PixModuleGroup "+oid.name+" into DbServer"));
      return false;
    }
  }
  ers::info(PixLib::pix::daq (ERS_HERE,applName,"PixModuleGroup with name "+oid.name+" has been loaded to DbServer (tag "+oid.tag+")"));
  return true;
}

bool loadModule(std::string id, PixDbDomain::obj_id oid) {
  if (oid.type == "PixModule" || oid.type == "MODULE_CFG") {
    // get module flavour from connectivity to hand it over to PixModule
    EnumMCCflavour::MCCflavour mccFlv = EnumMCCflavour::PM_NO_MCC;
    EnumFEflavour::FEflavour feFlv   = EnumFEflavour::PM_NO_FE;
    int nFe=0;
    std::string modName = oid.name;
    std::string connName = "";
    for (auto mod : conn->mods) {
      if (mod.second->prodId() == modName) {
	connName = mod.first;
	mod.second->getModPars(mccFlv, feFlv, nFe);
      }
    }
    if (connName == "") {
      if (conn->mods.find(modName) != conn->mods.end()) {
        connName = modName;
        modName = conn->mods[connName]->prodId();
	conn->mods[connName]->getModPars(mccFlv, feFlv, nFe);
      }
    }
    if (connName != "") {
      PixModule mod(oid.domain, oid.tag, modName, mccFlv, feFlv, nFe);
      try {
	unsigned int rev = 0;
	conf->readCfg(&(mod.config()), modName, rev, oid.tag, oid.domain);
      }
      catch (PixException &e) {
	std::string msg = "PixModule with name "+modName+" has not been loaded from DB"+e.what();
	ers::error(PixLib::pix::daq (ERS_HERE,applName,msg));
	return false;
      }
      catch (...) {
	ers::error(PixLib::pix::daq (ERS_HERE,applName,"PixModule with name "+modName+" has not been loaded from DB"));
	return false;
      }
      try {
	conf->writeCfg(&(mod.config()), modName, oid.rev, oid.tag, oid.domain, "OnDB");        
      }
      catch (PixException &e) {
	std::string msg = "Exception caught while writing PixModule "+modName+" into DbServer - "+e.what();
	ers::error(PixLib::pix::daq (ERS_HERE,applName,msg));
	return false;
      }
      catch (...) {
	ers::error(PixLib::pix::daq (ERS_HERE,applName,"Unknown exception caught while writing PixModule "+modName+" into DbServer"));
	return false;
      }
      ers::info(PixLib::pix::daq (ERS_HERE,applName,"PixModule with name "+modName+" has been loaded to DbServer (tag "+oid.tag+")"));
      return true;
    } else {
      ers::error(PixLib::pix::daq (ERS_HERE,applName, "Module "+modName+" not found in connectivity"));
      return false;
    }
  }
  return false;
}

unsigned int loadListLen(std::vector<std::string> &list) {
  pid_t tmppid=fork();
  if (tmppid==0) {
    char **vnl = NULL;
    int cnl=0;
    IPCCore::init(cnl,vnl);
    connectToIpc(ipcPartition, ipcPartitionName, DbServer, serverName);   
    //unsigned int llen=DbServer->getLoadListLen();
    DbServer->listLoadList(list);
    std::string fnam = bufFileName+"llen";
    std::ofstream *bf = new std::ofstream(fnam.c_str());
    *bf << list.size() << std::endl;
    for (std::string obj : list) {
      *bf << obj << std::endl;
    }
    delete bf;
    kill(getpid(), SIGTERM);
  }
  int status;
  waitpid(tmppid, &status,0);
  unsigned int llen;
  std::string fnam = bufFileName+"llen";
  std::ifstream *bf = new std::ifstream(fnam.c_str());
  *bf >> llen;
  std::string obj;
  std::getline(*bf, obj);
  for (unsigned int i=0; i<llen; i++) {
    std::getline(*bf, obj);
    list.push_back(obj);
  }
  delete bf;
  return llen;
}

void dbsReady() {
  pid_t tmppid=fork();
  if (tmppid==0) {
    char **vnl = NULL;
    int cnl=0;
    IPCCore::init(cnl,vnl);
    connectToIpc(ipcPartition, ipcPartitionName, DbServer, serverName);   
    DbServer->ready();
    kill(getpid(), SIGTERM);
  }
  int status;
  waitpid(tmppid, &status,0);
}

bool confPreLoad() {
  std::string msg;
  try {
    conf = new PixConfigDb();  
    ers::info(PixLib::pix::daq (ERS_HERE,applName,"Start loding configuration tables"));
    conf->setCfgTag(cfgTag, idTag);
    ers::info(PixLib::pix::daq (ERS_HERE,applName, "Table loaded : "+cfgTag+"/"+idTag));
    conf->setCfgTag(cfgMTag, idTag);
    ers::info(PixLib::pix::daq (ERS_HERE,applName,"Table loaded : "+cfgMTag+"/"+idTag));
    if (cfgMTag2 != "") {
      conf->setCfgTag(cfgMTag2, idTag);
      ers::info(PixLib::pix::daq (ERS_HERE,applName, "Table loaded : "+cfgMTag2+"/"+idTag));
    }
    return true;
  }
  catch(PixConfigDbExc &e) {
    msg = "PixConfigDb Exception caught while loading configuration tables : " +std::string( e.what());
     ers::fatal(PixLib::pix::daq (ERS_HERE,applName,msg));
    return false;
  }
  catch (...) {
    msg =  "Unknown exception caught while loading configuration tables";
    ers::fatal(PixLib::pix::daq (ERS_HERE,applName,msg));
    return false;
  }
}

void forkProc(int argc, char** argv, unsigned int forkNum, unsigned int forkId) {
  IPCCore::init(argc, argv);
  connectToIpc(ipcPartition, ipcPartitionName, DbServer, serverName);   
  std::stringstream info;
  info << "PixDbServerLoader_"<<forkId;
  applName=info.str();
  int nrecs = 0;
  std::string fnam = bufFileName+bufSuffix[forkNum];
  std::ofstream *bf = new std::ofstream(fnam.c_str());
  try {
    conn = new PixConnectivity(DbServer, connTag, connTag, connTag, cfgTag, idTag);
    conn->loadConn();
    conf->openDbServer(DbServer);
    conf->cfgFromDb();
    conf->cfgToDbServ();
    ers::info(PixLib::pix::daq (ERS_HERE,applName,"DbServer is ready; connectivity loaded"));
  }
  catch(PixConnectivityExc &e) {
     ers::fatal(PixLib::pix::daq (ERS_HERE,applName,"Exception caught creating PixConnectivity out: "+e.getDescr())); 
    kill(getpid(), SIGTERM);
  }
  catch(...) { 
    ers::fatal(PixLib::pix::daq (ERS_HERE,applName,"Unknown exception caught creating PixConnectivity out")); 
    kill(getpid(), SIGTERM);
  }
  std::vector< std::string > vid;
  try {
    unsigned int lcount=0;
    while (lcount <  maxprocobj) {
      PixDbDomain::obj_id oid;
      std::string id;
      DbServer->getLoadList(id, oid);
      if (id != "") {
	ers::info(PixLib::pix::daq (ERS_HERE,applName, "Processing obj id "+id));      
	std::string tid = oid.domain + "/" + oid.tag;
	if (oid.domain.substr(0,14) == "Configuration-") oid.domain = oid.domain.substr(14,oid.domain.size()-14); 
	bool haveCfg = false;
        try {
	  if (conf->checkCfgTag(oid.tag, oid.domain)) {
	    haveCfg = true;
	  } else {
	    std::cout << "checkCfgTag returns false for "+oid.tag+" "+oid.domain << std::endl;
          }
	}
    catch(PixConnectivityExc &e) {
	  ers::fatal(PixLib::pix::daq (ERS_HERE,applName,"Exception caught creating PixConnectivity in ("+tid+"): "+e.getDescr())); 
	  haveCfg = false;
	}
	catch(...) { 
	  ers::fatal(PixLib::pix::daq (ERS_HERE,applName,"Unknown exception caught creating PixConnectivity in ("+tid+")")); 
	  haveCfg = false;
	}
	bool done = false;
	ers::info(PixLib::pix::daq (ERS_HERE,applName,"Processing object "+oid.name+"/"+oid.type)); 
	if (haveCfg) {
	  if (oid.type == "PixDisable" || oid.type == "DISABLE_CFG") {
	    done = loadDisable(id, oid);
	  } else if (oid.type == "PixRunConfig" || oid.type == "RUNCONF_CFG") {
	    done = loadRunConf(id, oid);
	  } else if (oid.type == "TimPixTrigController" || oid.type == "TIM_CFG") {
	    done = loadTim(id, oid);
	  } else if (oid.type == "PixModuleGroup" || oid.type == "RODBOC_CFG") {
	    done = loadModGroup(id, oid);
	  } else if (oid.type == "PixModule" || oid.type == "MODULE_CFG") {
	    done = loadModule(id, oid);
	  }
	}
	if (!done) {
	  vid.push_back(id);
	} else {
	  DbServer->cleanLoadList(id);
	  nrecs++;
	}
      }
      lcount++;
    }
  }
  catch (...) {
    ers::fatal(PixLib::pix::daq (ERS_HERE,applName,"Unknown exception caught")); 
  }
  for (unsigned int il=0; il<vid.size(); il++) {
    DbServer->restoreLoadList(vid[il]);	
  }
  *bf << nrecs << std::endl << vid.size() << std::endl;
  delete bf;
  //mem(0);  
  kill(getpid(), SIGTERM);
}

int main(int argc, char** argv) {

  if (argc == 8)  {
    cfgMTag2=argv[7];
    cfgMTag=argv[6];
  } else if (argc == 7)  {
    cfgMTag2="";
    cfgMTag=argv[6];
  } else if (argc==6) {
    cfgMTag2="";
    cfgMTag=cfgTag;
  } else {
    std::cout << std::endl << " USAGE: ./PixDbServerLoader [partitionName] [serverName] [id_tag] [tag_c] [tag_cf] [tag_cf_mod] [tag_cf_mod2]"<<std::endl;
    exit(-1);
  }

  ipcPartitionName = argv[1];
  serverName = argv[2];
  idTag = argv[3]; 
  connTag = argv[4];
  cfgTag = argv[5];
  domainName="Configuration-"+idTag;

  if (!confPreLoad()) {
    std::string msg =  "Unable to load configuration tag "+idTag+"/"+cfgTag+"-"+cfgMTag;
    if (cfgMTag2 != "") msg = msg+"-"+cfgMTag2;
    ers::fatal(PixLib::pix::daq (ERS_HERE,applName,msg));
    return -1;
  }

  dbsReady();
  pmg_initSync();

  pid_t pids[maxprocs];
  int sleepTime=5;
  unsigned int nProc=0;
  unsigned int forkCount = 0;
  int totLoad=0, totFail=0, totCycles=0;

  for (unsigned int ip = 0; ip<maxprocs; ip++) pids[ip] = 0;
  
  // Install the signal handler
  std::signal(SIGINT   , signal_handler);
  std::signal(SIGQUIT  , signal_handler);
  std::signal(SIGILL   , signal_handler);
  std::signal(SIGTRAP  , signal_handler);
  std::signal(SIGABRT  , signal_handler);
  std::signal(SIGIOT   , signal_handler);
  std::signal(SIGBUS   , signal_handler);
  std::signal(SIGFPE   , signal_handler);
  std::signal(SIGKILL  , signal_handler);
  std::signal(SIGSEGV  , signal_handler);
  std::signal(SIGPIPE  , signal_handler);
  std::signal(SIGSTKFLT, signal_handler);
  std::signal(SIGSYS   , signal_handler);

  while(!stop) {
    totCycles++;
    std::vector<std::string> list;
    unsigned int llen=loadListLen(list);
    ers::info(PixLib::pix::daq (ERS_HERE,applName, "DbServer reports "+ std::to_string(llen) +" records to load"));
    if (llen == 0 && nProc == 0 && (totLoad > 0 || totCycles > 100)) {
      sleepTime=15;
    } else {
      int nObjReady = 0;
      if (llen > 0) {
	for (std::string obj : list) {
	  if (obj.substr(obj.size()-7,7) != " (PROC)") {
	    std::string id = element(obj, 0, '/');
	    std::string tag = element(obj, 1, '/');
            if (id.substr(0,14) == "Configuration-") id = id.substr(14,id.size()-14);
            try {
	      conf->openCfg(tag,id);
	      nObjReady++;
	    }
        catch(PixConfigDbExc &e) {
	      std::string msg = "PixConfigDb Exception caught while loading configuration tables : " +std::string(e.what());
	      ers::fatal(PixLib::pix::daq (ERS_HERE,applName,msg));
	      return -1;
	    }
	    catch (...) {
	      std::string msg =  "Unknown exception caught while loading configuration tables";
	      ers::fatal(PixLib::pix::daq (ERS_HERE,applName,msg));
	      return -1;
	    }
	  }
	}
      }
      if (nProc > 0) {
	pid_t pid = 0;
	do {
	  int status;
	  pid = waitpid(-1, &status, WNOHANG);
	  if (pid != 0) {
	    for (unsigned int proc=0; proc<maxprocs; proc++) {
	      if (pid == pids[proc]) {
		int nrecs, nfail;
		std::string fnam = bufFileName+bufSuffix[proc];
		std::ifstream *bf = new std::ifstream(fnam.c_str());
		*bf >> nrecs >> nfail;
		totLoad += nrecs;
		totFail += nfail;
		delete bf;
		std::ostringstream m3;
		m3 << "Child " << pids[proc] << " (" << proc << ") terminated (nrecs = " << nrecs << " nfail = " << nfail << ")";
		ers::info(PixLib::pix::daq (ERS_HERE,applName,m3.str()));     
		nProc--;
		pids[proc] = 0;
	      }
	    }
	  }
	} while (pid != 0 && nProc != 0);
      }
      nObjReady = nObjReady - 50*nProc;
      if (nObjReady > 0 && nProc < maxprocs) {
        do {
	  unsigned int ptProc=-1;
	  for (unsigned int ip=0; ip<maxprocs; ip++) {
	    if (pids[ip] == 0) {
	      ptProc = ip;
	      break;
	    }
	  }
	  nProc++;
	  forkCount++;
          nObjReady = nObjReady - 50;
	  pid_t tmppid=fork();
	  pids[ptProc]=tmppid;
	  if (tmppid==0) {
	    forkProc(argc, argv, ptProc, forkCount);
	  }
	  std::stringstream mx;
	  mx << "Starting process " << tmppid << "(" << ptProc << " - " << forkCount <<")";
	  ers::info(PixLib::pix::daq (ERS_HERE,applName,mx.str()));
	} while (nObjReady > 0 && nProc < maxprocs);
      }
      sleepTime = 5;
    }
    sleep(sleepTime);
  }

return EXIT_SUCCESS;

}
