#include <string>
#include <iostream>
#include <signal.h>
#include <vector>
#include <csignal>

#include "PixUtilities/PixMessages.h"

#include <ipc/partition.h>
#include <ipc/object.h>

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1.h>

void execution(TList* list, TDirectory* targetDir, TDirectory* sourceDir);

bool mvRootFile(std::string FileTargetName);
#ifdef USE_GCOV
extern "C" {
void __gcov_flush();
}

static void catch_function(int signal) {
   __gcov_flush();
}
#endif

int main(int argc, char **argv) 
{
#ifdef USE_GCOV
  struct sigaction sa;
  sigemptyset(&sa.sa_mask); 
  sa.sa_flags = 0;
  int ret1=0;
  sa.sa_handler=catch_function;
  ret1 = sigaction(SIGUSR1, &sa, NULL);
#endif 
  if(argc<3) {
    //USAGE: PixMerge [partition name] [target file name] [source file name0; source file name1; ...]
    return 1;
  }
  std::string ipcPartitionName = argv[1];
  std::string FileTargetName = argv[2];
  std::string FilesSourceName = argv[3];

  std::cout << FileTargetName << " and " <<  FilesSourceName << std::endl;

  TFile *FileTarget = TFile::Open( FileTargetName.c_str(), "RECREATE" );
  TDirectory* targetDir = gDirectory;

  std::vector<std::string> allSources;
  while (FilesSourceName.find(";") != std::string::npos) {
    allSources.push_back(FilesSourceName.substr(0,FilesSourceName.find(";")));
    FilesSourceName = FilesSourceName.substr(FilesSourceName.find(";")+1, FilesSourceName.size());
  }
  for(size_t i=0; i<allSources.size(); i++) {
    TFile *FileSource = TFile::Open(allSources[i].c_str()); 
    TDirectory* sourceDir = gDirectory;
    FileSource->ReadAll();
    TList* list = FileSource->GetList();
    execution(list, targetDir, sourceDir);
    std::cout << "SourceFile" <<i<< ": "<<allSources[i]<<std::endl;
    FileSource->Close();
  }
  FileTarget->Close();

  IPCCore::init(argc, argv);

  std::string mess = "The root file "+FileTargetName+" has been merged ";
  ers::info(PixLib::pix::daq (ERS_HERE, "saveRootFile", mess));

  std::cout << " ************** " << std::endl;
  std::cout << " FINISHED " << std::endl;
  std::cout << " ************** " << std::endl;

  bool move = mvRootFile(FileTargetName);
  char *epe = getenv("PIXSCAN_STORAGE_PATH_EXP");
  std::string exportPathE = "";
  std::string exportPath = "";
  if (epe != NULL) {
    exportPathE = epe;
    exportPathE += "/";
  }
  if (move) {
    std::string ok = "Successfully moved the root file from "+FileTargetName+" to "+ exportPath;
    ers::info(PixLib::pix::daq (ERS_HERE, "Merge", ok));
  } else {
    std::string problem = "The root file "+FileTargetName+" cannot be moved. $PIXSCAN_STORAGE_PATH_EXP exists?";
    ers::warning(PixLib::pix::daq (ERS_HERE, "Merge", problem));
  }

  //  sleep(300);
  return 0;
}


void execution(TList* list, TDirectory* targetDir, TDirectory* sourceDir)
{
  TIter next(list);
  TObject* obj;
  while ((obj = next())) {
    TString className = obj->IsA()->GetName();
//     std::cout << "Processing " << obj->GetName() 
// 	      << " class " << className << std::endl;
    if (className == "TDirectory" || className == "TDirectoryFile") {
      //     gDirectory->pwd();
      if (!targetDir->mkdir(obj->GetName())) {
	//	std::cout << "newDir is null" << std::endl;
	if ( !targetDir->cd(obj->GetName()) ) {
	  std::cout << "Error! Folder " << obj->GetName() << " can't be opened!" << std::endl;
	}
      } else {
	//	std::cout << "Created the folder" << std::endl;
	targetDir->cd(obj->GetName());
      }
      TDirectory* newSourceDir =  sourceDir->GetDirectory(obj->GetName());
      newSourceDir->ReadAll();
      TList *nextList = newSourceDir->GetList();
      execution(nextList, gDirectory, newSourceDir);
//       targetDir->pwd();
//       sourceDir->pwd();
//       gDirectory->pwd();
    } else if (className=="TH2F" || className=="TH2D" 
	       || className=="TH1F" || className=="TH1D") {
      obj->Write();
      delete obj;
    }
  }
}


bool mvRootFile(std::string FileTargetName)//outputName, std::string fileName)
{
  std::string exportPath = "";
  std::string exportPathE = "";
  int pos =  FileTargetName.rfind("/");
  std::string pureFileName = FileTargetName.substr(pos+1,FileTargetName.size());

  char *epe = getenv("PIXSCAN_STORAGE_PATH_EXP");
  if (epe != NULL) {
    exportPathE = epe;
    exportPathE += "/";
  }
  if (exportPathE != "") {
    char *ep = getenv("PIXSCAN_STORAGE_PATH");
    if (ep != NULL) {
      exportPath = ep;
      exportPath += "/";
    }
    if (exportPath != "") {
      std::string cmd = "/bin/mv "+FileTargetName+" "+exportPath+pureFileName;
      int b1 = system(cmd.c_str());
      if (b1==0) return true;
      else return false;
    }
  }
  return false;
}
