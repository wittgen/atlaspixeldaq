#include <fstream>
#include <unistd.h>
#include <csignal>

#include <owl/semaphore.h>
#include <ipc/core.h>

#include <pmg/pmg_initSync.h>

#include "PixHistoServer/PixHistoServerInterface.h"
#include "PixHistoServer/PixRootFileInMem.h"
#include "PixUtilities/PixMessages.h"

using namespace PixLib;


OWLSemaphore* sem=nullptr;

void signal_handler(int signal) {
  if(sem){
    sem->post();
  }
}

int main(int argc, char **argv) {

  if(argc!=4) {
    std::cout << "USAGE: PixReader [partition name] [reader name] [name server] " << std::endl;
    return 1;
  }
  
  std::string ipcPartitionName = argv[1];
  std::string readerName = argv[2];
  std::string nameServer = argv[3];
  std::cout << "partition name: "   << ipcPartitionName << std::endl;
  std::cout << "reader name: "      << readerName       << std::endl;
  std::cout << "name server name: " << nameServer       << std::endl;

  std::string applName = "PixReader";
  std::string msg;

  // Start IPCCore    
  IPCCore::init(argc, argv);
  IPCPartition *ipcPartition;
  try {
    ipcPartition = new IPCPartition(ipcPartitionName);
  } catch(...) {
    ers::fatal(PixLib::pix::daq (ERS_HERE, applName, "PixReader: FATAL!! problems while connecting to the partition"));
    return -1;
  }
  ers::log(PixLib::pix::daq (ERS_HERE, applName,"Successfully connected to partition "));

  //Starting the name_server class
  PixRootFileInMem *reader = new PixRootFileInMem(ipcPartition, readerName, nameServer);

  if (reader==NULL) {
    msg = "The "+readerName+" has been created null ";
    ers::fatal(PixLib::pix::daq (ERS_HERE, applName, msg));
    return -1;
  } else {
    msg = "Successfully created the reader "+readerName+" "; 
    ers::log(PixLib::pix::daq (ERS_HERE, applName, msg));
  }
  
  pmg_initSync();

  // Install the signal handler
  std::signal(SIGINT   , signal_handler);
  std::signal(SIGQUIT  , signal_handler);
  std::signal(SIGILL   , signal_handler);
  std::signal(SIGTRAP  , signal_handler);
  std::signal(SIGABRT  , signal_handler);
  std::signal(SIGIOT   , signal_handler);
  std::signal(SIGBUS   , signal_handler);
  std::signal(SIGFPE   , signal_handler);
  std::signal(SIGKILL  , signal_handler);
  std::signal(SIGSEGV  , signal_handler);
  std::signal(SIGPIPE  , signal_handler);
  std::signal(SIGTERM  , signal_handler);
  std::signal(SIGSTKFLT, signal_handler);
  std::signal(SIGSYS   , signal_handler);

  sem = new OWLSemaphore();
  sem->wait();
  delete sem;

  return EXIT_SUCCESS;

}
