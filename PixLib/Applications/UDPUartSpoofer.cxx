#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include "LPPacket.h"
#include "UDPClient.h"
#include <chrono>

#include "boost/program_options.hpp"
namespace po = boost::program_options;


std::string get_log_msg(int log_type, int rodid);


void usage(const po::options_description & desc) {

    std::cout << "Usage: UDPUartSpoofer "<< "[-h] --ip RODIP  --host-ip IP --host-port PORT --port LOCALPORT\n";
    std::cout << desc << std::endl;

}

int main(int argc, const char** argv) {


    std::string receiver_ip;
    uint16_t receiver_port, rodid, port;

    po::options_description desc =  po::options_description("Options");

    using po::value;
    using std::string;
    int musec = 1;

    desc.add_options()
                    ("help,h", "Print help and exit")
                    ("host-ip"			, value(&receiver_ip)->required()	, "Host IP" )
                    ("host-port"		, value(&receiver_port)->required()	, "Host port" )
                    ("rodid" 			, value(&rodid)->required()			, "Rod id. Determines the port on this machine." )
                    ("port" 			, value(&port)->required()			, "Port on this machine." )
                    ("musec" 			, value(&musec)						, "time microseconds" )
                    ;

    po::variables_map vm;
    try {
        po::store(po::parse_command_line( argc, argv, desc), vm );
        po::notify(vm);

        if(vm.count("help")) {usage(desc); exit(0); }

    } catch (...) {
        usage(desc);
        exit(1);
    }


    srand(time(NULL));

    UDPClient client(port);
    client.setIpAddress(receiver_ip.c_str());
    client.setPort(receiver_port);

    // initialize big message logs
    std::vector< std::vector<std::string>> msgs;
    for(int i=0; i<16; i++) {   // 16 rods
        std::vector<std::string> vec;
        for(int j=0; j<3; j++)  // 3 log types
            vec.push_back(get_log_msg(j, i));
        msgs.push_back(vec);
    }


    std::shared_ptr<LPPacket> packet = std::make_shared<LPPacket>();
    int res, n=0;
    std::stringstream ss;


    int npacket = 0;
    double bpacket = 0;
    std::chrono::steady_clock::time_point stopwatch;
    std::chrono::time_point<std::chrono::steady_clock>timenow;

    while(1) {


        int log_type = rand() % 3;
        packet->header.log_type = log_type;

        ss.str("");
        ss << msgs[rodid][log_type] << " "  << "Tick " <<  n << "\n";
        packet->body = ss.str();

        /*
        if(!packet->fits_buffer()) {
            std::cout << "Buffer is too small!\n";
            continue;
        }
        */

        packet->serialize();


        //std::cout<<"Sending packet len:"<< packet->serializedSize() <<std::endl;
        res = client.send_data(packet->buffer, packet-> serializedSize());


        if (res<0)
            std::cout<<"Send Error"<<std::endl;
        ++n;

        //std::cout << "Tick " << n << std::endl;
        usleep(musec);

        npacket++;
        bpacket += packet->serializedSize();


        timenow = std::chrono::steady_clock::now();
        if(npacket == 0) stopwatch = timenow;
        auto tdiff = std::chrono::duration_cast<std::chrono::seconds> (timenow - stopwatch);
        if( tdiff > std::chrono::seconds(5)) {
            double rate = npacket / tdiff.count();
            double brate = bpacket / tdiff.count();
            std:: cout << " Performance: " << rate << "packets/sec " <<  brate << " bytes/sec" << std::endl;
            npacket = 0;
            bpacket = 0;
            stopwatch = timenow;
        }


    }




    return 0;
}


// long message, for example 2 = log_type, a(hex) ->  slot = 10 + 1 +5
/*
2aaa..a
22aa..a
222a..a
.
2222222
*/
std::string  get_log_msg(int log_type, int rodid) {
    std::stringstream ss;
    const int nlines = 1;
    const int nchar = 100;
    for(int i=0; i< nlines; i++) {
        for(int j=0; j<nchar; j++) {
            if(j==nchar-1) {
                ss<<"\n";
                continue;
            }
            // first character on the line is log type
            if(j==0) {
                ss << log_type;
                continue;
            }

            // last line is full
            if(i == nlines-1) ss << std::hex << log_type;
            else
                if(j<=i) ss << log_type;	// triangle
                else ss << std::hex << rodid;
        }
    }

    return ss.str();
}
