#include <iostream>
#include <signal.h>
#include <unistd.h>
#include <csignal>

#include <ipc/object.h>

#include <pmg/pmg_initSync.h>

#include <sys/wait.h>

#include "PixConnectivity/PartitionConnectivity.h"
#include "PixConnectivity/Pp0Connectivity.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "PixConnectivity/PixConnectivity.h"
#include "PixConnectivity/PixDisable.h"
#include "PixTrigController/TimPixTrigController.h"
#include "PixModule/PixModule.h"
#include "PixBoc/PixBoc.h"
#include "PixUtilities/PixMessages.h"


using namespace PixLib;

int errcount=0;
int forkcount=0;

std::atomic_bool stop = false;

void signal_handler(int signal) {
  stop = true;
}

void mem(int del) {
  pid_t pid = getpid();
  std::ostringstream os;
  os << "/proc/" << pid << "/status";
  std::cout << "Mem Info (" << os.str() << "):" << std::endl;
  std::ifstream st(os.str().c_str());
  while (!st.eof()) {
    std::string line;
    std::getline(st, line);
    if (line.substr(0,2) == "Vm") {
      std::cout << "  " << line << std::endl;
    }
  }
  sleep(del);
}

std::string getTime(int i) {
  char t_s[100];
  time_t t;
  tm t_m;
  t = i;
  localtime_r(&t, &t_m);
  strftime(t_s,100,"%Y-%m-%d %H:%M:%S",&t_m);
  std::string date = t_s;
  return date;
}


std::string element(std::string str, int num, char sep) {
  size_t ip;
  int start = -1,end = -1;
  int nel = -1;
  bool spc = true;
  bool quote = false;                                                                                                                                        
  for (ip=0;ip<str.size();ip++) {
    if (str[ip] == '"') quote = !quote;
    if (str[ip] == sep || (sep == 0 && (str[ip] == 9 || str[ip]==10 || str[ip] == 32))) {
      if (!spc && !quote) {
        spc = true;
        if (nel==num) end = ip-1;
      }
    } else {
      if (spc) {
        spc = false;
        nel++;
        if (nel==num) start = ip;
      }
    }
  }
  if (!spc && nel==num) end = str.size()-1;                                                                                                                   
  if (start >= 0) {
    std::string c;
    std::string s = str.substr(start,end-start+1);
    for (ip=0;ip<s.size();ip++) {
      if (s[ip] != '"') c += s[ip];
    }
    return c;
  } else {
    return "";
  }
}

unsigned int getRev(std::string filename) {
  // std::cout << " filename is " << filename << std::endl;
  size_t pos = filename.find_last_of("_");
  if (pos != std::string::npos) {
    std::string sdate = filename.substr(pos+1);
    sdate = element(sdate, 0, '.');
    sdate = sdate.substr(0, sdate.size()-3);
    unsigned int date = 0;
    std::istringstream is(sdate);
    is >> date;
    std::cout << "->\t" << date << "\t" << filename << "\t(" << getTime(date) << ")" << std::endl;
    return date;
  } else {
    return 0;
  }
}


void connectToIpc(IPCPartition* &ipcPartition, std::string ipcPartitionName, PixDbServerInterface* &DbServer, std::string serverName) {
  //  Create IPCPartition constructors
  std::cout << std::endl << "Starting partition " << ipcPartitionName << std::endl;
  try {
    ipcPartition = new IPCPartition(ipcPartitionName);
  } catch(...) {
    std::cout << "PixDbServer: ERROR!! problems while connecting to the partition!" << std::endl;
  }
  bool ready=false;
  int nTry=0;
  std::cout << "Trying to connect to DbServer with name " << serverName << "  ..... " << std::endl;
  do {
    sleep(1);
    DbServer=new  PixDbServerInterface(ipcPartition,serverName,PixDbServerInterface::CLIENT, ready);
    if(!ready) delete DbServer;
    else break;
    std::cout << " ..... attempt number: " << nTry << std::endl;
    nTry++;
  } while(nTry<10);  
  if(!ready) {
    std::cout << "Impossible to connect to DbServer " << serverName << std::endl;
    exit(-1);
  }
  std::cout << "Succesfully connected to DbServer " << serverName << std::endl << std::endl;
}

std::string getTime() {
  char t_s[100];
  time_t t;
  tm t_m;
  t = time(NULL);
  localtime_r(&t, &t_m);
  strftime(t_s,100,"%Y-%m-%d-%H%M%S",&t_m);
  std::string date = t_s;
  return date;
}

int main(int argc, char** argv) {
#ifdef USE_GCOV
  struct sigaction sa;
  sigemptyset(&sa.sa_mask); 
  sa.sa_flags = 0;
  int ret1=0;
  sa.sa_handler=catch_function;
  ret1 = sigaction(SIGUSR1, &sa, NULL);
#endif
  struct timeval t0;
  struct timeval t1;  
  struct timeval t2;
  struct timeval t3;
  
  //int time;
  int status;
  std::string cfgMTag;
  
  if (argc == 7 )  {
    cfgMTag=argv[6];
  } else if (argc==6) {
    cfgMTag=argv[5];
  } else {
    std::cout << std::endl << " USAGE: ./PixDbServerLoader [partitionName] [serverName] [id_tag] [tag_c] [tag_cf] [tag_cf_mod](optional)"<<std::endl;
    exit(-1);
  }
  
  std::string ipcPartitionName = argv[1];
  std::string serverName = argv[2];
  std::string   idTag = argv[3]; 
  std::string connTag = argv[4];
  std::string  cfgTag = argv[5];
  int stepSize=0;
  int stepFirst=0;
  int stepLast=0;
  std::string domainName="Configuration-"+idTag;
  std::string firstStep;

  std::cout << " stepSize/First/Last is: " << stepSize << " " << stepFirst << " " << stepLast << std::endl << std::endl;

  bool result;

  //  Start IPCCore
  IPCCore::init(argc, argv);
  IPCPartition *ipcPartition;
  PixDbServerInterface *DbServer;
  
  connectToIpc(ipcPartition, ipcPartitionName, DbServer, serverName); 

  DbServer->ready();
  pmg_initSync();
  
  std::stringstream info1;
  info1 << "PixDbServerLoader_main_";
  std::string applName1=info1.str();

  ers::info(PixLib::pix::daq (ERS_HERE,applName1,  "Using tag: "+connTag+" / "+cfgTag+" / "+cfgMTag+" / "+idTag));

  // Install the signal handler
  std::signal(SIGINT   , signal_handler);
  std::signal(SIGQUIT  , signal_handler);
  std::signal(SIGILL   , signal_handler);
  std::signal(SIGTRAP  , signal_handler);
  std::signal(SIGABRT  , signal_handler);
  std::signal(SIGIOT   , signal_handler);
  std::signal(SIGBUS   , signal_handler);
  std::signal(SIGFPE   , signal_handler);
  std::signal(SIGKILL  , signal_handler);
  std::signal(SIGSEGV  , signal_handler);
  std::signal(SIGPIPE  , signal_handler);
  std::signal(SIGSTKFLT, signal_handler);
  std::signal(SIGSYS   , signal_handler);

  do {
    sleep(20);
    //std::cout << std::endl << std::endl;
    //std::cout << "*******************************************************" << std::endl;
    //std::cout << "**" << std::endl;
    //std::cout << "**    Starting process to update database on disk"       << std::endl;
    //std::cout << "**" << std::endl;
    //std::cout << "*******************************************************" << std::endl;
    
    pid_t  tpid, rpid, mpid, rppid;
    
    forkcount++;
    tpid=fork();
    if( tpid==0) {
      //std::cout << std::endl << "-----------------------------------------------------------------------------" << std::endl;
      //std::cout << "Starting tim Configuration loop ...... " << std::endl;

      std::stringstream info2;
      info2 << "PixDbServerLoader_fork_"<<forkcount;
      std::string applName2=info2.str();

      PixConnectivity* m_conn;
      try {
	m_conn = new PixConnectivity(DbServer, connTag, connTag, connTag, cfgTag, idTag, cfgMTag);
      }   catch(PixConnectivityExc &e) {
	ers::fatal(PixLib::pix::daq (ERS_HERE,applName2, 
			     "Exception caught: "+e.getDescr()));    
	return -1;
      }
      catch(...) { 
	ers::fatal(PixLib::pix::daq (ERS_HERE,applName2,
			     "Unknown exception caught while creating PixConnectivity"));   
	return -1;
      }
      
      bool first = true;
      std::string cftag;
      std::vector<std::string> domlist;
      DbServer->listDomainRem(domlist);
      std::vector<std::string>::iterator domit;
      std::string ref="Configuration-";
      for(domit=domlist.begin();domit!=domlist.end();domit++) {
	if ((*domit).find("Configuration-") != std::string::npos) {
	  domainName = (*domit);
	  std::string newidTag=(*domit).substr(ref.length());
	  std::vector<std::string> taglist;
	  DbServer->listTagsRem(domainName,taglist);
	  std::vector<std::string>::iterator tagit;
	  for(tagit=taglist.begin();tagit!=taglist.end();tagit++) {
	    //std::cout << std::endl;
	    std::map<std::string, std::vector<unsigned int> > newtim;       
            cftag = (*tagit);
	    std::vector<std::string> objlist;
	    DbServer->listObjectsRem(domainName,(*tagit),"TimPixTrigController",objlist);
	    std::vector<std::string>::iterator objit;
	    for(objit=objlist.begin();objit!=objlist.end();objit++) {
	      std::vector<unsigned int> newtimObj;
	      std::vector<unsigned int> revlist;
	      DbServer->listRevisionsRem(domainName,(*tagit),(*objit),revlist);
	      std::vector<unsigned int>::iterator revit;
	      bool fr=true;
	      for(revit=revlist.begin();revit!=revlist.end();revit++) {
		std::string pen;
		DbServer->getPending(pen,domainName,(*tagit),(*objit),(*revit));
		if (pen.find("_Tmp")== std::string::npos && pen!="OnDB") {
		  if (fr) {
		    std::cout << "- TIM " << (*objit) << std::endl; 
		    std::cout << "  -> revision " << (*revit) << " has pendingTag " << pen << std::endl;
		    newtimObj.push_back( (*revit) );
		    fr=false;
		  } else {
		    std::cout << "  -> revision " << (*revit) << " has pendingTag " << pen << std::endl;
		    newtimObj.push_back( (*revit) );
		  }
		}
		if ( pen!="OnDB" && pen.find("_Tmp")!= std::string::npos) {
		  //if (fr) {
		  //  std::cout << "- TIM " << (*objit) << std::endl; 
		  //  std::cout << "  -> revision " << (*revit) << " has pendingTag " << pen << std::endl;
		  //  fr=false;
		  //} else std::cout << "  -> revision " << (*revit) << " has pendingTag " << pen << std::endl;
		}
	      }
	      if (newtimObj.size()!=0)
		newtim[(*objit)]=newtimObj;
	    }
            if (newtim.size() > 0) {
	      std::cout <<newtim.size()  << " TIM config must be updated  in conf. tag " << (*tagit) << "  ( " << newidTag << " ) " << std::endl;
	    }
	    std::map<std::string, std::vector<unsigned int> >::iterator it;
	    for(it=newtim.begin(); it!=newtim.end(); it++) {
	      std::cout << std::endl << "- " << it->first << std::endl;
	      for (size_t i=0; i<(it->second).size(); i++) {
		unsigned int newRev = (it->second)[i];
		std::cout <<  "  - saving revision " << newRev << "  ...... " << std::endl;
		TimPixTrigController *tmpTim= new TimPixTrigController(it->first);
		result=tmpTim->readConfig(DbServer,domainName,(*tagit),newRev);
		if (!result) {
		  std::stringstream infom;
		  infom << "Error in loading Config for TIM: "+(it->first)+" with revision " << newRev << " from DbServer";		
		  ers::error(PixLib::pix::daq (ERS_HERE,applName2, infom.str()));
		} else {	
		  try {
		    if (first) { m_conn->loadConn(); first = false; }
		    m_conn->cfgFromDb();
		    try {
		      m_conn->setCfgTag(cftag, newidTag);
		    }
		    catch (PixConnectivityExc &e) {
		      if (e.getId() == "CANTOPENCFG" || e.getId() == "CANTOPENCFGW") {
			m_conn->setCfgTag(cfgTag, newidTag);
			m_conn->createCfgTag(cftag);
		      } else {
			throw(PixConnectivityExc(PixConnectivityExc::FATAL, "CANTOPENCFG", "Cannot open configuration db: "+e.getDescr()));
		      }
		    }
		    //m_conn->setCfgRev(it->second);
		    tmpTim->saveConfig(m_conn, newRev);
		    DbServer->setPending("OnDB",domainName, cftag, it->first,newRev);
		  } catch (PixConnectivityExc &ef) {
		    ers::error(PixLib::pix::daq (ERS_HERE,applName2,
					 "PixConnectivity Exception caught while saving Tim "+ it->first+" : "+ef.getDescr())); 
		  }
		}
	      }
	    }
	  }
	}
      }
      kill(getpid(), SIGTERM);
    }
    waitpid(tpid, &status,0);
   
  
    forkcount++;
    rpid=fork();
    if( rpid==0) {
      //std::cout << std::endl << "-----------------------------------------------------------------------------" << std::endl;
      //std::cout << "Starting ModuleGroup Configuration loop " << std::endl;

      std::stringstream info;
      info << "PixDbServerLoader_fork_"<<forkcount;
      std::string applName=info.str();

      PixConnectivity* m_conn;
      try {
	m_conn = new PixConnectivity(DbServer, connTag, connTag, connTag, cfgTag, idTag, cfgMTag);
      }   catch(PixConnectivityExc &e) {
	  ers::fatal(PixLib::pix::daq (ERS_HERE,applName,
			     "Exception caught: "+e.getDescr()));    
	return -1;
      }
      catch(...) { 
	ers::fatal(PixLib::pix::daq (ERS_HERE,applName,
			     "Unknown exception caught while creating PixConnectivity"));   
	return -1;
      }

      bool first = true;
      std::string cftag;
      std::vector<std::string> domlist;
      DbServer->listDomainRem(domlist);
      std::vector<std::string>::iterator domit;
      std::string ref="Configuration-";
      for(domit=domlist.begin();domit!=domlist.end();domit++) {
	if ((*domit).find("Configuration-") != std::string::npos) {
	  domainName = (*domit);
	  std::string newidTag=(*domit).substr(ref.length());
	  std::vector<std::string> taglist;
	  DbServer->listTagsRem(domainName,taglist);
	  std::vector<std::string>::iterator tagit;
	  for(tagit=taglist.begin();tagit!=taglist.end();tagit++) {
	    //std::cout << std::endl;
	    std::map<std::string, std::vector<unsigned int> > newrod;
	    cftag = (*tagit);
	    std::vector<std::string> objlist;
	    DbServer->listObjectsRem(domainName,(*tagit),"PixModuleGroup",objlist);
	    std::vector<std::string>::iterator objit;
	    for(objit=objlist.begin();objit!=objlist.end();objit++) {
	      std::vector<unsigned int> newrodObj;
	      std::vector<unsigned int> revlist;
	      DbServer->listRevisionsRem(domainName,(*tagit),(*objit),revlist);
	      std::vector<unsigned int>::iterator revit;
	      bool fr=true;
	      for(revit=revlist.begin();revit!=revlist.end();revit++) {
		std::string pen;
		DbServer->getPending(pen,domainName,(*tagit),(*objit),(*revit));	
		if (pen.find("_Tmp")== std::string::npos && pen!="OnDB") {
		  if (fr) {
		    std::cout << "- ROD " << (*objit) << std::endl; 
		    std::cout << "  -> revision " << (*revit) << " has pendingTag " << pen << std::endl;
		    newrodObj.push_back( (*revit) );
		    fr=false;
		  } else {
		    std::cout << "  -> revision " << (*revit) << " has pendingTag " << pen << std::endl;
		    newrodObj.push_back( (*revit) );
		  }
		}
		if ( pen!="OnDB" && pen.find("_Tmp")!= std::string::npos) {
		  //if (fr) {
		  //  std::cout << "- ROD " << (*objit) << std::endl; 
		  //  std::cout << "  -> revision " << (*revit) << " has pendingTag " << pen << std::endl;
		  //  fr=false;
		  //} else std::cout << "  -> revision " << (*revit) << " has pendingTag " << pen << std::endl;
		}
	      }
	      if (newrodObj.size()!=0)
		newrod[(*objit)]=newrodObj;
	    }
	    std::map<std::string,  std::vector<unsigned int> >::iterator it;
	    if (newrod.size() > 0) {
	      std::cout << newrod.size()  << " ROD config must be updated in conf. tag " << (*tagit) << "  ( " << newidTag << " ) " << std::endl;
	    }
	    if ( newrod.size()!=0 ) {
	      std::ostringstream message;
	      message <<  newrod.size() << " PixModuleGroup configuration must be written in permanent database";
	      ers::info(PixLib::pix::daq (ERS_HERE,applName,message.str()));
	    }
	    for(it=newrod.begin(); it!=newrod.end(); it++) {
	      std::cout << std::endl << "- " << it->first << std::endl;
	      for (size_t i=0; i<(it->second).size(); i++) {
		unsigned int newRev = (it->second)[i];
		// temporary cfg. reading to identify ctrl. type
		PixModuleGroup *tmpModGrp= new PixModuleGroup(it->first);
		try{
		  tmpModGrp->readConfig(DbServer,domainName,cftag,newRev);
		}catch(...){}
		CtrlType ctype = tmpModGrp->getCtrlType();
		delete tmpModGrp;
		// now create the actual PixModuleGroup
		tmpModGrp= new PixModuleGroup(it->first, ctype);
		result=tmpModGrp->readConfig(DbServer,domainName,cftag,newRev);
		unsigned int revM=(tmpModGrp->config()).rev();
		unsigned int revC=(tmpModGrp->getPixController()->config()).rev();
		unsigned int revB=newRev;
		if(tmpModGrp->getBocAvailable())
		  revB = tmpModGrp->getPixBoc()->getConfig()->rev();
		std::cout << it->first << "   " << revM  << "   " << revC << "   " << revB << std::endl;
		if (revM!=newRev || revC!=newRev || revB!=newRev) {
		  std::stringstream infom;
		  infom << "PixModuleGroup: "+(it->first)+": (" << newRev << ") "  <<  "   " << revM  << "   " << revC << "   " << revB;
		  ers::error(PixLib::pix::daq (ERS_HERE,applName, infom.str()));
		  result=false;
		} 
		if (!result) {
		  std::stringstream infom;
		  infom << "Error in loading Config for PixModuleGroup: "+(it->first)+" with revision " << newRev << " from DbServer";		
		  ers::error(PixLib::pix::daq (ERS_HERE,applName,  infom.str()));
		} else { 
		  try {
		    if (first) { m_conn->loadConn(); first = false; }
		    m_conn->cfgFromDb();
		    try {
		      m_conn->setCfgTag(cftag, newidTag);
		    }
		    catch (PixConnectivityExc &e) {
		      if (e.getId() == "CANTOPENCFG" || e.getId() == "CANTOPENCFGW") {
			m_conn->setCfgTag(cfgTag, newidTag);
			m_conn->createCfgTag(cftag);
		      } else {
			throw(PixConnectivityExc(PixConnectivityExc::FATAL, "CANTOPENCFG", "Cannot open configuration db: "+e.getDescr()));
		      }
		    }
		    //m_conn->setCfgRev(it->second);
		    tmpModGrp->saveConfig(m_conn, newRev);
		    DbServer->setPending("OnDB",domainName, cftag, it->first,newRev);
		  } catch (PixConnectivityExc &ef) { 
		    ers::error(PixLib::pix::daq (ERS_HERE,applName,  
					 "PixConnectivity Exception caught while saving PixModulegroup "+ it->first+" : "+ef.getDescr())); 
		  }
		}
		delete tmpModGrp;
	      }
	    }
	    if ( newrod.size()!=0 ) {
	      std::ostringstream message;
	      message <<  newrod.size() << " PixModuleGroup configuration have been written into the permanent database";
	      ers::info(PixLib::pix::daq (ERS_HERE,applName, message.str()));
	    }
	  }
	}
      }
      kill(getpid(), SIGTERM);
    }
    // decide where to put this line
    waitpid(rpid, &status,0);


    forkcount++;
    rppid=fork();
    if( rppid==0) {
      //std::cout << std::endl << "-----------------------------------------------------------------------------" << std::endl;
      //std::cout << "Starting RunConfig and PixDisable loop " << std::endl;

      std::stringstream info;
      info << "PixDbServerLoader_fork_"<<forkcount;
      std::string applName=info.str();

      PixConnectivity* m_conn;
      try {
	m_conn = new PixConnectivity(DbServer, connTag, connTag, connTag, cfgTag, idTag, cfgMTag);
      }   catch(PixConnectivityExc &e) {
	ers::fatal(PixLib::pix::daq (ERS_HERE,applName, 
			     "Exception caught: "+e.getDescr()));    
	return -1;
      }
      catch(...) { 
	ers::fatal(PixLib::pix::daq (ERS_HERE,applName,
			     "Unknown exception caught while creating PixConnectivity"));   
	return -1;
      }
	
      bool first = true;
      std::string cftag;
      std::vector<std::string> domlist;
      DbServer->listDomainRem(domlist);
      std::vector<std::string>::iterator domit;
      std::string ref="Configuration-";
      for(domit=domlist.begin();domit!=domlist.end();domit++) {
	if ((*domit).find("Configuration-") != std::string::npos) {
	  domainName = (*domit);
	  std::string newidTag=(*domit).substr(ref.length());
	  std::vector<std::string> taglist;
	  DbServer->listTagsRem(domainName,taglist);
	  std::vector<std::string>::iterator tagit;
	  for(tagit=taglist.begin();tagit!=taglist.end();tagit++) {
	    //std::cout << std::endl;
	    std::map<std::string, std::vector<unsigned int> > newrun;
	    std::map<std::string, std::vector<unsigned int> > newdis;
	    cftag = (*tagit);
	    std::vector<std::string> objlist;
	    DbServer->listObjectsRem(domainName,(*tagit),"PixRunConfig",objlist);
	    std::vector<std::string>::iterator objit;
	    for(objit=objlist.begin();objit!=objlist.end();objit++) {
	      std::vector<unsigned int> newrunObj;
	      std::vector<unsigned int> revlist;
	      DbServer->listRevisionsRem(domainName,(*tagit),(*objit),revlist);
	      std::vector<unsigned int>::iterator revit;
	      bool fr=true;
	      for(revit=revlist.begin();revit!=revlist.end();revit++) {
		std::string pen;
		DbServer->getPending(pen,domainName,(*tagit),(*objit),(*revit));
		if (pen.find("_Tmp")== std::string::npos && pen!="OnDB" && (*objit).find("|")== std::string::npos) {
		  if (fr) {
		    std::cout << "- PixRunConfig " << (*objit) << std::endl; 
		    std::cout << "  -> revision " << (*revit) << " has pendingTag " << pen << std::endl;
		    newrunObj.push_back( (*revit) );
		    fr=false;
		  } else {
		    std::cout << "  -> revision " << (*revit) << " has pendingTag " << pen << std::endl;
		    newrunObj.push_back( (*revit) );
		  }
		}
		if ( pen!="OnDB" && pen.find("_Tmp")!= std::string::npos && (*objit).find("|")== std::string::npos) {
		  //if (fr) {
		  //  std::cout << "- PixRunConfig " << (*objit) << std::endl; 
		  //  std::cout << "  -> revision " << (*revit) << " has pendingTag " << pen << std::endl;
		  //  fr=false;
		  //} else std::cout << "  -> revision " << (*revit) << " has pendingTag " << pen << std::endl;
		}
	      }
	      if (newrunObj.size()!=0)
		newrun[(*objit)]=newrunObj;
	    }

	    objlist.clear();
	    DbServer->listObjectsRem(domainName,(*tagit),"PixDisable",objlist);
	    for(objit=objlist.begin();objit!=objlist.end();objit++) {
	      std::vector<unsigned int> newdisObj;
	      std::vector<unsigned int> revlist;
	      DbServer->listRevisionsRem(domainName,(*tagit),(*objit),revlist);
	      std::vector<unsigned int>::iterator revit;
	      bool fr=true;
	      for(revit=revlist.begin();revit!=revlist.end();revit++) {
		std::string pen;
		DbServer->getPending(pen,domainName,(*tagit),(*objit),(*revit));
		if (pen.find("_Tmp")== std::string::npos && pen!="OnDB") {
		  if (fr) {
		    std::cout << "- PixDisable " << (*objit) << std::endl; 
		    std::cout << "  -> revision " << (*revit) << " has pendingTag " << pen << std::endl;
		    newdisObj.push_back( (*revit) );
		    fr=false;
		  } else {
		    std::cout << "  -> revision " << (*revit) << " has pendingTag " << pen << std::endl;
		    newdisObj.push_back( (*revit) );
		  }
		}
		if ( pen!="OnDB" && pen.find("_Tmp")!= std::string::npos) {
		  //if (fr) {
		  //  std::cout << "- PixDisable " << (*objit) << std::endl; 
		  //  std::cout << "  -> revision " << (*revit) << " has pendingTag " << pen << std::endl;
		  //  fr=false;
		  //} else std::cout << "  -> revision " << (*revit) << " has pendingTag " << pen << std::endl;
		}
	      }
	      if (newdisObj.size()!=0)
		newdis[(*objit)]=newdisObj;
	    }
	    
	    std::map<std::string,std::vector<unsigned int> >::iterator it;
	    if (newdis.size() > 0) {
	      std::cout << newdis.size()  << " PixDisable must be updated in conf. tag " << (*tagit) << "  ( " << newidTag << " ) " << std::endl;
	    }
	    for(it=newdis.begin(); it!=newdis.end(); it++) {
	      std::cout << std::endl << "- " << it->first << std::endl;
	      for (size_t i=0; i<(it->second).size(); i++) {
		unsigned int newRev = (it->second)[i];
		std::cout <<  "  - saving revision " << newRev << "  ...... " << std::endl;
		PixDisable *tmpDis= new PixDisable(it->first);
		result=tmpDis->readConfig(DbServer,domainName,(*tagit),newRev);
		if (!result) {
		  std::stringstream infom;
		  infom << "Error in loading PixDisable: "+(it->first)+" with revision " << newRev << " from DbServer";		
		  ers::error(PixLib::pix::daq (ERS_HERE,applName, infom.str()));
		} else {
		  try {		  
		    if (first) { m_conn->loadConn(); first = false; }
		    m_conn->cfgFromDb();
		    try {
		      m_conn->setCfgTag(cftag, newidTag);
		    }
		    catch (PixConnectivityExc &e) {
		      if (e.getId() == "CANTOPENCFG" || e.getId() == "CANTOPENCFGW") {
			m_conn->setCfgTag(cfgTag, newidTag);
			m_conn->createCfgTag(cftag);
		      } else {
			throw(PixConnectivityExc(PixConnectivityExc::FATAL, "CANTOPENCFG", "Cannot open configuration db"));
		      }
		    }
		    // m_conn->setCfgRev(newRev);
		    m_conn->writeDisable(tmpDis, it->first, newRev);
		    DbServer->setPending("OnDB",domainName, cftag, it->first,newRev);	
          } catch (PixConnectivityExc &ef) {
		    ers::error(PixLib::pix::daq (ERS_HERE,applName,
					 "PixConnectivity Exception caught while saving PixDisable "+ it->first+" : "+ef.getDescr()));
  		  }
		}
	      }
	    }
	    if (newrun.size() > 0) {
	      std::cout << std::endl << newrun.size()  << " PixRunConfig must be updated in conf. tag " << (*tagit) << "  ( " << newidTag << " ) " << std::endl;
	    }
	    for(it=newrun.begin(); it!=newrun.end(); it++) {
	      std::cout << std::endl << "- " << it->first << std::endl;
	      for (size_t i=0; i<(it->second).size(); i++) {
		unsigned int newRev = (it->second)[i];
		std::cout <<  "  - saving revision " << newRev << "  ...... " << std::endl;
		m_conn->setCfgRev(newRev);
		try {
		  if (first) { m_conn->loadConn(); first = false; }
		  m_conn->cfgFromDbServ();
		  try {
		    m_conn->setCfgTag(cftag, newidTag);
		  }
		  catch (PixConnectivityExc &e) {
		    if (e.getId() == "CANTOPENCFG" || e.getId() == "CANTOPENCFGW") {
		      m_conn->setCfgTag(cfgTag, newidTag);
		      m_conn->createCfgTag(cftag);
		    } else {
		      throw(PixConnectivityExc(PixConnectivityExc::FATAL, "CANTOPENCFG", "Cannot open configuration db: "+e.getDescr()));
		    }
		  }
		  PixRunConfig *tmpRun = m_conn->getRunConfig(it->first);
		  if (tmpRun==NULL) {
		    std::stringstream infom;
		    infom << "Error in loading PixRunConfig: "+(it->first)+" with revision " << newRev << " from DbServer";		
		    ers::error(PixLib::pix::daq (ERS_HERE,applName, infom.str()));
		  }
		  else {
		    m_conn->cfgFromDb();
		    m_conn->writeRunConfig(tmpRun, it->first, newRev);
		    DbServer->setPending("OnDB", domainName, cftag, it->first, newRev);
		  }
		}
		catch (PixConnectivityExc &ef) { 
		  ers::error(PixLib::pix::daq (ERS_HERE,applName, 
				       "PixConnectivity Exception caught while saving PixRunConfig "+ it->first+" : "+ef.getDescr())); 
		}
	      }
	    }
	  } 
	} 
      }
      kill(getpid(), SIGTERM);
    }
    waitpid(rppid, &status,0);

    forkcount++;
    mpid=fork();
    if( mpid==0) {
      //      time=gettimeofday(&t0,NULL);
      //std::cout << std::endl << "-----------------------------------------------------------------------------" << std::endl;    
      //std::cout << "Starting module Configuration loop " << std::endl;

      std::stringstream info;
      info << "PixDbServerLoader_fork_"<<forkcount;
      std::string applName=info.str();

      PixConnectivity* m_conn;
      try {
	m_conn = new PixConnectivity(DbServer, connTag, connTag, connTag, cfgTag, idTag, cfgMTag);
      }   catch(PixConnectivityExc &e) {
	ers::fatal(PixLib::pix::daq (ERS_HERE,applName,  
			     "Exception caught: "+e.getDescr()));    
	return -1;
      }
      catch(...) { 
	ers::fatal(PixLib::pix::daq (ERS_HERE,applName,
			     "Unknown exception caught while creating PixConnectivity"));   
	return -1;
      }
          
      //time=
              (void)gettimeofday(&t0,NULL);
      //mem(0);
      bool first = true;
      std::string cfMtag;
      std::vector<std::string> domlist;
      
      DbServer->listDomainRem(domlist);
      std::vector<std::string>::iterator domit;
      std::string ref="Configuration-";
      for(domit=domlist.begin();domit!=domlist.end();domit++) {
	if ((*domit).find("Configuration-") != std::string::npos) {
	  domainName = (*domit);
	  std::string newidTag=(*domit).substr(ref.length());
	  std::vector<std::string> taglist;
	  DbServer->listTagsRem(domainName,taglist);
	  std::vector<std::string>::iterator tagit;
	  for(tagit=taglist.begin();tagit!=taglist.end();tagit++) {
	    //std::cout << std::endl;
	    std::map<std::string, std::vector<unsigned int> > newmod;
	    cfMtag = (*tagit);
	    std::vector<std::string> objlist;
	    DbServer->listObjectsRem(domainName,(*tagit),"PixModule",objlist);
	    // JGK: update connectivity to have module list available
	    try {
	      m_conn->loadConn();
	    }catch(...) {}
	    //
	    std::vector<std::string>::iterator objit;
	    for(objit=objlist.begin();objit!=objlist.end();objit++) {
	      std::vector<unsigned int> newmodObj;
	      std::vector<unsigned int> revlist;
	      DbServer->listRevisionsRem(domainName,(*tagit),(*objit),revlist);
	      std::vector<unsigned int>::iterator revit;
	      bool fr=true;
	      for(revit=revlist.begin();revit!=revlist.end();revit++) {
		std::string pen;
		DbServer->getPending(pen,domainName,(*tagit),(*objit),(*revit));
		if (pen.find("_Tmp")== std::string::npos && pen!="OnDB") {
		  if (fr) {
		    std::cout << "- PixModule " << (*objit) << std::endl; 
		    std::cout << "  -> revision " << (*revit) << " has pendingTag " << pen << std::endl;
		    newmodObj.push_back( (*revit) );
		    fr=false;
		  } else {
		    std::cout << "  -> revision " << (*revit) << " has pendingTag " << pen << std::endl;
		    newmodObj.push_back( (*revit) );
		  }
		}
		if ( pen!="OnDB" && pen.find("_Tmp")!= std::string::npos) {
		  //if (fr) {
		  //  std::cout << "- PixModule " << (*objit) << std::endl; 
		  //  std::cout << "  -> revision " << (*revit) << " has pendingTag " << pen << std::endl;
		  //  fr=false;
		  //} else std::cout << "  -> revision " << (*revit) << " has pendingTag " << pen << std::endl;
		}
	      }
	      if (newmodObj.size()!=0)
		newmod[(*objit)]=newmodObj;
	    }
	    int step=0;
	    // number of module saved in each run: depends on root leak
	    int steplimit=500; //20
	    int count=0;

	    if (newmod.size() > 0) {
	      std::cout << newmod.size()  << " PixModule config must be updated in conf. tag " << (*tagit) << "  ( " << newidTag << " ) " << std::endl;
	    }
	    std::map<std::string, std::vector<unsigned int> >::iterator it;

	    if ( newmod.size()!=0 ) {
	      std::ostringstream message;
	      message <<  newmod.size() << " PixModule configuration must be written in permanent database";
	      ers::info(PixLib::pix::daq (ERS_HERE,applName, message.str()));
	    }
	    
	    for(it=newmod.begin(); it!=newmod.end(); it++) {
	      if ( step==steplimit) break;
	      count ++;
	      std::cout << std::endl << "- " << count << " - " << it->first << std::endl;
	      ModuleConnectivity *modConn=0;
	      for(std::map<std::string, ModuleConnectivity*>::iterator mci = m_conn->mods.begin(); mci!=m_conn->mods.end(); mci++){
		if(mci->second->prodId()==it->first){
		  modConn = mci->second;
		  break;
		}
	      }
	      EnumMCCflavour::MCCflavour mccFlv = EnumMCCflavour::PM_NO_MCC;
	      EnumFEflavour::FEflavour feFlv   = EnumFEflavour::PM_NO_FE;
	      int nFe=0;
	      if(modConn!=0) modConn->getModPars(mccFlv, feFlv, nFe);
	      for (size_t i=0; i<(it->second).size(); i++) {
		unsigned int newRev = (it->second)[i];
		std::cout <<  "  - saving revision " << newRev << "  ...... " << std::endl;
		PixModule *tmpMod= new PixModule(domainName,cfgMTag,it->first, mccFlv, feFlv, nFe);
		result=tmpMod->readConfig(DbServer,domainName,cfMtag,newRev);
		if (!result) {
		  std::stringstream infom;
		  infom << "Error in loading Config for Module: "+(it->first)+" with revision " << newRev << " from DbServer";		
		 ers::error(PixLib::pix::daq (ERS_HERE,applName, infom.str()));
		} else  {
		  try {
		    if (first) { 
		      //time=
                      (void)gettimeofday(&t2,NULL);
		      m_conn->loadConn(); 
		      m_conn->cfgFromDb();
		      first = false; 
		      //time=
                      (void)gettimeofday(&t3,NULL);
		    }
		    try {
		      m_conn->setCfgTag(cfgTag, newidTag, cfMtag);
		    }
		    catch (PixConnectivityExc &e) {
		      if (e.getId() == "CANTOPENMCFG" || e.getId() == "CANTOPENMCFGW") {
			m_conn->setCfgTag(cfgTag, newidTag, cfgMTag);
			m_conn->createModCfgTag(cfMtag);
			m_conn->setCfgTag(cfgTag, newidTag, cfMtag);
		      } else {
			throw(PixConnectivityExc(PixConnectivityExc::FATAL, "CANTOPENCFG", "Cannot open configuration db: "+e.getDescr()));
		      }
		    }
		    //m_conn->setCfgRev(it->second);
		    tmpMod->saveConfig(m_conn, newRev, true);
		    DbServer->setPending("OnDB",domainName, cfMtag, it->first,newRev);	
		    step++;
		  } catch (PixConnectivityExc &ef) { 
		    ers::error(PixLib::pix::daq (ERS_HERE,applName, 
					 "PixConnectivity Exception caught while saving module "+ it->first+" : "+ef.getDescr())); 
		  }
		}
		delete tmpMod;
	      }
	    }
	    if ( newmod.size()!=0 ) {
	      std::ostringstream message;
	      message <<  newmod.size()-count << " PixModule configuration have been written in permanent database";
	      ers::info(PixLib::pix::daq (ERS_HERE,applName, message.str()));
	    }
	  }
	}
      }
      //mem(0);
      //time=
              (void)gettimeofday(&t1,NULL);
      std::cout<< std::endl << "Time to write modules on disk is:  " <<t1.tv_sec - t0.tv_sec+ 1e-6*(t1.tv_usec-t0.tv_usec) <<std::endl << std::endl;      
      kill(getpid(), SIGTERM);
    }
    waitpid(mpid, &status,0);
  } while(!stop);

  return EXIT_SUCCESS;

}
