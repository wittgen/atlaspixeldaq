#include <iostream>
#include <unistd.h>
#include <csignal>

#include <owl/semaphore.h>
#include <ipc/core.h>

#include <pmg/pmg_initSync.h>

#include "PixDbServer/PixDbServerInterface.h"
#include "PixUtilities/PixMessages.h"

using namespace PixLib;

OWLSemaphore* sem=nullptr;

void signal_handler(int signal) {
  if(sem){
    sem->post();
  }
}


void mem(int del) {
  pid_t pid = getpid();
  std::ostringstream os;
  os << "/proc/" << pid << "/status";
  std::cout << "Mem Info (" << os.str() << "):" << std::endl;
  std::ifstream st(os.str().c_str());
  while (!st.eof()) {
    std::string line;
    std::getline(st, line);
    if (line.substr(0,2) == "Vm") {
      std::cout << "  " << line << std::endl;
    }
  }
  sleep(del);
}

int main(int argc, char** argv) {

  if (argc != 3 )  {
    std::cout << std::endl << " USAGE: ./PixDbServer [partitionName] [serverName] "<<std::endl;
    exit(-1);
  }
 
  std::string ipcPartitionName = argv[1];
  std::string serverName = argv[2];

  sleep(5); // really useful???
  
  // Start IPCCore    
  IPCCore::init(argc, argv);
  IPCPartition *ipcPartition;
  
  try {
    ipcPartition = new IPCPartition(ipcPartitionName);
  } catch(...) {
    std::cout << "PixDbServer: FATAL!! problems while connecting to the partition" << std::endl;
    return -1;
  }
  std::cout << "Successfully connected to partition " <<  ipcPartitionName << std::endl << std::endl;
 
   std::string applName = "PixDbServer";
 
  bool ready=false;
  //PixDbServerInterface *DbServer;
  try {
    //DbServer=
            (void)new PixDbServerInterface(ipcPartition,serverName,PixDbServerInterface::SERVER, ready);
  } catch(...) {
    ers::fatal(PixLib::pix::daq (ERS_HERE,applName,"Exception caught while creating DbServer"));
    return -1;
  }
  if (!ready) {
    ers::fatal(PixLib::pix::daq (ERS_HERE,applName,"DbServer "+serverName+" has not been created"));
    return -1;
  }
  ers::info(PixLib::pix::daq (ERS_HERE,applName,"Successfully created DbServer with name"+serverName));
  ers::info(PixLib::pix::daq (ERS_HERE,applName," ........ waiting for the enabling signal from DbServerLoader ...."));
 
  pmg_initSync();
  
  // Install the signal handler
  std::signal(SIGINT   , signal_handler);
  std::signal(SIGQUIT  , signal_handler);
  std::signal(SIGILL   , signal_handler);
  std::signal(SIGTRAP  , signal_handler);
  std::signal(SIGABRT  , signal_handler);
  std::signal(SIGIOT   , signal_handler);
  std::signal(SIGBUS   , signal_handler);
  std::signal(SIGFPE   , signal_handler);
  std::signal(SIGKILL  , signal_handler);
  std::signal(SIGSEGV  , signal_handler);
  std::signal(SIGPIPE  , signal_handler);
  std::signal(SIGTERM  , signal_handler);
  std::signal(SIGSTKFLT, signal_handler);
  std::signal(SIGSYS   , signal_handler);

  sem = new OWLSemaphore();
  sem->wait();
  delete sem;

  return EXIT_SUCCESS;
}
