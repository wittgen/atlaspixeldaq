#include <iostream>
#include <signal.h>
#include <csignal>
#include <sys/time.h>
#include <ipc/object.h>
#include <pmg/pmg_initSync.h>

#include <sys/wait.h>

#include "PixConnectivity/PartitionConnectivity.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "PixConnectivity/PixConnectivity.h"
#include "PixConnectivity/PixConfigDb.h"
#include "PixConnectivity/PixDisable.h"
#include "PixTrigController/TimPixTrigController.h"
#include "PixModule/PixModule.h"
#include "PixBoc/PixBoc.h"
#include "PixUtilities/PixMessages.h"
#include "PixUtilities/PixISManager.h"

#include "RCCVmeInterface.h"

#include "DFThreads/DFThread.h"

using namespace PixLib;

int errcount=-1;
int forkcount=-1;
bool connc = false;
bool confc = false;

std::string ipcPartitionName;
std::string serverName;
std::string   idTag; 
std::string connTag;
std::string  cfgTag;
std::string cfgMTag;
std::string cfgMTag2;
std::string domainName;
std::string applName;

PixConnectivity *conn;
PixConfigDb* conf;
IPCPartition *ipcPartition;
PixDbServerInterface *DbServer;
std::string IsServerName     = "PixelRunParams";
PixISManager* isManager = NULL; 

OWLSemaphore* sem=nullptr;

void signal_handler(int signal) {
  if(sem){
    sem->post();
  }
}

class ConfigurationThread : public DFThread {
  public:
    ConfigurationThread() {};
    virtual ~ConfigurationThread() {};
    virtual void run();
    virtual void cleanup() {};
};


void mem(int del) {
  pid_t pid = getpid();
  std::ostringstream os;
  os << "/proc/" << pid << "/status";
  std::cout << "Mem Info (" << os.str() << "):" << std::endl;
  std::ifstream st(os.str().c_str());
  while (!st.eof()) {
    std::string line;
    std::getline(st, line);
    if (line.substr(0,2) == "Vm") {
      std::cout << "  " << line << std::endl;
    }
  }
  sleep(del);
}

std::string getTime(int i) {
  char t_s[100];
  time_t t;
  tm t_m;
  t = i;
  localtime_r(&t, &t_m);
  strftime(t_s,100,"%Y-%m-%d %H:%M:%S",&t_m);
  std::string date = t_s;

  return date;
}

std::string element(std::string str, int num, char sep) {
  size_t ip;
  int start = -1,end = -1;
  int nel = -1;
  bool spc = true;
  bool quote = false;                                                                                                                                        
  for (ip=0;ip<str.size();ip++) {
    if (str[ip] == '"') quote = !quote;
    if (str[ip] == sep || (sep == 0 && (str[ip] == 9 || str[ip]==10 || str[ip] == 32))) {
      if (!spc && !quote) {
        spc = true;
        if (nel==num) end = ip-1;
      }
    } else {
      if (spc) {
        spc = false;
        nel++;
        if (nel==num) start = ip;
      }
    }
  }
  if (!spc && nel==num) end = str.size()-1;                                                                           
                                        
  if (start >= 0) {
    std::string c;
    std::string s = str.substr(start,end-start+1);
    for (ip=0;ip<s.size();ip++) {
      if (s[ip] != '"') c += s[ip];
    }
    return c;
  } else {
    return "";
  }
}

unsigned int getRev(std::string filename) {
  // std::cout << " filename is " << filename << std::endl;
  size_t pos = filename.find_last_of("_");
  if (pos != std::string::npos) {
    std::string sdate = filename.substr(pos+1);
    sdate = element(sdate, 0, '.');
    sdate = sdate.substr(0, sdate.size()-3);
    unsigned int date = 0;
    std::istringstream is(sdate);
    is >> date;
    std::cout << "->\t" << date << "\t" << filename << "\t(" << getTime(date) << ")" << std::endl;
    return date;
  } else {
    return 0;
  }
}

void connectToIpc(IPCPartition* &ipcPartitionArg, std::string ipcPartitionNameArg, PixDbServerInterface* &DbServerIf, std::string serverNameArg) {
  //  Create IPCPartition constructors
  std::cout << std::endl << "Starting partition " << ipcPartitionNameArg << std::endl;
  try {
    ipcPartitionArg = new IPCPartition(ipcPartitionNameArg);
  } catch(...) {
    std::cout << "PixDbServer: ERROR!! problems while connecting to the partition!" << std::endl;
  }
  bool ready=false;
  int nTry=0;
  std::cout << "Trying to connect to DbServer with name " << serverNameArg << "  ..... " << std::endl;
  do {
    sleep(1);
    DbServerIf=new  PixDbServerInterface(ipcPartitionArg, serverNameArg, PixDbServerInterface::CLIENT, ready);
    if(!ready) delete DbServerIf;
    else break;
    std::cout << " ..... attempt number: " << nTry << std::endl;
    nTry++;
  } while(nTry<10);  
  if(!ready) {
    std::cout << "Impossible to connect to DbServer " << serverNameArg << std::endl;
    exit(-1);
  }
  std::cout << "Succesfully connected to DbServer " << serverNameArg << std::endl;

  try {
    isManager = new PixISManager(ipcPartitionArg, IsServerName);
  }  
  catch(PixISManagerExc &e) {
    std::cout << "Exception caught in RunConfigEditor.cxx: " << e.getErrorLevel() << "-" << e.getISmanName() << "-" << e.getExcName() << std::endl; 
    return;
  }
  catch(...) { 
    std::cout <<"Unknown exception caught in RunConfigEditor.cxx while creating ISManager for IsServer " << IsServerName << std::endl; 
    return;
  }

}

bool checkCfg(std::string tag, std::string cfgType) {
  bool update = false;
  std::string modName;

  if (tag == "Base") return true;

  conf->cfgFromDbServ();
  std::vector<std::string> domlist = conf->listCfgTags(idTag);
  for (std::string it : domlist) {
    if (it == tag) update = true;
  }

  std::vector<std::string> dbsObj;
  if (update) {
    dbsObj = conf->listObjects(tag, idTag);
    ers::info(PixLib::pix::daq (ERS_HERE,applName,"Tag "+tag+" found in DbServer; check if updates are needed"));
  } else {
   ers::info(PixLib::pix::daq (ERS_HERE,applName,"Tag "+tag+" not found in DbServer; loading all"));
  }

  conf->cfgFromDb();
  std::vector<std::string> dbObj = conf->listObjects(tag, idTag);

  for (std::string obj : dbObj) {
    bool use = false;
    std::string objType = conf->getObjectType(obj, tag, idTag);
    if (objType.substr(objType.size()-4,4) == "_CFG") { 
      if (cfgType == "MOD" && objType == "MODULE_CFG") use = true;
      if (cfgType == "ROD" && objType == "RODBOC_CFG") use = true;
      if (cfgType == "ROD-OTH" && objType != "MODULE_CFG") use = true;
      if (cfgType == "OTH" && (objType != "MODULE_CFG" && objType != "RODBOC_CFG")) use = true;
      if (obj.substr(0,7) == "Disable" && objType == "DISABLE_CFG") use = false;
    }
    if (use) {
      std::vector<unsigned int> dbRev, dbsRev;
      dbRev = conf->listRevisions(obj, tag, idTag);
      unsigned int lrev=0;
      for (unsigned int rev : dbRev) {
	if (rev > lrev) lrev = rev;
      }
      std::ostringstream os;
      os << lrev;
      std::string srev = os.str();
      bool load = true;
      if (update) {
	if (std::find(dbsObj.begin(), dbsObj.end(), obj) != dbsObj.end()) {
          conf->cfgFromDbServ();
	  dbsRev = conf->listRevisions(obj, tag, idTag);
	  if (std::find(dbsRev.begin(), dbsRev.end(), lrev) != dbsRev.end()) {
	    ers::info(PixLib::pix::daq (ERS_HERE,applName,"Object "+obj+" "+objType+" rev "+srev+" found in DbServer"));
	    load = false;
	  } else {
	    ers::warning(PixLib::pix::daq (ERS_HERE,applName, "Object "+obj+" "+objType+" rev "+srev+" not found in DbServer; force loading"));
	  }
	} else {
	  ers::warning(PixLib::pix::daq (ERS_HERE,applName,"Object "+obj+" "+objType+" not found in DbServer; force loading"));
	}
      } else {
	ers::info(PixLib::pix::daq (ERS_HERE,applName, "Loading object "+obj+" "+objType));
      }
      if (load) {
	PixDbDomain::obj_id oid;
	oid.domain = "Configuration-"+idTag;
	oid.tag = tag; 
	oid.rev =lrev;
	oid.type = objType;
        oid.name = obj;
	DbServer->addLoadList(oid);
      }
    }
  }
  return true;
}

void loadConnectivity() {
  struct timeval t0,t1,t2;
  gettimeofday(&t0,NULL); 
  
  std::string msg;
  
  DbServer->setReadEnableRem(false);
  conn = new PixConnectivity(connTag, connTag, connTag, "Base", idTag, "Base", false);
  std::cout << "PixConnectivitly created, attempting to load ... \n";
  conn->loadConn();
  gettimeofday(&t1,NULL);
  std::ostringstream os;
  os << "Time to load connectivity from oracle is: " << t1.tv_sec-t0.tv_sec + 1e-6*(t1.tv_usec-t0.tv_usec);
  ers::log(PixLib::pix::daq (ERS_HERE,applName, os.str()));
  conn->dumpInDb(DbServer);
  DbServer->setReadEnableRem(true);

  ers::info(PixLib::pix::daq (ERS_HERE,applName, "Wait for DbServer to become ready"));
  DbServer->ready();
  ers::info(PixLib::pix::daq (ERS_HERE,applName,"DbServer is ready; start loading connectivity"));
  try {
    bool ok = false;
    do {
      sleep(1);
      std::vector<std::string> tags;
      DbServer->listTagsRem("Connectivity-"+idTag, tags);
      for (std::string const& tag : tags) {
	if (tag==connTag) ok = true;
      }
    } while (!ok);
    //sleep(10);
    conn = new PixConnectivity(DbServer, connTag, connTag, connTag, cfgTag, idTag, "", false);
    conn->loadConn();
    connc = true;
  }
  catch(PixConnectivityExc &e) {
    msg = "PixConnectivity Exception caught while loading connectivity from DbServer : "+e.getDescr();
    ers::fatal(PixLib::pix::daq (ERS_HERE,applName,msg));
    connc = false;
    return;
  }
  catch(...) {  
    msg =  "Unknown exception caught while loading connectivity from DbServer";
   ers::fatal(PixLib::pix::daq (ERS_HERE,applName,msg));
    connc = false;
    return;
  }
  connc=true;
  gettimeofday(&t2,NULL);
  std::ostringstream os2;
  os2 << " Time to load connectivity to db server: " << t2.tv_sec-t1.tv_sec + 1e-6*(t2.tv_usec-t1.tv_usec);
  ers::info(PixLib::pix::daq (ERS_HERE,applName,os2.str()));
}


void ConfigurationThread::run() {
  std::string msg;
  confc = false;
  try {
    conf = new PixConfigDb();  
    conf->setCfgTag(cfgTag, idTag);
    conf->setCfgTag(cfgMTag, idTag);
    if (cfgMTag2 != "") {
      conf->setCfgTag(cfgMTag2, idTag);
    }
    confc = true;
  }
  catch(PixConfigDbExc &e) {
    msg = "PixConfigDb Exception caught while loading configuration tables : " + std::string(e.what());
    ers::fatal(PixLib::pix::daq (ERS_HERE,applName,msg));
    confc = false;
    return;
  }
  catch (...) {
    msg =  "Unknown exception caught while loading configuration tables";
    ers::fatal(PixLib::pix::daq (ERS_HERE,applName,msg));
    confc = false;
    return;
  }
}

int main(int argc, char** argv) {
  struct timeval t0,t3;
  bool noPMG = false;

  if (argc == 8 || argc==7 || argc==6 || argc==5) { 
    ipcPartitionName = argv[1];
    if (ipcPartitionName[0] == '-') {
      noPMG = true;
      ipcPartitionName = ipcPartitionName.substr(1);
    }
    serverName = argv[2];
    idTag = argv[3]; 
    connTag = argv[4];
    domainName="Configuration-"+idTag;

    if (argc == 8)  {
      cfgMTag2 = argv[7];
      cfgMTag = argv[6];
      cfgTag = argv[5];
    } else if (argc == 7)  {
      cfgMTag2 = "";
      cfgMTag = argv[6];
      cfgTag = argv[5];
    } else if (argc==6) {
      cfgTag = argv[5];
      cfgMTag2 = "";
      cfgMTag = cfgTag;
    } else {
      cfgTag = "Base";
      cfgMTag2 = "";
      cfgMTag = "Base";
    }
  } else {
    std::cout << std::endl << " USAGE: ./PixDbServerLoaderConn [partitionName] [serverName] [id_tag] [tag_c] [tag_cf] [tag_cf_mod] [tag_cf_mod2]"<<std::endl;
    exit(-1);
  }

  //  Start IPCCore
  IPCCore::init(argc, argv);
  connectToIpc(ipcPartition, ipcPartitionName, DbServer, serverName);

  ConfigurationThread conft;
  conft.startExecution();
  loadConnectivity();
  
  while(conft.getCondition() != DFThread::TERMINATED)sleep(1);
  
  if (!connc) {
    std::string msg =  "Unable to load connectivity tag "+idTag+"/"+connTag;
    ers::fatal(PixLib::pix::daq (ERS_HERE,applName, msg));
  }

  gettimeofday(&t0,NULL);
  if (cfgTag != "Base") {
    // Enable DbSerber
    conf->openDbServer(DbServer);
    conf->addConnectivity(conn);
    
    // Fill the DBserver load list
    try {
      checkCfg(cfgTag, "ROD-OTH");
      ers::log(PixLib::pix::daq (ERS_HERE,applName, "Load list for tag "+cfgTag+" has been loaded into DbServer"));
    }
    catch (...) {
      ers::error(PixLib::pix::daq (ERS_HERE,applName, 
			   "Problems loading load list for "+cfgTag+" into DbServer"));
    }
    
    try {
      checkCfg(cfgMTag, "MOD");
      ers::log(PixLib::pix::daq (ERS_HERE,applName, 
			   "Load list for tag "+cfgMTag+" has been loaded into DbServer"));
    }
    catch (...) {
      ers::error(PixLib::pix::daq (ERS_HERE,applName, 
			   "Problems loading load list for "+cfgMTag+" into DbServer"));
    }
    if (cfgMTag2 != "") {
      try {
	checkCfg(cfgMTag2, "MOD");
	ers::log(PixLib::pix::daq (ERS_HERE,applName, 
			     "Load list for tag "+cfgMTag2+" has been loaded into DbServer"));
      }
      catch (...) {
	ers::error(PixLib::pix::daq (ERS_HERE,applName, 
			     "Problems loading load list for "+cfgMTag2+" into DbServer"));
      }
    }
    
    gettimeofday(&t3,NULL);
    std::ostringstream os;
    os << "Total time to fill the load list: " << t3.tv_sec-t0.tv_sec + 1e-6*(t3.tv_usec-t0.tv_usec);
    ers::info(PixLib::pix::daq (ERS_HERE,applName, os.str()));
    
    // Wait until the load list is empty
    size_t size = 999;
    do {
      sleep(1);
      std::vector<std::string>loadList;
      DbServer->listLoadList(loadList);
      size = loadList.size();
    } while (size > 0);
    ers::info(PixLib::pix::daq (ERS_HERE,applName, "DbServer load list is now empty"));
  }

  if (!noPMG) {
    std::cout << std::endl << "......  Program has finished: starting endless loop! " << std::endl << std::endl;
    pmg_initSync();
      // Install the signal handler
    std::signal(SIGINT   , signal_handler);
    std::signal(SIGQUIT  , signal_handler);
    std::signal(SIGILL   , signal_handler);
    std::signal(SIGTRAP  , signal_handler);
    std::signal(SIGABRT  , signal_handler);
    std::signal(SIGIOT   , signal_handler);
    std::signal(SIGBUS   , signal_handler);
    std::signal(SIGFPE   , signal_handler);
    std::signal(SIGKILL  , signal_handler);
    std::signal(SIGSEGV  , signal_handler);
    std::signal(SIGPIPE  , signal_handler);
    std::signal(SIGTERM  , signal_handler);
    std::signal(SIGSTKFLT, signal_handler);
    std::signal(SIGSYS   , signal_handler);

    sem = new OWLSemaphore();
    sem->wait();
    delete sem;
  }

return EXIT_SUCCESS;
}
