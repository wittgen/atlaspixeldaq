/*
 * Authors: 	JuanAn Garcia <jgarciap@cern.ch>
 *  MTS receiver to display messages from the PixelInfr in the DAQ slice
 */

#include <ipc/core.h>
#include <pmg/pmg_initSync.h>
#include <owl/semaphore.h>
#include <pmg/pmg_initSync.h>

#include "PixUtilities/PixMessages.h"

#include "ers/ers.h"

#include <csignal>

class PixelInfrReceiver : public ers::IssueReceiver {
  void receive( const ers::Issue & issue ) {
    if(issue.severity() == ers::Error )ers::error(issue);
    if(issue.severity() == ers::Fatal )ers::error(issue);//Drop FATAL from INFR as errors
  }
};


OWLSemaphore* sem=nullptr;

void signal_handler(int signal) {
  if(sem){
    sem->post();
  }
}

void help(){
  std::cout << "This program starts the MTS receiver to display errors from another partition" << std::endl;
  std::cout << "Usage: PixelInfrMTSReceiver PARTITION_NAME" << std::endl;
  exit(1);
}

int main(int argc, char** argv){

  if(argc != 2)help();

  std::string partitionName = argv[1];
  if(partitionName.find("PixelInfr") == std::string::npos){
    std::cout<<"Partition name "<< partitionName <<" is required to contain PixelInfr "<<std::endl;
    help();
  }

  IPCCore::init(argc, argv);
  pmg_initSync();

  PixelInfrReceiver *receiver = new PixelInfrReceiver;
  std::cout<<"Adding receiver for "<<partitionName<<std::endl;
    try {
      ers::StreamManager::instance().add_receiver( "mts", {partitionName, "*"}, receiver );
    } catch( ers::Issue & ex ) {
      ers::fatal( ex );
    }
    
  
  std::signal(SIGINT   , signal_handler);
  std::signal(SIGQUIT  , signal_handler);
  std::signal(SIGILL   , signal_handler);
  std::signal(SIGTRAP  , signal_handler);
  std::signal(SIGABRT  , signal_handler);
  std::signal(SIGIOT   , signal_handler);
  std::signal(SIGBUS   , signal_handler);
  std::signal(SIGFPE   , signal_handler);
  std::signal(SIGKILL  , signal_handler);
  std::signal(SIGSEGV  , signal_handler);
  std::signal(SIGPIPE  , signal_handler);
  std::signal(SIGTERM  , signal_handler);
  std::signal(SIGSTKFLT, signal_handler);
  std::signal(SIGSYS   , signal_handler);


  sem = new OWLSemaphore();
  sem->wait();
  delete sem;

    try {
      ers::StreamManager::instance().remove_receiver( receiver );
    } catch( ers::Issue & ex ) {
      ers::error( ex );
    }

  return EXIT_SUCCESS;
}

