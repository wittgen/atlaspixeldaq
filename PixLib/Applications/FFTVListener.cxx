//
// FFTV listener
// 
//
// MyPartition, MyServer, histoprovider, 
// path for bin files

#include <cstdlib>
#include <fstream>
#include <vector>

#include "boost/format.hpp"
#include "boost/lexical_cast.hpp"

#include "DaveModule.h"
#include "RCCVmeInterface.h"

#include <ipc/core.h>
#include <oh/OHRawProvider.h>

#include <is/infoT.h>
#include <is/inforeceiver.h>
#include <is/infodictionary.h>

#include <pmg/pmg_initSync.h> 

using SctPixelRod::VmeInterface;
using SctPixelRod::BaseException;

namespace Regs = DaveModuleRegisters;
struct callbackparams {
  DaveModule* d;
  std::string timname;
};

OWLSemaphore semaphore;
const double bc                  = 25.0e-9;  
const unsigned int perUp         = 20000;  //2 kHz
const unsigned int perLow        = 1000;   //40 kHz
const unsigned int perIgnoreLow  = 85;     //471 kHz 80-500 kHz 
const unsigned int vetoBUSY      = 239999; //ca. 6ms

class MyCommandListener : public OHCommandListener
{
public:
  void command ( const std::string & name, const std::string & cmd )
  {
    std::cout << " Command " << cmd << " received for the " << name << " histogram" << std::endl;
  }

  void command ( const std::string & cmd )
  {
    std::cout << " Command " << cmd << " received for all histograms" << std::endl;
    if ( cmd == "exit" )
      {
	//	  semaphore.post();
	//stop ipc server?
      }
  }
};

void dumpSRAMBlock(DaveModule &dave, std::ostream &os, uint32_t offset, int events, bool sixteenNot32) {
  int bytesPerEntry = sixteenNot32?2:4;

  for(uint32_t o=offset; o<offset + events*bytesPerEntry; o+=bytesPerEntry) {
    uint32_t addr = 0x800000+o;

    uint32_t val = dave.readReg(addr);
    if(!sixteenNot32) {
      val |= (dave.readReg(addr + 2) << 16);
    }

    uint32_t period;
    if(sixteenNot32) period = val & 0xfff;
    else period = val & 0xfffffff;

    uint32_t flags;
    if(sixteenNot32) flags = val >> 12;
    else flags = val >> 28;

    std::string flagString = "";

    if(flags == 0) flagString += " Busy off";
    if(flags & 1) flagString += " Trigger";
    if(flags & 2) flagString += " BCR";
    if(flags & 4) flagString += " ECR";
    if(flags & 8) flagString += " Busy on";

    os << (boost::format(sixteenNot32?"0x%08x: 0x%04x: %d%s\n":"0x%08x: 0x%08x: %d%s\n") % addr % val % period % flagString);
  }
}

void dumpSRAM(DaveModule &dave, bool sixteenNot32, std::string fname) {
  std::string filename(fname);

  std::ofstream ofs(filename.c_str(), std::ios::binary | std::ios::trunc);

  uint32_t sinkAddress = (dave.readReg(Regs::SSAD_HI) << 16) | dave.readReg(Regs::SSAD_LO);  
  uint32_t sinkCount = (dave.readReg(Regs::SSCOUNT_HI) << 16) | dave.readReg(Regs::SSCOUNT_LO);
  std::cout << (boost::format("Using address 0x%08x count 0x%08x\n") % sinkAddress % sinkCount);

  const uint32_t sramMaxOffset = 0x800000;

  int bytesPerEntry = sixteenNot32?2:4;

  if(sinkCount * bytesPerEntry > sinkAddress) {
 
    sinkCount = sramMaxOffset / bytesPerEntry;
    std::cout << (boost::format("SRAM wrapped, only readout the 0x%x stored events\n") % sinkCount);

    uint32_t secondBlockEvents = sinkAddress/bytesPerEntry;

    uint32_t firstBlockEvents = sinkCount - secondBlockEvents;
    uint32_t firstOffset = sramMaxOffset - (firstBlockEvents*bytesPerEntry);

    std::cout << (boost::format("Wrapped block offset 0x%08x counts 0x%08x+0x%08x\n") % firstOffset % firstBlockEvents % secondBlockEvents);

    dumpSRAMBlock(dave, ofs, firstOffset, firstBlockEvents, sixteenNot32);
    dumpSRAMBlock(dave, ofs, 0, secondBlockEvents, sixteenNot32);
  } else {
    uint32_t offset = sinkAddress - sinkCount * bytesPerEntry;

    std::cout << (boost::format("Normal block offset 0x%08x\n") % offset);

    dumpSRAMBlock(dave, ofs, offset, sinkCount, sixteenNot32);
  }

  ofs.close();

  std::cout << "SRAM saved to " << filename << std::endl;
}

void EnableSRAMrecording(DaveModule &dave) {
  int port;
 
  uint16_t SINK_ADDR_CLR = 1 << 9;
  uint16_t SINK_MODE     = 1 << 8;
  port = Regs::INT_ENA;
  int u = dave.readReg(port);

  dave.writeReg(port, u & (~SINK_MODE)); // Disable SRAM recording

  dave.writeReg(port, u | SINK_ADDR_CLR);     // SRAM Sink mode address clear
  dave.writeReg(port, u & (~SINK_ADDR_CLR));  // set to offset 0x000000

  dave.writeReg(port, u | SINK_MODE); // Enable SRAM recording
}

void DisableSRAMrecording(DaveModule &dave) {
  uint16_t SINK_MODE     = 1 << 8;
  int port = Regs::INT_ENA;
  int u = dave.readReg(port);
  dave.writeReg(port, u & (~SINK_MODE)); // Disable SRAM recording
}

void ConfigureDAVE(DaveModule &dave, int test) {
  int port;
  //
  //0x0187 with sync commands, 0x0181 - without
  if (test) {
    port = Regs::INT_ENA;     dave.writeReg(port, 0x2d87); printf("0x%3X: Internal:    %4d\n", port, dave.readReg(port));
    port = Regs::LEMOIN_ENA;  dave.writeReg(port, 0x0080); printf("0x%3X: Lemo Input:  %4d\n", port, dave.readReg(port)); 
    port = Regs::LEMOOUT_ENA; dave.writeReg(port, 0x0103); printf("0x%3X: Lemo Output:  %4d\n", port, dave.readReg(port)); //no ECR
  } else {
    //spying only, not generating (ATLAS)
    port = Regs::INT_ENA;     dave.writeReg(port, 0x0); printf("0x%3X: Internal:    %4d\n", port, dave.readReg(port));
    port = Regs::LEMOIN_ENA;  dave.writeReg(port, 0x0187); printf("0x%3X: Lemo Input:  %4d\n", port, dave.readReg(port));
    port = Regs::LEMOOUT_ENA; dave.writeReg(port, 0x0); printf("0x%3X: Lemo Output:  %4d\n", port, dave.readReg(port));
  }
  port = Regs::BCID_OFFSET; dave.writeReg(port, 0x00c2); printf("0x%3X: BCID offset:  %4d\n", port, dave.readReg(port)); //194

}

void extractTrigPeriods(int nvtrig, int tol, const std::vector<int> & chunk, std::vector<int> & fv ) {
  unsigned int seed;
  if (chunk.size()>1) {
    seed = chunk.at(chunk.size()-1);
    fv.push_back(seed);
    int tmp(seed);
    int vtrig(1);
    for (int ii=chunk.size()-2;ii>=0;ii--) {
      if (abs(chunk[ii]-tmp)<=tol) {
	if (chunk[ii]>=int(perLow)) {
	  fv.push_back(chunk[ii]);
	  vtrig++;
	}
	if (vtrig>nvtrig) {
	  std::cout << "VETO reconstructed, # of triggers: "<< vtrig <<std::endl;
	  std::cout << "# size: " <<chunk.size() << " seed: " << seed << " vtrig: " << vtrig <<std::endl;
	  break; 
	  // could go bit further...
	}
      } else vtrig--;
      tmp = chunk[ii];
    }
    std::cout << "Cannot reconstruct VETO, seed: "<< seed <<  " vtrig: " << vtrig  <<std::endl;
  } else std::cout << "Hmm, trigger chunk is too small (<2)..." <<std::endl;
}

void analyseDump( IPCPartition partition, std::string isServerName, std::vector<int> & fv, std::string fname, std::string timname ) {
  ISInfoDictionary dict(partition);
  std::string fftvthrname = isServerName + std::string(".")+ timname + std::string("/FFTVmatchThreshold");
  std::string fftvtolname = isServerName + std::string(".")+ timname + std::string("/FFTVmatchTolerance");
  std::string fftvemergthrname = isServerName + std::string(".")+ timname + std::string("/FFTVemergencyThreshold");
  //FIXME if not available
  ISInfoInt tol;
  dict.getValue(fftvtolname,tol);
  ISInfoInt nftrig;
  dict.getValue(fftvthrname,nftrig);
  ISInfoInt nvtrig;
  dict.getValue(fftvemergthrname,nvtrig);

  std::ifstream fin(fname);
  int linenum = 0;
  unsigned int totntrig = 0;
  unsigned int totaltime = 0;
  uint16_t value,flags=0,period=0;
  int nvetoes(0);
  vector<int> chunk;
  unsigned int dt(0);
  bool busyWasOn(false);
  bool debug(false);
  std::string flagString;
  unsigned int tbusy(0);  
  bool longerBUSYdueToECRBUSY(false);
 
  while(fin) {
    // copied from DAVEcode

    char line[64];
    fin.getline(line, 64);

    if(!fin) break;

    if(line[10] != ':') {
      if (debug) std::cout << "Parse error, line " << linenum << std::endl;
      return;
    }

    if(line[22] == ':') {
      if (debug) std::cout << "Parse error, expecting 16-bit data on line " << linenum << std::endl;
      return;
    }

    if(line[18] != ':') {
      if (debug) std::cout << "Parse error, line " << linenum << std::endl;
      return;
    }

    value = strtoul(&line[14], NULL, 16);
    flags = value >> 12;
    period = value & 0xfff;

    totaltime+=period;
    totaltime+=1;
  
    // start from the first trigger
    //
    if (totntrig>0) {
      dt+=period;
      if (debug) {
	flagString="";
	if(flags == 0) flagString += " Busy off";
	if(flags & 1) flagString += " Trigger";
	if(flags & 2) flagString += " BCR";
	if(flags & 4) flagString += " ECR";
	if(flags & 8) flagString += " Busy on";
	std::cout << flagString << " " << period <<std::endl;
      }

      if (flags&1 && (dt>=perIgnoreLow && dt<=perUp)) {
	if (debug) std::cout << "next taken trigger, distance "<< dt << std::endl;
	chunk.push_back(dt);
	dt=0;
      } else if (flags&1 && dt>perUp) {
	if (debug) std::cout << "ignored large trigger, distance "<< dt << std::endl;
	dt=0;
      } else dt+=1;

      // calc BUSY length tbusy
      //
      if ((flags&8 || flags==0) && busyWasOn) {
	if (flags&4) longerBUSYdueToECRBUSY=true;
	tbusy+=period;
	if (flags!=0) tbusy+=1;
      }
      if (flags&8) busyWasOn=true;
      else busyWasOn=false;

      if (flags==0) {
	if (debug) std::cout << "BUSY OFF, length: " << tbusy << std::endl;
	if ( (tbusy==vetoBUSY || (tbusy>=vetoBUSY && longerBUSYdueToECRBUSY)) ||
	     (tbusy==(vetoBUSY+1) || (tbusy>=(vetoBUSY+1) && longerBUSYdueToECRBUSY)) || 
	     (tbusy==(vetoBUSY-1) || (tbusy>=(vetoBUSY-1) && longerBUSYdueToECRBUSY)) ) {
	  nvetoes++;
	  extractTrigPeriods(nvtrig, tol, chunk,fv);
	  chunk.clear();
	} 
	tbusy = 0;
	dt    = 0;
      }
    }

    if(flags & 1) totntrig++;
    linenum++;
  }
  //if still BUSY at the end of the buffer => last veto
  //
  if (flags&8) {
    if (debug) std::cout << "BUSY ON at the dump end for "<< tbusy*bc <<"[s]" << std::endl;
    nvetoes++;
    extractTrigPeriods(nvtrig, tol, chunk,fv);
    chunk.clear();
  }
  if (debug) {
    std::cout << "meas time: "<< totaltime*bc << "[s], tot num trig found: " << totntrig 
	      << " nvetoes: " << nvetoes << " bad trigger size: "<< fv.size() <<std::endl;
  }
}

void publishHisto( IPCPartition partition, std::string server_name, std::vector<int> fv ) {

  server_name = "Histogramming";
  std::string histoprovider_name = "FFTVListener";

  bool valid;
  valid = partition.isValid();
  if (valid) {
    valid = partition.isObjectValid<is::repository>(server_name);
    if (valid) {
      std::cout << "INFO: "<< histoprovider_name << " has valid parition and oh server"<<std::endl;
    } else {
      std::cerr << "INFO: Provider: " << histoprovider_name << " --- m_ohServerName seems invalid, name is: " << server_name << std::endl;
    }
  } else {
    std::cerr << "INFO: Provider: " << histoprovider_name << " --- m_ipcPartition is not a valid partition, name is: " << std::endl;
  }

  if (!valid) return;
  //histo parameters
  //
  float hmin = 0.0;
  //float hmax = 40250; //20200.0;
  int nbins  = 161;//101;
  float step = 250;//200.0;

  std::string title = "vetoed fixed frequencies";
  float* data = new float[nbins];
  float elow(hmin);
  double freq;
  for (int i=0; i<nbins; i++) {
    data[i] = 0;
    for (unsigned int ii=0;ii<fv.size();ii++) {
      if (fv[ii]!=0) {
	freq=1.0/(fv[ii]*bc);
	if (elow<=freq && (elow+step)>freq) data[i]++;
      } else {
	//std::cout << "Warning: zero period found"<< std::endl;
      }
    }
    elow+=step;
  }
  float* errors = 0;


  OHRawProvider<> * raw = 0;
  MyCommandListener lst;
  try
    {
      raw = new OHRawProvider<>( partition, server_name, histoprovider_name, &lst );
    }
  catch( daq::oh::Exception & ex )
    {
      ers::fatal( ex );
    }

  std::vector<std::pair<std::string,std::string> > annotations;
  annotations.push_back( std::make_pair("FFTVListener::publishHisto", "RawProvider publish") );
  raw -> publish( "FFTV Spectra", title.c_str(),
		  OHAxis("frequency [Hz]", nbins, hmin, step),
		  data,
		  errors,
		  false,
		  -1, /*tag or update ?*/
		  annotations );
  delete[] data;
}

void publishVar(ISInfoDictionary infoDict, std::string name, int value)
{
  ISInfoInt val(value);
  if(infoDict.contains(name)){
    infoDict.update(name,val);
  }else{
    infoDict.insert(name,val);
  }
}


void callback(ISCallbackInfo * isc){
  //  if (isc->reason() == ISInfo::Deleted) {
  //  semaphore.post();
  //  return;
  //}
  ISInfoInt isi;
  isc->value(isi);
  int FFTVEmergencyFlag = isi;
  std::cout << "INFO: Got Callback, state of bEmergency flag: "<< FFTVEmergencyFlag << std::endl;
  if (FFTVEmergencyFlag==1 && isc->reason() != ISInfo::Deleted) {
    callbackparams* params = (static_cast<callbackparams*>(isc->parameter()));
    DisableSRAMrecording(*(params->d));
    OWLTime ot = isc->time();
    std::string tstamp = boost::lexical_cast<std::string>(ot.day())
      + boost::lexical_cast<std::string>(ot.month())
      + boost::lexical_cast<std::string>(ot.year())
      + std::string("_")
      + boost::lexical_cast<std::string>(ot.hour())
      + std::string("_")
      + boost::lexical_cast<std::string>(ot.min())
      + std::string("_")
      + boost::lexical_cast<std::string>(ot.sec());
    //FIXME LOG PATH
    std::string fname;
    if (getenv("TDAQ_LOGS_PATH")) fname = getenv("TDAQ_LOGS_PATH")+std::string("/Dave/"); 
    fname += (std::string("DaveSRAM_")+tstamp+std::string(".bin"));
    dumpSRAM(*(params->d), true, fname);

    std::vector<int> fv;
    analyseDump(isc->partition(),isc->serverName(),fv, fname, params->timname);
    if (fv.size()) publishHisto(isc->partition(), isc->serverName(), fv);
    else std::cout << "No vetoes has been found." << std::endl;

    EnableSRAMrecording(*(params->d));
    ISInfoDictionary dict(isc->partition());
    std::string varnameFFTVproc  = isc->serverName() + std::string(".") + params->timname + std::string("/FFTVdumpProcessing");
    publishVar(dict,varnameFFTVproc,0);
  }
}

int main(int argc, char *argv[]) {
  std::string timname;
  int test(0);
  if (argc<2) {
    std::cout<< "Please provide TIM connectivity name (e.g. TIM_I1), exiting.."<<std::endl;
    std::cout<< "additionally you can enable test mode with the 2nd argument (1)." << std::endl;
    return 0;
  } else timname = argv[1];
  if  (argc==3) test = atoi(argv[2]);
  IPCCore::init( argc, argv );
  std::string partition_name = "PixelInfr";
  if(getenv("PIX_PART_INFR_NAME")) partition_name = getenv("PIX_PART_INFR_NAME");
  else if(getenv("TDAQ_PARTITION_INFRASTRUCTURE")) partition_name = getenv("TDAQ_PARTITION_INFRASTRUCTURE");
  std::string server_name = "RunParams";
  IPCPartition partition( partition_name );
  ISInfoReceiver rec(partition);
  std::string varname("FFTVemergencyState");
  std::string fullvarname = server_name +  std::string(".") + timname + std::string("/")  + varname;
  std::cout<< "INFO: FFTVListener: will listen to the variable: " << fullvarname <<std::endl;

  //create DAVE, make sure it is saving data into SRAM
  SctPixelRod::RCCVmeInterface vme;
  uint32_t address = 0x11;
  DaveModule d(address << 24 , 0x01000000, vme);
  ConfigureDAVE(d,test);
  EnableSRAMrecording(d);

  pmg_initSync();
  callbackparams params = { &d, timname };
  rec.subscribe(fullvarname, callback, (void *)&params);
  semaphore.wait();
  //
  // blocking the thread, unblocking is done by deleting
  // the FFTVemergencyState variable
}
