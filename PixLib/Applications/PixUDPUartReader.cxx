/*
 * PixUDPUartReader
 * Author: oldrich.kepka@cern.ch
 */

#include "PixUDPUartReader/UDPUartReaderManager.h"

#include <cmdl/cmdargs.h>
#include <is/infodictionary.h>

#include <pmg/pmg_initSync.h>

#include <oh/OHRawProvider.h>

#include "csignal"

UDPUartReaderManager * uartreaderPtr = nullptr;

// capture kill signal
void signal_handler(int signal) {

    std::cout << "SIGNAL captured " << signal << std::endl;

    if(uartreaderPtr) {
        uartreaderPtr-> stop();
        uartreaderPtr = nullptr;
    }
}


void usage(const char *programName) {
    std::cout << "PixUDPUartReader -c[--crate] crateID -p[--partition] partition -d[--dbsrv] dbsrv_name"
              << "                 -t[--idTag] idTag -o[--connTag] connTag -g[--cfgTag] cfgTag -m[--cfgMTag] cfgMTag\n";
    std::cout << "crateID  :  Crate name, e.g. ROD_CRATE_3 in SR1. \n" << "\n";
}



int main(int argc, char** argv) {


    IPCCore::init( argc, argv );

    // Get parameters from the command line
    CmdArgStr crate_name	( 'c', "crate", "crate_name", "Crate name to log", CmdArg::isREQ );
    CmdArgStr partition_name( 'p', "partition", "partition_name", "Partition name", CmdArg::isREQ );
    CmdArgStr dbsrv_name	( 'd', "dbsrv", "dbsrv_name", "DBserver name" );
    CmdArgStr idTag			( 't', "idTag", "idTag", "ID tag for connectivity loading" );
    CmdArgStr connTag		( 'o', "connTag", "connTag", "Connectivity tag for connectivity loading" );
    CmdArgStr cfgTag		( 'g', "cfgTag", "cfgTag", "Config. tag for connectivity loading" );
    CmdArgStr cfgMTag		( 'm', "cfgMTag", "cfgMTag", "Module config. tag for connectivity loading" );

    CmdLine cmd( *argv, &crate_name, &partition_name,  &dbsrv_name, &idTag, &connTag, &cfgTag, &cfgMTag, NULL );

    CmdArgvIter arg_iter( --argc, ++argv );

    cmd.description( "Application for PixUDPUartReader" );
    cmd.parse(arg_iter);



    // signal init to PMG
    pmg_initSync();

    std::signal(SIGINT   , signal_handler);  
    std::signal(SIGQUIT  , signal_handler);  
    std::signal(SIGILL   , signal_handler);  
    std::signal(SIGTRAP  , signal_handler);  
    std::signal(SIGABRT  , signal_handler);  
    std::signal(SIGIOT   , signal_handler);  
    std::signal(SIGBUS   , signal_handler);  
    std::signal(SIGFPE   , signal_handler);  
    std::signal(SIGKILL  , signal_handler);  
    std::signal(SIGSEGV  , signal_handler);  
    std::signal(SIGPIPE  , signal_handler);  
    std::signal(SIGTERM  , signal_handler);  
    std::signal(SIGSTKFLT, signal_handler);  
    std::signal(SIGSYS   , signal_handler);  

    std::string crate = (const std::string) crate_name;
    try
    {
        using std::string;

// 2018/05/04
// Temporarly disable the UDPUartReaderManager completely
// to make restarting infrastructure easier. PixUDPUartApplication should
// go to ASENT immediately after the start
/*
        UDPUartReaderManager uartreader((std::string) partition_name, crate);

        uartreaderPtr = &uartreader;

        PixLib::PixConnectivity * conn = uartreader.initializePixConnectivity(
                    (string)partition_name, (string)dbsrv_name,
                    (string)idTag, (string)cfgTag, (string)connTag, (string)cfgMTag
                    );


        std::cout << "Initialize UDPUartReader\n";
        uartreader.initialize(conn);
        std::cout << "Starting run UDPUartReader\n";
        uartreader.run();
*/
        std::cout << "Stopping UDPUartReader\n";
    }

     
    catch( daq::oh::Exception & ex )
    {
        ers::fatal( ex );
    }




}
