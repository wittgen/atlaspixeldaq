#include <string>
#include <cmdl/cmdargs.h>
#include <ipc/core.h>
#include <pmg/pmg_initSync.h>

#include "PixUDPServer/PixUDPServer.h"


//Main function
int main (int argc, char** argv) {
  
  IPCCore::init(argc,argv);
  CmdArgStr partition_name('p',"partition","partition_name", "Partition name", CmdArg::isREQ);
  CmdArgInt workers       ('w',"workers","workers","Number of worker threads", CmdArg::isREQ);
  CmdArgInt debug       ('d',"debug","debug","Debug mode");
  CmdLine cmd(*argv, &partition_name,&workers,&debug,NULL);
  CmdArgvIter arg_iter(--argc,++argv);
    
  cmd.description( "Application for UDP Publisher");
  cmd.parse(arg_iter);
  
  
  //std::string infrPartName="PixelInfr";
  //if (getenv("PIX_PART_INFR_NAME")) infrPartName  = getenv("PIX_PART_INFR_NAME");
  //else if (getenv("TDAQ_PARTITION")) infrPartName = getenv("TDAQ_PARTITION"); 
  
  IPCPartition infrPart(partition_name);
  // signal init to PMG
  pmg_initSync();
  
  if (!infrPart.isValid())
    {
      std::cout<<"Partition "<<partition_name<<" does not exist"<<std::endl;
      if ( (const int) debug == 1)
	std::cout<<"In debug mode"<<std::endl;
      else
	return 1;
    }
  
  
  
  PixUDPServer UDPServer((const std::string)partition_name, (const int) workers);
  
  
  UDPServer.run();
  
  
  return 0;
}
