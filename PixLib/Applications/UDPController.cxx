#include "../PixThreads/UDPSock.h"
#include "../PixUDPServer/UDPPacket.h"
#include "QSTalkTask.h"


void DumpQSInfo(const QSTalkTask& QSTT);

int main(int argc, char** argv)
{
 
  if (argc<4)
    {
      std::cout<<"Usage UDPController RODIP RECVPORT QSNETWORKMODE QSMODE"<<std::endl;
    }
  
  char* ip = argv[1];

  std::cout<<"ip="<<ip<<std::endl;
  uint16_t port = atoi(argv[2]);
  uint8_t qsnmode = atoi(argv[3]);
  uint8_t qsmode  = atoi(argv[4]);
  
  UDPSock* sock = new UDPSock(ip,port);
  
  
  
  sock->add8(qsnmode);
  sock->sendPort();
  sock->add8(qsmode);
  
  int n = sock->sendBuffer();

  //clear the buffer.
  sock->clearBuffer();

  if (qsnmode==QSTalkTask::QSTT_INFO) {
    
    
    QSTalkTask dummyQSTT;
    n = sock->receiveData();
    
    if (n<0)
      {
	int errsv = errno;
	if (errsv == EWOULDBLOCK)
	  std::cout<<"UDP timeout reached.. Host request lost or PPC is down..."<<std::endl;
	std::cout<<"VALUE OF ERRNO:"<<errno<<std::endl;
      }
    

    dummyQSTT.result.deserialize(sock->getBuffer());
    DumpQSInfo(dummyQSTT);
  }
  
  
  delete sock;
}


void DumpQSInfo(const QSTalkTask& QSTT) {
  
  
  UDPPacket::printElement("slaveBusyCounters Slave A");
  std::cout<<std::endl;
  
  for (uint8_t i = 0 ; i<32 ; i++)
    UDPPacket::printElement((unsigned int)QSTT.result.m_qsMonInfos.slaveBusyCounters[i]);
  std::cout<<std::endl;
  
  UDPPacket::printElement("slaveBusyCounters Slave B");
  std::cout<<std::endl;
  
  for (uint8_t i = 32 ; i<64 ; i++)
    UDPPacket::printElement((unsigned int)QSTT.result.m_qsMonInfos.slaveBusyCounters[i]);
  std::cout<<std::endl;
  
  UDPPacket::printElement("nCFG rod Busy");
  std::cout<<std::endl;
  
  for (uint8_t i = 0 ; i<32 ; i++)
    UDPPacket::printElement((unsigned int)QSTT.result.m_qsMonInfos.nCFG_rodBusy[i]);
  std::cout<<std::endl;

  UDPPacket::printElement("nCFG timeout");
  std::cout<<std::endl;
  
  for (uint8_t i = 0 ; i<32 ; i++)
    UDPPacket::printElement((unsigned int)QSTT.result.m_qsMonInfos.nCFG_timeout[i]);
  std::cout<<std::endl;


  UDPPacket::printElement("link TrigCounters");
  std::cout<<std::endl;
  
  for (uint8_t i = 0 ; i<32 ; i++)
    UDPPacket::printElement((unsigned int)QSTT.result.m_qsMonInfos.linkTrigCounters[i]);
  std::cout<<std::endl;

  UDPPacket::printElement("link OccCounters");
  std::cout<<std::endl;
  
  for (uint8_t i = 0 ; i<32 ; i++)
    UDPPacket::printElement((unsigned int)QSTT.result.m_qsMonInfos.linkOccCounters[i]);
  std::cout<<std::endl;

  UDPPacket::printElement("tot Desync Counters");
  std::cout<<std::endl;
  
  for (uint8_t i = 0 ; i<32 ; i++)
    UDPPacket::printElement((unsigned int)QSTT.result.m_qsMonInfos.totDesyncCounters[i]);
  std::cout<<std::endl;

  UDPPacket::printElement("timeout Counters");
  std::cout<<std::endl;
  
  for (uint8_t i = 0 ; i<32 ; i++)
    UDPPacket::printElement((unsigned int)QSTT.result.m_qsMonInfos.timeoutCounters[i]);
  std::cout<<std::endl;

  UDPPacket::printElement("BOC decoding error Counters");
  std::cout<<std::endl;
  
  for (uint8_t i = 0 ; i<32 ; i++)
    UDPPacket::printElement((unsigned int)QSTT.result.m_qsMonInfos.decodingErrorBOC[i]);
  std::cout<<std::endl;

  UDPPacket::printElement("BOC frame error Counters");
  std::cout<<std::endl;
  
  for (uint8_t i = 0 ; i<32 ; i++)
    UDPPacket::printElement((unsigned int)QSTT.result.m_qsMonInfos.frameErrorBOC[i]);
  std::cout<<std::endl;

  UDPPacket::printElement("BOC frame count Counters");
  std::cout<<std::endl;
  
  for (uint8_t i = 0 ; i<32 ; i++)
    UDPPacket::printElement((unsigned int)QSTT.result.m_qsMonInfos.frameCountBOC[i]);
  std::cout<<std::endl;

  UDPPacket::printElement("BOC mean occupancy Counters");
  std::cout<<std::endl;
  
  for (uint8_t i = 0 ; i<32 ; i++)
    UDPPacket::printElement((unsigned int)QSTT.result.m_qsMonInfos.meanOccBOC[i]);
  std::cout<<std::endl;

  // To do: add print out for s-link counters

  UDPPacket::printElement("masterBusy");
  UDPPacket::printElement("fmtLinkEnable");
  UDPPacket::printElement("QSDisabled");
  UDPPacket::printElement("waitingForECR");
  std::cout<<std::endl;
  
  UDPPacket::printElement((unsigned int)QSTT.result.m_qsMonInfos.masterBusy);
  UDPPacket::printElement((unsigned int)QSTT.result.m_qsMonInfos.fmtLinkEn);
  UDPPacket::printElement((unsigned int)QSTT.result.m_qsMonInfos.QSDisabled);
  UDPPacket::printElement((unsigned int)QSTT.result.m_qsMonInfos.waitingForECR);
  
  std::cout<<std::endl;
  
  
}

