/////////////////////////////////////////////////////////////////////
// ConfGroup.h 
// version 4.0
/////////////////////////////////////////////////////////////////////
//
// 26/09/99  Version 0.1 (PM)
//  7/10/99  Version 0.2 (PM)
//  4/11/99  Version 0.3 (PM)
// 25/07/01  Version 2.1.0 (PM)
// 26/03/04  Version 3.0 - Imported from SimPix (PM)
// 20/07/06  Version 4.0 - Interface with PixDbInterface (PM)
//

#ifndef PIXLIB_CONF_GROUP
#define PIXLIB_CONF_GROUP

#include <vector>
#include <map>
#include <string>
#include <typeinfo>
#include "Config/ConfObj.h"
#include "PixDbServer/PixDbServerInterface.h"

namespace PixLib {

class DbRecord;

class ConfGroup {
    friend class Config;
public:
  ConfGroup(std::string name) { m_groupName = name; };    // Constructor
  ~ConfGroup();                                           // Destructor

  void copy(const ConfGroup &grp);                        // Copy values to another ConfGrp
  template<class T>
    void addInt(const std::string &name, T& val, T def, const std::string &comm, bool vis) {
    m_param.push_back(new ConfInt(m_groupName+"_"+name, val, def, comm, vis));
  }

  void addFloat(const std::string &name, float &val, float def, const std::string &comm, bool vis);                             // Adds a float
  void addVector(const std::string &name, std::vector<int> &val, std::vector<int> def, const std::string &comm, bool vis);      // Adds a vector
  void addVector(const std::string &name, std::vector<unsigned int> &val, std::vector<unsigned int> def, const std::string &comm, bool vis);      // Adds a vector
  void addVector(const std::string &name, std::vector<float> &val, std::vector<float> def, const std::string &comm, bool vis);  // Adds a vector
  void addStrVect(const std::string &name, std::vector<std::string> &val, std::string def, const std::string &comm, bool vis);  // Adds a string vector
  void addMatrix(const std::string &name, ConfMask<unsigned short int> &val, unsigned short int def, const std::string &comm, bool vis); // Adds a matrix
  void addMatrix(const std::string &name, ConfMask<bool> &val, bool def, const std::string &comm, bool vis);          // Adds a matrix
 template<class T>
  void addList(const std::string &name, T& val, T def, std::map<std::string, int> symb, const std::string &comm, bool vis) {
    m_param.push_back(new ConfList(m_groupName+"_"+name, val, def, symb, comm, vis));
  }
  void addBool(const std::string &name, bool &val, bool def, const std::string &comm, bool vis);                                // Adds a bool
  void addBool(const std::string &name, bool &val, bool def, std::string y, std::string n, const std::string &comm, bool vis);  // Adds a bool
  void addString(const std::string &name, std::string &val, std::string def, const std::string &comm, bool vis);                // Adds a string
  void addLink(const std::string &name, std::string &val, std::string def, const std::string &comm, bool vis);                // Adds a link
  void addLinkRO(const std::string &name, std::string &val, std::string def, const std::string &comm, bool vis);                // Adds a link
  void addLink(const std::string &name, std::string remnam, std::string &val, std::string def, const std::string &comm, bool vis);  // Adds a link
  void addAlias(const std::string &name, std::string &val, std::string def, const std::string &comm, bool vis);  // Adds an alias

  void write(PixDbDomain::rev_content &cont);
  void read(PixDbDomain::rev_content &cont);
  void dump(std::ostream &out);                           // Dump group content

  const std::string & name() const { return m_groupName; };             // Returns group name
  void name(const std::string &nameArg)  { m_groupName=nameArg; };             // Returns group name
  const std::size_t size() const { return m_param.size(); };                  // Returns the size
  ConfObj &operator[](std::size_t i)  { return *m_param[i]; };
  const ConfObj &operator[](std::size_t i)  const { return *m_param[i]; };
  ConfObj &operator[] (const std::string &);

  using iterator=std::vector<ConfObj*>::iterator;
  using const_iterator=std::vector<ConfObj*>::const_iterator;
  iterator begin() { return m_param.begin(); }
  iterator end() { return m_param.end(); }
  const_iterator begin() const { return m_param.begin(); }
  const_iterator end() const { return m_param.end(); }
  const_iterator cbegin() const { return m_param.cbegin(); }
  const_iterator cend() const { return m_param.cend(); }
  void push_back(ConfObj* arg) {m_param.push_back(arg);}

 private:
  void reset();   // Set all parameters to default
  bool read(DbRecord *dbr);                               // Reads from DB
  bool write(DbRecord *dbr);                              // Writes to DB
  std::vector<ConfObj*> m_param;
  std::string m_groupName;
  static ConfObj trashConfObj;
  void  getObjForEditing(std::map<int, ConfObj*> &obj);
};

}

#endif
