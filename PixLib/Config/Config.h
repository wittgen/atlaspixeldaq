/////////////////////////////////////////////////////////////////////
// config.h 
// version 4.0
/////////////////////////////////////////////////////////////////////
//
// 26/09/99  Version 0.1 (PM)
//         - Read/write on file of config data
//         - Import/export to tcl of config data
//         - Support for int, bool, char*, enum
//  7/10/99  Version 0.2 (PM)
//  4/11/99  Version 0.3 (PM)
// 25/07/01  Version 2.1.0 (PM)
// 26/03/04  Version 3.0 - Imported from SimPix (PM)
// 20/07/06  Version 4.0 - Interface with PixDbInterface (PM)
//

#ifndef PIXLIB_CONFIG
#define PIXLIB_CONFIG

#include <vector>
#include <map>
#include "Config/ConfGroup.h"
#include "Config/ConfMask.h"

namespace PixLib {

class DbRecord;
class PixDbServerInterface;

static inline ConfGroup trashConfGroup("__TrashConfGroup__");
class Config {
public:
  Config(const std::string &name);                              // Constructor
  Config(const std::string &name, const std::string &type);            // Constructor
  Config(const Config &cfg);                             // Copy constructor
  virtual ~Config();                                     // Destructor

  Config &operator=(const Config& cfg);                  // Copy operator
  void copy(const Config &cfg);                          // Copy values to another Config 

  void addGroup(const  std::string &name);                                   // Adds a group
  //void addConfig(const std::string &name, bool own);                   // Adds a sub-config
  //void addConfig(const std::string &name, const std::string &type, bool own); // Adds a sub-config
  //void addConfig(Config *conf, bool own);                       // Adds a sub-config
  void addConfig(const std::string &name);                   // Adds a sub-config
  void addConfig(const std::string &name, const std::string &type); // Adds a sub-config
  void addConfig(Config *conf, bool takeOwnership = true);                       // Adds a sub-config
  void removeConfig(const std::string &name);                               // Removes a sub-config
  //This function does the same as addConfig(...) but will not check/delete the old config, but simply override it
  //This can be used, if the old config has been deleted by some other part of the code. Only use this function
  //if you're sure this is what you need
  void insertConfigNoDelete(Config *conf, bool takeOwnership = true);

  virtual bool read(DbRecord *dbr);                              // Reads from DB2
  virtual bool write(DbRecord *dbr);                             // Writes to DB2

  virtual void write(PixDbDomain::rev_content &content);  
  virtual void read(PixDbDomain::rev_content &content);  
  virtual bool write(PixDbServerInterface *DbServer, std::string domainName, std::string tag, std::string objName, std::string objType, std::string pendingTag="Generic_Temp") {
    unsigned int revision = 0xffffffff;
    return write(DbServer, domainName, tag, objName, objType, pendingTag, revision);  
  } 
  virtual bool write(PixDbServerInterface *DbServer, std::string domainName, std::string tag, std::string objName, std::string objType, std::string pendingTag, unsigned int& revision);  
  virtual bool read(PixDbServerInterface *DbServer, std::string domainName, std::string tag, std::string objName, unsigned int revision=0xffffffff);  
  virtual void subConfRead(std::map< std::string, std::string >fields, std::map<std::string, Config *>&subConf); 
  virtual void subConfWrite(std::map<std::string, Config *>&subConf); 

  unsigned int rev() const { return m_revision; };
  void rev(unsigned int revision)  {m_revision=revision;}
  const std::string& penTag() const { return m_pendingTag; };
  void penTag(const std::string &tag ) { m_pendingTag=tag; };
  const std::string& tag() const { return m_tag; };
  void tag(const std::string &t ) {  m_tag=t; };
  bool edit();
  void dump(std::ostream &out, std::string incipit="");  // Dumps config content
  void reset(); // Set all parameters to default
  const std::string &name() const { return m_confName; };       // Returns config name
  const std::string &type() const { return m_confType; };       // Returns config type
  void name(std::string n) { m_confName = n; };          // Returns config name
  void type(std::string t) { m_confType = t; };          // Returns config type
  const std::size_t size() const { return m_group.size(); };                 // Returns the size
  ConfGroup &operator[](std::size_t i)  { return *m_group[i]; };
  const ConfGroup &operator[](std::size_t i)  const{ return *m_group[i]; };

  template<typename String,
                  typename=typename std::enable_if<
                            std::is_convertible<String, std::string>::value
                >::type>
                ConfGroup &operator[] (String &name) const {
                for (unsigned int i=0; i<m_group.size(); i++) {
        if (m_group[i]->name() == name) {
        return *m_group[i];
    }
  }
    return trashConfGroup;
}

  unsigned int subConfigSize() const { return m_config.size(); }; // Returns the number of depending configs
  const Config &subConfig(unsigned int i) const;
  Config &subConfig(unsigned int i);
  Config &subConfig(std::string name);

  //private:
  std::vector<ConfGroup*> m_group;
  std::map<std::string, Config*> m_config;
  std::string m_confName;
  std::string m_confType;
  std::string m_pendingTag;  // Pending tag in DB server
  unsigned int m_revision;   // revision in cfg DB
  std::string m_tag;         // Tag in cg DB
  bool m_ownership;          // true if the config is owned by its parent
};
}

#endif

