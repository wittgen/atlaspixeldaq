/////////////////////////////////////////////////////////////////////
// ConfObj.h 
// version 4.0
/////////////////////////////////////////////////////////////////////
//
// 26/09/99  Version 0.1 (PM)
//  7/10/99  Version 0.2 (PM)
//  4/11/99  Version 0.3 (PM)
// 25/07/01  Version 2.1.0 (PM)
// 26/03/04  Version 3.0 - Imported from SimPix (PM)
// 20/07/06  Version 4.0 - Interface with PixDbInterface (PM)
//

#ifndef PIXLIB_CONF_CF_OBJ
#define PIXLIB_CONF_CF_OBJ

#ifdef WIN32
#pragma warning(disable: 4786)
#pragma warning(disable: 4800)
#endif

#include "BaseException.h"

#include <string>
#include <map>
#include <vector>
#include <typeinfo>
namespace PixA { class OnDemandConfGrp;}
namespace PixLib {
  
  class DbRecord;

  template<class T> class ConfMask;
  
  //! Config Exception class
  class ConfigExc : public SctPixelRod::BaseException{
  public:
    //! Constructor
    ConfigExc(std::string name) : BaseException(name) {}; 
    //! Destructor
    virtual ~ConfigExc() {};

    //! Dump the error
    virtual void dump(std::ostream &out) {
     out << "Config " << getDescriptor() << std::endl; 
    }
  private:
  };
  class ConfObj {
      friend class ConfList;
      friend class ConfInt;
      friend class ConfFloat;
      friend class ConfVector;
      friend class ConfStrVect;
      friend class ConfMatrix;
      friend class ConfBool;
      friend class ConfString;
      friend class ConfGroup;
      friend class ConfLink;
      friend class ConfAlias;
      friend class PixA::OnDemandConfGrp;
  public:
    ConfObj()=default;
    enum types { INT, FLOAT, VECTOR, STRVECT, MATRIX, LIST, BOOL, STRING, LINK, ALIAS, VOID };
    enum subtypes { S32, U32, S16, U16, S8, U8,  //INT
         V_INT, V_UINT, V_FLOAT, // VECTOR
         M_U16, M_U1, // MATRIX
         NONE
    };
     subtypes subtype() const { return m_subtype; };
  private:
    // Constructors distructors and copy
    ConfObj(const std::string &nam, const std::string &comm, bool vis, types typ);
  public:
    virtual ~ConfObj();

    virtual void copy(const ConfObj & /* obj */) { };

    // Methods
    virtual void reset() { };
    virtual bool read(DbRecord * /* dbr */) { return false; };
    virtual bool write(DbRecord * /* dbr */) { return false; };
    virtual void dump(std::ostream & /* out */) {};
    virtual std::string name() const { return m_name; };
    virtual bool visible() const { return m_visible; };
    virtual std::string comment() const { return m_comment; };
    virtual types type() const { return m_type; };

    virtual int& valueInt() { throw ConfigExc("Incompatible value type"); };
    virtual unsigned int& valueUInt() { throw ConfigExc("Incompatible value type"); };
    virtual float& valueFloat() { throw ConfigExc("Incompatible value type"); };
    virtual bool& valueBool() { throw ConfigExc("Incompatible value type"); };
    virtual std::string& valueString() { throw ConfigExc("Incompatible value type"); };
    virtual bool edit() { return false; };
    virtual void print() const { std::cout << "--"; };

    // Data
  private:
    std::string m_name;
    bool m_visible;
    std::string m_comment;
    types m_type;
    subtypes m_subtype;
  };


  class ConfInt : public ConfObj {
      friend class ConfList;
       friend class ConfGroup;
       friend class PixA::OnDemandConfGrp;
    // Constructors and distructors
  private:
    template<class T>
    ConfInt(const std::string &nam, T& val, T def, const std::string &comm, bool vis, types typ = INT):
      ConfObj(std::move(nam), std::move(comm), vis, typ) {
      m_value = (void *)&val;
      if constexpr( sizeof(T) == 4 && std::is_signed_v<T>) {
        m_subtype = S32;
        m_defS32 = def;
      }
      else if constexpr( sizeof(T) == 4 && std::is_unsigned_v<T>) {
        m_subtype = U32;
        m_defU32 = def;
      }
      else if constexpr( sizeof(T) == 2 && std::is_signed_v<T>) {
        m_subtype = S16;
        m_defS32 = def;
      }
      else if constexpr( sizeof(T) == 2 && std::is_unsigned_v<T>) {
        m_subtype = U16;
        m_defU32 = def;
      }  
      else if constexpr( sizeof(T) == 1 && std::is_signed_v<T>) {
        m_subtype = S8;
        m_defS32 = def;
      }
      else if constexpr( sizeof(T) == 1 && std::is_unsigned_v<T>) {
        m_subtype = U8;
        m_defU32 = def;
      }
      else if constexpr( std::is_enum_v<T> && sizeof(T) == 4 && std::is_signed_v<std::underlying_type_t<T>>) {
        m_subtype = S32;
        m_defS32 = def;
      }
      else if constexpr( std::is_enum_v<T> && sizeof(T) == 4 && std::is_unsigned_v<std::underlying_type_t<T>>) {

	static_assert( sizeof(T)== -1, "ConfInt: please make the passed enums use int (enum fooBar: int)" );
      }
      else {
	//This should fail for any types not caught before
	static_assert( sizeof(T)== -1, "Unsupported type for ConfInt" );
      }
    }
  public:
    virtual ~ConfInt();

    virtual void copy(const ConfObj &obj);
    
    // Methods
    virtual void reset();
    virtual bool read(DbRecord *dbr);
    virtual bool write(DbRecord *dbr);
    virtual void dump(std::ostream &out);
    virtual bool edit();
    virtual void print() const;
    int getValue() const;
    void setValue(int val);
    int &value() const { return *((int *)m_value); };
    int &valueS32() { return *((int *)m_value); };
    unsigned int &valueU32() { return *((unsigned int *)m_value); };
    short int &valueS16() { return *((short int *)m_value); };
    unsigned short int &valueU16() { return *((unsigned short int *)m_value); };
    char &valueS8() { return *((char *)m_value); };
    unsigned char &valueU8() { return *((unsigned char *)m_value); };

    virtual int& valueInt() { 
      if (m_subtype == S32) {
        return *((int *)m_value); 
      } else {
        throw ConfigExc("Incompatible value type");
      }
    };
    virtual unsigned int& valueUInt() {
      if (m_subtype == U32) {
        return *((unsigned int *)m_value);
      } else {
        throw ConfigExc("Incompatible value type");
      }
    };    
    // Data
    void *m_value;
    int m_defS32;
    unsigned int m_defU32;
    //short int m_defS16;
    //unsigned short int m_defU16;
    //char m_defS8;
    //unsigned char m_defU8;

  };
  
  class ConfFloat : public ConfObj {
       friend class ConfList;
       friend class ConfGroup;
       friend class PixA::OnDemandConfGrp;
  public:
    // Constructors and distructors
  private:
    ConfFloat(const std::string &nam, float &val, float def, const std::string &comm, bool vis);
  public:
    virtual ~ConfFloat();
    virtual void copy(const ConfObj &obj);
    
    // Methods
    virtual void reset() { m_value = m_defval; };
    virtual bool read(DbRecord *dbr);
    virtual bool write(DbRecord *dbr);
    virtual void dump(std::ostream &out);
    float &value() { return m_value; };
    virtual float& valueFloat() { return m_value; };
    virtual bool edit();
    virtual void print() const;
    
    // Data
    float &m_value;
    float m_defval;
  };
  
  class ConfVector : public ConfObj {
       friend class ConfList;
       friend class ConfGroup;
       friend class PixA::OnDemandConfGrp;
  public:
    
    // Constructors and distructors
  private:
    ConfVector(const std::string &nam, std::vector<int> &val, std::vector<int> def, const std::string &comm, bool vis);
    ConfVector(const std::string &nam, std::vector<unsigned int> &val, std::vector<unsigned int> def, const std::string &comm, bool vis);
    ConfVector(const std::string &nam, std::vector<float> &val, std::vector<float> def, const std::string &comm, bool vis);
  public:
    virtual ~ConfVector();
    virtual void copy(const ConfObj &obj);
    
    // Methods
    virtual void reset();
    virtual bool read(DbRecord *dbr);
    virtual bool write(DbRecord *dbr);
    virtual void dump(std::ostream &out);
    
    std::vector<int> &valueVInt() { return *((std::vector<int> *)m_value); };
    std::vector<unsigned int> &valueVUint() { return *((std::vector<unsigned int> *)m_value); };
    std::vector<float> &valueVFloat() { return *((std::vector<float> *)m_value); };

    
    // Data
    void *m_value;
    std::vector<int> m_defInt;
    std::vector<unsigned int> m_defUint;
    std::vector<float> m_defFloat;
  };
  
  class ConfStrVect : public ConfObj {
      friend class ConfList;
       friend class ConfGroup;
       friend class PixA::OnDemandConfGrp;
  public:
    // Constructors and distructors
  private:
      ConfStrVect(const std::string &nam, std::vector<std::string> &val, std::string def, const std::string &comm, bool vis);
  public:
    virtual ~ConfStrVect();
    virtual void copy(const ConfObj &obj);
    
    // Methods
    virtual void reset();
    virtual bool read(DbRecord *dbr);
    virtual bool write(DbRecord *dbr);
    virtual void dump(std::ostream &out);
    
    std::vector<std::string> &value() { return *((std::vector<std::string> *)m_value); };
        
    // Data
    void *m_value;
    std::string m_def;
  };
  
  class ConfMatrix : public ConfObj {
      friend class ConfList;
       friend class ConfGroup;
       friend class PixA::OnDemandConfGrp;
  public:
    
    // Constructors and distructors
  private:
    ConfMatrix(const std::string &nam, ConfMask<unsigned short int> &val, unsigned short int def, const std::string &comm, bool vis);
    ConfMatrix(const std::string &nam, ConfMask<bool> &val, bool def, const std::string &comm, bool vis);
  public:
    virtual ~ConfMatrix();
    virtual void copy(const ConfObj &obj);

    // Methods
    virtual void reset();
    virtual bool read(DbRecord *dbr);
    virtual bool write(DbRecord *dbr);
    virtual void dump(std::ostream &out);
    
    ConfMask<unsigned short int> &valueU16() { return *((ConfMask<unsigned short int> *)m_value); };
    ConfMask<bool> &valueU1() { return *((ConfMask<bool> *)m_value); };

    
    // Data
    void *m_value;
    unsigned short int m_defU16;
    bool m_defU1;
  };
  
  class ConfList : public ConfInt {
    friend class ConfGroup;
    friend class PixA::OnDemandConfGrp;
  private:
    template<class T>
    ConfList(const std::string &nam, T& val, T def, std::map<std::string, int> symb, const std::string &comm, bool vis) :
    ConfInt(std::move(nam), val, def, std::move(comm), vis, LIST) {
      m_symbols = symb;
    }
  public:
    // Constructors and distructors
    virtual ~ConfList() = default;
    virtual void copy(const ConfObj &obj);

    // Methods
    virtual bool read(DbRecord *dbr);
    virtual bool write(DbRecord *dbr);
    virtual void dump(std::ostream &out);
    virtual std::string sValue() const;
    virtual std::map<std::string, int>& symbols() { return m_symbols; };
    virtual bool edit();
    virtual void print() const;
     
    // Data
    std::map<std::string, int> m_symbols;
  };
  
  class ConfBool : public ConfObj {
       friend class ConfList;
       friend class ConfGroup;
       friend class PixA::OnDemandConfGrp;
  private:
    // Constructors and distructors
    ConfBool(const std::string &nam, bool &val, bool def, const std::string &comm, bool vis);
    ConfBool(const std::string &nam, bool &val, bool def, const std::string &y, const std::string &n, const std::string &comm, bool vis);
  public:
    virtual ~ConfBool();
    virtual void copy(const ConfObj &obj);
    
    // Methods
    virtual void reset() { m_value = m_defval; };
    virtual bool read(DbRecord *dbr);
    virtual bool write(DbRecord *dbr);
    virtual void dump(std::ostream &out);
    bool &value() { return m_value; };
    virtual bool& valueBool() { return m_value; };
    virtual bool edit();
    virtual void print() const;
    
    // Data
    bool &m_value;
    bool m_defval;
    std::string m_yes;
    std::string m_no;
  };

  class ConfString : public ConfObj {
       friend class ConfList;
       friend class ConfGroup;
       friend class PixA::OnDemandConfGrp;
  private:
    // Constructors and distructors
    ConfString(const std::string &nam, std::string &val, std::string def, const std::string &comm, bool vis);
  public:
    virtual ~ConfString();
    virtual void copy(const ConfObj &obj);
    
    // Methods
    virtual void reset() { m_value = m_defval; };
    virtual bool read(DbRecord *dbr);
    virtual bool write(DbRecord *dbr);
    virtual void dump(std::ostream &out);
    std::string &value() { return m_value; };
    virtual std::string& valueString() { return m_value; };
    virtual bool edit();
    virtual void print() const;
    
    // Data
    std::string &m_value;
    std::string m_defval;
  };

  class ConfLink : public ConfObj {
      friend class ConfList;
      friend class ConfGroup;
      friend class PixA::OnDemandConfGrp;
  private:
    // Constructors and distructors
    ConfLink(const std::string &nam, std::string &val, std::string def, const std::string &comm, bool vis, bool write=true);
    ConfLink(const std::string &nam, std::string remnam, std::string &val, std::string def, const std::string &comm, bool vis);
  public:
    virtual ~ConfLink();
    virtual void copy(const ConfObj &obj);
   
    // Methods
    virtual void reset() { m_value = m_defval; };
    virtual bool read(DbRecord *dbr);
    virtual bool write(DbRecord *dbr);
    virtual void dump(std::ostream &out);
    std::string &value() { return m_value; };
    virtual std::string& valueString() { return m_value; };
    
    // Data
    std::string &m_value;
    std::string m_defval;
    std::string m_remnam;
    bool m_write;
  };
  
  class ConfAlias : public ConfObj {
      friend class ConfList;
      friend class ConfGroup;
      friend class PixA::OnDemandConfGrp;
  private:
    // Constructors and distructors
    ConfAlias(const std::string &nam, std::string &val, std::string def, const std::string &comm, bool vis, bool write=true);
  public:
    virtual ~ConfAlias();
    virtual void copy(const ConfObj &obj);
   
    // Methods
    virtual void reset() { m_value = m_defval; };
    virtual bool read(DbRecord *dbr);
    virtual bool write(DbRecord *dbr);
    virtual void dump(std::ostream &out);
    std::string &value() { return m_value; };
    virtual std::string& valueString() { return m_value; };
    
    // Data
    std::string &m_value;
    std::string m_defval;
  };
  
}
#endif
