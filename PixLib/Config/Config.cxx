/////////////////////////////////////////////////////////////////////
// config.cxx 
// version 4.0
/////////////////////////////////////////////////////////////////////
//
// 26/09/99  Version 0.1 (PM)
//         - Read/write on file of config data
//         - Import/export to tcl of config data
//         - Support for int, bool, char*, enum
//  7/10/99  Version 0.2 (PM)
//  4/11/99  Version 0.3 (PM)
// 25/07/01  Version 2.1.0 (PM)
// 26/03/04  Version 3.0 - Imported from SimPix (PM)
// 20/07/06  Version 4.0 - Interface with PixDbInterface (PM)
//

#include "PixDbInterface/PixDbInterface.h"
#include "PixDbServer/PixDbServerInterface.h"
#include "Config/Config.h"

using namespace PixLib;

Config    trashConfig("__TrashConfig__");

Config::Config(const std::string &name) {
  std::size_t ipos = name.rfind("/");
  if (ipos != std::string::npos) {
    m_confName = name.substr(0,ipos);
    m_confType = name.substr(ipos+1);
  } else {
    m_confName = name;
    m_confType = name;
  }
  m_revision = 0;
  m_pendingTag = "TEST";
  m_ownership = false;
}

Config::Config(const std::string &name, const std::string &type) {
  m_confName = name; 
  m_confType = type; 
  m_revision = 0;
  m_pendingTag = "TEST";
  m_ownership = false;
}

Config::Config(const Config &cfg) {
  copy(cfg);
}

Config::~Config() {
  for (unsigned int i = 0; i < m_group.size(); i++) {
    delete m_group[i];
  }
  //m_group.clear();
  //std::map<std::string, Config*>::iterator it;
  for (auto it = m_config.begin(); it != m_config.end(); ++it) {
    if ((*it).second->m_ownership) delete (*it).second;
  }
  //m_config.clear();
}

Config &Config::operator=(const Config &cfg) {
  if (&cfg != this) {
    copy(cfg);
  }
  return *this;
}

void Config::copy(const Config &cfg) {
  for (unsigned int i = 0; i < m_group.size(); i++) {
    for (unsigned int j = 0; j< cfg.m_group.size(); j++) {
      if (m_group[i]->name() == cfg.m_group[j]->name()) {
	m_group[i]->copy(*cfg.m_group[j]);
      }
    }
  }
  std::map<std::string, Config*>::iterator it1;
  unsigned int i2;
  for (it1 = m_config.begin(); it1 != m_config.end(); ++it1) {
    for (i2 = 0; i2<cfg.subConfigSize(); i2++) {
      if (((*it1).second)->name() == cfg.subConfig(i2).name()) {
	((*it1).second)->copy(cfg.subConfig(i2));
      }
    }
  }
  m_confName = cfg.m_confName;
  m_confType = cfg.m_confType;
  m_revision = cfg.m_revision;
  m_pendingTag = cfg.m_pendingTag;
  m_ownership = cfg.m_ownership; 
  m_tag = cfg.m_tag;
}

void Config::addGroup(const std::string &name) {
  m_group.push_back(new ConfGroup(name));
}

void Config::addConfig(const std::string &name) {
  if (m_config.find(name) != m_config.end()) {
    if(m_config[name]->m_ownership) delete m_config[name];
  }
  m_config[name] = new Config(name);
  m_config[name]->m_ownership = true;
}

void Config::addConfig(const std::string &name, const std::string &type) {
  if (m_config.find(name) != m_config.end()) {
    if(m_config[name]->m_ownership) delete m_config[name];
  }
  m_config[name] = new Config(name, type);
  m_config[name]->m_ownership = true;
}

void Config::addConfig(Config *conf, bool takeOwnership) {
  std::string name = conf->name();
  if (m_config.find(name) != m_config.end()) {
    if (m_config[name] != conf) {
      if(m_config[name]->m_ownership) delete m_config[name];
    }
  }
  m_config[name] = conf;
  if(takeOwnership) m_config[name]->m_ownership = true;
}

void Config::insertConfigNoDelete(Config *conf, bool takeOwnership) {
  std::string const name = conf->name();
  m_config[name] = conf;
  if(takeOwnership) m_config[name]->m_ownership = true;
}

void Config::removeConfig(const std::string &name) {
  std::map<std::string, Config*>::iterator it = m_config.find(name);
  if (it != m_config.end()) {
    if (m_config[name]->m_ownership) delete m_config[name];
    m_config.erase(it);
  }
}
  
bool Config::read(DbRecord *dbr) {
  unsigned int i;
  bool ret = true;
  for(i = 0; i < m_group.size(); i++) {
    if(!m_group[i]->read(dbr)) ret = false;  
  }

  std::map<std::string, std::string> subRec;
  for (dbRecordIterator it = dbr->recordBegin(); it != dbr->recordEnd(); ++it) {
    subRec[(*it)->getName()] = (*it)->getClassName();
  }
  std::map<std::string, Config*>subConf;
  subConfRead(subRec, subConf);

  std::map<std::string, Config*>::iterator it;
  for (auto sc : subConf) {
    std::cout << "Read extra Config: " << sc.first << std::endl;
    dbRecordIterator subdbi = dbr->findRecord((sc.second)->name()+"/"+(sc.second)->type());
    if (subdbi != dbr->recordEnd()) {
      dbRecordIterator ri;
      ri = dbr->getDb()->DbProcess(subdbi, PixDb::DBREAD);
      if (!(sc.second)->read(*ri)) ret = false;
    } else {
      ret = false;
      std::cout << "PixLib::Config " << m_confName << " - Error reading sub-config " << (sc.second)->name() << endl;
    }
  }

  for (it = m_config.begin(); it != m_config.end(); ++it) {
    dbRecordIterator subdbi = dbr->findRecord(((*it).second)->name()+"/"+((*it).second)->type());
    if (subdbi != dbr->recordEnd()) {
      dbRecordIterator ri;
      ri = dbr->getDb()->DbProcess(subdbi, PixDb::DBREAD);
      if (!((*it).second)->read(*ri)) ret = false;
    } else {
      ret = false;
      std::cout << "PixLib::Config " << m_confName << " - Error reading sub-config " << ((*it).second)->name() << endl;
    }
  }
  return ret;
}

bool Config::write(DbRecord *dbr) {
  for (unsigned int i = 0; i < m_group.size(); i++) {
    if(!m_group[i]->write(dbr)) return false;
  }

  std::map<std::string, Config*>subConf;
  subConfWrite(subConf);

  dbRecordIterator subdbi;
  std::map<std::string, Config*>::iterator it;
  for (it = m_config.begin(); it != m_config.end(); ++it) {
    Config *cf = (*it).second;
    subdbi = dbr->findRecord(cf->name()+"/"+((*it).second)->type());
    if (subdbi == dbr->recordEnd()) {
      DbRecord *r = dbr->addRecord(cf->type(), cf->name());
      if (!cf->write(r)) return false;
    } else {
      subdbi = dbr->getDb()->DbProcess(subdbi, PixDb::DBREAD);
      if (!cf->write(*subdbi)) return false;
    }
  }
  return true;
}
  
bool Config::read(PixDbServerInterface *DbServer, std::string domainName, std::string tag, std::string objName, unsigned int revision) {
  if (DbServer != NULL) {
    PixDbDomain::rev_content cont;
    bool result;
    if (revision==0) result=DbServer->getObject(cont, domainName, tag, objName);
    else  result=DbServer->getObject(cont, domainName, tag, objName, revision);
    
    //DbServer->printRevContent(cont);  
    if (!result) return false; 
    rev(cont.revision);
    penTag(cont.pendingTag);
    m_tag = tag;

    for (unsigned int i = 0; i < m_group.size(); i++) {
      m_group[i]->read(cont);
      //DbServer->printRevContent(cont);  
    }

    std::map<std::string, std::string> subRec;
    std::map<std::string, Config*>subConf;
    subConfRead(subRec, subConf);
    // !!!!!! subConf ignored in DbServer read, to avoid interferece with the old read method

    std::map<std::string, Config*>::iterator it;
    for(it=m_config.begin(); it!=m_config.end(); it++) {
      result=(*it).second->read(DbServer, domainName, tag, objName+"|"+(*it).first, revision);
      if (!result) {
       	std::cout << "Config::read ERROR in reading subconfig " << (*it).first << std::endl;
	return false;
      }
    }
    return true;
  } else {
    // std::cout << "PixDbServerInterface pointer is NULL!!" << std::endl;
    return false;
  }
}

void Config::read(PixDbDomain::rev_content &content) {
  rev(content.revision);
  penTag(content.pendingTag);
  for (unsigned int i = 0; i < m_group.size(); i++) {
    m_group[i]->read(content);
    //DbServer->printRevContent(cont);  
  }
}

bool Config::write(PixDbServerInterface *DbServer, std::string domainName, std::string tag, std::string objName, std::string objType, std::string pendingTag, unsigned int &revision) {
  PixDbDomain::rev_content cont;
  bool result;
  if (pendingTag=="") pendingTag=penTag();
  else   penTag(pendingTag);
  cont.pendingTag=pendingTag;
  //std::cout << "Object " << objType << std::endl << std::endl;
  for (unsigned int i = 0; i < m_group.size(); i++) {
    //(std::cout << std::endl << "Calling write on group: " <<  m_group[i]->name() <<  std::endl; 
    m_group[i]->write(cont);
  }

  std::map<std::string, Config*>subConf;
  subConfWrite(subConf);

  std::map<std::string, Config*>::iterator it;
  //std::cout << "Object has " << m_config.size() << "  subconfig" <<std::endl;
  PixDbDomain::link_object link;
  std::string realType;
  for(it=m_config.begin(); it!=m_config.end(); it++) {
    link.linkName = it->first;
    link.objType = it->second->type();
    link.objName = it->second->name();
    unsigned int linkrev=revision;
    std::string typ = link.objType;
    if (typ == "PixRunConfig") typ = "PixRunConfigSub"; 
    result=(*it).second->write(DbServer, domainName, tag, objName+"|"+link.linkName, typ, pendingTag, linkrev);
    if(result) {
      link.revision=linkrev;
      cont.downLink_content.push_back(link);
    } else {
      std::cout << "Problem in writing subconfig " << (*it).first;
    }
    //std::cout << " - " << (*it).first  << "   __   " << realType;
  }
  //DbServer->printRevContent(cont);  
  
  result=DbServer->writeObject(cont, domainName, tag, objName, objType,revision);
  
  if (result) rev(revision);
  else std::cout << " Error in writing config for " << objName << std::endl; 
  return result;
}

void Config::write( PixDbDomain::rev_content &content) {
  std::cout << "pendingTag in Config is " << penTag() << std::endl;
  for (unsigned int i = 0; i < m_group.size(); i++) {
    //(std::cout << std::endl << "Calling write on group: " <<  m_group[i]->name() <<  std::endl; 
    m_group[i]->write(content);
  }
  content.pendingTag=penTag();
}

void Config::subConfRead(std::map< std::string, std::string >fields, std::map<std::string, Config *>&subConf) {
}
 
void Config::subConfWrite(std::map<std::string, Config *>&subConf) {
}

bool Config::edit() {
  bool modif = false;
  bool cont = true;
  while (cont) {
    std::cout << "==========================================================" << std::endl;
    std::cout << " Editing " << m_confName << " / " << m_confType << std::endl;
    std::cout << "==========================================================" << std::endl;
    std::map<int, ConfObj*> obj;
    for (unsigned int i = 0; i < m_group.size(); i++) {
      m_group[i]->getObjForEditing(obj);
    }
    std::cout << "   Q - quit editor" << std::endl;
    bool badin = true;
    while (badin) {
      std::string inp;
      std::cout << ">>> ";
      std::cin >> inp;
      std::istringstream is(inp);
      int iinp;
      is >> iinp;
      if (inp == "q" || inp == "Q") {
	cont = false;
	badin = false;
      } else if (obj.find(iinp) != obj.end()) {
	modif = obj[iinp]->edit();
	badin = false;
      } else {
	std::cout << "Invalid input" << std::endl;
      }
    }
  }
  return modif;
}

void Config::dump(std::ostream &out, std::string incipit) {
  
  std::string newIncipit;
  std::string fullName = name();
  std::size_t position = fullName.find("/");
  std::string name;
  if(position != std::string::npos) {
    name = std::string(fullName, position+1, fullName.length()); 
    newIncipit = std::string(fullName, 0, position);
  }
  else {
    name = fullName;
    newIncipit = type();
  }
  
  out << "//" << std::endl
      << "BEGININQUIRE " << incipit << newIncipit << std::endl;
  
  for(unsigned int i = 0; i < m_group.size(); i++)
    m_group[i]->dump(out);
  
  
  std::map<std::string, Config*>::iterator it;
  for (it = m_config.begin(); it != m_config.end(); ++it) {
    std::string fullSubName = ((*it).second)->name();
    std::size_t subPosition = fullSubName.find("/");
    std::string subName;
    if(subPosition != std::string::npos)
      subName = std::string(fullSubName, subPosition+1, fullSubName.length()); 
    else
      subName = fullSubName;
    out << "INQUIRE " << subName << " " << subName << std::endl;
  }
  
  out << "ENDINQUIRE " << std::endl;
  
  for (it = m_config.begin(); it != m_config.end(); ++it) {
      ((*it).second)->dump(out, incipit+newIncipit+"/");
  }
}

void Config::reset() {
  for (unsigned int i = 0; i < m_group.size(); i++) {
    m_group[i]->reset();
  }
}

  
const Config &Config::subConfig(unsigned int i) const {
  if (i>=0 && i<m_config.size()) {
    std::map<std::string, Config*>::const_iterator it;
    unsigned int count = 0;
    for (it = m_config.begin(); it != m_config.end(); ++it) {
      if (count == i) {
	return *((*it).second);
      }
      count++;
    }
  }
  return trashConfig;
}

Config &Config::subConfig(unsigned int i) {
  if (i>=0 && i<m_config.size()) {
    std::map<std::string, Config*>::const_iterator it;
    unsigned int count = 0;
    for (it = m_config.begin(); it != m_config.end(); ++it) {
      if (count == i) {
	return *((*it).second);
      }
      count++;
    }
  }
  return trashConfig;
}

Config &Config::subConfig(std::string name) {
  if (m_config.find(name) != m_config.end()) {
    return *m_config[name];
  }
  return trashConfig;
}

