/////////////////////////////////////////////////////////////////////
// CF_group.cxx 
// version 4.0
/////////////////////////////////////////////////////////////////////
//
// 26/09/99  Version 0.1 (PM)
//  7/10/99  Version 0.2 (PM)
//  4/11/99  Version 0.3 (PM)
// 25/07/01  Version 2.1.0 (PM)
// 26/03/04  Version 3.0 - Imported from SimPix (PM)
// 20/07/06  Version 4.0 - Interface with PixDbInterface (PM)
//

#include "PixDbInterface/PixDbInterface.h"
#include "Config/ConfMask.h"
#include "Config/ConfGroup.h"
#include <iomanip>

using namespace PixLib;

ConfObj ConfGroup::trashConfObj("__TrashConfObj__", "Object not found", false, ConfObj::VOID);

ConfGroup::~ConfGroup() {
  for (unsigned int i = 0; i < m_param.size(); i++) {
    delete m_param[i];
  }
}

void ConfGroup::copy(const ConfGroup &grp) {
  for (unsigned int i = 0; i < m_param.size(); i++) {
    for (unsigned int j = 0; j< grp.m_param.size(); j++) {
      if (m_param[i]->name() == grp.m_param[j]->name()) {
	m_param[i]->copy(*grp.m_param[j]);
      }
    }
  }
}

void ConfGroup::addFloat(const std::string &name, float &val, float def, const std::string &comm, bool vis) {
  m_param.push_back(new ConfFloat(m_groupName+"_"+name, val, def, comm, vis));
}

void ConfGroup::addVector(const std::string &name, std::vector<int> &val, std::vector<int> def, const std::string &comm, bool vis) {
  m_param.push_back(new ConfVector(m_groupName+"_"+name, val, def, comm, vis));
}

void ConfGroup::addVector(const std::string &name, std::vector<unsigned int> &val, std::vector<unsigned int> def, const std::string &comm, bool vis) {
  m_param.push_back(new ConfVector(m_groupName+"_"+name, val, def, comm, vis));
}

void ConfGroup::addVector(const std::string &name, std::vector<float> &val, std::vector<float> def, const std::string &comm, bool vis) {
  m_param.push_back(new ConfVector(m_groupName+"_"+name, val, def, comm, vis));
}

void ConfGroup::addStrVect(const std::string &name, std::vector<std::string> &val, std::string def, const std::string &comm, bool vis) {
  m_param.push_back(new ConfStrVect(m_groupName+"_"+name, val, def, comm, vis));
}

void ConfGroup::addMatrix(const std::string &name, ConfMask<unsigned short int> &val, unsigned short int def, const std::string &comm, bool vis) {
  m_param.push_back(new ConfMatrix(m_groupName+"_"+name, val, def, comm, vis));
}

void ConfGroup::addMatrix(const std::string &name, ConfMask<bool> &val, bool def, const std::string &comm, bool vis) {
  m_param.push_back(new ConfMatrix(m_groupName+"_"+name, val, def, comm, vis));
}

void ConfGroup::addBool(const std::string &name, bool &val, bool def, const std::string &comm, bool vis) {
  m_param.push_back(new ConfBool(m_groupName+"_"+name, val, def, comm, vis));
}

void ConfGroup::addBool(const std::string &name, bool &val, bool def, std::string y, std::string n, const std::string &comm, bool vis) {
  m_param.push_back(new ConfBool(m_groupName+"_"+name, val, def, y, n, comm, vis));
}

void ConfGroup::addString(const std::string &name, std::string &val, std::string def, const std::string &comm, bool vis) {
  m_param.push_back(new ConfString(m_groupName+"_"+name, val, def, comm, vis));
}

void ConfGroup::addLink(const std::string &name, std::string &val, std::string def, const std::string &comm, bool vis) {
  m_param.push_back(new ConfLink(name, val, def, comm, vis, true));
}

void ConfGroup::addLinkRO(const std::string &name, std::string &val, std::string def, const std::string &comm, bool vis) {
  m_param.push_back(new ConfLink(name, val, def, comm, vis, false));
}

void ConfGroup::addLink(const std::string &name, std::string remnam, std::string &val, std::string def, const std::string &comm, bool vis) {
  m_param.push_back(new ConfLink(name, remnam, val, def, comm, vis));
}

void ConfGroup::addAlias(const std::string &name, std::string &val, std::string def, const std::string &comm, bool vis) {
  m_param.push_back(new ConfAlias(name, val, def, comm, vis));
}


bool ConfGroup::read(DbRecord *dbr) {
  bool ret = true;
  for (unsigned int i = 0; i < m_param.size(); i++) {
    if (!m_param[i]->read(dbr)) {
      ret = false;
      std::cout << "PixLib::ConfGrp - Group " << m_groupName << " - Error reading parameter " << m_param[i]->name() << endl;
    }
  }
  return ret;
}

bool ConfGroup::write(DbRecord *dbr) {
  for (unsigned int i = 0; i < m_param.size(); i++)
    if (!m_param[i]->write(dbr)) return false;

  return true;
}


void ConfGroup::write(PixDbDomain::rev_content &cont) {
  //std::cout << "Confgroup has" << m_param.size() << " confobj!" << std::endl;
  for (unsigned int i = 0; i < m_param.size(); i++) {
    int L;
    std::string realName;
    L=m_param[i]->name().length();
    //std::cout << "complete Name is " << m_param[i]->name() << " and group Name is " << m_groupName << std::endl;
    
    //realName=m_param[i]->name().substr(m_groupName.length()+1,L-m_groupName.length());	
   
    switch (m_param[i]->type()) {
    case ConfObj::INT: {
        realName = m_param[i]->name().substr(m_groupName.length() + 1, L - m_groupName.length());
        ConfObj::subtypes subtype = m_param[i]->subtype();
        if (subtype == ConfInt::S32 || subtype == ConfInt::S16 || subtype == ConfInt::S8) {
            //std::cout << "this is a int!!!" << std::endl;
            if (subtype == ConfInt::S32)
                cont.int_content[m_groupName + "_I32_" + realName] = (int) ((ConfInt *) (m_param[i]))->value();
            if (subtype == ConfInt::S16)
                cont.int_content[m_groupName + "_I16_" + realName] = (int) ((ConfInt *) (m_param[i]))->value();
            if (subtype == ConfInt::S8)
                cont.int_content[m_groupName + "_I8_" + realName] = (int) ((ConfInt *) (m_param[i]))->value();
        }
        if (subtype == ConfInt::U32 || subtype == ConfInt::U16 || subtype == ConfInt::U8) {
            //std::cout << "this is a unsigned!!!" << std::endl;
            if (subtype == ConfInt::U32)
                cont.unsigned_content[m_groupName + "_U32_" +
                                      realName] = (unsigned int) ((ConfInt *) (m_param[i]))->value();
            if (subtype == ConfInt::U16)
                cont.unsigned_content[m_groupName + "_U16_" +
                                      realName] = (unsigned int) ((ConfInt *) (m_param[i]))->value();
            if (subtype == ConfInt::U8)
                cont.unsigned_content[m_groupName + "_U8_" +
                                      realName] = (unsigned int) ((ConfInt *) (m_param[i]))->value();
        }
    }
      break;
    case ConfObj::FLOAT:
      realName=m_param[i]->name().substr(m_groupName.length()+1,L-m_groupName.length());
      //std::cout << "this is a float!! " << std::endl;
      cont.float_content[m_groupName+"_F_"+realName]=((ConfFloat*)(m_param[i]))->value();
      break;
    case ConfObj::VECTOR: {
      realName=m_param[i]->name().substr(m_groupName.length()+1,L-m_groupName.length());
      ConfObj::subtypes subtype = m_param[i]->subtype();
      //std::cout << "this is a vector!! " << std::endl;
      if ( subtype == ConfVector::V_INT) {
	std::vector<int> tmp=((ConfVector*)(m_param[i]))->valueVInt();
	for (unsigned int in=0; in<tmp.size(); in++) {
	  std::ostringstream os;
	  os << m_groupName << "_V_" << in << "_" << realName;
	  cont.int_content[os.str()]=tmp[in];
	}
	cont.unsigned_content[m_groupName+"_VS_"+realName]=tmp.size();
      }
      if ( subtype== ConfVector::V_UINT) {
	std::vector<unsigned int> tmp=((ConfVector*)(m_param[i]))->valueVUint();
	for (unsigned int in=0; in<tmp.size(); in++) {
	  std::ostringstream os;
	  os << m_groupName << "_V_" << in << "_" << realName;
	  cont.unsigned_content[os.str()]=tmp[in];
	}
	cont.unsigned_content[m_groupName+"_VS_"+realName]=tmp.size();
      }
      if ( subtype== ConfVector::V_FLOAT){
	std::vector<float> tmp=((ConfVector*)(m_param[i]))->valueVFloat();
	for (unsigned int in=0; in<tmp.size(); in++) {
	  std::ostringstream os;
	  os << m_groupName << "_V_" << in << "_" << realName;
	  cont.float_content[os.str()]=tmp[in];
	}
	cont.unsigned_content[m_groupName+"_VS_"+realName]=tmp.size();
      }
      break;
    }
    case ConfObj::STRVECT:
      realName=m_param[i]->name().substr(m_groupName.length()+1,L-m_groupName.length());
      {
	std::vector<std::string> tmp=((ConfStrVect*)(m_param[i]))->value();
	//std::cout << " the size of the STRVECT obj is " << tmp.size() << std::endl;
	for (unsigned int in=0; in<tmp.size(); in++) {
	  std::ostringstream os;
	  os << m_groupName << "_SV_" << in << "_" << realName;
	  //std::cout << " - " << tmp[in] << std::endl;
	  cont.string_content[os.str()]=tmp[in];
	}
	cont.unsigned_content[m_groupName+"_SVS_"+realName]=tmp.size();
      }
      break;
    case ConfObj::MATRIX: {
      realName=m_param[i]->name().substr(m_groupName.length()+1,L-m_groupName.length());
      ConfObj::subtypes subtype=m_param[i]->subtype();
      //std::cout << "this is a matrix!! " << std::endl;
      if ( subtype== ConfMatrix::M_U16) {
	std::vector<unsigned int> tmp=((ConfMatrix*)(m_param[i]))->valueU16().getMask();
	cont.vect_content[m_groupName+"_M_U16_"+realName]=tmp;
      }
      if ( subtype== ConfMatrix::M_U1) {
	std::vector<unsigned int> tmp=((ConfMatrix*)(m_param[i]))->valueU1().getMask();
	cont.vect_content[m_groupName+"_M_U1_"+realName]=tmp;
      }
      break;
    }
    case ConfObj::LIST: {
        realName = m_param[i]->name().substr(m_groupName.length() + 1, L - m_groupName.length());
        ConfObj::subtypes subtype = m_param[i]->subtype();
        //std::cout << "this is a list!! " << std::endl;
        if (subtype == ConfInt::S32 ||
            subtype == ConfInt::S16 ||
            subtype == ConfInt::S8) {
            //std::cout << "this is a int!!!" << std::endl;
            if (subtype == ConfInt::S32)
                cont.int_content[m_groupName + "_L_I32_" + realName] = (int) ((ConfInt *) (m_param[i]))->value();
            if (subtype == ConfInt::S16)
                cont.int_content[m_groupName + "_L_I16_" + realName] = (int) ((ConfInt *) (m_param[i]))->value();
            if (subtype == ConfInt::S8)
                cont.int_content[m_groupName + "_L_I8_" + realName] = (int) ((ConfInt *) (m_param[i]))->value();
        }
        if (subtype == ConfInt::U32 ||
            subtype == ConfInt::U16 ||
            subtype == ConfInt::U8) {
            //std::cout << "this is a unsigned!!!" << std::endl;
            if (subtype == ConfInt::U32)
                cont.unsigned_content[m_groupName + "_L_U32_" +
                                      realName] = (unsigned int) ((ConfInt *) (m_param[i]))->value();
            if (subtype == ConfInt::U16)
                cont.unsigned_content[m_groupName + "_L_U16_" +
                                      realName] = (unsigned int) ((ConfInt *) (m_param[i]))->value();
            if (subtype == ConfInt::U8)
                cont.unsigned_content[m_groupName + "_L_U8_" +
                                      realName] = (unsigned int) ((ConfInt *) (m_param[i]))->value();
        }
        break;
    }
    case ConfObj::BOOL:
      realName=m_param[i]->name().substr(m_groupName.length()+1,L-m_groupName.length());	
      //std::cout << "this is a bool!! " << std::endl;
      cont.bool_content[m_groupName+"_B_"+realName]=((ConfBool*)(m_param[i]))->value();
      break;
    case ConfObj::STRING:
      realName=m_param[i]->name().substr(m_groupName.length()+1,L-m_groupName.length());	
      //std::cout << "this is a string!! " << std::endl;
      cont.string_content[m_groupName+"_S_"+realName]=((ConfString*)(m_param[i]))->value();
      break;
    case ConfObj::LINK:
      cont.string_content[m_groupName+"_LI_"+m_param[i]->name()]=((ConfLink*)(m_param[i]))->value();
      //std::cout << "this is a link!! " << std::endl;
      break;
    case ConfObj::ALIAS:
      //std::cout << "this is a alias!! " << std::endl;
      cont.string_content[m_groupName+"_A_"+m_param[i]->name()]=((ConfAlias*)(m_param[i]))->value();
      break;
    case ConfObj::VOID:
      std::cout << "this is a void!! " << std::endl;
      break;
    default:
      std::cout << " default!!!!!" << std::endl;
      break;
    }
  }
}


void ConfGroup::read(PixDbDomain::rev_content &cont) {
  for (unsigned int i = 0; i < m_param.size(); i++) {
    int L=m_param[i]->name().length();
    std::string realName;
    std::map<std::string, bool>::iterator b_it;
    std::map<std::string, int>::iterator i_it;
    std::map<std::string, unsigned int>::iterator u_it;
    std::map<std::string, float>::iterator f_it;
    std::map<std::string, std::string>::iterator s_it;
    std::map<std::string, std::vector<unsigned int> >::iterator v_it;

    switch (m_param[i]->type()) {
      
    case ConfObj::INT: {
        realName = m_param[i]->name().substr(m_groupName.length() + 1, L - m_groupName.length());
        ConfObj::subtypes subtype = m_param[i]->subtype();
        if (subtype == ConfInt::S32 || subtype == ConfInt::S16 || subtype == ConfInt::S8) {
            //std::cout << "this is a int!!!" << std::endl;
            if (subtype == ConfInt::S32) {
                i_it = cont.int_content.find(m_groupName + "_I32_" + realName);
                if (i_it != cont.int_content.end()) {
                    ((ConfInt *) (m_param[i]))->valueS32() = (int) ((*i_it).second);
                    cont.int_content.erase(i_it);
                } else
                    std::cout << " Error in ConfGroup: parameter " << realName << " not found in DbServer" << std::endl;
            }
            if (subtype == ConfInt::S16) {
                i_it = cont.int_content.find(m_groupName + "_I16_" + realName);
                if (i_it != cont.int_content.end()) {
                    ((ConfInt *) (m_param[i]))->valueS16() = (short int) ((*i_it).second);
                    cont.int_content.erase(i_it);
                } else
                    std::cout << " Error in ConfGroup: parameter " << realName << " not found in DbServer" << std::endl;
            }
            if (subtype == ConfInt::S8) {
                i_it = cont.int_content.find(m_groupName + "_I8_" + realName);
                if (i_it != cont.int_content.end()) {
                    ((ConfInt *) (m_param[i]))->valueS8() = (char) ((*i_it).second);
                    cont.int_content.erase(i_it);
                } else
                    std::cout << " Error in ConfGroup: parameter " << realName << " not found in DbServer" << std::endl;
            }
        }
        if (subtype == ConfInt::U32 || subtype == ConfInt::U16 || subtype == ConfInt::U8) {
            //std::cout << "this is a unsigned!!!" << std::endl;
            if (subtype == ConfInt::U32) {
                u_it = cont.unsigned_content.find(m_groupName + "_U32_" + realName);
                if (u_it != cont.unsigned_content.end()) {
                    ((ConfInt *) (m_param[i]))->valueU32() = (unsigned int) ((*u_it).second);
                    cont.unsigned_content.erase(u_it);
                } else
                    std::cout << " Error in ConfGroup: parameter " << realName << " not found in DbServer" << std::endl;
            }
            if (subtype == ConfInt::U16) {
                u_it = cont.unsigned_content.find(m_groupName + "_U16_" + realName);
                if (u_it != cont.unsigned_content.end()) {
                    ((ConfInt *) (m_param[i]))->valueU16() = (unsigned short int) ((*u_it).second);
                    cont.unsigned_content.erase(u_it);
                } else
                    std::cout << " Error in ConfGroup: parameter " << realName << " not found in DbServer" << std::endl;
            }
            if (subtype == ConfInt::U8) {
                u_it = cont.unsigned_content.find(m_groupName + "_U8_" + realName);
                if (u_it != cont.unsigned_content.end()) {
                    ((ConfInt *) (m_param[i]))->valueU8() = (unsigned char) ((*u_it).second);
                    cont.unsigned_content.erase(u_it);
                } else
                    std::cout << " Error in ConfGroup: parameter " << realName << " not found in DbServer" << std::endl;
            }
        }
        break;
    }
    case ConfObj::FLOAT:
      realName=m_param[i]->name().substr(m_groupName.length()+1,L-m_groupName.length());
      f_it=cont.float_content.find(m_groupName+"_F_"+realName);
      if( f_it!=cont.float_content.end()) {
	//std::cout << "this is a float!! " << std::endl;
	((ConfFloat*)(m_param[i]))->valueFloat()=(*f_it).second;
	cont.float_content.erase(f_it);
      }    else std::cout << " Error in ConfGroup: parameter " << realName << " not found in DbServer" << std::endl;
      break;
    
    case ConfObj::VECTOR: {
        realName = m_param[i]->name().substr(m_groupName.length() + 1, L - m_groupName.length());
        ConfObj::subtypes subtype = m_param[i]->subtype();
        //std::cout << "this is a vector!! " << std::endl;
        if (subtype == ConfVector::V_INT) {
            u_it = cont.unsigned_content.find(m_groupName + "_VS_" + realName);
            unsigned int size = (*u_it).second;
            std::vector<int> &inttmp = ((ConfVector *) (m_param[i]))->valueVInt();
            inttmp.clear();
            for (unsigned int iv = 0; iv < size; iv++) {
                std::ostringstream os;
                os << m_groupName << "_V_" << iv << "_" << realName;
                i_it = cont.int_content.find(os.str());
                inttmp.push_back((*i_it).second);
            }
        }
        if (subtype == ConfVector::V_UINT) {
            u_it = cont.unsigned_content.find(m_groupName + "_VS_" + realName);
            unsigned int size = (*u_it).second;
            std::vector<unsigned int> &uinttmp = ((ConfVector *) (m_param[i]))->valueVUint();
            uinttmp.clear();
            for (unsigned int iv = 0; iv < size; iv++) {
                std::ostringstream os;
                os << m_groupName << "_V_" << iv << "_" << realName;
                u_it = cont.unsigned_content.find(os.str());
                uinttmp.push_back((*u_it).second);
            }
        }
        if (subtype == ConfVector::V_FLOAT) {
            u_it = cont.unsigned_content.find(m_groupName + "_VS_" + realName);
            unsigned int size = (*u_it).second;
            std::vector<float> &floattmp = ((ConfVector *) (m_param[i]))->valueVFloat();
            floattmp.clear();
            for (unsigned int iv = 0; iv < size; iv++) {
                std::ostringstream os;
                os << m_groupName << "_V_" << iv << "_" << realName;
                f_it = cont.float_content.find(os.str());
                floattmp.push_back((*f_it).second);
            }
        }
        break;
    }
    case ConfObj::STRVECT:
      realName=m_param[i]->name().substr(m_groupName.length()+1,L-m_groupName.length());
      {
	u_it=cont.unsigned_content.find(m_groupName+"_SVS_"+realName);
	unsigned int size=(*u_it).second;
	std::cout << "size is " << size << std::endl;
	std::vector<std::string> &stvtmp=((ConfStrVect*)(m_param[i]))->value();
	stvtmp.clear();
	for (unsigned int iv=0; iv<size; iv++) {
	  std::ostringstream os;
	  os << m_groupName << "_SV_" << iv << "_" << realName;
	  s_it=cont.string_content.find(os.str());
	  std::cout << " - objName " << os.str() << " and content " << (*s_it).second << std::endl;
	  stvtmp.push_back((*s_it).second);
	}
      }	
      break;  

    case ConfObj::MATRIX: {
      realName=m_param[i]->name().substr(m_groupName.length()+1,L-m_groupName.length());
      ConfObj::subtypes subtype=m_param[i]->subtype();
      if ( subtype== ConfMatrix::M_U16) {
	v_it=cont.vect_content.find(m_groupName+"_M_U16_"+realName);
	((ConfMatrix*)(m_param[i]))->valueU16().getMask()=(*v_it).second;
      }
      if ( subtype== ConfMatrix::M_U1) {
	v_it=cont.vect_content.find(m_groupName+"_M_U1_"+realName);
	((ConfMatrix*)(m_param[i]))->valueU1().getMask()=(*v_it).second;
      }
      //std::cout << "this is a matrix!! " << std::endl;
      break;
    }
    case ConfObj::LIST: {
        realName = m_param[i]->name().substr(m_groupName.length() + 1, L - m_groupName.length());
        ConfObj::subtypes subtype = m_param[i]->subtype();
        //std::cout << "this is a list!! " << std::endl;
        if (subtype == ConfInt::S32 ||
            subtype == ConfInt::S16 ||
            subtype == ConfInt::S8) {
            //std::cout << "this is a int!!!" << std::endl;
            if (subtype == ConfInt::S32) {
                i_it = cont.int_content.find(m_groupName + "_L_I32_" + realName);
                if (i_it != cont.int_content.end()) {
                    ((ConfInt *) (m_param[i]))->valueS32() = (int) ((*i_it).second);
                    cont.int_content.erase(i_it);
                } else
                    std::cout << " Error in ConfGroup: parameter " << realName << " not found in DbServer" << std::endl;
            }
            if (subtype == ConfInt::S16) {
                i_it = cont.int_content.find(m_groupName + "_L_I16_" + realName);
                if (i_it != cont.int_content.end()) {
                    ((ConfInt *) (m_param[i]))->valueS16() = (short int) ((*i_it).second);
                    cont.int_content.erase(i_it);
                } else
                    std::cout << " Error in ConfGroup: parameter " << realName << " not found in DbServer" << std::endl;
            }
            if (subtype == ConfInt::S8) {
                i_it = cont.int_content.find(m_groupName + "_L_I8_" + realName);
                if (i_it != cont.int_content.end()) {
                    ((ConfInt *) (m_param[i]))->valueS8() = (char) ((*i_it).second);
                    cont.int_content.erase(i_it);
                } else
                    std::cout << " Error in ConfGroup: parameter " << realName << " not found in DbServer" << std::endl;
            }
        }
        if (subtype == ConfInt::U32 ||
            subtype == ConfInt::U16 ||
            subtype == ConfInt::U8) {
            //std::cout << "this is a unsigned!!!" << std::endl;
            if (subtype == ConfInt::U32) {
                u_it = cont.unsigned_content.find(m_groupName + "_L_U32_" + realName);
                if (u_it != cont.unsigned_content.end()) {
                    ((ConfInt *) (m_param[i]))->valueU32() = (unsigned int) ((*u_it).second);
                    cont.unsigned_content.erase(u_it);
                } else
                    std::cout << " Error in ConfGroup: parameter " << realName << " not found in DbServer" << std::endl;
            }
            if (subtype == ConfInt::U16) {
                u_it = cont.unsigned_content.find(m_groupName + "_L_U16_" + realName);
                if (u_it != cont.unsigned_content.end()) {
                    ((ConfInt *) (m_param[i]))->valueU16() = (unsigned short int) ((*u_it).second);
                    cont.unsigned_content.erase(u_it);
                } else
                    std::cout << " Error in ConfGroup: parameter " << realName << " not found in DbServer" << std::endl;
            }
            if (subtype == ConfInt::U8) {
                u_it = cont.unsigned_content.find(m_groupName + "_L_U8_" + realName);
                if (u_it != cont.unsigned_content.end()) {
                    ((ConfInt *) (m_param[i]))->valueU8() = (unsigned char) ((*u_it).second);
                    cont.unsigned_content.erase(u_it);
                } else
                    std::cout << " Error in ConfGroup: parameter " << realName << " not found in DbServer" << std::endl;
            }
        }
        break;
    }
    case ConfObj::BOOL:
      realName=m_param[i]->name().substr(m_groupName.length()+1,L-m_groupName.length());
      b_it=cont.bool_content.find(m_groupName+"_B_"+realName);
      if( b_it!=cont.bool_content.end()) {
	//std::cout << "this is a bool!! " << std::endl;
	((ConfBool*)(m_param[i]))->valueBool()=(*b_it).second;
	cont.bool_content.erase(b_it);
      }
      else std::cout << " Error in ConfGroup: parameter " << realName << " not found in DbServer" << std::endl;
      break;
      
    case ConfObj::STRING:
      realName=m_param[i]->name().substr(m_groupName.length()+1,L-m_groupName.length());
      s_it=cont.string_content.find(m_groupName+"_S_"+realName);
      if( s_it!=cont.string_content.end()) {
	//std::cout << "this is a string!! " << std::endl;
	((ConfString*)(m_param[i]))->valueString()=(*s_it).second;
	cont.string_content.erase(s_it);
      }
      else std::cout << " Error in ConfGroup: parameter " << realName << " not found in DbServer" << std::endl;
      break;

    case ConfObj::LINK:
      s_it=cont.string_content.find(m_groupName+"_LI_"+m_param[i]->name());
      if( s_it!=cont.string_content.end()) {
	((ConfLink*)(m_param[i]))->valueString()=(*s_it).second;
	cont.string_content.erase(s_it);
      }    else std::cout << " Error in ConfGroup: parameter " << realName << " not found in DbServer" << std::endl;
      break;

    case ConfObj::ALIAS:
      s_it=cont.string_content.find(m_groupName+"_A_"+m_param[i]->name());
      if( s_it!=cont.string_content.end()) {
	((ConfAlias*)(m_param[i]))->valueString()=(*s_it).second;
	cont.string_content.erase(s_it);
      }    else std::cout << " Error in ConfGroup: parameter " << realName << " not found in DbServer" << std::endl;
      break;

    case ConfObj::VOID:
      std::cout << "this is a void!! " << std::endl;
      break;
    default:
      std::cout << " default!!!!!" << std::endl;
      break;
    }
  }
}

void ConfGroup::getObjForEditing(std::map<int, ConfObj*> &obj) {
  for (unsigned int i = 0; i < m_param.size(); i++) {
    int ic = obj.size();
    if (m_param[i]->visible()) {
      std::cout << std::setw(4) << std::right << ic;
      std::pair<int, ConfObj*> p(ic, m_param[i]);
      obj.insert(p);
      std::cout << " - " << m_param[i]->name() << " - " << m_param[i]->comment();
      std::cout << " (";
      m_param[i]->print();
      std::cout << ")" << std::endl;
    }
  }
}

void ConfGroup::dump(std::ostream &out) {
  for (unsigned int i = 0; i < m_param.size(); i++)
    m_param[i]->dump(out);
}
  
void ConfGroup::reset() {
  for (unsigned int i = 0; i < m_param.size(); i++) {
    m_param[i]->reset();
  }
}

ConfObj &ConfGroup::operator[] (const std::string &name) {
  std::string xnam = m_groupName+"_"+name;
  for (unsigned int i=0; i<m_param.size(); i++) {
    if (m_param[i]->name() == xnam) {
      return *m_param[i];
    }
  }
  return trashConfObj;
}


