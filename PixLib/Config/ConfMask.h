/////////////////////////////////////////////////////////////////////
// ConfMask.h 
// version 0.1
/////////////////////////////////////////////////////////////////////
//
// 13/04/04  Version 0.1 (CS)
//

#ifndef PIXLIB_CONFMASK
#define PIXLIB_CONFMASK
#include <vector>
#include <iostream>
#include <stdexcept>

namespace PixLib {

template<class T> class ConfMask {
public:
  ConfMask() : m_maxValue(0) { } // Default constructor
//  counter always becomes 1 _before_ _any_ of the main programs start
//          by creation of the following objects in PixFe/PixFeData.cxx:
//             ConfMask<bool> __a;
//             ConfMask<unsigned short int> __b;

  ConfMask(int nCol, int nRow, T maxValue); // Constructor (all entries are set to zero)
  ConfMask(int nCol, int nRow, T maxValue, T defValue); // Constructor (all entries are set to defValue)
  ConfMask(const ConfMask& c); //! Copy constructor
  ~ConfMask(); // Destructor
  void dumpCounters();

  //! Assignment operator
  ConfMask& operator = (const ConfMask& c);

  //! Entire mask operations
  void enableAll();       // Set all entries to m_maxValue
  void disableAll();      // Set all entries to zero
  void setAll(T value); // Set all entries to value
  
  //! Single column operations
  //void enableCol(int col);         // Set an entire column to m_maxValue
  //void disableCol(int col);        // Set an entire column to zero
  void setCol(int col, T value); // Set an entire column to value

  //! Single row operations
 // void enableRow(int row);         // Set an entire row to m_maxValue
  //void disableRow(int row);        // Set an entire row to zero
  void setRow(int row, T value); // Set an entire row to value

  //! Single entry operations
  void enable(int col, int row);         // Set entry to m_maxValue
  void disable(int col, int row);        // Set entry to zero
  void set(int col, int row, T value); // Set entry to value

  //! Conversion to and from vector methods
  void set(std::vector<T> &value);
  void get(std::vector<T> &output);

  //! Accessor methods
  std::vector< std::vector<T> > get();
  std::vector<T> get(int col);
  T get(int col, int row) const;
  int ncol() const { return m_ncol; };
  int nrow() const { return m_nrow; };
  std::vector<unsigned int> &getMask() { return m_mask; };
  T maxVal() const { return m_maxValue; };
private: 
  std::vector< unsigned int > m_mask;
  T m_maxValue;
  int m_nbits;
  int m_nrow;
  int m_ncol;
};

}


//! Constructor (all entries are set to zero)
template<class T> PixLib::ConfMask<T>::ConfMask(int nCol, int nRow, T maxValue) : m_maxValue(maxValue) {
  // Compute the number of bits required
  m_ncol = nCol;
  m_nrow = nRow;
  m_nbits = 32;
  for (int i=1; i<31; i++) {
    if (maxValue < (0x1<<i)) {
      m_nbits = i;
      break;
    }
  }
  // Check column and row numbers
  if(nCol>0 && nRow>0) {
    // Create mask
    int nw = 32/m_nbits;
    int nel = nCol*nRow/nw+1;
    m_mask.clear();
    for (int i=0; i<nel; i++) m_mask.push_back(0);

  }

  //std::cout << "++ count = " << counter <<  " size = " << size << std::endl;
}

//! Constructor (all entries are set to defValue)
template<class T> PixLib::ConfMask<T>::ConfMask(int nCol, int nRow, T maxValue, T defValue) : m_maxValue(maxValue) {
  // Compute the number of bits required
  m_ncol = nCol;
  m_nrow = nRow;
  m_nbits = 32;
  for (int i=1; i<31; i++) {
    if (maxValue < (0x1<<i)) {
      m_nbits = i;
      break;
    }
  }
  // Check column and row numbers
  if(nCol>0 && nRow>0) {
    // Create mask
    int nw = 32/m_nbits;
    int nel = nCol*nRow/nw+1;
    m_mask.clear();
    for (int i=0; i<nel; i++) m_mask.push_back(0);
    T defaultValue;
    if(defValue<=m_maxValue) defaultValue = defValue;
    else throw std::range_error("ConfMask::ConfMask value too large");
    setAll(defaultValue);

  }

  //std::cout << "++ count = " << counter <<  " size = " << size << std::endl;
}

//! Copy constructor
template<class T> PixLib::ConfMask<T>::ConfMask(const ConfMask<T> &c) {
  // Copy mask
  m_mask = c.m_mask;

  // Copy max value
  m_maxValue = c.m_maxValue;

  // Copy number of bits, rows, columns
  m_nbits = c.m_nbits;
  m_ncol = c.m_ncol;
  m_nrow = c.m_nrow;


  //std::cout << "+C count = " << counter <<  " size = " << size << std::endl;
}

//! Destructor
template<class T> PixLib::ConfMask<T>::~ConfMask() {
  // Clear mask

  m_mask.clear();

  //std::cout << "== count = " << counter <<  " size = " << size << std::endl;
}


//! Assignment operator
template<class T> PixLib::ConfMask<T>& PixLib::ConfMask<T>::operator = (const ConfMask<T>& c) {
  // Copy mask

  m_mask = c.m_mask;


  // Copy max value
  m_maxValue = c.m_maxValue;

  // Copy number of bits, rows, columns
  m_nbits = c.m_nbits;
  m_ncol = c.m_ncol;
  m_nrow = c.m_nrow;

  //std::cout << "CC count = " << count <<  " size = " << size << std::endl;

  return *this;
}

template<class T> void PixLib::ConfMask<T>::dumpCounters() {
  //std::cout << "ConfMask: count = " << counter <<  " size = " << size << std::endl;
}

//! Set all entries to m_maxValue
template<class T> void PixLib::ConfMask<T>::enableAll() {
  // Enable mask
  setAll(m_maxValue);
}

//! Set all entries to zero
template<class T> void PixLib::ConfMask<T>::disableAll() {
  // Disable mask
  setAll(0);
}

//! Set all entries to value
template<class T> void PixLib::ConfMask<T>::setAll(T value) {
  // Set mask
  T val;
  if(value<=m_maxValue) val = value;
  else throw std::range_error("ConfMask::setAll value too large");
  for (int ic=0; ic<m_ncol; ic++) {
    for (int ir=0; ir<m_nrow; ir++) {
      set(ic, ir, val);
    }
  }
}
  

//! Set an entire column to m_maxValue
#if 0
template<class T> void PixLib::ConfMask<T>::enableCol(int col) {
  // Check column number
  if(col<0 || col>=m_ncol) throw std::out_of_range("ConfMask::enableCol out of range");

  // Enable column
  for (int ir=0; ir<m_nrow; ir++) {
    set(col, ir, m_maxValue);
  }
}

//! Set an entire column to zero
template<class T> void PixLib::ConfMask<T>::disableCol(int col) {
  // Check column number
  if(col<0 || col>=m_ncol) throw std::out_of_range("ConfMask::disableCol out of range");

  // Disable column
  for (int ir=0; ir<m_nrow; ir++) {
    set(col, ir, 0);
  }
}
#endif
//! Set an entire column to value
template<class T> void PixLib::ConfMask<T>::setCol(int col, T value) {
  // Check column number
  if(col<0 || col>=m_ncol) throw std::out_of_range("ConfMask::setCol out of range");

  // Set column to value
  T val;
  if(value<=m_maxValue) val = value;
  else throw std::range_error("ConfMask::setCol value too large");
  for (int ir=0; ir<m_nrow; ir++) {
    set(col, ir, val);
  }
}

#if 0
//! Set an entire row to m_maxValue
template<class T> void PixLib::ConfMask<T>::enableRow(int row) {
  // Check row number
  if(row<0 || row>=m_nrow) throw std::out_of_range("ConfMask::enableRow out of range");

  // Enable row
  for (int ic=0; ic<m_ncol; ic++) {
    set(ic, row, m_maxValue);
  }
}

//! Set an entire row to zero
template<class T> void PixLib::ConfMask<T>::disableRow(int row) {
  // Check row number
  if(row<0 || row>=m_nrow) throw std::out_of_range("ConfMask::disableRow out of range");

  // Disable row
  for (int ic=0; ic<m_ncol; ic++) {
    set(ic, row, 0);
  }
}
#endif
//! Set an entire row to value
template<class T> void PixLib::ConfMask<T>::setRow(int row, T value) {
  // Check row number
  if(row<0 || row>=m_nrow) throw std::out_of_range("ConfMask::setRow out of range");

  // Set row to value
  T val;
  if(value<=m_maxValue) val = value;
  else throw std::range_error("ConfMask::setCol value too large");
  for (int ic=0; ic<m_ncol; ic++) {
    set(ic, row, val);
  }
}


//! Set entry to m_maxValue
template<class T> void PixLib::ConfMask<T>::enable(int col, int row) {
  // Check row and column number
  if(col<0 || col>=m_ncol) throw std::out_of_range("ConfMask::enable out of range");
  if(row<0 || row>=m_nrow) throw std::out_of_range("ConfMask::enable out of range");

  // Enable entry
  set(col, row, m_maxValue);
}

//! Set entry to m_maxValue
template<class T> void PixLib::ConfMask<T>::disable(int col, int row) {
  // Check row and column number
  if(col<0 || col>=m_ncol) throw std::out_of_range("ConfMask::disable out of range");
  if(row<0 || row>=m_nrow) throw std::out_of_range("ConfMask::disable out of range");

  // Disable entry
  set(col, row, 0);
}

//! Set entry to m_maxValue
template<class T> void PixLib::ConfMask<T>::set(int col, int row, T value) {
  // Check row and column number
  if(col<0 || col>=m_ncol) throw std::out_of_range("ConfMask::set out of range");
  if(row<0 || row>=m_nrow) throw std::out_of_range("ConfMask::set out of range");

  // Set entry to value
  int nel = 32/m_nbits;
  int pos = (col*m_nrow+row)/nel;
  int off = ((col*m_nrow+row)%nel)*m_nbits;
  T val;
  if(value<=m_maxValue) val = value;
  else throw std::range_error("ConfMask::set value too large");
  unsigned int uval = (val & ((0x1<<m_nbits)-1))<<off;
  unsigned int mask = ~(((0x1<<m_nbits)-1)<<off);
  if (pos<(int)m_mask.size()) { 
    m_mask[pos] &= mask;
    m_mask[pos] |= uval;
  } else {
    std::cout << "++++ Error : invalid pos = " << pos << std::endl;
  }
}

//! Initialization from vector of T
template<class T> void PixLib::ConfMask<T>::set(std::vector<T> &value) {
  int col=0, row=0;

  // Set entries
  for(unsigned int i=0; i<value.size() && col<m_ncol && row<m_nrow; i++) {
    set(col, row, value[i]);
    col++; if(col==m_ncol) {row++; col=0;}
  }
}

//! Output to vector of T
template<class T> void PixLib::ConfMask<T>::get(std::vector<T> &output) {

  // Get entries
  output.clear();
  for(int row=0; row<m_nrow; row++) {
    for(int col=0; col<m_ncol; col++) {
      output.push_back(get(col, row));
    }
  }
}

//! Mask accessor method
template<class T> std::vector< std::vector<T> > PixLib::ConfMask<T>::get() {
  std::vector< std::vector<T> > v;
  for (int ic=0; ic<m_ncol; ic++) {
    v.push_back(std::vector<T>());
    for (int ir=0; ir<m_nrow; ir++) {
      v[ic].push_back(get(ic, ir));
    }
  }
  return v;
}

//! Column accessor method
template<class T> std::vector<T> PixLib::ConfMask<T>::get(int col) {
  std::vector<T> v;
  if (col<0 || col>=m_ncol) return v;
  for (int ir=0; ir<m_nrow; ir++) {
    v.push_back(get(col, ir));
  }
  return v;
}

//! Mask accessor method
template<class T> T PixLib::ConfMask<T>::get(int col, int row) const {
  if (col<0 || col>=m_ncol) throw std::out_of_range("ConfMask::get out of range");
  if (row<0 || row>=m_nrow) throw std::out_of_range("ConfMask::get out of range");
  int nel = 32/m_nbits;
  int pos = (col*m_nrow+row)/nel;
  int off = ((col*m_nrow+row)%nel)*m_nbits;
  return (m_mask[pos]>>off)&((0x1<<m_nbits)-1);
}

#endif
