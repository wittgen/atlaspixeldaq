/////////////////////////////////////////////////////////////////////
// ConfObj.cxx
// version 4.0
/////////////////////////////////////////////////////////////////////
//
// 26/09/99  Version 0.1 (PM)
//  7/10/99  Version 0.2 (PM)
//  4/11/99  Version 0.3 (PM)
// 25/07/01  Version 2.1.0 (PM)
// 26/03/04  Version 3.0 - Imported from SimPix (PM)
// 20/07/06  Version 4.0 - Interface with PixDbInterface (PM)
//

#include "PixDbInterface/PixDbInterface.h"
#include "Config/ConfMask.h"
#include "Config/ConfObj.h"

#include <sstream>
#include <iomanip>
#include <type_traits>
#include <string>


namespace PixLib {
template class ConfMask<bool>;
template class ConfMask<unsigned short>;

}
using namespace PixLib;

/////////////////////
// ConfObj methods
/////////////////////

ConfObj::ConfObj(const std::string &nam, const std::string &comm, bool vis, types typ) {
  m_name = std::move(nam);
  m_comment = std::move(comm);
  m_visible = vis;
  m_type = typ;
  m_subtype=NONE;

//  counter always becomes 1 _before_ _any_ of the main programs start
//          by creation of the following object in Config/ConfGroup.cxx:
//             ConfObj trashConfObj("__TrashConfObj__", "Object not found", false, ConfObj::VOID);
}
 
ConfObj::~ConfObj() {

}

/////////////////////
// ConfInt methods
/////////////////////

ConfInt::~ConfInt() {

}

void ConfInt::copy(const ConfObj &obj) {
  try { 
    const ConfInt &cObj = dynamic_cast<const ConfInt &>(obj);
    if (m_subtype == cObj.m_subtype) {
      switch(m_subtype){
      case S32:
	*((int *)m_value) = *((int *)(cObj.m_value));
	break;
      case U32:
	*((unsigned int *)m_value) = *((unsigned int *)(cObj.m_value));
	break;
      case S16:
	*((short int *)m_value) = *((short int *)(cObj.m_value));
	break;
      case U16:
	*((unsigned short int *)m_value) = *((unsigned short int *)(cObj.m_value));
	break;
      case S8:
	*((char *)m_value) = *((char *)(cObj.m_value));
	break;
      case U8:
	*((unsigned char *)m_value) = *((unsigned char *)(cObj.m_value));
	break;
      default: ;
      }
    }
  }
  catch (const std::bad_cast &err) {
    return;
  } 
}

int ConfInt::getValue() const {
  int ivalue=0;
  switch (m_subtype) {
  case S32:
    ivalue = *((int *)m_value);
    break;
  case U32:  
    ivalue = *((unsigned int *)m_value);
    break;
  case S16:
    ivalue = *((short int *)m_value);
    break;
  case U16:
    ivalue = *((unsigned short int *)m_value);  
    break;
  case S8:
    ivalue = *((char *)m_value);
    break;
  case U8:
    ivalue = *((unsigned char *)m_value);
    break;
    default:
    throw ConfigExc("Invalid Config Subtype");
  }
  return ivalue;
}

void ConfInt::setValue(int val) {
  switch(m_subtype){
  case S32:
    *((int *)m_value) = (int)val;
    break;
  case U32:
    *((unsigned int *)m_value) = (unsigned int)val;
    break;
  case S16:
    *((short int *)m_value) = (short int)val;
    break;
  case U16:
    *((unsigned short int *)m_value) = (unsigned short int)val;
    break;
  case S8:
    *((char *)m_value) = (char)val;
    break;
  case U8:
    *((unsigned char *)m_value) = (unsigned char)val;
    break;
  default: throw ConfigExc("Invalid Config Subtype");
  }
}

void ConfInt::reset() {
  switch (m_subtype) {
  case S32:
    *((int *)m_value) = m_defS32;
    break;
  case U32:
    *((unsigned int *)m_value) = m_defU32;
    break;
  case S16:
    *((short int *)m_value) = (short int)m_defS32;
    break;
  case U16:
    *((unsigned short int *)m_value) = (unsigned short int)m_defU32;
    break;
  case S8:
    *((char *)m_value) = (char)m_defS32;
    break;
  case U8:
    *((unsigned char *)m_value) = (unsigned char)m_defU32;
    break;
      default: throw ConfigExc("Invalid Config Subtype");
  }
}

bool ConfInt::read(DbRecord *dbr) {
  dbFieldIterator f;
  int value;
  unsigned int uvalue;

  f = dbr->findField(m_name);
  if(f==dbr->fieldEnd()) return true;

  try {
    switch (m_subtype) {
    case S32:
      f = dbr->getDb()->DbProcess(f,PixDb::DBREAD,value); f.releaseMem();
      *((int *)m_value) = value;
      break;
    case U32:
      f = dbr->getDb()->DbProcess(f,PixDb::DBREAD,uvalue); f.releaseMem();
      *((unsigned int *)m_value) = uvalue;
      break;
    case S16:
      f = dbr->getDb()->DbProcess(f,PixDb::DBREAD,value); f.releaseMem();
      *((short int *)m_value) = value;
      break;
    case U16:
      f = dbr->getDb()->DbProcess(f,PixDb::DBREAD,value); f.releaseMem();
      *((unsigned short int *)m_value) = value;
      break;
    case S8:
      f = dbr->getDb()->DbProcess(f,PixDb::DBREAD,value); f.releaseMem();
      *((char *)m_value) = value;
      break;
    case U8:
      f = dbr->getDb()->DbProcess(f,PixDb::DBREAD,value); f.releaseMem();
      *((unsigned char *)m_value) = value;
      break;
        default: throw ConfigExc("Invalid Config Subtype");
    }
    f.releaseMem();
  }
  catch (const PixDBException &ex) {
    return false;
  }
  return true;
}
  
bool ConfInt::write(DbRecord *dbr) {
  dbFieldIterator f;
  DbField *fi = NULL;
  int value;
  unsigned int uvalue;
  bool unsig=false;
  //bool ret = true;
  f = dbr->findField(m_name);
  if (f==dbr->fieldEnd()) {
    f = dbr->addField(m_name);
  }

  switch (m_subtype) {
  case S32:
    value = (int) *((int *)m_value);
    unsig = false;
    break;
  case U32:
    uvalue = (unsigned int) *((unsigned int *)m_value);
    unsig = true;
    break;
  case S16:
    value = (int) *((short int *)m_value);
    unsig = false;
    break;
  case U16:
    value = (unsigned int) *((unsigned short int *)m_value);
    unsig = false;
    break;
  case S8:
    value = (int) *((char *)m_value);
    unsig = false;
    break;
  case U8:
    value = (unsigned int) *((unsigned char *)m_value);
    unsig = false;
    break;
      default: throw ConfigExc("Invalid Config Subtype");
  }
  try {
    if (unsig) {
      f = dbr->getDb()->DbProcess(f,PixDb::DBCOMMIT,uvalue);
    } else {
      f = dbr->getDb()->DbProcess(f,PixDb::DBCOMMIT,value);
    }
  }
  catch (const PixDBException &ex) {
    //ret = false;
  }
  if (fi != NULL) delete fi;

  return true;
}

void ConfInt::dump(std::ostream &out) {
  out << "FIELD " << name() << " int " << getValue() << std::endl;
}

bool ConfInt::edit() {
  bool modif = false;
  std::cout << "Insert value for " << m_name;
  switch (m_subtype) {
  case S32:
    std::cout << " (INT) : ";
    { 
      int val = *((int *)m_value);
      std::cin >> val;
      if (val != *((int *)m_value)) {
	*((int *)m_value) = val;
	modif = true;
      } 
    }
    break;
  case U32:
    std::cout << " (UINT) : ";
    { 
      unsigned int val = *((unsigned int *)m_value);
      std::cin >> val;
      if (val != *((unsigned int *)m_value)) {
	*((unsigned int *)m_value) = val;
	modif = true;
      } 
    }
    break;
  case S16:
    std::cout << " (SHORT) : ";
    { 
      short int val = *((short int *)m_value);
      std::cin >> val;
      if (val != *((short int *)m_value)) {
	*((short int *)m_value) = val;
	modif = true;
      } 
    }
    break;
  case U16:
    std::cout << " (USHORT) : ";
    { 
      unsigned short int val = *((unsigned short int *)m_value);
      std::cin >> val;
      if (val != *((unsigned short int *)m_value)) {
	*((unsigned short int *)m_value) = val;
	modif = true;
      }
    }
    break;
  case S8:
    std::cout << " (CHAR) : ";
    {
      char val = *((char *)m_value);
      std::cin >> val;
      if (val != *((char *)m_value)) {
	*((char *)m_value) = val;
	modif = true;
      }
    }
    break;
  case U8:
    std::cout << " (UCHAR) : ";
    {
      unsigned char val = *((unsigned char *)m_value);
      std::cin >> val;
      if (val != *((unsigned char *)m_value)) {
	*((unsigned char *)m_value) = val;
	modif = true;
      }
    }
    break;
  default: ;
  } 
  return modif;
}

void ConfInt::print() const {
  std::cout << getValue(); 
}

/////////////////////
// ConfFloat methods
/////////////////////

ConfFloat::ConfFloat(const std::string &nam, float &val, float def, const std::string &comm, bool vis) :
  ConfObj(nam,comm,vis,FLOAT), m_value(val) {
    m_defval = def;

}
  
ConfFloat::~ConfFloat() {

}

void ConfFloat::copy(const ConfObj &obj) {
  try { 
    const ConfFloat &cObj = dynamic_cast<const ConfFloat &>(obj);
    m_value = cObj.m_value;
  }
  catch (const std::bad_cast &err) {
    return;
  } 
}

bool ConfFloat::read(DbRecord *dbr) {
  dbFieldIterator f;
  f = dbr->findField(m_name);
  if(f==dbr->fieldEnd()) return true;

  try {
    f = dbr->getDb()->DbProcess(f,PixDb::DBREAD,m_value);
    f.releaseMem();
  }
  catch (const PixDBException &ex) {
    return false;
  }
  return true;
}
  
bool ConfFloat::write(DbRecord *dbr) {
  dbFieldIterator f;
  DbField *fi = NULL;
  bool ret = true;
  f = dbr->findField(m_name);
  if (f==dbr->fieldEnd()) {
    f = dbr->addField(m_name);
  }
  try {
    f = dbr->getDb()->DbProcess(f,PixDb::DBCOMMIT,m_value);
    if (fi != NULL) delete fi;
  }
  catch (const PixDBException &ex) {
    ret = false;
  }
  return ret;
}

void ConfFloat::dump(std::ostream &out) {
  out << "FIELD " << name() << " float " << m_value << std::endl;
}

bool ConfFloat::edit() {
  bool modif = false;
  std::cout << "Insert value for " << m_name;
  std::cout << " (FLOAT) : ";
  float val = m_value;
  std::cin >> val;
  if (val != m_value) {
    m_value = val;
    modif = true;
  } 
  return modif;
}

void ConfFloat::print() const {
  std::cout << m_value; 
}


/////////////////////
// ConfVector methods
/////////////////////

ConfVector::ConfVector(const std::string &nam, std::vector<int> &val, std::vector<int> def, const std::string &comm, bool vis) :
  ConfObj(nam,comm,vis,VECTOR) {
  m_value = (void *)&val;
  m_subtype = V_INT;
  m_defInt = def;

}
  
ConfVector::ConfVector(const std::string &nam, std::vector<unsigned int> &val, std::vector<unsigned int> def, const std::string &comm, bool vis) :
  ConfObj(nam,comm,vis,VECTOR) {
  m_value = (void *)&val;
  m_subtype = V_UINT;
  m_defUint = def;

}
  
ConfVector::ConfVector(const std::string &nam, std::vector<float> &val, std::vector<float> def, const std::string &comm, bool vis) :
  ConfObj(nam,comm,vis,VECTOR) {
  m_value = (void *)&val;
  m_subtype = V_FLOAT;
  m_defFloat = def;

}
  
ConfVector::~ConfVector() {

}

void ConfVector::copy(const ConfObj &obj) {
  try { 
    const ConfVector &cObj = dynamic_cast<const ConfVector &>(obj);
    if (m_subtype == cObj.m_subtype) {
      switch (m_subtype) {
      case V_INT:
	*((std::vector<int> *)m_value) = *((std::vector<int> *)(cObj.m_value));
	break;
      case V_UINT:
	*((std::vector<unsigned int> *)m_value) = *((std::vector<unsigned int> *)(cObj.m_value));
	break;
      case V_FLOAT:
	*((std::vector<float> *)m_value) = *((std::vector<float> *)(cObj.m_value));
	break;
          default: throw ConfigExc("Invalid Config Subtype");
      }
    }
  }
  catch (const std::bad_cast &err) {
    return;
  } 
}

void ConfVector::reset() {
  switch (m_subtype) {
  case V_INT:
    *((std::vector<int> *)m_value) = m_defInt;
    break;
  case V_UINT:
    *((std::vector<unsigned int> *)m_value) = m_defUint;
    break;
  case V_FLOAT:
    *((std::vector<float> *)m_value) = m_defFloat;
    break;
      default: throw ConfigExc("Invalid Config Subtype");
  }
}

bool ConfVector::read(DbRecord *dbr) {
  bool stat = true;
  dbFieldIterator f;
  f = dbr->findField(m_name);
  if(f==dbr->fieldEnd()) return true;

  try {
    switch (m_subtype) {
    case V_INT: {
      std::vector<int> value;
      f = dbr->getDb()->DbProcess(f,PixDb::DBREAD,value); f.releaseMem();
      if(stat) {
	*(std::vector<int>*)m_value = value;
      }
      break;
    }
    case V_UINT: {
      std::vector<int> value;
      std::vector<unsigned int> uvalue;
      f = dbr->getDb()->DbProcess(f,PixDb::DBREAD,value); f.releaseMem();
      if(stat) {
	for (unsigned int i=0; i<value.size(); i++) uvalue.push_back((unsigned int)value[i]);
	*(std::vector<unsigned int> *)m_value = uvalue;
      }
      break;
    }
    case V_FLOAT: {
        std::vector<float> value;
        f = dbr->getDb()->DbProcess(f, PixDb::DBREAD, value);
        f.releaseMem();
        if (stat) {
            *(std::vector<float> *) m_value = value;
        }
        break;
    }
        default: throw ConfigExc("Invalid Config Subtype");
    }
    f.releaseMem();
  }
  catch (const PixDBException &ex) {
    stat = false;
  }
  
  return stat;
}
  
bool ConfVector::write(DbRecord *dbr) {
  dbFieldIterator f;
  bool ret=true;
  DbField *fi = NULL;
  f = dbr->findField(m_name);
  if (f==dbr->fieldEnd()) {
    f = dbr->addField(m_name);
  }

  try {
    switch (m_subtype) {
    case V_INT: {
      std::vector<int> value;
      value = *(std::vector<int> *)m_value;
      f = dbr->getDb()->DbProcess(f,PixDb::DBCOMMIT,value);
      break;
    }
    case V_UINT: {
      std::vector<unsigned int> &v = *(std::vector<unsigned int> *)m_value;
      std::vector<int> value;
      std::vector<unsigned int>::iterator it;
      for(it=v.begin(); it!=v.end(); it++) {
	value.push_back((int)*it);
      }
      f = dbr->getDb()->DbProcess(f,PixDb::DBCOMMIT,value);
      break;
    }
    case V_FLOAT: {
      std::vector<float> value;
      value = *(std::vector<float> *)m_value;
      f = dbr->getDb()->DbProcess(f,PixDb::DBCOMMIT,value);
      break;
    }
     default: throw ConfigExc("Invalid Config Subtype");
    }
  }
  catch (const PixDBException&) {
    ret = false;
  }

  if (fi != NULL) delete fi;

  return ret;
}

void ConfVector::dump(std::ostream &out) {
  out << "FIELD " << name();
  std::vector<int> value;
  switch (m_subtype) {
  case V_INT: {
    out << " vector<int>";
    std::vector<int> &temp = *(std::vector<int> *)m_value;
    std::vector<int>::iterator it;
    for(it=temp.begin(); it != temp.end(); it++) {
      out << " " << (*it);
    }
    out << std::endl;
    break;
  }
  case V_UINT: {
    out << " vector<int>";
    std::vector<unsigned int> &temp = *(std::vector<unsigned int> *)m_value;
    std::vector<unsigned int>::iterator it;
    for(it=temp.begin(); it != temp.end(); it++) {
      out << " " << (*it);
    }
    out << std::endl;
    break;
  }
  case V_FLOAT: {
    out << " vector<float>";
    std::vector<float> &temp = *(std::vector<float> *)m_value;
    std::vector<float>::iterator it;
    for(it=temp.begin(); it != temp.end(); it++) {
      out << " " << (*it);
    }
    out << std::endl;
    break;
  }
      default: throw ConfigExc("Invalid Config Subtype");
  }
}

//////////////////////
// ConfStrVect methods
//////////////////////

ConfStrVect::ConfStrVect(const std::string &nam, std::vector<std::string> &val, std::string def, const std::string &comm, bool vis) :
  ConfObj(nam,comm,vis,STRVECT) {
  m_value = (void *)&val;
  m_def = def;

}
  
ConfStrVect::~ConfStrVect() {

}

void ConfStrVect::copy(const ConfObj &obj) {
  try { 
    const ConfStrVect &cObj = dynamic_cast<const ConfStrVect &>(obj);
    *((std::vector<std::string> *)m_value) = *((std::vector<std::string> *)(cObj.m_value));
  }
  catch (const std::bad_cast &err) {
    return;
  } 
}

void ConfStrVect::reset() {
  std::vector<std::string> &v = *((std::vector<std::string> *)m_value);
  for (unsigned int i=0; i<v.size(); i++) v[i] = m_def;
}

bool ConfStrVect::read(DbRecord *dbr)  {
  bool stat = true;
  dbFieldIterator f;
  unsigned int i=0;

  try {
    std::vector<std::string> &value = *(std::vector<std::string> *)m_value;
    value.clear();
    while (1)  {
      std::ostringstream os;
      os << m_name << "_" << i;
      f = dbr->findField(os.str());
      if (f==dbr->fieldEnd()) break;
      
      std::string str;
      f = dbr->getDb()->DbProcess(f,PixDb::DBREAD, str);
      if (stat) {
	std::string sstr;
        for (unsigned int j=0; j<str.size(); j++) {
	  if (str[j] == '\n') {
	    value.push_back(sstr);
	    sstr = "";
	  } else {
	    sstr += str[j];
	  }
	}
	if (sstr.size() > 0) {
	  value.push_back(sstr);
	  sstr = "";
	}
      }
      f.releaseMem();
      i++;
    }
    if (value.size() == 0) stat = true;
  }
  catch (PixDBException &ex) {
    stat = false;
  }
  return stat;
}
  
bool ConfStrVect::write(DbRecord *dbr) {
  dbFieldIterator f;
  bool ret=true;
  unsigned int i=0,j=0;

  try {
    std::vector<std::string> value = *(std::vector<std::string> *)m_value;
    while (i < value.size()) {
      std::string sval;
      for (int k=0; k<50; k++) {
	if (i < value.size()) {
	  sval += value[i];
          sval += '\n';
	  i++;
	}
      }
      std::ostringstream os;
      os << m_name << "_" << j++;
      f = dbr->findField(os.str());
      if (f==dbr->fieldEnd()) {
	f = dbr->addField(os.str());
      }
      f = dbr->getDb()->DbProcess(f,PixDb::DBCOMMIT,sval);
    }
  }
  catch (const PixDBException &) {
    ret = false;
  }
  return ret;
}

void ConfStrVect::dump(std::ostream &out) {
  out << "FIELD " << name();
  out << " vector<std::string>";
  std::vector<std::string> &temp = *(std::vector<std::string> *)m_value;
  std::vector<std::string>::iterator it;
  for(it=temp.begin(); it != temp.end(); it++) {
    out << " " << (*it);
  }
  out << std::endl;
}

/////////////////////
// ConfMatrix methods
/////////////////////

ConfMatrix::ConfMatrix(const std::string &nam, ConfMask<unsigned short int> &val, unsigned short int def, const std::string &comm, bool vis) :
  ConfObj(nam,comm,vis,MATRIX) {
  m_value = (void *)&val;
  m_subtype = M_U16;
  m_defU16 = def;

}
ConfMatrix::ConfMatrix(const std::string &nam, ConfMask<bool> &val, bool def, const std::string &comm, bool vis) :
  ConfObj(nam,comm,vis,MATRIX) {
  m_value = (void *)&val;
  m_subtype = M_U1;
  m_defU1 = def;

}
  
ConfMatrix::~ConfMatrix() {

}

void ConfMatrix::copy(const ConfObj &obj) {
  try { 
    const ConfMatrix &cObj = dynamic_cast<const ConfMatrix &>(obj);
    if (m_subtype == cObj.m_subtype) {
      switch(m_subtype){
      case M_U16:
	*((ConfMask<unsigned short int> *)m_value) = *((ConfMask<unsigned short int> *)(cObj.m_value));
	break;
      case M_U1:
	*((ConfMask<bool> *)m_value) = *((ConfMask<bool> *)(cObj.m_value));
	break;
      default: throw ConfigExc("Invalid Config Subtype");
      }
    }
  }
  catch (const std::bad_cast &err) {
    return;
  } 
}

void ConfMatrix::reset() {
  switch (m_subtype) {
  case M_U16:
    ((ConfMask<unsigned short int> *)m_value)->setAll(m_defU16);
    break;
  case M_U1:
    ((ConfMask<bool> *)m_value)->setAll(m_defU1);
    break;
      default: throw ConfigExc("Invalid Config Subtype");
  }
}

bool ConfMatrix::read(DbRecord *dbr) {
  bool stat=true;
  dbFieldIterator f;
  f = dbr->findField(m_name);
  if(f==dbr->fieldEnd()) return true;

  try {
    switch (m_subtype) {
      case M_U16: {
	std::vector<int> value;
	std::vector<unsigned short int> valueU16;
	f = dbr->getDb()->DbProcess(f,PixDb::DBREAD,value); 
	f.releaseMem();
	for(unsigned int i=0; i<value.size(); i++) valueU16.push_back((unsigned short int)value[i]);
	((ConfMask<unsigned short int>*)m_value)->set(valueU16);
	break;
      }
      case M_U1: {
	std::vector<bool> valueU1;
	f = dbr->getDb()->DbProcess(f,PixDb::DBREAD,valueU1); 
	f.releaseMem();
	((ConfMask<bool>*)m_value)->set(valueU1);
	break;
      }
      default: throw ConfigExc("Invalid Config Subtype");
    }
  }
  catch (const PixDBException &ex) {
    stat = false;
  }    
  
  f.releaseMem();

  return stat;
}
  
bool ConfMatrix::write(DbRecord *dbr) {
  std::vector<int> value;
  dbFieldIterator f;
  DbField *fi = NULL;
  bool ret = true;
  f = dbr->findField(m_name);
  if (f==dbr->fieldEnd()) {
    f = dbr->addField(m_name);
  }

  try {
    switch (m_subtype) {
    case M_U16: {
      std::vector<unsigned short int> temp;
      ((ConfMask<unsigned short int> *)m_value)->get(temp);
      std::vector<unsigned short int>::iterator it, itEnd=temp.end();
      for(it=temp.begin(); it!=itEnd; it++)
	value.push_back((int)*it);
      f = dbr->getDb()->DbProcess(f,PixDb::DBCOMMIT,value);
      break;
    }
    case M_U1: {
      std::vector<bool> temp;
      ((ConfMask<bool> *)m_value)->get(temp);
      f = dbr->getDb()->DbProcess(f,PixDb::DBCOMMIT,temp);
      break;
    }
      default: throw ConfigExc("Invalid Config Subtype");
    }
    if (fi != NULL) delete fi;
  }
  catch (const PixDBException&) {
    ret = false;
  }
  return ret;

}

void ConfMatrix::dump(std::ostream &out) {
  out << "FIELD " << name();

  std::vector<int> value;
  switch (m_subtype) {
  case M_U16: {
    out << " vector<int>";
    std::vector<unsigned short int> temp;
    ((ConfMask<unsigned short int> *)m_value)->get(temp);
    std::vector<unsigned short int>::iterator it, itEnd=temp.end();
    for(it=temp.begin(); it!=itEnd; it++)
      out << " " << ((int)*it);
    out << std::endl;
    break;
  }
  case M_U1: {
    out << " vector<bool>";
    std::vector<bool> temp;
    ((ConfMask<bool> *)m_value)->get(temp);
    std::vector<bool>::iterator it, itEnd=temp.end();
    for(it=temp.begin(); it!=itEnd; it++)
      out << " " << ((int)*it);
    out << std::endl;
    break;
  }
  default: throw ConfigExc("Invalid Config Subtype");
  }
}

/////////////////////
// ConfList methods
/////////////////////
void ConfList::copy(const ConfObj &obj) {
  try { 
    const ConfList &cObj = dynamic_cast<const ConfList &>(obj);
    if (m_subtype == cObj.m_subtype) {
      switch(m_subtype){
      case S32:
	*((int *)m_value) = *((int *)(cObj.m_value));
	break;
      case U32:
	*((unsigned int *)m_value) = *((unsigned int *)(cObj.m_value));
	break;
      case S16:
	*((short int *)m_value) = *((short int *)(cObj.m_value));
	break;
      case U16:
	*((unsigned short int *)m_value) = *((unsigned short int *)(cObj.m_value));
	break;
      case S8:
	*((char *)m_value) = *((char *)(cObj.m_value));
	break;
      case U8:
	*((unsigned char *)m_value) = *((unsigned char *)(cObj.m_value));
	break;
      default:  throw std::runtime_error("Invalid Config Subtype");;
      }
    }
  }
  catch (const std::bad_cast &err) {
    return;
  } 
}

bool ConfList::read(DbRecord *dbr) {
  dbFieldIterator f;
  std::string value;
  int ivalue;
  bool stat = true;
  f = dbr->findField(m_name);
  if(f==dbr->fieldEnd()) return true;

  try {
    f = dbr->getDb()->DbProcess(f,PixDb::DBREAD,value); f.releaseMem();
  }
  catch (const PixDBException&) {
    stat = false;
  }
  if (stat) {
    if (m_symbols.find(value) != m_symbols.end()) {
      ivalue = m_symbols[value];
    } else {
      stat = false;
    }
  }
  if (stat) {
    switch (m_subtype) {
    case S32:
      *((int *)m_value) = ivalue;
      break;
    case U32:  
      *((unsigned int *)m_value) = ivalue;
      break;
    case S16:
      *((short int *)m_value) = ivalue;
      break;
    case U16:
      *((unsigned short int *)m_value) = ivalue;  
      break;
    case S8:
      *((char *)m_value) = ivalue;
      break;
    case U8:
      *((unsigned char *)m_value) = ivalue;
      break;
        default: throw ConfigExc("Invalid Config Subtype");
    }
  }
  f.releaseMem();
  return stat;
}
  
bool ConfList::write(DbRecord *dbr) {
  bool stat=false;
  dbFieldIterator f;
  DbField *fi = NULL;
  std::string value;
  int ivalue=0;

  switch (m_subtype) {
  case S32:
    ivalue = (int) *((int *)m_value);
    break;
  case U32:  
    ivalue = (int) *((unsigned int *)m_value);
    break;
  case S16:
    ivalue = (int) *((short int *)m_value);
    break;
  case U16:
    ivalue = (int) *((unsigned short int *)m_value);  
    break;
  case S8:
    ivalue = (int) *((char *)m_value);
    break;
  case U8:
    ivalue = (int) *((unsigned char *)m_value);
    break;
      default: throw ConfigExc("Invalid Config Subtype");
  }

  std::map<std::string, int>::iterator s, sEnd=m_symbols.end();
  for(s=m_symbols.begin(); s!=sEnd; s++) {
    if (s->second == ivalue) {
      value = s->first;
      stat = true;
    }
  }

  f = dbr->findField(m_name);
  if (f==dbr->fieldEnd()) {
    f = dbr->addField(m_name);
  } 
  try {
    f = dbr->getDb()->DbProcess(f,PixDb::DBCOMMIT,value);
  }
  catch (const PixDBException&) {
    stat = false;
  }
  if (fi != NULL) delete fi;
  return stat;
}

void ConfList::dump(std::ostream &out) {
  out << "FIELD " << name() << " string " << sValue() << std::endl;
}

std::string ConfList::sValue() const {
  int ivalue=9;
  switch (m_subtype) {
  case S32:
    ivalue = *((int *)m_value);
    break;
  case U32:  
    ivalue = *((unsigned int *)m_value);
    break;
  case S16:
    ivalue = *((short int *)m_value);
    break;
  case U16:
    ivalue = *((unsigned short int *)m_value);  
    break;
  case S8:
    ivalue = *((char *)m_value);
    break;
  case U8:
    ivalue = *((unsigned char *)m_value);
    break;
      default: throw ConfigExc("Invalid Config Subtype");
  }
  for (auto it = m_symbols.begin(); it != m_symbols.end(); ++it) {
    if ((*it).second == ivalue) {
      return (*it).first;
    } 
  }
  return "????";
}

bool ConfList::edit() {
  bool modif = false;
  std::cout << "Choose value for " << m_name << std::endl;
  std::map<std::string, int>::iterator it;
  std::map<int, int> mv;
  int count = 0;
  for (it = m_symbols.begin(); it != m_symbols.end(); ++it) {
    std::pair<int, int> p(count, it->second);
    mv.insert(p);
    std::cout << std::setw(4) << count++ << " - " << it->first << std::endl;
  }
  std::cout << ">>> ";
  int iv;
  std::cin >> iv;
  if (mv.find(iv) != mv.end()) {
    int nval = mv[iv];
    int ivalue=0;
    switch (m_subtype) {
    case S32:
      ivalue = *((int *)m_value);
      break;
    case U32:  
      ivalue = *((unsigned int *)m_value);
      break;
    case S16:
      ivalue = *((short int *)m_value);
      break;
    case U16:
      ivalue = *((unsigned short int *)m_value);  
      break;
    case S8:
      ivalue = *((char *)m_value);
      break;
    case U8:
      ivalue = *((unsigned char *)m_value);
      break;
        default: throw ConfigExc("Invalid Config Subtype");
    }
    if (ivalue != nval) {
      modif = true;
      switch (m_subtype) {
      case S32:
	*((int *)m_value) = nval;
	break;
      case U32:  
	*((unsigned int *)m_value) = nval;
	break;
      case S16:
	*((short int *)m_value) = nval;
	break;
      case U16:
	*((unsigned short int *)m_value) = nval;  
	break;
      case S8:
	*((char *)m_value) = nval;
	break;
      case U8:
	*((unsigned char *)m_value) = nval;
	break;
          default: throw ConfigExc("Invalid Config Subtype");
      }
    }
    
  }
  return modif;
}

void ConfList::print() const {
  std::cout << sValue(); 
}

/////////////////////
// CF_bool methods
/////////////////////

ConfBool::ConfBool(const std::string &nam, bool &val, bool def, const std::string &comm, bool vis) :
  ConfObj(nam,comm,vis,BOOL), m_value(val) {
    m_defval = def;
    m_yes = "TRUE";
    m_no = "FALSE";

}
 
ConfBool::ConfBool(const std::string &nam, bool &val, bool def, const std::string &y, const std::string &n, const std::string &comm, bool vis) :
  ConfObj(nam,comm,vis,BOOL), m_value(val) {
    m_defval = def;
    m_yes = y;
    m_no = n;

}

ConfBool::~ConfBool() {

}

void ConfBool::copy(const ConfObj &obj) {
  try { 
    const ConfBool &cObj = dynamic_cast<const ConfBool &>(obj);
    m_value = cObj.m_value;
  }
  catch (const std::bad_cast &err) {
    return;
  } 
}

bool ConfBool::read(DbRecord *dbr) {
  dbFieldIterator f;
  f = dbr->findField(m_name);
  if(f==dbr->fieldEnd()) return true;
  try {
    f = dbr->getDb()->DbProcess(f,PixDb::DBREAD,m_value);
    f.releaseMem();
  }
  catch (const PixDBException&) {
    return false;
  }
  return true;
}
  
bool ConfBool::write(DbRecord *dbr) {
  dbFieldIterator f;
  DbField *fi = NULL;
  bool ret = true;
  f = dbr->findField(m_name);
  if (f==dbr->fieldEnd()) {
    f = dbr->addField(m_name);
  }
  try {
    f = dbr->getDb()->DbProcess(f,PixDb::DBCOMMIT,m_value);
  }
  catch (const PixDBException&) {
    ret= false;
  }
  if (fi != NULL) delete fi;
  return ret;
}

void ConfBool::dump(std::ostream &out) {
  out << "FIELD " << name() << " bool " << (int)m_value << std::endl;
}

bool ConfBool::edit() {
  bool modif = false;
  std::cout << "Insert value for " << m_name;
  std::cout << " (BOOL) : ";
  bool val = m_value;
  std::cin >> val;
  if (val != m_value) {
    m_value = val;
    modif = true;
  } 
  return modif;
}

void ConfBool::print() const{
  std::cout << m_value; 
}

/////////////////////
// CF_string methods
/////////////////////

ConfString::ConfString(const std::string &nam, std::string &val, std::string def, const std::string &comm, bool vis) :
  ConfObj(nam,comm,vis,STRING), m_value(val) {
    m_defval = def;

}
  
ConfString::~ConfString() {

}

void ConfString::copy(const ConfObj &obj) {
  try { 
    const ConfString &cObj = dynamic_cast<const ConfString &>(obj);
    m_value = cObj.m_value;
  }
  catch (const std::bad_cast &err) {
    return;
  } 
}

bool ConfString::read(DbRecord *dbr) {
  dbFieldIterator f;
  f = dbr->findField(m_name);
  if(f==dbr->fieldEnd()) return true;
  try {
    f = dbr->getDb()->DbProcess(f,PixDb::DBREAD,m_value);
    f.releaseMem();
  }
  catch (const PixDBException&) {
    return false;
  }
  return true;
}
  
bool ConfString::write(DbRecord *dbr) {
  dbFieldIterator f;
  DbField *fi = NULL;
  bool ret = true;
  f = dbr->findField(m_name);
  if (f==dbr->fieldEnd()) {
    f = dbr->addField(m_name);
  }
  try {
    f = dbr->getDb()->DbProcess(f,PixDb::DBCOMMIT,m_value);
  }
  catch (const PixDBException&) {
    ret = false;
  } 
  if (fi != NULL) delete fi;
  return ret;
}

void ConfString::dump(std::ostream &out) {
  out << "FIELD " << name() << " string " << m_value << std::endl;
}

bool ConfString::edit() {
  bool modif = false;
  std::cout << "Insert value for " << m_name;
  std::cout << " (STRING) : ";
  std::string val = m_value;
  std::cin >> val;
  if (val != m_value) {
    m_value = val;
    modif = true;
  } 
  return modif;
}

void ConfString::print() const {
  std::cout << m_value; 
}

/////////////////////
// CF_link methods
/////////////////////

ConfLink::ConfLink(const std::string &nam, std::string &val, std::string def, const std::string &comm, bool vis, bool write) :
  ConfObj(nam,comm,vis,LINK), m_value(val) {
    m_defval = def;
    m_remnam = "";
    m_write = write;

}
  
ConfLink::ConfLink(const std::string &nam, std::string remnam, std::string &val, std::string def, const std::string &comm, bool vis) :
  ConfObj(nam,comm,vis,LINK), m_value(val) {
    m_defval = def;
    m_remnam = remnam;
    m_write = true;

}
  
ConfLink::~ConfLink() {

}

void ConfLink::copy(const ConfObj &obj) {
  try { 
    const ConfLink &cObj = dynamic_cast<const ConfLink &>(obj);
    m_value = cObj.m_value;
  }
  catch (const std::bad_cast &err) {
    return;
  } 
}

bool ConfLink::read(DbRecord *dbr) {
  dbRecordIterator r;
  r = dbr->findRecord(m_name);
  if(r == dbr->recordEnd()) return true;
  try {
    r = dbr->getDb()->DbProcess(r, PixDb::DBREAD);
    m_value = (*r)->getDecName();
  }
  catch (const PixDBException&) {
    return false;
  }
  return true;
}
  
bool ConfLink::write(DbRecord *dbr) {
  if (m_write) {
    DbRecord *r;
    bool ret = true;
    try {
      r = dbr->getDb()->DbFindRecordByName(m_value);
    }
    catch (const PixDBException&) {
      r = NULL;
    }
    if (r != NULL) {
      bool err = false;
      try {
	if (m_remnam == "") {
	  //dbr->linkRecord(r, name(), dbr->getName());
	  r->linkRecord(dbr, dbr->getName(), name());
	} else {
	  //dbr->linkRecord(r, name(), m_remnam);
	  r->linkRecord(dbr, m_remnam, name());
	}
      }
      catch (const PixDBException &e) {
	err = true;
      }
      catch (const std::exception &e) {
	err = true;
      }
      if (err) {
	dbRecordIterator findIt;
	if((findIt = dbr->findRecord(name())) != dbr->recordEnd()){  
	  //the link failed because a slot name name() is already present in the dbr record. Erase it and do again the linking
	  try {
	    //dbr->eraseRecord(findIt);
	    if (m_remnam == "") {
	      dbr->updateLink(r, name(), dbr->getName());
	    //  dbr->linkRecord(r, name(), dbr->getName());
	    } else {
	      dbr->updateLink(r, name(), m_remnam);
	    //  dbr->linkRecord(r, name(), m_remnam);
	    }
	  }
	  catch (const PixDBException &e){
	    std::cerr<<"ConfLink::write(): Got PixDBException: ";
	    e.what(std::cerr);
	    std::cerr<<std::endl;
	    ret = false;
	  }
	}	
	ret = true;
      }
      
    } else {
      ret = true;
    }
    
    return ret;
  } else {
    return true;
  }
}

void ConfLink::dump(std::ostream &out) {
  out << "FIELD " << name() << " string " << m_value << std::endl;
}

/////////////////////
// CF_alias methods
/////////////////////

ConfAlias::ConfAlias(const std::string &nam, std::string &val, std::string def, const std::string &comm, bool vis, bool write) :
  ConfObj(nam,comm,vis,ALIAS), m_value(val) {
    m_defval = def;

}
  
ConfAlias::~ConfAlias() {

}

void ConfAlias::copy(const ConfObj &obj) {
  try { 
    const ConfAlias &cObj = dynamic_cast<const ConfAlias &>(obj);
    m_value = cObj.m_value;
  }
  catch (const std::bad_cast &err) {
    return;
  } 
}

bool ConfAlias::read(DbRecord *dbr) {
  try {
    m_value = dbr->getDb()->findAlias(dbr->getName(), m_name);
  }
  catch (...) { 
    std::cout << "Exception caught in ConfObj.cxx::read(DbRecord)" << std::endl;
    return false;
  }
  return true;
}
  
bool ConfAlias::write(DbRecord *dbr) {
  return true;
}

void ConfAlias::dump(std::ostream &out) {
  out << "FIELD " << name() << " string " << m_value << std::endl;
}
