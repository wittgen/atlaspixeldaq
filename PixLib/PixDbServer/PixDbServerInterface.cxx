/////////////////////////////////////////////////////////////////////
// PixDbServerInterface.cxx
// version 1.0.0
/////////////////////////////////////////////////////////////////////
//
// 02/10/07  Version 1.0 (VD)
//           Initial release
//

#include <fstream>
#include <iostream>
#include <sstream>

#include "PixDbServer/PixDbServerInterface.h"

using namespace PixLib;
using namespace SctPixelRod;

//! Constructor
PixDbServerInterface::PixDbServerInterface(IPCPartition *ipcPartition, std::string ipcName, serviceType type, bool &ready, std::string cashingFileName):
  IPCNamedObject<POA_ipc::PixDbServerInterface,ipc::multi_thread>(*ipcPartition,ipcName.c_str()),
  m_ipcPartition(ipcPartition), m_filename(cashingFileName), m_type(type), m_readEnable(false) {
  ready=true;
  m_expertMode = false;
  //m_cb = NULL;
  //m_cb.clear();
  switch (m_type) {
  case SERVER:
    try {
      publish();
    } catch (...) {
      std::cout << "Error in publishing server on ipc!!" << std::endl;
      ready=false;
    }
    m_mutex= new OWLMutexRW; 
    m_readEnable=false;
    break;
  case CLIENT:
    try {
      m_ipcInterface = ipcPartition->lookup<ipc::PixDbServerInterface>(ipcName.c_str());
      m_ipcInterface->ipc_ready();
    } catch (daq::ipc::InvalidPartition&) {
      // The parition ipcPartition does not exist
      std::cout << "IPC server is unavailable " << std::endl;
    } catch (daq::ipc::InvalidObjectName&) {
      // ipcName is an empty string
      std::cout << "The name of the PixDbServerInterface is null " << std::endl;
    } catch (daq::ipc::ObjectNotFound&) {
      // The PixDbServerInterface ipcName does not exist
      std::cout << "The PixDbServerInterface " << ipcName 
		<< " does not exist or is not registered with the partition server.  " << std::endl;
    } catch(...) {
      std::cout << " Unknown exception was thrown, impossible to connect to DbServer " << ipcName << std::endl;
      ready=false;
      //exit(-1);
    }
    m_mutex=NULL;
    break;
  default:
    std::cout << "No valid type for DbServerInterface costructor." << std::endl;
    exit(-1);
    break;
  }
}


//! Destructor
PixDbServerInterface::~PixDbServerInterface() {
  // Withdraw from IPC partition
  if (m_mutex!=NULL) delete m_mutex;
  if (m_type==SERVER) {
    withdraw();
  }
}


///////////////////////////
// Local + Remote  methods
///////////////////////////

bool PixDbServerInterface::writeObject(PixDbDomain::rev_content &content, std::string domainName, std::string tag, std::string objName, std::string objType) {
  unsigned int revision = 0xffffffff; 
  return writeObject(content, domainName, tag, objName, objType, revision, false);
}

bool PixDbServerInterface::writeObject(PixDbDomain::rev_content &content, std::string domainName, std::string tag, std::string objName, std::string objType, unsigned int &revision, bool expert) {
  bool result=true;
  if (m_type == CLIENT) {
    ipc::ipc_revision_content ipcRevCont;
    RevContentToIPC(content,ipcRevCont);
    if (m_expertMode) expert = true;
    try {
      CORBA::ULong crev = revision;
      result=m_ipcInterface->ipc_writeObject(ipcRevCont, domainName.c_str(), tag.c_str(), objName.c_str(), objType.c_str(), crev, expert);
      if(!result) std::cout << "Error : PixDbServerInterface::writeObject = problem on server side!" << std::endl;
    } catch (...) {
      std::cout << "Error : PixDbServerInterface::writeObject = problem in IPC comunication" << std::endl;
      result=false;
    }
  } else {
    if (writeAllowed()) {
      std::map<std::string, PixDbDomain>::iterator Iter=m_domainList.find(domainName);
      if (Iter == m_domainList.end()) {
	PixDbDomain *dom = new PixDbDomain(domainName);
	addDomain(*dom);
	Iter = m_domainList.find(domainName);
	if (Iter == m_domainList.end()) {
	  std::cout << "ERROR  :  PixDbServerInterface::writeObject  :  cannot create domain  '" << domainName << std::endl;
	  result=false;
	}
	delete dom;
      }
      if (result) {
	m_mutex->write_lock();
	try {
	  Iter->second.writeObject(content, tag, objName, objType, revision, expert);
	} catch(PixDbDomainExc &p) {
	  p.dump();
	  result=false;
	}
	m_mutex->unlock(); 
      }
    } else {
      result = false;
    }
  }
  return result;
}  

bool PixDbServerInterface::getObject( PixDbDomain::rev_content &content, std::string domainName, std::string tag, std::string objName, unsigned int revision) {
  bool result=true;
  if (m_type == CLIENT) {
    ipc::ipc_revision_content ipcRevCont;
    //std::cout << "called getObjectRemote" << std::endl;
    try {
      result=m_ipcInterface->ipc_getObject(ipcRevCont, domainName.c_str(), tag.c_str(), objName.c_str(), revision);
      
      if(!result) 
	{
	  std::cout << "Error : PixDbServerInterface::getObject = problem on server side! " 
		    << "Obj:"<<objName<<"domain:"<<domainName<<"tag:"<<tag<< std::endl;
	}
    } catch (...) {
      std::cout << "Error : PixDbServerInterface::getObject = problem in IPC comunication for object " << objName << std::endl;
      result=false;
    }
    IPCToRevContent(content,ipcRevCont);
  } else {
    m_mutex->read_lock();
    std::map<std::string, PixDbDomain>::iterator Iter=m_domainList.find(domainName);
    if (Iter!=m_domainList.end()) {
      Iter->second.clearRevisionCont(content);
      try {
	Iter->second.getObject(content, tag, objName, revision);
      } catch(PixDbDomainExc &p) {
	p.dump();
	result = false;
      }
    }  else {
      std::cout << "ERROR  :  PixDbServerInterface::getObject   :  domain  '" << domainName << "' not found!! " << std::endl;
      result=false;
    }
    m_mutex->unlock();
  }
  return result;
}

bool PixDbServerInterface::cloneTag(std::string domainName, std::string inTag, std::string outTag, std::string pendTag) {
  bool res = true;
  if (m_type == CLIENT) {
    try {
      res = m_ipcInterface->ipc_cloneTag(domainName.c_str(), inTag.c_str(), outTag.c_str(), pendTag.c_str());
    } catch (...) {
      std::cout << "Error : PixDbServerInterface::cloneTag - problem in IPC comunication" << std::endl;
    }
  } else {
    if (writeAllowed()) {
      std::map<std::string, PixDbDomain>::iterator Iter=m_domainList.find(domainName);
      if (Iter == m_domainList.end()) {
	std::cout << "Error : PixDbServerInterface::cloneTag - domain " << domainName << " does not exists" << std::endl;
	res  =false;
      } else {
	m_mutex->write_lock();
	try {
	  Iter->second.cloneTag(inTag, outTag, pendTag);
	} 
	catch(PixDbDomainExc &p) {
	  p.dump();
	  res = false;
	}
	m_mutex->unlock(); 
      }
    } else {
      res = false;
    }
  }
  return res;
}

bool PixDbServerInterface::removeTag(std::string domainName, std::string inTag) {
  bool res = true;
  if (m_type == CLIENT) {
    try {
      res = m_ipcInterface->ipc_removeTag(domainName.c_str(), inTag.c_str());
    } catch (...) {
      std::cout << "Error : PixDbServerInterface::removeTag = problem in IPC comunication" << std::endl;
    }
  } else {
    std::map<std::string, PixDbDomain>::iterator Iter=m_domainList.find(domainName);
    if (Iter == m_domainList.end()) {
      std::cout << "Error : PixDbServerInterface::removeTag - domain " << domainName << " does not exists" << std::endl;
      res  =false;
    } else {
      m_mutex->write_lock();
      try {
	Iter->second.removeTag(inTag);
      } 
      catch(PixDbDomainExc &p) {
	p.dump();
	res = false;
      }
      m_mutex->unlock(); 
    }
  }
  return res;
}

bool PixDbServerInterface::purgeTag(std::string domainName, std::string inTag) {
  bool res = true;
  if (m_type == CLIENT) {
    try {
      res = m_ipcInterface->ipc_purgeTag(domainName.c_str(), inTag.c_str());
    } catch (...) {
      std::cout << "Error : PixDbServerInterface::purgeTag = problem in IPC comunication" << std::endl;
    }
  } else {
    std::map<std::string, PixDbDomain>::iterator Iter=m_domainList.find(domainName);
    if (Iter == m_domainList.end()) {
      std::cout << "Error : PixDbServerInterface::purgeTag - domain " << domainName << " does not exists" << std::endl;
      res  =false;
    } else {
      m_mutex->write_lock();
      try {
	Iter->second.purgeTag(inTag);
      } 
      catch(PixDbDomainExc &p) {
	p.dump();
	res = false;
      }
      m_mutex->unlock(); 
    }
  }
  return res;
}

bool PixDbServerInterface::setPending( std::string penTag, std::string domainName, std::string tag, std::string objName, unsigned int revision) {
  bool result=true;
  if (m_type == CLIENT) {
    try {
      result=m_ipcInterface->ipc_setPending(penTag.c_str(), domainName.c_str(), tag.c_str(), objName.c_str(), revision);
      if(!result) std::cout << "Error : PixDbServerInterface::setPending = problem on server side!" << std::endl;
    } catch (...) {
      std::cout << "Error : PixDbServerInterface::writeObjectRemote = problem in IPC comunication" << std::endl;
    }
  } else {
    m_mutex->write_lock();
    std::map<std::string, PixDbDomain>::iterator Iter=m_domainList.find(domainName);
    PixDbDomain::rev_content content;
    if (Iter!=m_domainList.end()) {
      (*Iter).second.clearRevisionCont(content);
      try {
	(*Iter).second.getObject(content, tag, objName, revision);
	content.pendingTag=penTag;
	std::string objType=(*Iter).second.getObjectType( tag, objName);
	(*Iter).second.writeObject(content, tag, objName, objType, revision, true);
      } catch(PixDbDomainExc &p) {
	p.dump();
      }
    }  else {
      std::cout << "ERROR  :  PixDbServerInterface::setPendingTag   :  domain  '" << domainName << "' not found!! " << std::endl;
    }
    m_mutex->unlock();
  }
  return result;
}

bool PixDbServerInterface::getPending( std::string &penTag, std::string domainName, std::string tag, std::string objName, unsigned int revision) {
  bool result=true;
  if (m_type == CLIENT) {
    CORBA::String_var tmpPen="default";
    try {
      result=m_ipcInterface->ipc_getPending(tmpPen, domainName.c_str(), tag.c_str(), objName.c_str(), revision);
      if(!result) std::cout << "Error : PixDbServerInterface::getPending = problem on server side!" << std::endl;
    } catch (...) {
      std::cout << "Error : PixDbServerInterface::getObjectRemote = problem in IPC comunication" << std::endl;
    }
    penTag=(const char*)tmpPen;
  } else {
    m_mutex->read_lock();
    std::map<std::string, PixDbDomain>::iterator Iter=m_domainList.find(domainName);
    PixDbDomain::rev_content content;
    if (Iter!=m_domainList.end()) {
      try {
	(*Iter).second.getObject(content, tag, objName, revision);
	penTag=content.pendingTag;
      } catch(PixDbDomainExc &p) {
	p.dump();
	result=false;
      }  
    } else {
      std::cout << "ERROR  :  PixDbServerInterface::getPendingTag   :  domain  '" << domainName << "' not found!! " << std::endl;
      result=false;
    }
    if (penTag=="") penTag="ROOT";
    m_mutex->unlock();
  }
  return result;
}

bool PixDbServerInterface::removePendingTag(std::string domainName, std::string pendTag, std::string tag) {
  bool res = true;
  if (m_type == CLIENT) {
    try {
      res = m_ipcInterface->ipc_removePendingTag(domainName.c_str(), pendTag.c_str(), tag.c_str());
    } catch (...) {
      std::cout << "Error : PixDbServerInterface::removePendingTag = problem in IPC comunication" << std::endl;
    }
  } else {
    std::map<std::string, PixDbDomain>::iterator Iter=m_domainList.find(domainName);
    if (Iter == m_domainList.end()) {
      std::cout << "Error : PixDbServerInterface::removePendingTag - domain " << domainName << " does not exists" << std::endl;
      res  =false;
    } else {
      m_mutex->write_lock();
      try {
	Iter->second.removePendingTag(pendTag, tag);
      } 
      catch(PixDbDomainExc &p) {
	p.dump();
	res = false;
      }
      m_mutex->unlock(); 
    }
  }
  return res;
}

bool PixDbServerInterface::renamePendingTag(std::string domainName, std::string pendTag, std::string newPendTag, std::string tag, std::string type) {
  bool res = true;
  if (m_type == CLIENT) {
    try {
      res = m_ipcInterface->ipc_renamePendingTag(domainName.c_str(), pendTag.c_str(), newPendTag.c_str(), tag.c_str(), type.c_str());
    } catch (...) {
      std::cout << "Error : PixDbServerInterface::renamePendingTag = problem in IPC comunication" << std::endl;
    }
  } else {
    std::map<std::string, PixDbDomain>::iterator Iter=m_domainList.find(domainName);
    if (Iter == m_domainList.end()) {
      std::cout << "Error : PixDbServerInterface::renamePendingTag - domain " << domainName << " does not exists" << std::endl;
      res  =false;
    } else {
      m_mutex->write_lock();
      try {
	Iter->second.renamePendingTag(pendTag, newPendTag, tag, type);
      } 
      catch(PixDbDomainExc &p) {
	p.dump();
	res = false;
      }
      m_mutex->unlock(); 
    }
  }
  return res;
}


///////////////////////////
// local  methods
///////////////////////////

void PixDbServerInterface::addDomain(PixDbDomain &newDomain) {
  //std::cout << "called addDomain !!!" << std::endl;
  std::string domainName=newDomain.getName();
  std::map<std::string, PixDbDomain>::iterator Iter=m_domainList.find(domainName);
  if(Iter==m_domainList.end()) {
    m_mutex->write_lock();
    std::pair<std::string, PixDbDomain> mypair(domainName,newDomain);
    m_domainList.insert(mypair);
    m_mutex->unlock();
  } else { 
    std::cout << "- Domain  '" << domainName << "'  already exists" << std::endl;
  }
}

void PixDbServerInterface::listDomain(std::vector<std::string> &List) {
  m_mutex->read_lock();
  std::map<std::string, PixDbDomain>::iterator Iter;
  List.clear();
  for (Iter=m_domainList.begin() ; Iter!=m_domainList.end() ; Iter++) {
    List.push_back((*Iter).first);
  }
  m_mutex->unlock();
}


void PixDbServerInterface::listTags(std::string domainName, std::vector<std::string> &List) {
  m_mutex->read_lock();
  std::map<std::string, PixDbDomain>::iterator Iter=m_domainList.find(domainName);
  List.clear();
  if (Iter!=m_domainList.end()) {
    try {
      (*Iter).second.listTags(List);
    } catch(PixDbDomainExc &p) {
      p.dump();
    }
  }  else {
    std::cout << "ERROR  :  PixDbServerInterface::listTags  :  domain  '" << domainName << "' not found!! " << std::endl;
  }
  m_mutex->unlock();
}

void PixDbServerInterface::listObjects(std::string domainName, std::string tag, std::vector<std::string> &List) {
  m_mutex->read_lock();
  std::map<std::string, PixDbDomain>::iterator Iter=m_domainList.find(domainName);
  List.clear();
  if (Iter!=m_domainList.end()) {
    try {
      (*Iter).second.listObjects(tag,List);
    } catch(PixDbDomainExc &p) {
      p.dump();
    }
  }  else {
    std::cout << "ERROR  :  PixDbServerInterface::listObjects  :  domain  '" << domainName << "' not found!! " << std::endl;
  }
  m_mutex->unlock();
}

void PixDbServerInterface::listObjects(std::string domainName, std::string tag, std::string objType, std::vector<std::string> &List, std::vector<std::string> &Type) {
  m_mutex->read_lock();
  std::map<std::string, PixDbDomain>::iterator Iter=m_domainList.find(domainName);
  List.clear();
  Type.clear();
  if (Iter!=m_domainList.end()) {
    try {
      (*Iter).second.listObjects(tag, objType, List, Type);
    } catch(PixDbDomainExc &p) {
      p.dump();
    }
  }  else {
    std::cout << "ERROR  :  PixDbServerInterface::listObjects  :  domain  '" << domainName << "' not found!! " << std::endl;
  }
  m_mutex->unlock();
}

void PixDbServerInterface::listRevisions(std::string domainName, std::string tag, std::string objName, std::vector<unsigned int> &List) {
  m_mutex->read_lock();
  std::map<std::string, PixDbDomain>::iterator Iter=m_domainList.find(domainName);
  List.clear();
  if (Iter!=m_domainList.end()) {
    try {
      (*Iter).second.listRevisions(tag,objName,List);
    } catch(PixDbDomainExc &p) {
      p.dump();
    }
  }  else {
    std::cout << "ERROR  :  PixDbServerInterface::listRevisions  :  domain  '" << domainName << "' not found!! " << std::endl;
  }
  m_mutex->unlock();
}

PixDbDomain* PixDbServerInterface::getDomain(std::string domainName) {
  m_mutex->read_lock();
  std::cout << "called getDomain!!!" << std::endl;
  std::map<std::string, PixDbDomain>::iterator Iter=m_domainList.find(domainName);
  if(Iter!=m_domainList.end()) {
    return &((*Iter).second);
  } else { 
    std::cout << "domainName domain  '" << domainName << "'  does not exists" << std::endl;
    return NULL;
  }
  m_mutex->unlock();
}

void PixDbServerInterface::setReadEnable(bool value) {
  m_readEnable=value;
}

bool PixDbServerInterface::writeAllowed() {
  std::map<std::string, PixDbDomain>::iterator Iter;
  unsigned int size=0;
  for (Iter=m_domainList.begin() ; Iter!=m_domainList.end() ; Iter++) {
    size += (*Iter).second.getSize();
  }
  if (size/1024 > 2300000) { 
    return false;
  } else {
    return true;
  }
}

bool PixDbServerInterface::loadFromFile(std::string filename) {
  m_mutex->write_lock();
  /*
  if(filename=="defaultName") filename=m_filename;
  std::ifstream in;
  in.open(filename.c_str(), std::ios::in | std::ios::binary);
  if (!in) {
    std::cout << "Error  :  PixDbServerInterface::loadFromFile  :  Unable to open file "<< filename.c_str() << std::endl;
    return false;
  }
  std::cout << " called loadFromFile with filename: " <<  filename << std::endl;
  
  unsigned int ndomain;
  unsigned int len;
  char *str;
  in.read((char*)(&ndomain),sizeof(unsigned int));
  std::string domName;
  for (unsigned int d=0; d<ndomain; d++) {
    in.read((char*)(&len),sizeof(unsigned int));
    str=new char[len+1];
    in.getline(str,sizeof(char[len+1]));
    domName=(std::string)str;
    delete str;
    PixDbDomain myDom(domName);
    try {
      myDom.getFromFile(in);
    } catch(PixDbDomainExc &p) {
      p.dump();
    }
    std::pair<std::string, PixDbDomain> mypairDom(domName,myDom);
    m_domainList.insert(mypairDom);
    myDom.eraseAll();
  }
  in.close();
  */
  m_mutex->unlock();
  return true;
}

bool PixDbServerInterface::writeOnFile(std::string filename) {
  m_mutex->read_lock();
  std::cout << std::endl << "called write on file Local " << std::endl;
  /*
  if(filename=="defaultName") filename=m_filename;
  std::ofstream out(filename.c_str(), std::ios::out | std::ios::binary);
  if (!out) {
    std::cout << "Error  :  PixDbServerInterface::writeOnFile  :  Unable to open file "<< m_filename << std::endl;
    return false;
  }
  unsigned int len;
  unsigned int size=(unsigned int)(m_domainList.size());
  std::map<std::string, PixDbDomain>::iterator Iter;
  out.write((char*)(&size), sizeof(unsigned int));
  for (Iter=m_domainList.begin() ; Iter!=m_domainList.end() ; Iter++) {
    bool good;
    len=(*Iter).first.length();
    out.write((char*)(&len), sizeof(unsigned int));
    out << (*Iter).first << "\n";
    PixDbDomain* mydom=getDomain((*Iter).first);
    if (mydom==NULL)  std::cout << "failed to get domain " <<  (*Iter).first << std::endl;
    std::cout << "try to write domain " <<  (*Iter).first << std::endl;
    good=mydom->printOnFile(out);
    if(!good) {
      std::cout << "Error  :  PixDbServerInterface::writeOnFile : Problem while writing domain '" <<  (*Iter).first << "'  !" << std::endl;
      return false;
    }
    out << std::endl;
  }
  out.close();
  */
  m_mutex->unlock();
  return true;
}

void PixDbServerInterface::printAll() {
  m_mutex->read_lock();
  std::cout << "Called PixDbServerInterface::printAll " << std::endl;
  std::map<std::string, PixDbDomain>::iterator Iter;
  for (Iter=m_domainList.begin() ; Iter!=m_domainList.end() ; Iter++) {
    std::cout << "- Domain:   " <<  (*Iter).first  << std::endl;
    (*Iter).second.printAll();
  }
  m_mutex->unlock();
}


///////////////////////////
// remote  methods
///////////////////////////

void PixDbServerInterface::listDomainRem(std::vector<std::string> &List) {
  List.clear();
  ipc::ipc_string_vec ipc_list;
  bool convert=true;
  try {
    m_ipcInterface->ipc_listDomain(ipc_list);
  } catch (...) {
    std::cout << "Error : PixDbServerInterface::listDomain = problem in IPC comunication" << std::endl;
    convert=false;
  }
  if (convert)  IPCToStringList(List,ipc_list);
}


void PixDbServerInterface::listTypes(std::string domainName, std::string tag, std::vector<std::string> &List, std::vector<unsigned int> &Count) {
  List.clear();
  Count.clear();
  ipc::ipc_string_vec ipc_list;
  ipc::ipc_unsigned_vec ipc_count;
  bool convert=true;
  try {
    m_ipcInterface->ipc_listTypes(domainName.c_str(), tag.c_str(), ipc_list, ipc_count);
  } catch (...) {
    std::cout << "Error : PixDbServerInterface::listTypes = problem in IPC comunication" << std::endl;
    convert=false;
  }
  if (convert)  {
    IPCToStringList(List,ipc_list);
    IPCToUnsignedList(Count,ipc_count);
  }
}

void PixDbServerInterface::listTagsRem(std::string domainName, std::vector<std::string> &List) {
  List.clear();
  ipc::ipc_string_vec ipc_list;
  bool convert=true;
  try {
    m_ipcInterface->ipc_listTags(domainName.c_str(),ipc_list);
  } catch (...) {
    std::cout << "Error : PixDbServerInterface::listTags = problem in IPC comunication" << std::endl;
    convert=false;
  }
  if (convert)  IPCToStringList(List,ipc_list);
}

void PixDbServerInterface::listObjectsRem(std::string domainName, std::string tag, std::vector<std::string> &List) {
  List.clear();
  ipc::ipc_string_vec ipc_list;
  bool convert=true;
  try {
    m_ipcInterface->ipc_listObjects(domainName.c_str(),tag.c_str(), ipc_list);
  } catch (...) {
    std::cout << "Error : PixDbServerInterface::listObjects = problem in IPC comunication" << std::endl;
    convert=false;
  }
  if (convert)  IPCToStringList(List,ipc_list);
}


void PixDbServerInterface::listObjectsRem(std::string domainName, std::string tag, std::string objType, std::vector<std::string> &List) {
  List.clear();
  ipc::ipc_string_vec ipc_list;
  ipc::ipc_string_vec ipc_type;
  bool convert=true;
  try {
    m_ipcInterface->ipc_listObjects_type(domainName.c_str(),tag.c_str(), objType.c_str(), ipc_list, ipc_type);
  } catch (...) {
    std::cout << "Error : PixDbServerInterface::listObjectType = problem in IPC comunication" << std::endl;
    convert=false;
  }
  if (convert)  IPCToStringList(List,ipc_list);
}

void PixDbServerInterface::listObjectsRem(std::string domainName, std::string tag, std::string objType, std::vector<std::string> &List, std::vector<std::string> &Type) {
  List.clear();
  Type.clear();
  ipc::ipc_string_vec ipc_list;
  ipc::ipc_string_vec ipc_type;
  bool convert=true;
  try {
    m_ipcInterface->ipc_listObjects_type(domainName.c_str(),tag.c_str(), objType.c_str(), ipc_list, ipc_type);
  } catch (...) {
    std::cout << "Error : PixDbServerInterface::listObjectType = problem in IPC comunication" << std::endl;
    convert=false;
  }
  if (convert)  {
    IPCToStringList(List,ipc_list);
    IPCToStringList(Type,ipc_type);
  }
}

void PixDbServerInterface::listRevisionsRem(std::string domainName, std::string tag, std::string objName, std::vector<unsigned int> &List) {
  List.clear();
  ipc::ipc_unsigned_vec ipc_list;
  bool convert=true;
  try {
    m_ipcInterface->ipc_listRevisions(domainName.c_str(),tag.c_str(), objName.c_str(), ipc_list);
  } catch (...) {
    std::cout << "Error : PixDbServerInterface::listTags = problem in IPC comunication" << std::endl;
    convert=false;
  }
  if (convert) {
    for (unsigned int size=0; size!=ipc_list.length(); size++)
      List.push_back(ipc_list[size]);
  }
}

unsigned int PixDbServerInterface::getLastRev(std::string domainName, std::string tag, std::string objName) {
  unsigned int max=0;
  ipc::ipc_unsigned_vec ipc_list;
  bool convert=true;
  try {
    m_ipcInterface->ipc_listRevisions(domainName.c_str(),tag.c_str(), objName.c_str(), ipc_list);
  } catch (...) {
    std::cout << "Error : PixDbServerInterface::listTags = problem in IPC comunication" << std::endl;
    convert=false;
  }
  if (convert) {
    for (unsigned int size=0; size!=ipc_list.length(); size++) {
      if (ipc_list[size]>=max) max=ipc_list[size];
    }
    return max;
  } else return 0;
}

void PixDbServerInterface::getType(std::string domainName, std::string tag, std::string objName, std::string &type) {
}

void PixDbServerInterface::addLoadList(PixDbDomain::obj_id &obj) {
  if (m_type == CLIENT) {
    try {
      ipc::obj_id ipcObj;
      ipcObj.domain = obj.domain.c_str();
      ipcObj.tag = obj.tag.c_str();
      ipcObj.rev = obj.rev;
      ipcObj.name = obj.name.c_str();
      ipcObj.type = obj.type.c_str();
      m_ipcInterface->ipc_addLoadList(ipcObj);
    } catch (...) {
      std::cout << "Error : PixDbServerInterface::addLoadList = problem in IPC comunication" << std::endl;
    }
  }
}

void PixDbServerInterface::getLoadList(std::string &id, PixDbDomain::obj_id &obj) {
  CORBA::String_var tmpId=id.c_str();
  if (m_type == CLIENT) {
    try {
      ipc::obj_id ipcObj;
      m_ipcInterface->ipc_getLoadList(tmpId, ipcObj);
      obj.domain = ipcObj.domain;
      obj.tag = ipcObj.tag;
      obj.rev = ipcObj.rev;
      obj.name = ipcObj.name;
      obj.type = ipcObj.type;
      id = tmpId;
    } catch (...) {
      std::cout << "Error : PixDbServerInterface::getLoadList = problem in IPC comunication" << std::endl;
    }
  }
}

void PixDbServerInterface::listLoadList(std::vector<std::string> &List) {
  List.clear();
  ipc::ipc_string_vec ipc_list;
  bool convert=true;
  try {
    m_ipcInterface->ipc_listLoadList(ipc_list);
  } catch (...) {
    std::cout << "Error : PixDbServerInterface::listLoadList = problem in IPC comunication" << std::endl;
    convert=false;
  }
  if (convert)  IPCToStringList(List,ipc_list);
}

void PixDbServerInterface::listLoadListFail(std::vector<std::string> &List, std::vector<unsigned int> &fail) {
}

void PixDbServerInterface::cleanLoadList(const std::string id) {
  if (m_type == CLIENT) {
    try {
      m_ipcInterface->ipc_cleanLoadList(id.c_str());
    } catch (...) {
      std::cout << "Error : PixDbServerInterface::cleanLoadList = problem in IPC comunication" << std::endl;
    }
  }
}

void PixDbServerInterface::restoreLoadList(const std::string id) {
  if (m_type == CLIENT) {
    try {
      m_ipcInterface->ipc_restoreLoadList(id.c_str());
    } catch (...) {
      std::cout << "Error : PixDbServerInterface::restoreLoadList = problem in IPC comunication" << std::endl;
    }
  }
}

unsigned int PixDbServerInterface::getLoadListLen() {
  unsigned int size = 999999;
  if (m_type == CLIENT) {
    try {
      size = m_ipcInterface->ipc_getLoadListLen();
    } catch (...) {
      std::cout << "Error : PixDbServerInterface::getLoadListLen = problem in IPC comunication" << std::endl;
    }
  }
  return size;
}

void PixDbServerInterface::updateSaveList() {
  if (m_type == CLIENT) {
    try {
      m_ipcInterface->ipc_updateSaveList();
    } catch (...) {
      std::cout << "Error : PixDbServerInterface::updateSaveList = problem in IPC comunication" << std::endl;
    }
  }
}

void PixDbServerInterface::getSaveList(std::string &id, PixDbDomain::obj_id &obj) {
  CORBA::String_var tmpId=id.c_str();
  if (m_type == CLIENT) {
    try {
      ipc::obj_id ipcObj;
      ipcObj.domain = obj.domain.c_str();
      ipcObj.tag = obj.tag.c_str();
      ipcObj.rev = obj.rev;
      ipcObj.name = obj.name.c_str();
      ipcObj.type = obj.type.c_str();
      m_ipcInterface->ipc_getSaveList(tmpId, ipcObj);
    } catch (...) {
      std::cout << "Error : PixDbServerInterface::getSaveList = problem in IPC comunication" << std::endl;
    }
  }
}

void PixDbServerInterface::listSaveList(std::vector<std::string> &List) {
  List.clear();
  ipc::ipc_string_vec ipc_list;
  bool convert=true;
  try {
    m_ipcInterface->ipc_listSaveList(ipc_list);
  } catch (...) {
    std::cout << "Error : PixDbServerInterface::listSaveList = problem in IPC comunication" << std::endl;
    convert=false;
  }
  if (convert)  IPCToStringList(List,ipc_list);
}


void PixDbServerInterface::listSaveListFail(std::vector<std::string> &List, std::vector<unsigned int> &fail) {
}

void PixDbServerInterface::cleanSaveList(const std::string id) {
  if (m_type == CLIENT) {
    try {
      m_ipcInterface->ipc_cleanSaveList(id.c_str());
    } catch (...) {
      std::cout << "Error : PixDbServerInterface::cleanSaveList = problem in IPC comunication" << std::endl;
    }
  }
}

void PixDbServerInterface::restoreSaveList(const std::string id) {
  if (m_type == CLIENT) {
    try {
      m_ipcInterface->ipc_restoreSaveList(id.c_str());
    } catch (...) {
      std::cout << "Error : PixDbServerInterface::restoreSaveList = problem in IPC comunication" << std::endl;
    }
  }
}

unsigned int PixDbServerInterface::getSaveListLen() {
  unsigned int size = 999999;
  if (m_type == CLIENT) {
    try {
      size = m_ipcInterface->ipc_getSaveListLen();
    } catch (...) {
      std::cout << "Error : PixDbServerInterface::getSaveListLen = problem in IPC comunication" << std::endl;
    }
  }
  return size;
}

void PixDbServerInterface::memoryReport(std::vector<std::string> &tags, std::vector<unsigned int> &nobj, std::vector<unsigned int> &nrev, std::vector<unsigned int> &size, 
                                        unsigned int &narev, unsigned int &revsize) {
  tags.clear();
  nobj.clear();
  nrev.clear();
  size.clear();
  narev = 0;
  revsize = 0;


  if (m_type == CLIENT) {
    ipc::ipc_string_vec ipc_tags;
    ipc::ipc_unsigned_vec ipc_nobj;
    ipc::ipc_unsigned_vec ipc_nrev;
    ipc::ipc_unsigned_vec ipc_size;
    ipc_tags.length(0);
    ipc_nobj.length(0);
    ipc_nrev.length(0);
    ipc_size.length(0);

    CORBA::ULong ipc_narev=0, ipc_revsize=0;
    bool convert=true;
    try {
      m_ipcInterface->ipc_memoryReport(ipc_tags, ipc_nobj, ipc_nrev, ipc_size, ipc_narev, ipc_revsize);
    } catch (...) {
      std::cout << "Error : PixDbServerInterface::memoryReport = problem in IPC comunication" << std::endl;
      convert=false;
    }
    if (convert)  {
      IPCToStringList(tags, ipc_tags);
      IPCToUnsignedList(nobj, ipc_nobj);
      IPCToUnsignedList(nrev, ipc_nrev);
      IPCToUnsignedList(size, ipc_size);
      narev = ipc_narev;
      revsize = ipc_revsize;
    }
  } else {
    m_mutex->read_lock();
    for (auto dom : m_domainList) {
      dom.second.memoryReport(tags, nobj, nrev, size, narev, revsize);
    }
    m_mutex->unlock();
  }
}

void PixDbServerInterface::setReadEnableRem(bool value) {
  try {
    m_ipcInterface->ipc_setReadEnable(value);
  } catch (...) {
    std::cout << "Error : PixDbServerInterface::setReadEnable = problem in IPC comunication" << std::endl;
  }
}

/* // method with callback (not working properly)
void PixDbServerInterface::ready() {
  PixDbServerCallback *cb = NULL;
  cb = new PixDbServerCallback;
  //  ipc::PixDbServerCallback_ptr cbptr = cb->_this();

  bool ready=m_ipcInterface->ipc_getReadEnable();

  if (!ready) {
    try {
      //m_ipcInterface->ipc_setCallback(cbptr);
      m_ipcInterface->ipc_setCallback(cb->_this());
    } catch( CORBA::SystemException & ex ) {
      std::cout << "" << ex._name() <<std::endl;
    }
    std::cout << "  ....  waiting for server to be enable for reading ... " << std::endl;
    cb->run();
    //std::cout << "  ....  server is ready for reading but before destroy!! " << std::endl;
    cb->_destroy();
    std::cout << "  ....  server is ready for reading!! " << std::endl;
  } else { 
    std::cout << " server is already enable for reading!! " << std::endl;
  }
}
*/

void PixDbServerInterface::ready() {
  std::cout << " entering ready remote " << std::endl;
  bool ready=m_ipcInterface->ipc_getReadEnable();
  if (!ready) {
    std::cout << "  ....  waiting for server to be enable for reading ... " << std::endl;
    do { 
      sleep(1);
      ready=m_ipcInterface->ipc_getReadEnable();
    } while (!ready);
    std::cout << "  ....  server is ready for reading!! " << std::endl;
  } else { 
    std::cout << " server is already enable for reading!! " << std::endl;
  }
}

bool PixDbServerInterface::writeInFile(std::string fileName) {
  //std::cout << std::endl << "called write in file REMOTE " << std::endl;
  bool result;
  try {
    result=m_ipcInterface->ipc_cashInFile(fileName.c_str());
    if(!result)  std::cout << "Error : PixDbServerInterface::writeInFile = failed to write the file" << std::endl;
  } catch (...) {
    std::cout << "Error : PixDbServerInterface::writeInFile = problem in IPC comunication" << std::endl;
    result=false;
  }
  return result;
}


///////////////////////////
// ipc  methods
///////////////////////////

bool PixDbServerInterface::ipc_writeObject(ipc::ipc_revision_content &con, const char *domainName, const char *tag, const char *objName, const char* objType, CORBA::ULong &revision, bool expert) {
  PixDbDomain::rev_content revCont;
  IPCToRevContent(revCont, con);
  bool result=writeObject(revCont, domainName, tag, objName, objType, (unsigned int&)revision, expert);
  return result;
}

bool PixDbServerInterface::ipc_getObject(ipc::ipc_revision_content &con, const char *domainName, const char *tag, const char *objName, CORBA::ULong revision) {
  PixDbDomain::rev_content content;
  while(!m_readEnable) sleep(1);
  bool result=getObject(content, domainName, tag, objName, revision);
  RevContentToIPC(content, con);
  return result;
}

bool PixDbServerInterface::ipc_cloneTag(const char* domainName, const char* inTag, const char* outTag, const char* pendTag) {
  while(!m_readEnable) sleep(1);
  bool res = cloneTag(domainName, inTag, outTag, pendTag);
  return res;
}

bool PixDbServerInterface::ipc_removeTag(const char* domainName, const char* inTag) {
  while(!m_readEnable) sleep(1);
  bool res = removeTag(domainName, inTag);
  return res;
}
					
bool PixDbServerInterface::ipc_purgeTag(const char* domainName, const char* inTag) {
  while(!m_readEnable) sleep(1);
  bool res = purgeTag(domainName, inTag);
  return res;
}
		     
bool PixDbServerInterface::ipc_getPending(char* &penTag, const char *domainName, const char *tag, const char *objName, CORBA::ULong revision) { 
  std::string tmpPend;
  bool result=getPending(tmpPend, domainName, tag, objName, revision);
  if ( strlen(penTag)>=tmpPend.length() )    
    strcpy( penTag, tmpPend.c_str() );   
  else {
    CORBA::string_free(penTag);   
    penTag = CORBA::string_dup( tmpPend.c_str() );  
  }
  return result;
}


bool PixDbServerInterface::ipc_setPending(const char *penTag, const char *domainName, const char *tag, const char *objName, CORBA::ULong revision) {
  while(!m_readEnable) sleep(1);
  bool result=setPending(penTag, domainName, tag, objName, revision);
  return result;
}

bool PixDbServerInterface::ipc_removePendingTag(const char* domainName, const char* pendTag, const char* tag) {
  while(!m_readEnable) sleep(1);
  bool res = removePendingTag(domainName, pendTag, tag);
  return res;
}

bool PixDbServerInterface::ipc_renamePendingTag(const char* domainName, const char* pendTag, const char* newPendTag, const char* tag, const char* type) {
  while(!m_readEnable) sleep(1);
  bool res = renamePendingTag(domainName, pendTag, newPendTag, tag, type);
  return res;
}

void PixDbServerInterface::ipc_listDomain(ipc::ipc_string_vec &list) {
  std::vector<std::string> namelist;
  //while(!m_readEnable) sleep(1);
  listDomain(namelist);
  StringListToIPC(namelist,list);
  std::cout << " .. exiting list domain .. " << std::endl;
}

void PixDbServerInterface::ipc_listTypes(const char *domainName, const char *tag, ipc::ipc_string_vec &list, ipc::ipc_unsigned_vec &count) {
  std::vector<std::string> namelist;
  while(!m_readEnable) sleep(1);
  m_mutex->read_lock();
  std::map<std::string, PixDbDomain> ::iterator Iter=m_domainList.find(domainName);
  if (Iter!=m_domainList.end()) {
    std::map<std::string, unsigned int> types;
    try {
      types = (*Iter).second.getTypeList(tag);
    } 
    catch(PixDbDomainExc &p) {
      p.dump();
    }
    std::map<std::string, unsigned int>::const_iterator it;
    unsigned int i=0;
    list.length(types.size());
    count.length(types.size());
    for (it=types.begin(); it!=types.end(); it++)  {
      list[i]=CORBA::string_dup(it->first.c_str());
      count[i]=it->second;
      i++;
    }
  }  else {
    std::cout << "ERROR  :  PixDbServerInterface::listTypes  :  domain  '" << domainName << "' not found!! " << std::endl;
  }
  m_mutex->unlock();
}

void PixDbServerInterface::ipc_listTags(const char *domainName, ipc::ipc_string_vec& list) {
  std::vector<std::string> namelist;
  //while(!m_readEnable) sleep(1);
  listTags(domainName, namelist);
  StringListToIPC(namelist,list);
}

void PixDbServerInterface::ipc_listObjects(const char *domainName, const char *tag, ipc::ipc_string_vec& list) {
  std::vector<std::string> namelist;
  while(!m_readEnable) sleep(1);
  listObjects(domainName, tag, namelist);
  StringListToIPC(namelist,list);
}

void PixDbServerInterface::ipc_listObjects_type(const char *domainName, const char *tag, const char* objtype, ipc::ipc_string_vec& list, ipc::ipc_string_vec& type) {
  std::vector<std::string> namelist;
  std::vector<std::string> typelist;
  while(!m_readEnable) sleep(1);
  listObjects(domainName, tag, objtype, namelist, typelist);
  StringListToIPC(namelist,list);
  StringListToIPC(typelist,type);
}

void PixDbServerInterface::ipc_listRevisions(const char *domainName, const char *tag, const char *obj, ipc::ipc_unsigned_vec &list) {
  std::vector<unsigned int> namelist;
  while(!m_readEnable) sleep(1);
  listRevisions(domainName, tag, obj, namelist);
  unsigned int n=0;
  list.length(namelist.size());
  for (std::vector<unsigned int>::iterator myIt=namelist.begin();myIt!=namelist.end(); myIt++) {
    list[n]=(*myIt);
    n++;
  }
}

void PixDbServerInterface::ipc_getType(const char *domainName, const char *tag, const char *objName, char* &type) {
}

void PixDbServerInterface::ipc_addLoadList(ipc::obj_id &obj) {
  m_mutex->write_lock();
  std::ostringstream os;
  os << obj.domain << "/" << obj.tag << "/" << obj.rev << "/" << obj.name << "/" << obj.type;
  PixDbDomain::obj_id pixObj;
  pixObj.domain = obj.domain;
  pixObj.tag = obj.tag;
  pixObj.rev = obj.rev;
  pixObj.name = obj.name;
  pixObj.type = obj.type;  
  std::pair<std::string, PixDbDomain::obj_id> pp(os.str(), pixObj);
  std::map<std::string, PixDbDomain::obj_id>::iterator it = m_loadList.find(os.str());
  if (it == m_loadList.end()) {
    std::map<std::string, PixDbDomain::obj_id>::iterator it1 = m_loadListProc.find(os.str());
    if (it1 == m_loadListProc.end()) {
      bool found = false;
      std::map<std::string, PixDbDomain>::iterator domIter=m_domainList.find(pixObj.domain);
      if (domIter!=m_domainList.end()) {
	if (domIter->second.objectExists(pixObj.tag, pixObj.name, pixObj.rev)) {
	  found = true;
	}
      }
      if (!found) m_loadList.insert(pp);
    }
  }
  m_mutex->unlock();
}

void PixDbServerInterface::ipc_getLoadList(char* &id, ipc::obj_id &obj) {
  m_mutex->write_lock();
  std::map<std::string, PixDbDomain::obj_id>::iterator it = m_loadList.begin();
  if (it != m_loadList.end()) {
    std::string oname = it->first;
    PixDbDomain::obj_id pobj = it->second;
    std::pair<std::string, PixDbDomain::obj_id> pp(oname, pobj);
    obj.domain = pobj.domain.c_str();
    obj.tag = pobj.tag.c_str();
    obj.rev = pobj.rev;
    obj.name = pobj.name.c_str();
    obj.type = pobj.type.c_str();
    m_loadList.erase(it);
    m_loadListProc.insert(pp);
    CORBA::string_free(id);   
    id = CORBA::string_dup( oname.c_str() );  
  } else {
    CORBA::string_free(id);   
    id = CORBA::string_dup( "" );  
  }
  m_mutex->unlock();
}

void PixDbServerInterface::ipc_listLoadList(ipc::ipc_string_vec &list) {
  std::map<std::string, PixDbDomain::obj_id>::const_iterator it;
  unsigned int i=0;
  m_mutex->read_lock();
  list.length(m_loadList.size()+m_loadListProc.size());
  for (it=m_loadList.begin(); it!=m_loadList.end(); it++)  {
    list[i++]=CORBA::string_dup(it->first.c_str());
  }
  for (it=m_loadListProc.begin(); it!=m_loadListProc.end(); it++)  {
    std::string name = it->first + " (PROC)";
    list[i++]=CORBA::string_dup(name.c_str());
  }
  m_mutex->unlock();
}

void PixDbServerInterface::ipc_listLoadListFail(ipc::ipc_string_vec &list, ipc::ipc_unsigned_map &fail) {
}

void PixDbServerInterface::ipc_cleanLoadList(const char* id) {
  m_mutex->write_lock();
  std::string sid = id;
  std::map<std::string, PixDbDomain::obj_id>::iterator it = m_loadListProc.find(sid);
  if (it != m_loadListProc.end()) {
    m_loadListProc.erase(it);
  }
  m_mutex->unlock();
}

void PixDbServerInterface::ipc_restoreLoadList(const char* id) {
  m_mutex->write_lock();
  std::string sid = id;
  std::map<std::string, PixDbDomain::obj_id>::iterator it = m_loadListProc.find(sid);
  if (it != m_loadListProc.end()) {
    std::pair<std::string, PixDbDomain::obj_id> pp(it->first, it->second);
    m_loadList.insert(pp);
    m_loadListProc.erase(it);
  }
  m_mutex->unlock();
}

CORBA::ULong PixDbServerInterface::ipc_getLoadListLen() {
  return m_loadList.size();
}

void PixDbServerInterface::ipc_updateSaveList() {
  m_mutex->write_lock();
  std::map<std::string, PixDbDomain>::iterator itd;
  for (itd=m_domainList.begin(); itd!=m_domainList.end(); ++itd) {
    std::vector<std::string> ltags;
    try {
      itd->second.listTags(ltags);
    } catch(PixDbDomainExc &p) {
      p.dump();
      ltags.clear();
    }
    std::vector<std::string>::iterator itt;
    for (itt=ltags.begin(); itt!=ltags.end(); ++itt) { 
      std::vector<std::string> lobjs;
      std::vector<std::string> types;
      std::string type;
      try {
	itd->second.listObjects(*itt, type, lobjs, types);
      } catch(PixDbDomainExc &p) {
	p.dump();
	lobjs.clear();
      }
      std::vector<std::string>::iterator ito;
      for (ito=lobjs.begin(); ito!=lobjs.end(); ++ito) { 
	std::vector<unsigned int> lrevs;
	try {
	  itd->second.listRevisions(*itt, *ito, lrevs);
	} catch(PixDbDomainExc &p) {
	  p.dump();
	  lrevs.clear();
	}
	std::vector<unsigned int>::iterator itr;
	for (itr=lrevs.begin(); itr!=lrevs.end(); ++itr) { 
	  PixDbDomain::rev_content content;
	  itd->second.getObject(content, *itt, *ito, *itr);
	  std::string ptag=content.pendingTag;
	  if (ptag.find("_Tmp")== std::string::npos && ptag!="OnDB") {
	    PixDbDomain::obj_id pobj;
	    pobj.domain = itd->first;
	    pobj.tag = *itt;
	    pobj.name = *ito;
	    pobj.rev = *itr;
	    pobj.type = type;
	    std::ostringstream os;
	    os << pobj.domain << "/" << pobj.tag << "/" << pobj.rev << "/" << pobj.name << "/" << pobj.type;
	    std::pair<std::string, PixDbDomain::obj_id> pp(os.str(), pobj);
	    std::map<std::string, PixDbDomain::obj_id>::iterator it = m_saveList.find(os.str());
	    if (it != m_saveList.end()) {
	      std::map<std::string, PixDbDomain::obj_id>::iterator it1 = m_saveListProc.find(os.str());
	      if (it1 != m_saveListProc.end()) {
		m_saveList.insert(pp);
	      }
	    }
	  }
	}
      }
    }
  }
  m_mutex->unlock();
}

void PixDbServerInterface::ipc_getSaveList(char* &id, ipc::obj_id &obj) {
  m_mutex->write_lock();
  std::map<std::string, PixDbDomain::obj_id>::iterator it = m_saveList.begin();
  if (it != m_saveList.end()) {
    std::string oname = it->first;
    PixDbDomain::obj_id pobj = it->second;
    std::pair<std::string, PixDbDomain::obj_id> pp(oname, pobj);
    obj.domain = pobj.domain.c_str();
    obj.tag = pobj.tag.c_str();
    obj.rev = pobj.rev;
    obj.name = pobj.name.c_str();
    obj.type = pobj.type.c_str();
    m_saveList.erase(it);
    m_saveListProc.insert(pp);
    CORBA::string_free(id);   
    id = CORBA::string_dup( oname.c_str() );  
  } else {
    CORBA::string_free(id);   
    id = CORBA::string_dup( "" );  
  }
  m_mutex->unlock();
}

void PixDbServerInterface::ipc_listSaveList(ipc::ipc_string_vec &list) {
  std::map<std::string, PixDbDomain::obj_id>::iterator it;
  unsigned int i=0;
  m_mutex->read_lock();
  list.length(m_saveList.size()+m_saveListProc.size());
  for (it=m_saveList.begin(); it!=m_saveList.end(); it++)  {
    list[i++]=CORBA::string_dup(it->first.c_str());
  }
  for (it=m_saveListProc.begin(); it!=m_saveListProc.end(); it++)  {
    std::string name = it->first + " (PROC)";
    list[i++]=CORBA::string_dup(name.c_str());
  }
  m_mutex->unlock();
}

void PixDbServerInterface::ipc_listSaveListFail(ipc::ipc_string_vec &list, ipc::ipc_unsigned_map &fail) {
}

void PixDbServerInterface::ipc_cleanSaveList(const char* id) {
  m_mutex->write_lock();
  std::string sid = id;
  std::map<std::string, PixDbDomain::obj_id>::iterator it = m_saveListProc.find(sid);
  if (it != m_saveListProc.end()) {
    m_saveListProc.erase(it);
  }
  m_mutex->unlock();
}

void PixDbServerInterface::ipc_restoreSaveList(const char* id) {
  m_mutex->write_lock();
  std::string sid = id;
  std::map<std::string, PixDbDomain::obj_id>::iterator it = m_saveListProc.find(sid);
  if (it != m_saveListProc.end()) {
    std::pair<std::string, PixDbDomain::obj_id> pp(it->first, it->second);
    m_saveList.insert(pp);
    m_saveListProc.erase(it);
  }
  m_mutex->unlock();
}

CORBA::ULong PixDbServerInterface::ipc_getSaveListLen() {
  return m_saveList.size();
}

void PixDbServerInterface::ipc_memoryReport(ipc::ipc_string_vec &tags, ipc::ipc_unsigned_vec &nobj, ipc::ipc_unsigned_vec &nrev, ipc::ipc_unsigned_vec &size, 
                                            CORBA::ULong &narev, CORBA::ULong &revsize) {
  std::vector<std::string> c_tags;
  std::vector<unsigned int> c_nobj;
  std::vector<unsigned int> c_nrev;
  std::vector<unsigned int> c_size;
  unsigned int c_narev=0, c_revsize=0;

  m_mutex->read_lock();
  std::map<std::string, PixDbDomain>::iterator Iter;
  for (Iter=m_domainList.begin() ; Iter!=m_domainList.end() ; Iter++) {
    (*Iter).second.memoryReport(c_tags, c_nobj, c_nrev, c_size, c_narev, c_revsize);
  }
  StringListToIPC(c_tags, tags);
  UnsignedListToIPC(c_nobj, nobj);
  UnsignedListToIPC(c_nrev, nrev);
  UnsignedListToIPC(c_size, size);
  narev = c_narev;
  revsize = c_revsize;
  m_mutex->unlock();
}

void PixDbServerInterface::ipc_setReadEnable(bool value) {
  m_readEnable=value;
  //if (m_cb!=NULL) std::cout << "la callback NON e' nulla!!!!" << std::endl;
  /*
  if (value) {
    std::cout << "callback vector contains " << m_cb.size() << " objects " << std::endl;
    std::vector<ipc::PixDbServerCallback_var>::iterator it;
    for(it=m_cb.begin();it!=m_cb.end();it++) {
      (*it)->retStop();
      m_cb.erase(it);
    }
  }
  */
  std::cout << " enableReadout is " <<  m_readEnable << std::endl;
}

bool PixDbServerInterface::ipc_getReadEnable() { 
  //sleep(10);
  return m_readEnable;
}

void PixDbServerInterface::ipc_ready() {
  std::cout << std::endl << " 1 client is connected to this PixDbServerInterface server!!" << std::endl << std::endl;
  return;
}

bool PixDbServerInterface::ipc_cashInFile(const char* file) {
  //std::cout << std::endl << " called ipc_cash in File " << std::endl;
  while(!m_readEnable) sleep(1);
  bool result;
  if (writeOnFile(file))
    result=true;
  else 
    result=false;
  return result;
}

void PixDbServerInterface::ipc_setCallback(ipc::PixDbServerCallback_ptr cb) {
  std::cout << "Setting callback" << std::endl;
  ipc::PixDbServerCallback_var tmp_cb = ipc::PixDbServerCallback::_duplicate(cb);
  //m_cbNumber = m_cb->countConnections();
  m_cb.push_back(tmp_cb);
}

void PixDbServerInterface::ipc_unsetCallback() {
  // Get callback and save number of current connection
  if(m_cb.size() !=0){
    m_cb.clear();
    //m_cb = NULL;
  }
}


/*
void PixDbServerInterface::removeDomain(std::string domainName) {
  std::map<std::string, PixDbDomain>::iterator Iter=m_domainList.find(domainName);
  if(Iter!=m_domainList.end()) { 
    m_domainList.erase(Iter);
  } else { 
    std::cout << "ERROR  :  PixDbServerInterface::removeDomain  :  domain '" << domainName << "'  not found" << std::endl;
  }
}

void PixDbServerInterface::removeTag(std::string domainName, std::string tag) {
  std::map<std::string, PixDbDomain>::iterator Iter=m_domainList.find(domainName);
  if(Iter!=m_domainList.end()) { 
    try {
      (*Iter).second.removeTag(tag);
    } catch(PixDbDomainExc &p) {
      p.dump();
    }
  } else { 
    std::cout << "ERROR  :  PixDbServerInterface::removeDomain  :  domain '" << domainName << "'  not found" << std::endl;
  }
}

// ipc methods
void PixDbServerInterface::ipc_setFileName(const char *name) {
  m_filename=name;
  return;
}

void PixDbServerInterface::ipc_removeDomain(const char *type) {
  removeDomain(type);
  return;
}

void PixDbServerInterface::ipc_removeTag(const char *type, const char *tag) {
  removeTag(type,tag);
  return;
}

void PixDbServerInterface::ipc_cashInFile(const char* file) {
  writeOnFile(file);
  return;
}

*/

///////////////////////////
//  Conversion Methods
///////////////////////////

void PixDbServerInterface::RevContentToIPC(const PixDbDomain::rev_content &con, ipc::ipc_revision_content &con_ipc) {
  con_ipc.revision=con.revision;
  con_ipc.useCount=con.useCount;
  con_ipc.pendingTag=con.pendingTag.c_str();
  std::map<std::string, bool>::const_iterator  b_it;
  con_ipc.bool_c.length(con.bool_content.size());
  int i=0;
  for (b_it=con.bool_content.begin(); b_it!=con.bool_content.end(); b_it++)  {
    con_ipc.bool_c[i].name=(*b_it).first.c_str();
    con_ipc.bool_c[i].value=(*b_it).second;
    i++;
  }
  i=0;
  std::map<std::string, int>::const_iterator i_it;
  con_ipc.int_c.length(con.int_content.size());
  for (i_it=con.int_content.begin(); i_it!=con.int_content.end(); i_it++)  {
    con_ipc.int_c[i].name=(*i_it).first.c_str();
    con_ipc.int_c[i].value=(*i_it).second;
    i++;
  }
  i=0;
  std::map<std::string, unsigned int>::const_iterator u_it;
  con_ipc.unsigned_c.length(con.unsigned_content.size());
  for (u_it=con.unsigned_content.begin(); u_it!=con.unsigned_content.end(); u_it++)  {
    con_ipc.unsigned_c[i].name=(*u_it).first.c_str();
    con_ipc.unsigned_c[i].value=(*u_it).second;
    i++;
  }
  i=0;
  std::map<std::string, float>::const_iterator f_it;
  con_ipc.float_c.length(con.float_content.size());
  for (f_it=con.float_content.begin(); f_it!=con.float_content.end(); f_it++)  {
    con_ipc.float_c[i].name=(*f_it).first.c_str();
    con_ipc.float_c[i].value=(*f_it).second;
    i++;
  }
  i=0;
  std::map<std::string, std::string>::const_iterator s_it;
  con_ipc.string_c.length(con.string_content.size());
  for (s_it=con.string_content.begin(); s_it!=con.string_content.end(); s_it++)  {
    con_ipc.string_c[i].name=(*s_it).first.c_str();
    con_ipc.string_c[i].value=(*s_it).second.c_str();
    i++;
  }
  std::map<std::string, std::vector<unsigned int> >::const_iterator vec_it;
  i=0;
  (con_ipc.vect_c).length(con.vect_content.size());
  for (vec_it=con.vect_content.begin(); vec_it!=con.vect_content.end(); vec_it++) {
    (con_ipc.vect_c[i]).name=(*vec_it).first.c_str();    
    ((con_ipc.vect_c[i]).value).length(((*vec_it).second).size());
    for (unsigned int dim=0; dim<((*vec_it).second).size();dim++){
      (con_ipc.vect_c[i]).value[dim]=(*vec_it).second[dim];
    }
    i++;
  }
  std::vector<PixDbDomain::link_object>::const_iterator l_it;
  i=0;
  con_ipc.uplink_c.length(con.upLink_content.size());
  for (l_it=con.upLink_content.begin(); l_it!=con.upLink_content.end(); l_it++)  {
    con_ipc.uplink_c[i].linkName=(*l_it).linkName.c_str();
    con_ipc.uplink_c[i].objName=(*l_it).objName.c_str();
    con_ipc.uplink_c[i].objType=(*l_it).objType.c_str();
    con_ipc.uplink_c[i].revision=(*l_it).revision; 
    i++;
  }
  i=0;
  con_ipc.downlink_c.length(con.downLink_content.size());
  for (l_it=con.downLink_content.begin(); l_it!=con.downLink_content.end(); l_it++)  {
    con_ipc.downlink_c[i].linkName=(*l_it).linkName.c_str();
    con_ipc.downlink_c[i].objName=(*l_it).objName.c_str();
    con_ipc.downlink_c[i].objType=(*l_it).objType.c_str();
    con_ipc.downlink_c[i].revision=(*l_it).revision;
    i++;
  }
}

void PixDbServerInterface::IPCToRevContent(PixDbDomain::rev_content &con, const ipc::ipc_revision_content &con_ipc) {
  con.revision=con_ipc.revision;
  con.useCount=con_ipc.useCount;
  std::ostringstream ospen;
  ospen << con_ipc.pendingTag;
  con.pendingTag=ospen.str();

  unsigned int size=0;
  for (size=0; size!=con_ipc.bool_c.length(); size++) {
    std::ostringstream os;
    os << con_ipc.bool_c[size].name;
    con.bool_content[os.str()]=con_ipc.bool_c[size].value;
  }
  for (size=0; size!=con_ipc.int_c.length(); size++) {
    std::ostringstream os;
    os << con_ipc.int_c[size].name;
    con.int_content[os.str()]=con_ipc.int_c[size].value;
  }
  for (size=0; size!=con_ipc.unsigned_c.length(); size++) {
    std::ostringstream os;
    os << con_ipc.unsigned_c[size].name;
    con.unsigned_content[os.str()]=con_ipc.unsigned_c[size].value;
  }
  for (size=0; size!=con_ipc.float_c.length(); size++) {
    std::ostringstream os;
    os << con_ipc.float_c[size].name;
    con.float_content[os.str()]=con_ipc.float_c[size].value;
  } 
  for (size=0; size!=con_ipc.string_c.length(); size++) {
    std::ostringstream os,os2;
    os << con_ipc.string_c[size].name;
    os2 << con_ipc.string_c[size].value;
    con.string_content[os.str()]=os2.str();
  }
  std::vector<unsigned int> tmp;
  for (size=0; size<con_ipc.vect_c.length(); size++) {
    tmp.clear();
    for (unsigned int size2=0; size2!=((con_ipc.vect_c[size]).value).length(); size2++) {
      tmp.push_back(((con_ipc.vect_c[size]).value)[size2]);
    }
    std::ostringstream os;
    os << con_ipc.vect_c[size].name;
    con.vect_content[os.str()]=tmp;
  }
  for (size=0; size!=con_ipc.uplink_c.length(); size++) {
    std::ostringstream os1,os2,os3;
    os1 << con_ipc.uplink_c[size].linkName;
    os2 << con_ipc.uplink_c[size].objName;
    os3 << con_ipc.uplink_c[size].objType;
    PixDbDomain::link_object link;
    link.linkName=os1.str();
    link.objName=os2.str();
    link.objType=os3.str();
    link.revision= con_ipc.uplink_c[size].revision;
    con.upLink_content.push_back(link);
  }
  for (size=0; size!=con_ipc.downlink_c.length(); size++) {
    std::ostringstream os1,os2,os3;
    os1 << con_ipc.downlink_c[size].linkName;
    os2 << con_ipc.downlink_c[size].objName;
    os3 << con_ipc.downlink_c[size].objType;
    PixDbDomain::link_object link;
    link.linkName=os1.str();
    link.objName=os2.str();
    link.objType=os3.str();
    link.revision= con_ipc.downlink_c[size].revision;
    con.downLink_content.push_back(link);
  }
}


void PixDbServerInterface::StringListToIPC(const std::vector<std::string> &Slist,ipc::ipc_string_vec &ipc_list) {
  std::vector<std::string>::const_iterator it;
  unsigned int i=0;
  ipc_list.length(Slist.size());
  for (it=Slist.begin(); it!=Slist.end(); it++)  {
    ipc_list[i]=CORBA::string_dup((*it).c_str());
    i++;
  }
}

void PixDbServerInterface::IPCToStringList(std::vector<std::string> &Slist, const ipc::ipc_string_vec &ipc_list) {
  for (unsigned int size=0; size!=ipc_list.length(); size++) {
    Slist.push_back((std::string)(ipc_list[size]));
  }
}

void PixDbServerInterface::UnsignedListToIPC(const std::vector<unsigned int> &Slist,ipc::ipc_unsigned_vec &ipc_list) {
  std::vector<unsigned int>::const_iterator it;
  unsigned int i=0;
  ipc_list.length(Slist.size());
  for (it=Slist.begin(); it!=Slist.end(); it++)  {
    ipc_list[i]=*(it);
    i++;
  }
}

void PixDbServerInterface::IPCToUnsignedList(std::vector<unsigned int> &list, const ipc::ipc_unsigned_vec &ipc_list) {
  for (unsigned int size=0; size!=ipc_list.length(); size++) {
    list.push_back(ipc_list[size]);
  }
}

void PixDbServerInterface::printRevContent(const PixDbDomain::rev_content &content) {
  //std::cout  << "Revision_content ! " << std::endl;
  std::cout << "   -> content revision is " << content.revision  << std::endl;
  std::cout << "   -> content pendingTag is " << content.pendingTag  << std::endl;
  std::cout << "   -> content useCount is " << content.useCount  << std::endl;
  std::cout << "   -> bool_map has " << content.bool_content.size() << " entries!!" << std::endl;
  std::map<std::string, bool>::const_iterator b_it;
  for (b_it=(content.bool_content).begin(); b_it!=(content.bool_content).end(); b_it++) {
    std::cout << "      -- Name:   " <<  (*b_it).first  << "        value: " <<  (*b_it).second <<  std::endl;
  }
  std::cout << "   -> int_map has " << content.int_content.size() << " entries!!" << std::endl;
  std::map<std::string, int>::const_iterator i_it;
  for (i_it=content.int_content.begin(); i_it!=content.int_content.end(); i_it++) {
    std::cout << "      -- Name:   " <<  (*i_it).first  << "        value: " <<  (*i_it).second <<  std::endl;
  }
  std::cout << "   -> unsigned_map has " << content.unsigned_content.size() << " entries!!" << std::endl;
  std::map<std::string, unsigned int>::const_iterator u_it;
  for (u_it=content.unsigned_content.begin(); u_it!=content.unsigned_content.end(); u_it++) {
    std::cout << "      -- Name:   " <<  (*u_it).first  << "        value: " <<  (*u_it).second <<  std::endl;
  }
  std::cout << "   -> float_map has " << content.float_content.size() << " entries!!" << std::endl;
  std::map<std::string, float>::const_iterator f_it;
  for (f_it=content.float_content.begin(); f_it!=content.float_content.end(); f_it++) {
    std::cout << "      -- Name:   " <<  (*f_it).first  << "        value: " <<  (*f_it).second <<  std::endl;
  }
  std::cout << "   -> string_map has " << content.string_content.size() << " entries!!" << std::endl;
  std::map<std::string, std::string>::const_iterator s_it;
  for (s_it=content.string_content.begin(); s_it!=content.string_content.end(); s_it++) {
    std::cout << "      -- Name:   " <<  (*s_it).first  << "        value: " <<  (*s_it).second <<  std::endl;
  }
  std::cout << "   -> vect_map has " << content.vect_content.size() << " entries!!" << std::endl;
  std::map<std::string, std::vector<unsigned int> >::const_iterator v_it;
  for (v_it=content.vect_content.begin(); v_it!=content.vect_content.end(); v_it++) {
    std::cout << "      -- Name:   " <<  (*v_it).first  << "  has " <<  (*v_it).second.size() << " entries!!" << std::endl;
    for (unsigned int dim=0; dim<(*v_it).second.size();dim++){
      std::cout << "         -- Value  " << dim << ":  " << (*v_it).second[dim] << std::endl;
    }
  }
  std::cout << "   -> uplink_map has " << content.upLink_content.size() << " entries!!" << std::endl;
  std::vector<PixDbDomain::link_object>::const_iterator l_it;
  int i=0;
  for (l_it=content.upLink_content.begin(); l_it!=content.upLink_content.end(); l_it++) {
    i++;
    std::cout << "      -- upLink "<< i << " with name: " << (*l_it).linkName  << "  typeobj: " <<   (*l_it).objType << "  nameobj: " << (*l_it).objName << "  revision: " <<  (*l_it).revision << std::endl;
  }
  std::cout << "   -> downlink_map has " << content.downLink_content.size() << " entries!!" << std::endl;
  i=0;
  for (l_it=content.downLink_content.begin(); l_it!=content.downLink_content.end(); l_it++) {
    i++;
    std::cout << "      -- downLink "<< i << " with name: " << (*l_it).linkName  << "  typeobj: " <<   (*l_it).objType << "  nameobj: " << (*l_it).objName << "  revision: " <<  (*l_it).revision << std::endl;
  }
}

void PixDbServerInterface::printIPCRevContent(const ipc::ipc_revision_content &con) {
  std::cout << "   -> content revision is " << con.revision  << std::endl;
  std::cout << "   -> content pendingTag is " << con.pendingTag  << std::endl;
  
  std::cout << "   -> bool_map has " << con.bool_c.length() << " entries" << std::endl;
  unsigned int i=0;
  for (i=0; i<con.bool_c.length(); i++) {
    std::cout << "      -- Name:   " <<  con.bool_c[i].name  << "        value: " <<  con.bool_c[i].value <<  std::endl;
  }
  std::cout << "   -> int_map has " << con.int_c.length() << " entries!!" << std::endl;
  for (i=0; i<con.int_c.length(); i++) {
    std::cout << "      -- Name:   " <<  con.int_c[i].name  << "        value: " <<  con.int_c[i].value <<  std::endl;
  }
  std::cout << "   -> unsigned_map has " << con.unsigned_c.length() << " entries!!" << std::endl;
  for (i=0; i<con.unsigned_c.length(); i++) {
    std::cout << "      -- Name:   " <<  con.unsigned_c[i].name  << "        value: " <<  con.unsigned_c[i].value <<  std::endl;
  }
  std::cout << "   -> float_map has " << con.float_c.length() << " entries!!" << std::endl;
  for (i=0; i<con.float_c.length(); i++) {
    std::cout << "      -- Name:   " <<  con.float_c[i].name  << "        value: " <<  con.float_c[i].value <<  std::endl;
  }
  std::cout << "   -> string_map has " << con.string_c.length() << " entries!!" << std::endl;
  for (i=0; i<con.string_c.length(); i++) {
    std::cout << "      -- Name:   " <<  con.string_c[i].name  << "        value: " <<  con.string_c[i].value <<  std::endl;
  }
  std::cout << "   -> vector_map has " << con.vect_c.length() << " entries!!" << std::endl;
  for (i=0; i<con.vect_c.length(); i++) {
    std::cout << "      -- Name:   " <<  con.vect_c[i].name  << "    with " <<  con.vect_c[i].value.length() << " entries" <<  std::endl;
    for (unsigned int u=0; u<con.vect_c[i].value.length(); u++) {
      std::cout << "        -- index: " << u << "    value:  " <<  (con.vect_c[i]).value[u] << std::endl;
    }  
  }
  std::cout << "   -> uplink_map has " << con.uplink_c.length() << " entries!!" << std::endl;
  for (i=0; i<con.uplink_c.length(); i++) {
    std::cout << "      -- upLink "<< i << " with name: " << con.uplink_c[i].linkName  << "  obj: " << con.uplink_c[i].objName << "  revision: " <<  con.uplink_c[i].revision << std::endl;
  }
  std::cout << "   -> downlink_map has " << con.downlink_c.length() << " entries!!" << std::endl;
  for (i=0; i<con.downlink_c.length(); i++) {
    std::cout << "      -- downLink "<< i << " with name: " << con.downlink_c[i].linkName  << "  obj: " << con.downlink_c[i].objName << "  revision: " <<  con.downlink_c[i].revision << std::endl;
  }
}

