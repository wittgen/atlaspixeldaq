/////////////////////////////////////////////////////////////////////
// PixDbServerInterface.h
// version 1.0.0
/////////////////////////////////////////////////////////////////////
//
// 02/10/07  Version 1.0 (VD)
//           Initial release
//

#ifndef _PIXLIB_DBSERVERINTERFACE 
#define _PIXLIB_DBSERVERINTERFACE

#include <ipc/partition.h>
#include <ipc/object.h>
#include <owl/mutexrw.h>

#include "BaseException.h"
#include "PixDbServer/PixDbDomain.h"
#include "PixDbServer/PixDbServerCallback.h"
#include "PixDbServerInterfaceIDL.hh"

namespace PixLib {
  class PixDbServerInterface : public IPCNamedObject<POA_ipc::PixDbServerInterface,ipc::multi_thread> {
    
  public: 
    
    enum serviceType { SERVER, CLIENT };
    
    // Constructors
    PixDbServerInterface(IPCPartition *ipcPartition, std::string ipcName, serviceType serviceType,  bool &ready, std::string cashingFileName="/home/vdao/PixDbServer_cashing_file");
    
    // Distructor
    virtual ~PixDbServerInterface();

    // Local+Remote methods
    bool writeObject(PixDbDomain::rev_content &content, std::string domainName, std::string tag, std::string objName, std::string objType);
    bool writeObject(PixDbDomain::rev_content &content, std::string domainName, std::string tag, std::string objName, std::string objType, unsigned int &revision, bool expert=false);
    bool getObject(PixDbDomain::rev_content &content, std::string domainName, std::string tag, std::string objName, unsigned int revision=0xffffffff);
    bool cloneTag(std::string domainName, std::string inTag, std::string outTag, std::string pendTag);
    bool removeTag(std::string domainName, std::string inTag);
    bool purgeTag(std::string domainName, std::string inTag);
    bool getPending(std::string &penTag, std::string domainName, std::string tag, std::string objName, unsigned int revision); 
    bool setPending(std::string penTag, std::string domainName, std::string tag, std::string objName, unsigned int revision); 
    bool removePendingTag(std::string domainName, std::string pendTag, std::string tag);
    bool renamePendingTag(std::string domainName, std::string pendTag, std::string newPendTag, std::string tag, std::string type);

    // Local methods
    void addDomain(PixDbDomain &newDomain);
    void listDomain(std::vector<std::string> &List);
    void listTags(std::string domainName, std::vector<std::string> &List);
    void listObjects(std::string domainName, std::string tag, std::vector<std::string> &List);
    void listObjects(std::string domainName, std::string tag, std::string objType, std::vector<std::string> &List, std::vector<std::string> &Type);
    void listRevisions(std::string domainName, std::string tag, std::string objName, std::vector<unsigned int> &List);
    PixDbDomain* getDomain(std::string type);
    void setExpertMode(bool mode) { m_expertMode = mode; } 
    void setReadEnable(bool value);
    bool getEnable() { return m_readEnable; };
    bool writeAllowed();
    bool loadFromFile(std::string filename ="defaultName"); // deprecate, to be updated
    bool writeOnFile(std::string filename ="defaultName");  // depracate, to be updated
    void printAll();

    // Remote methods
    void listDomainRem(std::vector<std::string> &List);
    void listTypes(std::string domainName, std::string tag, std::vector<std::string> &List, std::vector<unsigned int> &count);
    void listTagsRem(std::string domainName, std::vector<std::string> &List);
    void listObjectsRem(std::string domainName, std::string tag, std::vector<std::string> &List);
    void listObjectsRem(std::string domainName, std::string tag, std::string objType, std::vector<std::string> &List);
    void listObjectsRem(std::string domainName, std::string tag, std::string objType, std::vector<std::string> &List, std::vector<std::string> &Type);
    void listRevisionsRem(std::string domainName, std::string tag, std::string objName, std::vector<unsigned int> &List);
    unsigned int getLastRev(std::string domainName, std::string tag, std::string objName);
    void getType(std::string domainName, std::string tag, std::string objName, std::string &type);
    void addLoadList(PixDbDomain::obj_id &obj);
    void getLoadList(std::string &id, PixDbDomain::obj_id &obj);
    void listLoadList(std::vector<std::string> &List);
    void listLoadListFail(std::vector<std::string> &List, std::vector<unsigned int> &fail);
    void cleanLoadList(const std::string id);
    void restoreLoadList(const std::string id);
    unsigned int getLoadListLen();
    void updateSaveList();
    void getSaveList(std::string &id, PixDbDomain::obj_id &obj);
    void listSaveList(std::vector<std::string> &List);
    void listSaveListFail(std::vector<std::string> &List, std::vector<unsigned int> &fail);
    void cleanSaveList(const std::string id);
    void restoreSaveList(const std::string id);
    unsigned int getSaveListLen();
    void memoryReport(std::vector<std::string> &tags, std::vector<unsigned int> &nobj, std::vector<unsigned int> &nrev, std::vector<unsigned int> & size, 
                      unsigned int &narev, unsigned int &revsize);
    void setReadEnableRem(bool value);
    void ready();
    bool writeInFile(std::string fileName);
    
    // IPC methods
    bool ipc_writeObject(ipc::ipc_revision_content &con, const char *domainName, const char *tag, const char *objName, const char* objType, CORBA::ULong &revision, bool expert);
    bool ipc_getObject(ipc::ipc_revision_content &content, const char *domainName, const char *tag, const char *objName, CORBA::ULong revision);
    bool ipc_cloneTag(const char* domainName, const char* inTag, const char* outTag, const char* pendTag);
    bool ipc_removeTag(const char* domainName, const char* inTag);
    bool ipc_purgeTag(const char* domainName, const char* inTag);
    bool ipc_getPending(char* &penTag, const char *domainName, const char *tag, const char *objName, CORBA::ULong revision); 
    bool ipc_setPending(const char *penTag, const char *domainName, const char *tag, const char *objName, CORBA::ULong revision); 
    bool ipc_removePendingTag(const char* domainName, const char* pendTag, const char* tag);
    bool ipc_renamePendingTag(const char* domainName, const char* pendTag, const char* newPensTag, const char* tag, const char* type);
    void ipc_listDomain(ipc::ipc_string_vec &list);
    void ipc_listTypes(const char *domainName, const char *tag, ipc::ipc_string_vec &list, ipc::ipc_unsigned_vec &count); 
    void ipc_listTags(const char *domainName, ipc::ipc_string_vec &list); 
    void ipc_listObjects(const char *domainName, const char *tag, ipc::ipc_string_vec &list);
    void ipc_listObjects_type(const char *domainName, const char *tag,  const char *objtype, ipc::ipc_string_vec &list, ipc::ipc_string_vec &type);
    void ipc_listRevisions(const char *domainName, const char *tag, const char *objName, ipc::ipc_unsigned_vec &list);
    void ipc_getType(const char *domainName, const char *tag, const char *objName, char* &type);
    void ipc_addLoadList(ipc::obj_id &obj);
    void ipc_getLoadList(char* &id, ipc::obj_id &obj);
    void ipc_listLoadList(ipc::ipc_string_vec &list);
    void ipc_listLoadListFail(ipc::ipc_string_vec &list, ipc::ipc_unsigned_map &nfail);
    void ipc_cleanLoadList(const char* id);
    void ipc_restoreLoadList(const char* id);
    CORBA::ULong ipc_getLoadListLen();
    void ipc_updateSaveList();
    void ipc_getSaveList(char* &id, ipc::obj_id &obj);
    void ipc_listSaveList(ipc::ipc_string_vec &list);
    void ipc_listSaveListFail(ipc::ipc_string_vec &list, ipc::ipc_unsigned_map &nfail);
    void ipc_cleanSaveList(const char* id);
    void ipc_restoreSaveList(const char* id);
    CORBA::ULong ipc_getSaveListLen();
    void ipc_memoryReport(ipc::ipc_string_vec &tags, ipc::ipc_unsigned_vec &nobj, ipc::ipc_unsigned_vec &nrev, ipc::ipc_unsigned_vec &size, 
                          CORBA::ULong &narev, CORBA::ULong &revsize);
    void ipc_memoryReport(ipc::ipc_string_vec &tags);
    void ipc_setReadEnable(bool value);
    bool ipc_getReadEnable();
    void ipc_ready();
    bool ipc_cashInFile(const char *file);
    void ipc_setCallback(ipc::PixDbServerCallback_ptr cb);
    void ipc_unsetCallback();

    // Conversion Methods and utilities
    void RevContentToIPC(const PixDbDomain::rev_content &con, ipc::ipc_revision_content &con_ipc);
    void IPCToRevContent(PixDbDomain::rev_content &con, const ipc::ipc_revision_content &con_ipc);
    void StringListToIPC(const std::vector<std::string> &Slist,ipc::ipc_string_vec &ipc_list);
    void IPCToStringList(std::vector<std::string> &Slist,const ipc::ipc_string_vec &ipc_list);
    void UnsignedListToIPC(const std::vector<unsigned int> &Slist,ipc::ipc_unsigned_vec &ipc_list);
    void IPCToUnsignedList(std::vector<unsigned int> &list, const ipc::ipc_unsigned_vec &ipc_list);
    void printRevContent(const PixDbDomain::rev_content &content);
    void printIPCRevContent(const ipc::ipc_revision_content &con);
  
  public:  
    // IPC shutdown
    virtual void shutdown() {}
    
  protected:    
    // SW interfaces
    IPCPartition *m_ipcPartition;
    ipc::PixDbServerInterface_var m_ipcInterface;
    std::map<std::string, PixDbDomain> m_domainList;
    std::string m_filename;
    serviceType m_type;
    bool m_readEnable;
    std::map<std::string, PixDbDomain::obj_id> m_loadList;
    std::map<std::string, PixDbDomain::obj_id> m_loadListProc;
    std::map<std::string, PixDbDomain::obj_id> m_saveList;
    std::map<std::string, PixDbDomain::obj_id> m_saveListProc;
    OWLMutexRW* m_mutex;
    bool m_expertMode;

    // Callback variables
    std::vector<ipc::PixDbServerCallback_var> m_cb;
  };
}
#endif
