
#include "PixModuleGroup/PixModuleGroup.h"
#include "PixTrigController/TimPixTrigController.h"
#include "PixController/PixRunConfig.h"
#include "PixModule/PixModule.h"
#include "PixBoc/PixBoc.h"
#include "PixConnectivity/PixConnectivity.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "PixConnectivity/PixConfigDb.h"
#include "PixConnectivity/PixDisable.h"
#include "PixUtilities/PixLock.h"

#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cstdio>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <wait.h>
#include <memory>

using namespace PixLib;

//std::map<std::string, PixMutex*> PixMutex::m_objects = std::map<std::string, PixMutex*>();

std::string element(std::string str, int num, char sep=' ') {
  unsigned int ip;
  int start = -1,end = -1;
  int nel = -1;
  bool spc = true;
  bool quote = false;
                                                                                                                                        
  for (ip=0;ip<str.size();ip++) {
    if (str[ip] == '"') quote = !quote;
    if (str[ip] == sep || (sep == 0 && 
       (str[ip] == 9 || str[ip]==10 || str[ip] == 32))) {
      if (!spc && !quote) {
        spc = true;
        if (nel==num) end = ip-1;
      }
    } else {
      if (spc) {
        spc = false;
        nel++;
        if (nel==num) start = ip;
      }
    }
  }
  if (!spc && nel==num) end = str.size()-1;
                                                                                                                                        
  if (start >= 0) {
    std::string c;
    std::string s = str.substr(start,end-start+1);
    for (ip=0;ip<s.size();ip++) {
      if (s[ip] != '"') c += s[ip];
    }
    return c;
  } else {
    return "";
  }
}

std::string upcase(std::string s) {
  std::string s1;
  std::transform(s.begin(), s.end(), s.begin(), (int(*)(int))std::toupper);
  return s;
}

std::map <std::string, PixDbServerInterface*> dbs;
std::map <std::string, IPCPartition*> dbsPart;

PixDbServerInterface *DbServer = NULL;
bool DbServerMode = false;
std::string DbsFname;

void openDbServer(std::string part, std::string name) {
  std::string fname = part+"/"+name;
  if (dbs.find(fname) != dbs.end()) {
    DbsFname = fname;
    DbServer = dbs[fname];
  } else {
    if (dbsPart.find(part) == dbsPart.end()) {
      // Create IPCPartition constructors                                                              
      try {
        dbsPart[part] = new IPCPartition(part);
      } 
      catch(...) {
        throw(PixException("Cannot connect to partition "+part));
      }
    }
    bool ready=false;
    int nTry=0;
    if (dbsPart[part] != NULL) {
      do {
        DbServer = new PixDbServerInterface(dbsPart[part], name, PixDbServerInterface::CLIENT, ready);
        if (!ready) {
          delete DbServer;
        } else {
          break;
        }
        nTry++;
        sleep(1);
      } while (nTry<20);
      if (!ready) {
        throw(PixException("Cannot connect to DB server "+name+" in "+part));
      }
      DbsFname = fname;
      dbs[fname] = DbServer;
    }
  }
}

int main(int argc, char** argv) {
  try {
    time_t t0 = time(NULL);
    // Start IPCCore    
    IPCCore::init(argc, argv);
    // Create PixConfigDb object 
    PixConfigDb *confDb = new PixConfigDb();  
    std::cout << "Time to open " << "Base/Base" << " = " << time(NULL) - t0 << std::endl;
    std::string tagID = "Base";
    std::string tagCF = "Base";
    PixConnectivity *conn = NULL;

    bool loop = true;
    while (loop) {
      std::string cmd;
      if (DbServerMode) std::cout << "("+DbsFname+")/";
      std::cout << tagID+"/"+tagCF+" >>> ";
      getline(std::cin, cmd);
      std::string ncmd = upcase(element(cmd, 0));
      
      try {
        if (ncmd == "QUIT") {
          loop = false;
        } else if (ncmd == "HELP") {
          std::cout << "Possible commmands are: " << std::endl;
	  std::cout << "  QUIT" << std::endl;
	  std::cout << "  HELP" << std::endl;
          std::cout << "  DBSERVER ON" << std::endl;
          std::cout << "  DBSERVER OFF" << std::endl;
          std::cout << "  DBSERVER <part> <name>" << std::endl;
	  std::cout << "  LIST ID" << std::endl;
	  std::cout << "  LIST CFG" << std::endl;
	  std::cout << "  LIST OBJ" << std::endl;
	  std::cout << "  LIST REV <object>" << std::endl;
	  std::cout << "  LIST CYC <object> <rev>" << std::endl;
	  std::cout << "  SET ID <id tag>" << std::endl;
	  std::cout << "  SET CFG <cfg tag>" << std::endl;
	  std::cout << "  SET <cfg tag> < id tag>" << std::endl;
	  std::cout << "  SET <cfg tag>" << std::endl;
	  std::cout << "  DUMP <object>" << std::endl;
	  std::cout << "  READCONN <id> <tag>" << std::endl;
 	  std::cout << "  MODTODBS <object> <dest id> <dest tag> <rep>" << std::endl;
 	  std::cout << "  DBSMEMORY" << std::endl;
 	  std::cout << "  CLONETAG <id tag> <source tag> <dest tag> <pend tag>" << std::endl;
 	  std::cout << "  PURGETAG <id tag> <tag>" << std::endl;
 	  std::cout << "  REMOVETAG <id tag> <tag>" << std::endl;
        } else if (ncmd == "DBSERVER") {
          std::string p1 = upcase(element(cmd, 1));
          if (upcase(p1) == "ON") {
            if (DbServer != NULL) {
              DbServerMode = true;
              confDb->cfgFromDbServ();
            }
          } else if (upcase(p1) == "OFF") {
            DbServerMode = false;
            confDb->cfgFromDb();
          } else {
            std::string p1 = element(cmd, 1);
            std::string p2 = element(cmd, 2);
            if (p2 == "") p2 = "PixelDbServer";
	    openDbServer(p1, p2);
            DbServerMode = true;
            confDb->openDbServer(DbServer);
          }
        } else if (ncmd == "LIST") {
          std::string p1 = upcase(element(cmd, 1));
          if (p1 == "ID") {
            std::vector<std::string> list;
            list = confDb->listObjTags();
            std::cout << "ID tags : " << std::endl;
            for (std::string s : list) {
              std::cout << "  " << s << std::endl;
            }
          } else if (p1 == "CFG") {
            std::vector<std::string> list;
            list = confDb->listCfgTags(tagID);
            std::cout << "CFG tags in " << tagID << " : " << std::endl;
            for (std::string s : list) {
              std::cout << "  " << s << std::endl;
            }
          } else if (p1 == "OBJ") {
            std::vector<std::string> list;
            list = confDb->listObjects(tagCF,tagID);
            std::cout << "Objects in " << tagID << "/" << tagCF << " : " << std::endl;
            for (std::string s : list) {
              std::cout << "  " << s << " " << confDb->getObjectType(s, tagCF, tagID) << std::endl;
            }
          } else if (p1 == "REV") {
            std::string object = element(cmd, 2);
            std::vector<unsigned int> revs; 
            revs = confDb->listRevisions(object, tagCF, tagID);
            std::cout << "Revisions for " << object << " in " << tagID << "/" << tagCF << " : " << std::endl;
            for (unsigned int r : revs) {
              std::cout << "  " << r << std::endl;
            }
          } else if (p1 == "CYC") {
            std::string object = element(cmd, 2);
            std::istringstream srev(element(cmd, 3));
            unsigned int rev;
            srev >> rev;
            std::vector<unsigned int> cycles; 
            cycles = confDb->listCycles(object, rev, tagCF, tagID);
            std::cout << "Cycles for " << object << " rev " << rev << " in " << tagID << "/" << tagCF << " : " << std::endl;
            for (unsigned int c : cycles) {
              std::cout << "  " << c << std::endl;
            }
          }
        } else if (ncmd == "SET") {
          std::string oldID = tagID;
          std::string oldCF = tagCF;
          std::string p1 = element(cmd, 1);
          std::string p2 = element(cmd, 2);
          if (upcase(p1) == "ID") {
            tagID = p2;
          } else if (upcase(p1) == "CFG") {
            tagCF = p2;
          } else {
            tagCF = p1;
            if (p2 != "") tagID = p2;
          }
          t0 = time(NULL);
          try {
            confDb->setCfgTag(tagCF, tagID);
            std::cout << "Time to open " << tagID << "/" << tagCF << " = " << time(NULL) - t0 << std::endl;
          }
	  catch (PixConfigDbExc s) {
	    std::cout << s.dump() << std::endl;
            if (s.getType() == "IDNOTFOUND") {
              tagCF = oldCF;
              tagID = oldID;
            } else if (s.getType() == "TAGNOTFOUND") {
              tagCF = oldCF;
            } else {
              tagCF = oldCF;
              tagID = oldID;
            }
          }
        } else if (ncmd == "DUMP") {
          std::string object = element(cmd, 1);
          std::istringstream srev(element(cmd, 2));
          unsigned int rev;
          srev >> rev;
          std::string type = confDb->getObjectType(object, tagCF, tagID);
          if (type == "DISABLE_CFG") {
            PixDisable dis(object);
            confDb->readCfg(&(dis.config()), object, rev, tagCF, tagID);
            dis.config().dump(std::cout);
          } else if (type == "TIM_CFG") {
            TimPixTrigController tim(object);
            confDb->readCfg(tim.config(), object, rev, tagCF, tagID);
            tim.config()->dump(std::cout);
          } else if (type == "RUNCONF_CFG") {
            PixRunConfig runc(object);
            confDb->readCfg(&(runc.config()), object, rev, tagCF, tagID);
            runc.config().dump(std::cout);
          } else if (type == "RODBOC_CFG") {
            PixModuleGroup modg(object, UNKNOWN, false);
            confDb->readCfg(&(modg.config()), object, rev, tagCF, tagID);
            modg.config().dump(std::cout);
            if (modg.getPixController() != NULL) {
              modg.getPixController()->config().dump(std::cout);
            }
            if (modg.getPixBoc() != NULL) {
              modg.getPixBoc()->getConfig()->dump(std::cout);
            }
          }
        } else if (ncmd == "READCONN") { 
          std::string id = element(cmd, 1);
          std::string tag = element(cmd, 2);
	  if (DbServer != NULL) {
	    conn = new PixConnectivity(DbServer, tag, tag, tag, "Base", id);
	    conn->loadConn();
	  } else {
	    std::cout << "You need a DB server" << std::endl;
	  }
        } else if (ncmd == "MODTODBS") {
	  if (DbServer != NULL && conn != NULL) { 
	    std::string object = element(cmd, 1);
	    std::string destid = element(cmd, 2);
	    std::string desttag = element(cmd, 3);
	    std::istringstream srep(element(cmd, 4));
	    unsigned int rep;
	    srep >> rep;
	    std::string type = confDb->getObjectType(object, tagCF, tagID);
	    if (type == "MODULE_CFG") {
	      EnumMCCflavour::MCCflavour mccFlv = EnumMCCflavour::PM_NO_MCC;
	      EnumFEflavour::FEflavour feFlv   = EnumFEflavour::PM_NO_FE;
	      int nFe=0;
	      std::string modName = object;
	      std::string connName = "";
	      for (auto mod : conn->mods) {
		if (mod.second->prodId() == modName) {
		  connName = mod.first;
		  mod.second->getModPars(mccFlv, feFlv, nFe);
		}
	      }
	      if (connName == "") {
		if (conn->mods.find(modName) != conn->mods.end()) {
		  connName = modName;
		  modName = conn->mods[connName]->prodId();
		  conn->mods[connName]->getModPars(mccFlv, feFlv, nFe);
		}
	      }
	      if (connName != "") {
                unsigned int rev=0, nrev = time(NULL);                
		PixModule mod((PixDbServerInterface*)NULL, (PixModuleGroup*)NULL, tagID, tagCF, modName, mccFlv, feFlv, nFe);
		confDb->readCfg(&(mod.config()), modName, rev, tagCF, tagID);
                if (!DbServerMode) confDb->cfgFromDbServ();
                for (unsigned int i=0; i<rep; i++) {
		  confDb->writeCfg(&(mod.config()), modName, nrev, desttag, destid, "_Tmp");
                  nrev++;        
		  std::cout << "\rModule : " << setw(8) << i;
		}
		std::cout << std::endl;
                if (!DbServerMode) confDb->cfgFromDb();
	      }
	    }
	  } else {
	    std::cout << "You need a DB server and a connectivity" << std::endl;
	  }
        } else if (ncmd == "DBSMEMORY") {
	  if (DbServer != NULL) {
	    std::vector<std::string> tags;
	    std::vector<unsigned int> nobj;
	    std::vector<unsigned int> nrev;
	    std::vector<unsigned int> size;
	    unsigned int narev = 0;
	    unsigned int revsize = 0;
      	    DbServer->memoryReport(tags, nobj, nrev, size, narev, revsize);	    
	    unsigned int maxsize=0;
	    for (unsigned int i=0; i<tags.size(); i++) {
	      if (tags[i].size() > maxsize) maxsize = tags[i].size(); 
	    }
	    if (maxsize < 32) maxsize = 32;
	    std::string sepLine;
	    for (unsigned int i=0; i<maxsize+41; i++) sepLine+="=";
	    std::cout << std::endl;
	    std::cout << sepLine << std::endl;
	    std::cout << " Tag" << setw(maxsize+36) << "# objects   # of revs   Size (KB)" << std::endl;
	    std::cout << sepLine << std::endl;
	    for (unsigned int i=0; i<tags.size(); i++) {
	      std::cout << " " << tags[i] << setw(maxsize+3-tags[i].size()) << " ";
	      std::cout << std::setw(12) << nobj[i];
	      std::cout << std::setw(12) << nrev[i];
	      std::cout << std::setw(12) << size[i]/1024 << std::endl;
	    }
	    std::cout << sepLine << std::endl;
	    std::cout << " Total number of revisions ";
	    std::cout << setw(12) << narev;
	    std::cout << "      Total size (KB) ";
	    std::cout << setw(12) << revsize/1024 << std::endl;
	    std::cout << sepLine << std::endl << std::endl;
	  } else {
	    std::cout << "You need a DB server" << std::endl;
	  }
        } else if (ncmd == "CLONETAG") { 
          std::string id = element(cmd, 1);
          std::string source = element(cmd, 2);
          std::string dest = element(cmd, 3);
          std::string pend = element(cmd, 4);
          if (pend == "") pend = "_Tmp";
	  if (DbServer != NULL) {
            DbServer->cloneTag("Configuration-"+id, source, dest, pend);
	  } else {
	    std::cout << "You need a DB server" << std::endl;
	  }
        } else if (ncmd == "PURGETAG") { 
          std::string id = element(cmd, 1);
          std::string tag = element(cmd, 2);
	  if (DbServer != NULL) {
            DbServer->purgeTag("Configuration-"+id, tag);
	  } else {
	    std::cout << "You need a DB server" << std::endl;
	  }
        } else if (ncmd == "REMOVETAG") { 
          std::string id = element(cmd, 1);
          std::string tag = element(cmd, 2);
	  if (DbServer != NULL) {
            DbServer->removeTag("Configuration-"+id, tag);
	  } else {
	    std::cout << "You need a DB server" << std::endl;
	  }
	}
      }
      catch (PixConfigDbExc s) {
        std::cout << s.dump() << std::endl;
      }
    }
    delete confDb;
  }
  catch (std::exception e) {
    std::cout << "Unhandled exception - " << e.what() << std::endl;
    return -1;
  }
  return 0;
}
