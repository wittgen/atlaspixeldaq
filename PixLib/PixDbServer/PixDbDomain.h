/////////////////////////////////////////////////////////////////////
// PixDbDomain.h
// version 1.0.0
/////////////////////////////////////////////////////////////////////
//
// 02/10/07  Version 1.0 (VD)
//           Initial release
//

#ifndef _PIXLIB_DBDOMAIN 
#define _PIXLIB_DBDOMAIN

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <set>
#include <map>

#include "BaseException.h"

namespace PixLib {
    
  class PixDbDomainExc : public SctPixelRod::BaseException { 
  public:
    enum ErrorLevel{INFO, WARNING, ERROR, FATAL};
    
    PixDbDomainExc(ErrorLevel el, std::string id, std::string descr, std::string method) : BaseException(id), m_errorLevel(el), m_id(id), m_descr(descr), m_method(method) {}; 

    virtual ~PixDbDomainExc() {};
    
    virtual void dump(std::ostream &out) {
      out << "PixelDbDomain Exception: " << dumpLevel() << ":" << m_id << " - "<< m_descr << std::endl; 
    };

    virtual void dump() {
      std::cout << "PixelDbDomain Exception in " << m_method << " function:  " << dumpLevel() << "  - "<< m_descr << std::endl; 
    };

    std::string dumpLevel() {
      switch (m_errorLevel) {
      case INFO : 
	return "INFO";
      case WARNING :
	return "WARNING";
      case ERROR :
      return "ERROR";
      case FATAL :
	return "FATAL";
      default :
	return "UNKNOWN";
      }
    };
    
    ErrorLevel getErrorLevel() const { return m_errorLevel; }; 
    std::string getId() const { return m_id; };
    std::string geDescr() const { return m_descr; };
  
  private:
    ErrorLevel m_errorLevel;
    std::string m_id;
    std::string m_descr;
    std::string m_method;
  };
  

  class PixDbDomain {
  public:
    
    struct link_object {
      std::string linkName;
      std::string objName;
      std::string objType;
      unsigned int revision=0;
    };

    struct rev_content {
      unsigned int revision=0;
      unsigned int useCount=0;
      unsigned int size=0;
      std::string pendingTag;
      std::map<std::string, bool> bool_content;
      std::map<std::string, int> int_content;
      std::map<std::string, unsigned int> unsigned_content;
      std::map<std::string, float> float_content;
      std::map<std::string, std::string> string_content;
      std::map<std::string, std::vector<unsigned int> > vect_content;
      std::vector<link_object> upLink_content;
      std::vector<link_object> downLink_content;
      void computeSize();
    };   
    
    struct rev_ptr {
      std::string pendingTag;
      rev_content *rev;
    };
    
    struct obj_content {
      std::map<unsigned int, rev_ptr> revisionList;
      std::string objType;
    };
    
    struct tag_content {
      std::map<std::string, obj_content> objectList;
      std::map<std::string, unsigned int> typeList;
    };

    struct obj_id {
      std::string domain;
      std::string tag;
      unsigned int rev=0;
      std::string name;
      std::string type;
      unsigned int nretry=0;
    };
    
  public:
    PixDbDomain(std::string name);
    PixDbDomain(std::string name, std::string tag);
    ~PixDbDomain();
   
    //list methods
    void listTags(std::vector<std::string> &List);
    void listObjects(std::string tag, std::vector<std::string> &List);
    void listObjects(std::string tag, std::string objType, std::vector<std::string> &List, std::vector<std::string> &Type);
    void listRevisions(std::string tag, std::string objName, std::vector<unsigned int> &List);
    void memoryReport(std::vector<std::string> &tags, std::vector<unsigned int> &nobj, std::vector<unsigned int> &nrev, std::vector<unsigned int> &size, 
                      unsigned int &narev, unsigned int &revsize);
    
    //remove methods
    void eraseAll();
    void removeTag(std::string tag);
    void purgeTag(std::string tag);
    void removePendingTag(std::string pendTag, std::string tag);
    void renamePendingTag(std::string pendTag, std::string newPendTag, std::string tag, std::string type);
    void clearRevisionCont(rev_content &con); // never used

    // clone methods
    void cloneTag(std::string inTag, std::string outTag, std::string pendTag);

    //print methods
    void printRevContent(rev_content &content); // the real is in the Interface class this is here only for
    void printAll();   // deprecated, to be updated
    
    //writing methods
    bool printOnFile(std::ofstream &out); // deprecated, to be updated
    void writeObject(rev_content &content, std::string tag, std::string objName, std::string objType) {
      unsigned int rev = 0xffffffff;
      writeObject(content, tag, objName, objType, rev, false);
    }
    void writeObject(rev_content &content, std::string tag, std::string objName, std::string objType, unsigned int &rev, bool expert=false);
 
    //reading methods
    bool getFromFile(std::ifstream &in); // deprecated, to be updated
    std::string getObjectType(std::string tag, std::string objName);
    void getObject(rev_content &object_out, std::string tag, std::string objName, unsigned int rev=0xffffffff);
    bool objectExists(std::string tag, std::string objName, unsigned int rev=0xffffffff);
    std::string getName() { return m_domainName; };
    unsigned int getSize() { return m_size; };
    unsigned int getRev() { return m_rev; };
    std::map<std::string, unsigned int> getTypeList(std::string tag);

  private:
    void incrTypeCount(std::string tag, std::string type);
    void decrTypeCount(std::string tag, std::string type);

    std::map<std::string, tag_content> m_tagList;
    std::string m_domainName;
    unsigned int m_size;
    unsigned int m_rev;
  };
}

#endif


/* 
   Comments & ToDo:
   - errors: exception or bool????

   metodi con eccezzioni:
   - listTags       :   INFO
   - listObjects    :   ERROR
   - listRevisions  :   ERROR
   - removeTag      :   ERROR
   - writeObject_rev:   WARNING
   - getFromFile    :   ERROR
   - getObject      :   ERROR
   - getObject_rev  :   ERROR
*/
