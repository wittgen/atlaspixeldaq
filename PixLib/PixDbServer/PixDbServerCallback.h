/////////////////////////////////////////////////////////////////////
// PixDbServerCallback.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 07/10/07  Version 1.0 (VD)
//           Initial release
//

//! Class for the Pixel dbserver callback

#ifndef _PIXLIB_DBSERVER_CALLBACK
#define _PIXLIB_DBSERVER_CALLBACK

//#include <ipc/server.h>
// possible replacement
#include <owl/semaphore.h>
#include <ipc/object.h>

#include "PixDbServerInterfaceIDL.hh"


namespace PixLib {
  
  //  class PixDbServerCallback : public IPCServer, public IPCObject<POA_ipc::PixDbServerCallback> {
  class PixDbServerCallback : public OWLSemaphore, public IPCObject<POA_ipc::PixDbServerCallback> {
    
  public:
    
    // Constructor
    PixDbServerCallback()  {}
    
    // Destructor
    ~PixDbServerCallback() { }
    
    /*
      void reset();
      void increaseConnections();
      void decreaseConnections();
      short countConnections();
    */

    // Reset
    //void reset() {m_connections=0;}
    
    // Connection counter handling
    //void  increaseConnections() {m_connections++;}
    //void  decreaseConnections() {m_connections--; if(m_connections==0) this->stop();}
    //short countConnections() {return m_connections;}    
    // JGK: commented after removing inheritance from ipc-server - correct replacement?
    //void retStop() { this->stop(); }
    void retStop() { this->post(); }

  private:

    // Connections counter
      
  };
}

#endif // _PIXLIB_DBSERVER_CALLBACK
