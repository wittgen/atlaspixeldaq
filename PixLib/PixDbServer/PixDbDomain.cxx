/////////////////////////////////////////////////////////////////////
// PixDbDomain.cxx
// version 1.0.0
/////////////////////////////////////////////////////////////////////
//
// 02/10/07  Version 1.0 (VD)
//           Initial release
//


#include <PixDbDomain.h>

using namespace PixLib;

void PixDbDomain::rev_content::computeSize() {
  size = 3*sizeof(unsigned int) + pendingTag.size(); 
  for (auto a : bool_content) { size += (a.first.size() + sizeof(bool)); }
  for (auto a : int_content) { size += (a.first.size() + sizeof(int)); }
  for (auto a : unsigned_content) { size += (a.first.size() + sizeof(unsigned int)); }
  for (auto a : float_content) { size += (a.first.size() + sizeof(float)); }
  for (auto a : string_content) { size += (a.first.size() + a.second.size()); }
  for (auto a : vect_content) { size += (a.first.size() + a.second.size()*sizeof(unsigned int)); }
}

PixDbDomain::PixDbDomain(std::string name) {
  m_domainName = name;
  m_size = 0;
  m_rev = 0;
}

PixDbDomain::PixDbDomain(std::string name, std::string tag) {
  m_domainName = name;
  m_size = 0;
  m_rev = 0;
  tag_content actualTag;
  std::pair<std::string, tag_content> mypair(tag,actualTag);
  m_tagList.insert(mypair);
}

PixDbDomain::~PixDbDomain() {
  eraseAll();
}

void PixDbDomain::incrTypeCount(std::string tag, std::string type) {
  std::map<std::string, tag_content>::iterator tagIter=m_tagList.find(tag);
  if (tagIter != m_tagList.end()) {
    std::map< std::string, unsigned int >::iterator it = (tagIter->second).typeList.find(type);
    if (it == (tagIter->second).typeList.end()) {
      (tagIter->second).typeList[type] = 1;
    } else {
      (it->second)++;
    }
  }
}

void PixDbDomain::decrTypeCount(std::string tag, std::string type) {
  std::map<std::string, tag_content>::iterator tagIter=m_tagList.find(tag);
  if (tagIter != m_tagList.end()) {
    std::map< std::string, unsigned int >::iterator it = (tagIter->second).typeList.find(type);
    if (it != (tagIter->second).typeList.end()) {
      if (it->second <= 1) {
	(tagIter->second).typeList.erase(it);
      } else {
	(it->second)--;
      }
    }
  }
}

std::map<std::string, unsigned int> PixDbDomain::getTypeList(std::string tag) {
  std::map<std::string, unsigned int> dummy;
  bool countUnsaved = false;
  if (tag[0] == '?') {
    tag = tag.substr(1);
    countUnsaved = true;
  }
  std::map<std::string, tag_content>::iterator tagIter=m_tagList.find(tag);
  if (tagIter != m_tagList.end()) {
    if (countUnsaved) {
      std::map<std::string, unsigned int> list;
      std::map<std::string, obj_content>::iterator objIter;
      for (objIter=(*tagIter).second.objectList.begin(); objIter!=(*tagIter).second.objectList.end(); objIter++) {
	std::string type = objIter->second.objType;
	std::map<unsigned int, rev_ptr>::iterator revIter;
	for (revIter=(*objIter).second.revisionList.begin(); revIter!= (*objIter).second.revisionList.end() ; revIter++) {
	  std::string pendTag = revIter->second.pendingTag;
	  if (pendTag.find("_Tmp") == std::string::npos && pendTag != "OnDB") {
	    if (list.find(type) == list.end()) {
	      list[type] = 1;
	    } else {
	      list[type]++;
	    }
	  }
	}
      }
      return list;
    } else {
      return (tagIter->second).typeList;
    }
  }
  return dummy;
}

void PixDbDomain::eraseAll() {
  std::map<std::string, tag_content>::iterator tagIter;
  for (tagIter=m_tagList.begin(); tagIter!=m_tagList.end(); tagIter++) {
    std::map<std::string, obj_content>::iterator objIter;
    for (objIter=(*tagIter).second.objectList.begin(); objIter!=(*tagIter).second.objectList.end(); objIter++) {
      std::map<unsigned int, rev_ptr>::iterator revIter;
      for (revIter=(*objIter).second.revisionList.begin(); revIter!= (*objIter).second.revisionList.end() ; revIter++) {
	revIter->second.rev->useCount--;
	if (revIter->second.rev->useCount == 0) {
          m_size -= revIter->second.rev->size;
          m_rev--;
          delete revIter->second.rev;
	}
      }
    }
  }
  m_tagList.clear();
}


void PixDbDomain::listTags(std::vector<std::string> &List) {
  std::map<std::string, tag_content>::iterator tagIter;
  List.clear();
  for (tagIter=m_tagList.begin() ; tagIter!=m_tagList.end() ; tagIter++) {
    List.push_back((*tagIter).first);
  }
  if(List.size()==0)   throw PixDbDomainExc(PixDbDomainExc::INFO, "NOTAGS", "No tags in this domain!", "listTags");
}


void PixDbDomain::listObjects(std::string tag, std::vector<std::string> &List) {
  std::map<std::string, tag_content>::iterator tagIter=m_tagList.find(tag);
  if (tagIter!=m_tagList.end()) {
    std::map<std::string, obj_content>::iterator objIter;
    for (objIter=(*tagIter).second.objectList.begin() ; objIter!= (*tagIter).second.objectList.end() ; objIter++) {
      List.push_back((*objIter).first);
    }
  } else {
    std::ostringstream ostr;
    ostr << "tag  '" << tag << "' not found";
    throw PixDbDomainExc(PixDbDomainExc::ERROR, "NOTAG", ostr.str(),"listObject");
    //std::cout << "ERROR  :  PixDbDomain::listObjects  :  tag  '" << tag << "' not found!! " << std::endl;
  }
  if(List.size()==0)   {
    std::ostringstream ostr;
    ostr << "No objects in tag  '" << tag << "'";
    throw PixDbDomainExc(PixDbDomainExc::INFO, "NOOBJECTS", ostr.str(),"listObject");
  }
}


void PixDbDomain::listObjects(std::string tag, std::string objType, std::vector<std::string> &List, std::vector<std::string> &Type) {
  std::map<std::string, tag_content>::iterator tagIter=m_tagList.find(tag);
  if (tagIter!=m_tagList.end()) {
    std::map<std::string, obj_content>::iterator objIter;
    for (objIter=(*tagIter).second.objectList.begin() ; objIter!= (*tagIter).second.objectList.end() ; objIter++) {
      if ((*objIter).second.objType == objType || objType == "") {
	List.push_back((*objIter).first);
	Type.push_back((*objIter).second.objType);
      }
    }
  } else {
    std::ostringstream ostr;
    ostr << "tag  '" << tag << "' not found";
    throw PixDbDomainExc(PixDbDomainExc::ERROR, "NOTAG", ostr.str(),"listObject");
    //std::cout << "ERROR  :  PixDbDomain::listObjects  :  tag  '" << tag << "' not found!! " << std::endl;
  }
  if(List.size()==0)   {
    std::ostringstream ostr;
    ostr << "No " << objType << " objects in tag  '" << tag << "'";
    throw PixDbDomainExc(PixDbDomainExc::INFO, "NOOBJECTS", ostr.str(),"listObject");
  }
}


void PixDbDomain::listRevisions(std::string tag, std::string objName, std::vector<unsigned int> &List) {
  std::map<std::string, tag_content>::iterator tagIter=m_tagList.find(tag);
  List.clear();
  if (tagIter!=m_tagList.end()) {
    std::map<std::string, obj_content>::iterator objIter = (*tagIter).second.objectList.find(objName);
    if (objIter!=(*tagIter).second.objectList.end()) {
      std::map<unsigned int, rev_ptr>::iterator revIter;
      for (revIter=(*objIter).second.revisionList.begin() ; revIter!= (*objIter).second.revisionList.end() ; revIter++) {
	List.push_back((*revIter).first);
      }
    } else {
      std::ostringstream ostr;
      ostr << "object  '" << objName << "' in tag '" << tag << "' not found";
      throw PixDbDomainExc(PixDbDomainExc::ERROR, "NOTAOBJECTS", ostr.str(),"listRevisions");
      //std::cout << "ERROR  :  PixDbDomain::listRevisions  :  object  '" << obj << "' not found!! " << std::endl;
    }
  } else {
    std::ostringstream ostr;
    ostr << "tag  '" << tag << "' not found";
    throw PixDbDomainExc(PixDbDomainExc::ERROR, "NOTAG", ostr.str(),"listRevisions");
    //std::cout << "ERROR  :  PixDbDomain::listRevisions  :  tag  '" << tag << "' not found!! " << std::endl;
  }
  if(List.size()==0)   {
    std::ostringstream ostr;
    ostr << "No revisions in object  '" << objName << "' in tag '" << tag << "'";
    throw PixDbDomainExc(PixDbDomainExc::INFO, "NOREVISIONS", ostr.str(),"listRevisions");
  }
}

void PixDbDomain::memoryReport(std::vector<std::string> &tags, std::vector<unsigned int> &nobj, std::vector<unsigned int> &nrev, std::vector<unsigned int> &size, 
			       unsigned int &narev, unsigned int &revsize) {
for (auto tag : m_tagList) {
  tags.push_back(m_domainName+"//"+tag.first);
  unsigned int objCount = 0;
  unsigned int revCount = 0;
  unsigned int domSize = 0;
  float frev=0, fsize=0;
  for (auto obj : tag.second.objectList) {
    objCount++; 
    for (auto rev : obj.second.revisionList) {
      if (rev.second.rev->useCount == 0) { 
	fsize += (float)(rev.second.rev->size);
	frev = frev + 1.0;
      } else {
	fsize += (float)(rev.second.rev->size)/rev.second.rev->useCount;
	frev = frev + 1.0/rev.second.rev->useCount;
      }
    }
  }
  revCount = frev;
  domSize = fsize;
  nobj.push_back(objCount);
  nrev.push_back(revCount);
  size.push_back(domSize);
 }
 narev = narev + m_rev;
 revsize = revsize + m_size;
}

void PixDbDomain::removeTag(std::string tag) {  
  std::map<std::string, tag_content>::iterator tagIter=m_tagList.find(tag);
  if (tagIter!=m_tagList.end()) {
    std::map<std::string, obj_content>::iterator objIter;
    for (objIter=(*tagIter).second.objectList.begin(); objIter!=(*tagIter).second.objectList.end(); objIter++) {
      std::map<unsigned int, rev_ptr>::iterator revIter;
      for (revIter=(*objIter).second.revisionList.begin(); revIter!= (*objIter).second.revisionList.end() ; revIter++) {
        revIter->second.rev->useCount--;
	if (revIter->second.rev->useCount == 0) {
          m_size -= revIter->second.rev->size;
          m_rev--;
          delete revIter->second.rev;
	}
      }
    }
    m_tagList.erase(tagIter);
  } else {
    std::ostringstream ostr;
    ostr << "tag  '" << tag << "' not found";
    throw PixDbDomainExc(PixDbDomainExc::ERROR, "NOTAG", ostr.str(),"removeTag");
    //std::cout << "ERROR  :  PixDbDomain::removeTag  :  tag  '" << tag << "' not found!!" << std::endl;
  } 
}

void PixDbDomain::purgeTag(std::string tag) {  
  std::map<std::string, tag_content>::iterator tagIter=m_tagList.find(tag);
  if (tagIter!=m_tagList.end()) {
    std::map<std::string, obj_content>::iterator objIter;
    for (objIter=(*tagIter).second.objectList.begin(); objIter!=(*tagIter).second.objectList.end(); objIter++) {
      std::map<unsigned int, rev_ptr>::iterator revIter;
      unsigned int maxrev=0;
      for (revIter=(*objIter).second.revisionList.begin(); revIter!= (*objIter).second.revisionList.end() ; revIter++) {
        if (revIter->first > maxrev) maxrev = revIter->first;
      }
      for (revIter=(*objIter).second.revisionList.begin(); revIter!= (*objIter).second.revisionList.end() ; revIter++) {
        if (revIter->first != maxrev) {
	  std::string pendTag = revIter->second.pendingTag;
	  if (pendTag.find("_Tmp") != std::string::npos || pendTag == "OnDB") {
	    revIter->second.rev->useCount--;
	    if (revIter->second.rev->useCount == 0) {
	      m_size -= revIter->second.rev->size;
	      m_rev--;
	      delete revIter->second.rev;
	    }
	    objIter->second.revisionList.erase(revIter);
	  }
	}
      }
    }
  } else {
    std::ostringstream ostr;
    ostr << "tag  '" << tag << "' not found";
    throw PixDbDomainExc(PixDbDomainExc::ERROR, "NOTAG", ostr.str(),"purgeTag");
    //std::cout << "ERROR  :  PixDbDomain::purgeTag  :  tag  '" << tag << "' not found!!" << std::endl;
  } 
}

void PixDbDomain::removePendingTag(std::string pendTag, std::string tag) {  
  std::map<std::string, tag_content>::iterator tagIter=m_tagList.find(tag);
  if (tagIter!=m_tagList.end()) {
    std::map<std::string, obj_content>::iterator objIter;
    for (objIter=(*tagIter).second.objectList.begin(); objIter!=(*tagIter).second.objectList.end(); objIter++) {
      std::map<unsigned int, rev_ptr>::iterator revIter;
      for (revIter=(*objIter).second.revisionList.begin(); revIter!= (*objIter).second.revisionList.end() ; revIter++) {
        if (revIter->second.pendingTag == pendTag) {
	  revIter->second.rev->useCount--;
	  decrTypeCount(tagIter->first, objIter->second.objType);
	  if (revIter->second.rev->useCount == 0) {
	    m_size -= revIter->second.rev->size;
	    m_rev--;
            delete revIter->second.rev;
	  }
	  objIter->second.revisionList.erase(revIter);
	}
      }
      if (objIter->second.revisionList.size() == 0) {
	tagIter->second.objectList.erase(objIter);
      }
    }
    if (tagIter->second.objectList.size() == 0) {
      m_tagList.erase(tagIter);
    }
  } else {
    std::ostringstream ostr;
    ostr << "tag  '" << tag << "' not found";
    throw PixDbDomainExc(PixDbDomainExc::ERROR, "NOTAG", ostr.str(),"removePendingTag");
    //std::cout << "ERROR  :  PixDbDomain::removeTag  :  tag  '" << tag << "' not found!!" << std::endl;
  } 
}

void PixDbDomain::renamePendingTag(std::string pendTag, std::string newPendTag, std::string tag, std::string type) {  
  std::map<std::string, tag_content>::iterator tagIter=m_tagList.find(tag);
  if (tagIter!=m_tagList.end()) {
    std::map<std::string, obj_content>::iterator objIter;
    for (objIter=(*tagIter).second.objectList.begin(); objIter!=(*tagIter).second.objectList.end(); objIter++) {
      if (type == "" || type == objIter->second.objType) {
	std::map<unsigned int, rev_ptr>::iterator revIter;
	for (revIter=(*objIter).second.revisionList.begin(); revIter!= (*objIter).second.revisionList.end() ; revIter++) {
	  if (revIter->second.pendingTag == pendTag) {
	    revIter->second.pendingTag = newPendTag;
	  }
	}
      }
    }
  } else {
    std::ostringstream ostr;
    ostr << "tag  '" << tag << "' not found";
    throw PixDbDomainExc(PixDbDomainExc::ERROR, "NOTAG", ostr.str(),"renamePendingTag");
    //std::cout << "ERROR  :  PixDbDomain::removeTag  :  tag  '" << tag << "' not found!!" << std::endl;
  } 
}

void PixDbDomain::clearRevisionCont(rev_content &con) {
  con.bool_content.clear();
  con.int_content.clear();  
  con.unsigned_content.clear();  
  con.float_content.clear();  
  con.string_content.clear();  
  con.vect_content.clear();  
  con.upLink_content.clear();  
  con.downLink_content.clear(); 
}

void PixDbDomain::cloneTag(std::string inTag, std::string outTag, std::string pendTag) {
  unsigned int tim = time(NULL);
  std::map<std::string, tag_content>::iterator tagIterIn=m_tagList.find(inTag);
  if (tagIterIn != m_tagList.end()) {
    std::map<std::string, tag_content>::iterator tagIterOut=m_tagList.find(outTag);
    if (tagIterOut == m_tagList.end()) {
      tag_content newTag;
      std::pair<std::string, tag_content> newpairTag(outTag, newTag);
      m_tagList.insert(newpairTag);
      tagIterOut=m_tagList.find(outTag);
    }
    std::map<std::string, obj_content>::iterator objIterIn;
    for (objIterIn=(*tagIterIn).second.objectList.begin(); objIterIn!=(*tagIterIn).second.objectList.end(); objIterIn++) {
      std::map<std::string, obj_content>::iterator objIterOut = tagIterOut->second.objectList.find(objIterIn->first);
      if (objIterOut == tagIterOut->second.objectList.end()) {
	obj_content newObj;
        newObj.objType = objIterIn->second.objType;
	std::pair<std::string, obj_content> newpairObj(objIterIn->first, newObj);
        tagIterOut->second.objectList.insert(newpairObj);
	objIterOut=tagIterOut->second.objectList.find(objIterIn->first);
	incrTypeCount(outTag, objIterOut->second.objType);
      }
      std::map<unsigned int, rev_ptr>::iterator revIterIn;
      unsigned int maxRev = 0;
      rev_content *revSel = NULL;
      for (revIterIn=(*objIterIn).second.revisionList.begin(); revIterIn!= (*objIterIn).second.revisionList.end() ; revIterIn++) {
	if (revIterIn->first > maxRev) {
	  maxRev = revIterIn->first;
	  revSel = revIterIn->second.rev;
	}
      }
      if (revSel != NULL) {
	std::map<unsigned int, rev_ptr>::iterator revIterOut;
	std::map<unsigned int, rev_ptr>::iterator revIterSelOut;
	unsigned int maxRevOut = 0;
	rev_content *revSelOut = NULL;
	for (revIterOut=(*objIterOut).second.revisionList.begin(); revIterOut!= (*objIterOut).second.revisionList.end() ; revIterOut++) {
	  if (revIterOut->first > maxRevOut) {
	    maxRevOut = revIterOut->first;
	    revSelOut = revIterOut->second.rev;
	    revIterSelOut = revIterOut;
	  }
	}
	if (revSelOut != revSel) {
	  revSel->useCount++;
	  rev_ptr rp;
	  rp.pendingTag = pendTag;
	  rp.rev = revSel;
	  std::pair<unsigned int, rev_ptr> newpairRev(tim, rp);
	  objIterOut->second.revisionList.insert(newpairRev);
	} else if (inTag == outTag) {
          if ((revIterSelOut->second).pendingTag != "OnDB") {
	    (revIterSelOut->second).pendingTag = pendTag;
	  }
	}
      }
    }
  } else {
    throw PixDbDomainExc(PixDbDomainExc::ERROR, "NOTAG", "Tag "+inTag+" not found","cloneTag");
  } 
}

void PixDbDomain::printRevContent( rev_content &content) {
  //std::cout  << "Revision_content ! " << std::endl;
  std::cout << "   -> bool_map has " << content.bool_content.size() << " entries!!" << std::endl;
  std::map<std::string, bool>::iterator b_it;
  for (b_it=(content.bool_content).begin(); b_it!=(content.bool_content).end(); b_it++) {
    std::cout << "      -- Name:   " <<  (*b_it).first  << "        value: " <<  (*b_it).second <<  std::endl;
  }
  std::cout << "   -> int_map has " << content.int_content.size() << " entries!!" << std::endl;
  std::map<std::string, int>::iterator i_it;
  for (i_it=content.int_content.begin(); i_it!=content.int_content.end(); i_it++) {
    std::cout << "      -- Name:   " <<  (*i_it).first  << "        value: " <<  (*i_it).second <<  std::endl;
  }
  std::cout << "   -> unsigned_map has " << content.unsigned_content.size() << " entries!!" << std::endl;
  std::map<std::string, unsigned int>::iterator u_it;
  for (u_it=content.unsigned_content.begin(); u_it!=content.unsigned_content.end(); u_it++) {
    std::cout << "      -- Name:   " <<  (*u_it).first  << "        value: " <<  (*u_it).second <<  std::endl;
  }
  std::cout << "   -> float_map has " << content.float_content.size() << " entries!!" << std::endl;
  std::map<std::string, float>::iterator f_it;
  for (f_it=content.float_content.begin(); f_it!=content.float_content.end(); f_it++) {
    std::cout << "      -- Name:   " <<  (*f_it).first  << "        value: " <<  (*f_it).second <<  std::endl;
  }
  std::cout << "   -> string_map has " << content.string_content.size() << " entries!!" << std::endl;
  std::map<std::string, std::string>::iterator s_it;
  for (s_it=content.string_content.begin(); s_it!=content.string_content.end(); s_it++) {
    std::cout << "      -- Name:   " <<  (*s_it).first  << "        value: " <<  (*s_it).second <<  std::endl;
  }

  std::cout << "   -> vect_map has " << content.vect_content.size() << " entries!!" << std::endl;
  std::map<std::string, std::vector<unsigned int> >::iterator v_it;
  for (v_it=content.vect_content.begin(); v_it!=content.vect_content.end(); v_it++) {
    std::cout << "      -- Name:   " <<  (*v_it).first  << "  has " <<  (*v_it).second.size() << " entries!!" << std::endl;
    for (unsigned int dim=0; dim<(*v_it).second.size();dim++){
      std::cout << "         -- Value  " << dim << ":  " << (*v_it).second[dim] << std::endl;
    }
  }
  std::cout << "   -> uplink_map has " << content.upLink_content.size() << " entries!!" << std::endl;
  std::vector<link_object>::iterator l_it;
  int i=0;
  for (l_it=content.upLink_content.begin(); l_it!=content.upLink_content.end(); l_it++) {
    i++;
    std::cout << "      -- upLink "<< i << " with name: " << (*l_it).linkName  << "  typeobj: " <<   (*l_it).objType << "  nameobj: " << (*l_it).objName << "  revision: " <<  (*l_it).revision << std::endl;
  }
  std::cout << "   -> downlink_map has " << content.downLink_content.size() << " entries!!" << std::endl;
  i=0;
  for (l_it=content.downLink_content.begin(); l_it!=content.downLink_content.end(); l_it++) {
    i++;
    std::cout << "      -- downLink "<< i << " with name: " << (*l_it).linkName  << "  typeobj: " <<   (*l_it).objType << "  nameobj: " << (*l_it).objName << "  revision: " <<  (*l_it).revision << std::endl;
  }
}

void PixDbDomain::printAll() {
  std::cout << "Called PixDbDomain::printAll " << std::endl;
  std::map<std::string, tag_content>::iterator tagIter;
  for (tagIter=m_tagList.begin() ; tagIter!=m_tagList.end() ; tagIter++) {
    std::cout << "> Tag:   " <<  (*tagIter).first  << std::endl;
    std::map<std::string, obj_content>::iterator objIter;
    for (objIter=(*tagIter).second.objectList.begin() ; objIter!= (*tagIter).second.objectList.end() ; objIter++) {
      std::cout << " -> Object:   " << (*objIter).second.objType << "   "  <<(*objIter).first  << std::endl;
      std::map<unsigned int, rev_ptr>::iterator revIter;
      for (revIter=(*objIter).second.revisionList.begin() ; revIter!= (*objIter).second.revisionList.end() ; revIter++) {
	std::cout << "  -> Revision:   " <<  (*revIter).first  << std::endl;
        revIter->second.rev->pendingTag = revIter->second.pendingTag;
        revIter->second.rev->revision = revIter->first;
	printRevContent(*((*revIter).second.rev));
	std::cout << std::endl;
      } 
    }
  }
}

bool PixDbDomain::printOnFile(std::ofstream &out) {
  /*
  if (!out) {
    std::ostringstream ostr;
    ostr << "OFstream reference not valid!";
    //throw PixDbDomainExc(PixDbDomainExc::ERROR, "OUTPUTFILEFAIL", ostr.str(),"printOnFile");
    std::cout << "ERROR  :  PixDbDomain::printOnFile  :  ofstream passed not working!! " << std::endl;
    return false;
  }
  std::cout << "called printOnFile " << std::endl;
  unsigned int size;
  unsigned int len;
  size=(unsigned int)(m_tagList.size());
  out.write((char*)(&size), sizeof(unsigned int));
  std::map<std::string, tag_content>::iterator tagIter;
  for (tagIter=m_tagList.begin() ; tagIter!=m_tagList.end() ; tagIter++) {
    len=(*tagIter).first.length();
    out.write((char*)(&len), sizeof(unsigned int));
    out << (*tagIter).first << "\n";
    size=(unsigned int)((*tagIter).second.objectList.size());
    out.write((char*)(&size), sizeof(unsigned int));
    std::map<std::string, obj_content>::iterator objIter;
    for (objIter=(*tagIter).second.objectList.begin() ; objIter!=(*tagIter).second.objectList.end() ; objIter++) {
      len=(*objIter).second.objType.length();
      out.write((char*)(&len), sizeof(unsigned int));
      out << (*objIter).second.objType << "\n";
      
      len=(*objIter).first.length();
      out.write((char*)(&len), sizeof(unsigned int));
      out <<  (*objIter).first << "\n";
      
      size=(unsigned int)((*objIter).second.revisionList.size());
      out.write((char*)(&size), sizeof(unsigned int));

      std::map<unsigned int, rev_content>::iterator revIter;
      for (revIter=(*objIter).second.revisionList.begin(); revIter!=(*objIter).second.revisionList.end(); revIter++) {
	out.write((char*)(&((*revIter).first)), sizeof(unsigned int));
	size=(unsigned int)((*revIter).second.bool_content.size());
       	out.write((char*)(&size), sizeof(unsigned int));
	
	std::map<std::string, bool>::iterator b_it;
	for (b_it=((*revIter).second.bool_content).begin(); b_it!=((*revIter).second.bool_content).end(); b_it++) {
	  len= (*b_it).first.length();
	  out.write((char*)(&len), sizeof(unsigned int));
	  out <<  (*b_it).first << "\n";
	  out.write((char*)(&((*b_it).second)), sizeof(bool));
	}
	
	size=(unsigned int)((*revIter).second.int_content.size());
	out.write((char*)(&size), sizeof(unsigned int));
	std::map<std::string, int>::iterator i_it;
	for (i_it=(*revIter).second.int_content.begin(); i_it!=(*revIter).second.int_content.end(); i_it++) {
	  len= (*i_it).first.length();
	  out.write((char*)(&len), sizeof(unsigned int));
	  out <<  (*i_it).first << "\n";
	  out.write((char*)((&(*i_it).second)), sizeof(int));
	}

	size=(unsigned int)((*revIter).second.unsigned_content.size());
	out.write((char*)(&size), sizeof(unsigned int));
	std::map<std::string, unsigned int>::iterator u_it;
	for (u_it=(*revIter).second.unsigned_content.begin(); u_it!=(*revIter).second.unsigned_content.end(); u_it++) {
	  len= (*u_it).first.length();
	  out.write((char*)(&len), sizeof(unsigned int));
	  out <<  (*u_it).first << "\n";
	  out.write((char*)((&(*u_it).second)), sizeof(unsigned int));
	}

	size=(unsigned int)((*revIter).second.float_content.size());
	out.write((char*)(&size), sizeof(unsigned int));
	std::map<std::string, float>::iterator f_it;
	for (f_it=(*revIter).second.float_content.begin(); f_it!=(*revIter).second.float_content.end(); f_it++) {
	  len= (*f_it).first.length();
	  out.write((char*)(&len), sizeof(unsigned int));
	  out <<  (*f_it).first << "\n";
	  out.write((char*)(&(*f_it).second), sizeof(float));
	}

	size=(unsigned int)((*revIter).second.string_content.size());
	out.write((char*)(&size), sizeof(unsigned int));
	std::map<std::string, std::string>::iterator s_it;
	for (s_it=(*revIter).second.string_content.begin(); s_it!=(*revIter).second.string_content.end(); s_it++) {
	  len= (*s_it).first.length();
	  out.write((char*)(&len), sizeof(unsigned int));
	  out <<  (*s_it).first << "\n";
	  len= (*s_it).second.length();
	  out.write((char*)(&len), sizeof(unsigned int));
	  out <<  (*s_it).second << "\n";
	}
	
	size=(unsigned int)((*revIter).second.vect_content.size());
	out.write((char*)(&size), sizeof(unsigned int));
	std::map<std::string, std::vector<unsigned int> >::iterator v_it;
	for (v_it=(*revIter).second.vect_content.begin(); v_it!=(*revIter).second.vect_content.end(); v_it++) {
	  len= (*v_it).first.length();
	  out.write((char*)(&len), sizeof(unsigned int));
	  out <<  (*v_it).first << "\n";
	  unsigned int dim=(unsigned int)((*v_it).second.size());
	  out.write((char*)(&dim), sizeof(unsigned int));
	  for (unsigned int i=0; i<dim; i++) {
	    unsigned int cont=((*v_it).second)[i];
	    out.write((char*)(&cont), sizeof(unsigned int));
	  }
	}
	    
	size=(unsigned int)((*revIter).second.upLink_content.size());
	out.write((char*)(&size), sizeof(unsigned int));
	std::vector<link_object>::iterator l_it;
	for (l_it=(*revIter).second.upLink_content.begin(); l_it!=(*revIter).second.upLink_content.end(); l_it++) {
	  len= (*l_it).tag.length();
	  out.write((char*)(&len), sizeof(unsigned int));
	  out <<  (*l_it).tag << "\n";
	  len= (*l_it).objtype.length();
	  out.write((char*)(&len), sizeof(unsigned int));
	  out <<  (*l_it).objtype << "\n";
	  len= (*l_it).obj.length();
	  out.write((char*)(&len), sizeof(unsigned int));
	  out <<  (*l_it).obj << "\n";
	  out.write((char*)(&((*l_it).revision)), sizeof(unsigned int));
	}	

	size=(unsigned int)((*revIter).second.downLink_content.size());
	out.write((char*)(&size), sizeof(unsigned int));
	for (l_it=(*revIter).second.downLink_content.begin(); l_it!=(*revIter).second.downLink_content.end(); l_it++) {
	  len= (*l_it).tag.length();
	  out.write((char*)(&len), sizeof(unsigned int));
	  out <<  (*l_it).tag << "\n";
	  len= (*l_it).objtype.length();
	  out.write((char*)(&len), sizeof(unsigned int));
	  out <<  (*l_it).objtype << "\n";
	  len= (*l_it).obj.length();
	  out.write((char*)(&len), sizeof(unsigned int));
	  out <<  (*l_it).obj << "\n";
	  out.write((char*)(&((*l_it).revision)), sizeof(unsigned int));
	}
      }
    }
  }
  */
  return true;
}


void PixDbDomain::writeObject(rev_content &content, std::string tag, std::string objName, std::string objType, unsigned int &revision, bool expert) {

  if (revision == 0xffffffff) revision = time(NULL);

  std::map<std::string, tag_content>::iterator tagIter=m_tagList.find(tag);
  if (tagIter!=m_tagList.end()) {
    std::map<std::string, obj_content>::iterator objIter= (*tagIter).second.objectList.find(objName);
    if (objIter!=(*tagIter).second.objectList.end()) {
      std::map<unsigned int, rev_ptr>::iterator revIter;
      bool found=false;
      for (revIter=(*objIter).second.revisionList.begin(); revIter!=(*objIter).second.revisionList.end(); revIter++) {
	if ((*revIter).first>=revision) {
	  found=true;
	  break;
	}
      }
      if (!found) {
        rev_content *mc = new rev_content(content);
	content.revision=revision;
      	mc->revision = revision;
	mc->useCount = 1;
        mc->computeSize();
        m_size += mc->size;
        m_rev++;
	incrTypeCount(tag, objIter->second.objType);
        rev_ptr rp;
	rp.pendingTag = content.pendingTag;
	rp.rev = mc;
	std::pair<unsigned int, rev_ptr> mypairRev(revision,rp);
	(*objIter).second.revisionList.insert(mypairRev);
      } else if (found && expert) {
	revIter=(*objIter).second.revisionList.find(revision);
	if (revIter != (*objIter).second.revisionList.end()) {
	  rev_content *mc = revIter->second.rev;
	  mc->useCount--;
	  if (mc->useCount == 0) {
            m_size -= mc->size;
            m_rev--;
            delete mc;
	  }
	  (*objIter).second.revisionList.erase(revIter);
	}
        rev_content *mc = new rev_content(content);
      	mc->revision = revision;
	mc->useCount = 1;
        mc->computeSize();
        m_size += mc->size;
        m_rev++;
	content.revision=revision;
        rev_ptr rp;
	rp.pendingTag = content.pendingTag;
	rp.rev = mc;       
	std::pair<unsigned int, rev_ptr> mypairRev(revision,rp);
	(*objIter).second.revisionList.insert(mypairRev);
      } else if (found && !expert) {
	std::ostringstream ostr;
	ostr << "Object '" << objName << "' already exists with revision " << revision << " in tag " << tag << "  and no expert mode!  No object will be written" << std::endl;
	throw PixDbDomainExc(PixDbDomainExc::WARNING, "OBJECTALREADYEXISTS", ostr.str(),"writeObject");
	//std::cout << "Object '" << obj << "' already exists with this revision (" << revision << ") and no update option!!" << std::endl;
      }
    } else {
      obj_content myObj;
      rev_content *mc = new rev_content(content);
      content.revision=revision;
      mc->revision = revision;
      mc->useCount = 1;
      mc->computeSize();
      m_size += mc->size;
      m_rev++;
      incrTypeCount(tag, objType);
      rev_ptr rp;
      rp.pendingTag = content.pendingTag;
      rp.rev = mc;
      std::pair<unsigned int, rev_ptr> mypairRev(revision,rp);
      myObj.revisionList.insert(mypairRev);
      myObj.objType=objType;
      std::pair<std::string, obj_content> mypairObj(objName,myObj);
      (*tagIter).second.objectList.insert(mypairObj);
    }
  } else {
    obj_content myObj;
    rev_content *mc = new rev_content(content);
    content.revision=revision;
    mc->revision = revision;
    mc->useCount = 1;
    mc->computeSize();
    m_size += mc->size;
    m_rev++;
    incrTypeCount(tag, objType);
    rev_ptr rp;
    rp.pendingTag = content.pendingTag;
    rp.rev = mc;
    std::pair<unsigned int, rev_ptr> mypairRev(revision,rp);
    myObj.revisionList.insert(mypairRev);
    myObj.objType=objType;
    tag_content myTag;
    std::pair<std::string, obj_content> mypairObj(objName,myObj);
    myTag.objectList.insert(mypairObj);
    
    std::pair<std::string, tag_content> mypairTag(tag,myTag);
    m_tagList.insert(mypairTag);
  }
}

bool PixDbDomain::getFromFile(std::ifstream &in) {
  std::cout << " called getFromFile" << std::endl;
  /*
  if (!in) {
    std::ostringstream ostr;
    ostr << "IFstream reference not valid!";
    throw PixDbDomainExc(PixDbDomainExc::ERROR, "INPUTFILEFAIL", ostr.str(),"getFromFile");
    //std::cout << "ERROR  :  PixDbDomain::getFromFile  :  ifstream passed not working!! " << std::endl;
    return false;
  }
  unsigned int ntag;
  unsigned int len;
  char *str;
  in.read((char*)(&ntag),sizeof(unsigned int));
  //std::cout << "ntag vale: " << ntag << std::endl;
  for (int t=0; t<ntag; t++) {
    unsigned int nobject;
    in.read((char*)(&len),sizeof(unsigned int));
    //std::cout <<" len vale: " <<  len << std::endl;
    str=new char[len+1];
    in.getline(str,sizeof(char[len+1]));
    std::string tagName=(std::string)str;
    delete str;
    in.read((char*)(&nobject),sizeof(unsigned int));
    std::cout << "tagname vale: " << tagName << "   e nobj: " << nobject << std::endl;
    tag_content myTag;
    // link_object linko;

    for (int o=0; o<nobject; o++) {
      //if(o!=0) exit(-1);
      unsigned int nrevision;
      in.read((char*)(&len),sizeof(unsigned int));
      str=new char[len+1];
      in.getline(str,sizeof(char[len+1]));
      std::string objType=(std::string)str;
      delete str;
      in.read((char*)(&len),sizeof(unsigned int));
      str=new char[len+1];
      in.getline(str,sizeof(char[len+1]));
      std::string objName=(std::string)str;
      delete str;
      in.read((char*)(&nrevision),sizeof(unsigned int));
      std::cout << "objectName vale: " << objName << "  e objType: " << objType << "   nrevision: " << nrevision << std::endl;
      obj_content myObj;
     
      for (int r=0; r<nrevision; r++) {
	unsigned int revision;
	rev_content rev;
	unsigned int nsize;
	std::string first;
	in.read((char*)(&revision),sizeof(unsigned int));
	in.read((char*)(&nsize),sizeof(unsigned int));
	//std::cout << " revision vale: " << revision << "  e nsize vale: " << nsize << std::endl;
	for (unsigned int c=0; c<nsize; c++) {
	  bool second;
	  in.read((char*)(&len),sizeof(unsigned int));
	  str=new char[len+1];
	  in.getline(str,sizeof(char[len+1]));
	  first=(std::string)str;
	  delete str;
	  in.read((char*)(&second),sizeof(bool));
	  rev.bool_content[first]=second;
	}
	in.read((char*)(&nsize),sizeof(unsigned int));
	for (unsigned int c=0; c<nsize; c++) {
	  int second;
	  in.read((char*)(&len),sizeof(unsigned int));
	  str=new char[len+1];
	  in.getline(str,sizeof(char[len+1]));
	  first=(std::string)str;
	  delete str;
	  in.read((char*)(&second),sizeof(int));
	  rev.int_content[first]=second;
	}
	in.read((char*)(&nsize),sizeof(unsigned int));
	for (unsigned int c=0; c<nsize; c++) {
	  unsigned int second;
	  in.read((char*)(&len),sizeof(unsigned int));
	  str=new char[len+1];
	  in.getline(str,sizeof(char[len+1]));
	  first=(std::string)str;
	  delete str;
	  in.read((char*)(&second),sizeof(unsigned int));
	  rev.unsigned_content[first]=second;
	}
	in.read((char*)(&nsize),sizeof(unsigned int));
	for (unsigned int c=0; c<nsize; c++) {
	  float second;
	  in.read((char*)(&len),sizeof(unsigned int));
	  str=new char[len+1];
	  in.getline(str,sizeof(char[len+1]));
	  first=(std::string)str;
	  delete str;
	  in.read((char*)(&second),sizeof(float));
	  rev.float_content[first]=second;
	}
      	in.read((char*)(&nsize),sizeof(unsigned int));
	for (unsigned int c=0; c<nsize; c++) {
	  in.read((char*)(&len),sizeof(unsigned int));
	  str=new char[len+1];
	  in.getline(str,sizeof(char[len+1]));
	  first=(std::string)str;
	  delete str;
	  in.read((char*)(&len),sizeof(unsigned int));
	  str=new char[len+1];
	  in.getline(str,sizeof(char[len+1]));
	  std::string second=(std::string)str;
	  delete str;
	  rev.string_content[first]=second;
	}
	
	std::vector<unsigned int> tmp;
	in.read((char*)(&nsize),sizeof(unsigned int));
	for (unsigned int c=0; c<nsize; c++) {
	  tmp.clear();
	  in.read((char*)(&len),sizeof(unsigned int));
	  str=new char[len+1];
	  in.getline(str,sizeof(char[len+1]));
	  first=(std::string)str;
	  delete str;
	  unsigned int dim;
	  in.read((char*)(&dim),sizeof(unsigned int));
	  for (unsigned int i=0; i<dim; i++) {
	    unsigned int val;
	    in.read((char*)(&val),sizeof(unsigned int));
	    tmp.push_back(val);
	  }
	  rev.vect_content[first]=tmp;
	}
	
	in.read((char*)(&nsize),sizeof(unsigned int));
	for (unsigned int c=0; c<nsize; c++) {
	  link_object linko;
	  in.read((char*)(&len),sizeof(unsigned int));
	  str=new char[len+1];
	  in.getline(str,sizeof(char[len+1]));
	  linko.tag=(std::string)str;
	  delete str;
	  in.read((char*)(&len),sizeof(unsigned int));
	  str=new char[len+1];
	  in.getline(str,sizeof(char[len+1]));
	  linko.objtype=(std::string)str;
	  delete str;
	  in.read((char*)(&len),sizeof(unsigned int));
	  str=new char[len+1];
	  in.getline(str,sizeof(char[len+1]));
	  linko.obj=(std::string)str;
	  delete str;
	  in.read((char*)(&(linko.revision)),sizeof(unsigned int));
	  rev.upLink_content.push_back(linko);
	}
	in.read((char*)(&nsize),sizeof(unsigned int));
	for (unsigned int c=0; c<nsize; c++) {
	  link_object linko;
	  in.read((char*)(&len),sizeof(unsigned int));
	  str=new char[len+1];
	  in.getline(str,sizeof(char[len+1]));
	  linko.tag=(std::string)str;
	  delete str;
	  in.read((char*)(&len),sizeof(unsigned int));
	  str=new char[len+1];
	  in.getline(str,sizeof(char[len+1]));
	  linko.objtype=(std::string)str;
	  delete str;
	  in.read((char*)(&len),sizeof(unsigned int));
	  str=new char[len+1];
	  in.getline(str,sizeof(char[len+1]));
	  linko.obj=(std::string)str;
	  delete str;
	  in.read((char*)(&(linko.revision)),sizeof(unsigned int));
	  rev.downLink_content.push_back(linko);
	}
	//printRevContent(rev);
	std::pair<unsigned int, rev_content> mypairRev(revision,rev);
	myObj.revisionList.insert(mypairRev);
	myObj.objType=objType;
      }
      std::pair<std::string, obj_content> mypairObj(objName,myObj);
      myTag.objectList.insert(mypairObj);
    }
    std::pair<std::string, tag_content> mypairTag(tagName,myTag);
    m_tagList.insert(mypairTag);
  }
  */
  return true;
}


std::string PixDbDomain::getObjectType(std::string tag, std::string objName) {
  std::string objType="default";
  std::map<std::string, tag_content>::iterator tagIter=m_tagList.find(tag);
  if (tagIter!=m_tagList.end()) {
    std::map<std::string, obj_content>::iterator objIter= (*tagIter).second.objectList.find(objName);
    if (objIter!=(*tagIter).second.objectList.end()) {
      objType=(*objIter).second.objType;
      return objType;
    } else {
      std::ostringstream ostr;
      ostr << "object  '" << objName << "' in tag '" << tag << "' not found";
      throw PixDbDomainExc(PixDbDomainExc::ERROR, "NOTAOBJECTS", ostr.str(),"getObject");
      //std::cout << "ERROR  :  PixDbDomain::getObject  :  object  '" << obj << "' not found!! " << std::endl;
    }
  } else {
    std::ostringstream ostr;
    ostr << "tag  '" << tag << "' not found";
    throw PixDbDomainExc(PixDbDomainExc::ERROR, "NOTAG", ostr.str(),"getObject");
    //std::cout << "ERROR  :  PixDbDomain::getObject  :  tag  '" << tag << "' not found!! " << std::endl;
  }
}

void PixDbDomain::getObject(rev_content &object_out, std::string tag, std::string objName, unsigned int revision) {
  //std::cout << "Called  PixDbDomain::getObject with tag: " << tag << " object " << obj << std::endl;
  clearRevisionCont(object_out);
  std::map<std::string, tag_content>::iterator tagIter=m_tagList.find(tag);
  if (tagIter!=m_tagList.end()) {
    std::map<std::string, obj_content>::iterator objIter= (*tagIter).second.objectList.find(objName);
    if (objIter!=(*tagIter).second.objectList.end()) {
      std::map<unsigned int, rev_ptr>::iterator revIter= (*objIter).second.revisionList.find(revision);
      if (revIter!=(*objIter).second.revisionList.end()) {
	object_out=*(revIter->second.rev);
	object_out.revision = revIter->first;
	object_out.pendingTag = revIter->second.pendingTag;
      } else {
	revIter= (*objIter).second.revisionList.begin();
	unsigned int revMax=0;
	for ( revIter=(*objIter).second.revisionList.begin(); revIter!=(*objIter).second.revisionList.end(); revIter++) {
	  if ((*revIter).first>=revMax && (*revIter).first<=revision) revMax=(*revIter).first;
	}
	revIter=(*objIter).second.revisionList.find(revMax);
	if (revIter!=(*objIter).second.revisionList.end()) {
  try {
	  object_out=*(revIter->second.rev);
  } catch( std::exception& e ) {
    std::cout << __PRETTY_FUNCTION__ << ": " << e.what() << std::endl; throw e;
  } catch( ... ) {
    std::cout << __PRETTY_FUNCTION__ << ": Caught an unkown exception!" << std::endl;
  }
	  object_out.revision = revIter->first;
	  object_out.pendingTag = revIter->second.pendingTag;
	} else {
	  std::ostringstream ostr;
	  ostr << "No revisions older than" << revision << " for object  '" << objName << "' in tag '" << tag << "'";
	  throw PixDbDomainExc(PixDbDomainExc::INFO, "NOREVISIONOLDERTHAN", ostr.str(),"getObject_rev");
	  //  std::cout << "ERROR  :  PixDbDomain::getObject  :  no revision older than " << revision << " for object  '" << obj << "' !! " << std::endl;
	}
      }
    } else {
      std::ostringstream ostr;
      ostr << "object  '" << objName << "' in tag '" << tag << "' not found";
      throw PixDbDomainExc(PixDbDomainExc::ERROR, "NOTAOBJECTS", ostr.str(),"getObject_rev");
      //std::cout << "ERROR  :  PixDbDomain::getObject  :  object  '" << obj << "' not found!! " << std::endl;
    }
  } else {
    std::ostringstream ostr;
    ostr << "tag  '" << tag << "' not found";
    throw PixDbDomainExc(PixDbDomainExc::ERROR, "NOTAG", ostr.str(),"getObject_rev");
    //std::cout << "ERROR  :  PixDbDomain::getObject  :  tag  '" << tag << "' not found!! " << std::endl;
  }
}

bool PixDbDomain::objectExists(std::string tag, std::string objName, unsigned int revision) {
  bool ret = false;
  std::map<std::string, tag_content>::iterator tagIter=m_tagList.find(tag);
  if (tagIter!=m_tagList.end()) {
    std::map<std::string, obj_content>::iterator objIter= (*tagIter).second.objectList.find(objName);
    if (objIter!=(*tagIter).second.objectList.end()) {
      std::map<unsigned int, rev_ptr>::iterator revIter= (*objIter).second.revisionList.find(revision);
      if (revIter!=(*objIter).second.revisionList.end()) {
	ret = true;
      }
    }
  }
  return ret;
}
