#include "AboutWindow.h"

#include <qlayout.h>
//Added by qt3to4:
#include <Q3VBoxLayout>
#include <QCloseEvent>
#include <QPixmap>

using namespace PixRunConfigurator;

AboutWindow::AboutWindow(QWidget* parent) : QWidget(parent,"write_object_dialog",Qt::WType_Dialog | Qt::WShowModal)
{
	setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
	makeWindow();
}

AboutWindow::~AboutWindow()
{
	delete mime;
}

void AboutWindow::makeWindow()
{
	
	Q3VBoxLayout* layout=new Q3VBoxLayout(this);
	
	content=new Q3TextBrowser(this);
	content->setMinimumSize(600,340);
	
	mime=new Q3MimeSourceFactory();
	mime->setPixmap("logo_atlas",QPixmap("atlas_logo.jpg"));
	mime->setPixmap("logo_uni_siegen",QPixmap("logo_uni_siegen.jpg"));
	
	content->setMimeSourceFactory(mime);
	
	QString html;
	html="<html><body>";
	html+="<table border='0' cellspacing='5' cellpadding='8' width='540' height='330'>";
	html+="<tr><td valign='middle' width='160'>";
	html+="<img src='logo_atlas' border='0'>";
	html+="</td>";
	html+="<td width='390' rowspan='2' valign='bottom'>";
	html+="<h1>PixRunConfigurator</h1>";
	html+="<h3>Version 1.0 Beta</h3>";
	html+="<p><b>Written by<br><br>Oliver Rosenthal</b><br><i>(Univ. of Siegen, Germany)</i></p>";
	html+="<p>A graphical interface editing the configuration ATLAS pixel objects.</p>";
	html+="<p>Please report bugs or request desired functionalities to:</p>";
	html+="<b>rosenthal@hep.physik.uni-siegen.de</b>";
	html+="</td></tr>";
	html+="<tr><td width='160' height='45' valign='middle'><img src='logo_uni_siegen' border='0'></td>";
	html+="</tr></table>";
	html+="</body></html>";
	
	content->setText(html);
	
	layout->addWidget(content);
	
	setCaption("About PixRunConfigurator");
	
	setIcon(QPixmap("icon_main.gif"));
	
	setMinimumSize(sizeHint());
	setMaximumSize(sizeHint());
	
	show();
}

void AboutWindow::closeEvent(QCloseEvent*)
{
	emit windowClosed();
}

