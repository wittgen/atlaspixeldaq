#ifndef _TagChooser_h_
#define _TagChooser_h_

#include <qwidget.h>
#include <qpushbutton.h>
#include <qstring.h>
#include <q3listbox.h>
#include <qcombobox.h>
//Added by qt3to4:
#include <QKeyEvent>
#include <QCloseEvent>

#include "DbConnection.h"

#include <string>

namespace PixRunConfigurator
{

class TagChooser : public QWidget
{
	Q_OBJECT
	
	public:
	
	TagChooser(QWidget* parent,DbConnection* dbConn,std::vector<std::string>& domainList);
	
	protected:
	
	void closeEvent(QCloseEvent*);
	
	private:
	
	QPushButton* okButton;
	QPushButton* cancelButton;
	QComboBox* partitionList;
	QComboBox* idList;
	QComboBox* connList;
	QComboBox* cfgList;
	
	QString* partitionName;
	QString part;
	
	void makeWindow();

	std::vector<std::string> domainList;	
	std::vector<std::string> connectList;	
	std::vector<std::string> confList;	

	DbConnection* dbConn;	
  
	protected:
	
	void keyPressEvent(QKeyEvent* ke);
	
	private slots:
	
	void setConnectList(const QString&);
	void setCfgList(const QString&);
	void enableButton();
	void ok();
	void cancel();
	
	signals:
	
	void okClicked(std::string,std::string,std::string);
	void cancelClicked();

};

}

#endif

