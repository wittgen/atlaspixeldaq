#ifndef _PartitionChooser_h_
#define _PartitionChooser_h_

#include <qwidget.h>
#include <qpushbutton.h>
#include <qstring.h>
#include <q3listbox.h>
//Added by qt3to4:
#include <QCloseEvent>
#include <QKeyEvent>

#include <string>

namespace PixRunConfigurator
{

class PartitionChooser : public QWidget
{
	Q_OBJECT
	
	public:
	
	PartitionChooser(QWidget* parent);
	
	protected:
	
	void closeEvent(QCloseEvent*);
	
	private:
	
	QPushButton* okButton;
	QPushButton* cancelButton;
	Q3ListBox* partitionList;
	
	QString* partitionName;
	
	void makeWindow();
	
	protected:
	
	void keyPressEvent(QKeyEvent* ke);
	
	private slots:
	
	void ok();
	void cancel();
	
	signals:
	
	void okClicked(std::string);
	void cancelClicked();

};

}

#endif

