#include "RodMonitoringTab.h"

#include <qfont.h>
#include <qcursor.h>
//Added by qt3to4:
#include <Q3GridLayout>
#include <Q3HBoxLayout>
#include <Q3VBoxLayout>
#include <QLabel>

#include <iostream>

using namespace PixRunConfigurator;

RodMonitoringTab::RodMonitoringTab(QWidget* parent,CommonDataTabView* root,DbConnection* dbConn,BrowserStateManager* stateManager, PixRunConfig* runConfig)
			  : View(stateManager,parent,QString("readout_struct"))
{
	this->parent=root;
 	this->conn=dbConn->getdefinedPixConnectivity();
	this->runConfig=runConfig;
	
	layout=new Q3HBoxLayout(this);	
	
	revLayout=new Q3VBoxLayout(layout);
	
	detailLayout=new Q3GridLayout(revLayout,19,4);
	detailLayout->setColSpacing(0,20);
	detailLayout->setColSpacing(2,5);
	detailLayout->setColSpacing(3,20);
	detailLayout->setRowSpacing(0,50);
	detailLayout->setRowSpacing(2,5);
	detailLayout->setRowSpacing(4,5);
	detailLayout->setRowSpacing(6,5);
	detailLayout->setRowSpacing(8,5);
	detailLayout->setRowSpacing(10,5);
	detailLayout->setRowSpacing(12,5);
	detailLayout->setRowSpacing(14,5);
	detailLayout->setRowSpacing(16,5);
	detailLayout->setRowSpacing(18,50);
	
	AutoDisable=new QLabel("Enables auto disable at run start:",this);
	detailLayout->addWidget(AutoDisable,3,1);	
	RodMonPrescale=new QLabel("ROD Monitoring prescale factor:",this);
	detailLayout->addWidget(RodMonPrescale,5,1);	
	OccupancyRate=new QLabel("ROD Occupancy histograms sampling rate:",this);
	detailLayout->addWidget(OccupancyRate,7,1);	
	BusyRate=new QLabel("ROD BUSY sampling rate:",this);
	detailLayout->addWidget(BusyRate,9,1);
	StatusRate=new QLabel("ROD status sampling rate:",this);
	detailLayout->addWidget(StatusRate,11,1);
 	BuffRate=new QLabel("ROD buffers sampling rate:",this);
 	detailLayout->addWidget(BuffRate,13,1);

	AutoDisable_edit=new QPushButton(this);
	detailLayout->addWidget(AutoDisable_edit,3,2);
	AutoDisable_edit->setMaximumWidth(80);
	RodMonPrescale_edit=new QSpinBox(this);
	detailLayout->addWidget(RodMonPrescale_edit,5,2);
	RodMonPrescale_edit->setMaximumWidth(80);
	RodMonPrescale_edit->setMaxValue(999);
	OccupancyRate_edit=new QSpinBox(this);
	detailLayout->addWidget(OccupancyRate_edit,7,2);
	OccupancyRate_edit->setMaximumWidth(80);
	OccupancyRate_edit->setMaxValue(999);
	BusyRate_edit=new QSpinBox(this);
	detailLayout->addWidget(BusyRate_edit,9,2);
	BusyRate_edit->setMaximumWidth(80);	
	BusyRate_edit->setMaxValue(999);
	StatusRate_edit=new QSpinBox(this);
	detailLayout->addWidget(StatusRate_edit,11,2);
	StatusRate_edit->setMaximumWidth(80);
	StatusRate_edit->setMaxValue(999);
	BuffRate_edit=new QSpinBox(this);
	detailLayout->addWidget(BuffRate_edit,13,2);
	BuffRate_edit->setMaximumWidth(80);
	BuffRate_edit->setMaxValue(999);
	
	setEnabled(true);
}

RodMonitoringTab::~RodMonitoringTab()
{
 	disconnectAll();
}

void RodMonitoringTab::connectAll()
{
	connect(RodMonPrescale_edit,SIGNAL(valueChanged(int)),this,SLOT(rodMonPrescaleChanges(int)));
	connect(OccupancyRate_edit,SIGNAL(valueChanged(int)),this,SLOT(occupancyRateChanges(int)));
	connect(BusyRate_edit,SIGNAL(valueChanged(int)),this,SLOT(busyRateChanges(int)));
	connect(StatusRate_edit,SIGNAL(valueChanged(int)),this,SLOT(statusRateChanges(int)));
	connect(BuffRate_edit,SIGNAL(valueChanged(int)),this,SLOT(buffRateChanges(int)));
	connect(AutoDisable_edit,SIGNAL(clicked()),this,SLOT(autoDisableChanges()));
}

void RodMonitoringTab::disconnectAll()
{
	connect(RodMonPrescale_edit,SIGNAL(valueChanged(int)),this,SLOT(rodMonPrescaleChanges(int)));
	connect(OccupancyRate_edit,SIGNAL(valueChanged(int)),this,SLOT(occupancyRateChanges(int)));
	connect(BusyRate_edit,SIGNAL(valueChanged(int)),this,SLOT(busyRateChanges(int)));
	connect(StatusRate_edit,SIGNAL(valueChanged(int)),this,SLOT(statusRateChanges(int)));
	connect(BuffRate_edit,SIGNAL(valueChanged(int)),this,SLOT(buffRateChanges(int)));
	connect(AutoDisable_edit,SIGNAL(clicked()),this,SLOT(autoDisableChanges()));
}
	
void RodMonitoringTab::rodMonPrescaleChanges(int value)
{
	runConfig->rodMonPrescale()=value;
        newValues.erase("ROD Monitoring prescale factor");	      
	newValues.insert( pair<std::string,int>("ROD Monitoring prescale factor",runConfig->rodMonPrescale()) );	      
}

void RodMonitoringTab::occupancyRateChanges(int value)
{
	runConfig->occupRate()=value;
        newValues.erase("ROD Occupancy histograms sampling rate");	      
	newValues.insert( pair<std::string,int>("ROD Occupancy histograms sampling rate",runConfig->occupRate()) );	      
}

void RodMonitoringTab::busyRateChanges(int value)
{
	runConfig->busyRate()=value;
        newValues.erase("ROD BUSY sampling rate");	      
	newValues.insert( pair<std::string,int>("ROD BUSY sampling rate",runConfig->busyRate()) );	      
}

void RodMonitoringTab::statusRateChanges(int value)
{
	runConfig->statusRate()=value;
        newValues.erase("ROD status sampling rate");	      
	newValues.insert( pair<std::string,int>("ROD status sampling rate",runConfig->statusRate()) );	      
}

void RodMonitoringTab::buffRateChanges(int value)
{
	runConfig->bufRate()=value;
        newValues.erase("ROD buffers sampling rate");	      
	newValues.insert( pair<std::string,int>("ROD buffers sampling rate",runConfig->bufRate()) );
}

void RodMonitoringTab::autoDisableChanges()
{
        newValues.erase("Enables auto disable at run start");	      
	if(AutoDisable_edit->text()=="On")
	{
	  AutoDisable_edit->setText("Off");
	  runConfig->autoDisable()=false;	  
	  newValues.insert( pair<std::string,int>("Enables auto disable at run start",0) );	      
	}
	else  
	{
	  AutoDisable_edit->setText("On");
	  runConfig->autoDisable()=true;	  
	  newValues.insert( pair<std::string,int>("Enables auto disable at run start",1) );	      
	}
}

void RodMonitoringTab::receiveStatus(PixRunConfigurator::StatusAction action)
{
 	if(action==PixRunConfigurator::editRun)
	{
		showObjectDetails();
	}
 	if(action==PixRunConfigurator::writeRun)
	{
		stateManager->setRodMonitoringChanges(defaultValues,newValues);
		showObjectDetails();
	}	
}

void RodMonitoringTab::showObjectDetails()
{
 	std::string object=stateManager->getCurrentObject();
	runConf=stateManager->getCurrentRunConf();
	runConfig->selectSubConf(object);
	if(runConfig->subConfExists(object)){
	cf = new Config(runConfig->config());
	RodMonPrescale_edit->setValue(runConfig->rodMonPrescale());
	OccupancyRate_edit->setValue(runConfig->occupRate());
	BusyRate_edit->setValue(runConfig->busyRate());
	StatusRate_edit->setValue(runConfig->statusRate());
	BuffRate_edit->setValue(runConfig->bufRate());

	defaultValues.clear();
	newValues.clear();
	
	defaultValues.insert( pair<std::string,int>("ROD Monitoring prescale factor",runConfig->rodMonPrescale()) );	      
	defaultValues.insert( pair<std::string,int>("ROD Occupancy histograms sampling rate",runConfig->occupRate()) );	      
	defaultValues.insert( pair<std::string,int>("ROD BUSY sampling rate",runConfig->busyRate()) );	      
	defaultValues.insert( pair<std::string,int>("ROD status sampling rate",runConfig->statusRate()) );	      
	defaultValues.insert( pair<std::string,int>("ROD buffers sampling rate",runConfig->bufRate()) );	      
		
	if(runConfig->autoDisable())
	{
	  AutoDisable_edit->setText("On");
	  defaultValues.insert( pair<std::string,int>("Enables auto disable at run start",1) );	      
	}
	else
	{
	  AutoDisable_edit->setText("Off");
	  defaultValues.insert( pair<std::string,int>("Enables auto disable at run start",0) );	      
	}
	
	}

	connectAll();
}