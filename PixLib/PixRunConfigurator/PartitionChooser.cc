#include "PartitionChooser.h"

#include <ipc/partition.h>

#include <qlayout.h>
#include <qlabel.h>
//Added by qt3to4:
#include <QPixmap>
#include <Q3GridLayout>
#include <Q3VBoxLayout>
#include <QKeyEvent>
#include <QCloseEvent>

#include <iostream>

using namespace PixRunConfigurator;

PartitionChooser::PartitionChooser(QWidget* parent) : QWidget(parent,"ipc_Chooser",Qt::WType_Dialog | Qt::WShowModal)
{	
	makeWindow();
}

void PartitionChooser::makeWindow()
{
	std::list<IPCPartition> partList;
	
	IPCPartition::getPartitions(partList);
	
	setCaption("Choose partition");
	
	Q3VBoxLayout* layout=new Q3VBoxLayout(this);
	Q3GridLayout* grid1=new Q3GridLayout(layout,7,3);
	grid1->addColSpacing(0,20);
	grid1->addColSpacing(2,20);
	grid1->addRowSpacing(0,20);
	grid1->addRowSpacing(1,20);
	grid1->addRowSpacing(1,60);
	grid1->addRowSpacing(6,10);
	
	QLabel* l1=new QLabel("Please select a partition:",this);	
	grid1->addWidget(l1,1,1);
	partitionList=new Q3ListBox(this);
	std::list<IPCPartition>::iterator it;
	for (it=partList.begin();it!=partList.end();++it)
	  partitionList->insertItem(it->name().c_str());
	partitionList->setCurrentItem(0);
	grid1->addWidget(partitionList,3,1);
	
	Q3GridLayout* grid2=new Q3GridLayout(layout,3,5);
	grid2->addColSpacing(0,10);
	grid2->addColSpacing(2,20);
	grid2->addColSpacing(4,10);
	grid2->addRowSpacing(0,10);
	grid2->addRowSpacing(2,10);
	
	okButton=new QPushButton("Ok",this);
	grid2->addWidget(okButton,1,1);
	cancelButton=new QPushButton("Cancel",this);
	grid2->addWidget(cancelButton,1,3);
	
	connect(okButton,SIGNAL(clicked()),this,SLOT(ok()));
	connect(cancelButton,SIGNAL(clicked()),this,SLOT(cancel()));
	connect(partitionList,SIGNAL(doubleClicked(Q3ListBoxItem*)),this,SLOT(ok()));	
	
	setIcon(QPixmap("icon_main.gif"));
	
	setMinimumSize(QSize(350,250));
	setMaximumSize(QSize(350,250));
	
	setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		
	show();
}

void PartitionChooser::ok()
{
	partitionName=new QString(partitionList->currentText());
	emit okClicked(std::string(partitionName->toLatin1().data()));
}

void PartitionChooser::cancel()
{
	emit cancelClicked();
}

void PartitionChooser::closeEvent(QCloseEvent*)
{
	emit cancelClicked();
}

void PartitionChooser::keyPressEvent(QKeyEvent* ke)
{
	if (ke->key()==Qt::Key_Return)
		ok();
	if (ke->key()==Qt::Key_Escape)
		cancel();
}

