#ifndef _View_h_
#define _View_h_

#include <qobject.h>
#include <q3frame.h>

#include "BrowserStateManager.h"

namespace PixRunConfigurator
{

class BrowserStateManager;

class View : public Q3Frame
{
	
	Q_OBJECT
	
	public:
	
	View(BrowserStateManager* stateManager,QWidget* parent,QString name);
	
	BrowserStateManager* getBrowserStateManager() {return this->stateManager;}
	
	public slots:
	
	virtual void receiveStatus(StatusAction)=0;
	
	protected:
	
	BrowserStateManager* stateManager;
	
	virtual void disconnectAll()=0;
	virtual void connectAll()=0;
	
	void bind();
	void unbind();
	
};

}


#endif


