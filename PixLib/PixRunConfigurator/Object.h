#ifndef _Object_h_
#define _Object_h_

#include <vector>
#include <string>

namespace PixRunConfigurator
{

class Revision;

class Object
{
	public:
	
	const std::vector<unsigned int>& getRevisionList() {return *revList;}
	unsigned int getLastRevision() {return lastRev;}	
	std::string getType() {return type;}
	std::string getTag() {return tag;}
	std::string getName() {return name;}
	std::string getDomain() {return domain;}
	
	void setType(std::string type) {this->type=type;}
	void setTag(std::string tag) {this->tag=tag;}
	void setName(std::string name) {this->name=name;}
	void setDomain(std::string domain) {this->domain=domain;}
	void setLastRevision(unsigned int lastRev) {this->lastRev=lastRev;}
	void setRevisionList(std::vector<unsigned int>* revList) {this->revList=revList;}
	
	
	private:
	
	std::vector<unsigned int>* revList;
	unsigned int lastRev;
	std::string name,type,domain,tag;
	
	
};

}

#endif

